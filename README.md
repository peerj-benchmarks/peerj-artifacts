Peerj Artifacts

In this repository, under the `src` folder, you can find the decompiled code produced during the evaluation of the PeerJ article titled _Combing control flow graphs for decompilation: techniques to emit idiomatic C constructs_.

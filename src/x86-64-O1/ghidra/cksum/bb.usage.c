
#include "cksum.h"

long null_ARRAY_0060fd2_0_8_;
long null_ARRAY_0060fd2_8_8_;
long null_ARRAY_0060ff0_0_8_;
long null_ARRAY_0060ff0_16_8_;
long null_ARRAY_0060ff0_24_8_;
long null_ARRAY_0060ff0_32_8_;
long null_ARRAY_0060ff0_40_8_;
long null_ARRAY_0060ff0_48_8_;
long null_ARRAY_0060ff0_8_8_;
long null_ARRAY_0060ff4_0_4_;
long null_ARRAY_0060ff4_16_8_;
long null_ARRAY_0060ff4_4_4_;
long null_ARRAY_0060ff4_8_4_;
long local_68_2_2_;
long local_6b_1_3_;
long local_6c_4_4_;
long local_6e_4_4_;
long local_6f_4_4_;
long local_70_0_2_;
long local_70_4_4_;
long local_71_0_1_;
long local_71_4_4_;
long local_72_4_4_;
long DAT_00000010;
long DAT_0040cd86;
long DAT_0040ce0a;
long DAT_0040ce3d;
long DAT_0040ce3e;
long DAT_0040ce40;
long DAT_0040d46c;
long DAT_0040d520;
long DAT_0040d524;
long DAT_0040d528;
long DAT_0040d52b;
long DAT_0040d52d;
long DAT_0040d531;
long DAT_0040d535;
long DAT_0040dce4;
long DAT_0040e075;
long DAT_0040e179;
long DAT_0040e17f;
long DAT_0040e191;
long DAT_0040e192;
long DAT_0040e1b0;
long DAT_0040e1b4;
long DAT_0040e23e;
long DAT_0060f8d8;
long DAT_0060f8e8;
long DAT_0060f8f8;
long DAT_0060fcc8;
long DAT_0060fd30;
long DAT_0060fd34;
long DAT_0060fd38;
long DAT_0060fd3c;
long DAT_0060fd40;
long DAT_0060fd48;
long DAT_0060fd50;
long DAT_0060fd60;
long DAT_0060fd68;
long DAT_0060fd80;
long DAT_0060fd88;
long DAT_0060fdd0;
long DAT_0060fdd8;
long DAT_0060fde0;
long DAT_0060fde8;
long DAT_0060ff78;
long DAT_0060ff80;
long DAT_0060ff88;
long DAT_0060ff98;
long fde_0040ed90;
long null_ARRAY_0040d040;
long null_ARRAY_0040d440;
long null_ARRAY_0040d480;
long null_ARRAY_0040e660;
long null_ARRAY_0040e8a0;
long null_ARRAY_0060fd20;
long null_ARRAY_0060fda0;
long null_ARRAY_0060fe00;
long null_ARRAY_0060ff00;
long null_ARRAY_0060ff40;
long PTR_DAT_0060fcc0;
long PTR_null_ARRAY_0060fd18;
void
FUN_00401b5e (uint uParm1)
{
  int iVar1;
  long lVar2;
  undefined8 uVar3;
  undefined **ppuVar4;
  char *pcVar5;
  undefined *puVar6;
  undefined *local_88;
  char *local_80;
  undefined8 local_78;
  char *local_70;
  char *local_68;
  char *local_60;
  char *local_58;
  char *local_50;
  char *local_48;
  char *local_40;
  char *local_38;
  char *local_30;
  undefined8 local_28;
  undefined8 local_20;

  ppuVar4 = &local_88;
  if (uParm1 != 0)
    {
      func_0x00401710 (DAT_0060fd60,
		       "Try \'%s --help\' for more information.\n",
		       DAT_0060fde8);
      goto LAB_00401d1f;
    }
  func_0x00401590 ("Usage: %s [FILE]...\n  or:  %s [OPTION]\n", DAT_0060fde8,
		   DAT_0060fde8);
  uVar3 = DAT_0060fd40;
  func_0x00401720 ("Print CRC checksum and byte counts of each FILE.\n\n",
		   DAT_0060fd40);
  func_0x00401720 ("      --help     display this help and exit\n", uVar3);
  func_0x00401720 ("      --version  output version information and exit\n",
		   uVar3);
  local_88 = &DAT_0040cd86;
  local_80 = "test invocation";
  local_78 = 0x40cde9;
  local_70 = "Multi-call invocation";
  local_68 = "sha224sum";
  local_60 = "sha2 utilities";
  local_58 = "sha256sum";
  local_50 = "sha2 utilities";
  local_48 = "sha384sum";
  local_40 = "sha2 utilities";
  local_38 = "sha512sum";
  local_30 = "sha2 utilities";
  local_28 = 0;
  local_20 = 0;
  puVar6 = &DAT_0040cd86;
  do
    {
      iVar1 = func_0x00401820 ("cksum", puVar6);
      if (iVar1 == 0)
	break;
      ppuVar4 = ppuVar4 + 2;
      puVar6 = *ppuVar4;
    }
  while (puVar6 != (undefined *) 0x0);
  pcVar5 = ppuVar4[1];
  if (pcVar5 == (char *) 0x0)
    {
      func_0x00401590 ("\n%s online help: <%s>\n", "GNU coreutils",
		       "https://www.gnu.org/software/coreutils/");
      lVar2 = func_0x00401880 (5, 0);
      if (lVar2 == 0)
	goto LAB_00401d26;
      iVar1 = func_0x00401780 (lVar2, &DAT_0040ce0a, 3);
      if (iVar1 == 0)
	{
	  func_0x00401590 ("Full documentation at: <%s%s>\n",
			   "https://www.gnu.org/software/coreutils/",
			   "cksum");
	  pcVar5 = "cksum";
	  uVar3 = 0x40cda2;
	  goto LAB_00401d0d;
	}
      pcVar5 = "cksum";
    LAB_00401ccb:
      ;
      func_0x00401590
	("Report %s translation bugs to <https://translationproject.org/team/>\n",
	 "cksum");
    }
  else
    {
      func_0x00401590 ("\n%s online help: <%s>\n", "GNU coreutils",
		       "https://www.gnu.org/software/coreutils/");
      lVar2 = func_0x00401880 (5, 0);
      if ((lVar2 != 0)
	  && (iVar1 = func_0x00401780 (lVar2, &DAT_0040ce0a, 3), iVar1 != 0))
	goto LAB_00401ccb;
    }
  func_0x00401590 ("Full documentation at: <%s%s>\n",
		   "https://www.gnu.org/software/coreutils/", "cksum");
  uVar3 = 0x40e1af;
  if (pcVar5 == "cksum")
    {
      uVar3 = 0x40cda2;
    }
LAB_00401d0d:
  ;
  do
    {
      func_0x00401590
	("or available locally via: info \'(coreutils) %s%s\'\n", pcVar5,
	 uVar3);
    LAB_00401d1f:
      ;
      func_0x004018d0 ((ulong) uParm1);
    LAB_00401d26:
      ;
      func_0x00401590 ("Full documentation at: <%s%s>\n",
		       "https://www.gnu.org/software/coreutils/", "cksum");
      pcVar5 = "cksum";
      uVar3 = 0x40cda2;
    }
  while (true);
}

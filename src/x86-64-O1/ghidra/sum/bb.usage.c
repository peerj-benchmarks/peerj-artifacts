
#include "sum.h"

long null_ARRAY_0061050_0_8_;
long null_ARRAY_0061050_8_8_;
long null_ARRAY_0061070_0_8_;
long null_ARRAY_0061070_16_8_;
long null_ARRAY_0061070_24_8_;
long null_ARRAY_0061070_32_8_;
long null_ARRAY_0061070_40_8_;
long null_ARRAY_0061070_48_8_;
long null_ARRAY_0061070_8_8_;
long null_ARRAY_0061074_0_4_;
long null_ARRAY_0061074_16_8_;
long null_ARRAY_0061074_4_4_;
long null_ARRAY_0061074_8_4_;
long local_68_2_2_;
long local_6b_1_3_;
long local_6c_4_4_;
long local_6e_4_4_;
long local_6f_4_4_;
long local_70_0_2_;
long local_70_4_4_;
long local_71_0_1_;
long local_71_4_4_;
long local_72_4_4_;
long DAT_00000010;
long DAT_0040d78f;
long DAT_0040d7e4;
long DAT_0040d7ea;
long DAT_0040d813;
long DAT_0040d839;
long DAT_0040db58;
long DAT_0040dc58;
long DAT_0040dc5c;
long DAT_0040dc60;
long DAT_0040dc63;
long DAT_0040dc65;
long DAT_0040dc69;
long DAT_0040dc6d;
long DAT_0040e3eb;
long DAT_0040e795;
long DAT_0040e899;
long DAT_0040e89f;
long DAT_0040e8a1;
long DAT_0040e8a2;
long DAT_0040e8c0;
long DAT_0040e8c4;
long DAT_0040e94e;
long DAT_006100b0;
long DAT_006100c0;
long DAT_006100d0;
long DAT_006104a8;
long DAT_00610510;
long DAT_00610514;
long DAT_00610518;
long DAT_0061051c;
long DAT_00610540;
long DAT_00610548;
long DAT_00610550;
long DAT_00610560;
long DAT_00610568;
long DAT_00610580;
long DAT_00610588;
long DAT_006105d0;
long DAT_006105d8;
long DAT_006105e0;
long DAT_006105e8;
long DAT_00610778;
long DAT_00610780;
long DAT_00610788;
long DAT_00610798;
long fde_0040f498;
long null_ARRAY_0040dac0;
long null_ARRAY_0040dbd8;
long null_ARRAY_0040ed60;
long null_ARRAY_0040ef90;
long null_ARRAY_00610500;
long null_ARRAY_006105a0;
long null_ARRAY_00610600;
long null_ARRAY_00610700;
long null_ARRAY_00610740;
long PTR_DAT_006104a0;
long PTR_null_ARRAY_006104f8;
long stack0x00000008;
void
FUN_00401fb9 (uint uParm1)
{
  int iVar1;
  long lVar2;
  undefined8 uVar3;
  undefined **ppuVar4;
  undefined *puVar5;
  undefined *local_88;
  char *local_80;
  undefined8 local_78;
  char *local_70;
  char *local_68;
  char *local_60;
  char *local_58;
  char *local_50;
  char *local_48;
  char *local_40;
  undefined *local_38;
  char *local_30;
  undefined8 local_28;
  undefined8 local_20;

  ppuVar4 = &local_88;
  if (uParm1 != 0)
    {
      func_0x00401790 (DAT_00610560,
		       "Try \'%s --help\' for more information.\n",
		       DAT_006105e8);
      goto LAB_00402191;
    }
  func_0x00401600 ("Usage: %s [OPTION]... [FILE]...\n", DAT_006105e8);
  uVar3 = DAT_00610540;
  func_0x004017a0 ("Print checksum and block counts for each FILE.\n",
		   DAT_00610540);
  func_0x004017a0
    ("\nWith no FILE, or when FILE is -, read standard input.\n", uVar3);
  func_0x004017a0
    ("\n  -r              use BSD sum algorithm, use 1K blocks\n  -s, --sysv      use System V sum algorithm, use 512 bytes blocks\n",
     uVar3);
  func_0x004017a0 ("      --help     display this help and exit\n", uVar3);
  func_0x004017a0 ("      --version  output version information and exit\n",
		   uVar3);
  local_88 = &DAT_0040d78f;
  local_80 = "test invocation";
  local_78 = 0x40d7f2;
  local_70 = "Multi-call invocation";
  local_68 = "sha224sum";
  local_60 = "sha2 utilities";
  local_58 = "sha256sum";
  local_50 = "sha2 utilities";
  local_48 = "sha384sum";
  local_40 = "sha2 utilities";
  local_38 = &DAT_0040d7e4;
  local_30 = "sha2 utilities";
  local_28 = 0;
  local_20 = 0;
  puVar5 = &DAT_0040d78f;
  do
    {
      iVar1 = func_0x004018a0 (&DAT_0040d7ea, puVar5);
      if (iVar1 == 0)
	break;
      ppuVar4 = ppuVar4 + 2;
      puVar5 = *ppuVar4;
    }
  while (puVar5 != (undefined *) 0x0);
  puVar5 = ppuVar4[1];
  if (puVar5 == (undefined *) 0x0)
    {
      func_0x00401600 ("\n%s online help: <%s>\n", "GNU coreutils",
		       "https://www.gnu.org/software/coreutils/");
      lVar2 = func_0x00401900 (5, 0);
      if (lVar2 == 0)
	goto LAB_00402198;
      iVar1 = func_0x00401810 (lVar2, &DAT_0040d813, 3);
      if (iVar1 == 0)
	{
	  func_0x00401600 ("Full documentation at: <%s%s>\n",
			   "https://www.gnu.org/software/coreutils/",
			   &DAT_0040d7ea);
	  puVar5 = &DAT_0040d7ea;
	  uVar3 = 0x40d7ab;
	  goto LAB_0040217f;
	}
      puVar5 = &DAT_0040d7ea;
    LAB_0040213d:
      ;
      func_0x00401600
	("Report %s translation bugs to <https://translationproject.org/team/>\n",
	 &DAT_0040d7ea);
    }
  else
    {
      func_0x00401600 ("\n%s online help: <%s>\n", "GNU coreutils",
		       "https://www.gnu.org/software/coreutils/");
      lVar2 = func_0x00401900 (5, 0);
      if ((lVar2 != 0)
	  && (iVar1 = func_0x00401810 (lVar2, &DAT_0040d813, 3), iVar1 != 0))
	goto LAB_0040213d;
    }
  func_0x00401600 ("Full documentation at: <%s%s>\n",
		   "https://www.gnu.org/software/coreutils/", &DAT_0040d7ea);
  uVar3 = 0x40e8bf;
  if (puVar5 == &DAT_0040d7ea)
    {
      uVar3 = 0x40d7ab;
    }
LAB_0040217f:
  ;
  do
    {
      func_0x00401600
	("or available locally via: info \'(coreutils) %s%s\'\n", puVar5,
	 uVar3);
    LAB_00402191:
      ;
      func_0x00401960 ((ulong) uParm1);
    LAB_00402198:
      ;
      func_0x00401600 ("Full documentation at: <%s%s>\n",
		       "https://www.gnu.org/software/coreutils/",
		       &DAT_0040d7ea);
      puVar5 = &DAT_0040d7ea;
      uVar3 = 0x40d7ab;
    }
  while (true);
}

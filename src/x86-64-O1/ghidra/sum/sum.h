typedef unsigned char   undefined;

typedef unsigned char    byte;
typedef unsigned char    dwfenc;
typedef unsigned int    dword;
typedef unsigned long    qword;
typedef unsigned char    undefined1;
typedef unsigned short    undefined2;
typedef unsigned int    undefined4;
typedef unsigned long    undefined6;
typedef unsigned long    undefined8;
typedef unsigned short    word;
#define unkbyte9   unsigned long long
#define unkbyte10   unsigned long long
#define unkbyte11   unsigned long long
#define unkbyte12   unsigned long long
#define unkbyte13   unsigned long long
#define unkbyte14   unsigned long long
#define unkbyte15   unsigned long long
#define unkbyte16   unsigned long long

#define unkuint9   unsigned long long
#define unkuint10   unsigned long long
#define unkuint11   unsigned long long
#define unkuint12   unsigned long long
#define unkuint13   unsigned long long
#define unkuint14   unsigned long long
#define unkuint15   unsigned long long
#define unkuint16   unsigned long long

#define unkint9   long long
#define unkint10   long long
#define unkint11   long long
#define unkint12   long long
#define unkint13   long long
#define unkint14   long long
#define unkint15   long long
#define unkint16   long long

#define unkfloat1   float
#define unkfloat2   float
#define unkfloat3   float
#define unkfloat5   double
#define unkfloat6   double
#define unkfloat7   double
#define unkfloat9   long double
#define unkfloat11   long double
#define unkfloat12   long double
#define unkfloat13   long double
#define unkfloat14   long double
#define unkfloat15   long double
#define unkfloat16   long double

#define BADSPACEBASE   void
#define code   void

#define ulong long
#define true 1
#define bool int
#define false 0
#define float10 int
#define ushort int
#define undefined7 long
#define undefined2 long
#define uint5 long
#define uint6 long

// Add definition for uint
typedef unsigned int    uint;

#define sbyte char

extern imperfection_wrapper(void);
typedef struct eh_frame_hdr eh_frame_hdr, *Peh_frame_hdr;

struct eh_frame_hdr {
    byte eh_frame_hdr_version; // Exception Handler Frame Header Version
    dwfenc eh_frame_pointer_encoding; // Exception Handler Frame Pointer Encoding
    dwfenc eh_frame_desc_entry_count_encoding; // Encoding of # of Exception Handler FDEs
    dwfenc eh_frame_table_encoding; // Exception Handler Table Encoding
};

typedef struct fde_table_entry fde_table_entry, *Pfde_table_entry;

struct fde_table_entry {
    dword initial_loc; // Initial Location
    dword data_loc; // Data location
};

typedef enum Elf64_DynTag {
    DT_AUDIT=1879047932,
    DT_AUXILIARY=2147483645,
    DT_BIND_NOW=24,
    DT_CHECKSUM=1879047672,
    DT_CONFIG=1879047930,
    DT_DEBUG=21,
    DT_DEPAUDIT=1879047931,
    DT_ENCODING=32,
    DT_FEATURE_1=1879047676,
    DT_FILTER=2147483647,
    DT_FINI=13,
    DT_FINI_ARRAY=26,
    DT_FINI_ARRAYSZ=28,
    DT_FLAGS=30,
    DT_FLAGS_1=1879048187,
    DT_GNU_CONFLICT=1879047928,
    DT_GNU_CONFLICTSZ=1879047670,
    DT_GNU_HASH=1879047925,
    DT_GNU_LIBLIST=1879047929,
    DT_GNU_LIBLISTSZ=1879047671,
    DT_GNU_PRELINKED=1879047669,
    DT_HASH=4,
    DT_INIT=12,
    DT_INIT_ARRAY=25,
    DT_INIT_ARRAYSZ=27,
    DT_JMPREL=23,
    DT_MOVEENT=1879047674,
    DT_MOVESZ=1879047675,
    DT_MOVETAB=1879047934,
    DT_NEEDED=1,
    DT_NULL=0,
    DT_PLTGOT=3,
    DT_PLTPAD=1879047933,
    DT_PLTPADSZ=1879047673,
    DT_PLTREL=20,
    DT_PLTRELSZ=2,
    DT_POSFLAG_1=1879047677,
    DT_PREINIT_ARRAYSZ=33,
    DT_REL=17,
    DT_RELA=7,
    DT_RELACOUNT=1879048185,
    DT_RELAENT=9,
    DT_RELASZ=8,
    DT_RELCOUNT=1879048186,
    DT_RELENT=19,
    DT_RELSZ=18,
    DT_RPATH=15,
    DT_RUNPATH=29,
    DT_SONAME=14,
    DT_STRSZ=10,
    DT_STRTAB=5,
    DT_SYMBOLIC=16,
    DT_SYMENT=11,
    DT_SYMINENT=1879047679,
    DT_SYMINFO=1879047935,
    DT_SYMINSZ=1879047678,
    DT_SYMTAB=6,
    DT_TEXTREL=22,
    DT_TLSDESC_GOT=1879047927,
    DT_TLSDESC_PLT=1879047926,
    DT_VERDEF=1879048188,
    DT_VERDEFNUM=1879048189,
    DT_VERNEED=1879048190,
    DT_VERNEEDNUM=1879048191,
    DT_VERSYM=1879048176
} Elf64_DynTag;

typedef enum Elf_ProgramHeaderType {
    PT_DYNAMIC=2,
    PT_GNU_EH_FRAME=1685382480,
    PT_GNU_RELRO=1685382482,
    PT_GNU_STACK=1685382481,
    PT_INTERP=3,
    PT_LOAD=1,
    PT_NOTE=4,
    PT_NULL=0,
    PT_PHDR=6,
    PT_SHLIB=5,
    PT_TLS=7
} Elf_ProgramHeaderType;

typedef struct Elf64_Dyn Elf64_Dyn, *PElf64_Dyn;

struct Elf64_Dyn {
    enum Elf64_DynTag d_tag;
    qword d_val;
};

typedef struct Elf64_Shdr Elf64_Shdr, *PElf64_Shdr;

typedef enum Elf_SectionHeaderType {
    SHT_CHECKSUM=1879048184,
    SHT_DYNAMIC=6,
    SHT_DYNSYM=11,
    SHT_FINI_ARRAY=15,
    SHT_GNU_ATTRIBUTES=1879048181,
    SHT_GNU_HASH=1879048182,
    SHT_GNU_LIBLIST=1879048183,
    SHT_GNU_verdef=1879048189,
    SHT_GNU_verneed=1879048190,
    SHT_GNU_versym=1879048191,
    SHT_GROUP=17,
    SHT_HASH=5,
    SHT_INIT_ARRAY=14,
    SHT_NOBITS=8,
    SHT_NOTE=7,
    SHT_NULL=0,
    SHT_PREINIT_ARRAY=16,
    SHT_PROGBITS=1,
    SHT_REL=9,
    SHT_RELA=4,
    SHT_SHLIB=10,
    SHT_STRTAB=3,
    SHT_SUNW_COMDAT=1879048187,
    SHT_SUNW_move=1879048186,
    SHT_SUNW_syminfo=1879048188,
    SHT_SYMTAB=2,
    SHT_SYMTAB_SHNDX=18
} Elf_SectionHeaderType;

struct Elf64_Shdr {
    dword sh_name;
    enum Elf_SectionHeaderType sh_type;
    qword sh_flags;
    qword sh_addr;
    qword sh_offset;
    qword sh_size;
    dword sh_link;
    dword sh_info;
    qword sh_addralign;
    qword sh_entsize;
};

typedef struct Elf64_Phdr Elf64_Phdr, *PElf64_Phdr;

struct Elf64_Phdr {
    enum Elf_ProgramHeaderType p_type;
    dword p_flags;
    qword p_offset;
    qword p_vaddr;
    qword p_paddr;
    qword p_filesz;
    qword p_memsz;
    qword p_align;
};

typedef struct Elf64_Sym Elf64_Sym, *PElf64_Sym;

struct Elf64_Sym {
    dword st_name;
    byte st_info;
    byte st_other;
    word st_shndx;
    qword st_value;
    qword st_size;
};

typedef struct Elf64_Rela Elf64_Rela, *PElf64_Rela;

struct Elf64_Rela {
    qword r_offset; // location to apply the relocation action
    qword r_info; // the symbol table index and the type of relocation
    qword r_addend; // a constant addend used to compute the relocatable field value
};

typedef struct Elf64_Ehdr Elf64_Ehdr, *PElf64_Ehdr;

struct Elf64_Ehdr {
    byte e_ident_magic_num;
    char e_ident_magic_str[3];
    byte e_ident_class;
    byte e_ident_data;
    byte e_ident_version;
    byte e_ident_pad[9];
    word e_type;
    word e_machine;
    dword e_version;
    qword e_entry;
    qword e_phoff;
    qword e_shoff;
    dword e_flags;
    word e_ehsize;
    word e_phentsize;
    word e_phnum;
    word e_shentsize;
    word e_shnum;
    word e_shstrndx;
};




undefined8 _DT_INIT(void);
void FUN_004015e0(void);
void entry(void);
void FUN_00401ab0(void);
void FUN_00401b30(void);
void FUN_00401bb0(void);
undefined8 FUN_00401bee(undefined8 uParm1,int iParm2);
undefined8 FUN_00401de0(undefined8 uParm1,int iParm2);
void FUN_00401fb9(uint uParm1);
ulong FUN_0040222f(uint uParm1,undefined8 *puParm2);
void FUN_004023d4(void);
void FUN_0040247a(long lParm1,ulong uParm2);
void FUN_0040249d(void);
char * FUN_00402572(ulong uParm1,long lParm2,ulong uParm3,ulong uParm4,ulong uParm5);
void FUN_00402c94(long lParm1);
int * FUN_00402d29(int *piParm1,int iParm2);
undefined * FUN_00402d59(char *pcParm1,int iParm2);
ulong FUN_00402e11(undefined *param_1,ulong param_2,long param_3,ulong param_4,uint param_5,ulong param_6,long param_7,char *param_8,char *param_9);
undefined1 * FUN_00403bc8(int iParm1,undefined8 uParm2,undefined8 uParm3,uint *puParm4);
ulong FUN_00403d6a(undefined1 *puParm1,byte bParm2,undefined8 uParm3);
void FUN_00403d9e(undefined8 uParm1,undefined8 uParm2,char cParm3);
void FUN_00403e27(undefined8 uParm1,char cParm2);
void FUN_00403e40(undefined8 uParm1);
void FUN_00403e53(uint uParm1,undefined8 uParm2,undefined8 uParm3);
long FUN_00403ee2(uint uParm1,undefined8 uParm2,ulong uParm3);
void FUN_00403f43(undefined8 uParm1,long lParm2,undefined8 uParm3,undefined8 uParm4,undefined8 *puParm5,undefined8 uParm6);
void FUN_00404180(void);
void FUN_004041d2(void);
void FUN_00404257(long lParm1);
long FUN_00404271(long lParm1,long lParm2);
void FUN_004042a4(void);
void FUN_004042cc(void);
void FUN_004042f9(undefined8 uParm1);
void FUN_00404339(uint uParm1,uint uParm2,undefined8 uParm3,undefined8 uParm4);
void FUN_00404390(undefined4 uParm1,undefined4 uParm2,undefined4 uParm3,undefined4 uParm4,undefined4 uParm5,undefined4 uParm6,undefined4 uParm7,undefined4 uParm8,ulong param_1,ulong param_2,undefined8 param_3,undefined8 param_4,undefined8 param_5,undefined8 param_6);
undefined8 FUN_00404463(undefined8 uParm1);
ulong FUN_004044e6(long lParm1);
undefined8 FUN_00404576(void);
void FUN_00404589(void);
void FUN_00404597(long lParm1,int *piParm2);
ulong FUN_00404656(int param_1,undefined8 *param_2,char *param_3,long *param_4,int *param_5,int param_6,int *param_7,int param_8,undefined8 param_9);
ulong FUN_00404b11(uint param_1,undefined8 *param_2,char *param_3,long param_4,undefined8 param_5,ulong param_6,uint *param_7,int param_8);
void FUN_00404f8d(void);
void FUN_00404fe3(void);
void FUN_00404ff9(long lParm1);
ulong FUN_00405013(uint *puParm1,byte *pbParm2,long lParm3);
void FUN_00405078(long lParm1,long lParm2);
ulong FUN_004050a6(undefined8 uParm1,undefined8 uParm2,undefined8 uParm3);
ulong FUN_00405173(undefined8 uParm1);
undefined8 FUN_004051d4(void);
ulong FUN_004051dc(ulong uParm1);
char * FUN_0040526a(void);
ulong FUN_004055a5(char *pcParm1,long lParm2);
long FUN_004055ec(ulong uParm1,uint *puParm2,ulong uParm3,uint *puParm4,long *plParm5);
ulong FUN_00405710(void);
ulong FUN_00405742(void);
int * FUN_0040594f(ulong uParm1,long lParm2,ulong uParm3,uint *puParm4,ulong *puParm5);
char * FUN_00405f32(int iParm1,long lParm2,long lParm3,long lParm4,uint uParm5);
void FUN_0040654a(uint param_1,undefined8 param_2);
ulong FUN_00406710(void);
void FUN_00406923(undefined8 uParm1,uint uParm2);
undefined8 *FUN_00406ac4(undefined8 *puParm1,undefined8 *puParm2,undefined8 *puParm3,undefined8 uParm4);
double FUN_0040c661(int *piParm1);
void FUN_0040c6a9(int *param_1);
undefined8 FUN_0040c72a(uint *puParm1,ulong *puParm2);
undefined8 FUN_0040cb4f(byte *pbParm1,ulong *puParm2,byte **ppbParm3);
ulong FUN_0040d6b4(void);
ulong FUN_0040d6dc(void);
void FUN_0040d710(void);
undefined8 _DT_FINI(void);
undefined FUN_00611000();
undefined FUN_00611008();
undefined FUN_00611010();
undefined FUN_00611018();
undefined FUN_00611020();
undefined FUN_00611028();
undefined FUN_00611030();
undefined FUN_00611038();
undefined FUN_00611040();
undefined FUN_00611048();
undefined FUN_00611050();
undefined FUN_00611058();
undefined FUN_00611060();
undefined FUN_00611068();
undefined FUN_00611070();
undefined FUN_00611078();
undefined FUN_00611080();
undefined FUN_00611088();
undefined FUN_00611090();
undefined FUN_00611098();
undefined FUN_006110a0();
undefined FUN_006110a8();
undefined FUN_006110b0();
undefined FUN_006110b8();
undefined FUN_006110c0();
undefined FUN_006110c8();
undefined FUN_006110d0();
undefined FUN_006110d8();
undefined FUN_006110e0();
undefined FUN_006110e8();
undefined FUN_006110f0();
undefined FUN_006110f8();
undefined FUN_00611100();
undefined FUN_00611108();
undefined FUN_00611110();
undefined FUN_00611118();
undefined FUN_00611120();
undefined FUN_00611128();
undefined FUN_00611130();
undefined FUN_00611138();
undefined FUN_00611140();
undefined FUN_00611148();
undefined FUN_00611150();
undefined FUN_00611158();
undefined FUN_00611160();
undefined FUN_00611168();
undefined FUN_00611170();
undefined FUN_00611178();
undefined FUN_00611180();
undefined FUN_00611188();
undefined FUN_00611190();
undefined FUN_00611198();
undefined FUN_006111a0();
undefined FUN_006111a8();
undefined FUN_006111b0();
undefined FUN_006111b8();
undefined FUN_006111c0();
undefined FUN_006111c8();
undefined FUN_006111d0();
undefined FUN_006111d8();
undefined FUN_006111e0();
undefined FUN_006111e8();
undefined FUN_006111f0();
undefined FUN_006111f8();
undefined FUN_00611200();
undefined FUN_00611208();
undefined FUN_00611210();
undefined FUN_00611218();
undefined FUN_00611220();
undefined FUN_00611228();
undefined FUN_00611230();
undefined FUN_00611238();


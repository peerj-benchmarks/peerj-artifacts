typedef unsigned char   undefined;

typedef unsigned char    byte;
typedef unsigned char    dwfenc;
typedef unsigned int    dword;
typedef unsigned long    qword;
typedef unsigned char    undefined1;
typedef unsigned short    undefined2;
typedef unsigned int    undefined3;
typedef unsigned int    undefined4;
typedef unsigned long    undefined5;
typedef unsigned long    undefined6;
typedef unsigned long    undefined8;
typedef unsigned short    word;
#define unkbyte9   unsigned long long
#define unkbyte10   unsigned long long
#define unkbyte11   unsigned long long
#define unkbyte12   unsigned long long
#define unkbyte13   unsigned long long
#define unkbyte14   unsigned long long
#define unkbyte15   unsigned long long
#define unkbyte16   unsigned long long

#define unkuint9   unsigned long long
#define unkuint10   unsigned long long
#define unkuint11   unsigned long long
#define unkuint12   unsigned long long
#define unkuint13   unsigned long long
#define unkuint14   unsigned long long
#define unkuint15   unsigned long long
#define unkuint16   unsigned long long

#define unkint9   long long
#define unkint10   long long
#define unkint11   long long
#define unkint12   long long
#define unkint13   long long
#define unkint14   long long
#define unkint15   long long
#define unkint16   long long

#define unkfloat1   float
#define unkfloat2   float
#define unkfloat3   float
#define unkfloat5   double
#define unkfloat6   double
#define unkfloat7   double
#define unkfloat9   long double
#define unkfloat11   long double
#define unkfloat12   long double
#define unkfloat13   long double
#define unkfloat14   long double
#define unkfloat15   long double
#define unkfloat16   long double

#define BADSPACEBASE   void
#define code   void

#define ulong long
#define true 1
#define bool int
#define false 0
#define float10 int
#define ushort int
#define undefined7 long
#define undefined2 long
#define uint5 long
#define uint6 long

// Add definition for uint
typedef unsigned int    uint;

#define sbyte char

extern imperfection_wrapper(void);
typedef struct eh_frame_hdr eh_frame_hdr, *Peh_frame_hdr;

struct eh_frame_hdr {
    byte eh_frame_hdr_version; // Exception Handler Frame Header Version
    dwfenc eh_frame_pointer_encoding; // Exception Handler Frame Pointer Encoding
    dwfenc eh_frame_desc_entry_count_encoding; // Encoding of # of Exception Handler FDEs
    dwfenc eh_frame_table_encoding; // Exception Handler Table Encoding
};

typedef struct fde_table_entry fde_table_entry, *Pfde_table_entry;

struct fde_table_entry {
    dword initial_loc; // Initial Location
    dword data_loc; // Data location
};

typedef enum Elf64_DynTag {
    DT_AUDIT=1879047932,
    DT_AUXILIARY=2147483645,
    DT_BIND_NOW=24,
    DT_CHECKSUM=1879047672,
    DT_CONFIG=1879047930,
    DT_DEBUG=21,
    DT_DEPAUDIT=1879047931,
    DT_ENCODING=32,
    DT_FEATURE_1=1879047676,
    DT_FILTER=2147483647,
    DT_FINI=13,
    DT_FINI_ARRAY=26,
    DT_FINI_ARRAYSZ=28,
    DT_FLAGS=30,
    DT_FLAGS_1=1879048187,
    DT_GNU_CONFLICT=1879047928,
    DT_GNU_CONFLICTSZ=1879047670,
    DT_GNU_HASH=1879047925,
    DT_GNU_LIBLIST=1879047929,
    DT_GNU_LIBLISTSZ=1879047671,
    DT_GNU_PRELINKED=1879047669,
    DT_HASH=4,
    DT_INIT=12,
    DT_INIT_ARRAY=25,
    DT_INIT_ARRAYSZ=27,
    DT_JMPREL=23,
    DT_MOVEENT=1879047674,
    DT_MOVESZ=1879047675,
    DT_MOVETAB=1879047934,
    DT_NEEDED=1,
    DT_NULL=0,
    DT_PLTGOT=3,
    DT_PLTPAD=1879047933,
    DT_PLTPADSZ=1879047673,
    DT_PLTREL=20,
    DT_PLTRELSZ=2,
    DT_POSFLAG_1=1879047677,
    DT_PREINIT_ARRAYSZ=33,
    DT_REL=17,
    DT_RELA=7,
    DT_RELACOUNT=1879048185,
    DT_RELAENT=9,
    DT_RELASZ=8,
    DT_RELCOUNT=1879048186,
    DT_RELENT=19,
    DT_RELSZ=18,
    DT_RPATH=15,
    DT_RUNPATH=29,
    DT_SONAME=14,
    DT_STRSZ=10,
    DT_STRTAB=5,
    DT_SYMBOLIC=16,
    DT_SYMENT=11,
    DT_SYMINENT=1879047679,
    DT_SYMINFO=1879047935,
    DT_SYMINSZ=1879047678,
    DT_SYMTAB=6,
    DT_TEXTREL=22,
    DT_TLSDESC_GOT=1879047927,
    DT_TLSDESC_PLT=1879047926,
    DT_VERDEF=1879048188,
    DT_VERDEFNUM=1879048189,
    DT_VERNEED=1879048190,
    DT_VERNEEDNUM=1879048191,
    DT_VERSYM=1879048176
} Elf64_DynTag;

typedef enum Elf_ProgramHeaderType {
    PT_DYNAMIC=2,
    PT_GNU_EH_FRAME=1685382480,
    PT_GNU_RELRO=1685382482,
    PT_GNU_STACK=1685382481,
    PT_INTERP=3,
    PT_LOAD=1,
    PT_NOTE=4,
    PT_NULL=0,
    PT_PHDR=6,
    PT_SHLIB=5,
    PT_TLS=7
} Elf_ProgramHeaderType;

typedef struct Elf64_Dyn Elf64_Dyn, *PElf64_Dyn;

struct Elf64_Dyn {
    enum Elf64_DynTag d_tag;
    qword d_val;
};

typedef struct Elf64_Shdr Elf64_Shdr, *PElf64_Shdr;

typedef enum Elf_SectionHeaderType {
    SHT_CHECKSUM=1879048184,
    SHT_DYNAMIC=6,
    SHT_DYNSYM=11,
    SHT_FINI_ARRAY=15,
    SHT_GNU_ATTRIBUTES=1879048181,
    SHT_GNU_HASH=1879048182,
    SHT_GNU_LIBLIST=1879048183,
    SHT_GNU_verdef=1879048189,
    SHT_GNU_verneed=1879048190,
    SHT_GNU_versym=1879048191,
    SHT_GROUP=17,
    SHT_HASH=5,
    SHT_INIT_ARRAY=14,
    SHT_NOBITS=8,
    SHT_NOTE=7,
    SHT_NULL=0,
    SHT_PREINIT_ARRAY=16,
    SHT_PROGBITS=1,
    SHT_REL=9,
    SHT_RELA=4,
    SHT_SHLIB=10,
    SHT_STRTAB=3,
    SHT_SUNW_COMDAT=1879048187,
    SHT_SUNW_move=1879048186,
    SHT_SUNW_syminfo=1879048188,
    SHT_SYMTAB=2,
    SHT_SYMTAB_SHNDX=18
} Elf_SectionHeaderType;

struct Elf64_Shdr {
    dword sh_name;
    enum Elf_SectionHeaderType sh_type;
    qword sh_flags;
    qword sh_addr;
    qword sh_offset;
    qword sh_size;
    dword sh_link;
    dword sh_info;
    qword sh_addralign;
    qword sh_entsize;
};

typedef struct Elf64_Phdr Elf64_Phdr, *PElf64_Phdr;

struct Elf64_Phdr {
    enum Elf_ProgramHeaderType p_type;
    dword p_flags;
    qword p_offset;
    qword p_vaddr;
    qword p_paddr;
    qword p_filesz;
    qword p_memsz;
    qword p_align;
};

typedef struct Elf64_Rela Elf64_Rela, *PElf64_Rela;

struct Elf64_Rela {
    qword r_offset; // location to apply the relocation action
    qword r_info; // the symbol table index and the type of relocation
    qword r_addend; // a constant addend used to compute the relocatable field value
};

typedef struct Elf64_Sym Elf64_Sym, *PElf64_Sym;

struct Elf64_Sym {
    dword st_name;
    byte st_info;
    byte st_other;
    word st_shndx;
    qword st_value;
    qword st_size;
};

typedef struct Elf64_Ehdr Elf64_Ehdr, *PElf64_Ehdr;

struct Elf64_Ehdr {
    byte e_ident_magic_num;
    char e_ident_magic_str[3];
    byte e_ident_class;
    byte e_ident_data;
    byte e_ident_version;
    byte e_ident_pad[9];
    word e_type;
    word e_machine;
    dword e_version;
    qword e_entry;
    qword e_phoff;
    qword e_shoff;
    dword e_flags;
    word e_ehsize;
    word e_phentsize;
    word e_phnum;
    word e_shentsize;
    word e_shnum;
    word e_shstrndx;
};




undefined8 _DT_INIT(void);
void FUN_00402510(void);
void FUN_00402790(void);
void FUN_00402a30(void);
void FUN_00402d50(void);
void entry(void);
void FUN_00402db0(void);
void FUN_00402e30(void);
void FUN_00402eb0(void);
ulong FUN_00402eee(long lParm1,undefined8 uParm2,long lParm3);
ulong FUN_00403000(undefined8 uParm1,undefined8 uParm2,char cParm3,undefined8 uParm4);
void FUN_00403083(uint uParm1);
ulong FUN_00403333(uint uParm1,undefined8 *puParm2);
void FUN_004039d9(long lParm1);
undefined8 FUN_00403a09(undefined8 uParm1,undefined8 uParm2,long lParm3);
undefined8 FUN_00403a51(long lParm1,long lParm2,char *pcParm3,char cParm4);
void FUN_00403bd1(undefined8 uParm1,undefined8 uParm2);
ulong FUN_00403be9(long lParm1,long lParm2,char cParm3,char *pcParm4,int iParm5,int *piParm6);
ulong FUN_004040a2(long *plParm1,long lParm2);
ulong FUN_00404668(undefined8 uParm1,undefined8 uParm2);
void FUN_004046af(long lParm1,undefined8 uParm2,long lParm3);
void FUN_00404779(undefined8 uParm1,undefined8 uParm2,long lParm3);
ulong FUN_004047f2(undefined8 uParm1,undefined8 uParm2,byte bParm3,uint uParm4,char cParm5);
undefined8 FUN_004048b1(uint uParm1,ulong uParm2);
ulong FUN_00404943(undefined8 uParm1,undefined8 uParm2,undefined8 uParm3);
undefined8 FUN_0040497f(ulong uParm1,undefined8 uParm2,char cParm3,long lParm4);
undefined8 FUN_00404a41(uint param_1,uint param_2,long param_3,ulong param_4,ulong param_5,byte param_6,undefined8 param_7,undefined8 param_8,ulong param_9,long *param_10,byte *param_11);
void FUN_00404e48(undefined8 uParm1,undefined8 uParm2,uint uParm3);
ulong FUN_00404e6b(void);
ulong FUN_00404e92(undefined8 uParm1,undefined8 uParm2,undefined8 uParm3,ulong uParm4,long lParm5);
undefined8 FUN_00404fc2(undefined8 uParm1,undefined8 uParm2,undefined8 uParm3,long lParm4);
void FUN_00405059(long lParm1);
void FUN_00405082(long lParm1);
ulong FUN_004050a7(long lParm1);
ulong FUN_004050d5(long lParm1,undefined8 uParm2,uint uParm3,long lParm4,char cParm5,long lParm6);
ulong FUN_004052a6(void);
ulong FUN_004052dc(char *param_1,char *param_2,ulong param_3,long **param_4,long **param_5,long **param_6,uint param_7,byte *param_8,byte *param_9,undefined *param_10);
void FUN_0040908b(undefined8 uParm1,undefined8 uParm2,ulong uParm3,uint *puParm4,undefined8 uParm5,undefined8 uParm6);
ulong FUN_004091be(ulong *puParm1,ulong uParm2);
ulong FUN_004091cd(long *plParm1,long *plParm2);
void FUN_004091ec(long lParm1);
void FUN_00409203(undefined8 uParm1,undefined8 uParm2);
undefined8 FUN_0040923a(undefined8 uParm1,undefined8 uParm2);
undefined8 FUN_0040926b(undefined8 uParm1,undefined8 uParm2,undefined8 uParm3);
void FUN_004092dd(void);
void FUN_00409316(undefined4 uParm1,undefined4 *puParm2);
ulong FUN_00409340(uint *puParm1);
void FUN_004095d9(undefined8 uParm1,uint *puParm2);
long FUN_004095fa(long lParm1,long lParm2);
void FUN_0040964c(undefined8 uParm1,undefined8 *puParm2);
ulong FUN_00409666(undefined4 uParm1,undefined8 uParm2,ulong uParm3,undefined8 uParm4,undefined4 uParm5,byte bParm6);
ulong FUN_0040977e(undefined8 uParm1,ulong uParm2,undefined8 uParm3,byte bParm4);
ulong FUN_0040988d(undefined8 uParm1,undefined8 uParm2,undefined8 uParm3);
ulong FUN_00409905(undefined8 uParm1);
long FUN_0040994b(undefined8 uParm1,ulong uParm2);
void FUN_00409a2a(long lParm1,long lParm2,undefined uParm3);
void FUN_00409b10(char *pcParm1);
long FUN_00409b59(long lParm1,int iParm2,char cParm3);
void FUN_0040a035(undefined8 uParm1,undefined8 uParm2);
ulong FUN_0040a048(undefined8 uParm1,char *pcParm2);
void FUN_0040a084(undefined8 uParm1,char *pcParm2);
ulong FUN_0040a0b5(ulong uParm1,ulong uParm2,ulong uParm3);
char * FUN_0040a127(char *pcParm1,uint uParm2);
void FUN_0040a6c1(undefined8 uParm1);
void FUN_0040a6d4(void);
void FUN_0040a7ac(void);
long FUN_0040a852(void);
void FUN_0040a8e1(void);
ulong FUN_0040a8f9(char *pcParm1);
undefined * FUN_0040a950(undefined8 uParm1);
char * FUN_0040a9b4(char *pcParm1);
ulong FUN_0040a9fa(long lParm1);
undefined FUN_0040aa34(char *pcParm1);;
void FUN_0040aa67(void);
void FUN_0040aa75(undefined8 param_1,byte param_2,ulong param_3);
void FUN_0040aabf(long lParm1,undefined8 uParm2,undefined8 *puParm3);
ulong FUN_0040ab2d(long lParm1,undefined8 uParm2,undefined8 *puParm3);
char * FUN_0040ab64(long lParm1);
void FUN_0040abdc(ulong uParm1,undefined *puParm2);
void FUN_0040ad4d(void);
long FUN_0040ad65(long lParm1,char *pcParm2,undefined8 *puParm3);
long FUN_0040ae3e(uint uParm1,long lParm2,long lParm3);
ulong FUN_0040aea3(ulong uParm1,ulong uParm2);
undefined FUN_0040aeb6(long lParm1,long lParm2);;
ulong FUN_0040aebd(long lParm1,undefined8 uParm2,uint uParm3);
ulong FUN_0040af41(ulong uParm1,long lParm2);
long FUN_0040b05d(long *plParm1,undefined8 uParm2);
undefined8 FUN_0040b07f(long lParm1,long **pplParm2,char cParm3);
long FUN_0040b1a3(long lParm1,long lParm2,long **pplParm3,char cParm4);
long FUN_0040b2a7(long lParm1,long lParm2);
long * FUN_0040b303(undefined8 uParm1,undefined1 *puParm2,code *pcParm3,code *pcParm4,long lParm5);
void FUN_0040b3ee(long **pplParm1);
ulong FUN_0040b493(long *plParm1,undefined8 uParm2);
undefined8 FUN_0040b5d0(long lParm1,long lParm2,long *plParm3);
undefined8 FUN_0040b7fd(undefined8 uParm1,undefined8 uParm2);
long FUN_0040b82c(long lParm1,undefined8 uParm2);
ulong FUN_0040ba08(undefined8 *puParm1,ulong uParm2);
ulong FUN_0040ba32(undefined8 *puParm1,undefined8 *puParm2);
ulong FUN_0040ba69(undefined8 *puParm1,undefined8 *puParm2);
void FUN_0040baa5(undefined8 *puParm1);
void FUN_0040babb(long lParm1);
undefined8 FUN_0040bb50(undefined8 uParm1,undefined8 uParm2,undefined8 uParm3,uint uParm4,uint uParm5);
void FUN_0040bb85(undefined8 uParm1,uint uParm2,undefined4 uParm3);
int * FUN_0040bbc1(int *piParm1,int iParm2);
undefined * FUN_0040bbf1(char *pcParm1,int iParm2);
ulong FUN_0040bca9(undefined *param_1,ulong param_2,long param_3,ulong param_4,uint param_5,ulong param_6,long param_7,char *param_8,char *param_9);
undefined1 * FUN_0040ca60(int iParm1,undefined8 uParm2,undefined8 uParm3,uint *puParm4);
ulong FUN_0040cc02(undefined1 *puParm1,byte bParm2,undefined8 uParm3);
void FUN_0040cc36(uint uParm1,undefined8 uParm2,undefined8 uParm3);
void FUN_0040cc64(ulong uParm1,undefined8 uParm2);
void FUN_0040cc7c(undefined8 uParm1,undefined8 uParm2,char cParm3);
void FUN_0040cd05(undefined8 uParm1,char cParm2);
void FUN_0040cd1e(undefined8 uParm1);
void FUN_0040cd31(uint uParm1,undefined8 uParm2,undefined8 uParm3);
void FUN_0040cdc0(void);
void FUN_0040cdd3(undefined8 uParm1,undefined8 uParm2);
void FUN_0040cde8(undefined8 uParm1);
ulong FUN_0040cdfe(uint uParm1,long lParm2,uint uParm3,long lParm4,uint uParm5);
undefined8 * FUN_0040d19f(undefined8 *puParm1);
long FUN_0040d1de(uint uParm1,undefined8 uParm2,ulong uParm3);
ulong FUN_0040d23f(undefined8 uParm1,undefined8 uParm2);
void FUN_0040d366(undefined8 *puParm1,undefined8 *puParm2);
long FUN_0040d37a(long lParm1,uint uParm2);
undefined8 FUN_0040d65f(undefined8 uParm1,ulong uParm2);
ulong FUN_0040d6bc(long lParm1,int iParm2,undefined8 uParm3,code *pcParm4,ulong uParm5);
ulong FUN_0040d80b(uint uParm1);
ulong FUN_0040d83d(ulong *puParm1,ulong uParm2);
ulong FUN_0040d84c(ulong *puParm1,ulong *puParm2);
ulong FUN_0040d856(undefined8 uParm1,undefined8 *puParm2,long lParm3,ulong uParm4);
ulong FUN_0040df10(undefined8 *puParm1);
undefined8 FUN_0040dfb4(long lParm1,long *plParm2);
ulong FUN_0040e05f(uint uParm1,long lParm2,undefined8 *puParm3);
void FUN_0040e40c(undefined8 uParm1,undefined8 uParm2);
ulong FUN_0040e425(undefined8 uParm1,undefined8 *puParm2);
void FUN_0040e60b(undefined8 uParm1,long lParm2,undefined8 uParm3,undefined8 uParm4,undefined8 *puParm5,undefined8 uParm6);
void FUN_0040e848(void);
void FUN_0040e89a(void);
ulong FUN_0040e91f(void);
void FUN_0040e955(long lParm1);
long FUN_0040e96f(long lParm1,long lParm2);
void FUN_0040e9a2(undefined8 uParm1,undefined8 uParm2);
void FUN_0040e9cb(undefined8 uParm1);
void FUN_0040e9e2(void);
void FUN_0040ea0a(undefined8 uParm1,uint uParm2);
long FUN_0040ea4b(void);
ulong FUN_0040ea77(void);
undefined8 FUN_0040eaa5(ulong uParm1,ulong uParm2,ulong uParm3);
void FUN_0040eaed(void);
void FUN_0040eb1a(undefined8 uParm1);
void FUN_0040eb5a(uint uParm1,uint uParm2,undefined8 uParm3,undefined8 uParm4);
void FUN_0040ebb1(undefined4 uParm1,undefined4 uParm2,undefined4 uParm3,undefined4 uParm4,undefined4 uParm5,undefined4 uParm6,undefined4 uParm7,undefined4 uParm8,ulong param_1,ulong param_2,undefined8 param_3,undefined8 param_4,undefined8 param_5,undefined8 param_6);
ulong FUN_0040ec84(ulong uParm1,long lParm2);
ulong FUN_0040ecfa(uint param_1,undefined8 param_2,undefined8 param_3,undefined8 param_4);
undefined8 FUN_0040ee69(ulong uParm1);
ulong FUN_0040eeb8(long lParm1);
undefined8 FUN_0040ef48(void);
void FUN_0040ef5b(void);
undefined8 FUN_0040ef69(ulong uParm1,long lParm2,long lParm3,ulong uParm4);
ulong FUN_0040f035(long *plParm1,long *plParm2);
ulong FUN_0040f054(long lParm1,ulong uParm2);
ulong FUN_0040f064(ulong *puParm1,ulong uParm2);
ulong FUN_0040f073(ulong *puParm1,ulong *puParm2);
ulong FUN_0040f07d(long *plParm1,long *plParm2);
undefined8 FUN_0040f0a3(long lParm1,long lParm2);
ulong FUN_0040f110(long lParm1,long lParm2,char cParm3);
long FUN_0040f268(long lParm1,long lParm2,ulong uParm3);
long FUN_0040f348(long lParm1,undefined8 uParm2,long lParm3);
ulong FUN_0040f3da(long lParm1);
void FUN_0040f439(long lParm1,undefined8 uParm2);
void FUN_0040f498(long lParm1);
void FUN_0040f4cf(long lParm1);
void FUN_0040f4fa(undefined8 uParm1);
undefined8 FUN_0040f521(long lParm1);
undefined8 FUN_0040f62c(void);
undefined8 FUN_0040f686(long lParm1,undefined8 *puParm2);
void FUN_0040f730(long lParm1,uint uParm2,char cParm3);
ulong FUN_0040f781(long lParm1);
ulong FUN_0040f7d1(long lParm1,long lParm2,ulong uParm3,long lParm4);
void FUN_0040f952(long lParm1,long lParm2);
long FUN_0040f9e4(long *plParm1,int iParm2);
long * FUN_004102ad(long *plParm1,uint uParm2,long lParm3);
undefined8 FUN_0041062c(long *plParm1);
long FUN_00410766(long *plParm1);
undefined8 FUN_00410dd2(undefined8 uParm1,long lParm2,uint uParm3);
long FUN_00410dfc(long lParm1,ulong uParm2);
void FUN_0041128c(long lParm1,int *piParm2);
ulong FUN_0041134b(int param_1,undefined8 *param_2,char *param_3,long *param_4,int *param_5,int param_6,int *param_7,int param_8,undefined8 param_9);
ulong FUN_00411806(uint param_1,undefined8 *param_2,char *param_3,long param_4,undefined8 param_5,ulong param_6,uint *param_7,int param_8);
void FUN_00411c82(void);
void FUN_00411cd8(void);
ulong FUN_00411cee(long lParm1,uint uParm2,uint uParm3);
ulong FUN_00411dd0(ulong uParm1,char *pcParm2,uint uParm3,long lParm4,uint uParm5);
ulong FUN_00412050(long lParm1,long lParm2);
void FUN_004120b9(long lParm1);
ulong FUN_004120d3(uint *puParm1,byte *pbParm2,long lParm3);
ulong FUN_00412138(long lParm1,ulong uParm2);
undefined8 FUN_0041221d(long lParm1,ulong uParm2);
undefined8 FUN_00412271(long lParm1,uint uParm2,long lParm3);
ulong FUN_00412306(uint param_1,undefined8 param_2,uint param_3,uint param_4);
undefined8 FUN_004123f6(long lParm1,undefined8 uParm2,undefined8 uParm3);
void FUN_00412458(long lParm1,long lParm2);
ulong FUN_00412486(long lParm1,long lParm2);
void FUN_00412860(void);
undefined8 FUN_00412874(long lParm1);
ulong FUN_00412904(long lParm1,long lParm2);
undefined8 FUN_00412950(undefined8 uParm1,ulong uParm2,long lParm3);
undefined8 FUN_004129b8(long lParm1);
undefined8 FUN_00412aad(ulong uParm1,long lParm2,ulong uParm3);
ulong FUN_00412bd4(undefined8 uParm1,undefined8 uParm2,undefined8 uParm3);
undefined8 FUN_00412ca1(undefined8 uParm1,undefined8 uParm2,undefined4 uParm3,undefined4 *puParm4);
void FUN_00412ccc(undefined8 uParm1,uint uParm2,uint uParm3);
ulong FUN_00412cee(uint *puParm1,undefined8 uParm2,uint uParm3);
void FUN_00412d10(undefined8 uParm1,undefined8 uParm2);
void FUN_00412d34(void);
long FUN_00412d47(undefined8 uParm1,long *plParm2,long lParm3,long lParm4);
void FUN_00412e37(undefined8 uParm1,undefined8 uParm2,long lParm3);
void FUN_00412e94(long *plParm1,long lParm2,long lParm3);
long FUN_00412f5f(undefined8 uParm1,undefined8 uParm2,undefined8 uParm3,undefined8 uParm4,undefined8 uParm5,code *pcParm6);
undefined *FUN_00412fca(uint uParm1,undefined8 uParm2,undefined *puParm3,ulong uParm4,undefined1 *puParm5,code *pcParm6);
ulong FUN_00413174(ulong uParm1,char cParm2);
ulong FUN_004131d4(undefined8 uParm1);
void FUN_00413235(long lParm1);
undefined8 FUN_00413245(long *plParm1,long *plParm2);
undefined8 FUN_004132d9(void);
void FUN_004132e1(undefined8 *puParm1);
ulong FUN_00413321(ulong uParm1);
ulong FUN_004133af(char *pcParm1,ulong uParm2);
void FUN_004133e4(undefined4 *puParm1,undefined4 uParm2);
ulong FUN_00413405(long lParm1);
ulong FUN_0041340a(long lParm1,uint uParm2);
ulong FUN_0041343f(long lParm1);
char * FUN_00413472(void);
void FUN_004137ad(void);
void FUN_004137f6(undefined8 uParm1);
undefined8 FUN_00413819(void);
ulong FUN_0041383b(undefined8 *puParm1,ulong uParm2);
void FUN_0041392c(undefined8 uParm1);
ulong FUN_00413944(undefined8 *puParm1);
long * FUN_00413974(ulong uParm1,ulong uParm2);
long * FUN_004139bf(long lParm1,ulong uParm2);
void FUN_00413c5a(long *plParm1,ulong uParm2,ulong uParm3);
undefined8 FUN_00413da6(long *plParm1);
void FUN_00413ddd(long *plParm1,long *plParm2);
void FUN_00414040(ulong *puParm1);
void FUN_00414271(undefined8 uParm1);
ulong FUN_0041428e(long lParm1,uint uParm2,uint uParm3);
void FUN_004143e5(undefined8 uParm1,undefined8 uParm2);
undefined8 FUN_004143fb(undefined8 uParm1);
void FUN_0041447e(void);
undefined8 FUN_0041452a(uint uParm1,long lParm2,undefined8 uParm3,undefined8 uParm4);
ulong FUN_00414597(char *pcParm1,long lParm2);
long FUN_004145de(ulong uParm1,uint *puParm2,ulong uParm3,uint *puParm4,long *plParm5);
ulong FUN_00414702(void);
ulong FUN_00414734(void);
int * FUN_00414941(ulong uParm1,long lParm2,ulong uParm3,uint *puParm4,ulong *puParm5);
char * FUN_00414f24(int iParm1,long lParm2,long lParm3,long lParm4,uint uParm5);
void FUN_0041553c(uint param_1,undefined8 param_2);
ulong FUN_00415702(void);
void FUN_00415915(undefined8 uParm1,uint uParm2);
undefined8 *FUN_00415ab6(undefined8 *puParm1,undefined8 *puParm2,undefined8 *puParm3,undefined8 uParm4);
long FUN_0041b653(undefined8 uParm1,undefined8 uParm2);
double FUN_0041b6e3(int *piParm1);
void FUN_0041b72b(int *param_1);
undefined8 FUN_0041b7ac(uint *puParm1,ulong *puParm2);
undefined8 FUN_0041bbd1(byte *pbParm1,ulong *puParm2,byte **ppbParm3);
ulong FUN_0041c736(void);
ulong FUN_0041c75e(void);
void FUN_0041c790(void);
undefined8 _DT_FINI(void);
undefined FUN_00625000();
undefined FUN_00625008();
undefined FUN_00625010();
undefined FUN_00625018();
undefined FUN_00625020();
undefined FUN_00625028();
undefined FUN_00625030();
undefined FUN_00625038();
undefined FUN_00625040();
undefined FUN_00625048();
undefined FUN_00625050();
undefined FUN_00625058();
undefined FUN_00625060();
undefined FUN_00625068();
undefined FUN_00625070();
undefined FUN_00625078();
undefined FUN_00625080();
undefined FUN_00625088();
undefined FUN_00625090();
undefined FUN_00625098();
undefined FUN_006250a0();
undefined FUN_006250a8();
undefined FUN_006250b0();
undefined FUN_006250b8();
undefined FUN_006250c0();
undefined FUN_006250c8();
undefined FUN_006250d0();
undefined FUN_006250d8();
undefined FUN_006250e0();
undefined FUN_006250e8();
undefined FUN_006250f0();
undefined FUN_006250f8();
undefined FUN_00625100();
undefined FUN_00625108();
undefined FUN_00625110();
undefined FUN_00625118();
undefined FUN_00625120();
undefined FUN_00625128();
undefined FUN_00625130();
undefined FUN_00625138();
undefined FUN_00625140();
undefined FUN_00625148();
undefined FUN_00625150();
undefined FUN_00625158();
undefined FUN_00625160();
undefined FUN_00625168();
undefined FUN_00625170();
undefined FUN_00625178();
undefined FUN_00625180();
undefined FUN_00625188();
undefined FUN_00625190();
undefined FUN_00625198();
undefined FUN_006251a0();
undefined FUN_006251a8();
undefined FUN_006251b0();
undefined FUN_006251b8();
undefined FUN_006251c0();
undefined FUN_006251c8();
undefined FUN_006251d0();
undefined FUN_006251d8();
undefined FUN_006251e0();
undefined FUN_006251e8();
undefined FUN_006251f0();
undefined FUN_006251f8();
undefined FUN_00625200();
undefined FUN_00625208();
undefined FUN_00625210();
undefined FUN_00625218();
undefined FUN_00625220();
undefined FUN_00625228();
undefined FUN_00625230();
undefined FUN_00625238();
undefined FUN_00625240();
undefined FUN_00625248();
undefined FUN_00625250();
undefined FUN_00625258();
undefined FUN_00625260();
undefined FUN_00625268();
undefined FUN_00625270();
undefined FUN_00625278();
undefined FUN_00625280();
undefined FUN_00625288();
undefined FUN_00625290();
undefined FUN_00625298();
undefined FUN_006252a0();
undefined FUN_006252a8();
undefined FUN_006252b0();
undefined FUN_006252b8();
undefined FUN_006252c0();
undefined FUN_006252c8();
undefined FUN_006252d0();
undefined FUN_006252d8();
undefined FUN_006252e0();
undefined FUN_006252e8();
undefined FUN_006252f0();
undefined FUN_006252f8();
undefined FUN_00625300();
undefined FUN_00625308();
undefined FUN_00625310();
undefined FUN_00625318();
undefined FUN_00625320();
undefined FUN_00625328();
undefined FUN_00625330();
undefined FUN_00625338();
undefined FUN_00625340();
undefined FUN_00625348();
undefined FUN_00625350();
undefined FUN_00625358();
undefined FUN_00625360();
undefined FUN_00625368();
undefined FUN_00625370();
undefined FUN_00625378();
undefined FUN_00625380();
undefined FUN_00625388();
undefined FUN_00625390();
undefined FUN_00625398();
undefined FUN_006253a0();
undefined FUN_006253a8();
undefined FUN_006253b0();
undefined FUN_006253b8();
undefined FUN_006253c0();
undefined FUN_006253c8();
undefined FUN_006253d0();
undefined FUN_006253d8();
undefined FUN_006253e0();
undefined FUN_006253e8();
undefined FUN_006253f0();
undefined FUN_006253f8();
undefined FUN_00625400();
undefined FUN_00625408();


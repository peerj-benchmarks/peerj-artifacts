
#include "mv.h"

long null_ARRAY_0062464_0_8_;
long null_ARRAY_0062464_8_8_;
long null_ARRAY_00624cc_0_8_;
long null_ARRAY_00624cc_16_8_;
long null_ARRAY_00624cc_24_8_;
long null_ARRAY_00624cc_32_8_;
long null_ARRAY_00624cc_40_8_;
long null_ARRAY_00624cc_48_8_;
long null_ARRAY_00624cc_8_8_;
long null_ARRAY_00624d2_0_4_;
long null_ARRAY_00624d2_16_8_;
long null_ARRAY_00624d2_4_4_;
long null_ARRAY_00624d2_8_4_;
long local_1d_4_4_;
long local_1e_0_4_;
long local_2d_1_7_;
long local_2d_4_4_;
long local_2e_0_1_;
long local_2e_0_4_;
long local_2e_1_7_;
long local_2e_4_4_;
long local_2f_0_4_;
long local_2f_1_7_;
long local_30_1_7_;
long local_30_4_4_;
long local_31_4_4_;
long local_32_4_4_;
long local_33_4_4_;
long local_68_2_2_;
long local_6b_1_3_;
long local_6c_4_4_;
long local_6e_4_4_;
long local_6f_4_4_;
long local_70_0_2_;
long local_70_4_4_;
long local_71_0_1_;
long local_71_4_4_;
long local_72_4_4_;
long DAT_00000010;
long DAT_0041c81f;
long DAT_0041c822;
long DAT_0041c8a6;
long DAT_0041d388;
long DAT_0041d5d6;
long DAT_0041d5d7;
long DAT_0041e374;
long DAT_0041e4b2;
long DAT_0041e548;
long DAT_0041e54e;
long DAT_0041e550;
long DAT_0041e554;
long DAT_0041e558;
long DAT_0041e55b;
long DAT_0041e55d;
long DAT_0041e561;
long DAT_0041e565;
long DAT_0041ed72;
long DAT_0041f11a;
long DAT_0041f2f1;
long DAT_0041f2f7;
long DAT_0041f309;
long DAT_0041f30a;
long DAT_0041f328;
long DAT_0041f381;
long DAT_0041f383;
long DAT_0041f3bc;
long DAT_0041f45f;
long DAT_0041f475;
long DAT_00624000;
long DAT_00624010;
long DAT_00624020;
long DAT_006245e0;
long DAT_006245e8;
long DAT_006245f8;
long DAT_00624650;
long DAT_00624654;
long DAT_00624658;
long DAT_0062465c;
long DAT_00624680;
long DAT_00624688;
long DAT_00624690;
long DAT_006246a0;
long DAT_006246a8;
long DAT_006246c0;
long DAT_006246c8;
long DAT_00624720;
long DAT_00624b40;
long DAT_00624b48;
long DAT_00624b50;
long DAT_00624b58;
long DAT_00624b60;
long DAT_00624b68;
long DAT_00624b70;
long DAT_00624b78;
long DAT_00624b80;
long DAT_00624cf8;
long DAT_00624d00;
long DAT_00624d08;
long DAT_00624d0c;
long DAT_00624d10;
long DAT_00624d11;
long DAT_00624d14;
long DAT_00624d58;
long DAT_00624d5c;
long DAT_00624d60;
long DAT_00624db8;
long DAT_00624dc0;
long DAT_00624dd0;
long fde_004205f8;
long int7;
long null_ARRAY_0041d3c0;
long null_ARRAY_0041e351;
long null_ARRAY_0041e3c0;
long null_ARRAY_0041e400;
long null_ARRAY_0041e4e0;
long null_ARRAY_0041ece0;
long null_ARRAY_0041f880;
long null_ARRAY_0041f9e0;
long null_ARRAY_0041fad0;
long null_ARRAY_00624640;
long null_ARRAY_006246e0;
long null_ARRAY_00624710;
long null_ARRAY_00624740;
long null_ARRAY_00624bc0;
long null_ARRAY_00624cc0;
long null_ARRAY_00624d20;
long PTR_DAT_006245f0;
long PTR_FUN_00624660;
long PTR_null_ARRAY_00624638;
long PTR_null_ARRAY_00624668;
void
FUN_00403083 (uint uParm1)
{
  int iVar1;
  long lVar2;
  undefined8 uVar3;
  undefined **ppuVar4;
  undefined *puVar5;
  undefined *local_88;
  char *local_80;
  undefined8 local_78;
  char *local_70;
  char *local_68;
  char *local_60;
  char *local_58;
  char *local_50;
  char *local_48;
  char *local_40;
  char *local_38;
  char *local_30;
  undefined8 local_28;
  undefined8 local_20;

  ppuVar4 = &local_88;
  if (uParm1 != 0)
    {
      func_0x00402910 (DAT_006246a0,
		       "Try \'%s --help\' for more information.\n",
		       DAT_00624b80);
      goto LAB_00403295;
    }
  func_0x00402580
    ("Usage: %s [OPTION]... [-T] SOURCE DEST\n  or:  %s [OPTION]... SOURCE... DIRECTORY\n  or:  %s [OPTION]... -t DIRECTORY SOURCE...\n",
     DAT_00624b80, DAT_00624b80, DAT_00624b80);
  uVar3 = DAT_00624680;
  func_0x00402920 ("Rename SOURCE to DEST, or move SOURCE(s) to DIRECTORY.\n",
		   DAT_00624680);
  func_0x00402920
    ("\nMandatory arguments to long options are mandatory for short options too.\n",
     uVar3);
  func_0x00402920
    ("      --backup[=CONTROL]       make a backup of each existing destination file\n  -b                           like --backup but does not accept an argument\n  -f, --force                  do not prompt before overwriting\n  -i, --interactive            prompt before overwrite\n  -n, --no-clobber             do not overwrite an existing file\nIf you specify more than one of -i, -f, -n, only the final one takes effect.\n",
     uVar3);
  func_0x00402920
    ("      --strip-trailing-slashes  remove any trailing slashes from each SOURCE\n                                 argument\n  -S, --suffix=SUFFIX          override the usual backup suffix\n",
     uVar3);
  func_0x00402920
    ("  -t, --target-directory=DIRECTORY  move all SOURCE arguments into DIRECTORY\n  -T, --no-target-directory    treat DEST as a normal file\n  -u, --update                 move only when the SOURCE file is newer\n                                 than the destination file or when the\n                                 destination file is missing\n  -v, --verbose                explain what is being done\n  -Z, --context                set SELinux security context of destination\n                                 file to default type\n",
     uVar3);
  func_0x00402920 ("      --help     display this help and exit\n", uVar3);
  func_0x00402920 ("      --version  output version information and exit\n",
		   uVar3);
  func_0x00402920
    ("\nThe backup suffix is \'~\', unless set with --suffix or SIMPLE_BACKUP_SUFFIX.\nThe version control method may be selected via the --backup option or through\nthe VERSION_CONTROL environment variable.  Here are the values:\n\n",
     uVar3);
  func_0x00402920
    ("  none, off       never make backups (even if --backup is given)\n  numbered, t     make numbered backups\n  existing, nil   numbered if numbered backups exist, simple otherwise\n  simple, never   always make simple backups\n",
     uVar3);
  local_88 = &DAT_0041c822;
  local_80 = "test invocation";
  local_78 = 0x41c885;
  local_70 = "Multi-call invocation";
  local_68 = "sha224sum";
  local_60 = "sha2 utilities";
  local_58 = "sha256sum";
  local_50 = "sha2 utilities";
  local_48 = "sha384sum";
  local_40 = "sha2 utilities";
  local_38 = "sha512sum";
  local_30 = "sha2 utilities";
  local_28 = 0;
  local_20 = 0;
  puVar5 = &DAT_0041c822;
  do
    {
      iVar1 = func_0x00402b10 (&DAT_0041c81f, puVar5);
      if (iVar1 == 0)
	break;
      ppuVar4 = ppuVar4 + 2;
      puVar5 = *ppuVar4;
    }
  while (puVar5 != (undefined *) 0x0);
  puVar5 = ppuVar4[1];
  if (puVar5 == (undefined *) 0x0)
    {
      func_0x00402580 ("\n%s online help: <%s>\n", "GNU coreutils",
		       "https://www.gnu.org/software/coreutils/");
      lVar2 = func_0x00402b90 (5, 0);
      if (lVar2 == 0)
	goto LAB_0040329c;
      iVar1 = func_0x004029e0 (lVar2, &DAT_0041c8a6, 3);
      if (iVar1 == 0)
	{
	  func_0x00402580 ("Full documentation at: <%s%s>\n",
			   "https://www.gnu.org/software/coreutils/",
			   &DAT_0041c81f);
	  puVar5 = &DAT_0041c81f;
	  uVar3 = 0x41c83e;
	  goto LAB_00403283;
	}
      puVar5 = &DAT_0041c81f;
    LAB_00403241:
      ;
      func_0x00402580
	("Report %s translation bugs to <https://translationproject.org/team/>\n",
	 &DAT_0041c81f);
    }
  else
    {
      func_0x00402580 ("\n%s online help: <%s>\n", "GNU coreutils",
		       "https://www.gnu.org/software/coreutils/");
      lVar2 = func_0x00402b90 (5, 0);
      if ((lVar2 != 0)
	  && (iVar1 = func_0x004029e0 (lVar2, &DAT_0041c8a6, 3), iVar1 != 0))
	goto LAB_00403241;
    }
  func_0x00402580 ("Full documentation at: <%s%s>\n",
		   "https://www.gnu.org/software/coreutils/", &DAT_0041c81f);
  uVar3 = 0x41f327;
  if (puVar5 == &DAT_0041c81f)
    {
      uVar3 = 0x41c83e;
    }
LAB_00403283:
  ;
  do
    {
      func_0x00402580
	("or available locally via: info \'(coreutils) %s%s\'\n", puVar5,
	 uVar3);
    LAB_00403295:
      ;
      func_0x00402c10 ((ulong) uParm1);
    LAB_0040329c:
      ;
      func_0x00402580 ("Full documentation at: <%s%s>\n",
		       "https://www.gnu.org/software/coreutils/",
		       &DAT_0041c81f);
      puVar5 = &DAT_0041c81f;
      uVar3 = 0x41c83e;
    }
  while (true);
}

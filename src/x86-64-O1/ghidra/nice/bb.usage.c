
#include "nice.h"

long null_ARRAY_0061044_0_8_;
long null_ARRAY_0061044_8_8_;
long null_ARRAY_0061064_0_8_;
long null_ARRAY_0061064_16_8_;
long null_ARRAY_0061064_24_8_;
long null_ARRAY_0061064_32_8_;
long null_ARRAY_0061064_40_8_;
long null_ARRAY_0061064_48_8_;
long null_ARRAY_0061064_8_8_;
long null_ARRAY_0061068_0_4_;
long null_ARRAY_0061068_16_8_;
long null_ARRAY_0061068_4_4_;
long null_ARRAY_0061068_8_4_;
long local_68_2_2_;
long local_6b_1_3_;
long local_6c_4_4_;
long local_6e_4_4_;
long local_6f_4_4_;
long local_70_0_2_;
long local_70_4_4_;
long local_71_0_1_;
long local_71_4_4_;
long local_72_4_4_;
long DAT_00000010;
long DAT_0040d000;
long DAT_0040d005;
long DAT_0040d089;
long DAT_0040d08d;
long DAT_0040d0cb;
long DAT_0040d5d8;
long DAT_0040d5dc;
long DAT_0040d5e0;
long DAT_0040d5e3;
long DAT_0040d5e5;
long DAT_0040d5e9;
long DAT_0040d5ed;
long DAT_0040dd6b;
long DAT_0040e485;
long DAT_0040e589;
long DAT_0040e58f;
long DAT_0040e5a1;
long DAT_0040e5a2;
long DAT_0040e5c0;
long DAT_0040e5c4;
long DAT_0040e64e;
long DAT_00610000;
long DAT_00610010;
long DAT_00610020;
long DAT_006103e8;
long DAT_00610450;
long DAT_00610454;
long DAT_00610458;
long DAT_0061045c;
long DAT_00610480;
long DAT_00610490;
long DAT_006104a0;
long DAT_006104a8;
long DAT_006104c0;
long DAT_006104c8;
long DAT_00610510;
long DAT_00610518;
long DAT_00610520;
long DAT_006106b8;
long DAT_006106c0;
long DAT_006106c8;
long DAT_006106d8;
long fde_0040f190;
long null_ARRAY_0040d500;
long null_ARRAY_0040ea60;
long null_ARRAY_0040eca0;
long null_ARRAY_00610440;
long null_ARRAY_006104e0;
long null_ARRAY_00610540;
long null_ARRAY_00610640;
long null_ARRAY_00610680;
long PTR_DAT_006103e0;
long PTR_null_ARRAY_00610438;
void
FUN_00401b1e (uint uParm1)
{
  int iVar1;
  long lVar2;
  undefined8 uVar3;
  undefined **ppuVar4;
  undefined *puVar5;
  undefined *local_88;
  char *local_80;
  undefined8 local_78;
  char *local_70;
  char *local_68;
  char *local_60;
  char *local_58;
  char *local_50;
  char *local_48;
  char *local_40;
  char *local_38;
  char *local_30;
  undefined8 local_28;
  undefined8 local_20;

  ppuVar4 = &local_88;
  if (uParm1 != 0)
    {
      func_0x004016f0 (DAT_006104a0,
		       "Try \'%s --help\' for more information.\n",
		       DAT_00610520);
      goto LAB_00401d16;
    }
  func_0x00401550 ("Usage: %s [OPTION] [COMMAND [ARG]...]\n", DAT_00610520);
  func_0x00401550
    ("Run COMMAND with an adjusted niceness, which affects process scheduling.\nWith no COMMAND, print the current niceness.  Niceness values range from\n%d (most favorable to the process) to %d (least favorable to the process).\n",
     0xffffffec, 0x13);
  uVar3 = DAT_00610480;
  func_0x00401700
    ("\nMandatory arguments to long options are mandatory for short options too.\n",
     DAT_00610480);
  func_0x00401700
    ("  -n, --adjustment=N   add integer N to the niceness (default 10)\n",
     uVar3);
  func_0x00401700 ("      --help     display this help and exit\n", uVar3);
  func_0x00401700 ("      --version  output version information and exit\n",
		   uVar3);
  func_0x00401550
    ("\nNOTE: your shell may have its own version of %s, which usually supersedes\nthe version described here.  Please refer to your shell\'s documentation\nfor details about the options it supports.\n",
     &DAT_0040d000);
  local_88 = &DAT_0040d005;
  local_80 = "test invocation";
  local_78 = 0x40d068;
  local_70 = "Multi-call invocation";
  local_68 = "sha224sum";
  local_60 = "sha2 utilities";
  local_58 = "sha256sum";
  local_50 = "sha2 utilities";
  local_48 = "sha384sum";
  local_40 = "sha2 utilities";
  local_38 = "sha512sum";
  local_30 = "sha2 utilities";
  local_28 = 0;
  local_20 = 0;
  puVar5 = &DAT_0040d005;
  do
    {
      iVar1 = func_0x004017e0 (&DAT_0040d000, puVar5);
      if (iVar1 == 0)
	break;
      ppuVar4 = ppuVar4 + 2;
      puVar5 = *ppuVar4;
    }
  while (puVar5 != (undefined *) 0x0);
  puVar5 = ppuVar4[1];
  if (puVar5 == (undefined *) 0x0)
    {
      func_0x00401550 ("\n%s online help: <%s>\n", "GNU coreutils",
		       "https://www.gnu.org/software/coreutils/");
      lVar2 = func_0x00401840 (5, 0);
      if (lVar2 == 0)
	goto LAB_00401d1d;
      iVar1 = func_0x00401750 (lVar2, &DAT_0040d089, 3);
      if (iVar1 == 0)
	{
	  func_0x00401550 ("Full documentation at: <%s%s>\n",
			   "https://www.gnu.org/software/coreutils/",
			   &DAT_0040d000);
	  puVar5 = &DAT_0040d000;
	  uVar3 = 0x40d021;
	  goto LAB_00401d04;
	}
      puVar5 = &DAT_0040d000;
    LAB_00401cc2:
      ;
      func_0x00401550
	("Report %s translation bugs to <https://translationproject.org/team/>\n",
	 &DAT_0040d000);
    }
  else
    {
      func_0x00401550 ("\n%s online help: <%s>\n", "GNU coreutils",
		       "https://www.gnu.org/software/coreutils/");
      lVar2 = func_0x00401840 (5, 0);
      if ((lVar2 != 0)
	  && (iVar1 = func_0x00401750 (lVar2, &DAT_0040d089, 3), iVar1 != 0))
	goto LAB_00401cc2;
    }
  func_0x00401550 ("Full documentation at: <%s%s>\n",
		   "https://www.gnu.org/software/coreutils/", &DAT_0040d000);
  uVar3 = 0x40e5bf;
  if (puVar5 == &DAT_0040d000)
    {
      uVar3 = 0x40d021;
    }
LAB_00401d04:
  ;
  do
    {
      func_0x00401550
	("or available locally via: info \'(coreutils) %s%s\'\n", puVar5,
	 uVar3);
    LAB_00401d16:
      ;
      func_0x00401880 ((ulong) uParm1);
    LAB_00401d1d:
      ;
      func_0x00401550 ("Full documentation at: <%s%s>\n",
		       "https://www.gnu.org/software/coreutils/",
		       &DAT_0040d000);
      puVar5 = &DAT_0040d000;
      uVar3 = 0x40d021;
    }
  while (true);
}


#include "kill.h"

long null_ARRAY_0061046_0_8_;
long null_ARRAY_0061046_8_8_;
long null_ARRAY_0061080_0_8_;
long null_ARRAY_0061080_16_8_;
long null_ARRAY_0061080_24_8_;
long null_ARRAY_0061080_32_8_;
long null_ARRAY_0061080_40_8_;
long null_ARRAY_0061080_48_8_;
long null_ARRAY_0061080_8_8_;
long null_ARRAY_0061084_0_4_;
long null_ARRAY_0061084_16_8_;
long null_ARRAY_0061084_4_4_;
long null_ARRAY_0061084_8_4_;
long local_68_2_2_;
long local_6b_1_3_;
long local_6c_4_4_;
long local_6e_4_4_;
long local_6f_4_4_;
long local_70_0_2_;
long local_70_4_4_;
long local_71_0_1_;
long local_71_4_4_;
long local_72_4_4_;
long DAT_00000010;
long DAT_0040d200;
long DAT_0040d205;
long DAT_0040d289;
long DAT_0040d28d;
long DAT_0040d2d5;
long DAT_0040d9f8;
long DAT_0040d9fc;
long DAT_0040da00;
long DAT_0040da03;
long DAT_0040da05;
long DAT_0040da09;
long DAT_0040da0d;
long DAT_0040e1ab;
long DAT_0040e555;
long DAT_0040e659;
long DAT_0040e65f;
long DAT_0040e671;
long DAT_0040e672;
long DAT_0040e690;
long DAT_0040e6a0;
long DAT_0040e6a4;
long DAT_00610000;
long DAT_00610010;
long DAT_00610020;
long DAT_00610408;
long DAT_00610470;
long DAT_00610474;
long DAT_00610478;
long DAT_0061047c;
long _DAT_00610480;
long DAT_00610480;
long DAT_00610484;
long DAT_0061048c;
long DAT_00610640;
long DAT_00610650;
long DAT_00610660;
long DAT_00610668;
long DAT_00610680;
long DAT_00610688;
long DAT_006106d0;
long DAT_006106d8;
long DAT_006106e0;
long DAT_00610878;
long DAT_00610880;
long DAT_00610888;
long DAT_00610898;
long fde_0040f288;
long null_ARRAY_0040d840;
long null_ARRAY_0040eb40;
long null_ARRAY_0040ed80;
long null_ARRAY_00610460;
long null_ARRAY_006106a0;
long null_ARRAY_00610700;
long null_ARRAY_00610800;
long null_ARRAY_00610840;
long PTR_DAT_00610400;
long PTR_null_ARRAY_00610458;
void
FUN_00401bbe (uint uParm1)
{
  int iVar1;
  long lVar2;
  undefined8 uVar3;
  undefined **ppuVar4;
  undefined *puVar5;
  undefined *local_88;
  char *local_80;
  undefined8 local_78;
  char *local_70;
  char *local_68;
  char *local_60;
  char *local_58;
  char *local_50;
  char *local_48;
  char *local_40;
  char *local_38;
  char *local_30;
  undefined8 local_28;
  undefined8 local_20;

  ppuVar4 = &local_88;
  if (uParm1 != 0)
    {
      func_0x00401790 (DAT_00610660,
		       "Try \'%s --help\' for more information.\n",
		       DAT_006106e0);
      goto LAB_00401dbd;
    }
  func_0x004015d0
    ("Usage: %s [-s SIGNAL | -SIGNAL] PID...\n  or:  %s -l [SIGNAL]...\n  or:  %s -t [SIGNAL]...\n",
     DAT_006106e0, DAT_006106e0, DAT_006106e0);
  uVar3 = DAT_00610640;
  func_0x004017b0 ("Send signals to processes, or list signals.\n",
		   DAT_00610640);
  func_0x004017b0
    ("\nMandatory arguments to long options are mandatory for short options too.\n",
     uVar3);
  func_0x004017b0
    ("  -s, --signal=SIGNAL, -SIGNAL\n                   specify the name or number of the signal to be sent\n  -l, --list       list signal names, or convert signal names to/from numbers\n  -t, --table      print a table of signal information\n",
     uVar3);
  func_0x004017b0 ("      --help     display this help and exit\n", uVar3);
  func_0x004017b0 ("      --version  output version information and exit\n",
		   uVar3);
  func_0x004017b0
    ("\nSIGNAL may be a signal name like \'HUP\', or a signal number like \'1\',\nor the exit status of a process terminated by a signal.\nPID is an integer; if negative it identifies a process group.\n",
     uVar3);
  func_0x004015d0
    ("\nNOTE: your shell may have its own version of %s, which usually supersedes\nthe version described here.  Please refer to your shell\'s documentation\nfor details about the options it supports.\n",
     &DAT_0040d200);
  local_88 = &DAT_0040d205;
  local_80 = "test invocation";
  local_78 = 0x40d268;
  local_70 = "Multi-call invocation";
  local_68 = "sha224sum";
  local_60 = "sha2 utilities";
  local_58 = "sha256sum";
  local_50 = "sha2 utilities";
  local_48 = "sha384sum";
  local_40 = "sha2 utilities";
  local_38 = "sha512sum";
  local_30 = "sha2 utilities";
  local_28 = 0;
  local_20 = 0;
  puVar5 = &DAT_0040d205;
  do
    {
      iVar1 = func_0x00401880 (&DAT_0040d200, puVar5);
      if (iVar1 == 0)
	break;
      ppuVar4 = ppuVar4 + 2;
      puVar5 = *ppuVar4;
    }
  while (puVar5 != (undefined *) 0x0);
  puVar5 = ppuVar4[1];
  if (puVar5 == (undefined *) 0x0)
    {
      func_0x004015d0 ("\n%s online help: <%s>\n", "GNU coreutils",
		       "https://www.gnu.org/software/coreutils/");
      lVar2 = func_0x004018f0 (5, 0);
      if (lVar2 == 0)
	goto LAB_00401dc4;
      iVar1 = func_0x00401800 (lVar2, &DAT_0040d289, 3);
      if (iVar1 == 0)
	{
	  func_0x004015d0 ("Full documentation at: <%s%s>\n",
			   "https://www.gnu.org/software/coreutils/",
			   &DAT_0040d200);
	  puVar5 = &DAT_0040d200;
	  uVar3 = 0x40d221;
	  goto LAB_00401dab;
	}
      puVar5 = &DAT_0040d200;
    LAB_00401d69:
      ;
      func_0x004015d0
	("Report %s translation bugs to <https://translationproject.org/team/>\n",
	 &DAT_0040d200);
    }
  else
    {
      func_0x004015d0 ("\n%s online help: <%s>\n", "GNU coreutils",
		       "https://www.gnu.org/software/coreutils/");
      lVar2 = func_0x004018f0 (5, 0);
      if ((lVar2 != 0)
	  && (iVar1 = func_0x00401800 (lVar2, &DAT_0040d289, 3), iVar1 != 0))
	goto LAB_00401d69;
    }
  func_0x004015d0 ("Full documentation at: <%s%s>\n",
		   "https://www.gnu.org/software/coreutils/", &DAT_0040d200);
  uVar3 = 0x40e68f;
  if (puVar5 == &DAT_0040d200)
    {
      uVar3 = 0x40d221;
    }
LAB_00401dab:
  ;
  do
    {
      func_0x004015d0
	("or available locally via: info \'(coreutils) %s%s\'\n", puVar5,
	 uVar3);
    LAB_00401dbd:
      ;
      func_0x00401930 ((ulong) uParm1);
    LAB_00401dc4:
      ;
      func_0x004015d0 ("Full documentation at: <%s%s>\n",
		       "https://www.gnu.org/software/coreutils/",
		       &DAT_0040d200);
      puVar5 = &DAT_0040d200;
      uVar3 = 0x40d221;
    }
  while (true);
}

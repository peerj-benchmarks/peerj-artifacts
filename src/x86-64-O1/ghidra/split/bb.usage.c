
#include "split.h"

long null_ARRAY_00614c0_0_8_;
long null_ARRAY_00614c0_8_8_;
long null_ARRAY_00614f0_0_8_;
long null_ARRAY_00614f0_24_4_;
long null_ARRAY_00614f0_48_8_;
long null_ARRAY_00614f0_56_8_;
long null_ARRAY_00614f0_8_8_;
long null_ARRAY_0061524_0_8_;
long null_ARRAY_0061524_16_8_;
long null_ARRAY_0061524_24_8_;
long null_ARRAY_0061524_32_8_;
long null_ARRAY_0061524_40_8_;
long null_ARRAY_0061524_48_8_;
long null_ARRAY_0061524_8_8_;
long null_ARRAY_0061528_0_4_;
long null_ARRAY_0061528_16_8_;
long null_ARRAY_0061528_4_4_;
long null_ARRAY_0061528_8_4_;
long local_68_2_2_;
long local_6b_1_3_;
long local_6c_4_4_;
long local_6e_4_4_;
long local_6f_4_4_;
long local_70_0_2_;
long local_70_4_4_;
long local_71_0_1_;
long local_71_4_4_;
long local_72_4_4_;
long DAT_00000010;
long DAT_004106db;
long DAT_0041075a;
long DAT_00410790;
long DAT_004107ac;
long DAT_004107b5;
long DAT_00410839;
long DAT_0041089e;
long DAT_004108a1;
long DAT_004108ea;
long DAT_004119e8;
long DAT_004119ec;
long DAT_004119f0;
long DAT_004119f3;
long DAT_004119f5;
long DAT_004119f9;
long DAT_004119fd;
long DAT_004121ab;
long DAT_004128c8;
long DAT_004129d1;
long DAT_004129d7;
long DAT_004129e9;
long DAT_004129ea;
long DAT_00412a08;
long DAT_00412a18;
long DAT_00412a1c;
long DAT_00614700;
long DAT_00614710;
long DAT_00614720;
long DAT_00614b80;
long DAT_00614b84;
long DAT_00614b90;
long DAT_00614ba0;
long DAT_00614c10;
long DAT_00614c14;
long DAT_00614c18;
long DAT_00614c1c;
long _DAT_00614c40;
long DAT_00614c44;
long DAT_00614c4c;
long DAT_00614e00;
long DAT_00614e10;
long DAT_00614e20;
long DAT_00614e28;
long DAT_00614e40;
long DAT_00614e48;
long DAT_00614ec0;
long DAT_00614ec8;
long DAT_00614ed0;
long DAT_00614ed8;
long DAT_00614ee0;
long DAT_00614ee1;
long DAT_00614ee2;
long DAT_00614f90;
long DAT_00614f98;
long DAT_00614fa0;
long DAT_00614fa8;
long DAT_00614fb0;
long DAT_00614fb8;
long DAT_00614fc0;
long DAT_00615100;
long DAT_00615108;
long DAT_00615110;
long DAT_00615118;
long DAT_00615120;
long DAT_00615128;
long DAT_00615130;
long DAT_00615138;
long DAT_006152b8;
long DAT_00615ac8;
long DAT_00615ad0;
long DAT_00615ad8;
long DAT_00615ae8;
long fde_004136e8;
long null_ARRAY_00411743;
long null_ARRAY_00411780;
long null_ARRAY_00412ec0;
long null_ARRAY_00413100;
long null_ARRAY_00614c00;
long null_ARRAY_00614e60;
long null_ARRAY_00614f00;
long null_ARRAY_00615000;
long null_ARRAY_00615080;
long null_ARRAY_00615140;
long null_ARRAY_00615240;
long null_ARRAY_00615280;
long null_ARRAY_006152c0;
long PTR_DAT_00614b98;
long PTR_null_ARRAY_00614bf8;
long PTR_s_abcdefghijklmnopqrstuvwxyz_00614b88;
long stack0x00000008;
void
FUN_00402f0e (uint uParm1)
{
  int iVar1;
  long lVar2;
  undefined8 uVar3;
  undefined **ppuVar4;
  char *pcVar5;
  undefined *puVar6;
  undefined *local_88;
  char *local_80;
  undefined8 local_78;
  char *local_70;
  char *local_68;
  char *local_60;
  char *local_58;
  char *local_50;
  char *local_48;
  char *local_40;
  char *local_38;
  char *local_30;
  undefined8 local_28;
  undefined8 local_20;

  ppuVar4 = &local_88;
  if (uParm1 != 0)
    {
      func_0x00401ca0 (DAT_00614e20,
		       "Try \'%s --help\' for more information.\n",
		       DAT_00615138);
      goto LAB_00403124;
    }
  func_0x00401a30 ("Usage: %s [OPTION]... [FILE [PREFIX]]\n", DAT_00615138);
  uVar3 = DAT_00614e00;
  func_0x00401cb0
    ("Output pieces of FILE to PREFIXaa, PREFIXab, ...;\ndefault size is 1000 lines, and default PREFIX is \'x\'.\n",
     DAT_00614e00);
  func_0x00401cb0
    ("\nWith no FILE, or when FILE is -, read standard input.\n", uVar3);
  func_0x00401cb0
    ("\nMandatory arguments to long options are mandatory for short options too.\n",
     uVar3);
  func_0x00401ca0 (uVar3,
		   "  -a, --suffix-length=N   generate suffixes of length N (default %d)\n      --additional-suffix=SUFFIX  append an additional SUFFIX to file names\n  -b, --bytes=SIZE        put SIZE bytes per output file\n  -C, --line-bytes=SIZE   put at most SIZE bytes of records per output file\n  -d                      use numeric suffixes starting at 0, not alphabetic\n      --numeric-suffixes[=FROM]  same as -d, but allow setting the start value\n  -x                      use hex suffixes starting at 0, not alphabetic\n      --hex-suffixes[=FROM]  same as -x, but allow setting the start value\n  -e, --elide-empty-files  do not generate empty output files with \'-n\'\n      --filter=COMMAND    write to shell COMMAND; file name is $FILE\n  -l, --lines=NUMBER      put NUMBER lines/records per output file\n  -n, --number=CHUNKS     generate CHUNKS output files; see explanation below\n  -t, --separator=SEP     use SEP instead of newline as the record separator;\n                            \'\\0\' (zero) specifies the NUL character\n  -u, --unbuffered        immediately copy input to output with \'-n r/...\'\n",
		   2);
  func_0x00401cb0
    ("      --verbose           print a diagnostic just before each\n                            output file is opened\n",
     uVar3);
  func_0x00401cb0 ("      --help     display this help and exit\n", uVar3);
  func_0x00401cb0 ("      --version  output version information and exit\n",
		   uVar3);
  func_0x00401cb0
    ("\nThe SIZE argument is an integer and optional unit (example: 10K is 10*1024).\nUnits are K,M,G,T,P,E,Z,Y (powers of 1024) or KB,MB,... (powers of 1000).\n",
     uVar3);
  func_0x00401cb0
    ("\nCHUNKS may be:\n  N       split into N files based on size of input\n  K/N     output Kth of N to stdout\n  l/N     split into N files without splitting lines/records\n  l/K/N   output Kth of N to stdout without splitting lines/records\n  r/N     like \'l\' but use round robin distribution\n  r/K/N   likewise but only output Kth of N to stdout\n",
     uVar3);
  local_88 = &DAT_004107b5;
  local_80 = "test invocation";
  local_78 = 0x410818;
  local_70 = "Multi-call invocation";
  local_68 = "sha224sum";
  local_60 = "sha2 utilities";
  local_58 = "sha256sum";
  local_50 = "sha2 utilities";
  local_48 = "sha384sum";
  local_40 = "sha2 utilities";
  local_38 = "sha512sum";
  local_30 = "sha2 utilities";
  local_28 = 0;
  local_20 = 0;
  puVar6 = &DAT_004107b5;
  do
    {
      iVar1 = func_0x00401dd0 ("split", puVar6);
      if (iVar1 == 0)
	break;
      ppuVar4 = ppuVar4 + 2;
      puVar6 = *ppuVar4;
    }
  while (puVar6 != (undefined *) 0x0);
  pcVar5 = ppuVar4[1];
  if (pcVar5 == (char *) 0x0)
    {
      func_0x00401a30 ("\n%s online help: <%s>\n", "GNU coreutils",
		       "https://www.gnu.org/software/coreutils/");
      lVar2 = func_0x00401e30 (5, 0);
      if (lVar2 == 0)
	goto LAB_0040312b;
      iVar1 = func_0x00401d10 (lVar2, &DAT_00410839, 3);
      if (iVar1 == 0)
	{
	  func_0x00401a30 ("Full documentation at: <%s%s>\n",
			   "https://www.gnu.org/software/coreutils/",
			   "split");
	  pcVar5 = "split";
	  uVar3 = 0x4107d1;
	  goto LAB_00403112;
	}
      pcVar5 = "split";
    LAB_004030d0:
      ;
      func_0x00401a30
	("Report %s translation bugs to <https://translationproject.org/team/>\n",
	 "split");
    }
  else
    {
      func_0x00401a30 ("\n%s online help: <%s>\n", "GNU coreutils",
		       "https://www.gnu.org/software/coreutils/");
      lVar2 = func_0x00401e30 (5, 0);
      if ((lVar2 != 0)
	  && (iVar1 = func_0x00401d10 (lVar2, &DAT_00410839, 3), iVar1 != 0))
	goto LAB_004030d0;
    }
  func_0x00401a30 ("Full documentation at: <%s%s>\n",
		   "https://www.gnu.org/software/coreutils/", "split");
  uVar3 = 0x412a07;
  if (pcVar5 == "split")
    {
      uVar3 = 0x4107d1;
    }
LAB_00403112:
  ;
  do
    {
      func_0x00401a30
	("or available locally via: info \'(coreutils) %s%s\'\n", pcVar5,
	 uVar3);
    LAB_00403124:
      ;
      func_0x00401e80 ((ulong) uParm1);
    LAB_0040312b:
      ;
      func_0x00401a30 ("Full documentation at: <%s%s>\n",
		       "https://www.gnu.org/software/coreutils/", "split");
      pcVar5 = "split";
      uVar3 = 0x4107d1;
    }
  while (true);
}

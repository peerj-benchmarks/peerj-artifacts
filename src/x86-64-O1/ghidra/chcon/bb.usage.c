
#include "chcon.h"

long null_ARRAY_006164c_0_8_;
long null_ARRAY_006164c_8_8_;
long null_ARRAY_0061670_0_8_;
long null_ARRAY_0061670_16_8_;
long null_ARRAY_0061670_24_8_;
long null_ARRAY_0061670_32_8_;
long null_ARRAY_0061670_40_8_;
long null_ARRAY_0061670_48_8_;
long null_ARRAY_0061670_8_8_;
long null_ARRAY_0061674_0_4_;
long null_ARRAY_0061674_16_8_;
long null_ARRAY_0061674_4_4_;
long null_ARRAY_0061674_8_4_;
long local_68_2_2_;
long local_6b_1_3_;
long local_6c_4_4_;
long local_6f_4_4_;
long local_70_0_2_;
long local_70_4_4_;
long local_71_0_1_;
long local_71_4_4_;
long local_72_4_4_;
long DAT_00000010;
long DAT_00411786;
long DAT_0041180a;
long DAT_00412698;
long DAT_0041269c;
long DAT_004126a0;
long DAT_004126a3;
long DAT_004126a5;
long DAT_004126a9;
long DAT_004126ad;
long DAT_00412e2b;
long DAT_004131fa;
long DAT_0041320b;
long DAT_0041320c;
long DAT_004133e9;
long DAT_004133ea;
long DAT_00413408;
long DAT_00413442;
long DAT_0041358a;
long DAT_00413594;
long DAT_00413609;
long DAT_00616000;
long DAT_00616010;
long DAT_00616020;
long DAT_00616468;
long DAT_006164d0;
long DAT_006164d4;
long DAT_006164d8;
long DAT_006164dc;
long DAT_00616500;
long DAT_00616510;
long DAT_00616520;
long DAT_00616528;
long DAT_00616540;
long DAT_00616548;
long DAT_006165a0;
long DAT_006165a8;
long DAT_006165b0;
long DAT_006165b8;
long DAT_006165c0;
long DAT_006165c8;
long DAT_006165d0;
long DAT_006165d1;
long DAT_006165d2;
long DAT_006165d8;
long DAT_006165e0;
long DAT_006165e8;
long DAT_00616778;
long DAT_00616780;
long DAT_00616784;
long DAT_00616788;
long DAT_00616790;
long DAT_00616798;
long DAT_006167a8;
long fde_004143b0;
long null_ARRAY_00412480;
long null_ARRAY_00413450;
long null_ARRAY_00413a20;
long null_ARRAY_00413c50;
long null_ARRAY_006164c0;
long null_ARRAY_00616560;
long null_ARRAY_00616590;
long null_ARRAY_00616600;
long null_ARRAY_00616700;
long null_ARRAY_00616740;
long PTR_DAT_00616460;
long PTR_null_ARRAY_006164b8;
long stack0x00000008;
void
FUN_00401f6e (uint uParm1)
{
  int iVar1;
  long lVar2;
  undefined8 uVar3;
  undefined **ppuVar4;
  char *pcVar5;
  undefined *puVar6;
  undefined *local_88;
  char *local_80;
  undefined8 local_78;
  char *local_70;
  char *local_68;
  char *local_60;
  char *local_58;
  char *local_50;
  char *local_48;
  char *local_40;
  char *local_38;
  char *local_30;
  undefined8 local_28;
  undefined8 local_20;

  ppuVar4 = &local_88;
  if (uParm1 != 0)
    {
      func_0x00401ad0 (DAT_00616520,
		       "Try \'%s --help\' for more information.\n",
		       DAT_006165e8);
      goto LAB_0040219a;
    }
  func_0x004018c0
    ("Usage: %s [OPTION]... CONTEXT FILE...\n  or:  %s [OPTION]... [-u USER] [-r ROLE] [-l RANGE] [-t TYPE] FILE...\n  or:  %s [OPTION]... --reference=RFILE FILE...\n",
     DAT_006165e8, DAT_006165e8, DAT_006165e8);
  uVar3 = DAT_00616500;
  func_0x00401ae0
    ("Change the SELinux security context of each FILE to CONTEXT.\nWith --reference, change the security context of each FILE to that of RFILE.\n",
     DAT_00616500);
  func_0x00401ae0
    ("\nMandatory arguments to long options are mandatory for short options too.\n",
     uVar3);
  func_0x00401ae0
    ("      --dereference      affect the referent of each symbolic link (this is\n                         the default), rather than the symbolic link itself\n  -h, --no-dereference   affect symbolic links instead of any referenced file\n",
     uVar3);
  func_0x00401ae0
    ("  -u, --user=USER        set user USER in the target security context\n  -r, --role=ROLE        set role ROLE in the target security context\n  -t, --type=TYPE        set type TYPE in the target security context\n  -l, --range=RANGE      set range RANGE in the target security context\n",
     uVar3);
  func_0x00401ae0
    ("      --no-preserve-root  do not treat \'/\' specially (the default)\n      --preserve-root    fail to operate recursively on \'/\'\n",
     uVar3);
  func_0x00401ae0
    ("      --reference=RFILE  use RFILE\'s security context rather than specifying\n                         a CONTEXT value\n",
     uVar3);
  func_0x00401ae0
    ("  -R, --recursive        operate on files and directories recursively\n",
     uVar3);
  func_0x00401ae0
    ("  -v, --verbose          output a diagnostic for every file processed\n",
     uVar3);
  func_0x00401ae0
    ("\nThe following options modify how a hierarchy is traversed when the -R\noption is also specified.  If more than one is specified, only the final\none takes effect.\n\n  -H                     if a command line argument is a symbolic link\n                         to a directory, traverse it\n  -L                     traverse every symbolic link to a directory\n                         encountered\n  -P                     do not traverse any symbolic links (default)\n\n",
     uVar3);
  func_0x00401ae0 ("      --help     display this help and exit\n", uVar3);
  func_0x00401ae0 ("      --version  output version information and exit\n",
		   uVar3);
  local_88 = &DAT_00411786;
  local_80 = "test invocation";
  local_78 = 0x4117e9;
  local_70 = "Multi-call invocation";
  local_68 = "sha224sum";
  local_60 = "sha2 utilities";
  local_58 = "sha256sum";
  local_50 = "sha2 utilities";
  local_48 = "sha384sum";
  local_40 = "sha2 utilities";
  local_38 = "sha512sum";
  local_30 = "sha2 utilities";
  local_28 = 0;
  local_20 = 0;
  puVar6 = &DAT_00411786;
  do
    {
      iVar1 = func_0x00401bf0 ("chcon", puVar6);
      if (iVar1 == 0)
	break;
      ppuVar4 = ppuVar4 + 2;
      puVar6 = *ppuVar4;
    }
  while (puVar6 != (undefined *) 0x0);
  pcVar5 = ppuVar4[1];
  if (pcVar5 == (char *) 0x0)
    {
      func_0x004018c0 ("\n%s online help: <%s>\n", "GNU coreutils",
		       "https://www.gnu.org/software/coreutils/");
      lVar2 = func_0x00401c50 (5, 0);
      if (lVar2 == 0)
	goto LAB_004021a1;
      iVar1 = func_0x00401b60 (lVar2, &DAT_0041180a, 3);
      if (iVar1 == 0)
	{
	  func_0x004018c0 ("Full documentation at: <%s%s>\n",
			   "https://www.gnu.org/software/coreutils/",
			   "chcon");
	  pcVar5 = "chcon";
	  uVar3 = 0x4117a2;
	  goto LAB_00402188;
	}
      pcVar5 = "chcon";
    LAB_00402146:
      ;
      func_0x004018c0
	("Report %s translation bugs to <https://translationproject.org/team/>\n",
	 "chcon");
    }
  else
    {
      func_0x004018c0 ("\n%s online help: <%s>\n", "GNU coreutils",
		       "https://www.gnu.org/software/coreutils/");
      lVar2 = func_0x00401c50 (5, 0);
      if ((lVar2 != 0)
	  && (iVar1 = func_0x00401b60 (lVar2, &DAT_0041180a, 3), iVar1 != 0))
	goto LAB_00402146;
    }
  func_0x004018c0 ("Full documentation at: <%s%s>\n",
		   "https://www.gnu.org/software/coreutils/", "chcon");
  uVar3 = 0x413407;
  if (pcVar5 == "chcon")
    {
      uVar3 = 0x4117a2;
    }
LAB_00402188:
  ;
  do
    {
      func_0x004018c0
	("or available locally via: info \'(coreutils) %s%s\'\n", pcVar5,
	 uVar3);
    LAB_0040219a:
      ;
      func_0x00401cb0 ((ulong) uParm1);
    LAB_004021a1:
      ;
      func_0x004018c0 ("Full documentation at: <%s%s>\n",
		       "https://www.gnu.org/software/coreutils/", "chcon");
      pcVar5 = "chcon";
      uVar3 = 0x4117a2;
    }
  while (true);
}

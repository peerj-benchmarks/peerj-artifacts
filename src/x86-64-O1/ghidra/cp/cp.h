typedef unsigned char   undefined;

typedef unsigned char    byte;
typedef unsigned char    dwfenc;
typedef unsigned int    dword;
typedef unsigned long    qword;
typedef unsigned char    undefined1;
typedef unsigned short    undefined2;
typedef unsigned int    undefined4;
typedef unsigned long    undefined6;
typedef unsigned long    undefined8;
typedef unsigned short    word;
#define unkbyte9   unsigned long long
#define unkbyte10   unsigned long long
#define unkbyte11   unsigned long long
#define unkbyte12   unsigned long long
#define unkbyte13   unsigned long long
#define unkbyte14   unsigned long long
#define unkbyte15   unsigned long long
#define unkbyte16   unsigned long long

#define unkuint9   unsigned long long
#define unkuint10   unsigned long long
#define unkuint11   unsigned long long
#define unkuint12   unsigned long long
#define unkuint13   unsigned long long
#define unkuint14   unsigned long long
#define unkuint15   unsigned long long
#define unkuint16   unsigned long long

#define unkint9   long long
#define unkint10   long long
#define unkint11   long long
#define unkint12   long long
#define unkint13   long long
#define unkint14   long long
#define unkint15   long long
#define unkint16   long long

#define unkfloat1   float
#define unkfloat2   float
#define unkfloat3   float
#define unkfloat5   double
#define unkfloat6   double
#define unkfloat7   double
#define unkfloat9   long double
#define unkfloat11   long double
#define unkfloat12   long double
#define unkfloat13   long double
#define unkfloat14   long double
#define unkfloat15   long double
#define unkfloat16   long double

#define BADSPACEBASE   void
#define code   void

#define ulong long
#define true 1
#define bool int
#define false 0
#define float10 int
#define ushort int
#define undefined7 long
#define undefined2 long
#define uint5 long
#define uint6 long

// Add definition for uint
typedef unsigned int    uint;

#define sbyte char

extern imperfection_wrapper(void);
typedef struct eh_frame_hdr eh_frame_hdr, *Peh_frame_hdr;

struct eh_frame_hdr {
    byte eh_frame_hdr_version; // Exception Handler Frame Header Version
    dwfenc eh_frame_pointer_encoding; // Exception Handler Frame Pointer Encoding
    dwfenc eh_frame_desc_entry_count_encoding; // Encoding of # of Exception Handler FDEs
    dwfenc eh_frame_table_encoding; // Exception Handler Table Encoding
};

typedef struct fde_table_entry fde_table_entry, *Pfde_table_entry;

struct fde_table_entry {
    dword initial_loc; // Initial Location
    dword data_loc; // Data location
};

typedef enum Elf64_DynTag {
    DT_AUDIT=1879047932,
    DT_AUXILIARY=2147483645,
    DT_BIND_NOW=24,
    DT_CHECKSUM=1879047672,
    DT_CONFIG=1879047930,
    DT_DEBUG=21,
    DT_DEPAUDIT=1879047931,
    DT_ENCODING=32,
    DT_FEATURE_1=1879047676,
    DT_FILTER=2147483647,
    DT_FINI=13,
    DT_FINI_ARRAY=26,
    DT_FINI_ARRAYSZ=28,
    DT_FLAGS=30,
    DT_FLAGS_1=1879048187,
    DT_GNU_CONFLICT=1879047928,
    DT_GNU_CONFLICTSZ=1879047670,
    DT_GNU_HASH=1879047925,
    DT_GNU_LIBLIST=1879047929,
    DT_GNU_LIBLISTSZ=1879047671,
    DT_GNU_PRELINKED=1879047669,
    DT_HASH=4,
    DT_INIT=12,
    DT_INIT_ARRAY=25,
    DT_INIT_ARRAYSZ=27,
    DT_JMPREL=23,
    DT_MOVEENT=1879047674,
    DT_MOVESZ=1879047675,
    DT_MOVETAB=1879047934,
    DT_NEEDED=1,
    DT_NULL=0,
    DT_PLTGOT=3,
    DT_PLTPAD=1879047933,
    DT_PLTPADSZ=1879047673,
    DT_PLTREL=20,
    DT_PLTRELSZ=2,
    DT_POSFLAG_1=1879047677,
    DT_PREINIT_ARRAYSZ=33,
    DT_REL=17,
    DT_RELA=7,
    DT_RELACOUNT=1879048185,
    DT_RELAENT=9,
    DT_RELASZ=8,
    DT_RELCOUNT=1879048186,
    DT_RELENT=19,
    DT_RELSZ=18,
    DT_RPATH=15,
    DT_RUNPATH=29,
    DT_SONAME=14,
    DT_STRSZ=10,
    DT_STRTAB=5,
    DT_SYMBOLIC=16,
    DT_SYMENT=11,
    DT_SYMINENT=1879047679,
    DT_SYMINFO=1879047935,
    DT_SYMINSZ=1879047678,
    DT_SYMTAB=6,
    DT_TEXTREL=22,
    DT_TLSDESC_GOT=1879047927,
    DT_TLSDESC_PLT=1879047926,
    DT_VERDEF=1879048188,
    DT_VERDEFNUM=1879048189,
    DT_VERNEED=1879048190,
    DT_VERNEEDNUM=1879048191,
    DT_VERSYM=1879048176
} Elf64_DynTag;

typedef enum Elf_ProgramHeaderType {
    PT_DYNAMIC=2,
    PT_GNU_EH_FRAME=1685382480,
    PT_GNU_RELRO=1685382482,
    PT_GNU_STACK=1685382481,
    PT_INTERP=3,
    PT_LOAD=1,
    PT_NOTE=4,
    PT_NULL=0,
    PT_PHDR=6,
    PT_SHLIB=5,
    PT_TLS=7
} Elf_ProgramHeaderType;

typedef struct Elf64_Dyn Elf64_Dyn, *PElf64_Dyn;

struct Elf64_Dyn {
    enum Elf64_DynTag d_tag;
    qword d_val;
};

typedef struct Elf64_Shdr Elf64_Shdr, *PElf64_Shdr;

typedef enum Elf_SectionHeaderType {
    SHT_CHECKSUM=1879048184,
    SHT_DYNAMIC=6,
    SHT_DYNSYM=11,
    SHT_FINI_ARRAY=15,
    SHT_GNU_ATTRIBUTES=1879048181,
    SHT_GNU_HASH=1879048182,
    SHT_GNU_LIBLIST=1879048183,
    SHT_GNU_verdef=1879048189,
    SHT_GNU_verneed=1879048190,
    SHT_GNU_versym=1879048191,
    SHT_GROUP=17,
    SHT_HASH=5,
    SHT_INIT_ARRAY=14,
    SHT_NOBITS=8,
    SHT_NOTE=7,
    SHT_NULL=0,
    SHT_PREINIT_ARRAY=16,
    SHT_PROGBITS=1,
    SHT_REL=9,
    SHT_RELA=4,
    SHT_SHLIB=10,
    SHT_STRTAB=3,
    SHT_SUNW_COMDAT=1879048187,
    SHT_SUNW_move=1879048186,
    SHT_SUNW_syminfo=1879048188,
    SHT_SYMTAB=2,
    SHT_SYMTAB_SHNDX=18
} Elf_SectionHeaderType;

struct Elf64_Shdr {
    dword sh_name;
    enum Elf_SectionHeaderType sh_type;
    qword sh_flags;
    qword sh_addr;
    qword sh_offset;
    qword sh_size;
    dword sh_link;
    dword sh_info;
    qword sh_addralign;
    qword sh_entsize;
};

typedef struct Elf64_Phdr Elf64_Phdr, *PElf64_Phdr;

struct Elf64_Phdr {
    enum Elf_ProgramHeaderType p_type;
    dword p_flags;
    qword p_offset;
    qword p_vaddr;
    qword p_paddr;
    qword p_filesz;
    qword p_memsz;
    qword p_align;
};

typedef struct Elf64_Rela Elf64_Rela, *PElf64_Rela;

struct Elf64_Rela {
    qword r_offset; // location to apply the relocation action
    qword r_info; // the symbol table index and the type of relocation
    qword r_addend; // a constant addend used to compute the relocatable field value
};

typedef struct Elf64_Sym Elf64_Sym, *PElf64_Sym;

struct Elf64_Sym {
    dword st_name;
    byte st_info;
    byte st_other;
    word st_shndx;
    qword st_value;
    qword st_size;
};

typedef struct Elf64_Ehdr Elf64_Ehdr, *PElf64_Ehdr;

struct Elf64_Ehdr {
    byte e_ident_magic_num;
    char e_ident_magic_str[3];
    byte e_ident_class;
    byte e_ident_data;
    byte e_ident_version;
    byte e_ident_pad[9];
    word e_type;
    word e_machine;
    dword e_version;
    qword e_entry;
    qword e_phoff;
    qword e_shoff;
    dword e_flags;
    word e_ehsize;
    word e_phentsize;
    word e_phnum;
    word e_shentsize;
    word e_shnum;
    word e_shstrndx;
};




undefined8 _DT_INIT(void);
void FUN_00402420(void);
void FUN_00402680(void);
void FUN_00402900(void);
void FUN_00402c20(void);
void entry(void);
void FUN_00402c80(void);
void FUN_00402d00(void);
void FUN_00402d80(void);
ulong FUN_00402dbe(undefined8 uParm1,long lParm2,undefined *puParm3);
void FUN_00402e3f(undefined8 uParm1,undefined8 uParm2,char cParm3);
undefined8 FUN_00402f4a(undefined8 uParm1,long lParm2,long lParm3,long *plParm4,char *pcParm5,long lParm6);
ulong FUN_00403472(undefined8 uParm1,long lParm2,long lParm3,long lParm4);
void FUN_00403658(uint uParm1);
ulong FUN_0040398a(int iParm1,undefined8 *puParm2,long lParm3,char cParm4,undefined1 *puParm5);
ulong FUN_00403ddb(uint uParm1,undefined8 *puParm2);
ulong FUN_00404611(undefined8 uParm1,undefined8 uParm2);
void FUN_00404658(long lParm1,undefined8 uParm2,long lParm3);
void FUN_00404722(undefined8 uParm1,undefined8 uParm2,long lParm3);
ulong FUN_0040479b(undefined8 uParm1,undefined8 uParm2,byte bParm3,uint uParm4,char cParm5);
undefined8 FUN_0040485a(uint uParm1,ulong uParm2);
ulong FUN_004048ec(undefined8 uParm1,undefined8 uParm2,undefined8 uParm3);
undefined8 FUN_00404928(ulong uParm1,undefined8 uParm2,char cParm3,long lParm4);
undefined8 FUN_004049ea(uint param_1,uint param_2,long param_3,ulong param_4,ulong param_5,byte param_6,undefined8 param_7,undefined8 param_8,ulong param_9,long *param_10,byte *param_11);
void FUN_00404df1(undefined8 uParm1,undefined8 uParm2,uint uParm3);
ulong FUN_00404e14(void);
ulong FUN_00404e3b(undefined8 uParm1,undefined8 uParm2,undefined8 uParm3,ulong uParm4,long lParm5);
undefined8 FUN_00404f6b(undefined8 uParm1,undefined8 uParm2,undefined8 uParm3,long lParm4);
void FUN_00405002(long lParm1);
void FUN_0040502b(long lParm1);
void FUN_00405054(long lParm1);
ulong FUN_00405079(long lParm1);
ulong FUN_004050a7(long lParm1,undefined8 uParm2,uint uParm3,long lParm4,char cParm5,long lParm6);
ulong FUN_00405278(void);
ulong FUN_004052ae(char *param_1,char *param_2,ulong param_3,long **param_4,long **param_5,long **param_6,uint param_7,byte *param_8,byte *param_9,undefined *param_10);
void FUN_0040905d(undefined8 uParm1,undefined8 uParm2,ulong uParm3,uint *puParm4,undefined8 uParm5,undefined8 uParm6);
ulong FUN_00409190(ulong *puParm1,ulong uParm2);
ulong FUN_0040919f(long *plParm1,long *plParm2);
void FUN_004091be(long lParm1);
void FUN_004091d5(undefined8 uParm1,undefined8 uParm2);
undefined8 FUN_0040920c(undefined8 uParm1,undefined8 uParm2);
undefined8 FUN_0040923d(undefined8 uParm1,undefined8 uParm2,undefined8 uParm3);
void FUN_004092af(void);
void FUN_004092e8(undefined4 uParm1,undefined4 *puParm2);
ulong FUN_00409312(uint *puParm1);
void FUN_004095ab(undefined8 uParm1,uint *puParm2);
long FUN_004095cc(long lParm1,long lParm2);
void FUN_0040961e(undefined8 uParm1,undefined8 *puParm2);
ulong FUN_00409638(undefined4 uParm1,undefined8 uParm2,ulong uParm3,undefined8 uParm4,undefined4 uParm5,byte bParm6);
ulong FUN_00409750(undefined8 uParm1,ulong uParm2,undefined8 uParm3,byte bParm4);
ulong FUN_0040985f(undefined8 uParm1,undefined8 uParm2,undefined8 uParm3);
ulong FUN_004098d7(undefined8 uParm1);
long FUN_0040991d(undefined8 uParm1,ulong uParm2);
void FUN_004099fc(void);
long FUN_00409a0f(undefined8 uParm1,long *plParm2,long lParm3,long lParm4);
void FUN_00409aff(undefined8 uParm1,undefined8 uParm2,long lParm3);
void FUN_00409b5c(long *plParm1,long lParm2,long lParm3);
long FUN_00409c27(undefined8 uParm1,undefined8 uParm2,undefined8 uParm3,undefined8 uParm4,undefined8 uParm5,code *pcParm6);
void FUN_00409c92(long lParm1,long lParm2,undefined uParm3);
void FUN_00409d78(char *pcParm1);
long FUN_00409dc1(long lParm1,int iParm2,char cParm3);
void FUN_0040a29d(undefined8 uParm1,undefined8 uParm2);
void FUN_0040a2b0(undefined8 uParm1,undefined8 uParm2);
ulong FUN_0040a2cd(undefined8 uParm1,char *pcParm2);
void FUN_0040a309(undefined8 uParm1,char *pcParm2);
ulong FUN_0040a33a(ulong uParm1,ulong uParm2,ulong uParm3);
char * FUN_0040a3ac(char *pcParm1,uint uParm2);
void FUN_0040a946(undefined8 uParm1);
void FUN_0040a959(void);
void FUN_0040aa31(void);
long FUN_0040aad7(void);
void FUN_0040ab66(void);
ulong FUN_0040ab7e(char *pcParm1);
undefined * FUN_0040abd5(undefined8 uParm1);
char * FUN_0040ac39(char *pcParm1);
ulong FUN_0040ac7f(long lParm1);
undefined FUN_0040acb9(char *pcParm1);;
void FUN_0040acec(void);
void FUN_0040acfa(undefined8 param_1,byte param_2,ulong param_3);
void FUN_0040ad44(long lParm1,undefined8 uParm2,undefined8 *puParm3);
ulong FUN_0040adb2(long lParm1,undefined8 uParm2,undefined8 *puParm3);
void FUN_0040ade9(ulong uParm1,undefined *puParm2);
void FUN_0040af5a(void);
long FUN_0040af72(long lParm1,char *pcParm2,undefined8 *puParm3);
long FUN_0040b04b(uint uParm1,long lParm2,long lParm3);
ulong FUN_0040b0b0(ulong uParm1,ulong uParm2);
undefined FUN_0040b0c3(long lParm1,long lParm2);;
ulong FUN_0040b0ca(long lParm1,undefined8 uParm2,uint uParm3);
ulong FUN_0040b14e(ulong uParm1,long lParm2);
long FUN_0040b26a(long *plParm1,undefined8 uParm2);
undefined8 FUN_0040b28c(long lParm1,long **pplParm2,char cParm3);
long FUN_0040b3b0(long lParm1,long lParm2,long **pplParm3,char cParm4);
long FUN_0040b4b4(long lParm1,long lParm2);
long * FUN_0040b510(undefined8 uParm1,undefined1 *puParm2,code *pcParm3,code *pcParm4,long lParm5);
void FUN_0040b5fb(long **pplParm1);
ulong FUN_0040b6a0(long *plParm1,undefined8 uParm2);
undefined8 FUN_0040b7dd(long lParm1,long lParm2,long *plParm3);
undefined8 FUN_0040ba0a(undefined8 uParm1,undefined8 uParm2);
long FUN_0040ba39(long lParm1,undefined8 uParm2);
ulong FUN_0040bc15(undefined8 *puParm1,ulong uParm2);
ulong FUN_0040bc3f(long lParm1,ulong uParm2);
ulong FUN_0040bc4f(undefined8 *puParm1,undefined8 *puParm2);
ulong FUN_0040bc86(undefined8 *puParm1,undefined8 *puParm2);
void FUN_0040bcc2(undefined8 *puParm1);
void FUN_0040bcd8(long lParm1);
undefined8 FUN_0040bd6d(undefined8 uParm1,undefined8 uParm2,undefined8 uParm3,uint uParm4,uint uParm5);
void FUN_0040bda2(undefined8 uParm1,uint uParm2,undefined4 uParm3);
int * FUN_0040bdde(int *piParm1,int iParm2);
undefined * FUN_0040be0e(char *pcParm1,int iParm2);
ulong FUN_0040bec6(undefined *param_1,ulong param_2,long param_3,ulong param_4,uint param_5,ulong param_6,long param_7,char *param_8,char *param_9);
undefined1 * FUN_0040cc7d(int iParm1,undefined8 uParm2,undefined8 uParm3,uint *puParm4);
ulong FUN_0040ce1f(undefined1 *puParm1,byte bParm2,undefined8 uParm3);
void FUN_0040ce53(uint uParm1,undefined8 uParm2,undefined8 uParm3);
void FUN_0040ce81(ulong uParm1,undefined8 uParm2);
void FUN_0040ce99(undefined8 uParm1,undefined8 uParm2,char cParm3);
void FUN_0040cf22(undefined8 uParm1,char cParm2);
void FUN_0040cf3b(undefined8 uParm1);
void FUN_0040cf4e(uint uParm1,undefined8 uParm2,undefined8 uParm3);
void FUN_0040cfdd(void);
void FUN_0040cff0(undefined8 uParm1,undefined8 uParm2);
void FUN_0040d005(undefined8 uParm1);
ulong FUN_0040d01b(uint uParm1,long lParm2,uint uParm3,long lParm4,uint uParm5);
long FUN_0040d3bc(uint uParm1,undefined8 uParm2,ulong uParm3);
ulong FUN_0040d41d(undefined8 uParm1,undefined8 uParm2);
void FUN_0040d544(undefined8 *puParm1,undefined8 *puParm2);
long FUN_0040d558(long lParm1,uint uParm2);
undefined8 FUN_0040d83d(undefined8 uParm1,ulong uParm2);
ulong FUN_0040d89a(long lParm1,int iParm2,undefined8 uParm3,code *pcParm4,ulong uParm5);
ulong FUN_0040d9e9(uint uParm1);
ulong FUN_0040da1b(ulong *puParm1,ulong uParm2);
ulong FUN_0040da2a(ulong *puParm1,ulong *puParm2);
ulong FUN_0040da34(undefined8 uParm1,undefined8 *puParm2,long lParm3,ulong uParm4);
ulong FUN_0040e0ee(undefined8 *puParm1);
undefined8 FUN_0040e192(long lParm1,long *plParm2);
ulong FUN_0040e23d(uint uParm1,long lParm2,undefined8 *puParm3);
void FUN_0040e5ea(undefined8 uParm1,undefined8 uParm2);
ulong FUN_0040e603(undefined8 uParm1,undefined8 *puParm2);
void FUN_0040e7e9(undefined8 uParm1,long lParm2,undefined8 uParm3,undefined8 uParm4,undefined8 *puParm5,undefined8 uParm6);
void FUN_0040ea26(void);
void FUN_0040ea78(void);
ulong FUN_0040eafd(void);
void FUN_0040eb33(long lParm1);
long FUN_0040eb4d(long lParm1,long lParm2);
void FUN_0040eb80(undefined8 uParm1,undefined8 uParm2);
void FUN_0040eba9(undefined8 uParm1);
long FUN_0040ebc0(void);
long FUN_0040ebe8(void);
ulong FUN_0040ec14(void);
undefined8 FUN_0040ec42(ulong uParm1,ulong uParm2,ulong uParm3);
void FUN_0040ec8a(void);
void FUN_0040ecb7(undefined8 uParm1);
void FUN_0040ecf7(uint uParm1,uint uParm2,undefined8 uParm3,undefined8 uParm4);
void FUN_0040ed4e(undefined4 uParm1,undefined4 uParm2,undefined4 uParm3,undefined4 uParm4,undefined4 uParm5,undefined4 uParm6,undefined4 uParm7,undefined4 uParm8,ulong param_1,ulong param_2,undefined8 param_3,undefined8 param_4,undefined8 param_5,undefined8 param_6);
ulong FUN_0040ee21(uint param_1,undefined8 param_2,undefined8 param_3,undefined8 param_4);
undefined8 FUN_0040ef90(ulong uParm1);
ulong FUN_0040efdf(long lParm1);
undefined8 FUN_0040f06f(void);
void FUN_0040f082(void);
undefined8 FUN_0040f090(ulong uParm1,long lParm2,long lParm3,ulong uParm4);
long FUN_0040f15c(long lParm1,ulong uParm2);
void FUN_0040f5ec(long lParm1,int *piParm2);
ulong FUN_0040f6ab(int param_1,undefined8 *param_2,char *param_3,long *param_4,int *param_5,int param_6,int *param_7,int param_8,undefined8 param_9);
ulong FUN_0040fb66(uint param_1,undefined8 *param_2,char *param_3,long param_4,undefined8 param_5,ulong param_6,uint *param_7,int param_8);
void FUN_0040ffe2(void);
void FUN_00410038(void);
ulong FUN_0041004e(long lParm1,uint uParm2,uint uParm3);
ulong FUN_00410130(ulong uParm1,char *pcParm2,uint uParm3,long lParm4,uint uParm5);
ulong FUN_004103b0(long lParm1,long lParm2);
void FUN_00410419(long lParm1);
ulong FUN_00410433(uint *puParm1,byte *pbParm2,long lParm3);
ulong FUN_00410498(long lParm1,ulong uParm2);
undefined8 FUN_0041057d(long lParm1,ulong uParm2);
undefined8 FUN_004105d1(long lParm1,uint uParm2,long lParm3);
ulong FUN_00410666(uint param_1,undefined8 param_2,uint param_3,uint param_4);
undefined8 FUN_00410756(long lParm1,undefined8 uParm2,undefined8 uParm3);
void FUN_004107b8(long lParm1,long lParm2);
ulong FUN_004107e6(long lParm1,long lParm2);
void FUN_00410bc0(void);
undefined8 FUN_00410bd4(long lParm1);
ulong FUN_00410c64(long lParm1,long lParm2);
undefined8 FUN_00410cb0(undefined8 uParm1,ulong uParm2,long lParm3);
undefined8 FUN_00410d18(long lParm1);
undefined8 FUN_00410e0d(ulong uParm1,long lParm2,ulong uParm3);
ulong FUN_00410f34(undefined8 uParm1,undefined8 uParm2,undefined8 uParm3);
undefined8 FUN_00411001(undefined8 uParm1,undefined8 uParm2,undefined4 uParm3,undefined4 *puParm4);
void FUN_0041102c(undefined8 uParm1,uint uParm2,uint uParm3);
ulong FUN_0041104e(uint *puParm1,undefined8 uParm2,uint uParm3);
void FUN_00411070(undefined8 uParm1,undefined8 uParm2);
undefined *FUN_00411094(uint uParm1,undefined8 uParm2,undefined *puParm3,ulong uParm4,undefined1 *puParm5,code *pcParm6);
ulong FUN_0041123e(ulong uParm1,char cParm2);
ulong FUN_0041129e(undefined8 uParm1);
undefined8 FUN_004112ff(void);
void FUN_00411307(undefined8 *puParm1);
ulong FUN_00411347(ulong uParm1);
ulong FUN_004113d5(char *pcParm1,ulong uParm2);
char * FUN_0041140a(void);
void FUN_00411745(undefined8 uParm1);
undefined8 FUN_00411768(void);
ulong FUN_0041178a(undefined8 *puParm1,ulong uParm2);
void FUN_0041187b(undefined8 uParm1);
ulong FUN_00411893(undefined8 *puParm1);
long * FUN_004118c3(ulong uParm1,ulong uParm2);
long * FUN_0041190e(long lParm1,ulong uParm2);
void FUN_00411ba9(long *plParm1,ulong uParm2,ulong uParm3);
undefined8 FUN_00411cf5(long *plParm1);
void FUN_00411d2c(long *plParm1,long *plParm2);
void FUN_00411f8f(ulong *puParm1);
void FUN_004121c0(undefined8 uParm1);
ulong FUN_004121dd(long lParm1,uint uParm2,uint uParm3);
void FUN_00412334(undefined8 uParm1,undefined8 uParm2);
undefined8 FUN_0041234a(undefined8 uParm1);
void FUN_004123cd(void);
undefined8 FUN_00412479(uint uParm1,long lParm2,undefined8 uParm3,undefined8 uParm4);
ulong FUN_004124e6(char *pcParm1,long lParm2);
long FUN_0041252d(ulong uParm1,uint *puParm2,ulong uParm3,uint *puParm4,long *plParm5);
ulong FUN_00412651(void);
ulong FUN_00412683(void);
int * FUN_00412890(ulong uParm1,long lParm2,ulong uParm3,uint *puParm4,ulong *puParm5);
char * FUN_00412e73(int iParm1,long lParm2,long lParm3,long lParm4,uint uParm5);
void FUN_0041348b(uint param_1,undefined8 param_2);
ulong FUN_00413651(void);
void FUN_00413864(undefined8 uParm1,uint uParm2);
undefined8 *FUN_00413a05(undefined8 *puParm1,undefined8 *puParm2,undefined8 *puParm3,undefined8 uParm4);
long FUN_004195a2(undefined8 uParm1,undefined8 uParm2);
double FUN_00419632(int *piParm1);
void FUN_0041967a(int *param_1);
undefined8 FUN_004196fb(uint *puParm1,ulong *puParm2);
undefined8 FUN_00419b20(byte *pbParm1,ulong *puParm2,byte **ppbParm3);
ulong FUN_0041a685(void);
ulong FUN_0041a6ad(void);
void FUN_0041a6e0(void);
undefined8 _DT_FINI(void);
undefined FUN_00623000();
undefined FUN_00623008();
undefined FUN_00623010();
undefined FUN_00623018();
undefined FUN_00623020();
undefined FUN_00623028();
undefined FUN_00623030();
undefined FUN_00623038();
undefined FUN_00623040();
undefined FUN_00623048();
undefined FUN_00623050();
undefined FUN_00623058();
undefined FUN_00623060();
undefined FUN_00623068();
undefined FUN_00623070();
undefined FUN_00623078();
undefined FUN_00623080();
undefined FUN_00623088();
undefined FUN_00623090();
undefined FUN_00623098();
undefined FUN_006230a0();
undefined FUN_006230a8();
undefined FUN_006230b0();
undefined FUN_006230b8();
undefined FUN_006230c0();
undefined FUN_006230c8();
undefined FUN_006230d0();
undefined FUN_006230d8();
undefined FUN_006230e0();
undefined FUN_006230e8();
undefined FUN_006230f0();
undefined FUN_006230f8();
undefined FUN_00623100();
undefined FUN_00623108();
undefined FUN_00623110();
undefined FUN_00623118();
undefined FUN_00623120();
undefined FUN_00623128();
undefined FUN_00623130();
undefined FUN_00623138();
undefined FUN_00623140();
undefined FUN_00623148();
undefined FUN_00623150();
undefined FUN_00623158();
undefined FUN_00623160();
undefined FUN_00623168();
undefined FUN_00623170();
undefined FUN_00623178();
undefined FUN_00623180();
undefined FUN_00623188();
undefined FUN_00623190();
undefined FUN_00623198();
undefined FUN_006231a0();
undefined FUN_006231a8();
undefined FUN_006231b0();
undefined FUN_006231b8();
undefined FUN_006231c0();
undefined FUN_006231c8();
undefined FUN_006231d0();
undefined FUN_006231d8();
undefined FUN_006231e0();
undefined FUN_006231e8();
undefined FUN_006231f0();
undefined FUN_006231f8();
undefined FUN_00623200();
undefined FUN_00623208();
undefined FUN_00623210();
undefined FUN_00623218();
undefined FUN_00623220();
undefined FUN_00623228();
undefined FUN_00623230();
undefined FUN_00623238();
undefined FUN_00623240();
undefined FUN_00623248();
undefined FUN_00623250();
undefined FUN_00623258();
undefined FUN_00623260();
undefined FUN_00623268();
undefined FUN_00623270();
undefined FUN_00623278();
undefined FUN_00623280();
undefined FUN_00623288();
undefined FUN_00623290();
undefined FUN_00623298();
undefined FUN_006232a0();
undefined FUN_006232a8();
undefined FUN_006232b0();
undefined FUN_006232b8();
undefined FUN_006232c0();
undefined FUN_006232c8();
undefined FUN_006232d0();
undefined FUN_006232d8();
undefined FUN_006232e0();
undefined FUN_006232e8();
undefined FUN_006232f0();
undefined FUN_006232f8();
undefined FUN_00623300();
undefined FUN_00623308();
undefined FUN_00623310();
undefined FUN_00623318();
undefined FUN_00623320();
undefined FUN_00623328();
undefined FUN_00623330();
undefined FUN_00623338();
undefined FUN_00623340();
undefined FUN_00623348();
undefined FUN_00623350();
undefined FUN_00623358();
undefined FUN_00623360();
undefined FUN_00623368();
undefined FUN_00623370();
undefined FUN_00623378();
undefined FUN_00623380();
undefined FUN_00623388();
undefined FUN_00623390();
undefined FUN_00623398();
undefined FUN_006233a0();
undefined FUN_006233a8();
undefined FUN_006233b0();
undefined FUN_006233b8();
undefined FUN_006233c0();
undefined FUN_006233c8();
undefined FUN_006233d0();
undefined FUN_006233d8();
undefined FUN_006233e0();
undefined FUN_006233e8();



#include "cp.h"

long null_ARRAY_0062264_0_8_;
long null_ARRAY_0062264_8_8_;
long null_ARRAY_0062274_0_4_;
long null_ARRAY_00622d4_0_8_;
long null_ARRAY_00622d4_16_8_;
long null_ARRAY_00622d4_24_8_;
long null_ARRAY_00622d4_32_8_;
long null_ARRAY_00622d4_40_8_;
long null_ARRAY_00622d4_48_8_;
long null_ARRAY_00622d4_8_8_;
long null_ARRAY_00622da_0_4_;
long null_ARRAY_00622da_16_8_;
long null_ARRAY_00622da_4_4_;
long null_ARRAY_00622da_8_4_;
long local_1d_4_4_;
long local_1e_0_4_;
long local_2d_1_7_;
long local_2d_4_4_;
long local_2e_0_1_;
long local_2e_0_4_;
long local_2e_1_7_;
long local_2e_4_4_;
long local_2f_0_4_;
long local_2f_1_7_;
long local_30_1_7_;
long local_30_4_4_;
long local_31_4_4_;
long local_32_4_4_;
long local_33_4_4_;
long local_68_2_2_;
long local_6b_1_3_;
long local_6c_4_4_;
long local_6e_4_4_;
long local_6f_4_4_;
long local_70_0_2_;
long local_70_4_4_;
long local_71_0_1_;
long local_71_4_4_;
long local_72_4_4_;
long DAT_00000010;
long DAT_0041a7a1;
long DAT_0041a7a4;
long DAT_0041a828;
long DAT_0041a879;
long DAT_0041a87a;
long DAT_0041b700;
long DAT_0041d854;
long DAT_0041d856;
long DAT_0041d86e;
long DAT_0041d9d8;
long DAT_0041d9de;
long DAT_0041d9e0;
long DAT_0041d9e4;
long DAT_0041d9e8;
long DAT_0041d9eb;
long DAT_0041d9ed;
long DAT_0041d9f1;
long DAT_0041d9f5;
long DAT_0041e232;
long DAT_0041e5d5;
long DAT_0041e6d9;
long DAT_0041e6df;
long DAT_0041e6e1;
long DAT_0041e6e2;
long DAT_0041e700;
long DAT_0041e704;
long DAT_0041e7a7;
long DAT_0041e7bd;
long DAT_00622000;
long DAT_00622010;
long DAT_00622020;
long DAT_006225c0;
long DAT_006225c8;
long DAT_006225e0;
long DAT_00622650;
long DAT_00622654;
long DAT_00622658;
long DAT_0062265c;
long DAT_00622680;
long DAT_00622688;
long DAT_00622690;
long DAT_006226a0;
long DAT_006226a8;
long DAT_006226c0;
long DAT_006226c8;
long DAT_00622788;
long DAT_00622789;
long DAT_0062278a;
long DAT_00622bc0;
long DAT_00622bc8;
long DAT_00622bd0;
long DAT_00622bd8;
long DAT_00622be0;
long DAT_00622be8;
long DAT_00622bf0;
long DAT_00622bf8;
long DAT_00622c00;
long DAT_00622d78;
long DAT_00622d80;
long DAT_00622d88;
long DAT_00622d8c;
long DAT_00622d90;
long DAT_00622d91;
long DAT_00622d94;
long DAT_00622dd8;
long DAT_00622ddc;
long DAT_00622de0;
long DAT_00622e38;
long DAT_00622e40;
long DAT_00622e50;
long fde_0041f830;
long null_ARRAY_0041b2e0;
long null_ARRAY_0041b300;
long null_ARRAY_0041b340;
long null_ARRAY_0041b710;
long null_ARRAY_0041b728;
long null_ARRAY_0041b740;
long null_ARRAY_0041d7f1;
long null_ARRAY_0041d8c0;
long null_ARRAY_0041d900;
long null_ARRAY_0041d970;
long null_ARRAY_0041e1a0;
long null_ARRAY_0041ebe0;
long null_ARRAY_0041ed40;
long null_ARRAY_0041ee30;
long null_ARRAY_00622640;
long null_ARRAY_006226e0;
long null_ARRAY_00622740;
long null_ARRAY_006227c0;
long null_ARRAY_00622c40;
long null_ARRAY_00622d40;
long null_ARRAY_00622da0;
long PTR_DAT_0041aa40;
long PTR_DAT_006225d0;
long PTR_FUN_006225d8;
long PTR_null_ARRAY_00622638;
long PTR_null_ARRAY_00622660;
void
FUN_00403658 (uint uParm1)
{
  int iVar1;
  long lVar2;
  undefined8 uVar3;
  undefined **ppuVar4;
  undefined *puVar5;
  undefined *local_88;
  char *local_80;
  undefined8 local_78;
  char *local_70;
  char *local_68;
  char *local_60;
  char *local_58;
  char *local_50;
  char *local_48;
  char *local_40;
  char *local_38;
  char *local_30;
  undefined8 local_28;
  undefined8 local_20;

  ppuVar4 = &local_88;
  if (uParm1 != 0)
    {
      func_0x004027e0 (DAT_006226a0,
		       "Try \'%s --help\' for more information.\n",
		       DAT_00622c00);
      goto LAB_004038ec;
    }
  func_0x00402490
    ("Usage: %s [OPTION]... [-T] SOURCE DEST\n  or:  %s [OPTION]... SOURCE... DIRECTORY\n  or:  %s [OPTION]... -t DIRECTORY SOURCE...\n",
     DAT_00622c00, DAT_00622c00, DAT_00622c00);
  uVar3 = DAT_00622680;
  func_0x004027f0
    ("Copy SOURCE to DEST, or multiple SOURCE(s) to DIRECTORY.\n",
     DAT_00622680);
  func_0x004027f0
    ("\nMandatory arguments to long options are mandatory for short options too.\n",
     uVar3);
  func_0x004027f0
    ("  -a, --archive                same as -dR --preserve=all\n      --attributes-only        don\'t copy the file data, just the attributes\n      --backup[=CONTROL]       make a backup of each existing destination file\n  -b                           like --backup but does not accept an argument\n      --copy-contents          copy contents of special files when recursive\n  -d                           same as --no-dereference --preserve=links\n",
     uVar3);
  func_0x004027f0
    ("  -f, --force                  if an existing destination file cannot be\n                                 opened, remove it and try again (this option\n                                 is ignored when the -n option is also used)\n  -i, --interactive            prompt before overwrite (overrides a previous -n\n                                  option)\n  -H                           follow command-line symbolic links in SOURCE\n",
     uVar3);
  func_0x004027f0
    ("  -l, --link                   hard link files instead of copying\n  -L, --dereference            always follow symbolic links in SOURCE\n",
     uVar3);
  func_0x004027f0
    ("  -n, --no-clobber             do not overwrite an existing file (overrides\n                                 a previous -i option)\n  -P, --no-dereference         never follow symbolic links in SOURCE\n",
     uVar3);
  func_0x004027f0
    ("  -p                           same as --preserve=mode,ownership,timestamps\n      --preserve[=ATTR_LIST]   preserve the specified attributes (default:\n                                 mode,ownership,timestamps), if possible\n                                 additional attributes: context, links, xattr,\n                                 all\n",
     uVar3);
  func_0x004027f0
    ("      --no-preserve=ATTR_LIST  don\'t preserve the specified attributes\n      --parents                use full source file name under DIRECTORY\n",
     uVar3);
  func_0x004027f0
    ("  -R, -r, --recursive          copy directories recursively\n      --reflink[=WHEN]         control clone/CoW copies. See below\n      --remove-destination     remove each existing destination file before\n                                 attempting to open it (contrast with --force)\n",
     uVar3);
  func_0x004027f0
    ("      --sparse=WHEN            control creation of sparse files. See below\n      --strip-trailing-slashes  remove any trailing slashes from each SOURCE\n                                 argument\n",
     uVar3);
  func_0x004027f0
    ("  -s, --symbolic-link          make symbolic links instead of copying\n  -S, --suffix=SUFFIX          override the usual backup suffix\n  -t, --target-directory=DIRECTORY  copy all SOURCE arguments into DIRECTORY\n  -T, --no-target-directory    treat DEST as a normal file\n",
     uVar3);
  func_0x004027f0
    ("  -u, --update                 copy only when the SOURCE file is newer\n                                 than the destination file or when the\n                                 destination file is missing\n  -v, --verbose                explain what is being done\n  -x, --one-file-system        stay on this file system\n",
     uVar3);
  func_0x004027f0
    ("  -Z                           set SELinux security context of destination\n                                 file to default type\n      --context[=CTX]          like -Z, or if CTX is specified then set the\n                                 SELinux or SMACK security context to CTX\n",
     uVar3);
  func_0x004027f0 ("      --help     display this help and exit\n", uVar3);
  func_0x004027f0 ("      --version  output version information and exit\n",
		   uVar3);
  func_0x004027f0
    ("\nBy default, sparse SOURCE files are detected by a crude heuristic and the\ncorresponding DEST file is made sparse as well.  That is the behavior\nselected by --sparse=auto.  Specify --sparse=always to create a sparse DEST\nfile whenever the SOURCE file contains a long enough sequence of zero bytes.\nUse --sparse=never to inhibit creation of sparse files.\n\nWhen --reflink[=always] is specified, perform a lightweight copy, where the\ndata blocks are copied only when modified.  If this is not possible the copy\nfails, or if --reflink=auto is specified, fall back to a standard copy.\n",
     uVar3);
  func_0x004027f0
    ("\nThe backup suffix is \'~\', unless set with --suffix or SIMPLE_BACKUP_SUFFIX.\nThe version control method may be selected via the --backup option or through\nthe VERSION_CONTROL environment variable.  Here are the values:\n\n",
     uVar3);
  func_0x004027f0
    ("  none, off       never make backups (even if --backup is given)\n  numbered, t     make numbered backups\n  existing, nil   numbered if numbered backups exist, simple otherwise\n  simple, never   always make simple backups\n",
     uVar3);
  func_0x004027f0
    ("\nAs a special case, cp makes a backup of SOURCE when the force and backup\noptions are given and SOURCE and DEST are the same name for an existing,\nregular file.\n",
     uVar3);
  local_88 = &DAT_0041a7a4;
  local_80 = "test invocation";
  local_78 = 0x41a807;
  local_70 = "Multi-call invocation";
  local_68 = "sha224sum";
  local_60 = "sha2 utilities";
  local_58 = "sha256sum";
  local_50 = "sha2 utilities";
  local_48 = "sha384sum";
  local_40 = "sha2 utilities";
  local_38 = "sha512sum";
  local_30 = "sha2 utilities";
  local_28 = 0;
  local_20 = 0;
  puVar5 = &DAT_0041a7a4;
  do
    {
      iVar1 = func_0x004029e0 (&DAT_0041a7a1, puVar5);
      if (iVar1 == 0)
	break;
      ppuVar4 = ppuVar4 + 2;
      puVar5 = *ppuVar4;
    }
  while (puVar5 != (undefined *) 0x0);
  puVar5 = ppuVar4[1];
  if (puVar5 == (undefined *) 0x0)
    {
      func_0x00402490 ("\n%s online help: <%s>\n", "GNU coreutils",
		       "https://www.gnu.org/software/coreutils/");
      lVar2 = func_0x00402a60 (5, 0);
      if (lVar2 == 0)
	goto LAB_004038f3;
      iVar1 = func_0x004028b0 (lVar2, &DAT_0041a828, 3);
      if (iVar1 == 0)
	{
	  func_0x00402490 ("Full documentation at: <%s%s>\n",
			   "https://www.gnu.org/software/coreutils/",
			   &DAT_0041a7a1);
	  puVar5 = &DAT_0041a7a1;
	  uVar3 = 0x41a7c0;
	  goto LAB_004038da;
	}
      puVar5 = &DAT_0041a7a1;
    LAB_00403898:
      ;
      func_0x00402490
	("Report %s translation bugs to <https://translationproject.org/team/>\n",
	 &DAT_0041a7a1);
    }
  else
    {
      func_0x00402490 ("\n%s online help: <%s>\n", "GNU coreutils",
		       "https://www.gnu.org/software/coreutils/");
      lVar2 = func_0x00402a60 (5, 0);
      if ((lVar2 != 0)
	  && (iVar1 = func_0x004028b0 (lVar2, &DAT_0041a828, 3), iVar1 != 0))
	goto LAB_00403898;
    }
  func_0x00402490 ("Full documentation at: <%s%s>\n",
		   "https://www.gnu.org/software/coreutils/", &DAT_0041a7a1);
  uVar3 = 0x41e6ff;
  if (puVar5 == &DAT_0041a7a1)
    {
      uVar3 = 0x41a7c0;
    }
LAB_004038da:
  ;
  do
    {
      func_0x00402490
	("or available locally via: info \'(coreutils) %s%s\'\n", puVar5,
	 uVar3);
    LAB_004038ec:
      ;
      func_0x00402ae0 ((ulong) uParm1);
    LAB_004038f3:
      ;
      func_0x00402490 ("Full documentation at: <%s%s>\n",
		       "https://www.gnu.org/software/coreutils/",
		       &DAT_0041a7a1);
      puVar5 = &DAT_0041a7a1;
      uVar3 = 0x41a7c0;
    }
  while (true);
}

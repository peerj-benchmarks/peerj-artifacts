
#include "tr.h"

long null_ARRAY_0061348_0_8_;
long null_ARRAY_0061348_8_8_;
long null_ARRAY_00613dc_0_8_;
long null_ARRAY_00613dc_16_8_;
long null_ARRAY_00613dc_24_8_;
long null_ARRAY_00613dc_32_8_;
long null_ARRAY_00613dc_40_8_;
long null_ARRAY_00613dc_48_8_;
long null_ARRAY_00613dc_8_8_;
long null_ARRAY_00613e0_0_4_;
long null_ARRAY_00613e0_16_8_;
long null_ARRAY_00613e0_4_4_;
long null_ARRAY_00613e0_8_4_;
long local_68_2_2_;
long local_6b_1_3_;
long local_6c_4_4_;
long local_6e_4_4_;
long local_6f_4_4_;
long local_70_0_2_;
long local_70_4_4_;
long local_71_0_1_;
long local_71_4_4_;
long local_72_4_4_;
long DAT_00000010;
long DAT_0040edf1;
long DAT_0040edf3;
long DAT_0040edf6;
long DAT_0040edf9;
long DAT_0040edfc;
long DAT_0040edff;
long DAT_0040ee00;
long DAT_0040ee02;
long DAT_0040ee05;
long DAT_0040ee62;
long DAT_0040ee65;
long DAT_0040eee9;
long DAT_004101b2;
long DAT_00410370;
long DAT_00410374;
long DAT_00410378;
long DAT_0041037b;
long DAT_0041037d;
long DAT_00410381;
long DAT_00410385;
long DAT_00410b2b;
long DAT_00411248;
long DAT_00411351;
long DAT_00411357;
long DAT_00411369;
long DAT_0041136a;
long DAT_00411388;
long DAT_0041138c;
long DAT_00411416;
long DAT_00613000;
long DAT_00613010;
long DAT_00613020;
long DAT_00613428;
long DAT_00613490;
long DAT_00613494;
long DAT_00613498;
long DAT_0061349c;
long DAT_006134c0;
long DAT_006134c8;
long DAT_006134d0;
long DAT_006134e0;
long DAT_006134e8;
long DAT_00613500;
long DAT_00613508;
long DAT_00613c80;
long DAT_00613c81;
long DAT_00613c82;
long DAT_00613c83;
long DAT_00613c84;
long DAT_00613c88;
long DAT_00613c90;
long DAT_00613c98;
long DAT_00613e38;
long DAT_00613e40;
long DAT_00613e48;
long DAT_00613e58;
long fde_00411fe0;
long null_ARRAY_004101c0;
long null_ARRAY_004102c0;
long null_ARRAY_00411820;
long null_ARRAY_00411a60;
long null_ARRAY_00613480;
long null_ARRAY_00613520;
long null_ARRAY_00613580;
long null_ARRAY_00613680;
long null_ARRAY_00613780;
long null_ARRAY_00613880;
long null_ARRAY_00613cc0;
long null_ARRAY_00613dc0;
long null_ARRAY_00613e00;
long PTR_DAT_00613420;
long PTR_null_ARRAY_00613478;
void
FUN_004030b0 (uint uParm1)
{
  int iVar1;
  long lVar2;
  undefined8 uVar3;
  undefined **ppuVar4;
  undefined *puVar5;
  undefined *local_88;
  char *local_80;
  undefined8 local_78;
  char *local_70;
  char *local_68;
  char *local_60;
  char *local_58;
  char *local_50;
  char *local_48;
  char *local_40;
  char *local_38;
  char *local_30;
  undefined8 local_28;
  undefined8 local_20;

  ppuVar4 = &local_88;
  if (uParm1 != 0)
    {
      func_0x00401990 (DAT_006134e0,
		       "Try \'%s --help\' for more information.\n",
		       DAT_00613c98);
      goto LAB_004032a2;
    }
  func_0x004017c0 ("Usage: %s [OPTION]... SET1 [SET2]\n", DAT_00613c98);
  uVar3 = DAT_006134c0;
  func_0x004019a0
    ("Translate, squeeze, and/or delete characters from standard input,\nwriting to standard output.\n\n  -c, -C, --complement    use the complement of SET1\n  -d, --delete            delete characters in SET1, do not translate\n  -s, --squeeze-repeats   replace each sequence of a repeated character\n                            that is listed in the last specified SET,\n                            with a single occurrence of that character\n  -t, --truncate-set1     first truncate SET1 to length of SET2\n",
     DAT_006134c0);
  func_0x004019a0 ("      --help     display this help and exit\n", uVar3);
  func_0x004019a0 ("      --version  output version information and exit\n",
		   uVar3);
  func_0x004019a0
    ("\nSETs are specified as strings of characters.  Most represent themselves.\nInterpreted sequences are:\n\n  \\NNN            character with octal value NNN (1 to 3 octal digits)\n  \\\\              backslash\n  \\a              audible BEL\n  \\b              backspace\n  \\f              form feed\n  \\n              new line\n  \\r              return\n  \\t              horizontal tab\n",
     uVar3);
  func_0x004019a0
    ("  \\v              vertical tab\n  CHAR1-CHAR2     all characters from CHAR1 to CHAR2 in ascending order\n  [CHAR*]         in SET2, copies of CHAR until length of SET1\n  [CHAR*REPEAT]   REPEAT copies of CHAR, REPEAT octal if starting with 0\n  [:alnum:]       all letters and digits\n  [:alpha:]       all letters\n  [:blank:]       all horizontal whitespace\n  [:cntrl:]       all control characters\n  [:digit:]       all digits\n",
     uVar3);
  func_0x004019a0
    ("  [:graph:]       all printable characters, not including space\n  [:lower:]       all lower case letters\n  [:print:]       all printable characters, including space\n  [:punct:]       all punctuation characters\n  [:space:]       all horizontal or vertical whitespace\n  [:upper:]       all upper case letters\n  [:xdigit:]      all hexadecimal digits\n  [=CHAR=]        all characters which are equivalent to CHAR\n",
     uVar3);
  func_0x004019a0
    ("\nTranslation occurs if -d is not given and both SET1 and SET2 appear.\n-t may be used only when translating.  SET2 is extended to length of\nSET1 by repeating its last character as necessary.  Excess characters\nof SET2 are ignored.  Only [:lower:] and [:upper:] are guaranteed to\nexpand in ascending order; used in SET2 while translating, they may\nonly be used in pairs to specify case conversion.  -s uses the last\nspecified SET, and occurs after translation or deletion.\n",
     uVar3);
  local_88 = &DAT_0040ee65;
  local_80 = "test invocation";
  local_78 = 0x40eec8;
  local_70 = "Multi-call invocation";
  local_68 = "sha224sum";
  local_60 = "sha2 utilities";
  local_58 = "sha256sum";
  local_50 = "sha2 utilities";
  local_48 = "sha384sum";
  local_40 = "sha2 utilities";
  local_38 = "sha512sum";
  local_30 = "sha2 utilities";
  local_28 = 0;
  local_20 = 0;
  puVar5 = &DAT_0040ee65;
  do
    {
      iVar1 = func_0x00401ab0 (&DAT_0040ee62, puVar5);
      if (iVar1 == 0)
	break;
      ppuVar4 = ppuVar4 + 2;
      puVar5 = *ppuVar4;
    }
  while (puVar5 != (undefined *) 0x0);
  puVar5 = ppuVar4[1];
  if (puVar5 == (undefined *) 0x0)
    {
      func_0x004017c0 ("\n%s online help: <%s>\n", "GNU coreutils",
		       "https://www.gnu.org/software/coreutils/");
      lVar2 = func_0x00401b10 (5, 0);
      if (lVar2 == 0)
	goto LAB_004032a9;
      iVar1 = func_0x00401a10 (lVar2, &DAT_0040eee9, 3);
      if (iVar1 == 0)
	{
	  func_0x004017c0 ("Full documentation at: <%s%s>\n",
			   "https://www.gnu.org/software/coreutils/",
			   &DAT_0040ee62);
	  puVar5 = &DAT_0040ee62;
	  uVar3 = 0x40ee81;
	  goto LAB_00403290;
	}
      puVar5 = &DAT_0040ee62;
    LAB_0040324e:
      ;
      func_0x004017c0
	("Report %s translation bugs to <https://translationproject.org/team/>\n",
	 &DAT_0040ee62);
    }
  else
    {
      func_0x004017c0 ("\n%s online help: <%s>\n", "GNU coreutils",
		       "https://www.gnu.org/software/coreutils/");
      lVar2 = func_0x00401b10 (5, 0);
      if ((lVar2 != 0)
	  && (iVar1 = func_0x00401a10 (lVar2, &DAT_0040eee9, 3), iVar1 != 0))
	goto LAB_0040324e;
    }
  func_0x004017c0 ("Full documentation at: <%s%s>\n",
		   "https://www.gnu.org/software/coreutils/", &DAT_0040ee62);
  uVar3 = 0x411387;
  if (puVar5 == &DAT_0040ee62)
    {
      uVar3 = 0x40ee81;
    }
LAB_00403290:
  ;
  do
    {
      func_0x004017c0
	("or available locally via: info \'(coreutils) %s%s\'\n", puVar5,
	 uVar3);
    LAB_004032a2:
      ;
      func_0x00401b60 ((ulong) uParm1);
    LAB_004032a9:
      ;
      func_0x004017c0 ("Full documentation at: <%s%s>\n",
		       "https://www.gnu.org/software/coreutils/",
		       &DAT_0040ee62);
      puVar5 = &DAT_0040ee62;
      uVar3 = 0x40ee81;
    }
  while (true);
}

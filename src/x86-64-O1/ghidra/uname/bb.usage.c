
#include "uname.h"

long null_ARRAY_0060fa8_0_8_;
long null_ARRAY_0060fa8_8_8_;
long null_ARRAY_0060fc8_0_8_;
long null_ARRAY_0060fc8_16_8_;
long null_ARRAY_0060fc8_24_8_;
long null_ARRAY_0060fc8_32_8_;
long null_ARRAY_0060fc8_40_8_;
long null_ARRAY_0060fc8_48_8_;
long null_ARRAY_0060fc8_8_8_;
long null_ARRAY_0060fcc_0_4_;
long null_ARRAY_0060fcc_16_8_;
long null_ARRAY_0060fcc_4_4_;
long null_ARRAY_0060fcc_8_4_;
long local_68_2_2_;
long local_6b_1_3_;
long local_6c_4_4_;
long local_6e_4_4_;
long local_6f_4_4_;
long local_70_0_2_;
long local_70_4_4_;
long local_71_0_1_;
long local_71_4_4_;
long local_72_4_4_;
long DAT_00000010;
long DAT_0040cb00;
long DAT_0040cb84;
long DAT_0040d358;
long DAT_0040d35c;
long DAT_0040d360;
long DAT_0040d363;
long DAT_0040d365;
long DAT_0040d369;
long DAT_0040d36d;
long DAT_0040daeb;
long DAT_0040de95;
long DAT_0040df99;
long DAT_0040df9f;
long DAT_0040dfb1;
long DAT_0040dfb2;
long DAT_0040dfd0;
long DAT_0040dfd4;
long DAT_0040e05e;
long DAT_0060f658;
long DAT_0060f668;
long DAT_0060f678;
long DAT_0060fa20;
long DAT_0060fa30;
long DAT_0060fa90;
long DAT_0060fa94;
long DAT_0060fa98;
long DAT_0060fa9c;
long DAT_0060fac0;
long DAT_0060fad0;
long DAT_0060fae0;
long DAT_0060fae8;
long DAT_0060fb00;
long DAT_0060fb08;
long DAT_0060fb50;
long DAT_0060fb58;
long DAT_0060fb60;
long DAT_0060fb68;
long DAT_0060fcf8;
long DAT_0060fd00;
long DAT_0060fd08;
long DAT_0060fd18;
long fde_0040eba8;
long null_ARRAY_0040d0c0;
long null_ARRAY_0040d140;
long null_ARRAY_0040e480;
long null_ARRAY_0040e6c0;
long null_ARRAY_0060fa80;
long null_ARRAY_0060fb20;
long null_ARRAY_0060fb80;
long null_ARRAY_0060fc80;
long null_ARRAY_0060fcc0;
long PTR_DAT_0060fa28;
long PTR_null_ARRAY_0060fa78;
void
FUN_00401a6d (uint uParm1)
{
  int iVar1;
  long lVar2;
  undefined8 uVar3;
  undefined1 **ppuVar4;
  char *pcVar5;
  char *unaff_RBP;
  undefined1 *puVar6;
  undefined1 *local_88;
  char *local_80;
  undefined8 local_78;
  char *local_70;
  char *local_68;
  char *local_60;
  char *local_58;
  char *local_50;
  char *local_48;
  char *local_40;
  char *local_38;
  char *local_30;
  undefined8 local_28;
  undefined8 local_20;

  ppuVar4 = &local_88;
  if (uParm1 != 0)
    {
      func_0x00401620 (DAT_0060fae0,
		       "Try \'%s --help\' for more information.\n",
		       DAT_0060fb68);
      goto LAB_00401c67;
    }
  func_0x004014a0 ("Usage: %s [OPTION]...\n", DAT_0060fb68);
  uVar3 = DAT_0060fac0;
  if (DAT_0060fa20 == 1)
    {
      func_0x00401630
	("Print certain system information.  With no OPTION, same as -s.\n\n  -a, --all                print all information, in the following order,\n                             except omit -p and -i if unknown:\n  -s, --kernel-name        print the kernel name\n  -n, --nodename           print the network node hostname\n  -r, --kernel-release     print the kernel release\n",
	 DAT_0060fac0);
      func_0x00401630
	("  -v, --kernel-version     print the kernel version\n  -m, --machine            print the machine hardware name\n  -p, --processor          print the processor type (non-portable)\n  -i, --hardware-platform  print the hardware platform (non-portable)\n  -o, --operating-system   print the operating system\n",
	 uVar3);
    }
  else
    {
      func_0x00401630 ("Print machine architecture.\n\n", DAT_0060fac0);
    }
  uVar3 = DAT_0060fac0;
  func_0x00401630 ("      --help     display this help and exit\n",
		   DAT_0060fac0);
  func_0x00401630 ("      --version  output version information and exit\n",
		   uVar3);
  unaff_RBP = "uname";
  if (DAT_0060fa20 != 1)
    {
      unaff_RBP = "arch";
    }
  local_88 = &DAT_0040cb00;
  local_80 = "test invocation";
  local_78 = 0x40cb63;
  local_70 = "Multi-call invocation";
  local_68 = "sha224sum";
  local_60 = "sha2 utilities";
  local_58 = "sha256sum";
  local_50 = "sha2 utilities";
  local_48 = "sha384sum";
  local_40 = "sha2 utilities";
  local_38 = "sha512sum";
  local_30 = "sha2 utilities";
  local_28 = 0;
  local_20 = 0;
  puVar6 = &DAT_0040cb00;
  do
    {
      iVar1 = func_0x00401700 (unaff_RBP, puVar6);
      if (iVar1 == 0)
	break;
      ppuVar4 = ppuVar4 + 2;
      puVar6 = *ppuVar4;
    }
  while (puVar6 != (undefined1 *) 0x0);
  pcVar5 = ppuVar4[1];
  if (pcVar5 == (char *) 0x0)
    {
      func_0x004014a0 ("\n%s online help: <%s>\n", "GNU coreutils",
		       "https://www.gnu.org/software/coreutils/");
      lVar2 = func_0x00401760 (5, 0);
      if (lVar2 == 0)
	goto LAB_00401c6f;
      iVar1 = func_0x00401680 (lVar2, &DAT_0040cb84, 3);
      pcVar5 = unaff_RBP;
      if (iVar1 == 0)
	{
	  func_0x004014a0 ("Full documentation at: <%s%s>\n",
			   "https://www.gnu.org/software/coreutils/",
			   unaff_RBP);
	  uVar3 = 0x40cb1c;
	  pcVar5 = unaff_RBP;
	  goto LAB_00401c55;
	}
    LAB_00401c1b:
      ;
      func_0x004014a0
	("Report %s translation bugs to <https://translationproject.org/team/>\n",
	 unaff_RBP);
    }
  else
    {
      func_0x004014a0 ("\n%s online help: <%s>\n", "GNU coreutils",
		       "https://www.gnu.org/software/coreutils/");
      lVar2 = func_0x00401760 (5, 0);
      if ((lVar2 != 0)
	  && (iVar1 = func_0x00401680 (lVar2, &DAT_0040cb84, 3), iVar1 != 0))
	goto LAB_00401c1b;
    }
  func_0x004014a0 ("Full documentation at: <%s%s>\n",
		   "https://www.gnu.org/software/coreutils/", unaff_RBP);
  uVar3 = 0x40caff;
  if (unaff_RBP == pcVar5)
    {
      uVar3 = 0x40cb1c;
    }
LAB_00401c55:
  ;
  do
    {
      func_0x004014a0
	("or available locally via: info \'(coreutils) %s%s\'\n", pcVar5,
	 uVar3);
    LAB_00401c67:
      ;
      func_0x004017b0 ((ulong) uParm1);
    LAB_00401c6f:
      ;
      func_0x004014a0 ("Full documentation at: <%s%s>\n",
		       "https://www.gnu.org/software/coreutils/", unaff_RBP);
      uVar3 = 0x40cb1c;
      pcVar5 = unaff_RBP;
    }
  while (true);
}

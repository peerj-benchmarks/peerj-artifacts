
#include "comm.h"

long DAT_0061151_0_1_;
long DAT_0061151_1_1_;
long null_ARRAY_0061144_0_8_;
long null_ARRAY_0061144_8_8_;
long null_ARRAY_0061164_0_8_;
long null_ARRAY_0061164_16_8_;
long null_ARRAY_0061164_24_8_;
long null_ARRAY_0061164_32_8_;
long null_ARRAY_0061164_40_8_;
long null_ARRAY_0061164_48_8_;
long null_ARRAY_0061164_8_8_;
long null_ARRAY_0061168_0_4_;
long null_ARRAY_0061168_16_8_;
long null_ARRAY_0061168_4_4_;
long null_ARRAY_0061168_8_4_;
long local_68_2_2_;
long local_6b_1_3_;
long local_6c_4_4_;
long local_6e_4_4_;
long local_6f_4_4_;
long local_70_0_2_;
long local_70_4_4_;
long local_71_0_1_;
long local_71_4_4_;
long local_72_4_4_;
long DAT_00000010;
long DAT_0040df54;
long DAT_0040df59;
long DAT_0040dfdd;
long DAT_0040e005;
long DAT_0040e081;
long DAT_0040e1d8;
long DAT_0040e220;
long DAT_0040e224;
long DAT_0040e228;
long DAT_0040e22b;
long DAT_0040e22d;
long DAT_0040e231;
long DAT_0040e235;
long DAT_0040e9eb;
long DAT_0040ee0a;
long DAT_0040ef11;
long DAT_0040ef17;
long DAT_0040ef29;
long DAT_0040ef2a;
long DAT_0040ef48;
long DAT_0040efce;
long DAT_00611000;
long DAT_00611010;
long DAT_00611020;
long DAT_006113e8;
long DAT_006113f8;
long DAT_00611450;
long DAT_00611454;
long DAT_00611458;
long DAT_0061145c;
long DAT_00611480;
long DAT_00611488;
long DAT_00611490;
long DAT_006114a0;
long DAT_006114a8;
long DAT_006114c0;
long DAT_006114c8;
long DAT_00611510;
long DAT_00611518;
long DAT_0061151c;
long DAT_0061151d;
long DAT_0061151f;
long DAT_00611520;
long DAT_00611521;
long DAT_00611522;
long DAT_00611523;
long DAT_00611528;
long DAT_00611530;
long DAT_00611538;
long DAT_00611678;
long DAT_006116b8;
long DAT_006116c0;
long DAT_006116c8;
long DAT_006116d8;
long fde_0040fb90;
long null_ARRAY_0040e0c0;
long null_ARRAY_0040f3e0;
long null_ARRAY_0040f620;
long null_ARRAY_00611440;
long null_ARRAY_006114e0;
long null_ARRAY_00611540;
long null_ARRAY_00611640;
long null_ARRAY_00611680;
long PTR_DAT_006113e0;
long PTR_DAT_006113f0;
long PTR_null_ARRAY_00611438;
void
FUN_004021fd (uint uParm1)
{
  undefined8 uVar1;
  int iVar2;
  long lVar3;
  undefined1 *puVar4;
  undefined **ppuVar5;
  undefined *puVar6;
  undefined *local_88;
  char *local_80;
  undefined8 local_78;
  char *local_70;
  char *local_68;
  char *local_60;
  char *local_58;
  char *local_50;
  char *local_48;
  char *local_40;
  char *local_38;
  char *local_30;
  undefined8 local_28;
  undefined8 local_20;

  ppuVar5 = &local_88;
  if (uParm1 != 0)
    {
      func_0x00401700 (DAT_006114a0,
		       "Try \'%s --help\' for more information.\n",
		       DAT_00611538);
      goto LAB_0040243c;
    }
  func_0x00401590 ("Usage: %s [OPTION]... FILE1 FILE2\n", DAT_00611538);
  uVar1 = DAT_00611480;
  func_0x00401710 ("Compare sorted files FILE1 and FILE2 line by line.\n",
		   DAT_00611480);
  func_0x00401710
    ("\nWhen FILE1 or FILE2 (not both) is -, read standard input.\n", uVar1);
  func_0x00401710
    ("\nWith no options, produce three-column output.  Column one contains\nlines unique to FILE1, column two contains lines unique to FILE2,\nand column three contains lines common to both files.\n",
     uVar1);
  func_0x00401710
    ("\n  -1              suppress column 1 (lines unique to FILE1)\n  -2              suppress column 2 (lines unique to FILE2)\n  -3              suppress column 3 (lines that appear in both files)\n",
     uVar1);
  func_0x00401710
    ("\n  --check-order     check that the input is correctly sorted, even\n                      if all input lines are pairable\n  --nocheck-order   do not check that the input is correctly sorted\n",
     uVar1);
  func_0x00401710 ("  --output-delimiter=STR  separate columns with STR\n",
		   uVar1);
  func_0x00401710 ("  --total           output a summary\n", uVar1);
  func_0x00401710
    ("  -z, --zero-terminated    line delimiter is NUL, not newline\n",
     uVar1);
  func_0x00401710 ("      --help     display this help and exit\n", uVar1);
  func_0x00401710 ("      --version  output version information and exit\n",
		   uVar1);
  func_0x00401710
    ("\nNote, comparisons honor the rules specified by \'LC_COLLATE\'.\n",
     uVar1);
  func_0x00401590
    ("\nExamples:\n  %s -12 file1 file2  Print only lines present in both file1 and file2.\n  %s -3 file1 file2  Print lines in file1 not in file2, and vice versa.\n",
     DAT_00611538, DAT_00611538);
  local_88 = &DAT_0040df59;
  local_80 = "test invocation";
  local_78 = 0x40dfbc;
  local_70 = "Multi-call invocation";
  local_68 = "sha224sum";
  local_60 = "sha2 utilities";
  local_58 = "sha256sum";
  local_50 = "sha2 utilities";
  local_48 = "sha384sum";
  local_40 = "sha2 utilities";
  local_38 = "sha512sum";
  local_30 = "sha2 utilities";
  local_28 = 0;
  local_20 = 0;
  puVar6 = &DAT_0040df59;
  do
    {
      iVar2 = func_0x00401810 (&DAT_0040df54, puVar6);
      if (iVar2 == 0)
	break;
      ppuVar5 = ppuVar5 + 2;
      puVar6 = *ppuVar5;
    }
  while (puVar6 != (undefined *) 0x0);
  puVar6 = ppuVar5[1];
  if (puVar6 == (undefined *) 0x0)
    {
      func_0x00401590 ("\n%s online help: <%s>\n", "GNU coreutils",
		       "https://www.gnu.org/software/coreutils/");
      lVar3 = func_0x00401870 (5, 0);
      if (lVar3 == 0)
	goto LAB_00402443;
      iVar2 = func_0x00401780 (lVar3, &DAT_0040dfdd, 3);
      if (iVar2 == 0)
	{
	  func_0x00401590 ("Full documentation at: <%s%s>\n",
			   "https://www.gnu.org/software/coreutils/",
			   &DAT_0040df54);
	  puVar6 = &DAT_0040df54;
	  puVar4 = (undefined1 *) 0x40df75;
	  goto LAB_0040242a;
	}
      puVar6 = &DAT_0040df54;
    LAB_004023e8:
      ;
      func_0x00401590
	("Report %s translation bugs to <https://translationproject.org/team/>\n",
	 &DAT_0040df54);
    }
  else
    {
      func_0x00401590 ("\n%s online help: <%s>\n", "GNU coreutils",
		       "https://www.gnu.org/software/coreutils/");
      lVar3 = func_0x00401870 (5, 0);
      if ((lVar3 != 0)
	  && (iVar2 = func_0x00401780 (lVar3, &DAT_0040dfdd, 3), iVar2 != 0))
	goto LAB_004023e8;
    }
  func_0x00401590 ("Full documentation at: <%s%s>\n",
		   "https://www.gnu.org/software/coreutils/", &DAT_0040df54);
  puVar4 = &DAT_0040e081;
  if (puVar6 == &DAT_0040df54)
    {
      puVar4 = (undefined1 *) 0x40df75;
    }
LAB_0040242a:
  ;
  do
    {
      func_0x00401590
	("or available locally via: info \'(coreutils) %s%s\'\n", puVar6,
	 puVar4);
    LAB_0040243c:
      ;
      func_0x004018d0 ((ulong) uParm1);
    LAB_00402443:
      ;
      func_0x00401590 ("Full documentation at: <%s%s>\n",
		       "https://www.gnu.org/software/coreutils/",
		       &DAT_0040df54);
      puVar6 = &DAT_0040df54;
      puVar4 = (undefined1 *) 0x40df75;
    }
  while (true);
}

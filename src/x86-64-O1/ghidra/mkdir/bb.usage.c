
#include "mkdir.h"

long null_ARRAY_006124a_0_8_;
long null_ARRAY_006124a_8_8_;
long null_ARRAY_0061268_0_8_;
long null_ARRAY_0061268_16_8_;
long null_ARRAY_0061268_24_8_;
long null_ARRAY_0061268_32_8_;
long null_ARRAY_0061268_40_8_;
long null_ARRAY_0061268_48_8_;
long null_ARRAY_0061268_8_8_;
long null_ARRAY_006126c_0_4_;
long null_ARRAY_006126c_16_8_;
long null_ARRAY_006126c_4_4_;
long null_ARRAY_006126c_8_4_;
long local_1_1_1_;
long local_68_2_2_;
long local_6b_1_3_;
long local_6c_4_4_;
long local_6e_4_4_;
long local_6f_4_4_;
long local_70_0_2_;
long local_70_4_4_;
long local_71_0_1_;
long local_71_4_4_;
long local_72_4_4_;
long DAT_00000010;
long DAT_0040eb89;
long DAT_0040eb8d;
long DAT_0040eb8f;
long DAT_0040ec13;
long DAT_0040edb8;
long DAT_0040ef70;
long DAT_0040ef74;
long DAT_0040ef78;
long DAT_0040ef7b;
long DAT_0040ef7d;
long DAT_0040ef81;
long DAT_0040ef85;
long DAT_0040f8c1;
long DAT_0040fc55;
long DAT_0040fc57;
long DAT_0040fd59;
long DAT_0040fd5f;
long DAT_0040fd71;
long DAT_0040fd72;
long DAT_0040fd90;
long DAT_0040fd94;
long DAT_0040fe1e;
long DAT_00612000;
long DAT_00612010;
long DAT_00612020;
long DAT_00612448;
long DAT_006124b0;
long DAT_006124b4;
long DAT_006124b8;
long DAT_006124bc;
long DAT_006124c0;
long DAT_006124d0;
long DAT_006124e0;
long DAT_006124e8;
long DAT_00612500;
long DAT_00612508;
long DAT_00612550;
long DAT_00612558;
long DAT_00612560;
long DAT_006126f8;
long DAT_00612700;
long DAT_00612708;
long DAT_00612710;
long DAT_00612720;
long fde_00410a40;
long null_ARRAY_0040ecc0;
long null_ARRAY_00410240;
long null_ARRAY_00410480;
long null_ARRAY_006124a0;
long null_ARRAY_00612520;
long null_ARRAY_00612580;
long null_ARRAY_00612680;
long null_ARRAY_006126c0;
long PTR_DAT_00612440;
long PTR_null_ARRAY_00612498;
void
FUN_004020c2 (uint uParm1)
{
  int iVar1;
  long lVar2;
  undefined8 uVar3;
  undefined **ppuVar4;
  undefined *puVar5;
  undefined *local_88;
  char *local_80;
  undefined8 local_78;
  char *local_70;
  char *local_68;
  char *local_60;
  char *local_58;
  char *local_50;
  char *local_48;
  char *local_40;
  char *local_38;
  char *local_30;
  undefined8 local_28;
  undefined8 local_20;

  ppuVar4 = &local_88;
  if (uParm1 != 0)
    {
      func_0x00401a10 (DAT_006124e0,
		       "Try \'%s --help\' for more information.\n",
		       DAT_00612560);
      goto LAB_004022a7;
    }
  func_0x00401830 ("Usage: %s [OPTION]... DIRECTORY...\n", DAT_00612560);
  uVar3 = DAT_006124c0;
  func_0x00401a20
    ("Create the DIRECTORY(ies), if they do not already exist.\n",
     DAT_006124c0);
  func_0x00401a20
    ("\nMandatory arguments to long options are mandatory for short options too.\n",
     uVar3);
  func_0x00401a20
    ("  -m, --mode=MODE   set file mode (as in chmod), not a=rwx - umask\n  -p, --parents     no error if existing, make parent directories as needed\n  -v, --verbose     print a message for each created directory\n",
     uVar3);
  func_0x00401a20
    ("  -Z                   set SELinux security context of each created directory\n                         to the default type\n      --context[=CTX]  like -Z, or if CTX is specified then set the SELinux\n                         or SMACK security context to CTX\n",
     uVar3);
  func_0x00401a20 ("      --help     display this help and exit\n", uVar3);
  func_0x00401a20 ("      --version  output version information and exit\n",
		   uVar3);
  local_88 = &DAT_0040eb8f;
  local_80 = "test invocation";
  local_78 = 0x40ebf2;
  local_70 = "Multi-call invocation";
  local_68 = "sha224sum";
  local_60 = "sha2 utilities";
  local_58 = "sha256sum";
  local_50 = "sha2 utilities";
  local_48 = "sha384sum";
  local_40 = "sha2 utilities";
  local_38 = "sha512sum";
  local_30 = "sha2 utilities";
  local_28 = 0;
  local_20 = 0;
  puVar5 = &DAT_0040eb8f;
  do
    {
      iVar1 = func_0x00401b50 (&DAT_0040eb89, puVar5);
      if (iVar1 == 0)
	break;
      ppuVar4 = ppuVar4 + 2;
      puVar5 = *ppuVar4;
    }
  while (puVar5 != (undefined *) 0x0);
  puVar5 = ppuVar4[1];
  if (puVar5 == (undefined *) 0x0)
    {
      func_0x00401830 ("\n%s online help: <%s>\n", "GNU coreutils",
		       "https://www.gnu.org/software/coreutils/");
      lVar2 = func_0x00401bb0 (5, 0);
      if (lVar2 == 0)
	goto LAB_004022ae;
      iVar1 = func_0x00401aa0 (lVar2, &DAT_0040ec13, 3);
      if (iVar1 == 0)
	{
	  func_0x00401830 ("Full documentation at: <%s%s>\n",
			   "https://www.gnu.org/software/coreutils/",
			   &DAT_0040eb89);
	  puVar5 = &DAT_0040eb89;
	  uVar3 = 0x40ebab;
	  goto LAB_00402295;
	}
      puVar5 = &DAT_0040eb89;
    LAB_00402253:
      ;
      func_0x00401830
	("Report %s translation bugs to <https://translationproject.org/team/>\n",
	 &DAT_0040eb89);
    }
  else
    {
      func_0x00401830 ("\n%s online help: <%s>\n", "GNU coreutils",
		       "https://www.gnu.org/software/coreutils/");
      lVar2 = func_0x00401bb0 (5, 0);
      if ((lVar2 != 0)
	  && (iVar1 = func_0x00401aa0 (lVar2, &DAT_0040ec13, 3), iVar1 != 0))
	goto LAB_00402253;
    }
  func_0x00401830 ("Full documentation at: <%s%s>\n",
		   "https://www.gnu.org/software/coreutils/", &DAT_0040eb89);
  uVar3 = 0x40fd8f;
  if (puVar5 == &DAT_0040eb89)
    {
      uVar3 = 0x40ebab;
    }
LAB_00402295:
  ;
  do
    {
      func_0x00401830
	("or available locally via: info \'(coreutils) %s%s\'\n", puVar5,
	 uVar3);
    LAB_004022a7:
      ;
      func_0x00401c00 ((ulong) uParm1);
    LAB_004022ae:
      ;
      func_0x00401830 ("Full documentation at: <%s%s>\n",
		       "https://www.gnu.org/software/coreutils/",
		       &DAT_0040eb89);
      puVar5 = &DAT_0040eb89;
      uVar3 = 0x40ebab;
    }
  while (true);
}

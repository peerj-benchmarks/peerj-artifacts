
#include "fmt.h"

long null_ARRAY_0061166_0_8_;
long null_ARRAY_0061166_8_8_;
long null_ARRAY_0061178_32_8_;
long null_ARRAY_0061c90_0_8_;
long null_ARRAY_0061c90_16_8_;
long null_ARRAY_0061c90_24_8_;
long null_ARRAY_0061c90_32_8_;
long null_ARRAY_0061c90_40_8_;
long null_ARRAY_0061c90_48_8_;
long null_ARRAY_0061c90_8_8_;
long null_ARRAY_0061c94_0_4_;
long null_ARRAY_0061c94_16_8_;
long null_ARRAY_0061c94_4_4_;
long null_ARRAY_0061c94_8_4_;
long local_68_2_2_;
long local_6b_1_3_;
long local_6c_4_4_;
long local_6e_4_4_;
long local_6f_4_4_;
long local_70_0_2_;
long local_70_4_4_;
long local_71_0_1_;
long local_71_4_4_;
long local_72_4_4_;
long DAT_00000010;
long DAT_0040df06;
long DAT_0040df09;
long DAT_0040df0b;
long DAT_0040df2b;
long DAT_0040df2f;
long DAT_0040dfb3;
long DAT_0040e798;
long DAT_0040e79c;
long DAT_0040e7a0;
long DAT_0040e7a3;
long DAT_0040e7a7;
long DAT_0040e7ab;
long DAT_0040ef2b;
long DAT_0040f648;
long DAT_0040f751;
long DAT_0040f757;
long DAT_0040f769;
long DAT_0040f76a;
long DAT_0040f788;
long DAT_0040f78c;
long DAT_0040f816;
long DAT_00611218;
long DAT_00611228;
long DAT_00611238;
long DAT_00611608;
long DAT_00611670;
long DAT_00611674;
long DAT_00611678;
long DAT_0061167c;
long DAT_00611680;
long DAT_00611688;
long DAT_00611690;
long DAT_006116a0;
long DAT_006116a8;
long DAT_006116c0;
long DAT_006116c8;
long DAT_00611740;
long DAT_00611744;
long DAT_00611748;
long DAT_0061174c;
long DAT_00611750;
long DAT_00611754;
long DAT_00611758;
long DAT_00611760;
long DAT_0061b3c0;
long DAT_0061c788;
long DAT_0061c78c;
long DAT_0061c790;
long DAT_0061c794;
long DAT_0061c798;
long DAT_0061c79c;
long DAT_0061c7a0;
long DAT_0061c7a8;
long DAT_0061c7b0;
long DAT_0061c7b1;
long DAT_0061c7b2;
long DAT_0061c7b3;
long DAT_0061c7b8;
long DAT_0061c7c0;
long DAT_0061c7c8;
long DAT_0061c978;
long DAT_0061c980;
long DAT_0061c988;
long DAT_0061c998;
long fde_004103e8;
long null_ARRAY_0040e600;
long null_ARRAY_0040fc20;
long null_ARRAY_0040fe60;
long null_ARRAY_00611660;
long null_ARRAY_006116e0;
long null_ARRAY_00611780;
long null_ARRAY_0061b400;
long null_ARRAY_0061c800;
long null_ARRAY_0061c900;
long null_ARRAY_0061c940;
long PTR_DAT_00611600;
long PTR_null_ARRAY_00611658;
void
FUN_004027c7 (uint uParm1)
{
  int iVar1;
  long lVar2;
  undefined8 uVar3;
  undefined **ppuVar4;
  undefined *puVar5;
  undefined *local_88;
  char *local_80;
  undefined8 local_78;
  char *local_70;
  char *local_68;
  char *local_60;
  char *local_58;
  char *local_50;
  char *local_48;
  char *local_40;
  char *local_38;
  char *local_30;
  undefined8 local_28;
  undefined8 local_20;

  ppuVar4 = &local_88;
  if (uParm1 != 0)
    {
      func_0x004017f0 (DAT_006116a0,
		       "Try \'%s --help\' for more information.\n",
		       DAT_0061c7c8);
      goto LAB_004029b9;
    }
  func_0x00401650 ("Usage: %s [-WIDTH] [OPTION]... [FILE]...\n",
		   DAT_0061c7c8);
  uVar3 = DAT_00611680;
  func_0x00401800
    ("Reformat each paragraph in the FILE(s), writing to standard output.\nThe option -WIDTH is an abbreviated form of --width=DIGITS.\n",
     DAT_00611680);
  func_0x00401800
    ("\nWith no FILE, or when FILE is -, read standard input.\n", uVar3);
  func_0x00401800
    ("\nMandatory arguments to long options are mandatory for short options too.\n",
     uVar3);
  func_0x00401800
    ("  -c, --crown-margin        preserve indentation of first two lines\n  -p, --prefix=STRING       reformat only lines beginning with STRING,\n                              reattaching the prefix to reformatted lines\n  -s, --split-only          split long lines, but do not refill\n",
     uVar3);
  func_0x00401800
    ("  -t, --tagged-paragraph    indentation of first line different from second\n  -u, --uniform-spacing     one space between words, two after sentences\n  -w, --width=WIDTH         maximum line width (default of 75 columns)\n  -g, --goal=WIDTH          goal width (default of 93% of width)\n",
     uVar3);
  func_0x00401800 ("      --help     display this help and exit\n", uVar3);
  func_0x00401800 ("      --version  output version information and exit\n",
		   uVar3);
  local_88 = &DAT_0040df2f;
  local_80 = "test invocation";
  local_78 = 0x40df92;
  local_70 = "Multi-call invocation";
  local_68 = "sha224sum";
  local_60 = "sha2 utilities";
  local_58 = "sha256sum";
  local_50 = "sha2 utilities";
  local_48 = "sha384sum";
  local_40 = "sha2 utilities";
  local_38 = "sha512sum";
  local_30 = "sha2 utilities";
  local_28 = 0;
  local_20 = 0;
  puVar5 = &DAT_0040df2f;
  do
    {
      iVar1 = func_0x00401900 (&DAT_0040df2b, puVar5);
      if (iVar1 == 0)
	break;
      ppuVar4 = ppuVar4 + 2;
      puVar5 = *ppuVar4;
    }
  while (puVar5 != (undefined *) 0x0);
  puVar5 = ppuVar4[1];
  if (puVar5 == (undefined *) 0x0)
    {
      func_0x00401650 ("\n%s online help: <%s>\n", "GNU coreutils",
		       "https://www.gnu.org/software/coreutils/");
      lVar2 = func_0x00401960 (5, 0);
      if (lVar2 == 0)
	goto LAB_004029c0;
      iVar1 = func_0x00401860 (lVar2, &DAT_0040dfb3, 3);
      if (iVar1 == 0)
	{
	  func_0x00401650 ("Full documentation at: <%s%s>\n",
			   "https://www.gnu.org/software/coreutils/",
			   &DAT_0040df2b);
	  puVar5 = &DAT_0040df2b;
	  uVar3 = 0x40df4b;
	  goto LAB_004029a7;
	}
      puVar5 = &DAT_0040df2b;
    LAB_00402965:
      ;
      func_0x00401650
	("Report %s translation bugs to <https://translationproject.org/team/>\n",
	 &DAT_0040df2b);
    }
  else
    {
      func_0x00401650 ("\n%s online help: <%s>\n", "GNU coreutils",
		       "https://www.gnu.org/software/coreutils/");
      lVar2 = func_0x00401960 (5, 0);
      if ((lVar2 != 0)
	  && (iVar1 = func_0x00401860 (lVar2, &DAT_0040dfb3, 3), iVar1 != 0))
	goto LAB_00402965;
    }
  func_0x00401650 ("Full documentation at: <%s%s>\n",
		   "https://www.gnu.org/software/coreutils/", &DAT_0040df2b);
  uVar3 = 0x40f787;
  if (puVar5 == &DAT_0040df2b)
    {
      uVar3 = 0x40df4b;
    }
LAB_004029a7:
  ;
  do
    {
      func_0x00401650
	("or available locally via: info \'(coreutils) %s%s\'\n", puVar5,
	 uVar3);
    LAB_004029b9:
      ;
      func_0x004019c0 ((ulong) uParm1);
    LAB_004029c0:
      ;
      func_0x00401650 ("Full documentation at: <%s%s>\n",
		       "https://www.gnu.org/software/coreutils/",
		       &DAT_0040df2b);
      puVar5 = &DAT_0040df2b;
      uVar3 = 0x40df4b;
    }
  while (true);
}

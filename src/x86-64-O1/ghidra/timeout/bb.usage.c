
#include "timeout.h"

long null_ARRAY_006114e_0_8_;
long null_ARRAY_006114e_8_8_;
long null_ARRAY_006118c_0_8_;
long null_ARRAY_006118c_16_8_;
long null_ARRAY_006118c_24_8_;
long null_ARRAY_006118c_32_8_;
long null_ARRAY_006118c_40_8_;
long null_ARRAY_006118c_48_8_;
long null_ARRAY_006118c_8_8_;
long null_ARRAY_0061190_0_4_;
long null_ARRAY_0061190_16_8_;
long null_ARRAY_0061190_4_4_;
long null_ARRAY_0061190_8_4_;
long local_68_2_2_;
long local_6b_1_3_;
long local_6c_4_4_;
long local_6e_4_4_;
long local_6f_4_4_;
long local_70_0_2_;
long local_70_4_4_;
long local_71_0_1_;
long local_71_4_4_;
long local_72_4_4_;
long bRam0000000025681579;
long _bVar15;
long cRam0000000000413833;
long cRam000000001940f989;
long cRam000000006473239a;
long cRam00000000647327ee;
long cRam000000007365f563;
long DAT_00000010;
long DAT_0040dd6d;
long DAT_0040dd8d;
long DAT_0040de11;
long DAT_0040e843;
long DAT_0040e8b0;
long DAT_0040e8b4;
long DAT_0040e8b8;
long DAT_0040e8bb;
long DAT_0040e8bd;
long DAT_0040e8c1;
long DAT_0040e8c5;
long DAT_0040f06b;
long DAT_0040f448;
long DAT_0040f4c8;
long DAT_0040f519;
long DAT_0040f531;
long DAT_0040f532;
long DAT_0040f550;
long DAT_0040f554;
long DAT_0040f560;
long DAT_0040f564;
long DAT_00412e9a;
long DAT_00611000;
long DAT_00611010;
long DAT_00611020;
long DAT_00611480;
long DAT_00611490;
long DAT_006114f0;
long DAT_006114f4;
long DAT_006114f8;
long DAT_006114fc;
long _DAT_00611500;
long DAT_00611500;
long DAT_00611504;
long DAT_0061150c;
long DAT_006116c0;
long DAT_006116d0;
long DAT_006116e0;
long DAT_006116e8;
long DAT_00611700;
long DAT_00611708;
long DAT_00611750;
long DAT_00611758;
long DAT_00611759;
long DAT_0061175a;
long DAT_00611760;
long DAT_00611768;
long DAT_0061176c;
long DAT_00611770;
long DAT_00611778;
long DAT_00611780;
long DAT_00611788;
long DAT_00611938;
long DAT_00611940;
long DAT_00611948;
long DAT_00611958;
long DAT_6f612ea0;
long DAT_6f612f68;
long fde_00410188;
long int6;
long int7;
long null_ARRAY_0040e700;
long null_ARRAY_0040f980;
long null_ARRAY_0040fc30;
long null_ARRAY_006114e0;
long null_ARRAY_00611720;
long null_ARRAY_006117c0;
long null_ARRAY_006118c0;
long null_ARRAY_00611900;
long PTR_DAT_00611488;
long PTR_null_ARRAY_006114d8;
long register0x00000020;
long stack0x00000008;
long uint7;
long uRama3b6000000000040;
long uRama3cc000000000040;
void
FUN_004022ec (uint uParm1)
{
  int iVar1;
  long lVar2;
  undefined8 uVar3;
  undefined **ppuVar4;
  char *pcVar5;
  undefined *puVar6;
  undefined *local_88;
  char *local_80;
  undefined8 local_78;
  char *local_70;
  char *local_68;
  char *local_60;
  char *local_58;
  char *local_50;
  char *local_48;
  char *local_40;
  char *local_38;
  char *local_30;
  undefined8 local_28;
  undefined8 local_20;

  ppuVar4 = &local_88;
  if (uParm1 != 0)
    {
      func_0x00401ba0 (DAT_006116e0,
		       "Try \'%s --help\' for more information.\n",
		       DAT_00611788);
      goto LAB_004024ee;
    }
  func_0x00401980
    ("Usage: %s [OPTION] DURATION COMMAND [ARG]...\n  or:  %s [OPTION]\n",
     DAT_00611788, DAT_00611788);
  uVar3 = DAT_006116c0;
  func_0x00401bc0
    ("Start COMMAND, and kill it if still running after DURATION.\n",
     DAT_006116c0);
  func_0x00401bc0
    ("\nMandatory arguments to long options are mandatory for short options too.\n",
     uVar3);
  func_0x00401bc0
    ("      --preserve-status\n                 exit with the same status as COMMAND, even when the\n                   command times out\n      --foreground\n                 when not running timeout directly from a shell prompt,\n                   allow COMMAND to read from the TTY and get TTY signals;\n                   in this mode, children of COMMAND will not be timed out\n  -k, --kill-after=DURATION\n                 also send a KILL signal if COMMAND is still running\n                   this long after the initial signal was sent\n  -s, --signal=SIGNAL\n                 specify the signal to be sent on timeout;\n                   SIGNAL may be a name like \'HUP\' or a number;\n                   see \'kill -l\' for a list of signals\n",
     uVar3);
  func_0x00401bc0
    ("  -v, --verbose  diagnose to stderr any signal sent upon timeout\n",
     uVar3);
  func_0x00401bc0 ("      --help     display this help and exit\n", uVar3);
  func_0x00401bc0 ("      --version  output version information and exit\n",
		   uVar3);
  func_0x00401bc0
    ("\nDURATION is a floating point number with an optional suffix:\n\'s\' for seconds (the default), \'m\' for minutes, \'h\' for hours or \'d\' for days.\n",
     uVar3);
  func_0x00401bc0
    ("\nIf the command times out, and --preserve-status is not set, then exit with\nstatus 124.  Otherwise, exit with the status of COMMAND.  If no signal\nis specified, send the TERM signal upon timeout.  The TERM signal kills\nany process that does not block or catch that signal.  It may be necessary\nto use the KILL (9) signal, since this signal cannot be caught, in which\ncase the exit status is 128+9 rather than 124.\n",
     uVar3);
  local_88 = &DAT_0040dd8d;
  local_80 = "test invocation";
  local_78 = 0x40ddf0;
  local_70 = "Multi-call invocation";
  local_68 = "sha224sum";
  local_60 = "sha2 utilities";
  local_58 = "sha256sum";
  local_50 = "sha2 utilities";
  local_48 = "sha384sum";
  local_40 = "sha2 utilities";
  local_38 = "sha512sum";
  local_30 = "sha2 utilities";
  local_28 = 0;
  local_20 = 0;
  puVar6 = &DAT_0040dd8d;
  do
    {
      iVar1 = func_0x00401cc0 ("timeout", puVar6);
      if (iVar1 == 0)
	break;
      ppuVar4 = ppuVar4 + 2;
      puVar6 = *ppuVar4;
    }
  while (puVar6 != (undefined *) 0x0);
  pcVar5 = ppuVar4[1];
  if (pcVar5 == (char *) 0x0)
    {
      func_0x00401980 ("\n%s online help: <%s>\n", "GNU coreutils",
		       "https://www.gnu.org/software/coreutils/");
      lVar2 = func_0x00401d30 (5, 0);
      if (lVar2 == 0)
	goto LAB_004024f5;
      iVar1 = func_0x00401c20 (lVar2, &DAT_0040de11, 3);
      if (iVar1 == 0)
	{
	  func_0x00401980 ("Full documentation at: <%s%s>\n",
			   "https://www.gnu.org/software/coreutils/",
			   "timeout");
	  pcVar5 = "timeout";
	  uVar3 = 0x40dda9;
	  goto LAB_004024dc;
	}
      pcVar5 = "timeout";
    LAB_0040249a:
      ;
      func_0x00401980
	("Report %s translation bugs to <https://translationproject.org/team/>\n",
	 "timeout");
    }
  else
    {
      func_0x00401980 ("\n%s online help: <%s>\n", "GNU coreutils",
		       "https://www.gnu.org/software/coreutils/");
      lVar2 = func_0x00401d30 (5, 0);
      if ((lVar2 != 0)
	  && (iVar1 = func_0x00401c20 (lVar2, &DAT_0040de11, 3), iVar1 != 0))
	goto LAB_0040249a;
    }
  func_0x00401980 ("Full documentation at: <%s%s>\n",
		   "https://www.gnu.org/software/coreutils/", "timeout");
  uVar3 = 0x40f54f;
  if (pcVar5 == "timeout")
    {
      uVar3 = 0x40dda9;
    }
LAB_004024dc:
  ;
  do
    {
      func_0x00401980
	("or available locally via: info \'(coreutils) %s%s\'\n", pcVar5,
	 uVar3);
    LAB_004024ee:
      ;
      func_0x00401d70 ((ulong) uParm1);
    LAB_004024f5:
      ;
      func_0x00401980 ("Full documentation at: <%s%s>\n",
		       "https://www.gnu.org/software/coreutils/", "timeout");
      pcVar5 = "timeout";
      uVar3 = 0x40dda9;
    }
  while (true);
}

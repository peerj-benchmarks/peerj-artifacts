
#include "link.h"

long null_ARRAY_0060f80_0_8_;
long null_ARRAY_0060f80_8_8_;
long null_ARRAY_0060fa0_0_8_;
long null_ARRAY_0060fa0_16_8_;
long null_ARRAY_0060fa0_24_8_;
long null_ARRAY_0060fa0_32_8_;
long null_ARRAY_0060fa0_40_8_;
long null_ARRAY_0060fa0_48_8_;
long null_ARRAY_0060fa0_8_8_;
long null_ARRAY_0060fa4_0_4_;
long null_ARRAY_0060fa4_16_8_;
long null_ARRAY_0060fa4_4_4_;
long null_ARRAY_0060fa4_8_4_;
long local_68_2_2_;
long local_6b_1_3_;
long local_6c_4_4_;
long local_6e_4_4_;
long local_6f_4_4_;
long local_70_0_2_;
long local_70_4_4_;
long local_71_0_1_;
long local_71_4_4_;
long local_72_4_4_;
long DAT_00000010;
long DAT_0040cbc0;
long DAT_0040cbc5;
long DAT_0040cc49;
long DAT_0040ced8;
long DAT_0040cfa0;
long DAT_0040cfa4;
long DAT_0040cfa8;
long DAT_0040cfab;
long DAT_0040cfad;
long DAT_0040cfb1;
long DAT_0040cfb5;
long DAT_0040d76b;
long DAT_0040db15;
long DAT_0040dc19;
long DAT_0040dc1f;
long DAT_0040dc31;
long DAT_0040dc32;
long DAT_0040dc50;
long DAT_0040dc54;
long DAT_0040dcde;
long DAT_0060f3e0;
long DAT_0060f3f0;
long DAT_0060f400;
long DAT_0060f7a8;
long DAT_0060f810;
long DAT_0060f814;
long DAT_0060f818;
long DAT_0060f81c;
long DAT_0060f840;
long DAT_0060f850;
long DAT_0060f860;
long DAT_0060f868;
long DAT_0060f880;
long DAT_0060f888;
long DAT_0060f8d0;
long DAT_0060f8d8;
long DAT_0060f8e0;
long DAT_0060fa78;
long DAT_0060fa80;
long DAT_0060fa88;
long DAT_0060fa98;
long fde_0040e848;
long null_ARRAY_0040cea0;
long null_ARRAY_0040cf00;
long null_ARRAY_0040e100;
long null_ARRAY_0040e340;
long null_ARRAY_0060f800;
long null_ARRAY_0060f8a0;
long null_ARRAY_0060f900;
long null_ARRAY_0060fa00;
long null_ARRAY_0060fa40;
long PTR_DAT_0060f7a0;
long PTR_null_ARRAY_0060f7f8;
void
FUN_00401a6e (uint uParm1)
{
  int iVar1;
  long lVar2;
  undefined8 uVar3;
  undefined **ppuVar4;
  undefined *puVar5;
  undefined *local_88;
  char *local_80;
  undefined8 local_78;
  char *local_70;
  char *local_68;
  char *local_60;
  char *local_58;
  char *local_50;
  char *local_48;
  char *local_40;
  char *local_38;
  char *local_30;
  undefined8 local_28;
  undefined8 local_20;

  ppuVar4 = &local_88;
  if (uParm1 != 0)
    {
      func_0x00401630 (DAT_0060f860,
		       "Try \'%s --help\' for more information.\n",
		       DAT_0060f8e0);
      goto LAB_00401c2f;
    }
  func_0x004014c0 ("Usage: %s FILE1 FILE2\n  or:  %s OPTION\n", DAT_0060f8e0,
		   DAT_0060f8e0);
  uVar3 = DAT_0060f840;
  func_0x00401640
    ("Call the link function to create a link named FILE2 to an existing FILE1.\n\n",
     DAT_0060f840);
  func_0x00401640 ("      --help     display this help and exit\n", uVar3);
  func_0x00401640 ("      --version  output version information and exit\n",
		   uVar3);
  local_88 = &DAT_0040cbc5;
  local_80 = "test invocation";
  local_78 = 0x40cc28;
  local_70 = "Multi-call invocation";
  local_68 = "sha224sum";
  local_60 = "sha2 utilities";
  local_58 = "sha256sum";
  local_50 = "sha2 utilities";
  local_48 = "sha384sum";
  local_40 = "sha2 utilities";
  local_38 = "sha512sum";
  local_30 = "sha2 utilities";
  local_28 = 0;
  local_20 = 0;
  puVar5 = &DAT_0040cbc5;
  do
    {
      iVar1 = func_0x00401720 (&DAT_0040cbc0, puVar5);
      if (iVar1 == 0)
	break;
      ppuVar4 = ppuVar4 + 2;
      puVar5 = *ppuVar4;
    }
  while (puVar5 != (undefined *) 0x0);
  puVar5 = ppuVar4[1];
  if (puVar5 == (undefined *) 0x0)
    {
      func_0x004014c0 ("\n%s online help: <%s>\n", "GNU coreutils",
		       "https://www.gnu.org/software/coreutils/");
      lVar2 = func_0x00401780 (5, 0);
      if (lVar2 == 0)
	goto LAB_00401c36;
      iVar1 = func_0x004016a0 (lVar2, &DAT_0040cc49, 3);
      if (iVar1 == 0)
	{
	  func_0x004014c0 ("Full documentation at: <%s%s>\n",
			   "https://www.gnu.org/software/coreutils/",
			   &DAT_0040cbc0);
	  puVar5 = &DAT_0040cbc0;
	  uVar3 = 0x40cbe1;
	  goto LAB_00401c1d;
	}
      puVar5 = &DAT_0040cbc0;
    LAB_00401bdb:
      ;
      func_0x004014c0
	("Report %s translation bugs to <https://translationproject.org/team/>\n",
	 &DAT_0040cbc0);
    }
  else
    {
      func_0x004014c0 ("\n%s online help: <%s>\n", "GNU coreutils",
		       "https://www.gnu.org/software/coreutils/");
      lVar2 = func_0x00401780 (5, 0);
      if ((lVar2 != 0)
	  && (iVar1 = func_0x004016a0 (lVar2, &DAT_0040cc49, 3), iVar1 != 0))
	goto LAB_00401bdb;
    }
  func_0x004014c0 ("Full documentation at: <%s%s>\n",
		   "https://www.gnu.org/software/coreutils/", &DAT_0040cbc0);
  uVar3 = 0x40dc4f;
  if (puVar5 == &DAT_0040cbc0)
    {
      uVar3 = 0x40cbe1;
    }
LAB_00401c1d:
  ;
  do
    {
      func_0x004014c0
	("or available locally via: info \'(coreutils) %s%s\'\n", puVar5,
	 uVar3);
    LAB_00401c2f:
      ;
      func_0x004017e0 ((ulong) uParm1);
    LAB_00401c36:
      ;
      func_0x004014c0 ("Full documentation at: <%s%s>\n",
		       "https://www.gnu.org/software/coreutils/",
		       &DAT_0040cbc0);
      puVar5 = &DAT_0040cbc0;
      uVar3 = 0x40cbe1;
    }
  while (true);
}

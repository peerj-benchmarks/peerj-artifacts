
#include "shred.h"

long null_ARRAY_0061594_0_8_;
long null_ARRAY_0061594_8_8_;
long null_ARRAY_00615b4_0_8_;
long null_ARRAY_00615b4_16_8_;
long null_ARRAY_00615b4_24_8_;
long null_ARRAY_00615b4_32_8_;
long null_ARRAY_00615b4_40_8_;
long null_ARRAY_00615b4_48_8_;
long null_ARRAY_00615b4_8_8_;
long null_ARRAY_00615b8_0_4_;
long null_ARRAY_00615b8_16_8_;
long null_ARRAY_00615b8_4_4_;
long null_ARRAY_00615b8_8_4_;
long local_4_0_5_;
long local_4_0_6_;
long local_4_0_7_;
long local_4_4_1_;
long local_4_4_4_;
long local_68_2_2_;
long local_6b_1_3_;
long local_6c_4_4_;
long local_6e_4_4_;
long local_6f_4_4_;
long local_70_0_2_;
long local_70_4_4_;
long local_71_0_1_;
long local_71_4_4_;
long local_72_4_4_;
long DAT_00000010;
long DAT_00410efa;
long DAT_00410f7e;
long DAT_00411095;
long DAT_004121ba;
long DAT_004121bc;
long DAT_004122d8;
long DAT_004122dc;
long DAT_004122e0;
long DAT_004122e3;
long DAT_004122e5;
long DAT_004122e9;
long DAT_004122ed;
long DAT_00412a77;
long DAT_00412a9a;
long DAT_004131a8;
long DAT_004132b1;
long DAT_004132b7;
long DAT_004132b9;
long DAT_004132ba;
long DAT_004132d8;
long DAT_004132dc;
long DAT_00413366;
long DAT_00615448;
long DAT_00615458;
long DAT_00615468;
long DAT_006158f0;
long DAT_00615950;
long DAT_00615954;
long DAT_00615958;
long DAT_0061595c;
long DAT_00615980;
long DAT_00615990;
long DAT_006159a0;
long DAT_006159a8;
long DAT_006159c0;
long DAT_006159c8;
long DAT_00615a10;
long DAT_00615a18;
long DAT_00615a20;
long DAT_00615a28;
long DAT_00615b78;
long DAT_00615bf8;
long DAT_00615c00;
long DAT_00615c08;
long DAT_00615c18;
long fde_00414028;
long null_ARRAY_00411ec0;
long null_ARRAY_00411fc0;
long null_ARRAY_00412120;
long null_ARRAY_00412140;
long null_ARRAY_00412258;
long null_ARRAY_00413780;
long null_ARRAY_004139b0;
long null_ARRAY_00615940;
long null_ARRAY_006159e0;
long null_ARRAY_00615a40;
long null_ARRAY_00615b40;
long null_ARRAY_00615b80;
long PTR_DAT_006158e0;
long PTR_FUN_006158e8;
long PTR_null_ARRAY_00615938;
long PTR_null_ARRAY_00615960;
void
FUN_0040329d (uint uParm1)
{
  int iVar1;
  long lVar2;
  undefined8 uVar3;
  undefined **ppuVar4;
  char *pcVar5;
  undefined *puVar6;
  undefined *local_88;
  char *local_80;
  undefined8 local_78;
  char *local_70;
  char *local_68;
  char *local_60;
  char *local_58;
  char *local_50;
  char *local_48;
  char *local_40;
  char *local_38;
  char *local_30;
  undefined8 local_28;
  undefined8 local_20;

  ppuVar4 = &local_88;
  if (uParm1 != 0)
    {
      func_0x00401e10 (DAT_006159a0,
		       "Try \'%s --help\' for more information.\n",
		       DAT_00615a28);
      goto LAB_004034e4;
    }
  func_0x00401bc0 ("Usage: %s [OPTION]... FILE...\n", DAT_00615a28);
  uVar3 = DAT_00615980;
  func_0x00401e20
    ("Overwrite the specified FILE(s) repeatedly, in order to make it harder\nfor even very expensive hardware probing to recover the data.\n",
     DAT_00615980);
  func_0x00401e20 ("\nIf FILE is -, shred standard output.\n", uVar3);
  func_0x00401e20
    ("\nMandatory arguments to long options are mandatory for short options too.\n",
     uVar3);
  func_0x00401bc0
    ("  -f, --force    change permissions to allow writing if necessary\n  -n, --iterations=N  overwrite N times instead of the default (%d)\n      --random-source=FILE  get random bytes from FILE\n  -s, --size=N   shred this many bytes (suffixes like K, M, G accepted)\n",
     3);
  func_0x00401e20
    ("  -u             deallocate and remove file after overwriting\n      --remove[=HOW]  like -u but give control on HOW to delete;  See below\n  -v, --verbose  show progress\n  -x, --exact    do not round file sizes up to the next full block;\n                   this is the default for non-regular files\n  -z, --zero     add a final overwrite with zeros to hide shredding\n",
     uVar3);
  func_0x00401e20 ("      --help     display this help and exit\n", uVar3);
  func_0x00401e20 ("      --version  output version information and exit\n",
		   uVar3);
  func_0x00401e20
    ("\nDelete FILE(s) if --remove (-u) is specified.  The default is not to remove\nthe files because it is common to operate on device files like /dev/hda,\nand those files usually should not be removed.\nThe optional HOW parameter indicates how to remove a directory entry:\n\'unlink\' => use a standard unlink call.\n\'wipe\' => also first obfuscate bytes in the name.\n\'wipesync\' => also sync each obfuscated byte to disk.\nThe default mode is \'wipesync\', but note it can be expensive.\n\n",
     uVar3);
  func_0x00401e20
    ("CAUTION: Note that shred relies on a very important assumption:\nthat the file system overwrites data in place.  This is the traditional\nway to do things, but many modern file system designs do not satisfy this\nassumption.  The following are examples of file systems on which shred is\nnot effective, or is not guaranteed to be effective in all file system modes:\n\n",
     uVar3);
  func_0x00401e20
    ("* log-structured or journaled file systems, such as those supplied with\nAIX and Solaris (and JFS, ReiserFS, XFS, Ext3, etc.)\n\n* file systems that write redundant data and carry on even if some writes\nfail, such as RAID-based file systems\n\n* file systems that make snapshots, such as Network Appliance\'s NFS server\n\n",
     uVar3);
  func_0x00401e20
    ("* file systems that cache in temporary locations, such as NFS\nversion 3 clients\n\n* compressed file systems\n\n",
     uVar3);
  func_0x00401e20
    ("In the case of ext3 file systems, the above disclaimer applies\n(and shred is thus of limited effectiveness) only in data=journal mode,\nwhich journals file data in addition to just metadata.  In both the\ndata=ordered (default) and data=writeback modes, shred works as usual.\nExt3 journaling modes can be changed by adding the data=something option\nto the mount options for a particular file system in the /etc/fstab file,\nas documented in the mount man page (man mount).\n\n",
     uVar3);
  func_0x00401e20
    ("In addition, file system backups and remote mirrors may contain copies\nof the file that cannot be removed, and that will allow a shredded file\nto be recovered later.\n",
     uVar3);
  local_88 = &DAT_00410efa;
  local_80 = "test invocation";
  local_78 = 0x410f5d;
  local_70 = "Multi-call invocation";
  local_68 = "sha224sum";
  local_60 = "sha2 utilities";
  local_58 = "sha256sum";
  local_50 = "sha2 utilities";
  local_48 = "sha384sum";
  local_40 = "sha2 utilities";
  local_38 = "sha512sum";
  local_30 = "sha2 utilities";
  local_28 = 0;
  local_20 = 0;
  puVar6 = &DAT_00410efa;
  do
    {
      iVar1 = func_0x00401f90 ("shred", puVar6);
      if (iVar1 == 0)
	break;
      ppuVar4 = ppuVar4 + 2;
      puVar6 = *ppuVar4;
    }
  while (puVar6 != (undefined *) 0x0);
  pcVar5 = ppuVar4[1];
  if (pcVar5 == (char *) 0x0)
    {
      func_0x00401bc0 ("\n%s online help: <%s>\n", "GNU coreutils",
		       "https://www.gnu.org/software/coreutils/");
      lVar2 = func_0x00401ff0 (5, 0);
      if (lVar2 == 0)
	goto LAB_004034eb;
      iVar1 = func_0x00401e90 (lVar2, &DAT_00410f7e, 3);
      if (iVar1 == 0)
	{
	  func_0x00401bc0 ("Full documentation at: <%s%s>\n",
			   "https://www.gnu.org/software/coreutils/",
			   "shred");
	  pcVar5 = "shred";
	  uVar3 = 0x410f16;
	  goto LAB_004034d2;
	}
      pcVar5 = "shred";
    LAB_00403490:
      ;
      func_0x00401bc0
	("Report %s translation bugs to <https://translationproject.org/team/>\n",
	 "shred");
    }
  else
    {
      func_0x00401bc0 ("\n%s online help: <%s>\n", "GNU coreutils",
		       "https://www.gnu.org/software/coreutils/");
      lVar2 = func_0x00401ff0 (5, 0);
      if ((lVar2 != 0)
	  && (iVar1 = func_0x00401e90 (lVar2, &DAT_00410f7e, 3), iVar1 != 0))
	goto LAB_00403490;
    }
  func_0x00401bc0 ("Full documentation at: <%s%s>\n",
		   "https://www.gnu.org/software/coreutils/", "shred");
  uVar3 = 0x4132d7;
  if (pcVar5 == "shred")
    {
      uVar3 = 0x410f16;
    }
LAB_004034d2:
  ;
  do
    {
      func_0x00401bc0
	("or available locally via: info \'(coreutils) %s%s\'\n", pcVar5,
	 uVar3);
    LAB_004034e4:
      ;
      func_0x00402060 ((ulong) uParm1);
    LAB_004034eb:
      ;
      func_0x00401bc0 ("Full documentation at: <%s%s>\n",
		       "https://www.gnu.org/software/coreutils/", "shred");
      pcVar5 = "shred";
      uVar3 = 0x410f16;
    }
  while (true);
}

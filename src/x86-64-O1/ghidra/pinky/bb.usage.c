
#include "pinky.h"

long null_ARRAY_00610a6_0_8_;
long null_ARRAY_00610a6_8_8_;
long null_ARRAY_00610bc_0_4_;
long null_ARRAY_00610d4_0_8_;
long null_ARRAY_00610d4_16_8_;
long null_ARRAY_00610d4_24_8_;
long null_ARRAY_00610d4_32_8_;
long null_ARRAY_00610d4_40_8_;
long null_ARRAY_00610d4_48_8_;
long null_ARRAY_00610d4_8_8_;
long null_ARRAY_00610d8_0_4_;
long null_ARRAY_00610d8_16_8_;
long null_ARRAY_00610d8_4_4_;
long null_ARRAY_00610d8_8_4_;
long local_68_2_2_;
long local_6b_1_3_;
long local_6c_4_4_;
long local_6e_4_4_;
long local_6f_4_4_;
long local_70_0_2_;
long local_70_4_4_;
long local_71_0_1_;
long local_71_4_4_;
long local_72_4_4_;
long lVar8;
long var8;
long DAT_00000010;
long DAT_0040d9a8;
long DAT_0040d9bc;
long DAT_0040d9f6;
long DAT_0040da4c;
long DAT_0040dad0;
long DAT_0040db2c;
long DAT_0040db31;
long DAT_0040db3d;
long DAT_0040db48;
long DAT_0040db4d;
long DAT_0040e0b8;
long DAT_0040e100;
long DAT_0040e104;
long DAT_0040e108;
long DAT_0040e10b;
long DAT_0040e10d;
long DAT_0040e111;
long DAT_0040e115;
long DAT_0040e8ab;
long DAT_0040ec55;
long DAT_0040ed59;
long DAT_0040ed5f;
long DAT_0040ed71;
long DAT_0040ed72;
long DAT_0040ed90;
long DAT_0040ee16;
long DAT_006105e8;
long DAT_006105f8;
long DAT_00610608;
long DAT_00610a00;
long DAT_00610a01;
long DAT_00610a02;
long DAT_00610a03;
long DAT_00610a04;
long DAT_00610a05;
long DAT_00610a06;
long DAT_00610a07;
long DAT_00610a10;
long DAT_00610a70;
long DAT_00610a74;
long DAT_00610a78;
long DAT_00610a7c;
long DAT_00610ac0;
long DAT_00610ad0;
long DAT_00610ae0;
long DAT_00610ae8;
long DAT_00610b00;
long DAT_00610b08;
long DAT_00610ba8;
long DAT_00610bb0;
long DAT_00610bb8;
long DAT_00610bf0;
long DAT_00610bf8;
long DAT_00610c00;
long DAT_00610c08;
long DAT_00610df8;
long DAT_00610e00;
long DAT_00610e08;
long DAT_00610e18;
long fde_0040f980;
long null_ARRAY_0040e040;
long null_ARRAY_0040f220;
long null_ARRAY_0040f460;
long null_ARRAY_00610a60;
long null_ARRAY_00610b20;
long null_ARRAY_00610b60;
long null_ARRAY_00610b90;
long null_ARRAY_00610bc0;
long null_ARRAY_00610c40;
long null_ARRAY_00610d40;
long null_ARRAY_00610d80;
long null_ARRAY_00610dc0;
long PTR_DAT_00610a08;
long PTR_null_ARRAY_00610a58;
long PTR_null_ARRAY_00610a80;
void
FUN_004024e0 (uint uParm1)
{
  int iVar1;
  long lVar2;
  undefined8 uVar3;
  undefined **ppuVar4;
  char *pcVar5;
  undefined *puVar6;
  undefined *local_88;
  char *local_80;
  undefined8 local_78;
  char *local_70;
  char *local_68;
  char *local_60;
  char *local_58;
  char *local_50;
  char *local_48;
  char *local_40;
  char *local_38;
  char *local_30;
  undefined8 local_28;
  undefined8 local_20;

  ppuVar4 = &local_88;
  if (uParm1 != 0)
    {
      func_0x004018c0 (DAT_00610ae0,
		       "Try \'%s --help\' for more information.\n",
		       DAT_00610c08);
      goto LAB_004026bf;
    }
  func_0x00401740 ("Usage: %s [OPTION]... [USER]...\n", DAT_00610c08);
  uVar3 = DAT_00610ac0;
  func_0x004018e0
    ("\n  -l              produce long format output for the specified USERs\n  -b              omit the user\'s home directory and shell in long format\n  -h              omit the user\'s project file in long format\n  -p              omit the user\'s plan file in long format\n  -s              do short format output, this is the default\n",
     DAT_00610ac0);
  func_0x004018e0
    ("  -f              omit the line of column headings in short format\n  -w              omit the user\'s full name in short format\n  -i              omit the user\'s full name and remote host in short format\n  -q              omit the user\'s full name, remote host and idle time\n                  in short format\n",
     uVar3);
  func_0x004018e0 ("      --help     display this help and exit\n", uVar3);
  func_0x004018e0 ("      --version  output version information and exit\n",
		   uVar3);
  func_0x00401740
    ("\nA lightweight \'finger\' program;  print user information.\nThe utmp file will be %s.\n",
     "/dev/null/utmp");
  local_88 = &DAT_0040da4c;
  local_80 = "test invocation";
  local_78 = 0x40daaf;
  local_70 = "Multi-call invocation";
  local_68 = "sha224sum";
  local_60 = "sha2 utilities";
  local_58 = "sha256sum";
  local_50 = "sha2 utilities";
  local_48 = "sha384sum";
  local_40 = "sha2 utilities";
  local_38 = "sha512sum";
  local_30 = "sha2 utilities";
  local_28 = 0;
  local_20 = 0;
  puVar6 = &DAT_0040da4c;
  do
    {
      iVar1 = func_0x004019e0 ("pinky", puVar6);
      if (iVar1 == 0)
	break;
      ppuVar4 = ppuVar4 + 2;
      puVar6 = *ppuVar4;
    }
  while (puVar6 != (undefined *) 0x0);
  pcVar5 = ppuVar4[1];
  if (pcVar5 == (char *) 0x0)
    {
      func_0x00401740 ("\n%s online help: <%s>\n", "GNU coreutils",
		       "https://www.gnu.org/software/coreutils/");
      lVar2 = func_0x00401a50 (5, 0);
      if (lVar2 == 0)
	goto LAB_004026c6;
      iVar1 = func_0x00401930 (lVar2, &DAT_0040dad0, 3);
      if (iVar1 == 0)
	{
	  func_0x00401740 ("Full documentation at: <%s%s>\n",
			   "https://www.gnu.org/software/coreutils/",
			   "pinky");
	  pcVar5 = "pinky";
	  uVar3 = 0x40da68;
	  goto LAB_004026ad;
	}
      pcVar5 = "pinky";
    LAB_0040266b:
      ;
      func_0x00401740
	("Report %s translation bugs to <https://translationproject.org/team/>\n",
	 "pinky");
    }
  else
    {
      func_0x00401740 ("\n%s online help: <%s>\n", "GNU coreutils",
		       "https://www.gnu.org/software/coreutils/");
      lVar2 = func_0x00401a50 (5, 0);
      if ((lVar2 != 0)
	  && (iVar1 = func_0x00401930 (lVar2, &DAT_0040dad0, 3), iVar1 != 0))
	goto LAB_0040266b;
    }
  func_0x00401740 ("Full documentation at: <%s%s>\n",
		   "https://www.gnu.org/software/coreutils/", "pinky");
  uVar3 = 0x40ed8f;
  if (pcVar5 == "pinky")
    {
      uVar3 = 0x40da68;
    }
LAB_004026ad:
  ;
  do
    {
      func_0x00401740
	("or available locally via: info \'(coreutils) %s%s\'\n", pcVar5,
	 uVar3);
    LAB_004026bf:
      ;
      func_0x00401ae0 ((ulong) uParm1);
    LAB_004026c6:
      ;
      func_0x00401740 ("Full documentation at: <%s%s>\n",
		       "https://www.gnu.org/software/coreutils/", "pinky");
      pcVar5 = "pinky";
      uVar3 = 0x40da68;
    }
  while (true);
}


#include "readlink.h"

long null_ARRAY_0061248_0_8_;
long null_ARRAY_0061248_8_8_;
long null_ARRAY_0061268_0_8_;
long null_ARRAY_0061268_16_8_;
long null_ARRAY_0061268_24_8_;
long null_ARRAY_0061268_32_8_;
long null_ARRAY_0061268_40_8_;
long null_ARRAY_0061268_48_8_;
long null_ARRAY_0061268_8_8_;
long null_ARRAY_006126c_0_4_;
long null_ARRAY_006126c_16_8_;
long null_ARRAY_006126c_4_4_;
long null_ARRAY_006126c_8_4_;
long local_68_2_2_;
long local_6b_1_3_;
long local_6c_4_4_;
long local_6e_4_4_;
long local_6f_4_4_;
long local_70_0_2_;
long local_70_4_4_;
long local_71_0_1_;
long local_71_4_4_;
long local_72_4_4_;
long DAT_00000010;
long DAT_0040e989;
long DAT_0040ea0d;
long DAT_0040f268;
long DAT_0040f26e;
long DAT_0040f270;
long DAT_0040f274;
long DAT_0040f278;
long DAT_0040f27b;
long DAT_0040f27d;
long DAT_0040f281;
long DAT_0040f285;
long DAT_0040fa2b;
long DAT_0040fdd5;
long DAT_0040fde6;
long DAT_0040fde7;
long DAT_0040fee1;
long DAT_0040fee7;
long DAT_0040fef9;
long DAT_0040fefa;
long DAT_0040ff18;
long DAT_0040ff1c;
long DAT_0040ffa6;
long DAT_00612000;
long DAT_00612010;
long DAT_00612020;
long DAT_00612428;
long DAT_00612490;
long DAT_00612494;
long DAT_00612498;
long DAT_0061249c;
long DAT_006124c0;
long DAT_006124d0;
long DAT_006124e0;
long DAT_006124e8;
long DAT_00612500;
long DAT_00612508;
long DAT_00612550;
long DAT_00612551;
long DAT_00612558;
long DAT_00612560;
long DAT_00612568;
long DAT_006126f8;
long DAT_00612700;
long DAT_00612708;
long DAT_00612710;
long DAT_00612718;
long DAT_00612728;
long fde_00410bd8;
long null_ARRAY_0040f080;
long null_ARRAY_0040f200;
long null_ARRAY_004103c0;
long null_ARRAY_004105f0;
long null_ARRAY_00612480;
long null_ARRAY_00612520;
long null_ARRAY_00612580;
long null_ARRAY_00612680;
long null_ARRAY_006126c0;
long PTR_DAT_00612420;
long PTR_null_ARRAY_00612478;
void
FUN_00401dbe (uint uParm1)
{
  int iVar1;
  long lVar2;
  undefined8 uVar3;
  undefined **ppuVar4;
  char *pcVar5;
  undefined *puVar6;
  undefined *local_88;
  char *local_80;
  undefined8 local_78;
  char *local_70;
  char *local_68;
  char *local_60;
  char *local_58;
  char *local_50;
  char *local_48;
  char *local_40;
  char *local_38;
  char *local_30;
  undefined8 local_28;
  undefined8 local_20;

  ppuVar4 = &local_88;
  if (uParm1 != 0)
    {
      func_0x00401950 (DAT_006124e0,
		       "Try \'%s --help\' for more information.\n",
		       DAT_00612568);
      goto LAB_00401f96;
    }
  func_0x00401770 ("Usage: %s [OPTION]... FILE...\n", DAT_00612568);
  uVar3 = DAT_006124c0;
  func_0x00401960
    ("Print value of a symbolic link or canonical file name\n\n",
     DAT_006124c0);
  func_0x00401960
    ("  -f, --canonicalize            canonicalize by following every symlink in\n                                every component of the given name recursively;\n                                all but the last component must exist\n  -e, --canonicalize-existing   canonicalize by following every symlink in\n                                every component of the given name recursively,\n                                all components must exist\n",
     uVar3);
  func_0x00401960
    ("  -m, --canonicalize-missing    canonicalize by following every symlink in\n                                every component of the given name recursively,\n                                without requirements on components existence\n  -n, --no-newline              do not output the trailing delimiter\n  -q, --quiet\n  -s, --silent                  suppress most error messages (on by default)\n  -v, --verbose                 report error messages\n  -z, --zero                    end each output line with NUL, not newline\n",
     uVar3);
  func_0x00401960 ("      --help     display this help and exit\n", uVar3);
  func_0x00401960 ("      --version  output version information and exit\n",
		   uVar3);
  local_88 = &DAT_0040e989;
  local_80 = "test invocation";
  local_78 = 0x40e9ec;
  local_70 = "Multi-call invocation";
  local_68 = "sha224sum";
  local_60 = "sha2 utilities";
  local_58 = "sha256sum";
  local_50 = "sha2 utilities";
  local_48 = "sha384sum";
  local_40 = "sha2 utilities";
  local_38 = "sha512sum";
  local_30 = "sha2 utilities";
  local_28 = 0;
  local_20 = 0;
  puVar6 = &DAT_0040e989;
  do
    {
      iVar1 = func_0x00401a40 ("readlink", puVar6);
      if (iVar1 == 0)
	break;
      ppuVar4 = ppuVar4 + 2;
      puVar6 = *ppuVar4;
    }
  while (puVar6 != (undefined *) 0x0);
  pcVar5 = ppuVar4[1];
  if (pcVar5 == (char *) 0x0)
    {
      func_0x00401770 ("\n%s online help: <%s>\n", "GNU coreutils",
		       "https://www.gnu.org/software/coreutils/");
      lVar2 = func_0x00401aa0 (5, 0);
      if (lVar2 == 0)
	goto LAB_00401f9d;
      iVar1 = func_0x004019c0 (lVar2, &DAT_0040ea0d, 3);
      if (iVar1 == 0)
	{
	  func_0x00401770 ("Full documentation at: <%s%s>\n",
			   "https://www.gnu.org/software/coreutils/",
			   "readlink");
	  pcVar5 = "readlink";
	  uVar3 = 0x40e9a5;
	  goto LAB_00401f84;
	}
      pcVar5 = "readlink";
    LAB_00401f42:
      ;
      func_0x00401770
	("Report %s translation bugs to <https://translationproject.org/team/>\n",
	 "readlink");
    }
  else
    {
      func_0x00401770 ("\n%s online help: <%s>\n", "GNU coreutils",
		       "https://www.gnu.org/software/coreutils/");
      lVar2 = func_0x00401aa0 (5, 0);
      if ((lVar2 != 0)
	  && (iVar1 = func_0x004019c0 (lVar2, &DAT_0040ea0d, 3), iVar1 != 0))
	goto LAB_00401f42;
    }
  func_0x00401770 ("Full documentation at: <%s%s>\n",
		   "https://www.gnu.org/software/coreutils/", "readlink");
  uVar3 = 0x40ff17;
  if (pcVar5 == "readlink")
    {
      uVar3 = 0x40e9a5;
    }
LAB_00401f84:
  ;
  do
    {
      func_0x00401770
	("or available locally via: info \'(coreutils) %s%s\'\n", pcVar5,
	 uVar3);
    LAB_00401f96:
      ;
      func_0x00401b00 ((ulong) uParm1);
    LAB_00401f9d:
      ;
      func_0x00401770 ("Full documentation at: <%s%s>\n",
		       "https://www.gnu.org/software/coreutils/", "readlink");
      pcVar5 = "readlink";
      uVar3 = 0x40e9a5;
    }
  while (true);
}


#include "nohup.h"

long null_ARRAY_0061044_0_8_;
long null_ARRAY_0061044_8_8_;
long null_ARRAY_0061064_0_8_;
long null_ARRAY_0061064_16_8_;
long null_ARRAY_0061064_24_8_;
long null_ARRAY_0061064_32_8_;
long null_ARRAY_0061064_40_8_;
long null_ARRAY_0061064_48_8_;
long null_ARRAY_0061064_8_8_;
long null_ARRAY_0061068_0_4_;
long null_ARRAY_0061068_16_8_;
long null_ARRAY_0061068_4_4_;
long null_ARRAY_0061068_8_4_;
long local_68_2_2_;
long local_6b_1_3_;
long local_6c_4_4_;
long local_6e_4_4_;
long local_6f_4_4_;
long local_70_0_2_;
long local_70_4_4_;
long local_71_0_1_;
long local_71_4_4_;
long local_72_4_4_;
long DAT_00000010;
long DAT_0040d1c6;
long DAT_0040d24a;
long DAT_0040d2a9;
long DAT_0040d2d4;
long DAT_0040d860;
long DAT_0040d864;
long DAT_0040d868;
long DAT_0040d86b;
long DAT_0040d86d;
long DAT_0040d871;
long DAT_0040d875;
long DAT_0040e02b;
long DAT_0040e3d5;
long DAT_0040e4d9;
long DAT_0040e4df;
long DAT_0040e4e1;
long DAT_0040e4e2;
long DAT_0040e500;
long DAT_0040e504;
long DAT_0040e58e;
long DAT_00610000;
long DAT_00610010;
long DAT_00610020;
long DAT_006103e8;
long DAT_00610450;
long DAT_00610454;
long DAT_00610458;
long DAT_0061045c;
long DAT_00610480;
long DAT_00610490;
long DAT_006104a0;
long DAT_006104a8;
long DAT_006104c0;
long DAT_006104c8;
long DAT_00610510;
long DAT_00610518;
long DAT_00610520;
long DAT_00610678;
long DAT_006106b8;
long DAT_006106c0;
long DAT_006106c8;
long DAT_006106d8;
long fde_0040f0f0;
long null_ARRAY_0040d760;
long null_ARRAY_0040d7c0;
long null_ARRAY_0040e9a0;
long null_ARRAY_0040ebe0;
long null_ARRAY_00610440;
long null_ARRAY_006104e0;
long null_ARRAY_00610540;
long null_ARRAY_00610640;
long null_ARRAY_00610680;
long PTR_DAT_006103e0;
long PTR_null_ARRAY_00610438;
void
FUN_00401b5e (uint uParm1)
{
  int iVar1;
  long lVar2;
  undefined8 uVar3;
  undefined **ppuVar4;
  char *pcVar5;
  undefined *puVar6;
  undefined *local_88;
  char *local_80;
  undefined8 local_78;
  char *local_70;
  char *local_68;
  char *local_60;
  char *local_58;
  char *local_50;
  char *local_48;
  char *local_40;
  char *local_38;
  char *local_30;
  undefined8 local_28;
  undefined8 local_20;

  ppuVar4 = &local_88;
  if (uParm1 != 0)
    {
      func_0x00401720 (DAT_006104a0,
		       "Try \'%s --help\' for more information.\n",
		       DAT_00610520);
      goto LAB_00401d49;
    }
  func_0x00401580 ("Usage: %s COMMAND [ARG]...\n  or:  %s OPTION\n",
		   DAT_00610520, DAT_00610520);
  uVar3 = DAT_00610480;
  func_0x00401730 ("Run COMMAND, ignoring hangup signals.\n\n", DAT_00610480);
  func_0x00401730 ("      --help     display this help and exit\n", uVar3);
  func_0x00401730 ("      --version  output version information and exit\n",
		   uVar3);
  func_0x00401580
    ("\nIf standard input is a terminal, redirect it from an unreadable file.\nIf standard output is a terminal, append output to \'nohup.out\' if possible,\n\'$HOME/nohup.out\' otherwise.\nIf standard error is a terminal, redirect it to standard output.\nTo save output to FILE, use \'%s COMMAND > FILE\'.\n",
     DAT_00610520);
  func_0x00401580
    ("\nNOTE: your shell may have its own version of %s, which usually supersedes\nthe version described here.  Please refer to your shell\'s documentation\nfor details about the options it supports.\n",
     "nohup");
  local_88 = &DAT_0040d1c6;
  local_80 = "test invocation";
  local_78 = 0x40d229;
  local_70 = "Multi-call invocation";
  local_68 = "sha224sum";
  local_60 = "sha2 utilities";
  local_58 = "sha256sum";
  local_50 = "sha2 utilities";
  local_48 = "sha384sum";
  local_40 = "sha2 utilities";
  local_38 = "sha512sum";
  local_30 = "sha2 utilities";
  local_28 = 0;
  local_20 = 0;
  puVar6 = &DAT_0040d1c6;
  do
    {
      iVar1 = func_0x00401820 ("nohup", puVar6);
      if (iVar1 == 0)
	break;
      ppuVar4 = ppuVar4 + 2;
      puVar6 = *ppuVar4;
    }
  while (puVar6 != (undefined *) 0x0);
  pcVar5 = ppuVar4[1];
  if (pcVar5 == (char *) 0x0)
    {
      func_0x00401580 ("\n%s online help: <%s>\n", "GNU coreutils",
		       "https://www.gnu.org/software/coreutils/");
      lVar2 = func_0x00401880 (5, 0);
      if (lVar2 == 0)
	goto LAB_00401d50;
      iVar1 = func_0x004017a0 (lVar2, &DAT_0040d24a, 3);
      if (iVar1 == 0)
	{
	  func_0x00401580 ("Full documentation at: <%s%s>\n",
			   "https://www.gnu.org/software/coreutils/",
			   "nohup");
	  pcVar5 = "nohup";
	  uVar3 = 0x40d1e2;
	  goto LAB_00401d37;
	}
      pcVar5 = "nohup";
    LAB_00401cf5:
      ;
      func_0x00401580
	("Report %s translation bugs to <https://translationproject.org/team/>\n",
	 "nohup");
    }
  else
    {
      func_0x00401580 ("\n%s online help: <%s>\n", "GNU coreutils",
		       "https://www.gnu.org/software/coreutils/");
      lVar2 = func_0x00401880 (5, 0);
      if ((lVar2 != 0)
	  && (iVar1 = func_0x004017a0 (lVar2, &DAT_0040d24a, 3), iVar1 != 0))
	goto LAB_00401cf5;
    }
  func_0x00401580 ("Full documentation at: <%s%s>\n",
		   "https://www.gnu.org/software/coreutils/", "nohup");
  uVar3 = 0x40e4ff;
  if (pcVar5 == "nohup")
    {
      uVar3 = 0x40d1e2;
    }
LAB_00401d37:
  ;
  do
    {
      func_0x00401580
	("or available locally via: info \'(coreutils) %s%s\'\n", pcVar5,
	 uVar3);
    LAB_00401d49:
      ;
      func_0x004018c0 ((ulong) uParm1);
    LAB_00401d50:
      ;
      func_0x00401580 ("Full documentation at: <%s%s>\n",
		       "https://www.gnu.org/software/coreutils/", "nohup");
      pcVar5 = "nohup";
      uVar3 = 0x40d1e2;
    }
  while (true);
}

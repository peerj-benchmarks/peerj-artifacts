
#include "cut.h"

long DAT_00611d1_0_1_;
long DAT_00611d1_1_1_;
long null_ARRAY_00611c6_0_8_;
long null_ARRAY_00611c6_8_8_;
long null_ARRAY_00611e8_0_8_;
long null_ARRAY_00611e8_16_8_;
long null_ARRAY_00611e8_24_8_;
long null_ARRAY_00611e8_32_8_;
long null_ARRAY_00611e8_40_8_;
long null_ARRAY_00611e8_48_8_;
long null_ARRAY_00611e8_8_8_;
long null_ARRAY_00611ec_0_4_;
long null_ARRAY_00611ec_16_8_;
long null_ARRAY_00611ec_4_4_;
long null_ARRAY_00611ec_8_4_;
long local_68_2_2_;
long local_6b_1_3_;
long local_6c_4_4_;
long local_6e_4_4_;
long local_6f_4_4_;
long local_70_0_2_;
long local_70_4_4_;
long local_71_0_1_;
long local_71_4_4_;
long local_72_4_4_;
long DAT_00000010;
long DAT_0040e497;
long DAT_0040e49b;
long DAT_0040e51f;
long DAT_0040f1c8;
long DAT_0040f1cc;
long DAT_0040f1d0;
long DAT_0040f1d3;
long DAT_0040f1d5;
long DAT_0040f1d9;
long DAT_0040f1dd;
long DAT_0040f96b;
long DAT_0040fd15;
long DAT_0040fe19;
long DAT_0040fe1f;
long DAT_0040fe31;
long DAT_0040fe32;
long DAT_0040fe50;
long DAT_0040fe54;
long DAT_0040fede;
long DAT_006117c8;
long DAT_006117d8;
long DAT_006117e8;
long DAT_00611c00;
long DAT_00611c10;
long DAT_00611c70;
long DAT_00611c74;
long DAT_00611c78;
long DAT_00611c7c;
long DAT_00611c80;
long DAT_00611c88;
long DAT_00611c90;
long DAT_00611ca0;
long DAT_00611ca8;
long DAT_00611cc0;
long DAT_00611cc8;
long DAT_00611d10;
long DAT_00611d12;
long DAT_00611d18;
long DAT_00611d20;
long DAT_00611d28;
long DAT_00611d29;
long DAT_00611d2a;
long DAT_00611d2b;
long DAT_00611d2c;
long DAT_00611d30;
long DAT_00611d38;
long DAT_00611d40;
long DAT_00611d48;
long DAT_00611d50;
long DAT_00611d58;
long DAT_00611d60;
long DAT_00611d68;
long DAT_00611ef8;
long DAT_00611f00;
long DAT_00611f08;
long DAT_00611f10;
long DAT_00611f18;
long DAT_00611f28;
long fde_00410a90;
long null_ARRAY_0040ee80;
long null_ARRAY_00410300;
long null_ARRAY_00410540;
long null_ARRAY_00611c60;
long null_ARRAY_00611ce0;
long null_ARRAY_00611d80;
long null_ARRAY_00611e80;
long null_ARRAY_00611ec0;
long PTR_DAT_00611c08;
long PTR_null_ARRAY_00611c58;
void
FUN_0040254a (uint uParm1)
{
  int iVar1;
  long lVar2;
  undefined8 uVar3;
  undefined **ppuVar4;
  undefined *puVar5;
  undefined *local_88;
  char *local_80;
  undefined8 local_78;
  char *local_70;
  char *local_68;
  char *local_60;
  char *local_58;
  char *local_50;
  char *local_48;
  char *local_40;
  char *local_38;
  char *local_30;
  undefined8 local_28;
  undefined8 local_20;

  ppuVar4 = &local_88;
  if (uParm1 != 0)
    {
      func_0x00401a80 (DAT_00611ca0,
		       "Try \'%s --help\' for more information.\n",
		       DAT_00611d68);
      goto LAB_0040277d;
    }
  func_0x004018c0 ("Usage: %s OPTION... [FILE]...\n", DAT_00611d68);
  uVar3 = DAT_00611c80;
  func_0x00401a90
    ("Print selected parts of lines from each FILE to standard output.\n",
     DAT_00611c80);
  func_0x00401a90
    ("\nWith no FILE, or when FILE is -, read standard input.\n", uVar3);
  func_0x00401a90
    ("\nMandatory arguments to long options are mandatory for short options too.\n",
     uVar3);
  func_0x00401a90
    ("  -b, --bytes=LIST        select only these bytes\n  -c, --characters=LIST   select only these characters\n  -d, --delimiter=DELIM   use DELIM instead of TAB for field delimiter\n",
     uVar3);
  func_0x00401a90
    ("  -f, --fields=LIST       select only these fields;  also print any line\n                            that contains no delimiter character, unless\n                            the -s option is specified\n  -n                      (ignored)\n",
     uVar3);
  func_0x00401a90
    ("      --complement        complement the set of selected bytes, characters\n                            or fields\n",
     uVar3);
  func_0x00401a90
    ("  -s, --only-delimited    do not print lines not containing delimiters\n      --output-delimiter=STRING  use STRING as the output delimiter\n                            the default is to use the input delimiter\n",
     uVar3);
  func_0x00401a90
    ("  -z, --zero-terminated    line delimiter is NUL, not newline\n",
     uVar3);
  func_0x00401a90 ("      --help     display this help and exit\n", uVar3);
  func_0x00401a90 ("      --version  output version information and exit\n",
		   uVar3);
  func_0x00401a90
    ("\nUse one, and only one of -b, -c or -f.  Each LIST is made up of one\nrange, or many ranges separated by commas.  Selected input is written\nin the same order that it is read, and is written exactly once.\n",
     uVar3);
  func_0x00401a90
    ("Each range is one of:\n\n  N     N\'th byte, character or field, counted from 1\n  N-    from N\'th byte, character or field, to end of line\n  N-M   from N\'th to M\'th (included) byte, character or field\n  -M    from first to M\'th (included) byte, character or field\n",
     uVar3);
  local_88 = &DAT_0040e49b;
  local_80 = "test invocation";
  local_78 = 0x40e4fe;
  local_70 = "Multi-call invocation";
  local_68 = "sha224sum";
  local_60 = "sha2 utilities";
  local_58 = "sha256sum";
  local_50 = "sha2 utilities";
  local_48 = "sha384sum";
  local_40 = "sha2 utilities";
  local_38 = "sha512sum";
  local_30 = "sha2 utilities";
  local_28 = 0;
  local_20 = 0;
  puVar5 = &DAT_0040e49b;
  do
    {
      iVar1 = func_0x00401bb0 (&DAT_0040e497, puVar5);
      if (iVar1 == 0)
	break;
      ppuVar4 = ppuVar4 + 2;
      puVar5 = *ppuVar4;
    }
  while (puVar5 != (undefined *) 0x0);
  puVar5 = ppuVar4[1];
  if (puVar5 == (undefined *) 0x0)
    {
      func_0x004018c0 ("\n%s online help: <%s>\n", "GNU coreutils",
		       "https://www.gnu.org/software/coreutils/");
      lVar2 = func_0x00401c20 (5, 0);
      if (lVar2 == 0)
	goto LAB_00402784;
      iVar1 = func_0x00401b00 (lVar2, &DAT_0040e51f, 3);
      if (iVar1 == 0)
	{
	  func_0x004018c0 ("Full documentation at: <%s%s>\n",
			   "https://www.gnu.org/software/coreutils/",
			   &DAT_0040e497);
	  puVar5 = &DAT_0040e497;
	  uVar3 = 0x40e4b7;
	  goto LAB_0040276b;
	}
      puVar5 = &DAT_0040e497;
    LAB_00402729:
      ;
      func_0x004018c0
	("Report %s translation bugs to <https://translationproject.org/team/>\n",
	 &DAT_0040e497);
    }
  else
    {
      func_0x004018c0 ("\n%s online help: <%s>\n", "GNU coreutils",
		       "https://www.gnu.org/software/coreutils/");
      lVar2 = func_0x00401c20 (5, 0);
      if ((lVar2 != 0)
	  && (iVar1 = func_0x00401b00 (lVar2, &DAT_0040e51f, 3), iVar1 != 0))
	goto LAB_00402729;
    }
  func_0x004018c0 ("Full documentation at: <%s%s>\n",
		   "https://www.gnu.org/software/coreutils/", &DAT_0040e497);
  uVar3 = 0x40fe4f;
  if (puVar5 == &DAT_0040e497)
    {
      uVar3 = 0x40e4b7;
    }
LAB_0040276b:
  ;
  do
    {
      func_0x004018c0
	("or available locally via: info \'(coreutils) %s%s\'\n", puVar5,
	 uVar3);
    LAB_0040277d:
      ;
      func_0x00401ca0 ((ulong) uParm1);
    LAB_00402784:
      ;
      func_0x004018c0 ("Full documentation at: <%s%s>\n",
		       "https://www.gnu.org/software/coreutils/",
		       &DAT_0040e497);
      puVar5 = &DAT_0040e497;
      uVar3 = 0x40e4b7;
    }
  while (true);
}

typedef unsigned char   undefined;

typedef unsigned char    byte;
typedef unsigned char    dwfenc;
typedef unsigned int    dword;
typedef unsigned long    qword;
typedef unsigned char    undefined1;
typedef unsigned short    undefined2;
typedef unsigned int    undefined4;
typedef unsigned long    undefined5;
typedef unsigned long    undefined6;
typedef unsigned long    undefined7;
typedef unsigned long    undefined8;
typedef unsigned short    word;
#define unkbyte9   unsigned long long
#define unkbyte10   unsigned long long
#define unkbyte11   unsigned long long
#define unkbyte12   unsigned long long
#define unkbyte13   unsigned long long
#define unkbyte14   unsigned long long
#define unkbyte15   unsigned long long
#define unkbyte16   unsigned long long

#define unkuint9   unsigned long long
#define unkuint10   unsigned long long
#define unkuint11   unsigned long long
#define unkuint12   unsigned long long
#define unkuint13   unsigned long long
#define unkuint14   unsigned long long
#define unkuint15   unsigned long long
#define unkuint16   unsigned long long

#define unkint9   long long
#define unkint10   long long
#define unkint11   long long
#define unkint12   long long
#define unkint13   long long
#define unkint14   long long
#define unkint15   long long
#define unkint16   long long

#define unkfloat1   float
#define unkfloat2   float
#define unkfloat3   float
#define unkfloat5   double
#define unkfloat6   double
#define unkfloat7   double
#define unkfloat9   long double
#define unkfloat11   long double
#define unkfloat12   long double
#define unkfloat13   long double
#define unkfloat14   long double
#define unkfloat15   long double
#define unkfloat16   long double

#define BADSPACEBASE   void
#define code   void

#define ulong long
#define true 1
#define bool int
#define false 0
#define float10 int
#define ushort int
#define undefined7 long
#define undefined2 long
#define uint5 long
#define uint6 long

// Add definition for uint
typedef unsigned int    uint;

#define sbyte char

extern imperfection_wrapper(void);
typedef struct eh_frame_hdr eh_frame_hdr, *Peh_frame_hdr;

struct eh_frame_hdr {
    byte eh_frame_hdr_version; // Exception Handler Frame Header Version
    dwfenc eh_frame_pointer_encoding; // Exception Handler Frame Pointer Encoding
    dwfenc eh_frame_desc_entry_count_encoding; // Encoding of # of Exception Handler FDEs
    dwfenc eh_frame_table_encoding; // Exception Handler Table Encoding
};

typedef struct fde_table_entry fde_table_entry, *Pfde_table_entry;

struct fde_table_entry {
    dword initial_loc; // Initial Location
    dword data_loc; // Data location
};

typedef enum Elf64_DynTag {
    DT_AUDIT=1879047932,
    DT_AUXILIARY=2147483645,
    DT_BIND_NOW=24,
    DT_CHECKSUM=1879047672,
    DT_CONFIG=1879047930,
    DT_DEBUG=21,
    DT_DEPAUDIT=1879047931,
    DT_ENCODING=32,
    DT_FEATURE_1=1879047676,
    DT_FILTER=2147483647,
    DT_FINI=13,
    DT_FINI_ARRAY=26,
    DT_FINI_ARRAYSZ=28,
    DT_FLAGS=30,
    DT_FLAGS_1=1879048187,
    DT_GNU_CONFLICT=1879047928,
    DT_GNU_CONFLICTSZ=1879047670,
    DT_GNU_HASH=1879047925,
    DT_GNU_LIBLIST=1879047929,
    DT_GNU_LIBLISTSZ=1879047671,
    DT_GNU_PRELINKED=1879047669,
    DT_HASH=4,
    DT_INIT=12,
    DT_INIT_ARRAY=25,
    DT_INIT_ARRAYSZ=27,
    DT_JMPREL=23,
    DT_MOVEENT=1879047674,
    DT_MOVESZ=1879047675,
    DT_MOVETAB=1879047934,
    DT_NEEDED=1,
    DT_NULL=0,
    DT_PLTGOT=3,
    DT_PLTPAD=1879047933,
    DT_PLTPADSZ=1879047673,
    DT_PLTREL=20,
    DT_PLTRELSZ=2,
    DT_POSFLAG_1=1879047677,
    DT_PREINIT_ARRAYSZ=33,
    DT_REL=17,
    DT_RELA=7,
    DT_RELACOUNT=1879048185,
    DT_RELAENT=9,
    DT_RELASZ=8,
    DT_RELCOUNT=1879048186,
    DT_RELENT=19,
    DT_RELSZ=18,
    DT_RPATH=15,
    DT_RUNPATH=29,
    DT_SONAME=14,
    DT_STRSZ=10,
    DT_STRTAB=5,
    DT_SYMBOLIC=16,
    DT_SYMENT=11,
    DT_SYMINENT=1879047679,
    DT_SYMINFO=1879047935,
    DT_SYMINSZ=1879047678,
    DT_SYMTAB=6,
    DT_TEXTREL=22,
    DT_TLSDESC_GOT=1879047927,
    DT_TLSDESC_PLT=1879047926,
    DT_VERDEF=1879048188,
    DT_VERDEFNUM=1879048189,
    DT_VERNEED=1879048190,
    DT_VERNEEDNUM=1879048191,
    DT_VERSYM=1879048176
} Elf64_DynTag;

typedef enum Elf_ProgramHeaderType {
    PT_DYNAMIC=2,
    PT_GNU_EH_FRAME=1685382480,
    PT_GNU_RELRO=1685382482,
    PT_GNU_STACK=1685382481,
    PT_INTERP=3,
    PT_LOAD=1,
    PT_NOTE=4,
    PT_NULL=0,
    PT_PHDR=6,
    PT_SHLIB=5,
    PT_TLS=7
} Elf_ProgramHeaderType;

typedef struct Elf64_Dyn Elf64_Dyn, *PElf64_Dyn;

struct Elf64_Dyn {
    enum Elf64_DynTag d_tag;
    qword d_val;
};

typedef struct Elf64_Shdr Elf64_Shdr, *PElf64_Shdr;

typedef enum Elf_SectionHeaderType {
    SHT_CHECKSUM=1879048184,
    SHT_DYNAMIC=6,
    SHT_DYNSYM=11,
    SHT_FINI_ARRAY=15,
    SHT_GNU_ATTRIBUTES=1879048181,
    SHT_GNU_HASH=1879048182,
    SHT_GNU_LIBLIST=1879048183,
    SHT_GNU_verdef=1879048189,
    SHT_GNU_verneed=1879048190,
    SHT_GNU_versym=1879048191,
    SHT_GROUP=17,
    SHT_HASH=5,
    SHT_INIT_ARRAY=14,
    SHT_NOBITS=8,
    SHT_NOTE=7,
    SHT_NULL=0,
    SHT_PREINIT_ARRAY=16,
    SHT_PROGBITS=1,
    SHT_REL=9,
    SHT_RELA=4,
    SHT_SHLIB=10,
    SHT_STRTAB=3,
    SHT_SUNW_COMDAT=1879048187,
    SHT_SUNW_move=1879048186,
    SHT_SUNW_syminfo=1879048188,
    SHT_SYMTAB=2,
    SHT_SYMTAB_SHNDX=18
} Elf_SectionHeaderType;

struct Elf64_Shdr {
    dword sh_name;
    enum Elf_SectionHeaderType sh_type;
    qword sh_flags;
    qword sh_addr;
    qword sh_offset;
    qword sh_size;
    dword sh_link;
    dword sh_info;
    qword sh_addralign;
    qword sh_entsize;
};

typedef struct Elf64_Phdr Elf64_Phdr, *PElf64_Phdr;

struct Elf64_Phdr {
    enum Elf_ProgramHeaderType p_type;
    dword p_flags;
    qword p_offset;
    qword p_vaddr;
    qword p_paddr;
    qword p_filesz;
    qword p_memsz;
    qword p_align;
};

typedef struct Elf64_Sym Elf64_Sym, *PElf64_Sym;

struct Elf64_Sym {
    dword st_name;
    byte st_info;
    byte st_other;
    word st_shndx;
    qword st_value;
    qword st_size;
};

typedef struct Elf64_Rela Elf64_Rela, *PElf64_Rela;

struct Elf64_Rela {
    qword r_offset; // location to apply the relocation action
    qword r_info; // the symbol table index and the type of relocation
    qword r_addend; // a constant addend used to compute the relocatable field value
};

typedef struct Elf64_Ehdr Elf64_Ehdr, *PElf64_Ehdr;

struct Elf64_Ehdr {
    byte e_ident_magic_num;
    char e_ident_magic_str[3];
    byte e_ident_class;
    byte e_ident_data;
    byte e_ident_version;
    byte e_ident_pad[9];
    word e_type;
    word e_machine;
    dword e_version;
    qword e_entry;
    qword e_phoff;
    qword e_shoff;
    dword e_flags;
    word e_ehsize;
    word e_phentsize;
    word e_phnum;
    word e_shentsize;
    word e_shnum;
    word e_shstrndx;
};




undefined8 _DT_INIT(void);
void FUN_004023d0(void);
void FUN_004026a0(void);
void FUN_004029a0(void);
void FUN_00402bb0(void);
void entry(void);
void FUN_00402c20(void);
void FUN_00402ca0(void);
void FUN_00402d20(void);
ulong FUN_00402d5e(ulong *puParm1,ulong uParm2);
ulong FUN_00402d6d(long *plParm1,long *plParm2);
void FUN_00402d8c(int iParm1);
void FUN_00402d9e(void);
void FUN_00402db9(char cParm1);
ulong FUN_00402dde(int iParm1,undefined8 uParm2,uint uParm3);
ulong FUN_00402df2(long lParm1,undefined8 uParm2,undefined8 uParm3,uint uParm4);
void FUN_00402e07(void);
ulong FUN_00402e3c(char cParm1,ulong uParm2,int iParm3);
void FUN_00402f11(void);
void FUN_00402fdd(void);
uint FUN_00402feb(byte bParm1);
void FUN_00403012(ulong uParm1,ulong uParm2);
ulong FUN_0040308c(byte **ppbParm1,byte **ppbParm2,char cParm3,long *plParm4);
ulong FUN_004032ef(undefined8 *puParm1,undefined8 *puParm2);
ulong FUN_00403361(undefined8 *puParm1,undefined8 *puParm2);
ulong FUN_004033c7(undefined8 *puParm1,undefined8 *puParm2);
void FUN_0040342d(undefined8 uParm1);
ulong FUN_00403452(uint uParm1);
void FUN_004034b3(undefined8 uParm1,long lParm2);
undefined8 FUN_0040354c(undefined8 uParm1);
void FUN_004035a4(void);
void FUN_0040377e(long lParm1,long lParm2,undefined uParm3);
ulong FUN_004037ea(undefined8 *puParm1,undefined8 *puParm2);
ulong FUN_0040381d(undefined8 *puParm1,undefined8 *puParm2);
ulong FUN_00403853(undefined8 *puParm1,undefined8 *puParm2);
ulong FUN_004038b3(undefined8 *puParm1,undefined8 *puParm2);
void FUN_00403913(undefined8 *puParm1,undefined8 *puParm2);
void FUN_00403927(undefined8 *puParm1,undefined8 *puParm2);
undefined8 FUN_0040393e(undefined8 *puParm1,undefined8 *puParm2);
undefined8 FUN_0040397b(undefined8 *puParm1,undefined8 *puParm2);
void FUN_004039b8(long lParm1,undefined8 uParm2,ulong uParm3);
void FUN_00403a46(byte bParm1,undefined8 uParm2,undefined8 uParm3);
ulong FUN_00403a88(char *pcParm1);
undefined8 FUN_00403adc(long lParm1,undefined8 uParm2);
byte * FUN_00403b1d(long lParm1,char cParm2);
void FUN_00403bc9(ulong uParm1);
undefined8 FUN_00403bec(undefined8 *puParm1,int iParm2,long lParm3,byte bParm4,char *pcParm5);
void FUN_00404767(ulong uParm1,ulong uParm2,char cParm3);
void FUN_004047a1(void);
ulong FUN_004048b1(undefined8 uParm1,undefined8 uParm2);
ulong FUN_00404940(undefined8 *puParm1,undefined8 *puParm2);
ulong FUN_00404973(undefined8 *puParm1,undefined8 *puParm2);
ulong FUN_004049a9(undefined8 *puParm1,undefined8 *puParm2);
ulong FUN_00404a09(undefined8 *puParm1,undefined8 *puParm2);
void FUN_00404a69(undefined8 *puParm1,undefined8 *puParm2);
void FUN_00404a7d(undefined8 *puParm1,undefined8 *puParm2);
undefined8 FUN_00404a94(undefined8 *puParm1,undefined8 *puParm2);
undefined8 FUN_00404ad1(undefined8 *puParm1,undefined8 *puParm2);
undefined8 FUN_00404b0e(undefined8 *puParm1,undefined8 *puParm2);
void FUN_00404b4b(undefined8 *puParm1,undefined8 *puParm2);
undefined8 FUN_00404b62(undefined8 *puParm1,undefined8 *puParm2);
void FUN_00404b9f(undefined8 *puParm1,undefined8 *puParm2);
undefined8 FUN_00404bb3(undefined8 *puParm1,undefined8 *puParm2);
byte * FUN_00404c32(byte **param_1,byte *param_2,byte *param_3,undefined8 param_4,int param_5,byte **param_6,byte *param_7);
long FUN_00404fe1(long *plParm1);
ulong FUN_00405181(char cParm1);
void FUN_00405449(char cParm1);
void FUN_004055ef(undefined8 *puParm1);
void FUN_0040563a(void);
void FUN_00405677(void);
void FUN_004056b6(void);
void FUN_004056d3(void);
long FUN_00405769(char *param_1,undefined8 param_2,uint param_3,long param_4,char param_5,long param_6,long param_7);
long FUN_00405a16(long *plParm1,byte bParm2,undefined8 uParm3,ulong uParm4);
long FUN_00405e7d(long lParm1,undefined8 uParm2);
void FUN_00405fb3(char cParm1);
ulong FUN_004060af(undefined8 *puParm1,undefined8 *puParm2);
ulong FUN_004060f9(undefined8 *puParm1,undefined8 *puParm2);
ulong FUN_00406143(undefined8 *puParm1,undefined8 *puParm2);
ulong FUN_00406190(undefined8 *puParm1,undefined8 *puParm2);
ulong FUN_004061dd(undefined8 *puParm1,undefined8 *puParm2);
ulong FUN_0040621b(undefined8 *puParm1,undefined8 *puParm2);
ulong FUN_00406259(undefined8 *puParm1,undefined8 *puParm2);
ulong FUN_0040629a(undefined8 *puParm1,undefined8 *puParm2);
ulong FUN_004062db(undefined8 *puParm1,undefined8 *puParm2);
ulong FUN_00406319(undefined8 *puParm1,undefined8 *puParm2);
ulong FUN_00406357(undefined8 *puParm1,undefined8 *puParm2);
ulong FUN_00406398(undefined8 *puParm1,undefined8 *puParm2);
ulong FUN_004063d9(undefined8 *puParm1,undefined8 *puParm2);
ulong FUN_0040643f(undefined8 *puParm1,undefined8 *puParm2);
ulong FUN_004064a5(undefined8 *puParm1,undefined8 *puParm2);
ulong FUN_0040650b(undefined8 *puParm1,undefined8 *puParm2);
ulong FUN_0040657d(undefined8 *puParm1,undefined8 *puParm2);
ulong FUN_004065ef(undefined8 *puParm1,undefined8 *puParm2);
ulong FUN_00406661(undefined8 *puParm1,undefined8 *puParm2);
ulong FUN_004066c7(undefined8 *puParm1,undefined8 *puParm2);
ulong FUN_0040672d(undefined8 *puParm1,undefined8 *puParm2);
void FUN_00406793(undefined8 *puParm1,undefined8 *puParm2);
void FUN_004067f0(undefined8 *puParm1,undefined8 *puParm2);
void FUN_0040684d(undefined8 *puParm1,undefined8 *puParm2);
void FUN_004068ab(undefined8 *puParm1,undefined8 *puParm2);
undefined8 FUN_00406909(undefined8 *puParm1,undefined8 *puParm2);
undefined8 FUN_00406988(undefined8 *puParm1,undefined8 *puParm2);
undefined8 FUN_00406a07(undefined8 *puParm1,undefined8 *puParm2);
void FUN_00406a86(long lParm1,byte bParm2);
void FUN_00406bed(long lParm1);
void FUN_004073a3(void);
void FUN_00407685(long lParm1,long lParm2,ulong uParm3);
void FUN_00407bbc(uint uParm1);
ulong FUN_00407f8a(ulong uParm1,undefined8 *puParm2);
long FUN_00409693(undefined8 uParm1,ulong uParm2);
void FUN_00409772(void);
long FUN_00409785(undefined8 uParm1,long *plParm2,long lParm3,long lParm4);
void FUN_00409875(undefined8 uParm1,undefined8 uParm2,long lParm3);
void FUN_004098d2(long *plParm1,long lParm2,long lParm3);
long FUN_0040999d(undefined8 uParm1,undefined8 uParm2,undefined8 uParm3,undefined8 uParm4,undefined8 uParm5,code *pcParm6);
char * FUN_00409a08(char *pcParm1,uint uParm2);
void FUN_00409fa2(void);
ulong FUN_0040a048(char *pcParm1);
char * FUN_0040a09f(char *pcParm1);
ulong FUN_0040a0e5(long lParm1);
undefined8 FUN_0040a11f(void);
void FUN_0040a125(long lParm1,undefined8 uParm2,undefined8 *puParm3);
ulong FUN_0040a193(long lParm1,undefined8 uParm2,undefined8 *puParm3);
void FUN_0040a1ca(ulong uParm1,undefined *puParm2);
void FUN_0040a33b(long lParm1);
void FUN_0040a345(void);
long FUN_0040a35d(long lParm1,char *pcParm2,undefined8 *puParm3);
char * FUN_0040a436(char **ppcParm1);
ulong FUN_0040a4d6(byte bParm1);
ulong FUN_0040a512(char *pcParm1,char *pcParm2);
void FUN_0040a7ec(undefined8 *puParm1);
ulong FUN_0040a82c(ulong uParm1);
ulong FUN_0040a8ba(ulong uParm1,ulong uParm2);
undefined FUN_0040a8cd(long lParm1,long lParm2);;
ulong FUN_0040a8d4(long lParm1,undefined8 uParm2,uint uParm3);
ulong FUN_0040a958(ulong uParm1,long lParm2);
long FUN_0040aa74(long *plParm1,undefined8 uParm2);
undefined8 FUN_0040aa96(long lParm1,long **pplParm2,char cParm3);
long FUN_0040abba(long lParm1,long lParm2,long **pplParm3,char cParm4);
undefined8 FUN_0040acbe(long lParm1);
long FUN_0040acc3(long lParm1,long lParm2);
long * FUN_0040ad1f(undefined8 uParm1,undefined1 *puParm2,code *pcParm3,code *pcParm4,long lParm5);
void FUN_0040ae0a(long **pplParm1);
ulong FUN_0040aeaf(long *plParm1,undefined8 uParm2);
undefined8 FUN_0040afec(long lParm1,long lParm2,long *plParm3);
undefined8 FUN_0040b219(undefined8 uParm1,undefined8 uParm2);
long FUN_0040b248(long lParm1,undefined8 uParm2);
ulong FUN_0040b424(undefined8 *puParm1,ulong uParm2);
ulong FUN_0040b44e(undefined8 *puParm1,undefined8 *puParm2);
void FUN_0040b48a(undefined8 *puParm1);
void FUN_0040b4a0(void);
char * FUN_0040b575(ulong uParm1,long lParm2,ulong uParm3,ulong uParm4,ulong uParm5);
undefined8 FUN_0040bc97(char *pcParm1,uint *puParm2,long *plParm3);
int * FUN_0040bdeb(int iParm1);
int * FUN_0040be76(int iParm1);
char * FUN_0040bf01(long lParm1,long lParm2);
char * FUN_0040bf8c(ulong uParm1,long lParm2);
undefined *FUN_0040bfc8(long lParm1,undefined *puParm2,long lParm3,undefined8 *puParm4,int iParm5,uint uParm6);
ulong FUN_0040c361(byte *pbParm1,long lParm2,ulong uParm3);
void FUN_0040c544(undefined8 uParm1,ulong uParm2);
void FUN_0040c568(undefined8 *puParm1,ulong uParm2,undefined8 *puParm3,code *pcParm4);
void FUN_0040c76f(long lParm1,long lParm2,undefined8 uParm3);
long FUN_0040c787(long lParm1,long lParm2,long lParm3);
char * FUN_0040c7b6(char *param_1,long param_2,char *param_3,undefined8 *param_4,undefined param_5,undefined8 param_6,undefined8 param_7,uint param_8);
void FUN_0040deb2(void);
void FUN_0040ded4(long lParm1);
int * FUN_0040df69(int *piParm1,int iParm2);
undefined * FUN_0040df99(char *pcParm1,int iParm2);
ulong FUN_0040e051(undefined *param_1,ulong param_2,long param_3,ulong param_4,uint param_5,ulong param_6,long param_7,char *param_8,char *param_9);
undefined1 * FUN_0040ee08(int iParm1,undefined8 uParm2,undefined8 uParm3,uint *puParm4);
undefined8 FUN_0040efaa(undefined1 *puParm1);
ulong FUN_0040efe4(undefined1 *puParm1);
void FUN_0040eff3(undefined1 *puParm1,undefined4 uParm2);
ulong FUN_0040f002(undefined1 *puParm1,byte bParm2,undefined8 uParm3);
undefined8 FUN_0040f036(undefined8 uParm1,undefined8 uParm2,undefined8 uParm3,undefined8 uParm4,undefined1 *puParm5);
void FUN_0040f0ad(uint uParm1,undefined8 uParm2,undefined8 uParm3);
void FUN_0040f0db(ulong uParm1,undefined8 uParm2);
void FUN_0040f0f3(undefined8 uParm1,undefined8 uParm2,char cParm3);
void FUN_0040f17c(undefined8 uParm1,char cParm2);
void FUN_0040f195(undefined8 uParm1);
void FUN_0040f1a8(uint uParm1,undefined8 uParm2,undefined8 uParm3);
void FUN_0040f237(void);
void FUN_0040f24a(undefined8 uParm1,undefined8 uParm2);
void FUN_0040f25f(undefined8 uParm1);
void FUN_0040f275(undefined8 uParm1,long lParm2,undefined8 uParm3,undefined8 uParm4,undefined8 *puParm5,undefined8 uParm6);
void FUN_0040f4b2(void);
void FUN_0040f504(void);
void FUN_0040f589(long lParm1);
long FUN_0040f5a3(long lParm1,long lParm2);
void FUN_0040f5d6(long lParm1,ulong *puParm2);
void FUN_0040f627(undefined8 uParm1,undefined8 uParm2);
void FUN_0040f650(undefined8 uParm1);
char * FUN_0040f667(void);
ulong FUN_0040f68f(undefined8 param_1,ulong param_2,ulong param_3,ulong param_4,undefined8 param_5,undefined8 param_6,uint param_7);
long FUN_0040f76c(void);
long FUN_0040f798(void);
ulong FUN_0040f842(ulong *puParm1,int iParm2,int iParm3);
ulong FUN_0040f8a0(byte *pbParm1,byte **ppbParm2,uint uParm3,ulong *puParm4,long lParm5);
ulong FUN_0040fbe9(uint uParm1);
ulong FUN_0040fc6c(ulong *puParm1,int iParm2,int iParm3);
ulong FUN_0040fcca(byte *pbParm1,byte **ppbParm2,uint uParm3,ulong *puParm4,long lParm5);
undefined8 FUN_00410013(ulong uParm1,ulong uParm2,ulong uParm3);
void FUN_0041005b(void);
void FUN_00410088(undefined8 uParm1);
void FUN_004100c8(uint uParm1,uint uParm2,undefined8 uParm3,undefined8 uParm4);
void FUN_0041011f(undefined4 uParm1,undefined4 uParm2,undefined4 uParm3,undefined4 uParm4,undefined4 uParm5,undefined4 uParm6,undefined4 uParm7,undefined4 uParm8,ulong param_1,ulong param_2,undefined8 param_3,undefined8 param_4,undefined8 param_5,undefined8 param_6);
int * FUN_004101f2(int *piParm1);
char * FUN_004102c5(char *pcParm1);
undefined8 FUN_00410398(int iParm1,long lParm2,ulong uParm3,ulong uParm4,undefined8 uParm5,uint uParm6);
ulong FUN_004109bc(uint *puParm1,uint *puParm2,uint *puParm3,byte bParm4,uint uParm5);
undefined8 FUN_0041146b(int iParm1,long lParm2,ulong uParm3,ulong uParm4,undefined8 uParm5,uint uParm6);
ulong FUN_00411a1f(byte *pbParm1,byte *pbParm2,byte *pbParm3,byte bParm4,uint uParm5);
ulong FUN_00412458(undefined8 uParm1,long lParm2,ulong uParm3);
long FUN_0041273d(long lParm1,ulong uParm2);
void FUN_00412bcd(long lParm1,int *piParm2);
ulong FUN_00412c8c(int param_1,undefined8 *param_2,char *param_3,long *param_4,int *param_5,int param_6,int *param_7,int param_8,undefined8 param_9);
ulong FUN_00413147(uint param_1,undefined8 *param_2,char *param_3,long param_4,undefined8 param_5,ulong param_6,uint *param_7,int param_8);
void FUN_004135c3(void);
void FUN_00413619(void);
void FUN_0041362f(void);
ulong FUN_004136db(long lParm1,long lParm2);
void FUN_00413744(long lParm1);
ulong FUN_0041375e(uint *puParm1,byte *pbParm2,long lParm3);
ulong * FUN_004137c3(ulong *puParm1,char cParm2,ulong uParm3);
void FUN_004138bb(long lParm1,undefined8 uParm2);
void FUN_004138dc(long lParm1,undefined8 uParm2);
undefined8 FUN_004138fd(long *plParm1,long lParm2,long lParm3);
void FUN_0041397a(void);
void FUN_004139a4(long lParm1,undefined8 uParm2,undefined8 uParm3,undefined8 uParm4,undefined8 uParm5);
void FUN_004139be(ulong *puParm1,long lParm2);
ulong FUN_00413a90(uint param_1,undefined8 param_2,uint param_3,uint param_4);
undefined8 FUN_00413b80(long lParm1,undefined8 uParm2,undefined8 uParm3);
void FUN_00413be2(long lParm1,long lParm2);
ulong FUN_00413c10(undefined4 uParm1,undefined4 uParm2,undefined4 uParm3,undefined4 uParm4,undefined4 uParm5,undefined4 uParm6,undefined4 uParm7,undefined4 uParm8,long param_1,ulong param_2,undefined8 param_3,undefined8 param_4,undefined8 param_5,undefined8 param_6);
ulong FUN_00413d1c(long lParm1,long lParm2);
char * FUN_00413d68(char *pcParm1,char *pcParm2,ulong uParm3);
void FUN_00413e19(long lParm1);
undefined8 * FUN_00413e45(long lParm1);
undefined8 FUN_00413ee8(long *plParm1,char *pcParm2);
void FUN_00414019(long *plParm1);
long FUN_00414038(long lParm1);
ulong FUN_004140e2(long lParm1);
long FUN_00414143(long lParm1,undefined8 uParm2,long lParm3);
long FUN_004141d1(long lParm1,uint *puParm2);
void FUN_004142cb(long lParm1);
void FUN_004142ea(void);
ulong FUN_00414392(char *pcParm1);
ulong FUN_004143eb(char *pcParm1,long lParm2);
long FUN_00414432(ulong uParm1,uint *puParm2,ulong uParm3,uint *puParm4,long *plParm5);
ulong FUN_00414556(void);
ulong FUN_00414588(void);
int * FUN_00414795(ulong uParm1,long lParm2,ulong uParm3,uint *puParm4,ulong *puParm5);
char * FUN_00414d78(int iParm1,long lParm2,long lParm3,long lParm4,uint uParm5);
void FUN_00415390(uint param_1,undefined8 param_2);
ulong FUN_00415556(void);
void FUN_00415769(undefined8 uParm1,uint uParm2);
undefined8 *FUN_0041590a(undefined8 *puParm1,undefined8 *puParm2,undefined8 *puParm3,undefined8 uParm4);
ulong FUN_0041b4a7(undefined8 uParm1,undefined8 uParm2,undefined8 uParm3);
ulong FUN_0041b574(int *piParm1,long lParm2);
void FUN_0041b5eb(ulong uParm1);
ulong FUN_0041b62b(ulong uParm1,char cParm2);
ulong FUN_0041b68b(undefined8 uParm1);
undefined8 FUN_0041b6ec(void);
ulong FUN_0041b6f4(char *pcParm1,ulong uParm2);
char * FUN_0041b729(void);
double FUN_0041ba64(int *piParm1);
void FUN_0041baac(int *param_1);
ulong FUN_0041bb2d(long lParm1,long lParm2,char cParm3);
undefined8 FUN_0041bb5f(long param_1,undefined8 param_2,char param_3,char param_4,char param_5,char param_6,char param_7,char param_8,char param_9);
ulong FUN_0041bbdf(uint uParm1,char *pcParm2);
undefined8 FUN_0041bf92(undefined8 uParm1);
ulong FUN_0041c015(uint param_1,undefined8 param_2,undefined8 param_3,undefined8 param_4);
undefined8 FUN_0041c184(ulong uParm1);
ulong FUN_0041c1d3(long lParm1);
undefined8 FUN_0041c263(void);
void FUN_0041c276(void);
undefined8 FUN_0041c284(ulong uParm1,long lParm2,long lParm3,ulong uParm4);
void FUN_0041c350(code *pcParm1,long *plParm2,undefined8 uParm3);
long FUN_0041c3f8(ulong param_1,long param_2,int param_3,int param_4,int param_5,uint param_6,int param_7,int param_8,int param_9,int param_10);
long FUN_0041c521(void);
long FUN_0041c5be(undefined8 *puParm1,code *pcParm2,long *plParm3);
void FUN_0041cbb9(undefined8 uParm1);
undefined8 FUN_0041cbd6(uint *puParm1,ulong *puParm2);
undefined8 FUN_0041cffb(byte *pbParm1,ulong *puParm2,byte **ppbParm3);
ulong FUN_0041db60(char *pcParm1,char *pcParm2,ulong uParm3);
ulong FUN_0041dc7d(void);
ulong FUN_0041dca5(void);
undefined8 * FUN_0041dcd6(ulong uParm1);
void FUN_0041dd4d(ulong uParm1);
void FUN_0041ddd0(void);
undefined8 _DT_FINI(void);
undefined FUN_0062d000();
undefined FUN_0062d008();
undefined FUN_0062d010();
undefined FUN_0062d018();
undefined FUN_0062d020();
undefined FUN_0062d028();
undefined FUN_0062d030();
undefined FUN_0062d038();
undefined FUN_0062d040();
undefined FUN_0062d048();
undefined FUN_0062d050();
undefined FUN_0062d058();
undefined FUN_0062d060();
undefined FUN_0062d068();
undefined FUN_0062d070();
undefined FUN_0062d078();
undefined FUN_0062d080();
undefined FUN_0062d088();
undefined FUN_0062d090();
undefined FUN_0062d098();
undefined FUN_0062d0a0();
undefined FUN_0062d0a8();
undefined FUN_0062d0b0();
undefined FUN_0062d0b8();
undefined FUN_0062d0c0();
undefined FUN_0062d0c8();
undefined FUN_0062d0d0();
undefined FUN_0062d0d8();
undefined FUN_0062d0e0();
undefined FUN_0062d0e8();
undefined FUN_0062d0f0();
undefined FUN_0062d0f8();
undefined FUN_0062d100();
undefined FUN_0062d108();
undefined FUN_0062d110();
undefined FUN_0062d118();
undefined FUN_0062d120();
undefined FUN_0062d128();
undefined FUN_0062d130();
undefined FUN_0062d138();
undefined FUN_0062d140();
undefined FUN_0062d148();
undefined FUN_0062d150();
undefined FUN_0062d158();
undefined FUN_0062d160();
undefined FUN_0062d168();
undefined FUN_0062d170();
undefined FUN_0062d178();
undefined FUN_0062d180();
undefined FUN_0062d188();
undefined FUN_0062d190();
undefined FUN_0062d198();
undefined FUN_0062d1a0();
undefined FUN_0062d1a8();
undefined FUN_0062d1b0();
undefined FUN_0062d1b8();
undefined FUN_0062d1c0();
undefined FUN_0062d1c8();
undefined FUN_0062d1d0();
undefined FUN_0062d1d8();
undefined FUN_0062d1e0();
undefined FUN_0062d1e8();
undefined FUN_0062d1f0();
undefined FUN_0062d1f8();
undefined FUN_0062d200();
undefined FUN_0062d208();
undefined FUN_0062d210();
undefined FUN_0062d218();
undefined FUN_0062d220();
undefined FUN_0062d228();
undefined FUN_0062d230();
undefined FUN_0062d238();
undefined FUN_0062d240();
undefined FUN_0062d248();
undefined FUN_0062d250();
undefined FUN_0062d258();
undefined FUN_0062d260();
undefined FUN_0062d268();
undefined FUN_0062d270();
undefined FUN_0062d278();
undefined FUN_0062d280();
undefined FUN_0062d288();
undefined FUN_0062d290();
undefined FUN_0062d298();
undefined FUN_0062d2a0();
undefined FUN_0062d2a8();
undefined FUN_0062d2b0();
undefined FUN_0062d2b8();
undefined FUN_0062d2c0();
undefined FUN_0062d2c8();
undefined FUN_0062d2d0();
undefined FUN_0062d2d8();
undefined FUN_0062d2e0();
undefined FUN_0062d2e8();
undefined FUN_0062d2f0();
undefined FUN_0062d2f8();
undefined FUN_0062d300();
undefined FUN_0062d308();
undefined FUN_0062d310();
undefined FUN_0062d318();
undefined FUN_0062d320();
undefined FUN_0062d328();
undefined FUN_0062d330();
undefined FUN_0062d338();
undefined FUN_0062d340();
undefined FUN_0062d348();
undefined FUN_0062d350();
undefined FUN_0062d358();
undefined FUN_0062d360();
undefined FUN_0062d368();
undefined FUN_0062d370();
undefined FUN_0062d378();
undefined FUN_0062d380();
undefined FUN_0062d388();
undefined FUN_0062d390();
undefined FUN_0062d398();
undefined FUN_0062d3a0();
undefined FUN_0062d3a8();
undefined FUN_0062d3b0();
undefined FUN_0062d3b8();
undefined FUN_0062d3c0();
undefined FUN_0062d3c8();
undefined FUN_0062d3d0();
undefined FUN_0062d3d8();
undefined FUN_0062d3e0();


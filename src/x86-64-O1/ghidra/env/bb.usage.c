
#include "env.h"

long null_ARRAY_0060fc0_0_8_;
long null_ARRAY_0060fc0_8_8_;
long null_ARRAY_0060fe4_0_8_;
long null_ARRAY_0060fe4_16_8_;
long null_ARRAY_0060fe4_24_8_;
long null_ARRAY_0060fe4_32_8_;
long null_ARRAY_0060fe4_40_8_;
long null_ARRAY_0060fe4_48_8_;
long null_ARRAY_0060fe4_8_8_;
long null_ARRAY_0060fe8_0_4_;
long null_ARRAY_0060fe8_16_8_;
long null_ARRAY_0060fe8_4_4_;
long null_ARRAY_0060fe8_8_4_;
long local_68_2_2_;
long local_6b_1_3_;
long local_6c_4_4_;
long local_6e_4_4_;
long local_6f_4_4_;
long local_70_0_2_;
long local_70_4_4_;
long local_71_0_1_;
long local_71_4_4_;
long local_72_4_4_;
long DAT_00000010;
long DAT_0040cdc0;
long DAT_0040cdc4;
long DAT_0040ce48;
long DAT_0040ce93;
long DAT_0040d3f8;
long DAT_0040d3fc;
long DAT_0040d400;
long DAT_0040d403;
long DAT_0040d405;
long DAT_0040d409;
long DAT_0040d40d;
long DAT_0040dbab;
long DAT_0040df55;
long DAT_0040e059;
long DAT_0040e05f;
long DAT_0040e071;
long DAT_0040e072;
long DAT_0040e090;
long DAT_0040e094;
long DAT_0040e11e;
long DAT_0060f7d8;
long DAT_0060f7e8;
long DAT_0060f7f8;
long DAT_0060fba8;
long DAT_0060fc10;
long DAT_0060fc14;
long DAT_0060fc18;
long DAT_0060fc1c;
long DAT_0060fc40;
long DAT_0060fc80;
long DAT_0060fc90;
long DAT_0060fca0;
long DAT_0060fca8;
long DAT_0060fcc0;
long DAT_0060fcc8;
long DAT_0060fd10;
long DAT_0060fd18;
long DAT_0060fd20;
long DAT_0060fd28;
long DAT_0060feb8;
long DAT_0060fec0;
long DAT_0060fec8;
long DAT_0060fed0;
long DAT_0060fee0;
long fde_0040ec88;
long null_ARRAY_0040d2c0;
long null_ARRAY_0040e540;
long null_ARRAY_0040e780;
long null_ARRAY_0060fc00;
long null_ARRAY_0060fce0;
long null_ARRAY_0060fd40;
long null_ARRAY_0060fe40;
long null_ARRAY_0060fe80;
long PTR_DAT_0060fba0;
long PTR_null_ARRAY_0060fbf8;
long stack0x00000008;
void
FUN_00401ace (uint uParm1)
{
  int iVar1;
  long lVar2;
  undefined8 uVar3;
  undefined **ppuVar4;
  undefined *puVar5;
  undefined *local_88;
  char *local_80;
  undefined8 local_78;
  char *local_70;
  char *local_68;
  char *local_60;
  char *local_58;
  char *local_50;
  char *local_48;
  char *local_40;
  char *local_38;
  char *local_30;
  undefined8 local_28;
  undefined8 local_20;

  ppuVar4 = &local_88;
  if (uParm1 != 0)
    {
      func_0x004016a0 (DAT_0060fca0,
		       "Try \'%s --help\' for more information.\n",
		       DAT_0060fd28);
      goto LAB_00401cc0;
    }
  func_0x00401530
    ("Usage: %s [OPTION]... [-] [NAME=VALUE]... [COMMAND [ARG]...]\n",
     DAT_0060fd28);
  uVar3 = DAT_0060fc40;
  func_0x004016b0
    ("Set each NAME to VALUE in the environment and run COMMAND.\n",
     DAT_0060fc40);
  func_0x004016b0
    ("\nMandatory arguments to long options are mandatory for short options too.\n",
     uVar3);
  func_0x004016b0
    ("  -i, --ignore-environment  start with an empty environment\n  -0, --null           end each output line with NUL, not newline\n  -u, --unset=NAME     remove variable from the environment\n",
     uVar3);
  func_0x004016b0 ("  -C, --chdir=DIR      change working directory to DIR\n",
		   uVar3);
  func_0x004016b0 ("      --help     display this help and exit\n", uVar3);
  func_0x004016b0 ("      --version  output version information and exit\n",
		   uVar3);
  func_0x004016b0
    ("\nA mere - implies -i.  If no COMMAND, print the resulting environment.\n",
     uVar3);
  local_88 = &DAT_0040cdc4;
  local_80 = "test invocation";
  local_78 = 0x40ce27;
  local_70 = "Multi-call invocation";
  local_68 = "sha224sum";
  local_60 = "sha2 utilities";
  local_58 = "sha256sum";
  local_50 = "sha2 utilities";
  local_48 = "sha384sum";
  local_40 = "sha2 utilities";
  local_38 = "sha512sum";
  local_30 = "sha2 utilities";
  local_28 = 0;
  local_20 = 0;
  puVar5 = &DAT_0040cdc4;
  do
    {
      iVar1 = func_0x00401790 (&DAT_0040cdc0, puVar5);
      if (iVar1 == 0)
	break;
      ppuVar4 = ppuVar4 + 2;
      puVar5 = *ppuVar4;
    }
  while (puVar5 != (undefined *) 0x0);
  puVar5 = ppuVar4[1];
  if (puVar5 == (undefined *) 0x0)
    {
      func_0x00401530 ("\n%s online help: <%s>\n", "GNU coreutils",
		       "https://www.gnu.org/software/coreutils/");
      lVar2 = func_0x004017f0 (5, 0);
      if (lVar2 == 0)
	goto LAB_00401cc7;
      iVar1 = func_0x00401710 (lVar2, &DAT_0040ce48, 3);
      if (iVar1 == 0)
	{
	  func_0x00401530 ("Full documentation at: <%s%s>\n",
			   "https://www.gnu.org/software/coreutils/",
			   &DAT_0040cdc0);
	  puVar5 = &DAT_0040cdc0;
	  uVar3 = 0x40cde0;
	  goto LAB_00401cae;
	}
      puVar5 = &DAT_0040cdc0;
    LAB_00401c6c:
      ;
      func_0x00401530
	("Report %s translation bugs to <https://translationproject.org/team/>\n",
	 &DAT_0040cdc0);
    }
  else
    {
      func_0x00401530 ("\n%s online help: <%s>\n", "GNU coreutils",
		       "https://www.gnu.org/software/coreutils/");
      lVar2 = func_0x004017f0 (5, 0);
      if ((lVar2 != 0)
	  && (iVar1 = func_0x00401710 (lVar2, &DAT_0040ce48, 3), iVar1 != 0))
	goto LAB_00401c6c;
    }
  func_0x00401530 ("Full documentation at: <%s%s>\n",
		   "https://www.gnu.org/software/coreutils/", &DAT_0040cdc0);
  uVar3 = 0x40e08f;
  if (puVar5 == &DAT_0040cdc0)
    {
      uVar3 = 0x40cde0;
    }
LAB_00401cae:
  ;
  do
    {
      func_0x00401530
	("or available locally via: info \'(coreutils) %s%s\'\n", puVar5,
	 uVar3);
    LAB_00401cc0:
      ;
      func_0x00401830 ((ulong) uParm1);
    LAB_00401cc7:
      ;
      func_0x00401530 ("Full documentation at: <%s%s>\n",
		       "https://www.gnu.org/software/coreutils/",
		       &DAT_0040cdc0);
      puVar5 = &DAT_0040cdc0;
      uVar3 = 0x40cde0;
    }
  while (true);
}

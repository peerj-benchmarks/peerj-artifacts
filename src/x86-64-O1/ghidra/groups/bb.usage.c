
#include "groups.h"

long null_ARRAY_0060fd4_0_8_;
long null_ARRAY_0060fd4_8_8_;
long null_ARRAY_0060ff4_0_8_;
long null_ARRAY_0060ff4_16_8_;
long null_ARRAY_0060ff4_24_8_;
long null_ARRAY_0060ff4_32_8_;
long null_ARRAY_0060ff4_40_8_;
long null_ARRAY_0060ff4_48_8_;
long null_ARRAY_0060ff4_8_8_;
long null_ARRAY_0060ff8_0_4_;
long null_ARRAY_0060ff8_16_8_;
long null_ARRAY_0060ff8_4_4_;
long null_ARRAY_0060ff8_8_4_;
long local_68_2_2_;
long local_6b_1_3_;
long local_6c_4_4_;
long local_6e_4_4_;
long local_6f_4_4_;
long local_70_0_2_;
long local_70_4_4_;
long local_71_0_1_;
long local_71_4_4_;
long local_72_4_4_;
long DAT_00000010;
long DAT_0040d047;
long DAT_0040d0cb;
long DAT_0040d4b8;
long DAT_0040d4bc;
long DAT_0040d4c0;
long DAT_0040d4c3;
long DAT_0040d4c5;
long DAT_0040d4c9;
long DAT_0040d4cd;
long DAT_0040dc6b;
long DAT_0040e015;
long DAT_0040e119;
long DAT_0040e11f;
long DAT_0040e131;
long DAT_0040e132;
long DAT_0040e150;
long DAT_0040e154;
long DAT_0040e1de;
long DAT_0060f8f8;
long DAT_0060f908;
long DAT_0060f918;
long DAT_0060fce8;
long DAT_0060fd50;
long DAT_0060fd54;
long DAT_0060fd58;
long DAT_0060fd5c;
long DAT_0060fd80;
long DAT_0060fd90;
long DAT_0060fda0;
long DAT_0060fda8;
long DAT_0060fdc0;
long DAT_0060fdc8;
long DAT_0060fe28;
long DAT_0060fe30;
long DAT_0060fe38;
long DAT_0060ffb8;
long DAT_0060ffc0;
long DAT_0060ffc8;
long DAT_0060ffd8;
long fde_0040ed50;
long null_ARRAY_0040d380;
long null_ARRAY_0040e600;
long null_ARRAY_0040e840;
long null_ARRAY_0060fd40;
long null_ARRAY_0060fde0;
long null_ARRAY_0060fe10;
long null_ARRAY_0060fe40;
long null_ARRAY_0060ff40;
long null_ARRAY_0060ff80;
long PTR_DAT_0060fce0;
long PTR_null_ARRAY_0060fd38;
long stack0x00000008;
void
FUN_00401c0e (uint uParm1)
{
  int iVar1;
  long lVar2;
  undefined8 uVar3;
  undefined **ppuVar4;
  char *pcVar5;
  undefined *puVar6;
  undefined *local_88;
  char *local_80;
  undefined8 local_78;
  char *local_70;
  char *local_68;
  char *local_60;
  char *local_58;
  char *local_50;
  char *local_48;
  char *local_40;
  char *local_38;
  char *local_30;
  undefined8 local_28;
  undefined8 local_20;

  ppuVar4 = &local_88;
  if (uParm1 != 0)
    {
      func_0x004017c0 (DAT_0060fda0,
		       "Try \'%s --help\' for more information.\n",
		       DAT_0060fe38);
      goto LAB_00401dcc;
    }
  func_0x00401620 ("Usage: %s [OPTION]... [USERNAME]...\n", DAT_0060fe38);
  uVar3 = DAT_0060fd80;
  func_0x004017d0
    ("Print group memberships for each USERNAME or, if no USERNAME is specified, for\nthe current process (which may differ if the groups database has changed).\n",
     DAT_0060fd80);
  func_0x004017d0 ("      --help     display this help and exit\n", uVar3);
  func_0x004017d0 ("      --version  output version information and exit\n",
		   uVar3);
  local_88 = &DAT_0040d047;
  local_80 = "test invocation";
  local_78 = 0x40d0aa;
  local_70 = "Multi-call invocation";
  local_68 = "sha224sum";
  local_60 = "sha2 utilities";
  local_58 = "sha256sum";
  local_50 = "sha2 utilities";
  local_48 = "sha384sum";
  local_40 = "sha2 utilities";
  local_38 = "sha512sum";
  local_30 = "sha2 utilities";
  local_28 = 0;
  local_20 = 0;
  puVar6 = &DAT_0040d047;
  do
    {
      iVar1 = func_0x004018b0 ("groups", puVar6);
      if (iVar1 == 0)
	break;
      ppuVar4 = ppuVar4 + 2;
      puVar6 = *ppuVar4;
    }
  while (puVar6 != (undefined *) 0x0);
  pcVar5 = ppuVar4[1];
  if (pcVar5 == (char *) 0x0)
    {
      func_0x00401620 ("\n%s online help: <%s>\n", "GNU coreutils",
		       "https://www.gnu.org/software/coreutils/");
      lVar2 = func_0x00401930 (5, 0);
      if (lVar2 == 0)
	goto LAB_00401dd3;
      iVar1 = func_0x00401820 (lVar2, &DAT_0040d0cb, 3);
      if (iVar1 == 0)
	{
	  func_0x00401620 ("Full documentation at: <%s%s>\n",
			   "https://www.gnu.org/software/coreutils/",
			   "groups");
	  pcVar5 = "groups";
	  uVar3 = 0x40d063;
	  goto LAB_00401dba;
	}
      pcVar5 = "groups";
    LAB_00401d78:
      ;
      func_0x00401620
	("Report %s translation bugs to <https://translationproject.org/team/>\n",
	 "groups");
    }
  else
    {
      func_0x00401620 ("\n%s online help: <%s>\n", "GNU coreutils",
		       "https://www.gnu.org/software/coreutils/");
      lVar2 = func_0x00401930 (5, 0);
      if ((lVar2 != 0)
	  && (iVar1 = func_0x00401820 (lVar2, &DAT_0040d0cb, 3), iVar1 != 0))
	goto LAB_00401d78;
    }
  func_0x00401620 ("Full documentation at: <%s%s>\n",
		   "https://www.gnu.org/software/coreutils/", "groups");
  uVar3 = 0x40e14f;
  if (pcVar5 == "groups")
    {
      uVar3 = 0x40d063;
    }
LAB_00401dba:
  ;
  do
    {
      func_0x00401620
	("or available locally via: info \'(coreutils) %s%s\'\n", pcVar5,
	 uVar3);
    LAB_00401dcc:
      ;
      func_0x00401980 ((ulong) uParm1);
    LAB_00401dd3:
      ;
      func_0x00401620 ("Full documentation at: <%s%s>\n",
		       "https://www.gnu.org/software/coreutils/", "groups");
      pcVar5 = "groups";
      uVar3 = 0x40d063;
    }
  while (true);
}

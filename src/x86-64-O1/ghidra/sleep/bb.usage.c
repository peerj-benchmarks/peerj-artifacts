
#include "sleep.h"

long null_ARRAY_0060f9e_0_8_;
long null_ARRAY_0060f9e_8_8_;
long null_ARRAY_0060fbc_0_8_;
long null_ARRAY_0060fbc_16_8_;
long null_ARRAY_0060fbc_24_8_;
long null_ARRAY_0060fbc_32_8_;
long null_ARRAY_0060fbc_40_8_;
long null_ARRAY_0060fbc_48_8_;
long null_ARRAY_0060fbc_8_8_;
long null_ARRAY_0060fc0_0_4_;
long null_ARRAY_0060fc0_16_8_;
long null_ARRAY_0060fc0_4_4_;
long null_ARRAY_0060fc0_8_4_;
long local_68_2_2_;
long local_6b_1_3_;
long local_6c_4_4_;
long local_6e_4_4_;
long local_6f_4_4_;
long local_70_0_2_;
long local_70_4_4_;
long local_71_0_1_;
long local_71_4_4_;
long local_72_4_4_;
long DAT_00000010;
long DAT_0040ccc6;
long DAT_0040cd4a;
long DAT_0040d0cd;
long DAT_0040d0e2;
long DAT_0040d1a0;
long DAT_0040d1a4;
long DAT_0040d1a8;
long DAT_0040d1ab;
long DAT_0040d1ad;
long DAT_0040d1b1;
long DAT_0040d1b5;
long DAT_0040d96b;
long DAT_0040dd15;
long DAT_0040de19;
long DAT_0040de1f;
long DAT_0040de31;
long DAT_0040de32;
long DAT_0040de50;
long DAT_0040def6;
long DAT_0060f5b0;
long DAT_0060f5c0;
long DAT_0060f5d0;
long DAT_0060f988;
long DAT_0060f9f0;
long DAT_0060f9f4;
long DAT_0060f9f8;
long DAT_0060f9fc;
long DAT_0060fa00;
long DAT_0060fa10;
long DAT_0060fa20;
long DAT_0060fa28;
long DAT_0060fa40;
long DAT_0060fa48;
long DAT_0060fa90;
long DAT_0060fa98;
long DAT_0060faa0;
long DAT_0060faa8;
long DAT_0060fc38;
long DAT_0060fc40;
long DAT_0060fc48;
long DAT_0060fc58;
long fde_0040ea38;
long null_ARRAY_0040d0a0;
long null_ARRAY_0040d100;
long null_ARRAY_0040e300;
long null_ARRAY_0040e530;
long null_ARRAY_0060f9e0;
long null_ARRAY_0060fa60;
long null_ARRAY_0060fac0;
long null_ARRAY_0060fbc0;
long null_ARRAY_0060fc00;
long PTR_DAT_0060f980;
long PTR_null_ARRAY_0060f9d8;
void
FUN_00401a7e (uint uParm1)
{
  int iVar1;
  long lVar2;
  undefined8 uVar3;
  undefined **ppuVar4;
  char *pcVar5;
  undefined *puVar6;
  undefined *local_88;
  char *local_80;
  undefined8 local_78;
  char *local_70;
  char *local_68;
  char *local_60;
  char *local_58;
  char *local_50;
  char *local_48;
  char *local_40;
  char *local_38;
  char *local_30;
  undefined8 local_28;
  undefined8 local_20;

  ppuVar4 = &local_88;
  if (uParm1 != 0)
    {
      func_0x00401660 (DAT_0060fa20,
		       "Try \'%s --help\' for more information.\n",
		       DAT_0060faa8);
      goto LAB_00401c32;
    }
  func_0x004014d0
    ("Usage: %s NUMBER[SUFFIX]...\n  or:  %s OPTION\nPause for NUMBER seconds.  SUFFIX may be \'s\' for seconds (the default),\n\'m\' for minutes, \'h\' for hours or \'d\' for days.  Unlike most implementations\nthat require NUMBER be an integer, here NUMBER may be an arbitrary floating\npoint number.  Given two or more arguments, pause for the amount of time\nspecified by the sum of their values.\n\n",
     DAT_0060faa8, DAT_0060faa8);
  uVar3 = DAT_0060fa00;
  func_0x00401670 ("      --help     display this help and exit\n",
		   DAT_0060fa00);
  func_0x00401670 ("      --version  output version information and exit\n",
		   uVar3);
  local_88 = &DAT_0040ccc6;
  local_80 = "test invocation";
  local_78 = 0x40cd29;
  local_70 = "Multi-call invocation";
  local_68 = "sha224sum";
  local_60 = "sha2 utilities";
  local_58 = "sha256sum";
  local_50 = "sha2 utilities";
  local_48 = "sha384sum";
  local_40 = "sha2 utilities";
  local_38 = "sha512sum";
  local_30 = "sha2 utilities";
  local_28 = 0;
  local_20 = 0;
  puVar6 = &DAT_0040ccc6;
  do
    {
      iVar1 = func_0x00401740 ("sleep", puVar6);
      if (iVar1 == 0)
	break;
      ppuVar4 = ppuVar4 + 2;
      puVar6 = *ppuVar4;
    }
  while (puVar6 != (undefined *) 0x0);
  pcVar5 = ppuVar4[1];
  if (pcVar5 == (char *) 0x0)
    {
      func_0x004014d0 ("\n%s online help: <%s>\n", "GNU coreutils",
		       "https://www.gnu.org/software/coreutils/");
      lVar2 = func_0x004017b0 (5, 0);
      if (lVar2 == 0)
	goto LAB_00401c39;
      iVar1 = func_0x004016c0 (lVar2, &DAT_0040cd4a, 3);
      if (iVar1 == 0)
	{
	  func_0x004014d0 ("Full documentation at: <%s%s>\n",
			   "https://www.gnu.org/software/coreutils/",
			   "sleep");
	  pcVar5 = "sleep";
	  uVar3 = 0x40cce2;
	  goto LAB_00401c20;
	}
      pcVar5 = "sleep";
    LAB_00401bde:
      ;
      func_0x004014d0
	("Report %s translation bugs to <https://translationproject.org/team/>\n",
	 "sleep");
    }
  else
    {
      func_0x004014d0 ("\n%s online help: <%s>\n", "GNU coreutils",
		       "https://www.gnu.org/software/coreutils/");
      lVar2 = func_0x004017b0 (5, 0);
      if ((lVar2 != 0)
	  && (iVar1 = func_0x004016c0 (lVar2, &DAT_0040cd4a, 3), iVar1 != 0))
	goto LAB_00401bde;
    }
  func_0x004014d0 ("Full documentation at: <%s%s>\n",
		   "https://www.gnu.org/software/coreutils/", "sleep");
  uVar3 = 0x40de4f;
  if (pcVar5 == "sleep")
    {
      uVar3 = 0x40cce2;
    }
LAB_00401c20:
  ;
  do
    {
      func_0x004014d0
	("or available locally via: info \'(coreutils) %s%s\'\n", pcVar5,
	 uVar3);
    LAB_00401c32:
      ;
      func_0x004017f0 ((ulong) uParm1);
    LAB_00401c39:
      ;
      func_0x004014d0 ("Full documentation at: <%s%s>\n",
		       "https://www.gnu.org/software/coreutils/", "sleep");
      pcVar5 = "sleep";
      uVar3 = 0x40cce2;
    }
  while (true);
}


#include "dirname.h"

long null_ARRAY_0060f4c_0_8_;
long null_ARRAY_0060f4c_8_8_;
long null_ARRAY_0060f6c_0_8_;
long null_ARRAY_0060f6c_16_8_;
long null_ARRAY_0060f6c_24_8_;
long null_ARRAY_0060f6c_32_8_;
long null_ARRAY_0060f6c_40_8_;
long null_ARRAY_0060f6c_48_8_;
long null_ARRAY_0060f6c_8_8_;
long null_ARRAY_0060f70_0_4_;
long null_ARRAY_0060f70_16_8_;
long null_ARRAY_0060f70_4_4_;
long null_ARRAY_0060f70_8_4_;
long local_68_2_2_;
long local_6b_1_3_;
long local_6c_4_4_;
long local_6e_4_4_;
long local_6f_4_4_;
long local_70_0_2_;
long local_70_4_4_;
long local_71_0_1_;
long local_71_4_4_;
long local_72_4_4_;
long DAT_00000010;
long DAT_0040c924;
long DAT_0040c9a8;
long DAT_0040c9ac;
long DAT_0040ccc0;
long DAT_0040cdd8;
long DAT_0040cddc;
long DAT_0040cde0;
long DAT_0040cde3;
long DAT_0040cde5;
long DAT_0040cde9;
long DAT_0040cded;
long DAT_0040d56b;
long DAT_0040d915;
long DAT_0040da19;
long DAT_0040da1f;
long DAT_0040da31;
long DAT_0040da32;
long DAT_0040da50;
long DAT_0040da54;
long DAT_0040dade;
long DAT_0060f0a0;
long DAT_0060f0b0;
long DAT_0060f0c0;
long DAT_0060f468;
long DAT_0060f4d0;
long DAT_0060f4d4;
long DAT_0060f4d8;
long DAT_0060f4dc;
long DAT_0060f500;
long DAT_0060f510;
long DAT_0060f520;
long DAT_0060f528;
long DAT_0060f540;
long DAT_0060f548;
long DAT_0060f590;
long DAT_0060f598;
long DAT_0060f5a0;
long DAT_0060f738;
long DAT_0060f740;
long DAT_0060f748;
long DAT_0060f758;
long fde_0040e618;
long null_ARRAY_0040cd00;
long null_ARRAY_0040df00;
long null_ARRAY_0040e140;
long null_ARRAY_0060f4c0;
long null_ARRAY_0060f560;
long null_ARRAY_0060f5c0;
long null_ARRAY_0060f6c0;
long null_ARRAY_0060f700;
long PTR_DAT_0060f460;
long PTR_null_ARRAY_0060f4b8;
long stack0x00000008;
void
FUN_00401a3e (uint uParm1)
{
  int iVar1;
  long lVar2;
  undefined8 uVar3;
  undefined **ppuVar4;
  char *pcVar5;
  undefined *puVar6;
  undefined *local_88;
  char *local_80;
  undefined8 local_78;
  char *local_70;
  char *local_68;
  char *local_60;
  char *local_58;
  char *local_50;
  char *local_48;
  char *local_40;
  char *local_38;
  char *local_30;
  undefined8 local_28;
  undefined8 local_20;

  ppuVar4 = &local_88;
  if (uParm1 != 0)
    {
      func_0x00401610 (DAT_0060f520,
		       "Try \'%s --help\' for more information.\n",
		       DAT_0060f5a0);
      goto LAB_00401c25;
    }
  func_0x004014a0 ("Usage: %s [OPTION] NAME...\n", DAT_0060f5a0);
  uVar3 = DAT_0060f500;
  func_0x00401620
    ("Output each NAME with its last non-slash component and trailing slashes\nremoved; if NAME contains no /\'s, output \'.\' (meaning the current directory).\n\n",
     DAT_0060f500);
  func_0x00401620
    ("  -z, --zero     end each output line with NUL, not newline\n", uVar3);
  func_0x00401620 ("      --help     display this help and exit\n", uVar3);
  func_0x00401620 ("      --version  output version information and exit\n",
		   uVar3);
  func_0x004014a0
    ("\nExamples:\n  %s /usr/bin/          -> \"/usr\"\n  %s dir1/str dir2/str  -> \"dir1\" followed by \"dir2\"\n  %s stdio.h            -> \".\"\n",
     DAT_0060f5a0, DAT_0060f5a0, DAT_0060f5a0);
  local_88 = &DAT_0040c924;
  local_80 = "test invocation";
  local_78 = 0x40c987;
  local_70 = "Multi-call invocation";
  local_68 = "sha224sum";
  local_60 = "sha2 utilities";
  local_58 = "sha256sum";
  local_50 = "sha2 utilities";
  local_48 = "sha384sum";
  local_40 = "sha2 utilities";
  local_38 = "sha512sum";
  local_30 = "sha2 utilities";
  local_28 = 0;
  local_20 = 0;
  puVar6 = &DAT_0040c924;
  do
    {
      iVar1 = func_0x004016f0 ("dirname", puVar6);
      if (iVar1 == 0)
	break;
      ppuVar4 = ppuVar4 + 2;
      puVar6 = *ppuVar4;
    }
  while (puVar6 != (undefined *) 0x0);
  pcVar5 = ppuVar4[1];
  if (pcVar5 == (char *) 0x0)
    {
      func_0x004014a0 ("\n%s online help: <%s>\n", "GNU coreutils",
		       "https://www.gnu.org/software/coreutils/");
      lVar2 = func_0x00401750 (5, 0);
      if (lVar2 == 0)
	goto LAB_00401c2c;
      iVar1 = func_0x00401670 (lVar2, &DAT_0040c9a8, 3);
      if (iVar1 == 0)
	{
	  func_0x004014a0 ("Full documentation at: <%s%s>\n",
			   "https://www.gnu.org/software/coreutils/",
			   "dirname");
	  pcVar5 = "dirname";
	  uVar3 = 0x40c940;
	  goto LAB_00401c13;
	}
      pcVar5 = "dirname";
    LAB_00401bd1:
      ;
      func_0x004014a0
	("Report %s translation bugs to <https://translationproject.org/team/>\n",
	 "dirname");
    }
  else
    {
      func_0x004014a0 ("\n%s online help: <%s>\n", "GNU coreutils",
		       "https://www.gnu.org/software/coreutils/");
      lVar2 = func_0x00401750 (5, 0);
      if ((lVar2 != 0)
	  && (iVar1 = func_0x00401670 (lVar2, &DAT_0040c9a8, 3), iVar1 != 0))
	goto LAB_00401bd1;
    }
  func_0x004014a0 ("Full documentation at: <%s%s>\n",
		   "https://www.gnu.org/software/coreutils/", "dirname");
  uVar3 = 0x40da4f;
  if (pcVar5 == "dirname")
    {
      uVar3 = 0x40c940;
    }
LAB_00401c13:
  ;
  do
    {
      func_0x004014a0
	("or available locally via: info \'(coreutils) %s%s\'\n", pcVar5,
	 uVar3);
    LAB_00401c25:
      ;
      func_0x004017b0 ((ulong) uParm1);
    LAB_00401c2c:
      ;
      func_0x004014a0 ("Full documentation at: <%s%s>\n",
		       "https://www.gnu.org/software/coreutils/", "dirname");
      pcVar5 = "dirname";
      uVar3 = 0x40c940;
    }
  while (true);
}


#include "echo.h"

long null_ARRAY_0060f40_0_8_;
long null_ARRAY_0060f40_8_8_;
long null_ARRAY_0060f60_0_8_;
long null_ARRAY_0060f60_16_8_;
long null_ARRAY_0060f60_24_8_;
long null_ARRAY_0060f60_32_8_;
long null_ARRAY_0060f60_40_8_;
long null_ARRAY_0060f60_48_8_;
long null_ARRAY_0060f60_8_8_;
long local_68_2_2_;
long local_6b_1_3_;
long local_6c_4_4_;
long local_6e_4_4_;
long local_6f_4_4_;
long local_70_0_2_;
long local_70_4_4_;
long local_71_0_1_;
long local_71_4_4_;
long local_72_4_4_;
long DAT_0040c3f8;
long DAT_0040c3fd;
long DAT_0040c481;
long DAT_0040c495;
long DAT_0040c9f0;
long DAT_0040c9f4;
long DAT_0040c9f6;
long DAT_0040c9fa;
long DAT_0040c9fd;
long DAT_0040c9ff;
long DAT_0040ca03;
long DAT_0040ca07;
long DAT_0040d1ab;
long DAT_0040d555;
long DAT_0040d566;
long DAT_0040d5ee;
long DAT_0060f000;
long DAT_0060f010;
long DAT_0060f020;
long DAT_0060f3a8;
long DAT_0060f410;
long DAT_0060f440;
long DAT_0060f450;
long DAT_0060f460;
long DAT_0060f468;
long DAT_0060f480;
long DAT_0060f488;
long DAT_0060f4d0;
long DAT_0060f4d8;
long DAT_0060f4e0;
long DAT_0060f638;
long DAT_0060f640;
long DAT_0060f648;
long fde_0040e0e8;
long null_ARRAY_0040da00;
long null_ARRAY_0040dc40;
long null_ARRAY_0060f400;
long null_ARRAY_0060f4a0;
long null_ARRAY_0060f500;
long null_ARRAY_0060f600;
long PTR_DAT_0060f3a0;
long PTR_null_ARRAY_0060f3f8;
void
FUN_0040198c (uint uParm1)
{
  int iVar1;
  long lVar2;
  undefined8 uVar3;
  undefined **ppuVar4;
  undefined *puVar5;
  undefined *local_88;
  char *local_80;
  undefined8 local_78;
  char *local_70;
  char *local_68;
  char *local_60;
  char *local_58;
  char *local_50;
  char *local_48;
  char *local_40;
  char *local_38;
  char *local_30;
  undefined8 local_28;
  undefined8 local_20;

  ppuVar4 = &local_88;
  if (uParm1 != 0)
    {
      func_0x00401560 (DAT_0060f460,
		       "Try \'%s --help\' for more information.\n",
		       DAT_0060f4e0);
      goto LAB_00401b95;
    }
  func_0x004013e0
    ("Usage: %s [SHORT-OPTION]... [STRING]...\n  or:  %s LONG-OPTION\n",
     DAT_0060f4e0, DAT_0060f4e0);
  uVar3 = DAT_0060f440;
  func_0x00401570
    ("Echo the STRING(s) to standard output.\n\n  -n             do not output the trailing newline\n",
     DAT_0060f440);
  func_0x00401570
    ("  -e             enable interpretation of backslash escapes\n  -E             disable interpretation of backslash escapes (default)\n",
     uVar3);
  func_0x00401570 ("      --help     display this help and exit\n", uVar3);
  func_0x00401570 ("      --version  output version information and exit\n",
		   uVar3);
  func_0x00401570
    ("\nIf -e is in effect, the following sequences are recognized:\n\n",
     uVar3);
  func_0x00401570
    ("  \\\\      backslash\n  \\a      alert (BEL)\n  \\b      backspace\n  \\c      produce no further output\n  \\e      escape\n  \\f      form feed\n  \\n      new line\n  \\r      carriage return\n  \\t      horizontal tab\n  \\v      vertical tab\n",
     uVar3);
  func_0x00401570
    ("  \\0NNN   byte with octal value NNN (1 to 3 digits)\n  \\xHH    byte with hexadecimal value HH (1 to 2 digits)\n",
     uVar3);
  func_0x004013e0
    ("\nNOTE: your shell may have its own version of %s, which usually supersedes\nthe version described here.  Please refer to your shell\'s documentation\nfor details about the options it supports.\n",
     &DAT_0040c3f8);
  local_88 = &DAT_0040c3fd;
  local_80 = "test invocation";
  local_78 = 0x40c460;
  local_70 = "Multi-call invocation";
  local_68 = "sha224sum";
  local_60 = "sha2 utilities";
  local_58 = "sha256sum";
  local_50 = "sha2 utilities";
  local_48 = "sha384sum";
  local_40 = "sha2 utilities";
  local_38 = "sha512sum";
  local_30 = "sha2 utilities";
  local_28 = 0;
  local_20 = 0;
  puVar5 = &DAT_0040c3fd;
  do
    {
      iVar1 = func_0x00401630 (&DAT_0040c3f8, puVar5);
      if (iVar1 == 0)
	break;
      ppuVar4 = ppuVar4 + 2;
      puVar5 = *ppuVar4;
    }
  while (puVar5 != (undefined *) 0x0);
  puVar5 = ppuVar4[1];
  if (puVar5 == (undefined *) 0x0)
    {
      func_0x004013e0 ("\n%s online help: <%s>\n", "GNU coreutils",
		       "https://www.gnu.org/software/coreutils/");
      lVar2 = func_0x00401680 (5, 0);
      if (lVar2 == 0)
	goto LAB_00401b9c;
      iVar1 = func_0x004015c0 (lVar2, &DAT_0040c481, 3);
      if (iVar1 == 0)
	{
	  func_0x004013e0 ("Full documentation at: <%s%s>\n",
			   "https://www.gnu.org/software/coreutils/",
			   &DAT_0040c3f8);
	  puVar5 = &DAT_0040c3f8;
	  uVar3 = 0x40c419;
	  goto LAB_00401b83;
	}
      puVar5 = &DAT_0040c3f8;
    LAB_00401b41:
      ;
      func_0x004013e0
	("Report %s translation bugs to <https://translationproject.org/team/>\n",
	 &DAT_0040c3f8);
    }
  else
    {
      func_0x004013e0 ("\n%s online help: <%s>\n", "GNU coreutils",
		       "https://www.gnu.org/software/coreutils/");
      lVar2 = func_0x00401680 (5, 0);
      if ((lVar2 != 0)
	  && (iVar1 = func_0x004015c0 (lVar2, &DAT_0040c481, 3), iVar1 != 0))
	goto LAB_00401b41;
    }
  func_0x004013e0 ("Full documentation at: <%s%s>\n",
		   "https://www.gnu.org/software/coreutils/", &DAT_0040c3f8);
  uVar3 = 0x40d1f1;
  if (puVar5 == &DAT_0040c3f8)
    {
      uVar3 = 0x40c419;
    }
LAB_00401b83:
  ;
  do
    {
      func_0x004013e0
	("or available locally via: info \'(coreutils) %s%s\'\n", puVar5,
	 uVar3);
    LAB_00401b95:
      ;
      func_0x004016d0 ((ulong) uParm1);
    LAB_00401b9c:
      ;
      func_0x004013e0 ("Full documentation at: <%s%s>\n",
		       "https://www.gnu.org/software/coreutils/",
		       &DAT_0040c3f8);
      puVar5 = &DAT_0040c3f8;
      uVar3 = 0x40c419;
    }
  while (true);
}

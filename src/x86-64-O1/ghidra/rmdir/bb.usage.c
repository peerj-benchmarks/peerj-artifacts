
#include "rmdir.h"

long null_ARRAY_0061044_0_8_;
long null_ARRAY_0061044_8_8_;
long null_ARRAY_0061064_0_8_;
long null_ARRAY_0061064_16_8_;
long null_ARRAY_0061064_24_8_;
long null_ARRAY_0061064_32_8_;
long null_ARRAY_0061064_40_8_;
long null_ARRAY_0061064_48_8_;
long null_ARRAY_0061064_8_8_;
long null_ARRAY_0061068_0_4_;
long null_ARRAY_0061068_16_8_;
long null_ARRAY_0061068_4_4_;
long null_ARRAY_0061068_8_4_;
long local_68_2_2_;
long local_6b_1_3_;
long local_6c_4_4_;
long local_6e_4_4_;
long local_6f_4_4_;
long local_70_0_2_;
long local_70_4_4_;
long local_71_0_1_;
long local_71_4_4_;
long local_72_4_4_;
long DAT_00000010;
long DAT_0040d3a0;
long DAT_0040d3a4;
long DAT_0040d3a6;
long DAT_0040d42a;
long DAT_0040d43e;
long DAT_0040d838;
long DAT_0040d83c;
long DAT_0040d840;
long DAT_0040d843;
long DAT_0040d845;
long DAT_0040d849;
long DAT_0040d84d;
long DAT_0040dfeb;
long DAT_0040e395;
long DAT_0040e397;
long DAT_0040e499;
long DAT_0040e49f;
long DAT_0040e4b1;
long DAT_0040e4b2;
long DAT_0040e4d0;
long DAT_0040e4d4;
long DAT_0040e55e;
long DAT_00610000;
long DAT_00610010;
long DAT_00610020;
long DAT_006103e8;
long DAT_00610450;
long DAT_00610454;
long DAT_00610458;
long DAT_0061045c;
long DAT_00610480;
long DAT_00610490;
long DAT_006104a0;
long DAT_006104a8;
long DAT_006104c0;
long DAT_006104c8;
long DAT_00610510;
long DAT_00610511;
long DAT_00610512;
long DAT_00610518;
long DAT_00610520;
long DAT_00610528;
long DAT_006106b8;
long DAT_006106c0;
long DAT_006106c8;
long DAT_006106d0;
long DAT_006106d8;
long DAT_006106e8;
long fde_0040f0f0;
long null_ARRAY_0040d2c0;
long null_ARRAY_0040e980;
long null_ARRAY_0040ebc0;
long null_ARRAY_00610440;
long null_ARRAY_006104e0;
long null_ARRAY_00610540;
long null_ARRAY_00610640;
long null_ARRAY_00610680;
long PTR_DAT_006103e0;
long PTR_null_ARRAY_00610438;
void
FUN_00401cb6 (uint uParm1)
{
  int iVar1;
  long lVar2;
  undefined8 uVar3;
  undefined **ppuVar4;
  undefined *puVar5;
  undefined *local_88;
  char *local_80;
  undefined8 local_78;
  char *local_70;
  char *local_68;
  char *local_60;
  char *local_58;
  char *local_50;
  char *local_48;
  char *local_40;
  char *local_38;
  char *local_30;
  undefined8 local_28;
  undefined8 local_20;

  ppuVar4 = &local_88;
  if (uParm1 != 0)
    {
      func_0x00401760 (DAT_006104a0,
		       "Try \'%s --help\' for more information.\n",
		       DAT_00610528);
      goto LAB_00401e81;
    }
  func_0x004015b0 ("Usage: %s [OPTION]... DIRECTORY...\n", DAT_00610528);
  uVar3 = DAT_00610480;
  func_0x00401770
    ("Remove the DIRECTORY(ies), if they are empty.\n\n      --ignore-fail-on-non-empty\n                  ignore each failure that is solely because a directory\n                    is non-empty\n",
     DAT_00610480);
  func_0x00401770
    ("  -p, --parents   remove DIRECTORY and its ancestors; e.g., \'rmdir -p a/b/c\' is\n                    similar to \'rmdir a/b/c a/b a\'\n  -v, --verbose   output a diagnostic for every directory processed\n",
     uVar3);
  func_0x00401770 ("      --help     display this help and exit\n", uVar3);
  func_0x00401770 ("      --version  output version information and exit\n",
		   uVar3);
  local_88 = &DAT_0040d3a6;
  local_80 = "test invocation";
  local_78 = 0x40d409;
  local_70 = "Multi-call invocation";
  local_68 = "sha224sum";
  local_60 = "sha2 utilities";
  local_58 = "sha256sum";
  local_50 = "sha2 utilities";
  local_48 = "sha384sum";
  local_40 = "sha2 utilities";
  local_38 = "sha512sum";
  local_30 = "sha2 utilities";
  local_28 = 0;
  local_20 = 0;
  puVar5 = &DAT_0040d3a6;
  do
    {
      iVar1 = func_0x00401840 (&DAT_0040d3a0, puVar5);
      if (iVar1 == 0)
	break;
      ppuVar4 = ppuVar4 + 2;
      puVar5 = *ppuVar4;
    }
  while (puVar5 != (undefined *) 0x0);
  puVar5 = ppuVar4[1];
  if (puVar5 == (undefined *) 0x0)
    {
      func_0x004015b0 ("\n%s online help: <%s>\n", "GNU coreutils",
		       "https://www.gnu.org/software/coreutils/");
      lVar2 = func_0x004018a0 (5, 0);
      if (lVar2 == 0)
	goto LAB_00401e88;
      iVar1 = func_0x004017c0 (lVar2, &DAT_0040d42a, 3);
      if (iVar1 == 0)
	{
	  func_0x004015b0 ("Full documentation at: <%s%s>\n",
			   "https://www.gnu.org/software/coreutils/",
			   &DAT_0040d3a0);
	  puVar5 = &DAT_0040d3a0;
	  uVar3 = 0x40d3c2;
	  goto LAB_00401e6f;
	}
      puVar5 = &DAT_0040d3a0;
    LAB_00401e2d:
      ;
      func_0x004015b0
	("Report %s translation bugs to <https://translationproject.org/team/>\n",
	 &DAT_0040d3a0);
    }
  else
    {
      func_0x004015b0 ("\n%s online help: <%s>\n", "GNU coreutils",
		       "https://www.gnu.org/software/coreutils/");
      lVar2 = func_0x004018a0 (5, 0);
      if ((lVar2 != 0)
	  && (iVar1 = func_0x004017c0 (lVar2, &DAT_0040d42a, 3), iVar1 != 0))
	goto LAB_00401e2d;
    }
  func_0x004015b0 ("Full documentation at: <%s%s>\n",
		   "https://www.gnu.org/software/coreutils/", &DAT_0040d3a0);
  uVar3 = 0x40e4cf;
  if (puVar5 == &DAT_0040d3a0)
    {
      uVar3 = 0x40d3c2;
    }
LAB_00401e6f:
  ;
  do
    {
      func_0x004015b0
	("or available locally via: info \'(coreutils) %s%s\'\n", puVar5,
	 uVar3);
    LAB_00401e81:
      ;
      func_0x004018e0 ((ulong) uParm1);
    LAB_00401e88:
      ;
      func_0x004015b0 ("Full documentation at: <%s%s>\n",
		       "https://www.gnu.org/software/coreutils/",
		       &DAT_0040d3a0);
      puVar5 = &DAT_0040d3a0;
      uVar3 = 0x40d3c2;
    }
  while (true);
}


#include "tee.h"

long null_ARRAY_006106a_0_8_;
long null_ARRAY_006106a_8_8_;
long null_ARRAY_0061088_0_8_;
long null_ARRAY_0061088_16_8_;
long null_ARRAY_0061088_24_8_;
long null_ARRAY_0061088_32_8_;
long null_ARRAY_0061088_40_8_;
long null_ARRAY_0061088_48_8_;
long null_ARRAY_0061088_8_8_;
long null_ARRAY_006108c_0_4_;
long null_ARRAY_006108c_16_8_;
long null_ARRAY_006108c_4_4_;
long null_ARRAY_006108c_8_4_;
long local_68_2_2_;
long local_6b_1_3_;
long local_6c_4_4_;
long local_6e_4_4_;
long local_6f_4_4_;
long local_70_0_2_;
long local_70_4_4_;
long local_71_0_1_;
long local_71_4_4_;
long local_72_4_4_;
long DAT_00000010;
long DAT_0040d440;
long DAT_0040d442;
long DAT_0040d45f;
long DAT_0040d463;
long DAT_0040d4e7;
long DAT_0040d52a;
long DAT_0040dc1a;
long DAT_0040dc1c;
long DAT_0040dc78;
long DAT_0040dc7c;
long DAT_0040dc80;
long DAT_0040dc83;
long DAT_0040dc85;
long DAT_0040dc89;
long DAT_0040dc8d;
long DAT_0040e42b;
long DAT_0040e7d5;
long DAT_0040e8d9;
long DAT_0040e8df;
long DAT_0040e8f1;
long DAT_0040e8f2;
long DAT_0040e910;
long DAT_0040e914;
long DAT_0040e99e;
long DAT_00610258;
long DAT_00610268;
long DAT_00610278;
long DAT_00610650;
long DAT_006106b0;
long DAT_006106b4;
long DAT_006106b8;
long DAT_006106bc;
long DAT_006106c0;
long DAT_006106c8;
long DAT_006106d0;
long DAT_006106e0;
long DAT_006106e8;
long DAT_00610700;
long DAT_00610708;
long DAT_00610750;
long DAT_00610754;
long DAT_00610755;
long DAT_00610758;
long DAT_00610760;
long DAT_00610768;
long DAT_006108b8;
long DAT_006108f8;
long DAT_00610900;
long DAT_00610908;
long DAT_00610918;
long fde_0040f548;
long null_ARRAY_0040da80;
long null_ARRAY_0040daa0;
long null_ARRAY_0040db00;
long null_ARRAY_0040edc0;
long null_ARRAY_0040f000;
long null_ARRAY_006106a0;
long null_ARRAY_00610720;
long null_ARRAY_00610780;
long null_ARRAY_00610880;
long null_ARRAY_006108c0;
long PTR_DAT_00610640;
long PTR_FUN_00610648;
long PTR_null_ARRAY_00610698;
void
FUN_00401f92 (uint uParm1)
{
  int iVar1;
  long lVar2;
  undefined8 uVar3;
  undefined **ppuVar4;
  undefined *puVar5;
  undefined *local_88;
  char *local_80;
  undefined8 local_78;
  char *local_70;
  char *local_68;
  char *local_60;
  char *local_58;
  char *local_50;
  char *local_48;
  char *local_40;
  char *local_38;
  char *local_30;
  undefined8 local_28;
  undefined8 local_20;

  ppuVar4 = &local_88;
  if (uParm1 != 0)
    {
      func_0x004017a0 (DAT_006106e0,
		       "Try \'%s --help\' for more information.\n",
		       DAT_00610768);
      goto LAB_0040216a;
    }
  func_0x00401610 ("Usage: %s [OPTION]... [FILE]...\n", DAT_00610768);
  uVar3 = DAT_006106c0;
  func_0x004017b0
    ("Copy standard input to each FILE, and also to standard output.\n\n  -a, --append              append to the given FILEs, do not overwrite\n  -i, --ignore-interrupts   ignore interrupt signals\n",
     DAT_006106c0);
  func_0x004017b0
    ("  -p                        diagnose errors writing to non pipes\n      --output-error[=MODE]   set behavior on write error.  See MODE below\n",
     uVar3);
  func_0x004017b0 ("      --help     display this help and exit\n", uVar3);
  func_0x004017b0 ("      --version  output version information and exit\n",
		   uVar3);
  func_0x004017b0
    ("\nMODE determines behavior with write errors on the outputs:\n  \'warn\'         diagnose errors writing to any output\n  \'warn-nopipe\'  diagnose errors writing to any output not a pipe\n  \'exit\'         exit on error writing to any output\n  \'exit-nopipe\'  exit on error writing to any output not a pipe\nThe default MODE for the -p option is \'warn-nopipe\'.\nThe default operation when --output-error is not specified, is to\nexit immediately on error writing to a pipe, and diagnose errors\nwriting to non pipe outputs.\n",
     uVar3);
  local_88 = &DAT_0040d463;
  local_80 = "test invocation";
  local_78 = 0x40d4c6;
  local_70 = "Multi-call invocation";
  local_68 = "sha224sum";
  local_60 = "sha2 utilities";
  local_58 = "sha256sum";
  local_50 = "sha2 utilities";
  local_48 = "sha384sum";
  local_40 = "sha2 utilities";
  local_38 = "sha512sum";
  local_30 = "sha2 utilities";
  local_28 = 0;
  local_20 = 0;
  puVar5 = &DAT_0040d463;
  do
    {
      iVar1 = func_0x004018c0 (&DAT_0040d45f, puVar5);
      if (iVar1 == 0)
	break;
      ppuVar4 = ppuVar4 + 2;
      puVar5 = *ppuVar4;
    }
  while (puVar5 != (undefined *) 0x0);
  puVar5 = ppuVar4[1];
  if (puVar5 == (undefined *) 0x0)
    {
      func_0x00401610 ("\n%s online help: <%s>\n", "GNU coreutils",
		       "https://www.gnu.org/software/coreutils/");
      lVar2 = func_0x00401920 (5, 0);
      if (lVar2 == 0)
	goto LAB_00402171;
      iVar1 = func_0x00401830 (lVar2, &DAT_0040d4e7, 3);
      if (iVar1 == 0)
	{
	  func_0x00401610 ("Full documentation at: <%s%s>\n",
			   "https://www.gnu.org/software/coreutils/",
			   &DAT_0040d45f);
	  puVar5 = &DAT_0040d45f;
	  uVar3 = 0x40d47f;
	  goto LAB_00402158;
	}
      puVar5 = &DAT_0040d45f;
    LAB_00402116:
      ;
      func_0x00401610
	("Report %s translation bugs to <https://translationproject.org/team/>\n",
	 &DAT_0040d45f);
    }
  else
    {
      func_0x00401610 ("\n%s online help: <%s>\n", "GNU coreutils",
		       "https://www.gnu.org/software/coreutils/");
      lVar2 = func_0x00401920 (5, 0);
      if ((lVar2 != 0)
	  && (iVar1 = func_0x00401830 (lVar2, &DAT_0040d4e7, 3), iVar1 != 0))
	goto LAB_00402116;
    }
  func_0x00401610 ("Full documentation at: <%s%s>\n",
		   "https://www.gnu.org/software/coreutils/", &DAT_0040d45f);
  uVar3 = 0x40e90f;
  if (puVar5 == &DAT_0040d45f)
    {
      uVar3 = 0x40d47f;
    }
LAB_00402158:
  ;
  do
    {
      func_0x00401610
	("or available locally via: info \'(coreutils) %s%s\'\n", puVar5,
	 uVar3);
    LAB_0040216a:
      ;
      func_0x00401970 ((ulong) uParm1);
    LAB_00402171:
      ;
      func_0x00401610 ("Full documentation at: <%s%s>\n",
		       "https://www.gnu.org/software/coreutils/",
		       &DAT_0040d45f);
      puVar5 = &DAT_0040d45f;
      uVar3 = 0x40d47f;
    }
  while (true);
}


#include "rm.h"

long null_ARRAY_006164c_0_8_;
long null_ARRAY_006164c_8_8_;
long null_ARRAY_006166c_0_8_;
long null_ARRAY_006166c_16_8_;
long null_ARRAY_006166c_24_8_;
long null_ARRAY_006166c_32_8_;
long null_ARRAY_006166c_40_8_;
long null_ARRAY_006166c_48_8_;
long null_ARRAY_006166c_8_8_;
long null_ARRAY_0061670_0_4_;
long null_ARRAY_0061670_16_8_;
long null_ARRAY_0061670_4_4_;
long null_ARRAY_0061670_8_4_;
long local_68_2_2_;
long local_6b_1_3_;
long local_6c_4_4_;
long local_6e_4_4_;
long local_6f_4_4_;
long local_70_0_2_;
long local_70_4_4_;
long local_71_0_1_;
long local_71_4_4_;
long local_72_4_4_;
long DAT_00000010;
long DAT_004116c0;
long DAT_004116c3;
long DAT_00411747;
long DAT_00412100;
long DAT_00412356;
long DAT_00412357;
long DAT_00412705;
long DAT_00412707;
long DAT_0041277e;
long DAT_004127e8;
long DAT_004127ee;
long DAT_004127f0;
long DAT_004127f4;
long DAT_004127f8;
long DAT_004127fb;
long DAT_004127fd;
long DAT_00412801;
long DAT_00412805;
long DAT_00412fab;
long DAT_0041335a;
long DAT_00413531;
long DAT_00413537;
long DAT_00413549;
long DAT_0041354a;
long DAT_00413568;
long DAT_004135a2;
long DAT_0041365e;
long DAT_00616000;
long DAT_00616010;
long DAT_00616020;
long DAT_00616470;
long DAT_006164d0;
long DAT_006164d4;
long DAT_006164d8;
long DAT_006164dc;
long DAT_00616500;
long DAT_00616508;
long DAT_00616510;
long DAT_00616520;
long DAT_00616528;
long DAT_00616540;
long DAT_00616548;
long DAT_006165a0;
long DAT_006165a8;
long DAT_006165b0;
long DAT_006165b8;
long DAT_006166f8;
long DAT_006166f9;
long DAT_00616738;
long DAT_00616740;
long DAT_00616748;
long DAT_00616750;
long DAT_00616758;
long DAT_00616768;
long fde_00414420;
long int7;
long null_ARRAY_00412110;
long null_ARRAY_00412140;
long null_ARRAY_00412180;
long null_ARRAY_004135b0;
long null_ARRAY_00413a80;
long null_ARRAY_00413cb0;
long null_ARRAY_006164c0;
long null_ARRAY_00616560;
long null_ARRAY_00616590;
long null_ARRAY_006165c0;
long null_ARRAY_006166c0;
long null_ARRAY_00616700;
long PTR_DAT_00616460;
long PTR_FUN_00616468;
long PTR_null_ARRAY_006164b8;
void
FUN_0040200e (uint uParm1)
{
  int iVar1;
  long lVar2;
  undefined8 uVar3;
  undefined **ppuVar4;
  undefined *puVar5;
  undefined *local_88;
  char *local_80;
  undefined8 local_78;
  char *local_70;
  char *local_68;
  char *local_60;
  char *local_58;
  char *local_50;
  char *local_48;
  char *local_40;
  char *local_38;
  char *local_30;
  undefined8 local_28;
  undefined8 local_20;

  ppuVar4 = &local_88;
  if (uParm1 != 0)
    {
      func_0x00401b70 (DAT_00616520,
		       "Try \'%s --help\' for more information.\n",
		       DAT_006165b8);
      goto LAB_00402226;
    }
  func_0x00401950 ("Usage: %s [OPTION]... [FILE]...\n", DAT_006165b8);
  uVar3 = DAT_00616500;
  func_0x00401b80
    ("Remove (unlink) the FILE(s).\n\n  -f, --force           ignore nonexistent files and arguments, never prompt\n  -i                    prompt before every removal\n",
     DAT_00616500);
  func_0x00401b80
    ("  -I                    prompt once before removing more than three files, or\n                          when removing recursively; less intrusive than -i,\n                          while still giving protection against most mistakes\n      --interactive[=WHEN]  prompt according to WHEN: never, once (-I), or\n                          always (-i); without WHEN, prompt always\n",
     uVar3);
  func_0x00401b80
    ("      --one-file-system  when removing a hierarchy recursively, skip any\n                          directory that is on a file system different from\n                          that of the corresponding command line argument\n",
     uVar3);
  func_0x00401b80
    ("      --no-preserve-root  do not treat \'/\' specially\n      --preserve-root   do not remove \'/\' (default)\n  -r, -R, --recursive   remove directories and their contents recursively\n  -d, --dir             remove empty directories\n  -v, --verbose         explain what is being done\n",
     uVar3);
  func_0x00401b80 ("      --help     display this help and exit\n", uVar3);
  func_0x00401b80 ("      --version  output version information and exit\n",
		   uVar3);
  func_0x00401b80
    ("\nBy default, rm does not remove directories.  Use the --recursive (-r or -R)\noption to remove each listed directory, too, along with all of its contents.\n",
     uVar3);
  func_0x00401950
    ("\nTo remove a file whose name starts with a \'-\', for example \'-foo\',\nuse one of these commands:\n  %s -- -foo\n\n  %s ./-foo\n",
     DAT_006165b8, DAT_006165b8);
  func_0x00401b80
    ("\nNote that if you use rm to remove a file, it might be possible to recover\nsome of its contents, given sufficient expertise and/or time.  For greater\nassurance that the contents are truly unrecoverable, consider using shred.\n",
     uVar3);
  local_88 = &DAT_004116c3;
  local_80 = "test invocation";
  local_78 = 0x411726;
  local_70 = "Multi-call invocation";
  local_68 = "sha224sum";
  local_60 = "sha2 utilities";
  local_58 = "sha256sum";
  local_50 = "sha2 utilities";
  local_48 = "sha384sum";
  local_40 = "sha2 utilities";
  local_38 = "sha512sum";
  local_30 = "sha2 utilities";
  local_28 = 0;
  local_20 = 0;
  puVar5 = &DAT_004116c3;
  do
    {
      iVar1 = func_0x00401ca0 (&DAT_004116c0, puVar5);
      if (iVar1 == 0)
	break;
      ppuVar4 = ppuVar4 + 2;
      puVar5 = *ppuVar4;
    }
  while (puVar5 != (undefined *) 0x0);
  puVar5 = ppuVar4[1];
  if (puVar5 == (undefined *) 0x0)
    {
      func_0x00401950 ("\n%s online help: <%s>\n", "GNU coreutils",
		       "https://www.gnu.org/software/coreutils/");
      lVar2 = func_0x00401d00 (5, 0);
      if (lVar2 == 0)
	goto LAB_0040222d;
      iVar1 = func_0x00401c00 (lVar2, &DAT_00411747, 3);
      if (iVar1 == 0)
	{
	  func_0x00401950 ("Full documentation at: <%s%s>\n",
			   "https://www.gnu.org/software/coreutils/",
			   &DAT_004116c0);
	  puVar5 = &DAT_004116c0;
	  uVar3 = 0x4116df;
	  goto LAB_00402214;
	}
      puVar5 = &DAT_004116c0;
    LAB_004021d2:
      ;
      func_0x00401950
	("Report %s translation bugs to <https://translationproject.org/team/>\n",
	 &DAT_004116c0);
    }
  else
    {
      func_0x00401950 ("\n%s online help: <%s>\n", "GNU coreutils",
		       "https://www.gnu.org/software/coreutils/");
      lVar2 = func_0x00401d00 (5, 0);
      if ((lVar2 != 0)
	  && (iVar1 = func_0x00401c00 (lVar2, &DAT_00411747, 3), iVar1 != 0))
	goto LAB_004021d2;
    }
  func_0x00401950 ("Full documentation at: <%s%s>\n",
		   "https://www.gnu.org/software/coreutils/", &DAT_004116c0);
  uVar3 = 0x413567;
  if (puVar5 == &DAT_004116c0)
    {
      uVar3 = 0x4116df;
    }
LAB_00402214:
  ;
  do
    {
      func_0x00401950
	("or available locally via: info \'(coreutils) %s%s\'\n", puVar5,
	 uVar3);
    LAB_00402226:
      ;
      func_0x00401d50 ((ulong) uParm1);
    LAB_0040222d:
      ;
      func_0x00401950 ("Full documentation at: <%s%s>\n",
		       "https://www.gnu.org/software/coreutils/",
		       &DAT_004116c0);
      puVar5 = &DAT_004116c0;
      uVar3 = 0x4116df;
    }
  while (true);
}

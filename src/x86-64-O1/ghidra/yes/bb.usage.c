
#include "yes.h"

long null_ARRAY_0060f52_0_8_;
long null_ARRAY_0060f52_8_8_;
long null_ARRAY_0060f70_0_8_;
long null_ARRAY_0060f70_16_8_;
long null_ARRAY_0060f70_24_8_;
long null_ARRAY_0060f70_32_8_;
long null_ARRAY_0060f70_40_8_;
long null_ARRAY_0060f70_48_8_;
long null_ARRAY_0060f70_8_8_;
long null_ARRAY_0060f74_0_4_;
long null_ARRAY_0060f74_16_8_;
long null_ARRAY_0060f74_4_4_;
long null_ARRAY_0060f74_8_4_;
long local_68_2_2_;
long local_6b_1_3_;
long local_6c_4_4_;
long local_6e_4_4_;
long local_6f_4_4_;
long local_70_0_2_;
long local_70_4_4_;
long local_71_0_1_;
long local_71_4_4_;
long local_72_4_4_;
long DAT_00000010;
long DAT_0040ca00;
long DAT_0040ca04;
long DAT_0040ca88;
long DAT_0040ca9c;
long DAT_0040cda0;
long DAT_0040cda4;
long DAT_0040cda8;
long DAT_0040cdab;
long DAT_0040cdad;
long DAT_0040cdb1;
long DAT_0040cdb5;
long DAT_0040d56b;
long DAT_0040d915;
long DAT_0040da19;
long DAT_0040da1f;
long DAT_0040da31;
long DAT_0040da32;
long DAT_0040da50;
long DAT_0040da54;
long DAT_0040dade;
long DAT_0060f108;
long DAT_0060f118;
long DAT_0060f128;
long DAT_0060f4c8;
long DAT_0060f530;
long DAT_0060f534;
long DAT_0060f538;
long DAT_0060f53c;
long DAT_0060f540;
long DAT_0060f550;
long DAT_0060f560;
long DAT_0060f568;
long DAT_0060f580;
long DAT_0060f588;
long DAT_0060f5d0;
long DAT_0060f5d8;
long DAT_0060f5e0;
long DAT_0060f778;
long DAT_0060f780;
long DAT_0060f788;
long DAT_0060f798;
long fde_0040e618;
long null_ARRAY_0040cc80;
long null_ARRAY_0040cd00;
long null_ARRAY_0040df00;
long null_ARRAY_0040e140;
long null_ARRAY_0060f520;
long null_ARRAY_0060f5a0;
long null_ARRAY_0060f600;
long null_ARRAY_0060f700;
long null_ARRAY_0060f740;
long PTR_DAT_0060f4c0;
long PTR_null_ARRAY_0060f518;
void
FUN_004019de (uint uParm1)
{
  int iVar1;
  long lVar2;
  undefined8 uVar3;
  undefined **ppuVar4;
  undefined *puVar5;
  undefined *local_88;
  char *local_80;
  undefined8 local_78;
  char *local_70;
  char *local_68;
  char *local_60;
  char *local_58;
  char *local_50;
  char *local_48;
  char *local_40;
  char *local_38;
  char *local_30;
  undefined8 local_28;
  undefined8 local_20;

  ppuVar4 = &local_88;
  if (uParm1 != 0)
    {
      func_0x004015d0 (DAT_0060f560,
		       "Try \'%s --help\' for more information.\n",
		       DAT_0060f5e0);
      goto LAB_00401b9f;
    }
  func_0x00401450 ("Usage: %s [STRING]...\n  or:  %s OPTION\n", DAT_0060f5e0,
		   DAT_0060f5e0);
  uVar3 = DAT_0060f540;
  func_0x004015e0
    ("Repeatedly output a line with all specified STRING(s), or \'y\'.\n\n",
     DAT_0060f540);
  func_0x004015e0 ("      --help     display this help and exit\n", uVar3);
  func_0x004015e0 ("      --version  output version information and exit\n",
		   uVar3);
  local_88 = &DAT_0040ca04;
  local_80 = "test invocation";
  local_78 = 0x40ca67;
  local_70 = "Multi-call invocation";
  local_68 = "sha224sum";
  local_60 = "sha2 utilities";
  local_58 = "sha256sum";
  local_50 = "sha2 utilities";
  local_48 = "sha384sum";
  local_40 = "sha2 utilities";
  local_38 = "sha512sum";
  local_30 = "sha2 utilities";
  local_28 = 0;
  local_20 = 0;
  puVar5 = &DAT_0040ca04;
  do
    {
      iVar1 = func_0x004016b0 (&DAT_0040ca00, puVar5);
      if (iVar1 == 0)
	break;
      ppuVar4 = ppuVar4 + 2;
      puVar5 = *ppuVar4;
    }
  while (puVar5 != (undefined *) 0x0);
  puVar5 = ppuVar4[1];
  if (puVar5 == (undefined *) 0x0)
    {
      func_0x00401450 ("\n%s online help: <%s>\n", "GNU coreutils",
		       "https://www.gnu.org/software/coreutils/");
      lVar2 = func_0x00401710 (5, 0);
      if (lVar2 == 0)
	goto LAB_00401ba6;
      iVar1 = func_0x00401630 (lVar2, &DAT_0040ca88, 3);
      if (iVar1 == 0)
	{
	  func_0x00401450 ("Full documentation at: <%s%s>\n",
			   "https://www.gnu.org/software/coreutils/",
			   &DAT_0040ca00);
	  puVar5 = &DAT_0040ca00;
	  uVar3 = 0x40ca20;
	  goto LAB_00401b8d;
	}
      puVar5 = &DAT_0040ca00;
    LAB_00401b4b:
      ;
      func_0x00401450
	("Report %s translation bugs to <https://translationproject.org/team/>\n",
	 &DAT_0040ca00);
    }
  else
    {
      func_0x00401450 ("\n%s online help: <%s>\n", "GNU coreutils",
		       "https://www.gnu.org/software/coreutils/");
      lVar2 = func_0x00401710 (5, 0);
      if ((lVar2 != 0)
	  && (iVar1 = func_0x00401630 (lVar2, &DAT_0040ca88, 3), iVar1 != 0))
	goto LAB_00401b4b;
    }
  func_0x00401450 ("Full documentation at: <%s%s>\n",
		   "https://www.gnu.org/software/coreutils/", &DAT_0040ca00);
  uVar3 = 0x40da4f;
  if (puVar5 == &DAT_0040ca00)
    {
      uVar3 = 0x40ca20;
    }
LAB_00401b8d:
  ;
  do
    {
      func_0x00401450
	("or available locally via: info \'(coreutils) %s%s\'\n", puVar5,
	 uVar3);
    LAB_00401b9f:
      ;
      func_0x00401750 ((ulong) uParm1);
    LAB_00401ba6:
      ;
      func_0x00401450 ("Full documentation at: <%s%s>\n",
		       "https://www.gnu.org/software/coreutils/",
		       &DAT_0040ca00);
      puVar5 = &DAT_0040ca00;
      uVar3 = 0x40ca20;
    }
  while (true);
}

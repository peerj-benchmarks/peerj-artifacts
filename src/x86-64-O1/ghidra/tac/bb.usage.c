
#include "tac.h"

long null_ARRAY_006218e_0_8_;
long null_ARRAY_006218e_8_8_;
long null_ARRAY_00623a2_16_8_;
long null_ARRAY_00623a2_8_8_;
long null_ARRAY_00623b4_0_8_;
long null_ARRAY_00623b4_32_8_;
long null_ARRAY_00623b4_40_8_;
long null_ARRAY_00623b4_8_8_;
long null_ARRAY_00623d0_0_8_;
long null_ARRAY_00623d0_16_8_;
long null_ARRAY_00623d0_24_8_;
long null_ARRAY_00623d0_32_8_;
long null_ARRAY_00623d0_40_8_;
long null_ARRAY_00623d0_48_8_;
long null_ARRAY_00623d0_8_8_;
long null_ARRAY_00623d4_0_4_;
long null_ARRAY_00623d4_16_8_;
long null_ARRAY_00623d4_4_4_;
long null_ARRAY_00623d4_8_4_;
long local_10_1_7_;
long local_11_0_1_;
long local_68_2_2_;
long local_6b_1_3_;
long local_6c_4_4_;
long local_6e_4_4_;
long local_6f_4_4_;
long local_70_0_2_;
long local_70_4_4_;
long local_71_0_1_;
long local_71_4_4_;
long local_72_4_4_;
long local_9_0_4_;
long local_a_0_1_;
long local_a_0_4_;
long DAT_00000010;
long DAT_0041bcf0;
long DAT_0041bcf4;
long DAT_0041bd78;
long DAT_0041bd7a;
long DAT_0041bd8b;
long DAT_0041bdac;
long DAT_0041bdf0;
long DAT_0041c350;
long DAT_0041c354;
long DAT_0041c358;
long DAT_0041c35b;
long DAT_0041c35d;
long DAT_0041c361;
long DAT_0041c365;
long DAT_0041caeb;
long DAT_0041ce95;
long DAT_0041cf99;
long DAT_0041cf9f;
long DAT_0041cfb1;
long DAT_0041cfb2;
long DAT_0041cfd0;
long DAT_0041dc02;
long DAT_0041dc09;
long DAT_0041dc47;
long DAT_0041dc60;
long DAT_0041dcd4;
long DAT_0041de05;
long DAT_0041e3a0;
long DAT_006213b8;
long DAT_006213c8;
long DAT_006213d8;
long DAT_00621888;
long DAT_006218f0;
long DAT_006218f4;
long DAT_006218f8;
long DAT_006218fc;
long DAT_00621940;
long DAT_00621950;
long DAT_00621960;
long DAT_00621968;
long DAT_00621980;
long DAT_00621988;
long DAT_00623a00;
long DAT_00623a08;
long DAT_00623a10;
long DAT_00623b80;
long DAT_00623b88;
long DAT_00623b90;
long DAT_00623b98;
long DAT_00623ba0;
long DAT_00623ba8;
long DAT_00623ba9;
long DAT_00623bb0;
long DAT_00623bb8;
long DAT_00623bc0;
long DAT_00623bc8;
long DAT_00623d78;
long DAT_00623d80;
long DAT_00623dd8;
long DAT_00623de0;
long DAT_00623df0;
long DAT_00623df8;
long fde_0041eda0;
long null_ARRAY_0041c200;
long null_ARRAY_0041c240;
long null_ARRAY_0041d940;
long null_ARRAY_0041d980;
long null_ARRAY_0041e220;
long null_ARRAY_0041e480;
long null_ARRAY_006218e0;
long null_ARRAY_006219a0;
long null_ARRAY_00621a00;
long null_ARRAY_00623a20;
long null_ARRAY_00623a40;
long null_ARRAY_00623b40;
long null_ARRAY_00623c00;
long null_ARRAY_00623d00;
long null_ARRAY_00623d40;
long PTR_DAT_00621880;
long PTR_null_ARRAY_006218d8;
long PTR_null_ARRAY_00621900;
void
FUN_00402a16 (uint uParm1)
{
  int iVar1;
  long lVar2;
  undefined8 uVar3;
  undefined **ppuVar4;
  undefined *puVar5;
  undefined *local_88;
  char *local_80;
  undefined8 local_78;
  char *local_70;
  char *local_68;
  char *local_60;
  char *local_58;
  char *local_50;
  char *local_48;
  char *local_40;
  char *local_38;
  char *local_30;
  undefined8 local_28;
  undefined8 local_20;

  ppuVar4 = &local_88;
  if (uParm1 != 0)
    {
      func_0x00401f30 (DAT_00621960,
		       "Try \'%s --help\' for more information.\n",
		       DAT_00623bc8);
      goto LAB_00402bfb;
    }
  func_0x00401ce0 ("Usage: %s [OPTION]... [FILE]...\n", DAT_00623bc8);
  uVar3 = DAT_00621940;
  func_0x00401f40 ("Write each FILE to standard output, last line first.\n",
		   DAT_00621940);
  func_0x00401f40
    ("\nWith no FILE, or when FILE is -, read standard input.\n", uVar3);
  func_0x00401f40
    ("\nMandatory arguments to long options are mandatory for short options too.\n",
     uVar3);
  func_0x00401f40
    ("  -b, --before             attach the separator before instead of after\n  -r, --regex              interpret the separator as a regular expression\n  -s, --separator=STRING   use STRING as the separator instead of newline\n",
     uVar3);
  func_0x00401f40 ("      --help     display this help and exit\n", uVar3);
  func_0x00401f40 ("      --version  output version information and exit\n",
		   uVar3);
  local_88 = &DAT_0041bcf4;
  local_80 = "test invocation";
  local_78 = 0x41bd57;
  local_70 = "Multi-call invocation";
  local_68 = "sha224sum";
  local_60 = "sha2 utilities";
  local_58 = "sha256sum";
  local_50 = "sha2 utilities";
  local_48 = "sha384sum";
  local_40 = "sha2 utilities";
  local_38 = "sha512sum";
  local_30 = "sha2 utilities";
  local_28 = 0;
  local_20 = 0;
  puVar5 = &DAT_0041bcf4;
  do
    {
      iVar1 = func_0x004020d0 (&DAT_0041bcf0, puVar5);
      if (iVar1 == 0)
	break;
      ppuVar4 = ppuVar4 + 2;
      puVar5 = *ppuVar4;
    }
  while (puVar5 != (undefined *) 0x0);
  puVar5 = ppuVar4[1];
  if (puVar5 == (undefined *) 0x0)
    {
      func_0x00401ce0 ("\n%s online help: <%s>\n", "GNU coreutils",
		       "https://www.gnu.org/software/coreutils/");
      lVar2 = func_0x00402130 (5, 0);
      if (lVar2 == 0)
	goto LAB_00402c02;
      iVar1 = func_0x00401fc0 (lVar2, &DAT_0041bd78, 3);
      if (iVar1 == 0)
	{
	  func_0x00401ce0 ("Full documentation at: <%s%s>\n",
			   "https://www.gnu.org/software/coreutils/",
			   &DAT_0041bcf0);
	  puVar5 = &DAT_0041bcf0;
	  uVar3 = 0x41bd10;
	  goto LAB_00402be9;
	}
      puVar5 = &DAT_0041bcf0;
    LAB_00402ba7:
      ;
      func_0x00401ce0
	("Report %s translation bugs to <https://translationproject.org/team/>\n",
	 &DAT_0041bcf0);
    }
  else
    {
      func_0x00401ce0 ("\n%s online help: <%s>\n", "GNU coreutils",
		       "https://www.gnu.org/software/coreutils/");
      lVar2 = func_0x00402130 (5, 0);
      if ((lVar2 != 0)
	  && (iVar1 = func_0x00401fc0 (lVar2, &DAT_0041bd78, 3), iVar1 != 0))
	goto LAB_00402ba7;
    }
  func_0x00401ce0 ("Full documentation at: <%s%s>\n",
		   "https://www.gnu.org/software/coreutils/", &DAT_0041bcf0);
  uVar3 = 0x41cfcf;
  if (puVar5 == &DAT_0041bcf0)
    {
      uVar3 = 0x41bd10;
    }
LAB_00402be9:
  ;
  do
    {
      func_0x00401ce0
	("or available locally via: info \'(coreutils) %s%s\'\n", puVar5,
	 uVar3);
    LAB_00402bfb:
      ;
      func_0x00402190 ((ulong) uParm1);
    LAB_00402c02:
      ;
      func_0x00401ce0 ("Full documentation at: <%s%s>\n",
		       "https://www.gnu.org/software/coreutils/",
		       &DAT_0041bcf0);
      puVar5 = &DAT_0041bcf0;
      uVar3 = 0x41bd10;
    }
  while (true);
}

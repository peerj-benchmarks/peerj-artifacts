typedef unsigned char   undefined;

typedef unsigned char    byte;
typedef unsigned char    dwfenc;
typedef unsigned int    dword;
typedef unsigned long    qword;
typedef unsigned char    undefined1;
typedef unsigned short    undefined2;
typedef unsigned int    undefined4;
typedef unsigned long    undefined6;
typedef unsigned long    undefined8;
typedef unsigned short    word;
#define unkbyte9   unsigned long long
#define unkbyte10   unsigned long long
#define unkbyte11   unsigned long long
#define unkbyte12   unsigned long long
#define unkbyte13   unsigned long long
#define unkbyte14   unsigned long long
#define unkbyte15   unsigned long long
#define unkbyte16   unsigned long long

#define unkuint9   unsigned long long
#define unkuint10   unsigned long long
#define unkuint11   unsigned long long
#define unkuint12   unsigned long long
#define unkuint13   unsigned long long
#define unkuint14   unsigned long long
#define unkuint15   unsigned long long
#define unkuint16   unsigned long long

#define unkint9   long long
#define unkint10   long long
#define unkint11   long long
#define unkint12   long long
#define unkint13   long long
#define unkint14   long long
#define unkint15   long long
#define unkint16   long long

#define unkfloat1   float
#define unkfloat2   float
#define unkfloat3   float
#define unkfloat5   double
#define unkfloat6   double
#define unkfloat7   double
#define unkfloat9   long double
#define unkfloat11   long double
#define unkfloat12   long double
#define unkfloat13   long double
#define unkfloat14   long double
#define unkfloat15   long double
#define unkfloat16   long double

#define BADSPACEBASE   void
#define code   void

#define ulong long
#define true 1
#define bool int
#define false 0
#define float10 int
#define ushort int
#define undefined7 long
#define undefined2 long
#define uint5 long
#define uint6 long

// Add definition for uint
typedef unsigned int    uint;

#define sbyte char

extern imperfection_wrapper(void);
typedef struct eh_frame_hdr eh_frame_hdr, *Peh_frame_hdr;

struct eh_frame_hdr {
    byte eh_frame_hdr_version; // Exception Handler Frame Header Version
    dwfenc eh_frame_pointer_encoding; // Exception Handler Frame Pointer Encoding
    dwfenc eh_frame_desc_entry_count_encoding; // Encoding of # of Exception Handler FDEs
    dwfenc eh_frame_table_encoding; // Exception Handler Table Encoding
};

typedef struct fde_table_entry fde_table_entry, *Pfde_table_entry;

struct fde_table_entry {
    dword initial_loc; // Initial Location
    dword data_loc; // Data location
};

typedef enum Elf64_DynTag {
    DT_AUDIT=1879047932,
    DT_AUXILIARY=2147483645,
    DT_BIND_NOW=24,
    DT_CHECKSUM=1879047672,
    DT_CONFIG=1879047930,
    DT_DEBUG=21,
    DT_DEPAUDIT=1879047931,
    DT_ENCODING=32,
    DT_FEATURE_1=1879047676,
    DT_FILTER=2147483647,
    DT_FINI=13,
    DT_FINI_ARRAY=26,
    DT_FINI_ARRAYSZ=28,
    DT_FLAGS=30,
    DT_FLAGS_1=1879048187,
    DT_GNU_CONFLICT=1879047928,
    DT_GNU_CONFLICTSZ=1879047670,
    DT_GNU_HASH=1879047925,
    DT_GNU_LIBLIST=1879047929,
    DT_GNU_LIBLISTSZ=1879047671,
    DT_GNU_PRELINKED=1879047669,
    DT_HASH=4,
    DT_INIT=12,
    DT_INIT_ARRAY=25,
    DT_INIT_ARRAYSZ=27,
    DT_JMPREL=23,
    DT_MOVEENT=1879047674,
    DT_MOVESZ=1879047675,
    DT_MOVETAB=1879047934,
    DT_NEEDED=1,
    DT_NULL=0,
    DT_PLTGOT=3,
    DT_PLTPAD=1879047933,
    DT_PLTPADSZ=1879047673,
    DT_PLTREL=20,
    DT_PLTRELSZ=2,
    DT_POSFLAG_1=1879047677,
    DT_PREINIT_ARRAYSZ=33,
    DT_REL=17,
    DT_RELA=7,
    DT_RELACOUNT=1879048185,
    DT_RELAENT=9,
    DT_RELASZ=8,
    DT_RELCOUNT=1879048186,
    DT_RELENT=19,
    DT_RELSZ=18,
    DT_RPATH=15,
    DT_RUNPATH=29,
    DT_SONAME=14,
    DT_STRSZ=10,
    DT_STRTAB=5,
    DT_SYMBOLIC=16,
    DT_SYMENT=11,
    DT_SYMINENT=1879047679,
    DT_SYMINFO=1879047935,
    DT_SYMINSZ=1879047678,
    DT_SYMTAB=6,
    DT_TEXTREL=22,
    DT_TLSDESC_GOT=1879047927,
    DT_TLSDESC_PLT=1879047926,
    DT_VERDEF=1879048188,
    DT_VERDEFNUM=1879048189,
    DT_VERNEED=1879048190,
    DT_VERNEEDNUM=1879048191,
    DT_VERSYM=1879048176
} Elf64_DynTag;

typedef enum Elf_ProgramHeaderType {
    PT_DYNAMIC=2,
    PT_GNU_EH_FRAME=1685382480,
    PT_GNU_RELRO=1685382482,
    PT_GNU_STACK=1685382481,
    PT_INTERP=3,
    PT_LOAD=1,
    PT_NOTE=4,
    PT_NULL=0,
    PT_PHDR=6,
    PT_SHLIB=5,
    PT_TLS=7
} Elf_ProgramHeaderType;

typedef struct Elf64_Dyn Elf64_Dyn, *PElf64_Dyn;

struct Elf64_Dyn {
    enum Elf64_DynTag d_tag;
    qword d_val;
};

typedef struct Elf64_Shdr Elf64_Shdr, *PElf64_Shdr;

typedef enum Elf_SectionHeaderType {
    SHT_CHECKSUM=1879048184,
    SHT_DYNAMIC=6,
    SHT_DYNSYM=11,
    SHT_FINI_ARRAY=15,
    SHT_GNU_ATTRIBUTES=1879048181,
    SHT_GNU_HASH=1879048182,
    SHT_GNU_LIBLIST=1879048183,
    SHT_GNU_verdef=1879048189,
    SHT_GNU_verneed=1879048190,
    SHT_GNU_versym=1879048191,
    SHT_GROUP=17,
    SHT_HASH=5,
    SHT_INIT_ARRAY=14,
    SHT_NOBITS=8,
    SHT_NOTE=7,
    SHT_NULL=0,
    SHT_PREINIT_ARRAY=16,
    SHT_PROGBITS=1,
    SHT_REL=9,
    SHT_RELA=4,
    SHT_SHLIB=10,
    SHT_STRTAB=3,
    SHT_SUNW_COMDAT=1879048187,
    SHT_SUNW_move=1879048186,
    SHT_SUNW_syminfo=1879048188,
    SHT_SYMTAB=2,
    SHT_SYMTAB_SHNDX=18
} Elf_SectionHeaderType;

struct Elf64_Shdr {
    dword sh_name;
    enum Elf_SectionHeaderType sh_type;
    qword sh_flags;
    qword sh_addr;
    qword sh_offset;
    qword sh_size;
    dword sh_link;
    dword sh_info;
    qword sh_addralign;
    qword sh_entsize;
};

typedef struct Elf64_Rela Elf64_Rela, *PElf64_Rela;

struct Elf64_Rela {
    qword r_offset; // location to apply the relocation action
    qword r_info; // the symbol table index and the type of relocation
    qword r_addend; // a constant addend used to compute the relocatable field value
};

typedef struct Elf64_Phdr Elf64_Phdr, *PElf64_Phdr;

struct Elf64_Phdr {
    enum Elf_ProgramHeaderType p_type;
    dword p_flags;
    qword p_offset;
    qword p_vaddr;
    qword p_paddr;
    qword p_filesz;
    qword p_memsz;
    qword p_align;
};

typedef struct Elf64_Sym Elf64_Sym, *PElf64_Sym;

struct Elf64_Sym {
    dword st_name;
    byte st_info;
    byte st_other;
    word st_shndx;
    qword st_value;
    qword st_size;
};

typedef struct Elf64_Ehdr Elf64_Ehdr, *PElf64_Ehdr;

struct Elf64_Ehdr {
    byte e_ident_magic_num;
    char e_ident_magic_str[3];
    byte e_ident_class;
    byte e_ident_data;
    byte e_ident_version;
    byte e_ident_pad[9];
    word e_type;
    word e_machine;
    dword e_version;
    qword e_entry;
    qword e_phoff;
    qword e_shoff;
    dword e_flags;
    word e_ehsize;
    word e_phentsize;
    word e_phnum;
    word e_shentsize;
    word e_shnum;
    word e_shstrndx;
};




undefined8 _DT_INIT(void);
void FUN_00401d10(void);
void FUN_00401ec0(void);
void FUN_00402080(void);
void FUN_00402350(void);
void entry(void);
void FUN_004023b0(void);
void FUN_00402430(void);
void FUN_004024b0(void);
ulong FUN_004024ee(char *pcParm1,undefined8 uParm2);
void FUN_00402c85(uint uParm1);
ulong FUN_00402f4c(uint uParm1,undefined8 *puParm2);
void FUN_004035e1(undefined8 uParm1,uint *puParm2);
long FUN_00403602(long lParm1,long lParm2);
void FUN_00403654(undefined8 uParm1,undefined8 *puParm2);
ulong FUN_0040366e(undefined4 uParm1,undefined8 uParm2,ulong uParm3,undefined8 uParm4,undefined4 uParm5,byte bParm6);
ulong FUN_00403786(undefined8 uParm1,ulong uParm2,undefined8 uParm3,byte bParm4);
undefined8 FUN_00403895(undefined8 uParm1,long *plParm2,ulong *puParm3);
ulong FUN_004038fa(char *pcParm1,char *pcParm2,undefined8 uParm3,undefined8 uParm4);
void FUN_00403af0(long lParm1,long lParm2,undefined uParm3);
void FUN_00403bd6(char *pcParm1);
long FUN_00403c1f(long lParm1,int iParm2,char cParm3);
void FUN_004040fb(undefined8 uParm1,undefined8 uParm2);
ulong FUN_00404118(undefined8 uParm1,char *pcParm2);
void FUN_00404154(undefined8 uParm1,char *pcParm2);
char * FUN_00404185(char *pcParm1,uint uParm2);
void FUN_0040471f(undefined8 uParm1);
void FUN_00404732(void);
void FUN_0040480a(void);
long FUN_004048b0(void);
void FUN_0040493f(void);
ulong FUN_00404957(char *pcParm1);
undefined * FUN_004049ae(undefined8 uParm1);
char * FUN_00404a12(char *pcParm1);
ulong FUN_00404a58(long lParm1);
undefined FUN_00404a92(char *pcParm1);;
void FUN_00404ac5(long lParm1,undefined8 uParm2,undefined8 *puParm3);
ulong FUN_00404b33(long lParm1,undefined8 uParm2,undefined8 *puParm3);
void FUN_00404b6a(void);
long FUN_00404b82(long lParm1,char *pcParm2,undefined8 *puParm3);
ulong FUN_00404c5b(ulong uParm1,ulong uParm2);
undefined FUN_00404c6e(long lParm1,long lParm2);;
ulong FUN_00404c75(long lParm1,undefined8 uParm2,uint uParm3);
ulong FUN_00404cf9(ulong uParm1,long lParm2);
long FUN_00404e15(long *plParm1,undefined8 uParm2);
undefined8 FUN_00404e37(long lParm1,long **pplParm2,char cParm3);
long FUN_00404f5b(long lParm1,long lParm2,long **pplParm3,char cParm4);
long FUN_0040505f(long lParm1,long lParm2);
long * FUN_004050bb(undefined8 uParm1,undefined1 *puParm2,code *pcParm3,code *pcParm4,long lParm5);
void FUN_004051a6(long **pplParm1);
ulong FUN_0040524b(long *plParm1,undefined8 uParm2);
undefined8 FUN_00405388(long lParm1,long lParm2,long *plParm3);
undefined8 FUN_004055b5(undefined8 uParm1,undefined8 uParm2);
ulong FUN_004055e4(undefined8 *puParm1,ulong uParm2);
ulong FUN_0040560e(undefined8 *puParm1,undefined8 *puParm2);
ulong FUN_00405645(undefined8 *puParm1,undefined8 *puParm2);
void FUN_00405681(undefined8 *puParm1);
void FUN_00405697(long lParm1);
int * FUN_0040572c(int *piParm1,int iParm2);
undefined * FUN_0040575c(char *pcParm1,int iParm2);
ulong FUN_00405814(undefined *param_1,ulong param_2,long param_3,ulong param_4,uint param_5,ulong param_6,long param_7,char *param_8,char *param_9);
undefined1 * FUN_004065cb(int iParm1,undefined8 uParm2,undefined8 uParm3,uint *puParm4);
ulong FUN_0040676d(undefined1 *puParm1,byte bParm2,undefined8 uParm3);
void FUN_004067a1(uint uParm1,undefined8 uParm2,undefined8 uParm3);
void FUN_004067cf(ulong uParm1,undefined8 uParm2);
void FUN_004067e7(undefined8 uParm1,undefined8 uParm2,char cParm3);
void FUN_00406870(undefined8 uParm1,char cParm2);
void FUN_00406889(undefined8 uParm1);
void FUN_0040689c(uint uParm1,undefined8 uParm2,undefined8 uParm3);
void FUN_0040692b(void);
void FUN_0040693e(undefined8 uParm1,undefined8 uParm2);
void FUN_00406953(undefined8 uParm1);
ulong FUN_00406969(uint uParm1,long lParm2,uint uParm3,long lParm4,uint uParm5);
ulong FUN_00406d0a(undefined8 uParm1,undefined8 uParm2);
ulong FUN_00406e31(long lParm1,int iParm2,undefined8 uParm3,code *pcParm4,ulong uParm5);
void FUN_00406f80(undefined8 uParm1,long lParm2,undefined8 uParm3,undefined8 uParm4,undefined8 *puParm5,undefined8 uParm6);
void FUN_004071bd(void);
void FUN_0040720f(void);
void FUN_00407294(long lParm1);
long FUN_004072ae(long lParm1,long lParm2);
void FUN_004072e1(undefined8 uParm1,undefined8 uParm2);
void FUN_0040730a(undefined8 uParm1);
long FUN_00407321(void);
long FUN_00407349(void);
ulong FUN_00407375(void);
undefined8 FUN_004073a3(ulong uParm1,ulong uParm2,ulong uParm3);
void FUN_004073eb(void);
void FUN_00407418(undefined8 uParm1);
void FUN_00407458(uint uParm1,uint uParm2,undefined8 uParm3,undefined8 uParm4);
void FUN_004074af(undefined4 uParm1,undefined4 uParm2,undefined4 uParm3,undefined4 uParm4,undefined4 uParm5,undefined4 uParm6,undefined4 uParm7,undefined4 uParm8,ulong param_1,ulong param_2,undefined8 param_3,undefined8 param_4,undefined8 param_5,undefined8 param_6);
ulong FUN_00407582(uint param_1,undefined8 param_2,undefined8 param_3,undefined8 param_4);
undefined8 FUN_004076f1(ulong uParm1);
ulong FUN_00407740(long lParm1);
undefined8 FUN_004077d0(void);
void FUN_004077e3(void);
undefined8 FUN_004077f1(ulong uParm1,long lParm2,long lParm3,ulong uParm4);
long FUN_004078bd(long lParm1,ulong uParm2);
void FUN_00407d4d(long lParm1,int *piParm2);
ulong FUN_00407e0c(int param_1,undefined8 *param_2,char *param_3,long *param_4,int *param_5,int param_6,int *param_7,int param_8,undefined8 param_9);
ulong FUN_004082c7(uint param_1,undefined8 *param_2,char *param_3,long param_4,undefined8 param_5,ulong param_6,uint *param_7,int param_8);
void FUN_00408743(void);
void FUN_00408799(void);
ulong FUN_004087af(ulong uParm1,char *pcParm2,uint uParm3,long lParm4,uint uParm5);
ulong FUN_00408a2f(long lParm1,long lParm2);
void FUN_00408a98(long lParm1);
ulong FUN_00408ab2(uint *puParm1,byte *pbParm2,long lParm3);
ulong FUN_00408b17(uint param_1,undefined8 param_2,uint param_3,uint param_4);
void FUN_00408c07(long lParm1,long lParm2);
ulong FUN_00408c35(long lParm1,long lParm2);
void FUN_0040900f(void);
undefined8 FUN_00409023(long lParm1);
ulong FUN_004090b3(long lParm1,long lParm2);
undefined8 FUN_004090ff(undefined8 uParm1,ulong uParm2,long lParm3);
undefined8 FUN_00409167(ulong uParm1,long lParm2,ulong uParm3);
ulong FUN_0040928e(undefined8 uParm1,undefined8 uParm2,undefined8 uParm3);
long FUN_0040935b(undefined8 uParm1,ulong uParm2);
void FUN_0040943a(undefined8 uParm1,undefined8 uParm2);
void FUN_0040945e(void);
long FUN_00409471(undefined8 uParm1,long *plParm2,long lParm3,long lParm4);
void FUN_00409561(undefined8 uParm1,undefined8 uParm2,long lParm3);
void FUN_004095be(long *plParm1,long lParm2,long lParm3);
long FUN_00409689(undefined8 uParm1,undefined8 uParm2,undefined8 uParm3,undefined8 uParm4,undefined8 uParm5,code *pcParm6);
undefined *FUN_004096f4(uint uParm1,undefined8 uParm2,undefined *puParm3,ulong uParm4,undefined1 *puParm5,code *pcParm6);
ulong FUN_0040989e(ulong uParm1,char cParm2);
ulong FUN_004098fe(undefined8 uParm1);
undefined8 FUN_0040995f(void);
ulong FUN_00409967(ulong uParm1);
ulong FUN_004099f5(char *pcParm1,ulong uParm2);
char * FUN_00409a2a(void);
void FUN_00409d65(undefined8 uParm1);
undefined8 FUN_00409d88(void);
ulong FUN_00409daa(undefined8 *puParm1,ulong uParm2);
void FUN_00409e9b(undefined8 uParm1);
ulong FUN_00409eb3(undefined8 *puParm1);
long * FUN_00409ee3(ulong uParm1,ulong uParm2);
long * FUN_00409f2e(long lParm1,ulong uParm2);
void FUN_0040a1c9(long *plParm1,ulong uParm2,ulong uParm3);
undefined8 FUN_0040a315(long *plParm1);
void FUN_0040a34c(long *plParm1,long *plParm2);
void FUN_0040a5af(ulong *puParm1);
void FUN_0040a7e0(undefined8 uParm1,undefined8 uParm2);
undefined8 FUN_0040a7f6(undefined8 uParm1);
void FUN_0040a879(void);
undefined8 FUN_0040a925(long lParm1,undefined8 uParm2,undefined8 uParm3);
undefined8 FUN_0040a987(uint uParm1,long lParm2,undefined8 uParm3,undefined8 uParm4);
ulong FUN_0040a9f4(char *pcParm1,long lParm2);
long FUN_0040aa3b(ulong uParm1,uint *puParm2,ulong uParm3,uint *puParm4,long *plParm5);
ulong FUN_0040ab5f(void);
ulong FUN_0040ab91(void);
int * FUN_0040ad9e(ulong uParm1,long lParm2,ulong uParm3,uint *puParm4,ulong *puParm5);
char * FUN_0040b381(int iParm1,long lParm2,long lParm3,long lParm4,uint uParm5);
void FUN_0040b999(uint param_1,undefined8 param_2);
ulong FUN_0040bb5f(void);
void FUN_0040bd72(undefined8 uParm1,uint uParm2);
undefined8 *FUN_0040bf13(undefined8 *puParm1,undefined8 *puParm2,undefined8 *puParm3,undefined8 uParm4);
long FUN_00411ab0(undefined8 uParm1,undefined8 uParm2);
double FUN_00411b40(int *piParm1);
void FUN_00411b88(int *param_1);
void FUN_00411c09(undefined8 uParm1);
undefined8 FUN_00411c26(uint *puParm1,ulong *puParm2);
undefined8 FUN_0041204b(byte *pbParm1,ulong *puParm2,byte **ppbParm3);
ulong FUN_00412bb0(void);
ulong FUN_00412bd8(void);
void FUN_00412c10(void);
undefined8 _DT_FINI(void);
undefined FUN_00619000();
undefined FUN_00619008();
undefined FUN_00619010();
undefined FUN_00619018();
undefined FUN_00619020();
undefined FUN_00619028();
undefined FUN_00619030();
undefined FUN_00619038();
undefined FUN_00619040();
undefined FUN_00619048();
undefined FUN_00619050();
undefined FUN_00619058();
undefined FUN_00619060();
undefined FUN_00619068();
undefined FUN_00619070();
undefined FUN_00619078();
undefined FUN_00619080();
undefined FUN_00619088();
undefined FUN_00619090();
undefined FUN_00619098();
undefined FUN_006190a0();
undefined FUN_006190a8();
undefined FUN_006190b0();
undefined FUN_006190b8();
undefined FUN_006190c0();
undefined FUN_006190c8();
undefined FUN_006190d0();
undefined FUN_006190d8();
undefined FUN_006190e0();
undefined FUN_006190e8();
undefined FUN_006190f0();
undefined FUN_006190f8();
undefined FUN_00619100();
undefined FUN_00619108();
undefined FUN_00619110();
undefined FUN_00619118();
undefined FUN_00619120();
undefined FUN_00619128();
undefined FUN_00619130();
undefined FUN_00619138();
undefined FUN_00619140();
undefined FUN_00619148();
undefined FUN_00619150();
undefined FUN_00619158();
undefined FUN_00619160();
undefined FUN_00619168();
undefined FUN_00619170();
undefined FUN_00619178();
undefined FUN_00619180();
undefined FUN_00619188();
undefined FUN_00619190();
undefined FUN_00619198();
undefined FUN_006191a0();
undefined FUN_006191a8();
undefined FUN_006191b0();
undefined FUN_006191b8();
undefined FUN_006191c0();
undefined FUN_006191c8();
undefined FUN_006191d0();
undefined FUN_006191d8();
undefined FUN_006191e0();
undefined FUN_006191e8();
undefined FUN_006191f0();
undefined FUN_006191f8();
undefined FUN_00619200();
undefined FUN_00619208();
undefined FUN_00619210();
undefined FUN_00619218();
undefined FUN_00619220();
undefined FUN_00619228();
undefined FUN_00619230();
undefined FUN_00619238();
undefined FUN_00619240();
undefined FUN_00619248();
undefined FUN_00619250();
undefined FUN_00619258();
undefined FUN_00619260();
undefined FUN_00619268();
undefined FUN_00619270();
undefined FUN_00619278();
undefined FUN_00619280();
undefined FUN_00619288();
undefined FUN_00619290();
undefined FUN_00619298();
undefined FUN_006192a0();
undefined FUN_006192a8();
undefined FUN_006192b0();
undefined FUN_006192b8();
undefined FUN_006192c0();
undefined FUN_006192c8();
undefined FUN_006192d0();
undefined FUN_006192d8();
undefined FUN_006192e0();
undefined FUN_006192e8();
undefined FUN_006192f0();
undefined FUN_006192f8();
undefined FUN_00619300();
undefined FUN_00619308();


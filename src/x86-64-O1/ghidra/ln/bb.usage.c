
#include "ln.h"

long null_ARRAY_0061854_0_8_;
long null_ARRAY_0061854_8_8_;
long null_ARRAY_0061878_0_8_;
long null_ARRAY_0061878_16_8_;
long null_ARRAY_0061878_24_8_;
long null_ARRAY_0061878_32_8_;
long null_ARRAY_0061878_40_8_;
long null_ARRAY_0061878_48_8_;
long null_ARRAY_0061878_8_8_;
long null_ARRAY_006187c_0_4_;
long null_ARRAY_006187c_16_8_;
long null_ARRAY_006187c_4_4_;
long null_ARRAY_006187c_8_4_;
long local_68_2_2_;
long local_6b_1_3_;
long local_6c_4_4_;
long local_6e_4_4_;
long local_6f_4_4_;
long local_70_0_2_;
long local_70_4_4_;
long local_71_0_1_;
long local_71_4_4_;
long local_72_4_4_;
long DAT_00000010;
long DAT_00413acd;
long DAT_00413ad0;
long DAT_00413ad3;
long DAT_00413b57;
long DAT_00413e89;
long DAT_00413e8a;
long DAT_00413e8b;
long DAT_00413ec0;
long DAT_00414058;
long DAT_0041405e;
long DAT_00414060;
long DAT_00414064;
long DAT_00414068;
long DAT_0041406b;
long DAT_0041406d;
long DAT_00414071;
long DAT_00414075;
long DAT_004148b2;
long DAT_00414c55;
long DAT_00414d59;
long DAT_00414d5f;
long DAT_00414d71;
long DAT_00414d72;
long DAT_00414d90;
long DAT_00414de9;
long DAT_00414deb;
long DAT_00414dee;
long DAT_00414e8f;
long DAT_00414ea5;
long DAT_00618000;
long DAT_00618010;
long DAT_00618020;
long DAT_006184e0;
long DAT_006184e1;
long DAT_006184f0;
long DAT_00618550;
long DAT_00618554;
long DAT_00618558;
long DAT_0061855c;
long DAT_00618580;
long DAT_00618588;
long DAT_00618590;
long DAT_006185a0;
long DAT_006185a8;
long DAT_006185c0;
long DAT_006185c8;
long DAT_00618610;
long DAT_00618618;
long DAT_00618619;
long DAT_0061861a;
long DAT_0061861b;
long DAT_0061861c;
long DAT_0061861d;
long DAT_00618620;
long DAT_00618628;
long DAT_00618630;
long DAT_00618638;
long DAT_00618640;
long DAT_00618648;
long DAT_006187b8;
long DAT_006187f8;
long DAT_006187fc;
long DAT_00618800;
long DAT_00618858;
long DAT_00618860;
long DAT_00618870;
long fde_00415cf8;
long null_ARRAY_00413c80;
long null_ARRAY_00413e80;
long null_ARRAY_00413f40;
long null_ARRAY_00413f80;
long null_ARRAY_00413ff0;
long null_ARRAY_004152c0;
long null_ARRAY_00415420;
long null_ARRAY_00415510;
long null_ARRAY_00618540;
long null_ARRAY_006185e0;
long null_ARRAY_00618680;
long null_ARRAY_00618780;
long null_ARRAY_006187c0;
long PTR_DAT_006184e8;
long PTR_FUN_00618560;
long PTR_null_ARRAY_00618538;
long PTR_null_ARRAY_00618568;
void
FUN_00402c85 (uint uParm1)
{
  int iVar1;
  long lVar2;
  undefined8 uVar3;
  undefined **ppuVar4;
  undefined *puVar5;
  undefined *local_88;
  char *local_80;
  undefined8 local_78;
  char *local_70;
  char *local_68;
  char *local_60;
  char *local_58;
  char *local_50;
  char *local_48;
  char *local_40;
  char *local_38;
  char *local_30;
  undefined8 local_28;
  undefined8 local_20;

  ppuVar4 = &local_88;
  if (uParm1 != 0)
    {
      func_0x00401fc0 (DAT_006185a0,
		       "Try \'%s --help\' for more information.\n",
		       DAT_00618648);
      goto LAB_00402eae;
    }
  func_0x00401d50
    ("Usage: %s [OPTION]... [-T] TARGET LINK_NAME   (1st form)\n  or:  %s [OPTION]... TARGET                  (2nd form)\n  or:  %s [OPTION]... TARGET... DIRECTORY     (3rd form)\n  or:  %s [OPTION]... -t DIRECTORY TARGET...  (4th form)\n",
     DAT_00618648, DAT_00618648, DAT_00618648, DAT_00618648);
  uVar3 = DAT_00618580;
  func_0x00401fd0
    ("In the 1st form, create a link to TARGET with the name LINK_NAME.\nIn the 2nd form, create a link to TARGET in the current directory.\nIn the 3rd and 4th forms, create links to each TARGET in DIRECTORY.\nCreate hard links by default, symbolic links with --symbolic.\nBy default, each destination (name of new link) should not already exist.\nWhen creating hard links, each TARGET must exist.  Symbolic links\ncan hold arbitrary text; if later resolved, a relative link is\ninterpreted in relation to its parent directory.\n",
     DAT_00618580);
  func_0x00401fd0
    ("\nMandatory arguments to long options are mandatory for short options too.\n",
     uVar3);
  func_0x00401fd0
    ("      --backup[=CONTROL]      make a backup of each existing destination file\n  -b                          like --backup but does not accept an argument\n  -d, -F, --directory         allow the superuser to attempt to hard link\n                                directories (note: will probably fail due to\n                                system restrictions, even for the superuser)\n  -f, --force                 remove existing destination files\n",
     uVar3);
  func_0x00401fd0
    ("  -i, --interactive           prompt whether to remove destinations\n  -L, --logical               dereference TARGETs that are symbolic links\n  -n, --no-dereference        treat LINK_NAME as a normal file if\n                                it is a symbolic link to a directory\n  -P, --physical              make hard links directly to symbolic links\n  -r, --relative              create symbolic links relative to link location\n  -s, --symbolic              make symbolic links instead of hard links\n",
     uVar3);
  func_0x00401fd0
    ("  -S, --suffix=SUFFIX         override the usual backup suffix\n  -t, --target-directory=DIRECTORY  specify the DIRECTORY in which to create\n                                the links\n  -T, --no-target-directory   treat LINK_NAME as a normal file always\n  -v, --verbose               print name of each linked file\n",
     uVar3);
  func_0x00401fd0 ("      --help     display this help and exit\n", uVar3);
  func_0x00401fd0 ("      --version  output version information and exit\n",
		   uVar3);
  func_0x00401fd0
    ("\nThe backup suffix is \'~\', unless set with --suffix or SIMPLE_BACKUP_SUFFIX.\nThe version control method may be selected via the --backup option or through\nthe VERSION_CONTROL environment variable.  Here are the values:\n\n",
     uVar3);
  func_0x00401fd0
    ("  none, off       never make backups (even if --backup is given)\n  numbered, t     make numbered backups\n  existing, nil   numbered if numbered backups exist, simple otherwise\n  simple, never   always make simple backups\n",
     uVar3);
  func_0x00401d50
    ("\nUsing -s ignores -L and -P.  Otherwise, the last option specified controls\nbehavior when a TARGET is a symbolic link, defaulting to %s.\n",
     &DAT_00413ad0);
  local_88 = &DAT_00413ad3;
  local_80 = "test invocation";
  local_78 = 0x413b36;
  local_70 = "Multi-call invocation";
  local_68 = "sha224sum";
  local_60 = "sha2 utilities";
  local_58 = "sha256sum";
  local_50 = "sha2 utilities";
  local_48 = "sha384sum";
  local_40 = "sha2 utilities";
  local_38 = "sha512sum";
  local_30 = "sha2 utilities";
  local_28 = 0;
  local_20 = 0;
  puVar5 = &DAT_00413ad3;
  do
    {
      iVar1 = func_0x00402150 (&DAT_00413acd, puVar5);
      if (iVar1 == 0)
	break;
      ppuVar4 = ppuVar4 + 2;
      puVar5 = *ppuVar4;
    }
  while (puVar5 != (undefined *) 0x0);
  puVar5 = ppuVar4[1];
  if (puVar5 == (undefined *) 0x0)
    {
      func_0x00401d50 ("\n%s online help: <%s>\n", "GNU coreutils",
		       "https://www.gnu.org/software/coreutils/");
      lVar2 = func_0x004021b0 (5, 0);
      if (lVar2 == 0)
	goto LAB_00402eb5;
      iVar1 = func_0x00402060 (lVar2, &DAT_00413b57, 3);
      if (iVar1 == 0)
	{
	  func_0x00401d50 ("Full documentation at: <%s%s>\n",
			   "https://www.gnu.org/software/coreutils/",
			   &DAT_00413acd);
	  puVar5 = &DAT_00413acd;
	  uVar3 = 0x413aef;
	  goto LAB_00402e9c;
	}
      puVar5 = &DAT_00413acd;
    LAB_00402e5a:
      ;
      func_0x00401d50
	("Report %s translation bugs to <https://translationproject.org/team/>\n",
	 &DAT_00413acd);
    }
  else
    {
      func_0x00401d50 ("\n%s online help: <%s>\n", "GNU coreutils",
		       "https://www.gnu.org/software/coreutils/");
      lVar2 = func_0x004021b0 (5, 0);
      if ((lVar2 != 0)
	  && (iVar1 = func_0x00402060 (lVar2, &DAT_00413b57, 3), iVar1 != 0))
	goto LAB_00402e5a;
    }
  func_0x00401d50 ("Full documentation at: <%s%s>\n",
		   "https://www.gnu.org/software/coreutils/", &DAT_00413acd);
  uVar3 = 0x414d8f;
  if (puVar5 == &DAT_00413acd)
    {
      uVar3 = 0x413aef;
    }
LAB_00402e9c:
  ;
  do
    {
      func_0x00401d50
	("or available locally via: info \'(coreutils) %s%s\'\n", puVar5,
	 uVar3);
    LAB_00402eae:
      ;
      func_0x00402220 ((ulong) uParm1);
    LAB_00402eb5:
      ;
      func_0x00401d50 ("Full documentation at: <%s%s>\n",
		       "https://www.gnu.org/software/coreutils/",
		       &DAT_00413acd);
      puVar5 = &DAT_00413acd;
      uVar3 = 0x413aef;
    }
  while (true);
}

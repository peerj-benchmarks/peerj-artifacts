
#include "mktemp.h"

long null_ARRAY_006124a_0_8_;
long null_ARRAY_006124a_8_8_;
long null_ARRAY_006126c_0_8_;
long null_ARRAY_006126c_16_8_;
long null_ARRAY_006126c_24_8_;
long null_ARRAY_006126c_32_8_;
long null_ARRAY_006126c_40_8_;
long null_ARRAY_006126c_48_8_;
long null_ARRAY_006126c_8_8_;
long null_ARRAY_0061270_0_4_;
long null_ARRAY_0061270_16_8_;
long null_ARRAY_0061270_4_4_;
long null_ARRAY_0061270_8_4_;
long local_68_2_2_;
long local_6b_1_3_;
long local_6c_4_4_;
long local_6e_4_4_;
long local_6f_4_4_;
long local_70_0_2_;
long local_70_4_4_;
long local_71_0_1_;
long local_71_4_4_;
long local_72_4_4_;
long DAT_00000010;
long DAT_0040e707;
long DAT_0040e78b;
long DAT_0040e828;
long DAT_0040f150;
long DAT_0040f154;
long DAT_0040f158;
long DAT_0040f15b;
long DAT_0040f15d;
long DAT_0040f161;
long DAT_0040f165;
long DAT_0040f992;
long DAT_0040fd35;
long DAT_0040fe39;
long DAT_0040fe3f;
long DAT_0040fe51;
long DAT_0040fe52;
long DAT_0040fe70;
long DAT_0040fe74;
long DAT_0040ff17;
long DAT_0040ff2d;
long DAT_00612000;
long DAT_00612010;
long DAT_00612020;
long DAT_00612448;
long DAT_006124b0;
long DAT_006124b4;
long DAT_006124b8;
long DAT_006124bc;
long DAT_00612500;
long DAT_00612510;
long DAT_00612520;
long DAT_00612528;
long DAT_00612540;
long DAT_00612548;
long DAT_00612590;
long DAT_00612598;
long DAT_006125a0;
long DAT_006125a8;
long DAT_00612738;
long DAT_00612778;
long DAT_00612780;
long DAT_00612788;
long DAT_00612798;
long fde_00410b78;
long null_ARRAY_0040f000;
long null_ARRAY_00410340;
long null_ARRAY_00410580;
long null_ARRAY_006124a0;
long null_ARRAY_00612560;
long null_ARRAY_006125c0;
long null_ARRAY_006126c0;
long null_ARRAY_00612700;
long PTR_DAT_00612440;
long PTR_null_ARRAY_00612498;
long PTR_null_ARRAY_006124c0;
void
FUN_00401f11 (uint uParm1)
{
  int iVar1;
  long lVar2;
  undefined8 uVar3;
  undefined **ppuVar4;
  char *pcVar5;
  undefined *puVar6;
  undefined *local_88;
  char *local_80;
  undefined8 local_78;
  char *local_70;
  char *local_68;
  char *local_60;
  char *local_58;
  char *local_50;
  char *local_48;
  char *local_40;
  char *local_38;
  char *local_30;
  undefined8 local_28;
  undefined8 local_20;

  ppuVar4 = &local_88;
  if (uParm1 != 0)
    {
      func_0x00401a20 (DAT_00612520,
		       "Try \'%s --help\' for more information.\n",
		       DAT_006125a8);
      goto LAB_0040211d;
    }
  func_0x00401850 ("Usage: %s [OPTION]... [TEMPLATE]\n", DAT_006125a8);
  uVar3 = DAT_00612500;
  func_0x00401a30
    ("Create a temporary file or directory, safely, and print its name.\nTEMPLATE must contain at least 3 consecutive \'X\'s in last component.\nIf TEMPLATE is not specified, use tmp.XXXXXXXXXX, and --tmpdir is implied.\n",
     DAT_00612500);
  func_0x00401a30
    ("Files are created u+rw, and directories u+rwx, minus umask restrictions.\n",
     uVar3);
  func_0x00401a30 (0x40fe6e, uVar3);
  func_0x00401a30
    ("  -d, --directory     create a directory, not a file\n  -u, --dry-run       do not create anything; merely print a name (unsafe)\n  -q, --quiet         suppress diagnostics about file/dir-creation failure\n",
     uVar3);
  func_0x00401a30
    ("      --suffix=SUFF   append SUFF to TEMPLATE; SUFF must not contain a slash.\n                        This option is implied if TEMPLATE does not end in X\n",
     uVar3);
  func_0x00401a30
    ("  -p DIR, --tmpdir[=DIR]  interpret TEMPLATE relative to DIR; if DIR is not\n                        specified, use $TMPDIR if set, else /tmp.  With\n                        this option, TEMPLATE must not be an absolute name;\n                        unlike with -t, TEMPLATE may contain slashes, but\n                        mktemp creates only the final component\n",
     uVar3);
  func_0x00401a30
    ("  -t                  interpret TEMPLATE as a single file name component,\n                        relative to a directory: $TMPDIR, if set; else the\n                        directory specified via -p; else /tmp [deprecated]\n",
     uVar3);
  func_0x00401a30 ("      --help     display this help and exit\n", uVar3);
  func_0x00401a30 ("      --version  output version information and exit\n",
		   uVar3);
  local_88 = &DAT_0040e707;
  local_80 = "test invocation";
  local_78 = 0x40e76a;
  local_70 = "Multi-call invocation";
  local_68 = "sha224sum";
  local_60 = "sha2 utilities";
  local_58 = "sha256sum";
  local_50 = "sha2 utilities";
  local_48 = "sha384sum";
  local_40 = "sha2 utilities";
  local_38 = "sha512sum";
  local_30 = "sha2 utilities";
  local_28 = 0;
  local_20 = 0;
  puVar6 = &DAT_0040e707;
  do
    {
      iVar1 = func_0x00401b80 ("mktemp", puVar6);
      if (iVar1 == 0)
	break;
      ppuVar4 = ppuVar4 + 2;
      puVar6 = *ppuVar4;
    }
  while (puVar6 != (undefined *) 0x0);
  pcVar5 = ppuVar4[1];
  if (pcVar5 == (char *) 0x0)
    {
      func_0x00401850 ("\n%s online help: <%s>\n", "GNU coreutils",
		       "https://www.gnu.org/software/coreutils/");
      lVar2 = func_0x00401be0 (5, 0);
      if (lVar2 == 0)
	goto LAB_00402124;
      iVar1 = func_0x00401aa0 (lVar2, &DAT_0040e78b, 3);
      if (iVar1 == 0)
	{
	  func_0x00401850 ("Full documentation at: <%s%s>\n",
			   "https://www.gnu.org/software/coreutils/",
			   "mktemp");
	  pcVar5 = "mktemp";
	  uVar3 = 0x40e723;
	  goto LAB_0040210b;
	}
      pcVar5 = "mktemp";
    LAB_004020c9:
      ;
      func_0x00401850
	("Report %s translation bugs to <https://translationproject.org/team/>\n",
	 "mktemp");
    }
  else
    {
      func_0x00401850 ("\n%s online help: <%s>\n", "GNU coreutils",
		       "https://www.gnu.org/software/coreutils/");
      lVar2 = func_0x00401be0 (5, 0);
      if ((lVar2 != 0)
	  && (iVar1 = func_0x00401aa0 (lVar2, &DAT_0040e78b, 3), iVar1 != 0))
	goto LAB_004020c9;
    }
  func_0x00401850 ("Full documentation at: <%s%s>\n",
		   "https://www.gnu.org/software/coreutils/", "mktemp");
  uVar3 = 0x40fe6f;
  if (pcVar5 == "mktemp")
    {
      uVar3 = 0x40e723;
    }
LAB_0040210b:
  ;
  do
    {
      func_0x00401850
	("or available locally via: info \'(coreutils) %s%s\'\n", pcVar5,
	 uVar3);
    LAB_0040211d:
      ;
      func_0x00401c30 ((ulong) uParm1);
    LAB_00402124:
      ;
      func_0x00401850 ("Full documentation at: <%s%s>\n",
		       "https://www.gnu.org/software/coreutils/", "mktemp");
      pcVar5 = "mktemp";
      uVar3 = 0x40e723;
    }
  while (true);
}


#include "id.h"

long null_ARRAY_006116c_0_8_;
long null_ARRAY_006116c_8_8_;
long null_ARRAY_0061194_0_8_;
long null_ARRAY_0061194_16_8_;
long null_ARRAY_0061194_24_8_;
long null_ARRAY_0061194_32_8_;
long null_ARRAY_0061194_40_8_;
long null_ARRAY_0061194_48_8_;
long null_ARRAY_0061194_8_8_;
long null_ARRAY_0061198_0_4_;
long null_ARRAY_0061198_16_8_;
long null_ARRAY_0061198_4_4_;
long null_ARRAY_0061198_8_4_;
long local_68_2_2_;
long local_6b_1_3_;
long local_6c_4_4_;
long local_6e_4_4_;
long local_6f_4_4_;
long local_70_0_2_;
long local_70_4_4_;
long local_71_0_1_;
long local_71_4_4_;
long local_72_4_4_;
long DAT_00000010;
long DAT_0040e000;
long DAT_0040e021;
long DAT_0040e0a5;
long DAT_0040e15d;
long DAT_0040e900;
long DAT_0040e904;
long DAT_0040e908;
long DAT_0040e90b;
long DAT_0040e90d;
long DAT_0040e911;
long DAT_0040e915;
long DAT_0040f0d3;
long DAT_0040f7e6;
long DAT_0040f8e9;
long DAT_0040f8ef;
long DAT_0040f8f1;
long DAT_0040f8f2;
long DAT_0040f910;
long DAT_0040f914;
long DAT_0040f99e;
long DAT_00611238;
long DAT_00611248;
long DAT_00611258;
long DAT_00611660;
long DAT_00611670;
long DAT_006116d0;
long DAT_006116d4;
long DAT_006116d8;
long DAT_006116dc;
long DAT_00611700;
long DAT_00611710;
long DAT_00611720;
long DAT_00611728;
long DAT_00611740;
long DAT_00611748;
long DAT_006117c8;
long DAT_006117cc;
long DAT_006117d0;
long DAT_006117d4;
long DAT_006117d8;
long DAT_006117f8;
long DAT_00611800;
long DAT_00611808;
long DAT_006119b8;
long DAT_006119c0;
long DAT_006119c8;
long DAT_006119d8;
long fde_00410540;
long null_ARRAY_0040e740;
long null_ARRAY_0040fdc0;
long null_ARRAY_00410000;
long null_ARRAY_006116c0;
long null_ARRAY_00611760;
long null_ARRAY_00611790;
long null_ARRAY_006117b0;
long null_ARRAY_006117e0;
long null_ARRAY_00611840;
long null_ARRAY_00611940;
long null_ARRAY_00611980;
long PTR_DAT_00611668;
long PTR_null_ARRAY_006116b8;
void
FUN_00401dde (uint uParm1)
{
  int iVar1;
  long lVar2;
  undefined8 uVar3;
  undefined **ppuVar4;
  undefined *puVar5;
  undefined *local_88;
  char *local_80;
  undefined8 local_78;
  char *local_70;
  char *local_68;
  char *local_60;
  char *local_58;
  char *local_50;
  char *local_48;
  char *local_40;
  char *local_38;
  char *local_30;
  undefined8 local_28;
  undefined8 local_20;

  ppuVar4 = &local_88;
  if (uParm1 != 0)
    {
      func_0x00401960 (DAT_00611720,
		       "Try \'%s --help\' for more information.\n",
		       DAT_00611808);
      goto LAB_00401fb6;
    }
  func_0x00401790 ("Usage: %s [OPTION]... [USER]\n", DAT_00611808);
  uVar3 = DAT_00611700;
  func_0x00401970
    ("Print user and group information for the specified USER,\nor (when USER omitted) for the current user.\n\n",
     DAT_00611700);
  func_0x00401970
    ("  -a             ignore, for compatibility with other versions\n  -Z, --context  print only the security context of the process\n  -g, --group    print only the effective group ID\n  -G, --groups   print all group IDs\n  -n, --name     print a name instead of a number, for -ugG\n  -r, --real     print the real ID instead of the effective ID, with -ugG\n  -u, --user     print only the effective user ID\n  -z, --zero     delimit entries with NUL characters, not whitespace;\n                   not permitted in default format\n",
     uVar3);
  func_0x00401970 ("      --help     display this help and exit\n", uVar3);
  func_0x00401970 ("      --version  output version information and exit\n",
		   uVar3);
  func_0x00401970
    ("\nWithout any OPTION, print some useful set of identified information.\n",
     uVar3);
  local_88 = &DAT_0040e021;
  local_80 = "test invocation";
  local_78 = 0x40e084;
  local_70 = "Multi-call invocation";
  local_68 = "sha224sum";
  local_60 = "sha2 utilities";
  local_58 = "sha256sum";
  local_50 = "sha2 utilities";
  local_48 = "sha384sum";
  local_40 = "sha2 utilities";
  local_38 = "sha512sum";
  local_30 = "sha2 utilities";
  local_28 = 0;
  local_20 = 0;
  puVar5 = &DAT_0040e021;
  do
    {
      iVar1 = func_0x00401a70 (&DAT_0040e000, puVar5);
      if (iVar1 == 0)
	break;
      ppuVar4 = ppuVar4 + 2;
      puVar5 = *ppuVar4;
    }
  while (puVar5 != (undefined *) 0x0);
  puVar5 = ppuVar4[1];
  if (puVar5 == (undefined *) 0x0)
    {
      func_0x00401790 ("\n%s online help: <%s>\n", "GNU coreutils",
		       "https://www.gnu.org/software/coreutils/");
      lVar2 = func_0x00401af0 (5, 0);
      if (lVar2 == 0)
	goto LAB_00401fbd;
      iVar1 = func_0x004019d0 (lVar2, &DAT_0040e0a5, 3);
      if (iVar1 == 0)
	{
	  func_0x00401790 ("Full documentation at: <%s%s>\n",
			   "https://www.gnu.org/software/coreutils/",
			   &DAT_0040e000);
	  puVar5 = &DAT_0040e000;
	  uVar3 = 0x40e03d;
	  goto LAB_00401fa4;
	}
      puVar5 = &DAT_0040e000;
    LAB_00401f62:
      ;
      func_0x00401790
	("Report %s translation bugs to <https://translationproject.org/team/>\n",
	 &DAT_0040e000);
    }
  else
    {
      func_0x00401790 ("\n%s online help: <%s>\n", "GNU coreutils",
		       "https://www.gnu.org/software/coreutils/");
      lVar2 = func_0x00401af0 (5, 0);
      if ((lVar2 != 0)
	  && (iVar1 = func_0x004019d0 (lVar2, &DAT_0040e0a5, 3), iVar1 != 0))
	goto LAB_00401f62;
    }
  func_0x00401790 ("Full documentation at: <%s%s>\n",
		   "https://www.gnu.org/software/coreutils/", &DAT_0040e000);
  uVar3 = 0x40f90f;
  if (puVar5 == &DAT_0040e000)
    {
      uVar3 = 0x40e03d;
    }
LAB_00401fa4:
  ;
  do
    {
      func_0x00401790
	("or available locally via: info \'(coreutils) %s%s\'\n", puVar5,
	 uVar3);
    LAB_00401fb6:
      ;
      func_0x00401b40 ((ulong) uParm1);
    LAB_00401fbd:
      ;
      func_0x00401790 ("Full documentation at: <%s%s>\n",
		       "https://www.gnu.org/software/coreutils/",
		       &DAT_0040e000);
      puVar5 = &DAT_0040e000;
      uVar3 = 0x40e03d;
    }
  while (true);
}

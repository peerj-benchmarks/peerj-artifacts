
#include "hostid.h"

long null_ARRAY_0060f42_0_8_;
long null_ARRAY_0060f42_8_8_;
long null_ARRAY_0060f60_0_8_;
long null_ARRAY_0060f60_16_8_;
long null_ARRAY_0060f60_24_8_;
long null_ARRAY_0060f60_32_8_;
long null_ARRAY_0060f60_40_8_;
long null_ARRAY_0060f60_48_8_;
long null_ARRAY_0060f60_8_8_;
long null_ARRAY_0060f64_0_4_;
long null_ARRAY_0060f64_16_8_;
long null_ARRAY_0060f64_4_4_;
long null_ARRAY_0060f64_8_4_;
long local_68_2_2_;
long local_6b_1_3_;
long local_6c_4_4_;
long local_6e_4_4_;
long local_6f_4_4_;
long local_70_0_2_;
long local_70_4_4_;
long local_71_0_1_;
long local_71_4_4_;
long local_72_4_4_;
long DAT_00000010;
long DAT_0040c847;
long DAT_0040c8cb;
long DAT_0040caf8;
long DAT_0040cbe0;
long DAT_0040cbe4;
long DAT_0040cbe8;
long DAT_0040cbeb;
long DAT_0040cbed;
long DAT_0040cbf1;
long DAT_0040cbf5;
long DAT_0040d3ab;
long DAT_0040d755;
long DAT_0040d859;
long DAT_0040d85f;
long DAT_0040d871;
long DAT_0040d872;
long DAT_0040d890;
long DAT_0040d894;
long DAT_0040d91e;
long DAT_0060f000;
long DAT_0060f010;
long DAT_0060f020;
long DAT_0060f3c8;
long DAT_0060f430;
long DAT_0060f434;
long DAT_0060f438;
long DAT_0060f43c;
long DAT_0060f440;
long DAT_0060f450;
long DAT_0060f460;
long DAT_0060f468;
long DAT_0060f480;
long DAT_0060f488;
long DAT_0060f4d0;
long DAT_0060f4d8;
long DAT_0060f4e0;
long DAT_0060f678;
long DAT_0060f680;
long DAT_0060f688;
long DAT_0060f698;
long fde_0040e460;
long null_ARRAY_0040cac0;
long null_ARRAY_0040cb40;
long null_ARRAY_0040dd40;
long null_ARRAY_0040df80;
long null_ARRAY_0060f420;
long null_ARRAY_0060f4a0;
long null_ARRAY_0060f500;
long null_ARRAY_0060f600;
long null_ARRAY_0060f640;
long PTR_DAT_0060f3c0;
long PTR_null_ARRAY_0060f418;
void
FUN_004019de (uint uParm1)
{
  int iVar1;
  long lVar2;
  undefined8 uVar3;
  undefined **ppuVar4;
  char *pcVar5;
  undefined *puVar6;
  undefined *local_88;
  char *local_80;
  undefined8 local_78;
  char *local_70;
  char *local_68;
  char *local_60;
  char *local_58;
  char *local_50;
  char *local_48;
  char *local_40;
  char *local_38;
  char *local_30;
  undefined8 local_28;
  undefined8 local_20;

  ppuVar4 = &local_88;
  if (uParm1 != 0)
    {
      func_0x004015c0 (DAT_0060f460,
		       "Try \'%s --help\' for more information.\n",
		       DAT_0060f4e0);
      goto LAB_00401b8f;
    }
  func_0x00401450
    ("Usage: %s [OPTION]\nPrint the numeric identifier (in hexadecimal) for the current host.\n\n",
     DAT_0060f4e0);
  uVar3 = DAT_0060f440;
  func_0x004015d0 ("      --help     display this help and exit\n",
		   DAT_0060f440);
  func_0x004015d0 ("      --version  output version information and exit\n",
		   uVar3);
  local_88 = &DAT_0040c847;
  local_80 = "test invocation";
  local_78 = 0x40c8aa;
  local_70 = "Multi-call invocation";
  local_68 = "sha224sum";
  local_60 = "sha2 utilities";
  local_58 = "sha256sum";
  local_50 = "sha2 utilities";
  local_48 = "sha384sum";
  local_40 = "sha2 utilities";
  local_38 = "sha512sum";
  local_30 = "sha2 utilities";
  local_28 = 0;
  local_20 = 0;
  puVar6 = &DAT_0040c847;
  do
    {
      iVar1 = func_0x004016a0 ("hostid", puVar6);
      if (iVar1 == 0)
	break;
      ppuVar4 = ppuVar4 + 2;
      puVar6 = *ppuVar4;
    }
  while (puVar6 != (undefined *) 0x0);
  pcVar5 = ppuVar4[1];
  if (pcVar5 == (char *) 0x0)
    {
      func_0x00401450 ("\n%s online help: <%s>\n", "GNU coreutils",
		       "https://www.gnu.org/software/coreutils/");
      lVar2 = func_0x00401700 (5, 0);
      if (lVar2 == 0)
	goto LAB_00401b96;
      iVar1 = func_0x00401620 (lVar2, &DAT_0040c8cb, 3);
      if (iVar1 == 0)
	{
	  func_0x00401450 ("Full documentation at: <%s%s>\n",
			   "https://www.gnu.org/software/coreutils/",
			   "hostid");
	  pcVar5 = "hostid";
	  uVar3 = 0x40c863;
	  goto LAB_00401b7d;
	}
      pcVar5 = "hostid";
    LAB_00401b3b:
      ;
      func_0x00401450
	("Report %s translation bugs to <https://translationproject.org/team/>\n",
	 "hostid");
    }
  else
    {
      func_0x00401450 ("\n%s online help: <%s>\n", "GNU coreutils",
		       "https://www.gnu.org/software/coreutils/");
      lVar2 = func_0x00401700 (5, 0);
      if ((lVar2 != 0)
	  && (iVar1 = func_0x00401620 (lVar2, &DAT_0040c8cb, 3), iVar1 != 0))
	goto LAB_00401b3b;
    }
  func_0x00401450 ("Full documentation at: <%s%s>\n",
		   "https://www.gnu.org/software/coreutils/", "hostid");
  uVar3 = 0x40d88f;
  if (pcVar5 == "hostid")
    {
      uVar3 = 0x40c863;
    }
LAB_00401b7d:
  ;
  do
    {
      func_0x00401450
	("or available locally via: info \'(coreutils) %s%s\'\n", pcVar5,
	 uVar3);
    LAB_00401b8f:
      ;
      func_0x00401740 ((ulong) uParm1);
    LAB_00401b96:
      ;
      func_0x00401450 ("Full documentation at: <%s%s>\n",
		       "https://www.gnu.org/software/coreutils/", "hostid");
      pcVar5 = "hostid";
      uVar3 = 0x40c863;
    }
  while (true);
}

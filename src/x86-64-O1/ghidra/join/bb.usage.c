
#include "join.h"

long DAT_006135f_0_1_;
long DAT_006135f_1_1_;
long null_ARRAY_006134a_0_8_;
long null_ARRAY_006134a_8_8_;
long null_ARRAY_006135c_16_8_;
long null_ARRAY_0061360_0_8_;
long null_ARRAY_0061360_8_8_;
long null_ARRAY_0061361_0_8_;
long null_ARRAY_0061361_8_8_;
long null_ARRAY_0061363_0_8_;
long null_ARRAY_0061363_8_8_;
long null_ARRAY_0061378_0_8_;
long null_ARRAY_0061378_16_8_;
long null_ARRAY_0061378_24_8_;
long null_ARRAY_0061378_32_8_;
long null_ARRAY_0061378_40_8_;
long null_ARRAY_0061378_48_8_;
long null_ARRAY_0061378_8_8_;
long null_ARRAY_006137c_0_4_;
long null_ARRAY_006137c_16_8_;
long null_ARRAY_006137c_4_4_;
long null_ARRAY_006137c_8_4_;
long local_68_2_2_;
long local_6b_1_3_;
long local_6c_4_4_;
long local_6e_4_4_;
long local_6f_4_4_;
long local_70_0_2_;
long local_70_4_4_;
long local_71_0_1_;
long local_71_4_4_;
long local_72_4_4_;
long DAT_00000010;
long DAT_0040eed9;
long DAT_0040eedc;
long DAT_0040ef43;
long DAT_0040ef48;
long DAT_0040efcc;
long DAT_0040efe1;
long DAT_0040efe6;
long DAT_0040fc58;
long DAT_0040fca0;
long DAT_0040fca4;
long DAT_0040fca8;
long DAT_0040fcab;
long DAT_0040fcad;
long DAT_0040fcb1;
long DAT_0040fcb5;
long DAT_0041046b;
long DAT_00410bfe;
long DAT_00410d01;
long DAT_00410d07;
long DAT_00410d19;
long DAT_00410d1a;
long DAT_00410d38;
long DAT_00410dbe;
long DAT_00613000;
long DAT_00613010;
long DAT_00613020;
long DAT_00613420;
long DAT_00613424;
long DAT_00613430;
long DAT_00613438;
long DAT_00613448;
long DAT_006134b0;
long DAT_006134b4;
long DAT_006134b8;
long DAT_006134bc;
long DAT_006134c0;
long DAT_006134c8;
long DAT_006134d0;
long DAT_006134e0;
long DAT_006134e8;
long DAT_00613500;
long DAT_00613508;
long DAT_00613560;
long DAT_00613561;
long DAT_006135b0;
long DAT_006135d8;
long DAT_006135e0;
long DAT_006135e8;
long DAT_006135f0;
long DAT_006135f8;
long DAT_006135fa;
long DAT_006135fb;
long DAT_006135fc;
long DAT_006135fd;
long DAT_006135fe;
long DAT_00613640;
long DAT_00613648;
long DAT_00613650;
long DAT_006137b8;
long DAT_006137f8;
long DAT_00613800;
long DAT_00613808;
long DAT_00613818;
long fde_00411a18;
long null_ARRAY_0040fb40;
long null_ARRAY_004111e0;
long null_ARRAY_00411420;
long null_ARRAY_006134a0;
long null_ARRAY_00613520;
long null_ARRAY_00613580;
long null_ARRAY_006135c0;
long null_ARRAY_00613600;
long null_ARRAY_00613610;
long null_ARRAY_00613620;
long null_ARRAY_00613630;
long null_ARRAY_00613680;
long null_ARRAY_00613780;
long null_ARRAY_006137c0;
long PTR_DAT_00613440;
long PTR_null_ARRAY_00613428;
long PTR_null_ARRAY_00613498;
void
FUN_0040276c (uint uParm1)
{
  undefined8 uVar1;
  int iVar2;
  long lVar3;
  undefined1 *puVar4;
  undefined **ppuVar5;
  undefined *puVar6;
  undefined *local_88;
  char *local_80;
  undefined8 local_78;
  char *local_70;
  char *local_68;
  char *local_60;
  char *local_58;
  char *local_50;
  char *local_48;
  char *local_40;
  char *local_38;
  char *local_30;
  undefined8 local_28;
  undefined8 local_20;

  ppuVar5 = &local_88;
  if (uParm1 != 0)
    {
      func_0x004018e0 (DAT_006134e0,
		       "Try \'%s --help\' for more information.\n",
		       DAT_00613650);
      goto LAB_00402978;
    }
  func_0x00401740 ("Usage: %s [OPTION]... FILE1 FILE2\n", DAT_00613650);
  uVar1 = DAT_006134c0;
  func_0x004018f0
    ("For each pair of input lines with identical join fields, write a line to\nstandard output.  The default join field is the first, delimited by blanks.\n",
     DAT_006134c0);
  func_0x004018f0
    ("\nWhen FILE1 or FILE2 (not both) is -, read standard input.\n", uVar1);
  func_0x004018f0
    ("\n  -a FILENUM        also print unpairable lines from file FILENUM, where\n                      FILENUM is 1 or 2, corresponding to FILE1 or FILE2\n  -e EMPTY          replace missing input fields with EMPTY\n",
     uVar1);
  func_0x004018f0
    ("  -i, --ignore-case  ignore differences in case when comparing fields\n  -j FIELD          equivalent to \'-1 FIELD -2 FIELD\'\n  -o FORMAT         obey FORMAT while constructing output line\n  -t CHAR           use CHAR as input and output field separator\n",
     uVar1);
  func_0x004018f0
    ("  -v FILENUM        like -a FILENUM, but suppress joined output lines\n  -1 FIELD          join on this FIELD of file 1\n  -2 FIELD          join on this FIELD of file 2\n  --check-order     check that the input is correctly sorted, even\n                      if all input lines are pairable\n  --nocheck-order   do not check that the input is correctly sorted\n  --header          treat the first line in each file as field headers,\n                      print them without trying to pair them\n",
     uVar1);
  func_0x004018f0
    ("  -z, --zero-terminated     line delimiter is NUL, not newline\n",
     uVar1);
  func_0x004018f0 ("      --help     display this help and exit\n", uVar1);
  func_0x004018f0 ("      --version  output version information and exit\n",
		   uVar1);
  func_0x004018f0
    ("\nUnless -t CHAR is given, leading blanks separate fields and are ignored,\nelse fields are separated by CHAR.  Any FIELD is a field number counted\nfrom 1.  FORMAT is one or more comma or blank separated specifications,\neach being \'FILENUM.FIELD\' or \'0\'.  Default FORMAT outputs the join field,\nthe remaining fields from FILE1, the remaining fields from FILE2, all\nseparated by CHAR.  If FORMAT is the keyword \'auto\', then the first\nline of each file determines the number of fields output for each line.\n\nImportant: FILE1 and FILE2 must be sorted on the join fields.\nE.g., use \"sort -k 1b,1\" if \'join\' has no options,\nor use \"join -t \'\'\" if \'sort\' has no options.\nNote, comparisons honor the rules specified by \'LC_COLLATE\'.\nIf the input is not sorted and some lines cannot be joined, a\nwarning message will be given.\n",
     uVar1);
  local_88 = &DAT_0040ef48;
  local_80 = "test invocation";
  local_78 = 0x40efab;
  local_70 = "Multi-call invocation";
  local_68 = "sha224sum";
  local_60 = "sha2 utilities";
  local_58 = "sha256sum";
  local_50 = "sha2 utilities";
  local_48 = "sha384sum";
  local_40 = "sha2 utilities";
  local_38 = "sha512sum";
  local_30 = "sha2 utilities";
  local_28 = 0;
  local_20 = 0;
  puVar6 = &DAT_0040ef48;
  do
    {
      iVar2 = func_0x00401a00 (&DAT_0040ef43, puVar6);
      if (iVar2 == 0)
	break;
      ppuVar5 = ppuVar5 + 2;
      puVar6 = *ppuVar5;
    }
  while (puVar6 != (undefined *) 0x0);
  puVar6 = ppuVar5[1];
  if (puVar6 == (undefined *) 0x0)
    {
      func_0x00401740 ("\n%s online help: <%s>\n", "GNU coreutils",
		       "https://www.gnu.org/software/coreutils/");
      lVar3 = func_0x00401a60 (5, 0);
      if (lVar3 == 0)
	goto LAB_0040297f;
      iVar2 = func_0x00401960 (lVar3, &DAT_0040efcc, 3);
      if (iVar2 == 0)
	{
	  func_0x00401740 ("Full documentation at: <%s%s>\n",
			   "https://www.gnu.org/software/coreutils/",
			   &DAT_0040ef43);
	  puVar6 = &DAT_0040ef43;
	  puVar4 = (undefined1 *) 0x40ef64;
	  goto LAB_00402966;
	}
      puVar6 = &DAT_0040ef43;
    LAB_00402924:
      ;
      func_0x00401740
	("Report %s translation bugs to <https://translationproject.org/team/>\n",
	 &DAT_0040ef43);
    }
  else
    {
      func_0x00401740 ("\n%s online help: <%s>\n", "GNU coreutils",
		       "https://www.gnu.org/software/coreutils/");
      lVar3 = func_0x00401a60 (5, 0);
      if ((lVar3 != 0)
	  && (iVar2 = func_0x00401960 (lVar3, &DAT_0040efcc, 3), iVar2 != 0))
	goto LAB_00402924;
    }
  func_0x00401740 ("Full documentation at: <%s%s>\n",
		   "https://www.gnu.org/software/coreutils/", &DAT_0040ef43);
  puVar4 = &DAT_0040eedc;
  if (puVar6 == &DAT_0040ef43)
    {
      puVar4 = (undefined1 *) 0x40ef64;
    }
LAB_00402966:
  ;
  do
    {
      func_0x00401740
	("or available locally via: info \'(coreutils) %s%s\'\n", puVar6,
	 puVar4);
    LAB_00402978:
      ;
      func_0x00401ad0 ((ulong) uParm1);
    LAB_0040297f:
      ;
      func_0x00401740 ("Full documentation at: <%s%s>\n",
		       "https://www.gnu.org/software/coreutils/",
		       &DAT_0040ef43);
      puVar6 = &DAT_0040ef43;
      puVar4 = (undefined1 *) 0x40ef64;
    }
  while (true);
}

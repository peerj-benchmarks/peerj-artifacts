
#include "paste.h"

long null_ARRAY_0061044_0_8_;
long null_ARRAY_0061044_8_8_;
long null_ARRAY_0061064_0_8_;
long null_ARRAY_0061064_16_8_;
long null_ARRAY_0061064_24_8_;
long null_ARRAY_0061064_32_8_;
long null_ARRAY_0061064_40_8_;
long null_ARRAY_0061064_48_8_;
long null_ARRAY_0061064_8_8_;
long null_ARRAY_0061068_0_4_;
long null_ARRAY_0061068_16_8_;
long null_ARRAY_0061068_4_4_;
long null_ARRAY_0061068_8_4_;
long local_68_2_2_;
long local_6b_1_3_;
long local_6c_4_4_;
long local_6e_4_4_;
long local_6f_4_4_;
long local_70_0_2_;
long local_70_4_4_;
long local_71_0_1_;
long local_71_4_4_;
long local_72_4_4_;
long DAT_00000010;
long DAT_0040d2ab;
long DAT_0040d32f;
long DAT_0040d337;
long DAT_0040d357;
long DAT_0040d850;
long DAT_0040d854;
long DAT_0040d858;
long DAT_0040d85b;
long DAT_0040d85d;
long DAT_0040d861;
long DAT_0040d865;
long DAT_0040dfeb;
long DAT_0040e395;
long DAT_0040e499;
long DAT_0040e49f;
long DAT_0040e4b1;
long DAT_0040e4b2;
long DAT_0040e4d0;
long DAT_0040e4d4;
long DAT_0040e55e;
long DAT_00610000;
long DAT_00610010;
long DAT_00610020;
long DAT_006103e0;
long DAT_006103f0;
long DAT_00610450;
long DAT_00610454;
long DAT_00610458;
long DAT_0061045c;
long DAT_00610480;
long DAT_00610488;
long DAT_00610490;
long DAT_006104a0;
long DAT_006104a8;
long DAT_006104c0;
long DAT_006104c8;
long DAT_00610510;
long DAT_00610518;
long DAT_00610520;
long DAT_00610521;
long DAT_00610528;
long DAT_00610530;
long DAT_00610538;
long DAT_006106b8;
long DAT_006106c0;
long DAT_006106c8;
long DAT_006106d8;
long fde_0040f0c8;
long null_ARRAY_0040d740;
long null_ARRAY_0040e980;
long null_ARRAY_0040ebc0;
long null_ARRAY_00610440;
long null_ARRAY_006104e0;
long null_ARRAY_00610540;
long null_ARRAY_00610640;
long null_ARRAY_00610680;
long PTR_DAT_006103e8;
long PTR_null_ARRAY_00610438;
void
FUN_004021f0 (uint uParm1)
{
  undefined8 uVar1;
  int iVar2;
  long lVar3;
  undefined1 *puVar4;
  undefined **ppuVar5;
  char *pcVar6;
  undefined *puVar7;
  undefined *local_88;
  char *local_80;
  undefined8 local_78;
  char *local_70;
  char *local_68;
  char *local_60;
  char *local_58;
  char *local_50;
  char *local_48;
  char *local_40;
  char *local_38;
  char *local_30;
  undefined8 local_28;
  undefined8 local_20;

  ppuVar5 = &local_88;
  if (uParm1 != 0)
    {
      func_0x00401720 (DAT_006104a0,
		       "Try \'%s --help\' for more information.\n",
		       DAT_00610538);
      goto LAB_004023e2;
    }
  func_0x004015a0 ("Usage: %s [OPTION]... [FILE]...\n", DAT_00610538);
  uVar1 = DAT_00610480;
  func_0x00401730
    ("Write lines consisting of the sequentially corresponding lines from\neach FILE, separated by TABs, to standard output.\n",
     DAT_00610480);
  func_0x00401730
    ("\nWith no FILE, or when FILE is -, read standard input.\n", uVar1);
  func_0x00401730
    ("\nMandatory arguments to long options are mandatory for short options too.\n",
     uVar1);
  func_0x00401730
    ("  -d, --delimiters=LIST   reuse characters from LIST instead of TABs\n  -s, --serial            paste one file at a time instead of in parallel\n",
     uVar1);
  func_0x00401730
    ("  -z, --zero-terminated    line delimiter is NUL, not newline\n",
     uVar1);
  func_0x00401730 ("      --help     display this help and exit\n", uVar1);
  func_0x00401730 ("      --version  output version information and exit\n",
		   uVar1);
  local_88 = &DAT_0040d2ab;
  local_80 = "test invocation";
  local_78 = 0x40d30e;
  local_70 = "Multi-call invocation";
  local_68 = "sha224sum";
  local_60 = "sha2 utilities";
  local_58 = "sha256sum";
  local_50 = "sha2 utilities";
  local_48 = "sha384sum";
  local_40 = "sha2 utilities";
  local_38 = "sha512sum";
  local_30 = "sha2 utilities";
  local_28 = 0;
  local_20 = 0;
  puVar7 = &DAT_0040d2ab;
  do
    {
      iVar2 = func_0x00401820 ("paste", puVar7);
      if (iVar2 == 0)
	break;
      ppuVar5 = ppuVar5 + 2;
      puVar7 = *ppuVar5;
    }
  while (puVar7 != (undefined *) 0x0);
  pcVar6 = ppuVar5[1];
  if (pcVar6 == (char *) 0x0)
    {
      func_0x004015a0 ("\n%s online help: <%s>\n", "GNU coreutils",
		       "https://www.gnu.org/software/coreutils/");
      lVar3 = func_0x00401880 (5, 0);
      if (lVar3 == 0)
	goto LAB_004023e9;
      iVar2 = func_0x00401790 (lVar3, &DAT_0040d32f, 3);
      if (iVar2 == 0)
	{
	  func_0x004015a0 ("Full documentation at: <%s%s>\n",
			   "https://www.gnu.org/software/coreutils/",
			   "paste");
	  pcVar6 = "paste";
	  puVar4 = (undefined1 *) 0x40d2c7;
	  goto LAB_004023d0;
	}
      pcVar6 = "paste";
    LAB_0040238e:
      ;
      func_0x004015a0
	("Report %s translation bugs to <https://translationproject.org/team/>\n",
	 "paste");
    }
  else
    {
      func_0x004015a0 ("\n%s online help: <%s>\n", "GNU coreutils",
		       "https://www.gnu.org/software/coreutils/");
      lVar3 = func_0x00401880 (5, 0);
      if ((lVar3 != 0)
	  && (iVar2 = func_0x00401790 (lVar3, &DAT_0040d32f, 3), iVar2 != 0))
	goto LAB_0040238e;
    }
  func_0x004015a0 ("Full documentation at: <%s%s>\n",
		   "https://www.gnu.org/software/coreutils/", "paste");
  puVar4 = &DAT_0040d337;
  if (pcVar6 == "paste")
    {
      puVar4 = (undefined1 *) 0x40d2c7;
    }
LAB_004023d0:
  ;
  do
    {
      func_0x004015a0
	("or available locally via: info \'(coreutils) %s%s\'\n", pcVar6,
	 puVar4);
    LAB_004023e2:
      ;
      func_0x004018e0 ((ulong) uParm1);
    LAB_004023e9:
      ;
      func_0x004015a0 ("Full documentation at: <%s%s>\n",
		       "https://www.gnu.org/software/coreutils/", "paste");
      pcVar6 = "paste";
      puVar4 = (undefined1 *) 0x40d2c7;
    }
  while (true);
}

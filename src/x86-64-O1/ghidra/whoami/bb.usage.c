
#include "whoami.h"

long null_ARRAY_0060f42_0_8_;
long null_ARRAY_0060f42_8_8_;
long null_ARRAY_0060f60_0_8_;
long null_ARRAY_0060f60_16_8_;
long null_ARRAY_0060f60_24_8_;
long null_ARRAY_0060f60_32_8_;
long null_ARRAY_0060f60_40_8_;
long null_ARRAY_0060f60_48_8_;
long null_ARRAY_0060f60_8_8_;
long null_ARRAY_0060f64_0_4_;
long null_ARRAY_0060f64_16_8_;
long null_ARRAY_0060f64_4_4_;
long null_ARRAY_0060f64_8_4_;
long local_68_2_2_;
long local_6b_1_3_;
long local_6c_4_4_;
long local_6e_4_4_;
long local_6f_4_4_;
long local_70_0_2_;
long local_70_4_4_;
long local_71_0_1_;
long local_71_4_4_;
long local_72_4_4_;
long DAT_00000010;
long DAT_0040c91e;
long DAT_0040c9a2;
long DAT_0040cbd8;
long DAT_0040cca0;
long DAT_0040cca4;
long DAT_0040cca8;
long DAT_0040ccab;
long DAT_0040ccad;
long DAT_0040ccb1;
long DAT_0040ccb5;
long DAT_0040d46b;
long DAT_0040d815;
long DAT_0040d919;
long DAT_0040d91f;
long DAT_0040d931;
long DAT_0040d932;
long DAT_0040d950;
long DAT_0040d954;
long DAT_0040d9de;
long DAT_0060f000;
long DAT_0060f010;
long DAT_0060f020;
long DAT_0060f3c8;
long DAT_0060f430;
long DAT_0060f434;
long DAT_0060f438;
long DAT_0060f43c;
long DAT_0060f440;
long DAT_0060f450;
long DAT_0060f460;
long DAT_0060f468;
long DAT_0060f480;
long DAT_0060f488;
long DAT_0060f4d0;
long DAT_0060f4d8;
long DAT_0060f4e0;
long DAT_0060f678;
long DAT_0060f680;
long DAT_0060f688;
long DAT_0060f698;
long fde_0040e520;
long null_ARRAY_0040cba0;
long null_ARRAY_0040cc00;
long null_ARRAY_0040de00;
long null_ARRAY_0040e040;
long null_ARRAY_0060f420;
long null_ARRAY_0060f4a0;
long null_ARRAY_0060f500;
long null_ARRAY_0060f600;
long null_ARRAY_0060f640;
long PTR_DAT_0060f3c0;
long PTR_null_ARRAY_0060f418;
void
FUN_00401a6e (uint uParm1)
{
  int iVar1;
  long lVar2;
  undefined8 uVar3;
  undefined **ppuVar4;
  char *pcVar5;
  undefined *puVar6;
  undefined *local_88;
  char *local_80;
  undefined8 local_78;
  char *local_70;
  char *local_68;
  char *local_60;
  char *local_58;
  char *local_50;
  char *local_48;
  char *local_40;
  char *local_38;
  char *local_30;
  undefined8 local_28;
  undefined8 local_20;

  ppuVar4 = &local_88;
  if (uParm1 != 0)
    {
      func_0x00401650 (DAT_0060f460,
		       "Try \'%s --help\' for more information.\n",
		       DAT_0060f4e0);
      goto LAB_00401c2c;
    }
  func_0x004014c0 ("Usage: %s [OPTION]...\n", DAT_0060f4e0);
  uVar3 = DAT_0060f440;
  func_0x00401660
    ("Print the user name associated with the current effective user ID.\nSame as id -un.\n\n",
     DAT_0060f440);
  func_0x00401660 ("      --help     display this help and exit\n", uVar3);
  func_0x00401660 ("      --version  output version information and exit\n",
		   uVar3);
  local_88 = &DAT_0040c91e;
  local_80 = "test invocation";
  local_78 = 0x40c981;
  local_70 = "Multi-call invocation";
  local_68 = "sha224sum";
  local_60 = "sha2 utilities";
  local_58 = "sha256sum";
  local_50 = "sha2 utilities";
  local_48 = "sha384sum";
  local_40 = "sha2 utilities";
  local_38 = "sha512sum";
  local_30 = "sha2 utilities";
  local_28 = 0;
  local_20 = 0;
  puVar6 = &DAT_0040c91e;
  do
    {
      iVar1 = func_0x00401730 ("whoami", puVar6);
      if (iVar1 == 0)
	break;
      ppuVar4 = ppuVar4 + 2;
      puVar6 = *ppuVar4;
    }
  while (puVar6 != (undefined *) 0x0);
  pcVar5 = ppuVar4[1];
  if (pcVar5 == (char *) 0x0)
    {
      func_0x004014c0 ("\n%s online help: <%s>\n", "GNU coreutils",
		       "https://www.gnu.org/software/coreutils/");
      lVar2 = func_0x004017a0 (5, 0);
      if (lVar2 == 0)
	goto LAB_00401c33;
      iVar1 = func_0x004016b0 (lVar2, &DAT_0040c9a2, 3);
      if (iVar1 == 0)
	{
	  func_0x004014c0 ("Full documentation at: <%s%s>\n",
			   "https://www.gnu.org/software/coreutils/",
			   "whoami");
	  pcVar5 = "whoami";
	  uVar3 = 0x40c93a;
	  goto LAB_00401c1a;
	}
      pcVar5 = "whoami";
    LAB_00401bd8:
      ;
      func_0x004014c0
	("Report %s translation bugs to <https://translationproject.org/team/>\n",
	 "whoami");
    }
  else
    {
      func_0x004014c0 ("\n%s online help: <%s>\n", "GNU coreutils",
		       "https://www.gnu.org/software/coreutils/");
      lVar2 = func_0x004017a0 (5, 0);
      if ((lVar2 != 0)
	  && (iVar1 = func_0x004016b0 (lVar2, &DAT_0040c9a2, 3), iVar1 != 0))
	goto LAB_00401bd8;
    }
  func_0x004014c0 ("Full documentation at: <%s%s>\n",
		   "https://www.gnu.org/software/coreutils/", "whoami");
  uVar3 = 0x40d94f;
  if (pcVar5 == "whoami")
    {
      uVar3 = 0x40c93a;
    }
LAB_00401c1a:
  ;
  do
    {
      func_0x004014c0
	("or available locally via: info \'(coreutils) %s%s\'\n", pcVar5,
	 uVar3);
    LAB_00401c2c:
      ;
      func_0x004017e0 ((ulong) uParm1);
    LAB_00401c33:
      ;
      func_0x004014c0 ("Full documentation at: <%s%s>\n",
		       "https://www.gnu.org/software/coreutils/", "whoami");
      pcVar5 = "whoami";
      uVar3 = 0x40c93a;
    }
  while (true);
}

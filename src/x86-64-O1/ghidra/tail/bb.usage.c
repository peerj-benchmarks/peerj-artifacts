
#include "tail.h"

long local_58_0_4_;
long DAT_00412696;
long DAT_0041269b;
long DAT_0041271f;
long DAT_00412753;
long DAT_00412755;
long DAT_00412756;
long DAT_00412758;
long DAT_00412843;
long DAT_00413b00;
long DAT_00413b9c;
long DAT_00617010;
long DAT_00617020;
long DAT_00617480;
long DAT_00617490;
long DAT_00617498;
long DAT_0061751c;
long DAT_00617540;
long DAT_00617560;
long DAT_00617580;
long DAT_00617588;
long DAT_006175d0;
long DAT_006175d1;
long DAT_006175d2;
long DAT_006175d4;
long DAT_006175d8;
long DAT_006175d9;
long DAT_006175da;
long DAT_006175db;
long DAT_006175dc;
long DAT_006175dd;
long DAT_006175de;
long DAT_006175f8;
long DAT_00617798;
long fde_004159d8;
long null_ARRAY_00413900;
long null_ARRAY_00413b10;
long null_ARRAY_006175a0;
long PTR_DAT_00617488;
long PTR_DAT_006174a0;
long PTR_FUN_006174a8;
void
FUN_00402ff4 (uint uParm1)
{
  int iVar1;
  long lVar2;
  undefined8 uVar3;
  undefined **ppuVar4;
  undefined *puVar5;
  undefined *local_88;
  char *local_80;
  undefined8 local_78;
  char *local_70;
  char *local_68;
  char *local_60;
  char *local_58;
  char *local_50;
  char *local_48;
  char *local_40;
  char *local_38;
  char *local_30;
  undefined8 local_28;
  undefined8 local_20;

  ppuVar4 = &local_88;
  if (uParm1 != 0)
    {
      func_0x00401c20 (DAT_00617560,
		       "Try \'%s --help\' for more information.\n",
		       DAT_006175f8);
      goto LAB_00403247;
    }
  func_0x004019d0 ("Usage: %s [OPTION]... [FILE]...\n", DAT_006175f8);
  func_0x004019d0
    ("Print the last %d lines of each FILE to standard output.\nWith more than one FILE, precede each with a header giving the file name.\n",
     10);
  uVar3 = DAT_00617540;
  func_0x00401c40
    ("\nWith no FILE, or when FILE is -, read standard input.\n",
     DAT_00617540);
  func_0x00401c40
    ("\nMandatory arguments to long options are mandatory for short options too.\n",
     uVar3);
  func_0x00401c40
    ("  -c, --bytes=[+]NUM       output the last NUM bytes; or use -c +NUM to\n                             output starting with byte NUM of each file\n",
     uVar3);
  func_0x00401c40
    ("  -f, --follow[={name|descriptor}]\n                           output appended data as the file grows;\n                             an absent option argument means \'descriptor\'\n  -F                       same as --follow=name --retry\n",
     uVar3);
  func_0x004019d0
    ("  -n, --lines=[+]NUM       output the last NUM lines, instead of the last %d;\n                             or use -n +NUM to output starting with line NUM\n      --max-unchanged-stats=N\n                           with --follow=name, reopen a FILE which has not\n                             changed size after N (default %d) iterations\n                             to see if it has been unlinked or renamed\n                             (this is the usual case of rotated log files);\n                             with inotify, this option is rarely useful\n",
     10, 5);
  func_0x00401c40
    ("      --pid=PID            with -f, terminate after process ID, PID dies\n  -q, --quiet, --silent    never output headers giving file names\n      --retry              keep trying to open a file if it is inaccessible\n",
     uVar3);
  func_0x00401c40
    ("  -s, --sleep-interval=N   with -f, sleep for approximately N seconds\n                             (default 1.0) between iterations;\n                             with inotify and --pid=P, check process P at\n                             least once every N seconds\n  -v, --verbose            always output headers giving file names\n",
     uVar3);
  func_0x00401c40
    ("  -z, --zero-terminated    line delimiter is NUL, not newline\n",
     uVar3);
  func_0x00401c40 ("      --help     display this help and exit\n", uVar3);
  func_0x00401c40 ("      --version  output version information and exit\n",
		   uVar3);
  func_0x00401c40
    ("\nNUM may have a multiplier suffix:\nb 512, kB 1000, K 1024, MB 1000*1000, M 1024*1024,\nGB 1000*1000*1000, G 1024*1024*1024, and so on for T, P, E, Z, Y.\n\n",
     uVar3);
  func_0x00401c40
    ("With --follow (-f), tail defaults to following the file descriptor, which\nmeans that even if a tail\'ed file is renamed, tail will continue to track\nits end.  This default behavior is not desirable when you really want to\ntrack the actual name of the file, not the file descriptor (e.g., log\nrotation).  Use --follow=name in that case.  That causes tail to track the\nnamed file in a way that accommodates renaming, removal and creation.\n",
     uVar3);
  local_88 = &DAT_0041269b;
  local_80 = "test invocation";
  local_78 = 0x4126fe;
  local_70 = "Multi-call invocation";
  local_68 = "sha224sum";
  local_60 = "sha2 utilities";
  local_58 = "sha256sum";
  local_50 = "sha2 utilities";
  local_48 = "sha384sum";
  local_40 = "sha2 utilities";
  local_38 = "sha512sum";
  local_30 = "sha2 utilities";
  local_28 = 0;
  local_20 = 0;
  puVar5 = &DAT_0041269b;
  do
    {
      iVar1 = func_0x00401d50 (&DAT_00412696, puVar5);
      if (iVar1 == 0)
	break;
      ppuVar4 = ppuVar4 + 2;
      puVar5 = *ppuVar4;
    }
  while (puVar5 != (undefined *) 0x0);
  puVar5 = ppuVar4[1];
  if (puVar5 == (undefined *) 0x0)
    {
      func_0x004019d0 ("\n%s online help: <%s>\n", "GNU coreutils",
		       "https://www.gnu.org/software/coreutils/");
      lVar2 = func_0x00401dc0 (5, 0);
      if (lVar2 == 0)
	goto LAB_0040324e;
      iVar1 = func_0x00401cc0 (lVar2, &DAT_0041271f, 3);
      if (iVar1 == 0)
	{
	  func_0x004019d0 ("Full documentation at: <%s%s>\n",
			   "https://www.gnu.org/software/coreutils/",
			   &DAT_00412696);
	  puVar5 = &DAT_00412696;
	  uVar3 = 0x4126b7;
	  goto LAB_00403235;
	}
      puVar5 = &DAT_00412696;
    LAB_004031f3:
      ;
      func_0x004019d0
	("Report %s translation bugs to <https://translationproject.org/team/>\n",
	 &DAT_00412696);
    }
  else
    {
      func_0x004019d0 ("\n%s online help: <%s>\n", "GNU coreutils",
		       "https://www.gnu.org/software/coreutils/");
      lVar2 = func_0x00401dc0 (5, 0);
      if ((lVar2 != 0)
	  && (iVar1 = func_0x00401cc0 (lVar2, &DAT_0041271f, 3), iVar1 != 0))
	goto LAB_004031f3;
    }
  func_0x004019d0 ("Full documentation at: <%s%s>\n",
		   "https://www.gnu.org/software/coreutils/", &DAT_00412696);
  uVar3 = 0x414c44;
  if (puVar5 == &DAT_00412696)
    {
      uVar3 = 0x4126b7;
    }
LAB_00403235:
  ;
  do
    {
      func_0x004019d0
	("or available locally via: info \'(coreutils) %s%s\'\n", puVar5,
	 uVar3);
    LAB_00403247:
      ;
      func_0x00401e30 ((ulong) uParm1);
    LAB_0040324e:
      ;
      func_0x004019d0 ("Full documentation at: <%s%s>\n",
		       "https://www.gnu.org/software/coreutils/",
		       &DAT_00412696);
      puVar5 = &DAT_00412696;
      uVar3 = 0x4126b7;
    }
  while (true);
}

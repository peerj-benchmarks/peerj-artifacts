
#include "realpath.h"

long null_ARRAY_0061274_0_8_;
long null_ARRAY_0061274_8_8_;
long null_ARRAY_0061294_0_8_;
long null_ARRAY_0061294_16_8_;
long null_ARRAY_0061294_24_8_;
long null_ARRAY_0061294_32_8_;
long null_ARRAY_0061294_40_8_;
long null_ARRAY_0061294_48_8_;
long null_ARRAY_0061294_8_8_;
long null_ARRAY_0061298_0_4_;
long null_ARRAY_0061298_16_8_;
long null_ARRAY_0061298_4_4_;
long null_ARRAY_0061298_8_4_;
long local_68_2_2_;
long local_6b_1_3_;
long local_6c_4_4_;
long local_6e_4_4_;
long local_6f_4_4_;
long local_70_0_2_;
long local_70_4_4_;
long local_71_0_1_;
long local_71_4_4_;
long local_72_4_4_;
long DAT_00000010;
long DAT_0040ef58;
long DAT_0040efdc;
long DAT_0040f6a0;
long DAT_0040f6a1;
long DAT_0040f6a2;
long DAT_0040f748;
long DAT_0040f74e;
long DAT_0040f750;
long DAT_0040f754;
long DAT_0040f758;
long DAT_0040f75b;
long DAT_0040f75d;
long DAT_0040f761;
long DAT_0040f765;
long DAT_0040feeb;
long DAT_00410295;
long DAT_00410399;
long DAT_0041039f;
long DAT_004103b1;
long DAT_004103b2;
long DAT_004103d0;
long DAT_004103d4;
long DAT_0041045e;
long DAT_006122b0;
long DAT_006122c0;
long DAT_006122d0;
long DAT_006126e0;
long DAT_006126f0;
long DAT_00612750;
long DAT_00612754;
long DAT_00612758;
long DAT_0061275c;
long DAT_00612780;
long DAT_00612790;
long DAT_006127a0;
long DAT_006127a8;
long DAT_006127c0;
long DAT_006127c8;
long DAT_00612810;
long DAT_00612818;
long DAT_00612820;
long DAT_00612821;
long DAT_00612828;
long DAT_00612830;
long DAT_00612838;
long DAT_006129b8;
long DAT_006129c0;
long DAT_006129c8;
long DAT_006129d0;
long DAT_006129d8;
long DAT_006129e8;
long fde_004110d0;
long null_ARRAY_0040f500;
long null_ARRAY_0040f6e0;
long null_ARRAY_00410880;
long null_ARRAY_00410ab0;
long null_ARRAY_00612740;
long null_ARRAY_006127e0;
long null_ARRAY_00612840;
long null_ARRAY_00612940;
long null_ARRAY_00612980;
long PTR_DAT_006126e8;
long PTR_null_ARRAY_00612738;
void
FUN_00401eda (uint uParm1)
{
  int iVar1;
  long lVar2;
  undefined8 uVar3;
  undefined **ppuVar4;
  char *pcVar5;
  undefined *puVar6;
  undefined *local_88;
  char *local_80;
  undefined8 local_78;
  char *local_70;
  char *local_68;
  char *local_60;
  char *local_58;
  char *local_50;
  char *local_48;
  char *local_40;
  char *local_38;
  char *local_30;
  undefined8 local_28;
  undefined8 local_20;

  ppuVar4 = &local_88;
  if (uParm1 != 0)
    {
      func_0x00401950 (DAT_006127a0,
		       "Try \'%s --help\' for more information.\n",
		       DAT_00612838);
      goto LAB_004020a5;
    }
  func_0x00401770 ("Usage: %s [OPTION]... FILE...\n", DAT_00612838);
  uVar3 = DAT_00612780;
  func_0x00401960
    ("Print the resolved absolute file name;\nall but the last component must exist\n\n",
     DAT_00612780);
  func_0x00401960
    ("  -e, --canonicalize-existing  all components of the path must exist\n  -m, --canonicalize-missing   no path components need exist or be a directory\n  -L, --logical                resolve \'..\' components before symlinks\n  -P, --physical               resolve symlinks as encountered (default)\n  -q, --quiet                  suppress most error messages\n      --relative-to=DIR        print the resolved path relative to DIR\n      --relative-base=DIR      print absolute paths unless paths below DIR\n  -s, --strip, --no-symlinks   don\'t expand symlinks\n  -z, --zero                   end each output line with NUL, not newline\n\n",
     uVar3);
  func_0x00401960 ("      --help     display this help and exit\n", uVar3);
  func_0x00401960 ("      --version  output version information and exit\n",
		   uVar3);
  local_88 = &DAT_0040ef58;
  local_80 = "test invocation";
  local_78 = 0x40efbb;
  local_70 = "Multi-call invocation";
  local_68 = "sha224sum";
  local_60 = "sha2 utilities";
  local_58 = "sha256sum";
  local_50 = "sha2 utilities";
  local_48 = "sha384sum";
  local_40 = "sha2 utilities";
  local_38 = "sha512sum";
  local_30 = "sha2 utilities";
  local_28 = 0;
  local_20 = 0;
  puVar6 = &DAT_0040ef58;
  do
    {
      iVar1 = func_0x00401a40 ("realpath", puVar6);
      if (iVar1 == 0)
	break;
      ppuVar4 = ppuVar4 + 2;
      puVar6 = *ppuVar4;
    }
  while (puVar6 != (undefined *) 0x0);
  pcVar5 = ppuVar4[1];
  if (pcVar5 == (char *) 0x0)
    {
      func_0x00401770 ("\n%s online help: <%s>\n", "GNU coreutils",
		       "https://www.gnu.org/software/coreutils/");
      lVar2 = func_0x00401aa0 (5, 0);
      if (lVar2 == 0)
	goto LAB_004020ac;
      iVar1 = func_0x004019c0 (lVar2, &DAT_0040efdc, 3);
      if (iVar1 == 0)
	{
	  func_0x00401770 ("Full documentation at: <%s%s>\n",
			   "https://www.gnu.org/software/coreutils/",
			   "realpath");
	  pcVar5 = "realpath";
	  uVar3 = 0x40ef74;
	  goto LAB_00402093;
	}
      pcVar5 = "realpath";
    LAB_00402051:
      ;
      func_0x00401770
	("Report %s translation bugs to <https://translationproject.org/team/>\n",
	 "realpath");
    }
  else
    {
      func_0x00401770 ("\n%s online help: <%s>\n", "GNU coreutils",
		       "https://www.gnu.org/software/coreutils/");
      lVar2 = func_0x00401aa0 (5, 0);
      if ((lVar2 != 0)
	  && (iVar1 = func_0x004019c0 (lVar2, &DAT_0040efdc, 3), iVar1 != 0))
	goto LAB_00402051;
    }
  func_0x00401770 ("Full documentation at: <%s%s>\n",
		   "https://www.gnu.org/software/coreutils/", "realpath");
  uVar3 = 0x4103cf;
  if (pcVar5 == "realpath")
    {
      uVar3 = 0x40ef74;
    }
LAB_00402093:
  ;
  do
    {
      func_0x00401770
	("or available locally via: info \'(coreutils) %s%s\'\n", pcVar5,
	 uVar3);
    LAB_004020a5:
      ;
      func_0x00401b00 ((ulong) uParm1);
    LAB_004020ac:
      ;
      func_0x00401770 ("Full documentation at: <%s%s>\n",
		       "https://www.gnu.org/software/coreutils/", "realpath");
      pcVar5 = "realpath";
      uVar3 = 0x40ef74;
    }
  while (true);
}

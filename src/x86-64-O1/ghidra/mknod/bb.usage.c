
#include "mknod.h"

long null_ARRAY_0061144_0_8_;
long null_ARRAY_0061144_8_8_;
long null_ARRAY_0061164_0_8_;
long null_ARRAY_0061164_16_8_;
long null_ARRAY_0061164_24_8_;
long null_ARRAY_0061164_32_8_;
long null_ARRAY_0061164_40_8_;
long null_ARRAY_0061164_48_8_;
long null_ARRAY_0061164_8_8_;
long null_ARRAY_0061168_0_4_;
long null_ARRAY_0061168_16_8_;
long null_ARRAY_0061168_4_4_;
long null_ARRAY_0061168_8_4_;
long local_68_2_2_;
long local_6b_1_3_;
long local_6c_4_4_;
long local_6e_4_4_;
long local_6f_4_4_;
long local_70_0_2_;
long local_70_4_4_;
long local_71_0_1_;
long local_71_4_4_;
long local_72_4_4_;
long DAT_00000010;
long DAT_0040d886;
long DAT_0040d90a;
long DAT_0040d91e;
long DAT_0040e280;
long DAT_0040e284;
long DAT_0040e288;
long DAT_0040e28b;
long DAT_0040e28d;
long DAT_0040e291;
long DAT_0040e295;
long DAT_0040ea2b;
long DAT_0040f148;
long DAT_0040f251;
long DAT_0040f257;
long DAT_0040f269;
long DAT_0040f26a;
long DAT_0040f288;
long DAT_0040f28c;
long DAT_0040f316;
long DAT_00611000;
long DAT_00611010;
long DAT_00611020;
long DAT_006113e8;
long DAT_00611450;
long DAT_00611454;
long DAT_00611458;
long DAT_0061145c;
long DAT_00611480;
long DAT_00611490;
long DAT_006114a0;
long DAT_006114a8;
long DAT_006114c0;
long DAT_006114c8;
long DAT_00611510;
long DAT_00611518;
long DAT_00611520;
long DAT_006116b8;
long DAT_006116c0;
long DAT_006116c8;
long DAT_006116d8;
long fde_0040fe98;
long null_ARRAY_0040e080;
long null_ARRAY_0040f720;
long null_ARRAY_0040f960;
long null_ARRAY_00611440;
long null_ARRAY_006114e0;
long null_ARRAY_00611540;
long null_ARRAY_00611640;
long null_ARRAY_00611680;
long PTR_DAT_006113e0;
long PTR_null_ARRAY_00611438;
void
FUN_00401bae (uint uParm1)
{
  int iVar1;
  long lVar2;
  undefined8 uVar3;
  undefined **ppuVar4;
  char *pcVar5;
  undefined *puVar6;
  undefined *local_88;
  char *local_80;
  undefined8 local_78;
  char *local_70;
  char *local_68;
  char *local_60;
  char *local_58;
  char *local_50;
  char *local_48;
  char *local_40;
  char *local_38;
  char *local_30;
  undefined8 local_28;
  undefined8 local_20;

  ppuVar4 = &local_88;
  if (uParm1 != 0)
    {
      func_0x00401750 (DAT_006114a0,
		       "Try \'%s --help\' for more information.\n",
		       DAT_00611520);
      goto LAB_00401dc1;
    }
  func_0x004015c0 ("Usage: %s [OPTION]... NAME TYPE [MAJOR MINOR]\n",
		   DAT_00611520);
  uVar3 = DAT_00611480;
  func_0x00401760 ("Create the special file NAME of the given TYPE.\n",
		   DAT_00611480);
  func_0x00401760
    ("\nMandatory arguments to long options are mandatory for short options too.\n",
     uVar3);
  func_0x00401760
    ("  -m, --mode=MODE    set file permission bits to MODE, not a=rw - umask\n",
     uVar3);
  func_0x00401760
    ("  -Z                   set the SELinux security context to default type\n      --context[=CTX]  like -Z, or if CTX is specified then set the SELinux\n                         or SMACK security context to CTX\n",
     uVar3);
  func_0x00401760 ("      --help     display this help and exit\n", uVar3);
  func_0x00401760 ("      --version  output version information and exit\n",
		   uVar3);
  func_0x00401760
    ("\nBoth MAJOR and MINOR must be specified when TYPE is b, c, or u, and they\nmust be omitted when TYPE is p.  If MAJOR or MINOR begins with 0x or 0X,\nit is interpreted as hexadecimal; otherwise, if it begins with 0, as octal;\notherwise, as decimal.  TYPE may be:\n",
     uVar3);
  func_0x00401760
    ("\n  b      create a block (buffered) special file\n  c, u   create a character (unbuffered) special file\n  p      create a FIFO\n",
     uVar3);
  func_0x004015c0
    ("\nNOTE: your shell may have its own version of %s, which usually supersedes\nthe version described here.  Please refer to your shell\'s documentation\nfor details about the options it supports.\n",
     "mknod");
  local_88 = &DAT_0040d886;
  local_80 = "test invocation";
  local_78 = 0x40d8e9;
  local_70 = "Multi-call invocation";
  local_68 = "sha224sum";
  local_60 = "sha2 utilities";
  local_58 = "sha256sum";
  local_50 = "sha2 utilities";
  local_48 = "sha384sum";
  local_40 = "sha2 utilities";
  local_38 = "sha512sum";
  local_30 = "sha2 utilities";
  local_28 = 0;
  local_20 = 0;
  puVar6 = &DAT_0040d886;
  do
    {
      iVar1 = func_0x00401870 ("mknod", puVar6);
      if (iVar1 == 0)
	break;
      ppuVar4 = ppuVar4 + 2;
      puVar6 = *ppuVar4;
    }
  while (puVar6 != (undefined *) 0x0);
  pcVar5 = ppuVar4[1];
  if (pcVar5 == (char *) 0x0)
    {
      func_0x004015c0 ("\n%s online help: <%s>\n", "GNU coreutils",
		       "https://www.gnu.org/software/coreutils/");
      lVar2 = func_0x004018d0 (5, 0);
      if (lVar2 == 0)
	goto LAB_00401dc8;
      iVar1 = func_0x004017d0 (lVar2, &DAT_0040d90a, 3);
      if (iVar1 == 0)
	{
	  func_0x004015c0 ("Full documentation at: <%s%s>\n",
			   "https://www.gnu.org/software/coreutils/",
			   "mknod");
	  pcVar5 = "mknod";
	  uVar3 = 0x40d8a2;
	  goto LAB_00401daf;
	}
      pcVar5 = "mknod";
    LAB_00401d6d:
      ;
      func_0x004015c0
	("Report %s translation bugs to <https://translationproject.org/team/>\n",
	 "mknod");
    }
  else
    {
      func_0x004015c0 ("\n%s online help: <%s>\n", "GNU coreutils",
		       "https://www.gnu.org/software/coreutils/");
      lVar2 = func_0x004018d0 (5, 0);
      if ((lVar2 != 0)
	  && (iVar1 = func_0x004017d0 (lVar2, &DAT_0040d90a, 3), iVar1 != 0))
	goto LAB_00401d6d;
    }
  func_0x004015c0 ("Full documentation at: <%s%s>\n",
		   "https://www.gnu.org/software/coreutils/", "mknod");
  uVar3 = 0x40f287;
  if (pcVar5 == "mknod")
    {
      uVar3 = 0x40d8a2;
    }
LAB_00401daf:
  ;
  do
    {
      func_0x004015c0
	("or available locally via: info \'(coreutils) %s%s\'\n", pcVar5,
	 uVar3);
    LAB_00401dc1:
      ;
      func_0x00401920 ((ulong) uParm1);
    LAB_00401dc8:
      ;
      func_0x004015c0 ("Full documentation at: <%s%s>\n",
		       "https://www.gnu.org/software/coreutils/", "mknod");
      pcVar5 = "mknod";
      uVar3 = 0x40d8a2;
    }
  while (true);
}

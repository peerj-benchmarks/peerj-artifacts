
#include "[.h"

long null_ARRAY_006115e_0_8_;
long null_ARRAY_006115e_8_8_;
long null_ARRAY_006117c_0_8_;
long null_ARRAY_006117c_16_8_;
long null_ARRAY_006117c_24_8_;
long null_ARRAY_006117c_32_8_;
long null_ARRAY_006117c_40_8_;
long null_ARRAY_006117c_48_8_;
long null_ARRAY_006117c_8_8_;
long local_68_2_2_;
long local_6b_1_3_;
long local_6c_4_4_;
long local_6e_4_4_;
long local_6f_4_4_;
long local_70_0_2_;
long local_70_4_4_;
long local_71_0_1_;
long local_71_4_4_;
long local_72_4_4_;
long DAT_00000006;
long DAT_00000010;
long DAT_0040dd00;
long DAT_0040dd01;
long DAT_0040dd03;
long DAT_0040dd06;
long DAT_0040dd0a;
long DAT_0040dd0e;
long DAT_0040dd12;
long DAT_0040dd16;
long DAT_0040dd1a;
long DAT_0040dd1e;
long DAT_0040dd22;
long DAT_0040dd26;
long DAT_0040ddb8;
long DAT_0040ddd6;
long DAT_0040ddfa;
long DAT_0040ddfd;
long DAT_0040dead;
long DAT_0040dee4;
long DAT_0040ecb8;
long DAT_0040ecbc;
long DAT_0040ecbe;
long DAT_0040ecc2;
long DAT_0040ecc5;
long DAT_0040ecc7;
long DAT_0040eccb;
long DAT_0040eccf;
long DAT_0040f48b;
long DAT_0040f48d;
long DAT_0040f835;
long DAT_0040f842;
long DAT_0040f846;
long DAT_0040f8ce;
long DAT_006111a0;
long DAT_006111b0;
long DAT_006111c0;
long DAT_00611588;
long DAT_006115f0;
long DAT_00611600;
long DAT_00611610;
long DAT_00611620;
long DAT_00611628;
long DAT_00611640;
long DAT_00611648;
long DAT_00611690;
long DAT_00611698;
long DAT_0061169c;
long DAT_006116a0;
long DAT_006116a8;
long DAT_006116b0;
long DAT_006117f8;
long DAT_00611800;
long DAT_00611808;
long DAT_00611810;
long DAT_00611818;
long DAT_00611820;
long fde_004104a0;
long null_ARRAY_0040fce0;
long null_ARRAY_0040ff20;
long null_ARRAY_006115e0;
long null_ARRAY_00611660;
long null_ARRAY_006116c0;
long null_ARRAY_006117c0;
long PTR_DAT_00611580;
long PTR_null_ARRAY_006115d8;
void
FUN_004047fb (undefined8 uParm1, long lParm2, undefined8 uParm3,
	      undefined8 uParm4, undefined8 * puParm5, undefined8 uParm6)
{
  undefined8 uVar1;

  if (lParm2 == 0)
    {
      func_0x004016f0 (uParm1, "%s %s\n");
    }
  else
    {
      func_0x004016f0 (uParm1, "%s (%s) %s\n", lParm2, uParm3, uParm4);
    }
  func_0x004016f0 (uParm1, "Copyright %s %d Free Software Foundation, Inc.",
		   &DAT_0040f48b, 0x7e1);
  uVar1 = 0x40485f;
  func_0x00401700
    ("\nLicense GPLv3+: GNU GPL version 3 or later <https://gnu.org/licenses/gpl.html>.\nThis is free software: you are free to change and redistribute it.\nThere is NO WARRANTY, to the extent permitted by law.\n\n",
     uParm1);
  switch (uParm6)
    {
    case 0:
      func_0x00401680 ();
    case 1:
      func_0x004016f0 (uParm1, "Written by %s.\n", *puParm5);
      break;
    case 2:
      func_0x004016f0 (uParm1, "Written by %s and %s.\n", *puParm5,
		       puParm5[1]);
      break;
    case 3:
      func_0x004016f0 (uParm1, "Written by %s, %s, and %s.\n", *puParm5,
		       puParm5[1], puParm5[2]);
      break;
    case 4:
      func_0x004016f0 (uParm1, "Written by %s, %s, %s,\nand %s.\n", *puParm5,
		       puParm5[1], puParm5[2], puParm5[3]);
      break;
    case 5:
      func_0x004016f0 (uParm1, "Written by %s, %s, %s,\n%s, and %s.\n",
		       *puParm5, puParm5[1], puParm5[2], puParm5[3],
		       puParm5[4]);
      break;
    case 6:
      func_0x004016f0 (uParm1, "Written by %s, %s, %s,\n%s, %s, and %s.\n",
		       *puParm5, puParm5[1], puParm5[2], puParm5[3],
		       puParm5[4], puParm5[5]);
      break;
    case 7:
      func_0x004016f0 (uParm1,
		       "Written by %s, %s, %s,\n%s, %s, %s, and %s.\n",
		       *puParm5, puParm5[1], puParm5[2], puParm5[3],
		       puParm5[4], puParm5[5], puParm5[6]);
      break;
    case 8:
      func_0x004016f0 (uParm1,
		       "Written by %s, %s, %s,\n%s, %s, %s, %s,\nand %s.\n",
		       *puParm5, puParm5[1], puParm5[2], puParm5[3],
		       puParm5[4], puParm5[5], puParm5[6], puParm5[7]);
      break;
    case 9:
      ;
      func_0x004016f0 (uParm1,
		       "Written by %s, %s, %s,\n%s, %s, %s, %s,\n%s, and %s.\n",
		       *puParm5, puParm5[1], puParm5[2], puParm5[3],
		       puParm5[4], puParm5[5], puParm5[6], puParm5[7],
		       puParm5[8], uVar1);
      break;
    default:
      func_0x004016f0 (uParm1,
		       "Written by %s, %s, %s,\n%s, %s, %s, %s,\n%s, %s, and others.\n",
		       *puParm5, puParm5[1], puParm5[2], puParm5[3],
		       puParm5[4], puParm5[5], puParm5[6], puParm5[7],
		       puParm5[8], uVar1);
    }
  return;
}

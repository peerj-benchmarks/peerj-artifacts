
#include "[.h"

long null_ARRAY_006115e_0_8_;
long null_ARRAY_006115e_8_8_;
long null_ARRAY_006117c_0_8_;
long null_ARRAY_006117c_16_8_;
long null_ARRAY_006117c_24_8_;
long null_ARRAY_006117c_32_8_;
long null_ARRAY_006117c_40_8_;
long null_ARRAY_006117c_48_8_;
long null_ARRAY_006117c_8_8_;
long local_68_2_2_;
long local_6b_1_3_;
long local_6c_4_4_;
long local_6e_4_4_;
long local_6f_4_4_;
long local_70_0_2_;
long local_70_4_4_;
long local_71_0_1_;
long local_71_4_4_;
long local_72_4_4_;
long DAT_00000006;
long DAT_00000010;
long DAT_0040dd00;
long DAT_0040dd01;
long DAT_0040dd03;
long DAT_0040dd06;
long DAT_0040dd0a;
long DAT_0040dd0e;
long DAT_0040dd12;
long DAT_0040dd16;
long DAT_0040dd1a;
long DAT_0040dd1e;
long DAT_0040dd22;
long DAT_0040dd26;
long DAT_0040ddb8;
long DAT_0040ddd6;
long DAT_0040ddfa;
long DAT_0040ddfd;
long DAT_0040dead;
long DAT_0040dee4;
long DAT_0040ecb8;
long DAT_0040ecbc;
long DAT_0040ecbe;
long DAT_0040ecc2;
long DAT_0040ecc5;
long DAT_0040ecc7;
long DAT_0040eccb;
long DAT_0040eccf;
long DAT_0040f48b;
long DAT_0040f48d;
long DAT_0040f835;
long DAT_0040f842;
long DAT_0040f846;
long DAT_0040f8ce;
long DAT_006111a0;
long DAT_006111b0;
long DAT_006111c0;
long DAT_00611588;
long DAT_006115f0;
long DAT_00611600;
long DAT_00611610;
long DAT_00611620;
long DAT_00611628;
long DAT_00611640;
long DAT_00611648;
long DAT_00611690;
long DAT_00611698;
long DAT_0061169c;
long DAT_006116a0;
long DAT_006116a8;
long DAT_006116b0;
long DAT_006117f8;
long DAT_00611800;
long DAT_00611808;
long DAT_00611810;
long DAT_00611818;
long DAT_00611820;
long fde_004104a0;
long null_ARRAY_0040fce0;
long null_ARRAY_0040ff20;
long null_ARRAY_006115e0;
long null_ARRAY_00611660;
long null_ARRAY_006116c0;
long null_ARRAY_006117c0;
long PTR_DAT_00611580;
long PTR_null_ARRAY_006115d8;
void
FUN_00402dbc (uint uParm1)
{
  int iVar1;
  long lVar2;
  undefined8 uVar3;
  long *plVar4;
  long lVar5;
  long local_88;
  char *local_80;
  undefined8 local_78;
  char *local_70;
  char *local_68;
  char *local_60;
  char *local_58;
  char *local_50;
  char *local_48;
  char *local_40;
  char *local_38;
  char *local_30;
  undefined8 local_28;
  undefined8 local_20;

  uVar3 = DAT_00611600;
  plVar4 = &local_88;
  if (uParm1 != 0)
    {
      func_0x004016f0 (DAT_00611620,
		       "Try \'%s --help\' for more information.\n",
		       DAT_006116b0);
      goto LAB_00403021;
    }
  func_0x00401700
    ("Usage: test EXPRESSION\n  or:  test\n  or:  [ EXPRESSION ]\n  or:  [ ]\n  or:  [ OPTION\n",
     DAT_00611600);
  func_0x00401700 ("Exit with the status determined by EXPRESSION.\n\n",
		   uVar3);
  func_0x00401700 ("      --help     display this help and exit\n", uVar3);
  func_0x00401700 ("      --version  output version information and exit\n",
		   uVar3);
  func_0x00401700
    ("\nAn omitted EXPRESSION defaults to false.  Otherwise,\nEXPRESSION is true or false and sets exit status.  It is one of:\n",
     uVar3);
  func_0x00401700
    ("\n  ( EXPRESSION )               EXPRESSION is true\n  ! EXPRESSION                 EXPRESSION is false\n  EXPRESSION1 -a EXPRESSION2   both EXPRESSION1 and EXPRESSION2 are true\n  EXPRESSION1 -o EXPRESSION2   either EXPRESSION1 or EXPRESSION2 is true\n",
     uVar3);
  func_0x00401700
    ("\n  -n STRING            the length of STRING is nonzero\n  STRING               equivalent to -n STRING\n  -z STRING            the length of STRING is zero\n  STRING1 = STRING2    the strings are equal\n  STRING1 != STRING2   the strings are not equal\n",
     uVar3);
  func_0x00401700
    ("\n  INTEGER1 -eq INTEGER2   INTEGER1 is equal to INTEGER2\n  INTEGER1 -ge INTEGER2   INTEGER1 is greater than or equal to INTEGER2\n  INTEGER1 -gt INTEGER2   INTEGER1 is greater than INTEGER2\n  INTEGER1 -le INTEGER2   INTEGER1 is less than or equal to INTEGER2\n  INTEGER1 -lt INTEGER2   INTEGER1 is less than INTEGER2\n  INTEGER1 -ne INTEGER2   INTEGER1 is not equal to INTEGER2\n",
     uVar3);
  func_0x00401700
    ("\n  FILE1 -ef FILE2   FILE1 and FILE2 have the same device and inode numbers\n  FILE1 -nt FILE2   FILE1 is newer (modification date) than FILE2\n  FILE1 -ot FILE2   FILE1 is older than FILE2\n",
     uVar3);
  func_0x00401700
    ("\n  -b FILE     FILE exists and is block special\n  -c FILE     FILE exists and is character special\n  -d FILE     FILE exists and is a directory\n  -e FILE     FILE exists\n",
     uVar3);
  func_0x00401700
    ("  -f FILE     FILE exists and is a regular file\n  -g FILE     FILE exists and is set-group-ID\n  -G FILE     FILE exists and is owned by the effective group ID\n  -h FILE     FILE exists and is a symbolic link (same as -L)\n  -k FILE     FILE exists and has its sticky bit set\n",
     uVar3);
  func_0x00401700
    ("  -L FILE     FILE exists and is a symbolic link (same as -h)\n  -O FILE     FILE exists and is owned by the effective user ID\n  -p FILE     FILE exists and is a named pipe\n  -r FILE     FILE exists and read permission is granted\n  -s FILE     FILE exists and has a size greater than zero\n",
     uVar3);
  func_0x00401700
    ("  -S FILE     FILE exists and is a socket\n  -t FD       file descriptor FD is opened on a terminal\n  -u FILE     FILE exists and its set-user-ID bit is set\n  -w FILE     FILE exists and write permission is granted\n  -x FILE     FILE exists and execute (or search) permission is granted\n",
     uVar3);
  func_0x00401700
    ("\nExcept for -h and -L, all FILE-related tests dereference symbolic links.\nBeware that parentheses need to be escaped (e.g., by backslashes) for shells.\nINTEGER may also be -l STRING, which evaluates to the length of STRING.\n",
     uVar3);
  func_0x00401700
    ("\nNOTE: Binary -a and -o are inherently ambiguous.  Use \'test EXPR1 && test\nEXPR2\' or \'test EXPR1 || test EXPR2\' instead.\n",
     uVar3);
  func_0x00401700
    ("\nNOTE: [ honors the --help and --version options, but test does not.\ntest treats each of those as it treats any other nonempty STRING.\n",
     uVar3);
  func_0x00401530
    ("\nNOTE: your shell may have its own version of %s, which usually supersedes\nthe version described here.  Please refer to your shell\'s documentation\nfor details about the options it supports.\n",
     "test and/or [");
  local_88 = 0x40de29;
  local_80 = "test invocation";
  local_78 = 0x40de8c;
  local_70 = "Multi-call invocation";
  local_68 = "sha224sum";
  local_60 = "sha2 utilities";
  local_58 = "sha256sum";
  local_50 = "sha2 utilities";
  local_48 = "sha384sum";
  local_40 = "sha2 utilities";
  local_38 = "sha512sum";
  local_30 = "sha2 utilities";
  local_28 = 0;
  local_20 = 0;
  lVar5 = 0x40de29;
  do
    {
      iVar1 = func_0x004017d0 (0x40de29, lVar5);
      if (iVar1 == 0)
	break;
      plVar4 = plVar4 + 2;
      lVar5 = *plVar4;
    }
  while (lVar5 != 0);
  lVar5 = plVar4[1];
  if (lVar5 == 0)
    {
      func_0x00401530 ("\n%s online help: <%s>\n", "GNU coreutils",
		       "https://www.gnu.org/software/coreutils/");
      lVar5 = func_0x00401830 (5, 0);
      if (lVar5 == 0)
	goto LAB_00403028;
      iVar1 = func_0x00401760 (lVar5, &DAT_0040dead, 3);
      if (iVar1 == 0)
	{
	  func_0x00401530 ("Full documentation at: <%s%s>\n",
			   "https://www.gnu.org/software/coreutils/",
			   0x40de29);
	  lVar5 = 0x40de29;
	  uVar3 = 0x40de45;
	  goto LAB_0040300f;
	}
      lVar5 = 0x40de29;
    LAB_00402fcd:
      ;
      func_0x00401530
	("Report %s translation bugs to <https://translationproject.org/team/>\n",
	 0x40de29);
    }
  else
    {
      func_0x00401530 ("\n%s online help: <%s>\n", "GNU coreutils",
		       "https://www.gnu.org/software/coreutils/");
      lVar2 = func_0x00401830 (5, 0);
      if ((lVar2 != 0)
	  && (iVar1 = func_0x00401760 (lVar2, &DAT_0040dead, 3), iVar1 != 0))
	goto LAB_00402fcd;
    }
  func_0x00401530 ("Full documentation at: <%s%s>\n",
		   "https://www.gnu.org/software/coreutils/", 0x40de29);
  uVar3 = 0x40f4d1;
  if (lVar5 == 0x40de29)
    {
      uVar3 = 0x40de45;
    }
LAB_0040300f:
  ;
  do
    {
      func_0x00401530
	("or available locally via: info \'(coreutils) %s%s\'\n", lVar5,
	 uVar3);
    LAB_00403021:
      ;
      func_0x00401880 ((ulong) uParm1);
    LAB_00403028:
      ;
      func_0x00401530 ("Full documentation at: <%s%s>\n",
		       "https://www.gnu.org/software/coreutils/", 0x40de29);
      lVar5 = 0x40de29;
      uVar3 = 0x40de45;
    }
  while (true);
}

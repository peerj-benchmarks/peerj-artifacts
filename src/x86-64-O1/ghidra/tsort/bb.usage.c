
#include "tsort.h"

long null_ARRAY_006104a_0_8_;
long null_ARRAY_006104a_8_8_;
long null_ARRAY_006106c_0_8_;
long null_ARRAY_006106c_16_8_;
long null_ARRAY_006106c_24_8_;
long null_ARRAY_006106c_32_8_;
long null_ARRAY_006106c_40_8_;
long null_ARRAY_006106c_48_8_;
long null_ARRAY_006106c_8_8_;
long null_ARRAY_0061070_0_4_;
long null_ARRAY_0061070_16_8_;
long null_ARRAY_0061070_4_4_;
long null_ARRAY_0061070_8_4_;
long local_68_2_2_;
long local_6b_1_3_;
long local_6c_4_4_;
long local_6e_4_4_;
long local_6f_4_4_;
long local_70_0_2_;
long local_70_4_4_;
long local_71_0_1_;
long local_71_4_4_;
long local_72_4_4_;
long DAT_00000010;
long DAT_0040d5c6;
long DAT_0040d64a;
long DAT_0040d67c;
long DAT_0040d67e;
long DAT_0040d67f;
long DAT_0040d695;
long DAT_0040d982;
long DAT_0040da60;
long DAT_0040da64;
long DAT_0040da68;
long DAT_0040da6b;
long DAT_0040da6d;
long DAT_0040da71;
long DAT_0040da75;
long DAT_0040e22b;
long DAT_0040e5d5;
long DAT_0040e6d9;
long DAT_0040e6df;
long DAT_0040e6f1;
long DAT_0040e6f2;
long DAT_0040e710;
long DAT_0040e714;
long DAT_0040e79e;
long DAT_00610060;
long DAT_00610070;
long DAT_00610080;
long DAT_00610448;
long DAT_006104b0;
long DAT_006104b4;
long DAT_006104b8;
long DAT_006104bc;
long DAT_006104c0;
long DAT_006104c8;
long DAT_006104d0;
long DAT_006104e0;
long DAT_006104e8;
long DAT_00610500;
long DAT_00610508;
long DAT_00610550;
long DAT_00610558;
long DAT_00610560;
long DAT_00610568;
long DAT_00610570;
long DAT_00610578;
long DAT_00610580;
long DAT_00610738;
long DAT_00610740;
long DAT_00610748;
long DAT_00610758;
long fde_0040f350;
long null_ARRAY_0040d940;
long null_ARRAY_0040d9c0;
long null_ARRAY_0040ebc0;
long null_ARRAY_0040ee00;
long null_ARRAY_006104a0;
long null_ARRAY_00610520;
long null_ARRAY_006105c0;
long null_ARRAY_006106c0;
long null_ARRAY_00610700;
long PTR_DAT_00610440;
long PTR_null_ARRAY_00610498;
void
FUN_00401be1 (uint uParm1)
{
  undefined8 uVar1;
  int iVar2;
  long lVar3;
  undefined1 *puVar4;
  undefined **ppuVar5;
  char *pcVar6;
  undefined *puVar7;
  undefined *local_88;
  char *local_80;
  undefined8 local_78;
  char *local_70;
  char *local_68;
  char *local_60;
  char *local_58;
  char *local_50;
  char *local_48;
  char *local_40;
  char *local_38;
  char *local_30;
  undefined8 local_28;
  undefined8 local_20;

  ppuVar5 = &local_88;
  if (uParm1 != 0)
    {
      func_0x00401710 (DAT_006104e0,
		       "Try \'%s --help\' for more information.\n",
		       DAT_00610580);
      goto LAB_00401dac;
    }
  func_0x00401580
    ("Usage: %s [OPTION] [FILE]\nWrite totally ordered list consistent with the partial ordering in FILE.\n",
     DAT_00610580);
  uVar1 = DAT_006104c0;
  func_0x00401720
    ("\nWith no FILE, or when FILE is -, read standard input.\n",
     DAT_006104c0);
  func_0x00401720 (&DAT_0040d67e, uVar1);
  func_0x00401720 ("      --help     display this help and exit\n", uVar1);
  func_0x00401720 ("      --version  output version information and exit\n",
		   uVar1);
  local_88 = &DAT_0040d5c6;
  local_80 = "test invocation";
  local_78 = 0x40d629;
  local_70 = "Multi-call invocation";
  local_68 = "sha224sum";
  local_60 = "sha2 utilities";
  local_58 = "sha256sum";
  local_50 = "sha2 utilities";
  local_48 = "sha384sum";
  local_40 = "sha2 utilities";
  local_38 = "sha512sum";
  local_30 = "sha2 utilities";
  local_28 = 0;
  local_20 = 0;
  puVar7 = &DAT_0040d5c6;
  do
    {
      iVar2 = func_0x00401810 ("tsort", puVar7);
      if (iVar2 == 0)
	break;
      ppuVar5 = ppuVar5 + 2;
      puVar7 = *ppuVar5;
    }
  while (puVar7 != (undefined *) 0x0);
  pcVar6 = ppuVar5[1];
  if (pcVar6 == (char *) 0x0)
    {
      func_0x00401580 ("\n%s online help: <%s>\n", "GNU coreutils",
		       "https://www.gnu.org/software/coreutils/");
      lVar3 = func_0x00401870 (5, 0);
      if (lVar3 == 0)
	goto LAB_00401db3;
      iVar2 = func_0x00401780 (lVar3, &DAT_0040d64a, 3);
      if (iVar2 == 0)
	{
	  func_0x00401580 ("Full documentation at: <%s%s>\n",
			   "https://www.gnu.org/software/coreutils/",
			   "tsort");
	  pcVar6 = "tsort";
	  puVar4 = (undefined1 *) 0x40d5e2;
	  goto LAB_00401d9a;
	}
      pcVar6 = "tsort";
    LAB_00401d58:
      ;
      func_0x00401580
	("Report %s translation bugs to <https://translationproject.org/team/>\n",
	 "tsort");
    }
  else
    {
      func_0x00401580 ("\n%s online help: <%s>\n", "GNU coreutils",
		       "https://www.gnu.org/software/coreutils/");
      lVar3 = func_0x00401870 (5, 0);
      if ((lVar3 != 0)
	  && (iVar2 = func_0x00401780 (lVar3, &DAT_0040d64a, 3), iVar2 != 0))
	goto LAB_00401d58;
    }
  func_0x00401580 ("Full documentation at: <%s%s>\n",
		   "https://www.gnu.org/software/coreutils/", "tsort");
  puVar4 = &DAT_0040d67f;
  if (pcVar6 == "tsort")
    {
      puVar4 = (undefined1 *) 0x40d5e2;
    }
LAB_00401d9a:
  ;
  do
    {
      func_0x00401580
	("or available locally via: info \'(coreutils) %s%s\'\n", pcVar6,
	 puVar4);
    LAB_00401dac:
      ;
      func_0x004018c0 ((ulong) uParm1);
    LAB_00401db3:
      ;
      func_0x00401580 ("Full documentation at: <%s%s>\n",
		       "https://www.gnu.org/software/coreutils/", "tsort");
      pcVar6 = "tsort";
      puVar4 = (undefined1 *) 0x40d5e2;
    }
  while (true);
}

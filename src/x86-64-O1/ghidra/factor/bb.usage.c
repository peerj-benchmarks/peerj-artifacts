
#include "factor.h"

long null_ARRAY_0061946_0_8_;
long null_ARRAY_0061946_8_8_;
long null_ARRAY_0061951_0_8_;
long null_ARRAY_0061951_8_8_;
long null_ARRAY_0061964_0_8_;
long null_ARRAY_0061964_16_8_;
long null_ARRAY_0061964_24_8_;
long null_ARRAY_0061964_32_8_;
long null_ARRAY_0061964_40_8_;
long null_ARRAY_0061964_48_8_;
long null_ARRAY_0061964_8_8_;
long null_ARRAY_0061968_0_4_;
long null_ARRAY_0061968_16_8_;
long null_ARRAY_0061968_4_4_;
long null_ARRAY_0061968_8_4_;
long local_68_2_2_;
long local_6b_1_3_;
long local_6c_4_4_;
long local_6e_4_4_;
long local_6f_4_4_;
long local_70_0_2_;
long local_70_4_4_;
long local_71_0_1_;
long local_71_4_4_;
long local_72_4_4_;
long DAT_00000010;
long DAT_004122c5;
long DAT_00412349;
long DAT_0041234d;
long DAT_00412368;
long DAT_00412398;
long DAT_00415850;
long DAT_004158c3;
long DAT_004158c7;
long DAT_004158ca;
long DAT_004158cc;
long DAT_004158d0;
long DAT_004158d4;
long DAT_00416095;
long DAT_00416435;
long DAT_00416551;
long DAT_00416552;
long DAT_00416570;
long DAT_00416574;
long DAT_00416639;
long DAT_00619000;
long DAT_00619010;
long DAT_00619020;
long DAT_00619400;
long DAT_00619410;
long DAT_00619470;
long DAT_00619474;
long DAT_00619478;
long DAT_0061947c;
long DAT_00619480;
long DAT_00619488;
long DAT_00619490;
long DAT_006194a0;
long DAT_006194a8;
long DAT_006194c0;
long DAT_006194c8;
long DAT_00619520;
long DAT_00619528;
long DAT_00619530;
long DAT_00619538;
long DAT_006196b8;
long DAT_00619ec8;
long DAT_00619ed0;
long DAT_00619ee0;
long fde_004172e8;
long null_ARRAY_004126d5;
long null_ARRAY_00412714;
long null_ARRAY_00412740;
long null_ARRAY_004127c0;
long null_ARRAY_00415200;
long null_ARRAY_004154c0;
long null_ARRAY_00415780;
long null_ARRAY_00416600;
long null_ARRAY_00416a60;
long null_ARRAY_00416cb0;
long null_ARRAY_00619460;
long null_ARRAY_006194e0;
long null_ARRAY_00619510;
long null_ARRAY_00619540;
long null_ARRAY_00619640;
long null_ARRAY_00619680;
long null_ARRAY_006196c0;
long PTR_DAT_00619408;
long PTR_null_ARRAY_00619458;
long stack0x00000008;
long stack0xffffffffffffffd8;
long stack0xfffffffffffffff0;
void
FUN_00403ec0 (uint uParm1)
{
  int iVar1;
  long lVar2;
  undefined8 uVar3;
  undefined **ppuVar4;
  char *pcVar5;
  undefined *puVar6;
  undefined *local_88;
  char *local_80;
  undefined8 local_78;
  char *local_70;
  char *local_68;
  char *local_60;
  char *local_58;
  char *local_50;
  char *local_48;
  char *local_40;
  char *local_38;
  char *local_30;
  undefined8 local_28;
  undefined8 local_20;

  ppuVar4 = &local_88;
  if (uParm1 != 0)
    {
      func_0x00401870 (DAT_006194a0,
		       "Try \'%s --help\' for more information.\n",
		       DAT_00619538);
      goto LAB_00404081;
    }
  func_0x004016b0 ("Usage: %s [NUMBER]...\n  or:  %s OPTION\n", DAT_00619538,
		   DAT_00619538);
  uVar3 = DAT_00619480;
  func_0x00401880
    ("Print the prime factors of each specified integer NUMBER.  If none\nare specified on the command line, read them from standard input.\n\n",
     DAT_00619480);
  func_0x00401880 ("      --help     display this help and exit\n", uVar3);
  func_0x00401880 ("      --version  output version information and exit\n",
		   uVar3);
  local_88 = &DAT_004122c5;
  local_80 = "test invocation";
  local_78 = 0x412328;
  local_70 = "Multi-call invocation";
  local_68 = "sha224sum";
  local_60 = "sha2 utilities";
  local_58 = "sha256sum";
  local_50 = "sha2 utilities";
  local_48 = "sha384sum";
  local_40 = "sha2 utilities";
  local_38 = "sha512sum";
  local_30 = "sha2 utilities";
  local_28 = 0;
  local_20 = 0;
  puVar6 = &DAT_004122c5;
  do
    {
      iVar1 = func_0x00401980 ("factor", puVar6);
      if (iVar1 == 0)
	break;
      ppuVar4 = ppuVar4 + 2;
      puVar6 = *ppuVar4;
    }
  while (puVar6 != (undefined *) 0x0);
  pcVar5 = ppuVar4[1];
  if (pcVar5 == (char *) 0x0)
    {
      func_0x004016b0 ("\n%s online help: <%s>\n", "GNU coreutils",
		       "https://www.gnu.org/software/coreutils/");
      lVar2 = func_0x004019e0 (5, 0);
      if (lVar2 == 0)
	goto LAB_00404088;
      iVar1 = func_0x004018d0 (lVar2, &DAT_00412349, 3);
      if (iVar1 == 0)
	{
	  func_0x004016b0 ("Full documentation at: <%s%s>\n",
			   "https://www.gnu.org/software/coreutils/",
			   "factor");
	  pcVar5 = "factor";
	  uVar3 = 0x4122e1;
	  goto LAB_0040406f;
	}
      pcVar5 = "factor";
    LAB_0040402d:
      ;
      func_0x004016b0
	("Report %s translation bugs to <https://translationproject.org/team/>\n",
	 "factor");
    }
  else
    {
      func_0x004016b0 ("\n%s online help: <%s>\n", "GNU coreutils",
		       "https://www.gnu.org/software/coreutils/");
      lVar2 = func_0x004019e0 (5, 0);
      if ((lVar2 != 0)
	  && (iVar1 = func_0x004018d0 (lVar2, &DAT_00412349, 3), iVar1 != 0))
	goto LAB_0040402d;
    }
  func_0x004016b0 ("Full documentation at: <%s%s>\n",
		   "https://www.gnu.org/software/coreutils/", "factor");
  uVar3 = 0x41656f;
  if (pcVar5 == "factor")
    {
      uVar3 = 0x4122e1;
    }
LAB_0040406f:
  ;
  do
    {
      func_0x004016b0
	("or available locally via: info \'(coreutils) %s%s\'\n", pcVar5,
	 uVar3);
    LAB_00404081:
      ;
      func_0x00401a30 ((ulong) uParm1);
    LAB_00404088:
      ;
      func_0x004016b0 ("Full documentation at: <%s%s>\n",
		       "https://www.gnu.org/software/coreutils/", "factor");
      pcVar5 = "factor";
      uVar3 = 0x4122e1;
    }
  while (true);
}

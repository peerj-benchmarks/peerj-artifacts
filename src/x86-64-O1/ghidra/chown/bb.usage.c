
#include "chown.h"

long null_ARRAY_00617d6_0_8_;
long null_ARRAY_00617d6_8_8_;
long null_ARRAY_00617f4_0_8_;
long null_ARRAY_00617f4_16_8_;
long null_ARRAY_00617f4_24_8_;
long null_ARRAY_00617f4_32_8_;
long null_ARRAY_00617f4_40_8_;
long null_ARRAY_00617f4_48_8_;
long null_ARRAY_00617f4_8_8_;
long null_ARRAY_00617f8_0_4_;
long null_ARRAY_00617f8_16_8_;
long null_ARRAY_00617f8_4_4_;
long null_ARRAY_00617f8_8_4_;
long local_68_2_2_;
long local_6b_1_3_;
long local_6c_4_4_;
long local_6f_4_4_;
long local_70_0_2_;
long local_70_4_4_;
long local_71_0_1_;
long local_71_4_4_;
long local_72_4_4_;
long DAT_00000010;
long DAT_0000001c;
long DAT_00412b06;
long DAT_00412b8a;
long DAT_00413d80;
long DAT_00413d84;
long DAT_00413d88;
long DAT_00413d8b;
long DAT_00413d8d;
long DAT_00413d91;
long DAT_00413d95;
long DAT_00414553;
long DAT_00414c8e;
long DAT_00414c9d;
long DAT_00414c9f;
long DAT_00414ca0;
long DAT_00414e81;
long DAT_00414e82;
long DAT_00414ea0;
long DAT_00414f11;
long DAT_0041505a;
long DAT_00415064;
long DAT_004150a2;
long DAT_00617850;
long DAT_00617860;
long DAT_00617870;
long DAT_00617d08;
long DAT_00617d70;
long DAT_00617d74;
long DAT_00617d78;
long DAT_00617d7c;
long DAT_00617d80;
long DAT_00617d90;
long DAT_00617da0;
long DAT_00617da8;
long DAT_00617dc0;
long DAT_00617dc8;
long DAT_00617e20;
long DAT_00617e28;
long DAT_00617e30;
long DAT_00617e38;
long DAT_00617fb8;
long DAT_00617fbc;
long DAT_00617fc0;
long DAT_00617fc8;
long DAT_00617fd0;
long DAT_00617fd8;
long DAT_00617fe8;
long fde_00415ea8;
long null_ARRAY_00413740;
long null_ARRAY_00414f20;
long null_ARRAY_004154c0;
long null_ARRAY_004156f0;
long null_ARRAY_00617d60;
long null_ARRAY_00617de0;
long null_ARRAY_00617e10;
long null_ARRAY_00617e40;
long null_ARRAY_00617f40;
long null_ARRAY_00617f80;
long PTR_DAT_00617d00;
long PTR_null_ARRAY_00617d58;
long stack0x00000008;
void
FUN_004023be (uint uParm1)
{
  int iVar1;
  long lVar2;
  undefined8 uVar3;
  undefined **ppuVar4;
  char *pcVar5;
  undefined *puVar6;
  undefined *local_88;
  char *local_80;
  undefined8 local_78;
  char *local_70;
  char *local_68;
  char *local_60;
  char *local_58;
  char *local_50;
  char *local_48;
  char *local_40;
  char *local_38;
  char *local_30;
  undefined8 local_28;
  undefined8 local_20;

  ppuVar4 = &local_88;
  if (uParm1 != 0)
    {
      func_0x00401ed0 (DAT_00617da0,
		       "Try \'%s --help\' for more information.\n",
		       DAT_00617e38);
      goto LAB_00402610;
    }
  func_0x00401c50
    ("Usage: %s [OPTION]... [OWNER][:[GROUP]] FILE...\n  or:  %s [OPTION]... --reference=RFILE FILE...\n",
     DAT_00617e38, DAT_00617e38);
  uVar3 = DAT_00617d80;
  func_0x00401ee0
    ("Change the owner and/or group of each FILE to OWNER and/or GROUP.\nWith --reference, change the owner and group of each FILE to those of RFILE.\n\n",
     DAT_00617d80);
  func_0x00401ee0
    ("  -c, --changes          like verbose but report only when a change is made\n  -f, --silent, --quiet  suppress most error messages\n  -v, --verbose          output a diagnostic for every file processed\n",
     uVar3);
  func_0x00401ee0
    ("      --dereference      affect the referent of each symbolic link (this is\n                         the default), rather than the symbolic link itself\n  -h, --no-dereference   affect symbolic links instead of any referenced file\n",
     uVar3);
  func_0x00401ee0
    ("                         (useful only on systems that can change the\n                         ownership of a symlink)\n",
     uVar3);
  func_0x00401ee0
    ("      --from=CURRENT_OWNER:CURRENT_GROUP\n                         change the owner and/or group of each file only if\n                         its current owner and/or group match those specified\n                         here.  Either may be omitted, in which case a match\n                         is not required for the omitted attribute\n",
     uVar3);
  func_0x00401ee0
    ("      --no-preserve-root  do not treat \'/\' specially (the default)\n      --preserve-root    fail to operate recursively on \'/\'\n",
     uVar3);
  func_0x00401ee0
    ("      --reference=RFILE  use RFILE\'s owner and group rather than\n                         specifying OWNER:GROUP values\n",
     uVar3);
  func_0x00401ee0
    ("  -R, --recursive        operate on files and directories recursively\n",
     uVar3);
  func_0x00401ee0
    ("\nThe following options modify how a hierarchy is traversed when the -R\noption is also specified.  If more than one is specified, only the final\none takes effect.\n\n  -H                     if a command line argument is a symbolic link\n                         to a directory, traverse it\n  -L                     traverse every symbolic link to a directory\n                         encountered\n  -P                     do not traverse any symbolic links (default)\n\n",
     uVar3);
  func_0x00401ee0 ("      --help     display this help and exit\n", uVar3);
  func_0x00401ee0 ("      --version  output version information and exit\n",
		   uVar3);
  func_0x00401ee0
    ("\nOwner is unchanged if missing.  Group is unchanged if missing, but changed\nto login group if implied by a \':\' following a symbolic OWNER.\nOWNER and GROUP may be numeric as well as symbolic.\n",
     uVar3);
  func_0x00401c50
    ("\nExamples:\n  %s root /u        Change the owner of /u to \"root\".\n  %s root:staff /u  Likewise, but also change its group to \"staff\".\n  %s -hR root /u    Change the owner of /u and subfiles to \"root\".\n",
     DAT_00617e38, DAT_00617e38, DAT_00617e38);
  local_88 = &DAT_00412b06;
  local_80 = "test invocation";
  local_78 = 0x412b69;
  local_70 = "Multi-call invocation";
  local_68 = "sha224sum";
  local_60 = "sha2 utilities";
  local_58 = "sha256sum";
  local_50 = "sha2 utilities";
  local_48 = "sha384sum";
  local_40 = "sha2 utilities";
  local_38 = "sha512sum";
  local_30 = "sha2 utilities";
  local_28 = 0;
  local_20 = 0;
  puVar6 = &DAT_00412b06;
  do
    {
      iVar1 = func_0x00402010 ("chown", puVar6);
      if (iVar1 == 0)
	break;
      ppuVar4 = ppuVar4 + 2;
      puVar6 = *ppuVar4;
    }
  while (puVar6 != (undefined *) 0x0);
  pcVar5 = ppuVar4[1];
  if (pcVar5 == (char *) 0x0)
    {
      func_0x00401c50 ("\n%s online help: <%s>\n", "GNU coreutils",
		       "https://www.gnu.org/software/coreutils/");
      lVar2 = func_0x00402090 (5, 0);
      if (lVar2 == 0)
	goto LAB_00402617;
      iVar1 = func_0x00401f70 (lVar2, &DAT_00412b8a, 3);
      if (iVar1 == 0)
	{
	  func_0x00401c50 ("Full documentation at: <%s%s>\n",
			   "https://www.gnu.org/software/coreutils/",
			   "chown");
	  pcVar5 = "chown";
	  uVar3 = 0x412b22;
	  goto LAB_004025fe;
	}
      pcVar5 = "chown";
    LAB_004025bc:
      ;
      func_0x00401c50
	("Report %s translation bugs to <https://translationproject.org/team/>\n",
	 "chown");
    }
  else
    {
      func_0x00401c50 ("\n%s online help: <%s>\n", "GNU coreutils",
		       "https://www.gnu.org/software/coreutils/");
      lVar2 = func_0x00402090 (5, 0);
      if ((lVar2 != 0)
	  && (iVar1 = func_0x00401f70 (lVar2, &DAT_00412b8a, 3), iVar1 != 0))
	goto LAB_004025bc;
    }
  func_0x00401c50 ("Full documentation at: <%s%s>\n",
		   "https://www.gnu.org/software/coreutils/", "chown");
  uVar3 = 0x414e9f;
  if (pcVar5 == "chown")
    {
      uVar3 = 0x412b22;
    }
LAB_004025fe:
  ;
  do
    {
      func_0x00401c50
	("or available locally via: info \'(coreutils) %s%s\'\n", pcVar5,
	 uVar3);
    LAB_00402610:
      ;
      func_0x004020f0 ((ulong) uParm1);
    LAB_00402617:
      ;
      func_0x00401c50 ("Full documentation at: <%s%s>\n",
		       "https://www.gnu.org/software/coreutils/", "chown");
      pcVar5 = "chown";
      uVar3 = 0x412b22;
    }
  while (true);
}

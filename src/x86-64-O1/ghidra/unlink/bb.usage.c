
#include "unlink.h"

long null_ARRAY_0060f76_0_8_;
long null_ARRAY_0060f76_8_8_;
long null_ARRAY_0060f94_0_8_;
long null_ARRAY_0060f94_16_8_;
long null_ARRAY_0060f94_24_8_;
long null_ARRAY_0060f94_32_8_;
long null_ARRAY_0060f94_40_8_;
long null_ARRAY_0060f94_48_8_;
long null_ARRAY_0060f94_8_8_;
long null_ARRAY_0060f98_0_4_;
long null_ARRAY_0060f98_16_8_;
long null_ARRAY_0060f98_4_4_;
long null_ARRAY_0060f98_8_4_;
long local_68_2_2_;
long local_6b_1_3_;
long local_6c_4_4_;
long local_6e_4_4_;
long local_6f_4_4_;
long local_70_0_2_;
long local_70_4_4_;
long local_71_0_1_;
long local_71_4_4_;
long local_72_4_4_;
long DAT_00000010;
long DAT_0040cb47;
long DAT_0040cbcb;
long DAT_0040ce18;
long DAT_0040cee0;
long DAT_0040cee4;
long DAT_0040cee8;
long DAT_0040ceeb;
long DAT_0040ceed;
long DAT_0040cef1;
long DAT_0040cef5;
long DAT_0040d6ab;
long DAT_0040da55;
long DAT_0040db59;
long DAT_0040db5f;
long DAT_0040db71;
long DAT_0040db72;
long DAT_0040db90;
long DAT_0040db94;
long DAT_0040dc1e;
long DAT_0060f340;
long DAT_0060f350;
long DAT_0060f360;
long DAT_0060f708;
long DAT_0060f770;
long DAT_0060f774;
long DAT_0060f778;
long DAT_0060f77c;
long DAT_0060f780;
long DAT_0060f790;
long DAT_0060f7a0;
long DAT_0060f7a8;
long DAT_0060f7c0;
long DAT_0060f7c8;
long DAT_0060f810;
long DAT_0060f818;
long DAT_0060f820;
long DAT_0060f9b8;
long DAT_0060f9c0;
long DAT_0060f9c8;
long DAT_0060f9d8;
long fde_0040e790;
long null_ARRAY_0040cde0;
long null_ARRAY_0040ce40;
long null_ARRAY_0040e040;
long null_ARRAY_0040e280;
long null_ARRAY_0060f760;
long null_ARRAY_0060f7e0;
long null_ARRAY_0060f840;
long null_ARRAY_0060f940;
long null_ARRAY_0060f980;
long PTR_DAT_0060f700;
long PTR_null_ARRAY_0060f758;
void
FUN_00401a6e (uint uParm1)
{
  int iVar1;
  long lVar2;
  undefined8 uVar3;
  undefined **ppuVar4;
  char *pcVar5;
  undefined *puVar6;
  undefined *local_88;
  char *local_80;
  undefined8 local_78;
  char *local_70;
  char *local_68;
  char *local_60;
  char *local_58;
  char *local_50;
  char *local_48;
  char *local_40;
  char *local_38;
  char *local_30;
  undefined8 local_28;
  undefined8 local_20;

  ppuVar4 = &local_88;
  if (uParm1 != 0)
    {
      func_0x00401630 (DAT_0060f7a0,
		       "Try \'%s --help\' for more information.\n",
		       DAT_0060f820);
      goto LAB_00401c2f;
    }
  func_0x004014c0 ("Usage: %s FILE\n  or:  %s OPTION\n", DAT_0060f820,
		   DAT_0060f820);
  uVar3 = DAT_0060f780;
  func_0x00401640
    ("Call the unlink function to remove the specified FILE.\n\n",
     DAT_0060f780);
  func_0x00401640 ("      --help     display this help and exit\n", uVar3);
  func_0x00401640 ("      --version  output version information and exit\n",
		   uVar3);
  local_88 = &DAT_0040cb47;
  local_80 = "test invocation";
  local_78 = 0x40cbaa;
  local_70 = "Multi-call invocation";
  local_68 = "sha224sum";
  local_60 = "sha2 utilities";
  local_58 = "sha256sum";
  local_50 = "sha2 utilities";
  local_48 = "sha384sum";
  local_40 = "sha2 utilities";
  local_38 = "sha512sum";
  local_30 = "sha2 utilities";
  local_28 = 0;
  local_20 = 0;
  puVar6 = &DAT_0040cb47;
  do
    {
      iVar1 = func_0x00401730 ("unlink", puVar6);
      if (iVar1 == 0)
	break;
      ppuVar4 = ppuVar4 + 2;
      puVar6 = *ppuVar4;
    }
  while (puVar6 != (undefined *) 0x0);
  pcVar5 = ppuVar4[1];
  if (pcVar5 == (char *) 0x0)
    {
      func_0x004014c0 ("\n%s online help: <%s>\n", "GNU coreutils",
		       "https://www.gnu.org/software/coreutils/");
      lVar2 = func_0x00401790 (5, 0);
      if (lVar2 == 0)
	goto LAB_00401c36;
      iVar1 = func_0x004016a0 (lVar2, &DAT_0040cbcb, 3);
      if (iVar1 == 0)
	{
	  func_0x004014c0 ("Full documentation at: <%s%s>\n",
			   "https://www.gnu.org/software/coreutils/",
			   "unlink");
	  pcVar5 = "unlink";
	  uVar3 = 0x40cb63;
	  goto LAB_00401c1d;
	}
      pcVar5 = "unlink";
    LAB_00401bdb:
      ;
      func_0x004014c0
	("Report %s translation bugs to <https://translationproject.org/team/>\n",
	 "unlink");
    }
  else
    {
      func_0x004014c0 ("\n%s online help: <%s>\n", "GNU coreutils",
		       "https://www.gnu.org/software/coreutils/");
      lVar2 = func_0x00401790 (5, 0);
      if ((lVar2 != 0)
	  && (iVar1 = func_0x004016a0 (lVar2, &DAT_0040cbcb, 3), iVar1 != 0))
	goto LAB_00401bdb;
    }
  func_0x004014c0 ("Full documentation at: <%s%s>\n",
		   "https://www.gnu.org/software/coreutils/", "unlink");
  uVar3 = 0x40db8f;
  if (pcVar5 == "unlink")
    {
      uVar3 = 0x40cb63;
    }
LAB_00401c1d:
  ;
  do
    {
      func_0x004014c0
	("or available locally via: info \'(coreutils) %s%s\'\n", pcVar5,
	 uVar3);
    LAB_00401c2f:
      ;
      func_0x004017e0 ((ulong) uParm1);
    LAB_00401c36:
      ;
      func_0x004014c0 ("Full documentation at: <%s%s>\n",
		       "https://www.gnu.org/software/coreutils/", "unlink");
      pcVar5 = "unlink";
      uVar3 = 0x40cb63;
    }
  while (true);
}

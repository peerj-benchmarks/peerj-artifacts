
#include "truncate.h"

long null_ARRAY_0061144_0_8_;
long null_ARRAY_0061144_8_8_;
long null_ARRAY_0061164_0_8_;
long null_ARRAY_0061164_16_8_;
long null_ARRAY_0061164_24_8_;
long null_ARRAY_0061164_32_8_;
long null_ARRAY_0061164_40_8_;
long null_ARRAY_0061164_48_8_;
long null_ARRAY_0061164_8_8_;
long null_ARRAY_0061168_0_4_;
long null_ARRAY_0061168_16_8_;
long null_ARRAY_0061168_4_4_;
long null_ARRAY_0061168_8_4_;
long bVar1;
long local_68_2_2_;
long local_6b_1_3_;
long local_6c_4_4_;
long local_6e_4_4_;
long local_6f_4_4_;
long local_70_0_2_;
long local_70_4_4_;
long local_71_0_1_;
long local_71_4_4_;
long local_72_4_4_;
long DAT_00000010;
long DAT_0040d7e6;
long DAT_0040d86a;
long DAT_0040e138;
long DAT_0040e13c;
long DAT_0040e140;
long DAT_0040e143;
long DAT_0040e145;
long DAT_0040e149;
long DAT_0040e14d;
long DAT_0040e8eb;
long DAT_0040f008;
long DAT_0040f111;
long DAT_0040f117;
long DAT_0040f129;
long DAT_0040f12a;
long DAT_0040f148;
long DAT_0040f14c;
long DAT_0040f1d6;
long DAT_00611000;
long DAT_00611010;
long DAT_00611020;
long DAT_006113e8;
long DAT_00611450;
long DAT_00611454;
long DAT_00611458;
long DAT_0061145c;
long DAT_00611480;
long DAT_00611490;
long DAT_006114a0;
long DAT_006114a8;
long DAT_006114c0;
long DAT_006114c8;
long DAT_00611510;
long DAT_00611518;
long DAT_00611519;
long DAT_00611520;
long DAT_00611528;
long DAT_00611530;
long DAT_006116b8;
long DAT_006116c0;
long DAT_006116c8;
long DAT_006116d8;
long fde_0040fd40;
long null_ARRAY_0040e000;
long null_ARRAY_0040f5e0;
long null_ARRAY_0040f820;
long null_ARRAY_00611440;
long null_ARRAY_006114e0;
long null_ARRAY_00611540;
long null_ARRAY_00611640;
long null_ARRAY_00611680;
long PTR_DAT_006113e0;
long PTR_null_ARRAY_00611438;
void
FUN_00401b0e (uint uParm1)
{
  int iVar1;
  long lVar2;
  undefined8 uVar3;
  undefined **ppuVar4;
  char *pcVar5;
  undefined *puVar6;
  undefined *local_88;
  char *local_80;
  undefined8 local_78;
  char *local_70;
  char *local_68;
  char *local_60;
  char *local_58;
  char *local_50;
  char *local_48;
  char *local_40;
  char *local_38;
  char *local_30;
  undefined8 local_28;
  undefined8 local_20;

  ppuVar4 = &local_88;
  if (uParm1 != 0)
    {
      func_0x004016e0 (DAT_006114a0,
		       "Try \'%s --help\' for more information.\n",
		       DAT_00611530);
      goto LAB_00401d1a;
    }
  func_0x00401540 ("Usage: %s OPTION... FILE...\n", DAT_00611530);
  uVar3 = DAT_00611480;
  func_0x004016f0
    ("Shrink or extend the size of each FILE to the specified size\n\nA FILE argument that does not exist is created.\n\nIf a FILE is larger than the specified size, the extra data is lost.\nIf a FILE is shorter, it is extended and the extended part (hole)\nreads as zero bytes.\n",
     DAT_00611480);
  func_0x004016f0
    ("\nMandatory arguments to long options are mandatory for short options too.\n",
     uVar3);
  func_0x004016f0 ("  -c, --no-create        do not create any files\n",
		   uVar3);
  func_0x004016f0
    ("  -o, --io-blocks        treat SIZE as number of IO blocks instead of bytes\n",
     uVar3);
  func_0x004016f0
    ("  -r, --reference=RFILE  base size on RFILE\n  -s, --size=SIZE        set or adjust the file size by SIZE bytes\n",
     uVar3);
  func_0x004016f0 ("      --help     display this help and exit\n", uVar3);
  func_0x004016f0 ("      --version  output version information and exit\n",
		   uVar3);
  func_0x004016f0
    ("\nThe SIZE argument is an integer and optional unit (example: 10K is 10*1024).\nUnits are K,M,G,T,P,E,Z,Y (powers of 1024) or KB,MB,... (powers of 1000).\n",
     uVar3);
  func_0x004016f0
    ("\nSIZE may also be prefixed by one of the following modifying characters:\n\'+\' extend by, \'-\' reduce by, \'<\' at most, \'>\' at least,\n\'/\' round down to multiple of, \'%\' round up to multiple of.\n",
     uVar3);
  local_88 = &DAT_0040d7e6;
  local_80 = "test invocation";
  local_78 = 0x40d849;
  local_70 = "Multi-call invocation";
  local_68 = "sha224sum";
  local_60 = "sha2 utilities";
  local_58 = "sha256sum";
  local_50 = "sha2 utilities";
  local_48 = "sha384sum";
  local_40 = "sha2 utilities";
  local_38 = "sha512sum";
  local_30 = "sha2 utilities";
  local_28 = 0;
  local_20 = 0;
  puVar6 = &DAT_0040d7e6;
  do
    {
      iVar1 = func_0x004017d0 ("truncate", puVar6);
      if (iVar1 == 0)
	break;
      ppuVar4 = ppuVar4 + 2;
      puVar6 = *ppuVar4;
    }
  while (puVar6 != (undefined *) 0x0);
  pcVar5 = ppuVar4[1];
  if (pcVar5 == (char *) 0x0)
    {
      func_0x00401540 ("\n%s online help: <%s>\n", "GNU coreutils",
		       "https://www.gnu.org/software/coreutils/");
      lVar2 = func_0x00401830 (5, 0);
      if (lVar2 == 0)
	goto LAB_00401d21;
      iVar1 = func_0x00401740 (lVar2, &DAT_0040d86a, 3);
      if (iVar1 == 0)
	{
	  func_0x00401540 ("Full documentation at: <%s%s>\n",
			   "https://www.gnu.org/software/coreutils/",
			   "truncate");
	  pcVar5 = "truncate";
	  uVar3 = 0x40d802;
	  goto LAB_00401d08;
	}
      pcVar5 = "truncate";
    LAB_00401cc6:
      ;
      func_0x00401540
	("Report %s translation bugs to <https://translationproject.org/team/>\n",
	 "truncate");
    }
  else
    {
      func_0x00401540 ("\n%s online help: <%s>\n", "GNU coreutils",
		       "https://www.gnu.org/software/coreutils/");
      lVar2 = func_0x00401830 (5, 0);
      if ((lVar2 != 0)
	  && (iVar1 = func_0x00401740 (lVar2, &DAT_0040d86a, 3), iVar1 != 0))
	goto LAB_00401cc6;
    }
  func_0x00401540 ("Full documentation at: <%s%s>\n",
		   "https://www.gnu.org/software/coreutils/", "truncate");
  uVar3 = 0x40f147;
  if (pcVar5 == "truncate")
    {
      uVar3 = 0x40d802;
    }
LAB_00401d08:
  ;
  do
    {
      func_0x00401540
	("or available locally via: info \'(coreutils) %s%s\'\n", pcVar5,
	 uVar3);
    LAB_00401d1a:
      ;
      func_0x00401880 ((ulong) uParm1);
    LAB_00401d21:
      ;
      func_0x00401540 ("Full documentation at: <%s%s>\n",
		       "https://www.gnu.org/software/coreutils/", "truncate");
      pcVar5 = "truncate";
      uVar3 = 0x40d802;
    }
  while (true);
}

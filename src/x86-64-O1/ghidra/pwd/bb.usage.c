
#include "pwd.h"

long null_ARRAY_0061174_0_8_;
long null_ARRAY_0061174_8_8_;
long null_ARRAY_0061194_0_8_;
long null_ARRAY_0061194_16_8_;
long null_ARRAY_0061194_24_8_;
long null_ARRAY_0061194_32_8_;
long null_ARRAY_0061194_40_8_;
long null_ARRAY_0061194_48_8_;
long null_ARRAY_0061194_8_8_;
long null_ARRAY_0061198_0_4_;
long null_ARRAY_0061198_16_8_;
long null_ARRAY_0061198_4_4_;
long null_ARRAY_0061198_8_4_;
long local_68_2_2_;
long local_6b_1_3_;
long local_6c_4_4_;
long local_6e_4_4_;
long local_6f_4_4_;
long local_70_0_2_;
long local_70_4_4_;
long local_71_0_1_;
long local_71_4_4_;
long local_72_4_4_;
long DAT_00000010;
long DAT_0040e540;
long DAT_0040e544;
long DAT_0040e547;
long DAT_0040e549;
long DAT_0040e55d;
long DAT_0040e55e;
long DAT_0040e5a4;
long DAT_0040e5bf;
long DAT_0040e643;
long DAT_0040e657;
long DAT_0040eaf8;
long DAT_0040eafc;
long DAT_0040eb00;
long DAT_0040eb03;
long DAT_0040eb05;
long DAT_0040eb09;
long DAT_0040eb0d;
long DAT_0040f2ab;
long DAT_0040f655;
long DAT_0040f759;
long DAT_0040f75f;
long DAT_0040f761;
long DAT_0040f762;
long DAT_0040f780;
long DAT_0040f784;
long DAT_0040f80e;
long DAT_006112a8;
long DAT_006112b8;
long DAT_006112c8;
long DAT_006116e8;
long DAT_00611750;
long DAT_00611754;
long DAT_00611758;
long DAT_0061175c;
long DAT_00611780;
long DAT_00611790;
long DAT_006117a0;
long DAT_006117a8;
long DAT_006117c0;
long DAT_006117c8;
long DAT_00611810;
long DAT_00611818;
long DAT_00611820;
long DAT_006119b8;
long DAT_006119c0;
long DAT_006119c8;
long DAT_006119d0;
long DAT_006119d8;
long DAT_006119e8;
long fde_004103e8;
long null_ARRAY_0040ea00;
long null_ARRAY_0040fc20;
long null_ARRAY_0040fe60;
long null_ARRAY_00611740;
long null_ARRAY_006117e0;
long null_ARRAY_00611840;
long null_ARRAY_00611940;
long null_ARRAY_00611980;
long PTR_DAT_006116e0;
long PTR_null_ARRAY_00611738;
void
FUN_00402350 (uint uParm1)
{
  int iVar1;
  long lVar2;
  undefined8 uVar3;
  undefined **ppuVar4;
  undefined *puVar5;
  undefined *local_88;
  char *local_80;
  undefined8 local_78;
  char *local_70;
  char *local_68;
  char *local_60;
  char *local_58;
  char *local_50;
  char *local_48;
  char *local_40;
  char *local_38;
  char *local_30;
  undefined8 local_28;
  undefined8 local_20;

  ppuVar4 = &local_88;
  if (uParm1 != 0)
    {
      func_0x00401960 (DAT_006117a0,
		       "Try \'%s --help\' for more information.\n",
		       DAT_00611820);
      goto LAB_0040253c;
    }
  func_0x00401780 ("Usage: %s [OPTION]...\n", DAT_00611820);
  uVar3 = DAT_00611780;
  func_0x00401970
    ("Print the full filename of the current working directory.\n\n",
     DAT_00611780);
  func_0x00401970
    ("  -L, --logical   use PWD from environment, even if it contains symlinks\n  -P, --physical  avoid all symlinks\n",
     uVar3);
  func_0x00401970 ("      --help     display this help and exit\n", uVar3);
  func_0x00401970 ("      --version  output version information and exit\n",
		   uVar3);
  func_0x00401970 ("\nIf no option is specified, -P is assumed.\n", uVar3);
  func_0x00401780
    ("\nNOTE: your shell may have its own version of %s, which usually supersedes\nthe version described here.  Please refer to your shell\'s documentation\nfor details about the options it supports.\n",
     &DAT_0040e5a4);
  local_88 = &DAT_0040e5bf;
  local_80 = "test invocation";
  local_78 = 0x40e622;
  local_70 = "Multi-call invocation";
  local_68 = "sha224sum";
  local_60 = "sha2 utilities";
  local_58 = "sha256sum";
  local_50 = "sha2 utilities";
  local_48 = "sha384sum";
  local_40 = "sha2 utilities";
  local_38 = "sha512sum";
  local_30 = "sha2 utilities";
  local_28 = 0;
  local_20 = 0;
  puVar5 = &DAT_0040e5bf;
  do
    {
      iVar1 = func_0x00401a80 (&DAT_0040e5a4, puVar5);
      if (iVar1 == 0)
	break;
      ppuVar4 = ppuVar4 + 2;
      puVar5 = *ppuVar4;
    }
  while (puVar5 != (undefined *) 0x0);
  puVar5 = ppuVar4[1];
  if (puVar5 == (undefined *) 0x0)
    {
      func_0x00401780 ("\n%s online help: <%s>\n", "GNU coreutils",
		       "https://www.gnu.org/software/coreutils/");
      lVar2 = func_0x00401ae0 (5, 0);
      if (lVar2 == 0)
	goto LAB_00402543;
      iVar1 = func_0x004019f0 (lVar2, &DAT_0040e643, 3);
      if (iVar1 == 0)
	{
	  func_0x00401780 ("Full documentation at: <%s%s>\n",
			   "https://www.gnu.org/software/coreutils/",
			   &DAT_0040e5a4);
	  puVar5 = &DAT_0040e5a4;
	  uVar3 = 0x40e5db;
	  goto LAB_0040252a;
	}
      puVar5 = &DAT_0040e5a4;
    LAB_004024e8:
      ;
      func_0x00401780
	("Report %s translation bugs to <https://translationproject.org/team/>\n",
	 &DAT_0040e5a4);
    }
  else
    {
      func_0x00401780 ("\n%s online help: <%s>\n", "GNU coreutils",
		       "https://www.gnu.org/software/coreutils/");
      lVar2 = func_0x00401ae0 (5, 0);
      if ((lVar2 != 0)
	  && (iVar1 = func_0x004019f0 (lVar2, &DAT_0040e643, 3), iVar1 != 0))
	goto LAB_004024e8;
    }
  func_0x00401780 ("Full documentation at: <%s%s>\n",
		   "https://www.gnu.org/software/coreutils/", &DAT_0040e5a4);
  uVar3 = 0x40f77f;
  if (puVar5 == &DAT_0040e5a4)
    {
      uVar3 = 0x40e5db;
    }
LAB_0040252a:
  ;
  do
    {
      func_0x00401780
	("or available locally via: info \'(coreutils) %s%s\'\n", puVar5,
	 uVar3);
    LAB_0040253c:
      ;
      func_0x00401b30 ((ulong) uParm1);
    LAB_00402543:
      ;
      func_0x00401780 ("Full documentation at: <%s%s>\n",
		       "https://www.gnu.org/software/coreutils/",
		       &DAT_0040e5a4);
      puVar5 = &DAT_0040e5a4;
      uVar3 = 0x40e5db;
    }
  while (true);
}

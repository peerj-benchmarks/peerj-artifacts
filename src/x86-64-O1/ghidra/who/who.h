typedef unsigned char   undefined;

typedef unsigned char    byte;
typedef unsigned char    dwfenc;
typedef unsigned int    dword;
typedef unsigned long    qword;
typedef unsigned char    undefined1;
typedef unsigned short    undefined2;
typedef unsigned int    undefined3;
typedef unsigned int    undefined4;
typedef unsigned long    undefined6;
typedef unsigned long    undefined8;
typedef unsigned short    word;
#define unkbyte9   unsigned long long
#define unkbyte10   unsigned long long
#define unkbyte11   unsigned long long
#define unkbyte12   unsigned long long
#define unkbyte13   unsigned long long
#define unkbyte14   unsigned long long
#define unkbyte15   unsigned long long
#define unkbyte16   unsigned long long

#define unkuint9   unsigned long long
#define unkuint10   unsigned long long
#define unkuint11   unsigned long long
#define unkuint12   unsigned long long
#define unkuint13   unsigned long long
#define unkuint14   unsigned long long
#define unkuint15   unsigned long long
#define unkuint16   unsigned long long

#define unkint9   long long
#define unkint10   long long
#define unkint11   long long
#define unkint12   long long
#define unkint13   long long
#define unkint14   long long
#define unkint15   long long
#define unkint16   long long

#define unkfloat1   float
#define unkfloat2   float
#define unkfloat3   float
#define unkfloat5   double
#define unkfloat6   double
#define unkfloat7   double
#define unkfloat9   long double
#define unkfloat11   long double
#define unkfloat12   long double
#define unkfloat13   long double
#define unkfloat14   long double
#define unkfloat15   long double
#define unkfloat16   long double

#define BADSPACEBASE   void
#define code   void

#define ulong long
#define true 1
#define bool int
#define false 0
#define float10 int
#define ushort int
#define undefined7 long
#define undefined2 long
#define uint5 long
#define uint6 long

// Add definition for uint
typedef unsigned int    uint;

#define sbyte char

extern imperfection_wrapper(void);
typedef struct eh_frame_hdr eh_frame_hdr, *Peh_frame_hdr;

struct eh_frame_hdr {
    byte eh_frame_hdr_version; // Exception Handler Frame Header Version
    dwfenc eh_frame_pointer_encoding; // Exception Handler Frame Pointer Encoding
    dwfenc eh_frame_desc_entry_count_encoding; // Encoding of # of Exception Handler FDEs
    dwfenc eh_frame_table_encoding; // Exception Handler Table Encoding
};

typedef struct fde_table_entry fde_table_entry, *Pfde_table_entry;

struct fde_table_entry {
    dword initial_loc; // Initial Location
    dword data_loc; // Data location
};

typedef enum Elf64_DynTag {
    DT_AUDIT=1879047932,
    DT_AUXILIARY=2147483645,
    DT_BIND_NOW=24,
    DT_CHECKSUM=1879047672,
    DT_CONFIG=1879047930,
    DT_DEBUG=21,
    DT_DEPAUDIT=1879047931,
    DT_ENCODING=32,
    DT_FEATURE_1=1879047676,
    DT_FILTER=2147483647,
    DT_FINI=13,
    DT_FINI_ARRAY=26,
    DT_FINI_ARRAYSZ=28,
    DT_FLAGS=30,
    DT_FLAGS_1=1879048187,
    DT_GNU_CONFLICT=1879047928,
    DT_GNU_CONFLICTSZ=1879047670,
    DT_GNU_HASH=1879047925,
    DT_GNU_LIBLIST=1879047929,
    DT_GNU_LIBLISTSZ=1879047671,
    DT_GNU_PRELINKED=1879047669,
    DT_HASH=4,
    DT_INIT=12,
    DT_INIT_ARRAY=25,
    DT_INIT_ARRAYSZ=27,
    DT_JMPREL=23,
    DT_MOVEENT=1879047674,
    DT_MOVESZ=1879047675,
    DT_MOVETAB=1879047934,
    DT_NEEDED=1,
    DT_NULL=0,
    DT_PLTGOT=3,
    DT_PLTPAD=1879047933,
    DT_PLTPADSZ=1879047673,
    DT_PLTREL=20,
    DT_PLTRELSZ=2,
    DT_POSFLAG_1=1879047677,
    DT_PREINIT_ARRAYSZ=33,
    DT_REL=17,
    DT_RELA=7,
    DT_RELACOUNT=1879048185,
    DT_RELAENT=9,
    DT_RELASZ=8,
    DT_RELCOUNT=1879048186,
    DT_RELENT=19,
    DT_RELSZ=18,
    DT_RPATH=15,
    DT_RUNPATH=29,
    DT_SONAME=14,
    DT_STRSZ=10,
    DT_STRTAB=5,
    DT_SYMBOLIC=16,
    DT_SYMENT=11,
    DT_SYMINENT=1879047679,
    DT_SYMINFO=1879047935,
    DT_SYMINSZ=1879047678,
    DT_SYMTAB=6,
    DT_TEXTREL=22,
    DT_TLSDESC_GOT=1879047927,
    DT_TLSDESC_PLT=1879047926,
    DT_VERDEF=1879048188,
    DT_VERDEFNUM=1879048189,
    DT_VERNEED=1879048190,
    DT_VERNEEDNUM=1879048191,
    DT_VERSYM=1879048176
} Elf64_DynTag;

typedef enum Elf_ProgramHeaderType {
    PT_DYNAMIC=2,
    PT_GNU_EH_FRAME=1685382480,
    PT_GNU_RELRO=1685382482,
    PT_GNU_STACK=1685382481,
    PT_INTERP=3,
    PT_LOAD=1,
    PT_NOTE=4,
    PT_NULL=0,
    PT_PHDR=6,
    PT_SHLIB=5,
    PT_TLS=7
} Elf_ProgramHeaderType;

typedef struct Elf64_Dyn Elf64_Dyn, *PElf64_Dyn;

struct Elf64_Dyn {
    enum Elf64_DynTag d_tag;
    qword d_val;
};

typedef struct Elf64_Shdr Elf64_Shdr, *PElf64_Shdr;

typedef enum Elf_SectionHeaderType {
    SHT_CHECKSUM=1879048184,
    SHT_DYNAMIC=6,
    SHT_DYNSYM=11,
    SHT_FINI_ARRAY=15,
    SHT_GNU_ATTRIBUTES=1879048181,
    SHT_GNU_HASH=1879048182,
    SHT_GNU_LIBLIST=1879048183,
    SHT_GNU_verdef=1879048189,
    SHT_GNU_verneed=1879048190,
    SHT_GNU_versym=1879048191,
    SHT_GROUP=17,
    SHT_HASH=5,
    SHT_INIT_ARRAY=14,
    SHT_NOBITS=8,
    SHT_NOTE=7,
    SHT_NULL=0,
    SHT_PREINIT_ARRAY=16,
    SHT_PROGBITS=1,
    SHT_REL=9,
    SHT_RELA=4,
    SHT_SHLIB=10,
    SHT_STRTAB=3,
    SHT_SUNW_COMDAT=1879048187,
    SHT_SUNW_move=1879048186,
    SHT_SUNW_syminfo=1879048188,
    SHT_SYMTAB=2,
    SHT_SYMTAB_SHNDX=18
} Elf_SectionHeaderType;

struct Elf64_Shdr {
    dword sh_name;
    enum Elf_SectionHeaderType sh_type;
    qword sh_flags;
    qword sh_addr;
    qword sh_offset;
    qword sh_size;
    dword sh_link;
    dword sh_info;
    qword sh_addralign;
    qword sh_entsize;
};

typedef struct Elf64_Phdr Elf64_Phdr, *PElf64_Phdr;

struct Elf64_Phdr {
    enum Elf_ProgramHeaderType p_type;
    dword p_flags;
    qword p_offset;
    qword p_vaddr;
    qword p_paddr;
    qword p_filesz;
    qword p_memsz;
    qword p_align;
};

typedef struct Elf64_Rela Elf64_Rela, *PElf64_Rela;

struct Elf64_Rela {
    qword r_offset; // location to apply the relocation action
    qword r_info; // the symbol table index and the type of relocation
    qword r_addend; // a constant addend used to compute the relocatable field value
};

typedef struct Elf64_Sym Elf64_Sym, *PElf64_Sym;

struct Elf64_Sym {
    dword st_name;
    byte st_info;
    byte st_other;
    word st_shndx;
    qword st_value;
    qword st_size;
};

typedef struct Elf64_Ehdr Elf64_Ehdr, *PElf64_Ehdr;

struct Elf64_Ehdr {
    byte e_ident_magic_num;
    char e_ident_magic_str[3];
    byte e_ident_class;
    byte e_ident_data;
    byte e_ident_version;
    byte e_ident_pad[9];
    word e_type;
    word e_machine;
    dword e_version;
    qword e_entry;
    qword e_phoff;
    qword e_shoff;
    dword e_flags;
    word e_ehsize;
    word e_phentsize;
    word e_phnum;
    word e_shentsize;
    word e_shnum;
    word e_shstrndx;
};




undefined8 _DT_INIT(void);
void FUN_00401740(void);
void entry(void);
void FUN_00401c80(void);
void FUN_00401d00(void);
void FUN_00401d80(void);
void FUN_00401dbe(uint param_1,undefined *param_2,undefined param_3,ulong param_4,undefined8 param_5,undefined8 param_6,undefined8 param_7,undefined8 param_8,undefined8 param_9,undefined8 param_10);
undefined1 * FUN_00401f6d(long lParm1);
void FUN_00401fc0(long lParm1,long lParm2);
undefined8 FUN_004023d5(long lParm1);
void FUN_0040241c(undefined8 uParm1,ulong uParm2);
void FUN_004029ea(uint uParm1);
undefined8 FUN_00402ca0(uint uParm1,undefined8 *puParm2);
long FUN_00403022(long lParm1,int *piParm2);
void FUN_004030ad(undefined8 uParm1);
void FUN_004030c0(void);
ulong FUN_00403166(ulong uParm1);
char * FUN_004031f4(long lParm1,long lParm2);
void FUN_0040327f(long lParm1);
int * FUN_00403314(int *piParm1,int iParm2);
undefined * FUN_00403344(char *pcParm1,int iParm2);
ulong FUN_004033fc(undefined *param_1,ulong param_2,long param_3,ulong param_4,uint param_5,ulong param_6,long param_7,char *param_8,char *param_9);
undefined1 * FUN_004041b3(int iParm1,undefined8 uParm2,undefined8 uParm3,uint *puParm4);
ulong FUN_00404355(undefined1 *puParm1,byte bParm2,undefined8 uParm3);
void FUN_00404389(undefined8 uParm1,undefined8 uParm2,char cParm3);
void FUN_00404412(undefined8 uParm1,char cParm2);
void FUN_0040442b(undefined8 uParm1);
void FUN_0040443e(uint uParm1,undefined8 uParm2,undefined8 uParm3);
void FUN_004044cd(void);
void FUN_004044e0(undefined8 uParm1,undefined8 uParm2);
void FUN_004044f5(undefined8 uParm1);
void FUN_0040450b(undefined8 uParm1,long lParm2,undefined8 uParm3,undefined8 uParm4,undefined8 *puParm5,undefined8 uParm6);
void FUN_00404748(void);
void FUN_0040479a(void);
void FUN_0040481f(long lParm1);
long FUN_00404839(long lParm1,long lParm2);
void FUN_0040486c(void);
void FUN_00404894(undefined4 uParm1,undefined4 uParm2,undefined4 uParm3,undefined4 uParm4,undefined4 uParm5,undefined4 uParm6,undefined4 uParm7,undefined4 uParm8,undefined8 param_1,undefined8 param_2,undefined8 param_3,undefined8 param_4,undefined8 param_5,undefined8 param_6);
void FUN_00404923(void);
void FUN_00404950(undefined8 uParm1);
void FUN_00404990(uint uParm1,uint uParm2,undefined8 uParm3,undefined8 uParm4);
void FUN_004049e7(undefined4 uParm1,undefined4 uParm2,undefined4 uParm3,undefined4 uParm4,undefined4 uParm5,undefined4 uParm6,undefined4 uParm7,undefined4 uParm8,ulong param_1,ulong param_2,undefined8 param_3,undefined8 param_4,undefined8 param_5,undefined8 param_6);
void FUN_00404aba(long lParm1,int *piParm2);
ulong FUN_00404b79(int param_1,undefined8 *param_2,char *param_3,long *param_4,int *param_5,int param_6,int *param_7,int param_8,undefined8 param_9);
ulong FUN_00405034(uint param_1,undefined8 *param_2,char *param_3,long param_4,undefined8 param_5,ulong param_6,uint *param_7,int param_8);
void FUN_004054b0(void);
void FUN_00405506(void);
void FUN_0040551c(void);
void FUN_0040553e(long lParm1);
ulong FUN_00405558(uint *puParm1,byte *pbParm2,long lParm3);
undefined * FUN_004055bd(long lParm1);
undefined8 FUN_0040561a(undefined8 uParm1,ulong *puParm2,long *plParm3,ulong uParm4);
void FUN_004057d0(long lParm1,long lParm2);
ulong FUN_004057fe(long lParm1,long lParm2);
ulong FUN_0040584a(long *plParm1,undefined8 uParm2,undefined8 uParm3);
ulong FUN_004058a6(undefined8 uParm1,undefined8 uParm2,undefined8 uParm3);
ulong FUN_00405973(undefined8 uParm1);
undefined8 FUN_004059d4(void);
char * FUN_004059dc(void);
undefined8 FUN_00405d17(undefined8 uParm1);
ulong FUN_00405d9a(long lParm1);
undefined8 FUN_00405e2a(void);
void FUN_00405e3d(void);
ulong FUN_00405e4b(char *pcParm1,long lParm2);
long FUN_00405e92(ulong uParm1,uint *puParm2,ulong uParm3,uint *puParm4,long *plParm5);
ulong FUN_00405fb6(void);
ulong FUN_00405fe8(void);
int * FUN_004061f5(ulong uParm1,long lParm2,ulong uParm3,uint *puParm4,ulong *puParm5);
char * FUN_004067d8(int iParm1,long lParm2,long lParm3,long lParm4,uint uParm5);
void FUN_00406df0(uint param_1,undefined8 param_2);
ulong FUN_00406fb6(void);
void FUN_004071c9(undefined8 uParm1,uint uParm2);
undefined8 *FUN_0040736a(undefined8 *puParm1,undefined8 *puParm2,undefined8 *puParm3,undefined8 uParm4);
double FUN_0040cf07(int *piParm1);
void FUN_0040cf4f(int *param_1);
undefined8 FUN_0040cfd0(uint *puParm1,ulong *puParm2);
undefined8 FUN_0040d3f5(byte *pbParm1,ulong *puParm2,byte **ppbParm3);
ulong FUN_0040df5a(void);
ulong FUN_0040df82(void);
void FUN_0040dfc0(void);
undefined8 _DT_FINI(void);
undefined FUN_00612000();
undefined FUN_00612008();
undefined FUN_00612010();
undefined FUN_00612018();
undefined FUN_00612020();
undefined FUN_00612028();
undefined FUN_00612030();
undefined FUN_00612038();
undefined FUN_00612040();
undefined FUN_00612048();
undefined FUN_00612050();
undefined FUN_00612058();
undefined FUN_00612060();
undefined FUN_00612068();
undefined FUN_00612070();
undefined FUN_00612078();
undefined FUN_00612080();
undefined FUN_00612088();
undefined FUN_00612090();
undefined FUN_00612098();
undefined FUN_006120a0();
undefined FUN_006120a8();
undefined FUN_006120b0();
undefined FUN_006120b8();
undefined FUN_006120c0();
undefined FUN_006120c8();
undefined FUN_006120d0();
undefined FUN_006120d8();
undefined FUN_006120e0();
undefined FUN_006120e8();
undefined FUN_006120f0();
undefined FUN_006120f8();
undefined FUN_00612100();
undefined FUN_00612108();
undefined FUN_00612110();
undefined FUN_00612118();
undefined FUN_00612120();
undefined FUN_00612128();
undefined FUN_00612130();
undefined FUN_00612138();
undefined FUN_00612140();
undefined FUN_00612148();
undefined FUN_00612150();
undefined FUN_00612158();
undefined FUN_00612160();
undefined FUN_00612168();
undefined FUN_00612170();
undefined FUN_00612178();
undefined FUN_00612180();
undefined FUN_00612188();
undefined FUN_00612190();
undefined FUN_00612198();
undefined FUN_006121a0();
undefined FUN_006121a8();
undefined FUN_006121b0();
undefined FUN_006121b8();
undefined FUN_006121c0();
undefined FUN_006121c8();
undefined FUN_006121d0();
undefined FUN_006121d8();
undefined FUN_006121e0();
undefined FUN_006121e8();
undefined FUN_006121f0();
undefined FUN_006121f8();
undefined FUN_00612200();
undefined FUN_00612208();
undefined FUN_00612210();
undefined FUN_00612218();
undefined FUN_00612220();
undefined FUN_00612228();
undefined FUN_00612230();
undefined FUN_00612238();
undefined FUN_00612240();
undefined FUN_00612248();
undefined FUN_00612250();
undefined FUN_00612258();
undefined FUN_00612260();
undefined FUN_00612268();
undefined FUN_00612270();


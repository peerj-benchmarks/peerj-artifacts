
#include "who.h"

long DAT_0061164_1_1_;
long null_ARRAY_006116a_0_8_;
long null_ARRAY_006116a_8_8_;
long null_ARRAY_0061182_0_4_;
long null_ARRAY_0061198_0_8_;
long null_ARRAY_0061198_16_8_;
long null_ARRAY_0061198_24_8_;
long null_ARRAY_0061198_32_8_;
long null_ARRAY_0061198_40_8_;
long null_ARRAY_0061198_48_8_;
long null_ARRAY_0061198_8_8_;
long null_ARRAY_006119c_0_4_;
long null_ARRAY_006119c_16_8_;
long null_ARRAY_006119c_4_4_;
long null_ARRAY_006119c_8_4_;
long local_68_2_2_;
long local_6b_1_3_;
long local_6c_4_4_;
long local_6e_4_4_;
long local_6f_4_4_;
long local_70_0_2_;
long local_70_4_4_;
long local_71_0_1_;
long local_71_4_4_;
long local_72_4_4_;
long DAT_00000010;
long DAT_0040e000;
long DAT_0040e01e;
long DAT_0040e022;
long DAT_0040e024;
long DAT_0040e060;
long DAT_0040e065;
long DAT_0040e071;
long DAT_0040e076;
long DAT_0040e07a;
long DAT_0040e08d;
long DAT_0040e092;
long DAT_0040e097;
long DAT_0040e09c;
long DAT_0040e0a9;
long DAT_0040e0ad;
long DAT_0040e0fd;
long DAT_0040e102;
long DAT_0040e124;
long DAT_0040e1a8;
long DAT_0040ead8;
long DAT_0040eb20;
long DAT_0040eb24;
long DAT_0040eb28;
long DAT_0040eb2b;
long DAT_0040eb2d;
long DAT_0040eb31;
long DAT_0040eb35;
long DAT_0040f2eb;
long DAT_0040f695;
long DAT_0040f799;
long DAT_0040f79f;
long DAT_0040f7b1;
long DAT_0040f7b2;
long DAT_0040f7d0;
long DAT_0040f856;
long DAT_00611210;
long DAT_00611220;
long DAT_00611230;
long DAT_00611640;
long DAT_00611648;
long DAT_00611658;
long DAT_006116b0;
long DAT_006116b4;
long DAT_006116b8;
long DAT_006116bc;
long DAT_00611700;
long DAT_00611710;
long DAT_00611720;
long DAT_00611728;
long DAT_00611740;
long DAT_00611748;
long DAT_006117a0;
long DAT_006117a8;
long DAT_006117b0;
long DAT_006117e1;
long DAT_006117e8;
long DAT_006117f0;
long DAT_006117f8;
long DAT_00611800;
long DAT_00611808;
long DAT_00611809;
long DAT_0061180a;
long DAT_0061180b;
long DAT_0061180c;
long DAT_0061180d;
long DAT_0061180e;
long DAT_0061180f;
long DAT_00611810;
long DAT_00611811;
long DAT_00611812;
long DAT_00611813;
long DAT_00611814;
long DAT_00611815;
long DAT_00611816;
long DAT_00611850;
long DAT_00611858;
long DAT_00611860;
long DAT_00611868;
long DAT_00611a38;
long DAT_00611a40;
long DAT_00611a48;
long DAT_00611a58;
long fde_00410400;
long null_ARRAY_0040e880;
long null_ARRAY_0040fc60;
long null_ARRAY_0040fea0;
long null_ARRAY_006116a0;
long null_ARRAY_00611760;
long null_ARRAY_006117c0;
long null_ARRAY_00611820;
long null_ARRAY_00611880;
long null_ARRAY_00611980;
long null_ARRAY_006119c0;
long null_ARRAY_00611a00;
long PTR_DAT_00611650;
long PTR_null_ARRAY_00611698;
long PTR_null_ARRAY_006116c0;
long stack0x00000008;
void
FUN_004029ea (uint uParm1)
{
  int iVar1;
  long lVar2;
  undefined8 uVar3;
  undefined **ppuVar4;
  undefined *puVar5;
  undefined *local_88;
  char *local_80;
  undefined8 local_78;
  char *local_70;
  char *local_68;
  char *local_60;
  char *local_58;
  char *local_50;
  char *local_48;
  char *local_40;
  char *local_38;
  char *local_30;
  undefined8 local_28;
  undefined8 local_20;

  ppuVar4 = &local_88;
  if (uParm1 != 0)
    {
      func_0x00401900 (DAT_00611720,
		       "Try \'%s --help\' for more information.\n",
		       DAT_00611868);
      goto LAB_00402c02;
    }
  func_0x00401770 ("Usage: %s [OPTION]... [ FILE | ARG1 ARG2 ]\n",
		   DAT_00611868);
  uVar3 = DAT_00611700;
  func_0x00401920
    ("Print information about users who are currently logged in.\n",
     DAT_00611700);
  func_0x00401920
    ("\n  -a, --all         same as -b -d --login -p -r -t -T -u\n  -b, --boot        time of last system boot\n  -d, --dead        print dead processes\n  -H, --heading     print line of column headings\n",
     uVar3);
  func_0x00401920 ("  -l, --login       print system login processes\n",
		   uVar3);
  func_0x00401920
    ("      --lookup      attempt to canonicalize hostnames via DNS\n  -m                only hostname and user associated with stdin\n  -p, --process     print active processes spawned by init\n",
     uVar3);
  func_0x00401920
    ("  -q, --count       all login names and number of users logged on\n  -r, --runlevel    print current runlevel\n  -s, --short       print only name, line, and time (default)\n  -t, --time        print last system clock change\n",
     uVar3);
  func_0x00401920
    ("  -T, -w, --mesg    add user\'s message status as +, - or ?\n  -u, --users       list users logged in\n      --message     same as -T\n      --writable    same as -T\n",
     uVar3);
  func_0x00401920 ("      --help     display this help and exit\n", uVar3);
  func_0x00401920 ("      --version  output version information and exit\n",
		   uVar3);
  func_0x00401770
    ("\nIf FILE is not specified, use %s.  %s as FILE is common.\nIf ARG1 ARG2 given, -m presumed: \'am i\' or \'mom likes\' are usual.\n",
     "/dev/null/utmp", "/dev/null/wtmp");
  local_88 = &DAT_0040e124;
  local_80 = "test invocation";
  local_78 = 0x40e187;
  local_70 = "Multi-call invocation";
  local_68 = "sha224sum";
  local_60 = "sha2 utilities";
  local_58 = "sha256sum";
  local_50 = "sha2 utilities";
  local_48 = "sha384sum";
  local_40 = "sha2 utilities";
  local_38 = "sha512sum";
  local_30 = "sha2 utilities";
  local_28 = 0;
  local_20 = 0;
  puVar5 = &DAT_0040e124;
  do
    {
      iVar1 = func_0x00401a50 (&DAT_0040e102, puVar5);
      if (iVar1 == 0)
	break;
      ppuVar4 = ppuVar4 + 2;
      puVar5 = *ppuVar4;
    }
  while (puVar5 != (undefined *) 0x0);
  puVar5 = ppuVar4[1];
  if (puVar5 == (undefined *) 0x0)
    {
      func_0x00401770 ("\n%s online help: <%s>\n", "GNU coreutils",
		       "https://www.gnu.org/software/coreutils/");
      lVar2 = func_0x00401ac0 (5, 0);
      if (lVar2 == 0)
	goto LAB_00402c09;
      iVar1 = func_0x00401970 (lVar2, &DAT_0040e1a8, 3);
      if (iVar1 == 0)
	{
	  func_0x00401770 ("Full documentation at: <%s%s>\n",
			   "https://www.gnu.org/software/coreutils/",
			   &DAT_0040e102);
	  puVar5 = &DAT_0040e102;
	  uVar3 = 0x40e140;
	  goto LAB_00402bf0;
	}
      puVar5 = &DAT_0040e102;
    LAB_00402bae:
      ;
      func_0x00401770
	("Report %s translation bugs to <https://translationproject.org/team/>\n",
	 &DAT_0040e102);
    }
  else
    {
      func_0x00401770 ("\n%s online help: <%s>\n", "GNU coreutils",
		       "https://www.gnu.org/software/coreutils/");
      lVar2 = func_0x00401ac0 (5, 0);
      if ((lVar2 != 0)
	  && (iVar1 = func_0x00401970 (lVar2, &DAT_0040e1a8, 3), iVar1 != 0))
	goto LAB_00402bae;
    }
  func_0x00401770 ("Full documentation at: <%s%s>\n",
		   "https://www.gnu.org/software/coreutils/", &DAT_0040e102);
  uVar3 = 0x40f7cf;
  if (puVar5 == &DAT_0040e102)
    {
      uVar3 = 0x40e140;
    }
LAB_00402bf0:
  ;
  do
    {
      func_0x00401770
	("or available locally via: info \'(coreutils) %s%s\'\n", puVar5,
	 uVar3);
    LAB_00402c02:
      ;
      func_0x00401b30 ((ulong) uParm1);
    LAB_00402c09:
      ;
      func_0x00401770 ("Full documentation at: <%s%s>\n",
		       "https://www.gnu.org/software/coreutils/",
		       &DAT_0040e102);
      puVar5 = &DAT_0040e102;
      uVar3 = 0x40e140;
    }
  while (true);
}

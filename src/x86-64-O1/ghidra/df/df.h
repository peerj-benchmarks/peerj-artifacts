typedef unsigned char   undefined;

typedef unsigned char    byte;
typedef unsigned char    dwfenc;
typedef unsigned int    dword;
typedef unsigned long    qword;
typedef unsigned char    undefined1;
typedef unsigned short    undefined2;
typedef unsigned int    undefined4;
typedef unsigned long    undefined6;
typedef unsigned long    undefined8;
typedef unsigned short    word;
#define unkbyte9   unsigned long long
#define unkbyte10   unsigned long long
#define unkbyte11   unsigned long long
#define unkbyte12   unsigned long long
#define unkbyte13   unsigned long long
#define unkbyte14   unsigned long long
#define unkbyte15   unsigned long long
#define unkbyte16   unsigned long long

#define unkuint9   unsigned long long
#define unkuint10   unsigned long long
#define unkuint11   unsigned long long
#define unkuint12   unsigned long long
#define unkuint13   unsigned long long
#define unkuint14   unsigned long long
#define unkuint15   unsigned long long
#define unkuint16   unsigned long long

#define unkint9   long long
#define unkint10   long long
#define unkint11   long long
#define unkint12   long long
#define unkint13   long long
#define unkint14   long long
#define unkint15   long long
#define unkint16   long long

#define unkfloat1   float
#define unkfloat2   float
#define unkfloat3   float
#define unkfloat5   double
#define unkfloat6   double
#define unkfloat7   double
#define unkfloat9   long double
#define unkfloat11   long double
#define unkfloat12   long double
#define unkfloat13   long double
#define unkfloat14   long double
#define unkfloat15   long double
#define unkfloat16   long double

#define BADSPACEBASE   void
#define code   void

#define ulong long
#define true 1
#define bool int
#define false 0
#define float10 int
#define ushort int
#define undefined7 long
#define undefined2 long
#define uint5 long
#define uint6 long

// Add definition for uint
typedef unsigned int    uint;

#define sbyte char

extern imperfection_wrapper(void);
typedef struct eh_frame_hdr eh_frame_hdr, *Peh_frame_hdr;

struct eh_frame_hdr {
    byte eh_frame_hdr_version; // Exception Handler Frame Header Version
    dwfenc eh_frame_pointer_encoding; // Exception Handler Frame Pointer Encoding
    dwfenc eh_frame_desc_entry_count_encoding; // Encoding of # of Exception Handler FDEs
    dwfenc eh_frame_table_encoding; // Exception Handler Table Encoding
};

typedef struct fde_table_entry fde_table_entry, *Pfde_table_entry;

struct fde_table_entry {
    dword initial_loc; // Initial Location
    dword data_loc; // Data location
};

typedef enum Elf64_DynTag {
    DT_AUDIT=1879047932,
    DT_AUXILIARY=2147483645,
    DT_BIND_NOW=24,
    DT_CHECKSUM=1879047672,
    DT_CONFIG=1879047930,
    DT_DEBUG=21,
    DT_DEPAUDIT=1879047931,
    DT_ENCODING=32,
    DT_FEATURE_1=1879047676,
    DT_FILTER=2147483647,
    DT_FINI=13,
    DT_FINI_ARRAY=26,
    DT_FINI_ARRAYSZ=28,
    DT_FLAGS=30,
    DT_FLAGS_1=1879048187,
    DT_GNU_CONFLICT=1879047928,
    DT_GNU_CONFLICTSZ=1879047670,
    DT_GNU_HASH=1879047925,
    DT_GNU_LIBLIST=1879047929,
    DT_GNU_LIBLISTSZ=1879047671,
    DT_GNU_PRELINKED=1879047669,
    DT_HASH=4,
    DT_INIT=12,
    DT_INIT_ARRAY=25,
    DT_INIT_ARRAYSZ=27,
    DT_JMPREL=23,
    DT_MOVEENT=1879047674,
    DT_MOVESZ=1879047675,
    DT_MOVETAB=1879047934,
    DT_NEEDED=1,
    DT_NULL=0,
    DT_PLTGOT=3,
    DT_PLTPAD=1879047933,
    DT_PLTPADSZ=1879047673,
    DT_PLTREL=20,
    DT_PLTRELSZ=2,
    DT_POSFLAG_1=1879047677,
    DT_PREINIT_ARRAYSZ=33,
    DT_REL=17,
    DT_RELA=7,
    DT_RELACOUNT=1879048185,
    DT_RELAENT=9,
    DT_RELASZ=8,
    DT_RELCOUNT=1879048186,
    DT_RELENT=19,
    DT_RELSZ=18,
    DT_RPATH=15,
    DT_RUNPATH=29,
    DT_SONAME=14,
    DT_STRSZ=10,
    DT_STRTAB=5,
    DT_SYMBOLIC=16,
    DT_SYMENT=11,
    DT_SYMINENT=1879047679,
    DT_SYMINFO=1879047935,
    DT_SYMINSZ=1879047678,
    DT_SYMTAB=6,
    DT_TEXTREL=22,
    DT_TLSDESC_GOT=1879047927,
    DT_TLSDESC_PLT=1879047926,
    DT_VERDEF=1879048188,
    DT_VERDEFNUM=1879048189,
    DT_VERNEED=1879048190,
    DT_VERNEEDNUM=1879048191,
    DT_VERSYM=1879048176
} Elf64_DynTag;

typedef enum Elf_ProgramHeaderType {
    PT_DYNAMIC=2,
    PT_GNU_EH_FRAME=1685382480,
    PT_GNU_RELRO=1685382482,
    PT_GNU_STACK=1685382481,
    PT_INTERP=3,
    PT_LOAD=1,
    PT_NOTE=4,
    PT_NULL=0,
    PT_PHDR=6,
    PT_SHLIB=5,
    PT_TLS=7
} Elf_ProgramHeaderType;

typedef struct Elf64_Dyn Elf64_Dyn, *PElf64_Dyn;

struct Elf64_Dyn {
    enum Elf64_DynTag d_tag;
    qword d_val;
};

typedef struct Elf64_Shdr Elf64_Shdr, *PElf64_Shdr;

typedef enum Elf_SectionHeaderType {
    SHT_CHECKSUM=1879048184,
    SHT_DYNAMIC=6,
    SHT_DYNSYM=11,
    SHT_FINI_ARRAY=15,
    SHT_GNU_ATTRIBUTES=1879048181,
    SHT_GNU_HASH=1879048182,
    SHT_GNU_LIBLIST=1879048183,
    SHT_GNU_verdef=1879048189,
    SHT_GNU_verneed=1879048190,
    SHT_GNU_versym=1879048191,
    SHT_GROUP=17,
    SHT_HASH=5,
    SHT_INIT_ARRAY=14,
    SHT_NOBITS=8,
    SHT_NOTE=7,
    SHT_NULL=0,
    SHT_PREINIT_ARRAY=16,
    SHT_PROGBITS=1,
    SHT_REL=9,
    SHT_RELA=4,
    SHT_SHLIB=10,
    SHT_STRTAB=3,
    SHT_SUNW_COMDAT=1879048187,
    SHT_SUNW_move=1879048186,
    SHT_SUNW_syminfo=1879048188,
    SHT_SYMTAB=2,
    SHT_SYMTAB_SHNDX=18
} Elf_SectionHeaderType;

struct Elf64_Shdr {
    dword sh_name;
    enum Elf_SectionHeaderType sh_type;
    qword sh_flags;
    qword sh_addr;
    qword sh_offset;
    qword sh_size;
    dword sh_link;
    dword sh_info;
    qword sh_addralign;
    qword sh_entsize;
};

typedef struct Elf64_Phdr Elf64_Phdr, *PElf64_Phdr;

struct Elf64_Phdr {
    enum Elf_ProgramHeaderType p_type;
    dword p_flags;
    qword p_offset;
    qword p_vaddr;
    qword p_paddr;
    qword p_filesz;
    qword p_memsz;
    qword p_align;
};

typedef struct Elf64_Rela Elf64_Rela, *PElf64_Rela;

struct Elf64_Rela {
    qword r_offset; // location to apply the relocation action
    qword r_info; // the symbol table index and the type of relocation
    qword r_addend; // a constant addend used to compute the relocatable field value
};

typedef struct Elf64_Sym Elf64_Sym, *PElf64_Sym;

struct Elf64_Sym {
    dword st_name;
    byte st_info;
    byte st_other;
    word st_shndx;
    qword st_value;
    qword st_size;
};

typedef struct Elf64_Ehdr Elf64_Ehdr, *PElf64_Ehdr;

struct Elf64_Ehdr {
    byte e_ident_magic_num;
    char e_ident_magic_str[3];
    byte e_ident_class;
    byte e_ident_data;
    byte e_ident_version;
    byte e_ident_pad[9];
    word e_type;
    word e_machine;
    dword e_version;
    qword e_entry;
    qword e_phoff;
    qword e_shoff;
    dword e_flags;
    word e_ehsize;
    word e_phentsize;
    word e_phnum;
    word e_shentsize;
    word e_shnum;
    word e_shstrndx;
};




undefined8 _DT_INIT(void);
void FUN_00401c70(void);
void entry(void);
void FUN_004022f0(void);
void FUN_00402370(void);
void FUN_004023f0(void);
ulong FUN_0040242e(ulong *puParm1,ulong uParm2);
ulong FUN_0040243d(ulong *puParm1,ulong *puParm2);
ulong FUN_00402447(long lParm1);
ulong FUN_00402494(long lParm1);
void FUN_004024e9(void);
void FUN_004024f7(void);
void FUN_0040256a(int iParm1,long lParm2);
byte * FUN_00402631(byte *pbParm1);
undefined * FUN_00402668(byte bParm1,ulong uParm2,long lParm3);
void FUN_004026b9(char *param_1,char *param_2,undefined *param_3,char *param_4,undefined *param_5,char param_6,char param_7,long *param_8,char param_9);
void FUN_004030ad(uint uParm1);
void FUN_00403385(void);
ulong FUN_004034d6(undefined4 uParm1,undefined8 *puParm2);
undefined8 FUN_00404adf(undefined8 uParm1,long lParm2);
char * FUN_00404d7b(char *pcParm1,uint uParm2);
void FUN_00405315(undefined8 uParm1);
void FUN_00405328(void);
void FUN_004053ce(void);
ulong FUN_004053e6(char *pcParm1);
undefined * FUN_0040543d(undefined8 uParm1);
char * FUN_004054a1(char *pcParm1);
void FUN_004054e7(long lParm1,undefined8 uParm2,undefined8 *puParm3);
ulong FUN_00405555(long lParm1,undefined8 uParm2,undefined8 *puParm3);
ulong FUN_0040558c(ulong uParm1,ulong uParm2);
undefined FUN_0040559f(long lParm1,long lParm2);;
ulong FUN_004055a6(long lParm1,undefined8 uParm2,uint uParm3);
ulong FUN_0040562a(ulong uParm1,long lParm2);
long FUN_00405746(long *plParm1,undefined8 uParm2);
undefined8 FUN_00405768(long lParm1,long **pplParm2,char cParm3);
long FUN_0040588c(long lParm1,long lParm2,long **pplParm3,char cParm4);
long FUN_00405990(long lParm1,long lParm2);
long * FUN_004059ec(undefined8 uParm1,undefined1 *puParm2,code *pcParm3,code *pcParm4,long lParm5);
void FUN_00405ad7(long **pplParm1);
ulong FUN_00405b7c(long *plParm1,undefined8 uParm2);
undefined8 FUN_00405cb9(long lParm1,long lParm2,long *plParm3);
undefined8 FUN_00405ee6(undefined8 uParm1,undefined8 uParm2);
ulong FUN_00405f15(undefined8 *puParm1,ulong uParm2);
ulong FUN_00405f3f(undefined8 *puParm1,undefined8 *puParm2);
void FUN_00405f7b(undefined8 *puParm1);
void FUN_00405f91(void);
char * FUN_00406066(ulong uParm1,long lParm2,ulong uParm3,ulong uParm4,ulong uParm5);
undefined8 FUN_00406788(char *pcParm1,uint *puParm2,long *plParm3);
char * FUN_004068dc(ulong uParm1,long lParm2);
undefined *FUN_00406918(long lParm1,undefined *puParm2,long lParm3,undefined8 *puParm4,int iParm5,uint uParm6);
long FUN_00406cb1(undefined8 uParm1,ulong *puParm2,uint uParm3,uint uParm4);
ulong FUN_00406d48(byte *pbParm1,long lParm2,ulong uParm3);
void FUN_00406f2b(undefined8 uParm1,ulong uParm2);
void FUN_00406f4f(long lParm1);
int * FUN_00406fe4(int *piParm1,int iParm2);
undefined * FUN_00407014(char *pcParm1,int iParm2);
ulong FUN_004070cc(undefined *param_1,ulong param_2,long param_3,ulong param_4,uint param_5,ulong param_6,long param_7,char *param_8,char *param_9);
undefined1 * FUN_00407e83(int iParm1,undefined8 uParm2,undefined8 uParm3,uint *puParm4);
ulong FUN_00408025(undefined1 *puParm1,byte bParm2,undefined8 uParm3);
void FUN_00408059(uint uParm1,undefined8 uParm2,undefined8 uParm3);
void FUN_00408087(ulong uParm1,undefined8 uParm2);
void FUN_0040809f(undefined8 uParm1,undefined8 uParm2,char cParm3);
void FUN_00408128(undefined8 uParm1,char cParm2);
void FUN_00408141(undefined8 uParm1);
void FUN_00408154(uint uParm1,undefined8 uParm2,undefined8 uParm3);
void FUN_004081e3(void);
void FUN_004081f6(undefined8 uParm1,undefined8 uParm2);
void FUN_0040820b(undefined8 uParm1);
ulong FUN_00408221(int *piParm1);
void FUN_0040826e(uint *puParm1);
void FUN_0040828f(int *piParm1);
void FUN_004082a9(undefined8 uParm1,long lParm2,undefined8 uParm3,undefined8 uParm4,undefined8 *puParm5,undefined8 uParm6);
void FUN_004084e6(void);
void FUN_00408538(void);
void FUN_004085bd(long lParm1);
long FUN_004085d7(long lParm1,long lParm2);
void FUN_0040860a(undefined8 uParm1,undefined8 uParm2);
void FUN_00408633(undefined8 uParm1);
long FUN_0040864a(void);
long FUN_00408672(void);
ulong FUN_0040869e(uint uParm1);
ulong FUN_00408721(ulong *puParm1,int iParm2,int iParm3);
ulong FUN_0040877f(byte *pbParm1,byte **ppbParm2,uint uParm3,ulong *puParm4,long lParm5);
void FUN_00408ac8(undefined4 uParm1,undefined4 uParm2,undefined4 uParm3,undefined4 uParm4,undefined4 uParm5,undefined4 uParm6,undefined4 uParm7,undefined4 uParm8,undefined8 param_1,undefined8 param_2,undefined8 param_3,undefined8 param_4,undefined8 param_5,undefined8 param_6);
undefined8 FUN_00408b57(ulong uParm1,ulong uParm2,ulong uParm3);
void FUN_00408b9f(int *piParm1);
undefined8 FUN_00408bd1(uint *puParm1,undefined8 uParm2);
ulong FUN_00408c0e(char *pcParm1);
void FUN_00408e6b(void);
void FUN_00408e98(undefined8 uParm1);
void FUN_00408ed8(uint uParm1,uint uParm2,undefined8 uParm3,undefined8 uParm4);
void FUN_00408f2f(undefined4 uParm1,undefined4 uParm2,undefined4 uParm3,undefined4 uParm4,undefined4 uParm5,undefined4 uParm6,undefined4 uParm7,undefined4 uParm8,ulong param_1,ulong param_2,undefined8 param_3,undefined8 param_4,undefined8 param_5,undefined8 param_6);
undefined8 FUN_00409002(undefined8 uParm1,undefined8 uParm2,long *plParm3);
long FUN_0040906c(long lParm1,ulong uParm2);
void FUN_004094fc(long lParm1,int *piParm2);
ulong FUN_004095bb(int param_1,undefined8 *param_2,char *param_3,long *param_4,int *param_5,int param_6,int *param_7,int param_8,undefined8 param_9);
ulong FUN_00409a76(uint param_1,undefined8 *param_2,char *param_3,long param_4,undefined8 param_5,ulong param_6,uint *param_7,int param_8);
void FUN_00409ef2(void);
void FUN_00409f48(void);
ulong FUN_00409f5e(long lParm1,long lParm2);
void FUN_00409fc7(long lParm1);
ulong FUN_00409fe1(uint *puParm1,byte *pbParm2,long lParm3);
ulong * FUN_0040a046(ulong *puParm1,char cParm2,ulong uParm3);
void FUN_0040a13e(long lParm1);
void FUN_0040a1c3(undefined8 *puParm1);
char * FUN_0040a1fa(void);
ulong FUN_0040a8ba(uint param_1,undefined8 param_2,uint param_3,uint param_4);
void FUN_0040a9aa(long lParm1,long lParm2);
ulong FUN_0040a9d8(long lParm1,long lParm2);
ulong FUN_0040aa24(long lParm1,ulong uParm2,long *plParm3);
long FUN_0040ab39(long lParm1,long lParm2,long lParm3,ulong uParm4);
char * FUN_0040aef8(char *pcParm1,char *pcParm2);
ulong FUN_0040b26a(long *plParm1,undefined8 uParm2,undefined8 uParm3);
ulong FUN_0040b2c6(undefined8 uParm1,undefined8 uParm2,undefined8 uParm3);
ulong FUN_0040b393(int *piParm1,long lParm2);
void FUN_0040b40a(ulong uParm1);
long FUN_0040b44a(undefined8 uParm1,ulong uParm2);
long FUN_0040b529(undefined8 uParm1,long *plParm2,long lParm3,long lParm4);
ulong FUN_0040b619(ulong uParm1,char cParm2);
ulong FUN_0040b679(undefined8 uParm1);
void FUN_0040b6da(undefined8 param_1,byte param_2,ulong param_3);
undefined8 FUN_0040b724(void);
ulong FUN_0040b72c(ulong uParm1);
ulong FUN_0040b7ba(char *pcParm1,ulong uParm2);
char * FUN_0040b7ef(void);
ulong FUN_0040bb2a(uint uParm1);
ulong FUN_0040bb5c(long lParm1,long lParm2,char cParm3);
undefined8 FUN_0040bb8e(long param_1,undefined8 param_2,char param_3,char param_4,char param_5,char param_6,char param_7,char param_8,char param_9);
ulong FUN_0040bc0e(uint uParm1,char *pcParm2);
undefined8 FUN_0040bfc1(undefined8 uParm1);
ulong FUN_0040c044(uint param_1,undefined8 param_2,undefined8 param_3,undefined8 param_4);
undefined8 FUN_0040c1b3(ulong uParm1);
ulong FUN_0040c202(long lParm1);
undefined8 FUN_0040c292(void);
void FUN_0040c2a5(void);
undefined8 FUN_0040c2b3(ulong uParm1,long lParm2,long lParm3,ulong uParm4);
void FUN_0040c37f(undefined8 uParm1,undefined8 uParm2,undefined8 uParm3);
undefined8 FUN_0040c395(long lParm1,undefined8 uParm2,undefined8 uParm3);
ulong FUN_0040c3f7(char *pcParm1,long lParm2);
long FUN_0040c43e(ulong uParm1,uint *puParm2,ulong uParm3,uint *puParm4,long *plParm5);
ulong FUN_0040c562(void);
ulong FUN_0040c594(void);
int * FUN_0040c7a1(ulong uParm1,long lParm2,ulong uParm3,uint *puParm4,ulong *puParm5);
char * FUN_0040cd84(int iParm1,long lParm2,long lParm3,long lParm4,uint uParm5);
void FUN_0040d39c(uint param_1,undefined8 param_2);
ulong FUN_0040d562(void);
void FUN_0040d775(undefined8 uParm1,uint uParm2);
undefined8 *FUN_0040d916(undefined8 *puParm1,undefined8 *puParm2,undefined8 *puParm3,undefined8 uParm4);
double FUN_004134b3(int *piParm1);
void FUN_004134fb(int *param_1);
void FUN_0041357c(undefined8 uParm1);
ulong FUN_00413599(long *plParm1,ulong *puParm2,int iParm3,long lParm4);
undefined8 FUN_004136c1(uint *puParm1,ulong *puParm2);
undefined8 FUN_00413ae6(byte *pbParm1,ulong *puParm2,byte **ppbParm3);
ulong FUN_0041464b(void);
ulong FUN_00414673(void);
void FUN_004146b0(void);
undefined8 _DT_FINI(void);
undefined FUN_0061b000();
undefined FUN_0061b008();
undefined FUN_0061b010();
undefined FUN_0061b018();
undefined FUN_0061b020();
undefined FUN_0061b028();
undefined FUN_0061b030();
undefined FUN_0061b038();
undefined FUN_0061b040();
undefined FUN_0061b048();
undefined FUN_0061b050();
undefined FUN_0061b058();
undefined FUN_0061b060();
undefined FUN_0061b068();
undefined FUN_0061b070();
undefined FUN_0061b078();
undefined FUN_0061b080();
undefined FUN_0061b088();
undefined FUN_0061b090();
undefined FUN_0061b098();
undefined FUN_0061b0a0();
undefined FUN_0061b0a8();
undefined FUN_0061b0b0();
undefined FUN_0061b0b8();
undefined FUN_0061b0c0();
undefined FUN_0061b0c8();
undefined FUN_0061b0d0();
undefined FUN_0061b0d8();
undefined FUN_0061b0e0();
undefined FUN_0061b0e8();
undefined FUN_0061b0f0();
undefined FUN_0061b0f8();
undefined FUN_0061b100();
undefined FUN_0061b108();
undefined FUN_0061b110();
undefined FUN_0061b118();
undefined FUN_0061b120();
undefined FUN_0061b128();
undefined FUN_0061b130();
undefined FUN_0061b138();
undefined FUN_0061b140();
undefined FUN_0061b148();
undefined FUN_0061b150();
undefined FUN_0061b158();
undefined FUN_0061b160();
undefined FUN_0061b168();
undefined FUN_0061b170();
undefined FUN_0061b178();
undefined FUN_0061b180();
undefined FUN_0061b188();
undefined FUN_0061b190();
undefined FUN_0061b198();
undefined FUN_0061b1a0();
undefined FUN_0061b1a8();
undefined FUN_0061b1b0();
undefined FUN_0061b1b8();
undefined FUN_0061b1c0();
undefined FUN_0061b1c8();
undefined FUN_0061b1d0();
undefined FUN_0061b1d8();
undefined FUN_0061b1e0();
undefined FUN_0061b1e8();
undefined FUN_0061b1f0();
undefined FUN_0061b1f8();
undefined FUN_0061b200();
undefined FUN_0061b208();
undefined FUN_0061b210();
undefined FUN_0061b218();
undefined FUN_0061b220();
undefined FUN_0061b228();
undefined FUN_0061b230();
undefined FUN_0061b238();
undefined FUN_0061b240();
undefined FUN_0061b248();
undefined FUN_0061b250();
undefined FUN_0061b258();
undefined FUN_0061b260();
undefined FUN_0061b268();
undefined FUN_0061b270();
undefined FUN_0061b278();
undefined FUN_0061b280();
undefined FUN_0061b288();
undefined FUN_0061b290();
undefined FUN_0061b298();
undefined FUN_0061b2a0();
undefined FUN_0061b2a8();
undefined FUN_0061b2b0();
undefined FUN_0061b2b8();
undefined FUN_0061b2c0();
undefined FUN_0061b2c8();
undefined FUN_0061b2d0();
undefined FUN_0061b2d8();
undefined FUN_0061b2e0();
undefined FUN_0061b2e8();
undefined FUN_0061b2f0();
undefined FUN_0061b2f8();
undefined FUN_0061b300();
undefined FUN_0061b308();
undefined FUN_0061b310();


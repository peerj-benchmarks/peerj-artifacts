
#include "df.h"

long null_ARRAY_0061a82_0_8_;
long null_ARRAY_0061a82_8_8_;
long null_ARRAY_0061a92_0_8_;
long null_ARRAY_0061a92_16_8_;
long null_ARRAY_0061a92_24_8_;
long null_ARRAY_0061a92_40_8_;
long null_ARRAY_0061a92_48_8_;
long null_ARRAY_0061a92_8_8_;
long null_ARRAY_0061aac_0_8_;
long null_ARRAY_0061aac_16_8_;
long null_ARRAY_0061aac_24_8_;
long null_ARRAY_0061aac_32_8_;
long null_ARRAY_0061aac_40_8_;
long null_ARRAY_0061aac_48_8_;
long null_ARRAY_0061aac_8_8_;
long null_ARRAY_0061ab0_0_4_;
long null_ARRAY_0061ab0_16_8_;
long null_ARRAY_0061ab0_4_4_;
long null_ARRAY_0061ab0_8_4_;
long local_68_2_2_;
long local_6b_1_3_;
long local_6c_4_4_;
long local_6e_4_4_;
long local_6f_4_4_;
long local_70_0_2_;
long local_70_4_4_;
long local_71_0_1_;
long local_71_4_4_;
long local_72_4_4_;
long DAT_00000010;
long DAT_00414769;
long DAT_0041476c;
long DAT_0041476f;
long DAT_004147f3;
long DAT_004147f7;
long DAT_00414839;
long DAT_0041483c;
long DAT_0041483f;
long DAT_0041488e;
long DAT_004148c0;
long DAT_004158ab;
long DAT_004158ac;
long DAT_00415990;
long DAT_00415a28;
long DAT_00415a2e;
long DAT_00415a30;
long DAT_00415a34;
long DAT_00415a38;
long DAT_00415a3b;
long DAT_00415a3f;
long DAT_00415a43;
long DAT_004161eb;
long DAT_004165b8;
long DAT_004165b9;
long DAT_0041698d;
long DAT_00416997;
long DAT_004169e4;
long DAT_00416b0d;
long DAT_00416b26;
long DAT_00416b47;
long DAT_00416b95;
long DAT_00416ba0;
long DAT_00416bb6;
long DAT_00416be0;
long DAT_00416c67;
long DAT_00417706;
long DAT_0061a0c0;
long DAT_0061a0d0;
long DAT_0061a0e0;
long DAT_0061a7c8;
long DAT_0061a830;
long DAT_0061a834;
long DAT_0061a838;
long DAT_0061a83c;
long DAT_0061a840;
long DAT_0061a850;
long DAT_0061a860;
long DAT_0061a868;
long DAT_0061a880;
long DAT_0061a888;
long DAT_0061a8e0;
long DAT_0061a8e8;
long DAT_0061a8f0;
long DAT_0061a8f8;
long DAT_0061a900;
long DAT_0061a958;
long DAT_0061a959;
long DAT_0061a960;
long DAT_0061a968;
long DAT_0061a970;
long DAT_0061a978;
long DAT_0061a97c;
long DAT_0061a97d;
long DAT_0061a980;
long DAT_0061a988;
long DAT_0061a98c;
long DAT_0061a98d;
long DAT_0061a98e;
long DAT_0061a990;
long DAT_0061a998;
long DAT_0061a9a0;
long DAT_0061a9a8;
long DAT_0061ab38;
long DAT_0061ab40;
long DAT_0061ab48;
long DAT_0061ab50;
long DAT_0061ab58;
long DAT_0061ab68;
long fde_004184f8;
long null_ARRAY_00414b00;
long null_ARRAY_00415930;
long null_ARRAY_004159a0;
long null_ARRAY_004159b8;
long null_ARRAY_00416c80;
long null_ARRAY_00416d80;
long null_ARRAY_00417b20;
long null_ARRAY_00417d30;
long null_ARRAY_0061a580;
long null_ARRAY_0061a820;
long null_ARRAY_0061a8a0;
long null_ARRAY_0061a920;
long null_ARRAY_0061a9c0;
long null_ARRAY_0061aac0;
long null_ARRAY_0061ab00;
long PTR_DAT_0061a7c0;
long PTR_null_ARRAY_0061a818;
void
FUN_004030ad (uint uParm1)
{
  int iVar1;
  long lVar2;
  undefined8 uVar3;
  undefined **ppuVar4;
  undefined *puVar5;
  undefined *local_88;
  char *local_80;
  undefined8 local_78;
  char *local_70;
  char *local_68;
  char *local_60;
  char *local_58;
  char *local_50;
  char *local_48;
  char *local_40;
  char *local_38;
  char *local_30;
  undefined8 local_28;
  undefined8 local_20;

  ppuVar4 = &local_88;
  if (uParm1 != 0)
    {
      func_0x00401f00 (DAT_0061a860,
		       "Try \'%s --help\' for more information.\n",
		       DAT_0061a9a8);
      goto LAB_004032e7;
    }
  func_0x00401ca0 ("Usage: %s [OPTION]... [FILE]...\n", DAT_0061a9a8);
  uVar3 = DAT_0061a840;
  func_0x00401f10
    ("Show information about the file system on which each FILE resides,\nor all file systems by default.\n",
     DAT_0061a840);
  func_0x00401f10
    ("\nMandatory arguments to long options are mandatory for short options too.\n",
     uVar3);
  func_0x00401f10
    ("  -a, --all             include pseudo, duplicate, inaccessible file systems\n  -B, --block-size=SIZE  scale sizes by SIZE before printing them; e.g.,\n                           \'-BM\' prints sizes in units of 1,048,576 bytes;\n                           see SIZE format below\n  -h, --human-readable  print sizes in powers of 1024 (e.g., 1023M)\n  -H, --si              print sizes in powers of 1000 (e.g., 1.1G)\n",
     uVar3);
  func_0x00401f10
    ("  -i, --inodes          list inode information instead of block usage\n  -k                    like --block-size=1K\n  -l, --local           limit listing to local file systems\n      --no-sync         do not invoke sync before getting usage info (default)\n",
     uVar3);
  func_0x00401f10
    ("      --output[=FIELD_LIST]  use the output format defined by FIELD_LIST,\n                               or print all fields if FIELD_LIST is omitted.\n  -P, --portability     use the POSIX output format\n      --sync            invoke sync before getting usage info\n",
     uVar3);
  func_0x00401f10
    ("      --total           elide all entries insignificant to available space,\n                          and produce a grand total\n",
     uVar3);
  func_0x00401f10
    ("  -t, --type=TYPE       limit listing to file systems of type TYPE\n  -T, --print-type      print file system type\n  -x, --exclude-type=TYPE   limit listing to file systems not of type TYPE\n  -v                    (ignored)\n",
     uVar3);
  func_0x00401f10 ("      --help     display this help and exit\n", uVar3);
  func_0x00401f10 ("      --version  output version information and exit\n",
		   uVar3);
  func_0x00401ca0
    ("\nDisplay values are in units of the first available SIZE from --block-size,\nand the %s_BLOCK_SIZE, BLOCK_SIZE and BLOCKSIZE environment variables.\nOtherwise, units default to 1024 bytes (or 512 if POSIXLY_CORRECT is set).\n",
     &DAT_0041476c);
  func_0x00401f10
    ("\nThe SIZE argument is an integer and optional unit (example: 10K is 10*1024).\nUnits are K,M,G,T,P,E,Z,Y (powers of 1024) or KB,MB,... (powers of 1000).\n",
     uVar3);
  func_0x00401f10
    ("\nFIELD_LIST is a comma-separated list of columns to be included.  Valid\nfield names are: \'source\', \'fstype\', \'itotal\', \'iused\', \'iavail\', \'ipcent\',\n\'size\', \'used\', \'avail\', \'pcent\', \'file\' and \'target\' (see info page).\n",
     uVar3);
  local_88 = &DAT_0041476f;
  local_80 = "test invocation";
  local_78 = 0x4147d2;
  local_70 = "Multi-call invocation";
  local_68 = "sha224sum";
  local_60 = "sha2 utilities";
  local_58 = "sha256sum";
  local_50 = "sha2 utilities";
  local_48 = "sha384sum";
  local_40 = "sha2 utilities";
  local_38 = "sha512sum";
  local_30 = "sha2 utilities";
  local_28 = 0;
  local_20 = 0;
  puVar5 = &DAT_0041476f;
  do
    {
      iVar1 = func_0x00402080 (&DAT_00414769, puVar5);
      if (iVar1 == 0)
	break;
      ppuVar4 = ppuVar4 + 2;
      puVar5 = *ppuVar4;
    }
  while (puVar5 != (undefined *) 0x0);
  puVar5 = ppuVar4[1];
  if (puVar5 == (undefined *) 0x0)
    {
      func_0x00401ca0 ("\n%s online help: <%s>\n", "GNU coreutils",
		       "https://www.gnu.org/software/coreutils/");
      lVar2 = func_0x004020e0 (5, 0);
      if (lVar2 == 0)
	goto LAB_004032ee;
      iVar1 = func_0x00401f80 (lVar2, &DAT_004147f3, 3);
      if (iVar1 == 0)
	{
	  func_0x00401ca0 ("Full documentation at: <%s%s>\n",
			   "https://www.gnu.org/software/coreutils/",
			   &DAT_00414769);
	  puVar5 = &DAT_00414769;
	  uVar3 = 0x41478b;
	  goto LAB_004032d5;
	}
      puVar5 = &DAT_00414769;
    LAB_00403293:
      ;
      func_0x00401ca0
	("Report %s translation bugs to <https://translationproject.org/team/>\n",
	 &DAT_00414769);
    }
  else
    {
      func_0x00401ca0 ("\n%s online help: <%s>\n", "GNU coreutils",
		       "https://www.gnu.org/software/coreutils/");
      lVar2 = func_0x004020e0 (5, 0);
      if ((lVar2 != 0)
	  && (iVar1 = func_0x00401f80 (lVar2, &DAT_004147f3, 3), iVar1 != 0))
	goto LAB_00403293;
    }
  func_0x00401ca0 ("Full documentation at: <%s%s>\n",
		   "https://www.gnu.org/software/coreutils/", &DAT_00414769);
  uVar3 = 0x416b0c;
  if (puVar5 == &DAT_00414769)
    {
      uVar3 = 0x41478b;
    }
LAB_004032d5:
  ;
  do
    {
      func_0x00401ca0
	("or available locally via: info \'(coreutils) %s%s\'\n", puVar5,
	 uVar3);
    LAB_004032e7:
      ;
      func_0x00402160 ((ulong) uParm1);
    LAB_004032ee:
      ;
      func_0x00401ca0 ("Full documentation at: <%s%s>\n",
		       "https://www.gnu.org/software/coreutils/",
		       &DAT_00414769);
      puVar5 = &DAT_00414769;
      uVar3 = 0x41478b;
    }
  while (true);
}

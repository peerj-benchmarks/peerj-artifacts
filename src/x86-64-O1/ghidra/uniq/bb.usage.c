
#include "uniq.h"

long null_ARRAY_0061248_0_8_;
long null_ARRAY_0061248_8_8_;
long null_ARRAY_006126c_0_8_;
long null_ARRAY_006126c_16_8_;
long null_ARRAY_006126c_24_8_;
long null_ARRAY_006126c_32_8_;
long null_ARRAY_006126c_40_8_;
long null_ARRAY_006126c_48_8_;
long null_ARRAY_006126c_8_8_;
long null_ARRAY_0061270_0_4_;
long null_ARRAY_0061270_16_8_;
long null_ARRAY_0061270_4_4_;
long null_ARRAY_0061270_8_4_;
long local_68_2_2_;
long local_6b_1_3_;
long local_6c_4_4_;
long local_6e_4_4_;
long local_6f_4_4_;
long local_70_0_2_;
long local_70_4_4_;
long local_71_0_1_;
long local_71_4_4_;
long local_72_4_4_;
long DAT_00000010;
long DAT_0040e44d;
long DAT_0040e452;
long DAT_0040e4d6;
long DAT_0040e54e;
long DAT_0040f0fa;
long DAT_0040f0fc;
long DAT_0040f115;
long DAT_0040f170;
long DAT_0040f174;
long DAT_0040f178;
long DAT_0040f17b;
long DAT_0040f17d;
long DAT_0040f181;
long DAT_0040f185;
long DAT_0040f92b;
long DAT_004100be;
long DAT_004101c1;
long DAT_004101c7;
long DAT_004101c9;
long DAT_004101ca;
long DAT_004101e8;
long DAT_0041026e;
long DAT_00612000;
long DAT_00612010;
long DAT_00612020;
long DAT_00612430;
long DAT_00612490;
long DAT_00612494;
long DAT_00612498;
long DAT_0061249c;
long DAT_006124c0;
long DAT_006124c8;
long DAT_006124d0;
long DAT_006124e0;
long DAT_006124e8;
long DAT_00612500;
long DAT_00612508;
long DAT_00612550;
long DAT_00612554;
long DAT_00612558;
long DAT_00612559;
long DAT_0061255a;
long DAT_0061255b;
long DAT_0061255c;
long DAT_00612560;
long DAT_00612568;
long DAT_00612570;
long DAT_00612578;
long DAT_00612580;
long DAT_00612588;
long DAT_00612590;
long DAT_00612738;
long DAT_00612740;
long DAT_00612748;
long DAT_00612758;
long fde_00410e80;
long null_ARRAY_0040ee80;
long null_ARRAY_0040f020;
long null_ARRAY_0040f040;
long null_ARRAY_0040f068;
long null_ARRAY_0040f080;
long null_ARRAY_00410680;
long null_ARRAY_004108c0;
long null_ARRAY_00612480;
long null_ARRAY_00612520;
long null_ARRAY_006125c0;
long null_ARRAY_006126c0;
long null_ARRAY_00612700;
long PTR_DAT_00612420;
long PTR_FUN_00612428;
long PTR_null_ARRAY_00612478;
void
FUN_00401f81 (uint uParm1)
{
  int iVar1;
  long lVar2;
  undefined8 uVar3;
  undefined **ppuVar4;
  undefined *puVar5;
  undefined *local_88;
  char *local_80;
  undefined8 local_78;
  char *local_70;
  char *local_68;
  char *local_60;
  char *local_58;
  char *local_50;
  char *local_48;
  char *local_40;
  char *local_38;
  char *local_30;
  undefined8 local_28;
  undefined8 local_20;

  ppuVar4 = &local_88;
  if (uParm1 != 0)
    {
      func_0x004018f0 (DAT_006124e0,
		       "Try \'%s --help\' for more information.\n",
		       DAT_00612590);
      goto LAB_004021c1;
    }
  func_0x00401740 ("Usage: %s [OPTION]... [INPUT [OUTPUT]]\n", DAT_00612590);
  uVar3 = DAT_006124c0;
  func_0x00401900
    ("Filter adjacent matching lines from INPUT (or standard input),\nwriting to OUTPUT (or standard output).\n\nWith no options, matching lines are merged to the first occurrence.\n",
     DAT_006124c0);
  func_0x00401900
    ("\nMandatory arguments to long options are mandatory for short options too.\n",
     uVar3);
  func_0x00401900
    ("  -c, --count           prefix lines by the number of occurrences\n  -d, --repeated        only print duplicate lines, one for each group\n",
     uVar3);
  func_0x00401900
    ("  -D                    print all duplicate lines\n      --all-repeated[=METHOD]  like -D, but allow separating groups\n                                 with an empty line;\n                                 METHOD={none(default),prepend,separate}\n",
     uVar3);
  func_0x00401900
    ("  -f, --skip-fields=N   avoid comparing the first N fields\n", uVar3);
  func_0x00401900
    ("      --group[=METHOD]  show all items, separating groups with an empty line;\n                          METHOD={separate(default),prepend,append,both}\n",
     uVar3);
  func_0x00401900
    ("  -i, --ignore-case     ignore differences in case when comparing\n  -s, --skip-chars=N    avoid comparing the first N characters\n  -u, --unique          only print unique lines\n",
     uVar3);
  func_0x00401900
    ("  -z, --zero-terminated     line delimiter is NUL, not newline\n",
     uVar3);
  func_0x00401900
    ("  -w, --check-chars=N   compare no more than N characters in lines\n",
     uVar3);
  func_0x00401900 ("      --help     display this help and exit\n", uVar3);
  func_0x00401900 ("      --version  output version information and exit\n",
		   uVar3);
  func_0x00401900
    ("\nA field is a run of blanks (usually spaces and/or TABs), then non-blank\ncharacters.  Fields are skipped before chars.\n",
     uVar3);
  func_0x00401900
    ("\nNote: \'uniq\' does not detect repeated lines unless they are adjacent.\nYou may want to sort the input first, or use \'sort -u\' without \'uniq\'.\nAlso, comparisons honor the rules specified by \'LC_COLLATE\'.\n",
     uVar3);
  local_88 = &DAT_0040e452;
  local_80 = "test invocation";
  local_78 = 0x40e4b5;
  local_70 = "Multi-call invocation";
  local_68 = "sha224sum";
  local_60 = "sha2 utilities";
  local_58 = "sha256sum";
  local_50 = "sha2 utilities";
  local_48 = "sha384sum";
  local_40 = "sha2 utilities";
  local_38 = "sha512sum";
  local_30 = "sha2 utilities";
  local_28 = 0;
  local_20 = 0;
  puVar5 = &DAT_0040e452;
  do
    {
      iVar1 = func_0x00401a00 (&DAT_0040e44d, puVar5);
      if (iVar1 == 0)
	break;
      ppuVar4 = ppuVar4 + 2;
      puVar5 = *ppuVar4;
    }
  while (puVar5 != (undefined *) 0x0);
  puVar5 = ppuVar4[1];
  if (puVar5 == (undefined *) 0x0)
    {
      func_0x00401740 ("\n%s online help: <%s>\n", "GNU coreutils",
		       "https://www.gnu.org/software/coreutils/");
      lVar2 = func_0x00401a60 (5, 0);
      if (lVar2 == 0)
	goto LAB_004021c8;
      iVar1 = func_0x00401970 (lVar2, &DAT_0040e4d6, 3);
      if (iVar1 == 0)
	{
	  func_0x00401740 ("Full documentation at: <%s%s>\n",
			   "https://www.gnu.org/software/coreutils/",
			   &DAT_0040e44d);
	  puVar5 = &DAT_0040e44d;
	  uVar3 = 0x40e46e;
	  goto LAB_004021af;
	}
      puVar5 = &DAT_0040e44d;
    LAB_0040216d:
      ;
      func_0x00401740
	("Report %s translation bugs to <https://translationproject.org/team/>\n",
	 &DAT_0040e44d);
    }
  else
    {
      func_0x00401740 ("\n%s online help: <%s>\n", "GNU coreutils",
		       "https://www.gnu.org/software/coreutils/");
      lVar2 = func_0x00401a60 (5, 0);
      if ((lVar2 != 0)
	  && (iVar1 = func_0x00401970 (lVar2, &DAT_0040e4d6, 3), iVar1 != 0))
	goto LAB_0040216d;
    }
  func_0x00401740 ("Full documentation at: <%s%s>\n",
		   "https://www.gnu.org/software/coreutils/", &DAT_0040e44d);
  uVar3 = 0x4101e7;
  if (puVar5 == &DAT_0040e44d)
    {
      uVar3 = 0x40e46e;
    }
LAB_004021af:
  ;
  do
    {
      func_0x00401740
	("or available locally via: info \'(coreutils) %s%s\'\n", puVar5,
	 uVar3);
    LAB_004021c1:
      ;
      func_0x00401ae0 ((ulong) uParm1);
    LAB_004021c8:
      ;
      func_0x00401740 ("Full documentation at: <%s%s>\n",
		       "https://www.gnu.org/software/coreutils/",
		       &DAT_0040e44d);
      puVar5 = &DAT_0040e44d;
      uVar3 = 0x40e46e;
    }
  while (true);
}

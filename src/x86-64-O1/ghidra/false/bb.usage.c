
#include "false.h"

long null_ARRAY_0060e40_0_8_;
long null_ARRAY_0060e40_8_8_;
long null_ARRAY_0060e60_0_8_;
long null_ARRAY_0060e60_16_8_;
long null_ARRAY_0060e60_24_8_;
long null_ARRAY_0060e60_32_8_;
long null_ARRAY_0060e60_40_8_;
long null_ARRAY_0060e60_48_8_;
long null_ARRAY_0060e60_8_8_;
long local_68_2_2_;
long local_6b_1_3_;
long local_6c_4_4_;
long local_6e_4_4_;
long local_6f_4_4_;
long local_70_0_2_;
long local_70_4_4_;
long local_71_0_1_;
long local_71_4_4_;
long local_72_4_4_;
long puVar14;
long DAT_0040bb86;
long DAT_0040bb8a;
long DAT_0040bb8b;
long DAT_0040bc0f;
long DAT_0040bef0;
long DAT_0040bef4;
long DAT_0040bef6;
long DAT_0040befa;
long DAT_0040befd;
long DAT_0040beff;
long DAT_0040bf03;
long DAT_0040bf07;
long DAT_0040c6ab;
long DAT_0040ca55;
long DAT_0040ca66;
long DAT_0040caee;
long DAT_0060e000;
long DAT_0060e010;
long DAT_0060e020;
long DAT_0060e3a8;
long DAT_0060e410;
long DAT_0060e440;
long DAT_0060e450;
long DAT_0060e460;
long DAT_0060e468;
long DAT_0060e480;
long DAT_0060e488;
long DAT_0060e4d0;
long DAT_0060e4d8;
long DAT_0060e4e0;
long DAT_0060e638;
long DAT_0060e640;
long DAT_0060e648;
long fde_0040d5e0;
long null_ARRAY_0040cf00;
long null_ARRAY_0040d140;
long null_ARRAY_0060e400;
long null_ARRAY_0060e4a0;
long null_ARRAY_0060e500;
long null_ARRAY_0060e600;
long PTR_DAT_0060e3a0;
long PTR_null_ARRAY_0060e3f8;
void
FUN_004018ae (uint uParm1)
{
  undefined8 uVar1;
  int iVar2;
  long lVar3;
  undefined1 *puVar4;
  undefined1 **ppuVar5;
  char *pcVar6;
  undefined1 *local_88;
  char *local_80;
  undefined8 local_78;
  char *local_70;
  char *local_68;
  char *local_60;
  char *local_58;
  char *local_50;
  char *local_48;
  char *local_40;
  char *local_38;
  char *local_30;
  undefined8 local_28;
  undefined8 local_20;

  ppuVar5 = &local_88;
  func_0x00401360
    ("Usage: %s [ignored command line arguments]\n  or:  %s OPTION\n",
     DAT_0060e4e0, DAT_0060e4e0);
  func_0x00401360 (&DAT_0040bb86,
		   "Exit with a status code indicating failure.");
  uVar1 = DAT_0060e440;
  func_0x004014e0 ("      --help     display this help and exit\n",
		   DAT_0060e440);
  func_0x004014e0 ("      --version  output version information and exit\n",
		   uVar1);
  func_0x00401360
    ("\nNOTE: your shell may have its own version of %s, which usually supersedes\nthe version described here.  Please refer to your shell\'s documentation\nfor details about the options it supports.\n",
     "false");
  local_88 = &DAT_0040bb8b;
  local_80 = "test invocation";
  local_78 = 0x40bbee;
  local_70 = "Multi-call invocation";
  local_68 = "sha224sum";
  local_60 = "sha2 utilities";
  local_58 = "sha256sum";
  local_50 = "sha2 utilities";
  local_48 = "sha384sum";
  local_40 = "sha2 utilities";
  local_38 = "sha512sum";
  local_30 = "sha2 utilities";
  local_28 = 0;
  local_20 = 0;
  puVar4 = &DAT_0040bb8b;
  do
    {
      iVar2 = func_0x004015a0 ("false", puVar4);
      if (iVar2 == 0)
	break;
      ppuVar5 = ppuVar5 + 2;
      puVar4 = *ppuVar5;
    }
  while (puVar4 != (undefined1 *) 0x0);
  pcVar6 = ppuVar5[1];
  if (pcVar6 == (char *) 0x0)
    {
      func_0x00401360 ("\n%s online help: <%s>\n", "GNU coreutils",
		       "https://www.gnu.org/software/coreutils/");
      lVar3 = func_0x004015f0 (5, 0);
      if (lVar3 == 0)
	goto LAB_00401a6b;
      iVar2 = func_0x00401530 (lVar3, &DAT_0040bc0f, 3);
      if (iVar2 == 0)
	{
	  func_0x00401360 ("Full documentation at: <%s%s>\n",
			   "https://www.gnu.org/software/coreutils/",
			   "false");
	  pcVar6 = "false";
	  puVar4 = (undefined1 *) 0x40bba7;
	  goto LAB_00401a52;
	}
      pcVar6 = "false";
    LAB_00401a10:
      ;
      func_0x00401360
	("Report %s translation bugs to <https://translationproject.org/team/>\n",
	 "false");
    }
  else
    {
      func_0x00401360 ("\n%s online help: <%s>\n", "GNU coreutils",
		       "https://www.gnu.org/software/coreutils/");
      lVar3 = func_0x004015f0 (5, 0);
      if ((lVar3 != 0)
	  && (iVar2 = func_0x00401530 (lVar3, &DAT_0040bc0f, 3), iVar2 != 0))
	goto LAB_00401a10;
    }
  func_0x00401360 ("Full documentation at: <%s%s>\n",
		   "https://www.gnu.org/software/coreutils/", "false");
  puVar4 = &DAT_0040bb8a;
  if (pcVar6 == "false")
    {
      puVar4 = (undefined1 *) 0x40bba7;
    }
LAB_00401a52:
  ;
  do
    {
      func_0x00401360
	("or available locally via: info \'(coreutils) %s%s\'\n", pcVar6,
	 puVar4);
      func_0x00401630 ((ulong) uParm1);
    LAB_00401a6b:
      ;
      func_0x00401360 ("Full documentation at: <%s%s>\n",
		       "https://www.gnu.org/software/coreutils/", "false");
      pcVar6 = "false";
      puVar4 = (undefined1 *) 0x40bba7;
    }
  while (true);
}

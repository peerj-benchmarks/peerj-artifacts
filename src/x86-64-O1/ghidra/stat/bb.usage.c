
#include "stat.h"

long null_ARRAY_0061b74_0_8_;
long null_ARRAY_0061b74_8_8_;
long null_ARRAY_0061ba0_0_8_;
long null_ARRAY_0061ba0_16_8_;
long null_ARRAY_0061ba0_24_8_;
long null_ARRAY_0061ba0_32_8_;
long null_ARRAY_0061ba0_40_8_;
long null_ARRAY_0061ba0_48_8_;
long null_ARRAY_0061ba0_8_8_;
long null_ARRAY_0061ba4_0_4_;
long null_ARRAY_0061ba4_16_8_;
long null_ARRAY_0061ba4_4_4_;
long null_ARRAY_0061ba4_8_4_;
long local_49_0_1_;
long local_68_2_2_;
long local_6b_1_3_;
long local_6c_4_4_;
long local_6e_4_4_;
long local_6f_4_4_;
long local_70_0_2_;
long local_70_4_4_;
long local_71_0_1_;
long local_71_4_4_;
long local_72_4_4_;
long DAT_00000010;
long DAT_004165cb;
long DAT_004165d0;
long DAT_004165d3;
long DAT_004165d7;
long DAT_004165da;
long DAT_004165de;
long DAT_00416665;
long DAT_004167f3;
long DAT_004168f2;
long DAT_0041692a;
long DAT_00416949;
long DAT_0041694e;
long DAT_0041696b;
long DAT_00416970;
long DAT_004169f4;
long DAT_0041723b;
long DAT_0041723c;
long DAT_004172f2;
long DAT_0041735f;
long DAT_00417788;
long DAT_0041778e;
long DAT_00417790;
long DAT_00417794;
long DAT_00417798;
long DAT_0041779b;
long DAT_0041779d;
long DAT_004177a1;
long DAT_004177a5;
long DAT_00417f2b;
long DAT_004182f2;
long DAT_004182fc;
long DAT_00418349;
long DAT_00418469;
long DAT_0041846a;
long DAT_00418488;
long DAT_004184a1;
long DAT_004184cf;
long DAT_004184eb;
long DAT_00418510;
long DAT_004185d4;
long DAT_004185d6;
long DAT_004185da;
long DAT_0061b200;
long DAT_0061b210;
long DAT_0061b220;
long DAT_0061b6f0;
long DAT_0061b750;
long DAT_0061b754;
long DAT_0061b758;
long DAT_0061b75c;
long DAT_0061b780;
long DAT_0061b790;
long DAT_0061b7a0;
long DAT_0061b7a8;
long DAT_0061b7c0;
long DAT_0061b7c8;
long DAT_0061b860;
long DAT_0061b868;
long DAT_0061b870;
long DAT_0061b8a0;
long DAT_0061b8a8;
long DAT_0061b8b0;
long DAT_0061b8b1;
long DAT_0061b8b8;
long DAT_0061b8c0;
long DAT_0061b8c8;
long DAT_0061ba78;
long DAT_0061ba80;
long DAT_0061ba88;
long DAT_0061ba90;
long DAT_0061bad8;
long DAT_0061c308;
long DAT_0061c310;
long DAT_0061c320;
long fde_00419460;
long null_ARRAY_00417080;
long null_ARRAY_004171ea;
long null_ARRAY_00417320;
long null_ARRAY_00417e60;
long null_ARRAY_00417ec0;
long null_ARRAY_004185a0;
long null_ARRAY_00418a00;
long null_ARRAY_00418c30;
long null_ARRAY_0061b740;
long null_ARRAY_0061b7e0;
long null_ARRAY_0061b820;
long null_ARRAY_0061b871;
long null_ARRAY_0061b880;
long null_ARRAY_0061b900;
long null_ARRAY_0061ba00;
long null_ARRAY_0061ba40;
long null_ARRAY_0061bb00;
long PTR_DAT_0061b6e8;
long PTR_null_ARRAY_0061b738;
long PTR_null_ARRAY_0061b760;
long PTR_s__0061b6e0;
long stack0x00000008;
long stack0xffffffffffffffe0;
void
FUN_00404071 (uint uParm1)
{
  int iVar1;
  long lVar2;
  undefined8 uVar3;
  undefined **ppuVar4;
  undefined *puVar5;
  undefined *local_88;
  char *local_80;
  undefined8 local_78;
  char *local_70;
  char *local_68;
  char *local_60;
  char *local_58;
  char *local_50;
  char *local_48;
  char *local_40;
  char *local_38;
  char *local_30;
  undefined8 local_28;
  undefined8 local_20;

  ppuVar4 = &local_88;
  if (uParm1 != 0)
    {
      func_0x00402040 (DAT_0061b7a0,
		       "Try \'%s --help\' for more information.\n",
		       DAT_0061b8c8);
      goto LAB_004042e0;
    }
  func_0x00401dd0 ("Usage: %s [OPTION]... FILE...\n", DAT_0061b8c8);
  uVar3 = DAT_0061b780;
  func_0x00402050 ("Display file or file system status.\n", DAT_0061b780);
  func_0x00402050
    ("\nMandatory arguments to long options are mandatory for short options too.\n",
     uVar3);
  func_0x00402050
    ("  -L, --dereference     follow links\n  -f, --file-system     display file system status instead of file status\n",
     uVar3);
  func_0x00402050
    ("  -c  --format=FORMAT   use the specified FORMAT instead of the default;\n                          output a newline after each use of FORMAT\n      --printf=FORMAT   like --format, but interpret backslash escapes,\n                          and do not output a mandatory trailing newline;\n                          if you want a newline, include \\n in FORMAT\n  -t, --terse           print the information in terse form\n",
     uVar3);
  func_0x00402050 ("      --help     display this help and exit\n", uVar3);
  func_0x00402050 ("      --version  output version information and exit\n",
		   uVar3);
  func_0x00402050
    ("\nThe valid format sequences for files (without --file-system):\n\n  %a   access rights in octal (note \'#\' and \'0\' printf flags)\n  %A   access rights in human readable form\n  %b   number of blocks allocated (see %B)\n  %B   the size in bytes of each block reported by %b\n  %C   SELinux security context string\n",
     uVar3);
  func_0x00402050
    ("  %d   device number in decimal\n  %D   device number in hex\n  %f   raw mode in hex\n  %F   file type\n  %g   group ID of owner\n  %G   group name of owner\n",
     uVar3);
  func_0x00402050
    ("  %h   number of hard links\n  %i   inode number\n  %m   mount point\n  %n   file name\n  %N   quoted file name with dereference if symbolic link\n  %o   optimal I/O transfer size hint\n  %s   total size, in bytes\n  %t   major device type in hex, for character/block device special files\n  %T   minor device type in hex, for character/block device special files\n",
     uVar3);
  func_0x00402050
    ("  %u   user ID of owner\n  %U   user name of owner\n  %w   time of file birth, human-readable; - if unknown\n  %W   time of file birth, seconds since Epoch; 0 if unknown\n  %x   time of last access, human-readable\n  %X   time of last access, seconds since Epoch\n  %y   time of last data modification, human-readable\n  %Y   time of last data modification, seconds since Epoch\n  %z   time of last status change, human-readable\n  %Z   time of last status change, seconds since Epoch\n\n",
     uVar3);
  func_0x00402050
    ("Valid format sequences for file systems:\n\n  %a   free blocks available to non-superuser\n  %b   total data blocks in file system\n  %c   total file nodes in file system\n  %d   free file nodes in file system\n  %f   free blocks in file system\n",
     uVar3);
  func_0x00402050
    ("  %i   file system ID in hex\n  %l   maximum length of filenames\n  %n   file name\n  %s   block size (for faster transfers)\n  %S   fundamental block size (for block counts)\n  %t   file system type in hex\n  %T   file system type in human readable form\n",
     uVar3);
  func_0x00401dd0 ("\n--terse is equivalent to the following FORMAT:\n    %s",
		   "%n %s %b %f %u %g %D %i %h %t %T %X %Y %Z %W %o\n");
  func_0x00401dd0
    ("--terse --file-system is equivalent to the following FORMAT:\n    %s",
     "%n %i %l %t %s %S %b %f %a %c %d\n");
  func_0x00401dd0
    ("\nNOTE: your shell may have its own version of %s, which usually supersedes\nthe version described here.  Please refer to your shell\'s documentation\nfor details about the options it supports.\n",
     &DAT_0041696b);
  local_88 = &DAT_00416970;
  local_80 = "test invocation";
  local_78 = 0x4169d3;
  local_70 = "Multi-call invocation";
  local_68 = "sha224sum";
  local_60 = "sha2 utilities";
  local_58 = "sha256sum";
  local_50 = "sha2 utilities";
  local_48 = "sha384sum";
  local_40 = "sha2 utilities";
  local_38 = "sha512sum";
  local_30 = "sha2 utilities";
  local_28 = 0;
  local_20 = 0;
  puVar5 = &DAT_00416970;
  do
    {
      iVar1 = func_0x004021a0 (&DAT_0041696b, puVar5);
      if (iVar1 == 0)
	break;
      ppuVar4 = ppuVar4 + 2;
      puVar5 = *ppuVar4;
    }
  while (puVar5 != (undefined *) 0x0);
  puVar5 = ppuVar4[1];
  if (puVar5 == (undefined *) 0x0)
    {
      func_0x00401dd0 ("\n%s online help: <%s>\n", "GNU coreutils",
		       "https://www.gnu.org/software/coreutils/");
      lVar2 = func_0x00402210 (5, 0);
      if (lVar2 == 0)
	goto LAB_004042e7;
      iVar1 = func_0x004020c0 (lVar2, &DAT_004169f4, 3);
      if (iVar1 == 0)
	{
	  func_0x00401dd0 ("Full documentation at: <%s%s>\n",
			   "https://www.gnu.org/software/coreutils/",
			   &DAT_0041696b);
	  puVar5 = &DAT_0041696b;
	  uVar3 = 0x41698c;
	  goto LAB_004042ce;
	}
      puVar5 = &DAT_0041696b;
    LAB_0040428c:
      ;
      func_0x00401dd0
	("Report %s translation bugs to <https://translationproject.org/team/>\n",
	 &DAT_0041696b);
    }
  else
    {
      func_0x00401dd0 ("\n%s online help: <%s>\n", "GNU coreutils",
		       "https://www.gnu.org/software/coreutils/");
      lVar2 = func_0x00402210 (5, 0);
      if ((lVar2 != 0)
	  && (iVar1 = func_0x004020c0 (lVar2, &DAT_004169f4, 3), iVar1 != 0))
	goto LAB_0040428c;
    }
  func_0x00401dd0 ("Full documentation at: <%s%s>\n",
		   "https://www.gnu.org/software/coreutils/", &DAT_0041696b);
  uVar3 = 0x418487;
  if (puVar5 == &DAT_0041696b)
    {
      uVar3 = 0x41698c;
    }
LAB_004042ce:
  ;
  do
    {
      func_0x00401dd0
	("or available locally via: info \'(coreutils) %s%s\'\n", puVar5,
	 uVar3);
    LAB_004042e0:
      ;
      func_0x004022b0 ((ulong) uParm1);
    LAB_004042e7:
      ;
      func_0x00401dd0 ("Full documentation at: <%s%s>\n",
		       "https://www.gnu.org/software/coreutils/",
		       &DAT_0041696b);
      puVar5 = &DAT_0041696b;
      uVar3 = 0x41698c;
    }
  while (true);
}

typedef unsigned char   undefined;

typedef unsigned char    byte;
typedef unsigned char    dwfenc;
typedef unsigned int    dword;
typedef unsigned long    qword;
typedef unsigned char    undefined1;
typedef unsigned short    undefined2;
typedef unsigned int    undefined4;
typedef unsigned long    undefined6;
typedef unsigned long    undefined8;
typedef unsigned short    word;
#define unkbyte9   unsigned long long
#define unkbyte10   unsigned long long
#define unkbyte11   unsigned long long
#define unkbyte12   unsigned long long
#define unkbyte13   unsigned long long
#define unkbyte14   unsigned long long
#define unkbyte15   unsigned long long
#define unkbyte16   unsigned long long

#define unkuint9   unsigned long long
#define unkuint10   unsigned long long
#define unkuint11   unsigned long long
#define unkuint12   unsigned long long
#define unkuint13   unsigned long long
#define unkuint14   unsigned long long
#define unkuint15   unsigned long long
#define unkuint16   unsigned long long

#define unkint9   long long
#define unkint10   long long
#define unkint11   long long
#define unkint12   long long
#define unkint13   long long
#define unkint14   long long
#define unkint15   long long
#define unkint16   long long

#define unkfloat1   float
#define unkfloat2   float
#define unkfloat3   float
#define unkfloat5   double
#define unkfloat6   double
#define unkfloat7   double
#define unkfloat9   long double
#define unkfloat11   long double
#define unkfloat12   long double
#define unkfloat13   long double
#define unkfloat14   long double
#define unkfloat15   long double
#define unkfloat16   long double

#define BADSPACEBASE   void
#define code   void

#define ulong long
#define true 1
#define bool int
#define false 0
#define float10 int
#define ushort int
#define undefined7 long
#define undefined2 long
#define uint5 long
#define uint6 long

// Add definition for uint
typedef unsigned int    uint;

#define sbyte char

extern imperfection_wrapper(void);
typedef struct eh_frame_hdr eh_frame_hdr, *Peh_frame_hdr;

struct eh_frame_hdr {
    byte eh_frame_hdr_version; // Exception Handler Frame Header Version
    dwfenc eh_frame_pointer_encoding; // Exception Handler Frame Pointer Encoding
    dwfenc eh_frame_desc_entry_count_encoding; // Encoding of # of Exception Handler FDEs
    dwfenc eh_frame_table_encoding; // Exception Handler Table Encoding
};

typedef struct fde_table_entry fde_table_entry, *Pfde_table_entry;

struct fde_table_entry {
    dword initial_loc; // Initial Location
    dword data_loc; // Data location
};

typedef enum Elf64_DynTag {
    DT_AUDIT=1879047932,
    DT_AUXILIARY=2147483645,
    DT_BIND_NOW=24,
    DT_CHECKSUM=1879047672,
    DT_CONFIG=1879047930,
    DT_DEBUG=21,
    DT_DEPAUDIT=1879047931,
    DT_ENCODING=32,
    DT_FEATURE_1=1879047676,
    DT_FILTER=2147483647,
    DT_FINI=13,
    DT_FINI_ARRAY=26,
    DT_FINI_ARRAYSZ=28,
    DT_FLAGS=30,
    DT_FLAGS_1=1879048187,
    DT_GNU_CONFLICT=1879047928,
    DT_GNU_CONFLICTSZ=1879047670,
    DT_GNU_HASH=1879047925,
    DT_GNU_LIBLIST=1879047929,
    DT_GNU_LIBLISTSZ=1879047671,
    DT_GNU_PRELINKED=1879047669,
    DT_HASH=4,
    DT_INIT=12,
    DT_INIT_ARRAY=25,
    DT_INIT_ARRAYSZ=27,
    DT_JMPREL=23,
    DT_MOVEENT=1879047674,
    DT_MOVESZ=1879047675,
    DT_MOVETAB=1879047934,
    DT_NEEDED=1,
    DT_NULL=0,
    DT_PLTGOT=3,
    DT_PLTPAD=1879047933,
    DT_PLTPADSZ=1879047673,
    DT_PLTREL=20,
    DT_PLTRELSZ=2,
    DT_POSFLAG_1=1879047677,
    DT_PREINIT_ARRAYSZ=33,
    DT_REL=17,
    DT_RELA=7,
    DT_RELACOUNT=1879048185,
    DT_RELAENT=9,
    DT_RELASZ=8,
    DT_RELCOUNT=1879048186,
    DT_RELENT=19,
    DT_RELSZ=18,
    DT_RPATH=15,
    DT_RUNPATH=29,
    DT_SONAME=14,
    DT_STRSZ=10,
    DT_STRTAB=5,
    DT_SYMBOLIC=16,
    DT_SYMENT=11,
    DT_SYMINENT=1879047679,
    DT_SYMINFO=1879047935,
    DT_SYMINSZ=1879047678,
    DT_SYMTAB=6,
    DT_TEXTREL=22,
    DT_TLSDESC_GOT=1879047927,
    DT_TLSDESC_PLT=1879047926,
    DT_VERDEF=1879048188,
    DT_VERDEFNUM=1879048189,
    DT_VERNEED=1879048190,
    DT_VERNEEDNUM=1879048191,
    DT_VERSYM=1879048176
} Elf64_DynTag;

typedef enum Elf_ProgramHeaderType {
    PT_DYNAMIC=2,
    PT_GNU_EH_FRAME=1685382480,
    PT_GNU_RELRO=1685382482,
    PT_GNU_STACK=1685382481,
    PT_INTERP=3,
    PT_LOAD=1,
    PT_NOTE=4,
    PT_NULL=0,
    PT_PHDR=6,
    PT_SHLIB=5,
    PT_TLS=7
} Elf_ProgramHeaderType;

typedef struct Elf64_Dyn Elf64_Dyn, *PElf64_Dyn;

struct Elf64_Dyn {
    enum Elf64_DynTag d_tag;
    qword d_val;
};

typedef struct Elf64_Shdr Elf64_Shdr, *PElf64_Shdr;

typedef enum Elf_SectionHeaderType {
    SHT_CHECKSUM=1879048184,
    SHT_DYNAMIC=6,
    SHT_DYNSYM=11,
    SHT_FINI_ARRAY=15,
    SHT_GNU_ATTRIBUTES=1879048181,
    SHT_GNU_HASH=1879048182,
    SHT_GNU_LIBLIST=1879048183,
    SHT_GNU_verdef=1879048189,
    SHT_GNU_verneed=1879048190,
    SHT_GNU_versym=1879048191,
    SHT_GROUP=17,
    SHT_HASH=5,
    SHT_INIT_ARRAY=14,
    SHT_NOBITS=8,
    SHT_NOTE=7,
    SHT_NULL=0,
    SHT_PREINIT_ARRAY=16,
    SHT_PROGBITS=1,
    SHT_REL=9,
    SHT_RELA=4,
    SHT_SHLIB=10,
    SHT_STRTAB=3,
    SHT_SUNW_COMDAT=1879048187,
    SHT_SUNW_move=1879048186,
    SHT_SUNW_syminfo=1879048188,
    SHT_SYMTAB=2,
    SHT_SYMTAB_SHNDX=18
} Elf_SectionHeaderType;

struct Elf64_Shdr {
    dword sh_name;
    enum Elf_SectionHeaderType sh_type;
    qword sh_flags;
    qword sh_addr;
    qword sh_offset;
    qword sh_size;
    dword sh_link;
    dword sh_info;
    qword sh_addralign;
    qword sh_entsize;
};

typedef struct Elf64_Phdr Elf64_Phdr, *PElf64_Phdr;

struct Elf64_Phdr {
    enum Elf_ProgramHeaderType p_type;
    dword p_flags;
    qword p_offset;
    qword p_vaddr;
    qword p_paddr;
    qword p_filesz;
    qword p_memsz;
    qword p_align;
};

typedef struct Elf64_Rela Elf64_Rela, *PElf64_Rela;

struct Elf64_Rela {
    qword r_offset; // location to apply the relocation action
    qword r_info; // the symbol table index and the type of relocation
    qword r_addend; // a constant addend used to compute the relocatable field value
};

typedef struct Elf64_Sym Elf64_Sym, *PElf64_Sym;

struct Elf64_Sym {
    dword st_name;
    byte st_info;
    byte st_other;
    word st_shndx;
    qword st_value;
    qword st_size;
};

typedef struct Elf64_Ehdr Elf64_Ehdr, *PElf64_Ehdr;

struct Elf64_Ehdr {
    byte e_ident_magic_num;
    char e_ident_magic_str[3];
    byte e_ident_class;
    byte e_ident_data;
    byte e_ident_version;
    byte e_ident_pad[9];
    word e_type;
    word e_machine;
    dword e_version;
    qword e_entry;
    qword e_phoff;
    qword e_shoff;
    dword e_flags;
    word e_ehsize;
    word e_phentsize;
    word e_phnum;
    word e_shentsize;
    word e_shnum;
    word e_shstrndx;
};




undefined8 _DT_INIT(void);
void FUN_00401940(void);
void FUN_00401ae0(void);
void FUN_00401d20(void);
void entry(void);
void FUN_00401ef0(void);
void FUN_00401f70(void);
void FUN_00401ff0(void);
ulong FUN_0040202e(void);
void FUN_00402085(long lParm1);
void FUN_004020d5(void);
void FUN_00402129(undefined uParm1);
undefined8 FUN_00402170(uint uParm1,char cParm2,char *pcParm3);
void FUN_00402284(undefined8 uParm1,int iParm2,undefined4 *puParm3,undefined8 uParm4);
ulong FUN_004022a7(long lParm1,long *plParm2);
void FUN_0040236b(long lParm1);
ulong FUN_0040245f(byte bParm1);
void FUN_004025f3(undefined1 *puParm1,uint uParm2);
void FUN_00402798(undefined8 *puParm1);
void FUN_00402883(undefined8 *puParm1,int iParm2);
void FUN_004029f6(undefined8 *puParm1);
ulong FUN_00402a5d(void);
void FUN_00402af1(int iParm1);
void FUN_00402b39(void);
void FUN_00402c01(void);
void FUN_00402cd0(long lParm1);
void FUN_00402d18(byte bParm1);
undefined8 FUN_00402d6b(long lParm1);
undefined8 FUN_00402e9a(undefined8 *puParm1);
void FUN_0040319e(uint uParm1,undefined8 *puParm2);
void FUN_00403fe2(undefined *puParm1);
void FUN_0040401b(uint uParm1);
void FUN_0040433a(char *pcParm1,char cParm2,char *pcParm3,undefined4 *puParm4);
ulong FUN_004043f3(uint uParm1,undefined8 *puParm2);
void FUN_00404b8d(void);
void FUN_00404c33(long lParm1,ulong uParm2);
long FUN_00404c56(undefined8 uParm1,undefined8 uParm2);
void FUN_00404ce6(undefined8 *puParm1);
ulong FUN_00404d26(ulong uParm1);
char * FUN_00404db4(long lParm1,long lParm2);
ulong FUN_00404e3f(byte *pbParm1,long lParm2,ulong uParm3);
void FUN_00405022(undefined8 uParm1,ulong uParm2);
long FUN_00405046(long lParm1,long lParm2,long lParm3);
char * FUN_00405075(char *param_1,long param_2,char *param_3,undefined8 *param_4,undefined param_5,undefined8 param_6,undefined8 param_7,uint param_8);
void FUN_00406771(void);
void FUN_00406793(long lParm1);
int * FUN_00406828(int *piParm1,int iParm2);
undefined * FUN_00406858(char *pcParm1,int iParm2);
ulong FUN_00406910(undefined *param_1,ulong param_2,long param_3,ulong param_4,uint param_5,ulong param_6,long param_7,char *param_8,char *param_9);
undefined1 * FUN_004076c7(int iParm1,undefined8 uParm2,undefined8 uParm3,uint *puParm4);
ulong FUN_00407869(undefined1 *puParm1,byte bParm2,undefined8 uParm3);
void FUN_0040789d(undefined8 uParm1,undefined8 uParm2,char cParm3);
void FUN_00407926(undefined8 uParm1,char cParm2);
void FUN_0040793f(undefined8 uParm1);
void FUN_00407952(uint uParm1,undefined8 uParm2,undefined8 uParm3);
void FUN_004079e1(void);
void FUN_004079f4(undefined8 uParm1,undefined8 uParm2);
void FUN_00407a09(undefined8 uParm1);
void FUN_00407a1f(undefined8 uParm1);
void FUN_00407a3c(undefined8 uParm1,long lParm2,undefined8 uParm3,undefined8 uParm4,undefined8 *puParm5,undefined8 uParm6);
void FUN_00407c79(void);
void FUN_00407ccb(void);
void FUN_00407d50(long lParm1);
long FUN_00407d6a(long lParm1,long lParm2);
void FUN_00407d9d(long lParm1,ulong *puParm2);
long FUN_00407dee(void);
long FUN_00407e16(undefined8 param_1,ulong param_2,long param_3,long param_4,undefined8 param_5,undefined8 param_6,uint param_7);
void FUN_00407f08(undefined8 uParm1,undefined8 uParm2,undefined8 uParm3,undefined8 uParm4,undefined8 uParm5,undefined8 uParm6);
ulong FUN_00407f29(long *plParm1,int iParm2,int iParm3);
ulong FUN_00407fb4(char *pcParm1,char **ppcParm2,uint uParm3,long *plParm4,long lParm5);
ulong FUN_00408362(long *plParm1,int iParm2,int iParm3);
ulong FUN_004083ed(char *pcParm1,char **ppcParm2,uint uParm3,long *plParm4,long lParm5);
ulong FUN_0040879b(uint uParm1);
ulong FUN_0040881e(ulong *puParm1,int iParm2,int iParm3);
ulong FUN_0040887c(byte *pbParm1,byte **ppbParm2,uint uParm3,ulong *puParm4,long lParm5);
void FUN_00408bc5(void);
void FUN_00408bf2(undefined8 uParm1);
void FUN_00408c32(uint uParm1,uint uParm2,undefined8 uParm3,undefined8 uParm4);
void FUN_00408c89(undefined4 uParm1,undefined4 uParm2,undefined4 uParm3,undefined4 uParm4,undefined4 uParm5,undefined4 uParm6,undefined4 uParm7,undefined4 uParm8,ulong param_1,ulong param_2,undefined8 param_3,undefined8 param_4,undefined8 param_5,undefined8 param_6);
undefined8 FUN_00408d5c(undefined8 uParm1);
ulong FUN_00408ddf(uint param_1,undefined8 param_2,undefined8 param_3,undefined8 param_4);
ulong FUN_00408f4e(long lParm1);
undefined8 FUN_00408fde(void);
void FUN_00408ff1(void);
void FUN_00408fff(long lParm1,int *piParm2);
ulong FUN_004090be(int param_1,undefined8 *param_2,char *param_3,long *param_4,int *param_5,int param_6,int *param_7,int param_8,undefined8 param_9);
ulong FUN_00409579(uint param_1,undefined8 *param_2,char *param_3,long param_4,undefined8 param_5,ulong param_6,uint *param_7,int param_8);
void FUN_004099f5(void);
void FUN_00409a4b(void);
void FUN_00409a61(void);
void FUN_00409b0d(long lParm1);
ulong FUN_00409b27(uint *puParm1,byte *pbParm2,long lParm3);
void FUN_00409b8c(long lParm1,long lParm2);
void FUN_00409bba(long lParm1);
undefined8 * FUN_00409be6(long lParm1);
undefined8 FUN_00409c89(long *plParm1,char *pcParm2);
void FUN_00409dba(long *plParm1);
long FUN_00409dd9(long lParm1);
ulong FUN_00409e83(long lParm1);
long FUN_00409ee4(long lParm1,undefined8 uParm2,long lParm3);
long FUN_00409f72(long lParm1,uint *puParm2);
void FUN_0040a06c(long lParm1);
void FUN_0040a08b(void);
ulong FUN_0040a133(char *pcParm1);
ulong FUN_0040a18c(undefined8 uParm1,undefined8 uParm2,undefined8 uParm3);
void FUN_0040a259(ulong uParm1);
ulong FUN_0040a299(undefined8 uParm1);
undefined8 FUN_0040a2fa(void);
char * FUN_0040a302(void);
ulong FUN_0040a63d(long lParm1,long lParm2,char cParm3);
undefined8 FUN_0040a66f(long param_1,undefined8 param_2,char param_3,char param_4,char param_5,char param_6,char param_7,char param_8,char param_9);
ulong FUN_0040a6ef(uint uParm1,char *pcParm2);
void FUN_0040aaa2(code *pcParm1,long *plParm2,undefined8 uParm3);
long FUN_0040ab4a(ulong param_1,long param_2,int param_3,int param_4,int param_5,uint param_6,int param_7,int param_8,int param_9,int param_10);
long FUN_0040ac73(void);
long FUN_0040ad10(undefined8 *puParm1,code *pcParm2,long *plParm3);
void FUN_0040b30b(undefined8 uParm1);
ulong FUN_0040b328(char *pcParm1,char *pcParm2,ulong uParm3);
ulong FUN_0040b445(char *pcParm1,long lParm2);
long FUN_0040b48c(ulong uParm1,uint *puParm2,ulong uParm3,uint *puParm4,long *plParm5);
ulong FUN_0040b5b0(void);
ulong FUN_0040b5e2(void);
int * FUN_0040b7ef(ulong uParm1,long lParm2,ulong uParm3,uint *puParm4,ulong *puParm5);
char * FUN_0040bdd2(int iParm1,long lParm2,long lParm3,long lParm4,uint uParm5);
void FUN_0040c3ea(uint param_1,undefined8 param_2);
ulong FUN_0040c5b0(void);
void FUN_0040c7c3(undefined8 uParm1,uint uParm2);
undefined8 *FUN_0040c964(undefined8 *puParm1,undefined8 *puParm2,undefined8 *puParm3,undefined8 uParm4);
undefined8 * FUN_00412501(ulong uParm1);
void FUN_00412578(ulong uParm1);
double FUN_004125f9(int *piParm1);
void FUN_00412641(int *param_1);
undefined8 FUN_004126c2(uint *puParm1,ulong *puParm2);
undefined8 FUN_00412ae7(byte *pbParm1,ulong *puParm2,byte **ppbParm3);
ulong FUN_0041364c(void);
ulong FUN_00413674(void);
void FUN_004136b0(void);
undefined8 _DT_FINI(void);
undefined FUN_0061c000();
undefined FUN_0061c008();
undefined FUN_0061c010();
undefined FUN_0061c018();
undefined FUN_0061c020();
undefined FUN_0061c028();
undefined FUN_0061c030();
undefined FUN_0061c038();
undefined FUN_0061c040();
undefined FUN_0061c048();
undefined FUN_0061c050();
undefined FUN_0061c058();
undefined FUN_0061c060();
undefined FUN_0061c068();
undefined FUN_0061c070();
undefined FUN_0061c078();
undefined FUN_0061c080();
undefined FUN_0061c088();
undefined FUN_0061c090();
undefined FUN_0061c098();
undefined FUN_0061c0a0();
undefined FUN_0061c0a8();
undefined FUN_0061c0b0();
undefined FUN_0061c0b8();
undefined FUN_0061c0c0();
undefined FUN_0061c0c8();
undefined FUN_0061c0d0();
undefined FUN_0061c0d8();
undefined FUN_0061c0e0();
undefined FUN_0061c0e8();
undefined FUN_0061c0f0();
undefined FUN_0061c0f8();
undefined FUN_0061c100();
undefined FUN_0061c108();
undefined FUN_0061c110();
undefined FUN_0061c118();
undefined FUN_0061c120();
undefined FUN_0061c128();
undefined FUN_0061c130();
undefined FUN_0061c138();
undefined FUN_0061c140();
undefined FUN_0061c148();
undefined FUN_0061c150();
undefined FUN_0061c158();
undefined FUN_0061c160();
undefined FUN_0061c168();
undefined FUN_0061c170();
undefined FUN_0061c178();
undefined FUN_0061c180();
undefined FUN_0061c188();
undefined FUN_0061c190();
undefined FUN_0061c198();
undefined FUN_0061c1a0();
undefined FUN_0061c1a8();
undefined FUN_0061c1b0();
undefined FUN_0061c1b8();
undefined FUN_0061c1c0();
undefined FUN_0061c1c8();
undefined FUN_0061c1d0();
undefined FUN_0061c1d8();
undefined FUN_0061c1e0();
undefined FUN_0061c1e8();
undefined FUN_0061c1f0();
undefined FUN_0061c1f8();
undefined FUN_0061c200();
undefined FUN_0061c208();
undefined FUN_0061c210();
undefined FUN_0061c218();
undefined FUN_0061c220();
undefined FUN_0061c228();
undefined FUN_0061c230();
undefined FUN_0061c238();
undefined FUN_0061c240();
undefined FUN_0061c248();
undefined FUN_0061c250();
undefined FUN_0061c258();
undefined FUN_0061c260();
undefined FUN_0061c268();
undefined FUN_0061c270();
undefined FUN_0061c278();
undefined FUN_0061c280();
undefined FUN_0061c288();
undefined FUN_0061c290();
undefined FUN_0061c298();


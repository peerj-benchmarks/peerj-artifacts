
#include "base32.h"

long null_ARRAY_0061146_0_8_;
long null_ARRAY_0061146_8_8_;
long null_ARRAY_0061164_0_8_;
long null_ARRAY_0061164_16_8_;
long null_ARRAY_0061164_24_8_;
long null_ARRAY_0061164_32_8_;
long null_ARRAY_0061164_40_8_;
long null_ARRAY_0061164_48_8_;
long null_ARRAY_0061164_8_8_;
long null_ARRAY_0061168_0_4_;
long null_ARRAY_0061168_16_8_;
long null_ARRAY_0061168_4_4_;
long null_ARRAY_0061168_8_4_;
long local_68_2_2_;
long local_6b_1_3_;
long local_6c_4_4_;
long local_6e_4_4_;
long local_6f_4_4_;
long local_70_0_2_;
long local_70_4_4_;
long local_71_0_1_;
long local_71_4_4_;
long local_72_4_4_;
long DAT_00000008;
long DAT_00000010;
long DAT_0040ddac;
long DAT_0040de30;
long DAT_0040de56;
long DAT_0040de6c;
long DAT_0040e588;
long DAT_0040e58c;
long DAT_0040e590;
long DAT_0040e593;
long DAT_0040e595;
long DAT_0040e599;
long DAT_0040e59d;
long DAT_0040ed2b;
long DAT_0040f448;
long DAT_0040f551;
long DAT_0040f557;
long DAT_0040f569;
long DAT_0040f56a;
long DAT_0040f588;
long DAT_0040f58c;
long DAT_0040f616;
long DAT_00611000;
long DAT_00611010;
long DAT_00611020;
long DAT_00611408;
long DAT_00611470;
long DAT_00611474;
long DAT_00611478;
long DAT_0061147c;
long DAT_00611480;
long DAT_00611488;
long DAT_00611490;
long DAT_006114a0;
long DAT_006114a8;
long DAT_006114c0;
long DAT_006114c8;
long DAT_00611510;
long DAT_00611518;
long DAT_00611520;
long DAT_006116b8;
long DAT_006116c0;
long DAT_006116c8;
long DAT_006116d8;
long fde_004101b8;
long null_ARRAY_0040e300;
long null_ARRAY_0040e440;
long null_ARRAY_0040fa20;
long null_ARRAY_0040fc60;
long null_ARRAY_00611460;
long null_ARRAY_006114e0;
long null_ARRAY_00611540;
long null_ARRAY_00611640;
long null_ARRAY_00611680;
long PTR_DAT_00611400;
long PTR_null_ARRAY_00611458;
void
FUN_004020ef (uint uParm1)
{
  int iVar1;
  long lVar2;
  undefined8 uVar3;
  undefined **ppuVar4;
  char *pcVar5;
  undefined *puVar6;
  undefined *local_88;
  char *local_80;
  undefined8 local_78;
  char *local_70;
  char *local_68;
  char *local_60;
  char *local_58;
  char *local_50;
  char *local_48;
  char *local_40;
  char *local_38;
  char *local_30;
  undefined8 local_28;
  undefined8 local_20;

  ppuVar4 = &local_88;
  if (uParm1 != 0)
    {
      func_0x00401820 (DAT_006114a0,
		       "Try \'%s --help\' for more information.\n",
		       DAT_00611520);
      goto LAB_004022e3;
    }
  func_0x00401690
    ("Usage: %s [OPTION]... [FILE]\nBase%d encode or decode FILE, or standard input, to standard output.\n",
     DAT_00611520, 0x20);
  uVar3 = DAT_00611480;
  func_0x00401830
    ("\nWith no FILE, or when FILE is -, read standard input.\n",
     DAT_00611480);
  func_0x00401830
    ("\nMandatory arguments to long options are mandatory for short options too.\n",
     uVar3);
  func_0x00401830
    ("  -d, --decode          decode data\n  -i, --ignore-garbage  when decoding, ignore non-alphabet characters\n  -w, --wrap=COLS       wrap encoded lines after COLS character (default 76).\n                          Use 0 to disable line wrapping\n\n",
     uVar3);
  func_0x00401830 ("      --help     display this help and exit\n", uVar3);
  func_0x00401830 ("      --version  output version information and exit\n",
		   uVar3);
  func_0x00401690
    ("\nThe data are encoded as described for the %s alphabet in RFC 4648.\nWhen decoding, the input may contain newlines in addition to the bytes of\nthe formal %s alphabet.  Use --ignore-garbage to attempt to recover\nfrom any other non-alphabet bytes in the encoded stream.\n",
     "base32", "base32");
  local_88 = &DAT_0040ddac;
  local_80 = "test invocation";
  local_78 = 0x40de0f;
  local_70 = "Multi-call invocation";
  local_68 = "sha224sum";
  local_60 = "sha2 utilities";
  local_58 = "sha256sum";
  local_50 = "sha2 utilities";
  local_48 = "sha384sum";
  local_40 = "sha2 utilities";
  local_38 = "sha512sum";
  local_30 = "sha2 utilities";
  local_28 = 0;
  local_20 = 0;
  puVar6 = &DAT_0040ddac;
  do
    {
      iVar1 = func_0x00401940 ("base32", puVar6);
      if (iVar1 == 0)
	break;
      ppuVar4 = ppuVar4 + 2;
      puVar6 = *ppuVar4;
    }
  while (puVar6 != (undefined *) 0x0);
  pcVar5 = ppuVar4[1];
  if (pcVar5 == (char *) 0x0)
    {
      func_0x00401690 ("\n%s online help: <%s>\n", "GNU coreutils",
		       "https://www.gnu.org/software/coreutils/");
      lVar2 = func_0x004019a0 (5, 0);
      if (lVar2 == 0)
	goto LAB_004022ea;
      iVar1 = func_0x00401890 (lVar2, &DAT_0040de30, 3);
      if (iVar1 == 0)
	{
	  func_0x00401690 ("Full documentation at: <%s%s>\n",
			   "https://www.gnu.org/software/coreutils/",
			   "base32");
	  pcVar5 = "base32";
	  uVar3 = 0x40ddc8;
	  goto LAB_004022d1;
	}
      pcVar5 = "base32";
    LAB_0040228f:
      ;
      func_0x00401690
	("Report %s translation bugs to <https://translationproject.org/team/>\n",
	 "base32");
    }
  else
    {
      func_0x00401690 ("\n%s online help: <%s>\n", "GNU coreutils",
		       "https://www.gnu.org/software/coreutils/");
      lVar2 = func_0x004019a0 (5, 0);
      if ((lVar2 != 0)
	  && (iVar1 = func_0x00401890 (lVar2, &DAT_0040de30, 3), iVar1 != 0))
	goto LAB_0040228f;
    }
  func_0x00401690 ("Full documentation at: <%s%s>\n",
		   "https://www.gnu.org/software/coreutils/", "base32");
  uVar3 = 0x40f587;
  if (pcVar5 == "base32")
    {
      uVar3 = 0x40ddc8;
    }
LAB_004022d1:
  ;
  do
    {
      func_0x00401690
	("or available locally via: info \'(coreutils) %s%s\'\n", pcVar5,
	 uVar3);
    LAB_004022e3:
      ;
      func_0x00401a00 ((ulong) uParm1);
    LAB_004022ea:
      ;
      func_0x00401690 ("Full documentation at: <%s%s>\n",
		       "https://www.gnu.org/software/coreutils/", "base32");
      pcVar5 = "base32";
      uVar3 = 0x40ddc8;
    }
  while (true);
}

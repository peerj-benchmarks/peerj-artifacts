
#include "cat.h"

long null_ARRAY_0061074_0_8_;
long null_ARRAY_0061074_8_8_;
long null_ARRAY_0061094_0_8_;
long null_ARRAY_0061094_16_8_;
long null_ARRAY_0061094_24_8_;
long null_ARRAY_0061094_32_8_;
long null_ARRAY_0061094_40_8_;
long null_ARRAY_0061094_48_8_;
long null_ARRAY_0061094_8_8_;
long null_ARRAY_0061098_0_4_;
long null_ARRAY_0061098_16_8_;
long null_ARRAY_0061098_4_4_;
long null_ARRAY_0061098_8_4_;
long local_68_2_2_;
long local_6b_1_3_;
long local_6c_4_4_;
long local_6e_4_4_;
long local_6f_4_4_;
long local_70_0_2_;
long local_70_4_4_;
long local_71_0_1_;
long local_71_4_4_;
long local_72_4_4_;
long DAT_00000010;
long DAT_0040d6c0;
long DAT_0040d6c4;
long DAT_0040d748;
long DAT_0040de90;
long DAT_0040de94;
long DAT_0040de98;
long DAT_0040de9b;
long DAT_0040de9d;
long DAT_0040dea1;
long DAT_0040dea5;
long DAT_0040e62b;
long DAT_0040e9d5;
long DAT_0040ead9;
long DAT_0040eadf;
long DAT_0040eaf1;
long DAT_0040eaf2;
long DAT_0040eb10;
long DAT_0040eb14;
long DAT_0040eb9e;
long DAT_006102d8;
long DAT_006102e8;
long DAT_006102f8;
long DAT_006106c0;
long DAT_006106c8;
long DAT_006106f0;
long DAT_00610750;
long DAT_00610754;
long DAT_00610758;
long DAT_0061075c;
long DAT_00610780;
long DAT_00610790;
long DAT_006107a0;
long DAT_006107a8;
long DAT_006107c0;
long DAT_006107c8;
long DAT_00610810;
long DAT_00610814;
long DAT_00610818;
long DAT_00610820;
long DAT_00610828;
long DAT_00610830;
long DAT_006109b8;
long DAT_006109c0;
long DAT_006109c8;
long DAT_006109d8;
long fde_0040f710;
long null_ARRAY_0040dd00;
long null_ARRAY_0040efc0;
long null_ARRAY_0040f200;
long null_ARRAY_006106d0;
long null_ARRAY_00610740;
long null_ARRAY_006107e0;
long null_ARRAY_00610840;
long null_ARRAY_00610940;
long null_ARRAY_00610980;
long PTR_DAT_006106e8;
long PTR_null_ARRAY_00610738;
void
FUN_00401c53 (uint uParm1)
{
  int iVar1;
  long lVar2;
  undefined8 uVar3;
  undefined **ppuVar4;
  undefined *puVar5;
  undefined *local_88;
  char *local_80;
  undefined8 local_78;
  char *local_70;
  char *local_68;
  char *local_60;
  char *local_58;
  char *local_50;
  char *local_48;
  char *local_40;
  char *local_38;
  char *local_30;
  undefined8 local_28;
  undefined8 local_20;

  ppuVar4 = &local_88;
  if (uParm1 != 0)
    {
      func_0x004017c0 (DAT_006107a0,
		       "Try \'%s --help\' for more information.\n",
		       DAT_00610830);
      goto LAB_00401e51;
    }
  func_0x00401610 ("Usage: %s [OPTION]... [FILE]...\n", DAT_00610830);
  uVar3 = DAT_00610780;
  func_0x004017d0 ("Concatenate FILE(s) to standard output.\n", DAT_00610780);
  func_0x004017d0
    ("\nWith no FILE, or when FILE is -, read standard input.\n", uVar3);
  func_0x004017d0
    ("\n  -A, --show-all           equivalent to -vET\n  -b, --number-nonblank    number nonempty output lines, overrides -n\n  -e                       equivalent to -vE\n  -E, --show-ends          display $ at end of each line\n  -n, --number             number all output lines\n  -s, --squeeze-blank      suppress repeated empty output lines\n",
     uVar3);
  func_0x004017d0
    ("  -t                       equivalent to -vT\n  -T, --show-tabs          display TAB characters as ^I\n  -u                       (ignored)\n  -v, --show-nonprinting   use ^ and M- notation, except for LFD and TAB\n",
     uVar3);
  func_0x004017d0 ("      --help     display this help and exit\n", uVar3);
  func_0x004017d0 ("      --version  output version information and exit\n",
		   uVar3);
  func_0x00401610
    ("\nExamples:\n  %s f - g  Output f\'s contents, then standard input, then g\'s contents.\n  %s        Copy standard input to standard output.\n",
     DAT_00610830, DAT_00610830);
  local_88 = &DAT_0040d6c4;
  local_80 = "test invocation";
  local_78 = 0x40d727;
  local_70 = "Multi-call invocation";
  local_68 = "sha224sum";
  local_60 = "sha2 utilities";
  local_58 = "sha256sum";
  local_50 = "sha2 utilities";
  local_48 = "sha384sum";
  local_40 = "sha2 utilities";
  local_38 = "sha512sum";
  local_30 = "sha2 utilities";
  local_28 = 0;
  local_20 = 0;
  puVar5 = &DAT_0040d6c4;
  do
    {
      iVar1 = func_0x004018c0 (&DAT_0040d6c0, puVar5);
      if (iVar1 == 0)
	break;
      ppuVar4 = ppuVar4 + 2;
      puVar5 = *ppuVar4;
    }
  while (puVar5 != (undefined *) 0x0);
  puVar5 = ppuVar4[1];
  if (puVar5 == (undefined *) 0x0)
    {
      func_0x00401610 ("\n%s online help: <%s>\n", "GNU coreutils",
		       "https://www.gnu.org/software/coreutils/");
      lVar2 = func_0x00401920 (5, 0);
      if (lVar2 == 0)
	goto LAB_00401e58;
      iVar1 = func_0x00401840 (lVar2, &DAT_0040d748, 3);
      if (iVar1 == 0)
	{
	  func_0x00401610 ("Full documentation at: <%s%s>\n",
			   "https://www.gnu.org/software/coreutils/",
			   &DAT_0040d6c0);
	  puVar5 = &DAT_0040d6c0;
	  uVar3 = 0x40d6e0;
	  goto LAB_00401e3f;
	}
      puVar5 = &DAT_0040d6c0;
    LAB_00401dfd:
      ;
      func_0x00401610
	("Report %s translation bugs to <https://translationproject.org/team/>\n",
	 &DAT_0040d6c0);
    }
  else
    {
      func_0x00401610 ("\n%s online help: <%s>\n", "GNU coreutils",
		       "https://www.gnu.org/software/coreutils/");
      lVar2 = func_0x00401920 (5, 0);
      if ((lVar2 != 0)
	  && (iVar1 = func_0x00401840 (lVar2, &DAT_0040d748, 3), iVar1 != 0))
	goto LAB_00401dfd;
    }
  func_0x00401610 ("Full documentation at: <%s%s>\n",
		   "https://www.gnu.org/software/coreutils/", &DAT_0040d6c0);
  uVar3 = 0x40eb0f;
  if (puVar5 == &DAT_0040d6c0)
    {
      uVar3 = 0x40d6e0;
    }
LAB_00401e3f:
  ;
  do
    {
      func_0x00401610
	("or available locally via: info \'(coreutils) %s%s\'\n", puVar5,
	 uVar3);
    LAB_00401e51:
      ;
      func_0x00401960 ((ulong) uParm1);
    LAB_00401e58:
      ;
      func_0x00401610 ("Full documentation at: <%s%s>\n",
		       "https://www.gnu.org/software/coreutils/",
		       &DAT_0040d6c0);
      puVar5 = &DAT_0040d6c0;
      uVar3 = 0x40d6e0;
    }
  while (true);
}


#include "nproc.h"

long null_ARRAY_0061044_0_8_;
long null_ARRAY_0061044_8_8_;
long null_ARRAY_0061064_0_8_;
long null_ARRAY_0061064_16_8_;
long null_ARRAY_0061064_24_8_;
long null_ARRAY_0061064_32_8_;
long null_ARRAY_0061064_40_8_;
long null_ARRAY_0061064_48_8_;
long null_ARRAY_0061064_8_8_;
long null_ARRAY_0061068_0_4_;
long null_ARRAY_0061068_16_8_;
long null_ARRAY_0061068_4_4_;
long null_ARRAY_0061068_8_4_;
long local_68_2_2_;
long local_6b_1_3_;
long local_6c_4_4_;
long local_6e_4_4_;
long local_6f_4_4_;
long local_70_0_2_;
long local_70_4_4_;
long local_71_0_1_;
long local_71_4_4_;
long local_72_4_4_;
long DAT_00000010;
long DAT_0040d05d;
long DAT_0040d0e1;
long DAT_0040d117;
long DAT_0040d4e0;
long DAT_0040d4e4;
long DAT_0040d4e8;
long DAT_0040d4eb;
long DAT_0040d4ed;
long DAT_0040d4f1;
long DAT_0040d4f5;
long DAT_0040dcab;
long DAT_0040e3c8;
long DAT_0040e4d1;
long DAT_0040e4d7;
long DAT_0040e4e9;
long DAT_0040e4ea;
long DAT_0040e508;
long DAT_0040e50c;
long DAT_0040e596;
long DAT_00610000;
long DAT_00610010;
long DAT_00610020;
long DAT_006103e8;
long DAT_00610450;
long DAT_00610454;
long DAT_00610458;
long DAT_0061045c;
long DAT_00610480;
long DAT_00610490;
long DAT_006104a0;
long DAT_006104a8;
long DAT_006104c0;
long DAT_006104c8;
long DAT_00610510;
long DAT_00610518;
long DAT_00610520;
long DAT_006106b8;
long DAT_006106c0;
long DAT_006106c8;
long DAT_006106d8;
long fde_0040f0f0;
long null_ARRAY_0040d3c0;
long null_ARRAY_0040e9a0;
long null_ARRAY_0040ebe0;
long null_ARRAY_00610440;
long null_ARRAY_006104e0;
long null_ARRAY_00610540;
long null_ARRAY_00610640;
long null_ARRAY_00610680;
long PTR_DAT_006103e0;
long PTR_null_ARRAY_00610438;
void
FUN_00401b7e (uint uParm1)
{
  int iVar1;
  long lVar2;
  undefined8 uVar3;
  undefined **ppuVar4;
  char *pcVar5;
  undefined *puVar6;
  undefined *local_88;
  char *local_80;
  undefined8 local_78;
  char *local_70;
  char *local_68;
  char *local_60;
  char *local_58;
  char *local_50;
  char *local_48;
  char *local_40;
  char *local_38;
  char *local_30;
  undefined8 local_28;
  undefined8 local_20;

  ppuVar4 = &local_88;
  if (uParm1 != 0)
    {
      func_0x00401740 (DAT_006104a0,
		       "Try \'%s --help\' for more information.\n",
		       DAT_00610520);
      goto LAB_00401d49;
    }
  func_0x004015b0 ("Usage: %s [OPTION]...\n", DAT_00610520);
  uVar3 = DAT_00610480;
  func_0x00401750
    ("Print the number of processing units available to the current process,\nwhich may be less than the number of online processors\n\n",
     DAT_00610480);
  func_0x00401750
    ("      --all      print the number of installed processors\n      --ignore=N  if possible, exclude N processing units\n",
     uVar3);
  func_0x00401750 ("      --help     display this help and exit\n", uVar3);
  func_0x00401750 ("      --version  output version information and exit\n",
		   uVar3);
  local_88 = &DAT_0040d05d;
  local_80 = "test invocation";
  local_78 = 0x40d0c0;
  local_70 = "Multi-call invocation";
  local_68 = "sha224sum";
  local_60 = "sha2 utilities";
  local_58 = "sha256sum";
  local_50 = "sha2 utilities";
  local_48 = "sha384sum";
  local_40 = "sha2 utilities";
  local_38 = "sha512sum";
  local_30 = "sha2 utilities";
  local_28 = 0;
  local_20 = 0;
  puVar6 = &DAT_0040d05d;
  do
    {
      iVar1 = func_0x00401840 ("nproc", puVar6);
      if (iVar1 == 0)
	break;
      ppuVar4 = ppuVar4 + 2;
      puVar6 = *ppuVar4;
    }
  while (puVar6 != (undefined *) 0x0);
  pcVar5 = ppuVar4[1];
  if (pcVar5 == (char *) 0x0)
    {
      func_0x004015b0 ("\n%s online help: <%s>\n", "GNU coreutils",
		       "https://www.gnu.org/software/coreutils/");
      lVar2 = func_0x004018a0 (5, 0);
      if (lVar2 == 0)
	goto LAB_00401d50;
      iVar1 = func_0x004017a0 (lVar2, &DAT_0040d0e1, 3);
      if (iVar1 == 0)
	{
	  func_0x004015b0 ("Full documentation at: <%s%s>\n",
			   "https://www.gnu.org/software/coreutils/",
			   "nproc");
	  pcVar5 = "nproc";
	  uVar3 = 0x40d079;
	  goto LAB_00401d37;
	}
      pcVar5 = "nproc";
    LAB_00401cf5:
      ;
      func_0x004015b0
	("Report %s translation bugs to <https://translationproject.org/team/>\n",
	 "nproc");
    }
  else
    {
      func_0x004015b0 ("\n%s online help: <%s>\n", "GNU coreutils",
		       "https://www.gnu.org/software/coreutils/");
      lVar2 = func_0x004018a0 (5, 0);
      if ((lVar2 != 0)
	  && (iVar1 = func_0x004017a0 (lVar2, &DAT_0040d0e1, 3), iVar1 != 0))
	goto LAB_00401cf5;
    }
  func_0x004015b0 ("Full documentation at: <%s%s>\n",
		   "https://www.gnu.org/software/coreutils/", "nproc");
  uVar3 = 0x40e507;
  if (pcVar5 == "nproc")
    {
      uVar3 = 0x40d079;
    }
LAB_00401d37:
  ;
  do
    {
      func_0x004015b0
	("or available locally via: info \'(coreutils) %s%s\'\n", pcVar5,
	 uVar3);
    LAB_00401d49:
      ;
      func_0x004018f0 ((ulong) uParm1);
    LAB_00401d50:
      ;
      func_0x004015b0 ("Full documentation at: <%s%s>\n",
		       "https://www.gnu.org/software/coreutils/", "nproc");
      pcVar5 = "nproc";
      uVar3 = 0x40d079;
    }
  while (true);
}

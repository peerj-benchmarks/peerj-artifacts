
#include "expr.h"

long null_ARRAY_006224c_0_8_;
long null_ARRAY_006224c_8_8_;
long null_ARRAY_006226c_0_8_;
long null_ARRAY_006226c_16_8_;
long null_ARRAY_006226c_24_8_;
long null_ARRAY_006226c_32_8_;
long null_ARRAY_006226c_40_8_;
long null_ARRAY_006226c_48_8_;
long null_ARRAY_006226c_8_8_;
long null_ARRAY_0062270_0_4_;
long null_ARRAY_0062270_16_8_;
long null_ARRAY_0062270_4_4_;
long null_ARRAY_0062270_8_4_;
long local_10_1_7_;
long local_11_0_1_;
long local_68_2_2_;
long local_6b_1_3_;
long local_6c_4_4_;
long local_6e_4_4_;
long local_6f_4_4_;
long local_70_0_2_;
long local_70_4_4_;
long local_71_0_1_;
long local_71_4_4_;
long local_72_4_4_;
long local_9_0_4_;
long local_a_0_1_;
long local_a_0_4_;
long DAT_00000010;
long DAT_00000022;
long DAT_0000002a;
long DAT_0000002b;
long DAT_0000002d;
long DAT_0000002f;
long DAT_0041bc00;
long DAT_0041bc05;
long DAT_0041bc89;
long DAT_0041bc8b;
long DAT_0041bc8d;
long DAT_0041bce0;
long DAT_0041bce2;
long DAT_0041bcfe;
long DAT_0041bd1d;
long DAT_0041bd1f;
long DAT_0041bd47;
long DAT_0041bd49;
long DAT_0041bd4c;
long DAT_0041bd4f;
long DAT_0041bd50;
long DAT_0041bd52;
long DAT_0041bd55;
long DAT_0041bd70;
long DAT_0041bd72;
long DAT_0041bd9e;
long DAT_0041bd9f;
long DAT_0041c930;
long DAT_0041c936;
long DAT_0041c938;
long DAT_0041c93c;
long DAT_0041c940;
long DAT_0041c943;
long DAT_0041c945;
long DAT_0041c949;
long DAT_0041c94d;
long DAT_0041d0eb;
long DAT_0041d0ed;
long DAT_0041d808;
long DAT_0041d817;
long DAT_0041d945;
long DAT_0041e5c7;
long DAT_0041e5e0;
long DAT_0041e654;
long DAT_0041e6de;
long DAT_00622000;
long DAT_00622010;
long DAT_00622020;
long DAT_00622460;
long DAT_006224d0;
long DAT_006224d4;
long DAT_006224d8;
long DAT_006224dc;
long DAT_00622500;
long DAT_00622510;
long DAT_00622520;
long DAT_00622528;
long DAT_00622540;
long DAT_00622548;
long DAT_00622590;
long DAT_00622598;
long DAT_006225a0;
long DAT_006225a8;
long DAT_00622738;
long DAT_00622740;
long DAT_00622748;
long DAT_00622758;
long DAT_00622760;
long fde_0041f630;
long null_ARRAY_0041c840;
long null_ARRAY_0041c8a0;
long null_ARRAY_0041e2c0;
long null_ARRAY_0041e300;
long null_ARRAY_0041eb00;
long null_ARRAY_0041ed40;
long null_ARRAY_006224c0;
long null_ARRAY_00622560;
long null_ARRAY_006225c0;
long null_ARRAY_006226c0;
long null_ARRAY_00622700;
long PTR_null_ARRAY_006224b8;
void
FUN_004020cc (uint uParm1)
{
  int iVar1;
  long lVar2;
  undefined8 uVar3;
  undefined **ppuVar4;
  undefined *puVar5;
  undefined *local_88;
  char *local_80;
  undefined8 local_78;
  char *local_70;
  char *local_68;
  char *local_60;
  char *local_58;
  char *local_50;
  char *local_48;
  char *local_40;
  char *local_38;
  char *local_30;
  undefined8 local_28;
  undefined8 local_20;

  ppuVar4 = &local_88;
  if (uParm1 != 0)
    {
      func_0x00401b50 (DAT_00622520,
		       "Try \'%s --help\' for more information.\n",
		       DAT_006225a8);
      goto LAB_004022f2;
    }
  func_0x00401940 ("Usage: %s EXPRESSION\n  or:  %s OPTION\n", DAT_006225a8,
		   DAT_006225a8);
  func_0x00401d10 (10);
  uVar3 = DAT_00622500;
  func_0x00401b60 ("      --help     display this help and exit\n",
		   DAT_00622500);
  func_0x00401b60 ("      --version  output version information and exit\n",
		   uVar3);
  func_0x00401b60
    ("\nPrint the value of EXPRESSION to standard output.  A blank line below\nseparates increasing precedence groups.  EXPRESSION may be:\n\n  ARG1 | ARG2       ARG1 if it is neither null nor 0, otherwise ARG2\n\n  ARG1 & ARG2       ARG1 if neither argument is null or 0, otherwise 0\n",
     uVar3);
  func_0x00401b60
    ("\n  ARG1 < ARG2       ARG1 is less than ARG2\n  ARG1 <= ARG2      ARG1 is less than or equal to ARG2\n  ARG1 = ARG2       ARG1 is equal to ARG2\n  ARG1 != ARG2      ARG1 is unequal to ARG2\n  ARG1 >= ARG2      ARG1 is greater than or equal to ARG2\n  ARG1 > ARG2       ARG1 is greater than ARG2\n",
     uVar3);
  func_0x00401b60
    ("\n  ARG1 + ARG2       arithmetic sum of ARG1 and ARG2\n  ARG1 - ARG2       arithmetic difference of ARG1 and ARG2\n",
     uVar3);
  func_0x00401b60
    ("\n  ARG1 * ARG2       arithmetic product of ARG1 and ARG2\n  ARG1 / ARG2       arithmetic quotient of ARG1 divided by ARG2\n  ARG1 % ARG2       arithmetic remainder of ARG1 divided by ARG2\n",
     uVar3);
  func_0x00401b60
    ("\n  STRING : REGEXP   anchored pattern match of REGEXP in STRING\n\n  match STRING REGEXP        same as STRING : REGEXP\n  substr STRING POS LENGTH   substring of STRING, POS counted from 1\n  index STRING CHARS         index in STRING where any CHARS is found, or 0\n  length STRING              length of STRING\n",
     uVar3);
  func_0x00401b60
    ("  + TOKEN                    interpret TOKEN as a string, even if it is a\n                               keyword like \'match\' or an operator like \'/\'\n\n  ( EXPRESSION )             value of EXPRESSION\n",
     uVar3);
  func_0x00401b60
    ("\nBeware that many operators need to be escaped or quoted for shells.\nComparisons are arithmetic if both ARGs are numbers, else lexicographical.\nPattern matches return the string matched between \\( and \\) or null; if\n\\( and \\) are not used, they return the number of characters matched or 0.\n",
     uVar3);
  func_0x00401b60
    ("\nExit status is 0 if EXPRESSION is neither null nor 0, 1 if EXPRESSION is null\nor 0, 2 if EXPRESSION is syntactically invalid, and 3 if an error occurred.\n",
     uVar3);
  local_88 = &DAT_0041bc05;
  local_80 = "test invocation";
  local_78 = 0x41bc68;
  local_70 = "Multi-call invocation";
  local_68 = "sha224sum";
  local_60 = "sha2 utilities";
  local_58 = "sha256sum";
  local_50 = "sha2 utilities";
  local_48 = "sha384sum";
  local_40 = "sha2 utilities";
  local_38 = "sha512sum";
  local_30 = "sha2 utilities";
  local_28 = 0;
  local_20 = 0;
  puVar5 = &DAT_0041bc05;
  do
    {
      iVar1 = func_0x00401c90 (&DAT_0041bc00, puVar5);
      if (iVar1 == 0)
	break;
      ppuVar4 = ppuVar4 + 2;
      puVar5 = *ppuVar4;
    }
  while (puVar5 != (undefined *) 0x0);
  puVar5 = ppuVar4[1];
  if (puVar5 == (undefined *) 0x0)
    {
      func_0x00401940 ("\n%s online help: <%s>\n", "GNU coreutils",
		       "https://www.gnu.org/software/coreutils/");
      lVar2 = func_0x00401d00 (5, 0);
      if (lVar2 == 0)
	goto LAB_004022f9;
      iVar1 = func_0x00401bd0 (lVar2, &DAT_0041bc89, 3);
      if (iVar1 == 0)
	{
	  func_0x00401940 ("Full documentation at: <%s%s>\n",
			   "https://www.gnu.org/software/coreutils/",
			   &DAT_0041bc00);
	  puVar5 = &DAT_0041bc00;
	  uVar3 = 0x41bc21;
	  goto LAB_004022e0;
	}
      puVar5 = &DAT_0041bc00;
    LAB_0040229e:
      ;
      func_0x00401940
	("Report %s translation bugs to <https://translationproject.org/team/>\n",
	 &DAT_0041bc00);
    }
  else
    {
      func_0x00401940 ("\n%s online help: <%s>\n", "GNU coreutils",
		       "https://www.gnu.org/software/coreutils/");
      lVar2 = func_0x00401d00 (5, 0);
      if ((lVar2 != 0)
	  && (iVar1 = func_0x00401bd0 (lVar2, &DAT_0041bc89, 3), iVar1 != 0))
	goto LAB_0040229e;
    }
  func_0x00401940 ("Full documentation at: <%s%s>\n",
		   "https://www.gnu.org/software/coreutils/", &DAT_0041bc00);
  uVar3 = 0x41d944;
  if (puVar5 == &DAT_0041bc00)
    {
      uVar3 = 0x41bc21;
    }
LAB_004022e0:
  ;
  do
    {
      func_0x00401940
	("or available locally via: info \'(coreutils) %s%s\'\n", puVar5,
	 uVar3);
    LAB_004022f2:
      ;
      func_0x00401d50 ((ulong) uParm1);
    LAB_004022f9:
      ;
      func_0x00401940 ("Full documentation at: <%s%s>\n",
		       "https://www.gnu.org/software/coreutils/",
		       &DAT_0041bc00);
      puVar5 = &DAT_0041bc00;
      uVar3 = 0x41bc21;
    }
  while (true);
}


#include "fold.h"

long null_ARRAY_0061066_0_8_;
long null_ARRAY_0061066_8_8_;
long null_ARRAY_0061084_0_8_;
long null_ARRAY_0061084_16_8_;
long null_ARRAY_0061084_24_8_;
long null_ARRAY_0061084_32_8_;
long null_ARRAY_0061084_40_8_;
long null_ARRAY_0061084_48_8_;
long null_ARRAY_0061084_8_8_;
long null_ARRAY_0061088_0_4_;
long null_ARRAY_0061088_16_8_;
long null_ARRAY_0061088_4_4_;
long null_ARRAY_0061088_8_4_;
long local_68_2_2_;
long local_6b_1_3_;
long local_6c_4_4_;
long local_6e_4_4_;
long local_6f_4_4_;
long local_70_0_2_;
long local_70_4_4_;
long local_71_0_1_;
long local_71_4_4_;
long local_72_4_4_;
long DAT_00000010;
long DAT_0040d4c0;
long DAT_0040d4c5;
long DAT_0040d549;
long DAT_0040d9c0;
long DAT_0040d9c4;
long DAT_0040d9c8;
long DAT_0040d9cb;
long DAT_0040d9cd;
long DAT_0040d9d1;
long DAT_0040d9d5;
long DAT_0040e16b;
long DAT_0040e888;
long DAT_0040e991;
long DAT_0040e997;
long DAT_0040e9a9;
long DAT_0040e9aa;
long DAT_0040e9c8;
long DAT_0040e9cc;
long DAT_0040ea56;
long DAT_00610210;
long DAT_00610220;
long DAT_00610230;
long DAT_00610608;
long DAT_00610670;
long DAT_00610674;
long DAT_00610678;
long DAT_0061067c;
long DAT_00610680;
long DAT_00610688;
long DAT_00610690;
long DAT_006106a0;
long DAT_006106a8;
long DAT_006106c0;
long DAT_006106c8;
long DAT_00610710;
long DAT_00610718;
long DAT_00610720;
long DAT_00610721;
long DAT_00610722;
long DAT_00610728;
long DAT_00610730;
long DAT_00610738;
long DAT_006108b8;
long DAT_006108c0;
long DAT_006108c8;
long DAT_006108d8;
long fde_0040f5d0;
long null_ARRAY_0040d880;
long null_ARRAY_0040d940;
long null_ARRAY_0040ee60;
long null_ARRAY_0040f0a0;
long null_ARRAY_00610660;
long null_ARRAY_006106e0;
long null_ARRAY_00610740;
long null_ARRAY_00610840;
long null_ARRAY_00610880;
long PTR_DAT_00610600;
long PTR_null_ARRAY_00610658;
void
FUN_00401ff1 (uint uParm1)
{
  int iVar1;
  long lVar2;
  undefined8 uVar3;
  undefined **ppuVar4;
  undefined *puVar5;
  undefined *local_88;
  char *local_80;
  undefined8 local_78;
  char *local_70;
  char *local_68;
  char *local_60;
  char *local_58;
  char *local_50;
  char *local_48;
  char *local_40;
  char *local_38;
  char *local_30;
  undefined8 local_28;
  undefined8 local_20;

  ppuVar4 = &local_88;
  if (uParm1 != 0)
    {
      func_0x004017f0 (DAT_006106a0,
		       "Try \'%s --help\' for more information.\n",
		       DAT_00610738);
      goto LAB_004021d6;
    }
  func_0x00401650 ("Usage: %s [OPTION]... [FILE]...\n", DAT_00610738);
  uVar3 = DAT_00610680;
  func_0x00401800
    ("Wrap input lines in each FILE, writing to standard output.\n",
     DAT_00610680);
  func_0x00401800
    ("\nWith no FILE, or when FILE is -, read standard input.\n", uVar3);
  func_0x00401800
    ("\nMandatory arguments to long options are mandatory for short options too.\n",
     uVar3);
  func_0x00401800
    ("  -b, --bytes         count bytes rather than columns\n  -s, --spaces        break at spaces\n  -w, --width=WIDTH   use WIDTH columns instead of 80\n",
     uVar3);
  func_0x00401800 ("      --help     display this help and exit\n", uVar3);
  func_0x00401800 ("      --version  output version information and exit\n",
		   uVar3);
  local_88 = &DAT_0040d4c5;
  local_80 = "test invocation";
  local_78 = 0x40d528;
  local_70 = "Multi-call invocation";
  local_68 = "sha224sum";
  local_60 = "sha2 utilities";
  local_58 = "sha256sum";
  local_50 = "sha2 utilities";
  local_48 = "sha384sum";
  local_40 = "sha2 utilities";
  local_38 = "sha512sum";
  local_30 = "sha2 utilities";
  local_28 = 0;
  local_20 = 0;
  puVar5 = &DAT_0040d4c5;
  do
    {
      iVar1 = func_0x00401900 (&DAT_0040d4c0, puVar5);
      if (iVar1 == 0)
	break;
      ppuVar4 = ppuVar4 + 2;
      puVar5 = *ppuVar4;
    }
  while (puVar5 != (undefined *) 0x0);
  puVar5 = ppuVar4[1];
  if (puVar5 == (undefined *) 0x0)
    {
      func_0x00401650 ("\n%s online help: <%s>\n", "GNU coreutils",
		       "https://www.gnu.org/software/coreutils/");
      lVar2 = func_0x00401960 (5, 0);
      if (lVar2 == 0)
	goto LAB_004021dd;
      iVar1 = func_0x00401860 (lVar2, &DAT_0040d549, 3);
      if (iVar1 == 0)
	{
	  func_0x00401650 ("Full documentation at: <%s%s>\n",
			   "https://www.gnu.org/software/coreutils/",
			   &DAT_0040d4c0);
	  puVar5 = &DAT_0040d4c0;
	  uVar3 = 0x40d4e1;
	  goto LAB_004021c4;
	}
      puVar5 = &DAT_0040d4c0;
    LAB_00402182:
      ;
      func_0x00401650
	("Report %s translation bugs to <https://translationproject.org/team/>\n",
	 &DAT_0040d4c0);
    }
  else
    {
      func_0x00401650 ("\n%s online help: <%s>\n", "GNU coreutils",
		       "https://www.gnu.org/software/coreutils/");
      lVar2 = func_0x00401960 (5, 0);
      if ((lVar2 != 0)
	  && (iVar1 = func_0x00401860 (lVar2, &DAT_0040d549, 3), iVar1 != 0))
	goto LAB_00402182;
    }
  func_0x00401650 ("Full documentation at: <%s%s>\n",
		   "https://www.gnu.org/software/coreutils/", &DAT_0040d4c0);
  uVar3 = 0x40e9c7;
  if (puVar5 == &DAT_0040d4c0)
    {
      uVar3 = 0x40d4e1;
    }
LAB_004021c4:
  ;
  do
    {
      func_0x00401650
	("or available locally via: info \'(coreutils) %s%s\'\n", puVar5,
	 uVar3);
    LAB_004021d6:
      ;
      func_0x004019c0 ((ulong) uParm1);
    LAB_004021dd:
      ;
      func_0x00401650 ("Full documentation at: <%s%s>\n",
		       "https://www.gnu.org/software/coreutils/",
		       &DAT_0040d4c0);
      puVar5 = &DAT_0040d4c0;
      uVar3 = 0x40d4e1;
    }
  while (true);
}

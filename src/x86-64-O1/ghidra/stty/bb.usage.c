
#include "stty.h"

long null_ARRAY_0061648_0_8_;
long null_ARRAY_0061648_8_8_;
long null_ARRAY_006165c_0_4_;
long null_ARRAY_006165c_12_4_;
long null_ARRAY_006165c_4_4_;
long null_ARRAY_006165c_8_4_;
long null_ARRAY_0061678_0_8_;
long null_ARRAY_0061678_16_8_;
long null_ARRAY_0061678_24_8_;
long null_ARRAY_0061678_32_8_;
long null_ARRAY_0061678_40_8_;
long null_ARRAY_0061678_48_8_;
long null_ARRAY_0061678_8_8_;
long null_ARRAY_006167c_0_4_;
long null_ARRAY_006167c_16_8_;
long null_ARRAY_006167c_4_4_;
long null_ARRAY_006167c_8_4_;
long local_68_2_2_;
long local_6b_1_3_;
long local_6c_4_4_;
long local_6e_4_4_;
long local_6f_4_4_;
long local_70_0_2_;
long local_70_4_4_;
long local_71_0_1_;
long local_71_4_4_;
long local_72_4_4_;
long DAT_00000010;
long DAT_0040f682;
long DAT_0040f6a1;
long DAT_0040f776;
long DAT_0040f7fa;
long DAT_0040f805;
long DAT_0040f811;
long DAT_0040f816;
long DAT_0040f819;
long DAT_0040f843;
long DAT_0040f856;
long DAT_0040f85b;
long DAT_0040f85f;
long DAT_0040f88e;
long DAT_0040f892;
long DAT_0040f897;
long DAT_0040f8ae;
long DAT_0040f8b3;
long DAT_0040f8c0;
long DAT_0040f8c5;
long DAT_0040f949;
long DAT_0040f9eb;
long DAT_0040fac8;
long DAT_0040faf7;
long DAT_0040fba9;
long DAT_00412e70;
long DAT_00412e74;
long DAT_00412e78;
long DAT_00412e7b;
long DAT_00412e7d;
long DAT_00412e81;
long DAT_00412e85;
long DAT_0041362b;
long DAT_00414080;
long DAT_00414189;
long DAT_0041418f;
long DAT_004141a1;
long DAT_004141a2;
long DAT_004141c0;
long DAT_004141c4;
long DAT_0041424e;
long DAT_00616000;
long DAT_00616010;
long DAT_00616020;
long DAT_00616420;
long DAT_00616430;
long DAT_00616490;
long DAT_00616494;
long DAT_00616498;
long DAT_0061649c;
long DAT_006164c0;
long DAT_006164d0;
long DAT_006164e0;
long DAT_006164e8;
long DAT_00616500;
long DAT_00616508;
long DAT_0061663c;
long DAT_00616640;
long DAT_00616648;
long DAT_00616650;
long DAT_00616658;
long DAT_006167b8;
long DAT_006167f8;
long DAT_00616800;
long DAT_00616808;
long DAT_00616818;
long fde_00414e28;
long null_ARRAY_0040fc80;
long null_ARRAY_00410000;
long null_ARRAY_004100c0;
long null_ARRAY_004102c0;
long null_ARRAY_00414660;
long null_ARRAY_004148a0;
long null_ARRAY_00616480;
long null_ARRAY_00616520;
long null_ARRAY_00616560;
long null_ARRAY_00616580;
long null_ARRAY_006165c0;
long null_ARRAY_00616600;
long null_ARRAY_00616680;
long null_ARRAY_00616780;
long null_ARRAY_006167c0;
long PTR_DAT_00616428;
long PTR_null_ARRAY_00616478;
void
FUN_0040231d (uint uParm1)
{
  int iVar1;
  long lVar2;
  undefined8 uVar3;
  undefined **ppuVar4;
  undefined *puVar5;
  undefined *local_88;
  char *local_80;
  undefined8 local_78;
  char *local_70;
  char *local_68;
  char *local_60;
  char *local_58;
  char *local_50;
  char *local_48;
  char *local_40;
  char *local_38;
  char *local_30;
  undefined8 local_28;
  undefined8 local_20;

  ppuVar4 = &local_88;
  if (uParm1 != 0)
    {
      func_0x00401930 (DAT_006164e0,
		       "Try \'%s --help\' for more information.\n",
		       DAT_00616658);
      goto LAB_0040290d;
    }
  func_0x00401750
    ("Usage: %s [-F DEVICE | --file=DEVICE] [SETTING]...\n  or:  %s [-F DEVICE | --file=DEVICE] [-a|--all]\n  or:  %s [-F DEVICE | --file=DEVICE] [-g|--save]\n",
     DAT_00616658, DAT_00616658, DAT_00616658);
  uVar3 = DAT_006164c0;
  func_0x00401940 ("Print or change terminal characteristics.\n",
		   DAT_006164c0);
  func_0x00401940
    ("\nMandatory arguments to long options are mandatory for short options too.\n",
     uVar3);
  func_0x00401940
    ("  -a, --all          print all current settings in human-readable form\n  -g, --save         print all current settings in a stty-readable form\n  -F, --file=DEVICE  open and use the specified DEVICE instead of stdin\n",
     uVar3);
  func_0x00401940 ("      --help     display this help and exit\n", uVar3);
  func_0x00401940 ("      --version  output version information and exit\n",
		   uVar3);
  func_0x00401940
    ("\nOptional - before SETTING indicates negation.  An * marks non-POSIX\nsettings.  The underlying system defines which settings are available.\n",
     uVar3);
  func_0x00401940 ("\nSpecial characters:\n", uVar3);
  func_0x00401940 (" * discard CHAR  CHAR will toggle discarding of output\n",
		   uVar3);
  func_0x00401940
    ("   eof CHAR      CHAR will send an end of file (terminate the input)\n   eol CHAR      CHAR will end the line\n",
     uVar3);
  func_0x00401940 (" * eol2 CHAR     alternate CHAR for ending the line\n",
		   uVar3);
  func_0x00401940
    ("   erase CHAR    CHAR will erase the last character typed\n   intr CHAR     CHAR will send an interrupt signal\n   kill CHAR     CHAR will erase the current line\n",
     uVar3);
  func_0x00401940
    (" * lnext CHAR    CHAR will enter the next character quoted\n", uVar3);
  func_0x00401940 ("   quit CHAR     CHAR will send a quit signal\n", uVar3);
  func_0x00401940 (" * rprnt CHAR    CHAR will redraw the current line\n",
		   uVar3);
  func_0x00401940
    ("   start CHAR    CHAR will restart the output after stopping it\n   stop CHAR     CHAR will stop the output\n   susp CHAR     CHAR will send a terminal stop signal\n",
     uVar3);
  func_0x00401940
    (" * swtch CHAR    CHAR will switch to a different shell layer\n", uVar3);
  func_0x00401940 (" * werase CHAR   CHAR will erase the last word typed\n",
		   uVar3);
  func_0x00401940
    ("\nSpecial settings:\n   N             set the input and output speeds to N bauds\n",
     uVar3);
  func_0x00401940
    (" * cols N        tell the kernel that the terminal has N columns\n * columns N     same as cols N\n",
     uVar3);
  puVar5 = &DAT_0040faf7;
  if (DAT_00616420 == 1)
    {
      puVar5 = (undefined *) 0x40fa3d;
    }
  func_0x00401750
    (" * [-]drain      wait for transmission before applying settings (%s by default)\n",
     puVar5);
  func_0x00401940 ("   ispeed N      set the input speed to N\n", uVar3);
  func_0x00401940 (" * line N        use line discipline N\n", uVar3);
  func_0x00401940
    ("   min N         with -icanon, set N characters minimum for a completed read\n   ospeed N      set the output speed to N\n",
     uVar3);
  func_0x00401940
    (" * rows N        tell the kernel that the terminal has N rows\n * size          print the number of rows and columns according to the kernel\n",
     uVar3);
  func_0x00401940
    ("   speed         print the terminal speed\n   time N        with -icanon, set read timeout of N tenths of a second\n",
     uVar3);
  func_0x00401940
    ("\nControl settings:\n   [-]clocal     disable modem control signals\n   [-]cread      allow input to be received\n",
     uVar3);
  func_0x00401940 (" * [-]crtscts    enable RTS/CTS handshaking\n", uVar3);
  func_0x00401940
    ("   csN           set character size to N bits, N in [5..8]\n", uVar3);
  func_0x00401940
    ("   [-]cstopb     use two stop bits per character (one with \'-\')\n   [-]hup        send a hangup signal when the last process closes the tty\n   [-]hupcl      same as [-]hup\n   [-]parenb     generate parity bit in output and expect parity bit in input\n   [-]parodd     set odd parity (or even parity with \'-\')\n",
     uVar3);
  func_0x00401940
    ("\nInput settings:\n   [-]brkint     breaks cause an interrupt signal\n   [-]icrnl      translate carriage return to newline\n   [-]ignbrk     ignore break characters\n   [-]igncr      ignore carriage return\n   [-]ignpar     ignore characters with parity errors\n",
     uVar3);
  func_0x00401940
    (" * [-]imaxbel    beep and do not flush a full input buffer on a character\n",
     uVar3);
  func_0x00401940
    ("   [-]inlcr      translate newline to carriage return\n   [-]inpck      enable input parity checking\n   [-]istrip     clear high (8th) bit of input characters\n",
     uVar3);
  func_0x00401940
    (" * [-]iutf8      assume input characters are UTF-8 encoded\n", uVar3);
  func_0x00401940
    (" * [-]iuclc      translate uppercase characters to lowercase\n", uVar3);
  func_0x00401940
    (" * [-]ixany      let any character restart output, not only start character\n",
     uVar3);
  func_0x00401940
    ("   [-]ixoff      enable sending of start/stop characters\n   [-]ixon       enable XON/XOFF flow control\n   [-]parmrk     mark parity errors (with a 255-0-character sequence)\n   [-]tandem     same as [-]ixoff\n",
     uVar3);
  func_0x00401940 ("\nOutput settings:\n", uVar3);
  func_0x00401940 (" * bsN           backspace delay style, N in [0..1]\n",
		   uVar3);
  func_0x00401940
    (" * crN           carriage return delay style, N in [0..3]\n", uVar3);
  func_0x00401940 (" * ffN           form feed delay style, N in [0..1]\n",
		   uVar3);
  func_0x00401940 (" * nlN           newline delay style, N in [0..1]\n",
		   uVar3);
  func_0x00401940 (" * [-]ocrnl      translate carriage return to newline\n",
		   uVar3);
  func_0x00401940
    (" * [-]ofdel      use delete characters for fill instead of NUL characters\n",
     uVar3);
  func_0x00401940
    (" * [-]ofill      use fill (padding) characters instead of timing for delays\n",
     uVar3);
  func_0x00401940
    (" * [-]olcuc      translate lowercase characters to uppercase\n", uVar3);
  func_0x00401940
    (" * [-]onlcr      translate newline to carriage return-newline\n",
     uVar3);
  func_0x00401940 (" * [-]onlret     newline performs a carriage return\n",
		   uVar3);
  func_0x00401940
    (" * [-]onocr      do not print carriage returns in the first column\n",
     uVar3);
  func_0x00401940 ("   [-]opost      postprocess output\n", uVar3);
  func_0x00401940
    (" * tabN          horizontal tab delay style, N in [0..3]\n * tabs          same as tab0\n * -tabs         same as tab3\n",
     uVar3);
  func_0x00401940 (" * vtN           vertical tab delay style, N in [0..1]\n",
		   uVar3);
  func_0x00401940
    ("\nLocal settings:\n   [-]crterase   echo erase characters as backspace-space-backspace\n",
     uVar3);
  func_0x00401940
    (" * crtkill       kill all line by obeying the echoprt and echoe settings\n * -crtkill      kill all line by obeying the echoctl and echok settings\n",
     uVar3);
  func_0x00401940
    (" * [-]ctlecho    echo control characters in hat notation (\'^c\')\n",
     uVar3);
  func_0x00401940 ("   [-]echo       echo input characters\n", uVar3);
  func_0x00401940 (" * [-]echoctl    same as [-]ctlecho\n", uVar3);
  func_0x00401940
    ("   [-]echoe      same as [-]crterase\n   [-]echok      echo a newline after a kill character\n",
     uVar3);
  func_0x00401940 (" * [-]echoke     same as [-]crtkill\n", uVar3);
  func_0x00401940
    ("   [-]echonl     echo newline even if not echoing other characters\n",
     uVar3);
  func_0x00401940
    (" * [-]echoprt    echo erased characters backward, between \'\\\' and \'/\'\n",
     uVar3);
  func_0x00401940
    (" * [-]extproc    enable \"LINEMODE\"; useful with high latency links\n",
     uVar3);
  func_0x00401940 (" * [-]flusho     discard output\n", uVar3);
  func_0x00401750
    ("   [-]icanon     enable special characters: %s\n   [-]iexten     enable non-POSIX special characters\n",
     "erase, kill, werase, rprnt");
  func_0x00401940
    ("   [-]isig       enable interrupt, quit, and suspend special characters\n   [-]noflsh     disable flushing after interrupt and quit special characters\n",
     uVar3);
  func_0x00401940 (" * [-]prterase   same as [-]echoprt\n", uVar3);
  func_0x00401940
    (" * [-]tostop     stop background jobs that try to write to the terminal\n",
     uVar3);
  func_0x00401940 ("\nCombination settings:\n", uVar3);
  func_0x00401940
    ("   cbreak        same as -icanon\n   -cbreak       same as icanon\n",
     uVar3);
  func_0x00401940
    ("   cooked        same as brkint ignpar istrip icrnl ixon opost isig\n                 icanon, eof and eol characters to their default values\n   -cooked       same as raw\n",
     uVar3);
  func_0x00401750 ("   crt           same as %s\n", "echoe echoctl echoke");
  func_0x00401750
    ("   dec           same as %s intr ^c erase 0177\n                 kill ^u\n",
     "echoe echoctl echoke -ixany");
  func_0x00401940 (" * [-]decctlq    same as [-]ixany\n", uVar3);
  func_0x00401940
    ("   ek            erase and kill characters to their default values\n   evenp         same as parenb -parodd cs7\n   -evenp        same as -parenb cs8\n",
     uVar3);
  func_0x00401940
    ("   litout        same as -parenb -istrip -opost cs8\n   -litout       same as parenb istrip opost cs7\n",
     uVar3);
  func_0x00401750
    ("   nl            same as %s\n   -nl           same as %s\n",
     "-icrnl -onlcr", "icrnl -inlcr -igncr onlcr -ocrnl -onlret");
  func_0x00401940
    ("   oddp          same as parenb parodd cs7\n   -oddp         same as -parenb cs8\n   [-]parity     same as [-]evenp\n   pass8         same as -parenb -istrip cs8\n   -pass8        same as parenb istrip cs7\n",
     uVar3);
  func_0x00401750
    ("   raw           same as -ignbrk -brkint -ignpar -parmrk -inpck -istrip\n                 -inlcr -igncr -icrnl -ixon -ixoff -icanon -opost\n                 -isig%s min 1 time 0\n   -raw          same as cooked\n",
     " -iuclc -ixany -imaxbel");
  func_0x00401750
    ("   sane          same as cread -ignbrk brkint -inlcr -igncr icrnl\n                 icanon iexten echo echoe echok -echonl -noflsh\n                 %s\n                 %s\n                 %s,\n                 all special characters to their default values\n",
     "-ixoff -iutf8 -iuclc -ixany imaxbel -olcuc -ocrnl",
     "opost -ofill onlcr -onocr -onlret nl0 cr0 tab0 bs0 vt0 ff0",
     "isig -tostop -ofdel -echoprt echoctl echoke -extproc -flusho");
  func_0x00401940
    ("\nHandle the tty line connected to standard input.  Without arguments,\nprints baud rate, line discipline, and deviations from stty sane.  In\nsettings, CHAR is taken literally, or coded as in ^c, 0x37, 0177 or\n127; special values ^- or undef used to disable special characters.\n",
     uVar3);
  local_88 = &DAT_0040f776;
  local_80 = "test invocation";
  local_78 = 0x40f7d9;
  local_70 = "Multi-call invocation";
  local_68 = "sha224sum";
  local_60 = "sha2 utilities";
  local_58 = "sha256sum";
  local_50 = "sha2 utilities";
  local_48 = "sha384sum";
  local_40 = "sha2 utilities";
  local_38 = "sha512sum";
  local_30 = "sha2 utilities";
  local_28 = 0;
  local_20 = 0;
  puVar5 = &DAT_0040f776;
  do
    {
      iVar1 = func_0x00401a40 (&DAT_0040f6a1, puVar5);
      if (iVar1 == 0)
	break;
      ppuVar4 = ppuVar4 + 2;
      puVar5 = *ppuVar4;
    }
  while (puVar5 != (undefined *) 0x0);
  puVar5 = ppuVar4[1];
  if (puVar5 == (undefined *) 0x0)
    {
      func_0x00401750 ("\n%s online help: <%s>\n", "GNU coreutils",
		       "https://www.gnu.org/software/coreutils/");
      lVar2 = func_0x00401aa0 (5, 0);
      if (lVar2 == 0)
	goto LAB_00402914;
      iVar1 = func_0x004019a0 (lVar2, &DAT_0040f7fa, 3);
      if (iVar1 == 0)
	{
	  func_0x00401750 ("Full documentation at: <%s%s>\n",
			   "https://www.gnu.org/software/coreutils/",
			   &DAT_0040f6a1);
	  puVar5 = &DAT_0040f6a1;
	  uVar3 = 0x40f792;
	  goto LAB_004028fb;
	}
      puVar5 = &DAT_0040f6a1;
    LAB_004028b9:
      ;
      func_0x00401750
	("Report %s translation bugs to <https://translationproject.org/team/>\n",
	 &DAT_0040f6a1);
    }
  else
    {
      func_0x00401750 ("\n%s online help: <%s>\n", "GNU coreutils",
		       "https://www.gnu.org/software/coreutils/");
      lVar2 = func_0x00401aa0 (5, 0);
      if ((lVar2 != 0)
	  && (iVar1 = func_0x004019a0 (lVar2, &DAT_0040f7fa, 3), iVar1 != 0))
	goto LAB_004028b9;
    }
  func_0x00401750 ("Full documentation at: <%s%s>\n",
		   "https://www.gnu.org/software/coreutils/", &DAT_0040f6a1);
  uVar3 = 0x4141bf;
  if (puVar5 == &DAT_0040f6a1)
    {
      uVar3 = 0x40f792;
    }
LAB_004028fb:
  ;
  do
    {
      func_0x00401750
	("or available locally via: info \'(coreutils) %s%s\'\n", puVar5,
	 uVar3);
    LAB_0040290d:
      ;
      func_0x00401af0 ((ulong) uParm1);
    LAB_00402914:
      ;
      func_0x00401750 ("Full documentation at: <%s%s>\n",
		       "https://www.gnu.org/software/coreutils/",
		       &DAT_0040f6a1);
      puVar5 = &DAT_0040f6a1;
      uVar3 = 0x40f792;
    }
  while (true);
}

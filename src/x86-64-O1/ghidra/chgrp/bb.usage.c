
#include "chgrp.h"

long null_ARRAY_0061750_0_8_;
long null_ARRAY_0061750_8_8_;
long null_ARRAY_0061770_0_8_;
long null_ARRAY_0061770_16_8_;
long null_ARRAY_0061770_24_8_;
long null_ARRAY_0061770_32_8_;
long null_ARRAY_0061770_40_8_;
long null_ARRAY_0061770_48_8_;
long null_ARRAY_0061770_8_8_;
long null_ARRAY_0061774_0_4_;
long null_ARRAY_0061774_16_8_;
long null_ARRAY_0061774_4_4_;
long null_ARRAY_0061774_8_4_;
long local_68_2_2_;
long local_6b_1_3_;
long local_6c_4_4_;
long local_6f_4_4_;
long local_70_0_2_;
long local_70_4_4_;
long local_71_0_1_;
long local_71_4_4_;
long local_72_4_4_;
long DAT_00000010;
long DAT_0000001c;
long DAT_004125c6;
long DAT_0041264a;
long DAT_00413560;
long DAT_00413564;
long DAT_00413568;
long DAT_0041356b;
long DAT_0041356d;
long DAT_00413571;
long DAT_00413575;
long DAT_00413d2b;
long DAT_0041446e;
long DAT_0041447d;
long DAT_0041447f;
long DAT_00414480;
long DAT_00414661;
long DAT_00414662;
long DAT_00414680;
long DAT_004146f1;
long DAT_0041483a;
long DAT_00414844;
long DAT_00414882;
long DAT_00617000;
long DAT_00617010;
long DAT_00617020;
long DAT_006174a8;
long DAT_00617510;
long DAT_00617514;
long DAT_00617518;
long DAT_0061751c;
long DAT_00617540;
long DAT_00617550;
long DAT_00617560;
long DAT_00617568;
long DAT_00617580;
long DAT_00617588;
long DAT_006175e0;
long DAT_006175e8;
long DAT_006175f0;
long DAT_006175f8;
long DAT_00617778;
long DAT_0061777c;
long DAT_00617780;
long DAT_00617788;
long DAT_00617790;
long DAT_00617798;
long DAT_006177a8;
long fde_00415678;
long null_ARRAY_00412f40;
long null_ARRAY_00414700;
long null_ARRAY_00414ca0;
long null_ARRAY_00414ed0;
long null_ARRAY_00617500;
long null_ARRAY_006175a0;
long null_ARRAY_006175d0;
long null_ARRAY_00617600;
long null_ARRAY_00617700;
long null_ARRAY_00617740;
long PTR_DAT_006174a0;
long PTR_null_ARRAY_006174f8;
long stack0x00000008;
void
FUN_0040231e (uint uParm1)
{
  int iVar1;
  long lVar2;
  undefined8 uVar3;
  undefined **ppuVar4;
  char *pcVar5;
  undefined *puVar6;
  undefined *local_88;
  char *local_80;
  undefined8 local_78;
  char *local_70;
  char *local_68;
  char *local_60;
  char *local_58;
  char *local_50;
  char *local_48;
  char *local_40;
  char *local_38;
  char *local_30;
  undefined8 local_28;
  undefined8 local_20;

  ppuVar4 = &local_88;
  if (uParm1 != 0)
    {
      func_0x00401e40 (DAT_00617560,
		       "Try \'%s --help\' for more information.\n",
		       DAT_006175f8);
      goto LAB_00402553;
    }
  func_0x00401bd0
    ("Usage: %s [OPTION]... GROUP FILE...\n  or:  %s [OPTION]... --reference=RFILE FILE...\n",
     DAT_006175f8, DAT_006175f8);
  uVar3 = DAT_00617540;
  func_0x00401e50
    ("Change the group of each FILE to GROUP.\nWith --reference, change the group of each FILE to that of RFILE.\n\n",
     DAT_00617540);
  func_0x00401e50
    ("  -c, --changes          like verbose but report only when a change is made\n  -f, --silent, --quiet  suppress most error messages\n  -v, --verbose          output a diagnostic for every file processed\n",
     uVar3);
  func_0x00401e50
    ("      --dereference      affect the referent of each symbolic link (this is\n                         the default), rather than the symbolic link itself\n  -h, --no-dereference   affect symbolic links instead of any referenced file\n",
     uVar3);
  func_0x00401e50
    ("                         (useful only on systems that can change the\n                         ownership of a symlink)\n",
     uVar3);
  func_0x00401e50
    ("      --no-preserve-root  do not treat \'/\' specially (the default)\n      --preserve-root    fail to operate recursively on \'/\'\n",
     uVar3);
  func_0x00401e50
    ("      --reference=RFILE  use RFILE\'s group rather than specifying a\n                         GROUP value\n",
     uVar3);
  func_0x00401e50
    ("  -R, --recursive        operate on files and directories recursively\n",
     uVar3);
  func_0x00401e50
    ("\nThe following options modify how a hierarchy is traversed when the -R\noption is also specified.  If more than one is specified, only the final\none takes effect.\n\n  -H                     if a command line argument is a symbolic link\n                         to a directory, traverse it\n  -L                     traverse every symbolic link to a directory\n                         encountered\n  -P                     do not traverse any symbolic links (default)\n\n",
     uVar3);
  func_0x00401e50 ("      --help     display this help and exit\n", uVar3);
  func_0x00401e50 ("      --version  output version information and exit\n",
		   uVar3);
  func_0x00401bd0
    ("\nExamples:\n  %s staff /u      Change the group of /u to \"staff\".\n  %s -hR staff /u  Change the group of /u and subfiles to \"staff\".\n",
     DAT_006175f8, DAT_006175f8);
  local_88 = &DAT_004125c6;
  local_80 = "test invocation";
  local_78 = 0x412629;
  local_70 = "Multi-call invocation";
  local_68 = "sha224sum";
  local_60 = "sha2 utilities";
  local_58 = "sha256sum";
  local_50 = "sha2 utilities";
  local_48 = "sha384sum";
  local_40 = "sha2 utilities";
  local_38 = "sha512sum";
  local_30 = "sha2 utilities";
  local_28 = 0;
  local_20 = 0;
  puVar6 = &DAT_004125c6;
  do
    {
      iVar1 = func_0x00401f80 ("chgrp", puVar6);
      if (iVar1 == 0)
	break;
      ppuVar4 = ppuVar4 + 2;
      puVar6 = *ppuVar4;
    }
  while (puVar6 != (undefined *) 0x0);
  pcVar5 = ppuVar4[1];
  if (pcVar5 == (char *) 0x0)
    {
      func_0x00401bd0 ("\n%s online help: <%s>\n", "GNU coreutils",
		       "https://www.gnu.org/software/coreutils/");
      lVar2 = func_0x00401ff0 (5, 0);
      if (lVar2 == 0)
	goto LAB_0040255a;
      iVar1 = func_0x00401ee0 (lVar2, &DAT_0041264a, 3);
      if (iVar1 == 0)
	{
	  func_0x00401bd0 ("Full documentation at: <%s%s>\n",
			   "https://www.gnu.org/software/coreutils/",
			   "chgrp");
	  pcVar5 = "chgrp";
	  uVar3 = 0x4125e2;
	  goto LAB_00402541;
	}
      pcVar5 = "chgrp";
    LAB_004024ff:
      ;
      func_0x00401bd0
	("Report %s translation bugs to <https://translationproject.org/team/>\n",
	 "chgrp");
    }
  else
    {
      func_0x00401bd0 ("\n%s online help: <%s>\n", "GNU coreutils",
		       "https://www.gnu.org/software/coreutils/");
      lVar2 = func_0x00401ff0 (5, 0);
      if ((lVar2 != 0)
	  && (iVar1 = func_0x00401ee0 (lVar2, &DAT_0041264a, 3), iVar1 != 0))
	goto LAB_004024ff;
    }
  func_0x00401bd0 ("Full documentation at: <%s%s>\n",
		   "https://www.gnu.org/software/coreutils/", "chgrp");
  uVar3 = 0x41467f;
  if (pcVar5 == "chgrp")
    {
      uVar3 = 0x4125e2;
    }
LAB_00402541:
  ;
  do
    {
      func_0x00401bd0
	("or available locally via: info \'(coreutils) %s%s\'\n", pcVar5,
	 uVar3);
    LAB_00402553:
      ;
      func_0x00402050 ((ulong) uParm1);
    LAB_0040255a:
      ;
      func_0x00401bd0 ("Full documentation at: <%s%s>\n",
		       "https://www.gnu.org/software/coreutils/", "chgrp");
      pcVar5 = "chgrp";
      uVar3 = 0x4125e2;
    }
  while (true);
}

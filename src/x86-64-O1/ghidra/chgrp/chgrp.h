typedef unsigned char   undefined;

typedef unsigned char    byte;
typedef unsigned char    dwfenc;
typedef unsigned int    dword;
typedef unsigned long    qword;
typedef unsigned char    undefined1;
typedef unsigned short    undefined2;
typedef unsigned int    undefined4;
typedef unsigned long    undefined6;
typedef unsigned long    undefined8;
typedef unsigned short    word;
#define unkbyte9   unsigned long long
#define unkbyte10   unsigned long long
#define unkbyte11   unsigned long long
#define unkbyte12   unsigned long long
#define unkbyte13   unsigned long long
#define unkbyte14   unsigned long long
#define unkbyte15   unsigned long long
#define unkbyte16   unsigned long long

#define unkuint9   unsigned long long
#define unkuint10   unsigned long long
#define unkuint11   unsigned long long
#define unkuint12   unsigned long long
#define unkuint13   unsigned long long
#define unkuint14   unsigned long long
#define unkuint15   unsigned long long
#define unkuint16   unsigned long long

#define unkint9   long long
#define unkint10   long long
#define unkint11   long long
#define unkint12   long long
#define unkint13   long long
#define unkint14   long long
#define unkint15   long long
#define unkint16   long long

#define unkfloat1   float
#define unkfloat2   float
#define unkfloat3   float
#define unkfloat5   double
#define unkfloat6   double
#define unkfloat7   double
#define unkfloat9   long double
#define unkfloat11   long double
#define unkfloat12   long double
#define unkfloat13   long double
#define unkfloat14   long double
#define unkfloat15   long double
#define unkfloat16   long double

#define BADSPACEBASE   void
#define code   void

#define ulong long
#define true 1
#define bool int
#define false 0
#define float10 int
#define ushort int
#define undefined7 long
#define undefined2 long
#define uint5 long
#define uint6 long

// Add definition for uint
typedef unsigned int    uint;

#define sbyte char

extern imperfection_wrapper(void);
typedef struct eh_frame_hdr eh_frame_hdr, *Peh_frame_hdr;

struct eh_frame_hdr {
    byte eh_frame_hdr_version; // Exception Handler Frame Header Version
    dwfenc eh_frame_pointer_encoding; // Exception Handler Frame Pointer Encoding
    dwfenc eh_frame_desc_entry_count_encoding; // Encoding of # of Exception Handler FDEs
    dwfenc eh_frame_table_encoding; // Exception Handler Table Encoding
};

typedef struct fde_table_entry fde_table_entry, *Pfde_table_entry;

struct fde_table_entry {
    dword initial_loc; // Initial Location
    dword data_loc; // Data location
};

typedef enum Elf64_DynTag {
    DT_AUDIT=1879047932,
    DT_AUXILIARY=2147483645,
    DT_BIND_NOW=24,
    DT_CHECKSUM=1879047672,
    DT_CONFIG=1879047930,
    DT_DEBUG=21,
    DT_DEPAUDIT=1879047931,
    DT_ENCODING=32,
    DT_FEATURE_1=1879047676,
    DT_FILTER=2147483647,
    DT_FINI=13,
    DT_FINI_ARRAY=26,
    DT_FINI_ARRAYSZ=28,
    DT_FLAGS=30,
    DT_FLAGS_1=1879048187,
    DT_GNU_CONFLICT=1879047928,
    DT_GNU_CONFLICTSZ=1879047670,
    DT_GNU_HASH=1879047925,
    DT_GNU_LIBLIST=1879047929,
    DT_GNU_LIBLISTSZ=1879047671,
    DT_GNU_PRELINKED=1879047669,
    DT_HASH=4,
    DT_INIT=12,
    DT_INIT_ARRAY=25,
    DT_INIT_ARRAYSZ=27,
    DT_JMPREL=23,
    DT_MOVEENT=1879047674,
    DT_MOVESZ=1879047675,
    DT_MOVETAB=1879047934,
    DT_NEEDED=1,
    DT_NULL=0,
    DT_PLTGOT=3,
    DT_PLTPAD=1879047933,
    DT_PLTPADSZ=1879047673,
    DT_PLTREL=20,
    DT_PLTRELSZ=2,
    DT_POSFLAG_1=1879047677,
    DT_PREINIT_ARRAYSZ=33,
    DT_REL=17,
    DT_RELA=7,
    DT_RELACOUNT=1879048185,
    DT_RELAENT=9,
    DT_RELASZ=8,
    DT_RELCOUNT=1879048186,
    DT_RELENT=19,
    DT_RELSZ=18,
    DT_RPATH=15,
    DT_RUNPATH=29,
    DT_SONAME=14,
    DT_STRSZ=10,
    DT_STRTAB=5,
    DT_SYMBOLIC=16,
    DT_SYMENT=11,
    DT_SYMINENT=1879047679,
    DT_SYMINFO=1879047935,
    DT_SYMINSZ=1879047678,
    DT_SYMTAB=6,
    DT_TEXTREL=22,
    DT_TLSDESC_GOT=1879047927,
    DT_TLSDESC_PLT=1879047926,
    DT_VERDEF=1879048188,
    DT_VERDEFNUM=1879048189,
    DT_VERNEED=1879048190,
    DT_VERNEEDNUM=1879048191,
    DT_VERSYM=1879048176
} Elf64_DynTag;

typedef enum Elf_ProgramHeaderType {
    PT_DYNAMIC=2,
    PT_GNU_EH_FRAME=1685382480,
    PT_GNU_RELRO=1685382482,
    PT_GNU_STACK=1685382481,
    PT_INTERP=3,
    PT_LOAD=1,
    PT_NOTE=4,
    PT_NULL=0,
    PT_PHDR=6,
    PT_SHLIB=5,
    PT_TLS=7
} Elf_ProgramHeaderType;

typedef struct Elf64_Dyn Elf64_Dyn, *PElf64_Dyn;

struct Elf64_Dyn {
    enum Elf64_DynTag d_tag;
    qword d_val;
};

typedef struct Elf64_Shdr Elf64_Shdr, *PElf64_Shdr;

typedef enum Elf_SectionHeaderType {
    SHT_CHECKSUM=1879048184,
    SHT_DYNAMIC=6,
    SHT_DYNSYM=11,
    SHT_FINI_ARRAY=15,
    SHT_GNU_ATTRIBUTES=1879048181,
    SHT_GNU_HASH=1879048182,
    SHT_GNU_LIBLIST=1879048183,
    SHT_GNU_verdef=1879048189,
    SHT_GNU_verneed=1879048190,
    SHT_GNU_versym=1879048191,
    SHT_GROUP=17,
    SHT_HASH=5,
    SHT_INIT_ARRAY=14,
    SHT_NOBITS=8,
    SHT_NOTE=7,
    SHT_NULL=0,
    SHT_PREINIT_ARRAY=16,
    SHT_PROGBITS=1,
    SHT_REL=9,
    SHT_RELA=4,
    SHT_SHLIB=10,
    SHT_STRTAB=3,
    SHT_SUNW_COMDAT=1879048187,
    SHT_SUNW_move=1879048186,
    SHT_SUNW_syminfo=1879048188,
    SHT_SYMTAB=2,
    SHT_SYMTAB_SHNDX=18
} Elf_SectionHeaderType;

struct Elf64_Shdr {
    dword sh_name;
    enum Elf_SectionHeaderType sh_type;
    qword sh_flags;
    qword sh_addr;
    qword sh_offset;
    qword sh_size;
    dword sh_link;
    dword sh_info;
    qword sh_addralign;
    qword sh_entsize;
};

typedef struct Elf64_Phdr Elf64_Phdr, *PElf64_Phdr;

struct Elf64_Phdr {
    enum Elf_ProgramHeaderType p_type;
    dword p_flags;
    qword p_offset;
    qword p_vaddr;
    qword p_paddr;
    qword p_filesz;
    qword p_memsz;
    qword p_align;
};

typedef struct Elf64_Rela Elf64_Rela, *PElf64_Rela;

struct Elf64_Rela {
    qword r_offset; // location to apply the relocation action
    qword r_info; // the symbol table index and the type of relocation
    qword r_addend; // a constant addend used to compute the relocatable field value
};

typedef struct Elf64_Sym Elf64_Sym, *PElf64_Sym;

struct Elf64_Sym {
    dword st_name;
    byte st_info;
    byte st_other;
    word st_shndx;
    qword st_value;
    qword st_size;
};

typedef struct Elf64_Ehdr Elf64_Ehdr, *PElf64_Ehdr;

struct Elf64_Ehdr {
    byte e_ident_magic_num;
    char e_ident_magic_str[3];
    byte e_ident_class;
    byte e_ident_data;
    byte e_ident_version;
    byte e_ident_pad[9];
    word e_type;
    word e_machine;
    dword e_version;
    qword e_entry;
    qword e_phoff;
    qword e_shoff;
    dword e_flags;
    word e_ehsize;
    word e_phentsize;
    word e_phnum;
    word e_shentsize;
    word e_shnum;
    word e_shstrndx;
};




undefined8 _DT_INIT(void);
void FUN_00401b90(void);
void thunk_FUN_00618128(void);
void thunk_FUN_006181c0(void);
void FUN_00402180(void);
void thunk_FUN_006182f0(void);
void entry(void);
void FUN_004021e0(void);
void FUN_00402260(void);
void FUN_004022e0(void);
void FUN_0040231e(uint uParm1);
ulong FUN_004025f1(uint uParm1,undefined8 *puParm2);
undefined8 FUN_00402a3d(long lParm1,long lParm2);
void FUN_00402abd(undefined4 *puParm1);
void FUN_00402ae8(ulong uParm1);
void FUN_00402b16(ulong uParm1);
ulong FUN_00402b44(undefined8 param_1,uint param_2,uint param_3,uint param_4,int param_5,int param_6,int *param_7);
void FUN_004036b4(void);
char * FUN_0040375a(ulong uParm1,long lParm2);
void FUN_00403796(long lParm1);
int * FUN_0040382b(int *piParm1,int iParm2);
undefined * FUN_0040385b(char *pcParm1,int iParm2);
ulong FUN_00403913(undefined *param_1,ulong param_2,long param_3,ulong param_4,uint param_5,ulong param_6,long param_7,char *param_8,char *param_9);
undefined1 * FUN_004046ca(int iParm1,undefined8 uParm2,undefined8 uParm3,uint *puParm4);
ulong FUN_0040486c(undefined1 *puParm1,byte bParm2,undefined8 uParm3);
void FUN_004048a0(uint uParm1,undefined8 uParm2,undefined8 uParm3);
void FUN_004048ce(ulong uParm1,undefined8 uParm2);
void FUN_004048e6(undefined8 uParm1,undefined8 uParm2,char cParm3);
void FUN_0040496f(undefined8 uParm1,char cParm2);
void FUN_00404988(undefined8 uParm1);
void FUN_0040499b(uint uParm1,undefined8 uParm2,undefined8 uParm3);
void FUN_00404a2a(void);
void FUN_00404a3d(undefined8 uParm1,undefined8 uParm2);
void FUN_00404a52(undefined8 uParm1);
undefined8 * FUN_00404a68(undefined8 *puParm1);
void FUN_00404aa7(undefined8 uParm1,long lParm2,undefined8 uParm3,undefined8 uParm4,undefined8 *puParm5,undefined8 uParm6);
void FUN_00404ce4(void);
void FUN_00404d36(void);
void FUN_00404dbb(long lParm1);
long FUN_00404dd5(long lParm1,long lParm2);
void FUN_00404e08(undefined8 uParm1,undefined8 uParm2);
void FUN_00404e31(undefined8 uParm1);
void FUN_00404e48(void);
void FUN_00404e70(undefined8 uParm1,uint uParm2);
ulong FUN_00404eb1(long lParm1,long lParm2);
ulong FUN_00404edc(ulong *puParm1,int iParm2,int iParm3);
ulong FUN_00404f3a(byte *pbParm1,byte **ppbParm2,uint uParm3,ulong *puParm4,long lParm5);
undefined8 FUN_00405283(ulong uParm1,ulong uParm2,ulong uParm3);
void FUN_004052cb(void);
void FUN_004052f8(undefined8 uParm1);
void FUN_00405338(uint uParm1,uint uParm2,undefined8 uParm3,undefined8 uParm4);
void FUN_0040538f(undefined4 uParm1,undefined4 uParm2,undefined4 uParm3,undefined4 uParm4,undefined4 uParm5,undefined4 uParm6,undefined4 uParm7,undefined4 uParm8,ulong param_1,ulong param_2,undefined8 param_3,undefined8 param_4,undefined8 param_5,undefined8 param_6);
ulong FUN_00405462(uint uParm1,char *pcParm2,ulong uParm3,ulong uParm4,uint uParm5);
undefined8 FUN_00405695(ulong uParm1,long lParm2,long lParm3,ulong uParm4);
ulong FUN_00405761(long *plParm1,long *plParm2);
ulong FUN_00405780(long lParm1,ulong uParm2);
ulong FUN_00405790(ulong *puParm1,ulong uParm2);
ulong FUN_0040579f(ulong *puParm1,ulong *puParm2);
ulong FUN_004057a9(long *plParm1,long *plParm2);
undefined8 FUN_004057cf(long lParm1,long lParm2);
ulong FUN_0040583c(long lParm1,long lParm2,char cParm3);
long FUN_00405994(long lParm1,long lParm2,ulong uParm3);
long FUN_00405a74(long lParm1,undefined8 uParm2,long lParm3);
ulong FUN_00405b06(long lParm1);
void FUN_00405b65(long lParm1,undefined8 uParm2);
void FUN_00405bc4(long lParm1);
void FUN_00405bfb(long lParm1);
void FUN_00405c26(undefined8 uParm1);
undefined8 FUN_00405c4d(long lParm1);
undefined8 FUN_00405d58(void);
undefined8 FUN_00405db2(long lParm1,undefined8 *puParm2);
void FUN_00405e5c(long lParm1,uint uParm2,char cParm3);
ulong FUN_00405ead(long lParm1);
ulong FUN_00405efd(long lParm1,long lParm2,ulong uParm3,long lParm4);
void FUN_0040607e(long lParm1,long lParm2);
long FUN_00406110(long *plParm1,int iParm2);
long * FUN_004069d9(long *plParm1,uint uParm2,long lParm3);
undefined8 FUN_00406d58(long *plParm1);
long FUN_00406e92(long *plParm1);
undefined8 FUN_004074fe(undefined8 uParm1,long lParm2,uint uParm3);
void FUN_00407528(long lParm1,int *piParm2);
ulong FUN_004075e7(int param_1,undefined8 *param_2,char *param_3,long *param_4,int *param_5,int param_6,int *param_7,int param_8,undefined8 param_9);
ulong FUN_00407aa2(uint param_1,undefined8 *param_2,char *param_3,long param_4,undefined8 param_5,ulong param_6,uint *param_7,int param_8);
void FUN_00407f1e(void);
void FUN_00407f74(void);
ulong FUN_00407f8a(long lParm1,uint uParm2,uint uParm3);
ulong FUN_0040806c(long lParm1,long lParm2);
void FUN_004080d5(long lParm1);
ulong FUN_004080ef(uint *puParm1,byte *pbParm2,long lParm3);
ulong FUN_00408154(uint param_1,undefined8 param_2,uint param_3,uint param_4);
undefined * FUN_00408244(undefined *puParm1,uint uParm2,char *pcParm3);
void FUN_00408343(long lParm1,long lParm2);
ulong FUN_00408371(long lParm1,long lParm2);
ulong FUN_004083bd(undefined8 uParm1,undefined8 uParm2,undefined8 uParm3);
ulong FUN_0040848a(ulong uParm1,char cParm2);
ulong FUN_004084ea(undefined8 uParm1);
void FUN_0040854b(long lParm1);
undefined8 FUN_0040855b(long *plParm1,long *plParm2);
void FUN_004085ef(undefined8 param_1,byte param_2,ulong param_3);
undefined8 FUN_00408639(void);
ulong FUN_00408641(ulong uParm1);
ulong FUN_004086cf(ulong uParm1,ulong uParm2);
undefined FUN_004086e2(long lParm1,long lParm2);;
ulong FUN_004086e9(long lParm1,undefined8 uParm2,uint uParm3);
ulong FUN_0040876d(ulong uParm1,long lParm2);
long FUN_00408889(long *plParm1,undefined8 uParm2);
undefined8 FUN_004088ab(long lParm1,long **pplParm2,char cParm3);
long FUN_004089cf(long lParm1,long lParm2,long **pplParm3,char cParm4);
long FUN_00408ad3(long lParm1,long lParm2);
long * FUN_00408b2f(undefined8 uParm1,undefined1 *puParm2,code *pcParm3,code *pcParm4,long lParm5);
void FUN_00408c1a(long **pplParm1);
ulong FUN_00408cbf(long *plParm1,undefined8 uParm2);
undefined8 FUN_00408dfc(long lParm1,long lParm2,long *plParm3);
undefined8 FUN_00409029(undefined8 uParm1,undefined8 uParm2);
long FUN_00409058(long lParm1,undefined8 uParm2);
void FUN_00409234(undefined4 *puParm1,undefined4 uParm2);
ulong FUN_00409255(long lParm1);
ulong FUN_0040925a(long lParm1,uint uParm2);
ulong FUN_0040928f(long lParm1);
char * FUN_004092c2(void);
void FUN_004095fd(ulong uParm1);
void FUN_0040961d(ulong uParm1);
void FUN_0040963d(void);
ulong FUN_00409686(int *piParm1);
void FUN_004096d3(uint *puParm1);
void FUN_004096f4(int *piParm1);
ulong FUN_0040970e(uint uParm1);
void FUN_00409740(int *piParm1);
undefined8 FUN_00409772(uint *puParm1,undefined8 uParm2);
ulong FUN_004097af(char *pcParm1);
ulong FUN_00409a0c(long lParm1,uint uParm2,uint uParm3);
undefined8 FUN_00409b63(undefined8 uParm1);
ulong FUN_00409be6(uint param_1,undefined8 param_2,undefined8 param_3,undefined8 param_4);
undefined8 FUN_00409d55(ulong uParm1);
ulong FUN_00409da4(long lParm1);
undefined8 FUN_00409e34(void);
void FUN_00409e47(void);
long FUN_00409e55(long lParm1,ulong uParm2);
ulong * FUN_0040a2e5(ulong *puParm1,char cParm2,ulong uParm3);
ulong FUN_0040a3dd(char *pcParm1,long lParm2);
long FUN_0040a424(ulong uParm1,uint *puParm2,ulong uParm3,uint *puParm4,long *plParm5);
ulong FUN_0040a548(void);
ulong FUN_0040a57a(void);
int * FUN_0040a787(ulong uParm1,long lParm2,ulong uParm3,uint *puParm4,ulong *puParm5);
char * FUN_0040ad6a(int iParm1,long lParm2,long lParm3,long lParm4,uint uParm5);
void FUN_0040b382(uint param_1);
ulong FUN_0040b548(void);
void FUN_0040b75b(undefined8 uParm1,uint uParm2);
long * FUN_0040b8fc(long *plParm1,long **pplParm2,long lParm3,undefined8 uParm4);
double FUN_00411499(int *piParm1);
void FUN_004114e1(int *param_1);
void FUN_00411562(undefined8 uParm1);
undefined8 FUN_0041157f(uint *puParm1,ulong *puParm2);
undefined8 FUN_004119a4(byte *pbParm1,ulong *puParm2,byte **ppbParm3);
ulong FUN_00412509(void);
ulong FUN_00412531(void);
void FUN_00412570(void);
undefined8 _DT_FINI(void);
undefined FUN_00618000();
undefined FUN_00618008();
undefined FUN_00618010();
undefined FUN_00618018();
undefined FUN_00618020();
undefined FUN_00618028();
undefined FUN_00618030();
undefined FUN_00618038();
undefined FUN_00618040();
undefined FUN_00618048();
undefined FUN_00618050();
undefined FUN_00618058();
undefined FUN_00618060();
undefined FUN_00618068();
undefined FUN_00618070();
undefined FUN_00618078();
undefined FUN_00618080();
undefined FUN_00618088();
undefined FUN_00618090();
undefined FUN_00618098();
undefined FUN_006180a0();
undefined FUN_006180a8();
undefined FUN_006180b0();
undefined FUN_006180b8();
undefined FUN_006180c0();
undefined FUN_006180c8();
undefined FUN_006180d0();
undefined FUN_006180d8();
undefined FUN_006180e0();
undefined FUN_006180e8();
undefined FUN_006180f0();
undefined FUN_006180f8();
undefined FUN_00618100();
undefined FUN_00618108();
undefined FUN_00618110();
undefined FUN_00618118();
undefined FUN_00618120();
undefined FUN_00618128();
undefined FUN_00618130();
undefined FUN_00618138();
undefined FUN_00618140();
undefined FUN_00618148();
undefined FUN_00618150();
undefined FUN_00618158();
undefined FUN_00618160();
undefined FUN_00618168();
undefined FUN_00618170();
undefined FUN_00618178();
undefined FUN_00618180();
undefined FUN_00618188();
undefined FUN_00618190();
undefined FUN_00618198();
undefined FUN_006181a0();
undefined FUN_006181a8();
undefined FUN_006181b0();
undefined FUN_006181b8();
undefined FUN_006181c0();
undefined FUN_006181c8();
undefined FUN_006181d0();
undefined FUN_006181d8();
undefined FUN_006181e0();
undefined FUN_006181e8();
undefined FUN_006181f0();
undefined FUN_006181f8();
undefined FUN_00618200();
undefined FUN_00618208();
undefined FUN_00618210();
undefined FUN_00618218();
undefined FUN_00618220();
undefined FUN_00618228();
undefined FUN_00618230();
undefined FUN_00618238();
undefined FUN_00618240();
undefined FUN_00618248();
undefined FUN_00618250();
undefined FUN_00618258();
undefined FUN_00618260();
undefined FUN_00618268();
undefined FUN_00618270();
undefined FUN_00618278();
undefined FUN_00618280();
undefined FUN_00618288();
undefined FUN_00618290();
undefined FUN_00618298();
undefined FUN_006182a0();
undefined FUN_006182a8();
undefined FUN_006182b0();
undefined FUN_006182b8();
undefined FUN_006182c0();
undefined FUN_006182c8();
undefined FUN_006182d0();
undefined FUN_006182d8();
undefined FUN_006182e0();
undefined FUN_006182e8();
undefined FUN_006182f0();


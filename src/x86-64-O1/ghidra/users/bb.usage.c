
#include "users.h"

long null_ARRAY_0060fb0_0_8_;
long null_ARRAY_0060fb0_8_8_;
long null_ARRAY_0060fd0_0_8_;
long null_ARRAY_0060fd0_16_8_;
long null_ARRAY_0060fd0_24_8_;
long null_ARRAY_0060fd0_32_8_;
long null_ARRAY_0060fd0_40_8_;
long null_ARRAY_0060fd0_48_8_;
long null_ARRAY_0060fd0_8_8_;
long null_ARRAY_0060fd4_0_4_;
long null_ARRAY_0060fd4_16_8_;
long null_ARRAY_0060fd4_4_4_;
long null_ARRAY_0060fd4_8_4_;
long local_68_2_2_;
long local_6b_1_3_;
long local_6c_4_4_;
long local_6e_4_4_;
long local_6f_4_4_;
long local_70_0_2_;
long local_70_4_4_;
long local_71_0_1_;
long local_71_4_4_;
long local_72_4_4_;
long DAT_00000010;
long DAT_0040ce82;
long DAT_0040cf06;
long DAT_0040d158;
long DAT_0040d220;
long DAT_0040d224;
long DAT_0040d228;
long DAT_0040d22b;
long DAT_0040d22d;
long DAT_0040d231;
long DAT_0040d235;
long DAT_0040d9eb;
long DAT_0040dd95;
long DAT_0040de99;
long DAT_0040de9f;
long DAT_0040deb1;
long DAT_0040deb2;
long DAT_0040ded0;
long DAT_0040ded4;
long DAT_0040df5e;
long DAT_0060f6b0;
long DAT_0060f6c0;
long DAT_0060f6d0;
long DAT_0060faa8;
long DAT_0060fb10;
long DAT_0060fb14;
long DAT_0060fb18;
long DAT_0060fb1c;
long DAT_0060fb40;
long DAT_0060fb50;
long DAT_0060fb60;
long DAT_0060fb68;
long DAT_0060fb80;
long DAT_0060fb88;
long DAT_0060fbd0;
long DAT_0060fbd8;
long DAT_0060fbe0;
long DAT_0060fd78;
long DAT_0060fd80;
long DAT_0060fd88;
long DAT_0060fd98;
long fde_0040ead0;
long null_ARRAY_0040d120;
long null_ARRAY_0040d180;
long null_ARRAY_0040e380;
long null_ARRAY_0040e5c0;
long null_ARRAY_0060fb00;
long null_ARRAY_0060fba0;
long null_ARRAY_0060fc00;
long null_ARRAY_0060fd00;
long null_ARRAY_0060fd40;
long PTR_DAT_0060faa0;
long PTR_null_ARRAY_0060faf8;
void
FUN_00401b82 (uint uParm1)
{
  int iVar1;
  long lVar2;
  undefined8 uVar3;
  undefined **ppuVar4;
  char *pcVar5;
  undefined *puVar6;
  undefined *local_88;
  char *local_80;
  undefined8 local_78;
  char *local_70;
  char *local_68;
  char *local_60;
  char *local_58;
  char *local_50;
  char *local_48;
  char *local_40;
  char *local_38;
  char *local_30;
  undefined8 local_28;
  undefined8 local_20;

  ppuVar4 = &local_88;
  if (uParm1 != 0)
    {
      func_0x00401710 (DAT_0060fb60,
		       "Try \'%s --help\' for more information.\n",
		       DAT_0060fbe0);
      goto LAB_00401d4c;
    }
  func_0x00401590 ("Usage: %s [OPTION]... [FILE]\n", DAT_0060fbe0);
  func_0x00401590
    ("Output who is currently logged in according to FILE.\nIf FILE is not specified, use %s.  %s as FILE is common.\n\n",
     "/dev/null/utmp", "/dev/null/wtmp");
  uVar3 = DAT_0060fb40;
  func_0x00401730 ("      --help     display this help and exit\n",
		   DAT_0060fb40);
  func_0x00401730 ("      --version  output version information and exit\n",
		   uVar3);
  local_88 = &DAT_0040ce82;
  local_80 = "test invocation";
  local_78 = 0x40cee5;
  local_70 = "Multi-call invocation";
  local_68 = "sha224sum";
  local_60 = "sha2 utilities";
  local_58 = "sha256sum";
  local_50 = "sha2 utilities";
  local_48 = "sha384sum";
  local_40 = "sha2 utilities";
  local_38 = "sha512sum";
  local_30 = "sha2 utilities";
  local_28 = 0;
  local_20 = 0;
  puVar6 = &DAT_0040ce82;
  do
    {
      iVar1 = func_0x00401830 ("users", puVar6);
      if (iVar1 == 0)
	break;
      ppuVar4 = ppuVar4 + 2;
      puVar6 = *ppuVar4;
    }
  while (puVar6 != (undefined *) 0x0);
  pcVar5 = ppuVar4[1];
  if (pcVar5 == (char *) 0x0)
    {
      func_0x00401590 ("\n%s online help: <%s>\n", "GNU coreutils",
		       "https://www.gnu.org/software/coreutils/");
      lVar2 = func_0x00401890 (5, 0);
      if (lVar2 == 0)
	goto LAB_00401d53;
      iVar1 = func_0x00401780 (lVar2, &DAT_0040cf06, 3);
      if (iVar1 == 0)
	{
	  func_0x00401590 ("Full documentation at: <%s%s>\n",
			   "https://www.gnu.org/software/coreutils/",
			   "users");
	  pcVar5 = "users";
	  uVar3 = 0x40ce9e;
	  goto LAB_00401d3a;
	}
      pcVar5 = "users";
    LAB_00401cf8:
      ;
      func_0x00401590
	("Report %s translation bugs to <https://translationproject.org/team/>\n",
	 "users");
    }
  else
    {
      func_0x00401590 ("\n%s online help: <%s>\n", "GNU coreutils",
		       "https://www.gnu.org/software/coreutils/");
      lVar2 = func_0x00401890 (5, 0);
      if ((lVar2 != 0)
	  && (iVar1 = func_0x00401780 (lVar2, &DAT_0040cf06, 3), iVar1 != 0))
	goto LAB_00401cf8;
    }
  func_0x00401590 ("Full documentation at: <%s%s>\n",
		   "https://www.gnu.org/software/coreutils/", "users");
  uVar3 = 0x40decf;
  if (pcVar5 == "users")
    {
      uVar3 = 0x40ce9e;
    }
LAB_00401d3a:
  ;
  do
    {
      func_0x00401590
	("or available locally via: info \'(coreutils) %s%s\'\n", pcVar5,
	 uVar3);
    LAB_00401d4c:
      ;
      func_0x004018e0 ((ulong) uParm1);
    LAB_00401d53:
      ;
      func_0x00401590 ("Full documentation at: <%s%s>\n",
		       "https://www.gnu.org/software/coreutils/", "users");
      pcVar5 = "users";
      uVar3 = 0x40ce9e;
    }
  while (true);
}

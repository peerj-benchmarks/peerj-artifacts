
#include "head.h"

long null_ARRAY_0061244_0_8_;
long null_ARRAY_0061244_8_8_;
long null_ARRAY_0061264_0_8_;
long null_ARRAY_0061264_16_8_;
long null_ARRAY_0061264_24_8_;
long null_ARRAY_0061264_32_8_;
long null_ARRAY_0061264_40_8_;
long null_ARRAY_0061264_48_8_;
long null_ARRAY_0061264_8_8_;
long null_ARRAY_0061268_0_4_;
long null_ARRAY_0061268_16_8_;
long null_ARRAY_0061268_4_4_;
long null_ARRAY_0061268_8_4_;
long local_68_2_2_;
long local_6b_1_3_;
long local_6c_4_4_;
long local_6e_4_4_;
long local_6f_4_4_;
long local_70_0_2_;
long local_70_4_4_;
long local_71_0_1_;
long local_71_4_4_;
long local_72_4_4_;
long DAT_00000010;
long DAT_0040e704;
long DAT_0040e709;
long DAT_0040e78d;
long DAT_0040f098;
long DAT_0040f09c;
long DAT_0040f0a0;
long DAT_0040f0a3;
long DAT_0040f0a5;
long DAT_0040f0a9;
long DAT_0040f0ad;
long DAT_0040f82b;
long DAT_0040ff48;
long DAT_00410051;
long DAT_00410057;
long DAT_00410069;
long DAT_0041006a;
long DAT_00410088;
long DAT_0041008c;
long DAT_00410116;
long DAT_00612000;
long DAT_00612010;
long DAT_00612020;
long DAT_006123e0;
long DAT_006123f0;
long DAT_00612450;
long DAT_00612454;
long DAT_00612458;
long DAT_0061245c;
long DAT_00612480;
long DAT_00612490;
long DAT_006124a0;
long DAT_006124a8;
long DAT_006124c0;
long DAT_006124c8;
long DAT_00612510;
long DAT_00612511;
long DAT_00612512;
long DAT_00612513;
long DAT_00612518;
long DAT_00612520;
long DAT_00612528;
long DAT_006126b8;
long DAT_006126c0;
long DAT_006126c8;
long DAT_006126d8;
long fde_00410cd8;
long null_ARRAY_0040eed0;
long null_ARRAY_0040ef00;
long null_ARRAY_00410520;
long null_ARRAY_00410760;
long null_ARRAY_00612440;
long null_ARRAY_006124e0;
long null_ARRAY_00612540;
long null_ARRAY_00612640;
long null_ARRAY_00612680;
long PTR_DAT_006123e8;
long PTR_null_ARRAY_00612438;
void
FUN_00402c60 (uint uParm1)
{
  int iVar1;
  long lVar2;
  undefined8 uVar3;
  undefined **ppuVar4;
  undefined *puVar5;
  undefined *local_88;
  char *local_80;
  undefined8 local_78;
  char *local_70;
  char *local_68;
  char *local_60;
  char *local_58;
  char *local_50;
  char *local_48;
  char *local_40;
  char *local_38;
  char *local_30;
  undefined8 local_28;
  undefined8 local_20;

  ppuVar4 = &local_88;
  if (uParm1 != 0)
    {
      func_0x00401780 (DAT_006124a0,
		       "Try \'%s --help\' for more information.\n",
		       DAT_00612528);
      goto LAB_00402e7a;
    }
  func_0x004015d0 ("Usage: %s [OPTION]... [FILE]...\n", DAT_00612528);
  func_0x004015d0
    ("Print the first %d lines of each FILE to standard output.\nWith more than one FILE, precede each with a header giving the file name.\n",
     10);
  uVar3 = DAT_00612480;
  func_0x00401790
    ("\nWith no FILE, or when FILE is -, read standard input.\n",
     DAT_00612480);
  func_0x00401790
    ("\nMandatory arguments to long options are mandatory for short options too.\n",
     uVar3);
  func_0x004015d0
    ("  -c, --bytes=[-]NUM       print the first NUM bytes of each file;\n                             with the leading \'-\', print all but the last\n                             NUM bytes of each file\n  -n, --lines=[-]NUM       print the first NUM lines instead of the first %d;\n                             with the leading \'-\', print all but the last\n                             NUM lines of each file\n",
     10);
  func_0x00401790
    ("  -q, --quiet, --silent    never print headers giving file names\n  -v, --verbose            always print headers giving file names\n",
     uVar3);
  func_0x00401790
    ("  -z, --zero-terminated    line delimiter is NUL, not newline\n",
     uVar3);
  func_0x00401790 ("      --help     display this help and exit\n", uVar3);
  func_0x00401790 ("      --version  output version information and exit\n",
		   uVar3);
  func_0x00401790
    ("\nNUM may have a multiplier suffix:\nb 512, kB 1000, K 1024, MB 1000*1000, M 1024*1024,\nGB 1000*1000*1000, G 1024*1024*1024, and so on for T, P, E, Z, Y.\n",
     uVar3);
  local_88 = &DAT_0040e709;
  local_80 = "test invocation";
  local_78 = 0x40e76c;
  local_70 = "Multi-call invocation";
  local_68 = "sha224sum";
  local_60 = "sha2 utilities";
  local_58 = "sha256sum";
  local_50 = "sha2 utilities";
  local_48 = "sha384sum";
  local_40 = "sha2 utilities";
  local_38 = "sha512sum";
  local_30 = "sha2 utilities";
  local_28 = 0;
  local_20 = 0;
  puVar5 = &DAT_0040e709;
  do
    {
      iVar1 = func_0x00401880 (&DAT_0040e704, puVar5);
      if (iVar1 == 0)
	break;
      ppuVar4 = ppuVar4 + 2;
      puVar5 = *ppuVar4;
    }
  while (puVar5 != (undefined *) 0x0);
  puVar5 = ppuVar4[1];
  if (puVar5 == (undefined *) 0x0)
    {
      func_0x004015d0 ("\n%s online help: <%s>\n", "GNU coreutils",
		       "https://www.gnu.org/software/coreutils/");
      lVar2 = func_0x004018e0 (5, 0);
      if (lVar2 == 0)
	goto LAB_00402e81;
      iVar1 = func_0x004017f0 (lVar2, &DAT_0040e78d, 3);
      if (iVar1 == 0)
	{
	  func_0x004015d0 ("Full documentation at: <%s%s>\n",
			   "https://www.gnu.org/software/coreutils/",
			   &DAT_0040e704);
	  puVar5 = &DAT_0040e704;
	  uVar3 = 0x40e725;
	  goto LAB_00402e68;
	}
      puVar5 = &DAT_0040e704;
    LAB_00402e26:
      ;
      func_0x004015d0
	("Report %s translation bugs to <https://translationproject.org/team/>\n",
	 &DAT_0040e704);
    }
  else
    {
      func_0x004015d0 ("\n%s online help: <%s>\n", "GNU coreutils",
		       "https://www.gnu.org/software/coreutils/");
      lVar2 = func_0x004018e0 (5, 0);
      if ((lVar2 != 0)
	  && (iVar1 = func_0x004017f0 (lVar2, &DAT_0040e78d, 3), iVar1 != 0))
	goto LAB_00402e26;
    }
  func_0x004015d0 ("Full documentation at: <%s%s>\n",
		   "https://www.gnu.org/software/coreutils/", &DAT_0040e704);
  uVar3 = 0x410087;
  if (puVar5 == &DAT_0040e704)
    {
      uVar3 = 0x40e725;
    }
LAB_00402e68:
  ;
  do
    {
      func_0x004015d0
	("or available locally via: info \'(coreutils) %s%s\'\n", puVar5,
	 uVar3);
    LAB_00402e7a:
      ;
      func_0x00401930 ((ulong) uParm1);
    LAB_00402e81:
      ;
      func_0x004015d0 ("Full documentation at: <%s%s>\n",
		       "https://www.gnu.org/software/coreutils/",
		       &DAT_0040e704);
      puVar5 = &DAT_0040e704;
      uVar3 = 0x40e725;
    }
  while (true);
}

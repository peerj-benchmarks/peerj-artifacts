
#include "printenv.h"

long null_ARRAY_0060f44_0_8_;
long null_ARRAY_0060f44_8_8_;
long null_ARRAY_0060f68_0_8_;
long null_ARRAY_0060f68_16_8_;
long null_ARRAY_0060f68_24_8_;
long null_ARRAY_0060f68_32_8_;
long null_ARRAY_0060f68_40_8_;
long null_ARRAY_0060f68_48_8_;
long null_ARRAY_0060f68_8_8_;
long null_ARRAY_0060f6c_0_4_;
long null_ARRAY_0060f6c_16_8_;
long null_ARRAY_0060f6c_4_4_;
long null_ARRAY_0060f6c_8_4_;
long local_68_2_2_;
long local_6b_1_3_;
long local_6c_4_4_;
long local_6e_4_4_;
long local_6f_4_4_;
long local_70_0_2_;
long local_70_4_4_;
long local_71_0_1_;
long local_71_4_4_;
long local_72_4_4_;
long DAT_00000010;
long DAT_0040c8c9;
long DAT_0040c94d;
long DAT_0040c978;
long DAT_0040cd98;
long DAT_0040cd9c;
long DAT_0040cda0;
long DAT_0040cda3;
long DAT_0040cda5;
long DAT_0040cda9;
long DAT_0040cdad;
long DAT_0040d52b;
long DAT_0040d8d5;
long DAT_0040d9d9;
long DAT_0040d9df;
long DAT_0040d9f1;
long DAT_0040d9f2;
long DAT_0040da10;
long DAT_0040da14;
long DAT_0040da9e;
long DAT_0060f020;
long DAT_0060f030;
long DAT_0060f040;
long DAT_0060f3e8;
long DAT_0060f450;
long DAT_0060f454;
long DAT_0060f458;
long DAT_0060f45c;
long DAT_0060f480;
long DAT_0060f4c0;
long DAT_0060f4d0;
long DAT_0060f4e0;
long DAT_0060f4e8;
long DAT_0060f500;
long DAT_0060f508;
long DAT_0060f550;
long DAT_0060f558;
long DAT_0060f560;
long DAT_0060f6f8;
long DAT_0060f700;
long DAT_0060f708;
long DAT_0060f718;
long fde_0040e5c8;
long null_ARRAY_0040ccc0;
long null_ARRAY_0040dec0;
long null_ARRAY_0040e100;
long null_ARRAY_0060f440;
long null_ARRAY_0060f520;
long null_ARRAY_0060f580;
long null_ARRAY_0060f680;
long null_ARRAY_0060f6c0;
long PTR_DAT_0060f3e0;
long PTR_null_ARRAY_0060f438;
void
FUN_004019ee (uint uParm1)
{
  int iVar1;
  long lVar2;
  undefined8 uVar3;
  undefined **ppuVar4;
  char *pcVar5;
  undefined *puVar6;
  undefined *local_88;
  char *local_80;
  undefined8 local_78;
  char *local_70;
  char *local_68;
  char *local_60;
  char *local_58;
  char *local_50;
  char *local_48;
  char *local_40;
  char *local_38;
  char *local_30;
  undefined8 local_28;
  undefined8 local_20;

  ppuVar4 = &local_88;
  if (uParm1 != 0)
    {
      func_0x004015e0 (DAT_0060f4e0,
		       "Try \'%s --help\' for more information.\n",
		       DAT_0060f560);
      goto LAB_00401bc0;
    }
  func_0x00401470
    ("Usage: %s [OPTION]... [VARIABLE]...\nPrint the values of the specified environment VARIABLE(s).\nIf no VARIABLE is specified, print name and value pairs for them all.\n\n",
     DAT_0060f560);
  uVar3 = DAT_0060f480;
  func_0x004015f0
    ("  -0, --null     end each output line with NUL, not newline\n",
     DAT_0060f480);
  func_0x004015f0 ("      --help     display this help and exit\n", uVar3);
  func_0x004015f0 ("      --version  output version information and exit\n",
		   uVar3);
  func_0x00401470
    ("\nNOTE: your shell may have its own version of %s, which usually supersedes\nthe version described here.  Please refer to your shell\'s documentation\nfor details about the options it supports.\n",
     "printenv");
  local_88 = &DAT_0040c8c9;
  local_80 = "test invocation";
  local_78 = 0x40c92c;
  local_70 = "Multi-call invocation";
  local_68 = "sha224sum";
  local_60 = "sha2 utilities";
  local_58 = "sha256sum";
  local_50 = "sha2 utilities";
  local_48 = "sha384sum";
  local_40 = "sha2 utilities";
  local_38 = "sha512sum";
  local_30 = "sha2 utilities";
  local_28 = 0;
  local_20 = 0;
  puVar6 = &DAT_0040c8c9;
  do
    {
      iVar1 = func_0x004016c0 ("printenv", puVar6);
      if (iVar1 == 0)
	break;
      ppuVar4 = ppuVar4 + 2;
      puVar6 = *ppuVar4;
    }
  while (puVar6 != (undefined *) 0x0);
  pcVar5 = ppuVar4[1];
  if (pcVar5 == (char *) 0x0)
    {
      func_0x00401470 ("\n%s online help: <%s>\n", "GNU coreutils",
		       "https://www.gnu.org/software/coreutils/");
      lVar2 = func_0x00401720 (5, 0);
      if (lVar2 == 0)
	goto LAB_00401bc7;
      iVar1 = func_0x00401640 (lVar2, &DAT_0040c94d, 3);
      if (iVar1 == 0)
	{
	  func_0x00401470 ("Full documentation at: <%s%s>\n",
			   "https://www.gnu.org/software/coreutils/",
			   "printenv");
	  pcVar5 = "printenv";
	  uVar3 = 0x40c8e5;
	  goto LAB_00401bae;
	}
      pcVar5 = "printenv";
    LAB_00401b6c:
      ;
      func_0x00401470
	("Report %s translation bugs to <https://translationproject.org/team/>\n",
	 "printenv");
    }
  else
    {
      func_0x00401470 ("\n%s online help: <%s>\n", "GNU coreutils",
		       "https://www.gnu.org/software/coreutils/");
      lVar2 = func_0x00401720 (5, 0);
      if ((lVar2 != 0)
	  && (iVar1 = func_0x00401640 (lVar2, &DAT_0040c94d, 3), iVar1 != 0))
	goto LAB_00401b6c;
    }
  func_0x00401470 ("Full documentation at: <%s%s>\n",
		   "https://www.gnu.org/software/coreutils/", "printenv");
  uVar3 = 0x40da0f;
  if (pcVar5 == "printenv")
    {
      uVar3 = 0x40c8e5;
    }
LAB_00401bae:
  ;
  do
    {
      func_0x00401470
	("or available locally via: info \'(coreutils) %s%s\'\n", pcVar5,
	 uVar3);
    LAB_00401bc0:
      ;
      func_0x00401760 ((ulong) uParm1);
    LAB_00401bc7:
      ;
      func_0x00401470 ("Full documentation at: <%s%s>\n",
		       "https://www.gnu.org/software/coreutils/", "printenv");
      pcVar5 = "printenv";
      uVar3 = 0x40c8e5;
    }
  while (true);
}

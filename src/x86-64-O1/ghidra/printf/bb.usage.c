
#include "printf.h"

long null_ARRAY_0061092_0_8_;
long null_ARRAY_0061092_8_8_;
long null_ARRAY_00610b0_0_8_;
long null_ARRAY_00610b0_16_8_;
long null_ARRAY_00610b0_24_8_;
long null_ARRAY_00610b0_32_8_;
long null_ARRAY_00610b0_40_8_;
long null_ARRAY_00610b0_48_8_;
long null_ARRAY_00610b0_8_8_;
long local_68_2_2_;
long local_6b_1_3_;
long local_6c_4_4_;
long local_6e_4_4_;
long local_6f_4_4_;
long local_70_0_2_;
long local_70_4_4_;
long local_71_0_1_;
long local_71_4_4_;
long local_72_4_4_;
long DAT_0040d2b0;
long DAT_0040d334;
long DAT_0040d338;
long DAT_0040d33b;
long DAT_0040d36e;
long DAT_0040e04d;
long DAT_0040e0a8;
long DAT_0040e0ac;
long DAT_0040e0ae;
long DAT_0040e0b2;
long DAT_0040e0b5;
long DAT_0040e0b7;
long DAT_0040e0bb;
long DAT_0040e0bf;
long DAT_0040e91c;
long DAT_0040ecd5;
long DAT_0040ed66;
long DAT_006104c0;
long DAT_006104d0;
long DAT_006104e0;
long DAT_006108c8;
long DAT_00610930;
long DAT_00610940;
long DAT_00610950;
long DAT_00610960;
long DAT_00610968;
long DAT_00610980;
long DAT_00610988;
long DAT_006109d0;
long DAT_006109d4;
long DAT_006109d8;
long DAT_006109e0;
long DAT_006109e8;
long DAT_006109f0;
long DAT_00610b38;
long DAT_00610b40;
long DAT_00610b44;
long DAT_00610b48;
long DAT_00610b50;
long DAT_00610b58;
long fde_0040f8f8;
long null_ARRAY_0040f180;
long null_ARRAY_0040f3c0;
long null_ARRAY_00610920;
long null_ARRAY_006109a0;
long null_ARRAY_00610a00;
long null_ARRAY_00610b00;
long PTR_DAT_006108c0;
long PTR_null_ARRAY_00610918;
void
FUN_0040204e (uint uParm1)
{
  int iVar1;
  long lVar2;
  undefined8 uVar3;
  undefined **ppuVar4;
  char *pcVar5;
  undefined *puVar6;
  undefined *local_88;
  char *local_80;
  undefined8 local_78;
  char *local_70;
  char *local_68;
  char *local_60;
  char *local_58;
  char *local_50;
  char *local_48;
  char *local_40;
  char *local_38;
  char *local_30;
  undefined8 local_28;
  undefined8 local_20;

  ppuVar4 = &local_88;
  if (uParm1 != 0)
    {
      func_0x004017e0 (DAT_00610960,
		       "Try \'%s --help\' for more information.\n",
		       DAT_006109f0);
      goto LAB_00402257;
    }
  func_0x00401600 ("Usage: %s FORMAT [ARGUMENT]...\n  or:  %s OPTION\n",
		   DAT_006109f0, DAT_006109f0);
  uVar3 = DAT_00610940;
  func_0x004017f0
    ("Print ARGUMENT(s) according to FORMAT, or execute according to OPTION:\n\n",
     DAT_00610940);
  func_0x004017f0 ("      --help     display this help and exit\n", uVar3);
  func_0x004017f0 ("      --version  output version information and exit\n",
		   uVar3);
  func_0x004017f0
    ("\nFORMAT controls the output as in C printf.  Interpreted sequences are:\n\n  \\\"      double quote\n",
     uVar3);
  func_0x004017f0
    ("  \\\\      backslash\n  \\a      alert (BEL)\n  \\b      backspace\n  \\c      produce no further output\n  \\e      escape\n  \\f      form feed\n  \\n      new line\n  \\r      carriage return\n  \\t      horizontal tab\n  \\v      vertical tab\n",
     uVar3);
  func_0x004017f0
    ("  \\NNN    byte with octal value NNN (1 to 3 digits)\n  \\xHH    byte with hexadecimal value HH (1 to 2 digits)\n  \\uHHHH  Unicode (ISO/IEC 10646) character with hex value HHHH (4 digits)\n  \\UHHHHHHHH  Unicode character with hex value HHHHHHHH (8 digits)\n",
     uVar3);
  func_0x004017f0
    ("  %%      a single %\n  %b      ARGUMENT as a string with \'\\\' escapes interpreted,\n          except that octal escapes are of the form \\0 or \\0NNN\n  %q      ARGUMENT is printed in a format that can be reused as shell input,\n          escaping non-printable characters with the proposed POSIX $\'\' syntax.\n\nand all C format specifications ending with one of diouxXfeEgGcs, with\nARGUMENTs converted to proper type first.  Variable widths are handled.\n",
     uVar3);
  func_0x00401600
    ("\nNOTE: your shell may have its own version of %s, which usually supersedes\nthe version described here.  Please refer to your shell\'s documentation\nfor details about the options it supports.\n",
     "printf");
  local_88 = &DAT_0040d2b0;
  local_80 = "test invocation";
  local_78 = 0x40d313;
  local_70 = "Multi-call invocation";
  local_68 = "sha224sum";
  local_60 = "sha2 utilities";
  local_58 = "sha256sum";
  local_50 = "sha2 utilities";
  local_48 = "sha384sum";
  local_40 = "sha2 utilities";
  local_38 = "sha512sum";
  local_30 = "sha2 utilities";
  local_28 = 0;
  local_20 = 0;
  puVar6 = &DAT_0040d2b0;
  do
    {
      iVar1 = func_0x004018c0 ("printf", puVar6);
      if (iVar1 == 0)
	break;
      ppuVar4 = ppuVar4 + 2;
      puVar6 = *ppuVar4;
    }
  while (puVar6 != (undefined *) 0x0);
  pcVar5 = ppuVar4[1];
  if (pcVar5 == (char *) 0x0)
    {
      func_0x00401600 ("\n%s online help: <%s>\n", "GNU coreutils",
		       "https://www.gnu.org/software/coreutils/");
      lVar2 = func_0x00401910 (5, 0);
      if (lVar2 == 0)
	goto LAB_0040225e;
      iVar1 = func_0x00401850 (lVar2, &DAT_0040d334, 3);
      if (iVar1 == 0)
	{
	  func_0x00401600 ("Full documentation at: <%s%s>\n",
			   "https://www.gnu.org/software/coreutils/",
			   "printf");
	  pcVar5 = "printf";
	  uVar3 = 0x40d2cc;
	  goto LAB_00402245;
	}
      pcVar5 = "printf";
    LAB_00402203:
      ;
      func_0x00401600
	("Report %s translation bugs to <https://translationproject.org/team/>\n",
	 "printf");
    }
  else
    {
      func_0x00401600 ("\n%s online help: <%s>\n", "GNU coreutils",
		       "https://www.gnu.org/software/coreutils/");
      lVar2 = func_0x00401910 (5, 0);
      if ((lVar2 != 0)
	  && (iVar1 = func_0x00401850 (lVar2, &DAT_0040d334, 3), iVar1 != 0))
	goto LAB_00402203;
    }
  func_0x00401600 ("Full documentation at: <%s%s>\n",
		   "https://www.gnu.org/software/coreutils/", "printf");
  uVar3 = 0x40e962;
  if (pcVar5 == "printf")
    {
      uVar3 = 0x40d2cc;
    }
LAB_00402245:
  ;
  do
    {
      func_0x00401600
	("or available locally via: info \'(coreutils) %s%s\'\n", pcVar5,
	 uVar3);
    LAB_00402257:
      ;
      func_0x00401970 ((ulong) uParm1);
    LAB_0040225e:
      ;
      func_0x00401600 ("Full documentation at: <%s%s>\n",
		       "https://www.gnu.org/software/coreutils/", "printf");
      pcVar5 = "printf";
      uVar3 = 0x40d2cc;
    }
  while (true);
}

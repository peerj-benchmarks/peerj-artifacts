
#include "unexpand.h"

long null_ARRAY_006109c_0_8_;
long null_ARRAY_006109c_8_8_;
long null_ARRAY_00610c0_0_8_;
long null_ARRAY_00610c0_16_8_;
long null_ARRAY_00610c0_24_8_;
long null_ARRAY_00610c0_32_8_;
long null_ARRAY_00610c0_40_8_;
long null_ARRAY_00610c0_48_8_;
long null_ARRAY_00610c0_8_8_;
long null_ARRAY_00610c4_0_4_;
long null_ARRAY_00610c4_16_8_;
long null_ARRAY_00610c4_4_4_;
long null_ARRAY_00610c4_8_4_;
long local_68_2_2_;
long local_6b_1_3_;
long local_6c_4_4_;
long local_6e_4_4_;
long local_6f_4_4_;
long local_70_0_2_;
long local_70_4_4_;
long local_71_0_1_;
long local_71_4_4_;
long local_72_4_4_;
long DAT_00000010;
long DAT_0040d7c9;
long DAT_0040d84d;
long DAT_0040e058;
long DAT_0040e05c;
long DAT_0040e060;
long DAT_0040e063;
long DAT_0040e065;
long DAT_0040e069;
long DAT_0040e06d;
long DAT_0040e7eb;
long DAT_0040eb95;
long DAT_0040ec99;
long DAT_0040ec9f;
long DAT_0040ecb1;
long DAT_0040ecb2;
long DAT_0040ecd0;
long DAT_0040ecd4;
long DAT_0040ed5e;
long DAT_00610558;
long DAT_00610568;
long DAT_00610578;
long DAT_00610978;
long DAT_006109d0;
long DAT_006109d4;
long DAT_006109d8;
long DAT_006109dc;
long DAT_00610a00;
long DAT_00610a08;
long DAT_00610a10;
long DAT_00610a20;
long DAT_00610a28;
long DAT_00610a40;
long DAT_00610a48;
long DAT_00610a90;
long DAT_00610a94;
long DAT_00610a98;
long DAT_00610aa0;
long DAT_00610aa8;
long DAT_00610ab0;
long DAT_00610ab8;
long DAT_00610ac0;
long DAT_00610ac8;
long DAT_00610ad0;
long DAT_00610ad8;
long DAT_00610ae0;
long DAT_00610ae8;
long DAT_00610af0;
long DAT_00610c78;
long DAT_00610c80;
long DAT_00610c88;
long DAT_00610c90;
long DAT_00610ca0;
long fde_0040f900;
long null_ARRAY_0040dc00;
long null_ARRAY_0040f180;
long null_ARRAY_0040f3c0;
long null_ARRAY_00610960;
long null_ARRAY_006109c0;
long null_ARRAY_00610a60;
long null_ARRAY_00610b00;
long null_ARRAY_00610c00;
long null_ARRAY_00610c40;
long PTR_DAT_00610970;
long PTR_null_ARRAY_006109b8;
void
FUN_00401c9e (uint uParm1)
{
  int iVar1;
  long lVar2;
  undefined8 uVar3;
  undefined **ppuVar4;
  char *pcVar5;
  undefined *puVar6;
  undefined *local_88;
  char *local_80;
  undefined8 local_78;
  char *local_70;
  char *local_68;
  char *local_60;
  char *local_58;
  char *local_50;
  char *local_48;
  char *local_40;
  char *local_38;
  char *local_30;
  undefined8 local_28;
  undefined8 local_20;

  ppuVar4 = &local_88;
  if (uParm1 != 0)
    {
      func_0x00401830 (DAT_00610a20,
		       "Try \'%s --help\' for more information.\n",
		       DAT_00610af0);
      goto LAB_00401e88;
    }
  func_0x00401690 ("Usage: %s [OPTION]... [FILE]...\n", DAT_00610af0);
  uVar3 = DAT_00610a00;
  func_0x00401840
    ("Convert blanks in each FILE to tabs, writing to standard output.\n",
     DAT_00610a00);
  func_0x00401840
    ("\nWith no FILE, or when FILE is -, read standard input.\n", uVar3);
  func_0x00401840
    ("\nMandatory arguments to long options are mandatory for short options too.\n",
     uVar3);
  func_0x00401840
    ("  -a, --all        convert all blanks, instead of just initial blanks\n      --first-only  convert only leading sequences of blanks (overrides -a)\n  -t, --tabs=N     have tabs N characters apart instead of 8 (enables -a)\n",
     uVar3);
  FUN_00402c1c ();
  func_0x00401840 ("      --help     display this help and exit\n", uVar3);
  func_0x00401840 ("      --version  output version information and exit\n",
		   uVar3);
  local_88 = &DAT_0040d7c9;
  local_80 = "test invocation";
  local_78 = 0x40d82c;
  local_70 = "Multi-call invocation";
  local_68 = "sha224sum";
  local_60 = "sha2 utilities";
  local_58 = "sha256sum";
  local_50 = "sha2 utilities";
  local_48 = "sha384sum";
  local_40 = "sha2 utilities";
  local_38 = "sha512sum";
  local_30 = "sha2 utilities";
  local_28 = 0;
  local_20 = 0;
  puVar6 = &DAT_0040d7c9;
  do
    {
      iVar1 = func_0x00401940 ("unexpand", puVar6);
      if (iVar1 == 0)
	break;
      ppuVar4 = ppuVar4 + 2;
      puVar6 = *ppuVar4;
    }
  while (puVar6 != (undefined *) 0x0);
  pcVar5 = ppuVar4[1];
  if (pcVar5 == (char *) 0x0)
    {
      func_0x00401690 ("\n%s online help: <%s>\n", "GNU coreutils",
		       "https://www.gnu.org/software/coreutils/");
      lVar2 = func_0x004019a0 (5, 0);
      if (lVar2 == 0)
	goto LAB_00401e8f;
      iVar1 = func_0x004018a0 (lVar2, &DAT_0040d84d, 3);
      if (iVar1 == 0)
	{
	  func_0x00401690 ("Full documentation at: <%s%s>\n",
			   "https://www.gnu.org/software/coreutils/",
			   "unexpand");
	  pcVar5 = "unexpand";
	  uVar3 = 0x40d7e5;
	  goto LAB_00401e76;
	}
      pcVar5 = "unexpand";
    LAB_00401e34:
      ;
      func_0x00401690
	("Report %s translation bugs to <https://translationproject.org/team/>\n",
	 "unexpand");
    }
  else
    {
      func_0x00401690 ("\n%s online help: <%s>\n", "GNU coreutils",
		       "https://www.gnu.org/software/coreutils/");
      lVar2 = func_0x004019a0 (5, 0);
      if ((lVar2 != 0)
	  && (iVar1 = func_0x004018a0 (lVar2, &DAT_0040d84d, 3), iVar1 != 0))
	goto LAB_00401e34;
    }
  func_0x00401690 ("Full documentation at: <%s%s>\n",
		   "https://www.gnu.org/software/coreutils/", "unexpand");
  uVar3 = 0x40eccf;
  if (pcVar5 == "unexpand")
    {
      uVar3 = 0x40d7e5;
    }
LAB_00401e76:
  ;
  do
    {
      func_0x00401690
	("or available locally via: info \'(coreutils) %s%s\'\n", pcVar5,
	 uVar3);
    LAB_00401e88:
      ;
      func_0x00401a00 ((ulong) uParm1);
    LAB_00401e8f:
      ;
      func_0x00401690 ("Full documentation at: <%s%s>\n",
		       "https://www.gnu.org/software/coreutils/", "unexpand");
      pcVar5 = "unexpand";
      uVar3 = 0x40d7e5;
    }
  while (true);
}

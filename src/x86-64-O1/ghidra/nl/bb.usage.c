
#include "nl.h"

long null_ARRAY_0062154_0_8_;
long null_ARRAY_0062154_8_8_;
long null_ARRAY_0062166_16_8_;
long null_ARRAY_0062166_8_8_;
long null_ARRAY_00621bc_0_8_;
long null_ARRAY_00621bc_16_8_;
long null_ARRAY_00621bc_24_8_;
long null_ARRAY_00621bc_32_8_;
long null_ARRAY_00621bc_40_8_;
long null_ARRAY_00621bc_48_8_;
long null_ARRAY_00621bc_8_8_;
long null_ARRAY_00621c0_0_4_;
long null_ARRAY_00621c0_16_8_;
long null_ARRAY_00621c0_4_4_;
long null_ARRAY_00621c0_8_4_;
long local_10_1_7_;
long local_11_0_1_;
long local_68_2_2_;
long local_6b_1_3_;
long local_6c_4_4_;
long local_6e_4_4_;
long local_6f_4_4_;
long local_70_0_2_;
long local_70_4_4_;
long local_71_0_1_;
long local_71_4_4_;
long local_72_4_4_;
long local_9_0_4_;
long local_a_0_1_;
long local_a_0_4_;
long DAT_00000010;
long DAT_0041aa55;
long DAT_0041aa58;
long DAT_0041aadc;
long DAT_0041aade;
long DAT_0041ab1b;
long DAT_0041ab1e;
long DAT_0041ab21;
long DAT_0041ac16;
long DAT_0041b738;
long DAT_0041b73c;
long DAT_0041b740;
long DAT_0041b743;
long DAT_0041b745;
long DAT_0041b749;
long DAT_0041b74d;
long DAT_0041beeb;
long DAT_0041c608;
long DAT_0041c711;
long DAT_0041c717;
long DAT_0041c729;
long DAT_0041c72a;
long DAT_0041c748;
long DAT_0041d382;
long DAT_0041d389;
long DAT_0041d3c7;
long DAT_0041d3e0;
long DAT_0041d454;
long DAT_0041d4de;
long DAT_00621000;
long DAT_00621010;
long DAT_00621020;
long DAT_00621488;
long DAT_00621490;
long DAT_00621498;
long DAT_006214a0;
long DAT_006214a8;
long DAT_006214e0;
long DAT_00621550;
long DAT_00621554;
long DAT_00621558;
long DAT_0062155c;
long DAT_00621580;
long DAT_00621588;
long DAT_00621590;
long DAT_006215a0;
long DAT_006215a8;
long DAT_006215c0;
long DAT_006215c8;
long DAT_00621640;
long DAT_00621648;
long DAT_00621650;
long DAT_00621658;
long DAT_00621678;
long DAT_00621680;
long DAT_00621688;
long DAT_00621690;
long DAT_00621698;
long DAT_006216a0;
long DAT_006216a8;
long DAT_00621a80;
long DAT_00621a88;
long DAT_00621a90;
long DAT_00621a98;
long DAT_00621c38;
long DAT_00621c40;
long DAT_00621c48;
long DAT_00621c58;
long DAT_00621c60;
long fde_0041e390;
long null_ARRAY_0041b500;
long null_ARRAY_0041d0c0;
long null_ARRAY_0041d100;
long null_ARRAY_0041d900;
long null_ARRAY_0041db40;
long null_ARRAY_00621540;
long null_ARRAY_006215e0;
long null_ARRAY_00621660;
long null_ARRAY_006216c0;
long null_ARRAY_006217c0;
long null_ARRAY_006218c0;
long null_ARRAY_006219c0;
long null_ARRAY_00621a00;
long null_ARRAY_00621a40;
long null_ARRAY_00621ac0;
long null_ARRAY_00621bc0;
long null_ARRAY_00621c00;
long PTR_DAT_006214b0;
long PTR_DAT_006214b8;
long PTR_DAT_006214c0;
long PTR_DAT_006214c8;
long PTR_DAT_006214d8;
long PTR_null_ARRAY_00621538;
long PTR_s___ld_s_00621480;
long PTR_s_t_006214d0;
long stack0x00000008;
void
FUN_00402688 (uint uParm1)
{
  undefined8 uVar1;
  int iVar2;
  long lVar3;
  undefined1 *puVar4;
  undefined **ppuVar5;
  undefined *puVar6;
  undefined *local_88;
  char *local_80;
  undefined8 local_78;
  char *local_70;
  char *local_68;
  char *local_60;
  char *local_58;
  char *local_50;
  char *local_48;
  char *local_40;
  char *local_38;
  char *local_30;
  undefined8 local_28;
  undefined8 local_20;

  ppuVar5 = &local_88;
  if (uParm1 != 0)
    {
      func_0x00401c20 (DAT_006215a0,
		       "Try \'%s --help\' for more information.\n",
		       DAT_00621a98);
      goto LAB_004028a1;
    }
  func_0x00401a20 ("Usage: %s [OPTION]... [FILE]...\n", DAT_00621a98);
  uVar1 = DAT_00621580;
  func_0x00401c30
    ("Write each FILE to standard output, with line numbers added.\n",
     DAT_00621580);
  func_0x00401c30
    ("\nWith no FILE, or when FILE is -, read standard input.\n", uVar1);
  func_0x00401c30
    ("\nMandatory arguments to long options are mandatory for short options too.\n",
     uVar1);
  func_0x00401c30
    ("  -b, --body-numbering=STYLE      use STYLE for numbering body lines\n  -d, --section-delimiter=CC      use CC for logical page delimiters\n  -f, --footer-numbering=STYLE    use STYLE for numbering footer lines\n",
     uVar1);
  func_0x00401c30
    ("  -h, --header-numbering=STYLE    use STYLE for numbering header lines\n  -i, --line-increment=NUMBER     line number increment at each line\n  -l, --join-blank-lines=NUMBER   group of NUMBER empty lines counted as one\n  -n, --number-format=FORMAT      insert line numbers according to FORMAT\n  -p, --no-renumber               do not reset line numbers for each section\n  -s, --number-separator=STRING   add STRING after (possible) line number\n",
     uVar1);
  func_0x00401c30
    ("  -v, --starting-line-number=NUMBER  first line number for each section\n  -w, --number-width=NUMBER       use NUMBER columns for line numbers\n",
     uVar1);
  func_0x00401c30 ("      --help     display this help and exit\n", uVar1);
  func_0x00401c30 ("      --version  output version information and exit\n",
		   uVar1);
  func_0x00401c30
    ("\nBy default, selects -v1 -i1 -l1 -sTAB -w6 -nrn -hn -bt -fn.\nCC are two delimiter characters used to construct logical page delimiters,\na missing second character implies :.  Type \\\\ for \\.  STYLE is one of:\n",
     uVar1);
  func_0x00401c30
    ("\n  a         number all lines\n  t         number only nonempty lines\n  n         number no lines\n  pBRE      number only lines that contain a match for the basic regular\n            expression, BRE\n\nFORMAT is one of:\n\n  ln   left justified, no leading zeros\n  rn   right justified, no leading zeros\n  rz   right justified, leading zeros\n\n",
     uVar1);
  local_88 = &DAT_0041aa58;
  local_80 = "test invocation";
  local_78 = 0x41aabb;
  local_70 = "Multi-call invocation";
  local_68 = "sha224sum";
  local_60 = "sha2 utilities";
  local_58 = "sha256sum";
  local_50 = "sha2 utilities";
  local_48 = "sha384sum";
  local_40 = "sha2 utilities";
  local_38 = "sha512sum";
  local_30 = "sha2 utilities";
  local_28 = 0;
  local_20 = 0;
  puVar6 = &DAT_0041aa58;
  do
    {
      iVar2 = func_0x00401d70 (&DAT_0041aa55, puVar6);
      if (iVar2 == 0)
	break;
      ppuVar5 = ppuVar5 + 2;
      puVar6 = *ppuVar5;
    }
  while (puVar6 != (undefined *) 0x0);
  puVar6 = ppuVar5[1];
  if (puVar6 == (undefined *) 0x0)
    {
      func_0x00401a20 ("\n%s online help: <%s>\n", "GNU coreutils",
		       "https://www.gnu.org/software/coreutils/");
      lVar3 = func_0x00401dd0 (5, 0);
      if (lVar3 == 0)
	goto LAB_004028a8;
      iVar2 = func_0x00401ca0 (lVar3, &DAT_0041aadc, 3);
      if (iVar2 == 0)
	{
	  func_0x00401a20 ("Full documentation at: <%s%s>\n",
			   "https://www.gnu.org/software/coreutils/",
			   &DAT_0041aa55);
	  puVar6 = &DAT_0041aa55;
	  puVar4 = (undefined1 *) 0x41aa74;
	  goto LAB_0040288f;
	}
      puVar6 = &DAT_0041aa55;
    LAB_0040284d:
      ;
      func_0x00401a20
	("Report %s translation bugs to <https://translationproject.org/team/>\n",
	 &DAT_0041aa55);
    }
  else
    {
      func_0x00401a20 ("\n%s online help: <%s>\n", "GNU coreutils",
		       "https://www.gnu.org/software/coreutils/");
      lVar3 = func_0x00401dd0 (5, 0);
      if ((lVar3 != 0)
	  && (iVar2 = func_0x00401ca0 (lVar3, &DAT_0041aadc, 3), iVar2 != 0))
	goto LAB_0040284d;
    }
  func_0x00401a20 ("Full documentation at: <%s%s>\n",
		   "https://www.gnu.org/software/coreutils/", &DAT_0041aa55);
  puVar4 = &DAT_0041ac16;
  if (puVar6 == &DAT_0041aa55)
    {
      puVar4 = (undefined1 *) 0x41aa74;
    }
LAB_0040288f:
  ;
  do
    {
      func_0x00401a20
	("or available locally via: info \'(coreutils) %s%s\'\n", puVar6,
	 puVar4);
    LAB_004028a1:
      ;
      func_0x00401e40 ((ulong) uParm1);
    LAB_004028a8:
      ;
      func_0x00401a20 ("Full documentation at: <%s%s>\n",
		       "https://www.gnu.org/software/coreutils/",
		       &DAT_0041aa55);
      puVar6 = &DAT_0041aa55;
      puVar4 = (undefined1 *) 0x41aa74;
    }
  while (true);
}

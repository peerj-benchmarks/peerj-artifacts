typedef unsigned char   undefined;

typedef unsigned char    byte;
typedef unsigned char    dwfenc;
typedef unsigned int    dword;
typedef unsigned long    qword;
typedef unsigned char    undefined1;
typedef unsigned short    undefined2;
typedef unsigned int    undefined3;
typedef unsigned int    undefined4;
typedef unsigned long    undefined6;
typedef unsigned long    undefined7;
typedef unsigned long    undefined8;
typedef unsigned short    word;
#define unkbyte9   unsigned long long
#define unkbyte10   unsigned long long
#define unkbyte11   unsigned long long
#define unkbyte12   unsigned long long
#define unkbyte13   unsigned long long
#define unkbyte14   unsigned long long
#define unkbyte15   unsigned long long
#define unkbyte16   unsigned long long

#define unkuint9   unsigned long long
#define unkuint10   unsigned long long
#define unkuint11   unsigned long long
#define unkuint12   unsigned long long
#define unkuint13   unsigned long long
#define unkuint14   unsigned long long
#define unkuint15   unsigned long long
#define unkuint16   unsigned long long

#define unkint9   long long
#define unkint10   long long
#define unkint11   long long
#define unkint12   long long
#define unkint13   long long
#define unkint14   long long
#define unkint15   long long
#define unkint16   long long

#define unkfloat1   float
#define unkfloat2   float
#define unkfloat3   float
#define unkfloat5   double
#define unkfloat6   double
#define unkfloat7   double
#define unkfloat9   long double
#define unkfloat11   long double
#define unkfloat12   long double
#define unkfloat13   long double
#define unkfloat14   long double
#define unkfloat15   long double
#define unkfloat16   long double

#define BADSPACEBASE   void
#define code   void

#define ulong long
#define true 1
#define bool int
#define false 0
#define float10 int
#define ushort int
#define undefined7 long
#define undefined2 long
#define uint5 long
#define uint6 long

// Add definition for uint
typedef unsigned int    uint;

#define sbyte char

extern imperfection_wrapper(void);
typedef struct eh_frame_hdr eh_frame_hdr, *Peh_frame_hdr;

struct eh_frame_hdr {
    byte eh_frame_hdr_version; // Exception Handler Frame Header Version
    dwfenc eh_frame_pointer_encoding; // Exception Handler Frame Pointer Encoding
    dwfenc eh_frame_desc_entry_count_encoding; // Encoding of # of Exception Handler FDEs
    dwfenc eh_frame_table_encoding; // Exception Handler Table Encoding
};

typedef struct fde_table_entry fde_table_entry, *Pfde_table_entry;

struct fde_table_entry {
    dword initial_loc; // Initial Location
    dword data_loc; // Data location
};

typedef enum Elf64_DynTag {
    DT_AUDIT=1879047932,
    DT_AUXILIARY=2147483645,
    DT_BIND_NOW=24,
    DT_CHECKSUM=1879047672,
    DT_CONFIG=1879047930,
    DT_DEBUG=21,
    DT_DEPAUDIT=1879047931,
    DT_ENCODING=32,
    DT_FEATURE_1=1879047676,
    DT_FILTER=2147483647,
    DT_FINI=13,
    DT_FINI_ARRAY=26,
    DT_FINI_ARRAYSZ=28,
    DT_FLAGS=30,
    DT_FLAGS_1=1879048187,
    DT_GNU_CONFLICT=1879047928,
    DT_GNU_CONFLICTSZ=1879047670,
    DT_GNU_HASH=1879047925,
    DT_GNU_LIBLIST=1879047929,
    DT_GNU_LIBLISTSZ=1879047671,
    DT_GNU_PRELINKED=1879047669,
    DT_HASH=4,
    DT_INIT=12,
    DT_INIT_ARRAY=25,
    DT_INIT_ARRAYSZ=27,
    DT_JMPREL=23,
    DT_MOVEENT=1879047674,
    DT_MOVESZ=1879047675,
    DT_MOVETAB=1879047934,
    DT_NEEDED=1,
    DT_NULL=0,
    DT_PLTGOT=3,
    DT_PLTPAD=1879047933,
    DT_PLTPADSZ=1879047673,
    DT_PLTREL=20,
    DT_PLTRELSZ=2,
    DT_POSFLAG_1=1879047677,
    DT_PREINIT_ARRAYSZ=33,
    DT_REL=17,
    DT_RELA=7,
    DT_RELACOUNT=1879048185,
    DT_RELAENT=9,
    DT_RELASZ=8,
    DT_RELCOUNT=1879048186,
    DT_RELENT=19,
    DT_RELSZ=18,
    DT_RPATH=15,
    DT_RUNPATH=29,
    DT_SONAME=14,
    DT_STRSZ=10,
    DT_STRTAB=5,
    DT_SYMBOLIC=16,
    DT_SYMENT=11,
    DT_SYMINENT=1879047679,
    DT_SYMINFO=1879047935,
    DT_SYMINSZ=1879047678,
    DT_SYMTAB=6,
    DT_TEXTREL=22,
    DT_TLSDESC_GOT=1879047927,
    DT_TLSDESC_PLT=1879047926,
    DT_VERDEF=1879048188,
    DT_VERDEFNUM=1879048189,
    DT_VERNEED=1879048190,
    DT_VERNEEDNUM=1879048191,
    DT_VERSYM=1879048176
} Elf64_DynTag;

typedef enum Elf_ProgramHeaderType {
    PT_DYNAMIC=2,
    PT_GNU_EH_FRAME=1685382480,
    PT_GNU_RELRO=1685382482,
    PT_GNU_STACK=1685382481,
    PT_INTERP=3,
    PT_LOAD=1,
    PT_NOTE=4,
    PT_NULL=0,
    PT_PHDR=6,
    PT_SHLIB=5,
    PT_TLS=7
} Elf_ProgramHeaderType;

typedef struct Elf64_Dyn Elf64_Dyn, *PElf64_Dyn;

struct Elf64_Dyn {
    enum Elf64_DynTag d_tag;
    qword d_val;
};

typedef struct Elf64_Shdr Elf64_Shdr, *PElf64_Shdr;

typedef enum Elf_SectionHeaderType {
    SHT_CHECKSUM=1879048184,
    SHT_DYNAMIC=6,
    SHT_DYNSYM=11,
    SHT_FINI_ARRAY=15,
    SHT_GNU_ATTRIBUTES=1879048181,
    SHT_GNU_HASH=1879048182,
    SHT_GNU_LIBLIST=1879048183,
    SHT_GNU_verdef=1879048189,
    SHT_GNU_verneed=1879048190,
    SHT_GNU_versym=1879048191,
    SHT_GROUP=17,
    SHT_HASH=5,
    SHT_INIT_ARRAY=14,
    SHT_NOBITS=8,
    SHT_NOTE=7,
    SHT_NULL=0,
    SHT_PREINIT_ARRAY=16,
    SHT_PROGBITS=1,
    SHT_REL=9,
    SHT_RELA=4,
    SHT_SHLIB=10,
    SHT_STRTAB=3,
    SHT_SUNW_COMDAT=1879048187,
    SHT_SUNW_move=1879048186,
    SHT_SUNW_syminfo=1879048188,
    SHT_SYMTAB=2,
    SHT_SYMTAB_SHNDX=18
} Elf_SectionHeaderType;

struct Elf64_Shdr {
    dword sh_name;
    enum Elf_SectionHeaderType sh_type;
    qword sh_flags;
    qword sh_addr;
    qword sh_offset;
    qword sh_size;
    dword sh_link;
    dword sh_info;
    qword sh_addralign;
    qword sh_entsize;
};

typedef struct Elf64_Phdr Elf64_Phdr, *PElf64_Phdr;

struct Elf64_Phdr {
    enum Elf_ProgramHeaderType p_type;
    dword p_flags;
    qword p_offset;
    qword p_vaddr;
    qword p_paddr;
    qword p_filesz;
    qword p_memsz;
    qword p_align;
};

typedef struct Elf64_Sym Elf64_Sym, *PElf64_Sym;

struct Elf64_Sym {
    dword st_name;
    byte st_info;
    byte st_other;
    word st_shndx;
    qword st_value;
    qword st_size;
};

typedef struct Elf64_Rela Elf64_Rela, *PElf64_Rela;

struct Elf64_Rela {
    qword r_offset; // location to apply the relocation action
    qword r_info; // the symbol table index and the type of relocation
    qword r_addend; // a constant addend used to compute the relocatable field value
};

typedef struct Elf64_Ehdr Elf64_Ehdr, *PElf64_Ehdr;

struct Elf64_Ehdr {
    byte e_ident_magic_num;
    char e_ident_magic_str[3];
    byte e_ident_class;
    byte e_ident_data;
    byte e_ident_version;
    byte e_ident_pad[9];
    word e_type;
    word e_machine;
    dword e_version;
    qword e_entry;
    qword e_phoff;
    qword e_shoff;
    dword e_flags;
    word e_ehsize;
    word e_phentsize;
    word e_phnum;
    word e_shentsize;
    word e_shnum;
    word e_shstrndx;
};




undefined8 _DT_INIT(void);
void FUN_004019f0(void);
void entry(void);
void FUN_00401fd0(void);
void FUN_00402050(void);
void FUN_004020d0(void);
void FUN_0040210e(void);
undefined8 FUN_00402172(char **ppcParm1,undefined8 *puParm2,undefined8 uParm3);
undefined8 FUN_00402227(undefined8 uParm1);
void FUN_00402688(uint uParm1);
ulong FUN_0040293f(uint uParm1,undefined8 *puParm2);
void FUN_00402ee1(void);
void FUN_00402f87(long lParm1,ulong uParm2);
void FUN_00402faa(undefined8 uParm1);
long * FUN_00402fc2(long *plParm1,undefined8 uParm2,char cParm3);
void FUN_00403092(undefined8 uParm1,undefined8 uParm2);
void FUN_004030a5(long lParm1);
int * FUN_0040313a(int *piParm1,int iParm2);
undefined * FUN_0040316a(char *pcParm1,int iParm2);
ulong FUN_00403222(undefined *param_1,ulong param_2,long param_3,ulong param_4,uint param_5,ulong param_6,long param_7,char *param_8,char *param_9);
undefined1 * FUN_00403fd9(int iParm1,undefined8 uParm2,undefined8 uParm3,uint *puParm4);
ulong FUN_0040417b(undefined1 *puParm1,byte bParm2,undefined8 uParm3);
void FUN_004041af(undefined8 uParm1,undefined8 uParm2,char cParm3);
void FUN_00404238(undefined8 uParm1,char cParm2);
void FUN_00404251(undefined8 uParm1);
void FUN_00404264(uint uParm1,undefined8 uParm2,undefined8 uParm3);
void FUN_004042f3(void);
void FUN_00404306(undefined8 uParm1,undefined8 uParm2);
void FUN_0040431b(undefined8 uParm1);
void FUN_00404331(undefined8 uParm1,long lParm2,undefined8 uParm3,undefined8 uParm4,undefined8 *puParm5,undefined8 uParm6);
void FUN_0040456e(void);
void FUN_004045c0(void);
void FUN_00404645(long lParm1);
long FUN_0040465f(long lParm1,long lParm2);
void FUN_00404692(long lParm1,ulong *puParm2);
long FUN_004046e3(void);
long FUN_0040470b(undefined8 param_1,ulong param_2,long param_3,long param_4,undefined8 param_5,undefined8 param_6,uint param_7);
void FUN_004047fd(undefined8 uParm1,undefined8 uParm2,undefined8 uParm3,undefined8 uParm4,undefined8 uParm5,undefined8 uParm6);
ulong FUN_0040481e(long *plParm1,int iParm2,int iParm3);
ulong FUN_004048a9(char *pcParm1,char **ppcParm2,uint uParm3,long *plParm4,long lParm5);
undefined8 FUN_00404c57(ulong uParm1,ulong uParm2,ulong uParm3);
void FUN_00404c9f(void);
void FUN_00404ccc(undefined8 uParm1);
void FUN_00404d0c(uint uParm1,uint uParm2,undefined8 uParm3,undefined8 uParm4);
void FUN_00404d63(undefined4 uParm1,undefined4 uParm2,undefined4 uParm3,undefined4 uParm4,undefined4 uParm5,undefined4 uParm6,undefined4 uParm7,undefined4 uParm8,ulong param_1,ulong param_2,undefined8 param_3,undefined8 param_4,undefined8 param_5,undefined8 param_6);
undefined8 FUN_00404e36(undefined8 uParm1);
ulong FUN_00404eb9(long lParm1);
undefined8 FUN_00404f49(void);
void FUN_00404f5c(void);
void FUN_00404f6a(long lParm1,int *piParm2);
ulong FUN_00405029(int param_1,undefined8 *param_2,char *param_3,long *param_4,int *param_5,int param_6,int *param_7,int param_8,undefined8 param_9);
ulong FUN_004054e4(uint param_1,undefined8 *param_2,char *param_3,long param_4,undefined8 param_5,ulong param_6,uint *param_7,int param_8);
void FUN_00405960(void);
void FUN_004059b6(void);
void FUN_004059cc(long lParm1);
ulong FUN_004059e6(uint *puParm1,byte *pbParm2,long lParm3);
void FUN_00405a4b(long lParm1,long lParm2);
void FUN_00405a79(long lParm1,long lParm2);
ulong FUN_00405aaa(long lParm1,long lParm2);
void FUN_00405ad9(ulong *puParm1);
void FUN_00405aeb(long lParm1,long lParm2);
void FUN_00405b04(long lParm1,long lParm2);
ulong FUN_00405b1d(long lParm1,long lParm2);
ulong FUN_00405b69(long lParm1,long lParm2);
void FUN_00405b83(long *plParm1);
ulong FUN_00405bc8(long lParm1,long lParm2);
long FUN_00405c18(long lParm1,long lParm2);
void FUN_00405c84(long lParm1,long lParm2);
undefined8 FUN_00405cc4(long lParm1,long lParm2);
undefined8 FUN_00405d4d(undefined8 uParm1,long lParm2);
undefined8 FUN_00405dab(char *pcParm1,long lParm2,ulong uParm3);
undefined8 FUN_00405eb6(long lParm1,long lParm2);
ulong FUN_00405ecc(long lParm1,uint uParm2,long lParm3,long lParm4,long lParm5);
undefined8 FUN_00406093(long lParm1,long lParm2,undefined8 uParm3,undefined8 uParm4,long lParm5);
long FUN_004060f2(long lParm1,long lParm2);
undefined8 FUN_00406195(long lParm1,long lParm2,undefined8 uParm3,undefined8 uParm4,undefined8 uParm5,undefined8 uParm6);
undefined8 FUN_0040628a(undefined4 *param_1,long *param_2,char *param_3,int param_4,undefined8 param_5,undefined8 param_6,char param_7);
undefined8 FUN_004064ff(long *plParm1,code *pcParm2,undefined8 uParm3);
undefined8 FUN_00406557(long *plParm1,code *pcParm2,undefined8 uParm3);
undefined8 FUN_004065ac(long lParm1,ulong uParm2);
undefined8 FUN_00406653(long *plParm1,undefined8 uParm2);
void FUN_004066b3(undefined8 uParm1);
long FUN_004066cb(long lParm1,long *plParm2,long *plParm3,undefined8 *puParm4);
long ** FUN_00406799(long **pplParm1,undefined8 uParm2);
void FUN_0040682c(void);
long FUN_00406841(undefined4 *puParm1,long *plParm2,long lParm3);
ulong FUN_0040695e(undefined8 uParm1,long lParm2);
undefined8 FUN_004069cd(undefined8 *puParm1,undefined8 uParm2);
ulong FUN_00406a1d(long *plParm1,long lParm2);
ulong FUN_00406b16(long *plParm1,undefined8 uParm2,long lParm3,long lParm4,uint uParm5);
undefined8 FUN_00406c03(long *plParm1,long lParm2);
undefined8 FUN_00406c43(long *plParm1,ulong *puParm2,ulong uParm3);
undefined8 FUN_00406d20(long lParm1,long lParm2,undefined8 uParm3);
void FUN_00406e5a(long *plParm1);
void FUN_00406ecb(long *plParm1);
undefined8 FUN_0040705e(long *plParm1);
undefined8 FUN_004075de(long lParm1,int iParm2);
undefined8 FUN_004076b2(long lParm1,long lParm2);
undefined8 FUN_0040772b(long *plParm1,long lParm2);
undefined8 FUN_004078d0(long *plParm1,long lParm2);
undefined8 FUN_00407952(long *plParm1,long lParm2,long lParm3);
undefined8 FUN_00407ae2(long *plParm1,long lParm2,long lParm3);
ulong FUN_00407c79(long lParm1,long lParm2,ulong uParm3);
ulong FUN_00407d46(long lParm1,undefined8 *puParm2,long lParm3);
long FUN_00407e75(long lParm1,long lParm2,undefined8 uParm3);
undefined8 FUN_00407f33(long *plParm1,long *plParm2,ulong uParm3);
void FUN_0040858e(undefined8 uParm1,long lParm2);
long FUN_0040859f(undefined8 uParm1,byte *pbParm2,undefined8 uParm3);
void FUN_00408645(undefined8 *puParm1);
void FUN_00408664(undefined8 *puParm1);
undefined8 FUN_00408691(undefined8 uParm1,long lParm2);
long FUN_004086a8(long *plParm1,undefined8 uParm2,undefined8 uParm3);
undefined8 FUN_0040888b(long *plParm1,long lParm2);
void FUN_00408915(long *plParm1,long lParm2,undefined8 uParm3);
ulong FUN_004089b8(long *plParm1,long lParm2,long lParm3,long lParm4,uint uParm5);
undefined8 FUN_00408c7b(undefined8 *puParm1,long *plParm2,long lParm3,byte bParm4);
void FUN_00408ea0(long lParm1);
ulong * FUN_00408efa(undefined4 *puParm1,long *plParm2,long lParm3,uint uParm4);
void FUN_004091af(long *plParm1);
void FUN_00409203(long lParm1);
void FUN_0040922d(long *plParm1);
ulong FUN_00409396(long *plParm1,undefined8 *puParm2,long lParm3,uint uParm4);
ulong FUN_004094b5(long *plParm1,long lParm2,undefined8 uParm3,undefined8 uParm4);
void FUN_00409678(long lParm1);
undefined8 FUN_0040972b(long *plParm1);
undefined8 FUN_0040978b(long lParm1,long lParm2);
undefined8 FUN_00409979(long *plParm1,undefined8 *puParm2,long lParm3,undefined8 uParm4,undefined8 *puParm5);
undefined8 FUN_00409a3a(long lParm1,undefined8 uParm2,long lParm3,long *plParm4,char *pcParm5,uint uParm6);
long FUN_0040a092(long lParm1,undefined8 uParm2,undefined8 uParm3,char *pcParm4,byte bParm5,int *piParm6);
void FUN_0040a273(long **pplParm1,long lParm2,long lParm3);
undefined8 FUN_0040a630(byte **ppbParm1,byte *pbParm2,ulong uParm3);
ulong FUN_0040acc5(long *plParm1,long lParm2,long lParm3,long lParm4);
ulong FUN_0040afc9(long **pplParm1,long lParm2,long lParm3,long *plParm4,char cParm5);
ulong * FUN_0040b769(undefined4 *puParm1,long *plParm2,long lParm3);
ulong FUN_0040b936(long lParm1,undefined8 uParm2,long lParm3,undefined8 uParm4,uint uParm5);
ulong FUN_0040bb4d(undefined8 uParm1,long lParm2,long lParm3,long lParm4);
ulong FUN_0040bbf7(long param_1,long *param_2,long param_3,long param_4,undefined8 param_5,long param_6,uint param_7);
undefined8 FUN_0040c400(long lParm1,long *plParm2,undefined8 *puParm3,undefined8 uParm4,long lParm5);
ulong FUN_0040c5dc(long lParm1,long lParm2);
long FUN_0040cd42(int *piParm1,long lParm2,long lParm3);
ulong FUN_0040cede(long lParm1,ulong *puParm2,ulong uParm3,long lParm4);
ulong FUN_0040d64d(long lParm1,long *plParm2);
ulong FUN_0040d901(long *plParm1,long lParm2);
ulong FUN_0040e555(long **param_1,long param_2,ulong param_3,long param_4,long param_5,ulong param_6,long *param_7,undefined8 *param_8,uint param_9);
long FUN_0040fba5(long lParm1,long *plParm2,undefined8 *puParm3,ulong uParm4,long lParm5,int *piParm6);
long FUN_00410fe8(undefined8 uParm1,undefined8 *puParm2,long lParm3,undefined8 uParm4,long lParm5,int *piParm6);
long FUN_00411128(undefined8 uParm1,long *plParm2,long lParm3,ulong uParm4,long lParm5,int *piParm6);
ulong FUN_0041127b(long **pplParm1,undefined8 uParm2,ulong uParm3,long *plParm4);
char * FUN_00411ee0(undefined8 uParm1,undefined8 uParm2,long lParm3);
undefined8 FUN_00411f3b(long *plParm1);
long FUN_00411fb7(long param_1,undefined8 param_2,long param_3,long param_4,long param_5,undefined8 param_6,ulong *param_7,char param_8);
void FUN_00412353(void);
ulong FUN_00412368(undefined8 uParm1,undefined8 uParm2,undefined8 uParm3);
ulong FUN_00412435(undefined8 uParm1);
undefined8 FUN_00412496(void);
ulong FUN_0041249e(ulong uParm1);
char * FUN_0041252c(void);
ulong FUN_00412867(char *pcParm1,long lParm2);
long FUN_004128ae(ulong uParm1,uint *puParm2,ulong uParm3,uint *puParm4,long *plParm5);
ulong FUN_004129d2(void);
ulong FUN_00412a04(void);
int * FUN_00412c11(ulong uParm1,long lParm2,ulong uParm3,uint *puParm4,ulong *puParm5);
char * FUN_004131f4(int iParm1,long lParm2,long lParm3,long lParm4,uint uParm5);
void FUN_0041380c(uint param_1,undefined8 param_2);
ulong FUN_004139d2(void);
void FUN_00413be5(undefined8 uParm1,uint uParm2);
undefined8 *FUN_00413d86(undefined8 *puParm1,undefined8 *puParm2,undefined8 *puParm3,undefined8 uParm4);
double FUN_00419923(int *piParm1);
void FUN_0041996b(int *param_1);
undefined8 FUN_004199ec(uint *puParm1,ulong *puParm2);
undefined8 FUN_00419e11(byte *pbParm1,ulong *puParm2,byte **ppbParm3);
ulong FUN_0041a976(void);
ulong FUN_0041a99e(void);
void FUN_0041a9d0(void);
undefined8 _DT_FINI(void);
undefined FUN_00622000();
undefined FUN_00622008();
undefined FUN_00622010();
undefined FUN_00622018();
undefined FUN_00622020();
undefined FUN_00622028();
undefined FUN_00622030();
undefined FUN_00622038();
undefined FUN_00622040();
undefined FUN_00622048();
undefined FUN_00622050();
undefined FUN_00622058();
undefined FUN_00622060();
undefined FUN_00622068();
undefined FUN_00622070();
undefined FUN_00622078();
undefined FUN_00622080();
undefined FUN_00622088();
undefined FUN_00622090();
undefined FUN_00622098();
undefined FUN_006220a0();
undefined FUN_006220a8();
undefined FUN_006220b0();
undefined FUN_006220b8();
undefined FUN_006220c0();
undefined FUN_006220c8();
undefined FUN_006220d0();
undefined FUN_006220d8();
undefined FUN_006220e0();
undefined FUN_006220e8();
undefined FUN_006220f0();
undefined FUN_006220f8();
undefined FUN_00622100();
undefined FUN_00622108();
undefined FUN_00622110();
undefined FUN_00622118();
undefined FUN_00622120();
undefined FUN_00622128();
undefined FUN_00622130();
undefined FUN_00622138();
undefined FUN_00622140();
undefined FUN_00622148();
undefined FUN_00622150();
undefined FUN_00622158();
undefined FUN_00622160();
undefined FUN_00622168();
undefined FUN_00622170();
undefined FUN_00622178();
undefined FUN_00622180();
undefined FUN_00622188();
undefined FUN_00622190();
undefined FUN_00622198();
undefined FUN_006221a0();
undefined FUN_006221a8();
undefined FUN_006221b0();
undefined FUN_006221b8();
undefined FUN_006221c0();
undefined FUN_006221c8();
undefined FUN_006221d0();
undefined FUN_006221d8();
undefined FUN_006221e0();
undefined FUN_006221e8();
undefined FUN_006221f0();
undefined FUN_006221f8();
undefined FUN_00622200();
undefined FUN_00622208();
undefined FUN_00622210();
undefined FUN_00622218();
undefined FUN_00622220();
undefined FUN_00622228();
undefined FUN_00622230();
undefined FUN_00622238();
undefined FUN_00622240();
undefined FUN_00622248();
undefined FUN_00622250();
undefined FUN_00622258();
undefined FUN_00622260();
undefined FUN_00622268();
undefined FUN_00622270();
undefined FUN_00622278();
undefined FUN_00622280();
undefined FUN_00622288();
undefined FUN_00622290();
undefined FUN_00622298();
undefined FUN_006222a0();
undefined FUN_006222a8();
undefined FUN_006222b0();
undefined FUN_006222b8();
undefined FUN_006222c0();



#include "du.h"

long null_ARRAY_0063232_0_8_;
long null_ARRAY_0063232_8_8_;
long null_ARRAY_0063243_0_8_;
long null_ARRAY_0063244_0_8_;
long null_ARRAY_0063244_16_8_;
long null_ARRAY_0063244_24_8_;
long null_ARRAY_0063244_8_8_;
long null_ARRAY_0063260_0_8_;
long null_ARRAY_0063260_16_8_;
long null_ARRAY_0063260_24_8_;
long null_ARRAY_0063260_32_8_;
long null_ARRAY_0063260_40_8_;
long null_ARRAY_0063260_48_8_;
long null_ARRAY_0063260_8_8_;
long null_ARRAY_0063264_0_4_;
long null_ARRAY_0063264_16_8_;
long null_ARRAY_0063264_4_4_;
long null_ARRAY_0063264_8_4_;
long local_10_1_7_;
long local_11_0_1_;
long local_5_0_4_;
long local_5_4_4_;
long local_6_4_4_;
long local_68_2_2_;
long local_6b_1_3_;
long local_6c_4_4_;
long local_6e_4_4_;
long local_6f_4_4_;
long local_70_0_2_;
long local_70_4_4_;
long local_71_0_1_;
long local_71_4_4_;
long local_72_4_4_;
long local_9_0_4_;
long local_a_0_1_;
long local_a_0_4_;
long DAT_00000010;
long DAT_004276a7;
long DAT_004276aa;
long DAT_004276ad;
long DAT_00427731;
long DAT_00427733;
long DAT_00427787;
long DAT_004293e5;
long DAT_0042981a;
long DAT_0042981c;
long DAT_0042a100;
long DAT_0042a1d8;
long DAT_0042a1dc;
long DAT_0042a1e0;
long DAT_0042a1e3;
long DAT_0042a1e5;
long DAT_0042a1e9;
long DAT_0042a1ed;
long DAT_0042a96b;
long DAT_0042b411;
long DAT_0042b412;
long DAT_0042b790;
long DAT_0042bdf9;
long DAT_0042bdfa;
long DAT_0042bfe5;
long DAT_0042bffe;
long DAT_0042c01f;
long DAT_0042c06d;
long DAT_0042c078;
long DAT_0042c08e;
long DAT_0042cd07;
long DAT_0042cd20;
long DAT_0042cdca;
long DAT_0042ce9a;
long DAT_00631d50;
long DAT_00631d60;
long DAT_00631d70;
long DAT_006322c0;
long DAT_006322d8;
long DAT_00632330;
long DAT_00632334;
long DAT_00632338;
long DAT_0063233c;
long DAT_00632380;
long DAT_00632388;
long DAT_00632390;
long DAT_006323a0;
long DAT_006323a8;
long DAT_006323c0;
long DAT_006323c8;
long DAT_00632420;
long DAT_00632428;
long DAT_00632460;
long DAT_00632468;
long DAT_00632470;
long DAT_00632478;
long DAT_00632480;
long DAT_00632488;
long DAT_0063248c;
long DAT_0063248d;
long DAT_00632490;
long DAT_00632498;
long DAT_006324a0;
long DAT_006324a1;
long DAT_006324a2;
long DAT_006324a3;
long DAT_006324a4;
long DAT_006324a5;
long DAT_006324a6;
long DAT_006324a8;
long DAT_006324b0;
long DAT_006324b8;
long DAT_006324c0;
long DAT_006324c8;
long DAT_006324d0;
long DAT_00632638;
long DAT_00632678;
long DAT_00632680;
long DAT_00632688;
long DAT_006326d8;
long DAT_006326e0;
long DAT_00632f08;
long DAT_00632f10;
long DAT_00632f20;
long fde_0042e1d8;
long null_ARRAY_004293f4;
long null_ARRAY_00429400;
long null_ARRAY_00429420;
long null_ARRAY_00429440;
long null_ARRAY_00429480;
long null_ARRAY_0042a090;
long null_ARRAY_0042a110;
long null_ARRAY_0042a128;
long null_ARRAY_0042a160;
long null_ARRAY_0042ca00;
long null_ARRAY_0042ce60;
long null_ARRAY_0042d2c0;
long null_ARRAY_0042d4f0;
long null_ARRAY_00632320;
long null_ARRAY_006323e0;
long null_ARRAY_00632430;
long null_ARRAY_00632440;
long null_ARRAY_00632500;
long null_ARRAY_00632600;
long null_ARRAY_00632640;
long null_ARRAY_00632700;
long PTR_DAT_006322c8;
long PTR_FUN_006322d0;
long PTR_null_ARRAY_00632318;
long PTR_null_ARRAY_00632340;
long stack0x00000008;
void
FUN_00402bd1 (uint uParm1)
{
  int iVar1;
  long lVar2;
  undefined8 uVar3;
  undefined **ppuVar4;
  undefined *puVar5;
  undefined *local_88;
  char *local_80;
  undefined8 local_78;
  char *local_70;
  char *local_68;
  char *local_60;
  char *local_58;
  char *local_50;
  char *local_48;
  char *local_40;
  char *local_38;
  char *local_30;
  undefined8 local_28;
  undefined8 local_20;

  ppuVar4 = &local_88;
  if (uParm1 != 0)
    {
      func_0x004024a0 (DAT_006323a0,
		       "Try \'%s --help\' for more information.\n",
		       DAT_006324d0);
      goto LAB_00402e1b;
    }
  func_0x004021c0
    ("Usage: %s [OPTION]... [FILE]...\n  or:  %s [OPTION]... --files0-from=F\n",
     DAT_006324d0, DAT_006324d0);
  uVar3 = DAT_00632380;
  func_0x004024b0
    ("Summarize disk usage of the set of FILEs, recursively for directories.\n",
     DAT_00632380);
  func_0x004024b0
    ("\nMandatory arguments to long options are mandatory for short options too.\n",
     uVar3);
  func_0x004024b0
    ("  -0, --null            end each output line with NUL, not newline\n  -a, --all             write counts for all files, not just directories\n      --apparent-size   print apparent sizes, rather than disk usage; although\n                          the apparent size is usually smaller, it may be\n                          larger due to holes in (\'sparse\') files, internal\n                          fragmentation, indirect blocks, and the like\n",
     uVar3);
  func_0x004024b0
    ("  -B, --block-size=SIZE  scale sizes by SIZE before printing them; e.g.,\n                           \'-BM\' prints sizes in units of 1,048,576 bytes;\n                           see SIZE format below\n  -b, --bytes           equivalent to \'--apparent-size --block-size=1\'\n  -c, --total           produce a grand total\n  -D, --dereference-args  dereference only symlinks that are listed on the\n                          command line\n  -d, --max-depth=N     print the total for a directory (or file, with --all)\n                          only if it is N or fewer levels below the command\n                          line argument;  --max-depth=0 is the same as\n                          --summarize\n",
     uVar3);
  func_0x004024b0
    ("      --files0-from=F   summarize disk usage of the\n                          NUL-terminated file names specified in file F;\n                          if F is -, then read names from standard input\n  -H                    equivalent to --dereference-args (-D)\n  -h, --human-readable  print sizes in human readable format (e.g., 1K 234M 2G)\n      --inodes          list inode usage information instead of block usage\n",
     uVar3);
  func_0x004024b0
    ("  -k                    like --block-size=1K\n  -L, --dereference     dereference all symbolic links\n  -l, --count-links     count sizes many times if hard linked\n  -m                    like --block-size=1M\n",
     uVar3);
  func_0x004024b0
    ("  -P, --no-dereference  don\'t follow any symbolic links (this is the default)\n  -S, --separate-dirs   for directories do not include size of subdirectories\n      --si              like -h, but use powers of 1000 not 1024\n  -s, --summarize       display only a total for each argument\n",
     uVar3);
  func_0x004024b0
    ("  -t, --threshold=SIZE  exclude entries smaller than SIZE if positive,\n                          or entries greater than SIZE if negative\n      --time            show time of the last modification of any file in the\n                          directory, or any of its subdirectories\n      --time=WORD       show time as WORD instead of modification time:\n                          atime, access, use, ctime or status\n      --time-style=STYLE  show times using STYLE, which can be:\n                            full-iso, long-iso, iso, or +FORMAT;\n                            FORMAT is interpreted like in \'date\'\n",
     uVar3);
  func_0x004024b0
    ("  -X, --exclude-from=FILE  exclude files that match any pattern in FILE\n      --exclude=PATTERN    exclude files that match PATTERN\n  -x, --one-file-system    skip directories on different file systems\n",
     uVar3);
  func_0x004024b0 ("      --help     display this help and exit\n", uVar3);
  func_0x004024b0 ("      --version  output version information and exit\n",
		   uVar3);
  func_0x004021c0
    ("\nDisplay values are in units of the first available SIZE from --block-size,\nand the %s_BLOCK_SIZE, BLOCK_SIZE and BLOCKSIZE environment variables.\nOtherwise, units default to 1024 bytes (or 512 if POSIXLY_CORRECT is set).\n",
     &DAT_004276aa);
  func_0x004024b0
    ("\nThe SIZE argument is an integer and optional unit (example: 10K is 10*1024).\nUnits are K,M,G,T,P,E,Z,Y (powers of 1024) or KB,MB,... (powers of 1000).\n",
     uVar3);
  local_88 = &DAT_004276ad;
  local_80 = "test invocation";
  local_78 = 0x427710;
  local_70 = "Multi-call invocation";
  local_68 = "sha224sum";
  local_60 = "sha2 utilities";
  local_58 = "sha256sum";
  local_50 = "sha2 utilities";
  local_48 = "sha384sum";
  local_40 = "sha2 utilities";
  local_38 = "sha512sum";
  local_30 = "sha2 utilities";
  local_28 = 0;
  local_20 = 0;
  puVar5 = &DAT_004276ad;
  do
    {
      iVar1 = func_0x00402660 (&DAT_004276a7, puVar5);
      if (iVar1 == 0)
	break;
      ppuVar4 = ppuVar4 + 2;
      puVar5 = *ppuVar4;
    }
  while (puVar5 != (undefined *) 0x0);
  puVar5 = ppuVar4[1];
  if (puVar5 == (undefined *) 0x0)
    {
      func_0x004021c0 ("\n%s online help: <%s>\n", "GNU coreutils",
		       "https://www.gnu.org/software/coreutils/");
      lVar2 = func_0x004026c0 (5, 0);
      if (lVar2 == 0)
	goto LAB_00402e22;
      iVar1 = func_0x00402540 (lVar2, &DAT_00427731, 3);
      if (iVar1 == 0)
	{
	  func_0x004021c0 ("Full documentation at: <%s%s>\n",
			   "https://www.gnu.org/software/coreutils/",
			   &DAT_004276a7);
	  puVar5 = &DAT_004276a7;
	  uVar3 = 0x4276c9;
	  goto LAB_00402e09;
	}
      puVar5 = &DAT_004276a7;
    LAB_00402dc7:
      ;
      func_0x004021c0
	("Report %s translation bugs to <https://translationproject.org/team/>\n",
	 &DAT_004276a7);
    }
  else
    {
      func_0x004021c0 ("\n%s online help: <%s>\n", "GNU coreutils",
		       "https://www.gnu.org/software/coreutils/");
      lVar2 = func_0x004026c0 (5, 0);
      if ((lVar2 != 0)
	  && (iVar1 = func_0x00402540 (lVar2, &DAT_00427731, 3), iVar1 != 0))
	goto LAB_00402dc7;
    }
  func_0x004021c0 ("Full documentation at: <%s%s>\n",
		   "https://www.gnu.org/software/coreutils/", &DAT_004276a7);
  uVar3 = 0x42bfe4;
  if (puVar5 == &DAT_004276a7)
    {
      uVar3 = 0x4276c9;
    }
LAB_00402e09:
  ;
  do
    {
      func_0x004021c0
	("or available locally via: info \'(coreutils) %s%s\'\n", puVar5,
	 uVar3);
    LAB_00402e1b:
      ;
      func_0x00402780 ((ulong) uParm1);
    LAB_00402e22:
      ;
      func_0x004021c0 ("Full documentation at: <%s%s>\n",
		       "https://www.gnu.org/software/coreutils/",
		       &DAT_004276a7);
      puVar5 = &DAT_004276a7;
      uVar3 = 0x4276c9;
    }
  while (true);
}

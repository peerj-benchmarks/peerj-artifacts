typedef unsigned char   undefined;

typedef unsigned char    byte;
typedef unsigned char    dwfenc;
typedef unsigned int    dword;
typedef unsigned long    qword;
typedef unsigned char    undefined1;
typedef unsigned short    undefined2;
typedef unsigned int    undefined4;
typedef unsigned long    undefined5;
typedef unsigned long    undefined6;
typedef unsigned long    undefined7;
typedef unsigned long    undefined8;
typedef unsigned short    word;
#define unkbyte9   unsigned long long
#define unkbyte10   unsigned long long
#define unkbyte11   unsigned long long
#define unkbyte12   unsigned long long
#define unkbyte13   unsigned long long
#define unkbyte14   unsigned long long
#define unkbyte15   unsigned long long
#define unkbyte16   unsigned long long

#define unkuint9   unsigned long long
#define unkuint10   unsigned long long
#define unkuint11   unsigned long long
#define unkuint12   unsigned long long
#define unkuint13   unsigned long long
#define unkuint14   unsigned long long
#define unkuint15   unsigned long long
#define unkuint16   unsigned long long

#define unkint9   long long
#define unkint10   long long
#define unkint11   long long
#define unkint12   long long
#define unkint13   long long
#define unkint14   long long
#define unkint15   long long
#define unkint16   long long

#define unkfloat1   float
#define unkfloat2   float
#define unkfloat3   float
#define unkfloat5   double
#define unkfloat6   double
#define unkfloat7   double
#define unkfloat9   long double
#define unkfloat11   long double
#define unkfloat12   long double
#define unkfloat13   long double
#define unkfloat14   long double
#define unkfloat15   long double
#define unkfloat16   long double

#define BADSPACEBASE   void
#define code   void

#define ulong long
#define true 1
#define bool int
#define false 0
#define float10 int
#define ushort int
#define undefined7 long
#define undefined2 long
#define uint5 long
#define uint6 long

// Add definition for uint
typedef unsigned int    uint;

#define sbyte char

extern imperfection_wrapper(void);
typedef struct eh_frame_hdr eh_frame_hdr, *Peh_frame_hdr;

struct eh_frame_hdr {
    byte eh_frame_hdr_version; // Exception Handler Frame Header Version
    dwfenc eh_frame_pointer_encoding; // Exception Handler Frame Pointer Encoding
    dwfenc eh_frame_desc_entry_count_encoding; // Encoding of # of Exception Handler FDEs
    dwfenc eh_frame_table_encoding; // Exception Handler Table Encoding
};

typedef struct fde_table_entry fde_table_entry, *Pfde_table_entry;

struct fde_table_entry {
    dword initial_loc; // Initial Location
    dword data_loc; // Data location
};

typedef enum Elf64_DynTag {
    DT_AUDIT=1879047932,
    DT_AUXILIARY=2147483645,
    DT_BIND_NOW=24,
    DT_CHECKSUM=1879047672,
    DT_CONFIG=1879047930,
    DT_DEBUG=21,
    DT_DEPAUDIT=1879047931,
    DT_ENCODING=32,
    DT_FEATURE_1=1879047676,
    DT_FILTER=2147483647,
    DT_FINI=13,
    DT_FINI_ARRAY=26,
    DT_FINI_ARRAYSZ=28,
    DT_FLAGS=30,
    DT_FLAGS_1=1879048187,
    DT_GNU_CONFLICT=1879047928,
    DT_GNU_CONFLICTSZ=1879047670,
    DT_GNU_HASH=1879047925,
    DT_GNU_LIBLIST=1879047929,
    DT_GNU_LIBLISTSZ=1879047671,
    DT_GNU_PRELINKED=1879047669,
    DT_HASH=4,
    DT_INIT=12,
    DT_INIT_ARRAY=25,
    DT_INIT_ARRAYSZ=27,
    DT_JMPREL=23,
    DT_MOVEENT=1879047674,
    DT_MOVESZ=1879047675,
    DT_MOVETAB=1879047934,
    DT_NEEDED=1,
    DT_NULL=0,
    DT_PLTGOT=3,
    DT_PLTPAD=1879047933,
    DT_PLTPADSZ=1879047673,
    DT_PLTREL=20,
    DT_PLTRELSZ=2,
    DT_POSFLAG_1=1879047677,
    DT_PREINIT_ARRAYSZ=33,
    DT_REL=17,
    DT_RELA=7,
    DT_RELACOUNT=1879048185,
    DT_RELAENT=9,
    DT_RELASZ=8,
    DT_RELCOUNT=1879048186,
    DT_RELENT=19,
    DT_RELSZ=18,
    DT_RPATH=15,
    DT_RUNPATH=29,
    DT_SONAME=14,
    DT_STRSZ=10,
    DT_STRTAB=5,
    DT_SYMBOLIC=16,
    DT_SYMENT=11,
    DT_SYMINENT=1879047679,
    DT_SYMINFO=1879047935,
    DT_SYMINSZ=1879047678,
    DT_SYMTAB=6,
    DT_TEXTREL=22,
    DT_TLSDESC_GOT=1879047927,
    DT_TLSDESC_PLT=1879047926,
    DT_VERDEF=1879048188,
    DT_VERDEFNUM=1879048189,
    DT_VERNEED=1879048190,
    DT_VERNEEDNUM=1879048191,
    DT_VERSYM=1879048176
} Elf64_DynTag;

typedef enum Elf_ProgramHeaderType {
    PT_DYNAMIC=2,
    PT_GNU_EH_FRAME=1685382480,
    PT_GNU_RELRO=1685382482,
    PT_GNU_STACK=1685382481,
    PT_INTERP=3,
    PT_LOAD=1,
    PT_NOTE=4,
    PT_NULL=0,
    PT_PHDR=6,
    PT_SHLIB=5,
    PT_TLS=7
} Elf_ProgramHeaderType;

typedef struct Elf64_Dyn Elf64_Dyn, *PElf64_Dyn;

struct Elf64_Dyn {
    enum Elf64_DynTag d_tag;
    qword d_val;
};

typedef struct Elf64_Rela Elf64_Rela, *PElf64_Rela;

struct Elf64_Rela {
    qword r_offset; // location to apply the relocation action
    qword r_info; // the symbol table index and the type of relocation
    qword r_addend; // a constant addend used to compute the relocatable field value
};

typedef struct Elf64_Shdr Elf64_Shdr, *PElf64_Shdr;

typedef enum Elf_SectionHeaderType {
    SHT_CHECKSUM=1879048184,
    SHT_DYNAMIC=6,
    SHT_DYNSYM=11,
    SHT_FINI_ARRAY=15,
    SHT_GNU_ATTRIBUTES=1879048181,
    SHT_GNU_HASH=1879048182,
    SHT_GNU_LIBLIST=1879048183,
    SHT_GNU_verdef=1879048189,
    SHT_GNU_verneed=1879048190,
    SHT_GNU_versym=1879048191,
    SHT_GROUP=17,
    SHT_HASH=5,
    SHT_INIT_ARRAY=14,
    SHT_NOBITS=8,
    SHT_NOTE=7,
    SHT_NULL=0,
    SHT_PREINIT_ARRAY=16,
    SHT_PROGBITS=1,
    SHT_REL=9,
    SHT_RELA=4,
    SHT_SHLIB=10,
    SHT_STRTAB=3,
    SHT_SUNW_COMDAT=1879048187,
    SHT_SUNW_move=1879048186,
    SHT_SUNW_syminfo=1879048188,
    SHT_SYMTAB=2,
    SHT_SYMTAB_SHNDX=18
} Elf_SectionHeaderType;

struct Elf64_Shdr {
    dword sh_name;
    enum Elf_SectionHeaderType sh_type;
    qword sh_flags;
    qword sh_addr;
    qword sh_offset;
    qword sh_size;
    dword sh_link;
    dword sh_info;
    qword sh_addralign;
    qword sh_entsize;
};

typedef struct Elf64_Phdr Elf64_Phdr, *PElf64_Phdr;

struct Elf64_Phdr {
    enum Elf_ProgramHeaderType p_type;
    dword p_flags;
    qword p_offset;
    qword p_vaddr;
    qword p_paddr;
    qword p_filesz;
    qword p_memsz;
    qword p_align;
};

typedef struct Elf64_Sym Elf64_Sym, *PElf64_Sym;

struct Elf64_Sym {
    dword st_name;
    byte st_info;
    byte st_other;
    word st_shndx;
    qword st_value;
    qword st_size;
};

typedef struct Elf64_Ehdr Elf64_Ehdr, *PElf64_Ehdr;

struct Elf64_Ehdr {
    byte e_ident_magic_num;
    char e_ident_magic_str[3];
    byte e_ident_class;
    byte e_ident_data;
    byte e_ident_version;
    byte e_ident_pad[9];
    word e_type;
    word e_machine;
    dword e_version;
    qword e_entry;
    qword e_phoff;
    qword e_shoff;
    dword e_flags;
    word e_ehsize;
    word e_phentsize;
    word e_phnum;
    word e_shentsize;
    word e_shnum;
    word e_shstrndx;
};




undefined8 _DT_INIT(void);
void FUN_00402180(void);
void FUN_00402400(void);
void FUN_004026f0(void);
void FUN_004028f0(void);
void entry(void);
void FUN_00402950(void);
void FUN_004029d0(void);
void FUN_00402a50(void);
void FUN_00402a8e(long *plParm1,undefined8 uParm2);
void FUN_00402bd1(uint uParm1);
ulong FUN_00402eb9(uint uParm1,undefined8 *puParm2);
void FUN_00404159(void);
long FUN_0040416c(undefined8 uParm1,long *plParm2,long lParm3,long lParm4);
void FUN_0040425c(undefined8 uParm1,undefined8 uParm2,long lParm3);
void FUN_004042b9(long *plParm1,long lParm2,long lParm3);
long FUN_00404384(undefined8 uParm1,undefined8 uParm2,undefined8 uParm3,undefined8 uParm4,undefined8 uParm5,code *pcParm6);
void FUN_004043ef(undefined8 uParm1);
void FUN_00404413(undefined8 uParm1);
long FUN_0040444b(long *plParm1,int *piParm2);
long FUN_004044d5(long *plParm1);
void FUN_004044ed(long *plParm1);
void FUN_0040450a(void);
ulong FUN_004045b0(ulong *puParm1,ulong uParm2);
ulong FUN_004045bf(ulong *puParm1,ulong *puParm2);
ulong FUN_004045c9(ulong uParm1,ulong uParm2);
void FUN_004045d8(long lParm1);
long FUN_004045ef(undefined8 *puParm1,long lParm2);
long FUN_0040468d(long lParm1,long lParm2);
long * FUN_004046ec(void);
void FUN_0040474f(undefined8 *puParm1);
undefined8 FUN_00404777(undefined8 uParm1,undefined8 uParm2,undefined8 uParm3);
ulong FUN_004047c2(undefined8 uParm1,undefined8 uParm2,undefined8 uParm3);
void FUN_00404811(void);
void FUN_0040481c(void);
uint FUN_0040482a(void);
uint FUN_0040483d(void);
ulong FUN_00404850(undefined8 uParm1,long lParm2,ulong uParm3);
void FUN_00404917(void);
ulong FUN_00404925(byte *pbParm1,ulong uParm2);
void FUN_00404b10(long lParm1,undefined8 uParm2);
undefined8 FUN_00404b3c(undefined *puParm1,ulong uParm2);
void FUN_00404baa(void);
void FUN_00404bbd(undefined8 uParm1,char *pcParm2,ulong uParm3);
ulong FUN_00404c38(long **pplParm1,long lParm2);
void FUN_00404db4(long **pplParm1,long lParm2,uint uParm3);
ulong FUN_0040508b(code *pcParm1,undefined8 uParm2,undefined8 uParm3,uint uParm4,byte bParm5);
ulong FUN_004051fe(undefined8 uParm1,undefined8 uParm2,char *pcParm3,ulong uParm4,char cParm5);
void FUN_00405298(undefined8 uParm1,byte *pbParm2,long lParm3);
char * FUN_004052cd(long param_1,char *param_2,undefined8 *param_3,undefined param_4,undefined8 param_5,undefined8 param_6,uint param_7);
void FUN_00406bc1(void);
undefined8 FUN_00406be3(int iParm1);
long FUN_00406c27(undefined8 uParm1,undefined8 uParm2,undefined8 uParm3);
ulong FUN_00406d90(ulong uParm1,ulong uParm2);
undefined FUN_00406da3(long lParm1,long lParm2);;
ulong FUN_00406daa(long lParm1,undefined8 uParm2,uint uParm3);
ulong FUN_00406e2e(ulong uParm1,long lParm2);
long FUN_00406f4a(long *plParm1,undefined8 uParm2);
undefined8 FUN_00406f6c(long lParm1,long **pplParm2,char cParm3);
long FUN_00407090(long lParm1,long lParm2,long **pplParm3,char cParm4);
long FUN_00407194(long lParm1,long lParm2);
ulong FUN_004071f0(byte *pbParm1,ulong uParm2);
long * FUN_0040722a(undefined8 uParm1,undefined1 *puParm2,code *pcParm3,code *pcParm4,long lParm5);
void FUN_00407315(long **pplParm1);
ulong FUN_004073ba(long *plParm1,undefined8 uParm2);
undefined8 FUN_004074f7(long lParm1,long lParm2,long *plParm3);
undefined8 FUN_00407724(undefined8 uParm1,undefined8 uParm2);
long FUN_00407753(long lParm1,undefined8 uParm2);
void FUN_0040792f(void);
char * FUN_00407a04(ulong uParm1,long lParm2,ulong uParm3,ulong uParm4,ulong uParm5);
undefined8 FUN_00408126(char *pcParm1,uint *puParm2,long *plParm3);
ulong FUN_0040827a(ulong *puParm1,ulong uParm2);
ulong FUN_00408289(ulong *puParm1,ulong *puParm2);
long * FUN_00408293(long lParm1);
long FUN_004082ff(undefined8 *puParm1,long lParm2);
char * FUN_00408388(long lParm1,long lParm2);
ulong FUN_00408413(byte *pbParm1,byte *pbParm2,undefined8 uParm3,undefined8 uParm4,undefined8 uParm5,undefined8 uParm6);
void FUN_00408b33(long lParm1);
int * FUN_00408bc8(int *piParm1,int iParm2);
undefined * FUN_00408bf8(char *pcParm1,int iParm2);
ulong FUN_00408cb0(undefined *param_1,ulong param_2,long param_3,ulong param_4,uint param_5,ulong param_6,long param_7,char *param_8,char *param_9);
undefined1 * FUN_00409a67(int iParm1,undefined8 uParm2,undefined8 uParm3,uint *puParm4);
ulong FUN_00409c09(undefined1 *puParm1,byte bParm2,undefined8 uParm3);
void FUN_00409c3d(uint uParm1,undefined8 uParm2,undefined8 uParm3);
void FUN_00409c6b(ulong uParm1,undefined8 uParm2);
void FUN_00409c83(undefined8 uParm1,undefined8 uParm2,char cParm3);
void FUN_00409d0c(undefined8 uParm1,char cParm2);
void FUN_00409d25(undefined8 uParm1);
void FUN_00409d38(uint uParm1,undefined8 uParm2,undefined8 uParm3);
void FUN_00409dc7(void);
void FUN_00409dda(undefined8 uParm1,undefined8 uParm2);
void FUN_00409def(undefined8 uParm1);
long FUN_00409e05(long lParm1,long lParm2);
void FUN_00409e36(undefined8 uParm1,long lParm2,undefined8 uParm3,undefined8 uParm4,undefined8 *puParm5,undefined8 uParm6);
void FUN_0040a073(void);
void FUN_0040a0c5(void);
void FUN_0040a14a(long lParm1);
long FUN_0040a164(long lParm1,long lParm2);
void FUN_0040a197(long lParm1,ulong *puParm2);
void FUN_0040a1e8(undefined8 uParm1);
void FUN_0040a203(ulong uParm1,ulong uParm2);
void FUN_0040a232(undefined8 uParm1,undefined8 uParm2);
void FUN_0040a25b(undefined8 uParm1);
void FUN_0040a272(void);
void FUN_0040a29a(undefined8 uParm1,uint uParm2);
ulong FUN_0040a2db(long lParm1,long lParm2);
ulong FUN_0040a306(long *plParm1,int iParm2,int iParm3);
ulong FUN_0040a391(char *pcParm1,char **ppcParm2,uint uParm3,long *plParm4,long lParm5);
ulong FUN_0040a73f(ulong *puParm1,int iParm2,int iParm3);
ulong FUN_0040a79d(byte *pbParm1,byte **ppbParm2,uint uParm3,ulong *puParm4,long lParm5);
ulong FUN_0040aae6(uint uParm1);
ulong FUN_0040ab69(ulong *puParm1,int iParm2,int iParm3);
ulong FUN_0040abc7(byte *pbParm1,byte **ppbParm2,uint uParm3,ulong *puParm4,long lParm5);
undefined8 FUN_0040af10(ulong uParm1,ulong uParm2,ulong uParm3);
void FUN_0040af58(void);
void FUN_0040af85(undefined8 uParm1);
void FUN_0040afc5(uint uParm1,uint uParm2,undefined8 uParm3,undefined8 uParm4);
void FUN_0040b01c(undefined4 uParm1,undefined4 uParm2,undefined4 uParm3,undefined4 uParm4,undefined4 uParm5,undefined4 uParm6,undefined4 uParm7,undefined4 uParm8,ulong param_1,ulong param_2,undefined8 param_3,undefined8 param_4,undefined8 param_5,undefined8 param_6);
undefined8 FUN_0040b0ef(undefined8 uParm1);
ulong FUN_0040b172(long lParm1);
int * FUN_0040b202(int *piParm1);
char * FUN_0040b2d5(char *pcParm1);
undefined8 FUN_0040b3a8(int iParm1,long lParm2,ulong uParm3,ulong uParm4,undefined8 uParm5,uint uParm6);
ulong FUN_0040b9cc(uint *puParm1,uint *puParm2,uint *puParm3,byte bParm4,uint uParm5);
undefined8 FUN_0040c47b(int iParm1,long lParm2,ulong uParm3,ulong uParm4,undefined8 uParm5,uint uParm6);
ulong FUN_0040ca2f(byte *pbParm1,byte *pbParm2,byte *pbParm3,byte bParm4,uint uParm5);
ulong FUN_0040d468(undefined8 uParm1,long lParm2,ulong uParm3);
undefined8 FUN_0040d74d(void);
void FUN_0040d760(void);
ulong FUN_0040d76e(long *plParm1,long *plParm2);
ulong FUN_0040d78d(long lParm1,ulong uParm2);
ulong FUN_0040d79d(ulong *puParm1,ulong uParm2);
ulong FUN_0040d7ac(ulong *puParm1,ulong *puParm2);
ulong FUN_0040d7b6(long *plParm1,long *plParm2);
undefined8 FUN_0040d7dc(long lParm1,long lParm2);
ulong FUN_0040d849(long lParm1,long lParm2,char cParm3);
long FUN_0040d9a1(long lParm1,long lParm2,ulong uParm3);
long FUN_0040da81(long lParm1,undefined8 uParm2,long lParm3);
ulong FUN_0040db13(long lParm1);
void FUN_0040db72(long lParm1,undefined8 uParm2);
void FUN_0040dbd1(long lParm1);
void FUN_0040dc08(long lParm1);
void FUN_0040dc33(undefined8 uParm1);
undefined8 FUN_0040dc5a(long lParm1);
undefined8 FUN_0040dd65(void);
undefined8 FUN_0040ddbf(long lParm1,undefined8 *puParm2);
void FUN_0040de69(long lParm1,uint uParm2,char cParm3);
ulong FUN_0040deba(long lParm1);
ulong FUN_0040df0a(long lParm1,long lParm2,ulong uParm3,long lParm4);
void FUN_0040e08b(long lParm1,long lParm2);
long FUN_0040e11d(long *plParm1,int iParm2);
long * FUN_0040e9e6(long *plParm1,uint uParm2,long lParm3);
undefined8 FUN_0040ed65(long *plParm1);
long FUN_0040ee9f(long *plParm1);
undefined8 FUN_0040f50b(undefined8 uParm1,long lParm2,uint uParm3);
ulong FUN_0040f535(long *plParm1,ulong *puParm2,int iParm3,long lParm4);
void FUN_0040f65d(long lParm1,int *piParm2);
ulong FUN_0040f71c(int param_1,undefined8 *param_2,char *param_3,long *param_4,int *param_5,int param_6,int *param_7,int param_8,undefined8 param_9);
ulong FUN_0040fbd7(uint param_1,undefined8 *param_2,char *param_3,long param_4,undefined8 param_5,ulong param_6,uint *param_7,int param_8);
void FUN_00410053(void);
void FUN_004100a9(void);
ulong FUN_004100bf(long lParm1,long lParm2);
void FUN_00410128(long lParm1);
ulong FUN_00410142(uint *puParm1,byte *pbParm2,long lParm3);
ulong * FUN_004101a7(ulong *puParm1,char cParm2,ulong uParm3);
void FUN_0041029f(long lParm1);
void FUN_00410324(undefined8 *puParm1);
char * FUN_0041035b(void);
void FUN_00410a1b(long lParm1,long lParm2);
void FUN_00410a49(long lParm1,long lParm2);
ulong FUN_00410a7a(long lParm1,long lParm2);
void FUN_00410aa9(ulong *puParm1);
void FUN_00410abb(long lParm1,long lParm2);
void FUN_00410ad4(long lParm1,long lParm2);
ulong FUN_00410aed(long lParm1,long lParm2);
ulong FUN_00410b39(long lParm1,long lParm2);
void FUN_00410b53(long *plParm1);
ulong FUN_00410b98(long lParm1,long lParm2);
long FUN_00410be8(long lParm1,long lParm2);
void FUN_00410c54(long lParm1,long lParm2);
undefined8 FUN_00410c94(long lParm1,long lParm2);
undefined8 FUN_00410d1d(undefined8 uParm1,long lParm2);
undefined8 FUN_00410d7b(char *pcParm1,long lParm2,ulong uParm3);
undefined8 FUN_00410e86(long lParm1,long lParm2);
ulong FUN_00410e9c(long lParm1,uint uParm2,long lParm3,long lParm4,long lParm5);
undefined8 FUN_00411063(long lParm1,long lParm2,undefined8 uParm3,undefined8 uParm4,long lParm5);
long FUN_004110c2(long lParm1,long lParm2);
undefined8 FUN_00411165(long lParm1,long lParm2,undefined8 uParm3,undefined8 uParm4,undefined8 uParm5,undefined8 uParm6);
undefined8 FUN_0041125a(undefined4 *param_1,long *param_2,char *param_3,int param_4,undefined8 param_5,undefined8 param_6,char param_7);
undefined8 FUN_004114cf(long *plParm1,code *pcParm2,undefined8 uParm3);
undefined8 FUN_00411527(long *plParm1,code *pcParm2,undefined8 uParm3);
undefined8 FUN_0041157c(long lParm1,ulong uParm2);
undefined8 FUN_00411623(long *plParm1,undefined8 uParm2);
void FUN_00411683(undefined8 uParm1);
long FUN_0041169b(long lParm1,long *plParm2,long *plParm3,undefined8 *puParm4);
long ** FUN_00411769(long **pplParm1,undefined8 uParm2);
void FUN_004117fc(void);
long FUN_00411811(undefined4 *puParm1,long *plParm2,long lParm3);
ulong FUN_0041192e(undefined8 uParm1,long lParm2);
undefined8 FUN_0041199d(undefined8 *puParm1,undefined8 uParm2);
ulong FUN_004119ed(long *plParm1,long lParm2);
ulong FUN_00411ae6(long *plParm1,undefined8 uParm2,long lParm3,long lParm4,uint uParm5);
undefined8 FUN_00411bd3(long *plParm1,long lParm2);
undefined8 FUN_00411c13(long *plParm1,ulong *puParm2,ulong uParm3);
undefined8 FUN_00411cf0(long lParm1,long lParm2,undefined8 uParm3);
void FUN_00411e2a(long *plParm1);
void FUN_00411e9b(long *plParm1);
undefined8 FUN_0041202e(long *plParm1);
undefined8 FUN_004125ae(long lParm1,int iParm2);
undefined8 FUN_00412682(long lParm1,long lParm2);
undefined8 FUN_004126fb(long *plParm1,long lParm2);
undefined8 FUN_004128a0(long *plParm1,long lParm2);
undefined8 FUN_00412922(long *plParm1,long lParm2,long lParm3);
undefined8 FUN_00412ab2(long *plParm1,long lParm2,long lParm3);
ulong FUN_00412c49(long lParm1,long lParm2,ulong uParm3);
ulong FUN_00412d16(long lParm1,undefined8 *puParm2,long lParm3);
long FUN_00412e45(long lParm1,long lParm2,undefined8 uParm3);
undefined8 FUN_00412f03(long *plParm1,long *plParm2,ulong uParm3);
void FUN_0041355e(undefined8 uParm1,long lParm2);
long FUN_0041356f(undefined8 uParm1,byte *pbParm2,undefined8 uParm3);
void FUN_00413615(undefined8 *puParm1);
void FUN_00413634(undefined8 *puParm1);
undefined8 FUN_00413661(undefined8 uParm1,long lParm2);
long FUN_00413678(long *plParm1,undefined8 uParm2,undefined8 uParm3);
undefined8 FUN_0041385b(long *plParm1,long lParm2);
void FUN_004138e5(long *plParm1,long lParm2,undefined8 uParm3);
ulong FUN_00413988(long *plParm1,long lParm2,long lParm3,long lParm4,uint uParm5);
undefined8 FUN_00413c4b(undefined8 *puParm1,long *plParm2,long lParm3,byte bParm4);
void FUN_00413e70(long lParm1);
ulong * FUN_00413eca(undefined4 *puParm1,long *plParm2,long lParm3,uint uParm4);
void FUN_0041417f(long *plParm1);
void FUN_004141d3(long lParm1);
void FUN_004141fd(long *plParm1);
ulong FUN_00414366(long *plParm1,undefined8 *puParm2,long lParm3,uint uParm4);
ulong FUN_00414485(long *plParm1,long lParm2,undefined8 uParm3,undefined8 uParm4);
void FUN_00414648(long lParm1);
undefined8 FUN_004146fb(long *plParm1);
undefined8 FUN_0041475b(long lParm1,long lParm2);
undefined8 FUN_00414949(long *plParm1,undefined8 *puParm2,long lParm3,undefined8 uParm4,undefined8 *puParm5);
undefined8 FUN_00414a0a(long lParm1,undefined8 uParm2,long lParm3,long *plParm4,char *pcParm5,uint uParm6);
long FUN_00415062(long lParm1,undefined8 uParm2,undefined8 uParm3,char *pcParm4,byte bParm5,int *piParm6);
void FUN_00415243(long **pplParm1,long lParm2,long lParm3);
undefined8 FUN_00415600(byte **ppbParm1,byte *pbParm2,ulong uParm3);
ulong FUN_00415c95(long *plParm1,long lParm2,long lParm3,long lParm4);
ulong FUN_00415f99(long **pplParm1,long lParm2,long lParm3,long *plParm4,char cParm5);
ulong * FUN_00416739(undefined4 *puParm1,long *plParm2,long lParm3);
ulong FUN_00416906(long lParm1,undefined8 uParm2,long lParm3,undefined8 uParm4,uint uParm5);
ulong FUN_00416b1d(undefined8 uParm1,long lParm2,long lParm3,long lParm4);
ulong FUN_00416bc7(long param_1,long *param_2,long param_3,long param_4,undefined8 param_5,long param_6,uint param_7);
undefined8 FUN_004173d0(long lParm1,long *plParm2,undefined8 *puParm3,undefined8 uParm4,long lParm5);
ulong FUN_004175ac(long lParm1,long lParm2);
long FUN_00417d12(int *piParm1,long lParm2,long lParm3);
ulong FUN_00417eae(long lParm1,ulong *puParm2,ulong uParm3,long lParm4);
ulong FUN_0041861d(long lParm1,long *plParm2);
ulong FUN_004188d1(long *plParm1,long lParm2);
ulong FUN_00419525(long **param_1,long param_2,ulong param_3,long param_4,long param_5,ulong param_6,long *param_7,undefined8 *param_8,uint param_9);
long FUN_0041ab75(long lParm1,long *plParm2,undefined8 *puParm3,ulong uParm4,long lParm5,int *piParm6);
long FUN_0041bfb8(undefined8 uParm1,undefined8 *puParm2,long lParm3,undefined8 uParm4,long lParm5,int *piParm6);
long FUN_0041c0f8(undefined8 uParm1,long *plParm2,long lParm3,ulong uParm4,long lParm5,int *piParm6);
ulong FUN_0041c24b(long **pplParm1,undefined8 uParm2,ulong uParm3,long *plParm4);
undefined8 FUN_0041ceb0(long *plParm1);
ulong FUN_0041cf2c(undefined8 *puParm1,undefined8 uParm2,ulong uParm3);
ulong FUN_0041d02f(long lParm1,undefined8 uParm2,undefined8 uParm3,undefined8 *puParm4,ulong uParm5);
ulong FUN_0041d0ce(long lParm1,long lParm2);
ulong FUN_0041d11a(long lParm1,ulong uParm2,long *plParm3);
long FUN_0041d22f(long lParm1,long lParm2,long lParm3,ulong uParm4);
char * FUN_0041d5ee(char *pcParm1,char *pcParm2);
void FUN_0041d960(long lParm1);
undefined8 * FUN_0041d98c(long lParm1);
undefined8 FUN_0041da2f(long *plParm1,char *pcParm2);
void FUN_0041db60(long *plParm1);
long FUN_0041db7f(long lParm1);
ulong FUN_0041dc29(long lParm1);
long FUN_0041dc8a(long lParm1,undefined8 uParm2,long lParm3);
long FUN_0041dd18(long lParm1,uint *puParm2);
void FUN_0041de12(long lParm1);
void FUN_0041de31(void);
ulong FUN_0041ded9(char *pcParm1);
ulong FUN_0041df32(undefined8 uParm1,undefined8 uParm2,undefined8 uParm3);
ulong FUN_0041dfff(undefined8 uParm1);
void FUN_0041e060(long lParm1);
undefined8 FUN_0041e070(long *plParm1,long *plParm2);
void FUN_0041e104(undefined8 param_1,byte param_2,ulong param_3);
undefined8 FUN_0041e14e(void);
ulong FUN_0041e156(ulong uParm1);
void FUN_0041e1e4(undefined4 *puParm1,undefined4 uParm2);
ulong FUN_0041e205(long lParm1);
ulong FUN_0041e20a(long lParm1,uint uParm2);
ulong FUN_0041e23f(long lParm1);
char * FUN_0041e272(void);
void FUN_0041e5ad(void);
ulong FUN_0041e5f6(uint uParm1);
ulong FUN_0041e628(uint param_1,undefined8 param_2,undefined8 param_3,undefined8 param_4);
undefined8 FUN_0041e797(ulong uParm1);
undefined8 FUN_0041e7e6(ulong uParm1,long lParm2,long lParm3,ulong uParm4);
void FUN_0041e8b2(undefined8 uParm1,undefined8 uParm2,undefined8 uParm3);
void FUN_0041e8c8(code *pcParm1,long *plParm2,undefined8 uParm3);
long FUN_0041e970(ulong param_1,long param_2,int param_3,int param_4,int param_5,uint param_6,int param_7,int param_8,int param_9,int param_10);
long FUN_0041ea99(void);
long FUN_0041eb36(undefined8 *puParm1,code *pcParm2,long *plParm3);
void FUN_0041f131(undefined8 uParm1);
ulong FUN_0041f14e(uint param_1,undefined8 param_2,uint param_3,uint param_4);
ulong FUN_0041f23e(char *pcParm1,char *pcParm2,ulong uParm3);
ulong FUN_0041f35b(char *pcParm1,long lParm2);
long FUN_0041f3a2(ulong uParm1,uint *puParm2,ulong uParm3,uint *puParm4,long *plParm5);
ulong FUN_0041f4c6(void);
ulong FUN_0041f4f8(void);
int * FUN_0041f705(ulong uParm1,long lParm2,ulong uParm3,uint *puParm4,ulong *puParm5);
char * FUN_0041fce8(int iParm1,long lParm2,long lParm3,long lParm4,uint uParm5);
void FUN_00420300(uint param_1,undefined8 param_2);
ulong FUN_004204c6(void);
void FUN_004206d9(undefined8 uParm1,uint uParm2);
undefined8 *FUN_0042087a(undefined8 *puParm1,undefined8 *puParm2,undefined8 *puParm3,undefined8 uParm4);
ulong FUN_00426417(ulong uParm1,char cParm2);
undefined8 * FUN_00426477(ulong uParm1);
void FUN_004264ee(ulong uParm1);
double FUN_0042656f(int *piParm1);
void FUN_004265b7(int *param_1);
void FUN_00426638(undefined8 uParm1);
undefined8 FUN_00426655(uint *puParm1,ulong *puParm2);
undefined8 FUN_00426a7a(byte *pbParm1,ulong *puParm2,byte **ppbParm3);
ulong FUN_004275df(void);
ulong FUN_00427607(void);
void FUN_00427640(void);
undefined8 _DT_FINI(void);
undefined FUN_00633000();
undefined FUN_00633008();
undefined FUN_00633010();
undefined FUN_00633018();
undefined FUN_00633020();
undefined FUN_00633028();
undefined FUN_00633030();
undefined FUN_00633038();
undefined FUN_00633040();
undefined FUN_00633048();
undefined FUN_00633050();
undefined FUN_00633058();
undefined FUN_00633060();
undefined FUN_00633068();
undefined FUN_00633070();
undefined FUN_00633078();
undefined FUN_00633080();
undefined FUN_00633088();
undefined FUN_00633090();
undefined FUN_00633098();
undefined FUN_006330a0();
undefined FUN_006330a8();
undefined FUN_006330b0();
undefined FUN_006330b8();
undefined FUN_006330c0();
undefined FUN_006330c8();
undefined FUN_006330d0();
undefined FUN_006330d8();
undefined FUN_006330e0();
undefined FUN_006330e8();
undefined FUN_006330f0();
undefined FUN_006330f8();
undefined FUN_00633100();
undefined FUN_00633108();
undefined FUN_00633110();
undefined FUN_00633118();
undefined FUN_00633120();
undefined FUN_00633128();
undefined FUN_00633130();
undefined FUN_00633138();
undefined FUN_00633140();
undefined FUN_00633148();
undefined FUN_00633150();
undefined FUN_00633158();
undefined FUN_00633160();
undefined FUN_00633168();
undefined FUN_00633170();
undefined FUN_00633178();
undefined FUN_00633180();
undefined FUN_00633188();
undefined FUN_00633190();
undefined FUN_00633198();
undefined FUN_006331a0();
undefined FUN_006331a8();
undefined FUN_006331b0();
undefined FUN_006331b8();
undefined FUN_006331c0();
undefined FUN_006331c8();
undefined FUN_006331d0();
undefined FUN_006331d8();
undefined FUN_006331e0();
undefined FUN_006331e8();
undefined FUN_006331f0();
undefined FUN_006331f8();
undefined FUN_00633200();
undefined FUN_00633208();
undefined FUN_00633210();
undefined FUN_00633218();
undefined FUN_00633220();
undefined FUN_00633228();
undefined FUN_00633230();
undefined FUN_00633238();
undefined FUN_00633240();
undefined FUN_00633248();
undefined FUN_00633250();
undefined FUN_00633258();
undefined FUN_00633260();
undefined FUN_00633268();
undefined FUN_00633270();
undefined FUN_00633278();
undefined FUN_00633280();
undefined FUN_00633288();
undefined FUN_00633290();
undefined FUN_00633298();
undefined FUN_006332a0();
undefined FUN_006332a8();
undefined FUN_006332b0();
undefined FUN_006332b8();
undefined FUN_006332c0();
undefined FUN_006332c8();
undefined FUN_006332d0();
undefined FUN_006332d8();
undefined FUN_006332e0();
undefined FUN_006332e8();
undefined FUN_006332f0();
undefined FUN_006332f8();
undefined FUN_00633300();
undefined FUN_00633308();
undefined FUN_00633310();
undefined FUN_00633318();
undefined FUN_00633320();
undefined FUN_00633328();
undefined FUN_00633330();
undefined FUN_00633338();
undefined FUN_00633340();
undefined FUN_00633348();
undefined FUN_00633350();
undefined FUN_00633358();
undefined FUN_00633360();
undefined FUN_00633368();
undefined FUN_00633370();
undefined FUN_00633378();
undefined FUN_00633380();
undefined FUN_00633388();
undefined FUN_00633390();
undefined FUN_00633398();
undefined FUN_006333a0();


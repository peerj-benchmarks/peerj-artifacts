
#include "basename.h"

long null_ARRAY_0060f92_0_8_;
long null_ARRAY_0060f92_8_8_;
long null_ARRAY_0060fb0_0_8_;
long null_ARRAY_0060fb0_16_8_;
long null_ARRAY_0060fb0_24_8_;
long null_ARRAY_0060fb0_32_8_;
long null_ARRAY_0060fb0_40_8_;
long null_ARRAY_0060fb0_48_8_;
long null_ARRAY_0060fb0_8_8_;
long null_ARRAY_0060fb4_0_4_;
long null_ARRAY_0060fb4_16_8_;
long null_ARRAY_0060fb4_4_4_;
long null_ARRAY_0060fb4_8_4_;
long local_68_2_2_;
long local_6b_1_3_;
long local_6c_4_4_;
long local_6e_4_4_;
long local_6f_4_4_;
long local_70_0_2_;
long local_70_4_4_;
long local_71_0_1_;
long local_71_4_4_;
long local_72_4_4_;
long DAT_00000010;
long DAT_0040cb09;
long DAT_0040cb8d;
long DAT_0040d118;
long DAT_0040d11c;
long DAT_0040d120;
long DAT_0040d123;
long DAT_0040d125;
long DAT_0040d129;
long DAT_0040d12d;
long DAT_0040d8ab;
long DAT_0040dc55;
long DAT_0040dd59;
long DAT_0040dd5f;
long DAT_0040dd71;
long DAT_0040dd72;
long DAT_0040dd90;
long DAT_0040dd94;
long DAT_0040de1e;
long DAT_0060f4f0;
long DAT_0060f500;
long DAT_0060f510;
long DAT_0060f8c8;
long DAT_0060f930;
long DAT_0060f934;
long DAT_0060f938;
long DAT_0060f93c;
long DAT_0060f940;
long DAT_0060f950;
long DAT_0060f960;
long DAT_0060f968;
long DAT_0060f980;
long DAT_0060f988;
long DAT_0060f9d0;
long DAT_0060f9d8;
long DAT_0060f9e0;
long DAT_0060fb78;
long DAT_0060fb80;
long DAT_0060fb88;
long DAT_0060fb98;
long fde_0040e990;
long null_ARRAY_0040d000;
long null_ARRAY_0040e240;
long null_ARRAY_0040e480;
long null_ARRAY_0060f920;
long null_ARRAY_0060f9a0;
long null_ARRAY_0060fa00;
long null_ARRAY_0060fb00;
long null_ARRAY_0060fb40;
long PTR_DAT_0060f8c0;
long PTR_null_ARRAY_0060f918;
void
FUN_00401ad9 (uint uParm1)
{
  int iVar1;
  long lVar2;
  undefined8 uVar3;
  undefined **ppuVar4;
  char *pcVar5;
  undefined *puVar6;
  undefined *local_88;
  char *local_80;
  undefined8 local_78;
  char *local_70;
  char *local_68;
  char *local_60;
  char *local_58;
  char *local_50;
  char *local_48;
  char *local_40;
  char *local_38;
  char *local_30;
  undefined8 local_28;
  undefined8 local_20;

  ppuVar4 = &local_88;
  if (uParm1 != 0)
    {
      func_0x00401620 (DAT_0060f960,
		       "Try \'%s --help\' for more information.\n",
		       DAT_0060f9e0);
      goto LAB_00401cd3;
    }
  func_0x004014a0 ("Usage: %s NAME [SUFFIX]\n  or:  %s OPTION... NAME...\n",
		   DAT_0060f9e0, DAT_0060f9e0);
  uVar3 = DAT_0060f940;
  func_0x00401630
    ("Print NAME with any leading directory components removed.\nIf specified, also remove a trailing SUFFIX.\n",
     DAT_0060f940);
  func_0x00401630
    ("\nMandatory arguments to long options are mandatory for short options too.\n",
     uVar3);
  func_0x00401630
    ("  -a, --multiple       support multiple arguments and treat each as a NAME\n  -s, --suffix=SUFFIX  remove a trailing SUFFIX; implies -a\n  -z, --zero           end each output line with NUL, not newline\n",
     uVar3);
  func_0x00401630 ("      --help     display this help and exit\n", uVar3);
  func_0x00401630 ("      --version  output version information and exit\n",
		   uVar3);
  func_0x004014a0
    ("\nExamples:\n  %s /usr/bin/sort          -> \"sort\"\n  %s include/stdio.h .h     -> \"stdio\"\n  %s -s .h include/stdio.h  -> \"stdio\"\n  %s -a any/str1 any/str2   -> \"str1\" followed by \"str2\"\n",
     DAT_0060f9e0, DAT_0060f9e0, DAT_0060f9e0, DAT_0060f9e0);
  local_88 = &DAT_0040cb09;
  local_80 = "test invocation";
  local_78 = 0x40cb6c;
  local_70 = "Multi-call invocation";
  local_68 = "sha224sum";
  local_60 = "sha2 utilities";
  local_58 = "sha256sum";
  local_50 = "sha2 utilities";
  local_48 = "sha384sum";
  local_40 = "sha2 utilities";
  local_38 = "sha512sum";
  local_30 = "sha2 utilities";
  local_28 = 0;
  local_20 = 0;
  puVar6 = &DAT_0040cb09;
  do
    {
      iVar1 = func_0x00401700 ("basename", puVar6);
      if (iVar1 == 0)
	break;
      ppuVar4 = ppuVar4 + 2;
      puVar6 = *ppuVar4;
    }
  while (puVar6 != (undefined *) 0x0);
  pcVar5 = ppuVar4[1];
  if (pcVar5 == (char *) 0x0)
    {
      func_0x004014a0 ("\n%s online help: <%s>\n", "GNU coreutils",
		       "https://www.gnu.org/software/coreutils/");
      lVar2 = func_0x00401760 (5, 0);
      if (lVar2 == 0)
	goto LAB_00401cda;
      iVar1 = func_0x00401680 (lVar2, &DAT_0040cb8d, 3);
      if (iVar1 == 0)
	{
	  func_0x004014a0 ("Full documentation at: <%s%s>\n",
			   "https://www.gnu.org/software/coreutils/",
			   "basename");
	  pcVar5 = "basename";
	  uVar3 = 0x40cb25;
	  goto LAB_00401cc1;
	}
      pcVar5 = "basename";
    LAB_00401c7f:
      ;
      func_0x004014a0
	("Report %s translation bugs to <https://translationproject.org/team/>\n",
	 "basename");
    }
  else
    {
      func_0x004014a0 ("\n%s online help: <%s>\n", "GNU coreutils",
		       "https://www.gnu.org/software/coreutils/");
      lVar2 = func_0x00401760 (5, 0);
      if ((lVar2 != 0)
	  && (iVar1 = func_0x00401680 (lVar2, &DAT_0040cb8d, 3), iVar1 != 0))
	goto LAB_00401c7f;
    }
  func_0x004014a0 ("Full documentation at: <%s%s>\n",
		   "https://www.gnu.org/software/coreutils/", "basename");
  uVar3 = 0x40dd8f;
  if (pcVar5 == "basename")
    {
      uVar3 = 0x40cb25;
    }
LAB_00401cc1:
  ;
  do
    {
      func_0x004014a0
	("or available locally via: info \'(coreutils) %s%s\'\n", pcVar5,
	 uVar3);
    LAB_00401cd3:
      ;
      func_0x004017b0 ((ulong) uParm1);
    LAB_00401cda:
      ;
      func_0x004014a0 ("Full documentation at: <%s%s>\n",
		       "https://www.gnu.org/software/coreutils/", "basename");
      pcVar5 = "basename";
      uVar3 = 0x40cb25;
    }
  while (true);
}

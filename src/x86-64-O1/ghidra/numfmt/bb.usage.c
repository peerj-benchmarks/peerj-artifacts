
#include "numfmt.h"

long null_ARRAY_006174e_0_8_;
long null_ARRAY_006174e_8_8_;
long null_ARRAY_0061774_0_8_;
long null_ARRAY_0061774_16_8_;
long null_ARRAY_0061774_24_8_;
long null_ARRAY_0061774_32_8_;
long null_ARRAY_0061774_40_8_;
long null_ARRAY_0061774_48_8_;
long null_ARRAY_0061774_8_8_;
long null_ARRAY_0061778_0_4_;
long null_ARRAY_0061778_16_8_;
long null_ARRAY_0061778_4_4_;
long null_ARRAY_0061778_8_4_;
long local_68_2_2_;
long local_6b_1_3_;
long local_6c_4_4_;
long local_6e_4_4_;
long local_6f_4_4_;
long local_70_0_2_;
long local_70_4_4_;
long local_71_0_1_;
long local_71_4_4_;
long local_72_4_4_;
long DAT_00000010;
long DAT_00410b40;
long DAT_00410c11;
long DAT_00410c28;
long DAT_00410c77;
long DAT_00410cfb;
long DAT_00410d0a;
long DAT_00410d0e;
long DAT_00410d11;
long DAT_00410d13;
long DAT_00410d1e;
long DAT_00410ea7;
long DAT_00412e9a;
long DAT_00412e9c;
long DAT_00412ef8;
long DAT_00412efc;
long DAT_00412f00;
long DAT_00412f03;
long DAT_00412f05;
long DAT_00412f09;
long DAT_00412f0d;
long DAT_004136ab;
long DAT_00414100;
long DAT_00414103;
long DAT_00414209;
long DAT_0041420f;
long DAT_00414221;
long DAT_00414222;
long DAT_00414240;
long DAT_0041424a;
long DAT_004147c0;
long DAT_00414847;
long DAT_00617000;
long DAT_00617010;
long DAT_00617020;
long DAT_00617440;
long DAT_00617444;
long DAT_00617448;
long DAT_0061744c;
long DAT_00617450;
long DAT_00617458;
long DAT_00617460;
long DAT_00617468;
long DAT_00617480;
long DAT_006174f0;
long DAT_006174f4;
long DAT_006174f8;
long DAT_006174fc;
long DAT_00617500;
long DAT_00617508;
long DAT_00617510;
long DAT_00617520;
long DAT_00617528;
long DAT_00617540;
long DAT_00617548;
long DAT_00617590;
long DAT_00617594;
long DAT_00617598;
long DAT_006175a0;
long DAT_006175a8;
long DAT_006175b0;
long DAT_006175b8;
long DAT_006175c0;
long DAT_006175c8;
long DAT_006175d0;
long DAT_006175d8;
long DAT_006175e0;
long DAT_006175e8;
long DAT_006175f0;
long DAT_006175f8;
long DAT_00617600;
long DAT_00617604;
long DAT_00617608;
long DAT_00617610;
long DAT_00617618;
long DAT_00617620;
long DAT_00617628;
long DAT_00617630;
long DAT_006177b8;
long DAT_006177c0;
long DAT_006177c8;
long DAT_006177d0;
long DAT_006177d8;
long DAT_006177e8;
long fde_004159a0;
long FLOAT_UNKNOWN;
long null_ARRAY_004128c0;
long null_ARRAY_00412b20;
long null_ARRAY_00412b40;
long null_ARRAY_00412b70;
long null_ARRAY_00412ba0;
long null_ARRAY_00412bd0;
long null_ARRAY_00412be0;
long null_ARRAY_00412c10;
long null_ARRAY_00412c40;
long null_ARRAY_00414660;
long null_ARRAY_00414880;
long null_ARRAY_00414980;
long null_ARRAY_004153d0;
long null_ARRAY_006174e0;
long null_ARRAY_00617560;
long null_ARRAY_00617640;
long null_ARRAY_00617740;
long null_ARRAY_00617780;
long PTR_DAT_00617470;
long PTR_FUN_00617478;
long PTR_null_ARRAY_006174d8;
void
FUN_004034d3 (uint uParm1)
{
  int iVar1;
  long lVar2;
  undefined8 uVar3;
  undefined **ppuVar4;
  char *pcVar5;
  undefined *puVar6;
  undefined *local_88;
  char *local_80;
  undefined8 local_78;
  char *local_70;
  char *local_68;
  char *local_60;
  char *local_58;
  char *local_50;
  char *local_48;
  char *local_40;
  char *local_38;
  char *local_30;
  undefined8 local_28;
  undefined8 local_20;

  ppuVar4 = &local_88;
  if (uParm1 != 0)
    {
      func_0x00401a20 (DAT_00617520,
		       "Try \'%s --help\' for more information.\n",
		       DAT_00617630);
      goto LAB_00403810;
    }
  func_0x00401820 ("Usage: %s [OPTION]... [NUMBER]...\n", DAT_00617630);
  uVar3 = DAT_00617500;
  func_0x00401a30
    ("Reformat NUMBER(s), or the numbers from standard input if none are specified.\n",
     DAT_00617500);
  func_0x00401a30
    ("\nMandatory arguments to long options are mandatory for short options too.\n",
     uVar3);
  func_0x00401a30
    ("      --debug          print warnings about invalid input\n", uVar3);
  func_0x00401a30
    ("  -d, --delimiter=X    use X instead of whitespace for field delimiter\n",
     uVar3);
  func_0x00401a30
    ("      --field=FIELDS   replace the numbers in these input fields (default=1)\n                         see FIELDS below\n",
     uVar3);
  func_0x00401a30
    ("      --format=FORMAT  use printf style floating-point FORMAT;\n                         see FORMAT below for details\n",
     uVar3);
  func_0x00401a30
    ("      --from=UNIT      auto-scale input numbers to UNITs; default is \'none\';\n                         see UNIT below\n",
     uVar3);
  func_0x00401a30
    ("      --from-unit=N    specify the input unit size (instead of the default 1)\n",
     uVar3);
  func_0x00401a30
    ("      --grouping       use locale-defined grouping of digits, e.g. 1,000,000\n                         (which means it has no effect in the C/POSIX locale)\n",
     uVar3);
  func_0x00401a30
    ("      --header[=N]     print (without converting) the first N header lines;\n                         N defaults to 1 if not specified\n",
     uVar3);
  func_0x00401a30
    ("      --invalid=MODE   failure mode for invalid numbers: MODE can be:\n                         abort (default), fail, warn, ignore\n",
     uVar3);
  func_0x00401a30
    ("      --padding=N      pad the output to N characters; positive N will\n                         right-align; negative N will left-align;\n                         padding is ignored if the output is wider than N;\n                         the default is to automatically pad if a whitespace\n                         is found\n",
     uVar3);
  func_0x00401a30
    ("      --round=METHOD   use METHOD for rounding when scaling; METHOD can be:\n                         up, down, from-zero (default), towards-zero, nearest\n",
     uVar3);
  func_0x00401a30
    ("      --suffix=SUFFIX  add SUFFIX to output numbers, and accept optional\n                         SUFFIX in input numbers\n",
     uVar3);
  func_0x00401a30
    ("      --to=UNIT        auto-scale output numbers to UNITs; see UNIT below\n",
     uVar3);
  func_0x00401a30
    ("      --to-unit=N      the output unit size (instead of the default 1)\n",
     uVar3);
  func_0x00401a30
    ("  -z, --zero-terminated    line delimiter is NUL, not newline\n",
     uVar3);
  func_0x00401a30 ("      --help     display this help and exit\n", uVar3);
  func_0x00401a30 ("      --version  output version information and exit\n",
		   uVar3);
  func_0x00401a30 ("\nUNIT options:\n", uVar3);
  func_0x00401a30
    ("  none       no auto-scaling is done; suffixes will trigger an error\n",
     uVar3);
  func_0x00401a30
    ("  auto       accept optional single/two letter suffix:\n               1K = 1000,\n               1Ki = 1024,\n               1M = 1000000,\n               1Mi = 1048576,\n",
     uVar3);
  func_0x00401a30
    ("  si         accept optional single letter suffix:\n               1K = 1000,\n               1M = 1000000,\n               ...\n",
     uVar3);
  func_0x00401a30
    ("  iec        accept optional single letter suffix:\n               1K = 1024,\n               1M = 1048576,\n               ...\n",
     uVar3);
  func_0x00401a30
    ("  iec-i      accept optional two-letter suffix:\n               1Ki = 1024,\n               1Mi = 1048576,\n               ...\n",
     uVar3);
  func_0x00401a30
    ("\nFIELDS supports cut(1) style field ranges:\n  N    N\'th field, counted from 1\n  N-   from N\'th field, to end of line\n  N-M  from N\'th to M\'th field (inclusive)\n  -M   from first to M\'th field (inclusive)\n  -    all fields\nMultiple fields/ranges can be separated with commas\n",
     uVar3);
  func_0x00401a30
    ("\nFORMAT must be suitable for printing one floating-point argument \'%f\'.\nOptional quote (%\'f) will enable --grouping (if supported by current locale).\nOptional width value (%10f) will pad output. Optional zero (%010f) width\nwill zero pad the number. Optional negative values (%-10f) will left align.\nOptional precision (%.1f) will override the input determined precision.\n",
     uVar3);
  func_0x00401820
    ("\nExit status is 0 if all input numbers were successfully converted.\nBy default, %s will stop at the first conversion error with exit status 2.\nWith --invalid=\'fail\' a warning is printed for each conversion error\nand the exit status is 2.  With --invalid=\'warn\' each conversion error is\ndiagnosed, but the exit status is 0.  With --invalid=\'ignore\' conversion\nerrors are not diagnosed and the exit status is 0.\n",
     DAT_00617630);
  func_0x00401820
    ("\nExamples:\n  $ %s --to=si 1000\n            -> \"1.0K\"\n  $ %s --to=iec 2048\n           -> \"2.0K\"\n  $ %s --to=iec-i 4096\n           -> \"4.0Ki\"\n  $ echo 1K | %s --from=si\n           -> \"1000\"\n  $ echo 1K | %s --from=iec\n           -> \"1024\"\n  $ df -B1 | %s --header --field 2-4 --to=si\n  $ ls -l  | %s --header --field 5 --to=iec\n  $ ls -lh | %s --header --field 5 --from=iec --padding=10\n  $ ls -lh | %s --header --field 5 --from=iec --format %%10f\n",
     DAT_00617630, DAT_00617630, DAT_00617630, DAT_00617630, DAT_00617630,
     DAT_00617630, DAT_00617630, DAT_00617630, DAT_00617630);
  local_88 = &DAT_00410c77;
  local_80 = "test invocation";
  local_78 = 0x410cda;
  local_70 = "Multi-call invocation";
  local_68 = "sha224sum";
  local_60 = "sha2 utilities";
  local_58 = "sha256sum";
  local_50 = "sha2 utilities";
  local_48 = "sha384sum";
  local_40 = "sha2 utilities";
  local_38 = "sha512sum";
  local_30 = "sha2 utilities";
  local_28 = 0;
  local_20 = 0;
  puVar6 = &DAT_00410c77;
  do
    {
      iVar1 = func_0x00401b30 ("numfmt", puVar6);
      if (iVar1 == 0)
	break;
      ppuVar4 = ppuVar4 + 2;
      puVar6 = *ppuVar4;
    }
  while (puVar6 != (undefined *) 0x0);
  pcVar5 = ppuVar4[1];
  if (pcVar5 == (char *) 0x0)
    {
      func_0x00401820 ("\n%s online help: <%s>\n", "GNU coreutils",
		       "https://www.gnu.org/software/coreutils/");
      lVar2 = func_0x00401b90 (5, 0);
      if (lVar2 == 0)
	goto LAB_00403817;
      iVar1 = func_0x00401a80 (lVar2, &DAT_00410cfb, 3);
      if (iVar1 == 0)
	{
	  func_0x00401820 ("Full documentation at: <%s%s>\n",
			   "https://www.gnu.org/software/coreutils/",
			   "numfmt");
	  pcVar5 = "numfmt";
	  uVar3 = 0x410c93;
	  goto LAB_004037fe;
	}
      pcVar5 = "numfmt";
    LAB_004037bc:
      ;
      func_0x00401820
	("Report %s translation bugs to <https://translationproject.org/team/>\n",
	 "numfmt");
    }
  else
    {
      func_0x00401820 ("\n%s online help: <%s>\n", "GNU coreutils",
		       "https://www.gnu.org/software/coreutils/");
      lVar2 = func_0x00401b90 (5, 0);
      if ((lVar2 != 0)
	  && (iVar1 = func_0x00401a80 (lVar2, &DAT_00410cfb, 3), iVar1 != 0))
	goto LAB_004037bc;
    }
  func_0x00401820 ("Full documentation at: <%s%s>\n",
		   "https://www.gnu.org/software/coreutils/", "numfmt");
  uVar3 = 0x41423f;
  if (pcVar5 == "numfmt")
    {
      uVar3 = 0x410c93;
    }
LAB_004037fe:
  ;
  do
    {
      func_0x00401820
	("or available locally via: info \'(coreutils) %s%s\'\n", pcVar5,
	 uVar3);
    LAB_00403810:
      ;
      func_0x00401be0 ((ulong) uParm1);
    LAB_00403817:
      ;
      func_0x00401820 ("Full documentation at: <%s%s>\n",
		       "https://www.gnu.org/software/coreutils/", "numfmt");
      pcVar5 = "numfmt";
      uVar3 = 0x410c93;
    }
  while (true);
}

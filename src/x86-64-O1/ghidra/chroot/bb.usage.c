
#include "chroot.h"

long null_ARRAY_00613ae_0_8_;
long null_ARRAY_00613ae_8_8_;
long null_ARRAY_00613cc_0_8_;
long null_ARRAY_00613cc_16_8_;
long null_ARRAY_00613cc_24_8_;
long null_ARRAY_00613cc_32_8_;
long null_ARRAY_00613cc_40_8_;
long null_ARRAY_00613cc_48_8_;
long null_ARRAY_00613cc_8_8_;
long null_ARRAY_00613d0_0_4_;
long null_ARRAY_00613d0_16_8_;
long null_ARRAY_00613d0_4_4_;
long null_ARRAY_00613d0_8_4_;
long local_68_2_2_;
long local_6b_1_3_;
long local_6c_4_4_;
long local_6e_4_4_;
long local_6f_4_4_;
long local_70_0_2_;
long local_70_4_4_;
long local_71_0_1_;
long local_71_4_4_;
long local_72_4_4_;
long DAT_00000010;
long DAT_0040ff00;
long DAT_0040ff30;
long DAT_0040ffb4;
long DAT_0040ffcf;
long DAT_00410588;
long DAT_0041058e;
long DAT_00410590;
long DAT_00410594;
long DAT_00410598;
long DAT_0041059b;
long DAT_0041059d;
long DAT_004105a1;
long DAT_004105a5;
long DAT_00410d53;
long DAT_00411466;
long DAT_00411477;
long DAT_00411478;
long DAT_00411571;
long DAT_00411577;
long DAT_00411589;
long DAT_0041158a;
long DAT_004115a8;
long DAT_004115ac;
long DAT_00411636;
long DAT_006135e8;
long DAT_006135f8;
long DAT_00613608;
long DAT_00613a88;
long DAT_00613af0;
long DAT_00613af4;
long DAT_00613af8;
long DAT_00613afc;
long DAT_00613b00;
long DAT_00613b10;
long DAT_00613b20;
long DAT_00613b28;
long DAT_00613b40;
long DAT_00613b48;
long DAT_00613b90;
long DAT_00613b98;
long DAT_00613ba0;
long DAT_00613d38;
long DAT_00613d40;
long DAT_00613d48;
long DAT_00613d50;
long DAT_00613d58;
long DAT_00613d68;
long fde_004122c8;
long null_ARRAY_00410440;
long null_ARRAY_00410520;
long null_ARRAY_00411a40;
long null_ARRAY_00411c70;
long null_ARRAY_00613ae0;
long null_ARRAY_00613b60;
long null_ARRAY_00613bc0;
long null_ARRAY_00613cc0;
long null_ARRAY_00613d00;
long PTR_DAT_00613a80;
long PTR_null_ARRAY_00613ad8;
void
FUN_00402522 (uint uParm1)
{
  int iVar1;
  undefined8 uVar2;
  long lVar3;
  undefined8 uVar4;
  undefined **ppuVar5;
  char *pcVar6;
  undefined *puVar7;
  undefined *local_88;
  char *local_80;
  undefined8 local_78;
  char *local_70;
  char *local_68;
  char *local_60;
  char *local_58;
  char *local_50;
  char *local_48;
  char *local_40;
  char *local_38;
  char *local_30;
  undefined8 local_28;
  undefined8 local_20;

  ppuVar5 = &local_88;
  if (uParm1 != 0)
    {
      func_0x00401da0 (DAT_00613b20,
		       "Try \'%s --help\' for more information.\n",
		       DAT_00613ba0);
      goto LAB_0040272b;
    }
  func_0x00401b80
    ("Usage: %s [OPTION] NEWROOT [COMMAND [ARG]...]\n  or:  %s OPTION\n",
     DAT_00613ba0, DAT_00613ba0);
  uVar4 = DAT_00613b00;
  func_0x00401db0 ("Run COMMAND with root directory set to NEWROOT.\n\n",
		   DAT_00613b00);
  func_0x00401db0
    ("  --groups=G_LIST        specify supplementary groups as g1,g2,..,gN\n",
     uVar4);
  func_0x00401db0
    ("  --userspec=USER:GROUP  specify user and group (ID or name) to use\n",
     uVar4);
  imperfection_wrapper ();	//  uVar2 = FUN_0040502f(4, &DAT_0041058e);
  func_0x00401b80
    ("  --skip-chdir           do not change working directory to %s\n",
     uVar2);
  func_0x00401db0 ("      --help     display this help and exit\n", uVar4);
  func_0x00401db0 ("      --version  output version information and exit\n",
		   uVar4);
  func_0x00401db0
    ("\nIf no command is given, run \'\"$SHELL\" -i\' (default: \'/bin/sh -i\').\n",
     uVar4);
  local_88 = &DAT_0040ff30;
  local_80 = "test invocation";
  local_78 = 0x40ff93;
  local_70 = "Multi-call invocation";
  local_68 = "sha224sum";
  local_60 = "sha2 utilities";
  local_58 = "sha256sum";
  local_50 = "sha2 utilities";
  local_48 = "sha384sum";
  local_40 = "sha2 utilities";
  local_38 = "sha512sum";
  local_30 = "sha2 utilities";
  local_28 = 0;
  local_20 = 0;
  puVar7 = &DAT_0040ff30;
  do
    {
      iVar1 = func_0x00401ef0 ("chroot", puVar7);
      if (iVar1 == 0)
	break;
      ppuVar5 = ppuVar5 + 2;
      puVar7 = *ppuVar5;
    }
  while (puVar7 != (undefined *) 0x0);
  pcVar6 = ppuVar5[1];
  if (pcVar6 == (char *) 0x0)
    {
      func_0x00401b80 ("\n%s online help: <%s>\n", "GNU coreutils",
		       "https://www.gnu.org/software/coreutils/");
      lVar3 = func_0x00401f70 (5, 0);
      if (lVar3 == 0)
	goto LAB_00402732;
      iVar1 = func_0x00401e40 (lVar3, &DAT_0040ffb4, 3);
      if (iVar1 == 0)
	{
	  func_0x00401b80 ("Full documentation at: <%s%s>\n",
			   "https://www.gnu.org/software/coreutils/",
			   "chroot");
	  pcVar6 = "chroot";
	  uVar4 = 0x40ff4c;
	  goto LAB_00402719;
	}
      pcVar6 = "chroot";
    LAB_004026d7:
      ;
      func_0x00401b80
	("Report %s translation bugs to <https://translationproject.org/team/>\n",
	 "chroot");
    }
  else
    {
      func_0x00401b80 ("\n%s online help: <%s>\n", "GNU coreutils",
		       "https://www.gnu.org/software/coreutils/");
      lVar3 = func_0x00401f70 (5, 0);
      if ((lVar3 != 0)
	  && (iVar1 = func_0x00401e40 (lVar3, &DAT_0040ffb4, 3), iVar1 != 0))
	goto LAB_004026d7;
    }
  func_0x00401b80 ("Full documentation at: <%s%s>\n",
		   "https://www.gnu.org/software/coreutils/", "chroot");
  uVar4 = 0x4115a7;
  if (pcVar6 == "chroot")
    {
      uVar4 = 0x40ff4c;
    }
LAB_00402719:
  ;
  do
    {
      func_0x00401b80
	("or available locally via: info \'(coreutils) %s%s\'\n", pcVar6,
	 uVar4);
    LAB_0040272b:
      ;
      func_0x00401fc0 ((ulong) uParm1);
    LAB_00402732:
      ;
      func_0x00401b80 ("Full documentation at: <%s%s>\n",
		       "https://www.gnu.org/software/coreutils/", "chroot");
      pcVar6 = "chroot";
      uVar4 = 0x40ff4c;
    }
  while (true);
}

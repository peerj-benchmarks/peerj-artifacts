
#include "touch.h"

long null_ARRAY_0061baa_0_8_;
long null_ARRAY_0061baa_8_8_;
long null_ARRAY_0061bbc_0_8_;
long null_ARRAY_0061bbc_16_8_;
long null_ARRAY_0061bbc_24_8_;
long null_ARRAY_0061bbc_8_8_;
long null_ARRAY_0061bd0_0_8_;
long null_ARRAY_0061bd0_16_8_;
long null_ARRAY_0061bd0_24_8_;
long null_ARRAY_0061bd0_32_8_;
long null_ARRAY_0061bd0_40_8_;
long null_ARRAY_0061bd0_48_8_;
long null_ARRAY_0061bd0_8_8_;
long null_ARRAY_0061bd4_0_4_;
long null_ARRAY_0061bd4_16_8_;
long null_ARRAY_0061bd4_4_4_;
long null_ARRAY_0061bd4_8_4_;
long local_49_0_1_;
long local_51_4_4_;
long local_68_2_2_;
long local_6b_1_3_;
long local_6c_4_4_;
long local_6e_4_4_;
long local_6f_4_4_;
long local_70_0_2_;
long local_70_4_4_;
long local_71_0_1_;
long local_71_4_4_;
long local_72_4_4_;
long _bVar3;
long DAT_00000010;
long DAT_0041555d;
long DAT_004155e1;
long DAT_004156e3;
long DAT_0041601a;
long DAT_0041601b;
long DAT_0041601c;
long DAT_00416032;
long DAT_00416036;
long DAT_0041605b;
long DAT_004160b0;
long DAT_0041619b;
long DAT_004161cd;
long DAT_004161d0;
long DAT_004161d2;
long DAT_004161d3;
long DAT_004161f9;
long DAT_004161fc;
long DAT_00416257;
long DAT_0041625a;
long DAT_0041634b;
long DAT_00416352;
long DAT_0041644e;
long DAT_004165c0;
long DAT_00418016;
long DAT_00418070;
long DAT_00418074;
long DAT_00418078;
long DAT_0041807b;
long DAT_0041807f;
long DAT_00418083;
long DAT_0041882b;
long DAT_00418bd5;
long DAT_00418cd9;
long DAT_00418cdf;
long DAT_00418cfd;
long DAT_00418d5a;
long DAT_0061b5e8;
long DAT_0061b5f8;
long DAT_0061b608;
long DAT_0061ba50;
long DAT_0061bab0;
long DAT_0061bab4;
long DAT_0061bab8;
long DAT_0061babc;
long DAT_0061bb00;
long DAT_0061bb10;
long DAT_0061bb20;
long DAT_0061bb28;
long DAT_0061bb40;
long DAT_0061bb48;
long DAT_0061bba0;
long DAT_0061bbe0;
long DAT_0061bbe1;
long DAT_0061bbe2;
long DAT_0061bbe3;
long DAT_0061bbe4;
long DAT_0061bbe8;
long DAT_0061bbf0;
long DAT_0061bbf8;
long DAT_0061bdb8;
long DAT_0061bdc0;
long DAT_0061bdc8;
long DAT_0061c608;
long DAT_0061c610;
long DAT_0061c620;
long fde_00419ee0;
long null_ARRAY_00415e50;
long null_ARRAY_00415e80;
long null_ARRAY_00415ec0;
long null_ARRAY_00417140;
long null_ARRAY_00417240;
long null_ARRAY_00417400;
long null_ARRAY_00417700;
long null_ARRAY_00417740;
long null_ARRAY_004178c0;
long null_ARRAY_00417980;
long null_ARRAY_00417b10;
long null_ARRAY_00417b40;
long null_ARRAY_00417bc0;
long null_ARRAY_00417cc0;
long null_ARRAY_00417d40;
long null_ARRAY_00417dc0;
long null_ARRAY_00417de0;
long null_ARRAY_00417e00;
long null_ARRAY_00417e80;
long null_ARRAY_00417f00;
long null_ARRAY_00418d20;
long null_ARRAY_00419180;
long null_ARRAY_00419830;
long null_ARRAY_0061baa0;
long null_ARRAY_0061bb60;
long null_ARRAY_0061bbc0;
long null_ARRAY_0061bc00;
long null_ARRAY_0061bd00;
long null_ARRAY_0061bd40;
long null_ARRAY_0061bd80;
long null_ARRAY_0061be00;
long PTR_DAT_0061ba40;
long PTR_FUN_0061ba48;
long PTR_null_ARRAY_0061ba98;
long PTR_null_ARRAY_0061bac0;
long stack0x00000008;
long stack0xffffffffffffffe0;
long UNK_00417181;
void
FUN_00401ff1 (uint uParm1)
{
  int iVar1;
  long lVar2;
  undefined8 uVar3;
  undefined **ppuVar4;
  char *pcVar5;
  undefined *puVar6;
  undefined *local_88;
  char *local_80;
  undefined8 local_78;
  char *local_70;
  char *local_68;
  char *local_60;
  char *local_58;
  char *local_50;
  char *local_48;
  char *local_40;
  char *local_38;
  char *local_30;
  undefined8 local_28;
  undefined8 local_20;

  ppuVar4 = &local_88;
  if (uParm1 != 0)
    {
      func_0x00401ae0 (DAT_0061bb20,
		       "Try \'%s --help\' for more information.\n",
		       DAT_0061bbf8);
      goto LAB_004021f0;
    }
  func_0x00401920 ("Usage: %s [OPTION]... FILE...\n", DAT_0061bbf8);
  uVar3 = DAT_0061bb00;
  func_0x00401af0
    ("Update the access and modification times of each FILE to the current time.\n\nA FILE argument that does not exist is created empty, unless -c or -h\nis supplied.\n\nA FILE argument string of - is handled specially and causes touch to\nchange the times of the file associated with standard output.\n",
     DAT_0061bb00);
  func_0x00401af0
    ("\nMandatory arguments to long options are mandatory for short options too.\n",
     uVar3);
  func_0x00401af0
    ("  -a                     change only the access time\n  -c, --no-create        do not create any files\n  -d, --date=STRING      parse STRING and use it instead of current time\n  -f                     (ignored)\n",
     uVar3);
  func_0x00401af0
    ("  -h, --no-dereference   affect each symbolic link instead of any referenced\n                         file (useful only on systems that can change the\n                         timestamps of a symlink)\n  -m                     change only the modification time\n",
     uVar3);
  func_0x00401af0
    ("  -r, --reference=FILE   use this file\'s times instead of current time\n  -t STAMP               use [[CC]YY]MMDDhhmm[.ss] instead of current time\n      --time=WORD        change the specified time:\n                           WORD is access, atime, or use: equivalent to -a\n                           WORD is modify or mtime: equivalent to -m\n",
     uVar3);
  func_0x00401af0 ("      --help     display this help and exit\n", uVar3);
  func_0x00401af0 ("      --version  output version information and exit\n",
		   uVar3);
  func_0x00401af0
    ("\nNote that the -d and -t options accept different time-date formats.\n",
     uVar3);
  local_88 = &DAT_0041555d;
  local_80 = "test invocation";
  local_78 = 0x4155c0;
  local_70 = "Multi-call invocation";
  local_68 = "sha224sum";
  local_60 = "sha2 utilities";
  local_58 = "sha256sum";
  local_50 = "sha2 utilities";
  local_48 = "sha384sum";
  local_40 = "sha2 utilities";
  local_38 = "sha512sum";
  local_30 = "sha2 utilities";
  local_28 = 0;
  local_20 = 0;
  puVar6 = &DAT_0041555d;
  do
    {
      iVar1 = func_0x00401c10 ("touch", puVar6);
      if (iVar1 == 0)
	break;
      ppuVar4 = ppuVar4 + 2;
      puVar6 = *ppuVar4;
    }
  while (puVar6 != (undefined *) 0x0);
  pcVar5 = ppuVar4[1];
  if (pcVar5 == (char *) 0x0)
    {
      func_0x00401920 ("\n%s online help: <%s>\n", "GNU coreutils",
		       "https://www.gnu.org/software/coreutils/");
      lVar2 = func_0x00401c80 (5, 0);
      if (lVar2 == 0)
	goto LAB_004021f7;
      iVar1 = func_0x00401b50 (lVar2, &DAT_004155e1, 3);
      if (iVar1 == 0)
	{
	  func_0x00401920 ("Full documentation at: <%s%s>\n",
			   "https://www.gnu.org/software/coreutils/",
			   "touch");
	  pcVar5 = "touch";
	  uVar3 = 0x415579;
	  goto LAB_004021de;
	}
      pcVar5 = "touch";
    LAB_0040219c:
      ;
      func_0x00401920
	("Report %s translation bugs to <https://translationproject.org/team/>\n",
	 "touch");
    }
  else
    {
      func_0x00401920 ("\n%s online help: <%s>\n", "GNU coreutils",
		       "https://www.gnu.org/software/coreutils/");
      lVar2 = func_0x00401c80 (5, 0);
      if ((lVar2 != 0)
	  && (iVar1 = func_0x00401b50 (lVar2, &DAT_004155e1, 3), iVar1 != 0))
	goto LAB_0040219c;
    }
  func_0x00401920 ("Full documentation at: <%s%s>\n",
		   "https://www.gnu.org/software/coreutils/", "touch");
  uVar3 = 0x418cfc;
  if (pcVar5 == "touch")
    {
      uVar3 = 0x415579;
    }
LAB_004021de:
  ;
  do
    {
      func_0x00401920
	("or available locally via: info \'(coreutils) %s%s\'\n", pcVar5,
	 uVar3);
    LAB_004021f0:
      ;
      func_0x00401d00 ((ulong) uParm1);
    LAB_004021f7:
      ;
      func_0x00401920 ("Full documentation at: <%s%s>\n",
		       "https://www.gnu.org/software/coreutils/", "touch");
      pcVar5 = "touch";
      uVar3 = 0x415579;
    }
  while (true);
}

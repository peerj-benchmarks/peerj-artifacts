
#include "csplit.h"

long null_ARRAY_00622be_0_8_;
long null_ARRAY_00622be_8_8_;
long null_ARRAY_00622f4_0_8_;
long null_ARRAY_00622f4_16_8_;
long null_ARRAY_00622f4_24_8_;
long null_ARRAY_00622f4_32_8_;
long null_ARRAY_00622f4_40_8_;
long null_ARRAY_00622f4_48_8_;
long null_ARRAY_00622f4_8_8_;
long null_ARRAY_00622f8_0_4_;
long null_ARRAY_00622f8_16_8_;
long null_ARRAY_00622f8_4_4_;
long null_ARRAY_00622f8_8_4_;
long local_10_1_7_;
long local_11_0_1_;
long local_68_2_2_;
long local_6b_1_3_;
long local_6c_4_4_;
long local_6e_4_4_;
long local_6f_4_4_;
long local_70_0_2_;
long local_70_4_4_;
long local_71_0_1_;
long local_71_4_4_;
long local_72_4_4_;
long local_9_0_4_;
long local_a_0_1_;
long local_a_0_4_;
long DAT_00000010;
long DAT_0041c380;
long DAT_0041c3ab;
long DAT_0041c3ee;
long DAT_0041c409;
long DAT_0041c48d;
long DAT_0041c48f;
long DAT_0041c491;
long DAT_0041d0f8;
long DAT_0041d0fc;
long DAT_0041d100;
long DAT_0041d103;
long DAT_0041d105;
long DAT_0041d109;
long DAT_0041d10d;
long DAT_0041d8ab;
long DAT_0041e2f8;
long DAT_0041e401;
long DAT_0041e407;
long DAT_0041e419;
long DAT_0041e41a;
long DAT_0041e438;
long DAT_0041f042;
long DAT_0041f049;
long DAT_0041f087;
long DAT_0041f0a0;
long DAT_0041f11a;
long DAT_0041f6b0;
long DAT_006226d8;
long DAT_006226e8;
long DAT_006226f8;
long DAT_00622b80;
long DAT_00622b90;
long DAT_00622bf0;
long DAT_00622bf4;
long DAT_00622bf8;
long DAT_00622bfc;
long DAT_00622c00;
long DAT_00622c10;
long DAT_00622c20;
long DAT_00622c28;
long DAT_00622c40;
long DAT_00622c48;
long DAT_00622cc0;
long DAT_00622cc8;
long DAT_00622cd0;
long DAT_00622d80;
long DAT_00622d88;
long DAT_00622d90;
long DAT_00622d91;
long DAT_00622d92;
long DAT_00622d93;
long DAT_00622d98;
long DAT_00622da0;
long DAT_00622da8;
long DAT_00622db0;
long DAT_00622db8;
long DAT_00622dc0;
long DAT_00622dc8;
long DAT_00622dd0;
long DAT_00622dd8;
long DAT_00622de0;
long DAT_00622de8;
long DAT_00622df0;
long DAT_00622df8;
long DAT_00622e00;
long DAT_00622e08;
long DAT_00622e10;
long DAT_00622e18;
long DAT_00622f78;
long DAT_00622fb8;
long DAT_00622fc0;
long DAT_00622fc8;
long DAT_00622fd8;
long DAT_00622fe0;
long fde_00420118;
long null_ARRAY_0041cee0;
long null_ARRAY_0041cf40;
long null_ARRAY_0041ed80;
long null_ARRAY_0041edc0;
long null_ARRAY_0041f540;
long null_ARRAY_0041f800;
long null_ARRAY_00622be0;
long null_ARRAY_00622c60;
long null_ARRAY_00622d00;
long null_ARRAY_00622e40;
long null_ARRAY_00622f40;
long null_ARRAY_00622f80;
long PTR_DAT_00622b88;
long PTR_null_ARRAY_00622bd8;
void
FUN_00402f80 (uint uParm1)
{
  int iVar1;
  long lVar2;
  undefined8 uVar3;
  undefined **ppuVar4;
  char *pcVar5;
  undefined *puVar6;
  undefined *local_88;
  char *local_80;
  undefined8 local_78;
  char *local_70;
  char *local_68;
  char *local_60;
  char *local_58;
  char *local_50;
  char *local_48;
  char *local_40;
  char *local_38;
  char *local_30;
  undefined8 local_28;
  undefined8 local_20;

  ppuVar4 = &local_88;
  if (uParm1 != 0)
    {
      func_0x00401e40 (DAT_00622c20,
		       "Try \'%s --help\' for more information.\n",
		       DAT_00622e18);
      goto LAB_0040318c;
    }
  func_0x00401c10 ("Usage: %s [OPTION]... FILE PATTERN...\n", DAT_00622e18);
  uVar3 = DAT_00622c00;
  func_0x00401e50
    ("Output pieces of FILE separated by PATTERN(s) to files \'xx00\', \'xx01\', ...,\nand output byte counts of each piece to standard output.\n",
     DAT_00622c00);
  func_0x00401e50 ("\nRead standard input if FILE is -\n", uVar3);
  func_0x00401e50
    ("\nMandatory arguments to long options are mandatory for short options too.\n",
     uVar3);
  func_0x00401e50
    ("  -b, --suffix-format=FORMAT  use sprintf FORMAT instead of %02d\n  -f, --prefix=PREFIX        use PREFIX instead of \'xx\'\n  -k, --keep-files           do not remove output files on errors\n",
     uVar3);
  func_0x00401e50
    ("      --suppress-matched     suppress the lines matching PATTERN\n",
     uVar3);
  func_0x00401e50
    ("  -n, --digits=DIGITS        use specified number of digits instead of 2\n  -s, --quiet, --silent      do not print counts of output file sizes\n  -z, --elide-empty-files    remove empty output files\n",
     uVar3);
  func_0x00401e50 ("      --help     display this help and exit\n", uVar3);
  func_0x00401e50 ("      --version  output version information and exit\n",
		   uVar3);
  func_0x00401e50
    ("\nEach PATTERN may be:\n  INTEGER            copy up to but not including specified line number\n  /REGEXP/[OFFSET]   copy up to but not including a matching line\n  %REGEXP%[OFFSET]   skip to, but not including a matching line\n  {INTEGER}          repeat the previous pattern specified number of times\n  {*}                repeat the previous pattern as many times as possible\n\nA line OFFSET is a required \'+\' or \'-\' followed by a positive integer.\n",
     uVar3);
  local_88 = &DAT_0041c409;
  local_80 = "test invocation";
  local_78 = 0x41c46c;
  local_70 = "Multi-call invocation";
  local_68 = "sha224sum";
  local_60 = "sha2 utilities";
  local_58 = "sha256sum";
  local_50 = "sha2 utilities";
  local_48 = "sha384sum";
  local_40 = "sha2 utilities";
  local_38 = "sha512sum";
  local_30 = "sha2 utilities";
  local_28 = 0;
  local_20 = 0;
  puVar6 = &DAT_0041c409;
  do
    {
      iVar1 = func_0x00401fd0 ("csplit", puVar6);
      if (iVar1 == 0)
	break;
      ppuVar4 = ppuVar4 + 2;
      puVar6 = *ppuVar4;
    }
  while (puVar6 != (undefined *) 0x0);
  pcVar5 = ppuVar4[1];
  if (pcVar5 == (char *) 0x0)
    {
      func_0x00401c10 ("\n%s online help: <%s>\n", "GNU coreutils",
		       "https://www.gnu.org/software/coreutils/");
      lVar2 = func_0x00402030 (5, 0);
      if (lVar2 == 0)
	goto LAB_00403193;
      iVar1 = func_0x00401ee0 (lVar2, &DAT_0041c48d, 3);
      if (iVar1 == 0)
	{
	  func_0x00401c10 ("Full documentation at: <%s%s>\n",
			   "https://www.gnu.org/software/coreutils/",
			   "csplit");
	  pcVar5 = "csplit";
	  uVar3 = 0x41c425;
	  goto LAB_0040317a;
	}
      pcVar5 = "csplit";
    LAB_00403138:
      ;
      func_0x00401c10
	("Report %s translation bugs to <https://translationproject.org/team/>\n",
	 "csplit");
    }
  else
    {
      func_0x00401c10 ("\n%s online help: <%s>\n", "GNU coreutils",
		       "https://www.gnu.org/software/coreutils/");
      lVar2 = func_0x00402030 (5, 0);
      if ((lVar2 != 0)
	  && (iVar1 = func_0x00401ee0 (lVar2, &DAT_0041c48d, 3), iVar1 != 0))
	goto LAB_00403138;
    }
  func_0x00401c10 ("Full documentation at: <%s%s>\n",
		   "https://www.gnu.org/software/coreutils/", "csplit");
  uVar3 = 0x41e437;
  if (pcVar5 == "csplit")
    {
      uVar3 = 0x41c425;
    }
LAB_0040317a:
  ;
  do
    {
      func_0x00401c10
	("or available locally via: info \'(coreutils) %s%s\'\n", pcVar5,
	 uVar3);
    LAB_0040318c:
      ;
      func_0x004020a0 ((ulong) uParm1);
    LAB_00403193:
      ;
      func_0x00401c10 ("Full documentation at: <%s%s>\n",
		       "https://www.gnu.org/software/coreutils/", "csplit");
      pcVar5 = "csplit";
      uVar3 = 0x41c425;
    }
  while (true);
}

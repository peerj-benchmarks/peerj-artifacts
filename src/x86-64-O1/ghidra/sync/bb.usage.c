
#include "sync.h"

long null_ARRAY_0060f9a_0_8_;
long null_ARRAY_0060f9a_8_8_;
long null_ARRAY_0060fb8_0_8_;
long null_ARRAY_0060fb8_16_8_;
long null_ARRAY_0060fb8_24_8_;
long null_ARRAY_0060fb8_32_8_;
long null_ARRAY_0060fb8_40_8_;
long null_ARRAY_0060fb8_48_8_;
long null_ARRAY_0060fb8_8_8_;
long null_ARRAY_0060fbc_0_4_;
long null_ARRAY_0060fbc_16_8_;
long null_ARRAY_0060fbc_4_4_;
long null_ARRAY_0060fbc_8_4_;
long local_68_2_2_;
long local_6b_1_3_;
long local_6c_4_4_;
long local_6e_4_4_;
long local_6f_4_4_;
long local_70_0_2_;
long local_70_4_4_;
long local_71_0_1_;
long local_71_4_4_;
long local_72_4_4_;
long DAT_00000010;
long DAT_0040ccc0;
long DAT_0040cce3;
long DAT_0040cd67;
long DAT_0040cd8a;
long DAT_0040d1f8;
long DAT_0040d1fc;
long DAT_0040d200;
long DAT_0040d203;
long DAT_0040d205;
long DAT_0040d209;
long DAT_0040d20d;
long DAT_0040d9ab;
long DAT_0040dd55;
long DAT_0040de59;
long DAT_0040de5f;
long DAT_0040de71;
long DAT_0040de72;
long DAT_0040de90;
long DAT_0040de94;
long DAT_0040df1e;
long DAT_0060f560;
long DAT_0060f570;
long DAT_0060f580;
long DAT_0060f948;
long DAT_0060f9b0;
long DAT_0060f9b4;
long DAT_0060f9b8;
long DAT_0060f9bc;
long DAT_0060f9c0;
long DAT_0060f9d0;
long DAT_0060f9e0;
long DAT_0060f9e8;
long DAT_0060fa00;
long DAT_0060fa08;
long DAT_0060fa50;
long DAT_0060fa58;
long DAT_0060fa60;
long DAT_0060fbb8;
long DAT_0060fbf8;
long DAT_0060fc00;
long DAT_0060fc08;
long DAT_0060fc18;
long fde_0040ea68;
long null_ARRAY_0040d100;
long null_ARRAY_0040e340;
long null_ARRAY_0040e580;
long null_ARRAY_0060f9a0;
long null_ARRAY_0060fa20;
long null_ARRAY_0060fa80;
long null_ARRAY_0060fb80;
long null_ARRAY_0060fbc0;
long PTR_DAT_0060f940;
long PTR_null_ARRAY_0060f998;
void
FUN_00401abe (uint uParm1)
{
  int iVar1;
  long lVar2;
  undefined8 uVar3;
  undefined **ppuVar4;
  undefined *puVar5;
  undefined *local_88;
  char *local_80;
  undefined8 local_78;
  char *local_70;
  char *local_68;
  char *local_60;
  char *local_58;
  char *local_50;
  char *local_48;
  char *local_40;
  char *local_38;
  char *local_30;
  undefined8 local_28;
  undefined8 local_20;

  ppuVar4 = &local_88;
  if (uParm1 != 0)
    {
      func_0x00401680 (DAT_0060f9e0,
		       "Try \'%s --help\' for more information.\n",
		       DAT_0060fa60);
      goto LAB_00401c96;
    }
  func_0x00401500 ("Usage: %s [OPTION] [FILE]...\n", DAT_0060fa60);
  uVar3 = DAT_0060f9c0;
  func_0x00401690
    ("Synchronize cached writes to persistent storage\n\nIf one or more files are specified, sync only them,\nor their containing file systems.\n\n",
     DAT_0060f9c0);
  func_0x00401690
    ("  -d, --data             sync only file data, no unneeded metadata\n",
     uVar3);
  func_0x00401690
    ("  -f, --file-system      sync the file systems that contain the files\n",
     uVar3);
  func_0x00401690 ("      --help     display this help and exit\n", uVar3);
  func_0x00401690 ("      --version  output version information and exit\n",
		   uVar3);
  local_88 = &DAT_0040cce3;
  local_80 = "test invocation";
  local_78 = 0x40cd46;
  local_70 = "Multi-call invocation";
  local_68 = "sha224sum";
  local_60 = "sha2 utilities";
  local_58 = "sha256sum";
  local_50 = "sha2 utilities";
  local_48 = "sha384sum";
  local_40 = "sha2 utilities";
  local_38 = "sha512sum";
  local_30 = "sha2 utilities";
  local_28 = 0;
  local_20 = 0;
  puVar5 = &DAT_0040cce3;
  do
    {
      iVar1 = func_0x00401770 (&DAT_0040ccc0, puVar5);
      if (iVar1 == 0)
	break;
      ppuVar4 = ppuVar4 + 2;
      puVar5 = *ppuVar4;
    }
  while (puVar5 != (undefined *) 0x0);
  puVar5 = ppuVar4[1];
  if (puVar5 == (undefined *) 0x0)
    {
      func_0x00401500 ("\n%s online help: <%s>\n", "GNU coreutils",
		       "https://www.gnu.org/software/coreutils/");
      lVar2 = func_0x004017d0 (5, 0);
      if (lVar2 == 0)
	goto LAB_00401c9d;
      iVar1 = func_0x004016e0 (lVar2, &DAT_0040cd67, 3);
      if (iVar1 == 0)
	{
	  func_0x00401500 ("Full documentation at: <%s%s>\n",
			   "https://www.gnu.org/software/coreutils/",
			   &DAT_0040ccc0);
	  puVar5 = &DAT_0040ccc0;
	  uVar3 = 0x40ccff;
	  goto LAB_00401c84;
	}
      puVar5 = &DAT_0040ccc0;
    LAB_00401c42:
      ;
      func_0x00401500
	("Report %s translation bugs to <https://translationproject.org/team/>\n",
	 &DAT_0040ccc0);
    }
  else
    {
      func_0x00401500 ("\n%s online help: <%s>\n", "GNU coreutils",
		       "https://www.gnu.org/software/coreutils/");
      lVar2 = func_0x004017d0 (5, 0);
      if ((lVar2 != 0)
	  && (iVar1 = func_0x004016e0 (lVar2, &DAT_0040cd67, 3), iVar1 != 0))
	goto LAB_00401c42;
    }
  func_0x00401500 ("Full documentation at: <%s%s>\n",
		   "https://www.gnu.org/software/coreutils/", &DAT_0040ccc0);
  uVar3 = 0x40de8f;
  if (puVar5 == &DAT_0040ccc0)
    {
      uVar3 = 0x40ccff;
    }
LAB_00401c84:
  ;
  do
    {
      func_0x00401500
	("or available locally via: info \'(coreutils) %s%s\'\n", puVar5,
	 uVar3);
    LAB_00401c96:
      ;
      func_0x00401830 ((ulong) uParm1);
    LAB_00401c9d:
      ;
      func_0x00401500 ("Full documentation at: <%s%s>\n",
		       "https://www.gnu.org/software/coreutils/",
		       &DAT_0040ccc0);
      puVar5 = &DAT_0040ccc0;
      uVar3 = 0x40ccff;
    }
  while (true);
}


#include "date.h"

long null_ARRAY_0061e9e_0_8_;
long null_ARRAY_0061e9e_8_8_;
long null_ARRAY_0061ec4_0_8_;
long null_ARRAY_0061ec4_16_8_;
long null_ARRAY_0061ec4_24_8_;
long null_ARRAY_0061ec4_32_8_;
long null_ARRAY_0061ec4_40_8_;
long null_ARRAY_0061ec4_48_8_;
long null_ARRAY_0061ec4_8_8_;
long null_ARRAY_0061ec8_0_4_;
long null_ARRAY_0061ec8_16_8_;
long null_ARRAY_0061ec8_4_4_;
long null_ARRAY_0061ec8_8_4_;
long local_49_0_1_;
long local_51_4_4_;
long local_68_2_2_;
long local_6b_1_3_;
long local_6c_4_4_;
long local_6e_4_4_;
long local_6f_4_4_;
long local_70_0_2_;
long local_70_4_4_;
long local_71_0_1_;
long local_71_4_4_;
long local_72_4_4_;
long _bVar3;
long DAT_00000010;
long DAT_00417118;
long DAT_0041719c;
long DAT_004171e0;
long DAT_004171e3;
long DAT_00417206;
long DAT_00418aea;
long DAT_00418aeb;
long DAT_00418aec;
long DAT_00418ef8;
long DAT_00418efc;
long DAT_00418f21;
long DAT_00418f76;
long DAT_00419059;
long DAT_0041908b;
long DAT_0041908e;
long DAT_00419090;
long DAT_00419091;
long DAT_004190b7;
long DAT_004190ba;
long DAT_00419204;
long DAT_0041920b;
long DAT_00419476;
long DAT_0041aed6;
long DAT_0041af20;
long DAT_0041af24;
long DAT_0041af28;
long DAT_0041af2b;
long DAT_0041af2f;
long DAT_0041af33;
long DAT_0041b6eb;
long DAT_0041ba95;
long DAT_0041bb99;
long DAT_0041bb9f;
long DAT_0041bbcd;
long DAT_0041bc1a;
long DAT_0061e540;
long DAT_0061e550;
long DAT_0061e560;
long DAT_0061e990;
long DAT_0061e9f0;
long DAT_0061e9f4;
long DAT_0061e9f8;
long DAT_0061e9fc;
long DAT_0061ea40;
long DAT_0061ea80;
long DAT_0061ea88;
long DAT_0061ea90;
long DAT_0061eaa0;
long DAT_0061eaa8;
long DAT_0061eac0;
long DAT_0061eac8;
long DAT_0061eb10;
long DAT_0061eb18;
long DAT_0061eb20;
long DAT_0061eb28;
long DAT_0061ecf8;
long DAT_0061ed00;
long DAT_0061ed08;
long DAT_0061ed10;
long DAT_0061f548;
long DAT_0061f550;
long DAT_0061f560;
long fde_0041cd98;
long null_ARRAY_004186c0;
long null_ARRAY_00418780;
long null_ARRAY_00418800;
long null_ARRAY_00418a00;
long null_ARRAY_00418a30;
long null_ARRAY_00418a60;
long null_ARRAY_0041a000;
long null_ARRAY_0041a100;
long null_ARRAY_0041a2c0;
long null_ARRAY_0041a5c0;
long null_ARRAY_0041a600;
long null_ARRAY_0041a780;
long null_ARRAY_0041a840;
long null_ARRAY_0041a9d0;
long null_ARRAY_0041aa00;
long null_ARRAY_0041aa80;
long null_ARRAY_0041ab80;
long null_ARRAY_0041ac00;
long null_ARRAY_0041ac80;
long null_ARRAY_0041aca0;
long null_ARRAY_0041acc0;
long null_ARRAY_0041ad40;
long null_ARRAY_0041adc0;
long null_ARRAY_0041bbe0;
long null_ARRAY_0041c040;
long null_ARRAY_0041c6d0;
long null_ARRAY_0061e9e0;
long null_ARRAY_0061eae0;
long null_ARRAY_0061eb40;
long null_ARRAY_0061ec40;
long null_ARRAY_0061ec80;
long null_ARRAY_0061ecc0;
long null_ARRAY_0061ed40;
long PTR_DAT_0061e980;
long PTR_FUN_0061e988;
long PTR_null_ARRAY_0061e9d8;
long PTR_null_ARRAY_0061ea00;
long stack0x00000008;
long stack0xffffffffffffffe0;
long UNK_0041a041;
void
FUN_004020bd (uint uParm1)
{
  int iVar1;
  long lVar2;
  undefined8 uVar3;
  undefined **ppuVar4;
  long lVar5;
  undefined *puVar6;
  undefined *local_88;
  char *local_80;
  undefined8 local_78;
  char *local_70;
  char *local_68;
  char *local_60;
  char *local_58;
  char *local_50;
  char *local_48;
  char *local_40;
  char *local_38;
  char *local_30;
  undefined8 local_28;
  undefined8 local_20;

  ppuVar4 = &local_88;
  if (uParm1 != 0)
    {
      func_0x00401b00 (DAT_0061eaa0,
		       "Try \'%s --help\' for more information.\n",
		       DAT_0061eb28);
      goto LAB_004023a9;
    }
  func_0x00401950
    ("Usage: %s [OPTION]... [+FORMAT]\n  or:  %s [-u|--utc|--universal] [MMDDhhmm[[CC]YY][.ss]]\n",
     DAT_0061eb28, DAT_0061eb28);
  uVar3 = DAT_0061ea40;
  func_0x00401b10
    ("Display the current time in the given FORMAT, or set the system date.\n",
     DAT_0061ea40);
  func_0x00401b10
    ("\nMandatory arguments to long options are mandatory for short options too.\n",
     uVar3);
  func_0x00401b10
    ("  -d, --date=STRING          display time described by STRING, not \'now\'\n",
     uVar3);
  func_0x00401b10
    ("      --debug                annotate the parsed date,\n                              and warn about questionable usage to stderr\n",
     uVar3);
  func_0x00401b10
    ("  -f, --file=DATEFILE        like --date; once for each line of DATEFILE\n",
     uVar3);
  func_0x00401b10
    ("  -I[FMT], --iso-8601[=FMT]  output date/time in ISO 8601 format.\n                               FMT=\'date\' for date only (the default),\n                               \'hours\', \'minutes\', \'seconds\', or \'ns\'\n                               for date and time to the indicated precision.\n                               Example: 2006-08-14T02:34:56-06:00\n",
     uVar3);
  func_0x00401b10
    ("  -R, --rfc-email            output date and time in RFC 5322 format.\n                               Example: Mon, 14 Aug 2006 02:34:56 -0600\n",
     uVar3);
  func_0x00401b10
    ("      --rfc-3339=FMT         output date/time in RFC 3339 format.\n                               FMT=\'date\', \'seconds\', or \'ns\'\n                               for date and time to the indicated precision.\n                               Example: 2006-08-14 02:34:56-06:00\n",
     uVar3);
  func_0x00401b10
    ("  -r, --reference=FILE       display the last modification time of FILE\n",
     uVar3);
  func_0x00401b10
    ("  -s, --set=STRING           set time described by STRING\n  -u, --utc, --universal     print or set Coordinated Universal Time (UTC)\n",
     uVar3);
  func_0x00401b10 ("      --help     display this help and exit\n", uVar3);
  func_0x00401b10 ("      --version  output version information and exit\n",
		   uVar3);
  func_0x00401b10
    ("\nFORMAT controls the output.  Interpreted sequences are:\n\n  %%   a literal %\n  %a   locale\'s abbreviated weekday name (e.g., Sun)\n",
     uVar3);
  func_0x00401b10
    ("  %A   locale\'s full weekday name (e.g., Sunday)\n  %b   locale\'s abbreviated month name (e.g., Jan)\n  %B   locale\'s full month name (e.g., January)\n  %c   locale\'s date and time (e.g., Thu Mar  3 23:05:25 2005)\n",
     uVar3);
  func_0x00401b10
    ("  %C   century; like %Y, except omit last two digits (e.g., 20)\n  %d   day of month (e.g., 01)\n  %D   date; same as %m/%d/%y\n  %e   day of month, space padded; same as %_d\n",
     uVar3);
  func_0x00401b10
    ("  %F   full date; same as %Y-%m-%d\n  %g   last two digits of year of ISO week number (see %G)\n  %G   year of ISO week number (see %V); normally useful only with %V\n",
     uVar3);
  func_0x00401b10
    ("  %h   same as %b\n  %H   hour (00..23)\n  %I   hour (01..12)\n  %j   day of year (001..366)\n",
     uVar3);
  func_0x00401b10
    ("  %k   hour, space padded ( 0..23); same as %_H\n  %l   hour, space padded ( 1..12); same as %_I\n  %m   month (01..12)\n  %M   minute (00..59)\n",
     uVar3);
  func_0x00401b10
    ("  %n   a newline\n  %N   nanoseconds (000000000..999999999)\n  %p   locale\'s equivalent of either AM or PM; blank if not known\n  %P   like %p, but lower case\n  %q   quarter of year (1..4)\n  %r   locale\'s 12-hour clock time (e.g., 11:11:04 PM)\n  %R   24-hour hour and minute; same as %H:%M\n  %s   seconds since 1970-01-01 00:00:00 UTC\n",
     uVar3);
  func_0x00401b10
    ("  %S   second (00..60)\n  %t   a tab\n  %T   time; same as %H:%M:%S\n  %u   day of week (1..7); 1 is Monday\n",
     uVar3);
  func_0x00401b10
    ("  %U   week number of year, with Sunday as first day of week (00..53)\n  %V   ISO week number, with Monday as first day of week (01..53)\n  %w   day of week (0..6); 0 is Sunday\n  %W   week number of year, with Monday as first day of week (00..53)\n",
     uVar3);
  func_0x00401b10
    ("  %x   locale\'s date representation (e.g., 12/31/99)\n  %X   locale\'s time representation (e.g., 23:13:48)\n  %y   last two digits of year (00..99)\n  %Y   year\n",
     uVar3);
  func_0x00401b10
    ("  %z   +hhmm numeric time zone (e.g., -0400)\n  %:z  +hh:mm numeric time zone (e.g., -04:00)\n  %::z  +hh:mm:ss numeric time zone (e.g., -04:00:00)\n  %:::z  numeric time zone with : to necessary precision (e.g., -04, +05:30)\n  %Z   alphabetic time zone abbreviation (e.g., EDT)\n\nBy default, date pads numeric fields with zeroes.\n",
     uVar3);
  func_0x00401b10
    ("The following optional flags may follow \'%\':\n\n  -  (hyphen) do not pad the field\n  _  (underscore) pad with spaces\n  0  (zero) pad with zeros\n  ^  use upper case if possible\n  #  use opposite case if possible\n",
     uVar3);
  func_0x00401b10
    ("\nAfter any flags comes an optional field width, as a decimal number;\nthen an optional modifier, which is either\nE to use the locale\'s alternate representations if available, or\nO to use the locale\'s alternate numeric symbols if available.\n",
     uVar3);
  func_0x00401b10
    ("\nExamples:\nConvert seconds since the epoch (1970-01-01 UTC) to a date\n  $ date --date=\'@2147483647\'\n\nShow the time on the west coast of the US (use tzselect(1) to find TZ)\n  $ TZ=\'America/Los_Angeles\' date\n\nShow the local time for 9AM next Friday on the west coast of the US\n  $ date --date=\'TZ=\"America/Los_Angeles\" 09:00 next Fri\'\n",
     uVar3);
  local_88 = &DAT_00417118;
  local_80 = "test invocation";
  local_78 = 0x41717b;
  local_70 = "Multi-call invocation";
  local_68 = "sha224sum";
  local_60 = "sha2 utilities";
  local_58 = "sha256sum";
  local_50 = "sha2 utilities";
  local_48 = "sha384sum";
  local_40 = "sha2 utilities";
  local_38 = "sha512sum";
  local_30 = "sha2 utilities";
  local_28 = 0;
  local_20 = 0;
  puVar6 = &DAT_00417118;
  do
    {
      iVar1 = func_0x00401c20 (0x417224, puVar6);
      if (iVar1 == 0)
	break;
      ppuVar4 = ppuVar4 + 2;
      puVar6 = *ppuVar4;
    }
  while (puVar6 != (undefined *) 0x0);
  lVar5 = (long) ppuVar4[1];
  if (lVar5 == 0)
    {
      func_0x00401950 ("\n%s online help: <%s>\n", "GNU coreutils",
		       "https://www.gnu.org/software/coreutils/");
      lVar5 = func_0x00401c80 (5, 0);
      if (lVar5 == 0)
	goto LAB_004023b0;
      iVar1 = func_0x00401b60 (lVar5, &DAT_0041719c, 3);
      if (iVar1 == 0)
	{
	  func_0x00401950 ("Full documentation at: <%s%s>\n",
			   "https://www.gnu.org/software/coreutils/",
			   0x417224);
	  lVar5 = 0x417224;
	  uVar3 = 0x417134;
	  goto LAB_00402397;
	}
      lVar5 = 0x417224;
    LAB_00402355:
      ;
      func_0x00401950
	("Report %s translation bugs to <https://translationproject.org/team/>\n",
	 0x417224);
    }
  else
    {
      func_0x00401950 ("\n%s online help: <%s>\n", "GNU coreutils",
		       "https://www.gnu.org/software/coreutils/");
      lVar2 = func_0x00401c80 (5, 0);
      if ((lVar2 != 0)
	  && (iVar1 = func_0x00401b60 (lVar2, &DAT_0041719c, 3), iVar1 != 0))
	goto LAB_00402355;
    }
  func_0x00401950 ("Full documentation at: <%s%s>\n",
		   "https://www.gnu.org/software/coreutils/", 0x417224);
  uVar3 = 0x41bbcc;
  if (lVar5 == 0x417224)
    {
      uVar3 = 0x417134;
    }
LAB_00402397:
  ;
  do
    {
      func_0x00401950
	("or available locally via: info \'(coreutils) %s%s\'\n", lVar5,
	 uVar3);
    LAB_004023a9:
      ;
      func_0x00401d00 ((ulong) uParm1);
    LAB_004023b0:
      ;
      func_0x00401950 ("Full documentation at: <%s%s>\n",
		       "https://www.gnu.org/software/coreutils/", 0x417224);
      lVar5 = 0x417224;
      uVar3 = 0x417134;
    }
  while (true);
}


#include "b2sum.h"

long null_ARRAY_006149c_0_8_;
long null_ARRAY_006149c_8_8_;
long null_ARRAY_00614c0_0_8_;
long null_ARRAY_00614c0_16_8_;
long null_ARRAY_00614c0_24_8_;
long null_ARRAY_00614c0_32_8_;
long null_ARRAY_00614c0_40_8_;
long null_ARRAY_00614c0_48_8_;
long null_ARRAY_00614c0_8_8_;
long null_ARRAY_00614c4_0_4_;
long null_ARRAY_00614c4_16_8_;
long null_ARRAY_00614c4_4_4_;
long null_ARRAY_00614c4_8_4_;
long local_68_2_2_;
long local_6b_1_3_;
long local_6c_4_4_;
long local_6e_4_4_;
long local_6f_4_4_;
long local_70_0_2_;
long local_70_4_4_;
long local_71_0_1_;
long local_71_4_4_;
long local_72_4_4_;
long DAT_00000010;
long DAT_004107c0;
long DAT_004107c3;
long DAT_004107dc;
long DAT_00410860;
long DAT_00410937;
long DAT_0041093c;
long DAT_0041093f;
long DAT_00410944;
long DAT_00411618;
long DAT_0041161c;
long DAT_00411620;
long DAT_00411623;
long DAT_00411625;
long DAT_00411629;
long DAT_0041162d;
long DAT_00411dab;
long DAT_00412808;
long DAT_00412911;
long DAT_00412917;
long DAT_00412929;
long DAT_0041292a;
long DAT_00412948;
long DAT_0041294c;
long DAT_004129d6;
long DAT_00614540;
long DAT_00614550;
long DAT_00614560;
long DAT_00614960;
long DAT_00614978;
long DAT_006149d0;
long DAT_006149d4;
long DAT_006149d8;
long DAT_006149dc;
long DAT_00614a00;
long DAT_00614a08;
long DAT_00614a10;
long DAT_00614a20;
long DAT_00614a28;
long DAT_00614a40;
long DAT_00614a48;
long DAT_00614a90;
long DAT_00614a98;
long DAT_00614a9c;
long DAT_00614a9d;
long DAT_00614a9e;
long DAT_00614a9f;
long DAT_00614aa0;
long DAT_00614aa8;
long DAT_00614ab0;
long DAT_00614ab8;
long DAT_00614ac0;
long DAT_00614ac8;
long DAT_00614ad0;
long DAT_00614c38;
long DAT_00614c78;
long DAT_00614c80;
long DAT_00614c88;
long DAT_00614c98;
long fde_004135c8;
long null_ARRAY_00411400;
long null_ARRAY_004115a0;
long null_ARRAY_004115b0;
long null_ARRAY_00412de0;
long null_ARRAY_00413020;
long null_ARRAY_006149c0;
long null_ARRAY_00614a60;
long null_ARRAY_00614b00;
long null_ARRAY_00614c00;
long null_ARRAY_00614c40;
long PTR_DAT_00614970;
long PTR_FUN_00614968;
long PTR_null_ARRAY_006149b8;
long stack0x00000008;
void
FUN_004020b4 (uint uParm1)
{
  int iVar1;
  long lVar2;
  undefined8 uVar3;
  undefined **ppuVar4;
  char *pcVar5;
  undefined *puVar6;
  undefined *local_88;
  char *local_80;
  undefined8 local_78;
  char *local_70;
  char *local_68;
  char *local_60;
  char *local_58;
  char *local_50;
  char *local_48;
  char *local_40;
  char *local_38;
  char *local_30;
  undefined8 local_28;
  undefined8 local_20;

  ppuVar4 = &local_88;
  if (uParm1 != 0)
    {
      func_0x00401940 (DAT_00614a20,
		       "Try \'%s --help\' for more information.\n",
		       DAT_00614ad0);
      goto LAB_004022e5;
    }
  func_0x00401770
    ("Usage: %s [OPTION]... [FILE]...\nPrint or check %s (%d-bit) checksums.\n",
     DAT_00614ad0, "BLAKE2", 0x200);
  uVar3 = DAT_00614a00;
  func_0x00401950
    ("\nWith no FILE, or when FILE is -, read standard input.\n",
     DAT_00614a00);
  func_0x00401950 ("\n  -b, --binary         read in binary mode\n", uVar3);
  func_0x00401770
    ("  -c, --check          read %s sums from the FILEs and check them\n",
     "BLAKE2");
  func_0x00401950
    ("  -l, --length         digest length in bits; must not exceed the maximum for\n                       the blake2 algorithm and must be a multiple of 8\n",
     uVar3);
  func_0x00401950 ("      --tag            create a BSD-style checksum\n",
		   uVar3);
  func_0x00401950 ("  -t, --text           read in text mode (default)\n",
		   uVar3);
  func_0x00401950
    ("\nThe following five options are useful only when verifying checksums:\n      --ignore-missing  don\'t fail or report status for missing files\n      --quiet          don\'t print OK for each successfully verified file\n      --status         don\'t output anything, status code shows success\n      --strict         exit non-zero for improperly formatted checksum lines\n  -w, --warn           warn about improperly formatted checksum lines\n\n",
     uVar3);
  func_0x00401950 ("      --help     display this help and exit\n", uVar3);
  func_0x00401950 ("      --version  output version information and exit\n",
		   uVar3);
  func_0x00401770
    ("\nThe sums are computed as described in %s.  When checking, the input\nshould be a former output of this program.  The default mode is to print a\nline with checksum, a space, a character indicating input mode (\'*\' for binary,\n\' \' for text or where binary is insignificant), and name for each FILE.\n",
     "RFC 7693");
  local_88 = &DAT_004107dc;
  local_80 = "test invocation";
  local_78 = 0x41083f;
  local_70 = "Multi-call invocation";
  local_68 = "sha224sum";
  local_60 = "sha2 utilities";
  local_58 = "sha256sum";
  local_50 = "sha2 utilities";
  local_48 = "sha384sum";
  local_40 = "sha2 utilities";
  local_38 = "sha512sum";
  local_30 = "sha2 utilities";
  local_28 = 0;
  local_20 = 0;
  puVar6 = &DAT_004107dc;
  do
    {
      iVar1 = func_0x00401a70 ("b2sum", puVar6);
      if (iVar1 == 0)
	break;
      ppuVar4 = ppuVar4 + 2;
      puVar6 = *ppuVar4;
    }
  while (puVar6 != (undefined *) 0x0);
  pcVar5 = ppuVar4[1];
  if (pcVar5 == (char *) 0x0)
    {
      func_0x00401770 ("\n%s online help: <%s>\n", "GNU coreutils",
		       "https://www.gnu.org/software/coreutils/");
      lVar2 = func_0x00401ad0 (5, 0);
      if (lVar2 == 0)
	goto LAB_004022ec;
      iVar1 = func_0x004019c0 (lVar2, &DAT_00410860, 3);
      if (iVar1 == 0)
	{
	  func_0x00401770 ("Full documentation at: <%s%s>\n",
			   "https://www.gnu.org/software/coreutils/",
			   "b2sum");
	  pcVar5 = "b2sum";
	  uVar3 = 0x4107f8;
	  goto LAB_004022d3;
	}
      pcVar5 = "b2sum";
    LAB_00402291:
      ;
      func_0x00401770
	("Report %s translation bugs to <https://translationproject.org/team/>\n",
	 "b2sum");
    }
  else
    {
      func_0x00401770 ("\n%s online help: <%s>\n", "GNU coreutils",
		       "https://www.gnu.org/software/coreutils/");
      lVar2 = func_0x00401ad0 (5, 0);
      if ((lVar2 != 0)
	  && (iVar1 = func_0x004019c0 (lVar2, &DAT_00410860, 3), iVar1 != 0))
	goto LAB_00402291;
    }
  func_0x00401770 ("Full documentation at: <%s%s>\n",
		   "https://www.gnu.org/software/coreutils/", "b2sum");
  uVar3 = 0x412947;
  if (pcVar5 == "b2sum")
    {
      uVar3 = 0x4107f8;
    }
LAB_004022d3:
  ;
  do
    {
      func_0x00401770
	("or available locally via: info \'(coreutils) %s%s\'\n", pcVar5,
	 uVar3);
    LAB_004022e5:
      ;
      func_0x00401b30 ((ulong) uParm1);
    LAB_004022ec:
      ;
      func_0x00401770 ("Full documentation at: <%s%s>\n",
		       "https://www.gnu.org/software/coreutils/", "b2sum");
      pcVar5 = "b2sum";
      uVar3 = 0x4107f8;
    }
  while (true);
}

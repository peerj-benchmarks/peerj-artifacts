
#include "shuf.h"

long null_ARRAY_006144c_0_8_;
long null_ARRAY_006144c_8_8_;
long null_ARRAY_006146c_0_8_;
long null_ARRAY_006146c_16_8_;
long null_ARRAY_006146c_24_8_;
long null_ARRAY_006146c_32_8_;
long null_ARRAY_006146c_40_8_;
long null_ARRAY_006146c_48_8_;
long null_ARRAY_006146c_8_8_;
long null_ARRAY_0061470_0_4_;
long null_ARRAY_0061470_16_8_;
long null_ARRAY_0061470_4_4_;
long null_ARRAY_0061470_8_4_;
long local_68_2_2_;
long local_6b_1_3_;
long local_6c_4_4_;
long local_6e_4_4_;
long local_6f_4_4_;
long local_70_0_2_;
long local_70_4_4_;
long local_71_0_1_;
long local_71_4_4_;
long local_72_4_4_;
long DAT_00000010;
long DAT_00410380;
long DAT_00410385;
long DAT_00410409;
long DAT_004104be;
long DAT_00410b90;
long DAT_00410b94;
long DAT_00410b98;
long DAT_00410b9b;
long DAT_00410b9d;
long DAT_00410ba1;
long DAT_00410ba5;
long DAT_00411337;
long DAT_0041135a;
long DAT_00411da8;
long DAT_00411eb1;
long DAT_00411eb7;
long DAT_00411ec9;
long DAT_00411eca;
long DAT_00411ee8;
long DAT_00411eec;
long DAT_00411fae;
long DAT_00614000;
long DAT_00614010;
long DAT_00614020;
long DAT_00614468;
long DAT_006144d0;
long DAT_006144d4;
long DAT_006144d8;
long DAT_006144dc;
long DAT_00614500;
long DAT_00614508;
long DAT_00614510;
long DAT_00614520;
long DAT_00614528;
long DAT_00614540;
long DAT_00614548;
long DAT_00614590;
long DAT_00614598;
long DAT_006145a0;
long DAT_00614778;
long DAT_00614780;
long DAT_00614788;
long DAT_00614790;
long DAT_006147a0;
long fde_00412c50;
long null_ARRAY_00410a00;
long null_ARRAY_00411f00;
long null_ARRAY_004123c0;
long null_ARRAY_004125f0;
long null_ARRAY_006144c0;
long null_ARRAY_00614560;
long null_ARRAY_006145c0;
long null_ARRAY_006146c0;
long null_ARRAY_00614700;
long PTR_DAT_00614460;
long PTR_null_ARRAY_006144b8;
long PTR_null_ARRAY_006144e0;
void
FUN_0040206e (uint uParm1)
{
  int iVar1;
  long lVar2;
  undefined8 uVar3;
  undefined **ppuVar4;
  undefined *puVar5;
  undefined *local_88;
  char *local_80;
  undefined8 local_78;
  char *local_70;
  char *local_68;
  char *local_60;
  char *local_58;
  char *local_50;
  char *local_48;
  char *local_40;
  char *local_38;
  char *local_30;
  undefined8 local_28;
  undefined8 local_20;

  ppuVar4 = &local_88;
  if (uParm1 != 0)
    {
      func_0x00401ba0 (DAT_00614520,
		       "Try \'%s --help\' for more information.\n",
		       DAT_006145a0);
      goto LAB_00402266;
    }
  func_0x004019b0
    ("Usage: %s [OPTION]... [FILE]\n  or:  %s -e [OPTION]... [ARG]...\n  or:  %s -i LO-HI [OPTION]...\n",
     DAT_006145a0, DAT_006145a0, DAT_006145a0);
  uVar3 = DAT_00614500;
  func_0x00401bb0
    ("Write a random permutation of the input lines to standard output.\n",
     DAT_00614500);
  func_0x00401bb0
    ("\nWith no FILE, or when FILE is -, read standard input.\n", uVar3);
  func_0x00401bb0
    ("\nMandatory arguments to long options are mandatory for short options too.\n",
     uVar3);
  func_0x00401bb0
    ("  -e, --echo                treat each ARG as an input line\n  -i, --input-range=LO-HI   treat each number LO through HI as an input line\n  -n, --head-count=COUNT    output at most COUNT lines\n  -o, --output=FILE         write result to FILE instead of standard output\n      --random-source=FILE  get random bytes from FILE\n  -r, --repeat              output lines can be repeated\n",
     uVar3);
  func_0x00401bb0
    ("  -z, --zero-terminated     line delimiter is NUL, not newline\n",
     uVar3);
  func_0x00401bb0 ("      --help     display this help and exit\n", uVar3);
  func_0x00401bb0 ("      --version  output version information and exit\n",
		   uVar3);
  local_88 = &DAT_00410385;
  local_80 = "test invocation";
  local_78 = 0x4103e8;
  local_70 = "Multi-call invocation";
  local_68 = "sha224sum";
  local_60 = "sha2 utilities";
  local_58 = "sha256sum";
  local_50 = "sha2 utilities";
  local_48 = "sha384sum";
  local_40 = "sha2 utilities";
  local_38 = "sha512sum";
  local_30 = "sha2 utilities";
  local_28 = 0;
  local_20 = 0;
  puVar5 = &DAT_00410385;
  do
    {
      iVar1 = func_0x00401d10 (&DAT_00410380, puVar5);
      if (iVar1 == 0)
	break;
      ppuVar4 = ppuVar4 + 2;
      puVar5 = *ppuVar4;
    }
  while (puVar5 != (undefined *) 0x0);
  puVar5 = ppuVar4[1];
  if (puVar5 == (undefined *) 0x0)
    {
      func_0x004019b0 ("\n%s online help: <%s>\n", "GNU coreutils",
		       "https://www.gnu.org/software/coreutils/");
      lVar2 = func_0x00401d70 (5, 0);
      if (lVar2 == 0)
	goto LAB_0040226d;
      iVar1 = func_0x00401c30 (lVar2, &DAT_00410409, 3);
      if (iVar1 == 0)
	{
	  func_0x004019b0 ("Full documentation at: <%s%s>\n",
			   "https://www.gnu.org/software/coreutils/",
			   &DAT_00410380);
	  puVar5 = &DAT_00410380;
	  uVar3 = 0x4103a1;
	  goto LAB_00402254;
	}
      puVar5 = &DAT_00410380;
    LAB_00402212:
      ;
      func_0x004019b0
	("Report %s translation bugs to <https://translationproject.org/team/>\n",
	 &DAT_00410380);
    }
  else
    {
      func_0x004019b0 ("\n%s online help: <%s>\n", "GNU coreutils",
		       "https://www.gnu.org/software/coreutils/");
      lVar2 = func_0x00401d70 (5, 0);
      if ((lVar2 != 0)
	  && (iVar1 = func_0x00401c30 (lVar2, &DAT_00410409, 3), iVar1 != 0))
	goto LAB_00402212;
    }
  func_0x004019b0 ("Full documentation at: <%s%s>\n",
		   "https://www.gnu.org/software/coreutils/", &DAT_00410380);
  uVar3 = 0x411ee7;
  if (puVar5 == &DAT_00410380)
    {
      uVar3 = 0x4103a1;
    }
LAB_00402254:
  ;
  do
    {
      func_0x004019b0
	("or available locally via: info \'(coreutils) %s%s\'\n", puVar5,
	 uVar3);
    LAB_00402266:
      ;
      func_0x00401de0 ((ulong) uParm1);
    LAB_0040226d:
      ;
      func_0x004019b0 ("Full documentation at: <%s%s>\n",
		       "https://www.gnu.org/software/coreutils/",
		       &DAT_00410380);
      puVar5 = &DAT_00410380;
      uVar3 = 0x4103a1;
    }
  while (true);
}

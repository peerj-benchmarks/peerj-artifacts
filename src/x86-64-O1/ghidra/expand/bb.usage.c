
#include "expand.h"

long null_ARRAY_006107a_0_8_;
long null_ARRAY_006107a_8_8_;
long null_ARRAY_006109c_0_8_;
long null_ARRAY_006109c_16_8_;
long null_ARRAY_006109c_24_8_;
long null_ARRAY_006109c_32_8_;
long null_ARRAY_006109c_40_8_;
long null_ARRAY_006109c_48_8_;
long null_ARRAY_006109c_8_8_;
long null_ARRAY_00610a0_0_4_;
long null_ARRAY_00610a0_16_8_;
long null_ARRAY_00610a0_4_4_;
long null_ARRAY_00610a0_8_4_;
long local_68_2_2_;
long local_6b_1_3_;
long local_6c_4_4_;
long local_6e_4_4_;
long local_6f_4_4_;
long local_70_0_2_;
long local_70_4_4_;
long local_71_0_1_;
long local_71_4_4_;
long local_72_4_4_;
long DAT_00000010;
long DAT_0040d607;
long DAT_0040d68b;
long DAT_0040de20;
long DAT_0040de24;
long DAT_0040de28;
long DAT_0040de2b;
long DAT_0040de2d;
long DAT_0040de31;
long DAT_0040de35;
long DAT_0040e5eb;
long DAT_0040e995;
long DAT_0040ea99;
long DAT_0040ea9f;
long DAT_0040eab1;
long DAT_0040eab2;
long DAT_0040ead0;
long DAT_0040ead4;
long DAT_0040eb5e;
long DAT_00610348;
long DAT_00610358;
long DAT_00610368;
long DAT_00610758;
long DAT_006107b0;
long DAT_006107b4;
long DAT_006107b8;
long DAT_006107bc;
long DAT_006107c0;
long DAT_006107c8;
long DAT_006107d0;
long DAT_006107e0;
long DAT_006107e8;
long DAT_00610800;
long DAT_00610808;
long DAT_00610850;
long DAT_00610854;
long DAT_00610858;
long DAT_00610860;
long DAT_00610868;
long DAT_00610870;
long DAT_00610878;
long DAT_00610880;
long DAT_00610888;
long DAT_00610890;
long DAT_00610898;
long DAT_006108a0;
long DAT_006108a8;
long DAT_006108b0;
long DAT_00610a38;
long DAT_00610a40;
long DAT_00610a48;
long DAT_00610a50;
long DAT_00610a60;
long fde_0040f700;
long null_ARRAY_0040d9c0;
long null_ARRAY_0040da60;
long null_ARRAY_0040ef80;
long null_ARRAY_0040f1c0;
long null_ARRAY_00610740;
long null_ARRAY_006107a0;
long null_ARRAY_00610820;
long null_ARRAY_006108c0;
long null_ARRAY_006109c0;
long null_ARRAY_00610a00;
long PTR_DAT_00610750;
long PTR_null_ARRAY_00610798;
void
FUN_00401c4e (uint uParm1)
{
  int iVar1;
  long lVar2;
  undefined8 uVar3;
  undefined **ppuVar4;
  char *pcVar5;
  undefined *puVar6;
  undefined *local_88;
  char *local_80;
  undefined8 local_78;
  char *local_70;
  char *local_68;
  char *local_60;
  char *local_58;
  char *local_50;
  char *local_48;
  char *local_40;
  char *local_38;
  char *local_30;
  undefined8 local_28;
  undefined8 local_20;

  ppuVar4 = &local_88;
  if (uParm1 != 0)
    {
      func_0x004017f0 (DAT_006107e0,
		       "Try \'%s --help\' for more information.\n",
		       DAT_006108b0);
      goto LAB_00401e38;
    }
  func_0x00401650 ("Usage: %s [OPTION]... [FILE]...\n", DAT_006108b0);
  uVar3 = DAT_006107c0;
  func_0x00401800
    ("Convert tabs in each FILE to spaces, writing to standard output.\n",
     DAT_006107c0);
  func_0x00401800
    ("\nWith no FILE, or when FILE is -, read standard input.\n", uVar3);
  func_0x00401800
    ("\nMandatory arguments to long options are mandatory for short options too.\n",
     uVar3);
  func_0x00401800
    ("  -i, --initial    do not convert tabs after non blanks\n  -t, --tabs=N     have tabs N characters apart, not 8\n",
     uVar3);
  FUN_00402a55 ();
  func_0x00401800 ("      --help     display this help and exit\n", uVar3);
  func_0x00401800 ("      --version  output version information and exit\n",
		   uVar3);
  local_88 = &DAT_0040d607;
  local_80 = "test invocation";
  local_78 = 0x40d66a;
  local_70 = "Multi-call invocation";
  local_68 = "sha224sum";
  local_60 = "sha2 utilities";
  local_58 = "sha256sum";
  local_50 = "sha2 utilities";
  local_48 = "sha384sum";
  local_40 = "sha2 utilities";
  local_38 = "sha512sum";
  local_30 = "sha2 utilities";
  local_28 = 0;
  local_20 = 0;
  puVar6 = &DAT_0040d607;
  do
    {
      iVar1 = func_0x00401900 ("expand", puVar6);
      if (iVar1 == 0)
	break;
      ppuVar4 = ppuVar4 + 2;
      puVar6 = *ppuVar4;
    }
  while (puVar6 != (undefined *) 0x0);
  pcVar5 = ppuVar4[1];
  if (pcVar5 == (char *) 0x0)
    {
      func_0x00401650 ("\n%s online help: <%s>\n", "GNU coreutils",
		       "https://www.gnu.org/software/coreutils/");
      lVar2 = func_0x00401960 (5, 0);
      if (lVar2 == 0)
	goto LAB_00401e3f;
      iVar1 = func_0x00401860 (lVar2, &DAT_0040d68b, 3);
      if (iVar1 == 0)
	{
	  func_0x00401650 ("Full documentation at: <%s%s>\n",
			   "https://www.gnu.org/software/coreutils/",
			   "expand");
	  pcVar5 = "expand";
	  uVar3 = 0x40d623;
	  goto LAB_00401e26;
	}
      pcVar5 = "expand";
    LAB_00401de4:
      ;
      func_0x00401650
	("Report %s translation bugs to <https://translationproject.org/team/>\n",
	 "expand");
    }
  else
    {
      func_0x00401650 ("\n%s online help: <%s>\n", "GNU coreutils",
		       "https://www.gnu.org/software/coreutils/");
      lVar2 = func_0x00401960 (5, 0);
      if ((lVar2 != 0)
	  && (iVar1 = func_0x00401860 (lVar2, &DAT_0040d68b, 3), iVar1 != 0))
	goto LAB_00401de4;
    }
  func_0x00401650 ("Full documentation at: <%s%s>\n",
		   "https://www.gnu.org/software/coreutils/", "expand");
  uVar3 = 0x40eacf;
  if (pcVar5 == "expand")
    {
      uVar3 = 0x40d623;
    }
LAB_00401e26:
  ;
  do
    {
      func_0x00401650
	("or available locally via: info \'(coreutils) %s%s\'\n", pcVar5,
	 uVar3);
    LAB_00401e38:
      ;
      func_0x004019b0 ((ulong) uParm1);
    LAB_00401e3f:
      ;
      func_0x00401650 ("Full documentation at: <%s%s>\n",
		       "https://www.gnu.org/software/coreutils/", "expand");
      pcVar5 = "expand";
      uVar3 = 0x40d623;
    }
  while (true);
}

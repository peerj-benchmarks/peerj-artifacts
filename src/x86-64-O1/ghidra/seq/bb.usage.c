
#include "seq.h"

long null_ARRAY_0061146_0_8_;
long null_ARRAY_0061146_8_8_;
long null_ARRAY_0061168_0_8_;
long null_ARRAY_0061168_16_8_;
long null_ARRAY_0061168_24_8_;
long null_ARRAY_0061168_32_8_;
long null_ARRAY_0061168_40_8_;
long null_ARRAY_0061168_48_8_;
long null_ARRAY_0061168_8_8_;
long null_ARRAY_006116c_0_4_;
long null_ARRAY_006116c_16_8_;
long null_ARRAY_006116c_4_4_;
long null_ARRAY_006116c_8_4_;
long local_68_2_2_;
long local_6b_1_3_;
long local_6c_4_4_;
long local_6e_4_4_;
long local_6f_4_4_;
long local_70_0_2_;
long local_70_4_4_;
long local_71_0_1_;
long local_71_4_4_;
long local_72_4_4_;
long local_c_0_4_;
long bRam0000000025681279;
long _bVar8;
long cRam0000000000000080;
long cRam000000006473254e;
long cRam000000007365f263;
long DAT_00000010;
long DAT_0040dc57;
long DAT_0040dc5b;
long DAT_0040dc5f;
long DAT_0040dce3;
long DAT_0040dd0c;
long DAT_0040dd0f;
long DAT_0040dd12;
long DAT_0040dd14;
long DAT_0040dd6d;
long DAT_0040dd72;
long DAT_0040ddab;
long DAT_0040e580;
long DAT_0040e5d8;
long DAT_0040e5dc;
long DAT_0040e5e0;
long DAT_0040e5e3;
long DAT_0040e5e5;
long DAT_0040e5e9;
long DAT_0040e5ed;
long DAT_0040ed6b;
long DAT_0040f148;
long DAT_0040f1c8;
long DAT_0040f219;
long DAT_0040f231;
long DAT_0040f232;
long DAT_0040f250;
long DAT_0040f254;
long DAT_0040f260;
long DAT_0040f2d6;
long DAT_00412b9a;
long DAT_00611000;
long DAT_00611010;
long DAT_00611020;
long DAT_00611408;
long DAT_00611470;
long DAT_00611474;
long DAT_00611478;
long DAT_0061147c;
long DAT_00611480;
long DAT_00611490;
long DAT_006114a0;
long DAT_006114a8;
long DAT_006114c0;
long DAT_006114c8;
long DAT_00611530;
long DAT_00611538;
long DAT_00611540;
long DAT_00611548;
long DAT_00611550;
long DAT_00611558;
long DAT_006116f8;
long DAT_00611700;
long DAT_00611708;
long DAT_00611718;
long DAT_6f612ba0;
long DAT_6f612c68;
long fde_0040fe40;
long int6;
long int7;
long null_ARRAY_0040e4c0;
long null_ARRAY_0040f6e0;
long null_ARRAY_0040f910;
long null_ARRAY_00611460;
long null_ARRAY_006114e0;
long null_ARRAY_00611510;
long null_ARRAY_00611580;
long null_ARRAY_00611680;
long null_ARRAY_006116c0;
long PTR_DAT_00611400;
long PTR_null_ARRAY_00611458;
long register0x00000020;
long stack0x00000008;
void
FUN_00401fce (uint uParm1)
{
  int iVar1;
  long lVar2;
  undefined8 uVar3;
  undefined **ppuVar4;
  undefined *puVar5;
  undefined *local_88;
  char *local_80;
  undefined8 local_78;
  char *local_70;
  char *local_68;
  char *local_60;
  char *local_58;
  char *local_50;
  char *local_48;
  char *local_40;
  char *local_38;
  char *local_30;
  undefined8 local_28;
  undefined8 local_20;

  ppuVar4 = &local_88;
  if (uParm1 != 0)
    {
      func_0x00401810 (DAT_006114a0,
		       "Try \'%s --help\' for more information.\n",
		       DAT_00611558);
      goto LAB_004021c6;
    }
  func_0x00401640
    ("Usage: %s [OPTION]... LAST\n  or:  %s [OPTION]... FIRST LAST\n  or:  %s [OPTION]... FIRST INCREMENT LAST\n",
     DAT_00611558, DAT_00611558, DAT_00611558);
  uVar3 = DAT_00611480;
  func_0x00401820
    ("Print numbers from FIRST to LAST, in steps of INCREMENT.\n",
     DAT_00611480);
  func_0x00401820
    ("\nMandatory arguments to long options are mandatory for short options too.\n",
     uVar3);
  func_0x00401820
    ("  -f, --format=FORMAT      use printf style floating-point FORMAT\n  -s, --separator=STRING   use STRING to separate numbers (default: \\n)\n  -w, --equal-width        equalize width by padding with leading zeroes\n",
     uVar3);
  func_0x00401820 ("      --help     display this help and exit\n", uVar3);
  func_0x00401820 ("      --version  output version information and exit\n",
		   uVar3);
  func_0x00401820
    ("\nIf FIRST or INCREMENT is omitted, it defaults to 1.  That is, an\nomitted INCREMENT defaults to 1 even when LAST is smaller than FIRST.\nThe sequence of numbers ends when the sum of the current number and\nINCREMENT would become greater than LAST.\nFIRST, INCREMENT, and LAST are interpreted as floating point values.\nINCREMENT is usually positive if FIRST is smaller than LAST, and\nINCREMENT is usually negative if FIRST is greater than LAST.\nINCREMENT must not be 0; none of FIRST, INCREMENT and LAST may be NaN.\n",
     uVar3);
  func_0x00401820
    ("FORMAT must be suitable for printing one argument of type \'double\';\nit defaults to %.PRECf if FIRST, INCREMENT, and LAST are all fixed point\ndecimal numbers with maximum precision PREC, and to %g otherwise.\n",
     uVar3);
  local_88 = &DAT_0040dc5f;
  local_80 = "test invocation";
  local_78 = 0x40dcc2;
  local_70 = "Multi-call invocation";
  local_68 = "sha224sum";
  local_60 = "sha2 utilities";
  local_58 = "sha256sum";
  local_50 = "sha2 utilities";
  local_48 = "sha384sum";
  local_40 = "sha2 utilities";
  local_38 = "sha512sum";
  local_30 = "sha2 utilities";
  local_28 = 0;
  local_20 = 0;
  puVar5 = &DAT_0040dc5f;
  do
    {
      iVar1 = func_0x004018f0 (&DAT_0040dc5b, puVar5);
      if (iVar1 == 0)
	break;
      ppuVar4 = ppuVar4 + 2;
      puVar5 = *ppuVar4;
    }
  while (puVar5 != (undefined *) 0x0);
  puVar5 = ppuVar4[1];
  if (puVar5 == (undefined *) 0x0)
    {
      func_0x00401640 ("\n%s online help: <%s>\n", "GNU coreutils",
		       "https://www.gnu.org/software/coreutils/");
      lVar2 = func_0x00401960 (5, 0);
      if (lVar2 == 0)
	goto LAB_004021cd;
      iVar1 = func_0x00401870 (lVar2, &DAT_0040dce3, 3);
      if (iVar1 == 0)
	{
	  func_0x00401640 ("Full documentation at: <%s%s>\n",
			   "https://www.gnu.org/software/coreutils/",
			   &DAT_0040dc5b);
	  puVar5 = &DAT_0040dc5b;
	  uVar3 = 0x40dc7b;
	  goto LAB_004021b4;
	}
      puVar5 = &DAT_0040dc5b;
    LAB_00402172:
      ;
      func_0x00401640
	("Report %s translation bugs to <https://translationproject.org/team/>\n",
	 &DAT_0040dc5b);
    }
  else
    {
      func_0x00401640 ("\n%s online help: <%s>\n", "GNU coreutils",
		       "https://www.gnu.org/software/coreutils/");
      lVar2 = func_0x00401960 (5, 0);
      if ((lVar2 != 0)
	  && (iVar1 = func_0x00401870 (lVar2, &DAT_0040dce3, 3), iVar1 != 0))
	goto LAB_00402172;
    }
  func_0x00401640 ("Full documentation at: <%s%s>\n",
		   "https://www.gnu.org/software/coreutils/", &DAT_0040dc5b);
  uVar3 = 0x40f24f;
  if (puVar5 == &DAT_0040dc5b)
    {
      uVar3 = 0x40dc7b;
    }
LAB_004021b4:
  ;
  do
    {
      func_0x00401640
	("or available locally via: info \'(coreutils) %s%s\'\n", puVar5,
	 uVar3);
    LAB_004021c6:
      ;
      func_0x004019b0 ((ulong) uParm1);
    LAB_004021cd:
      ;
      func_0x00401640 ("Full documentation at: <%s%s>\n",
		       "https://www.gnu.org/software/coreutils/",
		       &DAT_0040dc5b);
      puVar5 = &DAT_0040dc5b;
      uVar3 = 0x40dc7b;
    }
  while (true);
}

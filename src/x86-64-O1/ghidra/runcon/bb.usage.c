
#include "runcon.h"

long null_ARRAY_0060f66_0_8_;
long null_ARRAY_0060f66_8_8_;
long null_ARRAY_0060f84_0_8_;
long null_ARRAY_0060f84_16_8_;
long null_ARRAY_0060f84_24_8_;
long null_ARRAY_0060f84_32_8_;
long null_ARRAY_0060f84_40_8_;
long null_ARRAY_0060f84_48_8_;
long null_ARRAY_0060f84_8_8_;
long null_ARRAY_0060f88_0_4_;
long null_ARRAY_0060f88_16_8_;
long null_ARRAY_0060f88_4_4_;
long null_ARRAY_0060f88_8_4_;
long local_68_2_2_;
long local_6b_1_3_;
long local_6c_4_4_;
long local_6e_4_4_;
long local_6f_4_4_;
long local_70_0_2_;
long local_70_4_4_;
long local_71_0_1_;
long local_71_4_4_;
long local_72_4_4_;
long DAT_00000010;
long DAT_0040c907;
long DAT_0040c98b;
long DAT_0040cfd8;
long DAT_0040cfdc;
long DAT_0040cfe0;
long DAT_0040cfe3;
long DAT_0040cfe5;
long DAT_0040cfe9;
long DAT_0040cfed;
long DAT_0040d76b;
long DAT_0040db15;
long DAT_0040dc19;
long DAT_0040dc1f;
long DAT_0040dc31;
long DAT_0040dc32;
long DAT_0040dc50;
long DAT_0040dc54;
long DAT_0040dcde;
long DAT_0060f240;
long DAT_0060f250;
long DAT_0060f260;
long DAT_0060f608;
long DAT_0060f670;
long DAT_0060f674;
long DAT_0060f678;
long DAT_0060f67c;
long DAT_0060f680;
long DAT_0060f690;
long DAT_0060f6a0;
long DAT_0060f6a8;
long DAT_0060f6c0;
long DAT_0060f6c8;
long DAT_0060f710;
long DAT_0060f718;
long DAT_0060f720;
long DAT_0060f8b8;
long DAT_0060f8c0;
long DAT_0060f8c8;
long DAT_0060f8d8;
long fde_0040e808;
long null_ARRAY_0040ce80;
long null_ARRAY_0040e100;
long null_ARRAY_0040e340;
long null_ARRAY_0060f660;
long null_ARRAY_0060f6e0;
long null_ARRAY_0060f740;
long null_ARRAY_0060f840;
long null_ARRAY_0060f880;
long PTR_DAT_0060f600;
long PTR_null_ARRAY_0060f658;
void
FUN_0040199e (uint uParm1)
{
  int iVar1;
  long lVar2;
  undefined8 uVar3;
  undefined **ppuVar4;
  char *pcVar5;
  undefined *puVar6;
  undefined *local_88;
  char *local_80;
  undefined8 local_78;
  char *local_70;
  char *local_68;
  char *local_60;
  char *local_58;
  char *local_50;
  char *local_48;
  char *local_40;
  char *local_38;
  char *local_30;
  undefined8 local_28;
  undefined8 local_20;

  ppuVar4 = &local_88;
  if (uParm1 != 0)
    {
      func_0x00401590 (DAT_0060f6a0,
		       "Try \'%s --help\' for more information.\n",
		       DAT_0060f720);
      goto LAB_00401b79;
    }
  func_0x00401420
    ("Usage: %s CONTEXT COMMAND [args]\n  or:  %s [ -c ] [-u USER] [-r ROLE] [-t TYPE] [-l RANGE] COMMAND [args]\n",
     DAT_0060f720, DAT_0060f720);
  uVar3 = DAT_0060f680;
  func_0x004015a0
    ("Run a program in a different SELinux security context.\nWith neither CONTEXT nor COMMAND, print the current security context.\n",
     DAT_0060f680);
  func_0x004015a0
    ("\nMandatory arguments to long options are mandatory for short options too.\n",
     uVar3);
  func_0x004015a0
    ("  CONTEXT            Complete security context\n  -c, --compute      compute process transition context before modifying\n  -t, --type=TYPE    type (for same role as parent)\n  -u, --user=USER    user identity\n  -r, --role=ROLE    role\n  -l, --range=RANGE  levelrange\n\n",
     uVar3);
  func_0x004015a0 ("      --help     display this help and exit\n", uVar3);
  func_0x004015a0 ("      --version  output version information and exit\n",
		   uVar3);
  local_88 = &DAT_0040c907;
  local_80 = "test invocation";
  local_78 = 0x40c96a;
  local_70 = "Multi-call invocation";
  local_68 = "sha224sum";
  local_60 = "sha2 utilities";
  local_58 = "sha256sum";
  local_50 = "sha2 utilities";
  local_48 = "sha384sum";
  local_40 = "sha2 utilities";
  local_38 = "sha512sum";
  local_30 = "sha2 utilities";
  local_28 = 0;
  local_20 = 0;
  puVar6 = &DAT_0040c907;
  do
    {
      iVar1 = func_0x00401670 ("runcon", puVar6);
      if (iVar1 == 0)
	break;
      ppuVar4 = ppuVar4 + 2;
      puVar6 = *ppuVar4;
    }
  while (puVar6 != (undefined *) 0x0);
  pcVar5 = ppuVar4[1];
  if (pcVar5 == (char *) 0x0)
    {
      func_0x00401420 ("\n%s online help: <%s>\n", "GNU coreutils",
		       "https://www.gnu.org/software/coreutils/");
      lVar2 = func_0x004016d0 (5, 0);
      if (lVar2 == 0)
	goto LAB_00401b80;
      iVar1 = func_0x004015f0 (lVar2, &DAT_0040c98b, 3);
      if (iVar1 == 0)
	{
	  func_0x00401420 ("Full documentation at: <%s%s>\n",
			   "https://www.gnu.org/software/coreutils/",
			   "runcon");
	  pcVar5 = "runcon";
	  uVar3 = 0x40c923;
	  goto LAB_00401b67;
	}
      pcVar5 = "runcon";
    LAB_00401b25:
      ;
      func_0x00401420
	("Report %s translation bugs to <https://translationproject.org/team/>\n",
	 "runcon");
    }
  else
    {
      func_0x00401420 ("\n%s online help: <%s>\n", "GNU coreutils",
		       "https://www.gnu.org/software/coreutils/");
      lVar2 = func_0x004016d0 (5, 0);
      if ((lVar2 != 0)
	  && (iVar1 = func_0x004015f0 (lVar2, &DAT_0040c98b, 3), iVar1 != 0))
	goto LAB_00401b25;
    }
  func_0x00401420 ("Full documentation at: <%s%s>\n",
		   "https://www.gnu.org/software/coreutils/", "runcon");
  uVar3 = 0x40dc4f;
  if (pcVar5 == "runcon")
    {
      uVar3 = 0x40c923;
    }
LAB_00401b67:
  ;
  do
    {
      func_0x00401420
	("or available locally via: info \'(coreutils) %s%s\'\n", pcVar5,
	 uVar3);
    LAB_00401b79:
      ;
      func_0x00401710 ((ulong) uParm1);
    LAB_00401b80:
      ;
      func_0x00401420 ("Full documentation at: <%s%s>\n",
		       "https://www.gnu.org/software/coreutils/", "runcon");
      pcVar5 = "runcon";
      uVar3 = 0x40c923;
    }
  while (true);
}

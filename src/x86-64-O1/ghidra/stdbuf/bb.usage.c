
#include "stdbuf.h"

long null_ARRAY_0061146_0_8_;
long null_ARRAY_0061146_8_8_;
long null_ARRAY_0061170_0_8_;
long null_ARRAY_0061170_16_8_;
long null_ARRAY_0061170_24_8_;
long null_ARRAY_0061170_32_8_;
long null_ARRAY_0061170_40_8_;
long null_ARRAY_0061170_48_8_;
long null_ARRAY_0061170_8_8_;
long null_ARRAY_0061174_0_4_;
long null_ARRAY_0061174_16_8_;
long null_ARRAY_0061174_4_4_;
long null_ARRAY_0061174_8_4_;
long local_68_2_2_;
long local_6b_1_3_;
long local_6c_4_4_;
long local_6e_4_4_;
long local_6f_4_4_;
long local_70_0_2_;
long local_70_4_4_;
long local_71_0_1_;
long local_71_4_4_;
long local_72_4_4_;
long DAT_00000010;
long DAT_0040db64;
long DAT_0040dbe8;
long DAT_0040dc22;
long DAT_0040dc28;
long DAT_0040dc43;
long DAT_0040dc48;
long DAT_0040dc62;
long DAT_0040e400;
long DAT_0040e558;
long DAT_0040e55c;
long DAT_0040e560;
long DAT_0040e563;
long DAT_0040e565;
long DAT_0040e569;
long DAT_0040e56d;
long DAT_0040eceb;
long DAT_0040f408;
long DAT_0040f511;
long DAT_0040f517;
long DAT_0040f529;
long DAT_0040f52a;
long DAT_0040f548;
long DAT_0040f54c;
long DAT_0040f5d6;
long DAT_00611000;
long DAT_00611010;
long DAT_00611020;
long DAT_00611408;
long DAT_00611470;
long DAT_00611474;
long DAT_00611478;
long DAT_0061147c;
long DAT_00611480;
long DAT_006114c0;
long DAT_006114d0;
long DAT_006114e0;
long DAT_006114e8;
long DAT_00611500;
long DAT_00611508;
long DAT_006115c8;
long DAT_006115d0;
long DAT_006115d8;
long DAT_006115e0;
long DAT_00611778;
long DAT_00611780;
long DAT_00611788;
long DAT_00611790;
long DAT_006117a0;
long fde_004101d0;
long null_ARRAY_0040e440;
long null_ARRAY_0040f9e0;
long null_ARRAY_0040fb60;
long null_ARRAY_0040fc50;
long null_ARRAY_00611460;
long null_ARRAY_00611520;
long null_ARRAY_00611580;
long null_ARRAY_00611600;
long null_ARRAY_00611700;
long null_ARRAY_00611740;
long PTR_DAT_00611400;
long PTR_null_ARRAY_00611458;
void
FUN_00401c9e (uint uParm1)
{
  int iVar1;
  long lVar2;
  undefined8 uVar3;
  undefined **ppuVar4;
  char *pcVar5;
  undefined *puVar6;
  undefined *local_88;
  char *local_80;
  undefined8 local_78;
  char *local_70;
  char *local_68;
  char *local_60;
  char *local_58;
  char *local_50;
  char *local_48;
  char *local_40;
  char *local_38;
  char *local_30;
  undefined8 local_28;
  undefined8 local_20;

  ppuVar4 = &local_88;
  if (uParm1 != 0)
    {
      func_0x00401830 (DAT_006114e0,
		       "Try \'%s --help\' for more information.\n",
		       DAT_006115e0);
      goto LAB_00401eaa;
    }
  func_0x00401690 ("Usage: %s OPTION... COMMAND\n", DAT_006115e0);
  uVar3 = DAT_00611480;
  func_0x00401840
    ("Run COMMAND, with modified buffering operations for its standard streams.\n",
     DAT_00611480);
  func_0x00401840
    ("\nMandatory arguments to long options are mandatory for short options too.\n",
     uVar3);
  func_0x00401840
    ("  -i, --input=MODE   adjust standard input stream buffering\n  -o, --output=MODE  adjust standard output stream buffering\n  -e, --error=MODE   adjust standard error stream buffering\n",
     uVar3);
  func_0x00401840 ("      --help     display this help and exit\n", uVar3);
  func_0x00401840 ("      --version  output version information and exit\n",
		   uVar3);
  func_0x00401840
    ("\nIf MODE is \'L\' the corresponding stream will be line buffered.\nThis option is invalid with standard input.\n",
     uVar3);
  func_0x00401840
    ("\nIf MODE is \'0\' the corresponding stream will be unbuffered.\n",
     uVar3);
  func_0x00401840
    ("\nOtherwise MODE is a number which may be followed by one of the following:\nKB 1000, K 1024, MB 1000*1000, M 1024*1024, and so on for G, T, P, E, Z, Y.\nIn this case the corresponding stream will be fully buffered with the buffer\nsize set to MODE bytes.\n",
     uVar3);
  func_0x00401840
    ("\nNOTE: If COMMAND adjusts the buffering of its standard streams (\'tee\' does\nfor example) then that will override corresponding changes by \'stdbuf\'.\nAlso some filters (like \'dd\' and \'cat\' etc.) don\'t use streams for I/O,\nand are thus unaffected by \'stdbuf\' settings.\n",
     uVar3);
  local_88 = &DAT_0040db64;
  local_80 = "test invocation";
  local_78 = 0x40dbc7;
  local_70 = "Multi-call invocation";
  local_68 = "sha224sum";
  local_60 = "sha2 utilities";
  local_58 = "sha256sum";
  local_50 = "sha2 utilities";
  local_48 = "sha384sum";
  local_40 = "sha2 utilities";
  local_38 = "sha512sum";
  local_30 = "sha2 utilities";
  local_28 = 0;
  local_20 = 0;
  puVar6 = &DAT_0040db64;
  do
    {
      iVar1 = func_0x00401930 ("stdbuf", puVar6);
      if (iVar1 == 0)
	break;
      ppuVar4 = ppuVar4 + 2;
      puVar6 = *ppuVar4;
    }
  while (puVar6 != (undefined *) 0x0);
  pcVar5 = ppuVar4[1];
  if (pcVar5 == (char *) 0x0)
    {
      func_0x00401690 ("\n%s online help: <%s>\n", "GNU coreutils",
		       "https://www.gnu.org/software/coreutils/");
      lVar2 = func_0x00401990 (5, 0);
      if (lVar2 == 0)
	goto LAB_00401eb1;
      iVar1 = func_0x00401890 (lVar2, &DAT_0040dbe8, 3);
      if (iVar1 == 0)
	{
	  func_0x00401690 ("Full documentation at: <%s%s>\n",
			   "https://www.gnu.org/software/coreutils/",
			   "stdbuf");
	  pcVar5 = "stdbuf";
	  uVar3 = 0x40db80;
	  goto LAB_00401e98;
	}
      pcVar5 = "stdbuf";
    LAB_00401e56:
      ;
      func_0x00401690
	("Report %s translation bugs to <https://translationproject.org/team/>\n",
	 "stdbuf");
    }
  else
    {
      func_0x00401690 ("\n%s online help: <%s>\n", "GNU coreutils",
		       "https://www.gnu.org/software/coreutils/");
      lVar2 = func_0x00401990 (5, 0);
      if ((lVar2 != 0)
	  && (iVar1 = func_0x00401890 (lVar2, &DAT_0040dbe8, 3), iVar1 != 0))
	goto LAB_00401e56;
    }
  func_0x00401690 ("Full documentation at: <%s%s>\n",
		   "https://www.gnu.org/software/coreutils/", "stdbuf");
  uVar3 = 0x40f547;
  if (pcVar5 == "stdbuf")
    {
      uVar3 = 0x40db80;
    }
LAB_00401e98:
  ;
  do
    {
      func_0x00401690
	("or available locally via: info \'(coreutils) %s%s\'\n", pcVar5,
	 uVar3);
    LAB_00401eaa:
      ;
      func_0x004019f0 ((ulong) uParm1);
    LAB_00401eb1:
      ;
      func_0x00401690 ("Full documentation at: <%s%s>\n",
		       "https://www.gnu.org/software/coreutils/", "stdbuf");
      pcVar5 = "stdbuf";
      uVar3 = 0x40db80;
    }
  while (true);
}

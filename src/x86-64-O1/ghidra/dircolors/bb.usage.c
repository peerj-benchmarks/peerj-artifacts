
#include "dircolors.h"

long null_ARRAY_006154a_0_8_;
long null_ARRAY_006154a_8_8_;
long null_ARRAY_006155c_16_8_;
long null_ARRAY_006155c_24_8_;
long null_ARRAY_006155c_32_8_;
long null_ARRAY_006155c_48_8_;
long null_ARRAY_006155c_8_8_;
long null_ARRAY_0061574_0_8_;
long null_ARRAY_0061574_16_8_;
long null_ARRAY_0061574_24_8_;
long null_ARRAY_0061574_32_8_;
long null_ARRAY_0061574_40_8_;
long null_ARRAY_0061574_48_8_;
long null_ARRAY_0061574_8_8_;
long null_ARRAY_0061578_0_4_;
long null_ARRAY_0061578_16_8_;
long null_ARRAY_0061578_4_4_;
long null_ARRAY_0061578_8_4_;
long local_5_0_4_;
long local_5_4_4_;
long local_6_4_4_;
long local_68_2_2_;
long local_6b_1_3_;
long local_6c_4_4_;
long local_6e_4_4_;
long local_6f_4_4_;
long local_70_0_2_;
long local_70_4_4_;
long local_71_0_1_;
long local_71_4_4_;
long local_72_4_4_;
long DAT_00000010;
long DAT_004103d7;
long DAT_0041041b;
long DAT_0041049f;
long DAT_004104e6;
long DAT_00410501;
long DAT_00410502;
long DAT_004105ce;
long DAT_00411ecd;
long DAT_00411f30;
long DAT_00411f34;
long DAT_00411f38;
long DAT_00411f3b;
long DAT_00411f3d;
long DAT_00411f41;
long DAT_00411f45;
long DAT_004126eb;
long DAT_00412a95;
long DAT_00413211;
long DAT_00413212;
long DAT_00413230;
long DAT_004132b6;
long DAT_00615000;
long DAT_00615010;
long DAT_00615020;
long DAT_00615448;
long DAT_006154b0;
long DAT_006154b4;
long DAT_006154b8;
long DAT_006154bc;
long DAT_00615500;
long DAT_00615508;
long DAT_00615510;
long DAT_00615520;
long DAT_00615528;
long DAT_00615540;
long DAT_00615548;
long DAT_00615618;
long DAT_00615620;
long DAT_00615628;
long DAT_00615778;
long DAT_006157b8;
long DAT_006157c0;
long DAT_006157c8;
long DAT_006157d8;
long fde_00413ea8;
long null_ARRAY_00410b00;
long null_ARRAY_00410c00;
long null_ARRAY_00410d40;
long null_ARRAY_004136c0;
long null_ARRAY_00413900;
long null_ARRAY_006154a0;
long null_ARRAY_00615560;
long null_ARRAY_006155c0;
long null_ARRAY_00615640;
long null_ARRAY_00615740;
long null_ARRAY_00615780;
long PTR_DAT_00615440;
long PTR_FUN_006154c0;
long PTR_null_ARRAY_00615498;
void
FUN_0040263a (uint uParm1)
{
  int iVar1;
  long lVar2;
  undefined8 uVar3;
  undefined **ppuVar4;
  char *pcVar5;
  undefined *puVar6;
  undefined *local_88;
  char *local_80;
  undefined8 local_78;
  char *local_70;
  char *local_68;
  char *local_60;
  char *local_58;
  char *local_50;
  char *local_48;
  char *local_40;
  char *local_38;
  char *local_30;
  undefined8 local_28;
  undefined8 local_20;

  ppuVar4 = &local_88;
  if (uParm1 != 0)
    {
      func_0x00401a30 (DAT_00615520,
		       "Try \'%s --help\' for more information.\n",
		       DAT_00615628);
      goto LAB_00402805;
    }
  func_0x00401840 ("Usage: %s [OPTION]... [FILE]\n", DAT_00615628);
  uVar3 = DAT_00615500;
  func_0x00401a40
    ("Output commands to set the LS_COLORS environment variable.\n\nDetermine format of output:\n  -b, --sh, --bourne-shell    output Bourne shell code to set LS_COLORS\n  -c, --csh, --c-shell        output C shell code to set LS_COLORS\n  -p, --print-database        output defaults\n",
     DAT_00615500);
  func_0x00401a40 ("      --help     display this help and exit\n", uVar3);
  func_0x00401a40 ("      --version  output version information and exit\n",
		   uVar3);
  func_0x00401a40
    ("\nIf FILE is specified, read it to determine which colors to use for which\nfile types and extensions.  Otherwise, a precompiled database is used.\nFor details on the format of these files, run \'dircolors --print-database\'.\n",
     uVar3);
  local_88 = &DAT_0041041b;
  local_80 = "test invocation";
  local_78 = 0x41047e;
  local_70 = "Multi-call invocation";
  local_68 = "sha224sum";
  local_60 = "sha2 utilities";
  local_58 = "sha256sum";
  local_50 = "sha2 utilities";
  local_48 = "sha384sum";
  local_40 = "sha2 utilities";
  local_38 = "sha512sum";
  local_30 = "sha2 utilities";
  local_28 = 0;
  local_20 = 0;
  puVar6 = &DAT_0041041b;
  do
    {
      iVar1 = func_0x00401b60 ("dircolors", puVar6);
      if (iVar1 == 0)
	break;
      ppuVar4 = ppuVar4 + 2;
      puVar6 = *ppuVar4;
    }
  while (puVar6 != (undefined *) 0x0);
  pcVar5 = ppuVar4[1];
  if (pcVar5 == (char *) 0x0)
    {
      func_0x00401840 ("\n%s online help: <%s>\n", "GNU coreutils",
		       "https://www.gnu.org/software/coreutils/");
      lVar2 = func_0x00401bc0 (5, 0);
      if (lVar2 == 0)
	goto LAB_0040280c;
      iVar1 = func_0x00401ab0 (lVar2, &DAT_0041049f, 3);
      if (iVar1 == 0)
	{
	  func_0x00401840 ("Full documentation at: <%s%s>\n",
			   "https://www.gnu.org/software/coreutils/",
			   "dircolors");
	  pcVar5 = "dircolors";
	  uVar3 = 0x410437;
	  goto LAB_004027f3;
	}
      pcVar5 = "dircolors";
    LAB_004027b1:
      ;
      func_0x00401840
	("Report %s translation bugs to <https://translationproject.org/team/>\n",
	 "dircolors");
    }
  else
    {
      func_0x00401840 ("\n%s online help: <%s>\n", "GNU coreutils",
		       "https://www.gnu.org/software/coreutils/");
      lVar2 = func_0x00401bc0 (5, 0);
      if ((lVar2 != 0)
	  && (iVar1 = func_0x00401ab0 (lVar2, &DAT_0041049f, 3), iVar1 != 0))
	goto LAB_004027b1;
    }
  func_0x00401840 ("Full documentation at: <%s%s>\n",
		   "https://www.gnu.org/software/coreutils/", "dircolors");
  uVar3 = 0x41322f;
  if (pcVar5 == "dircolors")
    {
      uVar3 = 0x410437;
    }
LAB_004027f3:
  ;
  do
    {
      func_0x00401840
	("or available locally via: info \'(coreutils) %s%s\'\n", pcVar5,
	 uVar3);
    LAB_00402805:
      ;
      func_0x00401c20 ((ulong) uParm1);
    LAB_0040280c:
      ;
      func_0x00401840 ("Full documentation at: <%s%s>\n",
		       "https://www.gnu.org/software/coreutils/",
		       "dircolors");
      pcVar5 = "dircolors";
      uVar3 = 0x410437;
    }
  while (true);
}

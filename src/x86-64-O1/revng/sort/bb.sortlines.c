typedef unsigned char __u_char;
typedef unsigned short __u_short;
typedef unsigned int __u_int;
typedef unsigned long __u_long;
typedef signed char __int8_t;
typedef unsigned char __uint8_t;
typedef short __int16_t;
typedef unsigned short __uint16_t;
typedef int __int32_t;
typedef unsigned int __uint32_t;
typedef long __int64_t;
typedef unsigned long __uint64_t;
typedef __int8_t __int_least8_t;
typedef __uint8_t __uint_least8_t;
typedef __int16_t __int_least16_t;
typedef __uint16_t __uint_least16_t;
typedef __int32_t __int_least32_t;
typedef __uint32_t __uint_least32_t;
typedef __int64_t __int_least64_t;
typedef __uint64_t __uint_least64_t;
typedef long __quad_t;
typedef unsigned long __u_quad_t;
typedef long __intmax_t;
typedef unsigned long __uintmax_t;
typedef unsigned long __dev_t;
typedef unsigned int __uid_t;
typedef unsigned int __gid_t;
typedef unsigned long __ino_t;
typedef unsigned long __ino64_t;
typedef unsigned int __mode_t;
typedef unsigned long __nlink_t;
typedef long __off_t;
typedef long __off64_t;
typedef int __pid_t;
typedef struct {
    int __val[2];
} __fsid_t;
typedef long __clock_t;
typedef unsigned long __rlim_t;
typedef unsigned long __rlim64_t;
typedef unsigned int __id_t;
typedef long __time_t;
typedef unsigned int __useconds_t;
typedef long __suseconds_t;
typedef int __daddr_t;
typedef int __key_t;
typedef int __clockid_t;
typedef void *__timer_t;
typedef long __blksize_t;
typedef long __blkcnt_t;
typedef long __blkcnt64_t;
typedef unsigned long __fsblkcnt_t;
typedef unsigned long __fsblkcnt64_t;
typedef unsigned long __fsfilcnt_t;
typedef unsigned long __fsfilcnt64_t;
typedef long __fsword_t;
typedef long __ssize_t;
typedef long __syscall_slong_t;
typedef unsigned long __syscall_ulong_t;
typedef __off64_t __loff_t;
typedef char *__caddr_t;
typedef long __intptr_t;
typedef unsigned int __socklen_t;
typedef int __sig_atomic_t;
typedef __int8_t int8_t;
typedef __int16_t int16_t;
typedef __int32_t int32_t;
typedef __int64_t int64_t;
typedef __uint8_t uint8_t;
typedef __uint16_t uint16_t;
typedef __uint32_t uint32_t;
typedef __uint64_t uint64_t;
typedef __int_least8_t int_least8_t;
typedef __int_least16_t int_least16_t;
typedef __int_least32_t int_least32_t;
typedef __int_least64_t int_least64_t;
typedef __uint_least8_t uint_least8_t;
typedef __uint_least16_t uint_least16_t;
typedef __uint_least32_t uint_least32_t;
typedef __uint_least64_t uint_least64_t;
typedef signed char int_fast8_t;
typedef long int_fast16_t;
typedef long int_fast32_t;
typedef long int_fast64_t;
typedef unsigned char uint_fast8_t;
typedef unsigned long uint_fast16_t;
typedef unsigned long uint_fast32_t;
typedef unsigned long uint_fast64_t;
typedef long intptr_t;
typedef unsigned long uintptr_t;
typedef __intmax_t intmax_t;
typedef __uintmax_t uintmax_t;
typedef _Bool bool;
struct indirect_placeholder_125_ret_type;
struct indirect_placeholder_125_ret_type {
    uint64_t field_0;
    uint64_t field_1;
};
extern uint64_t revng_init_local_sp(uint64_t param_0);
extern uint64_t init_rbx(void);
extern void indirect_placeholder(void);
extern uint64_t init_rbp(void);
extern uint64_t init_r14(void);
extern uint64_t init_r13(void);
extern uint64_t init_r12(void);
extern uint64_t helper_cc_compute_all_wrapper(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint32_t param_3);
extern uint64_t init_r15(void);
extern void indirect_placeholder_3(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint64_t param_3);
extern void indirect_placeholder_8(uint64_t param_0, uint64_t param_1);
extern uint64_t indirect_placeholder_1(uint64_t param_0, uint64_t param_1);
extern void indirect_placeholder_4(uint64_t param_0, uint64_t param_1, uint64_t param_2);
extern void indirect_placeholder_15(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint64_t param_3, uint64_t param_4, uint64_t param_5);
extern struct indirect_placeholder_125_ret_type indirect_placeholder_125(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint64_t param_3, uint64_t param_4, uint64_t param_5, uint64_t param_6);
void bb_sortlines(uint64_t rdx, uint64_t rdi, uint64_t rcx, uint64_t rsi, uint64_t r9, uint64_t r8) {
    uint64_t var_0;
    uint64_t var_1;
    uint64_t var_2;
    uint64_t var_3;
    uint64_t var_4;
    uint64_t var_5;
    uint64_t var_6;
    uint64_t var_7;
    uint64_t *var_8;
    uint64_t var_9;
    uint64_t *var_10;
    uint64_t var_11;
    uint64_t var_12;
    uint64_t local_sp_9;
    uint64_t rbp_6;
    uint64_t var_109;
    uint64_t var_110;
    uint64_t local_sp_0;
    uint64_t local_sp_11;
    uint64_t local_sp_8;
    uint64_t r14_1;
    uint64_t var_29;
    uint64_t var_107;
    uint64_t var_108;
    uint64_t var_111;
    uint64_t rdi2_2;
    uint64_t var_95;
    uint64_t rbp_5;
    uint64_t local_sp_5;
    uint64_t local_sp_6;
    uint64_t rdi2_1;
    uint64_t var_101;
    uint64_t rbp_4;
    uint64_t local_sp_4;
    uint64_t r13_1_ph;
    uint64_t r13_2;
    uint64_t local_sp_1;
    uint64_t local_sp_3;
    uint64_t var_85;
    uint64_t var_86;
    uint64_t var_87;
    uint64_t var_88;
    uint64_t var_60;
    uint64_t var_61;
    uint64_t r12_2;
    uint64_t local_sp_2;
    uint64_t var_62;
    uint64_t var_63;
    uint64_t var_67;
    uint64_t rbp_7;
    uint64_t *var_44;
    uint64_t *var_45;
    uint64_t var_46;
    uint64_t **var_47;
    uint64_t var_48;
    uint64_t *var_49;
    uint64_t *var_50;
    uint64_t rbp_0_in;
    uint64_t r13_0;
    uint64_t rcx3_0;
    uint64_t rbp_0;
    uint64_t var_51;
    bool var_52;
    uint64_t var_64;
    uint64_t var_53;
    uint64_t var_92;
    uint64_t var_54;
    uint64_t var_55;
    uint64_t var_56;
    uint64_t var_57;
    uint64_t var_58;
    uint64_t var_59;
    uint64_t rcx3_1_ph;
    uint64_t r13_1;
    uint64_t rcx3_1;
    uint64_t rbp_3;
    uint64_t rbp_1;
    uint64_t rdi2_0;
    uint64_t r86_0;
    uint64_t var_69;
    uint64_t var_70;
    uint64_t var_71;
    uint64_t var_72;
    uint64_t r86_2;
    uint64_t rdx1_0;
    uint64_t rbp_2;
    uint64_t r86_1;
    uint64_t var_65;
    uint64_t var_66;
    uint64_t var_68;
    uint64_t var_73;
    uint64_t var_74;
    uint64_t *var_75;
    uint64_t *var_76;
    uint64_t rcx3_2;
    uint64_t var_77;
    bool var_78;
    uint64_t r13_3;
    uint64_t var_79;
    uint64_t var_80;
    uint64_t var_81;
    uint64_t var_82;
    uint64_t var_83;
    uint64_t var_84;
    uint64_t rcx3_3;
    uint64_t r12_0;
    uint64_t var_89;
    uint64_t var_96;
    uint64_t var_97;
    uint64_t var_98;
    uint64_t var_99;
    uint64_t var_100;
    uint64_t var_90;
    uint64_t var_91;
    uint64_t var_93;
    uint64_t var_94;
    uint64_t r13_4;
    uint64_t r12_1;
    uint64_t var_102;
    uint64_t var_103;
    uint64_t *var_104;
    uint64_t *var_105;
    uint64_t var_106;
    uint64_t var_39;
    uint64_t *var_40;
    uint64_t var_41;
    uint32_t var_42;
    uint64_t var_43;
    uint64_t local_sp_7;
    uint64_t r14_0;
    uint64_t var_31;
    uint64_t *var_32;
    uint64_t r13_5;
    struct indirect_placeholder_125_ret_type var_33;
    uint64_t var_34;
    bool var_35;
    uint64_t var_36;
    uint64_t var_37;
    uint64_t *var_38;
    uint64_t var_30;
    uint64_t var_112;
    uint64_t var_113;
    uint64_t var_114;
    uint64_t var_115;
    uint64_t var_116;
    uint64_t var_13;
    uint64_t var_14;
    uint64_t var_15;
    uint64_t var_16;
    uint64_t local_sp_10;
    uint64_t var_17;
    uint64_t var_18;
    uint64_t var_19;
    uint64_t var_20;
    uint64_t var_21;
    uint64_t var_22;
    uint64_t var_23;
    uint64_t var_24;
    uint64_t var_25;
    uint64_t var_26;
    uint64_t var_27;
    uint64_t *var_28;
    unsigned int loop_state_var;
    var_0 = revng_init_local_sp(0UL);
    var_1 = init_rbx();
    var_2 = init_r14();
    var_3 = init_rbp();
    var_4 = init_r13();
    var_5 = init_r12();
    var_6 = init_r15();
    *(uint64_t *)(var_0 + (-8L)) = var_6;
    *(uint64_t *)(var_0 + (-16L)) = var_2;
    *(uint64_t *)(var_0 + (-24L)) = var_4;
    *(uint64_t *)(var_0 + (-32L)) = var_5;
    *(uint64_t *)(var_0 + (-40L)) = var_3;
    *(uint64_t *)(var_0 + (-48L)) = var_1;
    var_7 = var_0 + (-184L);
    *(uint64_t *)(var_0 + (-152L)) = rdx;
    *(uint64_t *)(var_0 + (-128L)) = r9;
    var_8 = (uint64_t *)(rcx + 40UL);
    var_9 = *var_8;
    var_10 = (uint64_t *)(rcx + 48UL);
    var_11 = var_9 + *var_10;
    var_12 = rsi >> 1UL;
    *(uint64_t *)(var_0 + (-120L)) = rdi;
    *(uint64_t *)(var_0 + (-112L)) = var_12;
    *(uint64_t *)(var_0 + (-104L)) = rdx;
    *(uint64_t *)(var_0 + (-96L)) = *(uint64_t *)(rcx + 64UL);
    *(uint64_t *)(var_0 + (-88L)) = r8;
    *(uint64_t *)(var_0 + (-80L)) = r9;
    *(uint64_t *)(var_0 + (-72L)) = *(uint64_t *)(var_0 | 8UL);
    rbp_7 = rcx;
    local_sp_9 = var_7;
    if (!((rsi > 1UL) && (var_11 > 131071UL))) {
        var_13 = var_0 + (-192L);
        *(uint64_t *)var_13 = 4219524UL;
        indirect_placeholder();
        local_sp_9 = var_13;
        if ((uint64_t)(uint32_t)var_11 != 0UL) {
            var_112 = *(uint64_t *)(rcx + 72UL);
            var_113 = rsi - var_12;
            var_114 = rdi - (*var_8 << 5UL);
            *(uint64_t *)(var_0 + (-208L)) = *(uint64_t *)var_0;
            var_115 = *(uint64_t *)(var_0 + (-136L));
            var_116 = *(uint64_t *)(var_0 + (-160L));
            *(uint64_t *)(var_0 + (-216L)) = 4219581UL;
            indirect_placeholder_15(var_116, var_114, var_112, var_113, var_115, r8);
            *(uint64_t *)(var_0 + (-224L)) = 4219599UL;
            indirect_placeholder();
            return;
        }
    }
    var_14 = *var_8;
    var_15 = *var_10;
    var_16 = rdi - (*(uint64_t *)(local_sp_9 + 32UL) << 5UL);
    local_sp_10 = local_sp_9;
    if (var_15 > 1UL) {
        var_17 = var_16 - ((var_14 >> 1UL) << 5UL);
        var_18 = rdi - (var_14 << 5UL);
        var_19 = local_sp_9 + (-8L);
        *(uint64_t *)var_19 = 4219679UL;
        indirect_placeholder_3(var_17, var_18, 0UL, var_15);
        local_sp_10 = var_19;
    }
    local_sp_11 = local_sp_10;
    if (var_14 > 1UL) {
        var_20 = local_sp_10 + (-8L);
        *(uint64_t *)var_20 = 4219704UL;
        indirect_placeholder_3(var_16, rdi, 0UL, var_14);
        local_sp_11 = var_20;
    }
    *(uint64_t *)rcx = rdi;
    var_21 = var_14 << 5UL;
    var_22 = 0UL - var_21;
    var_23 = rdi - var_21;
    *(uint64_t *)(rcx + 8UL) = var_23;
    *(uint64_t *)(rcx + 16UL) = var_23;
    var_24 = var_15 << 5UL;
    var_25 = (var_22 - var_24) + rdi;
    *(uint64_t *)(rcx + 24UL) = var_25;
    var_26 = local_sp_11 + (-8L);
    *(uint64_t *)var_26 = 4219752UL;
    indirect_placeholder_8(r8, rcx);
    var_27 = r8 + 8UL;
    *(uint64_t *)(local_sp_11 + 32UL) = (r8 + 48UL);
    var_28 = (uint64_t *)r8;
    local_sp_8 = var_26;
    r14_1 = var_27;
    r13_5 = var_24;
    r12_2 = var_25;
    while (1U)
        {
            var_29 = local_sp_8 + (-8L);
            *(uint64_t *)var_29 = 4219773UL;
            indirect_placeholder();
            var_30 = *(uint64_t *)(local_sp_8 + 32UL);
            r13_0 = r13_5;
            r13_2 = r13_5;
            local_sp_7 = var_29;
            r14_0 = r14_1;
            var_31 = *var_28;
            var_32 = (uint64_t *)(local_sp_7 + (-8L));
            *var_32 = 4219799UL;
            var_33 = indirect_placeholder_125(var_30, r14_0, rbp_7, var_31, r13_5, r12_2, r8);
            var_34 = var_33.field_0;
            var_35 = (var_34 == 0UL);
            var_36 = var_33.field_1;
            var_37 = local_sp_7 + (-16L);
            var_38 = (uint64_t *)var_37;
            r12_1 = var_34;
            local_sp_7 = var_37;
            r14_0 = var_36;
            r14_1 = var_36;
            while (!var_35)
                {
                    *var_38 = 4219791UL;
                    indirect_placeholder();
                    var_31 = *var_28;
                    var_32 = (uint64_t *)(local_sp_7 + (-8L));
                    *var_32 = 4219799UL;
                    var_33 = indirect_placeholder_125(var_30, r14_0, rbp_7, var_31, r13_5, r12_2, r8);
                    var_34 = var_33.field_0;
                    var_35 = (var_34 == 0UL);
                    var_36 = var_33.field_1;
                    var_37 = local_sp_7 + (-16L);
                    var_38 = (uint64_t *)var_37;
                    r12_1 = var_34;
                    local_sp_7 = var_37;
                    r14_0 = var_36;
                    r14_1 = var_36;
                }
            *var_38 = 4219818UL;
            indirect_placeholder();
            var_39 = var_34 + 88UL;
            var_40 = (uint64_t *)(local_sp_7 + 8UL);
            *var_40 = var_39;
            var_41 = local_sp_7 + (-24L);
            *(uint64_t *)var_41 = 4219836UL;
            indirect_placeholder();
            *(unsigned char *)(var_34 + 84UL) = (unsigned char)'\x00';
            var_42 = *(uint32_t *)(var_34 + 80UL);
            var_43 = (uint64_t)var_42;
            local_sp_2 = var_41;
            local_sp_3 = var_41;
            if (var_42 == 0U) {
                break;
            }
            var_44 = (uint64_t *)var_34;
            *var_32 = *var_44;
            var_45 = (uint64_t *)(var_34 + 8UL);
            *var_38 = *var_45;
            var_46 = (*var_40 >> (((var_43 << 1UL) + 2UL) & 62UL)) + 1UL;
            rcx3_0 = var_46;
            rcx3_2 = var_46;
            if ((uint64_t)(var_42 & (-2)) == 0UL) {
                *(uint64_t *)(local_sp_7 + 24UL) = var_34;
                var_73 = *(uint64_t *)(local_sp_7 + 32UL);
                var_74 = *(uint64_t *)(local_sp_7 + 168UL);
                var_75 = (uint64_t *)(var_34 + 16UL);
                var_76 = (uint64_t *)(var_34 + 24UL);
                rbp_6 = var_74;
                var_77 = *var_44;
                var_78 = (var_77 == *var_75);
                local_sp_5 = local_sp_3;
                local_sp_6 = local_sp_3;
                rdi2_1 = var_77;
                local_sp_4 = local_sp_3;
                r13_3 = r13_2;
                rcx3_3 = rcx3_2;
                while (!var_78)
                    {
                        var_79 = *var_45;
                        if (var_79 == *var_76) {
                            break;
                        }
                        var_80 = rcx3_2 + (-1L);
                        r13_2 = var_80;
                        rcx3_2 = var_80;
                        r13_3 = var_80;
                        rcx3_3 = var_80;
                        if (rcx3_2 == 0UL) {
                            break;
                        }
                        var_81 = var_79 + (-32L);
                        var_82 = var_77 + (-32L);
                        *(uint64_t *)(local_sp_3 + (-8L)) = 4220325UL;
                        var_83 = indirect_placeholder_1(var_82, var_81);
                        var_84 = helper_cc_compute_all_wrapper(var_83, 0UL, 0UL, 24U);
                        if ((uint64_t)(((unsigned char)(var_84 >> 4UL) ^ (unsigned char)var_84) & '\xc0') == 0UL) {
                            var_87 = *var_45 + (-32L);
                            *var_45 = var_87;
                            var_88 = local_sp_3 + (-16L);
                            *(uint64_t *)var_88 = 4220375UL;
                            indirect_placeholder_4(var_74, var_87, var_73);
                            local_sp_1 = var_88;
                        } else {
                            var_85 = *var_44 + (-32L);
                            *var_44 = var_85;
                            var_86 = local_sp_3 + (-16L);
                            *(uint64_t *)var_86 = 4220350UL;
                            indirect_placeholder_4(var_74, var_85, var_73);
                            local_sp_1 = var_86;
                        }
                        local_sp_3 = local_sp_1;
                        var_77 = *var_44;
                        var_78 = (var_77 == *var_75);
                        local_sp_5 = local_sp_3;
                        local_sp_6 = local_sp_3;
                        rdi2_1 = var_77;
                        local_sp_4 = local_sp_3;
                        r13_3 = r13_2;
                        rcx3_3 = rcx3_2;
                    }
                r12_0 = *(uint64_t *)(local_sp_3 + 48UL);
                var_89 = *(uint64_t *)(r12_0 + 8UL);
                rdi2_2 = var_89;
                rbp_5 = rcx3_3;
                rbp_4 = rcx3_3;
                r13_4 = r13_3;
                r12_1 = r12_0;
                if ((uint64_t)((long)(*(uint64_t *)(local_sp_3 + 8UL) - var_89) >> (long)5UL) == *(uint64_t *)(r12_0 + 48UL)) {
                    if (!(var_78 || (rcx3_3 == 0UL))) {
                        var_96 = *(uint64_t *)(local_sp_3 + 56UL);
                        var_97 = *(uint64_t *)(local_sp_3 + 192UL);
                        r13_4 = var_97;
                        while (1U)
                            {
                                var_98 = rdi2_1 + (-32L);
                                *var_44 = var_98;
                                var_99 = local_sp_4 + (-8L);
                                *(uint64_t *)var_99 = 4220541UL;
                                indirect_placeholder_4(var_97, var_98, var_96);
                                var_100 = *var_44;
                                local_sp_4 = var_99;
                                rdi2_1 = var_100;
                                local_sp_6 = var_99;
                                rbp_6 = rbp_4;
                                if (var_100 != *var_75) {
                                    loop_state_var = 0U;
                                    break;
                                }
                                var_101 = rbp_4 + (-1L);
                                rbp_4 = var_101;
                                rbp_6 = 0UL;
                                if (var_101 == 0UL) {
                                    continue;
                                }
                                loop_state_var = 1U;
                                break;
                            }
                        switch (loop_state_var) {
                          case 0U:
                            {
                                r12_1 = *(uint64_t *)(local_sp_4 + 40UL);
                            }
                            break;
                          case 1U:
                            {
                                r12_1 = *(uint64_t *)(local_sp_4 + 40UL);
                            }
                            break;
                        }
                    }
                } else {
                    if ((uint64_t)((long)(*(uint64_t *)(local_sp_3 + 16UL) - var_77) >> (long)5UL) != *(uint64_t *)(r12_0 + 40UL) & !((var_89 == *(uint64_t *)(r12_0 + 24UL)) || (rcx3_3 == 0UL))) {
                        var_90 = *(uint64_t *)(local_sp_3 + 56UL);
                        var_91 = *(uint64_t *)(local_sp_3 + 192UL);
                        r13_4 = var_91;
                        while (1U)
                            {
                                var_92 = rdi2_2 + (-32L);
                                *var_45 = var_92;
                                var_93 = local_sp_5 + (-8L);
                                *(uint64_t *)var_93 = 4220637UL;
                                indirect_placeholder_4(var_91, var_92, var_90);
                                var_94 = *var_45;
                                local_sp_5 = var_93;
                                rdi2_2 = var_94;
                                local_sp_6 = var_93;
                                rbp_6 = rbp_5;
                                if (var_94 != *var_76) {
                                    loop_state_var = 1U;
                                    break;
                                }
                                var_95 = rbp_5 + (-1L);
                                rbp_5 = var_95;
                                rbp_6 = 0UL;
                                if (var_95 == 0UL) {
                                    continue;
                                }
                                loop_state_var = 0U;
                                break;
                            }
                        switch (loop_state_var) {
                          case 0U:
                            {
                                r12_1 = *(uint64_t *)(local_sp_5 + 40UL);
                            }
                            break;
                          case 1U:
                            {
                                r12_1 = *(uint64_t *)(local_sp_5 + 40UL);
                            }
                            break;
                        }
                    }
                }
            } else {
                var_47 = (uint64_t **)(var_34 + 32UL);
                var_48 = **var_47;
                var_49 = (uint64_t *)(var_34 + 16UL);
                var_50 = (uint64_t *)(var_34 + 24UL);
                rbp_0_in = var_48;
                while (1U)
                    {
                        rbp_0 = rbp_0_in + (-32L);
                        var_51 = *var_44;
                        var_52 = (var_51 == *var_49);
                        local_sp_6 = local_sp_2;
                        r13_1_ph = r13_0;
                        rbp_0_in = rbp_0;
                        rcx3_1_ph = rcx3_0;
                        r13_1 = r13_0;
                        rcx3_1 = rcx3_0;
                        rbp_3 = rbp_0;
                        rdi2_0 = var_51;
                        r86_0 = rbp_0_in;
                        r86_2 = rbp_0_in;
                        r86_1 = rbp_0_in;
                        if (!var_52) {
                            loop_state_var = 1U;
                            break;
                        }
                        var_53 = *var_45;
                        var_64 = var_53;
                        if (var_53 != *var_50) {
                            loop_state_var = 0U;
                            break;
                        }
                        var_54 = rcx3_0 + (-1L);
                        r13_0 = var_54;
                        rcx3_0 = var_54;
                        r13_1_ph = var_54;
                        rcx3_1_ph = var_54;
                        if (rcx3_0 != 0UL) {
                            loop_state_var = 0U;
                            break;
                        }
                        var_55 = var_53 + (-32L);
                        var_56 = var_51 + (-32L);
                        var_57 = local_sp_2 + (-8L);
                        *(uint64_t *)var_57 = 4219948UL;
                        var_58 = indirect_placeholder_1(var_56, var_55);
                        var_59 = helper_cc_compute_all_wrapper(var_58, 0UL, 0UL, 24U);
                        local_sp_2 = var_57;
                        if ((uint64_t)(((unsigned char)(var_59 >> 4UL) ^ (unsigned char)var_59) & '\xc0') == 0UL) {
                            var_62 = *var_45;
                            var_63 = var_62 + (-32L);
                            *var_45 = var_63;
                            *(uint64_t *)rbp_0 = *(uint64_t *)var_63;
                            *(uint64_t *)(rbp_0_in + (-24L)) = *(uint64_t *)(var_62 + (-24L));
                            *(uint64_t *)(rbp_0_in + (-16L)) = *(uint64_t *)(var_62 + (-16L));
                            *(uint64_t *)(rbp_0_in + (-8L)) = *(uint64_t *)(var_62 + (-8L));
                        } else {
                            var_60 = *var_44;
                            var_61 = var_60 + (-32L);
                            *var_44 = var_61;
                            *(uint64_t *)rbp_0 = *(uint64_t *)var_61;
                            *(uint64_t *)(rbp_0_in + (-24L)) = *(uint64_t *)(var_60 + (-24L));
                            *(uint64_t *)(rbp_0_in + (-16L)) = *(uint64_t *)(var_60 + (-16L));
                            *(uint64_t *)(rbp_0_in + (-8L)) = *(uint64_t *)(var_60 + (-8L));
                        }
                        continue;
                    }
                switch (loop_state_var) {
                  case 0U:
                    {
                        r13_1 = r13_1_ph;
                        rcx3_1 = rcx3_1_ph;
                    }
                    break;
                  case 1U:
                    {
                        var_64 = *var_45;
                    }
                    break;
                }
                rbp_1 = rcx3_1;
                rdx1_0 = var_64;
                rbp_2 = rcx3_1;
                r13_4 = r13_1;
                if ((uint64_t)((long)(*(uint64_t *)(local_sp_2 + 8UL) - var_64) >> (long)5UL) != *(uint64_t *)(var_34 + 48UL)) {
                    if ((uint64_t)((long)(*(uint64_t *)(local_sp_2 + 16UL) - var_51) >> (long)5UL) != *(uint64_t *)(var_34 + 40UL) & (var_64 == *var_50) || (rcx3_1 == 0UL)) {
                        var_65 = r86_1 + (-32L);
                        var_66 = rdx1_0 + (-32L);
                        *var_45 = var_66;
                        *(uint64_t *)var_65 = *(uint64_t *)var_66;
                        *(uint64_t *)(r86_1 + (-24L)) = *(uint64_t *)(rdx1_0 + (-24L));
                        *(uint64_t *)(r86_1 + (-16L)) = *(uint64_t *)(rdx1_0 + (-16L));
                        *(uint64_t *)(r86_1 + (-8L)) = *(uint64_t *)(rdx1_0 + (-8L));
                        var_67 = *var_45;
                        rdx1_0 = var_67;
                        r86_1 = var_65;
                        rbp_3 = rbp_2;
                        r86_2 = var_65;
                        while (var_67 != *var_50)
                            {
                                var_68 = rbp_2 + (-1L);
                                rbp_2 = var_68;
                                rbp_3 = 0UL;
                                if (var_68 == 0UL) {
                                    break;
                                }
                                var_65 = r86_1 + (-32L);
                                var_66 = rdx1_0 + (-32L);
                                *var_45 = var_66;
                                *(uint64_t *)var_65 = *(uint64_t *)var_66;
                                *(uint64_t *)(r86_1 + (-24L)) = *(uint64_t *)(rdx1_0 + (-24L));
                                *(uint64_t *)(r86_1 + (-16L)) = *(uint64_t *)(rdx1_0 + (-16L));
                                *(uint64_t *)(r86_1 + (-8L)) = *(uint64_t *)(rdx1_0 + (-8L));
                                var_67 = *var_45;
                                rdx1_0 = var_67;
                                r86_1 = var_65;
                                rbp_3 = rbp_2;
                                r86_2 = var_65;
                            }
                    }
                }
                if (var_52 || (rcx3_1 == 0UL)) {
                    var_69 = r86_0 + (-32L);
                    var_70 = rdi2_0 + (-32L);
                    *var_44 = var_70;
                    *(uint64_t *)var_69 = *(uint64_t *)var_70;
                    *(uint64_t *)(r86_0 + (-24L)) = *(uint64_t *)(rdi2_0 + (-24L));
                    *(uint64_t *)(r86_0 + (-16L)) = *(uint64_t *)(rdi2_0 + (-16L));
                    *(uint64_t *)(r86_0 + (-8L)) = *(uint64_t *)(rdi2_0 + (-8L));
                    var_71 = *var_44;
                    rdi2_0 = var_71;
                    r86_0 = var_69;
                    rbp_3 = rbp_1;
                    r86_2 = var_69;
                    while (var_71 != *var_49)
                        {
                            var_72 = rbp_1 + (-1L);
                            rbp_1 = var_72;
                            rbp_3 = 0UL;
                            if (var_72 == 0UL) {
                                break;
                            }
                            var_69 = r86_0 + (-32L);
                            var_70 = rdi2_0 + (-32L);
                            *var_44 = var_70;
                            *(uint64_t *)var_69 = *(uint64_t *)var_70;
                            *(uint64_t *)(r86_0 + (-24L)) = *(uint64_t *)(rdi2_0 + (-24L));
                            *(uint64_t *)(r86_0 + (-16L)) = *(uint64_t *)(rdi2_0 + (-16L));
                            *(uint64_t *)(r86_0 + (-8L)) = *(uint64_t *)(rdi2_0 + (-8L));
                            var_71 = *var_44;
                            rdi2_0 = var_71;
                            r86_0 = var_69;
                            rbp_3 = rbp_1;
                            r86_2 = var_69;
                        }
                }
                **var_47 = r86_2;
                rbp_6 = rbp_3;
            }
            var_102 = (uint64_t)((long)(*(uint64_t *)(local_sp_6 + 8UL) - *(uint64_t *)(r12_1 + 8UL)) >> (long)5UL);
            var_103 = (uint64_t)((long)(*(uint64_t *)(local_sp_6 + 16UL) - *(uint64_t *)r12_1) >> (long)5UL);
            var_104 = (uint64_t *)(var_34 + 40UL);
            *var_104 = (*var_104 - var_103);
            var_105 = (uint64_t *)(var_34 + 48UL);
            *var_105 = (*var_105 - var_102);
            var_106 = local_sp_6 + (-8L);
            *(uint64_t *)var_106 = 4220718UL;
            indirect_placeholder_8(r8, r12_1);
            local_sp_0 = var_106;
            rbp_7 = rbp_6;
            r13_5 = r13_4;
            r12_2 = r12_1;
            if (*(uint32_t *)(r12_1 + 80UL) > 1U) {
                *(uint64_t *)(local_sp_6 + (-16L)) = 4220740UL;
                indirect_placeholder();
                var_109 = *(uint64_t *)(r12_1 + 56UL);
                *(uint64_t *)(local_sp_6 + (-24L)) = 4220753UL;
                indirect_placeholder_8(r8, var_109);
                var_110 = local_sp_6 + (-32L);
                *(uint64_t *)var_110 = 4220767UL;
                indirect_placeholder();
                local_sp_0 = var_110;
            } else {
                if (*(uint64_t *)(r12_1 + 40UL) == (0UL - *(uint64_t *)(r12_1 + 48UL))) {
                    var_107 = *(uint64_t *)(r12_1 + 56UL);
                    var_108 = local_sp_6 + (-16L);
                    *(uint64_t *)var_108 = 4220794UL;
                    indirect_placeholder_8(r8, var_107);
                    local_sp_0 = var_108;
                }
            }
            var_111 = local_sp_0 + (-8L);
            *(uint64_t *)var_111 = 4220804UL;
            indirect_placeholder();
            local_sp_8 = var_111;
            continue;
        }
    *(uint64_t *)(local_sp_7 + (-32L)) = 4219861UL;
    indirect_placeholder();
    *(uint64_t *)(local_sp_7 + (-40L)) = 4219872UL;
    indirect_placeholder_8(r8, var_34);
    return;
}

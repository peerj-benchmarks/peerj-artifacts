typedef unsigned char __u_char;
typedef unsigned short __u_short;
typedef unsigned int __u_int;
typedef unsigned long __u_long;
typedef signed char __int8_t;
typedef unsigned char __uint8_t;
typedef short __int16_t;
typedef unsigned short __uint16_t;
typedef int __int32_t;
typedef unsigned int __uint32_t;
typedef long __int64_t;
typedef unsigned long __uint64_t;
typedef __int8_t __int_least8_t;
typedef __uint8_t __uint_least8_t;
typedef __int16_t __int_least16_t;
typedef __uint16_t __uint_least16_t;
typedef __int32_t __int_least32_t;
typedef __uint32_t __uint_least32_t;
typedef __int64_t __int_least64_t;
typedef __uint64_t __uint_least64_t;
typedef long __quad_t;
typedef unsigned long __u_quad_t;
typedef long __intmax_t;
typedef unsigned long __uintmax_t;
typedef unsigned long __dev_t;
typedef unsigned int __uid_t;
typedef unsigned int __gid_t;
typedef unsigned long __ino_t;
typedef unsigned long __ino64_t;
typedef unsigned int __mode_t;
typedef unsigned long __nlink_t;
typedef long __off_t;
typedef long __off64_t;
typedef int __pid_t;
typedef struct {
    int __val[2];
} __fsid_t;
typedef long __clock_t;
typedef unsigned long __rlim_t;
typedef unsigned long __rlim64_t;
typedef unsigned int __id_t;
typedef long __time_t;
typedef unsigned int __useconds_t;
typedef long __suseconds_t;
typedef int __daddr_t;
typedef int __key_t;
typedef int __clockid_t;
typedef void *__timer_t;
typedef long __blksize_t;
typedef long __blkcnt_t;
typedef long __blkcnt64_t;
typedef unsigned long __fsblkcnt_t;
typedef unsigned long __fsblkcnt64_t;
typedef unsigned long __fsfilcnt_t;
typedef unsigned long __fsfilcnt64_t;
typedef long __fsword_t;
typedef long __ssize_t;
typedef long __syscall_slong_t;
typedef unsigned long __syscall_ulong_t;
typedef __off64_t __loff_t;
typedef char *__caddr_t;
typedef long __intptr_t;
typedef unsigned int __socklen_t;
typedef int __sig_atomic_t;
typedef __int8_t int8_t;
typedef __int16_t int16_t;
typedef __int32_t int32_t;
typedef __int64_t int64_t;
typedef __uint8_t uint8_t;
typedef __uint16_t uint16_t;
typedef __uint32_t uint32_t;
typedef __uint64_t uint64_t;
typedef __int_least8_t int_least8_t;
typedef __int_least16_t int_least16_t;
typedef __int_least32_t int_least32_t;
typedef __int_least64_t int_least64_t;
typedef __uint_least8_t uint_least8_t;
typedef __uint_least16_t uint_least16_t;
typedef __uint_least32_t uint_least32_t;
typedef __uint_least64_t uint_least64_t;
typedef signed char int_fast8_t;
typedef long int_fast16_t;
typedef long int_fast32_t;
typedef long int_fast64_t;
typedef unsigned char uint_fast8_t;
typedef unsigned long uint_fast16_t;
typedef unsigned long uint_fast32_t;
typedef unsigned long uint_fast64_t;
typedef long intptr_t;
typedef unsigned long uintptr_t;
typedef __intmax_t intmax_t;
typedef __uintmax_t uintmax_t;
typedef _Bool bool;
struct bb_limfield_ret_type;
struct bb_limfield_ret_type {
    uint64_t field_0;
    uint64_t field_1;
};
extern uint64_t revng_init_local_sp(uint64_t param_0);
extern uint64_t init_cc_src2(void);
extern uint64_t helper_cc_compute_c_wrapper(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint32_t param_3);
struct bb_limfield_ret_type bb_limfield(uint64_t rdi, uint64_t rsi) {
    uint64_t rdi4_0;
    uint64_t var_0;
    uint64_t var_1;
    uint64_t var_2;
    uint64_t var_3;
    uint64_t var_4;
    uint64_t var_5;
    uint64_t var_6;
    uint32_t var_7;
    bool var_8;
    uint64_t var_9;
    bool or_cond;
    uint64_t rax_9;
    unsigned char var_12;
    uint64_t rax_8;
    uint64_t rax_4;
    uint64_t rax_0;
    uint64_t rax_2_ph;
    uint64_t rax_1;
    uint64_t var_13;
    uint64_t rax_2;
    uint64_t var_14;
    unsigned char var_15;
    uint64_t var_16;
    struct bb_limfield_ret_type mrv1;
    struct bb_limfield_ret_type mrv2;
    struct bb_limfield_ret_type mrv3;
    uint64_t r8_0_in;
    uint64_t r8_0;
    uint64_t rax_7;
    uint64_t rax_6;
    uint64_t rax_5;
    uint64_t var_10;
    uint64_t var_11;
    uint64_t rax_11;
    uint64_t var_17;
    uint64_t rax_10;
    uint64_t var_18;
    uint64_t var_19;
    struct bb_limfield_ret_type mrv;
    unsigned int loop_state_var;
    bool switch_state_var;
    revng_init_local_sp(0UL);
    var_0 = init_cc_src2();
    var_1 = *(uint64_t *)rdi;
    var_2 = (*(uint64_t *)(rdi + 8UL) + var_1) + (-1L);
    var_3 = *(uint64_t *)(rsi + 16UL);
    var_4 = *(uint64_t *)(rsi + 24UL);
    var_5 = helper_cc_compute_c_wrapper(var_4 + (-1L), 1UL, var_0, 17U);
    var_6 = var_3 + var_5;
    var_7 = *(uint32_t *)6428188UL;
    var_8 = ((uint64_t)(var_7 + (-128)) == 0UL);
    var_9 = helper_cc_compute_c_wrapper(var_1 - var_2, var_2, var_5, 17U);
    or_cond = ((var_9 == 0UL) || (var_6 == 0UL));
    rdi4_0 = var_6;
    rax_9 = var_1;
    rax_4 = var_1;
    rax_0 = var_1;
    r8_0_in = var_6;
    if (!var_8) {
        if (or_cond) {
            while (1U)
                {
                    r8_0 = r8_0_in + (-1L);
                    rax_9 = var_2;
                    r8_0_in = r8_0;
                    rax_7 = rax_4;
                    rax_6 = rax_4;
                    rax_5 = rax_4;
                    if (var_2 <= rax_4) {
                        rax_8 = rax_7;
                        rax_9 = rax_7;
                        if (var_2 > rax_7) {
                            break;
                        }
                        rax_4 = rax_8;
                        rax_9 = rax_8;
                        if (r8_0 != 0UL) {
                            continue;
                        }
                        break;
                    }
                    if ((uint64_t)(var_7 - (uint32_t)(uint64_t)*(unsigned char *)rax_4) == 0UL) {
                        rax_8 = rax_6;
                        if ((r8_0 | var_4) != 0UL) {
                            var_11 = rax_6 + 1UL;
                            rax_7 = var_11;
                            rax_8 = rax_7;
                            rax_9 = rax_7;
                            if (var_2 > rax_7) {
                                break;
                            }
                        }
                    }
                    while (1U)
                        {
                            var_10 = rax_5 + 1UL;
                            rax_5 = var_10;
                            rax_6 = var_10;
                            if (var_10 != var_2) {
                                loop_state_var = 1U;
                                break;
                            }
                            if ((uint64_t)(var_7 - (uint32_t)(uint64_t)*(unsigned char *)var_10) == 0UL) {
                                continue;
                            }
                            loop_state_var = 0U;
                            break;
                        }
                    switch_state_var = 0;
                    switch (loop_state_var) {
                      case 1U:
                        {
                            switch_state_var = 1;
                            break;
                        }
                        break;
                      case 0U:
                        {
                            break;
                        }
                        break;
                    }
                    if (switch_state_var)
                        break;
                }
        }
    }
    if (!or_cond) {
        var_12 = *(unsigned char *)var_1;
        rax_9 = var_2;
        while (1U)
            {
                rax_1 = rax_0;
                rax_2_ph = rax_0;
                if (*(unsigned char *)((uint64_t)var_12 + 6430016UL) != '\x00') {
                    while (1U)
                        {
                            var_13 = rax_1 + 1UL;
                            rax_1 = var_13;
                            rax_2_ph = var_13;
                            if (var_13 != var_2) {
                                loop_state_var = 0U;
                                break;
                            }
                            if (*(unsigned char *)((uint64_t)*(unsigned char *)var_13 + 6430016UL) == '\x00') {
                                continue;
                            }
                            loop_state_var = 1U;
                            break;
                        }
                    switch_state_var = 0;
                    switch (loop_state_var) {
                      case 0U:
                        {
                            switch_state_var = 1;
                            break;
                        }
                        break;
                      case 1U:
                        {
                            break;
                        }
                        break;
                    }
                    if (switch_state_var)
                        break;
                }
                rax_2 = rax_2_ph;
                while (1U)
                    {
                        var_14 = rax_2 + 1UL;
                        rax_0 = var_14;
                        rax_2 = var_14;
                        rax_9 = var_14;
                        if (var_2 <= var_14) {
                            loop_state_var = 1U;
                            break;
                        }
                        var_15 = *(unsigned char *)var_14;
                        var_12 = var_15;
                        if (*(unsigned char *)((uint64_t)var_15 + 6430016UL) == '\x00') {
                            continue;
                        }
                        loop_state_var = 0U;
                        break;
                    }
                switch_state_var = 0;
                switch (loop_state_var) {
                  case 1U:
                    {
                        switch_state_var = 1;
                        break;
                    }
                    break;
                  case 0U:
                    {
                        var_16 = rdi4_0 + (-1L);
                        rdi4_0 = var_16;
                        if (var_16 == 0UL) {
                            continue;
                        }
                        switch_state_var = 1;
                        break;
                    }
                    break;
                }
                if (switch_state_var)
                    break;
            }
    }
    rax_10 = rax_9;
    rax_11 = rax_9;
    if (var_4 == 0UL) {
        mrv2.field_0 = rax_9;
        mrv3 = mrv2;
        mrv3.field_1 = 0UL;
        return mrv3;
    }
    var_17 = helper_cc_compute_c_wrapper(rax_9 - var_2, var_2, var_5, 17U);
    if (~(*(unsigned char *)(rsi + 49UL) != '\x00' & var_17 != 0UL & *(unsigned char *)((uint64_t)*(unsigned char *)rax_9 + 6430016UL) == '\x00')) {
        var_19 = rax_11 + var_4;
        mrv.field_0 = (var_19 > var_2) ? var_2 : var_19;
        mrv1 = mrv;
        mrv1.field_1 = var_4;
        return mrv1;
    }
    var_18 = rax_10 + 1UL;
    rax_10 = var_18;
    rax_11 = var_2;
    while (var_18 != var_2)
        {
            rax_11 = var_18;
            if (*(unsigned char *)((uint64_t)*(unsigned char *)var_18 + 6430016UL) == '\x00') {
                break;
            }
            var_18 = rax_10 + 1UL;
            rax_10 = var_18;
            rax_11 = var_2;
        }
    var_19 = rax_11 + var_4;
    mrv.field_0 = (var_19 > var_2) ? var_2 : var_19;
    mrv1 = mrv;
    mrv1.field_1 = var_4;
    return mrv1;
}

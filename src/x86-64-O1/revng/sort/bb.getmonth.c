typedef unsigned char __u_char;
typedef unsigned short __u_short;
typedef unsigned int __u_int;
typedef unsigned long __u_long;
typedef signed char __int8_t;
typedef unsigned char __uint8_t;
typedef short __int16_t;
typedef unsigned short __uint16_t;
typedef int __int32_t;
typedef unsigned int __uint32_t;
typedef long __int64_t;
typedef unsigned long __uint64_t;
typedef __int8_t __int_least8_t;
typedef __uint8_t __uint_least8_t;
typedef __int16_t __int_least16_t;
typedef __uint16_t __uint_least16_t;
typedef __int32_t __int_least32_t;
typedef __uint32_t __uint_least32_t;
typedef __int64_t __int_least64_t;
typedef __uint64_t __uint_least64_t;
typedef long __quad_t;
typedef unsigned long __u_quad_t;
typedef long __intmax_t;
typedef unsigned long __uintmax_t;
typedef unsigned long __dev_t;
typedef unsigned int __uid_t;
typedef unsigned int __gid_t;
typedef unsigned long __ino_t;
typedef unsigned long __ino64_t;
typedef unsigned int __mode_t;
typedef unsigned long __nlink_t;
typedef long __off_t;
typedef long __off64_t;
typedef int __pid_t;
typedef struct {
    int __val[2];
} __fsid_t;
typedef long __clock_t;
typedef unsigned long __rlim_t;
typedef unsigned long __rlim64_t;
typedef unsigned int __id_t;
typedef long __time_t;
typedef unsigned int __useconds_t;
typedef long __suseconds_t;
typedef int __daddr_t;
typedef int __key_t;
typedef int __clockid_t;
typedef void *__timer_t;
typedef long __blksize_t;
typedef long __blkcnt_t;
typedef long __blkcnt64_t;
typedef unsigned long __fsblkcnt_t;
typedef unsigned long __fsblkcnt64_t;
typedef unsigned long __fsfilcnt_t;
typedef unsigned long __fsfilcnt64_t;
typedef long __fsword_t;
typedef long __ssize_t;
typedef long __syscall_slong_t;
typedef unsigned long __syscall_ulong_t;
typedef __off64_t __loff_t;
typedef char *__caddr_t;
typedef long __intptr_t;
typedef unsigned int __socklen_t;
typedef int __sig_atomic_t;
typedef __int8_t int8_t;
typedef __int16_t int16_t;
typedef __int32_t int32_t;
typedef __int64_t int64_t;
typedef __uint8_t uint8_t;
typedef __uint16_t uint16_t;
typedef __uint32_t uint32_t;
typedef __uint64_t uint64_t;
typedef __int_least8_t int_least8_t;
typedef __int_least16_t int_least16_t;
typedef __int_least32_t int_least32_t;
typedef __int_least64_t int_least64_t;
typedef __uint_least8_t uint_least8_t;
typedef __uint_least16_t uint_least16_t;
typedef __uint_least32_t uint_least32_t;
typedef __uint_least64_t uint_least64_t;
typedef signed char int_fast8_t;
typedef long int_fast16_t;
typedef long int_fast32_t;
typedef long int_fast64_t;
typedef unsigned char uint_fast8_t;
typedef unsigned long uint_fast16_t;
typedef unsigned long uint_fast32_t;
typedef unsigned long uint_fast64_t;
typedef long intptr_t;
typedef unsigned long uintptr_t;
typedef __intmax_t intmax_t;
typedef __uintmax_t uintmax_t;
extern uint64_t revng_init_local_sp(uint64_t param_0);
extern uint64_t init_cc_src2(void);
extern uint64_t helper_cc_compute_c_wrapper(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint32_t param_3);
extern uint64_t helper_cc_compute_all_wrapper(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint32_t param_3);
typedef _Bool bool;
uint64_t bb_getmonth(uint64_t rdi, uint64_t rsi) {
    uint64_t var_0;
    unsigned char *_cast;
    unsigned char *_pre_phi;
    uint64_t rdi1_0;
    uint64_t var_1;
    unsigned char *_cast1;
    uint64_t rdi1_1;
    uint64_t r11_1;
    uint64_t r10_1;
    uint64_t r10_0;
    uint64_t r11_0;
    uint64_t var_2;
    uint64_t var_3;
    uint64_t var_4;
    unsigned char var_5;
    uint64_t var_6;
    uint64_t rcx_1;
    uint64_t var_7;
    uint64_t var_8;
    uint64_t var_9;
    uint64_t rcx_0;
    uint64_t rdx_0;
    uint64_t var_10;
    uint64_t var_11;
    unsigned char var_12;
    uint64_t var_13;
    uint64_t var_14;
    uint64_t var_15;
    uint64_t var_16;
    uint64_t var_17;
    unsigned int loop_state_var;
    bool switch_state_var;
    revng_init_local_sp(0UL);
    var_0 = init_cc_src2();
    _cast = (unsigned char *)rdi;
    _pre_phi = _cast;
    rdi1_0 = rdi;
    rdi1_1 = rdi;
    r10_0 = 0UL;
    r11_0 = 12UL;
    if (*(unsigned char *)((uint64_t)*_cast + 6430016UL) == '\x00') {
        var_1 = rdi1_0 + 1UL;
        _cast1 = (unsigned char *)var_1;
        rdi1_0 = var_1;
        _pre_phi = _cast1;
        rdi1_1 = var_1;
        do {
            var_1 = rdi1_0 + 1UL;
            _cast1 = (unsigned char *)var_1;
            rdi1_0 = var_1;
            _pre_phi = _cast1;
            rdi1_1 = var_1;
        } while (*(unsigned char *)((uint64_t)*_cast1 + 6430016UL) != '\x00');
    }
    rcx_0 = rdi1_1;
    rcx_1 = rdi1_1;
    var_2 = (r10_0 + r11_0) >> 1UL;
    var_3 = var_2 << 4UL;
    var_4 = *(uint64_t *)(var_3 + 6428224UL);
    var_5 = *(unsigned char *)var_4;
    var_6 = (uint64_t)var_5;
    rdx_0 = var_4;
    r10_1 = r10_0;
    r11_1 = var_2;
    while (var_5 != '\x00')
        {
            var_7 = (uint64_t)*(unsigned char *)((uint64_t)*_pre_phi + 6429248UL) - var_6;
            var_8 = helper_cc_compute_c_wrapper(var_7, var_6, var_0, 14U);
            if (var_8 == 0UL) {
                var_17 = helper_cc_compute_c_wrapper(r10_1 - r11_1, r11_1, var_0, 17U);
                r10_0 = r10_1;
                r11_0 = r11_1;
                if (var_17 == 0UL) {
                    return 0UL;
                }
                var_2 = (r10_0 + r11_0) >> 1UL;
                var_3 = var_2 << 4UL;
                var_4 = *(uint64_t *)(var_3 + 6428224UL);
                var_5 = *(unsigned char *)var_4;
                var_6 = (uint64_t)var_5;
                rdx_0 = var_4;
                r10_1 = r10_0;
                r11_1 = var_2;
                continue;
            }
            var_9 = helper_cc_compute_all_wrapper(var_7, var_6, var_0, 14U);
            r11_1 = r11_0;
            if ((var_9 & 65UL) == 0UL) {
                while (1U)
                    {
                        var_10 = rcx_0 + 1UL;
                        var_11 = rdx_0 + 1UL;
                        var_12 = *(unsigned char *)var_11;
                        var_13 = (uint64_t)var_12;
                        rcx_0 = var_10;
                        rdx_0 = var_11;
                        r11_1 = var_2;
                        rcx_1 = var_10;
                        if (var_12 != '\x00') {
                            loop_state_var = 1U;
                            break;
                        }
                        var_14 = (uint64_t)*(unsigned char *)((uint64_t)*(unsigned char *)var_10 + 6429248UL) - var_13;
                        var_15 = helper_cc_compute_c_wrapper(var_14, var_13, var_0, 14U);
                        if (var_15 != 0UL) {
                            loop_state_var = 0U;
                            break;
                        }
                        var_16 = helper_cc_compute_all_wrapper(var_14, var_13, var_0, 14U);
                        if ((var_16 & 65UL) == 0UL) {
                            continue;
                        }
                        loop_state_var = 2U;
                        break;
                    }
                switch_state_var = 0;
                switch (loop_state_var) {
                  case 1U:
                    {
                        switch_state_var = 1;
                        break;
                    }
                    break;
                  case 2U:
                  case 0U:
                    {
                        r10_1 = var_2 + 1UL;
                    }
                    break;
                }
                if (switch_state_var)
                    break;
            } else {
                r10_1 = var_2 + 1UL;
            }
        }
    if (rsi == 0UL) {
        return (uint64_t)*(uint32_t *)(var_3 + 6428232UL);
    }
    *(uint64_t *)rsi = rcx_1;
    return (uint64_t)*(uint32_t *)(var_3 + 6428232UL);
}

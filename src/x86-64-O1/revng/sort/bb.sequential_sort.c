typedef unsigned char __u_char;
typedef unsigned short __u_short;
typedef unsigned int __u_int;
typedef unsigned long __u_long;
typedef signed char __int8_t;
typedef unsigned char __uint8_t;
typedef short __int16_t;
typedef unsigned short __uint16_t;
typedef int __int32_t;
typedef unsigned int __uint32_t;
typedef long __int64_t;
typedef unsigned long __uint64_t;
typedef __int8_t __int_least8_t;
typedef __uint8_t __uint_least8_t;
typedef __int16_t __int_least16_t;
typedef __uint16_t __uint_least16_t;
typedef __int32_t __int_least32_t;
typedef __uint32_t __uint_least32_t;
typedef __int64_t __int_least64_t;
typedef __uint64_t __uint_least64_t;
typedef long __quad_t;
typedef unsigned long __u_quad_t;
typedef long __intmax_t;
typedef unsigned long __uintmax_t;
typedef unsigned long __dev_t;
typedef unsigned int __uid_t;
typedef unsigned int __gid_t;
typedef unsigned long __ino_t;
typedef unsigned long __ino64_t;
typedef unsigned int __mode_t;
typedef unsigned long __nlink_t;
typedef long __off_t;
typedef long __off64_t;
typedef int __pid_t;
typedef struct {
    int __val[2];
} __fsid_t;
typedef long __clock_t;
typedef unsigned long __rlim_t;
typedef unsigned long __rlim64_t;
typedef unsigned int __id_t;
typedef long __time_t;
typedef unsigned int __useconds_t;
typedef long __suseconds_t;
typedef int __daddr_t;
typedef int __key_t;
typedef int __clockid_t;
typedef void *__timer_t;
typedef long __blksize_t;
typedef long __blkcnt_t;
typedef long __blkcnt64_t;
typedef unsigned long __fsblkcnt_t;
typedef unsigned long __fsblkcnt64_t;
typedef unsigned long __fsfilcnt_t;
typedef unsigned long __fsfilcnt64_t;
typedef long __fsword_t;
typedef long __ssize_t;
typedef long __syscall_slong_t;
typedef unsigned long __syscall_ulong_t;
typedef __off64_t __loff_t;
typedef char *__caddr_t;
typedef long __intptr_t;
typedef unsigned int __socklen_t;
typedef int __sig_atomic_t;
typedef __int8_t int8_t;
typedef __int16_t int16_t;
typedef __int32_t int32_t;
typedef __int64_t int64_t;
typedef __uint8_t uint8_t;
typedef __uint16_t uint16_t;
typedef __uint32_t uint32_t;
typedef __uint64_t uint64_t;
typedef __int_least8_t int_least8_t;
typedef __int_least16_t int_least16_t;
typedef __int_least32_t int_least32_t;
typedef __int_least64_t int_least64_t;
typedef __uint_least8_t uint_least8_t;
typedef __uint_least16_t uint_least16_t;
typedef __uint_least32_t uint_least32_t;
typedef __uint_least64_t uint_least64_t;
typedef signed char int_fast8_t;
typedef long int_fast16_t;
typedef long int_fast32_t;
typedef long int_fast64_t;
typedef unsigned char uint_fast8_t;
typedef unsigned long uint_fast16_t;
typedef unsigned long uint_fast32_t;
typedef unsigned long uint_fast64_t;
typedef long intptr_t;
typedef unsigned long uintptr_t;
typedef __intmax_t intmax_t;
typedef __uintmax_t uintmax_t;
typedef _Bool bool;
extern uint64_t revng_init_local_sp(uint64_t param_0);
extern uint64_t init_rbx(void);
extern uint64_t init_rbp(void);
extern uint64_t init_r14(void);
extern uint64_t init_r13(void);
extern uint64_t init_r12(void);
extern uint64_t helper_cc_compute_all_wrapper(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint32_t param_3);
extern uint64_t init_r15(void);
extern void indirect_placeholder_3(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint64_t param_3);
extern uint64_t indirect_placeholder_1(uint64_t param_0, uint64_t param_1);
void bb_sequential_sort(uint64_t rdx, uint64_t rdi, uint64_t rcx, uint64_t rsi) {
    uint64_t var_0;
    uint64_t var_1;
    uint64_t var_2;
    uint64_t var_3;
    uint64_t var_4;
    uint64_t var_5;
    uint64_t var_6;
    uint64_t var_19;
    uint64_t storemerge_in;
    uint64_t r14_1;
    uint64_t *var_25;
    uint64_t var_26;
    uint64_t _pre_phi;
    uint64_t r14_0;
    uint64_t r12_3;
    uint64_t rbp_1;
    uint64_t var_27;
    uint64_t *var_28;
    uint64_t rax_0;
    uint64_t r12_0;
    uint64_t var_29;
    uint64_t var_30;
    uint64_t var_31;
    uint64_t rbp_0;
    uint64_t r12_1;
    uint64_t local_sp_1;
    uint64_t local_sp_0;
    uint64_t rbx_0;
    uint64_t r12_2;
    uint64_t storemerge;
    uint64_t var_20;
    uint64_t var_21;
    uint64_t var_22;
    uint64_t var_23;
    uint64_t var_24;
    uint64_t var_15;
    uint64_t var_16;
    uint64_t var_37;
    uint64_t var_38;
    uint64_t *var_39;
    uint64_t var_40;
    uint64_t *var_41;
    uint64_t var_42;
    uint64_t *var_43;
    uint64_t var_44;
    uint64_t *var_45;
    uint64_t var_46;
    uint64_t *var_47;
    uint64_t *var_48;
    uint64_t *var_49;
    uint64_t *var_50;
    uint64_t var_32;
    uint64_t var_33;
    uint64_t var_34;
    uint64_t var_35;
    bool var_36;
    uint64_t var_7;
    uint64_t var_8;
    uint64_t var_9;
    uint64_t var_10;
    uint64_t var_11;
    uint64_t var_12;
    uint64_t var_13;
    uint64_t var_14;
    uint64_t var_17;
    uint64_t var_18;
    unsigned int loop_state_var;
    var_0 = revng_init_local_sp(0UL);
    var_1 = init_rbx();
    var_2 = init_r14();
    var_3 = init_rbp();
    var_4 = init_r13();
    var_5 = init_r12();
    var_6 = init_r15();
    *(uint64_t *)(var_0 + (-8L)) = var_6;
    *(uint64_t *)(var_0 + (-16L)) = var_2;
    *(uint64_t *)(var_0 + (-24L)) = var_4;
    *(uint64_t *)(var_0 + (-32L)) = var_5;
    *(uint64_t *)(var_0 + (-40L)) = var_3;
    *(uint64_t *)(var_0 + (-48L)) = var_1;
    rbx_0 = rdi;
    r12_2 = rdx;
    if (rsi == 2UL) {
        var_32 = rdi + (-64L);
        var_33 = rdi + (-32L);
        *(uint64_t *)(var_0 + (-80L)) = 4213977UL;
        var_34 = indirect_placeholder_1(var_33, var_32);
        var_35 = helper_cc_compute_all_wrapper(var_34, 0UL, 0UL, 24U);
        var_36 = ((uint64_t)(((unsigned char)(var_35 >> 4UL) ^ (unsigned char)var_35) & '\xc0') == 0UL);
        if ((uint64_t)(unsigned char)rcx == 0UL) {
            var_37 = (var_36 ? 18446744073709551552UL : 18446744073709551584UL) + rdi;
            *(uint64_t *)(rdx + (-32L)) = *(uint64_t *)var_37;
            *(uint64_t *)(rdx + (-24L)) = *(uint64_t *)(var_37 + 8UL);
            *(uint64_t *)(rdx + (-16L)) = *(uint64_t *)(var_37 + 16UL);
            *(uint64_t *)(rdx + (-8L)) = *(uint64_t *)(var_37 + 24UL);
            var_38 = (var_36 ? 18446744073709551584UL : 18446744073709551552UL) + rdi;
            *(uint64_t *)(rdx + (-64L)) = *(uint64_t *)var_38;
            *(uint64_t *)(rdx + (-56L)) = *(uint64_t *)(var_38 + 8UL);
            *(uint64_t *)(rdx + (-48L)) = *(uint64_t *)(var_38 + 16UL);
            *(uint64_t *)(rdx + (-40L)) = *(uint64_t *)(var_38 + 24UL);
        } else {
            if (var_36) {
                var_39 = (uint64_t *)var_33;
                var_40 = *var_39;
                *(uint64_t *)(rdx + (-32L)) = var_40;
                var_41 = (uint64_t *)(rdi + (-24L));
                var_42 = *var_41;
                *(uint64_t *)(rdx + (-24L)) = var_42;
                var_43 = (uint64_t *)(rdi + (-16L));
                var_44 = *var_43;
                *(uint64_t *)(rdx + (-16L)) = var_44;
                var_45 = (uint64_t *)(rdi + (-8L));
                var_46 = *var_45;
                *(uint64_t *)(rdx + (-8L)) = var_46;
                var_47 = (uint64_t *)var_32;
                *var_39 = *var_47;
                var_48 = (uint64_t *)(rdi + (-56L));
                *var_41 = *var_48;
                var_49 = (uint64_t *)(rdi + (-48L));
                *var_43 = *var_49;
                var_50 = (uint64_t *)(rdi + (-40L));
                *var_45 = *var_50;
                *var_47 = var_40;
                *var_48 = var_42;
                *var_49 = var_44;
                *var_50 = var_46;
            }
        }
    } else {
        var_7 = rsi >> 1UL;
        *(uint64_t *)(var_0 + (-64L)) = var_7;
        var_8 = rsi - var_7;
        var_9 = var_7 << 5UL;
        var_10 = rdi - var_9;
        var_11 = (uint64_t)(unsigned char)rcx;
        _pre_phi = var_10;
        r14_1 = var_8;
        if (var_11 == 0UL) {
            var_17 = var_0 + (-80L);
            *(uint64_t *)var_17 = 4214496UL;
            indirect_placeholder_3(rdx, var_10, 0UL, var_8);
            var_18 = *(uint64_t *)(var_0 + (-72L));
            local_sp_0 = var_17;
            if (var_18 > 1UL) {
                var_19 = var_0 + (-88L);
                *(uint64_t *)var_19 = 4214533UL;
                indirect_placeholder_3(rdx, rdi, 1UL, var_18);
                local_sp_0 = var_19;
            } else {
                *(uint64_t *)(rdx + (-32L)) = *(uint64_t *)(rdi + (-32L));
                *(uint64_t *)(rdx + (-24L)) = *(uint64_t *)(rdi + (-24L));
                *(uint64_t *)(rdx + (-16L)) = *(uint64_t *)(rdi + (-16L));
                *(uint64_t *)(rdx + (-8L)) = *(uint64_t *)(rdi + (-8L));
            }
        } else {
            var_12 = rdx - var_9;
            var_13 = var_0 + (-80L);
            *(uint64_t *)var_13 = 4214248UL;
            indirect_placeholder_3(var_12, var_10, var_11, var_8);
            var_14 = *(uint64_t *)(var_0 + (-72L));
            _pre_phi = var_12;
            local_sp_0 = var_13;
            rbx_0 = rdx;
            r12_2 = rdi;
            if (var_14 > 1UL) {
                var_15 = var_11 ^ 1UL;
                var_16 = var_0 + (-88L);
                *(uint64_t *)var_16 = 4214571UL;
                indirect_placeholder_3(rdx, rdi, var_15, var_14);
                local_sp_0 = var_16;
            }
        }
        local_sp_1 = local_sp_0;
        rbp_1 = _pre_phi;
        r12_3 = r12_2;
        storemerge_in = rbx_0;
        while (1U)
            {
                storemerge = storemerge_in + (-32L);
                var_20 = rbp_1 + (-32L);
                var_21 = r12_3 + (-32L);
                var_22 = local_sp_1 + (-8L);
                *(uint64_t *)var_22 = 4214343UL;
                var_23 = indirect_placeholder_1(var_21, var_20);
                var_24 = helper_cc_compute_all_wrapper(var_23, 0UL, 0UL, 24U);
                storemerge_in = storemerge;
                r14_0 = r14_1;
                rax_0 = storemerge;
                r12_0 = r12_3;
                rbp_0 = rbp_1;
                r12_1 = var_21;
                local_sp_1 = var_22;
                if ((uint64_t)(((unsigned char)(var_24 >> 4UL) ^ (unsigned char)var_24) & '\xc0') == 0UL) {
                    *(uint64_t *)storemerge = *(uint64_t *)var_20;
                    *(uint64_t *)(storemerge_in + (-24L)) = *(uint64_t *)(rbp_1 + (-24L));
                    *(uint64_t *)(storemerge_in + (-16L)) = *(uint64_t *)(rbp_1 + (-16L));
                    *(uint64_t *)(storemerge_in + (-8L)) = *(uint64_t *)(rbp_1 + (-8L));
                    var_27 = r14_1 + (-1L);
                    r14_0 = var_27;
                    rbp_0 = var_20;
                    r12_1 = r12_3;
                    if (var_27 != 0UL) {
                        var_28 = (uint64_t *)local_sp_1;
                        loop_state_var = 1U;
                        break;
                    }
                }
                *(uint64_t *)storemerge = *(uint64_t *)var_21;
                *(uint64_t *)(storemerge_in + (-24L)) = *(uint64_t *)(r12_3 + (-24L));
                *(uint64_t *)(storemerge_in + (-16L)) = *(uint64_t *)(r12_3 + (-16L));
                *(uint64_t *)(storemerge_in + (-8L)) = *(uint64_t *)(r12_3 + (-8L));
                var_25 = (uint64_t *)local_sp_1;
                var_26 = *var_25 + (-1L);
                *var_25 = var_26;
                if (var_26 != 0UL) {
                    loop_state_var = 0U;
                    break;
                }
                r14_1 = r14_0;
                rbp_1 = rbp_0;
                r12_3 = r12_1;
                continue;
            }
        var_29 = rax_0 + (-32L);
        var_30 = r12_0 + (-32L);
        *(uint64_t *)var_29 = *(uint64_t *)var_30;
        *(uint64_t *)(rax_0 + (-24L)) = *(uint64_t *)(r12_0 + (-24L));
        *(uint64_t *)(rax_0 + (-16L)) = *(uint64_t *)(r12_0 + (-16L));
        *(uint64_t *)(rax_0 + (-8L)) = *(uint64_t *)(r12_0 + (-8L));
        var_31 = *var_28 + (-1L);
        *var_28 = var_31;
        rax_0 = var_29;
        r12_0 = var_30;
        do {
            var_29 = rax_0 + (-32L);
            var_30 = r12_0 + (-32L);
            *(uint64_t *)var_29 = *(uint64_t *)var_30;
            *(uint64_t *)(rax_0 + (-24L)) = *(uint64_t *)(r12_0 + (-24L));
            *(uint64_t *)(rax_0 + (-16L)) = *(uint64_t *)(r12_0 + (-16L));
            *(uint64_t *)(rax_0 + (-8L)) = *(uint64_t *)(r12_0 + (-8L));
            var_31 = *var_28 + (-1L);
            *var_28 = var_31;
            rax_0 = var_29;
            r12_0 = var_30;
        } while (var_31 != 0UL);
    }
}

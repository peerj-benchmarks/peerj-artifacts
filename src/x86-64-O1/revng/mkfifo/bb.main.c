typedef unsigned char __u_char;
typedef unsigned short __u_short;
typedef unsigned int __u_int;
typedef unsigned long __u_long;
typedef signed char __int8_t;
typedef unsigned char __uint8_t;
typedef short __int16_t;
typedef unsigned short __uint16_t;
typedef int __int32_t;
typedef unsigned int __uint32_t;
typedef long __int64_t;
typedef unsigned long __uint64_t;
typedef __int8_t __int_least8_t;
typedef __uint8_t __uint_least8_t;
typedef __int16_t __int_least16_t;
typedef __uint16_t __uint_least16_t;
typedef __int32_t __int_least32_t;
typedef __uint32_t __uint_least32_t;
typedef __int64_t __int_least64_t;
typedef __uint64_t __uint_least64_t;
typedef long __quad_t;
typedef unsigned long __u_quad_t;
typedef long __intmax_t;
typedef unsigned long __uintmax_t;
typedef unsigned long __dev_t;
typedef unsigned int __uid_t;
typedef unsigned int __gid_t;
typedef unsigned long __ino_t;
typedef unsigned long __ino64_t;
typedef unsigned int __mode_t;
typedef unsigned long __nlink_t;
typedef long __off_t;
typedef long __off64_t;
typedef int __pid_t;
typedef struct {
    int __val[2];
} __fsid_t;
typedef long __clock_t;
typedef unsigned long __rlim_t;
typedef unsigned long __rlim64_t;
typedef unsigned int __id_t;
typedef long __time_t;
typedef unsigned int __useconds_t;
typedef long __suseconds_t;
typedef int __daddr_t;
typedef int __key_t;
typedef int __clockid_t;
typedef void *__timer_t;
typedef long __blksize_t;
typedef long __blkcnt_t;
typedef long __blkcnt64_t;
typedef unsigned long __fsblkcnt_t;
typedef unsigned long __fsblkcnt64_t;
typedef unsigned long __fsfilcnt_t;
typedef unsigned long __fsfilcnt64_t;
typedef long __fsword_t;
typedef long __ssize_t;
typedef long __syscall_slong_t;
typedef unsigned long __syscall_ulong_t;
typedef __off64_t __loff_t;
typedef char *__caddr_t;
typedef long __intptr_t;
typedef unsigned int __socklen_t;
typedef int __sig_atomic_t;
typedef __int8_t int8_t;
typedef __int16_t int16_t;
typedef __int32_t int32_t;
typedef __int64_t int64_t;
typedef __uint8_t uint8_t;
typedef __uint16_t uint16_t;
typedef __uint32_t uint32_t;
typedef __uint64_t uint64_t;
typedef __int_least8_t int_least8_t;
typedef __int_least16_t int_least16_t;
typedef __int_least32_t int_least32_t;
typedef __int_least64_t int_least64_t;
typedef __uint_least8_t uint_least8_t;
typedef __uint_least16_t uint_least16_t;
typedef __uint_least32_t uint_least32_t;
typedef __uint_least64_t uint_least64_t;
typedef signed char int_fast8_t;
typedef long int_fast16_t;
typedef long int_fast32_t;
typedef long int_fast64_t;
typedef unsigned char uint_fast8_t;
typedef unsigned long uint_fast16_t;
typedef unsigned long uint_fast32_t;
typedef unsigned long uint_fast64_t;
typedef long intptr_t;
typedef unsigned long uintptr_t;
typedef __intmax_t intmax_t;
typedef __uintmax_t uintmax_t;
typedef _Bool bool;
struct indirect_placeholder_13_ret_type;
struct indirect_placeholder_15_ret_type;
struct indirect_placeholder_13_ret_type {
    uint64_t field_0;
    uint64_t field_1;
    uint64_t field_2;
    uint64_t field_3;
};
struct indirect_placeholder_15_ret_type {
    uint64_t field_0;
    uint64_t field_1;
    uint64_t field_2;
    uint64_t field_3;
};
extern void abort(void);
extern uint64_t revng_init_local_sp(uint64_t param_0);
extern uint64_t init_rbx(void);
extern void indirect_placeholder(void);
extern uint64_t init_rbp(void);
extern uint64_t init_r14(void);
extern uint64_t init_r13(void);
extern uint64_t init_r12(void);
extern void indirect_placeholder_10(uint64_t param_0);
extern uint64_t indirect_placeholder_5(uint64_t param_0, uint64_t param_1);
extern uint64_t indirect_placeholder_11(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint64_t param_3, uint64_t param_4, uint64_t param_5);
extern void indirect_placeholder_12(uint64_t param_0, uint64_t param_1, uint64_t param_2);
extern uint64_t indirect_placeholder_6(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint64_t param_3, uint64_t param_4, uint64_t param_5, uint64_t param_6);
extern uint64_t init_r10(void);
extern struct indirect_placeholder_13_ret_type indirect_placeholder_13(uint64_t param_0, uint64_t param_1, uint64_t param_2);
extern void indirect_placeholder_14(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint64_t param_3, uint64_t param_4, uint64_t param_5, uint64_t param_6);
extern struct indirect_placeholder_15_ret_type indirect_placeholder_15(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint64_t param_3, uint64_t param_4);
void bb_main(uint64_t rdi, uint64_t rsi) {
    struct indirect_placeholder_15_ret_type var_12;
    uint64_t var_0;
    uint64_t var_1;
    uint64_t var_2;
    uint64_t var_3;
    uint64_t var_4;
    uint64_t var_5;
    uint64_t var_6;
    uint32_t var_7;
    uint64_t var_8;
    uint64_t var_9;
    uint64_t var_10;
    uint64_t r12_0_ph;
    uint64_t local_sp_6_ph;
    uint64_t local_sp_6;
    uint64_t r9_2;
    uint64_t var_51;
    uint64_t var_52;
    uint64_t var_53;
    uint64_t var_54;
    uint64_t r8_2;
    uint64_t local_sp_0;
    uint64_t var_45;
    uint64_t var_46;
    uint64_t var_47;
    uint64_t var_48;
    uint32_t var_49;
    uint64_t var_50;
    uint32_t var_55;
    uint32_t var_38;
    uint64_t var_36;
    uint64_t var_37;
    uint64_t _pre157;
    uint64_t _pre_phi;
    uint64_t var_28;
    uint64_t var_29;
    uint64_t var_30;
    uint64_t rax_0;
    uint64_t local_sp_1;
    uint64_t var_31;
    uint64_t var_32;
    uint32_t var_33;
    uint64_t var_34;
    uint64_t var_35;
    uint32_t _pre;
    uint32_t var_22;
    bool var_23;
    uint64_t var_24;
    struct indirect_placeholder_13_ret_type var_25;
    uint64_t var_26;
    uint64_t var_27;
    uint64_t r13_0;
    uint64_t local_sp_2;
    uint64_t r9_0;
    uint64_t r8_0;
    uint64_t local_sp_4;
    uint64_t r13_1;
    uint64_t local_sp_3;
    uint64_t r9_1;
    uint64_t r8_1;
    uint64_t var_39;
    uint64_t var_40;
    uint64_t r13_2;
    uint64_t var_41;
    uint64_t var_42;
    uint64_t var_43;
    uint64_t var_44;
    uint64_t local_sp_5;
    uint64_t var_18;
    uint64_t var_19;
    uint64_t var_20;
    uint64_t local_sp_6_be;
    uint64_t var_21;
    uint64_t var_11;
    uint64_t var_13;
    uint64_t var_14;
    uint64_t var_15;
    uint64_t var_16;
    uint32_t var_17;
    unsigned int loop_state_var;
    bool switch_state_var;
    var_0 = revng_init_local_sp(0UL);
    var_1 = init_rbx();
    var_2 = init_r14();
    var_3 = init_rbp();
    var_4 = init_r13();
    var_5 = init_r12();
    var_6 = init_r10();
    *(uint64_t *)(var_0 + (-8L)) = var_2;
    *(uint64_t *)(var_0 + (-16L)) = var_4;
    *(uint64_t *)(var_0 + (-24L)) = var_5;
    *(uint64_t *)(var_0 + (-32L)) = var_3;
    *(uint64_t *)(var_0 + (-40L)) = var_1;
    var_7 = (uint32_t)rdi;
    var_8 = (uint64_t)var_7;
    var_9 = *(uint64_t *)rsi;
    *(uint64_t *)(var_0 + (-48L)) = 4201814UL;
    indirect_placeholder_10(var_9);
    *(uint64_t *)(var_0 + (-56L)) = 4201829UL;
    indirect_placeholder();
    var_10 = var_0 + (-64L);
    *(uint64_t *)var_10 = 4201839UL;
    indirect_placeholder();
    r12_0_ph = 0UL;
    local_sp_6_ph = var_10;
    r13_0 = 438UL;
    r8_2 = 0UL;
    while (1U)
        {
            local_sp_6 = local_sp_6_ph;
            while (1U)
                {
                    var_11 = local_sp_6 + (-8L);
                    *(uint64_t *)var_11 = 4202031UL;
                    var_12 = indirect_placeholder_15(4247583UL, var_8, 4248640UL, rsi, 0UL);
                    var_13 = var_12.field_0;
                    var_14 = var_12.field_1;
                    var_15 = var_12.field_2;
                    var_16 = var_12.field_3;
                    var_17 = (uint32_t)var_13;
                    local_sp_6_ph = var_11;
                    local_sp_2 = var_11;
                    r9_0 = var_15;
                    r8_0 = var_16;
                    local_sp_5 = var_11;
                    local_sp_6_be = var_11;
                    if ((uint64_t)(var_17 + 1U) != 0UL) {
                        var_22 = *(uint32_t *)6358108UL;
                        _pre = var_22;
                        if ((uint64_t)(var_22 - var_7) == 0UL) {
                            *(uint64_t *)(local_sp_6 + (-16L)) = 4202073UL;
                            indirect_placeholder_6(0UL, 4247587UL, 0UL, var_14, 0UL, var_15, var_16);
                            *(uint64_t *)(local_sp_6 + (-24L)) = 4202083UL;
                            indirect_placeholder_12(rsi, var_8, 1UL);
                            abort();
                        }
                        var_23 = (r12_0_ph == 0UL);
                        if (!var_23) {
                            loop_state_var = 0U;
                            break;
                        }
                        var_24 = local_sp_6 + (-16L);
                        *(uint64_t *)var_24 = 4202096UL;
                        var_25 = indirect_placeholder_13(r12_0_ph, var_15, var_16);
                        var_26 = var_25.field_0;
                        var_27 = var_25.field_2;
                        r9_2 = var_27;
                        rax_0 = var_26;
                        local_sp_1 = var_24;
                        r9_0 = var_27;
                        r8_0 = 0UL;
                        if (var_26 != 0UL) {
                            loop_state_var = 1U;
                            break;
                        }
                        var_28 = var_25.field_3;
                        var_29 = var_25.field_1;
                        var_30 = local_sp_6 + (-24L);
                        *(uint64_t *)var_30 = 4202129UL;
                        indirect_placeholder_6(0UL, 4247603UL, 1UL, var_29, 0UL, var_27, var_28);
                        rax_0 = 0UL;
                        local_sp_1 = var_30;
                        loop_state_var = 1U;
                        break;
                    }
                    if ((uint64_t)(var_17 + 130U) == 0UL) {
                        *(uint64_t *)(local_sp_6 + (-16L)) = 4201939UL;
                        indirect_placeholder_12(rsi, var_8, 0UL);
                        abort();
                    }
                    if ((int)var_17 > (int)4294967166U) {
                        if ((uint64_t)(var_17 + (-90)) != 0UL) {
                            loop_state_var = 2U;
                            break;
                        }
                        if (*(uint64_t *)6358744UL == 0UL) {
                            var_21 = local_sp_6 + (-16L);
                            *(uint64_t *)var_21 = 4201927UL;
                            indirect_placeholder_6(0UL, 4248488UL, 0UL, var_14, 0UL, var_15, var_16);
                            local_sp_6_be = var_21;
                        }
                        local_sp_6 = local_sp_6_be;
                        continue;
                    }
                    if ((uint64_t)(var_17 + 131U) != 0UL) {
                        loop_state_var = 3U;
                        break;
                    }
                    var_18 = *(uint64_t *)6357984UL;
                    var_19 = *(uint64_t *)6358144UL;
                    *(uint64_t *)(local_sp_6 + (-16L)) = 4201985UL;
                    indirect_placeholder_14(0UL, 4247526UL, var_19, var_18, 4247424UL, 0UL, 4247567UL);
                    var_20 = local_sp_6 + (-24L);
                    *(uint64_t *)var_20 = 4201995UL;
                    indirect_placeholder();
                    local_sp_5 = var_20;
                    loop_state_var = 3U;
                    break;
                }
            switch_state_var = 0;
            switch (loop_state_var) {
              case 2U:
                {
                    if ((uint64_t)(var_17 + (-109)) != 0UL) {
                        loop_state_var = 2U;
                        switch_state_var = 1;
                        break;
                    }
                    r12_0_ph = *(uint64_t *)6358744UL;
                    continue;
                }
                break;
              case 1U:
                {
                    *(uint64_t *)(local_sp_1 + (-8L)) = 4202139UL;
                    indirect_placeholder();
                    var_31 = (uint64_t)(uint32_t)rax_0;
                    *(uint64_t *)(local_sp_1 + (-16L)) = 4202149UL;
                    indirect_placeholder();
                    *(uint64_t *)(local_sp_1 + (-24L)) = 4202176UL;
                    var_32 = indirect_placeholder_11(var_31, 438UL, var_26, var_6, 0UL, 0UL);
                    var_33 = (uint32_t)var_32;
                    var_34 = (uint64_t)var_33;
                    var_35 = local_sp_1 + (-32L);
                    *(uint64_t *)var_35 = 4202187UL;
                    indirect_placeholder();
                    r13_0 = var_34;
                    local_sp_2 = var_35;
                    r13_2 = var_34;
                    if ((uint64_t)(var_33 & (-512)) == 0UL) {
                        _pre = *(uint32_t *)6358108UL;
                        loop_state_var = 1U;
                        switch_state_var = 1;
                        break;
                    }
                    var_36 = local_sp_1 + (-40L);
                    *(uint64_t *)var_36 = 4202240UL;
                    var_37 = indirect_placeholder_6(0UL, 4248568UL, 1UL, var_26, 0UL, var_27, 0UL);
                    _pre157 = var_37 << 32UL;
                    _pre_phi = _pre157;
                    local_sp_4 = var_36;
                    loop_state_var = 0U;
                    switch_state_var = 1;
                    break;
                }
                break;
              case 0U:
                {
                    loop_state_var = 1U;
                    switch_state_var = 1;
                    break;
                }
                break;
              case 3U:
                {
                    loop_state_var = 2U;
                    switch_state_var = 1;
                    break;
                }
                break;
            }
            if (switch_state_var)
                break;
        }
    switch (loop_state_var) {
      case 2U:
        {
            *(uint64_t *)(local_sp_5 + (-8L)) = 4202005UL;
            indirect_placeholder_12(rsi, var_8, 1UL);
            abort();
        }
        break;
      case 1U:
      case 0U:
        {
            switch (loop_state_var) {
              case 1U:
                {
                    var_38 = _pre;
                    r13_1 = r13_0;
                    local_sp_3 = local_sp_2;
                    r9_1 = r9_0;
                    r8_1 = r8_0;
                }
                break;
              case 0U:
                {
                    var_41 = *(uint64_t *)((uint64_t)((long)_pre_phi >> (long)29UL) + rsi);
                    var_42 = (uint64_t)(uint32_t)r13_2;
                    var_43 = local_sp_4 + (-8L);
                    *(uint64_t *)var_43 = 4202255UL;
                    var_44 = indirect_placeholder_5(var_41, var_42);
                    local_sp_0 = var_43;
                    r13_1 = r13_2;
                    r9_1 = r9_2;
                    r8_1 = r8_2;
                    if ((uint64_t)(uint32_t)var_44 == 0UL) {
                        var_45 = *(uint64_t *)(((uint64_t)*(uint32_t *)6358108UL << 3UL) + rsi);
                        *(uint64_t *)(local_sp_4 + (-16L)) = 4202280UL;
                        var_46 = indirect_placeholder_5(4UL, var_45);
                        *(uint64_t *)(local_sp_4 + (-24L)) = 4202288UL;
                        indirect_placeholder();
                        var_47 = (uint64_t)*(uint32_t *)var_46;
                        var_48 = local_sp_4 + (-32L);
                        *(uint64_t *)var_48 = 4202313UL;
                        indirect_placeholder_6(0UL, 4247616UL, 0UL, var_46, var_47, r9_2, r8_2);
                        local_sp_0 = var_48;
                    } else {
                        var_49 = *(uint32_t *)6358108UL;
                        var_50 = local_sp_4 + (-16L);
                        *(uint64_t *)var_50 = 4202345UL;
                        indirect_placeholder();
                        local_sp_0 = var_50;
                        if (!var_23 && var_49 == 0U) {
                            var_51 = *(uint64_t *)(((uint64_t)*(uint32_t *)6358108UL << 3UL) + rsi);
                            *(uint64_t *)(local_sp_4 + (-24L)) = 4202370UL;
                            var_52 = indirect_placeholder_5(4UL, var_51);
                            *(uint64_t *)(local_sp_4 + (-32L)) = 4202378UL;
                            indirect_placeholder();
                            var_53 = (uint64_t)*(uint32_t *)var_52;
                            var_54 = local_sp_4 + (-40L);
                            *(uint64_t *)var_54 = 4202403UL;
                            indirect_placeholder_6(0UL, 4247638UL, 0UL, var_52, var_53, r9_2, r8_2);
                            local_sp_0 = var_54;
                        }
                    }
                    var_55 = *(uint32_t *)6358108UL + 1U;
                    *(uint32_t *)6358108UL = var_55;
                    var_38 = var_55;
                    local_sp_3 = local_sp_0;
                }
                break;
            }
            var_39 = rdi << 32UL;
            var_40 = (uint64_t)var_38 << 32UL;
            r9_2 = r9_1;
            r8_2 = r8_1;
            _pre_phi = var_40;
            local_sp_4 = local_sp_3;
            r13_2 = r13_1;
            while ((long)var_39 <= (long)var_40)
                {
                    var_41 = *(uint64_t *)((uint64_t)((long)_pre_phi >> (long)29UL) + rsi);
                    var_42 = (uint64_t)(uint32_t)r13_2;
                    var_43 = local_sp_4 + (-8L);
                    *(uint64_t *)var_43 = 4202255UL;
                    var_44 = indirect_placeholder_5(var_41, var_42);
                    local_sp_0 = var_43;
                    r13_1 = r13_2;
                    r9_1 = r9_2;
                    r8_1 = r8_2;
                    if ((uint64_t)(uint32_t)var_44 == 0UL) {
                        var_45 = *(uint64_t *)(((uint64_t)*(uint32_t *)6358108UL << 3UL) + rsi);
                        *(uint64_t *)(local_sp_4 + (-16L)) = 4202280UL;
                        var_46 = indirect_placeholder_5(4UL, var_45);
                        *(uint64_t *)(local_sp_4 + (-24L)) = 4202288UL;
                        indirect_placeholder();
                        var_47 = (uint64_t)*(uint32_t *)var_46;
                        var_48 = local_sp_4 + (-32L);
                        *(uint64_t *)var_48 = 4202313UL;
                        indirect_placeholder_6(0UL, 4247616UL, 0UL, var_46, var_47, r9_2, r8_2);
                        local_sp_0 = var_48;
                    } else {
                        var_49 = *(uint32_t *)6358108UL;
                        var_50 = local_sp_4 + (-16L);
                        *(uint64_t *)var_50 = 4202345UL;
                        indirect_placeholder();
                        local_sp_0 = var_50;
                        if (!var_23 && var_49 == 0U) {
                            var_51 = *(uint64_t *)(((uint64_t)*(uint32_t *)6358108UL << 3UL) + rsi);
                            *(uint64_t *)(local_sp_4 + (-24L)) = 4202370UL;
                            var_52 = indirect_placeholder_5(4UL, var_51);
                            *(uint64_t *)(local_sp_4 + (-32L)) = 4202378UL;
                            indirect_placeholder();
                            var_53 = (uint64_t)*(uint32_t *)var_52;
                            var_54 = local_sp_4 + (-40L);
                            *(uint64_t *)var_54 = 4202403UL;
                            indirect_placeholder_6(0UL, 4247638UL, 0UL, var_52, var_53, r9_2, r8_2);
                            local_sp_0 = var_54;
                        }
                    }
                    var_55 = *(uint32_t *)6358108UL + 1U;
                    *(uint32_t *)6358108UL = var_55;
                    var_38 = var_55;
                    local_sp_3 = local_sp_0;
                    var_39 = rdi << 32UL;
                    var_40 = (uint64_t)var_38 << 32UL;
                    r9_2 = r9_1;
                    r8_2 = r8_1;
                    _pre_phi = var_40;
                    local_sp_4 = local_sp_3;
                    r13_2 = r13_1;
                }
            return;
        }
        break;
    }
}

typedef unsigned char __u_char;
typedef unsigned short __u_short;
typedef unsigned int __u_int;
typedef unsigned long __u_long;
typedef signed char __int8_t;
typedef unsigned char __uint8_t;
typedef short __int16_t;
typedef unsigned short __uint16_t;
typedef int __int32_t;
typedef unsigned int __uint32_t;
typedef long __int64_t;
typedef unsigned long __uint64_t;
typedef __int8_t __int_least8_t;
typedef __uint8_t __uint_least8_t;
typedef __int16_t __int_least16_t;
typedef __uint16_t __uint_least16_t;
typedef __int32_t __int_least32_t;
typedef __uint32_t __uint_least32_t;
typedef __int64_t __int_least64_t;
typedef __uint64_t __uint_least64_t;
typedef long __quad_t;
typedef unsigned long __u_quad_t;
typedef long __intmax_t;
typedef unsigned long __uintmax_t;
typedef unsigned long __dev_t;
typedef unsigned int __uid_t;
typedef unsigned int __gid_t;
typedef unsigned long __ino_t;
typedef unsigned long __ino64_t;
typedef unsigned int __mode_t;
typedef unsigned long __nlink_t;
typedef long __off_t;
typedef long __off64_t;
typedef int __pid_t;
typedef struct {
    int __val[2];
} __fsid_t;
typedef long __clock_t;
typedef unsigned long __rlim_t;
typedef unsigned long __rlim64_t;
typedef unsigned int __id_t;
typedef long __time_t;
typedef unsigned int __useconds_t;
typedef long __suseconds_t;
typedef int __daddr_t;
typedef int __key_t;
typedef int __clockid_t;
typedef void *__timer_t;
typedef long __blksize_t;
typedef long __blkcnt_t;
typedef long __blkcnt64_t;
typedef unsigned long __fsblkcnt_t;
typedef unsigned long __fsblkcnt64_t;
typedef unsigned long __fsfilcnt_t;
typedef unsigned long __fsfilcnt64_t;
typedef long __fsword_t;
typedef long __ssize_t;
typedef long __syscall_slong_t;
typedef unsigned long __syscall_ulong_t;
typedef __off64_t __loff_t;
typedef char *__caddr_t;
typedef long __intptr_t;
typedef unsigned int __socklen_t;
typedef int __sig_atomic_t;
typedef __int8_t int8_t;
typedef __int16_t int16_t;
typedef __int32_t int32_t;
typedef __int64_t int64_t;
typedef __uint8_t uint8_t;
typedef __uint16_t uint16_t;
typedef __uint32_t uint32_t;
typedef __uint64_t uint64_t;
typedef __int_least8_t int_least8_t;
typedef __int_least16_t int_least16_t;
typedef __int_least32_t int_least32_t;
typedef __int_least64_t int_least64_t;
typedef __uint_least8_t uint_least8_t;
typedef __uint_least16_t uint_least16_t;
typedef __uint_least32_t uint_least32_t;
typedef __uint_least64_t uint_least64_t;
typedef signed char int_fast8_t;
typedef long int_fast16_t;
typedef long int_fast32_t;
typedef long int_fast64_t;
typedef unsigned char uint_fast8_t;
typedef unsigned long uint_fast16_t;
typedef unsigned long uint_fast32_t;
typedef unsigned long uint_fast64_t;
typedef long intptr_t;
typedef unsigned long uintptr_t;
typedef __intmax_t intmax_t;
typedef __uintmax_t uintmax_t;
typedef _Bool bool;
struct indirect_placeholder_11_ret_type;
struct indirect_placeholder_12_ret_type;
struct helper_idivl_EAX_wrapper_ret_type;
struct type_7;
struct indirect_placeholder_13_ret_type;
struct indirect_placeholder_14_ret_type;
struct indirect_placeholder_11_ret_type {
    uint64_t field_0;
    uint64_t field_1;
    uint64_t field_2;
    uint64_t field_3;
    uint64_t field_4;
    uint64_t field_5;
};
struct indirect_placeholder_12_ret_type {
    uint64_t field_0;
    uint64_t field_1;
    uint64_t field_2;
    uint64_t field_3;
    uint64_t field_4;
    uint64_t field_5;
};
struct helper_idivl_EAX_wrapper_ret_type {
    uint64_t field_0;
    uint64_t field_1;
    uint64_t field_2;
    uint64_t field_3;
    uint32_t field_4;
    uint32_t field_5;
    uint64_t field_6;
    uint32_t field_7;
    uint64_t field_8;
    uint32_t field_9;
    unsigned char field_10;
    uint32_t field_11;
    uint32_t field_12;
};
struct type_7 {
};
struct indirect_placeholder_13_ret_type {
    uint64_t field_0;
    uint64_t field_1;
    uint64_t field_2;
    uint64_t field_3;
    uint64_t field_4;
    uint64_t field_5;
};
struct indirect_placeholder_14_ret_type {
    uint64_t field_0;
    uint64_t field_1;
    uint64_t field_2;
    uint64_t field_3;
    uint64_t field_4;
    uint64_t field_5;
};
extern void abort(void);
extern uint64_t revng_init_local_sp(uint64_t param_0);
extern uint64_t init_rbx(void);
extern void indirect_placeholder(void);
extern uint64_t init_rbp(void);
extern uint64_t init_r14(void);
extern uint64_t init_r13(void);
extern uint64_t init_r12(void);
extern uint64_t indirect_placeholder_1(uint64_t param_0);
extern uint64_t init_r15(void);
extern uint32_t init_state_0x8248(void);
extern uint64_t init_state_0x9018(void);
extern uint32_t init_state_0x9010(void);
extern uint64_t init_state_0x8408(void);
extern uint64_t init_state_0x8328(void);
extern uint64_t init_state_0x82d8(void);
extern uint32_t init_state_0x9080(void);
extern uint64_t indirect_placeholder_10(uint64_t param_0, uint64_t param_1, uint64_t param_2);
extern void indirect_placeholder_6(uint64_t param_0);
extern uint64_t indirect_placeholder_7(uint64_t param_0, uint64_t param_1);
extern void indirect_placeholder_8(uint64_t param_0, uint64_t param_1, uint64_t param_2);
extern void indirect_placeholder_9(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint64_t param_3, uint64_t param_4, uint64_t param_5, uint64_t param_6);
extern struct indirect_placeholder_11_ret_type indirect_placeholder_11(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint64_t param_3, uint64_t param_4, uint64_t param_5);
extern struct indirect_placeholder_12_ret_type indirect_placeholder_12(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint64_t param_3, uint64_t param_4, uint64_t param_5);
extern struct helper_idivl_EAX_wrapper_ret_type helper_idivl_EAX_wrapper(struct type_7 *param_0, uint64_t param_1, uint64_t param_2, uint64_t param_3, uint64_t param_4, uint64_t param_5, uint64_t param_6, uint64_t param_7, uint64_t param_8, uint64_t param_9, uint64_t param_10, uint64_t param_11, uint64_t param_12, uint32_t param_13, uint64_t param_14, uint32_t param_15, uint64_t param_16, uint64_t param_17, uint64_t param_18, uint32_t param_19);
extern struct indirect_placeholder_13_ret_type indirect_placeholder_13(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint64_t param_3, uint64_t param_4, uint64_t param_5);
extern struct indirect_placeholder_14_ret_type indirect_placeholder_14(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint64_t param_3, uint64_t param_4);
void bb_main(uint64_t rdi, uint64_t rsi) {
    struct indirect_placeholder_14_ret_type var_23;
    uint64_t var_0;
    uint64_t var_1;
    uint64_t var_2;
    uint64_t var_3;
    uint64_t var_4;
    uint64_t var_5;
    uint64_t var_6;
    uint32_t var_7;
    uint64_t var_8;
    uint32_t var_9;
    uint64_t var_10;
    uint64_t var_11;
    uint64_t var_12;
    uint32_t var_13;
    uint32_t var_14;
    uint64_t var_15;
    uint64_t *_cast;
    uint64_t var_16;
    uint64_t var_17;
    uint64_t rbx_0;
    uint64_t var_79;
    uint64_t var_80;
    uint64_t var_81;
    uint64_t r8_1;
    uint64_t rax_0;
    uint64_t local_sp_3;
    uint64_t var_77;
    uint64_t var_78;
    uint64_t var_82;
    uint64_t var_83;
    uint64_t var_84;
    uint64_t local_sp_0;
    uint64_t var_85;
    uint64_t var_86;
    uint32_t var_87;
    uint64_t rdi1_0;
    uint64_t rdx_0;
    bool var_75;
    uint64_t *var_76;
    uint64_t var_49;
    uint64_t var_50;
    uint64_t r12_1;
    struct indirect_placeholder_11_ret_type var_51;
    uint64_t var_52;
    uint64_t var_53;
    uint64_t var_54;
    uint64_t r9_1;
    uint64_t local_sp_5;
    uint64_t var_40;
    struct indirect_placeholder_12_ret_type var_41;
    uint64_t var_42;
    uint64_t var_43;
    uint64_t var_44;
    uint64_t var_45;
    uint64_t var_46;
    uint64_t var_47;
    uint32_t var_48;
    uint32_t var_63;
    uint64_t rax_3;
    uint64_t r10_0;
    uint64_t rsi2_0;
    uint64_t r9_0;
    uint64_t r8_0;
    uint64_t local_sp_1;
    uint64_t rbp_0;
    uint64_t r12_0;
    struct helper_idivl_EAX_wrapper_ret_type var_64;
    uint64_t var_65;
    uint64_t local_sp_2;
    uint32_t var_66;
    uint64_t var_67;
    bool var_68;
    uint64_t var_69;
    uint64_t rax_1;
    uint64_t var_70;
    uint64_t var_71;
    bool var_72;
    uint64_t var_73;
    uint64_t *var_74;
    uint64_t var_55;
    uint64_t var_56;
    struct indirect_placeholder_13_ret_type var_57;
    uint64_t var_58;
    uint64_t var_59;
    uint32_t var_60;
    uint32_t var_61;
    uint64_t var_62;
    uint32_t _pre_phi;
    uint64_t rax_2;
    uint64_t var_39;
    uint64_t rax_5;
    uint64_t r14_0;
    uint64_t rax_4;
    uint64_t local_sp_4;
    uint64_t r13_0_be;
    uint64_t r12_1_be;
    uint64_t local_sp_5_be;
    uint64_t r13_0;
    uint64_t var_26;
    uint64_t var_27;
    uint64_t var_28;
    uint64_t var_29;
    uint64_t var_30;
    uint64_t var_31;
    uint64_t var_32;
    uint64_t var_33;
    uint64_t var_34;
    uint32_t var_35;
    uint64_t var_36;
    uint64_t r14_1;
    uint64_t var_37;
    uint64_t var_38;
    uint64_t var_18;
    uint64_t *var_19;
    uint64_t var_20;
    uint64_t var_21;
    uint64_t var_22;
    uint64_t var_24;
    uint32_t var_25;
    unsigned int loop_state_var;
    var_0 = revng_init_local_sp(0UL);
    var_1 = init_rbx();
    var_2 = init_r14();
    var_3 = init_rbp();
    var_4 = init_r13();
    var_5 = init_r12();
    var_6 = init_r15();
    var_7 = init_state_0x8248();
    var_8 = init_state_0x9018();
    var_9 = init_state_0x9010();
    var_10 = init_state_0x8408();
    var_11 = init_state_0x8328();
    var_12 = init_state_0x82d8();
    var_13 = init_state_0x9080();
    *(uint64_t *)(var_0 + (-8L)) = var_6;
    *(uint64_t *)(var_0 + (-16L)) = var_2;
    *(uint64_t *)(var_0 + (-24L)) = var_4;
    *(uint64_t *)(var_0 + (-32L)) = var_5;
    *(uint64_t *)(var_0 + (-40L)) = var_3;
    *(uint64_t *)(var_0 + (-48L)) = var_1;
    var_14 = (uint32_t)rdi;
    var_15 = (uint64_t)var_14;
    _cast = (uint64_t *)rsi;
    var_16 = *_cast;
    *(uint64_t *)(var_0 + (-64L)) = 4205170UL;
    indirect_placeholder_6(var_16);
    *(uint64_t *)(var_0 + (-72L)) = 4205185UL;
    indirect_placeholder();
    var_17 = var_0 + (-80L);
    *(uint64_t *)var_17 = 4205195UL;
    indirect_placeholder();
    *(unsigned char *)6408112UL = (unsigned char)'\x00';
    *(unsigned char *)6408113UL = (unsigned char)'\x00';
    *(unsigned char *)6408114UL = (unsigned char)'\x00';
    *(unsigned char *)6408115UL = (unsigned char)'\x00';
    *(uint32_t *)6408096UL = 75U;
    *(uint64_t *)6408104UL = 4257671UL;
    *(uint32_t *)6408092UL = 0U;
    *(uint32_t *)6408088UL = 0U;
    *(uint32_t *)6408084UL = 0U;
    rbx_0 = rsi;
    local_sp_5 = var_17;
    rbp_0 = var_15;
    r12_0 = 0UL;
    rax_4 = 1UL;
    r13_0 = 0UL;
    var_18 = rsi + 8UL;
    var_19 = (uint64_t *)var_18;
    var_20 = *var_19;
    var_21 = var_20 + 1UL;
    if ((int)var_14 <= (int)1U & *(unsigned char *)var_20 != '-' & (uint64_t)(((uint32_t)(uint64_t)*(unsigned char *)var_21 + (-48)) & (-2)) > 9UL) {
        *var_19 = *_cast;
        rbx_0 = var_18;
        rbp_0 = (uint64_t)(var_14 + (-1));
        r12_0 = var_21;
    }
    r12_1 = r12_0;
    while (1U)
        {
            var_22 = local_sp_5 + (-8L);
            *(uint64_t *)var_22 = 4205752UL;
            var_23 = indirect_placeholder_14(4251589UL, rbp_0, 4253184UL, rbx_0, 0UL);
            var_24 = var_23.field_0;
            var_25 = (uint32_t)var_24;
            local_sp_1 = var_22;
            _pre_phi = var_25;
            rax_5 = var_24;
            local_sp_4 = var_22;
            r13_0_be = r13_0;
            r12_1_be = r12_1;
            local_sp_5_be = var_22;
            if ((uint64_t)(var_25 + 1U) != 0UL) {
                if (r12_1 == 0UL) {
                    var_40 = local_sp_5 + (-16L);
                    *(uint64_t *)var_40 = 4205805UL;
                    var_41 = indirect_placeholder_12(2500UL, r12_1, 4257671UL, 0UL, 0UL, 4251610UL);
                    var_42 = var_41.field_0;
                    var_43 = var_41.field_1;
                    var_44 = var_41.field_2;
                    var_45 = var_41.field_3;
                    var_46 = var_41.field_4;
                    var_47 = var_41.field_5;
                    var_48 = (uint32_t)var_42;
                    *(uint32_t *)6408096UL = var_48;
                    rdi1_0 = var_43;
                    var_63 = var_48;
                    r10_0 = var_44;
                    rsi2_0 = var_45;
                    r9_0 = var_46;
                    r8_0 = var_47;
                    local_sp_1 = var_40;
                    if (r13_0 != 0UL) {
                        loop_state_var = 1U;
                        break;
                    }
                    var_49 = (uint64_t)((long)(var_42 << 32UL) >> (long)32UL);
                    var_50 = local_sp_5 + (-24L);
                    *(uint64_t *)var_50 = 4206214UL;
                    var_51 = indirect_placeholder_11(var_49, r13_0, 4257671UL, 0UL, 0UL, 4251610UL);
                    var_52 = var_51.field_0;
                    var_53 = var_51.field_4;
                    var_54 = var_51.field_5;
                    *(uint32_t *)6408080UL = (uint32_t)var_52;
                    r9_1 = var_53;
                    r8_1 = var_54;
                    rax_0 = var_52;
                    local_sp_2 = var_50;
                    loop_state_var = 0U;
                    break;
                }
                rdi1_0 = var_23.field_1;
                r10_0 = var_23.field_2;
                rsi2_0 = var_23.field_3;
                r9_0 = var_23.field_4;
                r8_0 = var_23.field_5;
                if (r13_0 != 0UL) {
                    var_63 = *(uint32_t *)6408096UL;
                    loop_state_var = 1U;
                    break;
                }
                var_55 = (uint64_t)*(uint32_t *)6408096UL;
                var_56 = local_sp_5 + (-16L);
                *(uint64_t *)var_56 = 4206151UL;
                var_57 = indirect_placeholder_13(var_55, r13_0, 4257671UL, 0UL, 0UL, 4251610UL);
                var_58 = var_57.field_4;
                var_59 = var_57.field_5;
                var_60 = (uint32_t)var_57.field_0;
                *(uint32_t *)6408080UL = var_60;
                var_61 = var_60 + 10U;
                var_62 = (uint64_t)var_61;
                *(uint32_t *)6408096UL = var_61;
                r9_1 = var_58;
                r8_1 = var_59;
                rax_0 = var_62;
                local_sp_2 = var_56;
                loop_state_var = 0U;
                break;
            }
            if ((uint64_t)(var_25 + (-112)) == 0UL) {
                var_33 = *(uint64_t *)6408600UL;
                *(uint32_t *)6408088UL = 0U;
                r14_0 = var_33;
                r14_1 = var_33;
                if (*(unsigned char *)var_33 != ' ') {
                    var_34 = r14_0 + 1UL;
                    var_35 = (uint32_t)rax_4;
                    var_36 = (uint64_t)(var_35 + 1U);
                    r14_0 = var_34;
                    rax_4 = var_36;
                    r14_1 = var_34;
                    rax_5 = var_36;
                    do {
                        var_34 = r14_0 + 1UL;
                        var_35 = (uint32_t)rax_4;
                        var_36 = (uint64_t)(var_35 + 1U);
                        r14_0 = var_34;
                        rax_4 = var_36;
                        r14_1 = var_34;
                        rax_5 = var_36;
                    } while (*(unsigned char *)var_34 != ' ');
                    *(uint32_t *)6408088UL = var_35;
                    _pre_phi = (uint32_t)var_36;
                }
                *(uint64_t *)6408104UL = r14_1;
                var_37 = local_sp_5 + (-16L);
                *(uint64_t *)var_37 = 4205604UL;
                indirect_placeholder();
                *(uint32_t *)6408092UL = _pre_phi;
                var_38 = (uint64_t)((long)(rax_5 << 32UL) >> (long)32UL) + r14_1;
                rax_2 = var_38;
                rax_3 = var_38;
                local_sp_5_be = var_37;
                if (var_38 <= r14_1 & *(unsigned char *)(var_38 + (-1L)) == ' ') {
                    var_39 = rax_2 + (-1L);
                    rax_2 = var_39;
                    rax_3 = r14_1;
                    while (var_39 != r14_1)
                        {
                            rax_3 = var_39;
                            if (*(unsigned char *)(rax_2 + (-2L)) == ' ') {
                                break;
                            }
                            var_39 = rax_2 + (-1L);
                            rax_2 = var_39;
                            rax_3 = r14_1;
                        }
                }
                *(unsigned char *)rax_3 = (unsigned char)'\x00';
                *(uint32_t *)6408084UL = ((uint32_t)rax_3 - (uint32_t)r14_1);
            } else {
                if ((int)var_25 > (int)112U) {
                    if ((uint64_t)(var_25 + (-116)) == 0UL) {
                        *(unsigned char *)6408114UL = (unsigned char)'\x01';
                    } else {
                        if ((int)var_25 > (int)116U) {
                            if ((uint64_t)(var_25 + (-117)) == 0UL) {
                                *(unsigned char *)6408112UL = (unsigned char)'\x01';
                            } else {
                                if ((uint64_t)(var_25 + (-119)) != 0UL) {
                                    loop_state_var = 2U;
                                    break;
                                }
                                r12_1_be = *(uint64_t *)6408600UL;
                            }
                        } else {
                            if ((uint64_t)(var_25 + (-115)) != 0UL) {
                                loop_state_var = 2U;
                                break;
                            }
                            *(unsigned char *)6408113UL = (unsigned char)'\x01';
                        }
                    }
                } else {
                    if ((uint64_t)(var_25 + 130U) == 0UL) {
                        *(uint64_t *)(local_sp_5 + (-16L)) = 4205670UL;
                        indirect_placeholder_8(rbx_0, rbp_0, 0UL);
                        abort();
                    }
                    if ((int)var_25 > (int)4294967166U) {
                        if ((uint64_t)(var_25 + (-99)) == 0UL) {
                            *(unsigned char *)6408115UL = (unsigned char)'\x01';
                        } else {
                            if ((uint64_t)(var_25 + (-103)) != 0UL) {
                                loop_state_var = 2U;
                                break;
                            }
                            r13_0_be = *(uint64_t *)6408600UL;
                        }
                    } else {
                        if ((uint64_t)(var_25 + 131U) != 0UL) {
                            loop_state_var = 2U;
                            break;
                        }
                        var_26 = *(uint64_t *)6362624UL;
                        var_27 = *(uint64_t *)6362752UL;
                        *(uint64_t *)(local_sp_5 + (-16L)) = 4205716UL;
                        indirect_placeholder_9(0UL, 4251534UL, var_27, var_26, 4251435UL, 0UL, 4251575UL);
                        var_28 = local_sp_5 + (-24L);
                        *(uint64_t *)var_28 = 4205726UL;
                        indirect_placeholder();
                        local_sp_5_be = var_28;
                    }
                }
            }
            r13_0 = r13_0_be;
            r12_1 = r12_1_be;
            local_sp_5 = local_sp_5_be;
            continue;
        }
    switch (loop_state_var) {
      case 2U:
        {
            if ((uint64_t)((var_25 + (-48)) & (-2)) <= 9UL) {
                var_29 = var_23.field_5;
                var_30 = var_23.field_4;
                var_31 = (uint64_t)var_25;
                var_32 = local_sp_5 + (-16L);
                *(uint64_t *)var_32 = 4205458UL;
                indirect_placeholder_9(0UL, 4252968UL, 0UL, var_31, 0UL, var_30, var_29);
                local_sp_4 = var_32;
            }
            *(uint64_t *)(local_sp_4 + (-8L)) = 4205468UL;
            indirect_placeholder_8(rbx_0, rbp_0, 1UL);
            abort();
        }
        break;
      case 1U:
      case 0U:
        {
            switch (loop_state_var) {
              case 1U:
                {
                    var_64 = helper_idivl_EAX_wrapper((struct type_7 *)(0UL), 200UL, (uint64_t)(var_63 * 187U), 4205836UL, (uint64_t)(uint32_t)(uint64_t)((long)((uint64_t)var_63 * 803158884352UL) >> (long)63UL), rbx_0, rbp_0, rdi1_0, 200UL, r10_0, rsi2_0, r9_0, r8_0, var_7, var_8, var_9, var_10, var_11, var_12, var_13);
                    var_65 = var_64.field_0;
                    *(uint32_t *)6408080UL = (uint32_t)var_65;
                    r9_1 = r9_0;
                    r8_1 = r8_0;
                    rax_0 = var_65;
                    local_sp_2 = local_sp_1;
                }
                break;
              case 0U:
                {
                    var_66 = *(uint32_t *)6362748UL;
                    var_67 = (uint64_t)var_66;
                    var_68 = ((uint64_t)((uint32_t)rbp_0 - var_66) == 0UL);
                    var_69 = *(uint64_t *)6362760UL;
                    rdx_0 = var_67;
                    rax_1 = rax_0;
                    local_sp_3 = local_sp_2;
                    if (!var_68) {
                        var_70 = rbp_0 << 32UL;
                        if ((long)var_70 > (long)(var_67 << 32UL)) {
                            while (1U)
                                {
                                    var_71 = *(uint64_t *)((uint64_t)((long)(rdx_0 << 32UL) >> (long)29UL) + rbx_0);
                                    *(uint64_t *)(local_sp_3 + (-8L)) = 4205919UL;
                                    indirect_placeholder();
                                    var_72 = ((uint64_t)(uint32_t)rax_1 == 0UL);
                                    var_73 = local_sp_3 + (-16L);
                                    var_74 = (uint64_t *)var_73;
                                    local_sp_0 = var_73;
                                    if (var_72) {
                                        *var_74 = 4205931UL;
                                        indirect_placeholder_6(var_69);
                                    } else {
                                        *var_74 = 4205949UL;
                                        indirect_placeholder();
                                        var_75 = (rax_1 == 0UL);
                                        var_76 = (uint64_t *)(local_sp_3 + (-24L));
                                        if (var_75) {
                                            *var_76 = 4206050UL;
                                            var_82 = indirect_placeholder_7(4UL, var_71);
                                            *(uint64_t *)(local_sp_3 + (-32L)) = 4206058UL;
                                            indirect_placeholder();
                                            var_83 = (uint64_t)*(uint32_t *)var_82;
                                            var_84 = local_sp_3 + (-40L);
                                            *(uint64_t *)var_84 = 4206083UL;
                                            indirect_placeholder_9(0UL, 4251624UL, 0UL, var_82, var_83, r9_1, r8_1);
                                            local_sp_0 = var_84;
                                        } else {
                                            *var_76 = 4205965UL;
                                            indirect_placeholder_6(rax_1);
                                            var_77 = local_sp_3 + (-32L);
                                            *(uint64_t *)var_77 = 4205973UL;
                                            var_78 = indirect_placeholder_1(rax_1);
                                            local_sp_0 = var_77;
                                            if ((uint64_t)((uint32_t)var_78 + 1U) == 0UL) {
                                                *(uint64_t *)(local_sp_3 + (-40L)) = 4205996UL;
                                                var_79 = indirect_placeholder_10(var_71, 0UL, 3UL);
                                                *(uint64_t *)(local_sp_3 + (-48L)) = 4206004UL;
                                                indirect_placeholder();
                                                var_80 = (uint64_t)*(uint32_t *)var_79;
                                                var_81 = local_sp_3 + (-56L);
                                                *(uint64_t *)var_81 = 4206029UL;
                                                indirect_placeholder_9(0UL, 4253525UL, 0UL, var_79, var_80, r9_1, r8_1);
                                                local_sp_0 = var_81;
                                            }
                                        }
                                    }
                                    var_85 = (uint64_t)*(uint32_t *)6362748UL;
                                    var_86 = var_85 + 1UL;
                                    var_87 = (uint32_t)var_86;
                                    *(uint32_t *)6362748UL = var_87;
                                    rax_1 = var_85;
                                    local_sp_3 = local_sp_0;
                                    if ((long)var_70 > (long)(var_86 << 32UL)) {
                                        break;
                                    }
                                    rdx_0 = (uint64_t)var_87;
                                    continue;
                                }
                        }
                    }
                    *(uint64_t *)(local_sp_2 + (-8L)) = 4205888UL;
                    indirect_placeholder_6(var_69);
                    return;
                }
                break;
            }
        }
        break;
    }
}

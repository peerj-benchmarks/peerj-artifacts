typedef unsigned char __u_char;
typedef unsigned short __u_short;
typedef unsigned int __u_int;
typedef unsigned long __u_long;
typedef signed char __int8_t;
typedef unsigned char __uint8_t;
typedef short __int16_t;
typedef unsigned short __uint16_t;
typedef int __int32_t;
typedef unsigned int __uint32_t;
typedef long __int64_t;
typedef unsigned long __uint64_t;
typedef __int8_t __int_least8_t;
typedef __uint8_t __uint_least8_t;
typedef __int16_t __int_least16_t;
typedef __uint16_t __uint_least16_t;
typedef __int32_t __int_least32_t;
typedef __uint32_t __uint_least32_t;
typedef __int64_t __int_least64_t;
typedef __uint64_t __uint_least64_t;
typedef long __quad_t;
typedef unsigned long __u_quad_t;
typedef long __intmax_t;
typedef unsigned long __uintmax_t;
typedef unsigned long __dev_t;
typedef unsigned int __uid_t;
typedef unsigned int __gid_t;
typedef unsigned long __ino_t;
typedef unsigned long __ino64_t;
typedef unsigned int __mode_t;
typedef unsigned long __nlink_t;
typedef long __off_t;
typedef long __off64_t;
typedef int __pid_t;
typedef struct {
    int __val[2];
} __fsid_t;
typedef long __clock_t;
typedef unsigned long __rlim_t;
typedef unsigned long __rlim64_t;
typedef unsigned int __id_t;
typedef long __time_t;
typedef unsigned int __useconds_t;
typedef long __suseconds_t;
typedef int __daddr_t;
typedef int __key_t;
typedef int __clockid_t;
typedef void *__timer_t;
typedef long __blksize_t;
typedef long __blkcnt_t;
typedef long __blkcnt64_t;
typedef unsigned long __fsblkcnt_t;
typedef unsigned long __fsblkcnt64_t;
typedef unsigned long __fsfilcnt_t;
typedef unsigned long __fsfilcnt64_t;
typedef long __fsword_t;
typedef long __ssize_t;
typedef long __syscall_slong_t;
typedef unsigned long __syscall_ulong_t;
typedef __off64_t __loff_t;
typedef char *__caddr_t;
typedef long __intptr_t;
typedef unsigned int __socklen_t;
typedef int __sig_atomic_t;
typedef __int8_t int8_t;
typedef __int16_t int16_t;
typedef __int32_t int32_t;
typedef __int64_t int64_t;
typedef __uint8_t uint8_t;
typedef __uint16_t uint16_t;
typedef __uint32_t uint32_t;
typedef __uint64_t uint64_t;
typedef __int_least8_t int_least8_t;
typedef __int_least16_t int_least16_t;
typedef __int_least32_t int_least32_t;
typedef __int_least64_t int_least64_t;
typedef __uint_least8_t uint_least8_t;
typedef __uint_least16_t uint_least16_t;
typedef __uint_least32_t uint_least32_t;
typedef __uint_least64_t uint_least64_t;
typedef signed char int_fast8_t;
typedef long int_fast16_t;
typedef long int_fast32_t;
typedef long int_fast64_t;
typedef unsigned char uint_fast8_t;
typedef unsigned long uint_fast16_t;
typedef unsigned long uint_fast32_t;
typedef unsigned long uint_fast64_t;
typedef long intptr_t;
typedef unsigned long uintptr_t;
typedef __intmax_t intmax_t;
typedef __uintmax_t uintmax_t;
typedef _Bool bool;
extern uint64_t revng_init_local_sp(uint64_t param_0);
extern uint64_t init_rbx(void);
extern void indirect_placeholder(void);
extern uint64_t init_rbp(void);
extern uint64_t init_r13(void);
extern uint64_t init_r12(void);
extern uint64_t indirect_placeholder_1(uint64_t param_0);
extern void indirect_placeholder_6(uint64_t param_0);
extern uint64_t indirect_placeholder_7(uint64_t param_0, uint64_t param_1);
extern void indirect_placeholder_8(uint64_t param_0, uint64_t param_1, uint64_t param_2);
void bb_fmt(uint64_t rdi) {
    uint64_t var_0;
    uint64_t var_1;
    uint64_t var_2;
    uint64_t var_3;
    uint64_t var_4;
    uint64_t var_5;
    uint64_t var_6;
    uint64_t local_sp_11;
    uint64_t local_sp_7_ph;
    uint64_t rbx_2;
    uint64_t var_33;
    uint64_t var_34;
    uint64_t local_sp_2;
    uint64_t local_sp_0;
    uint64_t var_31;
    uint64_t rbx_0;
    uint64_t var_27;
    uint64_t var_28;
    uint64_t var_29;
    uint64_t var_30;
    uint64_t r12_1;
    uint64_t local_sp_1;
    uint64_t rbx_1;
    uint64_t var_21;
    uint64_t var_22;
    uint64_t var_23;
    uint64_t var_24;
    uint64_t var_19;
    uint64_t var_20;
    uint64_t var_25;
    uint64_t var_26;
    uint64_t _pre_phi114;
    uint64_t var_32;
    uint64_t local_sp_3;
    uint64_t rbx_3;
    uint64_t var_35;
    uint64_t var_37;
    uint64_t var_36;
    uint64_t local_sp_4;
    unsigned char *var_38;
    uint64_t var_39;
    uint64_t var_40;
    uint64_t var_64;
    uint64_t var_65;
    uint64_t local_sp_5;
    uint64_t local_sp_10;
    uint64_t rax_0;
    uint64_t local_sp_6_ph;
    uint64_t rax_1_ph;
    uint32_t var_69;
    uint64_t var_70;
    bool or_cond;
    uint64_t local_sp_6;
    uint64_t var_71;
    uint64_t _pre113;
    uint64_t local_sp_7;
    uint64_t r13_0_ph;
    uint64_t r13_0;
    uint64_t var_55;
    uint64_t var_56;
    uint32_t var_57;
    uint64_t var_58;
    uint32_t var_59;
    uint64_t var_60;
    uint32_t var_61;
    uint64_t local_sp_8;
    uint64_t rax_2;
    uint64_t rcx_0;
    uint64_t local_sp_9;
    uint64_t rax_3;
    uint64_t rcx_1;
    uint64_t var_62;
    uint64_t var_63;
    uint64_t local_sp_14;
    uint64_t var_66;
    uint64_t var_67;
    uint64_t rax_4;
    uint64_t var_68;
    uint64_t r12_0;
    uint64_t local_sp_12;
    uint64_t storemerge;
    uint32_t var_7;
    uint64_t var_8;
    bool var_9;
    uint64_t var_10;
    uint32_t var_11;
    uint64_t var_12;
    uint32_t var_13;
    uint64_t var_14;
    uint64_t var_15;
    uint64_t var_16;
    uint64_t var_17;
    uint64_t var_18;
    uint32_t var_41;
    uint64_t var_42;
    uint64_t local_sp_13;
    uint64_t var_43;
    uint64_t var_44;
    uint32_t var_45;
    uint64_t var_46;
    uint32_t var_47;
    uint64_t var_48;
    uint64_t var_72;
    uint64_t var_73;
    uint64_t var_49;
    uint64_t var_50;
    uint32_t var_51;
    uint64_t var_52;
    uint32_t var_53;
    uint64_t var_54;
    unsigned int loop_state_var;
    bool switch_state_var;
    var_0 = revng_init_local_sp(0UL);
    var_1 = init_rbx();
    var_2 = init_rbp();
    var_3 = init_r13();
    var_4 = init_r12();
    *(uint64_t *)(var_0 + (-8L)) = var_3;
    *(uint64_t *)(var_0 + (-16L)) = var_4;
    *(uint64_t *)(var_0 + (-24L)) = var_2;
    *(uint64_t *)(var_0 + (-32L)) = var_1;
    *(uint64_t *)(var_0 + (-48L)) = 4203651UL;
    indirect_placeholder_8(var_1, rdi, 2UL);
    *(unsigned char *)6362968UL = (unsigned char)'\x00';
    *(uint32_t *)6362956UL = 0U;
    var_5 = var_0 + (-56L);
    *(uint64_t *)var_5 = 4203676UL;
    var_6 = indirect_placeholder_1(rdi);
    *(uint32_t *)6362952UL = (uint32_t)var_6;
    local_sp_11 = var_5;
    r12_0 = var_4;
    while (1U)
        {
            *(uint32_t *)6362944UL = 0U;
            local_sp_12 = local_sp_11;
            r12_1 = r12_0;
            storemerge = (uint64_t)*(uint32_t *)6362952UL;
            while (1U)
                {
                    var_7 = (uint32_t)storemerge;
                    var_8 = (uint64_t)(var_7 + 1U);
                    var_9 = (var_8 == 0UL);
                    var_10 = (r12_1 & (-256L)) | var_9;
                    r12_0 = var_10;
                    r12_1 = var_10;
                    _pre_phi114 = var_8;
                    local_sp_13 = local_sp_12;
                    if (!var_9) {
                        var_11 = *(uint32_t *)6362948UL;
                        var_12 = (uint64_t)var_11;
                        var_13 = *(uint32_t *)6408076UL;
                        if ((uint64_t)(var_7 + (-10)) != 0UL & (long)(var_12 << 32UL) >= (long)((uint64_t)*(uint32_t *)6408088UL << 32UL) & (long)((var_12 + (uint64_t)*(uint32_t *)6408092UL) << 32UL) <= (long)((uint64_t)var_13 << 32UL)) {
                            loop_state_var = 1U;
                            break;
                        }
                    }
                    *(uint32_t *)6408072UL = 0U;
                    var_41 = *(uint32_t *)6362948UL;
                    var_42 = (uint64_t)var_41;
                    if ((int)var_41 < (int)*(uint32_t *)6408076UL) {
                        var_49 = local_sp_12 + (-8L);
                        *(uint64_t *)var_49 = 4203755UL;
                        indirect_placeholder_6(var_42);
                        var_50 = *(uint64_t *)6408104UL;
                        var_51 = *(uint32_t *)6408072UL;
                        var_52 = (uint64_t)var_51;
                        var_53 = *(uint32_t *)6408076UL;
                        var_54 = (uint64_t)var_53;
                        local_sp_7_ph = var_49;
                        r13_0_ph = var_50;
                        var_61 = var_53;
                        local_sp_8 = var_49;
                        rax_2 = var_54;
                        rcx_0 = var_54;
                        rcx_0 = var_52;
                        if ((uint64_t)(var_51 - var_53) != 0UL & *(unsigned char *)var_50 != '\x00') {
                            local_sp_7 = local_sp_7_ph;
                            r13_0 = r13_0_ph;
                            var_55 = r13_0 + 1UL;
                            var_56 = local_sp_7 + (-8L);
                            *(uint64_t *)var_56 = 4203799UL;
                            indirect_placeholder();
                            var_57 = *(uint32_t *)6408072UL + 1U;
                            var_58 = (uint64_t)var_57;
                            *(uint32_t *)6408072UL = var_57;
                            var_59 = *(uint32_t *)6408076UL;
                            var_60 = (uint64_t)var_59;
                            local_sp_7 = var_56;
                            r13_0 = var_55;
                            var_61 = var_59;
                            local_sp_8 = var_56;
                            rax_2 = var_58;
                            rcx_0 = var_60;
                            while ((uint64_t)(var_57 - var_59) != 0UL)
                                {
                                    rax_2 = var_60;
                                    rcx_0 = var_58;
                                    if (*(unsigned char *)var_55 == '\x00') {
                                        break;
                                    }
                                    var_55 = r13_0 + 1UL;
                                    var_56 = local_sp_7 + (-8L);
                                    *(uint64_t *)var_56 = 4203799UL;
                                    indirect_placeholder();
                                    var_57 = *(uint32_t *)6408072UL + 1U;
                                    var_58 = (uint64_t)var_57;
                                    *(uint32_t *)6408072UL = var_57;
                                    var_59 = *(uint32_t *)6408076UL;
                                    var_60 = (uint64_t)var_59;
                                    local_sp_7 = var_56;
                                    r13_0 = var_55;
                                    var_61 = var_59;
                                    local_sp_8 = var_56;
                                    rax_2 = var_58;
                                    rcx_0 = var_60;
                                }
                        }
                    }
                    if (!var_9) {
                        local_sp_14 = local_sp_13;
                        if (_pre_phi114 != 0UL) {
                            loop_state_var = 0U;
                            break;
                        }
                        *(uint64_t *)(local_sp_14 + (-8L)) = 4203975UL;
                        indirect_placeholder();
                        var_72 = local_sp_14 + (-16L);
                        *(uint64_t *)var_72 = 4203983UL;
                        var_73 = indirect_placeholder_1(rdi);
                        local_sp_12 = var_72;
                        storemerge = (uint64_t)(uint32_t)var_73;
                        continue;
                    }
                    if ((uint64_t)(var_7 + (-10)) != 0UL) {
                        local_sp_14 = local_sp_13;
                        if (_pre_phi114 != 0UL) {
                            loop_state_var = 0U;
                            break;
                        }
                        *(uint64_t *)(local_sp_14 + (-8L)) = 4203975UL;
                        indirect_placeholder();
                        var_72 = local_sp_14 + (-16L);
                        *(uint64_t *)var_72 = 4203983UL;
                        var_73 = indirect_placeholder_1(rdi);
                        local_sp_12 = var_72;
                        storemerge = (uint64_t)(uint32_t)var_73;
                        continue;
                    }
                    var_43 = local_sp_12 + (-8L);
                    *(uint64_t *)var_43 = 4204376UL;
                    indirect_placeholder_6(var_42);
                    var_44 = *(uint64_t *)6408104UL;
                    var_45 = *(uint32_t *)6408072UL;
                    var_46 = (uint64_t)var_45;
                    var_47 = *(uint32_t *)6408076UL;
                    var_48 = (uint64_t)var_47;
                    local_sp_7_ph = var_43;
                    r13_0_ph = var_44;
                    local_sp_9 = var_43;
                    rax_3 = var_48;
                    rcx_1 = var_48;
                    if ((uint64_t)(var_45 - var_47) != 0UL) {
                        var_62 = (uint64_t)((uint32_t)rax_3 - (uint32_t)rcx_1);
                        var_63 = local_sp_9 + (-8L);
                        *(uint64_t *)var_63 = 4203862UL;
                        indirect_placeholder_6(var_62);
                        local_sp_5 = var_63;
                        local_sp_6_ph = var_63;
                        rax_1_ph = var_62;
                        local_sp_10 = var_63;
                        if (!var_9) {
                            var_64 = (uint64_t)*(uint32_t *)6408084UL + (uint64_t)*(uint32_t *)6362948UL;
                            var_65 = (uint64_t)(uint32_t)var_64;
                            rax_0 = var_65;
                            rax_4 = var_65;
                            if ((long)(var_64 << 32UL) > (long)((uint64_t)*(uint32_t *)6408076UL << 32UL)) {
                                var_68 = local_sp_10 + (-8L);
                                *(uint64_t *)var_68 = 4203913UL;
                                indirect_placeholder();
                                local_sp_5 = var_68;
                                rax_0 = rax_4;
                            }
                            local_sp_6_ph = local_sp_5;
                            rax_1_ph = rax_0;
                            if (!var_9) {
                                loop_state_var = 0U;
                                break;
                            }
                            if ((uint64_t)(var_7 + (-10)) != 0UL) {
                                loop_state_var = 0U;
                                break;
                            }
                        }
                        var_69 = (uint32_t)rax_1_ph;
                        var_70 = (uint64_t)var_69;
                        or_cond = (((uint64_t)(var_69 + 1U) == 0UL) || ((uint64_t)(var_69 + (-10)) == 0UL));
                        local_sp_6 = local_sp_6_ph;
                        *(uint64_t *)(local_sp_6 + (-8L)) = 4203925UL;
                        indirect_placeholder();
                        var_71 = local_sp_6 + (-16L);
                        *(uint64_t *)var_71 = 4203933UL;
                        indirect_placeholder();
                        local_sp_6 = var_71;
                        local_sp_13 = var_71;
                        do {
                            *(uint64_t *)(local_sp_6 + (-8L)) = 4203925UL;
                            indirect_placeholder();
                            var_71 = local_sp_6 + (-16L);
                            *(uint64_t *)var_71 = 4203933UL;
                            indirect_placeholder();
                            local_sp_6 = var_71;
                            local_sp_13 = var_71;
                        } while (!or_cond);
                        _pre113 = (uint64_t)((uint32_t)var_70 + 1U);
                        _pre_phi114 = _pre113;
                        local_sp_14 = local_sp_13;
                        if (_pre_phi114 != 0UL) {
                            loop_state_var = 0U;
                            break;
                        }
                        *(uint64_t *)(local_sp_14 + (-8L)) = 4203975UL;
                        indirect_placeholder();
                        var_72 = local_sp_14 + (-16L);
                        *(uint64_t *)var_72 = 4203983UL;
                        var_73 = indirect_placeholder_1(rdi);
                        local_sp_12 = var_72;
                        storemerge = (uint64_t)(uint32_t)var_73;
                        continue;
                    }
                    rcx_1 = var_46;
                    if (*(unsigned char *)var_44 == '\x00') {
                        var_62 = (uint64_t)((uint32_t)rax_3 - (uint32_t)rcx_1);
                        var_63 = local_sp_9 + (-8L);
                        *(uint64_t *)var_63 = 4203862UL;
                        indirect_placeholder_6(var_62);
                        local_sp_5 = var_63;
                        local_sp_6_ph = var_63;
                        rax_1_ph = var_62;
                        local_sp_10 = var_63;
                        if (!var_9) {
                            var_64 = (uint64_t)*(uint32_t *)6408084UL + (uint64_t)*(uint32_t *)6362948UL;
                            var_65 = (uint64_t)(uint32_t)var_64;
                            rax_0 = var_65;
                            rax_4 = var_65;
                            if ((long)(var_64 << 32UL) > (long)((uint64_t)*(uint32_t *)6408076UL << 32UL)) {
                                var_68 = local_sp_10 + (-8L);
                                *(uint64_t *)var_68 = 4203913UL;
                                indirect_placeholder();
                                local_sp_5 = var_68;
                                rax_0 = rax_4;
                            }
                            local_sp_6_ph = local_sp_5;
                            rax_1_ph = rax_0;
                            if (!var_9) {
                                loop_state_var = 0U;
                                break;
                            }
                            if ((uint64_t)(var_7 + (-10)) != 0UL) {
                                loop_state_var = 0U;
                                break;
                            }
                        }
                        var_69 = (uint32_t)rax_1_ph;
                        var_70 = (uint64_t)var_69;
                        or_cond = (((uint64_t)(var_69 + 1U) == 0UL) || ((uint64_t)(var_69 + (-10)) == 0UL));
                        local_sp_6 = local_sp_6_ph;
                        *(uint64_t *)(local_sp_6 + (-8L)) = 4203925UL;
                        indirect_placeholder();
                        var_71 = local_sp_6 + (-16L);
                        *(uint64_t *)var_71 = 4203933UL;
                        indirect_placeholder();
                        local_sp_6 = var_71;
                        local_sp_13 = var_71;
                        do {
                            *(uint64_t *)(local_sp_6 + (-8L)) = 4203925UL;
                            indirect_placeholder();
                            var_71 = local_sp_6 + (-16L);
                            *(uint64_t *)var_71 = 4203933UL;
                            indirect_placeholder();
                            local_sp_6 = var_71;
                            local_sp_13 = var_71;
                        } while (!or_cond);
                        _pre113 = (uint64_t)((uint32_t)var_70 + 1U);
                        _pre_phi114 = _pre113;
                        local_sp_14 = local_sp_13;
                        if (_pre_phi114 != 0UL) {
                            loop_state_var = 0U;
                            break;
                        }
                        *(uint64_t *)(local_sp_14 + (-8L)) = 4203975UL;
                        indirect_placeholder();
                        var_72 = local_sp_14 + (-16L);
                        *(uint64_t *)var_72 = 4203983UL;
                        var_73 = indirect_placeholder_1(rdi);
                        local_sp_12 = var_72;
                        storemerge = (uint64_t)(uint32_t)var_73;
                        continue;
                    }
                    local_sp_7 = local_sp_7_ph;
                    r13_0 = r13_0_ph;
                    var_55 = r13_0 + 1UL;
                    var_56 = local_sp_7 + (-8L);
                    *(uint64_t *)var_56 = 4203799UL;
                    indirect_placeholder();
                    var_57 = *(uint32_t *)6408072UL + 1U;
                    var_58 = (uint64_t)var_57;
                    *(uint32_t *)6408072UL = var_57;
                    var_59 = *(uint32_t *)6408076UL;
                    var_60 = (uint64_t)var_59;
                    local_sp_7 = var_56;
                    r13_0 = var_55;
                    var_61 = var_59;
                    local_sp_8 = var_56;
                    rax_2 = var_58;
                    rcx_0 = var_60;
                    while ((uint64_t)(var_57 - var_59) != 0UL)
                        {
                            rax_2 = var_60;
                            rcx_0 = var_58;
                            if (*(unsigned char *)var_55 == '\x00') {
                                break;
                            }
                            var_55 = r13_0 + 1UL;
                            var_56 = local_sp_7 + (-8L);
                            *(uint64_t *)var_56 = 4203799UL;
                            indirect_placeholder();
                            var_57 = *(uint32_t *)6408072UL + 1U;
                            var_58 = (uint64_t)var_57;
                            *(uint32_t *)6408072UL = var_57;
                            var_59 = *(uint32_t *)6408076UL;
                            var_60 = (uint64_t)var_59;
                            local_sp_7 = var_56;
                            r13_0 = var_55;
                            var_61 = var_59;
                            local_sp_8 = var_56;
                            rax_2 = var_58;
                            rcx_0 = var_60;
                        }
                    local_sp_10 = local_sp_8;
                    local_sp_9 = local_sp_8;
                    rax_3 = rax_2;
                    rcx_1 = rcx_0;
                    local_sp_14 = local_sp_8;
                    if (((uint64_t)(var_7 + (-10)) == 0UL) || var_9) {
                        if (!var_9) {
                            *(uint64_t *)(local_sp_14 + (-8L)) = 4203975UL;
                            indirect_placeholder();
                            var_72 = local_sp_14 + (-16L);
                            *(uint64_t *)var_72 = 4203983UL;
                            var_73 = indirect_placeholder_1(rdi);
                            local_sp_12 = var_72;
                            storemerge = (uint64_t)(uint32_t)var_73;
                            continue;
                        }
                        var_66 = (uint64_t)*(uint32_t *)6408084UL + (uint64_t)*(uint32_t *)6362948UL;
                        var_67 = (uint64_t)(uint32_t)var_66;
                        rax_4 = var_67;
                        if ((long)(var_66 << 32UL) > (long)((uint64_t)var_61 << 32UL)) {
                            loop_state_var = 0U;
                            break;
                        }
                        var_68 = local_sp_10 + (-8L);
                        *(uint64_t *)var_68 = 4203913UL;
                        indirect_placeholder();
                        local_sp_5 = var_68;
                        rax_0 = rax_4;
                        local_sp_6_ph = local_sp_5;
                        rax_1_ph = rax_0;
                        if (var_9) {
                            loop_state_var = 0U;
                            break;
                        }
                        if ((uint64_t)(var_7 + (-10)) != 0UL) {
                            loop_state_var = 0U;
                            break;
                        }
                    }
                    var_62 = (uint64_t)((uint32_t)rax_3 - (uint32_t)rcx_1);
                    var_63 = local_sp_9 + (-8L);
                    *(uint64_t *)var_63 = 4203862UL;
                    indirect_placeholder_6(var_62);
                    local_sp_5 = var_63;
                    local_sp_6_ph = var_63;
                    rax_1_ph = var_62;
                    local_sp_10 = var_63;
                    if (!var_9) {
                        var_64 = (uint64_t)*(uint32_t *)6408084UL + (uint64_t)*(uint32_t *)6362948UL;
                        var_65 = (uint64_t)(uint32_t)var_64;
                        rax_0 = var_65;
                        rax_4 = var_65;
                        if ((long)(var_64 << 32UL) > (long)((uint64_t)*(uint32_t *)6408076UL << 32UL)) {
                            var_68 = local_sp_10 + (-8L);
                            *(uint64_t *)var_68 = 4203913UL;
                            indirect_placeholder();
                            local_sp_5 = var_68;
                            rax_0 = rax_4;
                        }
                        local_sp_6_ph = local_sp_5;
                        rax_1_ph = rax_0;
                        if (!var_9) {
                            loop_state_var = 0U;
                            break;
                        }
                        if ((uint64_t)(var_7 + (-10)) == 0UL) {
                            loop_state_var = 0U;
                            break;
                        }
                    }
                    var_69 = (uint32_t)rax_1_ph;
                    var_70 = (uint64_t)var_69;
                    or_cond = (((uint64_t)(var_69 + 1U) == 0UL) || ((uint64_t)(var_69 + (-10)) == 0UL));
                    local_sp_6 = local_sp_6_ph;
                    *(uint64_t *)(local_sp_6 + (-8L)) = 4203925UL;
                    indirect_placeholder();
                    var_71 = local_sp_6 + (-16L);
                    *(uint64_t *)var_71 = 4203933UL;
                    indirect_placeholder();
                    local_sp_6 = var_71;
                    local_sp_13 = var_71;
                    do {
                        *(uint64_t *)(local_sp_6 + (-8L)) = 4203925UL;
                        indirect_placeholder();
                        var_71 = local_sp_6 + (-16L);
                        *(uint64_t *)var_71 = 4203933UL;
                        indirect_placeholder();
                        local_sp_6 = var_71;
                        local_sp_13 = var_71;
                    } while (!or_cond);
                    _pre113 = (uint64_t)((uint32_t)var_70 + 1U);
                    _pre_phi114 = _pre113;
                    local_sp_14 = local_sp_13;
                    if (_pre_phi114 != 0UL) {
                        loop_state_var = 0U;
                        break;
                    }
                    *(uint64_t *)(local_sp_14 + (-8L)) = 4203975UL;
                    indirect_placeholder();
                    var_72 = local_sp_14 + (-16L);
                    *(uint64_t *)var_72 = 4203983UL;
                    var_73 = indirect_placeholder_1(rdi);
                    local_sp_12 = var_72;
                    storemerge = (uint64_t)(uint32_t)var_73;
                    continue;
                }
            switch_state_var = 0;
            switch (loop_state_var) {
              case 0U:
                {
                    switch_state_var = 1;
                    break;
                }
                break;
              case 1U:
                {
                    *(uint32_t *)6362964UL = var_11;
                    *(uint32_t *)6362960UL = var_13;
                    *(uint64_t *)6403008UL = 6403072UL;
                    *(uint64_t *)6362976UL = 6363008UL;
                    *(uint64_t *)(local_sp_12 + (-8L)) = 4204091UL;
                    var_14 = indirect_placeholder_7(rdi, storemerge);
                    var_15 = (uint64_t)(uint32_t)var_14;
                    *(uint64_t *)(local_sp_12 + (-16L)) = 4204100UL;
                    var_16 = indirect_placeholder_1(var_15);
                    var_17 = (uint64_t)(unsigned char)var_16;
                    var_18 = local_sp_12 + (-24L);
                    *(uint64_t *)var_18 = 4204108UL;
                    indirect_placeholder_6(var_17);
                    rbx_2 = var_15;
                    local_sp_2 = var_18;
                    rbx_0 = var_15;
                    rbx_1 = var_15;
                    local_sp_3 = var_18;
                    rbx_3 = var_15;
                    if (*(unsigned char *)6408113UL != '\x00') {
                        if (*(unsigned char *)6408115UL != '\x00') {
                            var_19 = local_sp_12 + (-32L);
                            *(uint64_t *)var_19 = 4204137UL;
                            var_20 = indirect_placeholder_1(var_15);
                            local_sp_1 = var_19;
                            local_sp_3 = var_19;
                            if ((uint64_t)(unsigned char)var_20 == 0UL) {
                                *(uint64_t *)(local_sp_1 + (-8L)) = 4204155UL;
                                var_21 = indirect_placeholder_7(rdi, rbx_1);
                                var_22 = (uint64_t)(uint32_t)var_21;
                                var_23 = local_sp_1 + (-16L);
                                *(uint64_t *)var_23 = 4204164UL;
                                var_24 = indirect_placeholder_1(var_22);
                                local_sp_1 = var_23;
                                rbx_1 = var_22;
                                local_sp_3 = var_23;
                                rbx_3 = var_22;
                                while ((uint64_t)(unsigned char)var_24 != 0UL)
                                    {
                                        if ((uint64_t)(*(uint32_t *)6408076UL - *(uint32_t *)6362956UL) == 0UL) {
                                            break;
                                        }
                                        *(uint64_t *)(local_sp_1 + (-8L)) = 4204155UL;
                                        var_21 = indirect_placeholder_7(rdi, rbx_1);
                                        var_22 = (uint64_t)(uint32_t)var_21;
                                        var_23 = local_sp_1 + (-16L);
                                        *(uint64_t *)var_23 = 4204164UL;
                                        var_24 = indirect_placeholder_1(var_22);
                                        local_sp_1 = var_23;
                                        rbx_1 = var_22;
                                        local_sp_3 = var_23;
                                        rbx_3 = var_22;
                                    }
                            }
                        }
                        if (*(unsigned char *)6408114UL != '\x00') {
                            var_25 = local_sp_12 + (-32L);
                            *(uint64_t *)var_25 = 4204200UL;
                            var_26 = indirect_placeholder_1(var_15);
                            local_sp_0 = var_25;
                            local_sp_3 = var_25;
                            if ((uint64_t)(unsigned char)var_26 != 0UL & (uint64_t)(*(uint32_t *)6408076UL - *(uint32_t *)6362960UL) == 0UL) {
                                *(uint64_t *)(local_sp_0 + (-8L)) = 4204228UL;
                                var_27 = indirect_placeholder_7(rdi, rbx_0);
                                var_28 = (uint64_t)(uint32_t)var_27;
                                var_29 = local_sp_0 + (-16L);
                                *(uint64_t *)var_29 = 4204237UL;
                                var_30 = indirect_placeholder_1(var_28);
                                local_sp_0 = var_29;
                                rbx_0 = var_28;
                                local_sp_3 = var_29;
                                rbx_3 = var_28;
                                while ((uint64_t)(unsigned char)var_30 != 0UL)
                                    {
                                        if ((uint64_t)(*(uint32_t *)6408076UL - *(uint32_t *)6362956UL) == 0UL) {
                                            break;
                                        }
                                        *(uint64_t *)(local_sp_0 + (-8L)) = 4204228UL;
                                        var_27 = indirect_placeholder_7(rdi, rbx_0);
                                        var_28 = (uint64_t)(uint32_t)var_27;
                                        var_29 = local_sp_0 + (-16L);
                                        *(uint64_t *)var_29 = 4204237UL;
                                        var_30 = indirect_placeholder_1(var_28);
                                        local_sp_0 = var_29;
                                        rbx_0 = var_28;
                                        local_sp_3 = var_29;
                                        rbx_3 = var_28;
                                    }
                            }
                        }
                        var_31 = local_sp_2 + (-8L);
                        *(uint64_t *)var_31 = 4204276UL;
                        var_32 = indirect_placeholder_1(rbx_2);
                        local_sp_3 = var_31;
                        rbx_3 = rbx_2;
                        while ((uint64_t)(unsigned char)var_32 != 0UL)
                            {
                                if ((uint64_t)(*(uint32_t *)6408076UL - *(uint32_t *)6362956UL) == 0UL) {
                                    break;
                                }
                                var_33 = local_sp_2 + (-16L);
                                *(uint64_t *)var_33 = 4204267UL;
                                var_34 = indirect_placeholder_7(rdi, rbx_2);
                                local_sp_2 = var_33;
                                rbx_2 = (uint64_t)(uint32_t)var_34;
                                var_31 = local_sp_2 + (-8L);
                                *(uint64_t *)var_31 = 4204276UL;
                                var_32 = indirect_placeholder_1(rbx_2);
                                local_sp_3 = var_31;
                                rbx_3 = rbx_2;
                            }
                    }
                    var_35 = *(uint64_t *)6362976UL;
                    var_37 = var_35;
                    local_sp_4 = local_sp_3;
                    if (var_35 > 6363008UL) {
                        var_36 = local_sp_3 + (-8L);
                        *(uint64_t *)var_36 = 4204332UL;
                        indirect_placeholder();
                        var_37 = *(uint64_t *)6362976UL;
                        local_sp_4 = var_36;
                    }
                    var_38 = (unsigned char *)(var_37 + (-24L));
                    *var_38 = (*var_38 | '\n');
                    *(uint32_t *)6362952UL = (uint32_t)rbx_3;
                    *(uint64_t *)(local_sp_4 + (-8L)) = 4204354UL;
                    indirect_placeholder();
                    var_39 = *(uint64_t *)6362976UL;
                    var_40 = local_sp_4 + (-16L);
                    *(uint64_t *)var_40 = 4204366UL;
                    indirect_placeholder_6(var_39);
                    local_sp_11 = var_40;
                    continue;
                }
                break;
            }
            if (switch_state_var)
                break;
        }
    *(uint32_t *)6362952UL = 4294967295U;
    return;
}

typedef unsigned char __u_char;
typedef unsigned short __u_short;
typedef unsigned int __u_int;
typedef unsigned long __u_long;
typedef signed char __int8_t;
typedef unsigned char __uint8_t;
typedef short __int16_t;
typedef unsigned short __uint16_t;
typedef int __int32_t;
typedef unsigned int __uint32_t;
typedef long __int64_t;
typedef unsigned long __uint64_t;
typedef __int8_t __int_least8_t;
typedef __uint8_t __uint_least8_t;
typedef __int16_t __int_least16_t;
typedef __uint16_t __uint_least16_t;
typedef __int32_t __int_least32_t;
typedef __uint32_t __uint_least32_t;
typedef __int64_t __int_least64_t;
typedef __uint64_t __uint_least64_t;
typedef long __quad_t;
typedef unsigned long __u_quad_t;
typedef long __intmax_t;
typedef unsigned long __uintmax_t;
typedef unsigned long __dev_t;
typedef unsigned int __uid_t;
typedef unsigned int __gid_t;
typedef unsigned long __ino_t;
typedef unsigned long __ino64_t;
typedef unsigned int __mode_t;
typedef unsigned long __nlink_t;
typedef long __off_t;
typedef long __off64_t;
typedef int __pid_t;
typedef struct {
    int __val[2];
} __fsid_t;
typedef long __clock_t;
typedef unsigned long __rlim_t;
typedef unsigned long __rlim64_t;
typedef unsigned int __id_t;
typedef long __time_t;
typedef unsigned int __useconds_t;
typedef long __suseconds_t;
typedef int __daddr_t;
typedef int __key_t;
typedef int __clockid_t;
typedef void *__timer_t;
typedef long __blksize_t;
typedef long __blkcnt_t;
typedef long __blkcnt64_t;
typedef unsigned long __fsblkcnt_t;
typedef unsigned long __fsblkcnt64_t;
typedef unsigned long __fsfilcnt_t;
typedef unsigned long __fsfilcnt64_t;
typedef long __fsword_t;
typedef long __ssize_t;
typedef long __syscall_slong_t;
typedef unsigned long __syscall_ulong_t;
typedef __off64_t __loff_t;
typedef char *__caddr_t;
typedef long __intptr_t;
typedef unsigned int __socklen_t;
typedef int __sig_atomic_t;
typedef __int8_t int8_t;
typedef __int16_t int16_t;
typedef __int32_t int32_t;
typedef __int64_t int64_t;
typedef __uint8_t uint8_t;
typedef __uint16_t uint16_t;
typedef __uint32_t uint32_t;
typedef __uint64_t uint64_t;
typedef __int_least8_t int_least8_t;
typedef __int_least16_t int_least16_t;
typedef __int_least32_t int_least32_t;
typedef __int_least64_t int_least64_t;
typedef __uint_least8_t uint_least8_t;
typedef __uint_least16_t uint_least16_t;
typedef __uint_least32_t uint_least32_t;
typedef __uint_least64_t uint_least64_t;
typedef signed char int_fast8_t;
typedef long int_fast16_t;
typedef long int_fast32_t;
typedef long int_fast64_t;
typedef unsigned char uint_fast8_t;
typedef unsigned long uint_fast16_t;
typedef unsigned long uint_fast32_t;
typedef unsigned long uint_fast64_t;
typedef long intptr_t;
typedef unsigned long uintptr_t;
typedef __intmax_t intmax_t;
typedef __uintmax_t uintmax_t;
struct indirect_placeholder_8_ret_type;
struct indirect_placeholder_9_ret_type;
struct indirect_placeholder_10_ret_type;
struct indirect_placeholder_11_ret_type;
struct indirect_placeholder_12_ret_type;
struct indirect_placeholder_13_ret_type;
struct indirect_placeholder_14_ret_type;
struct indirect_placeholder_8_ret_type {
    uint64_t field_0;
    uint64_t field_1;
    uint64_t field_2;
    uint64_t field_3;
};
struct indirect_placeholder_9_ret_type {
    uint64_t field_0;
    uint64_t field_1;
    uint64_t field_2;
    uint64_t field_3;
    uint64_t field_4;
};
struct indirect_placeholder_10_ret_type {
    uint64_t field_0;
    uint64_t field_1;
    uint64_t field_2;
    uint64_t field_3;
};
struct indirect_placeholder_11_ret_type {
    uint64_t field_0;
    uint64_t field_1;
    uint64_t field_2;
    uint64_t field_3;
};
struct indirect_placeholder_12_ret_type {
    uint64_t field_0;
    uint64_t field_1;
    uint64_t field_2;
    uint64_t field_3;
    uint64_t field_4;
};
struct indirect_placeholder_13_ret_type {
    uint64_t field_0;
    uint64_t field_1;
    uint64_t field_2;
    uint64_t field_3;
};
struct indirect_placeholder_14_ret_type {
    uint64_t field_0;
    uint64_t field_1;
    uint64_t field_2;
    uint64_t field_3;
};
extern void abort(void);
extern uint64_t revng_init_local_sp(uint64_t param_0);
extern uint64_t init_rbx(void);
extern void indirect_placeholder(void);
extern uint64_t init_rbp(void);
extern uint64_t init_cc_src2(void);
extern uint64_t helper_cc_compute_c_wrapper(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint32_t param_3);
extern uint64_t init_r14(void);
extern uint64_t init_r13(void);
extern uint64_t init_r12(void);
extern uint64_t indirect_placeholder_1(uint64_t param_0);
extern void indirect_placeholder_6(uint64_t param_0);
extern void indirect_placeholder_7(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint64_t param_3, uint64_t param_4, uint64_t param_5, uint64_t param_6);
extern void indirect_placeholder_3(uint64_t param_0, uint64_t param_1, uint64_t param_2);
extern struct indirect_placeholder_8_ret_type indirect_placeholder_8(uint64_t param_0, uint64_t param_1);
extern struct indirect_placeholder_9_ret_type indirect_placeholder_9(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint64_t param_3, uint64_t param_4, uint64_t param_5, uint64_t param_6);
extern struct indirect_placeholder_10_ret_type indirect_placeholder_10(uint64_t param_0, uint64_t param_1);
extern struct indirect_placeholder_11_ret_type indirect_placeholder_11(uint64_t param_0, uint64_t param_1);
extern struct indirect_placeholder_12_ret_type indirect_placeholder_12(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint64_t param_3, uint64_t param_4, uint64_t param_5, uint64_t param_6);
extern struct indirect_placeholder_13_ret_type indirect_placeholder_13(uint64_t param_0, uint64_t param_1);
extern struct indirect_placeholder_14_ret_type indirect_placeholder_14(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint64_t param_3, uint64_t param_4);
void bb_main(uint64_t rdi, uint64_t rsi) {
    struct indirect_placeholder_14_ret_type var_12;
    uint64_t rax_0;
    uint64_t var_0;
    uint64_t var_1;
    uint64_t var_2;
    uint64_t var_3;
    uint64_t var_4;
    uint64_t var_5;
    uint64_t var_6;
    uint32_t var_7;
    uint64_t var_8;
    uint64_t var_9;
    uint64_t var_10;
    uint64_t local_sp_7;
    uint64_t var_67;
    struct indirect_placeholder_8_ret_type var_68;
    uint64_t var_69;
    uint64_t var_70;
    uint64_t var_71;
    uint64_t var_72;
    uint64_t var_73;
    uint64_t rdx_0;
    uint64_t var_60;
    uint64_t var_61;
    uint64_t r13_2;
    uint64_t r12_2;
    uint64_t rcx_4;
    uint64_t local_sp_0;
    struct indirect_placeholder_9_ret_type var_62;
    uint64_t var_63;
    uint64_t var_64;
    uint64_t var_65;
    uint64_t var_66;
    uint64_t local_sp_5;
    uint64_t var_50;
    uint64_t rax_1;
    uint64_t local_sp_1;
    uint64_t var_51;
    uint64_t local_sp_2;
    struct indirect_placeholder_10_ret_type var_52;
    uint64_t var_53;
    uint64_t var_54;
    uint64_t var_55;
    uint64_t var_56;
    uint64_t var_57;
    uint64_t rcx_0;
    uint64_t var_58;
    uint64_t var_59;
    uint64_t rbp_0;
    uint64_t r13_0;
    uint64_t r12_0;
    uint64_t rcx_1;
    uint64_t rcx_5;
    uint64_t rbp_1;
    uint64_t rax_2;
    uint64_t rcx_2;
    uint64_t var_49;
    uint64_t var_40;
    struct indirect_placeholder_11_ret_type var_41;
    uint64_t var_42;
    uint64_t var_43;
    uint64_t var_44;
    uint64_t var_45;
    uint64_t var_46;
    uint64_t var_33;
    uint64_t var_34;
    struct indirect_placeholder_12_ret_type var_35;
    uint64_t var_36;
    uint64_t var_37;
    uint64_t var_38;
    uint64_t var_39;
    uint64_t r13_1;
    uint64_t var_47;
    uint64_t var_48;
    uint64_t r12_1;
    uint64_t rcx_3;
    uint64_t local_sp_3;
    uint32_t var_74;
    uint32_t var_20;
    uint64_t var_17;
    uint64_t var_18;
    uint32_t var_19;
    uint64_t var_75;
    uint64_t var_76;
    uint64_t rbp_2;
    uint64_t local_sp_4;
    uint64_t var_21;
    uint64_t var_22;
    uint64_t var_23;
    struct indirect_placeholder_13_ret_type var_24;
    uint64_t var_25;
    uint64_t var_26;
    uint64_t var_27;
    uint64_t var_28;
    uint64_t var_29;
    uint64_t var_30;
    uint64_t var_31;
    uint64_t var_32;
    uint64_t local_sp_6;
    uint64_t var_14;
    uint64_t var_15;
    uint64_t var_16;
    uint64_t var_11;
    uint32_t var_13;
    unsigned int loop_state_var;
    var_0 = revng_init_local_sp(0UL);
    var_1 = init_rbx();
    var_2 = init_r14();
    var_3 = init_rbp();
    var_4 = init_r13();
    var_5 = init_r12();
    var_6 = init_cc_src2();
    *(uint64_t *)(var_0 + (-8L)) = var_2;
    *(uint64_t *)(var_0 + (-16L)) = var_4;
    *(uint64_t *)(var_0 + (-24L)) = var_5;
    *(uint64_t *)(var_0 + (-32L)) = var_3;
    *(uint64_t *)(var_0 + (-40L)) = var_1;
    var_7 = (uint32_t)rdi;
    var_8 = (uint64_t)var_7;
    var_9 = *(uint64_t *)rsi;
    *(uint64_t *)(var_0 + (-48L)) = 4202293UL;
    indirect_placeholder_6(var_9);
    *(uint64_t *)(var_0 + (-56L)) = 4202308UL;
    indirect_placeholder();
    var_10 = var_0 + (-64L);
    *(uint64_t *)var_10 = 4202318UL;
    indirect_placeholder();
    *(unsigned char *)6358290UL = (unsigned char)'\x00';
    local_sp_7 = var_10;
    rdx_0 = 1UL;
    r13_2 = 1UL;
    r12_2 = var_8;
    rbp_2 = rsi;
    while (1U)
        {
            var_11 = local_sp_7 + (-8L);
            *(uint64_t *)var_11 = 4202500UL;
            var_12 = indirect_placeholder_14(4248638UL, var_8, 4248256UL, rsi, 0UL);
            var_13 = (uint32_t)var_12.field_0;
            local_sp_4 = var_11;
            local_sp_6 = var_11;
            local_sp_7 = var_11;
            if ((uint64_t)(var_13 + 1U) == 0UL) {
                if ((uint64_t)(var_13 + (-112)) == 0UL) {
                    *(unsigned char *)6358290UL = (unsigned char)'\x01';
                    continue;
                }
                if ((int)var_13 > (int)112U) {
                    if ((uint64_t)(var_13 + (-118)) == 0UL) {
                        *(unsigned char *)6358288UL = (unsigned char)'\x01';
                        continue;
                    }
                    if ((uint64_t)(var_13 + (-128)) == 0UL) {
                        break;
                    }
                    *(unsigned char *)6358289UL = (unsigned char)'\x01';
                    continue;
                }
                if ((uint64_t)(var_13 + 131U) != 0UL) {
                    if ((uint64_t)(var_13 + 130U) != 0UL) {
                        break;
                    }
                    *(uint64_t *)(local_sp_7 + (-16L)) = 4202407UL;
                    indirect_placeholder_3(var_1, rsi, 0UL);
                    abort();
                }
                var_14 = *(uint64_t *)6357984UL;
                var_15 = *(uint64_t *)6358144UL;
                *(uint64_t *)(local_sp_7 + (-16L)) = 4202453UL;
                indirect_placeholder_7(0UL, 4248581UL, var_15, var_14, 4248480UL, 0UL, 4248622UL);
                var_16 = local_sp_7 + (-24L);
                *(uint64_t *)var_16 = 4202463UL;
                indirect_placeholder();
                local_sp_6 = var_16;
                break;
            }
            var_17 = var_12.field_1;
            var_18 = *(uint64_t *)6358144UL;
            var_19 = *(uint32_t *)6358108UL;
            var_20 = var_19;
            rcx_4 = var_17;
            if ((uint64_t)(var_19 - var_7) == 0UL) {
                var_75 = var_12.field_3;
                var_76 = var_12.field_2;
                *(uint64_t *)(local_sp_7 + (-16L)) = 4202560UL;
                indirect_placeholder_7(0UL, 4248641UL, 0UL, var_17, 0UL, var_76, var_75);
                *(uint64_t *)(local_sp_7 + (-24L)) = 4202570UL;
                indirect_placeholder_3(var_1, rsi, 1UL);
                abort();
            }
            var_21 = r12_2 << 32UL;
            var_22 = (uint64_t)var_20 << 32UL;
            local_sp_5 = local_sp_4;
            rbp_0 = rbp_2;
            r13_0 = r13_2;
            r12_0 = r12_2;
            rcx_5 = rcx_4;
            rbp_1 = rbp_2;
            r13_1 = r13_2;
            r12_1 = r12_2;
            while ((long)var_21 <= (long)var_22)
                {
                    var_23 = *(uint64_t *)((uint64_t)((long)var_22 >> (long)29UL) + rbp_2);
                    if (*(unsigned char *)6358288UL == '\x00') {
                        *(uint64_t *)(local_sp_4 + (-8L)) = 4202599UL;
                        var_24 = indirect_placeholder_13(4UL, var_23);
                        var_25 = var_24.field_0;
                        var_26 = var_24.field_1;
                        var_27 = var_24.field_2;
                        var_28 = var_24.field_3;
                        var_29 = *(uint64_t *)6358144UL;
                        var_30 = local_sp_4 + (-16L);
                        *(uint64_t *)var_30 = 4202624UL;
                        indirect_placeholder_7(0UL, var_25, var_29, var_26, 4248657UL, var_27, var_28);
                        rcx_5 = var_26;
                        local_sp_5 = var_30;
                    }
                    var_31 = local_sp_5 + (-8L);
                    *(uint64_t *)var_31 = 4202632UL;
                    var_32 = indirect_placeholder_1(var_23);
                    rcx_2 = rcx_5;
                    rcx_3 = rcx_5;
                    local_sp_3 = var_31;
                    if ((uint64_t)(uint32_t)var_32 != 0UL) {
                        *(uint64_t *)(local_sp_5 + (-16L)) = 4202641UL;
                        indirect_placeholder();
                        var_33 = (uint64_t)*(uint32_t *)var_32;
                        var_34 = local_sp_5 + (-24L);
                        *(uint64_t *)var_34 = 4202651UL;
                        var_35 = indirect_placeholder_12(var_23, rbp_2, var_33, r13_2, r12_2, rcx_5, var_23);
                        var_36 = var_35.field_0;
                        var_37 = var_35.field_2;
                        var_38 = var_35.field_3;
                        var_39 = var_35.field_4;
                        rbp_1 = var_37;
                        r13_1 = var_38;
                        r12_1 = var_39;
                        local_sp_3 = var_34;
                        if ((uint64_t)(unsigned char)var_36 == 0UL) {
                            var_40 = var_35.field_1;
                            *(uint64_t *)(local_sp_5 + (-32L)) = 4202672UL;
                            var_41 = indirect_placeholder_11(4UL, var_40);
                            var_42 = var_41.field_0;
                            var_43 = var_41.field_2;
                            var_44 = var_41.field_3;
                            *(uint64_t *)(local_sp_5 + (-40L)) = 4202680UL;
                            indirect_placeholder();
                            var_45 = (uint64_t)*(uint32_t *)var_42;
                            var_46 = local_sp_5 + (-48L);
                            *(uint64_t *)var_46 = 4202705UL;
                            indirect_placeholder_7(0UL, 4248680UL, 0UL, var_42, var_45, var_43, var_44);
                            r13_1 = 0UL;
                            rcx_3 = var_42;
                            local_sp_3 = var_46;
                        }
                        var_74 = *(uint32_t *)6358108UL + 1U;
                        *(uint32_t *)6358108UL = var_74;
                        r13_2 = r13_1;
                        r12_2 = r12_1;
                        rcx_4 = rcx_3;
                        var_20 = var_74;
                        rbp_2 = rbp_1;
                        local_sp_4 = local_sp_3;
                        var_21 = r12_2 << 32UL;
                        var_22 = (uint64_t)var_20 << 32UL;
                        local_sp_5 = local_sp_4;
                        rbp_0 = rbp_2;
                        r13_0 = r13_2;
                        r12_0 = r12_2;
                        rcx_5 = rcx_4;
                        rbp_1 = rbp_2;
                        r13_1 = r13_2;
                        r12_1 = r12_2;
                        continue;
                    }
                    if (*(unsigned char *)6358290UL != '\x00') {
                        var_47 = local_sp_5 + (-16L);
                        *(uint64_t *)var_47 = 4202737UL;
                        var_48 = indirect_placeholder_1(var_23);
                        rax_2 = var_48;
                        local_sp_2 = var_47;
                        while (1U)
                            {
                                var_49 = local_sp_2 + (-8L);
                                *(uint64_t *)var_49 = 4202750UL;
                                indirect_placeholder();
                                rax_0 = rax_2;
                                local_sp_0 = var_49;
                                rax_1 = rax_2;
                                local_sp_1 = var_49;
                                rcx_0 = rcx_2;
                                rcx_1 = rcx_2;
                                if (rax_2 != 0UL) {
                                    loop_state_var = 0U;
                                    break;
                                }
                                var_50 = helper_cc_compute_c_wrapper(var_23 - rax_2, rax_2, var_6, 17U);
                                if (var_50 != 0UL & *(unsigned char *)rax_2 == '/') {
                                    var_51 = rax_0 + (-1L);
                                    rax_0 = var_51;
                                    rax_1 = var_23;
                                    while (var_51 != var_23)
                                        {
                                            rax_1 = var_51;
                                            if (*(unsigned char *)var_51 == '/') {
                                                break;
                                            }
                                            var_51 = rax_0 + (-1L);
                                            rax_0 = var_51;
                                            rax_1 = var_23;
                                        }
                                }
                                *(unsigned char *)(rax_1 + 1UL) = (unsigned char)'\x00';
                                if (*(unsigned char *)6358288UL == '\x00') {
                                    *(uint64_t *)(local_sp_2 + (-16L)) = 4202814UL;
                                    var_52 = indirect_placeholder_10(4UL, var_23);
                                    var_53 = var_52.field_0;
                                    var_54 = var_52.field_1;
                                    var_55 = var_52.field_2;
                                    var_56 = var_52.field_3;
                                    var_57 = local_sp_2 + (-24L);
                                    *(uint64_t *)var_57 = 4202835UL;
                                    indirect_placeholder_7(0UL, var_53, var_18, var_54, 4248657UL, var_55, var_56);
                                    rcx_0 = var_54;
                                    local_sp_0 = var_57;
                                }
                                var_58 = local_sp_0 + (-8L);
                                *(uint64_t *)var_58 = 4202843UL;
                                var_59 = indirect_placeholder_1(var_23);
                                rcx_1 = rcx_0;
                                rax_2 = var_59;
                                rcx_2 = rcx_0;
                                local_sp_2 = var_58;
                                if ((uint64_t)(uint32_t)var_59 == 0UL) {
                                    continue;
                                }
                                loop_state_var = 1U;
                                break;
                            }
                        switch (loop_state_var) {
                          case 0U:
                            {
                                rbp_1 = rbp_0;
                                r13_1 = r13_0 & rdx_0;
                                r12_1 = r12_0;
                                rcx_3 = rcx_1;
                                local_sp_3 = local_sp_1;
                            }
                            break;
                          case 1U:
                            {
                                *(uint64_t *)(local_sp_0 + (-16L)) = 4202852UL;
                                indirect_placeholder();
                                var_60 = (uint64_t)*(uint32_t *)var_59;
                                var_61 = local_sp_0 + (-24L);
                                *(uint64_t *)var_61 = 4202862UL;
                                var_62 = indirect_placeholder_9(var_23, rbp_2, var_60, r13_2, r12_2, rcx_0, var_23);
                                var_63 = var_62.field_0;
                                var_64 = var_62.field_2;
                                var_65 = var_62.field_3;
                                var_66 = var_62.field_4;
                                rbp_0 = var_64;
                                r13_0 = var_65;
                                r12_0 = var_66;
                                local_sp_1 = var_61;
                                if ((uint64_t)(unsigned char)var_63 == 0UL) {
                                    var_67 = var_62.field_1;
                                    *(uint64_t *)(local_sp_0 + (-32L)) = 4202884UL;
                                    var_68 = indirect_placeholder_8(4UL, var_67);
                                    var_69 = var_68.field_0;
                                    var_70 = var_68.field_2;
                                    var_71 = var_68.field_3;
                                    *(uint64_t *)(local_sp_0 + (-40L)) = 4202892UL;
                                    indirect_placeholder();
                                    var_72 = (uint64_t)*(uint32_t *)var_69;
                                    var_73 = local_sp_0 + (-48L);
                                    *(uint64_t *)var_73 = 4202917UL;
                                    indirect_placeholder_7(0UL, 4248700UL, 0UL, var_69, var_72, var_70, var_71);
                                    rdx_0 = 0UL;
                                    rcx_1 = var_69;
                                    local_sp_1 = var_73;
                                }
                            }
                            break;
                        }
                    }
                }
            return;
        }
    *(uint64_t *)(local_sp_6 + (-8L)) = 4202473UL;
    indirect_placeholder_3(var_1, rsi, 1UL);
    abort();
}

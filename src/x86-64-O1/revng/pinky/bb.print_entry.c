typedef unsigned char __u_char;
typedef unsigned short __u_short;
typedef unsigned int __u_int;
typedef unsigned long __u_long;
typedef signed char __int8_t;
typedef unsigned char __uint8_t;
typedef short __int16_t;
typedef unsigned short __uint16_t;
typedef int __int32_t;
typedef unsigned int __uint32_t;
typedef long __int64_t;
typedef unsigned long __uint64_t;
typedef __int8_t __int_least8_t;
typedef __uint8_t __uint_least8_t;
typedef __int16_t __int_least16_t;
typedef __uint16_t __uint_least16_t;
typedef __int32_t __int_least32_t;
typedef __uint32_t __uint_least32_t;
typedef __int64_t __int_least64_t;
typedef __uint64_t __uint_least64_t;
typedef long __quad_t;
typedef unsigned long __u_quad_t;
typedef long __intmax_t;
typedef unsigned long __uintmax_t;
typedef unsigned long __dev_t;
typedef unsigned int __uid_t;
typedef unsigned int __gid_t;
typedef unsigned long __ino_t;
typedef unsigned long __ino64_t;
typedef unsigned int __mode_t;
typedef unsigned long __nlink_t;
typedef long __off_t;
typedef long __off64_t;
typedef int __pid_t;
typedef struct {
    int __val[2];
} __fsid_t;
typedef long __clock_t;
typedef unsigned long __rlim_t;
typedef unsigned long __rlim64_t;
typedef unsigned int __id_t;
typedef long __time_t;
typedef unsigned int __useconds_t;
typedef long __suseconds_t;
typedef int __daddr_t;
typedef int __key_t;
typedef int __clockid_t;
typedef void *__timer_t;
typedef long __blksize_t;
typedef long __blkcnt_t;
typedef long __blkcnt64_t;
typedef unsigned long __fsblkcnt_t;
typedef unsigned long __fsblkcnt64_t;
typedef unsigned long __fsfilcnt_t;
typedef unsigned long __fsfilcnt64_t;
typedef long __fsword_t;
typedef long __ssize_t;
typedef long __syscall_slong_t;
typedef unsigned long __syscall_ulong_t;
typedef __off64_t __loff_t;
typedef char *__caddr_t;
typedef long __intptr_t;
typedef unsigned int __socklen_t;
typedef int __sig_atomic_t;
typedef __int8_t int8_t;
typedef __int16_t int16_t;
typedef __int32_t int32_t;
typedef __int64_t int64_t;
typedef __uint8_t uint8_t;
typedef __uint16_t uint16_t;
typedef __uint32_t uint32_t;
typedef __uint64_t uint64_t;
typedef __int_least8_t int_least8_t;
typedef __int_least16_t int_least16_t;
typedef __int_least32_t int_least32_t;
typedef __int_least64_t int_least64_t;
typedef __uint_least8_t uint_least8_t;
typedef __uint_least16_t uint_least16_t;
typedef __uint_least32_t uint_least32_t;
typedef __uint_least64_t uint_least64_t;
typedef signed char int_fast8_t;
typedef long int_fast16_t;
typedef long int_fast32_t;
typedef long int_fast64_t;
typedef unsigned char uint_fast8_t;
typedef unsigned long uint_fast16_t;
typedef unsigned long uint_fast32_t;
typedef unsigned long uint_fast64_t;
typedef long intptr_t;
typedef unsigned long uintptr_t;
typedef __intmax_t intmax_t;
typedef __uintmax_t uintmax_t;
typedef _Bool bool;
extern uint64_t revng_init_local_sp(uint64_t param_0);
extern uint64_t init_rbx(void);
extern void indirect_placeholder(void);
extern uint64_t init_rbp(void);
extern uint64_t init_cc_src2(void);
extern uint64_t helper_cc_compute_c_wrapper(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint32_t param_3);
extern uint64_t init_r14(void);
extern uint64_t init_r13(void);
extern uint64_t init_r12(void);
extern uint64_t indirect_placeholder_1(uint64_t param_0);
extern uint64_t indirect_placeholder_4(void);
extern uint64_t indirect_placeholder_6(uint64_t param_0, uint64_t param_1);
void bb_print_entry(uint64_t rdi) {
    uint64_t var_0;
    uint64_t var_1;
    uint64_t var_2;
    uint64_t var_3;
    uint64_t var_4;
    uint64_t var_5;
    uint64_t var_6;
    uint64_t var_7;
    uint64_t var_8;
    uint64_t var_9;
    unsigned char *var_10;
    uint64_t local_sp_9;
    uint64_t local_sp_2;
    uint64_t local_sp_0;
    uint64_t rbx_0;
    uint64_t var_66;
    uint64_t rdx_1;
    uint64_t var_61;
    uint64_t var_54;
    uint64_t local_sp_1;
    uint64_t rbp_1;
    uint64_t rbx_1;
    uint64_t rbp_0;
    uint64_t var_64;
    uint64_t var_62;
    uint64_t var_63;
    uint64_t rbx_2;
    uint64_t var_65;
    unsigned char var_55;
    uint64_t var_56;
    uint64_t rax_1;
    uint64_t rax_0;
    uint64_t rdx_0;
    unsigned char rcx_0_in;
    uint64_t var_57;
    uint64_t var_58;
    unsigned char var_59;
    uint64_t var_60;
    uint64_t local_sp_3;
    uint64_t local_sp_4;
    uint64_t var_53;
    uint64_t var_50;
    uint64_t var_51;
    uint64_t var_52;
    uint64_t var_37;
    uint64_t var_39;
    uint64_t var_38;
    uint64_t local_sp_5;
    uint64_t var_40;
    uint64_t local_sp_6;
    bool var_41;
    uint64_t var_42;
    uint64_t *var_43;
    uint64_t var_44;
    uint64_t *var_45;
    uint64_t var_46;
    uint64_t *var_47;
    uint64_t *_pre_phi;
    uint64_t local_sp_7;
    uint64_t *var_48;
    uint64_t var_49;
    uint64_t var_32;
    uint64_t var_33;
    uint64_t var_34;
    uint64_t var_23;
    uint64_t rax_3;
    unsigned char var_24;
    uint64_t rax_2;
    unsigned char rdx_2_in;
    uint64_t rbx_3;
    uint64_t var_25;
    uint64_t var_26;
    unsigned char var_27;
    unsigned char *var_28;
    bool var_29;
    uint64_t var_30;
    uint64_t *var_31;
    uint64_t local_sp_8;
    uint64_t var_35;
    uint64_t *var_36;
    uint64_t rax_5;
    uint64_t storemerge;
    uint64_t var_21;
    uint64_t var_22;
    uint64_t var_11;
    uint64_t var_12;
    uint64_t var_13;
    unsigned char var_14;
    uint64_t rax_4;
    uint64_t rdx_3;
    unsigned char rsi_0_in;
    uint64_t var_15;
    uint64_t var_16;
    unsigned char var_17;
    uint64_t var_18;
    uint64_t var_19;
    uint64_t var_20;
    var_0 = revng_init_local_sp(0UL);
    var_1 = init_rbx();
    var_2 = init_r14();
    var_3 = init_rbp();
    var_4 = init_r13();
    var_5 = init_r12();
    var_6 = init_cc_src2();
    *(uint64_t *)(var_0 + (-8L)) = var_2;
    *(uint64_t *)(var_0 + (-16L)) = var_4;
    *(uint64_t *)(var_0 + (-24L)) = var_5;
    *(uint64_t *)(var_0 + (-32L)) = var_3;
    *(uint64_t *)(var_0 + (-40L)) = var_1;
    var_7 = var_0 + (-504L);
    var_8 = var_0 + (-232L);
    var_9 = rdi + 8UL;
    var_10 = (unsigned char *)var_9;
    local_sp_9 = var_7;
    rbp_1 = 0UL;
    rax_5 = var_8;
    storemerge = 0UL;
    rax_4 = var_8;
    rdx_3 = var_9;
    if (*var_10 == '/') {
        var_11 = var_0 + (-512L);
        *(uint64_t *)var_11 = 4202162UL;
        indirect_placeholder();
        local_sp_9 = var_11;
    }
    var_12 = rdi + 40UL;
    var_13 = helper_cc_compute_c_wrapper(18446744073709551584UL, var_12, var_6, 17U);
    var_14 = *var_10;
    rsi_0_in = var_14;
    if (var_13 != 0UL & var_14 == '\x00') {
        var_15 = rax_4 + 1UL;
        var_16 = rdx_3 + 1UL;
        *(unsigned char *)rax_4 = rsi_0_in;
        rax_4 = var_15;
        rdx_3 = var_16;
        rax_5 = var_15;
        while (var_16 != var_12)
            {
                var_17 = *(unsigned char *)var_16;
                rsi_0_in = var_17;
                if (var_17 == '\x00') {
                    break;
                }
                var_15 = rax_4 + 1UL;
                var_16 = rdx_3 + 1UL;
                *(unsigned char *)rax_4 = rsi_0_in;
                rax_4 = var_15;
                rdx_3 = var_16;
                rax_5 = var_15;
            }
    }
    *(unsigned char *)rax_5 = (unsigned char)'\x00';
    var_18 = local_sp_9 + 320UL;
    var_19 = local_sp_9 + 272UL;
    *(uint64_t *)(local_sp_9 + (-8L)) = 4202236UL;
    var_20 = indirect_placeholder_6(var_19, var_18);
    if ((uint64_t)(uint32_t)var_20 == 0UL) {
        helper_cc_compute_c_wrapper((uint64_t)(*(uint32_t *)(local_sp_9 + 336UL) & 16U) + (-1L), 1UL, var_6, 16U);
        storemerge = *(uint64_t *)(local_sp_9 + 384UL);
    }
    var_21 = rdi + 44UL;
    var_22 = local_sp_9 + (-16L);
    *(uint64_t *)var_22 = 4202313UL;
    indirect_placeholder();
    rax_2 = var_22;
    rbx_3 = var_21;
    rax_3 = var_22;
    local_sp_8 = var_22;
    if (*(unsigned char *)6359557UL != '\x00') {
        var_23 = rdi + 76UL;
        var_24 = *(unsigned char *)var_21;
        rdx_2_in = var_24;
        if (var_23 <= var_21 & var_24 == '\x00') {
            var_25 = rax_2 + 1UL;
            var_26 = rbx_3 + 1UL;
            *(unsigned char *)rax_2 = rdx_2_in;
            rax_2 = var_25;
            rbx_3 = var_26;
            rax_3 = var_25;
            while (var_26 != var_23)
                {
                    var_27 = *(unsigned char *)var_26;
                    rdx_2_in = var_27;
                    if (var_27 == '\x00') {
                        break;
                    }
                    var_25 = rax_2 + 1UL;
                    var_26 = rbx_3 + 1UL;
                    *(unsigned char *)rax_2 = rdx_2_in;
                    rax_2 = var_25;
                    rbx_3 = var_26;
                    rax_3 = var_25;
                }
        }
        var_28 = (unsigned char *)rax_3;
        *var_28 = (unsigned char)'\x00';
        *(uint64_t *)(local_sp_9 + (-24L)) = 4202390UL;
        indirect_placeholder();
        var_29 = (rax_3 == 0UL);
        var_30 = local_sp_9 + (-32L);
        var_31 = (uint64_t *)var_30;
        local_sp_8 = var_30;
        if (var_29) {
            *var_31 = 4202418UL;
            indirect_placeholder();
        } else {
            *var_31 = 4202434UL;
            indirect_placeholder();
            *var_28 = (unsigned char)'\x00';
            var_32 = *(uint64_t *)(rax_3 + 24UL);
            var_33 = *(uint64_t *)rax_3;
            *(uint64_t *)(local_sp_9 + (-40L)) = 4202454UL;
            indirect_placeholder_6(var_32, var_33);
            *(uint64_t *)(local_sp_9 + (-48L)) = 4202475UL;
            indirect_placeholder();
            var_34 = local_sp_9 + (-56L);
            *(uint64_t *)var_34 = 4202483UL;
            indirect_placeholder();
            local_sp_8 = var_34;
        }
    }
    var_35 = local_sp_8 + (-8L);
    var_36 = (uint64_t *)var_35;
    *var_36 = 4202510UL;
    indirect_placeholder();
    local_sp_5 = var_35;
    _pre_phi = var_36;
    local_sp_7 = var_35;
    if (*(unsigned char *)6359559UL != '\x00') {
        if (storemerge == 0UL) {
            var_46 = local_sp_8 + (-16L);
            var_47 = (uint64_t *)var_46;
            *var_47 = 4202781UL;
            indirect_placeholder();
            _pre_phi = var_47;
            local_sp_7 = var_46;
        } else {
            var_37 = *(uint64_t *)6359976UL;
            var_39 = var_37;
            if (var_37 == 0UL) {
                var_38 = local_sp_8 + (-16L);
                *(uint64_t *)var_38 = 4202552UL;
                indirect_placeholder();
                var_39 = *(uint64_t *)6359976UL;
                local_sp_5 = var_38;
            }
            var_40 = var_39 - storemerge;
            local_sp_6 = local_sp_5;
            if ((long)var_40 <= (long)59UL) {
                var_41 = ((long)var_40 > (long)86399UL);
                var_42 = local_sp_5 + (-8L);
                var_43 = (uint64_t *)var_42;
                local_sp_6 = var_42;
                if (var_41) {
                    *var_43 = 4202739UL;
                    indirect_placeholder();
                } else {
                    *var_43 = 4202685UL;
                    indirect_placeholder();
                }
            }
            var_44 = local_sp_6 + (-8L);
            var_45 = (uint64_t *)var_44;
            *var_45 = 4202759UL;
            indirect_placeholder();
            _pre_phi = var_45;
            local_sp_7 = var_44;
        }
    }
    *_pre_phi = *(uint64_t *)(rdi + 344UL);
    var_48 = (uint64_t *)(local_sp_7 + (-8L));
    *var_48 = 4202800UL;
    var_49 = indirect_placeholder_4();
    if (var_49 == 0UL) {
        var_51 = *var_48;
        var_52 = local_sp_7 + (-16L);
        *(uint64_t *)var_52 = 4202851UL;
        indirect_placeholder_6(var_51, 6359904UL);
        local_sp_4 = var_52;
    } else {
        var_50 = local_sp_7 + (-16L);
        *(uint64_t *)var_50 = 4202830UL;
        indirect_placeholder();
        local_sp_4 = var_50;
    }
    var_53 = local_sp_4 + (-8L);
    *(uint64_t *)var_53 = 4202869UL;
    indirect_placeholder();
    rdx_0 = var_53;
    rdx_1 = var_53;
    local_sp_3 = var_53;
    var_54 = rdi + 76UL;
    var_55 = *(unsigned char *)var_54;
    rax_0 = var_54;
    rcx_0_in = var_55;
    rax_1 = var_54;
    if (~(*(unsigned char *)6359552UL != '\x00' & var_55 != '\x00')) {
        *(uint64_t *)(local_sp_3 + (-8L)) = 4203081UL;
        indirect_placeholder();
        return;
    }
    var_56 = rdi + 332UL;
    if (var_56 > var_54) {
        var_57 = rdx_0 + 1UL;
        var_58 = rax_0 + 1UL;
        *(unsigned char *)rdx_0 = rcx_0_in;
        rax_0 = var_58;
        rdx_0 = var_57;
        rax_1 = var_56;
        rdx_1 = var_57;
        while (var_58 != var_56)
            {
                var_59 = *(unsigned char *)var_58;
                rcx_0_in = var_59;
                rax_1 = var_58;
                if (var_59 == '\x00') {
                    break;
                }
                var_57 = rdx_0 + 1UL;
                var_58 = rax_0 + 1UL;
                *(unsigned char *)rdx_0 = rcx_0_in;
                rax_0 = var_58;
                rdx_0 = var_57;
                rax_1 = var_56;
                rdx_1 = var_57;
            }
    }
    *(unsigned char *)rdx_1 = (unsigned char)'\x00';
    var_60 = local_sp_4 + (-16L);
    *(uint64_t *)var_60 = 4202968UL;
    indirect_placeholder();
    local_sp_1 = var_60;
    rbx_1 = var_60;
    local_sp_2 = var_60;
    rbx_2 = var_60;
    if (rax_1 == 0UL) {
        if (*(unsigned char *)var_60 != '\x00') {
            var_65 = local_sp_2 + (-8L);
            *(uint64_t *)var_65 = 4203058UL;
            indirect_placeholder();
            local_sp_0 = var_65;
            rbx_0 = rbx_2;
            local_sp_3 = local_sp_0;
            if (rbx_0 != local_sp_0) {
                var_66 = local_sp_0 + (-8L);
                *(uint64_t *)var_66 = 4203071UL;
                indirect_placeholder();
                local_sp_3 = var_66;
            }
            *(uint64_t *)(local_sp_3 + (-8L)) = 4203081UL;
            indirect_placeholder();
            return;
        }
        var_62 = local_sp_4 + (-24L);
        *(uint64_t *)var_62 = 4203002UL;
        var_63 = indirect_placeholder_1(var_60);
        local_sp_1 = var_62;
        rbx_1 = (var_63 == 0UL) ? var_62 : var_63;
        rbp_0 = rbp_1;
    } else {
        var_61 = rax_1 + 1UL;
        *(unsigned char *)rax_1 = (unsigned char)'\x00';
        rbp_0 = var_61;
        rbp_1 = var_61;
        if (*(unsigned char *)var_60 == '\x00') {
            var_62 = local_sp_4 + (-24L);
            *(uint64_t *)var_62 = 4203002UL;
            var_63 = indirect_placeholder_1(var_60);
            local_sp_1 = var_62;
            rbx_1 = (var_63 == 0UL) ? var_62 : var_63;
            rbp_0 = rbp_1;
        }
    }
    rbx_0 = rbx_1;
    local_sp_2 = local_sp_1;
    rbx_2 = rbx_1;
    if (rbp_0 == 0UL) {
        var_65 = local_sp_2 + (-8L);
        *(uint64_t *)var_65 = 4203058UL;
        indirect_placeholder();
        local_sp_0 = var_65;
        rbx_0 = rbx_2;
    } else {
        var_64 = local_sp_1 + (-8L);
        *(uint64_t *)var_64 = 4203038UL;
        indirect_placeholder();
        local_sp_0 = var_64;
    }
    local_sp_3 = local_sp_0;
    if (rbx_0 != local_sp_0) {
        *(uint64_t *)(local_sp_3 + (-8L)) = 4203081UL;
        indirect_placeholder();
        return;
    }
    var_66 = local_sp_0 + (-8L);
    *(uint64_t *)var_66 = 4203071UL;
    indirect_placeholder();
    local_sp_3 = var_66;
    *(uint64_t *)(local_sp_3 + (-8L)) = 4203081UL;
    indirect_placeholder();
    return;
}

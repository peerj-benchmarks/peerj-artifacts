typedef unsigned char __u_char;
typedef unsigned short __u_short;
typedef unsigned int __u_int;
typedef unsigned long __u_long;
typedef signed char __int8_t;
typedef unsigned char __uint8_t;
typedef short __int16_t;
typedef unsigned short __uint16_t;
typedef int __int32_t;
typedef unsigned int __uint32_t;
typedef long __int64_t;
typedef unsigned long __uint64_t;
typedef __int8_t __int_least8_t;
typedef __uint8_t __uint_least8_t;
typedef __int16_t __int_least16_t;
typedef __uint16_t __uint_least16_t;
typedef __int32_t __int_least32_t;
typedef __uint32_t __uint_least32_t;
typedef __int64_t __int_least64_t;
typedef __uint64_t __uint_least64_t;
typedef long __quad_t;
typedef unsigned long __u_quad_t;
typedef long __intmax_t;
typedef unsigned long __uintmax_t;
typedef unsigned long __dev_t;
typedef unsigned int __uid_t;
typedef unsigned int __gid_t;
typedef unsigned long __ino_t;
typedef unsigned long __ino64_t;
typedef unsigned int __mode_t;
typedef unsigned long __nlink_t;
typedef long __off_t;
typedef long __off64_t;
typedef int __pid_t;
typedef struct {
    int __val[2];
} __fsid_t;
typedef long __clock_t;
typedef unsigned long __rlim_t;
typedef unsigned long __rlim64_t;
typedef unsigned int __id_t;
typedef long __time_t;
typedef unsigned int __useconds_t;
typedef long __suseconds_t;
typedef int __daddr_t;
typedef int __key_t;
typedef int __clockid_t;
typedef void *__timer_t;
typedef long __blksize_t;
typedef long __blkcnt_t;
typedef long __blkcnt64_t;
typedef unsigned long __fsblkcnt_t;
typedef unsigned long __fsblkcnt64_t;
typedef unsigned long __fsfilcnt_t;
typedef unsigned long __fsfilcnt64_t;
typedef long __fsword_t;
typedef long __ssize_t;
typedef long __syscall_slong_t;
typedef unsigned long __syscall_ulong_t;
typedef __off64_t __loff_t;
typedef char *__caddr_t;
typedef long __intptr_t;
typedef unsigned int __socklen_t;
typedef int __sig_atomic_t;
typedef __int8_t int8_t;
typedef __int16_t int16_t;
typedef __int32_t int32_t;
typedef __int64_t int64_t;
typedef __uint8_t uint8_t;
typedef __uint16_t uint16_t;
typedef __uint32_t uint32_t;
typedef __uint64_t uint64_t;
typedef __int_least8_t int_least8_t;
typedef __int_least16_t int_least16_t;
typedef __int_least32_t int_least32_t;
typedef __int_least64_t int_least64_t;
typedef __uint_least8_t uint_least8_t;
typedef __uint_least16_t uint_least16_t;
typedef __uint_least32_t uint_least32_t;
typedef __uint_least64_t uint_least64_t;
typedef signed char int_fast8_t;
typedef long int_fast16_t;
typedef long int_fast32_t;
typedef long int_fast64_t;
typedef unsigned char uint_fast8_t;
typedef unsigned long uint_fast16_t;
typedef unsigned long uint_fast32_t;
typedef unsigned long uint_fast64_t;
typedef long intptr_t;
typedef unsigned long uintptr_t;
typedef __intmax_t intmax_t;
typedef __uintmax_t uintmax_t;
typedef _Bool bool;
struct bb_keycmp_ret_type;
struct indirect_placeholder_18_ret_type;
struct bb_keycmp_ret_type {
    uint64_t field_0;
    uint64_t field_1;
    uint64_t field_2;
    uint64_t field_3;
    uint64_t field_4;
    uint64_t field_5;
    uint64_t field_6;
    uint64_t field_7;
    uint64_t field_8;
};
struct indirect_placeholder_18_ret_type {
    uint64_t field_0;
    uint64_t field_1;
    uint64_t field_2;
};
extern uint64_t revng_init_local_sp(uint64_t param_0);
extern uint64_t init_rbx(void);
extern void indirect_placeholder(void);
extern uint64_t init_rbp(void);
extern uint64_t init_cc_src2(void);
extern uint64_t helper_cc_compute_c_wrapper(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint32_t param_3);
extern uint64_t indirect_placeholder_2(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint64_t param_3, uint64_t param_4);
extern uint64_t init_r8(void);
extern uint64_t init_r9(void);
extern uint64_t init_r10(void);
extern struct indirect_placeholder_18_ret_type indirect_placeholder_18(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint64_t param_3, uint64_t param_4, uint64_t param_5, uint64_t param_6, uint64_t param_7, uint64_t param_8, uint64_t param_9);
struct bb_keycmp_ret_type bb_keycmp(uint64_t rdx, uint64_t r14, uint64_t rdi, uint64_t r13, uint64_t r12, uint64_t r15, uint64_t rcx, uint64_t rsi) {
    uint64_t var_23;
    uint64_t var_0;
    uint64_t var_1;
    uint64_t var_2;
    uint64_t var_3;
    uint64_t var_4;
    uint64_t var_5;
    uint64_t var_6;
    uint64_t var_14;
    struct indirect_placeholder_18_ret_type var_15;
    uint64_t var_16;
    uint64_t var_17;
    uint64_t var_18;
    uint64_t var_19;
    uint64_t rcx15_1;
    uint64_t var_20;
    uint64_t var_8;
    uint64_t var_9;
    uint64_t var_10;
    uint64_t var_11;
    uint64_t var_12;
    uint64_t var_13;
    uint64_t rcx15_0;
    bool var_21;
    uint64_t *var_22;
    uint64_t rbx_1;
    uint64_t rcx15_055;
    uint64_t var_7;
    uint64_t rax_0;
    struct bb_keycmp_ret_type mrv;
    struct bb_keycmp_ret_type mrv1;
    struct bb_keycmp_ret_type mrv2;
    struct bb_keycmp_ret_type mrv3;
    struct bb_keycmp_ret_type mrv4;
    struct bb_keycmp_ret_type mrv5;
    struct bb_keycmp_ret_type mrv6;
    struct bb_keycmp_ret_type mrv7;
    struct bb_keycmp_ret_type mrv8;
    var_0 = revng_init_local_sp(0UL);
    var_1 = init_rbx();
    var_2 = init_rbp();
    var_3 = init_cc_src2();
    var_4 = init_r10();
    var_5 = init_r9();
    var_6 = init_r8();
    *(uint64_t *)(var_0 + (-8L)) = var_2;
    *(uint64_t *)(var_0 + (-16L)) = var_1;
    rcx15_0 = rcx;
    rcx15_055 = rcx;
    rcx15_1 = rcx;
    rax_0 = 1UL;
    if (*(uint64_t *)(rdi + 24UL) > rdx) {
        var_8 = (rdx << 4UL) + *(uint64_t *)(rdi + 40UL);
        var_9 = *(uint64_t *)var_8;
        var_10 = *(uint64_t *)(var_8 + 8UL);
        if (*(uint64_t *)(rsi + 24UL) > rcx) {
            var_11 = (rcx << 4UL) + *(uint64_t *)(rsi + 40UL);
            var_12 = *(uint64_t *)var_11;
            var_13 = *(uint64_t *)(var_11 + 8UL);
            rcx15_0 = var_11;
            rbx_1 = var_13;
            rcx15_1 = var_11;
            if (var_10 == 0UL) {
                rcx15_055 = rcx15_0;
                rcx15_1 = rcx15_0;
                rax_0 = 4294967295UL;
                if (rbx_1 == 0UL) {
                    rcx15_1 = rcx15_055;
                    rax_0 = 0UL;
                }
            } else {
                if (var_13 != 0UL) {
                    rcx15_1 = var_13;
                    if (*(unsigned char *)6370657UL == '\x00') {
                        var_21 = (*(unsigned char *)6370814UL == '\x00');
                        var_22 = (uint64_t *)(var_0 + (-32L));
                        if (var_21) {
                            *var_22 = 4202866UL;
                            indirect_placeholder();
                        } else {
                            *var_22 = 4202849UL;
                            var_23 = indirect_placeholder_2(1UL, var_12, var_9, var_13, var_10);
                            rax_0 = var_23;
                        }
                    } else {
                        var_14 = (var_10 > var_13) ? var_13 : var_10;
                        *(uint64_t *)(var_0 + (-32L)) = 4202822UL;
                        var_15 = indirect_placeholder_18(1UL, var_14, var_13, r14, var_10, var_9, r13, r12, r15, var_12);
                        var_16 = var_15.field_0;
                        var_17 = var_15.field_1;
                        var_18 = var_15.field_2;
                        var_19 = (uint64_t)(uint32_t)var_16;
                        rax_0 = var_19;
                        var_20 = helper_cc_compute_c_wrapper(var_18 - var_17, var_17, var_3, 17U);
                        rax_0 = 4294967295UL;
                        if (var_19 != 0UL & var_20 == 0UL) {
                            rax_0 = (var_18 != var_17);
                        }
                    }
                }
            }
        } else {
            if (var_10 == 0UL) {
                rcx15_1 = rcx15_055;
                rax_0 = 0UL;
            }
        }
    } else {
        if (*(uint64_t *)(rsi + 24UL) > rcx) {
            rcx15_1 = rcx15_055;
            rax_0 = 0UL;
        } else {
            var_7 = *(uint64_t *)(((rcx << 4UL) + *(uint64_t *)(rsi + 40UL)) + 8UL);
            rbx_1 = var_7;
            rcx15_055 = rcx15_0;
            rcx15_1 = rcx15_0;
            rax_0 = 4294967295UL;
            if (rbx_1 == 0UL) {
                rcx15_1 = rcx15_055;
                rax_0 = 0UL;
            }
        }
    }
    mrv.field_0 = rax_0;
    mrv1 = mrv;
    mrv1.field_1 = r14;
    mrv2 = mrv1;
    mrv2.field_2 = r13;
    mrv3 = mrv2;
    mrv3.field_3 = r12;
    mrv4 = mrv3;
    mrv4.field_4 = r15;
    mrv5 = mrv4;
    mrv5.field_5 = rcx15_1;
    mrv6 = mrv5;
    mrv6.field_6 = var_4;
    mrv7 = mrv6;
    mrv7.field_7 = var_5;
    mrv8 = mrv7;
    mrv8.field_8 = var_6;
    return mrv8;
}

typedef unsigned char __u_char;
typedef unsigned short __u_short;
typedef unsigned int __u_int;
typedef unsigned long __u_long;
typedef signed char __int8_t;
typedef unsigned char __uint8_t;
typedef short __int16_t;
typedef unsigned short __uint16_t;
typedef int __int32_t;
typedef unsigned int __uint32_t;
typedef long __int64_t;
typedef unsigned long __uint64_t;
typedef __int8_t __int_least8_t;
typedef __uint8_t __uint_least8_t;
typedef __int16_t __int_least16_t;
typedef __uint16_t __uint_least16_t;
typedef __int32_t __int_least32_t;
typedef __uint32_t __uint_least32_t;
typedef __int64_t __int_least64_t;
typedef __uint64_t __uint_least64_t;
typedef long __quad_t;
typedef unsigned long __u_quad_t;
typedef long __intmax_t;
typedef unsigned long __uintmax_t;
typedef unsigned long __dev_t;
typedef unsigned int __uid_t;
typedef unsigned int __gid_t;
typedef unsigned long __ino_t;
typedef unsigned long __ino64_t;
typedef unsigned int __mode_t;
typedef unsigned long __nlink_t;
typedef long __off_t;
typedef long __off64_t;
typedef int __pid_t;
typedef struct {
    int __val[2];
} __fsid_t;
typedef long __clock_t;
typedef unsigned long __rlim_t;
typedef unsigned long __rlim64_t;
typedef unsigned int __id_t;
typedef long __time_t;
typedef unsigned int __useconds_t;
typedef long __suseconds_t;
typedef int __daddr_t;
typedef int __key_t;
typedef int __clockid_t;
typedef void *__timer_t;
typedef long __blksize_t;
typedef long __blkcnt_t;
typedef long __blkcnt64_t;
typedef unsigned long __fsblkcnt_t;
typedef unsigned long __fsblkcnt64_t;
typedef unsigned long __fsfilcnt_t;
typedef unsigned long __fsfilcnt64_t;
typedef long __fsword_t;
typedef long __ssize_t;
typedef long __syscall_slong_t;
typedef unsigned long __syscall_ulong_t;
typedef __off64_t __loff_t;
typedef char *__caddr_t;
typedef long __intptr_t;
typedef unsigned int __socklen_t;
typedef int __sig_atomic_t;
typedef __int8_t int8_t;
typedef __int16_t int16_t;
typedef __int32_t int32_t;
typedef __int64_t int64_t;
typedef __uint8_t uint8_t;
typedef __uint16_t uint16_t;
typedef __uint32_t uint32_t;
typedef __uint64_t uint64_t;
typedef __int_least8_t int_least8_t;
typedef __int_least16_t int_least16_t;
typedef __int_least32_t int_least32_t;
typedef __int_least64_t int_least64_t;
typedef __uint_least8_t uint_least8_t;
typedef __uint_least16_t uint_least16_t;
typedef __uint_least32_t uint_least32_t;
typedef __uint_least64_t uint_least64_t;
typedef signed char int_fast8_t;
typedef long int_fast16_t;
typedef long int_fast32_t;
typedef long int_fast64_t;
typedef unsigned char uint_fast8_t;
typedef unsigned long uint_fast16_t;
typedef unsigned long uint_fast32_t;
typedef unsigned long uint_fast64_t;
typedef long intptr_t;
typedef unsigned long uintptr_t;
typedef __intmax_t intmax_t;
typedef __uintmax_t uintmax_t;
struct bb_digest_word_file_ret_type;
struct indirect_placeholder_113_ret_type;
struct indirect_placeholder_114_ret_type;
struct bb_digest_word_file_ret_type {
    uint64_t field_0;
    uint64_t field_1;
    uint64_t field_2;
    uint64_t field_3;
};
struct indirect_placeholder_113_ret_type {
    uint64_t field_0;
    uint64_t field_1;
    uint64_t field_2;
};
struct indirect_placeholder_114_ret_type {
    uint64_t field_0;
    uint64_t field_1;
    uint64_t field_2;
    uint64_t field_3;
};
extern void abort(void);
extern uint64_t revng_init_local_sp(uint64_t param_0);
extern uint64_t init_rbx(void);
extern void indirect_placeholder(void);
extern uint64_t init_rbp(void);
extern uint64_t init_cc_src2(void);
extern uint64_t helper_cc_compute_c_wrapper(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint32_t param_3);
extern uint64_t init_r14(void);
extern uint64_t init_r13(void);
extern uint64_t init_r12(void);
extern void indirect_placeholder_9(uint64_t param_0, uint64_t param_1);
extern struct indirect_placeholder_113_ret_type indirect_placeholder_113(uint64_t param_0, uint64_t param_1);
extern struct indirect_placeholder_114_ret_type indirect_placeholder_114(uint64_t param_0, uint64_t param_1);
struct bb_digest_word_file_ret_type bb_digest_word_file(uint64_t rdi, uint64_t rsi) {
    uint64_t var_0;
    uint64_t var_1;
    uint64_t var_2;
    uint64_t var_3;
    uint64_t var_4;
    uint64_t var_5;
    uint64_t var_6;
    uint64_t var_7;
    uint64_t var_8;
    uint64_t *var_9;
    struct indirect_placeholder_113_ret_type var_10;
    uint64_t var_11;
    uint64_t var_12;
    uint64_t var_13;
    uint64_t *var_14;
    uint64_t *var_15;
    uint64_t *var_16;
    uint64_t rbp_0_be;
    uint64_t local_sp_0_be;
    uint64_t local_sp_0;
    uint64_t r10_0_be;
    uint64_t rbp_0;
    uint64_t r9_0_be;
    uint64_t r10_0;
    uint64_t r8_0_be;
    uint64_t r9_0;
    uint64_t r8_0;
    uint64_t *var_17;
    uint64_t var_18;
    uint64_t var_19;
    uint64_t local_sp_2;
    uint64_t rax_0;
    uint64_t var_20;
    uint64_t rbx_0;
    uint64_t var_21;
    uint64_t var_22;
    uint64_t *_pre_phi62;
    uint64_t var_23;
    uint64_t var_34;
    uint64_t rsi5_0;
    uint64_t var_24;
    uint64_t var_25;
    struct indirect_placeholder_114_ret_type var_26;
    uint64_t var_27;
    uint64_t var_28;
    uint64_t var_29;
    uint64_t var_30;
    uint64_t var_31;
    uint64_t var_32;
    uint64_t local_sp_1;
    uint64_t r10_1;
    uint64_t r9_1;
    uint64_t r8_1;
    uint64_t rbx_1;
    uint64_t r10_2;
    uint64_t r9_2;
    uint64_t r8_2;
    uint64_t var_33;
    struct bb_digest_word_file_ret_type mrv;
    struct bb_digest_word_file_ret_type mrv1;
    struct bb_digest_word_file_ret_type mrv2;
    struct bb_digest_word_file_ret_type mrv3;
    unsigned int loop_state_var;
    var_0 = revng_init_local_sp(0UL);
    var_1 = init_cc_src2();
    var_2 = init_r14();
    var_3 = init_rbx();
    var_4 = init_rbp();
    var_5 = init_r13();
    var_6 = init_r12();
    *(uint64_t *)(var_0 + (-8L)) = var_2;
    *(uint64_t *)(var_0 + (-16L)) = var_5;
    *(uint64_t *)(var_0 + (-24L)) = var_6;
    *(uint64_t *)(var_0 + (-32L)) = var_4;
    *(uint64_t *)(var_0 + (-40L)) = var_3;
    var_7 = var_0 + (-56L);
    var_8 = var_0 + (-64L);
    var_9 = (uint64_t *)var_8;
    *var_9 = 4204601UL;
    var_10 = indirect_placeholder_113(rdi, var_7);
    var_11 = var_10.field_0;
    var_12 = var_10.field_1;
    var_13 = var_10.field_2;
    var_14 = (uint64_t *)rsi;
    *var_14 = 0UL;
    var_15 = (uint64_t *)(rsi + 8UL);
    *var_15 = 0UL;
    var_16 = (uint64_t *)(rsi + 16UL);
    *var_16 = 0UL;
    local_sp_0 = var_8;
    rbp_0 = *var_9;
    r10_0 = var_11;
    r9_0 = var_12;
    r8_0 = var_13;
    rsi5_0 = 8UL;
    while (1U)
        {
            var_17 = (uint64_t *)(local_sp_0 + 8UL);
            var_18 = *var_17;
            var_19 = helper_cc_compute_c_wrapper(rbp_0 - var_18, var_18, var_1, 17U);
            local_sp_2 = local_sp_0;
            rax_0 = rbp_0;
            rbx_0 = var_18;
            _pre_phi62 = var_17;
            local_sp_1 = local_sp_0;
            r10_1 = r10_0;
            r9_1 = r9_0;
            r8_1 = r8_0;
            rbx_1 = rbp_0;
            r10_2 = r10_0;
            r9_2 = r9_0;
            r8_2 = r8_0;
            if (var_19 == 0UL) {
                var_34 = *var_14;
                *(uint64_t *)(local_sp_0 + (-8L)) = 4204875UL;
                indirect_placeholder();
                mrv.field_0 = var_34;
                mrv1 = mrv;
                mrv1.field_1 = r10_0;
                mrv2 = mrv1;
                mrv2.field_2 = r9_0;
                mrv3 = mrv2;
                mrv3.field_3 = r8_0;
                return mrv3;
            }
            if (*(unsigned char *)rbp_0 == '\n') {
                var_33 = rbx_1 + 1UL;
                rbp_0_be = var_33;
                local_sp_0_be = local_sp_2;
                r10_0_be = r10_2;
                r9_0_be = r9_2;
                r8_0_be = r8_2;
                local_sp_0 = local_sp_0_be;
                rbp_0 = rbp_0_be;
                r10_0 = r10_0_be;
                r9_0 = r9_0_be;
                r8_0 = r8_0_be;
                continue;
            }
            while (1U)
                {
                    var_20 = rax_0 + 1UL;
                    rax_0 = var_20;
                    rbx_1 = var_20;
                    if (var_20 != var_18) {
                        loop_state_var = 1U;
                        break;
                    }
                    rbx_0 = var_20;
                    if (*(unsigned char *)var_20 == '\n') {
                        continue;
                    }
                    loop_state_var = 0U;
                    break;
                }
            switch (loop_state_var) {
              case 1U:
                {
                    var_22 = *var_16;
                    var_32 = var_22;
                    rbx_1 = rbx_0;
                    rbp_0_be = rbx_0;
                    if (var_22 != *var_15) {
                        var_31 = *var_14;
                        *(uint64_t *)((var_32 << 4UL) + var_31) = rbp_0;
                        *(uint64_t *)(((*var_16 << 4UL) + *var_14) + 8UL) = (rbx_0 - rbp_0);
                        *var_16 = (*var_16 + 1UL);
                        local_sp_0_be = local_sp_1;
                        r10_0_be = r10_1;
                        r9_0_be = r9_1;
                        r8_0_be = r8_1;
                        local_sp_2 = local_sp_1;
                        r10_2 = r10_1;
                        r9_2 = r9_1;
                        r8_2 = r8_1;
                        if (*_pre_phi62 <= rbx_0) {
                            local_sp_0 = local_sp_0_be;
                            rbp_0 = rbp_0_be;
                            r10_0 = r10_0_be;
                            r9_0 = r9_0_be;
                            r8_0 = r8_0_be;
                            continue;
                        }
                    }
                    var_23 = *var_14;
                    if (var_23 == 0UL) {
                        rsi5_0 = var_22;
                        if (var_22 != 0UL & var_22 > 576460752303423487UL) {
                            *(uint64_t *)(local_sp_0 + (-8L)) = 4204718UL;
                            indirect_placeholder_9(r9_0, r8_0);
                            abort();
                        }
                    }
                    if (var_22 > 384307168202282324UL) {
                        *(uint64_t *)(local_sp_0 + (-8L)) = 4204728UL;
                        indirect_placeholder_9(r9_0, r8_0);
                        abort();
                    }
                    rsi5_0 = (var_22 + (var_22 >> 1UL)) + 1UL;
                    *var_15 = rsi5_0;
                    var_24 = rsi5_0 << 4UL;
                    var_25 = local_sp_0 + (-8L);
                    *(uint64_t *)var_25 = 4204760UL;
                    var_26 = indirect_placeholder_114(var_23, var_24);
                    var_27 = var_26.field_0;
                    var_28 = var_26.field_1;
                    var_29 = var_26.field_2;
                    var_30 = var_26.field_3;
                    *var_14 = var_27;
                    _pre_phi62 = (uint64_t *)(var_25 + 8UL);
                    var_31 = var_27;
                    var_32 = *var_16;
                    local_sp_1 = var_25;
                    r10_1 = var_28;
                    r9_1 = var_29;
                    r8_1 = var_30;
                }
                break;
              case 0U:
                {
                    var_21 = helper_cc_compute_c_wrapper(rbp_0 - var_20, var_20, var_1, 17U);
                    if (var_21 != 0UL) {
                        var_33 = rbx_1 + 1UL;
                        rbp_0_be = var_33;
                        local_sp_0_be = local_sp_2;
                        r10_0_be = r10_2;
                        r9_0_be = r9_2;
                        r8_0_be = r8_2;
                        local_sp_0 = local_sp_0_be;
                        rbp_0 = rbp_0_be;
                        r10_0 = r10_0_be;
                        r9_0 = r9_0_be;
                        r8_0 = r8_0_be;
                        continue;
                    }
                }
                break;
            }
        }
}

typedef unsigned char __u_char;
typedef unsigned short __u_short;
typedef unsigned int __u_int;
typedef unsigned long __u_long;
typedef signed char __int8_t;
typedef unsigned char __uint8_t;
typedef short __int16_t;
typedef unsigned short __uint16_t;
typedef int __int32_t;
typedef unsigned int __uint32_t;
typedef long __int64_t;
typedef unsigned long __uint64_t;
typedef __int8_t __int_least8_t;
typedef __uint8_t __uint_least8_t;
typedef __int16_t __int_least16_t;
typedef __uint16_t __uint_least16_t;
typedef __int32_t __int_least32_t;
typedef __uint32_t __uint_least32_t;
typedef __int64_t __int_least64_t;
typedef __uint64_t __uint_least64_t;
typedef long __quad_t;
typedef unsigned long __u_quad_t;
typedef long __intmax_t;
typedef unsigned long __uintmax_t;
typedef unsigned long __dev_t;
typedef unsigned int __uid_t;
typedef unsigned int __gid_t;
typedef unsigned long __ino_t;
typedef unsigned long __ino64_t;
typedef unsigned int __mode_t;
typedef unsigned long __nlink_t;
typedef long __off_t;
typedef long __off64_t;
typedef int __pid_t;
typedef struct {
    int __val[2];
} __fsid_t;
typedef long __clock_t;
typedef unsigned long __rlim_t;
typedef unsigned long __rlim64_t;
typedef unsigned int __id_t;
typedef long __time_t;
typedef unsigned int __useconds_t;
typedef long __suseconds_t;
typedef int __daddr_t;
typedef int __key_t;
typedef int __clockid_t;
typedef void *__timer_t;
typedef long __blksize_t;
typedef long __blkcnt_t;
typedef long __blkcnt64_t;
typedef unsigned long __fsblkcnt_t;
typedef unsigned long __fsblkcnt64_t;
typedef unsigned long __fsfilcnt_t;
typedef unsigned long __fsfilcnt64_t;
typedef long __fsword_t;
typedef long __ssize_t;
typedef long __syscall_slong_t;
typedef unsigned long __syscall_ulong_t;
typedef __off64_t __loff_t;
typedef char *__caddr_t;
typedef long __intptr_t;
typedef unsigned int __socklen_t;
typedef int __sig_atomic_t;
typedef __int8_t int8_t;
typedef __int16_t int16_t;
typedef __int32_t int32_t;
typedef __int64_t int64_t;
typedef __uint8_t uint8_t;
typedef __uint16_t uint16_t;
typedef __uint32_t uint32_t;
typedef __uint64_t uint64_t;
typedef __int_least8_t int_least8_t;
typedef __int_least16_t int_least16_t;
typedef __int_least32_t int_least32_t;
typedef __int_least64_t int_least64_t;
typedef __uint_least8_t uint_least8_t;
typedef __uint_least16_t uint_least16_t;
typedef __uint_least32_t uint_least32_t;
typedef __uint_least64_t uint_least64_t;
typedef signed char int_fast8_t;
typedef long int_fast16_t;
typedef long int_fast32_t;
typedef long int_fast64_t;
typedef unsigned char uint_fast8_t;
typedef unsigned long uint_fast16_t;
typedef unsigned long uint_fast32_t;
typedef unsigned long uint_fast64_t;
typedef long intptr_t;
typedef unsigned long uintptr_t;
typedef __intmax_t intmax_t;
typedef __uintmax_t uintmax_t;
extern uint64_t revng_init_local_sp(uint64_t param_0);
extern uint64_t init_rbx(void);
extern void indirect_placeholder(void);
extern uint64_t init_rbp(void);
extern uint64_t init_cc_src2(void);
extern uint64_t helper_cc_compute_c_wrapper(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint32_t param_3);
extern uint64_t init_r14(void);
extern uint64_t init_r13(void);
extern uint64_t init_r12(void);
extern uint64_t indirect_placeholder_10(uint64_t param_0);
extern uint64_t init_r15(void);
extern uint64_t init_rax(void);
extern void indirect_placeholder_9(uint64_t param_0, uint64_t param_1);
uint64_t bb_fread_file(uint64_t rdi, uint64_t rsi) {
    uint64_t var_0;
    uint64_t var_1;
    uint64_t var_2;
    uint64_t var_3;
    uint64_t var_4;
    uint64_t var_5;
    uint64_t var_6;
    uint64_t var_7;
    uint64_t var_8;
    uint64_t var_9;
    uint64_t local_sp_3;
    uint64_t rbx_1;
    uint64_t var_30;
    uint64_t rax_0;
    uint64_t rbp_0;
    uint64_t local_sp_2;
    uint64_t local_sp_0;
    uint64_t var_22;
    uint64_t var_23;
    uint32_t var_20;
    uint64_t var_21;
    uint64_t var_24;
    uint64_t var_25;
    uint64_t var_26;
    uint64_t var_27;
    uint64_t var_28;
    uint64_t var_29;
    uint64_t rax_1;
    uint64_t local_sp_1;
    uint32_t r13_0_shrunk;
    uint64_t rbx_0;
    uint64_t var_17;
    uint64_t var_18;
    uint64_t var_19;
    uint64_t rax_2;
    uint64_t var_12;
    uint64_t var_13;
    uint64_t var_14;
    uint32_t var_10;
    uint64_t var_11;
    uint64_t var_15;
    uint64_t var_16;
    unsigned int loop_state_var;
    var_0 = revng_init_local_sp(0UL);
    var_1 = init_rax();
    var_2 = init_cc_src2();
    var_3 = init_r14();
    var_4 = init_rbx();
    var_5 = init_rbp();
    var_6 = init_r13();
    var_7 = init_r12();
    var_8 = init_r15();
    *(uint64_t *)(var_0 + (-8L)) = var_8;
    *(uint64_t *)(var_0 + (-16L)) = var_3;
    *(uint64_t *)(var_0 + (-24L)) = var_6;
    *(uint64_t *)(var_0 + (-32L)) = var_7;
    *(uint64_t *)(var_0 + (-40L)) = var_5;
    *(uint64_t *)(var_0 + (-48L)) = var_4;
    *(uint64_t *)(var_0 + (-208L)) = rsi;
    *(uint64_t *)(var_0 + (-224L)) = 4223505UL;
    indirect_placeholder();
    var_9 = var_0 + (-232L);
    *(uint64_t *)var_9 = 4223517UL;
    indirect_placeholder();
    local_sp_3 = var_9;
    rbx_1 = 1024UL;
    rbp_0 = 0UL;
    r13_0_shrunk = 12U;
    rax_2 = 0UL;
    var_10 = (uint32_t)((uint16_t)*(uint32_t *)(var_0 + (-192L)) & (unsigned short)61440U);
    var_11 = (uint64_t)var_10;
    var_12 = var_0 + (-240L);
    *(uint64_t *)var_12 = 4223550UL;
    indirect_placeholder();
    var_13 = *(uint64_t *)(var_0 + (-176L));
    local_sp_3 = var_12;
    if ((int)(uint32_t)var_1 >= (int)0U & (uint64_t)((var_10 + (-32768)) & (-4096)) != 0UL & (long)var_13 <= (long)var_11) {
        var_14 = var_13 - var_11;
        if (var_14 != 18446744073709551615UL) {
            *(uint64_t *)(var_0 + (-248L)) = 4223579UL;
            indirect_placeholder();
            *(uint32_t *)var_11 = 12U;
            return rax_2;
        }
        rbx_1 = var_14 + 1UL;
    }
    var_15 = local_sp_3 + (-8L);
    *(uint64_t *)var_15 = 4223628UL;
    var_16 = indirect_placeholder_10(rbx_1);
    rax_1 = var_16;
    local_sp_2 = var_15;
    rbx_0 = rbx_1;
    if (var_16 == 0UL) {
        return rax_2;
    }
    while (1U)
        {
            var_17 = rbx_0 - rbp_0;
            var_18 = local_sp_2 + (-8L);
            *(uint64_t *)var_18 = 4223683UL;
            indirect_placeholder();
            var_19 = rbp_0 + rax_1;
            rax_0 = rax_1;
            local_sp_1 = var_18;
            rbp_0 = var_19;
            rax_2 = rax_1;
            if (rax_1 == var_17) {
                if (rbx_0 != 18446744073709551615UL) {
                    loop_state_var = 0U;
                    break;
                }
                var_24 = rbx_0 >> 1UL;
                var_25 = var_24 ^ (-1L);
                var_26 = var_24 + rbx_0;
                var_27 = helper_cc_compute_c_wrapper(rbx_0 - var_25, var_25, var_2, 17U);
                var_28 = (var_27 == 0UL) ? 18446744073709551615UL : var_26;
                var_29 = local_sp_2 + (-16L);
                *(uint64_t *)var_29 = 4223799UL;
                indirect_placeholder_9(rax_1, var_28);
                rax_0 = 0UL;
                rax_1 = var_28;
                local_sp_2 = var_29;
                rbx_0 = var_28;
                if (var_28 == 0UL) {
                    continue;
                }
                var_30 = local_sp_2 + (-24L);
                *(uint64_t *)var_30 = 4223813UL;
                indirect_placeholder();
                local_sp_1 = var_30;
                r13_0_shrunk = *(volatile uint32_t *)(uint32_t *)0UL;
                loop_state_var = 0U;
                break;
            }
            *(uint64_t *)(local_sp_2 + (-16L)) = 4223696UL;
            indirect_placeholder();
            var_20 = *(uint32_t *)rax_1;
            var_21 = local_sp_2 + (-24L);
            *(uint64_t *)var_21 = 4223707UL;
            indirect_placeholder();
            local_sp_0 = var_21;
            local_sp_1 = var_21;
            r13_0_shrunk = var_20;
            if ((uint64_t)(uint32_t)rax_1 != 0UL) {
                loop_state_var = 0U;
                break;
            }
            if ((rbx_0 + (-1L)) <= var_19) {
                loop_state_var = 1U;
                break;
            }
            var_22 = var_19 + 1UL;
            var_23 = local_sp_2 + (-32L);
            *(uint64_t *)var_23 = 4223732UL;
            indirect_placeholder_9(rax_1, var_22);
            local_sp_0 = var_23;
            loop_state_var = 1U;
            break;
        }
    switch (loop_state_var) {
      case 0U:
        {
            *(uint64_t *)(local_sp_1 + (-8L)) = 4223832UL;
            indirect_placeholder();
            *(uint64_t *)(local_sp_1 + (-16L)) = 4223837UL;
            indirect_placeholder();
            *(uint32_t *)rax_0 = r13_0_shrunk;
        }
        break;
      case 1U:
        {
            *(unsigned char *)(var_19 + rax_1) = (unsigned char)'\x00';
            **(uint64_t **)(local_sp_0 + 8UL) = var_19;
        }
        break;
    }
    return rax_2;
}

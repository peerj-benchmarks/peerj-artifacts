typedef unsigned char __u_char;
typedef unsigned short __u_short;
typedef unsigned int __u_int;
typedef unsigned long __u_long;
typedef signed char __int8_t;
typedef unsigned char __uint8_t;
typedef short __int16_t;
typedef unsigned short __uint16_t;
typedef int __int32_t;
typedef unsigned int __uint32_t;
typedef long __int64_t;
typedef unsigned long __uint64_t;
typedef __int8_t __int_least8_t;
typedef __uint8_t __uint_least8_t;
typedef __int16_t __int_least16_t;
typedef __uint16_t __uint_least16_t;
typedef __int32_t __int_least32_t;
typedef __uint32_t __uint_least32_t;
typedef __int64_t __int_least64_t;
typedef __uint64_t __uint_least64_t;
typedef long __quad_t;
typedef unsigned long __u_quad_t;
typedef long __intmax_t;
typedef unsigned long __uintmax_t;
typedef unsigned long __dev_t;
typedef unsigned int __uid_t;
typedef unsigned int __gid_t;
typedef unsigned long __ino_t;
typedef unsigned long __ino64_t;
typedef unsigned int __mode_t;
typedef unsigned long __nlink_t;
typedef long __off_t;
typedef long __off64_t;
typedef int __pid_t;
typedef struct {
    int __val[2];
} __fsid_t;
typedef long __clock_t;
typedef unsigned long __rlim_t;
typedef unsigned long __rlim64_t;
typedef unsigned int __id_t;
typedef long __time_t;
typedef unsigned int __useconds_t;
typedef long __suseconds_t;
typedef int __daddr_t;
typedef int __key_t;
typedef int __clockid_t;
typedef void *__timer_t;
typedef long __blksize_t;
typedef long __blkcnt_t;
typedef long __blkcnt64_t;
typedef unsigned long __fsblkcnt_t;
typedef unsigned long __fsblkcnt64_t;
typedef unsigned long __fsfilcnt_t;
typedef unsigned long __fsfilcnt64_t;
typedef long __fsword_t;
typedef long __ssize_t;
typedef long __syscall_slong_t;
typedef unsigned long __syscall_ulong_t;
typedef __off64_t __loff_t;
typedef char *__caddr_t;
typedef long __intptr_t;
typedef unsigned int __socklen_t;
typedef int __sig_atomic_t;
typedef __int8_t int8_t;
typedef __int16_t int16_t;
typedef __int32_t int32_t;
typedef __int64_t int64_t;
typedef __uint8_t uint8_t;
typedef __uint16_t uint16_t;
typedef __uint32_t uint32_t;
typedef __uint64_t uint64_t;
typedef __int_least8_t int_least8_t;
typedef __int_least16_t int_least16_t;
typedef __int_least32_t int_least32_t;
typedef __int_least64_t int_least64_t;
typedef __uint_least8_t uint_least8_t;
typedef __uint_least16_t uint_least16_t;
typedef __uint_least32_t uint_least32_t;
typedef __uint_least64_t uint_least64_t;
typedef signed char int_fast8_t;
typedef long int_fast16_t;
typedef long int_fast32_t;
typedef long int_fast64_t;
typedef unsigned char uint_fast8_t;
typedef unsigned long uint_fast16_t;
typedef unsigned long uint_fast32_t;
typedef unsigned long uint_fast64_t;
typedef long intptr_t;
typedef unsigned long uintptr_t;
typedef __intmax_t intmax_t;
typedef __uintmax_t uintmax_t;
typedef _Bool bool;
struct bb_check_halt_state_context_ret_type;
struct indirect_placeholder_130_ret_type;
struct bb_check_halt_state_context_ret_type {
    uint64_t field_0;
    uint64_t field_1;
    uint64_t field_2;
    uint64_t field_3;
    uint64_t field_4;
};
struct indirect_placeholder_130_ret_type {
    uint64_t field_0;
    uint64_t field_1;
};
extern uint64_t revng_init_local_sp(uint64_t param_0);
extern uint64_t init_rbp(void);
extern uint64_t init_rbx(void);
extern uint64_t helper_cc_compute_all_wrapper(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint32_t param_3);
extern uint64_t init_r10(void);
extern uint64_t init_r9(void);
extern struct indirect_placeholder_130_ret_type indirect_placeholder_130(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint64_t param_3);
struct bb_check_halt_state_context_ret_type bb_check_halt_state_context(uint64_t rax, uint64_t rdx, uint64_t rdi, uint64_t rsi) {
    uint64_t r10_0;
    uint64_t var_0;
    uint64_t var_1;
    uint64_t var_2;
    uint64_t var_3;
    uint64_t var_4;
    uint64_t var_5;
    struct indirect_placeholder_130_ret_type var_6;
    uint64_t var_7;
    uint64_t var_8;
    uint64_t var_9;
    uint64_t var_10;
    uint64_t rax5_0;
    uint64_t var_11;
    uint64_t var_12;
    uint64_t var_13;
    uint64_t var_14;
    uint64_t var_15;
    bool var_16;
    bool var_17;
    bool var_18;
    uint64_t rcx_0;
    uint64_t rsi8_0;
    uint64_t var_19;
    uint64_t var_20;
    uint64_t var_21;
    uint32_t var_22;
    uint64_t var_23;
    uint64_t r9_0;
    uint64_t var_24;
    struct bb_check_halt_state_context_ret_type mrv;
    struct bb_check_halt_state_context_ret_type mrv1;
    struct bb_check_halt_state_context_ret_type mrv2;
    struct bb_check_halt_state_context_ret_type mrv3;
    struct bb_check_halt_state_context_ret_type mrv4;
    var_0 = revng_init_local_sp(0UL);
    var_1 = init_rbp();
    var_2 = init_rbx();
    var_3 = init_r10();
    var_4 = init_r9();
    *(uint64_t *)(var_0 + (-8L)) = var_1;
    *(uint64_t *)(var_0 + (-16L)) = var_2;
    var_5 = (uint64_t)*(uint32_t *)(rdi + 160UL);
    *(uint64_t *)(var_0 + (-32L)) = 4232508UL;
    var_6 = indirect_placeholder_130(rax, var_5, rdi, rdx);
    var_7 = var_6.field_0;
    var_8 = var_6.field_1;
    var_9 = *(uint64_t *)(rsi + 16UL);
    var_10 = helper_cc_compute_all_wrapper(var_9, 0UL, 0UL, 25U);
    r10_0 = var_3;
    rax5_0 = 0UL;
    rcx_0 = var_8;
    rsi8_0 = 0UL;
    r9_0 = var_4;
    if ((uint64_t)(((unsigned char)(var_10 >> 4UL) ^ (unsigned char)var_10) & '\xc0') == 0UL) {
        mrv.field_0 = rax5_0;
        mrv1 = mrv;
        mrv1.field_1 = rcx_0;
        mrv2 = mrv1;
        mrv2.field_2 = r10_0;
        mrv3 = mrv2;
        mrv3.field_3 = r9_0;
        mrv4 = mrv3;
        mrv4.field_4 = var_9;
        return mrv4;
    }
    var_11 = *(uint64_t *)(rsi + 24UL);
    var_12 = **(uint64_t **)(rdi + 152UL);
    var_13 = var_7 & 8UL;
    var_14 = var_7 & 2UL;
    var_15 = var_7 & 1UL;
    var_16 = (var_15 == 0UL);
    var_17 = (var_14 == 0UL);
    var_18 = (var_13 == 0UL);
    r10_0 = var_15;
    r9_0 = var_11;
    while (1U)
        {
            var_19 = *(uint64_t *)((rsi8_0 << 3UL) + var_11);
            var_20 = (var_19 << 4UL) + var_12;
            var_21 = var_20 + 8UL;
            var_22 = *(uint32_t *)var_21 >> 8U;
            var_23 = (uint64_t)(uint32_t)((uint16_t)var_22 & (unsigned short)1023U);
            rax5_0 = var_19;
            rcx_0 = var_20;
            if (*(unsigned char *)var_21 == '\x02') {
                var_24 = rsi8_0 + 1UL;
                rsi8_0 = var_24;
                rax5_0 = 0UL;
                if (var_24 != var_9) {
                    continue;
                }
                break;
            }
            if ((uint64_t)((uint16_t)var_23 & (unsigned short)1023U) == 0UL) {
                break;
            }
            if ((var_23 & 4UL) == 0UL) {
                if (!(((var_23 & 8UL) == 0UL) || var_16)) {
                    var_24 = rsi8_0 + 1UL;
                    rsi8_0 = var_24;
                    rax5_0 = 0UL;
                    if (var_24 != var_9) {
                        continue;
                    }
                    break;
                }
            }
            if (!((var_16 ^ 1) && ((var_23 & 8UL) == 0UL))) {
                var_24 = rsi8_0 + 1UL;
                rsi8_0 = var_24;
                rax5_0 = 0UL;
                if (var_24 != var_9) {
                    continue;
                }
                break;
            }
            if (!(((var_23 & 32UL) == 0UL) || (var_17 ^ 1)) && ((signed char)(unsigned char)var_22 < '\x00') && var_18) {
                break;
            }
        }
    mrv.field_0 = rax5_0;
    mrv1 = mrv;
    mrv1.field_1 = rcx_0;
    mrv2 = mrv1;
    mrv2.field_2 = r10_0;
    mrv3 = mrv2;
    mrv3.field_3 = r9_0;
    mrv4 = mrv3;
    mrv4.field_4 = var_9;
    return mrv4;
}

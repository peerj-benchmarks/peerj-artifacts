typedef unsigned char __u_char;
typedef unsigned short __u_short;
typedef unsigned int __u_int;
typedef unsigned long __u_long;
typedef signed char __int8_t;
typedef unsigned char __uint8_t;
typedef short __int16_t;
typedef unsigned short __uint16_t;
typedef int __int32_t;
typedef unsigned int __uint32_t;
typedef long __int64_t;
typedef unsigned long __uint64_t;
typedef __int8_t __int_least8_t;
typedef __uint8_t __uint_least8_t;
typedef __int16_t __int_least16_t;
typedef __uint16_t __uint_least16_t;
typedef __int32_t __int_least32_t;
typedef __uint32_t __uint_least32_t;
typedef __int64_t __int_least64_t;
typedef __uint64_t __uint_least64_t;
typedef long __quad_t;
typedef unsigned long __u_quad_t;
typedef long __intmax_t;
typedef unsigned long __uintmax_t;
typedef unsigned long __dev_t;
typedef unsigned int __uid_t;
typedef unsigned int __gid_t;
typedef unsigned long __ino_t;
typedef unsigned long __ino64_t;
typedef unsigned int __mode_t;
typedef unsigned long __nlink_t;
typedef long __off_t;
typedef long __off64_t;
typedef int __pid_t;
typedef struct {
    int __val[2];
} __fsid_t;
typedef long __clock_t;
typedef unsigned long __rlim_t;
typedef unsigned long __rlim64_t;
typedef unsigned int __id_t;
typedef long __time_t;
typedef unsigned int __useconds_t;
typedef long __suseconds_t;
typedef int __daddr_t;
typedef int __key_t;
typedef int __clockid_t;
typedef void *__timer_t;
typedef long __blksize_t;
typedef long __blkcnt_t;
typedef long __blkcnt64_t;
typedef unsigned long __fsblkcnt_t;
typedef unsigned long __fsblkcnt64_t;
typedef unsigned long __fsfilcnt_t;
typedef unsigned long __fsfilcnt64_t;
typedef long __fsword_t;
typedef long __ssize_t;
typedef long __syscall_slong_t;
typedef unsigned long __syscall_ulong_t;
typedef __off64_t __loff_t;
typedef char *__caddr_t;
typedef long __intptr_t;
typedef unsigned int __socklen_t;
typedef int __sig_atomic_t;
typedef __int8_t int8_t;
typedef __int16_t int16_t;
typedef __int32_t int32_t;
typedef __int64_t int64_t;
typedef __uint8_t uint8_t;
typedef __uint16_t uint16_t;
typedef __uint32_t uint32_t;
typedef __uint64_t uint64_t;
typedef __int_least8_t int_least8_t;
typedef __int_least16_t int_least16_t;
typedef __int_least32_t int_least32_t;
typedef __int_least64_t int_least64_t;
typedef __uint_least8_t uint_least8_t;
typedef __uint_least16_t uint_least16_t;
typedef __uint_least32_t uint_least32_t;
typedef __uint_least64_t uint_least64_t;
typedef signed char int_fast8_t;
typedef long int_fast16_t;
typedef long int_fast32_t;
typedef long int_fast64_t;
typedef unsigned char uint_fast8_t;
typedef unsigned long uint_fast16_t;
typedef unsigned long uint_fast32_t;
typedef unsigned long uint_fast64_t;
typedef long intptr_t;
typedef unsigned long uintptr_t;
typedef __intmax_t intmax_t;
typedef __uintmax_t uintmax_t;
typedef _Bool bool;
struct bb_re_acquire_state_context_ret_type;
struct indirect_placeholder_134_ret_type;
struct indirect_placeholder_135_ret_type;
struct bb_re_acquire_state_context_ret_type {
    uint64_t field_0;
    uint64_t field_1;
    uint64_t field_2;
    uint64_t field_3;
    uint64_t field_4;
};
struct indirect_placeholder_134_ret_type {
    uint64_t field_0;
    uint64_t field_1;
    uint64_t field_2;
    uint64_t field_3;
};
struct indirect_placeholder_135_ret_type {
    uint64_t field_0;
    uint64_t field_1;
    uint64_t field_2;
    uint64_t field_3;
    uint64_t field_4;
    uint64_t field_5;
    uint64_t field_6;
};
extern uint64_t revng_init_local_sp(uint64_t param_0);
extern uint64_t init_rbp(void);
extern uint64_t init_rbx(void);
extern uint64_t init_r12(void);
extern void indirect_placeholder(void);
extern uint64_t init_r14(void);
extern uint64_t init_r13(void);
extern uint64_t indirect_placeholder_4(uint64_t param_0);
extern uint64_t helper_cc_compute_all_wrapper(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint32_t param_3);
extern uint64_t init_r15(void);
extern void indirect_placeholder_17(uint64_t param_0);
extern uint64_t indirect_placeholder_5(uint64_t param_0, uint64_t param_1);
extern uint64_t indirect_placeholder_3(uint64_t param_0, uint64_t param_1, uint64_t param_2);
extern void indirect_placeholder_1(uint64_t param_0, uint64_t param_1);
extern struct indirect_placeholder_134_ret_type indirect_placeholder_134(uint64_t param_0, uint64_t param_1);
extern struct indirect_placeholder_135_ret_type indirect_placeholder_135(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint64_t param_3, uint64_t param_4, uint64_t param_5, uint64_t param_6, uint64_t param_7);
struct bb_re_acquire_state_context_ret_type bb_re_acquire_state_context(uint64_t rdx, uint64_t rdi, uint64_t rcx, uint64_t r10, uint64_t rsi, uint64_t r9, uint64_t r8) {
    uint64_t var_0;
    uint64_t var_1;
    uint64_t var_2;
    uint64_t var_3;
    uint64_t var_4;
    uint64_t var_5;
    uint64_t var_6;
    uint64_t var_7;
    uint64_t *var_8;
    uint32_t *var_9;
    uint32_t var_10;
    uint64_t var_11;
    uint64_t r15_0;
    uint64_t var_66;
    uint64_t local_sp_1;
    uint64_t var_59;
    uint64_t rbx_2;
    uint64_t var_42;
    uint32_t *var_43;
    uint32_t var_44;
    unsigned char *var_45;
    uint64_t *var_46;
    uint64_t *var_47;
    uint64_t local_sp_3;
    uint64_t r811_2;
    uint64_t local_sp_2;
    uint64_t local_sp_0;
    uint64_t rbx_0;
    uint64_t r811_0;
    uint64_t var_63;
    uint64_t var_48;
    uint64_t var_49;
    uint64_t var_50;
    uint64_t var_51;
    bool var_52;
    uint32_t var_53;
    uint32_t _pre_phi;
    unsigned char var_54;
    uint64_t var_55;
    bool var_56;
    uint64_t var_57;
    uint64_t *var_58;
    uint64_t r811_1;
    uint64_t var_60;
    uint64_t var_61;
    uint64_t var_62;
    uint64_t var_31;
    uint64_t rcx7_0;
    uint64_t r811_3;
    uint64_t var_64;
    uint64_t var_65;
    uint64_t var_39;
    uint64_t var_40;
    uint64_t var_41;
    uint64_t local_sp_7;
    uint64_t local_sp_4;
    uint64_t rcx7_1;
    uint64_t r811_4;
    uint64_t r12_0;
    uint64_t rcx7_2;
    uint64_t r108_0;
    uint64_t r910_0;
    uint64_t r811_5;
    struct bb_re_acquire_state_context_ret_type mrv;
    struct bb_re_acquire_state_context_ret_type mrv1;
    struct bb_re_acquire_state_context_ret_type mrv2;
    struct bb_re_acquire_state_context_ret_type mrv3;
    struct bb_re_acquire_state_context_ret_type mrv4;
    uint64_t var_12;
    uint64_t var_13;
    uint64_t r15_2;
    uint64_t var_14;
    uint64_t var_15;
    uint64_t rax_0;
    uint64_t r15_1;
    uint64_t var_16;
    uint64_t var_17;
    uint64_t var_18;
    uint64_t var_19;
    uint64_t var_20;
    uint64_t var_21;
    uint64_t rcx7_4;
    uint64_t local_sp_6;
    uint64_t local_sp_5;
    uint64_t rbx_1;
    uint64_t r910_2;
    uint64_t rcx7_3;
    uint64_t r811_7;
    uint64_t r910_1;
    uint64_t r811_6;
    uint64_t var_22;
    uint64_t var_23;
    uint64_t var_24;
    struct indirect_placeholder_134_ret_type var_25;
    uint64_t var_26;
    uint64_t var_27;
    uint64_t var_28;
    uint64_t var_29;
    uint64_t var_30;
    uint64_t rcx7_5;
    uint64_t r910_3;
    uint64_t r811_8;
    struct indirect_placeholder_135_ret_type var_32;
    uint64_t var_33;
    uint64_t var_34;
    uint64_t var_35;
    uint64_t var_36;
    uint64_t var_37;
    uint64_t var_38;
    unsigned int loop_state_var;
    var_0 = revng_init_local_sp(0UL);
    var_1 = init_rbp();
    var_2 = init_rbx();
    var_3 = init_r14();
    var_4 = init_r13();
    var_5 = init_r12();
    var_6 = init_r15();
    *(uint64_t *)(var_0 + (-8L)) = var_6;
    *(uint64_t *)(var_0 + (-16L)) = var_3;
    *(uint64_t *)(var_0 + (-24L)) = var_4;
    *(uint64_t *)(var_0 + (-32L)) = var_5;
    *(uint64_t *)(var_0 + (-40L)) = var_1;
    *(uint64_t *)(var_0 + (-48L)) = var_2;
    var_7 = var_0 + (-104L);
    *(uint64_t *)(var_0 + (-72L)) = rdi;
    var_8 = (uint64_t *)(var_0 + (-88L));
    *var_8 = rsi;
    var_9 = (uint32_t *)(var_0 + (-92L));
    var_10 = (uint32_t)rcx;
    *var_9 = var_10;
    var_11 = *(uint64_t *)(rdx + 8UL);
    rbx_2 = var_2;
    rbx_0 = 0UL;
    r811_0 = 0UL;
    local_sp_7 = var_7;
    r12_0 = 0UL;
    rcx7_2 = rcx;
    r108_0 = r10;
    r910_0 = r9;
    r811_5 = r8;
    local_sp_5 = var_7;
    rbx_1 = 0UL;
    r910_1 = r9;
    r811_6 = r8;
    r910_3 = r9;
    r811_8 = r8;
    if (var_11 == 0UL) {
        *(uint32_t *)rdi = 0U;
    } else {
        var_12 = var_11 + (uint64_t)var_10;
        var_13 = helper_cc_compute_all_wrapper(var_11, 0UL, 0UL, 25U);
        r15_1 = var_12;
        r15_2 = var_12;
        if ((uint64_t)(((unsigned char)(var_13 >> 4UL) ^ (unsigned char)var_13) & '\xc0') != 0UL) {
            var_14 = *(uint64_t *)(rdx + 16UL);
            var_15 = (var_11 << 3UL) + var_14;
            rax_0 = var_14;
            var_16 = r15_1 + *(uint64_t *)rax_0;
            var_17 = rax_0 + 8UL;
            rax_0 = var_17;
            r15_1 = var_16;
            r15_2 = var_16;
            do {
                var_16 = r15_1 + *(uint64_t *)rax_0;
                var_17 = rax_0 + 8UL;
                rax_0 = var_17;
                r15_1 = var_16;
                r15_2 = var_16;
            } while (var_17 != var_15);
        }
        var_18 = *var_8;
        var_19 = ((r15_2 & *(uint64_t *)(var_18 + 136UL)) * 24UL) + *(uint64_t *)(var_18 + 64UL);
        var_20 = *(uint64_t *)var_19;
        var_21 = helper_cc_compute_all_wrapper(var_20, 0UL, 0UL, 25U);
        r15_0 = r15_2;
        rcx7_3 = var_18;
        rcx7_5 = var_18;
        if ((uint64_t)(((unsigned char)(var_21 >> 4UL) ^ (unsigned char)var_21) & '\xc0') != 0UL) {
            rbx_2 = var_20;
            while (1U)
                {
                    var_22 = *(uint64_t *)((rbx_1 << 3UL) + *(uint64_t *)(var_19 + 16UL));
                    r12_0 = var_22;
                    rcx7_4 = rcx7_3;
                    local_sp_6 = local_sp_5;
                    r910_2 = r910_1;
                    r811_7 = r811_6;
                    if (*(uint64_t *)var_22 != r15_2) {
                        var_23 = *(uint64_t *)(var_22 + 80UL);
                        var_24 = local_sp_5 + (-8L);
                        *(uint64_t *)var_24 = 4236883UL;
                        var_25 = indirect_placeholder_134(var_23, rdx);
                        var_26 = var_25.field_0;
                        var_27 = var_25.field_1;
                        var_28 = var_25.field_2;
                        var_29 = var_25.field_3;
                        rcx7_2 = var_27;
                        r910_0 = var_28;
                        r811_5 = var_29;
                        rcx7_4 = var_27;
                        local_sp_6 = var_24;
                        r910_2 = var_28;
                        r811_7 = var_29;
                        if ((uint64_t)((uint32_t)(uint64_t)(*(unsigned char *)(var_22 + 104UL) & '\x0f') - *(uint32_t *)(local_sp_5 + 12UL)) != 0UL & (uint64_t)(unsigned char)var_26 != 0UL) {
                            loop_state_var = 1U;
                            break;
                        }
                    }
                    var_30 = rbx_1 + 1UL;
                    local_sp_7 = local_sp_6;
                    local_sp_5 = local_sp_6;
                    rbx_1 = var_30;
                    rcx7_3 = rcx7_4;
                    r910_1 = r910_2;
                    r811_6 = r811_7;
                    rcx7_5 = rcx7_4;
                    r910_3 = r910_2;
                    r811_8 = r811_7;
                    if (var_30 == var_20) {
                        continue;
                    }
                    loop_state_var = 0U;
                    break;
                }
            switch (loop_state_var) {
              case 1U:
                {
                    mrv.field_0 = r12_0;
                    mrv1 = mrv;
                    mrv1.field_1 = rcx7_2;
                    mrv2 = mrv1;
                    mrv2.field_2 = r108_0;
                    mrv3 = mrv2;
                    mrv3.field_3 = r910_0;
                    mrv4 = mrv3;
                    mrv4.field_4 = r811_5;
                    return mrv4;
                }
                break;
              case 0U:
                {
                    break;
                }
                break;
            }
        }
        var_31 = local_sp_7 + (-8L);
        *(uint64_t *)var_31 = 4236915UL;
        var_32 = indirect_placeholder_135(rdx, rbx_2, 112UL, rcx7_5, r10, 1UL, r910_3, r811_8);
        var_33 = var_32.field_0;
        var_34 = var_32.field_1;
        var_35 = var_32.field_3;
        var_36 = var_32.field_4;
        var_37 = var_32.field_5;
        var_38 = var_32.field_6;
        rcx7_0 = var_35;
        r811_3 = var_38;
        local_sp_4 = var_31;
        rcx7_1 = var_35;
        r811_4 = var_38;
        r12_0 = var_33;
        r108_0 = var_36;
        r910_0 = var_37;
        if (var_33 == 0UL) {
            var_39 = var_33 + 8UL;
            var_40 = local_sp_7 + (-16L);
            *(uint64_t *)var_40 = 4236942UL;
            var_41 = indirect_placeholder_5(var_39, var_34);
            local_sp_0 = var_40;
            local_sp_3 = var_40;
            if ((uint64_t)(uint32_t)var_41 == 0UL) {
                var_43 = (uint32_t *)(local_sp_7 + (-4L));
                var_44 = *var_43;
                var_45 = (unsigned char *)(var_33 + 104UL);
                *var_45 = ((*var_45 & '\xf0') | ((unsigned char)var_44 & '\x0f'));
                var_46 = (uint64_t *)(var_33 + 80UL);
                *var_46 = var_39;
                var_47 = (uint64_t *)(var_34 + 8UL);
                if ((long)*var_47 <= (long)0UL) {
                    *(uint32_t *)(local_sp_7 + 12UL) = (var_44 & 4U);
                    *(uint32_t *)(local_sp_7 + 8UL) = (var_44 & 2U);
                    *var_43 = (var_44 & 1U);
                    *(uint64_t *)(local_sp_7 + 24UL) = r15_2;
                    while (1U)
                        {
                            var_48 = (*(uint64_t *)((rbx_0 << 3UL) + *(uint64_t *)(var_34 + 16UL)) << 4UL) + **(uint64_t **)local_sp_7;
                            var_49 = var_48 + 8UL;
                            var_50 = (uint64_t)*(unsigned char *)var_49;
                            var_51 = (uint64_t)(*(uint32_t *)var_49 >> 8U);
                            var_52 = ((uint64_t)((uint16_t)var_51 & (unsigned short)1023U) == 0UL);
                            local_sp_1 = local_sp_0;
                            r811_2 = r811_0;
                            local_sp_2 = local_sp_0;
                            r811_1 = r811_0;
                            rcx7_0 = var_50;
                            rcx7_1 = var_50;
                            r811_4 = r811_0;
                            if (var_52) {
                                var_53 = (uint32_t)var_50;
                                _pre_phi = var_53;
                                if ((uint64_t)(var_53 + (-1)) != 0UL) {
                                    var_63 = rbx_0 + 1UL;
                                    local_sp_3 = local_sp_2;
                                    local_sp_0 = local_sp_2;
                                    rbx_0 = var_63;
                                    r811_0 = r811_2;
                                    r811_3 = r811_2;
                                    if ((long)var_63 < (long)*var_47) {
                                        continue;
                                    }
                                    r15_0 = *(uint64_t *)(local_sp_2 + 40UL);
                                    loop_state_var = 1U;
                                    break;
                                }
                            }
                            _pre_phi = (uint32_t)var_50;
                            var_54 = ((*(unsigned char *)(var_48 + 10UL) << '\x01') & ' ') | *var_45;
                            *var_45 = var_54;
                            if ((uint64_t)(_pre_phi + (-2)) == 0UL) {
                                *var_45 = (var_54 | '\x10');
                            } else {
                                if ((uint64_t)(_pre_phi + (-4)) == 0UL) {
                                    *var_45 = (var_54 | '@');
                                }
                            }
                            if (var_52) {
                                var_63 = rbx_0 + 1UL;
                                local_sp_3 = local_sp_2;
                                local_sp_0 = local_sp_2;
                                rbx_0 = var_63;
                                r811_0 = r811_2;
                                r811_3 = r811_2;
                                if ((long)var_63 < (long)*var_47) {
                                    continue;
                                }
                                r15_0 = *(uint64_t *)(local_sp_2 + 40UL);
                                loop_state_var = 1U;
                                break;
                            }
                            if (var_39 != *var_46) {
                                *(uint64_t *)(local_sp_0 + (-8L)) = 4237176UL;
                                var_55 = indirect_placeholder_4(24UL);
                                *var_46 = var_55;
                                var_56 = (var_55 == 0UL);
                                var_57 = local_sp_0 + (-16L);
                                var_58 = (uint64_t *)var_57;
                                local_sp_1 = var_57;
                                r811_1 = 0UL;
                                local_sp_4 = var_57;
                                if (!var_56) {
                                    *var_58 = 4237193UL;
                                    indirect_placeholder_17(var_33);
                                    loop_state_var = 0U;
                                    break;
                                }
                                *var_58 = 4237209UL;
                                var_59 = indirect_placeholder_5(var_55, var_34);
                                if ((uint64_t)(uint32_t)var_59 != 0UL) {
                                    loop_state_var = 0U;
                                    break;
                                }
                                *var_45 = (*var_45 | '\x80');
                            }
                            local_sp_2 = local_sp_1;
                            r811_2 = r811_1;
                            if ((var_51 & 1UL) == 0UL) {
                                if ((var_51 & 2UL) != 0UL & *(uint32_t *)(local_sp_1 + 12UL) != 0U) {
                                    var_60 = rbx_0 - r811_1;
                                    var_61 = local_sp_1 + (-8L);
                                    *(uint64_t *)var_61 = 4237299UL;
                                    indirect_placeholder_1(var_39, var_60);
                                    var_62 = r811_1 + 1UL;
                                    local_sp_2 = var_61;
                                    r811_2 = var_62;
                                    var_63 = rbx_0 + 1UL;
                                    local_sp_3 = local_sp_2;
                                    local_sp_0 = local_sp_2;
                                    rbx_0 = var_63;
                                    r811_0 = r811_2;
                                    r811_3 = r811_2;
                                    if ((long)var_63 < (long)*var_47) {
                                        continue;
                                    }
                                    r15_0 = *(uint64_t *)(local_sp_2 + 40UL);
                                    loop_state_var = 1U;
                                    break;
                                }
                            }
                            if (!((*(uint32_t *)(local_sp_1 + 12UL) != 0U) && ((var_51 & 2UL) == 0UL))) {
                                var_60 = rbx_0 - r811_1;
                                var_61 = local_sp_1 + (-8L);
                                *(uint64_t *)var_61 = 4237299UL;
                                indirect_placeholder_1(var_39, var_60);
                                var_62 = r811_1 + 1UL;
                                local_sp_2 = var_61;
                                r811_2 = var_62;
                                var_63 = rbx_0 + 1UL;
                                local_sp_3 = local_sp_2;
                                local_sp_0 = local_sp_2;
                                rbx_0 = var_63;
                                r811_0 = r811_2;
                                r811_3 = r811_2;
                                if ((long)var_63 < (long)*var_47) {
                                    continue;
                                }
                                r15_0 = *(uint64_t *)(local_sp_2 + 40UL);
                                loop_state_var = 1U;
                                break;
                            }
                            if ((var_51 & 16UL) != 0UL) {
                                if (*(uint32_t *)(local_sp_1 + 24UL) != 0U) {
                                    var_60 = rbx_0 - r811_1;
                                    var_61 = local_sp_1 + (-8L);
                                    *(uint64_t *)var_61 = 4237299UL;
                                    indirect_placeholder_1(var_39, var_60);
                                    var_62 = r811_1 + 1UL;
                                    local_sp_2 = var_61;
                                    r811_2 = var_62;
                                    var_63 = rbx_0 + 1UL;
                                    local_sp_3 = local_sp_2;
                                    local_sp_0 = local_sp_2;
                                    rbx_0 = var_63;
                                    r811_0 = r811_2;
                                    r811_3 = r811_2;
                                    if ((long)var_63 < (long)*var_47) {
                                        continue;
                                    }
                                    r15_0 = *(uint64_t *)(local_sp_2 + 40UL);
                                    loop_state_var = 1U;
                                    break;
                                }
                            }
                            if ((var_51 & 64UL) != 0UL & *(uint32_t *)(local_sp_1 + 28UL) == 0U) {
                                var_60 = rbx_0 - r811_1;
                                var_61 = local_sp_1 + (-8L);
                                *(uint64_t *)var_61 = 4237299UL;
                                indirect_placeholder_1(var_39, var_60);
                                var_62 = r811_1 + 1UL;
                                local_sp_2 = var_61;
                                r811_2 = var_62;
                            }
                        }
                    switch (loop_state_var) {
                      case 1U:
                        {
                            break;
                        }
                        break;
                      case 0U:
                        {
                            **(uint32_t **)(local_sp_4 + 32UL) = 12U;
                            rcx7_2 = rcx7_1;
                            r811_5 = r811_4;
                            mrv.field_0 = r12_0;
                            mrv1 = mrv;
                            mrv1.field_1 = rcx7_2;
                            mrv2 = mrv1;
                            mrv2.field_2 = r108_0;
                            mrv3 = mrv2;
                            mrv3.field_3 = r910_0;
                            mrv4 = mrv3;
                            mrv4.field_4 = r811_5;
                            return mrv4;
                        }
                        break;
                    }
                }
                var_64 = *(uint64_t *)(local_sp_3 + 16UL);
                *(uint64_t *)(local_sp_3 + (-8L)) = 4237344UL;
                var_65 = indirect_placeholder_3(r15_0, var_64, var_33);
                rcx7_1 = rcx7_0;
                r811_4 = r811_3;
                rcx7_2 = rcx7_0;
                r811_5 = r811_3;
                if ((uint64_t)(uint32_t)var_65 != 0UL) {
                    var_66 = local_sp_3 + (-16L);
                    *(uint64_t *)var_66 = 4237359UL;
                    indirect_placeholder_17(var_33);
                    local_sp_4 = var_66;
                    **(uint32_t **)(local_sp_4 + 32UL) = 12U;
                    rcx7_2 = rcx7_1;
                    r811_5 = r811_4;
                }
            } else {
                var_42 = local_sp_7 + (-24L);
                *(uint64_t *)var_42 = 4236954UL;
                indirect_placeholder();
                local_sp_4 = var_42;
                **(uint32_t **)(local_sp_4 + 32UL) = 12U;
                rcx7_2 = rcx7_1;
                r811_5 = r811_4;
            }
        } else {
            **(uint32_t **)(local_sp_4 + 32UL) = 12U;
            rcx7_2 = rcx7_1;
            r811_5 = r811_4;
        }
    }
}

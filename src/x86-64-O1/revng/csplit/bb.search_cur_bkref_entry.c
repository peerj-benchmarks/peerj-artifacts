typedef unsigned char __u_char;
typedef unsigned short __u_short;
typedef unsigned int __u_int;
typedef unsigned long __u_long;
typedef signed char __int8_t;
typedef unsigned char __uint8_t;
typedef short __int16_t;
typedef unsigned short __uint16_t;
typedef int __int32_t;
typedef unsigned int __uint32_t;
typedef long __int64_t;
typedef unsigned long __uint64_t;
typedef __int8_t __int_least8_t;
typedef __uint8_t __uint_least8_t;
typedef __int16_t __int_least16_t;
typedef __uint16_t __uint_least16_t;
typedef __int32_t __int_least32_t;
typedef __uint32_t __uint_least32_t;
typedef __int64_t __int_least64_t;
typedef __uint64_t __uint_least64_t;
typedef long __quad_t;
typedef unsigned long __u_quad_t;
typedef long __intmax_t;
typedef unsigned long __uintmax_t;
typedef unsigned long __dev_t;
typedef unsigned int __uid_t;
typedef unsigned int __gid_t;
typedef unsigned long __ino_t;
typedef unsigned long __ino64_t;
typedef unsigned int __mode_t;
typedef unsigned long __nlink_t;
typedef long __off_t;
typedef long __off64_t;
typedef int __pid_t;
typedef struct {
    int __val[2];
} __fsid_t;
typedef long __clock_t;
typedef unsigned long __rlim_t;
typedef unsigned long __rlim64_t;
typedef unsigned int __id_t;
typedef long __time_t;
typedef unsigned int __useconds_t;
typedef long __suseconds_t;
typedef int __daddr_t;
typedef int __key_t;
typedef int __clockid_t;
typedef void *__timer_t;
typedef long __blksize_t;
typedef long __blkcnt_t;
typedef long __blkcnt64_t;
typedef unsigned long __fsblkcnt_t;
typedef unsigned long __fsblkcnt64_t;
typedef unsigned long __fsfilcnt_t;
typedef unsigned long __fsfilcnt64_t;
typedef long __fsword_t;
typedef long __ssize_t;
typedef long __syscall_slong_t;
typedef unsigned long __syscall_ulong_t;
typedef __off64_t __loff_t;
typedef char *__caddr_t;
typedef long __intptr_t;
typedef unsigned int __socklen_t;
typedef int __sig_atomic_t;
typedef __int8_t int8_t;
typedef __int16_t int16_t;
typedef __int32_t int32_t;
typedef __int64_t int64_t;
typedef __uint8_t uint8_t;
typedef __uint16_t uint16_t;
typedef __uint32_t uint32_t;
typedef __uint64_t uint64_t;
typedef __int_least8_t int_least8_t;
typedef __int_least16_t int_least16_t;
typedef __int_least32_t int_least32_t;
typedef __int_least64_t int_least64_t;
typedef __uint_least8_t uint_least8_t;
typedef __uint_least16_t uint_least16_t;
typedef __uint_least32_t uint_least32_t;
typedef __uint_least64_t uint_least64_t;
typedef signed char int_fast8_t;
typedef long int_fast16_t;
typedef long int_fast32_t;
typedef long int_fast64_t;
typedef unsigned char uint_fast8_t;
typedef unsigned long uint_fast16_t;
typedef unsigned long uint_fast32_t;
typedef unsigned long uint_fast64_t;
typedef long intptr_t;
typedef unsigned long uintptr_t;
typedef __intmax_t intmax_t;
typedef __uintmax_t uintmax_t;
struct bb_search_cur_bkref_entry_ret_type;
struct bb_search_cur_bkref_entry_ret_type {
    uint64_t field_0;
    uint64_t field_1;
    uint64_t field_2;
    uint64_t field_3;
};
extern uint64_t revng_init_local_sp(uint64_t param_0);
extern uint64_t init_r10(void);
extern uint64_t init_r9(void);
typedef _Bool bool;
struct bb_search_cur_bkref_entry_ret_type bb_search_cur_bkref_entry(uint64_t rdi, uint64_t rsi) {
    uint64_t rcx_0;
    uint64_t var_0;
    uint64_t var_1;
    uint64_t var_2;
    uint64_t *var_3;
    uint64_t rcx_1;
    uint64_t r10_0;
    uint64_t rax_0;
    uint64_t r9_0;
    uint64_t r9_2;
    uint64_t r8_0;
    uint64_t r10_1;
    uint64_t var_4;
    uint64_t var_5;
    uint64_t var_6;
    struct bb_search_cur_bkref_entry_ret_type mrv2;
    struct bb_search_cur_bkref_entry_ret_type mrv3;
    uint64_t r8_1;
    uint64_t var_7;
    uint64_t var_8;
    uint64_t r9_3;
    uint64_t r8_2;
    struct bb_search_cur_bkref_entry_ret_type mrv;
    struct bb_search_cur_bkref_entry_ret_type mrv1;
    struct bb_search_cur_bkref_entry_ret_type mrv5 = {18446744073709551615UL, /*implicit*/(int)0, /*implicit*/(int)0, /*implicit*/(int)0};
    struct bb_search_cur_bkref_entry_ret_type mrv6;
    struct bb_search_cur_bkref_entry_ret_type mrv7;
    unsigned int loop_state_var;
    bool switch_state_var;
    revng_init_local_sp(0UL);
    var_0 = init_r10();
    var_1 = init_r9();
    var_2 = *(uint64_t *)(rdi + 200UL);
    var_3 = (uint64_t *)(rdi + 216UL);
    r10_0 = var_0;
    rax_0 = 0UL;
    r9_0 = var_1;
    r8_0 = var_2;
    rcx_1 = r8_0;
    r10_1 = r10_0;
    r8_1 = r8_0;
    r9_3 = r9_0;
    r8_2 = r8_0;
    while ((long)rax_0 >= (long)r8_0)
        {
            var_4 = r8_0 + rax_0;
            var_5 = (uint64_t)((long)((var_4 >> 63UL) + var_4) >> (long)1UL);
            var_6 = *var_3;
            r10_0 = var_6;
            rcx_0 = var_5;
            r9_2 = var_5;
            r10_1 = var_6;
            if ((long)*(uint64_t *)(((var_5 * 40UL) + var_6) + 8UL) < (long)rsi) {
                rax_0 = r9_2 + 1UL;
                r9_0 = r9_2;
                r8_0 = rcx_1;
                rcx_1 = r8_0;
                r10_1 = r10_0;
                r8_1 = r8_0;
                r9_3 = r9_0;
                r8_2 = r8_0;
                continue;
            }
            while (1U)
                {
                    rcx_1 = rcx_0;
                    r9_3 = rcx_0;
                    r8_2 = r8_1;
                    if ((long)rax_0 >= (long)rcx_0) {
                        loop_state_var = 1U;
                        break;
                    }
                    var_7 = rcx_0 + rax_0;
                    var_8 = (uint64_t)((long)(var_7 + (var_7 >> 63UL)) >> (long)1UL);
                    rcx_0 = var_8;
                    r9_2 = var_8;
                    if ((long)*(uint64_t *)(((var_8 * 40UL) + var_6) + 8UL) >= (long)rsi) {
                        loop_state_var = 0U;
                        break;
                    }
                    r8_1 = var_8 * 5UL;
                    continue;
                }
            switch_state_var = 0;
            switch (loop_state_var) {
              case 1U:
                {
                    switch_state_var = 1;
                    break;
                }
                break;
              case 0U:
                {
                    break;
                }
                break;
            }
            if (switch_state_var)
                break;
        }
    if ((long)var_2 <= (long)rax_0) {
        mrv5.field_1 = r10_1;
        mrv6 = mrv5;
        mrv6.field_2 = r9_3;
        mrv7 = mrv6;
        mrv7.field_3 = r8_2;
        return mrv7;
    }
    mrv.field_0 = (*(uint64_t *)(((rax_0 * 40UL) + *var_3) + 8UL) == rsi) ? rax_0 : 18446744073709551615UL;
    mrv1 = mrv;
    mrv1.field_1 = r10_1;
    mrv2 = mrv1;
    mrv2.field_2 = r9_3;
    mrv3 = mrv2;
    mrv3.field_3 = r8_2;
    return mrv3;
    mrv.field_0 = (*(uint64_t *)(((rax_0 * 40UL) + *var_3) + 8UL) == rsi) ? rax_0 : 18446744073709551615UL;
    mrv1 = mrv;
    mrv1.field_1 = r10_1;
    mrv2 = mrv1;
    mrv2.field_2 = r9_3;
    mrv3 = mrv2;
    mrv3.field_3 = r8_2;
    return mrv3;
}

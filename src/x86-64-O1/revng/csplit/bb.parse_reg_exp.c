typedef unsigned char __u_char;
typedef unsigned short __u_short;
typedef unsigned int __u_int;
typedef unsigned long __u_long;
typedef signed char __int8_t;
typedef unsigned char __uint8_t;
typedef short __int16_t;
typedef unsigned short __uint16_t;
typedef int __int32_t;
typedef unsigned int __uint32_t;
typedef long __int64_t;
typedef unsigned long __uint64_t;
typedef __int8_t __int_least8_t;
typedef __uint8_t __uint_least8_t;
typedef __int16_t __int_least16_t;
typedef __uint16_t __uint_least16_t;
typedef __int32_t __int_least32_t;
typedef __uint32_t __uint_least32_t;
typedef __int64_t __int_least64_t;
typedef __uint64_t __uint_least64_t;
typedef long __quad_t;
typedef unsigned long __u_quad_t;
typedef long __intmax_t;
typedef unsigned long __uintmax_t;
typedef unsigned long __dev_t;
typedef unsigned int __uid_t;
typedef unsigned int __gid_t;
typedef unsigned long __ino_t;
typedef unsigned long __ino64_t;
typedef unsigned int __mode_t;
typedef unsigned long __nlink_t;
typedef long __off_t;
typedef long __off64_t;
typedef int __pid_t;
typedef struct {
    int __val[2];
} __fsid_t;
typedef long __clock_t;
typedef unsigned long __rlim_t;
typedef unsigned long __rlim64_t;
typedef unsigned int __id_t;
typedef long __time_t;
typedef unsigned int __useconds_t;
typedef long __suseconds_t;
typedef int __daddr_t;
typedef int __key_t;
typedef int __clockid_t;
typedef void *__timer_t;
typedef long __blksize_t;
typedef long __blkcnt_t;
typedef long __blkcnt64_t;
typedef unsigned long __fsblkcnt_t;
typedef unsigned long __fsblkcnt64_t;
typedef unsigned long __fsfilcnt_t;
typedef unsigned long __fsfilcnt64_t;
typedef long __fsword_t;
typedef long __ssize_t;
typedef long __syscall_slong_t;
typedef unsigned long __syscall_ulong_t;
typedef __off64_t __loff_t;
typedef char *__caddr_t;
typedef long __intptr_t;
typedef unsigned int __socklen_t;
typedef int __sig_atomic_t;
typedef __int8_t int8_t;
typedef __int16_t int16_t;
typedef __int32_t int32_t;
typedef __int64_t int64_t;
typedef __uint8_t uint8_t;
typedef __uint16_t uint16_t;
typedef __uint32_t uint32_t;
typedef __uint64_t uint64_t;
typedef __int_least8_t int_least8_t;
typedef __int_least16_t int_least16_t;
typedef __int_least32_t int_least32_t;
typedef __int_least64_t int_least64_t;
typedef __uint_least8_t uint_least8_t;
typedef __uint_least16_t uint_least16_t;
typedef __uint_least32_t uint_least32_t;
typedef __uint_least64_t uint_least64_t;
typedef signed char int_fast8_t;
typedef long int_fast16_t;
typedef long int_fast32_t;
typedef long int_fast64_t;
typedef unsigned char uint_fast8_t;
typedef unsigned long uint_fast16_t;
typedef unsigned long uint_fast32_t;
typedef unsigned long uint_fast64_t;
typedef long intptr_t;
typedef unsigned long uintptr_t;
typedef __intmax_t intmax_t;
typedef __uintmax_t uintmax_t;
struct indirect_placeholder_231_ret_type;
struct indirect_placeholder_231_ret_type {
    uint64_t field_0;
    uint64_t field_1;
    uint64_t field_2;
    uint64_t field_3;
};
extern uint64_t revng_init_local_sp(uint64_t param_0);
extern uint64_t init_rbp(void);
extern uint64_t init_rbx(void);
extern uint64_t init_r12(void);
extern uint64_t init_r14(void);
extern uint64_t init_r13(void);
extern uint64_t init_r15(void);
extern uint64_t indirect_placeholder_10(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint64_t param_3, uint64_t param_4, uint64_t param_5);
extern uint64_t indirect_placeholder_8(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint64_t param_3);
extern struct indirect_placeholder_231_ret_type indirect_placeholder_231(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint64_t param_3, uint64_t param_4, uint64_t param_5, uint64_t param_6);
uint64_t bb_parse_reg_exp(uint64_t rdx, uint64_t rdi, uint64_t rcx, uint64_t rsi, uint64_t r9, uint64_t r8) {
    struct indirect_placeholder_231_ret_type var_15;
    uint64_t var_0;
    uint64_t var_1;
    uint64_t var_2;
    uint64_t var_3;
    uint64_t var_4;
    uint64_t var_5;
    uint64_t var_6;
    uint64_t *var_7;
    uint64_t var_8;
    uint64_t var_9;
    uint64_t var_10;
    uint64_t rdx1_0;
    uint64_t rax_0;
    uint64_t rbp_0;
    uint64_t r14_0;
    uint64_t r13_0;
    uint64_t *var_21;
    uint64_t var_22;
    uint64_t var_23;
    uint64_t var_24;
    uint64_t var_25;
    uint64_t var_26;
    uint64_t var_27;
    uint64_t local_sp_0;
    uint64_t var_28;
    uint64_t var_29;
    uint64_t local_sp_1;
    unsigned char *var_11;
    uint64_t r12_0;
    uint64_t *var_12;
    uint64_t var_13;
    uint64_t var_14;
    uint64_t var_16;
    uint64_t var_17;
    uint64_t var_18;
    uint64_t var_19;
    unsigned char var_20;
    var_0 = revng_init_local_sp(0UL);
    var_1 = init_rbp();
    var_2 = init_rbx();
    var_3 = init_r14();
    var_4 = init_r13();
    var_5 = init_r12();
    var_6 = init_r15();
    *(uint64_t *)(var_0 + (-8L)) = var_6;
    *(uint64_t *)(var_0 + (-16L)) = var_3;
    *(uint64_t *)(var_0 + (-24L)) = var_4;
    *(uint64_t *)(var_0 + (-32L)) = var_5;
    *(uint64_t *)(var_0 + (-40L)) = var_1;
    *(uint64_t *)(var_0 + (-48L)) = var_2;
    *(uint64_t *)(var_0 + (-64L)) = rsi;
    *(uint64_t *)(var_0 + (-80L)) = rcx;
    var_7 = (uint64_t *)(var_0 + (-88L));
    *var_7 = r9;
    var_8 = *(uint64_t *)rsi;
    *(uint64_t *)(var_0 + (-72L)) = *(uint64_t *)(var_8 + 168UL);
    var_9 = var_0 + (-112L);
    *(uint64_t *)var_9 = 4270104UL;
    var_10 = indirect_placeholder_10(rdx, rdi, rcx, rsi, r9, r8);
    rdx1_0 = 0UL;
    rax_0 = 0UL;
    rbp_0 = var_8;
    r14_0 = rdi;
    r13_0 = r8;
    local_sp_1 = var_9;
    r12_0 = var_10;
    if (var_10 != 0UL) {
        if (*(uint32_t *)r9 == 0U) {
            return rax_0;
        }
    }
    *(uint64_t *)(var_0 + (-104L)) = (*var_7 | 8388608UL);
    var_11 = (unsigned char *)(rdx + 8UL);
    rax_0 = r12_0;
    while (*var_11 != '\n')
        {
            var_12 = (uint64_t *)(local_sp_1 + 8UL);
            var_13 = *var_12;
            var_14 = local_sp_1 + (-8L);
            *(uint64_t *)var_14 = 4270159UL;
            var_15 = indirect_placeholder_231(rbp_0, var_13, r14_0, rdx, r13_0, r12_0, r14_0);
            var_16 = var_15.field_0;
            var_17 = var_15.field_1;
            var_18 = var_15.field_2;
            var_19 = var_15.field_3;
            var_20 = *var_11;
            rax_0 = 0UL;
            rbp_0 = var_16;
            r14_0 = var_17;
            r13_0 = var_18;
            local_sp_0 = var_14;
            if ((uint64_t)((var_20 & '\xf7') + '\xfe') == 0UL) {
                var_28 = local_sp_0 + (-8L);
                *(uint64_t *)var_28 = 4270315UL;
                var_29 = indirect_placeholder_8(rdx1_0, var_16, 10UL, var_19);
                local_sp_1 = var_28;
                r12_0 = var_29;
                if (var_29 != 0UL) {
                    **(uint32_t **)(local_sp_0 + 8UL) = 12U;
                    break;
                }
                rax_0 = r12_0;
                continue;
            }
            if (var_18 != 0UL) {
                if ((uint64_t)(var_20 + '\xf7') != 0UL) {
                    var_28 = local_sp_0 + (-8L);
                    *(uint64_t *)var_28 = 4270315UL;
                    var_29 = indirect_placeholder_8(rdx1_0, var_16, 10UL, var_19);
                    local_sp_1 = var_28;
                    r12_0 = var_29;
                    if (var_29 != 0UL) {
                        **(uint32_t **)(local_sp_0 + 8UL) = 12U;
                        break;
                    }
                    rax_0 = r12_0;
                    continue;
                }
            }
            var_21 = (uint64_t *)(var_16 + 168UL);
            var_22 = *var_21;
            *var_21 = *(uint64_t *)(local_sp_1 + 24UL);
            var_23 = *var_12;
            var_24 = *(uint64_t *)(local_sp_1 + 16UL);
            var_25 = *(uint64_t *)(local_sp_1 + 32UL);
            var_26 = local_sp_1 + (-16L);
            *(uint64_t *)var_26 = 4270230UL;
            var_27 = indirect_placeholder_10(rdx, var_17, var_24, var_25, var_23, var_18);
            local_sp_0 = var_26;
            rdx1_0 = var_27;
            if (var_27 != 0UL) {
                if (**(uint32_t **)local_sp_1 != 0U) {
                    if (var_19 == 0UL) {
                        break;
                    }
                    *(uint64_t *)(local_sp_1 + (-24L)) = 4270271UL;
                    indirect_placeholder_8(0UL, 0UL, var_19, 4234558UL);
                    break;
                }
            }
            *var_21 = (*var_21 | var_22);
        }
    return rax_0;
}

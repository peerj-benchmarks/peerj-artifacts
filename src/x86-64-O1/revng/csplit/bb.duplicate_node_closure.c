typedef unsigned char __u_char;
typedef unsigned short __u_short;
typedef unsigned int __u_int;
typedef unsigned long __u_long;
typedef signed char __int8_t;
typedef unsigned char __uint8_t;
typedef short __int16_t;
typedef unsigned short __uint16_t;
typedef int __int32_t;
typedef unsigned int __uint32_t;
typedef long __int64_t;
typedef unsigned long __uint64_t;
typedef __int8_t __int_least8_t;
typedef __uint8_t __uint_least8_t;
typedef __int16_t __int_least16_t;
typedef __uint16_t __uint_least16_t;
typedef __int32_t __int_least32_t;
typedef __uint32_t __uint_least32_t;
typedef __int64_t __int_least64_t;
typedef __uint64_t __uint_least64_t;
typedef long __quad_t;
typedef unsigned long __u_quad_t;
typedef long __intmax_t;
typedef unsigned long __uintmax_t;
typedef unsigned long __dev_t;
typedef unsigned int __uid_t;
typedef unsigned int __gid_t;
typedef unsigned long __ino_t;
typedef unsigned long __ino64_t;
typedef unsigned int __mode_t;
typedef unsigned long __nlink_t;
typedef long __off_t;
typedef long __off64_t;
typedef int __pid_t;
typedef struct {
    int __val[2];
} __fsid_t;
typedef long __clock_t;
typedef unsigned long __rlim_t;
typedef unsigned long __rlim64_t;
typedef unsigned int __id_t;
typedef long __time_t;
typedef unsigned int __useconds_t;
typedef long __suseconds_t;
typedef int __daddr_t;
typedef int __key_t;
typedef int __clockid_t;
typedef void *__timer_t;
typedef long __blksize_t;
typedef long __blkcnt_t;
typedef long __blkcnt64_t;
typedef unsigned long __fsblkcnt_t;
typedef unsigned long __fsblkcnt64_t;
typedef unsigned long __fsfilcnt_t;
typedef unsigned long __fsfilcnt64_t;
typedef long __fsword_t;
typedef long __ssize_t;
typedef long __syscall_slong_t;
typedef unsigned long __syscall_ulong_t;
typedef __off64_t __loff_t;
typedef char *__caddr_t;
typedef long __intptr_t;
typedef unsigned int __socklen_t;
typedef int __sig_atomic_t;
typedef __int8_t int8_t;
typedef __int16_t int16_t;
typedef __int32_t int32_t;
typedef __int64_t int64_t;
typedef __uint8_t uint8_t;
typedef __uint16_t uint16_t;
typedef __uint32_t uint32_t;
typedef __uint64_t uint64_t;
typedef __int_least8_t int_least8_t;
typedef __int_least16_t int_least16_t;
typedef __int_least32_t int_least32_t;
typedef __int_least64_t int_least64_t;
typedef __uint_least8_t uint_least8_t;
typedef __uint_least16_t uint_least16_t;
typedef __uint_least32_t uint_least32_t;
typedef __uint_least64_t uint_least64_t;
typedef signed char int_fast8_t;
typedef long int_fast16_t;
typedef long int_fast32_t;
typedef long int_fast64_t;
typedef unsigned char uint_fast8_t;
typedef unsigned long uint_fast16_t;
typedef unsigned long uint_fast32_t;
typedef unsigned long uint_fast64_t;
typedef long intptr_t;
typedef unsigned long uintptr_t;
typedef __intmax_t intmax_t;
typedef __uintmax_t uintmax_t;
extern uint64_t revng_init_local_sp(uint64_t param_0);
extern uint64_t init_rbp(void);
extern uint64_t init_rbx(void);
extern uint64_t init_r12(void);
extern uint64_t init_cc_src2(void);
extern uint64_t helper_cc_compute_c_wrapper(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint32_t param_3);
extern uint64_t init_r14(void);
extern uint64_t init_r13(void);
extern uint64_t helper_cc_compute_all_wrapper(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint32_t param_3);
extern uint64_t indirect_placeholder_6(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint64_t param_3, uint64_t param_4);
extern uint64_t init_r15(void);
extern uint64_t indirect_placeholder_5(uint64_t param_0, uint64_t param_1);
extern uint64_t indirect_placeholder_3(uint64_t param_0, uint64_t param_1, uint64_t param_2);
typedef _Bool bool;
uint64_t bb_duplicate_node_closure(uint64_t rdx, uint64_t rdi, uint64_t rcx, uint64_t rsi, uint64_t r8) {
    uint64_t var_52;
    uint64_t var_0;
    uint64_t var_1;
    uint64_t var_2;
    uint64_t var_3;
    uint64_t var_4;
    uint64_t var_5;
    uint64_t var_6;
    uint64_t var_7;
    uint64_t var_8;
    uint64_t var_9;
    uint64_t *var_10;
    uint64_t *var_11;
    uint64_t *var_12;
    uint64_t *var_13;
    uint64_t *var_14;
    uint64_t local_sp_1_ph;
    uint64_t rbx_0_ph;
    uint64_t r14_0_ph;
    uint64_t r12_0_ph;
    uint32_t var_15;
    uint64_t var_16;
    uint64_t local_sp_1;
    uint64_t local_sp_1_be;
    uint64_t rbx_0_be;
    uint64_t r12_0_be;
    uint64_t var_55;
    uint64_t var_56;
    uint64_t var_57;
    uint64_t rbx_0;
    uint64_t local_sp_0;
    uint64_t var_53;
    uint64_t var_54;
    uint64_t var_50;
    uint64_t var_51;
    uint64_t var_47;
    uint64_t *var_48;
    uint64_t var_49;
    uint64_t var_28;
    uint64_t var_29;
    uint64_t var_30;
    uint64_t var_63;
    uint64_t var_64;
    uint64_t var_65;
    uint64_t var_66;
    uint64_t r12_0;
    uint64_t var_17;
    uint64_t var_58;
    uint64_t var_59;
    uint64_t var_60;
    uint64_t var_61;
    uint64_t var_62;
    uint64_t var_18;
    uint64_t var_19;
    uint64_t var_20;
    uint64_t var_31;
    uint64_t rax_0;
    uint64_t var_21;
    uint64_t var_22;
    uint64_t var_23;
    uint64_t var_24;
    uint64_t var_25;
    uint64_t var_26;
    uint64_t var_27;
    uint64_t var_32;
    uint64_t var_33;
    uint64_t var_34;
    uint64_t var_35;
    uint64_t var_36;
    uint64_t var_37;
    uint64_t var_38;
    uint64_t var_39;
    uint64_t rax_1;
    uint64_t rsi4_0;
    uint64_t var_43;
    uint64_t var_44;
    uint64_t var_45;
    uint64_t var_40;
    uint64_t var_41;
    uint64_t var_42;
    uint64_t var_46;
    unsigned int loop_state_var;
    bool switch_state_var;
    var_0 = revng_init_local_sp(0UL);
    var_1 = init_cc_src2();
    var_2 = init_rbp();
    var_3 = init_rbx();
    var_4 = init_r14();
    var_5 = init_r13();
    var_6 = init_r12();
    var_7 = init_r15();
    *(uint64_t *)(var_0 + (-8L)) = var_7;
    *(uint64_t *)(var_0 + (-16L)) = var_4;
    *(uint64_t *)(var_0 + (-24L)) = var_5;
    *(uint64_t *)(var_0 + (-32L)) = var_6;
    *(uint64_t *)(var_0 + (-40L)) = var_2;
    *(uint64_t *)(var_0 + (-48L)) = var_3;
    var_8 = var_0 + (-72L);
    *(uint64_t *)var_8 = rcx;
    var_9 = (uint64_t)(uint32_t)r8;
    var_10 = (uint64_t *)rdi;
    var_11 = (uint64_t *)(rdi + 40UL);
    var_12 = (uint64_t *)(rdi + 16UL);
    var_13 = (uint64_t *)(rdi + 32UL);
    var_14 = (uint64_t *)(rdi + 24UL);
    local_sp_1_ph = var_8;
    rbx_0_ph = rsi;
    r14_0_ph = var_9;
    r12_0_ph = rdx;
    rax_0 = 12UL;
    while (1U)
        {
            var_15 = (uint32_t)r14_0_ph;
            var_16 = (uint64_t)var_15;
            local_sp_1 = local_sp_1_ph;
            rbx_0 = rbx_0_ph;
            r12_0 = r12_0_ph;
            while (1U)
                {
                    var_17 = rbx_0 << 4UL;
                    if (*(unsigned char *)((var_17 + *var_10) + 8UL) != '\x04') {
                        var_58 = *var_14;
                        var_59 = rbx_0 << 3UL;
                        var_60 = *(uint64_t *)(var_59 + var_58);
                        *(uint64_t *)(local_sp_1 + 8UL) = r12_0;
                        var_61 = r12_0 * 24UL;
                        *(uint64_t *)((var_61 + *var_11) + 8UL) = 0UL;
                        *(uint64_t *)(local_sp_1 + (-8L)) = 4235474UL;
                        var_62 = indirect_placeholder_3(var_16, rdi, var_60);
                        rbx_0_be = var_60;
                        r12_0_be = var_62;
                        if (var_62 != 18446744073709551615UL) {
                            loop_state_var = 1U;
                            break;
                        }
                        var_63 = *var_14;
                        *(uint64_t *)((*(uint64_t *)local_sp_1 << 3UL) + var_63) = *(uint64_t *)(var_59 + var_63);
                        var_64 = var_61 + *var_11;
                        var_65 = local_sp_1 + (-16L);
                        *(uint64_t *)var_65 = 4235519UL;
                        var_66 = indirect_placeholder_5(var_64, var_62);
                        local_sp_1_be = var_65;
                        if ((uint64_t)(unsigned char)var_66 != 0UL) {
                            loop_state_var = 1U;
                            break;
                        }
                        local_sp_1 = local_sp_1_be;
                        rbx_0 = rbx_0_be;
                        r12_0 = r12_0_be;
                        continue;
                    }
                    var_18 = *var_11;
                    var_19 = rbx_0 * 24UL;
                    var_20 = var_19 + var_18;
                    rax_0 = 0UL;
                    switch_state_var = 0;
                    switch (*(uint64_t *)(var_20 + 8UL)) {
                      case 0UL:
                        {
                            var_31 = *var_14;
                            *(uint64_t *)((r12_0 << 3UL) + var_31) = *(uint64_t *)((rbx_0 << 3UL) + var_31);
                            loop_state_var = 1U;
                            switch_state_var = 1;
                            break;
                        }
                        break;
                      case 1UL:
                        {
                            loop_state_var = 0U;
                            switch_state_var = 1;
                            break;
                        }
                        break;
                      default:
                        {
                            var_32 = **(uint64_t **)(var_20 + 16UL);
                            var_33 = r12_0 * 24UL;
                            *(uint64_t *)((var_33 + var_18) + 8UL) = 0UL;
                            var_34 = *var_12;
                            var_35 = var_34 + (-1L);
                            var_36 = *var_10;
                            var_37 = (var_35 << 4UL) + var_36;
                            var_38 = helper_cc_compute_all_wrapper(var_35, 0UL, 0UL, 25U);
                            rax_1 = var_37;
                            rsi4_0 = var_35;
                            if ((uint64_t)(((unsigned char)(var_38 >> 4UL) ^ (unsigned char)var_38) & '\xc0') != 0UL) {
                                *(uint64_t *)(local_sp_1 + (-8L)) = 4235872UL;
                                var_46 = indirect_placeholder_3(var_16, rdi, var_32);
                                if (var_46 == 18446744073709551615UL) {
                                    loop_state_var = 1U;
                                    switch_state_var = 1;
                                    break;
                                }
                                var_47 = var_33 + *var_11;
                                var_48 = (uint64_t *)(local_sp_1 + (-16L));
                                *var_48 = 4235900UL;
                                var_49 = indirect_placeholder_5(var_47, var_46);
                                if ((uint64_t)(unsigned char)var_49 == 0UL) {
                                    loop_state_var = 1U;
                                    switch_state_var = 1;
                                    break;
                                }
                                var_50 = *var_48;
                                var_51 = local_sp_1 + (-24L);
                                *(uint64_t *)var_51 = 4235929UL;
                                var_52 = indirect_placeholder_6(var_46, rdi, var_50, var_32, var_16);
                                local_sp_0 = var_51;
                                rax_0 = var_52;
                                if ((uint64_t)(uint32_t)var_52 != 0UL) {
                                    loop_state_var = 1U;
                                    switch_state_var = 1;
                                    break;
                                }
                                var_53 = *(uint64_t *)(*(uint64_t *)((var_19 + *var_11) + 16UL) + 8UL);
                                *(uint64_t *)(local_sp_0 + (-8L)) = 4235978UL;
                                var_54 = indirect_placeholder_3(var_16, rdi, var_53);
                                rbx_0_be = var_53;
                                r12_0_be = var_54;
                                if (var_54 != 18446744073709551615UL) {
                                    loop_state_var = 1U;
                                    switch_state_var = 1;
                                    break;
                                }
                                var_55 = var_33 + *var_11;
                                var_56 = local_sp_0 + (-16L);
                                *(uint64_t *)var_56 = 4236002UL;
                                var_57 = indirect_placeholder_5(var_55, var_54);
                                local_sp_1_be = var_56;
                                if ((uint64_t)(unsigned char)var_57 != 0UL) {
                                    loop_state_var = 1U;
                                    switch_state_var = 1;
                                    break;
                                }
                                local_sp_1 = local_sp_1_be;
                                rbx_0 = rbx_0_be;
                                r12_0 = r12_0_be;
                                continue;
                            }
                            if ((*(unsigned char *)(var_37 + 10UL) & '\x04') != '\x00') {
                                *(uint64_t *)(local_sp_1 + (-8L)) = 4235872UL;
                                var_46 = indirect_placeholder_3(var_16, rdi, var_32);
                                if (var_46 == 18446744073709551615UL) {
                                    loop_state_var = 1U;
                                    switch_state_var = 1;
                                    break;
                                }
                                var_47 = var_33 + *var_11;
                                var_48 = (uint64_t *)(local_sp_1 + (-16L));
                                *var_48 = 4235900UL;
                                var_49 = indirect_placeholder_5(var_47, var_46);
                                if ((uint64_t)(unsigned char)var_49 == 0UL) {
                                    loop_state_var = 1U;
                                    switch_state_var = 1;
                                    break;
                                }
                                var_50 = *var_48;
                                var_51 = local_sp_1 + (-24L);
                                *(uint64_t *)var_51 = 4235929UL;
                                var_52 = indirect_placeholder_6(var_46, rdi, var_50, var_32, var_16);
                                local_sp_0 = var_51;
                                rax_0 = var_52;
                                if ((uint64_t)(uint32_t)var_52 != 0UL) {
                                    loop_state_var = 1U;
                                    switch_state_var = 1;
                                    break;
                                }
                            }
                            var_39 = ((var_34 << 4UL) - var_37) + (-32L);
                            while (1U)
                                {
                                    if (var_32 != *(uint64_t *)((rsi4_0 << 3UL) + *var_13)) {
                                        if ((uint64_t)(var_15 - (uint32_t)((uint16_t)(*(uint32_t *)(rax_1 + 8UL) >> 8U) & (unsigned short)1023U)) != 0UL) {
                                            loop_state_var = 1U;
                                            break;
                                        }
                                    }
                                    var_40 = rsi4_0 + (-1L);
                                    var_41 = (rax_1 + var_39) + var_36;
                                    var_42 = helper_cc_compute_all_wrapper(var_40, 0UL, 0UL, 25U);
                                    rax_1 = var_41;
                                    rsi4_0 = var_40;
                                    if ((uint64_t)(((unsigned char)(var_42 >> 4UL) ^ (unsigned char)var_42) & '\xc0') != 0UL) {
                                        loop_state_var = 0U;
                                        break;
                                    }
                                    if ((*(unsigned char *)(var_41 + 10UL) & '\x04') == '\x00') {
                                        continue;
                                    }
                                    loop_state_var = 0U;
                                    break;
                                }
                            switch_state_var = 0;
                            switch (loop_state_var) {
                              case 1U:
                                {
                                    if (rsi4_0 != 18446744073709551615UL) {
                                        var_43 = var_33 + *var_11;
                                        var_44 = local_sp_1 + (-8L);
                                        *(uint64_t *)var_44 = 4235947UL;
                                        var_45 = indirect_placeholder_5(var_43, rsi4_0);
                                        local_sp_0 = var_44;
                                        if ((uint64_t)(unsigned char)var_45 != 0UL) {
                                            loop_state_var = 1U;
                                            switch_state_var = 1;
                                            break;
                                        }
                                    }
                                    *(uint64_t *)(local_sp_1 + (-8L)) = 4235872UL;
                                    var_46 = indirect_placeholder_3(var_16, rdi, var_32);
                                    if (var_46 != 18446744073709551615UL) {
                                        loop_state_var = 1U;
                                        switch_state_var = 1;
                                        break;
                                    }
                                    var_47 = var_33 + *var_11;
                                    var_48 = (uint64_t *)(local_sp_1 + (-16L));
                                    *var_48 = 4235900UL;
                                    var_49 = indirect_placeholder_5(var_47, var_46);
                                    if ((uint64_t)(unsigned char)var_49 != 0UL) {
                                        loop_state_var = 1U;
                                        switch_state_var = 1;
                                        break;
                                    }
                                    var_50 = *var_48;
                                    var_51 = local_sp_1 + (-24L);
                                    *(uint64_t *)var_51 = 4235929UL;
                                    var_52 = indirect_placeholder_6(var_46, rdi, var_50, var_32, var_16);
                                    local_sp_0 = var_51;
                                    rax_0 = var_52;
                                    if ((uint64_t)(uint32_t)var_52 != 0UL) {
                                        loop_state_var = 1U;
                                        switch_state_var = 1;
                                        break;
                                    }
                                }
                                break;
                              case 0U:
                                {
                                    *(uint64_t *)(local_sp_1 + (-8L)) = 4235872UL;
                                    var_46 = indirect_placeholder_3(var_16, rdi, var_32);
                                    if (var_46 == 18446744073709551615UL) {
                                        loop_state_var = 1U;
                                        switch_state_var = 1;
                                        break;
                                    }
                                    var_47 = var_33 + *var_11;
                                    var_48 = (uint64_t *)(local_sp_1 + (-16L));
                                    *var_48 = 4235900UL;
                                    var_49 = indirect_placeholder_5(var_47, var_46);
                                    if ((uint64_t)(unsigned char)var_49 == 0UL) {
                                        loop_state_var = 1U;
                                        switch_state_var = 1;
                                        break;
                                    }
                                    var_50 = *var_48;
                                    var_51 = local_sp_1 + (-24L);
                                    *(uint64_t *)var_51 = 4235929UL;
                                    var_52 = indirect_placeholder_6(var_46, rdi, var_50, var_32, var_16);
                                    local_sp_0 = var_51;
                                    rax_0 = var_52;
                                    if ((uint64_t)(uint32_t)var_52 != 0UL) {
                                        loop_state_var = 1U;
                                        switch_state_var = 1;
                                        break;
                                    }
                                }
                                break;
                            }
                            if (switch_state_var)
                                break;
                        }
                        break;
                    }
                    if (switch_state_var)
                        break;
                }
            switch_state_var = 0;
            switch (loop_state_var) {
              case 1U:
                {
                    switch_state_var = 1;
                    break;
                }
                break;
              case 0U:
                {
                    var_21 = **(uint64_t **)(var_20 + 16UL);
                    var_22 = r12_0 * 24UL;
                    *(uint64_t *)((var_22 + var_18) + 8UL) = 0UL;
                    rbx_0_ph = var_21;
                    if (r12_0 == rbx_0) {
                        var_26 = var_16 | (uint64_t)(uint32_t)((uint16_t)(*(uint32_t *)((var_17 + *var_10) + 8UL) >> 8U) & (unsigned short)1023U);
                        *(uint64_t *)(local_sp_1 + (-8L)) = 4235685UL;
                        var_27 = indirect_placeholder_3(var_26, rdi, var_21);
                        r14_0_ph = var_26;
                        r12_0_ph = var_27;
                        if (var_27 == 18446744073709551615UL) {
                            switch_state_var = 1;
                            break;
                        }
                        var_28 = var_22 + *var_11;
                        var_29 = local_sp_1 + (-16L);
                        *(uint64_t *)var_29 = 4235713UL;
                        var_30 = indirect_placeholder_5(var_28, var_27);
                        local_sp_1_ph = var_29;
                        if ((uint64_t)(unsigned char)var_30 != 0UL) {
                            continue;
                        }
                        switch_state_var = 1;
                        break;
                    }
                    if (rbx_0 != *(uint64_t *)local_sp_1) {
                        var_23 = var_22 + *var_11;
                        *(uint64_t *)(local_sp_1 + (-8L)) = 4235640UL;
                        var_24 = indirect_placeholder_5(var_23, var_21);
                        var_25 = helper_cc_compute_c_wrapper(var_24 + (-1L), 1UL, var_1, 14U);
                        rax_0 = (0UL - var_25) & 12UL;
                        switch_state_var = 1;
                        break;
                    }
                }
                break;
            }
            if (switch_state_var)
                break;
        }
    return rax_0;
}

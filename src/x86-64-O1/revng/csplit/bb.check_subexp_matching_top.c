typedef unsigned char __u_char;
typedef unsigned short __u_short;
typedef unsigned int __u_int;
typedef unsigned long __u_long;
typedef signed char __int8_t;
typedef unsigned char __uint8_t;
typedef short __int16_t;
typedef unsigned short __uint16_t;
typedef int __int32_t;
typedef unsigned int __uint32_t;
typedef long __int64_t;
typedef unsigned long __uint64_t;
typedef __int8_t __int_least8_t;
typedef __uint8_t __uint_least8_t;
typedef __int16_t __int_least16_t;
typedef __uint16_t __uint_least16_t;
typedef __int32_t __int_least32_t;
typedef __uint32_t __uint_least32_t;
typedef __int64_t __int_least64_t;
typedef __uint64_t __uint_least64_t;
typedef long __quad_t;
typedef unsigned long __u_quad_t;
typedef long __intmax_t;
typedef unsigned long __uintmax_t;
typedef unsigned long __dev_t;
typedef unsigned int __uid_t;
typedef unsigned int __gid_t;
typedef unsigned long __ino_t;
typedef unsigned long __ino64_t;
typedef unsigned int __mode_t;
typedef unsigned long __nlink_t;
typedef long __off_t;
typedef long __off64_t;
typedef int __pid_t;
typedef struct {
    int __val[2];
} __fsid_t;
typedef long __clock_t;
typedef unsigned long __rlim_t;
typedef unsigned long __rlim64_t;
typedef unsigned int __id_t;
typedef long __time_t;
typedef unsigned int __useconds_t;
typedef long __suseconds_t;
typedef int __daddr_t;
typedef int __key_t;
typedef int __clockid_t;
typedef void *__timer_t;
typedef long __blksize_t;
typedef long __blkcnt_t;
typedef long __blkcnt64_t;
typedef unsigned long __fsblkcnt_t;
typedef unsigned long __fsblkcnt64_t;
typedef unsigned long __fsfilcnt_t;
typedef unsigned long __fsfilcnt64_t;
typedef long __fsword_t;
typedef long __ssize_t;
typedef long __syscall_slong_t;
typedef unsigned long __syscall_ulong_t;
typedef __off64_t __loff_t;
typedef char *__caddr_t;
typedef long __intptr_t;
typedef unsigned int __socklen_t;
typedef int __sig_atomic_t;
typedef __int8_t int8_t;
typedef __int16_t int16_t;
typedef __int32_t int32_t;
typedef __int64_t int64_t;
typedef __uint8_t uint8_t;
typedef __uint16_t uint16_t;
typedef __uint32_t uint32_t;
typedef __uint64_t uint64_t;
typedef __int_least8_t int_least8_t;
typedef __int_least16_t int_least16_t;
typedef __int_least32_t int_least32_t;
typedef __int_least64_t int_least64_t;
typedef __uint_least8_t uint_least8_t;
typedef __uint_least16_t uint_least16_t;
typedef __uint_least32_t uint_least32_t;
typedef __uint_least64_t uint_least64_t;
typedef signed char int_fast8_t;
typedef long int_fast16_t;
typedef long int_fast32_t;
typedef long int_fast64_t;
typedef unsigned char uint_fast8_t;
typedef unsigned long uint_fast16_t;
typedef unsigned long uint_fast32_t;
typedef unsigned long uint_fast64_t;
typedef long intptr_t;
typedef unsigned long uintptr_t;
typedef __intmax_t intmax_t;
typedef __uintmax_t uintmax_t;
struct bb_check_subexp_matching_top_ret_type;
struct indirect_placeholder_2_ret_type;
struct bb_check_subexp_matching_top_ret_type {
    uint64_t field_0;
    uint64_t field_1;
    uint64_t field_2;
    uint64_t field_3;
    uint64_t field_4;
};
struct indirect_placeholder_2_ret_type {
    uint64_t field_0;
    uint64_t field_1;
    uint64_t field_2;
    uint64_t field_3;
    uint64_t field_4;
    uint64_t field_5;
    uint64_t field_6;
};
extern uint64_t revng_init_local_sp(uint64_t param_0);
extern uint64_t init_rbp(void);
extern uint64_t init_rbx(void);
extern uint64_t init_r12(void);
extern uint64_t init_cc_src2(void);
extern uint64_t helper_cc_compute_c_wrapper(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint32_t param_3);
extern uint64_t init_r14(void);
extern uint64_t init_r13(void);
extern uint64_t helper_cc_compute_all_wrapper(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint32_t param_3);
extern uint64_t init_r15(void);
extern void indirect_placeholder_1(uint64_t param_0, uint64_t param_1);
extern struct indirect_placeholder_2_ret_type indirect_placeholder_2(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint64_t param_3, uint64_t param_4, uint64_t param_5, uint64_t param_6, uint64_t param_7);
struct bb_check_subexp_matching_top_ret_type bb_check_subexp_matching_top(uint64_t rdx, uint64_t rdi, uint64_t rcx, uint64_t r10, uint64_t rsi, uint64_t r9, uint64_t r8) {
    uint64_t var_0;
    uint64_t var_1;
    uint64_t var_2;
    uint64_t var_3;
    uint64_t var_4;
    uint64_t var_5;
    uint64_t var_6;
    uint64_t var_7;
    uint64_t var_8;
    uint64_t rax_0;
    uint64_t var_32;
    uint64_t var_33;
    uint64_t var_34;
    uint64_t var_35;
    uint64_t var_36;
    uint64_t var_37;
    uint64_t var_38;
    uint64_t *var_39;
    uint64_t var_40;
    uint64_t local_sp_2;
    uint64_t var_26;
    uint64_t var_27;
    uint64_t var_9;
    uint64_t *var_10;
    uint64_t *var_11;
    uint64_t *var_12;
    uint64_t *var_13;
    uint64_t *var_14;
    uint64_t rbp_1;
    uint64_t local_sp_0;
    uint64_t rbx_1;
    uint64_t rbp_0;
    uint64_t rbx_0;
    uint64_t r108_2;
    uint64_t rcx7_1;
    uint64_t r910_2;
    uint64_t r108_1;
    uint64_t r811_2;
    uint64_t r910_1;
    uint64_t rcx7_3;
    uint64_t r811_1;
    uint64_t var_15;
    uint64_t var_16;
    uint64_t var_17;
    uint64_t var_18;
    uint64_t var_19;
    uint64_t var_20;
    uint64_t var_21;
    uint64_t var_22;
    uint64_t var_23;
    uint64_t var_24;
    uint64_t var_25;
    uint64_t local_sp_1;
    uint64_t var_28;
    uint64_t var_29;
    struct indirect_placeholder_2_ret_type var_30;
    uint64_t var_31;
    uint64_t rcx7_2;
    uint64_t r108_3;
    uint64_t r910_3;
    uint64_t var_41;
    uint64_t r811_3;
    struct bb_check_subexp_matching_top_ret_type mrv;
    struct bb_check_subexp_matching_top_ret_type mrv1;
    struct bb_check_subexp_matching_top_ret_type mrv2;
    struct bb_check_subexp_matching_top_ret_type mrv3;
    struct bb_check_subexp_matching_top_ret_type mrv4;
    var_0 = revng_init_local_sp(0UL);
    var_1 = init_cc_src2();
    var_2 = init_rbp();
    var_3 = init_rbx();
    var_4 = init_r14();
    var_5 = init_r13();
    var_6 = init_r12();
    var_7 = init_r15();
    *(uint64_t *)(var_0 + (-8L)) = var_7;
    *(uint64_t *)(var_0 + (-16L)) = var_4;
    *(uint64_t *)(var_0 + (-24L)) = var_5;
    *(uint64_t *)(var_0 + (-32L)) = var_6;
    *(uint64_t *)(var_0 + (-40L)) = var_2;
    *(uint64_t *)(var_0 + (-48L)) = var_3;
    var_8 = *(uint64_t *)(rdi + 152UL);
    rax_0 = 0UL;
    rbp_0 = rsi;
    rbx_0 = 0UL;
    rcx7_1 = rcx;
    r108_1 = r10;
    r910_1 = r9;
    rcx7_3 = rcx;
    r811_1 = r8;
    r108_3 = r10;
    r910_3 = r9;
    r811_3 = r8;
    if ((long)*(uint64_t *)(rsi + 8UL) > (long)0UL) {
        mrv.field_0 = rax_0;
        mrv1 = mrv;
        mrv1.field_1 = rcx7_3;
        mrv2 = mrv1;
        mrv2.field_2 = r108_3;
        mrv3 = mrv2;
        mrv3.field_3 = r910_3;
        mrv4 = mrv3;
        mrv4.field_4 = r811_3;
        return mrv4;
    }
    var_9 = var_0 + (-72L);
    *(uint64_t *)(var_0 + (-64L)) = rdx;
    var_10 = (uint64_t *)var_8;
    var_11 = (uint64_t *)(var_8 + 160UL);
    var_12 = (uint64_t *)(rdi + 232UL);
    var_13 = (uint64_t *)(rdi + 240UL);
    var_14 = (uint64_t *)(rdi + 248UL);
    local_sp_0 = var_9;
    rax_0 = 12UL;
    var_41 = rbx_1 + 1UL;
    rax_0 = 0UL;
    local_sp_0 = local_sp_2;
    rbp_0 = rbp_1;
    rbx_0 = var_41;
    rcx7_1 = rcx7_2;
    r108_1 = r108_2;
    r910_1 = r910_2;
    rcx7_3 = rcx7_2;
    r811_1 = r811_2;
    r108_3 = r108_2;
    r910_3 = r910_2;
    r811_3 = r811_2;
    do {
        var_15 = *(uint64_t *)((rbx_0 << 3UL) + *(uint64_t *)(rbp_0 + 16UL));
        var_16 = (var_15 << 4UL) + *var_10;
        local_sp_2 = local_sp_0;
        rbp_1 = rbp_0;
        rbx_1 = rbx_0;
        r108_2 = r108_1;
        r910_2 = r910_1;
        r811_2 = r811_1;
        rcx7_3 = rcx7_1;
        local_sp_1 = local_sp_0;
        rcx7_2 = rcx7_1;
        r108_3 = r108_1;
        r910_3 = r910_1;
        r811_3 = r811_1;
        var_17 = *(uint64_t *)var_16;
        var_18 = var_17 + (-63L);
        var_19 = *var_11 >> (var_17 & 63UL);
        var_20 = helper_cc_compute_all_wrapper(var_18, 63UL, var_1, 17U);
        var_21 = helper_cc_compute_c_wrapper(var_18, (var_20 & (-2L)) | (var_19 & 1UL), var_1, 1U);
        if (*(unsigned char *)(var_16 + 8UL) != '\b' & (long)var_17 <= (long)63UL & var_21 != 0UL) {
            var_22 = *var_12;
            var_27 = var_22;
            if (var_22 != *var_13) {
                var_23 = var_22 << 4UL;
                var_24 = *var_14;
                var_25 = local_sp_0 + (-8L);
                *(uint64_t *)var_25 = 4228188UL;
                indirect_placeholder_1(var_24, var_23);
                local_sp_1 = var_25;
                if (var_17 != 0UL) {
                    break;
                }
                var_26 = var_22 << 1UL;
                *var_14 = var_17;
                *var_13 = var_26;
                var_27 = *var_12;
            }
            var_28 = (var_27 << 3UL) + *var_14;
            var_29 = local_sp_1 + (-8L);
            *(uint64_t *)var_29 = 4228244UL;
            var_30 = indirect_placeholder_2(rbp_0, rbx_0, 1UL, rcx7_1, r108_1, 48UL, r910_1, r811_1);
            *(uint64_t *)var_28 = var_30.field_0;
            var_31 = *(uint64_t *)((*var_12 << 3UL) + *var_14);
            local_sp_2 = var_29;
            if (var_31 != 0UL) {
                rcx7_3 = var_30.field_3;
                r108_3 = var_30.field_4;
                r910_3 = var_30.field_5;
                r811_3 = var_30.field_6;
                break;
            }
            var_32 = var_30.field_6;
            var_33 = var_30.field_5;
            var_34 = var_30.field_4;
            var_35 = var_30.field_2;
            var_36 = var_30.field_1;
            *(uint64_t *)(var_31 + 8UL) = var_15;
            var_37 = *var_14;
            var_38 = *var_12;
            *var_12 = (var_38 + 1UL);
            var_39 = *(uint64_t **)((var_38 << 3UL) + var_37);
            var_40 = *(uint64_t *)local_sp_1;
            *var_39 = var_40;
            rbp_1 = var_36;
            rbx_1 = var_35;
            r108_2 = var_34;
            r910_2 = var_33;
            r811_2 = var_32;
            rcx7_2 = var_40;
        }
        var_41 = rbx_1 + 1UL;
        rax_0 = 0UL;
        local_sp_0 = local_sp_2;
        rbp_0 = rbp_1;
        rbx_0 = var_41;
        rcx7_1 = rcx7_2;
        r108_1 = r108_2;
        r910_1 = r910_2;
        rcx7_3 = rcx7_2;
        r811_1 = r811_2;
        r108_3 = r108_2;
        r910_3 = r910_2;
        r811_3 = r811_2;
    } while ((long)*(uint64_t *)(rbp_1 + 8UL) <= (long)var_41);
    mrv.field_0 = rax_0;
    mrv1 = mrv;
    mrv1.field_1 = rcx7_3;
    mrv2 = mrv1;
    mrv2.field_2 = r108_3;
    mrv3 = mrv2;
    mrv3.field_3 = r910_3;
    mrv4 = mrv3;
    mrv4.field_4 = r811_3;
    return mrv4;
}

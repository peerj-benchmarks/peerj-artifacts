typedef unsigned char __u_char;
typedef unsigned short __u_short;
typedef unsigned int __u_int;
typedef unsigned long __u_long;
typedef signed char __int8_t;
typedef unsigned char __uint8_t;
typedef short __int16_t;
typedef unsigned short __uint16_t;
typedef int __int32_t;
typedef unsigned int __uint32_t;
typedef long __int64_t;
typedef unsigned long __uint64_t;
typedef __int8_t __int_least8_t;
typedef __uint8_t __uint_least8_t;
typedef __int16_t __int_least16_t;
typedef __uint16_t __uint_least16_t;
typedef __int32_t __int_least32_t;
typedef __uint32_t __uint_least32_t;
typedef __int64_t __int_least64_t;
typedef __uint64_t __uint_least64_t;
typedef long __quad_t;
typedef unsigned long __u_quad_t;
typedef long __intmax_t;
typedef unsigned long __uintmax_t;
typedef unsigned long __dev_t;
typedef unsigned int __uid_t;
typedef unsigned int __gid_t;
typedef unsigned long __ino_t;
typedef unsigned long __ino64_t;
typedef unsigned int __mode_t;
typedef unsigned long __nlink_t;
typedef long __off_t;
typedef long __off64_t;
typedef int __pid_t;
typedef struct {
    int __val[2];
} __fsid_t;
typedef long __clock_t;
typedef unsigned long __rlim_t;
typedef unsigned long __rlim64_t;
typedef unsigned int __id_t;
typedef long __time_t;
typedef unsigned int __useconds_t;
typedef long __suseconds_t;
typedef int __daddr_t;
typedef int __key_t;
typedef int __clockid_t;
typedef void *__timer_t;
typedef long __blksize_t;
typedef long __blkcnt_t;
typedef long __blkcnt64_t;
typedef unsigned long __fsblkcnt_t;
typedef unsigned long __fsblkcnt64_t;
typedef unsigned long __fsfilcnt_t;
typedef unsigned long __fsfilcnt64_t;
typedef long __fsword_t;
typedef long __ssize_t;
typedef long __syscall_slong_t;
typedef unsigned long __syscall_ulong_t;
typedef __off64_t __loff_t;
typedef char *__caddr_t;
typedef long __intptr_t;
typedef unsigned int __socklen_t;
typedef int __sig_atomic_t;
typedef __int8_t int8_t;
typedef __int16_t int16_t;
typedef __int32_t int32_t;
typedef __int64_t int64_t;
typedef __uint8_t uint8_t;
typedef __uint16_t uint16_t;
typedef __uint32_t uint32_t;
typedef __uint64_t uint64_t;
typedef __int_least8_t int_least8_t;
typedef __int_least16_t int_least16_t;
typedef __int_least32_t int_least32_t;
typedef __int_least64_t int_least64_t;
typedef __uint_least8_t uint_least8_t;
typedef __uint_least16_t uint_least16_t;
typedef __uint_least32_t uint_least32_t;
typedef __uint_least64_t uint_least64_t;
typedef signed char int_fast8_t;
typedef long int_fast16_t;
typedef long int_fast32_t;
typedef long int_fast64_t;
typedef unsigned char uint_fast8_t;
typedef unsigned long uint_fast16_t;
typedef unsigned long uint_fast32_t;
typedef unsigned long uint_fast64_t;
typedef long intptr_t;
typedef unsigned long uintptr_t;
typedef __intmax_t intmax_t;
typedef __uintmax_t uintmax_t;
struct bb_get_first_line_in_buffer_ret_type;
struct indirect_placeholder_98_ret_type;
struct indirect_placeholder_99_ret_type;
struct bb_get_first_line_in_buffer_ret_type {
    uint64_t field_0;
    uint64_t field_1;
    uint64_t field_2;
    uint64_t field_3;
    uint64_t field_4;
    uint64_t field_5;
};
struct indirect_placeholder_98_ret_type {
    uint64_t field_0;
    uint64_t field_1;
    uint64_t field_2;
    uint64_t field_3;
    uint64_t field_4;
    uint64_t field_5;
    uint64_t field_6;
};
struct indirect_placeholder_99_ret_type {
    uint64_t field_0;
    uint64_t field_1;
    uint64_t field_2;
    uint64_t field_3;
    uint64_t field_4;
    uint64_t field_5;
    uint64_t field_6;
};
extern uint64_t revng_init_local_sp(uint64_t param_0);
extern void indirect_placeholder(void);
extern uint64_t init_rcx(void);
extern uint64_t init_r9(void);
extern uint64_t init_r8(void);
extern struct indirect_placeholder_98_ret_type indirect_placeholder_98(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint64_t param_3, uint64_t param_4, uint64_t param_5, uint64_t param_6);
extern struct indirect_placeholder_99_ret_type indirect_placeholder_99(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint64_t param_3, uint64_t param_4, uint64_t param_5);
struct bb_get_first_line_in_buffer_ret_type bb_get_first_line_in_buffer(uint64_t rbp, uint64_t rbx, uint64_t r14, uint64_t r13, uint64_t r12, uint64_t r15) {
    uint64_t var_0;
    uint64_t var_1;
    uint64_t var_2;
    uint64_t var_3;
    uint64_t var_4;
    uint64_t var_12;
    struct bb_get_first_line_in_buffer_ret_type mrv;
    struct bb_get_first_line_in_buffer_ret_type mrv1;
    struct bb_get_first_line_in_buffer_ret_type mrv2;
    struct bb_get_first_line_in_buffer_ret_type mrv3;
    struct bb_get_first_line_in_buffer_ret_type mrv4;
    struct bb_get_first_line_in_buffer_ret_type mrv5;
    struct indirect_placeholder_99_ret_type var_5;
    uint64_t var_6;
    uint64_t var_7;
    uint64_t var_8;
    uint64_t var_9;
    uint64_t var_10;
    uint64_t var_11;
    struct bb_get_first_line_in_buffer_ret_type mrv6;
    struct bb_get_first_line_in_buffer_ret_type mrv7;
    struct bb_get_first_line_in_buffer_ret_type mrv8;
    struct bb_get_first_line_in_buffer_ret_type mrv9;
    struct bb_get_first_line_in_buffer_ret_type mrv10;
    struct bb_get_first_line_in_buffer_ret_type mrv11;
    var_0 = revng_init_local_sp(0UL);
    var_1 = init_rcx();
    var_2 = init_r9();
    var_3 = init_r8();
    var_4 = *(uint64_t *)6434304UL;
    if (var_4 == 0UL) {
        mrv6.field_0 = *(uint64_t *)(var_4 + 24UL);
        mrv7 = mrv6;
        mrv7.field_1 = rbp;
        mrv8 = mrv7;
        mrv8.field_2 = rbx;
        mrv9 = mrv8;
        mrv9.field_3 = r14;
        mrv10 = mrv9;
        mrv10.field_4 = r13;
        mrv11 = mrv10;
        mrv11.field_5 = r15;
        return mrv11;
    }
    *(uint64_t *)(var_0 + (-16L)) = 4206405UL;
    var_5 = indirect_placeholder_99(rbp, rbx, r14, r13, r12, r15);
    var_6 = var_5.field_0;
    var_7 = var_5.field_1;
    var_8 = var_5.field_2;
    var_9 = var_5.field_3;
    var_10 = var_5.field_4;
    var_11 = var_5.field_6;
    if ((uint64_t)(unsigned char)var_6 != 0UL) {
        mrv.field_0 = *(uint64_t *)(*(uint64_t *)6434304UL + 24UL);
        mrv1 = mrv;
        mrv1.field_1 = var_7;
        mrv2 = mrv1;
        mrv2.field_2 = var_8;
        mrv3 = mrv2;
        mrv3.field_3 = var_9;
        mrv4 = mrv3;
        mrv4.field_4 = var_10;
        mrv5 = mrv4;
        mrv5.field_5 = var_11;
        return mrv5;
    }
    *(uint64_t *)(var_0 + (-24L)) = 4206414UL;
    indirect_placeholder();
    var_12 = (uint64_t)*(uint32_t *)var_6;
    *(uint64_t *)(var_0 + (-32L)) = 4206436UL;
    indirect_placeholder_98(0UL, 4310000UL, 1UL, var_1, var_12, var_2, var_3);
    mrv.field_0 = *(uint64_t *)(*(uint64_t *)6434304UL + 24UL);
    mrv1 = mrv;
    mrv1.field_1 = var_7;
    mrv2 = mrv1;
    mrv2.field_2 = var_8;
    mrv3 = mrv2;
    mrv3.field_3 = var_9;
    mrv4 = mrv3;
    mrv4.field_4 = var_10;
    mrv5 = mrv4;
    mrv5.field_5 = var_11;
    return mrv5;
}

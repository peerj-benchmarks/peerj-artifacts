typedef unsigned char __u_char;
typedef unsigned short __u_short;
typedef unsigned int __u_int;
typedef unsigned long __u_long;
typedef signed char __int8_t;
typedef unsigned char __uint8_t;
typedef short __int16_t;
typedef unsigned short __uint16_t;
typedef int __int32_t;
typedef unsigned int __uint32_t;
typedef long __int64_t;
typedef unsigned long __uint64_t;
typedef __int8_t __int_least8_t;
typedef __uint8_t __uint_least8_t;
typedef __int16_t __int_least16_t;
typedef __uint16_t __uint_least16_t;
typedef __int32_t __int_least32_t;
typedef __uint32_t __uint_least32_t;
typedef __int64_t __int_least64_t;
typedef __uint64_t __uint_least64_t;
typedef long __quad_t;
typedef unsigned long __u_quad_t;
typedef long __intmax_t;
typedef unsigned long __uintmax_t;
typedef unsigned long __dev_t;
typedef unsigned int __uid_t;
typedef unsigned int __gid_t;
typedef unsigned long __ino_t;
typedef unsigned long __ino64_t;
typedef unsigned int __mode_t;
typedef unsigned long __nlink_t;
typedef long __off_t;
typedef long __off64_t;
typedef int __pid_t;
typedef struct {
    int __val[2];
} __fsid_t;
typedef long __clock_t;
typedef unsigned long __rlim_t;
typedef unsigned long __rlim64_t;
typedef unsigned int __id_t;
typedef long __time_t;
typedef unsigned int __useconds_t;
typedef long __suseconds_t;
typedef int __daddr_t;
typedef int __key_t;
typedef int __clockid_t;
typedef void *__timer_t;
typedef long __blksize_t;
typedef long __blkcnt_t;
typedef long __blkcnt64_t;
typedef unsigned long __fsblkcnt_t;
typedef unsigned long __fsblkcnt64_t;
typedef unsigned long __fsfilcnt_t;
typedef unsigned long __fsfilcnt64_t;
typedef long __fsword_t;
typedef long __ssize_t;
typedef long __syscall_slong_t;
typedef unsigned long __syscall_ulong_t;
typedef __off64_t __loff_t;
typedef char *__caddr_t;
typedef long __intptr_t;
typedef unsigned int __socklen_t;
typedef int __sig_atomic_t;
typedef __int8_t int8_t;
typedef __int16_t int16_t;
typedef __int32_t int32_t;
typedef __int64_t int64_t;
typedef __uint8_t uint8_t;
typedef __uint16_t uint16_t;
typedef __uint32_t uint32_t;
typedef __uint64_t uint64_t;
typedef __int_least8_t int_least8_t;
typedef __int_least16_t int_least16_t;
typedef __int_least32_t int_least32_t;
typedef __int_least64_t int_least64_t;
typedef __uint_least8_t uint_least8_t;
typedef __uint_least16_t uint_least16_t;
typedef __uint_least32_t uint_least32_t;
typedef __uint_least64_t uint_least64_t;
typedef signed char int_fast8_t;
typedef long int_fast16_t;
typedef long int_fast32_t;
typedef long int_fast64_t;
typedef unsigned char uint_fast8_t;
typedef unsigned long uint_fast16_t;
typedef unsigned long uint_fast32_t;
typedef unsigned long uint_fast64_t;
typedef long intptr_t;
typedef unsigned long uintptr_t;
typedef __intmax_t intmax_t;
typedef __uintmax_t uintmax_t;
struct bb_get_subexp_sub_ret_type;
struct indirect_placeholder_171_ret_type;
struct indirect_placeholder_172_ret_type;
struct bb_get_subexp_sub_ret_type {
    uint64_t field_0;
    uint64_t field_1;
    uint64_t field_2;
    uint64_t field_3;
};
struct indirect_placeholder_171_ret_type {
    uint64_t field_0;
    uint64_t field_1;
    uint64_t field_2;
    uint64_t field_3;
    uint64_t field_4;
};
struct indirect_placeholder_172_ret_type {
    uint64_t field_0;
    uint64_t field_1;
    uint64_t field_2;
    uint64_t field_3;
};
extern uint64_t revng_init_local_sp(uint64_t param_0);
extern uint64_t init_rbp(void);
extern uint64_t init_rbx(void);
extern uint64_t init_r12(void);
extern void indirect_placeholder(void);
extern uint64_t init_r14(void);
extern uint64_t init_r13(void);
extern uint64_t helper_cc_compute_all_wrapper(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint32_t param_3);
extern uint64_t init_r15(void);
extern void indirect_placeholder_1(uint64_t param_0, uint64_t param_1);
extern struct indirect_placeholder_171_ret_type indirect_placeholder_171(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint64_t param_3, uint64_t param_4, uint64_t param_5);
extern struct indirect_placeholder_172_ret_type indirect_placeholder_172(uint64_t param_0, uint64_t param_1);
struct bb_get_subexp_sub_ret_type bb_get_subexp_sub(uint64_t rdx, uint64_t rdi, uint64_t rcx, uint64_t rsi, uint64_t r8) {
    uint64_t var_0;
    uint64_t var_1;
    uint64_t var_2;
    uint64_t var_3;
    uint64_t var_4;
    uint64_t var_5;
    uint64_t var_6;
    uint64_t *var_7;
    uint64_t var_8;
    uint64_t var_9;
    uint64_t *var_10;
    uint64_t var_11;
    uint64_t var_12;
    uint64_t *var_13;
    struct indirect_placeholder_171_ret_type var_14;
    uint64_t var_15;
    uint64_t var_16;
    uint64_t var_17;
    uint64_t var_18;
    uint64_t rax_0;
    uint64_t var_29;
    uint64_t var_19;
    uint64_t var_20;
    uint64_t var_21;
    uint64_t *var_22;
    uint64_t var_23;
    uint64_t *var_24;
    uint64_t var_25;
    uint64_t r9_0;
    uint64_t r88_0;
    uint64_t var_26;
    uint64_t *var_27;
    uint64_t var_28;
    uint64_t local_sp_0;
    uint64_t var_30;
    uint64_t *_pre_phi70;
    uint64_t *var_31;
    uint64_t var_32;
    uint64_t var_33;
    uint64_t var_34;
    uint32_t *var_35;
    uint64_t var_36;
    uint64_t var_37;
    uint64_t var_38;
    struct indirect_placeholder_172_ret_type var_39;
    uint64_t var_40;
    uint64_t var_41;
    uint64_t var_42;
    struct bb_get_subexp_sub_ret_type mrv;
    struct bb_get_subexp_sub_ret_type mrv1;
    struct bb_get_subexp_sub_ret_type mrv2;
    struct bb_get_subexp_sub_ret_type mrv3;
    var_0 = revng_init_local_sp(0UL);
    var_1 = init_rbp();
    var_2 = init_rbx();
    var_3 = init_r14();
    var_4 = init_r13();
    var_5 = init_r12();
    var_6 = init_r15();
    *(uint64_t *)(var_0 + (-8L)) = var_6;
    *(uint64_t *)(var_0 + (-16L)) = var_3;
    *(uint64_t *)(var_0 + (-24L)) = var_4;
    *(uint64_t *)(var_0 + (-32L)) = var_5;
    *(uint64_t *)(var_0 + (-40L)) = var_1;
    *(uint64_t *)(var_0 + (-48L)) = var_2;
    *(uint64_t *)(var_0 + (-64L)) = rsi;
    var_7 = (uint64_t *)(rdx + 8UL);
    var_8 = *var_7;
    var_9 = rdx + 16UL;
    var_10 = (uint64_t *)(var_0 + (-88L));
    *var_10 = 8UL;
    var_11 = *(uint64_t *)rdx;
    var_12 = var_0 + (-96L);
    var_13 = (uint64_t *)var_12;
    *var_13 = 4250340UL;
    var_14 = indirect_placeholder_171(var_11, rdi, var_8, var_9, r8, rcx);
    var_15 = var_14.field_0;
    var_16 = var_14.field_2;
    var_17 = var_14.field_3;
    var_18 = var_14.field_4;
    rax_0 = var_15;
    r9_0 = var_17;
    r88_0 = var_18;
    if ((uint64_t)(uint32_t)var_15 == 0UL) {
        mrv.field_0 = rax_0;
        mrv1 = mrv;
        mrv1.field_1 = var_16;
        mrv2 = mrv1;
        mrv2.field_2 = r9_0;
        mrv3 = mrv2;
        mrv3.field_3 = r88_0;
        return mrv3;
    }
    var_19 = var_0 + (-80L);
    var_20 = *var_7;
    var_21 = **(uint64_t **)(var_0 + (-72L));
    var_22 = (uint64_t *)(rdi + 208UL);
    var_23 = *var_22;
    var_24 = (uint64_t *)(rdi + 200UL);
    var_25 = *var_24;
    var_29 = var_25;
    local_sp_0 = var_19;
    rax_0 = 12UL;
    if ((long)var_25 >= (long)var_23) {
        var_26 = var_23 * 80UL;
        var_27 = (uint64_t *)(rdi + 216UL);
        var_28 = *var_27;
        *var_10 = 4250402UL;
        indirect_placeholder_1(var_28, var_26);
        local_sp_0 = var_12;
        if (var_23 != 0UL) {
            *var_13 = 4250419UL;
            indirect_placeholder();
            mrv.field_0 = rax_0;
            mrv1 = mrv;
            mrv1.field_1 = var_16;
            mrv2 = mrv1;
            mrv2.field_2 = r9_0;
            mrv3 = mrv2;
            mrv3.field_3 = r88_0;
            return mrv3;
        }
        *var_27 = var_23;
        *var_13 = 4250476UL;
        indirect_placeholder();
        *var_22 = (*var_22 << 1UL);
        var_29 = *var_24;
    }
    var_30 = helper_cc_compute_all_wrapper(var_29, 0UL, 0UL, 25U);
    if ((uint64_t)(((unsigned char)(var_30 >> 4UL) ^ (unsigned char)var_30) & '\xc0') == 0UL) {
        _pre_phi70 = (uint64_t *)(rdi + 216UL);
    } else {
        var_31 = (uint64_t *)(rdi + 216UL);
        var_32 = (var_29 * 40UL) + *var_31;
        _pre_phi70 = var_31;
        if (*(uint64_t *)(var_32 + (-32L)) == r8) {
            *(unsigned char *)(var_32 + (-8L)) = (unsigned char)'\x01';
        }
    }
    *(uint64_t *)((*var_24 * 40UL) + *_pre_phi70) = rcx;
    *(uint64_t *)(((*var_24 * 40UL) + *_pre_phi70) + 8UL) = r8;
    *(uint64_t *)(((*var_24 * 40UL) + *_pre_phi70) + 16UL) = var_21;
    *(uint64_t *)(((*var_24 * 40UL) + *_pre_phi70) + 24UL) = var_20;
    *(uint16_t *)(((*var_24 * 40UL) + *_pre_phi70) + 34UL) = (var_20 == var_21);
    var_33 = *_pre_phi70;
    var_34 = *var_24;
    *var_24 = (var_34 + 1UL);
    *(unsigned char *)(((var_34 * 40UL) + var_33) + 32UL) = (unsigned char)'\x00';
    var_35 = (uint32_t *)(rdi + 224UL);
    var_36 = (uint64_t)*var_35;
    var_37 = var_20 - var_21;
    if ((long)var_37 > (long)var_36) {
        *var_35 = (uint32_t)var_37;
    }
    var_38 = (*var_7 + r8) - **(uint64_t **)(local_sp_0 + 8UL);
    *(uint64_t *)(local_sp_0 + (-8L)) = 4250746UL;
    var_39 = indirect_placeholder_172(rdi, var_38);
    var_40 = var_39.field_0;
    var_41 = var_39.field_2;
    var_42 = var_39.field_3;
    rax_0 = var_40;
    r9_0 = var_41;
    r88_0 = var_42;
    mrv.field_0 = rax_0;
    mrv1 = mrv;
    mrv1.field_1 = var_16;
    mrv2 = mrv1;
    mrv2.field_2 = r9_0;
    mrv3 = mrv2;
    mrv3.field_3 = r88_0;
    return mrv3;
}

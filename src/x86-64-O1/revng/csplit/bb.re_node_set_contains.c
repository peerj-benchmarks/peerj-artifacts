typedef unsigned char __u_char;
typedef unsigned short __u_short;
typedef unsigned int __u_int;
typedef unsigned long __u_long;
typedef signed char __int8_t;
typedef unsigned char __uint8_t;
typedef short __int16_t;
typedef unsigned short __uint16_t;
typedef int __int32_t;
typedef unsigned int __uint32_t;
typedef long __int64_t;
typedef unsigned long __uint64_t;
typedef __int8_t __int_least8_t;
typedef __uint8_t __uint_least8_t;
typedef __int16_t __int_least16_t;
typedef __uint16_t __uint_least16_t;
typedef __int32_t __int_least32_t;
typedef __uint32_t __uint_least32_t;
typedef __int64_t __int_least64_t;
typedef __uint64_t __uint_least64_t;
typedef long __quad_t;
typedef unsigned long __u_quad_t;
typedef long __intmax_t;
typedef unsigned long __uintmax_t;
typedef unsigned long __dev_t;
typedef unsigned int __uid_t;
typedef unsigned int __gid_t;
typedef unsigned long __ino_t;
typedef unsigned long __ino64_t;
typedef unsigned int __mode_t;
typedef unsigned long __nlink_t;
typedef long __off_t;
typedef long __off64_t;
typedef int __pid_t;
typedef struct {
    int __val[2];
} __fsid_t;
typedef long __clock_t;
typedef unsigned long __rlim_t;
typedef unsigned long __rlim64_t;
typedef unsigned int __id_t;
typedef long __time_t;
typedef unsigned int __useconds_t;
typedef long __suseconds_t;
typedef int __daddr_t;
typedef int __key_t;
typedef int __clockid_t;
typedef void *__timer_t;
typedef long __blksize_t;
typedef long __blkcnt_t;
typedef long __blkcnt64_t;
typedef unsigned long __fsblkcnt_t;
typedef unsigned long __fsblkcnt64_t;
typedef unsigned long __fsfilcnt_t;
typedef unsigned long __fsfilcnt64_t;
typedef long __fsword_t;
typedef long __ssize_t;
typedef long __syscall_slong_t;
typedef unsigned long __syscall_ulong_t;
typedef __off64_t __loff_t;
typedef char *__caddr_t;
typedef long __intptr_t;
typedef unsigned int __socklen_t;
typedef int __sig_atomic_t;
typedef __int8_t int8_t;
typedef __int16_t int16_t;
typedef __int32_t int32_t;
typedef __int64_t int64_t;
typedef __uint8_t uint8_t;
typedef __uint16_t uint16_t;
typedef __uint32_t uint32_t;
typedef __uint64_t uint64_t;
typedef __int_least8_t int_least8_t;
typedef __int_least16_t int_least16_t;
typedef __int_least32_t int_least32_t;
typedef __int_least64_t int_least64_t;
typedef __uint_least8_t uint_least8_t;
typedef __uint_least16_t uint_least16_t;
typedef __uint_least32_t uint_least32_t;
typedef __uint_least64_t uint_least64_t;
typedef signed char int_fast8_t;
typedef long int_fast16_t;
typedef long int_fast32_t;
typedef long int_fast64_t;
typedef unsigned char uint_fast8_t;
typedef unsigned long uint_fast16_t;
typedef unsigned long uint_fast32_t;
typedef unsigned long uint_fast64_t;
typedef long intptr_t;
typedef unsigned long uintptr_t;
typedef __intmax_t intmax_t;
typedef __uintmax_t uintmax_t;
struct bb_re_node_set_contains_ret_type;
struct bb_re_node_set_contains_ret_type {
    uint64_t field_0;
    uint64_t field_1;
    uint64_t field_2;
};
extern uint64_t revng_init_local_sp(uint64_t param_0);
extern uint64_t init_cc_src2(void);
extern uint64_t helper_cc_compute_c_wrapper(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint32_t param_3);
extern uint64_t helper_cc_compute_all_wrapper(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint32_t param_3);
extern uint64_t init_r8(void);
typedef _Bool bool;
struct bb_re_node_set_contains_ret_type bb_re_node_set_contains(uint64_t rdi, uint64_t rsi) {
    uint64_t var_0;
    uint64_t var_1;
    uint64_t var_2;
    uint64_t var_3;
    uint64_t var_4;
    uint64_t *var_5;
    uint64_t rdx_1;
    uint64_t rcx_0;
    uint64_t rax_0;
    uint64_t r9_0;
    uint64_t r8_0;
    uint64_t r8_1;
    uint64_t var_6;
    uint64_t var_7;
    uint64_t rdx_0;
    uint64_t var_8;
    uint64_t var_9;
    struct bb_re_node_set_contains_ret_type mrv;
    struct bb_re_node_set_contains_ret_type mrv1;
    struct bb_re_node_set_contains_ret_type mrv2;
    struct bb_re_node_set_contains_ret_type mrv4 = {0UL, /*implicit*/(int)0, /*implicit*/(int)0};
    struct bb_re_node_set_contains_ret_type mrv5;
    unsigned int loop_state_var;
    bool switch_state_var;
    revng_init_local_sp(0UL);
    var_0 = init_cc_src2();
    var_1 = init_r8();
    var_2 = *(uint64_t *)(rdi + 8UL);
    var_3 = helper_cc_compute_all_wrapper(var_2, 0UL, 0UL, 25U);
    rcx_0 = 0UL;
    r8_0 = var_1;
    if ((uint64_t)(((unsigned char)(var_3 >> 4UL) ^ (unsigned char)var_3) & '\xc0') == 0UL) {
        mrv4.field_1 = var_2;
        mrv5 = mrv4;
        mrv5.field_2 = var_1;
        return mrv5;
    }
    var_4 = var_2 + (-1L);
    var_5 = (uint64_t *)(rdi + 16UL);
    r9_0 = var_4;
    rdx_1 = r9_0;
    r8_1 = r8_0;
    while (r9_0 <= rcx_0)
        {
            var_6 = (r9_0 + rcx_0) >> 1UL;
            var_7 = *var_5;
            r8_0 = var_7;
            rdx_0 = var_6;
            rax_0 = var_6;
            r8_1 = var_7;
            if ((long)*(uint64_t *)((var_6 << 3UL) + var_7) >= (long)rsi) {
                while (1U)
                    {
                        var_8 = helper_cc_compute_c_wrapper(rcx_0 - rdx_0, rdx_0, var_0, 17U);
                        rdx_1 = rdx_0;
                        if (var_8 != 0UL) {
                            loop_state_var = 1U;
                            break;
                        }
                        var_9 = (rcx_0 + rdx_0) >> 1UL;
                        rdx_0 = var_9;
                        rax_0 = var_9;
                        if ((long)*(uint64_t *)((var_9 << 3UL) + var_7) < (long)rsi) {
                            continue;
                        }
                        loop_state_var = 0U;
                        break;
                    }
                switch_state_var = 0;
                switch (loop_state_var) {
                  case 1U:
                    {
                        switch_state_var = 1;
                        break;
                    }
                    break;
                  case 0U:
                    {
                        break;
                    }
                    break;
                }
                if (switch_state_var)
                    break;
            }
            rcx_0 = rax_0 + 1UL;
            r9_0 = rdx_1;
            rdx_1 = r9_0;
            r8_1 = r8_0;
        }
    mrv.field_0 = (*(uint64_t *)((rcx_0 << 3UL) + *var_5) == rsi) ? (rcx_0 + 1UL) : 0UL;
    mrv1 = mrv;
    mrv1.field_1 = r9_0;
    mrv2 = mrv1;
    mrv2.field_2 = r8_1;
    return mrv2;
}

typedef unsigned char __u_char;
typedef unsigned short __u_short;
typedef unsigned int __u_int;
typedef unsigned long __u_long;
typedef signed char __int8_t;
typedef unsigned char __uint8_t;
typedef short __int16_t;
typedef unsigned short __uint16_t;
typedef int __int32_t;
typedef unsigned int __uint32_t;
typedef long __int64_t;
typedef unsigned long __uint64_t;
typedef __int8_t __int_least8_t;
typedef __uint8_t __uint_least8_t;
typedef __int16_t __int_least16_t;
typedef __uint16_t __uint_least16_t;
typedef __int32_t __int_least32_t;
typedef __uint32_t __uint_least32_t;
typedef __int64_t __int_least64_t;
typedef __uint64_t __uint_least64_t;
typedef long __quad_t;
typedef unsigned long __u_quad_t;
typedef long __intmax_t;
typedef unsigned long __uintmax_t;
typedef unsigned long __dev_t;
typedef unsigned int __uid_t;
typedef unsigned int __gid_t;
typedef unsigned long __ino_t;
typedef unsigned long __ino64_t;
typedef unsigned int __mode_t;
typedef unsigned long __nlink_t;
typedef long __off_t;
typedef long __off64_t;
typedef int __pid_t;
typedef struct {
    int __val[2];
} __fsid_t;
typedef long __clock_t;
typedef unsigned long __rlim_t;
typedef unsigned long __rlim64_t;
typedef unsigned int __id_t;
typedef long __time_t;
typedef unsigned int __useconds_t;
typedef long __suseconds_t;
typedef int __daddr_t;
typedef int __key_t;
typedef int __clockid_t;
typedef void *__timer_t;
typedef long __blksize_t;
typedef long __blkcnt_t;
typedef long __blkcnt64_t;
typedef unsigned long __fsblkcnt_t;
typedef unsigned long __fsblkcnt64_t;
typedef unsigned long __fsfilcnt_t;
typedef unsigned long __fsfilcnt64_t;
typedef long __fsword_t;
typedef long __ssize_t;
typedef long __syscall_slong_t;
typedef unsigned long __syscall_ulong_t;
typedef __off64_t __loff_t;
typedef char *__caddr_t;
typedef long __intptr_t;
typedef unsigned int __socklen_t;
typedef int __sig_atomic_t;
typedef __int8_t int8_t;
typedef __int16_t int16_t;
typedef __int32_t int32_t;
typedef __int64_t int64_t;
typedef __uint8_t uint8_t;
typedef __uint16_t uint16_t;
typedef __uint32_t uint32_t;
typedef __uint64_t uint64_t;
typedef __int_least8_t int_least8_t;
typedef __int_least16_t int_least16_t;
typedef __int_least32_t int_least32_t;
typedef __int_least64_t int_least64_t;
typedef __uint_least8_t uint_least8_t;
typedef __uint_least16_t uint_least16_t;
typedef __uint_least32_t uint_least32_t;
typedef __uint_least64_t uint_least64_t;
typedef signed char int_fast8_t;
typedef long int_fast16_t;
typedef long int_fast32_t;
typedef long int_fast64_t;
typedef unsigned char uint_fast8_t;
typedef unsigned long uint_fast16_t;
typedef unsigned long uint_fast32_t;
typedef unsigned long uint_fast64_t;
typedef long intptr_t;
typedef unsigned long uintptr_t;
typedef __intmax_t intmax_t;
typedef __uintmax_t uintmax_t;
struct bb_re_acquire_state_ret_type;
struct indirect_placeholder_160_ret_type;
struct indirect_placeholder_161_ret_type;
struct bb_re_acquire_state_ret_type {
    uint64_t field_0;
    uint64_t field_1;
};
struct indirect_placeholder_160_ret_type {
    uint64_t field_0;
    uint64_t field_1;
    uint64_t field_2;
    uint64_t field_3;
};
struct indirect_placeholder_161_ret_type {
    uint64_t field_0;
    uint64_t field_1;
    uint64_t field_2;
    uint64_t field_3;
    uint64_t field_4;
    uint64_t field_5;
    uint64_t field_6;
};
extern uint64_t revng_init_local_sp(uint64_t param_0);
extern uint64_t init_rbp(void);
extern uint64_t init_rbx(void);
extern uint64_t init_r12(void);
extern void indirect_placeholder(void);
extern uint64_t init_r14(void);
extern uint64_t init_r13(void);
extern uint64_t helper_cc_compute_all_wrapper(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint32_t param_3);
extern uint64_t init_r15(void);
extern void indirect_placeholder_17(uint64_t param_0);
extern uint64_t indirect_placeholder_5(uint64_t param_0, uint64_t param_1);
extern uint64_t indirect_placeholder_3(uint64_t param_0, uint64_t param_1, uint64_t param_2);
extern struct indirect_placeholder_160_ret_type indirect_placeholder_160(uint64_t param_0, uint64_t param_1);
extern struct indirect_placeholder_161_ret_type indirect_placeholder_161(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint64_t param_3, uint64_t param_4, uint64_t param_5, uint64_t param_6, uint64_t param_7);
struct bb_re_acquire_state_ret_type bb_re_acquire_state(uint64_t rdx, uint64_t rdi, uint64_t r10, uint64_t rsi, uint64_t r9, uint64_t r8) {
    uint64_t var_0;
    uint64_t var_1;
    uint64_t var_2;
    uint64_t var_3;
    uint64_t var_4;
    uint64_t var_5;
    uint64_t var_6;
    uint64_t var_7;
    uint64_t *var_8;
    uint64_t var_9;
    uint64_t rbp_1;
    uint64_t var_48;
    uint64_t var_34;
    uint64_t rcx_0;
    uint64_t *var_35;
    unsigned char *var_36;
    uint64_t rsi5_0;
    uint64_t var_37;
    uint64_t var_38;
    uint32_t var_39;
    uint32_t *var_41;
    unsigned char var_42;
    uint32_t *_pre_phi;
    unsigned char var_40;
    unsigned char var_44;
    uint64_t rcx_2;
    unsigned char var_43;
    uint64_t var_45;
    uint64_t var_46;
    uint64_t var_47;
    uint64_t var_30;
    uint64_t var_31;
    uint64_t *var_32;
    uint64_t var_33;
    uint64_t local_sp_3;
    uint64_t local_sp_0;
    uint64_t rbp_0;
    uint64_t r96_0;
    struct bb_re_acquire_state_ret_type mrv;
    struct bb_re_acquire_state_ret_type mrv1;
    uint64_t var_10;
    uint64_t r12_1;
    uint64_t var_11;
    uint64_t var_12;
    uint64_t rax_0;
    uint64_t r12_0;
    uint64_t var_13;
    uint64_t var_14;
    uint64_t var_15;
    uint64_t var_16;
    uint64_t var_17;
    uint64_t local_sp_2;
    uint64_t local_sp_1;
    uint64_t rbx_0;
    uint64_t r96_2;
    uint64_t rcx_1;
    uint64_t r87_1;
    uint64_t r96_1;
    uint64_t r87_0;
    uint64_t var_18;
    uint64_t var_19;
    uint64_t var_20;
    struct indirect_placeholder_160_ret_type var_21;
    uint64_t var_22;
    uint64_t var_23;
    uint64_t var_24;
    uint64_t var_25;
    uint64_t rbx_1;
    uint64_t rcx_3;
    uint64_t r96_3;
    uint64_t r87_2;
    uint64_t var_26;
    struct indirect_placeholder_161_ret_type var_27;
    uint64_t var_28;
    uint64_t var_29;
    unsigned int loop_state_var;
    var_0 = revng_init_local_sp(0UL);
    var_1 = init_rbp();
    var_2 = init_rbx();
    var_3 = init_r14();
    var_4 = init_r13();
    var_5 = init_r12();
    var_6 = init_r15();
    *(uint64_t *)(var_0 + (-8L)) = var_6;
    *(uint64_t *)(var_0 + (-16L)) = var_3;
    *(uint64_t *)(var_0 + (-24L)) = var_4;
    *(uint64_t *)(var_0 + (-32L)) = var_5;
    *(uint64_t *)(var_0 + (-40L)) = var_1;
    *(uint64_t *)(var_0 + (-48L)) = var_2;
    var_7 = var_0 + (-72L);
    var_8 = (uint64_t *)(rdx + 8UL);
    var_9 = *var_8;
    rbp_1 = var_1;
    rcx_0 = var_9;
    rsi5_0 = 0UL;
    local_sp_3 = var_7;
    rbp_0 = 0UL;
    r96_0 = r9;
    r12_1 = var_9;
    r12_0 = var_9;
    local_sp_1 = var_7;
    rbx_0 = 0UL;
    r96_1 = r9;
    r87_0 = r8;
    rbx_1 = var_2;
    r96_3 = r9;
    r87_2 = r8;
    if (var_9 == 0UL) {
        *(uint32_t *)rdi = 0U;
    } else {
        var_10 = helper_cc_compute_all_wrapper(var_9, 0UL, 0UL, 25U);
        if ((uint64_t)(((unsigned char)(var_10 >> 4UL) ^ (unsigned char)var_10) & '\xc0') != 0UL) {
            var_11 = *(uint64_t *)(rdx + 16UL);
            var_12 = (var_9 << 3UL) + var_11;
            rax_0 = var_11;
            rcx_0 = var_12;
            var_13 = r12_0 + *(uint64_t *)rax_0;
            var_14 = rax_0 + 8UL;
            rax_0 = var_14;
            r12_0 = var_13;
            r12_1 = var_13;
            do {
                var_13 = r12_0 + *(uint64_t *)rax_0;
                var_14 = rax_0 + 8UL;
                rax_0 = var_14;
                r12_0 = var_13;
                r12_1 = var_13;
            } while (var_14 != var_12);
        }
        *(uint64_t *)var_7 = rsi;
        *(uint64_t *)(var_0 + (-64L)) = rdi;
        var_15 = ((r12_1 & *(uint64_t *)(rsi + 136UL)) * 24UL) + *(uint64_t *)(rsi + 64UL);
        var_16 = *(uint64_t *)var_15;
        var_17 = helper_cc_compute_all_wrapper(var_16, 0UL, 0UL, 25U);
        rcx_1 = rcx_0;
        rcx_3 = rcx_0;
        if ((uint64_t)(((unsigned char)(var_17 >> 4UL) ^ (unsigned char)var_17) & '\xc0') != 0UL) {
            rbx_1 = var_16;
            while (1U)
                {
                    var_18 = *(uint64_t *)((rbx_0 << 3UL) + *(uint64_t *)(var_15 + 16UL));
                    rbp_1 = var_18;
                    rcx_2 = rcx_1;
                    rbp_0 = var_18;
                    local_sp_2 = local_sp_1;
                    r96_2 = r96_1;
                    r87_1 = r87_0;
                    if (*(uint64_t *)var_18 != r12_1) {
                        var_19 = var_18 + 8UL;
                        var_20 = local_sp_1 + (-8L);
                        *(uint64_t *)var_20 = 4247202UL;
                        var_21 = indirect_placeholder_160(var_19, rdx);
                        var_22 = var_21.field_0;
                        var_23 = var_21.field_1;
                        var_24 = var_21.field_2;
                        rcx_2 = var_23;
                        r96_0 = var_24;
                        local_sp_2 = var_20;
                        r96_2 = var_24;
                        r87_1 = var_21.field_3;
                        if ((uint64_t)(unsigned char)var_22 != 0UL) {
                            loop_state_var = 1U;
                            break;
                        }
                    }
                    var_25 = rbx_0 + 1UL;
                    local_sp_3 = local_sp_2;
                    local_sp_1 = local_sp_2;
                    rbx_0 = var_25;
                    rcx_1 = rcx_2;
                    r96_1 = r96_2;
                    r87_0 = r87_1;
                    rcx_3 = rcx_2;
                    r96_3 = r96_2;
                    r87_2 = r87_1;
                    if (var_25 == var_16) {
                        continue;
                    }
                    loop_state_var = 0U;
                    break;
                }
            switch (loop_state_var) {
              case 1U:
                {
                    mrv.field_0 = rbp_0;
                    mrv1 = mrv;
                    mrv1.field_1 = r96_0;
                    return mrv1;
                }
                break;
              case 0U:
                {
                    break;
                }
                break;
            }
        }
        var_26 = local_sp_3 + (-8L);
        *(uint64_t *)var_26 = 4247234UL;
        var_27 = indirect_placeholder_161(rbp_1, rbx_1, 112UL, rcx_3, r10, 1UL, r96_3, r87_2);
        var_28 = var_27.field_0;
        var_29 = var_27.field_5;
        local_sp_0 = var_26;
        rbp_0 = var_28;
        r96_0 = var_29;
        if (var_28 != 0UL) {
            var_30 = var_28 + 8UL;
            var_31 = local_sp_3 + (-16L);
            var_32 = (uint64_t *)var_31;
            *var_32 = 4247261UL;
            var_33 = indirect_placeholder_5(var_30, rdx);
            if ((uint64_t)(uint32_t)var_33 == 0UL) {
                var_34 = local_sp_3 + (-24L);
                *(uint64_t *)var_34 = 4247273UL;
                indirect_placeholder();
                local_sp_0 = var_34;
            } else {
                *(uint64_t *)(var_28 + 80UL) = var_30;
                if ((long)*var_8 <= (long)0UL) {
                    var_35 = (uint64_t *)(rdx + 16UL);
                    var_36 = (unsigned char *)(var_28 + 104UL);
                    var_45 = rsi5_0 + 1UL;
                    rsi5_0 = var_45;
                    do {
                        var_37 = (*(uint64_t *)((rsi5_0 << 3UL) + *var_35) << 4UL) + **(uint64_t **)var_31;
                        var_38 = var_37 + 8UL;
                        var_39 = (uint32_t)(uint64_t)*(unsigned char *)var_38;
                        if ((uint64_t)(var_39 + (-1)) == 0UL) {
                            var_41 = (uint32_t *)var_38;
                            _pre_phi = var_41;
                            if ((*var_41 & 261888U) != 0U) {
                                var_45 = rsi5_0 + 1UL;
                                rsi5_0 = var_45;
                                if ((long)var_45 >= (long)*var_8) {
                                    continue;
                                }
                                break;
                            }
                            var_42 = ((*(unsigned char *)(var_37 + 10UL) << '\x01') & ' ') | *var_36;
                            *var_36 = var_42;
                            var_43 = var_42;
                        } else {
                            var_40 = ((*(unsigned char *)(var_37 + 10UL) << '\x01') & ' ') | *var_36;
                            *var_36 = var_40;
                            var_43 = var_40;
                            var_44 = var_40;
                            if ((uint64_t)(var_39 + (-2)) != 0UL) {
                                *var_36 = (var_40 | '\x10');
                                var_45 = rsi5_0 + 1UL;
                                rsi5_0 = var_45;
                                if ((long)var_45 >= (long)*var_8) {
                                    continue;
                                }
                                break;
                            }
                            if ((uint64_t)(var_39 + (-4)) != 0UL) {
                                *var_36 = (var_40 | '@');
                                var_45 = rsi5_0 + 1UL;
                                rsi5_0 = var_45;
                                if ((long)var_45 >= (long)*var_8) {
                                    continue;
                                }
                                break;
                            }
                            if ((uint64_t)(var_39 + (-12)) != 0UL) {
                                *var_36 = (var_44 | '\x80');
                                var_45 = rsi5_0 + 1UL;
                                rsi5_0 = var_45;
                                if ((long)var_45 >= (long)*var_8) {
                                    continue;
                                }
                                break;
                            }
                            _pre_phi = (uint32_t *)var_38;
                        }
                        var_44 = var_43;
                        if ((*_pre_phi & 261888U) == 0U) {
                            *var_36 = (var_44 | '\x80');
                        }
                        var_45 = rsi5_0 + 1UL;
                        rsi5_0 = var_45;
                    } while ((long)var_45 >= (long)*var_8);
                }
                var_46 = *var_32;
                *(uint64_t *)(local_sp_3 + (-24L)) = 4247437UL;
                var_47 = indirect_placeholder_3(r12_1, var_46, var_28);
                if ((uint64_t)(uint32_t)var_47 != 0UL) {
                    mrv.field_0 = rbp_0;
                    mrv1 = mrv;
                    mrv1.field_1 = r96_0;
                    return mrv1;
                }
                var_48 = local_sp_3 + (-32L);
                *(uint64_t *)var_48 = 4247452UL;
                indirect_placeholder_17(var_28);
                local_sp_0 = var_48;
            }
        }
        **(uint32_t **)(local_sp_0 + 8UL) = 12U;
    }
}

typedef unsigned char __u_char;
typedef unsigned short __u_short;
typedef unsigned int __u_int;
typedef unsigned long __u_long;
typedef signed char __int8_t;
typedef unsigned char __uint8_t;
typedef short __int16_t;
typedef unsigned short __uint16_t;
typedef int __int32_t;
typedef unsigned int __uint32_t;
typedef long __int64_t;
typedef unsigned long __uint64_t;
typedef __int8_t __int_least8_t;
typedef __uint8_t __uint_least8_t;
typedef __int16_t __int_least16_t;
typedef __uint16_t __uint_least16_t;
typedef __int32_t __int_least32_t;
typedef __uint32_t __uint_least32_t;
typedef __int64_t __int_least64_t;
typedef __uint64_t __uint_least64_t;
typedef long __quad_t;
typedef unsigned long __u_quad_t;
typedef long __intmax_t;
typedef unsigned long __uintmax_t;
typedef unsigned long __dev_t;
typedef unsigned int __uid_t;
typedef unsigned int __gid_t;
typedef unsigned long __ino_t;
typedef unsigned long __ino64_t;
typedef unsigned int __mode_t;
typedef unsigned long __nlink_t;
typedef long __off_t;
typedef long __off64_t;
typedef int __pid_t;
typedef struct {
    int __val[2];
} __fsid_t;
typedef long __clock_t;
typedef unsigned long __rlim_t;
typedef unsigned long __rlim64_t;
typedef unsigned int __id_t;
typedef long __time_t;
typedef unsigned int __useconds_t;
typedef long __suseconds_t;
typedef int __daddr_t;
typedef int __key_t;
typedef int __clockid_t;
typedef void *__timer_t;
typedef long __blksize_t;
typedef long __blkcnt_t;
typedef long __blkcnt64_t;
typedef unsigned long __fsblkcnt_t;
typedef unsigned long __fsblkcnt64_t;
typedef unsigned long __fsfilcnt_t;
typedef unsigned long __fsfilcnt64_t;
typedef long __fsword_t;
typedef long __ssize_t;
typedef long __syscall_slong_t;
typedef unsigned long __syscall_ulong_t;
typedef __off64_t __loff_t;
typedef char *__caddr_t;
typedef long __intptr_t;
typedef unsigned int __socklen_t;
typedef int __sig_atomic_t;
typedef __int8_t int8_t;
typedef __int16_t int16_t;
typedef __int32_t int32_t;
typedef __int64_t int64_t;
typedef __uint8_t uint8_t;
typedef __uint16_t uint16_t;
typedef __uint32_t uint32_t;
typedef __uint64_t uint64_t;
typedef __int_least8_t int_least8_t;
typedef __int_least16_t int_least16_t;
typedef __int_least32_t int_least32_t;
typedef __int_least64_t int_least64_t;
typedef __uint_least8_t uint_least8_t;
typedef __uint_least16_t uint_least16_t;
typedef __uint_least32_t uint_least32_t;
typedef __uint_least64_t uint_least64_t;
typedef signed char int_fast8_t;
typedef long int_fast16_t;
typedef long int_fast32_t;
typedef long int_fast64_t;
typedef unsigned char uint_fast8_t;
typedef unsigned long uint_fast16_t;
typedef unsigned long uint_fast32_t;
typedef unsigned long uint_fast64_t;
typedef long intptr_t;
typedef unsigned long uintptr_t;
typedef __intmax_t intmax_t;
typedef __uintmax_t uintmax_t;
extern uint64_t revng_init_local_sp(uint64_t param_0);
extern uint64_t init_rbp(void);
extern uint64_t init_rbx(void);
extern uint64_t helper_cc_compute_all_wrapper(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint32_t param_3);
extern uint64_t indirect_placeholder_5(uint64_t param_0, uint64_t param_1);
extern void indirect_placeholder_1(uint64_t param_0, uint64_t param_1);
uint64_t bb_re_node_set_insert(uint64_t rdi, uint64_t rsi) {
    uint64_t var_0;
    uint64_t var_1;
    uint64_t var_2;
    uint64_t *_cast;
    uint64_t var_3;
    uint64_t *_pre_phi49;
    uint64_t var_20;
    uint64_t var_21;
    uint64_t rax_3;
    uint64_t *var_4;
    uint64_t var_5;
    uint64_t rdx_0;
    uint64_t *var_8;
    uint64_t var_6;
    uint64_t var_7;
    uint64_t var_9;
    uint64_t var_11;
    uint64_t var_10;
    uint64_t var_17;
    uint64_t rax_2;
    uint64_t rax_0;
    uint64_t var_18;
    uint64_t var_19;
    uint64_t var_12;
    uint64_t var_13;
    uint64_t rax_1;
    uint64_t rdx_1;
    uint64_t rdi1_0;
    uint64_t rcx_0;
    uint64_t rsi2_0;
    uint64_t var_14;
    uint64_t var_15;
    uint64_t var_16;
    var_0 = revng_init_local_sp(0UL);
    var_1 = init_rbp();
    var_2 = init_rbx();
    *(uint64_t *)(var_0 + (-8L)) = var_1;
    *(uint64_t *)(var_0 + (-16L)) = var_2;
    _cast = (uint64_t *)rdi;
    var_3 = *_cast;
    rax_3 = 1UL;
    if (var_3 == 0UL) {
        *(uint64_t *)(var_0 + (-32L)) = 4227302UL;
        var_20 = indirect_placeholder_5(rdi, rsi);
        var_21 = (var_20 & (-256L)) | ((uint64_t)(uint32_t)var_20 == 0UL);
        rax_3 = var_21;
    } else {
        var_4 = (uint64_t *)(rdi + 8UL);
        var_5 = *var_4;
        var_10 = var_5;
        if (var_5 == 0UL) {
            **(uint64_t **)(rdi + 16UL) = rsi;
            *var_4 = (*var_4 + 1UL);
        } else {
            rax_3 = 0UL;
            if (var_3 == var_5) {
                _pre_phi49 = (uint64_t *)(rdi + 16UL);
            } else {
                var_6 = var_3 << 1UL;
                *_cast = var_6;
                var_7 = var_3 << 4UL;
                var_8 = (uint64_t *)(rdi + 16UL);
                var_9 = *var_8;
                *(uint64_t *)(var_0 + (-32L)) = 4227368UL;
                indirect_placeholder_1(var_9, var_7);
                _pre_phi49 = var_8;
                if (var_6 != 0UL) {
                    return rax_3;
                }
                *var_8 = var_6;
                var_10 = *var_4;
            }
            var_11 = *_pre_phi49;
            rax_0 = var_10;
            rax_1 = var_10;
            rdx_1 = var_11;
            rax_2 = var_10;
            if ((long)*(uint64_t *)var_11 > (long)rsi) {
                var_17 = helper_cc_compute_all_wrapper(var_10, 0UL, 0UL, 25U);
                if ((uint64_t)(((unsigned char)(var_17 >> 4UL) ^ (unsigned char)var_17) & '\xc0') != 0UL) {
                    rdx_0 = var_10 << 3UL;
                    rax_2 = 0UL;
                    var_18 = rdx_0 + *_pre_phi49;
                    *(uint64_t *)var_18 = *(uint64_t *)(var_18 + (-8L));
                    var_19 = rax_0 + (-1L);
                    rax_0 = var_19;
                    while (var_19 != 0UL)
                        {
                            rdx_0 = rdx_0 + (-8L);
                            var_18 = rdx_0 + *_pre_phi49;
                            *(uint64_t *)var_18 = *(uint64_t *)(var_18 + (-8L));
                            var_19 = rax_0 + (-1L);
                            rax_0 = var_19;
                        }
                }
            }
            var_12 = var_10 << 3UL;
            var_13 = *(uint64_t *)((var_12 + var_11) + (-8L));
            rcx_0 = var_13;
            rsi2_0 = var_12;
            if ((long)var_13 <= (long)rsi) {
                rdi1_0 = var_12 + (-16L);
                *(uint64_t *)(rsi2_0 + rdx_1) = rcx_0;
                var_14 = rax_1 + (-1L);
                var_15 = *_pre_phi49;
                var_16 = *(uint64_t *)(rdi1_0 + var_15);
                rax_1 = var_14;
                rdx_1 = var_15;
                rcx_0 = var_16;
                rax_2 = var_14;
                while ((long)var_16 <= (long)rsi)
                    {
                        rdi1_0 = rdi1_0 + (-8L);
                        rsi2_0 = rsi2_0 + (-8L);
                        *(uint64_t *)(rsi2_0 + rdx_1) = rcx_0;
                        var_14 = rax_1 + (-1L);
                        var_15 = *_pre_phi49;
                        var_16 = *(uint64_t *)(rdi1_0 + var_15);
                        rax_1 = var_14;
                        rdx_1 = var_15;
                        rcx_0 = var_16;
                        rax_2 = var_14;
                    }
            }
            *(uint64_t *)((rax_2 << 3UL) + *_pre_phi49) = rsi;
            *var_4 = (*var_4 + 1UL);
        }
    }
    return rax_3;
}

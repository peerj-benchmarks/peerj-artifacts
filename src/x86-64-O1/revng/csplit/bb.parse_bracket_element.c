typedef unsigned char __u_char;
typedef unsigned short __u_short;
typedef unsigned int __u_int;
typedef unsigned long __u_long;
typedef signed char __int8_t;
typedef unsigned char __uint8_t;
typedef short __int16_t;
typedef unsigned short __uint16_t;
typedef int __int32_t;
typedef unsigned int __uint32_t;
typedef long __int64_t;
typedef unsigned long __uint64_t;
typedef __int8_t __int_least8_t;
typedef __uint8_t __uint_least8_t;
typedef __int16_t __int_least16_t;
typedef __uint16_t __uint_least16_t;
typedef __int32_t __int_least32_t;
typedef __uint32_t __uint_least32_t;
typedef __int64_t __int_least64_t;
typedef __uint64_t __uint_least64_t;
typedef long __quad_t;
typedef unsigned long __u_quad_t;
typedef long __intmax_t;
typedef unsigned long __uintmax_t;
typedef unsigned long __dev_t;
typedef unsigned int __uid_t;
typedef unsigned int __gid_t;
typedef unsigned long __ino_t;
typedef unsigned long __ino64_t;
typedef unsigned int __mode_t;
typedef unsigned long __nlink_t;
typedef long __off_t;
typedef long __off64_t;
typedef int __pid_t;
typedef struct {
    int __val[2];
} __fsid_t;
typedef long __clock_t;
typedef unsigned long __rlim_t;
typedef unsigned long __rlim64_t;
typedef unsigned int __id_t;
typedef long __time_t;
typedef unsigned int __useconds_t;
typedef long __suseconds_t;
typedef int __daddr_t;
typedef int __key_t;
typedef int __clockid_t;
typedef void *__timer_t;
typedef long __blksize_t;
typedef long __blkcnt_t;
typedef long __blkcnt64_t;
typedef unsigned long __fsblkcnt_t;
typedef unsigned long __fsblkcnt64_t;
typedef unsigned long __fsfilcnt_t;
typedef unsigned long __fsfilcnt64_t;
typedef long __fsword_t;
typedef long __ssize_t;
typedef long __syscall_slong_t;
typedef unsigned long __syscall_ulong_t;
typedef __off64_t __loff_t;
typedef char *__caddr_t;
typedef long __intptr_t;
typedef unsigned int __socklen_t;
typedef int __sig_atomic_t;
typedef __int8_t int8_t;
typedef __int16_t int16_t;
typedef __int32_t int32_t;
typedef __int64_t int64_t;
typedef __uint8_t uint8_t;
typedef __uint16_t uint16_t;
typedef __uint32_t uint32_t;
typedef __uint64_t uint64_t;
typedef __int_least8_t int_least8_t;
typedef __int_least16_t int_least16_t;
typedef __int_least32_t int_least32_t;
typedef __int_least64_t int_least64_t;
typedef __uint_least8_t uint_least8_t;
typedef __uint_least16_t uint_least16_t;
typedef __uint_least32_t uint_least32_t;
typedef __uint_least64_t uint_least64_t;
typedef signed char int_fast8_t;
typedef long int_fast16_t;
typedef long int_fast32_t;
typedef long int_fast64_t;
typedef unsigned char uint_fast8_t;
typedef unsigned long uint_fast16_t;
typedef unsigned long uint_fast32_t;
typedef unsigned long uint_fast64_t;
typedef long intptr_t;
typedef unsigned long uintptr_t;
typedef __intmax_t intmax_t;
typedef __uintmax_t uintmax_t;
typedef _Bool bool;
extern uint64_t revng_init_local_sp(uint64_t param_0);
extern uint64_t init_rbp(void);
extern uint64_t init_rbx(void);
extern uint64_t init_r12(void);
extern uint64_t init_r14(void);
extern uint64_t init_r13(void);
extern uint64_t init_r15(void);
extern uint64_t indirect_placeholder_5(uint64_t param_0, uint64_t param_1);
extern uint64_t indirect_placeholder_3(uint64_t param_0, uint64_t param_1, uint64_t param_2);
void bb_parse_bracket_element(uint64_t rdx, uint64_t rdi, uint64_t rcx, uint64_t rsi, uint64_t r9) {
    uint64_t var_0;
    uint64_t var_1;
    uint64_t var_2;
    uint64_t var_3;
    uint64_t var_4;
    uint64_t var_5;
    uint64_t var_6;
    uint64_t var_7;
    uint32_t var_8;
    uint64_t *var_9;
    uint64_t var_10;
    uint64_t var_11;
    uint64_t var_12;
    uint64_t var_52;
    uint64_t var_53;
    uint64_t var_13;
    unsigned char *var_14;
    unsigned char var_15;
    uint64_t var_50;
    uint64_t var_16;
    unsigned char var_17;
    uint64_t *var_18;
    uint64_t *var_19;
    unsigned char *var_20;
    unsigned char *var_21;
    uint64_t *var_22;
    uint64_t *var_23;
    uint64_t *var_24;
    uint64_t *var_25;
    uint64_t *var_26;
    uint64_t *var_27;
    uint64_t rbp_0;
    uint64_t local_sp_0;
    unsigned char r14_0_in;
    uint64_t var_46;
    uint64_t var_47;
    uint64_t var_48;
    unsigned char var_49;
    bool var_32;
    uint64_t var_33;
    uint64_t local_sp_1;
    uint64_t var_34;
    uint64_t var_35;
    unsigned char var_36;
    unsigned char var_37;
    uint64_t var_38;
    uint64_t var_39;
    unsigned char var_40;
    uint64_t var_41;
    uint64_t var_42;
    uint64_t var_43;
    uint64_t var_44;
    unsigned char var_45;
    uint64_t var_28;
    uint64_t var_29;
    uint64_t var_30;
    unsigned char var_31;
    unsigned char var_51;
    var_0 = revng_init_local_sp(0UL);
    var_1 = init_rbp();
    var_2 = init_rbx();
    var_3 = init_r14();
    var_4 = init_r13();
    var_5 = init_r12();
    var_6 = init_r15();
    *(uint64_t *)(var_0 + (-8L)) = var_6;
    *(uint64_t *)(var_0 + (-16L)) = var_3;
    *(uint64_t *)(var_0 + (-24L)) = var_4;
    *(uint64_t *)(var_0 + (-32L)) = var_5;
    *(uint64_t *)(var_0 + (-40L)) = var_1;
    *(uint64_t *)(var_0 + (-48L)) = var_2;
    *(uint32_t *)(var_0 + (-76L)) = (uint32_t)rcx;
    var_7 = var_0 + (-72L);
    *(uint64_t *)var_7 = r9;
    var_8 = *(uint32_t *)(var_0 | 8UL);
    var_9 = (uint64_t *)(rsi + 72UL);
    var_10 = *var_9;
    var_11 = var_0 + (-88L);
    *(uint64_t *)var_11 = 4225387UL;
    var_12 = indirect_placeholder_5(rsi, var_10);
    rbp_0 = 0UL;
    local_sp_0 = var_11;
    if ((int)(uint32_t)var_12 > (int)1U) {
        *(uint32_t *)rdi = 1U;
        var_52 = *var_9;
        *(uint64_t *)(var_0 + (-96L)) = 4225414UL;
        var_53 = indirect_placeholder_5(rsi, var_52);
        *(uint32_t *)(rdi + 8UL) = (uint32_t)var_53;
        *var_9 = (*var_9 + (uint64_t)((long)(var_12 << 32UL) >> (long)32UL));
    } else {
        var_13 = var_10 + (uint64_t)*(uint32_t *)(var_0 + (-84L));
        *var_9 = var_13;
        var_14 = (unsigned char *)(rdx + 8UL);
        var_15 = *var_14;
        if ((uint64_t)((var_15 & '\xfb') + '\xe6') != 0UL) {
            if ((uint64_t)(var_15 + '\xe4') != 0UL) {
                if ((uint32_t)(unsigned char)var_8 != 0U) {
                    var_16 = *(uint64_t *)(var_0 + (-80L));
                    *(uint64_t *)(var_0 + (-96L)) = 4225891UL;
                    indirect_placeholder_3(var_16, var_7, rsi);
                    if ((uint64_t)(var_15 + '\xea') != 0UL & *(unsigned char *)var_7 == '\x15') {
                        return;
                    }
                }
                *(uint32_t *)rdi = 0U;
                *(unsigned char *)(rdi + 8UL) = *(unsigned char *)rdx;
                return;
            }
        }
        var_17 = *(unsigned char *)rdx;
        var_18 = (uint64_t *)(rsi + 104UL);
        if ((long)var_13 >= (long)*var_18) {
            var_19 = (uint64_t *)(rsi + 8UL);
            var_20 = (unsigned char *)(rsi + 139UL);
            var_21 = (unsigned char *)(rsi + 140UL);
            var_22 = (uint64_t *)(rsi + 48UL);
            var_23 = (uint64_t *)(rsi + 16UL);
            var_24 = (uint64_t *)(rsi + 24UL);
            var_25 = (uint64_t *)rsi;
            var_26 = (uint64_t *)(rsi + 40UL);
            var_27 = (uint64_t *)(rdi + 8UL);
            while (1U)
                {
                    *(uint32_t *)(local_sp_0 + 4UL) = (uint32_t)rbp_0;
                    local_sp_1 = local_sp_0;
                    if (*var_14 != '\x1e') {
                        var_28 = *var_19;
                        var_29 = *var_9;
                        var_30 = var_29 + 1UL;
                        *var_9 = var_30;
                        var_31 = *(unsigned char *)(var_29 + var_28);
                        var_50 = var_30;
                        r14_0_in = var_31;
                        local_sp_0 = local_sp_1;
                        if ((long)*var_18 > (long)var_50) {
                            break;
                        }
                        if ((uint64_t)(var_17 - r14_0_in) == 0UL) {
                            *(unsigned char *)(rbp_0 + *var_27) = r14_0_in;
                            if (rbp_0 == 31UL) {
                                break;
                            }
                            rbp_0 = rbp_0 + 1UL;
                            continue;
                        }
                        if (*(unsigned char *)(var_50 + *var_19) != ']') {
                            *var_9 = (var_50 + 1UL);
                            *(unsigned char *)(*var_27 + (uint64_t)*(uint32_t *)(local_sp_1 + 4UL)) = (unsigned char)'\x00';
                            var_51 = *var_14;
                            if ((uint64_t)(var_51 + '\xe4') != 0UL) {
                                *(uint32_t *)rdi = 2U;
                                break;
                            }
                            if ((uint64_t)(var_51 + '\xe2') == 0UL) {
                                *(uint32_t *)rdi = 4U;
                                break;
                            }
                            if ((uint64_t)(var_51 + '\xe6') == 0UL) {
                                break;
                            }
                            *(uint32_t *)rdi = 3U;
                            break;
                        }
                    }
                    if (*var_20 == '\x00') {
                        var_46 = *var_19;
                        var_47 = *var_9;
                        var_48 = var_47 + 1UL;
                        *var_9 = var_48;
                        var_49 = *(unsigned char *)(var_47 + var_46);
                        var_50 = var_48;
                        r14_0_in = var_49;
                    } else {
                        var_32 = (*var_21 == '\x00');
                        var_33 = *var_9;
                        if (var_32) {
                            var_44 = var_33 + 1UL;
                            *var_9 = var_44;
                            var_45 = *(unsigned char *)(var_33 + (*var_25 + *var_26));
                            var_50 = var_44;
                            r14_0_in = var_45;
                        } else {
                            *(uint64_t *)(local_sp_0 + 8UL) = var_33;
                            if (var_33 != *var_22) {
                                if (*(uint32_t *)((var_33 << 2UL) + *var_23) != 4294967295U) {
                                    var_34 = *var_19;
                                    var_35 = var_33 + 1UL;
                                    *var_9 = var_35;
                                    var_36 = *(unsigned char *)(var_33 + var_34);
                                    var_50 = var_35;
                                    r14_0_in = var_36;
                                    local_sp_0 = local_sp_1;
                                    if ((long)*var_18 > (long)var_50) {
                                        break;
                                    }
                                    if ((uint64_t)(var_17 - r14_0_in) == 0UL) {
                                        *(unsigned char *)(rbp_0 + *var_27) = r14_0_in;
                                        if (rbp_0 == 31UL) {
                                            break;
                                        }
                                        rbp_0 = rbp_0 + 1UL;
                                        continue;
                                    }
                                    if (*(unsigned char *)(var_50 + *var_19) != ']') {
                                        *var_9 = (var_50 + 1UL);
                                        *(unsigned char *)(*var_27 + (uint64_t)*(uint32_t *)(local_sp_1 + 4UL)) = (unsigned char)'\x00';
                                        var_51 = *var_14;
                                        if ((uint64_t)(var_51 + '\xe4') != 0UL) {
                                            *(uint32_t *)rdi = 2U;
                                            break;
                                        }
                                        if ((uint64_t)(var_51 + '\xe2') == 0UL) {
                                            *(uint32_t *)rdi = 4U;
                                            break;
                                        }
                                        if ((uint64_t)(var_51 + '\xe6') == 0UL) {
                                            break;
                                        }
                                        *(uint32_t *)rdi = 3U;
                                        break;
                                    }
                                }
                            }
                            var_37 = *(unsigned char *)((*(uint64_t *)((var_33 << 3UL) + *var_24) + *var_25) + *var_26);
                            r14_0_in = var_37;
                            if ((signed char)var_37 > '\xff') {
                                var_41 = local_sp_0 + (-8L);
                                *(uint64_t *)var_41 = 4225663UL;
                                var_42 = indirect_placeholder_5(rsi, var_33);
                                var_43 = (uint64_t)((long)(var_42 << 32UL) >> (long)32UL) + *(uint64_t *)local_sp_0;
                                *var_9 = var_43;
                                var_50 = var_43;
                                local_sp_1 = var_41;
                            } else {
                                var_38 = *var_19;
                                var_39 = var_33 + 1UL;
                                *var_9 = var_39;
                                var_40 = *(unsigned char *)(var_33 + var_38);
                                var_50 = var_39;
                                r14_0_in = var_40;
                            }
                        }
                    }
                }
        }
    }
}

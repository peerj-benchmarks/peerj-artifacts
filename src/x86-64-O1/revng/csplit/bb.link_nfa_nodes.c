typedef unsigned char __u_char;
typedef unsigned short __u_short;
typedef unsigned int __u_int;
typedef unsigned long __u_long;
typedef signed char __int8_t;
typedef unsigned char __uint8_t;
typedef short __int16_t;
typedef unsigned short __uint16_t;
typedef int __int32_t;
typedef unsigned int __uint32_t;
typedef long __int64_t;
typedef unsigned long __uint64_t;
typedef __int8_t __int_least8_t;
typedef __uint8_t __uint_least8_t;
typedef __int16_t __int_least16_t;
typedef __uint16_t __uint_least16_t;
typedef __int32_t __int_least32_t;
typedef __uint32_t __uint_least32_t;
typedef __int64_t __int_least64_t;
typedef __uint64_t __uint_least64_t;
typedef long __quad_t;
typedef unsigned long __u_quad_t;
typedef long __intmax_t;
typedef unsigned long __uintmax_t;
typedef unsigned long __dev_t;
typedef unsigned int __uid_t;
typedef unsigned int __gid_t;
typedef unsigned long __ino_t;
typedef unsigned long __ino64_t;
typedef unsigned int __mode_t;
typedef unsigned long __nlink_t;
typedef long __off_t;
typedef long __off64_t;
typedef int __pid_t;
typedef struct {
    int __val[2];
} __fsid_t;
typedef long __clock_t;
typedef unsigned long __rlim_t;
typedef unsigned long __rlim64_t;
typedef unsigned int __id_t;
typedef long __time_t;
typedef unsigned int __useconds_t;
typedef long __suseconds_t;
typedef int __daddr_t;
typedef int __key_t;
typedef int __clockid_t;
typedef void *__timer_t;
typedef long __blksize_t;
typedef long __blkcnt_t;
typedef long __blkcnt64_t;
typedef unsigned long __fsblkcnt_t;
typedef unsigned long __fsblkcnt64_t;
typedef unsigned long __fsfilcnt_t;
typedef unsigned long __fsfilcnt64_t;
typedef long __fsword_t;
typedef long __ssize_t;
typedef long __syscall_slong_t;
typedef unsigned long __syscall_ulong_t;
typedef __off64_t __loff_t;
typedef char *__caddr_t;
typedef long __intptr_t;
typedef unsigned int __socklen_t;
typedef int __sig_atomic_t;
typedef __int8_t int8_t;
typedef __int16_t int16_t;
typedef __int32_t int32_t;
typedef __int64_t int64_t;
typedef __uint8_t uint8_t;
typedef __uint16_t uint16_t;
typedef __uint32_t uint32_t;
typedef __uint64_t uint64_t;
typedef __int_least8_t int_least8_t;
typedef __int_least16_t int_least16_t;
typedef __int_least32_t int_least32_t;
typedef __int_least64_t int_least64_t;
typedef __uint_least8_t uint_least8_t;
typedef __uint_least16_t uint_least16_t;
typedef __uint_least32_t uint_least32_t;
typedef __uint_least64_t uint_least64_t;
typedef signed char int_fast8_t;
typedef long int_fast16_t;
typedef long int_fast32_t;
typedef long int_fast64_t;
typedef unsigned char uint_fast8_t;
typedef unsigned long uint_fast16_t;
typedef unsigned long uint_fast32_t;
typedef unsigned long uint_fast64_t;
typedef long intptr_t;
typedef unsigned long uintptr_t;
typedef __intmax_t intmax_t;
typedef __uintmax_t uintmax_t;
typedef _Bool bool;
extern void abort(void);
extern uint64_t revng_init_local_sp(uint64_t param_0);
extern uint64_t init_rbp(void);
extern uint64_t init_rbx(void);
extern uint64_t init_r12(void);
extern void indirect_placeholder(void);
extern uint64_t init_r14(void);
extern uint64_t init_r13(void);
extern uint64_t indirect_placeholder_4(uint64_t param_0);
extern uint64_t indirect_placeholder_5(uint64_t param_0, uint64_t param_1);
void bb_link_nfa_nodes(uint64_t rdi, uint64_t rsi) {
    uint64_t var_0;
    uint64_t var_1;
    uint64_t var_2;
    uint64_t var_3;
    uint64_t var_4;
    uint64_t var_5;
    uint64_t var_6;
    uint64_t var_7;
    unsigned char *var_8;
    unsigned char var_9;
    uint64_t var_10;
    uint64_t local_sp_0;
    bool var_30;
    uint64_t *var_31;
    bool var_32;
    uint64_t *var_33;
    uint64_t var_11;
    uint64_t *var_12;
    uint64_t var_13;
    uint64_t var_14;
    uint64_t var_15;
    uint64_t var_16;
    uint64_t var_17;
    uint64_t var_18;
    unsigned char *var_19;
    uint64_t var_20;
    bool var_21;
    uint64_t var_22;
    uint64_t var_23;
    uint64_t storemerge;
    uint64_t var_24;
    uint64_t storemerge1;
    uint64_t var_25;
    uint64_t local_sp_1;
    uint64_t var_26;
    uint64_t var_27;
    uint64_t var_28;
    uint64_t *var_29;
    var_0 = revng_init_local_sp(0UL);
    var_1 = init_rbp();
    var_2 = init_rbx();
    var_3 = init_r14();
    var_4 = init_r13();
    var_5 = init_r12();
    *(uint64_t *)(var_0 + (-8L)) = var_3;
    *(uint64_t *)(var_0 + (-16L)) = var_4;
    *(uint64_t *)(var_0 + (-24L)) = var_5;
    *(uint64_t *)(var_0 + (-32L)) = var_1;
    var_6 = var_0 + (-40L);
    *(uint64_t *)var_6 = var_2;
    var_7 = *(uint64_t *)(rsi + 56UL);
    var_8 = (unsigned char *)(rsi + 48UL);
    var_9 = *var_8;
    var_10 = (uint64_t)var_9;
    local_sp_0 = var_6;
    if (var_9 <= '\x10') {
        if ((var_10 & 8UL) == 0UL) {
            *(uint64_t *)(var_0 + (-48L)) = 4239353UL;
            indirect_placeholder();
        }
        *(uint64_t *)((var_7 << 3UL) + *(uint64_t *)(rdi + 24UL)) = *(uint64_t *)(*(uint64_t *)(rsi + 32UL) + 56UL);
        return;
    }
    switch (*(uint64_t *)((var_10 << 3UL) + 4319896UL)) {
      case 4239241UL:
        {
            var_17 = *(uint64_t *)(*(uint64_t *)(rsi + 32UL) + 56UL);
            var_18 = (var_7 * 24UL) + *(uint64_t *)(rdi + 40UL);
            *(uint64_t *)(var_0 + (-48L)) = 4239267UL;
            indirect_placeholder_5(var_18, var_17);
        }
        break;
      case 4239377UL:
        {
            return;
        }
        break;
      case 4238944UL:
        {
            if (*(uint64_t *)(rsi + 32UL) == 0UL) {
                *(uint64_t *)(var_0 + (-48L)) = 4238985UL;
                indirect_placeholder();
            }
        }
        break;
      case 4238995UL:
        {
            var_19 = (unsigned char *)(rdi + 176UL);
            *var_19 = (*var_19 | '\x01');
            var_20 = *(uint64_t *)(rsi + 8UL);
            var_21 = (var_20 == 0UL);
            var_22 = var_20 + 24UL;
            var_23 = rsi + 32UL;
            storemerge = *(uint64_t *)(*(uint64_t *)(var_21 ? var_23 : var_22) + 56UL);
            var_24 = *(uint64_t *)(rsi + 16UL);
            storemerge1 = *(uint64_t *)(*(uint64_t *)((var_24 == 0UL) ? var_23 : (var_24 + 24UL)) + 56UL);
            if ((long)storemerge > (long)18446744073709551615UL) {
                var_25 = var_0 + (-48L);
                *(uint64_t *)var_25 = 4239086UL;
                indirect_placeholder();
                local_sp_0 = var_25;
            }
            local_sp_1 = local_sp_0;
            if ((long)storemerge1 > (long)18446744073709551615UL) {
                var_26 = local_sp_0 + (-8L);
                *(uint64_t *)var_26 = 4239116UL;
                indirect_placeholder();
                local_sp_1 = var_26;
            }
            var_27 = (var_7 * 24UL) + *(uint64_t *)(rdi + 40UL);
            *(uint64_t *)var_27 = 2UL;
            *(uint64_t *)(local_sp_1 + (-8L)) = 4239147UL;
            var_28 = indirect_placeholder_4(16UL);
            var_29 = (uint64_t *)(var_27 + 16UL);
            *var_29 = var_28;
            if (var_28 != 0UL) {
                var_30 = (storemerge == storemerge1);
                var_31 = (uint64_t *)(var_27 + 8UL);
                if (var_30) {
                    *var_31 = 1UL;
                    *(uint64_t *)var_28 = storemerge;
                } else {
                    *var_31 = 2UL;
                    var_32 = ((long)storemerge < (long)storemerge1);
                    var_33 = (uint64_t *)var_28;
                    if (var_32) {
                        *var_33 = storemerge;
                        *(uint64_t *)(*var_29 + 8UL) = storemerge1;
                    } else {
                        *var_33 = storemerge1;
                        *(uint64_t *)(*var_29 + 8UL) = storemerge;
                    }
                }
            }
        }
        break;
      case 4239269UL:
        {
            var_11 = *(uint64_t *)(*(uint64_t *)(rsi + 32UL) + 56UL);
            var_12 = (uint64_t *)(rdi + 24UL);
            var_13 = *var_12;
            var_14 = var_7 << 3UL;
            *(uint64_t *)(var_14 + var_13) = var_11;
            if (*var_8 == '\x04') {
                var_15 = *(uint64_t *)(var_14 + *var_12);
                var_16 = (var_7 * 24UL) + *(uint64_t *)(rdi + 40UL);
                *(uint64_t *)(var_0 + (-48L)) = 4239322UL;
                indirect_placeholder_5(var_16, var_15);
            }
        }
        break;
      case 4239324UL:
        {
            if ((var_10 & 8UL) != 0UL) {
                *(uint64_t *)(var_0 + (-48L)) = 4239353UL;
                indirect_placeholder();
            }
            *(uint64_t *)((var_7 << 3UL) + *(uint64_t *)(rdi + 24UL)) = *(uint64_t *)(*(uint64_t *)(rsi + 32UL) + 56UL);
        }
        break;
      default:
        {
            abort();
        }
        break;
    }
}

typedef unsigned char __u_char;
typedef unsigned short __u_short;
typedef unsigned int __u_int;
typedef unsigned long __u_long;
typedef signed char __int8_t;
typedef unsigned char __uint8_t;
typedef short __int16_t;
typedef unsigned short __uint16_t;
typedef int __int32_t;
typedef unsigned int __uint32_t;
typedef long __int64_t;
typedef unsigned long __uint64_t;
typedef __int8_t __int_least8_t;
typedef __uint8_t __uint_least8_t;
typedef __int16_t __int_least16_t;
typedef __uint16_t __uint_least16_t;
typedef __int32_t __int_least32_t;
typedef __uint32_t __uint_least32_t;
typedef __int64_t __int_least64_t;
typedef __uint64_t __uint_least64_t;
typedef long __quad_t;
typedef unsigned long __u_quad_t;
typedef long __intmax_t;
typedef unsigned long __uintmax_t;
typedef unsigned long __dev_t;
typedef unsigned int __uid_t;
typedef unsigned int __gid_t;
typedef unsigned long __ino_t;
typedef unsigned long __ino64_t;
typedef unsigned int __mode_t;
typedef unsigned long __nlink_t;
typedef long __off_t;
typedef long __off64_t;
typedef int __pid_t;
typedef struct {
    int __val[2];
} __fsid_t;
typedef long __clock_t;
typedef unsigned long __rlim_t;
typedef unsigned long __rlim64_t;
typedef unsigned int __id_t;
typedef long __time_t;
typedef unsigned int __useconds_t;
typedef long __suseconds_t;
typedef int __daddr_t;
typedef int __key_t;
typedef int __clockid_t;
typedef void *__timer_t;
typedef long __blksize_t;
typedef long __blkcnt_t;
typedef long __blkcnt64_t;
typedef unsigned long __fsblkcnt_t;
typedef unsigned long __fsblkcnt64_t;
typedef unsigned long __fsfilcnt_t;
typedef unsigned long __fsfilcnt64_t;
typedef long __fsword_t;
typedef long __ssize_t;
typedef long __syscall_slong_t;
typedef unsigned long __syscall_ulong_t;
typedef __off64_t __loff_t;
typedef char *__caddr_t;
typedef long __intptr_t;
typedef unsigned int __socklen_t;
typedef int __sig_atomic_t;
typedef __int8_t int8_t;
typedef __int16_t int16_t;
typedef __int32_t int32_t;
typedef __int64_t int64_t;
typedef __uint8_t uint8_t;
typedef __uint16_t uint16_t;
typedef __uint32_t uint32_t;
typedef __uint64_t uint64_t;
typedef __int_least8_t int_least8_t;
typedef __int_least16_t int_least16_t;
typedef __int_least32_t int_least32_t;
typedef __int_least64_t int_least64_t;
typedef __uint_least8_t uint_least8_t;
typedef __uint_least16_t uint_least16_t;
typedef __uint_least32_t uint_least32_t;
typedef __uint_least64_t uint_least64_t;
typedef signed char int_fast8_t;
typedef long int_fast16_t;
typedef long int_fast32_t;
typedef long int_fast64_t;
typedef unsigned char uint_fast8_t;
typedef unsigned long uint_fast16_t;
typedef unsigned long uint_fast32_t;
typedef unsigned long uint_fast64_t;
typedef long intptr_t;
typedef unsigned long uintptr_t;
typedef __intmax_t intmax_t;
typedef __uintmax_t uintmax_t;
typedef _Bool bool;
extern uint64_t revng_init_local_sp(uint64_t param_0);
extern uint64_t init_rbp(void);
extern uint64_t init_rbx(void);
extern uint64_t init_r12(void);
extern uint64_t init_cc_src2(void);
extern uint64_t helper_cc_compute_c_wrapper(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint32_t param_3);
extern uint64_t init_r14(void);
extern uint64_t init_r13(void);
extern uint64_t helper_cc_compute_all_wrapper(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint32_t param_3);
extern uint64_t init_r15(void);
extern uint64_t indirect_placeholder_8(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint64_t param_3);
uint64_t bb_lower_subexp(uint64_t rdx, uint64_t rdi, uint64_t rsi) {
    uint64_t local_sp_0;
    uint64_t var_0;
    uint64_t var_1;
    uint64_t var_2;
    uint64_t var_3;
    uint64_t var_4;
    uint64_t var_5;
    uint64_t var_6;
    uint64_t var_7;
    uint64_t var_8;
    uint64_t var_9;
    uint64_t var_10;
    bool var_11;
    uint64_t var_24;
    uint64_t var_25;
    unsigned char var_26;
    unsigned char *var_27;
    unsigned char *var_28;
    uint64_t rax_0;
    uint64_t r15_0;
    uint64_t var_20;
    uint64_t var_21;
    uint64_t *var_22;
    uint64_t var_23;
    uint64_t var_12;
    uint64_t var_13;
    uint64_t var_14;
    uint64_t var_15;
    uint64_t var_16;
    uint64_t var_17;
    uint64_t var_18;
    uint64_t var_19;
    var_0 = revng_init_local_sp(0UL);
    var_1 = init_cc_src2();
    var_2 = init_rbp();
    var_3 = init_rbx();
    var_4 = init_r14();
    var_5 = init_r13();
    var_6 = init_r12();
    var_7 = init_r15();
    *(uint64_t *)(var_0 + (-8L)) = var_7;
    *(uint64_t *)(var_0 + (-16L)) = var_4;
    *(uint64_t *)(var_0 + (-24L)) = var_5;
    *(uint64_t *)(var_0 + (-32L)) = var_6;
    *(uint64_t *)(var_0 + (-40L)) = var_2;
    *(uint64_t *)(var_0 + (-48L)) = var_3;
    var_8 = var_0 + (-72L);
    var_9 = *(uint64_t *)rsi;
    var_10 = *(uint64_t *)(rdx + 8UL);
    var_11 = (var_10 != 0UL);
    rax_0 = var_10;
    if (!(((*(unsigned char *)(rsi + 56UL) & '\x10') == '\x00') || (var_11 ^ 1))) {
        var_12 = *(uint64_t *)(rdx + 40UL);
        var_13 = var_12 + (-63L);
        if ((long)var_12 > (long)63UL) {
            return rax_0;
        }
        var_14 = *(uint64_t *)(var_9 + 160UL) >> (var_12 & 63UL);
        var_15 = helper_cc_compute_all_wrapper(var_13, 63UL, var_1, 17U);
        var_16 = helper_cc_compute_c_wrapper(var_13, (var_15 & (-2L)) | (var_14 & 1UL), var_1, 1U);
        if (var_16 == 0UL) {
            return rax_0;
        }
    }
    *(uint64_t *)var_8 = rdx;
    *(uint64_t *)(var_0 + (-64L)) = rdi;
    *(uint64_t *)(var_0 + (-80L)) = 4226903UL;
    var_17 = indirect_placeholder_8(0UL, var_9, 8UL, 0UL);
    var_18 = var_0 + (-88L);
    *(uint64_t *)var_18 = 4226929UL;
    var_19 = indirect_placeholder_8(0UL, var_9, 9UL, 0UL);
    local_sp_0 = var_18;
    r15_0 = var_19;
    if (var_11) {
        var_20 = var_0 + (-96L);
        *(uint64_t *)var_20 = 4226959UL;
        var_21 = indirect_placeholder_8(var_19, var_9, 16UL, var_10);
        local_sp_0 = var_20;
        r15_0 = var_21;
    }
    var_22 = (uint64_t *)(local_sp_0 + (-8L));
    *var_22 = 4226981UL;
    var_23 = indirect_placeholder_8(r15_0, var_9, 16UL, var_17);
    rax_0 = var_23;
    if ((((var_23 == 0UL) || (r15_0 == 0UL)) || (var_17 == 0UL)) || (var_19 == 0UL)) {
        **(uint32_t **)local_sp_0 = 12U;
        rax_0 = 0UL;
    } else {
        var_24 = *var_22;
        var_25 = *(uint64_t *)(var_24 + 40UL);
        *(uint64_t *)(var_19 + 40UL) = var_25;
        *(uint64_t *)(var_17 + 40UL) = var_25;
        var_26 = *(unsigned char *)(var_24 + 50UL) & '\b';
        var_27 = (unsigned char *)(var_19 + 50UL);
        *var_27 = (var_26 | (*var_27 & '\xf7'));
        var_28 = (unsigned char *)(var_17 + 50UL);
        *var_28 = (var_26 | (*var_28 & '\xf7'));
    }
    return rax_0;
}

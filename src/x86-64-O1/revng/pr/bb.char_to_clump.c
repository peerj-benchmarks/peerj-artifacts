typedef unsigned char __u_char;
typedef unsigned short __u_short;
typedef unsigned int __u_int;
typedef unsigned long __u_long;
typedef signed char __int8_t;
typedef unsigned char __uint8_t;
typedef short __int16_t;
typedef unsigned short __uint16_t;
typedef int __int32_t;
typedef unsigned int __uint32_t;
typedef long __int64_t;
typedef unsigned long __uint64_t;
typedef __int8_t __int_least8_t;
typedef __uint8_t __uint_least8_t;
typedef __int16_t __int_least16_t;
typedef __uint16_t __uint_least16_t;
typedef __int32_t __int_least32_t;
typedef __uint32_t __uint_least32_t;
typedef __int64_t __int_least64_t;
typedef __uint64_t __uint_least64_t;
typedef long __quad_t;
typedef unsigned long __u_quad_t;
typedef long __intmax_t;
typedef unsigned long __uintmax_t;
typedef unsigned long __dev_t;
typedef unsigned int __uid_t;
typedef unsigned int __gid_t;
typedef unsigned long __ino_t;
typedef unsigned long __ino64_t;
typedef unsigned int __mode_t;
typedef unsigned long __nlink_t;
typedef long __off_t;
typedef long __off64_t;
typedef int __pid_t;
typedef struct {
    int __val[2];
} __fsid_t;
typedef long __clock_t;
typedef unsigned long __rlim_t;
typedef unsigned long __rlim64_t;
typedef unsigned int __id_t;
typedef long __time_t;
typedef unsigned int __useconds_t;
typedef long __suseconds_t;
typedef int __daddr_t;
typedef int __key_t;
typedef int __clockid_t;
typedef void *__timer_t;
typedef long __blksize_t;
typedef long __blkcnt_t;
typedef long __blkcnt64_t;
typedef unsigned long __fsblkcnt_t;
typedef unsigned long __fsblkcnt64_t;
typedef unsigned long __fsfilcnt_t;
typedef unsigned long __fsfilcnt64_t;
typedef long __fsword_t;
typedef long __ssize_t;
typedef long __syscall_slong_t;
typedef unsigned long __syscall_ulong_t;
typedef __off64_t __loff_t;
typedef char *__caddr_t;
typedef long __intptr_t;
typedef unsigned int __socklen_t;
typedef int __sig_atomic_t;
typedef __int8_t int8_t;
typedef __int16_t int16_t;
typedef __int32_t int32_t;
typedef __int64_t int64_t;
typedef __uint8_t uint8_t;
typedef __uint16_t uint16_t;
typedef __uint32_t uint32_t;
typedef __uint64_t uint64_t;
typedef __int_least8_t int_least8_t;
typedef __int_least16_t int_least16_t;
typedef __int_least32_t int_least32_t;
typedef __int_least64_t int_least64_t;
typedef __uint_least8_t uint_least8_t;
typedef __uint_least16_t uint_least16_t;
typedef __uint_least32_t uint_least32_t;
typedef __uint_least64_t uint_least64_t;
typedef signed char int_fast8_t;
typedef long int_fast16_t;
typedef long int_fast32_t;
typedef long int_fast64_t;
typedef unsigned char uint_fast8_t;
typedef unsigned long uint_fast16_t;
typedef unsigned long uint_fast32_t;
typedef unsigned long uint_fast64_t;
typedef long intptr_t;
typedef unsigned long uintptr_t;
typedef __intmax_t intmax_t;
typedef __uintmax_t uintmax_t;
typedef _Bool bool;
struct bb_char_to_clump_ret_type;
struct helper_idivl_EAX_wrapper_ret_type;
struct type_6;
struct bb_char_to_clump_ret_type {
    uint64_t field_0;
    uint64_t field_1;
    uint64_t field_2;
    uint64_t field_3;
    uint64_t field_4;
    uint64_t field_5;
};
struct helper_idivl_EAX_wrapper_ret_type {
    uint64_t field_0;
    uint64_t field_1;
    uint64_t field_2;
    uint64_t field_3;
    uint32_t field_4;
    uint32_t field_5;
    uint64_t field_6;
    uint32_t field_7;
    uint64_t field_8;
    uint32_t field_9;
    unsigned char field_10;
    uint32_t field_11;
    uint32_t field_12;
};
struct type_6 {
};
extern uint64_t revng_init_local_sp(uint64_t param_0);
extern uint64_t init_rbx(void);
extern void indirect_placeholder(void);
extern uint32_t init_state_0x8248(void);
extern uint64_t init_state_0x9018(void);
extern uint32_t init_state_0x9010(void);
extern uint64_t init_state_0x8408(void);
extern uint64_t init_state_0x8328(void);
extern uint64_t init_state_0x82d8(void);
extern uint32_t init_state_0x9080(void);
extern struct helper_idivl_EAX_wrapper_ret_type helper_idivl_EAX_wrapper(struct type_6 *param_0, uint64_t param_1, uint64_t param_2, uint64_t param_3, uint64_t param_4, uint64_t param_5, uint64_t param_6, uint64_t param_7, uint64_t param_8, uint64_t param_9, uint64_t param_10, uint64_t param_11, uint64_t param_12, uint32_t param_13, uint64_t param_14, uint32_t param_15, uint64_t param_16, uint64_t param_17, uint64_t param_18, uint32_t param_19);
struct bb_char_to_clump_ret_type bb_char_to_clump(uint64_t rbp, uint64_t rdi, uint64_t r10, uint64_t rsi, uint64_t r9, uint64_t r8) {
    uint64_t var_0;
    uint64_t var_1;
    uint32_t var_2;
    uint64_t var_3;
    uint32_t var_4;
    uint64_t var_5;
    uint64_t var_6;
    uint64_t var_7;
    uint32_t var_8;
    uint64_t var_9;
    uint64_t var_10;
    unsigned char var_11;
    unsigned char var_12;
    uint64_t rcx_0;
    uint64_t var_13;
    uint64_t rcx_2;
    bool var_14;
    unsigned char *var_15;
    uint64_t var_16;
    bool var_17;
    unsigned char *var_18;
    uint64_t rcx_1;
    uint64_t var_19;
    uint32_t var_21;
    struct helper_idivl_EAX_wrapper_ret_type var_20;
    uint64_t var_22;
    uint64_t var_23;
    uint64_t rbx_0;
    uint64_t storemerge;
    uint64_t rax_0;
    uint32_t var_24;
    uint64_t rsi9_1;
    uint64_t var_25;
    uint64_t var_26;
    uint64_t var_27;
    uint64_t rsi9_0;
    uint64_t rax_1;
    uint64_t rax_2;
    struct bb_char_to_clump_ret_type mrv;
    struct bb_char_to_clump_ret_type mrv1;
    struct bb_char_to_clump_ret_type mrv2;
    struct bb_char_to_clump_ret_type mrv3;
    struct bb_char_to_clump_ret_type mrv4;
    struct bb_char_to_clump_ret_type mrv5;
    var_0 = revng_init_local_sp(0UL);
    var_1 = init_rbx();
    var_2 = init_state_0x8248();
    var_3 = init_state_0x9018();
    var_4 = init_state_0x9010();
    var_5 = init_state_0x8408();
    var_6 = init_state_0x8328();
    var_7 = init_state_0x82d8();
    var_8 = init_state_0x9080();
    *(uint64_t *)(var_0 + (-8L)) = var_1;
    var_9 = *(uint64_t *)6400616UL;
    var_10 = (uint64_t)*(uint32_t *)6400244UL;
    var_11 = *(unsigned char *)6400248UL;
    var_12 = (unsigned char)rdi;
    rcx_0 = var_10;
    rcx_2 = 4UL;
    rcx_1 = 4294967295UL;
    rbx_0 = var_9;
    storemerge = 1UL;
    rax_0 = 1UL;
    rsi9_1 = rsi;
    rsi9_0 = rsi;
    rax_1 = 0UL;
    rax_2 = 0UL;
    if ((uint64_t)(var_12 - var_11) == 0UL) {
        var_19 = (uint64_t)*(uint32_t *)6400756UL;
        var_20 = helper_idivl_EAX_wrapper((struct type_6 *)(0UL), rcx_0, 4203660UL, var_19, (uint64_t)(uint32_t)(uint64_t)((long)(var_19 << 32UL) >> (long)63UL), var_9, rbp, rdi, rcx_0, r10, rsi, r9, r8, var_2, var_3, var_4, var_5, var_6, var_7, var_8);
        var_21 = (uint32_t)rcx_0 - (uint32_t)var_20.field_2;
        var_22 = (uint64_t)var_21;
        rcx_1 = var_22;
        rcx_2 = var_22;
        if (*(unsigned char *)6400773UL != '\x00') {
            storemerge = var_22;
            rcx_2 = 0UL;
            if (var_22 != 0UL) {
                *(uint32_t *)6400756UL = (*(uint32_t *)6400756UL + (uint32_t)rcx_2);
                rsi9_1 = rsi9_0;
                rax_2 = rax_1;
                mrv.field_0 = rax_2;
                mrv1 = mrv;
                mrv1.field_1 = rbp;
                mrv2 = mrv1;
                mrv2.field_2 = r10;
                mrv3 = mrv2;
                mrv3.field_3 = rsi9_1;
                mrv4 = mrv3;
                mrv4.field_4 = r9;
                mrv5 = mrv4;
                mrv5.field_5 = r8;
                return mrv5;
            }
            var_23 = (uint64_t)(var_21 + (-1)) + var_9;
            *(unsigned char *)rbx_0 = (unsigned char)' ';
            while (rbx_0 != var_23)
                {
                    rbx_0 = rbx_0 + 1UL;
                    *(unsigned char *)rbx_0 = (unsigned char)' ';
                }
        }
        *(unsigned char *)var_9 = var_12;
        rax_0 = storemerge;
        rax_1 = storemerge;
        if ((int)var_21 <= (int)4294967295U) {
            *(uint32_t *)6400756UL = (*(uint32_t *)6400756UL + (uint32_t)rcx_2);
            rsi9_1 = rsi9_0;
            rax_2 = rax_1;
            mrv.field_0 = rax_2;
            mrv1 = mrv;
            mrv1.field_1 = rbp;
            mrv2 = mrv1;
            mrv2.field_2 = r10;
            mrv3 = mrv2;
            mrv3.field_3 = rsi9_1;
            mrv4 = mrv3;
            mrv4.field_4 = r9;
            mrv5 = mrv4;
            mrv5.field_5 = r8;
            return mrv5;
        }
        var_24 = *(uint32_t *)6400756UL;
        rcx_2 = rcx_1;
        rax_1 = rax_0;
        if (var_24 != 0U) {
            var_25 = (uint64_t)var_24;
            var_26 = 0UL - rcx_1;
            var_27 = (uint64_t)(uint32_t)var_26;
            rsi9_0 = var_27;
            rsi9_1 = var_27;
            rax_2 = rax_0;
            if ((long)(var_25 << 32UL) > (long)(var_26 << 32UL)) {
                *(uint32_t *)6400756UL = (*(uint32_t *)6400756UL + (uint32_t)rcx_2);
                rsi9_1 = rsi9_0;
                rax_2 = rax_1;
            } else {
                *(uint32_t *)6400756UL = 0U;
            }
        }
        mrv.field_0 = rax_2;
        mrv1 = mrv;
        mrv1.field_1 = rbp;
        mrv2 = mrv1;
        mrv2.field_2 = r10;
        mrv3 = mrv2;
        mrv3.field_3 = rsi9_1;
        mrv4 = mrv3;
        mrv4.field_4 = r9;
        mrv5 = mrv4;
        mrv5.field_5 = r8;
        return mrv5;
    }
    rcx_0 = 8UL;
    rsi9_0 = 4273939UL;
    rax_1 = 1UL;
    if ((uint64_t)(var_12 + '\xf7') == 0UL) {
        return;
    }
    if ((uint64_t)((uint32_t)(uint64_t)var_12 + (-32)) <= 94UL) {
        *(unsigned char *)var_9 = var_12;
        rcx_2 = 1UL;
        *(uint32_t *)6400756UL = (*(uint32_t *)6400756UL + (uint32_t)rcx_2);
        rsi9_1 = rsi9_0;
        rax_2 = rax_1;
        mrv.field_0 = rax_2;
        mrv1 = mrv;
        mrv1.field_1 = rbp;
        mrv2 = mrv1;
        mrv2.field_2 = r10;
        mrv3 = mrv2;
        mrv3.field_3 = rsi9_1;
        mrv4 = mrv3;
        mrv4.field_4 = r9;
        mrv5 = mrv4;
        mrv5.field_5 = r8;
        return mrv5;
    }
    rax_1 = 4UL;
    if (*(unsigned char *)6400698UL != '\x00') {
        *(unsigned char *)var_9 = (unsigned char)'\\';
        var_13 = var_0 + (-32L);
        *(uint64_t *)var_13 = 4203772UL;
        indirect_placeholder();
        *(unsigned char *)(var_9 + 1UL) = *(unsigned char *)var_13;
        *(unsigned char *)(var_9 + 2UL) = *(unsigned char *)(var_0 + (-31L));
        *(unsigned char *)(var_9 + 3UL) = *(unsigned char *)(var_0 + (-30L));
        *(uint32_t *)6400756UL = (*(uint32_t *)6400756UL + (uint32_t)rcx_2);
        rsi9_1 = rsi9_0;
        rax_2 = rax_1;
        mrv.field_0 = rax_2;
        mrv1 = mrv;
        mrv1.field_1 = rbp;
        mrv2 = mrv1;
        mrv2.field_2 = r10;
        mrv3 = mrv2;
        mrv3.field_3 = rsi9_1;
        mrv4 = mrv3;
        mrv4.field_4 = r9;
        mrv5 = mrv4;
        mrv5.field_5 = r8;
        return mrv5;
    }
    rcx_2 = 0UL;
    rax_1 = 1UL;
    if (*(unsigned char *)6400697UL == '\x00') {
        var_17 = ((uint64_t)(var_12 + '\xf8') == 0UL);
        var_18 = (unsigned char *)var_9;
        if (!var_17) {
            *var_18 = var_12;
            *(uint32_t *)6400756UL = (*(uint32_t *)6400756UL + (uint32_t)rcx_2);
            rsi9_1 = rsi9_0;
            rax_2 = rax_1;
            mrv.field_0 = rax_2;
            mrv1 = mrv;
            mrv1.field_1 = rbp;
            mrv2 = mrv1;
            mrv2.field_2 = r10;
            mrv3 = mrv2;
            mrv3.field_3 = rsi9_1;
            mrv4 = mrv3;
            mrv4.field_4 = r9;
            mrv5 = mrv4;
            mrv5.field_5 = r8;
            return mrv5;
        }
        *var_18 = (unsigned char)'\b';
        var_24 = *(uint32_t *)6400756UL;
        rcx_2 = rcx_1;
        rax_1 = rax_0;
        if (var_24 != 0U) {
            var_25 = (uint64_t)var_24;
            var_26 = 0UL - rcx_1;
            var_27 = (uint64_t)(uint32_t)var_26;
            rsi9_0 = var_27;
            rsi9_1 = var_27;
            rax_2 = rax_0;
            if ((long)(var_25 << 32UL) > (long)(var_26 << 32UL)) {
                *(uint32_t *)6400756UL = (*(uint32_t *)6400756UL + (uint32_t)rcx_2);
                rsi9_1 = rsi9_0;
                rax_2 = rax_1;
            } else {
                *(uint32_t *)6400756UL = 0U;
            }
        }
        mrv.field_0 = rax_2;
        mrv1 = mrv;
        mrv1.field_1 = rbp;
        mrv2 = mrv1;
        mrv2.field_2 = r10;
        mrv3 = mrv2;
        mrv3.field_3 = rsi9_1;
        mrv4 = mrv3;
        mrv4.field_4 = r9;
        mrv5 = mrv4;
        mrv5.field_5 = r8;
        return mrv5;
    }
    var_14 = ((signed char)var_12 < '\x00');
    var_15 = (unsigned char *)var_9;
    rcx_2 = 2UL;
    rax_1 = 2UL;
    if (var_14) {
        *var_15 = (unsigned char)'\\';
        var_16 = var_0 + (-32L);
        *(uint64_t *)var_16 = 4203874UL;
        indirect_placeholder();
        *(unsigned char *)(var_9 + 1UL) = *(unsigned char *)var_16;
        *(unsigned char *)(var_9 + 2UL) = *(unsigned char *)(var_0 + (-31L));
        *(unsigned char *)(var_9 + 3UL) = *(unsigned char *)(var_0 + (-30L));
    } else {
        *var_15 = (unsigned char)'^';
        *(unsigned char *)(var_9 + 1UL) = (var_12 ^ '@');
    }
    *(uint32_t *)6400756UL = (*(uint32_t *)6400756UL + (uint32_t)rcx_2);
    rsi9_1 = rsi9_0;
    rax_2 = rax_1;
    mrv.field_0 = rax_2;
    mrv1 = mrv;
    mrv1.field_1 = rbp;
    mrv2 = mrv1;
    mrv2.field_2 = r10;
    mrv3 = mrv2;
    mrv3.field_3 = rsi9_1;
    mrv4 = mrv3;
    mrv4.field_4 = r9;
    mrv5 = mrv4;
    mrv5.field_5 = r8;
    return mrv5;
}

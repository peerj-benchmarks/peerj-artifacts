typedef unsigned char __u_char;
typedef unsigned short __u_short;
typedef unsigned int __u_int;
typedef unsigned long __u_long;
typedef signed char __int8_t;
typedef unsigned char __uint8_t;
typedef short __int16_t;
typedef unsigned short __uint16_t;
typedef int __int32_t;
typedef unsigned int __uint32_t;
typedef long __int64_t;
typedef unsigned long __uint64_t;
typedef __int8_t __int_least8_t;
typedef __uint8_t __uint_least8_t;
typedef __int16_t __int_least16_t;
typedef __uint16_t __uint_least16_t;
typedef __int32_t __int_least32_t;
typedef __uint32_t __uint_least32_t;
typedef __int64_t __int_least64_t;
typedef __uint64_t __uint_least64_t;
typedef long __quad_t;
typedef unsigned long __u_quad_t;
typedef long __intmax_t;
typedef unsigned long __uintmax_t;
typedef unsigned long __dev_t;
typedef unsigned int __uid_t;
typedef unsigned int __gid_t;
typedef unsigned long __ino_t;
typedef unsigned long __ino64_t;
typedef unsigned int __mode_t;
typedef unsigned long __nlink_t;
typedef long __off_t;
typedef long __off64_t;
typedef int __pid_t;
typedef struct {
    int __val[2];
} __fsid_t;
typedef long __clock_t;
typedef unsigned long __rlim_t;
typedef unsigned long __rlim64_t;
typedef unsigned int __id_t;
typedef long __time_t;
typedef unsigned int __useconds_t;
typedef long __suseconds_t;
typedef int __daddr_t;
typedef int __key_t;
typedef int __clockid_t;
typedef void *__timer_t;
typedef long __blksize_t;
typedef long __blkcnt_t;
typedef long __blkcnt64_t;
typedef unsigned long __fsblkcnt_t;
typedef unsigned long __fsblkcnt64_t;
typedef unsigned long __fsfilcnt_t;
typedef unsigned long __fsfilcnt64_t;
typedef long __fsword_t;
typedef long __ssize_t;
typedef long __syscall_slong_t;
typedef unsigned long __syscall_ulong_t;
typedef __off64_t __loff_t;
typedef char *__caddr_t;
typedef long __intptr_t;
typedef unsigned int __socklen_t;
typedef int __sig_atomic_t;
typedef __int8_t int8_t;
typedef __int16_t int16_t;
typedef __int32_t int32_t;
typedef __int64_t int64_t;
typedef __uint8_t uint8_t;
typedef __uint16_t uint16_t;
typedef __uint32_t uint32_t;
typedef __uint64_t uint64_t;
typedef __int_least8_t int_least8_t;
typedef __int_least16_t int_least16_t;
typedef __int_least32_t int_least32_t;
typedef __int_least64_t int_least64_t;
typedef __uint_least8_t uint_least8_t;
typedef __uint_least16_t uint_least16_t;
typedef __uint_least32_t uint_least32_t;
typedef __uint_least64_t uint_least64_t;
typedef signed char int_fast8_t;
typedef long int_fast16_t;
typedef long int_fast32_t;
typedef long int_fast64_t;
typedef unsigned char uint_fast8_t;
typedef unsigned long uint_fast16_t;
typedef unsigned long uint_fast32_t;
typedef unsigned long uint_fast64_t;
typedef long intptr_t;
typedef unsigned long uintptr_t;
typedef __intmax_t intmax_t;
typedef __uintmax_t uintmax_t;
typedef _Bool bool;
extern uint64_t revng_init_local_sp(uint64_t param_0);
extern uint64_t init_rbx(void);
extern void indirect_placeholder(void);
extern uint64_t init_rbp(void);
extern uint64_t init_r13(void);
extern uint64_t init_r12(void);
extern uint64_t init_rax(void);
extern void indirect_placeholder_3(uint64_t param_0, uint64_t param_1);
void bb_skip_read(uint64_t rdi, uint64_t rsi) {
    uint64_t var_0;
    uint64_t var_1;
    uint64_t var_2;
    uint64_t var_3;
    uint64_t var_4;
    uint64_t var_5;
    uint64_t var_6;
    uint64_t var_7;
    uint32_t var_8;
    bool var_9;
    unsigned char *_pre49;
    unsigned char *_pre_phi50;
    uint64_t local_sp_7;
    uint64_t local_sp_6;
    uint64_t local_sp_3;
    uint64_t rax_1;
    uint64_t var_22;
    uint64_t local_sp_8;
    uint64_t local_sp_0;
    uint64_t r12_3;
    uint64_t r12_4;
    uint64_t var_13;
    uint64_t var_12;
    unsigned char *_pre_phi47;
    unsigned char *var_10;
    uint64_t var_11;
    unsigned char var_15;
    uint64_t var_16;
    uint64_t local_sp_1;
    uint64_t var_14;
    uint64_t var_21;
    uint64_t local_sp_2;
    uint64_t r12_0;
    uint64_t r12_1;
    uint64_t var_17;
    uint64_t r12_2;
    uint32_t var_18;
    uint64_t var_19;
    uint64_t rax_0;
    uint64_t rdx_0;
    uint64_t var_20;
    unsigned int loop_state_var;
    var_0 = revng_init_local_sp(0UL);
    var_1 = init_rax();
    var_2 = init_rbx();
    var_3 = init_rbp();
    var_4 = init_r13();
    var_5 = init_r12();
    *(uint64_t *)(var_0 + (-8L)) = var_4;
    *(uint64_t *)(var_0 + (-16L)) = var_5;
    *(uint64_t *)(var_0 + (-24L)) = var_3;
    *(uint64_t *)(var_0 + (-32L)) = var_2;
    var_6 = *(uint64_t *)rdi;
    var_7 = var_0 + (-48L);
    *(uint64_t *)var_7 = 4204702UL;
    indirect_placeholder();
    var_8 = (uint32_t)var_1;
    var_9 = ((uint64_t)(var_8 + (-12)) == 0UL);
    rax_1 = var_1;
    local_sp_8 = var_7;
    r12_3 = 1UL;
    local_sp_1 = var_7;
    local_sp_2 = var_7;
    if (var_9) {
        var_10 = (unsigned char *)(rdi + 57UL);
        _pre_phi50 = var_10;
        _pre_phi47 = var_10;
        if (*var_10 == '\x00') {
            var_15 = *(unsigned char *)6400608UL;
            var_16 = (uint64_t)var_15;
            r12_0 = var_16;
            if (var_15 != '\x00') {
                var_21 = local_sp_8 + (-8L);
                *(uint64_t *)var_21 = 4204860UL;
                indirect_placeholder();
                local_sp_0 = var_21;
                r12_4 = r12_3;
                if ((uint64_t)((uint32_t)rax_1 + (-10)) == 0UL) {
                    var_22 = local_sp_8 + (-16L);
                    *(uint64_t *)var_22 = 4204875UL;
                    indirect_placeholder();
                    local_sp_0 = var_22;
                }
                *(uint64_t *)(local_sp_0 + (-8L)) = 4204883UL;
                indirect_placeholder_3(rax_1, rdi);
                if (*(unsigned char *)6400208UL != '\x00' & (*(unsigned char *)6400799UL <= ((uint64_t)((uint32_t)rsi + (-1)) == 0UL)) && ((uint64_t)(unsigned char)r12_4 == 0UL)) {
                    *(uint32_t *)6400212UL = (*(uint32_t *)6400212UL + 1U);
                }
                return;
            }
            *_pre_phi47 = (unsigned char)'\x01';
            local_sp_3 = local_sp_2;
            r12_1 = r12_0;
        } else {
            var_11 = var_0 + (-56L);
            *(uint64_t *)var_11 = 4204725UL;
            indirect_placeholder();
            local_sp_2 = var_11;
            local_sp_8 = var_11;
            if ((uint64_t)(var_8 + (-10)) == 0UL) {
                var_13 = var_0 + (-64L);
                *(uint64_t *)var_13 = 4204742UL;
                indirect_placeholder();
                local_sp_1 = var_13;
                *_pre_phi50 = (unsigned char)'\x00';
                var_14 = (var_5 & (-256L)) | var_9;
                local_sp_3 = local_sp_1;
                _pre_phi47 = _pre_phi50;
                local_sp_2 = local_sp_1;
                r12_0 = var_14;
                r12_1 = var_14;
                if (*(unsigned char *)6400608UL == '\x00') {
                    *_pre_phi47 = (unsigned char)'\x01';
                    local_sp_3 = local_sp_2;
                    r12_1 = r12_0;
                }
            } else {
                *var_10 = (unsigned char)'\x00';
                var_12 = (var_5 & (-256L)) | 1UL;
                r12_0 = var_12;
                r12_3 = var_12;
                if (*(unsigned char *)6400608UL != '\x00') {
                    var_21 = local_sp_8 + (-8L);
                    *(uint64_t *)var_21 = 4204860UL;
                    indirect_placeholder();
                    local_sp_0 = var_21;
                    r12_4 = r12_3;
                    if ((uint64_t)((uint32_t)rax_1 + (-10)) == 0UL) {
                        var_22 = local_sp_8 + (-16L);
                        *(uint64_t *)var_22 = 4204875UL;
                        indirect_placeholder();
                        local_sp_0 = var_22;
                    }
                    *(uint64_t *)(local_sp_0 + (-8L)) = 4204883UL;
                    indirect_placeholder_3(rax_1, rdi);
                    if (*(unsigned char *)6400208UL != '\x00' & (*(unsigned char *)6400799UL <= ((uint64_t)((uint32_t)rsi + (-1)) == 0UL)) && ((uint64_t)(unsigned char)r12_4 == 0UL)) {
                        *(uint32_t *)6400212UL = (*(uint32_t *)6400212UL + 1U);
                    }
                    return;
                }
                *_pre_phi47 = (unsigned char)'\x01';
                local_sp_3 = local_sp_2;
                r12_1 = r12_0;
            }
        }
    } else {
        _pre49 = (unsigned char *)(rdi + 57UL);
        _pre_phi50 = _pre49;
        *_pre_phi50 = (unsigned char)'\x00';
        var_14 = (var_5 & (-256L)) | var_9;
        local_sp_3 = local_sp_1;
        _pre_phi47 = _pre_phi50;
        local_sp_2 = local_sp_1;
        r12_0 = var_14;
        r12_1 = var_14;
        if (*(unsigned char *)6400608UL == '\x00') {
            *_pre_phi47 = (unsigned char)'\x01';
            local_sp_3 = local_sp_2;
            r12_1 = r12_0;
        }
    }
    local_sp_6 = local_sp_3;
    local_sp_7 = local_sp_3;
    r12_2 = r12_1;
    r12_4 = r12_1;
    if ((uint64_t)(var_8 + (-10)) != 0UL) {
        if (*(unsigned char *)6400208UL != '\x00' & (*(unsigned char *)6400799UL <= ((uint64_t)((uint32_t)rsi + (-1)) == 0UL)) && ((uint64_t)(unsigned char)r12_4 == 0UL)) {
            *(uint32_t *)6400212UL = (*(uint32_t *)6400212UL + 1U);
        }
        return;
    }
    r12_4 = 0UL;
    if ((uint64_t)(unsigned char)r12_1 != 0UL) {
        r12_2 = 0UL;
        if ((uint64_t)(var_8 + 1U) != 0UL) {
            *(uint64_t *)(local_sp_3 + (-8L)) = 4204898UL;
            indirect_placeholder_3(var_6, rdi);
            if (*(unsigned char *)6400208UL != '\x00' & (*(unsigned char *)6400799UL <= ((uint64_t)((uint32_t)rsi + (-1)) == 0UL)) && ((uint64_t)(unsigned char)r12_4 == 0UL)) {
                *(uint32_t *)6400212UL = (*(uint32_t *)6400212UL + 1U);
            }
            return;
        }
        r12_4 = (uint64_t)0L;
        while (1U)
            {
                var_17 = local_sp_6 + (-8L);
                *(uint64_t *)var_17 = 4204914UL;
                indirect_placeholder();
                local_sp_6 = var_17;
                local_sp_7 = var_17;
                if (!0) {
                    loop_state_var = 1U;
                    break;
                }
                if (var_9) {
                    continue;
                }
                loop_state_var = 0U;
                break;
            }
        switch (loop_state_var) {
          case 0U:
            {
                break;
            }
            break;
          case 1U:
            {
                if (*(unsigned char *)6400208UL != '\x00' & (*(unsigned char *)6400799UL <= ((uint64_t)((uint32_t)rsi + (-1)) == 0UL)) && ((uint64_t)(unsigned char)r12_4 == 0UL)) {
                    *(uint32_t *)6400212UL = (*(uint32_t *)6400212UL + 1U);
                }
                return;
            }
            break;
        }
    }
    local_sp_8 = local_sp_7;
    r12_3 = r12_2;
    if (*(unsigned char *)6400608UL != '\x00') {
        if (*(unsigned char *)6400799UL != '\x00') {
            var_18 = *(uint32_t *)6400232UL;
            var_19 = (uint64_t)var_18;
            rax_0 = var_19;
            rax_1 = var_19;
            if (var_18 != 0U) {
                rdx_0 = *(uint64_t *)6400840UL;
                rax_1 = 0UL;
                *(unsigned char *)(rdx_0 + 57UL) = (unsigned char)'\x00';
                var_20 = (uint64_t)((uint32_t)rax_0 + (-1));
                rax_0 = var_20;
                while (var_20 != 0UL)
                    {
                        rdx_0 = rdx_0 + 64UL;
                        *(unsigned char *)(rdx_0 + 57UL) = (unsigned char)'\x00';
                        var_20 = (uint64_t)((uint32_t)rax_0 + (-1));
                        rax_0 = var_20;
                    }
            }
        }
        *(unsigned char *)(rdi + 57UL) = (unsigned char)'\x00';
    }
    var_21 = local_sp_8 + (-8L);
    *(uint64_t *)var_21 = 4204860UL;
    indirect_placeholder();
    local_sp_0 = var_21;
    r12_4 = r12_3;
    if ((uint64_t)((uint32_t)rax_1 + (-10)) == 0UL) {
        var_22 = local_sp_8 + (-16L);
        *(uint64_t *)var_22 = 4204875UL;
        indirect_placeholder();
        local_sp_0 = var_22;
    }
    *(uint64_t *)(local_sp_0 + (-8L)) = 4204883UL;
    indirect_placeholder_3(rax_1, rdi);
    if (~(*(unsigned char *)6400208UL != '\x00' & (*(unsigned char *)6400799UL <= ((uint64_t)((uint32_t)rsi + (-1)) == 0UL)) && ((uint64_t)(unsigned char)r12_4 == 0UL))) {
        return;
    }
    *(uint32_t *)6400212UL = (*(uint32_t *)6400212UL + 1U);
    return;
}

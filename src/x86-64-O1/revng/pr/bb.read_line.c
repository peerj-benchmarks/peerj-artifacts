typedef unsigned char __u_char;
typedef unsigned short __u_short;
typedef unsigned int __u_int;
typedef unsigned long __u_long;
typedef signed char __int8_t;
typedef unsigned char __uint8_t;
typedef short __int16_t;
typedef unsigned short __uint16_t;
typedef int __int32_t;
typedef unsigned int __uint32_t;
typedef long __int64_t;
typedef unsigned long __uint64_t;
typedef __int8_t __int_least8_t;
typedef __uint8_t __uint_least8_t;
typedef __int16_t __int_least16_t;
typedef __uint16_t __uint_least16_t;
typedef __int32_t __int_least32_t;
typedef __uint32_t __uint_least32_t;
typedef __int64_t __int_least64_t;
typedef __uint64_t __uint_least64_t;
typedef long __quad_t;
typedef unsigned long __u_quad_t;
typedef long __intmax_t;
typedef unsigned long __uintmax_t;
typedef unsigned long __dev_t;
typedef unsigned int __uid_t;
typedef unsigned int __gid_t;
typedef unsigned long __ino_t;
typedef unsigned long __ino64_t;
typedef unsigned int __mode_t;
typedef unsigned long __nlink_t;
typedef long __off_t;
typedef long __off64_t;
typedef int __pid_t;
typedef struct {
    int __val[2];
} __fsid_t;
typedef long __clock_t;
typedef unsigned long __rlim_t;
typedef unsigned long __rlim64_t;
typedef unsigned int __id_t;
typedef long __time_t;
typedef unsigned int __useconds_t;
typedef long __suseconds_t;
typedef int __daddr_t;
typedef int __key_t;
typedef int __clockid_t;
typedef void *__timer_t;
typedef long __blksize_t;
typedef long __blkcnt_t;
typedef long __blkcnt64_t;
typedef unsigned long __fsblkcnt_t;
typedef unsigned long __fsblkcnt64_t;
typedef unsigned long __fsfilcnt_t;
typedef unsigned long __fsfilcnt64_t;
typedef long __fsword_t;
typedef long __ssize_t;
typedef long __syscall_slong_t;
typedef unsigned long __syscall_ulong_t;
typedef __off64_t __loff_t;
typedef char *__caddr_t;
typedef long __intptr_t;
typedef unsigned int __socklen_t;
typedef int __sig_atomic_t;
typedef __int8_t int8_t;
typedef __int16_t int16_t;
typedef __int32_t int32_t;
typedef __int64_t int64_t;
typedef __uint8_t uint8_t;
typedef __uint16_t uint16_t;
typedef __uint32_t uint32_t;
typedef __uint64_t uint64_t;
typedef __int_least8_t int_least8_t;
typedef __int_least16_t int_least16_t;
typedef __int_least32_t int_least32_t;
typedef __int_least64_t int_least64_t;
typedef __uint_least8_t uint_least8_t;
typedef __uint_least16_t uint_least16_t;
typedef __uint_least32_t uint_least32_t;
typedef __uint_least64_t uint_least64_t;
typedef signed char int_fast8_t;
typedef long int_fast16_t;
typedef long int_fast32_t;
typedef long int_fast64_t;
typedef unsigned char uint_fast8_t;
typedef unsigned long uint_fast16_t;
typedef unsigned long uint_fast32_t;
typedef unsigned long uint_fast64_t;
typedef long intptr_t;
typedef unsigned long uintptr_t;
typedef __intmax_t intmax_t;
typedef __uintmax_t uintmax_t;
typedef _Bool bool;
struct bb_read_line_ret_type;
struct indirect_placeholder_54_ret_type;
struct indirect_placeholder_53_ret_type;
struct indirect_placeholder_55_ret_type;
struct indirect_placeholder_56_ret_type;
struct indirect_placeholder_57_ret_type;
struct indirect_placeholder_58_ret_type;
struct bb_read_line_ret_type {
    uint64_t field_0;
    uint64_t field_1;
};
struct indirect_placeholder_54_ret_type {
    uint64_t field_0;
    uint64_t field_1;
    uint64_t field_2;
    uint64_t field_3;
    uint64_t field_4;
};
struct indirect_placeholder_53_ret_type {
    uint64_t field_0;
    uint64_t field_1;
    uint64_t field_2;
    uint64_t field_3;
    uint64_t field_4;
    uint64_t field_5;
};
struct indirect_placeholder_55_ret_type {
    uint64_t field_0;
    uint64_t field_1;
    uint64_t field_2;
    uint64_t field_3;
    uint64_t field_4;
    uint64_t field_5;
};
struct indirect_placeholder_56_ret_type {
    uint64_t field_0;
    uint64_t field_1;
    uint64_t field_2;
    uint64_t field_3;
    uint64_t field_4;
};
struct indirect_placeholder_57_ret_type {
    uint64_t field_0;
    uint64_t field_1;
    uint64_t field_2;
    uint64_t field_3;
};
struct indirect_placeholder_58_ret_type {
    uint64_t field_0;
    uint64_t field_1;
    uint64_t field_2;
    uint64_t field_3;
};
extern uint64_t revng_init_local_sp(uint64_t param_0);
extern uint64_t init_rbx(void);
extern void indirect_placeholder(void);
extern uint64_t init_rbp(void);
extern uint64_t init_r14(void);
extern uint64_t init_r13(void);
extern uint64_t init_r12(void);
extern uint64_t indirect_placeholder_1(uint64_t param_0);
extern uint64_t helper_cc_compute_all_wrapper(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint32_t param_3);
extern uint64_t init_rsi(void);
extern uint64_t init_r15(void);
extern uint64_t init_r8(void);
extern uint64_t init_rax(void);
extern void indirect_placeholder_3(uint64_t param_0, uint64_t param_1);
extern void indirect_placeholder_5(uint64_t param_0);
extern struct indirect_placeholder_54_ret_type indirect_placeholder_54(void);
extern uint64_t init_r9(void);
extern uint64_t init_r10(void);
extern struct indirect_placeholder_53_ret_type indirect_placeholder_53(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint64_t param_3, uint64_t param_4, uint64_t param_5);
extern struct indirect_placeholder_55_ret_type indirect_placeholder_55(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint64_t param_3, uint64_t param_4, uint64_t param_5);
extern struct indirect_placeholder_56_ret_type indirect_placeholder_56(void);
extern struct indirect_placeholder_57_ret_type indirect_placeholder_57(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint64_t param_3, uint64_t param_4);
extern struct indirect_placeholder_58_ret_type indirect_placeholder_58(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint64_t param_3, uint64_t param_4);
struct bb_read_line_ret_type bb_read_line(uint64_t rdi) {
    uint64_t var_67;
    uint64_t r10_3;
    uint64_t local_sp_8;
    uint64_t var_69;
    uint64_t _pre_phi139;
    uint64_t local_sp_2;
    uint64_t var_70;
    struct indirect_placeholder_54_ret_type var_71;
    uint64_t local_sp_3;
    uint64_t rax_0;
    uint64_t r10_0;
    struct indirect_placeholder_58_ret_type var_42;
    struct indirect_placeholder_57_ret_type var_32;
    uint64_t var_0;
    uint64_t var_1;
    uint64_t var_2;
    uint64_t var_3;
    uint64_t var_4;
    uint64_t var_5;
    uint64_t var_6;
    uint64_t var_7;
    uint64_t var_8;
    uint64_t var_9;
    uint64_t var_10;
    uint64_t var_11;
    uint64_t var_12;
    uint32_t var_13;
    uint32_t var_14;
    bool var_15;
    uint64_t _pre_phi147;
    uint64_t local_sp_0;
    uint64_t local_sp_15_be;
    uint64_t rax_1_be;
    uint64_t rbx_3_be;
    uint64_t local_sp_15;
    uint64_t var_43;
    uint64_t rax_2;
    uint64_t var_61;
    uint32_t var_62;
    uint64_t var_63;
    uint64_t var_64;
    uint64_t rbx_0;
    uint64_t var_65;
    uint64_t var_66;
    uint64_t rbx_3;
    uint64_t r10_7;
    uint64_t var_20;
    uint64_t rax_1;
    uint64_t rsi_6;
    uint64_t rbp_1;
    uint32_t var_51;
    uint64_t var_52;
    uint64_t var_53;
    uint64_t r9_6;
    uint64_t r8_6;
    struct indirect_placeholder_53_ret_type var_54;
    uint64_t var_55;
    uint64_t var_56;
    uint64_t var_57;
    uint64_t var_58;
    uint64_t var_59;
    uint64_t var_60;
    uint64_t var_19;
    unsigned char *var_16;
    uint64_t local_sp_4;
    unsigned char *_pre_phi143;
    uint64_t local_sp_6;
    uint64_t _pre138;
    uint64_t var_17;
    uint64_t var_18;
    uint32_t storemerge;
    bool var_21;
    uint64_t var_22;
    uint64_t var_23;
    struct indirect_placeholder_55_ret_type var_24;
    uint64_t local_sp_5;
    uint64_t var_68;
    uint64_t r14_0;
    uint64_t rbp_0;
    uint64_t r10_1;
    uint64_t rsi_0;
    uint64_t r9_0;
    uint64_t r8_0;
    uint64_t local_sp_12;
    uint64_t local_sp_7;
    uint64_t var_25;
    struct indirect_placeholder_56_ret_type var_26;
    uint64_t r10_2;
    uint64_t rsi_1;
    uint64_t r9_1;
    uint64_t r8_1;
    uint64_t local_sp_10;
    uint64_t var_27;
    uint64_t var_28;
    uint64_t var_29;
    uint64_t local_sp_9;
    uint64_t var_30;
    uint64_t r13_0;
    uint64_t r12_0;
    uint64_t rsi_2;
    uint64_t r9_2;
    uint64_t r8_2;
    uint64_t var_31;
    uint64_t var_33;
    uint64_t r10_4;
    uint64_t rsi_3;
    uint64_t r9_3;
    uint64_t r8_3;
    uint64_t r10_5;
    uint64_t rsi_4;
    uint64_t r9_4;
    uint64_t r8_4;
    uint32_t var_34;
    uint64_t var_35;
    uint32_t var_36;
    uint64_t var_37;
    uint64_t local_sp_11;
    uint64_t var_38;
    uint64_t var_39;
    uint64_t var_40;
    uint64_t rdi2_0;
    uint64_t var_41;
    uint64_t r10_6;
    uint64_t rsi_5;
    uint64_t r9_5;
    uint64_t r8_5;
    uint64_t local_sp_13;
    uint64_t var_44;
    uint32_t var_45;
    uint64_t local_sp_15_ph;
    uint64_t var_46;
    uint64_t var_47;
    uint64_t local_sp_14;
    uint64_t rbx_2;
    uint64_t var_48;
    uint64_t var_49;
    uint64_t rax_1_ph;
    uint64_t rbx_3_ph;
    uint32_t var_50;
    uint64_t r10_8;
    struct bb_read_line_ret_type mrv;
    struct bb_read_line_ret_type mrv1;
    unsigned int loop_state_var;
    var_0 = revng_init_local_sp(0UL);
    var_1 = init_rax();
    var_2 = init_rbx();
    var_3 = init_r14();
    var_4 = init_rbp();
    var_5 = init_r13();
    var_6 = init_r12();
    var_7 = init_r15();
    var_8 = init_r10();
    var_9 = init_rsi();
    var_10 = init_r9();
    var_11 = init_r8();
    *(uint64_t *)(var_0 + (-8L)) = var_7;
    *(uint64_t *)(var_0 + (-16L)) = var_3;
    *(uint64_t *)(var_0 + (-24L)) = var_5;
    *(uint64_t *)(var_0 + (-32L)) = var_6;
    *(uint64_t *)(var_0 + (-40L)) = var_4;
    *(uint64_t *)(var_0 + (-48L)) = var_2;
    var_12 = var_0 + (-64L);
    *(uint64_t *)var_12 = 4206259UL;
    indirect_placeholder();
    var_13 = *(uint32_t *)6400756UL;
    var_14 = (uint32_t)var_1;
    var_15 = ((uint64_t)(var_14 + (-12)) == 0UL);
    rax_0 = var_1;
    r10_0 = var_8;
    rax_2 = 1UL;
    local_sp_4 = var_12;
    storemerge = 0U;
    local_sp_5 = var_12;
    r14_0 = var_3;
    rbp_0 = rdi;
    r10_1 = var_8;
    rsi_0 = var_9;
    r9_0 = var_10;
    r8_0 = var_11;
    r13_0 = 1UL;
    rax_1_ph = 1UL;
    r10_8 = var_8;
    if (var_15) {
        _pre_phi147 = (uint64_t)(var_14 + (-10));
        _pre_phi143 = (unsigned char *)(rdi + 57UL);
    } else {
        var_16 = (unsigned char *)(rdi + 57UL);
        _pre_phi143 = var_16;
        if (*var_16 != '\x00') {
            _pre138 = (uint64_t)(var_14 + (-10));
            _pre_phi139 = _pre138;
            var_68 = local_sp_5 + (-8L);
            *(uint64_t *)var_68 = 4206340UL;
            indirect_placeholder();
            local_sp_2 = var_68;
            if (_pre_phi139 == 0UL) {
                var_69 = local_sp_5 + (-16L);
                *(uint64_t *)var_69 = 4206356UL;
                indirect_placeholder();
                local_sp_2 = var_69;
            }
            *(unsigned char *)6400796UL = (unsigned char)'\x01';
            local_sp_3 = local_sp_2;
            if (*(unsigned char *)6400792UL == '\x00') {
                if (*(unsigned char *)6400260UL == '\x00') {
                    *(unsigned char *)6400672UL = (unsigned char)'\x01';
                    var_70 = local_sp_2 + (-8L);
                    *(uint64_t *)var_70 = 4206393UL;
                    var_71 = indirect_placeholder_54();
                    local_sp_3 = var_70;
                    rax_0 = var_71.field_0;
                    r10_0 = var_71.field_1;
                } else {
                    if (*(unsigned char *)6400794UL == '\x00') {
                        *(unsigned char *)6400793UL = (unsigned char)'\x01';
                    }
                }
            } else {
                if (*(unsigned char *)6400794UL == '\x00') {
                    *(unsigned char *)6400793UL = (unsigned char)'\x01';
                }
            }
            *(uint64_t *)(local_sp_3 + (-8L)) = 4206419UL;
            indirect_placeholder_3(rax_0, rdi);
            r10_8 = r10_0;
            mrv.field_0 = rax_2;
            mrv1 = mrv;
            mrv1.field_1 = r10_8;
            return mrv1;
        }
        var_17 = var_0 + (-72L);
        *(uint64_t *)var_17 = 4206288UL;
        indirect_placeholder();
        var_18 = (uint64_t)(var_14 + (-10));
        _pre_phi147 = var_18;
        local_sp_4 = var_17;
        if (var_18 == 0UL) {
            var_19 = var_0 + (-80L);
            *(uint64_t *)var_19 = 4206304UL;
            indirect_placeholder();
            _pre_phi147 = 0UL;
            local_sp_4 = var_19;
        }
    }
    var_20 = (uint64_t)var_14;
    *_pre_phi143 = (unsigned char)'\x00';
    var_21 = (_pre_phi147 == 0UL);
    _pre_phi139 = _pre_phi147;
    local_sp_5 = local_sp_4;
    local_sp_6 = local_sp_4;
    rax_2 = 0UL;
    if (!var_21) {
        if (!var_15) {
            var_68 = local_sp_5 + (-8L);
            *(uint64_t *)var_68 = 4206340UL;
            indirect_placeholder();
            local_sp_2 = var_68;
            if (_pre_phi139 == 0UL) {
                var_69 = local_sp_5 + (-16L);
                *(uint64_t *)var_69 = 4206356UL;
                indirect_placeholder();
                local_sp_2 = var_69;
            }
            *(unsigned char *)6400796UL = (unsigned char)'\x01';
            local_sp_3 = local_sp_2;
            if (*(unsigned char *)6400792UL == '\x00') {
                if (*(unsigned char *)6400260UL == '\x00') {
                    *(unsigned char *)6400672UL = (unsigned char)'\x01';
                    var_70 = local_sp_2 + (-8L);
                    *(uint64_t *)var_70 = 4206393UL;
                    var_71 = indirect_placeholder_54();
                    local_sp_3 = var_70;
                    rax_0 = var_71.field_0;
                    r10_0 = var_71.field_1;
                } else {
                    if (*(unsigned char *)6400794UL == '\x00') {
                        *(unsigned char *)6400793UL = (unsigned char)'\x01';
                    }
                }
            } else {
                if (*(unsigned char *)6400794UL == '\x00') {
                    *(unsigned char *)6400793UL = (unsigned char)'\x01';
                }
            }
            *(uint64_t *)(local_sp_3 + (-8L)) = 4206419UL;
            indirect_placeholder_3(rax_0, rdi);
            r10_8 = r10_0;
            mrv.field_0 = rax_2;
            mrv1 = mrv;
            mrv1.field_1 = r10_8;
            return mrv1;
        }
        if ((uint64_t)(var_14 + 1U) != 0UL) {
            *(uint64_t *)(local_sp_4 + (-8L)) = 4206437UL;
            indirect_placeholder_3(var_20, rdi);
            mrv.field_0 = rax_2;
            mrv1 = mrv;
            mrv1.field_1 = r10_8;
            return mrv1;
        }
        var_22 = (uint64_t)(uint32_t)((int)(var_14 << 24U) >> (int)24U);
        var_23 = local_sp_4 + (-8L);
        *(uint64_t *)var_23 = 4206455UL;
        var_24 = indirect_placeholder_55(rdi, var_22, var_8, var_9, var_10, var_11);
        local_sp_6 = var_23;
        r14_0 = (uint64_t)(uint32_t)var_24.field_0;
        rbp_0 = var_24.field_1;
        r10_1 = var_24.field_2;
        rsi_0 = var_24.field_3;
        r9_0 = var_24.field_4;
        r8_0 = var_24.field_5;
    }
    rbp_1 = rbp_0;
    local_sp_12 = local_sp_6;
    local_sp_7 = local_sp_6;
    r10_2 = r10_1;
    rsi_1 = rsi_0;
    r9_1 = r9_0;
    r8_1 = r8_0;
    r10_6 = r10_1;
    rsi_5 = rsi_0;
    r9_5 = r9_0;
    r8_5 = r8_0;
    r10_8 = r10_1;
    if (*(unsigned char *)6400781UL != '\x00') {
        if ((int)*(uint32_t *)6400776UL >= (int)*(uint32_t *)6400756UL) {
            *(uint32_t *)6400756UL = var_13;
            mrv.field_0 = rax_2;
            mrv1 = mrv;
            mrv1.field_1 = r10_8;
            return mrv1;
        }
    }
    if (*(uint64_t *)(rbp_0 + 32UL) != 4202793UL) {
        *(unsigned char *)6400672UL = (unsigned char)'\x01';
        if (*(unsigned char *)6400792UL != '\x00' & *(unsigned char *)6400260UL == '\x00') {
            var_25 = local_sp_6 + (-8L);
            *(uint64_t *)var_25 = 4206542UL;
            var_26 = indirect_placeholder_56();
            local_sp_7 = var_25;
            r10_2 = var_26.field_1;
            rsi_1 = var_26.field_2;
            r9_1 = var_26.field_3;
            r8_1 = var_26.field_4;
        }
        r10_3 = r10_2;
        local_sp_8 = local_sp_7;
        local_sp_10 = local_sp_7;
        local_sp_9 = local_sp_7;
        rsi_2 = rsi_1;
        r9_2 = r9_1;
        r8_2 = r8_1;
        r10_4 = r10_2;
        rsi_3 = rsi_1;
        r9_3 = r9_1;
        r8_3 = r8_1;
        r10_5 = r10_2;
        rsi_4 = rsi_1;
        r9_4 = r9_1;
        r8_4 = r8_1;
        if (*(unsigned char *)6400799UL != '\x00' & *(unsigned char *)6400798UL != '\x00') {
            var_27 = (uint64_t)*(uint32_t *)6400680UL;
            *(uint32_t *)6400680UL = 0U;
            var_28 = *(uint64_t *)6400840UL;
            var_29 = helper_cc_compute_all_wrapper(var_27, 0UL, 0UL, 24U);
            r12_0 = var_28;
            if ((uint64_t)(((unsigned char)(var_29 >> 4UL) ^ (unsigned char)var_29) & '\xc0') != 0UL) {
                var_30 = var_27 << 32UL;
                var_31 = local_sp_8 + (-8L);
                *(uint64_t *)var_31 = 4206603UL;
                var_32 = indirect_placeholder_57(r12_0, r10_3, rsi_2, r9_2, r8_2);
                *(uint32_t *)6400680UL = (*(uint32_t *)6400680UL + 1U);
                var_33 = r13_0 + 1UL;
                local_sp_8 = var_31;
                local_sp_9 = var_31;
                while ((long)var_30 >= (long)(var_33 << 32UL))
                    {
                        r10_3 = var_32.field_0;
                        r13_0 = (uint64_t)(uint32_t)var_33;
                        r12_0 = r12_0 + 64UL;
                        rsi_2 = var_32.field_1;
                        r9_2 = var_32.field_2;
                        r8_2 = var_32.field_3;
                        var_31 = local_sp_8 + (-8L);
                        *(uint64_t *)var_31 = 4206603UL;
                        var_32 = indirect_placeholder_57(r12_0, r10_3, rsi_2, r9_2, r8_2);
                        *(uint32_t *)6400680UL = (*(uint32_t *)6400680UL + 1U);
                        var_33 = r13_0 + 1UL;
                        local_sp_8 = var_31;
                        local_sp_9 = var_31;
                    }
                r10_4 = var_32.field_0;
                rsi_3 = var_32.field_1;
                r9_3 = var_32.field_2;
                r8_3 = var_32.field_3;
            }
            *(uint32_t *)6400676UL = *(uint32_t *)(rbp_0 + 52UL);
            local_sp_10 = local_sp_9;
            r10_5 = r10_4;
            rsi_4 = rsi_3;
            r9_4 = r9_3;
            r8_4 = r8_3;
            if (*(unsigned char *)6400781UL != '\x00') {
                storemerge = *(uint32_t *)6400776UL;
            }
            *(uint32_t *)6400768UL = storemerge;
            *(unsigned char *)6400798UL = (unsigned char)'\x00';
        }
        var_34 = *(uint32_t *)6400684UL;
        var_35 = (uint64_t)var_34;
        var_36 = *(uint32_t *)6400676UL;
        var_37 = (uint64_t)var_36;
        local_sp_11 = local_sp_10;
        rdi2_0 = var_37;
        r10_6 = r10_5;
        rsi_5 = rsi_4;
        r9_5 = r9_4;
        r8_5 = r8_4;
        if ((long)(var_35 << 32UL) < (long)(var_37 << 32UL)) {
            var_38 = (uint64_t)(var_36 - var_34);
            var_39 = local_sp_10 + (-8L);
            *(uint64_t *)var_39 = 4206695UL;
            var_40 = indirect_placeholder_1(var_38);
            *(uint32_t *)6400676UL = 0U;
            local_sp_11 = var_39;
            rdi2_0 = var_40;
        }
        local_sp_12 = local_sp_11;
        if (*(unsigned char *)6400688UL == '\x00') {
            var_41 = local_sp_11 + (-8L);
            *(uint64_t *)var_41 = 4206719UL;
            var_42 = indirect_placeholder_58(rdi2_0, r10_5, rsi_4, r9_4, r8_4);
            local_sp_12 = var_41;
            r10_6 = var_42.field_0;
            rsi_5 = var_42.field_1;
            r9_5 = var_42.field_2;
            r8_5 = var_42.field_3;
        }
    }
    r10_7 = r10_6;
    rsi_6 = rsi_5;
    r9_6 = r9_5;
    r8_6 = r8_5;
    local_sp_13 = local_sp_12;
    r10_8 = r10_6;
    if (*(unsigned char *)(rbp_0 + 56UL) == '\x00') {
        var_43 = local_sp_12 + (-8L);
        *(uint64_t *)var_43 = 4206733UL;
        indirect_placeholder_5(rbp_0);
        local_sp_13 = var_43;
    }
    *(unsigned char *)6400797UL = (unsigned char)'\x00';
    local_sp_14 = local_sp_13;
    local_sp_15_ph = local_sp_13;
    if (var_21) {
        mrv.field_0 = rax_2;
        mrv1 = mrv;
        mrv1.field_1 = r10_8;
        return mrv1;
    }
    var_44 = *(uint64_t *)6400616UL;
    var_45 = (uint32_t)r14_0;
    rbx_2 = var_44;
    rbx_3_ph = var_44;
    if ((uint64_t)var_45 != 0UL) {
        var_46 = (uint64_t)(var_45 + (-1));
        var_47 = var_46 + var_44;
        rax_1_ph = var_46;
        var_48 = rbx_2 + 1UL;
        var_49 = local_sp_14 + (-8L);
        *(uint64_t *)var_49 = 4206786UL;
        indirect_placeholder();
        local_sp_14 = var_49;
        rbx_2 = var_48;
        local_sp_15_ph = var_49;
        rbx_3_ph = var_48;
        do {
            var_48 = rbx_2 + 1UL;
            var_49 = local_sp_14 + (-8L);
            *(uint64_t *)var_49 = 4206786UL;
            indirect_placeholder();
            local_sp_14 = var_49;
            rbx_2 = var_48;
            local_sp_15_ph = var_49;
            rbx_3_ph = var_48;
        } while (rbx_2 != var_47);
    }
    local_sp_15 = local_sp_15_ph;
    rax_1 = rax_1_ph;
    rbx_3 = rbx_3_ph;
    while (1U)
        {
            *(uint64_t *)(local_sp_15 + (-8L)) = 4206800UL;
            indirect_placeholder();
            var_50 = (uint32_t)rax_1;
            r10_8 = r10_7;
            if ((uint64_t)(var_50 + (-10)) != 0UL) {
                loop_state_var = 0U;
                break;
            }
            rax_2 = 0UL;
            if ((uint64_t)(var_50 + (-12)) != 0UL) {
                *(uint64_t *)(local_sp_15 + (-16L)) = 4206830UL;
                indirect_placeholder();
                var_67 = local_sp_15 + (-24L);
                *(uint64_t *)var_67 = 4206846UL;
                indirect_placeholder();
                rax_2 = 1UL;
                if (*(unsigned char *)6400794UL != '\x00') {
                    loop_state_var = 1U;
                    break;
                }
                *(unsigned char *)6400793UL = (unsigned char)'\x01';
                loop_state_var = 1U;
                break;
            }
            if ((uint64_t)(var_50 + 1U) != 0UL) {
                *(uint64_t *)(local_sp_15 + (-16L)) = 4206885UL;
                indirect_placeholder_3(rbx_3, rbp_1);
                rax_2 = 1UL;
                loop_state_var = 0U;
                break;
            }
            var_51 = *(uint32_t *)6400756UL;
            var_52 = (uint64_t)(uint32_t)((int)(var_50 << 24U) >> (int)24U);
            var_53 = local_sp_15 + (-16L);
            *(uint64_t *)var_53 = 4206906UL;
            var_54 = indirect_placeholder_53(rbp_1, var_52, r10_7, rsi_6, r9_6, r8_6);
            var_55 = var_54.field_0;
            var_56 = var_54.field_1;
            var_57 = var_54.field_2;
            var_58 = var_54.field_3;
            var_59 = var_54.field_4;
            var_60 = var_54.field_5;
            local_sp_0 = var_53;
            local_sp_15_be = var_53;
            rax_1_be = var_55;
            r10_7 = var_57;
            rsi_6 = var_58;
            rbp_1 = var_56;
            r9_6 = var_59;
            r8_6 = var_60;
            r10_8 = var_57;
            if (*(unsigned char *)6400781UL != '\x00') {
                if ((int)*(uint32_t *)6400776UL >= (int)*(uint32_t *)6400756UL) {
                    *(uint32_t *)6400756UL = var_51;
                    loop_state_var = 0U;
                    break;
                }
            }
            var_61 = *(uint64_t *)6400616UL;
            var_62 = (uint32_t)var_55;
            rbx_3_be = var_61;
            rbx_0 = var_61;
            if ((uint64_t)var_62 != 0UL) {
                var_63 = (uint64_t)(var_62 + (-1));
                var_64 = var_63 + var_61;
                rax_1_be = var_63;
                var_65 = rbx_0 + 1UL;
                var_66 = local_sp_0 + (-8L);
                *(uint64_t *)var_66 = 4206976UL;
                indirect_placeholder();
                local_sp_15_be = var_66;
                rbx_3_be = var_65;
                local_sp_0 = var_66;
                rbx_0 = var_65;
                do {
                    var_65 = rbx_0 + 1UL;
                    var_66 = local_sp_0 + (-8L);
                    *(uint64_t *)var_66 = 4206976UL;
                    indirect_placeholder();
                    local_sp_15_be = var_66;
                    rbx_3_be = var_65;
                    local_sp_0 = var_66;
                    rbx_0 = var_65;
                } while (rbx_0 != var_64);
            }
            local_sp_15 = local_sp_15_be;
            rax_1 = rax_1_be;
            rbx_3 = rbx_3_be;
            continue;
        }
    *(uint64_t *)(var_67 + (-8L)) = 4206870UL;
    indirect_placeholder_3(rax_1, rbp_1);
}

typedef unsigned char __u_char;
typedef unsigned short __u_short;
typedef unsigned int __u_int;
typedef unsigned long __u_long;
typedef signed char __int8_t;
typedef unsigned char __uint8_t;
typedef short __int16_t;
typedef unsigned short __uint16_t;
typedef int __int32_t;
typedef unsigned int __uint32_t;
typedef long __int64_t;
typedef unsigned long __uint64_t;
typedef __int8_t __int_least8_t;
typedef __uint8_t __uint_least8_t;
typedef __int16_t __int_least16_t;
typedef __uint16_t __uint_least16_t;
typedef __int32_t __int_least32_t;
typedef __uint32_t __uint_least32_t;
typedef __int64_t __int_least64_t;
typedef __uint64_t __uint_least64_t;
typedef long __quad_t;
typedef unsigned long __u_quad_t;
typedef long __intmax_t;
typedef unsigned long __uintmax_t;
typedef unsigned long __dev_t;
typedef unsigned int __uid_t;
typedef unsigned int __gid_t;
typedef unsigned long __ino_t;
typedef unsigned long __ino64_t;
typedef unsigned int __mode_t;
typedef unsigned long __nlink_t;
typedef long __off_t;
typedef long __off64_t;
typedef int __pid_t;
typedef struct {
    int __val[2];
} __fsid_t;
typedef long __clock_t;
typedef unsigned long __rlim_t;
typedef unsigned long __rlim64_t;
typedef unsigned int __id_t;
typedef long __time_t;
typedef unsigned int __useconds_t;
typedef long __suseconds_t;
typedef int __daddr_t;
typedef int __key_t;
typedef int __clockid_t;
typedef void *__timer_t;
typedef long __blksize_t;
typedef long __blkcnt_t;
typedef long __blkcnt64_t;
typedef unsigned long __fsblkcnt_t;
typedef unsigned long __fsblkcnt64_t;
typedef unsigned long __fsfilcnt_t;
typedef unsigned long __fsfilcnt64_t;
typedef long __fsword_t;
typedef long __ssize_t;
typedef long __syscall_slong_t;
typedef unsigned long __syscall_ulong_t;
typedef __off64_t __loff_t;
typedef char *__caddr_t;
typedef long __intptr_t;
typedef unsigned int __socklen_t;
typedef int __sig_atomic_t;
typedef __int8_t int8_t;
typedef __int16_t int16_t;
typedef __int32_t int32_t;
typedef __int64_t int64_t;
typedef __uint8_t uint8_t;
typedef __uint16_t uint16_t;
typedef __uint32_t uint32_t;
typedef __uint64_t uint64_t;
typedef __int_least8_t int_least8_t;
typedef __int_least16_t int_least16_t;
typedef __int_least32_t int_least32_t;
typedef __int_least64_t int_least64_t;
typedef __uint_least8_t uint_least8_t;
typedef __uint_least16_t uint_least16_t;
typedef __uint_least32_t uint_least32_t;
typedef __uint_least64_t uint_least64_t;
typedef signed char int_fast8_t;
typedef long int_fast16_t;
typedef long int_fast32_t;
typedef long int_fast64_t;
typedef unsigned char uint_fast8_t;
typedef unsigned long uint_fast16_t;
typedef unsigned long uint_fast32_t;
typedef unsigned long uint_fast64_t;
typedef long intptr_t;
typedef unsigned long uintptr_t;
typedef __intmax_t intmax_t;
typedef __uintmax_t uintmax_t;
typedef _Bool bool;
extern uint64_t revng_init_local_sp(uint64_t param_0);
extern uint64_t init_rbx(void);
extern uint64_t init_rbp(void);
extern uint64_t init_r14(void);
extern uint64_t init_r13(void);
extern uint64_t init_r12(void);
extern uint64_t init_r15(void);
extern uint64_t indirect_placeholder_13(uint64_t param_0, uint64_t param_1, uint64_t param_2);
extern uint64_t indirect_placeholder_23(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint64_t param_3);
uint64_t bb_base32_decode_ctx(uint64_t rdx, uint64_t rdi, uint64_t rcx, uint64_t rsi, uint64_t r8) {
    uint64_t var_0;
    uint64_t var_1;
    uint64_t var_2;
    uint64_t var_3;
    uint64_t var_4;
    uint64_t var_5;
    uint64_t var_6;
    uint64_t var_7;
    uint64_t var_8;
    bool var_9;
    uint64_t var_10;
    uint64_t local_sp_5;
    uint64_t var_49;
    uint64_t var_50;
    uint64_t r13_1;
    uint64_t rax_0_be;
    uint64_t rbx_0_be;
    uint64_t rbp_0;
    uint64_t local_sp_0;
    uint64_t rbp_0_be;
    uint64_t local_sp_0_be;
    uint64_t rax_0;
    uint64_t rbp_1;
    uint64_t local_sp_1;
    uint64_t rbx_1;
    uint32_t *var_11;
    uint32_t var_12;
    uint64_t var_13;
    uint32_t *_pre_phi;
    uint64_t rax_0_ph;
    uint64_t r14_0_ph;
    uint64_t var_14;
    bool var_15;
    uint64_t rbx_0;
    uint64_t var_16;
    uint64_t var_17;
    uint64_t rax_1;
    uint64_t var_18;
    uint64_t var_19;
    uint64_t var_20;
    uint64_t var_21;
    uint64_t var_22;
    uint64_t rbx_2;
    uint64_t rbp_2;
    uint64_t r13_0;
    uint64_t local_sp_2;
    bool var_23;
    uint64_t var_24;
    uint64_t var_25;
    uint64_t rbp_5;
    uint64_t var_26;
    uint64_t var_27;
    uint64_t *var_28;
    uint64_t var_29;
    uint64_t *var_30;
    uint64_t var_31;
    uint64_t local_sp_496;
    uint64_t *var_32;
    uint64_t var_33;
    uint64_t *var_34;
    uint64_t local_sp_3;
    uint64_t var_35;
    uint64_t *var_36;
    uint64_t var_37;
    uint64_t rbx_5;
    uint64_t rbx_3;
    uint64_t var_38;
    unsigned char var_39;
    uint32_t var_40;
    uint64_t var_41;
    uint64_t r13_295;
    uint32_t var_42;
    uint64_t var_43;
    uint64_t rbp_394;
    uint64_t rbx_493;
    uint64_t rbp_4;
    uint64_t r13_3;
    uint64_t var_44;
    uint64_t var_45;
    uint64_t var_46;
    uint64_t *var_47;
    uint64_t var_48;
    uint64_t local_sp_6;
    uint64_t var_51;
    uint64_t *var_52;
    bool switch_state_var;
    var_0 = revng_init_local_sp(0UL);
    var_1 = init_rbx();
    var_2 = init_r14();
    var_3 = init_rbp();
    var_4 = init_r13();
    var_5 = init_r12();
    var_6 = init_r15();
    *(uint64_t *)(var_0 + (-8L)) = var_6;
    *(uint64_t *)(var_0 + (-16L)) = var_2;
    *(uint64_t *)(var_0 + (-24L)) = var_4;
    *(uint64_t *)(var_0 + (-32L)) = var_5;
    *(uint64_t *)(var_0 + (-40L)) = var_3;
    *(uint64_t *)(var_0 + (-48L)) = var_1;
    var_7 = var_0 + (-104L);
    *(uint64_t *)(var_0 + (-80L)) = rcx;
    *(uint64_t *)(var_0 + (-88L)) = r8;
    var_8 = *(uint64_t *)r8;
    *(uint64_t *)(var_0 + (-64L)) = var_8;
    var_9 = (rdi != 0UL);
    var_10 = var_9;
    rbp_0 = rdx;
    local_sp_0 = var_7;
    rax_0_ph = var_8;
    r14_0_ph = 0UL;
    rbx_0 = rsi;
    rbp_4 = 8UL;
    if (rdi == 0UL) {
        *(uint32_t *)(var_0 + (-92L)) = 0U;
        _pre_phi = (uint32_t *)rdi;
    } else {
        var_11 = (uint32_t *)rdi;
        var_12 = *var_11;
        var_13 = (uint64_t)var_12;
        *(uint32_t *)(var_0 + (-92L)) = var_12;
        _pre_phi = var_11;
        rax_0_ph = var_13;
        r14_0_ph = (rdx == 0UL);
    }
    var_14 = rdi + 4UL;
    var_15 = (r14_0_ph < var_10);
    rax_0 = rax_0_ph;
    rbx_493 = var_14;
    while (1U)
        {
            var_16 = *(uint64_t *)(local_sp_0 + 40UL);
            var_17 = (*(uint32_t *)(local_sp_0 + 12UL) == 0U);
            rbp_1 = rbp_0;
            local_sp_1 = local_sp_0;
            rbx_1 = rbx_0;
            rax_1 = (rax_0 & (-256L)) | var_17;
            rbx_2 = rbx_0;
            rbp_2 = rbp_0;
            r13_0 = var_16;
            local_sp_2 = local_sp_0;
            if (r14_0_ph < var_17) {
                var_18 = local_sp_1 + 40UL;
                var_19 = *(uint64_t *)var_18;
                var_20 = local_sp_1 + 24UL;
                var_21 = local_sp_1 + (-8L);
                *(uint64_t *)var_21 = 4205330UL;
                var_22 = indirect_placeholder_23(var_20, rbx_1, var_18, rbp_1);
                local_sp_1 = var_21;
                rax_1 = var_22;
                rbx_2 = rbx_1;
                rbp_2 = rbp_1;
                r13_0 = var_19;
                local_sp_2 = var_21;
                while ((uint64_t)(unsigned char)var_22 != 0UL)
                    {
                        rbx_1 = rbx_1 + 8UL;
                        rbp_1 = rbp_1 + (-8L);
                        var_18 = local_sp_1 + 40UL;
                        var_19 = *(uint64_t *)var_18;
                        var_20 = local_sp_1 + 24UL;
                        var_21 = local_sp_1 + (-8L);
                        *(uint64_t *)var_21 = 4205330UL;
                        var_22 = indirect_placeholder_23(var_20, rbx_1, var_18, rbp_1);
                        local_sp_1 = var_21;
                        rax_1 = var_22;
                        rbx_2 = rbx_1;
                        rbp_2 = rbp_1;
                        r13_0 = var_19;
                        local_sp_2 = var_21;
                    }
            }
            var_23 = (rbp_2 == 0UL);
            var_24 = var_23;
            var_25 = (rax_1 & (-256L)) | var_24;
            r13_1 = rbx_2;
            rax_0_be = var_25;
            local_sp_0_be = local_sp_2;
            rbp_5 = rbp_2;
            local_sp_496 = local_sp_2;
            local_sp_3 = local_sp_2;
            rbx_5 = rbx_2;
            rbx_3 = rbx_2;
            rbp_394 = rbp_2;
            local_sp_6 = local_sp_2;
            if (r14_0_ph < var_24) {
                break;
            }
            rbp_5 = 0UL;
            if (var_23) {
                var_32 = (uint64_t *)(local_sp_2 + 40UL);
                var_33 = *var_32 - r13_0;
                var_34 = (uint64_t *)(local_sp_2 + 24UL);
                *var_34 = (*var_34 + var_33);
                *var_32 = r13_0;
                if (var_9) {
                    break;
                }
            }
            rbx_493 = rbx_2;
            if (!(((*(unsigned char *)rbx_2 == '\n') ^ 1) || (var_9 ^ 1))) {
                var_26 = rbx_2 + 1UL;
                var_27 = rbp_2 + (-1L);
                rbx_0_be = var_26;
                rbp_0_be = var_27;
                rax_0 = rax_0_be;
                rbx_0 = rbx_0_be;
                rbp_0 = rbp_0_be;
                local_sp_0 = local_sp_0_be;
                continue;
            }
            var_28 = (uint64_t *)(local_sp_2 + 40UL);
            var_29 = *var_28 - r13_0;
            var_30 = (uint64_t *)(local_sp_2 + 24UL);
            *var_30 = (*var_30 + var_29);
            *var_28 = r13_0;
            var_31 = rbp_2 + rbx_2;
            *(uint64_t *)local_sp_2 = rbx_2;
            r13_1 = var_31;
            r13_295 = var_31;
            if (!var_9) {
                local_sp_5 = local_sp_496;
                rbx_5 = rbx_493;
                rbp_4 = rbp_394;
                r13_3 = r13_295;
                local_sp_6 = local_sp_496;
                if ((rbp_394 < 8UL) && var_15) {
                    break;
                }
                var_44 = local_sp_5 + 40UL;
                var_45 = local_sp_5 + 24UL;
                var_46 = local_sp_5 + (-8L);
                var_47 = (uint64_t *)var_46;
                *var_47 = 4205603UL;
                var_48 = indirect_placeholder_23(var_45, rbx_5, var_44, rbp_4);
                local_sp_0_be = var_46;
                rbp_5 = rbp_4;
                local_sp_6 = var_46;
                if ((uint64_t)(unsigned char)var_48 == 0UL) {
                    break;
                }
                var_49 = *var_47;
                var_50 = r13_3 - var_49;
                rax_0_be = var_49;
                rbx_0_be = var_49;
                rbp_0_be = var_50;
                rax_0 = rax_0_be;
                rbx_0 = rbx_0_be;
                rbp_0 = rbp_0_be;
                local_sp_0 = local_sp_0_be;
                continue;
            }
            switch_state_var = 0;
            switch (*_pre_phi) {
              case 8U:
                {
                    *_pre_phi = 0U;
                }
                break;
              case 0U:
                {
                    var_35 = local_sp_2 + (-8L);
                    var_36 = (uint64_t *)var_35;
                    *var_36 = 4205475UL;
                    var_37 = indirect_placeholder_13(8UL, rbx_2, 10UL);
                    local_sp_3 = var_35;
                    local_sp_5 = var_35;
                    if ((long)rbp_2 <= (long)7UL & var_37 != 0UL) {
                        *var_36 = (rbx_2 + 8UL);
                        var_44 = local_sp_5 + 40UL;
                        var_45 = local_sp_5 + 24UL;
                        var_46 = local_sp_5 + (-8L);
                        var_47 = (uint64_t *)var_46;
                        *var_47 = 4205603UL;
                        var_48 = indirect_placeholder_23(var_45, rbx_5, var_44, rbp_4);
                        local_sp_0_be = var_46;
                        rbp_5 = rbp_4;
                        local_sp_6 = var_46;
                        if ((uint64_t)(unsigned char)var_48 == 0UL) {
                            switch_state_var = 1;
                            break;
                        }
                        var_49 = *var_47;
                        var_50 = r13_3 - var_49;
                        rax_0_be = var_49;
                        rbx_0_be = var_49;
                        rbp_0_be = var_50;
                        rax_0 = rax_0_be;
                        rbx_0 = rbx_0_be;
                        rbp_0 = rbp_0_be;
                        local_sp_0 = local_sp_0_be;
                        continue;
                    }
                }
                break;
            }
            if (switch_state_var)
                break;
        }
    var_51 = *(uint64_t *)(local_sp_6 + 40UL);
    var_52 = (uint64_t *)*(uint64_t *)(local_sp_6 + 16UL);
    *var_52 = (*var_52 - var_51);
    return (var_51 & (-256L)) | (rbp_5 == 0UL);
}

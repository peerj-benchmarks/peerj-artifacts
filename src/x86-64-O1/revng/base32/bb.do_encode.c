typedef unsigned char __u_char;
typedef unsigned short __u_short;
typedef unsigned int __u_int;
typedef unsigned long __u_long;
typedef signed char __int8_t;
typedef unsigned char __uint8_t;
typedef short __int16_t;
typedef unsigned short __uint16_t;
typedef int __int32_t;
typedef unsigned int __uint32_t;
typedef long __int64_t;
typedef unsigned long __uint64_t;
typedef __int8_t __int_least8_t;
typedef __uint8_t __uint_least8_t;
typedef __int16_t __int_least16_t;
typedef __uint16_t __uint_least16_t;
typedef __int32_t __int_least32_t;
typedef __uint32_t __uint_least32_t;
typedef __int64_t __int_least64_t;
typedef __uint64_t __uint_least64_t;
typedef long __quad_t;
typedef unsigned long __u_quad_t;
typedef long __intmax_t;
typedef unsigned long __uintmax_t;
typedef unsigned long __dev_t;
typedef unsigned int __uid_t;
typedef unsigned int __gid_t;
typedef unsigned long __ino_t;
typedef unsigned long __ino64_t;
typedef unsigned int __mode_t;
typedef unsigned long __nlink_t;
typedef long __off_t;
typedef long __off64_t;
typedef int __pid_t;
typedef struct {
    int __val[2];
} __fsid_t;
typedef long __clock_t;
typedef unsigned long __rlim_t;
typedef unsigned long __rlim64_t;
typedef unsigned int __id_t;
typedef long __time_t;
typedef unsigned int __useconds_t;
typedef long __suseconds_t;
typedef int __daddr_t;
typedef int __key_t;
typedef int __clockid_t;
typedef void *__timer_t;
typedef long __blksize_t;
typedef long __blkcnt_t;
typedef long __blkcnt64_t;
typedef unsigned long __fsblkcnt_t;
typedef unsigned long __fsblkcnt64_t;
typedef unsigned long __fsfilcnt_t;
typedef unsigned long __fsfilcnt64_t;
typedef long __fsword_t;
typedef long __ssize_t;
typedef long __syscall_slong_t;
typedef unsigned long __syscall_ulong_t;
typedef __off64_t __loff_t;
typedef char *__caddr_t;
typedef long __intptr_t;
typedef unsigned int __socklen_t;
typedef int __sig_atomic_t;
typedef __int8_t int8_t;
typedef __int16_t int16_t;
typedef __int32_t int32_t;
typedef __int64_t int64_t;
typedef __uint8_t uint8_t;
typedef __uint16_t uint16_t;
typedef __uint32_t uint32_t;
typedef __uint64_t uint64_t;
typedef __int_least8_t int_least8_t;
typedef __int_least16_t int_least16_t;
typedef __int_least32_t int_least32_t;
typedef __int_least64_t int_least64_t;
typedef __uint_least8_t uint_least8_t;
typedef __uint_least16_t uint_least16_t;
typedef __uint_least32_t uint_least32_t;
typedef __uint_least64_t uint_least64_t;
typedef signed char int_fast8_t;
typedef long int_fast16_t;
typedef long int_fast32_t;
typedef long int_fast64_t;
typedef unsigned char uint_fast8_t;
typedef unsigned long uint_fast16_t;
typedef unsigned long uint_fast32_t;
typedef unsigned long uint_fast64_t;
typedef long intptr_t;
typedef unsigned long uintptr_t;
typedef __intmax_t intmax_t;
typedef __uintmax_t uintmax_t;
struct indirect_placeholder_4_ret_type;
struct indirect_placeholder_5_ret_type;
struct indirect_placeholder_6_ret_type;
struct indirect_placeholder_7_ret_type;
struct indirect_placeholder_8_ret_type;
struct indirect_placeholder_4_ret_type {
    uint64_t field_0;
    uint64_t field_1;
    uint64_t field_2;
};
struct indirect_placeholder_5_ret_type {
    uint64_t field_0;
    uint64_t field_1;
    uint64_t field_2;
};
struct indirect_placeholder_6_ret_type {
    uint64_t field_0;
    uint64_t field_1;
    uint64_t field_2;
};
struct indirect_placeholder_7_ret_type {
    uint64_t field_0;
    uint64_t field_1;
    uint64_t field_2;
};
struct indirect_placeholder_8_ret_type {
    uint64_t field_0;
    uint64_t field_1;
    uint64_t field_2;
};
extern uint64_t revng_init_local_sp(uint64_t param_0);
extern uint64_t init_rbx(void);
extern void indirect_placeholder(void);
extern uint64_t init_rbp(void);
extern uint64_t init_r14(void);
extern uint64_t init_r13(void);
extern uint64_t init_r12(void);
extern uint64_t init_r15(void);
extern uint64_t init_r8(void);
extern uint64_t init_r9(void);
extern void indirect_placeholder_9(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint64_t param_3, uint64_t param_4);
extern struct indirect_placeholder_4_ret_type indirect_placeholder_4(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint64_t param_3, uint64_t param_4, uint64_t param_5, uint64_t param_6);
extern struct indirect_placeholder_5_ret_type indirect_placeholder_5(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint64_t param_3, uint64_t param_4, uint64_t param_5, uint64_t param_6);
extern struct indirect_placeholder_6_ret_type indirect_placeholder_6(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint64_t param_3, uint64_t param_4, uint64_t param_5, uint64_t param_6);
extern struct indirect_placeholder_7_ret_type indirect_placeholder_7(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint64_t param_3, uint64_t param_4, uint64_t param_5, uint64_t param_6);
extern struct indirect_placeholder_8_ret_type indirect_placeholder_8(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint64_t param_3, uint64_t param_4, uint64_t param_5, uint64_t param_6);
typedef _Bool bool;
void bb_do_encode(uint64_t rdx, uint64_t rdi, uint64_t rsi) {
    struct indirect_placeholder_4_ret_type var_50;
    uint64_t var_54;
    uint64_t var_0;
    uint64_t var_1;
    uint64_t var_2;
    uint64_t var_3;
    uint64_t var_4;
    uint64_t var_5;
    uint64_t var_6;
    uint64_t var_7;
    uint64_t var_8;
    uint64_t var_9;
    uint64_t local_sp_9;
    uint64_t rax_2;
    uint64_t local_sp_0;
    uint64_t r14_5;
    uint64_t r9_5;
    uint64_t r14_0;
    uint64_t rcx_0;
    uint64_t r9_0;
    uint64_t r8_0;
    uint64_t rcx_1;
    uint64_t r8_5;
    uint64_t rcx_2;
    uint64_t var_48;
    uint64_t var_49;
    uint64_t r9_2;
    uint64_t r8_2;
    uint64_t var_51;
    uint64_t var_52;
    uint64_t var_53;
    uint64_t r14_3;
    uint64_t r13_3;
    uint64_t var_45;
    uint64_t var_46;
    uint64_t r8_8;
    struct indirect_placeholder_5_ret_type var_47;
    uint64_t local_sp_2;
    uint64_t rax_5;
    uint64_t var_43;
    uint64_t local_sp_1_ph;
    uint64_t local_sp_1;
    uint64_t rax_1;
    uint64_t r9_8;
    uint64_t r14_1;
    uint64_t r13_0;
    uint64_t rcx_4;
    uint64_t r9_4;
    uint64_t r8_4;
    uint64_t r9_1;
    uint64_t r8_1;
    uint64_t var_44;
    uint64_t r9_7;
    uint64_t var_37;
    uint64_t var_38;
    uint64_t r8_7;
    struct indirect_placeholder_6_ret_type var_39;
    uint64_t local_sp_7;
    uint64_t local_sp_3;
    uint64_t rax_3;
    uint64_t rcx_3;
    uint64_t r9_3;
    uint64_t r8_3;
    uint64_t r13_2;
    uint64_t var_40;
    uint64_t rcx_6;
    uint64_t var_41;
    uint64_t r14_4;
    uint64_t var_31;
    uint64_t var_32;
    uint64_t r9_6;
    uint64_t r8_6;
    struct indirect_placeholder_7_ret_type var_33;
    uint64_t local_sp_4;
    uint64_t rax_4;
    uint64_t r14_2;
    uint64_t local_sp_6;
    uint64_t var_24;
    uint64_t var_25;
    struct indirect_placeholder_8_ret_type var_26;
    uint64_t local_sp_6_ph;
    uint64_t local_sp_5;
    uint64_t r13_1;
    uint64_t rcx_5;
    uint64_t var_42;
    uint64_t r13_2_ph;
    uint64_t rcx_6_ph;
    uint64_t r9_6_ph;
    uint64_t r8_6_ph;
    uint64_t var_27;
    uint64_t var_28;
    uint64_t var_29;
    uint64_t var_30;
    uint64_t var_34;
    uint64_t var_35;
    uint64_t var_36;
    uint64_t var_22;
    uint64_t var_23;
    uint64_t rbx_0;
    uint64_t var_14;
    uint64_t local_sp_8112;
    uint64_t local_sp_10;
    unsigned __int128 var_15;
    uint64_t var_16;
    uint64_t var_17;
    uint64_t var_18;
    uint64_t var_19;
    uint64_t var_20;
    uint64_t *var_21;
    uint64_t var_13;
    uint64_t var_10;
    uint64_t var_11;
    uint64_t var_12;
    unsigned int loop_state_var;
    bool switch_state_var;
    var_0 = revng_init_local_sp(0UL);
    var_1 = init_rbx();
    var_2 = init_r14();
    var_3 = init_rbp();
    var_4 = init_r13();
    var_5 = init_r12();
    var_6 = init_r15();
    var_7 = init_r9();
    var_8 = init_r8();
    *(uint64_t *)(var_0 + (-8L)) = var_6;
    *(uint64_t *)(var_0 + (-16L)) = var_2;
    *(uint64_t *)(var_0 + (-24L)) = var_4;
    *(uint64_t *)(var_0 + (-32L)) = var_5;
    *(uint64_t *)(var_0 + (-40L)) = var_3;
    *(uint64_t *)(var_0 + (-48L)) = var_1;
    var_9 = var_0 + (-79944L);
    *(uint64_t *)(var_0 + (-79936L)) = rsi;
    *(uint64_t *)var_9 = rdx;
    local_sp_9 = var_9;
    local_sp_0 = (uint64_t)0L;
    r14_5 = 0UL;
    r14_0 = (uint64_t)0L;
    rcx_0 = (uint64_t)0L;
    r9_0 = (uint64_t)0L;
    r8_0 = (uint64_t)0L;
    rcx_1 = rdi;
    r13_3 = var_4;
    r8_8 = var_8;
    rax_1 = 0UL;
    r9_8 = var_7;
    r14_2 = 0UL;
    rcx_5 = 0UL;
    rbx_0 = 0UL;
    while (1U)
        {
            r9_5 = r9_8;
            r8_5 = r8_8;
            r14_3 = r14_5;
            r14_1 = r14_5;
            r9_1 = r9_8;
            r8_1 = r8_8;
            r14_4 = r14_5;
            r13_1 = r13_3;
            r13_2_ph = r13_3;
            r9_6_ph = r9_8;
            r8_6_ph = r8_8;
            local_sp_10 = local_sp_9;
            while (1U)
                {
                    var_10 = local_sp_10 + 49168UL;
                    *(uint64_t *)(local_sp_10 + (-8L)) = 4202220UL;
                    indirect_placeholder();
                    var_11 = rbx_0 + var_10;
                    var_12 = local_sp_10 + (-16L);
                    *(uint64_t *)var_12 = 4202231UL;
                    indirect_placeholder();
                    local_sp_8112 = var_12;
                    rbx_0 = var_11;
                    if ((uint64_t)(uint32_t)var_10 != 0UL) {
                        loop_state_var = 1U;
                        break;
                    }
                    var_14 = local_sp_10 + (-24L);
                    *(uint64_t *)var_14 = 4202243UL;
                    indirect_placeholder();
                    local_sp_8112 = var_14;
                    local_sp_10 = var_14;
                    if (var_11 > 30719UL) {
                        continue;
                    }
                    loop_state_var = 0U;
                    break;
                }
            switch_state_var = 0;
            switch (loop_state_var) {
              case 0U:
                {
                    var_15 = (unsigned __int128)(var_11 + 4UL) * 14757395258967641293ULL;
                    var_16 = (uint64_t)var_15;
                    var_17 = (uint64_t)((var_15 >> 66ULL) << 3ULL);
                    var_18 = local_sp_8112 + 16UL;
                    var_19 = local_sp_8112 + 49168UL;
                    var_20 = local_sp_8112 + (-8L);
                    var_21 = (uint64_t *)var_20;
                    *var_21 = 4202318UL;
                    indirect_placeholder_9(var_16, var_18, var_19, var_17, var_11);
                    local_sp_5 = var_20;
                    rax_5 = var_16;
                    local_sp_6_ph = var_20;
                    rcx_6_ph = var_17;
                    if (*var_21 != 0UL) {
                        r13_1 = 0UL;
                        r13_2_ph = 0UL;
                        if (var_17 != 0UL) {
                            var_42 = local_sp_5 + (-8L);
                            *(uint64_t *)var_42 = 4202566UL;
                            indirect_placeholder();
                            r14_5 = r14_3;
                            rcx_1 = rcx_5;
                            r13_3 = r13_1;
                            r8_8 = r8_5;
                            local_sp_1_ph = var_42;
                            rax_1 = rax_5;
                            r9_8 = r9_5;
                            r14_1 = r14_3;
                            r9_1 = r9_5;
                            r8_1 = r8_5;
                            if ((uint64_t)(uint32_t)rax_5 != 0UL) {
                                loop_state_var = 1U;
                                switch_state_var = 1;
                                break;
                            }
                            var_43 = local_sp_5 + (-16L);
                            *(uint64_t *)var_43 = 4202578UL;
                            indirect_placeholder();
                            local_sp_1_ph = var_43;
                            local_sp_9 = var_43;
                            if (var_11 == 30720UL) {
                                continue;
                            }
                            loop_state_var = 1U;
                            switch_state_var = 1;
                            break;
                        }
                    }
                    var_22 = *(uint64_t *)6362240UL;
                    var_23 = local_sp_8112 + (-16L);
                    *(uint64_t *)var_23 = 4202366UL;
                    indirect_placeholder();
                    local_sp_5 = var_23;
                    rcx_5 = var_22;
                    if (var_17 <= var_16) {
                        var_42 = local_sp_5 + (-8L);
                        *(uint64_t *)var_42 = 4202566UL;
                        indirect_placeholder();
                        r14_5 = r14_3;
                        rcx_1 = rcx_5;
                        r13_3 = r13_1;
                        r8_8 = r8_5;
                        local_sp_1_ph = var_42;
                        rax_1 = rax_5;
                        r9_8 = r9_5;
                        r14_1 = r14_3;
                        r9_1 = r9_5;
                        r8_1 = r8_5;
                        if ((uint64_t)(uint32_t)rax_5 != 0UL) {
                            loop_state_var = 1U;
                            switch_state_var = 1;
                            break;
                        }
                        var_43 = local_sp_5 + (-16L);
                        *(uint64_t *)var_43 = 4202578UL;
                        indirect_placeholder();
                        local_sp_1_ph = var_43;
                        local_sp_9 = var_43;
                        if (var_11 == 30720UL) {
                            continue;
                        }
                        loop_state_var = 1U;
                        switch_state_var = 1;
                        break;
                    }
                    *(uint64_t *)(local_sp_8112 + (-24L)) = 4202380UL;
                    indirect_placeholder();
                    var_24 = (uint64_t)*(uint32_t *)var_16;
                    var_25 = local_sp_8112 + (-32L);
                    *(uint64_t *)var_25 = 4202402UL;
                    var_26 = indirect_placeholder_8(0UL, 4251019UL, 1UL, var_22, var_24, r9_8, r8_8);
                    local_sp_6_ph = var_25;
                    rcx_6_ph = var_26.field_0;
                    r9_6_ph = var_26.field_1;
                    r8_6_ph = var_26.field_2;
                }
                break;
              case 1U:
                {
                    rax_1 = var_10;
                    if (var_11 != 0UL) {
                        var_13 = var_12 + (-8L);
                        *(uint64_t *)var_13 = 4202711UL;
                        indirect_placeholder();
                        local_sp_1 = var_13;
                        if (!0) {
                            loop_state_var = 2U;
                            switch_state_var = 1;
                            break;
                        }
                        loop_state_var = 0U;
                        switch_state_var = 1;
                        break;
                    }
                }
                break;
            }
            if (switch_state_var)
                break;
        }
    switch (loop_state_var) {
      case 1U:
        {
            local_sp_1 = local_sp_1_ph;
        }
        break;
      case 0U:
        {
            var_54 = local_sp_0 + (-8L);
            *(uint64_t *)var_54 = 4202701UL;
            indirect_placeholder();
            rcx_1 = rcx_0;
            local_sp_1 = var_54;
            r14_1 = r14_0;
            r9_1 = r9_0;
            r8_1 = r8_0;
        }
        break;
      case 2U:
        {
            while (1U)
                {
                    rax_2 = rax_1;
                    r14_0 = r14_1;
                    rcx_2 = rcx_1;
                    r9_2 = r9_1;
                    r8_2 = r8_1;
                    local_sp_2 = local_sp_1;
                    var_44 = local_sp_1 + (-8L);
                    *(uint64_t *)var_44 = 4202622UL;
                    indirect_placeholder();
                    local_sp_2 = var_44;
                    if (!((*(uint64_t *)local_sp_1 == 0UL) || (r14_1 == 0UL)) && (uint64_t)((uint32_t)rax_1 + 1U) == 0UL) {
                        *(uint64_t *)(local_sp_1 + (-16L)) = 4202632UL;
                        indirect_placeholder();
                        var_45 = (uint64_t)*(uint32_t *)rax_1;
                        var_46 = local_sp_1 + (-24L);
                        *(uint64_t *)var_46 = 4202654UL;
                        var_47 = indirect_placeholder_5(0UL, 4251019UL, 1UL, rcx_1, var_45, r9_1, r8_1);
                        rax_2 = 0UL;
                        rcx_2 = var_47.field_0;
                        r9_2 = var_47.field_1;
                        r8_2 = var_47.field_2;
                        local_sp_2 = var_46;
                    }
                    *(uint64_t *)(local_sp_2 + (-8L)) = 4202662UL;
                    indirect_placeholder();
                    if ((uint64_t)(uint32_t)rax_2 == 0UL) {
                        break;
                    }
                    *(uint64_t *)(local_sp_2 + (-16L)) = 4202671UL;
                    indirect_placeholder();
                    var_48 = (uint64_t)*(uint32_t *)rax_2;
                    var_49 = local_sp_2 + (-24L);
                    *(uint64_t *)var_49 = 4202693UL;
                    var_50 = indirect_placeholder_4(0UL, 4251008UL, 1UL, rcx_2, var_48, r9_2, r8_2);
                    var_51 = var_50.field_0;
                    var_52 = var_50.field_1;
                    var_53 = var_50.field_2;
                    local_sp_0 = var_49;
                    rcx_0 = var_51;
                    r9_0 = var_52;
                    r8_0 = var_53;
                    var_54 = local_sp_0 + (-8L);
                    *(uint64_t *)var_54 = 4202701UL;
                    indirect_placeholder();
                    rcx_1 = rcx_0;
                    local_sp_1 = var_54;
                    r14_1 = r14_0;
                    r9_1 = r9_0;
                    r8_1 = r8_0;
                    continue;
                }
            return;
        }
        break;
    }
}

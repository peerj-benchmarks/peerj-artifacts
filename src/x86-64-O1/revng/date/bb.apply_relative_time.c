typedef unsigned char __u_char;
typedef unsigned short __u_short;
typedef unsigned int __u_int;
typedef unsigned long __u_long;
typedef signed char __int8_t;
typedef unsigned char __uint8_t;
typedef short __int16_t;
typedef unsigned short __uint16_t;
typedef int __int32_t;
typedef unsigned int __uint32_t;
typedef long __int64_t;
typedef unsigned long __uint64_t;
typedef __int8_t __int_least8_t;
typedef __uint8_t __uint_least8_t;
typedef __int16_t __int_least16_t;
typedef __uint16_t __uint_least16_t;
typedef __int32_t __int_least32_t;
typedef __uint32_t __uint_least32_t;
typedef __int64_t __int_least64_t;
typedef __uint64_t __uint_least64_t;
typedef long __quad_t;
typedef unsigned long __u_quad_t;
typedef long __intmax_t;
typedef unsigned long __uintmax_t;
typedef unsigned long __dev_t;
typedef unsigned int __uid_t;
typedef unsigned int __gid_t;
typedef unsigned long __ino_t;
typedef unsigned long __ino64_t;
typedef unsigned int __mode_t;
typedef unsigned long __nlink_t;
typedef long __off_t;
typedef long __off64_t;
typedef int __pid_t;
typedef struct {
    int __val[2];
} __fsid_t;
typedef long __clock_t;
typedef unsigned long __rlim_t;
typedef unsigned long __rlim64_t;
typedef unsigned int __id_t;
typedef long __time_t;
typedef unsigned int __useconds_t;
typedef long __suseconds_t;
typedef int __daddr_t;
typedef int __key_t;
typedef int __clockid_t;
typedef void *__timer_t;
typedef long __blksize_t;
typedef long __blkcnt_t;
typedef long __blkcnt64_t;
typedef unsigned long __fsblkcnt_t;
typedef unsigned long __fsblkcnt64_t;
typedef unsigned long __fsfilcnt_t;
typedef unsigned long __fsfilcnt64_t;
typedef long __fsword_t;
typedef long __ssize_t;
typedef long __syscall_slong_t;
typedef unsigned long __syscall_ulong_t;
typedef __off64_t __loff_t;
typedef char *__caddr_t;
typedef long __intptr_t;
typedef unsigned int __socklen_t;
typedef int __sig_atomic_t;
typedef __int8_t int8_t;
typedef __int16_t int16_t;
typedef __int32_t int32_t;
typedef __int64_t int64_t;
typedef __uint8_t uint8_t;
typedef __uint16_t uint16_t;
typedef __uint32_t uint32_t;
typedef __uint64_t uint64_t;
typedef __int_least8_t int_least8_t;
typedef __int_least16_t int_least16_t;
typedef __int_least32_t int_least32_t;
typedef __int_least64_t int_least64_t;
typedef __uint_least8_t uint_least8_t;
typedef __uint_least16_t uint_least16_t;
typedef __uint_least32_t uint_least32_t;
typedef __uint_least64_t uint_least64_t;
typedef signed char int_fast8_t;
typedef long int_fast16_t;
typedef long int_fast32_t;
typedef long int_fast64_t;
typedef unsigned char uint_fast8_t;
typedef unsigned long uint_fast16_t;
typedef unsigned long uint_fast32_t;
typedef unsigned long uint_fast64_t;
typedef long intptr_t;
typedef unsigned long uintptr_t;
typedef __intmax_t intmax_t;
typedef __uintmax_t uintmax_t;
typedef _Bool bool;
extern uint64_t revng_init_local_sp(uint64_t param_0);
void bb_apply_relative_time(uint64_t rdi, uint64_t rsi) {
    uint64_t var_0;
    uint64_t var_1;
    uint64_t var_2;
    uint64_t var_3;
    uint64_t var_4;
    uint64_t var_5;
    uint64_t var_6;
    uint32_t var_7;
    uint64_t var_8;
    bool var_9;
    bool var_10;
    uint64_t var_11;
    uint32_t *var_12;
    uint32_t *_pre_phi237;
    uint64_t var_13;
    uint32_t *var_14;
    uint64_t storemerge;
    bool var_15;
    uint64_t _;
    uint64_t var_16;
    uint64_t *var_17;
    uint64_t *_pre_phi239;
    uint64_t var_18;
    uint64_t *var_19;
    uint64_t storemerge2;
    bool var_20;
    uint64_t var_21;
    uint64_t var_22;
    uint64_t *var_23;
    uint64_t *_pre_phi241;
    uint64_t var_24;
    uint64_t *var_25;
    uint64_t storemerge4;
    bool var_26;
    uint64_t var_27;
    uint64_t storemerge46;
    uint64_t var_28;
    uint64_t *var_29;
    uint64_t *_pre_phi243;
    uint64_t var_30;
    uint64_t *var_31;
    uint64_t storemerge7;
    bool var_32;
    uint64_t var_33;
    uint64_t var_34;
    uint64_t *var_63;
    uint64_t *var_35;
    uint64_t *_pre_phi245;
    uint64_t var_36;
    uint64_t *var_37;
    uint64_t storemerge10;
    bool var_38;
    uint64_t _213;
    uint64_t var_39;
    uint64_t *var_40;
    uint64_t *_pre_phi247;
    uint64_t var_41;
    uint64_t *var_42;
    uint64_t storemerge13;
    bool var_43;
    uint64_t _214;
    uint64_t _masked15;
    uint64_t var_44;
    uint64_t *var_45;
    uint64_t *_pre_phi249;
    uint64_t var_46;
    uint64_t *var_47;
    uint64_t storemerge16;
    bool var_48;
    uint64_t var_49;
    uint32_t *var_50;
    uint32_t *_pre_phi223;
    uint64_t var_51;
    uint32_t *var_52;
    uint64_t storemerge26;
    bool var_53;
    uint64_t _216;
    uint64_t var_54;
    uint64_t *var_55;
    uint64_t *_pre_phi225;
    uint64_t var_56;
    uint64_t *var_57;
    uint64_t storemerge28;
    bool var_58;
    uint64_t var_59;
    uint64_t var_60;
    uint64_t *var_61;
    uint64_t *_pre_phi227;
    uint64_t var_62;
    uint64_t storemerge31;
    bool var_64;
    uint64_t var_65;
    uint64_t var_66;
    uint64_t *var_67;
    uint64_t *_pre_phi229;
    uint64_t var_68;
    uint64_t *var_69;
    uint64_t storemerge34;
    bool var_70;
    uint64_t var_71;
    uint64_t var_72;
    uint64_t *var_73;
    uint64_t *_pre_phi231;
    uint64_t var_74;
    uint64_t *var_75;
    uint64_t storemerge37;
    bool var_76;
    uint64_t _220;
    uint64_t var_77;
    uint64_t *var_78;
    uint64_t *_pre_phi233;
    uint64_t var_79;
    uint64_t *var_80;
    uint64_t storemerge40;
    bool var_81;
    uint64_t _221;
    uint64_t _masked42;
    uint64_t var_82;
    uint64_t *var_83;
    uint64_t *_pre_phi235;
    uint64_t var_84;
    uint64_t *var_85;
    uint64_t storemerge43;
    bool var_86;
    var_0 = revng_init_local_sp(0UL);
    var_1 = *(uint64_t *)(var_0 | 8UL);
    var_2 = *(uint64_t *)(var_0 | 16UL);
    var_3 = *(uint64_t *)(var_0 | 24UL);
    var_4 = *(uint64_t *)(var_0 | 32UL);
    var_5 = *(uint64_t *)(var_0 | 40UL);
    var_6 = *(uint64_t *)(var_0 | 48UL);
    var_7 = *(uint32_t *)(var_0 | 56UL);
    var_8 = (uint64_t)var_7;
    var_9 = ((int)(uint32_t)rsi > (int)4294967295U);
    var_10 = ((int)var_7 > (int)4294967295U);
    if (var_9) {
        if (var_10) {
            var_51 = 2147483647UL - var_8;
            var_52 = (uint32_t *)(rdi + 152UL);
            _pre_phi223 = var_52;
            storemerge26 = (uint64_t)((uint32_t)var_51 & (-256)) | ((long)(var_51 << 32UL) < (long)((uint64_t)*var_52 << 32UL));
        } else {
            var_49 = 2147483648UL - var_8;
            var_50 = (uint32_t *)(rdi + 152UL);
            _pre_phi223 = var_50;
            storemerge26 = (uint64_t)((uint32_t)var_49 & (-256)) | ((long)(uint64_t)((long)(var_49 << 32UL) >> (long)32UL) > (long)(uint64_t)*var_50);
        }
        var_53 = ((uint64_t)(unsigned char)storemerge26 == 0UL);
        *_pre_phi223 = (*_pre_phi223 + var_7);
        _216 = var_53 ? 0UL : 1UL;
        if ((long)var_6 > (long)18446744073709551615UL) {
            var_56 = 9223372036854775807UL - var_6;
            var_57 = (uint64_t *)(rdi + 144UL);
            _pre_phi225 = var_57;
            storemerge28 = (var_56 & (-256L)) | ((long)var_56 < (long)*var_57);
        } else {
            var_54 = 9223372036854775808UL - (-9223372036854775808L);
            var_55 = (uint64_t *)(rdi + 144UL);
            _pre_phi225 = var_55;
            storemerge28 = (var_54 & (-256L)) | ((long)*var_55 < (long)var_54);
        }
        var_58 = ((uint64_t)(unsigned char)storemerge28 == 0UL);
        *_pre_phi225 = (*_pre_phi225 + var_6);
        var_59 = _216 | (var_58 ? 0UL : 1UL);
        if ((long)var_5 > (long)18446744073709551615UL) {
            var_62 = 9223372036854775807UL - var_5;
            var_63 = (uint64_t *)(rdi + 136UL);
            _pre_phi227 = var_63;
            storemerge31 = (var_62 & (-256L)) | ((long)var_62 < (long)*var_63);
        } else {
            var_60 = 9223372036854775808UL - (-9223372036854775808L);
            var_61 = (uint64_t *)(rdi + 136UL);
            _pre_phi227 = var_61;
            storemerge31 = (var_60 & (-256L)) | ((long)*var_61 < (long)var_60);
        }
        var_64 = ((uint64_t)(unsigned char)storemerge31 == 0UL);
        *_pre_phi227 = (*_pre_phi227 + var_5);
        var_65 = var_59 | (var_64 ? 0UL : 1UL);
        if ((long)var_4 > (long)18446744073709551615UL) {
            var_68 = 9223372036854775807UL - var_4;
            var_69 = (uint64_t *)(rdi + 128UL);
            _pre_phi229 = var_69;
            storemerge34 = (var_68 & (-256L)) | ((long)var_68 < (long)*var_69);
        } else {
            var_66 = 9223372036854775808UL - (-9223372036854775808L);
            var_67 = (uint64_t *)(rdi + 128UL);
            _pre_phi229 = var_67;
            storemerge34 = (var_66 & (-256L)) | ((long)*var_67 < (long)var_66);
        }
        var_70 = ((uint64_t)(unsigned char)storemerge34 == 0UL);
        *_pre_phi229 = (*_pre_phi229 + var_4);
        var_71 = var_65 | (var_70 ? 0UL : 1UL);
        if ((long)var_3 > (long)18446744073709551615UL) {
            var_74 = 9223372036854775807UL - var_3;
            var_75 = (uint64_t *)(rdi + 120UL);
            _pre_phi231 = var_75;
            storemerge37 = (var_74 & (-256L)) | ((long)var_74 < (long)*var_75);
        } else {
            var_72 = 9223372036854775808UL - (-9223372036854775808L);
            var_73 = (uint64_t *)(rdi + 120UL);
            _pre_phi231 = var_73;
            storemerge37 = (var_72 & (-256L)) | ((long)*var_73 < (long)var_72);
        }
        var_76 = ((uint64_t)(unsigned char)storemerge37 == 0UL);
        *_pre_phi231 = (*_pre_phi231 + var_3);
        _220 = var_76 ? 0UL : 1UL;
        if ((long)var_2 > (long)18446744073709551615UL) {
            var_79 = 9223372036854775807UL - var_2;
            var_80 = (uint64_t *)(rdi + 112UL);
            _pre_phi233 = var_80;
            storemerge40 = (var_79 & (-256L)) | ((long)var_79 < (long)*var_80);
        } else {
            var_77 = 9223372036854775808UL - (-9223372036854775808L);
            var_78 = (uint64_t *)(rdi + 112UL);
            _pre_phi233 = var_78;
            storemerge40 = (var_77 & (-256L)) | ((long)*var_78 < (long)var_77);
        }
        var_81 = ((uint64_t)(unsigned char)storemerge40 == 0UL);
        *_pre_phi233 = (*_pre_phi233 + var_2);
        _221 = var_81 ? 0UL : 1UL;
        _masked42 = var_71 | _220;
        if ((long)var_1 > (long)18446744073709551615UL) {
            var_84 = 9223372036854775807UL - var_1;
            var_85 = (uint64_t *)(rdi + 104UL);
            _pre_phi235 = var_85;
            storemerge43 = (var_84 & (-256L)) | ((long)var_84 < (long)*var_85);
        } else {
            var_82 = 9223372036854775808UL - (-9223372036854775808L);
            var_83 = (uint64_t *)(rdi + 104UL);
            _pre_phi235 = var_83;
            storemerge43 = (var_82 & (-256L)) | ((long)*var_83 < (long)var_82);
        }
        var_86 = ((uint64_t)(unsigned char)storemerge43 == 0UL);
        *_pre_phi235 = (*_pre_phi235 + var_1);
        storemerge46 = (_masked42 | _221) | (var_86 ? 0UL : 1UL);
    } else {
        if (var_10) {
            var_13 = var_8 ^ 2147483648UL;
            var_14 = (uint32_t *)(rdi + 152UL);
            _pre_phi237 = var_14;
            storemerge = (uint64_t)((uint32_t)var_13 & (-256)) | ((long)(uint64_t)((long)(var_13 << 32UL) >> (long)32UL) > (long)(uint64_t)*var_14);
        } else {
            var_11 = var_8 + 2147483647UL;
            var_12 = (uint32_t *)(rdi + 152UL);
            _pre_phi237 = var_12;
            storemerge = (uint64_t)((uint32_t)var_11 & (-256)) | ((long)(var_11 << 32UL) < (long)((uint64_t)*var_12 << 32UL));
        }
        var_15 = ((uint64_t)(unsigned char)storemerge == 0UL);
        *_pre_phi237 = (*_pre_phi237 - var_7);
        _ = var_15 ? 0UL : 1UL;
        if ((long)var_6 > (long)18446744073709551615UL) {
            var_18 = var_6 ^ (-9223372036854775808L);
            var_19 = (uint64_t *)(rdi + 144UL);
            _pre_phi239 = var_19;
            storemerge2 = (var_18 & (-256L)) | ((long)*var_19 < (long)var_18);
        } else {
            var_16 = var_6 + 9223372036854775807UL;
            var_17 = (uint64_t *)(rdi + 144UL);
            _pre_phi239 = var_17;
            storemerge2 = (var_16 & (-256L)) | ((long)var_16 < (long)*var_17);
        }
        var_20 = ((uint64_t)(unsigned char)storemerge2 == 0UL);
        *_pre_phi239 = (*_pre_phi239 - var_6);
        var_21 = _ | (var_20 ? 0UL : 1UL);
        if ((long)var_5 > (long)18446744073709551615UL) {
            var_24 = var_5 ^ (-9223372036854775808L);
            var_25 = (uint64_t *)(rdi + 136UL);
            _pre_phi241 = var_25;
            storemerge4 = (var_24 & (-256L)) | ((long)*var_25 < (long)var_24);
        } else {
            var_22 = var_5 + 9223372036854775807UL;
            var_23 = (uint64_t *)(rdi + 136UL);
            _pre_phi241 = var_23;
            storemerge4 = (var_22 & (-256L)) | ((long)var_22 < (long)*var_23);
        }
        var_26 = ((uint64_t)(unsigned char)storemerge4 == 0UL);
        *_pre_phi241 = (*_pre_phi241 - var_5);
        var_27 = var_21 | (var_26 ? 0UL : 1UL);
        if ((long)var_4 > (long)18446744073709551615UL) {
            var_30 = var_4 ^ (-9223372036854775808L);
            var_31 = (uint64_t *)(rdi + 128UL);
            _pre_phi243 = var_31;
            storemerge7 = (var_30 & (-256L)) | ((long)*var_31 < (long)var_30);
        } else {
            var_28 = var_4 + 9223372036854775807UL;
            var_29 = (uint64_t *)(rdi + 128UL);
            _pre_phi243 = var_29;
            storemerge7 = (var_28 & (-256L)) | ((long)var_28 < (long)*var_29);
        }
        var_32 = ((uint64_t)(unsigned char)storemerge7 == 0UL);
        *_pre_phi243 = (*_pre_phi243 - var_4);
        var_33 = var_27 | (var_32 ? 0UL : 1UL);
        if ((long)var_3 > (long)18446744073709551615UL) {
            var_36 = var_3 ^ (-9223372036854775808L);
            var_37 = (uint64_t *)(rdi + 120UL);
            _pre_phi245 = var_37;
            storemerge10 = (var_36 & (-256L)) | ((long)*var_37 < (long)var_36);
        } else {
            var_34 = var_3 + 9223372036854775807UL;
            var_35 = (uint64_t *)(rdi + 120UL);
            _pre_phi245 = var_35;
            storemerge10 = (var_34 & (-256L)) | ((long)var_34 < (long)*var_35);
        }
        var_38 = ((uint64_t)(unsigned char)storemerge10 == 0UL);
        *_pre_phi245 = (*_pre_phi245 - var_3);
        _213 = var_38 ? 0UL : 1UL;
        if ((long)var_2 > (long)18446744073709551615UL) {
            var_41 = var_2 ^ (-9223372036854775808L);
            var_42 = (uint64_t *)(rdi + 112UL);
            _pre_phi247 = var_42;
            storemerge13 = (var_41 & (-256L)) | ((long)*var_42 < (long)var_41);
        } else {
            var_39 = var_2 + 9223372036854775807UL;
            var_40 = (uint64_t *)(rdi + 112UL);
            _pre_phi247 = var_40;
            storemerge13 = (var_39 & (-256L)) | ((long)var_39 < (long)*var_40);
        }
        var_43 = ((uint64_t)(unsigned char)storemerge13 == 0UL);
        *_pre_phi247 = (*_pre_phi247 - var_2);
        _214 = var_43 ? 0UL : 1UL;
        _masked15 = var_33 | _213;
        if ((long)var_1 > (long)18446744073709551615UL) {
            var_46 = var_1 ^ (-9223372036854775808L);
            var_47 = (uint64_t *)(rdi + 104UL);
            _pre_phi249 = var_47;
            storemerge16 = (var_46 & (-256L)) | ((long)*var_47 < (long)var_46);
        } else {
            var_44 = var_1 + 9223372036854775807UL;
            var_45 = (uint64_t *)(rdi + 104UL);
            _pre_phi249 = var_45;
            storemerge16 = (var_44 & (-256L)) | ((long)var_44 < (long)*var_45);
        }
        var_48 = ((uint64_t)(unsigned char)storemerge16 == 0UL);
        *_pre_phi249 = (*_pre_phi249 - var_1);
        storemerge46 = (_masked15 | _214) | (var_48 ? 0UL : 1UL);
    }
    if ((uint64_t)(unsigned char)storemerge46 != 0UL) {
        return;
    }
    *(unsigned char *)(rdi + 161UL) = (unsigned char)'\x01';
    return;
}

typedef unsigned char __u_char;
typedef unsigned short __u_short;
typedef unsigned int __u_int;
typedef unsigned long __u_long;
typedef signed char __int8_t;
typedef unsigned char __uint8_t;
typedef short __int16_t;
typedef unsigned short __uint16_t;
typedef int __int32_t;
typedef unsigned int __uint32_t;
typedef long __int64_t;
typedef unsigned long __uint64_t;
typedef __int8_t __int_least8_t;
typedef __uint8_t __uint_least8_t;
typedef __int16_t __int_least16_t;
typedef __uint16_t __uint_least16_t;
typedef __int32_t __int_least32_t;
typedef __uint32_t __uint_least32_t;
typedef __int64_t __int_least64_t;
typedef __uint64_t __uint_least64_t;
typedef long __quad_t;
typedef unsigned long __u_quad_t;
typedef long __intmax_t;
typedef unsigned long __uintmax_t;
typedef unsigned long __dev_t;
typedef unsigned int __uid_t;
typedef unsigned int __gid_t;
typedef unsigned long __ino_t;
typedef unsigned long __ino64_t;
typedef unsigned int __mode_t;
typedef unsigned long __nlink_t;
typedef long __off_t;
typedef long __off64_t;
typedef int __pid_t;
typedef struct {
    int __val[2];
} __fsid_t;
typedef long __clock_t;
typedef unsigned long __rlim_t;
typedef unsigned long __rlim64_t;
typedef unsigned int __id_t;
typedef long __time_t;
typedef unsigned int __useconds_t;
typedef long __suseconds_t;
typedef int __daddr_t;
typedef int __key_t;
typedef int __clockid_t;
typedef void *__timer_t;
typedef long __blksize_t;
typedef long __blkcnt_t;
typedef long __blkcnt64_t;
typedef unsigned long __fsblkcnt_t;
typedef unsigned long __fsblkcnt64_t;
typedef unsigned long __fsfilcnt_t;
typedef unsigned long __fsfilcnt64_t;
typedef long __fsword_t;
typedef long __ssize_t;
typedef long __syscall_slong_t;
typedef unsigned long __syscall_ulong_t;
typedef __off64_t __loff_t;
typedef char *__caddr_t;
typedef long __intptr_t;
typedef unsigned int __socklen_t;
typedef int __sig_atomic_t;
typedef __int8_t int8_t;
typedef __int16_t int16_t;
typedef __int32_t int32_t;
typedef __int64_t int64_t;
typedef __uint8_t uint8_t;
typedef __uint16_t uint16_t;
typedef __uint32_t uint32_t;
typedef __uint64_t uint64_t;
typedef __int_least8_t int_least8_t;
typedef __int_least16_t int_least16_t;
typedef __int_least32_t int_least32_t;
typedef __int_least64_t int_least64_t;
typedef __uint_least8_t uint_least8_t;
typedef __uint_least16_t uint_least16_t;
typedef __uint_least32_t uint_least32_t;
typedef __uint_least64_t uint_least64_t;
typedef signed char int_fast8_t;
typedef long int_fast16_t;
typedef long int_fast32_t;
typedef long int_fast64_t;
typedef unsigned char uint_fast8_t;
typedef unsigned long uint_fast16_t;
typedef unsigned long uint_fast32_t;
typedef unsigned long uint_fast64_t;
typedef long intptr_t;
typedef unsigned long uintptr_t;
typedef __intmax_t intmax_t;
typedef __uintmax_t uintmax_t;
typedef _Bool bool;
struct indirect_placeholder_133_ret_type;
struct indirect_placeholder_133_ret_type {
    uint64_t field_0;
    uint64_t field_1;
};
extern uint64_t revng_init_local_sp(uint64_t param_0);
extern void indirect_placeholder(void);
extern uint64_t init_rbx(void);
extern uint64_t init_rbp(void);
extern uint64_t init_cc_src2(void);
extern uint64_t helper_cc_compute_c_wrapper(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint32_t param_3);
extern uint64_t init_r14(void);
extern uint64_t init_r13(void);
extern uint64_t init_r12(void);
extern uint64_t init_r15(void);
extern uint64_t init_rax(void);
extern struct indirect_placeholder_133_ret_type indirect_placeholder_133(uint64_t param_0);
uint64_t bb_save_abbr(uint64_t rdi, uint64_t rsi) {
    uint64_t var_0;
    uint64_t var_1;
    uint64_t var_2;
    uint64_t var_3;
    uint64_t var_4;
    uint64_t var_5;
    uint64_t var_6;
    uint64_t var_7;
    uint64_t var_8;
    uint64_t var_9;
    uint64_t *var_10;
    uint64_t var_11;
    uint64_t rax_2;
    uint64_t local_sp_0;
    uint64_t var_19;
    uint64_t rax_1_ph;
    uint64_t rax_1_ph_ph;
    uint64_t rbx_1;
    bool var_22;
    uint64_t *var_23;
    uint64_t rbx_2_ph;
    struct indirect_placeholder_133_ret_type var_24;
    uint64_t var_25;
    uint64_t rbx_0;
    uint64_t var_20;
    uint64_t var_21;
    uint64_t var_17;
    uint64_t var_18;
    uint64_t rbx_2;
    uint64_t rax_0;
    uint64_t var_12;
    uint64_t var_13;
    uint64_t rbx_2_ph_ph_in;
    uint64_t local_sp_0_ph_ph;
    uint64_t rbx_2_ph_ph;
    unsigned char *var_14;
    uint64_t *var_15;
    uint64_t local_sp_0_ph;
    bool var_16;
    unsigned int loop_state_var;
    bool switch_state_var;
    var_0 = revng_init_local_sp(0UL);
    var_1 = init_rax();
    var_2 = init_rbx();
    var_3 = init_r14();
    var_4 = init_rbp();
    var_5 = init_r13();
    var_6 = init_r12();
    var_7 = init_r15();
    var_8 = init_cc_src2();
    *(uint64_t *)(var_0 + (-8L)) = var_7;
    *(uint64_t *)(var_0 + (-16L)) = var_3;
    *(uint64_t *)(var_0 + (-24L)) = var_5;
    *(uint64_t *)(var_0 + (-32L)) = var_6;
    *(uint64_t *)(var_0 + (-40L)) = var_4;
    *(uint64_t *)(var_0 + (-48L)) = var_2;
    var_9 = var_0 + (-56L);
    var_10 = (uint64_t *)(rsi + 48UL);
    var_11 = *var_10;
    rax_2 = 1UL;
    rbx_1 = 4307916UL;
    rax_0 = var_1;
    rbx_2_ph_ph_in = rdi;
    local_sp_0_ph_ph = var_9;
    if (var_11 == 0UL) {
        return rax_2;
    }
    rax_2 = 0UL;
    if (var_11 >= rsi) {
        var_12 = rsi + 56UL;
        var_13 = helper_cc_compute_c_wrapper(var_11 - var_12, var_12, var_8, 17U);
        rax_0 = 1UL;
        if (var_13 == 0UL) {
            return rax_2;
        }
    }
    rax_1_ph_ph = rax_0;
    if (*(unsigned char *)var_11 != '\x00') {
        while (1U)
            {
                rbx_2_ph_ph = rbx_2_ph_ph_in + 9UL;
                var_14 = (unsigned char *)(rbx_2_ph_ph_in + 8UL);
                var_15 = (uint64_t *)rbx_2_ph_ph_in;
                rax_1_ph = rax_1_ph_ph;
                rbx_2_ph = rbx_2_ph_ph;
                local_sp_0_ph = local_sp_0_ph_ph;
                while (1U)
                    {
                        var_16 = ((uint64_t)(uint32_t)rax_1_ph == 0UL);
                        rax_1_ph = 0UL;
                        rbx_2 = rbx_2_ph;
                        local_sp_0 = local_sp_0_ph;
                        while (1U)
                            {
                                *(uint64_t *)(local_sp_0 + (-8L)) = 4246455UL;
                                indirect_placeholder();
                                rbx_0 = rbx_2;
                                rbx_1 = rbx_2;
                                if (!var_16) {
                                    loop_state_var = 0U;
                                    break;
                                }
                                if (*(unsigned char *)rbx_2 == '\x00') {
                                    var_17 = local_sp_0 + (-16L);
                                    *(uint64_t *)var_17 = 4246418UL;
                                    indirect_placeholder();
                                    var_18 = (rax_1_ph + rbx_2) + 1UL;
                                    local_sp_0 = var_17;
                                    rbx_2_ph = var_18;
                                    rbx_2 = var_18;
                                    local_sp_0_ph_ph = var_17;
                                    local_sp_0_ph = var_17;
                                    if (*(unsigned char *)var_18 == '\x00') {
                                        continue;
                                    }
                                    loop_state_var = 1U;
                                    break;
                                }
                                if (rbx_2_ph_ph != rbx_2) {
                                    loop_state_var = 2U;
                                    break;
                                }
                                rbx_0 = rbx_2_ph_ph;
                                if (*var_14 != '\x00') {
                                    loop_state_var = 2U;
                                    break;
                                }
                            }
                        switch_state_var = 0;
                        switch (loop_state_var) {
                          case 1U:
                            {
                                var_19 = *var_15;
                                rax_1_ph_ph = var_19;
                                rbx_2_ph_ph_in = var_19;
                                if (var_19 == 0UL) {
                                    continue;
                                }
                                loop_state_var = 0U;
                                switch_state_var = 1;
                                break;
                            }
                            break;
                          case 2U:
                            {
                                *(uint64_t *)(local_sp_0 + (-16L)) = 4246310UL;
                                indirect_placeholder();
                                var_20 = rax_1_ph + 1UL;
                                var_21 = rbx_0 - rbx_2_ph_ph;
                                rbx_1 = rbx_0;
                                if (var_20 > (var_21 ^ (-1L))) {
                                    *(uint64_t *)(local_sp_0 + (-24L)) = 4246339UL;
                                    indirect_placeholder();
                                    *(uint32_t *)var_21 = 12U;
                                    loop_state_var = 1U;
                                    switch_state_var = 1;
                                    break;
                                }
                                var_22 = ((var_21 + var_20) > 118UL);
                                var_23 = (uint64_t *)(local_sp_0 + (-24L));
                                if (var_22) {
                                    *var_23 = 4246375UL;
                                    indirect_placeholder();
                                    *(unsigned char *)((rax_1_ph + rbx_0) + 1UL) = (unsigned char)'\x00';
                                    loop_state_var = 2U;
                                    switch_state_var = 1;
                                    break;
                                }
                                *var_23 = 4246391UL;
                                var_24 = indirect_placeholder_133(var_11);
                                var_25 = var_24.field_0;
                                *var_15 = var_25;
                                if (var_25 != 0UL) {
                                    loop_state_var = 1U;
                                    switch_state_var = 1;
                                    break;
                                }
                                *(unsigned char *)(var_25 + 8UL) = (unsigned char)'\x00';
                                rbx_1 = var_25 + 9UL;
                                loop_state_var = 2U;
                                switch_state_var = 1;
                                break;
                            }
                            break;
                          case 0U:
                            {
                                loop_state_var = 2U;
                                switch_state_var = 1;
                                break;
                            }
                            break;
                        }
                        if (switch_state_var)
                            break;
                    }
                switch_state_var = 0;
                switch (loop_state_var) {
                  case 0U:
                    {
                        continue;
                    }
                    break;
                  case 1U:
                    {
                        loop_state_var = 0U;
                        switch_state_var = 1;
                        break;
                    }
                    break;
                  case 2U:
                    {
                        loop_state_var = 1U;
                        switch_state_var = 1;
                        break;
                    }
                    break;
                }
                if (switch_state_var)
                    break;
            }
        switch (loop_state_var) {
          case 1U:
            {
                break;
            }
            break;
          case 0U:
            {
                return rax_2;
            }
            break;
        }
    }
    *var_10 = rbx_1;
    rax_2 = 1UL;
}

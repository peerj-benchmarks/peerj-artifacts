typedef unsigned char __u_char;
typedef unsigned short __u_short;
typedef unsigned int __u_int;
typedef unsigned long __u_long;
typedef signed char __int8_t;
typedef unsigned char __uint8_t;
typedef short __int16_t;
typedef unsigned short __uint16_t;
typedef int __int32_t;
typedef unsigned int __uint32_t;
typedef long __int64_t;
typedef unsigned long __uint64_t;
typedef __int8_t __int_least8_t;
typedef __uint8_t __uint_least8_t;
typedef __int16_t __int_least16_t;
typedef __uint16_t __uint_least16_t;
typedef __int32_t __int_least32_t;
typedef __uint32_t __uint_least32_t;
typedef __int64_t __int_least64_t;
typedef __uint64_t __uint_least64_t;
typedef long __quad_t;
typedef unsigned long __u_quad_t;
typedef long __intmax_t;
typedef unsigned long __uintmax_t;
typedef unsigned long __dev_t;
typedef unsigned int __uid_t;
typedef unsigned int __gid_t;
typedef unsigned long __ino_t;
typedef unsigned long __ino64_t;
typedef unsigned int __mode_t;
typedef unsigned long __nlink_t;
typedef long __off_t;
typedef long __off64_t;
typedef int __pid_t;
typedef struct {
    int __val[2];
} __fsid_t;
typedef long __clock_t;
typedef unsigned long __rlim_t;
typedef unsigned long __rlim64_t;
typedef unsigned int __id_t;
typedef long __time_t;
typedef unsigned int __useconds_t;
typedef long __suseconds_t;
typedef int __daddr_t;
typedef int __key_t;
typedef int __clockid_t;
typedef void *__timer_t;
typedef long __blksize_t;
typedef long __blkcnt_t;
typedef long __blkcnt64_t;
typedef unsigned long __fsblkcnt_t;
typedef unsigned long __fsblkcnt64_t;
typedef unsigned long __fsfilcnt_t;
typedef unsigned long __fsfilcnt64_t;
typedef long __fsword_t;
typedef long __ssize_t;
typedef long __syscall_slong_t;
typedef unsigned long __syscall_ulong_t;
typedef __off64_t __loff_t;
typedef char *__caddr_t;
typedef long __intptr_t;
typedef unsigned int __socklen_t;
typedef int __sig_atomic_t;
typedef __int8_t int8_t;
typedef __int16_t int16_t;
typedef __int32_t int32_t;
typedef __int64_t int64_t;
typedef __uint8_t uint8_t;
typedef __uint16_t uint16_t;
typedef __uint32_t uint32_t;
typedef __uint64_t uint64_t;
typedef __int_least8_t int_least8_t;
typedef __int_least16_t int_least16_t;
typedef __int_least32_t int_least32_t;
typedef __int_least64_t int_least64_t;
typedef __uint_least8_t uint_least8_t;
typedef __uint_least16_t uint_least16_t;
typedef __uint_least32_t uint_least32_t;
typedef __uint_least64_t uint_least64_t;
typedef signed char int_fast8_t;
typedef long int_fast16_t;
typedef long int_fast32_t;
typedef long int_fast64_t;
typedef unsigned char uint_fast8_t;
typedef unsigned long uint_fast16_t;
typedef unsigned long uint_fast32_t;
typedef unsigned long uint_fast64_t;
typedef long intptr_t;
typedef unsigned long uintptr_t;
typedef __intmax_t intmax_t;
typedef __uintmax_t uintmax_t;
typedef _Bool bool;
extern uint64_t revng_init_local_sp(uint64_t param_0);
extern uint64_t init_rbx(void);
extern void indirect_placeholder(void);
extern uint64_t init_rbp(void);
extern uint64_t init_r14(void);
extern uint64_t init_r13(void);
extern uint64_t init_r12(void);
extern uint64_t indirect_placeholder_1(uint64_t param_0);
extern uint64_t init_r15(void);
extern uint64_t init_rax(void);
extern uint64_t indirect_placeholder_10(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint64_t param_3);
void bb_rpl_unlinkat(uint64_t rdx, uint64_t rdi, uint64_t rsi) {
    uint64_t var_0;
    uint64_t var_1;
    uint64_t var_2;
    uint64_t var_3;
    uint64_t var_4;
    uint64_t var_5;
    uint64_t var_6;
    uint64_t var_7;
    uint64_t var_8;
    bool var_9;
    uint64_t var_10;
    uint64_t *var_11;
    uint32_t var_23;
    uint64_t local_sp_0;
    uint64_t rax_0;
    uint64_t r14_0;
    uint64_t var_20;
    uint64_t var_24;
    uint64_t local_sp_1;
    uint64_t var_21;
    uint64_t var_22;
    uint64_t var_19;
    uint64_t var_15;
    bool var_16;
    uint64_t var_17;
    uint64_t *var_18;
    uint64_t var_12;
    uint64_t var_13;
    uint64_t var_14;
    unsigned int loop_state_var;
    var_0 = revng_init_local_sp(0UL);
    var_1 = init_rax();
    var_2 = init_rbx();
    var_3 = init_r14();
    var_4 = init_rbp();
    var_5 = init_r13();
    var_6 = init_r12();
    var_7 = init_r15();
    *(uint64_t *)(var_0 + (-8L)) = var_7;
    *(uint64_t *)(var_0 + (-16L)) = var_3;
    *(uint64_t *)(var_0 + (-24L)) = var_5;
    *(uint64_t *)(var_0 + (-32L)) = var_6;
    *(uint64_t *)(var_0 + (-40L)) = var_4;
    *(uint64_t *)(var_0 + (-48L)) = var_2;
    var_8 = (uint64_t)(uint32_t)rdi;
    var_9 = ((uint64_t)((uint16_t)rdx & (unsigned short)512U) == 0UL);
    var_10 = var_0 + (-208L);
    var_11 = (uint64_t *)var_10;
    local_sp_1 = var_10;
    if (!var_9) {
        *var_11 = 4263472UL;
        indirect_placeholder();
        return;
    }
    *var_11 = 4263485UL;
    indirect_placeholder();
    if (var_1 == 0UL) {
        *(uint64_t *)(local_sp_1 + (-8L)) = 4263687UL;
        indirect_placeholder();
    } else {
        var_12 = var_1 + (-1L);
        r14_0 = var_12;
        if (*(unsigned char *)((var_1 + rsi) + (-1L)) != '/') {
            var_13 = var_0 + (-216L);
            *(uint64_t *)var_13 = 4263531UL;
            var_14 = indirect_placeholder_10(var_10, var_8, 256UL, rsi);
            if ((uint64_t)(uint32_t)var_14 == 0UL) {
                return;
            }
            *(uint64_t *)(var_0 + (-224L)) = 4263549UL;
            var_15 = indirect_placeholder_1(var_1);
            var_16 = (var_15 == 0UL);
            var_17 = var_0 + (-232L);
            var_18 = (uint64_t *)var_17;
            local_sp_0 = var_17;
            if (var_16) {
                *var_18 = 4263562UL;
                indirect_placeholder();
                *(volatile uint32_t *)(uint32_t *)0UL = 1U;
            } else {
                *var_18 = 4263584UL;
                indirect_placeholder();
                var_19 = var_12 + var_15;
                if (*(unsigned char *)var_19 != '/') {
                    while (1U)
                        {
                            *(unsigned char *)((r14_0 + (var_19 - var_1)) + 1UL) = (unsigned char)'\x00';
                            if (r14_0 != 0UL) {
                                loop_state_var = 0U;
                                break;
                            }
                            var_20 = r14_0 + (-1L);
                            r14_0 = var_20;
                            if (*(unsigned char *)(var_20 + var_15) == '/') {
                                continue;
                            }
                            loop_state_var = 1U;
                            break;
                        }
                    switch (loop_state_var) {
                      case 1U:
                        {
                            break;
                        }
                        break;
                      case 0U:
                        {
                            var_24 = local_sp_0 + (-8L);
                            *(uint64_t *)var_24 = 4263665UL;
                            indirect_placeholder();
                            local_sp_1 = var_24;
                            *(uint64_t *)(local_sp_1 + (-8L)) = 4263687UL;
                            indirect_placeholder();
                            return;
                        }
                        break;
                    }
                }
                var_21 = var_0 + (-240L);
                *(uint64_t *)var_21 = 4263708UL;
                var_22 = indirect_placeholder_10(var_17, var_8, 256UL, var_15);
                rax_0 = var_22;
                local_sp_0 = var_21;
                if ((uint64_t)(uint32_t)var_22 != 0UL) {
                    var_23 = (uint32_t)((uint16_t)*(uint32_t *)var_13 & (unsigned short)61440U);
                    rax_0 = (uint64_t)var_23;
                    if ((uint64_t)((var_23 + (-40960)) & (-4096)) != 0UL) {
                        var_24 = local_sp_0 + (-8L);
                        *(uint64_t *)var_24 = 4263665UL;
                        indirect_placeholder();
                        local_sp_1 = var_24;
                        *(uint64_t *)(local_sp_1 + (-8L)) = 4263687UL;
                        indirect_placeholder();
                        return;
                    }
                }
                *(uint64_t *)(var_0 + (-248L)) = 4263644UL;
                indirect_placeholder();
                *(uint64_t *)(var_0 + (-256L)) = 4263649UL;
                indirect_placeholder();
                *(uint32_t *)rax_0 = 1U;
            }
        }
    }
}

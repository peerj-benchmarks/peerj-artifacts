typedef unsigned char __u_char;
typedef unsigned short __u_short;
typedef unsigned int __u_int;
typedef unsigned long __u_long;
typedef signed char __int8_t;
typedef unsigned char __uint8_t;
typedef short __int16_t;
typedef unsigned short __uint16_t;
typedef int __int32_t;
typedef unsigned int __uint32_t;
typedef long __int64_t;
typedef unsigned long __uint64_t;
typedef __int8_t __int_least8_t;
typedef __uint8_t __uint_least8_t;
typedef __int16_t __int_least16_t;
typedef __uint16_t __uint_least16_t;
typedef __int32_t __int_least32_t;
typedef __uint32_t __uint_least32_t;
typedef __int64_t __int_least64_t;
typedef __uint64_t __uint_least64_t;
typedef long __quad_t;
typedef unsigned long __u_quad_t;
typedef long __intmax_t;
typedef unsigned long __uintmax_t;
typedef unsigned long __dev_t;
typedef unsigned int __uid_t;
typedef unsigned int __gid_t;
typedef unsigned long __ino_t;
typedef unsigned long __ino64_t;
typedef unsigned int __mode_t;
typedef unsigned long __nlink_t;
typedef long __off_t;
typedef long __off64_t;
typedef int __pid_t;
typedef struct {
    int __val[2];
} __fsid_t;
typedef long __clock_t;
typedef unsigned long __rlim_t;
typedef unsigned long __rlim64_t;
typedef unsigned int __id_t;
typedef long __time_t;
typedef unsigned int __useconds_t;
typedef long __suseconds_t;
typedef int __daddr_t;
typedef int __key_t;
typedef int __clockid_t;
typedef void *__timer_t;
typedef long __blksize_t;
typedef long __blkcnt_t;
typedef long __blkcnt64_t;
typedef unsigned long __fsblkcnt_t;
typedef unsigned long __fsblkcnt64_t;
typedef unsigned long __fsfilcnt_t;
typedef unsigned long __fsfilcnt64_t;
typedef long __fsword_t;
typedef long __ssize_t;
typedef long __syscall_slong_t;
typedef unsigned long __syscall_ulong_t;
typedef __off64_t __loff_t;
typedef char *__caddr_t;
typedef long __intptr_t;
typedef unsigned int __socklen_t;
typedef int __sig_atomic_t;
typedef __int8_t int8_t;
typedef __int16_t int16_t;
typedef __int32_t int32_t;
typedef __int64_t int64_t;
typedef __uint8_t uint8_t;
typedef __uint16_t uint16_t;
typedef __uint32_t uint32_t;
typedef __uint64_t uint64_t;
typedef __int_least8_t int_least8_t;
typedef __int_least16_t int_least16_t;
typedef __int_least32_t int_least32_t;
typedef __int_least64_t int_least64_t;
typedef __uint_least8_t uint_least8_t;
typedef __uint_least16_t uint_least16_t;
typedef __uint_least32_t uint_least32_t;
typedef __uint_least64_t uint_least64_t;
typedef signed char int_fast8_t;
typedef long int_fast16_t;
typedef long int_fast32_t;
typedef long int_fast64_t;
typedef unsigned char uint_fast8_t;
typedef unsigned long uint_fast16_t;
typedef unsigned long uint_fast32_t;
typedef unsigned long uint_fast64_t;
typedef long intptr_t;
typedef unsigned long uintptr_t;
typedef __intmax_t intmax_t;
typedef __uintmax_t uintmax_t;
typedef _Bool bool;
extern void abort(void);
extern uint64_t revng_init_local_sp(uint64_t param_0);
extern uint64_t init_rbx(void);
extern void indirect_placeholder(void);
extern uint64_t init_rbp(void);
extern uint64_t init_cc_src2(void);
extern uint64_t helper_cc_compute_c_wrapper(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint32_t param_3);
extern uint64_t init_r14(void);
extern uint64_t init_r13(void);
extern uint64_t init_r12(void);
extern uint64_t init_r15(void);
extern void llvm_trap(void);
void bb_careadlinkat(uint64_t rdx, uint64_t rdi, uint64_t rcx, uint64_t rsi, uint64_t r9, uint64_t r8) {
    uint64_t var_20;
    uint64_t var_0;
    uint64_t var_1;
    uint64_t var_2;
    uint64_t var_3;
    uint64_t var_4;
    uint64_t var_5;
    uint64_t var_6;
    uint64_t var_7;
    uint64_t var_8;
    uint64_t var_9;
    bool var_10;
    uint64_t var_11;
    uint64_t r14_0;
    uint64_t rbp_0;
    uint64_t local_sp_2;
    uint64_t rax_0;
    uint64_t storemerge;
    uint64_t var_17;
    uint64_t rax_1;
    uint64_t local_sp_3;
    uint64_t local_sp_1;
    uint64_t var_21;
    uint64_t rbx_2;
    uint64_t var_13;
    uint64_t local_sp_4;
    uint32_t var_14;
    uint64_t var_19;
    uint64_t var_18;
    uint64_t var_15;
    uint64_t var_16;
    uint64_t var_12;
    unsigned int loop_state_var;
    var_0 = revng_init_local_sp(0UL);
    var_1 = init_rbx();
    var_2 = init_r14();
    var_3 = init_rbp();
    var_4 = init_r13();
    var_5 = init_r12();
    var_6 = init_r15();
    var_7 = init_cc_src2();
    *(uint64_t *)(var_0 + (-8L)) = var_6;
    *(uint64_t *)(var_0 + (-16L)) = var_2;
    *(uint64_t *)(var_0 + (-24L)) = var_4;
    *(uint64_t *)(var_0 + (-32L)) = var_5;
    *(uint64_t *)(var_0 + (-40L)) = var_3;
    *(uint64_t *)(var_0 + (-48L)) = var_1;
    var_8 = var_0 + (-1096L);
    *(uint32_t *)(var_0 + (-1088L)) = (uint32_t)rdi;
    *(uint64_t *)var_8 = rsi;
    var_9 = (r8 == 0UL) ? 4320576UL : r8;
    var_10 = (rcx == 0UL);
    var_11 = (rcx & (-65536L)) | 1024UL;
    r14_0 = var_10 ? (var_0 + (-1080L)) : rdx;
    rbp_0 = r14_0;
    rax_0 = 4611686018427387904UL;
    storemerge = 9223372036854775808UL;
    rax_1 = 4320576UL;
    rbx_2 = var_10 ? var_11 : rcx;
    local_sp_4 = var_8;
    while (1U)
        {
            var_12 = local_sp_4 + (-8L);
            *(uint64_t *)var_12 = 4264167UL;
            indirect_placeholder();
            local_sp_2 = var_12;
            if ((long)rax_1 <= (long)18446744073709551615UL) {
                var_13 = local_sp_4 + (-16L);
                *(uint64_t *)var_13 = 4264180UL;
                indirect_placeholder();
                var_14 = *(uint32_t *)rax_1;
                *(uint32_t *)(local_sp_4 + (-4L)) = var_14;
                local_sp_2 = var_13;
                if (var_14 != 34U) {
                    if (rbp_0 != r14_0) {
                        loop_state_var = 1U;
                        break;
                    }
                    *(uint64_t *)(local_sp_4 + (-24L)) = 4264212UL;
                    indirect_placeholder();
                    *(uint64_t *)(local_sp_4 + (-32L)) = 4264217UL;
                    indirect_placeholder();
                    llvm_trap();
                    abort();
                }
            }
            local_sp_3 = local_sp_2;
            if (rbx_2 > rax_1) {
                if (rbp_0 == r14_0) {
                    var_15 = local_sp_2 + (-8L);
                    *(uint64_t *)var_15 = 4264370UL;
                    indirect_placeholder();
                    local_sp_3 = var_15;
                }
                if (rbx_2 <= 4611686018427387904UL) {
                    storemerge = rbx_2 << 1UL;
                    var_17 = local_sp_3 + (-8L);
                    *(uint64_t *)var_17 = 4264440UL;
                    indirect_placeholder();
                    rbp_0 = rax_0;
                    rbx_2 = storemerge;
                    rax_1 = rax_0;
                    local_sp_4 = var_17;
                    continue;
                }
                var_16 = helper_cc_compute_c_wrapper(rbx_2 ^ (-9223372036854775808L), 9223372036854775808UL, var_7, 17U);
                rax_0 = 9223372036854775808UL;
                if (var_16 != 0UL) {
                    *(uint64_t *)(local_sp_3 + (-8L)) = 4264410UL;
                    indirect_placeholder();
                    *(uint32_t *)9223372036854775808UL = 36U;
                    loop_state_var = 1U;
                    break;
                }
            }
            *(unsigned char *)(rax_1 + rbp_0) = (unsigned char)'\x00';
            if (rbp_0 != (local_sp_2 + 16UL)) {
                var_18 = helper_cc_compute_c_wrapper((rax_1 + 1UL) - rbx_2, rbx_2, var_7, 17U);
                if (!((var_18 == 0UL) || (rbp_0 == r14_0))) {
                    loop_state_var = 1U;
                    break;
                }
                if (*(uint64_t *)(var_9 + 8UL) != 0UL) {
                    loop_state_var = 1U;
                    break;
                }
                *(uint64_t *)(local_sp_2 + (-8L)) = 4264346UL;
                indirect_placeholder();
                loop_state_var = 1U;
                break;
            }
            var_19 = local_sp_2 + (-8L);
            *(uint64_t *)var_19 = 4264271UL;
            indirect_placeholder();
            local_sp_1 = var_19;
            if (rbp_0 == 0UL) {
                *(uint64_t *)(local_sp_2 + (-16L)) = 4264299UL;
                indirect_placeholder();
                loop_state_var = 1U;
                break;
            }
            var_20 = *(uint64_t *)(var_9 + 24UL);
            if (var_20 != 0UL) {
                loop_state_var = 0U;
                break;
            }
            var_21 = local_sp_2 + (-16L);
            *(uint64_t *)var_21 = 4264471UL;
            indirect_placeholder();
            local_sp_1 = var_21;
            loop_state_var = 0U;
            break;
        }
    switch (loop_state_var) {
      case 0U:
        {
            *(uint64_t *)(local_sp_1 + (-8L)) = 4264476UL;
            indirect_placeholder();
            *(uint32_t *)var_20 = 12U;
        }
        break;
      case 1U:
        {
            return;
        }
        break;
    }
}

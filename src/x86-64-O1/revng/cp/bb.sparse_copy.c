typedef unsigned char __u_char;
typedef unsigned short __u_short;
typedef unsigned int __u_int;
typedef unsigned long __u_long;
typedef signed char __int8_t;
typedef unsigned char __uint8_t;
typedef short __int16_t;
typedef unsigned short __uint16_t;
typedef int __int32_t;
typedef unsigned int __uint32_t;
typedef long __int64_t;
typedef unsigned long __uint64_t;
typedef __int8_t __int_least8_t;
typedef __uint8_t __uint_least8_t;
typedef __int16_t __int_least16_t;
typedef __uint16_t __uint_least16_t;
typedef __int32_t __int_least32_t;
typedef __uint32_t __uint_least32_t;
typedef __int64_t __int_least64_t;
typedef __uint64_t __uint_least64_t;
typedef long __quad_t;
typedef unsigned long __u_quad_t;
typedef long __intmax_t;
typedef unsigned long __uintmax_t;
typedef unsigned long __dev_t;
typedef unsigned int __uid_t;
typedef unsigned int __gid_t;
typedef unsigned long __ino_t;
typedef unsigned long __ino64_t;
typedef unsigned int __mode_t;
typedef unsigned long __nlink_t;
typedef long __off_t;
typedef long __off64_t;
typedef int __pid_t;
typedef struct {
    int __val[2];
} __fsid_t;
typedef long __clock_t;
typedef unsigned long __rlim_t;
typedef unsigned long __rlim64_t;
typedef unsigned int __id_t;
typedef long __time_t;
typedef unsigned int __useconds_t;
typedef long __suseconds_t;
typedef int __daddr_t;
typedef int __key_t;
typedef int __clockid_t;
typedef void *__timer_t;
typedef long __blksize_t;
typedef long __blkcnt_t;
typedef long __blkcnt64_t;
typedef unsigned long __fsblkcnt_t;
typedef unsigned long __fsblkcnt64_t;
typedef unsigned long __fsfilcnt_t;
typedef unsigned long __fsfilcnt64_t;
typedef long __fsword_t;
typedef long __ssize_t;
typedef long __syscall_slong_t;
typedef unsigned long __syscall_ulong_t;
typedef __off64_t __loff_t;
typedef char *__caddr_t;
typedef long __intptr_t;
typedef unsigned int __socklen_t;
typedef int __sig_atomic_t;
typedef __int8_t int8_t;
typedef __int16_t int16_t;
typedef __int32_t int32_t;
typedef __int64_t int64_t;
typedef __uint8_t uint8_t;
typedef __uint16_t uint16_t;
typedef __uint32_t uint32_t;
typedef __uint64_t uint64_t;
typedef __int_least8_t int_least8_t;
typedef __int_least16_t int_least16_t;
typedef __int_least32_t int_least32_t;
typedef __int_least64_t int_least64_t;
typedef __uint_least8_t uint_least8_t;
typedef __uint_least16_t uint_least16_t;
typedef __uint_least32_t uint_least32_t;
typedef __uint_least64_t uint_least64_t;
typedef signed char int_fast8_t;
typedef long int_fast16_t;
typedef long int_fast32_t;
typedef long int_fast64_t;
typedef unsigned char uint_fast8_t;
typedef unsigned long uint_fast16_t;
typedef unsigned long uint_fast32_t;
typedef unsigned long uint_fast64_t;
typedef long intptr_t;
typedef unsigned long uintptr_t;
typedef __intmax_t intmax_t;
typedef __uintmax_t uintmax_t;
typedef _Bool bool;
struct indirect_placeholder_47_ret_type;
struct indirect_placeholder_46_ret_type;
struct indirect_placeholder_49_ret_type;
struct indirect_placeholder_48_ret_type;
struct indirect_placeholder_50_ret_type;
struct indirect_placeholder_45_ret_type;
struct indirect_placeholder_47_ret_type {
    uint64_t field_0;
    uint64_t field_1;
    uint64_t field_2;
};
struct indirect_placeholder_46_ret_type {
    uint64_t field_0;
    uint64_t field_1;
    uint64_t field_2;
    uint64_t field_3;
    uint64_t field_4;
    uint64_t field_5;
};
struct indirect_placeholder_49_ret_type {
    uint64_t field_0;
    uint64_t field_1;
    uint64_t field_2;
};
struct indirect_placeholder_48_ret_type {
    uint64_t field_0;
    uint64_t field_1;
    uint64_t field_2;
    uint64_t field_3;
    uint64_t field_4;
    uint64_t field_5;
};
struct indirect_placeholder_50_ret_type {
    uint64_t field_0;
    uint64_t field_1;
    uint64_t field_2;
};
struct indirect_placeholder_45_ret_type {
    uint64_t field_0;
    uint64_t field_1;
    uint64_t field_2;
    uint64_t field_3;
    uint64_t field_4;
    uint64_t field_5;
};
extern uint64_t revng_init_local_sp(uint64_t param_0);
extern uint64_t init_rbx(void);
extern void indirect_placeholder(void);
extern uint64_t init_rbp(void);
extern uint64_t init_cc_src2(void);
extern uint64_t helper_cc_compute_c_wrapper(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint32_t param_3);
extern uint64_t init_r14(void);
extern uint64_t init_r13(void);
extern uint64_t init_r12(void);
extern uint64_t indirect_placeholder_2(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint64_t param_3, uint64_t param_4);
extern uint64_t init_r15(void);
extern uint64_t indirect_placeholder_9(uint64_t param_0, uint64_t param_1, uint64_t param_2);
extern uint64_t indirect_placeholder_10(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint64_t param_3);
extern struct indirect_placeholder_47_ret_type indirect_placeholder_47(uint64_t param_0, uint64_t param_1);
extern struct indirect_placeholder_46_ret_type indirect_placeholder_46(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint64_t param_3, uint64_t param_4, uint64_t param_5, uint64_t param_6);
extern struct indirect_placeholder_49_ret_type indirect_placeholder_49(uint64_t param_0, uint64_t param_1);
extern struct indirect_placeholder_48_ret_type indirect_placeholder_48(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint64_t param_3, uint64_t param_4, uint64_t param_5, uint64_t param_6);
extern struct indirect_placeholder_50_ret_type indirect_placeholder_50(uint64_t param_0, uint64_t param_1);
extern struct indirect_placeholder_45_ret_type indirect_placeholder_45(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint64_t param_3, uint64_t param_4, uint64_t param_5, uint64_t param_6);
uint64_t bb_sparse_copy(uint64_t rdx, uint64_t rdi, uint64_t rcx, uint64_t rsi, uint64_t r9, uint64_t r8) {
    uint64_t var_81;
    uint64_t var_0;
    uint64_t var_1;
    uint64_t var_2;
    uint64_t var_3;
    uint64_t var_4;
    uint64_t var_5;
    uint64_t var_6;
    uint64_t var_7;
    uint32_t *var_8;
    uint32_t var_9;
    uint64_t rax_1;
    uint64_t var_64;
    struct indirect_placeholder_47_ret_type var_65;
    uint64_t var_66;
    uint64_t var_67;
    uint64_t var_68;
    uint64_t var_69;
    uint64_t local_sp_0;
    uint64_t local_sp_10;
    bool var_70;
    bool var_71;
    uint64_t rbp_0;
    uint64_t r12_9;
    uint64_t rbx_1;
    unsigned char *_pre_phi215;
    uint64_t r12_2;
    uint64_t var_25;
    uint64_t r14_1;
    uint64_t rbx_0;
    uint64_t r12_10;
    uint64_t var_30;
    uint64_t local_sp_3;
    uint64_t var_31;
    uint64_t r13_0;
    uint64_t r13_1;
    uint64_t var_32;
    uint64_t r12_0;
    uint64_t local_sp_1;
    unsigned char *var_33;
    unsigned char var_34;
    unsigned char var_35;
    uint64_t local_sp_12;
    uint32_t var_36;
    uint64_t var_37;
    unsigned char *var_38;
    uint64_t var_39;
    uint64_t var_40;
    uint64_t var_60;
    bool var_41;
    uint64_t r13_3;
    uint64_t var_15;
    struct indirect_placeholder_49_ret_type var_16;
    uint64_t var_17;
    uint64_t var_18;
    uint64_t var_19;
    uint64_t var_20;
    uint64_t var_14;
    uint64_t *_pre_phi219;
    uint64_t r14_5;
    uint64_t *var_21;
    uint64_t var_22;
    uint64_t var_23;
    uint64_t var_24;
    uint64_t rbp_2;
    uint64_t r14_0;
    uint64_t local_sp_6;
    uint64_t r12_1;
    uint64_t r15_0;
    uint64_t local_sp_2;
    uint64_t var_26;
    uint64_t var_27;
    bool var_28;
    uint64_t var_29;
    unsigned char *_pre210_pre_phi;
    unsigned char *_pre_phi211;
    uint64_t var_42;
    uint64_t var_43;
    unsigned char *var_44;
    uint64_t var_45;
    uint64_t var_46;
    uint64_t r12_4;
    uint64_t local_sp_5;
    uint64_t r12_5;
    uint64_t var_47;
    uint64_t var_72;
    unsigned char *var_73;
    uint64_t r14_2;
    uint64_t r13_4;
    uint64_t r12_6;
    uint64_t local_sp_7;
    uint64_t var_74;
    uint64_t *var_75;
    unsigned char *var_76;
    unsigned char var_77;
    uint64_t r14_3;
    uint64_t r12_7;
    uint64_t local_sp_8;
    uint64_t r14_6;
    uint64_t var_48;
    struct indirect_placeholder_50_ret_type var_49;
    uint64_t var_50;
    uint64_t var_51;
    uint64_t var_52;
    uint64_t var_53;
    unsigned char *_pre206;
    unsigned char *_pre_phi207;
    uint64_t r13_5;
    uint64_t r12_8;
    uint64_t local_sp_9;
    uint64_t var_54;
    uint64_t r14_4;
    uint64_t r13_6;
    uint64_t var_61;
    uint64_t var_62;
    uint64_t var_63;
    uint64_t var_55;
    uint64_t var_56;
    uint64_t var_57;
    uint64_t var_58;
    uint64_t var_59;
    uint64_t local_sp_11;
    uint64_t var_78;
    uint64_t var_79;
    uint64_t var_80;
    uint64_t var_10;
    uint64_t *var_11;
    uint64_t var_12;
    uint64_t var_13;
    unsigned int loop_state_var;
    bool switch_state_var;
    var_0 = revng_init_local_sp(0UL);
    var_1 = init_rbx();
    var_2 = init_r14();
    var_3 = init_rbp();
    var_4 = init_r13();
    var_5 = init_r12();
    var_6 = init_r15();
    var_7 = init_cc_src2();
    *(uint64_t *)(var_0 + (-8L)) = var_6;
    *(uint64_t *)(var_0 + (-16L)) = var_2;
    *(uint64_t *)(var_0 + (-24L)) = var_4;
    *(uint64_t *)(var_0 + (-32L)) = var_5;
    *(uint64_t *)(var_0 + (-40L)) = var_3;
    *(uint64_t *)(var_0 + (-48L)) = var_1;
    *(uint32_t *)(var_0 + (-80L)) = (uint32_t)rdi;
    *(uint32_t *)(var_0 + (-112L)) = (uint32_t)rsi;
    *(uint64_t *)(var_0 + (-104L)) = rdx;
    *(uint64_t *)(var_0 + (-96L)) = rcx;
    *(uint64_t *)(var_0 + (-128L)) = r8;
    var_8 = (uint32_t *)(var_0 + (-76L));
    var_9 = (uint32_t)r9;
    *var_8 = var_9;
    **(unsigned char **)(var_0 | 40UL) = (unsigned char)'\x00';
    **(uint64_t **)(var_0 | 32UL) = 0UL;
    r13_0 = 0UL;
    r14_6 = 0UL;
    r12_10 = var_5;
    rax_1 = 1UL;
    if (*(uint64_t *)(var_0 | 24UL) == 0UL) {
        return rax_1;
    }
    var_10 = var_0 + (-152L);
    *(unsigned char *)(var_0 + (-130L)) = (unsigned char)'\x00';
    *(uint32_t *)(var_0 + (-108L)) = (uint32_t)(unsigned char)var_9;
    local_sp_12 = var_10;
    rax_1 = 0UL;
    while (1U)
        {
            var_11 = (uint64_t *)(local_sp_12 + 56UL);
            var_12 = *var_11;
            var_13 = local_sp_12 + (-8L);
            *(uint64_t *)var_13 = 4213370UL;
            indirect_placeholder();
            *var_11 = var_12;
            var_25 = var_12;
            r14_5 = r14_6;
            r14_0 = r14_6;
            r12_1 = r12_10;
            local_sp_2 = var_13;
            r14_3 = r14_6;
            r12_7 = r12_10;
            local_sp_11 = var_13;
            if ((long)var_12 <= (long)18446744073709551615UL) {
                var_14 = local_sp_12 + (-16L);
                *(uint64_t *)var_14 = 4213385UL;
                indirect_placeholder();
                local_sp_8 = var_14;
                if (*(uint32_t *)var_12 == 4U) {
                    _pre_phi219 = (uint64_t *)(var_14 + 176UL);
                    r12_10 = r12_7;
                    local_sp_12 = local_sp_8;
                    r14_5 = r14_3;
                    r14_6 = r14_3;
                    local_sp_11 = local_sp_8;
                    if (*_pre_phi219 == 0UL) {
                        continue;
                    }
                    loop_state_var = 1U;
                    break;
                }
                var_15 = *(uint64_t *)(local_sp_12 + 144UL);
                *(uint64_t *)(local_sp_12 + (-24L)) = 4213412UL;
                var_16 = indirect_placeholder_49(4UL, var_15);
                var_17 = var_16.field_0;
                var_18 = var_16.field_1;
                var_19 = var_16.field_2;
                *(uint64_t *)(local_sp_12 + (-32L)) = 4213420UL;
                indirect_placeholder();
                var_20 = (uint64_t)*(uint32_t *)var_17;
                *(uint64_t *)(local_sp_12 + (-40L)) = 4213445UL;
                indirect_placeholder_48(0UL, 4314285UL, 0UL, var_17, var_20, var_18, var_19);
                loop_state_var = 0U;
                break;
            }
            if (var_12 != 0UL) {
                loop_state_var = 1U;
                break;
            }
            var_21 = (uint64_t *)*(uint64_t *)(local_sp_12 + 176UL);
            *var_21 = (*var_21 + var_12);
            var_22 = *(uint64_t *)(local_sp_12 + 16UL);
            var_23 = (var_22 == 0UL) ? *(uint64_t *)(local_sp_12 + 48UL) : var_22;
            var_24 = *(uint64_t *)(local_sp_12 + 40UL);
            *(uint64_t *)(local_sp_12 + 24UL) = var_24;
            rbx_0 = var_23;
            r15_0 = var_24;
            while (1U)
                {
                    var_26 = local_sp_2 + 8UL;
                    *(uint64_t *)var_26 = var_25;
                    var_27 = (rbx_0 > var_25) ? var_25 : rbx_0;
                    var_28 = (var_27 == 0UL);
                    rbp_0 = var_25;
                    rbx_1 = var_27;
                    r12_2 = var_27;
                    r14_1 = var_27;
                    local_sp_3 = local_sp_2;
                    r13_1 = r15_0;
                    rbp_2 = var_25;
                    local_sp_5 = local_sp_2;
                    r14_2 = var_27;
                    local_sp_9 = local_sp_2;
                    r14_4 = r14_0;
                    if (!var_28) {
                        var_42 = (r12_1 & (-256L)) | var_28;
                        var_43 = (var_25 & (-256L)) | (var_25 == var_27);
                        var_44 = (unsigned char *)(local_sp_2 + 22UL);
                        var_45 = (uint64_t)*var_44;
                        var_46 = helper_cc_compute_c_wrapper(var_45 - var_43, var_43, var_7, 14U);
                        r13_3 = var_45;
                        r12_4 = var_42;
                        _pre210_pre_phi = var_44;
                        r12_8 = var_42;
                        if (((var_46 == 0UL) ^ 1) || var_28) {
                            r12_5 = r12_4;
                            local_sp_6 = local_sp_5;
                            if (r14_0 <= (9223372036854775807UL - var_27)) {
                                var_47 = r14_0 + var_27;
                                *(unsigned char *)(local_sp_5 + 22UL) = (unsigned char)r13_3;
                                r14_1 = var_47;
                                var_72 = rbp_2 - rbx_1;
                                var_25 = var_72;
                                rbx_0 = rbx_1;
                                r14_0 = r14_1;
                                r12_1 = r12_5;
                                local_sp_2 = local_sp_6;
                                r14_2 = r14_1;
                                r12_6 = r12_5;
                                local_sp_7 = local_sp_6;
                                if (var_72 != 0UL) {
                                    r15_0 = r15_0 + rbx_1;
                                    continue;
                                }
                                loop_state_var = 2U;
                                break;
                            }
                            var_48 = *(uint64_t *)(local_sp_5 + 160UL);
                            *(uint64_t *)(local_sp_5 + (-8L)) = 4213943UL;
                            var_49 = indirect_placeholder_50(4UL, var_48);
                            var_50 = var_49.field_0;
                            var_51 = var_49.field_1;
                            var_52 = var_49.field_2;
                            *(uint64_t *)(local_sp_5 + (-16L)) = 4213971UL;
                            indirect_placeholder_45(0UL, 4314319UL, 0UL, var_50, 0UL, var_51, var_52);
                            loop_state_var = 1U;
                            break;
                        }
                        var_53 = (uint64_t)*var_44;
                        *(unsigned char *)var_26 = (unsigned char)'\x01';
                        _pre206 = (unsigned char *)(local_sp_2 + 23UL);
                        _pre_phi207 = _pre206;
                        r13_5 = var_53;
                        var_54 = r14_0 + var_27;
                        *_pre_phi207 = (unsigned char)'\x00';
                        local_sp_10 = local_sp_9;
                        r12_9 = r12_8;
                        _pre_phi211 = _pre210_pre_phi;
                        r14_4 = var_54;
                        r13_6 = r13_5;
                        rbx_1 = 0UL;
                        r12_5 = r12_9;
                        r13_4 = r13_6;
                        r12_6 = r12_9;
                        if (*_pre_phi211 == '\x00') {
                            var_60 = *(uint64_t *)(local_sp_10 + 32UL);
                            var_61 = (uint64_t)*(uint32_t *)(local_sp_10 + 40UL);
                            var_62 = local_sp_10 + (-8L);
                            *(uint64_t *)var_62 = 4213755UL;
                            var_63 = indirect_placeholder_9(r14_4, var_61, var_60);
                            local_sp_0 = var_62;
                            if (r14_4 != var_63) {
                                var_64 = *(uint64_t *)(local_sp_10 + 160UL);
                                *(uint64_t *)(local_sp_10 + (-16L)) = 4213778UL;
                                var_65 = indirect_placeholder_47(4UL, var_64);
                                var_66 = var_65.field_0;
                                var_67 = var_65.field_1;
                                var_68 = var_65.field_2;
                                *(uint64_t *)(local_sp_10 + (-24L)) = 4213786UL;
                                indirect_placeholder();
                                var_69 = (uint64_t)*(uint32_t *)var_66;
                                *(uint64_t *)(local_sp_10 + (-32L)) = 4213811UL;
                                indirect_placeholder_46(0UL, 4314302UL, 0UL, var_66, var_69, var_67, var_68);
                                loop_state_var = 1U;
                                break;
                            }
                        }
                        var_55 = (uint64_t)*(uint32_t *)(local_sp_10 + 44UL);
                        var_56 = *(uint64_t *)(local_sp_10 + 168UL);
                        var_57 = (uint64_t)*(uint32_t *)(local_sp_10 + 40UL);
                        var_58 = local_sp_10 + (-8L);
                        *(uint64_t *)var_58 = 4213845UL;
                        var_59 = indirect_placeholder_10(var_55, var_57, r14_4, var_56);
                        local_sp_0 = var_58;
                        rax_1 = var_59;
                        if ((uint64_t)(unsigned char)var_59 != 0UL) {
                            loop_state_var = 1U;
                            break;
                        }
                        local_sp_6 = local_sp_0;
                        local_sp_7 = local_sp_0;
                        if (*(unsigned char *)(local_sp_0 + 8UL) == '\x00') {
                            *(unsigned char *)(local_sp_0 + 22UL) = (unsigned char)r13_6;
                            *(uint64_t *)(local_sp_0 + 32UL) = r15_0;
                        } else {
                            var_70 = ((uint64_t)(unsigned char)r12_9 == 0UL);
                            var_71 = (*(unsigned char *)(local_sp_0 + 23UL) == '\x00');
                            r14_1 = 0UL;
                            if (var_70) {
                                if (var_71) {
                                    *(unsigned char *)(local_sp_0 + 22UL) = (unsigned char)r13_6;
                                    *(uint64_t *)(local_sp_0 + 32UL) = r15_0;
                                    rbp_2 = rbp_0;
                                } else {
                                    *(unsigned char *)(local_sp_0 + 22UL) = (unsigned char)r13_6;
                                    *(uint64_t *)(local_sp_0 + 32UL) = r15_0;
                                }
                            } else {
                                rbp_0 = 0UL;
                                if (!var_71) {
                                    loop_state_var = 0U;
                                    break;
                                }
                                *(unsigned char *)(local_sp_0 + 22UL) = (unsigned char)r13_6;
                                *(uint64_t *)(local_sp_0 + 32UL) = r15_0;
                                rbp_2 = rbp_0;
                            }
                        }
                        var_72 = rbp_2 - rbx_1;
                        var_25 = var_72;
                        rbx_0 = rbx_1;
                        r14_0 = r14_1;
                        r12_1 = r12_5;
                        local_sp_2 = local_sp_6;
                        r14_2 = r14_1;
                        r12_6 = r12_5;
                        local_sp_7 = local_sp_6;
                        if (var_72 != 0UL) {
                            loop_state_var = 2U;
                            break;
                        }
                        r15_0 = r15_0 + rbx_1;
                        continue;
                    }
                    if (*(uint64_t *)(local_sp_2 + 24UL) == 0UL) {
                        var_42 = (r12_1 & (-256L)) | var_28;
                        var_43 = (var_25 & (-256L)) | (var_25 == var_27);
                        var_44 = (unsigned char *)(local_sp_2 + 22UL);
                        var_45 = (uint64_t)*var_44;
                        var_46 = helper_cc_compute_c_wrapper(var_45 - var_43, var_43, var_7, 14U);
                        r13_3 = var_45;
                        r12_4 = var_42;
                        _pre210_pre_phi = var_44;
                        r12_8 = var_42;
                        if (!(((var_46 == 0UL) ^ 1) || var_28)) {
                            r12_5 = r12_4;
                            local_sp_6 = local_sp_5;
                            if (r14_0 > (9223372036854775807UL - var_27)) {
                                var_47 = r14_0 + var_27;
                                *(unsigned char *)(local_sp_5 + 22UL) = (unsigned char)r13_3;
                                r14_1 = var_47;
                                var_72 = rbp_2 - rbx_1;
                                var_25 = var_72;
                                rbx_0 = rbx_1;
                                r14_0 = r14_1;
                                r12_1 = r12_5;
                                local_sp_2 = local_sp_6;
                                r14_2 = r14_1;
                                r12_6 = r12_5;
                                local_sp_7 = local_sp_6;
                                if (var_72 != 0UL) {
                                    loop_state_var = 2U;
                                    break;
                                }
                                r15_0 = r15_0 + rbx_1;
                                continue;
                            }
                            var_48 = *(uint64_t *)(local_sp_5 + 160UL);
                            *(uint64_t *)(local_sp_5 + (-8L)) = 4213943UL;
                            var_49 = indirect_placeholder_50(4UL, var_48);
                            var_50 = var_49.field_0;
                            var_51 = var_49.field_1;
                            var_52 = var_49.field_2;
                            *(uint64_t *)(local_sp_5 + (-16L)) = 4213971UL;
                            indirect_placeholder_45(0UL, 4314319UL, 0UL, var_50, 0UL, var_51, var_52);
                            loop_state_var = 1U;
                            break;
                        }
                        var_53 = (uint64_t)*var_44;
                        *(unsigned char *)var_26 = (unsigned char)'\x01';
                        _pre206 = (unsigned char *)(local_sp_2 + 23UL);
                        _pre_phi207 = _pre206;
                        r13_5 = var_53;
                        var_54 = r14_0 + var_27;
                        *_pre_phi207 = (unsigned char)'\x00';
                        local_sp_10 = local_sp_9;
                        r12_9 = r12_8;
                        _pre_phi211 = _pre210_pre_phi;
                        r14_4 = var_54;
                        r13_6 = r13_5;
                    } else {
                        while (1U)
                            {
                                var_29 = local_sp_3 + (-8L);
                                *(uint64_t *)var_29 = 4213579UL;
                                indirect_placeholder();
                                r12_0 = r12_2;
                                local_sp_1 = var_29;
                                local_sp_3 = var_29;
                                if (*(unsigned char *)(local_sp_3 + 87UL) != '\x00') {
                                    loop_state_var = 1U;
                                    break;
                                }
                                var_30 = r13_1 + 1UL;
                                var_31 = r12_2 + (-1L);
                                r13_0 = 1UL;
                                r12_0 = 0UL;
                                r13_1 = var_30;
                                r12_2 = var_31;
                                if (var_31 != 0UL) {
                                    loop_state_var = 1U;
                                    break;
                                }
                                r12_0 = var_31;
                                if ((var_31 & 15UL) == 0UL) {
                                    continue;
                                }
                                loop_state_var = 0U;
                                break;
                            }
                        switch_state_var = 0;
                        switch (loop_state_var) {
                          case 0U:
                            {
                                var_32 = local_sp_3 + (-16L);
                                *(uint64_t *)var_32 = 4213616UL;
                                indirect_placeholder();
                                r13_0 = (var_30 & (-256L)) | ((uint64_t)(uint32_t)var_25 == 0UL);
                                local_sp_1 = var_32;
                            }
                            break;
                          case 1U:
                            {
                                var_33 = (unsigned char *)(local_sp_1 + 22UL);
                                var_34 = *var_33;
                                var_35 = (unsigned char)r13_0;
                                var_36 = (((uint32_t)r12_0 & (-256)) | (uint32_t)((uint64_t)(var_35 - var_34) != 0UL)) & (((uint32_t)var_25 & (-256)) | (uint32_t)(r14_0 != 0UL));
                                var_37 = (uint64_t)var_36;
                                var_38 = (unsigned char *)(local_sp_1 + 23UL);
                                *var_38 = (unsigned char)var_37;
                                var_39 = (uint64_t)(var_36 & (-256));
                                var_40 = local_sp_1 + 8UL;
                                var_41 = ((uint64_t)var_35 < (*(uint64_t *)var_40 == var_27));
                                local_sp_10 = local_sp_1;
                                r12_9 = var_39;
                                r13_3 = r13_0;
                                _pre210_pre_phi = var_33;
                                _pre_phi211 = var_33;
                                r12_4 = var_39;
                                local_sp_5 = local_sp_1;
                                _pre_phi207 = var_38;
                                r13_5 = r13_0;
                                r12_8 = var_39;
                                local_sp_9 = local_sp_1;
                                r13_6 = r13_0;
                                if (!((var_41 ^ 1) && ((var_37 & 1UL) == 0UL))) {
                                    r12_5 = r12_4;
                                    local_sp_6 = local_sp_5;
                                    if (r14_0 > (9223372036854775807UL - var_27)) {
                                        var_47 = r14_0 + var_27;
                                        *(unsigned char *)(local_sp_5 + 22UL) = (unsigned char)r13_3;
                                        r14_1 = var_47;
                                        var_72 = rbp_2 - rbx_1;
                                        var_25 = var_72;
                                        rbx_0 = rbx_1;
                                        r14_0 = r14_1;
                                        r12_1 = r12_5;
                                        local_sp_2 = local_sp_6;
                                        r14_2 = r14_1;
                                        r12_6 = r12_5;
                                        local_sp_7 = local_sp_6;
                                        if (var_72 != 0UL) {
                                            loop_state_var = 2U;
                                            switch_state_var = 1;
                                            break;
                                        }
                                        r15_0 = r15_0 + rbx_1;
                                        continue;
                                    }
                                    var_48 = *(uint64_t *)(local_sp_5 + 160UL);
                                    *(uint64_t *)(local_sp_5 + (-8L)) = 4213943UL;
                                    var_49 = indirect_placeholder_50(4UL, var_48);
                                    var_50 = var_49.field_0;
                                    var_51 = var_49.field_1;
                                    var_52 = var_49.field_2;
                                    *(uint64_t *)(local_sp_5 + (-16L)) = 4213971UL;
                                    indirect_placeholder_45(0UL, 4314319UL, 0UL, var_50, 0UL, var_51, var_52);
                                    loop_state_var = 1U;
                                    switch_state_var = 1;
                                    break;
                                }
                                *(unsigned char *)var_40 = var_41;
                                if (*var_38 == '\x00') {
                                    var_54 = r14_0 + var_27;
                                    *_pre_phi207 = (unsigned char)'\x00';
                                    local_sp_10 = local_sp_9;
                                    r12_9 = r12_8;
                                    _pre_phi211 = _pre210_pre_phi;
                                    r14_4 = var_54;
                                    r13_6 = r13_5;
                                }
                            }
                            break;
                        }
                        if (switch_state_var)
                            break;
                    }
                }
            switch_state_var = 0;
            switch (loop_state_var) {
              case 2U:
              case 0U:
                {
                    switch (loop_state_var) {
                      case 0U:
                        {
                            _pre_phi215 = (unsigned char *)(local_sp_0 + 22UL);
                        }
                        break;
                      case 2U:
                        {
                            var_73 = (unsigned char *)(local_sp_6 + 22UL);
                            _pre_phi215 = var_73;
                            r13_4 = (uint64_t)*var_73;
                        }
                        break;
                    }
                    var_74 = *(uint64_t *)(local_sp_7 + 64UL);
                    var_75 = (uint64_t *)(local_sp_7 + 176UL);
                    *var_75 = (*var_75 - var_74);
                    var_76 = *(unsigned char **)(local_sp_7 + 192UL);
                    var_77 = (unsigned char)r13_4;
                    *var_76 = var_77;
                    *_pre_phi215 = var_77;
                    _pre_phi219 = var_75;
                    r14_3 = r14_2;
                    r12_7 = r12_6;
                    local_sp_8 = local_sp_7;
                }
                break;
              case 1U:
                {
                    loop_state_var = 0U;
                    switch_state_var = 1;
                    break;
                }
                break;
            }
            if (switch_state_var)
                break;
        }
    rax_1 = 1UL;
    if (*(unsigned char *)(local_sp_11 + 22UL) != '\x00') {
        return;
    }
    var_78 = (uint64_t)*(unsigned char *)(local_sp_11 + 76UL);
    var_79 = *(uint64_t *)(local_sp_11 + 168UL);
    var_80 = (uint64_t)*(uint32_t *)(local_sp_11 + 40UL);
    *(uint64_t *)(local_sp_11 + (-8L)) = 4214120UL;
    var_81 = indirect_placeholder_2(1UL, var_78, var_80, r14_5, var_79);
    rax_1 = var_81;
}

typedef unsigned char __u_char;
typedef unsigned short __u_short;
typedef unsigned int __u_int;
typedef unsigned long __u_long;
typedef signed char __int8_t;
typedef unsigned char __uint8_t;
typedef short __int16_t;
typedef unsigned short __uint16_t;
typedef int __int32_t;
typedef unsigned int __uint32_t;
typedef long __int64_t;
typedef unsigned long __uint64_t;
typedef __int8_t __int_least8_t;
typedef __uint8_t __uint_least8_t;
typedef __int16_t __int_least16_t;
typedef __uint16_t __uint_least16_t;
typedef __int32_t __int_least32_t;
typedef __uint32_t __uint_least32_t;
typedef __int64_t __int_least64_t;
typedef __uint64_t __uint_least64_t;
typedef long __quad_t;
typedef unsigned long __u_quad_t;
typedef long __intmax_t;
typedef unsigned long __uintmax_t;
typedef unsigned long __dev_t;
typedef unsigned int __uid_t;
typedef unsigned int __gid_t;
typedef unsigned long __ino_t;
typedef unsigned long __ino64_t;
typedef unsigned int __mode_t;
typedef unsigned long __nlink_t;
typedef long __off_t;
typedef long __off64_t;
typedef int __pid_t;
typedef struct {
    int __val[2];
} __fsid_t;
typedef long __clock_t;
typedef unsigned long __rlim_t;
typedef unsigned long __rlim64_t;
typedef unsigned int __id_t;
typedef long __time_t;
typedef unsigned int __useconds_t;
typedef long __suseconds_t;
typedef int __daddr_t;
typedef int __key_t;
typedef int __clockid_t;
typedef void *__timer_t;
typedef long __blksize_t;
typedef long __blkcnt_t;
typedef long __blkcnt64_t;
typedef unsigned long __fsblkcnt_t;
typedef unsigned long __fsblkcnt64_t;
typedef unsigned long __fsfilcnt_t;
typedef unsigned long __fsfilcnt64_t;
typedef long __fsword_t;
typedef long __ssize_t;
typedef long __syscall_slong_t;
typedef unsigned long __syscall_ulong_t;
typedef __off64_t __loff_t;
typedef char *__caddr_t;
typedef long __intptr_t;
typedef unsigned int __socklen_t;
typedef int __sig_atomic_t;
typedef __int8_t int8_t;
typedef __int16_t int16_t;
typedef __int32_t int32_t;
typedef __int64_t int64_t;
typedef __uint8_t uint8_t;
typedef __uint16_t uint16_t;
typedef __uint32_t uint32_t;
typedef __uint64_t uint64_t;
typedef __int_least8_t int_least8_t;
typedef __int_least16_t int_least16_t;
typedef __int_least32_t int_least32_t;
typedef __int_least64_t int_least64_t;
typedef __uint_least8_t uint_least8_t;
typedef __uint_least16_t uint_least16_t;
typedef __uint_least32_t uint_least32_t;
typedef __uint_least64_t uint_least64_t;
typedef signed char int_fast8_t;
typedef long int_fast16_t;
typedef long int_fast32_t;
typedef long int_fast64_t;
typedef unsigned char uint_fast8_t;
typedef unsigned long uint_fast16_t;
typedef unsigned long uint_fast32_t;
typedef unsigned long uint_fast64_t;
typedef long intptr_t;
typedef unsigned long uintptr_t;
typedef __intmax_t intmax_t;
typedef __uintmax_t uintmax_t;
struct bb_randread_ret_type;
struct bb_randread_ret_type {
    uint64_t field_0;
    uint64_t field_1;
    uint64_t field_2;
    uint64_t field_3;
    uint64_t field_4;
};
extern uint64_t revng_init_local_sp(uint64_t param_0);
extern uint64_t init_rbx(void);
extern void indirect_placeholder(void);
extern uint64_t init_rbp(void);
extern uint64_t init_r14(void);
extern uint64_t init_r13(void);
extern uint64_t init_r12(void);
extern uint64_t init_r15(void);
extern uint64_t init_rax(void);
extern uint64_t init_r9(void);
extern uint64_t init_r8(void);
extern void indirect_placeholder_8(uint64_t param_0, uint64_t param_1);
extern uint64_t init_r10(void);
typedef _Bool bool;
struct bb_randread_ret_type bb_randread(uint64_t rdx, uint64_t rdi, uint64_t rsi) {
    uint64_t var_0;
    uint64_t var_1;
    uint64_t var_2;
    uint64_t var_3;
    uint64_t var_4;
    uint64_t var_5;
    uint64_t var_6;
    uint64_t var_7;
    uint64_t var_8;
    uint64_t var_9;
    uint64_t var_10;
    uint64_t var_11;
    uint64_t rbx_2;
    uint64_t local_sp_3;
    uint64_t rbx_1;
    uint64_t rbx_4;
    uint64_t rbx_0;
    struct bb_randread_ret_type mrv3;
    struct bb_randread_ret_type mrv4;
    uint64_t rbp_0;
    uint64_t local_sp_0;
    uint64_t var_28;
    uint64_t var_29;
    uint64_t rbx_3;
    uint64_t rbp_1;
    uint64_t local_sp_1;
    uint64_t var_25;
    uint64_t var_26;
    uint64_t var_27;
    uint64_t rax_0;
    uint64_t local_sp_2;
    uint64_t var_13;
    uint32_t *var_14;
    uint64_t var_15;
    uint32_t var_16;
    uint64_t var_17;
    uint64_t var_18;
    uint64_t rdi6_0;
    uint64_t rsi7_0;
    struct bb_randread_ret_type mrv;
    struct bb_randread_ret_type mrv1;
    struct bb_randread_ret_type mrv2;
    uint64_t rbp_2;
    uint64_t var_12;
    uint64_t *var_19;
    uint64_t var_20;
    uint64_t r14_0;
    uint64_t rbp_3;
    uint64_t var_30;
    uint64_t var_21;
    uint64_t r14_1;
    uint64_t rbp_4;
    uint64_t local_sp_4;
    uint64_t var_22;
    uint64_t var_23;
    uint64_t var_24;
    unsigned int loop_state_var;
    bool switch_state_var;
    var_0 = revng_init_local_sp(0UL);
    var_1 = init_rax();
    var_2 = init_rbx();
    var_3 = init_r14();
    var_4 = init_rbp();
    var_5 = init_r13();
    var_6 = init_r12();
    var_7 = init_r15();
    var_8 = init_r10();
    var_9 = init_r9();
    var_10 = init_r8();
    *(uint64_t *)(var_0 + (-8L)) = var_7;
    *(uint64_t *)(var_0 + (-16L)) = var_3;
    *(uint64_t *)(var_0 + (-24L)) = var_5;
    *(uint64_t *)(var_0 + (-32L)) = var_6;
    *(uint64_t *)(var_0 + (-40L)) = var_4;
    *(uint64_t *)(var_0 + (-48L)) = var_2;
    var_11 = var_0 + (-72L);
    rbx_2 = rdx;
    local_sp_3 = var_11;
    rbx_4 = rdx;
    rbx_3 = rdx;
    rax_0 = var_1;
    local_sp_2 = var_11;
    rsi7_0 = 1UL;
    rbp_2 = rsi;
    rbp_3 = rsi;
    rbp_4 = rsi;
    local_sp_4 = var_11;
    if (*(uint64_t *)rdi != 0UL) {
        *(uint64_t *)(local_sp_2 + (-8L)) = 4266970UL;
        indirect_placeholder();
        *(uint64_t *)(local_sp_2 + (-16L)) = 4266978UL;
        indirect_placeholder();
        var_12 = rbx_2 - rax_0;
        rdi6_0 = rbp_2;
        rbx_2 = var_12;
        while (var_12 != 0UL)
            {
                var_13 = rbp_2 + rax_0;
                var_14 = (uint32_t *)rax_0;
                var_15 = (uint64_t)*var_14;
                *(uint64_t *)(local_sp_2 + (-24L)) = 4266998UL;
                indirect_placeholder();
                *(uint64_t *)(local_sp_2 + (-32L)) = 4267010UL;
                indirect_placeholder();
                var_16 = (uint32_t)(((uint64_t)(uint32_t)rax_0 == 0UL) ? rax_0 : var_15);
                var_17 = (uint64_t)var_16;
                *var_14 = var_16;
                var_18 = local_sp_2 + (-40L);
                *(uint64_t *)var_18 = 4267030UL;
                indirect_placeholder();
                rbp_2 = var_13;
                rax_0 = var_17;
                local_sp_2 = var_18;
                *(uint64_t *)(local_sp_2 + (-8L)) = 4266970UL;
                indirect_placeholder();
                *(uint64_t *)(local_sp_2 + (-16L)) = 4266978UL;
                indirect_placeholder();
                var_12 = rbx_2 - rax_0;
                rdi6_0 = rbp_2;
                rbx_2 = var_12;
            }
        mrv.field_0 = rdi6_0;
        mrv1 = mrv;
        mrv1.field_1 = var_8;
        mrv2 = mrv1;
        mrv2.field_2 = rsi7_0;
        mrv3 = mrv2;
        mrv3.field_3 = var_9;
        mrv4 = mrv3;
        mrv4.field_4 = var_10;
        return mrv4;
    }
    var_19 = (uint64_t *)(rdi + 24UL);
    var_20 = *var_19;
    r14_0 = var_20;
    r14_1 = var_20;
    if (var_20 >= rdx) {
        var_21 = rdi + 32UL;
        *(uint64_t *)(var_0 + (-64L)) = (rdi + 2104UL);
        rdi6_0 = var_21;
        r14_0 = 2048UL;
        while (1U)
            {
                var_22 = local_sp_4 + (-8L);
                *(uint64_t *)var_22 = 4267133UL;
                indirect_placeholder();
                var_23 = rbp_4 + r14_1;
                var_24 = rbx_4 - r14_1;
                rbx_1 = var_24;
                rbx_0 = var_24;
                rbp_0 = var_23;
                local_sp_0 = var_22;
                rbp_1 = var_23;
                local_sp_1 = var_22;
                r14_1 = 2048UL;
                if (((var_23 & 7UL) == 0UL) && (var_24 > 2047UL)) {
                    var_28 = *(uint64_t *)(local_sp_0 + 8UL);
                    var_29 = local_sp_0 + (-8L);
                    *(uint64_t *)var_29 = 4267218UL;
                    indirect_placeholder_8(var_21, var_28);
                    local_sp_3 = var_29;
                    rbx_4 = rbx_0;
                    rbx_3 = rbx_0;
                    rbp_3 = rbp_0;
                    rbp_4 = rbp_0;
                    local_sp_4 = var_29;
                    if (rbx_0 > 2048UL) {
                        continue;
                    }
                    loop_state_var = 0U;
                    break;
                }
                while (1U)
                    {
                        var_25 = local_sp_1 + (-8L);
                        *(uint64_t *)var_25 = 4267158UL;
                        indirect_placeholder_8(var_21, rbp_1);
                        var_26 = rbp_1 + 2048UL;
                        var_27 = rbx_1 + (-2048L);
                        rbx_1 = var_27;
                        rbx_0 = var_27;
                        rbp_0 = var_26;
                        local_sp_0 = var_25;
                        rbp_1 = var_26;
                        local_sp_1 = var_25;
                        rsi7_0 = rbp_1;
                        if (var_27 == 0UL) {
                            if (var_27 > 2047UL) {
                                continue;
                            }
                            loop_state_var = 0U;
                            break;
                        }
                        *var_19 = 0UL;
                        loop_state_var = 1U;
                        break;
                    }
                switch_state_var = 0;
                switch (loop_state_var) {
                  case 0U:
                    {
                        break;
                    }
                    break;
                  case 1U:
                    {
                        loop_state_var = 1U;
                        switch_state_var = 1;
                        break;
                    }
                    break;
                }
                if (switch_state_var)
                    break;
            }
        switch (loop_state_var) {
          case 0U:
            {
                break;
            }
            break;
          case 1U:
            {
                mrv.field_0 = rdi6_0;
                mrv1 = mrv;
                mrv1.field_1 = var_8;
                mrv2 = mrv1;
                mrv2.field_2 = rsi7_0;
                mrv3 = mrv2;
                mrv3.field_3 = var_9;
                mrv4 = mrv3;
                mrv4.field_4 = var_10;
                return mrv4;
            }
            break;
        }
    }
    var_30 = (rdi - r14_0) + 4152UL;
    *(uint64_t *)(local_sp_3 + (-8L)) = 4267073UL;
    indirect_placeholder();
    *var_19 = (r14_0 - rbx_3);
    rdi6_0 = rbp_3;
    rsi7_0 = var_30;
}

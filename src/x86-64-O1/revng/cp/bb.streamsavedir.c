typedef unsigned char __u_char;
typedef unsigned short __u_short;
typedef unsigned int __u_int;
typedef unsigned long __u_long;
typedef signed char __int8_t;
typedef unsigned char __uint8_t;
typedef short __int16_t;
typedef unsigned short __uint16_t;
typedef int __int32_t;
typedef unsigned int __uint32_t;
typedef long __int64_t;
typedef unsigned long __uint64_t;
typedef __int8_t __int_least8_t;
typedef __uint8_t __uint_least8_t;
typedef __int16_t __int_least16_t;
typedef __uint16_t __uint_least16_t;
typedef __int32_t __int_least32_t;
typedef __uint32_t __uint_least32_t;
typedef __int64_t __int_least64_t;
typedef __uint64_t __uint_least64_t;
typedef long __quad_t;
typedef unsigned long __u_quad_t;
typedef long __intmax_t;
typedef unsigned long __uintmax_t;
typedef unsigned long __dev_t;
typedef unsigned int __uid_t;
typedef unsigned int __gid_t;
typedef unsigned long __ino_t;
typedef unsigned long __ino64_t;
typedef unsigned int __mode_t;
typedef unsigned long __nlink_t;
typedef long __off_t;
typedef long __off64_t;
typedef int __pid_t;
typedef struct {
    int __val[2];
} __fsid_t;
typedef long __clock_t;
typedef unsigned long __rlim_t;
typedef unsigned long __rlim64_t;
typedef unsigned int __id_t;
typedef long __time_t;
typedef unsigned int __useconds_t;
typedef long __suseconds_t;
typedef int __daddr_t;
typedef int __key_t;
typedef int __clockid_t;
typedef void *__timer_t;
typedef long __blksize_t;
typedef long __blkcnt_t;
typedef long __blkcnt64_t;
typedef unsigned long __fsblkcnt_t;
typedef unsigned long __fsblkcnt64_t;
typedef unsigned long __fsfilcnt_t;
typedef unsigned long __fsfilcnt64_t;
typedef long __fsword_t;
typedef long __ssize_t;
typedef long __syscall_slong_t;
typedef unsigned long __syscall_ulong_t;
typedef __off64_t __loff_t;
typedef char *__caddr_t;
typedef long __intptr_t;
typedef unsigned int __socklen_t;
typedef int __sig_atomic_t;
typedef __int8_t int8_t;
typedef __int16_t int16_t;
typedef __int32_t int32_t;
typedef __int64_t int64_t;
typedef __uint8_t uint8_t;
typedef __uint16_t uint16_t;
typedef __uint32_t uint32_t;
typedef __uint64_t uint64_t;
typedef __int_least8_t int_least8_t;
typedef __int_least16_t int_least16_t;
typedef __int_least32_t int_least32_t;
typedef __int_least64_t int_least64_t;
typedef __uint_least8_t uint_least8_t;
typedef __uint_least16_t uint_least16_t;
typedef __uint_least32_t uint_least32_t;
typedef __uint_least64_t uint_least64_t;
typedef signed char int_fast8_t;
typedef long int_fast16_t;
typedef long int_fast32_t;
typedef long int_fast64_t;
typedef unsigned char uint_fast8_t;
typedef unsigned long uint_fast16_t;
typedef unsigned long uint_fast32_t;
typedef unsigned long uint_fast64_t;
typedef long intptr_t;
typedef unsigned long uintptr_t;
typedef __intmax_t intmax_t;
typedef __uintmax_t uintmax_t;
typedef _Bool bool;
struct indirect_placeholder_275_ret_type;
struct indirect_placeholder_276_ret_type;
struct indirect_placeholder_275_ret_type {
    uint64_t field_0;
    uint64_t field_1;
    uint64_t field_2;
    uint64_t field_3;
    uint64_t field_4;
    uint64_t field_5;
};
struct indirect_placeholder_276_ret_type {
    uint64_t field_0;
    uint64_t field_1;
    uint64_t field_2;
    uint64_t field_3;
    uint64_t field_4;
    uint64_t field_5;
};
extern void abort(void);
extern uint64_t revng_init_local_sp(uint64_t param_0);
extern uint64_t init_rbx(void);
extern void indirect_placeholder(void);
extern uint64_t init_rbp(void);
extern uint64_t init_cc_src2(void);
extern uint64_t helper_cc_compute_c_wrapper(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint32_t param_3);
extern uint64_t init_r14(void);
extern uint64_t init_r13(void);
extern uint64_t init_r12(void);
extern uint64_t indirect_placeholder_1(uint64_t param_0);
extern uint64_t init_r15(void);
extern uint64_t indirect_placeholder_4(uint64_t param_0, uint64_t param_1);
extern uint64_t init_r9(void);
extern uint64_t init_r8(void);
extern void indirect_placeholder_8(uint64_t param_0, uint64_t param_1);
extern struct indirect_placeholder_275_ret_type indirect_placeholder_275(uint64_t param_0);
extern struct indirect_placeholder_276_ret_type indirect_placeholder_276(uint64_t param_0);
uint64_t bb_streamsavedir(uint64_t rdi, uint64_t rsi) {
    uint64_t var_0;
    uint64_t var_1;
    uint64_t var_2;
    uint64_t var_3;
    uint64_t var_4;
    uint64_t var_5;
    uint64_t var_6;
    uint64_t var_7;
    uint64_t var_8;
    uint64_t var_9;
    uint64_t var_10;
    uint64_t rax_4;
    uint64_t rbx_0;
    uint64_t r14_0;
    uint64_t local_sp_0;
    uint64_t rbx_2;
    uint64_t r14_3;
    uint64_t rax_2;
    uint64_t rax_0;
    uint64_t rbx_1;
    uint64_t rbp_0;
    uint64_t local_sp_1;
    uint64_t var_46;
    uint64_t var_47;
    uint64_t var_48;
    uint64_t var_49;
    uint64_t var_50;
    uint64_t local_sp_5_ph;
    uint64_t r14_4_ph;
    uint64_t var_42;
    uint64_t var_43;
    struct indirect_placeholder_275_ret_type var_44;
    uint64_t var_45;
    uint64_t var_51;
    uint64_t var_52;
    struct indirect_placeholder_276_ret_type var_53;
    uint64_t var_54;
    uint64_t var_55;
    uint64_t var_56;
    uint64_t r13_2_ph;
    uint64_t r14_1;
    uint64_t r13_1;
    uint64_t rax_1;
    uint64_t local_sp_4;
    uint64_t r14_2;
    uint64_t r15_1_ph;
    uint64_t local_sp_3;
    uint64_t r13_0;
    uint64_t r15_0;
    uint64_t local_sp_2;
    uint64_t rax_3_ph;
    uint64_t var_23;
    uint64_t var_22;
    uint64_t var_24;
    uint64_t var_25;
    uint64_t var_26;
    uint64_t var_27;
    uint64_t var_28;
    uint64_t *var_29;
    uint64_t var_30;
    uint64_t var_31;
    uint64_t var_32;
    uint64_t var_33;
    bool var_34;
    uint64_t var_35;
    uint64_t var_37;
    uint64_t var_36;
    uint64_t var_38;
    uint64_t var_39;
    uint64_t var_40;
    uint64_t rdx_0;
    uint64_t rbx_3_ph;
    uint64_t local_sp_5;
    uint64_t var_20;
    uint64_t var_21;
    uint32_t var_41;
    uint64_t var_11;
    uint32_t *var_12;
    bool var_13;
    uint64_t var_14;
    unsigned char *var_15;
    unsigned char *var_16;
    uint64_t *var_17;
    uint64_t var_18;
    uint64_t *var_19;
    unsigned int loop_state_var;
    bool switch_state_var;
    var_0 = revng_init_local_sp(0UL);
    var_1 = init_rbx();
    var_2 = init_r14();
    var_3 = init_rbp();
    var_4 = init_r13();
    var_5 = init_r12();
    var_6 = init_r15();
    var_7 = init_cc_src2();
    var_8 = init_r9();
    var_9 = init_r8();
    *(uint64_t *)(var_0 + (-8L)) = var_6;
    *(uint64_t *)(var_0 + (-16L)) = var_2;
    *(uint64_t *)(var_0 + (-24L)) = var_4;
    *(uint64_t *)(var_0 + (-32L)) = var_5;
    *(uint64_t *)(var_0 + (-40L)) = var_3;
    *(uint64_t *)(var_0 + (-48L)) = var_1;
    var_10 = *(uint64_t *)(((rsi << 3UL) & 34359738360UL) + 4317600UL);
    *(uint64_t *)(var_0 + (-88L)) = var_10;
    rax_4 = 0UL;
    rbx_0 = 0UL;
    rbx_1 = 0UL;
    rbp_0 = 0UL;
    r14_4_ph = 0UL;
    r13_2_ph = 0UL;
    r15_1_ph = 0UL;
    rax_3_ph = var_10;
    rdx_0 = 0UL;
    rbx_3_ph = 0UL;
    if (rdi == 0UL) {
        return rax_4;
    }
    var_11 = var_0 + (-104L);
    *(uint64_t *)(var_0 + (-96L)) = 0UL;
    *(uint64_t *)(var_0 + (-72L)) = 0UL;
    local_sp_5_ph = var_11;
    while (1U)
        {
            var_12 = (uint32_t *)rax_3_ph;
            var_13 = (rax_3_ph == 0UL);
            var_14 = rax_3_ph + 19UL;
            var_15 = (unsigned char *)var_14;
            var_16 = (unsigned char *)(rax_3_ph + 20UL);
            rbx_2 = rbx_3_ph;
            r14_3 = r14_4_ph;
            r14_1 = r14_4_ph;
            r13_1 = r13_2_ph;
            r14_2 = r14_4_ph;
            r13_0 = r13_2_ph;
            r15_0 = r15_1_ph;
            var_23 = r15_1_ph;
            local_sp_5 = local_sp_5_ph;
            while (1U)
                {
                    var_17 = (uint64_t *)(local_sp_5 + (-8L));
                    *var_17 = 4249007UL;
                    indirect_placeholder();
                    *var_12 = 0U;
                    var_18 = local_sp_5 + (-16L);
                    var_19 = (uint64_t *)var_18;
                    *var_19 = 4249021UL;
                    indirect_placeholder();
                    local_sp_5 = var_18;
                    if (var_13) {
                        if (*var_15 == '.') {
                            rdx_0 = (*var_16 == '.') ? 2UL : 1UL;
                        }
                        if (*(unsigned char *)((rdx_0 + rax_3_ph) + 19UL) == '\x00') {
                            continue;
                        }
                        loop_state_var = 4U;
                        break;
                    }
                    *var_17 = r14_4_ph;
                    *(uint64_t *)(local_sp_5 + (-24L)) = 4249425UL;
                    indirect_placeholder();
                    var_41 = *var_12;
                    if (var_41 == 0U) {
                        *(uint64_t *)(local_sp_5 + (-32L)) = 4249441UL;
                        indirect_placeholder();
                        *(uint64_t *)(local_sp_5 + (-40L)) = 4249449UL;
                        indirect_placeholder();
                        *(uint64_t *)(local_sp_5 + (-48L)) = 4249454UL;
                        indirect_placeholder();
                        *var_12 = var_41;
                        loop_state_var = 3U;
                        break;
                    }
                    if (*var_17 != 0UL) {
                        if (*(uint64_t *)(local_sp_5 + 8UL) != rbx_3_ph) {
                            loop_state_var = 1U;
                            break;
                        }
                        var_54 = rbx_3_ph + 1UL;
                        var_55 = *var_19;
                        *(uint64_t *)(local_sp_5 + (-32L)) = 4249607UL;
                        var_56 = indirect_placeholder_4(var_55, var_54);
                        r14_1 = var_56;
                        loop_state_var = 1U;
                        break;
                    }
                    if (r15_1_ph != 0UL) {
                        var_51 = rbx_3_ph + 1UL;
                        var_52 = local_sp_5 + (-32L);
                        *(uint64_t *)var_52 = 4249636UL;
                        var_53 = indirect_placeholder_276(var_51);
                        r14_0 = var_53.field_0;
                        local_sp_0 = var_52;
                        loop_state_var = 2U;
                        break;
                    }
                    *(uint64_t *)(local_sp_5 + (-32L)) = 4249505UL;
                    indirect_placeholder();
                    var_42 = rbx_3_ph + 1UL;
                    var_43 = local_sp_5 + (-40L);
                    *(uint64_t *)var_43 = 4249514UL;
                    var_44 = indirect_placeholder_275(var_42);
                    var_45 = var_44.field_0;
                    r14_0 = var_45;
                    rax_0 = var_45;
                    local_sp_1 = var_43;
                    loop_state_var = 0U;
                    break;
                }
            switch_state_var = 0;
            switch (loop_state_var) {
              case 4U:
                {
                    var_20 = local_sp_5 + (-24L);
                    *(uint64_t *)var_20 = 4249074UL;
                    indirect_placeholder();
                    var_21 = rax_3_ph + 1UL;
                    *(uint64_t *)local_sp_5 = var_21;
                    local_sp_3 = var_20;
                    local_sp_4 = var_20;
                    if (*var_17 == 0UL) {
                        var_29 = (uint64_t *)(local_sp_5 + 8UL);
                        var_30 = *var_29 - rbx_3_ph;
                        var_31 = helper_cc_compute_c_wrapper(var_21 - var_30, var_30, var_7, 17U);
                        rax_2 = var_30;
                        if (var_31 == 0UL) {
                            var_40 = local_sp_4 + (-8L);
                            *(uint64_t *)var_40 = 4249405UL;
                            indirect_placeholder();
                            rax_1 = rax_2;
                            r14_2 = r14_3;
                            local_sp_2 = var_40;
                        } else {
                            var_32 = var_21 + rbx_3_ph;
                            *var_29 = var_32;
                            var_33 = helper_cc_compute_c_wrapper(var_32, rbx_3_ph, var_7, 9U);
                            if (var_33 == 0UL) {
                                *(uint64_t *)(local_sp_5 + (-32L)) = 4249283UL;
                                indirect_placeholder_8(var_8, var_9);
                                abort();
                            }
                            var_34 = (r14_4_ph == 0UL);
                            var_35 = *var_29;
                            var_37 = var_35;
                            if (var_34) {
                                if (var_35 != 0UL) {
                                    if ((long)var_35 < (long)0UL) {
                                        *(uint64_t *)(local_sp_5 + (-32L)) = 4249318UL;
                                        indirect_placeholder_8(var_8, var_9);
                                        abort();
                                    }
                                }
                                *var_29 = 128UL;
                                var_37 = 128UL;
                            } else {
                                if (var_35 > 6148914691236517203UL) {
                                    *(uint64_t *)(local_sp_5 + (-32L)) = 4249340UL;
                                    indirect_placeholder_8(var_8, var_9);
                                    abort();
                                }
                                var_36 = ((var_35 >> 1UL) + var_35) + 1UL;
                                *var_29 = var_36;
                                var_37 = var_36;
                                var_38 = local_sp_5 + (-32L);
                                *(uint64_t *)var_38 = 4249385UL;
                                var_39 = indirect_placeholder_4(r14_4_ph, var_37);
                                rax_2 = var_39;
                                r14_3 = var_39;
                                local_sp_4 = var_38;
                            }
                        }
                    } else {
                        if (*var_19 == r15_1_ph) {
                            *(uint64_t *)(local_sp_3 + 40UL) = ((r15_1_ph << 3UL) + r13_1);
                            var_27 = local_sp_3 + (-8L);
                            *(uint64_t *)var_27 = 4249233UL;
                            var_28 = indirect_placeholder_1(var_14);
                            **(uint64_t **)(local_sp_3 + 32UL) = var_28;
                            rax_1 = var_28;
                            r13_0 = r13_1;
                            r15_0 = r15_1_ph + 1UL;
                            local_sp_2 = var_27;
                            local_sp_5_ph = local_sp_2;
                            r14_4_ph = r14_2;
                            r13_2_ph = r13_0;
                            r15_1_ph = r15_0;
                            rax_3_ph = rax_1;
                            rbx_3_ph = rbx_3_ph + *(uint64_t *)(local_sp_2 + 24UL);
                            continue;
                        }
                        if (r13_2_ph == 0UL) {
                            if (r15_1_ph != 0UL) {
                                if (r15_1_ph > 1152921504606846975UL) {
                                    *(uint64_t *)(local_sp_5 + (-32L)) = 4249137UL;
                                    indirect_placeholder_8(var_8, var_9);
                                    abort();
                                }
                            }
                            *var_19 = 16UL;
                            var_23 = 16UL;
                        } else {
                            if (r15_1_ph > 768614336404564649UL) {
                                *(uint64_t *)(local_sp_5 + (-32L)) = 4249159UL;
                                indirect_placeholder_8(var_8, var_9);
                                abort();
                            }
                            var_22 = ((r15_1_ph >> 1UL) + r15_1_ph) + 1UL;
                            *var_19 = var_22;
                            var_23 = var_22;
                            var_24 = var_23 << 3UL;
                            var_25 = local_sp_5 + (-32L);
                            *(uint64_t *)var_25 = 4249212UL;
                            var_26 = indirect_placeholder_4(r13_2_ph, var_24);
                            r13_1 = var_26;
                            local_sp_3 = var_25;
                        }
                    }
                }
                break;
              case 0U:
                {
                    loop_state_var = 0U;
                    switch_state_var = 1;
                    break;
                }
                break;
              case 1U:
                {
                    loop_state_var = 1U;
                    switch_state_var = 1;
                    break;
                }
                break;
              case 2U:
                {
                    loop_state_var = 2U;
                    switch_state_var = 1;
                    break;
                }
                break;
              case 3U:
                {
                    loop_state_var = 3U;
                    switch_state_var = 1;
                    break;
                }
                break;
            }
            if (switch_state_var)
                break;
        }
    switch (loop_state_var) {
      case 1U:
        {
            *(unsigned char *)(rbx_2 + r14_1) = (unsigned char)'\x00';
            rax_4 = r14_1;
        }
        break;
      case 2U:
      case 0U:
        {
            switch (loop_state_var) {
              case 0U:
                {
                    var_46 = rbx_1 + var_45;
                    *(uint64_t *)(local_sp_1 + (-8L)) = 4249544UL;
                    indirect_placeholder();
                    var_47 = rax_0 - var_46;
                    var_48 = (var_47 + rbx_1) + 1UL;
                    var_49 = local_sp_1 + (-16L);
                    *(uint64_t *)var_49 = 4249562UL;
                    indirect_placeholder();
                    var_50 = rbp_0 + 1UL;
                    rbx_0 = var_48;
                    local_sp_0 = var_49;
                    rax_0 = var_47;
                    rbx_1 = var_48;
                    rbp_0 = var_50;
                    local_sp_1 = var_49;
                    do {
                        var_46 = rbx_1 + var_45;
                        *(uint64_t *)(local_sp_1 + (-8L)) = 4249544UL;
                        indirect_placeholder();
                        var_47 = rax_0 - var_46;
                        var_48 = (var_47 + rbx_1) + 1UL;
                        var_49 = local_sp_1 + (-16L);
                        *(uint64_t *)var_49 = 4249562UL;
                        indirect_placeholder();
                        var_50 = rbp_0 + 1UL;
                        rbx_0 = var_48;
                        local_sp_0 = var_49;
                        rax_0 = var_47;
                        rbx_1 = var_48;
                        rbp_0 = var_50;
                        local_sp_1 = var_49;
                    } while (var_50 != r15_1_ph);
                }
                break;
              case 2U:
                {
                    *(uint64_t *)(local_sp_0 + (-8L)) = 4249579UL;
                    indirect_placeholder();
                    rbx_2 = rbx_0;
                    r14_1 = r14_0;
                }
                break;
            }
        }
        break;
    }
}

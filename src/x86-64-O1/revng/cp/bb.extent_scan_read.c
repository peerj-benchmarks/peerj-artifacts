typedef unsigned char __u_char;
typedef unsigned short __u_short;
typedef unsigned int __u_int;
typedef unsigned long __u_long;
typedef signed char __int8_t;
typedef unsigned char __uint8_t;
typedef short __int16_t;
typedef unsigned short __uint16_t;
typedef int __int32_t;
typedef unsigned int __uint32_t;
typedef long __int64_t;
typedef unsigned long __uint64_t;
typedef __int8_t __int_least8_t;
typedef __uint8_t __uint_least8_t;
typedef __int16_t __int_least16_t;
typedef __uint16_t __uint_least16_t;
typedef __int32_t __int_least32_t;
typedef __uint32_t __uint_least32_t;
typedef __int64_t __int_least64_t;
typedef __uint64_t __uint_least64_t;
typedef long __quad_t;
typedef unsigned long __u_quad_t;
typedef long __intmax_t;
typedef unsigned long __uintmax_t;
typedef unsigned long __dev_t;
typedef unsigned int __uid_t;
typedef unsigned int __gid_t;
typedef unsigned long __ino_t;
typedef unsigned long __ino64_t;
typedef unsigned int __mode_t;
typedef unsigned long __nlink_t;
typedef long __off_t;
typedef long __off64_t;
typedef int __pid_t;
typedef struct {
    int __val[2];
} __fsid_t;
typedef long __clock_t;
typedef unsigned long __rlim_t;
typedef unsigned long __rlim64_t;
typedef unsigned int __id_t;
typedef long __time_t;
typedef unsigned int __useconds_t;
typedef long __suseconds_t;
typedef int __daddr_t;
typedef int __key_t;
typedef int __clockid_t;
typedef void *__timer_t;
typedef long __blksize_t;
typedef long __blkcnt_t;
typedef long __blkcnt64_t;
typedef unsigned long __fsblkcnt_t;
typedef unsigned long __fsblkcnt64_t;
typedef unsigned long __fsfilcnt_t;
typedef unsigned long __fsfilcnt64_t;
typedef long __fsword_t;
typedef long __ssize_t;
typedef long __syscall_slong_t;
typedef unsigned long __syscall_ulong_t;
typedef __off64_t __loff_t;
typedef char *__caddr_t;
typedef long __intptr_t;
typedef unsigned int __socklen_t;
typedef int __sig_atomic_t;
typedef __int8_t int8_t;
typedef __int16_t int16_t;
typedef __int32_t int32_t;
typedef __int64_t int64_t;
typedef __uint8_t uint8_t;
typedef __uint16_t uint16_t;
typedef __uint32_t uint32_t;
typedef __uint64_t uint64_t;
typedef __int_least8_t int_least8_t;
typedef __int_least16_t int_least16_t;
typedef __int_least32_t int_least32_t;
typedef __int_least64_t int_least64_t;
typedef __uint_least8_t uint_least8_t;
typedef __uint_least16_t uint_least16_t;
typedef __uint_least32_t uint_least32_t;
typedef __uint_least64_t uint_least64_t;
typedef signed char int_fast8_t;
typedef long int_fast16_t;
typedef long int_fast32_t;
typedef long int_fast64_t;
typedef unsigned char uint_fast8_t;
typedef unsigned long uint_fast16_t;
typedef unsigned long uint_fast32_t;
typedef unsigned long uint_fast64_t;
typedef long intptr_t;
typedef unsigned long uintptr_t;
typedef __intmax_t intmax_t;
typedef __uintmax_t uintmax_t;
struct bb_extent_scan_read_ret_type;
struct bb_extent_scan_read_ret_type {
    uint64_t field_0;
    uint64_t field_1;
    uint64_t field_2;
    uint64_t field_3;
};
extern void abort(void);
extern uint64_t revng_init_local_sp(uint64_t param_0);
extern uint64_t init_rbx(void);
extern void indirect_placeholder(void);
extern uint64_t init_rbp(void);
extern uint64_t init_cc_src2(void);
extern uint64_t helper_cc_compute_c_wrapper(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint32_t param_3);
extern uint64_t init_r14(void);
extern uint64_t init_r13(void);
extern uint64_t init_r12(void);
extern uint64_t init_r15(void);
extern uint64_t indirect_placeholder_4(uint64_t param_0, uint64_t param_1);
extern uint64_t init_r9(void);
extern uint64_t init_r8(void);
extern void indirect_placeholder_8(uint64_t param_0, uint64_t param_1);
extern uint64_t init_r10(void);
typedef _Bool bool;
struct bb_extent_scan_read_ret_type bb_extent_scan_read(uint64_t rdi) {
    uint64_t var_0;
    uint64_t var_1;
    uint64_t var_2;
    uint64_t var_3;
    uint64_t var_4;
    uint64_t var_5;
    uint64_t var_6;
    uint64_t var_7;
    uint64_t var_8;
    uint64_t var_9;
    uint64_t var_10;
    uint64_t var_11;
    uint64_t *var_12;
    uint64_t var_13;
    uint64_t *var_14;
    uint32_t *var_15;
    uint64_t *var_16;
    unsigned char *var_17;
    uint64_t r14_4;
    uint64_t rbp_4;
    uint64_t rbp_1;
    uint64_t r14_1;
    uint64_t r14_0;
    uint64_t r13_1;
    uint64_t rbp_0;
    uint64_t r13_0;
    uint64_t local_sp_0;
    uint64_t var_32;
    uint64_t *var_33;
    uint64_t var_34;
    uint64_t *var_35;
    uint64_t local_sp_1;
    uint64_t var_36;
    uint64_t var_47;
    uint32_t var_37;
    uint64_t var_38;
    uint32_t *var_39;
    uint32_t var_40;
    uint32_t *var_41;
    uint64_t *var_42;
    uint64_t *_cast_pre_phi;
    uint64_t var_43;
    uint64_t *var_44;
    uint64_t var_45;
    uint64_t var_46;
    uint64_t *_pre_phi99;
    uint64_t rbp_3;
    uint64_t var_48;
    uint64_t var_54;
    uint32_t *_pre_phi104;
    uint64_t var_49;
    uint64_t var_50;
    uint64_t var_51;
    uint64_t var_52;
    uint64_t var_53;
    uint64_t rax_0;
    uint64_t var_55;
    uint64_t var_56;
    uint64_t var_57;
    uint64_t r14_2;
    uint64_t rax_1;
    uint64_t var_58;
    uint64_t var_59;
    uint64_t rbp_2;
    uint64_t local_sp_2;
    uint64_t r14_3;
    uint32_t var_60;
    uint64_t var_61;
    uint32_t var_62;
    uint64_t local_sp_4;
    uint64_t var_18;
    uint64_t var_19;
    uint32_t var_20;
    struct bb_extent_scan_read_ret_type mrv;
    struct bb_extent_scan_read_ret_type mrv1;
    struct bb_extent_scan_read_ret_type mrv2;
    struct bb_extent_scan_read_ret_type mrv3;
    uint64_t var_21;
    uint64_t var_22;
    uint64_t var_24;
    uint64_t var_23;
    uint64_t local_sp_3;
    uint64_t var_25;
    uint64_t var_26;
    uint64_t var_27;
    uint64_t var_28;
    uint64_t var_29;
    uint64_t var_30;
    uint64_t var_31;
    unsigned int loop_state_var;
    bool switch_state_var;
    var_0 = revng_init_local_sp(0UL);
    var_1 = init_rbx();
    var_2 = init_r14();
    var_3 = init_rbp();
    var_4 = init_r13();
    var_5 = init_r12();
    var_6 = init_r15();
    var_7 = init_cc_src2();
    var_8 = init_r10();
    var_9 = init_r9();
    var_10 = init_r8();
    *(uint64_t *)(var_0 + (-8L)) = var_6;
    *(uint64_t *)(var_0 + (-16L)) = var_2;
    *(uint64_t *)(var_0 + (-24L)) = var_4;
    *(uint64_t *)(var_0 + (-32L)) = var_5;
    *(uint64_t *)(var_0 + (-40L)) = var_3;
    *(uint64_t *)(var_0 + (-48L)) = var_1;
    var_11 = var_0 + (-4168L);
    var_12 = (uint64_t *)(rdi + 40UL);
    var_13 = *var_12;
    *(uint64_t *)(var_0 + (-4160L)) = (var_0 + (-4120L));
    var_14 = (uint64_t *)(rdi + 8UL);
    var_15 = (uint32_t *)(rdi + 16UL);
    var_16 = (uint64_t *)(rdi + 24UL);
    var_17 = (unsigned char *)(rdi + 33UL);
    r14_4 = 0UL;
    rbp_4 = var_13;
    r13_0 = 0UL;
    rax_1 = 0UL;
    local_sp_4 = var_11;
    while (1U)
        {
            *(uint64_t *)(local_sp_4 + (-8L)) = 4232024UL;
            indirect_placeholder();
            var_18 = *var_14;
            *(uint64_t *)(local_sp_4 + 8UL) = var_18;
            *(uint32_t *)(local_sp_4 + 24UL) = *var_15;
            *(uint32_t *)(local_sp_4 + 32UL) = 72U;
            *(uint64_t *)(local_sp_4 + 16UL) = (var_18 ^ (-1L));
            var_19 = local_sp_4 + (-16L);
            *(uint64_t *)var_19 = 4232083UL;
            indirect_placeholder();
            var_20 = *(uint32_t *)(local_sp_4 + 20UL);
            r14_0 = r14_4;
            r14_2 = r14_4;
            local_sp_3 = var_19;
            if (var_20 != 0U) {
                *var_17 = (unsigned char)'\x01';
                rax_1 = (*var_14 != 0UL);
                break;
            }
            var_21 = (uint64_t)var_20 ^ (-1L);
            var_22 = *var_16;
            var_24 = var_22;
            if (var_22 > var_21) {
                var_23 = local_sp_4 + (-24L);
                *(uint64_t *)var_23 = 4232180UL;
                indirect_placeholder();
                var_24 = *var_16;
                local_sp_3 = var_23;
            }
            var_25 = var_24 + (uint64_t)*(uint32_t *)(local_sp_3 + 36UL);
            *var_16 = var_25;
            var_26 = *var_12;
            if (var_25 > 384307168202282325UL) {
                *(uint64_t *)(local_sp_3 + (-8L)) = 4232222UL;
                indirect_placeholder_8(var_9, var_10);
                abort();
            }
            var_27 = rbp_4 - var_26;
            var_28 = var_25 * 24UL;
            var_29 = local_sp_3 + (-8L);
            *(uint64_t *)var_29 = 4232235UL;
            var_30 = indirect_placeholder_4(var_26, var_28);
            *var_12 = var_30;
            var_31 = var_27 + var_30;
            rbp_0 = var_31;
            local_sp_0 = var_29;
            rbp_2 = var_31;
            local_sp_2 = var_29;
            if (*(uint32_t *)(local_sp_3 + 28UL) == 0U) {
                r14_3 = r14_2;
                rbp_3 = rbp_2;
                local_sp_4 = local_sp_2;
                rax_1 = 1UL;
                if ((*(unsigned char *)(rbp_2 + 16UL) & '\x01') == '\x00') {
                    *var_17 = (unsigned char)'\x01';
                } else {
                    var_60 = (uint32_t)r14_2;
                    if ((uint64_t)var_60 <= 72UL & *var_17 == '\x00') {
                        var_61 = (uint64_t)(var_60 + (-1));
                        r14_3 = var_61;
                        rbp_3 = ((var_61 * 24UL) + *var_12) + (-24L);
                    }
                }
                var_62 = (uint32_t)r14_3;
                *var_16 = (uint64_t)var_62;
                r14_4 = r14_3;
                rbp_4 = rbp_3;
                if (*var_17 == '\x00') {
                    break;
                }
                *var_14 = (*(uint64_t *)(rbp_3 + 8UL) + *(uint64_t *)rbp_3);
                if ((uint64_t)(var_62 & (-8)) <= 71UL) {
                    continue;
                }
                break;
            }
            while (1U)
                {
                    var_32 = (r13_0 * 56UL) + *(uint64_t *)(local_sp_0 + 8UL);
                    var_33 = (uint64_t *)(var_32 + 16UL);
                    var_34 = 9223372036854775807UL - *var_33;
                    var_35 = (uint64_t *)var_32;
                    local_sp_1 = local_sp_0;
                    r14_1 = r14_0;
                    rbp_1 = rbp_0;
                    r13_1 = r13_0;
                    if (*var_35 > var_34) {
                        var_36 = local_sp_0 + (-8L);
                        *(uint64_t *)var_36 = 4232320UL;
                        indirect_placeholder();
                        local_sp_1 = var_36;
                    }
                    var_37 = (uint32_t)r14_0;
                    var_38 = (uint64_t)var_37;
                    local_sp_0 = local_sp_1;
                    local_sp_2 = local_sp_1;
                    if (var_38 != 0UL) {
                        var_39 = (uint32_t *)(var_32 + 40UL);
                        var_40 = *var_39 & (-2);
                        var_41 = (uint32_t *)(rbp_0 + 16UL);
                        _pre_phi104 = var_39;
                        if ((uint64_t)(*var_41 - var_40) == 0UL) {
                            var_42 = (uint64_t *)(rbp_0 + 8UL);
                            var_43 = *var_42;
                            var_44 = (uint64_t *)rbp_0;
                            var_45 = var_43 + *var_44;
                            var_46 = *var_35;
                            _cast_pre_phi = var_44;
                            _pre_phi99 = var_42;
                            var_47 = var_46;
                            if (var_45 != var_46) {
                                *var_42 = (var_43 + *var_33);
                                *var_41 = *var_39;
                                var_59 = (uint64_t)((uint32_t)r13_1 + 1U);
                                r14_0 = r14_1;
                                rbp_0 = rbp_1;
                                r13_0 = var_59;
                                r14_2 = r14_1;
                                rbp_2 = rbp_1;
                                if (var_59 < (uint64_t)*(uint32_t *)(local_sp_1 + 36UL)) {
                                    continue;
                                }
                                loop_state_var = 0U;
                                break;
                            }
                        }
                        _cast_pre_phi = (uint64_t *)rbp_0;
                        _pre_phi99 = (uint64_t *)(rbp_0 + 8UL);
                        var_47 = *var_35;
                        var_48 = *_pre_phi99 + *_cast_pre_phi;
                        var_51 = var_47;
                        var_54 = var_47;
                        rax_0 = var_48;
                        if (var_48 <= var_47) {
                            var_55 = rax_0 - var_54;
                            var_56 = *var_33;
                            var_57 = helper_cc_compute_c_wrapper(var_55 - var_56, var_56, var_7, 17U);
                            if (var_57 != 0UL) {
                                if (*var_14 != 0UL) {
                                    loop_state_var = 1U;
                                    break;
                                }
                                *(unsigned char *)(rdi + 32UL) = (unsigned char)'\x01';
                                loop_state_var = 1U;
                                break;
                            }
                            *var_35 = rax_0;
                            *var_33 = (var_56 - var_55);
                            var_58 = (uint64_t)((uint32_t)r13_0 + (-1));
                            r13_1 = var_58;
                            var_59 = (uint64_t)((uint32_t)r13_1 + 1U);
                            r14_0 = r14_1;
                            rbp_0 = rbp_1;
                            r13_0 = var_59;
                            r14_2 = r14_1;
                            rbp_2 = rbp_1;
                            if (var_59 < (uint64_t)*(uint32_t *)(local_sp_1 + 36UL)) {
                                continue;
                            }
                            loop_state_var = 0U;
                            break;
                        }
                    }
                    var_49 = *var_14;
                    var_50 = *var_35;
                    var_51 = var_50;
                    var_54 = var_50;
                    rax_0 = var_49;
                    if (var_49 <= var_50) {
                        var_55 = rax_0 - var_54;
                        var_56 = *var_33;
                        var_57 = helper_cc_compute_c_wrapper(var_55 - var_56, var_56, var_7, 17U);
                        if (var_57 != 0UL) {
                            if (*var_14 != 0UL) {
                                loop_state_var = 1U;
                                break;
                            }
                            *(unsigned char *)(rdi + 32UL) = (unsigned char)'\x01';
                            loop_state_var = 1U;
                            break;
                        }
                        *var_35 = rax_0;
                        *var_33 = (var_56 - var_55);
                        var_58 = (uint64_t)((uint32_t)r13_0 + (-1));
                        r13_1 = var_58;
                        var_59 = (uint64_t)((uint32_t)r13_1 + 1U);
                        r14_0 = r14_1;
                        rbp_0 = rbp_1;
                        r13_0 = var_59;
                        r14_2 = r14_1;
                        rbp_2 = rbp_1;
                        if (var_59 < (uint64_t)*(uint32_t *)(local_sp_1 + 36UL)) {
                            continue;
                        }
                        loop_state_var = 0U;
                        break;
                    }
                    _pre_phi104 = (uint32_t *)(var_32 + 40UL);
                }
            switch_state_var = 0;
            switch (loop_state_var) {
              case 1U:
                {
                    switch_state_var = 1;
                    break;
                }
                break;
              case 0U:
                {
                    break;
                }
                break;
            }
            if (switch_state_var)
                break;
        }
    mrv.field_0 = rax_1;
    mrv1 = mrv;
    mrv1.field_1 = var_8;
    mrv2 = mrv1;
    mrv2.field_2 = var_9;
    mrv3 = mrv2;
    mrv3.field_3 = var_10;
    return mrv3;
}

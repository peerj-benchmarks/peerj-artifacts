typedef unsigned char __u_char;
typedef unsigned short __u_short;
typedef unsigned int __u_int;
typedef unsigned long __u_long;
typedef signed char __int8_t;
typedef unsigned char __uint8_t;
typedef short __int16_t;
typedef unsigned short __uint16_t;
typedef int __int32_t;
typedef unsigned int __uint32_t;
typedef long __int64_t;
typedef unsigned long __uint64_t;
typedef __int8_t __int_least8_t;
typedef __uint8_t __uint_least8_t;
typedef __int16_t __int_least16_t;
typedef __uint16_t __uint_least16_t;
typedef __int32_t __int_least32_t;
typedef __uint32_t __uint_least32_t;
typedef __int64_t __int_least64_t;
typedef __uint64_t __uint_least64_t;
typedef long __quad_t;
typedef unsigned long __u_quad_t;
typedef long __intmax_t;
typedef unsigned long __uintmax_t;
typedef unsigned long __dev_t;
typedef unsigned int __uid_t;
typedef unsigned int __gid_t;
typedef unsigned long __ino_t;
typedef unsigned long __ino64_t;
typedef unsigned int __mode_t;
typedef unsigned long __nlink_t;
typedef long __off_t;
typedef long __off64_t;
typedef int __pid_t;
typedef struct {
    int __val[2];
} __fsid_t;
typedef long __clock_t;
typedef unsigned long __rlim_t;
typedef unsigned long __rlim64_t;
typedef unsigned int __id_t;
typedef long __time_t;
typedef unsigned int __useconds_t;
typedef long __suseconds_t;
typedef int __daddr_t;
typedef int __key_t;
typedef int __clockid_t;
typedef void *__timer_t;
typedef long __blksize_t;
typedef long __blkcnt_t;
typedef long __blkcnt64_t;
typedef unsigned long __fsblkcnt_t;
typedef unsigned long __fsblkcnt64_t;
typedef unsigned long __fsfilcnt_t;
typedef unsigned long __fsfilcnt64_t;
typedef long __fsword_t;
typedef long __ssize_t;
typedef long __syscall_slong_t;
typedef unsigned long __syscall_ulong_t;
typedef __off64_t __loff_t;
typedef char *__caddr_t;
typedef long __intptr_t;
typedef unsigned int __socklen_t;
typedef int __sig_atomic_t;
typedef __int8_t int8_t;
typedef __int16_t int16_t;
typedef __int32_t int32_t;
typedef __int64_t int64_t;
typedef __uint8_t uint8_t;
typedef __uint16_t uint16_t;
typedef __uint32_t uint32_t;
typedef __uint64_t uint64_t;
typedef __int_least8_t int_least8_t;
typedef __int_least16_t int_least16_t;
typedef __int_least32_t int_least32_t;
typedef __int_least64_t int_least64_t;
typedef __uint_least8_t uint_least8_t;
typedef __uint_least16_t uint_least16_t;
typedef __uint_least32_t uint_least32_t;
typedef __uint_least64_t uint_least64_t;
typedef signed char int_fast8_t;
typedef long int_fast16_t;
typedef long int_fast32_t;
typedef long int_fast64_t;
typedef unsigned char uint_fast8_t;
typedef unsigned long uint_fast16_t;
typedef unsigned long uint_fast32_t;
typedef unsigned long uint_fast64_t;
typedef long intptr_t;
typedef unsigned long uintptr_t;
typedef __intmax_t intmax_t;
typedef __uintmax_t uintmax_t;
typedef _Bool bool;
struct helper_pxor_xmm_wrapper_ret_type;
struct type_5;
struct type_7;
struct helper_pxor_xmm_wrapper_93_ret_type;
struct helper_cvtsq2ss_wrapper_ret_type;
struct helper_addss_wrapper_ret_type;
struct helper_mulss_wrapper_96_ret_type;
struct helper_ucomiss_wrapper_97_ret_type;
struct helper_cvttss2sq_wrapper_ret_type;
struct helper_subss_wrapper_ret_type;
struct helper_ucomiss_wrapper_99_ret_type;
struct helper_ucomiss_wrapper_ret_type;
struct helper_cvtsq2ss_wrapper_94_ret_type;
struct helper_addss_wrapper_95_ret_type;
struct helper_mulss_wrapper_ret_type;
struct helper_mulss_wrapper_98_ret_type;
struct helper_pxor_xmm_wrapper_ret_type {
    uint64_t field_0;
    uint64_t field_1;
};
struct type_5 {
};
struct type_7 {
};
struct helper_pxor_xmm_wrapper_93_ret_type {
    uint64_t field_0;
    uint64_t field_1;
};
struct helper_cvtsq2ss_wrapper_ret_type {
    uint64_t field_0;
    unsigned char field_1;
};
struct helper_addss_wrapper_ret_type {
    uint64_t field_0;
    unsigned char field_1;
};
struct helper_mulss_wrapper_96_ret_type {
    uint64_t field_0;
    unsigned char field_1;
};
struct helper_ucomiss_wrapper_97_ret_type {
    uint64_t field_0;
    unsigned char field_1;
};
struct helper_cvttss2sq_wrapper_ret_type {
    uint64_t field_0;
    unsigned char field_1;
};
struct helper_subss_wrapper_ret_type {
    uint64_t field_0;
    unsigned char field_1;
};
struct helper_ucomiss_wrapper_99_ret_type {
    uint64_t field_0;
    unsigned char field_1;
};
struct helper_ucomiss_wrapper_ret_type {
    uint64_t field_0;
    unsigned char field_1;
};
struct helper_cvtsq2ss_wrapper_94_ret_type {
    uint64_t field_0;
    unsigned char field_1;
};
struct helper_addss_wrapper_95_ret_type {
    uint64_t field_0;
    unsigned char field_1;
};
struct helper_mulss_wrapper_ret_type {
    uint64_t field_0;
    unsigned char field_1;
};
struct helper_mulss_wrapper_98_ret_type {
    uint64_t field_0;
    unsigned char field_1;
};
extern uint64_t revng_init_local_sp(uint64_t param_0);
extern uint64_t init_rbx(void);
extern uint64_t init_state_0x8558(void);
extern unsigned char init_state_0x8549(void);
extern unsigned char init_state_0x854c(void);
extern unsigned char init_state_0x8548(void);
extern unsigned char init_state_0x854b(void);
extern unsigned char init_state_0x8547(void);
extern unsigned char init_state_0x854d(void);
extern void indirect_placeholder_1(void);
extern uint64_t init_rbp(void);
extern uint64_t init_cc_src2(void);
extern uint64_t helper_cc_compute_c_wrapper(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint32_t param_3);
extern uint64_t init_state_0x8560(void);
extern struct helper_pxor_xmm_wrapper_ret_type helper_pxor_xmm_wrapper(struct type_5 *param_0, struct type_7 *param_1, struct type_7 *param_2, uint64_t param_3, uint64_t param_4);
extern uint64_t init_state_0x8598(void);
extern uint64_t init_state_0x85a0(void);
extern uint64_t init_r12(void);
extern struct helper_pxor_xmm_wrapper_93_ret_type helper_pxor_xmm_wrapper_93(struct type_5 *param_0, struct type_7 *param_1, struct type_7 *param_2, uint64_t param_3, uint64_t param_4);
extern uint64_t init_state_0x8d58(void);
extern struct helper_cvtsq2ss_wrapper_ret_type helper_cvtsq2ss_wrapper(struct type_5 *param_0, struct type_7 *param_1, uint64_t param_2, unsigned char param_3, unsigned char param_4, unsigned char param_5, unsigned char param_6);
extern struct helper_addss_wrapper_ret_type helper_addss_wrapper(struct type_5 *param_0, struct type_7 *param_1, struct type_7 *param_2, uint64_t param_3, unsigned char param_4, unsigned char param_5, unsigned char param_6, unsigned char param_7, unsigned char param_8, unsigned char param_9);
extern struct helper_mulss_wrapper_96_ret_type helper_mulss_wrapper_96(struct type_5 *param_0, struct type_7 *param_1, struct type_7 *param_2, uint64_t param_3, uint64_t param_4, unsigned char param_5, unsigned char param_6, unsigned char param_7, unsigned char param_8, unsigned char param_9, unsigned char param_10);
extern struct helper_ucomiss_wrapper_97_ret_type helper_ucomiss_wrapper_97(struct type_5 *param_0, struct type_7 *param_1, struct type_7 *param_2, uint64_t param_3, uint64_t param_4, unsigned char param_5, unsigned char param_6);
extern struct helper_cvttss2sq_wrapper_ret_type helper_cvttss2sq_wrapper(struct type_5 *param_0, struct type_7 *param_1, uint64_t param_2, unsigned char param_3, unsigned char param_4);
extern struct helper_subss_wrapper_ret_type helper_subss_wrapper(struct type_5 *param_0, struct type_7 *param_1, struct type_7 *param_2, uint64_t param_3, uint64_t param_4, unsigned char param_5, unsigned char param_6, unsigned char param_7, unsigned char param_8, unsigned char param_9, unsigned char param_10);
extern uint64_t indirect_placeholder_15(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint64_t param_3);
extern uint64_t indirect_placeholder(uint64_t param_0, uint64_t param_1);
extern uint64_t init_r9(void);
extern struct helper_ucomiss_wrapper_99_ret_type helper_ucomiss_wrapper_99(struct type_5 *param_0, struct type_7 *param_1, struct type_7 *param_2, uint64_t param_3, uint64_t param_4, unsigned char param_5, unsigned char param_6);
extern struct helper_ucomiss_wrapper_ret_type helper_ucomiss_wrapper(struct type_5 *param_0, struct type_7 *param_1, struct type_7 *param_2, uint64_t param_3, uint64_t param_4, unsigned char param_5, unsigned char param_6);
extern uint64_t init_r10(void);
extern uint64_t init_r8(void);
extern struct helper_cvtsq2ss_wrapper_94_ret_type helper_cvtsq2ss_wrapper_94(struct type_5 *param_0, struct type_7 *param_1, uint64_t param_2, unsigned char param_3, unsigned char param_4, unsigned char param_5, unsigned char param_6);
extern struct helper_addss_wrapper_95_ret_type helper_addss_wrapper_95(struct type_5 *param_0, struct type_7 *param_1, struct type_7 *param_2, uint64_t param_3, unsigned char param_4, unsigned char param_5, unsigned char param_6, unsigned char param_7, unsigned char param_8, unsigned char param_9);
extern uint64_t indirect_placeholder_2(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint64_t param_3, uint64_t param_4, uint64_t param_5);
extern struct helper_mulss_wrapper_ret_type helper_mulss_wrapper(struct type_5 *param_0, struct type_7 *param_1, struct type_7 *param_2, uint64_t param_3, uint64_t param_4, unsigned char param_5, unsigned char param_6, unsigned char param_7, unsigned char param_8, unsigned char param_9, unsigned char param_10);
extern struct helper_mulss_wrapper_98_ret_type helper_mulss_wrapper_98(struct type_5 *param_0, struct type_7 *param_1, struct type_7 *param_2, uint64_t param_3, uint64_t param_4, unsigned char param_5, unsigned char param_6, unsigned char param_7, unsigned char param_8, unsigned char param_9, unsigned char param_10);
uint64_t bb_hash_delete(uint64_t rdi, uint64_t rsi) {
    struct helper_cvttss2sq_wrapper_ret_type var_86;
    struct helper_cvttss2sq_wrapper_ret_type var_84;
    struct helper_cvttss2sq_wrapper_ret_type var_77;
    struct helper_cvttss2sq_wrapper_ret_type var_75;
    struct helper_cvtsq2ss_wrapper_ret_type var_53;
    uint64_t var_0;
    uint64_t var_1;
    uint64_t var_2;
    uint64_t var_3;
    uint64_t var_4;
    uint64_t var_5;
    uint64_t var_6;
    uint64_t var_7;
    uint64_t var_8;
    uint64_t var_9;
    uint64_t var_10;
    uint64_t var_11;
    uint64_t var_12;
    unsigned char var_13;
    unsigned char var_14;
    unsigned char var_15;
    unsigned char var_16;
    unsigned char var_17;
    unsigned char var_18;
    uint64_t var_19;
    uint64_t var_20;
    uint64_t rax_0;
    uint64_t *var_89;
    uint64_t var_90;
    uint64_t rdi1_0;
    uint64_t local_sp_0;
    uint64_t var_91;
    uint64_t var_92;
    uint64_t state_0x8560_0;
    unsigned char storemerge3;
    uint64_t state_0x85a0_0;
    uint64_t var_54;
    struct helper_cvtsq2ss_wrapper_ret_type var_55;
    struct helper_addss_wrapper_ret_type var_56;
    uint64_t rcx_0;
    uint64_t state_0x8558_0;
    unsigned char storemerge1;
    uint64_t var_57;
    uint64_t state_0x8598_1;
    struct helper_cvtsq2ss_wrapper_94_ret_type var_58;
    uint64_t rcx_1;
    uint64_t var_59;
    struct helper_cvtsq2ss_wrapper_94_ret_type var_60;
    struct helper_addss_wrapper_95_ret_type var_61;
    uint64_t state_0x8598_0;
    unsigned char storemerge;
    struct helper_mulss_wrapper_ret_type var_62;
    struct helper_ucomiss_wrapper_ret_type var_63;
    unsigned char var_64;
    unsigned char var_65;
    uint64_t var_66;
    bool var_67;
    struct helper_mulss_wrapper_96_ret_type var_68;
    uint64_t var_69;
    unsigned char var_70;
    struct helper_ucomiss_wrapper_97_ret_type var_71;
    uint64_t var_72;
    unsigned char var_73;
    uint64_t var_74;
    uint64_t rsi2_0;
    struct helper_subss_wrapper_ret_type var_76;
    struct helper_mulss_wrapper_96_ret_type var_78;
    uint64_t var_79;
    struct helper_ucomiss_wrapper_97_ret_type var_80;
    uint64_t var_81;
    unsigned char var_82;
    uint64_t var_83;
    struct helper_subss_wrapper_ret_type var_85;
    uint64_t var_87;
    uint64_t var_88;
    uint64_t *var_21;
    uint64_t *var_22;
    uint64_t var_23;
    struct helper_pxor_xmm_wrapper_93_ret_type var_24;
    uint64_t var_25;
    struct helper_cvtsq2ss_wrapper_ret_type var_26;
    uint64_t state_0x8558_1;
    uint64_t var_27;
    struct helper_pxor_xmm_wrapper_93_ret_type var_28;
    uint64_t var_29;
    struct helper_cvtsq2ss_wrapper_ret_type var_30;
    struct helper_addss_wrapper_ret_type var_31;
    uint64_t var_32;
    uint32_t *var_33;
    uint64_t *var_34;
    uint64_t var_35;
    struct helper_pxor_xmm_wrapper_ret_type var_36;
    uint64_t var_37;
    struct helper_cvtsq2ss_wrapper_94_ret_type var_38;
    uint64_t rcx_2;
    uint64_t var_39;
    struct helper_pxor_xmm_wrapper_ret_type var_40;
    uint64_t var_41;
    struct helper_cvtsq2ss_wrapper_94_ret_type var_42;
    struct helper_addss_wrapper_95_ret_type var_43;
    unsigned char storemerge2;
    uint64_t var_44;
    uint64_t var_45;
    struct helper_mulss_wrapper_98_ret_type var_46;
    uint64_t var_47;
    struct helper_ucomiss_wrapper_99_ret_type var_48;
    uint64_t var_49;
    unsigned char var_50;
    uint64_t var_51;
    uint64_t var_52;
    var_0 = revng_init_local_sp(0UL);
    var_1 = init_state_0x8598();
    var_2 = init_state_0x85a0();
    var_3 = init_rbx();
    var_4 = init_rbp();
    var_5 = init_r12();
    var_6 = init_cc_src2();
    var_7 = init_r10();
    var_8 = init_state_0x8558();
    var_9 = init_state_0x8560();
    var_10 = init_r9();
    var_11 = init_r8();
    var_12 = init_state_0x8d58();
    var_13 = init_state_0x8549();
    var_14 = init_state_0x854c();
    var_15 = init_state_0x8548();
    var_16 = init_state_0x854b();
    var_17 = init_state_0x8547();
    var_18 = init_state_0x854d();
    *(uint64_t *)(var_0 + (-8L)) = var_5;
    *(uint64_t *)(var_0 + (-16L)) = var_4;
    *(uint64_t *)(var_0 + (-24L)) = var_3;
    var_19 = var_0 + (-32L);
    *(uint64_t *)(var_0 + (-48L)) = 4227174UL;
    var_20 = indirect_placeholder_15(var_19, rdi, 1UL, rsi);
    rcx_2 = 1UL;
    rax_0 = 0UL;
    var_21 = (uint64_t *)(rdi + 32UL);
    *var_21 = (*var_21 + (-1L));
    rax_0 = var_20;
    if (~(var_20 != 0UL & **(uint64_t **)(var_0 + (-40L)) != 0UL)) {
        return rax_0;
    }
    var_22 = (uint64_t *)(rdi + 24UL);
    var_23 = *var_22 + (-1L);
    *var_22 = var_23;
    if ((long)var_23 < (long)0UL) {
        var_27 = (var_23 >> 1UL) | (var_23 & 1UL);
        var_28 = helper_pxor_xmm_wrapper_93((struct type_5 *)(0UL), (struct type_7 *)(776UL), (struct type_7 *)(776UL), var_8, var_9);
        var_29 = var_28.field_1;
        var_30 = helper_cvtsq2ss_wrapper((struct type_5 *)(0UL), (struct type_7 *)(776UL), var_27, var_13, var_15, var_16, var_17);
        var_31 = helper_addss_wrapper((struct type_5 *)(0UL), (struct type_7 *)(776UL), (struct type_7 *)(776UL), var_30.field_0, var_30.field_1, var_14, var_15, var_16, var_17, var_18);
        state_0x8558_1 = var_31.field_0;
        state_0x8560_0 = var_29;
        storemerge3 = var_31.field_1;
    } else {
        var_24 = helper_pxor_xmm_wrapper_93((struct type_5 *)(0UL), (struct type_7 *)(776UL), (struct type_7 *)(776UL), var_8, var_9);
        var_25 = var_24.field_1;
        var_26 = helper_cvtsq2ss_wrapper((struct type_5 *)(0UL), (struct type_7 *)(776UL), var_23, var_13, var_15, var_16, var_17);
        state_0x8558_1 = var_26.field_0;
        state_0x8560_0 = var_25;
        storemerge3 = var_26.field_1;
    }
    var_32 = rdi + 40UL;
    var_33 = *(uint32_t **)var_32;
    var_34 = (uint64_t *)(rdi + 16UL);
    var_35 = *var_34;
    if ((long)var_35 < (long)0UL) {
        var_39 = (var_35 >> 1UL) | (var_35 & 1UL);
        var_40 = helper_pxor_xmm_wrapper((struct type_5 *)(0UL), (struct type_7 *)(840UL), (struct type_7 *)(840UL), var_1, var_2);
        var_41 = var_40.field_1;
        var_42 = helper_cvtsq2ss_wrapper_94((struct type_5 *)(0UL), (struct type_7 *)(840UL), var_39, storemerge3, var_15, var_16, var_17);
        var_43 = helper_addss_wrapper_95((struct type_5 *)(0UL), (struct type_7 *)(840UL), (struct type_7 *)(840UL), var_42.field_0, var_42.field_1, var_14, var_15, var_16, var_17, var_18);
        rcx_2 = var_39;
        state_0x85a0_0 = var_41;
        state_0x8598_1 = var_43.field_0;
        storemerge2 = var_43.field_1;
    } else {
        var_36 = helper_pxor_xmm_wrapper((struct type_5 *)(0UL), (struct type_7 *)(840UL), (struct type_7 *)(840UL), var_1, var_2);
        var_37 = var_36.field_1;
        var_38 = helper_cvtsq2ss_wrapper_94((struct type_5 *)(0UL), (struct type_7 *)(840UL), var_35, storemerge3, var_15, var_16, var_17);
        state_0x85a0_0 = var_37;
        state_0x8598_1 = var_38.field_0;
        storemerge2 = var_38.field_1;
    }
    var_44 = (uint64_t)*var_33;
    var_45 = var_12 & (-4294967296L);
    var_46 = helper_mulss_wrapper_98((struct type_5 *)(0UL), (struct type_7 *)(840UL), (struct type_7 *)(2824UL), state_0x8598_1, var_45 | var_44, storemerge2, var_14, var_15, var_16, var_17, var_18);
    var_47 = var_46.field_0;
    var_48 = helper_ucomiss_wrapper_99((struct type_5 *)(0UL), (struct type_7 *)(840UL), (struct type_7 *)(776UL), var_47, state_0x8558_1, var_46.field_1, var_14);
    var_49 = var_48.field_0;
    var_50 = var_48.field_1;
    rcx_0 = rcx_2;
    if ((var_49 & 65UL) == 0UL) {
        return rax_0;
    }
    *(uint64_t *)(var_0 + (-56L)) = 4227332UL;
    indirect_placeholder(var_20, rdi);
    var_51 = *(uint64_t *)var_32;
    var_52 = *var_34;
    if ((long)var_52 < (long)0UL) {
        var_54 = (var_52 >> 1UL) | (var_52 & 1UL);
        helper_pxor_xmm_wrapper_93((struct type_5 *)(0UL), (struct type_7 *)(776UL), (struct type_7 *)(776UL), state_0x8558_1, state_0x8560_0);
        var_55 = helper_cvtsq2ss_wrapper((struct type_5 *)(0UL), (struct type_7 *)(776UL), var_54, var_50, var_15, var_16, var_17);
        var_56 = helper_addss_wrapper((struct type_5 *)(0UL), (struct type_7 *)(776UL), (struct type_7 *)(776UL), var_55.field_0, var_55.field_1, var_14, var_15, var_16, var_17, var_18);
        rcx_0 = var_54;
        state_0x8558_0 = var_56.field_0;
        storemerge1 = var_56.field_1;
    } else {
        helper_pxor_xmm_wrapper_93((struct type_5 *)(0UL), (struct type_7 *)(776UL), (struct type_7 *)(776UL), state_0x8558_1, state_0x8560_0);
        var_53 = helper_cvtsq2ss_wrapper((struct type_5 *)(0UL), (struct type_7 *)(776UL), var_52, var_50, var_15, var_16, var_17);
        state_0x8558_0 = var_53.field_0;
        storemerge1 = var_53.field_1;
    }
    var_57 = *var_22;
    rcx_1 = rcx_0;
    if ((long)var_57 < (long)0UL) {
        var_59 = (var_57 >> 1UL) | (var_57 & 1UL);
        helper_pxor_xmm_wrapper((struct type_5 *)(0UL), (struct type_7 *)(840UL), (struct type_7 *)(840UL), var_47, state_0x85a0_0);
        var_60 = helper_cvtsq2ss_wrapper_94((struct type_5 *)(0UL), (struct type_7 *)(840UL), var_59, storemerge1, var_15, var_16, var_17);
        var_61 = helper_addss_wrapper_95((struct type_5 *)(0UL), (struct type_7 *)(840UL), (struct type_7 *)(840UL), var_60.field_0, var_60.field_1, var_14, var_15, var_16, var_17, var_18);
        rcx_1 = var_59;
        state_0x8598_0 = var_61.field_0;
        storemerge = var_61.field_1;
    } else {
        helper_pxor_xmm_wrapper((struct type_5 *)(0UL), (struct type_7 *)(840UL), (struct type_7 *)(840UL), var_47, state_0x85a0_0);
        var_58 = helper_cvtsq2ss_wrapper_94((struct type_5 *)(0UL), (struct type_7 *)(840UL), var_57, storemerge1, var_15, var_16, var_17);
        state_0x8598_0 = var_58.field_0;
        storemerge = var_58.field_1;
    }
    var_62 = helper_mulss_wrapper((struct type_5 *)(0UL), (struct type_7 *)(904UL), (struct type_7 *)(2824UL), var_45 | (uint64_t)*(uint32_t *)var_51, state_0x8558_0, storemerge, var_14, var_15, var_16, var_17, var_18);
    var_63 = helper_ucomiss_wrapper((struct type_5 *)(0UL), (struct type_7 *)(904UL), (struct type_7 *)(840UL), state_0x8598_0, var_62.field_0, var_62.field_1, var_14);
    var_64 = var_63.field_1;
    if ((var_63.field_0 & 65UL) != 0UL) {
        var_65 = *(unsigned char *)(var_51 + 16UL);
        var_66 = (uint64_t)var_65;
        var_67 = (var_65 == '\x00');
        var_68 = helper_mulss_wrapper_96((struct type_5 *)(0UL), (struct type_7 *)(776UL), (struct type_7 *)(2824UL), state_0x8558_0, var_45 | (uint64_t)*(uint32_t *)(var_51 + 4UL), var_64, var_14, var_15, var_16, var_17, var_18);
        var_69 = var_68.field_0;
        var_70 = var_68.field_1;
        if (var_67) {
            var_78 = helper_mulss_wrapper_96((struct type_5 *)(0UL), (struct type_7 *)(776UL), (struct type_7 *)(2824UL), var_69, var_45 | (uint64_t)*(uint32_t *)(var_51 + 8UL), var_70, var_14, var_15, var_16, var_17, var_18);
            var_79 = var_78.field_0;
            var_80 = helper_ucomiss_wrapper_97((struct type_5 *)(0UL), (struct type_7 *)(776UL), (struct type_7 *)(2824UL), var_79, var_45 | (uint64_t)*(uint32_t *)4273276UL, var_78.field_1, var_14);
            var_81 = var_80.field_0;
            var_82 = var_80.field_1;
            var_83 = helper_cc_compute_c_wrapper(var_66, var_81, var_6, 1U);
            if (var_83 == 0UL) {
                var_85 = helper_subss_wrapper((struct type_5 *)(0UL), (struct type_7 *)(776UL), (struct type_7 *)(2824UL), var_79, var_45 | (uint64_t)*(uint32_t *)4273276UL, var_82, var_14, var_15, var_16, var_17, var_18);
                var_86 = helper_cvttss2sq_wrapper((struct type_5 *)(0UL), (struct type_7 *)(776UL), var_85.field_0, var_85.field_1, var_14);
                rsi2_0 = var_86.field_0 ^ (-9223372036854775808L);
            } else {
                var_84 = helper_cvttss2sq_wrapper((struct type_5 *)(0UL), (struct type_7 *)(776UL), var_79, var_82, var_14);
                rsi2_0 = var_84.field_0;
            }
        } else {
            var_71 = helper_ucomiss_wrapper_97((struct type_5 *)(0UL), (struct type_7 *)(776UL), (struct type_7 *)(2824UL), var_69, var_45 | (uint64_t)*(uint32_t *)4273276UL, var_70, var_14);
            var_72 = var_71.field_0;
            var_73 = var_71.field_1;
            var_74 = helper_cc_compute_c_wrapper(var_66, var_72, var_6, 1U);
            if (var_74 == 0UL) {
                var_76 = helper_subss_wrapper((struct type_5 *)(0UL), (struct type_7 *)(776UL), (struct type_7 *)(2824UL), var_69, var_45 | (uint64_t)*(uint32_t *)4273276UL, var_73, var_14, var_15, var_16, var_17, var_18);
                var_77 = helper_cvttss2sq_wrapper((struct type_5 *)(0UL), (struct type_7 *)(776UL), var_76.field_0, var_76.field_1, var_14);
                rsi2_0 = var_77.field_0 ^ (-9223372036854775808L);
            } else {
                var_75 = helper_cvttss2sq_wrapper((struct type_5 *)(0UL), (struct type_7 *)(776UL), var_69, var_73, var_14);
                rsi2_0 = var_75.field_0;
            }
        }
        var_87 = var_0 + (-64L);
        *(uint64_t *)var_87 = 4227560UL;
        var_88 = indirect_placeholder_2(rdi, rcx_1, var_7, rsi2_0, var_10, var_11);
        local_sp_0 = var_87;
        if ((uint64_t)(unsigned char)var_88 != 0UL) {
            var_89 = (uint64_t *)(rdi + 72UL);
            var_90 = *var_89;
            rdi1_0 = var_90;
            if (var_90 != 0UL) {
                var_91 = *(uint64_t *)(rdi1_0 + 8UL);
                var_92 = local_sp_0 + (-8L);
                *(uint64_t *)var_92 = 4227587UL;
                indirect_placeholder_1();
                rdi1_0 = var_91;
                local_sp_0 = var_92;
                do {
                    var_91 = *(uint64_t *)(rdi1_0 + 8UL);
                    var_92 = local_sp_0 + (-8L);
                    *(uint64_t *)var_92 = 4227587UL;
                    indirect_placeholder_1();
                    rdi1_0 = var_91;
                    local_sp_0 = var_92;
                } while (var_91 != 0UL);
            }
            *var_89 = 0UL;
        }
    }
    return rax_0;
}

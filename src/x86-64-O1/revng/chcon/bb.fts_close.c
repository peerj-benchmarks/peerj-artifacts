typedef unsigned char __u_char;
typedef unsigned short __u_short;
typedef unsigned int __u_int;
typedef unsigned long __u_long;
typedef signed char __int8_t;
typedef unsigned char __uint8_t;
typedef short __int16_t;
typedef unsigned short __uint16_t;
typedef int __int32_t;
typedef unsigned int __uint32_t;
typedef long __int64_t;
typedef unsigned long __uint64_t;
typedef __int8_t __int_least8_t;
typedef __uint8_t __uint_least8_t;
typedef __int16_t __int_least16_t;
typedef __uint16_t __uint_least16_t;
typedef __int32_t __int_least32_t;
typedef __uint32_t __uint_least32_t;
typedef __int64_t __int_least64_t;
typedef __uint64_t __uint_least64_t;
typedef long __quad_t;
typedef unsigned long __u_quad_t;
typedef long __intmax_t;
typedef unsigned long __uintmax_t;
typedef unsigned long __dev_t;
typedef unsigned int __uid_t;
typedef unsigned int __gid_t;
typedef unsigned long __ino_t;
typedef unsigned long __ino64_t;
typedef unsigned int __mode_t;
typedef unsigned long __nlink_t;
typedef long __off_t;
typedef long __off64_t;
typedef int __pid_t;
typedef struct {
    int __val[2];
} __fsid_t;
typedef long __clock_t;
typedef unsigned long __rlim_t;
typedef unsigned long __rlim64_t;
typedef unsigned int __id_t;
typedef long __time_t;
typedef unsigned int __useconds_t;
typedef long __suseconds_t;
typedef int __daddr_t;
typedef int __key_t;
typedef int __clockid_t;
typedef void *__timer_t;
typedef long __blksize_t;
typedef long __blkcnt_t;
typedef long __blkcnt64_t;
typedef unsigned long __fsblkcnt_t;
typedef unsigned long __fsblkcnt64_t;
typedef unsigned long __fsfilcnt_t;
typedef unsigned long __fsfilcnt64_t;
typedef long __fsword_t;
typedef long __ssize_t;
typedef long __syscall_slong_t;
typedef unsigned long __syscall_ulong_t;
typedef __off64_t __loff_t;
typedef char *__caddr_t;
typedef long __intptr_t;
typedef unsigned int __socklen_t;
typedef int __sig_atomic_t;
typedef __int8_t int8_t;
typedef __int16_t int16_t;
typedef __int32_t int32_t;
typedef __int64_t int64_t;
typedef __uint8_t uint8_t;
typedef __uint16_t uint16_t;
typedef __uint32_t uint32_t;
typedef __uint64_t uint64_t;
typedef __int_least8_t int_least8_t;
typedef __int_least16_t int_least16_t;
typedef __int_least32_t int_least32_t;
typedef __int_least64_t int_least64_t;
typedef __uint_least8_t uint_least8_t;
typedef __uint_least16_t uint_least16_t;
typedef __uint_least32_t uint_least32_t;
typedef __uint_least64_t uint_least64_t;
typedef signed char int_fast8_t;
typedef long int_fast16_t;
typedef long int_fast32_t;
typedef long int_fast64_t;
typedef unsigned char uint_fast8_t;
typedef unsigned long uint_fast16_t;
typedef unsigned long uint_fast32_t;
typedef unsigned long uint_fast64_t;
typedef long intptr_t;
typedef unsigned long uintptr_t;
typedef __intmax_t intmax_t;
typedef __uintmax_t uintmax_t;
typedef _Bool bool;
extern uint64_t revng_init_local_sp(uint64_t param_0);
extern uint64_t init_rbx(void);
extern void indirect_placeholder_1(void);
extern uint64_t init_rbp(void);
extern void indirect_placeholder_6(uint64_t param_0);
uint64_t bb_fts_close(uint64_t rdi) {
    uint64_t var_0;
    uint64_t var_1;
    uint64_t var_2;
    uint64_t var_3;
    uint64_t var_4;
    uint64_t local_sp_7;
    uint64_t rbx_1;
    uint64_t local_sp_1;
    uint64_t rax_0;
    uint64_t local_sp_4;
    uint64_t local_sp_0;
    uint64_t rbx_0;
    uint64_t rdi1_0;
    uint64_t var_28;
    uint32_t var_29;
    uint64_t rbx_2;
    uint64_t var_23;
    uint64_t var_24;
    uint64_t var_21;
    uint64_t local_sp_2;
    uint64_t var_22;
    uint64_t local_sp_3;
    uint64_t rbx_3;
    uint64_t var_25;
    uint64_t var_26;
    uint64_t var_27;
    uint64_t var_14;
    uint64_t var_15;
    uint64_t var_13;
    uint64_t local_sp_6;
    uint64_t var_19;
    bool var_20;
    uint64_t var_16;
    uint64_t var_17;
    uint64_t var_18;
    uint64_t rbx_5;
    uint64_t local_sp_5;
    uint64_t var_7;
    uint64_t rdi1_1;
    uint64_t var_5;
    uint64_t var_6;
    uint64_t var_8;
    uint64_t local_sp_8;
    uint64_t var_9;
    uint64_t var_10;
    uint32_t var_11;
    uint64_t var_12;
    var_0 = revng_init_local_sp(0UL);
    var_1 = init_rbx();
    var_2 = init_rbp();
    *(uint64_t *)(var_0 + (-8L)) = var_2;
    *(uint64_t *)(var_0 + (-16L)) = var_1;
    var_3 = var_0 + (-24L);
    var_4 = *(uint64_t *)rdi;
    local_sp_7 = var_3;
    rax_0 = 0UL;
    rbx_0 = 0UL;
    rbx_2 = 0UL;
    local_sp_6 = var_3;
    local_sp_5 = var_3;
    rdi1_1 = var_4;
    if (var_4 != 0UL) {
        if ((long)*(uint64_t *)(var_4 + 88UL) <= (long)18446744073709551615UL) {
            var_6 = local_sp_6 + (-8L);
            *(uint64_t *)var_6 = 4217828UL;
            indirect_placeholder_1();
            local_sp_5 = var_6;
            local_sp_6 = var_6;
            rdi1_1 = rbx_5;
            do {
                var_5 = *(uint64_t *)(rdi1_1 + 16UL);
                rbx_5 = var_5;
                if (var_5 == 0UL) {
                    rbx_5 = *(uint64_t *)(rdi1_1 + 8UL);
                }
                var_6 = local_sp_6 + (-8L);
                *(uint64_t *)var_6 = 4217828UL;
                indirect_placeholder_1();
                local_sp_5 = var_6;
                local_sp_6 = var_6;
                rdi1_1 = rbx_5;
            } while ((long)*(uint64_t *)(rbx_5 + 88UL) <= (long)18446744073709551615UL);
        }
        var_7 = local_sp_5 + (-8L);
        *(uint64_t *)var_7 = 4217848UL;
        indirect_placeholder_1();
        local_sp_7 = var_7;
    }
    var_8 = *(uint64_t *)(rdi + 8UL);
    local_sp_8 = local_sp_7;
    if (var_8 == 0UL) {
        var_9 = local_sp_7 + (-8L);
        *(uint64_t *)var_9 = 4217862UL;
        indirect_placeholder_6(var_8);
        local_sp_8 = var_9;
    }
    *(uint64_t *)(local_sp_8 + (-8L)) = 4217871UL;
    indirect_placeholder_1();
    var_10 = local_sp_8 + (-16L);
    *(uint64_t *)var_10 = 4217880UL;
    indirect_placeholder_1();
    var_11 = *(uint32_t *)(rdi + 72UL);
    var_12 = (uint64_t)var_11;
    local_sp_4 = var_10;
    if ((uint64_t)((uint16_t)var_12 & (unsigned short)512U) != 0UL) {
        if ((var_12 & 4UL) == 0UL) {
            var_19 = local_sp_8 + (-24L);
            *(uint64_t *)var_19 = 4217937UL;
            indirect_placeholder_1();
            var_20 = (var_11 == 0U);
            local_sp_2 = var_19;
            if (var_20) {
                var_21 = local_sp_8 + (-32L);
                *(uint64_t *)var_21 = 4217951UL;
                indirect_placeholder_1();
                local_sp_2 = var_21;
                rbx_2 = (uint64_t)*(uint32_t *)var_12;
            }
            var_22 = local_sp_2 + (-8L);
            *(uint64_t *)var_22 = 4217961UL;
            indirect_placeholder_1();
            local_sp_3 = var_22;
            rbx_3 = rbx_2;
            if ((var_20 ^ 1) && (rbx_2 == 0UL)) {
                var_23 = local_sp_2 + (-16L);
                *(uint64_t *)var_23 = 4217974UL;
                indirect_placeholder_1();
                var_24 = (uint64_t)*(uint32_t *)var_12;
                local_sp_3 = var_23;
                rbx_3 = var_24;
            }
            var_25 = rdi + 96UL;
            var_26 = local_sp_3 + (-8L);
            *(uint64_t *)var_26 = 4217985UL;
            indirect_placeholder_6(var_25);
            var_27 = *(uint64_t *)(rdi + 80UL);
            rbx_1 = rbx_3;
            local_sp_1 = var_26;
            local_sp_0 = var_26;
            rbx_0 = rbx_3;
            rdi1_0 = var_27;
            if (var_27 == 0UL) {
                var_28 = local_sp_0 + (-8L);
                *(uint64_t *)var_28 = 4217999UL;
                indirect_placeholder_6(rdi1_0);
                local_sp_1 = var_28;
                rbx_1 = rbx_0;
            }
        } else {
            var_16 = rdi + 96UL;
            var_17 = local_sp_4 + (-8L);
            *(uint64_t *)var_17 = 4218072UL;
            indirect_placeholder_6(var_16);
            var_18 = *(uint64_t *)(rdi + 80UL);
            local_sp_0 = var_17;
            rdi1_0 = var_18;
            if (var_18 != 0UL) {
                *(uint64_t *)(local_sp_4 + (-16L)) = 4218048UL;
                indirect_placeholder_6(rdi);
                *(uint64_t *)(local_sp_4 + (-24L)) = 4218056UL;
                indirect_placeholder_1();
                return rax_0;
            }
            var_28 = local_sp_0 + (-8L);
            *(uint64_t *)var_28 = 4217999UL;
            indirect_placeholder_6(rdi1_0);
            local_sp_1 = var_28;
            rbx_1 = rbx_0;
        }
        *(uint64_t *)(local_sp_1 + (-8L)) = 4218007UL;
        indirect_placeholder_6(rdi);
        *(uint64_t *)(local_sp_1 + (-16L)) = 4218015UL;
        indirect_placeholder_1();
        var_29 = (uint32_t)rbx_1;
        if ((uint64_t)var_29 != 0UL) {
            *(uint64_t *)(local_sp_1 + (-24L)) = 4218024UL;
            indirect_placeholder_1();
            *(uint32_t *)var_12 = var_29;
            rax_0 = 4294967295UL;
        }
        return rax_0;
    }
    if ((int)*(uint32_t *)(rdi + 44UL) >= (int)0U) {
        var_16 = rdi + 96UL;
        var_17 = local_sp_4 + (-8L);
        *(uint64_t *)var_17 = 4218072UL;
        indirect_placeholder_6(var_16);
        var_18 = *(uint64_t *)(rdi + 80UL);
        local_sp_0 = var_17;
        rdi1_0 = var_18;
        if (var_18 != 0UL) {
            *(uint64_t *)(local_sp_4 + (-16L)) = 4218048UL;
            indirect_placeholder_6(rdi);
            *(uint64_t *)(local_sp_4 + (-24L)) = 4218056UL;
            indirect_placeholder_1();
            return rax_0;
        }
        var_28 = local_sp_0 + (-8L);
        *(uint64_t *)var_28 = 4217999UL;
        indirect_placeholder_6(rdi1_0);
        local_sp_1 = var_28;
        rbx_1 = rbx_0;
        *(uint64_t *)(local_sp_1 + (-8L)) = 4218007UL;
        indirect_placeholder_6(rdi);
        *(uint64_t *)(local_sp_1 + (-16L)) = 4218015UL;
        indirect_placeholder_1();
        var_29 = (uint32_t)rbx_1;
        if ((uint64_t)var_29 != 0UL) {
            *(uint64_t *)(local_sp_1 + (-24L)) = 4218024UL;
            indirect_placeholder_1();
            *(uint32_t *)var_12 = var_29;
            rax_0 = 4294967295UL;
        }
        return rax_0;
    }
    var_13 = local_sp_8 + (-24L);
    *(uint64_t *)var_13 = 4217904UL;
    indirect_placeholder_1();
    local_sp_4 = var_13;
    if (var_11 == 0U) {
        var_16 = rdi + 96UL;
        var_17 = local_sp_4 + (-8L);
        *(uint64_t *)var_17 = 4218072UL;
        indirect_placeholder_6(var_16);
        var_18 = *(uint64_t *)(rdi + 80UL);
        local_sp_0 = var_17;
        rdi1_0 = var_18;
        if (var_18 != 0UL) {
            *(uint64_t *)(local_sp_4 + (-16L)) = 4218048UL;
            indirect_placeholder_6(rdi);
            *(uint64_t *)(local_sp_4 + (-24L)) = 4218056UL;
            indirect_placeholder_1();
            return rax_0;
        }
        var_28 = local_sp_0 + (-8L);
        *(uint64_t *)var_28 = 4217999UL;
        indirect_placeholder_6(rdi1_0);
        local_sp_1 = var_28;
        rbx_1 = rbx_0;
    } else {
        var_14 = local_sp_8 + (-32L);
        *(uint64_t *)var_14 = 4217917UL;
        indirect_placeholder_1();
        var_15 = (uint64_t)*(uint32_t *)var_12;
        local_sp_3 = var_14;
        rbx_3 = var_15;
        var_25 = rdi + 96UL;
        var_26 = local_sp_3 + (-8L);
        *(uint64_t *)var_26 = 4217985UL;
        indirect_placeholder_6(var_25);
        var_27 = *(uint64_t *)(rdi + 80UL);
        rbx_1 = rbx_3;
        local_sp_1 = var_26;
        local_sp_0 = var_26;
        rbx_0 = rbx_3;
        rdi1_0 = var_27;
        if (var_27 == 0UL) {
            var_28 = local_sp_0 + (-8L);
            *(uint64_t *)var_28 = 4217999UL;
            indirect_placeholder_6(rdi1_0);
            local_sp_1 = var_28;
            rbx_1 = rbx_0;
        }
    }
    *(uint64_t *)(local_sp_1 + (-8L)) = 4218007UL;
    indirect_placeholder_6(rdi);
    *(uint64_t *)(local_sp_1 + (-16L)) = 4218015UL;
    indirect_placeholder_1();
    var_29 = (uint32_t)rbx_1;
    if ((uint64_t)var_29 == 0UL) {
        return rax_0;
    }
    *(uint64_t *)(local_sp_1 + (-24L)) = 4218024UL;
    indirect_placeholder_1();
    *(uint32_t *)var_12 = var_29;
    rax_0 = 4294967295UL;
    return rax_0;
}

typedef unsigned char __u_char;
typedef unsigned short __u_short;
typedef unsigned int __u_int;
typedef unsigned long __u_long;
typedef signed char __int8_t;
typedef unsigned char __uint8_t;
typedef short __int16_t;
typedef unsigned short __uint16_t;
typedef int __int32_t;
typedef unsigned int __uint32_t;
typedef long __int64_t;
typedef unsigned long __uint64_t;
typedef __int8_t __int_least8_t;
typedef __uint8_t __uint_least8_t;
typedef __int16_t __int_least16_t;
typedef __uint16_t __uint_least16_t;
typedef __int32_t __int_least32_t;
typedef __uint32_t __uint_least32_t;
typedef __int64_t __int_least64_t;
typedef __uint64_t __uint_least64_t;
typedef long __quad_t;
typedef unsigned long __u_quad_t;
typedef long __intmax_t;
typedef unsigned long __uintmax_t;
typedef unsigned long __dev_t;
typedef unsigned int __uid_t;
typedef unsigned int __gid_t;
typedef unsigned long __ino_t;
typedef unsigned long __ino64_t;
typedef unsigned int __mode_t;
typedef unsigned long __nlink_t;
typedef long __off_t;
typedef long __off64_t;
typedef int __pid_t;
typedef struct {
    int __val[2];
} __fsid_t;
typedef long __clock_t;
typedef unsigned long __rlim_t;
typedef unsigned long __rlim64_t;
typedef unsigned int __id_t;
typedef long __time_t;
typedef unsigned int __useconds_t;
typedef long __suseconds_t;
typedef int __daddr_t;
typedef int __key_t;
typedef int __clockid_t;
typedef void *__timer_t;
typedef long __blksize_t;
typedef long __blkcnt_t;
typedef long __blkcnt64_t;
typedef unsigned long __fsblkcnt_t;
typedef unsigned long __fsblkcnt64_t;
typedef unsigned long __fsfilcnt_t;
typedef unsigned long __fsfilcnt64_t;
typedef long __fsword_t;
typedef long __ssize_t;
typedef long __syscall_slong_t;
typedef unsigned long __syscall_ulong_t;
typedef __off64_t __loff_t;
typedef char *__caddr_t;
typedef long __intptr_t;
typedef unsigned int __socklen_t;
typedef int __sig_atomic_t;
typedef __int8_t int8_t;
typedef __int16_t int16_t;
typedef __int32_t int32_t;
typedef __int64_t int64_t;
typedef __uint8_t uint8_t;
typedef __uint16_t uint16_t;
typedef __uint32_t uint32_t;
typedef __uint64_t uint64_t;
typedef __int_least8_t int_least8_t;
typedef __int_least16_t int_least16_t;
typedef __int_least32_t int_least32_t;
typedef __int_least64_t int_least64_t;
typedef __uint_least8_t uint_least8_t;
typedef __uint_least16_t uint_least16_t;
typedef __uint_least32_t uint_least32_t;
typedef __uint_least64_t uint_least64_t;
typedef signed char int_fast8_t;
typedef long int_fast16_t;
typedef long int_fast32_t;
typedef long int_fast64_t;
typedef unsigned char uint_fast8_t;
typedef unsigned long uint_fast16_t;
typedef unsigned long uint_fast32_t;
typedef unsigned long uint_fast64_t;
typedef long intptr_t;
typedef unsigned long uintptr_t;
typedef __intmax_t intmax_t;
typedef __uintmax_t uintmax_t;
typedef _Bool bool;
struct helper_pxor_xmm_wrapper_ret_type;
struct type_5;
struct type_7;
struct helper_pxor_xmm_wrapper_93_ret_type;
struct helper_cvtsq2ss_wrapper_ret_type;
struct helper_addss_wrapper_ret_type;
struct helper_mulss_wrapper_96_ret_type;
struct helper_ucomiss_wrapper_97_ret_type;
struct helper_cvttss2sq_wrapper_ret_type;
struct helper_subss_wrapper_ret_type;
struct helper_mulss_wrapper_128_ret_type;
struct helper_ucomiss_wrapper_99_ret_type;
struct helper_cvtsq2ss_wrapper_94_ret_type;
struct helper_addss_wrapper_125_ret_type;
struct helper_addss_wrapper_95_ret_type;
struct helper_mulss_wrapper_126_ret_type;
struct helper_ucomiss_wrapper_127_ret_type;
struct helper_pxor_xmm_wrapper_ret_type {
    uint64_t field_0;
    uint64_t field_1;
};
struct type_5 {
};
struct type_7 {
};
struct helper_pxor_xmm_wrapper_93_ret_type {
    uint64_t field_0;
    uint64_t field_1;
};
struct helper_cvtsq2ss_wrapper_ret_type {
    uint64_t field_0;
    unsigned char field_1;
};
struct helper_addss_wrapper_ret_type {
    uint64_t field_0;
    unsigned char field_1;
};
struct helper_mulss_wrapper_96_ret_type {
    uint64_t field_0;
    unsigned char field_1;
};
struct helper_ucomiss_wrapper_97_ret_type {
    uint64_t field_0;
    unsigned char field_1;
};
struct helper_cvttss2sq_wrapper_ret_type {
    uint64_t field_0;
    unsigned char field_1;
};
struct helper_subss_wrapper_ret_type {
    uint64_t field_0;
    unsigned char field_1;
};
struct helper_mulss_wrapper_128_ret_type {
    uint64_t field_0;
    unsigned char field_1;
};
struct helper_ucomiss_wrapper_99_ret_type {
    uint64_t field_0;
    unsigned char field_1;
};
struct helper_cvtsq2ss_wrapper_94_ret_type {
    uint64_t field_0;
    unsigned char field_1;
};
struct helper_addss_wrapper_125_ret_type {
    uint64_t field_0;
    unsigned char field_1;
};
struct helper_addss_wrapper_95_ret_type {
    uint64_t field_0;
    unsigned char field_1;
};
struct helper_mulss_wrapper_126_ret_type {
    uint64_t field_0;
    unsigned char field_1;
};
struct helper_ucomiss_wrapper_127_ret_type {
    uint64_t field_0;
    unsigned char field_1;
};
extern uint64_t revng_init_local_sp(uint64_t param_0);
extern uint64_t init_rbx(void);
extern uint64_t init_state_0x8558(void);
extern unsigned char init_state_0x8549(void);
extern unsigned char init_state_0x854c(void);
extern unsigned char init_state_0x8548(void);
extern unsigned char init_state_0x854b(void);
extern unsigned char init_state_0x8547(void);
extern unsigned char init_state_0x854d(void);
extern void indirect_placeholder_1(void);
extern uint64_t init_rbp(void);
extern uint64_t init_cc_src2(void);
extern uint64_t helper_cc_compute_c_wrapper(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint32_t param_3);
extern uint64_t init_state_0x8560(void);
extern struct helper_pxor_xmm_wrapper_ret_type helper_pxor_xmm_wrapper(struct type_5 *param_0, struct type_7 *param_1, struct type_7 *param_2, uint64_t param_3, uint64_t param_4);
extern uint64_t init_state_0x8598(void);
extern uint64_t init_state_0x85a0(void);
extern uint64_t init_r12(void);
extern uint64_t indirect_placeholder_3(uint64_t param_0);
extern struct helper_pxor_xmm_wrapper_93_ret_type helper_pxor_xmm_wrapper_93(struct type_5 *param_0, struct type_7 *param_1, struct type_7 *param_2, uint64_t param_3, uint64_t param_4);
extern uint64_t init_state_0x8d58(void);
extern struct helper_cvtsq2ss_wrapper_ret_type helper_cvtsq2ss_wrapper(struct type_5 *param_0, struct type_7 *param_1, uint64_t param_2, unsigned char param_3, unsigned char param_4, unsigned char param_5, unsigned char param_6);
extern struct helper_addss_wrapper_ret_type helper_addss_wrapper(struct type_5 *param_0, struct type_7 *param_1, struct type_7 *param_2, uint64_t param_3, unsigned char param_4, unsigned char param_5, unsigned char param_6, unsigned char param_7, unsigned char param_8, unsigned char param_9);
extern struct helper_mulss_wrapper_96_ret_type helper_mulss_wrapper_96(struct type_5 *param_0, struct type_7 *param_1, struct type_7 *param_2, uint64_t param_3, uint64_t param_4, unsigned char param_5, unsigned char param_6, unsigned char param_7, unsigned char param_8, unsigned char param_9, unsigned char param_10);
extern struct helper_ucomiss_wrapper_97_ret_type helper_ucomiss_wrapper_97(struct type_5 *param_0, struct type_7 *param_1, struct type_7 *param_2, uint64_t param_3, uint64_t param_4, unsigned char param_5, unsigned char param_6);
extern struct helper_cvttss2sq_wrapper_ret_type helper_cvttss2sq_wrapper(struct type_5 *param_0, struct type_7 *param_1, uint64_t param_2, unsigned char param_3, unsigned char param_4);
extern struct helper_subss_wrapper_ret_type helper_subss_wrapper(struct type_5 *param_0, struct type_7 *param_1, struct type_7 *param_2, uint64_t param_3, uint64_t param_4, unsigned char param_5, unsigned char param_6, unsigned char param_7, unsigned char param_8, unsigned char param_9, unsigned char param_10);
extern struct helper_mulss_wrapper_128_ret_type helper_mulss_wrapper_128(struct type_5 *param_0, struct type_7 *param_1, struct type_7 *param_2, uint64_t param_3, uint64_t param_4, unsigned char param_5, unsigned char param_6, unsigned char param_7, unsigned char param_8, unsigned char param_9, unsigned char param_10);
extern uint64_t indirect_placeholder_15(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint64_t param_3);
extern uint64_t init_r9(void);
extern struct helper_ucomiss_wrapper_99_ret_type helper_ucomiss_wrapper_99(struct type_5 *param_0, struct type_7 *param_1, struct type_7 *param_2, uint64_t param_3, uint64_t param_4, unsigned char param_5, unsigned char param_6);
extern uint64_t init_r10(void);
extern uint64_t init_r8(void);
extern struct helper_cvtsq2ss_wrapper_94_ret_type helper_cvtsq2ss_wrapper_94(struct type_5 *param_0, struct type_7 *param_1, uint64_t param_2, unsigned char param_3, unsigned char param_4, unsigned char param_5, unsigned char param_6);
extern struct helper_addss_wrapper_125_ret_type helper_addss_wrapper_125(struct type_5 *param_0, struct type_7 *param_1, struct type_7 *param_2, uint64_t param_3, uint64_t param_4, unsigned char param_5, unsigned char param_6, unsigned char param_7, unsigned char param_8, unsigned char param_9, unsigned char param_10);
extern struct helper_addss_wrapper_95_ret_type helper_addss_wrapper_95(struct type_5 *param_0, struct type_7 *param_1, struct type_7 *param_2, uint64_t param_3, unsigned char param_4, unsigned char param_5, unsigned char param_6, unsigned char param_7, unsigned char param_8, unsigned char param_9);
extern struct helper_mulss_wrapper_126_ret_type helper_mulss_wrapper_126(struct type_5 *param_0, struct type_7 *param_1, struct type_7 *param_2, uint64_t param_3, uint64_t param_4, unsigned char param_5, unsigned char param_6, unsigned char param_7, unsigned char param_8, unsigned char param_9, unsigned char param_10);
extern struct helper_ucomiss_wrapper_127_ret_type helper_ucomiss_wrapper_127(struct type_5 *param_0, struct type_7 *param_1, struct type_7 *param_2, uint64_t param_3, uint64_t param_4, unsigned char param_5, unsigned char param_6);
extern uint64_t indirect_placeholder_2(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint64_t param_3, uint64_t param_4, uint64_t param_5);
uint64_t bb_hash_insert_if_absent(uint64_t rdx, uint64_t rdi, uint64_t rsi) {
    struct helper_mulss_wrapper_128_ret_type var_78;
    unsigned char storemerge4;
    uint64_t var_79;
    uint64_t var_80;
    struct helper_ucomiss_wrapper_97_ret_type var_81;
    uint64_t var_82;
    struct helper_pxor_xmm_wrapper_ret_type var_25;
    struct helper_cvttss2sq_wrapper_ret_type var_89;
    struct helper_cvtsq2ss_wrapper_ret_type var_56;
    struct helper_cvttss2sq_wrapper_ret_type var_87;
    struct helper_pxor_xmm_wrapper_ret_type var_58;
    uint64_t var_0;
    uint64_t var_1;
    uint64_t var_2;
    uint64_t var_3;
    uint64_t var_4;
    uint64_t var_5;
    uint64_t var_6;
    uint64_t var_7;
    uint64_t var_8;
    uint64_t var_9;
    uint64_t var_10;
    uint64_t var_11;
    uint64_t var_12;
    unsigned char var_13;
    unsigned char var_14;
    unsigned char var_15;
    unsigned char var_16;
    unsigned char var_17;
    unsigned char var_18;
    uint64_t local_sp_2;
    uint64_t var_93;
    uint64_t local_sp_0;
    uint64_t var_91;
    uint64_t var_92;
    unsigned char storemerge1;
    uint64_t state_0x8558_2;
    unsigned char storemerge;
    uint64_t var_57;
    uint64_t state_0x8598_2;
    uint64_t var_59;
    struct helper_cvtsq2ss_wrapper_94_ret_type var_60;
    uint64_t var_61;
    struct helper_addss_wrapper_125_ret_type var_62;
    uint64_t rcx_0;
    uint64_t state_0x8558_1;
    uint64_t state_0x8558_0;
    uint64_t state_0x85a0_0;
    uint64_t state_0x8598_0;
    unsigned char storemerge2;
    uint64_t var_63;
    struct helper_cvtsq2ss_wrapper_94_ret_type var_64;
    uint64_t rcx_1;
    uint64_t var_65;
    struct helper_cvtsq2ss_wrapper_94_ret_type var_66;
    struct helper_addss_wrapper_95_ret_type var_67;
    uint64_t state_0x8598_1;
    unsigned char storemerge3;
    struct helper_mulss_wrapper_126_ret_type var_68;
    struct helper_ucomiss_wrapper_127_ret_type var_69;
    unsigned char var_70;
    unsigned char var_71;
    uint64_t var_72;
    bool var_73;
    uint64_t var_74;
    struct helper_mulss_wrapper_96_ret_type var_75;
    uint64_t var_76;
    unsigned char var_77;
    uint64_t rax_0;
    struct helper_ucomiss_wrapper_97_ret_type var_83;
    uint64_t var_84;
    unsigned char var_85;
    uint64_t var_86;
    uint64_t rsi3_0;
    struct helper_subss_wrapper_ret_type var_88;
    uint64_t var_90;
    uint64_t *var_23;
    uint64_t var_24;
    uint64_t var_26;
    struct helper_cvtsq2ss_wrapper_94_ret_type var_27;
    uint64_t state_0x85a0_1;
    uint64_t var_28;
    struct helper_pxor_xmm_wrapper_ret_type var_29;
    uint64_t var_30;
    struct helper_cvtsq2ss_wrapper_94_ret_type var_31;
    struct helper_addss_wrapper_95_ret_type var_32;
    uint64_t *var_33;
    uint64_t var_34;
    uint64_t *var_35;
    uint64_t var_36;
    struct helper_pxor_xmm_wrapper_93_ret_type var_37;
    uint64_t var_38;
    struct helper_cvtsq2ss_wrapper_ret_type var_39;
    uint64_t rcx_2;
    uint64_t var_40;
    struct helper_pxor_xmm_wrapper_93_ret_type var_41;
    uint64_t var_42;
    struct helper_cvtsq2ss_wrapper_ret_type var_43;
    struct helper_addss_wrapper_ret_type var_44;
    uint64_t state_0x8560_0;
    uint64_t var_45;
    uint64_t var_46;
    struct helper_mulss_wrapper_96_ret_type var_47;
    uint64_t var_48;
    struct helper_ucomiss_wrapper_99_ret_type var_49;
    uint64_t var_50;
    unsigned char var_51;
    uint64_t var_52;
    uint64_t var_53;
    uint64_t var_54;
    uint64_t var_55;
    uint64_t *var_94;
    uint64_t *_cast;
    uint64_t *var_95;
    uint64_t var_96;
    uint64_t *var_97;
    uint64_t *_pre_phi190;
    uint64_t *_pre_phi186;
    uint64_t rdx1_0;
    uint64_t *var_100;
    uint64_t *var_101;
    uint64_t var_98;
    uint64_t var_99;
    uint64_t *var_102;
    uint64_t var_19;
    uint64_t var_20;
    uint64_t var_21;
    uint64_t var_22;
    var_0 = revng_init_local_sp(0UL);
    var_1 = init_state_0x8598();
    var_2 = init_state_0x85a0();
    var_3 = init_rbx();
    var_4 = init_rbp();
    var_5 = init_r12();
    var_6 = init_cc_src2();
    var_7 = init_r10();
    var_8 = init_state_0x8558();
    var_9 = init_state_0x8560();
    var_10 = init_r9();
    var_11 = init_r8();
    var_12 = init_state_0x8d58();
    var_13 = init_state_0x8549();
    var_14 = init_state_0x854c();
    var_15 = init_state_0x8548();
    var_16 = init_state_0x854b();
    var_17 = init_state_0x8547();
    var_18 = init_state_0x854d();
    *(uint64_t *)(var_0 + (-8L)) = var_5;
    *(uint64_t *)(var_0 + (-16L)) = var_4;
    *(uint64_t *)(var_0 + (-24L)) = var_3;
    rcx_2 = 0UL;
    rax_0 = 0UL;
    local_sp_2 = var_0 + (-40L);
    if (rsi == 0UL) {
        var_19 = var_0 + (-48L);
        *(uint64_t *)var_19 = 4226565UL;
        indirect_placeholder_1();
        local_sp_2 = var_19;
    }
    var_20 = local_sp_2 + 8UL;
    var_21 = local_sp_2 + (-8L);
    *(uint64_t *)var_21 = 4226586UL;
    var_22 = indirect_placeholder_15(var_20, rdi, 0UL, rsi);
    local_sp_0 = var_21;
    if (var_22 == 0UL) {
        var_23 = (uint64_t *)(rdi + 24UL);
        var_24 = *var_23;
        rax_0 = 4294967295UL;
        if ((long)var_24 < (long)0UL) {
            var_28 = (var_24 >> 1UL) | (var_24 & 1UL);
            var_29 = helper_pxor_xmm_wrapper((struct type_5 *)(0UL), (struct type_7 *)(840UL), (struct type_7 *)(840UL), var_1, var_2);
            var_30 = var_29.field_1;
            var_31 = helper_cvtsq2ss_wrapper_94((struct type_5 *)(0UL), (struct type_7 *)(840UL), var_28, var_13, var_15, var_16, var_17);
            var_32 = helper_addss_wrapper_95((struct type_5 *)(0UL), (struct type_7 *)(840UL), (struct type_7 *)(840UL), var_31.field_0, var_31.field_1, var_14, var_15, var_16, var_17, var_18);
            state_0x85a0_1 = var_30;
            state_0x8598_2 = var_32.field_0;
            storemerge = var_32.field_1;
        } else {
            var_25 = helper_pxor_xmm_wrapper((struct type_5 *)(0UL), (struct type_7 *)(840UL), (struct type_7 *)(840UL), var_1, var_2);
            var_26 = var_25.field_1;
            var_27 = helper_cvtsq2ss_wrapper_94((struct type_5 *)(0UL), (struct type_7 *)(840UL), var_24, var_13, var_15, var_16, var_17);
            state_0x85a0_1 = var_26;
            state_0x8598_2 = var_27.field_0;
            storemerge = var_27.field_1;
        }
        var_33 = (uint64_t *)(rdi + 40UL);
        var_34 = *var_33;
        var_35 = (uint64_t *)(rdi + 16UL);
        var_36 = *var_35;
        state_0x85a0_0 = state_0x85a0_1;
        state_0x8598_0 = state_0x8598_2;
        if ((long)var_36 < (long)0UL) {
            var_40 = (var_36 >> 1UL) | (var_36 & 1UL);
            var_41 = helper_pxor_xmm_wrapper_93((struct type_5 *)(0UL), (struct type_7 *)(776UL), (struct type_7 *)(776UL), var_8, var_9);
            var_42 = var_41.field_1;
            var_43 = helper_cvtsq2ss_wrapper((struct type_5 *)(0UL), (struct type_7 *)(776UL), var_40, storemerge, var_15, var_16, var_17);
            var_44 = helper_addss_wrapper((struct type_5 *)(0UL), (struct type_7 *)(776UL), (struct type_7 *)(776UL), var_43.field_0, var_43.field_1, var_14, var_15, var_16, var_17, var_18);
            rcx_2 = var_40;
            state_0x8558_2 = var_44.field_0;
            state_0x8560_0 = var_42;
            storemerge1 = var_44.field_1;
        } else {
            var_37 = helper_pxor_xmm_wrapper_93((struct type_5 *)(0UL), (struct type_7 *)(776UL), (struct type_7 *)(776UL), var_8, var_9);
            var_38 = var_37.field_1;
            var_39 = helper_cvtsq2ss_wrapper((struct type_5 *)(0UL), (struct type_7 *)(776UL), var_36, storemerge, var_15, var_16, var_17);
            state_0x8558_2 = var_39.field_0;
            state_0x8560_0 = var_38;
            storemerge1 = var_39.field_1;
        }
        var_45 = (uint64_t)*(uint32_t *)(var_34 + 8UL);
        var_46 = var_12 & (-4294967296L);
        var_47 = helper_mulss_wrapper_96((struct type_5 *)(0UL), (struct type_7 *)(776UL), (struct type_7 *)(2824UL), state_0x8558_2, var_46 | var_45, storemerge1, var_14, var_15, var_16, var_17, var_18);
        var_48 = var_47.field_0;
        var_49 = helper_ucomiss_wrapper_99((struct type_5 *)(0UL), (struct type_7 *)(840UL), (struct type_7 *)(776UL), state_0x8598_2, var_48, var_47.field_1, var_14);
        var_50 = var_49.field_0;
        var_51 = var_49.field_1;
        rcx_0 = rcx_2;
        if ((var_50 & 65UL) != 0UL) {
            var_52 = local_sp_2 + (-16L);
            *(uint64_t *)var_52 = 4226730UL;
            indirect_placeholder_3(rdi);
            var_53 = *var_33;
            var_54 = (uint64_t)*(uint32_t *)(var_53 + 8UL);
            var_55 = *var_35;
            local_sp_0 = var_52;
            if ((long)var_55 < (long)0UL) {
                var_57 = (var_55 >> 1UL) | (var_55 & 1UL);
                var_58 = helper_pxor_xmm_wrapper((struct type_5 *)(0UL), (struct type_7 *)(840UL), (struct type_7 *)(840UL), state_0x8598_2, state_0x85a0_1);
                var_59 = var_58.field_1;
                var_60 = helper_cvtsq2ss_wrapper_94((struct type_5 *)(0UL), (struct type_7 *)(840UL), var_57, var_51, var_15, var_16, var_17);
                var_61 = var_60.field_0;
                var_62 = helper_addss_wrapper_125((struct type_5 *)(0UL), (struct type_7 *)(776UL), (struct type_7 *)(840UL), var_61, var_61, var_60.field_1, var_14, var_15, var_16, var_17, var_18);
                rcx_0 = var_57;
                state_0x8558_0 = var_62.field_0;
                state_0x85a0_0 = var_59;
                state_0x8598_0 = var_61;
                storemerge2 = var_62.field_1;
            } else {
                helper_pxor_xmm_wrapper_93((struct type_5 *)(0UL), (struct type_7 *)(776UL), (struct type_7 *)(776UL), var_48, state_0x8560_0);
                var_56 = helper_cvtsq2ss_wrapper((struct type_5 *)(0UL), (struct type_7 *)(776UL), var_55, var_51, var_15, var_16, var_17);
                state_0x8558_0 = var_56.field_0;
                storemerge2 = var_56.field_1;
            }
            var_63 = *var_23;
            rcx_1 = rcx_0;
            if ((long)var_63 < (long)0UL) {
                var_65 = (var_63 >> 1UL) | (var_63 & 1UL);
                helper_pxor_xmm_wrapper((struct type_5 *)(0UL), (struct type_7 *)(840UL), (struct type_7 *)(840UL), state_0x8598_0, state_0x85a0_0);
                var_66 = helper_cvtsq2ss_wrapper_94((struct type_5 *)(0UL), (struct type_7 *)(840UL), var_65, storemerge2, var_15, var_16, var_17);
                var_67 = helper_addss_wrapper_95((struct type_5 *)(0UL), (struct type_7 *)(840UL), (struct type_7 *)(840UL), var_66.field_0, var_66.field_1, var_14, var_15, var_16, var_17, var_18);
                rcx_1 = var_65;
                state_0x8598_1 = var_67.field_0;
                storemerge3 = var_67.field_1;
            } else {
                helper_pxor_xmm_wrapper((struct type_5 *)(0UL), (struct type_7 *)(840UL), (struct type_7 *)(840UL), state_0x8598_0, state_0x85a0_0);
                var_64 = helper_cvtsq2ss_wrapper_94((struct type_5 *)(0UL), (struct type_7 *)(840UL), var_63, storemerge2, var_15, var_16, var_17);
                state_0x8598_1 = var_64.field_0;
                storemerge3 = var_64.field_1;
            }
            var_68 = helper_mulss_wrapper_126((struct type_5 *)(0UL), (struct type_7 *)(968UL), (struct type_7 *)(776UL), state_0x8558_0, var_54, storemerge3, var_14, var_15, var_16, var_17, var_18);
            var_69 = helper_ucomiss_wrapper_127((struct type_5 *)(0UL), (struct type_7 *)(840UL), (struct type_7 *)(968UL), state_0x8598_1, var_68.field_0, var_68.field_1, var_14);
            var_70 = var_69.field_1;
            if ((var_69.field_0 & 65UL) != 0UL) {
                var_71 = *(unsigned char *)(var_53 + 16UL);
                var_72 = (uint64_t)var_71;
                var_73 = (var_71 == '\x00');
                var_74 = var_46 | (uint64_t)*(uint32_t *)(var_53 + 12UL);
                var_75 = helper_mulss_wrapper_96((struct type_5 *)(0UL), (struct type_7 *)(776UL), (struct type_7 *)(2824UL), state_0x8558_0, var_74, var_70, var_14, var_15, var_16, var_17, var_18);
                var_76 = var_75.field_0;
                var_77 = var_75.field_1;
                state_0x8558_1 = var_76;
                storemerge4 = var_77;
                if (var_73) {
                    var_78 = helper_mulss_wrapper_128((struct type_5 *)(0UL), (struct type_7 *)(776UL), (struct type_7 *)(904UL), var_76, var_54, var_77, var_14, var_15, var_16, var_17, var_18);
                    state_0x8558_1 = var_78.field_0;
                    storemerge4 = var_78.field_1;
                }
                var_79 = (uint64_t)*(uint32_t *)4273272UL;
                var_80 = var_74 & (-4294967296L);
                var_81 = helper_ucomiss_wrapper_97((struct type_5 *)(0UL), (struct type_7 *)(776UL), (struct type_7 *)(2824UL), state_0x8558_1, var_80 | var_79, storemerge4, var_14);
                var_82 = helper_cc_compute_c_wrapper(var_72, var_81.field_0, var_6, 1U);
                if (var_82 == 0UL) {
                    return rax_0;
                }
                var_83 = helper_ucomiss_wrapper_97((struct type_5 *)(0UL), (struct type_7 *)(776UL), (struct type_7 *)(2824UL), state_0x8558_1, var_80 | (uint64_t)*(uint32_t *)4273276UL, var_81.field_1, var_14);
                var_84 = var_83.field_0;
                var_85 = var_83.field_1;
                var_86 = helper_cc_compute_c_wrapper(var_72, var_84, var_6, 1U);
                if (var_86 == 0UL) {
                    var_88 = helper_subss_wrapper((struct type_5 *)(0UL), (struct type_7 *)(776UL), (struct type_7 *)(2824UL), state_0x8558_1, var_80 | (uint64_t)*(uint32_t *)4273276UL, var_85, var_14, var_15, var_16, var_17, var_18);
                    var_89 = helper_cvttss2sq_wrapper((struct type_5 *)(0UL), (struct type_7 *)(776UL), var_88.field_0, var_88.field_1, var_14);
                    rsi3_0 = var_89.field_0 ^ (-9223372036854775808L);
                } else {
                    var_87 = helper_cvttss2sq_wrapper((struct type_5 *)(0UL), (struct type_7 *)(776UL), state_0x8558_1, var_85, var_14);
                    rsi3_0 = var_87.field_0;
                }
                *(uint64_t *)(local_sp_2 + (-24L)) = 4226929UL;
                var_90 = indirect_placeholder_2(rdi, rcx_1, var_7, rsi3_0, var_10, var_11);
                if ((uint64_t)(unsigned char)var_90 == 0UL) {
                    return rax_0;
                }
                var_91 = local_sp_2 + (-32L);
                *(uint64_t *)var_91 = 4226958UL;
                var_92 = indirect_placeholder_15(var_52, rdi, 0UL, rsi);
                local_sp_0 = var_91;
                if (var_92 == 0UL) {
                    var_93 = local_sp_2 + (-40L);
                    *(uint64_t *)var_93 = 4226968UL;
                    indirect_placeholder_1();
                    local_sp_0 = var_93;
                }
            }
        }
        var_94 = (uint64_t *)(local_sp_0 + 8UL);
        _cast = (uint64_t *)*var_94;
        _pre_phi186 = var_94;
        rax_0 = 1UL;
        if (*_cast == 0UL) {
            *_cast = rsi;
            var_102 = (uint64_t *)(rdi + 32UL);
            *var_102 = (*var_102 + 1UL);
            *var_23 = (*var_23 + 1UL);
        } else {
            var_95 = (uint64_t *)(rdi + 72UL);
            var_96 = *var_95;
            rdx1_0 = var_96;
            if (var_96 == 0UL) {
                var_97 = (uint64_t *)(var_96 + 8UL);
                *var_95 = *var_97;
                _pre_phi190 = var_97;
            } else {
                var_98 = local_sp_0 + (-8L);
                *(uint64_t *)var_98 = 4227008UL;
                var_99 = indirect_placeholder_3(16UL);
                rdx1_0 = var_99;
                if (var_99 != 0UL) {
                    return rax_0;
                }
                _pre_phi190 = (uint64_t *)(var_99 + 8UL);
                _pre_phi186 = (uint64_t *)(var_98 + 8UL);
            }
            *(uint64_t *)rdx1_0 = rsi;
            var_100 = (uint64_t *)(*_pre_phi186 + 8UL);
            *_pre_phi190 = *var_100;
            *var_100 = rdx1_0;
            var_101 = (uint64_t *)(rdi + 32UL);
            *var_101 = (*var_101 + 1UL);
        }
    } else {
        if (rdx == 0UL) {
            *(uint64_t *)rdx = var_22;
        }
    }
    return rax_0;
}

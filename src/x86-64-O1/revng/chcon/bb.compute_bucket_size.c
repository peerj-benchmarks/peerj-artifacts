typedef unsigned char __u_char;
typedef unsigned short __u_short;
typedef unsigned int __u_int;
typedef unsigned long __u_long;
typedef signed char __int8_t;
typedef unsigned char __uint8_t;
typedef short __int16_t;
typedef unsigned short __uint16_t;
typedef int __int32_t;
typedef unsigned int __uint32_t;
typedef long __int64_t;
typedef unsigned long __uint64_t;
typedef __int8_t __int_least8_t;
typedef __uint8_t __uint_least8_t;
typedef __int16_t __int_least16_t;
typedef __uint16_t __uint_least16_t;
typedef __int32_t __int_least32_t;
typedef __uint32_t __uint_least32_t;
typedef __int64_t __int_least64_t;
typedef __uint64_t __uint_least64_t;
typedef long __quad_t;
typedef unsigned long __u_quad_t;
typedef long __intmax_t;
typedef unsigned long __uintmax_t;
typedef unsigned long __dev_t;
typedef unsigned int __uid_t;
typedef unsigned int __gid_t;
typedef unsigned long __ino_t;
typedef unsigned long __ino64_t;
typedef unsigned int __mode_t;
typedef unsigned long __nlink_t;
typedef long __off_t;
typedef long __off64_t;
typedef int __pid_t;
typedef struct {
    int __val[2];
} __fsid_t;
typedef long __clock_t;
typedef unsigned long __rlim_t;
typedef unsigned long __rlim64_t;
typedef unsigned int __id_t;
typedef long __time_t;
typedef unsigned int __useconds_t;
typedef long __suseconds_t;
typedef int __daddr_t;
typedef int __key_t;
typedef int __clockid_t;
typedef void *__timer_t;
typedef long __blksize_t;
typedef long __blkcnt_t;
typedef long __blkcnt64_t;
typedef unsigned long __fsblkcnt_t;
typedef unsigned long __fsblkcnt64_t;
typedef unsigned long __fsfilcnt_t;
typedef unsigned long __fsfilcnt64_t;
typedef long __fsword_t;
typedef long __ssize_t;
typedef long __syscall_slong_t;
typedef unsigned long __syscall_ulong_t;
typedef __off64_t __loff_t;
typedef char *__caddr_t;
typedef long __intptr_t;
typedef unsigned int __socklen_t;
typedef int __sig_atomic_t;
typedef __int8_t int8_t;
typedef __int16_t int16_t;
typedef __int32_t int32_t;
typedef __int64_t int64_t;
typedef __uint8_t uint8_t;
typedef __uint16_t uint16_t;
typedef __uint32_t uint32_t;
typedef __uint64_t uint64_t;
typedef __int_least8_t int_least8_t;
typedef __int_least16_t int_least16_t;
typedef __int_least32_t int_least32_t;
typedef __int_least64_t int_least64_t;
typedef __uint_least8_t uint_least8_t;
typedef __uint_least16_t uint_least16_t;
typedef __uint_least32_t uint_least32_t;
typedef __uint_least64_t uint_least64_t;
typedef signed char int_fast8_t;
typedef long int_fast16_t;
typedef long int_fast32_t;
typedef long int_fast64_t;
typedef unsigned char uint_fast8_t;
typedef unsigned long uint_fast16_t;
typedef unsigned long uint_fast32_t;
typedef unsigned long uint_fast64_t;
typedef long intptr_t;
typedef unsigned long uintptr_t;
typedef __intmax_t intmax_t;
typedef __uintmax_t uintmax_t;
struct bb_compute_bucket_size_ret_type;
struct helper_pxor_xmm_wrapper_93_ret_type;
struct type_5;
struct type_7;
struct helper_cvtsq2ss_wrapper_ret_type;
struct helper_addss_wrapper_ret_type;
struct helper_ucomiss_wrapper_97_ret_type;
struct helper_cvttss2sq_wrapper_ret_type;
struct helper_subss_wrapper_ret_type;
struct helper_divq_EAX_wrapper_ret_type;
struct helper_divss_wrapper_ret_type;
struct bb_compute_bucket_size_ret_type {
    uint64_t field_0;
    uint64_t field_1;
    uint64_t field_2;
    uint64_t field_3;
    uint64_t field_4;
    uint64_t field_5;
    uint64_t field_6;
};
struct helper_pxor_xmm_wrapper_93_ret_type {
    uint64_t field_0;
    uint64_t field_1;
};
struct type_5 {
};
struct type_7 {
};
struct helper_cvtsq2ss_wrapper_ret_type {
    uint64_t field_0;
    unsigned char field_1;
};
struct helper_addss_wrapper_ret_type {
    uint64_t field_0;
    unsigned char field_1;
};
struct helper_ucomiss_wrapper_97_ret_type {
    uint64_t field_0;
    unsigned char field_1;
};
struct helper_cvttss2sq_wrapper_ret_type {
    uint64_t field_0;
    unsigned char field_1;
};
struct helper_subss_wrapper_ret_type {
    uint64_t field_0;
    unsigned char field_1;
};
struct helper_divq_EAX_wrapper_ret_type {
    uint64_t field_0;
    uint64_t field_1;
    uint64_t field_2;
    uint64_t field_3;
    uint32_t field_4;
    uint32_t field_5;
    uint64_t field_6;
    uint32_t field_7;
    uint64_t field_8;
    uint32_t field_9;
    uint32_t field_10;
    unsigned char field_11;
    uint32_t field_12;
};
struct helper_divss_wrapper_ret_type {
    uint64_t field_0;
    unsigned char field_1;
};
extern uint64_t revng_init_local_sp(uint64_t param_0);
extern uint64_t init_state_0x8558(void);
extern unsigned char init_state_0x8549(void);
extern unsigned char init_state_0x854c(void);
extern unsigned char init_state_0x8548(void);
extern unsigned char init_state_0x854b(void);
extern unsigned char init_state_0x8547(void);
extern unsigned char init_state_0x854d(void);
extern uint64_t init_cc_src2(void);
extern uint64_t helper_cc_compute_c_wrapper(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint32_t param_3);
extern uint64_t init_state_0x8560(void);
extern struct helper_pxor_xmm_wrapper_93_ret_type helper_pxor_xmm_wrapper_93(struct type_5 *param_0, struct type_7 *param_1, struct type_7 *param_2, uint64_t param_3, uint64_t param_4);
extern uint64_t init_state_0x8d58(void);
extern struct helper_cvtsq2ss_wrapper_ret_type helper_cvtsq2ss_wrapper(struct type_5 *param_0, struct type_7 *param_1, uint64_t param_2, unsigned char param_3, unsigned char param_4, unsigned char param_5, unsigned char param_6);
extern struct helper_addss_wrapper_ret_type helper_addss_wrapper(struct type_5 *param_0, struct type_7 *param_1, struct type_7 *param_2, uint64_t param_3, unsigned char param_4, unsigned char param_5, unsigned char param_6, unsigned char param_7, unsigned char param_8, unsigned char param_9);
extern struct helper_ucomiss_wrapper_97_ret_type helper_ucomiss_wrapper_97(struct type_5 *param_0, struct type_7 *param_1, struct type_7 *param_2, uint64_t param_3, uint64_t param_4, unsigned char param_5, unsigned char param_6);
extern struct helper_cvttss2sq_wrapper_ret_type helper_cvttss2sq_wrapper(struct type_5 *param_0, struct type_7 *param_1, uint64_t param_2, unsigned char param_3, unsigned char param_4);
extern struct helper_subss_wrapper_ret_type helper_subss_wrapper(struct type_5 *param_0, struct type_7 *param_1, struct type_7 *param_2, uint64_t param_3, uint64_t param_4, unsigned char param_5, unsigned char param_6, unsigned char param_7, unsigned char param_8, unsigned char param_9, unsigned char param_10);
extern uint64_t init_state_0x9018(void);
extern uint32_t init_state_0x9010(void);
extern uint64_t init_state_0x8408(void);
extern uint64_t init_state_0x8328(void);
extern uint64_t init_state_0x82d8(void);
extern uint32_t init_state_0x9080(void);
extern uint32_t init_state_0x8248(void);
extern struct helper_divq_EAX_wrapper_ret_type helper_divq_EAX_wrapper(struct type_5 *param_0, uint64_t param_1, uint64_t param_2, uint64_t param_3, uint64_t param_4, uint64_t param_5, uint64_t param_6, uint64_t param_7, uint64_t param_8, uint64_t param_9, uint64_t param_10, uint64_t param_11, uint64_t param_12, uint64_t param_13, uint32_t param_14, uint64_t param_15, uint64_t param_16, uint64_t param_17, uint32_t param_18, uint32_t param_19);
extern uint64_t init_rcx(void);
extern uint64_t init_r9(void);
extern struct helper_divss_wrapper_ret_type helper_divss_wrapper(struct type_5 *param_0, struct type_7 *param_1, struct type_7 *param_2, uint64_t param_3, uint64_t param_4, unsigned char param_5, unsigned char param_6, unsigned char param_7, unsigned char param_8, unsigned char param_9, unsigned char param_10);
typedef _Bool bool;
struct bb_compute_bucket_size_ret_type bb_compute_bucket_size(uint64_t rbx, uint64_t rbp, uint64_t rdi, uint64_t r10, uint64_t rsi, uint64_t r8) {
    struct helper_cvtsq2ss_wrapper_ret_type var_21;
    struct helper_cvttss2sq_wrapper_ret_type var_33;
    uint64_t var_0;
    uint64_t var_1;
    uint64_t var_2;
    uint64_t var_3;
    uint64_t var_4;
    uint64_t var_5;
    unsigned char var_6;
    unsigned char var_7;
    unsigned char var_8;
    unsigned char var_9;
    unsigned char var_10;
    unsigned char var_11;
    uint64_t var_12;
    uint32_t var_13;
    uint64_t var_14;
    uint64_t var_15;
    uint64_t var_16;
    uint32_t var_17;
    uint32_t var_18;
    uint64_t rdi23_0;
    struct helper_cvtsq2ss_wrapper_ret_type var_19;
    uint64_t state_0x8558_0;
    uint64_t var_20;
    struct helper_addss_wrapper_ret_type var_22;
    struct bb_compute_bucket_size_ret_type mrv3;
    struct bb_compute_bucket_size_ret_type mrv4;
    uint64_t cc_dst_0;
    unsigned char storemerge;
    uint64_t var_23;
    uint64_t var_24;
    struct helper_divss_wrapper_ret_type var_25;
    uint64_t var_26;
    struct helper_ucomiss_wrapper_97_ret_type var_27;
    uint64_t var_28;
    uint64_t r9_0;
    struct helper_ucomiss_wrapper_97_ret_type var_29;
    uint64_t var_30;
    unsigned char var_31;
    uint64_t var_32;
    struct helper_subss_wrapper_ret_type var_34;
    struct helper_cvttss2sq_wrapper_ret_type var_35;
    uint64_t rdi23_1;
    uint64_t var_36;
    struct bb_compute_bucket_size_ret_type mrv1 = {0UL, /*implicit*/(int)0, /*implicit*/(int)0, /*implicit*/(int)0, /*implicit*/(int)0, /*implicit*/(int)0, /*implicit*/(int)0};
    struct bb_compute_bucket_size_ret_type mrv2;
    uint64_t state_0x9018_1;
    uint32_t state_0x9010_1;
    struct bb_compute_bucket_size_ret_type mrv5;
    struct bb_compute_bucket_size_ret_type mrv6;
    uint64_t rsi25_3;
    uint64_t r826_0;
    uint64_t rcx_4;
    uint64_t rsi25_0;
    uint64_t state_0x9018_3;
    uint64_t rcx_0;
    uint64_t rdi23_2;
    uint32_t state_0x9010_3;
    uint64_t state_0x9018_0;
    uint64_t state_0x82d8_3;
    uint32_t state_0x9010_0;
    uint32_t state_0x9080_3;
    uint64_t state_0x82d8_0;
    uint32_t state_0x8248_3;
    uint32_t state_0x9080_0;
    uint32_t state_0x8248_0;
    uint64_t r826_2;
    uint64_t r826_4;
    uint64_t r826_1;
    uint64_t rsi25_1;
    uint64_t rcx_1;
    uint64_t state_0x82d8_1;
    uint32_t state_0x9080_1;
    uint32_t state_0x8248_1;
    uint64_t var_37;
    uint64_t var_38;
    uint64_t var_39;
    uint64_t var_40;
    struct helper_divq_EAX_wrapper_ret_type var_41;
    uint64_t rsi25_2;
    uint64_t rcx_2;
    uint64_t state_0x9018_2;
    uint32_t state_0x9010_2;
    uint64_t state_0x82d8_2;
    uint32_t state_0x9080_2;
    uint32_t state_0x8248_2;
    struct helper_divq_EAX_wrapper_ret_type var_42;
    uint64_t r826_3;
    uint64_t rcx_3;
    uint64_t rax_0;
    struct bb_compute_bucket_size_ret_type mrv7;
    struct bb_compute_bucket_size_ret_type mrv8;
    struct bb_compute_bucket_size_ret_type mrv9;
    struct bb_compute_bucket_size_ret_type mrv10;
    struct bb_compute_bucket_size_ret_type mrv11;
    struct bb_compute_bucket_size_ret_type mrv12;
    struct bb_compute_bucket_size_ret_type mrv13;
    struct bb_compute_bucket_size_ret_type mrv15 = {0UL, /*implicit*/(int)0, /*implicit*/(int)0, /*implicit*/(int)0, /*implicit*/(int)0, /*implicit*/(int)0, /*implicit*/(int)0};
    struct bb_compute_bucket_size_ret_type mrv16;
    struct bb_compute_bucket_size_ret_type mrv17;
    struct bb_compute_bucket_size_ret_type mrv18;
    struct bb_compute_bucket_size_ret_type mrv19;
    struct bb_compute_bucket_size_ret_type mrv20;
    unsigned int loop_state_var;
    bool switch_state_var;
    revng_init_local_sp(0UL);
    var_0 = init_rcx();
    var_1 = init_cc_src2();
    var_2 = init_state_0x8558();
    var_3 = init_state_0x8560();
    var_4 = init_r9();
    var_5 = init_state_0x8d58();
    var_6 = init_state_0x8549();
    var_7 = init_state_0x854c();
    var_8 = init_state_0x8548();
    var_9 = init_state_0x854b();
    var_10 = init_state_0x8547();
    var_11 = init_state_0x854d();
    var_12 = init_state_0x9018();
    var_13 = init_state_0x9010();
    var_14 = init_state_0x8408();
    var_15 = init_state_0x8328();
    var_16 = init_state_0x82d8();
    var_17 = init_state_0x9080();
    var_18 = init_state_0x8248();
    rdi23_0 = rdi;
    cc_dst_0 = rdi;
    r9_0 = 12297829382473034411UL;
    rdi23_1 = 11UL;
    r826_0 = r8;
    rsi25_0 = rsi;
    rcx_0 = var_0;
    state_0x9018_0 = var_12;
    state_0x9010_0 = var_13;
    state_0x82d8_0 = var_16;
    state_0x9080_0 = var_17;
    state_0x8248_0 = var_18;
    r826_1 = 16UL;
    rsi25_1 = 9UL;
    rcx_1 = 3UL;
    rcx_2 = 3UL;
    r826_3 = r8;
    rcx_3 = var_0;
    rax_0 = 0UL;
    if (*(unsigned char *)(rsi + 16UL) != '\x00') {
        r9_0 = var_4;
        if ((long)rdi < (long)0UL) {
            var_20 = (rdi >> 1UL) | (rdi & 1UL);
            helper_pxor_xmm_wrapper_93((struct type_5 *)(0UL), (struct type_7 *)(776UL), (struct type_7 *)(776UL), var_2, var_3);
            var_21 = helper_cvtsq2ss_wrapper((struct type_5 *)(0UL), (struct type_7 *)(776UL), var_20, var_6, var_8, var_9, var_10);
            var_22 = helper_addss_wrapper((struct type_5 *)(0UL), (struct type_7 *)(776UL), (struct type_7 *)(776UL), var_21.field_0, var_21.field_1, var_7, var_8, var_9, var_10, var_11);
            state_0x8558_0 = var_22.field_0;
            cc_dst_0 = var_20;
            storemerge = var_22.field_1;
        } else {
            helper_pxor_xmm_wrapper_93((struct type_5 *)(0UL), (struct type_7 *)(776UL), (struct type_7 *)(776UL), var_2, var_3);
            var_19 = helper_cvtsq2ss_wrapper((struct type_5 *)(0UL), (struct type_7 *)(776UL), rdi, var_6, var_8, var_9, var_10);
            state_0x8558_0 = var_19.field_0;
            storemerge = var_19.field_1;
        }
        var_23 = (uint64_t)*(uint32_t *)(rsi + 8UL);
        var_24 = var_5 & (-4294967296L);
        var_25 = helper_divss_wrapper((struct type_5 *)(0UL), (struct type_7 *)(776UL), (struct type_7 *)(2824UL), state_0x8558_0, var_24 | var_23, storemerge, var_7, var_8, var_9, var_10, var_11);
        var_26 = var_25.field_0;
        var_27 = helper_ucomiss_wrapper_97((struct type_5 *)(0UL), (struct type_7 *)(776UL), (struct type_7 *)(2824UL), var_26, var_24 | (uint64_t)*(uint32_t *)4273272UL, var_25.field_1, var_7);
        var_28 = helper_cc_compute_c_wrapper(cc_dst_0, var_27.field_0, var_1, 1U);
        if (var_28 == 0UL) {
            mrv7.field_0 = rax_0;
            mrv8 = mrv7;
            mrv8.field_1 = rbx;
            mrv9 = mrv8;
            mrv9.field_2 = rbp;
            mrv10 = mrv9;
            mrv10.field_3 = rcx_3;
            mrv11 = mrv10;
            mrv11.field_4 = r10;
            mrv12 = mrv11;
            mrv12.field_5 = r9_0;
            mrv13 = mrv12;
            mrv13.field_6 = r826_3;
            return mrv13;
        }
        var_29 = helper_ucomiss_wrapper_97((struct type_5 *)(0UL), (struct type_7 *)(776UL), (struct type_7 *)(2824UL), var_26, var_24 | (uint64_t)*(uint32_t *)4273276UL, var_27.field_1, var_7);
        var_30 = var_29.field_0;
        var_31 = var_29.field_1;
        var_32 = helper_cc_compute_c_wrapper(cc_dst_0, var_30, var_1, 1U);
        if (var_32 == 0UL) {
            var_34 = helper_subss_wrapper((struct type_5 *)(0UL), (struct type_7 *)(776UL), (struct type_7 *)(2824UL), var_26, var_24 | (uint64_t)*(uint32_t *)4273276UL, var_31, var_7, var_8, var_9, var_10, var_11);
            var_35 = helper_cvttss2sq_wrapper((struct type_5 *)(0UL), (struct type_7 *)(776UL), var_34.field_0, var_34.field_1, var_7);
            rdi23_0 = var_35.field_0 ^ (-9223372036854775808L);
        } else {
            var_33 = helper_cvttss2sq_wrapper((struct type_5 *)(0UL), (struct type_7 *)(776UL), var_26, var_31, var_7);
            rdi23_0 = var_33.field_0;
        }
    }
    if (rdi23_0 <= 9UL) {
        var_36 = rdi23_0 | 1UL;
        rdi23_1 = var_36;
        if (var_36 == 18446744073709551615UL) {
            mrv1.field_1 = rbx;
            mrv2 = mrv1;
            mrv2.field_2 = rbp;
            mrv3 = mrv2;
            mrv3.field_3 = var_0;
            mrv4 = mrv3;
            mrv4.field_4 = r10;
            mrv5 = mrv4;
            mrv5.field_5 = var_4;
            mrv6 = mrv5;
            mrv6.field_6 = r8;
            return mrv6;
        }
    }
    rdi23_2 = rdi23_1;
    while (1U)
        {
            state_0x9018_1 = state_0x9018_0;
            state_0x9010_1 = state_0x9010_0;
            rsi25_3 = rsi25_0;
            rcx_4 = rcx_0;
            state_0x9018_3 = state_0x9018_0;
            state_0x9010_3 = state_0x9010_0;
            state_0x82d8_3 = state_0x82d8_0;
            state_0x9080_3 = state_0x9080_0;
            state_0x8248_3 = state_0x8248_0;
            r826_2 = r826_0;
            r826_4 = r826_0;
            state_0x82d8_1 = state_0x82d8_0;
            state_0x9080_1 = state_0x9080_0;
            state_0x8248_1 = state_0x8248_0;
            rsi25_2 = rsi25_0;
            state_0x9018_2 = state_0x9018_0;
            state_0x9010_2 = state_0x9010_0;
            state_0x82d8_2 = state_0x82d8_0;
            state_0x9080_2 = state_0x9080_0;
            state_0x8248_2 = state_0x8248_0;
            if (rdi23_2 <= 9UL) {
                var_42 = helper_divq_EAX_wrapper((struct type_5 *)(0UL), rcx_2, 4225117UL, rdi23_2, 0UL, rbx, rbp, rdi23_2, rcx_2, r10, rsi25_2, 12297829382473034411UL, r826_2, state_0x9018_2, state_0x9010_2, var_14, var_15, state_0x82d8_2, state_0x9080_2, state_0x8248_2);
                rsi25_3 = rsi25_2;
                rcx_4 = rcx_2;
                state_0x9018_3 = var_42.field_6;
                state_0x9010_3 = var_42.field_7;
                state_0x82d8_3 = var_42.field_8;
                state_0x9080_3 = var_42.field_9;
                state_0x8248_3 = var_42.field_10;
                r826_4 = r826_2;
                r826_3 = r826_2;
                rcx_3 = rcx_2;
                if (var_42.field_2 != 0UL) {
                    rax_0 = (rdi23_2 > 1152921504606846975UL) ? 0UL : rdi23_2;
                    break;
                }
                r826_0 = r826_4;
                rsi25_0 = rsi25_3;
                rcx_0 = rcx_4;
                state_0x9018_0 = state_0x9018_3;
                state_0x9010_0 = state_0x9010_3;
                state_0x82d8_0 = state_0x82d8_3;
                state_0x9080_0 = state_0x9080_3;
                state_0x8248_0 = state_0x8248_3;
                if (rdi23_2 != 18446744073709551613UL) {
                    rdi23_2 = rdi23_2 + 2UL;
                    continue;
                }
                mrv15.field_1 = rbx;
                mrv16 = mrv15;
                mrv16.field_2 = rbp;
                mrv17 = mrv16;
                mrv17.field_3 = rcx_4;
                mrv18 = mrv17;
                mrv18.field_4 = r10;
                mrv19 = mrv18;
                mrv19.field_5 = 12297829382473034411UL;
                mrv20 = mrv19;
                mrv20.field_6 = r826_4;
                return mrv20;
            }
            if (rdi23_2 != ((uint64_t)(((unsigned __int128)rdi23_2 * 12297829382473034411ULL) >> 65ULL) * 3UL)) {
                while (1U)
                    {
                        var_37 = rsi25_1 + r826_1;
                        var_38 = rcx_1 + 2UL;
                        var_39 = helper_cc_compute_c_wrapper(var_37 - rdi23_2, rdi23_2, var_1, 17U);
                        rsi25_3 = var_37;
                        rcx_4 = var_38;
                        r826_2 = r826_1;
                        rsi25_1 = var_37;
                        rcx_1 = var_38;
                        rsi25_2 = var_37;
                        rcx_2 = var_38;
                        state_0x9018_2 = state_0x9018_1;
                        state_0x9010_2 = state_0x9010_1;
                        state_0x82d8_2 = state_0x82d8_1;
                        state_0x9080_2 = state_0x9080_1;
                        state_0x8248_2 = state_0x8248_1;
                        if (var_39 != 0UL) {
                            loop_state_var = 1U;
                            break;
                        }
                        var_40 = r826_1 + 8UL;
                        var_41 = helper_divq_EAX_wrapper((struct type_5 *)(0UL), var_38, 4225094UL, rdi23_2, 0UL, rbx, rbp, rdi23_2, var_38, r10, var_37, 12297829382473034411UL, var_40, state_0x9018_1, state_0x9010_1, var_14, var_15, state_0x82d8_1, state_0x9080_1, state_0x8248_1);
                        r826_1 = var_40;
                        r826_4 = var_40;
                        if (var_41.field_2 != 0UL) {
                            loop_state_var = 0U;
                            break;
                        }
                        state_0x9018_1 = var_41.field_6;
                        state_0x9010_1 = var_41.field_7;
                        state_0x82d8_1 = var_41.field_8;
                        state_0x9080_1 = var_41.field_9;
                        state_0x8248_1 = var_41.field_10;
                        continue;
                    }
                switch_state_var = 0;
                switch (loop_state_var) {
                  case 0U:
                    {
                        state_0x9018_3 = var_41.field_6;
                        state_0x9010_3 = var_41.field_7;
                        state_0x82d8_3 = var_41.field_8;
                        state_0x9080_3 = var_41.field_9;
                        state_0x8248_3 = var_41.field_10;
                    }
                    break;
                  case 1U:
                    {
                        var_42 = helper_divq_EAX_wrapper((struct type_5 *)(0UL), rcx_2, 4225117UL, rdi23_2, 0UL, rbx, rbp, rdi23_2, rcx_2, r10, rsi25_2, 12297829382473034411UL, r826_2, state_0x9018_2, state_0x9010_2, var_14, var_15, state_0x82d8_2, state_0x9080_2, state_0x8248_2);
                        rsi25_3 = rsi25_2;
                        rcx_4 = rcx_2;
                        state_0x9018_3 = var_42.field_6;
                        state_0x9010_3 = var_42.field_7;
                        state_0x82d8_3 = var_42.field_8;
                        state_0x9080_3 = var_42.field_9;
                        state_0x8248_3 = var_42.field_10;
                        r826_4 = r826_2;
                        r826_3 = r826_2;
                        rcx_3 = rcx_2;
                        if (var_42.field_2 != 0UL) {
                            rax_0 = (rdi23_2 > 1152921504606846975UL) ? 0UL : rdi23_2;
                            switch_state_var = 1;
                            break;
                        }
                    }
                    break;
                }
                if (switch_state_var)
                    break;
            }
        }
    mrv7.field_0 = rax_0;
    mrv8 = mrv7;
    mrv8.field_1 = rbx;
    mrv9 = mrv8;
    mrv9.field_2 = rbp;
    mrv10 = mrv9;
    mrv10.field_3 = rcx_3;
    mrv11 = mrv10;
    mrv11.field_4 = r10;
    mrv12 = mrv11;
    mrv12.field_5 = r9_0;
    mrv13 = mrv12;
    mrv13.field_6 = r826_3;
    return mrv13;
}

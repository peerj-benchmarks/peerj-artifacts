typedef unsigned char __u_char;
typedef unsigned short __u_short;
typedef unsigned int __u_int;
typedef unsigned long __u_long;
typedef signed char __int8_t;
typedef unsigned char __uint8_t;
typedef short __int16_t;
typedef unsigned short __uint16_t;
typedef int __int32_t;
typedef unsigned int __uint32_t;
typedef long __int64_t;
typedef unsigned long __uint64_t;
typedef __int8_t __int_least8_t;
typedef __uint8_t __uint_least8_t;
typedef __int16_t __int_least16_t;
typedef __uint16_t __uint_least16_t;
typedef __int32_t __int_least32_t;
typedef __uint32_t __uint_least32_t;
typedef __int64_t __int_least64_t;
typedef __uint64_t __uint_least64_t;
typedef long __quad_t;
typedef unsigned long __u_quad_t;
typedef long __intmax_t;
typedef unsigned long __uintmax_t;
typedef unsigned long __dev_t;
typedef unsigned int __uid_t;
typedef unsigned int __gid_t;
typedef unsigned long __ino_t;
typedef unsigned long __ino64_t;
typedef unsigned int __mode_t;
typedef unsigned long __nlink_t;
typedef long __off_t;
typedef long __off64_t;
typedef int __pid_t;
typedef struct {
    int __val[2];
} __fsid_t;
typedef long __clock_t;
typedef unsigned long __rlim_t;
typedef unsigned long __rlim64_t;
typedef unsigned int __id_t;
typedef long __time_t;
typedef unsigned int __useconds_t;
typedef long __suseconds_t;
typedef int __daddr_t;
typedef int __key_t;
typedef int __clockid_t;
typedef void *__timer_t;
typedef long __blksize_t;
typedef long __blkcnt_t;
typedef long __blkcnt64_t;
typedef unsigned long __fsblkcnt_t;
typedef unsigned long __fsblkcnt64_t;
typedef unsigned long __fsfilcnt_t;
typedef unsigned long __fsfilcnt64_t;
typedef long __fsword_t;
typedef long __ssize_t;
typedef long __syscall_slong_t;
typedef unsigned long __syscall_ulong_t;
typedef __off64_t __loff_t;
typedef char *__caddr_t;
typedef long __intptr_t;
typedef unsigned int __socklen_t;
typedef int __sig_atomic_t;
typedef __int8_t int8_t;
typedef __int16_t int16_t;
typedef __int32_t int32_t;
typedef __int64_t int64_t;
typedef __uint8_t uint8_t;
typedef __uint16_t uint16_t;
typedef __uint32_t uint32_t;
typedef __uint64_t uint64_t;
typedef __int_least8_t int_least8_t;
typedef __int_least16_t int_least16_t;
typedef __int_least32_t int_least32_t;
typedef __int_least64_t int_least64_t;
typedef __uint_least8_t uint_least8_t;
typedef __uint_least16_t uint_least16_t;
typedef __uint_least32_t uint_least32_t;
typedef __uint_least64_t uint_least64_t;
typedef signed char int_fast8_t;
typedef long int_fast16_t;
typedef long int_fast32_t;
typedef long int_fast64_t;
typedef unsigned char uint_fast8_t;
typedef unsigned long uint_fast16_t;
typedef unsigned long uint_fast32_t;
typedef unsigned long uint_fast64_t;
typedef long intptr_t;
typedef unsigned long uintptr_t;
typedef __intmax_t intmax_t;
typedef __uintmax_t uintmax_t;
extern uint64_t revng_init_local_sp(uint64_t param_0);
extern uint64_t init_rbx(void);
extern void indirect_placeholder_1(void);
extern uint64_t init_rbp(void);
extern uint64_t init_r12(void);
extern uint64_t indirect_placeholder_3(uint64_t param_0);
extern uint64_t indirect_placeholder_5(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint64_t param_3, uint64_t param_4);
extern uint64_t indirect_placeholder(uint64_t param_0, uint64_t param_1);
uint64_t bb_filesystem_type(uint64_t rdi) {
    uint64_t var_11;
    uint64_t var_0;
    uint64_t var_1;
    uint64_t var_2;
    uint64_t var_3;
    uint64_t *var_4;
    uint64_t var_5;
    uint64_t *var_6;
    uint64_t var_7;
    uint64_t rax_0;
    uint64_t r12_0;
    uint64_t var_21;
    uint64_t local_sp_0;
    uint64_t var_22;
    uint64_t var_19;
    uint64_t var_20;
    uint64_t local_sp_1;
    uint64_t var_25;
    uint64_t var_17;
    uint64_t var_18;
    uint64_t var_15;
    uint64_t var_16;
    uint64_t var_23;
    uint64_t var_24;
    uint64_t var_8;
    uint64_t *_pre_phi;
    uint64_t var_9;
    uint64_t *var_10;
    uint64_t *var_12;
    uint64_t local_sp_2;
    uint64_t *var_13;
    uint64_t var_14;
    var_0 = revng_init_local_sp(0UL);
    var_1 = init_rbx();
    var_2 = init_rbp();
    var_3 = init_r12();
    *(uint64_t *)(var_0 + (-8L)) = var_3;
    *(uint64_t *)(var_0 + (-16L)) = var_2;
    *(uint64_t *)(var_0 + (-24L)) = var_1;
    var_4 = (uint64_t *)(rdi + 80UL);
    var_5 = *var_4;
    var_6 = (uint64_t *)(var_5 + 80UL);
    var_7 = *var_6;
    r12_0 = var_7;
    rax_0 = 0UL;
    if ((*(unsigned char *)(var_5 + 73UL) & '\x02') == '\x00') {
        return rax_0;
    }
    var_8 = var_0 + (-168L);
    local_sp_2 = var_8;
    if (var_7 == 0UL) {
        var_9 = var_0 + (-176L);
        var_10 = (uint64_t *)var_9;
        *var_10 = 4213486UL;
        var_11 = indirect_placeholder_5(4212205UL, 13UL, 4212220UL, 0UL, 4201936UL);
        *var_6 = var_11;
        _pre_phi = var_10;
        local_sp_2 = var_9;
        r12_0 = var_11;
        if (var_11 != 0UL) {
            var_23 = *var_4;
            var_24 = var_0 + (-184L);
            *(uint64_t *)var_24 = 4213662UL;
            indirect_placeholder_1();
            local_sp_1 = var_24;
            if ((uint64_t)(uint32_t)var_23 == 0UL) {
                return rax_0;
            }
            var_25 = *(uint64_t *)(local_sp_1 + 16UL);
            rax_0 = var_25;
            return rax_0;
        }
    }
    _pre_phi = (uint64_t *)var_8;
    var_12 = (uint64_t *)(rdi + 120UL);
    *_pre_phi = *var_12;
    var_13 = (uint64_t *)(local_sp_2 + (-8L));
    *var_13 = 4213521UL;
    var_14 = indirect_placeholder(r12_0, local_sp_2);
    if (var_14 != 0UL) {
        var_15 = *(uint64_t *)(var_14 + 8UL);
        rax_0 = var_15;
        return rax_0;
    }
    var_16 = *var_4;
    *(uint64_t *)(local_sp_2 + (-16L)) = 4213552UL;
    indirect_placeholder_1();
    if ((uint64_t)(uint32_t)var_16 == 0UL) {
        return rax_0;
    }
    var_17 = local_sp_2 + (-24L);
    *(uint64_t *)var_17 = 4213566UL;
    var_18 = indirect_placeholder_3(16UL);
    local_sp_1 = var_17;
    if (var_18 != 0UL) {
        *(uint64_t *)var_18 = *var_12;
        *(uint64_t *)(var_18 + 8UL) = *var_13;
        var_19 = local_sp_2 + (-32L);
        *(uint64_t *)var_19 = 4213601UL;
        var_20 = indirect_placeholder(r12_0, var_18);
        local_sp_0 = var_19;
        local_sp_1 = var_19;
        if (var_20 == 0UL) {
            var_22 = local_sp_0 + (-8L);
            *(uint64_t *)var_22 = 4213624UL;
            indirect_placeholder_1();
            local_sp_1 = var_22;
        } else {
            if (var_20 != var_18) {
                var_21 = local_sp_2 + (-40L);
                *(uint64_t *)var_21 = 4213616UL;
                indirect_placeholder_1();
                local_sp_0 = var_21;
                var_22 = local_sp_0 + (-8L);
                *(uint64_t *)var_22 = 4213624UL;
                indirect_placeholder_1();
                local_sp_1 = var_22;
            }
        }
    }
    var_25 = *(uint64_t *)(local_sp_1 + 16UL);
    rax_0 = var_25;
}

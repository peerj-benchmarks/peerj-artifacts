typedef unsigned char __u_char;
typedef unsigned short __u_short;
typedef unsigned int __u_int;
typedef unsigned long __u_long;
typedef signed char __int8_t;
typedef unsigned char __uint8_t;
typedef short __int16_t;
typedef unsigned short __uint16_t;
typedef int __int32_t;
typedef unsigned int __uint32_t;
typedef long __int64_t;
typedef unsigned long __uint64_t;
typedef __int8_t __int_least8_t;
typedef __uint8_t __uint_least8_t;
typedef __int16_t __int_least16_t;
typedef __uint16_t __uint_least16_t;
typedef __int32_t __int_least32_t;
typedef __uint32_t __uint_least32_t;
typedef __int64_t __int_least64_t;
typedef __uint64_t __uint_least64_t;
typedef long __quad_t;
typedef unsigned long __u_quad_t;
typedef long __intmax_t;
typedef unsigned long __uintmax_t;
typedef unsigned long __dev_t;
typedef unsigned int __uid_t;
typedef unsigned int __gid_t;
typedef unsigned long __ino_t;
typedef unsigned long __ino64_t;
typedef unsigned int __mode_t;
typedef unsigned long __nlink_t;
typedef long __off_t;
typedef long __off64_t;
typedef int __pid_t;
typedef struct {
    int __val[2];
} __fsid_t;
typedef long __clock_t;
typedef unsigned long __rlim_t;
typedef unsigned long __rlim64_t;
typedef unsigned int __id_t;
typedef long __time_t;
typedef unsigned int __useconds_t;
typedef long __suseconds_t;
typedef int __daddr_t;
typedef int __key_t;
typedef int __clockid_t;
typedef void *__timer_t;
typedef long __blksize_t;
typedef long __blkcnt_t;
typedef long __blkcnt64_t;
typedef unsigned long __fsblkcnt_t;
typedef unsigned long __fsblkcnt64_t;
typedef unsigned long __fsfilcnt_t;
typedef unsigned long __fsfilcnt64_t;
typedef long __fsword_t;
typedef long __ssize_t;
typedef long __syscall_slong_t;
typedef unsigned long __syscall_ulong_t;
typedef __off64_t __loff_t;
typedef char *__caddr_t;
typedef long __intptr_t;
typedef unsigned int __socklen_t;
typedef int __sig_atomic_t;
typedef __int8_t int8_t;
typedef __int16_t int16_t;
typedef __int32_t int32_t;
typedef __int64_t int64_t;
typedef __uint8_t uint8_t;
typedef __uint16_t uint16_t;
typedef __uint32_t uint32_t;
typedef __uint64_t uint64_t;
typedef __int_least8_t int_least8_t;
typedef __int_least16_t int_least16_t;
typedef __int_least32_t int_least32_t;
typedef __int_least64_t int_least64_t;
typedef __uint_least8_t uint_least8_t;
typedef __uint_least16_t uint_least16_t;
typedef __uint_least32_t uint_least32_t;
typedef __uint_least64_t uint_least64_t;
typedef signed char int_fast8_t;
typedef long int_fast16_t;
typedef long int_fast32_t;
typedef long int_fast64_t;
typedef unsigned char uint_fast8_t;
typedef unsigned long uint_fast16_t;
typedef unsigned long uint_fast32_t;
typedef unsigned long uint_fast64_t;
typedef long intptr_t;
typedef unsigned long uintptr_t;
typedef __intmax_t intmax_t;
typedef __uintmax_t uintmax_t;
typedef _Bool bool;
struct indirect_placeholder_7_ret_type;
struct indirect_placeholder_11_ret_type;
struct indirect_placeholder_10_ret_type;
struct indirect_placeholder_12_ret_type;
struct indirect_placeholder_13_ret_type;
struct indirect_placeholder_6_ret_type;
struct indirect_placeholder_14_ret_type;
struct indirect_placeholder_15_ret_type;
struct indirect_placeholder_17_ret_type;
struct indirect_placeholder_7_ret_type {
    uint64_t field_0;
    uint64_t field_1;
    uint64_t field_2;
};
struct indirect_placeholder_11_ret_type {
    uint64_t field_0;
    uint64_t field_1;
};
struct indirect_placeholder_10_ret_type {
    uint64_t field_0;
    uint64_t field_1;
    uint64_t field_2;
};
struct indirect_placeholder_12_ret_type {
    uint64_t field_0;
    uint64_t field_1;
    uint64_t field_2;
};
struct indirect_placeholder_13_ret_type {
    uint64_t field_0;
    uint64_t field_1;
    uint64_t field_2;
};
struct indirect_placeholder_6_ret_type {
    uint64_t field_0;
    uint64_t field_1;
    uint64_t field_2;
};
struct indirect_placeholder_14_ret_type {
    uint64_t field_0;
    uint64_t field_1;
    uint64_t field_2;
};
struct indirect_placeholder_15_ret_type {
    uint64_t field_0;
    uint64_t field_1;
    uint64_t field_2;
    uint64_t field_3;
};
struct indirect_placeholder_17_ret_type {
    uint64_t field_0;
    uint64_t field_1;
    uint64_t field_2;
    uint64_t field_3;
};
extern void abort(void);
extern uint64_t revng_init_local_sp(uint64_t param_0);
extern uint64_t init_rbx(void);
extern void indirect_placeholder(void);
extern uint64_t init_rbp(void);
extern uint64_t init_cc_src2(void);
extern uint64_t helper_cc_compute_c_wrapper(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint32_t param_3);
extern uint64_t init_r14(void);
extern uint64_t init_r13(void);
extern uint64_t init_r12(void);
extern uint64_t indirect_placeholder_1(uint64_t param_0);
extern void indirect_placeholder_5(uint64_t param_0);
extern uint64_t indirect_placeholder_8(uint64_t param_0, uint64_t param_1);
extern void indirect_placeholder_9(uint64_t param_0, uint64_t param_1, uint64_t param_2);
extern struct indirect_placeholder_7_ret_type indirect_placeholder_7(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint64_t param_3, uint64_t param_4, uint64_t param_5, uint64_t param_6);
extern struct indirect_placeholder_11_ret_type indirect_placeholder_11(uint64_t param_0);
extern struct indirect_placeholder_10_ret_type indirect_placeholder_10(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint64_t param_3, uint64_t param_4, uint64_t param_5, uint64_t param_6);
extern struct indirect_placeholder_12_ret_type indirect_placeholder_12(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint64_t param_3, uint64_t param_4, uint64_t param_5, uint64_t param_6);
extern struct indirect_placeholder_13_ret_type indirect_placeholder_13(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint64_t param_3, uint64_t param_4, uint64_t param_5, uint64_t param_6);
extern struct indirect_placeholder_6_ret_type indirect_placeholder_6(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint64_t param_3, uint64_t param_4, uint64_t param_5, uint64_t param_6);
extern struct indirect_placeholder_14_ret_type indirect_placeholder_14(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint64_t param_3, uint64_t param_4, uint64_t param_5, uint64_t param_6);
extern struct indirect_placeholder_15_ret_type indirect_placeholder_15(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint64_t param_3, uint64_t param_4);
extern void indirect_placeholder_16(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint64_t param_3, uint64_t param_4, uint64_t param_5, uint64_t param_6);
extern struct indirect_placeholder_17_ret_type indirect_placeholder_17(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint64_t param_3, uint64_t param_4);
void bb_main(uint64_t rdi, uint64_t rsi) {
    struct indirect_placeholder_7_ret_type var_67;
    struct indirect_placeholder_17_ret_type var_11;
    struct indirect_placeholder_15_ret_type var_21;
    uint64_t rcx_0;
    uint64_t var_0;
    uint64_t var_1;
    uint64_t var_2;
    uint64_t var_3;
    uint64_t var_4;
    uint64_t var_5;
    uint64_t var_6;
    uint64_t var_7;
    uint64_t var_8;
    uint64_t var_9;
    uint64_t local_sp_10_ph;
    uint64_t rax_1;
    uint64_t r12_0_ph170;
    uint64_t var_64;
    uint64_t var_65;
    uint64_t var_66;
    uint64_t r8_2;
    uint64_t local_sp_4;
    uint64_t rcx_1;
    uint64_t var_43;
    uint64_t rax_0;
    uint64_t var_44;
    struct indirect_placeholder_11_ret_type var_45;
    uint64_t local_sp_0;
    uint64_t var_46;
    uint64_t var_47;
    uint64_t var_48;
    uint64_t var_49;
    uint64_t r8_1;
    struct indirect_placeholder_10_ret_type var_50;
    uint64_t rdx_0;
    uint64_t rbx_0;
    uint64_t r9_1;
    uint64_t r9_0;
    uint64_t r8_0;
    uint64_t var_51;
    uint32_t var_52;
    uint64_t var_53;
    uint64_t local_sp_1;
    uint32_t var_54;
    uint32_t var_38;
    uint64_t local_sp_2;
    uint64_t rdx_2;
    uint32_t var_39;
    uint64_t var_40;
    uint64_t rdx_1;
    uint64_t rbx_1;
    uint64_t var_41;
    uint64_t var_42;
    uint64_t var_32;
    uint64_t var_33;
    uint64_t var_34;
    uint32_t var_35;
    uint64_t var_36;
    uint64_t var_37;
    uint64_t rbx_2;
    uint64_t rcx_2;
    uint64_t r9_2;
    bool var_55;
    uint64_t var_56;
    bool var_57;
    uint64_t var_58;
    uint64_t var_59;
    uint64_t r13_0_ph169;
    uint64_t r12_0_ph173;
    uint64_t var_10;
    uint64_t local_sp_3;
    uint64_t rbx_3;
    uint64_t cc_src2_0;
    uint64_t var_60;
    uint64_t var_61;
    uint64_t var_62;
    uint64_t var_63;
    uint64_t rbx_4;
    uint64_t r9_3;
    uint64_t r8_3;
    uint64_t var_68;
    uint64_t var_69;
    uint64_t var_70;
    uint64_t local_sp_8_be;
    uint64_t var_26;
    uint64_t var_27;
    uint64_t var_28;
    uint64_t var_29;
    uint64_t var_30;
    uint64_t var_31;
    uint64_t local_sp_8_ph;
    uint64_t local_sp_8;
    uint64_t var_23;
    uint64_t var_24;
    uint64_t var_25;
    uint32_t var_16;
    uint64_t var_17;
    uint64_t var_18;
    uint64_t local_sp_5;
    uint64_t var_19;
    uint64_t local_sp_6;
    uint64_t r13_0_ph;
    uint64_t local_sp_7;
    uint64_t rbx_5_ph;
    uint64_t var_20;
    uint32_t var_22;
    uint64_t local_sp_10_ph172;
    uint64_t local_sp_9;
    uint64_t var_13;
    uint64_t var_14;
    uint64_t var_15;
    uint64_t local_sp_10;
    uint64_t local_sp_10_ph168;
    uint64_t r14_0_ph;
    uint64_t r12_0_ph;
    uint32_t var_12;
    unsigned int loop_state_var;
    bool switch_state_var;
    var_0 = revng_init_local_sp(0UL);
    var_1 = init_rbx();
    var_2 = init_r14();
    var_3 = init_rbp();
    var_4 = init_r13();
    var_5 = init_r12();
    var_6 = init_cc_src2();
    *(uint64_t *)(var_0 + (-8L)) = var_2;
    *(uint64_t *)(var_0 + (-16L)) = var_4;
    *(uint64_t *)(var_0 + (-24L)) = var_5;
    *(uint64_t *)(var_0 + (-32L)) = var_3;
    *(uint64_t *)(var_0 + (-40L)) = var_1;
    var_7 = (uint64_t)(uint32_t)rdi;
    var_8 = *(uint64_t *)rsi;
    *(uint64_t *)(var_0 + (-48L)) = 4201843UL;
    indirect_placeholder_5(var_8);
    *(uint64_t *)(var_0 + (-56L)) = 4201858UL;
    indirect_placeholder();
    *(uint32_t *)6355880UL = 125U;
    var_9 = var_0 + (-64L);
    *(uint64_t *)var_9 = 4201878UL;
    indirect_placeholder();
    local_sp_10_ph = var_9;
    rdx_2 = 4247149UL;
    rdx_1 = 4247149UL;
    cc_src2_0 = var_6;
    r13_0_ph = 0UL;
    rbx_5_ph = rsi;
    r14_0_ph = 0UL;
    r12_0_ph = 0UL;
    while (1U)
        {
            r14_0_ph = 1UL;
            local_sp_10_ph168 = local_sp_10_ph;
            r13_0_ph169 = r13_0_ph;
            r12_0_ph170 = r12_0_ph;
            while (1U)
                {
                    r13_0_ph = r13_0_ph169;
                    local_sp_10_ph172 = local_sp_10_ph168;
                    r12_0_ph173 = r12_0_ph170;
                    while (1U)
                        {
                            r12_0_ph = r12_0_ph173;
                            r12_0_ph170 = r12_0_ph173;
                            r12_0_ph173 = 1UL;
                            local_sp_10 = local_sp_10_ph172;
                            while (1U)
                                {
                                    var_10 = local_sp_10 + (-8L);
                                    *(uint64_t *)var_10 = 4202079UL;
                                    var_11 = indirect_placeholder_17(4247149UL, var_7, 4248256UL, rsi, 0UL);
                                    var_12 = (uint32_t)var_11.field_0;
                                    local_sp_10_ph = var_10;
                                    local_sp_5 = var_10;
                                    local_sp_10_ph172 = var_10;
                                    local_sp_9 = var_10;
                                    local_sp_10 = var_10;
                                    local_sp_10_ph168 = var_10;
                                    if ((uint64_t)(var_12 + 1U) != 0UL) {
                                        var_16 = *(uint32_t *)6355996UL;
                                        var_17 = (uint64_t)var_16;
                                        var_18 = rdi << 32UL;
                                        if ((long)var_18 <= (long)(var_17 << 32UL)) {
                                            loop_state_var = 5U;
                                            break;
                                        }
                                        var_19 = local_sp_10 + (-16L);
                                        *(uint64_t *)var_19 = 4202114UL;
                                        indirect_placeholder();
                                        local_sp_5 = var_19;
                                        local_sp_6 = var_19;
                                        if (var_16 != 0U) {
                                            loop_state_var = 5U;
                                            break;
                                        }
                                        loop_state_var = 4U;
                                        break;
                                    }
                                    if ((uint64_t)(var_12 + (-48)) != 0UL) {
                                        loop_state_var = 0U;
                                        break;
                                    }
                                    if ((int)var_12 > (int)48U) {
                                        if ((uint64_t)(var_12 + (-105)) != 0UL) {
                                            loop_state_var = 1U;
                                            break;
                                        }
                                        if ((uint64_t)(var_12 + (-117)) == 0UL) {
                                            continue;
                                        }
                                        loop_state_var = 3U;
                                        break;
                                    }
                                    if ((uint64_t)(var_12 + 131U) != 0UL) {
                                        if ((uint64_t)(var_12 + 130U) != 0UL) {
                                            loop_state_var = 2U;
                                            break;
                                        }
                                        *(uint64_t *)(local_sp_10 + (-16L)) = 4201975UL;
                                        indirect_placeholder_9(rsi, var_7, 0UL);
                                        abort();
                                    }
                                    *(uint64_t *)(local_sp_10 + (-24L)) = 0UL;
                                    var_13 = *(uint64_t *)6355872UL;
                                    var_14 = *(uint64_t *)6356032UL;
                                    *(uint64_t *)(local_sp_10 + (-32L)) = 4202027UL;
                                    indirect_placeholder_16(0UL, 4247075UL, var_14, var_13, 4246976UL, 4247116UL, 4247132UL);
                                    var_15 = local_sp_10 + (-40L);
                                    *(uint64_t *)var_15 = 4202037UL;
                                    indirect_placeholder();
                                    local_sp_9 = var_15;
                                    loop_state_var = 2U;
                                    break;
                                }
                            switch_state_var = 0;
                            switch (loop_state_var) {
                              case 0U:
                                {
                                    continue;
                                }
                                break;
                              case 5U:
                                {
                                    local_sp_6 = local_sp_5;
                                    local_sp_7 = local_sp_5;
                                    if (r14_0_ph != 0UL) {
                                        loop_state_var = 4U;
                                        switch_state_var = 1;
                                        break;
                                    }
                                    loop_state_var = 3U;
                                    switch_state_var = 1;
                                    break;
                                }
                                break;
                              case 1U:
                                {
                                    loop_state_var = 0U;
                                    switch_state_var = 1;
                                    break;
                                }
                                break;
                              case 2U:
                                {
                                    loop_state_var = 1U;
                                    switch_state_var = 1;
                                    break;
                                }
                                break;
                              case 3U:
                                {
                                    loop_state_var = 2U;
                                    switch_state_var = 1;
                                    break;
                                }
                                break;
                              case 4U:
                                {
                                    loop_state_var = 4U;
                                    switch_state_var = 1;
                                    break;
                                }
                                break;
                            }
                            if (switch_state_var)
                                break;
                        }
                    switch_state_var = 0;
                    switch (loop_state_var) {
                      case 2U:
                        {
                            if ((uint64_t)(var_12 + (-67)) != 0UL) {
                                loop_state_var = 1U;
                                switch_state_var = 1;
                                break;
                            }
                            r13_0_ph169 = *(uint64_t *)6356704UL;
                            continue;
                        }
                        break;
                      case 4U:
                        {
                            *(uint64_t *)6356096UL = 6356240UL;
                            local_sp_7 = local_sp_6;
                            loop_state_var = 2U;
                            switch_state_var = 1;
                            break;
                        }
                        break;
                      case 0U:
                        {
                            loop_state_var = 0U;
                            switch_state_var = 1;
                            break;
                        }
                        break;
                      case 1U:
                        {
                            loop_state_var = 1U;
                            switch_state_var = 1;
                            break;
                        }
                        break;
                      case 3U:
                        {
                            loop_state_var = 2U;
                            switch_state_var = 1;
                            break;
                        }
                        break;
                    }
                    if (switch_state_var)
                        break;
                }
            switch_state_var = 0;
            switch (loop_state_var) {
              case 0U:
                {
                    continue;
                }
                break;
              case 1U:
                {
                    *(uint64_t *)(local_sp_9 + (-8L)) = 4202047UL;
                    indirect_placeholder_9(rsi, var_7, 125UL);
                    abort();
                }
                break;
              case 2U:
                {
                    *(uint32_t *)6355996UL = 0U;
                    local_sp_8_ph = local_sp_7;
                    switch_state_var = 1;
                    break;
                }
                break;
            }
            if (switch_state_var)
                break;
        }
    while (1U)
        {
            rbx_1 = rbx_5_ph;
            rbx_2 = rbx_5_ph;
            local_sp_8 = local_sp_8_ph;
            while (1U)
                {
                    var_20 = local_sp_8 + (-8L);
                    *(uint64_t *)var_20 = 4202238UL;
                    var_21 = indirect_placeholder_15(4247149UL, var_7, 4248256UL, rbx_5_ph, 0UL);
                    var_22 = (uint32_t)var_21.field_0;
                    local_sp_2 = var_20;
                    local_sp_8_be = var_20;
                    if ((uint64_t)(var_22 + 1U) == 0UL) {
                        if ((uint64_t)(var_22 + (-117)) == 0UL) {
                            local_sp_8 = local_sp_8_be;
                            continue;
                        }
                        var_23 = *(uint64_t *)6356704UL;
                        var_24 = local_sp_8 + (-16L);
                        *(uint64_t *)var_24 = 4202163UL;
                        var_25 = indirect_placeholder_1(var_23);
                        local_sp_8_be = var_24;
                        if ((uint64_t)(uint32_t)var_25 != 0UL) {
                            loop_state_var = 2U;
                            break;
                        }
                    }
                    var_32 = var_21.field_1;
                    var_33 = var_21.field_2;
                    var_34 = var_21.field_3;
                    var_35 = *(uint32_t *)6355996UL;
                    var_36 = (uint64_t)var_35;
                    rax_1 = var_36;
                    r8_2 = var_34;
                    rcx_1 = var_32;
                    r8_1 = var_34;
                    r9_1 = var_33;
                    var_54 = var_35;
                    rcx_2 = var_32;
                    r9_2 = var_33;
                    if ((long)var_18 <= (long)(var_36 << 32UL)) {
                        loop_state_var = 0U;
                        break;
                    }
                    var_37 = local_sp_8 + (-16L);
                    *(uint64_t *)var_37 = 4202273UL;
                    indirect_placeholder();
                    local_sp_1 = var_37;
                    local_sp_2 = var_37;
                    if (var_35 == 0U) {
                        var_39 = *(uint32_t *)6355996UL;
                        loop_state_var = 1U;
                        break;
                    }
                    var_38 = *(uint32_t *)6355996UL + 1U;
                    *(uint32_t *)6355996UL = var_38;
                    var_39 = var_38;
                    loop_state_var = 1U;
                    break;
                }
            switch_state_var = 0;
            switch (loop_state_var) {
              case 2U:
                {
                    var_26 = var_21.field_2;
                    var_27 = var_21.field_3;
                    var_28 = *(uint64_t *)6356704UL;
                    *(uint64_t *)(local_sp_8 + (-24L)) = 4202179UL;
                    var_29 = indirect_placeholder_1(var_28);
                    *(uint64_t *)(local_sp_8 + (-32L)) = 4202187UL;
                    indirect_placeholder();
                    var_30 = (uint64_t)*(uint32_t *)var_29;
                    var_31 = local_sp_8 + (-40L);
                    *(uint64_t *)var_31 = 4202212UL;
                    indirect_placeholder_14(0UL, 4247157UL, 125UL, var_29, var_30, var_26, var_27);
                    local_sp_8_ph = var_31;
                    rbx_5_ph = var_29;
                    continue;
                }
                break;
              case 1U:
                {
                    var_40 = (uint64_t)var_39;
                    rax_0 = var_40;
                    var_54 = var_39;
                    rax_1 = var_40;
                    if ((long)var_18 <= (long)(var_40 << 32UL)) {
                        loop_state_var = 1U;
                        switch_state_var = 1;
                        break;
                    }
                    loop_state_var = 0U;
                    switch_state_var = 1;
                    break;
                }
                break;
              case 0U:
                {
                    loop_state_var = 0U;
                    switch_state_var = 1;
                    break;
                }
                break;
            }
            if (switch_state_var)
                break;
        }
    switch (loop_state_var) {
      case 1U:
        {
            while (1U)
                {
                    var_41 = (uint64_t)((long)(rax_0 << 32UL) >> (long)32UL);
                    var_42 = local_sp_1 + (-8L);
                    *(uint64_t *)var_42 = 4202404UL;
                    indirect_placeholder();
                    rax_1 = var_41;
                    r8_2 = r8_1;
                    rdx_0 = rdx_1;
                    rbx_0 = rbx_1;
                    r9_0 = r9_1;
                    r8_0 = r8_1;
                    local_sp_2 = var_42;
                    rdx_2 = rdx_1;
                    rbx_2 = rbx_1;
                    rcx_2 = rcx_1;
                    r9_2 = r9_1;
                    if (rax_0 != 0UL) {
                        var_54 = *(uint32_t *)6355996UL;
                        break;
                    }
                    var_43 = *(uint64_t *)(((uint64_t)*(uint32_t *)6355996UL << 3UL) + rbx_1);
                    var_44 = local_sp_1 + (-16L);
                    *(uint64_t *)var_44 = 4202312UL;
                    var_45 = indirect_placeholder_11(var_43);
                    local_sp_0 = var_44;
                    rcx_0 = var_45.field_1;
                    if ((uint64_t)(uint32_t)var_45.field_0 == 0UL) {
                        *(unsigned char *)var_41 = (unsigned char)'\x00';
                        var_46 = *(uint64_t *)(((uint64_t)*(uint32_t *)6355996UL << 3UL) + rbx_1);
                        *(uint64_t *)(local_sp_1 + (-24L)) = 4202336UL;
                        var_47 = indirect_placeholder_1(var_46);
                        *(uint64_t *)(local_sp_1 + (-32L)) = 4202344UL;
                        indirect_placeholder();
                        var_48 = (uint64_t)*(uint32_t *)var_47;
                        var_49 = local_sp_1 + (-40L);
                        *(uint64_t *)var_49 = 4202369UL;
                        var_50 = indirect_placeholder_10(0UL, 4247173UL, 125UL, var_47, var_48, r9_1, r8_1);
                        rcx_0 = var_50.field_0;
                        local_sp_0 = var_49;
                        rdx_0 = 4247173UL;
                        rbx_0 = var_47;
                        r9_0 = var_50.field_1;
                        r8_0 = var_50.field_2;
                    }
                    var_51 = (uint64_t)*(uint32_t *)6355996UL + 1UL;
                    var_52 = (uint32_t)var_51;
                    var_53 = (uint64_t)var_52;
                    *(uint32_t *)6355996UL = var_52;
                    rax_1 = var_53;
                    r8_2 = r8_0;
                    rcx_1 = rcx_0;
                    rax_0 = var_53;
                    r8_1 = r8_0;
                    r9_1 = r9_0;
                    local_sp_1 = local_sp_0;
                    var_54 = var_52;
                    local_sp_2 = local_sp_0;
                    rdx_2 = rdx_0;
                    rdx_1 = rdx_0;
                    rbx_1 = rbx_0;
                    rbx_2 = rbx_0;
                    rcx_2 = rcx_0;
                    r9_2 = r9_0;
                    if ((long)var_18 <= (long)(var_51 << 32UL)) {
                        continue;
                    }
                    break;
                }
        }
        break;
      case 0U:
        {
            var_55 = ((long)(uint64_t)((long)var_18 >> (long)32UL) > (long)(uint64_t)var_54);
            var_56 = (rax_1 & (-256L)) | var_55;
            local_sp_4 = local_sp_2;
            local_sp_3 = local_sp_2;
            rbx_4 = rbx_2;
            r9_3 = r9_2;
            r8_3 = r8_2;
            if (((uint64_t)(unsigned char)r12_0_ph173 == 0UL) || (var_55 ^ 1)) {
                *(uint64_t *)(local_sp_2 + (-8L)) = 4202455UL;
                indirect_placeholder_12(0UL, 4248144UL, 0UL, rcx_2, 0UL, r9_2, r8_2);
                *(uint64_t *)(local_sp_2 + (-16L)) = 4202465UL;
                indirect_placeholder_9(rbx_2, var_7, 125UL);
                abort();
            }
            var_57 = (r13_0_ph169 != 0UL);
            var_58 = (rdx_2 & (-256L)) | var_57;
            var_59 = helper_cc_compute_c_wrapper(var_56 - var_58, var_58, var_6, 14U);
            if (var_59 == 0UL) {
                *(uint64_t *)(local_sp_2 + (-8L)) = 4202500UL;
                indirect_placeholder_13(0UL, 4248184UL, 0UL, rcx_2, 0UL, r9_2, r8_2);
                *(uint64_t *)(local_sp_2 + (-16L)) = 4202510UL;
                indirect_placeholder_9(rbx_2, var_7, 125UL);
                abort();
            }
            if (!var_55) {
                rbx_3 = *(uint64_t *)6356096UL;
                while (*(uint64_t *)rbx_3 != 0UL)
                    {
                        var_60 = helper_cc_compute_c_wrapper(r12_0_ph173 + (-1L), 1UL, cc_src2_0, 14U);
                        var_61 = rbx_3 + 8UL;
                        var_62 = local_sp_3 + (-8L);
                        *(uint64_t *)var_62 = 4202551UL;
                        indirect_placeholder();
                        local_sp_3 = var_62;
                        rbx_3 = var_61;
                        cc_src2_0 = var_60;
                    }
            }
            var_63 = local_sp_2 + (-8L);
            *(uint64_t *)var_63 = 4202581UL;
            indirect_placeholder();
            local_sp_4 = var_63;
            if (!var_57 && (uint64_t)((uint32_t)var_56 & (-255)) == 0UL) {
                *(uint64_t *)(local_sp_2 + (-16L)) = 4202598UL;
                var_64 = indirect_placeholder_8(4UL, r13_0_ph169);
                *(uint64_t *)(local_sp_2 + (-24L)) = 4202606UL;
                indirect_placeholder();
                var_65 = (uint64_t)*(uint32_t *)var_64;
                var_66 = local_sp_2 + (-32L);
                *(uint64_t *)var_66 = 4202631UL;
                var_67 = indirect_placeholder_7(0UL, 4247192UL, 125UL, var_64, var_65, r9_2, r8_2);
                local_sp_4 = var_66;
                rbx_4 = var_64;
                r9_3 = var_67.field_1;
                r8_3 = var_67.field_2;
            }
            *(uint64_t *)(local_sp_4 + (-8L)) = 4202653UL;
            indirect_placeholder();
            *(uint64_t *)(local_sp_4 + (-16L)) = 4202658UL;
            indirect_placeholder();
            var_68 = *(uint64_t *)(((uint64_t)*(uint32_t *)6355996UL << 3UL) + rbx_4);
            *(uint64_t *)(local_sp_4 + (-24L)) = 4202688UL;
            var_69 = indirect_placeholder_1(var_68);
            *(uint64_t *)(local_sp_4 + (-32L)) = 4202696UL;
            indirect_placeholder();
            var_70 = (uint64_t)*(uint32_t *)var_69;
            *(uint64_t *)(local_sp_4 + (-40L)) = 4202721UL;
            indirect_placeholder_6(0UL, 4248501UL, 0UL, var_69, var_70, r9_3, r8_3);
            return;
        }
        break;
    }
}

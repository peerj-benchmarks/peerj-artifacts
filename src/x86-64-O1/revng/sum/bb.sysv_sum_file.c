typedef unsigned char __u_char;
typedef unsigned short __u_short;
typedef unsigned int __u_int;
typedef unsigned long __u_long;
typedef signed char __int8_t;
typedef unsigned char __uint8_t;
typedef short __int16_t;
typedef unsigned short __uint16_t;
typedef int __int32_t;
typedef unsigned int __uint32_t;
typedef long __int64_t;
typedef unsigned long __uint64_t;
typedef __int8_t __int_least8_t;
typedef __uint8_t __uint_least8_t;
typedef __int16_t __int_least16_t;
typedef __uint16_t __uint_least16_t;
typedef __int32_t __int_least32_t;
typedef __uint32_t __uint_least32_t;
typedef __int64_t __int_least64_t;
typedef __uint64_t __uint_least64_t;
typedef long __quad_t;
typedef unsigned long __u_quad_t;
typedef long __intmax_t;
typedef unsigned long __uintmax_t;
typedef unsigned long __dev_t;
typedef unsigned int __uid_t;
typedef unsigned int __gid_t;
typedef unsigned long __ino_t;
typedef unsigned long __ino64_t;
typedef unsigned int __mode_t;
typedef unsigned long __nlink_t;
typedef long __off_t;
typedef long __off64_t;
typedef int __pid_t;
typedef struct {
    int __val[2];
} __fsid_t;
typedef long __clock_t;
typedef unsigned long __rlim_t;
typedef unsigned long __rlim64_t;
typedef unsigned int __id_t;
typedef long __time_t;
typedef unsigned int __useconds_t;
typedef long __suseconds_t;
typedef int __daddr_t;
typedef int __key_t;
typedef int __clockid_t;
typedef void *__timer_t;
typedef long __blksize_t;
typedef long __blkcnt_t;
typedef long __blkcnt64_t;
typedef unsigned long __fsblkcnt_t;
typedef unsigned long __fsblkcnt64_t;
typedef unsigned long __fsfilcnt_t;
typedef unsigned long __fsfilcnt64_t;
typedef long __fsword_t;
typedef long __ssize_t;
typedef long __syscall_slong_t;
typedef unsigned long __syscall_ulong_t;
typedef __off64_t __loff_t;
typedef char *__caddr_t;
typedef long __intptr_t;
typedef unsigned int __socklen_t;
typedef int __sig_atomic_t;
typedef __int8_t int8_t;
typedef __int16_t int16_t;
typedef __int32_t int32_t;
typedef __int64_t int64_t;
typedef __uint8_t uint8_t;
typedef __uint16_t uint16_t;
typedef __uint32_t uint32_t;
typedef __uint64_t uint64_t;
typedef __int_least8_t int_least8_t;
typedef __int_least16_t int_least16_t;
typedef __int_least32_t int_least32_t;
typedef __int_least64_t int_least64_t;
typedef __uint_least8_t uint_least8_t;
typedef __uint_least16_t uint_least16_t;
typedef __uint_least32_t uint_least32_t;
typedef __uint_least64_t uint_least64_t;
typedef signed char int_fast8_t;
typedef long int_fast16_t;
typedef long int_fast32_t;
typedef long int_fast64_t;
typedef unsigned char uint_fast8_t;
typedef unsigned long uint_fast16_t;
typedef unsigned long uint_fast32_t;
typedef unsigned long uint_fast64_t;
typedef long intptr_t;
typedef unsigned long uintptr_t;
typedef __intmax_t intmax_t;
typedef __uintmax_t uintmax_t;
typedef _Bool bool;
extern uint64_t revng_init_local_sp(uint64_t param_0);
extern uint64_t init_rbx(void);
extern void indirect_placeholder(void);
extern uint64_t init_rbp(void);
extern uint64_t init_r14(void);
extern uint64_t init_r13(void);
extern uint64_t init_r12(void);
extern uint64_t indirect_placeholder_3(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint64_t param_3, uint64_t param_4);
extern uint64_t init_r15(void);
extern uint64_t init_rax(void);
extern uint64_t indirect_placeholder_2(uint64_t param_0, uint64_t param_1, uint64_t param_2);
extern uint64_t init_r8(void);
extern uint64_t init_r9(void);
extern void indirect_placeholder_1(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint64_t param_3, uint64_t param_4, uint64_t param_5, uint64_t param_6);
void bb_sysv_sum_file(uint64_t rdi, uint64_t rsi) {
    uint64_t var_0;
    uint64_t var_1;
    uint64_t var_2;
    uint64_t var_3;
    uint64_t var_4;
    uint64_t var_5;
    uint64_t var_6;
    uint64_t var_7;
    uint64_t var_8;
    uint64_t var_9;
    uint64_t var_10;
    uint64_t var_11;
    bool var_12;
    uint64_t var_21;
    uint64_t local_sp_1;
    uint64_t local_sp_0;
    uint64_t local_sp_2;
    uint64_t var_17;
    uint64_t var_18;
    uint64_t var_14;
    uint64_t var_22;
    uint64_t var_23;
    uint64_t rdx_0;
    uint64_t rbp_0;
    uint64_t rbx_0;
    uint64_t var_24;
    uint64_t var_25;
    uint64_t local_sp_3;
    uint64_t rbx_1;
    uint64_t var_19;
    uint64_t var_20;
    uint64_t var_15;
    uint64_t var_16;
    uint64_t var_13;
    unsigned int loop_state_var;
    bool switch_state_var;
    var_0 = revng_init_local_sp(0UL);
    var_1 = init_rax();
    var_2 = init_rbx();
    var_3 = init_r14();
    var_4 = init_rbp();
    var_5 = init_r13();
    var_6 = init_r12();
    var_7 = init_r15();
    var_8 = init_r9();
    var_9 = init_r8();
    *(uint64_t *)(var_0 + (-8L)) = var_7;
    *(uint64_t *)(var_0 + (-16L)) = var_3;
    *(uint64_t *)(var_0 + (-24L)) = var_5;
    *(uint64_t *)(var_0 + (-32L)) = var_6;
    *(uint64_t *)(var_0 + (-40L)) = var_4;
    *(uint64_t *)(var_0 + (-48L)) = var_2;
    var_10 = (uint64_t)(uint32_t)rsi;
    var_11 = var_0 + (-8464L);
    *(uint64_t *)var_11 = 4201487UL;
    indirect_placeholder();
    var_12 = ((uint64_t)(uint32_t)var_1 == 0UL);
    local_sp_2 = var_11;
    rbx_1 = 0UL;
    rbp_0 = 0UL;
    if (var_12) {
        *(unsigned char *)6358480UL = (unsigned char)'\x01';
    } else {
        var_13 = var_0 + (-8472L);
        *(uint64_t *)var_13 = 4201538UL;
        indirect_placeholder();
        local_sp_2 = var_13;
    }
    local_sp_3 = local_sp_2;
    while (1U)
        {
            var_14 = local_sp_3 + 208UL;
            var_15 = local_sp_3 + (-8L);
            *(uint64_t *)var_15 = 4201628UL;
            var_16 = indirect_placeholder_2(8192UL, 0UL, var_14);
            rbx_0 = rbx_1;
            local_sp_1 = var_15;
            local_sp_3 = var_15;
            switch_state_var = 0;
            switch (var_16) {
              case 0UL:
                {
                    if (!var_12) {
                        loop_state_var = 1U;
                        switch_state_var = 1;
                        break;
                    }
                    var_19 = local_sp_3 + (-16L);
                    *(uint64_t *)var_19 = 4201769UL;
                    indirect_placeholder();
                    local_sp_1 = var_19;
                    loop_state_var = 1U;
                    switch_state_var = 1;
                    break;
                }
                break;
              case 18446744073709551615UL:
                {
                    *(uint64_t *)(local_sp_3 + (-16L)) = 4201657UL;
                    var_17 = indirect_placeholder_2(rdi, 0UL, 3UL);
                    *(uint64_t *)(local_sp_3 + (-24L)) = 4201665UL;
                    indirect_placeholder();
                    var_18 = (uint64_t)*(uint32_t *)var_17;
                    *(uint64_t *)(local_sp_3 + (-32L)) = 4201690UL;
                    indirect_placeholder_1(0UL, 4250453UL, 0UL, var_17, var_18, var_8, var_9);
                    if (!var_12) {
                        loop_state_var = 0U;
                        switch_state_var = 1;
                        break;
                    }
                    *(uint64_t *)(local_sp_3 + (-40L)) = 4201712UL;
                    indirect_placeholder();
                    loop_state_var = 0U;
                    switch_state_var = 1;
                    break;
                }
                break;
              default:
                {
                    var_22 = local_sp_3 + 200UL;
                    var_23 = var_16 + var_22;
                    rdx_0 = var_22;
                    var_24 = (uint64_t)((uint32_t)rbx_0 + (uint32_t)(uint64_t)*(unsigned char *)rdx_0);
                    var_25 = rdx_0 + 1UL;
                    rdx_0 = var_25;
                    rbx_0 = var_24;
                    rbx_1 = var_24;
                    do {
                        var_24 = (uint64_t)((uint32_t)rbx_0 + (uint32_t)(uint64_t)*(unsigned char *)rdx_0);
                        var_25 = rdx_0 + 1UL;
                        rdx_0 = var_25;
                        rbx_0 = var_24;
                        rbx_1 = var_24;
                    } while (var_25 != var_23);
                    rbp_0 = rbp_0 + var_16;
                    continue;
                }
                break;
            }
            if (switch_state_var)
                break;
        }
    switch (loop_state_var) {
      case 0U:
        {
            return;
        }
        break;
      case 1U:
        {
            *(uint64_t *)(local_sp_1 + (-8L)) = 4201876UL;
            indirect_placeholder_3(0UL, rbp_0, 1UL, local_sp_1, 512UL);
            var_20 = local_sp_1 + (-16L);
            *(uint64_t *)var_20 = 4201896UL;
            indirect_placeholder();
            local_sp_0 = var_20;
            if (var_10 != 0UL) {
                var_21 = local_sp_1 + (-24L);
                *(uint64_t *)var_21 = 4201919UL;
                indirect_placeholder();
                local_sp_0 = var_21;
            }
            *(uint64_t *)(local_sp_0 + (-8L)) = 4201929UL;
            indirect_placeholder();
        }
        break;
    }
}

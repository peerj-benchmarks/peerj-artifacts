typedef unsigned char __u_char;
typedef unsigned short __u_short;
typedef unsigned int __u_int;
typedef unsigned long __u_long;
typedef signed char __int8_t;
typedef unsigned char __uint8_t;
typedef short __int16_t;
typedef unsigned short __uint16_t;
typedef int __int32_t;
typedef unsigned int __uint32_t;
typedef long __int64_t;
typedef unsigned long __uint64_t;
typedef __int8_t __int_least8_t;
typedef __uint8_t __uint_least8_t;
typedef __int16_t __int_least16_t;
typedef __uint16_t __uint_least16_t;
typedef __int32_t __int_least32_t;
typedef __uint32_t __uint_least32_t;
typedef __int64_t __int_least64_t;
typedef __uint64_t __uint_least64_t;
typedef long __quad_t;
typedef unsigned long __u_quad_t;
typedef long __intmax_t;
typedef unsigned long __uintmax_t;
typedef unsigned long __dev_t;
typedef unsigned int __uid_t;
typedef unsigned int __gid_t;
typedef unsigned long __ino_t;
typedef unsigned long __ino64_t;
typedef unsigned int __mode_t;
typedef unsigned long __nlink_t;
typedef long __off_t;
typedef long __off64_t;
typedef int __pid_t;
typedef struct {
    int __val[2];
} __fsid_t;
typedef long __clock_t;
typedef unsigned long __rlim_t;
typedef unsigned long __rlim64_t;
typedef unsigned int __id_t;
typedef long __time_t;
typedef unsigned int __useconds_t;
typedef long __suseconds_t;
typedef int __daddr_t;
typedef int __key_t;
typedef int __clockid_t;
typedef void *__timer_t;
typedef long __blksize_t;
typedef long __blkcnt_t;
typedef long __blkcnt64_t;
typedef unsigned long __fsblkcnt_t;
typedef unsigned long __fsblkcnt64_t;
typedef unsigned long __fsfilcnt_t;
typedef unsigned long __fsfilcnt64_t;
typedef long __fsword_t;
typedef long __ssize_t;
typedef long __syscall_slong_t;
typedef unsigned long __syscall_ulong_t;
typedef __off64_t __loff_t;
typedef char *__caddr_t;
typedef long __intptr_t;
typedef unsigned int __socklen_t;
typedef int __sig_atomic_t;
typedef __int8_t int8_t;
typedef __int16_t int16_t;
typedef __int32_t int32_t;
typedef __int64_t int64_t;
typedef __uint8_t uint8_t;
typedef __uint16_t uint16_t;
typedef __uint32_t uint32_t;
typedef __uint64_t uint64_t;
typedef __int_least8_t int_least8_t;
typedef __int_least16_t int_least16_t;
typedef __int_least32_t int_least32_t;
typedef __int_least64_t int_least64_t;
typedef __uint_least8_t uint_least8_t;
typedef __uint_least16_t uint_least16_t;
typedef __uint_least32_t uint_least32_t;
typedef __uint_least64_t uint_least64_t;
typedef signed char int_fast8_t;
typedef long int_fast16_t;
typedef long int_fast32_t;
typedef long int_fast64_t;
typedef unsigned char uint_fast8_t;
typedef unsigned long uint_fast16_t;
typedef unsigned long uint_fast32_t;
typedef unsigned long uint_fast64_t;
typedef long intptr_t;
typedef unsigned long uintptr_t;
typedef __intmax_t intmax_t;
typedef __uintmax_t uintmax_t;
extern void abort(void);
extern uint64_t revng_init_local_sp(uint64_t param_0);
extern uint64_t init_rbx(void);
extern void indirect_placeholder(void);
extern uint64_t init_rbp(void);
extern uint64_t init_r14(void);
extern uint64_t init_r13(void);
extern uint64_t init_r12(void);
extern uint64_t indirect_placeholder_7(uint64_t param_0);
extern uint64_t helper_cc_compute_all_wrapper(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint32_t param_3);
extern uint64_t indirect_placeholder_3(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint64_t param_3, uint64_t param_4);
extern void indirect_placeholder_9(uint64_t param_0);
extern uint64_t indirect_placeholder_2(uint64_t param_0, uint64_t param_1, uint64_t param_2);
extern uint64_t init_r9(void);
extern void indirect_placeholder_1(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint64_t param_3, uint64_t param_4, uint64_t param_5, uint64_t param_6);
extern void indirect_placeholder_8(uint64_t param_0, uint64_t param_1, uint64_t param_2);
void bb_main(uint64_t rdi, uint64_t rsi) {
    uint64_t var_12;
    uint64_t var_0;
    uint64_t var_1;
    uint64_t var_2;
    uint64_t var_3;
    uint64_t var_4;
    uint64_t var_5;
    uint64_t var_6;
    uint32_t var_7;
    uint64_t var_8;
    uint64_t var_9;
    uint64_t var_10;
    uint64_t local_sp_3;
    uint64_t var_22;
    uint64_t var_23;
    uint64_t var_17;
    uint64_t var_18;
    uint64_t local_sp_1;
    uint32_t var_19;
    uint64_t local_sp_0;
    uint64_t var_20;
    uint64_t var_21;
    uint64_t var_24;
    uint32_t var_25;
    uint64_t local_sp_2;
    uint64_t var_14;
    uint64_t var_15;
    uint64_t var_16;
    uint64_t var_11;
    uint32_t var_13;
    unsigned int loop_state_var;
    var_0 = revng_init_local_sp(0UL);
    var_1 = init_rbx();
    var_2 = init_r14();
    var_3 = init_rbp();
    var_4 = init_r13();
    var_5 = init_r12();
    var_6 = init_r9();
    *(uint64_t *)(var_0 + (-8L)) = var_2;
    *(uint64_t *)(var_0 + (-16L)) = var_4;
    *(uint64_t *)(var_0 + (-24L)) = var_5;
    *(uint64_t *)(var_0 + (-32L)) = var_3;
    *(uint64_t *)(var_0 + (-40L)) = var_1;
    var_7 = (uint32_t)rdi;
    var_8 = (uint64_t)var_7;
    var_9 = *(uint64_t *)rsi;
    *(uint64_t *)(var_0 + (-48L)) = 4203076UL;
    indirect_placeholder_9(var_9);
    *(uint64_t *)(var_0 + (-56L)) = 4203091UL;
    indirect_placeholder();
    *(uint64_t *)(var_0 + (-64L)) = 4203101UL;
    indirect_placeholder();
    var_10 = var_0 + (-72L);
    *(uint64_t *)var_10 = 4203128UL;
    indirect_placeholder();
    *(unsigned char *)6358480UL = (unsigned char)'\x00';
    local_sp_3 = var_10;
    while (1U)
        {
            var_11 = local_sp_3 + (-8L);
            *(uint64_t *)var_11 = 4203301UL;
            var_12 = indirect_placeholder_3(4249657UL, var_8, 4250304UL, rsi, 0UL);
            var_13 = (uint32_t)var_12;
            local_sp_0 = var_11;
            local_sp_2 = var_11;
            local_sp_3 = var_11;
            if ((uint64_t)(var_13 + 1U) != 0UL) {
                var_17 = helper_cc_compute_all_wrapper((uint64_t)(var_7 - *(uint32_t *)6358300UL), 0UL, 0UL, 24U);
                if ((uint64_t)(((unsigned char)(var_17 >> 4UL) ^ (unsigned char)var_17) & '\xc0') != 0UL) {
                    var_19 = *(uint32_t *)6358300UL;
                    loop_state_var = 1U;
                    break;
                }
                var_18 = local_sp_3 + (-16L);
                *(uint64_t *)var_18 = 4203342UL;
                indirect_placeholder();
                local_sp_1 = var_18;
                loop_state_var = 2U;
                break;
            }
            if ((uint64_t)(var_13 + 130U) == 0UL) {
                *(uint64_t *)(local_sp_3 + (-16L)) = 4203197UL;
                indirect_placeholder_8(rsi, var_8, 0UL);
                abort();
            }
            if ((int)var_13 > (int)4294967166U) {
                if ((uint64_t)(var_13 + (-114)) != 0UL) {
                    continue;
                }
                if ((uint64_t)(var_13 + (-115)) == 0UL) {
                    continue;
                }
                loop_state_var = 0U;
                break;
            }
            if ((uint64_t)(var_13 + 131U) != 0UL) {
                loop_state_var = 0U;
                break;
            }
            *(uint64_t *)(local_sp_3 + (-24L)) = 0UL;
            var_14 = *(uint64_t *)6358176UL;
            var_15 = *(uint64_t *)6358336UL;
            *(uint64_t *)(local_sp_3 + (-32L)) = 4203249UL;
            indirect_placeholder_1(0UL, 4249582UL, var_15, var_14, 4249578UL, 4249623UL, 4249639UL);
            var_16 = local_sp_3 + (-40L);
            *(uint64_t *)var_16 = 4203259UL;
            indirect_placeholder();
            local_sp_2 = var_16;
            loop_state_var = 0U;
            break;
        }
    switch (loop_state_var) {
      case 0U:
        {
            *(uint64_t *)(local_sp_2 + (-8L)) = 4203269UL;
            indirect_placeholder_8(rsi, var_8, 1UL);
            abort();
        }
        break;
      case 2U:
      case 1U:
        {
            switch (loop_state_var) {
              case 1U:
                {
                    local_sp_1 = local_sp_0;
                    while ((long)(rdi << 32UL) <= (long)((uint64_t)var_19 << 32UL))
                        {
                            var_24 = local_sp_0 + (-8L);
                            *(uint64_t *)var_24 = 4203360UL;
                            indirect_placeholder();
                            var_25 = *(uint32_t *)6358300UL + 1U;
                            *(uint32_t *)6358300UL = var_25;
                            var_19 = var_25;
                            local_sp_0 = var_24;
                            local_sp_1 = local_sp_0;
                        }
                }
                break;
              case 2U:
                {
                    var_20 = *(uint64_t *)6358344UL;
                    *(uint64_t *)(local_sp_1 + (-8L)) = 4203401UL;
                    var_21 = indirect_placeholder_7(var_20);
                    if (*(unsigned char *)6358480UL != '\x00' & (uint64_t)((uint32_t)var_21 + 1U) == 0UL) {
                        *(uint64_t *)(local_sp_1 + (-16L)) = 4203426UL;
                        var_22 = indirect_placeholder_2(4253858UL, 0UL, 3UL);
                        *(uint64_t *)(local_sp_1 + (-24L)) = 4203434UL;
                        indirect_placeholder();
                        var_23 = (uint64_t)*(uint32_t *)var_22;
                        *(uint64_t *)(local_sp_1 + (-32L)) = 4203459UL;
                        indirect_placeholder_1(0UL, 4250453UL, 1UL, var_22, var_23, var_6, 0UL);
                    }
                    return;
                }
                break;
            }
        }
        break;
    }
}

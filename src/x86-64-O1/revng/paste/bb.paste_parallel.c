typedef unsigned char __u_char;
typedef unsigned short __u_short;
typedef unsigned int __u_int;
typedef unsigned long __u_long;
typedef signed char __int8_t;
typedef unsigned char __uint8_t;
typedef short __int16_t;
typedef unsigned short __uint16_t;
typedef int __int32_t;
typedef unsigned int __uint32_t;
typedef long __int64_t;
typedef unsigned long __uint64_t;
typedef __int8_t __int_least8_t;
typedef __uint8_t __uint_least8_t;
typedef __int16_t __int_least16_t;
typedef __uint16_t __uint_least16_t;
typedef __int32_t __int_least32_t;
typedef __uint32_t __uint_least32_t;
typedef __int64_t __int_least64_t;
typedef __uint64_t __uint_least64_t;
typedef long __quad_t;
typedef unsigned long __u_quad_t;
typedef long __intmax_t;
typedef unsigned long __uintmax_t;
typedef unsigned long __dev_t;
typedef unsigned int __uid_t;
typedef unsigned int __gid_t;
typedef unsigned long __ino_t;
typedef unsigned long __ino64_t;
typedef unsigned int __mode_t;
typedef unsigned long __nlink_t;
typedef long __off_t;
typedef long __off64_t;
typedef int __pid_t;
typedef struct {
    int __val[2];
} __fsid_t;
typedef long __clock_t;
typedef unsigned long __rlim_t;
typedef unsigned long __rlim64_t;
typedef unsigned int __id_t;
typedef long __time_t;
typedef unsigned int __useconds_t;
typedef long __suseconds_t;
typedef int __daddr_t;
typedef int __key_t;
typedef int __clockid_t;
typedef void *__timer_t;
typedef long __blksize_t;
typedef long __blkcnt_t;
typedef long __blkcnt64_t;
typedef unsigned long __fsblkcnt_t;
typedef unsigned long __fsblkcnt64_t;
typedef unsigned long __fsfilcnt_t;
typedef unsigned long __fsfilcnt64_t;
typedef long __fsword_t;
typedef long __ssize_t;
typedef long __syscall_slong_t;
typedef unsigned long __syscall_ulong_t;
typedef __off64_t __loff_t;
typedef char *__caddr_t;
typedef long __intptr_t;
typedef unsigned int __socklen_t;
typedef int __sig_atomic_t;
typedef __int8_t int8_t;
typedef __int16_t int16_t;
typedef __int32_t int32_t;
typedef __int64_t int64_t;
typedef __uint8_t uint8_t;
typedef __uint16_t uint16_t;
typedef __uint32_t uint32_t;
typedef __uint64_t uint64_t;
typedef __int_least8_t int_least8_t;
typedef __int_least16_t int_least16_t;
typedef __int_least32_t int_least32_t;
typedef __int_least64_t int_least64_t;
typedef __uint_least8_t uint_least8_t;
typedef __uint_least16_t uint_least16_t;
typedef __uint_least32_t uint_least32_t;
typedef __uint_least64_t uint_least64_t;
typedef signed char int_fast8_t;
typedef long int_fast16_t;
typedef long int_fast32_t;
typedef long int_fast64_t;
typedef unsigned char uint_fast8_t;
typedef unsigned long uint_fast16_t;
typedef unsigned long uint_fast32_t;
typedef unsigned long uint_fast64_t;
typedef long intptr_t;
typedef unsigned long uintptr_t;
typedef __intmax_t intmax_t;
typedef __uintmax_t uintmax_t;
typedef _Bool bool;
struct indirect_placeholder_46_ret_type;
struct indirect_placeholder_39_ret_type;
struct indirect_placeholder_41_ret_type;
struct indirect_placeholder_40_ret_type;
struct indirect_placeholder_43_ret_type;
struct indirect_placeholder_42_ret_type;
struct indirect_placeholder_45_ret_type;
struct indirect_placeholder_44_ret_type;
struct indirect_placeholder_47_ret_type;
struct indirect_placeholder_48_ret_type;
struct indirect_placeholder_46_ret_type {
    uint64_t field_0;
    uint64_t field_1;
    uint64_t field_2;
};
struct indirect_placeholder_39_ret_type {
    uint64_t field_0;
    uint64_t field_1;
    uint64_t field_2;
};
struct indirect_placeholder_41_ret_type {
    uint64_t field_0;
    uint64_t field_1;
    uint64_t field_2;
};
struct indirect_placeholder_40_ret_type {
    uint64_t field_0;
    uint64_t field_1;
};
struct indirect_placeholder_43_ret_type {
    uint64_t field_0;
    uint64_t field_1;
    uint64_t field_2;
};
struct indirect_placeholder_42_ret_type {
    uint64_t field_0;
    uint64_t field_1;
};
struct indirect_placeholder_45_ret_type {
    uint64_t field_0;
    uint64_t field_1;
};
struct indirect_placeholder_44_ret_type {
    uint64_t field_0;
    uint64_t field_1;
};
struct indirect_placeholder_47_ret_type {
    uint64_t field_0;
    uint64_t field_1;
};
struct indirect_placeholder_48_ret_type {
    uint64_t field_0;
    uint64_t field_1;
    uint64_t field_2;
};
extern void abort(void);
extern uint64_t revng_init_local_sp(uint64_t param_0);
extern uint64_t init_rbx(void);
extern void indirect_placeholder(void);
extern uint64_t init_rbp(void);
extern uint64_t init_r14(void);
extern uint64_t init_r13(void);
extern uint64_t init_r12(void);
extern uint64_t indirect_placeholder_3(uint64_t param_0);
extern uint64_t init_r15(void);
extern void indirect_placeholder_24(uint64_t param_0, uint64_t param_1);
extern struct indirect_placeholder_46_ret_type indirect_placeholder_46(uint64_t param_0, uint64_t param_1, uint64_t param_2);
extern uint64_t init_rcx(void);
extern struct indirect_placeholder_39_ret_type indirect_placeholder_39(uint64_t param_0);
extern struct indirect_placeholder_41_ret_type indirect_placeholder_41(uint64_t param_0, uint64_t param_1, uint64_t param_2);
extern struct indirect_placeholder_40_ret_type indirect_placeholder_40(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint64_t param_3, uint64_t param_4, uint64_t param_5, uint64_t param_6);
extern struct indirect_placeholder_43_ret_type indirect_placeholder_43(uint64_t param_0, uint64_t param_1, uint64_t param_2);
extern struct indirect_placeholder_42_ret_type indirect_placeholder_42(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint64_t param_3, uint64_t param_4, uint64_t param_5, uint64_t param_6);
extern struct indirect_placeholder_45_ret_type indirect_placeholder_45(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint64_t param_3, uint64_t param_4, uint64_t param_5, uint64_t param_6);
extern struct indirect_placeholder_44_ret_type indirect_placeholder_44(uint64_t param_0, uint64_t param_1, uint64_t param_2);
extern struct indirect_placeholder_47_ret_type indirect_placeholder_47(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint64_t param_3, uint64_t param_4, uint64_t param_5, uint64_t param_6);
extern struct indirect_placeholder_48_ret_type indirect_placeholder_48(uint64_t param_0);
void bb_paste_parallel(uint64_t rdi, uint64_t rsi) {
    uint64_t rax_14;
    uint64_t var_21;
    struct indirect_placeholder_46_ret_type var_23;
    uint64_t var_24;
    uint64_t var_25;
    uint64_t var_26;
    uint64_t var_16;
    uint64_t local_sp_0;
    uint64_t var_0;
    uint64_t var_1;
    uint64_t var_2;
    uint64_t var_3;
    uint64_t var_4;
    uint64_t var_5;
    uint64_t var_6;
    uint64_t var_7;
    struct indirect_placeholder_40_ret_type var_78;
    uint64_t var_8;
    struct indirect_placeholder_39_ret_type var_9;
    uint64_t var_10;
    uint64_t rax_2;
    uint64_t rdx_10;
    uint64_t rax_10;
    uint64_t rax_1;
    uint64_t rdx_9;
    uint64_t var_71;
    struct indirect_placeholder_41_ret_type var_72;
    uint64_t var_73;
    uint64_t var_74;
    uint64_t var_75;
    uint64_t var_76;
    uint64_t var_77;
    uint64_t var_79;
    uint64_t rax_0;
    uint64_t rdx_0;
    uint64_t *var_80;
    uint64_t local_sp_12;
    uint64_t var_57;
    struct indirect_placeholder_43_ret_type var_58;
    uint64_t var_59;
    uint64_t var_60;
    uint64_t var_61;
    uint64_t var_62;
    uint64_t var_63;
    struct indirect_placeholder_42_ret_type var_64;
    uint64_t var_65;
    uint64_t local_sp_1;
    uint64_t rdx_1;
    uint64_t var_66;
    bool var_67;
    uint64_t var_68;
    uint64_t *var_69;
    uint64_t var_70;
    uint64_t rax_9;
    uint64_t var_22;
    uint64_t var_46;
    uint64_t var_47;
    uint64_t *var_48;
    uint64_t local_sp_2;
    uint64_t rdx_2;
    uint64_t var_49;
    uint64_t local_sp_3;
    uint64_t local_sp_5;
    uint64_t r12_2;
    uint64_t var_55;
    uint64_t local_sp_15;
    uint64_t local_sp_4;
    uint64_t var_52;
    uint64_t var_82;
    uint64_t *_cast3;
    uint64_t var_53;
    uint64_t local_sp_14;
    uint64_t var_54;
    uint64_t rax_3;
    uint64_t var_50;
    uint64_t var_51;
    uint64_t var_56;
    uint64_t rdx_8;
    uint64_t r13_0;
    uint64_t r15_0;
    uint64_t *_cast1;
    uint64_t var_27;
    uint64_t var_28;
    struct indirect_placeholder_45_ret_type var_29;
    uint64_t *_pre_phi223;
    uint64_t _pre_phi217;
    uint64_t local_sp_6;
    uint64_t rbx_4;
    uint64_t rdx_3;
    uint64_t r14_1;
    uint64_t rbx_2;
    uint64_t rcx_1;
    uint64_t r12_0;
    uint64_t r8_2;
    uint64_t r9_0;
    uint64_t r8_0;
    uint64_t rbp_0;
    uint64_t var_30;
    uint64_t var_31;
    uint64_t var_32;
    struct indirect_placeholder_44_ret_type var_33;
    uint64_t local_sp_7;
    uint64_t rdx_13;
    uint64_t rax_5;
    uint64_t rdx_4;
    uint64_t rbx_3;
    uint64_t r14_0;
    uint64_t r12_1;
    uint64_t r9_2;
    uint64_t rcx_0;
    uint64_t r9_1;
    uint64_t r8_1;
    uint64_t var_34;
    uint64_t r12_3;
    uint64_t local_sp_8;
    uint64_t var_35;
    struct indirect_placeholder_47_ret_type var_36;
    uint64_t var_37;
    uint64_t local_sp_9;
    uint64_t rax_6;
    uint64_t rdx_5;
    uint64_t rax_7;
    uint64_t rdx_6;
    uint64_t rax_13;
    uint64_t local_sp_10;
    uint64_t rdx_12;
    uint64_t rax_8;
    uint64_t rdx_7;
    uint64_t var_38;
    uint64_t var_39;
    uint64_t rax_12;
    uint64_t local_sp_17;
    uint64_t local_sp_11;
    uint32_t var_40;
    uint64_t var_41;
    uint64_t *var_42;
    uint32_t var_43;
    uint64_t var_44;
    uint64_t var_45;
    uint64_t var_91;
    uint64_t local_sp_13;
    uint64_t var_92;
    uint64_t var_93;
    unsigned char var_81;
    uint64_t rax_11;
    uint64_t var_83;
    uint64_t var_84;
    uint64_t var_85;
    uint64_t var_86;
    uint64_t rdx_11;
    uint64_t r13_1;
    bool var_87;
    uint64_t var_88;
    bool var_89;
    uint64_t var_90;
    uint64_t local_sp_16;
    uint64_t var_18;
    uint64_t *var_19;
    uint64_t *var_20;
    uint64_t var_15;
    uint64_t var_17;
    uint64_t var_94;
    uint64_t var_95;
    uint64_t var_11;
    uint64_t var_12;
    struct indirect_placeholder_48_ret_type var_13;
    uint64_t var_14;
    unsigned int loop_state_var;
    var_0 = revng_init_local_sp(0UL);
    var_1 = init_rbx();
    var_2 = init_r14();
    var_3 = init_rbp();
    var_4 = init_r13();
    var_5 = init_r12();
    var_6 = init_r15();
    var_7 = init_rcx();
    *(uint64_t *)(var_0 + (-8L)) = var_6;
    *(uint64_t *)(var_0 + (-16L)) = var_2;
    *(uint64_t *)(var_0 + (-24L)) = var_4;
    *(uint64_t *)(var_0 + (-32L)) = var_5;
    *(uint64_t *)(var_0 + (-40L)) = var_3;
    *(uint64_t *)(var_0 + (-48L)) = var_1;
    *(uint64_t *)(var_0 + (-72L)) = rsi;
    var_8 = rdi + 2UL;
    *(uint64_t *)(var_0 + (-112L)) = 4201385UL;
    var_9 = indirect_placeholder_39(var_8);
    *(uint64_t *)(var_0 + (-96L)) = var_9.field_0;
    var_10 = rdi + 1UL;
    r15_0 = 1UL;
    rbx_4 = 0UL;
    r14_1 = 0UL;
    rcx_1 = var_7;
    rdx_13 = 1152921504606846975UL;
    if (var_10 > 1152921504606846975UL) {
        var_94 = var_9.field_2;
        var_95 = var_9.field_1;
        *(uint64_t *)(var_0 + (-120L)) = 4201414UL;
        indirect_placeholder_24(var_95, var_94);
        abort();
    }
    var_11 = var_10 << 3UL;
    var_12 = var_0 + (-120L);
    *(uint64_t *)var_12 = 4201427UL;
    var_13 = indirect_placeholder_48(var_11);
    var_14 = var_13.field_0;
    *(uint64_t *)(var_0 + (-80L)) = var_14;
    local_sp_16 = var_12;
    rax_14 = var_14;
    r12_3 = var_14;
    local_sp_17 = var_12;
    if (rdi != 0UL) {
        var_15 = *(uint64_t *)6358152UL;
        rbp_0 = *(uint64_t *)(var_0 + (-88L));
        r9_2 = var_13.field_1;
        r8_2 = var_13.field_2;
        while (1U)
            {
                *(uint64_t *)local_sp_16 = rbp_0;
                var_16 = local_sp_16 + (-8L);
                *(uint64_t *)var_16 = 4201485UL;
                indirect_placeholder();
                var_17 = (uint64_t)(uint32_t)rax_14;
                _pre_phi217 = var_17;
                rdx_3 = rdx_13;
                rbx_2 = rbx_4;
                r12_0 = r12_3;
                r9_0 = r9_2;
                r8_0 = r8_2;
                local_sp_7 = var_16;
                rax_5 = rax_14;
                rdx_4 = rdx_13;
                rbx_3 = rbx_4;
                r14_0 = r14_1;
                r12_1 = r12_3;
                rcx_0 = rcx_1;
                r9_1 = r9_2;
                r8_1 = r8_2;
                if (var_17 == 0UL) {
                    *(unsigned char *)6358305UL = (unsigned char)'\x01';
                    *(uint64_t *)((rbx_4 << 3UL) + r12_3) = var_15;
                } else {
                    var_18 = local_sp_16 + (-16L);
                    var_19 = (uint64_t *)var_18;
                    *var_19 = 4201516UL;
                    indirect_placeholder();
                    var_20 = (uint64_t *)((rbx_4 << 3UL) + r12_3);
                    *var_20 = rax_14;
                    _pre_phi223 = var_20;
                    local_sp_6 = var_18;
                    rax_5 = 1UL;
                    if (rax_14 == 0UL) {
                        var_21 = *var_19;
                        var_22 = *(uint64_t *)var_21;
                        *(uint64_t *)(local_sp_16 + (-24L)) = 4201548UL;
                        var_23 = indirect_placeholder_46(var_22, 0UL, 3UL);
                        var_24 = var_23.field_0;
                        var_25 = var_23.field_1;
                        var_26 = var_23.field_2;
                        *(uint64_t *)(local_sp_16 + (-32L)) = 4201556UL;
                        indirect_placeholder();
                        var_27 = (uint64_t)*(uint32_t *)var_24;
                        var_28 = local_sp_16 + (-40L);
                        *(uint64_t *)var_28 = 4201581UL;
                        var_29 = indirect_placeholder_45(0UL, 4249609UL, 1UL, var_24, var_27, var_25, var_26);
                        _pre_phi223 = (uint64_t *)((var_24 << 3UL) + var_21);
                        _pre_phi217 = (uint64_t)(uint32_t)var_29.field_0;
                        local_sp_6 = var_28;
                        rdx_3 = 4249609UL;
                        rbx_2 = var_24;
                        r12_0 = var_21;
                        r9_0 = var_25;
                        r8_0 = var_26;
                    }
                    *(uint64_t *)(local_sp_6 + (-8L)) = 4201589UL;
                    indirect_placeholder();
                    var_30 = (_pre_phi217 == 0UL) ? 1UL : (uint64_t)(uint32_t)r14_1;
                    var_31 = *_pre_phi223;
                    var_32 = local_sp_6 + (-16L);
                    *(uint64_t *)var_32 = 4201614UL;
                    var_33 = indirect_placeholder_44(rbx_2, var_31, 2UL);
                    local_sp_7 = var_32;
                    rdx_4 = rdx_3;
                    rbx_3 = var_33.field_0;
                    r14_0 = var_30;
                    r12_1 = r12_0;
                    rcx_0 = var_33.field_1;
                    r9_1 = r9_0;
                    r8_1 = r8_0;
                }
                var_34 = rbx_3 + 1UL;
                rax_14 = rax_5;
                rbx_4 = var_34;
                r14_1 = r14_0;
                rcx_1 = rcx_0;
                r8_2 = r8_1;
                rdx_13 = rdx_4;
                r9_2 = r9_1;
                r12_3 = r12_1;
                local_sp_8 = local_sp_7;
                local_sp_9 = local_sp_7;
                rax_6 = rax_5;
                rdx_5 = rdx_4;
                rax_7 = rax_5;
                rdx_6 = rdx_4;
                local_sp_16 = local_sp_7;
                if (var_34 == rdi) {
                    break;
                }
                rbp_0 = rbp_0 + 8UL;
                continue;
            }
        if ((uint64_t)(unsigned char)r14_0 == 0UL) {
            *(uint64_t *)(local_sp_8 + 8UL) = rdi;
            *(unsigned char *)(local_sp_8 + 31UL) = (unsigned char)'\x01';
            local_sp_9 = local_sp_8;
            rax_7 = rax_6;
            rdx_6 = rdx_5;
        } else {
            rdx_5 = 4248204UL;
            if (*(unsigned char *)6358305UL == '\x00') {
                *(uint64_t *)(local_sp_7 + 8UL) = rdi;
                *(unsigned char *)(local_sp_7 + 31UL) = (unsigned char)'\x01';
            } else {
                var_35 = local_sp_7 + (-8L);
                *(uint64_t *)var_35 = 4201670UL;
                var_36 = indirect_placeholder_47(0UL, 4248204UL, 1UL, rcx_0, 0UL, r9_1, r8_1);
                var_37 = var_36.field_0;
                local_sp_8 = var_35;
                rax_6 = var_37;
                *(uint64_t *)(local_sp_8 + 8UL) = rdi;
                *(unsigned char *)(local_sp_8 + 31UL) = (unsigned char)'\x01';
                local_sp_9 = local_sp_8;
                rax_7 = rax_6;
                rdx_6 = rdx_5;
            }
        }
        local_sp_10 = local_sp_9;
        rax_8 = rax_7;
        rdx_7 = rdx_6;
        while (1U)
            {
                var_38 = *(uint64_t *)6358296UL;
                rax_9 = rax_8;
                local_sp_15 = local_sp_10;
                rdx_8 = rdx_7;
                r13_0 = var_38;
                rax_13 = rax_8;
                rdx_12 = rdx_7;
                local_sp_11 = local_sp_10;
                if (*(uint64_t *)(local_sp_10 + 8UL) == 0UL) {
                    local_sp_10 = local_sp_15;
                    rax_8 = rax_13;
                    rdx_7 = rdx_12;
                    local_sp_17 = local_sp_15;
                    if (*(uint64_t *)(local_sp_15 + 8UL) != 0UL) {
                        continue;
                    }
                    break;
                }
                var_39 = *(uint64_t *)(local_sp_10 + 40UL);
                *(uint64_t *)local_sp_10 = 0UL;
                *(unsigned char *)(local_sp_10 + 30UL) = (unsigned char)'\x00';
                r12_2 = var_39;
                while (1U)
                    {
                        _cast1 = (uint64_t *)r12_2;
                        rdx_2 = rdx_8;
                        local_sp_12 = local_sp_11;
                        rax_10 = rax_9;
                        rdx_9 = rdx_8;
                        if (*_cast1 != 0UL) {
                            *(uint64_t *)(local_sp_11 + (-8L)) = 4201768UL;
                            indirect_placeholder();
                            var_40 = (uint32_t)rax_9;
                            var_41 = local_sp_11 + (-16L);
                            var_42 = (uint64_t *)var_41;
                            *var_42 = 4201775UL;
                            indirect_placeholder();
                            var_43 = *(uint32_t *)rax_9;
                            var_44 = (uint64_t)var_43;
                            *(uint32_t *)(local_sp_11 + 8UL) = var_43;
                            var_45 = (uint64_t)(var_40 + 1U);
                            *(unsigned char *)(local_sp_11 + 13UL) = (var_45 != 0UL);
                            rax_1 = var_44;
                            local_sp_2 = var_41;
                            rax_10 = var_44;
                            var_46 = *var_42;
                            if (var_45 != 0UL & var_46 != 0UL) {
                                var_47 = local_sp_11 + (-24L);
                                var_48 = (uint64_t *)var_47;
                                *var_48 = 4201861UL;
                                indirect_placeholder();
                                local_sp_2 = var_47;
                                rdx_2 = var_46;
                                if (*var_48 != var_44) {
                                    *(uint64_t *)(local_sp_11 + (-32L)) = 4201872UL;
                                    indirect_placeholder();
                                    abort();
                                }
                                *var_48 = 0UL;
                            }
                            rdx_9 = rdx_2;
                            local_sp_12 = local_sp_2;
                            rdx_1 = rdx_2;
                            local_sp_3 = local_sp_2;
                            local_sp_5 = local_sp_2;
                            rdx_12 = rdx_2;
                            rdx_11 = rdx_2;
                            if (*(unsigned char *)(local_sp_2 + 29UL) != '\x00') {
                                var_49 = (uint64_t)*(unsigned char *)6357984UL;
                                rax_2 = var_49;
                                rax_3 = var_49;
                                if ((uint64_t)(var_40 - (uint32_t)var_49) == 0UL) {
                                    *(uint64_t *)(local_sp_5 + (-8L)) = 4201895UL;
                                    indirect_placeholder();
                                    *(uint64_t *)(local_sp_5 + (-16L)) = 4201913UL;
                                    indirect_placeholder();
                                    var_50 = local_sp_5 + (-24L);
                                    *(uint64_t *)var_50 = 4201920UL;
                                    indirect_placeholder();
                                    var_51 = (uint64_t)*(unsigned char *)6357984UL;
                                    local_sp_3 = var_50;
                                    rax_2 = var_51;
                                    local_sp_5 = var_50;
                                    rax_3 = var_51;
                                    do {
                                        *(uint64_t *)(local_sp_5 + (-8L)) = 4201895UL;
                                        indirect_placeholder();
                                        *(uint64_t *)(local_sp_5 + (-16L)) = 4201913UL;
                                        indirect_placeholder();
                                        var_50 = local_sp_5 + (-24L);
                                        *(uint64_t *)var_50 = 4201920UL;
                                        indirect_placeholder();
                                        var_51 = (uint64_t)*(unsigned char *)6357984UL;
                                        local_sp_3 = var_50;
                                        rax_2 = var_51;
                                        local_sp_5 = var_50;
                                        rax_3 = var_51;
                                    } while ((uint64_t)((uint32_t)var_51 - (uint32_t)rax_3) != 0UL);
                                }
                                local_sp_4 = local_sp_3;
                                rax_12 = rax_2;
                                rax_13 = rax_2;
                                if (r15_0 != rdi) {
                                    loop_state_var = 2U;
                                    break;
                                }
                                if (*(unsigned char *)r13_0 != '\x00') {
                                    var_52 = local_sp_3 + (-8L);
                                    *(uint64_t *)var_52 = 4202249UL;
                                    indirect_placeholder();
                                    local_sp_4 = var_52;
                                    if ((int)(uint32_t)rax_2 > (int)4294967295U) {
                                        *(uint64_t *)(local_sp_3 + (-16L)) = 4202258UL;
                                        indirect_placeholder();
                                        abort();
                                    }
                                }
                                var_53 = r13_0 + 1UL;
                                *(unsigned char *)(local_sp_4 + 30UL) = (unsigned char)'\x01';
                                local_sp_14 = local_sp_4;
                                r13_1 = var_53;
                                if (var_53 == *(uint64_t *)6358288UL) {
                                    var_54 = *(uint64_t *)6358296UL;
                                    r13_1 = var_54;
                                }
                                var_87 = (*(uint64_t *)(local_sp_14 + 8UL) != 0UL);
                                var_88 = (rdx_11 & (-256L)) | var_87;
                                var_89 = (r15_0 < rdi);
                                var_90 = (rax_12 & (-256L)) | var_89;
                                rax_9 = var_90;
                                local_sp_15 = local_sp_14;
                                rdx_8 = var_88;
                                r13_0 = r13_1;
                                rax_13 = var_90;
                                rdx_12 = var_88;
                                local_sp_11 = local_sp_14;
                                if (!(var_87 && var_89)) {
                                    loop_state_var = 0U;
                                    break;
                                }
                                r12_2 = r12_2 + 8UL;
                                r15_0 = r15_0 + 1UL;
                                continue;
                            }
                            if (*_cast1 != 0UL) {
                                var_56 = local_sp_2 + (-8L);
                                *(uint64_t *)var_56 = 4201935UL;
                                indirect_placeholder();
                                local_sp_1 = var_56;
                                if (var_43 == 0U) {
                                    var_57 = *(uint64_t *)(((r15_0 << 3UL) + *(uint64_t *)(local_sp_2 + 24UL)) + (-8L));
                                    *(uint64_t *)(local_sp_2 + (-16L)) = 4201964UL;
                                    var_58 = indirect_placeholder_43(var_57, 0UL, 3UL);
                                    var_59 = var_58.field_0;
                                    var_60 = var_58.field_1;
                                    var_61 = var_58.field_2;
                                    var_62 = (uint64_t)*(uint32_t *)(local_sp_2 + 8UL);
                                    var_63 = local_sp_2 + (-24L);
                                    *(uint64_t *)var_63 = 4201991UL;
                                    var_64 = indirect_placeholder_42(0UL, 4249609UL, 0UL, var_59, var_62, var_60, var_61);
                                    var_65 = var_64.field_0;
                                    *(unsigned char *)(local_sp_2 + 7UL) = (unsigned char)'\x00';
                                    local_sp_1 = var_63;
                                    rax_1 = var_65;
                                    rdx_1 = 4249609UL;
                                }
                                var_66 = *_cast1;
                                var_67 = (var_66 == *(uint64_t *)6358152UL);
                                var_68 = local_sp_1 + (-8L);
                                var_69 = (uint64_t *)var_68;
                                local_sp_0 = var_68;
                                rax_0 = rax_1;
                                rdx_0 = rdx_1;
                                if (var_67) {
                                    *var_69 = 4202014UL;
                                    indirect_placeholder();
                                } else {
                                    *var_69 = 4202021UL;
                                    var_70 = indirect_placeholder_3(var_66);
                                    rax_0 = var_70;
                                    if ((uint64_t)((uint32_t)var_70 + 1U) == 0UL) {
                                        var_71 = *(uint64_t *)(((r15_0 << 3UL) + *(uint64_t *)(local_sp_1 + 24UL)) + (-8L));
                                        *(uint64_t *)(local_sp_1 + (-16L)) = 4202051UL;
                                        var_72 = indirect_placeholder_41(var_71, 0UL, 3UL);
                                        var_73 = var_72.field_0;
                                        var_74 = var_72.field_1;
                                        var_75 = var_72.field_2;
                                        *(uint64_t *)(local_sp_1 + (-24L)) = 4202059UL;
                                        indirect_placeholder();
                                        var_76 = (uint64_t)*(uint32_t *)var_73;
                                        var_77 = local_sp_1 + (-32L);
                                        *(uint64_t *)var_77 = 4202084UL;
                                        var_78 = indirect_placeholder_40(0UL, 4249609UL, 0UL, var_73, var_76, var_74, var_75);
                                        var_79 = var_78.field_0;
                                        *(unsigned char *)(local_sp_1 + (-1L)) = (unsigned char)'\x00';
                                        local_sp_0 = var_77;
                                        rax_0 = var_79;
                                        rdx_0 = 4249609UL;
                                    }
                                }
                                *_cast1 = 0UL;
                                var_80 = (uint64_t *)(local_sp_0 + 8UL);
                                *var_80 = (*var_80 + (-1L));
                                local_sp_12 = local_sp_0;
                                rax_10 = rax_0;
                                rdx_9 = rdx_0;
                            }
                        }
                        rdx_10 = rdx_9;
                        local_sp_15 = local_sp_12;
                        local_sp_14 = local_sp_12;
                        rax_13 = rax_10;
                        rdx_12 = rdx_9;
                        local_sp_13 = local_sp_12;
                        rdx_11 = rdx_9;
                        if (r15_0 != rdi) {
                            loop_state_var = 1U;
                            break;
                        }
                        var_81 = *(unsigned char *)r13_0;
                        rax_11 = (uint64_t)var_81;
                        if (var_81 == '\x00') {
                            var_82 = *(uint64_t *)(local_sp_12 + 16UL);
                            _cast3 = (uint64_t *)local_sp_12;
                            var_83 = *_cast3;
                            *(unsigned char *)(var_83 + var_82) = var_81;
                            var_84 = var_83 + 1UL;
                            *_cast3 = var_84;
                            rax_11 = var_84;
                        }
                        var_85 = r13_0 + 1UL;
                        var_86 = (var_85 == *(uint64_t *)6358288UL) ? *(uint64_t *)6358296UL : var_85;
                        rax_12 = rax_11;
                        r13_1 = var_86;
                        var_87 = (*(uint64_t *)(local_sp_14 + 8UL) != 0UL);
                        var_88 = (rdx_11 & (-256L)) | var_87;
                        var_89 = (r15_0 < rdi);
                        var_90 = (rax_12 & (-256L)) | var_89;
                        rax_9 = var_90;
                        local_sp_15 = local_sp_14;
                        rdx_8 = var_88;
                        r13_0 = r13_1;
                        rax_13 = var_90;
                        rdx_12 = var_88;
                        local_sp_11 = local_sp_14;
                        if (!(var_87 && var_89)) {
                            loop_state_var = 0U;
                            break;
                        }
                        r12_2 = r12_2 + 8UL;
                        r15_0 = r15_0 + 1UL;
                        continue;
                    }
                switch (loop_state_var) {
                  case 0U:
                    {
                        break;
                    }
                    break;
                  case 1U:
                    {
                        if (*(unsigned char *)(local_sp_12 + 30UL) != '\x00') {
                            var_91 = *(uint64_t *)local_sp_12;
                            if (var_91 != 0UL) {
                                var_92 = local_sp_12 + (-8L);
                                *(uint64_t *)var_92 = 4202155UL;
                                indirect_placeholder();
                                local_sp_13 = var_92;
                                rdx_10 = rax_10;
                                if (rax_10 == var_91) {
                                    *(uint64_t *)(local_sp_12 + (-16L)) = 4202165UL;
                                    indirect_placeholder();
                                    abort();
                                }
                            }
                            var_93 = local_sp_13 + (-8L);
                            *(uint64_t *)var_93 = 4202177UL;
                            indirect_placeholder();
                            local_sp_15 = var_93;
                            rdx_12 = rdx_10;
                            if ((int)(uint32_t)rax_10 > (int)4294967295U) {
                                *(uint64_t *)(local_sp_13 + (-16L)) = 4202190UL;
                                indirect_placeholder();
                                abort();
                            }
                        }
                    }
                    break;
                  case 2U:
                    {
                        var_55 = local_sp_3 + (-8L);
                        *(uint64_t *)var_55 = 4202307UL;
                        indirect_placeholder();
                        local_sp_15 = var_55;
                        if ((int)(uint32_t)rax_2 > (int)4294967295U) {
                            *(uint64_t *)(local_sp_3 + (-16L)) = 4202316UL;
                            indirect_placeholder();
                            abort();
                        }
                    }
                    break;
                }
            }
    }
    *(unsigned char *)(var_0 + (-89L)) = (unsigned char)'\x01';
    *(uint64_t *)(local_sp_17 + (-8L)) = 4202427UL;
    indirect_placeholder();
    *(uint64_t *)(local_sp_17 + (-16L)) = 4202437UL;
    indirect_placeholder();
    return;
}

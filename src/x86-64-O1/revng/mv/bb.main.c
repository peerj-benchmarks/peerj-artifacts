typedef unsigned char __u_char;
typedef unsigned short __u_short;
typedef unsigned int __u_int;
typedef unsigned long __u_long;
typedef signed char __int8_t;
typedef unsigned char __uint8_t;
typedef short __int16_t;
typedef unsigned short __uint16_t;
typedef int __int32_t;
typedef unsigned int __uint32_t;
typedef long __int64_t;
typedef unsigned long __uint64_t;
typedef __int8_t __int_least8_t;
typedef __uint8_t __uint_least8_t;
typedef __int16_t __int_least16_t;
typedef __uint16_t __uint_least16_t;
typedef __int32_t __int_least32_t;
typedef __uint32_t __uint_least32_t;
typedef __int64_t __int_least64_t;
typedef __uint64_t __uint_least64_t;
typedef long __quad_t;
typedef unsigned long __u_quad_t;
typedef long __intmax_t;
typedef unsigned long __uintmax_t;
typedef unsigned long __dev_t;
typedef unsigned int __uid_t;
typedef unsigned int __gid_t;
typedef unsigned long __ino_t;
typedef unsigned long __ino64_t;
typedef unsigned int __mode_t;
typedef unsigned long __nlink_t;
typedef long __off_t;
typedef long __off64_t;
typedef int __pid_t;
typedef struct {
    int __val[2];
} __fsid_t;
typedef long __clock_t;
typedef unsigned long __rlim_t;
typedef unsigned long __rlim64_t;
typedef unsigned int __id_t;
typedef long __time_t;
typedef unsigned int __useconds_t;
typedef long __suseconds_t;
typedef int __daddr_t;
typedef int __key_t;
typedef int __clockid_t;
typedef void *__timer_t;
typedef long __blksize_t;
typedef long __blkcnt_t;
typedef long __blkcnt64_t;
typedef unsigned long __fsblkcnt_t;
typedef unsigned long __fsblkcnt64_t;
typedef unsigned long __fsfilcnt_t;
typedef unsigned long __fsfilcnt64_t;
typedef long __fsword_t;
typedef long __ssize_t;
typedef long __syscall_slong_t;
typedef unsigned long __syscall_ulong_t;
typedef __off64_t __loff_t;
typedef char *__caddr_t;
typedef long __intptr_t;
typedef unsigned int __socklen_t;
typedef int __sig_atomic_t;
typedef __int8_t int8_t;
typedef __int16_t int16_t;
typedef __int32_t int32_t;
typedef __int64_t int64_t;
typedef __uint8_t uint8_t;
typedef __uint16_t uint16_t;
typedef __uint32_t uint32_t;
typedef __uint64_t uint64_t;
typedef __int_least8_t int_least8_t;
typedef __int_least16_t int_least16_t;
typedef __int_least32_t int_least32_t;
typedef __int_least64_t int_least64_t;
typedef __uint_least8_t uint_least8_t;
typedef __uint_least16_t uint_least16_t;
typedef __uint_least32_t uint_least32_t;
typedef __uint_least64_t uint_least64_t;
typedef signed char int_fast8_t;
typedef long int_fast16_t;
typedef long int_fast32_t;
typedef long int_fast64_t;
typedef unsigned char uint_fast8_t;
typedef unsigned long uint_fast16_t;
typedef unsigned long uint_fast32_t;
typedef unsigned long uint_fast64_t;
typedef long intptr_t;
typedef unsigned long uintptr_t;
typedef __intmax_t intmax_t;
typedef __uintmax_t uintmax_t;
typedef _Bool bool;
struct indirect_placeholder_242_ret_type;
struct indirect_placeholder_243_ret_type;
struct indirect_placeholder_238_ret_type;
struct indirect_placeholder_237_ret_type;
struct indirect_placeholder_239_ret_type;
struct indirect_placeholder_236_ret_type;
struct indirect_placeholder_241_ret_type;
struct indirect_placeholder_244_ret_type;
struct indirect_placeholder_245_ret_type;
struct indirect_placeholder_240_ret_type;
struct indirect_placeholder_246_ret_type;
struct indirect_placeholder_249_ret_type;
struct indirect_placeholder_248_ret_type;
struct indirect_placeholder_250_ret_type;
struct indirect_placeholder_247_ret_type;
struct indirect_placeholder_252_ret_type;
struct indirect_placeholder_253_ret_type;
struct indirect_placeholder_242_ret_type {
    uint64_t field_0;
    uint64_t field_1;
    uint64_t field_2;
};
struct indirect_placeholder_243_ret_type {
    uint64_t field_0;
    uint64_t field_1;
    uint64_t field_2;
};
struct indirect_placeholder_238_ret_type {
    uint64_t field_0;
    uint64_t field_1;
    uint64_t field_2;
};
struct indirect_placeholder_237_ret_type {
    uint64_t field_0;
    uint64_t field_1;
    uint64_t field_2;
};
struct indirect_placeholder_239_ret_type {
    uint64_t field_0;
    uint64_t field_1;
    uint64_t field_2;
};
struct indirect_placeholder_236_ret_type {
    uint64_t field_0;
    uint64_t field_1;
    uint64_t field_2;
};
struct indirect_placeholder_241_ret_type {
    uint64_t field_0;
    uint64_t field_1;
    uint64_t field_2;
};
struct indirect_placeholder_244_ret_type {
    uint64_t field_0;
    uint64_t field_1;
    uint64_t field_2;
};
struct indirect_placeholder_245_ret_type {
    uint64_t field_0;
    uint64_t field_1;
    uint64_t field_2;
};
struct indirect_placeholder_240_ret_type {
    uint64_t field_0;
    uint64_t field_1;
    uint64_t field_2;
};
struct indirect_placeholder_246_ret_type {
    uint64_t field_0;
    uint64_t field_1;
    uint64_t field_2;
};
struct indirect_placeholder_249_ret_type {
    uint64_t field_0;
    uint64_t field_1;
    uint64_t field_2;
};
struct indirect_placeholder_248_ret_type {
    uint64_t field_0;
    uint64_t field_1;
    uint64_t field_2;
};
struct indirect_placeholder_250_ret_type {
    uint64_t field_0;
    uint64_t field_1;
    uint64_t field_2;
};
struct indirect_placeholder_247_ret_type {
    uint64_t field_0;
    uint64_t field_1;
    uint64_t field_2;
};
struct indirect_placeholder_252_ret_type {
    uint64_t field_0;
    uint64_t field_1;
    uint64_t field_2;
};
struct indirect_placeholder_253_ret_type {
    uint64_t field_0;
    uint64_t field_1;
    uint64_t field_2;
};
extern void abort(void);
extern struct indirect_placeholder_242_ret_type indirect_placeholder_242(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint64_t param_3, uint64_t param_4, uint64_t param_5, uint64_t param_6);
extern struct indirect_placeholder_243_ret_type indirect_placeholder_243(uint64_t param_0, uint64_t param_1, uint64_t param_2);
extern uint64_t revng_init_local_sp(uint64_t param_0);
extern uint64_t init_rbx(void);
extern void indirect_placeholder(void);
extern uint64_t init_rbp(void);
extern uint64_t init_r14(void);
extern uint64_t init_r13(void);
extern uint64_t init_r12(void);
extern uint64_t helper_cc_compute_all_wrapper(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint32_t param_3);
extern uint64_t init_r15(void);
extern uint64_t init_rax(void);
extern uint64_t indirect_placeholder_1(uint64_t param_0, uint64_t param_1);
extern void indirect_placeholder_2(uint64_t param_0);
extern void indirect_placeholder_16(uint64_t param_0, uint64_t param_1, uint64_t param_2);
extern uint64_t indirect_placeholder_5(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint64_t param_3);
extern struct indirect_placeholder_238_ret_type indirect_placeholder_238(uint64_t param_0, uint64_t param_1);
extern struct indirect_placeholder_237_ret_type indirect_placeholder_237(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint64_t param_3, uint64_t param_4, uint64_t param_5, uint64_t param_6);
extern struct indirect_placeholder_239_ret_type indirect_placeholder_239(uint64_t param_0, uint64_t param_1);
extern struct indirect_placeholder_236_ret_type indirect_placeholder_236(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint64_t param_3, uint64_t param_4, uint64_t param_5, uint64_t param_6);
extern struct indirect_placeholder_241_ret_type indirect_placeholder_241(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint64_t param_3, uint64_t param_4, uint64_t param_5, uint64_t param_6);
extern struct indirect_placeholder_244_ret_type indirect_placeholder_244(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint64_t param_3, uint64_t param_4, uint64_t param_5, uint64_t param_6);
extern struct indirect_placeholder_245_ret_type indirect_placeholder_245(uint64_t param_0, uint64_t param_1);
extern struct indirect_placeholder_240_ret_type indirect_placeholder_240(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint64_t param_3, uint64_t param_4, uint64_t param_5, uint64_t param_6);
extern struct indirect_placeholder_246_ret_type indirect_placeholder_246(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint64_t param_3, uint64_t param_4, uint64_t param_5, uint64_t param_6);
extern struct indirect_placeholder_249_ret_type indirect_placeholder_249(uint64_t param_0, uint64_t param_1);
extern struct indirect_placeholder_248_ret_type indirect_placeholder_248(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint64_t param_3, uint64_t param_4, uint64_t param_5, uint64_t param_6);
extern struct indirect_placeholder_250_ret_type indirect_placeholder_250(uint64_t param_0, uint64_t param_1);
extern struct indirect_placeholder_247_ret_type indirect_placeholder_247(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint64_t param_3, uint64_t param_4, uint64_t param_5, uint64_t param_6);
extern void indirect_placeholder_251(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint64_t param_3, uint64_t param_4, uint64_t param_5, uint64_t param_6);
extern struct indirect_placeholder_252_ret_type indirect_placeholder_252(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint64_t param_3, uint64_t param_4, uint64_t param_5, uint64_t param_6);
extern struct indirect_placeholder_253_ret_type indirect_placeholder_253(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint64_t param_3, uint64_t param_4);
void bb_main(uint64_t rdi, uint64_t rsi) {
    uint64_t var_53;
    uint64_t r13_0;
    uint64_t var_99;
    uint64_t var_100;
    uint64_t var_101;
    struct indirect_placeholder_253_ret_type var_17;
    uint64_t var_0;
    uint64_t var_1;
    uint64_t var_2;
    uint64_t var_3;
    uint64_t var_4;
    uint64_t var_5;
    uint64_t var_6;
    uint64_t var_7;
    uint64_t var_8;
    uint64_t var_9;
    uint64_t var_10;
    unsigned char *var_11;
    unsigned char *var_12;
    unsigned char *var_13;
    unsigned char *var_14;
    uint64_t var_15;
    uint64_t local_sp_16;
    uint64_t local_sp_1;
    uint64_t r12_2;
    uint32_t rax_0;
    uint64_t local_sp_0;
    uint64_t var_95;
    uint64_t var_96;
    uint64_t var_97;
    uint64_t var_98;
    uint64_t local_sp_11;
    uint64_t local_sp_2;
    uint64_t var_102;
    uint64_t var_103;
    uint64_t var_104;
    uint64_t local_sp_4;
    uint64_t rcx_1;
    uint64_t var_75;
    struct indirect_placeholder_238_ret_type var_76;
    uint64_t var_77;
    uint64_t var_78;
    uint64_t var_79;
    uint64_t var_80;
    uint64_t var_81;
    uint64_t local_sp_5;
    uint64_t var_73;
    uint32_t var_74;
    uint64_t local_sp_10;
    uint64_t local_sp_8;
    uint64_t local_sp_3;
    uint64_t var_89;
    uint64_t local_sp_9;
    uint64_t var_82;
    struct indirect_placeholder_239_ret_type var_83;
    uint64_t var_84;
    uint64_t var_85;
    uint64_t var_86;
    uint64_t var_87;
    struct indirect_placeholder_236_ret_type var_88;
    uint64_t rbx_2;
    uint64_t local_sp_6;
    uint64_t r14_0;
    uint32_t var_44;
    uint64_t var_45;
    uint32_t var_46;
    uint64_t var_47;
    uint64_t var_48;
    uint64_t r15_0;
    bool var_49;
    uint64_t var_50;
    uint64_t var_51;
    uint64_t var_52;
    struct indirect_placeholder_243_ret_type var_54;
    uint64_t var_55;
    uint64_t var_56;
    uint64_t var_57;
    uint64_t var_58;
    uint64_t local_sp_7;
    uint64_t var_59;
    struct indirect_placeholder_244_ret_type var_60;
    uint64_t rcx_0;
    uint64_t r9_0;
    uint64_t r8_0;
    uint64_t r12_3;
    uint64_t var_62;
    struct indirect_placeholder_245_ret_type var_63;
    uint64_t var_64;
    uint64_t var_65;
    uint64_t var_66;
    uint64_t r12_3_be;
    uint64_t r15_0_be;
    uint64_t var_61;
    uint64_t var_67;
    uint64_t var_68;
    uint64_t var_69;
    uint64_t var_70;
    uint64_t var_71;
    uint64_t var_72;
    uint64_t rbp_0;
    uint64_t r12_0;
    uint64_t rcx_2;
    uint64_t r9_1;
    uint64_t r8_1;
    uint64_t rbp_1;
    uint64_t r12_1;
    uint64_t rcx_3;
    uint64_t r9_2;
    uint64_t r8_2;
    bool var_90;
    uint64_t var_91;
    uint64_t *var_92;
    uint64_t r13_1;
    uint64_t var_93;
    uint64_t rbp_2;
    uint64_t var_94;
    uint64_t var_31;
    struct indirect_placeholder_249_ret_type var_32;
    uint64_t var_33;
    uint64_t var_34;
    uint64_t var_35;
    uint64_t var_36;
    uint64_t var_37;
    uint64_t local_sp_15;
    uint64_t local_sp_12;
    uint64_t rbx_1;
    uint64_t local_sp_13;
    uint64_t var_38;
    struct indirect_placeholder_250_ret_type var_39;
    uint64_t var_40;
    uint64_t var_41;
    uint64_t var_42;
    uint64_t var_43;
    uint64_t local_sp_16_be;
    uint64_t rbx_2_be;
    uint64_t r14_0_be;
    uint64_t r13_1_be;
    uint64_t local_sp_14;
    uint64_t var_22;
    uint64_t var_23;
    uint64_t var_24;
    uint64_t var_25;
    uint64_t var_26;
    uint64_t var_27;
    uint64_t var_28;
    uint64_t var_29;
    uint64_t var_30;
    uint64_t var_16;
    uint64_t var_18;
    uint64_t var_19;
    uint64_t var_20;
    uint32_t var_21;
    unsigned int loop_state_var;
    var_0 = revng_init_local_sp(0UL);
    var_1 = init_rax();
    var_2 = init_rbx();
    var_3 = init_r14();
    var_4 = init_rbp();
    var_5 = init_r13();
    var_6 = init_r12();
    var_7 = init_r15();
    *(uint64_t *)(var_0 + (-8L)) = var_7;
    *(uint64_t *)(var_0 + (-16L)) = var_3;
    *(uint64_t *)(var_0 + (-24L)) = var_5;
    *(uint64_t *)(var_0 + (-32L)) = var_6;
    *(uint64_t *)(var_0 + (-40L)) = var_4;
    *(uint64_t *)(var_0 + (-48L)) = var_2;
    var_8 = (uint64_t)(uint32_t)rdi;
    var_9 = *(uint64_t *)rsi;
    *(uint64_t *)(var_0 + (-320L)) = 4207441UL;
    indirect_placeholder_2(var_9);
    *(uint64_t *)(var_0 + (-328L)) = 4207456UL;
    indirect_placeholder();
    *(uint64_t *)(var_0 + (-336L)) = 4207466UL;
    indirect_placeholder();
    var_10 = var_0 + (-160L);
    *(uint64_t *)(var_0 + (-344L)) = 4207479UL;
    indirect_placeholder_2(var_10);
    *(unsigned char *)(var_0 + (-148L)) = (unsigned char)'\x00';
    *(uint32_t *)(var_0 + (-116L)) = 1U;
    *(uint32_t *)(var_0 + (-164L)) = 2U;
    *(unsigned char *)(var_0 + (-147L)) = (unsigned char)'\x00';
    *(unsigned char *)(var_0 + (-146L)) = (unsigned char)'\x00';
    *(unsigned char *)(var_0 + (-145L)) = (unsigned char)'\x00';
    *(uint32_t *)var_10 = 4U;
    *(unsigned char *)(var_0 + (-144L)) = (unsigned char)'\x01';
    *(unsigned char *)(var_0 + (-143L)) = (unsigned char)'\x00';
    *(unsigned char *)(var_0 + (-140L)) = (unsigned char)'\x00';
    *(unsigned char *)(var_0 + (-139L)) = (unsigned char)'\x01';
    *(unsigned char *)(var_0 + (-134L)) = (unsigned char)'\x01';
    *(unsigned char *)(var_0 + (-138L)) = (unsigned char)'\x01';
    *(unsigned char *)(var_0 + (-137L)) = (unsigned char)'\x01';
    *(unsigned char *)(var_0 + (-136L)) = (unsigned char)'\x00';
    var_11 = (unsigned char *)(var_0 + (-131L));
    *var_11 = (unsigned char)'\x00';
    *(unsigned char *)(var_0 + (-135L)) = (unsigned char)'\x00';
    *(unsigned char *)(var_0 + (-127L)) = (unsigned char)'\x00';
    *(unsigned char *)(var_0 + (-133L)) = (unsigned char)'\x01';
    *(unsigned char *)(var_0 + (-132L)) = (unsigned char)'\x00';
    var_12 = (unsigned char *)(var_0 + (-130L));
    *var_12 = (unsigned char)'\x00';
    var_13 = (unsigned char *)(var_0 + (-129L));
    *var_13 = (unsigned char)'\x01';
    var_14 = (unsigned char *)(var_0 + (-128L));
    *var_14 = (unsigned char)'\x00';
    *(unsigned char *)(var_0 + (-126L)) = (unsigned char)'\x01';
    *(uint32_t *)(var_0 + (-156L)) = 2U;
    *(unsigned char *)(var_0 + (-124L)) = (unsigned char)'\x00';
    *(unsigned char *)(var_0 + (-125L)) = (unsigned char)'\x00';
    *(uint32_t *)(var_0 + (-152L)) = 0U;
    var_15 = var_0 + (-352L);
    *(uint64_t *)var_15 = 4207728UL;
    indirect_placeholder();
    *var_13 = ((uint64_t)(uint32_t)var_1 != 0UL);
    *var_14 = (unsigned char)'\x00';
    *var_11 = (unsigned char)'\x00';
    *var_12 = (unsigned char)'\x00';
    *(uint64_t *)(var_0 + (-120L)) = 0UL;
    *(uint64_t *)(var_0 + (-112L)) = 0UL;
    *(unsigned char *)(var_0 + (-340L)) = (unsigned char)'\x00';
    local_sp_16 = var_15;
    rax_0 = 0U;
    rcx_1 = 0UL;
    rbx_2 = rsi;
    r14_0 = 0UL;
    r15_0 = 0UL;
    rcx_0 = 0UL;
    r12_3 = 0UL;
    r13_1 = 0UL;
    while (1U)
        {
            var_16 = local_sp_16 + (-8L);
            *(uint64_t *)var_16 = 4208369UL;
            var_17 = indirect_placeholder_253(4311300UL, var_8, 4314048UL, rbx_2, 0UL);
            var_18 = var_17.field_0;
            var_19 = var_17.field_1;
            var_20 = var_17.field_2;
            var_21 = (uint32_t)var_18;
            local_sp_8 = var_16;
            local_sp_9 = var_16;
            local_sp_7 = var_16;
            r9_0 = var_19;
            r8_0 = var_20;
            r12_3_be = r12_3;
            r15_0_be = r15_0;
            r12_0 = r12_3;
            rcx_2 = r12_3;
            r9_1 = var_19;
            r8_1 = var_20;
            local_sp_15 = var_16;
            rbx_1 = rbx_2;
            local_sp_16_be = var_16;
            rbx_2_be = rbx_2;
            r14_0_be = r14_0;
            r13_1_be = r13_1;
            local_sp_14 = var_16;
            if ((uint64_t)(var_21 + 1U) != 0UL) {
                var_44 = *(uint32_t *)6440540UL;
                var_45 = rdi - (uint64_t)var_44;
                var_46 = (uint32_t)var_45;
                var_47 = (uint64_t)var_46;
                var_48 = ((uint64_t)var_44 << 3UL) + rbx_2;
                var_49 = (r12_3 == 0UL);
                var_50 = var_45 << 32UL;
                r13_0 = var_48;
                rbp_0 = var_47;
                rbp_1 = var_47;
                if ((long)var_50 <= (long)(var_49 ? 4294967296UL : 0UL)) {
                    var_51 = helper_cc_compute_all_wrapper(var_47, 0UL, 0UL, 24U);
                    if ((uint64_t)(((unsigned char)(var_51 >> 4UL) ^ (unsigned char)var_51) & '\xc0') != 0UL) {
                        var_52 = local_sp_16 + (-16L);
                        *(uint64_t *)var_52 = 4208437UL;
                        indirect_placeholder_242(0UL, 4311313UL, 0UL, r12_3, 0UL, var_19, var_20);
                        local_sp_6 = var_52;
                        loop_state_var = 0U;
                        break;
                    }
                    var_53 = *(uint64_t *)var_48;
                    *(uint64_t *)(local_sp_16 + (-16L)) = 4208452UL;
                    var_54 = indirect_placeholder_243(4UL, r12_3, var_53);
                    var_55 = var_54.field_0;
                    var_56 = var_54.field_1;
                    var_57 = var_54.field_2;
                    var_58 = local_sp_16 + (-24L);
                    *(uint64_t *)var_58 = 4208480UL;
                    indirect_placeholder_241(0UL, 4313768UL, 0UL, var_55, 0UL, var_56, var_57);
                    local_sp_6 = var_58;
                    loop_state_var = 0U;
                    break;
                }
                if (*(unsigned char *)(local_sp_16 + 4UL) != '\x00') {
                    if (!var_49) {
                        loop_state_var = 2U;
                        break;
                    }
                    r12_0 = 0UL;
                    if ((int)var_46 <= (int)1U) {
                        loop_state_var = 3U;
                        break;
                    }
                    var_67 = local_sp_16 + (-16L);
                    *(uint64_t *)var_67 = 4208626UL;
                    indirect_placeholder();
                    local_sp_8 = var_67;
                    rcx_1 = 4313992UL;
                    loop_state_var = 3U;
                    break;
                }
                if (!var_49) {
                    loop_state_var = 4U;
                    break;
                }
                var_59 = local_sp_16 + (-16L);
                *(uint64_t *)var_59 = 4208527UL;
                var_60 = indirect_placeholder_244(0UL, 4313816UL, 1UL, r12_3, 0UL, var_19, var_20);
                local_sp_7 = var_59;
                rcx_0 = var_60.field_0;
                r9_0 = var_60.field_1;
                r8_0 = var_60.field_2;
                loop_state_var = 4U;
                break;
            }
            if ((uint64_t)(var_21 + (-102)) == 0UL) {
                *(uint32_t *)(local_sp_16 + 176UL) = 1U;
            } else {
                r13_1_be = 1UL;
                if ((int)var_21 > (int)102U) {
                    if ((uint64_t)(var_21 + (-116)) == 0UL) {
                        if (r12_3 == 0UL) {
                            var_26 = local_sp_16 + (-16L);
                            *(uint64_t *)var_26 = 4208084UL;
                            indirect_placeholder_252(0UL, 4313728UL, 1UL, 4314048UL, 0UL, var_19, var_20);
                            local_sp_15 = var_26;
                        }
                        var_27 = local_sp_15 + 32UL;
                        var_28 = *(uint64_t *)6442448UL;
                        var_29 = local_sp_15 + (-8L);
                        *(uint64_t *)var_29 = 4208101UL;
                        var_30 = indirect_placeholder_1(var_28, var_27);
                        local_sp_12 = var_29;
                        if ((uint64_t)(uint32_t)var_30 == 0UL) {
                            var_31 = *(uint64_t *)6442448UL;
                            *(uint64_t *)(local_sp_15 + (-16L)) = 4208122UL;
                            var_32 = indirect_placeholder_249(4UL, var_31);
                            var_33 = var_32.field_0;
                            var_34 = var_32.field_1;
                            var_35 = var_32.field_2;
                            *(uint64_t *)(local_sp_15 + (-24L)) = 4208130UL;
                            indirect_placeholder();
                            var_36 = (uint64_t)*(uint32_t *)var_33;
                            var_37 = local_sp_15 + (-32L);
                            *(uint64_t *)var_37 = 4208155UL;
                            indirect_placeholder_248(0UL, 4311210UL, 1UL, var_33, var_36, var_34, var_35);
                            local_sp_12 = var_37;
                            rbx_1 = var_33;
                        }
                        local_sp_13 = local_sp_12;
                        rbx_2_be = rbx_1;
                        if ((uint32_t)((uint16_t)*(uint32_t *)(local_sp_12 + 56UL) & (unsigned short)61440U) != 16384U) {
                            var_38 = *(uint64_t *)6442448UL;
                            *(uint64_t *)(local_sp_12 + (-8L)) = 4208188UL;
                            var_39 = indirect_placeholder_250(4UL, var_38);
                            var_40 = var_39.field_0;
                            var_41 = var_39.field_1;
                            var_42 = var_39.field_2;
                            var_43 = local_sp_12 + (-16L);
                            *(uint64_t *)var_43 = 4208216UL;
                            indirect_placeholder_247(0UL, 4311230UL, 1UL, var_40, 0UL, var_41, var_42);
                            local_sp_13 = var_43;
                        }
                        local_sp_16_be = local_sp_13;
                        r12_3_be = *(uint64_t *)6442448UL;
                    } else {
                        if ((int)var_21 > (int)116U) {
                            if ((uint64_t)(var_21 + (-118)) == 0UL) {
                                *(unsigned char *)(local_sp_16 + 214UL) = (unsigned char)'\x01';
                            } else {
                                if ((int)var_21 < (int)118U) {
                                    *(unsigned char *)(local_sp_16 + 213UL) = (unsigned char)'\x01';
                                } else {
                                    if ((uint64_t)(var_21 + (-128)) != 0UL) {
                                        loop_state_var = 1U;
                                        break;
                                    }
                                    *(unsigned char *)6440736UL = (unsigned char)'\x01';
                                }
                            }
                        } else {
                            if ((uint64_t)(var_21 + (-105)) == 0UL) {
                                *(uint32_t *)(local_sp_16 + 176UL) = 3U;
                            } else {
                                if ((uint64_t)(var_21 + (-110)) != 0UL) {
                                    loop_state_var = 1U;
                                    break;
                                }
                                *(uint32_t *)(local_sp_16 + 176UL) = 2U;
                            }
                        }
                    }
                } else {
                    if ((uint64_t)(var_21 + (-83)) == 0UL) {
                        r15_0_be = *(uint64_t *)6442448UL;
                    } else {
                        if ((int)var_21 <= (int)83U) {
                            if ((uint64_t)(var_21 + 131U) != 0UL) {
                                if ((uint64_t)(var_21 + 130U) != 0UL) {
                                    loop_state_var = 1U;
                                    break;
                                }
                                *(uint64_t *)(local_sp_16 + (-16L)) = 4208270UL;
                                indirect_placeholder_16(rbx_2, var_8, 0UL);
                                abort();
                            }
                            *(uint64_t *)(local_sp_16 + (-16L)) = 0UL;
                            *(uint64_t *)(local_sp_16 + (-24L)) = 4311287UL;
                            var_22 = *(uint64_t *)6440432UL;
                            var_23 = *(uint64_t *)6440576UL;
                            *(uint64_t *)(local_sp_16 + (-32L)) = 4208323UL;
                            indirect_placeholder_251(0UL, 4311169UL, var_23, var_22, 4311071UL, 4311259UL, 4311275UL);
                            var_24 = local_sp_16 + (-40L);
                            *(uint64_t *)var_24 = 4208333UL;
                            indirect_placeholder();
                            local_sp_14 = var_24;
                            loop_state_var = 1U;
                            break;
                        }
                        if ((uint64_t)(var_21 + (-90)) != 0UL) {
                            r13_1_be = 1UL;
                            if ((uint64_t)(var_21 + (-98)) == 0UL) {
                                var_25 = *(uint64_t *)6442448UL;
                                r14_0_be = (var_25 == 0UL) ? r14_0 : var_25;
                            } else {
                                r13_1_be = r13_1;
                                if ((uint64_t)(var_21 + (-84)) != 0UL) {
                                    loop_state_var = 1U;
                                    break;
                                }
                                *(unsigned char *)(local_sp_16 + 4UL) = (unsigned char)'\x01';
                            }
                        }
                    }
                }
            }
            local_sp_16 = local_sp_16_be;
            rbx_2 = rbx_2_be;
            r14_0 = r14_0_be;
            r15_0 = r15_0_be;
            r12_3 = r12_3_be;
            r13_1 = r13_1_be;
            continue;
        }
    switch (loop_state_var) {
      case 0U:
        {
            *(uint64_t *)(local_sp_6 + (-8L)) = 4208490UL;
            indirect_placeholder_16(var_48, var_47, 1UL);
            abort();
        }
        break;
      case 1U:
        {
            *(uint64_t *)(local_sp_14 + (-8L)) = 4208343UL;
            indirect_placeholder_16(rbx_2, var_8, 1UL);
            abort();
        }
        break;
      case 4U:
        {
            local_sp_10 = local_sp_7;
            r12_1 = rcx_0;
            rcx_3 = rcx_0;
            r9_2 = r9_0;
            r8_2 = r8_0;
            if ((int)var_46 > (int)2U) {
                var_62 = *(uint64_t *)(var_48 + 16UL);
                *(uint64_t *)(local_sp_7 + (-8L)) = 4208550UL;
                var_63 = indirect_placeholder_245(4UL, var_62);
                var_64 = var_63.field_0;
                var_65 = var_63.field_1;
                var_66 = var_63.field_2;
                *(uint64_t *)(local_sp_7 + (-16L)) = 4208578UL;
                indirect_placeholder_240(0UL, 4311334UL, 0UL, var_64, 0UL, var_65, var_66);
                *(uint64_t *)(local_sp_7 + (-24L)) = 4208588UL;
                indirect_placeholder_16(var_48, var_47, 1UL);
                abort();
            }
            if ((uint64_t)(unsigned char)r13_1 != 0UL) {
                *(uint32_t *)(local_sp_7 + 176UL) = 0U;
                *(uint64_t *)(local_sp_7 + (-8L)) = 4209071UL;
                indirect_placeholder_2(r15_0);
                var_61 = local_sp_7 + (-16L);
                *(uint64_t *)var_61 = 4209076UL;
                indirect_placeholder();
                local_sp_2 = var_61;
                var_102 = *(uint64_t *)(var_48 + 8UL);
                var_103 = local_sp_2 + 176UL;
                var_104 = *(uint64_t *)var_48;
                *(uint64_t *)(local_sp_2 + (-8L)) = 4208997UL;
                indirect_placeholder_5(0UL, var_104, var_103, var_102);
                return;
            }
            var_90 = (*(uint32_t *)(local_sp_10 + 184UL) == 2U);
            var_91 = local_sp_10 + (-8L);
            var_92 = (uint64_t *)var_91;
            local_sp_11 = var_91;
            rbp_2 = rbp_1;
            r12_2 = r12_1;
            if (var_90) {
                *var_92 = 4208841UL;
                indirect_placeholder_246(0UL, 4313888UL, 0UL, rcx_3, 0UL, r9_2, r8_2);
                *(uint64_t *)(local_sp_10 + (-16L)) = 4208851UL;
                indirect_placeholder_16(var_48, rbp_1, 1UL);
                abort();
            }
            *var_92 = 4208864UL;
            var_93 = indirect_placeholder_1(4311364UL, r14_0);
            rax_0 = (uint32_t)var_93;
            *(uint32_t *)(local_sp_11 + 176UL) = rax_0;
            *(uint64_t *)(local_sp_11 + (-8L)) = 4208886UL;
            indirect_placeholder_2(r15_0);
            var_94 = local_sp_11 + (-16L);
            *(uint64_t *)var_94 = 4208891UL;
            indirect_placeholder();
            local_sp_0 = var_94;
            local_sp_2 = var_94;
            if (r12_2 == 0UL) {
                var_102 = *(uint64_t *)(var_48 + 8UL);
                var_103 = local_sp_2 + 176UL;
                var_104 = *(uint64_t *)var_48;
                *(uint64_t *)(local_sp_2 + (-8L)) = 4208997UL;
                indirect_placeholder_5(0UL, var_104, var_103, var_102);
                return;
            }
            if ((int)(uint32_t)rbp_2 > (int)1U) {
                var_95 = local_sp_11 + 160UL;
                var_96 = local_sp_11 + (-24L);
                *(uint64_t *)var_96 = 4208914UL;
                indirect_placeholder_2(var_95);
                local_sp_0 = var_96;
            }
            var_97 = helper_cc_compute_all_wrapper(rbp_2, 0UL, 0UL, 24U);
            local_sp_1 = local_sp_0;
            if ((uint64_t)(((unsigned char)(var_97 >> 4UL) ^ (unsigned char)var_97) & '\xc0') != 0UL) {
                var_98 = (((rbp_2 << 3UL) + 34359738360UL) & 34359738360UL) + var_48;
                var_99 = local_sp_1 + 176UL;
                var_100 = *(uint64_t *)r13_0;
                var_101 = local_sp_1 + (-8L);
                *(uint64_t *)var_101 = 4208959UL;
                indirect_placeholder_5(1UL, var_100, var_99, r12_2);
                local_sp_1 = var_101;
                while (r13_0 != var_98)
                    {
                        r13_0 = r13_0 + 8UL;
                        var_99 = local_sp_1 + 176UL;
                        var_100 = *(uint64_t *)r13_0;
                        var_101 = local_sp_1 + (-8L);
                        *(uint64_t *)var_101 = 4208959UL;
                        indirect_placeholder_5(1UL, var_100, var_99, r12_2);
                        local_sp_1 = var_101;
                    }
            }
            return;
        }
        break;
      case 3U:
      case 2U:
        {
            switch (loop_state_var) {
              case 2U:
                {
                    r12_2 = r12_0;
                    local_sp_11 = local_sp_9;
                    local_sp_10 = local_sp_9;
                    rbp_1 = rbp_0;
                    r12_1 = r12_0;
                    rcx_3 = rcx_2;
                    r9_2 = r9_1;
                    r8_2 = r8_1;
                    rbp_2 = rbp_0;
                    if ((uint64_t)(unsigned char)r13_1 != 0UL) {
                        var_90 = (*(uint32_t *)(local_sp_10 + 184UL) == 2U);
                        var_91 = local_sp_10 + (-8L);
                        var_92 = (uint64_t *)var_91;
                        local_sp_11 = var_91;
                        rbp_2 = rbp_1;
                        r12_2 = r12_1;
                        if (var_90) {
                            *var_92 = 4208841UL;
                            indirect_placeholder_246(0UL, 4313888UL, 0UL, rcx_3, 0UL, r9_2, r8_2);
                            *(uint64_t *)(local_sp_10 + (-16L)) = 4208851UL;
                            indirect_placeholder_16(var_48, rbp_1, 1UL);
                            abort();
                        }
                        *var_92 = 4208864UL;
                        var_93 = indirect_placeholder_1(4311364UL, r14_0);
                        rax_0 = (uint32_t)var_93;
                    }
                    *(uint32_t *)(local_sp_11 + 176UL) = rax_0;
                    *(uint64_t *)(local_sp_11 + (-8L)) = 4208886UL;
                    indirect_placeholder_2(r15_0);
                    var_94 = local_sp_11 + (-16L);
                    *(uint64_t *)var_94 = 4208891UL;
                    indirect_placeholder();
                    local_sp_0 = var_94;
                    local_sp_2 = var_94;
                    if (r12_2 != 0UL) {
                        var_102 = *(uint64_t *)(var_48 + 8UL);
                        var_103 = local_sp_2 + 176UL;
                        var_104 = *(uint64_t *)var_48;
                        *(uint64_t *)(local_sp_2 + (-8L)) = 4208997UL;
                        indirect_placeholder_5(0UL, var_104, var_103, var_102);
                        return;
                    }
                    if ((int)(uint32_t)rbp_2 > (int)1U) {
                        var_95 = local_sp_11 + 160UL;
                        var_96 = local_sp_11 + (-24L);
                        *(uint64_t *)var_96 = 4208914UL;
                        indirect_placeholder_2(var_95);
                        local_sp_0 = var_96;
                    }
                    var_97 = helper_cc_compute_all_wrapper(rbp_2, 0UL, 0UL, 24U);
                    local_sp_1 = local_sp_0;
                    if ((uint64_t)(((unsigned char)(var_97 >> 4UL) ^ (unsigned char)var_97) & '\xc0') != 0UL) {
                        var_98 = (((rbp_2 << 3UL) + 34359738360UL) & 34359738360UL) + var_48;
                        var_99 = local_sp_1 + 176UL;
                        var_100 = *(uint64_t *)r13_0;
                        var_101 = local_sp_1 + (-8L);
                        *(uint64_t *)var_101 = 4208959UL;
                        indirect_placeholder_5(1UL, var_100, var_99, r12_2);
                        local_sp_1 = var_101;
                        while (r13_0 != var_98)
                            {
                                r13_0 = r13_0 + 8UL;
                                var_99 = local_sp_1 + 176UL;
                                var_100 = *(uint64_t *)r13_0;
                                var_101 = local_sp_1 + (-8L);
                                *(uint64_t *)var_101 = 4208959UL;
                                indirect_placeholder_5(1UL, var_100, var_99, r12_2);
                                local_sp_1 = var_101;
                            }
                    }
                    return;
                }
                break;
              case 3U:
                {
                    var_68 = ((uint64_t)((long)var_50 >> (long)29UL) + var_48) + (-8L);
                    *(uint64_t *)(local_sp_8 + 16UL) = var_68;
                    var_69 = *(uint64_t *)var_68;
                    *(uint64_t *)(local_sp_8 + 24UL) = var_69;
                    var_70 = local_sp_8 + 32UL;
                    var_71 = local_sp_8 + (-8L);
                    *(uint64_t *)var_71 = 4208660UL;
                    var_72 = indirect_placeholder_1(var_69, var_70);
                    local_sp_3 = var_71;
                    rcx_2 = rcx_1;
                    var_73 = local_sp_8 + (-16L);
                    *(uint64_t *)var_73 = 4208669UL;
                    indirect_placeholder();
                    var_74 = *(uint32_t *)var_72;
                    *(uint32_t *)(local_sp_8 + (-4L)) = var_74;
                    local_sp_3 = var_73;
                    local_sp_4 = var_73;
                    if ((uint64_t)(uint32_t)var_72 == 0UL & var_74 != 0U) {
                        if ((var_74 & (-3)) == 0U) {
                            var_75 = *(uint64_t *)(local_sp_8 + 8UL);
                            *(uint64_t *)(local_sp_8 + (-24L)) = 4208723UL;
                            var_76 = indirect_placeholder_238(4UL, var_75);
                            var_77 = var_76.field_0;
                            var_78 = var_76.field_1;
                            var_79 = var_76.field_2;
                            var_80 = (uint64_t)*(uint32_t *)(local_sp_8 + (-12L));
                            var_81 = local_sp_8 + (-32L);
                            *(uint64_t *)var_81 = 4208750UL;
                            indirect_placeholder_237(0UL, 4311210UL, 1UL, var_77, var_80, var_78, var_79);
                            local_sp_5 = var_81;
                            var_82 = **(uint64_t **)(local_sp_5 + 16UL);
                            *(uint64_t *)(local_sp_5 + (-8L)) = 4208768UL;
                            var_83 = indirect_placeholder_239(4UL, var_82);
                            var_84 = var_83.field_0;
                            var_85 = var_83.field_1;
                            var_86 = var_83.field_2;
                            var_87 = local_sp_5 + (-16L);
                            *(uint64_t *)var_87 = 4208796UL;
                            var_88 = indirect_placeholder_236(0UL, 4311230UL, 1UL, var_84, 0UL, var_85, var_86);
                            local_sp_9 = var_87;
                            rcx_2 = var_88.field_0;
                            r9_1 = var_88.field_1;
                            r8_1 = var_88.field_2;
                        } else {
                            local_sp_5 = local_sp_4;
                            local_sp_9 = local_sp_4;
                            if ((int)var_46 > (int)2U) {
                                var_82 = **(uint64_t **)(local_sp_5 + 16UL);
                                *(uint64_t *)(local_sp_5 + (-8L)) = 4208768UL;
                                var_83 = indirect_placeholder_239(4UL, var_82);
                                var_84 = var_83.field_0;
                                var_85 = var_83.field_1;
                                var_86 = var_83.field_2;
                                var_87 = local_sp_5 + (-16L);
                                *(uint64_t *)var_87 = 4208796UL;
                                var_88 = indirect_placeholder_236(0UL, 4311230UL, 1UL, var_84, 0UL, var_85, var_86);
                                local_sp_9 = var_87;
                                rcx_2 = var_88.field_0;
                                r9_1 = var_88.field_1;
                                r8_1 = var_88.field_2;
                            }
                        }
                    } else {
                        local_sp_4 = local_sp_3;
                        local_sp_9 = local_sp_3;
                        if ((uint32_t)((uint16_t)*(uint32_t *)(local_sp_3 + 56UL) & (unsigned short)61440U) == 16384U) {
                            var_89 = (uint64_t)(var_46 + (-1));
                            rbp_0 = var_89;
                            r12_0 = *(uint64_t *)((uint64_t)((long)(var_89 << 32UL) >> (long)29UL) + var_48);
                        } else {
                            local_sp_5 = local_sp_4;
                            local_sp_9 = local_sp_4;
                            if ((int)var_46 > (int)2U) {
                                var_82 = **(uint64_t **)(local_sp_5 + 16UL);
                                *(uint64_t *)(local_sp_5 + (-8L)) = 4208768UL;
                                var_83 = indirect_placeholder_239(4UL, var_82);
                                var_84 = var_83.field_0;
                                var_85 = var_83.field_1;
                                var_86 = var_83.field_2;
                                var_87 = local_sp_5 + (-16L);
                                *(uint64_t *)var_87 = 4208796UL;
                                var_88 = indirect_placeholder_236(0UL, 4311230UL, 1UL, var_84, 0UL, var_85, var_86);
                                local_sp_9 = var_87;
                                rcx_2 = var_88.field_0;
                                r9_1 = var_88.field_1;
                                r8_1 = var_88.field_2;
                            }
                        }
                    }
                }
                break;
            }
        }
        break;
    }
}

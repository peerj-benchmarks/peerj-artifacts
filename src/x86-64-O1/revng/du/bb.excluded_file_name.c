typedef unsigned char __u_char;
typedef unsigned short __u_short;
typedef unsigned int __u_int;
typedef unsigned long __u_long;
typedef signed char __int8_t;
typedef unsigned char __uint8_t;
typedef short __int16_t;
typedef unsigned short __uint16_t;
typedef int __int32_t;
typedef unsigned int __uint32_t;
typedef long __int64_t;
typedef unsigned long __uint64_t;
typedef __int8_t __int_least8_t;
typedef __uint8_t __uint_least8_t;
typedef __int16_t __int_least16_t;
typedef __uint16_t __uint_least16_t;
typedef __int32_t __int_least32_t;
typedef __uint32_t __uint_least32_t;
typedef __int64_t __int_least64_t;
typedef __uint64_t __uint_least64_t;
typedef long __quad_t;
typedef unsigned long __u_quad_t;
typedef long __intmax_t;
typedef unsigned long __uintmax_t;
typedef unsigned long __dev_t;
typedef unsigned int __uid_t;
typedef unsigned int __gid_t;
typedef unsigned long __ino_t;
typedef unsigned long __ino64_t;
typedef unsigned int __mode_t;
typedef unsigned long __nlink_t;
typedef long __off_t;
typedef long __off64_t;
typedef int __pid_t;
typedef struct {
    int __val[2];
} __fsid_t;
typedef long __clock_t;
typedef unsigned long __rlim_t;
typedef unsigned long __rlim64_t;
typedef unsigned int __id_t;
typedef long __time_t;
typedef unsigned int __useconds_t;
typedef long __suseconds_t;
typedef int __daddr_t;
typedef int __key_t;
typedef int __clockid_t;
typedef void *__timer_t;
typedef long __blksize_t;
typedef long __blkcnt_t;
typedef long __blkcnt64_t;
typedef unsigned long __fsblkcnt_t;
typedef unsigned long __fsblkcnt64_t;
typedef unsigned long __fsfilcnt_t;
typedef unsigned long __fsfilcnt64_t;
typedef long __fsword_t;
typedef long __ssize_t;
typedef long __syscall_slong_t;
typedef unsigned long __syscall_ulong_t;
typedef __off64_t __loff_t;
typedef char *__caddr_t;
typedef long __intptr_t;
typedef unsigned int __socklen_t;
typedef int __sig_atomic_t;
typedef __int8_t int8_t;
typedef __int16_t int16_t;
typedef __int32_t int32_t;
typedef __int64_t int64_t;
typedef __uint8_t uint8_t;
typedef __uint16_t uint16_t;
typedef __uint32_t uint32_t;
typedef __uint64_t uint64_t;
typedef __int_least8_t int_least8_t;
typedef __int_least16_t int_least16_t;
typedef __int_least32_t int_least32_t;
typedef __int_least64_t int_least64_t;
typedef __uint_least8_t uint_least8_t;
typedef __uint_least16_t uint_least16_t;
typedef __uint_least32_t uint_least32_t;
typedef __uint_least64_t uint_least64_t;
typedef signed char int_fast8_t;
typedef long int_fast16_t;
typedef long int_fast32_t;
typedef long int_fast64_t;
typedef unsigned char uint_fast8_t;
typedef unsigned long uint_fast16_t;
typedef unsigned long uint_fast32_t;
typedef unsigned long uint_fast64_t;
typedef long intptr_t;
typedef unsigned long uintptr_t;
typedef __intmax_t intmax_t;
typedef __uintmax_t uintmax_t;
struct bb_excluded_file_name_ret_type;
struct indirect_placeholder_30_ret_type;
struct bb_excluded_file_name_ret_type {
    uint64_t field_0;
    uint64_t field_1;
    uint64_t field_2;
    uint64_t field_3;
    uint64_t field_4;
};
struct indirect_placeholder_30_ret_type {
    uint64_t field_0;
    uint64_t field_1;
    uint64_t field_2;
    uint64_t field_3;
    uint64_t field_4;
    uint64_t field_5;
};
extern void abort(void);
extern uint64_t revng_init_local_sp(uint64_t param_0);
extern uint64_t init_rbx(void);
extern void indirect_placeholder_1(void);
extern uint64_t init_rbp(void);
extern uint64_t init_r14(void);
extern uint64_t init_r13(void);
extern uint64_t init_r12(void);
extern uint64_t indirect_placeholder_5(uint64_t param_0);
extern uint64_t init_r15(void);
extern uint64_t indirect_placeholder_7(uint64_t param_0, uint64_t param_1);
extern uint64_t init_rax(void);
extern uint64_t init_r10(void);
extern uint64_t init_r9(void);
extern uint64_t init_r8(void);
extern uint64_t init_rcx(void);
extern struct indirect_placeholder_30_ret_type indirect_placeholder_30(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint64_t param_3, uint64_t param_4, uint64_t param_5, uint64_t param_6, uint64_t param_7, uint64_t param_8, uint64_t param_9);
extern void indirect_placeholder_31(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint64_t param_3, uint64_t param_4, uint64_t param_5, uint64_t param_6, uint64_t param_7, uint64_t param_8);
typedef _Bool bool;
struct bb_excluded_file_name_ret_type bb_excluded_file_name(uint64_t rdi, uint64_t rsi) {
    uint64_t var_0;
    uint64_t var_1;
    uint64_t var_2;
    uint64_t var_3;
    uint64_t var_4;
    uint64_t var_5;
    uint64_t var_6;
    uint64_t var_7;
    uint64_t var_8;
    uint64_t var_9;
    uint64_t var_10;
    uint64_t var_11;
    uint64_t var_12;
    uint64_t rcx_3;
    uint64_t var_23;
    uint64_t rcx_1;
    uint64_t rbx_2;
    uint64_t local_sp_0;
    uint64_t rax_0;
    uint64_t var_36;
    uint64_t r8_1;
    uint64_t local_sp_2;
    uint64_t var_37;
    uint64_t local_sp_6;
    uint64_t rbx_1;
    uint64_t rbp_2;
    uint64_t local_sp_3;
    uint32_t var_30;
    uint64_t var_31;
    uint64_t var_32;
    uint64_t var_33;
    uint64_t var_34;
    uint64_t var_35;
    uint64_t rcx_2;
    uint64_t var_13;
    uint64_t rbx_3;
    uint64_t rcx_0;
    uint64_t r14_2;
    uint64_t rbx_0;
    uint64_t r14_0;
    uint64_t r15_0;
    uint64_t storemerge;
    struct bb_excluded_file_name_ret_type mrv;
    struct bb_excluded_file_name_ret_type mrv1;
    struct bb_excluded_file_name_ret_type mrv2;
    uint64_t r8_0;
    uint64_t var_27;
    uint64_t var_28;
    uint64_t var_29;
    uint64_t var_14;
    uint64_t r14_1;
    uint64_t rbp_1;
    uint64_t r13_0;
    uint64_t r12_0;
    uint64_t local_sp_5;
    uint32_t var_15;
    uint64_t var_16;
    uint64_t var_17;
    struct indirect_placeholder_30_ret_type var_18;
    uint64_t var_19;
    uint64_t var_20;
    uint64_t var_21;
    uint64_t var_22;
    uint64_t var_24;
    uint64_t var_25;
    uint64_t var_26;
    uint64_t var_38;
    uint64_t local_sp_7;
    uint64_t r8_2;
    uint64_t r8_3;
    struct bb_excluded_file_name_ret_type mrv3;
    struct bb_excluded_file_name_ret_type mrv4;
    unsigned int loop_state_var;
    bool switch_state_var;
    var_0 = revng_init_local_sp(0UL);
    var_1 = init_rax();
    var_2 = init_rcx();
    var_3 = init_rbx();
    var_4 = init_r14();
    var_5 = init_rbp();
    var_6 = init_r13();
    var_7 = init_r12();
    var_8 = init_r15();
    var_9 = init_r10();
    var_10 = init_r9();
    var_11 = init_r8();
    *(uint64_t *)(var_0 + (-8L)) = var_8;
    *(uint64_t *)(var_0 + (-16L)) = var_4;
    *(uint64_t *)(var_0 + (-24L)) = var_6;
    *(uint64_t *)(var_0 + (-32L)) = var_7;
    *(uint64_t *)(var_0 + (-40L)) = var_5;
    *(uint64_t *)(var_0 + (-48L)) = var_3;
    var_12 = *(uint64_t *)rdi;
    rcx_3 = var_2;
    rax_0 = var_1;
    rbp_2 = 0UL;
    rcx_2 = 0UL;
    rcx_0 = var_2;
    rbx_0 = 0UL;
    r14_0 = var_4;
    r15_0 = var_12;
    storemerge = 0UL;
    r8_0 = var_11;
    r12_0 = 0UL;
    r8_2 = 0UL;
    r8_3 = var_11;
    if (var_12 == 0UL) {
        mrv.field_0 = storemerge;
        mrv1 = mrv;
        mrv1.field_1 = rcx_3;
        mrv2 = mrv1;
        mrv2.field_2 = var_9;
        mrv3 = mrv2;
        mrv3.field_3 = var_10;
        mrv4 = mrv3;
        mrv4.field_4 = r8_3;
        return mrv4;
    }
    var_13 = var_0 + (-72L);
    *(uint64_t *)(var_0 + (-64L)) = rsi;
    local_sp_2 = var_13;
    while (1U)
        {
            rcx_1 = rcx_0;
            rbx_2 = rbx_0;
            r8_1 = r8_0;
            local_sp_6 = local_sp_2;
            rbx_1 = rbx_0;
            local_sp_3 = local_sp_2;
            rbx_3 = rbx_0;
            r14_2 = r14_0;
            r14_1 = r14_0;
            local_sp_5 = local_sp_2;
            if (*(uint32_t *)(r15_0 + 8UL) != 0U) {
                var_14 = *(uint64_t *)(r15_0 + 32UL);
                r13_0 = var_14;
                if (var_14 != 0UL) {
                    rbp_1 = *(uint64_t *)(r15_0 + 16UL) + 8UL;
                    rcx_1 = 0UL;
                    r8_1 = 0UL;
                    while (1U)
                        {
                            var_15 = *(uint32_t *)(rbp_1 + (-8L));
                            if ((var_15 & 134217728U) != 0U) {
                                var_16 = *(uint64_t *)(local_sp_5 + 8UL);
                                var_17 = local_sp_5 + (-8L);
                                *(uint64_t *)var_17 = 4214082UL;
                                var_18 = indirect_placeholder_30(0UL, 0UL, rbx_2, r14_1, rbp_1, rbp_1, r13_0, r12_0, var_16, 0UL);
                                var_19 = var_18.field_0;
                                var_20 = var_18.field_1;
                                var_21 = var_18.field_2;
                                var_22 = var_18.field_4;
                                rbx_2 = var_20;
                                local_sp_6 = var_17;
                                rbx_3 = var_20;
                                r14_2 = var_21;
                                r14_1 = var_21;
                                r13_0 = var_22;
                                local_sp_5 = var_17;
                                local_sp_7 = var_17;
                                if ((uint64_t)(uint32_t)var_19 != 0UL) {
                                    loop_state_var = 0U;
                                    break;
                                }
                                var_23 = var_18.field_5 + 1UL;
                                r12_0 = var_23;
                                if (var_23 != var_22) {
                                    loop_state_var = 1U;
                                    break;
                                }
                                rbp_1 = var_18.field_3 + 72UL;
                                continue;
                            }
                            var_24 = (uint64_t)var_15;
                            var_25 = *(uint64_t *)(local_sp_5 + 8UL);
                            var_26 = *(uint64_t *)rbp_1;
                            *(uint64_t *)(local_sp_5 + (-8L)) = 4214103UL;
                            indirect_placeholder_31(var_24, rbx_2, r14_1, rbp_1, var_26, r13_0, r12_0, r15_0, var_25);
                            abort();
                        }
                    switch_state_var = 0;
                    switch (loop_state_var) {
                      case 0U:
                        {
                            switch_state_var = 1;
                            break;
                        }
                        break;
                      case 1U:
                        {
                            break;
                        }
                        break;
                    }
                    if (switch_state_var)
                        break;
                }
            }
            rcx_2 = rcx_0;
            r8_2 = r8_0;
            if (rbx_0 == 0UL) {
                *(uint64_t *)(local_sp_2 + (-8L)) = 4213883UL;
                indirect_placeholder_1();
                var_27 = rax_0 + 1UL;
                var_28 = local_sp_2 + (-16L);
                *(uint64_t *)var_28 = 4213892UL;
                var_29 = indirect_placeholder_5(var_27);
                rbx_1 = var_29;
                local_sp_3 = var_28;
            }
            var_30 = *(uint32_t *)(r15_0 + 12UL);
            var_31 = (uint64_t)var_30;
            var_32 = *(uint64_t *)(r15_0 + 16UL);
            var_33 = (uint64_t)(var_30 & 1073741824U);
            *(uint64_t *)(local_sp_3 + (-8L)) = 4213933UL;
            indirect_placeholder_1();
            var_34 = local_sp_3 + (-16L);
            *(uint64_t *)var_34 = 4213944UL;
            var_35 = indirect_placeholder_7(var_32, rbx_1);
            local_sp_0 = var_34;
            rbx_3 = rbx_1;
            r14_2 = var_33;
            local_sp_7 = var_34;
            if (var_35 == 0UL) {
                break;
            }
            if ((var_31 & 8UL) == 0UL) {
                var_36 = local_sp_3 + (-24L);
                *(uint64_t *)var_36 = 4213971UL;
                indirect_placeholder_1();
                local_sp_0 = var_36;
            }
            local_sp_6 = local_sp_0;
            if (var_33 == 0UL) {
                var_37 = local_sp_0 + (-8L);
                *(uint64_t *)var_37 = 4214003UL;
                indirect_placeholder_1();
                r14_2 = 0UL;
                local_sp_6 = var_37;
            }
        }
    *(uint64_t *)(local_sp_7 + (-8L)) = 4214159UL;
    indirect_placeholder_1();
    rcx_3 = rcx_2;
    r8_3 = r8_2;
    storemerge = (uint64_t)(uint32_t)rbp_2 ^ (uint64_t)(((*(uint32_t *)(r15_0 + 12UL) >> 29U) & 1U) ^ 1U);
    mrv.field_0 = storemerge;
    mrv1 = mrv;
    mrv1.field_1 = rcx_3;
    mrv2 = mrv1;
    mrv2.field_2 = var_9;
    mrv3 = mrv2;
    mrv3.field_3 = var_10;
    mrv4 = mrv3;
    mrv4.field_4 = r8_3;
    return mrv4;
}

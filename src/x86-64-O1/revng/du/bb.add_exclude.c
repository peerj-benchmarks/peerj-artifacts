typedef unsigned char __u_char;
typedef unsigned short __u_short;
typedef unsigned int __u_int;
typedef unsigned long __u_long;
typedef signed char __int8_t;
typedef unsigned char __uint8_t;
typedef short __int16_t;
typedef unsigned short __uint16_t;
typedef int __int32_t;
typedef unsigned int __uint32_t;
typedef long __int64_t;
typedef unsigned long __uint64_t;
typedef __int8_t __int_least8_t;
typedef __uint8_t __uint_least8_t;
typedef __int16_t __int_least16_t;
typedef __uint16_t __uint_least16_t;
typedef __int32_t __int_least32_t;
typedef __uint32_t __uint_least32_t;
typedef __int64_t __int_least64_t;
typedef __uint64_t __uint_least64_t;
typedef long __quad_t;
typedef unsigned long __u_quad_t;
typedef long __intmax_t;
typedef unsigned long __uintmax_t;
typedef unsigned long __dev_t;
typedef unsigned int __uid_t;
typedef unsigned int __gid_t;
typedef unsigned long __ino_t;
typedef unsigned long __ino64_t;
typedef unsigned int __mode_t;
typedef unsigned long __nlink_t;
typedef long __off_t;
typedef long __off64_t;
typedef int __pid_t;
typedef struct {
    int __val[2];
} __fsid_t;
typedef long __clock_t;
typedef unsigned long __rlim_t;
typedef unsigned long __rlim64_t;
typedef unsigned int __id_t;
typedef long __time_t;
typedef unsigned int __useconds_t;
typedef long __suseconds_t;
typedef int __daddr_t;
typedef int __key_t;
typedef int __clockid_t;
typedef void *__timer_t;
typedef long __blksize_t;
typedef long __blkcnt_t;
typedef long __blkcnt64_t;
typedef unsigned long __fsblkcnt_t;
typedef unsigned long __fsblkcnt64_t;
typedef unsigned long __fsfilcnt_t;
typedef unsigned long __fsfilcnt64_t;
typedef long __fsword_t;
typedef long __ssize_t;
typedef long __syscall_slong_t;
typedef unsigned long __syscall_ulong_t;
typedef __off64_t __loff_t;
typedef char *__caddr_t;
typedef long __intptr_t;
typedef unsigned int __socklen_t;
typedef int __sig_atomic_t;
typedef __int8_t int8_t;
typedef __int16_t int16_t;
typedef __int32_t int32_t;
typedef __int64_t int64_t;
typedef __uint8_t uint8_t;
typedef __uint16_t uint16_t;
typedef __uint32_t uint32_t;
typedef __uint64_t uint64_t;
typedef __int_least8_t int_least8_t;
typedef __int_least16_t int_least16_t;
typedef __int_least32_t int_least32_t;
typedef __int_least64_t int_least64_t;
typedef __uint_least8_t uint_least8_t;
typedef __uint_least16_t uint_least16_t;
typedef __uint_least32_t uint_least32_t;
typedef __uint_least64_t uint_least64_t;
typedef signed char int_fast8_t;
typedef long int_fast16_t;
typedef long int_fast32_t;
typedef long int_fast64_t;
typedef unsigned char uint_fast8_t;
typedef unsigned long uint_fast16_t;
typedef unsigned long uint_fast32_t;
typedef unsigned long uint_fast64_t;
typedef long intptr_t;
typedef unsigned long uintptr_t;
typedef __intmax_t intmax_t;
typedef __uintmax_t uintmax_t;
typedef _Bool bool;
struct indirect_placeholder_78_ret_type;
struct indirect_placeholder_79_ret_type;
struct indirect_placeholder_80_ret_type;
struct indirect_placeholder_81_ret_type;
struct indirect_placeholder_78_ret_type {
    uint64_t field_0;
    uint64_t field_1;
    uint64_t field_2;
    uint64_t field_3;
    uint64_t field_4;
};
struct indirect_placeholder_79_ret_type {
    uint64_t field_0;
    uint64_t field_1;
    uint64_t field_2;
};
struct indirect_placeholder_80_ret_type {
    uint64_t field_0;
    uint64_t field_1;
};
struct indirect_placeholder_81_ret_type {
    uint64_t field_0;
    uint64_t field_1;
    uint64_t field_2;
};
extern void abort(void);
extern uint64_t revng_init_local_sp(uint64_t param_0);
extern uint64_t init_rbx(void);
extern void indirect_placeholder_1(void);
extern uint64_t init_rbp(void);
extern uint64_t init_cc_src2(void);
extern uint64_t helper_cc_compute_c_wrapper(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint32_t param_3);
extern uint64_t init_r14(void);
extern uint64_t init_r13(void);
extern uint64_t init_r12(void);
extern uint64_t indirect_placeholder_5(uint64_t param_0);
extern uint64_t indirect_placeholder_6(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint64_t param_3, uint64_t param_4);
extern uint64_t init_r15(void);
extern uint64_t indirect_placeholder_7(uint64_t param_0, uint64_t param_1);
extern void indirect_placeholder_4(uint64_t param_0, uint64_t param_1);
extern uint64_t indirect_placeholder(uint64_t param_0, uint64_t param_1, uint64_t param_2);
extern uint64_t init_r9(void);
extern struct indirect_placeholder_78_ret_type indirect_placeholder_78(uint64_t param_0, uint64_t param_1);
extern struct indirect_placeholder_79_ret_type indirect_placeholder_79(uint64_t param_0);
extern struct indirect_placeholder_80_ret_type indirect_placeholder_80(uint64_t param_0, uint64_t param_1);
extern struct indirect_placeholder_81_ret_type indirect_placeholder_81(uint64_t param_0);
void bb_add_exclude(uint64_t rdx, uint64_t rdi, uint64_t rsi) {
    uint64_t var_52;
    uint64_t var_0;
    uint64_t var_1;
    uint64_t var_2;
    uint64_t var_3;
    uint64_t var_4;
    uint64_t var_5;
    uint64_t var_6;
    uint64_t var_7;
    uint64_t var_8;
    uint64_t var_9;
    uint32_t var_10;
    uint64_t local_sp_2;
    uint64_t var_53;
    uint64_t rax_0;
    uint64_t rcx_0;
    uint64_t rbx_1_in;
    uint64_t rdx1_0;
    uint64_t var_55;
    unsigned char var_56;
    uint64_t var_57;
    uint64_t var_58;
    uint64_t rbx_2;
    uint64_t rbx_0_in;
    uint64_t rbx_1;
    uint64_t var_37;
    uint64_t var_38;
    uint64_t var_39;
    uint64_t var_40;
    uint64_t *var_15;
    uint64_t var_16;
    uint64_t var_22;
    uint64_t local_sp_0;
    uint64_t r9_0;
    uint64_t r8_0;
    uint64_t *var_23;
    uint64_t var_24;
    uint64_t *var_25;
    uint64_t *_pre_phi133;
    uint64_t *var_26;
    uint64_t var_27;
    uint64_t rax_1;
    uint64_t var_28;
    uint64_t var_29;
    struct indirect_placeholder_78_ret_type var_30;
    uint64_t var_31;
    uint64_t local_sp_1;
    uint64_t var_32;
    uint64_t var_33;
    uint64_t var_34;
    uint64_t var_35;
    uint64_t var_36;
    uint64_t var_41;
    uint64_t var_42;
    uint64_t r13_0;
    uint64_t var_43;
    uint64_t var_17;
    struct indirect_placeholder_79_ret_type var_18;
    uint64_t var_19;
    uint64_t var_20;
    uint64_t var_21;
    uint64_t var_11;
    uint64_t var_12;
    struct indirect_placeholder_80_ret_type var_13;
    uint64_t var_14;
    uint64_t *var_44;
    uint64_t var_45;
    uint64_t local_sp_3;
    uint64_t var_54;
    struct indirect_placeholder_81_ret_type var_46;
    uint64_t var_47;
    bool var_48;
    uint64_t var_49;
    uint64_t var_50;
    uint64_t var_51;
    unsigned int loop_state_var;
    var_0 = revng_init_local_sp(0UL);
    var_1 = init_cc_src2();
    var_2 = init_rbx();
    var_3 = init_r14();
    var_4 = init_rbp();
    var_5 = init_r13();
    var_6 = init_r12();
    var_7 = init_r15();
    var_8 = init_r9();
    *(uint64_t *)(var_0 + (-8L)) = var_7;
    *(uint64_t *)(var_0 + (-16L)) = var_3;
    *(uint64_t *)(var_0 + (-24L)) = var_5;
    *(uint64_t *)(var_0 + (-32L)) = var_6;
    *(uint64_t *)(var_0 + (-40L)) = var_4;
    *(uint64_t *)(var_0 + (-48L)) = var_2;
    var_9 = var_0 + (-56L);
    var_10 = (uint32_t)rdx;
    local_sp_2 = var_9;
    rdx1_0 = 0UL;
    r9_0 = var_8;
    rax_1 = 1UL;
    r13_0 = rsi;
    if ((uint64_t)(var_10 & 402653184U) == 0UL) {
        var_44 = (uint64_t *)rdi;
        var_45 = *var_44;
        var_53 = var_45;
        local_sp_3 = local_sp_2;
        if (var_45 == 0UL) {
            *(uint64_t *)(local_sp_2 + (-8L)) = 4214651UL;
            var_46 = indirect_placeholder_81(40UL);
            var_47 = var_46.field_0;
            *(uint32_t *)(var_47 + 8UL) = 0U;
            *(uint32_t *)(var_47 + 12UL) = var_10;
            var_48 = ((rdx & 16UL) == 0UL);
            var_49 = var_48 ? 4212797UL : 4212778UL;
            var_50 = var_48 ? 4213015UL : 4213029UL;
            var_51 = local_sp_2 + (-16L);
            *(uint64_t *)var_51 = 4214718UL;
            var_52 = indirect_placeholder_6(var_49, var_50, 0UL, 0UL, 4212764UL);
            *(uint64_t *)(var_47 + 16UL) = var_52;
            *(uint64_t *)var_47 = *var_44;
            *var_44 = var_47;
            var_53 = var_47;
            local_sp_3 = var_51;
        } else {
            if (*(uint32_t *)(var_45 + 8UL) == 0U) {
                *(uint64_t *)(local_sp_2 + (-8L)) = 4214651UL;
                var_46 = indirect_placeholder_81(40UL);
                var_47 = var_46.field_0;
                *(uint32_t *)(var_47 + 8UL) = 0U;
                *(uint32_t *)(var_47 + 12UL) = var_10;
                var_48 = ((rdx & 16UL) == 0UL);
                var_49 = var_48 ? 4212797UL : 4212778UL;
                var_50 = var_48 ? 4213015UL : 4213029UL;
                var_51 = local_sp_2 + (-16L);
                *(uint64_t *)var_51 = 4214718UL;
                var_52 = indirect_placeholder_6(var_49, var_50, 0UL, 0UL, 4212764UL);
                *(uint64_t *)(var_47 + 16UL) = var_52;
                *(uint64_t *)var_47 = *var_44;
                *var_44 = var_47;
                var_53 = var_47;
                local_sp_3 = var_51;
            } else {
                if ((uint64_t)((*(uint32_t *)(var_45 + 12UL) ^ var_10) & 1610612760U) == 0UL) {
                    *(uint64_t *)(local_sp_2 + (-8L)) = 4214651UL;
                    var_46 = indirect_placeholder_81(40UL);
                    var_47 = var_46.field_0;
                    *(uint32_t *)(var_47 + 8UL) = 0U;
                    *(uint32_t *)(var_47 + 12UL) = var_10;
                    var_48 = ((rdx & 16UL) == 0UL);
                    var_49 = var_48 ? 4212797UL : 4212778UL;
                    var_50 = var_48 ? 4213015UL : 4213029UL;
                    var_51 = local_sp_2 + (-16L);
                    *(uint64_t *)var_51 = 4214718UL;
                    var_52 = indirect_placeholder_6(var_49, var_50, 0UL, 0UL, 4212764UL);
                    *(uint64_t *)(var_47 + 16UL) = var_52;
                    *(uint64_t *)var_47 = *var_44;
                    *var_44 = var_47;
                    var_53 = var_47;
                    local_sp_3 = var_51;
                }
            }
        }
        *(uint64_t *)(local_sp_3 + (-8L)) = 4214747UL;
        var_54 = indirect_placeholder_5(rsi);
        rax_0 = var_54;
        rcx_0 = var_54;
        if ((uint64_t)(((var_10 & 268435458U) + (-268435456)) & (-268435454)) == 0UL) {
            while (1U)
                {
                    if (*(unsigned char *)rax_0 == '\\') {
                        rdx1_0 = (*(unsigned char *)(rax_0 + 1UL) != '\x00');
                    }
                    var_55 = rdx1_0 + rax_0;
                    var_56 = *(unsigned char *)var_55;
                    *(unsigned char *)rcx_0 = var_56;
                    if (var_56 == '\x00') {
                        break;
                    }
                    rax_0 = var_55 + 1UL;
                    rcx_0 = rcx_0 + 1UL;
                    continue;
                }
        }
        var_57 = *(uint64_t *)(var_53 + 16UL);
        *(uint64_t *)(local_sp_3 + (-16L)) = 4214824UL;
        var_58 = indirect_placeholder_7(var_57, var_54);
        if (var_58 != var_54) {
            *(uint64_t *)(local_sp_3 + (-24L)) = 4214837UL;
            indirect_placeholder_1();
        }
        return;
    }
    var_11 = (uint64_t)var_10;
    var_12 = var_0 + (-64L);
    *(uint64_t *)var_12 = 4214240UL;
    var_13 = indirect_placeholder_80(rsi, var_11);
    var_14 = var_13.field_1;
    local_sp_0 = var_12;
    r8_0 = var_14;
    local_sp_2 = var_12;
    if ((uint64_t)(unsigned char)var_13.field_0 == 0UL) {
        return;
    }
    var_15 = (uint64_t *)rdi;
    var_16 = *var_15;
    var_22 = var_16;
    if (var_16 == 0UL) {
        var_17 = var_0 + (-72L);
        *(uint64_t *)var_17 = 4214287UL;
        var_18 = indirect_placeholder_79(40UL);
        var_19 = var_18.field_0;
        var_20 = var_18.field_1;
        var_21 = var_18.field_2;
        *(uint32_t *)(var_19 + 8UL) = 1U;
        *(uint32_t *)(var_19 + 12UL) = var_10;
        *(uint64_t *)var_19 = *var_15;
        *var_15 = var_19;
        var_22 = var_19;
        local_sp_0 = var_17;
        r9_0 = var_20;
        r8_0 = var_21;
    } else {
        if (*(uint32_t *)(var_16 + 8UL) == 1U) {
            var_17 = var_0 + (-72L);
            *(uint64_t *)var_17 = 4214287UL;
            var_18 = indirect_placeholder_79(40UL);
            var_19 = var_18.field_0;
            var_20 = var_18.field_1;
            var_21 = var_18.field_2;
            *(uint32_t *)(var_19 + 8UL) = 1U;
            *(uint32_t *)(var_19 + 12UL) = var_10;
            *(uint64_t *)var_19 = *var_15;
            *var_15 = var_19;
            var_22 = var_19;
            local_sp_0 = var_17;
            r9_0 = var_20;
            r8_0 = var_21;
        } else {
            if ((uint64_t)((*(uint32_t *)(var_16 + 12UL) ^ var_10) & 536870912U) == 0UL) {
                var_17 = var_0 + (-72L);
                *(uint64_t *)var_17 = 4214287UL;
                var_18 = indirect_placeholder_79(40UL);
                var_19 = var_18.field_0;
                var_20 = var_18.field_1;
                var_21 = var_18.field_2;
                *(uint32_t *)(var_19 + 8UL) = 1U;
                *(uint32_t *)(var_19 + 12UL) = var_10;
                *(uint64_t *)var_19 = *var_15;
                *var_15 = var_19;
                var_22 = var_19;
                local_sp_0 = var_17;
                r9_0 = var_20;
                r8_0 = var_21;
            }
        }
    }
    var_23 = (uint64_t *)(var_22 + 32UL);
    var_24 = *var_23;
    var_25 = (uint64_t *)(var_22 + 24UL);
    var_31 = var_24;
    local_sp_1 = local_sp_0;
    if (var_24 == *var_25) {
        var_26 = (uint64_t *)(var_22 + 16UL);
        var_27 = *var_26;
        _pre_phi133 = var_26;
        if (var_27 == 0UL) {
            rax_1 = var_24;
            if (var_24 != 0UL & var_24 > 128102389400760775UL) {
                *(uint64_t *)(local_sp_0 + (-8L)) = 4214359UL;
                indirect_placeholder_4(r9_0, r8_0);
                abort();
            }
        }
        if (var_24 > 85401592933840515UL) {
            *(uint64_t *)(local_sp_0 + (-8L)) = 4214379UL;
            indirect_placeholder_4(r9_0, r8_0);
            abort();
        }
        rax_1 = (var_24 + (var_24 >> 1UL)) + 1UL;
        *var_25 = rax_1;
        var_28 = rax_1 * 72UL;
        var_29 = local_sp_0 + (-8L);
        *(uint64_t *)var_29 = 4214415UL;
        var_30 = indirect_placeholder_78(var_27, var_28);
        *var_26 = var_30.field_0;
        var_31 = *var_23;
        local_sp_1 = var_29;
    } else {
        _pre_phi133 = (uint64_t *)(var_22 + 16UL);
        var_32 = *_pre_phi133;
        *var_23 = (var_31 + 1UL);
        var_33 = (var_31 * 72UL) + var_32;
        *(uint32_t *)var_33 = var_10;
        if ((uint64_t)(var_10 & 134217728U) == 0UL) {
            if ((uint64_t)(var_10 & 67108864U) != 0UL) {
                *(uint64_t *)(local_sp_1 + (-8L)) = 4214589UL;
                var_43 = indirect_placeholder_5(rsi);
                *(uint64_t *)(local_sp_1 + (-16L)) = 4214603UL;
                indirect_placeholder_4(rdi, var_43);
                r13_0 = var_43;
            }
            *(uint64_t *)(var_33 + 8UL) = r13_0;
        } else {
            var_34 = rdx & 16UL;
            var_35 = helper_cc_compute_c_wrapper(var_34 + (-1L), 1UL, var_1, 16U);
            var_36 = (uint64_t)(((0U - (uint32_t)var_35) & (-2)) + 11U);
            rbx_1_in = var_34;
            rbx_2 = var_34;
            if ((rdx & 8UL) == 0UL) {
                var_41 = var_33 + 8UL;
                *(uint64_t *)(local_sp_1 + (-8L)) = 4214552UL;
                var_42 = indirect_placeholder(var_36, var_41, rsi);
                rbx_0_in = var_42;
            } else {
                *(uint64_t *)(local_sp_1 + (-8L)) = 4214487UL;
                indirect_placeholder_1();
                if (var_34 != 0UL) {
                    *var_23 = (*var_23 + (-1L));
                    return;
                }
                if (*(unsigned char *)((var_34 + rsi) + (-1L)) != '/') {
                    while (1U)
                        {
                            rbx_1 = rbx_1_in + (-1L);
                            rbx_1_in = rbx_1;
                            rbx_2 = rbx_1;
                            if (rbx_1 != 0UL) {
                                loop_state_var = 1U;
                                break;
                            }
                            if (*(unsigned char *)((rbx_1 + rsi) + (-1L)) == '/') {
                                continue;
                            }
                            loop_state_var = 0U;
                            break;
                        }
                    switch (loop_state_var) {
                      case 0U:
                        {
                            break;
                        }
                        break;
                      case 1U:
                        {
                            *var_23 = (*var_23 + (-1L));
                            return;
                        }
                        break;
                    }
                }
                var_37 = rbx_2 + 7UL;
                *(uint64_t *)(local_sp_1 + (-16L)) = 4214848UL;
                var_38 = indirect_placeholder_5(var_37);
                *(uint64_t *)(local_sp_1 + (-24L)) = 4214865UL;
                indirect_placeholder_1();
                *(uint64_t *)(local_sp_1 + (-32L)) = 4214879UL;
                indirect_placeholder_1();
                var_39 = var_33 + 8UL;
                *(uint64_t *)(local_sp_1 + (-40L)) = 4214893UL;
                var_40 = indirect_placeholder(var_36, var_39, var_38);
                *(uint64_t *)(local_sp_1 + (-48L)) = 4214903UL;
                indirect_placeholder_1();
                rbx_0_in = var_40;
            }
        }
    }
}

typedef unsigned char __u_char;
typedef unsigned short __u_short;
typedef unsigned int __u_int;
typedef unsigned long __u_long;
typedef signed char __int8_t;
typedef unsigned char __uint8_t;
typedef short __int16_t;
typedef unsigned short __uint16_t;
typedef int __int32_t;
typedef unsigned int __uint32_t;
typedef long __int64_t;
typedef unsigned long __uint64_t;
typedef __int8_t __int_least8_t;
typedef __uint8_t __uint_least8_t;
typedef __int16_t __int_least16_t;
typedef __uint16_t __uint_least16_t;
typedef __int32_t __int_least32_t;
typedef __uint32_t __uint_least32_t;
typedef __int64_t __int_least64_t;
typedef __uint64_t __uint_least64_t;
typedef long __quad_t;
typedef unsigned long __u_quad_t;
typedef long __intmax_t;
typedef unsigned long __uintmax_t;
typedef unsigned long __dev_t;
typedef unsigned int __uid_t;
typedef unsigned int __gid_t;
typedef unsigned long __ino_t;
typedef unsigned long __ino64_t;
typedef unsigned int __mode_t;
typedef unsigned long __nlink_t;
typedef long __off_t;
typedef long __off64_t;
typedef int __pid_t;
typedef struct {
    int __val[2];
} __fsid_t;
typedef long __clock_t;
typedef unsigned long __rlim_t;
typedef unsigned long __rlim64_t;
typedef unsigned int __id_t;
typedef long __time_t;
typedef unsigned int __useconds_t;
typedef long __suseconds_t;
typedef int __daddr_t;
typedef int __key_t;
typedef int __clockid_t;
typedef void *__timer_t;
typedef long __blksize_t;
typedef long __blkcnt_t;
typedef long __blkcnt64_t;
typedef unsigned long __fsblkcnt_t;
typedef unsigned long __fsblkcnt64_t;
typedef unsigned long __fsfilcnt_t;
typedef unsigned long __fsfilcnt64_t;
typedef long __fsword_t;
typedef long __ssize_t;
typedef long __syscall_slong_t;
typedef unsigned long __syscall_ulong_t;
typedef __off64_t __loff_t;
typedef char *__caddr_t;
typedef long __intptr_t;
typedef unsigned int __socklen_t;
typedef int __sig_atomic_t;
typedef __int8_t int8_t;
typedef __int16_t int16_t;
typedef __int32_t int32_t;
typedef __int64_t int64_t;
typedef __uint8_t uint8_t;
typedef __uint16_t uint16_t;
typedef __uint32_t uint32_t;
typedef __uint64_t uint64_t;
typedef __int_least8_t int_least8_t;
typedef __int_least16_t int_least16_t;
typedef __int_least32_t int_least32_t;
typedef __int_least64_t int_least64_t;
typedef __uint_least8_t uint_least8_t;
typedef __uint_least16_t uint_least16_t;
typedef __uint_least32_t uint_least32_t;
typedef __uint_least64_t uint_least64_t;
typedef signed char int_fast8_t;
typedef long int_fast16_t;
typedef long int_fast32_t;
typedef long int_fast64_t;
typedef unsigned char uint_fast8_t;
typedef unsigned long uint_fast16_t;
typedef unsigned long uint_fast32_t;
typedef unsigned long uint_fast64_t;
typedef long intptr_t;
typedef unsigned long uintptr_t;
typedef __intmax_t intmax_t;
typedef __uintmax_t uintmax_t;
typedef _Bool bool;
extern uint64_t revng_init_local_sp(uint64_t param_0);
extern uint64_t init_rbx(void);
extern void indirect_placeholder_1(void);
extern uint64_t init_rbp(void);
extern uint64_t init_r13(void);
extern uint64_t init_r12(void);
extern uint64_t indirect_placeholder_5(uint64_t param_0);
extern uint64_t helper_cc_compute_all_wrapper(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint32_t param_3);
extern uint64_t indirect_placeholder_7(uint64_t param_0, uint64_t param_1);
extern uint64_t init_rax(void);
uint64_t bb_fnmatch_no_wildcards(uint64_t rdx, uint64_t rdi, uint64_t rsi) {
    uint64_t var_0;
    uint64_t var_1;
    uint64_t var_2;
    uint64_t var_3;
    uint64_t var_4;
    uint64_t var_5;
    bool var_6;
    bool var_7;
    uint64_t var_8;
    uint64_t *var_9;
    uint64_t local_sp_0;
    uint64_t rbp_0_in;
    unsigned char *var_11;
    uint64_t rax_0;
    uint64_t var_12;
    uint64_t var_13;
    uint64_t var_14;
    uint64_t var_15;
    uint64_t var_16;
    uint64_t local_sp_1;
    uint64_t var_17;
    uint64_t rbp_1_in;
    uint64_t var_18;
    uint64_t var_10;
    var_0 = revng_init_local_sp(0UL);
    var_1 = init_rax();
    var_2 = init_rbx();
    var_3 = init_rbp();
    var_4 = init_r13();
    var_5 = init_r12();
    *(uint64_t *)(var_0 + (-8L)) = var_4;
    *(uint64_t *)(var_0 + (-16L)) = var_5;
    *(uint64_t *)(var_0 + (-24L)) = var_3;
    *(uint64_t *)(var_0 + (-32L)) = var_2;
    var_6 = ((rdx & 8UL) == 0UL);
    var_7 = ((rdx & 16UL) == 0UL);
    var_8 = var_0 + (-48L);
    var_9 = (uint64_t *)var_8;
    local_sp_1 = var_8;
    rbp_1_in = var_1;
    if (var_6) {
        if (var_7) {
            *var_9 = 4212859UL;
            indirect_placeholder_1();
        } else {
            *var_9 = 4212847UL;
            var_18 = indirect_placeholder_7(rdi, rsi);
            rbp_1_in = var_18;
        }
    } else {
        if (var_7) {
            *var_9 = 4212878UL;
            indirect_placeholder_1();
            *(uint64_t *)(var_0 + (-56L)) = 4212895UL;
            indirect_placeholder_1();
            if ((uint64_t)(uint32_t)var_1 == 0UL) {
                var_17 = (uint64_t)*(unsigned char *)(var_1 + rsi);
                rbp_1_in = ((uint64_t)((uint32_t)var_17 + (-47)) == 0UL) ? 0UL : var_17;
            }
        } else {
            *var_9 = 4212920UL;
            var_10 = indirect_placeholder_5(rsi);
            rax_0 = var_10;
            while (1U)
                {
                    *(uint64_t *)(local_sp_1 + (-8L)) = 4212936UL;
                    indirect_placeholder_1();
                    if (rax_0 == 0UL) {
                        var_11 = (unsigned char *)rax_0;
                        *var_11 = (unsigned char)'\x00';
                        var_12 = local_sp_1 + (-16L);
                        *(uint64_t *)var_12 = 4212958UL;
                        var_13 = indirect_placeholder_7(rdi, var_10);
                        var_14 = helper_cc_compute_all_wrapper(var_13, 0UL, 0UL, 24U);
                        local_sp_0 = var_12;
                        rbp_0_in = var_13;
                        local_sp_1 = var_12;
                        rax_0 = var_13;
                        if ((uint64_t)(((unsigned char)(var_14 >> 4UL) ^ (unsigned char)var_14) & '\xc0') == 0UL) {
                            break;
                        }
                        *var_11 = (unsigned char)'/';
                        continue;
                    }
                    var_15 = local_sp_1 + (-16L);
                    *(uint64_t *)var_15 = 4212998UL;
                    var_16 = indirect_placeholder_7(rdi, var_10);
                    local_sp_0 = var_15;
                    rbp_0_in = var_16;
                    break;
                }
            *(uint64_t *)(local_sp_0 + (-8L)) = 4212981UL;
            indirect_placeholder_1();
            rbp_1_in = rbp_0_in;
        }
    }
    return (uint64_t)(uint32_t)rbp_1_in;
}

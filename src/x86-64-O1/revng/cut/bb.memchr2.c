typedef unsigned char __u_char;
typedef unsigned short __u_short;
typedef unsigned int __u_int;
typedef unsigned long __u_long;
typedef signed char __int8_t;
typedef unsigned char __uint8_t;
typedef short __int16_t;
typedef unsigned short __uint16_t;
typedef int __int32_t;
typedef unsigned int __uint32_t;
typedef long __int64_t;
typedef unsigned long __uint64_t;
typedef __int8_t __int_least8_t;
typedef __uint8_t __uint_least8_t;
typedef __int16_t __int_least16_t;
typedef __uint16_t __uint_least16_t;
typedef __int32_t __int_least32_t;
typedef __uint32_t __uint_least32_t;
typedef __int64_t __int_least64_t;
typedef __uint64_t __uint_least64_t;
typedef long __quad_t;
typedef unsigned long __u_quad_t;
typedef long __intmax_t;
typedef unsigned long __uintmax_t;
typedef unsigned long __dev_t;
typedef unsigned int __uid_t;
typedef unsigned int __gid_t;
typedef unsigned long __ino_t;
typedef unsigned long __ino64_t;
typedef unsigned int __mode_t;
typedef unsigned long __nlink_t;
typedef long __off_t;
typedef long __off64_t;
typedef int __pid_t;
typedef struct {
    int __val[2];
} __fsid_t;
typedef long __clock_t;
typedef unsigned long __rlim_t;
typedef unsigned long __rlim64_t;
typedef unsigned int __id_t;
typedef long __time_t;
typedef unsigned int __useconds_t;
typedef long __suseconds_t;
typedef int __daddr_t;
typedef int __key_t;
typedef int __clockid_t;
typedef void *__timer_t;
typedef long __blksize_t;
typedef long __blkcnt_t;
typedef long __blkcnt64_t;
typedef unsigned long __fsblkcnt_t;
typedef unsigned long __fsblkcnt64_t;
typedef unsigned long __fsfilcnt_t;
typedef unsigned long __fsfilcnt64_t;
typedef long __fsword_t;
typedef long __ssize_t;
typedef long __syscall_slong_t;
typedef unsigned long __syscall_ulong_t;
typedef __off64_t __loff_t;
typedef char *__caddr_t;
typedef long __intptr_t;
typedef unsigned int __socklen_t;
typedef int __sig_atomic_t;
typedef __int8_t int8_t;
typedef __int16_t int16_t;
typedef __int32_t int32_t;
typedef __int64_t int64_t;
typedef __uint8_t uint8_t;
typedef __uint16_t uint16_t;
typedef __uint32_t uint32_t;
typedef __uint64_t uint64_t;
typedef __int_least8_t int_least8_t;
typedef __int_least16_t int_least16_t;
typedef __int_least32_t int_least32_t;
typedef __int_least64_t int_least64_t;
typedef __uint_least8_t uint_least8_t;
typedef __uint_least16_t uint_least16_t;
typedef __uint_least32_t uint_least32_t;
typedef __uint_least64_t uint_least64_t;
typedef signed char int_fast8_t;
typedef long int_fast16_t;
typedef long int_fast32_t;
typedef long int_fast64_t;
typedef unsigned char uint_fast8_t;
typedef unsigned long uint_fast16_t;
typedef unsigned long uint_fast32_t;
typedef unsigned long uint_fast64_t;
typedef long intptr_t;
typedef unsigned long uintptr_t;
typedef __intmax_t intmax_t;
typedef __uintmax_t uintmax_t;
extern uint64_t revng_init_local_sp(uint64_t param_0);
extern uint64_t init_rbx(void);
extern uint64_t init_rbp(void);
extern uint64_t init_r14(void);
extern uint64_t init_r13(void);
extern uint64_t init_r12(void);
extern uint64_t indirect_placeholder_4(uint64_t param_0, uint64_t param_1, uint64_t param_2);
uint64_t bb_memchr2(uint64_t rdx, uint64_t rdi, uint64_t rcx, uint64_t rsi) {
    uint64_t var_0;
    uint64_t var_1;
    uint64_t var_2;
    uint64_t var_3;
    uint64_t var_4;
    uint64_t var_5;
    unsigned char var_6;
    unsigned char var_7;
    uint64_t rdi2_2;
    unsigned char var_8;
    uint64_t rax_1;
    uint64_t rdi2_3;
    uint64_t rdi2_0;
    uint64_t rcx3_0;
    uint64_t var_9;
    uint64_t var_10;
    uint64_t rcx3_1;
    unsigned char var_11;
    uint64_t var_12;
    uint64_t var_13;
    uint64_t var_14;
    uint64_t var_15;
    uint64_t var_16;
    uint64_t var_17;
    uint64_t var_18;
    uint64_t var_19;
    uint64_t rdi2_4;
    uint64_t var_20;
    uint64_t var_21;
    uint64_t var_22;
    uint64_t rcx3_357;
    uint64_t rcx3_2;
    uint64_t var_23;
    uint64_t var_24;
    uint64_t var_25;
    uint64_t var_26;
    uint64_t var_27;
    uint64_t rcx3_3;
    uint64_t rdi2_456;
    unsigned char var_28;
    uint64_t var_29;
    uint64_t rax_0_in;
    uint64_t rax_0;
    unsigned char var_30;
    uint64_t var_31;
    uint64_t var_32;
    unsigned int loop_state_var;
    var_0 = revng_init_local_sp(0UL);
    var_1 = init_rbx();
    var_2 = init_r14();
    var_3 = init_rbp();
    var_4 = init_r13();
    var_5 = init_r12();
    *(uint64_t *)(var_0 + (-8L)) = var_2;
    *(uint64_t *)(var_0 + (-16L)) = var_4;
    *(uint64_t *)(var_0 + (-24L)) = var_5;
    *(uint64_t *)(var_0 + (-32L)) = var_3;
    *(uint64_t *)(var_0 + (-40L)) = var_1;
    var_6 = (unsigned char)rsi;
    var_7 = (unsigned char)rdx;
    rdi2_2 = rdi;
    rax_1 = rdi;
    rdi2_0 = rdi;
    rcx3_0 = rcx;
    rcx3_1 = rcx;
    if ((uint64_t)(var_6 - var_7) != 0UL) {
        if (!((rcx == 0UL) || ((rdi & 7UL) == 0UL))) {
            var_8 = *(unsigned char *)rdi;
            if ((uint64_t)(var_8 - var_6) == 0UL) {
                return rax_1;
            }
            if ((uint64_t)(var_8 - var_7) == 0UL) {
                return rax_1;
            }
            while (1U)
                {
                    var_9 = rdi2_0 + 1UL;
                    var_10 = rcx3_0 + (-1L);
                    rdi2_2 = var_9;
                    rax_1 = var_9;
                    rdi2_0 = var_9;
                    rcx3_0 = var_10;
                    rcx3_1 = var_10;
                    if (!((var_10 == 0UL) || ((var_9 & 7UL) == 0UL))) {
                        loop_state_var = 1U;
                        break;
                    }
                    var_11 = *(unsigned char *)var_9;
                    if ((uint64_t)(var_6 - var_11) != 0UL) {
                        loop_state_var = 0U;
                        break;
                    }
                    if ((uint64_t)(var_7 - var_11) == 0UL) {
                        continue;
                    }
                    loop_state_var = 0U;
                    break;
                }
            switch (loop_state_var) {
              case 0U:
                {
                    return rax_1;
                }
                break;
              case 1U:
                {
                    break;
                }
                break;
            }
        }
        var_12 = (uint64_t)var_6;
        var_13 = var_12 | (var_12 << 8UL);
        var_14 = (uint64_t)var_7;
        var_15 = var_14 | (var_14 << 8UL);
        var_16 = (var_13 << 16UL) | var_13;
        var_17 = (var_15 << 16UL) | var_15;
        var_18 = var_16 | (var_16 << 32UL);
        var_19 = var_17 | (var_17 << 32UL);
        rax_1 = 0UL;
        rdi2_3 = rdi2_2;
        rdi2_4 = rdi2_2;
        rcx3_357 = rcx3_1;
        rcx3_2 = rcx3_1;
        rcx3_3 = rcx3_1;
        rdi2_456 = rdi2_2;
        if (rcx3_1 > 7UL) {
            var_20 = *(uint64_t *)rdi2_2;
            var_21 = var_20 ^ var_18;
            var_22 = var_20 ^ var_19;
            if (((((var_22 + (-72340172838076673L)) & (var_22 ^ (-9187201950435737472L))) | ((var_21 + (-72340172838076673L)) & (var_21 ^ (-9187201950435737472L)))) & (-9187201950435737472L)) != 0UL) {
                while (1U)
                    {
                        var_23 = rdi2_3 + 8UL;
                        var_24 = rcx3_2 + (-8L);
                        rdi2_3 = var_23;
                        rdi2_4 = var_23;
                        rcx3_357 = var_24;
                        rcx3_2 = var_24;
                        rcx3_3 = var_24;
                        rdi2_456 = var_23;
                        if (var_24 <= 7UL) {
                            loop_state_var = 1U;
                            break;
                        }
                        var_25 = *(uint64_t *)var_23;
                        var_26 = var_18 ^ var_25;
                        var_27 = var_25 ^ var_19;
                        if (((((var_27 + (-72340172838076673L)) & (var_27 ^ (-9187201950435737472L))) | ((var_26 + (-72340172838076673L)) & (var_26 ^ (-9187201950435737472L)))) & (-9187201950435737472L)) == 0UL) {
                            continue;
                        }
                        loop_state_var = 0U;
                        break;
                    }
                rcx3_357 = rcx3_3;
                rdi2_456 = rdi2_4;
                if (rcx3_3 == 0UL) {
                    return rax_1;
                }
            }
        }
        rcx3_357 = rcx3_3;
        rdi2_456 = rdi2_4;
        if (rcx3_3 == 0UL) {
            return rax_1;
        }
        var_28 = *(unsigned char *)rdi2_456;
        rax_0_in = rdi2_456;
        rax_1 = rdi2_456;
        if ((uint64_t)(var_28 - var_6) != 0UL & (uint64_t)(var_28 - var_7) != 0UL) {
            var_29 = rcx3_357 + rdi2_456;
            rax_1 = 0UL;
            rax_0 = rax_0_in + 1UL;
            rax_0_in = rax_0;
            while (rax_0 != var_29)
                {
                    var_30 = *(unsigned char *)rax_0;
                    rax_1 = rax_0;
                    if ((uint64_t)(var_6 - var_30) == 0UL) {
                        break;
                    }
                    if ((uint64_t)(var_7 - var_30) == 0UL) {
                        break;
                    }
                    rax_0 = rax_0_in + 1UL;
                    rax_0_in = rax_0;
                }
        }
    }
    var_31 = (uint64_t)var_6;
    *(uint64_t *)(var_0 + (-48L)) = 4208159UL;
    var_32 = indirect_placeholder_4(rcx, rdi, var_31);
    rax_1 = var_32;
}

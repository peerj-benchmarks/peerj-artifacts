typedef unsigned char __u_char;
typedef unsigned short __u_short;
typedef unsigned int __u_int;
typedef unsigned long __u_long;
typedef signed char __int8_t;
typedef unsigned char __uint8_t;
typedef short __int16_t;
typedef unsigned short __uint16_t;
typedef int __int32_t;
typedef unsigned int __uint32_t;
typedef long __int64_t;
typedef unsigned long __uint64_t;
typedef __int8_t __int_least8_t;
typedef __uint8_t __uint_least8_t;
typedef __int16_t __int_least16_t;
typedef __uint16_t __uint_least16_t;
typedef __int32_t __int_least32_t;
typedef __uint32_t __uint_least32_t;
typedef __int64_t __int_least64_t;
typedef __uint64_t __uint_least64_t;
typedef long __quad_t;
typedef unsigned long __u_quad_t;
typedef long __intmax_t;
typedef unsigned long __uintmax_t;
typedef unsigned long __dev_t;
typedef unsigned int __uid_t;
typedef unsigned int __gid_t;
typedef unsigned long __ino_t;
typedef unsigned long __ino64_t;
typedef unsigned int __mode_t;
typedef unsigned long __nlink_t;
typedef long __off_t;
typedef long __off64_t;
typedef int __pid_t;
typedef struct {
    int __val[2];
} __fsid_t;
typedef long __clock_t;
typedef unsigned long __rlim_t;
typedef unsigned long __rlim64_t;
typedef unsigned int __id_t;
typedef long __time_t;
typedef unsigned int __useconds_t;
typedef long __suseconds_t;
typedef int __daddr_t;
typedef int __key_t;
typedef int __clockid_t;
typedef void *__timer_t;
typedef long __blksize_t;
typedef long __blkcnt_t;
typedef long __blkcnt64_t;
typedef unsigned long __fsblkcnt_t;
typedef unsigned long __fsblkcnt64_t;
typedef unsigned long __fsfilcnt_t;
typedef unsigned long __fsfilcnt64_t;
typedef long __fsword_t;
typedef long __ssize_t;
typedef long __syscall_slong_t;
typedef unsigned long __syscall_ulong_t;
typedef __off64_t __loff_t;
typedef char *__caddr_t;
typedef long __intptr_t;
typedef unsigned int __socklen_t;
typedef int __sig_atomic_t;
typedef __int8_t int8_t;
typedef __int16_t int16_t;
typedef __int32_t int32_t;
typedef __int64_t int64_t;
typedef __uint8_t uint8_t;
typedef __uint16_t uint16_t;
typedef __uint32_t uint32_t;
typedef __uint64_t uint64_t;
typedef __int_least8_t int_least8_t;
typedef __int_least16_t int_least16_t;
typedef __int_least32_t int_least32_t;
typedef __int_least64_t int_least64_t;
typedef __uint_least8_t uint_least8_t;
typedef __uint_least16_t uint_least16_t;
typedef __uint_least32_t uint_least32_t;
typedef __uint_least64_t uint_least64_t;
typedef signed char int_fast8_t;
typedef long int_fast16_t;
typedef long int_fast32_t;
typedef long int_fast64_t;
typedef unsigned char uint_fast8_t;
typedef unsigned long uint_fast16_t;
typedef unsigned long uint_fast32_t;
typedef unsigned long uint_fast64_t;
typedef long intptr_t;
typedef unsigned long uintptr_t;
typedef __intmax_t intmax_t;
typedef __uintmax_t uintmax_t;
typedef _Bool bool;
struct indirect_placeholder_23_ret_type;
struct indirect_placeholder_23_ret_type {
    uint64_t field_0;
    uint64_t field_1;
    uint64_t field_2;
    uint64_t field_3;
    uint64_t field_4;
};
extern void abort(void);
extern uint64_t revng_init_local_sp(uint64_t param_0);
extern uint64_t init_rbx(void);
extern void indirect_placeholder(void);
extern uint64_t init_rbp(void);
extern uint64_t init_cc_src2(void);
extern uint64_t helper_cc_compute_c_wrapper(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint32_t param_3);
extern uint64_t init_r13(void);
extern uint64_t init_r12(void);
extern uint64_t indirect_placeholder_1(uint64_t param_0);
extern void indirect_placeholder_5(uint64_t param_0);
extern void indirect_placeholder_6(uint64_t param_0, uint64_t param_1);
extern void indirect_placeholder_10(uint64_t param_0, uint64_t param_1, uint64_t param_2);
extern void indirect_placeholder_11(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint64_t param_3, uint64_t param_4, uint64_t param_5, uint64_t param_6);
extern struct indirect_placeholder_23_ret_type indirect_placeholder_23(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint64_t param_3, uint64_t param_4);
void bb_main(uint64_t rdi, uint64_t rsi) {
    struct indirect_placeholder_23_ret_type var_11;
    uint64_t var_0;
    uint64_t var_1;
    uint64_t var_2;
    uint64_t var_3;
    uint64_t var_4;
    uint64_t var_5;
    uint32_t var_6;
    uint64_t var_7;
    uint64_t var_8;
    uint64_t var_9;
    uint64_t local_sp_4;
    uint64_t var_53;
    uint64_t local_sp_1;
    uint64_t r13_0;
    uint32_t var_43;
    uint64_t var_44;
    uint64_t var_45;
    uint64_t local_sp_0;
    uint64_t var_51;
    uint64_t var_52;
    uint64_t rdx_0;
    uint64_t var_46;
    uint64_t var_47;
    uint64_t var_48;
    uint32_t var_49;
    uint64_t var_50;
    uint64_t var_31;
    uint64_t var_32;
    uint64_t var_33;
    uint32_t var_34;
    uint64_t var_35;
    uint64_t var_36;
    bool var_37;
    bool var_38;
    uint64_t var_39;
    uint64_t var_40;
    uint64_t var_41;
    uint64_t var_42;
    uint64_t r12_1;
    uint64_t local_sp_2;
    uint64_t var_16;
    uint64_t var_17;
    uint64_t var_18;
    uint64_t var_13;
    uint64_t var_14;
    uint64_t var_15;
    uint64_t var_19;
    unsigned char var_20;
    uint64_t var_21;
    uint64_t var_22;
    uint64_t var_23;
    uint64_t local_sp_3;
    uint64_t var_24;
    uint64_t var_25;
    uint64_t var_26;
    uint64_t var_27;
    uint64_t local_sp_4_be;
    uint64_t r13_0_be;
    uint64_t r12_1_be;
    uint64_t var_28;
    uint64_t var_29;
    uint64_t var_30;
    uint64_t var_10;
    uint32_t var_12;
    unsigned int loop_state_var;
    var_0 = revng_init_local_sp(0UL);
    var_1 = init_rbx();
    var_2 = init_rbp();
    var_3 = init_r13();
    var_4 = init_r12();
    var_5 = init_cc_src2();
    *(uint64_t *)(var_0 + (-8L)) = var_3;
    *(uint64_t *)(var_0 + (-16L)) = var_4;
    *(uint64_t *)(var_0 + (-24L)) = var_2;
    *(uint64_t *)(var_0 + (-32L)) = var_1;
    var_6 = (uint32_t)rdi;
    var_7 = (uint64_t)var_6;
    var_8 = *(uint64_t *)rsi;
    *(uint64_t *)(var_0 + (-48L)) = 4204594UL;
    indirect_placeholder_5(var_8);
    *(uint64_t *)(var_0 + (-56L)) = 4204609UL;
    indirect_placeholder();
    var_9 = var_0 + (-64L);
    *(uint64_t *)var_9 = 4204619UL;
    indirect_placeholder();
    *(uint32_t *)6364460UL = 0U;
    *(unsigned char *)6364459UL = (unsigned char)'\x00';
    *(unsigned char *)6364457UL = (unsigned char)'\x00';
    *(unsigned char *)6364434UL = (unsigned char)'\x00';
    local_sp_4 = var_9;
    r13_0 = var_3;
    r12_1 = 0UL;
    while (1U)
        {
            var_10 = local_sp_4 + (-8L);
            *(uint64_t *)var_10 = 4205187UL;
            var_11 = indirect_placeholder_23(4253007UL, var_7, 4255360UL, rsi, 0UL);
            var_12 = (uint32_t)var_11.field_0;
            local_sp_2 = var_10;
            local_sp_3 = var_10;
            local_sp_4_be = var_10;
            r13_0_be = r13_0;
            r12_1_be = r12_1;
            if ((uint64_t)(var_12 + 1U) != 0UL) {
                var_31 = var_11.field_1;
                var_32 = var_11.field_3;
                var_33 = var_11.field_4;
                var_34 = *(uint32_t *)6364460UL;
                var_35 = (uint64_t)var_34;
                if (var_34 == 0U) {
                    *(uint64_t *)(local_sp_4 + (-16L)) = 4205226UL;
                    indirect_placeholder_11(var_35, 4255080UL, 0UL, var_31, 0UL, var_32, var_33);
                    *(uint64_t *)(local_sp_4 + (-24L)) = 4205236UL;
                    indirect_placeholder_10(rsi, var_7, 1UL);
                    abort();
                }
                var_36 = (uint64_t)(var_34 + (-2));
                var_37 = (var_36 != 0UL);
                var_38 = ((uint64_t)(unsigned char)r12_1 == 0UL);
                if (var_38 || (var_37 ^ 1)) {
                    *(uint64_t *)(local_sp_4 + (-16L)) = 4205276UL;
                    indirect_placeholder_11(0UL, 4255136UL, 0UL, var_31, 0UL, var_32, var_33);
                    *(uint64_t *)(local_sp_4 + (-24L)) = 4205286UL;
                    indirect_placeholder_10(rsi, var_7, 1UL);
                    abort();
                }
                if (!var_37) {
                    loop_state_var = 0U;
                    break;
                }
                if (*(unsigned char *)6364459UL != '\x00') {
                    loop_state_var = 0U;
                    break;
                }
                *(uint64_t *)(local_sp_4 + (-16L)) = 4205324UL;
                indirect_placeholder_11(0UL, 4255208UL, 0UL, var_31, 0UL, var_32, var_33);
                *(uint64_t *)(local_sp_4 + (-24L)) = 4205334UL;
                indirect_placeholder_10(rsi, var_7, 1UL);
                abort();
            }
            if ((uint64_t)(var_12 + (-102)) == 0UL) {
                if (*(uint32_t *)6364460UL != 0U) {
                    var_28 = var_11.field_1;
                    var_29 = var_11.field_3;
                    var_30 = var_11.field_4;
                    *(uint64_t *)(local_sp_4 + (-16L)) = 4204890UL;
                    indirect_placeholder_11(0UL, 4254992UL, 0UL, var_28, 0UL, var_29, var_30);
                    *(uint64_t *)(local_sp_4 + (-24L)) = 4204900UL;
                    indirect_placeholder_10(rsi, var_7, 1UL);
                    abort();
                }
                *(uint32_t *)6364460UL = 2U;
                r13_0_be = *(uint64_t *)6364968UL;
            } else {
                r12_1_be = 1UL;
                if ((int)var_12 > (int)102U) {
                    if ((uint64_t)(var_12 + (-122)) == 0UL) {
                        *(unsigned char *)6364160UL = (unsigned char)'\x00';
                    } else {
                        if ((int)var_12 > (int)122U) {
                            if ((uint64_t)(var_12 + (-128)) == 0UL) {
                                *(unsigned char *)6364456UL = (unsigned char)'\x01';
                                if (**(unsigned char **)6364968UL != '\x00') {
                                    var_24 = local_sp_4 + (-16L);
                                    *(uint64_t *)var_24 = 4205023UL;
                                    indirect_placeholder();
                                    local_sp_3 = var_24;
                                }
                                *(uint64_t *)6364448UL = 1UL;
                                var_25 = *(uint64_t *)6364968UL;
                                var_26 = local_sp_3 + (-8L);
                                *(uint64_t *)var_26 = 4205042UL;
                                var_27 = indirect_placeholder_1(var_25);
                                *(uint64_t *)6364440UL = var_27;
                                local_sp_4_be = var_26;
                            } else {
                                if ((uint64_t)(var_12 + (-129)) != 0UL) {
                                    loop_state_var = 1U;
                                    break;
                                }
                                *(unsigned char *)6364458UL = (unsigned char)'\x01';
                            }
                        } else {
                            if ((uint64_t)(var_12 + (-110)) != 0UL) {
                                if ((uint64_t)(var_12 + (-115)) != 0UL) {
                                    loop_state_var = 1U;
                                    break;
                                }
                                *(unsigned char *)6364459UL = (unsigned char)'\x01';
                            }
                        }
                    }
                } else {
                    if ((int)var_12 > (int)99U) {
                        if ((uint64_t)(var_12 + (-100)) != 0UL) {
                            loop_state_var = 1U;
                            break;
                        }
                        var_19 = *(uint64_t *)6364968UL;
                        var_20 = *(unsigned char *)var_19;
                        if (var_20 != '\x00') {
                            if (*(unsigned char *)(var_19 + 1UL) != '\x00') {
                                var_21 = var_11.field_1;
                                var_22 = var_11.field_3;
                                var_23 = var_11.field_4;
                                *(uint64_t *)(local_sp_4 + (-16L)) = 4204967UL;
                                indirect_placeholder_11(0UL, 4255032UL, 0UL, var_21, 0UL, var_22, var_23);
                                *(uint64_t *)(local_sp_4 + (-24L)) = 4204977UL;
                                indirect_placeholder_10(rsi, var_7, 1UL);
                                abort();
                            }
                        }
                        *(unsigned char *)6364457UL = var_20;
                    } else {
                        if ((int)var_12 >= (int)98U) {
                            if ((uint64_t)(var_12 + 131U) != 0UL) {
                                if ((uint64_t)(var_12 + 130U) != 0UL) {
                                    loop_state_var = 1U;
                                    break;
                                }
                                *(uint64_t *)(local_sp_4 + (-16L)) = 4205088UL;
                                indirect_placeholder_10(rsi, var_7, 0UL);
                                abort();
                            }
                            *(uint64_t *)(local_sp_4 + (-16L)) = 0UL;
                            *(uint64_t *)(local_sp_4 + (-24L)) = 4252994UL;
                            var_16 = *(uint64_t *)6364168UL;
                            var_17 = *(uint64_t *)6364288UL;
                            *(uint64_t *)(local_sp_4 + (-32L)) = 4205141UL;
                            indirect_placeholder_11(0UL, 4252922UL, var_17, var_16, 4252823UL, 4252963UL, 4252979UL);
                            var_18 = local_sp_4 + (-40L);
                            *(uint64_t *)var_18 = 4205151UL;
                            indirect_placeholder();
                            local_sp_2 = var_18;
                            loop_state_var = 1U;
                            break;
                        }
                        if (*(uint32_t *)6364460UL == 0U) {
                            var_13 = var_11.field_1;
                            var_14 = var_11.field_3;
                            var_15 = var_11.field_4;
                            *(uint64_t *)(local_sp_4 + (-16L)) = 4204824UL;
                            indirect_placeholder_11(0UL, 4254992UL, 0UL, var_13, 0UL, var_14, var_15);
                            *(uint64_t *)(local_sp_4 + (-24L)) = 4204834UL;
                            indirect_placeholder_10(rsi, var_7, 1UL);
                            abort();
                        }
                        *(uint32_t *)6364460UL = 1U;
                        r13_0_be = *(uint64_t *)6364968UL;
                    }
                }
            }
        }
    switch (loop_state_var) {
      case 1U:
        {
            *(uint64_t *)(local_sp_2 + (-8L)) = 4205161UL;
            indirect_placeholder_10(rsi, var_7, 1UL);
            abort();
        }
        break;
      case 0U:
        {
            var_39 = (var_36 == 0UL) ? 0UL : 4UL;
            var_40 = helper_cc_compute_c_wrapper((uint64_t)*(unsigned char *)6364458UL + (-1L), 1UL, var_5, 14U);
            var_41 = (((0UL - var_40) & 2UL) | var_39) ^ 2UL;
            var_42 = local_sp_4 + (-16L);
            *(uint64_t *)var_42 = 4205370UL;
            indirect_placeholder_6(r13_0, var_41);
            local_sp_0 = var_42;
            local_sp_1 = var_42;
            if (var_38) {
                *(unsigned char *)6364457UL = (unsigned char)'\t';
            }
            if (*(uint64_t *)6364440UL == 0UL) {
                *(unsigned char *)6364432UL = *(unsigned char *)6364457UL;
                *(unsigned char *)6364433UL = (unsigned char)'\x00';
                *(uint64_t *)6364440UL = 6364432UL;
                *(uint64_t *)6364448UL = 1UL;
            }
            var_43 = *(uint32_t *)6364284UL;
            var_44 = (uint64_t)var_43;
            rdx_0 = var_44;
            if ((uint64_t)(var_43 - var_6) != 0UL) {
                var_45 = rdi << 32UL;
                if ((long)var_45 > (long)(var_44 << 32UL)) {
                    var_46 = *(uint64_t *)((uint64_t)((long)(rdx_0 << 32UL) >> (long)29UL) + rsi);
                    var_47 = local_sp_1 + (-8L);
                    *(uint64_t *)var_47 = 4205483UL;
                    indirect_placeholder_1(var_46);
                    var_48 = (uint64_t)*(uint32_t *)6364284UL + 1UL;
                    var_49 = (uint32_t)var_48;
                    *(uint32_t *)6364284UL = var_49;
                    local_sp_0 = var_47;
                    local_sp_1 = var_47;
                    while ((long)var_45 <= (long)(var_48 << 32UL))
                        {
                            rdx_0 = (uint64_t)var_49;
                            var_46 = *(uint64_t *)((uint64_t)((long)(rdx_0 << 32UL) >> (long)29UL) + rsi);
                            var_47 = local_sp_1 + (-8L);
                            *(uint64_t *)var_47 = 4205483UL;
                            indirect_placeholder_1(var_46);
                            var_48 = (uint64_t)*(uint32_t *)6364284UL + 1UL;
                            var_49 = (uint32_t)var_48;
                            *(uint32_t *)6364284UL = var_49;
                            local_sp_0 = var_47;
                            local_sp_1 = var_47;
                        }
                }
            }
            var_50 = local_sp_4 + (-24L);
            *(uint64_t *)var_50 = 4205466UL;
            indirect_placeholder_1(4259378UL);
            local_sp_0 = var_50;
            var_51 = *(uint64_t *)6364296UL;
            *(uint64_t *)(local_sp_0 + (-8L)) = 4205526UL;
            var_52 = indirect_placeholder_1(var_51);
            if (*(unsigned char *)6364434UL != '\x00' & (uint64_t)((uint32_t)var_52 + 1U) == 0UL) {
                *(uint64_t *)(local_sp_0 + (-16L)) = 4205536UL;
                indirect_placeholder();
                var_53 = (uint64_t)*(uint32_t *)var_52;
                *(uint64_t *)(local_sp_0 + (-24L)) = 4205558UL;
                indirect_placeholder_11(0UL, 4259378UL, 0UL, var_31, var_53, var_32, var_33);
            }
            return;
        }
        break;
    }
}

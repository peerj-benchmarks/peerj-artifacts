typedef unsigned char __u_char;
typedef unsigned short __u_short;
typedef unsigned int __u_int;
typedef unsigned long __u_long;
typedef signed char __int8_t;
typedef unsigned char __uint8_t;
typedef short __int16_t;
typedef unsigned short __uint16_t;
typedef int __int32_t;
typedef unsigned int __uint32_t;
typedef long __int64_t;
typedef unsigned long __uint64_t;
typedef __int8_t __int_least8_t;
typedef __uint8_t __uint_least8_t;
typedef __int16_t __int_least16_t;
typedef __uint16_t __uint_least16_t;
typedef __int32_t __int_least32_t;
typedef __uint32_t __uint_least32_t;
typedef __int64_t __int_least64_t;
typedef __uint64_t __uint_least64_t;
typedef long __quad_t;
typedef unsigned long __u_quad_t;
typedef long __intmax_t;
typedef unsigned long __uintmax_t;
typedef unsigned long __dev_t;
typedef unsigned int __uid_t;
typedef unsigned int __gid_t;
typedef unsigned long __ino_t;
typedef unsigned long __ino64_t;
typedef unsigned int __mode_t;
typedef unsigned long __nlink_t;
typedef long __off_t;
typedef long __off64_t;
typedef int __pid_t;
typedef struct {
    int __val[2];
} __fsid_t;
typedef long __clock_t;
typedef unsigned long __rlim_t;
typedef unsigned long __rlim64_t;
typedef unsigned int __id_t;
typedef long __time_t;
typedef unsigned int __useconds_t;
typedef long __suseconds_t;
typedef int __daddr_t;
typedef int __key_t;
typedef int __clockid_t;
typedef void *__timer_t;
typedef long __blksize_t;
typedef long __blkcnt_t;
typedef long __blkcnt64_t;
typedef unsigned long __fsblkcnt_t;
typedef unsigned long __fsblkcnt64_t;
typedef unsigned long __fsfilcnt_t;
typedef unsigned long __fsfilcnt64_t;
typedef long __fsword_t;
typedef long __ssize_t;
typedef long __syscall_slong_t;
typedef unsigned long __syscall_ulong_t;
typedef __off64_t __loff_t;
typedef char *__caddr_t;
typedef long __intptr_t;
typedef unsigned int __socklen_t;
typedef int __sig_atomic_t;
typedef __int8_t int8_t;
typedef __int16_t int16_t;
typedef __int32_t int32_t;
typedef __int64_t int64_t;
typedef __uint8_t uint8_t;
typedef __uint16_t uint16_t;
typedef __uint32_t uint32_t;
typedef __uint64_t uint64_t;
typedef __int_least8_t int_least8_t;
typedef __int_least16_t int_least16_t;
typedef __int_least32_t int_least32_t;
typedef __int_least64_t int_least64_t;
typedef __uint_least8_t uint_least8_t;
typedef __uint_least16_t uint_least16_t;
typedef __uint_least32_t uint_least32_t;
typedef __uint_least64_t uint_least64_t;
typedef signed char int_fast8_t;
typedef long int_fast16_t;
typedef long int_fast32_t;
typedef long int_fast64_t;
typedef unsigned char uint_fast8_t;
typedef unsigned long uint_fast16_t;
typedef unsigned long uint_fast32_t;
typedef unsigned long uint_fast64_t;
typedef long intptr_t;
typedef unsigned long uintptr_t;
typedef __intmax_t intmax_t;
typedef __uintmax_t uintmax_t;
typedef _Bool bool;
struct indirect_placeholder_18_ret_type;
struct indirect_placeholder_19_ret_type;
struct indirect_placeholder_13_ret_type;
struct indirect_placeholder_14_ret_type;
struct indirect_placeholder_15_ret_type;
struct indirect_placeholder_16_ret_type;
struct indirect_placeholder_17_ret_type;
struct indirect_placeholder_20_ret_type;
struct indirect_placeholder_18_ret_type {
    uint64_t field_0;
    uint64_t field_1;
    uint64_t field_2;
};
struct indirect_placeholder_19_ret_type {
    uint64_t field_0;
    uint64_t field_1;
    uint64_t field_2;
};
struct indirect_placeholder_13_ret_type {
    uint64_t field_0;
    uint64_t field_1;
    uint64_t field_2;
};
struct indirect_placeholder_14_ret_type {
    uint64_t field_0;
    uint64_t field_1;
    uint64_t field_2;
};
struct indirect_placeholder_15_ret_type {
    uint64_t field_0;
    uint64_t field_1;
    uint64_t field_2;
};
struct indirect_placeholder_16_ret_type {
    uint64_t field_0;
    uint64_t field_1;
    uint64_t field_2;
};
struct indirect_placeholder_17_ret_type {
    uint64_t field_0;
    uint64_t field_1;
    uint64_t field_2;
};
struct indirect_placeholder_20_ret_type {
    uint64_t field_0;
    uint64_t field_1;
    uint64_t field_2;
};
extern void abort(void);
extern uint64_t revng_init_local_sp(uint64_t param_0);
extern uint64_t init_rbx(void);
extern void indirect_placeholder(void);
extern uint64_t init_rbp(void);
extern uint64_t init_cc_src2(void);
extern uint64_t helper_cc_compute_c_wrapper(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint32_t param_3);
extern uint64_t init_r14(void);
extern uint64_t init_r13(void);
extern uint64_t init_r12(void);
extern uint64_t init_r15(void);
extern uint64_t init_r8(void);
extern uint64_t indirect_placeholder_7(uint64_t param_0, uint64_t param_1);
extern uint64_t init_r9(void);
extern void indirect_placeholder_10(uint64_t param_0, uint64_t param_1, uint64_t param_2);
extern void indirect_placeholder_11(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint64_t param_3, uint64_t param_4, uint64_t param_5, uint64_t param_6);
extern struct indirect_placeholder_18_ret_type indirect_placeholder_18(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint64_t param_3);
extern struct indirect_placeholder_19_ret_type indirect_placeholder_19(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint64_t param_3);
extern uint64_t init_rcx(void);
extern struct indirect_placeholder_13_ret_type indirect_placeholder_13(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint64_t param_3);
extern struct indirect_placeholder_14_ret_type indirect_placeholder_14(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint64_t param_3);
extern struct indirect_placeholder_15_ret_type indirect_placeholder_15(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint64_t param_3);
extern struct indirect_placeholder_16_ret_type indirect_placeholder_16(uint64_t param_0);
extern struct indirect_placeholder_17_ret_type indirect_placeholder_17(uint64_t param_0);
extern struct indirect_placeholder_20_ret_type indirect_placeholder_20(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint64_t param_3);
void bb_set_fields(uint64_t rdi, uint64_t rsi) {
    uint64_t var_16;
    unsigned char *var_37;
    uint64_t var_38;
    uint64_t var_39;
    uint64_t *_pre_phi314;
    uint64_t *var_40;
    bool var_41;
    uint64_t var_42;
    uint64_t var_47;
    struct indirect_placeholder_18_ret_type var_48;
    bool var_43;
    uint64_t var_44;
    uint64_t *var_45;
    struct indirect_placeholder_19_ret_type var_46;
    uint64_t var_0;
    uint64_t var_1;
    uint64_t var_2;
    uint64_t var_3;
    uint64_t var_4;
    uint64_t var_5;
    uint64_t var_6;
    uint64_t var_7;
    uint64_t var_8;
    uint64_t var_9;
    uint64_t var_10;
    uint64_t var_11;
    uint64_t var_12;
    uint64_t local_sp_10;
    uint64_t var_66;
    uint64_t var_67;
    uint64_t var_68;
    uint64_t var_69;
    uint64_t local_sp_8;
    uint64_t rax_0;
    uint64_t rbx_0;
    uint64_t rbp_0;
    uint64_t r8_5;
    uint64_t local_sp_0;
    uint64_t local_sp_4;
    uint64_t r8_0;
    uint64_t var_52;
    uint64_t var_53;
    uint64_t local_sp_6;
    uint64_t var_70;
    uint64_t var_71;
    uint64_t r8_6;
    uint64_t local_sp_1;
    uint64_t var_72;
    uint64_t var_73;
    struct indirect_placeholder_13_ret_type var_74;
    uint64_t r9_0;
    uint64_t r8_1;
    uint64_t local_sp_7;
    uint64_t rbx_1;
    uint64_t r12_0;
    uint64_t r9_2;
    uint64_t local_sp_2;
    uint64_t r8_3;
    uint64_t r9_1;
    uint64_t r9_3;
    uint64_t r8_2;
    uint64_t var_75;
    uint64_t var_76;
    uint64_t local_sp_3;
    uint64_t var_77;
    uint64_t var_78;
    struct indirect_placeholder_14_ret_type var_79;
    uint64_t var_80;
    uint64_t r8_4;
    uint64_t var_81;
    uint64_t local_sp_5;
    uint64_t var_82;
    uint64_t var_83;
    uint64_t var_84;
    uint64_t var_85;
    uint64_t var_86;
    uint64_t var_87;
    uint64_t var_88;
    uint64_t var_89;
    uint64_t var_90;
    uint64_t var_54;
    uint64_t var_55;
    uint64_t var_56;
    uint64_t var_57;
    uint64_t var_58;
    uint64_t var_59;
    uint64_t var_60;
    uint64_t rdi1_0;
    uint64_t rcx_0;
    uint64_t var_61;
    uint64_t var_62;
    uint64_t var_63;
    uint64_t var_64;
    uint64_t var_65;
    uint64_t local_sp_13;
    uint64_t rax_1;
    uint64_t rcx_1;
    uint64_t local_sp_9;
    uint64_t r9_4;
    uint64_t rax_4;
    uint64_t var_91;
    uint64_t rcx_3;
    uint64_t var_51;
    unsigned char var_17;
    uint64_t var_18;
    uint64_t rax_5;
    uint64_t r8_8;
    unsigned char *var_19;
    uint64_t rbp_3;
    uint64_t local_sp_12;
    bool var_20;
    uint64_t r12_2;
    uint64_t var_21;
    unsigned char *var_22;
    uint64_t rax_2;
    uint64_t var_23;
    uint64_t var_24;
    uint64_t var_25;
    uint64_t var_26;
    struct indirect_placeholder_16_ret_type var_27;
    uint64_t var_28;
    uint64_t var_29;
    uint64_t var_30;
    uint64_t var_31;
    struct indirect_placeholder_17_ret_type var_32;
    uint64_t var_33;
    uint64_t var_34;
    uint64_t var_35;
    uint64_t var_36;
    uint64_t var_13;
    uint64_t r14_2;
    uint64_t rax_3;
    uint64_t rbx_3_in;
    uint64_t r14_1;
    uint64_t rbp_2;
    uint64_t r12_1;
    uint64_t rcx_2;
    uint64_t r9_6;
    uint64_t local_sp_11;
    uint64_t r9_5;
    uint64_t r8_7;
    uint64_t rbx_3;
    unsigned char *var_14;
    unsigned char var_15;
    uint64_t var_93;
    bool var_94;
    bool var_95;
    uint64_t var_96;
    uint64_t var_97;
    uint64_t var_92;
    uint64_t var_49;
    struct indirect_placeholder_20_ret_type var_50;
    unsigned int loop_state_var;
    var_0 = revng_init_local_sp(0UL);
    var_1 = init_rbx();
    var_2 = init_r14();
    var_3 = init_rbp();
    var_4 = init_r13();
    var_5 = init_r12();
    var_6 = init_r15();
    var_7 = init_rcx();
    var_8 = init_cc_src2();
    var_9 = init_r9();
    var_10 = init_r8();
    *(uint64_t *)(var_0 + (-8L)) = var_6;
    *(uint64_t *)(var_0 + (-16L)) = var_2;
    *(uint64_t *)(var_0 + (-24L)) = var_4;
    *(uint64_t *)(var_0 + (-32L)) = var_5;
    *(uint64_t *)(var_0 + (-40L)) = var_3;
    *(uint64_t *)(var_0 + (-48L)) = var_1;
    var_11 = var_0 + (-88L);
    *(uint32_t *)(var_0 + (-60L)) = (uint32_t)rsi;
    var_12 = rsi & 1UL;
    *(uint32_t *)(var_0 + (-64L)) = (uint32_t)var_12;
    local_sp_10 = var_11;
    rbx_0 = 0UL;
    rbp_0 = 32UL;
    r12_0 = 1UL;
    rbp_3 = 0UL;
    rax_3 = var_12;
    rbx_3_in = rdi;
    r14_1 = 0UL;
    rbp_2 = 0UL;
    r12_1 = 0UL;
    rcx_2 = var_7;
    r9_5 = var_9;
    r8_7 = var_10;
    if (var_12 == 0UL) {
        var_13 = var_0 + (-96L);
        *(uint64_t *)var_13 = 4205852UL;
        indirect_placeholder();
        local_sp_10 = var_13;
    }
    *(unsigned char *)(local_sp_10 + 15UL) = (unsigned char)'\x00';
    *(unsigned char *)(local_sp_10 + 14UL) = (unsigned char)'\x00';
    *(uint64_t *)(local_sp_10 + 16UL) = 1UL;
    local_sp_11 = local_sp_10;
    while (1U)
        {
            rbx_3 = rbx_3_in + 1UL;
            var_14 = (unsigned char *)rbx_3_in;
            var_15 = *var_14;
            local_sp_13 = local_sp_11;
            rax_4 = rax_3;
            rcx_3 = rcx_2;
            rax_5 = rax_3;
            r8_8 = r8_7;
            local_sp_12 = local_sp_11;
            r12_2 = r12_1;
            r14_2 = r14_1;
            rbx_3_in = rbx_3;
            r9_6 = r9_5;
            if ((uint64_t)(var_15 + '\xd3') == 0UL) {
                r12_2 = 1UL;
                if ((uint64_t)(unsigned char)r12_1 == 0UL) {
                    var_93 = ((*(uint32_t *)(local_sp_11 + 28UL) & 4U) == 0U) ? 4255954UL : 4255712UL;
                    *(uint64_t *)(local_sp_11 + (-8L)) = 4206016UL;
                    indirect_placeholder_11(0UL, var_93, 0UL, rcx_2, 0UL, r9_5, r8_7);
                    *(uint64_t *)(local_sp_11 + (-16L)) = 4206026UL;
                    indirect_placeholder_10(rbx_3, rbp_2, 1UL);
                    abort();
                }
                var_94 = (rbp_2 != 0UL);
                var_95 = ((uint64_t)(unsigned char)r14_1 == 0UL);
                if (var_94 || var_95) {
                    var_96 = ((*(uint32_t *)(local_sp_11 + 28UL) & 4U) == 0U) ? 4255974UL : 4255744UL;
                    *(uint64_t *)(local_sp_11 + (-8L)) = 4206077UL;
                    indirect_placeholder_11(0UL, var_96, 0UL, rcx_2, 0UL, r9_5, r8_7);
                    *(uint64_t *)(local_sp_11 + (-16L)) = 4206087UL;
                    indirect_placeholder_10(rbx_3, 0UL, 1UL);
                    abort();
                }
                if (var_95) {
                    *(unsigned char *)(local_sp_11 + 15UL) = (unsigned char)'\x00';
                    *(uint64_t *)(local_sp_11 + 16UL) = 1UL;
                } else {
                    var_97 = (uint64_t)(uint32_t)r14_1;
                    *(uint64_t *)(local_sp_11 + 16UL) = rbp_2;
                    *(unsigned char *)(local_sp_11 + 15UL) = (unsigned char)'\x00';
                    r12_2 = var_97;
                }
            } else {
                r14_2 = 0UL;
                rcx_3 = 1844674407370955161UL;
                if ((uint64_t)(var_15 + '\xd4') != 0UL) {
                    var_16 = local_sp_11 + (-8L);
                    *(uint64_t *)var_16 = 4206134UL;
                    indirect_placeholder();
                    local_sp_12 = var_16;
                    local_sp_13 = var_16;
                    var_17 = *var_14;
                    var_18 = (uint64_t)var_17;
                    rax_2 = var_18;
                    rax_5 = var_18;
                    if ((uint64_t)(uint32_t)rax_3 != 0UL & var_17 != '\x00') {
                        if (((uint32_t)var_17 + (-48)) > 9U) {
                            *(uint64_t *)(local_sp_11 + (-16L)) = 4206639UL;
                            var_32 = indirect_placeholder_17(rbx_3_in);
                            var_33 = var_32.field_0;
                            var_34 = var_32.field_1;
                            var_35 = var_32.field_2;
                            var_36 = ((*(uint32_t *)(local_sp_11 + 12UL) & 4U) == 0U) ? 4256030UL : 4255832UL;
                            *(uint64_t *)(local_sp_11 + (-24L)) = 4206683UL;
                            indirect_placeholder_11(0UL, var_36, 0UL, var_33, 0UL, var_34, var_35);
                            *(uint64_t *)(local_sp_11 + (-32L)) = 4206693UL;
                            indirect_placeholder_10(rbx_3, rbp_2, 1UL);
                            abort();
                        }
                        var_19 = (unsigned char *)(local_sp_11 + 7UL);
                        if (*var_19 == '\x00') {
                            *(uint64_t *)6364488UL = rbx_3_in;
                        } else {
                            if (*(uint64_t *)6364488UL == 0UL) {
                                *(uint64_t *)6364488UL = rbx_3_in;
                            }
                        }
                        var_20 = ((uint64_t)(unsigned char)r12_1 == 0UL);
                        var_21 = var_20 ? 1UL : (uint64_t)(uint32_t)r14_1;
                        var_22 = (unsigned char *)(local_sp_11 + 6UL);
                        *var_22 = (var_20 ? *var_22 : '\x01');
                        r14_2 = var_21;
                        if (rbp_2 <= 1844674407370955161UL) {
                            loop_state_var = 1U;
                            break;
                        }
                        var_23 = (rbp_2 * 10UL) + (uint64_t)((long)(((uint64_t)var_17 << 32UL) + (-206158430208L)) >> (long)32UL);
                        var_24 = helper_cc_compute_c_wrapper(var_23 - rbp_2, rbp_2, var_8, 17U);
                        rax_2 = var_23;
                        rax_4 = var_23;
                        rbp_3 = var_23;
                        if (!((var_24 != 0UL) || (var_23 == 18446744073709551615UL))) {
                            loop_state_var = 1U;
                            break;
                        }
                        *var_19 = (unsigned char)'\x01';
                        rax_3 = rax_4;
                        r14_1 = r14_2;
                        rbp_2 = rbp_3;
                        r12_1 = r12_2;
                        rcx_2 = rcx_3;
                        local_sp_11 = local_sp_12;
                        r9_5 = r9_6;
                        r8_7 = r8_8;
                        continue;
                    }
                }
                rax_1 = rax_5;
                r12_2 = 0UL;
                if ((uint64_t)(unsigned char)r12_1 == 0UL) {
                    if (rbp_2 != 0UL) {
                        var_92 = ((*(uint32_t *)(local_sp_13 + 28UL) & 4U) == 0U) ? 4255974UL : 4255744UL;
                        *(uint64_t *)(local_sp_13 + (-8L)) = 4206352UL;
                        indirect_placeholder_11(0UL, var_92, 0UL, rcx_2, 0UL, r9_5, r8_7);
                        *(uint64_t *)(local_sp_13 + (-16L)) = 4206362UL;
                        indirect_placeholder_10(rbx_3, 0UL, 1UL);
                        abort();
                    }
                    var_49 = local_sp_13 + (-8L);
                    *(uint64_t *)var_49 = 4206373UL;
                    var_50 = indirect_placeholder_20(rbp_2, rbp_2, r9_5, r8_7);
                    rcx_1 = var_50.field_0;
                    local_sp_9 = var_49;
                    r9_4 = var_50.field_1;
                    r8_6 = var_50.field_2;
                } else {
                    var_37 = (unsigned char *)(local_sp_13 + 14UL);
                    var_38 = (uint64_t)(*var_37 ^ '\x01');
                    var_39 = helper_cc_compute_c_wrapper(r14_1 - var_38, var_38, var_8, 14U);
                    rax_1 = var_38;
                    if (var_39 == 0UL) {
                        _pre_phi314 = (uint64_t *)(local_sp_13 + 16UL);
                    } else {
                        if (*(uint32_t *)(local_sp_13 + 24UL) != 0U) {
                            *(uint64_t *)(local_sp_13 + (-8L)) = 4206204UL;
                            indirect_placeholder_11(0UL, 4255920UL, 0UL, rcx_2, 0UL, r9_5, r8_7);
                            *(uint64_t *)(local_sp_13 + (-16L)) = 4206214UL;
                            indirect_placeholder_10(rbx_3, rbp_2, 1UL);
                            abort();
                        }
                        var_40 = (uint64_t *)(local_sp_13 + 16UL);
                        *var_40 = 1UL;
                        _pre_phi314 = var_40;
                    }
                    var_41 = (*var_37 == '\x00');
                    var_42 = *_pre_phi314;
                    if (var_41) {
                        var_47 = local_sp_13 + (-8L);
                        *(uint64_t *)var_47 = 4206247UL;
                        var_48 = indirect_placeholder_18(var_42, 18446744073709551615UL, r9_5, r8_7);
                        rcx_1 = var_48.field_0;
                        local_sp_9 = var_47;
                        r9_4 = var_48.field_1;
                        r8_6 = var_48.field_2;
                    } else {
                        var_43 = (var_42 > rbp_2);
                        var_44 = local_sp_13 + (-8L);
                        var_45 = (uint64_t *)var_44;
                        local_sp_9 = var_44;
                        if (!var_43) {
                            *var_45 = 4206281UL;
                            indirect_placeholder_11(0UL, 4256076UL, 0UL, rcx_2, 0UL, r9_5, r8_7);
                            *(uint64_t *)(local_sp_13 + (-16L)) = 4206291UL;
                            indirect_placeholder_10(rbx_3, rbp_2, 1UL);
                            abort();
                        }
                        *var_45 = 4206304UL;
                        var_46 = indirect_placeholder_19(var_42, rbp_2, r9_5, r8_7);
                        rcx_1 = var_46.field_0;
                        r9_4 = var_46.field_1;
                        r8_6 = var_46.field_2;
                    }
                }
                r8_0 = r8_6;
                r9_0 = r9_4;
                rax_4 = rax_1;
                rcx_3 = rcx_1;
                r8_8 = r8_6;
                local_sp_12 = local_sp_9;
                r9_6 = r9_4;
                if (*var_14 != '\x00') {
                    if (*(uint64_t *)6364936UL != 0UL) {
                        var_51 = local_sp_9 + (-8L);
                        *(uint64_t *)var_51 = 4206773UL;
                        indirect_placeholder();
                        local_sp_0 = var_51;
                        loop_state_var = 0U;
                        break;
                    }
                    var_91 = ((*(uint32_t *)(local_sp_9 + 28UL) & 4U) == 0U) ? 4256053UL : 4255872UL;
                    *(uint64_t *)(local_sp_9 + (-8L)) = 4206741UL;
                    indirect_placeholder_11(0UL, var_91, 0UL, rcx_1, 0UL, r9_4, r8_6);
                    *(uint64_t *)(local_sp_9 + (-16L)) = 4206751UL;
                    indirect_placeholder_10(rbx_3, rbp_2, 1UL);
                    abort();
                }
                *(unsigned char *)(local_sp_9 + 15UL) = (unsigned char)'\x00';
                *(unsigned char *)(local_sp_9 + 14UL) = (unsigned char)'\x00';
            }
        }
    switch (loop_state_var) {
      case 1U:
        {
            *(uint64_t *)(local_sp_11 + (-16L)) = 4206543UL;
            indirect_placeholder();
            var_25 = *(uint64_t *)6364488UL;
            *(uint64_t *)(local_sp_11 + (-24L)) = 4206558UL;
            var_26 = indirect_placeholder_7(var_25, rax_2);
            *(uint64_t *)(local_sp_11 + (-32L)) = 4206569UL;
            var_27 = indirect_placeholder_16(var_26);
            var_28 = var_27.field_0;
            var_29 = var_27.field_1;
            var_30 = var_27.field_2;
            var_31 = ((*(uint32_t *)(local_sp_11 + (-4L)) & 4U) == 0U) ? 4256001UL : 4255792UL;
            *(uint64_t *)(local_sp_11 + (-40L)) = 4206613UL;
            indirect_placeholder_11(0UL, var_31, 0UL, var_28, 0UL, var_29, var_30);
            *(uint64_t *)(local_sp_11 + (-48L)) = 4206621UL;
            indirect_placeholder();
            *(uint64_t *)(local_sp_11 + (-56L)) = 4206631UL;
            indirect_placeholder_10(var_26, rbp_2, 1UL);
            abort();
        }
        break;
      case 0U:
        {
            var_52 = *(uint64_t *)6364936UL;
            var_53 = helper_cc_compute_c_wrapper(rbx_0 - var_52, var_52, var_8, 17U);
            local_sp_8 = local_sp_0;
            r8_5 = r8_0;
            local_sp_6 = local_sp_0;
            local_sp_1 = local_sp_0;
            r8_1 = r8_0;
            local_sp_7 = local_sp_0;
            while (var_53 != 0UL)
                {
                    var_54 = rbx_0 + 1UL;
                    rbx_0 = var_54;
                    var_55 = *(uint64_t *)6364928UL;
                    var_56 = var_54 << 4UL;
                    var_57 = var_56 + var_55;
                    var_58 = rbp_0 + (-32L);
                    var_59 = var_58 + var_55;
                    var_60 = *(uint64_t *)(var_59 + 8UL);
                    rax_0 = var_60;
                    rdi1_0 = var_57;
                    rcx_0 = var_59;
                    if (var_52 <= var_54 & *(uint64_t *)var_57 > var_60) {
                        var_61 = *(uint64_t *)(rdi1_0 + 8UL);
                        var_62 = helper_cc_compute_c_wrapper(rax_0 - var_61, var_61, var_8, 17U);
                        *(uint64_t *)(rcx_0 + 8UL) = ((var_62 == 0UL) ? rax_0 : var_61);
                        var_63 = local_sp_7 + (-8L);
                        *(uint64_t *)var_63 = 4206895UL;
                        indirect_placeholder();
                        var_64 = *(uint64_t *)6364936UL + (-1L);
                        *(uint64_t *)6364936UL = var_64;
                        var_65 = helper_cc_compute_c_wrapper(var_54 - var_64, var_64, var_8, 17U);
                        local_sp_7 = var_63;
                        local_sp_8 = var_63;
                        r8_5 = var_61;
                        while (var_65 != 0UL)
                            {
                                var_66 = *(uint64_t *)6364928UL;
                                var_67 = var_56 + var_66;
                                var_68 = var_58 + var_66;
                                var_69 = *(uint64_t *)(var_68 + 8UL);
                                rax_0 = var_69;
                                rdi1_0 = var_67;
                                rcx_0 = var_68;
                                if (*(uint64_t *)var_67 > var_69) {
                                    break;
                                }
                                var_61 = *(uint64_t *)(rdi1_0 + 8UL);
                                var_62 = helper_cc_compute_c_wrapper(rax_0 - var_61, var_61, var_8, 17U);
                                *(uint64_t *)(rcx_0 + 8UL) = ((var_62 == 0UL) ? rax_0 : var_61);
                                var_63 = local_sp_7 + (-8L);
                                *(uint64_t *)var_63 = 4206895UL;
                                indirect_placeholder();
                                var_64 = *(uint64_t *)6364936UL + (-1L);
                                *(uint64_t *)6364936UL = var_64;
                                var_65 = helper_cc_compute_c_wrapper(var_54 - var_64, var_64, var_8, 17U);
                                local_sp_7 = var_63;
                                local_sp_8 = var_63;
                                r8_5 = var_61;
                            }
                    }
                    rbp_0 = rbp_0 + 16UL;
                    local_sp_0 = local_sp_8;
                    r8_0 = r8_5;
                    var_52 = *(uint64_t *)6364936UL;
                    var_53 = helper_cc_compute_c_wrapper(rbx_0 - var_52, var_52, var_8, 17U);
                    local_sp_8 = local_sp_0;
                    r8_5 = r8_0;
                    local_sp_6 = local_sp_0;
                    local_sp_1 = local_sp_0;
                    r8_1 = r8_0;
                    local_sp_7 = local_sp_0;
                }
            if ((*(unsigned char *)(local_sp_0 + 28UL) & '\x02') != '\x00') {
                var_70 = *(uint64_t *)6364928UL;
                *(uint64_t *)6364928UL = 0UL;
                *(uint64_t *)6364936UL = 0UL;
                *(uint64_t *)6364496UL = 0UL;
                var_71 = *(uint64_t *)var_70;
                if (var_71 > 1UL) {
                    var_72 = var_71 + (-1L);
                    var_73 = local_sp_0 + (-8L);
                    *(uint64_t *)var_73 = 4207013UL;
                    var_74 = indirect_placeholder_13(1UL, var_72, r9_4, r8_0);
                    local_sp_1 = var_73;
                    r9_0 = var_74.field_1;
                    r8_1 = var_74.field_2;
                }
                local_sp_4 = local_sp_1;
                local_sp_2 = local_sp_1;
                r9_1 = r9_0;
                r9_3 = r9_0;
                r8_2 = r8_1;
                r8_4 = r8_1;
                if (var_52 <= 1UL) {
                    rbx_1 = var_70 + 8UL;
                    while (1U)
                        {
                            var_75 = *(uint64_t *)rbx_1 + 1UL;
                            var_76 = *(uint64_t *)(rbx_1 + 8UL);
                            local_sp_3 = local_sp_2;
                            r9_2 = r9_1;
                            r8_3 = r8_2;
                            if (var_75 == var_76) {
                                var_77 = var_76 + (-1L);
                                var_78 = local_sp_2 + (-8L);
                                *(uint64_t *)var_78 = 4207054UL;
                                var_79 = indirect_placeholder_14(var_75, var_77, r9_1, r8_2);
                                local_sp_3 = var_78;
                                r9_2 = var_79.field_1;
                                r8_3 = var_79.field_2;
                            }
                            var_80 = r12_0 + 1UL;
                            local_sp_4 = local_sp_3;
                            r12_0 = var_80;
                            local_sp_2 = local_sp_3;
                            r9_1 = r9_2;
                            r9_3 = r9_2;
                            r8_2 = r8_3;
                            r8_4 = r8_3;
                            if (var_80 == var_52) {
                                break;
                            }
                            rbx_1 = rbx_1 + 16UL;
                            continue;
                        }
                }
                var_81 = *(uint64_t *)(((var_52 << 4UL) + var_70) + (-8L));
                local_sp_5 = local_sp_4;
                if (var_81 != 18446744073709551615UL) {
                    var_82 = var_81 + 1UL;
                    var_83 = local_sp_4 + (-8L);
                    *(uint64_t *)var_83 = 4207101UL;
                    indirect_placeholder_15(var_82, 18446744073709551615UL, r9_3, r8_4);
                    local_sp_5 = var_83;
                }
                var_84 = local_sp_5 + (-8L);
                *(uint64_t *)var_84 = 4207109UL;
                indirect_placeholder();
                local_sp_6 = var_84;
            }
            var_85 = *(uint64_t *)6364936UL + 1UL;
            *(uint64_t *)6364936UL = var_85;
            var_86 = var_85 << 4UL;
            var_87 = *(uint64_t *)6364928UL;
            *(uint64_t *)(local_sp_6 + (-8L)) = 4207143UL;
            var_88 = indirect_placeholder_7(var_87, var_86);
            *(uint64_t *)6364928UL = var_88;
            var_89 = (*(uint64_t *)6364936UL << 4UL) + var_88;
            var_90 = var_89 + (-16L);
            *(uint64_t *)(var_89 + (-8L)) = 18446744073709551615UL;
            *(uint64_t *)var_90 = 18446744073709551615UL;
            return;
        }
        break;
    }
}

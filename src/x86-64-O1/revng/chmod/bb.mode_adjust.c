typedef unsigned char __u_char;
typedef unsigned short __u_short;
typedef unsigned int __u_int;
typedef unsigned long __u_long;
typedef signed char __int8_t;
typedef unsigned char __uint8_t;
typedef short __int16_t;
typedef unsigned short __uint16_t;
typedef int __int32_t;
typedef unsigned int __uint32_t;
typedef long __int64_t;
typedef unsigned long __uint64_t;
typedef __int8_t __int_least8_t;
typedef __uint8_t __uint_least8_t;
typedef __int16_t __int_least16_t;
typedef __uint16_t __uint_least16_t;
typedef __int32_t __int_least32_t;
typedef __uint32_t __uint_least32_t;
typedef __int64_t __int_least64_t;
typedef __uint64_t __uint_least64_t;
typedef long __quad_t;
typedef unsigned long __u_quad_t;
typedef long __intmax_t;
typedef unsigned long __uintmax_t;
typedef unsigned long __dev_t;
typedef unsigned int __uid_t;
typedef unsigned int __gid_t;
typedef unsigned long __ino_t;
typedef unsigned long __ino64_t;
typedef unsigned int __mode_t;
typedef unsigned long __nlink_t;
typedef long __off_t;
typedef long __off64_t;
typedef int __pid_t;
typedef struct {
    int __val[2];
} __fsid_t;
typedef long __clock_t;
typedef unsigned long __rlim_t;
typedef unsigned long __rlim64_t;
typedef unsigned int __id_t;
typedef long __time_t;
typedef unsigned int __useconds_t;
typedef long __suseconds_t;
typedef int __daddr_t;
typedef int __key_t;
typedef int __clockid_t;
typedef void *__timer_t;
typedef long __blksize_t;
typedef long __blkcnt_t;
typedef long __blkcnt64_t;
typedef unsigned long __fsblkcnt_t;
typedef unsigned long __fsblkcnt64_t;
typedef unsigned long __fsfilcnt_t;
typedef unsigned long __fsfilcnt64_t;
typedef long __fsword_t;
typedef long __ssize_t;
typedef long __syscall_slong_t;
typedef unsigned long __syscall_ulong_t;
typedef __off64_t __loff_t;
typedef char *__caddr_t;
typedef long __intptr_t;
typedef unsigned int __socklen_t;
typedef int __sig_atomic_t;
typedef __int8_t int8_t;
typedef __int16_t int16_t;
typedef __int32_t int32_t;
typedef __int64_t int64_t;
typedef __uint8_t uint8_t;
typedef __uint16_t uint16_t;
typedef __uint32_t uint32_t;
typedef __uint64_t uint64_t;
typedef __int_least8_t int_least8_t;
typedef __int_least16_t int_least16_t;
typedef __int_least32_t int_least32_t;
typedef __int_least64_t int_least64_t;
typedef __uint_least8_t uint_least8_t;
typedef __uint_least16_t uint_least16_t;
typedef __uint_least32_t uint_least32_t;
typedef __uint_least64_t uint_least64_t;
typedef signed char int_fast8_t;
typedef long int_fast16_t;
typedef long int_fast32_t;
typedef long int_fast64_t;
typedef unsigned char uint_fast8_t;
typedef unsigned long uint_fast16_t;
typedef unsigned long uint_fast32_t;
typedef unsigned long uint_fast64_t;
typedef long intptr_t;
typedef unsigned long uintptr_t;
typedef __intmax_t intmax_t;
typedef __uintmax_t uintmax_t;
typedef _Bool bool;
struct bb_mode_adjust_ret_type;
struct bb_mode_adjust_ret_type {
    uint64_t field_0;
    uint64_t field_1;
};
extern uint64_t revng_init_local_sp(uint64_t param_0);
extern uint64_t init_rbx(void);
extern uint64_t init_rbp(void);
extern uint64_t init_cc_src2(void);
extern uint64_t helper_cc_compute_c_wrapper(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint32_t param_3);
extern uint64_t init_r13(void);
extern uint64_t init_r12(void);
struct bb_mode_adjust_ret_type bb_mode_adjust(uint64_t rdx, uint64_t rdi, uint64_t rcx, uint64_t r10, uint64_t rsi, uint64_t r8) {
    uint64_t var_0;
    uint64_t var_1;
    uint64_t var_2;
    uint64_t var_3;
    uint64_t var_4;
    uint64_t var_5;
    uint64_t var_6;
    unsigned char var_7;
    uint64_t rbx_2;
    uint64_t var_8;
    uint64_t var_9;
    uint64_t var_10;
    uint64_t r105_1;
    uint64_t rbx_0;
    unsigned char rdi3_0_in;
    uint64_t rcx4_0;
    uint64_t r105_0;
    uint64_t cc_src2_0;
    uint64_t r105_2;
    uint64_t rax_0;
    uint32_t var_11;
    uint64_t var_12;
    uint64_t var_13;
    uint64_t var_14;
    uint64_t var_15;
    uint64_t cc_src2_1;
    uint64_t var_16;
    uint64_t var_17;
    uint64_t var_18;
    uint64_t var_19;
    uint64_t var_20;
    uint64_t var_21;
    bool var_22;
    uint64_t var_23;
    unsigned char var_24;
    uint64_t rax_1;
    uint64_t rax_2;
    uint64_t rbx_1;
    uint64_t var_25;
    uint64_t var_26;
    uint64_t var_27;
    unsigned char var_28;
    struct bb_mode_adjust_ret_type mrv;
    struct bb_mode_adjust_ret_type mrv1;
    var_0 = revng_init_local_sp(0UL);
    var_1 = init_rbx();
    var_2 = init_rbp();
    var_3 = init_r13();
    var_4 = init_r12();
    var_5 = init_cc_src2();
    *(uint64_t *)(var_0 + (-8L)) = var_3;
    *(uint64_t *)(var_0 + (-16L)) = var_4;
    *(uint64_t *)(var_0 + (-24L)) = var_2;
    *(uint64_t *)(var_0 + (-32L)) = var_1;
    var_6 = (uint64_t)((uint16_t)rdi & (unsigned short)4095U);
    var_7 = *(unsigned char *)(rcx + 1UL);
    rbx_2 = 0UL;
    rbx_0 = 0UL;
    rdi3_0_in = var_7;
    rcx4_0 = rcx;
    cc_src2_0 = var_5;
    r105_2 = r10;
    rax_0 = var_6;
    rax_2 = var_6;
    if (var_7 != '\x00') {
        var_8 = (uint64_t)(unsigned char)rsi;
        var_9 = rdx ^ 4294967295UL;
        var_10 = rsi + (-1L);
        while (1U)
            {
                var_11 = *(uint32_t *)(rcx4_0 + 4UL);
                var_12 = (uint64_t)var_11;
                var_13 = helper_cc_compute_c_wrapper(var_10, 1UL, cc_src2_0, 14U);
                var_14 = ((uint64_t)(((unsigned short)0U - (uint16_t)var_13) & (unsigned short)3072U) ^ 3072UL) & (uint64_t)(*(uint32_t *)(rcx4_0 + 12UL) ^ (-1));
                var_15 = (uint64_t)*(uint32_t *)(rcx4_0 + 8UL);
                cc_src2_1 = var_13;
                r105_0 = var_15;
                rbx_1 = rbx_0;
                rax_1 = rax_0;
                if ((uint64_t)(rdi3_0_in + '\xfe') == 0UL) {
                    r105_0 = (((rax_0 & 73UL) | var_8) == 0UL) ? var_15 : (var_15 | 73UL);
                } else {
                    if ((uint64_t)(rdi3_0_in + '\xfd') == 0UL) {
                        var_16 = rax_0 & var_15;
                        var_17 = helper_cc_compute_c_wrapper((uint64_t)((uint16_t)var_16 & (unsigned short)292U) + (-1L), 1UL, var_13, 16U);
                        var_18 = (uint64_t)(((unsigned short)0U - (uint16_t)var_17) & (unsigned short)292U);
                        var_19 = helper_cc_compute_c_wrapper((uint64_t)((unsigned char)var_16 & '\x92') + (-1L), 1UL, var_17, 16U);
                        var_20 = var_18 | (uint64_t)(('\x00' - (unsigned char)var_19) & '\x92');
                        var_21 = helper_cc_compute_c_wrapper((var_16 & 73UL) + (-1L), 1UL, var_19, 16U);
                        cc_src2_1 = var_21;
                        r105_0 = var_16 | ((var_20 | ((0UL - var_21) & 73UL)) ^ 511UL);
                    }
                }
                var_22 = (var_11 == 0U);
                var_23 = (r105_0 & (var_14 ^ 4294967295UL)) & (uint64_t)(uint32_t)(var_22 ? var_9 : var_12);
                var_24 = *(unsigned char *)rcx4_0;
                cc_src2_0 = cc_src2_1;
                r105_1 = var_23;
                if ((uint64_t)(var_24 + '\xd3') == 0UL) {
                    var_26 = (uint64_t)(uint32_t)rbx_0 | var_23;
                    var_27 = var_23 ^ 4294967295UL;
                    rbx_1 = var_26;
                    r105_1 = var_27;
                    rax_1 = rax_0 & var_27;
                } else {
                    if ((uint64_t)(var_24 + '\xc3') == 0UL) {
                        var_25 = var_14 | (var_22 ? 0UL : (var_12 ^ 4294967295UL));
                        rbx_1 = (uint64_t)(uint32_t)rbx_0 | ((uint64_t)((uint16_t)var_25 & (unsigned short)4095U) ^ 4095UL);
                        rax_1 = (rax_0 & var_25) | var_23;
                    } else {
                        if ((uint64_t)(var_24 + '\xd5') == 0UL) {
                            rbx_1 = (uint64_t)(uint32_t)rbx_0 | var_23;
                            rax_1 = (uint64_t)(uint32_t)rax_0 | var_23;
                        }
                    }
                }
                var_28 = *(unsigned char *)(rcx4_0 + 17UL);
                rbx_2 = rbx_1;
                rbx_0 = rbx_1;
                rdi3_0_in = var_28;
                r105_2 = r105_1;
                rax_0 = rax_1;
                rax_2 = rax_1;
                if (var_28 == '\x00') {
                    break;
                }
                rcx4_0 = rcx4_0 + 16UL;
                continue;
            }
    }
    if (r8 == 0UL) {
        mrv.field_0 = rax_2;
        mrv1 = mrv;
        mrv1.field_1 = r105_2;
        return mrv1;
    }
    *(uint32_t *)r8 = (uint32_t)rbx_2;
    mrv.field_0 = rax_2;
    mrv1 = mrv;
    mrv1.field_1 = r105_2;
    return mrv1;
}

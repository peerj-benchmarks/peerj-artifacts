typedef unsigned char __u_char;
typedef unsigned short __u_short;
typedef unsigned int __u_int;
typedef unsigned long __u_long;
typedef signed char __int8_t;
typedef unsigned char __uint8_t;
typedef short __int16_t;
typedef unsigned short __uint16_t;
typedef int __int32_t;
typedef unsigned int __uint32_t;
typedef long __int64_t;
typedef unsigned long __uint64_t;
typedef __int8_t __int_least8_t;
typedef __uint8_t __uint_least8_t;
typedef __int16_t __int_least16_t;
typedef __uint16_t __uint_least16_t;
typedef __int32_t __int_least32_t;
typedef __uint32_t __uint_least32_t;
typedef __int64_t __int_least64_t;
typedef __uint64_t __uint_least64_t;
typedef long __quad_t;
typedef unsigned long __u_quad_t;
typedef long __intmax_t;
typedef unsigned long __uintmax_t;
typedef unsigned long __dev_t;
typedef unsigned int __uid_t;
typedef unsigned int __gid_t;
typedef unsigned long __ino_t;
typedef unsigned long __ino64_t;
typedef unsigned int __mode_t;
typedef unsigned long __nlink_t;
typedef long __off_t;
typedef long __off64_t;
typedef int __pid_t;
typedef struct {
    int __val[2];
} __fsid_t;
typedef long __clock_t;
typedef unsigned long __rlim_t;
typedef unsigned long __rlim64_t;
typedef unsigned int __id_t;
typedef long __time_t;
typedef unsigned int __useconds_t;
typedef long __suseconds_t;
typedef int __daddr_t;
typedef int __key_t;
typedef int __clockid_t;
typedef void *__timer_t;
typedef long __blksize_t;
typedef long __blkcnt_t;
typedef long __blkcnt64_t;
typedef unsigned long __fsblkcnt_t;
typedef unsigned long __fsblkcnt64_t;
typedef unsigned long __fsfilcnt_t;
typedef unsigned long __fsfilcnt64_t;
typedef long __fsword_t;
typedef long __ssize_t;
typedef long __syscall_slong_t;
typedef unsigned long __syscall_ulong_t;
typedef __off64_t __loff_t;
typedef char *__caddr_t;
typedef long __intptr_t;
typedef unsigned int __socklen_t;
typedef int __sig_atomic_t;
typedef __int8_t int8_t;
typedef __int16_t int16_t;
typedef __int32_t int32_t;
typedef __int64_t int64_t;
typedef __uint8_t uint8_t;
typedef __uint16_t uint16_t;
typedef __uint32_t uint32_t;
typedef __uint64_t uint64_t;
typedef __int_least8_t int_least8_t;
typedef __int_least16_t int_least16_t;
typedef __int_least32_t int_least32_t;
typedef __int_least64_t int_least64_t;
typedef __uint_least8_t uint_least8_t;
typedef __uint_least16_t uint_least16_t;
typedef __uint_least32_t uint_least32_t;
typedef __uint_least64_t uint_least64_t;
typedef signed char int_fast8_t;
typedef long int_fast16_t;
typedef long int_fast32_t;
typedef long int_fast64_t;
typedef unsigned char uint_fast8_t;
typedef unsigned long uint_fast16_t;
typedef unsigned long uint_fast32_t;
typedef unsigned long uint_fast64_t;
typedef long intptr_t;
typedef unsigned long uintptr_t;
typedef __intmax_t intmax_t;
typedef __uintmax_t uintmax_t;
typedef _Bool bool;
struct indirect_placeholder_33_ret_type;
struct indirect_placeholder_32_ret_type;
struct indirect_placeholder_33_ret_type {
    uint64_t field_0;
    uint64_t field_1;
    uint64_t field_2;
    uint64_t field_3;
};
struct indirect_placeholder_32_ret_type {
    uint64_t field_0;
    uint64_t field_1;
    uint64_t field_2;
};
extern void function_dispatcher(unsigned char *param_0);
extern uint64_t revng_init_local_sp(uint64_t param_0);
extern uint64_t init_rbx(void);
extern void indirect_placeholder(void);
extern uint64_t init_rbp(void);
extern uint64_t init_r14(void);
extern uint64_t init_r13(void);
extern uint64_t init_r12(void);
extern uint64_t init_r15(void);
extern uint64_t init_rax(void);
extern void indirect_placeholder_4(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint64_t param_3, uint64_t param_4, uint64_t param_5, uint64_t param_6);
extern struct indirect_placeholder_33_ret_type indirect_placeholder_33(uint64_t param_0);
extern struct indirect_placeholder_32_ret_type indirect_placeholder_32(uint64_t param_0);
uint64_t bb_print_it(uint64_t rdx, uint64_t rdi, uint64_t rcx, uint64_t r8, uint64_t rsi) {
    uint64_t var_0;
    uint64_t var_1;
    uint64_t var_2;
    uint64_t var_3;
    uint64_t var_4;
    uint64_t var_5;
    uint64_t var_6;
    uint64_t var_7;
    uint64_t var_8;
    uint64_t var_9;
    struct indirect_placeholder_33_ret_type var_10;
    uint64_t var_11;
    unsigned char var_12;
    unsigned char rdi2_0_in;
    uint64_t var_24;
    unsigned char var_25;
    uint64_t rax_0_in;
    bool var_26;
    uint64_t var_27;
    uint64_t rax_6;
    uint64_t rax_1;
    uint64_t rbp_0;
    uint64_t rax_5;
    uint64_t var_28;
    uint64_t var_22;
    uint64_t var_23;
    uint64_t local_sp_1;
    uint64_t r12_2;
    uint64_t local_sp_0;
    uint64_t rcx3_1;
    uint64_t rbx_0;
    uint64_t rbp_2;
    uint64_t rax_3;
    uint64_t rbp_1;
    uint64_t r84_1;
    uint64_t r84_2;
    uint64_t rcx3_0;
    uint64_t r9_1;
    uint64_t r9_2;
    uint64_t r84_0;
    uint64_t r9_0;
    uint64_t var_65;
    unsigned char var_66;
    uint64_t local_sp_5;
    uint64_t local_sp_3;
    uint64_t storemerge;
    uint64_t var_53;
    uint64_t var_54;
    uint64_t var_55;
    uint64_t var_56;
    uint64_t var_57;
    unsigned char *var_58;
    unsigned char var_47;
    struct indirect_placeholder_32_ret_type var_59;
    uint64_t var_60;
    uint64_t var_61;
    uint64_t var_62;
    uint64_t var_63;
    uint64_t rax_4;
    uint64_t var_64;
    uint64_t var_45;
    uint64_t var_46;
    unsigned char *_cast1_phi_trans_insert;
    unsigned char *_cast1_pre_phi;
    uint64_t local_sp_2;
    uint64_t var_48;
    uint32_t var_49;
    uint64_t var_50;
    uint64_t var_51;
    uint64_t var_52;
    uint64_t var_13;
    uint64_t var_14;
    uint64_t var_15;
    uint64_t rcx3_2;
    uint64_t var_16;
    uint64_t var_40;
    uint64_t var_17;
    unsigned char *var_18;
    unsigned char var_19;
    uint64_t var_20;
    uint64_t var_34;
    unsigned char var_35;
    uint64_t var_36;
    unsigned char var_37;
    uint64_t var_38;
    uint64_t var_39;
    unsigned char var_29;
    uint64_t var_21;
    uint64_t local_sp_4;
    uint64_t var_33;
    uint64_t var_30;
    uint64_t var_31;
    uint64_t var_32;
    uint64_t var_41;
    uint64_t var_42;
    uint64_t var_43;
    unsigned char *_cast;
    unsigned char var_44;
    var_0 = revng_init_local_sp(0UL);
    var_1 = init_rax();
    var_2 = init_rbx();
    var_3 = init_r14();
    var_4 = init_rbp();
    var_5 = init_r13();
    var_6 = init_r12();
    var_7 = init_r15();
    *(uint64_t *)(var_0 + (-8L)) = var_7;
    *(uint64_t *)(var_0 + (-16L)) = var_3;
    *(uint64_t *)(var_0 + (-24L)) = var_5;
    *(uint64_t *)(var_0 + (-32L)) = var_6;
    *(uint64_t *)(var_0 + (-40L)) = var_4;
    *(uint64_t *)(var_0 + (-48L)) = var_2;
    *(uint32_t *)(var_0 + (-84L)) = (uint32_t)rsi;
    *(uint64_t *)(var_0 + (-80L)) = rdx;
    *(uint64_t *)(var_0 + (-72L)) = rcx;
    *(uint64_t *)(var_0 + (-64L)) = r8;
    *(uint64_t *)(var_0 + (-96L)) = 4207423UL;
    indirect_placeholder();
    var_8 = var_1 + 3UL;
    var_9 = var_0 + (-104L);
    *(uint64_t *)var_9 = 4207432UL;
    var_10 = indirect_placeholder_33(var_8);
    var_11 = var_10.field_0;
    var_12 = *(unsigned char *)rdi;
    rdi2_0_in = var_12;
    rax_1 = 0UL;
    rax_5 = var_11;
    rbx_0 = rdi;
    local_sp_5 = var_9;
    local_sp_3 = var_9;
    if (var_12 == '\x00') {
        *(unsigned char *)(var_0 + (-101L)) = (unsigned char)'\x00';
    } else {
        var_13 = var_10.field_3;
        var_14 = var_10.field_2;
        var_15 = var_10.field_1;
        *(unsigned char *)(var_0 + (-101L)) = (unsigned char)'\x00';
        rcx3_2 = var_15;
        r84_2 = var_14;
        r9_2 = var_13;
        while (1U)
            {
                rcx3_1 = rcx3_2;
                rax_3 = rax_5;
                rbp_1 = rbx_0;
                r84_1 = r84_2;
                rcx3_0 = rcx3_2;
                r9_1 = r9_2;
                r84_0 = r84_2;
                r9_0 = r9_2;
                rax_4 = rax_5;
                local_sp_4 = local_sp_3;
                if ((uint64_t)(rdi2_0_in + '\xdb') != 0UL) {
                    if ((uint64_t)(rdi2_0_in + '\xa4') == 0UL) {
                        var_16 = local_sp_3 + (-8L);
                        *(uint64_t *)var_16 = 4208135UL;
                        indirect_placeholder();
                        local_sp_0 = var_16;
                    } else {
                        rax_3 = 0UL;
                        if (*(unsigned char *)6404272UL == '\x00') {
                            var_40 = local_sp_3 + (-8L);
                            *(uint64_t *)var_40 = 4207719UL;
                            indirect_placeholder();
                            local_sp_0 = var_40;
                            rax_3 = rax_5;
                        } else {
                            var_17 = rbx_0 + 1UL;
                            var_18 = (unsigned char *)var_17;
                            var_19 = *var_18;
                            var_20 = (uint64_t)((uint32_t)(uint64_t)var_19 + (-48));
                            rbp_1 = var_17;
                            var_29 = var_19;
                            if ((uint64_t)((unsigned char)var_20 & '\xf8') == 0UL) {
                                var_34 = rbx_0 + 2UL;
                                var_35 = *(unsigned char *)var_34;
                                rax_6 = (uint64_t)var_35;
                                rbp_2 = var_34;
                                var_36 = rbx_0 + 3UL;
                                var_37 = *(unsigned char *)var_36;
                                rax_6 = (uint64_t)var_37;
                                rbp_2 = var_36;
                                if ((uint64_t)((var_35 + '\xd0') & '\xf8') != 0UL & (uint64_t)((var_37 + '\xd0') & '\xf8') == 0UL) {
                                    rax_6 = (uint64_t)(uint32_t)(uint64_t)var_37;
                                    rbp_2 = rbx_0 + 4UL;
                                }
                                var_38 = local_sp_3 + (-8L);
                                *(uint64_t *)var_38 = 4207782UL;
                                indirect_placeholder();
                                var_39 = rbp_2 + (-1L);
                                local_sp_0 = var_38;
                                rax_3 = rax_6;
                                rbp_1 = var_39;
                            } else {
                                if ((uint64_t)(var_19 + '\x88') != 0UL) {
                                    var_21 = local_sp_3 + (-8L);
                                    *(uint64_t *)var_21 = 4207810UL;
                                    indirect_placeholder();
                                    local_sp_4 = var_21;
                                    if (var_20 != 0UL) {
                                        var_22 = rbx_0 + 2UL;
                                        var_23 = (uint64_t)(uint32_t)(uint64_t)*(unsigned char *)var_22;
                                        *(uint64_t *)(local_sp_3 + (-16L)) = 4207872UL;
                                        indirect_placeholder();
                                        rbp_0 = var_22;
                                        if (var_23 != 0UL) {
                                            var_24 = rbx_0 + 3UL;
                                            var_25 = *(unsigned char *)var_24;
                                            rbp_0 = var_24;
                                            if ((uint64_t)((var_25 + '\x9f') & '\xfe') > 5UL) {
                                                rax_0_in = (uint64_t)var_25 + 4294967209UL;
                                            } else {
                                                var_26 = ((uint64_t)((var_25 + '\xbf') & '\xfe') > 5UL);
                                                var_27 = (uint64_t)var_25;
                                                if (var_26) {
                                                    rax_0_in = var_27 + 4294967248UL;
                                                } else {
                                                    rax_0_in = var_27 + 4294967241UL;
                                                }
                                            }
                                            rax_1 = (uint64_t)(uint32_t)rax_0_in;
                                        }
                                        var_28 = local_sp_3 + (-24L);
                                        *(uint64_t *)var_28 = 4207941UL;
                                        indirect_placeholder();
                                        local_sp_0 = var_28;
                                        rax_3 = rax_1;
                                        rbp_1 = rbp_0;
                                        var_65 = rbp_1 + 1UL;
                                        var_66 = *(unsigned char *)var_65;
                                        rdi2_0_in = var_66;
                                        rax_5 = rax_3;
                                        rbx_0 = var_65;
                                        r84_2 = r84_0;
                                        r9_2 = r9_0;
                                        local_sp_5 = local_sp_0;
                                        local_sp_3 = local_sp_0;
                                        rcx3_2 = rcx3_0;
                                        if (var_66 != '\x00') {
                                            continue;
                                        }
                                        break;
                                    }
                                    var_29 = *var_18;
                                }
                                if (var_29 == '\x00') {
                                    *(uint64_t *)(local_sp_4 + (-8L)) = 4207981UL;
                                    indirect_placeholder_4(0UL, 4282384UL, 0UL, rcx3_2, r84_2, 0UL, r9_2);
                                    var_33 = local_sp_4 + (-16L);
                                    *(uint64_t *)var_33 = 4207991UL;
                                    indirect_placeholder();
                                    local_sp_0 = var_33;
                                } else {
                                    var_30 = (uint64_t)(var_29 + '\xde');
                                    if (var_30 <= 84UL) {
                                        function_dispatcher((unsigned char *)(0UL));
                                        return var_30;
                                    }
                                    var_31 = (uint64_t)(uint32_t)(uint64_t)var_29;
                                    *(uint64_t *)(local_sp_4 + (-8L)) = 4208111UL;
                                    indirect_placeholder_4(0UL, 4282424UL, 0UL, var_31, r84_2, 0UL, r9_2);
                                    var_32 = local_sp_4 + (-16L);
                                    *(uint64_t *)var_32 = 4208124UL;
                                    indirect_placeholder();
                                    local_sp_0 = var_32;
                                    rcx3_0 = var_31;
                                }
                            }
                        }
                    }
                    var_65 = rbp_1 + 1UL;
                    var_66 = *(unsigned char *)var_65;
                    rdi2_0_in = var_66;
                    rax_5 = rax_3;
                    rbx_0 = var_65;
                    r84_2 = r84_0;
                    r9_2 = r9_0;
                    local_sp_5 = local_sp_0;
                    local_sp_3 = local_sp_0;
                    rcx3_2 = rcx3_0;
                    if (var_66 != '\x00') {
                        continue;
                    }
                    break;
                }
                *(uint64_t *)(local_sp_3 + (-8L)) = 4207490UL;
                indirect_placeholder();
                var_41 = (rax_5 + rbx_0) + 1UL;
                var_42 = local_sp_3 + (-16L);
                *(uint64_t *)var_42 = 4207508UL;
                indirect_placeholder();
                var_43 = var_41 + rax_5;
                _cast = (unsigned char *)var_43;
                var_44 = *_cast;
                _cast1_pre_phi = _cast;
                var_47 = var_44;
                local_sp_2 = var_42;
                r12_2 = var_43;
                if (var_44 == '.') {
                    var_45 = local_sp_3 + (-24L);
                    *(uint64_t *)var_45 = 4207533UL;
                    indirect_placeholder();
                    var_46 = (rax_5 + var_43) + 1UL;
                    _cast1_phi_trans_insert = (unsigned char *)var_46;
                    _cast1_pre_phi = _cast1_phi_trans_insert;
                    var_47 = *_cast1_phi_trans_insert;
                    local_sp_2 = var_45;
                    r12_2 = var_46;
                }
                var_48 = r12_2 + (rbx_0 ^ (-1L));
                var_49 = (uint32_t)(uint64_t)var_47;
                var_50 = (uint64_t)var_49;
                var_51 = r12_2 - rbx_0;
                var_52 = local_sp_2 + (-8L);
                *(uint64_t *)var_52 = 4207567UL;
                indirect_placeholder();
                rbp_1 = r12_2;
                storemerge = r12_2;
                local_sp_1 = var_52;
                if (var_50 != 0UL) {
                    if ((uint64_t)(var_49 + (-37)) != 0UL) {
                        var_53 = *(uint64_t *)(local_sp_2 + 16UL);
                        var_54 = *(uint64_t *)local_sp_2;
                        var_55 = (uint64_t)*(uint32_t *)(local_sp_2 + (-4L));
                        var_56 = *(uint64_t *)(local_sp_2 + 8UL);
                        var_57 = local_sp_2 + (-16L);
                        *(uint64_t *)var_57 = 4207688UL;
                        indirect_placeholder();
                        var_58 = (unsigned char *)(local_sp_2 + (-13L));
                        *var_58 = (*var_58 | (unsigned char)var_56);
                        local_sp_0 = var_57;
                        rax_3 = var_56;
                        rcx3_0 = var_55;
                        r84_0 = var_54;
                        r9_0 = var_53;
                        var_65 = rbp_1 + 1UL;
                        var_66 = *(unsigned char *)var_65;
                        rdi2_0_in = var_66;
                        rax_5 = rax_3;
                        rbx_0 = var_65;
                        r84_2 = r84_0;
                        r9_2 = r9_0;
                        local_sp_5 = local_sp_0;
                        local_sp_3 = local_sp_0;
                        rcx3_2 = rcx3_0;
                        if (var_66 != '\x00') {
                            continue;
                        }
                        break;
                    }
                }
                storemerge = r12_2 + (-1L);
            }
    }
    *(uint64_t *)(local_sp_5 + (-8L)) = 4208204UL;
    indirect_placeholder();
    *(uint64_t *)(local_sp_5 + (-16L)) = 4208223UL;
    indirect_placeholder();
    return (uint64_t)*(unsigned char *)(local_sp_5 + (-13L));
}

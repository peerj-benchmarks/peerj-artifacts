typedef unsigned char __u_char;
typedef unsigned short __u_short;
typedef unsigned int __u_int;
typedef unsigned long __u_long;
typedef signed char __int8_t;
typedef unsigned char __uint8_t;
typedef short __int16_t;
typedef unsigned short __uint16_t;
typedef int __int32_t;
typedef unsigned int __uint32_t;
typedef long __int64_t;
typedef unsigned long __uint64_t;
typedef __int8_t __int_least8_t;
typedef __uint8_t __uint_least8_t;
typedef __int16_t __int_least16_t;
typedef __uint16_t __uint_least16_t;
typedef __int32_t __int_least32_t;
typedef __uint32_t __uint_least32_t;
typedef __int64_t __int_least64_t;
typedef __uint64_t __uint_least64_t;
typedef long __quad_t;
typedef unsigned long __u_quad_t;
typedef long __intmax_t;
typedef unsigned long __uintmax_t;
typedef unsigned long __dev_t;
typedef unsigned int __uid_t;
typedef unsigned int __gid_t;
typedef unsigned long __ino_t;
typedef unsigned long __ino64_t;
typedef unsigned int __mode_t;
typedef unsigned long __nlink_t;
typedef long __off_t;
typedef long __off64_t;
typedef int __pid_t;
typedef struct {
    int __val[2];
} __fsid_t;
typedef long __clock_t;
typedef unsigned long __rlim_t;
typedef unsigned long __rlim64_t;
typedef unsigned int __id_t;
typedef long __time_t;
typedef unsigned int __useconds_t;
typedef long __suseconds_t;
typedef int __daddr_t;
typedef int __key_t;
typedef int __clockid_t;
typedef void *__timer_t;
typedef long __blksize_t;
typedef long __blkcnt_t;
typedef long __blkcnt64_t;
typedef unsigned long __fsblkcnt_t;
typedef unsigned long __fsblkcnt64_t;
typedef unsigned long __fsfilcnt_t;
typedef unsigned long __fsfilcnt64_t;
typedef long __fsword_t;
typedef long __ssize_t;
typedef long __syscall_slong_t;
typedef unsigned long __syscall_ulong_t;
typedef __off64_t __loff_t;
typedef char *__caddr_t;
typedef long __intptr_t;
typedef unsigned int __socklen_t;
typedef int __sig_atomic_t;
typedef __int8_t int8_t;
typedef __int16_t int16_t;
typedef __int32_t int32_t;
typedef __int64_t int64_t;
typedef __uint8_t uint8_t;
typedef __uint16_t uint16_t;
typedef __uint32_t uint32_t;
typedef __uint64_t uint64_t;
typedef __int_least8_t int_least8_t;
typedef __int_least16_t int_least16_t;
typedef __int_least32_t int_least32_t;
typedef __int_least64_t int_least64_t;
typedef __uint_least8_t uint_least8_t;
typedef __uint_least16_t uint_least16_t;
typedef __uint_least32_t uint_least32_t;
typedef __uint_least64_t uint_least64_t;
typedef signed char int_fast8_t;
typedef long int_fast16_t;
typedef long int_fast32_t;
typedef long int_fast64_t;
typedef unsigned char uint_fast8_t;
typedef unsigned long uint_fast16_t;
typedef unsigned long uint_fast32_t;
typedef unsigned long uint_fast64_t;
typedef long intptr_t;
typedef unsigned long uintptr_t;
typedef __intmax_t intmax_t;
typedef __uintmax_t uintmax_t;
typedef _Bool bool;
struct indirect_placeholder_5_ret_type;
struct indirect_placeholder_8_ret_type;
struct indirect_placeholder_7_ret_type;
struct indirect_placeholder_10_ret_type;
struct indirect_placeholder_14_ret_type;
struct indirect_placeholder_5_ret_type {
    uint64_t field_0;
    uint64_t field_1;
    uint64_t field_2;
};
struct indirect_placeholder_8_ret_type {
    uint64_t field_0;
    uint64_t field_1;
    uint64_t field_2;
};
struct indirect_placeholder_7_ret_type {
    uint64_t field_0;
    uint64_t field_1;
    uint64_t field_2;
};
struct indirect_placeholder_10_ret_type {
    uint64_t field_0;
    uint64_t field_1;
    uint64_t field_2;
};
struct indirect_placeholder_14_ret_type {
    uint64_t field_0;
    uint64_t field_1;
    uint64_t field_2;
    uint64_t field_3;
};
extern void abort(void);
extern uint64_t revng_init_local_sp(uint64_t param_0);
extern uint64_t init_rbx(void);
extern void indirect_placeholder(void);
extern uint64_t init_rbp(void);
extern uint64_t init_r14(void);
extern uint64_t init_r13(void);
extern uint64_t init_r12(void);
extern uint64_t indirect_placeholder_2(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint64_t param_3, uint64_t param_4);
extern uint64_t init_r15(void);
extern uint64_t init_rax(void);
extern uint64_t indirect_placeholder_6(uint64_t param_0, uint64_t param_1);
extern void indirect_placeholder_13(uint64_t param_0, uint64_t param_1, uint64_t param_2);
extern void indirect_placeholder_11(uint64_t param_0, uint64_t param_1);
extern uint64_t indirect_placeholder_9(uint64_t param_0, uint64_t param_1, uint64_t param_2);
extern void indirect_placeholder_3(uint64_t param_0);
extern uint64_t indirect_placeholder_12(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint64_t param_3);
extern void indirect_placeholder_4(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint64_t param_3, uint64_t param_4, uint64_t param_5, uint64_t param_6);
extern struct indirect_placeholder_5_ret_type indirect_placeholder_5(uint64_t param_0, uint64_t param_1);
extern struct indirect_placeholder_8_ret_type indirect_placeholder_8(uint64_t param_0, uint64_t param_1);
extern struct indirect_placeholder_7_ret_type indirect_placeholder_7(uint64_t param_0, uint64_t param_1);
extern struct indirect_placeholder_10_ret_type indirect_placeholder_10(uint64_t param_0);
extern struct indirect_placeholder_14_ret_type indirect_placeholder_14(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint64_t param_3, uint64_t param_4);
void bb_main(uint64_t rdi, uint64_t rsi) {
    uint64_t var_22;
    uint64_t var_23;
    uint64_t var_32;
    struct indirect_placeholder_14_ret_type var_14;
    uint64_t var_0;
    uint64_t var_1;
    uint64_t var_2;
    uint64_t var_3;
    uint64_t var_4;
    uint64_t var_5;
    uint64_t var_6;
    uint64_t var_7;
    uint32_t var_8;
    uint64_t var_9;
    uint64_t var_10;
    uint64_t var_11;
    uint64_t var_12;
    uint64_t local_sp_9_ph;
    uint64_t r14_0_ph;
    uint64_t r12_1_ph213;
    uint64_t r13_1_ph;
    uint64_t r12_1_ph;
    uint64_t local_sp_9_ph211;
    uint64_t local_sp_4;
    uint64_t storemerge2;
    uint64_t var_71;
    uint64_t *var_72;
    struct indirect_placeholder_5_ret_type var_73;
    uint64_t var_74;
    uint64_t var_75;
    uint64_t var_76;
    uint64_t var_77;
    uint64_t var_78;
    uint64_t var_79;
    uint64_t r12_0;
    uint64_t var_62;
    uint64_t var_63;
    uint64_t r8_5;
    uint64_t r9_5;
    uint64_t local_sp_2;
    uint64_t rcx_4;
    uint64_t var_80;
    uint64_t var_81;
    uint64_t var_82;
    uint64_t var_83;
    uint64_t var_84;
    uint64_t var_61;
    bool var_64;
    uint64_t var_65;
    uint64_t var_66;
    uint64_t var_67;
    uint64_t *var_68;
    uint64_t var_69;
    uint64_t var_70;
    uint64_t var_38;
    uint64_t rcx_2;
    uint64_t r8_2;
    uint64_t r9_2;
    uint64_t rcx_5;
    uint64_t local_sp_7;
    uint64_t var_51;
    struct indirect_placeholder_8_ret_type var_52;
    uint64_t var_53;
    uint64_t var_54;
    uint64_t var_55;
    uint64_t var_56;
    uint64_t *var_41;
    uint64_t var_42;
    uint64_t *var_43;
    struct indirect_placeholder_7_ret_type var_44;
    uint64_t var_45;
    uint64_t var_46;
    uint64_t var_47;
    uint64_t var_48;
    uint64_t var_49;
    uint64_t var_50;
    uint64_t var_25;
    uint64_t var_26;
    struct indirect_placeholder_10_ret_type var_27;
    uint64_t var_28;
    uint64_t var_29;
    uint64_t var_30;
    uint64_t var_31;
    uint64_t var_24;
    uint64_t local_sp_5;
    uint64_t rcx_3;
    uint64_t r8_3;
    uint64_t r9_3;
    uint64_t local_sp_6;
    uint64_t r13_0;
    uint64_t r8_4;
    uint64_t r9_4;
    uint64_t var_36;
    bool var_37;
    uint64_t var_39;
    uint64_t *var_40;
    bool var_57;
    uint64_t var_58;
    uint32_t *var_59;
    uint32_t var_60;
    uint64_t var_19;
    uint64_t var_20;
    uint64_t var_21;
    uint64_t var_33;
    uint64_t var_34;
    uint64_t var_35;
    unsigned char storemerge258;
    uint64_t local_sp_8;
    uint64_t var_16;
    uint64_t var_17;
    uint64_t var_18;
    uint64_t local_sp_9;
    uint64_t local_sp_9_ph215;
    uint64_t storemerge257;
    uint64_t r13_1_ph212_be;
    uint64_t r13_1_ph212;
    uint64_t r12_1_ph216;
    uint64_t var_13;
    uint32_t var_15;
    unsigned int loop_state_var;
    bool switch_state_var;
    var_0 = revng_init_local_sp(0UL);
    var_1 = init_rax();
    var_2 = init_rbx();
    var_3 = init_r14();
    var_4 = init_rbp();
    var_5 = init_r13();
    var_6 = init_r12();
    var_7 = init_r15();
    *(uint64_t *)(var_0 + (-8L)) = var_7;
    *(uint64_t *)(var_0 + (-16L)) = var_3;
    *(uint64_t *)(var_0 + (-24L)) = var_5;
    *(uint64_t *)(var_0 + (-32L)) = var_6;
    *(uint64_t *)(var_0 + (-40L)) = var_4;
    *(uint64_t *)(var_0 + (-48L)) = var_2;
    var_8 = (uint32_t)rdi;
    var_9 = (uint64_t)var_8;
    var_10 = *(uint64_t *)rsi;
    *(uint64_t *)(var_0 + (-240L)) = 4211612UL;
    indirect_placeholder_3(var_10);
    *(uint64_t *)(var_0 + (-248L)) = 4211627UL;
    indirect_placeholder();
    *(uint64_t *)(var_0 + (-256L)) = 4211632UL;
    indirect_placeholder();
    var_11 = *(uint64_t *)var_1;
    *(uint64_t *)6404264UL = ((*(unsigned char *)var_11 == '\x00') ? 4289084UL : var_11);
    *(uint64_t *)(var_0 + (-264L)) = 4211659UL;
    indirect_placeholder();
    *(uint64_t *)6404256UL = var_11;
    var_12 = var_0 + (-272L);
    *(uint64_t *)var_12 = 4211676UL;
    indirect_placeholder();
    local_sp_9_ph = var_12;
    r14_0_ph = 0UL;
    r13_1_ph = 0UL;
    r12_1_ph = 0UL;
    rcx_2 = 4209375UL;
    storemerge258 = (unsigned char)'\x00';
    storemerge257 = 4293766UL;
    while (1U)
        {
            r14_0_ph = 1UL;
            local_sp_9_ph211 = local_sp_9_ph;
            r13_1_ph212 = r13_1_ph;
            r12_1_ph213 = r12_1_ph;
            while (1U)
                {
                    r13_1_ph = r13_1_ph212;
                    r13_0 = r13_1_ph212;
                    local_sp_9_ph215 = local_sp_9_ph211;
                    r12_1_ph216 = r12_1_ph213;
                    while (1U)
                        {
                            r12_1_ph = r12_1_ph216;
                            r12_1_ph213 = r12_1_ph216;
                            r12_1_ph216 = 1UL;
                            local_sp_9 = local_sp_9_ph215;
                            while (1U)
                                {
                                    var_13 = local_sp_9 + (-8L);
                                    *(uint64_t *)var_13 = 4211942UL;
                                    var_14 = indirect_placeholder_14(4286983UL, var_9, 4288640UL, 0UL, rsi);
                                    var_15 = (uint32_t)var_14.field_0;
                                    local_sp_9_ph = var_13;
                                    local_sp_9_ph211 = var_13;
                                    local_sp_8 = var_13;
                                    local_sp_9 = var_13;
                                    local_sp_9_ph215 = var_13;
                                    if ((uint64_t)(var_15 + 1U) == 0UL) {
                                        if ((uint64_t)(var_15 + (-99)) != 0UL) {
                                            loop_state_var = 3U;
                                            break;
                                        }
                                        storemerge258 = (unsigned char)'\x01';
                                        storemerge257 = 4293767UL;
                                        if ((int)var_15 <= (int)99U) {
                                            loop_state_var = 4U;
                                            break;
                                        }
                                        if ((uint64_t)(var_15 + 130U) == 0UL) {
                                            *(uint64_t *)(local_sp_9 + (-16L)) = 4211844UL;
                                            indirect_placeholder_13(rsi, var_9, 0UL);
                                            abort();
                                        }
                                        if ((uint64_t)(var_15 + (-76)) == 0UL) {
                                            *(unsigned char *)6404273UL = (unsigned char)'\x01';
                                            continue;
                                        }
                                        if ((uint64_t)(var_15 + 131U) != 0UL) {
                                            loop_state_var = 2U;
                                            break;
                                        }
                                        var_16 = *(uint64_t *)6403816UL;
                                        var_17 = *(uint64_t *)6403968UL;
                                        *(uint64_t *)(local_sp_9 + (-16L)) = 4211890UL;
                                        indirect_placeholder_4(0UL, 4286927UL, var_17, var_16, 4286968UL, 4286827UL, 0UL);
                                        var_18 = local_sp_9 + (-24L);
                                        *(uint64_t *)var_18 = 4211900UL;
                                        indirect_placeholder();
                                        local_sp_8 = var_18;
                                        loop_state_var = 2U;
                                        break;
                                    }
                                    var_19 = var_14.field_1;
                                    var_20 = var_14.field_2;
                                    var_21 = var_14.field_3;
                                    rcx_4 = var_19;
                                    rcx_3 = var_19;
                                    r8_3 = var_20;
                                    r9_3 = var_21;
                                    r8_4 = var_20;
                                    r9_4 = var_21;
                                    if ((uint64_t)(var_8 - *(uint32_t *)6403932UL) == 0UL) {
                                        *(uint64_t *)(local_sp_9 + (-16L)) = 4211987UL;
                                        indirect_placeholder_4(0UL, 4286989UL, 0UL, var_19, var_20, 0UL, var_21);
                                        *(uint64_t *)(local_sp_9 + (-24L)) = 4211997UL;
                                        indirect_placeholder_13(rsi, var_9, 1UL);
                                        abort();
                                    }
                                    if (r13_1_ph212 == 0UL) {
                                        var_32 = (uint64_t)(unsigned char)r12_1_ph216;
                                        *(uint64_t *)(local_sp_9 + (-16L)) = 4212191UL;
                                        var_33 = indirect_placeholder_9(0UL, r14_0_ph, var_32);
                                        var_34 = local_sp_9 + (-24L);
                                        *(uint64_t *)var_34 = 4212210UL;
                                        var_35 = indirect_placeholder_9(1UL, r14_0_ph, var_32);
                                        *(uint64_t *)local_sp_9 = var_35;
                                        local_sp_6 = var_34;
                                        r13_0 = var_33;
                                        loop_state_var = 0U;
                                        break;
                                    }
                                    var_22 = local_sp_9 + (-16L);
                                    *(uint64_t *)var_22 = 4212016UL;
                                    var_23 = indirect_placeholder_6(r13_1_ph212, 4287005UL);
                                    local_sp_5 = var_22;
                                    if (var_23 != 0UL) {
                                        loop_state_var = 1U;
                                        break;
                                    }
                                    *(uint64_t *)(local_sp_9 + (-24L)) = 4212035UL;
                                    indirect_placeholder();
                                    *(uint64_t *)(local_sp_9 + (-32L)) = 4212066UL;
                                    var_24 = indirect_placeholder_12(4292192UL, var_23, 4UL, 4292288UL);
                                    rcx_3 = 4UL;
                                    if ((int)(uint32_t)var_24 >= (int)0U) {
                                        var_25 = (uint64_t)*(uint32_t *)((uint64_t)((long)(var_24 << 32UL) >> (long)30UL) + 4292192UL);
                                        var_26 = local_sp_9 + (-40L);
                                        *(uint64_t *)var_26 = 4212090UL;
                                        indirect_placeholder_11(0UL, var_25);
                                        local_sp_5 = var_26;
                                        loop_state_var = 1U;
                                        break;
                                    }
                                    *(uint64_t *)(local_sp_9 + (-40L)) = 4212107UL;
                                    indirect_placeholder_11(0UL, 4UL);
                                    *(uint64_t *)(local_sp_9 + (-48L)) = 4212115UL;
                                    var_27 = indirect_placeholder_10(var_23);
                                    var_28 = var_27.field_0;
                                    var_29 = var_27.field_1;
                                    var_30 = var_27.field_2;
                                    var_31 = local_sp_9 + (-56L);
                                    *(uint64_t *)var_31 = 4212143UL;
                                    indirect_placeholder_4(0UL, 4285712UL, 0UL, var_28, var_29, 0UL, var_30);
                                    local_sp_5 = var_31;
                                    rcx_3 = var_28;
                                    r8_3 = var_29;
                                    r9_3 = var_30;
                                    loop_state_var = 1U;
                                    break;
                                }
                            switch_state_var = 0;
                            switch (loop_state_var) {
                              case 4U:
                                {
                                    if ((uint64_t)(var_15 + (-116)) == 0UL) {
                                        continue;
                                    }
                                    loop_state_var = 3U;
                                    switch_state_var = 1;
                                    break;
                                }
                                break;
                              case 1U:
                                {
                                    *(uint64_t *)(local_sp_5 + 24UL) = r13_1_ph212;
                                    local_sp_6 = local_sp_5;
                                    rcx_4 = rcx_3;
                                    r8_4 = r8_3;
                                    r9_4 = r9_3;
                                    loop_state_var = 0U;
                                    switch_state_var = 1;
                                    break;
                                }
                                break;
                              case 0U:
                                {
                                    loop_state_var = 0U;
                                    switch_state_var = 1;
                                    break;
                                }
                                break;
                              case 2U:
                                {
                                    loop_state_var = 1U;
                                    switch_state_var = 1;
                                    break;
                                }
                                break;
                              case 3U:
                                {
                                    loop_state_var = 2U;
                                    switch_state_var = 1;
                                    break;
                                }
                                break;
                            }
                            if (switch_state_var)
                                break;
                        }
                    switch_state_var = 0;
                    switch (loop_state_var) {
                      case 2U:
                        {
                            r13_1_ph212_be = *(uint64_t *)6406944UL;
                            *(unsigned char *)6404272UL = storemerge258;
                            *(uint64_t *)6403808UL = storemerge257;
                            r13_1_ph212 = r13_1_ph212_be;
                            continue;
                        }
                        break;
                      case 3U:
                        {
                            if ((uint64_t)(var_15 + (-128)) == 0UL) {
                                loop_state_var = 2U;
                                switch_state_var = 1;
                                break;
                            }
                            r13_1_ph212_be = *(uint64_t *)6406944UL;
                            *(unsigned char *)6404272UL = storemerge258;
                            *(uint64_t *)6403808UL = storemerge257;
                            r13_1_ph212 = r13_1_ph212_be;
                            continue;
                        }
                        break;
                      case 0U:
                        {
                            var_36 = rdi << 32UL;
                            var_37 = (r14_0_ph == 0UL);
                            r12_0 = (uint64_t)*(uint32_t *)6403932UL;
                            r8_5 = r8_4;
                            r9_5 = r9_4;
                            rcx_5 = rcx_4;
                            local_sp_7 = local_sp_6;
                            loop_state_var = 0U;
                            switch_state_var = 1;
                            break;
                        }
                        break;
                      case 1U:
                        {
                            loop_state_var = 1U;
                            switch_state_var = 1;
                            break;
                        }
                        break;
                    }
                    if (switch_state_var)
                        break;
                }
            switch_state_var = 0;
            switch (loop_state_var) {
              case 2U:
                {
                    if ((uint64_t)(var_15 + (-102)) == 0UL) {
                        continue;
                    }
                    loop_state_var = 1U;
                    switch_state_var = 1;
                    break;
                }
                break;
              case 0U:
                {
                    loop_state_var = 0U;
                    switch_state_var = 1;
                    break;
                }
                break;
              case 1U:
                {
                    loop_state_var = 1U;
                    switch_state_var = 1;
                    break;
                }
                break;
            }
            if (switch_state_var)
                break;
        }
    switch (loop_state_var) {
      case 1U:
        {
            *(uint64_t *)(local_sp_8 + (-8L)) = 4211910UL;
            indirect_placeholder_13(rsi, var_9, 1UL);
            abort();
        }
        break;
      case 0U:
        {
            var_38 = r12_0 << 32UL;
            r8_2 = r8_5;
            r9_2 = r9_5;
            while ((long)var_38 >= (long)var_36)
                {
                    var_39 = *(uint64_t *)((uint64_t)((long)var_38 >> (long)29UL) + rsi);
                    *(uint64_t *)(local_sp_7 + 8UL) = var_39;
                    var_40 = (uint64_t *)(local_sp_7 + (-8L));
                    if (!var_37) {
                        *var_40 = 4212267UL;
                        indirect_placeholder();
                        if ((uint64_t)(uint32_t)var_39 == 0UL) {
                            var_51 = *(uint64_t *)local_sp_7;
                            *(uint64_t *)(local_sp_7 + (-16L)) = 4212286UL;
                            var_52 = indirect_placeholder_8(4UL, var_51);
                            var_53 = var_52.field_0;
                            var_54 = var_52.field_1;
                            var_55 = var_52.field_2;
                            var_56 = local_sp_7 + (-24L);
                            *(uint64_t *)var_56 = 4212314UL;
                            indirect_placeholder_4(0UL, 4285784UL, 0UL, var_53, var_54, 0UL, var_55);
                            local_sp_4 = var_56;
                            rcx_2 = var_53;
                            r8_2 = var_54;
                            r9_2 = var_55;
                        } else {
                            var_41 = (uint64_t *)(local_sp_7 + (-16L));
                            *var_41 = 4212336UL;
                            indirect_placeholder();
                            var_42 = *var_40;
                            var_43 = (uint64_t *)(local_sp_7 + (-24L));
                            *var_43 = 4212355UL;
                            var_44 = indirect_placeholder_7(4UL, var_42);
                            var_45 = var_44.field_0;
                            var_46 = var_44.field_1;
                            var_47 = var_44.field_2;
                            *var_41 = var_45;
                            *(uint64_t *)(local_sp_7 + (-32L)) = 4212365UL;
                            indirect_placeholder();
                            var_48 = *var_43;
                            var_49 = (uint64_t)*(uint32_t *)var_45;
                            var_50 = local_sp_7 + (-40L);
                            *(uint64_t *)var_50 = 4212392UL;
                            indirect_placeholder_4(0UL, 4285856UL, 0UL, var_48, var_46, var_49, var_47);
                            local_sp_4 = var_50;
                            rcx_2 = var_48;
                            r8_2 = var_46;
                            r9_2 = var_47;
                        }
                        r12_0 = (uint64_t)((uint32_t)r12_0 + 1U);
                        r8_5 = r8_2;
                        r9_5 = r9_2;
                        rcx_5 = rcx_2;
                        local_sp_7 = local_sp_4;
                        var_38 = r12_0 << 32UL;
                        r8_2 = r8_5;
                        r9_2 = r9_5;
                        continue;
                    }
                    *var_40 = 4212463UL;
                    indirect_placeholder();
                    var_57 = ((uint64_t)(uint32_t)var_39 == 0UL);
                    var_58 = var_57 ? 0UL : 4294967295UL;
                    var_59 = (uint32_t *)(local_sp_7 + 12UL);
                    var_60 = (uint32_t)var_58;
                    *var_59 = var_60;
                    if ((int)var_60 < (int)0U) {
                        var_64 = (*(unsigned char *)6404273UL == '\x00');
                        var_65 = local_sp_7 + 24UL;
                        var_66 = *(uint64_t *)local_sp_7;
                        var_67 = local_sp_7 + (-16L);
                        var_68 = (uint64_t *)var_67;
                        local_sp_2 = var_67;
                        if (var_64) {
                            *var_68 = 4212584UL;
                            var_70 = indirect_placeholder_6(var_66, var_65);
                            storemerge2 = (var_70 & (-256L)) | ((uint64_t)(uint32_t)var_70 != 0UL);
                        } else {
                            *var_68 = 4212562UL;
                            var_69 = indirect_placeholder_6(var_66, var_65);
                            storemerge2 = (var_69 & (-256L)) | ((uint64_t)(uint32_t)var_69 != 0UL);
                        }
                        if ((uint64_t)(unsigned char)storemerge2 != 0UL) {
                            var_71 = *(uint64_t *)(var_67 + 8UL);
                            var_72 = (uint64_t *)(var_67 + (-8L));
                            *var_72 = 4212608UL;
                            var_73 = indirect_placeholder_5(4UL, var_71);
                            var_74 = var_73.field_0;
                            var_75 = var_73.field_1;
                            var_76 = var_73.field_2;
                            *var_68 = var_74;
                            *(uint64_t *)(var_67 + (-16L)) = 4212618UL;
                            indirect_placeholder();
                            var_77 = *var_72;
                            var_78 = (uint64_t)*(uint32_t *)var_74;
                            var_79 = var_67 + (-24L);
                            *(uint64_t *)var_79 = 4212645UL;
                            indirect_placeholder_4(0UL, 4287049UL, 0UL, var_77, var_75, var_78, var_76);
                            local_sp_4 = var_79;
                            rcx_2 = var_77;
                            r8_2 = var_75;
                            r9_2 = var_76;
                            r12_0 = (uint64_t)((uint32_t)r12_0 + 1U);
                            r8_5 = r8_2;
                            r9_5 = r9_2;
                            rcx_5 = rcx_2;
                            local_sp_7 = local_sp_4;
                            var_38 = r12_0 << 32UL;
                            r8_2 = r8_5;
                            r9_2 = r9_5;
                            continue;
                        }
                    }
                    var_61 = local_sp_7 + (-16L);
                    *(uint64_t *)var_61 = 4212493UL;
                    indirect_placeholder();
                    local_sp_2 = var_61;
                    rcx_2 = rcx_5;
                    if (!var_57) {
                        *(uint64_t *)(local_sp_7 + (-24L)) = 4212506UL;
                        indirect_placeholder();
                        var_62 = (uint64_t)*(uint32_t *)var_58;
                        var_63 = local_sp_7 + (-32L);
                        *(uint64_t *)var_63 = 4212528UL;
                        indirect_placeholder_4(0UL, 4287022UL, 0UL, rcx_5, r8_5, var_62, r9_5);
                        local_sp_4 = var_63;
                        r12_0 = (uint64_t)((uint32_t)r12_0 + 1U);
                        r8_5 = r8_2;
                        r9_5 = r9_2;
                        rcx_5 = rcx_2;
                        local_sp_7 = local_sp_4;
                        var_38 = r12_0 << 32UL;
                        r8_2 = r8_5;
                        r9_2 = r9_5;
                        continue;
                    }
                    var_80 = ((uint32_t)((uint16_t)*(uint32_t *)(local_sp_2 + 56UL) & (unsigned short)45056U) == 8192U) ? *(uint64_t *)(local_sp_2 + 24UL) : r13_0;
                    var_81 = local_sp_2 + 32UL;
                    var_82 = *(uint64_t *)(local_sp_2 + 8UL);
                    var_83 = (uint64_t)*(uint32_t *)(local_sp_2 + 20UL);
                    var_84 = local_sp_2 + (-8L);
                    *(uint64_t *)var_84 = 4212699UL;
                    indirect_placeholder_2(var_82, var_80, 4209375UL, var_81, var_83);
                    local_sp_4 = var_84;
                    r8_2 = var_81;
                }
            return;
        }
        break;
    }
}

typedef unsigned char __u_char;
typedef unsigned short __u_short;
typedef unsigned int __u_int;
typedef unsigned long __u_long;
typedef signed char __int8_t;
typedef unsigned char __uint8_t;
typedef short __int16_t;
typedef unsigned short __uint16_t;
typedef int __int32_t;
typedef unsigned int __uint32_t;
typedef long __int64_t;
typedef unsigned long __uint64_t;
typedef __int8_t __int_least8_t;
typedef __uint8_t __uint_least8_t;
typedef __int16_t __int_least16_t;
typedef __uint16_t __uint_least16_t;
typedef __int32_t __int_least32_t;
typedef __uint32_t __uint_least32_t;
typedef __int64_t __int_least64_t;
typedef __uint64_t __uint_least64_t;
typedef long __quad_t;
typedef unsigned long __u_quad_t;
typedef long __intmax_t;
typedef unsigned long __uintmax_t;
typedef unsigned long __dev_t;
typedef unsigned int __uid_t;
typedef unsigned int __gid_t;
typedef unsigned long __ino_t;
typedef unsigned long __ino64_t;
typedef unsigned int __mode_t;
typedef unsigned long __nlink_t;
typedef long __off_t;
typedef long __off64_t;
typedef int __pid_t;
typedef struct {
    int __val[2];
} __fsid_t;
typedef long __clock_t;
typedef unsigned long __rlim_t;
typedef unsigned long __rlim64_t;
typedef unsigned int __id_t;
typedef long __time_t;
typedef unsigned int __useconds_t;
typedef long __suseconds_t;
typedef int __daddr_t;
typedef int __key_t;
typedef int __clockid_t;
typedef void *__timer_t;
typedef long __blksize_t;
typedef long __blkcnt_t;
typedef long __blkcnt64_t;
typedef unsigned long __fsblkcnt_t;
typedef unsigned long __fsblkcnt64_t;
typedef unsigned long __fsfilcnt_t;
typedef unsigned long __fsfilcnt64_t;
typedef long __fsword_t;
typedef long __ssize_t;
typedef long __syscall_slong_t;
typedef unsigned long __syscall_ulong_t;
typedef __off64_t __loff_t;
typedef char *__caddr_t;
typedef long __intptr_t;
typedef unsigned int __socklen_t;
typedef int __sig_atomic_t;
typedef __int8_t int8_t;
typedef __int16_t int16_t;
typedef __int32_t int32_t;
typedef __int64_t int64_t;
typedef __uint8_t uint8_t;
typedef __uint16_t uint16_t;
typedef __uint32_t uint32_t;
typedef __uint64_t uint64_t;
typedef __int_least8_t int_least8_t;
typedef __int_least16_t int_least16_t;
typedef __int_least32_t int_least32_t;
typedef __int_least64_t int_least64_t;
typedef __uint_least8_t uint_least8_t;
typedef __uint_least16_t uint_least16_t;
typedef __uint_least32_t uint_least32_t;
typedef __uint_least64_t uint_least64_t;
typedef signed char int_fast8_t;
typedef long int_fast16_t;
typedef long int_fast32_t;
typedef long int_fast64_t;
typedef unsigned char uint_fast8_t;
typedef unsigned long uint_fast16_t;
typedef unsigned long uint_fast32_t;
typedef unsigned long uint_fast64_t;
typedef long intptr_t;
typedef unsigned long uintptr_t;
typedef __intmax_t intmax_t;
typedef __uintmax_t uintmax_t;
typedef _Bool bool;
struct indirect_placeholder_21_ret_type;
struct indirect_placeholder_22_ret_type;
struct indirect_placeholder_23_ret_type;
struct indirect_placeholder_24_ret_type;
struct indirect_placeholder_26_ret_type;
struct indirect_placeholder_27_ret_type;
struct indirect_placeholder_28_ret_type;
struct indirect_placeholder_21_ret_type {
    uint64_t field_0;
    uint64_t field_1;
    uint64_t field_2;
};
struct indirect_placeholder_22_ret_type {
    uint64_t field_0;
    uint64_t field_1;
    uint64_t field_2;
};
struct indirect_placeholder_23_ret_type {
    uint64_t field_0;
    uint64_t field_1;
    uint64_t field_2;
};
struct indirect_placeholder_24_ret_type {
    uint64_t field_0;
    uint64_t field_1;
    uint64_t field_2;
};
struct indirect_placeholder_26_ret_type {
    uint64_t field_0;
    uint64_t field_1;
};
struct indirect_placeholder_27_ret_type {
    uint64_t field_0;
    uint64_t field_1;
};
struct indirect_placeholder_28_ret_type {
    uint64_t field_0;
    uint64_t field_1;
};
extern void abort(void);
extern void function_dispatcher(unsigned char *param_0);
extern uint64_t revng_init_local_sp(uint64_t param_0);
extern uint64_t init_rbx(void);
extern void indirect_placeholder(void);
extern uint64_t init_rbp(void);
extern uint64_t init_r14(void);
extern uint64_t init_r13(void);
extern uint64_t init_r12(void);
extern uint64_t indirect_placeholder_1(uint64_t param_0);
extern uint64_t indirect_placeholder_2(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint64_t param_3, uint64_t param_4);
extern uint64_t indirect_placeholder_13(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint64_t param_3, uint64_t param_4, uint64_t param_5);
extern void indirect_placeholder_5(uint64_t param_0);
extern uint64_t init_r9(void);
extern uint64_t indirect_placeholder_7(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint64_t param_3, uint64_t param_4, uint64_t param_5, uint64_t param_6);
extern void indirect_placeholder_17(uint64_t param_0, uint64_t param_1, uint64_t param_2);
extern struct indirect_placeholder_21_ret_type indirect_placeholder_21(uint64_t param_0);
extern struct indirect_placeholder_22_ret_type indirect_placeholder_22(uint64_t param_0);
extern struct indirect_placeholder_23_ret_type indirect_placeholder_23(uint64_t param_0);
extern struct indirect_placeholder_24_ret_type indirect_placeholder_24(uint64_t param_0);
extern void indirect_placeholder_25(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint64_t param_3, uint64_t param_4, uint64_t param_5, uint64_t param_6);
extern struct indirect_placeholder_26_ret_type indirect_placeholder_26(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint64_t param_3);
extern struct indirect_placeholder_27_ret_type indirect_placeholder_27(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint64_t param_3);
extern struct indirect_placeholder_28_ret_type indirect_placeholder_28(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint64_t param_3);
void bb_main(uint64_t rdi, uint64_t rsi) {
    uint64_t var_12;
    uint64_t var_0;
    uint64_t var_1;
    uint64_t var_2;
    uint64_t var_3;
    uint64_t var_4;
    uint64_t var_5;
    uint64_t var_6;
    uint32_t var_7;
    uint64_t var_8;
    uint64_t var_9;
    uint64_t var_10;
    uint64_t local_sp_4;
    uint64_t var_90;
    uint64_t r12_2;
    uint64_t local_sp_2;
    uint64_t var_81;
    uint64_t rbx_0;
    uint64_t local_sp_0;
    uint64_t var_61;
    uint64_t var_88;
    uint64_t var_89;
    uint64_t rdx_0;
    uint64_t r9_1;
    uint64_t r12_1;
    uint64_t var_82;
    uint64_t var_83;
    uint64_t var_84;
    uint64_t var_85;
    uint32_t var_86;
    uint64_t var_87;
    bool var_67;
    uint64_t *var_68;
    uint64_t var_69;
    uint64_t var_70;
    uint64_t var_71;
    uint64_t var_72;
    uint64_t var_73;
    uint64_t var_74;
    uint64_t var_75;
    uint64_t var_76;
    uint64_t var_77;
    uint64_t var_78;
    uint32_t var_79;
    uint64_t var_80;
    uint32_t var_14;
    uint64_t var_59;
    uint64_t local_sp_4_be;
    uint64_t rbx_0_be;
    uint64_t r12_2_be;
    uint64_t r9_1_be;
    uint64_t var_49;
    struct indirect_placeholder_21_ret_type var_50;
    uint64_t var_51;
    uint64_t var_52;
    uint64_t var_53;
    uint64_t var_54;
    uint64_t var_32;
    struct indirect_placeholder_22_ret_type var_33;
    uint64_t var_34;
    uint64_t var_35;
    uint64_t var_36;
    uint64_t var_37;
    uint64_t var_42;
    struct indirect_placeholder_23_ret_type var_43;
    uint64_t var_44;
    uint64_t var_45;
    uint64_t var_46;
    uint64_t var_47;
    uint64_t var_22;
    struct indirect_placeholder_24_ret_type var_23;
    uint64_t var_24;
    uint64_t var_25;
    uint64_t var_26;
    uint64_t var_27;
    uint64_t var_15;
    uint64_t var_16;
    uint64_t var_17;
    uint64_t var_18;
    struct indirect_placeholder_26_ret_type var_19;
    uint64_t var_20;
    uint64_t var_21;
    uint64_t var_28;
    struct indirect_placeholder_27_ret_type var_29;
    uint64_t var_30;
    uint64_t var_31;
    uint64_t var_38;
    struct indirect_placeholder_28_ret_type var_39;
    uint64_t var_40;
    uint64_t var_41;
    uint64_t var_48;
    uint64_t var_55;
    uint64_t var_56;
    uint64_t var_57;
    uint64_t var_58;
    uint64_t var_60;
    uint64_t var_62;
    uint64_t var_63;
    uint64_t var_64;
    uint64_t var_65;
    uint64_t var_66;
    uint64_t var_11;
    uint64_t var_13;
    unsigned int loop_state_var;
    var_0 = revng_init_local_sp(0UL);
    var_1 = init_rbp();
    var_2 = init_rbx();
    var_3 = init_r14();
    var_4 = init_r13();
    var_5 = init_r12();
    var_6 = init_r9();
    *(uint64_t *)(var_0 + (-8L)) = var_3;
    *(uint64_t *)(var_0 + (-16L)) = var_4;
    *(uint64_t *)(var_0 + (-24L)) = var_5;
    *(uint64_t *)(var_0 + (-32L)) = var_1;
    *(uint64_t *)(var_0 + (-40L)) = var_2;
    var_7 = (uint32_t)rdi;
    var_8 = (uint64_t)var_7;
    var_9 = *(uint64_t *)rsi;
    *(uint64_t *)(var_0 + (-48L)) = 4204884UL;
    indirect_placeholder_5(var_9);
    *(uint64_t *)(var_0 + (-56L)) = 4204899UL;
    indirect_placeholder();
    var_10 = var_0 + (-64L);
    *(uint64_t *)var_10 = 4204909UL;
    indirect_placeholder();
    *(unsigned char *)6428232UL = (unsigned char)'\x00';
    local_sp_4 = var_10;
    rbx_0 = rsi;
    r12_2 = 1UL;
    r9_1 = var_6;
    while (1U)
        {
            var_11 = local_sp_4 + (-8L);
            *(uint64_t *)var_11 = 4205838UL;
            var_12 = indirect_placeholder_2(var_8, rbx_0, 4303682UL, 4306176UL, 0UL);
            var_13 = var_12 + 1UL;
            r12_1 = r12_2;
            local_sp_4_be = var_11;
            rbx_0_be = rbx_0;
            r12_2_be = r12_2;
            r9_1_be = r9_1;
            if ((uint64_t)(uint32_t)var_13 != 0UL) {
                var_67 = ((uint64_t)(unsigned char)r12_2 == 0UL);
                var_68 = (uint64_t *)(local_sp_4 + (-16L));
                if (var_67) {
                    *var_68 = 4205862UL;
                    indirect_placeholder_17(1UL, var_8, rbx_0);
                    abort();
                }
                *var_68 = 4205874UL;
                indirect_placeholder();
                var_69 = var_12 * 3UL;
                *(uint64_t *)6428312UL = var_69;
                var_70 = var_69 + 1UL;
                *(uint64_t *)(local_sp_4 + (-24L)) = 4205897UL;
                var_71 = indirect_placeholder_1(var_70);
                *(uint64_t *)6428320UL = var_71;
                *(uint64_t *)(local_sp_4 + (-32L)) = 4205922UL;
                indirect_placeholder();
                *(uint64_t *)(local_sp_4 + (-40L)) = 4205933UL;
                indirect_placeholder();
                *(uint64_t *)(local_sp_4 + (-48L)) = 4205944UL;
                indirect_placeholder();
                var_72 = var_12 << 1UL;
                *(uint64_t *)6428296UL = var_72;
                var_73 = var_72 | 1UL;
                *(uint64_t *)(local_sp_4 + (-56L)) = 4205965UL;
                var_74 = indirect_placeholder_1(var_73);
                *(uint64_t *)6428304UL = var_74;
                *(uint64_t *)(local_sp_4 + (-64L)) = 4205990UL;
                indirect_placeholder();
                *(uint64_t *)(local_sp_4 + (-72L)) = 4206001UL;
                indirect_placeholder();
                *(uint64_t *)6428280UL = var_12;
                *(uint64_t *)(local_sp_4 + (-80L)) = 4206017UL;
                var_75 = indirect_placeholder_1(var_13);
                *(uint64_t *)6428288UL = var_75;
                *(uint64_t *)(local_sp_4 + (-88L)) = 4206039UL;
                indirect_placeholder();
                *(uint64_t *)(local_sp_4 + (-96L)) = 4206049UL;
                indirect_placeholder();
                *(uint64_t *)(local_sp_4 + (-104L)) = 4206061UL;
                indirect_placeholder();
                var_76 = (var_75 + (uint64_t)*(uint32_t *)6427784UL) + 1UL;
                *(uint64_t *)(local_sp_4 + (-112L)) = 4206081UL;
                var_77 = indirect_placeholder_1(var_76);
                *(uint64_t *)6428248UL = var_77;
                var_78 = local_sp_4 + (-120L);
                *(uint64_t *)var_78 = 4206111UL;
                indirect_placeholder();
                *(unsigned char *)((var_75 + *(uint64_t *)6428248UL) + (uint64_t)*(uint32_t *)6427784UL) = (unsigned char)'\x00';
                *(uint64_t *)6428240UL = *(uint64_t *)6427816UL;
                *(uint64_t *)6429312UL = *(uint64_t *)6427856UL;
                *(uint64_t *)6428328UL = 6429248UL;
                var_79 = *(uint32_t *)6427996UL;
                var_80 = (uint64_t)var_79;
                local_sp_0 = var_78;
                local_sp_2 = var_78;
                rdx_0 = var_80;
                if ((uint64_t)(var_79 - var_7) != 0UL) {
                    var_81 = rdi << 32UL;
                    if ((long)var_81 <= (long)(var_80 << 32UL)) {
                        loop_state_var = 1U;
                        break;
                    }
                    loop_state_var = 0U;
                    break;
                }
                var_87 = local_sp_4 + (-128L);
                *(uint64_t *)var_87 = 4206196UL;
                indirect_placeholder_1(4310826UL);
                local_sp_0 = var_87;
                loop_state_var = 1U;
                break;
            }
            var_14 = (uint32_t)var_12;
            if ((uint64_t)(var_14 + (-105)) == 0UL) {
                var_64 = *(uint64_t *)6429784UL;
                var_65 = local_sp_4 + (-16L);
                *(uint64_t *)var_65 = 4205417UL;
                var_66 = indirect_placeholder_13(var_64, 1UL, 9223372036854775807UL, 4303894UL, 0UL, 4303613UL);
                *(uint64_t *)6427808UL = var_66;
                local_sp_4_be = var_65;
                r9_1_be = 0UL;
            } else {
                r12_2_be = 0UL;
                if ((int)var_14 > (int)105U) {
                    r9_1_be = 0UL;
                    if ((uint64_t)(var_14 + (-112)) == 0UL) {
                        *(unsigned char *)6427800UL = (unsigned char)'\x00';
                    } else {
                        if ((int)var_14 > (int)112U) {
                            if ((uint64_t)(var_14 + (-118)) == 0UL) {
                                var_61 = *(uint64_t *)6429784UL;
                                var_62 = local_sp_4 + (-16L);
                                *(uint64_t *)var_62 = 4205368UL;
                                var_63 = indirect_placeholder_13(var_61, 9223372036854775808UL, 9223372036854775807UL, 4303894UL, 0UL, 4303584UL);
                                *(uint64_t *)6427816UL = var_63;
                                local_sp_4_be = var_62;
                            } else {
                                if ((uint64_t)(var_14 + (-119)) == 0UL) {
                                    var_58 = *(uint64_t *)6429784UL;
                                    var_59 = local_sp_4 + (-16L);
                                    *(uint64_t *)var_59 = 4205548UL;
                                    var_60 = indirect_placeholder_13(var_58, 1UL, 2147483647UL, 4303894UL, 0UL, 4306048UL);
                                    *(uint32_t *)6427784UL = (uint32_t)var_60;
                                    local_sp_4_be = var_59;
                                } else {
                                    if ((uint64_t)(var_14 + (-115)) == 0UL) {
                                        *(uint64_t *)6427832UL = *(uint64_t *)6429784UL;
                                        r12_2_be = r12_2;
                                    }
                                }
                            }
                        } else {
                            if ((uint64_t)(var_14 + (-108)) == 0UL) {
                                var_55 = *(uint64_t *)6429784UL;
                                var_56 = local_sp_4 + (-16L);
                                *(uint64_t *)var_56 = 4205478UL;
                                var_57 = indirect_placeholder_13(var_55, 1UL, 9223372036854775807UL, 4303894UL, 0UL, 4306008UL);
                                *(uint64_t *)6427792UL = var_57;
                                local_sp_4_be = var_56;
                            } else {
                                if ((uint64_t)(var_14 + (-110)) != 0UL) {
                                    var_48 = local_sp_4 + (-16L);
                                    *(uint64_t *)var_48 = 4205576UL;
                                    indirect_placeholder();
                                    local_sp_4_be = var_48;
                                    r12_2_be = r12_2;
                                    if ((uint64_t)var_14 == 0UL) {
                                        *(uint64_t *)6427776UL = 4306627UL;
                                    } else {
                                        *(uint64_t *)(local_sp_4 + (-24L)) = 4205613UL;
                                        indirect_placeholder();
                                        *(uint64_t *)(local_sp_4 + (-32L)) = 4205650UL;
                                        indirect_placeholder();
                                        var_49 = *(uint64_t *)6429784UL;
                                        *(uint64_t *)(local_sp_4 + (-40L)) = 4205682UL;
                                        var_50 = indirect_placeholder_21(var_49);
                                        var_51 = var_50.field_0;
                                        var_52 = var_50.field_1;
                                        var_53 = var_50.field_2;
                                        var_54 = local_sp_4 + (-48L);
                                        *(uint64_t *)var_54 = 4205710UL;
                                        indirect_placeholder_7(0UL, 0UL, 0UL, 4306080UL, var_51, var_52, var_53);
                                        local_sp_4_be = var_54;
                                        r12_2_be = 0UL;
                                        r9_1_be = var_52;
                                    }
                                }
                            }
                        }
                    }
                } else {
                    if ((uint64_t)(var_14 + (-98)) == 0UL) {
                        var_38 = local_sp_4 + (-16L);
                        *(uint64_t *)var_38 = 4205188UL;
                        var_39 = indirect_placeholder_28(6427856UL, 6429248UL, 6428864UL, rbx_0);
                        var_40 = var_39.field_0;
                        var_41 = var_39.field_1;
                        local_sp_4_be = var_38;
                        rbx_0_be = var_41;
                        if ((uint64_t)(unsigned char)var_40 == 0UL) {
                            var_42 = *(uint64_t *)6429784UL;
                            *(uint64_t *)(local_sp_4 + (-24L)) = 4205208UL;
                            var_43 = indirect_placeholder_23(var_42);
                            var_44 = var_43.field_0;
                            var_45 = var_43.field_1;
                            var_46 = var_43.field_2;
                            var_47 = local_sp_4 + (-32L);
                            *(uint64_t *)var_47 = 4205236UL;
                            indirect_placeholder_7(0UL, 0UL, 0UL, 4305928UL, var_44, var_45, var_46);
                            local_sp_4_be = var_47;
                            r12_2_be = 0UL;
                            r9_1_be = var_45;
                        }
                    } else {
                        r9_1_be = 4303652UL;
                        if ((int)var_14 > (int)98U) {
                            if ((uint64_t)(var_14 + (-102)) == 0UL) {
                                var_28 = local_sp_4 + (-16L);
                                *(uint64_t *)var_28 = 4205267UL;
                                var_29 = indirect_placeholder_27(6427840UL, 6429120UL, 6428352UL, rbx_0);
                                var_30 = var_29.field_0;
                                var_31 = var_29.field_1;
                                local_sp_4_be = var_28;
                                rbx_0_be = var_31;
                                if ((uint64_t)(unsigned char)var_30 == 0UL) {
                                    var_32 = *(uint64_t *)6429784UL;
                                    *(uint64_t *)(local_sp_4 + (-24L)) = 4205287UL;
                                    var_33 = indirect_placeholder_22(var_32);
                                    var_34 = var_33.field_0;
                                    var_35 = var_33.field_1;
                                    var_36 = var_33.field_2;
                                    var_37 = local_sp_4 + (-32L);
                                    *(uint64_t *)var_37 = 4205315UL;
                                    indirect_placeholder_7(0UL, 0UL, 0UL, 4305968UL, var_34, var_35, var_36);
                                    local_sp_4_be = var_37;
                                    r12_2_be = 0UL;
                                    r9_1_be = var_35;
                                }
                            } else {
                                if ((uint64_t)(var_14 + (-104)) == 0UL) {
                                    var_18 = local_sp_4 + (-16L);
                                    *(uint64_t *)var_18 = 4205109UL;
                                    var_19 = indirect_placeholder_26(6427848UL, 6429184UL, 6428608UL, rbx_0);
                                    var_20 = var_19.field_0;
                                    var_21 = var_19.field_1;
                                    local_sp_4_be = var_18;
                                    rbx_0_be = var_21;
                                    if ((uint64_t)(unsigned char)var_20 == 0UL) {
                                        var_22 = *(uint64_t *)6429784UL;
                                        *(uint64_t *)(local_sp_4 + (-24L)) = 4205129UL;
                                        var_23 = indirect_placeholder_24(var_22);
                                        var_24 = var_23.field_0;
                                        var_25 = var_23.field_1;
                                        var_26 = var_23.field_2;
                                        var_27 = local_sp_4 + (-32L);
                                        *(uint64_t *)var_27 = 4205157UL;
                                        indirect_placeholder_7(0UL, 0UL, 0UL, 4305888UL, var_24, var_25, var_26);
                                        local_sp_4_be = var_27;
                                        r12_2_be = 0UL;
                                        r9_1_be = var_25;
                                    }
                                } else {
                                    if ((uint64_t)(var_14 + (-100)) == 0UL) {
                                        *(uint64_t *)6427824UL = *(uint64_t *)6429784UL;
                                        r12_2_be = r12_2;
                                    }
                                }
                            }
                        } else {
                            if ((uint64_t)(var_14 + 131U) != 0UL) {
                                if ((uint64_t)(var_14 + 130U) == 0UL) {
                                    *(uint64_t *)(local_sp_4 + (-16L)) = 4205744UL;
                                    indirect_placeholder_17(0UL, var_8, rbx_0);
                                    abort();
                                }
                            }
                            *(uint64_t *)(local_sp_4 + (-24L)) = 0UL;
                            var_15 = *(uint64_t *)6427864UL;
                            var_16 = *(uint64_t *)6428032UL;
                            *(uint64_t *)(local_sp_4 + (-32L)) = 4205796UL;
                            indirect_placeholder_25(0UL, var_16, 4303445UL, 4303543UL, var_15, 4303652UL, 4303668UL);
                            var_17 = local_sp_4 + (-40L);
                            *(uint64_t *)var_17 = 4205806UL;
                            indirect_placeholder();
                            local_sp_4_be = var_17;
                        }
                    }
                }
            }
        }
    switch (loop_state_var) {
      case 0U:
        {
            var_82 = *(uint64_t *)((uint64_t)((long)(rdx_0 << 32UL) >> (long)29UL) + rbx_0);
            var_83 = local_sp_2 + (-8L);
            *(uint64_t *)var_83 = 4206213UL;
            var_84 = indirect_placeholder_1(var_82);
            var_85 = (uint64_t)*(uint32_t *)6427996UL + 1UL;
            var_86 = (uint32_t)var_85;
            *(uint32_t *)6427996UL = var_86;
            local_sp_0 = var_83;
            local_sp_2 = var_83;
            while ((long)var_81 <= (long)(var_85 << 32UL))
                {
                    rdx_0 = (uint64_t)var_86;
                    r12_1 = (uint64_t)((uint32_t)r12_1 & (uint32_t)var_84);
                    var_82 = *(uint64_t *)((uint64_t)((long)(rdx_0 << 32UL) >> (long)29UL) + rbx_0);
                    var_83 = local_sp_2 + (-8L);
                    *(uint64_t *)var_83 = 4206213UL;
                    var_84 = indirect_placeholder_1(var_82);
                    var_85 = (uint64_t)*(uint32_t *)6427996UL + 1UL;
                    var_86 = (uint32_t)var_85;
                    *(uint32_t *)6427996UL = var_86;
                    local_sp_0 = var_83;
                    local_sp_2 = var_83;
                }
        }
        break;
      case 1U:
        {
            var_88 = *(uint64_t *)6428040UL;
            *(uint64_t *)(local_sp_0 + (-8L)) = 4206256UL;
            var_89 = indirect_placeholder_1(var_88);
            if (*(unsigned char *)6428232UL != '\x00' & (uint64_t)((uint32_t)var_89 + 1U) == 0UL) {
                *(uint64_t *)(local_sp_0 + (-16L)) = 4206266UL;
                indirect_placeholder();
                var_90 = (uint64_t)*(uint32_t *)var_89;
                *(uint64_t *)(local_sp_0 + (-24L)) = 4206288UL;
                indirect_placeholder_7(0UL, 1UL, var_90, 4310826UL, 4306176UL, r9_1, 0UL);
            }
            function_dispatcher((unsigned char *)(0UL));
            return;
        }
        break;
    }
}

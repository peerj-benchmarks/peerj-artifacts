typedef unsigned char __u_char;
typedef unsigned short __u_short;
typedef unsigned int __u_int;
typedef unsigned long __u_long;
typedef signed char __int8_t;
typedef unsigned char __uint8_t;
typedef short __int16_t;
typedef unsigned short __uint16_t;
typedef int __int32_t;
typedef unsigned int __uint32_t;
typedef long __int64_t;
typedef unsigned long __uint64_t;
typedef __int8_t __int_least8_t;
typedef __uint8_t __uint_least8_t;
typedef __int16_t __int_least16_t;
typedef __uint16_t __uint_least16_t;
typedef __int32_t __int_least32_t;
typedef __uint32_t __uint_least32_t;
typedef __int64_t __int_least64_t;
typedef __uint64_t __uint_least64_t;
typedef long __quad_t;
typedef unsigned long __u_quad_t;
typedef long __intmax_t;
typedef unsigned long __uintmax_t;
typedef unsigned long __dev_t;
typedef unsigned int __uid_t;
typedef unsigned int __gid_t;
typedef unsigned long __ino_t;
typedef unsigned long __ino64_t;
typedef unsigned int __mode_t;
typedef unsigned long __nlink_t;
typedef long __off_t;
typedef long __off64_t;
typedef int __pid_t;
typedef struct {
    int __val[2];
} __fsid_t;
typedef long __clock_t;
typedef unsigned long __rlim_t;
typedef unsigned long __rlim64_t;
typedef unsigned int __id_t;
typedef long __time_t;
typedef unsigned int __useconds_t;
typedef long __suseconds_t;
typedef int __daddr_t;
typedef int __key_t;
typedef int __clockid_t;
typedef void *__timer_t;
typedef long __blksize_t;
typedef long __blkcnt_t;
typedef long __blkcnt64_t;
typedef unsigned long __fsblkcnt_t;
typedef unsigned long __fsblkcnt64_t;
typedef unsigned long __fsfilcnt_t;
typedef unsigned long __fsfilcnt64_t;
typedef long __fsword_t;
typedef long __ssize_t;
typedef long __syscall_slong_t;
typedef unsigned long __syscall_ulong_t;
typedef __off64_t __loff_t;
typedef char *__caddr_t;
typedef long __intptr_t;
typedef unsigned int __socklen_t;
typedef int __sig_atomic_t;
typedef __int8_t int8_t;
typedef __int16_t int16_t;
typedef __int32_t int32_t;
typedef __int64_t int64_t;
typedef __uint8_t uint8_t;
typedef __uint16_t uint16_t;
typedef __uint32_t uint32_t;
typedef __uint64_t uint64_t;
typedef __int_least8_t int_least8_t;
typedef __int_least16_t int_least16_t;
typedef __int_least32_t int_least32_t;
typedef __int_least64_t int_least64_t;
typedef __uint_least8_t uint_least8_t;
typedef __uint_least16_t uint_least16_t;
typedef __uint_least32_t uint_least32_t;
typedef __uint_least64_t uint_least64_t;
typedef signed char int_fast8_t;
typedef long int_fast16_t;
typedef long int_fast32_t;
typedef long int_fast64_t;
typedef unsigned char uint_fast8_t;
typedef unsigned long uint_fast16_t;
typedef unsigned long uint_fast32_t;
typedef unsigned long uint_fast64_t;
typedef long intptr_t;
typedef unsigned long uintptr_t;
typedef __intmax_t intmax_t;
typedef __uintmax_t uintmax_t;
typedef _Bool bool;
extern uint64_t revng_init_local_sp(uint64_t param_0);
extern void indirect_placeholder(void);
extern uint64_t init_cc_src2(void);
extern uint64_t helper_cc_compute_all_wrapper(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint32_t param_3);
void bb_usage(uint64_t rbx, uint64_t rdi, uint64_t rbp) {
    uint64_t var_16;
    uint64_t *var_17;
    uint64_t local_sp_0;
    uint64_t var_0;
    uint64_t var_1;
    uint64_t var_2;
    bool var_3;
    uint64_t var_4;
    uint64_t *var_5;
    uint64_t var_30;
    uint64_t var_28;
    uint64_t var_29;
    uint64_t local_sp_5;
    uint64_t rbx1_3;
    uint64_t rbx1_2;
    uint64_t var_38;
    uint64_t local_sp_1;
    uint64_t var_39;
    uint64_t rbx1_0;
    uint64_t local_sp_2;
    uint64_t var_35;
    uint64_t var_33;
    uint64_t var_34;
    uint64_t rbx1_1;
    uint64_t local_sp_3;
    uint64_t var_36;
    uint64_t cc_src_1;
    uint64_t var_20;
    uint64_t var_21;
    uint64_t cc_src_0;
    uint64_t var_22;
    uint64_t var_23;
    bool var_24;
    uint64_t *var_25;
    uint64_t var_31;
    uint64_t var_32;
    uint64_t var_26;
    uint64_t var_27;
    uint64_t local_sp_4;
    uint64_t var_18;
    uint64_t var_19;
    uint64_t var_37;
    uint64_t *var_6;
    uint64_t *var_7;
    uint64_t *var_8;
    uint64_t *var_9;
    uint64_t *var_10;
    uint64_t *var_11;
    uint64_t *var_12;
    uint64_t *var_13;
    uint64_t *var_14;
    uint64_t *var_15;
    var_0 = revng_init_local_sp(0UL);
    var_1 = init_cc_src2();
    *(uint64_t *)(var_0 + (-8L)) = rbp;
    *(uint64_t *)(var_0 + (-16L)) = rbx;
    var_2 = helper_cc_compute_all_wrapper(rdi, 120UL, var_1, 24U);
    var_3 = ((var_2 & 64UL) == 0UL);
    var_4 = var_0 + (-144L);
    var_5 = (uint64_t *)var_4;
    rbx1_0 = 4307968UL;
    cc_src_1 = var_2;
    local_sp_5 = var_4;
    if (var_3) {
        *var_5 = 4202741UL;
        indirect_placeholder();
    } else {
        *var_5 = 4202771UL;
        indirect_placeholder();
        var_6 = (uint64_t *)(var_0 + (-152L));
        *var_6 = 4202781UL;
        indirect_placeholder();
        var_7 = (uint64_t *)(var_0 + (-160L));
        *var_7 = 4202801UL;
        indirect_placeholder();
        var_8 = (uint64_t *)(var_0 + (-168L));
        *var_8 = 4202814UL;
        indirect_placeholder();
        var_9 = (uint64_t *)(var_0 + (-176L));
        *var_9 = 4202827UL;
        indirect_placeholder();
        var_10 = (uint64_t *)(var_0 + (-184L));
        *var_10 = 4202840UL;
        indirect_placeholder();
        var_11 = (uint64_t *)(var_0 + (-192L));
        *var_11 = 4202853UL;
        indirect_placeholder();
        var_12 = (uint64_t *)(var_0 + (-200L));
        *var_12 = 4202866UL;
        indirect_placeholder();
        var_13 = (uint64_t *)(var_0 + (-208L));
        *var_13 = 4202879UL;
        indirect_placeholder();
        var_14 = (uint64_t *)(var_0 + (-216L));
        *var_14 = 4202892UL;
        indirect_placeholder();
        var_15 = (uint64_t *)(var_0 + (-224L));
        *var_15 = 4202905UL;
        indirect_placeholder();
        var_16 = var_0 + (-232L);
        var_17 = (uint64_t *)var_16;
        *var_17 = 4202918UL;
        indirect_placeholder();
        *var_17 = 4307973UL;
        *var_15 = 4307975UL;
        *var_14 = 4308072UL;
        *var_13 = 4307991UL;
        *var_12 = 4308013UL;
        *var_11 = 4308023UL;
        *var_10 = 4308038UL;
        *var_9 = 4308023UL;
        *var_8 = 4308048UL;
        *var_7 = 4308023UL;
        *var_6 = 4308058UL;
        *var_5 = 4308023UL;
        *(uint64_t *)(var_0 + (-136L)) = 0UL;
        *(uint64_t *)(var_0 + (-128L)) = 0UL;
        rbx1_3 = var_16;
        local_sp_4 = var_16;
        var_18 = local_sp_4 + (-8L);
        *(uint64_t *)var_18 = 4203075UL;
        indirect_placeholder();
        var_19 = helper_cc_compute_all_wrapper(0UL, cc_src_1, var_1, 24U);
        rbx1_2 = rbx1_3;
        cc_src_0 = var_19;
        local_sp_4 = var_18;
        while ((var_19 & 64UL) != 0UL)
            {
                var_20 = rbx1_3 + 16UL;
                var_21 = helper_cc_compute_all_wrapper(*(uint64_t *)var_20, 16UL, var_1, 25U);
                rbx1_2 = var_20;
                cc_src_0 = var_21;
                rbx1_3 = var_20;
                cc_src_1 = var_21;
                if ((var_21 & 64UL) == 0UL) {
                    break;
                }
                var_18 = local_sp_4 + (-8L);
                *(uint64_t *)var_18 = 4203075UL;
                indirect_placeholder();
                var_19 = helper_cc_compute_all_wrapper(0UL, cc_src_1, var_1, 24U);
                rbx1_2 = rbx1_3;
                cc_src_0 = var_19;
                local_sp_4 = var_18;
            }
        var_22 = *(uint64_t *)(rbx1_2 + 8UL);
        var_23 = helper_cc_compute_all_wrapper(var_22, cc_src_0, var_1, 25U);
        var_24 = ((var_23 & 64UL) == 0UL);
        var_25 = (uint64_t *)(local_sp_4 + (-16L));
        rbx1_1 = var_22;
        if (var_24) {
            *var_25 = 4203117UL;
            indirect_placeholder();
            var_31 = local_sp_4 + (-24L);
            *(uint64_t *)var_31 = 4203132UL;
            indirect_placeholder();
            var_32 = helper_cc_compute_all_wrapper(0UL, var_23, var_1, 25U);
            rbx1_0 = var_22;
            local_sp_3 = var_31;
            var_33 = local_sp_4 + (-32L);
            *(uint64_t *)var_33 = 4203155UL;
            indirect_placeholder();
            var_34 = helper_cc_compute_all_wrapper(0UL, var_32, var_1, 24U);
            local_sp_2 = var_33;
            local_sp_3 = var_33;
            if ((var_32 & 64UL) != 0UL & (var_34 & 64UL) == 0UL) {
                var_35 = local_sp_2 + (-8L);
                *(uint64_t *)var_35 = 4203186UL;
                indirect_placeholder();
                rbx1_1 = rbx1_0;
                local_sp_3 = var_35;
            }
            var_36 = local_sp_3 + (-8L);
            *(uint64_t *)var_36 = 4203211UL;
            indirect_placeholder();
            helper_cc_compute_all_wrapper(rbx1_1 + (-4307968L), 4307968UL, var_1, 17U);
            local_sp_1 = var_36;
        } else {
            *var_25 = 4203319UL;
            indirect_placeholder();
            var_26 = local_sp_4 + (-24L);
            *(uint64_t *)var_26 = 4203334UL;
            indirect_placeholder();
            var_27 = helper_cc_compute_all_wrapper(0UL, var_23, var_1, 25U);
            local_sp_0 = var_26;
            if ((var_27 & 64UL) == 0UL) {
                var_38 = local_sp_0 + (-8L);
                *(uint64_t *)var_38 = 4203282UL;
                indirect_placeholder();
                local_sp_1 = var_38;
            } else {
                var_28 = local_sp_4 + (-32L);
                *(uint64_t *)var_28 = 4203399UL;
                indirect_placeholder();
                var_29 = helper_cc_compute_all_wrapper(0UL, var_27, var_1, 24U);
                local_sp_2 = var_28;
                if ((var_29 & 64UL) == 0UL) {
                    var_30 = local_sp_4 + (-40L);
                    *(uint64_t *)var_30 = 4203366UL;
                    indirect_placeholder();
                    local_sp_1 = var_30;
                } else {
                    var_35 = local_sp_2 + (-8L);
                    *(uint64_t *)var_35 = 4203186UL;
                    indirect_placeholder();
                    rbx1_1 = rbx1_0;
                    local_sp_3 = var_35;
                    var_36 = local_sp_3 + (-8L);
                    *(uint64_t *)var_36 = 4203211UL;
                    indirect_placeholder();
                    helper_cc_compute_all_wrapper(rbx1_1 + (-4307968L), 4307968UL, var_1, 17U);
                    local_sp_1 = var_36;
                }
            }
        }
        var_39 = local_sp_1 + (-8L);
        *(uint64_t *)var_39 = 4203250UL;
        indirect_placeholder();
        local_sp_5 = var_39;
    }
    while (1U)
        {
            var_37 = local_sp_5 + (-8L);
            *(uint64_t *)var_37 = 4203257UL;
            indirect_placeholder();
            local_sp_0 = var_37;
            var_38 = local_sp_0 + (-8L);
            *(uint64_t *)var_38 = 4203282UL;
            indirect_placeholder();
            local_sp_1 = var_38;
            var_39 = local_sp_1 + (-8L);
            *(uint64_t *)var_39 = 4203250UL;
            indirect_placeholder();
            local_sp_5 = var_39;
            continue;
        }
}

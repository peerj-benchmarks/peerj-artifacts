typedef unsigned char __u_char;
typedef unsigned short __u_short;
typedef unsigned int __u_int;
typedef unsigned long __u_long;
typedef signed char __int8_t;
typedef unsigned char __uint8_t;
typedef short __int16_t;
typedef unsigned short __uint16_t;
typedef int __int32_t;
typedef unsigned int __uint32_t;
typedef long __int64_t;
typedef unsigned long __uint64_t;
typedef __int8_t __int_least8_t;
typedef __uint8_t __uint_least8_t;
typedef __int16_t __int_least16_t;
typedef __uint16_t __uint_least16_t;
typedef __int32_t __int_least32_t;
typedef __uint32_t __uint_least32_t;
typedef __int64_t __int_least64_t;
typedef __uint64_t __uint_least64_t;
typedef long __quad_t;
typedef unsigned long __u_quad_t;
typedef long __intmax_t;
typedef unsigned long __uintmax_t;
typedef unsigned long __dev_t;
typedef unsigned int __uid_t;
typedef unsigned int __gid_t;
typedef unsigned long __ino_t;
typedef unsigned long __ino64_t;
typedef unsigned int __mode_t;
typedef unsigned long __nlink_t;
typedef long __off_t;
typedef long __off64_t;
typedef int __pid_t;
typedef struct {
    int __val[2];
} __fsid_t;
typedef long __clock_t;
typedef unsigned long __rlim_t;
typedef unsigned long __rlim64_t;
typedef unsigned int __id_t;
typedef long __time_t;
typedef unsigned int __useconds_t;
typedef long __suseconds_t;
typedef int __daddr_t;
typedef int __key_t;
typedef int __clockid_t;
typedef void *__timer_t;
typedef long __blksize_t;
typedef long __blkcnt_t;
typedef long __blkcnt64_t;
typedef unsigned long __fsblkcnt_t;
typedef unsigned long __fsblkcnt64_t;
typedef unsigned long __fsfilcnt_t;
typedef unsigned long __fsfilcnt64_t;
typedef long __fsword_t;
typedef long __ssize_t;
typedef long __syscall_slong_t;
typedef unsigned long __syscall_ulong_t;
typedef __off64_t __loff_t;
typedef char *__caddr_t;
typedef long __intptr_t;
typedef unsigned int __socklen_t;
typedef int __sig_atomic_t;
typedef __int8_t int8_t;
typedef __int16_t int16_t;
typedef __int32_t int32_t;
typedef __int64_t int64_t;
typedef __uint8_t uint8_t;
typedef __uint16_t uint16_t;
typedef __uint32_t uint32_t;
typedef __uint64_t uint64_t;
typedef __int_least8_t int_least8_t;
typedef __int_least16_t int_least16_t;
typedef __int_least32_t int_least32_t;
typedef __int_least64_t int_least64_t;
typedef __uint_least8_t uint_least8_t;
typedef __uint_least16_t uint_least16_t;
typedef __uint_least32_t uint_least32_t;
typedef __uint_least64_t uint_least64_t;
typedef signed char int_fast8_t;
typedef long int_fast16_t;
typedef long int_fast32_t;
typedef long int_fast64_t;
typedef unsigned char uint_fast8_t;
typedef unsigned long uint_fast16_t;
typedef unsigned long uint_fast32_t;
typedef unsigned long uint_fast64_t;
typedef long intptr_t;
typedef unsigned long uintptr_t;
typedef __intmax_t intmax_t;
typedef __uintmax_t uintmax_t;
struct indirect_placeholder_18_ret_type;
struct indirect_placeholder_19_ret_type;
struct indirect_placeholder_20_ret_type;
struct indirect_placeholder_18_ret_type {
    uint64_t field_0;
    uint64_t field_1;
};
struct indirect_placeholder_19_ret_type {
    uint64_t field_0;
    uint64_t field_1;
    uint64_t field_2;
    uint64_t field_3;
    uint64_t field_4;
    uint64_t field_5;
    uint64_t field_6;
};
struct indirect_placeholder_20_ret_type {
    uint64_t field_0;
    uint64_t field_1;
};
extern uint64_t revng_init_local_sp(uint64_t param_0);
extern uint64_t init_rbx(void);
extern void indirect_placeholder(void);
extern uint64_t init_rbp(void);
extern uint64_t init_cc_src2(void);
extern uint64_t helper_cc_compute_c_wrapper(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint32_t param_3);
extern uint64_t init_r13(void);
extern uint64_t init_r12(void);
extern uint64_t helper_cc_compute_all_wrapper(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint32_t param_3);
extern uint64_t init_rax(void);
extern struct indirect_placeholder_18_ret_type indirect_placeholder_18(void);
extern struct indirect_placeholder_19_ret_type indirect_placeholder_19(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint64_t param_3, uint64_t param_4, uint64_t param_5, uint64_t param_6);
extern struct indirect_placeholder_20_ret_type indirect_placeholder_20(void);
uint64_t bb_iread(uint64_t rdx, uint64_t rdi, uint64_t rsi) {
    uint64_t var_0;
    uint64_t var_1;
    uint64_t var_2;
    uint64_t var_3;
    uint64_t var_4;
    uint64_t var_5;
    uint64_t var_6;
    uint64_t rax_1;
    uint64_t var_12;
    uint64_t var_13;
    uint64_t rax_0;
    uint64_t var_14;
    uint64_t var_15;
    uint64_t var_16;
    uint64_t var_17;
    uint64_t var_18;
    uint64_t cc_src_1;
    uint64_t var_10;
    uint64_t local_sp_3;
    uint64_t var_11;
    uint64_t r8_0;
    uint64_t var_19;
    uint64_t cc_src_0;
    uint64_t local_sp_0;
    uint64_t var_20;
    uint64_t var_21;
    uint64_t local_sp_1;
    uint64_t var_22;
    uint64_t var_23;
    uint64_t var_24;
    uint64_t var_25;
    uint64_t var_26;
    uint64_t var_27;
    uint64_t var_28;
    struct indirect_placeholder_18_ret_type var_29;
    uint64_t rbx_0;
    uint64_t r12_0;
    uint64_t local_sp_2;
    uint64_t r9_0;
    uint64_t var_30;
    uint64_t var_31;
    uint64_t var_32;
    uint64_t var_33;
    uint64_t var_34;
    uint64_t var_35;
    uint64_t var_36;
    uint64_t var_37;
    struct indirect_placeholder_20_ret_type var_7;
    uint64_t var_8;
    uint64_t var_9;
    unsigned int loop_state_var;
    var_0 = revng_init_local_sp(0UL);
    var_1 = init_rax();
    var_2 = init_rbx();
    var_3 = init_rbp();
    var_4 = init_r13();
    var_5 = init_r12();
    var_6 = init_cc_src2();
    *(uint64_t *)(var_0 + (-8L)) = var_4;
    *(uint64_t *)(var_0 + (-16L)) = var_5;
    *(uint64_t *)(var_0 + (-24L)) = var_3;
    *(uint64_t *)(var_0 + (-32L)) = var_2;
    cc_src_1 = rdx;
    r12_0 = 0UL;
    rax_1 = var_1;
    local_sp_3 = var_0 + (-40L);
    while (1U)
        {
            *(uint64_t *)(local_sp_3 + (-8L)) = 4206948UL;
            var_7 = indirect_placeholder_20();
            var_8 = local_sp_3 + (-16L);
            *(uint64_t *)var_8 = 4206962UL;
            indirect_placeholder();
            var_9 = helper_cc_compute_all_wrapper(rax_1 + 1UL, 18446744073709551615UL, var_6, 17U);
            rax_0 = rax_1;
            local_sp_0 = var_8;
            local_sp_1 = var_8;
            rbx_0 = rax_1;
            if ((var_9 & 64UL) == 0UL) {
                var_19 = helper_cc_compute_all_wrapper(rax_1, var_9, var_6, 25U);
                cc_src_0 = var_19;
                if ((signed char)(unsigned char)var_19 <= '\xff') {
                    loop_state_var = 0U;
                    break;
                }
            }
            var_10 = local_sp_3 + (-24L);
            *(uint64_t *)var_10 = 4206976UL;
            indirect_placeholder();
            var_11 = helper_cc_compute_all_wrapper((uint64_t)*(uint32_t *)rax_1 + (-22L), 22UL, var_6, 16U);
            local_sp_0 = var_10;
            rbx_0 = 0UL;
            if ((var_11 & 64UL) == 0UL) {
                var_20 = local_sp_0 + (-8L);
                *(uint64_t *)var_20 = 4207041UL;
                indirect_placeholder();
                var_21 = helper_cc_compute_all_wrapper((uint64_t)*(uint32_t *)rax_0 + (-4L), 4UL, var_6, 16U);
                cc_src_0 = var_21;
                local_sp_1 = var_20;
                rax_1 = rax_0;
                local_sp_3 = var_20;
                if ((var_21 & 64UL) == 0UL) {
                    continue;
                }
                loop_state_var = 0U;
                break;
            }
            var_12 = *(uint64_t *)6383704UL;
            var_13 = helper_cc_compute_all_wrapper(var_12, var_11, var_6, 25U);
            rax_0 = var_12;
            var_14 = helper_cc_compute_all_wrapper(rdx - var_12, var_12, var_6, 17U);
            var_15 = helper_cc_compute_all_wrapper((uint64_t)(*(unsigned char *)6384329UL & '@'), var_14, var_6, 22U);
            cc_src_1 = var_15;
            if ((uint64_t)(((unsigned char)(var_13 >> 4UL) ^ (unsigned char)var_13) & '\xc0') != 0UL & (var_14 & 65UL) != 0UL & (var_15 & 64UL) != 0UL) {
                var_16 = var_7.field_0;
                var_17 = var_7.field_1;
                var_18 = local_sp_3 + (-32L);
                *(uint64_t *)var_18 = 4207012UL;
                indirect_placeholder();
                *(uint32_t *)var_12 = 0U;
                local_sp_2 = var_18;
                r9_0 = var_16;
                r8_0 = var_17;
                loop_state_var = 1U;
                break;
            }
        }
    switch (loop_state_var) {
      case 1U:
        {
            var_30 = helper_cc_compute_all_wrapper(r12_0, cc_src_1, var_6, 22U);
            var_31 = helper_cc_compute_all_wrapper((uint64_t)*(unsigned char *)6384232UL, 0UL, var_6, 14U);
            var_32 = *(uint64_t *)6383704UL;
            var_33 = helper_cc_compute_all_wrapper(var_32, var_31, var_6, 25U);
            var_34 = helper_cc_compute_all_wrapper(rdx - var_32, var_32, var_6, 17U);
            if ((var_30 & 64UL) != 0UL & (var_31 & 64UL) != 0UL & (uint64_t)(((unsigned char)(var_33 >> 4UL) ^ (unsigned char)var_33) & '\xc0') != 0UL & (var_34 & 65UL) != 0UL) {
                var_35 = helper_cc_compute_all_wrapper((uint64_t)*(uint32_t *)6383276UL + (-1L), 1UL, var_6, 16U);
                if ((var_35 & 64UL) == 0UL) {
                    var_36 = helper_cc_compute_all_wrapper(var_32 + (-1L), 1UL, var_6, 17U);
                    var_37 = ((var_36 & 64UL) == 0UL) ? 4269624UL : 4269560UL;
                    *(uint64_t *)(local_sp_2 + (-8L)) = 4207146UL;
                    indirect_placeholder_19(0UL, var_37, 0UL, var_32, 0UL, r9_0, r8_0);
                }
                *(unsigned char *)6384232UL = (unsigned char)'\x00';
            }
            *(uint64_t *)6383704UL = rbx_0;
            return rbx_0;
        }
        break;
      case 0U:
        {
            var_22 = var_7.field_0;
            var_23 = var_7.field_1;
            var_24 = helper_cc_compute_all_wrapper(rax_1, cc_src_0, var_6, 25U);
            var_25 = (rsi & (-256L)) | ((uint64_t)(((unsigned char)(var_24 >> 4UL) ^ (unsigned char)var_24) & '\xc0') == 0UL);
            var_26 = helper_cc_compute_c_wrapper(rax_1 - rdx, rdx, var_6, 17U);
            r12_0 = var_25;
            local_sp_2 = local_sp_1;
            r9_0 = var_22;
            r8_0 = var_23;
            var_27 = helper_cc_compute_all_wrapper(var_25, rdx, var_6, 22U);
            cc_src_1 = var_27;
            if (var_26 != 0UL & (var_27 & 64UL) == 0UL) {
                var_28 = local_sp_1 + (-8L);
                *(uint64_t *)var_28 = 4207068UL;
                var_29 = indirect_placeholder_18();
                local_sp_2 = var_28;
                r9_0 = var_29.field_0;
                r8_0 = var_29.field_1;
            }
        }
        break;
    }
}

typedef unsigned char __u_char;
typedef unsigned short __u_short;
typedef unsigned int __u_int;
typedef unsigned long __u_long;
typedef signed char __int8_t;
typedef unsigned char __uint8_t;
typedef short __int16_t;
typedef unsigned short __uint16_t;
typedef int __int32_t;
typedef unsigned int __uint32_t;
typedef long __int64_t;
typedef unsigned long __uint64_t;
typedef __int8_t __int_least8_t;
typedef __uint8_t __uint_least8_t;
typedef __int16_t __int_least16_t;
typedef __uint16_t __uint_least16_t;
typedef __int32_t __int_least32_t;
typedef __uint32_t __uint_least32_t;
typedef __int64_t __int_least64_t;
typedef __uint64_t __uint_least64_t;
typedef long __quad_t;
typedef unsigned long __u_quad_t;
typedef long __intmax_t;
typedef unsigned long __uintmax_t;
typedef unsigned long __dev_t;
typedef unsigned int __uid_t;
typedef unsigned int __gid_t;
typedef unsigned long __ino_t;
typedef unsigned long __ino64_t;
typedef unsigned int __mode_t;
typedef unsigned long __nlink_t;
typedef long __off_t;
typedef long __off64_t;
typedef int __pid_t;
typedef struct {
    int __val[2];
} __fsid_t;
typedef long __clock_t;
typedef unsigned long __rlim_t;
typedef unsigned long __rlim64_t;
typedef unsigned int __id_t;
typedef long __time_t;
typedef unsigned int __useconds_t;
typedef long __suseconds_t;
typedef int __daddr_t;
typedef int __key_t;
typedef int __clockid_t;
typedef void *__timer_t;
typedef long __blksize_t;
typedef long __blkcnt_t;
typedef long __blkcnt64_t;
typedef unsigned long __fsblkcnt_t;
typedef unsigned long __fsblkcnt64_t;
typedef unsigned long __fsfilcnt_t;
typedef unsigned long __fsfilcnt64_t;
typedef long __fsword_t;
typedef long __ssize_t;
typedef long __syscall_slong_t;
typedef unsigned long __syscall_ulong_t;
typedef __off64_t __loff_t;
typedef char *__caddr_t;
typedef long __intptr_t;
typedef unsigned int __socklen_t;
typedef int __sig_atomic_t;
typedef __int8_t int8_t;
typedef __int16_t int16_t;
typedef __int32_t int32_t;
typedef __int64_t int64_t;
typedef __uint8_t uint8_t;
typedef __uint16_t uint16_t;
typedef __uint32_t uint32_t;
typedef __uint64_t uint64_t;
typedef __int_least8_t int_least8_t;
typedef __int_least16_t int_least16_t;
typedef __int_least32_t int_least32_t;
typedef __int_least64_t int_least64_t;
typedef __uint_least8_t uint_least8_t;
typedef __uint_least16_t uint_least16_t;
typedef __uint_least32_t uint_least32_t;
typedef __uint_least64_t uint_least64_t;
typedef signed char int_fast8_t;
typedef long int_fast16_t;
typedef long int_fast32_t;
typedef long int_fast64_t;
typedef unsigned char uint_fast8_t;
typedef unsigned long uint_fast16_t;
typedef unsigned long uint_fast32_t;
typedef unsigned long uint_fast64_t;
typedef long intptr_t;
typedef unsigned long uintptr_t;
typedef __intmax_t intmax_t;
typedef __uintmax_t uintmax_t;
typedef _Bool bool;
struct bb_invalidate_cache_ret_type;
struct helper_divq_EAX_wrapper_ret_type;
struct type_6;
struct bb_invalidate_cache_ret_type {
    uint64_t field_0;
    uint64_t field_1;
};
struct helper_divq_EAX_wrapper_ret_type {
    uint64_t field_0;
    uint64_t field_1;
    uint64_t field_2;
    uint64_t field_3;
    uint32_t field_4;
    uint32_t field_5;
    uint64_t field_6;
    uint32_t field_7;
    uint64_t field_8;
    uint32_t field_9;
    unsigned char field_10;
    uint32_t field_11;
    uint32_t field_12;
};
struct type_6 {
};
extern uint64_t revng_init_local_sp(uint64_t param_0);
extern uint64_t init_rbx(void);
extern void indirect_placeholder(void);
extern uint64_t init_rbp(void);
extern uint64_t init_r14(void);
extern uint64_t init_r13(void);
extern uint64_t init_r12(void);
extern uint32_t init_state_0x8248(void);
extern uint64_t init_state_0x9018(void);
extern uint32_t init_state_0x9010(void);
extern uint64_t init_state_0x8408(void);
extern uint64_t init_state_0x8328(void);
extern uint64_t init_state_0x82d8(void);
extern uint32_t init_state_0x9080(void);
extern struct helper_divq_EAX_wrapper_ret_type helper_divq_EAX_wrapper(struct type_6 *param_0, uint64_t param_1, uint64_t param_2, uint64_t param_3, uint64_t param_4, uint64_t param_5, uint64_t param_6, uint64_t param_7, uint64_t param_8, uint64_t param_9, uint64_t param_10, uint64_t param_11, uint64_t param_12, uint32_t param_13, uint64_t param_14, uint32_t param_15, uint64_t param_16, uint64_t param_17, uint64_t param_18, uint32_t param_19);
extern uint64_t init_r9(void);
extern uint64_t init_r8(void);
extern uint64_t init_r10(void);
extern uint64_t init_rcx(void);
struct bb_invalidate_cache_ret_type bb_invalidate_cache(uint64_t rdi, uint64_t rsi) {
    uint64_t var_29;
    uint64_t rdi2_1;
    uint64_t var_30;
    uint64_t r14_1;
    uint64_t rbx_2;
    uint64_t local_sp_1;
    uint64_t var_31;
    uint64_t var_0;
    uint64_t var_1;
    uint64_t var_2;
    uint64_t var_3;
    uint64_t var_4;
    uint64_t var_5;
    uint64_t var_6;
    uint64_t var_7;
    uint64_t var_8;
    uint64_t var_9;
    uint32_t var_10;
    uint64_t var_11;
    uint32_t var_12;
    uint64_t var_13;
    uint64_t var_14;
    uint64_t var_15;
    uint32_t var_16;
    uint64_t var_17;
    uint64_t var_18;
    bool var_19;
    unsigned char *storemerge2_in_in;
    uint64_t *storemerge1;
    unsigned char storemerge2_in;
    uint64_t storemerge2;
    bool var_20;
    uint64_t var_21;
    uint64_t var_22;
    uint64_t var_23;
    uint64_t rcx_1;
    uint64_t var_24;
    uint64_t var_25;
    uint64_t var_26;
    uint64_t r14_0;
    uint64_t var_27;
    uint64_t rbx_1;
    uint64_t rax_0;
    uint64_t rbx_0;
    uint64_t rax_1;
    uint64_t rdx_0;
    uint64_t rcx_0;
    uint64_t rax_6;
    struct bb_invalidate_cache_ret_type mrv;
    struct bb_invalidate_cache_ret_type mrv1;
    uint64_t rax_2;
    uint64_t storemerge;
    uint64_t var_28;
    bool var_32;
    uint64_t var_33;
    uint64_t spec_select;
    uint64_t rsi3_0;
    uint64_t var_34;
    uint64_t var_35;
    uint64_t rax_4;
    struct helper_divq_EAX_wrapper_ret_type var_36;
    uint64_t rax_5;
    uint64_t var_37;
    var_0 = revng_init_local_sp(0UL);
    var_1 = init_rbx();
    var_2 = init_r14();
    var_3 = init_rbp();
    var_4 = init_r13();
    var_5 = init_r12();
    var_6 = init_rcx();
    var_7 = init_r10();
    var_8 = init_r9();
    var_9 = init_r8();
    var_10 = init_state_0x8248();
    var_11 = init_state_0x9018();
    var_12 = init_state_0x9010();
    var_13 = init_state_0x8408();
    var_14 = init_state_0x8328();
    var_15 = init_state_0x82d8();
    var_16 = init_state_0x9080();
    *(uint64_t *)(var_0 + (-8L)) = var_2;
    *(uint64_t *)(var_0 + (-16L)) = var_4;
    *(uint64_t *)(var_0 + (-24L)) = var_5;
    *(uint64_t *)(var_0 + (-32L)) = var_3;
    var_17 = var_0 + (-40L);
    *(uint64_t *)var_17 = var_1;
    var_18 = (uint64_t)(uint32_t)rdi;
    var_19 = (var_18 == 0UL);
    storemerge2_in_in = (unsigned char *)var_19 ? 6384009UL : 6384008UL;
    storemerge1 = (uint64_t *)var_19 ? 6383688UL : 6383680UL;
    storemerge2_in = *storemerge2_in_in;
    storemerge2 = (uint64_t)storemerge2_in;
    var_20 = (rsi == 0UL);
    var_21 = *storemerge1;
    rdi2_1 = rdi;
    local_sp_1 = var_17;
    rcx_1 = var_6;
    r14_0 = 0UL;
    rbx_1 = var_21;
    rax_0 = var_21;
    rbx_0 = var_21;
    rdx_0 = 18446744073709551615UL;
    rcx_0 = var_6;
    rax_6 = 1UL;
    rax_2 = 0UL;
    storemerge = 0UL;
    rax_5 = 4294967295UL;
    if (!var_20) {
        var_22 = var_21 + rsi;
        var_23 = (uint64_t)((uint32_t)var_22 & 131071U);
        *storemerge1 = var_23;
        if (var_22 > 131071UL) {
            mrv.field_0 = rax_6;
            mrv1 = mrv;
            mrv1.field_1 = rcx_1;
            return mrv1;
        }
        var_24 = var_22 - var_23;
        rbx_0 = var_24;
        rbx_1 = var_24;
        if (var_24 == 0UL) {
            mrv.field_0 = rax_6;
            mrv1 = mrv;
            mrv1.field_1 = rcx_1;
            return mrv1;
        }
        var_25 = (((var_24 | rsi) & (-256L)) | storemerge2) | 1UL;
        var_26 = *(uint64_t *)6383688UL;
        r14_0 = var_26;
        rax_1 = var_25;
        rax_2 = var_25;
        if (var_19) {
            var_27 = *(uint64_t *)6383680UL;
            storemerge = var_27;
            var_28 = *(uint64_t *)6383264UL;
            r14_1 = storemerge;
            rbx_2 = rbx_1;
            if (var_28 != 18446744073709551615UL) {
                rdx_0 = rax_2;
                rdi2_1 = var_18;
                if ((long)var_28 > (long)18446744073709551615UL) {
                    var_29 = var_0 + (-48L);
                    *(uint64_t *)var_29 = 4205863UL;
                    indirect_placeholder();
                    *(uint64_t *)6383264UL = rax_2;
                    local_sp_1 = var_29;
                } else {
                    rdx_0 = var_28;
                    if ((uint64_t)(unsigned char)rax_2 == 0UL) {
                        var_30 = var_28 + (rbx_1 + storemerge);
                        *(uint64_t *)6383264UL = var_30;
                        rdx_0 = var_30;
                    }
                }
            }
        } else {
            r14_1 = r14_0;
            rbx_2 = rbx_0;
            if (*(unsigned char *)6384252UL != '\x00') {
                *(uint64_t *)(var_0 + (-48L)) = 4205800UL;
                indirect_placeholder();
                *(uint32_t *)rax_1 = 29U;
                var_37 = (rax_5 & (-256L)) | ((uint64_t)((uint32_t)rax_5 + 1U) != 0UL);
                rcx_1 = rcx_0;
                rax_6 = var_37;
                mrv.field_0 = rax_6;
                mrv1 = mrv;
                mrv1.field_1 = rcx_1;
                return mrv1;
            }
            rdx_0 = *(uint64_t *)6384240UL;
        }
        if ((long)rdx_0 >= (long)0UL) {
            var_31 = var_20;
            rcx_0 = 4UL;
            if ((storemerge2 & var_31) == 0UL) {
                var_34 = var_31 | 4294967040UL;
                var_35 = (rdx_0 - rbx_2) - r14_1;
                rsi3_0 = var_35;
                rax_4 = var_34;
                if (rbx_2 == 0UL) {
                    var_36 = helper_divq_EAX_wrapper((struct type_6 *)(0UL), *(uint64_t *)6384408UL, 4205956UL, rsi3_0, 0UL, rbx_2, rsi, rdi2_1, var_6, var_7, rsi3_0, var_8, var_9, var_10, var_11, var_12, var_13, var_14, var_15, var_16);
                    rax_4 = var_36.field_1;
                }
            } else {
                var_32 = (rbx_2 == 0UL);
                var_33 = rdx_0 - rbx_2;
                spec_select = var_32 ? (var_33 - r14_1) : var_33;
                rsi3_0 = spec_select;
                var_36 = helper_divq_EAX_wrapper((struct type_6 *)(0UL), *(uint64_t *)6384408UL, 4205956UL, rsi3_0, 0UL, rbx_2, rsi, rdi2_1, var_6, var_7, rsi3_0, var_8, var_9, var_10, var_11, var_12, var_13, var_14, var_15, var_16);
                rax_4 = var_36.field_1;
            }
            *(uint64_t *)(local_sp_1 + (-8L)) = 4205987UL;
            indirect_placeholder();
            rax_5 = rax_4;
        }
        var_37 = (rax_5 & (-256L)) | ((uint64_t)((uint32_t)rax_5 + 1U) != 0UL);
        rcx_1 = rcx_0;
        rax_6 = var_37;
        mrv.field_0 = rax_6;
        mrv1 = mrv;
        mrv1.field_1 = rcx_1;
        return mrv1;
    }
    if (var_21 != 0UL) {
        rax_0 = 1UL;
        if (storemerge2_in == '\x00') {
            mrv.field_0 = rax_6;
            mrv1 = mrv;
            mrv1.field_1 = rcx_1;
            return mrv1;
        }
    }
    rax_1 = rax_0;
    if (var_19) {
        r14_1 = r14_0;
        rbx_2 = rbx_0;
        if (*(unsigned char *)6384252UL != '\x00') {
            *(uint64_t *)(var_0 + (-48L)) = 4205800UL;
            indirect_placeholder();
            *(uint32_t *)rax_1 = 29U;
            var_37 = (rax_5 & (-256L)) | ((uint64_t)((uint32_t)rax_5 + 1U) != 0UL);
            rcx_1 = rcx_0;
            rax_6 = var_37;
            mrv.field_0 = rax_6;
            mrv1 = mrv;
            mrv1.field_1 = rcx_1;
            return mrv1;
        }
        rdx_0 = *(uint64_t *)6384240UL;
    } else {
        var_28 = *(uint64_t *)6383264UL;
        r14_1 = storemerge;
        rbx_2 = rbx_1;
        if (var_28 != 18446744073709551615UL) {
            rdx_0 = rax_2;
            rdi2_1 = var_18;
            if ((long)var_28 > (long)18446744073709551615UL) {
                var_29 = var_0 + (-48L);
                *(uint64_t *)var_29 = 4205863UL;
                indirect_placeholder();
                *(uint64_t *)6383264UL = rax_2;
                local_sp_1 = var_29;
            } else {
                rdx_0 = var_28;
                if ((uint64_t)(unsigned char)rax_2 == 0UL) {
                    var_30 = var_28 + (rbx_1 + storemerge);
                    *(uint64_t *)6383264UL = var_30;
                    rdx_0 = var_30;
                }
            }
        }
    }
}

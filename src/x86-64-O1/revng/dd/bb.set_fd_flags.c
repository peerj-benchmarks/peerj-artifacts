typedef unsigned char __u_char;
typedef unsigned short __u_short;
typedef unsigned int __u_int;
typedef unsigned long __u_long;
typedef signed char __int8_t;
typedef unsigned char __uint8_t;
typedef short __int16_t;
typedef unsigned short __uint16_t;
typedef int __int32_t;
typedef unsigned int __uint32_t;
typedef long __int64_t;
typedef unsigned long __uint64_t;
typedef __int8_t __int_least8_t;
typedef __uint8_t __uint_least8_t;
typedef __int16_t __int_least16_t;
typedef __uint16_t __uint_least16_t;
typedef __int32_t __int_least32_t;
typedef __uint32_t __uint_least32_t;
typedef __int64_t __int_least64_t;
typedef __uint64_t __uint_least64_t;
typedef long __quad_t;
typedef unsigned long __u_quad_t;
typedef long __intmax_t;
typedef unsigned long __uintmax_t;
typedef unsigned long __dev_t;
typedef unsigned int __uid_t;
typedef unsigned int __gid_t;
typedef unsigned long __ino_t;
typedef unsigned long __ino64_t;
typedef unsigned int __mode_t;
typedef unsigned long __nlink_t;
typedef long __off_t;
typedef long __off64_t;
typedef int __pid_t;
typedef struct {
    int __val[2];
} __fsid_t;
typedef long __clock_t;
typedef unsigned long __rlim_t;
typedef unsigned long __rlim64_t;
typedef unsigned int __id_t;
typedef long __time_t;
typedef unsigned int __useconds_t;
typedef long __suseconds_t;
typedef int __daddr_t;
typedef int __key_t;
typedef int __clockid_t;
typedef void *__timer_t;
typedef long __blksize_t;
typedef long __blkcnt_t;
typedef long __blkcnt64_t;
typedef unsigned long __fsblkcnt_t;
typedef unsigned long __fsblkcnt64_t;
typedef unsigned long __fsfilcnt_t;
typedef unsigned long __fsfilcnt64_t;
typedef long __fsword_t;
typedef long __ssize_t;
typedef long __syscall_slong_t;
typedef unsigned long __syscall_ulong_t;
typedef __off64_t __loff_t;
typedef char *__caddr_t;
typedef long __intptr_t;
typedef unsigned int __socklen_t;
typedef int __sig_atomic_t;
typedef __int8_t int8_t;
typedef __int16_t int16_t;
typedef __int32_t int32_t;
typedef __int64_t int64_t;
typedef __uint8_t uint8_t;
typedef __uint16_t uint16_t;
typedef __uint32_t uint32_t;
typedef __uint64_t uint64_t;
typedef __int_least8_t int_least8_t;
typedef __int_least16_t int_least16_t;
typedef __int_least32_t int_least32_t;
typedef __int_least64_t int_least64_t;
typedef __uint_least8_t uint_least8_t;
typedef __uint_least16_t uint_least16_t;
typedef __uint_least32_t uint_least32_t;
typedef __uint_least64_t uint_least64_t;
typedef signed char int_fast8_t;
typedef long int_fast16_t;
typedef long int_fast32_t;
typedef long int_fast64_t;
typedef unsigned char uint_fast8_t;
typedef unsigned long uint_fast16_t;
typedef unsigned long uint_fast32_t;
typedef unsigned long uint_fast64_t;
typedef long intptr_t;
typedef unsigned long uintptr_t;
typedef __intmax_t intmax_t;
typedef __uintmax_t uintmax_t;
typedef _Bool bool;
struct indirect_placeholder_13_ret_type;
struct indirect_placeholder_14_ret_type;
struct indirect_placeholder_15_ret_type;
struct indirect_placeholder_12_ret_type;
struct indirect_placeholder_16_ret_type;
struct indirect_placeholder_13_ret_type {
    uint64_t field_0;
    uint64_t field_1;
};
struct indirect_placeholder_14_ret_type {
    uint64_t field_0;
    uint64_t field_1;
};
struct indirect_placeholder_15_ret_type {
    uint64_t field_0;
    uint64_t field_1;
    uint64_t field_2;
};
struct indirect_placeholder_12_ret_type {
    uint64_t field_0;
    uint64_t field_1;
    uint64_t field_2;
    uint64_t field_3;
    uint64_t field_4;
    uint64_t field_5;
    uint64_t field_6;
};
struct indirect_placeholder_16_ret_type {
    uint64_t field_0;
    uint64_t field_1;
};
extern uint64_t revng_init_local_sp(uint64_t param_0);
extern uint64_t init_rbx(void);
extern void indirect_placeholder(void);
extern uint64_t init_rbp(void);
extern uint64_t init_r13(void);
extern uint64_t init_r12(void);
extern struct indirect_placeholder_13_ret_type indirect_placeholder_13(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint64_t param_3, uint64_t param_4);
extern struct indirect_placeholder_14_ret_type indirect_placeholder_14(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint64_t param_3, uint64_t param_4);
extern struct indirect_placeholder_15_ret_type indirect_placeholder_15(uint64_t param_0, uint64_t param_1);
extern struct indirect_placeholder_12_ret_type indirect_placeholder_12(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint64_t param_3, uint64_t param_4, uint64_t param_5, uint64_t param_6);
extern struct indirect_placeholder_16_ret_type indirect_placeholder_16(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint64_t param_3, uint64_t param_4);
void bb_set_fd_flags(uint64_t rdx, uint64_t rdi, uint64_t rcx, uint64_t rsi) {
    struct indirect_placeholder_16_ret_type var_8;
    struct indirect_placeholder_13_ret_type var_23;
    struct indirect_placeholder_14_ret_type var_24;
    uint64_t var_0;
    uint64_t var_1;
    uint64_t var_2;
    uint64_t var_3;
    uint64_t var_4;
    uint32_t var_5;
    uint32_t var_17;
    uint64_t var_18;
    uint64_t var_19;
    uint32_t var_20;
    uint64_t var_21;
    uint64_t var_22;
    uint64_t local_sp_2;
    uint64_t local_sp_1_ph;
    uint32_t var_12;
    uint64_t var_13;
    bool var_14;
    uint64_t var_15;
    uint64_t *var_16;
    struct indirect_placeholder_15_ret_type var_25;
    uint64_t var_26;
    uint64_t var_27;
    uint64_t var_28;
    uint64_t var_29;
    uint64_t var_30;
    uint64_t var_6;
    uint64_t var_7;
    uint64_t var_9;
    uint64_t var_10;
    uint32_t var_11;
    var_0 = revng_init_local_sp(0UL);
    var_1 = init_rbx();
    var_2 = init_rbp();
    var_3 = init_r13();
    var_4 = init_r12();
    *(uint64_t *)(var_0 + (-8L)) = var_3;
    *(uint64_t *)(var_0 + (-16L)) = var_4;
    *(uint64_t *)(var_0 + (-24L)) = var_2;
    *(uint64_t *)(var_0 + (-32L)) = var_1;
    var_5 = (uint32_t)rsi & (-131329);
    if ((uint64_t)var_5 == 0UL) {
        return;
    }
    var_6 = (uint64_t)(uint32_t)rdi;
    var_7 = var_0 + (-192L);
    *(uint64_t *)var_7 = 4204644UL;
    var_8 = indirect_placeholder_16(0UL, rdx, rdi, rcx, 3UL);
    var_9 = var_8.field_0;
    var_10 = var_8.field_1;
    var_11 = (uint32_t)var_9;
    local_sp_2 = var_7;
    if ((int)var_11 < (int)0U) {
        while (1U)
            {
                *(uint64_t *)(local_sp_2 + (-8L)) = 4204760UL;
                var_25 = indirect_placeholder_15(4UL, rdx);
                var_26 = var_25.field_0;
                var_27 = var_25.field_1;
                var_28 = var_25.field_2;
                *(uint64_t *)(local_sp_2 + (-16L)) = 4204768UL;
                indirect_placeholder();
                var_29 = (uint64_t)*(uint32_t *)var_26;
                var_30 = local_sp_2 + (-24L);
                *(uint64_t *)var_30 = 4204793UL;
                indirect_placeholder_12(0UL, 4265039UL, 1UL, var_26, var_29, var_27, var_28);
                local_sp_1_ph = var_30;
                local_sp_2 = local_sp_1_ph;
                continue;
            }
    }
    var_12 = var_5 | var_11;
    var_13 = (uint64_t)var_12;
    if ((uint64_t)(var_11 - var_12) == 0UL) {
        return;
    }
    var_14 = ((uint64_t)(var_12 & 65536U) == 0UL);
    var_15 = var_0 + (-200L);
    var_16 = (uint64_t *)var_15;
    local_sp_1_ph = var_15;
    local_sp_2 = var_15;
    if (var_14) {
        *var_16 = 4204834UL;
        var_24 = indirect_placeholder_14(0UL, var_13, var_6, var_10, 4UL);
        if ((uint64_t)((uint32_t)var_24.field_0 + 1U) == 0UL) {
            return;
        }
    }
    *var_16 = 4204683UL;
    indirect_placeholder();
    if ((uint64_t)var_11 == 0UL) {
        var_17 = (uint32_t)((uint16_t)*(uint32_t *)(var_0 + (-176L)) & (unsigned short)61440U);
        var_18 = (uint64_t)var_17;
        if ((uint64_t)((var_17 + (-16384)) & (-4096)) == 0UL) {
            var_20 = var_12 & (-65537);
            var_21 = (uint64_t)var_20;
            if ((uint64_t)(var_11 - var_20) == 0UL) {
                return;
            }
            var_22 = var_0 + (-208L);
            *(uint64_t *)var_22 = 4204733UL;
            var_23 = indirect_placeholder_13(0UL, var_21, var_6, var_10, 4UL);
            local_sp_2 = var_22;
            if ((uint64_t)((uint32_t)var_23.field_0 + 1U) == 0UL) {
                return;
            }
        }
        var_19 = var_0 + (-208L);
        *(uint64_t *)var_19 = 4204708UL;
        indirect_placeholder();
        *(uint32_t *)var_18 = 20U;
        local_sp_1_ph = var_19;
        local_sp_2 = local_sp_1_ph;
        while (1U)
            {
                *(uint64_t *)(local_sp_2 + (-8L)) = 4204760UL;
                var_25 = indirect_placeholder_15(4UL, rdx);
                var_26 = var_25.field_0;
                var_27 = var_25.field_1;
                var_28 = var_25.field_2;
                *(uint64_t *)(local_sp_2 + (-16L)) = 4204768UL;
                indirect_placeholder();
                var_29 = (uint64_t)*(uint32_t *)var_26;
                var_30 = local_sp_2 + (-24L);
                *(uint64_t *)var_30 = 4204793UL;
                indirect_placeholder_12(0UL, 4265039UL, 1UL, var_26, var_29, var_27, var_28);
                local_sp_1_ph = var_30;
                local_sp_2 = local_sp_1_ph;
                continue;
            }
    }
    local_sp_2 = local_sp_1_ph;
}

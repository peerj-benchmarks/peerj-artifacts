typedef unsigned char __u_char;
typedef unsigned short __u_short;
typedef unsigned int __u_int;
typedef unsigned long __u_long;
typedef signed char __int8_t;
typedef unsigned char __uint8_t;
typedef short __int16_t;
typedef unsigned short __uint16_t;
typedef int __int32_t;
typedef unsigned int __uint32_t;
typedef long __int64_t;
typedef unsigned long __uint64_t;
typedef __int8_t __int_least8_t;
typedef __uint8_t __uint_least8_t;
typedef __int16_t __int_least16_t;
typedef __uint16_t __uint_least16_t;
typedef __int32_t __int_least32_t;
typedef __uint32_t __uint_least32_t;
typedef __int64_t __int_least64_t;
typedef __uint64_t __uint_least64_t;
typedef long __quad_t;
typedef unsigned long __u_quad_t;
typedef long __intmax_t;
typedef unsigned long __uintmax_t;
typedef unsigned long __dev_t;
typedef unsigned int __uid_t;
typedef unsigned int __gid_t;
typedef unsigned long __ino_t;
typedef unsigned long __ino64_t;
typedef unsigned int __mode_t;
typedef unsigned long __nlink_t;
typedef long __off_t;
typedef long __off64_t;
typedef int __pid_t;
typedef struct {
    int __val[2];
} __fsid_t;
typedef long __clock_t;
typedef unsigned long __rlim_t;
typedef unsigned long __rlim64_t;
typedef unsigned int __id_t;
typedef long __time_t;
typedef unsigned int __useconds_t;
typedef long __suseconds_t;
typedef int __daddr_t;
typedef int __key_t;
typedef int __clockid_t;
typedef void *__timer_t;
typedef long __blksize_t;
typedef long __blkcnt_t;
typedef long __blkcnt64_t;
typedef unsigned long __fsblkcnt_t;
typedef unsigned long __fsblkcnt64_t;
typedef unsigned long __fsfilcnt_t;
typedef unsigned long __fsfilcnt64_t;
typedef long __fsword_t;
typedef long __ssize_t;
typedef long __syscall_slong_t;
typedef unsigned long __syscall_ulong_t;
typedef __off64_t __loff_t;
typedef char *__caddr_t;
typedef long __intptr_t;
typedef unsigned int __socklen_t;
typedef int __sig_atomic_t;
typedef __int8_t int8_t;
typedef __int16_t int16_t;
typedef __int32_t int32_t;
typedef __int64_t int64_t;
typedef __uint8_t uint8_t;
typedef __uint16_t uint16_t;
typedef __uint32_t uint32_t;
typedef __uint64_t uint64_t;
typedef __int_least8_t int_least8_t;
typedef __int_least16_t int_least16_t;
typedef __int_least32_t int_least32_t;
typedef __int_least64_t int_least64_t;
typedef __uint_least8_t uint_least8_t;
typedef __uint_least16_t uint_least16_t;
typedef __uint_least32_t uint_least32_t;
typedef __uint_least64_t uint_least64_t;
typedef signed char int_fast8_t;
typedef long int_fast16_t;
typedef long int_fast32_t;
typedef long int_fast64_t;
typedef unsigned char uint_fast8_t;
typedef unsigned long uint_fast16_t;
typedef unsigned long uint_fast32_t;
typedef unsigned long uint_fast64_t;
typedef long intptr_t;
typedef unsigned long uintptr_t;
typedef __intmax_t intmax_t;
typedef __uintmax_t uintmax_t;
struct indirect_placeholder_16_ret_type;
struct indirect_placeholder_15_ret_type;
struct indirect_placeholder_16_ret_type {
    uint64_t field_0;
    uint64_t field_1;
    uint64_t field_2;
    uint64_t field_3;
};
struct indirect_placeholder_15_ret_type {
    uint64_t field_0;
    uint64_t field_1;
    uint64_t field_2;
};
extern void abort(void);
extern uint64_t revng_init_local_sp(uint64_t param_0);
extern uint64_t init_rbx(void);
extern void indirect_placeholder(void);
extern uint64_t init_rbp(void);
extern uint64_t init_r14(void);
extern uint64_t init_r13(void);
extern uint64_t init_r12(void);
extern void indirect_placeholder_7(uint64_t param_0);
extern struct indirect_placeholder_16_ret_type indirect_placeholder_16(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint64_t param_3, uint64_t param_4);
extern void indirect_placeholder_10(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint64_t param_3, uint64_t param_4, uint64_t param_5, uint64_t param_6);
extern void indirect_placeholder_12(uint64_t param_0, uint64_t param_1, uint64_t param_2);
extern struct indirect_placeholder_15_ret_type indirect_placeholder_15(uint64_t param_0);
typedef _Bool bool;
void bb_main(uint64_t rdi, uint64_t rsi) {
    uint64_t var_10;
    uint32_t var_12;
    struct indirect_placeholder_16_ret_type var_11;
    uint64_t r13_0_ph88;
    uint64_t var_0;
    uint64_t var_1;
    uint64_t var_2;
    uint64_t var_3;
    uint64_t var_4;
    uint64_t var_5;
    uint32_t var_6;
    uint64_t var_7;
    uint64_t var_8;
    uint64_t var_9;
    uint64_t local_sp_2_ph;
    uint64_t r14_0_ph;
    uint64_t r13_0_ph;
    uint64_t r12_0_ph;
    uint64_t local_sp_2_ph87;
    uint64_t r12_0_ph89;
    uint64_t local_sp_2;
    uint64_t r12_0;
    uint64_t local_sp_0;
    uint64_t var_13;
    uint64_t var_14;
    uint64_t var_15;
    uint32_t var_16;
    uint64_t var_17;
    uint64_t var_18;
    uint64_t var_19;
    uint64_t var_35;
    uint64_t var_36;
    uint64_t var_37;
    uint64_t var_26;
    uint64_t var_30;
    struct indirect_placeholder_15_ret_type var_31;
    uint64_t var_32;
    uint64_t var_33;
    uint64_t var_34;
    uint64_t var_27;
    uint64_t _pre_phi113;
    uint64_t var_28;
    uint64_t rsi2_0;
    uint64_t var_29;
    uint64_t var_20;
    uint32_t var_21;
    uint64_t local_sp_1;
    uint64_t var_22;
    uint64_t var_23;
    uint64_t var_24;
    uint32_t var_25;
    unsigned int loop_state_var;
    bool switch_state_var;
    var_0 = revng_init_local_sp(0UL);
    var_1 = init_rbx();
    var_2 = init_r14();
    var_3 = init_rbp();
    var_4 = init_r13();
    var_5 = init_r12();
    *(uint64_t *)(var_0 + (-8L)) = var_2;
    *(uint64_t *)(var_0 + (-16L)) = var_4;
    *(uint64_t *)(var_0 + (-24L)) = var_5;
    *(uint64_t *)(var_0 + (-32L)) = var_3;
    *(uint64_t *)(var_0 + (-40L)) = var_1;
    var_6 = (uint32_t)rdi;
    var_7 = (uint64_t)var_6;
    var_8 = *(uint64_t *)rsi;
    *(uint64_t *)(var_0 + (-48L)) = 4201862UL;
    indirect_placeholder_7(var_8);
    *(uint64_t *)(var_0 + (-56L)) = 4201877UL;
    indirect_placeholder();
    var_9 = var_0 + (-64L);
    *(uint64_t *)var_9 = 4201887UL;
    indirect_placeholder();
    local_sp_2_ph = var_9;
    r14_0_ph = 0UL;
    r13_0_ph = 0UL;
    r12_0_ph = 0UL;
    rsi2_0 = 0UL;
    while (1U)
        {
            r12_0_ph = 1UL;
            local_sp_2_ph87 = local_sp_2_ph;
            r13_0_ph88 = r13_0_ph;
            r12_0_ph89 = r12_0_ph;
            while (1U)
                {
                    r13_0_ph = r13_0_ph88;
                    r13_0_ph88 = 1UL;
                    local_sp_2 = local_sp_2_ph87;
                    r12_0 = r12_0_ph89;
                    while (1U)
                        {
                            var_10 = local_sp_2 + (-8L);
                            *(uint64_t *)var_10 = 4201939UL;
                            var_11 = indirect_placeholder_16(4246417UL, var_7, 4247552UL, rsi, 0UL);
                            var_12 = (uint32_t)var_11.field_0;
                            local_sp_2_ph = var_10;
                            local_sp_2_ph87 = var_10;
                            r12_0_ph89 = r12_0;
                            local_sp_2 = var_10;
                            r12_0 = 1UL;
                            local_sp_0 = var_10;
                            local_sp_1 = var_10;
                            if ((uint64_t)(var_12 + 1U) == 0UL) {
                                if ((uint64_t)(var_12 + (-97)) == 0UL) {
                                    continue;
                                }
                                loop_state_var = 0U;
                                break;
                            }
                            var_16 = *(uint32_t *)6355260UL;
                            var_17 = (uint64_t)var_16;
                            var_18 = rdi << 32UL;
                            var_19 = var_17 << 32UL;
                            var_21 = var_16;
                            if ((long)var_18 < (long)(var_19 + 4294967296UL)) {
                                var_35 = var_11.field_3;
                                var_36 = var_11.field_2;
                                var_37 = var_11.field_1;
                                *(uint64_t *)(local_sp_2 + (-16L)) = 4202123UL;
                                indirect_placeholder_10(0UL, 4246439UL, 0UL, var_37, 0UL, var_36, var_35);
                                *(uint64_t *)(local_sp_2 + (-24L)) = 4202133UL;
                                indirect_placeholder_12(rsi, var_7, 1UL);
                                abort();
                            }
                            if ((uint64_t)(unsigned char)r12_0 != 0UL) {
                                var_20 = (uint64_t)(unsigned char)r13_0_ph88;
                                loop_state_var = 2U;
                                break;
                            }
                            var_26 = var_17 + 2UL;
                            if ((long)var_18 <= (long)(var_26 << 32UL)) {
                                var_27 = (uint64_t)(unsigned char)r13_0_ph88;
                                if ((uint64_t)(var_6 - (uint32_t)var_26) != 0UL) {
                                    _pre_phi113 = (uint64_t)((long)var_19 >> (long)29UL) + rsi;
                                    loop_state_var = 1U;
                                    break;
                                }
                                var_28 = (uint64_t)((long)var_19 >> (long)29UL) + rsi;
                                _pre_phi113 = var_28;
                                rsi2_0 = *(uint64_t *)(var_28 + 8UL);
                                loop_state_var = 1U;
                                break;
                            }
                            var_30 = *(uint64_t *)(((uint64_t)((long)var_19 >> (long)29UL) + rsi) + 16UL);
                            *(uint64_t *)(local_sp_2 + (-16L)) = 4202157UL;
                            var_31 = indirect_placeholder_15(var_30);
                            var_32 = var_31.field_0;
                            var_33 = var_31.field_1;
                            var_34 = var_31.field_2;
                            *(uint64_t *)(local_sp_2 + (-24L)) = 4202185UL;
                            indirect_placeholder_10(0UL, 4246455UL, 0UL, var_32, 0UL, var_33, var_34);
                            *(uint64_t *)(local_sp_2 + (-32L)) = 4202195UL;
                            indirect_placeholder_12(rsi, var_7, 1UL);
                            abort();
                        }
                    switch_state_var = 0;
                    switch (loop_state_var) {
                      case 0U:
                        {
                            if ((int)var_12 > (int)97U) {
                                if ((uint64_t)(var_12 + (-115)) != 0UL) {
                                    loop_state_var = 0U;
                                    switch_state_var = 1;
                                    break;
                                }
                                if ((uint64_t)(var_12 + (-122)) == 0UL) {
                                    continue;
                                }
                                loop_state_var = 1U;
                                switch_state_var = 1;
                                break;
                            }
                            if ((uint64_t)(var_12 + 131U) == 0UL) {
                                if ((uint64_t)(var_12 + 130U) != 0UL) {
                                    loop_state_var = 1U;
                                    switch_state_var = 1;
                                    break;
                                }
                                *(uint64_t *)(local_sp_2 + (-16L)) = 4202019UL;
                                indirect_placeholder_12(rsi, var_7, 0UL);
                                abort();
                            }
                            var_13 = *(uint64_t *)6355136UL;
                            var_14 = *(uint64_t *)6355264UL;
                            *(uint64_t *)(local_sp_2 + (-16L)) = 4202065UL;
                            indirect_placeholder_10(0UL, 4246376UL, var_14, var_13, 4246272UL, 0UL, 4246423UL);
                            var_15 = local_sp_2 + (-24L);
                            *(uint64_t *)var_15 = 4202075UL;
                            indirect_placeholder();
                            local_sp_0 = var_15;
                            loop_state_var = 1U;
                            switch_state_var = 1;
                            break;
                        }
                        break;
                      case 1U:
                        {
                            var_29 = *(uint64_t *)_pre_phi113;
                            *(uint64_t *)(local_sp_2 + (-16L)) = 4202269UL;
                            indirect_placeholder_12(var_27, var_29, rsi2_0);
                            loop_state_var = 2U;
                            switch_state_var = 1;
                            break;
                        }
                        break;
                      case 2U:
                        {
                            loop_state_var = 3U;
                            switch_state_var = 1;
                            break;
                        }
                        break;
                    }
                    if (switch_state_var)
                        break;
                }
            switch_state_var = 0;
            switch (loop_state_var) {
              case 1U:
                {
                    *(uint64_t *)(local_sp_0 + (-8L)) = 4202085UL;
                    indirect_placeholder_12(rsi, var_7, 1UL);
                    abort();
                }
                break;
              case 0U:
                {
                    r14_0_ph = *(uint64_t *)6355864UL;
                    continue;
                }
                break;
              case 2U:
                {
                    loop_state_var = 0U;
                    switch_state_var = 1;
                    break;
                }
                break;
              case 3U:
                {
                    loop_state_var = 1U;
                    switch_state_var = 1;
                    break;
                }
                break;
            }
            if (switch_state_var)
                break;
        }
    switch (loop_state_var) {
      case 0U:
        {
            return;
        }
        break;
      case 1U:
        {
            var_22 = (uint64_t)var_21 << 32UL;
            while ((long)var_18 <= (long)var_22)
                {
                    var_23 = *(uint64_t *)((uint64_t)((long)var_22 >> (long)29UL) + rsi);
                    var_24 = local_sp_1 + (-8L);
                    *(uint64_t *)var_24 = 4202212UL;
                    indirect_placeholder_12(var_20, var_23, r14_0_ph);
                    var_25 = *(uint32_t *)6355260UL + 1U;
                    *(uint32_t *)6355260UL = var_25;
                    var_21 = var_25;
                    local_sp_1 = var_24;
                    var_22 = (uint64_t)var_21 << 32UL;
                }
        }
        break;
    }
}

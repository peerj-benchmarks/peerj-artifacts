typedef unsigned char __u_char;
typedef unsigned short __u_short;
typedef unsigned int __u_int;
typedef unsigned long __u_long;
typedef signed char __int8_t;
typedef unsigned char __uint8_t;
typedef short __int16_t;
typedef unsigned short __uint16_t;
typedef int __int32_t;
typedef unsigned int __uint32_t;
typedef long __int64_t;
typedef unsigned long __uint64_t;
typedef __int8_t __int_least8_t;
typedef __uint8_t __uint_least8_t;
typedef __int16_t __int_least16_t;
typedef __uint16_t __uint_least16_t;
typedef __int32_t __int_least32_t;
typedef __uint32_t __uint_least32_t;
typedef __int64_t __int_least64_t;
typedef __uint64_t __uint_least64_t;
typedef long __quad_t;
typedef unsigned long __u_quad_t;
typedef long __intmax_t;
typedef unsigned long __uintmax_t;
typedef unsigned long __dev_t;
typedef unsigned int __uid_t;
typedef unsigned int __gid_t;
typedef unsigned long __ino_t;
typedef unsigned long __ino64_t;
typedef unsigned int __mode_t;
typedef unsigned long __nlink_t;
typedef long __off_t;
typedef long __off64_t;
typedef int __pid_t;
typedef struct {
    int __val[2];
} __fsid_t;
typedef long __clock_t;
typedef unsigned long __rlim_t;
typedef unsigned long __rlim64_t;
typedef unsigned int __id_t;
typedef long __time_t;
typedef unsigned int __useconds_t;
typedef long __suseconds_t;
typedef int __daddr_t;
typedef int __key_t;
typedef int __clockid_t;
typedef void *__timer_t;
typedef long __blksize_t;
typedef long __blkcnt_t;
typedef long __blkcnt64_t;
typedef unsigned long __fsblkcnt_t;
typedef unsigned long __fsblkcnt64_t;
typedef unsigned long __fsfilcnt_t;
typedef unsigned long __fsfilcnt64_t;
typedef long __fsword_t;
typedef long __ssize_t;
typedef long __syscall_slong_t;
typedef unsigned long __syscall_ulong_t;
typedef __off64_t __loff_t;
typedef char *__caddr_t;
typedef long __intptr_t;
typedef unsigned int __socklen_t;
typedef int __sig_atomic_t;
typedef __int8_t int8_t;
typedef __int16_t int16_t;
typedef __int32_t int32_t;
typedef __int64_t int64_t;
typedef __uint8_t uint8_t;
typedef __uint16_t uint16_t;
typedef __uint32_t uint32_t;
typedef __uint64_t uint64_t;
typedef __int_least8_t int_least8_t;
typedef __int_least16_t int_least16_t;
typedef __int_least32_t int_least32_t;
typedef __int_least64_t int_least64_t;
typedef __uint_least8_t uint_least8_t;
typedef __uint_least16_t uint_least16_t;
typedef __uint_least32_t uint_least32_t;
typedef __uint_least64_t uint_least64_t;
typedef signed char int_fast8_t;
typedef long int_fast16_t;
typedef long int_fast32_t;
typedef long int_fast64_t;
typedef unsigned char uint_fast8_t;
typedef unsigned long uint_fast16_t;
typedef unsigned long uint_fast32_t;
typedef unsigned long uint_fast64_t;
typedef long intptr_t;
typedef unsigned long uintptr_t;
typedef __intmax_t intmax_t;
typedef __uintmax_t uintmax_t;
typedef _Bool bool;
struct indirect_placeholder_49_ret_type;
struct indirect_placeholder_48_ret_type;
struct indirect_placeholder_63_ret_type;
struct indirect_placeholder_62_ret_type;
struct indirect_placeholder_67_ret_type;
struct indirect_placeholder_66_ret_type;
struct indirect_placeholder_49_ret_type {
    uint64_t field_0;
    uint64_t field_1;
    uint64_t field_2;
};
struct indirect_placeholder_48_ret_type {
    uint64_t field_0;
    uint64_t field_1;
    uint64_t field_2;
    uint64_t field_3;
};
struct indirect_placeholder_63_ret_type {
    uint64_t field_0;
    uint64_t field_1;
    uint64_t field_2;
};
struct indirect_placeholder_62_ret_type {
    uint64_t field_0;
    uint64_t field_1;
    uint64_t field_2;
    uint64_t field_3;
};
struct indirect_placeholder_67_ret_type {
    uint64_t field_0;
    uint64_t field_1;
    uint64_t field_2;
};
struct indirect_placeholder_66_ret_type {
    uint64_t field_0;
    uint64_t field_1;
    uint64_t field_2;
    uint64_t field_3;
};
extern void abort(void);
extern uint64_t revng_init_local_sp(uint64_t param_0);
extern uint64_t init_rbx(void);
extern void indirect_placeholder(void);
extern uint64_t init_rbp(void);
extern uint64_t init_cc_src2(void);
extern uint64_t helper_cc_compute_c_wrapper(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint32_t param_3);
extern uint64_t init_r14(void);
extern uint64_t init_r13(void);
extern uint64_t init_r12(void);
extern uint64_t indirect_placeholder_10(uint64_t param_0);
extern uint64_t init_r15(void);
extern void indirect_placeholder_8(uint64_t param_0, uint64_t param_1, uint64_t param_2);
extern struct indirect_placeholder_49_ret_type indirect_placeholder_49(uint64_t param_0);
extern struct indirect_placeholder_48_ret_type indirect_placeholder_48(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint64_t param_3, uint64_t param_4, uint64_t param_5, uint64_t param_6);
extern struct indirect_placeholder_63_ret_type indirect_placeholder_63(uint64_t param_0);
extern struct indirect_placeholder_62_ret_type indirect_placeholder_62(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint64_t param_3, uint64_t param_4, uint64_t param_5, uint64_t param_6);
extern struct indirect_placeholder_67_ret_type indirect_placeholder_67(uint64_t param_0);
extern struct indirect_placeholder_66_ret_type indirect_placeholder_66(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint64_t param_3, uint64_t param_4, uint64_t param_5, uint64_t param_6);
void bb_apply_settings(uint64_t rdx, uint64_t rdi, uint64_t rcx, uint64_t r8, uint64_t rsi, uint64_t r9) {
    uint64_t var_30;
    uint64_t var_31;
    uint64_t var_32;
    uint64_t var_0;
    uint64_t var_1;
    uint64_t var_2;
    uint64_t var_3;
    uint64_t var_4;
    uint64_t var_5;
    uint64_t var_6;
    uint64_t var_7;
    uint32_t *var_8;
    uint32_t var_9;
    uint32_t var_72;
    uint32_t *var_73;
    uint64_t local_sp_14;
    uint64_t var_21;
    uint64_t *var_22;
    uint32_t var_85;
    uint32_t *var_86;
    uint64_t local_sp_0;
    uint64_t r15_0;
    uint64_t r12_1;
    uint64_t rax_0;
    uint64_t *var_50;
    uint64_t var_51;
    uint64_t rbp_0;
    uint64_t rbx_0;
    uint64_t r13_0;
    uint64_t var_52;
    uint64_t local_sp_22;
    uint64_t var_53;
    uint64_t var_54;
    uint64_t var_55;
    uint64_t *var_34;
    uint64_t local_sp_1;
    uint64_t rax_1;
    uint64_t *var_38;
    uint64_t var_39;
    uint64_t rbx_2;
    uint64_t r13_1;
    uint64_t var_40;
    uint64_t var_41;
    uint32_t var_42;
    uint64_t var_43;
    uint32_t var_44;
    uint64_t var_45;
    uint64_t cc_src2_0;
    uint64_t *var_46;
    uint32_t *var_47;
    uint64_t var_48;
    uint64_t *var_49;
    uint64_t rbx_1;
    uint64_t rbp_1;
    uint64_t r12_0;
    struct indirect_placeholder_49_ret_type var_56;
    uint64_t var_57;
    uint64_t var_58;
    uint64_t var_59;
    uint64_t rax_5;
    uint64_t var_33;
    uint64_t local_sp_2;
    uint64_t cc_src2_1;
    uint64_t var_11;
    uint64_t var_12;
    uint64_t var_13;
    uint32_t *var_35;
    uint64_t var_36;
    uint64_t *var_37;
    uint32_t var_60;
    uint32_t var_61;
    uint64_t var_62;
    uint64_t var_63;
    uint64_t var_64;
    struct indirect_placeholder_63_ret_type var_65;
    uint64_t var_66;
    uint64_t var_67;
    uint64_t var_68;
    uint64_t r13_3;
    uint64_t local_sp_17;
    uint64_t rbx_7;
    uint64_t var_74;
    uint32_t *var_75;
    uint32_t var_76;
    uint64_t var_87;
    uint32_t *var_88;
    uint32_t var_89;
    uint64_t var_27;
    uint64_t var_28;
    uint64_t local_sp_16;
    uint64_t rbx_5;
    uint64_t var_69;
    uint64_t var_70;
    uint64_t var_71;
    uint64_t var_82;
    uint64_t var_83;
    uint64_t var_84;
    uint64_t rbp_6;
    uint64_t rbx_4;
    uint64_t rbp_3;
    uint64_t var_29;
    uint64_t var_23;
    uint64_t var_24;
    uint64_t rbp_4;
    uint64_t var_77;
    struct indirect_placeholder_67_ret_type var_78;
    uint64_t var_79;
    uint64_t var_80;
    uint64_t var_81;
    uint64_t rbp_5;
    uint64_t r13_2;
    uint64_t var_25;
    uint64_t var_26;
    uint64_t local_sp_20;
    uint32_t storemerge;
    uint64_t var_19;
    bool var_20;
    uint64_t var_10;
    uint64_t r15_1;
    uint64_t local_sp_21;
    uint64_t var_14;
    uint64_t var_15;
    uint64_t cc_src2_2;
    bool var_16;
    uint64_t var_17;
    uint64_t *var_18;
    uint64_t var_90;
    unsigned int loop_state_var;
    bool switch_state_var;
    var_0 = revng_init_local_sp(0UL);
    var_1 = init_rbx();
    var_2 = init_r14();
    var_3 = init_rbp();
    var_4 = init_r13();
    var_5 = init_r12();
    var_6 = init_r15();
    var_7 = init_cc_src2();
    *(uint64_t *)(var_0 + (-8L)) = var_6;
    *(uint64_t *)(var_0 + (-16L)) = var_2;
    *(uint64_t *)(var_0 + (-24L)) = var_4;
    *(uint64_t *)(var_0 + (-32L)) = var_5;
    *(uint64_t *)(var_0 + (-40L)) = var_3;
    *(uint64_t *)(var_0 + (-48L)) = var_1;
    *(uint64_t *)(var_0 + (-96L)) = rsi;
    var_8 = (uint32_t *)(var_0 + (-124L));
    var_9 = (uint32_t)rcx;
    *var_8 = var_9;
    *(uint64_t *)(var_0 + (-120L)) = r8;
    *(uint64_t *)(var_0 + (-104L)) = r9;
    *(unsigned char *)(var_0 + (-105L)) = (unsigned char)rdi;
    r15_0 = 1UL;
    rbp_0 = 0UL;
    rbx_0 = 58UL;
    rbx_2 = 0UL;
    cc_src2_1 = var_7;
    r13_3 = 0UL;
    rbx_7 = 4257790UL;
    rbp_6 = 4260576UL;
    rbp_5 = 4260056UL;
    r13_2 = 0UL;
    storemerge = 1U;
    if ((int)var_9 > (int)1U) {
        return;
    }
    var_10 = var_0 + (-136L);
    *(uint32_t *)(var_0 + (-112L)) = (var_9 + (-1));
    local_sp_21 = var_10;
    while (1U)
        {
            var_11 = r15_0 << 32UL;
            var_12 = (uint64_t)((long)var_11 >> (long)32UL);
            var_13 = (uint64_t)((long)var_11 >> (long)29UL);
            *(uint64_t *)local_sp_21 = var_13;
            var_14 = var_13 + rdx;
            var_15 = *(uint64_t *)var_14;
            r12_1 = var_15;
            local_sp_22 = local_sp_21;
            cc_src2_0 = cc_src2_1;
            rax_5 = var_12;
            r15_1 = r15_0;
            cc_src2_2 = cc_src2_1;
            if (var_15 == 0UL) {
                var_90 = r15_1 + 1UL;
                local_sp_21 = local_sp_22;
                cc_src2_1 = cc_src2_2;
                if ((long)(uint64_t)((long)(var_90 << 32UL) >> (long)32UL) >= (long)(uint64_t)*(uint32_t *)(local_sp_22 + 12UL)) {
                    loop_state_var = 3U;
                    break;
                }
                r15_0 = (uint64_t)(uint32_t)var_90;
                continue;
            }
            var_16 = (*(unsigned char *)var_15 == '-');
            var_17 = local_sp_21 + (-8L);
            var_18 = (uint64_t *)var_17;
            local_sp_20 = var_17;
            local_sp_22 = var_17;
            r15_1 = 0UL;
            if (var_16) {
                *var_18 = 4205092UL;
                indirect_placeholder();
                storemerge = 0U;
                if (r15_0 != 0UL) {
                    *(uint32_t *)6382624UL = storemerge;
                    var_90 = r15_1 + 1UL;
                    local_sp_21 = local_sp_22;
                    cc_src2_1 = cc_src2_2;
                    if ((long)(uint64_t)((long)(var_90 << 32UL) >> (long)32UL) >= (long)(uint64_t)*(uint32_t *)(local_sp_22 + 12UL)) {
                        loop_state_var = 3U;
                        break;
                    }
                    r15_0 = (uint64_t)(uint32_t)var_90;
                    continue;
                }
                var_19 = var_15 + 1UL;
                *(unsigned char *)(local_sp_21 + 22UL) = (unsigned char)'\x01';
                r12_1 = var_19;
            } else {
                *var_18 = 4207978UL;
                indirect_placeholder();
                if (r15_0 != 0UL) {
                    *(uint32_t *)6382624UL = storemerge;
                    var_90 = r15_1 + 1UL;
                    local_sp_21 = local_sp_22;
                    cc_src2_1 = cc_src2_2;
                    if ((long)(uint64_t)((long)(var_90 << 32UL) >> (long)32UL) >= (long)(uint64_t)*(uint32_t *)(local_sp_22 + 12UL)) {
                        loop_state_var = 3U;
                        break;
                    }
                    r15_0 = (uint64_t)(uint32_t)var_90;
                    continue;
                }
                *(unsigned char *)(local_sp_21 + 22UL) = (unsigned char)'\x00';
            }
            var_20 = (r15_0 == 0UL);
            r12_0 = r12_1;
            r13_1 = r12_1;
            while (1U)
                {
                    var_21 = local_sp_20 + (-8L);
                    var_22 = (uint64_t *)var_21;
                    *var_22 = 4205157UL;
                    indirect_placeholder();
                    local_sp_17 = var_21;
                    local_sp_16 = var_21;
                    rbx_5 = rbx_7;
                    rbx_4 = rbx_7;
                    rbp_3 = rbp_6;
                    rbp_4 = rbp_6;
                    local_sp_20 = var_21;
                    if (!var_20) {
                        loop_state_var = 0U;
                        break;
                    }
                    var_23 = rbp_6 + 32UL;
                    var_24 = *(uint64_t *)rbp_6;
                    rbx_7 = var_24;
                    rbx_5 = 0UL;
                    rbp_6 = var_23;
                    rbx_4 = 0UL;
                    rbp_4 = var_23;
                    if (var_24 != 0UL) {
                        loop_state_var = 1U;
                        break;
                    }
                    r13_3 = (uint64_t)((uint32_t)r13_3 + 1U);
                    continue;
                }
            switch_state_var = 0;
            switch (loop_state_var) {
              case 0U:
                {
                    var_27 = (uint64_t)((long)(r13_3 << 32UL) >> (long)27UL);
                    var_28 = (uint64_t)*(unsigned char *)(var_27 + 4260556UL);
                    rax_5 = var_28;
                    if ((var_28 & 16UL) != 0UL) {
                        if (*(unsigned char *)(local_sp_20 + 22UL) == '\x00') {
                            var_82 = (uint64_t)*(uint32_t *)(var_27 + 4260552UL);
                            var_83 = local_sp_20 + (-16L);
                            *(uint64_t *)var_83 = 4208019UL;
                            var_84 = indirect_placeholder_10(var_82);
                            local_sp_14 = var_83;
                            if (var_84 == 0UL) {
                                var_87 = local_sp_20 + (-24L);
                                *(uint64_t *)var_87 = 4208206UL;
                                indirect_placeholder();
                                var_88 = (uint32_t *)(*var_22 + 8UL);
                                var_89 = *var_88;
                                *(uint32_t *)var_87 = var_89;
                                *var_88 = ((var_89 & (-817)) | 288U);
                                local_sp_14 = var_87;
                            } else {
                                var_85 = *(uint32_t *)(var_27 + 4260568UL) ^ (-1);
                                var_86 = (uint32_t *)var_84;
                                *var_86 = ((*var_86 & var_85) | *(uint32_t *)(var_27 + 4260560UL));
                            }
                        } else {
                            if ((var_28 & 4UL) != 0UL) {
                                **(unsigned char **)(local_sp_20 + 136UL) = (unsigned char)'\x01';
                                loop_state_var = 2U;
                                switch_state_var = 1;
                                break;
                            }
                            var_69 = (uint64_t)*(uint32_t *)(var_27 + 4260552UL);
                            var_70 = local_sp_20 + (-16L);
                            *(uint64_t *)var_70 = 4208056UL;
                            var_71 = indirect_placeholder_10(var_69);
                            local_sp_14 = var_70;
                            if (var_71 == 0UL) {
                                var_74 = local_sp_20 + (-24L);
                                *(uint64_t *)var_74 = 4208180UL;
                                indirect_placeholder();
                                var_75 = (uint32_t *)(*var_22 + 8UL);
                                var_76 = *var_75;
                                *(uint32_t *)var_74 = var_76;
                                *var_75 = ((var_76 & (-305)) | 48U);
                                local_sp_14 = var_74;
                            } else {
                                var_72 = (*(uint32_t *)(var_27 + 4260560UL) | *(uint32_t *)(var_27 + 4260568UL)) ^ (-1);
                                var_73 = (uint32_t *)var_71;
                                *var_73 = (*var_73 & var_72);
                            }
                        }
                        **(unsigned char **)(local_sp_14 + 144UL) = (unsigned char)'\x01';
                        local_sp_22 = local_sp_14;
                        var_90 = r15_1 + 1UL;
                        local_sp_21 = local_sp_22;
                        cc_src2_1 = cc_src2_2;
                        if ((long)(uint64_t)((long)(var_90 << 32UL) >> (long)32UL) >= (long)(uint64_t)*(uint32_t *)(local_sp_22 + 12UL)) {
                            loop_state_var = 3U;
                            switch_state_var = 1;
                            break;
                        }
                        r15_0 = (uint64_t)(uint32_t)var_90;
                        continue;
                    }
                }
                break;
              case 1U:
                {
                    if (*(unsigned char *)(local_sp_20 + 22UL) != '\x00') {
                        loop_state_var = 2U;
                        switch_state_var = 1;
                        break;
                    }
                    var_25 = local_sp_17 + (-8L);
                    *(uint64_t *)var_25 = 4206424UL;
                    indirect_placeholder();
                    var_26 = rbp_5 + 24UL;
                    local_sp_16 = var_25;
                    rbp_3 = var_26;
                    local_sp_17 = var_25;
                    rbp_5 = var_26;
                    while (*(uint64_t *)rbp_5 != 0UL)
                        {
                            r13_2 = (uint64_t)((uint32_t)r13_2 + 1U);
                            var_25 = local_sp_17 + (-8L);
                            *(uint64_t *)var_25 = 4206424UL;
                            indirect_placeholder();
                            var_26 = rbp_5 + 24UL;
                            local_sp_16 = var_25;
                            rbp_3 = var_26;
                            local_sp_17 = var_25;
                            rbp_5 = var_26;
                        }
                    var_29 = local_sp_16 + (-8L);
                    *(uint64_t *)var_29 = 4206743UL;
                    indirect_placeholder();
                    local_sp_22 = var_29;
                    if ((uint64_t)(uint32_t)rax_5 == 0UL) {
                        var_60 = *(uint32_t *)(local_sp_16 + 16UL);
                        var_61 = (uint32_t)r15_0;
                        if ((uint64_t)(var_60 - var_61) != 0UL) {
                            loop_state_var = 0U;
                            switch_state_var = 1;
                            break;
                        }
                        var_62 = *(uint64_t *)(var_14 + 8UL);
                        if (var_62 != 0UL) {
                            loop_state_var = 0U;
                            switch_state_var = 1;
                            break;
                        }
                        var_63 = (uint64_t)(var_61 + 1U);
                        r15_1 = var_63;
                        if (*(unsigned char *)(local_sp_16 + 23UL) == '\x00') {
                            *(uint64_t *)(local_sp_16 + (-16L)) = 4206833UL;
                            indirect_placeholder_10(var_62);
                            var_64 = local_sp_16 + (-24L);
                            *(uint64_t *)var_64 = 4206845UL;
                            indirect_placeholder();
                            **(unsigned char **)(local_sp_16 + 8UL) = (unsigned char)'\x01';
                            **(unsigned char **)(local_sp_16 + 120UL) = (unsigned char)'\x01';
                            local_sp_22 = var_64;
                        }
                    } else {
                        *(uint64_t *)(local_sp_16 + (-16L)) = 4206882UL;
                        indirect_placeholder();
                        *(uint64_t *)(local_sp_16 + (-24L)) = 4207021UL;
                        indirect_placeholder();
                        *(uint64_t *)(local_sp_16 + (-32L)) = 4207151UL;
                        indirect_placeholder();
                        *(uint64_t *)(local_sp_16 + (-40L)) = 4207168UL;
                        indirect_placeholder();
                        *(uint64_t *)(local_sp_16 + (-48L)) = 4207298UL;
                        indirect_placeholder();
                        var_30 = local_sp_16 + (-56L);
                        *(uint64_t *)var_30 = 4207367UL;
                        indirect_placeholder();
                        *(uint64_t *)(local_sp_16 + (-64L)) = 4207541UL;
                        indirect_placeholder();
                        var_31 = local_sp_16 + (-72L);
                        *(uint64_t *)var_31 = 4207595UL;
                        var_32 = indirect_placeholder_10(r12_1);
                        local_sp_2 = var_31;
                        rax_1 = var_32;
                        local_sp_22 = var_31;
                        if ((uint64_t)((uint32_t)var_32 + 1U) == 0UL) {
                            if (*(unsigned char *)(local_sp_16 + (-41L)) == '\x00') {
                                *(uint64_t *)(local_sp_16 + (-80L)) = 4207628UL;
                                indirect_placeholder();
                                var_33 = local_sp_16 + (-88L);
                                *(uint64_t *)var_33 = 4207638UL;
                                indirect_placeholder();
                                **(unsigned char **)var_30 = (unsigned char)'\x01';
                                **(unsigned char **)(local_sp_16 + 56UL) = (unsigned char)'\x01';
                                local_sp_22 = var_33;
                            }
                        } else {
                            while (1U)
                                {
                                    var_34 = (uint64_t *)(local_sp_2 + (-8L));
                                    *var_34 = 4207675UL;
                                    indirect_placeholder();
                                    var_35 = (uint32_t *)rax_1;
                                    *var_35 = 0U;
                                    *(uint64_t *)(local_sp_2 + (-16L)) = 4207699UL;
                                    indirect_placeholder();
                                    var_36 = local_sp_2 + (-24L);
                                    var_37 = (uint64_t *)var_36;
                                    *var_37 = 4207707UL;
                                    indirect_placeholder();
                                    local_sp_0 = var_36;
                                    local_sp_1 = var_36;
                                    rbx_1 = rbx_2;
                                    rbp_1 = rax_1;
                                    local_sp_2 = var_36;
                                    if (*var_35 != 0U) {
                                        loop_state_var = 0U;
                                        break;
                                    }
                                    var_38 = (uint64_t *)(local_sp_2 + 32UL);
                                    var_39 = *var_38;
                                    if (r13_1 != var_39) {
                                        loop_state_var = 0U;
                                        break;
                                    }
                                    if (!((*(unsigned char *)var_39 == ':') && (rax_1 < 4294967296UL))) {
                                        loop_state_var = 0U;
                                        break;
                                    }
                                    *(uint32_t *)(((rbx_2 << 2UL) + var_36) + 64UL) = (uint32_t)rax_1;
                                    var_40 = *var_38;
                                    var_41 = var_40 + 1UL;
                                    r13_0 = var_41;
                                    rax_1 = var_40;
                                    r13_1 = var_41;
                                    if (rbx_2 != 3UL) {
                                        loop_state_var = 1U;
                                        break;
                                    }
                                    rbx_2 = rbx_2 + 1UL;
                                    continue;
                                }
                            switch_state_var = 0;
                            switch (loop_state_var) {
                              case 1U:
                                {
                                    var_42 = *(uint32_t *)(local_sp_2 + 40UL);
                                    var_43 = *var_34;
                                    *(uint32_t *)var_43 = var_42;
                                    *(uint32_t *)(var_43 + 4UL) = *(uint32_t *)(local_sp_2 + 44UL);
                                    *(uint32_t *)(var_43 + 8UL) = *(uint32_t *)(local_sp_2 + 48UL);
                                    var_44 = *(uint32_t *)(local_sp_2 + 52UL);
                                    var_45 = (uint64_t)var_44;
                                    *(uint32_t *)(var_43 + 12UL) = var_44;
                                    *var_37 = r12_1;
                                    rax_0 = var_45;
                                    while (1U)
                                        {
                                            var_46 = (uint64_t *)(local_sp_0 + (-8L));
                                            *var_46 = 4207835UL;
                                            indirect_placeholder();
                                            var_47 = (uint32_t *)rax_0;
                                            *var_47 = 0U;
                                            *(uint64_t *)(local_sp_0 + (-16L)) = 4207859UL;
                                            indirect_placeholder();
                                            var_48 = local_sp_0 + (-24L);
                                            var_49 = (uint64_t *)var_48;
                                            *var_49 = 4207867UL;
                                            indirect_placeholder();
                                            local_sp_0 = var_48;
                                            local_sp_22 = var_48;
                                            local_sp_1 = var_48;
                                            rbx_1 = rbx_0;
                                            rbp_1 = rbp_0;
                                            cc_src2_2 = cc_src2_0;
                                            if (*var_47 != 0U) {
                                                r12_0 = *var_49;
                                                loop_state_var = 1U;
                                                break;
                                            }
                                            var_50 = (uint64_t *)(local_sp_0 + 32UL);
                                            var_51 = *var_50;
                                            if (r13_0 != var_51) {
                                                r12_0 = *var_49;
                                                loop_state_var = 1U;
                                                break;
                                            }
                                            if ((uint64_t)((unsigned char)rbx_0 - *(unsigned char *)var_51) != 0UL) {
                                                r12_0 = *var_49;
                                                loop_state_var = 1U;
                                                break;
                                            }
                                            if (rax_0 >= 256UL) {
                                                r12_0 = *var_49;
                                                loop_state_var = 1U;
                                                break;
                                            }
                                            *(unsigned char *)((rbp_0 + *var_46) + 17UL) = (unsigned char)rax_0;
                                            var_52 = *var_50;
                                            rax_0 = var_52;
                                            if (rbp_0 != 31UL) {
                                                loop_state_var = 0U;
                                                break;
                                            }
                                            var_53 = rbp_0 + 1UL;
                                            var_54 = var_52 + 1UL;
                                            var_55 = helper_cc_compute_c_wrapper(rbp_0 + (-30L), 31UL, cc_src2_0, 17U);
                                            rbx_0 = (0UL - var_55) & 58UL;
                                            rbp_0 = var_53;
                                            r13_0 = var_54;
                                            cc_src2_0 = var_55;
                                            continue;
                                        }
                                    switch_state_var = 0;
                                    switch (loop_state_var) {
                                      case 0U:
                                        {
                                            **(unsigned char **)(local_sp_0 + 120UL) = (unsigned char)'\x01';
                                        }
                                        break;
                                      case 1U:
                                        {
                                            loop_state_var = 1U;
                                            switch_state_var = 1;
                                            break;
                                        }
                                        break;
                                    }
                                    if (switch_state_var)
                                        break;
                                }
                                break;
                              case 0U:
                                {
                                    loop_state_var = 1U;
                                    switch_state_var = 1;
                                    break;
                                }
                                break;
                            }
                            if (switch_state_var)
                                break;
                        }
                    }
                }
                break;
            }
            if (switch_state_var)
                break;
        }
    switch (loop_state_var) {
      case 0U:
        {
            *(uint64_t *)(local_sp_16 + (-16L)) = 4206775UL;
            var_65 = indirect_placeholder_63(r12_1);
            var_66 = var_65.field_0;
            var_67 = var_65.field_1;
            var_68 = var_65.field_2;
            *(uint64_t *)(local_sp_16 + (-24L)) = 4206803UL;
            indirect_placeholder_62(0UL, 4257911UL, 0UL, var_66, var_67, 0UL, var_68);
            *(uint64_t *)(local_sp_16 + (-32L)) = 4206813UL;
            indirect_placeholder_8(rbx_4, rbp_3, 1UL);
            abort();
        }
        break;
      case 1U:
        {
            *(uint64_t *)(local_sp_1 + (-8L)) = 4208113UL;
            var_56 = indirect_placeholder_49(r12_0);
            var_57 = var_56.field_0;
            var_58 = var_56.field_1;
            var_59 = var_56.field_2;
            *(uint64_t *)(local_sp_1 + (-16L)) = 4208141UL;
            indirect_placeholder_48(0UL, 4257891UL, 0UL, var_57, var_58, 0UL, var_59);
            *(uint64_t *)(local_sp_1 + (-24L)) = 4208151UL;
            indirect_placeholder_8(rbx_1, rbp_1, 1UL);
            abort();
        }
        break;
      case 2U:
        {
            var_77 = r12_1 + (-1L);
            *(uint64_t *)(local_sp_20 + (-16L)) = 4206359UL;
            var_78 = indirect_placeholder_67(var_77);
            var_79 = var_78.field_0;
            var_80 = var_78.field_1;
            var_81 = var_78.field_2;
            *(uint64_t *)(local_sp_20 + (-24L)) = 4206387UL;
            indirect_placeholder_66(0UL, 4257891UL, 0UL, var_79, var_80, 0UL, var_81);
            *(uint64_t *)(local_sp_20 + (-32L)) = 4206397UL;
            indirect_placeholder_8(rbx_5, rbp_4, 1UL);
            abort();
        }
        break;
      case 3U:
        {
            return;
        }
        break;
    }
}

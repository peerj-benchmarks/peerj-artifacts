typedef unsigned char __u_char;
typedef unsigned short __u_short;
typedef unsigned int __u_int;
typedef unsigned long __u_long;
typedef signed char __int8_t;
typedef unsigned char __uint8_t;
typedef short __int16_t;
typedef unsigned short __uint16_t;
typedef int __int32_t;
typedef unsigned int __uint32_t;
typedef long __int64_t;
typedef unsigned long __uint64_t;
typedef __int8_t __int_least8_t;
typedef __uint8_t __uint_least8_t;
typedef __int16_t __int_least16_t;
typedef __uint16_t __uint_least16_t;
typedef __int32_t __int_least32_t;
typedef __uint32_t __uint_least32_t;
typedef __int64_t __int_least64_t;
typedef __uint64_t __uint_least64_t;
typedef long __quad_t;
typedef unsigned long __u_quad_t;
typedef long __intmax_t;
typedef unsigned long __uintmax_t;
typedef unsigned long __dev_t;
typedef unsigned int __uid_t;
typedef unsigned int __gid_t;
typedef unsigned long __ino_t;
typedef unsigned long __ino64_t;
typedef unsigned int __mode_t;
typedef unsigned long __nlink_t;
typedef long __off_t;
typedef long __off64_t;
typedef int __pid_t;
typedef struct {
    int __val[2];
} __fsid_t;
typedef long __clock_t;
typedef unsigned long __rlim_t;
typedef unsigned long __rlim64_t;
typedef unsigned int __id_t;
typedef long __time_t;
typedef unsigned int __useconds_t;
typedef long __suseconds_t;
typedef int __daddr_t;
typedef int __key_t;
typedef int __clockid_t;
typedef void *__timer_t;
typedef long __blksize_t;
typedef long __blkcnt_t;
typedef long __blkcnt64_t;
typedef unsigned long __fsblkcnt_t;
typedef unsigned long __fsblkcnt64_t;
typedef unsigned long __fsfilcnt_t;
typedef unsigned long __fsfilcnt64_t;
typedef long __fsword_t;
typedef long __ssize_t;
typedef long __syscall_slong_t;
typedef unsigned long __syscall_ulong_t;
typedef __off64_t __loff_t;
typedef char *__caddr_t;
typedef long __intptr_t;
typedef unsigned int __socklen_t;
typedef int __sig_atomic_t;
typedef __int8_t int8_t;
typedef __int16_t int16_t;
typedef __int32_t int32_t;
typedef __int64_t int64_t;
typedef __uint8_t uint8_t;
typedef __uint16_t uint16_t;
typedef __uint32_t uint32_t;
typedef __uint64_t uint64_t;
typedef __int_least8_t int_least8_t;
typedef __int_least16_t int_least16_t;
typedef __int_least32_t int_least32_t;
typedef __int_least64_t int_least64_t;
typedef __uint_least8_t uint_least8_t;
typedef __uint_least16_t uint_least16_t;
typedef __uint_least32_t uint_least32_t;
typedef __uint_least64_t uint_least64_t;
typedef signed char int_fast8_t;
typedef long int_fast16_t;
typedef long int_fast32_t;
typedef long int_fast64_t;
typedef unsigned char uint_fast8_t;
typedef unsigned long uint_fast16_t;
typedef unsigned long uint_fast32_t;
typedef unsigned long uint_fast64_t;
typedef long intptr_t;
typedef unsigned long uintptr_t;
typedef __intmax_t intmax_t;
typedef __uintmax_t uintmax_t;
struct indirect_placeholder_12_ret_type;
struct indirect_placeholder_12_ret_type {
    uint64_t field_0;
    uint64_t field_1;
};
extern void abort(void);
extern uint64_t revng_init_local_sp(uint64_t param_0);
extern uint64_t init_rbx(void);
extern void indirect_placeholder_1(void);
extern uint64_t init_rbp(void);
extern uint64_t init_cc_src2(void);
extern uint64_t helper_cc_compute_c_wrapper(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint32_t param_3);
extern uint64_t init_r14(void);
extern uint64_t init_r13(void);
extern uint64_t init_r12(void);
extern uint64_t init_r15(void);
extern void indirect_placeholder_11(uint64_t param_0);
extern void indirect_placeholder_10(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint64_t param_3, uint64_t param_4, uint64_t param_5, uint64_t param_6);
extern void indirect_placeholder_5(uint64_t param_0, uint64_t param_1, uint64_t param_2);
extern struct indirect_placeholder_12_ret_type indirect_placeholder_12(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint64_t param_3, uint64_t param_4);
void bb_main(uint64_t rdi, uint64_t rsi) {
    uint64_t cc_src2_2;
    uint64_t cc_src2_4;
    uint64_t var_31;
    uint64_t var_17;
    uint64_t var_18;
    uint64_t r12_0;
    uint64_t local_sp_3;
    uint64_t cc_src2_3;
    uint64_t storemerge;
    uint64_t var_19;
    uint64_t var_20;
    uint64_t r14_0;
    uint64_t var_21;
    struct indirect_placeholder_12_ret_type var_12;
    uint64_t var_0;
    uint64_t var_1;
    uint64_t var_2;
    uint64_t var_3;
    uint64_t var_4;
    uint64_t var_5;
    uint64_t var_6;
    uint64_t var_7;
    uint64_t var_8;
    uint64_t *_cast;
    uint64_t var_9;
    uint64_t var_10;
    uint64_t local_sp_6;
    uint64_t cc_src2_1;
    uint64_t local_sp_1;
    uint64_t local_sp_0;
    uint64_t r13_0;
    uint64_t r8_1;
    uint64_t cc_src2_0;
    uint64_t local_sp_4;
    uint64_t r8_0;
    uint64_t var_22;
    uint32_t *var_30;
    uint64_t local_sp_2;
    unsigned char var_23;
    uint64_t rax_0;
    uint64_t rdx_0;
    unsigned char var_24;
    uint64_t var_25;
    uint64_t var_26;
    unsigned char var_27;
    uint64_t var_28;
    uint64_t var_29;
    uint64_t local_sp_5;
    uint64_t var_14;
    uint64_t var_15;
    uint64_t var_16;
    uint64_t var_11;
    uint32_t var_13;
    unsigned int loop_state_var;
    var_0 = revng_init_local_sp(0UL);
    var_1 = init_rbx();
    var_2 = init_r14();
    var_3 = init_rbp();
    var_4 = init_r13();
    var_5 = init_r12();
    var_6 = init_r15();
    var_7 = init_cc_src2();
    *(uint64_t *)(var_0 + (-8L)) = var_6;
    *(uint64_t *)(var_0 + (-16L)) = var_2;
    *(uint64_t *)(var_0 + (-24L)) = var_4;
    *(uint64_t *)(var_0 + (-32L)) = var_5;
    *(uint64_t *)(var_0 + (-40L)) = var_3;
    *(uint64_t *)(var_0 + (-48L)) = var_1;
    var_8 = (uint64_t)(uint32_t)rdi;
    _cast = (uint64_t *)rsi;
    var_9 = *_cast;
    *(uint64_t *)(var_0 + (-80L)) = 4201593UL;
    indirect_placeholder_11(var_9);
    *(uint64_t *)(var_0 + (-88L)) = 4201608UL;
    indirect_placeholder_1();
    *(uint32_t *)6353896UL = 2U;
    var_10 = var_0 + (-96L);
    *(uint64_t *)var_10 = 4201628UL;
    indirect_placeholder_1();
    cc_src2_4 = var_7;
    r12_0 = 18446744073709551615UL;
    cc_src2_3 = var_7;
    local_sp_6 = var_10;
    r8_0 = 0UL;
    while (1U)
        {
            var_11 = local_sp_6 + (-8L);
            *(uint64_t *)var_11 = 4201771UL;
            var_12 = indirect_placeholder_12(4245874UL, var_8, 4246720UL, rsi, 0UL);
            var_13 = (uint32_t)var_12.field_0;
            r12_0 = 0UL;
            local_sp_3 = var_11;
            local_sp_6 = var_11;
            local_sp_4 = var_11;
            local_sp_5 = var_11;
            if ((uint64_t)(var_13 + 1U) == 0UL) {
                if ((uint64_t)(var_13 + 130U) == 0UL) {
                    *(uint64_t *)(local_sp_6 + (-16L)) = 4201673UL;
                    indirect_placeholder_5(rsi, var_8, 0UL);
                    abort();
                }
                if ((uint64_t)(var_13 + (-48)) == 0UL) {
                    continue;
                }
                if ((uint64_t)(var_13 + 131U) != 0UL) {
                    loop_state_var = 1U;
                    break;
                }
                *(uint64_t *)(local_sp_6 + (-24L)) = 0UL;
                var_14 = *(uint64_t *)6353888UL;
                var_15 = *(uint64_t *)6354048UL;
                *(uint64_t *)(local_sp_6 + (-32L)) = 4201725UL;
                indirect_placeholder_10(0UL, 4245800UL, var_15, var_14, 4245696UL, 4245841UL, 4245858UL);
                var_16 = local_sp_6 + (-40L);
                *(uint64_t *)var_16 = 4201735UL;
                indirect_placeholder_1();
                local_sp_5 = var_16;
                loop_state_var = 1U;
                break;
            }
            var_17 = (uint64_t)*(uint32_t *)6354012UL;
            var_18 = rdi << 32UL;
            r14_0 = var_17;
            if ((long)var_18 > (long)(var_17 << 32UL)) {
                storemerge = *(uint64_t *)6354112UL;
                loop_state_var = 0U;
                break;
            }
            *(uint32_t *)(local_sp_6 | 4UL) = 0U;
            while (1U)
                {
                    var_21 = local_sp_4 + (-8L);
                    *(uint64_t *)var_21 = 4201875UL;
                    indirect_placeholder_1();
                    local_sp_0 = var_21;
                    cc_src2_0 = cc_src2_4;
                    local_sp_2 = var_21;
                    cc_src2_2 = cc_src2_4;
                    if (r14_0 != 0UL) {
                        r13_0 = *(uint64_t *)6354112UL;
                        var_22 = *(uint64_t *)r13_0;
                        cc_src2_2 = cc_src2_0;
                        cc_src2_1 = cc_src2_0;
                        local_sp_1 = local_sp_0;
                        r8_1 = r8_0;
                        local_sp_2 = local_sp_0;
                        rax_0 = var_22;
                        while (var_22 != 0UL)
                            {
                                var_23 = *(unsigned char *)var_22;
                                rdx_0 = *_cast;
                                while (1U)
                                    {
                                        if (var_23 != '\x00') {
                                            loop_state_var = 0U;
                                            break;
                                        }
                                        var_24 = *(unsigned char *)rdx_0;
                                        if (var_24 != '\x00') {
                                            loop_state_var = 0U;
                                            break;
                                        }
                                        var_25 = rax_0 + 1UL;
                                        var_26 = rdx_0 + 1UL;
                                        rax_0 = var_25;
                                        rdx_0 = var_26;
                                        if ((uint64_t)(var_23 - var_24) != 0UL) {
                                            loop_state_var = 0U;
                                            break;
                                        }
                                        var_27 = *(unsigned char *)var_25;
                                        var_23 = var_27;
                                        r8_1 = 1UL;
                                        if (var_27 != '=') {
                                            continue;
                                        }
                                        if (*(unsigned char *)var_26 == '\x00') {
                                            continue;
                                        }
                                        loop_state_var = 1U;
                                        break;
                                    }
                                switch (loop_state_var) {
                                  case 1U:
                                    {
                                        var_28 = helper_cc_compute_c_wrapper(r12_0, 1UL, cc_src2_0, 14U);
                                        var_29 = local_sp_0 + (-8L);
                                        *(uint64_t *)var_29 = 4201939UL;
                                        indirect_placeholder_1();
                                        local_sp_1 = var_29;
                                        cc_src2_1 = var_28;
                                    }
                                    break;
                                  case 0U:
                                    {
                                        local_sp_0 = local_sp_1;
                                        r13_0 = r13_0 + 8UL;
                                        cc_src2_0 = cc_src2_1;
                                        r8_0 = r8_1;
                                        var_22 = *(uint64_t *)r13_0;
                                        cc_src2_2 = cc_src2_0;
                                        cc_src2_1 = cc_src2_0;
                                        local_sp_1 = local_sp_0;
                                        r8_1 = r8_0;
                                        local_sp_2 = local_sp_0;
                                        rax_0 = var_22;
                                        continue;
                                    }
                                    break;
                                }
                            }
                        var_30 = (uint32_t *)(local_sp_0 + 12UL);
                        *var_30 = (*var_30 + (uint32_t)(unsigned char)(uint32_t)r8_0);
                    }
                    var_31 = r14_0 + 1UL;
                    local_sp_4 = local_sp_2;
                    cc_src2_4 = cc_src2_2;
                    if ((long)var_18 > (long)(var_31 << 32UL)) {
                        break;
                    }
                    r14_0 = (uint64_t)(uint32_t)var_31;
                    continue;
                }
            loop_state_var = 2U;
            break;
        }
    switch (loop_state_var) {
      case 1U:
        {
            *(uint64_t *)(local_sp_5 + (-8L)) = 4201745UL;
            indirect_placeholder_5(rsi, var_8, 2UL);
            abort();
        }
        break;
      case 2U:
      case 0U:
        {
            switch (loop_state_var) {
              case 0U:
                {
                    while (*(uint64_t *)storemerge != 0UL)
                        {
                            var_19 = helper_cc_compute_c_wrapper(r12_0, 1UL, cc_src2_3, 14U);
                            var_20 = local_sp_3 + (-8L);
                            *(uint64_t *)var_20 = 4201825UL;
                            indirect_placeholder_1();
                            local_sp_3 = var_20;
                            cc_src2_3 = var_19;
                            storemerge = storemerge + 8UL;
                        }
                }
                break;
              case 2U:
                {
                    return;
                }
                break;
            }
        }
        break;
    }
}

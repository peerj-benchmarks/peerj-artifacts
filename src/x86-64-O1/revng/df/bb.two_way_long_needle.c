typedef unsigned char __u_char;
typedef unsigned short __u_short;
typedef unsigned int __u_int;
typedef unsigned long __u_long;
typedef signed char __int8_t;
typedef unsigned char __uint8_t;
typedef short __int16_t;
typedef unsigned short __uint16_t;
typedef int __int32_t;
typedef unsigned int __uint32_t;
typedef long __int64_t;
typedef unsigned long __uint64_t;
typedef __int8_t __int_least8_t;
typedef __uint8_t __uint_least8_t;
typedef __int16_t __int_least16_t;
typedef __uint16_t __uint_least16_t;
typedef __int32_t __int_least32_t;
typedef __uint32_t __uint_least32_t;
typedef __int64_t __int_least64_t;
typedef __uint64_t __uint_least64_t;
typedef long __quad_t;
typedef unsigned long __u_quad_t;
typedef long __intmax_t;
typedef unsigned long __uintmax_t;
typedef unsigned long __dev_t;
typedef unsigned int __uid_t;
typedef unsigned int __gid_t;
typedef unsigned long __ino_t;
typedef unsigned long __ino64_t;
typedef unsigned int __mode_t;
typedef unsigned long __nlink_t;
typedef long __off_t;
typedef long __off64_t;
typedef int __pid_t;
typedef struct {
    int __val[2];
} __fsid_t;
typedef long __clock_t;
typedef unsigned long __rlim_t;
typedef unsigned long __rlim64_t;
typedef unsigned int __id_t;
typedef long __time_t;
typedef unsigned int __useconds_t;
typedef long __suseconds_t;
typedef int __daddr_t;
typedef int __key_t;
typedef int __clockid_t;
typedef void *__timer_t;
typedef long __blksize_t;
typedef long __blkcnt_t;
typedef long __blkcnt64_t;
typedef unsigned long __fsblkcnt_t;
typedef unsigned long __fsblkcnt64_t;
typedef unsigned long __fsfilcnt_t;
typedef unsigned long __fsfilcnt64_t;
typedef long __fsword_t;
typedef long __ssize_t;
typedef long __syscall_slong_t;
typedef unsigned long __syscall_ulong_t;
typedef __off64_t __loff_t;
typedef char *__caddr_t;
typedef long __intptr_t;
typedef unsigned int __socklen_t;
typedef int __sig_atomic_t;
typedef __int8_t int8_t;
typedef __int16_t int16_t;
typedef __int32_t int32_t;
typedef __int64_t int64_t;
typedef __uint8_t uint8_t;
typedef __uint16_t uint16_t;
typedef __uint32_t uint32_t;
typedef __uint64_t uint64_t;
typedef __int_least8_t int_least8_t;
typedef __int_least16_t int_least16_t;
typedef __int_least32_t int_least32_t;
typedef __int_least64_t int_least64_t;
typedef __uint_least8_t uint_least8_t;
typedef __uint_least16_t uint_least16_t;
typedef __uint_least32_t uint_least32_t;
typedef __uint_least64_t uint_least64_t;
typedef signed char int_fast8_t;
typedef long int_fast16_t;
typedef long int_fast32_t;
typedef long int_fast64_t;
typedef unsigned char uint_fast8_t;
typedef unsigned long uint_fast16_t;
typedef unsigned long uint_fast32_t;
typedef unsigned long uint_fast64_t;
typedef long intptr_t;
typedef unsigned long uintptr_t;
typedef __intmax_t intmax_t;
typedef __uintmax_t uintmax_t;
typedef _Bool bool;
struct indirect_placeholder_106_ret_type;
struct indirect_placeholder_107_ret_type;
struct indirect_placeholder_108_ret_type;
struct indirect_placeholder_106_ret_type {
    uint64_t field_0;
    uint64_t field_1;
    uint64_t field_2;
};
struct indirect_placeholder_107_ret_type {
    uint64_t field_0;
    uint64_t field_1;
};
struct indirect_placeholder_108_ret_type {
    uint64_t field_0;
    uint64_t field_1;
};
extern uint64_t revng_init_local_sp(uint64_t param_0);
extern uint64_t init_rbx(void);
extern void indirect_placeholder(void);
extern uint64_t init_rbp(void);
extern uint64_t init_cc_src2(void);
extern uint64_t helper_cc_compute_c_wrapper(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint32_t param_3);
extern uint64_t init_r14(void);
extern uint64_t init_r13(void);
extern uint64_t init_r12(void);
extern uint64_t init_r15(void);
extern struct indirect_placeholder_106_ret_type indirect_placeholder_106(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint64_t param_3, uint64_t param_4);
extern struct indirect_placeholder_107_ret_type indirect_placeholder_107(uint64_t param_0, uint64_t param_1, uint64_t param_2);
extern struct indirect_placeholder_108_ret_type indirect_placeholder_108(uint64_t param_0, uint64_t param_1, uint64_t param_2);
uint64_t bb_two_way_long_needle(uint64_t rdx, uint64_t rdi, uint64_t rcx, uint64_t rsi) {
    uint64_t var_78;
    uint64_t r12_0;
    uint64_t rax_5;
    struct indirect_placeholder_106_ret_type var_11;
    uint64_t var_0;
    uint64_t var_1;
    uint64_t var_2;
    uint64_t var_3;
    uint64_t var_4;
    uint64_t var_5;
    uint64_t var_6;
    uint64_t var_7;
    uint64_t var_8;
    uint64_t var_9;
    uint64_t *var_10;
    uint64_t var_12;
    uint64_t var_13;
    uint64_t var_14;
    uint64_t *var_15;
    uint64_t var_16;
    uint64_t var_17;
    uint64_t rax_8;
    uint64_t var_32;
    uint64_t r12_1;
    uint64_t var_34;
    uint64_t var_35;
    uint64_t *var_36;
    uint64_t var_37;
    uint64_t var_38;
    uint64_t var_41;
    uint64_t rax_0;
    uint64_t var_39;
    uint64_t var_40;
    uint64_t _pre;
    uint64_t rax_1;
    uint64_t var_42;
    uint64_t *var_43;
    uint64_t *_pre_phi;
    uint64_t var_44;
    uint64_t var_45;
    uint64_t rax_2;
    uint64_t var_46;
    uint64_t *var_33;
    uint64_t var_56;
    bool var_57;
    uint64_t *var_58;
    uint64_t var_59;
    uint64_t rcx3_0;
    uint64_t var_54;
    uint64_t var_60;
    uint64_t var_61;
    uint64_t var_62;
    uint64_t *var_63;
    uint64_t var_64;
    uint64_t var_65;
    uint64_t var_66;
    uint64_t var_67;
    uint64_t *var_68;
    uint64_t var_69;
    uint64_t var_70;
    uint64_t var_73;
    uint64_t rax_3;
    uint64_t var_71;
    uint64_t var_72;
    uint64_t _pre180;
    uint64_t rax_4;
    uint64_t var_74;
    uint64_t var_75;
    uint64_t rax_6;
    uint64_t var_76;
    uint64_t var_77;
    uint64_t var_25;
    uint64_t local_sp_1;
    uint64_t *var_26;
    uint64_t var_27;
    uint64_t var_28;
    uint64_t rax_7;
    uint64_t var_79;
    uint64_t *var_80;
    uint64_t var_47;
    uint64_t var_48;
    uint64_t var_49;
    uint64_t local_sp_0;
    uint64_t *var_50;
    uint64_t var_51;
    uint64_t var_52;
    uint64_t var_53;
    struct indirect_placeholder_107_ret_type var_55;
    uint64_t var_21;
    uint64_t var_22;
    uint64_t var_23;
    uint64_t var_24;
    uint64_t var_29;
    uint64_t var_30;
    struct indirect_placeholder_108_ret_type var_31;
    uint64_t var_18;
    uint64_t rax_10;
    uint64_t rax_9;
    uint64_t var_19;
    uint64_t var_20;
    unsigned int loop_state_var;
    bool switch_state_var;
    var_0 = revng_init_local_sp(0UL);
    var_1 = init_rbx();
    var_2 = init_r14();
    var_3 = init_rbp();
    var_4 = init_r13();
    var_5 = init_r12();
    var_6 = init_r15();
    var_7 = init_cc_src2();
    *(uint64_t *)(var_0 + (-8L)) = var_6;
    *(uint64_t *)(var_0 + (-16L)) = var_2;
    *(uint64_t *)(var_0 + (-24L)) = var_4;
    *(uint64_t *)(var_0 + (-32L)) = var_5;
    *(uint64_t *)(var_0 + (-40L)) = var_3;
    *(uint64_t *)(var_0 + (-48L)) = var_1;
    var_8 = var_0 + (-64L);
    var_9 = var_0 + (-2192L);
    var_10 = (uint64_t *)var_9;
    *var_10 = 4238185UL;
    var_11 = indirect_placeholder_106(var_8, rcx, rdx, rdx, rcx);
    var_12 = var_11.field_0;
    var_13 = var_11.field_1;
    var_14 = var_11.field_2;
    var_15 = (uint64_t *)(var_0 + (-2168L));
    *var_15 = var_12;
    var_16 = var_0 + (-2128L);
    var_17 = var_0 + (-80L);
    r12_0 = rsi;
    rax_8 = var_16;
    r12_1 = rsi;
    rax_7 = 0UL;
    rax_10 = var_17;
    rax_9 = 0UL;
    *(uint64_t *)rax_8 = var_13;
    var_18 = rax_8 + 8UL;
    rax_8 = var_18;
    do {
        *(uint64_t *)rax_8 = var_13;
        var_18 = rax_8 + 8UL;
        rax_8 = var_18;
    } while (var_18 != var_17);
    if (var_13 == 0UL) {
        *(uint64_t *)((((uint64_t)*(unsigned char *)(rax_9 + var_14) << 3UL) + var_9) + 64UL) = (var_13 + (rax_9 ^ (-1L)));
        var_19 = rax_9 + 1UL;
        rax_9 = var_19;
        rax_10 = var_13;
        do {
            *(uint64_t *)((((uint64_t)*(unsigned char *)(rax_9 + var_14) << 3UL) + var_9) + 64UL) = (var_13 + (rax_9 ^ (-1L)));
            var_19 = rax_9 + 1UL;
            rax_9 = var_19;
            rax_10 = var_13;
        } while (var_19 != var_13);
    }
    var_20 = var_0 + (-2200L);
    *(uint64_t *)var_20 = 4238278UL;
    indirect_placeholder();
    local_sp_0 = var_20;
    local_sp_1 = var_20;
    if ((uint64_t)(uint32_t)rax_10 == 0UL) {
        var_47 = *(uint64_t *)var_17;
        *var_15 = var_47;
        *(uint64_t *)(var_0 + (-2160L)) = (var_13 - var_47);
        *var_10 = 0UL;
        *(uint64_t *)(var_0 + (-2184L)) = 0UL;
        var_48 = var_13 + (-1L);
        var_49 = *(uint64_t *)(var_0 + (-2176L));
        *(uint64_t *)(var_0 + (-2152L)) = (var_49 + (-1L));
        *(uint64_t *)(var_0 + (-2144L)) = (1UL - var_49);
        var_50 = (uint64_t *)(local_sp_0 + 16UL);
        var_51 = var_13 + *var_50;
        var_52 = var_51 - r12_0;
        var_53 = r12_0 + rdi;
        var_54 = local_sp_0 + (-8L);
        *(uint64_t *)var_54 = 4238651UL;
        var_55 = indirect_placeholder_107(var_52, var_53, 0UL);
        r12_0 = var_51;
        local_sp_0 = var_54;
        while (!((var_51 != 0UL) && (var_55.field_0 == 0UL)))
            {
                var_56 = *(uint64_t *)((((uint64_t)*(unsigned char *)((var_51 + rdi) + (-1L)) << 3UL) + var_54) + 64UL);
                var_57 = (var_56 == 0UL);
                var_58 = (uint64_t *)local_sp_0;
                var_59 = *var_58;
                rcx3_0 = var_56;
                if (!var_57) {
                    if (var_59 == 0UL) {
                        var_60 = *(uint64_t *)(local_sp_0 + 24UL);
                        var_61 = *(uint64_t *)(local_sp_0 + 32UL);
                        var_62 = helper_cc_compute_c_wrapper(var_56 - var_60, var_60, var_7, 17U);
                        rcx3_0 = (var_62 == 0UL) ? var_56 : var_61;
                    }
                    var_63 = (uint64_t *)(local_sp_0 + 8UL);
                    *var_63 = (*var_63 + rcx3_0);
                    *var_58 = 0UL;
                    var_50 = (uint64_t *)(local_sp_0 + 16UL);
                    var_51 = var_13 + *var_50;
                    var_52 = var_51 - r12_0;
                    var_53 = r12_0 + rdi;
                    var_54 = local_sp_0 + (-8L);
                    *(uint64_t *)var_54 = 4238651UL;
                    var_55 = indirect_placeholder_107(var_52, var_53, 0UL);
                    r12_0 = var_51;
                    local_sp_0 = var_54;
                    continue;
                }
                var_64 = *var_50;
                var_65 = helper_cc_compute_c_wrapper(var_59 - var_64, var_64, var_7, 17U);
                var_66 = (var_65 == 0UL) ? var_59 : var_64;
                var_67 = helper_cc_compute_c_wrapper(var_66 - var_48, var_48, var_7, 17U);
                rax_3 = var_66;
                rax_4 = var_66;
                if (var_67 == 0UL) {
                    var_74 = *var_50;
                    var_75 = helper_cc_compute_c_wrapper(*var_58 - var_74, var_74, var_7, 17U);
                    rax_6 = var_74;
                    if (var_75 != 0UL) {
                        var_76 = *(uint64_t *)(local_sp_0 + 8UL) + rdi;
                        var_77 = *(uint64_t *)(local_sp_0 + 40UL);
                        rax_5 = var_77;
                        if ((uint64_t)(*(unsigned char *)(var_77 + var_14) - *(unsigned char *)(var_77 + var_76)) != 0UL) {
                            var_78 = *var_58;
                            rax_6 = var_78;
                            while (rax_5 != var_78)
                                {
                                    rax_6 = rax_5;
                                    if ((uint64_t)(*(unsigned char *)((rax_5 + var_14) + (-1L)) - *(unsigned char *)((var_76 + rax_5) + (-1L))) == 0UL) {
                                        break;
                                    }
                                    rax_5 = rax_5 + (-1L);
                                }
                        }
                        rax_6 = *var_50;
                    }
                    if ((*var_58 + 1UL) <= rax_6) {
                        rax_7 = *(uint64_t *)(local_sp_0 + 8UL) + rdi;
                        break;
                    }
                    var_79 = *(uint64_t *)(local_sp_0 + 24UL);
                    var_80 = (uint64_t *)(local_sp_0 + 8UL);
                    *var_80 = (*var_80 + var_79);
                    *var_58 = *(uint64_t *)(local_sp_0 + 32UL);
                    var_50 = (uint64_t *)(local_sp_0 + 16UL);
                    var_51 = var_13 + *var_50;
                    var_52 = var_51 - r12_0;
                    var_53 = r12_0 + rdi;
                    var_54 = local_sp_0 + (-8L);
                    *(uint64_t *)var_54 = 4238651UL;
                    var_55 = indirect_placeholder_107(var_52, var_53, 0UL);
                    r12_0 = var_51;
                    local_sp_0 = var_54;
                    continue;
                }
                var_68 = (uint64_t *)(local_sp_0 + 8UL);
                var_69 = *var_68;
                var_70 = var_69 + rdi;
                var_73 = var_69;
                if ((uint64_t)(*(unsigned char *)(var_66 + var_14) - *(unsigned char *)(var_66 + var_70)) != 0UL) {
                    *var_68 = (rax_4 + (var_73 + *(uint64_t *)(local_sp_0 + 48UL)));
                    *var_58 = 0UL;
                    var_50 = (uint64_t *)(local_sp_0 + 16UL);
                    var_51 = var_13 + *var_50;
                    var_52 = var_51 - r12_0;
                    var_53 = r12_0 + rdi;
                    var_54 = local_sp_0 + (-8L);
                    *(uint64_t *)var_54 = 4238651UL;
                    var_55 = indirect_placeholder_107(var_52, var_53, 0UL);
                    r12_0 = var_51;
                    local_sp_0 = var_54;
                    continue;
                }
                while (1U)
                    {
                        var_71 = rax_3 + 1UL;
                        var_72 = helper_cc_compute_c_wrapper(var_71 - var_48, var_48, var_7, 17U);
                        rax_3 = var_71;
                        rax_4 = var_71;
                        if (var_72 != 0UL) {
                            loop_state_var = 0U;
                            break;
                        }
                        if ((uint64_t)(*(unsigned char *)(var_71 + var_14) - *(unsigned char *)(var_71 + var_70)) == 0UL) {
                            continue;
                        }
                        loop_state_var = 1U;
                        break;
                    }
                switch (loop_state_var) {
                  case 0U:
                    {
                        break;
                    }
                    break;
                  case 1U:
                    {
                        _pre180 = *var_68;
                        var_73 = _pre180;
                        *var_68 = (rax_4 + (var_73 + *(uint64_t *)(local_sp_0 + 48UL)));
                        *var_58 = 0UL;
                        var_50 = (uint64_t *)(local_sp_0 + 16UL);
                        var_51 = var_13 + *var_50;
                        var_52 = var_51 - r12_0;
                        var_53 = r12_0 + rdi;
                        var_54 = local_sp_0 + (-8L);
                        *(uint64_t *)var_54 = 4238651UL;
                        var_55 = indirect_placeholder_107(var_52, var_53, 0UL);
                        r12_0 = var_51;
                        local_sp_0 = var_54;
                        continue;
                    }
                    break;
                }
            }
    }
    var_21 = *(uint64_t *)(var_0 + (-2176L));
    var_22 = var_13 - var_21;
    var_23 = helper_cc_compute_c_wrapper(var_22 - var_21, var_21, var_7, 17U);
    var_24 = ((var_23 == 0UL) ? var_22 : var_21) + 1UL;
    *(uint64_t *)(var_0 + (-2160L)) = var_24;
    *(uint64_t *)var_17 = var_24;
    *var_10 = 0UL;
    var_25 = var_13 + (-1L);
    *(uint64_t *)(var_0 + (-2184L)) = (var_21 + (-1L));
    *var_15 = (1UL - var_21);
    while (1U)
        {
            var_26 = (uint64_t *)(local_sp_1 + 8UL);
            var_27 = var_13 + *var_26;
            var_28 = var_27 - r12_1;
            var_29 = r12_1 + rdi;
            var_30 = local_sp_1 + (-8L);
            *(uint64_t *)var_30 = 4238939UL;
            var_31 = indirect_placeholder_108(var_28, var_29, 0UL);
            r12_1 = var_27;
            local_sp_1 = var_30;
            if (!((var_27 != 0UL) && (var_31.field_0 == 0UL))) {
                loop_state_var = 1U;
                break;
            }
            var_32 = *(uint64_t *)((((uint64_t)*(unsigned char *)((var_27 + rdi) + (-1L)) << 3UL) + var_30) + 64UL);
            if (var_32 != 0UL) {
                var_33 = (uint64_t *)local_sp_1;
                *var_33 = (*var_33 + var_32);
                continue;
            }
            var_34 = *(uint64_t *)(local_sp_1 + 16UL);
            var_35 = helper_cc_compute_c_wrapper(var_34 - var_25, var_25, var_7, 17U);
            rax_0 = var_34;
            rax_1 = var_34;
            if (var_35 == 0UL) {
                var_42 = *var_26;
                rax_2 = var_42;
                if (var_42 != 18446744073709551615UL) {
                    _pre_phi = (uint64_t *)local_sp_1;
                    loop_state_var = 0U;
                    break;
                }
                var_43 = (uint64_t *)local_sp_1;
                var_44 = *var_43;
                var_45 = var_44 + rdi;
                _pre_phi = var_43;
                if ((uint64_t)(*(unsigned char *)(var_42 + var_14) - *(unsigned char *)(var_42 + var_45)) == 0UL) {
                    *var_43 = (var_44 + *(uint64_t *)(local_sp_1 + 32UL));
                    continue;
                }
                while (1U)
                    {
                        var_46 = rax_2 + (-1L);
                        rax_2 = var_46;
                        if (rax_2 != 0UL) {
                            loop_state_var = 0U;
                            break;
                        }
                        if ((uint64_t)(*(unsigned char *)(var_46 + var_14) - *(unsigned char *)(var_46 + var_45)) == 0UL) {
                            continue;
                        }
                        loop_state_var = 1U;
                        break;
                    }
                switch_state_var = 0;
                switch (loop_state_var) {
                  case 1U:
                    {
                        break;
                    }
                    break;
                  case 0U:
                    {
                        loop_state_var = 0U;
                        switch_state_var = 1;
                        break;
                    }
                    break;
                }
                if (switch_state_var)
                    break;
            }
            var_36 = (uint64_t *)local_sp_1;
            var_37 = *var_36;
            var_38 = var_37 + rdi;
            var_41 = var_37;
            if ((uint64_t)(*(unsigned char *)(var_34 + var_14) - *(unsigned char *)(var_34 + var_38)) != 0UL) {
                *var_36 = (rax_1 + (var_41 + *(uint64_t *)(local_sp_1 + 24UL)));
                continue;
            }
            while (1U)
                {
                    var_39 = rax_0 + 1UL;
                    var_40 = helper_cc_compute_c_wrapper(var_39 - var_25, var_25, var_7, 17U);
                    rax_0 = var_39;
                    rax_1 = var_39;
                    if (var_40 != 0UL) {
                        loop_state_var = 0U;
                        break;
                    }
                    if ((uint64_t)(*(unsigned char *)(var_39 + var_14) - *(unsigned char *)(var_39 + var_38)) == 0UL) {
                        continue;
                    }
                    loop_state_var = 1U;
                    break;
                }
            switch (loop_state_var) {
              case 0U:
                {
                    break;
                }
                break;
              case 1U:
                {
                    _pre = *var_36;
                    var_41 = _pre;
                    *var_36 = (rax_1 + (var_41 + *(uint64_t *)(local_sp_1 + 24UL)));
                    continue;
                }
                break;
            }
        }
    rax_7 = *_pre_phi + rdi;
}

typedef unsigned char __u_char;
typedef unsigned short __u_short;
typedef unsigned int __u_int;
typedef unsigned long __u_long;
typedef signed char __int8_t;
typedef unsigned char __uint8_t;
typedef short __int16_t;
typedef unsigned short __uint16_t;
typedef int __int32_t;
typedef unsigned int __uint32_t;
typedef long __int64_t;
typedef unsigned long __uint64_t;
typedef __int8_t __int_least8_t;
typedef __uint8_t __uint_least8_t;
typedef __int16_t __int_least16_t;
typedef __uint16_t __uint_least16_t;
typedef __int32_t __int_least32_t;
typedef __uint32_t __uint_least32_t;
typedef __int64_t __int_least64_t;
typedef __uint64_t __uint_least64_t;
typedef long __quad_t;
typedef unsigned long __u_quad_t;
typedef long __intmax_t;
typedef unsigned long __uintmax_t;
typedef unsigned long __dev_t;
typedef unsigned int __uid_t;
typedef unsigned int __gid_t;
typedef unsigned long __ino_t;
typedef unsigned long __ino64_t;
typedef unsigned int __mode_t;
typedef unsigned long __nlink_t;
typedef long __off_t;
typedef long __off64_t;
typedef int __pid_t;
typedef struct {
    int __val[2];
} __fsid_t;
typedef long __clock_t;
typedef unsigned long __rlim_t;
typedef unsigned long __rlim64_t;
typedef unsigned int __id_t;
typedef long __time_t;
typedef unsigned int __useconds_t;
typedef long __suseconds_t;
typedef int __daddr_t;
typedef int __key_t;
typedef int __clockid_t;
typedef void *__timer_t;
typedef long __blksize_t;
typedef long __blkcnt_t;
typedef long __blkcnt64_t;
typedef unsigned long __fsblkcnt_t;
typedef unsigned long __fsblkcnt64_t;
typedef unsigned long __fsfilcnt_t;
typedef unsigned long __fsfilcnt64_t;
typedef long __fsword_t;
typedef long __ssize_t;
typedef long __syscall_slong_t;
typedef unsigned long __syscall_ulong_t;
typedef __off64_t __loff_t;
typedef char *__caddr_t;
typedef long __intptr_t;
typedef unsigned int __socklen_t;
typedef int __sig_atomic_t;
typedef __int8_t int8_t;
typedef __int16_t int16_t;
typedef __int32_t int32_t;
typedef __int64_t int64_t;
typedef __uint8_t uint8_t;
typedef __uint16_t uint16_t;
typedef __uint32_t uint32_t;
typedef __uint64_t uint64_t;
typedef __int_least8_t int_least8_t;
typedef __int_least16_t int_least16_t;
typedef __int_least32_t int_least32_t;
typedef __int_least64_t int_least64_t;
typedef __uint_least8_t uint_least8_t;
typedef __uint_least16_t uint_least16_t;
typedef __uint_least32_t uint_least32_t;
typedef __uint_least64_t uint_least64_t;
typedef signed char int_fast8_t;
typedef long int_fast16_t;
typedef long int_fast32_t;
typedef long int_fast64_t;
typedef unsigned char uint_fast8_t;
typedef unsigned long uint_fast16_t;
typedef unsigned long uint_fast32_t;
typedef unsigned long uint_fast64_t;
typedef long intptr_t;
typedef unsigned long uintptr_t;
typedef __intmax_t intmax_t;
typedef __uintmax_t uintmax_t;
extern uint64_t revng_init_local_sp(uint64_t param_0);
extern uint64_t init_rbx(void);
extern void indirect_placeholder(void);
extern uint64_t init_rbp(void);
extern uint64_t init_r13(void);
extern uint64_t init_r12(void);
uint64_t bb_sig2str(uint64_t rdi, uint64_t rsi) {
    uint64_t var_0;
    uint64_t var_1;
    uint64_t var_2;
    uint64_t var_3;
    uint64_t var_4;
    uint32_t var_5;
    uint32_t var_6;
    uint64_t var_10;
    uint64_t rax_0;
    uint64_t rax_1;
    uint64_t rcx_0;
    uint64_t var_7;
    uint64_t var_8;
    uint64_t var_9;
    unsigned int loop_state_var;
    var_0 = revng_init_local_sp(0UL);
    var_1 = init_rbx();
    var_2 = init_rbp();
    var_3 = init_r13();
    var_4 = init_r12();
    *(uint64_t *)(var_0 + (-8L)) = var_3;
    *(uint64_t *)(var_0 + (-16L)) = var_4;
    *(uint64_t *)(var_0 + (-24L)) = var_2;
    *(uint64_t *)(var_0 + (-32L)) = var_1;
    var_5 = *(uint32_t *)6358144UL;
    var_6 = (uint32_t)rdi;
    rax_0 = 0UL;
    rax_1 = 1UL;
    rcx_0 = 6358156UL;
    if ((uint64_t)(var_5 - var_6) == 0UL) {
        *(uint64_t *)(var_0 + (-48L)) = 4213117UL;
        indirect_placeholder();
        return rax_0;
    }
    while (1U)
        {
            rax_0 = 4294967295UL;
            if ((uint64_t)(*(uint32_t *)rcx_0 - var_6) != 0UL) {
                loop_state_var = 1U;
                break;
            }
            var_7 = rax_1 + 1UL;
            if ((uint64_t)((uint32_t)rax_1 + (-33)) == 0UL) {
                rax_1 = (uint64_t)(uint32_t)var_7;
                rcx_0 = rcx_0 + 12UL;
                continue;
            }
            *(uint64_t *)(var_0 + (-48L)) = 4213141UL;
            indirect_placeholder();
            *(uint64_t *)(var_0 + (-56L)) = 4213149UL;
            indirect_placeholder();
            var_8 = rdi << 32UL;
            var_9 = var_7 << 32UL;
            if (!(((long)var_8 > (long)var_9) || ((long)var_9 > (long)var_8))) {
                loop_state_var = 0U;
                break;
            }
            var_10 = var_0 + (-64L);
            *(uint64_t *)var_10 = 4213195UL;
            indirect_placeholder();
            rax_0 = 0UL;
            if ((uint64_t)(var_6 - (uint32_t)var_7) != 0UL) {
                loop_state_var = 0U;
                break;
            }
            *(uint64_t *)(var_10 + (-8L)) = 4213239UL;
            indirect_placeholder();
            loop_state_var = 0U;
            break;
        }
    switch (loop_state_var) {
      case 0U:
        {
            return rax_0;
        }
        break;
      case 1U:
        {
            break;
        }
        break;
    }
}

typedef unsigned char __u_char;
typedef unsigned short __u_short;
typedef unsigned int __u_int;
typedef unsigned long __u_long;
typedef signed char __int8_t;
typedef unsigned char __uint8_t;
typedef short __int16_t;
typedef unsigned short __uint16_t;
typedef int __int32_t;
typedef unsigned int __uint32_t;
typedef long __int64_t;
typedef unsigned long __uint64_t;
typedef __int8_t __int_least8_t;
typedef __uint8_t __uint_least8_t;
typedef __int16_t __int_least16_t;
typedef __uint16_t __uint_least16_t;
typedef __int32_t __int_least32_t;
typedef __uint32_t __uint_least32_t;
typedef __int64_t __int_least64_t;
typedef __uint64_t __uint_least64_t;
typedef long __quad_t;
typedef unsigned long __u_quad_t;
typedef long __intmax_t;
typedef unsigned long __uintmax_t;
typedef unsigned long __dev_t;
typedef unsigned int __uid_t;
typedef unsigned int __gid_t;
typedef unsigned long __ino_t;
typedef unsigned long __ino64_t;
typedef unsigned int __mode_t;
typedef unsigned long __nlink_t;
typedef long __off_t;
typedef long __off64_t;
typedef int __pid_t;
typedef struct {
    int __val[2];
} __fsid_t;
typedef long __clock_t;
typedef unsigned long __rlim_t;
typedef unsigned long __rlim64_t;
typedef unsigned int __id_t;
typedef long __time_t;
typedef unsigned int __useconds_t;
typedef long __suseconds_t;
typedef int __daddr_t;
typedef int __key_t;
typedef int __clockid_t;
typedef void *__timer_t;
typedef long __blksize_t;
typedef long __blkcnt_t;
typedef long __blkcnt64_t;
typedef unsigned long __fsblkcnt_t;
typedef unsigned long __fsblkcnt64_t;
typedef unsigned long __fsfilcnt_t;
typedef unsigned long __fsfilcnt64_t;
typedef long __fsword_t;
typedef long __ssize_t;
typedef long __syscall_slong_t;
typedef unsigned long __syscall_ulong_t;
typedef __off64_t __loff_t;
typedef char *__caddr_t;
typedef long __intptr_t;
typedef unsigned int __socklen_t;
typedef int __sig_atomic_t;
typedef __int8_t int8_t;
typedef __int16_t int16_t;
typedef __int32_t int32_t;
typedef __int64_t int64_t;
typedef __uint8_t uint8_t;
typedef __uint16_t uint16_t;
typedef __uint32_t uint32_t;
typedef __uint64_t uint64_t;
typedef __int_least8_t int_least8_t;
typedef __int_least16_t int_least16_t;
typedef __int_least32_t int_least32_t;
typedef __int_least64_t int_least64_t;
typedef __uint_least8_t uint_least8_t;
typedef __uint_least16_t uint_least16_t;
typedef __uint_least32_t uint_least32_t;
typedef __uint_least64_t uint_least64_t;
typedef signed char int_fast8_t;
typedef long int_fast16_t;
typedef long int_fast32_t;
typedef long int_fast64_t;
typedef unsigned char uint_fast8_t;
typedef unsigned long uint_fast16_t;
typedef unsigned long uint_fast32_t;
typedef unsigned long uint_fast64_t;
typedef long intptr_t;
typedef unsigned long uintptr_t;
typedef __intmax_t intmax_t;
typedef __uintmax_t uintmax_t;
typedef _Bool bool;
extern uint64_t revng_init_local_sp(uint64_t param_0);
extern void indirect_placeholder(void);
extern uint64_t init_rbx(void);
extern uint64_t init_r14(void);
extern uint64_t init_rbp(void);
extern uint64_t init_r13(void);
extern uint64_t init_r12(void);
extern uint64_t init_rax(void);
extern uint64_t indirect_placeholder_5(uint64_t param_0);
extern uint64_t indirect_placeholder_8(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint64_t param_3, uint64_t param_4);
extern uint64_t init_r15(void);
uint64_t bb_gnu_fnmatch(uint64_t rdx, uint64_t rdi, uint64_t rsi) {
    uint64_t var_42;
    uint64_t var_46;
    uint64_t var_34;
    uint64_t var_0;
    uint64_t var_1;
    uint64_t var_2;
    uint64_t var_3;
    uint64_t var_4;
    uint64_t var_5;
    uint64_t var_6;
    uint64_t var_7;
    uint64_t *var_8;
    uint64_t *var_9;
    uint64_t var_10;
    uint64_t var_11;
    uint64_t local_sp_3;
    uint64_t var_43;
    uint64_t var_44;
    uint64_t var_45;
    uint64_t local_sp_0;
    uint64_t var_39;
    uint64_t var_40;
    uint64_t var_41;
    uint64_t storemerge_in;
    uint64_t var_47;
    uint64_t var_36;
    bool var_37;
    uint64_t *var_38;
    uint64_t var_32;
    uint64_t var_33;
    uint64_t var_29;
    uint64_t var_30;
    uint64_t var_31;
    uint64_t rax_0;
    uint64_t var_19;
    uint64_t local_sp_1;
    uint64_t var_20;
    bool var_21;
    uint64_t var_22;
    uint64_t var_23;
    uint64_t var_24;
    uint64_t var_25;
    uint64_t var_26;
    bool var_27;
    uint64_t *var_28;
    uint64_t var_35;
    uint64_t var_18;
    uint64_t var_15;
    uint64_t local_sp_2;
    uint64_t var_16;
    uint64_t var_17;
    uint64_t var_14;
    uint64_t var_12;
    uint64_t var_13;
    uint64_t rax_1;
    uint64_t var_48;
    uint64_t var_49;
    uint64_t var_50;
    uint64_t var_51;
    uint64_t var_52;
    uint64_t var_53;
    var_0 = revng_init_local_sp(0UL);
    var_1 = init_rax();
    var_2 = init_rbx();
    var_3 = init_r14();
    var_4 = init_rbp();
    var_5 = init_r13();
    var_6 = init_r12();
    var_7 = init_r15();
    *(uint64_t *)(var_0 + (-8L)) = var_4;
    *(uint64_t *)(var_0 + (-16L)) = var_7;
    *(uint64_t *)(var_0 + (-24L)) = var_3;
    *(uint64_t *)(var_0 + (-32L)) = var_5;
    *(uint64_t *)(var_0 + (-40L)) = var_6;
    *(uint64_t *)(var_0 + (-48L)) = var_2;
    var_8 = (uint64_t *)(var_0 + (-80L));
    *var_8 = rdi;
    var_9 = (uint64_t *)(var_0 + (-88L));
    *var_9 = rsi;
    var_10 = (uint64_t)(uint32_t)rdx;
    var_11 = var_0 + (-96L);
    *(uint64_t *)var_11 = 4269176UL;
    indirect_placeholder();
    rax_0 = 4294967295UL;
    local_sp_3 = var_11;
    rax_1 = 1UL;
    if (var_1 == 1UL) {
        var_48 = *var_9;
        *(uint64_t *)(local_sp_3 + (-8L)) = 4269549UL;
        indirect_placeholder();
        var_49 = (rdx >> 2UL) & 1UL;
        var_50 = rax_1 + var_48;
        var_51 = *var_9;
        var_52 = *var_8;
        *(uint64_t *)(local_sp_3 + (-16L)) = 4269577UL;
        var_53 = indirect_placeholder_8(var_50, var_52, var_49, var_10, var_51);
        rax_0 = var_53;
        return rax_0;
    }
    *(uint64_t *)(var_0 + (-104L)) = 4269205UL;
    indirect_placeholder();
    var_12 = var_0 + (-112L);
    *(uint64_t *)var_12 = 4269228UL;
    indirect_placeholder();
    var_13 = var_1 + 1UL;
    local_sp_3 = var_12;
    rax_1 = 0UL;
    if (var_13 == 0UL) {
        return;
    }
    var_14 = var_0 + (-120L);
    *(uint64_t *)var_14 = 4269250UL;
    indirect_placeholder();
    local_sp_2 = var_14;
    if ((uint64_t)(uint32_t)var_13 == 0UL) {
        var_15 = var_0 + (-128L);
        *(uint64_t *)var_15 = 4269279UL;
        indirect_placeholder();
        local_sp_2 = var_15;
    }
    var_16 = local_sp_2 + (-8L);
    *(uint64_t *)var_16 = 4269302UL;
    indirect_placeholder();
    var_17 = var_1 + 2UL;
    local_sp_3 = var_16;
    if (var_17 != 0UL) {
        var_18 = local_sp_2 + (-16L);
        *(uint64_t *)var_18 = 4269324UL;
        indirect_placeholder();
        local_sp_1 = var_18;
        if ((uint64_t)(uint32_t)var_17 == 0UL) {
            var_19 = local_sp_2 + (-24L);
            *(uint64_t *)var_19 = 4269353UL;
            indirect_placeholder();
            local_sp_1 = var_19;
        }
        var_20 = var_13 + var_17;
        if ((var_20 > 4611686018427387903UL) || (var_13 > var_20)) {
            *(uint64_t *)(local_sp_1 + (-8L)) = 4269383UL;
            indirect_placeholder();
            *(uint32_t *)var_20 = 12U;
        } else {
            var_21 = (var_20 > 1999UL);
            var_22 = var_20 << 2UL;
            if (var_21) {
                *(uint64_t *)(local_sp_1 + (-8L)) = 4269489UL;
                var_35 = indirect_placeholder_5(var_22);
                if (var_35 == 0UL) {
                    *(uint64_t *)(local_sp_1 + (-16L)) = 4269506UL;
                    indirect_placeholder();
                    *(volatile uint32_t *)(uint32_t *)0UL = 12U;
                } else {
                    var_36 = (var_13 << 2UL) + var_35;
                    *(uint64_t *)(local_sp_1 + (-16L)) = 4269655UL;
                    indirect_placeholder();
                    *(uint64_t *)(local_sp_1 + (-24L)) = 4269664UL;
                    indirect_placeholder();
                    var_37 = ((uint64_t)(uint32_t)var_35 == 0UL);
                    var_38 = (uint64_t *)(local_sp_1 + (-32L));
                    if (var_37) {
                        *var_38 = 4269746UL;
                        indirect_placeholder();
                        *(uint64_t *)(local_sp_1 + (-40L)) = 4269765UL;
                        indirect_placeholder();
                        var_43 = (rdx >> 2UL) & 1UL;
                        var_44 = ((var_17 << 2UL) + var_36) + (-4L);
                        var_45 = local_sp_1 + (-48L);
                        *(uint64_t *)var_45 = 4269792UL;
                        var_46 = indirect_placeholder_8(var_44, var_35, var_43, var_10, var_36);
                        local_sp_0 = var_45;
                        storemerge_in = var_46;
                    } else {
                        *var_38 = 4269601UL;
                        indirect_placeholder();
                        var_39 = (rdx >> 2UL) & 1UL;
                        var_40 = ((var_17 << 2UL) + var_36) + (-4L);
                        var_41 = local_sp_1 + (-40L);
                        *(uint64_t *)var_41 = 4269628UL;
                        var_42 = indirect_placeholder_8(var_40, var_35, var_39, var_10, var_36);
                        local_sp_0 = var_41;
                        storemerge_in = var_42;
                    }
                    var_47 = (uint64_t)(uint32_t)storemerge_in;
                    *(uint64_t *)(local_sp_0 + (-8L)) = 4269530UL;
                    indirect_placeholder();
                    rax_0 = var_47;
                }
            } else {
                var_23 = var_22 + 30UL;
                var_24 = local_sp_1 - (var_23 & (-16L));
                var_25 = (var_24 + 15UL) & (-16L);
                var_26 = (var_13 << 2UL) + var_25;
                *(uint64_t *)(var_24 + (-8L)) = 4269454UL;
                indirect_placeholder();
                *(uint64_t *)(var_24 + (-16L)) = 4269463UL;
                indirect_placeholder();
                var_27 = ((uint64_t)((uint32_t)var_23 & (-16)) == 0UL);
                var_28 = (uint64_t *)(var_24 + (-24L));
                if (var_27) {
                    *var_28 = 4269824UL;
                    indirect_placeholder();
                    *(uint64_t *)(var_24 + (-32L)) = 4269843UL;
                    indirect_placeholder();
                    var_32 = (rdx >> 2UL) & 1UL;
                    var_33 = ((var_17 << 2UL) + var_26) + (-4L);
                    *(uint64_t *)(var_24 + (-40L)) = 4269870UL;
                    var_34 = indirect_placeholder_8(var_33, var_25, var_32, var_10, var_26);
                    rax_0 = var_34;
                } else {
                    *var_28 = 4269689UL;
                    indirect_placeholder();
                    var_29 = (rdx >> 2UL) & 1UL;
                    var_30 = ((var_17 << 2UL) + var_26) + (-4L);
                    *(uint64_t *)(var_24 + (-32L)) = 4269716UL;
                    var_31 = indirect_placeholder_8(var_30, var_25, var_29, var_10, var_26);
                    rax_0 = var_31;
                }
            }
        }
        return rax_0;
    }
}

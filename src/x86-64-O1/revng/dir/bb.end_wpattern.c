typedef unsigned char __u_char;
typedef unsigned short __u_short;
typedef unsigned int __u_int;
typedef unsigned long __u_long;
typedef signed char __int8_t;
typedef unsigned char __uint8_t;
typedef short __int16_t;
typedef unsigned short __uint16_t;
typedef int __int32_t;
typedef unsigned int __uint32_t;
typedef long __int64_t;
typedef unsigned long __uint64_t;
typedef __int8_t __int_least8_t;
typedef __uint8_t __uint_least8_t;
typedef __int16_t __int_least16_t;
typedef __uint16_t __uint_least16_t;
typedef __int32_t __int_least32_t;
typedef __uint32_t __uint_least32_t;
typedef __int64_t __int_least64_t;
typedef __uint64_t __uint_least64_t;
typedef long __quad_t;
typedef unsigned long __u_quad_t;
typedef long __intmax_t;
typedef unsigned long __uintmax_t;
typedef unsigned long __dev_t;
typedef unsigned int __uid_t;
typedef unsigned int __gid_t;
typedef unsigned long __ino_t;
typedef unsigned long __ino64_t;
typedef unsigned int __mode_t;
typedef unsigned long __nlink_t;
typedef long __off_t;
typedef long __off64_t;
typedef int __pid_t;
typedef struct {
    int __val[2];
} __fsid_t;
typedef long __clock_t;
typedef unsigned long __rlim_t;
typedef unsigned long __rlim64_t;
typedef unsigned int __id_t;
typedef long __time_t;
typedef unsigned int __useconds_t;
typedef long __suseconds_t;
typedef int __daddr_t;
typedef int __key_t;
typedef int __clockid_t;
typedef void *__timer_t;
typedef long __blksize_t;
typedef long __blkcnt_t;
typedef long __blkcnt64_t;
typedef unsigned long __fsblkcnt_t;
typedef unsigned long __fsblkcnt64_t;
typedef unsigned long __fsfilcnt_t;
typedef unsigned long __fsfilcnt64_t;
typedef long __fsword_t;
typedef long __ssize_t;
typedef long __syscall_slong_t;
typedef unsigned long __syscall_ulong_t;
typedef __off64_t __loff_t;
typedef char *__caddr_t;
typedef long __intptr_t;
typedef unsigned int __socklen_t;
typedef int __sig_atomic_t;
typedef __int8_t int8_t;
typedef __int16_t int16_t;
typedef __int32_t int32_t;
typedef __int64_t int64_t;
typedef __uint8_t uint8_t;
typedef __uint16_t uint16_t;
typedef __uint32_t uint32_t;
typedef __uint64_t uint64_t;
typedef __int_least8_t int_least8_t;
typedef __int_least16_t int_least16_t;
typedef __int_least32_t int_least32_t;
typedef __int_least64_t int_least64_t;
typedef __uint_least8_t uint_least8_t;
typedef __uint_least16_t uint_least16_t;
typedef __uint_least32_t uint_least32_t;
typedef __uint_least64_t uint_least64_t;
typedef signed char int_fast8_t;
typedef long int_fast16_t;
typedef long int_fast32_t;
typedef long int_fast64_t;
typedef unsigned char uint_fast8_t;
typedef unsigned long uint_fast16_t;
typedef unsigned long uint_fast32_t;
typedef unsigned long uint_fast64_t;
typedef long intptr_t;
typedef unsigned long uintptr_t;
typedef __intmax_t intmax_t;
typedef __uintmax_t uintmax_t;
typedef _Bool bool;
extern uint64_t revng_init_local_sp(uint64_t param_0);
extern void indirect_placeholder(void);
extern uint64_t init_rbx(void);
extern uint64_t init_rbp(void);
extern uint64_t init_cc_src2(void);
extern uint64_t init_rax(void);
extern uint64_t indirect_placeholder_5(uint64_t param_0);
extern uint64_t helper_cc_compute_c_wrapper(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint32_t param_3);
extern uint64_t init_rsi(void);
extern uint64_t init_rcx(void);
uint64_t bb_end_wpattern(uint64_t rdi) {
    uint64_t var_0;
    uint64_t var_1;
    uint64_t var_2;
    uint64_t var_3;
    uint64_t var_4;
    uint64_t var_5;
    uint64_t var_6;
    uint64_t cc_src2_1;
    uint64_t local_sp_0_ph_ph;
    uint64_t rax_0_ph_ph;
    uint64_t rbx_0_ph_ph;
    uint64_t rsi_0;
    uint64_t rcx_0_ph_ph;
    uint64_t cc_src2_0_ph_ph;
    uint64_t rsi_0_ph_ph;
    uint64_t local_sp_0_ph;
    uint64_t rax_0_ph;
    uint64_t rbx_0_ph;
    uint64_t rcx_0_ph;
    uint64_t rsi_0_ph;
    uint64_t rax_0;
    uint64_t rbx_0;
    uint64_t rcx_0;
    uint64_t var_7;
    uint32_t var_8;
    uint64_t rax_1;
    uint32_t var_19;
    uint32_t var_23;
    uint64_t var_20;
    uint64_t var_21;
    uint32_t var_22;
    uint64_t local_sp_1;
    uint64_t var_24;
    uint32_t var_25;
    uint64_t storemerge;
    uint32_t var_27;
    uint64_t var_26;
    uint64_t var_28;
    uint64_t rbx_1;
    bool var_9;
    uint64_t var_10;
    uint64_t var_11;
    bool var_12;
    uint64_t var_13;
    uint64_t var_14;
    uint64_t var_15;
    uint64_t var_16;
    uint64_t var_17;
    uint64_t var_18;
    unsigned int loop_state_var;
    bool switch_state_var;
    var_0 = revng_init_local_sp(0UL);
    var_1 = init_rax();
    var_2 = init_rbx();
    var_3 = init_rbp();
    var_4 = init_rcx();
    var_5 = init_cc_src2();
    var_6 = init_rsi();
    *(uint64_t *)(var_0 + (-8L)) = var_3;
    *(uint64_t *)(var_0 + (-16L)) = var_2;
    local_sp_0_ph_ph = var_0 + (-24L);
    rax_0_ph_ph = var_1;
    rbx_0_ph_ph = rdi;
    rcx_0_ph_ph = var_4;
    cc_src2_0_ph_ph = var_5;
    rsi_0_ph_ph = var_6;
    rax_1 = rdi;
    while (1U)
        {
            cc_src2_1 = cc_src2_0_ph_ph;
            local_sp_0_ph = local_sp_0_ph_ph;
            rax_0_ph = rax_0_ph_ph;
            rbx_0_ph = rbx_0_ph_ph;
            rcx_0_ph = rcx_0_ph_ph;
            rsi_0_ph = rsi_0_ph_ph;
            while (1U)
                {
                    rsi_0 = rsi_0_ph;
                    rax_0 = rax_0_ph;
                    rbx_0 = rbx_0_ph;
                    rcx_0 = rcx_0_ph;
                    local_sp_1 = local_sp_0_ph;
                    while (1U)
                        {
                            var_7 = rbx_0 + 4UL;
                            var_8 = *(uint32_t *)var_7;
                            rcx_0_ph_ph = rcx_0;
                            rsi_0_ph_ph = rsi_0;
                            rbx_0 = var_7;
                            if (var_8 != 0U) {
                                loop_state_var = 0U;
                                break;
                            }
                            if ((uint64_t)(var_8 + (-91)) != 0UL) {
                                loop_state_var = 2U;
                                break;
                            }
                            var_9 = ((uint64_t)((var_8 + (-63)) & (-2)) == 0UL);
                            var_10 = (rsi_0 & (-256L)) | var_9;
                            var_11 = (uint64_t)(var_8 + (-42));
                            var_12 = (var_11 < 2UL);
                            var_13 = var_12;
                            var_14 = (rcx_0 & (-256L)) | var_13;
                            var_15 = var_10 | var_13;
                            rsi_0 = var_15;
                            rcx_0_ph = var_14;
                            rsi_0_ph = var_15;
                            rax_0 = var_11;
                            rcx_0 = var_14;
                            if (var_9 || var_12) {
                                var_16 = rbx_0 + 8UL;
                                if ((uint64_t)(var_8 + (-33)) != 0UL & *(uint32_t *)var_16 != 40U) {
                                    loop_state_var = 1U;
                                    break;
                                }
                            }
                            var_16 = rbx_0 + 8UL;
                            if (*(uint32_t *)var_16 != 40U) {
                                loop_state_var = 1U;
                                break;
                            }
                            if ((uint64_t)(var_8 + (-41)) == 0UL) {
                                continue;
                            }
                            rax_1 = rbx_0 + 8UL;
                            loop_state_var = 0U;
                            break;
                        }
                    switch_state_var = 0;
                    switch (loop_state_var) {
                      case 1U:
                        {
                            var_17 = local_sp_0_ph + (-8L);
                            *(uint64_t *)var_17 = 4260506UL;
                            var_18 = indirect_placeholder_5(var_16);
                            local_sp_0_ph = var_17;
                            rax_0_ph = var_18;
                            rbx_0_ph = var_18;
                            continue;
                        }
                        break;
                      case 0U:
                        {
                            loop_state_var = 0U;
                            switch_state_var = 1;
                            break;
                        }
                        break;
                      case 2U:
                        {
                            loop_state_var = 1U;
                            switch_state_var = 1;
                            break;
                        }
                        break;
                    }
                    if (switch_state_var)
                        break;
                }
            switch_state_var = 0;
            switch (loop_state_var) {
              case 0U:
                {
                    switch_state_var = 1;
                    break;
                }
                break;
              case 1U:
                {
                    var_19 = *(uint32_t *)6470648UL;
                    var_23 = var_19;
                    if (var_19 == 0U) {
                        var_20 = local_sp_0_ph + (-8L);
                        *(uint64_t *)var_20 = 4260385UL;
                        indirect_placeholder();
                        var_21 = helper_cc_compute_c_wrapper(rax_0 + (-1L), 1UL, cc_src2_0_ph_ph, 17U);
                        var_22 = (0U - (uint32_t)var_21) | 1U;
                        *(uint32_t *)6470648UL = var_22;
                        var_23 = var_22;
                        local_sp_1 = var_20;
                        cc_src2_1 = var_21;
                    }
                    var_24 = rbx_0 + 8UL;
                    var_25 = *(uint32_t *)var_24;
                    local_sp_0_ph_ph = local_sp_1;
                    cc_src2_0_ph_ph = cc_src2_1;
                    var_27 = var_25;
                    storemerge = var_24;
                    if ((uint64_t)(var_25 + (-33)) == 0UL) {
                        var_26 = rbx_0 + 12UL;
                        var_27 = *(uint32_t *)var_26;
                        storemerge = var_26;
                    } else {
                        if ((uint64_t)(var_25 + (-94)) != 0UL & (int)var_23 < (int)0U) {
                            var_26 = rbx_0 + 12UL;
                            var_27 = *(uint32_t *)var_26;
                            storemerge = var_26;
                        }
                    }
                    var_28 = storemerge + 4UL;
                    rax_0_ph_ph = var_28;
                    rbx_1 = (var_27 == 93U) ? var_28 : storemerge;
                    while (1U)
                        {
                            rbx_0_ph_ph = rbx_1;
                            switch_state_var = 0;
                            switch (*(uint32_t *)rbx_1) {
                              case 93U:
                                {
                                    loop_state_var = 0U;
                                    switch_state_var = 1;
                                    break;
                                }
                                break;
                              case 0U:
                                {
                                    loop_state_var = 1U;
                                    switch_state_var = 1;
                                    break;
                                }
                                break;
                              default:
                                {
                                    rbx_1 = rbx_1 + 4UL;
                                    continue;
                                }
                                break;
                            }
                            if (switch_state_var)
                                break;
                        }
                    switch_state_var = 0;
                    switch (loop_state_var) {
                      case 1U:
                        {
                            switch_state_var = 1;
                            break;
                        }
                        break;
                      case 0U:
                        {
                            continue;
                        }
                        break;
                    }
                    if (switch_state_var)
                        break;
                }
                break;
            }
            if (switch_state_var)
                break;
        }
    return rax_1;
}

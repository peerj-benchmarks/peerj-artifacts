typedef unsigned char __u_char;
typedef unsigned short __u_short;
typedef unsigned int __u_int;
typedef unsigned long __u_long;
typedef signed char __int8_t;
typedef unsigned char __uint8_t;
typedef short __int16_t;
typedef unsigned short __uint16_t;
typedef int __int32_t;
typedef unsigned int __uint32_t;
typedef long __int64_t;
typedef unsigned long __uint64_t;
typedef __int8_t __int_least8_t;
typedef __uint8_t __uint_least8_t;
typedef __int16_t __int_least16_t;
typedef __uint16_t __uint_least16_t;
typedef __int32_t __int_least32_t;
typedef __uint32_t __uint_least32_t;
typedef __int64_t __int_least64_t;
typedef __uint64_t __uint_least64_t;
typedef long __quad_t;
typedef unsigned long __u_quad_t;
typedef long __intmax_t;
typedef unsigned long __uintmax_t;
typedef unsigned long __dev_t;
typedef unsigned int __uid_t;
typedef unsigned int __gid_t;
typedef unsigned long __ino_t;
typedef unsigned long __ino64_t;
typedef unsigned int __mode_t;
typedef unsigned long __nlink_t;
typedef long __off_t;
typedef long __off64_t;
typedef int __pid_t;
typedef struct {
    int __val[2];
} __fsid_t;
typedef long __clock_t;
typedef unsigned long __rlim_t;
typedef unsigned long __rlim64_t;
typedef unsigned int __id_t;
typedef long __time_t;
typedef unsigned int __useconds_t;
typedef long __suseconds_t;
typedef int __daddr_t;
typedef int __key_t;
typedef int __clockid_t;
typedef void *__timer_t;
typedef long __blksize_t;
typedef long __blkcnt_t;
typedef long __blkcnt64_t;
typedef unsigned long __fsblkcnt_t;
typedef unsigned long __fsblkcnt64_t;
typedef unsigned long __fsfilcnt_t;
typedef unsigned long __fsfilcnt64_t;
typedef long __fsword_t;
typedef long __ssize_t;
typedef long __syscall_slong_t;
typedef unsigned long __syscall_ulong_t;
typedef __off64_t __loff_t;
typedef char *__caddr_t;
typedef long __intptr_t;
typedef unsigned int __socklen_t;
typedef int __sig_atomic_t;
typedef __int8_t int8_t;
typedef __int16_t int16_t;
typedef __int32_t int32_t;
typedef __int64_t int64_t;
typedef __uint8_t uint8_t;
typedef __uint16_t uint16_t;
typedef __uint32_t uint32_t;
typedef __uint64_t uint64_t;
typedef __int_least8_t int_least8_t;
typedef __int_least16_t int_least16_t;
typedef __int_least32_t int_least32_t;
typedef __int_least64_t int_least64_t;
typedef __uint_least8_t uint_least8_t;
typedef __uint_least16_t uint_least16_t;
typedef __uint_least32_t uint_least32_t;
typedef __uint_least64_t uint_least64_t;
typedef signed char int_fast8_t;
typedef long int_fast16_t;
typedef long int_fast32_t;
typedef long int_fast64_t;
typedef unsigned char uint_fast8_t;
typedef unsigned long uint_fast16_t;
typedef unsigned long uint_fast32_t;
typedef unsigned long uint_fast64_t;
typedef long intptr_t;
typedef unsigned long uintptr_t;
typedef __intmax_t intmax_t;
typedef __uintmax_t uintmax_t;
struct bb_length_of_file_name_and_frills_ret_type;
struct bb_length_of_file_name_and_frills_ret_type {
    uint64_t field_0;
    uint64_t field_1;
};
extern uint64_t revng_init_local_sp(uint64_t param_0);
extern void indirect_placeholder(void);
extern uint64_t init_rbx(void);
extern uint64_t indirect_placeholder_1(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint64_t param_3, uint64_t param_4, uint64_t param_5);
extern uint64_t init_rbp(void);
extern uint64_t init_r13(void);
extern uint64_t init_r12(void);
extern uint64_t init_rax(void);
extern uint64_t indirect_placeholder_8(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint64_t param_3, uint64_t param_4);
extern uint64_t indirect_placeholder_7(uint64_t param_0, uint64_t param_1, uint64_t param_2);
extern uint64_t indirect_placeholder_3(uint64_t param_0, uint64_t param_1);
struct bb_length_of_file_name_and_frills_ret_type bb_length_of_file_name_and_frills(uint64_t rdi) {
    uint64_t var_18;
    uint64_t var_0;
    uint64_t var_1;
    uint64_t var_2;
    uint64_t var_3;
    uint64_t var_4;
    uint64_t var_5;
    uint64_t var_6;
    uint64_t local_sp_1;
    uint64_t local_sp_8;
    uint64_t local_sp_0;
    uint64_t var_31;
    uint64_t rbx_0;
    uint64_t var_32;
    uint64_t var_33;
    uint64_t var_34;
    uint64_t var_35;
    struct bb_length_of_file_name_and_frills_ret_type mrv;
    struct bb_length_of_file_name_and_frills_ret_type mrv1;
    uint64_t local_sp_2;
    uint64_t var_8;
    uint64_t var_9;
    uint64_t var_10;
    uint64_t var_11;
    uint64_t var_12;
    uint64_t var_7;
    uint64_t local_sp_3;
    uint64_t local_sp_6;
    uint64_t rax_0;
    uint64_t storemerge1;
    uint64_t local_sp_5;
    uint64_t var_21;
    uint64_t var_13;
    uint64_t var_14;
    uint64_t var_15;
    uint64_t var_16;
    uint64_t var_17;
    uint64_t rax_1;
    uint64_t var_19;
    uint64_t local_sp_4;
    uint64_t rbx_1;
    uint64_t rbx_2;
    uint64_t storemerge_in;
    uint64_t storemerge;
    uint64_t rax_2;
    uint64_t rbx_3;
    uint64_t var_20;
    uint64_t local_sp_7;
    uint64_t rbx_4;
    uint64_t rbx_5;
    uint64_t storemerge2_in;
    uint64_t rbx_6;
    uint64_t var_22;
    uint64_t var_23;
    uint64_t var_24;
    uint64_t var_25;
    uint64_t *var_26;
    uint64_t var_27;
    uint64_t var_28;
    uint64_t var_29;
    uint64_t var_30;
    var_0 = revng_init_local_sp(0UL);
    var_1 = init_rax();
    var_2 = init_rbx();
    var_3 = init_rbp();
    var_4 = init_r13();
    var_5 = init_r12();
    *(uint64_t *)(var_0 + (-8L)) = var_4;
    *(uint64_t *)(var_0 + (-16L)) = var_5;
    *(uint64_t *)(var_0 + (-24L)) = var_3;
    *(uint64_t *)(var_0 + (-32L)) = var_2;
    var_6 = var_0 + (-1304L);
    local_sp_1 = var_6;
    local_sp_8 = var_6;
    local_sp_3 = var_6;
    local_sp_6 = var_6;
    rax_0 = var_1;
    storemerge1 = 0UL;
    if (*(unsigned char *)6470044UL == '\x00') {
        if (*(uint32_t *)6470104UL == 4U) {
            var_8 = *(uint64_t *)(rdi + 32UL);
            var_9 = var_0 + (-248L);
            *(uint64_t *)(var_0 + (-1312L)) = 4214804UL;
            var_10 = indirect_placeholder_3(var_8, var_9);
            var_11 = var_0 + (-1320L);
            *(uint64_t *)var_11 = 4214812UL;
            indirect_placeholder();
            var_12 = var_10 + 1UL;
            local_sp_1 = var_11;
            rax_0 = var_10;
            storemerge1 = var_12;
            local_sp_2 = local_sp_1;
            local_sp_3 = local_sp_1;
            local_sp_5 = local_sp_1;
            rax_1 = rax_0;
            rbx_1 = storemerge1;
            rbx_2 = storemerge1;
            rax_2 = rax_0;
            rbx_3 = storemerge1;
            if (*(unsigned char *)6470092UL != '\x00') {
                if (*(uint32_t *)6470104UL == 4U) {
                    local_sp_4 = local_sp_3;
                    rbx_2 = rbx_1;
                    storemerge_in = (uint64_t)*(uint32_t *)6470140UL;
                } else {
                    if (*(unsigned char *)(rdi + 184UL) != '\x00') {
                        var_13 = *(uint64_t *)(rdi + 88UL);
                        var_14 = *(uint64_t *)6470080UL;
                        var_15 = (uint64_t)*(uint32_t *)6470088UL;
                        var_16 = local_sp_1 + 1056UL;
                        var_17 = local_sp_1 + (-8L);
                        *(uint64_t *)var_17 = 4214915UL;
                        var_18 = indirect_placeholder_8(var_15, var_13, 512UL, var_14, var_16);
                        local_sp_2 = var_17;
                        rax_1 = var_18;
                    }
                    var_19 = local_sp_2 + (-8L);
                    *(uint64_t *)var_19 = 4214923UL;
                    indirect_placeholder();
                    local_sp_4 = var_19;
                    storemerge_in = rax_1;
                }
                storemerge = storemerge_in + 1UL;
                local_sp_5 = local_sp_4;
                rax_2 = storemerge;
                rbx_3 = rbx_2 + storemerge;
            }
            local_sp_8 = local_sp_5;
            local_sp_6 = local_sp_5;
            rbx_4 = rbx_3;
            rbx_5 = rbx_3;
            storemerge2_in = rax_2;
            rbx_6 = rbx_3;
            if (*(unsigned char *)6470149UL != '\x00') {
                if (*(uint32_t *)6470104UL == 4U) {
                    local_sp_7 = local_sp_6;
                    rbx_5 = rbx_4;
                    storemerge2_in = (uint64_t)*(uint32_t *)6470132UL;
                } else {
                    var_20 = local_sp_5 + (-8L);
                    *(uint64_t *)var_20 = 4214973UL;
                    indirect_placeholder();
                    local_sp_7 = var_20;
                }
                local_sp_8 = local_sp_7;
                rbx_6 = rbx_5 + (storemerge2_in + 1UL);
            }
        } else {
            var_7 = (uint64_t)*(uint32_t *)6470144UL + 1UL;
            rbx_1 = var_7;
            rbx_4 = var_7;
            rbx_6 = var_7;
            if (*(unsigned char *)6470092UL == '\x00') {
                if (*(unsigned char *)6470149UL != '\x00') {
                    local_sp_7 = local_sp_6;
                    rbx_5 = rbx_4;
                    storemerge2_in = (uint64_t)*(uint32_t *)6470132UL;
                    local_sp_8 = local_sp_7;
                    rbx_6 = rbx_5 + (storemerge2_in + 1UL);
                }
            } else {
                local_sp_4 = local_sp_3;
                rbx_2 = rbx_1;
                storemerge_in = (uint64_t)*(uint32_t *)6470140UL;
                storemerge = storemerge_in + 1UL;
                local_sp_5 = local_sp_4;
                rax_2 = storemerge;
                rbx_3 = rbx_2 + storemerge;
                local_sp_8 = local_sp_5;
                local_sp_6 = local_sp_5;
                rbx_4 = rbx_3;
                rbx_5 = rbx_3;
                storemerge2_in = rax_2;
                rbx_6 = rbx_3;
                if (*(unsigned char *)6470149UL != '\x00') {
                    if (*(uint32_t *)6470104UL == 4U) {
                        var_20 = local_sp_5 + (-8L);
                        *(uint64_t *)var_20 = 4214973UL;
                        indirect_placeholder();
                        local_sp_7 = var_20;
                    } else {
                        local_sp_7 = local_sp_6;
                        rbx_5 = rbx_4;
                        storemerge2_in = (uint64_t)*(uint32_t *)6470132UL;
                    }
                    local_sp_8 = local_sp_7;
                    rbx_6 = rbx_5 + (storemerge2_in + 1UL);
                }
            }
        }
    } else {
        local_sp_2 = local_sp_1;
        local_sp_3 = local_sp_1;
        local_sp_5 = local_sp_1;
        rax_1 = rax_0;
        rbx_1 = storemerge1;
        rbx_2 = storemerge1;
        rax_2 = rax_0;
        rbx_3 = storemerge1;
        if (*(unsigned char *)6470092UL != '\x00') {
            if (*(uint32_t *)6470104UL == 4U) {
                local_sp_4 = local_sp_3;
                rbx_2 = rbx_1;
                storemerge_in = (uint64_t)*(uint32_t *)6470140UL;
            } else {
                if (*(unsigned char *)(rdi + 184UL) == '\x00') {
                    var_13 = *(uint64_t *)(rdi + 88UL);
                    var_14 = *(uint64_t *)6470080UL;
                    var_15 = (uint64_t)*(uint32_t *)6470088UL;
                    var_16 = local_sp_1 + 1056UL;
                    var_17 = local_sp_1 + (-8L);
                    *(uint64_t *)var_17 = 4214915UL;
                    var_18 = indirect_placeholder_8(var_15, var_13, 512UL, var_14, var_16);
                    local_sp_2 = var_17;
                    rax_1 = var_18;
                }
                var_19 = local_sp_2 + (-8L);
                *(uint64_t *)var_19 = 4214923UL;
                indirect_placeholder();
                local_sp_4 = var_19;
                storemerge_in = rax_1;
            }
            storemerge = storemerge_in + 1UL;
            local_sp_5 = local_sp_4;
            rax_2 = storemerge;
            rbx_3 = rbx_2 + storemerge;
        }
        local_sp_8 = local_sp_5;
        local_sp_6 = local_sp_5;
        rbx_4 = rbx_3;
        rbx_5 = rbx_3;
        storemerge2_in = rax_2;
        rbx_6 = rbx_3;
        if (*(unsigned char *)6470149UL != '\x00') {
            if (*(uint32_t *)6470104UL == 4U) {
                local_sp_7 = local_sp_6;
                rbx_5 = rbx_4;
                storemerge2_in = (uint64_t)*(uint32_t *)6470132UL;
            } else {
                var_20 = local_sp_5 + (-8L);
                *(uint64_t *)var_20 = 4214973UL;
                indirect_placeholder();
                local_sp_7 = var_20;
            }
            local_sp_8 = local_sp_7;
            rbx_6 = rbx_5 + (storemerge2_in + 1UL);
        }
    }
    var_21 = *(uint64_t *)rdi;
    var_22 = local_sp_8 + 32UL;
    var_23 = local_sp_8 + 16UL;
    *(uint64_t *)var_23 = var_22;
    var_24 = local_sp_8 + 15UL;
    var_25 = local_sp_8 + (-16L);
    var_26 = (uint64_t *)var_25;
    *var_26 = var_24;
    var_27 = local_sp_8 + 24UL;
    var_28 = (uint64_t)*(uint32_t *)(rdi + 196UL);
    var_29 = *(uint64_t *)6470000UL;
    *(uint64_t *)(local_sp_8 + (-24L)) = 4215054UL;
    indirect_placeholder_1(var_21, var_23, var_29, var_28, var_27, 1024UL);
    var_30 = *(uint64_t *)(local_sp_8 + 8UL);
    local_sp_0 = local_sp_8 + (-8L);
    if ((var_30 == var_22) || (var_21 == var_30)) {
        *var_26 = 4215078UL;
        indirect_placeholder();
        local_sp_0 = var_25;
    }
    var_31 = rbx_6 + (*(uint64_t *)(local_sp_0 + 24UL) + (uint64_t)*(unsigned char *)(local_sp_0 + 15UL));
    rbx_0 = var_31;
    if (*(uint32_t *)6470068UL == 0U) {
        mrv.field_0 = rbx_0;
        mrv1 = mrv;
        mrv1.field_1 = var_27;
        return mrv1;
    }
    var_32 = (uint64_t)*(uint32_t *)(rdi + 168UL);
    var_33 = (uint64_t)*(uint32_t *)(rdi + 48UL);
    var_34 = (uint64_t)*(unsigned char *)(rdi + 184UL);
    *(uint64_t *)(local_sp_0 + (-8L)) = 4215121UL;
    var_35 = indirect_placeholder_7(var_32, var_34, var_33);
    rbx_0 = var_31 + ((uint64_t)(unsigned char)var_35 != 0UL);
    mrv.field_0 = rbx_0;
    mrv1 = mrv;
    mrv1.field_1 = var_27;
    return mrv1;
}

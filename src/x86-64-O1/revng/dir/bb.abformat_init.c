typedef unsigned char __u_char;
typedef unsigned short __u_short;
typedef unsigned int __u_int;
typedef unsigned long __u_long;
typedef signed char __int8_t;
typedef unsigned char __uint8_t;
typedef short __int16_t;
typedef unsigned short __uint16_t;
typedef int __int32_t;
typedef unsigned int __uint32_t;
typedef long __int64_t;
typedef unsigned long __uint64_t;
typedef __int8_t __int_least8_t;
typedef __uint8_t __uint_least8_t;
typedef __int16_t __int_least16_t;
typedef __uint16_t __uint_least16_t;
typedef __int32_t __int_least32_t;
typedef __uint32_t __uint_least32_t;
typedef __int64_t __int_least64_t;
typedef __uint64_t __uint_least64_t;
typedef long __quad_t;
typedef unsigned long __u_quad_t;
typedef long __intmax_t;
typedef unsigned long __uintmax_t;
typedef unsigned long __dev_t;
typedef unsigned int __uid_t;
typedef unsigned int __gid_t;
typedef unsigned long __ino_t;
typedef unsigned long __ino64_t;
typedef unsigned int __mode_t;
typedef unsigned long __nlink_t;
typedef long __off_t;
typedef long __off64_t;
typedef int __pid_t;
typedef struct {
    int __val[2];
} __fsid_t;
typedef long __clock_t;
typedef unsigned long __rlim_t;
typedef unsigned long __rlim64_t;
typedef unsigned int __id_t;
typedef long __time_t;
typedef unsigned int __useconds_t;
typedef long __suseconds_t;
typedef int __daddr_t;
typedef int __key_t;
typedef int __clockid_t;
typedef void *__timer_t;
typedef long __blksize_t;
typedef long __blkcnt_t;
typedef long __blkcnt64_t;
typedef unsigned long __fsblkcnt_t;
typedef unsigned long __fsblkcnt64_t;
typedef unsigned long __fsfilcnt_t;
typedef unsigned long __fsfilcnt64_t;
typedef long __fsword_t;
typedef long __ssize_t;
typedef long __syscall_slong_t;
typedef unsigned long __syscall_ulong_t;
typedef __off64_t __loff_t;
typedef char *__caddr_t;
typedef long __intptr_t;
typedef unsigned int __socklen_t;
typedef int __sig_atomic_t;
typedef __int8_t int8_t;
typedef __int16_t int16_t;
typedef __int32_t int32_t;
typedef __int64_t int64_t;
typedef __uint8_t uint8_t;
typedef __uint16_t uint16_t;
typedef __uint32_t uint32_t;
typedef __uint64_t uint64_t;
typedef __int_least8_t int_least8_t;
typedef __int_least16_t int_least16_t;
typedef __int_least32_t int_least32_t;
typedef __int_least64_t int_least64_t;
typedef __uint_least8_t uint_least8_t;
typedef __uint_least16_t uint_least16_t;
typedef __uint_least32_t uint_least32_t;
typedef __uint_least64_t uint_least64_t;
typedef signed char int_fast8_t;
typedef long int_fast16_t;
typedef long int_fast32_t;
typedef long int_fast64_t;
typedef unsigned char uint_fast8_t;
typedef unsigned long uint_fast16_t;
typedef unsigned long uint_fast32_t;
typedef unsigned long uint_fast64_t;
typedef long intptr_t;
typedef unsigned long uintptr_t;
typedef __intmax_t intmax_t;
typedef __uintmax_t uintmax_t;
struct indirect_placeholder_27_ret_type;
struct indirect_placeholder_28_ret_type;
struct indirect_placeholder_29_ret_type;
struct indirect_placeholder_27_ret_type {
    uint64_t field_0;
    uint64_t field_1;
    uint64_t field_2;
};
struct indirect_placeholder_28_ret_type {
    uint64_t field_0;
    uint64_t field_1;
    uint64_t field_2;
};
struct indirect_placeholder_29_ret_type {
    uint64_t field_0;
    uint64_t field_1;
    uint64_t field_2;
};
extern uint64_t revng_init_local_sp(uint64_t param_0);
extern void indirect_placeholder(void);
extern uint64_t init_rbx(void);
extern uint64_t init_r14(void);
extern uint64_t init_rbp(void);
extern uint64_t init_r13(void);
extern uint64_t init_r12(void);
extern uint64_t init_cc_src2(void);
extern uint64_t helper_cc_compute_c_wrapper(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint32_t param_3);
extern uint64_t init_r15(void);
extern struct indirect_placeholder_27_ret_type indirect_placeholder_27(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint64_t param_3, uint64_t param_4, uint64_t param_5, uint64_t param_6);
extern struct indirect_placeholder_28_ret_type indirect_placeholder_28(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint64_t param_3, uint64_t param_4, uint64_t param_5, uint64_t param_6);
extern struct indirect_placeholder_29_ret_type indirect_placeholder_29(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint64_t param_3, uint64_t param_4, uint64_t param_5);
typedef _Bool bool;
void bb_abformat_init(void) {
    struct indirect_placeholder_27_ret_type var_30;
    uint64_t var_0;
    uint64_t var_1;
    uint64_t var_2;
    uint64_t var_3;
    uint64_t var_4;
    uint64_t var_5;
    uint64_t var_6;
    uint64_t var_7;
    uint64_t var_8;
    uint64_t rax_0;
    uint64_t local_sp_0;
    uint64_t r8_0;
    uint64_t r9_0;
    uint64_t rbp_0;
    uint64_t var_31;
    uint64_t local_sp_1;
    uint64_t r8_1;
    uint64_t r9_1;
    uint64_t local_sp_2;
    uint64_t rbx_0;
    uint64_t rbx_1;
    uint64_t var_20;
    uint64_t r12_0;
    uint64_t var_21;
    uint64_t var_22;
    uint64_t rbp_1;
    uint64_t var_23;
    uint64_t local_sp_3;
    uint64_t local_sp_4;
    uint64_t r14_0;
    uint64_t r13_0;
    uint64_t var_24;
    uint64_t var_25;
    uint64_t r8_2;
    uint64_t r9_2;
    uint64_t var_26;
    uint64_t var_29;
    uint64_t var_27;
    struct indirect_placeholder_28_ret_type var_28;
    uint64_t rax_2;
    uint64_t var_17;
    uint64_t var_18;
    struct indirect_placeholder_29_ret_type var_19;
    uint64_t rsi_0;
    uint64_t var_9;
    unsigned char var_10;
    uint64_t rdx_1;
    uint64_t rcx_0;
    unsigned char rax_1_in;
    uint64_t rdx_0;
    uint64_t var_11;
    unsigned char var_12;
    uint64_t var_13;
    unsigned char var_14;
    uint64_t var_15;
    uint64_t var_16;
    uint64_t rbx_2;
    unsigned int loop_state_var;
    bool switch_state_var;
    var_0 = revng_init_local_sp(0UL);
    var_1 = init_rbx();
    var_2 = init_r14();
    var_3 = init_rbp();
    var_4 = init_r13();
    var_5 = init_r12();
    var_6 = init_r15();
    var_7 = init_cc_src2();
    *(uint64_t *)(var_0 + (-8L)) = var_6;
    *(uint64_t *)(var_0 + (-16L)) = var_2;
    *(uint64_t *)(var_0 + (-24L)) = var_4;
    *(uint64_t *)(var_0 + (-32L)) = var_5;
    *(uint64_t *)(var_0 + (-40L)) = var_3;
    *(uint64_t *)(var_0 + (-48L)) = var_1;
    var_8 = var_0 + (-1640L);
    rbx_0 = 6466112UL;
    r12_0 = 131086UL;
    rbp_1 = 0UL;
    local_sp_3 = var_8;
    r14_0 = 5UL;
    r13_0 = 0UL;
    rsi_0 = 0UL;
    rdx_1 = 0UL;
    while (1U)
        {
            var_9 = *(uint64_t *)(rsi_0 + 6464976UL);
            var_10 = *(unsigned char *)var_9;
            rax_1_in = var_10;
            rdx_0 = var_9;
            if (var_10 == '\x00') {
                while (1U)
                    {
                        rcx_0 = rdx_0;
                        rdx_1 = rdx_0;
                        if (rax_1_in != '%') {
                            var_11 = rdx_0 + 1UL;
                            var_12 = *(unsigned char *)var_11;
                            rcx_0 = var_11;
                            rcx_0 = rdx_0;
                            if ((uint64_t)(var_12 + '\xdb') != 0UL & (uint64_t)(var_12 + '\x9e') == 0UL) {
                                break;
                            }
                        }
                        var_13 = rcx_0 + 1UL;
                        var_14 = *(unsigned char *)var_13;
                        rax_1_in = var_14;
                        rdx_0 = var_13;
                        if (var_14 != '\x00') {
                            continue;
                        }
                        break;
                    }
            }
            *(uint64_t *)((rsi_0 + var_8) + 1568UL) = rdx_1;
            if (rsi_0 == 8UL) {
                break;
            }
            rsi_0 = rsi_0 + 8UL;
            continue;
        }
    var_15 = var_0 + (-72L);
    if (*(uint64_t *)var_15 != 0UL) {
        if (*(uint64_t *)(var_0 + (-64L)) == 0UL) {
            return;
        }
    }
    while (1U)
        {
            var_16 = local_sp_3 + 32UL;
            *(uint64_t *)(local_sp_3 + 8UL) = var_16;
            rax_2 = var_16;
            rbx_2 = var_16;
            local_sp_4 = local_sp_3;
            while (1U)
                {
                    *(uint64_t *)(local_sp_4 + 24UL) = r14_0;
                    *(uint64_t *)(local_sp_4 + (-8L)) = 4208228UL;
                    indirect_placeholder();
                    *(uint64_t *)(local_sp_4 + (-16L)) = 4208244UL;
                    indirect_placeholder();
                    if (rax_2 != 0UL) {
                        loop_state_var = 1U;
                        break;
                    }
                    var_17 = local_sp_4 + 8UL;
                    var_18 = local_sp_4 + (-24L);
                    *(uint64_t *)var_18 = 4208286UL;
                    var_19 = indirect_placeholder_29(128UL, 0UL, var_17, 0UL, 0UL, rbx_2);
                    local_sp_1 = var_18;
                    local_sp_3 = var_18;
                    local_sp_4 = var_18;
                    if (var_19.field_0 <= 127UL) {
                        loop_state_var = 1U;
                        break;
                    }
                    var_20 = *(uint64_t *)local_sp_4;
                    var_21 = helper_cc_compute_c_wrapper(rbp_1 - var_20, var_20, var_7, 17U);
                    var_22 = (var_21 == 0UL) ? rbp_1 : var_20;
                    var_23 = rbx_2 + 128UL;
                    r14_0 = var_22;
                    rax_2 = var_20;
                    rbx_2 = var_23;
                    rbp_1 = var_22;
                    if (var_23 != var_15) {
                        loop_state_var = 0U;
                        break;
                    }
                    r12_0 = (uint64_t)((uint32_t)r12_0 + 1U);
                    continue;
                }
            switch_state_var = 0;
            switch (loop_state_var) {
              case 1U:
                {
                    switch_state_var = 1;
                    break;
                }
                break;
              case 0U:
                {
                    if (r14_0 > var_22) {
                        continue;
                    }
                    r8_1 = var_19.field_1;
                    r9_1 = var_19.field_2;
                    while (1U)
                        {
                            var_24 = *(uint64_t *)(r13_0 + 6464976UL);
                            var_25 = rbx_0 + 1536UL;
                            rbp_0 = *(uint64_t *)(local_sp_1 + 8UL);
                            local_sp_2 = local_sp_1;
                            rbx_0 = var_25;
                            rbx_1 = rbx_0;
                            r8_2 = r8_1;
                            r9_2 = r9_1;
                            while (1U)
                                {
                                    var_26 = *(uint64_t *)((r13_0 + local_sp_2) + 1568UL);
                                    if (var_26 == 0UL) {
                                        var_29 = local_sp_2 + (-8L);
                                        *(uint64_t *)var_29 = 4208394UL;
                                        var_30 = indirect_placeholder_27(0UL, 4337748UL, rbx_1, var_24, r8_2, r9_2, 128UL);
                                        rax_0 = var_30.field_0;
                                        local_sp_0 = var_29;
                                        r8_0 = var_30.field_1;
                                        r9_0 = var_30.field_2;
                                    } else {
                                        var_27 = var_26 - var_24;
                                        if ((long)var_27 <= (long)128UL) {
                                            loop_state_var = 1U;
                                            break;
                                        }
                                        *(uint64_t *)(local_sp_2 + (-16L)) = (var_26 + 2UL);
                                        *(uint64_t *)(local_sp_2 + (-24L)) = 4208446UL;
                                        var_28 = indirect_placeholder_28(0UL, 4327701UL, rbx_1, var_27, var_24, rbp_0, 128UL);
                                        rax_0 = var_28.field_0;
                                        local_sp_0 = local_sp_2 + (-8L);
                                        r8_0 = var_28.field_1;
                                        r9_0 = var_28.field_2;
                                    }
                                    local_sp_1 = local_sp_0;
                                    r8_1 = r8_0;
                                    r9_1 = r9_0;
                                    local_sp_2 = local_sp_0;
                                    r8_2 = r8_0;
                                    r9_2 = r9_0;
                                    if ((uint64_t)((uint32_t)rax_0 & (-128)) != 0UL) {
                                        loop_state_var = 1U;
                                        break;
                                    }
                                    var_31 = rbx_1 + 128UL;
                                    rbx_1 = var_31;
                                    if (var_31 != var_25) {
                                        loop_state_var = 0U;
                                        break;
                                    }
                                    rbp_0 = rbp_0 + 128UL;
                                    continue;
                                }
                            switch_state_var = 0;
                            switch (loop_state_var) {
                              case 1U:
                                {
                                    switch_state_var = 1;
                                    break;
                                }
                                break;
                              case 0U:
                                {
                                    if (r13_0 == 8UL) {
                                        r13_0 = r13_0 + 8UL;
                                        continue;
                                    }
                                    *(unsigned char *)6466056UL = (unsigned char)'\x01';
                                    switch_state_var = 1;
                                    break;
                                }
                                break;
                            }
                            if (switch_state_var)
                                break;
                        }
                    switch_state_var = 1;
                    break;
                }
                break;
            }
            if (switch_state_var)
                break;
        }
    return;
}

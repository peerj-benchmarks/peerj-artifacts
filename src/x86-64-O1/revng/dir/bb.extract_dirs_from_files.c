typedef unsigned char __u_char;
typedef unsigned short __u_short;
typedef unsigned int __u_int;
typedef unsigned long __u_long;
typedef signed char __int8_t;
typedef unsigned char __uint8_t;
typedef short __int16_t;
typedef unsigned short __uint16_t;
typedef int __int32_t;
typedef unsigned int __uint32_t;
typedef long __int64_t;
typedef unsigned long __uint64_t;
typedef __int8_t __int_least8_t;
typedef __uint8_t __uint_least8_t;
typedef __int16_t __int_least16_t;
typedef __uint16_t __uint_least16_t;
typedef __int32_t __int_least32_t;
typedef __uint32_t __uint_least32_t;
typedef __int64_t __int_least64_t;
typedef __uint64_t __uint_least64_t;
typedef long __quad_t;
typedef unsigned long __u_quad_t;
typedef long __intmax_t;
typedef unsigned long __uintmax_t;
typedef unsigned long __dev_t;
typedef unsigned int __uid_t;
typedef unsigned int __gid_t;
typedef unsigned long __ino_t;
typedef unsigned long __ino64_t;
typedef unsigned int __mode_t;
typedef unsigned long __nlink_t;
typedef long __off_t;
typedef long __off64_t;
typedef int __pid_t;
typedef struct {
    int __val[2];
} __fsid_t;
typedef long __clock_t;
typedef unsigned long __rlim_t;
typedef unsigned long __rlim64_t;
typedef unsigned int __id_t;
typedef long __time_t;
typedef unsigned int __useconds_t;
typedef long __suseconds_t;
typedef int __daddr_t;
typedef int __key_t;
typedef int __clockid_t;
typedef void *__timer_t;
typedef long __blksize_t;
typedef long __blkcnt_t;
typedef long __blkcnt64_t;
typedef unsigned long __fsblkcnt_t;
typedef unsigned long __fsblkcnt64_t;
typedef unsigned long __fsfilcnt_t;
typedef unsigned long __fsfilcnt64_t;
typedef long __fsword_t;
typedef long __ssize_t;
typedef long __syscall_slong_t;
typedef unsigned long __syscall_ulong_t;
typedef __off64_t __loff_t;
typedef char *__caddr_t;
typedef long __intptr_t;
typedef unsigned int __socklen_t;
typedef int __sig_atomic_t;
typedef __int8_t int8_t;
typedef __int16_t int16_t;
typedef __int32_t int32_t;
typedef __int64_t int64_t;
typedef __uint8_t uint8_t;
typedef __uint16_t uint16_t;
typedef __uint32_t uint32_t;
typedef __uint64_t uint64_t;
typedef __int_least8_t int_least8_t;
typedef __int_least16_t int_least16_t;
typedef __int_least32_t int_least32_t;
typedef __int_least64_t int_least64_t;
typedef __uint_least8_t uint_least8_t;
typedef __uint_least16_t uint_least16_t;
typedef __uint_least32_t uint_least32_t;
typedef __uint_least64_t uint_least64_t;
typedef signed char int_fast8_t;
typedef long int_fast16_t;
typedef long int_fast32_t;
typedef long int_fast64_t;
typedef unsigned char uint_fast8_t;
typedef unsigned long uint_fast16_t;
typedef unsigned long uint_fast32_t;
typedef unsigned long uint_fast64_t;
typedef long intptr_t;
typedef unsigned long uintptr_t;
typedef __intmax_t intmax_t;
typedef __uintmax_t uintmax_t;
typedef _Bool bool;
extern uint64_t revng_init_local_sp(uint64_t param_0);
extern void indirect_placeholder(void);
extern uint64_t init_rbx(void);
extern uint64_t init_r14(void);
extern uint64_t init_rbp(void);
extern uint64_t init_r13(void);
extern uint64_t init_r12(void);
extern uint64_t init_cc_src2(void);
extern uint64_t indirect_placeholder_5(uint64_t param_0);
extern uint64_t helper_cc_compute_c_wrapper(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint32_t param_3);
extern uint64_t init_r15(void);
extern void indirect_placeholder_10(uint64_t param_0, uint64_t param_1, uint64_t param_2);
extern uint64_t indirect_placeholder_7(uint64_t param_0, uint64_t param_1, uint64_t param_2);
void bb_extract_dirs_from_files(uint64_t rdi, uint64_t rsi) {
    uint64_t var_0;
    uint64_t var_1;
    uint64_t var_2;
    uint64_t var_3;
    uint64_t var_4;
    uint64_t var_5;
    uint64_t var_6;
    uint64_t var_7;
    uint64_t var_8;
    bool var_9;
    uint64_t local_sp_3;
    uint64_t *_pre_phi;
    uint64_t local_sp_0;
    uint64_t var_27;
    uint64_t var_11;
    uint64_t var_21;
    uint64_t var_22;
    uint64_t var_23;
    uint64_t *var_17;
    uint64_t *_pre;
    uint64_t var_19;
    uint64_t var_18;
    uint64_t var_20;
    uint64_t local_sp_1;
    uint64_t var_24;
    uint64_t var_25;
    uint64_t var_26;
    uint64_t local_sp_2;
    uint64_t rbx_0;
    uint64_t var_10;
    uint64_t var_12;
    uint64_t var_13;
    uint64_t r12_0_in;
    uint64_t local_sp_4;
    uint64_t r12_0;
    uint64_t var_28;
    uint64_t rdx_1;
    uint64_t var_29;
    uint64_t rax_0;
    uint64_t rdx_0;
    uint64_t var_30;
    uint64_t var_31;
    uint64_t var_32;
    uint64_t var_33;
    uint64_t var_14;
    uint64_t var_15;
    uint64_t var_16;
    var_0 = revng_init_local_sp(0UL);
    var_1 = init_rbx();
    var_2 = init_r14();
    var_3 = init_rbp();
    var_4 = init_r13();
    var_5 = init_r12();
    var_6 = init_r15();
    var_7 = init_cc_src2();
    *(uint64_t *)(var_0 + (-8L)) = var_6;
    *(uint64_t *)(var_0 + (-16L)) = var_2;
    *(uint64_t *)(var_0 + (-24L)) = var_4;
    *(uint64_t *)(var_0 + (-32L)) = var_5;
    *(uint64_t *)(var_0 + (-40L)) = var_3;
    *(uint64_t *)(var_0 + (-48L)) = var_1;
    var_8 = var_0 + (-56L);
    var_9 = (rdi == 0UL);
    local_sp_3 = var_8;
    rax_0 = 0UL;
    rdx_0 = 0UL;
    rdx_1 = 0UL;
    if (!var_9 && *(uint64_t *)6470248UL == 0UL) {
        var_10 = var_0 + (-64L);
        *(uint64_t *)var_10 = 4221627UL;
        indirect_placeholder_10(0UL, 0UL, rdi);
        local_sp_3 = var_10;
    }
    var_11 = *(uint64_t *)6470224UL;
    var_12 = var_11 << 3UL;
    var_13 = (uint64_t)(unsigned char)rsi;
    rbx_0 = var_11;
    r12_0_in = var_12;
    local_sp_4 = local_sp_3;
    r12_0 = r12_0_in + (-8L);
    r12_0_in = r12_0;
    while (rbx_0 != 0UL)
        {
            var_14 = *(uint64_t *)(r12_0 + *(uint64_t *)6470208UL);
            var_15 = local_sp_4 + (-8L);
            *(uint64_t *)var_15 = 4221670UL;
            var_16 = indirect_placeholder_5(var_14);
            local_sp_1 = var_15;
            local_sp_2 = var_15;
            if ((uint64_t)(unsigned char)var_16 == 0UL) {
                rbx_0 = rbx_0 + (-1L);
                local_sp_4 = local_sp_2;
                r12_0 = r12_0_in + (-8L);
                r12_0_in = r12_0;
                continue;
            }
            if (var_9) {
                _pre = (uint64_t *)var_14;
                _pre_phi = _pre;
                var_24 = *(uint64_t *)(var_14 + 8UL);
                var_25 = *_pre_phi;
                var_26 = local_sp_1 + (-8L);
                *(uint64_t *)var_26 = 4221745UL;
                indirect_placeholder_10(var_13, var_25, var_24);
                local_sp_0 = var_26;
            } else {
                var_17 = (uint64_t *)var_14;
                var_18 = *var_17;
                var_19 = local_sp_4 + (-16L);
                *(uint64_t *)var_19 = 4221695UL;
                var_20 = indirect_placeholder_5(var_18);
                _pre_phi = var_17;
                local_sp_1 = var_19;
                local_sp_2 = var_19;
                if (*(unsigned char *)var_20 != '.') {
                    rbx_0 = rbx_0 + (-1L);
                    local_sp_4 = local_sp_2;
                    r12_0 = r12_0_in + (-8L);
                    r12_0_in = r12_0;
                    continue;
                }
                if (*(unsigned char *)var_18 == '/') {
                    var_24 = *(uint64_t *)(var_14 + 8UL);
                    var_25 = *_pre_phi;
                    var_26 = local_sp_1 + (-8L);
                    *(uint64_t *)var_26 = 4221745UL;
                    indirect_placeholder_10(var_13, var_25, var_24);
                    local_sp_0 = var_26;
                } else {
                    *(uint64_t *)(local_sp_4 + (-24L)) = 4221763UL;
                    var_21 = indirect_placeholder_7(0UL, rdi, var_18);
                    var_22 = *(uint64_t *)(var_14 + 8UL);
                    *(uint64_t *)(local_sp_4 + (-32L)) = 4221781UL;
                    indirect_placeholder_10(var_13, var_21, var_22);
                    var_23 = local_sp_4 + (-40L);
                    *(uint64_t *)var_23 = 4221789UL;
                    indirect_placeholder();
                    local_sp_0 = var_23;
                }
            }
        }
    var_28 = *(uint64_t *)6470224UL;
    if (var_28 == 0UL) {
        *(uint64_t *)6470224UL = rdx_1;
        return;
    }
    var_29 = *(uint64_t *)6470208UL;
    var_30 = *(uint64_t *)((rax_0 << 3UL) + var_29);
    *(uint64_t *)((rdx_0 << 3UL) + var_29) = var_30;
    var_31 = rdx_0 + (*(uint32_t *)(var_30 + 168UL) != 9U);
    var_32 = rax_0 + 1UL;
    var_33 = helper_cc_compute_c_wrapper(var_32 - var_28, var_28, var_7, 17U);
    rax_0 = var_32;
    rdx_0 = var_31;
    rdx_1 = var_31;
    do {
        var_30 = *(uint64_t *)((rax_0 << 3UL) + var_29);
        *(uint64_t *)((rdx_0 << 3UL) + var_29) = var_30;
        var_31 = rdx_0 + (*(uint32_t *)(var_30 + 168UL) != 9U);
        var_32 = rax_0 + 1UL;
        var_33 = helper_cc_compute_c_wrapper(var_32 - var_28, var_28, var_7, 17U);
        rax_0 = var_32;
        rdx_0 = var_31;
        rdx_1 = var_31;
    } while (var_33 != 0UL);
    *(uint64_t *)6470224UL = rdx_1;
    return;
}

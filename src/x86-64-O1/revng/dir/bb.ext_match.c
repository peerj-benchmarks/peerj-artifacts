typedef unsigned char __u_char;
typedef unsigned short __u_short;
typedef unsigned int __u_int;
typedef unsigned long __u_long;
typedef signed char __int8_t;
typedef unsigned char __uint8_t;
typedef short __int16_t;
typedef unsigned short __uint16_t;
typedef int __int32_t;
typedef unsigned int __uint32_t;
typedef long __int64_t;
typedef unsigned long __uint64_t;
typedef __int8_t __int_least8_t;
typedef __uint8_t __uint_least8_t;
typedef __int16_t __int_least16_t;
typedef __uint16_t __uint_least16_t;
typedef __int32_t __int_least32_t;
typedef __uint32_t __uint_least32_t;
typedef __int64_t __int_least64_t;
typedef __uint64_t __uint_least64_t;
typedef long __quad_t;
typedef unsigned long __u_quad_t;
typedef long __intmax_t;
typedef unsigned long __uintmax_t;
typedef unsigned long __dev_t;
typedef unsigned int __uid_t;
typedef unsigned int __gid_t;
typedef unsigned long __ino_t;
typedef unsigned long __ino64_t;
typedef unsigned int __mode_t;
typedef unsigned long __nlink_t;
typedef long __off_t;
typedef long __off64_t;
typedef int __pid_t;
typedef struct {
    int __val[2];
} __fsid_t;
typedef long __clock_t;
typedef unsigned long __rlim_t;
typedef unsigned long __rlim64_t;
typedef unsigned int __id_t;
typedef long __time_t;
typedef unsigned int __useconds_t;
typedef long __suseconds_t;
typedef int __daddr_t;
typedef int __key_t;
typedef int __clockid_t;
typedef void *__timer_t;
typedef long __blksize_t;
typedef long __blkcnt_t;
typedef long __blkcnt64_t;
typedef unsigned long __fsblkcnt_t;
typedef unsigned long __fsblkcnt64_t;
typedef unsigned long __fsfilcnt_t;
typedef unsigned long __fsfilcnt64_t;
typedef long __fsword_t;
typedef long __ssize_t;
typedef long __syscall_slong_t;
typedef unsigned long __syscall_ulong_t;
typedef __off64_t __loff_t;
typedef char *__caddr_t;
typedef long __intptr_t;
typedef unsigned int __socklen_t;
typedef int __sig_atomic_t;
typedef __int8_t int8_t;
typedef __int16_t int16_t;
typedef __int32_t int32_t;
typedef __int64_t int64_t;
typedef __uint8_t uint8_t;
typedef __uint16_t uint16_t;
typedef __uint32_t uint32_t;
typedef __uint64_t uint64_t;
typedef __int_least8_t int_least8_t;
typedef __int_least16_t int_least16_t;
typedef __int_least32_t int_least32_t;
typedef __int_least64_t int_least64_t;
typedef __uint_least8_t uint_least8_t;
typedef __uint_least16_t uint_least16_t;
typedef __uint_least32_t uint_least32_t;
typedef __uint_least64_t uint_least64_t;
typedef signed char int_fast8_t;
typedef long int_fast16_t;
typedef long int_fast32_t;
typedef long int_fast64_t;
typedef unsigned char uint_fast8_t;
typedef unsigned long uint_fast16_t;
typedef unsigned long uint_fast32_t;
typedef unsigned long uint_fast64_t;
typedef long intptr_t;
typedef unsigned long uintptr_t;
typedef __intmax_t intmax_t;
typedef __uintmax_t uintmax_t;
typedef _Bool bool;
extern void abort(void);
extern uint64_t revng_init_local_sp(uint64_t param_0);
extern void indirect_placeholder(void);
extern uint64_t init_rbx(void);
extern uint64_t init_r14(void);
extern uint64_t init_rbp(void);
extern uint64_t init_r13(void);
extern uint64_t init_r12(void);
extern uint64_t init_cc_src2(void);
extern uint64_t init_rax(void);
extern uint64_t helper_cc_compute_c_wrapper(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint32_t param_3);
extern uint64_t indirect_placeholder_8(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint64_t param_3, uint64_t param_4);
extern uint64_t init_r15(void);
void bb_ext_match(uint64_t rdx, uint64_t rdi, uint64_t rcx, uint64_t r8, uint64_t r9, uint64_t rsi) {
    uint64_t var_63;
    uint64_t var_54;
    uint64_t var_49;
    uint64_t var_90;
    uint64_t var_93;
    uint64_t var_104;
    uint64_t var_96;
    uint64_t var_100;
    uint64_t var_0;
    uint64_t var_1;
    uint64_t var_2;
    uint64_t var_3;
    uint64_t var_4;
    uint64_t var_5;
    uint64_t var_6;
    uint64_t var_7;
    uint64_t var_8;
    uint32_t *var_9;
    uint32_t var_10;
    uint64_t *var_11;
    uint64_t *var_12;
    uint64_t var_13;
    uint64_t var_14;
    uint32_t *var_15;
    uint64_t var_16;
    unsigned char *var_17;
    uint64_t var_18;
    uint64_t *var_19;
    uint64_t var_20;
    uint64_t var_21;
    uint64_t *var_22;
    uint64_t var_23;
    unsigned char var_24;
    uint64_t local_sp_13;
    uint64_t var_119;
    uint64_t local_sp_12;
    uint64_t rbx_1;
    uint32_t *storemerge_in_in_pre_phi;
    uint64_t rcx3_3;
    uint64_t local_sp_0;
    uint64_t local_sp_11;
    uint64_t var_125;
    uint64_t var_126;
    uint64_t r13_0;
    uint64_t var_64;
    uint64_t rax_0;
    uint64_t rcx3_0;
    uint64_t var_97;
    uint64_t var_98;
    uint64_t var_99;
    uint64_t local_sp_9;
    uint64_t local_sp_2;
    uint64_t local_sp_7;
    uint64_t local_sp_1;
    uint64_t storemerge;
    unsigned char *var_94;
    uint64_t rcx3_1;
    uint64_t var_95;
    uint64_t var_101;
    uint64_t var_102;
    uint64_t var_103;
    uint64_t var_105;
    uint64_t var_106;
    uint64_t rbx_0;
    uint64_t local_sp_10;
    uint64_t var_46;
    uint64_t local_sp_3;
    uint64_t r12_0;
    uint64_t local_sp_4;
    uint64_t var_47;
    uint32_t var_48;
    unsigned char var_135;
    uint64_t local_sp_5;
    uint32_t var_55;
    uint64_t var_56;
    uint64_t var_57;
    uint64_t var_58;
    uint64_t var_59;
    uint64_t local_sp_6;
    uint64_t *var_60;
    uint32_t var_127;
    uint64_t var_61;
    uint64_t var_62;
    uint64_t var_50;
    uint64_t var_51;
    uint64_t var_52;
    uint64_t var_53;
    uint32_t var_71;
    uint64_t var_72;
    uint32_t var_73;
    uint64_t var_74;
    uint32_t *var_75;
    bool var_76;
    unsigned char var_77;
    uint64_t var_78;
    uint64_t var_79;
    uint32_t var_80;
    uint32_t *var_81;
    uint64_t *var_82;
    uint64_t var_83;
    uint64_t var_84;
    bool var_85;
    uint32_t *var_86;
    uint64_t local_sp_8;
    uint64_t var_87;
    uint64_t var_91;
    uint64_t var_92;
    uint64_t var_88;
    uint64_t var_89;
    uint64_t var_107;
    uint64_t var_65;
    uint64_t var_66;
    uint64_t var_67;
    uint64_t var_68;
    uint64_t var_69;
    uint64_t var_70;
    uint64_t var_108;
    uint32_t var_109;
    uint64_t var_110;
    uint64_t var_111;
    uint32_t *var_112;
    uint64_t var_113;
    uint64_t *var_114;
    uint64_t var_115;
    uint64_t var_120;
    uint64_t var_121;
    uint64_t var_122;
    uint64_t var_123;
    uint64_t var_124;
    uint64_t var_116;
    uint64_t var_117;
    uint64_t var_118;
    uint64_t var_25;
    uint64_t *var_26;
    uint64_t var_27;
    uint32_t *var_28;
    uint64_t rax_4;
    unsigned char rax_1_ph_in;
    uint64_t rbx_2_ph;
    uint64_t r12_1;
    uint64_t r14_0_ph;
    uint64_t rcx3_5;
    uint64_t r12_0_ph;
    uint64_t cc_src2_2;
    uint64_t rcx3_4_ph;
    uint64_t local_sp_16;
    uint64_t cc_src2_0_ph;
    uint64_t local_sp_14_ph;
    unsigned char rax_1_in;
    uint64_t rbx_2;
    uint64_t rcx3_4;
    uint64_t rax_1;
    uint32_t var_131;
    uint64_t var_128;
    uint64_t var_129;
    uint32_t var_130;
    uint64_t cc_src2_1;
    uint64_t local_sp_15;
    uint64_t var_132;
    unsigned char var_133;
    uint64_t storemerge3;
    uint64_t var_134;
    uint64_t rax_2;
    bool var_29;
    uint64_t var_30;
    bool var_31;
    bool var_32;
    uint64_t var_33;
    uint64_t var_34;
    uint64_t var_40;
    uint64_t var_41;
    uint64_t var_42;
    uint64_t var_43;
    uint64_t var_44;
    uint64_t var_45;
    uint64_t var_35;
    uint64_t var_36;
    uint64_t var_37;
    uint64_t var_38;
    uint64_t var_39;
    uint64_t r14_1;
    uint64_t var_136;
    unsigned char var_137;
    unsigned int loop_state_var;
    bool switch_state_var;
    var_0 = revng_init_local_sp(0UL);
    var_1 = init_rax();
    var_2 = init_rbx();
    var_3 = init_r14();
    var_4 = init_rbp();
    var_5 = init_r13();
    var_6 = init_r12();
    var_7 = init_r15();
    var_8 = init_cc_src2();
    *(uint64_t *)(var_0 + (-8L)) = var_4;
    *(uint64_t *)(var_0 + (-16L)) = var_7;
    *(uint64_t *)(var_0 + (-24L)) = var_3;
    *(uint64_t *)(var_0 + (-32L)) = var_5;
    *(uint64_t *)(var_0 + (-40L)) = var_6;
    *(uint64_t *)(var_0 + (-48L)) = var_2;
    var_9 = (uint32_t *)(var_0 + (-128L));
    var_10 = (uint32_t)rdi;
    *var_9 = var_10;
    var_11 = (uint64_t *)(var_0 + (-120L));
    *var_11 = rdx;
    var_12 = (uint64_t *)(var_0 + (-112L));
    *var_12 = rcx;
    var_13 = var_0 + (-132L);
    *(uint32_t *)var_13 = (uint32_t)r8;
    var_14 = var_0 + (-104L);
    var_15 = (uint32_t *)var_14;
    *var_15 = (uint32_t)r9;
    var_16 = var_0 + (-124L);
    var_17 = (unsigned char *)var_16;
    *var_17 = (unsigned char)r8;
    var_18 = var_0 + (-64L);
    var_19 = (uint64_t *)var_18;
    *var_19 = 0UL;
    var_20 = var_0 + (-144L);
    *(uint64_t *)var_20 = 4265129UL;
    indirect_placeholder();
    var_21 = var_0 + (-88L);
    var_22 = (uint64_t *)var_21;
    *var_22 = var_1;
    var_23 = rsi + 1UL;
    var_24 = *(unsigned char *)var_23;
    rcx3_0 = 0UL;
    rcx3_1 = 0UL;
    rax_1_ph_in = var_24;
    rbx_2_ph = var_23;
    r14_0_ph = var_23;
    r12_0_ph = 0UL;
    rcx3_4_ph = rcx;
    cc_src2_0_ph = var_8;
    local_sp_14_ph = var_20;
    if (var_24 == '\x00') {
        return;
    }
    var_25 = var_0 + (-80L);
    var_26 = (uint64_t *)var_25;
    *var_26 = var_18;
    var_27 = var_0 + (-96L);
    var_28 = (uint32_t *)var_27;
    *var_28 = (var_10 + (-63));
    while (1U)
        {
            r12_0 = r12_0_ph;
            cc_src2_2 = cc_src2_0_ph;
            local_sp_16 = local_sp_14_ph;
            rax_1_in = rax_1_ph_in;
            rbx_2 = rbx_2_ph;
            rcx3_4 = rcx3_4_ph;
            cc_src2_1 = cc_src2_0_ph;
            local_sp_15 = local_sp_14_ph;
            r14_1 = r14_0_ph;
            while (1U)
                {
                    rax_1 = (uint64_t)rax_1_in;
                    rax_1_in = (unsigned char)'(';
                    rax_4 = rbx_2;
                    r12_1 = r12_0;
                    rcx3_5 = rcx3_4;
                    if ((uint64_t)(rax_1_in + '\xa5') != 0UL) {
                        loop_state_var = 1U;
                        break;
                    }
                    var_29 = ((uint64_t)((rax_1_in + '\xc1') & '\xfe') == 0UL);
                    var_30 = (rcx3_4 & (-256L)) | var_29;
                    var_31 = ((uint64_t)((rax_1_in + '\xd6') & '\xfe') == 0UL);
                    var_32 = var_29 || var_31;
                    var_33 = var_30 | var_31;
                    rcx3_4 = var_33;
                    rcx3_5 = var_33;
                    if (!var_32) {
                        if ((uint64_t)(rax_1_in + '\xdf') != 0UL) {
                            loop_state_var = 0U;
                            break;
                        }
                    }
                    var_34 = rbx_2 + 1UL;
                    rbx_2 = var_34;
                    if (*(unsigned char *)var_34 != '(') {
                        loop_state_var = 0U;
                        break;
                    }
                    r12_0 = r12_0 + 1UL;
                    continue;
                }
            switch_state_var = 0;
            switch (loop_state_var) {
              case 0U:
                {
                    if ((uint64_t)(rax_1_in + '\xd7') == 0UL) {
                        r12_1 = r12_0 + (-1L);
                        if (r12_0 != 0UL) {
                            if ((*var_9 + (-63)) <= 1U) {
                                loop_state_var = 1U;
                                switch_state_var = 1;
                                break;
                            }
                            *var_22 = ((rbx_2 - r14_0_ph) + 1UL);
                            loop_state_var = 1U;
                            switch_state_var = 1;
                            break;
                        }
                    }
                    if (!(((uint64_t)(rax_1_in + '\x84') == 0UL) && (r12_0 == 0UL))) {
                        var_35 = (((*var_28 > 1U) ? ((rbx_2 - r14_0_ph) + 1UL) : *var_22) + 15UL) & (-8L);
                        r12_1 = 0UL;
                        if ((var_35 + (-8L)) <= 7991UL) {
                            loop_state_var = 0U;
                            switch_state_var = 1;
                            break;
                        }
                        var_36 = (var_35 + 30UL) & (-16L);
                        var_37 = local_sp_14_ph - var_36;
                        var_38 = (var_37 + 15UL) & (-16L);
                        var_39 = var_37 + (-8L);
                        *(uint64_t *)var_39 = 4265585UL;
                        indirect_placeholder();
                        *(unsigned char *)var_36 = (unsigned char)'\x00';
                        *(uint64_t *)var_38 = 0UL;
                        **(uint64_t **)var_25 = var_38;
                        *var_26 = var_38;
                        r14_1 = rbx_2 + 1UL;
                        local_sp_16 = var_39;
                    }
                }
                break;
              case 1U:
                {
                    var_127 = *(uint32_t *)6470648UL;
                    var_131 = var_127;
                    if (var_127 == 0U) {
                        var_128 = local_sp_14_ph + (-8L);
                        *(uint64_t *)var_128 = 4265199UL;
                        indirect_placeholder();
                        var_129 = helper_cc_compute_c_wrapper(rax_1 + (-1L), 1UL, cc_src2_0_ph, 17U);
                        var_130 = (0U - (uint32_t)var_129) | 1U;
                        *(uint32_t *)6470648UL = var_130;
                        var_131 = var_130;
                        cc_src2_1 = var_129;
                        local_sp_15 = var_128;
                    }
                    var_132 = rbx_2 + 1UL;
                    var_133 = *(unsigned char *)var_132;
                    var_135 = var_133;
                    storemerge3 = var_132;
                    cc_src2_2 = cc_src2_1;
                    local_sp_16 = local_sp_15;
                    if ((uint64_t)(var_133 + '\xdf') == 0UL) {
                        var_134 = rbx_2 + 2UL;
                        var_135 = *(unsigned char *)var_134;
                        storemerge3 = var_134;
                    } else {
                        if ((uint64_t)(var_133 + '\xa2') != 0UL & (int)var_131 < (int)0U) {
                            var_134 = rbx_2 + 2UL;
                            var_135 = *(unsigned char *)var_134;
                            storemerge3 = var_134;
                        }
                    }
                    rax_2 = storemerge3 + (var_135 == ']');
                    while (1U)
                        {
                            rax_4 = rax_2;
                            switch_state_var = 0;
                            switch (*(unsigned char *)rax_2) {
                              case ']':
                                {
                                    loop_state_var = 0U;
                                    switch_state_var = 1;
                                    break;
                                }
                                break;
                              case '\x00':
                                {
                                    loop_state_var = 1U;
                                    switch_state_var = 1;
                                    break;
                                }
                                break;
                              default:
                                {
                                    rax_2 = rax_2 + 1UL;
                                    continue;
                                }
                                break;
                            }
                            if (switch_state_var)
                                break;
                        }
                    switch_state_var = 0;
                    switch (loop_state_var) {
                      case 0U:
                        {
                            var_136 = rax_4 + 1UL;
                            var_137 = *(unsigned char *)var_136;
                            rax_1_ph_in = var_137;
                            rbx_2_ph = var_136;
                            r14_0_ph = r14_1;
                            r12_0_ph = r12_1;
                            rcx3_4_ph = rcx3_5;
                            cc_src2_0_ph = cc_src2_2;
                            local_sp_14_ph = local_sp_16;
                            if (var_137 == '\x00') {
                                continue;
                            }
                            loop_state_var = 0U;
                            switch_state_var = 1;
                            break;
                        }
                        break;
                      case 1U:
                        {
                            loop_state_var = 0U;
                            switch_state_var = 1;
                            break;
                        }
                        break;
                    }
                    if (switch_state_var)
                        break;
                }
                break;
            }
            if (switch_state_var)
                break;
        }
    var_40 = (*var_22 + 15UL) & (-8L);
    if ((var_40 + (-8L)) > 7991UL) {
        return;
    }
    var_41 = (var_40 + 30UL) & (-16L);
    var_42 = local_sp_14_ph - var_41;
    var_43 = (var_42 + 15UL) & (-16L);
    var_44 = var_42 + (-8L);
    *(uint64_t *)var_44 = 4265453UL;
    indirect_placeholder();
    *(unsigned char *)var_41 = (unsigned char)'\x00';
    *(uint64_t *)var_43 = 0UL;
    **(uint64_t **)var_25 = var_43;
    var_45 = *var_19;
    *var_26 = var_45;
    local_sp_3 = var_44;
    if (var_45 == 0UL) {
        var_46 = var_42 + (-16L);
        *(uint64_t *)var_46 = 4265672UL;
        indirect_placeholder();
        local_sp_3 = var_46;
    }
    local_sp_4 = local_sp_3;
    if (*(unsigned char *)(rbx_2 + (-1L)) == ')') {
        var_47 = local_sp_3 + (-8L);
        *(uint64_t *)var_47 = 4265703UL;
        indirect_placeholder();
        local_sp_4 = var_47;
    }
    var_48 = *var_9 + (-33);
    local_sp_5 = local_sp_4;
    local_sp_7 = local_sp_4;
    local_sp_11 = local_sp_4;
    if (var_48 <= 31U) {
        *(uint64_t *)(local_sp_4 + (-8L)) = 4266369UL;
        indirect_placeholder();
        return;
    }
    switch (*(uint64_t *)(((uint64_t)var_48 << 3UL) + 4344656UL)) {
      case 4266344UL:
        {
            *(uint64_t *)(local_sp_4 + (-8L)) = 4266369UL;
            indirect_placeholder();
        }
        break;
      case 4265727UL:
        {
            var_108 = *var_11;
            r13_0 = var_108;
            if (var_108 <= *var_12) {
                var_109 = *var_15;
                var_110 = (uint64_t)var_109;
                var_111 = ((var_110 & 1UL) == 0UL) ? (uint64_t)(var_109 & (-5)) : var_110;
                var_112 = (uint32_t *)var_21;
                *var_112 = (uint32_t)var_111;
                var_113 = (uint64_t)*var_17;
                *var_28 = (var_109 & 5U);
                var_114 = (uint64_t *)var_14;
                *var_114 = rbx_2;
                rcx3_3 = var_113;
                while (1U)
                    {
                        var_115 = *var_26;
                        local_sp_12 = local_sp_11;
                        rbx_1 = var_115;
                        local_sp_13 = local_sp_11;
                        if (var_115 == 0UL) {
                            rcx3_3 = 0UL;
                            if (r13_0 != var_108 & *(unsigned char *)(r13_0 + (-1L)) == '/') {
                                rcx3_3 = (*var_28 == 5U);
                            }
                            var_120 = (uint64_t)*var_112;
                            var_121 = *var_12;
                            var_122 = *var_114;
                            var_123 = local_sp_12 + (-8L);
                            *(uint64_t *)var_123 = 4266312UL;
                            var_124 = indirect_placeholder_8(var_121, var_122, rcx3_3, var_120, r13_0);
                            local_sp_0 = var_123;
                            if ((uint64_t)(uint32_t)var_124 == 0UL) {
                                break;
                            }
                            var_125 = r13_0 + 1UL;
                            var_126 = helper_cc_compute_c_wrapper(*var_12 - var_125, var_125, cc_src2_0_ph, 17U);
                            r13_0 = var_125;
                            local_sp_11 = local_sp_0;
                            if (var_126 != 0UL) {
                                continue;
                            }
                            break;
                        }
                        while (1U)
                            {
                                var_116 = rbx_1 + 8UL;
                                var_117 = local_sp_13 + (-8L);
                                *(uint64_t *)var_117 = 4266238UL;
                                var_118 = indirect_placeholder_8(r13_0, var_116, var_113, var_111, var_108);
                                local_sp_0 = var_117;
                                local_sp_12 = var_117;
                                local_sp_13 = var_117;
                                if ((uint64_t)(uint32_t)var_118 != 0UL) {
                                    loop_state_var = 1U;
                                    break;
                                }
                                var_119 = *(uint64_t *)rbx_1;
                                rbx_1 = var_119;
                                if (var_119 == 0UL) {
                                    continue;
                                }
                                loop_state_var = 0U;
                                break;
                            }
                        switch_state_var = 0;
                        switch (loop_state_var) {
                          case 0U:
                            {
                                break;
                            }
                            break;
                          case 1U:
                            {
                                var_125 = r13_0 + 1UL;
                                var_126 = helper_cc_compute_c_wrapper(*var_12 - var_125, var_125, cc_src2_0_ph, 17U);
                                r13_0 = var_125;
                                local_sp_11 = local_sp_0;
                                if (var_126 != 0UL) {
                                    continue;
                                }
                                switch_state_var = 1;
                                break;
                            }
                            break;
                        }
                        if (switch_state_var)
                            break;
                    }
            }
        }
        break;
      case 4265798UL:
        {
            var_65 = (uint64_t)*(unsigned char *)var_13;
            var_66 = (uint64_t)*var_15;
            var_67 = *var_12;
            var_68 = *var_11;
            var_69 = local_sp_4 + (-8L);
            *(uint64_t *)var_69 = 4265822UL;
            var_70 = indirect_placeholder_8(var_67, rbx_2, var_65, var_66, var_68);
            local_sp_7 = var_69;
            if ((uint64_t)(uint32_t)var_70 == 0UL) {
                return;
            }
            var_71 = *var_15;
            var_72 = (uint64_t)var_71;
            var_73 = var_71 & (-5);
            var_74 = (uint64_t)var_73;
            var_75 = (uint32_t *)var_21;
            *var_75 = var_73;
            var_76 = ((var_72 & 1UL) == 0UL);
            *var_9 = (uint32_t)(var_76 ? var_74 : var_72);
            var_77 = *var_17;
            var_78 = (uint64_t)var_77;
            var_79 = rsi + (-1L);
            var_80 = var_71 & 5U;
            var_81 = (uint32_t *)var_16;
            *var_81 = var_80;
            var_82 = (uint64_t *)var_27;
            *var_82 = rbx_2;
            var_83 = *var_12;
            *var_12 = var_79;
            var_84 = *var_11;
            var_85 = (var_84 > var_83);
            var_86 = (uint32_t *)var_25;
            storemerge_in_in_pre_phi = var_75;
            local_sp_8 = local_sp_7;
            rbx_0 = var_84;
            while (1U)
                {
                    local_sp_9 = local_sp_8;
                    local_sp_10 = local_sp_8;
                    if (var_85) {
                        var_107 = **(uint64_t **)var_18;
                        *var_19 = var_107;
                        local_sp_8 = local_sp_10;
                        if (var_107 != 0UL) {
                            continue;
                        }
                        break;
                    }
                    while (1U)
                        {
                            *var_86 = (uint32_t)var_77;
                            var_87 = *var_19 + 8UL;
                            if (var_76) {
                                var_91 = (uint64_t)*var_75;
                                var_92 = local_sp_9 + (-8L);
                                *(uint64_t *)var_92 = 4265940UL;
                                var_93 = indirect_placeholder_8(rbx_0, var_87, var_78, var_91, var_84);
                                local_sp_1 = var_92;
                                local_sp_2 = var_92;
                                if ((uint64_t)(uint32_t)var_93 != 0UL) {
                                    var_105 = rbx_0 + 1UL;
                                    var_106 = helper_cc_compute_c_wrapper(var_83 - var_105, var_105, cc_src2_0_ph, 17U);
                                    rbx_0 = var_105;
                                    local_sp_9 = local_sp_2;
                                    local_sp_10 = local_sp_2;
                                    if (var_106 == 0UL) {
                                        continue;
                                    }
                                    loop_state_var = 0U;
                                    break;
                                }
                            }
                            var_88 = (uint64_t)*var_15;
                            var_89 = local_sp_9 + (-8L);
                            *(uint64_t *)var_89 = 4266451UL;
                            var_90 = indirect_placeholder_8(rbx_0, var_87, var_78, var_88, var_84);
                            storemerge_in_in_pre_phi = var_15;
                            local_sp_1 = var_89;
                            local_sp_2 = var_89;
                            if ((uint64_t)(uint32_t)var_90 != 0UL) {
                                var_105 = rbx_0 + 1UL;
                                var_106 = helper_cc_compute_c_wrapper(var_83 - var_105, var_105, cc_src2_0_ph, 17U);
                                rbx_0 = var_105;
                                local_sp_9 = local_sp_2;
                                local_sp_10 = local_sp_2;
                                if (var_106 == 0UL) {
                                    continue;
                                }
                                loop_state_var = 0U;
                                break;
                            }
                            storemerge = (uint64_t)*storemerge_in_in_pre_phi;
                            if (rbx_0 == var_84) {
                                var_101 = (uint64_t)*var_86;
                                var_102 = *var_82;
                                var_103 = local_sp_1 + (-8L);
                                *(uint64_t *)var_103 = 4266482UL;
                                var_104 = indirect_placeholder_8(var_83, var_102, var_101, storemerge, var_84);
                                local_sp_2 = var_103;
                                if ((uint64_t)(uint32_t)var_104 != 0UL) {
                                    loop_state_var = 1U;
                                    break;
                                }
                            }
                            var_94 = (unsigned char *)(rbx_0 + (-1L));
                            if (*var_94 == '/') {
                                rcx3_1 = (*var_81 == 5U);
                            }
                            var_95 = *var_82;
                            *(uint64_t *)(local_sp_1 + (-8L)) = 4266002UL;
                            var_96 = indirect_placeholder_8(var_83, var_95, rcx3_1, storemerge, rbx_0);
                            if ((uint64_t)(uint32_t)var_96 != 0UL) {
                                loop_state_var = 1U;
                                break;
                            }
                            if (*var_94 == '/') {
                                rcx3_0 = (*var_81 == 5U);
                            }
                            var_97 = (uint64_t)*var_9;
                            var_98 = *var_12;
                            var_99 = local_sp_1 + (-16L);
                            *(uint64_t *)var_99 = 4266047UL;
                            var_100 = indirect_placeholder_8(var_83, var_98, rcx3_0, var_97, rbx_0);
                            local_sp_2 = var_99;
                            if ((uint64_t)(uint32_t)var_100 != 0UL) {
                                loop_state_var = 1U;
                                break;
                            }
                            var_105 = rbx_0 + 1UL;
                            var_106 = helper_cc_compute_c_wrapper(var_83 - var_105, var_105, cc_src2_0_ph, 17U);
                            rbx_0 = var_105;
                            local_sp_9 = local_sp_2;
                            local_sp_10 = local_sp_2;
                            if (var_106 == 0UL) {
                                continue;
                            }
                            loop_state_var = 0U;
                            break;
                        }
                    switch_state_var = 0;
                    switch (loop_state_var) {
                      case 1U:
                        {
                            switch_state_var = 1;
                            break;
                        }
                        break;
                      case 0U:
                        {
                            break;
                        }
                        break;
                    }
                    if (switch_state_var)
                        break;
                }
        }
        break;
      case 4265830UL:
        {
            var_71 = *var_15;
            var_72 = (uint64_t)var_71;
            var_73 = var_71 & (-5);
            var_74 = (uint64_t)var_73;
            var_75 = (uint32_t *)var_21;
            *var_75 = var_73;
            var_76 = ((var_72 & 1UL) == 0UL);
            *var_9 = (uint32_t)(var_76 ? var_74 : var_72);
            var_77 = *var_17;
            var_78 = (uint64_t)var_77;
            var_79 = rsi + (-1L);
            var_80 = var_71 & 5U;
            var_81 = (uint32_t *)var_16;
            *var_81 = var_80;
            var_82 = (uint64_t *)var_27;
            *var_82 = rbx_2;
            var_83 = *var_12;
            *var_12 = var_79;
            var_84 = *var_11;
            var_85 = (var_84 > var_83);
            var_86 = (uint32_t *)var_25;
            storemerge_in_in_pre_phi = var_75;
            local_sp_8 = local_sp_7;
            rbx_0 = var_84;
            while (1U)
                {
                    local_sp_9 = local_sp_8;
                    local_sp_10 = local_sp_8;
                    if (var_85) {
                        var_107 = **(uint64_t **)var_18;
                        *var_19 = var_107;
                        local_sp_8 = local_sp_10;
                        if (var_107 != 0UL) {
                            continue;
                        }
                        break;
                    }
                    while (1U)
                        {
                            *var_86 = (uint32_t)var_77;
                            var_87 = *var_19 + 8UL;
                            if (var_76) {
                                var_91 = (uint64_t)*var_75;
                                var_92 = local_sp_9 + (-8L);
                                *(uint64_t *)var_92 = 4265940UL;
                                var_93 = indirect_placeholder_8(rbx_0, var_87, var_78, var_91, var_84);
                                local_sp_1 = var_92;
                                local_sp_2 = var_92;
                                if ((uint64_t)(uint32_t)var_93 != 0UL) {
                                    var_105 = rbx_0 + 1UL;
                                    var_106 = helper_cc_compute_c_wrapper(var_83 - var_105, var_105, cc_src2_0_ph, 17U);
                                    rbx_0 = var_105;
                                    local_sp_9 = local_sp_2;
                                    local_sp_10 = local_sp_2;
                                    if (var_106 == 0UL) {
                                        continue;
                                    }
                                    loop_state_var = 0U;
                                    break;
                                }
                            }
                            var_88 = (uint64_t)*var_15;
                            var_89 = local_sp_9 + (-8L);
                            *(uint64_t *)var_89 = 4266451UL;
                            var_90 = indirect_placeholder_8(rbx_0, var_87, var_78, var_88, var_84);
                            storemerge_in_in_pre_phi = var_15;
                            local_sp_1 = var_89;
                            local_sp_2 = var_89;
                            if ((uint64_t)(uint32_t)var_90 != 0UL) {
                                var_105 = rbx_0 + 1UL;
                                var_106 = helper_cc_compute_c_wrapper(var_83 - var_105, var_105, cc_src2_0_ph, 17U);
                                rbx_0 = var_105;
                                local_sp_9 = local_sp_2;
                                local_sp_10 = local_sp_2;
                                if (var_106 == 0UL) {
                                    continue;
                                }
                                loop_state_var = 0U;
                                break;
                            }
                            storemerge = (uint64_t)*storemerge_in_in_pre_phi;
                            if (rbx_0 == var_84) {
                                var_101 = (uint64_t)*var_86;
                                var_102 = *var_82;
                                var_103 = local_sp_1 + (-8L);
                                *(uint64_t *)var_103 = 4266482UL;
                                var_104 = indirect_placeholder_8(var_83, var_102, var_101, storemerge, var_84);
                                local_sp_2 = var_103;
                                if ((uint64_t)(uint32_t)var_104 != 0UL) {
                                    loop_state_var = 1U;
                                    break;
                                }
                            }
                            var_94 = (unsigned char *)(rbx_0 + (-1L));
                            if (*var_94 == '/') {
                                rcx3_1 = (*var_81 == 5U);
                            }
                            var_95 = *var_82;
                            *(uint64_t *)(local_sp_1 + (-8L)) = 4266002UL;
                            var_96 = indirect_placeholder_8(var_83, var_95, rcx3_1, storemerge, rbx_0);
                            if ((uint64_t)(uint32_t)var_96 != 0UL) {
                                loop_state_var = 1U;
                                break;
                            }
                            if (*var_94 == '/') {
                                rcx3_0 = (*var_81 == 5U);
                            }
                            var_97 = (uint64_t)*var_9;
                            var_98 = *var_12;
                            var_99 = local_sp_1 + (-16L);
                            *(uint64_t *)var_99 = 4266047UL;
                            var_100 = indirect_placeholder_8(var_83, var_98, rcx3_0, var_97, rbx_0);
                            local_sp_2 = var_99;
                            if ((uint64_t)(uint32_t)var_100 != 0UL) {
                                loop_state_var = 1U;
                                break;
                            }
                            var_105 = rbx_0 + 1UL;
                            var_106 = helper_cc_compute_c_wrapper(var_83 - var_105, var_105, cc_src2_0_ph, 17U);
                            rbx_0 = var_105;
                            local_sp_9 = local_sp_2;
                            local_sp_10 = local_sp_2;
                            if (var_106 == 0UL) {
                                continue;
                            }
                            loop_state_var = 0U;
                            break;
                        }
                    switch_state_var = 0;
                    switch (loop_state_var) {
                      case 1U:
                        {
                            switch_state_var = 1;
                            break;
                        }
                        break;
                      case 0U:
                        {
                            break;
                        }
                        break;
                    }
                    if (switch_state_var)
                        break;
                }
        }
        break;
      case 4266095UL:
        {
            var_49 = (uint64_t)*(unsigned char *)var_13;
            var_50 = (uint64_t)*var_15;
            var_51 = *var_12;
            var_52 = *var_11;
            var_53 = local_sp_4 + (-8L);
            *(uint64_t *)var_53 = 4266119UL;
            var_54 = indirect_placeholder_8(var_51, rbx_2, var_49, var_50, var_52);
            local_sp_5 = var_53;
            if ((uint64_t)(uint32_t)var_54 == 0UL) {
                return;
            }
            var_55 = *var_15;
            var_56 = (uint64_t)var_55;
            var_57 = ((var_56 & 1UL) == 0UL) ? (uint64_t)(var_55 & (-5)) : var_56;
            var_58 = (uint64_t)*var_17;
            var_59 = *var_12;
            rax_0 = var_56;
            local_sp_6 = local_sp_5;
            var_60 = *(uint64_t **)var_18;
            *(uint64_t *)(local_sp_6 + (-8L)) = 4266168UL;
            indirect_placeholder();
            var_61 = *var_11;
            var_62 = local_sp_6 + (-16L);
            *(uint64_t *)var_62 = 4266189UL;
            var_63 = indirect_placeholder_8(var_59, rax_0, var_58, var_57, var_61);
            local_sp_6 = var_62;
            while ((uint64_t)(uint32_t)var_63 != 0UL)
                {
                    var_64 = *var_60;
                    *var_19 = var_64;
                    rax_0 = var_64;
                    if (var_64 == 0UL) {
                        break;
                    }
                    var_60 = *(uint64_t **)var_18;
                    *(uint64_t *)(local_sp_6 + (-8L)) = 4266168UL;
                    indirect_placeholder();
                    var_61 = *var_11;
                    var_62 = local_sp_6 + (-16L);
                    *(uint64_t *)var_62 = 4266189UL;
                    var_63 = indirect_placeholder_8(var_59, rax_0, var_58, var_57, var_61);
                    local_sp_6 = var_62;
                }
        }
        break;
      case 4266127UL:
        {
            var_55 = *var_15;
            var_56 = (uint64_t)var_55;
            var_57 = ((var_56 & 1UL) == 0UL) ? (uint64_t)(var_55 & (-5)) : var_56;
            var_58 = (uint64_t)*var_17;
            var_59 = *var_12;
            rax_0 = var_56;
            local_sp_6 = local_sp_5;
            var_60 = *(uint64_t **)var_18;
            *(uint64_t *)(local_sp_6 + (-8L)) = 4266168UL;
            indirect_placeholder();
            var_61 = *var_11;
            var_62 = local_sp_6 + (-16L);
            *(uint64_t *)var_62 = 4266189UL;
            var_63 = indirect_placeholder_8(var_59, rax_0, var_58, var_57, var_61);
            local_sp_6 = var_62;
            while ((uint64_t)(uint32_t)var_63 != 0UL)
                {
                    var_64 = *var_60;
                    *var_19 = var_64;
                    rax_0 = var_64;
                    if (var_64 == 0UL) {
                        break;
                    }
                    var_60 = *(uint64_t **)var_18;
                    *(uint64_t *)(local_sp_6 + (-8L)) = 4266168UL;
                    indirect_placeholder();
                    var_61 = *var_11;
                    var_62 = local_sp_6 + (-16L);
                    *(uint64_t *)var_62 = 4266189UL;
                    var_63 = indirect_placeholder_8(var_59, rax_0, var_58, var_57, var_61);
                    local_sp_6 = var_62;
                }
        }
        break;
      default:
        {
            abort();
        }
        break;
    }
}

typedef unsigned char __u_char;
typedef unsigned short __u_short;
typedef unsigned int __u_int;
typedef unsigned long __u_long;
typedef signed char __int8_t;
typedef unsigned char __uint8_t;
typedef short __int16_t;
typedef unsigned short __uint16_t;
typedef int __int32_t;
typedef unsigned int __uint32_t;
typedef long __int64_t;
typedef unsigned long __uint64_t;
typedef __int8_t __int_least8_t;
typedef __uint8_t __uint_least8_t;
typedef __int16_t __int_least16_t;
typedef __uint16_t __uint_least16_t;
typedef __int32_t __int_least32_t;
typedef __uint32_t __uint_least32_t;
typedef __int64_t __int_least64_t;
typedef __uint64_t __uint_least64_t;
typedef long __quad_t;
typedef unsigned long __u_quad_t;
typedef long __intmax_t;
typedef unsigned long __uintmax_t;
typedef unsigned long __dev_t;
typedef unsigned int __uid_t;
typedef unsigned int __gid_t;
typedef unsigned long __ino_t;
typedef unsigned long __ino64_t;
typedef unsigned int __mode_t;
typedef unsigned long __nlink_t;
typedef long __off_t;
typedef long __off64_t;
typedef int __pid_t;
typedef struct {
    int __val[2];
} __fsid_t;
typedef long __clock_t;
typedef unsigned long __rlim_t;
typedef unsigned long __rlim64_t;
typedef unsigned int __id_t;
typedef long __time_t;
typedef unsigned int __useconds_t;
typedef long __suseconds_t;
typedef int __daddr_t;
typedef int __key_t;
typedef int __clockid_t;
typedef void *__timer_t;
typedef long __blksize_t;
typedef long __blkcnt_t;
typedef long __blkcnt64_t;
typedef unsigned long __fsblkcnt_t;
typedef unsigned long __fsblkcnt64_t;
typedef unsigned long __fsfilcnt_t;
typedef unsigned long __fsfilcnt64_t;
typedef long __fsword_t;
typedef long __ssize_t;
typedef long __syscall_slong_t;
typedef unsigned long __syscall_ulong_t;
typedef __off64_t __loff_t;
typedef char *__caddr_t;
typedef long __intptr_t;
typedef unsigned int __socklen_t;
typedef int __sig_atomic_t;
typedef __int8_t int8_t;
typedef __int16_t int16_t;
typedef __int32_t int32_t;
typedef __int64_t int64_t;
typedef __uint8_t uint8_t;
typedef __uint16_t uint16_t;
typedef __uint32_t uint32_t;
typedef __uint64_t uint64_t;
typedef __int_least8_t int_least8_t;
typedef __int_least16_t int_least16_t;
typedef __int_least32_t int_least32_t;
typedef __int_least64_t int_least64_t;
typedef __uint_least8_t uint_least8_t;
typedef __uint_least16_t uint_least16_t;
typedef __uint_least32_t uint_least32_t;
typedef __uint_least64_t uint_least64_t;
typedef signed char int_fast8_t;
typedef long int_fast16_t;
typedef long int_fast32_t;
typedef long int_fast64_t;
typedef unsigned char uint_fast8_t;
typedef unsigned long uint_fast16_t;
typedef unsigned long uint_fast32_t;
typedef unsigned long uint_fast64_t;
typedef long intptr_t;
typedef unsigned long uintptr_t;
typedef __intmax_t intmax_t;
typedef __uintmax_t uintmax_t;
typedef _Bool bool;
extern uint64_t revng_init_local_sp(uint64_t param_0);
extern void indirect_placeholder(void);
extern uint64_t init_rbx(void);
extern uint64_t init_rbp(void);
extern uint64_t init_cc_src2(void);
extern uint64_t init_rax(void);
extern uint64_t indirect_placeholder_5(uint64_t param_0);
extern uint64_t helper_cc_compute_c_wrapper(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint32_t param_3);
extern uint64_t init_rsi(void);
extern uint64_t init_rcx(void);
uint64_t bb_end_pattern(uint64_t rdi) {
    uint64_t var_0;
    uint64_t var_1;
    uint64_t var_2;
    uint64_t var_3;
    uint64_t var_4;
    uint64_t var_5;
    uint64_t var_6;
    uint64_t cc_src2_1;
    uint64_t local_sp_0_ph_ph;
    uint64_t rax_0_ph_ph;
    uint64_t rbx_0_ph_ph;
    uint64_t rsi_0;
    uint64_t rcx_0_ph_ph;
    uint64_t cc_src2_0_ph_ph;
    uint64_t rsi_0_ph_ph;
    uint64_t local_sp_0_ph;
    uint64_t rax_0_ph;
    uint64_t rbx_0_ph;
    uint64_t rcx_0_ph;
    uint64_t rsi_0_ph;
    uint64_t rax_0;
    uint64_t rbx_0;
    uint64_t rcx_0;
    uint64_t var_7;
    unsigned char var_8;
    uint64_t var_9;
    uint64_t rax_1;
    uint32_t var_20;
    uint32_t var_24;
    uint64_t var_21;
    uint64_t var_22;
    uint32_t var_23;
    uint64_t local_sp_1;
    uint64_t var_25;
    unsigned char var_26;
    uint64_t storemerge;
    unsigned char var_28;
    uint64_t var_27;
    uint64_t var_29;
    uint64_t rbx_1;
    bool var_10;
    uint64_t var_11;
    uint64_t var_12;
    bool var_13;
    uint64_t var_14;
    uint64_t var_15;
    uint64_t var_16;
    uint64_t var_17;
    uint64_t var_18;
    uint64_t var_19;
    unsigned int loop_state_var;
    bool switch_state_var;
    var_0 = revng_init_local_sp(0UL);
    var_1 = init_rax();
    var_2 = init_rbx();
    var_3 = init_rbp();
    var_4 = init_rcx();
    var_5 = init_cc_src2();
    var_6 = init_rsi();
    *(uint64_t *)(var_0 + (-8L)) = var_3;
    *(uint64_t *)(var_0 + (-16L)) = var_2;
    local_sp_0_ph_ph = var_0 + (-24L);
    rax_0_ph_ph = var_1;
    rbx_0_ph_ph = rdi;
    rcx_0_ph_ph = var_4;
    cc_src2_0_ph_ph = var_5;
    rsi_0_ph_ph = var_6;
    rax_1 = rdi;
    while (1U)
        {
            cc_src2_1 = cc_src2_0_ph_ph;
            local_sp_0_ph = local_sp_0_ph_ph;
            rax_0_ph = rax_0_ph_ph;
            rbx_0_ph = rbx_0_ph_ph;
            rcx_0_ph = rcx_0_ph_ph;
            rsi_0_ph = rsi_0_ph_ph;
            while (1U)
                {
                    rsi_0 = rsi_0_ph;
                    rax_0 = rax_0_ph;
                    rbx_0 = rbx_0_ph;
                    rcx_0 = rcx_0_ph;
                    local_sp_1 = local_sp_0_ph;
                    while (1U)
                        {
                            var_7 = rbx_0 + 1UL;
                            var_8 = *(unsigned char *)var_7;
                            var_9 = (uint64_t)var_8;
                            rcx_0_ph_ph = rcx_0;
                            rsi_0_ph_ph = rsi_0;
                            rbx_0 = var_7;
                            if (var_8 != '\x00') {
                                loop_state_var = 0U;
                                break;
                            }
                            if ((uint64_t)(var_8 + '\xa5') != 0UL) {
                                loop_state_var = 1U;
                                break;
                            }
                            var_10 = ((uint64_t)((var_8 + '\xc1') & '\xfe') == 0UL);
                            var_11 = (rsi_0 & (-256L)) | var_10;
                            var_12 = (uint64_t)((uint32_t)var_9 + (-42));
                            var_13 = ((uint64_t)((unsigned char)var_12 & '\xfe') == 0UL);
                            var_14 = var_13;
                            var_15 = (rcx_0 & (-256L)) | var_14;
                            var_16 = var_11 | var_14;
                            rsi_0 = var_16;
                            rcx_0_ph = var_15;
                            rsi_0_ph = var_16;
                            rax_0 = var_12;
                            rcx_0 = var_15;
                            if (var_10 || var_13) {
                                var_17 = rbx_0 + 2UL;
                                if ((uint64_t)(var_8 + '\xdf') != 0UL & *(unsigned char *)var_17 != '(') {
                                    loop_state_var = 2U;
                                    break;
                                }
                            }
                            var_17 = rbx_0 + 2UL;
                            if (*(unsigned char *)var_17 != '(') {
                                loop_state_var = 2U;
                                break;
                            }
                            if ((uint64_t)(var_8 + '\xd7') == 0UL) {
                                continue;
                            }
                            rax_1 = rbx_0 + 2UL;
                            loop_state_var = 0U;
                            break;
                        }
                    switch_state_var = 0;
                    switch (loop_state_var) {
                      case 2U:
                        {
                            var_18 = local_sp_0_ph + (-8L);
                            *(uint64_t *)var_18 = 4260717UL;
                            var_19 = indirect_placeholder_5(var_17);
                            local_sp_0_ph = var_18;
                            rax_0_ph = var_19;
                            rbx_0_ph = var_19;
                            continue;
                        }
                        break;
                      case 0U:
                        {
                            loop_state_var = 0U;
                            switch_state_var = 1;
                            break;
                        }
                        break;
                      case 1U:
                        {
                            loop_state_var = 1U;
                            switch_state_var = 1;
                            break;
                        }
                        break;
                    }
                    if (switch_state_var)
                        break;
                }
            switch_state_var = 0;
            switch (loop_state_var) {
              case 0U:
                {
                    switch_state_var = 1;
                    break;
                }
                break;
              case 1U:
                {
                    var_20 = *(uint32_t *)6470648UL;
                    var_24 = var_20;
                    if (var_20 == 0U) {
                        var_21 = local_sp_0_ph + (-8L);
                        *(uint64_t *)var_21 = 4260597UL;
                        indirect_placeholder();
                        var_22 = helper_cc_compute_c_wrapper(rax_0 + (-1L), 1UL, cc_src2_0_ph_ph, 17U);
                        var_23 = (0U - (uint32_t)var_22) | 1U;
                        *(uint32_t *)6470648UL = var_23;
                        var_24 = var_23;
                        local_sp_1 = var_21;
                        cc_src2_1 = var_22;
                    }
                    var_25 = rbx_0 + 2UL;
                    var_26 = *(unsigned char *)var_25;
                    local_sp_0_ph_ph = local_sp_1;
                    cc_src2_0_ph_ph = cc_src2_1;
                    var_28 = var_26;
                    storemerge = var_25;
                    if ((uint64_t)(var_26 + '\xdf') == 0UL) {
                        var_27 = rbx_0 + 3UL;
                        var_28 = *(unsigned char *)var_27;
                        storemerge = var_27;
                    } else {
                        if ((uint64_t)(var_26 + '\xa2') != 0UL & (int)var_24 < (int)0U) {
                            var_27 = rbx_0 + 3UL;
                            var_28 = *(unsigned char *)var_27;
                            storemerge = var_27;
                        }
                    }
                    var_29 = (var_28 == ']');
                    rax_0_ph_ph = var_29;
                    rbx_1 = storemerge + var_29;
                    while (1U)
                        {
                            rbx_0_ph_ph = rbx_1;
                            switch_state_var = 0;
                            switch (*(unsigned char *)rbx_1) {
                              case ']':
                                {
                                    loop_state_var = 0U;
                                    switch_state_var = 1;
                                    break;
                                }
                                break;
                              case '\x00':
                                {
                                    loop_state_var = 1U;
                                    switch_state_var = 1;
                                    break;
                                }
                                break;
                              default:
                                {
                                    rbx_1 = rbx_1 + 1UL;
                                    continue;
                                }
                                break;
                            }
                            if (switch_state_var)
                                break;
                        }
                    switch_state_var = 0;
                    switch (loop_state_var) {
                      case 1U:
                        {
                            switch_state_var = 1;
                            break;
                        }
                        break;
                      case 0U:
                        {
                            continue;
                        }
                        break;
                    }
                    if (switch_state_var)
                        break;
                }
                break;
            }
            if (switch_state_var)
                break;
        }
    return rax_1;
}

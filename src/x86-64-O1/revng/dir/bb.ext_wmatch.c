typedef unsigned char __u_char;
typedef unsigned short __u_short;
typedef unsigned int __u_int;
typedef unsigned long __u_long;
typedef signed char __int8_t;
typedef unsigned char __uint8_t;
typedef short __int16_t;
typedef unsigned short __uint16_t;
typedef int __int32_t;
typedef unsigned int __uint32_t;
typedef long __int64_t;
typedef unsigned long __uint64_t;
typedef __int8_t __int_least8_t;
typedef __uint8_t __uint_least8_t;
typedef __int16_t __int_least16_t;
typedef __uint16_t __uint_least16_t;
typedef __int32_t __int_least32_t;
typedef __uint32_t __uint_least32_t;
typedef __int64_t __int_least64_t;
typedef __uint64_t __uint_least64_t;
typedef long __quad_t;
typedef unsigned long __u_quad_t;
typedef long __intmax_t;
typedef unsigned long __uintmax_t;
typedef unsigned long __dev_t;
typedef unsigned int __uid_t;
typedef unsigned int __gid_t;
typedef unsigned long __ino_t;
typedef unsigned long __ino64_t;
typedef unsigned int __mode_t;
typedef unsigned long __nlink_t;
typedef long __off_t;
typedef long __off64_t;
typedef int __pid_t;
typedef struct {
    int __val[2];
} __fsid_t;
typedef long __clock_t;
typedef unsigned long __rlim_t;
typedef unsigned long __rlim64_t;
typedef unsigned int __id_t;
typedef long __time_t;
typedef unsigned int __useconds_t;
typedef long __suseconds_t;
typedef int __daddr_t;
typedef int __key_t;
typedef int __clockid_t;
typedef void *__timer_t;
typedef long __blksize_t;
typedef long __blkcnt_t;
typedef long __blkcnt64_t;
typedef unsigned long __fsblkcnt_t;
typedef unsigned long __fsblkcnt64_t;
typedef unsigned long __fsfilcnt_t;
typedef unsigned long __fsfilcnt64_t;
typedef long __fsword_t;
typedef long __ssize_t;
typedef long __syscall_slong_t;
typedef unsigned long __syscall_ulong_t;
typedef __off64_t __loff_t;
typedef char *__caddr_t;
typedef long __intptr_t;
typedef unsigned int __socklen_t;
typedef int __sig_atomic_t;
typedef __int8_t int8_t;
typedef __int16_t int16_t;
typedef __int32_t int32_t;
typedef __int64_t int64_t;
typedef __uint8_t uint8_t;
typedef __uint16_t uint16_t;
typedef __uint32_t uint32_t;
typedef __uint64_t uint64_t;
typedef __int_least8_t int_least8_t;
typedef __int_least16_t int_least16_t;
typedef __int_least32_t int_least32_t;
typedef __int_least64_t int_least64_t;
typedef __uint_least8_t uint_least8_t;
typedef __uint_least16_t uint_least16_t;
typedef __uint_least32_t uint_least32_t;
typedef __uint_least64_t uint_least64_t;
typedef signed char int_fast8_t;
typedef long int_fast16_t;
typedef long int_fast32_t;
typedef long int_fast64_t;
typedef unsigned char uint_fast8_t;
typedef unsigned long uint_fast16_t;
typedef unsigned long uint_fast32_t;
typedef unsigned long uint_fast64_t;
typedef long intptr_t;
typedef unsigned long uintptr_t;
typedef __intmax_t intmax_t;
typedef __uintmax_t uintmax_t;
typedef _Bool bool;
struct helper_divq_EAX_wrapper_ret_type;
struct type_5;
struct helper_divq_EAX_wrapper_ret_type {
    uint64_t field_0;
    uint64_t field_1;
    uint64_t field_2;
    uint64_t field_3;
    uint32_t field_4;
    uint32_t field_5;
    uint64_t field_6;
    uint32_t field_7;
    uint64_t field_8;
    uint32_t field_9;
    unsigned char field_10;
    uint32_t field_11;
    uint32_t field_12;
};
struct type_5 {
};
extern void abort(void);
extern uint64_t revng_init_local_sp(uint64_t param_0);
extern void indirect_placeholder(void);
extern uint64_t init_rbx(void);
extern uint64_t init_r14(void);
extern uint64_t init_rbp(void);
extern uint64_t init_r13(void);
extern uint64_t init_r12(void);
extern uint64_t init_cc_src2(void);
extern uint64_t init_rax(void);
extern uint64_t helper_cc_compute_c_wrapper(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint32_t param_3);
extern uint32_t init_state_0x8248(void);
extern uint64_t init_state_0x9018(void);
extern uint32_t init_state_0x9010(void);
extern uint64_t indirect_placeholder_8(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint64_t param_3, uint64_t param_4);
extern uint64_t init_r15(void);
extern uint64_t init_state_0x8408(void);
extern uint64_t init_state_0x8328(void);
extern uint64_t init_state_0x82d8(void);
extern uint32_t init_state_0x9080(void);
extern struct helper_divq_EAX_wrapper_ret_type helper_divq_EAX_wrapper(struct type_5 *param_0, uint64_t param_1, uint64_t param_2, uint64_t param_3, uint64_t param_4, uint64_t param_5, uint64_t param_6, uint64_t param_7, uint64_t param_8, uint64_t param_9, uint64_t param_10, uint64_t param_11, uint64_t param_12, uint32_t param_13, uint64_t param_14, uint32_t param_15, uint64_t param_16, uint64_t param_17, uint64_t param_18, uint32_t param_19);
extern uint64_t init_r10(void);
void bb_ext_wmatch(uint64_t rdx, uint64_t rdi, uint64_t rcx, uint64_t r8, uint64_t r9, uint64_t rsi) {
    uint64_t var_120;
    uint64_t var_124;
    uint64_t var_0;
    uint64_t var_1;
    uint64_t var_2;
    uint64_t var_3;
    uint64_t var_4;
    uint64_t var_5;
    uint64_t var_6;
    uint64_t var_7;
    uint64_t var_8;
    uint64_t var_9;
    uint32_t var_10;
    uint64_t var_11;
    uint32_t var_12;
    uint64_t var_13;
    uint64_t var_14;
    uint64_t var_15;
    uint32_t var_16;
    uint64_t var_17;
    uint32_t *var_18;
    uint32_t var_19;
    uint64_t *var_20;
    uint64_t *var_21;
    uint64_t var_22;
    uint64_t var_23;
    uint32_t *var_24;
    uint64_t var_25;
    unsigned char *var_26;
    uint64_t var_27;
    uint64_t *var_28;
    uint64_t var_29;
    uint64_t var_30;
    uint64_t *var_31;
    uint64_t var_32;
    uint32_t var_33;
    uint64_t local_sp_13;
    uint64_t var_143;
    uint64_t local_sp_12;
    uint64_t rbx_1;
    uint32_t *storemerge_in_in_pre_phi;
    uint64_t rcx3_3;
    uint64_t local_sp_0;
    uint64_t local_sp_11;
    uint64_t var_149;
    uint64_t state_0x9018_1;
    uint64_t var_150;
    uint64_t r13_0;
    uint64_t var_88;
    uint64_t rax_0;
    uint64_t rcx3_0;
    uint64_t var_121;
    uint64_t var_122;
    uint64_t var_123;
    uint64_t local_sp_9;
    uint64_t local_sp_2;
    uint64_t local_sp_7;
    uint64_t local_sp_1;
    uint64_t storemerge;
    uint32_t *var_118;
    uint64_t rcx3_1;
    uint64_t var_119;
    uint64_t var_125;
    uint64_t var_126;
    uint64_t var_127;
    uint64_t var_128;
    uint64_t var_129;
    uint64_t var_130;
    uint64_t rbx_0;
    uint64_t local_sp_10;
    uint64_t var_70;
    uint64_t local_sp_3;
    uint64_t r12_0;
    uint64_t local_sp_4;
    uint64_t var_71;
    uint32_t var_72;
    uint32_t var_155;
    uint64_t var_152;
    uint64_t local_sp_5;
    uint32_t var_79;
    uint64_t var_80;
    uint64_t var_81;
    uint64_t var_82;
    uint64_t local_sp_6;
    uint64_t *var_83;
    uint64_t var_84;
    uint64_t var_85;
    uint64_t var_86;
    uint64_t var_87;
    uint64_t var_73;
    uint64_t var_74;
    uint64_t var_75;
    uint64_t var_76;
    uint64_t var_77;
    uint64_t var_78;
    uint32_t var_95;
    uint64_t var_96;
    uint32_t var_97;
    uint64_t var_98;
    uint32_t *var_99;
    bool var_100;
    unsigned char var_101;
    uint64_t var_102;
    uint64_t var_103;
    uint32_t var_104;
    uint32_t *var_105;
    uint64_t *var_106;
    uint64_t var_107;
    uint64_t var_108;
    bool var_109;
    uint32_t *var_110;
    uint64_t local_sp_8;
    uint64_t var_111;
    uint64_t var_115;
    uint64_t var_116;
    uint64_t var_117;
    uint64_t var_112;
    uint64_t var_113;
    uint64_t var_114;
    uint64_t var_131;
    uint64_t var_89;
    uint64_t var_90;
    uint64_t var_91;
    uint64_t var_92;
    uint64_t var_93;
    uint64_t var_94;
    uint64_t var_132;
    uint32_t var_133;
    uint64_t var_134;
    uint64_t var_135;
    uint32_t *var_136;
    uint64_t var_137;
    uint64_t *var_138;
    uint64_t var_139;
    uint64_t var_144;
    uint64_t var_145;
    uint64_t var_146;
    uint64_t var_147;
    uint64_t var_148;
    uint64_t var_140;
    uint64_t var_141;
    uint64_t var_142;
    uint64_t var_34;
    uint64_t *var_35;
    uint32_t var_36;
    uint64_t var_37;
    uint64_t var_38;
    uint32_t *var_39;
    uint64_t *var_40;
    uint64_t rdi2_2;
    uint64_t rbx_2_ph;
    uint64_t r12_1;
    uint64_t rdi2_0_ph;
    uint64_t r15_1;
    uint64_t r12_0_ph;
    uint64_t rcx3_5;
    uint64_t r15_0_ph;
    uint64_t cc_src2_2;
    uint64_t rcx3_4_ph;
    uint32_t state_0x8248_1;
    uint64_t cc_src2_0_ph;
    uint32_t state_0x8248_0_ph;
    uint32_t state_0x9010_1;
    uint64_t state_0x9018_0_ph;
    uint64_t rax_3;
    uint32_t state_0x9010_0_ph;
    uint32_t rax_1_ph_in;
    uint64_t state_0x82d8_1;
    uint64_t local_sp_14_ph;
    uint32_t state_0x9080_1;
    uint64_t state_0x82d8_0_ph;
    uint32_t state_0x9080_0_ph;
    uint64_t rbx_2;
    uint64_t rcx3_4;
    uint32_t rax_1_in;
    uint64_t rax_1;
    uint32_t var_151;
    uint64_t var_153;
    uint32_t var_154;
    uint64_t rdi2_1;
    uint64_t cc_src2_1;
    uint64_t local_sp_15;
    uint64_t var_156;
    uint32_t var_157;
    uint64_t storemerge4;
    uint32_t var_159;
    uint64_t var_158;
    uint64_t rax_2;
    bool var_41;
    uint64_t var_42;
    bool var_43;
    bool var_44;
    uint64_t var_45;
    uint64_t var_46;
    uint64_t var_62;
    uint64_t var_63;
    uint64_t var_64;
    uint64_t var_65;
    uint64_t var_66;
    uint64_t var_67;
    uint64_t var_68;
    uint64_t var_69;
    uint64_t var_47;
    bool var_48;
    uint64_t var_49;
    uint64_t rdx1_0;
    uint64_t var_50;
    uint64_t var_51;
    uint32_t var_53;
    uint64_t var_54;
    uint32_t var_55;
    struct helper_divq_EAX_wrapper_ret_type var_52;
    uint64_t var_56;
    uint32_t var_57;
    uint64_t var_58;
    uint64_t var_59;
    uint64_t var_60;
    uint64_t var_61;
    uint64_t local_sp_16;
    uint64_t var_160;
    uint32_t var_161;
    unsigned int loop_state_var;
    bool switch_state_var;
    var_0 = revng_init_local_sp(0UL);
    var_1 = init_rax();
    var_2 = init_rbx();
    var_3 = init_r14();
    var_4 = init_rbp();
    var_5 = init_r13();
    var_6 = init_r12();
    var_7 = init_r15();
    var_8 = init_cc_src2();
    var_9 = init_r10();
    var_10 = init_state_0x8248();
    var_11 = init_state_0x9018();
    var_12 = init_state_0x9010();
    var_13 = init_state_0x8408();
    var_14 = init_state_0x8328();
    var_15 = init_state_0x82d8();
    var_16 = init_state_0x9080();
    var_17 = var_0 + (-8L);
    *(uint64_t *)var_17 = var_4;
    *(uint64_t *)(var_0 + (-16L)) = var_7;
    *(uint64_t *)(var_0 + (-24L)) = var_3;
    *(uint64_t *)(var_0 + (-32L)) = var_5;
    *(uint64_t *)(var_0 + (-40L)) = var_6;
    *(uint64_t *)(var_0 + (-48L)) = var_2;
    var_18 = (uint32_t *)(var_0 + (-128L));
    var_19 = (uint32_t)rdi;
    *var_18 = var_19;
    var_20 = (uint64_t *)(var_0 + (-120L));
    *var_20 = rdx;
    var_21 = (uint64_t *)(var_0 + (-104L));
    *var_21 = rcx;
    var_22 = var_0 + (-132L);
    *(uint32_t *)var_22 = (uint32_t)r8;
    var_23 = var_0 + (-112L);
    var_24 = (uint32_t *)var_23;
    *var_24 = (uint32_t)r9;
    var_25 = var_0 + (-124L);
    var_26 = (unsigned char *)var_25;
    *var_26 = (unsigned char)r8;
    var_27 = var_0 + (-64L);
    var_28 = (uint64_t *)var_27;
    *var_28 = 0UL;
    var_29 = var_0 + (-160L);
    *(uint64_t *)var_29 = 4260822UL;
    indirect_placeholder();
    var_30 = var_0 + (-88L);
    var_31 = (uint64_t *)var_30;
    *var_31 = var_1;
    var_32 = rsi + 4UL;
    var_33 = *(uint32_t *)var_32;
    rcx3_0 = 0UL;
    rcx3_1 = 0UL;
    rbx_2_ph = var_32;
    r12_0_ph = 0UL;
    r15_0_ph = var_32;
    rcx3_4_ph = rcx;
    cc_src2_0_ph = var_8;
    state_0x8248_0_ph = var_10;
    state_0x9018_0_ph = var_11;
    state_0x9010_0_ph = var_12;
    rax_1_ph_in = var_33;
    local_sp_14_ph = var_29;
    state_0x82d8_0_ph = var_15;
    state_0x9080_0_ph = var_16;
    if (var_33 == 0U) {
        return;
    }
    var_34 = var_0 + (-80L);
    var_35 = (uint64_t *)var_34;
    *var_35 = var_27;
    var_36 = var_19 + (-63);
    var_37 = (uint64_t)var_36;
    var_38 = var_0 + (-96L);
    var_39 = (uint32_t *)var_38;
    *var_39 = var_36;
    var_40 = (uint64_t *)(var_0 + (-144L));
    rdi2_0_ph = var_37;
    while (1U)
        {
            state_0x9018_1 = state_0x9018_0_ph;
            r12_0 = r12_0_ph;
            rdi2_2 = rdi2_0_ph;
            r15_1 = r15_0_ph;
            cc_src2_2 = cc_src2_0_ph;
            state_0x8248_1 = state_0x8248_0_ph;
            state_0x9010_1 = state_0x9010_0_ph;
            state_0x82d8_1 = state_0x82d8_0_ph;
            state_0x9080_1 = state_0x9080_0_ph;
            rbx_2 = rbx_2_ph;
            rcx3_4 = rcx3_4_ph;
            rax_1_in = rax_1_ph_in;
            rdi2_1 = rdi2_0_ph;
            cc_src2_1 = cc_src2_0_ph;
            local_sp_15 = local_sp_14_ph;
            local_sp_16 = local_sp_14_ph;
            while (1U)
                {
                    rax_1 = (uint64_t)rax_1_in;
                    rax_1_in = 40U;
                    r12_1 = r12_0;
                    rcx3_5 = rcx3_4;
                    rax_3 = rbx_2;
                    if ((uint64_t)(rax_1_in + (-91)) != 0UL) {
                        loop_state_var = 1U;
                        break;
                    }
                    var_41 = ((uint64_t)((rax_1_in + (-63)) & (-2)) == 0UL);
                    var_42 = (rcx3_4 & (-256L)) | var_41;
                    var_43 = ((uint64_t)((rax_1_in + (-42)) & (-2)) == 0UL);
                    var_44 = var_41 || var_43;
                    var_45 = var_42 | var_43;
                    rcx3_4 = var_45;
                    rcx3_5 = var_45;
                    if (!var_44) {
                        if ((uint64_t)(rax_1_in + (-33)) != 0UL) {
                            loop_state_var = 0U;
                            break;
                        }
                    }
                    var_46 = rbx_2 + 4UL;
                    rbx_2 = var_46;
                    if (*(uint32_t *)var_46 != 40U) {
                        loop_state_var = 0U;
                        break;
                    }
                    r12_0 = r12_0 + 1UL;
                    continue;
                }
            switch_state_var = 0;
            switch (loop_state_var) {
              case 0U:
                {
                    if ((uint64_t)(rax_1_in + (-41)) == 0UL) {
                        r12_1 = r12_0 + (-1L);
                        if (r12_0 != 0UL) {
                            if ((*var_18 + (-63)) <= 1U) {
                                loop_state_var = 1U;
                                switch_state_var = 1;
                                break;
                            }
                            *var_31 = ((uint64_t)((long)(rbx_2 - r15_0_ph) >> (long)2UL) + 1UL);
                            loop_state_var = 1U;
                            switch_state_var = 1;
                            break;
                        }
                    }
                    if (!(((uint64_t)(rax_1_in + (-124)) == 0UL) && (r12_0 == 0UL))) {
                        var_47 = *var_31;
                        var_48 = (*var_39 > 1U);
                        var_49 = rbx_2 - r15_0_ph;
                        rdx1_0 = var_48 ? ((uint64_t)((long)var_49 >> (long)2UL) + 1UL) : var_47;
                        var_50 = ((rdx1_0 << 2UL) + 15UL) & (-8L);
                        var_51 = var_50 + (-8L);
                        r12_1 = 0UL;
                        rcx3_5 = var_51;
                        if (!((var_51 > 7991UL) || (rdx1_0 > 4611686018427387903UL))) {
                            loop_state_var = 0U;
                            switch_state_var = 1;
                            break;
                        }
                        var_52 = helper_divq_EAX_wrapper((struct type_5 *)(0UL), 16UL, 4261322UL, var_50 + 30UL, 0UL, rbx_2, var_17, rdi2_0_ph, var_51, var_9, r8, r9, 16UL, state_0x8248_0_ph, state_0x9018_0_ph, state_0x9010_0_ph, var_13, var_14, state_0x82d8_0_ph, state_0x9080_0_ph);
                        var_53 = var_52.field_5;
                        var_54 = var_52.field_6;
                        var_55 = var_52.field_7;
                        var_56 = var_52.field_8;
                        var_57 = var_52.field_9;
                        var_58 = local_sp_14_ph - (var_52.field_1 << 4UL);
                        var_59 = (var_58 + 15UL) & (-16L);
                        *var_40 = var_49;
                        var_60 = var_59 | 8UL;
                        var_61 = var_58 + (-8L);
                        *(uint64_t *)var_61 = 4261373UL;
                        indirect_placeholder();
                        *(uint32_t *)(*var_40 + var_49) = 0U;
                        *(uint64_t *)var_59 = 0UL;
                        **(uint64_t **)var_34 = var_59;
                        *var_35 = var_59;
                        state_0x9018_1 = var_54;
                        rdi2_2 = var_60;
                        r15_1 = rbx_2 + 4UL;
                        state_0x8248_1 = var_53;
                        state_0x9010_1 = var_55;
                        state_0x82d8_1 = var_56;
                        state_0x9080_1 = var_57;
                        local_sp_16 = var_61;
                    }
                }
                break;
              case 1U:
                {
                    var_151 = *(uint32_t *)6470648UL;
                    var_155 = var_151;
                    if (var_151 == 0U) {
                        var_152 = local_sp_14_ph + (-8L);
                        *(uint64_t *)var_152 = 4260890UL;
                        indirect_placeholder();
                        var_153 = helper_cc_compute_c_wrapper(rax_1 + (-1L), 1UL, cc_src2_0_ph, 17U);
                        var_154 = (0U - (uint32_t)var_153) | 1U;
                        *(uint32_t *)6470648UL = var_154;
                        var_155 = var_154;
                        rdi2_1 = 4337846UL;
                        cc_src2_1 = var_153;
                        local_sp_15 = var_152;
                    }
                    var_156 = rbx_2 + 4UL;
                    var_157 = *(uint32_t *)var_156;
                    rdi2_2 = rdi2_1;
                    cc_src2_2 = cc_src2_1;
                    storemerge4 = var_156;
                    var_159 = var_157;
                    local_sp_16 = local_sp_15;
                    if ((uint64_t)(var_157 + (-33)) == 0UL) {
                        var_158 = rbx_2 + 8UL;
                        var_159 = *(uint32_t *)var_158;
                        storemerge4 = var_158;
                    } else {
                        if ((uint64_t)(var_157 + (-94)) != 0UL & (int)var_155 < (int)0U) {
                            var_158 = rbx_2 + 8UL;
                            var_159 = *(uint32_t *)var_158;
                            storemerge4 = var_158;
                        }
                    }
                    rax_2 = (var_159 == 93U) ? (storemerge4 + 4UL) : storemerge4;
                    while (1U)
                        {
                            rax_3 = rax_2;
                            switch_state_var = 0;
                            switch (*(uint32_t *)rax_2) {
                              case 0U:
                                {
                                    loop_state_var = 0U;
                                    switch_state_var = 1;
                                    break;
                                }
                                break;
                              case 93U:
                                {
                                    loop_state_var = 1U;
                                    switch_state_var = 1;
                                    break;
                                }
                                break;
                              default:
                                {
                                    rax_2 = rax_2 + 4UL;
                                    continue;
                                }
                                break;
                            }
                            if (switch_state_var)
                                break;
                        }
                    switch_state_var = 0;
                    switch (loop_state_var) {
                      case 1U:
                        {
                            var_160 = rax_3 + 4UL;
                            var_161 = *(uint32_t *)var_160;
                            rbx_2_ph = var_160;
                            rdi2_0_ph = rdi2_2;
                            r12_0_ph = r12_1;
                            r15_0_ph = r15_1;
                            rcx3_4_ph = rcx3_5;
                            cc_src2_0_ph = cc_src2_2;
                            state_0x8248_0_ph = state_0x8248_1;
                            state_0x9018_0_ph = state_0x9018_1;
                            state_0x9010_0_ph = state_0x9010_1;
                            rax_1_ph_in = var_161;
                            local_sp_14_ph = local_sp_16;
                            state_0x82d8_0_ph = state_0x82d8_1;
                            state_0x9080_0_ph = state_0x9080_1;
                            if (var_161 == 0U) {
                                continue;
                            }
                            loop_state_var = 0U;
                            switch_state_var = 1;
                            break;
                        }
                        break;
                      case 0U:
                        {
                            loop_state_var = 0U;
                            switch_state_var = 1;
                            break;
                        }
                        break;
                    }
                    if (switch_state_var)
                        break;
                }
                break;
            }
            if (switch_state_var)
                break;
        }
    var_62 = *var_31;
    var_63 = ((var_62 << 2UL) + 15UL) & (-8L);
    if (!!(((var_63 + (-8L)) > 7991UL) || (var_62 > 4611686018427387903UL))) {
        return;
    }
    var_64 = (var_63 + 30UL) & (-16L);
    var_65 = local_sp_14_ph - var_64;
    var_66 = (var_65 + 15UL) & (-16L);
    var_67 = rbx_2 - r15_0_ph;
    var_68 = var_65 + (-8L);
    *(uint64_t *)var_68 = 4261173UL;
    indirect_placeholder();
    *(uint32_t *)(var_67 + var_64) = 0U;
    *(uint64_t *)var_66 = 0UL;
    **(uint64_t **)var_34 = var_66;
    var_69 = *var_28;
    *var_35 = var_69;
    local_sp_3 = var_68;
    if (var_69 == 0UL) {
        var_70 = var_65 + (-16L);
        *(uint64_t *)var_70 = 4261470UL;
        indirect_placeholder();
        local_sp_3 = var_70;
    }
    local_sp_4 = local_sp_3;
    if (*(uint32_t *)(rbx_2 + (-4L)) == 41U) {
        var_71 = local_sp_3 + (-8L);
        *(uint64_t *)var_71 = 4261501UL;
        indirect_placeholder();
        local_sp_4 = var_71;
    }
    var_72 = *var_18 + (-33);
    local_sp_5 = local_sp_4;
    local_sp_7 = local_sp_4;
    local_sp_11 = local_sp_4;
    if (var_72 <= 31U) {
        *(uint64_t *)(local_sp_4 + (-8L)) = 4262164UL;
        indirect_placeholder();
        return;
    }
    switch (*(uint64_t *)(((uint64_t)var_72 << 3UL) + 4343920UL)) {
      case 4262139UL:
        {
            *(uint64_t *)(local_sp_4 + (-8L)) = 4262164UL;
            indirect_placeholder();
        }
        break;
      case 4261525UL:
        {
            var_132 = *var_20;
            r13_0 = var_132;
            if (var_132 <= *var_21) {
                var_133 = *var_24;
                var_134 = (uint64_t)var_133;
                var_135 = ((var_134 & 1UL) == 0UL) ? (uint64_t)(var_133 & (-5)) : var_134;
                var_136 = (uint32_t *)var_30;
                *var_136 = (uint32_t)var_135;
                var_137 = (uint64_t)*var_26;
                *var_39 = (var_133 & 5U);
                var_138 = (uint64_t *)var_23;
                *var_138 = rbx_2;
                rcx3_3 = var_137;
                while (1U)
                    {
                        var_139 = *var_35;
                        local_sp_12 = local_sp_11;
                        rbx_1 = var_139;
                        local_sp_13 = local_sp_11;
                        if (var_139 == 0UL) {
                            rcx3_3 = 0UL;
                            if (r13_0 != var_132 & *(uint32_t *)(r13_0 + (-4L)) == 47U) {
                                rcx3_3 = (*var_39 == 5U);
                            }
                            var_144 = (uint64_t)*var_136;
                            var_145 = *var_21;
                            var_146 = *var_138;
                            var_147 = local_sp_12 + (-8L);
                            *(uint64_t *)var_147 = 4262107UL;
                            var_148 = indirect_placeholder_8(var_145, var_146, rcx3_3, var_144, r13_0);
                            local_sp_0 = var_147;
                            if ((uint64_t)(uint32_t)var_148 == 0UL) {
                                break;
                            }
                            var_149 = r13_0 + 4UL;
                            var_150 = helper_cc_compute_c_wrapper(*var_21 - var_149, var_149, cc_src2_0_ph, 17U);
                            r13_0 = var_149;
                            local_sp_11 = local_sp_0;
                            if (var_150 != 0UL) {
                                continue;
                            }
                            break;
                        }
                        while (1U)
                            {
                                var_140 = rbx_1 + 8UL;
                                var_141 = local_sp_13 + (-8L);
                                *(uint64_t *)var_141 = 4262033UL;
                                var_142 = indirect_placeholder_8(r13_0, var_140, var_137, var_135, var_132);
                                local_sp_0 = var_141;
                                local_sp_12 = var_141;
                                local_sp_13 = var_141;
                                if ((uint64_t)(uint32_t)var_142 != 0UL) {
                                    loop_state_var = 1U;
                                    break;
                                }
                                var_143 = *(uint64_t *)rbx_1;
                                rbx_1 = var_143;
                                if (var_143 == 0UL) {
                                    continue;
                                }
                                loop_state_var = 0U;
                                break;
                            }
                        switch_state_var = 0;
                        switch (loop_state_var) {
                          case 0U:
                            {
                                break;
                            }
                            break;
                          case 1U:
                            {
                                var_149 = r13_0 + 4UL;
                                var_150 = helper_cc_compute_c_wrapper(*var_21 - var_149, var_149, cc_src2_0_ph, 17U);
                                r13_0 = var_149;
                                local_sp_11 = local_sp_0;
                                if (var_150 != 0UL) {
                                    continue;
                                }
                                switch_state_var = 1;
                                break;
                            }
                            break;
                        }
                        if (switch_state_var)
                            break;
                    }
            }
        }
        break;
      case 4261596UL:
        {
            var_89 = (uint64_t)*(unsigned char *)var_22;
            var_90 = (uint64_t)*var_24;
            var_91 = *var_21;
            var_92 = *var_20;
            var_93 = local_sp_4 + (-8L);
            *(uint64_t *)var_93 = 4261620UL;
            var_94 = indirect_placeholder_8(var_91, rbx_2, var_89, var_90, var_92);
            local_sp_7 = var_93;
            if ((uint64_t)(uint32_t)var_94 == 0UL) {
                return;
            }
            var_95 = *var_24;
            var_96 = (uint64_t)var_95;
            var_97 = var_95 & (-5);
            var_98 = (uint64_t)var_97;
            var_99 = (uint32_t *)var_30;
            *var_99 = var_97;
            var_100 = ((var_96 & 1UL) == 0UL);
            *var_18 = (uint32_t)(var_100 ? var_98 : var_96);
            var_101 = *var_26;
            var_102 = (uint64_t)var_101;
            var_103 = rsi + (-4L);
            var_104 = var_95 & 5U;
            var_105 = (uint32_t *)var_25;
            *var_105 = var_104;
            var_106 = (uint64_t *)var_38;
            *var_106 = rbx_2;
            var_107 = *var_21;
            *var_21 = var_103;
            var_108 = *var_20;
            var_109 = (var_108 > var_107);
            var_110 = (uint32_t *)var_34;
            storemerge_in_in_pre_phi = var_99;
            local_sp_8 = local_sp_7;
            rbx_0 = var_108;
            while (1U)
                {
                    local_sp_9 = local_sp_8;
                    local_sp_10 = local_sp_8;
                    if (var_109) {
                        var_131 = **(uint64_t **)var_27;
                        *var_28 = var_131;
                        local_sp_8 = local_sp_10;
                        if (var_131 != 0UL) {
                            continue;
                        }
                        break;
                    }
                    while (1U)
                        {
                            *var_110 = (uint32_t)var_101;
                            var_111 = *var_28 + 8UL;
                            if (var_100) {
                                var_115 = (uint64_t)*var_99;
                                var_116 = local_sp_9 + (-8L);
                                *(uint64_t *)var_116 = 4261738UL;
                                var_117 = indirect_placeholder_8(rbx_0, var_111, var_102, var_115, var_108);
                                local_sp_1 = var_116;
                                local_sp_2 = var_116;
                                if ((uint64_t)(uint32_t)var_117 != 0UL) {
                                    var_129 = rbx_0 + 4UL;
                                    var_130 = helper_cc_compute_c_wrapper(var_107 - var_129, var_129, cc_src2_0_ph, 17U);
                                    rbx_0 = var_129;
                                    local_sp_9 = local_sp_2;
                                    local_sp_10 = local_sp_2;
                                    if (var_130 == 0UL) {
                                        continue;
                                    }
                                    loop_state_var = 0U;
                                    break;
                                }
                            }
                            var_112 = (uint64_t)*var_24;
                            var_113 = local_sp_9 + (-8L);
                            *(uint64_t *)var_113 = 4262256UL;
                            var_114 = indirect_placeholder_8(rbx_0, var_111, var_102, var_112, var_108);
                            storemerge_in_in_pre_phi = var_24;
                            local_sp_1 = var_113;
                            local_sp_2 = var_113;
                            if ((uint64_t)(uint32_t)var_114 != 0UL) {
                                var_129 = rbx_0 + 4UL;
                                var_130 = helper_cc_compute_c_wrapper(var_107 - var_129, var_129, cc_src2_0_ph, 17U);
                                rbx_0 = var_129;
                                local_sp_9 = local_sp_2;
                                local_sp_10 = local_sp_2;
                                if (var_130 == 0UL) {
                                    continue;
                                }
                                loop_state_var = 0U;
                                break;
                            }
                            storemerge = (uint64_t)*storemerge_in_in_pre_phi;
                            if (rbx_0 == var_108) {
                                var_125 = (uint64_t)*var_110;
                                var_126 = *var_106;
                                var_127 = local_sp_1 + (-8L);
                                *(uint64_t *)var_127 = 4262287UL;
                                var_128 = indirect_placeholder_8(var_107, var_126, var_125, storemerge, var_108);
                                local_sp_2 = var_127;
                                if ((uint64_t)(uint32_t)var_128 != 0UL) {
                                    loop_state_var = 1U;
                                    break;
                                }
                            }
                            var_118 = (uint32_t *)(rbx_0 + (-4L));
                            if (*var_118 == 47U) {
                                rcx3_1 = (*var_105 == 5U);
                            }
                            var_119 = *var_106;
                            *(uint64_t *)(local_sp_1 + (-8L)) = 4261800UL;
                            var_120 = indirect_placeholder_8(var_107, var_119, rcx3_1, storemerge, rbx_0);
                            if ((uint64_t)(uint32_t)var_120 != 0UL) {
                                loop_state_var = 1U;
                                break;
                            }
                            if (*var_118 == 47U) {
                                rcx3_0 = (*var_105 == 5U);
                            }
                            var_121 = (uint64_t)*var_18;
                            var_122 = *var_21;
                            var_123 = local_sp_1 + (-16L);
                            *(uint64_t *)var_123 = 4261845UL;
                            var_124 = indirect_placeholder_8(var_107, var_122, rcx3_0, var_121, rbx_0);
                            local_sp_2 = var_123;
                            if ((uint64_t)(uint32_t)var_124 != 0UL) {
                                loop_state_var = 1U;
                                break;
                            }
                            var_129 = rbx_0 + 4UL;
                            var_130 = helper_cc_compute_c_wrapper(var_107 - var_129, var_129, cc_src2_0_ph, 17U);
                            rbx_0 = var_129;
                            local_sp_9 = local_sp_2;
                            local_sp_10 = local_sp_2;
                            if (var_130 == 0UL) {
                                continue;
                            }
                            loop_state_var = 0U;
                            break;
                        }
                    switch_state_var = 0;
                    switch (loop_state_var) {
                      case 1U:
                        {
                            switch_state_var = 1;
                            break;
                        }
                        break;
                      case 0U:
                        {
                            break;
                        }
                        break;
                    }
                    if (switch_state_var)
                        break;
                }
        }
        break;
      case 4261628UL:
        {
            var_95 = *var_24;
            var_96 = (uint64_t)var_95;
            var_97 = var_95 & (-5);
            var_98 = (uint64_t)var_97;
            var_99 = (uint32_t *)var_30;
            *var_99 = var_97;
            var_100 = ((var_96 & 1UL) == 0UL);
            *var_18 = (uint32_t)(var_100 ? var_98 : var_96);
            var_101 = *var_26;
            var_102 = (uint64_t)var_101;
            var_103 = rsi + (-4L);
            var_104 = var_95 & 5U;
            var_105 = (uint32_t *)var_25;
            *var_105 = var_104;
            var_106 = (uint64_t *)var_38;
            *var_106 = rbx_2;
            var_107 = *var_21;
            *var_21 = var_103;
            var_108 = *var_20;
            var_109 = (var_108 > var_107);
            var_110 = (uint32_t *)var_34;
            storemerge_in_in_pre_phi = var_99;
            local_sp_8 = local_sp_7;
            rbx_0 = var_108;
            while (1U)
                {
                    local_sp_9 = local_sp_8;
                    local_sp_10 = local_sp_8;
                    if (var_109) {
                        var_131 = **(uint64_t **)var_27;
                        *var_28 = var_131;
                        local_sp_8 = local_sp_10;
                        if (var_131 != 0UL) {
                            continue;
                        }
                        break;
                    }
                    while (1U)
                        {
                            *var_110 = (uint32_t)var_101;
                            var_111 = *var_28 + 8UL;
                            if (var_100) {
                                var_115 = (uint64_t)*var_99;
                                var_116 = local_sp_9 + (-8L);
                                *(uint64_t *)var_116 = 4261738UL;
                                var_117 = indirect_placeholder_8(rbx_0, var_111, var_102, var_115, var_108);
                                local_sp_1 = var_116;
                                local_sp_2 = var_116;
                                if ((uint64_t)(uint32_t)var_117 != 0UL) {
                                    var_129 = rbx_0 + 4UL;
                                    var_130 = helper_cc_compute_c_wrapper(var_107 - var_129, var_129, cc_src2_0_ph, 17U);
                                    rbx_0 = var_129;
                                    local_sp_9 = local_sp_2;
                                    local_sp_10 = local_sp_2;
                                    if (var_130 == 0UL) {
                                        continue;
                                    }
                                    loop_state_var = 0U;
                                    break;
                                }
                            }
                            var_112 = (uint64_t)*var_24;
                            var_113 = local_sp_9 + (-8L);
                            *(uint64_t *)var_113 = 4262256UL;
                            var_114 = indirect_placeholder_8(rbx_0, var_111, var_102, var_112, var_108);
                            storemerge_in_in_pre_phi = var_24;
                            local_sp_1 = var_113;
                            local_sp_2 = var_113;
                            if ((uint64_t)(uint32_t)var_114 != 0UL) {
                                var_129 = rbx_0 + 4UL;
                                var_130 = helper_cc_compute_c_wrapper(var_107 - var_129, var_129, cc_src2_0_ph, 17U);
                                rbx_0 = var_129;
                                local_sp_9 = local_sp_2;
                                local_sp_10 = local_sp_2;
                                if (var_130 == 0UL) {
                                    continue;
                                }
                                loop_state_var = 0U;
                                break;
                            }
                            storemerge = (uint64_t)*storemerge_in_in_pre_phi;
                            if (rbx_0 == var_108) {
                                var_125 = (uint64_t)*var_110;
                                var_126 = *var_106;
                                var_127 = local_sp_1 + (-8L);
                                *(uint64_t *)var_127 = 4262287UL;
                                var_128 = indirect_placeholder_8(var_107, var_126, var_125, storemerge, var_108);
                                local_sp_2 = var_127;
                                if ((uint64_t)(uint32_t)var_128 != 0UL) {
                                    loop_state_var = 1U;
                                    break;
                                }
                            }
                            var_118 = (uint32_t *)(rbx_0 + (-4L));
                            if (*var_118 == 47U) {
                                rcx3_1 = (*var_105 == 5U);
                            }
                            var_119 = *var_106;
                            *(uint64_t *)(local_sp_1 + (-8L)) = 4261800UL;
                            var_120 = indirect_placeholder_8(var_107, var_119, rcx3_1, storemerge, rbx_0);
                            if ((uint64_t)(uint32_t)var_120 != 0UL) {
                                loop_state_var = 1U;
                                break;
                            }
                            if (*var_118 == 47U) {
                                rcx3_0 = (*var_105 == 5U);
                            }
                            var_121 = (uint64_t)*var_18;
                            var_122 = *var_21;
                            var_123 = local_sp_1 + (-16L);
                            *(uint64_t *)var_123 = 4261845UL;
                            var_124 = indirect_placeholder_8(var_107, var_122, rcx3_0, var_121, rbx_0);
                            local_sp_2 = var_123;
                            if ((uint64_t)(uint32_t)var_124 != 0UL) {
                                loop_state_var = 1U;
                                break;
                            }
                            var_129 = rbx_0 + 4UL;
                            var_130 = helper_cc_compute_c_wrapper(var_107 - var_129, var_129, cc_src2_0_ph, 17U);
                            rbx_0 = var_129;
                            local_sp_9 = local_sp_2;
                            local_sp_10 = local_sp_2;
                            if (var_130 == 0UL) {
                                continue;
                            }
                            loop_state_var = 0U;
                            break;
                        }
                    switch_state_var = 0;
                    switch (loop_state_var) {
                      case 1U:
                        {
                            switch_state_var = 1;
                            break;
                        }
                        break;
                      case 0U:
                        {
                            break;
                        }
                        break;
                    }
                    if (switch_state_var)
                        break;
                }
        }
        break;
      case 4261893UL:
        {
            var_73 = (uint64_t)*(unsigned char *)var_22;
            var_74 = (uint64_t)*var_24;
            var_75 = *var_21;
            var_76 = *var_20;
            var_77 = local_sp_4 + (-8L);
            *(uint64_t *)var_77 = 4261917UL;
            var_78 = indirect_placeholder_8(var_75, rbx_2, var_73, var_74, var_76);
            local_sp_5 = var_77;
            if ((uint64_t)(uint32_t)var_78 == 0UL) {
                return;
            }
            var_79 = *var_24;
            var_80 = (uint64_t)var_79;
            var_81 = ((var_80 & 1UL) == 0UL) ? (uint64_t)(var_79 & (-5)) : var_80;
            var_82 = (uint64_t)*var_26;
            rax_0 = var_80;
            local_sp_6 = local_sp_5;
            var_83 = *(uint64_t **)var_27;
            *(uint64_t *)(local_sp_6 + (-8L)) = 4261962UL;
            indirect_placeholder();
            var_84 = *var_21;
            var_85 = *var_20;
            var_86 = local_sp_6 + (-16L);
            *(uint64_t *)var_86 = 4261984UL;
            var_87 = indirect_placeholder_8(var_84, rax_0, var_82, var_81, var_85);
            local_sp_6 = var_86;
            while ((uint64_t)(uint32_t)var_87 != 0UL)
                {
                    var_88 = *var_83;
                    *var_28 = var_88;
                    rax_0 = var_88;
                    if (var_88 == 0UL) {
                        break;
                    }
                    var_83 = *(uint64_t **)var_27;
                    *(uint64_t *)(local_sp_6 + (-8L)) = 4261962UL;
                    indirect_placeholder();
                    var_84 = *var_21;
                    var_85 = *var_20;
                    var_86 = local_sp_6 + (-16L);
                    *(uint64_t *)var_86 = 4261984UL;
                    var_87 = indirect_placeholder_8(var_84, rax_0, var_82, var_81, var_85);
                    local_sp_6 = var_86;
                }
        }
        break;
      case 4261925UL:
        {
            var_79 = *var_24;
            var_80 = (uint64_t)var_79;
            var_81 = ((var_80 & 1UL) == 0UL) ? (uint64_t)(var_79 & (-5)) : var_80;
            var_82 = (uint64_t)*var_26;
            rax_0 = var_80;
            local_sp_6 = local_sp_5;
            var_83 = *(uint64_t **)var_27;
            *(uint64_t *)(local_sp_6 + (-8L)) = 4261962UL;
            indirect_placeholder();
            var_84 = *var_21;
            var_85 = *var_20;
            var_86 = local_sp_6 + (-16L);
            *(uint64_t *)var_86 = 4261984UL;
            var_87 = indirect_placeholder_8(var_84, rax_0, var_82, var_81, var_85);
            local_sp_6 = var_86;
            while ((uint64_t)(uint32_t)var_87 != 0UL)
                {
                    var_88 = *var_83;
                    *var_28 = var_88;
                    rax_0 = var_88;
                    if (var_88 == 0UL) {
                        break;
                    }
                    var_83 = *(uint64_t **)var_27;
                    *(uint64_t *)(local_sp_6 + (-8L)) = 4261962UL;
                    indirect_placeholder();
                    var_84 = *var_21;
                    var_85 = *var_20;
                    var_86 = local_sp_6 + (-16L);
                    *(uint64_t *)var_86 = 4261984UL;
                    var_87 = indirect_placeholder_8(var_84, rax_0, var_82, var_81, var_85);
                    local_sp_6 = var_86;
                }
        }
        break;
      default:
        {
            abort();
        }
        break;
    }
}

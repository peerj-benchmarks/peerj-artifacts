typedef unsigned char __u_char;
typedef unsigned short __u_short;
typedef unsigned int __u_int;
typedef unsigned long __u_long;
typedef signed char __int8_t;
typedef unsigned char __uint8_t;
typedef short __int16_t;
typedef unsigned short __uint16_t;
typedef int __int32_t;
typedef unsigned int __uint32_t;
typedef long __int64_t;
typedef unsigned long __uint64_t;
typedef __int8_t __int_least8_t;
typedef __uint8_t __uint_least8_t;
typedef __int16_t __int_least16_t;
typedef __uint16_t __uint_least16_t;
typedef __int32_t __int_least32_t;
typedef __uint32_t __uint_least32_t;
typedef __int64_t __int_least64_t;
typedef __uint64_t __uint_least64_t;
typedef long __quad_t;
typedef unsigned long __u_quad_t;
typedef long __intmax_t;
typedef unsigned long __uintmax_t;
typedef unsigned long __dev_t;
typedef unsigned int __uid_t;
typedef unsigned int __gid_t;
typedef unsigned long __ino_t;
typedef unsigned long __ino64_t;
typedef unsigned int __mode_t;
typedef unsigned long __nlink_t;
typedef long __off_t;
typedef long __off64_t;
typedef int __pid_t;
typedef struct {
    int __val[2];
} __fsid_t;
typedef long __clock_t;
typedef unsigned long __rlim_t;
typedef unsigned long __rlim64_t;
typedef unsigned int __id_t;
typedef long __time_t;
typedef unsigned int __useconds_t;
typedef long __suseconds_t;
typedef int __daddr_t;
typedef int __key_t;
typedef int __clockid_t;
typedef void *__timer_t;
typedef long __blksize_t;
typedef long __blkcnt_t;
typedef long __blkcnt64_t;
typedef unsigned long __fsblkcnt_t;
typedef unsigned long __fsblkcnt64_t;
typedef unsigned long __fsfilcnt_t;
typedef unsigned long __fsfilcnt64_t;
typedef long __fsword_t;
typedef long __ssize_t;
typedef long __syscall_slong_t;
typedef unsigned long __syscall_ulong_t;
typedef __off64_t __loff_t;
typedef char *__caddr_t;
typedef long __intptr_t;
typedef unsigned int __socklen_t;
typedef int __sig_atomic_t;
typedef __int8_t int8_t;
typedef __int16_t int16_t;
typedef __int32_t int32_t;
typedef __int64_t int64_t;
typedef __uint8_t uint8_t;
typedef __uint16_t uint16_t;
typedef __uint32_t uint32_t;
typedef __uint64_t uint64_t;
typedef __int_least8_t int_least8_t;
typedef __int_least16_t int_least16_t;
typedef __int_least32_t int_least32_t;
typedef __int_least64_t int_least64_t;
typedef __uint_least8_t uint_least8_t;
typedef __uint_least16_t uint_least16_t;
typedef __uint_least32_t uint_least32_t;
typedef __uint_least64_t uint_least64_t;
typedef signed char int_fast8_t;
typedef long int_fast16_t;
typedef long int_fast32_t;
typedef long int_fast64_t;
typedef unsigned char uint_fast8_t;
typedef unsigned long uint_fast16_t;
typedef unsigned long uint_fast32_t;
typedef unsigned long uint_fast64_t;
typedef long intptr_t;
typedef unsigned long uintptr_t;
typedef __intmax_t intmax_t;
typedef __uintmax_t uintmax_t;
extern void abort(void);
extern uint64_t revng_init_local_sp(uint64_t param_0);
extern uint64_t init_rbx(void);
extern void indirect_placeholder(void);
extern uint64_t init_rbp(void);
extern uint64_t init_cc_src2(void);
extern uint64_t helper_cc_compute_c_wrapper(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint32_t param_3);
extern uint64_t init_r14(void);
extern uint64_t init_r13(void);
extern uint64_t init_r12(void);
extern uint64_t init_r15(void);
extern uint64_t init_rax(void);
extern void indirect_placeholder_3(uint64_t param_0, uint64_t param_1);
extern uint64_t indirect_placeholder_4(uint64_t param_0, uint64_t param_1);
extern uint64_t init_r8(void);
extern uint64_t init_r9(void);
uint64_t bb_readtoken(uint64_t rdx, uint64_t rdi, uint64_t rcx, uint64_t rsi) {
    uint64_t var_22;
    uint64_t var_0;
    uint64_t var_1;
    uint64_t var_2;
    uint64_t var_3;
    uint64_t var_4;
    uint64_t var_5;
    uint64_t var_6;
    uint64_t var_7;
    uint64_t var_8;
    uint64_t var_9;
    uint64_t var_10;
    uint64_t var_11;
    uint64_t rax_2;
    uint64_t local_sp_0;
    uint64_t var_23;
    uint64_t var_24;
    uint64_t var_25;
    uint64_t rbx_0_in;
    uint64_t rax_0;
    uint64_t var_19;
    uint64_t var_20;
    uint64_t var_21;
    uint64_t r13_1;
    uint64_t rax_1;
    uint64_t local_sp_1;
    uint64_t var_26;
    uint64_t rbx_1_in;
    uint64_t r15_2;
    uint64_t r13_0;
    uint64_t local_sp_3;
    uint64_t r15_0;
    uint64_t local_sp_2;
    uint64_t storemerge;
    uint64_t r15_1;
    uint64_t var_27;
    uint64_t var_28;
    uint64_t rdi2_0;
    uint64_t var_29;
    uint64_t var_30;
    uint64_t var_31;
    uint64_t var_32;
    uint64_t var_33;
    uint64_t var_34;
    uint64_t var_12;
    uint64_t var_13;
    uint64_t var_14;
    uint64_t var_15;
    uint64_t *var_16;
    uint64_t var_17;
    uint64_t var_18;
    unsigned int loop_state_var;
    var_0 = revng_init_local_sp(0UL);
    var_1 = init_rax();
    var_2 = init_rbx();
    var_3 = init_r14();
    var_4 = init_rbp();
    var_5 = init_r13();
    var_6 = init_r12();
    var_7 = init_r15();
    var_8 = init_cc_src2();
    var_9 = init_r9();
    var_10 = init_r8();
    *(uint64_t *)(var_0 + (-8L)) = var_7;
    *(uint64_t *)(var_0 + (-16L)) = var_3;
    *(uint64_t *)(var_0 + (-24L)) = var_5;
    *(uint64_t *)(var_0 + (-32L)) = var_6;
    *(uint64_t *)(var_0 + (-40L)) = var_4;
    *(uint64_t *)(var_0 + (-48L)) = var_2;
    *(uint64_t *)(var_0 + (-96L)) = rcx;
    var_11 = var_0 + (-112L);
    *(uint64_t *)var_11 = 4218895UL;
    indirect_placeholder();
    rax_2 = var_1;
    rax_1 = 18446744073709551615UL;
    storemerge = 0UL;
    r15_1 = 128UL;
    rdi2_0 = rsi;
    if (rdx != 0UL) {
        var_12 = rdx + rsi;
        var_13 = (uint64_t)*(unsigned char *)rdi2_0;
        var_14 = (var_13 >> 3UL) & 24UL;
        var_15 = 1UL << (var_13 & 63UL);
        var_16 = (uint64_t *)((var_14 + var_11) + 16UL);
        *var_16 = (*var_16 | var_15);
        var_17 = rdi2_0 + 1UL;
        rdi2_0 = var_17;
        rax_2 = var_14;
        do {
            var_13 = (uint64_t)*(unsigned char *)rdi2_0;
            var_14 = (var_13 >> 3UL) & 24UL;
            var_15 = 1UL << (var_13 & 63UL);
            var_16 = (uint64_t *)((var_14 + var_11) + 16UL);
            *var_16 = (*var_16 | var_15);
            var_17 = rdi2_0 + 1UL;
            rdi2_0 = var_17;
            rax_2 = var_14;
        } while (var_17 != var_12);
    }
    var_18 = var_0 + (-120L);
    *(uint64_t *)var_18 = 4218957UL;
    indirect_placeholder();
    local_sp_0 = var_18;
    rbx_0_in = rax_2;
    local_sp_1 = var_18;
    if ((int)(uint32_t)rax_2 < (int)0U) {
        return rax_1;
    }
    var_19 = (uint64_t)((long)(rax_2 << 32UL) >> (long)32UL) >> 6UL;
    var_20 = *(uint64_t *)(((var_19 << 3UL) + var_18) + 16UL);
    var_21 = helper_cc_compute_c_wrapper(var_19, var_20 >> (rax_2 & 63UL), var_8, 41U);
    rax_0 = var_20;
    if (var_21 != 0UL) {
        while (1U)
            {
                var_22 = local_sp_0 + (-8L);
                *(uint64_t *)var_22 = 4218996UL;
                indirect_placeholder();
                local_sp_0 = var_22;
                rbx_0_in = rax_0;
                local_sp_1 = var_22;
                if ((int)(uint32_t)rax_0 >= (int)0U) {
                    loop_state_var = 0U;
                    break;
                }
                var_23 = (uint64_t)((long)(rax_0 << 32UL) >> (long)32UL) >> 6UL;
                var_24 = *(uint64_t *)(((var_23 << 3UL) + var_22) + 16UL);
                var_25 = helper_cc_compute_c_wrapper(var_23, var_24 >> (rax_0 & 63UL), var_8, 41U);
                rax_0 = var_24;
                if (var_25 == 0UL) {
                    continue;
                }
                loop_state_var = 1U;
                break;
            }
        switch (loop_state_var) {
          case 0U:
            {
                return rax_1;
            }
            break;
          case 1U:
            {
                break;
            }
            break;
        }
    }
    var_26 = *(uint64_t *)(local_sp_1 + 8UL);
    rbx_1_in = rbx_0_in;
    r13_0 = *(uint64_t *)(var_26 + 8UL);
    r15_0 = *(uint64_t *)var_26;
    local_sp_2 = local_sp_1;
    while (1U)
        {
            r13_1 = r13_0;
            r15_2 = r15_0;
            local_sp_3 = local_sp_2;
            rax_1 = storemerge;
            if (storemerge == r15_0) {
                r13_0 = r13_1;
                r15_0 = r15_2;
                if ((int)(uint32_t)rbx_1_in <= (int)4294967295U) {
                    *(unsigned char *)(storemerge + r13_1) = (unsigned char)'\x00';
                    loop_state_var = 1U;
                    break;
                }
                var_29 = (uint64_t)((long)(rbx_1_in << 32UL) >> (long)32UL) >> 6UL;
                var_30 = *(uint64_t *)(((var_29 << 3UL) + local_sp_3) + 16UL);
                var_31 = helper_cc_compute_c_wrapper(var_29, var_30 >> (rbx_1_in & 63UL), var_8, 41U);
                rbx_1_in = var_30;
                rax_1 = 18446744073709551615UL;
                if (var_31 == 0UL) {
                    var_33 = storemerge + 1UL;
                    *(unsigned char *)(storemerge + r13_1) = (unsigned char)rbx_1_in;
                    var_34 = local_sp_3 + (-8L);
                    *(uint64_t *)var_34 = 4219176UL;
                    indirect_placeholder();
                    local_sp_2 = var_34;
                    storemerge = var_33;
                    if (((int)(uint32_t)var_30 <= (int)4294967295U) && (var_33 == 0UL)) {
                        continue;
                    }
                    loop_state_var = 0U;
                    break;
                }
                *(unsigned char *)(storemerge + r13_1) = (unsigned char)'\x00';
                loop_state_var = 1U;
                break;
            }
            if (r13_0 == 0UL) {
                r15_1 = r15_0;
                if (r15_0 != 0UL & (long)r15_0 < (long)0UL) {
                    *(uint64_t *)(local_sp_2 + (-8L)) = 4219063UL;
                    indirect_placeholder_3(var_9, var_10);
                    abort();
                }
            }
            if (r15_0 > 6148914691236517203UL) {
                *(uint64_t *)(local_sp_2 + (-8L)) = 4219083UL;
                indirect_placeholder_3(var_9, var_10);
                abort();
            }
            r15_1 = (r15_0 + (r15_0 >> 1UL)) + 1UL;
            var_27 = local_sp_2 + (-8L);
            *(uint64_t *)var_27 = 4219118UL;
            var_28 = indirect_placeholder_4(r13_0, r15_1);
            r13_1 = var_28;
            r15_2 = r15_1;
            local_sp_3 = var_27;
        }
    var_32 = *(uint64_t *)(local_sp_3 + 8UL);
    *(uint64_t *)(var_32 + 8UL) = r13_1;
    *(uint64_t *)var_32 = r15_2;
}

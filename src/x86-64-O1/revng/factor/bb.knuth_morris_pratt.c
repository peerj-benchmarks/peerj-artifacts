typedef unsigned char __u_char;
typedef unsigned short __u_short;
typedef unsigned int __u_int;
typedef unsigned long __u_long;
typedef signed char __int8_t;
typedef unsigned char __uint8_t;
typedef short __int16_t;
typedef unsigned short __uint16_t;
typedef int __int32_t;
typedef unsigned int __uint32_t;
typedef long __int64_t;
typedef unsigned long __uint64_t;
typedef __int8_t __int_least8_t;
typedef __uint8_t __uint_least8_t;
typedef __int16_t __int_least16_t;
typedef __uint16_t __uint_least16_t;
typedef __int32_t __int_least32_t;
typedef __uint32_t __uint_least32_t;
typedef __int64_t __int_least64_t;
typedef __uint64_t __uint_least64_t;
typedef long __quad_t;
typedef unsigned long __u_quad_t;
typedef long __intmax_t;
typedef unsigned long __uintmax_t;
typedef unsigned long __dev_t;
typedef unsigned int __uid_t;
typedef unsigned int __gid_t;
typedef unsigned long __ino_t;
typedef unsigned long __ino64_t;
typedef unsigned int __mode_t;
typedef unsigned long __nlink_t;
typedef long __off_t;
typedef long __off64_t;
typedef int __pid_t;
typedef struct {
    int __val[2];
} __fsid_t;
typedef long __clock_t;
typedef unsigned long __rlim_t;
typedef unsigned long __rlim64_t;
typedef unsigned int __id_t;
typedef long __time_t;
typedef unsigned int __useconds_t;
typedef long __suseconds_t;
typedef int __daddr_t;
typedef int __key_t;
typedef int __clockid_t;
typedef void *__timer_t;
typedef long __blksize_t;
typedef long __blkcnt_t;
typedef long __blkcnt64_t;
typedef unsigned long __fsblkcnt_t;
typedef unsigned long __fsblkcnt64_t;
typedef unsigned long __fsfilcnt_t;
typedef unsigned long __fsfilcnt64_t;
typedef long __fsword_t;
typedef long __ssize_t;
typedef long __syscall_slong_t;
typedef unsigned long __syscall_ulong_t;
typedef __off64_t __loff_t;
typedef char *__caddr_t;
typedef long __intptr_t;
typedef unsigned int __socklen_t;
typedef int __sig_atomic_t;
typedef __int8_t int8_t;
typedef __int16_t int16_t;
typedef __int32_t int32_t;
typedef __int64_t int64_t;
typedef __uint8_t uint8_t;
typedef __uint16_t uint16_t;
typedef __uint32_t uint32_t;
typedef __uint64_t uint64_t;
typedef __int_least8_t int_least8_t;
typedef __int_least16_t int_least16_t;
typedef __int_least32_t int_least32_t;
typedef __int_least64_t int_least64_t;
typedef __uint_least8_t uint_least8_t;
typedef __uint_least16_t uint_least16_t;
typedef __uint_least32_t uint_least32_t;
typedef __uint_least64_t uint_least64_t;
typedef signed char int_fast8_t;
typedef long int_fast16_t;
typedef long int_fast32_t;
typedef long int_fast64_t;
typedef unsigned char uint_fast8_t;
typedef unsigned long uint_fast16_t;
typedef unsigned long uint_fast32_t;
typedef unsigned long uint_fast64_t;
typedef long intptr_t;
typedef unsigned long uintptr_t;
typedef __intmax_t intmax_t;
typedef __uintmax_t uintmax_t;
typedef _Bool bool;
extern uint64_t revng_init_local_sp(uint64_t param_0);
extern uint64_t init_rbx(void);
extern uint64_t init_rbp(void);
extern uint64_t init_r14(void);
extern uint64_t init_r13(void);
extern uint64_t init_r12(void);
extern void indirect_placeholder_9(uint64_t param_0);
extern uint64_t indirect_placeholder_4(uint64_t param_0, uint64_t param_1);
uint64_t bb_knuth_morris_pratt(uint64_t rdx, uint64_t rdi, uint64_t rcx, uint64_t rsi) {
    uint64_t var_0;
    uint64_t var_1;
    uint64_t var_2;
    uint64_t var_3;
    uint64_t var_4;
    uint64_t var_5;
    uint64_t var_6;
    uint64_t rax_2;
    uint64_t var_7;
    uint64_t var_8;
    uint64_t local_sp_0;
    uint64_t storemerge;
    uint64_t var_13;
    uint64_t r8_3;
    uint64_t rax_0;
    uint64_t r12_0_ph_be;
    uint64_t r8_0;
    unsigned char var_11;
    uint64_t r8_2;
    uint64_t r8_1;
    uint64_t var_12;
    uint64_t var_14;
    uint64_t *var_15;
    uint64_t rax_1_ph;
    uint64_t rcx3_0_ph_be;
    uint64_t r12_0_ph;
    uint64_t rcx3_0_ph;
    unsigned char *var_16;
    bool var_17;
    uint64_t r12_0;
    uint64_t rcx3_0;
    unsigned char var_18;
    uint64_t var_22;
    uint64_t rax_1_ph_be;
    uint64_t var_23;
    uint64_t var_19;
    uint64_t var_20;
    uint64_t var_21;
    uint64_t var_9;
    uint64_t var_10;
    unsigned int loop_state_var;
    bool switch_state_var;
    var_0 = revng_init_local_sp(0UL);
    var_1 = init_rbx();
    var_2 = init_r14();
    var_3 = init_rbp();
    var_4 = init_r13();
    var_5 = init_r12();
    *(uint64_t *)(var_0 + (-8L)) = var_3;
    *(uint64_t *)(var_0 + (-16L)) = var_2;
    *(uint64_t *)(var_0 + (-24L)) = var_4;
    *(uint64_t *)(var_0 + (-32L)) = var_5;
    var_6 = var_0 + (-40L);
    *(uint64_t *)var_6 = var_1;
    rax_2 = 0UL;
    r8_3 = 0UL;
    rax_0 = 2UL;
    r8_0 = 0UL;
    rax_1_ph = 0UL;
    r12_0_ph = rdi;
    rcx3_0_ph = rdi;
    if (rdx > 1152921504606846975UL) {
        return rax_2;
    }
    var_7 = rdx << 3UL;
    if (var_7 > 4015UL) {
        var_9 = var_0 + (-48L);
        *(uint64_t *)var_9 = 4226608UL;
        var_10 = indirect_placeholder_4(0UL, var_7);
        local_sp_0 = var_9;
        storemerge = var_10;
    } else {
        var_8 = var_6 - ((var_7 + 46UL) & (-16L));
        local_sp_0 = var_8;
        storemerge = (var_8 + 31UL) & (-16L);
    }
    if (storemerge == 0UL) {
        return rax_2;
    }
    *(uint64_t *)(storemerge + 8UL) = 1UL;
    rax_2 = 1UL;
    if (rdx > 2UL) {
        while (1U)
            {
                var_11 = *(unsigned char *)((rax_0 + rsi) + (-1L));
                r8_1 = r8_0;
                r8_2 = r8_0;
                if ((uint64_t)(*(unsigned char *)(r8_0 + rsi) - var_11) == 0UL) {
                    var_13 = r8_2 + 1UL;
                    *(uint64_t *)((rax_0 << 3UL) + storemerge) = (rax_0 - var_13);
                    r8_3 = var_13;
                    var_14 = rax_0 + 1UL;
                    rax_0 = var_14;
                    r8_0 = r8_3;
                    if (var_14 != rdx) {
                        continue;
                    }
                    break;
                }
                if (r8_0 != 0UL) {
                    *(uint64_t *)((rax_0 << 3UL) + storemerge) = rax_0;
                    var_14 = rax_0 + 1UL;
                    rax_0 = var_14;
                    r8_0 = r8_3;
                    if (var_14 != rdx) {
                        continue;
                    }
                    break;
                }
                while (1U)
                    {
                        var_12 = r8_1 - *(uint64_t *)((r8_1 << 3UL) + storemerge);
                        r8_1 = var_12;
                        r8_2 = var_12;
                        if ((uint64_t)(*(unsigned char *)(var_12 + rsi) - var_11) != 0UL) {
                            loop_state_var = 0U;
                            break;
                        }
                        if (var_12 == 0UL) {
                            continue;
                        }
                        loop_state_var = 1U;
                        break;
                    }
                switch_state_var = 0;
                switch (loop_state_var) {
                  case 0U:
                    {
                        break;
                    }
                    break;
                  case 1U:
                    {
                        *(uint64_t *)((rax_0 << 3UL) + storemerge) = rax_0;
                        var_14 = rax_0 + 1UL;
                        rax_0 = var_14;
                        r8_0 = r8_3;
                        if (var_14 != rdx) {
                            continue;
                        }
                        switch_state_var = 1;
                        break;
                    }
                    break;
                }
                if (switch_state_var)
                    break;
            }
    }
    var_15 = (uint64_t *)rcx;
    *var_15 = 0UL;
    while (1U)
        {
            var_16 = (unsigned char *)(rax_1_ph + rsi);
            var_17 = (rax_1_ph == 0UL);
            r12_0 = r12_0_ph;
            rcx3_0 = rcx3_0_ph;
            while (1U)
                {
                    var_18 = *(unsigned char *)rcx3_0;
                    r12_0_ph_be = r12_0;
                    rcx3_0_ph_be = rcx3_0;
                    if (var_18 != '\x00') {
                        loop_state_var = 2U;
                        break;
                    }
                    if ((uint64_t)(var_18 - *var_16) != 0UL) {
                        loop_state_var = 0U;
                        break;
                    }
                    if (!var_17) {
                        loop_state_var = 1U;
                        break;
                    }
                    r12_0 = r12_0 + 1UL;
                    rcx3_0 = rcx3_0 + 1UL;
                    continue;
                }
            switch_state_var = 0;
            switch (loop_state_var) {
              case 2U:
                {
                    switch_state_var = 1;
                    break;
                }
                break;
              case 0U:
                {
                    var_22 = rax_1_ph + 1UL;
                    rax_1_ph_be = var_22;
                    if (var_22 != rdx) {
                        *var_15 = r12_0;
                        switch_state_var = 1;
                        break;
                    }
                    var_23 = rcx3_0 + 1UL;
                    rcx3_0_ph_be = var_23;
                    rax_1_ph = rax_1_ph_be;
                    r12_0_ph = r12_0_ph_be;
                    rcx3_0_ph = rcx3_0_ph_be;
                    continue;
                }
                break;
              case 1U:
                {
                    var_19 = *(uint64_t *)((rax_1_ph << 3UL) + storemerge);
                    var_20 = r12_0 + var_19;
                    var_21 = rax_1_ph - var_19;
                    rax_1_ph_be = var_21;
                    r12_0_ph_be = var_20;
                    rax_1_ph = rax_1_ph_be;
                    r12_0_ph = r12_0_ph_be;
                    rcx3_0_ph = rcx3_0_ph_be;
                    continue;
                }
                break;
            }
            if (switch_state_var)
                break;
        }
    *(uint64_t *)(local_sp_0 + (-8L)) = 4226794UL;
    indirect_placeholder_9(storemerge);
    return rax_2;
}

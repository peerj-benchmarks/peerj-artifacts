typedef unsigned char __u_char;
typedef unsigned short __u_short;
typedef unsigned int __u_int;
typedef unsigned long __u_long;
typedef signed char __int8_t;
typedef unsigned char __uint8_t;
typedef short __int16_t;
typedef unsigned short __uint16_t;
typedef int __int32_t;
typedef unsigned int __uint32_t;
typedef long __int64_t;
typedef unsigned long __uint64_t;
typedef __int8_t __int_least8_t;
typedef __uint8_t __uint_least8_t;
typedef __int16_t __int_least16_t;
typedef __uint16_t __uint_least16_t;
typedef __int32_t __int_least32_t;
typedef __uint32_t __uint_least32_t;
typedef __int64_t __int_least64_t;
typedef __uint64_t __uint_least64_t;
typedef long __quad_t;
typedef unsigned long __u_quad_t;
typedef long __intmax_t;
typedef unsigned long __uintmax_t;
typedef unsigned long __dev_t;
typedef unsigned int __uid_t;
typedef unsigned int __gid_t;
typedef unsigned long __ino_t;
typedef unsigned long __ino64_t;
typedef unsigned int __mode_t;
typedef unsigned long __nlink_t;
typedef long __off_t;
typedef long __off64_t;
typedef int __pid_t;
typedef struct {
    int __val[2];
} __fsid_t;
typedef long __clock_t;
typedef unsigned long __rlim_t;
typedef unsigned long __rlim64_t;
typedef unsigned int __id_t;
typedef long __time_t;
typedef unsigned int __useconds_t;
typedef long __suseconds_t;
typedef int __daddr_t;
typedef int __key_t;
typedef int __clockid_t;
typedef void *__timer_t;
typedef long __blksize_t;
typedef long __blkcnt_t;
typedef long __blkcnt64_t;
typedef unsigned long __fsblkcnt_t;
typedef unsigned long __fsblkcnt64_t;
typedef unsigned long __fsfilcnt_t;
typedef unsigned long __fsfilcnt64_t;
typedef long __fsword_t;
typedef long __ssize_t;
typedef long __syscall_slong_t;
typedef unsigned long __syscall_ulong_t;
typedef __off64_t __loff_t;
typedef char *__caddr_t;
typedef long __intptr_t;
typedef unsigned int __socklen_t;
typedef int __sig_atomic_t;
typedef __int8_t int8_t;
typedef __int16_t int16_t;
typedef __int32_t int32_t;
typedef __int64_t int64_t;
typedef __uint8_t uint8_t;
typedef __uint16_t uint16_t;
typedef __uint32_t uint32_t;
typedef __uint64_t uint64_t;
typedef __int_least8_t int_least8_t;
typedef __int_least16_t int_least16_t;
typedef __int_least32_t int_least32_t;
typedef __int_least64_t int_least64_t;
typedef __uint_least8_t uint_least8_t;
typedef __uint_least16_t uint_least16_t;
typedef __uint_least32_t uint_least32_t;
typedef __uint_least64_t uint_least64_t;
typedef signed char int_fast8_t;
typedef long int_fast16_t;
typedef long int_fast32_t;
typedef long int_fast64_t;
typedef unsigned char uint_fast8_t;
typedef unsigned long uint_fast16_t;
typedef unsigned long uint_fast32_t;
typedef unsigned long uint_fast64_t;
typedef long intptr_t;
typedef unsigned long uintptr_t;
typedef __intmax_t intmax_t;
typedef __uintmax_t uintmax_t;
typedef _Bool bool;
struct indirect_placeholder_48_ret_type;
struct indirect_placeholder_48_ret_type {
    uint64_t field_0;
    uint64_t field_1;
    uint64_t field_2;
    uint64_t field_3;
};
extern uint64_t revng_init_local_sp(uint64_t param_0);
extern uint64_t init_rbx(void);
extern void indirect_placeholder(void);
extern uint64_t init_rbp(void);
extern uint64_t init_cc_src2(void);
extern uint64_t helper_cc_compute_c_wrapper(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint32_t param_3);
extern uint64_t init_r14(void);
extern uint64_t init_r13(void);
extern uint64_t init_r12(void);
extern struct indirect_placeholder_48_ret_type indirect_placeholder_48(uint64_t param_0, uint64_t param_1);
uint64_t bb_gcd2_odd(uint64_t rdx, uint64_t rdi, uint64_t rcx, uint64_t rsi, uint64_t r8) {
    uint64_t var_0;
    uint64_t var_1;
    uint64_t var_2;
    uint64_t var_3;
    uint64_t var_4;
    uint64_t var_5;
    uint64_t var_6;
    uint64_t var_7;
    uint64_t local_sp_0;
    uint64_t var_8;
    uint64_t rbx_0;
    uint64_t r13_1;
    uint64_t rbx_1_ph;
    uint64_t rbp_0;
    uint64_t var_9;
    uint64_t var_10;
    uint64_t rbp_1_ph;
    uint64_t rbx_2;
    uint64_t rbx_1_ph47;
    uint64_t rbp_1_ph48;
    uint64_t r12_0;
    uint64_t r13_0_ph;
    uint64_t cc_src2_0;
    uint64_t r12_0_ph;
    uint64_t cc_src2_0_ph;
    uint64_t r13_0;
    struct indirect_placeholder_48_ret_type var_20;
    uint64_t rax_0;
    bool _not;
    bool _not62;
    uint64_t var_11;
    uint64_t var_12;
    uint64_t var_13;
    uint64_t r12_1;
    uint64_t var_14;
    uint64_t var_15;
    uint64_t var_16;
    uint64_t var_17;
    uint64_t rbp_2;
    uint64_t var_18;
    uint64_t var_19;
    unsigned int loop_state_var;
    bool switch_state_var;
    var_0 = revng_init_local_sp(0UL);
    var_1 = init_rbx();
    var_2 = init_r14();
    var_3 = init_rbp();
    var_4 = init_r13();
    var_5 = init_r12();
    var_6 = init_cc_src2();
    *(uint64_t *)(var_0 + (-8L)) = var_2;
    *(uint64_t *)(var_0 + (-16L)) = var_4;
    *(uint64_t *)(var_0 + (-24L)) = var_5;
    *(uint64_t *)(var_0 + (-32L)) = var_3;
    var_7 = var_0 + (-40L);
    *(uint64_t *)var_7 = var_1;
    local_sp_0 = var_7;
    rbx_0 = rdx;
    rbx_1_ph = rdx;
    rbp_0 = rsi;
    rbp_1_ph = rsi;
    r13_0_ph = rcx;
    r12_0_ph = r8;
    cc_src2_0_ph = var_6;
    rax_0 = r8;
    if ((r8 & 1UL) == 0UL) {
        var_8 = var_0 + (-48L);
        *(uint64_t *)var_8 = 4203341UL;
        indirect_placeholder();
        local_sp_0 = var_8;
    }
    if ((rdx | rsi) != 0UL) {
        if ((rdx & 1UL) == 0UL) {
            var_9 = (rbx_0 >> 1UL) | (rbp_0 << 63UL);
            var_10 = rbp_0 >> 1UL;
            rbx_0 = var_9;
            rbp_0 = var_10;
            rbx_1_ph = var_9;
            rbp_1_ph = var_10;
            do {
                var_9 = (rbx_0 >> 1UL) | (rbp_0 << 63UL);
                var_10 = rbp_0 >> 1UL;
                rbx_0 = var_9;
                rbp_0 = var_10;
                rbx_1_ph = var_9;
                rbp_1_ph = var_10;
            } while ((rbx_0 & 2UL) != 0UL);
        }
        rbx_1_ph47 = rbx_1_ph;
        rbp_1_ph48 = rbp_1_ph;
        while (1U)
            {
                r13_0 = r13_0_ph;
                r12_0 = r12_0_ph;
                cc_src2_0 = cc_src2_0_ph;
                rax_0 = rbx_1_ph47;
                while (1U)
                    {
                        r13_0_ph = r13_0;
                        r12_0_ph = r12_0;
                        if ((r13_0 | rbp_1_ph48) != 0UL) {
                            *(uint64_t *)rdi = 0UL;
                            *(uint64_t *)(local_sp_0 + (-8L)) = 4203414UL;
                            var_20 = indirect_placeholder_48(r12_0, rbx_1_ph47);
                            rax_0 = var_20.field_0;
                            loop_state_var = 1U;
                            break;
                        }
                        if (rbp_1_ph48 <= r13_0) {
                            loop_state_var = 0U;
                            break;
                        }
                        _not = ((r12_0 < rbx_1_ph47) ^ 1);
                        _not62 = ((rbp_1_ph48 == r13_0) ^ 1);
                        if (!(_not || _not62)) {
                            loop_state_var = 0U;
                            break;
                        }
                        var_11 = helper_cc_compute_c_wrapper(rbp_1_ph48 - r13_0, r13_0, cc_src2_0, 17U);
                        if (var_11 != 0UL) {
                            if (!(((r12_0 > rbx_1_ph47) ^ 1) || _not62)) {
                                *(uint64_t *)rdi = rbp_1_ph48;
                                loop_state_var = 1U;
                                break;
                            }
                        }
                        var_12 = r12_0 - rbx_1_ph47;
                        var_13 = helper_cc_compute_c_wrapper(var_12, rbx_1_ph47, cc_src2_0, 17U);
                        cc_src2_0 = var_13;
                        r13_1 = (r13_0 - rbp_1_ph48) - var_13;
                        r12_1 = var_12;
                        var_14 = (r12_1 >> 1UL) | (r13_1 << 63UL);
                        var_15 = r13_1 >> 1UL;
                        r13_0 = var_15;
                        r12_0 = var_14;
                        r13_1 = var_15;
                        r12_1 = var_14;
                        do {
                            var_14 = (r12_1 >> 1UL) | (r13_1 << 63UL);
                            var_15 = r13_1 >> 1UL;
                            r13_0 = var_15;
                            r12_0 = var_14;
                            r13_1 = var_15;
                            r12_1 = var_14;
                        } while ((r12_1 & 2UL) != 0UL);
                        continue;
                    }
                switch_state_var = 0;
                switch (loop_state_var) {
                  case 1U:
                    {
                        switch_state_var = 1;
                        break;
                    }
                    break;
                  case 0U:
                    {
                        var_16 = rbx_1_ph47 - r12_0;
                        var_17 = helper_cc_compute_c_wrapper(var_16, r12_0, cc_src2_0, 17U);
                        cc_src2_0_ph = var_17;
                        rbx_2 = var_16;
                        rbp_2 = (rbp_1_ph48 - r13_0) - var_17;
                        var_18 = (rbx_2 >> 1UL) | (rbp_2 << 63UL);
                        var_19 = rbp_2 >> 1UL;
                        rbx_1_ph47 = var_18;
                        rbp_1_ph48 = var_19;
                        rbx_2 = var_18;
                        rbp_2 = var_19;
                        do {
                            var_18 = (rbx_2 >> 1UL) | (rbp_2 << 63UL);
                            var_19 = rbp_2 >> 1UL;
                            rbx_1_ph47 = var_18;
                            rbp_1_ph48 = var_19;
                            rbx_2 = var_18;
                            rbp_2 = var_19;
                        } while ((rbx_2 & 2UL) != 0UL);
                        continue;
                    }
                    break;
                }
                if (switch_state_var)
                    break;
            }
    }
    *(uint64_t *)rdi = rcx;
    return rax_0;
}

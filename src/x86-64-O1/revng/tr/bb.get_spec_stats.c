typedef unsigned char __u_char;
typedef unsigned short __u_short;
typedef unsigned int __u_int;
typedef unsigned long __u_long;
typedef signed char __int8_t;
typedef unsigned char __uint8_t;
typedef short __int16_t;
typedef unsigned short __uint16_t;
typedef int __int32_t;
typedef unsigned int __uint32_t;
typedef long __int64_t;
typedef unsigned long __uint64_t;
typedef __int8_t __int_least8_t;
typedef __uint8_t __uint_least8_t;
typedef __int16_t __int_least16_t;
typedef __uint16_t __uint_least16_t;
typedef __int32_t __int_least32_t;
typedef __uint32_t __uint_least32_t;
typedef __int64_t __int_least64_t;
typedef __uint64_t __uint_least64_t;
typedef long __quad_t;
typedef unsigned long __u_quad_t;
typedef long __intmax_t;
typedef unsigned long __uintmax_t;
typedef unsigned long __dev_t;
typedef unsigned int __uid_t;
typedef unsigned int __gid_t;
typedef unsigned long __ino_t;
typedef unsigned long __ino64_t;
typedef unsigned int __mode_t;
typedef unsigned long __nlink_t;
typedef long __off_t;
typedef long __off64_t;
typedef int __pid_t;
typedef struct {
    int __val[2];
} __fsid_t;
typedef long __clock_t;
typedef unsigned long __rlim_t;
typedef unsigned long __rlim64_t;
typedef unsigned int __id_t;
typedef long __time_t;
typedef unsigned int __useconds_t;
typedef long __suseconds_t;
typedef int __daddr_t;
typedef int __key_t;
typedef int __clockid_t;
typedef void *__timer_t;
typedef long __blksize_t;
typedef long __blkcnt_t;
typedef long __blkcnt64_t;
typedef unsigned long __fsblkcnt_t;
typedef unsigned long __fsblkcnt64_t;
typedef unsigned long __fsfilcnt_t;
typedef unsigned long __fsfilcnt64_t;
typedef long __fsword_t;
typedef long __ssize_t;
typedef long __syscall_slong_t;
typedef unsigned long __syscall_ulong_t;
typedef __off64_t __loff_t;
typedef char *__caddr_t;
typedef long __intptr_t;
typedef unsigned int __socklen_t;
typedef int __sig_atomic_t;
typedef __int8_t int8_t;
typedef __int16_t int16_t;
typedef __int32_t int32_t;
typedef __int64_t int64_t;
typedef __uint8_t uint8_t;
typedef __uint16_t uint16_t;
typedef __uint32_t uint32_t;
typedef __uint64_t uint64_t;
typedef __int_least8_t int_least8_t;
typedef __int_least16_t int_least16_t;
typedef __int_least32_t int_least32_t;
typedef __int_least64_t int_least64_t;
typedef __uint_least8_t uint_least8_t;
typedef __uint_least16_t uint_least16_t;
typedef __uint_least32_t uint_least32_t;
typedef __uint_least64_t uint_least64_t;
typedef signed char int_fast8_t;
typedef long int_fast16_t;
typedef long int_fast32_t;
typedef long int_fast64_t;
typedef unsigned char uint_fast8_t;
typedef unsigned long uint_fast16_t;
typedef unsigned long uint_fast32_t;
typedef unsigned long uint_fast64_t;
typedef long intptr_t;
typedef unsigned long uintptr_t;
typedef __intmax_t intmax_t;
typedef __uintmax_t uintmax_t;
struct bb_get_spec_stats_ret_type;
struct indirect_placeholder_45_ret_type;
struct bb_get_spec_stats_ret_type {
    uint64_t field_0;
    uint64_t field_1;
    uint64_t field_2;
};
struct indirect_placeholder_45_ret_type {
    uint64_t field_0;
    uint64_t field_1;
    uint64_t field_2;
    uint64_t field_3;
};
extern void abort(void);
extern uint64_t revng_init_local_sp(uint64_t param_0);
extern uint64_t init_rbx(void);
extern void indirect_placeholder(void);
extern uint64_t init_rbp(void);
extern uint64_t init_cc_src2(void);
extern uint64_t helper_cc_compute_c_wrapper(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint32_t param_3);
extern uint64_t init_r14(void);
extern uint64_t init_r13(void);
extern uint64_t init_r12(void);
extern uint64_t indirect_placeholder_1(uint64_t param_0);
extern uint64_t init_r15(void);
extern struct indirect_placeholder_45_ret_type indirect_placeholder_45(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint64_t param_3, uint64_t param_4, uint64_t param_5, uint64_t param_6);
struct bb_get_spec_stats_ret_type bb_get_spec_stats(uint64_t rdi, uint64_t rcx, uint64_t r9, uint64_t r8) {
    struct indirect_placeholder_45_ret_type var_35;
    uint64_t local_sp_2;
    uint64_t var_0;
    uint64_t var_1;
    uint64_t var_2;
    uint64_t var_3;
    uint64_t var_4;
    uint64_t var_5;
    uint64_t var_6;
    uint64_t var_7;
    uint64_t *var_8;
    unsigned char *var_9;
    unsigned char *var_10;
    unsigned char *var_11;
    uint64_t var_12;
    uint64_t rbx_3;
    uint64_t local_sp_4;
    uint64_t *var_13;
    uint64_t rcx4_3;
    uint64_t local_sp_5;
    uint64_t local_sp_0;
    uint64_t rbp_0;
    uint64_t r95_1;
    uint64_t rcx4_0;
    uint64_t cc_src2_0;
    uint64_t r86_1;
    uint64_t r95_0;
    uint64_t r86_0;
    uint64_t storemerge;
    uint32_t var_14;
    uint64_t var_15;
    unsigned char var_16;
    uint64_t var_17;
    uint64_t rbx_1;
    uint64_t rax_0;
    uint64_t rbx_0;
    uint64_t var_18;
    uint32_t var_19;
    uint32_t var_20;
    uint64_t var_21;
    uint64_t r14_0;
    uint64_t cc_src2_1;
    uint64_t var_22;
    uint64_t var_23;
    uint64_t var_24;
    uint64_t var_25;
    uint32_t var_26;
    unsigned char *var_27;
    uint64_t var_28;
    unsigned char *var_29;
    uint64_t var_30;
    uint64_t local_sp_3;
    uint64_t var_31;
    uint64_t rcx4_1;
    uint64_t rbx_2;
    uint64_t rcx4_2;
    uint64_t cc_src2_2;
    uint64_t var_33;
    uint64_t var_36;
    uint64_t var_34;
    uint64_t var_32;
    uint64_t rcx4_4;
    uint64_t r95_2;
    uint64_t r86_2;
    struct bb_get_spec_stats_ret_type mrv;
    struct bb_get_spec_stats_ret_type mrv1;
    struct bb_get_spec_stats_ret_type mrv2;
    var_0 = revng_init_local_sp(0UL);
    var_1 = init_rbx();
    var_2 = init_r14();
    var_3 = init_rbp();
    var_4 = init_r13();
    var_5 = init_r12();
    var_6 = init_r15();
    var_7 = init_cc_src2();
    *(uint64_t *)(var_0 + (-8L)) = var_6;
    *(uint64_t *)(var_0 + (-16L)) = var_2;
    *(uint64_t *)(var_0 + (-24L)) = var_4;
    *(uint64_t *)(var_0 + (-32L)) = var_5;
    *(uint64_t *)(var_0 + (-40L)) = var_3;
    *(uint64_t *)(var_0 + (-48L)) = var_1;
    var_8 = (uint64_t *)(rdi + 32UL);
    *var_8 = 0UL;
    var_9 = (unsigned char *)(rdi + 48UL);
    *var_9 = (unsigned char)'\x00';
    var_10 = (unsigned char *)(rdi + 50UL);
    *var_10 = (unsigned char)'\x00';
    var_11 = (unsigned char *)(rdi + 49UL);
    *var_11 = (unsigned char)'\x00';
    var_12 = *(uint64_t *)(*(uint64_t *)rdi + 8UL);
    rbx_3 = 0UL;
    rbp_0 = var_12;
    rcx4_0 = rcx;
    cc_src2_0 = var_7;
    r95_0 = r9;
    r86_0 = r8;
    storemerge = 0UL;
    rbx_1 = 0UL;
    rax_0 = 0UL;
    rbx_0 = 0UL;
    r14_0 = 0UL;
    rbx_2 = 1UL;
    rcx4_4 = rcx;
    r95_2 = r9;
    r86_2 = r8;
    if (var_12 == 0UL) {
        *(uint64_t *)(rdi + 24UL) = rbx_3;
        mrv.field_0 = rcx4_4;
        mrv1 = mrv;
        mrv1.field_1 = r95_2;
        mrv2 = mrv1;
        mrv2.field_2 = r86_2;
        return mrv2;
    }
    var_13 = (uint64_t *)(rdi + 40UL);
    local_sp_0 = var_0 + (-56L);
    while (1U)
        {
            var_14 = *(uint32_t *)rbp_0;
            local_sp_2 = local_sp_0;
            local_sp_4 = local_sp_0;
            r95_1 = r95_0;
            r86_1 = r86_0;
            cc_src2_1 = cc_src2_0;
            local_sp_3 = local_sp_0;
            rcx4_1 = rcx4_0;
            rcx4_2 = rcx4_0;
            cc_src2_2 = cc_src2_0;
            if (var_14 > 4U) {
                var_32 = local_sp_0 + (-8L);
                *(uint64_t *)var_32 = 4205984UL;
                indirect_placeholder();
                local_sp_4 = var_32;
            } else {
                switch (*(uint64_t *)(((uint64_t)var_14 << 3UL) + 4259936UL)) {
                  case 4205984UL:
                    {
                        break;
                    }
                    break;
                  case 4205792UL:
                    {
                        var_27 = (unsigned char *)(rbp_0 + 16UL);
                        var_28 = (uint64_t)*var_27;
                        var_29 = (unsigned char *)(rbp_0 + 17UL);
                        var_30 = helper_cc_compute_c_wrapper((uint64_t)*var_29 - var_28, var_28, cc_src2_0, 14U);
                        if (var_30 != 0UL) {
                            var_31 = local_sp_0 + (-8L);
                            *(uint64_t *)var_31 = 4205826UL;
                            indirect_placeholder();
                            local_sp_3 = var_31;
                            rcx4_1 = 4260102UL;
                        }
                        local_sp_4 = local_sp_3;
                        rbx_2 = ((uint64_t)*var_29 - (uint64_t)*var_27) + 1UL;
                        rcx4_2 = rcx4_1;
                    }
                    break;
                  case 4205847UL:
                    {
                        *var_11 = (unsigned char)'\x01';
                        var_20 = *(uint32_t *)(rbp_0 + 16UL);
                        var_21 = (uint64_t)var_20;
                        var_22 = local_sp_2 + (-8L);
                        *(uint64_t *)var_22 = 4205879UL;
                        var_23 = indirect_placeholder_1(var_21);
                        var_24 = helper_cc_compute_c_wrapper(var_23 + (-1L), 1UL, cc_src2_1, 14U);
                        var_25 = (rbx_1 + 1UL) - var_24;
                        var_26 = (uint32_t)r14_0;
                        local_sp_2 = var_22;
                        local_sp_4 = var_22;
                        rbx_1 = var_25;
                        cc_src2_1 = var_24;
                        rbx_2 = var_25;
                        cc_src2_2 = var_24;
                        while ((uint64_t)(var_26 + (-255)) != 0UL)
                            {
                                r14_0 = (uint64_t)(var_26 + 1U);
                                var_22 = local_sp_2 + (-8L);
                                *(uint64_t *)var_22 = 4205879UL;
                                var_23 = indirect_placeholder_1(var_21);
                                var_24 = helper_cc_compute_c_wrapper(var_23 + (-1L), 1UL, cc_src2_1, 14U);
                                var_25 = (rbx_1 + 1UL) - var_24;
                                var_26 = (uint32_t)r14_0;
                                local_sp_2 = var_22;
                                local_sp_4 = var_22;
                                rbx_1 = var_25;
                                cc_src2_1 = var_24;
                                rbx_2 = var_25;
                                cc_src2_2 = var_24;
                            }
                        if ((uint64_t)(var_20 + (-6)) != 0UL & (uint64_t)(var_20 + (-10)) == 0UL) {
                            *var_10 = (unsigned char)'\x01';
                        }
                    }
                    break;
                  case 4205917UL:
                    {
                        var_16 = *(unsigned char *)(rbp_0 + 16UL);
                        var_17 = (uint64_t)var_16;
                        rcx4_2 = var_17;
                        var_18 = rbx_0 + ((uint64_t)(var_16 - (unsigned char)rax_0) == 0UL);
                        var_19 = (uint32_t)rax_0;
                        rbx_0 = var_18;
                        rbx_2 = var_18;
                        while ((uint64_t)(var_19 + (-255)) != 0UL)
                            {
                                rax_0 = (uint64_t)(var_19 + 1U);
                                var_18 = rbx_0 + ((uint64_t)(var_16 - (unsigned char)rax_0) == 0UL);
                                var_19 = (uint32_t)rax_0;
                                rbx_0 = var_18;
                                rbx_2 = var_18;
                            }
                        *var_9 = (unsigned char)'\x01';
                    }
                    break;
                  case 4205959UL:
                    {
                        var_15 = *(uint64_t *)(rbp_0 + 24UL);
                        rbx_2 = var_15;
                        if (var_15 == 0UL) {
                            *var_13 = rbp_0;
                            *var_8 = (*var_8 + 1UL);
                            rbx_2 = 0UL;
                        }
                    }
                    break;
                  default:
                    {
                        abort();
                    }
                    break;
                }
            }
        }
    *(uint64_t *)(rdi + 24UL) = rbx_3;
    mrv.field_0 = rcx4_4;
    mrv1 = mrv;
    mrv1.field_1 = r95_2;
    mrv2 = mrv1;
    mrv2.field_2 = r86_2;
    return mrv2;
}

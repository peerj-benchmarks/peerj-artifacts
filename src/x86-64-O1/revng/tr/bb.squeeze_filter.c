typedef unsigned char __u_char;
typedef unsigned short __u_short;
typedef unsigned int __u_int;
typedef unsigned long __u_long;
typedef signed char __int8_t;
typedef unsigned char __uint8_t;
typedef short __int16_t;
typedef unsigned short __uint16_t;
typedef int __int32_t;
typedef unsigned int __uint32_t;
typedef long __int64_t;
typedef unsigned long __uint64_t;
typedef __int8_t __int_least8_t;
typedef __uint8_t __uint_least8_t;
typedef __int16_t __int_least16_t;
typedef __uint16_t __uint_least16_t;
typedef __int32_t __int_least32_t;
typedef __uint32_t __uint_least32_t;
typedef __int64_t __int_least64_t;
typedef __uint64_t __uint_least64_t;
typedef long __quad_t;
typedef unsigned long __u_quad_t;
typedef long __intmax_t;
typedef unsigned long __uintmax_t;
typedef unsigned long __dev_t;
typedef unsigned int __uid_t;
typedef unsigned int __gid_t;
typedef unsigned long __ino_t;
typedef unsigned long __ino64_t;
typedef unsigned int __mode_t;
typedef unsigned long __nlink_t;
typedef long __off_t;
typedef long __off64_t;
typedef int __pid_t;
typedef struct {
    int __val[2];
} __fsid_t;
typedef long __clock_t;
typedef unsigned long __rlim_t;
typedef unsigned long __rlim64_t;
typedef unsigned int __id_t;
typedef long __time_t;
typedef unsigned int __useconds_t;
typedef long __suseconds_t;
typedef int __daddr_t;
typedef int __key_t;
typedef int __clockid_t;
typedef void *__timer_t;
typedef long __blksize_t;
typedef long __blkcnt_t;
typedef long __blkcnt64_t;
typedef unsigned long __fsblkcnt_t;
typedef unsigned long __fsblkcnt64_t;
typedef unsigned long __fsfilcnt_t;
typedef unsigned long __fsfilcnt64_t;
typedef long __fsword_t;
typedef long __ssize_t;
typedef long __syscall_slong_t;
typedef unsigned long __syscall_ulong_t;
typedef __off64_t __loff_t;
typedef char *__caddr_t;
typedef long __intptr_t;
typedef unsigned int __socklen_t;
typedef int __sig_atomic_t;
typedef __int8_t int8_t;
typedef __int16_t int16_t;
typedef __int32_t int32_t;
typedef __int64_t int64_t;
typedef __uint8_t uint8_t;
typedef __uint16_t uint16_t;
typedef __uint32_t uint32_t;
typedef __uint64_t uint64_t;
typedef __int_least8_t int_least8_t;
typedef __int_least16_t int_least16_t;
typedef __int_least32_t int_least32_t;
typedef __int_least64_t int_least64_t;
typedef __uint_least8_t uint_least8_t;
typedef __uint_least16_t uint_least16_t;
typedef __uint_least32_t uint_least32_t;
typedef __uint_least64_t uint_least64_t;
typedef signed char int_fast8_t;
typedef long int_fast16_t;
typedef long int_fast32_t;
typedef long int_fast64_t;
typedef unsigned char uint_fast8_t;
typedef unsigned long uint_fast16_t;
typedef unsigned long uint_fast32_t;
typedef unsigned long uint_fast64_t;
typedef long intptr_t;
typedef unsigned long uintptr_t;
typedef __intmax_t intmax_t;
typedef __uintmax_t uintmax_t;
struct indirect_placeholder_41_ret_type;
struct indirect_placeholder_41_ret_type {
    uint64_t field_0;
    uint64_t field_1;
    uint64_t field_2;
    uint64_t field_3;
};
extern uint64_t revng_init_local_sp(uint64_t param_0);
extern uint64_t init_rbx(void);
extern void indirect_placeholder(void);
extern uint64_t init_rbp(void);
extern uint64_t init_cc_src2(void);
extern uint64_t helper_cc_compute_c_wrapper(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint32_t param_3);
extern uint64_t init_r14(void);
extern uint64_t init_r13(void);
extern uint64_t init_r12(void);
extern uint64_t init_rsi(void);
extern uint64_t init_r15(void);
extern uint64_t init_rax(void);
extern uint64_t init_r8(void);
extern uint64_t init_r9(void);
extern struct indirect_placeholder_41_ret_type indirect_placeholder_41(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint64_t param_3, uint64_t param_4, uint64_t param_5, uint64_t param_6);
typedef _Bool bool;
void bb_squeeze_filter(uint64_t rdx, uint64_t rdi) {
    struct indirect_placeholder_41_ret_type var_35;
    uint64_t rdi2_4;
    uint64_t var_0;
    uint64_t var_1;
    uint64_t var_2;
    uint64_t var_3;
    uint64_t var_4;
    uint64_t var_5;
    uint64_t var_6;
    uint64_t var_7;
    uint64_t var_8;
    uint64_t var_9;
    uint64_t var_10;
    uint64_t var_11;
    uint64_t var_12;
    unsigned char *var_13;
    uint64_t rdi2_7;
    uint64_t rbp_3;
    uint64_t rbx_0_ph;
    uint64_t rax_6;
    uint64_t rbp_0_ph;
    uint64_t local_sp_8;
    uint64_t rdi2_0_ph;
    uint64_t r9_2;
    uint64_t rax_0_ph;
    uint64_t r8_2;
    uint64_t local_sp_0_ph;
    uint64_t r9_0_ph;
    uint64_t r8_0_ph;
    uint64_t local_sp_4;
    uint64_t rax_0;
    uint64_t local_sp_6;
    uint64_t var_33;
    uint64_t var_34;
    uint64_t rbx_7;
    uint64_t rax_3;
    uint64_t rbx_8;
    uint64_t var_17;
    uint64_t rbx_1;
    uint64_t rbx_5;
    uint64_t rbx_4;
    uint64_t rbx_0;
    uint64_t rbp_0;
    uint64_t rdi2_0;
    uint64_t local_sp_0;
    uint64_t var_14;
    uint64_t var_16;
    uint64_t var_15;
    uint64_t rbx_2;
    uint64_t rdi2_1;
    uint64_t rax_1;
    uint64_t local_sp_1;
    uint64_t rbx_3;
    uint64_t r12_0;
    uint64_t rax_2;
    uint64_t var_18;
    uint64_t var_19;
    uint64_t var_20;
    uint64_t rdi2_2;
    uint64_t r12_1;
    uint64_t local_sp_2;
    uint64_t rdi2_3;
    uint64_t local_sp_3;
    uint64_t var_21;
    uint64_t var_22;
    uint64_t var_23;
    uint64_t r12_2;
    uint64_t var_30;
    uint64_t rbx_6;
    uint64_t rdi2_5;
    uint64_t r12_3;
    uint64_t local_sp_5;
    uint64_t var_24;
    uint32_t var_25;
    uint64_t var_26;
    uint64_t var_27;
    uint64_t var_28;
    uint64_t r14_0;
    uint64_t var_29;
    uint64_t r14_1;
    uint64_t rbp_1;
    uint64_t r12_4;
    uint64_t rax_4;
    uint64_t var_31;
    uint64_t var_32;
    uint64_t rbp_2;
    uint64_t r12_5;
    uint64_t rax_5;
    uint64_t local_sp_7;
    uint64_t r9_1;
    uint64_t r8_1;
    uint64_t rdi2_8;
    uint64_t rbx_0_ph_be;
    uint64_t rbp_0_ph_be;
    uint64_t rdi2_0_ph_be;
    uint64_t rax_0_ph_be;
    uint64_t local_sp_0_ph_be;
    uint64_t r9_0_ph_be;
    uint64_t r8_0_ph_be;
    uint64_t var_36;
    uint32_t var_37;
    uint64_t var_38;
    uint64_t var_39;
    uint64_t var_40;
    unsigned int loop_state_var;
    bool switch_state_var;
    var_0 = revng_init_local_sp(0UL);
    var_1 = init_rax();
    var_2 = init_rbx();
    var_3 = init_r14();
    var_4 = init_rbp();
    var_5 = init_r13();
    var_6 = init_r12();
    var_7 = init_r15();
    var_8 = init_cc_src2();
    var_9 = init_rsi();
    var_10 = init_r9();
    var_11 = init_r8();
    *(uint64_t *)(var_0 + (-8L)) = var_7;
    *(uint64_t *)(var_0 + (-16L)) = var_3;
    *(uint64_t *)(var_0 + (-24L)) = var_5;
    *(uint64_t *)(var_0 + (-32L)) = var_6;
    *(uint64_t *)(var_0 + (-40L)) = var_4;
    *(uint64_t *)(var_0 + (-48L)) = var_2;
    var_12 = var_0 + (-72L);
    *(uint64_t *)(var_0 + (-64L)) = var_9;
    var_13 = (unsigned char *)rdi;
    rbx_0_ph = 0UL;
    rbp_0_ph = 2147483647UL;
    rdi2_0_ph = 0UL;
    rax_0_ph = var_1;
    local_sp_0_ph = var_12;
    r9_0_ph = var_10;
    r8_0_ph = var_11;
    rdi2_1 = 0UL;
    rdi2_5 = 0UL;
    r12_3 = 0UL;
    rbp_1 = 2147483647UL;
    while (1U)
        {
            r9_2 = r9_0_ph;
            r8_2 = r8_0_ph;
            rax_0 = rax_0_ph;
            rbx_0 = rbx_0_ph;
            rbp_0 = rbp_0_ph;
            rdi2_0 = rdi2_0_ph;
            local_sp_0 = local_sp_0_ph;
            r9_1 = r9_0_ph;
            r8_1 = r8_0_ph;
            while (1U)
                {
                    var_14 = helper_cc_compute_c_wrapper(rdi2_0 - rbx_0, rbx_0, var_8, 17U);
                    rdi2_7 = rdi2_0;
                    rbp_3 = rbp_0;
                    rax_6 = rax_0;
                    local_sp_8 = local_sp_0;
                    rbx_8 = rbx_0;
                    rbx_1 = rax_0;
                    rbx_5 = rax_0;
                    rbp_0 = 2147483647UL;
                    rbx_2 = rbx_0;
                    local_sp_1 = local_sp_0;
                    rdi2_2 = rdi2_0;
                    r12_1 = rdi2_0;
                    local_sp_2 = local_sp_0;
                    if (var_14 == 0UL) {
                        var_16 = local_sp_0 + (-8L);
                        *(uint64_t *)var_16 = 4202220UL;
                        indirect_placeholder();
                        rdi2_7 = 0UL;
                        local_sp_8 = var_16;
                        rbx_8 = rax_0;
                        local_sp_1 = var_16;
                        local_sp_5 = var_16;
                        if (rax_0 == 0UL) {
                            return;
                        }
                        if ((uint64_t)((uint32_t)rbp_0 + (-2147483647)) != 0UL) {
                            loop_state_var = 2U;
                            break;
                        }
                        var_17 = (uint64_t)*var_13;
                        rax_1 = var_17;
                        if (*(unsigned char *)(var_17 + 6371200UL) != '\x00') {
                            loop_state_var = 1U;
                            break;
                        }
                    }
                    rbx_1 = rbx_0;
                    rdi2_1 = rdi2_0;
                    if ((uint64_t)((uint32_t)rbp_0 + (-2147483647)) != 0UL) {
                        loop_state_var = 2U;
                        break;
                    }
                    var_15 = (uint64_t)*(unsigned char *)(rdi2_0 + rdi);
                    rax_1 = var_15;
                    if (*(unsigned char *)(var_15 + 6371200UL) != '\x00') {
                        rbx_5 = rbx_2;
                        rbx_3 = rbx_2;
                        rdi2_3 = rdi2_2;
                        local_sp_3 = local_sp_2;
                        rdi2_5 = rdi2_2;
                        r12_3 = r12_1;
                        local_sp_5 = local_sp_2;
                        if (r12_1 != rbx_2) {
                            loop_state_var = 1U;
                            break;
                        }
                        var_21 = rbx_3 + (-1L);
                        var_22 = (uint64_t)*(unsigned char *)((rbx_3 + rdi) + (-1L));
                        rdi2_4 = rdi2_3;
                        local_sp_4 = local_sp_3;
                        rax_3 = var_22;
                        rbx_5 = rbx_3;
                        rbx_4 = rbx_3;
                        r12_2 = rbx_3;
                        rdi2_5 = rdi2_3;
                        r12_3 = var_21;
                        local_sp_5 = local_sp_3;
                        if (*(unsigned char *)(var_22 + 6371200UL) != '\x00') {
                            var_23 = helper_cc_compute_c_wrapper(18446744073709551615UL, rbx_3, var_8, 17U);
                            r12_2 = var_21;
                            if (var_23 != 0UL) {
                                loop_state_var = 1U;
                                break;
                            }
                        }
                        var_30 = rbx_4 - rdi2_4;
                        rax_0 = rax_3;
                        local_sp_6 = local_sp_4;
                        rbx_0 = rbx_4;
                        rdi2_0 = r12_2;
                        local_sp_0 = local_sp_4;
                        rbx_6 = rbx_4;
                        r14_1 = var_30;
                        r12_4 = r12_2;
                        rax_4 = rax_3;
                        if (var_30 == 0UL) {
                            continue;
                        }
                        loop_state_var = 0U;
                        break;
                    }
                    rdi2_4 = rdi2_1;
                    local_sp_4 = local_sp_1;
                    rbx_4 = rbx_1;
                    rbx_2 = rbx_1;
                    rbx_3 = rbx_1;
                    r12_0 = rdi2_1;
                    rax_2 = rax_1;
                    rdi2_2 = rdi2_1;
                    local_sp_2 = local_sp_1;
                    rdi2_3 = rdi2_1;
                    local_sp_3 = local_sp_1;
                    while (1U)
                        {
                            var_18 = r12_0 + 2UL;
                            var_19 = helper_cc_compute_c_wrapper(var_18 - rbx_1, rbx_1, var_8, 17U);
                            r12_0 = var_18;
                            r12_1 = var_18;
                            r12_2 = var_18;
                            rax_3 = rax_2;
                            if (var_19 != 0UL) {
                                loop_state_var = 1U;
                                break;
                            }
                            var_20 = (uint64_t)*(unsigned char *)(var_18 + rdi);
                            rax_2 = var_20;
                            if (*(unsigned char *)(var_20 + 6371200UL) == '\x00') {
                                continue;
                            }
                            loop_state_var = 0U;
                            break;
                        }
                    switch_state_var = 0;
                    switch (loop_state_var) {
                      case 0U:
                        {
                            rbx_5 = rbx_2;
                            rbx_3 = rbx_2;
                            rdi2_3 = rdi2_2;
                            local_sp_3 = local_sp_2;
                            rdi2_5 = rdi2_2;
                            r12_3 = r12_1;
                            local_sp_5 = local_sp_2;
                            if (r12_1 != rbx_2) {
                                loop_state_var = 1U;
                                switch_state_var = 1;
                                break;
                            }
                        }
                        break;
                      case 1U:
                        {
                            if (var_18 != rbx_1) {
                                var_30 = rbx_4 - rdi2_4;
                                rax_0 = rax_3;
                                local_sp_6 = local_sp_4;
                                rbx_0 = rbx_4;
                                rdi2_0 = r12_2;
                                local_sp_0 = local_sp_4;
                                rbx_6 = rbx_4;
                                r14_1 = var_30;
                                r12_4 = r12_2;
                                rax_4 = rax_3;
                                if (var_30 == 0UL) {
                                    continue;
                                }
                                loop_state_var = 0U;
                                switch_state_var = 1;
                                break;
                            }
                            var_21 = rbx_3 + (-1L);
                            var_22 = (uint64_t)*(unsigned char *)((rbx_3 + rdi) + (-1L));
                            rdi2_4 = rdi2_3;
                            local_sp_4 = local_sp_3;
                            rax_3 = var_22;
                            rbx_5 = rbx_3;
                            rbx_4 = rbx_3;
                            r12_2 = rbx_3;
                            rdi2_5 = rdi2_3;
                            r12_3 = var_21;
                            local_sp_5 = local_sp_3;
                            if (*(unsigned char *)(var_22 + 6371200UL) != '\x00') {
                                var_23 = helper_cc_compute_c_wrapper(18446744073709551615UL, rbx_3, var_8, 17U);
                                r12_2 = var_21;
                                if (var_23 != 0UL) {
                                    loop_state_var = 1U;
                                    switch_state_var = 1;
                                    break;
                                }
                            }
                            var_30 = rbx_4 - rdi2_4;
                            rax_0 = rax_3;
                            local_sp_6 = local_sp_4;
                            rbx_0 = rbx_4;
                            rdi2_0 = r12_2;
                            local_sp_0 = local_sp_4;
                            rbx_6 = rbx_4;
                            r14_1 = var_30;
                            r12_4 = r12_2;
                            rax_4 = rax_3;
                            if (var_30 == 0UL) {
                                continue;
                            }
                            loop_state_var = 0U;
                            switch_state_var = 1;
                            break;
                        }
                        break;
                    }
                    if (switch_state_var)
                        break;
                }
            switch (loop_state_var) {
              case 2U:
                {
                    var_36 = helper_cc_compute_c_wrapper(rdi2_7 - rbx_8, rbx_8, var_8, 17U);
                    rdi2_8 = rdi2_7;
                    rbx_0_ph_be = rbx_8;
                    rbp_0_ph_be = rbp_3;
                    rdi2_0_ph_be = rdi2_7;
                    rax_0_ph_be = rax_6;
                    local_sp_0_ph_be = local_sp_8;
                    r9_0_ph_be = r9_2;
                    r8_0_ph_be = r8_2;
                    var_37 = (uint32_t)(uint64_t)*(unsigned char *)(rdi2_7 + rdi);
                    var_38 = (uint64_t)var_37;
                    rbp_0_ph_be = 2147483647UL;
                    rax_0_ph_be = var_38;
                    if (var_36 != 0UL & (uint64_t)(var_37 - (uint32_t)rbp_3) == 0UL) {
                        var_39 = rdi2_8 + 1UL;
                        var_40 = helper_cc_compute_c_wrapper(var_39 - rbx_8, rbx_8, var_8, 17U);
                        rbp_0_ph_be = rbp_3;
                        rdi2_0_ph_be = var_39;
                        rdi2_8 = var_39;
                        while (var_40 != 0UL)
                            {
                                rbp_0_ph_be = 2147483647UL;
                                if ((uint64_t)((uint32_t)(uint64_t)*(unsigned char *)(var_39 + rdi) - var_37) == 0UL) {
                                    break;
                                }
                                var_39 = rdi2_8 + 1UL;
                                var_40 = helper_cc_compute_c_wrapper(var_39 - rbx_8, rbx_8, var_8, 17U);
                                rbp_0_ph_be = rbp_3;
                                rdi2_0_ph_be = var_39;
                                rdi2_8 = var_39;
                            }
                    }
                    rbx_0_ph = rbx_0_ph_be;
                    rbp_0_ph = rbp_0_ph_be;
                    rdi2_0_ph = rdi2_0_ph_be;
                    rax_0_ph = rax_0_ph_be;
                    local_sp_0_ph = local_sp_0_ph_be;
                    r9_0_ph = r9_0_ph_be;
                    r8_0_ph = r8_0_ph_be;
                    continue;
                }
                break;
              case 1U:
              case 0U:
                {
                    switch (loop_state_var) {
                      case 1U:
                        {
                            var_24 = r12_3 + rdi;
                            var_25 = (uint32_t)(uint64_t)*(unsigned char *)var_24;
                            var_26 = (uint64_t)var_25;
                            var_27 = r12_3 - rdi2_5;
                            var_28 = var_27 + 1UL;
                            local_sp_6 = local_sp_5;
                            rbx_7 = rbx_5;
                            rbx_6 = rbx_5;
                            r14_0 = var_28;
                            rbp_1 = var_26;
                            rax_4 = var_27;
                            rbp_2 = var_26;
                            rax_5 = var_27;
                            local_sp_7 = local_sp_5;
                            if (r12_3 == 0UL) {
                                r14_0 = ((uint64_t)((uint32_t)(uint64_t)*(unsigned char *)(var_24 + (-1L)) - var_25) == 0UL) ? var_27 : var_28;
                            }
                            var_29 = r12_3 + 1UL;
                            r14_1 = r14_0;
                            r12_4 = var_29;
                            r12_5 = var_29;
                            var_31 = *(uint64_t *)6370496UL;
                            var_32 = local_sp_6 + (-8L);
                            *(uint64_t *)var_32 = 4202443UL;
                            indirect_placeholder();
                            rbx_7 = rbx_6;
                            rbp_2 = rbp_1;
                            r12_5 = r12_4;
                            rax_5 = r14_1;
                            local_sp_7 = var_32;
                            if (r14_0 != 0UL & rax_4 == r14_1) {
                                *(uint64_t *)(local_sp_6 + (-16L)) = 4202453UL;
                                indirect_placeholder();
                                var_33 = (uint64_t)*(uint32_t *)rax_4;
                                var_34 = local_sp_6 + (-24L);
                                *(uint64_t *)var_34 = 4202475UL;
                                var_35 = indirect_placeholder_41(0UL, 4255168UL, 1UL, var_31, var_33, r9_0_ph, r8_0_ph);
                                rax_5 = var_35.field_0;
                                local_sp_7 = var_34;
                                r9_1 = var_35.field_2;
                                r8_1 = var_35.field_3;
                            }
                        }
                        break;
                      case 0U:
                        {
                            var_31 = *(uint64_t *)6370496UL;
                            var_32 = local_sp_6 + (-8L);
                            *(uint64_t *)var_32 = 4202443UL;
                            indirect_placeholder();
                            rbx_7 = rbx_6;
                            rbp_2 = rbp_1;
                            r12_5 = r12_4;
                            rax_5 = r14_1;
                            local_sp_7 = var_32;
                            if (rax_4 == r14_1) {
                                *(uint64_t *)(local_sp_6 + (-16L)) = 4202453UL;
                                indirect_placeholder();
                                var_33 = (uint64_t)*(uint32_t *)rax_4;
                                var_34 = local_sp_6 + (-24L);
                                *(uint64_t *)var_34 = 4202475UL;
                                var_35 = indirect_placeholder_41(0UL, 4255168UL, 1UL, var_31, var_33, r9_0_ph, r8_0_ph);
                                rax_5 = var_35.field_0;
                                local_sp_7 = var_34;
                                r9_1 = var_35.field_2;
                                r8_1 = var_35.field_3;
                            }
                        }
                        break;
                    }
                    rdi2_7 = r12_5;
                    rbp_3 = rbp_2;
                    rax_6 = rax_5;
                    local_sp_8 = local_sp_7;
                    r9_2 = r9_1;
                    r8_2 = r8_1;
                    rbx_8 = rbx_7;
                    rbx_0_ph_be = rbx_7;
                    rbp_0_ph_be = rbp_2;
                    rdi2_0_ph_be = r12_5;
                    rax_0_ph_be = rax_5;
                    local_sp_0_ph_be = local_sp_7;
                    r9_0_ph_be = r9_1;
                    r8_0_ph_be = r8_1;
                    if ((uint64_t)((uint32_t)rbp_2 + (-2147483647)) != 0UL) {
                        rbx_0_ph = rbx_0_ph_be;
                        rbp_0_ph = rbp_0_ph_be;
                        rdi2_0_ph = rdi2_0_ph_be;
                        rax_0_ph = rax_0_ph_be;
                        local_sp_0_ph = local_sp_0_ph_be;
                        r9_0_ph = r9_0_ph_be;
                        r8_0_ph = r8_0_ph_be;
                        continue;
                    }
                }
                break;
            }
        }
}

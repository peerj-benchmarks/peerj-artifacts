typedef unsigned char __u_char;
typedef unsigned short __u_short;
typedef unsigned int __u_int;
typedef unsigned long __u_long;
typedef signed char __int8_t;
typedef unsigned char __uint8_t;
typedef short __int16_t;
typedef unsigned short __uint16_t;
typedef int __int32_t;
typedef unsigned int __uint32_t;
typedef long __int64_t;
typedef unsigned long __uint64_t;
typedef __int8_t __int_least8_t;
typedef __uint8_t __uint_least8_t;
typedef __int16_t __int_least16_t;
typedef __uint16_t __uint_least16_t;
typedef __int32_t __int_least32_t;
typedef __uint32_t __uint_least32_t;
typedef __int64_t __int_least64_t;
typedef __uint64_t __uint_least64_t;
typedef long __quad_t;
typedef unsigned long __u_quad_t;
typedef long __intmax_t;
typedef unsigned long __uintmax_t;
typedef unsigned long __dev_t;
typedef unsigned int __uid_t;
typedef unsigned int __gid_t;
typedef unsigned long __ino_t;
typedef unsigned long __ino64_t;
typedef unsigned int __mode_t;
typedef unsigned long __nlink_t;
typedef long __off_t;
typedef long __off64_t;
typedef int __pid_t;
typedef struct {
    int __val[2];
} __fsid_t;
typedef long __clock_t;
typedef unsigned long __rlim_t;
typedef unsigned long __rlim64_t;
typedef unsigned int __id_t;
typedef long __time_t;
typedef unsigned int __useconds_t;
typedef long __suseconds_t;
typedef int __daddr_t;
typedef int __key_t;
typedef int __clockid_t;
typedef void *__timer_t;
typedef long __blksize_t;
typedef long __blkcnt_t;
typedef long __blkcnt64_t;
typedef unsigned long __fsblkcnt_t;
typedef unsigned long __fsblkcnt64_t;
typedef unsigned long __fsfilcnt_t;
typedef unsigned long __fsfilcnt64_t;
typedef long __fsword_t;
typedef long __ssize_t;
typedef long __syscall_slong_t;
typedef unsigned long __syscall_ulong_t;
typedef __off64_t __loff_t;
typedef char *__caddr_t;
typedef long __intptr_t;
typedef unsigned int __socklen_t;
typedef int __sig_atomic_t;
typedef __int8_t int8_t;
typedef __int16_t int16_t;
typedef __int32_t int32_t;
typedef __int64_t int64_t;
typedef __uint8_t uint8_t;
typedef __uint16_t uint16_t;
typedef __uint32_t uint32_t;
typedef __uint64_t uint64_t;
typedef __int_least8_t int_least8_t;
typedef __int_least16_t int_least16_t;
typedef __int_least32_t int_least32_t;
typedef __int_least64_t int_least64_t;
typedef __uint_least8_t uint_least8_t;
typedef __uint_least16_t uint_least16_t;
typedef __uint_least32_t uint_least32_t;
typedef __uint_least64_t uint_least64_t;
typedef signed char int_fast8_t;
typedef long int_fast16_t;
typedef long int_fast32_t;
typedef long int_fast64_t;
typedef unsigned char uint_fast8_t;
typedef unsigned long uint_fast16_t;
typedef unsigned long uint_fast32_t;
typedef unsigned long uint_fast64_t;
typedef long intptr_t;
typedef unsigned long uintptr_t;
typedef __intmax_t intmax_t;
typedef __uintmax_t uintmax_t;
struct bb_make_printable_str_ret_type;
struct indirect_placeholder_31_ret_type;
struct bb_make_printable_str_ret_type {
    uint64_t field_0;
    uint64_t field_1;
    uint64_t field_2;
};
struct indirect_placeholder_31_ret_type {
    uint64_t field_0;
    uint64_t field_1;
    uint64_t field_2;
    uint64_t field_3;
    uint64_t field_4;
};
extern void abort(void);
extern uint64_t revng_init_local_sp(uint64_t param_0);
extern uint64_t init_rbx(void);
extern void indirect_placeholder(void);
extern uint64_t init_rbp(void);
extern uint64_t init_cc_src2(void);
extern uint64_t helper_cc_compute_c_wrapper(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint32_t param_3);
extern uint64_t init_r13(void);
extern uint64_t init_r12(void);
extern void indirect_placeholder_30(uint64_t param_0, uint64_t param_1);
extern struct indirect_placeholder_31_ret_type indirect_placeholder_31(uint64_t param_0);
struct bb_make_printable_str_ret_type bb_make_printable_str(uint64_t rdi, uint64_t rsi, uint64_t r9, uint64_t r8) {
    uint64_t var_0;
    uint64_t var_1;
    uint64_t var_2;
    uint64_t var_3;
    uint64_t var_4;
    uint64_t var_5;
    uint64_t var_6;
    uint64_t var_13;
    uint64_t local_sp_1;
    uint64_t rax_1;
    uint64_t rax_0;
    uint64_t rbx_0;
    uint64_t local_sp_0;
    uint64_t var_19;
    unsigned char var_14;
    uint64_t var_15;
    uint64_t var_16;
    uint64_t var_17;
    uint64_t var_18;
    uint64_t var_20;
    struct bb_make_printable_str_ret_type mrv;
    struct bb_make_printable_str_ret_type mrv1;
    struct bb_make_printable_str_ret_type mrv2;
    uint64_t var_7;
    uint64_t var_8;
    struct indirect_placeholder_31_ret_type var_9;
    uint64_t var_10;
    uint64_t var_11;
    uint64_t var_12;
    var_0 = revng_init_local_sp(0UL);
    var_1 = init_rbx();
    var_2 = init_rbp();
    var_3 = init_r13();
    var_4 = init_r12();
    var_5 = init_cc_src2();
    *(uint64_t *)(var_0 + (-8L)) = var_3;
    *(uint64_t *)(var_0 + (-16L)) = var_4;
    *(uint64_t *)(var_0 + (-24L)) = var_2;
    *(uint64_t *)(var_0 + (-32L)) = var_1;
    var_6 = rsi + 1UL;
    rbx_0 = rdi;
    if (var_6 > 2305843009213693951UL) {
        *(uint64_t *)(var_0 + (-64L)) = 4203220UL;
        indirect_placeholder_30(r9, r8);
        abort();
    }
    var_7 = var_6 << 2UL;
    var_8 = var_0 + (-64L);
    *(uint64_t *)var_8 = 4203236UL;
    var_9 = indirect_placeholder_31(var_7);
    var_10 = var_9.field_0;
    var_11 = var_9.field_3;
    var_12 = var_9.field_4;
    rax_0 = var_10;
    local_sp_0 = var_8;
    if (rsi == 0UL) {
        mrv.field_0 = var_10;
        mrv1 = mrv;
        mrv1.field_1 = var_11;
        mrv2 = mrv1;
        mrv2.field_2 = var_12;
        return mrv2;
    }
    var_13 = rdi + rsi;
    while (1U)
        {
            var_14 = *(unsigned char *)rbx_0;
            var_15 = (uint64_t)var_14;
            rax_1 = rax_0;
            local_sp_1 = local_sp_0;
            if ((uint64_t)(var_14 + '\xf6') == 0UL) {
                var_19 = local_sp_1 + (-8L);
                *(uint64_t *)var_19 = 4203407UL;
                indirect_placeholder();
                var_20 = rbx_0 + 1UL;
                rax_0 = rax_1;
                rbx_0 = var_20;
                local_sp_0 = var_19;
                if (var_20 != var_13) {
                    continue;
                }
                break;
            }
            if (var_14 > '\n') {
                if ((uint64_t)(var_14 + '\xf4') != 0UL) {
                    var_19 = local_sp_1 + (-8L);
                    *(uint64_t *)var_19 = 4203407UL;
                    indirect_placeholder();
                    var_20 = rbx_0 + 1UL;
                    rax_0 = rax_1;
                    rbx_0 = var_20;
                    local_sp_0 = var_19;
                    if (var_20 != var_13) {
                        continue;
                    }
                    break;
                }
                var_16 = helper_cc_compute_c_wrapper(var_15 + (-12L), 12UL, var_5, 14U);
                if (var_16 != 0UL) {
                    var_19 = local_sp_1 + (-8L);
                    *(uint64_t *)var_19 = 4203407UL;
                    indirect_placeholder();
                    var_20 = rbx_0 + 1UL;
                    rax_0 = rax_1;
                    rbx_0 = var_20;
                    local_sp_0 = var_19;
                    if (var_20 != var_13) {
                        continue;
                    }
                    break;
                }
                if ((uint64_t)(var_14 + '\xf3') != 0UL) {
                    var_19 = local_sp_1 + (-8L);
                    *(uint64_t *)var_19 = 4203407UL;
                    indirect_placeholder();
                    var_20 = rbx_0 + 1UL;
                    rax_0 = rax_1;
                    rbx_0 = var_20;
                    local_sp_0 = var_19;
                    if (var_20 != var_13) {
                        continue;
                    }
                    break;
                }
                if ((uint64_t)(var_14 + '\xa4') != 0UL) {
                    var_19 = local_sp_1 + (-8L);
                    *(uint64_t *)var_19 = 4203407UL;
                    indirect_placeholder();
                    var_20 = rbx_0 + 1UL;
                    rax_0 = rax_1;
                    rbx_0 = var_20;
                    local_sp_0 = var_19;
                    if (var_20 != var_13) {
                        continue;
                    }
                    break;
                }
            }
            if (!(((uint64_t)(var_14 + '\xf8') == 0UL) || (var_14 > '\b'))) {
                var_19 = local_sp_1 + (-8L);
                *(uint64_t *)var_19 = 4203407UL;
                indirect_placeholder();
                var_20 = rbx_0 + 1UL;
                rax_0 = rax_1;
                rbx_0 = var_20;
                local_sp_0 = var_19;
                if (var_20 != var_13) {
                    continue;
                }
                break;
            }
            if ((uint64_t)(var_14 + '\xf9') != 0UL) {
                var_19 = local_sp_1 + (-8L);
                *(uint64_t *)var_19 = 4203407UL;
                indirect_placeholder();
                var_20 = rbx_0 + 1UL;
                rax_0 = rax_1;
                rbx_0 = var_20;
                local_sp_0 = var_19;
                if (var_20 != var_13) {
                    continue;
                }
                break;
            }
            var_17 = (uint64_t)((uint32_t)var_15 + (-32));
            rax_1 = var_17;
            if (var_17 > 94UL) {
                var_18 = local_sp_0 + (-8L);
                *(uint64_t *)var_18 = 4203396UL;
                indirect_placeholder();
                rax_1 = 0UL;
                local_sp_1 = var_18;
            } else {
                *(unsigned char *)local_sp_0 = var_14;
                *(unsigned char *)(local_sp_0 + 1UL) = (unsigned char)'\x00';
            }
        }
    mrv.field_0 = var_10;
    mrv1 = mrv;
    mrv1.field_1 = var_11;
    mrv2 = mrv1;
    mrv2.field_2 = var_12;
    return mrv2;
}

typedef unsigned char __u_char;
typedef unsigned short __u_short;
typedef unsigned int __u_int;
typedef unsigned long __u_long;
typedef signed char __int8_t;
typedef unsigned char __uint8_t;
typedef short __int16_t;
typedef unsigned short __uint16_t;
typedef int __int32_t;
typedef unsigned int __uint32_t;
typedef long __int64_t;
typedef unsigned long __uint64_t;
typedef __int8_t __int_least8_t;
typedef __uint8_t __uint_least8_t;
typedef __int16_t __int_least16_t;
typedef __uint16_t __uint_least16_t;
typedef __int32_t __int_least32_t;
typedef __uint32_t __uint_least32_t;
typedef __int64_t __int_least64_t;
typedef __uint64_t __uint_least64_t;
typedef long __quad_t;
typedef unsigned long __u_quad_t;
typedef long __intmax_t;
typedef unsigned long __uintmax_t;
typedef unsigned long __dev_t;
typedef unsigned int __uid_t;
typedef unsigned int __gid_t;
typedef unsigned long __ino_t;
typedef unsigned long __ino64_t;
typedef unsigned int __mode_t;
typedef unsigned long __nlink_t;
typedef long __off_t;
typedef long __off64_t;
typedef int __pid_t;
typedef struct {
    int __val[2];
} __fsid_t;
typedef long __clock_t;
typedef unsigned long __rlim_t;
typedef unsigned long __rlim64_t;
typedef unsigned int __id_t;
typedef long __time_t;
typedef unsigned int __useconds_t;
typedef long __suseconds_t;
typedef int __daddr_t;
typedef int __key_t;
typedef int __clockid_t;
typedef void *__timer_t;
typedef long __blksize_t;
typedef long __blkcnt_t;
typedef long __blkcnt64_t;
typedef unsigned long __fsblkcnt_t;
typedef unsigned long __fsblkcnt64_t;
typedef unsigned long __fsfilcnt_t;
typedef unsigned long __fsfilcnt64_t;
typedef long __fsword_t;
typedef long __ssize_t;
typedef long __syscall_slong_t;
typedef unsigned long __syscall_ulong_t;
typedef __off64_t __loff_t;
typedef char *__caddr_t;
typedef long __intptr_t;
typedef unsigned int __socklen_t;
typedef int __sig_atomic_t;
typedef __int8_t int8_t;
typedef __int16_t int16_t;
typedef __int32_t int32_t;
typedef __int64_t int64_t;
typedef __uint8_t uint8_t;
typedef __uint16_t uint16_t;
typedef __uint32_t uint32_t;
typedef __uint64_t uint64_t;
typedef __int_least8_t int_least8_t;
typedef __int_least16_t int_least16_t;
typedef __int_least32_t int_least32_t;
typedef __int_least64_t int_least64_t;
typedef __uint_least8_t uint_least8_t;
typedef __uint_least16_t uint_least16_t;
typedef __uint_least32_t uint_least32_t;
typedef __uint_least64_t uint_least64_t;
typedef signed char int_fast8_t;
typedef long int_fast16_t;
typedef long int_fast32_t;
typedef long int_fast64_t;
typedef unsigned char uint_fast8_t;
typedef unsigned long uint_fast16_t;
typedef unsigned long uint_fast32_t;
typedef unsigned long uint_fast64_t;
typedef long intptr_t;
typedef unsigned long uintptr_t;
typedef __intmax_t intmax_t;
typedef __uintmax_t uintmax_t;
typedef _Bool bool;
struct indirect_placeholder_15_ret_type;
struct indirect_placeholder_14_ret_type;
struct indirect_placeholder_16_ret_type;
struct indirect_placeholder_17_ret_type;
struct indirect_placeholder_18_ret_type;
struct indirect_placeholder_15_ret_type {
    uint64_t field_0;
    uint64_t field_1;
};
struct indirect_placeholder_14_ret_type {
    uint64_t field_0;
    uint64_t field_1;
};
struct indirect_placeholder_16_ret_type {
    uint64_t field_0;
    uint64_t field_1;
    uint64_t field_2;
};
struct indirect_placeholder_17_ret_type {
    uint64_t field_0;
    uint64_t field_1;
    uint64_t field_2;
};
struct indirect_placeholder_18_ret_type {
    uint64_t field_0;
    uint64_t field_1;
    uint64_t field_2;
    uint64_t field_3;
};
extern void abort(void);
extern uint64_t revng_init_local_sp(uint64_t param_0);
extern uint64_t init_rbx(void);
extern void indirect_placeholder(void);
extern uint64_t init_rbp(void);
extern uint64_t init_cc_src2(void);
extern uint64_t helper_cc_compute_c_wrapper(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint32_t param_3);
extern uint64_t init_r14(void);
extern uint64_t init_r13(void);
extern uint64_t init_r12(void);
extern uint64_t indirect_placeholder_1(uint64_t param_0);
extern uint64_t init_r15(void);
extern uint64_t indirect_placeholder_3(uint64_t param_0, uint64_t param_1, uint64_t param_2);
extern void indirect_placeholder_12(uint64_t param_0);
extern uint64_t indirect_placeholder_4(uint64_t param_0, uint64_t param_1);
extern void indirect_placeholder_13(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint64_t param_3, uint64_t param_4, uint64_t param_5, uint64_t param_6);
extern void indirect_placeholder_8(uint64_t param_0, uint64_t param_1, uint64_t param_2);
extern struct indirect_placeholder_15_ret_type indirect_placeholder_15(uint64_t param_0, uint64_t param_1, uint64_t param_2);
extern struct indirect_placeholder_14_ret_type indirect_placeholder_14(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint64_t param_3);
extern struct indirect_placeholder_16_ret_type indirect_placeholder_16(uint64_t param_0, uint64_t param_1);
extern struct indirect_placeholder_17_ret_type indirect_placeholder_17(uint64_t param_0, uint64_t param_1);
extern struct indirect_placeholder_18_ret_type indirect_placeholder_18(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint64_t param_3, uint64_t param_4);
void bb_main(uint64_t rdi, uint64_t rsi) {
    uint64_t var_56;
    uint64_t var_57;
    uint64_t local_sp_2;
    uint64_t rsi2_0;
    uint64_t rcx_0_be;
    uint64_t rax_0_be;
    uint64_t rax_0;
    uint64_t rsi2_1_be;
    uint64_t rcx_0;
    uint64_t rax_4;
    uint64_t rsi2_1;
    unsigned char var_60;
    uint64_t rax_1;
    uint64_t rax_2;
    uint64_t var_61;
    unsigned char var_62;
    uint64_t rdx_0;
    uint64_t var_63;
    struct indirect_placeholder_18_ret_type var_12;
    uint64_t local_sp_1;
    uint64_t var_0;
    uint64_t var_1;
    uint64_t var_2;
    uint64_t var_3;
    uint64_t var_4;
    uint64_t var_5;
    uint64_t var_6;
    uint64_t var_7;
    uint64_t var_8;
    uint64_t var_9;
    uint64_t local_sp_13;
    uint64_t rax_6;
    uint64_t rbp_1;
    uint64_t var_49;
    uint64_t var_50;
    uint64_t var_78;
    uint64_t var_79;
    uint64_t var_80;
    uint64_t var_81;
    uint64_t r8_1;
    uint64_t var_75;
    uint32_t var_76;
    uint64_t var_77;
    uint64_t var_58;
    uint64_t var_59;
    uint64_t r13_0;
    uint64_t r12_0;
    uint64_t rax_3;
    uint64_t var_53;
    uint64_t var_54;
    uint64_t var_55;
    uint64_t var_52;
    uint64_t rbp_0;
    unsigned char var_64;
    bool var_65;
    uint64_t rdx_1;
    uint64_t rcx_1;
    uint64_t var_66;
    unsigned char var_67;
    uint64_t rbx_3;
    uint64_t rcx_7;
    uint64_t var_40;
    struct indirect_placeholder_15_ret_type var_41;
    uint64_t var_42;
    uint64_t var_43;
    uint64_t var_44;
    struct indirect_placeholder_14_ret_type var_45;
    uint64_t var_46;
    uint64_t var_47;
    uint64_t var_48;
    uint64_t r12_1;
    uint64_t local_sp_3;
    struct indirect_placeholder_16_ret_type var_94;
    uint64_t var_95;
    uint64_t var_96;
    uint64_t r12_2;
    uint64_t rcx_3;
    uint64_t local_sp_4;
    uint64_t r15_0;
    uint64_t rcx_4;
    uint64_t local_sp_5;
    uint64_t rdx_3;
    uint64_t rax_9;
    uint64_t rax_5;
    unsigned char rbp_2;
    uint64_t rbx_0;
    uint64_t r15_2;
    uint64_t r13_1;
    uint64_t local_sp_7;
    uint64_t r15_1;
    uint64_t rcx_5;
    uint64_t local_sp_6;
    unsigned char var_68;
    uint64_t rbx_1;
    uint64_t rbx_2;
    uint64_t var_69;
    unsigned char var_70;
    uint32_t *var_71;
    unsigned char *var_72;
    uint64_t var_73;
    unsigned char var_74;
    uint64_t r13_2;
    uint64_t var_82;
    unsigned char var_83;
    bool var_84;
    uint64_t var_85;
    uint64_t var_86;
    uint64_t rax_7;
    uint64_t rdx_2;
    uint64_t var_87;
    unsigned char var_88;
    uint64_t var_89;
    uint64_t var_90;
    unsigned char *var_91;
    uint64_t var_92;
    uint64_t var_93;
    uint64_t local_sp_11;
    uint64_t r12_3;
    uint64_t local_sp_8;
    uint64_t r9_0;
    uint64_t r8_0;
    unsigned char var_97;
    unsigned char *var_98;
    uint32_t var_99;
    uint32_t var_19;
    uint64_t local_sp_9;
    struct indirect_placeholder_17_ret_type var_29;
    uint64_t var_30;
    uint64_t var_31;
    uint64_t var_32;
    uint64_t var_33;
    bool var_25;
    bool var_26;
    unsigned char *var_27;
    uint64_t var_28;
    uint64_t local_sp_10;
    bool var_34;
    uint64_t var_35;
    uint64_t var_36;
    uint64_t var_37;
    uint64_t var_38;
    uint64_t var_39;
    uint64_t var_51;
    uint64_t var_17;
    uint64_t var_18;
    uint64_t var_100;
    uint64_t r12_4;
    uint64_t r9_1;
    uint64_t var_20;
    uint64_t var_21;
    uint64_t var_22;
    uint64_t var_23;
    uint64_t var_24;
    uint64_t local_sp_12;
    uint64_t var_14;
    uint64_t var_15;
    uint64_t var_16;
    uint64_t var_10;
    uint64_t var_11;
    uint32_t var_13;
    unsigned int loop_state_var;
    bool switch_state_var;
    var_0 = revng_init_local_sp(0UL);
    var_1 = init_rbx();
    var_2 = init_r14();
    var_3 = init_rbp();
    var_4 = init_r13();
    var_5 = init_r12();
    var_6 = init_r15();
    var_7 = init_cc_src2();
    *(uint64_t *)(var_0 + (-8L)) = var_6;
    *(uint64_t *)(var_0 + (-16L)) = var_2;
    *(uint64_t *)(var_0 + (-24L)) = var_4;
    *(uint64_t *)(var_0 + (-32L)) = var_5;
    *(uint64_t *)(var_0 + (-40L)) = var_3;
    *(uint64_t *)(var_0 + (-48L)) = var_1;
    *(uint32_t *)(var_0 + (-208L)) = (uint32_t)rdi;
    var_8 = *(uint64_t *)rsi;
    *(uint64_t *)(var_0 + (-240L)) = 4201786UL;
    indirect_placeholder_12(var_8);
    *(uint64_t *)(var_0 + (-248L)) = 4201801UL;
    indirect_placeholder();
    var_9 = var_0 + (-256L);
    *(uint64_t *)var_9 = 4201811UL;
    indirect_placeholder();
    *(unsigned char *)(var_0 + (-228L)) = (unsigned char)'\x00';
    *(unsigned char *)var_9 = (unsigned char)'\x00';
    rsi2_0 = 4UL;
    local_sp_13 = var_9;
    r13_0 = 4248917UL;
    r12_0 = 4248918UL;
    rax_3 = 4248918UL;
    rbp_0 = 1UL;
    rcx_1 = 1UL;
    r12_1 = 4248918UL;
    r12_2 = 4248918UL;
    r15_0 = 14UL;
    rdx_3 = 1UL;
    rbp_2 = (unsigned char)'\x00';
    r13_1 = 255UL;
    rdx_2 = 1UL;
    r12_4 = var_5;
    while (1U)
        {
            var_10 = (uint64_t)*(uint32_t *)(local_sp_13 + 24UL);
            var_11 = local_sp_13 + (-8L);
            *(uint64_t *)var_11 = 4202002UL;
            var_12 = indirect_placeholder_18(4247675UL, var_10, 4248704UL, rsi, 0UL);
            var_13 = (uint32_t)var_12.field_0;
            local_sp_11 = var_11;
            local_sp_12 = var_11;
            local_sp_13 = var_11;
            if ((uint64_t)(var_13 + 1U) == 0UL) {
                if ((uint64_t)(var_13 + (-80)) == 0UL) {
                    *(unsigned char *)(local_sp_13 + 20UL) = (unsigned char)'\x01';
                    continue;
                }
                if ((int)var_13 > (int)80U) {
                    if ((uint64_t)(var_13 + (-112)) == 0UL) {
                        *(unsigned char *)var_11 = (unsigned char)'\x01';
                        continue;
                    }
                    if ((uint64_t)(var_13 + (-128)) == 0UL) {
                        break;
                    }
                    *(unsigned char *)(local_sp_13 + 20UL) = (unsigned char)'\x01';
                    *(unsigned char *)var_11 = (unsigned char)'\x01';
                    continue;
                }
                if ((uint64_t)(var_13 + 131U) != 0UL) {
                    if ((uint64_t)(var_13 + 130U) != 0UL) {
                        break;
                    }
                    *(uint64_t *)(local_sp_13 + (-16L)) = 4201885UL;
                    indirect_placeholder_8(var_1, var_3, 0UL);
                    abort();
                }
                *(uint64_t *)(local_sp_13 + (-16L)) = 0UL;
                *(uint64_t *)(local_sp_13 + (-24L)) = 4247662UL;
                var_14 = *(uint64_t *)6357984UL;
                var_15 = *(uint64_t *)6358144UL;
                *(uint64_t *)(local_sp_13 + (-32L)) = 4201938UL;
                indirect_placeholder_13(0UL, 4247591UL, var_15, var_14, 4247488UL, 4247634UL, 4247650UL);
                var_16 = local_sp_13 + (-40L);
                *(uint64_t *)var_16 = 4201948UL;
                indirect_placeholder();
                local_sp_12 = var_16;
                break;
            }
            var_17 = var_12.field_2;
            var_18 = var_12.field_3;
            *(unsigned char *)(local_sp_13 + 22UL) = (unsigned char)'\x01';
            r9_1 = var_17;
            r8_1 = var_18;
            if ((uint64_t)(*(uint32_t *)6358108UL - *(uint32_t *)(local_sp_13 + 16UL)) == 0UL) {
                var_100 = var_12.field_1;
                *(uint64_t *)(local_sp_13 + (-16L)) = 4202057UL;
                indirect_placeholder_13(0UL, 4247679UL, 0UL, var_100, 0UL, var_17, var_18);
                *(uint64_t *)(local_sp_13 + (-24L)) = 4202067UL;
                indirect_placeholder_8(var_1, var_3, 1UL);
                abort();
            }
            *(unsigned char *)(local_sp_13 + 23UL) = (*(unsigned char *)var_11 | (unsigned char)*(uint32_t *)(local_sp_13 + 20UL));
            *(uint64_t *)(local_sp_13 + 8UL) = rsi;
            var_19 = *(uint32_t *)6358108UL;
            r12_3 = r12_4;
            r9_0 = r9_1;
            r8_0 = r8_1;
            while ((int)var_19 >= (int)*(uint32_t *)(local_sp_11 + 24UL))
                {
                    var_20 = (uint64_t)var_19 << 32UL;
                    var_21 = (uint64_t)var_19;
                    var_22 = *(uint64_t *)(local_sp_11 + 16UL);
                    var_23 = *(uint64_t *)((uint64_t)((long)var_20 >> (long)29UL) + var_22);
                    var_24 = local_sp_11 + (-8L);
                    *(uint64_t *)var_24 = 4202086UL;
                    indirect_placeholder();
                    rax_0 = var_23;
                    rcx_0 = var_22;
                    rcx_3 = var_22;
                    rbx_0 = var_23;
                    local_sp_9 = var_24;
                    local_sp_10 = var_24;
                    if (*(unsigned char *)(local_sp_11 + 20UL) != '\x00') {
                        var_25 = (var_19 == 0U);
                        var_26 = (var_23 == var_21);
                        var_27 = (unsigned char *)(var_21 + (-1L));
                        while (1U)
                            {
                                var_28 = local_sp_9 + (-8L);
                                *(uint64_t *)var_28 = 4202179UL;
                                indirect_placeholder();
                                local_sp_9 = var_28;
                                local_sp_10 = var_28;
                                if (!var_25) {
                                    loop_state_var = 0U;
                                    break;
                                }
                                if (!var_26) {
                                    loop_state_var = 1U;
                                    break;
                                }
                                if (*var_27 == '/') {
                                    continue;
                                }
                                loop_state_var = 1U;
                                break;
                            }
                        switch (loop_state_var) {
                          case 0U:
                            {
                                break;
                            }
                            break;
                          case 1U:
                            {
                                *(uint64_t *)(local_sp_9 + (-16L)) = 4202122UL;
                                var_29 = indirect_placeholder_17(4UL, var_23);
                                var_30 = var_29.field_0;
                                var_31 = var_29.field_1;
                                var_32 = var_29.field_2;
                                var_33 = local_sp_9 + (-24L);
                                *(uint64_t *)var_33 = 4202150UL;
                                indirect_placeholder_13(0UL, 4248344UL, 0UL, var_30, 0UL, var_31, var_32);
                                *(unsigned char *)(local_sp_9 + 5UL) = (unsigned char)'\x00';
                                local_sp_8 = var_33;
                                r9_0 = var_31;
                                r8_0 = var_32;
                                var_97 = *(unsigned char *)(local_sp_8 + 29UL);
                                var_98 = (unsigned char *)(local_sp_8 + 30UL);
                                *var_98 = (var_97 & *var_98);
                                var_99 = *(uint32_t *)6358108UL + 1U;
                                *(uint32_t *)6358108UL = var_99;
                                r8_1 = r8_0;
                                local_sp_11 = local_sp_8;
                                var_19 = var_99;
                                r12_4 = r12_3;
                                r9_1 = r9_0;
                                r12_3 = r12_4;
                                r9_0 = r9_1;
                                r8_0 = r8_1;
                                continue;
                            }
                            break;
                        }
                    }
                    var_34 = (var_19 == 0U);
                    var_35 = var_34;
                    var_36 = (r12_4 & (-256L)) | var_35;
                    var_37 = var_35 & (uint64_t)*(unsigned char *)(local_sp_10 + 31UL);
                    *(unsigned char *)(local_sp_10 + 29UL) = (unsigned char)var_37;
                    r12_3 = var_36;
                    if (var_37 != 0UL) {
                        var_38 = local_sp_10 + (-8L);
                        *(uint64_t *)var_38 = 4202230UL;
                        indirect_placeholder_13(0UL, 4247695UL, 0UL, var_22, 0UL, r9_1, r8_1);
                        *(unsigned char *)(local_sp_10 + 21UL) = (unsigned char)'\x00';
                        local_sp_8 = var_38;
                        var_97 = *(unsigned char *)(local_sp_8 + 29UL);
                        var_98 = (unsigned char *)(local_sp_8 + 30UL);
                        *var_98 = (var_97 & *var_98);
                        var_99 = *(uint32_t *)6358108UL + 1U;
                        *(uint32_t *)6358108UL = var_99;
                        r8_1 = r8_0;
                        local_sp_11 = local_sp_8;
                        var_19 = var_99;
                        r12_4 = r12_3;
                        r9_1 = r9_0;
                        r12_3 = r12_4;
                        r9_0 = r9_1;
                        r8_0 = r8_1;
                        continue;
                    }
                    r8_0 = var_21;
                    if (*(unsigned char *)local_sp_10 == '\x00') {
                        var_49 = local_sp_10 + 32UL;
                        var_50 = local_sp_10 + (-8L);
                        *(uint64_t *)var_50 = 4202409UL;
                        var_51 = indirect_placeholder_4(var_23, var_49);
                        local_sp_1 = var_50;
                        if ((uint64_t)(uint32_t)var_51 != 0UL) {
                            var_52 = local_sp_10 + (-16L);
                            *(uint64_t *)var_52 = 4202423UL;
                            indirect_placeholder();
                            rbp_0 = 0UL;
                            local_sp_1 = var_52;
                            if (!(((*(uint32_t *)var_51 == 2U) ^ 1) || var_34)) {
                                *(uint64_t *)(local_sp_10 + (-24L)) = 4202456UL;
                                var_53 = indirect_placeholder_3(var_23, 0UL, 3UL);
                                *(uint64_t *)(local_sp_10 + (-32L)) = 4202464UL;
                                indirect_placeholder();
                                var_54 = (uint64_t)*(uint32_t *)var_53;
                                var_55 = local_sp_10 + (-40L);
                                *(uint64_t *)var_55 = 4202489UL;
                                indirect_placeholder_13(0UL, 4248853UL, 0UL, var_53, var_54, r9_1, r8_1);
                                local_sp_8 = var_55;
                                var_97 = *(unsigned char *)(local_sp_8 + 29UL);
                                var_98 = (unsigned char *)(local_sp_8 + 30UL);
                                *var_98 = (var_97 & *var_98);
                                var_99 = *(uint32_t *)6358108UL + 1U;
                                *(uint32_t *)6358108UL = var_99;
                                r8_1 = r8_0;
                                local_sp_11 = local_sp_8;
                                var_19 = var_99;
                                r12_4 = r12_3;
                                r9_1 = r9_0;
                                r12_3 = r12_4;
                                r9_0 = r9_1;
                                r8_0 = r8_1;
                                continue;
                            }
                        }
                        local_sp_2 = local_sp_1;
                        local_sp_8 = local_sp_1;
                        if (rbp_0 < (var_19 > 1023U)) {
                            *(uint64_t *)(local_sp_1 + (-8L)) = 4202537UL;
                            indirect_placeholder();
                            *(uint32_t *)4248918UL = 0U;
                            var_58 = local_sp_1 + (-16L);
                            *(uint64_t *)var_58 = 4202556UL;
                            indirect_placeholder();
                            var_59 = helper_cc_compute_c_wrapper(var_21 + (-4248918L), 4248918UL, var_7, 17U);
                            local_sp_2 = var_58;
                            local_sp_3 = var_58;
                            local_sp_4 = var_58;
                            if (var_59 != 0UL) {
                                *(uint64_t *)(local_sp_3 + (-8L)) = 4202638UL;
                                var_94 = indirect_placeholder_16(4UL, var_23);
                                var_95 = var_94.field_0;
                                var_96 = local_sp_3 + (-16L);
                                *(uint64_t *)var_96 = 4202673UL;
                                indirect_placeholder_13(0UL, 4248568UL, 0UL, r13_0, 0UL, var_95, var_21);
                                r12_3 = r12_1;
                                local_sp_8 = var_96;
                                r9_0 = var_95;
                                var_97 = *(unsigned char *)(local_sp_8 + 29UL);
                                var_98 = (unsigned char *)(local_sp_8 + 30UL);
                                *var_98 = (var_97 & *var_98);
                                var_99 = *(uint32_t *)6358108UL + 1U;
                                *(uint32_t *)6358108UL = var_99;
                                r8_1 = r8_0;
                                local_sp_11 = local_sp_8;
                                var_19 = var_99;
                                r12_4 = r12_3;
                                r9_1 = r9_0;
                                r12_3 = r12_4;
                                r9_0 = r9_1;
                                r8_0 = r8_1;
                                continue;
                            }
                            if ((rbp_0 ^ 1UL) <= (uint64_t)*(unsigned char *)var_58) {
                                rax_4 = rax_3;
                                rcx_4 = rcx_3;
                                local_sp_5 = local_sp_4;
                                r12_3 = r12_2;
                                local_sp_8 = local_sp_4;
                                if (*(unsigned char *)local_sp_4 != '\x00') {
                                    *(unsigned char *)(local_sp_4 + 29UL) = (unsigned char)'\x01';
                                    var_97 = *(unsigned char *)(local_sp_8 + 29UL);
                                    var_98 = (unsigned char *)(local_sp_8 + 30UL);
                                    *var_98 = (var_97 & *var_98);
                                    var_99 = *(uint32_t *)6358108UL + 1U;
                                    *(uint32_t *)6358108UL = var_99;
                                    r8_1 = r8_0;
                                    local_sp_11 = local_sp_8;
                                    var_19 = var_99;
                                    r12_4 = r12_3;
                                    r9_1 = r9_0;
                                    r12_3 = r12_4;
                                    r9_0 = r9_1;
                                    r8_0 = r8_1;
                                    continue;
                                }
                            }
                            rsi2_1 = rsi2_0;
                            r12_2 = r12_0;
                            local_sp_4 = local_sp_2;
                            local_sp_5 = local_sp_2;
                            while (1U)
                                {
                                    var_60 = *(unsigned char *)rax_0;
                                    rcx_0_be = rcx_0;
                                    rsi2_1_be = rsi2_1;
                                    rax_1 = rax_0;
                                    rax_2 = rax_0;
                                    rdx_0 = (uint64_t)var_60;
                                    rcx_3 = rcx_0;
                                    if ((uint64_t)(var_60 + '\xd1') != 0UL) {
                                        var_61 = rax_1 + 1UL;
                                        var_62 = *(unsigned char *)var_61;
                                        rax_1 = var_61;
                                        rax_2 = var_61;
                                        do {
                                            var_61 = rax_1 + 1UL;
                                            var_62 = *(unsigned char *)var_61;
                                            rax_1 = var_61;
                                            rax_2 = var_61;
                                        } while (var_62 != '/');
                                        rdx_0 = (uint64_t)var_62;
                                    }
                                    rax_3 = rax_2;
                                    if (rdx_0 != 0UL) {
                                        loop_state_var = 0U;
                                        break;
                                    }
                                    var_63 = rax_2 + 1UL;
                                    var_64 = *(unsigned char *)var_63;
                                    rax_0_be = var_63;
                                    var_65 = (var_64 != '/');
                                    rcx_0_be = 1UL;
                                    rsi2_1_be = (rsi2_1 & (-256L)) | var_65;
                                    if (var_64 != '\x00' & !var_65) {
                                        rdx_1 = rax_2 + 2UL;
                                        while (1U)
                                            {
                                                switch_state_var = 1;
                                                break;
                                            }
                                        if (var_66 <= 255UL) {
                                            loop_state_var = 1U;
                                            break;
                                        }
                                        rsi2_1_be = (uint64_t)var_67;
                                    }
                                    rax_0 = rax_0_be;
                                    rcx_0 = rcx_0_be;
                                    rsi2_1 = rsi2_1_be;
                                    continue;
                                }
                            switch (loop_state_var) {
                              case 0U:
                                {
                                    rax_4 = rax_3;
                                    rcx_4 = rcx_3;
                                    local_sp_5 = local_sp_4;
                                    r12_3 = r12_2;
                                    local_sp_8 = local_sp_4;
                                    if (*(unsigned char *)local_sp_4 != '\x00') {
                                        *(unsigned char *)(local_sp_4 + 29UL) = (unsigned char)'\x01';
                                        var_97 = *(unsigned char *)(local_sp_8 + 29UL);
                                        var_98 = (unsigned char *)(local_sp_8 + 30UL);
                                        *var_98 = (var_97 & *var_98);
                                        var_99 = *(uint32_t *)6358108UL + 1U;
                                        *(uint32_t *)6358108UL = var_99;
                                        r8_1 = r8_0;
                                        local_sp_11 = local_sp_8;
                                        var_19 = var_99;
                                        r12_4 = r12_3;
                                        r9_1 = r9_0;
                                        r12_3 = r12_4;
                                        r9_0 = r9_1;
                                        r8_0 = r8_1;
                                        continue;
                                    }
                                }
                                break;
                              case 1U:
                                {
                                    r15_0 = (uint64_t)((long)(*(uint64_t *)local_sp_2 << 63UL) >> (long)63UL) & 14UL;
                                }
                                break;
                            }
                        } else {
                            var_56 = rbp_0 ^ 1UL;
                            var_57 = helper_cc_compute_c_wrapper((uint64_t)*(unsigned char *)local_sp_1 - var_56, var_56, var_7, 14U);
                            r12_0 = var_36;
                            rsi2_0 = var_49;
                            if (var_57 == 0UL) {
                                *(unsigned char *)(local_sp_1 + 29UL) = (unsigned char)'\x01';
                                var_97 = *(unsigned char *)(local_sp_8 + 29UL);
                                var_98 = (unsigned char *)(local_sp_8 + 30UL);
                                *var_98 = (var_97 & *var_98);
                                var_99 = *(uint32_t *)6358108UL + 1U;
                                *(uint32_t *)6358108UL = var_99;
                                r8_1 = r8_0;
                                local_sp_11 = local_sp_8;
                                var_19 = var_99;
                                r12_4 = r12_3;
                                r9_1 = r9_0;
                                r12_3 = r12_4;
                                r9_0 = r9_1;
                                r8_0 = r8_1;
                                continue;
                            }
                            rsi2_1 = rsi2_0;
                            r12_2 = r12_0;
                            local_sp_4 = local_sp_2;
                            local_sp_5 = local_sp_2;
                            while (1U)
                                {
                                    var_60 = *(unsigned char *)rax_0;
                                    rcx_0_be = rcx_0;
                                    rsi2_1_be = rsi2_1;
                                    rax_1 = rax_0;
                                    rax_2 = rax_0;
                                    rdx_0 = (uint64_t)var_60;
                                    rcx_3 = rcx_0;
                                    if ((uint64_t)(var_60 + '\xd1') != 0UL) {
                                        var_61 = rax_1 + 1UL;
                                        var_62 = *(unsigned char *)var_61;
                                        rax_1 = var_61;
                                        rax_2 = var_61;
                                        do {
                                            var_61 = rax_1 + 1UL;
                                            var_62 = *(unsigned char *)var_61;
                                            rax_1 = var_61;
                                            rax_2 = var_61;
                                        } while (var_62 != '/');
                                        rdx_0 = (uint64_t)var_62;
                                    }
                                    rax_3 = rax_2;
                                    if (rdx_0 != 0UL) {
                                        loop_state_var = 0U;
                                        break;
                                    }
                                    var_63 = rax_2 + 1UL;
                                    var_64 = *(unsigned char *)var_63;
                                    rax_0_be = var_63;
                                    var_65 = (var_64 != '/');
                                    rcx_0_be = 1UL;
                                    rsi2_1_be = (rsi2_1 & (-256L)) | var_65;
                                    if (var_64 != '\x00' & !var_65) {
                                        rdx_1 = rax_2 + 2UL;
                                        while (1U)
                                            {
                                                switch_state_var = 1;
                                                break;
                                            }
                                        if (var_66 <= 255UL) {
                                            loop_state_var = 1U;
                                            break;
                                        }
                                        rsi2_1_be = (uint64_t)var_67;
                                    }
                                    rax_0 = rax_0_be;
                                    rcx_0 = rcx_0_be;
                                    rsi2_1 = rsi2_1_be;
                                    continue;
                                }
                            switch (loop_state_var) {
                              case 1U:
                                {
                                    r15_0 = (uint64_t)((long)(*(uint64_t *)local_sp_2 << 63UL) >> (long)63UL) & 14UL;
                                }
                                break;
                              case 0U:
                                {
                                    rax_4 = rax_3;
                                    rcx_4 = rcx_3;
                                    local_sp_5 = local_sp_4;
                                    r12_3 = r12_2;
                                    local_sp_8 = local_sp_4;
                                    if (*(unsigned char *)local_sp_4 != '\x00') {
                                        *(unsigned char *)(local_sp_4 + 29UL) = (unsigned char)'\x01';
                                        var_97 = *(unsigned char *)(local_sp_8 + 29UL);
                                        var_98 = (unsigned char *)(local_sp_8 + 30UL);
                                        *var_98 = (var_97 & *var_98);
                                        var_99 = *(uint32_t *)6358108UL + 1U;
                                        *(uint32_t *)6358108UL = var_99;
                                        r8_1 = r8_0;
                                        local_sp_11 = local_sp_8;
                                        var_19 = var_99;
                                        r12_4 = r12_3;
                                        r9_1 = r9_0;
                                        r12_3 = r12_4;
                                        r9_0 = r9_1;
                                        r8_0 = r8_1;
                                        continue;
                                    }
                                }
                                break;
                            }
                        }
                    } else {
                        var_39 = local_sp_10 + (-8L);
                        *(uint64_t *)var_39 = 4202263UL;
                        indirect_placeholder();
                        r13_0 = 255UL;
                        rax_3 = 0UL;
                        r12_1 = var_36;
                        local_sp_3 = var_39;
                        r12_2 = var_36;
                        local_sp_4 = var_39;
                        if (*(unsigned char *)var_23 != '\x00') {
                            var_40 = local_sp_10 + 24UL;
                            *(uint64_t *)var_40 = 0UL;
                            *(uint64_t *)(local_sp_10 + (-16L)) = 4202305UL;
                            var_41 = indirect_placeholder_15(var_40, var_23, var_21);
                            var_42 = var_41.field_0;
                            *(uint64_t *)(local_sp_10 + (-24L)) = 4202326UL;
                            var_43 = indirect_placeholder_3(var_23, 0UL, 4UL);
                            var_44 = (var_42 > 4UL) ? 1UL : var_42;
                            *(uint64_t *)(local_sp_10 + (-32L)) = 4202360UL;
                            var_45 = indirect_placeholder_14(var_23, 1UL, var_44, 8UL);
                            var_46 = var_45.field_0;
                            var_47 = var_45.field_1;
                            var_48 = local_sp_10 + (-40L);
                            *(uint64_t *)var_48 = 4202391UL;
                            indirect_placeholder_13(0UL, 4248464UL, 0UL, var_46, 0UL, var_47, var_43);
                            r12_3 = var_43;
                            local_sp_8 = var_48;
                            r9_0 = var_47;
                            r8_0 = var_43;
                            var_97 = *(unsigned char *)(local_sp_8 + 29UL);
                            var_98 = (unsigned char *)(local_sp_8 + 30UL);
                            *var_98 = (var_97 & *var_98);
                            var_99 = *(uint32_t *)6358108UL + 1U;
                            *(uint32_t *)6358108UL = var_99;
                            r8_1 = r8_0;
                            local_sp_11 = local_sp_8;
                            var_19 = var_99;
                            r12_4 = r12_3;
                            r9_1 = r9_0;
                            r12_3 = r12_4;
                            r9_0 = r9_1;
                            r8_0 = r8_1;
                            continue;
                        }
                        if (var_19 > 255U) {
                            *(uint64_t *)(local_sp_3 + (-8L)) = 4202638UL;
                            var_94 = indirect_placeholder_16(4UL, var_23);
                            var_95 = var_94.field_0;
                            var_96 = local_sp_3 + (-16L);
                            *(uint64_t *)var_96 = 4202673UL;
                            indirect_placeholder_13(0UL, 4248568UL, 0UL, r13_0, 0UL, var_95, var_21);
                            r12_3 = r12_1;
                            local_sp_8 = var_96;
                            r9_0 = var_95;
                            var_97 = *(unsigned char *)(local_sp_8 + 29UL);
                            var_98 = (unsigned char *)(local_sp_8 + 30UL);
                            *var_98 = (var_97 & *var_98);
                            var_99 = *(uint32_t *)6358108UL + 1U;
                            *(uint32_t *)6358108UL = var_99;
                            r8_1 = r8_0;
                            local_sp_11 = local_sp_8;
                            var_19 = var_99;
                            r12_4 = r12_3;
                            r9_1 = r9_0;
                            r12_3 = r12_4;
                            r9_0 = r9_1;
                            r8_0 = r8_1;
                            continue;
                        }
                        rax_4 = rax_3;
                        rcx_4 = rcx_3;
                        local_sp_5 = local_sp_4;
                        r12_3 = r12_2;
                        local_sp_8 = local_sp_4;
                        if (*(unsigned char *)local_sp_4 != '\x00') {
                            *(unsigned char *)(local_sp_4 + 29UL) = (unsigned char)'\x01';
                            var_97 = *(unsigned char *)(local_sp_8 + 29UL);
                            var_98 = (unsigned char *)(local_sp_8 + 30UL);
                            *var_98 = (var_97 & *var_98);
                            var_99 = *(uint32_t *)6358108UL + 1U;
                            *(uint32_t *)6358108UL = var_99;
                            r8_1 = r8_0;
                            local_sp_11 = local_sp_8;
                            var_19 = var_99;
                            r12_4 = r12_3;
                            r9_1 = r9_0;
                            r12_3 = r12_4;
                            r9_0 = r9_1;
                            r8_0 = r8_1;
                            continue;
                        }
                        rax_5 = rax_4;
                        r15_1 = r15_0;
                        rcx_5 = rcx_4;
                        local_sp_6 = local_sp_5;
                        while (1U)
                            {
                                var_68 = *(unsigned char *)rbx_0;
                                rax_6 = rax_5;
                                rbp_1 = (uint64_t)var_68;
                                rcx_7 = rcx_5;
                                r15_2 = r15_1;
                                local_sp_7 = local_sp_6;
                                rbx_1 = rbx_0;
                                rbx_2 = rbx_0;
                                r13_2 = r15_1;
                                local_sp_8 = local_sp_6;
                                if ((uint64_t)(var_68 + '\xd1') != 0UL) {
                                    var_69 = rbx_1 + 1UL;
                                    var_70 = *(unsigned char *)var_69;
                                    rbx_1 = var_69;
                                    rbx_2 = var_69;
                                    do {
                                        var_69 = rbx_1 + 1UL;
                                        var_70 = *(unsigned char *)var_69;
                                        rbx_1 = var_69;
                                        rbx_2 = var_69;
                                    } while (var_70 != '/');
                                    rbp_1 = (uint64_t)var_70;
                                }
                                r12_3 = rbx_2;
                                if (rbp_1 != 0UL) {
                                    loop_state_var = 2U;
                                    break;
                                }
                                if (r15_1 != 0UL) {
                                    *(uint64_t *)(local_sp_6 + (-8L)) = 4202865UL;
                                    indirect_placeholder();
                                    var_71 = (uint32_t *)rax_5;
                                    *var_71 = 0U;
                                    var_72 = (unsigned char *)rbx_2;
                                    *var_72 = (unsigned char)'\x00';
                                    var_73 = local_sp_6 + (-16L);
                                    *(uint64_t *)var_73 = 4202889UL;
                                    indirect_placeholder();
                                    var_74 = (unsigned char)rbp_1;
                                    *var_72 = var_74;
                                    r13_2 = rax_5;
                                    r15_2 = 0UL;
                                    local_sp_7 = var_73;
                                    var_75 = local_sp_6 + (-24L);
                                    *(uint64_t *)var_75 = 4202908UL;
                                    indirect_placeholder();
                                    var_76 = *var_71;
                                    var_77 = (uint64_t)var_76;
                                    rax_6 = var_77;
                                    r13_2 = 18446744073709551615UL;
                                    local_sp_7 = var_75;
                                    r13_2 = r13_1;
                                    r15_2 = r13_1;
                                    if ((long)rax_5 >= (long)0UL & var_76 != 0U & (uint64_t)(var_76 + (-2)) != 0UL) {
                                        loop_state_var = 0U;
                                        break;
                                    }
                                }
                                var_82 = rbx_2 + 1UL;
                                var_83 = *(unsigned char *)var_82;
                                rbx_3 = var_82;
                                rax_9 = rax_6;
                                r13_1 = r13_2;
                                r15_1 = r15_2;
                                local_sp_6 = local_sp_7;
                                var_84 = (var_83 != '/');
                                var_85 = (rcx_5 & (-256L)) | var_84;
                                var_86 = rbx_2 + 2UL;
                                rax_7 = var_86;
                                rax_9 = var_86;
                                rbp_2 = (unsigned char)'/';
                                rcx_7 = var_85;
                                if (var_83 != '\x00' & var_84) {
                                    while (1U)
                                        {
                                            switch_state_var = 1;
                                            break;
                                        }
                                }
                                var_90 = helper_cc_compute_c_wrapper(r13_2 - rdx_3, rdx_3, var_7, 17U);
                                rax_5 = rax_9;
                                rbx_0 = rbx_3;
                                rcx_5 = rcx_7;
                                r8_0 = rdx_3;
                                if (var_90 == 0UL) {
                                    continue;
                                }
                                loop_state_var = 1U;
                                break;
                            }
                        switch (loop_state_var) {
                          case 2U:
                            {
                                *(unsigned char *)(local_sp_6 + 29UL) = (unsigned char)'\x01';
                            }
                            break;
                          case 1U:
                            {
                                var_91 = (unsigned char *)rbx_3;
                                *var_91 = (unsigned char)'\x00';
                                *(uint64_t *)(local_sp_7 + (-8L)) = 4203092UL;
                                var_92 = indirect_placeholder_1(rbx_2);
                                var_93 = local_sp_7 + (-16L);
                                *(uint64_t *)var_93 = 4203126UL;
                                indirect_placeholder_13(0UL, 4248624UL, 0UL, r13_2, 0UL, var_92, rdx_3);
                                *var_91 = rbp_2;
                                local_sp_8 = var_93;
                                r9_0 = var_92;
                            }
                            break;
                          case 0U:
                            {
                                var_78 = (var_23 == rbx_2) ? 4247632UL : var_23;
                                *var_72 = (unsigned char)'\x00';
                                *(uint64_t *)(local_sp_6 + (-32L)) = 4202947UL;
                                var_79 = indirect_placeholder_3(var_78, 0UL, 3UL);
                                *(uint64_t *)(local_sp_6 + (-40L)) = 4202955UL;
                                indirect_placeholder();
                                var_80 = (uint64_t)*(uint32_t *)var_79;
                                var_81 = local_sp_6 + (-48L);
                                *(uint64_t *)var_81 = 4202980UL;
                                indirect_placeholder_13(0UL, 4248853UL, 0UL, var_79, var_80, r9_1, r8_1);
                                *var_72 = var_74;
                                local_sp_8 = var_81;
                            }
                            break;
                        }
                    }
                }
            return;
        }
    *(uint64_t *)(local_sp_12 + (-8L)) = 4201958UL;
    indirect_placeholder_8(var_1, var_3, 1UL);
    abort();
}

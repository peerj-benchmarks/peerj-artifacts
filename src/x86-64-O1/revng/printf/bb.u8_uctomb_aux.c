typedef unsigned char __u_char;
typedef unsigned short __u_short;
typedef unsigned int __u_int;
typedef unsigned long __u_long;
typedef signed char __int8_t;
typedef unsigned char __uint8_t;
typedef short __int16_t;
typedef unsigned short __uint16_t;
typedef int __int32_t;
typedef unsigned int __uint32_t;
typedef long __int64_t;
typedef unsigned long __uint64_t;
typedef __int8_t __int_least8_t;
typedef __uint8_t __uint_least8_t;
typedef __int16_t __int_least16_t;
typedef __uint16_t __uint_least16_t;
typedef __int32_t __int_least32_t;
typedef __uint32_t __uint_least32_t;
typedef __int64_t __int_least64_t;
typedef __uint64_t __uint_least64_t;
typedef long __quad_t;
typedef unsigned long __u_quad_t;
typedef long __intmax_t;
typedef unsigned long __uintmax_t;
typedef unsigned long __dev_t;
typedef unsigned int __uid_t;
typedef unsigned int __gid_t;
typedef unsigned long __ino_t;
typedef unsigned long __ino64_t;
typedef unsigned int __mode_t;
typedef unsigned long __nlink_t;
typedef long __off_t;
typedef long __off64_t;
typedef int __pid_t;
typedef struct {
    int __val[2];
} __fsid_t;
typedef long __clock_t;
typedef unsigned long __rlim_t;
typedef unsigned long __rlim64_t;
typedef unsigned int __id_t;
typedef long __time_t;
typedef unsigned int __useconds_t;
typedef long __suseconds_t;
typedef int __daddr_t;
typedef int __key_t;
typedef int __clockid_t;
typedef void *__timer_t;
typedef long __blksize_t;
typedef long __blkcnt_t;
typedef long __blkcnt64_t;
typedef unsigned long __fsblkcnt_t;
typedef unsigned long __fsblkcnt64_t;
typedef unsigned long __fsfilcnt_t;
typedef unsigned long __fsfilcnt64_t;
typedef long __fsword_t;
typedef long __ssize_t;
typedef long __syscall_slong_t;
typedef unsigned long __syscall_ulong_t;
typedef __off64_t __loff_t;
typedef char *__caddr_t;
typedef long __intptr_t;
typedef unsigned int __socklen_t;
typedef int __sig_atomic_t;
typedef __int8_t int8_t;
typedef __int16_t int16_t;
typedef __int32_t int32_t;
typedef __int64_t int64_t;
typedef __uint8_t uint8_t;
typedef __uint16_t uint16_t;
typedef __uint32_t uint32_t;
typedef __uint64_t uint64_t;
typedef __int_least8_t int_least8_t;
typedef __int_least16_t int_least16_t;
typedef __int_least32_t int_least32_t;
typedef __int_least64_t int_least64_t;
typedef __uint_least8_t uint_least8_t;
typedef __uint_least16_t uint_least16_t;
typedef __uint_least32_t uint_least32_t;
typedef __uint_least64_t uint_least64_t;
typedef signed char int_fast8_t;
typedef long int_fast16_t;
typedef long int_fast32_t;
typedef long int_fast64_t;
typedef unsigned char uint_fast8_t;
typedef unsigned long uint_fast16_t;
typedef unsigned long uint_fast32_t;
typedef unsigned long uint_fast64_t;
typedef long intptr_t;
typedef unsigned long uintptr_t;
typedef __intmax_t intmax_t;
typedef __uintmax_t uintmax_t;
extern uint64_t revng_init_local_sp(uint64_t param_0);
uint64_t bb_u8_uctomb_aux(uint64_t rdx, uint64_t rdi, uint64_t rsi) {
    uint32_t var_0;
    uint64_t var_1;
    uint64_t merge;
    uint64_t rsi3_0;
    uint64_t storemerge;
    uint64_t rax_0;
    uint64_t rsi3_1;
    revng_init_local_sp(0UL);
    var_0 = (uint32_t)rsi;
    merge = 4294967294UL;
    rsi3_0 = rsi;
    storemerge = 4UL;
    rax_0 = 2UL;
    rsi3_1 = rsi;
    if ((uint64_t)(var_0 & (-128)) == 0UL) {
        return merge;
    }
    merge = 4294967295UL;
    if ((uint64_t)(var_0 & (-2048)) == 0UL) {
        merge = 4294967294UL;
        if ((int)(uint32_t)rdx <= (int)1U) {
            return merge;
        }
    }
    var_1 = (uint64_t)(var_0 & (-65536));
    if (var_1 == 0UL) {
        storemerge = 3UL;
        if ((uint64_t)((var_0 + (-55296)) & (-2048)) == 0UL) {
            return merge;
        }
        merge = 4294967294UL;
        if ((int)(uint32_t)rdx > (int)2U) {
            return merge;
        }
    }
    if (var_1 > 1114111UL) {
        return merge;
    }
    merge = 4294967294UL;
    if ((int)(uint32_t)rdx > (int)3U) {
        return merge;
    }
    *(unsigned char *)(rdi + 3UL) = (((unsigned char)rsi & '?') | '\x80');
    rsi3_0 = (uint64_t)((uint32_t)(rsi >> 6UL) & 67043327U) | 65536UL;
    *(unsigned char *)(rdi + 2UL) = (((unsigned char)rsi3_0 & '?') | '\x80');
    rax_0 = storemerge;
    rsi3_1 = (uint64_t)((uint32_t)(rsi3_0 >> 6UL) & 67106815U) | 2048UL;
    *(unsigned char *)(rdi + 1UL) = (((unsigned char)rsi3_1 & '?') | '\x80');
    *(unsigned char *)rdi = ((unsigned char)(rsi3_1 >> 6UL) | '\xc0');
    return rax_0;
}

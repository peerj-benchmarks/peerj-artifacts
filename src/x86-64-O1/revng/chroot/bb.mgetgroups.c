typedef unsigned char __u_char;
typedef unsigned short __u_short;
typedef unsigned int __u_int;
typedef unsigned long __u_long;
typedef signed char __int8_t;
typedef unsigned char __uint8_t;
typedef short __int16_t;
typedef unsigned short __uint16_t;
typedef int __int32_t;
typedef unsigned int __uint32_t;
typedef long __int64_t;
typedef unsigned long __uint64_t;
typedef __int8_t __int_least8_t;
typedef __uint8_t __uint_least8_t;
typedef __int16_t __int_least16_t;
typedef __uint16_t __uint_least16_t;
typedef __int32_t __int_least32_t;
typedef __uint32_t __uint_least32_t;
typedef __int64_t __int_least64_t;
typedef __uint64_t __uint_least64_t;
typedef long __quad_t;
typedef unsigned long __u_quad_t;
typedef long __intmax_t;
typedef unsigned long __uintmax_t;
typedef unsigned long __dev_t;
typedef unsigned int __uid_t;
typedef unsigned int __gid_t;
typedef unsigned long __ino_t;
typedef unsigned long __ino64_t;
typedef unsigned int __mode_t;
typedef unsigned long __nlink_t;
typedef long __off_t;
typedef long __off64_t;
typedef int __pid_t;
typedef struct {
    int __val[2];
} __fsid_t;
typedef long __clock_t;
typedef unsigned long __rlim_t;
typedef unsigned long __rlim64_t;
typedef unsigned int __id_t;
typedef long __time_t;
typedef unsigned int __useconds_t;
typedef long __suseconds_t;
typedef int __daddr_t;
typedef int __key_t;
typedef int __clockid_t;
typedef void *__timer_t;
typedef long __blksize_t;
typedef long __blkcnt_t;
typedef long __blkcnt64_t;
typedef unsigned long __fsblkcnt_t;
typedef unsigned long __fsblkcnt64_t;
typedef unsigned long __fsfilcnt_t;
typedef unsigned long __fsfilcnt64_t;
typedef long __fsword_t;
typedef long __ssize_t;
typedef long __syscall_slong_t;
typedef unsigned long __syscall_ulong_t;
typedef __off64_t __loff_t;
typedef char *__caddr_t;
typedef long __intptr_t;
typedef unsigned int __socklen_t;
typedef int __sig_atomic_t;
typedef __int8_t int8_t;
typedef __int16_t int16_t;
typedef __int32_t int32_t;
typedef __int64_t int64_t;
typedef __uint8_t uint8_t;
typedef __uint16_t uint16_t;
typedef __uint32_t uint32_t;
typedef __uint64_t uint64_t;
typedef __int_least8_t int_least8_t;
typedef __int_least16_t int_least16_t;
typedef __int_least32_t int_least32_t;
typedef __int_least64_t int_least64_t;
typedef __uint_least8_t uint_least8_t;
typedef __uint_least16_t uint_least16_t;
typedef __uint_least32_t uint_least32_t;
typedef __uint_least64_t uint_least64_t;
typedef signed char int_fast8_t;
typedef long int_fast16_t;
typedef long int_fast32_t;
typedef long int_fast64_t;
typedef unsigned char uint_fast8_t;
typedef unsigned long uint_fast16_t;
typedef unsigned long uint_fast32_t;
typedef unsigned long uint_fast64_t;
typedef long intptr_t;
typedef unsigned long uintptr_t;
typedef __intmax_t intmax_t;
typedef __uintmax_t uintmax_t;
typedef _Bool bool;
extern uint64_t revng_init_local_sp(uint64_t param_0);
extern uint64_t init_rbx(void);
extern void indirect_placeholder(void);
extern uint64_t init_rbp(void);
extern uint64_t init_r14(void);
extern uint64_t init_r13(void);
extern uint64_t init_r12(void);
extern uint64_t init_r15(void);
extern uint64_t init_rax(void);
extern uint64_t indirect_placeholder_10(void);
extern void indirect_placeholder_3(uint64_t param_0, uint64_t param_1);
uint64_t bb_mgetgroups(uint64_t rdx, uint64_t rdi, uint64_t rsi) {
    uint64_t var_0;
    uint64_t var_1;
    uint64_t var_2;
    uint64_t var_3;
    uint64_t var_4;
    uint64_t var_5;
    uint64_t var_6;
    uint64_t var_7;
    uint64_t var_35;
    uint32_t _pre_phi;
    uint64_t rsi3_0_in;
    uint64_t var_40;
    uint64_t rax_1;
    uint32_t var_41;
    uint64_t var_42;
    uint64_t rbx_1;
    uint64_t rdx1_0;
    uint64_t rsi3_2;
    uint64_t rbx_0;
    uint64_t rsi3_1;
    uint32_t var_43;
    uint64_t var_44;
    uint64_t var_45;
    uint64_t var_46;
    uint64_t rax_0;
    uint32_t *var_38;
    uint32_t var_39;
    uint64_t var_29;
    uint32_t var_30;
    bool var_31;
    uint64_t var_32;
    uint64_t *var_33;
    uint64_t var_36;
    uint32_t var_37;
    uint64_t var_34;
    uint32_t var_25;
    uint64_t var_26;
    uint64_t var_24;
    uint32_t var_28;
    uint32_t var_27;
    uint64_t var_16;
    uint64_t var_17;
    uint64_t local_sp_3;
    uint64_t var_18;
    uint32_t *var_19;
    uint32_t var_20;
    uint32_t *var_14;
    uint32_t *_pre_phi106;
    uint32_t var_15;
    uint32_t var_9;
    bool var_10;
    bool var_11;
    uint32_t var_12;
    uint64_t var_13;
    uint64_t var_8;
    uint64_t var_21;
    uint32_t *var_22;
    uint32_t var_23;
    var_0 = revng_init_local_sp(0UL);
    var_1 = init_rax();
    var_2 = init_rbx();
    var_3 = init_r14();
    var_4 = init_rbp();
    var_5 = init_r13();
    var_6 = init_r12();
    var_7 = init_r15();
    *(uint64_t *)(var_0 + (-8L)) = var_7;
    *(uint64_t *)(var_0 + (-16L)) = var_3;
    *(uint64_t *)(var_0 + (-24L)) = var_5;
    *(uint64_t *)(var_0 + (-32L)) = var_6;
    *(uint64_t *)(var_0 + (-40L)) = var_4;
    *(uint64_t *)(var_0 + (-48L)) = var_2;
    *(uint64_t *)(var_0 + (-80L)) = rdx;
    rdx1_0 = 2305843009213693955UL;
    rbx_0 = 2305843009213693951UL;
    rax_1 = 4294967295UL;
    if (rdi != 0UL) {
        *(uint32_t *)(var_0 + (-60L)) = 10U;
        var_8 = var_0 + (-96L);
        *(uint64_t *)var_8 = 4224709UL;
        indirect_placeholder_3(0UL, 40UL);
        local_sp_3 = var_8;
        if (var_1 != 0UL) {
            var_9 = (uint32_t)var_1;
            var_10 = ((int)var_9 > (int)4294967295U);
            var_11 = ((int)var_9 < (int)0U);
            while (1U)
                {
                    var_12 = *(uint32_t *)(local_sp_3 + 28UL);
                    var_13 = local_sp_3 + (-8L);
                    *(uint64_t *)var_13 = 4224760UL;
                    indirect_placeholder();
                    if (var_10) {
                        _pre_phi106 = (uint32_t *)(local_sp_3 + 20UL);
                    } else {
                        var_14 = (uint32_t *)(local_sp_3 + 20UL);
                        _pre_phi106 = var_14;
                        if ((uint64_t)(var_12 - *var_14) == 0UL) {
                            *var_14 = (var_12 << 1U);
                        }
                    }
                    var_15 = *_pre_phi106;
                    if ((int)var_15 < (int)0U) {
                        var_16 = (uint64_t)var_15 << 2UL;
                        var_17 = local_sp_3 + (-16L);
                        *(uint64_t *)var_17 = 4224816UL;
                        indirect_placeholder_3(var_1, var_16);
                        local_sp_3 = var_17;
                        if (var_11) {
                            continue;
                        }
                        **(uint64_t **)var_13 = var_1;
                        var_18 = (uint64_t)*(uint32_t *)(local_sp_3 | 12UL);
                        rax_1 = var_18;
                        break;
                    }
                    *(uint64_t *)(local_sp_3 + (-16L)) = 4224796UL;
                    indirect_placeholder();
                    var_19 = (uint32_t *)var_1;
                    *var_19 = 12U;
                    *(uint64_t *)(local_sp_3 + (-24L)) = 4224826UL;
                    indirect_placeholder();
                    var_20 = *var_19;
                    *(uint64_t *)(local_sp_3 + (-32L)) = 4224836UL;
                    indirect_placeholder();
                    *(uint64_t *)(local_sp_3 + (-40L)) = 4224841UL;
                    indirect_placeholder();
                    *var_19 = var_20;
                    break;
                }
        }
        return rax_1;
    }
    *(uint64_t *)(var_0 + (-96L)) = 4224889UL;
    var_21 = indirect_placeholder_10();
    var_22 = (uint32_t *)(var_0 + (-68L));
    var_23 = (uint32_t)var_21;
    *var_22 = var_23;
    var_28 = var_23;
    if ((int)var_23 > (int)4294967295U) {
        if ((uint64_t)var_23 == 0UL) {
            var_27 = var_23 + 1U;
            *var_22 = var_27;
            var_28 = var_27;
        } else {
            if ((uint64_t)((uint32_t)rsi + 1U) == 0UL) {
                var_27 = var_23 + 1U;
                *var_22 = var_27;
                var_28 = var_27;
            }
        }
        if ((int)var_28 < (int)0U) {
            *(uint64_t *)(var_0 + (-104L)) = 4225011UL;
            indirect_placeholder();
            *(uint32_t *)2305843009213693951UL = 12U;
        } else {
            var_29 = (uint64_t)var_28 << 2UL;
            *(uint64_t *)(var_0 + (-104L)) = 4225041UL;
            indirect_placeholder_3(0UL, var_29);
            var_30 = (uint32_t)rsi;
            var_31 = ((uint64_t)(var_30 + 1U) == 0UL);
            var_32 = var_0 + (-112L);
            var_33 = (uint64_t *)var_32;
            if (var_31) {
                *var_33 = 4225265UL;
                var_36 = indirect_placeholder_10();
                var_37 = (uint32_t)var_36;
                _pre_phi = var_37;
                rsi3_0_in = var_36;
                rax_0 = var_36;
                if ((int)var_37 <= (int)4294967295U) {
                    *(uint64_t *)(var_32 + (-8L)) = 4225073UL;
                    indirect_placeholder();
                    var_38 = (uint32_t *)rax_0;
                    var_39 = *var_38;
                    *(uint64_t *)(var_32 + (-16L)) = 4225083UL;
                    indirect_placeholder();
                    *(uint64_t *)(var_32 + (-24L)) = 4225088UL;
                    indirect_placeholder();
                    *var_38 = var_39;
                    return rax_1;
                }
            }
            *var_33 = 4225228UL;
            var_34 = indirect_placeholder_10();
            rax_0 = var_34;
            if ((int)(uint32_t)var_34 <= (int)4294967295U) {
                *(uint64_t *)(var_32 + (-8L)) = 4225073UL;
                indirect_placeholder();
                var_38 = (uint32_t *)rax_0;
                var_39 = *var_38;
                *(uint64_t *)(var_32 + (-16L)) = 4225083UL;
                indirect_placeholder();
                *(uint64_t *)(var_32 + (-24L)) = 4225088UL;
                indirect_placeholder();
                *var_38 = var_39;
                return rax_1;
            }
            *(uint32_t *)2305843009213693951UL = var_30;
            var_35 = var_34 + 1UL;
            _pre_phi = (uint32_t)var_35;
            rsi3_0_in = var_35;
            var_40 = (uint64_t)_pre_phi;
            **(uint64_t **)(var_32 + 8UL) = 2305843009213693951UL;
            rsi3_1 = var_40;
            rax_1 = var_40;
            var_41 = *(uint32_t *)2305843009213693951UL;
            var_42 = (uint64_t)((long)(rsi3_0_in << 32UL) >> (long)30UL) + 2305843009213693951UL;
            if ((int)_pre_phi <= (int)1U & var_42 <= 2305843009213693955UL) {
                var_45 = rdx1_0 + 4UL;
                rdx1_0 = var_45;
                rbx_0 = rbx_1;
                rsi3_1 = rsi3_2;
                do {
                    var_43 = *(uint32_t *)rdx1_0;
                    rbx_1 = rbx_0;
                    rsi3_2 = rsi3_1;
                    if ((uint64_t)(var_43 - var_41) == 0UL) {
                        rsi3_2 = (uint64_t)((uint32_t)rsi3_1 + (-1));
                    } else {
                        if ((uint64_t)(var_43 - *(uint32_t *)rbx_0) == 0UL) {
                            rsi3_2 = (uint64_t)((uint32_t)rsi3_1 + (-1));
                        } else {
                            var_44 = rbx_0 + 4UL;
                            *(uint32_t *)var_44 = var_43;
                            rbx_1 = var_44;
                        }
                    }
                    var_45 = rdx1_0 + 4UL;
                    rdx1_0 = var_45;
                    rbx_0 = rbx_1;
                    rsi3_1 = rsi3_2;
                } while (var_42 <= var_45);
                var_46 = (uint64_t)(uint32_t)rsi3_2;
                rax_1 = var_46;
            }
        }
    } else {
        var_24 = var_0 + (-104L);
        *(uint64_t *)var_24 = 4224902UL;
        indirect_placeholder();
        if (*(uint32_t *)var_21 == 38U) {
            *(uint64_t *)(var_0 + (-112L)) = 4224934UL;
            indirect_placeholder_3(0UL, 4UL);
            **(uint64_t **)var_24 = 4294967295UL;
            var_25 = (uint32_t)rsi;
            *(uint32_t *)4294967295UL = var_25;
            var_26 = ((uint64_t)(var_25 + 1U) != 0UL);
            rax_1 = var_26;
        }
    }
}

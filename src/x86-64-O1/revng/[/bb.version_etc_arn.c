typedef unsigned char __u_char;
typedef unsigned short __u_short;
typedef unsigned int __u_int;
typedef unsigned long __u_long;
typedef signed char __int8_t;
typedef unsigned char __uint8_t;
typedef short __int16_t;
typedef unsigned short __uint16_t;
typedef int __int32_t;
typedef unsigned int __uint32_t;
typedef long __int64_t;
typedef unsigned long __uint64_t;
typedef __int8_t __int_least8_t;
typedef __uint8_t __uint_least8_t;
typedef __int16_t __int_least16_t;
typedef __uint16_t __uint_least16_t;
typedef __int32_t __int_least32_t;
typedef __uint32_t __uint_least32_t;
typedef __int64_t __int_least64_t;
typedef __uint64_t __uint_least64_t;
typedef long __quad_t;
typedef unsigned long __u_quad_t;
typedef long __intmax_t;
typedef unsigned long __uintmax_t;
typedef unsigned long __dev_t;
typedef unsigned int __uid_t;
typedef unsigned int __gid_t;
typedef unsigned long __ino_t;
typedef unsigned long __ino64_t;
typedef unsigned int __mode_t;
typedef unsigned long __nlink_t;
typedef long __off_t;
typedef long __off64_t;
typedef int __pid_t;
typedef struct {
    int __val[2];
} __fsid_t;
typedef long __clock_t;
typedef unsigned long __rlim_t;
typedef unsigned long __rlim64_t;
typedef unsigned int __id_t;
typedef long __time_t;
typedef unsigned int __useconds_t;
typedef long __suseconds_t;
typedef int __daddr_t;
typedef int __key_t;
typedef int __clockid_t;
typedef void *__timer_t;
typedef long __blksize_t;
typedef long __blkcnt_t;
typedef long __blkcnt64_t;
typedef unsigned long __fsblkcnt_t;
typedef unsigned long __fsblkcnt64_t;
typedef unsigned long __fsfilcnt_t;
typedef unsigned long __fsfilcnt64_t;
typedef long __fsword_t;
typedef long __ssize_t;
typedef long __syscall_slong_t;
typedef unsigned long __syscall_ulong_t;
typedef __off64_t __loff_t;
typedef char *__caddr_t;
typedef long __intptr_t;
typedef unsigned int __socklen_t;
typedef int __sig_atomic_t;
typedef __int8_t int8_t;
typedef __int16_t int16_t;
typedef __int32_t int32_t;
typedef __int64_t int64_t;
typedef __uint8_t uint8_t;
typedef __uint16_t uint16_t;
typedef __uint32_t uint32_t;
typedef __uint64_t uint64_t;
typedef __int_least8_t int_least8_t;
typedef __int_least16_t int_least16_t;
typedef __int_least32_t int_least32_t;
typedef __int_least64_t int_least64_t;
typedef __uint_least8_t uint_least8_t;
typedef __uint_least16_t uint_least16_t;
typedef __uint_least32_t uint_least32_t;
typedef __uint_least64_t uint_least64_t;
typedef signed char int_fast8_t;
typedef long int_fast16_t;
typedef long int_fast32_t;
typedef long int_fast64_t;
typedef unsigned char uint_fast8_t;
typedef unsigned long uint_fast16_t;
typedef unsigned long uint_fast32_t;
typedef unsigned long uint_fast64_t;
typedef long intptr_t;
typedef unsigned long uintptr_t;
typedef __intmax_t intmax_t;
typedef __uintmax_t uintmax_t;
typedef _Bool bool;
extern void abort(void);
extern uint64_t revng_init_local_sp(uint64_t param_0);
extern uint64_t init_rbx(void);
extern void indirect_placeholder(void);
extern uint64_t init_rbp(void);
extern uint64_t init_r12(void);
void bb_version_etc_arn(uint64_t rdx, uint64_t rdi, uint64_t rcx, uint64_t rsi, uint64_t r9, uint64_t r8) {
    uint64_t var_7;
    uint64_t var_0;
    uint64_t var_1;
    uint64_t var_2;
    uint64_t var_3;
    bool var_4;
    uint64_t var_5;
    uint64_t *var_6;
    uint64_t local_sp_0;
    uint64_t var_8;
    var_0 = revng_init_local_sp(0UL);
    var_1 = init_rbx();
    var_2 = init_rbp();
    var_3 = init_r12();
    *(uint64_t *)(var_0 + (-8L)) = var_3;
    *(uint64_t *)(var_0 + (-16L)) = var_2;
    *(uint64_t *)(var_0 + (-24L)) = var_1;
    var_4 = (rsi == 0UL);
    var_5 = var_0 + (-32L);
    var_6 = (uint64_t *)var_5;
    if (var_4) {
        *var_6 = 4212790UL;
        indirect_placeholder();
    } else {
        *var_6 = 4212773UL;
        indirect_placeholder();
    }
    *(uint64_t *)(var_5 + (-8L)) = 4212818UL;
    indirect_placeholder();
    var_7 = var_5 + (-16L);
    *(uint64_t *)var_7 = 4212831UL;
    indirect_placeholder();
    local_sp_0 = var_7;
    if (r9 > 9UL) {
        *(uint64_t *)(var_5 + (-32L)) = *(uint64_t *)(r8 + 64UL);
        *(uint64_t *)(var_5 + (-40L)) = *(uint64_t *)(r8 + 56UL);
        *(uint64_t *)(var_5 + (-48L)) = *(uint64_t *)(r8 + 48UL);
        *(uint64_t *)(var_5 + (-56L)) = *(uint64_t *)(r8 + 40UL);
        *(uint64_t *)(var_5 + (-64L)) = *(uint64_t *)(r8 + 32UL);
        *(uint64_t *)(var_5 + (-72L)) = 4213295UL;
        indirect_placeholder();
    } else {
        switch (*(uint64_t *)((r9 << 3UL) + 4257656UL)) {
          case 4212910UL:
            {
                *(uint64_t *)(var_5 + (-24L)) = 4212939UL;
                indirect_placeholder();
            }
            break;
          case 4213134UL:
            {
                *(uint64_t *)(var_5 + (-24L)) = *(uint64_t *)(r8 + 56UL);
                *(uint64_t *)(var_5 + (-32L)) = *(uint64_t *)(r8 + 48UL);
                *(uint64_t *)(var_5 + (-40L)) = *(uint64_t *)(r8 + 40UL);
                *(uint64_t *)(var_5 + (-48L)) = *(uint64_t *)(r8 + 32UL);
                *(uint64_t *)(var_5 + (-56L)) = 4213179UL;
                indirect_placeholder();
            }
            break;
          case 4212944UL:
            {
                *(uint64_t *)(var_5 + (-24L)) = 4212977UL;
                indirect_placeholder();
            }
            break;
          case 4213079UL:
            {
                *(uint64_t *)(var_5 + (-32L)) = *(uint64_t *)(r8 + 48UL);
                *(uint64_t *)(var_5 + (-40L)) = *(uint64_t *)(r8 + 40UL);
                *(uint64_t *)(var_5 + (-48L)) = *(uint64_t *)(r8 + 32UL);
                *(uint64_t *)(var_5 + (-56L)) = 4213125UL;
                indirect_placeholder();
            }
            break;
          case 4212982UL:
            {
                *(uint64_t *)(var_5 + (-32L)) = *(uint64_t *)(r8 + 32UL);
                *(uint64_t *)(var_5 + (-40L)) = 4213022UL;
                indirect_placeholder();
            }
            break;
          case 4213031UL:
            {
                *(uint64_t *)(var_5 + (-24L)) = *(uint64_t *)(r8 + 40UL);
                *(uint64_t *)(var_5 + (-32L)) = *(uint64_t *)(r8 + 32UL);
                *(uint64_t *)(var_5 + (-40L)) = 4213070UL;
                indirect_placeholder();
            }
            break;
          case 4213185UL:
            {
                *(uint64_t *)(var_5 + (-32L)) = *(uint64_t *)(r8 + 64UL);
                *(uint64_t *)(var_5 + (-40L)) = *(uint64_t *)(r8 + 56UL);
                *(uint64_t *)(var_5 + (-48L)) = *(uint64_t *)(r8 + 48UL);
                *(uint64_t *)(var_5 + (-56L)) = *(uint64_t *)(r8 + 40UL);
                *(uint64_t *)(var_5 + (-64L)) = *(uint64_t *)(r8 + 32UL);
                *(uint64_t *)(var_5 + (-72L)) = 4213237UL;
                indirect_placeholder();
            }
            break;
          case 4212880UL:
            {
                *(uint64_t *)(var_5 + (-24L)) = 4212905UL;
                indirect_placeholder();
            }
            break;
          case 4212854UL:
          case 4212849UL:
            {
                switch (*(uint64_t *)((r9 << 3UL) + 4257656UL)) {
                  case 4212849UL:
                    {
                        var_8 = var_5 + (-24L);
                        *(uint64_t *)var_8 = 4212854UL;
                        indirect_placeholder();
                        local_sp_0 = var_8;
                    }
                    break;
                  case 4212854UL:
                    {
                        *(uint64_t *)(local_sp_0 + (-8L)) = 4212875UL;
                        indirect_placeholder();
                    }
                    break;
                }
            }
            break;
          default:
            {
                abort();
            }
            break;
        }
    }
}

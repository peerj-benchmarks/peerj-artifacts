typedef unsigned char __u_char;
typedef unsigned short __u_short;
typedef unsigned int __u_int;
typedef unsigned long __u_long;
typedef signed char __int8_t;
typedef unsigned char __uint8_t;
typedef short __int16_t;
typedef unsigned short __uint16_t;
typedef int __int32_t;
typedef unsigned int __uint32_t;
typedef long __int64_t;
typedef unsigned long __uint64_t;
typedef __int8_t __int_least8_t;
typedef __uint8_t __uint_least8_t;
typedef __int16_t __int_least16_t;
typedef __uint16_t __uint_least16_t;
typedef __int32_t __int_least32_t;
typedef __uint32_t __uint_least32_t;
typedef __int64_t __int_least64_t;
typedef __uint64_t __uint_least64_t;
typedef long __quad_t;
typedef unsigned long __u_quad_t;
typedef long __intmax_t;
typedef unsigned long __uintmax_t;
typedef unsigned long __dev_t;
typedef unsigned int __uid_t;
typedef unsigned int __gid_t;
typedef unsigned long __ino_t;
typedef unsigned long __ino64_t;
typedef unsigned int __mode_t;
typedef unsigned long __nlink_t;
typedef long __off_t;
typedef long __off64_t;
typedef int __pid_t;
typedef struct {
    int __val[2];
} __fsid_t;
typedef long __clock_t;
typedef unsigned long __rlim_t;
typedef unsigned long __rlim64_t;
typedef unsigned int __id_t;
typedef long __time_t;
typedef unsigned int __useconds_t;
typedef long __suseconds_t;
typedef int __daddr_t;
typedef int __key_t;
typedef int __clockid_t;
typedef void *__timer_t;
typedef long __blksize_t;
typedef long __blkcnt_t;
typedef long __blkcnt64_t;
typedef unsigned long __fsblkcnt_t;
typedef unsigned long __fsblkcnt64_t;
typedef unsigned long __fsfilcnt_t;
typedef unsigned long __fsfilcnt64_t;
typedef long __fsword_t;
typedef long __ssize_t;
typedef long __syscall_slong_t;
typedef unsigned long __syscall_ulong_t;
typedef __off64_t __loff_t;
typedef char *__caddr_t;
typedef long __intptr_t;
typedef unsigned int __socklen_t;
typedef int __sig_atomic_t;
typedef __int8_t int8_t;
typedef __int16_t int16_t;
typedef __int32_t int32_t;
typedef __int64_t int64_t;
typedef __uint8_t uint8_t;
typedef __uint16_t uint16_t;
typedef __uint32_t uint32_t;
typedef __uint64_t uint64_t;
typedef __int_least8_t int_least8_t;
typedef __int_least16_t int_least16_t;
typedef __int_least32_t int_least32_t;
typedef __int_least64_t int_least64_t;
typedef __uint_least8_t uint_least8_t;
typedef __uint_least16_t uint_least16_t;
typedef __uint_least32_t uint_least32_t;
typedef __uint_least64_t uint_least64_t;
typedef signed char int_fast8_t;
typedef long int_fast16_t;
typedef long int_fast32_t;
typedef long int_fast64_t;
typedef unsigned char uint_fast8_t;
typedef unsigned long uint_fast16_t;
typedef unsigned long uint_fast32_t;
typedef unsigned long uint_fast64_t;
typedef long intptr_t;
typedef unsigned long uintptr_t;
typedef __intmax_t intmax_t;
typedef __uintmax_t uintmax_t;
typedef _Bool bool;
struct indirect_placeholder_25_ret_type;
struct indirect_placeholder_24_ret_type;
struct indirect_placeholder_26_ret_type;
struct indirect_placeholder_25_ret_type {
    uint64_t field_0;
    uint64_t field_1;
};
struct indirect_placeholder_24_ret_type {
    uint64_t field_0;
    uint64_t field_1;
};
struct indirect_placeholder_26_ret_type {
    uint64_t field_0;
    uint64_t field_1;
};
extern void abort(void);
extern uint64_t revng_init_local_sp(uint64_t param_0);
extern uint64_t init_rbx(void);
extern void indirect_placeholder(void);
extern uint64_t helper_cc_compute_all_wrapper(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint32_t param_3);
extern uint64_t indirect_placeholder_6(void);
extern struct indirect_placeholder_25_ret_type indirect_placeholder_25(uint64_t param_0);
extern struct indirect_placeholder_24_ret_type indirect_placeholder_24(uint64_t param_0);
extern struct indirect_placeholder_26_ret_type indirect_placeholder_26(uint64_t param_0);
uint64_t bb_posixtest(uint64_t rdi) {
    uint64_t rbx_0;
    uint64_t var_0;
    uint64_t var_1;
    uint64_t var_2;
    uint32_t var_3;
    uint64_t var_17;
    uint64_t var_18;
    uint64_t var_11;
    uint64_t var_12;
    uint64_t local_sp_0;
    uint64_t var_19;
    uint64_t var_20;
    uint64_t var_9;
    uint64_t var_10;
    uint64_t var_4;
    uint64_t var_5;
    uint64_t var_6;
    uint64_t var_7;
    bool var_13;
    uint64_t *var_14;
    uint64_t var_15;
    uint64_t var_16;
    uint64_t var_8;
    uint64_t var_21;
    uint64_t var_22;
    uint64_t var_23;
    uint64_t var_24;
    var_0 = revng_init_local_sp(0UL);
    var_1 = init_rbx();
    var_2 = var_0 + (-8L);
    *(uint64_t *)var_2 = var_1;
    var_3 = (uint32_t)rdi;
    local_sp_0 = var_2;
    if ((uint64_t)(var_3 + (-2)) == 0UL) {
        *(uint64_t *)(var_0 + (-16L)) = 4204628UL;
        var_23 = indirect_placeholder_6();
        var_24 = (uint64_t)(uint32_t)var_23;
        rbx_0 = var_24;
    } else {
        if ((int)var_3 > (int)2U) {
            if ((uint64_t)(var_3 + (-3)) != 0UL) {
                *(uint64_t *)(var_0 + (-16L)) = 4204640UL;
                var_21 = indirect_placeholder_6();
                var_22 = (uint64_t)(uint32_t)var_21;
                rbx_0 = var_22;
                return (uint64_t)(uint32_t)rbx_0;
            }
            if ((uint64_t)(var_3 + (-4)) == 0UL) {
                var_8 = *(uint64_t *)6362768UL;
                *(uint64_t *)(var_0 + (-16L)) = 4204675UL;
                indirect_placeholder();
                if ((uint64_t)(uint32_t)var_8 != 0UL) {
                    *(uint64_t *)(var_0 + (-24L)) = 4204689UL;
                    indirect_placeholder_26(1UL);
                    *(uint64_t *)(var_0 + (-32L)) = 4204694UL;
                    var_19 = indirect_placeholder_6();
                    var_20 = (uint64_t)(uint32_t)var_19 ^ 1UL;
                    rbx_0 = var_20;
                    return (uint64_t)(uint32_t)rbx_0;
                }
                var_9 = *(uint64_t *)6362768UL;
                var_10 = var_0 + (-24L);
                *(uint64_t *)var_10 = 4204732UL;
                indirect_placeholder();
                local_sp_0 = var_10;
                var_11 = *(uint64_t *)6362768UL;
                var_12 = var_0 + (-32L);
                *(uint64_t *)var_12 = 4204765UL;
                indirect_placeholder();
                local_sp_0 = var_12;
                if ((uint64_t)(uint32_t)var_9 != 0UL & (uint64_t)(uint32_t)var_11 != 0UL) {
                    *(uint64_t *)(var_0 + (-40L)) = 4204779UL;
                    indirect_placeholder_25(0UL);
                    *(uint64_t *)(var_0 + (-48L)) = 4204784UL;
                    var_17 = indirect_placeholder_6();
                    var_18 = (uint64_t)(uint32_t)var_17;
                    *(uint64_t *)(var_0 + (-56L)) = 4204796UL;
                    indirect_placeholder_24(0UL);
                    rbx_0 = var_18;
                    return (uint64_t)(uint32_t)rbx_0;
                }
            }
            var_6 = helper_cc_compute_all_wrapper(rdi, 0UL, 0UL, 24U);
            if ((uint64_t)(((unsigned char)(var_6 >> 4UL) ^ (unsigned char)var_6) & '\xc0') == 0UL) {
                var_7 = var_0 + (-16L);
                *(uint64_t *)var_7 = 4204807UL;
                indirect_placeholder();
                local_sp_0 = var_7;
            }
        } else {
            if ((uint64_t)(var_3 + (-1)) == 0UL) {
                *(uint64_t *)(var_0 + (-16L)) = 4204616UL;
                var_4 = indirect_placeholder_6();
                var_5 = (uint64_t)(uint32_t)var_4;
                rbx_0 = var_5;
                return (uint64_t)(uint32_t)rbx_0;
            }
            var_6 = helper_cc_compute_all_wrapper(rdi, 0UL, 0UL, 24U);
            if ((uint64_t)(((unsigned char)(var_6 >> 4UL) ^ (unsigned char)var_6) & '\xc0') == 0UL) {
                var_7 = var_0 + (-16L);
                *(uint64_t *)var_7 = 4204807UL;
                indirect_placeholder();
                local_sp_0 = var_7;
            }
            var_13 = ((int)*(uint32_t *)6362776UL > (int)*(uint32_t *)6362780UL);
            var_14 = (uint64_t *)(local_sp_0 + (-8L));
            if (!var_13) {
                *var_14 = 4204826UL;
                indirect_placeholder();
                abort();
            }
            *var_14 = 4204831UL;
            var_15 = indirect_placeholder_6();
            var_16 = (uint64_t)(uint32_t)var_15;
            rbx_0 = var_16;
        }
    }
}

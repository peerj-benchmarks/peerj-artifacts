typedef unsigned char __u_char;
typedef unsigned short __u_short;
typedef unsigned int __u_int;
typedef unsigned long __u_long;
typedef signed char __int8_t;
typedef unsigned char __uint8_t;
typedef short __int16_t;
typedef unsigned short __uint16_t;
typedef int __int32_t;
typedef unsigned int __uint32_t;
typedef long __int64_t;
typedef unsigned long __uint64_t;
typedef __int8_t __int_least8_t;
typedef __uint8_t __uint_least8_t;
typedef __int16_t __int_least16_t;
typedef __uint16_t __uint_least16_t;
typedef __int32_t __int_least32_t;
typedef __uint32_t __uint_least32_t;
typedef __int64_t __int_least64_t;
typedef __uint64_t __uint_least64_t;
typedef long __quad_t;
typedef unsigned long __u_quad_t;
typedef long __intmax_t;
typedef unsigned long __uintmax_t;
typedef unsigned long __dev_t;
typedef unsigned int __uid_t;
typedef unsigned int __gid_t;
typedef unsigned long __ino_t;
typedef unsigned long __ino64_t;
typedef unsigned int __mode_t;
typedef unsigned long __nlink_t;
typedef long __off_t;
typedef long __off64_t;
typedef int __pid_t;
typedef struct {
    int __val[2];
} __fsid_t;
typedef long __clock_t;
typedef unsigned long __rlim_t;
typedef unsigned long __rlim64_t;
typedef unsigned int __id_t;
typedef long __time_t;
typedef unsigned int __useconds_t;
typedef long __suseconds_t;
typedef int __daddr_t;
typedef int __key_t;
typedef int __clockid_t;
typedef void *__timer_t;
typedef long __blksize_t;
typedef long __blkcnt_t;
typedef long __blkcnt64_t;
typedef unsigned long __fsblkcnt_t;
typedef unsigned long __fsblkcnt64_t;
typedef unsigned long __fsfilcnt_t;
typedef unsigned long __fsfilcnt64_t;
typedef long __fsword_t;
typedef long __ssize_t;
typedef long __syscall_slong_t;
typedef unsigned long __syscall_ulong_t;
typedef __off64_t __loff_t;
typedef char *__caddr_t;
typedef long __intptr_t;
typedef unsigned int __socklen_t;
typedef int __sig_atomic_t;
typedef __int8_t int8_t;
typedef __int16_t int16_t;
typedef __int32_t int32_t;
typedef __int64_t int64_t;
typedef __uint8_t uint8_t;
typedef __uint16_t uint16_t;
typedef __uint32_t uint32_t;
typedef __uint64_t uint64_t;
typedef __int_least8_t int_least8_t;
typedef __int_least16_t int_least16_t;
typedef __int_least32_t int_least32_t;
typedef __int_least64_t int_least64_t;
typedef __uint_least8_t uint_least8_t;
typedef __uint_least16_t uint_least16_t;
typedef __uint_least32_t uint_least32_t;
typedef __uint_least64_t uint_least64_t;
typedef signed char int_fast8_t;
typedef long int_fast16_t;
typedef long int_fast32_t;
typedef long int_fast64_t;
typedef unsigned char uint_fast8_t;
typedef unsigned long uint_fast16_t;
typedef unsigned long uint_fast32_t;
typedef unsigned long uint_fast64_t;
typedef long intptr_t;
typedef unsigned long uintptr_t;
typedef __intmax_t intmax_t;
typedef __uintmax_t uintmax_t;
extern uint64_t revng_init_local_sp(uint64_t param_0);
extern uint64_t init_cc_src2(void);
extern uint64_t helper_cc_compute_c_wrapper(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint32_t param_3);
uint64_t bb_strintcmp(uint64_t rdi, uint64_t rsi) {
    uint64_t var_0;
    unsigned char var_1;
    uint64_t var_2;
    unsigned char var_3;
    uint64_t var_4;
    uint64_t rdi1_0;
    uint64_t var_26;
    unsigned char var_27;
    uint64_t var_28;
    uint64_t rsi2_1;
    uint64_t rdi1_2;
    uint64_t merge;
    uint64_t rdx_0;
    uint64_t rsi2_0;
    uint64_t var_29;
    uint64_t var_30;
    unsigned char var_31;
    uint64_t rcx_0;
    uint64_t rdi1_1;
    uint64_t rsi2_2;
    uint64_t var_32;
    unsigned char var_33;
    uint64_t var_34;
    uint64_t var_35;
    unsigned char var_36;
    uint64_t rax_0;
    uint64_t rsi2_3;
    uint32_t var_37;
    uint32_t var_38;
    uint64_t var_39;
    uint64_t rdx_2;
    uint64_t rdx_1;
    uint64_t rdi1_3;
    uint64_t var_40;
    uint64_t var_41;
    uint64_t rsi2_5;
    uint64_t rcx_1;
    uint64_t rsi2_4;
    uint64_t var_42;
    uint64_t var_43;
    uint64_t var_44;
    uint64_t var_23;
    unsigned char var_24;
    uint64_t rdi1_4;
    uint64_t rcx_2;
    uint64_t rdi1_5;
    uint64_t var_25;
    uint64_t rcx_3;
    unsigned char var_5;
    uint64_t rdx_3;
    uint64_t rsi2_6;
    unsigned char var_6;
    uint64_t rdx_4;
    uint64_t rdi1_7;
    uint64_t rdi1_6;
    uint64_t rsi2_7;
    uint64_t var_7;
    unsigned char var_8;
    uint64_t var_9;
    uint64_t var_10;
    unsigned char var_11;
    uint64_t rcx_4;
    uint64_t rsi2_8;
    uint64_t var_12;
    uint32_t var_13;
    uint32_t var_14;
    uint64_t var_15;
    uint64_t r8_1;
    uint64_t rdi1_8;
    uint64_t r8_0;
    uint64_t var_16;
    uint64_t var_17;
    uint64_t rdx_5;
    uint64_t rsi2_9;
    uint64_t var_18;
    uint64_t var_19;
    uint64_t var_20;
    uint64_t var_21;
    uint64_t var_22;
    revng_init_local_sp(0UL);
    var_0 = init_cc_src2();
    var_1 = *(unsigned char *)rdi;
    var_2 = (uint64_t)var_1;
    var_3 = *(unsigned char *)rsi;
    var_4 = (uint64_t)var_3;
    rdi1_0 = rdi;
    rsi2_1 = rsi;
    merge = 4294967295UL;
    rdx_0 = var_4;
    rsi2_0 = rsi;
    rdx_2 = 0UL;
    rdx_1 = 0UL;
    rsi2_5 = rsi;
    rcx_1 = 0UL;
    rdi1_4 = rdi;
    rcx_2 = var_2;
    rdi1_5 = rdi;
    rcx_3 = var_2;
    rdx_3 = var_4;
    rsi2_6 = rsi;
    r8_1 = 0UL;
    r8_0 = 0UL;
    rdx_5 = 0UL;
    if ((uint64_t)(var_1 + '\xd3') != 0UL) {
        var_26 = rdi1_0 + 1UL;
        var_27 = *(unsigned char *)var_26;
        rdi1_0 = var_26;
        rdi1_1 = var_26;
        rdi1_2 = var_26;
        do {
            var_26 = rdi1_0 + 1UL;
            var_27 = *(unsigned char *)var_26;
            rdi1_0 = var_26;
            rdi1_1 = var_26;
            rdi1_2 = var_26;
        } while (var_27 != '0');
        var_28 = (uint64_t)var_27;
        rax_0 = var_28;
        if ((uint64_t)(var_3 + '\xd3') == 0UL) {
            if ((uint64_t)(((uint32_t)var_28 + (-48)) & (-2)) > 9UL) {
                return merge;
            }
            while ((uint64_t)((unsigned char)rdx_0 + '\xd0') != 0UL)
                {
                    var_29 = rsi2_0 + 1UL;
                    rdx_0 = (uint64_t)*(unsigned char *)var_29;
                    rsi2_0 = var_29;
                }
            return ((uint64_t)(((uint32_t)rdx_0 + (-48)) & (-2)) < 10UL) ? 4294967295UL : 0UL;
        }
        var_30 = rsi2_1 + 1UL;
        var_31 = *(unsigned char *)var_30;
        rsi2_1 = var_30;
        rsi2_2 = var_30;
        rsi2_3 = var_30;
        do {
            var_30 = rsi2_1 + 1UL;
            var_31 = *(unsigned char *)var_30;
            rsi2_1 = var_30;
            rsi2_2 = var_30;
            rsi2_3 = var_30;
        } while (var_31 != '0');
        rcx_0 = (uint64_t)var_31;
        rcx_0 = var_28;
        if ((uint64_t)(var_27 - var_31) != 0UL & (uint64_t)(((uint32_t)var_28 + (-48)) & (-2)) > 9UL) {
            var_32 = rdi1_1 + 1UL;
            var_33 = *(unsigned char *)var_32;
            var_34 = (uint64_t)var_33;
            var_35 = rsi2_2 + 1UL;
            var_36 = *(unsigned char *)var_35;
            rdi1_2 = var_32;
            rcx_0 = (uint64_t)var_36;
            rdi1_1 = var_32;
            rsi2_2 = var_35;
            rax_0 = var_34;
            rsi2_3 = var_35;
            while ((uint64_t)(var_33 - var_36) != 0UL)
                {
                    rcx_0 = var_34;
                    if ((uint64_t)(((uint32_t)var_34 + (-48)) & (-2)) > 9UL) {
                        break;
                    }
                    var_32 = rdi1_1 + 1UL;
                    var_33 = *(unsigned char *)var_32;
                    var_34 = (uint64_t)var_33;
                    var_35 = rsi2_2 + 1UL;
                    var_36 = *(unsigned char *)var_35;
                    rdi1_2 = var_32;
                    rcx_0 = (uint64_t)var_36;
                    rdi1_1 = var_32;
                    rsi2_2 = var_35;
                    rax_0 = var_34;
                    rsi2_3 = var_35;
                }
        }
        var_37 = (uint32_t)rcx_0;
        var_38 = (uint32_t)rax_0;
        var_39 = (uint64_t)(var_37 - var_38);
        rdi1_3 = rdi1_2;
        rsi2_4 = rsi2_3;
        if ((uint64_t)((var_38 + (-48)) & (-2)) > 9UL) {
            var_40 = rdi1_3 + 1UL;
            var_41 = rdx_1 + 1UL;
            rdx_1 = var_41;
            rdi1_3 = var_40;
            rdx_2 = var_41;
            do {
                var_40 = rdi1_3 + 1UL;
                var_41 = rdx_1 + 1UL;
                rdx_1 = var_41;
                rdi1_3 = var_40;
                rdx_2 = var_41;
            } while ((uint64_t)(((uint32_t)(uint64_t)*(unsigned char *)var_40 + (-48)) & (-2)) <= 9UL);
        }
        if ((uint64_t)((var_37 + (-48)) & (-2)) > 9UL) {
            return (rdx_2 == 0UL) ? 0UL : 4294967295UL;
        }
        var_42 = rsi2_4 + 1UL;
        var_43 = rcx_1 + 1UL;
        rcx_1 = var_43;
        rsi2_4 = var_42;
        do {
            var_42 = rsi2_4 + 1UL;
            var_43 = rcx_1 + 1UL;
            rcx_1 = var_43;
            rsi2_4 = var_42;
        } while ((uint64_t)(((uint32_t)(uint64_t)*(unsigned char *)var_42 + (-48)) & (-2)) <= 9UL);
        if (rdx_2 == var_43) {
            return (rdx_2 == 0UL) ? 0UL : var_39;
        }
        var_44 = helper_cc_compute_c_wrapper(rdx_2 - var_43, var_43, var_0, 17U);
        return (uint64_t)((uint32_t)((0UL - var_44) & 2UL) + (-1));
    }
    merge = 1UL;
    if ((uint64_t)(var_3 + '\xd3') == 0UL) {
        var_23 = rsi2_5 + 1UL;
        var_24 = *(unsigned char *)var_23;
        rsi2_5 = var_23;
        do {
            var_23 = rsi2_5 + 1UL;
            var_24 = *(unsigned char *)var_23;
            rsi2_5 = var_23;
        } while (var_24 != '0');
        if ((uint64_t)(((uint32_t)(uint64_t)var_24 + (-48)) & (-2)) <= 9UL) {
            while ((uint64_t)((unsigned char)rcx_2 + '\xd0') != 0UL)
                {
                    var_25 = rdi1_4 + 1UL;
                    rdi1_4 = var_25;
                    rcx_2 = (uint64_t)*(unsigned char *)var_25;
                }
            return ((uint64_t)(((uint32_t)rcx_2 + (-48)) & (-2)) < 10UL);
        }
    }
    var_5 = (unsigned char)rcx_3;
    rdi1_6 = rdi1_5;
    rdi1_7 = rdi1_5;
    rcx_4 = rcx_3;
    while ((uint64_t)(var_5 + '\xd0') != 0UL)
        {
            var_22 = rdi1_5 + 1UL;
            rdi1_5 = var_22;
            rcx_3 = (uint64_t)*(unsigned char *)var_22;
            var_5 = (unsigned char)rcx_3;
            rdi1_6 = rdi1_5;
            rdi1_7 = rdi1_5;
            rcx_4 = rcx_3;
        }
    var_6 = (unsigned char)rdx_3;
    rsi2_7 = rsi2_6;
    rdx_4 = rdx_3;
    rsi2_8 = rsi2_6;
    while ((uint64_t)(var_6 + '\xd0') != 0UL)
        {
            var_21 = rsi2_6 + 1UL;
            rdx_3 = (uint64_t)*(unsigned char *)var_21;
            rsi2_6 = var_21;
            var_6 = (unsigned char)rdx_3;
            rsi2_7 = rsi2_6;
            rdx_4 = rdx_3;
            rsi2_8 = rsi2_6;
        }
    rdx_4 = rcx_3;
    if ((uint64_t)(var_5 - var_6) != 0UL & (uint64_t)(((uint32_t)rcx_3 + (-48)) & (-2)) > 9UL) {
        var_7 = rdi1_6 + 1UL;
        var_8 = *(unsigned char *)var_7;
        var_9 = (uint64_t)var_8;
        var_10 = rsi2_7 + 1UL;
        var_11 = *(unsigned char *)var_10;
        rdx_4 = (uint64_t)var_11;
        rdi1_7 = var_7;
        rdi1_6 = var_7;
        rsi2_7 = var_10;
        rcx_4 = var_9;
        rsi2_8 = var_10;
        while ((uint64_t)(var_8 - var_11) != 0UL)
            {
                rdx_4 = var_9;
                if ((uint64_t)(((uint32_t)var_9 + (-48)) & (-2)) > 9UL) {
                    break;
                }
                var_7 = rdi1_6 + 1UL;
                var_8 = *(unsigned char *)var_7;
                var_9 = (uint64_t)var_8;
                var_10 = rsi2_7 + 1UL;
                var_11 = *(unsigned char *)var_10;
                rdx_4 = (uint64_t)var_11;
                rdi1_7 = var_7;
                rdi1_6 = var_7;
                rsi2_7 = var_10;
                rcx_4 = var_9;
                rsi2_8 = var_10;
            }
    }
    var_12 = (uint64_t)(unsigned char)rdx_4;
    var_13 = (uint32_t)(uint64_t)(unsigned char)rcx_4;
    var_14 = (uint32_t)var_12;
    var_15 = (uint64_t)(var_13 - var_14);
    rdi1_8 = rdi1_7;
    rsi2_9 = rsi2_8;
    if ((uint64_t)((var_13 + (-48)) & (-2)) > 9UL) {
        var_16 = rdi1_8 + 1UL;
        var_17 = r8_0 + 1UL;
        rdi1_8 = var_16;
        r8_0 = var_17;
        r8_1 = var_17;
        do {
            var_16 = rdi1_8 + 1UL;
            var_17 = r8_0 + 1UL;
            rdi1_8 = var_16;
            r8_0 = var_17;
            r8_1 = var_17;
        } while ((uint64_t)(((uint32_t)(uint64_t)*(unsigned char *)var_16 + (-48)) & (-2)) <= 9UL);
    }
    if ((uint64_t)((var_14 + (-48)) & (-2)) > 9UL) {
        return (r8_1 != 0UL);
    }
    var_18 = rsi2_9 + 1UL;
    var_19 = rdx_5 + 1UL;
    rdx_5 = var_19;
    rsi2_9 = var_18;
    do {
        var_18 = rsi2_9 + 1UL;
        var_19 = rdx_5 + 1UL;
        rdx_5 = var_19;
        rsi2_9 = var_18;
    } while ((uint64_t)(((uint32_t)(uint64_t)*(unsigned char *)var_18 + (-48)) & (-2)) <= 9UL);
    if (r8_1 == var_19) {
        return (r8_1 == 0UL) ? 0UL : var_15;
    }
    var_20 = helper_cc_compute_c_wrapper(r8_1 - var_19, var_19, var_0, 17U);
    return (uint64_t)((0U - (uint32_t)var_20) & (-2)) | 1UL;
    var_20 = helper_cc_compute_c_wrapper(r8_1 - var_19, var_19, var_0, 17U);
    return (uint64_t)((0U - (uint32_t)var_20) & (-2)) | 1UL;
}

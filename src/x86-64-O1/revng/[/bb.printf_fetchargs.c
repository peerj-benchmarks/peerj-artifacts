typedef unsigned char __u_char;
typedef unsigned short __u_short;
typedef unsigned int __u_int;
typedef unsigned long __u_long;
typedef signed char __int8_t;
typedef unsigned char __uint8_t;
typedef short __int16_t;
typedef unsigned short __uint16_t;
typedef int __int32_t;
typedef unsigned int __uint32_t;
typedef long __int64_t;
typedef unsigned long __uint64_t;
typedef __int8_t __int_least8_t;
typedef __uint8_t __uint_least8_t;
typedef __int16_t __int_least16_t;
typedef __uint16_t __uint_least16_t;
typedef __int32_t __int_least32_t;
typedef __uint32_t __uint_least32_t;
typedef __int64_t __int_least64_t;
typedef __uint64_t __uint_least64_t;
typedef long __quad_t;
typedef unsigned long __u_quad_t;
typedef long __intmax_t;
typedef unsigned long __uintmax_t;
typedef unsigned long __dev_t;
typedef unsigned int __uid_t;
typedef unsigned int __gid_t;
typedef unsigned long __ino_t;
typedef unsigned long __ino64_t;
typedef unsigned int __mode_t;
typedef unsigned long __nlink_t;
typedef long __off_t;
typedef long __off64_t;
typedef int __pid_t;
typedef struct {
    int __val[2];
} __fsid_t;
typedef long __clock_t;
typedef unsigned long __rlim_t;
typedef unsigned long __rlim64_t;
typedef unsigned int __id_t;
typedef long __time_t;
typedef unsigned int __useconds_t;
typedef long __suseconds_t;
typedef int __daddr_t;
typedef int __key_t;
typedef int __clockid_t;
typedef void *__timer_t;
typedef long __blksize_t;
typedef long __blkcnt_t;
typedef long __blkcnt64_t;
typedef unsigned long __fsblkcnt_t;
typedef unsigned long __fsblkcnt64_t;
typedef unsigned long __fsfilcnt_t;
typedef unsigned long __fsfilcnt64_t;
typedef long __fsword_t;
typedef long __ssize_t;
typedef long __syscall_slong_t;
typedef unsigned long __syscall_ulong_t;
typedef __off64_t __loff_t;
typedef char *__caddr_t;
typedef long __intptr_t;
typedef unsigned int __socklen_t;
typedef int __sig_atomic_t;
typedef __int8_t int8_t;
typedef __int16_t int16_t;
typedef __int32_t int32_t;
typedef __int64_t int64_t;
typedef __uint8_t uint8_t;
typedef __uint16_t uint16_t;
typedef __uint32_t uint32_t;
typedef __uint64_t uint64_t;
typedef __int_least8_t int_least8_t;
typedef __int_least16_t int_least16_t;
typedef __int_least32_t int_least32_t;
typedef __int_least64_t int_least64_t;
typedef __uint_least8_t uint_least8_t;
typedef __uint_least16_t uint_least16_t;
typedef __uint_least32_t uint_least32_t;
typedef __uint_least64_t uint_least64_t;
typedef signed char int_fast8_t;
typedef long int_fast16_t;
typedef long int_fast32_t;
typedef long int_fast64_t;
typedef unsigned char uint_fast8_t;
typedef unsigned long uint_fast16_t;
typedef unsigned long uint_fast32_t;
typedef unsigned long uint_fast64_t;
typedef long intptr_t;
typedef unsigned long uintptr_t;
typedef __intmax_t intmax_t;
typedef __uintmax_t uintmax_t;
struct type_3;
struct helper_fpop_wrapper_ret_type;
struct helper_fldt_ST0_wrapper_ret_type;
struct type_3 {
};
struct helper_fpop_wrapper_ret_type {
    uint32_t field_0;
    unsigned char field_1;
    unsigned char field_2;
    unsigned char field_3;
    unsigned char field_4;
    unsigned char field_5;
    unsigned char field_6;
    unsigned char field_7;
    unsigned char field_8;
};
struct helper_fldt_ST0_wrapper_ret_type {
    uint32_t field_0;
    uint64_t field_1;
    uint64_t field_2;
    uint64_t field_3;
    uint64_t field_4;
    uint64_t field_5;
    uint64_t field_6;
    uint64_t field_7;
    uint64_t field_8;
    uint16_t field_9;
    uint16_t field_10;
    uint16_t field_11;
    uint16_t field_12;
    uint16_t field_13;
    uint16_t field_14;
    uint16_t field_15;
    uint16_t field_16;
    unsigned char field_17;
    unsigned char field_18;
    unsigned char field_19;
    unsigned char field_20;
    unsigned char field_21;
    unsigned char field_22;
    unsigned char field_23;
    unsigned char field_24;
};
extern void abort(void);
extern uint64_t revng_init_local_sp(uint64_t param_0);
extern uint32_t init_state_0x8480(void);
extern void helper_fstt_ST0_wrapper(struct type_3 *param_0, uint64_t param_1, uint32_t param_2, uint64_t param_3, uint64_t param_4, uint64_t param_5, uint64_t param_6, uint64_t param_7, uint64_t param_8, uint64_t param_9, uint64_t param_10, uint16_t param_11, uint16_t param_12, uint16_t param_13, uint16_t param_14, uint16_t param_15, uint16_t param_16, uint16_t param_17, uint16_t param_18);
extern struct helper_fpop_wrapper_ret_type helper_fpop_wrapper(struct type_3 *param_0, uint32_t param_1);
extern uint64_t init_cc_src2(void);
extern uint64_t helper_cc_compute_c_wrapper(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint32_t param_3);
extern struct helper_fldt_ST0_wrapper_ret_type helper_fldt_ST0_wrapper(struct type_3 *param_0, uint64_t param_1, uint32_t param_2);
typedef _Bool bool;
uint64_t bb_printf_fetchargs(uint64_t rdi, uint64_t rsi) {
    uint32_t var_84;
    uint64_t var_85;
    uint64_t var_17;
    uint32_t var_23;
    uint64_t var_24;
    uint64_t var_0;
    uint64_t *_cast;
    uint64_t merge;
    uint64_t var_1;
    uint32_t var_2;
    uint32_t *var_3;
    uint64_t *var_4;
    uint64_t *var_5;
    uint32_t *var_6;
    uint64_t rdx_0;
    uint64_t rax_0;
    uint32_t state_0x8480_0;
    uint32_t var_7;
    uint32_t var_8;
    uint64_t var_9;
    uint64_t var_10;
    uint64_t var_11;
    uint64_t r10_0;
    uint64_t var_12;
    uint32_t state_0x8480_1;
    uint32_t var_13;
    uint64_t var_14;
    uint64_t var_15;
    uint64_t var_16;
    uint64_t r10_1;
    uint32_t var_44;
    uint64_t var_45;
    uint64_t var_46;
    uint64_t var_47;
    uint32_t var_18;
    uint64_t var_19;
    uint64_t var_20;
    uint64_t var_21;
    uint64_t r10_2;
    uint64_t var_22;
    uint64_t var_25;
    uint64_t var_26;
    uint64_t r10_3;
    uint64_t var_27;
    uint32_t var_28;
    uint64_t var_29;
    uint64_t var_30;
    uint64_t var_31;
    uint64_t r10_4;
    uint64_t var_32;
    uint32_t var_33;
    uint64_t var_34;
    uint64_t var_35;
    uint64_t var_36;
    uint64_t r10_5;
    uint64_t var_37;
    uint32_t var_38;
    uint64_t var_39;
    uint64_t var_40;
    uint64_t var_41;
    uint64_t r10_6;
    uint64_t var_42;
    uint64_t var_43;
    uint32_t var_104;
    uint64_t var_105;
    uint64_t var_106;
    uint64_t var_107;
    uint64_t r10_18;
    uint64_t var_108;
    uint64_t r10_7;
    uint64_t var_48;
    uint64_t var_49;
    uint32_t var_50;
    uint64_t var_51;
    uint64_t var_52;
    uint64_t var_53;
    uint64_t r10_8;
    uint64_t var_54;
    uint32_t var_55;
    uint64_t var_56;
    uint64_t var_57;
    uint64_t var_58;
    uint64_t r10_9;
    uint64_t var_59;
    uint64_t var_60;
    struct helper_fldt_ST0_wrapper_ret_type var_61;
    uint32_t var_62;
    struct helper_fpop_wrapper_ret_type var_63;
    uint32_t var_64;
    uint64_t var_65;
    uint64_t var_66;
    uint64_t var_67;
    uint64_t r10_10;
    uint64_t var_68;
    uint32_t var_69;
    uint64_t var_70;
    uint64_t var_71;
    uint64_t var_72;
    uint64_t r10_11;
    uint64_t var_73;
    uint32_t var_74;
    uint64_t var_75;
    uint64_t var_76;
    uint64_t var_77;
    uint64_t r10_12;
    uint64_t var_78;
    uint32_t var_79;
    uint64_t var_80;
    uint64_t var_81;
    uint64_t var_82;
    uint64_t r10_13;
    uint64_t var_83;
    uint64_t var_86;
    uint64_t var_87;
    uint64_t r10_14;
    uint64_t var_88;
    uint32_t var_89;
    uint64_t var_90;
    uint64_t var_91;
    uint64_t var_92;
    uint64_t r10_15;
    uint64_t var_93;
    uint32_t var_94;
    uint64_t var_95;
    uint64_t var_96;
    uint64_t var_97;
    uint64_t r10_16;
    uint64_t var_98;
    uint32_t var_99;
    uint64_t var_100;
    uint64_t var_101;
    uint64_t var_102;
    uint64_t r10_17;
    uint64_t var_103;
    uint32_t var_109;
    uint64_t var_110;
    uint64_t var_111;
    uint64_t var_112;
    uint64_t r10_19;
    uint64_t var_113;
    uint32_t var_114;
    uint64_t var_115;
    uint64_t var_116;
    uint64_t var_117;
    uint64_t r10_20;
    uint64_t var_118;
    uint64_t var_119;
    bool switch_state_var;
    revng_init_local_sp(0UL);
    var_0 = init_cc_src2();
    _cast = (uint64_t *)rsi;
    rdx_0 = 0UL;
    merge = 0UL;
    if (*_cast == 0UL) {
        return merge;
    }
    var_1 = *(uint64_t *)(rsi + 8UL);
    var_2 = init_state_0x8480();
    var_3 = (uint32_t *)rdi;
    var_4 = (uint64_t *)(rdi + 16UL);
    var_5 = (uint64_t *)(rdi + 8UL);
    var_6 = (uint32_t *)(rdi + 4UL);
    rax_0 = var_1;
    state_0x8480_0 = var_2;
    merge = 4294967295UL;
    var_7 = *(uint32_t *)rax_0;
    state_0x8480_1 = state_0x8480_0;
    while (var_7 <= 22U)
        {
            switch_state_var = 0;
            switch (*(uint64_t *)(((uint64_t)var_7 << 3UL) + 4259424UL)) {
              case 4246943UL:
                {
                    switch_state_var = 1;
                    break;
                }
                break;
              case 4246874UL:
              case 4246832UL:
              case 4246790UL:
              case 4246748UL:
              case 4246703UL:
              case 4246658UL:
              case 4246606UL:
              case 4246554UL:
              case 4246510UL:
              case 4246466UL:
              case 4246436UL:
              case 4246383UL:
              case 4246338UL:
              case 4246293UL:
              case 4246248UL:
              case 4246203UL:
              case 4246159UL:
              case 4246115UL:
              case 4246070UL:
              case 4246025UL:
              case 4245981UL:
              case 4245937UL:
                {
                    switch (*(uint64_t *)(((uint64_t)var_7 << 3UL) + 4259424UL)) {
                      case 4246436UL:
                        {
                            var_60 = (*var_5 + 15UL) & (-16L);
                            *var_5 = (var_60 + 16UL);
                            var_61 = helper_fldt_ST0_wrapper((struct type_3 *)(0UL), var_60, state_0x8480_0);
                            var_62 = var_61.field_0;
                            helper_fstt_ST0_wrapper((struct type_3 *)(0UL), rax_0 + 16UL, var_62, var_61.field_1, var_61.field_2, var_61.field_3, var_61.field_4, var_61.field_5, var_61.field_6, var_61.field_7, var_61.field_8, var_61.field_9, var_61.field_10, var_61.field_11, var_61.field_12, var_61.field_13, var_61.field_14, var_61.field_15, var_61.field_16);
                            var_63 = helper_fpop_wrapper((struct type_3 *)(0UL), var_62);
                            state_0x8480_1 = var_63.field_0;
                        }
                        break;
                      case 4246338UL:
                        {
                            var_69 = *var_3;
                            var_70 = (uint64_t)var_69;
                            var_71 = helper_cc_compute_c_wrapper(var_70 + (-48L), 48UL, var_0, 16U);
                            if (var_71 == 0UL) {
                                var_73 = *var_5;
                                *var_5 = (var_73 + 8UL);
                                r10_11 = var_73;
                            } else {
                                var_72 = *var_4 + var_70;
                                *var_3 = (var_69 + 8U);
                                r10_11 = var_72;
                            }
                            *(uint64_t *)(rax_0 + 16UL) = *(uint64_t *)r10_11;
                        }
                        break;
                      case 4246025UL:
                        {
                            var_104 = *var_3;
                            var_105 = (uint64_t)var_104;
                            var_106 = helper_cc_compute_c_wrapper(var_105 + (-48L), 48UL, var_0, 16U);
                            if (var_106 == 0UL) {
                                var_108 = *var_5;
                                *var_5 = (var_108 + 8UL);
                                r10_18 = var_108;
                            } else {
                                var_107 = *var_4 + var_105;
                                *var_3 = (var_104 + 8U);
                                r10_18 = var_107;
                            }
                            *(uint16_t *)(rax_0 + 16UL) = (uint16_t)*(uint32_t *)r10_18;
                        }
                        break;
                      case 4246554UL:
                        {
                            var_44 = *var_3;
                            var_45 = (uint64_t)var_44;
                            var_46 = helper_cc_compute_c_wrapper(var_45 + (-48L), 48UL, var_0, 16U);
                            if (var_46 == 0UL) {
                                var_48 = *var_5;
                                *var_5 = (var_48 + 8UL);
                                r10_7 = var_48;
                            } else {
                                var_47 = *var_4 + var_45;
                                *var_3 = (var_44 + 8U);
                                r10_7 = var_47;
                            }
                            var_49 = *(uint64_t *)r10_7;
                            *(uint64_t *)(rax_0 + 16UL) = ((var_49 == 0UL) ? 4259408UL : var_49);
                        }
                        break;
                      case 4246293UL:
                        {
                            var_74 = *var_3;
                            var_75 = (uint64_t)var_74;
                            var_76 = helper_cc_compute_c_wrapper(var_75 + (-48L), 48UL, var_0, 16U);
                            if (var_76 == 0UL) {
                                var_78 = *var_5;
                                *var_5 = (var_78 + 8UL);
                                r10_12 = var_78;
                            } else {
                                var_77 = *var_4 + var_75;
                                *var_3 = (var_74 + 8U);
                                r10_12 = var_77;
                            }
                            *(uint64_t *)(rax_0 + 16UL) = *(uint64_t *)r10_12;
                        }
                        break;
                      case 4246658UL:
                        {
                            var_33 = *var_3;
                            var_34 = (uint64_t)var_33;
                            var_35 = helper_cc_compute_c_wrapper(var_34 + (-48L), 48UL, var_0, 16U);
                            if (var_35 == 0UL) {
                                var_37 = *var_5;
                                *var_5 = (var_37 + 8UL);
                                r10_5 = var_37;
                            } else {
                                var_36 = *var_4 + var_34;
                                *var_3 = (var_33 + 8U);
                                r10_5 = var_36;
                            }
                            *(uint64_t *)(rax_0 + 16UL) = *(uint64_t *)r10_5;
                        }
                        break;
                      case 4246159UL:
                        {
                            var_89 = *var_3;
                            var_90 = (uint64_t)var_89;
                            var_91 = helper_cc_compute_c_wrapper(var_90 + (-48L), 48UL, var_0, 16U);
                            if (var_91 == 0UL) {
                                var_93 = *var_5;
                                *var_5 = (var_93 + 8UL);
                                r10_15 = var_93;
                            } else {
                                var_92 = *var_4 + var_90;
                                *var_3 = (var_89 + 8U);
                                r10_15 = var_92;
                            }
                            *(uint32_t *)(rax_0 + 16UL) = *(uint32_t *)r10_15;
                        }
                        break;
                      case 4246606UL:
                        {
                            var_38 = *var_3;
                            var_39 = (uint64_t)var_38;
                            var_40 = helper_cc_compute_c_wrapper(var_39 + (-48L), 48UL, var_0, 16U);
                            if (var_40 == 0UL) {
                                var_42 = *var_5;
                                *var_5 = (var_42 + 8UL);
                                r10_6 = var_42;
                            } else {
                                var_41 = *var_4 + var_39;
                                *var_3 = (var_38 + 8U);
                                r10_6 = var_41;
                            }
                            var_43 = *(uint64_t *)r10_6;
                            *(uint64_t *)(rax_0 + 16UL) = ((var_43 == 0UL) ? 4259616UL : var_43);
                        }
                        break;
                      case 4246748UL:
                        {
                            var_23 = *var_3;
                            var_24 = (uint64_t)var_23;
                            var_25 = helper_cc_compute_c_wrapper(var_24 + (-48L), 48UL, var_0, 16U);
                            if (var_25 == 0UL) {
                                var_27 = *var_5;
                                *var_5 = (var_27 + 8UL);
                                r10_3 = var_27;
                            } else {
                                var_26 = *var_4 + var_24;
                                *var_3 = (var_23 + 8U);
                                r10_3 = var_26;
                            }
                            *(uint64_t *)(rax_0 + 16UL) = *(uint64_t *)r10_3;
                        }
                        break;
                      case 4246070UL:
                        {
                            var_99 = *var_3;
                            var_100 = (uint64_t)var_99;
                            var_101 = helper_cc_compute_c_wrapper(var_100 + (-48L), 48UL, var_0, 16U);
                            if (var_101 == 0UL) {
                                var_103 = *var_5;
                                *var_5 = (var_103 + 8UL);
                                r10_17 = var_103;
                            } else {
                                var_102 = *var_4 + var_100;
                                *var_3 = (var_99 + 8U);
                                r10_17 = var_102;
                            }
                            *(uint16_t *)(rax_0 + 16UL) = (uint16_t)*(uint32_t *)r10_17;
                        }
                        break;
                      case 4246383UL:
                        {
                            var_64 = *var_6;
                            var_65 = (uint64_t)var_64;
                            var_66 = helper_cc_compute_c_wrapper(var_65 + (-176L), 176UL, var_0, 16U);
                            if (var_66 == 0UL) {
                                var_68 = *var_5;
                                *var_5 = (var_68 + 8UL);
                                r10_10 = var_68;
                            } else {
                                var_67 = *var_4 + var_65;
                                *var_6 = (var_64 + 16U);
                                r10_10 = var_67;
                            }
                            *(uint64_t *)(rax_0 + 16UL) = *(uint64_t *)r10_10;
                        }
                        break;
                      case 4246790UL:
                        {
                            var_18 = *var_3;
                            var_19 = (uint64_t)var_18;
                            var_20 = helper_cc_compute_c_wrapper(var_19 + (-48L), 48UL, var_0, 16U);
                            if (var_20 == 0UL) {
                                var_22 = *var_5;
                                *var_5 = (var_22 + 8UL);
                                r10_2 = var_22;
                            } else {
                                var_21 = *var_4 + var_19;
                                *var_3 = (var_18 + 8U);
                                r10_2 = var_21;
                            }
                            *(uint64_t *)(rax_0 + 16UL) = *(uint64_t *)r10_2;
                        }
                        break;
                      case 4245937UL:
                        {
                            var_114 = *var_3;
                            var_115 = (uint64_t)var_114;
                            var_116 = helper_cc_compute_c_wrapper(var_115 + (-48L), 48UL, var_0, 16U);
                            if (var_116 == 0UL) {
                                var_118 = *var_5;
                                *var_5 = (var_118 + 8UL);
                                r10_20 = var_118;
                            } else {
                                var_117 = *var_4 + var_115;
                                *var_3 = (var_114 + 8U);
                                r10_20 = var_117;
                            }
                            *(unsigned char *)(rax_0 + 16UL) = (unsigned char)*(uint32_t *)r10_20;
                        }
                        break;
                      case 4246466UL:
                        {
                            var_55 = *var_3;
                            var_56 = (uint64_t)var_55;
                            var_57 = helper_cc_compute_c_wrapper(var_56 + (-48L), 48UL, var_0, 16U);
                            if (var_57 == 0UL) {
                                var_59 = *var_5;
                                *var_5 = (var_59 + 8UL);
                                r10_9 = var_59;
                            } else {
                                var_58 = *var_4 + var_56;
                                *var_3 = (var_55 + 8U);
                                r10_9 = var_58;
                            }
                            *(uint32_t *)(rax_0 + 16UL) = *(uint32_t *)r10_9;
                        }
                        break;
                      case 4245981UL:
                        {
                            var_109 = *var_3;
                            var_110 = (uint64_t)var_109;
                            var_111 = helper_cc_compute_c_wrapper(var_110 + (-48L), 48UL, var_0, 16U);
                            if (var_111 == 0UL) {
                                var_113 = *var_5;
                                *var_5 = (var_113 + 8UL);
                                r10_19 = var_113;
                            } else {
                                var_112 = *var_4 + var_110;
                                *var_3 = (var_109 + 8U);
                                r10_19 = var_112;
                            }
                            *(unsigned char *)(rax_0 + 16UL) = (unsigned char)*(uint32_t *)r10_19;
                        }
                        break;
                      case 4246832UL:
                        {
                            var_13 = *var_3;
                            var_14 = (uint64_t)var_13;
                            var_15 = helper_cc_compute_c_wrapper(var_14 + (-48L), 48UL, var_0, 16U);
                            if (var_15 == 0UL) {
                                var_17 = *var_5;
                                *var_5 = (var_17 + 8UL);
                                r10_1 = var_17;
                            } else {
                                var_16 = *var_4 + var_14;
                                *var_3 = (var_13 + 8U);
                                r10_1 = var_16;
                            }
                            *(uint64_t *)(rax_0 + 16UL) = *(uint64_t *)r10_1;
                        }
                        break;
                      case 4246248UL:
                        {
                            var_79 = *var_3;
                            var_80 = (uint64_t)var_79;
                            var_81 = helper_cc_compute_c_wrapper(var_80 + (-48L), 48UL, var_0, 16U);
                            if (var_81 == 0UL) {
                                var_83 = *var_5;
                                *var_5 = (var_83 + 8UL);
                                r10_13 = var_83;
                            } else {
                                var_82 = *var_4 + var_80;
                                *var_3 = (var_79 + 8U);
                                r10_13 = var_82;
                            }
                            *(uint64_t *)(rax_0 + 16UL) = *(uint64_t *)r10_13;
                        }
                        break;
                      case 4246203UL:
                        {
                            var_84 = *var_3;
                            var_85 = (uint64_t)var_84;
                            var_86 = helper_cc_compute_c_wrapper(var_85 + (-48L), 48UL, var_0, 16U);
                            if (var_86 == 0UL) {
                                var_88 = *var_5;
                                *var_5 = (var_88 + 8UL);
                                r10_14 = var_88;
                            } else {
                                var_87 = *var_4 + var_85;
                                *var_3 = (var_84 + 8U);
                                r10_14 = var_87;
                            }
                            *(uint64_t *)(rax_0 + 16UL) = *(uint64_t *)r10_14;
                        }
                        break;
                      case 4246874UL:
                        {
                            var_8 = *var_3;
                            var_9 = (uint64_t)var_8;
                            var_10 = helper_cc_compute_c_wrapper(var_9 + (-48L), 48UL, var_0, 16U);
                            if (var_10 == 0UL) {
                                var_12 = *var_5;
                                *var_5 = (var_12 + 8UL);
                                r10_0 = var_12;
                            } else {
                                var_11 = *var_4 + var_9;
                                *var_3 = (var_8 + 8U);
                                r10_0 = var_11;
                            }
                            *(uint64_t *)(rax_0 + 16UL) = *(uint64_t *)r10_0;
                        }
                        break;
                      case 4246703UL:
                        {
                            var_28 = *var_3;
                            var_29 = (uint64_t)var_28;
                            var_30 = helper_cc_compute_c_wrapper(var_29 + (-48L), 48UL, var_0, 16U);
                            if (var_30 == 0UL) {
                                var_32 = *var_5;
                                *var_5 = (var_32 + 8UL);
                                r10_4 = var_32;
                            } else {
                                var_31 = *var_4 + var_29;
                                *var_3 = (var_28 + 8U);
                                r10_4 = var_31;
                            }
                            *(uint64_t *)(rax_0 + 16UL) = *(uint64_t *)r10_4;
                        }
                        break;
                      case 4246510UL:
                        {
                            var_50 = *var_3;
                            var_51 = (uint64_t)var_50;
                            var_52 = helper_cc_compute_c_wrapper(var_51 + (-48L), 48UL, var_0, 16U);
                            if (var_52 == 0UL) {
                                var_54 = *var_5;
                                *var_5 = (var_54 + 8UL);
                                r10_8 = var_54;
                            } else {
                                var_53 = *var_4 + var_51;
                                *var_3 = (var_50 + 8U);
                                r10_8 = var_53;
                            }
                            *(uint32_t *)(rax_0 + 16UL) = *(uint32_t *)r10_8;
                        }
                        break;
                      case 4246115UL:
                        {
                            var_94 = *var_3;
                            var_95 = (uint64_t)var_94;
                            var_96 = helper_cc_compute_c_wrapper(var_95 + (-48L), 48UL, var_0, 16U);
                            if (var_96 == 0UL) {
                                var_98 = *var_5;
                                *var_5 = (var_98 + 8UL);
                                r10_16 = var_98;
                            } else {
                                var_97 = *var_4 + var_95;
                                *var_3 = (var_94 + 8U);
                                r10_16 = var_97;
                            }
                            *(uint32_t *)(rax_0 + 16UL) = *(uint32_t *)r10_16;
                        }
                        break;
                    }
                    var_119 = rdx_0 + 1UL;
                    rdx_0 = var_119;
                    state_0x8480_0 = state_0x8480_1;
                    merge = 0UL;
                    if (*_cast > var_119) {
                        switch_state_var = 1;
                        break;
                    }
                    rax_0 = rax_0 + 32UL;
                    var_7 = *(uint32_t *)rax_0;
                    state_0x8480_1 = state_0x8480_0;
                    continue;
                }
                break;
              default:
                {
                    abort();
                }
                break;
            }
            if (switch_state_var)
                break;
        }
    return merge;
}

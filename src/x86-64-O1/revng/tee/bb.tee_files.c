typedef unsigned char __u_char;
typedef unsigned short __u_short;
typedef unsigned int __u_int;
typedef unsigned long __u_long;
typedef signed char __int8_t;
typedef unsigned char __uint8_t;
typedef short __int16_t;
typedef unsigned short __uint16_t;
typedef int __int32_t;
typedef unsigned int __uint32_t;
typedef long __int64_t;
typedef unsigned long __uint64_t;
typedef __int8_t __int_least8_t;
typedef __uint8_t __uint_least8_t;
typedef __int16_t __int_least16_t;
typedef __uint16_t __uint_least16_t;
typedef __int32_t __int_least32_t;
typedef __uint32_t __uint_least32_t;
typedef __int64_t __int_least64_t;
typedef __uint64_t __uint_least64_t;
typedef long __quad_t;
typedef unsigned long __u_quad_t;
typedef long __intmax_t;
typedef unsigned long __uintmax_t;
typedef unsigned long __dev_t;
typedef unsigned int __uid_t;
typedef unsigned int __gid_t;
typedef unsigned long __ino_t;
typedef unsigned long __ino64_t;
typedef unsigned int __mode_t;
typedef unsigned long __nlink_t;
typedef long __off_t;
typedef long __off64_t;
typedef int __pid_t;
typedef struct {
    int __val[2];
} __fsid_t;
typedef long __clock_t;
typedef unsigned long __rlim_t;
typedef unsigned long __rlim64_t;
typedef unsigned int __id_t;
typedef long __time_t;
typedef unsigned int __useconds_t;
typedef long __suseconds_t;
typedef int __daddr_t;
typedef int __key_t;
typedef int __clockid_t;
typedef void *__timer_t;
typedef long __blksize_t;
typedef long __blkcnt_t;
typedef long __blkcnt64_t;
typedef unsigned long __fsblkcnt_t;
typedef unsigned long __fsblkcnt64_t;
typedef unsigned long __fsfilcnt_t;
typedef unsigned long __fsfilcnt64_t;
typedef long __fsword_t;
typedef long __ssize_t;
typedef long __syscall_slong_t;
typedef unsigned long __syscall_ulong_t;
typedef __off64_t __loff_t;
typedef char *__caddr_t;
typedef long __intptr_t;
typedef unsigned int __socklen_t;
typedef int __sig_atomic_t;
typedef __int8_t int8_t;
typedef __int16_t int16_t;
typedef __int32_t int32_t;
typedef __int64_t int64_t;
typedef __uint8_t uint8_t;
typedef __uint16_t uint16_t;
typedef __uint32_t uint32_t;
typedef __uint64_t uint64_t;
typedef __int_least8_t int_least8_t;
typedef __int_least16_t int_least16_t;
typedef __int_least32_t int_least32_t;
typedef __int_least64_t int_least64_t;
typedef __uint_least8_t uint_least8_t;
typedef __uint_least16_t uint_least16_t;
typedef __uint_least32_t uint_least32_t;
typedef __uint_least64_t uint_least64_t;
typedef signed char int_fast8_t;
typedef long int_fast16_t;
typedef long int_fast32_t;
typedef long int_fast64_t;
typedef unsigned char uint_fast8_t;
typedef unsigned long uint_fast16_t;
typedef unsigned long uint_fast32_t;
typedef unsigned long uint_fast64_t;
typedef long intptr_t;
typedef unsigned long uintptr_t;
typedef __intmax_t intmax_t;
typedef __uintmax_t uintmax_t;
struct indirect_placeholder_12_ret_type;
struct indirect_placeholder_13_ret_type;
struct indirect_placeholder_14_ret_type;
struct indirect_placeholder_12_ret_type {
    uint64_t field_0;
    uint64_t field_1;
    uint64_t field_2;
};
struct indirect_placeholder_13_ret_type {
    uint64_t field_0;
    uint64_t field_1;
    uint64_t field_2;
};
struct indirect_placeholder_14_ret_type {
    uint64_t field_0;
    uint64_t field_1;
    uint64_t field_2;
};
extern void abort(void);
extern uint64_t revng_init_local_sp(uint64_t param_0);
extern uint64_t init_rbx(void);
extern void indirect_placeholder(void);
extern uint64_t init_rbp(void);
extern uint64_t init_r14(void);
extern uint64_t init_r13(void);
extern uint64_t init_r12(void);
extern uint64_t indirect_placeholder_1(uint64_t param_0);
extern uint64_t helper_cc_compute_all_wrapper(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint32_t param_3);
extern uint64_t init_r15(void);
extern uint64_t init_r8(void);
extern void indirect_placeholder_10(uint64_t param_0, uint64_t param_1);
extern uint64_t indirect_placeholder_3(uint64_t param_0, uint64_t param_1);
extern uint64_t init_r9(void);
extern void indirect_placeholder_8(uint64_t param_0, uint64_t param_1, uint64_t param_2);
extern void indirect_placeholder_7(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint64_t param_3, uint64_t param_4, uint64_t param_5, uint64_t param_6);
extern struct indirect_placeholder_12_ret_type indirect_placeholder_12(uint64_t param_0, uint64_t param_1, uint64_t param_2);
extern struct indirect_placeholder_13_ret_type indirect_placeholder_13(uint64_t param_0, uint64_t param_1, uint64_t param_2);
extern struct indirect_placeholder_14_ret_type indirect_placeholder_14(uint64_t param_0, uint64_t param_1, uint64_t param_2);
uint64_t bb_tee_files(uint64_t rdi, uint64_t rsi) {
    uint64_t var_0;
    uint64_t var_1;
    uint64_t var_2;
    uint64_t var_3;
    uint64_t var_4;
    uint64_t var_5;
    uint64_t var_6;
    uint64_t var_7;
    uint64_t var_8;
    uint64_t var_9;
    uint64_t var_10;
    uint64_t *var_11;
    uint64_t var_12;
    uint64_t rbx_1;
    uint64_t var_69;
    struct indirect_placeholder_12_ret_type var_70;
    uint64_t var_71;
    uint64_t var_72;
    uint64_t var_73;
    uint64_t var_74;
    uint64_t var_75;
    uint64_t local_sp_11;
    uint32_t var_47;
    uint64_t var_48;
    uint64_t rax_2;
    uint64_t local_sp_0;
    uint64_t rcx_1;
    uint64_t rax_0;
    uint64_t local_sp_2;
    uint64_t local_sp_1;
    uint64_t rax_1;
    uint64_t rcx_0;
    uint64_t r9_8;
    uint64_t r9_0;
    uint64_t r8_0;
    uint64_t *var_58;
    uint64_t local_sp_4;
    uint64_t var_49;
    uint64_t r9_3;
    uint64_t r8_3;
    uint64_t var_50;
    struct indirect_placeholder_13_ret_type var_51;
    uint64_t var_52;
    uint64_t var_53;
    uint64_t var_54;
    uint64_t var_55;
    uint64_t var_56;
    uint64_t var_57;
    uint64_t var_43;
    uint64_t rbx_0;
    uint32_t var_44;
    uint64_t var_45;
    uint64_t var_46;
    uint64_t rax_7;
    uint64_t var_38;
    uint64_t rcx_6;
    uint64_t local_sp_5;
    uint64_t local_sp_6;
    uint64_t var_39;
    uint64_t local_sp_7;
    uint64_t var_16;
    uint64_t rax_4;
    uint64_t rax_5;
    uint64_t local_sp_3;
    uint64_t rax_3;
    uint64_t r8_2;
    uint64_t r8_8;
    uint64_t r9_1;
    uint64_t r8_1;
    uint64_t var_40;
    uint64_t *_cast;
    uint64_t var_41;
    uint64_t var_42;
    uint64_t r9_2;
    uint32_t var_59;
    uint64_t rcx_2;
    uint64_t var_37;
    uint64_t local_sp_15;
    uint64_t rax_6;
    uint64_t rcx_3;
    uint64_t r9_4;
    uint64_t r8_4;
    uint64_t var_60;
    uint64_t var_61;
    uint32_t *var_62;
    uint32_t *_pre_phi150;
    uint64_t local_sp_12;
    uint64_t r14_1;
    uint64_t local_sp_8;
    uint64_t rbp_0;
    uint64_t r14_0;
    uint64_t rcx_4;
    uint64_t r8_6;
    uint64_t r9_5;
    uint64_t r8_5;
    uint64_t r9_6;
    uint32_t var_35;
    uint64_t local_sp_9;
    uint64_t local_sp_14;
    uint32_t *_pre149;
    uint32_t var_63;
    uint64_t var_64;
    uint64_t local_sp_13;
    uint64_t local_sp_10;
    uint64_t var_65;
    uint64_t var_66;
    uint64_t var_67;
    uint64_t var_68;
    uint32_t var_76;
    uint64_t var_27;
    struct indirect_placeholder_14_ret_type var_28;
    uint64_t var_29;
    uint64_t var_30;
    uint64_t var_31;
    uint64_t var_32;
    uint64_t var_33;
    uint64_t var_34;
    uint64_t var_26;
    uint64_t var_20;
    uint64_t var_21;
    uint64_t var_22;
    uint64_t *var_23;
    uint64_t var_24;
    uint64_t var_25;
    uint64_t rcx_5;
    uint64_t r9_7;
    uint64_t r8_7;
    uint32_t var_36;
    uint64_t var_13;
    uint64_t *var_14;
    uint64_t var_15;
    uint64_t var_17;
    uint32_t var_18;
    uint64_t var_19;
    unsigned int loop_state_var;
    var_0 = revng_init_local_sp(0UL);
    var_1 = init_rbx();
    var_2 = init_r14();
    var_3 = init_rbp();
    var_4 = init_r13();
    var_5 = init_r12();
    var_6 = init_r15();
    var_7 = init_r9();
    var_8 = init_r8();
    *(uint64_t *)(var_0 + (-8L)) = var_6;
    *(uint64_t *)(var_0 + (-16L)) = var_2;
    *(uint64_t *)(var_0 + (-24L)) = var_4;
    *(uint64_t *)(var_0 + (-32L)) = var_5;
    *(uint64_t *)(var_0 + (-40L)) = var_3;
    *(uint64_t *)(var_0 + (-48L)) = var_1;
    *(uint32_t *)(var_0 + (-1084L)) = (uint32_t)rdi;
    var_9 = (*(unsigned char *)6358869UL == '\x00') ? 4248642UL : 4248640UL;
    var_10 = *(uint64_t *)6358728UL;
    var_11 = (uint64_t *)(var_0 + (-1136L));
    *var_11 = 4201536UL;
    indirect_placeholder_8(rsi, var_10, 2UL);
    var_12 = (rdi << 32UL) + 4294967296UL;
    rbx_1 = 1UL;
    rcx_1 = 0UL;
    rax_1 = 0UL;
    rbx_0 = 0UL;
    r14_1 = 1UL;
    rbp_0 = 1UL;
    rcx_4 = 0UL;
    r8_6 = var_8;
    r9_6 = var_7;
    rcx_5 = 0UL;
    r9_7 = var_7;
    r8_7 = var_8;
    if ((uint64_t)((long)var_12 >> (long)32UL) > 1152921504606846975UL) {
        *(uint64_t *)(var_0 + (-1144L)) = 4201564UL;
        indirect_placeholder_10(var_7, var_8);
        abort();
    }
    var_13 = (uint64_t)((long)var_12 >> (long)29UL);
    var_14 = (uint64_t *)(var_0 + (-1144L));
    *var_14 = 4201577UL;
    var_15 = indirect_placeholder_1(var_13);
    var_16 = rsi + (-8L);
    *(uint64_t *)(var_0 + (-1112L)) = var_16;
    *(uint64_t *)var_15 = *(uint64_t *)6358720UL;
    *(uint64_t *)var_16 = 4248644UL;
    var_17 = var_0 + (-1152L);
    *(uint64_t *)var_17 = 4201628UL;
    indirect_placeholder();
    var_18 = *(uint32_t *)(var_0 + (-1108L));
    var_19 = helper_cc_compute_all_wrapper((uint64_t)var_18, 0UL, 0UL, 24U);
    local_sp_13 = var_17;
    local_sp_14 = var_17;
    if ((uint64_t)(((unsigned char)(var_19 >> 4UL) ^ (unsigned char)var_19) & '\xc0') == 0UL) {
        *(unsigned char *)(var_0 + (-1109L)) = (unsigned char)'\x01';
        *var_14 = var_9;
        while (1U)
            {
                var_20 = (uint64_t)((long)(rbp_0 << 32UL) >> (long)29UL);
                var_21 = var_20 + var_15;
                var_22 = *(uint64_t *)(local_sp_13 + 8UL);
                var_23 = (uint64_t *)(var_20 + *(uint64_t *)(local_sp_13 + 32UL));
                var_24 = *var_23;
                *(uint64_t *)(local_sp_13 + (-8L)) = 4201697UL;
                var_25 = indirect_placeholder_3(var_24, var_22);
                *(uint64_t *)var_21 = var_25;
                r14_0 = r14_1;
                r9_5 = r9_6;
                r8_5 = r8_6;
                if (var_25 == 0UL) {
                    var_27 = *var_23;
                    *(uint64_t *)(local_sp_13 + (-16L)) = 4201724UL;
                    var_28 = indirect_placeholder_14(var_27, 0UL, 3UL);
                    var_29 = var_28.field_0;
                    var_30 = var_28.field_1;
                    var_31 = var_28.field_2;
                    *(uint64_t *)(local_sp_13 + (-24L)) = 4201732UL;
                    indirect_placeholder();
                    var_32 = ((*(uint32_t *)6358864UL + (-3)) < 2U);
                    var_33 = (uint64_t)*(uint32_t *)var_29;
                    var_34 = local_sp_13 + (-32L);
                    *(uint64_t *)var_34 = 4201772UL;
                    indirect_placeholder_7(0UL, 4250652UL, var_32, var_29, var_33, var_30, var_31);
                    *(unsigned char *)(local_sp_13 + 11UL) = (unsigned char)'\x00';
                    local_sp_8 = var_34;
                    rcx_4 = var_29;
                    r9_5 = var_30;
                    r8_5 = var_31;
                } else {
                    var_26 = local_sp_13 + (-16L);
                    *(uint64_t *)var_26 = 4201802UL;
                    indirect_placeholder();
                    local_sp_8 = var_26;
                    r14_0 = r14_1 + 1UL;
                }
                var_35 = (uint32_t)rbp_0;
                r14_1 = r14_0;
                r8_6 = r8_5;
                r9_6 = r9_5;
                local_sp_9 = local_sp_8;
                local_sp_14 = local_sp_8;
                local_sp_13 = local_sp_8;
                rcx_5 = rcx_4;
                r9_7 = r9_5;
                r8_7 = r8_5;
                if ((uint64_t)(var_35 - var_18) == 0UL) {
                    break;
                }
                rbp_0 = (uint64_t)(var_35 + 1U);
                continue;
            }
        *(uint64_t *)(local_sp_8 + 16UL) = r14_0;
        if (r14_0 != 0UL) {
            _pre149 = (uint32_t *)(local_sp_8 + 44UL);
            _pre_phi150 = _pre149;
            var_63 = *_pre_phi150 + 1U;
            var_64 = *(uint64_t *)(local_sp_9 + 32UL);
            local_sp_10 = local_sp_9;
            while (1U)
                {
                    var_65 = (uint64_t)((long)(rbx_1 << 32UL) >> (long)29UL);
                    var_66 = *(uint64_t *)(var_65 + var_15);
                    local_sp_11 = local_sp_10;
                    var_67 = local_sp_10 + (-8L);
                    *(uint64_t *)var_67 = 4202249UL;
                    var_68 = indirect_placeholder_1(var_66);
                    local_sp_11 = var_67;
                    if (var_66 != 0UL & (uint64_t)(uint32_t)var_68 == 0UL) {
                        var_69 = *(uint64_t *)(var_65 + var_64);
                        *(uint64_t *)(local_sp_10 + (-16L)) = 4202272UL;
                        var_70 = indirect_placeholder_12(var_69, 0UL, 3UL);
                        var_71 = var_70.field_0;
                        var_72 = var_70.field_1;
                        var_73 = var_70.field_2;
                        *(uint64_t *)(local_sp_10 + (-24L)) = 4202280UL;
                        indirect_placeholder();
                        var_74 = (uint64_t)*(uint32_t *)var_71;
                        var_75 = local_sp_10 + (-32L);
                        *(uint64_t *)var_75 = 4202305UL;
                        indirect_placeholder_7(0UL, 4250652UL, 0UL, var_71, var_74, var_72, var_73);
                        *(unsigned char *)(local_sp_10 + 11UL) = (unsigned char)'\x00';
                        local_sp_11 = var_75;
                    }
                    var_76 = (uint32_t)rbx_1 + 1U;
                    local_sp_10 = local_sp_11;
                    rbx_1 = (uint64_t)var_76;
                    local_sp_12 = local_sp_11;
                    if ((uint64_t)(var_76 - var_63) != 0UL) {
                        continue;
                    }
                    break;
                }
            *(uint64_t *)(local_sp_12 + (-8L)) = 4202363UL;
            indirect_placeholder();
            return (uint64_t)*(unsigned char *)(local_sp_12 + 35UL);
        }
    }
    *var_11 = 1UL;
    *(unsigned char *)(var_0 + (-1109L)) = (unsigned char)'\x01';
    var_36 = *(uint32_t *)(local_sp_14 + 44UL);
    r9_8 = r9_7;
    rax_7 = (uint64_t)var_36;
    rcx_6 = rcx_5;
    r8_8 = r8_7;
    local_sp_15 = local_sp_14;
    while (1U)
        {
            var_37 = local_sp_15 + (-8L);
            *(uint64_t *)var_37 = 4201875UL;
            indirect_placeholder();
            r9_3 = r9_8;
            r8_3 = r8_8;
            local_sp_5 = var_37;
            local_sp_7 = var_37;
            rax_5 = rax_7;
            local_sp_3 = var_37;
            rax_3 = rax_7;
            r9_1 = r9_8;
            r8_1 = r8_8;
            rcx_2 = rcx_6;
            rax_6 = rax_7;
            rcx_3 = rcx_6;
            r9_4 = r9_8;
            r8_4 = r8_8;
            if ((long)rax_7 <= (long)18446744073709551615UL) {
                var_38 = local_sp_15 + (-16L);
                *(uint64_t *)var_38 = 4201888UL;
                indirect_placeholder();
                local_sp_5 = var_38;
                local_sp_6 = var_38;
                if (*(uint32_t *)rax_7 != 4U) {
                    loop_state_var = 1U;
                    break;
                }
            }
            var_39 = helper_cc_compute_all_wrapper(rax_7, 0UL, 0UL, 25U);
            if ((uint64_t)(((unsigned char)(var_39 >> 4UL) ^ (unsigned char)var_39) & '\xc0') != 0UL) {
                loop_state_var = 0U;
                break;
            }
            if ((int)*(uint32_t *)(local_sp_15 + 36UL) < (int)0U) {
                while (1U)
                    {
                        var_40 = (uint64_t)((long)(rbx_0 << 32UL) >> (long)29UL);
                        _cast = (uint64_t *)(var_40 + var_15);
                        var_41 = *_cast;
                        rcx_0 = var_41;
                        r9_0 = r9_1;
                        r8_0 = r8_1;
                        local_sp_4 = local_sp_3;
                        rax_4 = rax_3;
                        r8_2 = r8_1;
                        r9_2 = r9_1;
                        if (var_41 == 0UL) {
                            var_59 = (uint32_t)rbx_0;
                            r9_3 = r9_2;
                            r8_3 = r8_2;
                            local_sp_5 = local_sp_4;
                            rax_5 = rax_4;
                            local_sp_3 = local_sp_4;
                            rax_3 = rax_4;
                            r9_1 = r9_2;
                            r8_1 = r8_2;
                            rcx_2 = rcx_1;
                            if ((uint64_t)(var_59 - var_36) == 0UL) {
                                break;
                            }
                            rbx_0 = (uint64_t)(var_59 + 1U);
                            continue;
                        }
                        var_42 = local_sp_3 + (-8L);
                        *(uint64_t *)var_42 = 4201966UL;
                        indirect_placeholder();
                        local_sp_4 = var_42;
                        rax_4 = 1UL;
                        rcx_1 = var_41;
                        if (rax_3 != 1UL) {
                            var_43 = local_sp_3 + (-16L);
                            *(uint64_t *)var_43 = 4201981UL;
                            indirect_placeholder();
                            var_44 = *(uint32_t *)rax_3;
                            var_45 = (uint64_t)var_44;
                            *(uint32_t *)(local_sp_3 + 12UL) = var_44;
                            var_46 = local_sp_3 + (-24L);
                            *(uint64_t *)var_46 = 4201992UL;
                            indirect_placeholder();
                            local_sp_0 = var_46;
                            rax_2 = var_45;
                            local_sp_2 = var_46;
                            if (*(uint32_t *)var_45 == 32U) {
                                var_47 = *(uint32_t *)6358864UL & (-3);
                                var_48 = (uint64_t)var_47;
                                *(unsigned char *)var_43 = ((uint64_t)(var_47 + (-1)) == 0UL);
                                rax_0 = var_48;
                                rax_2 = var_48;
                                if (*_cast == *(uint64_t *)6358720UL) {
                                    var_49 = local_sp_3 + (-32L);
                                    *(uint64_t *)var_49 = 4202043UL;
                                    indirect_placeholder();
                                    local_sp_0 = var_49;
                                    rax_0 = rax_2;
                                }
                                local_sp_1 = local_sp_0;
                                rax_1 = rax_0;
                                local_sp_2 = local_sp_0;
                                if (*(unsigned char *)(local_sp_0 + 8UL) != '\x00') {
                                    *_cast = 0UL;
                                    var_58 = (uint64_t *)(local_sp_1 + 16UL);
                                    *var_58 = (*var_58 + (-1L));
                                    rcx_1 = rcx_0;
                                    local_sp_4 = local_sp_1;
                                    rax_4 = rax_1;
                                    r8_2 = r8_0;
                                    r9_2 = r9_0;
                                    var_59 = (uint32_t)rbx_0;
                                    r9_3 = r9_2;
                                    r8_3 = r8_2;
                                    local_sp_5 = local_sp_4;
                                    rax_5 = rax_4;
                                    local_sp_3 = local_sp_4;
                                    rax_3 = rax_4;
                                    r9_1 = r9_2;
                                    r8_1 = r8_2;
                                    rcx_2 = rcx_1;
                                    if ((uint64_t)(var_59 - var_36) == 0UL) {
                                        break;
                                    }
                                    rbx_0 = (uint64_t)(var_59 + 1U);
                                    continue;
                                }
                            }
                            if (*_cast != *(uint64_t *)6358720UL) {
                                *(unsigned char *)var_43 = (unsigned char)'\x01';
                                var_49 = local_sp_3 + (-32L);
                                *(uint64_t *)var_49 = 4202043UL;
                                indirect_placeholder();
                                local_sp_0 = var_49;
                                rax_0 = rax_2;
                                local_sp_1 = local_sp_0;
                                rax_1 = rax_0;
                                local_sp_2 = local_sp_0;
                                if (*(unsigned char *)(local_sp_0 + 8UL) == '\x00') {
                                    *_cast = 0UL;
                                    var_58 = (uint64_t *)(local_sp_1 + 16UL);
                                    *var_58 = (*var_58 + (-1L));
                                    rcx_1 = rcx_0;
                                    local_sp_4 = local_sp_1;
                                    rax_4 = rax_1;
                                    r8_2 = r8_0;
                                    r9_2 = r9_0;
                                    var_59 = (uint32_t)rbx_0;
                                    r9_3 = r9_2;
                                    r8_3 = r8_2;
                                    local_sp_5 = local_sp_4;
                                    rax_5 = rax_4;
                                    local_sp_3 = local_sp_4;
                                    rax_3 = rax_4;
                                    r9_1 = r9_2;
                                    r8_1 = r8_2;
                                    rcx_2 = rcx_1;
                                    if ((uint64_t)(var_59 - var_36) == 0UL) {
                                        break;
                                    }
                                    rbx_0 = (uint64_t)(var_59 + 1U);
                                    continue;
                                }
                            }
                            var_50 = *(uint64_t *)(var_40 + *(uint64_t *)(local_sp_2 + 32UL));
                            *(uint64_t *)(local_sp_2 + (-8L)) = 4202078UL;
                            var_51 = indirect_placeholder_13(var_50, 0UL, 3UL);
                            var_52 = var_51.field_0;
                            var_53 = var_51.field_1;
                            var_54 = var_51.field_2;
                            var_55 = ((*(uint32_t *)6358864UL + (-3)) < 2U);
                            var_56 = (uint64_t)*(uint32_t *)(local_sp_2 + 20UL);
                            var_57 = local_sp_2 + (-16L);
                            *(uint64_t *)var_57 = 4202120UL;
                            indirect_placeholder_7(0UL, 4250652UL, var_55, var_52, var_56, var_53, var_54);
                            *_cast = 0UL;
                            *(unsigned char *)(local_sp_2 + 27UL) = (unsigned char)'\x00';
                            local_sp_1 = var_57;
                            rcx_0 = var_52;
                            r9_0 = var_53;
                            r8_0 = var_54;
                            var_58 = (uint64_t *)(local_sp_1 + 16UL);
                            *var_58 = (*var_58 + (-1L));
                            rcx_1 = rcx_0;
                            local_sp_4 = local_sp_1;
                            rax_4 = rax_1;
                            r8_2 = r8_0;
                            r9_2 = r9_0;
                        }
                    }
            }
        }
    switch (loop_state_var) {
      case 0U:
        {
            var_62 = (uint32_t *)(local_sp_7 + 44UL);
            _pre_phi150 = var_62;
            local_sp_9 = local_sp_7;
            local_sp_12 = local_sp_7;
            if ((int)*var_62 > (int)0U) {
                *(uint64_t *)(local_sp_12 + (-8L)) = 4202363UL;
                indirect_placeholder();
                return (uint64_t)*(unsigned char *)(local_sp_12 + 35UL);
            }
            var_63 = *_pre_phi150 + 1U;
            var_64 = *(uint64_t *)(local_sp_9 + 32UL);
            local_sp_10 = local_sp_9;
            while (1U)
                {
                    var_65 = (uint64_t)((long)(rbx_1 << 32UL) >> (long)29UL);
                    var_66 = *(uint64_t *)(var_65 + var_15);
                    local_sp_11 = local_sp_10;
                    var_67 = local_sp_10 + (-8L);
                    *(uint64_t *)var_67 = 4202249UL;
                    var_68 = indirect_placeholder_1(var_66);
                    local_sp_11 = var_67;
                    if (var_66 != 0UL & (uint64_t)(uint32_t)var_68 == 0UL) {
                        var_69 = *(uint64_t *)(var_65 + var_64);
                        *(uint64_t *)(local_sp_10 + (-16L)) = 4202272UL;
                        var_70 = indirect_placeholder_12(var_69, 0UL, 3UL);
                        var_71 = var_70.field_0;
                        var_72 = var_70.field_1;
                        var_73 = var_70.field_2;
                        *(uint64_t *)(local_sp_10 + (-24L)) = 4202280UL;
                        indirect_placeholder();
                        var_74 = (uint64_t)*(uint32_t *)var_71;
                        var_75 = local_sp_10 + (-32L);
                        *(uint64_t *)var_75 = 4202305UL;
                        indirect_placeholder_7(0UL, 4250652UL, 0UL, var_71, var_74, var_72, var_73);
                        *(unsigned char *)(local_sp_10 + 11UL) = (unsigned char)'\x00';
                        local_sp_11 = var_75;
                    }
                    var_76 = (uint32_t)rbx_1 + 1U;
                    local_sp_10 = local_sp_11;
                    rbx_1 = (uint64_t)var_76;
                    local_sp_12 = local_sp_11;
                    if ((uint64_t)(var_76 - var_63) != 0UL) {
                        continue;
                    }
                    break;
                }
            *(uint64_t *)(local_sp_12 + (-8L)) = 4202363UL;
            indirect_placeholder();
            return (uint64_t)*(unsigned char *)(local_sp_12 + 35UL);
        }
        break;
      case 1U:
        {
            local_sp_7 = local_sp_6;
            if (rax_7 == 18446744073709551615UL) {
                *(uint64_t *)(local_sp_6 + (-8L)) = 4202174UL;
                indirect_placeholder();
                var_60 = (uint64_t)*(uint32_t *)rax_6;
                var_61 = local_sp_6 + (-16L);
                *(uint64_t *)var_61 = 4202196UL;
                indirect_placeholder_7(0UL, 4248660UL, 0UL, rcx_3, var_60, r9_4, r8_4);
                *(unsigned char *)(local_sp_6 + 27UL) = (unsigned char)'\x00';
                local_sp_7 = var_61;
            }
        }
        break;
    }
}

typedef unsigned char __u_char;
typedef unsigned short __u_short;
typedef unsigned int __u_int;
typedef unsigned long __u_long;
typedef signed char __int8_t;
typedef unsigned char __uint8_t;
typedef short __int16_t;
typedef unsigned short __uint16_t;
typedef int __int32_t;
typedef unsigned int __uint32_t;
typedef long __int64_t;
typedef unsigned long __uint64_t;
typedef __int8_t __int_least8_t;
typedef __uint8_t __uint_least8_t;
typedef __int16_t __int_least16_t;
typedef __uint16_t __uint_least16_t;
typedef __int32_t __int_least32_t;
typedef __uint32_t __uint_least32_t;
typedef __int64_t __int_least64_t;
typedef __uint64_t __uint_least64_t;
typedef long __quad_t;
typedef unsigned long __u_quad_t;
typedef long __intmax_t;
typedef unsigned long __uintmax_t;
typedef unsigned long __dev_t;
typedef unsigned int __uid_t;
typedef unsigned int __gid_t;
typedef unsigned long __ino_t;
typedef unsigned long __ino64_t;
typedef unsigned int __mode_t;
typedef unsigned long __nlink_t;
typedef long __off_t;
typedef long __off64_t;
typedef int __pid_t;
typedef struct {
    int __val[2];
} __fsid_t;
typedef long __clock_t;
typedef unsigned long __rlim_t;
typedef unsigned long __rlim64_t;
typedef unsigned int __id_t;
typedef long __time_t;
typedef unsigned int __useconds_t;
typedef long __suseconds_t;
typedef int __daddr_t;
typedef int __key_t;
typedef int __clockid_t;
typedef void *__timer_t;
typedef long __blksize_t;
typedef long __blkcnt_t;
typedef long __blkcnt64_t;
typedef unsigned long __fsblkcnt_t;
typedef unsigned long __fsblkcnt64_t;
typedef unsigned long __fsfilcnt_t;
typedef unsigned long __fsfilcnt64_t;
typedef long __fsword_t;
typedef long __ssize_t;
typedef long __syscall_slong_t;
typedef unsigned long __syscall_ulong_t;
typedef __off64_t __loff_t;
typedef char *__caddr_t;
typedef long __intptr_t;
typedef unsigned int __socklen_t;
typedef int __sig_atomic_t;
typedef __int8_t int8_t;
typedef __int16_t int16_t;
typedef __int32_t int32_t;
typedef __int64_t int64_t;
typedef __uint8_t uint8_t;
typedef __uint16_t uint16_t;
typedef __uint32_t uint32_t;
typedef __uint64_t uint64_t;
typedef __int_least8_t int_least8_t;
typedef __int_least16_t int_least16_t;
typedef __int_least32_t int_least32_t;
typedef __int_least64_t int_least64_t;
typedef __uint_least8_t uint_least8_t;
typedef __uint_least16_t uint_least16_t;
typedef __uint_least32_t uint_least32_t;
typedef __uint_least64_t uint_least64_t;
typedef signed char int_fast8_t;
typedef long int_fast16_t;
typedef long int_fast32_t;
typedef long int_fast64_t;
typedef unsigned char uint_fast8_t;
typedef unsigned long uint_fast16_t;
typedef unsigned long uint_fast32_t;
typedef unsigned long uint_fast64_t;
typedef long intptr_t;
typedef unsigned long uintptr_t;
typedef __intmax_t intmax_t;
typedef __uintmax_t uintmax_t;
typedef _Bool bool;
extern uint64_t revng_init_local_sp(uint64_t param_0);
extern uint64_t init_rbx(void);
extern void indirect_placeholder(void);
extern uint64_t init_rbp(void);
extern uint64_t init_r14(void);
extern uint64_t init_r13(void);
extern uint64_t init_r12(void);
extern uint64_t init_r15(void);
extern uint64_t indirect_placeholder_10(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint64_t param_3);
uint64_t bb_mkancesdirs(uint64_t rdx, uint64_t rdi, uint64_t rcx, uint64_t rsi) {
    uint64_t rbx_0_ph47;
    uint64_t var_0;
    uint64_t var_1;
    uint64_t var_2;
    uint64_t var_3;
    uint64_t var_4;
    uint64_t var_5;
    uint64_t var_6;
    uint64_t var_7;
    uint64_t local_sp_0_ph;
    uint64_t rbx_0_ph;
    uint64_t r14_0_ph;
    uint64_t r12_0_ph49;
    uint64_t rbp_0_ph;
    uint64_t rcx3_0_ph;
    uint64_t rcx3_0;
    uint64_t r14_1;
    uint32_t *var_23;
    uint64_t storemerge;
    uint64_t var_17;
    uint64_t local_sp_1;
    unsigned char var_9;
    uint64_t r12_0_ph53;
    uint64_t rbx_0;
    uint64_t var_10;
    uint64_t rcx3_0_ph54;
    unsigned char var_11;
    bool var_12;
    bool var_13;
    uint64_t rbp_0_ph48;
    uint64_t rcx3_0_ph50;
    bool var_8;
    uint64_t rbx_0_ph52;
    uint64_t var_14;
    uint64_t var_15;
    uint64_t var_18;
    uint64_t var_19;
    uint64_t var_20;
    uint64_t var_21;
    uint32_t var_22;
    uint64_t var_16;
    unsigned int loop_state_var;
    bool switch_state_var;
    var_0 = revng_init_local_sp(0UL);
    var_1 = init_rbx();
    var_2 = init_r14();
    var_3 = init_rbp();
    var_4 = init_r13();
    var_5 = init_r12();
    var_6 = init_r15();
    *(uint64_t *)(var_0 + (-8L)) = var_6;
    *(uint64_t *)(var_0 + (-16L)) = var_2;
    *(uint64_t *)(var_0 + (-24L)) = var_4;
    *(uint64_t *)(var_0 + (-32L)) = var_5;
    *(uint64_t *)(var_0 + (-40L)) = var_3;
    *(uint64_t *)(var_0 + (-48L)) = var_1;
    var_7 = var_0 + (-88L);
    *(uint64_t *)(var_0 + (-80L)) = rsi;
    *(uint64_t *)(var_0 + (-72L)) = rcx;
    local_sp_0_ph = var_7;
    rbx_0_ph = rdi;
    r14_0_ph = 0UL;
    rbp_0_ph = 0UL;
    rcx3_0_ph = rcx;
    while (1U)
        {
            rbx_0_ph47 = rbx_0_ph;
            r12_0_ph49 = rbx_0_ph;
            rcx3_0_ph = 0UL;
            r14_1 = r14_0_ph;
            local_sp_1 = local_sp_0_ph;
            rbp_0_ph48 = rbp_0_ph;
            rcx3_0_ph50 = rcx3_0_ph;
            while (1U)
                {
                    var_8 = (rbp_0_ph48 == 0UL);
                    rbp_0_ph = rbp_0_ph48;
                    rbx_0_ph52 = rbx_0_ph47;
                    r12_0_ph53 = r12_0_ph49;
                    rcx3_0_ph54 = rcx3_0_ph50;
                    while (1U)
                        {
                            var_9 = *(unsigned char *)rbx_0_ph52;
                            rbx_0 = rbx_0_ph52;
                            rcx3_0 = rcx3_0_ph54;
                            r12_0_ph49 = r12_0_ph53;
                            while (1U)
                                {
                                    var_10 = rbx_0 + 1UL;
                                    rbx_0_ph47 = var_10;
                                    rbx_0_ph = var_10;
                                    r12_0_ph53 = var_10;
                                    rbx_0 = var_10;
                                    rcx3_0_ph50 = rcx3_0;
                                    rbx_0_ph52 = var_10;
                                    if (var_9 != '\x00') {
                                        storemerge = r12_0_ph53 - rdi;
                                        loop_state_var = 1U;
                                        break;
                                    }
                                    var_11 = *(unsigned char *)var_10;
                                    var_12 = (var_11 == '/');
                                    var_13 = (var_9 == '/');
                                    var_9 = var_11;
                                    if (!var_12) {
                                        loop_state_var = 2U;
                                        break;
                                    }
                                    var_14 = (rcx3_0 & (-256L)) | var_13;
                                    rcx3_0 = var_14;
                                    rcx3_0_ph54 = var_14;
                                    if (((var_13 && (var_11 != '\x00')) ^ 1) || var_8) {
                                        continue;
                                    }
                                    loop_state_var = 0U;
                                    break;
                                }
                            switch_state_var = 0;
                            switch (loop_state_var) {
                              case 0U:
                                {
                                    var_15 = rbp_0_ph48 - r12_0_ph53;
                                    if (var_15 != 1UL) {
                                        loop_state_var = 2U;
                                        switch_state_var = 1;
                                        break;
                                    }
                                    if (*(unsigned char *)r12_0_ph53 == '.') {
                                        continue;
                                    }
                                    loop_state_var = 3U;
                                    switch_state_var = 1;
                                    break;
                                }
                                break;
                              case 1U:
                                {
                                    loop_state_var = 0U;
                                    switch_state_var = 1;
                                    break;
                                }
                                break;
                              case 2U:
                                {
                                    loop_state_var = 1U;
                                    switch_state_var = 1;
                                    break;
                                }
                                break;
                            }
                            if (switch_state_var)
                                break;
                        }
                    switch_state_var = 0;
                    switch (loop_state_var) {
                      case 1U:
                        {
                            rbp_0_ph48 = var_13 ? rbp_0_ph48 : var_10;
                            continue;
                        }
                        break;
                      case 0U:
                        {
                            loop_state_var = 0U;
                            switch_state_var = 1;
                            break;
                        }
                        break;
                      case 2U:
                        {
                            loop_state_var = 1U;
                            switch_state_var = 1;
                            break;
                        }
                        break;
                      case 3U:
                        {
                            loop_state_var = 2U;
                            switch_state_var = 1;
                            break;
                        }
                        break;
                    }
                    if (switch_state_var)
                        break;
                }
            switch_state_var = 0;
            switch (loop_state_var) {
              case 0U:
                {
                    loop_state_var = 0U;
                    switch_state_var = 1;
                    break;
                }
                break;
              case 2U:
              case 1U:
                {
                    switch (loop_state_var) {
                      case 2U:
                        {
                            *(unsigned char *)rbp_0_ph48 = (unsigned char)'\x00';
                            var_16 = local_sp_0_ph + (-8L);
                            *(uint64_t *)var_16 = 4218105UL;
                            indirect_placeholder();
                            local_sp_1 = var_16;
                            if ((int)(uint32_t)var_15 > (int)4294967295U) {
                                var_17 = local_sp_0_ph + (-16L);
                                *(uint64_t *)var_17 = 4218114UL;
                                indirect_placeholder();
                                *(uint32_t *)(local_sp_0_ph + 12UL) = *(uint32_t *)var_15;
                                local_sp_1 = var_17;
                            } else {
                                *(uint32_t *)(local_sp_0_ph + 20UL) = 0U;
                                r14_1 = 1UL;
                            }
                        }
                        break;
                      case 1U:
                        {
                            *(unsigned char *)rbp_0_ph48 = (unsigned char)'\x00';
                            r14_1 = 0UL;
                            if (var_15 == 2UL && *(unsigned char *)r12_0_ph53 == '.') {
                                if (*(unsigned char *)(r12_0_ph53 + 1UL) == '.') {
                                    *(uint32_t *)(local_sp_0_ph + 28UL) = 0U;
                                } else {
                                    var_16 = local_sp_0_ph + (-8L);
                                    *(uint64_t *)var_16 = 4218105UL;
                                    indirect_placeholder();
                                    local_sp_1 = var_16;
                                    if ((int)(uint32_t)var_15 > (int)4294967295U) {
                                        *(uint32_t *)(local_sp_0_ph + 20UL) = 0U;
                                        r14_1 = 1UL;
                                    } else {
                                        var_17 = local_sp_0_ph + (-16L);
                                        *(uint64_t *)var_17 = 4218114UL;
                                        indirect_placeholder();
                                        *(uint32_t *)(local_sp_0_ph + 12UL) = *(uint32_t *)var_15;
                                        local_sp_1 = var_17;
                                    }
                                }
                            } else {
                                var_16 = local_sp_0_ph + (-8L);
                                *(uint64_t *)var_16 = 4218105UL;
                                indirect_placeholder();
                                local_sp_1 = var_16;
                                if ((int)(uint32_t)var_15 > (int)4294967295U) {
                                    var_17 = local_sp_0_ph + (-16L);
                                    *(uint64_t *)var_17 = 4218114UL;
                                    indirect_placeholder();
                                    *(uint32_t *)(local_sp_0_ph + 12UL) = *(uint32_t *)var_15;
                                    local_sp_1 = var_17;
                                } else {
                                    *(uint32_t *)(local_sp_0_ph + 20UL) = 0U;
                                    r14_1 = 1UL;
                                }
                            }
                        }
                        break;
                    }
                    var_18 = (uint64_t)(unsigned char)r14_1;
                    var_19 = *(uint64_t *)(local_sp_1 + 8UL);
                    var_20 = local_sp_1 + (-8L);
                    *(uint64_t *)var_20 = 4218174UL;
                    var_21 = indirect_placeholder_10(var_18, var_19, 0UL, r12_0_ph53);
                    var_22 = (uint32_t)var_21;
                    local_sp_0_ph = var_20;
                    r14_0_ph = r14_1;
                    *(unsigned char *)rbp_0_ph48 = (unsigned char)'/';
                    if ((uint64_t)(var_22 + 1U) == 0UL && (uint64_t)var_22 == 0UL) {
                        continue;
                    }
                    loop_state_var = 1U;
                    switch_state_var = 1;
                    break;
                }
                break;
            }
            if (switch_state_var)
                break;
        }
    switch (loop_state_var) {
      case 0U:
        {
            return storemerge;
        }
        break;
      case 1U:
        {
            *(uint64_t *)(local_sp_1 + (-16L)) = 4218202UL;
            indirect_placeholder();
            var_23 = (uint32_t *)var_21;
            if (*(uint32_t *)(local_sp_1 + 20UL) != 0U & *var_23 == 2U) {
                *(uint64_t *)(local_sp_1 + (-24L)) = 4218212UL;
                indirect_placeholder();
                *var_23 = *(uint32_t *)(local_sp_1 + 4UL);
            }
            storemerge = (uint64_t)((long)(var_21 << 32UL) >> (long)32UL);
        }
        break;
    }
}

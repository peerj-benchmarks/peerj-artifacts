typedef unsigned char __u_char;
typedef unsigned short __u_short;
typedef unsigned int __u_int;
typedef unsigned long __u_long;
typedef signed char __int8_t;
typedef unsigned char __uint8_t;
typedef short __int16_t;
typedef unsigned short __uint16_t;
typedef int __int32_t;
typedef unsigned int __uint32_t;
typedef long __int64_t;
typedef unsigned long __uint64_t;
typedef __int8_t __int_least8_t;
typedef __uint8_t __uint_least8_t;
typedef __int16_t __int_least16_t;
typedef __uint16_t __uint_least16_t;
typedef __int32_t __int_least32_t;
typedef __uint32_t __uint_least32_t;
typedef __int64_t __int_least64_t;
typedef __uint64_t __uint_least64_t;
typedef long __quad_t;
typedef unsigned long __u_quad_t;
typedef long __intmax_t;
typedef unsigned long __uintmax_t;
typedef unsigned long __dev_t;
typedef unsigned int __uid_t;
typedef unsigned int __gid_t;
typedef unsigned long __ino_t;
typedef unsigned long __ino64_t;
typedef unsigned int __mode_t;
typedef unsigned long __nlink_t;
typedef long __off_t;
typedef long __off64_t;
typedef int __pid_t;
typedef struct {
    int __val[2];
} __fsid_t;
typedef long __clock_t;
typedef unsigned long __rlim_t;
typedef unsigned long __rlim64_t;
typedef unsigned int __id_t;
typedef long __time_t;
typedef unsigned int __useconds_t;
typedef long __suseconds_t;
typedef int __daddr_t;
typedef int __key_t;
typedef int __clockid_t;
typedef void *__timer_t;
typedef long __blksize_t;
typedef long __blkcnt_t;
typedef long __blkcnt64_t;
typedef unsigned long __fsblkcnt_t;
typedef unsigned long __fsblkcnt64_t;
typedef unsigned long __fsfilcnt_t;
typedef unsigned long __fsfilcnt64_t;
typedef long __fsword_t;
typedef long __ssize_t;
typedef long __syscall_slong_t;
typedef unsigned long __syscall_ulong_t;
typedef __off64_t __loff_t;
typedef char *__caddr_t;
typedef long __intptr_t;
typedef unsigned int __socklen_t;
typedef int __sig_atomic_t;
typedef __int8_t int8_t;
typedef __int16_t int16_t;
typedef __int32_t int32_t;
typedef __int64_t int64_t;
typedef __uint8_t uint8_t;
typedef __uint16_t uint16_t;
typedef __uint32_t uint32_t;
typedef __uint64_t uint64_t;
typedef __int_least8_t int_least8_t;
typedef __int_least16_t int_least16_t;
typedef __int_least32_t int_least32_t;
typedef __int_least64_t int_least64_t;
typedef __uint_least8_t uint_least8_t;
typedef __uint_least16_t uint_least16_t;
typedef __uint_least32_t uint_least32_t;
typedef __uint_least64_t uint_least64_t;
typedef signed char int_fast8_t;
typedef long int_fast16_t;
typedef long int_fast32_t;
typedef long int_fast64_t;
typedef unsigned char uint_fast8_t;
typedef unsigned long uint_fast16_t;
typedef unsigned long uint_fast32_t;
typedef unsigned long uint_fast64_t;
typedef long intptr_t;
typedef unsigned long uintptr_t;
typedef __intmax_t intmax_t;
typedef __uintmax_t uintmax_t;
typedef _Bool bool;
struct indirect_placeholder_23_ret_type;
struct indirect_placeholder_22_ret_type;
struct indirect_placeholder_24_ret_type;
struct indirect_placeholder_25_ret_type;
struct indirect_placeholder_26_ret_type;
struct indirect_placeholder_27_ret_type;
struct indirect_placeholder_23_ret_type {
    uint64_t field_0;
    uint64_t field_1;
    uint64_t field_2;
};
struct indirect_placeholder_22_ret_type {
    uint64_t field_0;
    uint64_t field_1;
};
struct indirect_placeholder_24_ret_type {
    uint64_t field_0;
    uint64_t field_1;
};
struct indirect_placeholder_25_ret_type {
    uint64_t field_0;
    uint64_t field_1;
};
struct indirect_placeholder_26_ret_type {
    uint64_t field_0;
    uint64_t field_1;
};
struct indirect_placeholder_27_ret_type {
    uint64_t field_0;
    uint64_t field_1;
    uint64_t field_2;
    uint64_t field_3;
};
extern void abort(void);
extern uint64_t revng_init_local_sp(uint64_t param_0);
extern uint64_t init_rbx(void);
extern void indirect_placeholder(void);
extern uint64_t init_rbp(void);
extern uint64_t init_r14(void);
extern uint64_t init_r13(void);
extern uint64_t init_r12(void);
extern uint64_t indirect_placeholder_8(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint64_t param_3, uint64_t param_4, uint64_t param_5);
extern void indirect_placeholder_21(uint64_t param_0);
extern void indirect_placeholder_1(uint64_t param_0, uint64_t param_1, uint64_t param_2);
extern uint64_t indirect_placeholder_10(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint64_t param_3);
extern struct indirect_placeholder_23_ret_type indirect_placeholder_23(uint64_t param_0);
extern struct indirect_placeholder_22_ret_type indirect_placeholder_22(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint64_t param_3, uint64_t param_4, uint64_t param_5, uint64_t param_6);
extern struct indirect_placeholder_24_ret_type indirect_placeholder_24(uint64_t param_0, uint64_t param_1, uint64_t param_2);
extern struct indirect_placeholder_25_ret_type indirect_placeholder_25(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint64_t param_3, uint64_t param_4, uint64_t param_5, uint64_t param_6);
extern void indirect_placeholder_18(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint64_t param_3, uint64_t param_4, uint64_t param_5, uint64_t param_6);
extern struct indirect_placeholder_26_ret_type indirect_placeholder_26(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint64_t param_3, uint64_t param_4, uint64_t param_5, uint64_t param_6);
extern struct indirect_placeholder_27_ret_type indirect_placeholder_27(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint64_t param_3, uint64_t param_4);
void bb_main(uint64_t rdi, uint64_t rsi) {
    struct indirect_placeholder_22_ret_type var_35;
    struct indirect_placeholder_27_ret_type var_13;
    uint64_t r10_0;
    uint64_t var_0;
    uint64_t var_1;
    uint64_t var_2;
    uint64_t var_3;
    uint64_t var_4;
    uint64_t var_5;
    uint64_t var_6;
    uint32_t var_7;
    uint64_t var_8;
    uint64_t var_9;
    uint64_t var_10;
    uint64_t local_sp_3_ph;
    uint64_t r12_0_ph;
    uint64_t local_sp_3;
    struct indirect_placeholder_23_ret_type var_30;
    uint64_t var_31;
    uint64_t var_32;
    uint64_t var_33;
    uint64_t var_34;
    uint64_t local_sp_0;
    uint64_t rax_0;
    uint64_t var_36;
    uint64_t var_37;
    uint64_t var_38;
    uint64_t local_sp_1;
    uint64_t var_27;
    struct indirect_placeholder_24_ret_type var_28;
    uint64_t var_29;
    bool var_23;
    uint32_t var_39;
    uint64_t var_40;
    uint64_t var_41;
    uint64_t var_24;
    uint64_t var_25;
    uint64_t var_26;
    uint64_t local_sp_2;
    uint64_t var_19;
    uint64_t var_20;
    uint64_t var_21;
    uint64_t local_sp_3_be;
    uint64_t var_22;
    uint64_t var_11;
    uint64_t *var_12;
    uint64_t var_14;
    uint64_t var_15;
    uint64_t var_16;
    uint64_t var_17;
    uint32_t var_18;
    unsigned int loop_state_var;
    bool switch_state_var;
    var_0 = revng_init_local_sp(0UL);
    var_1 = init_rbx();
    var_2 = init_r14();
    var_3 = init_rbp();
    var_4 = init_r13();
    var_5 = init_r12();
    *(uint64_t *)(var_0 + (-8L)) = var_2;
    *(uint64_t *)(var_0 + (-16L)) = var_4;
    *(uint64_t *)(var_0 + (-24L)) = var_5;
    *(uint64_t *)(var_0 + (-32L)) = var_3;
    *(uint64_t *)(var_0 + (-40L)) = var_1;
    var_6 = var_0 + (-72L);
    var_7 = (uint32_t)rdi;
    var_8 = (uint64_t)var_7;
    *(uint64_t *)var_6 = 0UL;
    *(uint32_t *)(var_0 + (-60L)) = 511U;
    *(uint32_t *)(var_0 + (-56L)) = 0U;
    *(uint64_t *)(var_0 + (-48L)) = 0UL;
    *(unsigned char *)(var_0 + (-52L)) = (unsigned char)'\x00';
    var_9 = *(uint64_t *)rsi;
    *(uint64_t *)(var_0 + (-80L)) = 4203396UL;
    indirect_placeholder_21(var_9);
    *(uint64_t *)(var_0 + (-88L)) = 4203411UL;
    indirect_placeholder();
    var_10 = var_0 + (-96L);
    *(uint64_t *)var_10 = 4203421UL;
    indirect_placeholder();
    local_sp_3_ph = var_10;
    r12_0_ph = 0UL;
    while (1U)
        {
            local_sp_3 = local_sp_3_ph;
            while (1U)
                {
                    var_11 = local_sp_3 + (-8L);
                    var_12 = (uint64_t *)var_11;
                    *var_12 = 4203654UL;
                    var_13 = indirect_placeholder_27(4254780UL, var_8, 4254912UL, rsi, 0UL);
                    var_14 = var_13.field_0;
                    var_15 = var_13.field_1;
                    var_16 = var_13.field_2;
                    var_17 = var_13.field_3;
                    var_18 = (uint32_t)var_14;
                    local_sp_3_ph = var_11;
                    local_sp_1 = var_11;
                    local_sp_2 = var_11;
                    local_sp_3_be = var_11;
                    if ((uint64_t)(var_18 + 1U) != 0UL) {
                        if ((uint64_t)(*(uint32_t *)6366396UL - var_7) != 0UL) {
                            var_23 = (r12_0_ph == 0UL);
                            if (!var_23) {
                                loop_state_var = 0U;
                                break;
                            }
                            if (*var_12 != 0UL) {
                                loop_state_var = 1U;
                                break;
                            }
                            loop_state_var = 0U;
                            break;
                        }
                        *(uint64_t *)(local_sp_3 + (-16L)) = 4203696UL;
                        indirect_placeholder_25(0UL, 4254786UL, 0UL, var_15, 0UL, var_16, var_17);
                        *(uint64_t *)(local_sp_3 + (-24L)) = 4203706UL;
                        indirect_placeholder_1(rsi, var_8, 1UL);
                        abort();
                    }
                    if ((uint64_t)(var_18 + (-90)) == 0UL) {
                        if (*(uint64_t *)6367008UL == 0UL) {
                            var_22 = local_sp_3 + (-16L);
                            *(uint64_t *)var_22 = 4203550UL;
                            indirect_placeholder_26(0UL, 4254528UL, 0UL, var_15, 0UL, var_16, var_17);
                            local_sp_3_be = var_22;
                        }
                    } else {
                        if ((int)var_18 <= (int)90U) {
                            if ((uint64_t)(var_18 + 131U) == 0UL) {
                                if ((uint64_t)(var_18 + 130U) != 0UL) {
                                    loop_state_var = 3U;
                                    break;
                                }
                                *(uint64_t *)(local_sp_3 + (-16L)) = 4203562UL;
                                indirect_placeholder_1(rsi, var_8, 0UL);
                                abort();
                            }
                            var_19 = *(uint64_t *)6366272UL;
                            var_20 = *(uint64_t *)6366400UL;
                            *(uint64_t *)(local_sp_3 + (-16L)) = 4203608UL;
                            indirect_placeholder_18(0UL, 4254702UL, var_20, var_19, 4254601UL, 0UL, 4254764UL);
                            var_21 = local_sp_3 + (-24L);
                            *(uint64_t *)var_21 = 4203618UL;
                            indirect_placeholder();
                            local_sp_2 = var_21;
                            loop_state_var = 3U;
                            break;
                        }
                        if ((uint64_t)(var_18 + (-112)) == 0UL) {
                            *var_12 = 4202480UL;
                        } else {
                            if ((uint64_t)(var_18 + (-118)) != 0UL) {
                                loop_state_var = 2U;
                                break;
                            }
                            *(uint64_t *)(local_sp_3 + 16UL) = 4254743UL;
                        }
                    }
                    local_sp_3 = local_sp_3_be;
                    continue;
                }
            switch_state_var = 0;
            switch (loop_state_var) {
              case 2U:
                {
                    if ((uint64_t)(var_18 + (-109)) != 0UL) {
                        loop_state_var = 2U;
                        switch_state_var = 1;
                        break;
                    }
                    r12_0_ph = *(uint64_t *)6367008UL;
                    continue;
                }
                break;
              case 0U:
                {
                    var_24 = local_sp_3 + (-16L);
                    *(uint64_t *)var_24 = 4203736UL;
                    indirect_placeholder();
                    var_25 = (uint64_t)var_18;
                    var_26 = local_sp_3 + (-24L);
                    *(uint64_t *)var_26 = 4203746UL;
                    indirect_placeholder();
                    *(uint32_t *)var_24 = var_18;
                    local_sp_1 = var_26;
                    if (!var_23) {
                        *(uint32_t *)(local_sp_3 + (-12L)) = 511U;
                        loop_state_var = 0U;
                        switch_state_var = 1;
                        break;
                    }
                    var_27 = local_sp_3 + (-32L);
                    *(uint64_t *)var_27 = 4203764UL;
                    var_28 = indirect_placeholder_24(r12_0_ph, var_16, var_17);
                    var_29 = var_28.field_0;
                    local_sp_0 = var_27;
                    rax_0 = var_29;
                    r10_0 = var_28.field_1;
                    if (var_29 != 0UL) {
                        loop_state_var = 1U;
                        switch_state_var = 1;
                        break;
                    }
                    *(uint64_t *)(local_sp_3 + (-40L)) = 4203780UL;
                    var_30 = indirect_placeholder_23(r12_0_ph);
                    var_31 = var_30.field_0;
                    var_32 = var_30.field_1;
                    var_33 = var_30.field_2;
                    var_34 = local_sp_3 + (-48L);
                    *(uint64_t *)var_34 = 4203808UL;
                    var_35 = indirect_placeholder_22(0UL, 4254802UL, 1UL, var_31, 0UL, var_32, var_33);
                    local_sp_0 = var_34;
                    rax_0 = var_35.field_0;
                    r10_0 = var_35.field_1;
                    loop_state_var = 1U;
                    switch_state_var = 1;
                    break;
                }
                break;
              case 1U:
                {
                    loop_state_var = 0U;
                    switch_state_var = 1;
                    break;
                }
                break;
              case 3U:
                {
                    loop_state_var = 2U;
                    switch_state_var = 1;
                    break;
                }
                break;
            }
            if (switch_state_var)
                break;
        }
    switch (loop_state_var) {
      case 2U:
        {
            *(uint64_t *)(local_sp_2 + (-8L)) = 4203628UL;
            indirect_placeholder_1(rsi, var_8, 1UL);
            abort();
        }
        break;
      case 1U:
      case 0U:
        {
            switch (loop_state_var) {
              case 0U:
                {
                    var_39 = *(uint32_t *)6366396UL;
                    var_40 = ((uint64_t)var_39 << 3UL) + rsi;
                    var_41 = (uint64_t)(var_7 - var_39);
                    *(uint64_t *)(local_sp_1 + (-8L)) = 4203886UL;
                    indirect_placeholder_10(4202174UL, var_41, local_sp_1, var_40);
                    return;
                }
                break;
              case 1U:
                {
                    var_36 = local_sp_0 + 16UL;
                    *(uint64_t *)(local_sp_0 + (-8L)) = 4203834UL;
                    var_37 = indirect_placeholder_8(var_25, 511UL, rax_0, r10_0, 1UL, var_36);
                    *(uint32_t *)(local_sp_0 + 4UL) = (uint32_t)var_37;
                    var_38 = local_sp_0 + (-16L);
                    *(uint64_t *)var_38 = 4203846UL;
                    indirect_placeholder();
                    local_sp_1 = var_38;
                }
                break;
            }
        }
        break;
    }
}

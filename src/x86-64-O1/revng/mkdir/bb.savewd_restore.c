typedef unsigned char __u_char;
typedef unsigned short __u_short;
typedef unsigned int __u_int;
typedef unsigned long __u_long;
typedef signed char __int8_t;
typedef unsigned char __uint8_t;
typedef short __int16_t;
typedef unsigned short __uint16_t;
typedef int __int32_t;
typedef unsigned int __uint32_t;
typedef long __int64_t;
typedef unsigned long __uint64_t;
typedef __int8_t __int_least8_t;
typedef __uint8_t __uint_least8_t;
typedef __int16_t __int_least16_t;
typedef __uint16_t __uint_least16_t;
typedef __int32_t __int_least32_t;
typedef __uint32_t __uint_least32_t;
typedef __int64_t __int_least64_t;
typedef __uint64_t __uint_least64_t;
typedef long __quad_t;
typedef unsigned long __u_quad_t;
typedef long __intmax_t;
typedef unsigned long __uintmax_t;
typedef unsigned long __dev_t;
typedef unsigned int __uid_t;
typedef unsigned int __gid_t;
typedef unsigned long __ino_t;
typedef unsigned long __ino64_t;
typedef unsigned int __mode_t;
typedef unsigned long __nlink_t;
typedef long __off_t;
typedef long __off64_t;
typedef int __pid_t;
typedef struct {
    int __val[2];
} __fsid_t;
typedef long __clock_t;
typedef unsigned long __rlim_t;
typedef unsigned long __rlim64_t;
typedef unsigned int __id_t;
typedef long __time_t;
typedef unsigned int __useconds_t;
typedef long __suseconds_t;
typedef int __daddr_t;
typedef int __key_t;
typedef int __clockid_t;
typedef void *__timer_t;
typedef long __blksize_t;
typedef long __blkcnt_t;
typedef long __blkcnt64_t;
typedef unsigned long __fsblkcnt_t;
typedef unsigned long __fsblkcnt64_t;
typedef unsigned long __fsfilcnt_t;
typedef unsigned long __fsfilcnt64_t;
typedef long __fsword_t;
typedef long __ssize_t;
typedef long __syscall_slong_t;
typedef unsigned long __syscall_ulong_t;
typedef __off64_t __loff_t;
typedef char *__caddr_t;
typedef long __intptr_t;
typedef unsigned int __socklen_t;
typedef int __sig_atomic_t;
typedef __int8_t int8_t;
typedef __int16_t int16_t;
typedef __int32_t int32_t;
typedef __int64_t int64_t;
typedef __uint8_t uint8_t;
typedef __uint16_t uint16_t;
typedef __uint32_t uint32_t;
typedef __uint64_t uint64_t;
typedef __int_least8_t int_least8_t;
typedef __int_least16_t int_least16_t;
typedef __int_least32_t int_least32_t;
typedef __int_least64_t int_least64_t;
typedef __uint_least8_t uint_least8_t;
typedef __uint_least16_t uint_least16_t;
typedef __uint_least32_t uint_least32_t;
typedef __uint_least64_t uint_least64_t;
typedef signed char int_fast8_t;
typedef long int_fast16_t;
typedef long int_fast32_t;
typedef long int_fast64_t;
typedef unsigned char uint_fast8_t;
typedef unsigned long uint_fast16_t;
typedef unsigned long uint_fast32_t;
typedef unsigned long uint_fast64_t;
typedef long intptr_t;
typedef unsigned long uintptr_t;
typedef __intmax_t intmax_t;
typedef __uintmax_t uintmax_t;
extern void abort(void);
extern uint64_t revng_init_local_sp(uint64_t param_0);
extern uint64_t init_rbx(void);
extern void indirect_placeholder(void);
extern uint64_t init_rbp(void);
extern uint64_t helper_cc_compute_all_wrapper(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint32_t param_3);
uint64_t bb_savewd_restore(uint64_t rdi, uint64_t rsi) {
    uint64_t var_0;
    uint64_t var_1;
    uint64_t var_2;
    uint64_t var_3;
    uint32_t *_cast;
    uint32_t var_4;
    uint64_t var_11;
    uint64_t local_sp_0;
    uint64_t var_12;
    uint64_t var_13;
    uint64_t rax_0;
    uint32_t *_pre_phi27;
    uint64_t local_sp_3;
    uint32_t *var_14;
    uint32_t var_15;
    uint64_t var_16;
    uint32_t *var_17;
    uint32_t *_pre_phi29;
    uint64_t var_5;
    uint64_t local_sp_1;
    uint32_t *_pre26;
    uint32_t *_pre28;
    uint32_t *var_6;
    uint32_t var_7;
    uint64_t var_8;
    uint64_t var_9;
    uint64_t var_10;
    var_0 = revng_init_local_sp(0UL);
    var_1 = init_rbx();
    var_2 = init_rbp();
    *(uint64_t *)(var_0 + (-8L)) = var_2;
    *(uint64_t *)(var_0 + (-16L)) = var_1;
    var_3 = var_0 + (-40L);
    _cast = (uint32_t *)rdi;
    var_4 = *_cast;
    local_sp_1 = var_3;
    rax_0 = 0UL;
    local_sp_3 = var_3;
    if (var_4 > 4U) {
        *(uint64_t *)(var_0 + (-48L)) = 4211575UL;
        indirect_placeholder();
    } else {
        var_5 = (uint64_t)var_4;
        switch (*(uint64_t *)((var_5 << 3UL) + 4257832UL)) {
          case 4211582UL:
            {
                break;
            }
            break;
          case 4211372UL:
            {
                *(uint64_t *)(var_0 + (-48L)) = 4211380UL;
                indirect_placeholder();
                rax_0 = var_5;
                if (var_4 != 0U) {
                    *_cast = 1U;
                    return rax_0;
                }
                *(uint64_t *)(var_0 + (-56L)) = 4211401UL;
                indirect_placeholder();
                var_14 = (uint32_t *)var_5;
                var_15 = *var_14;
                var_16 = var_0 + (-64L);
                *(uint64_t *)var_16 = 4211411UL;
                indirect_placeholder();
                *_cast = 4U;
                var_17 = (uint32_t *)(rdi + 4UL);
                *var_17 = var_15;
                _pre_phi29 = var_14;
                _pre_phi27 = var_17;
                local_sp_3 = var_16;
                *(uint64_t *)(local_sp_3 + (-8L)) = 4211426UL;
                indirect_placeholder();
                *_pre_phi29 = *_pre_phi27;
                rax_0 = 4294967295UL;
            }
            break;
          case 4211421UL:
            {
                _pre26 = (uint32_t *)(rdi + 4UL);
                _pre28 = (uint32_t *)var_5;
                _pre_phi29 = _pre28;
                _pre_phi27 = _pre26;
                *(uint64_t *)(local_sp_3 + (-8L)) = 4211426UL;
                indirect_placeholder();
                *_pre_phi29 = *_pre_phi27;
                rax_0 = 4294967295UL;
            }
            break;
          case 4211441UL:
            {
                var_6 = (uint32_t *)(rdi + 4UL);
                var_7 = *var_6;
                var_8 = (uint64_t)var_7;
                if (var_7 == 0U) {
                    var_9 = var_0 + (-48L);
                    *(uint64_t *)var_9 = 4211455UL;
                    indirect_placeholder();
                    local_sp_1 = var_9;
                }
                var_10 = helper_cc_compute_all_wrapper(var_8, 0UL, 0UL, 24U);
                if ((uint64_t)(((unsigned char)(var_10 >> 4UL) ^ (unsigned char)var_10) & '\xc0') != 0UL) {
                    var_11 = local_sp_1 + (-8L);
                    *(uint64_t *)var_11 = 4211518UL;
                    indirect_placeholder();
                    *var_6 = 4294967295U;
                    local_sp_0 = var_11;
                    if ((*(uint32_t *)(local_sp_1 + 4UL) & 127U) != 0U) {
                        var_12 = local_sp_1 + (-16L);
                        *(uint64_t *)var_12 = 4211543UL;
                        indirect_placeholder();
                        local_sp_0 = var_12;
                    }
                    var_13 = (uint64_t)*(unsigned char *)(local_sp_0 + 13UL);
                    rax_0 = var_13;
                }
            }
            break;
          default:
            {
                abort();
            }
            break;
        }
    }
}

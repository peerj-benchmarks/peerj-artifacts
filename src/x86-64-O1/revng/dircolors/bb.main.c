typedef unsigned char __u_char;
typedef unsigned short __u_short;
typedef unsigned int __u_int;
typedef unsigned long __u_long;
typedef signed char __int8_t;
typedef unsigned char __uint8_t;
typedef short __int16_t;
typedef unsigned short __uint16_t;
typedef int __int32_t;
typedef unsigned int __uint32_t;
typedef long __int64_t;
typedef unsigned long __uint64_t;
typedef __int8_t __int_least8_t;
typedef __uint8_t __uint_least8_t;
typedef __int16_t __int_least16_t;
typedef __uint16_t __uint_least16_t;
typedef __int32_t __int_least32_t;
typedef __uint32_t __uint_least32_t;
typedef __int64_t __int_least64_t;
typedef __uint64_t __uint_least64_t;
typedef long __quad_t;
typedef unsigned long __u_quad_t;
typedef long __intmax_t;
typedef unsigned long __uintmax_t;
typedef unsigned long __dev_t;
typedef unsigned int __uid_t;
typedef unsigned int __gid_t;
typedef unsigned long __ino_t;
typedef unsigned long __ino64_t;
typedef unsigned int __mode_t;
typedef unsigned long __nlink_t;
typedef long __off_t;
typedef long __off64_t;
typedef int __pid_t;
typedef struct {
    int __val[2];
} __fsid_t;
typedef long __clock_t;
typedef unsigned long __rlim_t;
typedef unsigned long __rlim64_t;
typedef unsigned int __id_t;
typedef long __time_t;
typedef unsigned int __useconds_t;
typedef long __suseconds_t;
typedef int __daddr_t;
typedef int __key_t;
typedef int __clockid_t;
typedef void *__timer_t;
typedef long __blksize_t;
typedef long __blkcnt_t;
typedef long __blkcnt64_t;
typedef unsigned long __fsblkcnt_t;
typedef unsigned long __fsblkcnt64_t;
typedef unsigned long __fsfilcnt_t;
typedef unsigned long __fsfilcnt64_t;
typedef long __fsword_t;
typedef long __ssize_t;
typedef long __syscall_slong_t;
typedef unsigned long __syscall_ulong_t;
typedef __off64_t __loff_t;
typedef char *__caddr_t;
typedef long __intptr_t;
typedef unsigned int __socklen_t;
typedef int __sig_atomic_t;
typedef __int8_t int8_t;
typedef __int16_t int16_t;
typedef __int32_t int32_t;
typedef __int64_t int64_t;
typedef __uint8_t uint8_t;
typedef __uint16_t uint16_t;
typedef __uint32_t uint32_t;
typedef __uint64_t uint64_t;
typedef __int_least8_t int_least8_t;
typedef __int_least16_t int_least16_t;
typedef __int_least32_t int_least32_t;
typedef __int_least64_t int_least64_t;
typedef __uint_least8_t uint_least8_t;
typedef __uint_least16_t uint_least16_t;
typedef __uint_least32_t uint_least32_t;
typedef __uint_least64_t uint_least64_t;
typedef signed char int_fast8_t;
typedef long int_fast16_t;
typedef long int_fast32_t;
typedef long int_fast64_t;
typedef unsigned char uint_fast8_t;
typedef unsigned long uint_fast16_t;
typedef unsigned long uint_fast32_t;
typedef unsigned long uint_fast64_t;
typedef long intptr_t;
typedef unsigned long uintptr_t;
typedef __intmax_t intmax_t;
typedef __uintmax_t uintmax_t;
typedef _Bool bool;
struct indirect_placeholder_7_ret_type;
struct indirect_placeholder_8_ret_type;
struct indirect_placeholder_12_ret_type;
struct indirect_placeholder_14_ret_type;
struct indirect_placeholder_7_ret_type {
    uint64_t field_0;
    uint64_t field_1;
};
struct indirect_placeholder_8_ret_type {
    uint64_t field_0;
    uint64_t field_1;
};
struct indirect_placeholder_12_ret_type {
    uint64_t field_0;
    uint64_t field_1;
    uint64_t field_2;
};
struct indirect_placeholder_14_ret_type {
    uint64_t field_0;
    uint64_t field_1;
    uint64_t field_2;
};
extern void abort(void);
extern uint64_t revng_init_local_sp(uint64_t param_0);
extern uint64_t init_rbx(void);
extern void indirect_placeholder(void);
extern uint64_t init_rbp(void);
extern uint64_t init_r14(void);
extern uint64_t init_r13(void);
extern uint64_t init_r12(void);
extern uint64_t indirect_placeholder_1(uint64_t param_0);
extern uint64_t indirect_placeholder_9(uint64_t param_0, uint64_t param_1, uint64_t param_2);
extern void indirect_placeholder_5(uint64_t param_0);
extern uint64_t indirect_placeholder_10(uint64_t param_0, uint64_t param_1);
extern void indirect_placeholder_6(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint64_t param_3, uint64_t param_4, uint64_t param_5, uint64_t param_6);
extern void indirect_placeholder_11(uint64_t param_0, uint64_t param_1, uint64_t param_2);
extern void indirect_placeholder_13(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint64_t param_3, uint64_t param_4);
extern struct indirect_placeholder_7_ret_type indirect_placeholder_7(uint64_t param_0, uint64_t param_1, uint64_t param_2);
extern struct indirect_placeholder_8_ret_type indirect_placeholder_8(uint64_t param_0, uint64_t param_1, uint64_t param_2);
extern struct indirect_placeholder_12_ret_type indirect_placeholder_12(uint64_t param_0, uint64_t param_1);
extern struct indirect_placeholder_14_ret_type indirect_placeholder_14(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint64_t param_3, uint64_t param_4);
void bb_main(uint64_t rdi, uint64_t rsi) {
    struct indirect_placeholder_14_ret_type var_10;
    uint64_t var_0;
    uint64_t var_1;
    uint64_t var_2;
    uint64_t var_3;
    uint64_t var_4;
    uint64_t var_5;
    uint64_t var_6;
    uint64_t var_7;
    uint64_t var_8;
    uint64_t local_sp_6_ph;
    struct indirect_placeholder_7_ret_type var_45;
    uint64_t var_46;
    uint64_t var_47;
    uint64_t var_48;
    uint64_t rax_0;
    struct indirect_placeholder_8_ret_type var_37;
    uint64_t var_38;
    uint64_t var_39;
    uint64_t var_40;
    uint64_t var_34;
    uint64_t var_35;
    uint64_t var_36;
    uint64_t local_sp_0;
    uint64_t var_41;
    uint64_t var_42;
    uint64_t var_43;
    uint64_t var_44;
    uint64_t local_sp_1;
    uint64_t rbx_0_in;
    uint64_t var_51;
    uint64_t var_52;
    uint64_t var_53;
    uint64_t var_54;
    uint64_t var_55;
    uint64_t spec_store_select;
    uint64_t var_49;
    uint64_t var_50;
    uint64_t var_32;
    uint64_t var_33;
    uint64_t r12_1;
    uint64_t var_30;
    uint64_t local_sp_3;
    uint64_t var_28;
    uint64_t var_29;
    uint64_t var_31;
    uint64_t var_62;
    uint64_t local_sp_2;
    uint64_t var_15;
    uint64_t var_16;
    uint32_t var_17;
    uint64_t var_18;
    uint32_t var_19;
    uint64_t var_20;
    uint64_t var_21;
    uint64_t var_22;
    bool var_23;
    uint64_t r12_1_ph;
    bool var_24;
    uint64_t var_25;
    uint64_t var_56;
    struct indirect_placeholder_12_ret_type var_57;
    uint64_t var_58;
    uint64_t var_59;
    uint64_t var_60;
    uint64_t var_61;
    uint64_t local_sp_4;
    uint64_t storemerge;
    uint64_t var_26;
    uint64_t var_27;
    uint64_t r12_1_be;
    uint64_t local_sp_5;
    uint64_t var_12;
    uint64_t var_13;
    uint64_t var_14;
    uint64_t r13_0_ph;
    uint64_t local_sp_6;
    uint64_t var_9;
    uint32_t var_11;
    unsigned int loop_state_var;
    bool switch_state_var;
    var_0 = revng_init_local_sp(0UL);
    var_1 = init_rbx();
    var_2 = init_r14();
    var_3 = init_rbp();
    var_4 = init_r13();
    var_5 = init_r12();
    *(uint64_t *)(var_0 + (-8L)) = var_2;
    *(uint64_t *)(var_0 + (-16L)) = var_4;
    *(uint64_t *)(var_0 + (-24L)) = var_5;
    *(uint64_t *)(var_0 + (-32L)) = var_3;
    *(uint64_t *)(var_0 + (-40L)) = var_1;
    var_6 = (uint64_t)(uint32_t)rdi;
    var_7 = *(uint64_t *)rsi;
    *(uint64_t *)(var_0 + (-48L)) = 4204728UL;
    indirect_placeholder_5(var_7);
    *(uint64_t *)(var_0 + (-56L)) = 4204743UL;
    indirect_placeholder();
    var_8 = var_0 + (-64L);
    *(uint64_t *)var_8 = 4204753UL;
    indirect_placeholder();
    local_sp_6_ph = var_8;
    r12_1_ph = 2UL;
    storemerge = 4263552UL;
    r12_1_be = 0UL;
    r13_0_ph = 0UL;
    while (1U)
        {
            r13_0_ph = 1UL;
            local_sp_6 = local_sp_6_ph;
            r12_1 = r12_1_ph;
            while (1U)
                {
                    var_9 = local_sp_6 + (-8L);
                    *(uint64_t *)var_9 = 4204932UL;
                    var_10 = indirect_placeholder_14(4261094UL, var_6, 4262656UL, rsi, 0UL);
                    var_11 = (uint32_t)var_10.field_0;
                    local_sp_6_ph = var_9;
                    local_sp_3 = var_9;
                    r12_1_ph = r12_1;
                    local_sp_4 = var_9;
                    local_sp_5 = var_9;
                    local_sp_6 = var_9;
                    if ((uint64_t)(var_11 + 1U) == 0UL) {
                        if ((uint64_t)(var_11 + (-98)) == 0UL) {
                            r12_1 = r12_1_be;
                            continue;
                        }
                        r12_1_be = 1UL;
                        if ((int)var_11 > (int)98U) {
                            if ((uint64_t)(var_11 + (-99)) != 0UL) {
                                loop_state_var = 5U;
                                break;
                            }
                        }
                        if ((uint64_t)(var_11 + 131U) != 0UL) {
                            if ((uint64_t)(var_11 + 130U) != 0UL) {
                                loop_state_var = 4U;
                                break;
                            }
                            *(uint64_t *)(local_sp_6 + (-16L)) = 4204826UL;
                            indirect_placeholder_11(rsi, var_6, 0UL);
                            abort();
                        }
                        var_12 = *(uint64_t *)6378560UL;
                        var_13 = *(uint64_t *)6378752UL;
                        *(uint64_t *)(local_sp_6 + (-16L)) = 4204872UL;
                        indirect_placeholder_6(0UL, 4260986UL, var_13, var_12, 4260851UL, 4261079UL, 0UL);
                        var_14 = local_sp_6 + (-24L);
                        *(uint64_t *)var_14 = 4204882UL;
                        indirect_placeholder();
                        local_sp_5 = var_14;
                        loop_state_var = 4U;
                        break;
                    }
                    var_15 = var_10.field_1;
                    var_16 = var_10.field_2;
                    var_17 = (uint32_t)r12_1;
                    var_18 = (uint64_t)var_17;
                    var_19 = *(uint32_t *)6378684UL;
                    var_20 = rdi - (uint64_t)var_19;
                    var_21 = (uint64_t)(uint32_t)var_20;
                    var_22 = ((uint64_t)var_19 << 3UL) + rsi;
                    var_23 = ((uint64_t)(var_17 + (-2)) == 0UL);
                    var_24 = (r13_0_ph == 0UL);
                    if (var_23 || var_24) {
                        *(uint64_t *)(local_sp_6 + (-16L)) = 4204994UL;
                        indirect_placeholder_6(0UL, 4262400UL, 0UL, var_18, 0UL, var_15, var_16);
                        *(uint64_t *)(local_sp_6 + (-24L)) = 4205004UL;
                        indirect_placeholder_11(var_22, var_21, 1UL);
                        abort();
                    }
                    var_25 = r13_0_ph ^ 1UL;
                    rax_0 = var_25;
                    if ((long)(var_20 << 32UL) > (long)(var_25 << 32UL)) {
                        var_56 = *(uint64_t *)((var_25 << 3UL) + var_22);
                        *(uint64_t *)(local_sp_6 + (-16L)) = 4205029UL;
                        var_57 = indirect_placeholder_12(var_56, var_18);
                        var_58 = var_57.field_0;
                        var_59 = var_57.field_1;
                        var_60 = var_57.field_2;
                        var_61 = local_sp_6 + (-24L);
                        *(uint64_t *)var_61 = 4205057UL;
                        indirect_placeholder_6(0UL, 4261098UL, 0UL, var_58, 0UL, var_59, var_60);
                        local_sp_2 = var_61;
                        if (!var_24) {
                            loop_state_var = 0U;
                            break;
                        }
                        var_62 = local_sp_6 + (-32L);
                        *(uint64_t *)var_62 = 4205089UL;
                        indirect_placeholder();
                        local_sp_2 = var_62;
                        loop_state_var = 0U;
                        break;
                    }
                    if (!var_24) {
                        loop_state_var = 2U;
                        break;
                    }
                    if (!var_23) {
                        loop_state_var = 3U;
                        break;
                    }
                    *(uint64_t *)(local_sp_6 + (-16L)) = 4205176UL;
                    indirect_placeholder();
                    rax_0 = 0UL;
                    if (var_25 != 0UL) {
                        loop_state_var = 1U;
                        break;
                    }
                    if (*(unsigned char *)var_25 != '\x00') {
                        loop_state_var = 1U;
                        break;
                    }
                    *(uint64_t *)(local_sp_6 + (-24L)) = 4205194UL;
                    var_28 = indirect_placeholder_1(var_25);
                    var_29 = local_sp_6 + (-32L);
                    *(uint64_t *)var_29 = 4205210UL;
                    indirect_placeholder();
                    local_sp_3 = var_29;
                    rax_0 = var_28;
                    if ((uint64_t)(uint32_t)var_28 != 0UL) {
                        loop_state_var = 3U;
                        break;
                    }
                    var_30 = local_sp_6 + (-40L);
                    *(uint64_t *)var_30 = 4205233UL;
                    indirect_placeholder();
                    local_sp_3 = var_30;
                    loop_state_var = 3U;
                    break;
                }
            switch_state_var = 0;
            switch (loop_state_var) {
              case 0U:
                {
                    *(uint64_t *)(local_sp_2 + (-8L)) = 4205099UL;
                    indirect_placeholder_11(var_22, var_21, 1UL);
                    abort();
                }
                break;
              case 5U:
                {
                    if ((uint64_t)(var_11 + (-112)) == 0UL) {
                        continue;
                    }
                    loop_state_var = 2U;
                    switch_state_var = 1;
                    break;
                }
                break;
              case 1U:
                {
                    var_31 = local_sp_6 + (-24L);
                    *(uint64_t *)var_31 = 4205270UL;
                    indirect_placeholder_6(0UL, 4262568UL, 1UL, var_18, 0UL, var_15, var_16);
                    local_sp_3 = var_31;
                    loop_state_var = 1U;
                    switch_state_var = 1;
                    break;
                }
                break;
              case 2U:
                {
                    loop_state_var = 0U;
                    switch_state_var = 1;
                    break;
                }
                break;
              case 3U:
                {
                    loop_state_var = 1U;
                    switch_state_var = 1;
                    break;
                }
                break;
              case 4U:
                {
                    loop_state_var = 2U;
                    switch_state_var = 1;
                    break;
                }
                break;
            }
            if (switch_state_var)
                break;
        }
    switch (loop_state_var) {
      case 2U:
        {
            *(uint64_t *)(local_sp_5 + (-8L)) = 4204892UL;
            indirect_placeholder_11(rsi, var_6, 1UL);
            abort();
        }
        break;
      case 1U:
      case 0U:
        {
            switch (loop_state_var) {
              case 1U:
                {
                    *(uint64_t *)(local_sp_3 + (-8L)) = 4205301UL;
                    indirect_placeholder_13(0UL, 6378944UL, 4225085UL, 0UL, 4201776UL);
                    if (var_21 != 0UL) {
                        var_32 = *(uint64_t *)var_22;
                        var_33 = local_sp_3 + (-16L);
                        *(uint64_t *)var_33 = 4205343UL;
                        indirect_placeholder();
                        local_sp_0 = var_33;
                        if ((uint64_t)(uint32_t)rax_0 != 0UL) {
                            var_34 = *(uint64_t *)6378760UL;
                            var_35 = local_sp_3 + (-24L);
                            *(uint64_t *)var_35 = 4205367UL;
                            var_36 = indirect_placeholder_9(var_34, var_32, 4267740UL);
                            local_sp_0 = var_35;
                            if (var_36 != 0UL) {
                                *(uint64_t *)(local_sp_3 + (-32L)) = 4205390UL;
                                var_37 = indirect_placeholder_8(var_32, 0UL, 3UL);
                                var_38 = var_37.field_0;
                                var_39 = var_37.field_1;
                                *(uint64_t *)(local_sp_3 + (-40L)) = 4205398UL;
                                indirect_placeholder();
                                var_40 = (uint64_t)*(uint32_t *)var_38;
                                *(uint64_t *)(local_sp_3 + (-48L)) = 4205423UL;
                                indirect_placeholder_6(0UL, 4267746UL, 0UL, var_38, var_40, 4201776UL, var_39);
                                return;
                            }
                        }
                        var_41 = *(uint64_t *)6378760UL;
                        *(uint64_t *)(local_sp_0 + (-8L)) = 4205446UL;
                        var_42 = indirect_placeholder_10(var_41, var_32);
                        var_43 = local_sp_0 + (-16L);
                        *(uint64_t *)var_43 = 4205456UL;
                        var_44 = indirect_placeholder_1(var_41);
                        local_sp_1 = var_43;
                        rbx_0_in = var_42;
                        if ((uint64_t)(uint32_t)var_44 != 0UL) {
                            *(uint64_t *)(local_sp_0 + (-24L)) = 4205478UL;
                            var_45 = indirect_placeholder_7(var_32, 0UL, 3UL);
                            var_46 = var_45.field_0;
                            var_47 = var_45.field_1;
                            *(uint64_t *)(local_sp_0 + (-32L)) = 4205486UL;
                            indirect_placeholder();
                            var_48 = (uint64_t)*(uint32_t *)var_46;
                            *(uint64_t *)(local_sp_0 + (-40L)) = 4205511UL;
                            indirect_placeholder_6(0UL, 4267746UL, 0UL, var_46, var_48, 4201776UL, var_47);
                            return;
                        }
                    }
                    var_49 = local_sp_3 + (-16L);
                    *(uint64_t *)var_49 = 4205320UL;
                    var_50 = indirect_placeholder_10(0UL, 0UL);
                    local_sp_1 = var_49;
                    rbx_0_in = var_50;
                }
                break;
              case 0U:
                {
                    var_26 = storemerge + (-4263552L);
                    while (var_26 <= 4172UL)
                        {
                            *(uint64_t *)(local_sp_4 + (-8L)) = 4205114UL;
                            indirect_placeholder();
                            var_27 = local_sp_4 + (-16L);
                            *(uint64_t *)var_27 = 4205122UL;
                            indirect_placeholder();
                            local_sp_4 = var_27;
                            storemerge = (var_26 + storemerge) + 1UL;
                            var_26 = storemerge + (-4263552L);
                        }
                    return;
                }
                break;
            }
        }
        break;
    }
}

typedef unsigned char __u_char;
typedef unsigned short __u_short;
typedef unsigned int __u_int;
typedef unsigned long __u_long;
typedef signed char __int8_t;
typedef unsigned char __uint8_t;
typedef short __int16_t;
typedef unsigned short __uint16_t;
typedef int __int32_t;
typedef unsigned int __uint32_t;
typedef long __int64_t;
typedef unsigned long __uint64_t;
typedef __int8_t __int_least8_t;
typedef __uint8_t __uint_least8_t;
typedef __int16_t __int_least16_t;
typedef __uint16_t __uint_least16_t;
typedef __int32_t __int_least32_t;
typedef __uint32_t __uint_least32_t;
typedef __int64_t __int_least64_t;
typedef __uint64_t __uint_least64_t;
typedef long __quad_t;
typedef unsigned long __u_quad_t;
typedef long __intmax_t;
typedef unsigned long __uintmax_t;
typedef unsigned long __dev_t;
typedef unsigned int __uid_t;
typedef unsigned int __gid_t;
typedef unsigned long __ino_t;
typedef unsigned long __ino64_t;
typedef unsigned int __mode_t;
typedef unsigned long __nlink_t;
typedef long __off_t;
typedef long __off64_t;
typedef int __pid_t;
typedef struct {
    int __val[2];
} __fsid_t;
typedef long __clock_t;
typedef unsigned long __rlim_t;
typedef unsigned long __rlim64_t;
typedef unsigned int __id_t;
typedef long __time_t;
typedef unsigned int __useconds_t;
typedef long __suseconds_t;
typedef int __daddr_t;
typedef int __key_t;
typedef int __clockid_t;
typedef void *__timer_t;
typedef long __blksize_t;
typedef long __blkcnt_t;
typedef long __blkcnt64_t;
typedef unsigned long __fsblkcnt_t;
typedef unsigned long __fsblkcnt64_t;
typedef unsigned long __fsfilcnt_t;
typedef unsigned long __fsfilcnt64_t;
typedef long __fsword_t;
typedef long __ssize_t;
typedef long __syscall_slong_t;
typedef unsigned long __syscall_ulong_t;
typedef __off64_t __loff_t;
typedef char *__caddr_t;
typedef long __intptr_t;
typedef unsigned int __socklen_t;
typedef int __sig_atomic_t;
typedef __int8_t int8_t;
typedef __int16_t int16_t;
typedef __int32_t int32_t;
typedef __int64_t int64_t;
typedef __uint8_t uint8_t;
typedef __uint16_t uint16_t;
typedef __uint32_t uint32_t;
typedef __uint64_t uint64_t;
typedef __int_least8_t int_least8_t;
typedef __int_least16_t int_least16_t;
typedef __int_least32_t int_least32_t;
typedef __int_least64_t int_least64_t;
typedef __uint_least8_t uint_least8_t;
typedef __uint_least16_t uint_least16_t;
typedef __uint_least32_t uint_least32_t;
typedef __uint_least64_t uint_least64_t;
typedef signed char int_fast8_t;
typedef long int_fast16_t;
typedef long int_fast32_t;
typedef long int_fast64_t;
typedef unsigned char uint_fast8_t;
typedef unsigned long uint_fast16_t;
typedef unsigned long uint_fast32_t;
typedef unsigned long uint_fast64_t;
typedef long intptr_t;
typedef unsigned long uintptr_t;
typedef __intmax_t intmax_t;
typedef __uintmax_t uintmax_t;
typedef _Bool bool;
struct indirect_placeholder_19_ret_type;
struct indirect_placeholder_20_ret_type;
struct indirect_placeholder_24_ret_type;
struct indirect_placeholder_23_ret_type;
struct indirect_placeholder_25_ret_type;
struct indirect_placeholder_22_ret_type;
struct indirect_placeholder_26_ret_type;
struct indirect_placeholder_21_ret_type;
struct indirect_placeholder_29_ret_type;
struct indirect_placeholder_30_ret_type;
struct indirect_placeholder_31_ret_type;
struct indirect_placeholder_28_ret_type;
struct indirect_placeholder_32_ret_type;
struct indirect_placeholder_27_ret_type;
struct indirect_placeholder_33_ret_type;
struct indirect_placeholder_36_ret_type;
struct indirect_placeholder_35_ret_type;
struct indirect_placeholder_37_ret_type;
struct indirect_placeholder_34_ret_type;
struct indirect_placeholder_39_ret_type;
struct indirect_placeholder_40_ret_type;
struct indirect_placeholder_19_ret_type {
    uint64_t field_0;
    uint64_t field_1;
};
struct indirect_placeholder_20_ret_type {
    uint64_t field_0;
    uint64_t field_1;
    uint64_t field_2;
};
struct indirect_placeholder_24_ret_type {
    uint64_t field_0;
    uint64_t field_1;
    uint64_t field_2;
};
struct indirect_placeholder_23_ret_type {
    uint64_t field_0;
    uint64_t field_1;
};
struct indirect_placeholder_25_ret_type {
    uint64_t field_0;
    uint64_t field_1;
    uint64_t field_2;
};
struct indirect_placeholder_22_ret_type {
    uint64_t field_0;
    uint64_t field_1;
};
struct indirect_placeholder_26_ret_type {
    uint64_t field_0;
    uint64_t field_1;
    uint64_t field_2;
};
struct indirect_placeholder_21_ret_type {
    uint64_t field_0;
    uint64_t field_1;
};
struct indirect_placeholder_29_ret_type {
    uint64_t field_0;
    uint64_t field_1;
};
struct indirect_placeholder_30_ret_type {
    uint64_t field_0;
    uint64_t field_1;
};
struct indirect_placeholder_31_ret_type {
    uint64_t field_0;
    uint64_t field_1;
    uint64_t field_2;
};
struct indirect_placeholder_28_ret_type {
    uint64_t field_0;
    uint64_t field_1;
};
struct indirect_placeholder_32_ret_type {
    uint64_t field_0;
    uint64_t field_1;
    uint64_t field_2;
};
struct indirect_placeholder_27_ret_type {
    uint64_t field_0;
    uint64_t field_1;
};
struct indirect_placeholder_33_ret_type {
    uint64_t field_0;
    uint64_t field_1;
};
struct indirect_placeholder_36_ret_type {
    uint64_t field_0;
    uint64_t field_1;
    uint64_t field_2;
};
struct indirect_placeholder_35_ret_type {
    uint64_t field_0;
    uint64_t field_1;
};
struct indirect_placeholder_37_ret_type {
    uint64_t field_0;
    uint64_t field_1;
    uint64_t field_2;
};
struct indirect_placeholder_34_ret_type {
    uint64_t field_0;
    uint64_t field_1;
};
struct indirect_placeholder_39_ret_type {
    uint64_t field_0;
    uint64_t field_1;
};
struct indirect_placeholder_40_ret_type {
    uint64_t field_0;
    uint64_t field_1;
    uint64_t field_2;
    uint64_t field_3;
};
extern void abort(void);
extern uint64_t revng_init_local_sp(uint64_t param_0);
extern uint64_t init_rbx(void);
extern void indirect_placeholder_2(void);
extern uint64_t init_rbp(void);
extern uint64_t init_r14(void);
extern uint64_t init_r13(void);
extern uint64_t init_r12(void);
extern uint64_t indirect_placeholder(uint64_t param_0);
extern uint64_t helper_cc_compute_all_wrapper(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint32_t param_3);
extern uint64_t init_r15(void);
extern uint64_t indirect_placeholder_11(uint64_t param_0, uint64_t param_1);
extern void indirect_placeholder_16(uint64_t param_0);
extern void indirect_placeholder_13(uint64_t param_0, uint64_t param_1, uint64_t param_2);
extern uint64_t indirect_placeholder_4(uint64_t param_0, uint64_t param_1, uint64_t param_2);
extern void indirect_placeholder_6(uint64_t param_0, uint64_t param_1);
extern struct indirect_placeholder_19_ret_type indirect_placeholder_19(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint64_t param_3, uint64_t param_4, uint64_t param_5, uint64_t param_6);
extern struct indirect_placeholder_20_ret_type indirect_placeholder_20(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint64_t param_3, uint64_t param_4);
extern struct indirect_placeholder_24_ret_type indirect_placeholder_24(uint64_t param_0, uint64_t param_1);
extern struct indirect_placeholder_23_ret_type indirect_placeholder_23(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint64_t param_3, uint64_t param_4, uint64_t param_5, uint64_t param_6);
extern struct indirect_placeholder_25_ret_type indirect_placeholder_25(uint64_t param_0, uint64_t param_1);
extern struct indirect_placeholder_22_ret_type indirect_placeholder_22(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint64_t param_3, uint64_t param_4, uint64_t param_5, uint64_t param_6);
extern struct indirect_placeholder_26_ret_type indirect_placeholder_26(uint64_t param_0, uint64_t param_1);
extern struct indirect_placeholder_21_ret_type indirect_placeholder_21(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint64_t param_3, uint64_t param_4, uint64_t param_5, uint64_t param_6);
extern struct indirect_placeholder_29_ret_type indirect_placeholder_29(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint64_t param_3, uint64_t param_4, uint64_t param_5, uint64_t param_6);
extern struct indirect_placeholder_30_ret_type indirect_placeholder_30(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint64_t param_3, uint64_t param_4, uint64_t param_5, uint64_t param_6);
extern struct indirect_placeholder_31_ret_type indirect_placeholder_31(uint64_t param_0, uint64_t param_1);
extern struct indirect_placeholder_28_ret_type indirect_placeholder_28(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint64_t param_3, uint64_t param_4, uint64_t param_5, uint64_t param_6);
extern struct indirect_placeholder_32_ret_type indirect_placeholder_32(uint64_t param_0, uint64_t param_1);
extern struct indirect_placeholder_27_ret_type indirect_placeholder_27(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint64_t param_3, uint64_t param_4, uint64_t param_5, uint64_t param_6);
extern struct indirect_placeholder_33_ret_type indirect_placeholder_33(uint64_t param_0, uint64_t param_1, uint64_t param_2);
extern struct indirect_placeholder_36_ret_type indirect_placeholder_36(uint64_t param_0, uint64_t param_1);
extern struct indirect_placeholder_35_ret_type indirect_placeholder_35(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint64_t param_3, uint64_t param_4, uint64_t param_5, uint64_t param_6);
extern struct indirect_placeholder_37_ret_type indirect_placeholder_37(uint64_t param_0, uint64_t param_1);
extern struct indirect_placeholder_34_ret_type indirect_placeholder_34(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint64_t param_3, uint64_t param_4, uint64_t param_5, uint64_t param_6);
extern void indirect_placeholder_38(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint64_t param_3, uint64_t param_4, uint64_t param_5, uint64_t param_6);
extern struct indirect_placeholder_39_ret_type indirect_placeholder_39(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint64_t param_3, uint64_t param_4, uint64_t param_5, uint64_t param_6);
extern struct indirect_placeholder_40_ret_type indirect_placeholder_40(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint64_t param_3, uint64_t param_4);
void bb_main(uint64_t rdi, uint64_t rsi) {
    struct indirect_placeholder_40_ret_type var_12;
    struct indirect_placeholder_19_ret_type var_108;
    struct indirect_placeholder_20_ret_type var_110;
    uint32_t rax_1;
    uint64_t var_0;
    uint64_t var_1;
    uint64_t var_2;
    uint64_t var_3;
    uint64_t var_4;
    uint64_t var_5;
    uint64_t var_6;
    uint32_t var_7;
    uint64_t var_8;
    uint64_t var_9;
    uint64_t var_10;
    uint64_t local_sp_17;
    uint64_t local_sp_2;
    uint64_t r8_0;
    uint64_t var_113;
    uint64_t r8_1;
    uint64_t var_107;
    uint64_t r9_3;
    uint64_t local_sp_12;
    uint64_t local_sp_0;
    uint64_t r8_6;
    uint64_t r15_0;
    uint64_t local_sp_1;
    uint64_t var_109;
    uint64_t var_111;
    uint64_t var_112;
    uint64_t rbx_1;
    uint64_t var_114;
    uint64_t rbx_0;
    uint64_t var_115;
    uint64_t var_116;
    uint64_t *var_117;
    uint64_t var_118;
    uint64_t var_119;
    uint64_t var_120;
    uint64_t var_121;
    uint64_t var_122;
    uint64_t var_123;
    uint64_t var_124;
    uint64_t var_125;
    uint64_t var_126;
    uint64_t var_127;
    struct indirect_placeholder_24_ret_type var_82;
    uint64_t var_83;
    uint64_t var_84;
    uint64_t var_85;
    uint64_t var_86;
    struct indirect_placeholder_23_ret_type var_87;
    uint64_t var_88;
    uint64_t local_sp_5;
    uint64_t local_sp_7;
    uint64_t local_sp_6;
    uint64_t var_80;
    uint32_t var_81;
    uint64_t var_63;
    uint64_t var_64;
    uint64_t var_65;
    uint64_t var_66;
    uint64_t rax_0;
    uint64_t local_sp_4;
    uint64_t var_76;
    uint32_t *var_77;
    uint32_t var_78;
    uint64_t var_79;
    uint64_t var_102;
    uint64_t local_sp_11;
    uint64_t r8_2;
    uint64_t r9_0;
    struct indirect_placeholder_25_ret_type var_89;
    uint64_t var_90;
    uint64_t var_91;
    uint64_t var_92;
    uint64_t var_93;
    struct indirect_placeholder_22_ret_type var_94;
    uint64_t r8_3;
    uint64_t r9_1;
    uint64_t var_95;
    struct indirect_placeholder_26_ret_type var_96;
    uint64_t var_97;
    uint64_t var_98;
    uint64_t var_99;
    uint64_t var_100;
    struct indirect_placeholder_21_ret_type var_101;
    uint64_t rbx_3;
    bool var_71;
    uint64_t var_72;
    uint64_t *var_73;
    uint64_t var_74;
    uint64_t var_75;
    uint64_t local_sp_8;
    uint64_t r14_0;
    uint32_t var_42;
    uint32_t var_43;
    uint64_t var_44;
    uint64_t var_45;
    uint64_t var_46;
    uint64_t *var_47;
    uint64_t var_48;
    bool var_49;
    bool var_50;
    uint64_t local_sp_9;
    uint64_t var_51;
    struct indirect_placeholder_30_ret_type var_52;
    uint64_t r8_4;
    bool var_53;
    uint64_t var_54;
    uint64_t var_55;
    struct indirect_placeholder_31_ret_type var_56;
    uint64_t var_57;
    uint64_t var_58;
    uint64_t var_59;
    uint64_t var_60;
    uint64_t var_61;
    struct indirect_placeholder_32_ret_type var_62;
    uint64_t var_67;
    uint64_t var_68;
    uint64_t var_69;
    uint32_t *var_70;
    uint64_t rbp_1;
    uint64_t r12_1;
    uint64_t var_103;
    struct indirect_placeholder_33_ret_type var_104;
    uint64_t var_105;
    uint64_t r13_0;
    uint64_t var_106;
    uint64_t var_27;
    struct indirect_placeholder_36_ret_type var_28;
    uint64_t var_29;
    uint64_t var_30;
    uint64_t var_31;
    uint64_t var_32;
    uint64_t var_33;
    uint64_t local_sp_16;
    uint64_t local_sp_13;
    uint64_t rbx_2;
    uint64_t local_sp_14;
    uint64_t var_34;
    struct indirect_placeholder_37_ret_type var_35;
    uint64_t var_36;
    uint64_t var_37;
    uint64_t var_38;
    uint64_t var_39;
    uint64_t local_sp_17_be;
    uint64_t rbx_3_be;
    uint64_t r14_0_be;
    uint64_t r13_0_be;
    uint64_t r12_1_be;
    uint64_t local_sp_15;
    uint64_t var_18;
    uint64_t var_19;
    uint64_t var_20;
    uint64_t var_21;
    uint64_t var_22;
    uint64_t var_23;
    uint64_t var_24;
    uint64_t var_25;
    uint64_t var_26;
    uint64_t var_40;
    uint64_t var_41;
    uint64_t var_11;
    uint64_t var_13;
    uint64_t var_14;
    uint64_t var_15;
    uint64_t var_16;
    uint32_t var_17;
    unsigned int loop_state_var;
    var_0 = revng_init_local_sp(0UL);
    var_1 = init_rbx();
    var_2 = init_r14();
    var_3 = init_rbp();
    var_4 = init_r13();
    var_5 = init_r12();
    var_6 = init_r15();
    *(uint64_t *)(var_0 + (-8L)) = var_6;
    *(uint64_t *)(var_0 + (-16L)) = var_2;
    *(uint64_t *)(var_0 + (-24L)) = var_4;
    *(uint64_t *)(var_0 + (-32L)) = var_5;
    *(uint64_t *)(var_0 + (-40L)) = var_3;
    *(uint64_t *)(var_0 + (-48L)) = var_1;
    var_7 = (uint32_t)rdi;
    var_8 = (uint64_t)var_7;
    var_9 = *(uint64_t *)rsi;
    *(uint64_t *)(var_0 + (-240L)) = 4206442UL;
    indirect_placeholder_16(var_9);
    *(uint64_t *)(var_0 + (-248L)) = 4206457UL;
    indirect_placeholder_2();
    var_10 = var_0 + (-256L);
    *(uint64_t *)var_10 = 4206467UL;
    indirect_placeholder_2();
    *(unsigned char *)6391320UL = (unsigned char)'\x00';
    *(unsigned char *)6391321UL = (unsigned char)'\x00';
    *(unsigned char *)6391323UL = (unsigned char)'\x00';
    *(unsigned char *)6391322UL = (unsigned char)'\x00';
    *(unsigned char *)6391325UL = (unsigned char)'\x00';
    *(unsigned char *)(var_0 + (-244L)) = (unsigned char)'\x00';
    *(unsigned char *)(var_0 + (-245L)) = (unsigned char)'\x00';
    rax_1 = 0U;
    local_sp_17 = var_10;
    rbx_1 = 0UL;
    rbx_3 = rsi;
    r14_0 = 0UL;
    r12_1 = 0UL;
    r13_0 = 0UL;
    while (1U)
        {
            var_11 = local_sp_17 + (-8L);
            *(uint64_t *)var_11 = 4207158UL;
            var_12 = indirect_placeholder_40(4275092UL, var_8, 4275328UL, 0UL, rbx_3);
            var_13 = var_12.field_0;
            var_14 = var_12.field_1;
            var_15 = var_12.field_2;
            var_16 = var_12.field_3;
            var_17 = (uint32_t)var_13;
            r9_3 = var_16;
            r8_6 = var_15;
            r15_0 = r12_1;
            local_sp_11 = var_11;
            r8_2 = var_15;
            r9_0 = var_16;
            r8_3 = var_15;
            r9_1 = var_16;
            local_sp_9 = var_11;
            r8_4 = var_15;
            local_sp_16 = var_11;
            rbx_2 = rbx_3;
            local_sp_17_be = var_11;
            rbx_3_be = rbx_3;
            r14_0_be = r14_0;
            r13_0_be = r13_0;
            r12_1_be = r12_1;
            local_sp_15 = var_11;
            if ((uint64_t)(var_17 + 1U) != 0UL) {
                var_42 = *(uint32_t *)6391132UL;
                var_43 = var_7 - var_42;
                var_44 = (uint64_t)var_43;
                var_45 = ((uint64_t)var_42 << 3UL) + rbx_3;
                var_46 = local_sp_17 + 8UL;
                var_47 = (uint64_t *)var_46;
                *var_47 = var_45;
                var_48 = helper_cc_compute_all_wrapper(var_44, 0UL, 0UL, 24U);
                rbp_1 = var_44;
                if ((uint64_t)(((unsigned char)(var_48 >> 4UL) ^ (unsigned char)var_48) & '\xc0') == 0UL) {
                    *(uint64_t *)(local_sp_17 + (-16L)) = 4207218UL;
                    indirect_placeholder_29(0UL, 4275109UL, 0UL, var_14, var_15, 0UL, var_16);
                    *(uint64_t *)(local_sp_17 + (-24L)) = 4207228UL;
                    indirect_placeholder_13(rbx_3, var_44, 1UL);
                    abort();
                }
                var_49 = (*(unsigned char *)(local_sp_17 + 4UL) == '\x00');
                var_50 = (r12_1 == 0UL);
                if (!var_49) {
                    if (!var_50) {
                        loop_state_var = 2U;
                        break;
                    }
                    var_51 = local_sp_17 + (-16L);
                    *(uint64_t *)var_51 = 4207269UL;
                    var_52 = indirect_placeholder_30(0UL, 4274592UL, 1UL, var_14, var_15, 0UL, var_16);
                    local_sp_9 = var_51;
                    r8_4 = var_52.field_1;
                    loop_state_var = 2U;
                    break;
                }
                if (!var_50) {
                    loop_state_var = 1U;
                    break;
                }
                r15_0 = 4275851UL;
                if ((int)var_43 <= (int)1U) {
                    loop_state_var = 1U;
                    break;
                }
                var_67 = ((uint64_t)((long)(var_44 << 32UL) >> (long)29UL) + *var_47) + (-8L);
                *(uint64_t *)(local_sp_17 + 16UL) = var_67;
                var_68 = *(uint64_t *)var_67;
                *(uint64_t *)(local_sp_17 + (-16L)) = 4207435UL;
                var_69 = indirect_placeholder(var_68);
                *(uint64_t *)(local_sp_17 + (-24L)) = 4207446UL;
                indirect_placeholder_2();
                var_70 = (uint32_t *)(local_sp_17 + (-12L));
                *var_70 = 1U;
                r15_0 = 0UL;
                if (var_69 != 0UL) {
                    loop_state_var = 3U;
                    break;
                }
                *var_70 = (*(unsigned char *)((var_69 << 1UL) + (-1L)) == '/');
                loop_state_var = 3U;
                break;
            }
            if ((uint64_t)(var_17 + (-98)) == 0UL) {
                var_40 = *(uint64_t *)6391920UL;
                var_41 = (var_40 == 0UL) ? r13_0 : var_40;
                *(unsigned char *)(local_sp_17 + 3UL) = (unsigned char)'\x01';
                r13_0_be = var_41;
            } else {
                if ((int)var_17 > (int)98U) {
                    if ((uint64_t)(var_17 + (-110)) == 0UL) {
                        *(unsigned char *)6391008UL = (unsigned char)'\x00';
                    } else {
                        if ((int)var_17 > (int)110U) {
                            if ((uint64_t)(var_17 + (-115)) == 0UL) {
                                *(unsigned char *)6391325UL = (unsigned char)'\x01';
                            } else {
                                if ((int)var_17 > (int)115U) {
                                    if ((uint64_t)(var_17 + (-116)) == 0UL) {
                                        if (r12_1 == 0UL) {
                                            var_22 = local_sp_17 + (-16L);
                                            *(uint64_t *)var_22 = 4206881UL;
                                            indirect_placeholder_39(0UL, 4274552UL, 1UL, var_14, var_15, 0UL, var_16);
                                            local_sp_16 = var_22;
                                        }
                                        var_23 = local_sp_16 + 32UL;
                                        var_24 = *(uint64_t *)6391920UL;
                                        var_25 = local_sp_16 + (-8L);
                                        *(uint64_t *)var_25 = 4206898UL;
                                        var_26 = indirect_placeholder_11(var_24, var_23);
                                        local_sp_13 = var_25;
                                        if ((uint64_t)(uint32_t)var_26 == 0UL) {
                                            var_27 = *(uint64_t *)6391920UL;
                                            *(uint64_t *)(local_sp_16 + (-16L)) = 4206919UL;
                                            var_28 = indirect_placeholder_36(4UL, var_27);
                                            var_29 = var_28.field_0;
                                            var_30 = var_28.field_1;
                                            var_31 = var_28.field_2;
                                            *(uint64_t *)(local_sp_16 + (-24L)) = 4206927UL;
                                            indirect_placeholder_2();
                                            var_32 = (uint64_t)*(uint32_t *)var_29;
                                            var_33 = local_sp_16 + (-32L);
                                            *(uint64_t *)var_33 = 4206952UL;
                                            indirect_placeholder_35(0UL, 4274774UL, 1UL, var_29, var_30, var_32, var_31);
                                            local_sp_13 = var_33;
                                            rbx_2 = var_29;
                                        }
                                        local_sp_14 = local_sp_13;
                                        rbx_3_be = rbx_2;
                                        if ((uint32_t)((uint16_t)*(uint32_t *)(local_sp_13 + 56UL) & (unsigned short)61440U) != 16384U) {
                                            var_34 = *(uint64_t *)6391920UL;
                                            *(uint64_t *)(local_sp_13 + (-8L)) = 4206985UL;
                                            var_35 = indirect_placeholder_37(4UL, var_34);
                                            var_36 = var_35.field_0;
                                            var_37 = var_35.field_1;
                                            var_38 = var_35.field_2;
                                            var_39 = local_sp_13 + (-16L);
                                            *(uint64_t *)var_39 = 4207013UL;
                                            indirect_placeholder_34(0UL, 4275035UL, 1UL, var_36, var_37, 0UL, var_38);
                                            local_sp_14 = var_39;
                                        }
                                        local_sp_17_be = local_sp_14;
                                        r12_1_be = *(uint64_t *)6391920UL;
                                    } else {
                                        if ((uint64_t)(var_17 + (-118)) != 0UL) {
                                            loop_state_var = 0U;
                                            break;
                                        }
                                        *(unsigned char *)6391321UL = (unsigned char)'\x01';
                                    }
                                } else {
                                    if ((uint64_t)(var_17 + (-114)) != 0UL) {
                                        loop_state_var = 0U;
                                        break;
                                    }
                                    *(unsigned char *)6391324UL = (unsigned char)'\x01';
                                }
                            }
                        } else {
                            if ((uint64_t)(var_17 + (-102)) == 0UL) {
                                *(unsigned char *)6391322UL = (unsigned char)'\x01';
                                *(unsigned char *)6391323UL = (unsigned char)'\x00';
                            } else {
                                if ((uint64_t)(var_17 + (-105)) == 0UL) {
                                    *(unsigned char *)6391322UL = (unsigned char)'\x00';
                                    *(unsigned char *)6391323UL = (unsigned char)'\x01';
                                } else {
                                    if ((uint64_t)(var_17 + (-100)) != 0UL) {
                                        loop_state_var = 0U;
                                        break;
                                    }
                                    *(unsigned char *)6391320UL = (unsigned char)'\x01';
                                }
                            }
                        }
                    }
                } else {
                    if ((uint64_t)(var_17 + (-76)) == 0UL) {
                        *(unsigned char *)6391009UL = (unsigned char)'\x01';
                    } else {
                        if ((int)var_17 > (int)76U) {
                            if ((uint64_t)(var_17 + (-83)) == 0UL) {
                                var_21 = *(uint64_t *)6391920UL;
                                *(unsigned char *)(local_sp_17 + 3UL) = (unsigned char)'\x01';
                                r14_0_be = var_21;
                            } else {
                                if ((uint64_t)(var_17 + (-84)) == 0UL) {
                                    *(unsigned char *)(local_sp_17 + 4UL) = (unsigned char)'\x01';
                                } else {
                                    if ((uint64_t)(var_17 + (-80)) != 0UL) {
                                        loop_state_var = 0U;
                                        break;
                                    }
                                    *(unsigned char *)6391009UL = (unsigned char)'\x00';
                                }
                            }
                        } else {
                            if ((uint64_t)(var_17 + 130U) == 0UL) {
                                *(uint64_t *)(local_sp_17 + (-16L)) = 4207055UL;
                                indirect_placeholder_13(rbx_3, var_8, 0UL);
                                abort();
                            }
                            if ((uint64_t)(var_17 + (-70)) != 0UL) {
                                if ((uint64_t)(var_17 + 131U) != 0UL) {
                                    loop_state_var = 0U;
                                    break;
                                }
                                *(uint64_t *)(local_sp_17 + (-24L)) = 0UL;
                                var_18 = *(uint64_t *)6391016UL;
                                var_19 = *(uint64_t *)6391168UL;
                                *(uint64_t *)(local_sp_17 + (-32L)) = 4207107UL;
                                indirect_placeholder_38(0UL, 4274994UL, var_19, var_18, 4275080UL, 4274893UL, 4275064UL);
                                var_20 = local_sp_17 + (-40L);
                                *(uint64_t *)var_20 = 4207117UL;
                                indirect_placeholder_2();
                                local_sp_15 = var_20;
                                loop_state_var = 0U;
                                break;
                            }
                            *(unsigned char *)6391320UL = (unsigned char)'\x01';
                        }
                    }
                }
            }
            local_sp_17 = local_sp_17_be;
            rbx_3 = rbx_3_be;
            r14_0 = r14_0_be;
            r12_1 = r12_1_be;
            r13_0 = r13_0_be;
            continue;
        }
    switch (loop_state_var) {
      case 0U:
        {
            *(uint64_t *)(local_sp_15 + (-8L)) = 4207127UL;
            indirect_placeholder_13(rbx_3, var_8, 1UL);
            abort();
        }
        break;
      case 2U:
        {
            local_sp_11 = local_sp_9;
            r8_6 = r8_4;
            if ((uint64_t)(var_43 + (-2)) != 0UL) {
                var_53 = ((int)var_43 > (int)1U);
                var_54 = local_sp_9 + 16UL;
                if (var_53) {
                    var_61 = *(uint64_t *)(*(uint64_t *)var_54 + 16UL);
                    *(uint64_t *)(local_sp_9 + (-8L)) = 4207350UL;
                    var_62 = indirect_placeholder_32(4UL, var_61);
                    var_63 = var_62.field_0;
                    var_64 = var_62.field_1;
                    var_65 = var_62.field_2;
                    var_66 = local_sp_9 + (-16L);
                    *(uint64_t *)var_66 = 4207378UL;
                    indirect_placeholder_27(0UL, 4275130UL, 0UL, var_63, var_64, 0UL, var_65);
                    local_sp_8 = var_66;
                } else {
                    var_55 = **(uint64_t **)var_54;
                    *(uint64_t *)(local_sp_9 + (-8L)) = 4207301UL;
                    var_56 = indirect_placeholder_31(4UL, var_55);
                    var_57 = var_56.field_0;
                    var_58 = var_56.field_1;
                    var_59 = var_56.field_2;
                    var_60 = local_sp_9 + (-16L);
                    *(uint64_t *)var_60 = 4207329UL;
                    indirect_placeholder_28(0UL, 4274656UL, 0UL, var_57, var_58, 0UL, var_59);
                    local_sp_8 = var_60;
                }
                *(uint64_t *)(local_sp_8 + (-8L)) = 4207388UL;
                indirect_placeholder_13(rbx_3, var_44, 1UL);
                abort();
            }
            r8_0 = r8_6;
            local_sp_12 = local_sp_11;
            if (*(unsigned char *)(local_sp_11 + 11UL) == '\x00') {
                var_103 = local_sp_11 + (-8L);
                *(uint64_t *)var_103 = 4207749UL;
                var_104 = indirect_placeholder_33(0UL, 4275147UL, r13_0);
                local_sp_12 = var_103;
                rax_1 = (uint32_t)var_104.field_0;
            }
            *(uint32_t *)6391328UL = rax_1;
            var_105 = local_sp_12 + (-8L);
            *(uint64_t *)var_105 = 4207763UL;
            var_106 = indirect_placeholder(r14_0);
            local_sp_0 = var_105;
            if (*(unsigned char *)6391324UL != '\x00' & *(unsigned char *)6391325UL == '\x00') {
                var_107 = local_sp_12 + (-16L);
                *(uint64_t *)var_107 = 4207806UL;
                var_108 = indirect_placeholder_19(0UL, 4274704UL, 1UL, var_106, r8_6, 0UL, r9_3);
                local_sp_0 = var_107;
                r8_0 = var_108.field_1;
            }
            local_sp_1 = local_sp_0;
            r8_1 = r8_0;
            if (r15_0 != 0UL) {
                if ((int)(uint32_t)rbp_1 <= (int)1U) {
                    var_109 = local_sp_0 + (-8L);
                    *(uint64_t *)var_109 = 4207892UL;
                    var_110 = indirect_placeholder_20(4216292UL, 61UL, 4216334UL, 4216449UL, 0UL);
                    var_111 = var_110.field_0;
                    var_112 = var_110.field_1;
                    *(uint64_t *)6391312UL = var_111;
                    local_sp_1 = var_109;
                    r8_1 = var_112;
                    if (*(unsigned char *)6391322UL != '\x00' & *(unsigned char *)6391325UL != '\x00' & *(uint32_t *)6391328UL != 3U & var_111 == 0UL) {
                        var_113 = var_110.field_2;
                        *(uint64_t *)(local_sp_0 + (-16L)) = 4207909UL;
                        indirect_placeholder_6(var_112, var_113);
                        abort();
                    }
                }
                var_114 = helper_cc_compute_all_wrapper(rbp_1, 0UL, 0UL, 24U);
                local_sp_2 = local_sp_1;
                if ((uint64_t)(((unsigned char)(var_114 >> 4UL) ^ (unsigned char)var_114) & '\xc0') != 0UL) {
                    var_115 = *(uint64_t *)(local_sp_1 + 16UL);
                    var_116 = (((rbp_1 << 3UL) + 34359738360UL) & 34359738360UL) + var_115;
                    rbx_0 = var_115;
                    var_117 = (uint64_t *)rbx_0;
                    var_118 = *var_117;
                    *(uint64_t *)(local_sp_2 + (-8L)) = 4207938UL;
                    var_119 = indirect_placeholder(var_118);
                    var_120 = local_sp_2 + 24UL;
                    *(uint64_t *)(local_sp_2 + (-16L)) = 4207954UL;
                    var_121 = indirect_placeholder_4(var_120, r15_0, var_119);
                    var_122 = *(uint64_t *)(local_sp_2 + 16UL);
                    *(uint64_t *)(local_sp_2 + (-24L)) = 4207967UL;
                    indirect_placeholder(var_122);
                    var_123 = *var_117;
                    *(uint64_t *)(local_sp_2 + (-32L)) = 4207978UL;
                    indirect_placeholder_4(var_123, r8_1, var_121);
                    var_124 = local_sp_2 + (-40L);
                    *(uint64_t *)var_124 = 4207988UL;
                    indirect_placeholder_2();
                    local_sp_2 = var_124;
                    while (rbx_0 != var_116)
                        {
                            rbx_0 = rbx_0 + 8UL;
                            var_117 = (uint64_t *)rbx_0;
                            var_118 = *var_117;
                            *(uint64_t *)(local_sp_2 + (-8L)) = 4207938UL;
                            var_119 = indirect_placeholder(var_118);
                            var_120 = local_sp_2 + 24UL;
                            *(uint64_t *)(local_sp_2 + (-16L)) = 4207954UL;
                            var_121 = indirect_placeholder_4(var_120, r15_0, var_119);
                            var_122 = *(uint64_t *)(local_sp_2 + 16UL);
                            *(uint64_t *)(local_sp_2 + (-24L)) = 4207967UL;
                            indirect_placeholder(var_122);
                            var_123 = *var_117;
                            *(uint64_t *)(local_sp_2 + (-32L)) = 4207978UL;
                            indirect_placeholder_4(var_123, r8_1, var_121);
                            var_124 = local_sp_2 + (-40L);
                            *(uint64_t *)var_124 = 4207988UL;
                            indirect_placeholder_2();
                            local_sp_2 = var_124;
                        }
                }
            }
            var_125 = *(uint64_t *)(local_sp_0 + 16UL);
            var_126 = *(uint64_t *)(var_125 + 8UL);
            var_127 = *(uint64_t *)var_125;
            *(uint64_t *)(local_sp_0 + (-8L)) = 4208016UL;
            indirect_placeholder_4(var_127, r8_0, var_126);
        }
        break;
      case 3U:
      case 1U:
        {
            switch (loop_state_var) {
              case 1U:
                {
                    r8_0 = r8_6;
                    local_sp_12 = local_sp_11;
                    if (*(unsigned char *)(local_sp_11 + 11UL) == '\x00') {
                        var_103 = local_sp_11 + (-8L);
                        *(uint64_t *)var_103 = 4207749UL;
                        var_104 = indirect_placeholder_33(0UL, 4275147UL, r13_0);
                        local_sp_12 = var_103;
                        rax_1 = (uint32_t)var_104.field_0;
                    }
                    *(uint32_t *)6391328UL = rax_1;
                    var_105 = local_sp_12 + (-8L);
                    *(uint64_t *)var_105 = 4207763UL;
                    var_106 = indirect_placeholder(r14_0);
                    local_sp_0 = var_105;
                    if (*(unsigned char *)6391324UL != '\x00' & *(unsigned char *)6391325UL == '\x00') {
                        var_107 = local_sp_12 + (-16L);
                        *(uint64_t *)var_107 = 4207806UL;
                        var_108 = indirect_placeholder_19(0UL, 4274704UL, 1UL, var_106, r8_6, 0UL, r9_3);
                        local_sp_0 = var_107;
                        r8_0 = var_108.field_1;
                    }
                    local_sp_1 = local_sp_0;
                    r8_1 = r8_0;
                    if (r15_0 != 0UL) {
                        var_125 = *(uint64_t *)(local_sp_0 + 16UL);
                        var_126 = *(uint64_t *)(var_125 + 8UL);
                        var_127 = *(uint64_t *)var_125;
                        *(uint64_t *)(local_sp_0 + (-8L)) = 4208016UL;
                        indirect_placeholder_4(var_127, r8_0, var_126);
                        return;
                    }
                    if ((int)(uint32_t)rbp_1 <= (int)1U) {
                        var_114 = helper_cc_compute_all_wrapper(rbp_1, 0UL, 0UL, 24U);
                        local_sp_2 = local_sp_1;
                        if ((uint64_t)(((unsigned char)(var_114 >> 4UL) ^ (unsigned char)var_114) & '\xc0') != 0UL) {
                            var_115 = *(uint64_t *)(local_sp_1 + 16UL);
                            var_116 = (((rbp_1 << 3UL) + 34359738360UL) & 34359738360UL) + var_115;
                            rbx_0 = var_115;
                            var_117 = (uint64_t *)rbx_0;
                            var_118 = *var_117;
                            *(uint64_t *)(local_sp_2 + (-8L)) = 4207938UL;
                            var_119 = indirect_placeholder(var_118);
                            var_120 = local_sp_2 + 24UL;
                            *(uint64_t *)(local_sp_2 + (-16L)) = 4207954UL;
                            var_121 = indirect_placeholder_4(var_120, r15_0, var_119);
                            var_122 = *(uint64_t *)(local_sp_2 + 16UL);
                            *(uint64_t *)(local_sp_2 + (-24L)) = 4207967UL;
                            indirect_placeholder(var_122);
                            var_123 = *var_117;
                            *(uint64_t *)(local_sp_2 + (-32L)) = 4207978UL;
                            indirect_placeholder_4(var_123, r8_1, var_121);
                            var_124 = local_sp_2 + (-40L);
                            *(uint64_t *)var_124 = 4207988UL;
                            indirect_placeholder_2();
                            local_sp_2 = var_124;
                            while (rbx_0 != var_116)
                                {
                                    rbx_0 = rbx_0 + 8UL;
                                    var_117 = (uint64_t *)rbx_0;
                                    var_118 = *var_117;
                                    *(uint64_t *)(local_sp_2 + (-8L)) = 4207938UL;
                                    var_119 = indirect_placeholder(var_118);
                                    var_120 = local_sp_2 + 24UL;
                                    *(uint64_t *)(local_sp_2 + (-16L)) = 4207954UL;
                                    var_121 = indirect_placeholder_4(var_120, r15_0, var_119);
                                    var_122 = *(uint64_t *)(local_sp_2 + 16UL);
                                    *(uint64_t *)(local_sp_2 + (-24L)) = 4207967UL;
                                    indirect_placeholder(var_122);
                                    var_123 = *var_117;
                                    *(uint64_t *)(local_sp_2 + (-32L)) = 4207978UL;
                                    indirect_placeholder_4(var_123, r8_1, var_121);
                                    var_124 = local_sp_2 + (-40L);
                                    *(uint64_t *)var_124 = 4207988UL;
                                    indirect_placeholder_2();
                                    local_sp_2 = var_124;
                                }
                        }
                    }
                    if (*(unsigned char *)6391322UL != '\x00') {
                        var_114 = helper_cc_compute_all_wrapper(rbp_1, 0UL, 0UL, 24U);
                        local_sp_2 = local_sp_1;
                        if ((uint64_t)(((unsigned char)(var_114 >> 4UL) ^ (unsigned char)var_114) & '\xc0') != 0UL) {
                            var_115 = *(uint64_t *)(local_sp_1 + 16UL);
                            var_116 = (((rbp_1 << 3UL) + 34359738360UL) & 34359738360UL) + var_115;
                            rbx_0 = var_115;
                            var_117 = (uint64_t *)rbx_0;
                            var_118 = *var_117;
                            *(uint64_t *)(local_sp_2 + (-8L)) = 4207938UL;
                            var_119 = indirect_placeholder(var_118);
                            var_120 = local_sp_2 + 24UL;
                            *(uint64_t *)(local_sp_2 + (-16L)) = 4207954UL;
                            var_121 = indirect_placeholder_4(var_120, r15_0, var_119);
                            var_122 = *(uint64_t *)(local_sp_2 + 16UL);
                            *(uint64_t *)(local_sp_2 + (-24L)) = 4207967UL;
                            indirect_placeholder(var_122);
                            var_123 = *var_117;
                            *(uint64_t *)(local_sp_2 + (-32L)) = 4207978UL;
                            indirect_placeholder_4(var_123, r8_1, var_121);
                            var_124 = local_sp_2 + (-40L);
                            *(uint64_t *)var_124 = 4207988UL;
                            indirect_placeholder_2();
                            local_sp_2 = var_124;
                            while (rbx_0 != var_116)
                                {
                                    rbx_0 = rbx_0 + 8UL;
                                    var_117 = (uint64_t *)rbx_0;
                                    var_118 = *var_117;
                                    *(uint64_t *)(local_sp_2 + (-8L)) = 4207938UL;
                                    var_119 = indirect_placeholder(var_118);
                                    var_120 = local_sp_2 + 24UL;
                                    *(uint64_t *)(local_sp_2 + (-16L)) = 4207954UL;
                                    var_121 = indirect_placeholder_4(var_120, r15_0, var_119);
                                    var_122 = *(uint64_t *)(local_sp_2 + 16UL);
                                    *(uint64_t *)(local_sp_2 + (-24L)) = 4207967UL;
                                    indirect_placeholder(var_122);
                                    var_123 = *var_117;
                                    *(uint64_t *)(local_sp_2 + (-32L)) = 4207978UL;
                                    indirect_placeholder_4(var_123, r8_1, var_121);
                                    var_124 = local_sp_2 + (-40L);
                                    *(uint64_t *)var_124 = 4207988UL;
                                    indirect_placeholder_2();
                                    local_sp_2 = var_124;
                                }
                        }
                        return;
                    }
                    if (*(unsigned char *)6391325UL != '\x00') {
                        var_114 = helper_cc_compute_all_wrapper(rbp_1, 0UL, 0UL, 24U);
                        local_sp_2 = local_sp_1;
                        if ((uint64_t)(((unsigned char)(var_114 >> 4UL) ^ (unsigned char)var_114) & '\xc0') != 0UL) {
                            var_115 = *(uint64_t *)(local_sp_1 + 16UL);
                            var_116 = (((rbp_1 << 3UL) + 34359738360UL) & 34359738360UL) + var_115;
                            rbx_0 = var_115;
                            var_117 = (uint64_t *)rbx_0;
                            var_118 = *var_117;
                            *(uint64_t *)(local_sp_2 + (-8L)) = 4207938UL;
                            var_119 = indirect_placeholder(var_118);
                            var_120 = local_sp_2 + 24UL;
                            *(uint64_t *)(local_sp_2 + (-16L)) = 4207954UL;
                            var_121 = indirect_placeholder_4(var_120, r15_0, var_119);
                            var_122 = *(uint64_t *)(local_sp_2 + 16UL);
                            *(uint64_t *)(local_sp_2 + (-24L)) = 4207967UL;
                            indirect_placeholder(var_122);
                            var_123 = *var_117;
                            *(uint64_t *)(local_sp_2 + (-32L)) = 4207978UL;
                            indirect_placeholder_4(var_123, r8_1, var_121);
                            var_124 = local_sp_2 + (-40L);
                            *(uint64_t *)var_124 = 4207988UL;
                            indirect_placeholder_2();
                            local_sp_2 = var_124;
                            while (rbx_0 != var_116)
                                {
                                    rbx_0 = rbx_0 + 8UL;
                                    var_117 = (uint64_t *)rbx_0;
                                    var_118 = *var_117;
                                    *(uint64_t *)(local_sp_2 + (-8L)) = 4207938UL;
                                    var_119 = indirect_placeholder(var_118);
                                    var_120 = local_sp_2 + 24UL;
                                    *(uint64_t *)(local_sp_2 + (-16L)) = 4207954UL;
                                    var_121 = indirect_placeholder_4(var_120, r15_0, var_119);
                                    var_122 = *(uint64_t *)(local_sp_2 + 16UL);
                                    *(uint64_t *)(local_sp_2 + (-24L)) = 4207967UL;
                                    indirect_placeholder(var_122);
                                    var_123 = *var_117;
                                    *(uint64_t *)(local_sp_2 + (-32L)) = 4207978UL;
                                    indirect_placeholder_4(var_123, r8_1, var_121);
                                    var_124 = local_sp_2 + (-40L);
                                    *(uint64_t *)var_124 = 4207988UL;
                                    indirect_placeholder_2();
                                    local_sp_2 = var_124;
                                }
                        }
                        return;
                    }
                    if (*(uint32_t *)6391328UL != 3U) {
                        var_109 = local_sp_0 + (-8L);
                        *(uint64_t *)var_109 = 4207892UL;
                        var_110 = indirect_placeholder_20(4216292UL, 61UL, 4216334UL, 4216449UL, 0UL);
                        var_111 = var_110.field_0;
                        var_112 = var_110.field_1;
                        *(uint64_t *)6391312UL = var_111;
                        local_sp_1 = var_109;
                        r8_1 = var_112;
                        if (var_111 == 0UL) {
                            var_113 = var_110.field_2;
                            *(uint64_t *)(local_sp_0 + (-16L)) = 4207909UL;
                            indirect_placeholder_6(var_112, var_113);
                            abort();
                        }
                    }
                    var_114 = helper_cc_compute_all_wrapper(rbp_1, 0UL, 0UL, 24U);
                    local_sp_2 = local_sp_1;
                    if ((uint64_t)(((unsigned char)(var_114 >> 4UL) ^ (unsigned char)var_114) & '\xc0') != 0UL) {
                        var_115 = *(uint64_t *)(local_sp_1 + 16UL);
                        var_116 = (((rbp_1 << 3UL) + 34359738360UL) & 34359738360UL) + var_115;
                        rbx_0 = var_115;
                        var_117 = (uint64_t *)rbx_0;
                        var_118 = *var_117;
                        *(uint64_t *)(local_sp_2 + (-8L)) = 4207938UL;
                        var_119 = indirect_placeholder(var_118);
                        var_120 = local_sp_2 + 24UL;
                        *(uint64_t *)(local_sp_2 + (-16L)) = 4207954UL;
                        var_121 = indirect_placeholder_4(var_120, r15_0, var_119);
                        var_122 = *(uint64_t *)(local_sp_2 + 16UL);
                        *(uint64_t *)(local_sp_2 + (-24L)) = 4207967UL;
                        indirect_placeholder(var_122);
                        var_123 = *var_117;
                        *(uint64_t *)(local_sp_2 + (-32L)) = 4207978UL;
                        indirect_placeholder_4(var_123, r8_1, var_121);
                        var_124 = local_sp_2 + (-40L);
                        *(uint64_t *)var_124 = 4207988UL;
                        indirect_placeholder_2();
                        local_sp_2 = var_124;
                        while (rbx_0 != var_116)
                            {
                                rbx_0 = rbx_0 + 8UL;
                                var_117 = (uint64_t *)rbx_0;
                                var_118 = *var_117;
                                *(uint64_t *)(local_sp_2 + (-8L)) = 4207938UL;
                                var_119 = indirect_placeholder(var_118);
                                var_120 = local_sp_2 + 24UL;
                                *(uint64_t *)(local_sp_2 + (-16L)) = 4207954UL;
                                var_121 = indirect_placeholder_4(var_120, r15_0, var_119);
                                var_122 = *(uint64_t *)(local_sp_2 + 16UL);
                                *(uint64_t *)(local_sp_2 + (-24L)) = 4207967UL;
                                indirect_placeholder(var_122);
                                var_123 = *var_117;
                                *(uint64_t *)(local_sp_2 + (-32L)) = 4207978UL;
                                indirect_placeholder_4(var_123, r8_1, var_121);
                                var_124 = local_sp_2 + (-40L);
                                *(uint64_t *)var_124 = 4207988UL;
                                indirect_placeholder_2();
                                local_sp_2 = var_124;
                            }
                    }
                    return;
                }
                break;
              case 3U:
                {
                    var_71 = (*(unsigned char *)6391008UL == '\x00');
                    var_72 = local_sp_17 + (-32L);
                    var_73 = (uint64_t *)var_72;
                    local_sp_4 = var_72;
                    if (var_71) {
                        *var_73 = 4207511UL;
                        var_75 = indirect_placeholder_11(var_68, var_46);
                        rax_0 = var_75;
                    } else {
                        *var_73 = 4207496UL;
                        var_74 = indirect_placeholder_11(var_68, var_46);
                        rax_0 = var_74;
                    }
                    var_76 = var_72 + (-8L);
                    *(uint64_t *)var_76 = 4207520UL;
                    indirect_placeholder_2();
                    var_77 = (uint32_t *)rax_0;
                    var_78 = *var_77;
                    var_79 = (uint64_t)var_78;
                    local_sp_4 = var_76;
                    rbx_1 = var_79;
                    if ((uint64_t)(uint32_t)rax_0 == 0UL & var_78 != 0U) {
                        var_80 = var_72 + (-16L);
                        *(uint64_t *)var_80 = 4208025UL;
                        indirect_placeholder_2();
                        var_81 = *var_77;
                        local_sp_6 = var_80;
                        local_sp_7 = var_80;
                        if ((uint64_t)(var_81 + (-2)) == 0UL) {
                            if ((uint64_t)(var_81 + (-40)) == 0UL) {
                                if ((uint64_t)((var_81 + (-20)) & (-17)) == 0UL) {
                                    if ((*(unsigned char *)(var_72 + (-4L)) & '\x01') == '\x00') {
                                        *(uint64_t *)(local_sp_6 + (-8L)) = 4207637UL;
                                        var_89 = indirect_placeholder_25(4UL, var_68);
                                        var_90 = var_89.field_0;
                                        var_91 = var_89.field_1;
                                        var_92 = var_89.field_2;
                                        var_93 = local_sp_6 + (-16L);
                                        *(uint64_t *)var_93 = 4207662UL;
                                        var_94 = indirect_placeholder_22(0UL, 4275035UL, 1UL, var_90, var_91, rbx_1, var_92);
                                        local_sp_7 = var_93;
                                        r8_3 = var_94.field_1;
                                        r9_1 = var_92;
                                    }
                                } else {
                                    *(uint64_t *)(var_72 + (-24L)) = 4207587UL;
                                    var_82 = indirect_placeholder_24(4UL, var_68);
                                    var_83 = var_82.field_0;
                                    var_84 = var_82.field_1;
                                    var_85 = var_82.field_2;
                                    var_86 = var_72 + (-32L);
                                    *(uint64_t *)var_86 = 4207612UL;
                                    var_87 = indirect_placeholder_23(0UL, 4274774UL, 1UL, var_83, var_84, var_79, var_85);
                                    var_88 = var_87.field_1;
                                    local_sp_5 = var_86;
                                    r8_2 = var_88;
                                    r9_0 = var_85;
                                    local_sp_6 = local_sp_5;
                                    local_sp_7 = local_sp_5;
                                    r8_3 = r8_2;
                                    r9_1 = r9_0;
                                    if ((*(unsigned char *)(local_sp_5 + 12UL) & '\x01') == '\x00') {
                                        *(uint64_t *)(local_sp_6 + (-8L)) = 4207637UL;
                                        var_89 = indirect_placeholder_25(4UL, var_68);
                                        var_90 = var_89.field_0;
                                        var_91 = var_89.field_1;
                                        var_92 = var_89.field_2;
                                        var_93 = local_sp_6 + (-16L);
                                        *(uint64_t *)var_93 = 4207662UL;
                                        var_94 = indirect_placeholder_22(0UL, 4275035UL, 1UL, var_90, var_91, rbx_1, var_92);
                                        local_sp_7 = var_93;
                                        r8_3 = var_94.field_1;
                                        r9_1 = var_92;
                                    }
                                }
                            } else {
                                if ((*(unsigned char *)(var_72 + (-4L)) & '\x01') == '\x00') {
                                    *(uint64_t *)(local_sp_6 + (-8L)) = 4207637UL;
                                    var_89 = indirect_placeholder_25(4UL, var_68);
                                    var_90 = var_89.field_0;
                                    var_91 = var_89.field_1;
                                    var_92 = var_89.field_2;
                                    var_93 = local_sp_6 + (-16L);
                                    *(uint64_t *)var_93 = 4207662UL;
                                    var_94 = indirect_placeholder_22(0UL, 4275035UL, 1UL, var_90, var_91, rbx_1, var_92);
                                    local_sp_7 = var_93;
                                    r8_3 = var_94.field_1;
                                    r9_1 = var_92;
                                }
                            }
                        } else {
                            if ((*(unsigned char *)(var_72 + (-4L)) & '\x01') == '\x00') {
                                *(uint64_t *)(local_sp_6 + (-8L)) = 4207637UL;
                                var_89 = indirect_placeholder_25(4UL, var_68);
                                var_90 = var_89.field_0;
                                var_91 = var_89.field_1;
                                var_92 = var_89.field_2;
                                var_93 = local_sp_6 + (-16L);
                                *(uint64_t *)var_93 = 4207662UL;
                                var_94 = indirect_placeholder_22(0UL, 4275035UL, 1UL, var_90, var_91, rbx_1, var_92);
                                local_sp_7 = var_93;
                                r8_3 = var_94.field_1;
                                r9_1 = var_92;
                            }
                        }
                        local_sp_11 = local_sp_7;
                        r8_6 = r8_3;
                        r9_3 = r9_1;
                        if ((int)var_43 > (int)2U) {
                            var_95 = **(uint64_t **)(local_sp_7 + 24UL);
                            *(uint64_t *)(local_sp_7 + (-8L)) = 4207685UL;
                            var_96 = indirect_placeholder_26(4UL, var_95);
                            var_97 = var_96.field_0;
                            var_98 = var_96.field_1;
                            var_99 = var_96.field_2;
                            var_100 = local_sp_7 + (-16L);
                            *(uint64_t *)var_100 = 4207713UL;
                            var_101 = indirect_placeholder_21(0UL, 4275035UL, 1UL, var_97, var_98, 0UL, var_99);
                            local_sp_11 = var_100;
                            r15_0 = var_68;
                            r8_6 = var_101.field_1;
                            r9_3 = var_99;
                        }
                    } else {
                        local_sp_5 = local_sp_4;
                        local_sp_11 = local_sp_4;
                        if ((uint32_t)((uint16_t)*(uint32_t *)(local_sp_4 + 56UL) & (unsigned short)61440U) == 16384U) {
                            var_102 = (uint64_t)(var_43 + (-1));
                            rbp_1 = var_102;
                            r15_0 = *(uint64_t *)((uint64_t)((long)(var_102 << 32UL) >> (long)29UL) + *(uint64_t *)(local_sp_4 + 16UL));
                        } else {
                            local_sp_6 = local_sp_5;
                            local_sp_7 = local_sp_5;
                            r8_3 = r8_2;
                            r9_1 = r9_0;
                            if ((*(unsigned char *)(local_sp_5 + 12UL) & '\x01') == '\x00') {
                                *(uint64_t *)(local_sp_6 + (-8L)) = 4207637UL;
                                var_89 = indirect_placeholder_25(4UL, var_68);
                                var_90 = var_89.field_0;
                                var_91 = var_89.field_1;
                                var_92 = var_89.field_2;
                                var_93 = local_sp_6 + (-16L);
                                *(uint64_t *)var_93 = 4207662UL;
                                var_94 = indirect_placeholder_22(0UL, 4275035UL, 1UL, var_90, var_91, rbx_1, var_92);
                                local_sp_7 = var_93;
                                r8_3 = var_94.field_1;
                                r9_1 = var_92;
                            }
                            local_sp_11 = local_sp_7;
                            r8_6 = r8_3;
                            r9_3 = r9_1;
                            if ((int)var_43 > (int)2U) {
                                var_95 = **(uint64_t **)(local_sp_7 + 24UL);
                                *(uint64_t *)(local_sp_7 + (-8L)) = 4207685UL;
                                var_96 = indirect_placeholder_26(4UL, var_95);
                                var_97 = var_96.field_0;
                                var_98 = var_96.field_1;
                                var_99 = var_96.field_2;
                                var_100 = local_sp_7 + (-16L);
                                *(uint64_t *)var_100 = 4207713UL;
                                var_101 = indirect_placeholder_21(0UL, 4275035UL, 1UL, var_97, var_98, 0UL, var_99);
                                local_sp_11 = var_100;
                                r15_0 = var_68;
                                r8_6 = var_101.field_1;
                                r9_3 = var_99;
                            }
                        }
                    }
                }
                break;
            }
        }
        break;
    }
}

typedef unsigned char __u_char;
typedef unsigned short __u_short;
typedef unsigned int __u_int;
typedef unsigned long __u_long;
typedef signed char __int8_t;
typedef unsigned char __uint8_t;
typedef short __int16_t;
typedef unsigned short __uint16_t;
typedef int __int32_t;
typedef unsigned int __uint32_t;
typedef long __int64_t;
typedef unsigned long __uint64_t;
typedef __int8_t __int_least8_t;
typedef __uint8_t __uint_least8_t;
typedef __int16_t __int_least16_t;
typedef __uint16_t __uint_least16_t;
typedef __int32_t __int_least32_t;
typedef __uint32_t __uint_least32_t;
typedef __int64_t __int_least64_t;
typedef __uint64_t __uint_least64_t;
typedef long __quad_t;
typedef unsigned long __u_quad_t;
typedef long __intmax_t;
typedef unsigned long __uintmax_t;
typedef unsigned long __dev_t;
typedef unsigned int __uid_t;
typedef unsigned int __gid_t;
typedef unsigned long __ino_t;
typedef unsigned long __ino64_t;
typedef unsigned int __mode_t;
typedef unsigned long __nlink_t;
typedef long __off_t;
typedef long __off64_t;
typedef int __pid_t;
typedef struct {
    int __val[2];
} __fsid_t;
typedef long __clock_t;
typedef unsigned long __rlim_t;
typedef unsigned long __rlim64_t;
typedef unsigned int __id_t;
typedef long __time_t;
typedef unsigned int __useconds_t;
typedef long __suseconds_t;
typedef int __daddr_t;
typedef int __key_t;
typedef int __clockid_t;
typedef void *__timer_t;
typedef long __blksize_t;
typedef long __blkcnt_t;
typedef long __blkcnt64_t;
typedef unsigned long __fsblkcnt_t;
typedef unsigned long __fsblkcnt64_t;
typedef unsigned long __fsfilcnt_t;
typedef unsigned long __fsfilcnt64_t;
typedef long __fsword_t;
typedef long __ssize_t;
typedef long __syscall_slong_t;
typedef unsigned long __syscall_ulong_t;
typedef __off64_t __loff_t;
typedef char *__caddr_t;
typedef long __intptr_t;
typedef unsigned int __socklen_t;
typedef int __sig_atomic_t;
typedef __int8_t int8_t;
typedef __int16_t int16_t;
typedef __int32_t int32_t;
typedef __int64_t int64_t;
typedef __uint8_t uint8_t;
typedef __uint16_t uint16_t;
typedef __uint32_t uint32_t;
typedef __uint64_t uint64_t;
typedef __int_least8_t int_least8_t;
typedef __int_least16_t int_least16_t;
typedef __int_least32_t int_least32_t;
typedef __int_least64_t int_least64_t;
typedef __uint_least8_t uint_least8_t;
typedef __uint_least16_t uint_least16_t;
typedef __uint_least32_t uint_least32_t;
typedef __uint_least64_t uint_least64_t;
typedef signed char int_fast8_t;
typedef long int_fast16_t;
typedef long int_fast32_t;
typedef long int_fast64_t;
typedef unsigned char uint_fast8_t;
typedef unsigned long uint_fast16_t;
typedef unsigned long uint_fast32_t;
typedef unsigned long uint_fast64_t;
typedef long intptr_t;
typedef unsigned long uintptr_t;
typedef __intmax_t intmax_t;
typedef __uintmax_t uintmax_t;
typedef _Bool bool;
struct indirect_placeholder_14_ret_type;
struct indirect_placeholder_14_ret_type {
    uint64_t field_0;
    uint64_t field_1;
    uint64_t field_2;
};
extern void abort(void);
extern uint64_t revng_init_local_sp(uint64_t param_0);
extern uint64_t init_rbx(void);
extern void indirect_placeholder(void);
extern uint64_t init_rbp(void);
extern uint64_t init_r12(void);
extern uint64_t indirect_placeholder_2(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint64_t param_3, uint64_t param_4);
extern void indirect_placeholder_13(uint64_t param_0);
extern uint64_t init_r9(void);
extern void indirect_placeholder_3(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint64_t param_3, uint64_t param_4, uint64_t param_5, uint64_t param_6);
extern void indirect_placeholder_5(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint64_t param_3);
extern struct indirect_placeholder_14_ret_type indirect_placeholder_14(uint64_t param_0);
void bb_main(uint64_t rdi, uint64_t rsi) {
    uint64_t rbx_1;
    uint64_t local_sp_3;
    uint64_t local_sp_2;
    uint64_t rbx_2;
    uint64_t var_36;
    uint64_t rbx_3;
    uint64_t local_sp_4;
    uint64_t var_37;
    uint64_t var_38;
    uint64_t local_sp_10;
    uint64_t local_sp_5;
    uint64_t var_39;
    uint64_t var_40;
    uint64_t local_sp_6;
    uint64_t var_41;
    uint64_t var_42;
    uint64_t var_20;
    uint64_t var_10;
    uint64_t var_0;
    uint64_t var_1;
    uint64_t var_2;
    uint64_t var_3;
    uint64_t var_4;
    uint32_t var_5;
    uint64_t var_6;
    uint64_t var_7;
    uint64_t var_8;
    uint64_t local_sp_15;
    uint64_t rbx_7;
    uint64_t local_sp_12;
    uint64_t local_sp_0;
    uint64_t rbx_5;
    uint64_t rax_0;
    uint64_t rcx_1;
    uint64_t rbx_0;
    uint64_t rcx_0;
    uint64_t var_34;
    uint64_t var_35;
    uint64_t local_sp_1;
    uint64_t local_sp_7;
    uint64_t var_43;
    uint64_t var_44;
    uint32_t var_16;
    uint64_t var_17;
    uint64_t local_sp_13;
    uint64_t rbx_4;
    bool var_45;
    bool _not;
    uint64_t local_sp_8;
    uint64_t var_46;
    uint64_t local_sp_9;
    uint64_t var_47;
    uint64_t var_48;
    uint64_t var_18;
    uint64_t local_sp_11;
    uint64_t var_12;
    uint64_t var_13;
    uint64_t var_14;
    uint64_t var_15;
    uint64_t rax_1;
    uint64_t rbx_7_be;
    uint32_t var_26;
    uint64_t var_27;
    uint64_t var_33;
    uint64_t rax_2;
    uint64_t rbx_6;
    uint64_t var_28;
    struct indirect_placeholder_14_ret_type var_29;
    uint64_t var_30;
    uint64_t var_31;
    uint64_t var_32;
    uint64_t local_sp_14;
    uint64_t var_22;
    uint64_t var_23;
    uint64_t var_24;
    uint64_t var_25;
    uint64_t var_9;
    uint32_t var_11;
    uint64_t var_19;
    uint32_t var_21;
    unsigned int loop_state_var;
    var_0 = revng_init_local_sp(0UL);
    var_1 = init_rbx();
    var_2 = init_rbp();
    var_3 = init_r12();
    var_4 = init_r9();
    *(uint64_t *)(var_0 + (-8L)) = var_3;
    *(uint64_t *)(var_0 + (-16L)) = var_2;
    *(uint64_t *)(var_0 + (-24L)) = var_1;
    var_5 = (uint32_t)rdi;
    var_6 = (uint64_t)var_5;
    var_7 = *(uint64_t *)rsi;
    *(uint64_t *)(var_0 + (-432L)) = 4201751UL;
    indirect_placeholder_13(var_7);
    *(uint64_t *)(var_0 + (-440L)) = 4201766UL;
    indirect_placeholder();
    var_8 = var_0 + (-448L);
    *(uint64_t *)var_8 = 4201776UL;
    indirect_placeholder();
    rbx_2 = 1UL;
    local_sp_15 = var_8;
    rbx_7 = 0UL;
    rbx_5 = 16UL;
    rcx_1 = 4247872UL;
    rcx_0 = 4247872UL;
    rbx_7_be = 4294967295UL;
    rbx_6 = var_1;
    if (*(uint32_t *)6355488UL == 2U) {
        while (1U)
            {
                var_9 = local_sp_15 + (-8L);
                *(uint64_t *)var_9 = 4202210UL;
                var_10 = indirect_placeholder_2(4246434UL, var_6, 4247872UL, rsi, 0UL);
                var_11 = (uint32_t)var_10;
                local_sp_15 = var_9;
                local_sp_12 = var_9;
                rbx_5 = rbx_7;
                rbx_0 = rbx_7;
                local_sp_7 = var_9;
                local_sp_13 = var_9;
                rbx_4 = rbx_7;
                local_sp_11 = var_9;
                rbx_6 = rbx_7;
                if ((uint64_t)(var_11 + 1U) != 0UL) {
                    var_16 = *(uint32_t *)6355612UL;
                    var_17 = (uint64_t)var_16;
                    rax_0 = var_17;
                    rax_1 = var_17;
                    rax_2 = var_17;
                    if ((uint64_t)(var_5 - var_16) != 0UL) {
                        loop_state_var = 1U;
                        break;
                    }
                    if ((uint64_t)(uint32_t)rbx_7 != 0UL) {
                        var_18 = local_sp_15 + (-16L);
                        *(uint64_t *)var_18 = 4202512UL;
                        indirect_placeholder();
                        local_sp_0 = var_18;
                        local_sp_2 = var_18;
                        if ((uint64_t)(var_16 + 1U) != 0UL) {
                            loop_state_var = 4U;
                            break;
                        }
                        loop_state_var = 0U;
                        break;
                    }
                    if ((rbx_7 & 31UL) != 0UL) {
                        loop_state_var = 5U;
                        break;
                    }
                    loop_state_var = 2U;
                    break;
                }
                if ((uint64_t)(var_11 + (-110)) == 0UL) {
                    rbx_7_be = (uint64_t)((uint32_t)rbx_7 & (-3)) | 2UL;
                } else {
                    if ((int)var_11 > (int)110U) {
                        if ((uint64_t)(var_11 + (-114)) == 0UL) {
                            rbx_7_be = (uint64_t)((uint32_t)rbx_7 & (-5)) | 4UL;
                        } else {
                            if ((int)var_11 > (int)114U) {
                                if ((uint64_t)(var_11 + (-115)) == 0UL) {
                                    rbx_7_be = (uint64_t)((uint32_t)rbx_7 & (-2)) | 1UL;
                                } else {
                                    if ((uint64_t)(var_11 + (-118)) != 0UL) {
                                        loop_state_var = 3U;
                                        break;
                                    }
                                    rbx_7_be = (uint64_t)((uint32_t)rbx_7 & (-9)) | 8UL;
                                }
                            } else {
                                if ((uint64_t)(var_11 + (-111)) == 0UL) {
                                    rbx_7_be = rbx_7 | 128UL;
                                } else {
                                    if ((uint64_t)(var_11 + (-112)) != 0UL) {
                                        loop_state_var = 3U;
                                        break;
                                    }
                                    rbx_7_be = (uint64_t)((uint32_t)rbx_7 & (-33)) | 32UL;
                                }
                            }
                        }
                    } else {
                        if ((uint64_t)(var_11 + (-97)) != 0UL) {
                            if ((int)var_11 <= (int)97U) {
                                if ((uint64_t)(var_11 + 131U) == 0UL) {
                                    if ((uint64_t)(var_11 + 130U) != 0UL) {
                                        loop_state_var = 3U;
                                        break;
                                    }
                                    *(uint64_t *)(local_sp_15 + (-16L)) = 4202096UL;
                                    indirect_placeholder_5(rbx_7, rsi, 0UL, var_6);
                                    abort();
                                }
                                var_12 = (*(uint32_t *)6355488UL == 1U) ? 4246208UL : 4246214UL;
                                var_13 = *(uint64_t *)6355496UL;
                                var_14 = *(uint64_t *)6355648UL;
                                *(uint64_t *)(local_sp_15 + (-16L)) = 4202158UL;
                                indirect_placeholder_3(0UL, 4246367UL, var_14, var_13, var_12, 0UL, 4246418UL);
                                var_15 = local_sp_15 + (-24L);
                                *(uint64_t *)var_15 = 4202168UL;
                                indirect_placeholder();
                                local_sp_11 = var_15;
                                loop_state_var = 3U;
                                break;
                            }
                            if ((uint64_t)(var_11 + (-105)) == 0UL) {
                                rbx_7_be = (uint64_t)((uint32_t)rbx_7 & (-65)) | 64UL;
                            } else {
                                if ((uint64_t)(var_11 + (-109)) != 0UL) {
                                    loop_state_var = 3U;
                                    break;
                                }
                                rbx_7_be = (uint64_t)((uint32_t)rbx_7 & (-17)) | 16UL;
                            }
                        }
                    }
                }
                rbx_7 = rbx_7_be;
                continue;
            }
        switch (loop_state_var) {
          case 1U:
            {
                var_28 = *(uint64_t *)((uint64_t)((long)(rax_2 << 32UL) >> (long)29UL) + rsi);
                *(uint64_t *)(local_sp_13 + (-8L)) = 4202242UL;
                var_29 = indirect_placeholder_14(var_28);
                var_30 = var_29.field_0;
                var_31 = var_29.field_1;
                var_32 = var_29.field_2;
                *(uint64_t *)(local_sp_13 + (-16L)) = 4202270UL;
                indirect_placeholder_3(0UL, 4246444UL, 0UL, var_30, 0UL, var_31, var_32);
                *(uint64_t *)(local_sp_13 + (-24L)) = 4202280UL;
                indirect_placeholder_5(rbx_6, rsi, 1UL, var_6);
                abort();
            }
            break;
          case 3U:
            {
                *(uint64_t *)(local_sp_11 + (-8L)) = 4202178UL;
                indirect_placeholder_5(rbx_7, rsi, 1UL, var_6);
                abort();
            }
            break;
          case 5U:
          case 4U:
          case 2U:
          case 0U:
            {
                switch (loop_state_var) {
                  case 4U:
                  case 2U:
                  case 0U:
                    {
                        switch (loop_state_var) {
                          case 4U:
                            {
                                var_36 = local_sp_2 + (-8L);
                                *(uint64_t *)var_36 = 4202346UL;
                                indirect_placeholder_13(local_sp_2);
                                local_sp_3 = var_36;
                                rbx_3 = rbx_2;
                            }
                            break;
                          case 2U:
                          case 0U:
                            {
                                switch (loop_state_var) {
                                  case 0U:
                                    {
                                        *(uint64_t *)(local_sp_0 + (-8L)) = 4202311UL;
                                        indirect_placeholder();
                                        var_34 = (uint64_t)*(uint32_t *)rax_0;
                                        var_35 = local_sp_0 + (-16L);
                                        *(uint64_t *)var_35 = 4202333UL;
                                        indirect_placeholder_3(0UL, 4246461UL, 1UL, rcx_0, var_34, var_4, 0UL);
                                        local_sp_1 = var_35;
                                        rbx_1 = rbx_0;
                                    }
                                    break;
                                  case 2U:
                                    {
                                        var_33 = local_sp_12 + (-8L);
                                        *(uint64_t *)var_33 = 4202301UL;
                                        indirect_placeholder();
                                        rbx_1 = rbx_5;
                                        local_sp_0 = var_33;
                                        rax_0 = rax_1;
                                        rbx_0 = rbx_5;
                                        rcx_0 = rcx_1;
                                        local_sp_1 = var_33;
                                        if ((uint64_t)((uint32_t)rax_1 + 1U) == 0UL) {
                                            *(uint64_t *)(local_sp_0 + (-8L)) = 4202311UL;
                                            indirect_placeholder();
                                            var_34 = (uint64_t)*(uint32_t *)rax_0;
                                            var_35 = local_sp_0 + (-16L);
                                            *(uint64_t *)var_35 = 4202333UL;
                                            indirect_placeholder_3(0UL, 4246461UL, 1UL, rcx_0, var_34, var_4, 0UL);
                                            local_sp_1 = var_35;
                                            rbx_1 = rbx_0;
                                        }
                                    }
                                    break;
                                }
                                local_sp_2 = local_sp_1;
                                rbx_2 = rbx_1;
                                local_sp_3 = local_sp_1;
                                rbx_3 = rbx_1;
                                if ((rbx_1 & 1UL) == 0UL) {
                                    var_36 = local_sp_2 + (-8L);
                                    *(uint64_t *)var_36 = 4202346UL;
                                    indirect_placeholder_13(local_sp_2);
                                    local_sp_3 = var_36;
                                    rbx_3 = rbx_2;
                                }
                            }
                            break;
                        }
                        local_sp_4 = local_sp_3;
                        rbx_4 = rbx_3;
                        if ((rbx_3 & 2UL) == 0UL) {
                            var_37 = local_sp_3 + 65UL;
                            var_38 = local_sp_3 + (-8L);
                            *(uint64_t *)var_38 = 4202361UL;
                            indirect_placeholder_13(var_37);
                            local_sp_4 = var_38;
                        }
                        local_sp_5 = local_sp_4;
                        if ((rbx_3 & 4UL) == 0UL) {
                            var_39 = local_sp_4 + 130UL;
                            var_40 = local_sp_4 + (-8L);
                            *(uint64_t *)var_40 = 4202379UL;
                            indirect_placeholder_13(var_39);
                            local_sp_5 = var_40;
                        }
                        local_sp_6 = local_sp_5;
                        if ((rbx_3 & 8UL) == 0UL) {
                            var_41 = local_sp_5 + 195UL;
                            var_42 = local_sp_5 + (-8L);
                            *(uint64_t *)var_42 = 4202397UL;
                            indirect_placeholder_13(var_41);
                            local_sp_6 = var_42;
                        }
                        local_sp_7 = local_sp_6;
                        if ((rbx_3 & 16UL) == 0UL) {
                            var_43 = local_sp_6 + 260UL;
                            var_44 = local_sp_6 + (-8L);
                            *(uint64_t *)var_44 = 4202415UL;
                            indirect_placeholder_13(var_43);
                            local_sp_7 = var_44;
                        }
                        var_45 = ((rbx_4 & 32UL) == 0UL);
                        _not = (((uint64_t)((uint32_t)rbx_4 + 1U) != 0UL) ^ 1);
                        local_sp_8 = local_sp_7;
                        if (var_45 || _not) {
                            var_46 = local_sp_7 + (-8L);
                            *(uint64_t *)var_46 = 4202442UL;
                            indirect_placeholder_13(4247680UL);
                            local_sp_8 = var_46;
                        }
                        local_sp_9 = local_sp_8;
                        if (((rbx_4 & 64UL) == 0UL) || _not) {
                            var_47 = local_sp_8 + (-8L);
                            *(uint64_t *)var_47 = 4202462UL;
                            indirect_placeholder_13(4247680UL);
                            local_sp_9 = var_47;
                        }
                        local_sp_10 = local_sp_9;
                        if ((signed char)(unsigned char)rbx_4 <= '\xff') {
                            var_48 = local_sp_9 + (-8L);
                            *(uint64_t *)var_48 = 4202477UL;
                            indirect_placeholder_13(4246484UL);
                            local_sp_10 = var_48;
                        }
                        *(uint64_t *)(local_sp_10 + (-8L)) = 4202541UL;
                        indirect_placeholder();
                        return;
                    }
                    break;
                  case 5U:
                    {
                        var_45 = ((rbx_4 & 32UL) == 0UL);
                        _not = (((uint64_t)((uint32_t)rbx_4 + 1U) != 0UL) ^ 1);
                        local_sp_8 = local_sp_7;
                        if (var_45 || _not) {
                            var_46 = local_sp_7 + (-8L);
                            *(uint64_t *)var_46 = 4202442UL;
                            indirect_placeholder_13(4247680UL);
                            local_sp_8 = var_46;
                        }
                        local_sp_9 = local_sp_8;
                        if (((rbx_4 & 64UL) == 0UL) || _not) {
                            var_47 = local_sp_8 + (-8L);
                            *(uint64_t *)var_47 = 4202462UL;
                            indirect_placeholder_13(4247680UL);
                            local_sp_9 = var_47;
                        }
                        local_sp_10 = local_sp_9;
                        if ((signed char)(unsigned char)rbx_4 <= '\xff') {
                            var_48 = local_sp_9 + (-8L);
                            *(uint64_t *)var_48 = 4202477UL;
                            indirect_placeholder_13(4246484UL);
                            local_sp_10 = var_48;
                        }
                        *(uint64_t *)(local_sp_10 + (-8L)) = 4202541UL;
                        indirect_placeholder();
                        return;
                    }
                    break;
                }
            }
            break;
        }
    }
    var_19 = var_0 + (-456L);
    *(uint64_t *)var_19 = 4201934UL;
    var_20 = indirect_placeholder_2(4246271UL, var_6, 4247744UL, rsi, 0UL);
    var_21 = (uint32_t)var_20;
    local_sp_12 = var_19;
    rcx_1 = 4247744UL;
    local_sp_13 = var_19;
    local_sp_14 = var_19;
    if ((uint64_t)(var_21 + 1U) == 0UL) {
        var_26 = *(uint32_t *)6355612UL;
        var_27 = (uint64_t)var_26;
        rax_1 = var_27;
        rax_2 = var_27;
        if ((uint64_t)(var_5 - var_26) == 0UL) {
            var_28 = *(uint64_t *)((uint64_t)((long)(rax_2 << 32UL) >> (long)29UL) + rsi);
            *(uint64_t *)(local_sp_13 + (-8L)) = 4202242UL;
            var_29 = indirect_placeholder_14(var_28);
            var_30 = var_29.field_0;
            var_31 = var_29.field_1;
            var_32 = var_29.field_2;
            *(uint64_t *)(local_sp_13 + (-16L)) = 4202270UL;
            indirect_placeholder_3(0UL, 4246444UL, 0UL, var_30, 0UL, var_31, var_32);
            *(uint64_t *)(local_sp_13 + (-24L)) = 4202280UL;
            indirect_placeholder_5(rbx_6, rsi, 1UL, var_6);
            abort();
        }
        var_33 = local_sp_12 + (-8L);
        *(uint64_t *)var_33 = 4202301UL;
        indirect_placeholder();
        rbx_1 = rbx_5;
        local_sp_0 = var_33;
        rax_0 = rax_1;
        rbx_0 = rbx_5;
        rcx_0 = rcx_1;
        local_sp_1 = var_33;
        if ((uint64_t)((uint32_t)rax_1 + 1U) == 0UL) {
            *(uint64_t *)(local_sp_0 + (-8L)) = 4202311UL;
            indirect_placeholder();
            var_34 = (uint64_t)*(uint32_t *)rax_0;
            var_35 = local_sp_0 + (-16L);
            *(uint64_t *)var_35 = 4202333UL;
            indirect_placeholder_3(0UL, 4246461UL, 1UL, rcx_0, var_34, var_4, 0UL);
            local_sp_1 = var_35;
            rbx_1 = rbx_0;
        }
        local_sp_2 = local_sp_1;
        rbx_2 = rbx_1;
        local_sp_3 = local_sp_1;
        rbx_3 = rbx_1;
        if ((rbx_1 & 1UL) == 0UL) {
            var_36 = local_sp_2 + (-8L);
            *(uint64_t *)var_36 = 4202346UL;
            indirect_placeholder_13(local_sp_2);
            local_sp_3 = var_36;
            rbx_3 = rbx_2;
        }
        local_sp_4 = local_sp_3;
        rbx_4 = rbx_3;
        if ((rbx_3 & 2UL) == 0UL) {
            var_37 = local_sp_3 + 65UL;
            var_38 = local_sp_3 + (-8L);
            *(uint64_t *)var_38 = 4202361UL;
            indirect_placeholder_13(var_37);
            local_sp_4 = var_38;
        }
        local_sp_5 = local_sp_4;
        if ((rbx_3 & 4UL) == 0UL) {
            var_39 = local_sp_4 + 130UL;
            var_40 = local_sp_4 + (-8L);
            *(uint64_t *)var_40 = 4202379UL;
            indirect_placeholder_13(var_39);
            local_sp_5 = var_40;
        }
        local_sp_6 = local_sp_5;
        if ((rbx_3 & 8UL) == 0UL) {
            var_41 = local_sp_5 + 195UL;
            var_42 = local_sp_5 + (-8L);
            *(uint64_t *)var_42 = 4202397UL;
            indirect_placeholder_13(var_41);
            local_sp_6 = var_42;
        }
        local_sp_7 = local_sp_6;
        if ((rbx_3 & 16UL) == 0UL) {
            var_43 = local_sp_6 + 260UL;
            var_44 = local_sp_6 + (-8L);
            *(uint64_t *)var_44 = 4202415UL;
            indirect_placeholder_13(var_43);
            local_sp_7 = var_44;
        }
        var_45 = ((rbx_4 & 32UL) == 0UL);
        _not = (((uint64_t)((uint32_t)rbx_4 + 1U) != 0UL) ^ 1);
        local_sp_8 = local_sp_7;
        if (var_45 || _not) {
            var_46 = local_sp_7 + (-8L);
            *(uint64_t *)var_46 = 4202442UL;
            indirect_placeholder_13(4247680UL);
            local_sp_8 = var_46;
        }
        local_sp_9 = local_sp_8;
        if (((rbx_4 & 64UL) == 0UL) || _not) {
            var_47 = local_sp_8 + (-8L);
            *(uint64_t *)var_47 = 4202462UL;
            indirect_placeholder_13(4247680UL);
            local_sp_9 = var_47;
        }
        local_sp_10 = local_sp_9;
        if ((signed char)(unsigned char)rbx_4 <= '\xff') {
            var_48 = local_sp_9 + (-8L);
            *(uint64_t *)var_48 = 4202477UL;
            indirect_placeholder_13(4246484UL);
            local_sp_10 = var_48;
        }
        *(uint64_t *)(local_sp_10 + (-8L)) = 4202541UL;
        indirect_placeholder();
        return;
    }
    if ((uint64_t)(var_21 + 131U) != 0UL) {
        if ((uint64_t)(var_21 + 130U) != 0UL) {
            *(uint64_t *)(var_0 + (-464L)) = 4201819UL;
            indirect_placeholder_5(var_1, rsi, 0UL, var_6);
            abort();
        }
    }
    var_22 = (*(uint32_t *)6355488UL == 1U) ? 4246208UL : 4246214UL;
    *(uint64_t *)(var_0 + (-472L)) = 0UL;
    var_23 = *(uint64_t *)6355496UL;
    var_24 = *(uint64_t *)6355648UL;
    *(uint64_t *)(var_0 + (-480L)) = 4201887UL;
    indirect_placeholder_3(0UL, 4246367UL, var_24, var_23, var_22, 4246408UL, 4246418UL);
    var_25 = var_0 + (-488L);
    *(uint64_t *)var_25 = 4201897UL;
    indirect_placeholder();
    local_sp_14 = var_25;
}

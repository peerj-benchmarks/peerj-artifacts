typedef unsigned char __u_char;
typedef unsigned short __u_short;
typedef unsigned int __u_int;
typedef unsigned long __u_long;
typedef signed char __int8_t;
typedef unsigned char __uint8_t;
typedef short __int16_t;
typedef unsigned short __uint16_t;
typedef int __int32_t;
typedef unsigned int __uint32_t;
typedef long __int64_t;
typedef unsigned long __uint64_t;
typedef __int8_t __int_least8_t;
typedef __uint8_t __uint_least8_t;
typedef __int16_t __int_least16_t;
typedef __uint16_t __uint_least16_t;
typedef __int32_t __int_least32_t;
typedef __uint32_t __uint_least32_t;
typedef __int64_t __int_least64_t;
typedef __uint64_t __uint_least64_t;
typedef long __quad_t;
typedef unsigned long __u_quad_t;
typedef long __intmax_t;
typedef unsigned long __uintmax_t;
typedef unsigned long __dev_t;
typedef unsigned int __uid_t;
typedef unsigned int __gid_t;
typedef unsigned long __ino_t;
typedef unsigned long __ino64_t;
typedef unsigned int __mode_t;
typedef unsigned long __nlink_t;
typedef long __off_t;
typedef long __off64_t;
typedef int __pid_t;
typedef struct {
    int __val[2];
} __fsid_t;
typedef long __clock_t;
typedef unsigned long __rlim_t;
typedef unsigned long __rlim64_t;
typedef unsigned int __id_t;
typedef long __time_t;
typedef unsigned int __useconds_t;
typedef long __suseconds_t;
typedef int __daddr_t;
typedef int __key_t;
typedef int __clockid_t;
typedef void *__timer_t;
typedef long __blksize_t;
typedef long __blkcnt_t;
typedef long __blkcnt64_t;
typedef unsigned long __fsblkcnt_t;
typedef unsigned long __fsblkcnt64_t;
typedef unsigned long __fsfilcnt_t;
typedef unsigned long __fsfilcnt64_t;
typedef long __fsword_t;
typedef long __ssize_t;
typedef long __syscall_slong_t;
typedef unsigned long __syscall_ulong_t;
typedef __off64_t __loff_t;
typedef char *__caddr_t;
typedef long __intptr_t;
typedef unsigned int __socklen_t;
typedef int __sig_atomic_t;
typedef __int8_t int8_t;
typedef __int16_t int16_t;
typedef __int32_t int32_t;
typedef __int64_t int64_t;
typedef __uint8_t uint8_t;
typedef __uint16_t uint16_t;
typedef __uint32_t uint32_t;
typedef __uint64_t uint64_t;
typedef __int_least8_t int_least8_t;
typedef __int_least16_t int_least16_t;
typedef __int_least32_t int_least32_t;
typedef __int_least64_t int_least64_t;
typedef __uint_least8_t uint_least8_t;
typedef __uint_least16_t uint_least16_t;
typedef __uint_least32_t uint_least32_t;
typedef __uint_least64_t uint_least64_t;
typedef signed char int_fast8_t;
typedef long int_fast16_t;
typedef long int_fast32_t;
typedef long int_fast64_t;
typedef unsigned char uint_fast8_t;
typedef unsigned long uint_fast16_t;
typedef unsigned long uint_fast32_t;
typedef unsigned long uint_fast64_t;
typedef long intptr_t;
typedef unsigned long uintptr_t;
typedef __intmax_t intmax_t;
typedef __uintmax_t uintmax_t;
typedef _Bool bool;
struct helper_pxor_xmm_wrapper_ret_type;
struct type_6;
struct type_8;
struct helper_mulsd_wrapper_ret_type;
struct helper_ucomisd_wrapper_75_ret_type;
struct helper_pxor_xmm_wrapper_76_ret_type;
struct helper_cvtsi2sd_wrapper_87_ret_type;
struct helper_pxor_xmm_wrapper_85_ret_type;
struct helper_cvtsi2sd_wrapper_ret_type;
struct helper_divsd_wrapper_ret_type;
struct helper_ucomisd_wrapper_86_ret_type;
struct helper_addsd_wrapper_88_ret_type;
struct helper_ucomisd_wrapper_89_ret_type;
struct helper_pxor_xmm_wrapper_90_ret_type;
struct helper_cvtsi2sd_wrapper_91_ret_type;
struct helper_divsd_wrapper_92_ret_type;
struct helper_ucomisd_wrapper_93_ret_type;
struct helper_divsd_wrapper_94_ret_type;
struct helper_ucomisd_wrapper_95_ret_type;
struct helper_mulsd_wrapper_96_ret_type;
struct helper_ucomisd_wrapper_97_ret_type;
struct helper_divsd_wrapper_98_ret_type;
struct helper_ucomisd_wrapper_99_ret_type;
struct helper_ucomisd_wrapper_100_ret_type;
struct helper_pxor_xmm_wrapper_ret_type {
    uint64_t field_0;
    uint64_t field_1;
};
struct type_6 {
};
struct type_8 {
};
struct helper_mulsd_wrapper_ret_type {
    uint64_t field_0;
    unsigned char field_1;
};
struct helper_ucomisd_wrapper_75_ret_type {
    uint64_t field_0;
    unsigned char field_1;
};
struct helper_pxor_xmm_wrapper_76_ret_type {
    uint64_t field_0;
    uint64_t field_1;
};
struct helper_cvtsi2sd_wrapper_87_ret_type {
    uint64_t field_0;
};
struct helper_pxor_xmm_wrapper_85_ret_type {
    uint64_t field_0;
    uint64_t field_1;
};
struct helper_cvtsi2sd_wrapper_ret_type {
    uint64_t field_0;
};
struct helper_divsd_wrapper_ret_type {
    uint64_t field_0;
    unsigned char field_1;
};
struct helper_ucomisd_wrapper_86_ret_type {
    uint64_t field_0;
    unsigned char field_1;
};
struct helper_addsd_wrapper_88_ret_type {
    uint64_t field_0;
    unsigned char field_1;
};
struct helper_ucomisd_wrapper_89_ret_type {
    uint64_t field_0;
    unsigned char field_1;
};
struct helper_pxor_xmm_wrapper_90_ret_type {
    uint64_t field_0;
    uint64_t field_1;
};
struct helper_cvtsi2sd_wrapper_91_ret_type {
    uint64_t field_0;
};
struct helper_divsd_wrapper_92_ret_type {
    uint64_t field_0;
    unsigned char field_1;
};
struct helper_ucomisd_wrapper_93_ret_type {
    uint64_t field_0;
    unsigned char field_1;
};
struct helper_divsd_wrapper_94_ret_type {
    uint64_t field_0;
    unsigned char field_1;
};
struct helper_ucomisd_wrapper_95_ret_type {
    uint64_t field_0;
    unsigned char field_1;
};
struct helper_mulsd_wrapper_96_ret_type {
    uint64_t field_0;
    unsigned char field_1;
};
struct helper_ucomisd_wrapper_97_ret_type {
    uint64_t field_0;
    unsigned char field_1;
};
struct helper_divsd_wrapper_98_ret_type {
    uint64_t field_0;
    unsigned char field_1;
};
struct helper_ucomisd_wrapper_99_ret_type {
    uint64_t field_0;
    unsigned char field_1;
};
struct helper_ucomisd_wrapper_100_ret_type {
    uint64_t field_0;
    unsigned char field_1;
};
extern void function_dispatcher(unsigned char *param_0);
extern uint64_t revng_init_local_sp(uint64_t param_0);
extern uint64_t init_rbx(void);
extern uint64_t init_state_0x8558(void);
extern unsigned char init_state_0x8549(void);
extern unsigned char init_state_0x8548(void);
extern unsigned char init_state_0x854c(void);
extern unsigned char init_state_0x854b(void);
extern unsigned char init_state_0x8547(void);
extern unsigned char init_state_0x854d(void);
extern void indirect_placeholder(void);
extern uint64_t init_rbp(void);
extern uint64_t init_cc_src2(void);
extern uint64_t helper_cc_compute_c_wrapper(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint32_t param_3);
extern uint64_t init_state_0x8560(void);
extern struct helper_pxor_xmm_wrapper_ret_type helper_pxor_xmm_wrapper(struct type_6 *param_0, struct type_8 *param_1, struct type_8 *param_2, uint64_t param_3, uint64_t param_4);
extern uint64_t init_state_0x8598(void);
extern uint64_t init_state_0x85a0(void);
extern uint64_t init_r14(void);
extern uint64_t init_r13(void);
extern uint64_t init_r12(void);
extern struct helper_mulsd_wrapper_ret_type helper_mulsd_wrapper(struct type_6 *param_0, struct type_8 *param_1, struct type_8 *param_2, uint64_t param_3, uint64_t param_4, unsigned char param_5, unsigned char param_6, unsigned char param_7, unsigned char param_8, unsigned char param_9, unsigned char param_10);
extern struct helper_ucomisd_wrapper_75_ret_type helper_ucomisd_wrapper_75(struct type_6 *param_0, struct type_8 *param_1, struct type_8 *param_2, uint64_t param_3, uint64_t param_4, unsigned char param_5, unsigned char param_6);
extern uint64_t helper_cc_compute_all_wrapper(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint32_t param_3);
extern struct helper_pxor_xmm_wrapper_76_ret_type helper_pxor_xmm_wrapper_76(struct type_6 *param_0, struct type_8 *param_1, struct type_8 *param_2, uint64_t param_3, uint64_t param_4);
extern struct helper_cvtsi2sd_wrapper_87_ret_type helper_cvtsi2sd_wrapper_87(struct type_6 *param_0, struct type_8 *param_1, uint32_t param_2);
extern uint64_t init_r15(void);
extern uint64_t init_state_0x86e0(void);
extern uint64_t init_state_0x86d8(void);
extern struct helper_pxor_xmm_wrapper_85_ret_type helper_pxor_xmm_wrapper_85(struct type_6 *param_0, struct type_8 *param_1, struct type_8 *param_2, uint64_t param_3, uint64_t param_4);
extern struct helper_cvtsi2sd_wrapper_ret_type helper_cvtsi2sd_wrapper(struct type_6 *param_0, struct type_8 *param_1, uint32_t param_2);
extern struct helper_divsd_wrapper_ret_type helper_divsd_wrapper(struct type_6 *param_0, struct type_8 *param_1, struct type_8 *param_2, uint64_t param_3, uint64_t param_4, unsigned char param_5, unsigned char param_6, unsigned char param_7, unsigned char param_8, unsigned char param_9, unsigned char param_10);
extern struct helper_ucomisd_wrapper_86_ret_type helper_ucomisd_wrapper_86(struct type_6 *param_0, struct type_8 *param_1, struct type_8 *param_2, uint64_t param_3, uint64_t param_4, unsigned char param_5, unsigned char param_6);
extern struct helper_addsd_wrapper_88_ret_type helper_addsd_wrapper_88(struct type_6 *param_0, struct type_8 *param_1, struct type_8 *param_2, uint64_t param_3, uint64_t param_4, unsigned char param_5, unsigned char param_6, unsigned char param_7, unsigned char param_8, unsigned char param_9, unsigned char param_10);
extern struct helper_ucomisd_wrapper_89_ret_type helper_ucomisd_wrapper_89(struct type_6 *param_0, struct type_8 *param_1, struct type_8 *param_2, uint64_t param_3, uint64_t param_4, unsigned char param_5, unsigned char param_6);
extern struct helper_pxor_xmm_wrapper_90_ret_type helper_pxor_xmm_wrapper_90(struct type_6 *param_0, struct type_8 *param_1, struct type_8 *param_2, uint64_t param_3, uint64_t param_4);
extern struct helper_cvtsi2sd_wrapper_91_ret_type helper_cvtsi2sd_wrapper_91(struct type_6 *param_0, struct type_8 *param_1, uint32_t param_2);
extern struct helper_divsd_wrapper_92_ret_type helper_divsd_wrapper_92(struct type_6 *param_0, struct type_8 *param_1, struct type_8 *param_2, uint64_t param_3, uint64_t param_4, unsigned char param_5, unsigned char param_6, unsigned char param_7, unsigned char param_8, unsigned char param_9, unsigned char param_10);
extern struct helper_ucomisd_wrapper_93_ret_type helper_ucomisd_wrapper_93(struct type_6 *param_0, struct type_8 *param_1, struct type_8 *param_2, uint64_t param_3, uint64_t param_4, unsigned char param_5, unsigned char param_6);
extern struct helper_divsd_wrapper_94_ret_type helper_divsd_wrapper_94(struct type_6 *param_0, struct type_8 *param_1, struct type_8 *param_2, uint64_t param_3, uint64_t param_4, unsigned char param_5, unsigned char param_6, unsigned char param_7, unsigned char param_8, unsigned char param_9, unsigned char param_10);
extern struct helper_ucomisd_wrapper_95_ret_type helper_ucomisd_wrapper_95(struct type_6 *param_0, struct type_8 *param_1, struct type_8 *param_2, uint64_t param_3, uint64_t param_4, unsigned char param_5, unsigned char param_6);
extern struct helper_mulsd_wrapper_96_ret_type helper_mulsd_wrapper_96(struct type_6 *param_0, struct type_8 *param_1, struct type_8 *param_2, uint64_t param_3, uint64_t param_4, unsigned char param_5, unsigned char param_6, unsigned char param_7, unsigned char param_8, unsigned char param_9, unsigned char param_10);
extern struct helper_ucomisd_wrapper_97_ret_type helper_ucomisd_wrapper_97(struct type_6 *param_0, struct type_8 *param_1, struct type_8 *param_2, uint64_t param_3, uint64_t param_4, unsigned char param_5, unsigned char param_6);
extern struct helper_divsd_wrapper_98_ret_type helper_divsd_wrapper_98(struct type_6 *param_0, struct type_8 *param_1, struct type_8 *param_2, uint64_t param_3, uint64_t param_4, unsigned char param_5, unsigned char param_6, unsigned char param_7, unsigned char param_8, unsigned char param_9, unsigned char param_10);
extern struct helper_ucomisd_wrapper_99_ret_type helper_ucomisd_wrapper_99(struct type_6 *param_0, struct type_8 *param_1, struct type_8 *param_2, uint64_t param_3, uint64_t param_4, unsigned char param_5, unsigned char param_6);
extern struct helper_ucomisd_wrapper_100_ret_type helper_ucomisd_wrapper_100(struct type_6 *param_0, struct type_8 *param_1, struct type_8 *param_2, uint64_t param_3, uint64_t param_4, unsigned char param_5, unsigned char param_6);
uint64_t bb_parse_number(uint64_t rdx, uint64_t rdi, uint64_t rcx, uint64_t rsi, uint64_t r8, uint64_t r9) {
    struct helper_cvtsi2sd_wrapper_ret_type var_99;
    uint64_t var_100;
    struct helper_pxor_xmm_wrapper_ret_type var_101;
    uint64_t var_102;
    uint64_t *var_103;
    unsigned char state_0x8549_4;
    struct helper_pxor_xmm_wrapper_ret_type var_49;
    struct helper_ucomisd_wrapper_86_ret_type var_42;
    struct helper_pxor_xmm_wrapper_85_ret_type var_23;
    struct helper_pxor_xmm_wrapper_76_ret_type var_36;
    uint64_t var_0;
    uint64_t var_1;
    uint64_t var_2;
    uint64_t var_3;
    uint64_t var_4;
    uint64_t var_5;
    uint64_t var_6;
    uint64_t var_7;
    uint64_t var_8;
    uint64_t var_9;
    uint64_t var_10;
    uint64_t var_11;
    uint64_t var_12;
    uint64_t var_13;
    unsigned char var_14;
    unsigned char var_15;
    unsigned char var_16;
    unsigned char var_17;
    unsigned char var_18;
    unsigned char var_19;
    uint64_t var_20;
    uint32_t var_21;
    uint64_t var_22;
    uint64_t var_24;
    uint64_t *var_25;
    uint64_t var_26;
    uint64_t var_27;
    uint32_t var_28;
    bool var_29;
    uint64_t rdi2_1;
    uint64_t rbp_0;
    uint64_t var_69;
    uint64_t var_70;
    uint64_t rbx_3;
    uint64_t var_71;
    uint64_t var_72;
    uint64_t rbx_5;
    uint64_t rbx_1;
    uint64_t state_0x8558_2;
    uint64_t rdi2_0;
    uint64_t state_0x8560_1;
    uint64_t state_0x8558_0;
    unsigned char state_0x8549_5;
    uint64_t state_0x8560_0;
    uint64_t state_0x85a0_2;
    unsigned char state_0x8549_0;
    uint64_t state_0x8598_2;
    uint64_t state_0x85a0_0;
    uint64_t state_0x8598_0;
    unsigned char var_30;
    uint64_t var_31;
    uint32_t var_32;
    uint32_t var_33;
    unsigned char var_34;
    uint64_t var_35;
    uint64_t var_37;
    struct helper_cvtsi2sd_wrapper_ret_type var_38;
    uint64_t var_39;
    struct helper_divsd_wrapper_ret_type var_40;
    uint64_t var_41;
    uint64_t var_43;
    unsigned char var_44;
    uint64_t var_45;
    struct helper_mulsd_wrapper_ret_type var_46;
    uint64_t var_47;
    unsigned char var_48;
    uint64_t var_50;
    struct helper_cvtsi2sd_wrapper_87_ret_type var_51;
    uint64_t var_52;
    struct helper_addsd_wrapper_88_ret_type var_53;
    uint64_t var_54;
    unsigned char var_55;
    uint64_t rbx_2;
    uint64_t state_0x8558_1;
    unsigned char state_0x8549_1;
    uint64_t state_0x85a0_1;
    uint64_t state_0x8598_1;
    uint64_t var_56;
    uint64_t var_57;
    uint64_t var_58;
    uint64_t *var_59;
    uint64_t var_60;
    uint64_t var_61;
    uint64_t var_62;
    unsigned char var_63;
    uint64_t var_64;
    uint32_t var_65;
    uint64_t var_66;
    uint64_t var_67;
    uint64_t var_68;
    uint64_t rbp_1;
    uint64_t rax_1;
    uint64_t local_sp_0;
    uint64_t var_73;
    uint64_t rax_5;
    uint64_t *var_74;
    struct helper_ucomisd_wrapper_89_ret_type var_75;
    uint64_t var_76;
    unsigned char var_77;
    uint64_t var_78;
    struct helper_cvtsi2sd_wrapper_91_ret_type var_79;
    uint64_t var_80;
    struct helper_divsd_wrapper_92_ret_type var_81;
    uint64_t var_82;
    unsigned char var_83;
    uint64_t var_84;
    struct helper_ucomisd_wrapper_93_ret_type var_85;
    uint64_t rax_4;
    struct helper_divsd_wrapper_94_ret_type var_86;
    uint64_t var_87;
    unsigned char var_88;
    uint64_t var_89;
    struct helper_ucomisd_wrapper_95_ret_type var_90;
    uint64_t rax_3;
    unsigned char var_91;
    uint64_t *var_92;
    unsigned char state_0x8549_2;
    uint64_t rax_2;
    struct helper_mulsd_wrapper_96_ret_type var_93;
    uint64_t var_94;
    struct helper_ucomisd_wrapper_75_ret_type var_95;
    unsigned char var_96;
    uint64_t var_97;
    struct helper_ucomisd_wrapper_97_ret_type var_98;
    uint64_t rbx_4_in;
    unsigned char state_0x8549_3;
    uint64_t rbx_4;
    struct helper_divsd_wrapper_98_ret_type var_104;
    uint64_t var_105;
    unsigned char var_106;
    struct helper_ucomisd_wrapper_99_ret_type var_107;
    uint64_t var_108;
    unsigned char var_109;
    struct helper_ucomisd_wrapper_100_ret_type var_110;
    unsigned int loop_state_var;
    var_0 = revng_init_local_sp(0UL);
    var_1 = init_state_0x8598();
    var_2 = init_state_0x85a0();
    var_3 = init_rbx();
    var_4 = init_r14();
    var_5 = init_rbp();
    var_6 = init_r13();
    var_7 = init_r12();
    var_8 = init_r15();
    var_9 = init_cc_src2();
    var_10 = init_state_0x8558();
    var_11 = init_state_0x8560();
    var_12 = init_state_0x86e0();
    var_13 = init_state_0x86d8();
    var_14 = init_state_0x8549();
    var_15 = init_state_0x8548();
    var_16 = init_state_0x854c();
    var_17 = init_state_0x854b();
    var_18 = init_state_0x8547();
    var_19 = init_state_0x854d();
    *(uint64_t *)(var_0 + (-8L)) = var_8;
    *(uint64_t *)(var_0 + (-16L)) = var_4;
    *(uint64_t *)(var_0 + (-24L)) = var_6;
    *(uint64_t *)(var_0 + (-32L)) = var_7;
    *(uint64_t *)(var_0 + (-40L)) = var_5;
    *(uint64_t *)(var_0 + (-48L)) = var_3;
    var_20 = var_0 + (-104L);
    var_21 = (uint32_t)rdx;
    var_22 = (uint64_t)var_21;
    var_23 = helper_pxor_xmm_wrapper_85((struct type_6 *)(0UL), (struct type_8 *)(1160UL), (struct type_8 *)(1160UL), var_12, var_13);
    var_24 = var_23.field_1;
    var_25 = (uint64_t *)var_20;
    *var_25 = var_24;
    var_26 = *(uint64_t *)4275936UL;
    var_27 = (uint64_t)((long)(rcx << 32UL) >> (long)32UL);
    var_28 = (uint32_t)rsi;
    var_29 = ((uint64_t)(var_28 + (-16)) != 0UL);
    rbp_0 = rdi;
    rbx_1 = 0UL;
    rdi2_0 = 0UL;
    state_0x8558_0 = var_10;
    state_0x8560_0 = var_11;
    state_0x8549_0 = var_14;
    state_0x85a0_0 = var_2;
    state_0x8598_0 = var_1;
    state_0x85a0_1 = 0UL;
    local_sp_0 = var_20;
    rax_3 = 0UL;
    rax_2 = 0UL;
    while (1U)
        {
            var_30 = *(unsigned char *)rbp_0;
            var_31 = (uint64_t)var_30;
            var_32 = (uint32_t)var_31;
            var_33 = var_32 + (-48);
            var_34 = (unsigned char)(uint64_t)var_33;
            rdi2_1 = rdi2_0;
            rbx_3 = rbx_1;
            rbx_5 = rbx_1;
            state_0x8558_2 = state_0x8558_0;
            state_0x8560_1 = state_0x8560_0;
            state_0x8549_5 = state_0x8549_0;
            state_0x85a0_2 = state_0x85a0_0;
            state_0x8598_2 = state_0x8598_0;
            rbx_2 = rbx_1;
            rbp_1 = rbp_0;
            if ((uint64_t)(var_34 & '\xfe') > 9UL) {
                rdi2_1 = 1UL;
                if (var_29 || ((uint64_t)var_34 > 54UL)) {
                    function_dispatcher((unsigned char *)(0UL));
                    return var_31;
                }
                var_56 = (uint64_t)(var_33 & (-256)) | ((uint64_t)(var_30 + '\xd2') == 0UL);
                var_57 = helper_cc_compute_c_wrapper(rdi2_0 - var_56, var_56, var_9, 14U);
                if (var_57 != 0UL) {
                    var_58 = *var_25;
                    var_59 = (uint64_t *)(var_0 + (-96L));
                    *var_59 = var_58;
                    var_60 = (uint64_t)var_30;
                    var_61 = (uint64_t)(var_32 + (-65));
                    rax_1 = var_61;
                    if ((uint64_t)((uint32_t)((int)((uint32_t)r8 << 24U) >> (int)24U) - (uint32_t)(((uint64_t)((unsigned char)var_61 & '\xfe') > 25UL) ? var_60 : (var_60 + 32UL))) == 0UL) {
                        break;
                    }
                    var_62 = rbp_0 + 1UL;
                    var_63 = *(unsigned char *)var_62;
                    var_64 = (uint64_t)((uint32_t)(uint64_t)var_63 + (-9));
                    rax_1 = var_64;
                    if (var_64 > 4UL) {
                        break;
                    }
                    if ((uint64_t)(var_63 + '\xe0') == 0UL) {
                        break;
                    }
                    *(uint64_t *)(var_0 + (-112L)) = 4225692UL;
                    indirect_placeholder();
                    var_65 = *(uint32_t *)var_64;
                    var_66 = (uint64_t)var_65;
                    *(uint32_t *)(var_0 + (-92L)) = var_65;
                    *(uint64_t *)(var_0 + (-120L)) = 4225720UL;
                    indirect_placeholder();
                    *var_59 = var_66;
                    var_67 = var_0 + (-128L);
                    *(uint64_t *)var_67 = 4225730UL;
                    indirect_placeholder();
                    *(uint32_t *)var_66 = *(uint32_t *)(var_0 + (-108L));
                    var_68 = *(uint64_t *)(var_0 + (-88L));
                    rax_1 = rbp_0;
                    local_sp_0 = var_67;
                    if (var_62 == var_68) {
                        break;
                    }
                    rbp_1 = var_68;
                    rax_1 = var_68;
                    if ((long)rbx_1 <= (long)18446744073709551615UL) {
                        var_71 = 9223372036854775807UL - rbx_1;
                        var_72 = *var_25;
                        rbx_3 = ((long)var_72 > (long)var_71) ? 9223372036854775807UL : (rbx_1 + var_72);
                        break;
                    }
                    var_69 = 9223372036854775808UL - (-9223372036854775808L);
                    var_70 = *var_25;
                    rbx_3 = ((long)var_70 < (long)var_69) ? 9223372036854775808UL : (var_70 + rbx_1);
                    break;
                }
            }
            var_35 = (uint64_t)(uint32_t)(uint64_t)var_30 + (-48L);
            var_36 = helper_pxor_xmm_wrapper_76((struct type_6 *)(0UL), (struct type_8 *)(776UL), (struct type_8 *)(776UL), state_0x8558_0, state_0x8560_0);
            var_37 = var_36.field_1;
            var_38 = helper_cvtsi2sd_wrapper((struct type_6 *)(0UL), (struct type_8 *)(776UL), var_28);
            var_39 = var_38.field_0;
            var_40 = helper_divsd_wrapper((struct type_6 *)(0UL), (struct type_8 *)(840UL), (struct type_8 *)(776UL), var_26, var_39, state_0x8549_0, var_15, var_16, var_17, var_18, var_19);
            var_41 = var_40.field_0;
            var_42 = helper_ucomisd_wrapper_86((struct type_6 *)(0UL), (struct type_8 *)(840UL), (struct type_8 *)(2824UL), var_41, *var_25, var_40.field_1, var_16);
            var_43 = var_42.field_0;
            var_44 = var_42.field_1;
            var_45 = helper_cc_compute_c_wrapper(var_35, var_43, var_9, 1U);
            state_0x8558_1 = var_39;
            state_0x8549_1 = var_44;
            state_0x8598_1 = var_41;
            state_0x8560_1 = var_37;
            if (var_45 == 0UL) {
                var_46 = helper_mulsd_wrapper((struct type_6 *)(0UL), (struct type_8 *)(776UL), (struct type_8 *)(2824UL), var_39, *var_25, var_44, var_15, var_16, var_17, var_18, var_19);
                var_47 = var_46.field_0;
                var_48 = var_46.field_1;
                var_49 = helper_pxor_xmm_wrapper((struct type_6 *)(0UL), (struct type_8 *)(840UL), (struct type_8 *)(840UL), var_41, 0UL);
                var_50 = var_49.field_1;
                var_51 = helper_cvtsi2sd_wrapper_87((struct type_6 *)(0UL), (struct type_8 *)(840UL), (uint32_t)var_35);
                var_52 = var_51.field_0;
                var_53 = helper_addsd_wrapper_88((struct type_6 *)(0UL), (struct type_8 *)(776UL), (struct type_8 *)(840UL), var_52, var_47, var_48, var_15, var_16, var_17, var_18, var_19);
                var_54 = var_53.field_0;
                var_55 = var_53.field_1;
                *var_25 = var_54;
                state_0x8558_1 = var_54;
                state_0x8549_1 = var_55;
                state_0x85a0_1 = var_50;
                state_0x8598_1 = var_52;
            } else {
                rbx_2 = rbx_1 + var_27;
            }
            rbx_5 = rbx_2 - (((uint64_t)(unsigned char)rdi2_0 == 0UL) ? 0UL : var_27);
            state_0x8558_2 = state_0x8558_1;
            state_0x8549_5 = state_0x8549_1;
            state_0x85a0_2 = state_0x85a0_1;
            state_0x8598_2 = state_0x8598_1;
            rbp_0 = rbp_0 + 1UL;
            rbx_1 = rbx_5;
            rdi2_0 = rdi2_1;
            state_0x8558_0 = state_0x8558_2;
            state_0x8560_0 = state_0x8560_1;
            state_0x8549_0 = state_0x8549_5;
            state_0x85a0_0 = state_0x85a0_2;
            state_0x8598_0 = state_0x8598_2;
            continue;
        }
    *(uint64_t *)r9 = rbp_1;
    var_73 = var_22 + (-2L);
    rax_4 = rax_1;
    rbx_4_in = rbx_3;
    rax_5 = rax_1;
    if ((uint64_t)(uint32_t)var_73 == 0UL) {
        *(uint64_t *)(local_sp_0 + (-8L)) = 4225889UL;
        indirect_placeholder();
    } else {
        var_74 = (uint64_t *)(local_sp_0 + 8UL);
        var_75 = helper_ucomisd_wrapper_89((struct type_6 *)(0UL), (struct type_8 *)(1224UL), (struct type_8 *)(2824UL), *(uint64_t *)4275912UL, *var_74, state_0x8549_0, var_16);
        var_76 = var_75.field_0;
        var_77 = var_75.field_1;
        state_0x8549_3 = var_77;
        if ((var_76 & 4UL) != 0UL) {
            var_78 = helper_cc_compute_all_wrapper(var_73, var_76, var_9, 1U);
            if ((var_78 & 64UL) == 0UL) {
                return rax_5;
            }
        }
        if ((long)rbx_3 < (long)0UL) {
            helper_pxor_xmm_wrapper_76((struct type_6 *)(0UL), (struct type_8 *)(776UL), (struct type_8 *)(776UL), state_0x8558_0, state_0x8560_0);
            var_99 = helper_cvtsi2sd_wrapper((struct type_6 *)(0UL), (struct type_8 *)(776UL), var_21);
            var_100 = var_99.field_0;
            var_101 = helper_pxor_xmm_wrapper((struct type_6 *)(0UL), (struct type_8 *)(840UL), (struct type_8 *)(840UL), state_0x8598_0, state_0x85a0_0);
            var_102 = var_101.field_0;
            var_103 = (uint64_t *)local_sp_0;
            while (1U)
                {
                    rbx_4 = rbx_4_in + 1UL;
                    var_104 = helper_divsd_wrapper_98((struct type_6 *)(0UL), (struct type_8 *)(1096UL), (struct type_8 *)(776UL), var_100, *var_103, state_0x8549_3, var_15, var_16, var_17, var_18, var_19);
                    var_105 = var_104.field_0;
                    var_106 = var_104.field_1;
                    *var_103 = var_105;
                    var_107 = helper_ucomisd_wrapper_99((struct type_6 *)(0UL), (struct type_8 *)(1096UL), (struct type_8 *)(840UL), var_102, var_105, var_106, var_16);
                    var_108 = var_107.field_0;
                    var_109 = var_107.field_1;
                    rbx_4_in = rbx_4;
                    state_0x8549_4 = var_109;
                    if ((var_108 & 4UL) == 0UL) {
                        state_0x8549_3 = state_0x8549_4;
                        if (rbx_4 != 0UL) {
                            continue;
                        }
                        break;
                    }
                    var_110 = helper_ucomisd_wrapper_100((struct type_6 *)(0UL), (struct type_8 *)(1096UL), (struct type_8 *)(904UL), var_105, var_102, var_109, var_16);
                    state_0x8549_4 = var_110.field_1;
                    if ((var_110.field_0 & 64UL) != 0UL) {
                        *(uint64_t *)(local_sp_0 + (-8L)) = 4226031UL;
                        indirect_placeholder();
                        *(uint32_t *)rax_1 = 34U;
                        break;
                    }
                }
        }
        if (rbx_3 != 0UL) {
            helper_pxor_xmm_wrapper_90((struct type_6 *)(0UL), (struct type_8 *)(904UL), (struct type_8 *)(904UL), var_26, 0UL);
            var_79 = helper_cvtsi2sd_wrapper_91((struct type_6 *)(0UL), (struct type_8 *)(904UL), var_21);
            var_80 = var_79.field_0;
            var_81 = helper_divsd_wrapper_92((struct type_6 *)(0UL), (struct type_8 *)(776UL), (struct type_8 *)(904UL), *(uint64_t *)4275944UL, var_80, var_77, var_15, var_16, var_17, var_18, var_19);
            var_82 = var_81.field_0;
            var_83 = var_81.field_1;
            var_84 = *var_74;
            var_85 = helper_ucomisd_wrapper_93((struct type_6 *)(0UL), (struct type_8 *)(776UL), (struct type_8 *)(1032UL), var_82, var_84, var_83, var_16);
            if ((var_85.field_0 & 65UL) != 0UL) {
                *(uint64_t *)(local_sp_0 + (-8L)) = 4226079UL;
                indirect_placeholder();
                *(uint32_t *)rax_4 = 34U;
                rax_5 = rax_4;
                return rax_5;
            }
            var_86 = helper_divsd_wrapper_94((struct type_6 *)(0UL), (struct type_8 *)(840UL), (struct type_8 *)(904UL), *(uint64_t *)4275936UL, var_80, var_85.field_1, var_15, var_16, var_17, var_18, var_19);
            var_87 = var_86.field_0;
            var_88 = var_86.field_1;
            var_89 = rbx_3 + (-1L);
            var_90 = helper_ucomisd_wrapper_95((struct type_6 *)(0UL), (struct type_8 *)(1032UL), (struct type_8 *)(840UL), var_87, var_84, var_88, var_16);
            rax_5 = var_89;
            if ((var_90.field_0 & 65UL) != 0UL) {
                *(uint64_t *)(local_sp_0 + (-8L)) = 4226115UL;
                indirect_placeholder();
                *(uint32_t *)rax_3 = 34U;
                rax_5 = rax_3;
                return rax_5;
            }
            var_91 = var_90.field_1;
            var_92 = (uint64_t *)local_sp_0;
            state_0x8549_2 = var_91;
            while (1U)
                {
                    var_93 = helper_mulsd_wrapper_96((struct type_6 *)(0UL), (struct type_8 *)(968UL), (struct type_8 *)(904UL), var_80, *var_92, state_0x8549_2, var_15, var_16, var_17, var_18, var_19);
                    var_94 = var_93.field_0;
                    *var_92 = var_94;
                    rax_4 = rax_2;
                    if (rax_2 != var_89) {
                        loop_state_var = 0U;
                        break;
                    }
                    var_95 = helper_ucomisd_wrapper_75((struct type_6 *)(0UL), (struct type_8 *)(776UL), (struct type_8 *)(2824UL), var_82, var_94, var_93.field_1, var_16);
                    if ((var_95.field_0 & 65UL) != 0UL) {
                        loop_state_var = 1U;
                        break;
                    }
                    var_96 = var_95.field_1;
                    var_97 = rax_2 + 1UL;
                    var_98 = helper_ucomisd_wrapper_97((struct type_6 *)(0UL), (struct type_8 *)(1160UL), (struct type_8 *)(840UL), var_87, *var_92, var_96, var_16);
                    rax_2 = var_97;
                    rax_3 = var_97;
                    if ((var_98.field_0 & 65UL) != 0UL) {
                        loop_state_var = 2U;
                        break;
                    }
                    state_0x8549_2 = var_98.field_1;
                    continue;
                }
            switch (loop_state_var) {
              case 0U:
                {
                    break;
                }
                break;
              case 1U:
                {
                    *(uint64_t *)(local_sp_0 + (-8L)) = 4226079UL;
                    indirect_placeholder();
                    *(uint32_t *)rax_4 = 34U;
                    rax_5 = rax_4;
                }
                break;
              case 2U:
                {
                    *(uint64_t *)(local_sp_0 + (-8L)) = 4226115UL;
                    indirect_placeholder();
                    *(uint32_t *)rax_3 = 34U;
                    rax_5 = rax_3;
                }
                break;
            }
        }
    }
}

typedef unsigned char __u_char;
typedef unsigned short __u_short;
typedef unsigned int __u_int;
typedef unsigned long __u_long;
typedef signed char __int8_t;
typedef unsigned char __uint8_t;
typedef short __int16_t;
typedef unsigned short __uint16_t;
typedef int __int32_t;
typedef unsigned int __uint32_t;
typedef long __int64_t;
typedef unsigned long __uint64_t;
typedef __int8_t __int_least8_t;
typedef __uint8_t __uint_least8_t;
typedef __int16_t __int_least16_t;
typedef __uint16_t __uint_least16_t;
typedef __int32_t __int_least32_t;
typedef __uint32_t __uint_least32_t;
typedef __int64_t __int_least64_t;
typedef __uint64_t __uint_least64_t;
typedef long __quad_t;
typedef unsigned long __u_quad_t;
typedef long __intmax_t;
typedef unsigned long __uintmax_t;
typedef unsigned long __dev_t;
typedef unsigned int __uid_t;
typedef unsigned int __gid_t;
typedef unsigned long __ino_t;
typedef unsigned long __ino64_t;
typedef unsigned int __mode_t;
typedef unsigned long __nlink_t;
typedef long __off_t;
typedef long __off64_t;
typedef int __pid_t;
typedef struct {
    int __val[2];
} __fsid_t;
typedef long __clock_t;
typedef unsigned long __rlim_t;
typedef unsigned long __rlim64_t;
typedef unsigned int __id_t;
typedef long __time_t;
typedef unsigned int __useconds_t;
typedef long __suseconds_t;
typedef int __daddr_t;
typedef int __key_t;
typedef int __clockid_t;
typedef void *__timer_t;
typedef long __blksize_t;
typedef long __blkcnt_t;
typedef long __blkcnt64_t;
typedef unsigned long __fsblkcnt_t;
typedef unsigned long __fsblkcnt64_t;
typedef unsigned long __fsfilcnt_t;
typedef unsigned long __fsfilcnt64_t;
typedef long __fsword_t;
typedef long __ssize_t;
typedef long __syscall_slong_t;
typedef unsigned long __syscall_ulong_t;
typedef __off64_t __loff_t;
typedef char *__caddr_t;
typedef long __intptr_t;
typedef unsigned int __socklen_t;
typedef int __sig_atomic_t;
typedef __int8_t int8_t;
typedef __int16_t int16_t;
typedef __int32_t int32_t;
typedef __int64_t int64_t;
typedef __uint8_t uint8_t;
typedef __uint16_t uint16_t;
typedef __uint32_t uint32_t;
typedef __uint64_t uint64_t;
typedef __int_least8_t int_least8_t;
typedef __int_least16_t int_least16_t;
typedef __int_least32_t int_least32_t;
typedef __int_least64_t int_least64_t;
typedef __uint_least8_t uint_least8_t;
typedef __uint_least16_t uint_least16_t;
typedef __uint_least32_t uint_least32_t;
typedef __uint_least64_t uint_least64_t;
typedef signed char int_fast8_t;
typedef long int_fast16_t;
typedef long int_fast32_t;
typedef long int_fast64_t;
typedef unsigned char uint_fast8_t;
typedef unsigned long uint_fast16_t;
typedef unsigned long uint_fast32_t;
typedef unsigned long uint_fast64_t;
typedef long intptr_t;
typedef unsigned long uintptr_t;
typedef __intmax_t intmax_t;
typedef __uintmax_t uintmax_t;
typedef _Bool bool;
struct bb_skip_ret_type;
struct indirect_placeholder_6_ret_type;
struct indirect_placeholder_5_ret_type;
struct indirect_placeholder_3_ret_type;
struct indirect_placeholder_4_ret_type;
struct indirect_placeholder_7_ret_type;
struct bb_skip_ret_type {
    uint64_t field_0;
    uint64_t field_1;
    uint64_t field_2;
    uint64_t field_3;
    uint64_t field_4;
    uint64_t field_5;
    uint64_t field_6;
};
struct indirect_placeholder_6_ret_type {
    uint64_t field_0;
    uint64_t field_1;
    uint64_t field_2;
};
struct indirect_placeholder_5_ret_type {
    uint64_t field_0;
    uint64_t field_1;
    uint64_t field_2;
    uint64_t field_3;
};
struct indirect_placeholder_3_ret_type {
    uint64_t field_0;
    uint64_t field_1;
    uint64_t field_2;
    uint64_t field_3;
    uint64_t field_4;
    uint64_t field_5;
};
struct indirect_placeholder_4_ret_type {
    uint64_t field_0;
    uint64_t field_1;
    uint64_t field_2;
    uint64_t field_3;
    uint64_t field_4;
};
struct indirect_placeholder_7_ret_type {
    uint64_t field_0;
    uint64_t field_1;
    uint64_t field_2;
    uint64_t field_3;
    uint64_t field_4;
};
extern uint64_t revng_init_local_sp(uint64_t param_0);
extern uint64_t init_rbx(void);
extern void indirect_placeholder(void);
extern uint64_t init_rbp(void);
extern uint64_t init_cc_src2(void);
extern uint64_t helper_cc_compute_c_wrapper(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint32_t param_3);
extern uint64_t init_r14(void);
extern uint64_t init_r13(void);
extern uint64_t init_r12(void);
extern uint64_t init_rsi(void);
extern uint64_t init_r15(void);
extern struct indirect_placeholder_6_ret_type indirect_placeholder_6(uint64_t param_0, uint64_t param_1, uint64_t param_2);
extern uint64_t init_rax(void);
extern uint64_t init_r10(void);
extern struct indirect_placeholder_5_ret_type indirect_placeholder_5(uint64_t param_0);
extern struct indirect_placeholder_3_ret_type indirect_placeholder_3(void);
extern struct indirect_placeholder_4_ret_type indirect_placeholder_4(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint64_t param_3, uint64_t param_4, uint64_t param_5, uint64_t param_6);
extern struct indirect_placeholder_7_ret_type indirect_placeholder_7(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint64_t param_3, uint64_t param_4, uint64_t param_5, uint64_t param_6);
struct bb_skip_ret_type bb_skip(uint64_t rdi, uint64_t rcx, uint64_t r8, uint64_t r9) {
    bool var_26;
    uint64_t rbp_0;
    uint64_t local_sp_3;
    uint64_t var_27;
    uint64_t var_28;
    uint64_t var_29;
    uint64_t var_30;
    uint64_t var_31;
    uint64_t var_15;
    struct indirect_placeholder_6_ret_type var_16;
    uint64_t var_17;
    uint64_t var_18;
    uint64_t var_19;
    uint64_t rdi7_2;
    uint64_t var_0;
    uint64_t var_1;
    uint64_t var_2;
    uint64_t var_3;
    uint64_t var_4;
    uint64_t var_5;
    uint64_t var_6;
    uint64_t var_7;
    uint64_t var_8;
    uint64_t var_9;
    uint64_t var_10;
    uint64_t var_11;
    uint64_t rax_2;
    uint64_t r12_2;
    uint64_t var_43;
    uint64_t var_44;
    uint64_t var_45;
    uint64_t var_46;
    uint64_t var_47;
    uint64_t rcx8_3;
    uint64_t rax_1;
    uint64_t var_33;
    uint64_t var_34;
    uint64_t var_32;
    uint64_t rbx_2;
    uint64_t rdi7_0;
    uint64_t local_sp_0;
    uint64_t r10_0;
    uint32_t var_22;
    uint64_t rax_0;
    uint64_t var_23;
    uint64_t var_24;
    uint64_t var_25;
    uint64_t r12_3;
    uint64_t var_35;
    uint64_t local_sp_5;
    uint64_t r89_1;
    uint64_t r910_1;
    uint64_t var_36;
    uint64_t var_48;
    uint64_t rcx8_2;
    uint64_t r12_1;
    uint64_t local_sp_4;
    uint64_t rcx8_1;
    uint64_t rsi_1;
    uint64_t rbx_1;
    uint64_t local_sp_2;
    struct indirect_placeholder_5_ret_type var_37;
    uint64_t var_38;
    uint64_t var_39;
    struct indirect_placeholder_3_ret_type var_40;
    uint64_t var_41;
    uint64_t var_42;
    uint64_t var_20;
    uint64_t var_21;
    uint64_t rbx_3;
    uint64_t r89_0;
    uint64_t r910_0;
    uint64_t var_12;
    uint64_t var_13;
    uint64_t var_14;
    struct indirect_placeholder_7_ret_type var_49;
    uint64_t rdi7_4;
    uint64_t rcx8_5;
    uint64_t r10_2;
    uint64_t rsi_3;
    uint64_t r89_3;
    uint64_t r910_3;
    struct bb_skip_ret_type mrv;
    struct bb_skip_ret_type mrv1;
    struct bb_skip_ret_type mrv2;
    struct bb_skip_ret_type mrv3;
    struct bb_skip_ret_type mrv4;
    struct bb_skip_ret_type mrv5;
    struct bb_skip_ret_type mrv6;
    unsigned int loop_state_var;
    var_0 = revng_init_local_sp(0UL);
    var_1 = init_rax();
    var_2 = init_rbx();
    var_3 = init_r14();
    var_4 = init_rbp();
    var_5 = init_r13();
    var_6 = init_r12();
    var_7 = init_r15();
    var_8 = init_cc_src2();
    var_9 = init_r10();
    var_10 = init_rsi();
    *(uint64_t *)(var_0 + (-8L)) = var_7;
    *(uint64_t *)(var_0 + (-16L)) = var_3;
    *(uint64_t *)(var_0 + (-24L)) = var_5;
    *(uint64_t *)(var_0 + (-32L)) = var_6;
    *(uint64_t *)(var_0 + (-40L)) = var_4;
    *(uint64_t *)(var_0 + (-48L)) = var_2;
    var_11 = var_0 + (-1224L);
    rbp_0 = 1024UL;
    rdi7_2 = 0UL;
    rax_2 = 1UL;
    r12_2 = 0UL;
    rcx8_3 = rcx;
    rax_1 = var_1;
    r10_0 = var_9;
    r12_3 = 1UL;
    local_sp_5 = var_11;
    r89_1 = r8;
    r910_1 = r9;
    rcx8_2 = rcx;
    local_sp_4 = var_11;
    rbx_3 = rdi;
    r89_0 = r8;
    r910_0 = r9;
    rdi7_4 = 0UL;
    rcx8_5 = rcx;
    r10_2 = var_9;
    rsi_3 = var_10;
    r89_3 = r8;
    r910_3 = r9;
    if (rdi == 0UL) {
        mrv.field_0 = rax_2;
        mrv1 = mrv;
        mrv1.field_1 = rdi7_4;
        mrv2 = mrv1;
        mrv2.field_2 = rcx8_5;
        mrv3 = mrv2;
        mrv3.field_3 = r10_2;
        mrv4 = mrv3;
        mrv4.field_4 = rsi_3;
        mrv5 = mrv4;
        mrv5.field_5 = r89_3;
        mrv6 = mrv5;
        mrv6.field_6 = r910_3;
        return mrv6;
    }
    rdi7_4 = 1UL;
    if (*(uint64_t *)6383432UL == 0UL) {
        while (1U)
            {
                var_12 = local_sp_4 + (-8L);
                *(uint64_t *)var_12 = 4205707UL;
                indirect_placeholder();
                var_13 = (uint64_t)(uint32_t)rax_1;
                var_14 = local_sp_4 + (-16L);
                *(uint64_t *)var_14 = 4205717UL;
                indirect_placeholder();
                local_sp_3 = var_14;
                rbx_2 = rbx_3;
                r12_1 = r12_3;
                rcx8_1 = rcx8_2;
                rsi_1 = var_12;
                rbx_1 = rbx_3;
                local_sp_2 = var_14;
                r10_2 = r10_0;
                r89_3 = r89_0;
                r910_3 = r910_0;
                if (var_13 == 0UL) {
                    var_22 = (uint32_t)((uint16_t)*(uint32_t *)(local_sp_4 + 8UL) & (unsigned short)53248U);
                    rax_0 = (uint64_t)var_22;
                    r12_2 = r12_3;
                    if ((uint64_t)((var_22 + (-32768)) & (-12288)) != 0UL) {
                        loop_state_var = 1U;
                        break;
                    }
                    var_23 = *(uint64_t *)(local_sp_4 + 40UL);
                    var_24 = ((var_23 + (-1L)) > 2305843009213693951UL) ? 512UL : var_23;
                    var_25 = *(uint64_t *)(local_sp_4 + 32UL);
                    rax_0 = var_24;
                    rsi_1 = rbx_3;
                    if ((long)var_24 >= (long)var_25) {
                        loop_state_var = 1U;
                        break;
                    }
                    var_35 = helper_cc_compute_c_wrapper(var_25 - rbx_3, rbx_3, var_8, 17U);
                    if (var_35 == 0UL) {
                        var_36 = rbx_3 - var_25;
                        rsi_1 = var_12;
                        rbx_1 = var_36;
                        if (var_36 != 0UL) {
                            loop_state_var = 0U;
                            break;
                        }
                    }
                    var_48 = *(uint64_t *)6383432UL;
                    *(uint64_t *)(local_sp_4 + (-24L)) = 4205815UL;
                    indirect_placeholder();
                    rdi7_2 = var_48;
                    if ((uint64_t)(uint32_t)var_24 != 0UL) {
                        loop_state_var = 0U;
                        break;
                    }
                    *(uint64_t *)(local_sp_4 + (-32L)) = 4205828UL;
                    indirect_placeholder();
                    r12_1 = 0UL;
                    loop_state_var = 0U;
                    break;
                }
                var_15 = *(uint64_t *)6383448UL;
                *(uint64_t *)(local_sp_4 + (-24L)) = 4205969UL;
                var_16 = indirect_placeholder_6(var_15, 0UL, 3UL);
                var_17 = var_16.field_0;
                var_18 = var_16.field_1;
                var_19 = var_16.field_2;
                *(uint64_t *)(local_sp_4 + (-32L)) = 4205977UL;
                indirect_placeholder();
                var_20 = (uint64_t)*(uint32_t *)var_17;
                var_21 = local_sp_4 + (-40L);
                *(uint64_t *)var_21 = 4206002UL;
                indirect_placeholder_4(0UL, 4270612UL, 0UL, var_17, var_20, var_18, var_19);
                local_sp_2 = var_21;
                *(uint64_t *)(local_sp_2 + (-8L)) = 4206016UL;
                var_37 = indirect_placeholder_5(0UL);
                var_38 = (uint64_t)((uint32_t)r12_2 & (uint32_t)var_37.field_0);
                var_39 = local_sp_2 + (-16L);
                *(uint64_t *)var_39 = 4206024UL;
                var_40 = indirect_placeholder_3();
                var_41 = var_40.field_0;
                var_42 = var_38 & var_41;
                rax_2 = var_42;
                rax_1 = var_41;
                r12_3 = var_42;
                local_sp_5 = var_39;
                local_sp_4 = var_39;
                rbx_3 = rbx_1;
                if (*(uint64_t *)6383432UL != 0UL) {
                    var_43 = var_40.field_1;
                    var_44 = var_40.field_2;
                    var_45 = var_40.field_3;
                    var_46 = var_40.field_4;
                    var_47 = var_40.field_5;
                    rcx8_3 = var_43;
                    r89_1 = var_46;
                    r910_1 = var_47;
                    rcx8_5 = var_43;
                    r10_2 = var_44;
                    rsi_3 = var_45;
                    r89_3 = var_46;
                    r910_3 = var_47;
                    if (rbx_1 != 0UL) {
                        loop_state_var = 3U;
                        break;
                    }
                    loop_state_var = 2U;
                    break;
                }
                rcx8_2 = var_40.field_1;
                r10_0 = var_40.field_2;
                r89_0 = var_40.field_4;
                r910_0 = var_40.field_5;
                continue;
            }
        switch (loop_state_var) {
          case 2U:
            {
                break;
            }
            break;
          case 3U:
            {
                *(uint64_t *)(local_sp_5 + (-8L)) = 4206083UL;
                var_49 = indirect_placeholder_7(0UL, 4266312UL, 1UL, rcx8_3, 0UL, r89_1, r910_1);
                rcx8_5 = var_49.field_0;
                r10_2 = var_49.field_1;
                rsi_3 = var_49.field_2;
                r89_3 = var_49.field_3;
                r910_3 = var_49.field_4;
            }
            break;
          case 1U:
          case 0U:
            {
                switch (loop_state_var) {
                  case 0U:
                    {
                        rax_2 = (uint64_t)(uint32_t)r12_1;
                        rdi7_4 = rdi7_2;
                        rcx8_5 = rcx8_1;
                        rsi_3 = rsi_1;
                    }
                    break;
                  case 1U:
                    {
                        if (rbx_3 != 0UL) {
                            var_26 = ((uint64_t)(uint32_t)rax_0 == 0UL);
                            r12_1 = 0UL;
                            rsi_1 = 1UL;
                            rdi7_2 = rdi7_0;
                            r12_1 = r12_3;
                            local_sp_3 = local_sp_0;
                            do {
                                var_27 = (rbp_0 > rbx_2) ? rbx_2 : rbp_0;
                                var_28 = *(uint64_t *)6383432UL;
                                var_29 = local_sp_3 + 144UL;
                                var_30 = local_sp_3 + (-8L);
                                *(uint64_t *)var_30 = 4205874UL;
                                indirect_placeholder();
                                var_31 = rbx_2 - rax_0;
                                rbp_0 = var_27;
                                rbx_2 = var_31;
                                rdi7_0 = var_29;
                                local_sp_0 = var_30;
                                rcx8_1 = var_28;
                                if (rax_0 != var_27) {
                                    var_32 = *(uint64_t *)6383432UL;
                                    *(uint64_t *)(local_sp_3 + (-16L)) = 4205894UL;
                                    indirect_placeholder();
                                    rdi7_2 = var_32;
                                    if (!var_26) {
                                        *(uint64_t *)(local_sp_3 + (-24L)) = 4205903UL;
                                        indirect_placeholder();
                                        break;
                                    }
                                    var_33 = *(uint64_t *)6383432UL;
                                    var_34 = local_sp_3 + (-24L);
                                    *(uint64_t *)var_34 = 4205931UL;
                                    indirect_placeholder();
                                    rdi7_0 = var_33;
                                    local_sp_0 = var_34;
                                }
                                rdi7_2 = rdi7_0;
                                r12_1 = r12_3;
                                local_sp_3 = local_sp_0;
                            } while (var_31 != 0UL);
                        }
                    }
                    break;
                }
            }
            break;
        }
    } else {
        *(uint64_t *)(local_sp_5 + (-8L)) = 4206083UL;
        var_49 = indirect_placeholder_7(0UL, 4266312UL, 1UL, rcx8_3, 0UL, r89_1, r910_1);
        rcx8_5 = var_49.field_0;
        r10_2 = var_49.field_1;
        rsi_3 = var_49.field_2;
        r89_3 = var_49.field_3;
        r910_3 = var_49.field_4;
    }
}

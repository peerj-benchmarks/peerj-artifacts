typedef unsigned char __u_char;
typedef unsigned short __u_short;
typedef unsigned int __u_int;
typedef unsigned long __u_long;
typedef signed char __int8_t;
typedef unsigned char __uint8_t;
typedef short __int16_t;
typedef unsigned short __uint16_t;
typedef int __int32_t;
typedef unsigned int __uint32_t;
typedef long __int64_t;
typedef unsigned long __uint64_t;
typedef __int8_t __int_least8_t;
typedef __uint8_t __uint_least8_t;
typedef __int16_t __int_least16_t;
typedef __uint16_t __uint_least16_t;
typedef __int32_t __int_least32_t;
typedef __uint32_t __uint_least32_t;
typedef __int64_t __int_least64_t;
typedef __uint64_t __uint_least64_t;
typedef long __quad_t;
typedef unsigned long __u_quad_t;
typedef long __intmax_t;
typedef unsigned long __uintmax_t;
typedef unsigned long __dev_t;
typedef unsigned int __uid_t;
typedef unsigned int __gid_t;
typedef unsigned long __ino_t;
typedef unsigned long __ino64_t;
typedef unsigned int __mode_t;
typedef unsigned long __nlink_t;
typedef long __off_t;
typedef long __off64_t;
typedef int __pid_t;
typedef struct {
    int __val[2];
} __fsid_t;
typedef long __clock_t;
typedef unsigned long __rlim_t;
typedef unsigned long __rlim64_t;
typedef unsigned int __id_t;
typedef long __time_t;
typedef unsigned int __useconds_t;
typedef long __suseconds_t;
typedef int __daddr_t;
typedef int __key_t;
typedef int __clockid_t;
typedef void *__timer_t;
typedef long __blksize_t;
typedef long __blkcnt_t;
typedef long __blkcnt64_t;
typedef unsigned long __fsblkcnt_t;
typedef unsigned long __fsblkcnt64_t;
typedef unsigned long __fsfilcnt_t;
typedef unsigned long __fsfilcnt64_t;
typedef long __fsword_t;
typedef long __ssize_t;
typedef long __syscall_slong_t;
typedef unsigned long __syscall_ulong_t;
typedef __off64_t __loff_t;
typedef char *__caddr_t;
typedef long __intptr_t;
typedef unsigned int __socklen_t;
typedef int __sig_atomic_t;
typedef __int8_t int8_t;
typedef __int16_t int16_t;
typedef __int32_t int32_t;
typedef __int64_t int64_t;
typedef __uint8_t uint8_t;
typedef __uint16_t uint16_t;
typedef __uint32_t uint32_t;
typedef __uint64_t uint64_t;
typedef __int_least8_t int_least8_t;
typedef __int_least16_t int_least16_t;
typedef __int_least32_t int_least32_t;
typedef __int_least64_t int_least64_t;
typedef __uint_least8_t uint_least8_t;
typedef __uint_least16_t uint_least16_t;
typedef __uint_least32_t uint_least32_t;
typedef __uint_least64_t uint_least64_t;
typedef signed char int_fast8_t;
typedef long int_fast16_t;
typedef long int_fast32_t;
typedef long int_fast64_t;
typedef unsigned char uint_fast8_t;
typedef unsigned long uint_fast16_t;
typedef unsigned long uint_fast32_t;
typedef unsigned long uint_fast64_t;
typedef long intptr_t;
typedef unsigned long uintptr_t;
typedef __intmax_t intmax_t;
typedef __uintmax_t uintmax_t;
typedef _Bool bool;
extern uint64_t revng_init_local_sp(uint64_t param_0);
extern uint64_t init_rbx(void);
extern void indirect_placeholder_1(void);
extern uint64_t init_rbp(void);
extern uint64_t init_r14(void);
extern uint64_t init_r13(void);
extern uint64_t init_r12(void);
extern uint64_t init_r15(void);
extern uint64_t init_rax(void);
uint64_t bb_find_field(uint64_t rdi) {
    uint64_t var_0;
    uint64_t var_1;
    uint64_t var_2;
    uint64_t var_3;
    uint64_t var_4;
    uint64_t var_5;
    uint64_t var_6;
    uint64_t var_7;
    uint64_t var_8;
    uint64_t *var_9;
    uint64_t var_10;
    uint64_t var_11;
    uint64_t rbx_4;
    uint64_t var_17;
    uint64_t local_sp_0;
    uint64_t rbx_0;
    unsigned char var_15;
    uint64_t var_16;
    uint64_t local_sp_2;
    uint64_t var_18;
    uint64_t local_sp_3;
    bool var_12;
    uint64_t rbx_2;
    uint64_t local_sp_1;
    uint64_t rbx_3;
    uint64_t rbx_1;
    uint64_t r15_0;
    uint64_t var_19;
    unsigned char var_13;
    uint64_t var_14;
    uint64_t var_20;
    uint64_t var_21;
    unsigned int loop_state_var;
    bool switch_state_var;
    var_0 = revng_init_local_sp(0UL);
    var_1 = init_rax();
    var_2 = init_rbx();
    var_3 = init_r14();
    var_4 = init_rbp();
    var_5 = init_r13();
    var_6 = init_r12();
    var_7 = init_r15();
    *(uint64_t *)(var_0 + (-8L)) = var_7;
    *(uint64_t *)(var_0 + (-16L)) = var_3;
    *(uint64_t *)(var_0 + (-24L)) = var_5;
    *(uint64_t *)(var_0 + (-32L)) = var_6;
    *(uint64_t *)(var_0 + (-40L)) = var_4;
    *(uint64_t *)(var_0 + (-48L)) = var_2;
    var_8 = var_0 + (-56L);
    var_9 = (uint64_t *)(rdi + 16UL);
    var_10 = *var_9;
    var_11 = *(uint64_t *)(rdi + 8UL) + (-1L);
    local_sp_1 = var_8;
    rbx_1 = 0UL;
    r15_0 = 0UL;
    rbx_4 = 0UL;
    if (~(var_11 != 0UL & *(uint64_t *)6366576UL != 0UL)) {
        var_20 = var_11 - rbx_4;
        var_21 = *(uint64_t *)6366568UL;
        return (((var_20 > var_21) ? var_21 : var_20) + rbx_4) + *var_9;
    }
    var_12 = ((uint64_t)(uint32_t)var_1 == 0UL);
    while (1U)
        {
            local_sp_2 = local_sp_1;
            rbx_2 = rbx_1;
            local_sp_3 = local_sp_1;
            rbx_3 = rbx_1;
            if (var_11 > rbx_1) {
                var_19 = r15_0 + 1UL;
                local_sp_1 = local_sp_2;
                rbx_1 = rbx_2;
                r15_0 = var_19;
                rbx_4 = rbx_2;
                if (var_11 > rbx_2) {
                    break;
                }
                if (*(uint64_t *)6366576UL <= var_19) {
                    continue;
                }
                break;
            }
            while (1U)
                {
                    var_13 = *(unsigned char *)(rbx_3 + var_10);
                    var_14 = local_sp_3 + (-8L);
                    *(uint64_t *)var_14 = 4202125UL;
                    indirect_placeholder_1();
                    rbx_4 = rbx_3;
                    local_sp_0 = var_14;
                    rbx_0 = rbx_3;
                    local_sp_2 = var_14;
                    local_sp_3 = var_14;
                    if (!((var_13 == '\n') || (var_12 ^ 1))) {
                        loop_state_var = 1U;
                        break;
                    }
                    var_18 = rbx_3 + 1UL;
                    rbx_2 = var_18;
                    rbx_3 = var_18;
                    if (var_11 > var_18) {
                        continue;
                    }
                    loop_state_var = 0U;
                    break;
                }
            if (var_11 > rbx_3) {
                switch_state_var = 1;
                break;
            }
            while (1U)
                {
                    var_15 = *(unsigned char *)(rbx_0 + var_10);
                    var_16 = local_sp_0 + (-8L);
                    *(uint64_t *)var_16 = 4202166UL;
                    indirect_placeholder_1();
                    local_sp_0 = var_16;
                    local_sp_2 = var_16;
                    rbx_2 = rbx_0;
                    if (var_15 != '\n') {
                        loop_state_var = 0U;
                        break;
                    }
                    var_17 = rbx_0 + 1UL;
                    rbx_0 = var_17;
                    rbx_4 = var_17;
                    if (var_11 > var_17) {
                        continue;
                    }
                    loop_state_var = 1U;
                    break;
                }
            switch_state_var = 0;
            switch (loop_state_var) {
              case 1U:
                {
                    switch_state_var = 1;
                    break;
                }
                break;
              case 0U:
                {
                    break;
                }
                break;
            }
            if (switch_state_var)
                break;
        }
    var_20 = var_11 - rbx_4;
    var_21 = *(uint64_t *)6366568UL;
    return (((var_20 > var_21) ? var_21 : var_20) + rbx_4) + *var_9;
}

typedef unsigned char __u_char;
typedef unsigned short __u_short;
typedef unsigned int __u_int;
typedef unsigned long __u_long;
typedef signed char __int8_t;
typedef unsigned char __uint8_t;
typedef short __int16_t;
typedef unsigned short __uint16_t;
typedef int __int32_t;
typedef unsigned int __uint32_t;
typedef long __int64_t;
typedef unsigned long __uint64_t;
typedef __int8_t __int_least8_t;
typedef __uint8_t __uint_least8_t;
typedef __int16_t __int_least16_t;
typedef __uint16_t __uint_least16_t;
typedef __int32_t __int_least32_t;
typedef __uint32_t __uint_least32_t;
typedef __int64_t __int_least64_t;
typedef __uint64_t __uint_least64_t;
typedef long __quad_t;
typedef unsigned long __u_quad_t;
typedef long __intmax_t;
typedef unsigned long __uintmax_t;
typedef unsigned long __dev_t;
typedef unsigned int __uid_t;
typedef unsigned int __gid_t;
typedef unsigned long __ino_t;
typedef unsigned long __ino64_t;
typedef unsigned int __mode_t;
typedef unsigned long __nlink_t;
typedef long __off_t;
typedef long __off64_t;
typedef int __pid_t;
typedef struct {
    int __val[2];
} __fsid_t;
typedef long __clock_t;
typedef unsigned long __rlim_t;
typedef unsigned long __rlim64_t;
typedef unsigned int __id_t;
typedef long __time_t;
typedef unsigned int __useconds_t;
typedef long __suseconds_t;
typedef int __daddr_t;
typedef int __key_t;
typedef int __clockid_t;
typedef void *__timer_t;
typedef long __blksize_t;
typedef long __blkcnt_t;
typedef long __blkcnt64_t;
typedef unsigned long __fsblkcnt_t;
typedef unsigned long __fsblkcnt64_t;
typedef unsigned long __fsfilcnt_t;
typedef unsigned long __fsfilcnt64_t;
typedef long __fsword_t;
typedef long __ssize_t;
typedef long __syscall_slong_t;
typedef unsigned long __syscall_ulong_t;
typedef __off64_t __loff_t;
typedef char *__caddr_t;
typedef long __intptr_t;
typedef unsigned int __socklen_t;
typedef int __sig_atomic_t;
typedef __int8_t int8_t;
typedef __int16_t int16_t;
typedef __int32_t int32_t;
typedef __int64_t int64_t;
typedef __uint8_t uint8_t;
typedef __uint16_t uint16_t;
typedef __uint32_t uint32_t;
typedef __uint64_t uint64_t;
typedef __int_least8_t int_least8_t;
typedef __int_least16_t int_least16_t;
typedef __int_least32_t int_least32_t;
typedef __int_least64_t int_least64_t;
typedef __uint_least8_t uint_least8_t;
typedef __uint_least16_t uint_least16_t;
typedef __uint_least32_t uint_least32_t;
typedef __uint_least64_t uint_least64_t;
typedef signed char int_fast8_t;
typedef long int_fast16_t;
typedef long int_fast32_t;
typedef long int_fast64_t;
typedef unsigned char uint_fast8_t;
typedef unsigned long uint_fast16_t;
typedef unsigned long uint_fast32_t;
typedef unsigned long uint_fast64_t;
typedef long intptr_t;
typedef unsigned long uintptr_t;
typedef __intmax_t intmax_t;
typedef __uintmax_t uintmax_t;
typedef _Bool bool;
struct indirect_placeholder_15_ret_type;
struct indirect_placeholder_15_ret_type {
    uint64_t field_0;
    uint64_t field_1;
    uint64_t field_2;
};
extern void abort(void);
extern uint64_t revng_init_local_sp(uint64_t param_0);
extern uint64_t init_rbx(void);
extern void indirect_placeholder_1(void);
extern uint64_t init_rbp(void);
extern uint64_t indirect_placeholder_2(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint64_t param_3, uint64_t param_4);
extern void indirect_placeholder_8(uint64_t param_0);
extern void indirect_placeholder_11(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint64_t param_3, uint64_t param_4, uint64_t param_5, uint64_t param_6);
extern void indirect_placeholder_12(uint64_t param_0, uint64_t param_1, uint64_t param_2);
extern struct indirect_placeholder_15_ret_type indirect_placeholder_15(uint64_t param_0);
void bb_main(uint64_t rdi, uint64_t rsi) {
    uint64_t var_6;
    uint32_t var_8;
    uint64_t var_7;
    uint64_t var_0;
    uint64_t var_1;
    uint64_t var_2;
    uint64_t var_3;
    uint64_t var_4;
    uint64_t var_5;
    uint64_t local_sp_1;
    unsigned char storemerge;
    uint64_t var_12;
    uint64_t var_13;
    uint64_t var_14;
    uint64_t var_17;
    struct indirect_placeholder_15_ret_type var_18;
    uint64_t var_19;
    uint64_t var_20;
    uint64_t var_21;
    bool var_15;
    uint64_t *var_16;
    uint64_t local_sp_0;
    uint64_t var_9;
    uint64_t var_10;
    uint64_t var_11;
    unsigned int loop_state_var;
    var_0 = revng_init_local_sp(0UL);
    var_1 = init_rbx();
    var_2 = init_rbp();
    *(uint64_t *)(var_0 + (-8L)) = var_2;
    *(uint64_t *)(var_0 + (-16L)) = var_1;
    var_3 = (uint64_t)(uint32_t)rdi;
    var_4 = *(uint64_t *)rsi;
    *(uint64_t *)(var_0 + (-32L)) = 4201693UL;
    indirect_placeholder_8(var_4);
    *(uint64_t *)(var_0 + (-40L)) = 4201708UL;
    indirect_placeholder_1();
    *(uint32_t *)6353864UL = 3U;
    var_5 = var_0 + (-48L);
    *(uint64_t *)var_5 = 4201728UL;
    indirect_placeholder_1();
    local_sp_1 = var_5;
    storemerge = (unsigned char)'\x00';
    while (1U)
        {
            *(unsigned char *)6354128UL = storemerge;
            var_6 = local_sp_1 + (-8L);
            *(uint64_t *)var_6 = 4201869UL;
            var_7 = indirect_placeholder_2(4246646UL, var_3, 4246464UL, rsi, 0UL);
            var_8 = (uint32_t)var_7;
            local_sp_0 = var_6;
            local_sp_1 = var_6;
            storemerge = (unsigned char)'\x01';
            if ((uint64_t)(var_8 + 1U) == 0UL) {
                if ((uint64_t)(var_8 + 130U) == 0UL) {
                    *(uint64_t *)(local_sp_1 + (-16L)) = 4201777UL;
                    indirect_placeholder_12(rsi, var_3, 0UL);
                    abort();
                }
                if ((uint64_t)(var_8 + (-115)) == 0UL) {
                    continue;
                }
                if ((uint64_t)(var_8 + 131U) != 0UL) {
                    loop_state_var = 1U;
                    break;
                }
                var_9 = *(uint64_t *)6353856UL;
                var_10 = *(uint64_t *)6353984UL;
                *(uint64_t *)(local_sp_1 + (-16L)) = 4201823UL;
                indirect_placeholder_11(0UL, 4245814UL, var_10, var_9, 4245861UL, 0UL, 4245865UL);
                var_11 = local_sp_1 + (-24L);
                *(uint64_t *)var_11 = 4201833UL;
                indirect_placeholder_1();
                local_sp_0 = var_11;
                loop_state_var = 1U;
                break;
            }
            var_12 = (uint64_t)*(uint32_t *)6353980UL;
            var_13 = rdi << 32UL;
            var_14 = var_12 << 32UL;
            if ((long)var_13 <= (long)var_14) {
                *(uint64_t *)(local_sp_1 + (-16L)) = 4201942UL;
                indirect_placeholder_1();
                *(uint32_t *)var_12 = 2U;
                var_15 = (*(unsigned char *)6354128UL == '\x00');
                var_16 = (uint64_t *)(local_sp_1 + (-24L));
                if (var_15) {
                    *var_16 = 4201967UL;
                    indirect_placeholder_1();
                    loop_state_var = 0U;
                    break;
                }
                *var_16 = 4201987UL;
                indirect_placeholder_1();
                *(uint64_t *)(local_sp_1 + (-32L)) = 4202012UL;
                indirect_placeholder_1();
                loop_state_var = 0U;
                break;
            }
            var_17 = *(uint64_t *)((uint64_t)((long)var_14 >> (long)29UL) + rsi);
            *(uint64_t *)(local_sp_1 + (-16L)) = 4201899UL;
            var_18 = indirect_placeholder_15(var_17);
            var_19 = var_18.field_0;
            var_20 = var_18.field_1;
            var_21 = var_18.field_2;
            *(uint64_t *)(local_sp_1 + (-24L)) = 4201927UL;
            indirect_placeholder_11(0UL, 4245881UL, 0UL, var_19, 0UL, var_20, var_21);
            *(uint64_t *)(local_sp_1 + (-32L)) = 4201937UL;
            indirect_placeholder_12(rsi, var_3, 2UL);
            abort();
        }
    switch (loop_state_var) {
      case 0U:
        {
            return;
        }
        break;
      case 1U:
        {
            *(uint64_t *)(local_sp_0 + (-8L)) = 4201843UL;
            indirect_placeholder_12(rsi, var_3, 2UL);
            abort();
        }
        break;
    }
}

typedef unsigned char __u_char;
typedef unsigned short __u_short;
typedef unsigned int __u_int;
typedef unsigned long __u_long;
typedef signed char __int8_t;
typedef unsigned char __uint8_t;
typedef short __int16_t;
typedef unsigned short __uint16_t;
typedef int __int32_t;
typedef unsigned int __uint32_t;
typedef long __int64_t;
typedef unsigned long __uint64_t;
typedef __int8_t __int_least8_t;
typedef __uint8_t __uint_least8_t;
typedef __int16_t __int_least16_t;
typedef __uint16_t __uint_least16_t;
typedef __int32_t __int_least32_t;
typedef __uint32_t __uint_least32_t;
typedef __int64_t __int_least64_t;
typedef __uint64_t __uint_least64_t;
typedef long __quad_t;
typedef unsigned long __u_quad_t;
typedef long __intmax_t;
typedef unsigned long __uintmax_t;
typedef unsigned long __dev_t;
typedef unsigned int __uid_t;
typedef unsigned int __gid_t;
typedef unsigned long __ino_t;
typedef unsigned long __ino64_t;
typedef unsigned int __mode_t;
typedef unsigned long __nlink_t;
typedef long __off_t;
typedef long __off64_t;
typedef int __pid_t;
typedef struct {
    int __val[2];
} __fsid_t;
typedef long __clock_t;
typedef unsigned long __rlim_t;
typedef unsigned long __rlim64_t;
typedef unsigned int __id_t;
typedef long __time_t;
typedef unsigned int __useconds_t;
typedef long __suseconds_t;
typedef int __daddr_t;
typedef int __key_t;
typedef int __clockid_t;
typedef void *__timer_t;
typedef long __blksize_t;
typedef long __blkcnt_t;
typedef long __blkcnt64_t;
typedef unsigned long __fsblkcnt_t;
typedef unsigned long __fsblkcnt64_t;
typedef unsigned long __fsfilcnt_t;
typedef unsigned long __fsfilcnt64_t;
typedef long __fsword_t;
typedef long __ssize_t;
typedef long __syscall_slong_t;
typedef unsigned long __syscall_ulong_t;
typedef __off64_t __loff_t;
typedef char *__caddr_t;
typedef long __intptr_t;
typedef unsigned int __socklen_t;
typedef int __sig_atomic_t;
typedef __int8_t int8_t;
typedef __int16_t int16_t;
typedef __int32_t int32_t;
typedef __int64_t int64_t;
typedef __uint8_t uint8_t;
typedef __uint16_t uint16_t;
typedef __uint32_t uint32_t;
typedef __uint64_t uint64_t;
typedef __int_least8_t int_least8_t;
typedef __int_least16_t int_least16_t;
typedef __int_least32_t int_least32_t;
typedef __int_least64_t int_least64_t;
typedef __uint_least8_t uint_least8_t;
typedef __uint_least16_t uint_least16_t;
typedef __uint_least32_t uint_least32_t;
typedef __uint_least64_t uint_least64_t;
typedef signed char int_fast8_t;
typedef long int_fast16_t;
typedef long int_fast32_t;
typedef long int_fast64_t;
typedef unsigned char uint_fast8_t;
typedef unsigned long uint_fast16_t;
typedef unsigned long uint_fast32_t;
typedef unsigned long uint_fast64_t;
typedef long intptr_t;
typedef unsigned long uintptr_t;
typedef __intmax_t intmax_t;
typedef __uintmax_t uintmax_t;
extern uint64_t revng_init_local_sp(uint64_t param_0);
extern void indirect_placeholder_1(void);
uint64_t bb_parse_omp_threads(uint64_t rdi) {
    uint64_t var_0;
    unsigned char var_8;
    uint64_t rdx_0_in;
    unsigned char rcx_0_in;
    uint64_t rdx_0;
    unsigned char var_9;
    uint64_t rax_0;
    unsigned char var_1;
    unsigned char rax_1_in;
    uint64_t rdi1_0;
    uint64_t var_2;
    unsigned char var_3;
    uint64_t var_4;
    uint64_t var_5;
    uint64_t *var_6;
    uint64_t var_7;
    unsigned int loop_state_var;
    var_0 = revng_init_local_sp(0UL);
    rax_0 = 0UL;
    rdi1_0 = rdi;
    if (rdi == 0UL) {
        return rax_0;
    }
    var_1 = *(unsigned char *)rdi;
    rax_1_in = var_1;
    if (var_1 == '\x00') {
        return;
    }
    while (1U)
        {
            if ((signed char)rax_1_in >= '\t') {
                loop_state_var = 0U;
                break;
            }
            if ((signed char)rax_1_in <= '\r') {
                if ((uint64_t)(rax_1_in + '\xe0') != 0UL) {
                    loop_state_var = 0U;
                    break;
                }
            }
            var_2 = rdi1_0 + 1UL;
            var_3 = *(unsigned char *)var_2;
            rax_1_in = var_3;
            rdi1_0 = var_2;
            if (var_3 == '\x00') {
                continue;
            }
            loop_state_var = 1U;
            break;
        }
    var_4 = (uint64_t)((uint32_t)(uint64_t)rax_1_in + (-48));
    if ((uint64_t)((unsigned char)var_4 & '\xfe') <= 9UL) {
        var_5 = var_0 + (-24L);
        *(uint64_t *)(var_0 + (-16L)) = 0UL;
        *(uint64_t *)(var_0 + (-32L)) = 4202587UL;
        indirect_placeholder_1();
        var_6 = (uint64_t *)var_5;
        var_7 = *var_6;
        rdx_0_in = var_7;
        rax_0 = var_4;
        if (var_7 == 0UL) {
            var_8 = *(unsigned char *)var_7;
            rcx_0_in = var_8;
            if (var_8 != '\x00') {
                while (1U)
                    {
                        rdx_0 = rdx_0_in + 1UL;
                        rdx_0_in = rdx_0;
                        if ((signed char)rcx_0_in >= '\t') {
                            loop_state_var = 1U;
                            break;
                        }
                        if ((signed char)rcx_0_in <= '\r') {
                            if ((uint64_t)(rcx_0_in + '\xe0') != 0UL) {
                                loop_state_var = 1U;
                                break;
                            }
                        }
                        *var_6 = rdx_0;
                        var_9 = *(unsigned char *)rdx_0;
                        rcx_0_in = var_9;
                        if (var_9 == '\x00') {
                            continue;
                        }
                        loop_state_var = 0U;
                        break;
                    }
                if ((uint64_t)(rcx_0_in + '\xd4') == 0UL) {
                    rax_0 = 0UL;
                }
            }
        } else {
            rax_0 = 0UL;
        }
    }
}

typedef unsigned char __u_char;
typedef unsigned short __u_short;
typedef unsigned int __u_int;
typedef unsigned long __u_long;
typedef signed char __int8_t;
typedef unsigned char __uint8_t;
typedef short __int16_t;
typedef unsigned short __uint16_t;
typedef int __int32_t;
typedef unsigned int __uint32_t;
typedef long __int64_t;
typedef unsigned long __uint64_t;
typedef __int8_t __int_least8_t;
typedef __uint8_t __uint_least8_t;
typedef __int16_t __int_least16_t;
typedef __uint16_t __uint_least16_t;
typedef __int32_t __int_least32_t;
typedef __uint32_t __uint_least32_t;
typedef __int64_t __int_least64_t;
typedef __uint64_t __uint_least64_t;
typedef long __quad_t;
typedef unsigned long __u_quad_t;
typedef long __intmax_t;
typedef unsigned long __uintmax_t;
typedef unsigned long __dev_t;
typedef unsigned int __uid_t;
typedef unsigned int __gid_t;
typedef unsigned long __ino_t;
typedef unsigned long __ino64_t;
typedef unsigned int __mode_t;
typedef unsigned long __nlink_t;
typedef long __off_t;
typedef long __off64_t;
typedef int __pid_t;
typedef struct {
    int __val[2];
} __fsid_t;
typedef long __clock_t;
typedef unsigned long __rlim_t;
typedef unsigned long __rlim64_t;
typedef unsigned int __id_t;
typedef long __time_t;
typedef unsigned int __useconds_t;
typedef long __suseconds_t;
typedef int __daddr_t;
typedef int __key_t;
typedef int __clockid_t;
typedef void *__timer_t;
typedef long __blksize_t;
typedef long __blkcnt_t;
typedef long __blkcnt64_t;
typedef unsigned long __fsblkcnt_t;
typedef unsigned long __fsblkcnt64_t;
typedef unsigned long __fsfilcnt_t;
typedef unsigned long __fsfilcnt64_t;
typedef long __fsword_t;
typedef long __ssize_t;
typedef long __syscall_slong_t;
typedef unsigned long __syscall_ulong_t;
typedef __off64_t __loff_t;
typedef char *__caddr_t;
typedef long __intptr_t;
typedef unsigned int __socklen_t;
typedef int __sig_atomic_t;
typedef __int8_t int8_t;
typedef __int16_t int16_t;
typedef __int32_t int32_t;
typedef __int64_t int64_t;
typedef __uint8_t uint8_t;
typedef __uint16_t uint16_t;
typedef __uint32_t uint32_t;
typedef __uint64_t uint64_t;
typedef __int_least8_t int_least8_t;
typedef __int_least16_t int_least16_t;
typedef __int_least32_t int_least32_t;
typedef __int_least64_t int_least64_t;
typedef __uint_least8_t uint_least8_t;
typedef __uint_least16_t uint_least16_t;
typedef __uint_least32_t uint_least32_t;
typedef __uint_least64_t uint_least64_t;
typedef signed char int_fast8_t;
typedef long int_fast16_t;
typedef long int_fast32_t;
typedef long int_fast64_t;
typedef unsigned char uint_fast8_t;
typedef unsigned long uint_fast16_t;
typedef unsigned long uint_fast32_t;
typedef unsigned long uint_fast64_t;
typedef long intptr_t;
typedef unsigned long uintptr_t;
typedef __intmax_t intmax_t;
typedef __uintmax_t uintmax_t;
struct indirect_placeholder_9_ret_type;
struct indirect_placeholder_9_ret_type {
    uint64_t field_0;
    uint64_t field_1;
    uint64_t field_2;
};
extern void abort(void);
extern uint64_t revng_init_local_sp(uint64_t param_0);
extern uint64_t init_rbx(void);
extern void indirect_placeholder_1(void);
extern uint64_t init_rbp(void);
extern uint64_t init_r14(void);
extern uint64_t init_r13(void);
extern uint64_t init_r12(void);
extern uint64_t indirect_placeholder(uint64_t param_0);
extern uint64_t indirect_placeholder_2(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint64_t param_3, uint64_t param_4);
extern void indirect_placeholder_6(uint64_t param_0);
extern void indirect_placeholder_8(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint64_t param_3, uint64_t param_4, uint64_t param_5, uint64_t param_6);
extern uint64_t indirect_placeholder_3(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint64_t param_3, uint64_t param_4, uint64_t param_5);
extern void indirect_placeholder_7(uint64_t param_0, uint64_t param_1, uint64_t param_2);
extern struct indirect_placeholder_9_ret_type indirect_placeholder_9(uint64_t param_0);
typedef _Bool bool;
void bb_main(uint64_t rdi, uint64_t rsi) {
    uint64_t var_11;
    uint64_t var_0;
    uint64_t var_1;
    uint64_t var_2;
    uint64_t var_3;
    uint64_t var_4;
    uint64_t var_5;
    uint32_t var_6;
    uint64_t var_7;
    uint64_t var_8;
    uint64_t var_9;
    uint64_t local_sp_1_ph;
    uint64_t r13_0_ph;
    uint64_t local_sp_1;
    uint64_t var_13;
    uint64_t var_14;
    uint64_t var_15;
    uint64_t local_sp_0;
    uint64_t var_16;
    uint64_t var_17;
    uint32_t var_18;
    uint64_t var_19;
    struct indirect_placeholder_9_ret_type var_20;
    uint64_t var_21;
    uint64_t var_22;
    uint64_t var_23;
    uint64_t var_10;
    uint32_t var_12;
    unsigned int loop_state_var;
    bool switch_state_var;
    var_0 = revng_init_local_sp(0UL);
    var_1 = init_rbx();
    var_2 = init_r14();
    var_3 = init_rbp();
    var_4 = init_r13();
    var_5 = init_r12();
    *(uint64_t *)(var_0 + (-8L)) = var_2;
    *(uint64_t *)(var_0 + (-16L)) = var_4;
    *(uint64_t *)(var_0 + (-24L)) = var_5;
    *(uint64_t *)(var_0 + (-32L)) = var_3;
    *(uint64_t *)(var_0 + (-40L)) = var_1;
    var_6 = (uint32_t)rdi;
    var_7 = (uint64_t)var_6;
    var_8 = *(uint64_t *)rsi;
    *(uint64_t *)(var_0 + (-48L)) = 4201980UL;
    indirect_placeholder_6(var_8);
    *(uint64_t *)(var_0 + (-56L)) = 4201995UL;
    indirect_placeholder_1();
    var_9 = var_0 + (-64L);
    *(uint64_t *)var_9 = 4202005UL;
    indirect_placeholder_1();
    local_sp_1_ph = var_9;
    r13_0_ph = 2UL;
    while (1U)
        {
            r13_0_ph = 0UL;
            local_sp_1 = local_sp_1_ph;
            while (1U)
                {
                    var_10 = local_sp_1 + (-8L);
                    *(uint64_t *)var_10 = 4202058UL;
                    var_11 = indirect_placeholder_2(4252935UL, var_7, 4248512UL, rsi, 0UL);
                    var_12 = (uint32_t)var_11;
                    local_sp_1_ph = var_10;
                    local_sp_0 = var_10;
                    if ((uint64_t)(var_12 + 1U) != 0UL) {
                        var_18 = *(uint32_t *)6358108UL;
                        if ((uint64_t)(var_6 - var_18) == 0UL) {
                            *(uint64_t *)(local_sp_1 + (-16L)) = 4202294UL;
                            indirect_placeholder(r13_0_ph);
                            *(uint64_t *)(local_sp_1 + (-24L)) = 4202327UL;
                            indirect_placeholder_1();
                            return;
                        }
                        var_19 = *(uint64_t *)((uint64_t)((long)((uint64_t)var_18 << 32UL) >> (long)29UL) + rsi);
                        *(uint64_t *)(local_sp_1 + (-16L)) = 4202248UL;
                        var_20 = indirect_placeholder_9(var_19);
                        var_21 = var_20.field_0;
                        var_22 = var_20.field_1;
                        var_23 = var_20.field_2;
                        *(uint64_t *)(local_sp_1 + (-24L)) = 4202276UL;
                        indirect_placeholder_8(0UL, 4247814UL, 0UL, var_21, 0UL, var_22, var_23);
                        *(uint64_t *)(local_sp_1 + (-32L)) = 4202286UL;
                        indirect_placeholder_7(rsi, var_7, 1UL);
                        abort();
                    }
                    if ((uint64_t)(var_12 + 130U) == 0UL) {
                        *(uint64_t *)(local_sp_1 + (-16L)) = 4202116UL;
                        indirect_placeholder_7(rsi, var_7, 0UL);
                        abort();
                    }
                    if ((int)var_12 > (int)4294967166U) {
                        if ((uint64_t)(var_12 + (-128)) != 0UL) {
                            loop_state_var = 0U;
                            break;
                        }
                        if ((uint64_t)(var_12 + (-129)) != 0UL) {
                            loop_state_var = 1U;
                            break;
                        }
                    }
                    if ((uint64_t)(var_12 + 131U) != 0UL) {
                        loop_state_var = 1U;
                        break;
                    }
                    var_13 = *(uint64_t *)6357984UL;
                    var_14 = *(uint64_t *)6358144UL;
                    *(uint64_t *)(local_sp_1 + (-16L)) = 4202162UL;
                    indirect_placeholder_8(0UL, 4247740UL, var_14, var_13, 4247616UL, 0UL, 4247781UL);
                    var_15 = local_sp_1 + (-24L);
                    *(uint64_t *)var_15 = 4202172UL;
                    indirect_placeholder_1();
                    local_sp_0 = var_15;
                    var_16 = *(uint64_t *)6358744UL;
                    var_17 = local_sp_0 + (-8L);
                    *(uint64_t *)var_17 = 4202209UL;
                    indirect_placeholder_3(18446744073709551615UL, var_16, 4252935UL, 0UL, 0UL, 4247799UL);
                    local_sp_1 = var_17;
                    continue;
                }
            switch_state_var = 0;
            switch (loop_state_var) {
              case 1U:
                {
                    switch_state_var = 1;
                    break;
                }
                break;
              case 0U:
                {
                    continue;
                }
                break;
            }
            if (switch_state_var)
                break;
        }
    *(uint64_t *)(local_sp_1 + (-16L)) = 4202227UL;
    indirect_placeholder_7(rsi, var_7, 1UL);
    abort();
}

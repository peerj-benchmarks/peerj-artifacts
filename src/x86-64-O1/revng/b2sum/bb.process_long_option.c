typedef unsigned char __u_char;
typedef unsigned short __u_short;
typedef unsigned int __u_int;
typedef unsigned long __u_long;
typedef signed char __int8_t;
typedef unsigned char __uint8_t;
typedef short __int16_t;
typedef unsigned short __uint16_t;
typedef int __int32_t;
typedef unsigned int __uint32_t;
typedef long __int64_t;
typedef unsigned long __uint64_t;
typedef __int8_t __int_least8_t;
typedef __uint8_t __uint_least8_t;
typedef __int16_t __int_least16_t;
typedef __uint16_t __uint_least16_t;
typedef __int32_t __int_least32_t;
typedef __uint32_t __uint_least32_t;
typedef __int64_t __int_least64_t;
typedef __uint64_t __uint_least64_t;
typedef long __quad_t;
typedef unsigned long __u_quad_t;
typedef long __intmax_t;
typedef unsigned long __uintmax_t;
typedef unsigned long __dev_t;
typedef unsigned int __uid_t;
typedef unsigned int __gid_t;
typedef unsigned long __ino_t;
typedef unsigned long __ino64_t;
typedef unsigned int __mode_t;
typedef unsigned long __nlink_t;
typedef long __off_t;
typedef long __off64_t;
typedef int __pid_t;
typedef struct {
    int __val[2];
} __fsid_t;
typedef long __clock_t;
typedef unsigned long __rlim_t;
typedef unsigned long __rlim64_t;
typedef unsigned int __id_t;
typedef long __time_t;
typedef unsigned int __useconds_t;
typedef long __suseconds_t;
typedef int __daddr_t;
typedef int __key_t;
typedef int __clockid_t;
typedef void *__timer_t;
typedef long __blksize_t;
typedef long __blkcnt_t;
typedef long __blkcnt64_t;
typedef unsigned long __fsblkcnt_t;
typedef unsigned long __fsblkcnt64_t;
typedef unsigned long __fsfilcnt_t;
typedef unsigned long __fsfilcnt64_t;
typedef long __fsword_t;
typedef long __ssize_t;
typedef long __syscall_slong_t;
typedef unsigned long __syscall_ulong_t;
typedef __off64_t __loff_t;
typedef char *__caddr_t;
typedef long __intptr_t;
typedef unsigned int __socklen_t;
typedef int __sig_atomic_t;
typedef __int8_t int8_t;
typedef __int16_t int16_t;
typedef __int32_t int32_t;
typedef __int64_t int64_t;
typedef __uint8_t uint8_t;
typedef __uint16_t uint16_t;
typedef __uint32_t uint32_t;
typedef __uint64_t uint64_t;
typedef __int_least8_t int_least8_t;
typedef __int_least16_t int_least16_t;
typedef __int_least32_t int_least32_t;
typedef __int_least64_t int_least64_t;
typedef __uint_least8_t uint_least8_t;
typedef __uint_least16_t uint_least16_t;
typedef __uint_least32_t uint_least32_t;
typedef __uint_least64_t uint_least64_t;
typedef signed char int_fast8_t;
typedef long int_fast16_t;
typedef long int_fast32_t;
typedef long int_fast64_t;
typedef unsigned char uint_fast8_t;
typedef unsigned long uint_fast16_t;
typedef unsigned long uint_fast32_t;
typedef unsigned long uint_fast64_t;
typedef long intptr_t;
typedef unsigned long uintptr_t;
typedef __intmax_t intmax_t;
typedef __uintmax_t uintmax_t;
typedef _Bool bool;
extern uint64_t revng_init_local_sp(uint64_t param_0);
extern uint64_t init_rbx(void);
extern void indirect_placeholder_1(void);
extern uint64_t init_rbp(void);
extern uint64_t init_r14(void);
extern uint64_t init_r13(void);
extern uint64_t init_r12(void);
extern uint64_t indirect_placeholder(uint64_t param_0);
extern uint64_t helper_cc_compute_all_wrapper(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint32_t param_3);
extern uint64_t init_r15(void);
uint64_t bb_process_long_option(uint64_t rdx, uint64_t rdi, uint64_t rcx, uint64_t rsi, uint64_t r9, uint64_t r8) {
    uint64_t var_0;
    uint64_t var_1;
    uint64_t var_2;
    uint64_t var_3;
    uint64_t var_4;
    uint64_t var_5;
    uint64_t var_6;
    uint64_t var_7;
    uint64_t *var_8;
    uint64_t var_9;
    uint64_t *var_10;
    uint64_t var_11;
    uint64_t r12_2;
    uint64_t r12_3;
    uint64_t var_48;
    uint64_t rbp_5;
    uint64_t local_sp_1;
    uint64_t r12_0;
    uint64_t local_sp_0;
    uint64_t var_16;
    uint64_t var_49;
    uint64_t var_50;
    uint64_t local_sp_2;
    uint64_t var_51;
    uint64_t rax_2;
    uint64_t var_34;
    uint64_t var_35;
    uint64_t *var_36;
    uint64_t _pre;
    uint64_t var_37;
    uint64_t rax_3;
    uint64_t local_sp_12;
    uint64_t rbx_3;
    uint32_t var_24;
    uint64_t var_25;
    uint32_t var_26;
    uint64_t var_27;
    uint64_t rax_0;
    uint64_t r12_4;
    bool var_28;
    uint64_t var_29;
    bool var_30;
    uint64_t rbp_0;
    uint64_t var_31;
    uint64_t var_32;
    uint64_t var_33;
    uint64_t local_sp_3;
    uint64_t r15_0;
    uint64_t var_38;
    uint64_t rax_1;
    uint64_t rbx_0;
    uint64_t rbp_1;
    uint64_t local_sp_4;
    uint64_t var_39;
    uint64_t var_40;
    uint64_t var_41;
    uint64_t var_42;
    bool var_43;
    uint64_t var_54;
    uint64_t local_sp_13;
    uint64_t rbx_1;
    uint64_t var_44;
    uint64_t *var_45;
    uint64_t var_46;
    uint64_t var_47;
    uint64_t local_sp_5;
    uint64_t local_sp_6;
    uint64_t var_52;
    uint32_t *var_53;
    uint64_t rax_4;
    uint64_t rbp_3;
    uint64_t *_pre_phi;
    uint64_t local_sp_10;
    uint64_t rbp_2;
    uint64_t r12_1;
    uint64_t local_sp_7;
    uint32_t *var_55;
    uint32_t var_56;
    uint64_t var_57;
    bool var_58;
    uint32_t var_59;
    uint64_t var_60;
    uint64_t var_61;
    uint64_t local_sp_8;
    uint64_t var_62;
    uint64_t var_63;
    uint64_t var_64;
    uint64_t var_65;
    bool var_66;
    uint32_t var_67;
    uint64_t var_68;
    uint64_t var_17;
    uint64_t *var_18;
    uint64_t rbx_2;
    uint64_t local_sp_9;
    uint64_t var_19;
    uint64_t var_20;
    uint64_t var_12;
    uint64_t var_13;
    bool var_14;
    bool var_15;
    uint64_t var_21;
    uint64_t rbp_4;
    uint64_t local_sp_11;
    uint64_t var_22;
    uint64_t var_23;
    uint64_t local_sp_14;
    uint64_t var_69;
    uint64_t var_70;
    uint32_t *var_71;
    unsigned int loop_state_var;
    var_13 = *var_8;
    r12_1 = r12_3;
    rbx_2 = var_13;
    if (*(uint64_t *)var_13 != 0UL) {
        var_14 = ((uint64_t)(uint32_t)var_13 == 0UL);
        var_15 = (var_13 == (r12_3 - var_11));
        while (1U)
            {
                var_16 = local_sp_10 + (-8L);
                *(uint64_t *)var_16 = 4224500UL;
                indirect_placeholder_1();
                rbx_1 = rbx_2;
                rbp_2 = rbp_3;
                local_sp_9 = var_16;
                rbp_4 = rbp_3;
                if (var_14) {
                    var_19 = rbx_2 + 32UL;
                    var_20 = (uint64_t)((uint32_t)rbp_3 + 1U);
                    rbp_3 = var_20;
                    local_sp_10 = local_sp_9;
                    rbx_2 = var_19;
                    rbp_4 = var_20;
                    local_sp_11 = local_sp_9;
                    if (*(uint64_t *)var_19 == 0UL) {
                        continue;
                    }
                    _pre_phi = (uint64_t *)local_sp_9;
                    loop_state_var = 0U;
                    break;
                }
                var_17 = local_sp_10 + (-16L);
                var_18 = (uint64_t *)var_17;
                *var_18 = 4224512UL;
                indirect_placeholder_1();
                local_sp_7 = var_17;
                local_sp_9 = var_17;
                _pre_phi = var_18;
                local_sp_11 = var_17;
                if (!var_15) {
                    if (rbx_2 != 0UL) {
                        loop_state_var = 1U;
                        break;
                    }
                    loop_state_var = 0U;
                    break;
                }
            }
        switch (loop_state_var) {
          case 0U:
            {
                break;
            }
            break;
          case 1U:
            {
                var_55 = (uint32_t *)var_9;
                var_56 = *var_55;
                var_57 = (uint64_t)var_56 + 1UL;
                *var_55 = (uint32_t)var_57;
                *var_10 = 0UL;
                var_58 = (*(unsigned char *)r12_1 == '\x00');
                var_59 = *(uint32_t *)(rbx_1 + 8UL);
                local_sp_8 = local_sp_7;
                if (var_58) {
                    if (var_59 != 1U) {
                        var_60 = (uint64_t)*(uint32_t *)(local_sp_7 + 48UL);
                        var_61 = var_57 << 32UL;
                        if ((long)var_61 >= (long)(var_60 << 32UL)) {
                            if (*(uint32_t *)(local_sp_7 + 152UL) == 0U) {
                                var_62 = local_sp_7 + (-8L);
                                *(uint64_t *)var_62 = 4225442UL;
                                indirect_placeholder_1();
                                local_sp_8 = var_62;
                            }
                            *(uint32_t *)(var_9 + 8UL) = *(uint32_t *)(rbx_1 + 24UL);
                            var_63 = (**(unsigned char **)(local_sp_8 + 32UL) == ':') ? 58UL : 63UL;
                            rax_4 = var_63;
                            return rax_4;
                        }
                        *var_55 = (var_56 + 2U);
                        *(uint64_t *)(var_9 + 16UL) = *(uint64_t *)((uint64_t)((long)var_61 >> (long)29UL) + *(uint64_t *)(local_sp_7 + 24UL));
                    }
                } else {
                    if (var_59 != 0U) {
                        if (*(uint32_t *)(local_sp_7 + 152UL) == 0U) {
                            *(uint64_t *)(local_sp_7 + (-8L)) = 4225338UL;
                            indirect_placeholder_1();
                        }
                        *(uint32_t *)(var_9 + 8UL) = *(uint32_t *)(rbx_1 + 24UL);
                        return rax_4;
                    }
                    *(uint64_t *)(var_9 + 16UL) = (r12_1 + 1UL);
                }
                var_64 = *(uint64_t *)(local_sp_7 + 16UL);
                rax_4 = 0UL;
                if (var_64 == 0UL) {
                    *(uint32_t *)var_64 = (uint32_t)rbp_2;
                }
                var_65 = *(uint64_t *)(rbx_1 + 16UL);
                var_66 = (var_65 == 0UL);
                var_67 = *(uint32_t *)(rbx_1 + 24UL);
                if (var_66) {
                    var_68 = (uint64_t)var_67;
                    rax_4 = var_68;
                } else {
                    *(uint32_t *)var_65 = var_67;
                }
                return rax_4;
            }
            break;
        }
    }
    var_21 = *_pre_phi;
    r15_0 = var_21;
    local_sp_12 = local_sp_11;
    local_sp_13 = local_sp_11;
    rax_4 = 4294967295UL;
    if (*(uint64_t *)var_21 != 0UL) {
        *(uint32_t *)(local_sp_11 + 52UL) = 4294967295U;
        *(uint32_t *)(local_sp_11 + 68UL) = 0U;
        *(uint64_t *)(local_sp_11 + 40UL) = 0UL;
        var_22 = (uint64_t)((long)(rbp_4 << 32UL) >> (long)32UL);
        *(uint64_t *)(local_sp_11 + 72UL) = var_22;
        *(uint64_t *)(local_sp_11 + 56UL) = r12_3;
        *(uint32_t *)(local_sp_11 + 64UL) = (uint32_t)rbp_4;
        rax_3 = var_22;
        while (1U)
            {
                var_23 = local_sp_12 + (-8L);
                *(uint64_t *)var_23 = 4224636UL;
                indirect_placeholder_1();
                rax_0 = rax_3;
                rbp_0 = rbp_5;
                local_sp_3 = var_23;
                rax_1 = rax_3;
                rbx_0 = rbx_3;
                rbp_1 = rbp_5;
                local_sp_4 = var_23;
                if ((uint64_t)(uint32_t)rax_3 == 0UL) {
                    var_39 = r15_0 + 32UL;
                    rbp_5 = rbp_1;
                    rax_3 = rax_1;
                    local_sp_12 = local_sp_4;
                    rbx_3 = rbx_0;
                    r15_0 = var_39;
                    local_sp_13 = local_sp_4;
                    rbx_1 = rbx_0;
                    local_sp_5 = local_sp_4;
                    local_sp_7 = local_sp_4;
                    if (*(uint64_t *)var_39 == 0UL) {
                        break;
                    }
                    r12_4 = (uint64_t)((uint32_t)r12_4 + 1U);
                    continue;
                }
                rbx_0 = r15_0;
                if (rbx_3 == 0UL) {
                    *(uint32_t *)(local_sp_12 + 44UL) = (uint32_t)r12_4;
                } else {
                    if (*(uint32_t *)(local_sp_12 + 4UL) != 0U) {
                        var_24 = *(uint32_t *)(r15_0 + 8UL);
                        rax_0 = (uint64_t)var_24;
                        var_25 = *(uint64_t *)(r15_0 + 16UL);
                        rax_0 = var_25;
                        var_26 = *(uint32_t *)(r15_0 + 24UL);
                        var_27 = (uint64_t)var_26;
                        rax_0 = var_27;
                        rax_1 = var_27;
                        if ((uint64_t)(*(uint32_t *)(rbx_3 + 8UL) - var_24) != 0UL & *(uint64_t *)(rbx_3 + 16UL) != var_25 & (uint64_t)(*(uint32_t *)(rbx_3 + 24UL) - var_26) != 0UL) {
                            var_39 = r15_0 + 32UL;
                            rbp_5 = rbp_1;
                            rax_3 = rax_1;
                            local_sp_12 = local_sp_4;
                            rbx_3 = rbx_0;
                            r15_0 = var_39;
                            local_sp_13 = local_sp_4;
                            rbx_1 = rbx_0;
                            local_sp_5 = local_sp_4;
                            local_sp_7 = local_sp_4;
                            if (*(uint64_t *)var_39 == 0UL) {
                                break;
                            }
                            r12_4 = (uint64_t)((uint32_t)r12_4 + 1U);
                            continue;
                        }
                    }
                    rax_1 = rax_0;
                    if ((uint64_t)(uint32_t)rbp_5 != 0UL) {
                        var_28 = (*(uint32_t *)(local_sp_12 + 144UL) == 0U);
                        var_29 = *(uint64_t *)(local_sp_12 + 32UL);
                        var_30 = (var_29 == 0UL);
                        var_37 = var_29;
                        rax_1 = 0UL;
                        rbp_1 = 1UL;
                        if (var_28) {
                            rbp_0 = 1UL;
                            rax_1 = rax_0;
                            if (var_30) {
                                var_38 = (uint64_t)((long)(r12_4 << 32UL) >> (long)32UL);
                                *(unsigned char *)(var_38 + var_37) = (unsigned char)'\x01';
                                rax_1 = var_38;
                                rbp_1 = rbp_0;
                                local_sp_4 = local_sp_3;
                            }
                        } else {
                            if (var_30) {
                                var_38 = (uint64_t)((long)(r12_4 << 32UL) >> (long)32UL);
                                *(unsigned char *)(var_38 + var_37) = (unsigned char)'\x01';
                                rax_1 = var_38;
                                rbp_1 = rbp_0;
                                local_sp_4 = local_sp_3;
                            } else {
                                var_31 = *(uint64_t *)(local_sp_12 + 64UL);
                                var_32 = local_sp_12 + (-16L);
                                *(uint64_t *)var_32 = 4224716UL;
                                var_33 = indirect_placeholder(var_31);
                                *(uint64_t *)(local_sp_12 + 24UL) = var_33;
                                local_sp_4 = var_32;
                                if (var_33 != 0UL) {
                                    var_34 = local_sp_12 + (-24L);
                                    *(uint64_t *)var_34 = 4225537UL;
                                    indirect_placeholder_1();
                                    var_35 = (uint64_t)*(uint32_t *)(local_sp_12 + 28UL);
                                    var_36 = (uint64_t *)(local_sp_12 + 16UL);
                                    *(unsigned char *)(*var_36 + var_35) = (unsigned char)'\x01';
                                    *(uint32_t *)(local_sp_12 + 44UL) = 1U;
                                    _pre = *var_36;
                                    var_37 = _pre;
                                    local_sp_3 = var_34;
                                    var_38 = (uint64_t)((long)(r12_4 << 32UL) >> (long)32UL);
                                    *(unsigned char *)(var_38 + var_37) = (unsigned char)'\x01';
                                    rax_1 = var_38;
                                    rbp_1 = rbp_0;
                                    local_sp_4 = local_sp_3;
                                }
                            }
                        }
                    }
                }
            }
        var_40 = (uint64_t)(uint32_t)rbp_1;
        var_41 = *(uint64_t *)(local_sp_4 + 56UL);
        var_42 = (uint64_t)*(uint32_t *)(local_sp_4 + 64UL);
        var_43 = ((uint64_t)(unsigned char)rbp_1 == 0UL);
        rax_2 = var_40;
        r12_1 = var_41;
        if (var_43) {
            if (*(uint32_t *)(local_sp_4 + 152UL) != 0U) {
                var_44 = local_sp_4 + (-8L);
                var_45 = (uint64_t *)var_44;
                rax_2 = 0UL;
                local_sp_5 = var_44;
                if (var_43) {
                    *var_45 = 4224881UL;
                    indirect_placeholder_1();
                } else {
                    *var_45 = 4224901UL;
                    indirect_placeholder_1();
                    var_46 = local_sp_4 + (-16L);
                    *(uint64_t *)var_46 = 4224939UL;
                    indirect_placeholder_1();
                    var_47 = helper_cc_compute_all_wrapper(var_42, 0UL, 0UL, 24U);
                    local_sp_0 = var_46;
                    local_sp_2 = var_46;
                    if ((uint64_t)(((unsigned char)(var_47 >> 4UL) ^ (unsigned char)var_47) & '\xc0') != 0UL) {
                        var_48 = var_42 << 32UL;
                        var_50 = r12_0 + 1UL;
                        r12_0 = var_50;
                        local_sp_0 = local_sp_1;
                        local_sp_2 = local_sp_1;
                        do {
                            local_sp_1 = local_sp_0;
                            if (*(unsigned char *)(r12_0 + *(uint64_t *)(local_sp_4 + 24UL)) == '\x00') {
                                var_49 = local_sp_0 + (-8L);
                                *(uint64_t *)var_49 = 4224994UL;
                                indirect_placeholder_1();
                                local_sp_1 = var_49;
                            }
                            var_50 = r12_0 + 1UL;
                            r12_0 = var_50;
                            local_sp_0 = local_sp_1;
                            local_sp_2 = local_sp_1;
                        } while ((long)var_48 <= (long)(var_50 << 32UL));
                    }
                    *(uint64_t *)(local_sp_2 + (-8L)) = 4225025UL;
                    indirect_placeholder_1();
                    var_51 = local_sp_2 + (-16L);
                    *(uint64_t *)var_51 = 4225033UL;
                    indirect_placeholder_1();
                    local_sp_5 = var_51;
                }
            }
            local_sp_6 = local_sp_5;
            if (*(uint32_t *)(local_sp_5 + 68UL) == 0U) {
                var_52 = local_sp_5 + (-8L);
                *(uint64_t *)var_52 = 4225050UL;
                indirect_placeholder_1();
                local_sp_6 = var_52;
            }
            *(uint64_t *)(local_sp_6 + (-8L)) = 4225059UL;
            indirect_placeholder_1();
            *var_10 = (*var_10 + rax_2);
            var_53 = (uint32_t *)var_9;
            *var_53 = (*var_53 + 1U);
            *(uint32_t *)(var_9 + 8UL) = 0U;
            return rax_4;
        }
        if (*(uint64_t *)(local_sp_4 + 40UL) != 0UL) {
            var_54 = (uint64_t)*(uint32_t *)(local_sp_4 + 52UL);
            rbp_2 = var_54;
            if (rbx_0 != 0UL) {
                local_sp_14 = local_sp_13;
                if (*(uint32_t *)(local_sp_13 + 12UL) != 0U) {
                    var_69 = *var_10;
                    var_70 = local_sp_13 + (-8L);
                    *(uint64_t *)var_70 = 4225142UL;
                    indirect_placeholder_1();
                    local_sp_14 = var_70;
                    if (*(unsigned char *)(*(uint64_t *)(((uint64_t)*(uint32_t *)var_9 << 3UL) + *(uint64_t *)(local_sp_13 + 24UL)) + 1UL) != '-' & var_69 == 0UL) {
                        return rax_4;
                    }
                }
                if (*(uint32_t *)(local_sp_14 + 152UL) == 0U) {
                    *(uint64_t *)(local_sp_14 + (-8L)) = 4225211UL;
                    indirect_placeholder_1();
                }
                *var_10 = 0UL;
                var_71 = (uint32_t *)var_9;
                *var_71 = (*var_71 + 1U);
                *(uint32_t *)(var_9 + 8UL) = 0U;
                return rax_4;
            }
            var_55 = (uint32_t *)var_9;
            var_56 = *var_55;
            var_57 = (uint64_t)var_56 + 1UL;
            *var_55 = (uint32_t)var_57;
            *var_10 = 0UL;
            var_58 = (*(unsigned char *)r12_1 == '\x00');
            var_59 = *(uint32_t *)(rbx_1 + 8UL);
            local_sp_8 = local_sp_7;
            if (var_58) {
                if (var_59 != 1U) {
                    var_60 = (uint64_t)*(uint32_t *)(local_sp_7 + 48UL);
                    var_61 = var_57 << 32UL;
                    if ((long)var_61 >= (long)(var_60 << 32UL)) {
                        if (*(uint32_t *)(local_sp_7 + 152UL) == 0U) {
                            var_62 = local_sp_7 + (-8L);
                            *(uint64_t *)var_62 = 4225442UL;
                            indirect_placeholder_1();
                            local_sp_8 = var_62;
                        }
                        *(uint32_t *)(var_9 + 8UL) = *(uint32_t *)(rbx_1 + 24UL);
                        var_63 = (**(unsigned char **)(local_sp_8 + 32UL) == ':') ? 58UL : 63UL;
                        rax_4 = var_63;
                        return rax_4;
                    }
                    *var_55 = (var_56 + 2U);
                    *(uint64_t *)(var_9 + 16UL) = *(uint64_t *)((uint64_t)((long)var_61 >> (long)29UL) + *(uint64_t *)(local_sp_7 + 24UL));
                }
            } else {
                if (var_59 != 0U) {
                    if (*(uint32_t *)(local_sp_7 + 152UL) == 0U) {
                        *(uint64_t *)(local_sp_7 + (-8L)) = 4225338UL;
                        indirect_placeholder_1();
                    }
                    *(uint32_t *)(var_9 + 8UL) = *(uint32_t *)(rbx_1 + 24UL);
                    return rax_4;
                }
                *(uint64_t *)(var_9 + 16UL) = (r12_1 + 1UL);
            }
            var_64 = *(uint64_t *)(local_sp_7 + 16UL);
            rax_4 = 0UL;
            if (var_64 == 0UL) {
                *(uint32_t *)var_64 = (uint32_t)rbp_2;
            }
            var_65 = *(uint64_t *)(rbx_1 + 16UL);
            var_66 = (var_65 == 0UL);
            var_67 = *(uint32_t *)(rbx_1 + 24UL);
            if (var_66) {
                var_68 = (uint64_t)var_67;
                rax_4 = var_68;
            } else {
                *(uint32_t *)var_65 = var_67;
            }
            return rax_4;
        }
    }
    local_sp_14 = local_sp_13;
    if (*(uint32_t *)(local_sp_13 + 12UL) != 0U) {
        var_69 = *var_10;
        var_70 = local_sp_13 + (-8L);
        *(uint64_t *)var_70 = 4225142UL;
        indirect_placeholder_1();
        local_sp_14 = var_70;
        if (*(unsigned char *)(*(uint64_t *)(((uint64_t)*(uint32_t *)var_9 << 3UL) + *(uint64_t *)(local_sp_13 + 24UL)) + 1UL) != '-' & var_69 == 0UL) {
            return rax_4;
        }
    }
    if (*(uint32_t *)(local_sp_14 + 152UL) != 0U) {
        *(uint64_t *)(local_sp_14 + (-8L)) = 4225211UL;
        indirect_placeholder_1();
    }
    *var_10 = 0UL;
    var_71 = (uint32_t *)var_9;
    *var_71 = (*var_71 + 1U);
    *(uint32_t *)(var_9 + 8UL) = 0U;
}

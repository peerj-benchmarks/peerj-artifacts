typedef unsigned char __u_char;
typedef unsigned short __u_short;
typedef unsigned int __u_int;
typedef unsigned long __u_long;
typedef signed char __int8_t;
typedef unsigned char __uint8_t;
typedef short __int16_t;
typedef unsigned short __uint16_t;
typedef int __int32_t;
typedef unsigned int __uint32_t;
typedef long __int64_t;
typedef unsigned long __uint64_t;
typedef __int8_t __int_least8_t;
typedef __uint8_t __uint_least8_t;
typedef __int16_t __int_least16_t;
typedef __uint16_t __uint_least16_t;
typedef __int32_t __int_least32_t;
typedef __uint32_t __uint_least32_t;
typedef __int64_t __int_least64_t;
typedef __uint64_t __uint_least64_t;
typedef long __quad_t;
typedef unsigned long __u_quad_t;
typedef long __intmax_t;
typedef unsigned long __uintmax_t;
typedef unsigned long __dev_t;
typedef unsigned int __uid_t;
typedef unsigned int __gid_t;
typedef unsigned long __ino_t;
typedef unsigned long __ino64_t;
typedef unsigned int __mode_t;
typedef unsigned long __nlink_t;
typedef long __off_t;
typedef long __off64_t;
typedef int __pid_t;
typedef struct {
    int __val[2];
} __fsid_t;
typedef long __clock_t;
typedef unsigned long __rlim_t;
typedef unsigned long __rlim64_t;
typedef unsigned int __id_t;
typedef long __time_t;
typedef unsigned int __useconds_t;
typedef long __suseconds_t;
typedef int __daddr_t;
typedef int __key_t;
typedef int __clockid_t;
typedef void *__timer_t;
typedef long __blksize_t;
typedef long __blkcnt_t;
typedef long __blkcnt64_t;
typedef unsigned long __fsblkcnt_t;
typedef unsigned long __fsblkcnt64_t;
typedef unsigned long __fsfilcnt_t;
typedef unsigned long __fsfilcnt64_t;
typedef long __fsword_t;
typedef long __ssize_t;
typedef long __syscall_slong_t;
typedef unsigned long __syscall_ulong_t;
typedef __off64_t __loff_t;
typedef char *__caddr_t;
typedef long __intptr_t;
typedef unsigned int __socklen_t;
typedef int __sig_atomic_t;
typedef __int8_t int8_t;
typedef __int16_t int16_t;
typedef __int32_t int32_t;
typedef __int64_t int64_t;
typedef __uint8_t uint8_t;
typedef __uint16_t uint16_t;
typedef __uint32_t uint32_t;
typedef __uint64_t uint64_t;
typedef __int_least8_t int_least8_t;
typedef __int_least16_t int_least16_t;
typedef __int_least32_t int_least32_t;
typedef __int_least64_t int_least64_t;
typedef __uint_least8_t uint_least8_t;
typedef __uint_least16_t uint_least16_t;
typedef __uint_least32_t uint_least32_t;
typedef __uint_least64_t uint_least64_t;
typedef signed char int_fast8_t;
typedef long int_fast16_t;
typedef long int_fast32_t;
typedef long int_fast64_t;
typedef unsigned char uint_fast8_t;
typedef unsigned long uint_fast16_t;
typedef unsigned long uint_fast32_t;
typedef unsigned long uint_fast64_t;
typedef long intptr_t;
typedef unsigned long uintptr_t;
typedef __intmax_t intmax_t;
typedef __uintmax_t uintmax_t;
struct indirect_placeholder_126_ret_type;
struct indirect_placeholder_125_ret_type;
struct indirect_placeholder_126_ret_type {
    uint64_t field_0;
    uint64_t field_1;
    uint64_t field_2;
};
struct indirect_placeholder_125_ret_type {
    uint64_t field_0;
    uint64_t field_1;
    uint64_t field_2;
    uint64_t field_3;
};
extern uint64_t revng_init_local_sp(uint64_t param_0);
extern uint64_t init_rbx(void);
extern void indirect_placeholder_1(void);
extern uint64_t init_cc_src2(void);
extern uint64_t helper_cc_compute_c_wrapper(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint32_t param_3);
extern uint64_t init_rax(void);
extern struct indirect_placeholder_126_ret_type indirect_placeholder_126(uint64_t param_0, uint64_t param_1, uint64_t param_2);
extern struct indirect_placeholder_125_ret_type indirect_placeholder_125(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint64_t param_3, uint64_t param_4, uint64_t param_5, uint64_t param_6);
uint64_t bb_fremote(uint64_t rdi, uint64_t rsi) {
    uint64_t var_0;
    uint64_t var_1;
    uint64_t var_2;
    uint64_t var_3;
    uint64_t *var_4;
    struct indirect_placeholder_126_ret_type var_5;
    uint64_t var_6;
    uint64_t var_7;
    uint64_t var_8;
    uint64_t var_9;
    uint64_t rax_0;
    uint64_t var_10;
    uint64_t var_11;
    uint64_t var_12;
    var_0 = revng_init_local_sp(0UL);
    var_1 = init_rax();
    var_2 = init_rbx();
    var_3 = init_cc_src2();
    *(uint64_t *)(var_0 + (-8L)) = var_2;
    var_4 = (uint64_t *)(var_0 + (-144L));
    *var_4 = 4203833UL;
    indirect_placeholder_1();
    rax_0 = 1UL;
    if ((uint64_t)(uint32_t)var_1 != 0UL) {
        *(uint64_t *)(var_0 + (-152L)) = 4203842UL;
        indirect_placeholder_1();
        if (*(uint32_t *)var_1 == 38U) {
            *(uint64_t *)(var_0 + (-160L)) = 4203872UL;
            var_5 = indirect_placeholder_126(1UL, 4UL, rsi);
            var_6 = var_5.field_0;
            var_7 = var_5.field_1;
            var_8 = var_5.field_2;
            *(uint64_t *)(var_0 + (-168L)) = 4203880UL;
            indirect_placeholder_1();
            var_9 = (uint64_t)*(uint32_t *)var_6;
            *(uint64_t *)(var_0 + (-176L)) = 4203905UL;
            indirect_placeholder_125(0UL, 4270520UL, 0UL, var_6, var_9, var_7, var_8);
        }
        return rax_0;
    }
    var_10 = *var_4;
    rax_0 = 0UL;
    if (var_10 == 352400198UL) {
        if (var_10 > 352400198UL) {
            if (var_10 == 1650746742UL) {
                if (var_10 > 1650746742UL) {
                    if (var_10 == 1936880249UL) {
                        if (var_10 > 1936880249UL) {
                            if (var_10 == 3380511080UL) {
                                if (var_10 > 3380511080UL) {
                                    if (var_10 == 4076150800UL) {
                                        if (var_10 > 4076150800UL) {
                                        } else {
                                        }
                                    } else {
                                    }
                                } else {
                                    if (var_10 == 2435016766UL) {
                                        if (var_10 > 2435016766UL) {
                                        } else {
                                        }
                                    } else {
                                    }
                                }
                            } else {
                            }
                        } else {
                            if (var_10 == 1684300152UL) {
                                if (var_10 > 1684300152UL) {
                                    if (var_10 == 1853056627UL) {
                                        if (var_10 > 1853056627UL) {
                                        } else {
                                        }
                                    } else {
                                    }
                                } else {
                                    if (var_10 == 1650812274UL) {
                                        if (var_10 > 1650812274UL) {
                                        } else {
                                            if (var_10 == 1650812272UL) {
                                            }
                                        }
                                    } else {
                                    }
                                }
                            } else {
                            }
                        }
                    } else {
                    }
                } else {
                    if (var_10 == 1161678120UL) {
                        if (var_10 > 1161678120UL) {
                            if (var_10 == 1410924800UL) {
                                if (var_10 > 1410924800UL) {
                                    if (var_10 == 1481003842UL) {
                                        if (var_10 > 1481003842UL) {
                                        } else {
                                        }
                                    } else {
                                    }
                                } else {
                                    if (var_10 == 1397114950UL) {
                                        if (var_10 > 1397114950UL) {
                                        } else {
                                        }
                                    } else {
                                    }
                                }
                            } else {
                            }
                        } else {
                            if (var_10 == 732765674UL) {
                                if (var_10 > 732765674UL) {
                                    if (var_10 == 1111905073UL) {
                                        if (var_10 > 1111905073UL) {
                                        } else {
                                        }
                                    } else {
                                    }
                                } else {
                                    if (var_10 == 464386766UL) {
                                        if (var_10 > 464386766UL) {
                                        } else {
                                            if (var_10 == 427819522UL) {
                                            }
                                        }
                                    } else {
                                    }
                                }
                            } else {
                            }
                        }
                    } else {
                    }
                }
            } else {
            }
        } else {
            if (var_10 == 29366UL) {
                if (var_10 > 29366UL) {
                    if (var_10 == 4278867UL) {
                        if (var_10 > 4278867UL) {
                            if (var_10 > 19920823UL) {
                                if (var_10 == 195894762UL) {
                                    if (var_10 > 195894762UL) {
                                    } else {
                                    }
                                } else {
                                }
                            } else {
                                var_12 = helper_cc_compute_c_wrapper(var_10 + (-19920820L), 19920820UL, var_3, 17U);
                                if ((var_12 == 0UL) || (var_10 == 16914836UL)) {
                                    if (var_10 > 16914836UL) {
                                    } else {
                                        if (var_10 == 12648430UL) {
                                        }
                                    }
                                } else {
                                }
                            }
                        } else {
                            if (var_10 == 44543UL) {
                                if (var_10 > 44543UL) {
                                    if (var_10 == 61791UL) {
                                        if (var_10 > 61791UL) {
                                        } else {
                                        }
                                    } else {
                                    }
                                } else {
                                    if (var_10 > 40866UL) {
                                        if (var_10 == 44533UL) {
                                        }
                                    } else {
                                        var_11 = helper_cc_compute_c_wrapper(var_10 + (-40864L), 40864UL, var_3, 17U);
                                        if ((var_11 == 0UL) || (var_10 == 38496UL)) {
                                        }
                                    }
                                }
                            } else {
                            }
                        }
                    } else {
                    }
                } else {
                    if (var_10 == 13364UL) {
                        if (var_10 > 13364UL) {
                            if (var_10 == 18475UL) {
                                if (var_10 > 18475UL) {
                                    if (var_10 == 19802UL) {
                                        if (var_10 > 19802UL) {
                                        } else {
                                        }
                                    } else {
                                    }
                                } else {
                                    if (var_10 == 16388UL) {
                                        if (var_10 > 16388UL) {
                                        } else {
                                            if (var_10 == 16384UL) {
                                            }
                                        }
                                    } else {
                                    }
                                }
                            } else {
                            }
                        } else {
                            if (var_10 == 4989UL) {
                                if (var_10 > 4989UL) {
                                    if (var_10 == 7377UL) {
                                        if (var_10 > 7377UL) {
                                        } else {
                                        }
                                    } else {
                                    }
                                } else {
                                    if (var_10 == 391UL) {
                                        if (var_10 > 391UL) {
                                        } else {
                                            if (var_10 == 47UL) {
                                            }
                                        }
                                    } else {
                                    }
                                }
                            } else {
                            }
                        }
                    } else {
                    }
                }
            } else {
            }
        }
    } else {
    }
}

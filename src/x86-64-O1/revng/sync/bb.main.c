typedef unsigned char __u_char;
typedef unsigned short __u_short;
typedef unsigned int __u_int;
typedef unsigned long __u_long;
typedef signed char __int8_t;
typedef unsigned char __uint8_t;
typedef short __int16_t;
typedef unsigned short __uint16_t;
typedef int __int32_t;
typedef unsigned int __uint32_t;
typedef long __int64_t;
typedef unsigned long __uint64_t;
typedef __int8_t __int_least8_t;
typedef __uint8_t __uint_least8_t;
typedef __int16_t __int_least16_t;
typedef __uint16_t __uint_least16_t;
typedef __int32_t __int_least32_t;
typedef __uint32_t __uint_least32_t;
typedef __int64_t __int_least64_t;
typedef __uint64_t __uint_least64_t;
typedef long __quad_t;
typedef unsigned long __u_quad_t;
typedef long __intmax_t;
typedef unsigned long __uintmax_t;
typedef unsigned long __dev_t;
typedef unsigned int __uid_t;
typedef unsigned int __gid_t;
typedef unsigned long __ino_t;
typedef unsigned long __ino64_t;
typedef unsigned int __mode_t;
typedef unsigned long __nlink_t;
typedef long __off_t;
typedef long __off64_t;
typedef int __pid_t;
typedef struct {
    int __val[2];
} __fsid_t;
typedef long __clock_t;
typedef unsigned long __rlim_t;
typedef unsigned long __rlim64_t;
typedef unsigned int __id_t;
typedef long __time_t;
typedef unsigned int __useconds_t;
typedef long __suseconds_t;
typedef int __daddr_t;
typedef int __key_t;
typedef int __clockid_t;
typedef void *__timer_t;
typedef long __blksize_t;
typedef long __blkcnt_t;
typedef long __blkcnt64_t;
typedef unsigned long __fsblkcnt_t;
typedef unsigned long __fsblkcnt64_t;
typedef unsigned long __fsfilcnt_t;
typedef unsigned long __fsfilcnt64_t;
typedef long __fsword_t;
typedef long __ssize_t;
typedef long __syscall_slong_t;
typedef unsigned long __syscall_ulong_t;
typedef __off64_t __loff_t;
typedef char *__caddr_t;
typedef long __intptr_t;
typedef unsigned int __socklen_t;
typedef int __sig_atomic_t;
typedef __int8_t int8_t;
typedef __int16_t int16_t;
typedef __int32_t int32_t;
typedef __int64_t int64_t;
typedef __uint8_t uint8_t;
typedef __uint16_t uint16_t;
typedef __uint32_t uint32_t;
typedef __uint64_t uint64_t;
typedef __int_least8_t int_least8_t;
typedef __int_least16_t int_least16_t;
typedef __int_least32_t int_least32_t;
typedef __int_least64_t int_least64_t;
typedef __uint_least8_t uint_least8_t;
typedef __uint_least16_t uint_least16_t;
typedef __uint_least32_t uint_least32_t;
typedef __uint_least64_t uint_least64_t;
typedef signed char int_fast8_t;
typedef long int_fast16_t;
typedef long int_fast32_t;
typedef long int_fast64_t;
typedef unsigned char uint_fast8_t;
typedef unsigned long uint_fast16_t;
typedef unsigned long uint_fast32_t;
typedef unsigned long uint_fast64_t;
typedef long intptr_t;
typedef unsigned long uintptr_t;
typedef __intmax_t intmax_t;
typedef __uintmax_t uintmax_t;
typedef _Bool bool;
struct indirect_placeholder_14_ret_type;
struct indirect_placeholder_11_ret_type;
struct indirect_placeholder_17_ret_type;
struct indirect_placeholder_9_ret_type;
struct indirect_placeholder_8_ret_type;
struct indirect_placeholder_12_ret_type;
struct indirect_placeholder_10_ret_type;
struct indirect_placeholder_13_ret_type;
struct indirect_placeholder_18_ret_type;
struct indirect_placeholder_19_ret_type;
struct indirect_placeholder_22_ret_type;
struct indirect_placeholder_14_ret_type {
    uint64_t field_0;
    uint64_t field_1;
    uint64_t field_2;
};
struct indirect_placeholder_11_ret_type {
    uint64_t field_0;
    uint64_t field_1;
    uint64_t field_2;
    uint64_t field_3;
    uint64_t field_4;
};
struct indirect_placeholder_17_ret_type {
    uint64_t field_0;
    uint64_t field_1;
};
struct indirect_placeholder_9_ret_type {
    uint64_t field_0;
    uint64_t field_1;
    uint64_t field_2;
};
struct indirect_placeholder_8_ret_type {
    uint64_t field_0;
    uint64_t field_1;
    uint64_t field_2;
    uint64_t field_3;
    uint64_t field_4;
};
struct indirect_placeholder_12_ret_type {
    uint64_t field_0;
    uint64_t field_1;
    uint64_t field_2;
};
struct indirect_placeholder_10_ret_type {
    uint64_t field_0;
    uint64_t field_1;
    uint64_t field_2;
    uint64_t field_3;
    uint64_t field_4;
};
struct indirect_placeholder_13_ret_type {
    uint64_t field_0;
    uint64_t field_1;
};
struct indirect_placeholder_18_ret_type {
    uint64_t field_0;
    uint64_t field_1;
    uint64_t field_2;
    uint64_t field_3;
    uint64_t field_4;
};
struct indirect_placeholder_19_ret_type {
    uint64_t field_0;
    uint64_t field_1;
    uint64_t field_2;
    uint64_t field_3;
    uint64_t field_4;
};
struct indirect_placeholder_22_ret_type {
    uint64_t field_0;
    uint64_t field_1;
    uint64_t field_2;
    uint64_t field_3;
};
extern void abort(void);
extern uint64_t revng_init_local_sp(uint64_t param_0);
extern uint64_t init_rbx(void);
extern void indirect_placeholder(void);
extern uint64_t init_rbp(void);
extern uint64_t init_cc_src2(void);
extern uint64_t helper_cc_compute_c_wrapper(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint32_t param_3);
extern uint64_t init_r14(void);
extern uint64_t init_r13(void);
extern uint64_t init_r12(void);
extern uint64_t init_r15(void);
extern void indirect_placeholder_7(uint64_t param_0);
extern struct indirect_placeholder_14_ret_type indirect_placeholder_14(uint64_t param_0, uint64_t param_1);
extern struct indirect_placeholder_11_ret_type indirect_placeholder_11(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint64_t param_3, uint64_t param_4, uint64_t param_5, uint64_t param_6);
extern struct indirect_placeholder_17_ret_type indirect_placeholder_17(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint64_t param_3, uint64_t param_4);
extern void indirect_placeholder_21(uint64_t param_0, uint64_t param_1, uint64_t param_2);
extern struct indirect_placeholder_9_ret_type indirect_placeholder_9(uint64_t param_0, uint64_t param_1);
extern struct indirect_placeholder_8_ret_type indirect_placeholder_8(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint64_t param_3, uint64_t param_4, uint64_t param_5, uint64_t param_6);
extern struct indirect_placeholder_12_ret_type indirect_placeholder_12(uint64_t param_0, uint64_t param_1);
extern struct indirect_placeholder_10_ret_type indirect_placeholder_10(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint64_t param_3, uint64_t param_4, uint64_t param_5, uint64_t param_6);
extern struct indirect_placeholder_13_ret_type indirect_placeholder_13(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint64_t param_3, uint64_t param_4);
extern struct indirect_placeholder_18_ret_type indirect_placeholder_18(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint64_t param_3, uint64_t param_4, uint64_t param_5, uint64_t param_6);
extern struct indirect_placeholder_19_ret_type indirect_placeholder_19(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint64_t param_3, uint64_t param_4, uint64_t param_5, uint64_t param_6);
extern void indirect_placeholder_20(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint64_t param_3, uint64_t param_4, uint64_t param_5, uint64_t param_6);
extern struct indirect_placeholder_22_ret_type indirect_placeholder_22(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint64_t param_3, uint64_t param_4);
void bb_main(uint64_t rdi, uint64_t rsi) {
    uint64_t *var_63;
    struct indirect_placeholder_14_ret_type var_64;
    uint64_t var_65;
    uint64_t var_66;
    uint64_t var_67;
    uint64_t *var_68;
    uint64_t var_69;
    uint64_t var_70;
    uint64_t var_71;
    struct indirect_placeholder_11_ret_type var_72;
    uint64_t var_73;
    uint64_t var_74;
    uint64_t rcx_1;
    uint64_t local_sp_3;
    uint64_t rdx_0;
    uint32_t var_83;
    uint32_t var_35;
    uint64_t var_37;
    uint64_t var_38;
    uint64_t rcx_4;
    uint64_t local_sp_6;
    uint32_t var_40;
    struct indirect_placeholder_22_ret_type var_12;
    struct indirect_placeholder_17_ret_type var_39;
    struct indirect_placeholder_8_ret_type var_82;
    struct indirect_placeholder_13_ret_type var_44;
    uint64_t var_0;
    uint64_t var_1;
    uint64_t var_2;
    uint64_t var_3;
    uint64_t var_4;
    uint64_t var_5;
    uint64_t var_6;
    uint64_t var_7;
    uint64_t var_8;
    uint64_t var_9;
    uint64_t var_10;
    uint64_t r13_0_ph;
    uint64_t local_sp_8;
    uint64_t r12_1_ph;
    uint64_t local_sp_8_ph;
    uint64_t r12_1;
    struct indirect_placeholder_9_ret_type var_76;
    uint64_t var_77;
    uint64_t var_78;
    uint64_t var_79;
    uint64_t var_80;
    uint64_t var_81;
    uint64_t rdx_0_in_in_in;
    uint64_t local_sp_1;
    uint64_t var_75;
    uint64_t var_47;
    uint64_t *var_49;
    struct indirect_placeholder_12_ret_type var_50;
    uint64_t var_51;
    uint64_t var_52;
    uint64_t var_53;
    uint64_t *var_54;
    uint64_t var_55;
    uint64_t var_56;
    uint64_t var_57;
    struct indirect_placeholder_10_ret_type var_58;
    uint64_t var_59;
    uint64_t var_60;
    uint64_t local_sp_0;
    uint64_t rax_1;
    uint64_t rcx_0;
    uint64_t rcx_3;
    uint64_t local_sp_5;
    uint64_t var_61;
    uint64_t var_48;
    uint64_t var_62;
    uint64_t var_41;
    uint64_t var_42;
    uint64_t var_43;
    uint64_t var_45;
    uint64_t var_46;
    uint64_t local_sp_2;
    uint64_t var_18;
    uint64_t var_19;
    uint64_t var_20;
    uint64_t var_21;
    uint64_t var_22;
    uint64_t var_23;
    uint64_t var_24;
    uint64_t rax_2;
    uint64_t var_25;
    struct indirect_placeholder_18_ret_type var_26;
    uint64_t rdx_1;
    uint64_t rcx_2;
    uint64_t local_sp_4;
    uint64_t r9_0;
    uint64_t r8_0;
    uint64_t var_27;
    uint64_t rax_3;
    uint64_t var_28;
    struct indirect_placeholder_19_ret_type var_29;
    uint64_t rdx_2;
    uint64_t var_30;
    uint32_t var_31;
    bool var_32;
    uint64_t var_33;
    bool var_34;
    uint64_t rdx_3;
    uint64_t var_36;
    uint64_t local_sp_7;
    uint64_t var_15;
    uint64_t var_16;
    uint64_t var_17;
    uint64_t var_11;
    uint64_t var_13;
    uint32_t var_14;
    unsigned int loop_state_var;
    bool switch_state_var;
    var_0 = revng_init_local_sp(0UL);
    var_1 = init_rbx();
    var_2 = init_r14();
    var_3 = init_rbp();
    var_4 = init_r13();
    var_5 = init_r12();
    var_6 = init_r15();
    var_7 = init_cc_src2();
    *(uint64_t *)(var_0 + (-8L)) = var_6;
    *(uint64_t *)(var_0 + (-16L)) = var_2;
    *(uint64_t *)(var_0 + (-24L)) = var_4;
    *(uint64_t *)(var_0 + (-32L)) = var_5;
    *(uint64_t *)(var_0 + (-40L)) = var_3;
    *(uint64_t *)(var_0 + (-48L)) = var_1;
    var_8 = (uint64_t)(uint32_t)rdi;
    var_9 = *(uint64_t *)rsi;
    *(uint64_t *)(var_0 + (-80L)) = 4201807UL;
    indirect_placeholder_7(var_9);
    *(uint64_t *)(var_0 + (-88L)) = 4201822UL;
    indirect_placeholder();
    var_10 = var_0 + (-96L);
    *(uint64_t *)var_10 = 4201832UL;
    indirect_placeholder();
    r13_0_ph = 0UL;
    r12_1_ph = 0UL;
    local_sp_8_ph = var_10;
    while (1U)
        {
            r13_0_ph = 1UL;
            r12_1 = r12_1_ph;
            local_sp_8 = local_sp_8_ph;
            while (1U)
                {
                    var_11 = local_sp_8 + (-8L);
                    *(uint64_t *)var_11 = 4202004UL;
                    var_12 = indirect_placeholder_22(4246922UL, var_8, 4247808UL, rsi, 0UL);
                    var_13 = var_12.field_0;
                    var_14 = (uint32_t)var_13;
                    local_sp_8 = var_11;
                    r12_1_ph = r12_1;
                    local_sp_8_ph = var_11;
                    r12_1 = 1UL;
                    local_sp_4 = var_11;
                    local_sp_7 = var_11;
                    if ((uint64_t)(var_14 + 1U) != 0UL) {
                        var_18 = var_12.field_1;
                        var_19 = var_12.field_2;
                        var_20 = var_12.field_3;
                        var_21 = (uint64_t)*(uint32_t *)6355388UL;
                        var_22 = rdi << 32UL;
                        var_23 = (var_13 & (-256L)) | ((long)(uint64_t)((long)var_22 >> (long)32UL) > (long)var_21);
                        var_24 = r12_1 & r13_0_ph;
                        rax_2 = var_23;
                        rdx_1 = (uint64_t)((uint32_t)r12_1 & (-256)) | var_24;
                        rcx_2 = var_18;
                        r9_0 = var_19;
                        r8_0 = var_20;
                        if (var_24 != 0UL) {
                            loop_state_var = 2U;
                            break;
                        }
                        var_25 = local_sp_8 + (-16L);
                        *(uint64_t *)var_25 = 4202055UL;
                        var_26 = indirect_placeholder_18(0UL, 4247640UL, 1UL, var_18, 0UL, var_19, var_20);
                        rax_2 = var_26.field_0;
                        rdx_1 = var_26.field_1;
                        rcx_2 = var_26.field_2;
                        local_sp_4 = var_25;
                        r9_0 = var_26.field_3;
                        r8_0 = var_26.field_4;
                        loop_state_var = 2U;
                        break;
                    }
                    if ((uint64_t)(var_14 + 130U) == 0UL) {
                        *(uint64_t *)(local_sp_8 + (-16L)) = 4201900UL;
                        indirect_placeholder_21(rsi, var_8, 0UL);
                        abort();
                    }
                    if ((int)var_14 > (int)4294967166U) {
                        if ((uint64_t)(var_14 + (-100)) == 0UL) {
                            continue;
                        }
                        loop_state_var = 1U;
                        break;
                    }
                    if ((uint64_t)(var_14 + 131U) != 0UL) {
                        loop_state_var = 0U;
                        break;
                    }
                    *(uint64_t *)(local_sp_8 + (-24L)) = 0UL;
                    var_15 = *(uint64_t *)6355264UL;
                    var_16 = *(uint64_t *)6355392UL;
                    *(uint64_t *)(local_sp_8 + (-32L)) = 4201952UL;
                    indirect_placeholder_20(0UL, 4246850UL, var_16, var_15, 4246720UL, 4246891UL, 4246909UL);
                    var_17 = local_sp_8 + (-40L);
                    *(uint64_t *)var_17 = 4201962UL;
                    indirect_placeholder();
                    local_sp_7 = var_17;
                    loop_state_var = 0U;
                    break;
                }
            switch_state_var = 0;
            switch (loop_state_var) {
              case 1U:
                {
                    if ((uint64_t)(var_14 + (-102)) == 0UL) {
                        continue;
                    }
                    loop_state_var = 0U;
                    switch_state_var = 1;
                    break;
                }
                break;
              case 2U:
                {
                    var_27 = helper_cc_compute_c_wrapper(rax_2 - r12_1, r12_1, var_7, 14U);
                    rax_3 = rax_2;
                    rdx_2 = rdx_1;
                    rcx_3 = rcx_2;
                    local_sp_5 = local_sp_4;
                    if (var_27 != 0UL) {
                        loop_state_var = 1U;
                        switch_state_var = 1;
                        break;
                    }
                    var_28 = local_sp_4 + (-8L);
                    *(uint64_t *)var_28 = 4202085UL;
                    var_29 = indirect_placeholder_19(0UL, 4247688UL, 1UL, rcx_2, 0UL, r9_0, r8_0);
                    rax_3 = var_29.field_0;
                    rdx_2 = var_29.field_1;
                    rcx_3 = var_29.field_2;
                    local_sp_5 = var_28;
                    loop_state_var = 1U;
                    switch_state_var = 1;
                    break;
                }
                break;
              case 0U:
                {
                    loop_state_var = 0U;
                    switch_state_var = 1;
                    break;
                }
                break;
            }
            if (switch_state_var)
                break;
        }
    switch (loop_state_var) {
      case 0U:
        {
            *(uint64_t *)(local_sp_7 + (-8L)) = 4201972UL;
            indirect_placeholder_21(rsi, var_8, 1UL);
            abort();
        }
        break;
      case 1U:
        {
            rdx_3 = rdx_2;
            rcx_4 = rcx_3;
            local_sp_6 = local_sp_5;
            if ((uint64_t)(unsigned char)rax_3 != 0UL) {
                var_30 = (r13_0_ph == 0UL) ? (uint64_t)(unsigned char)r12_1 : 2UL;
                *(unsigned char *)(local_sp_5 + 15UL) = (unsigned char)rdx_2;
                var_31 = (uint32_t)var_30;
                var_32 = ((uint64_t)(var_31 + (-1)) == 0UL);
                var_33 = var_30 + (-1L);
                var_34 = ((uint64_t)(var_31 + (-2)) == 0UL);
                var_35 = *(uint32_t *)6355388UL;
                var_36 = (uint64_t)var_35 << 32UL;
                while ((long)var_22 <= (long)var_36)
                    {
                        var_37 = *(uint64_t *)((uint64_t)((long)var_36 >> (long)29UL) + rsi);
                        *(uint64_t *)(local_sp_6 + (-8L)) = 4202161UL;
                        indirect_placeholder();
                        var_38 = local_sp_6 + (-16L);
                        *(uint64_t *)var_38 = 4202273UL;
                        var_39 = indirect_placeholder_17(0UL, rdx_3, 0UL, rcx_4, 3UL);
                        var_40 = (uint32_t)var_39.field_0;
                        local_sp_2 = var_38;
                        if ((uint64_t)(var_40 + 1U) == 0UL) {
                            var_63 = (uint64_t *)(local_sp_2 + (-8L));
                            *var_63 = 4202322UL;
                            var_64 = indirect_placeholder_14(4UL, var_37);
                            var_65 = var_64.field_0;
                            var_66 = var_64.field_1;
                            var_67 = var_64.field_2;
                            *var_63 = var_65;
                            var_68 = (uint64_t *)(local_sp_2 + (-16L));
                            *var_68 = 4202331UL;
                            indirect_placeholder();
                            var_69 = *var_68;
                            var_70 = (uint64_t)*(uint32_t *)var_65;
                            var_71 = local_sp_2 + (-24L);
                            *(uint64_t *)var_71 = 4202357UL;
                            var_72 = indirect_placeholder_11(0UL, 4247728UL, 0UL, var_69, var_70, var_66, var_67);
                            var_73 = var_72.field_0;
                            var_74 = var_72.field_2;
                            *(unsigned char *)var_71 = (unsigned char)'\x00';
                            rax_1 = var_73;
                            rcx_0 = var_74;
                            local_sp_1 = var_71;
                        } else {
                            var_41 = var_39.field_1;
                            var_42 = (uint64_t)(var_40 & (-2049));
                            var_43 = local_sp_6 + (-24L);
                            *(uint64_t *)var_43 = 4202301UL;
                            var_44 = indirect_placeholder_13(0UL, var_42, 0UL, var_41, 4UL);
                            var_45 = var_44.field_0;
                            var_46 = var_44.field_1;
                            rax_1 = var_45;
                            rcx_0 = var_46;
                            local_sp_2 = var_43;
                            if ((int)(uint32_t)var_45 > (int)4294967295U) {
                                var_63 = (uint64_t *)(local_sp_2 + (-8L));
                                *var_63 = 4202322UL;
                                var_64 = indirect_placeholder_14(4UL, var_37);
                                var_65 = var_64.field_0;
                                var_66 = var_64.field_1;
                                var_67 = var_64.field_2;
                                *var_63 = var_65;
                                var_68 = (uint64_t *)(local_sp_2 + (-16L));
                                *var_68 = 4202331UL;
                                indirect_placeholder();
                                var_69 = *var_68;
                                var_70 = (uint64_t)*(uint32_t *)var_65;
                                var_71 = local_sp_2 + (-24L);
                                *(uint64_t *)var_71 = 4202357UL;
                                var_72 = indirect_placeholder_11(0UL, 4247728UL, 0UL, var_69, var_70, var_66, var_67);
                                var_73 = var_72.field_0;
                                var_74 = var_72.field_2;
                                *(unsigned char *)var_71 = (unsigned char)'\x00';
                                rax_1 = var_73;
                                rcx_0 = var_74;
                                local_sp_1 = var_71;
                            } else {
                                if (var_32) {
                                    var_62 = local_sp_6 + (-32L);
                                    *(uint64_t *)var_62 = 4202371UL;
                                    indirect_placeholder();
                                    local_sp_0 = var_62;
                                    *(unsigned char *)local_sp_0 = (unsigned char)'\x01';
                                    local_sp_1 = local_sp_0;
                                } else {
                                    var_47 = helper_cc_compute_c_wrapper(var_33, 1UL, var_7, 16U);
                                    if (var_47 == 0UL) {
                                        if (var_34) {
                                            *(unsigned char *)var_43 = (unsigned char)'\x01';
                                            var_49 = (uint64_t *)(var_43 + (-8L));
                                            *var_49 = 4202419UL;
                                            var_50 = indirect_placeholder_12(4UL, var_37);
                                            var_51 = var_50.field_0;
                                            var_52 = var_50.field_1;
                                            var_53 = var_50.field_2;
                                            *var_49 = var_51;
                                            var_54 = (uint64_t *)(var_43 + (-16L));
                                            *var_54 = 4202428UL;
                                            indirect_placeholder();
                                            var_55 = *var_54;
                                            var_56 = (uint64_t)*(uint32_t *)var_51;
                                            var_57 = var_43 + (-24L);
                                            *(uint64_t *)var_57 = 4202454UL;
                                            var_58 = indirect_placeholder_10(0UL, 4246942UL, 0UL, var_55, var_56, var_52, var_53);
                                            var_59 = var_58.field_0;
                                            var_60 = var_58.field_2;
                                            *(unsigned char *)var_57 = (unsigned char)'\x00';
                                            rax_1 = var_59;
                                            rcx_0 = var_60;
                                            local_sp_1 = var_57;
                                        } else {
                                            var_61 = local_sp_6 + (-32L);
                                            *(uint64_t *)var_61 = 4202391UL;
                                            indirect_placeholder();
                                            local_sp_0 = var_61;
                                            *(unsigned char *)local_sp_0 = (unsigned char)'\x01';
                                            local_sp_1 = local_sp_0;
                                        }
                                    } else {
                                        var_48 = local_sp_6 + (-32L);
                                        *(uint64_t *)var_48 = 4202381UL;
                                        indirect_placeholder();
                                        local_sp_0 = var_48;
                                        *(unsigned char *)local_sp_0 = (unsigned char)'\x01';
                                        local_sp_1 = local_sp_0;
                                    }
                                }
                            }
                        }
                        var_75 = local_sp_1 + (-8L);
                        *(uint64_t *)var_75 = 4202466UL;
                        indirect_placeholder();
                        rdx_0_in_in_in = var_75;
                        rcx_1 = rcx_0;
                        local_sp_3 = var_75;
                        if ((int)(uint32_t)rax_1 > (int)4294967295U) {
                            *(uint64_t *)(local_sp_1 + (-16L)) = 4202487UL;
                            var_76 = indirect_placeholder_9(4UL, var_37);
                            var_77 = var_76.field_0;
                            var_78 = var_76.field_1;
                            var_79 = var_76.field_2;
                            *(uint64_t *)(local_sp_1 + (-24L)) = 4202495UL;
                            indirect_placeholder();
                            var_80 = (uint64_t)*(uint32_t *)var_77;
                            var_81 = local_sp_1 + (-32L);
                            *(uint64_t *)var_81 = 4202520UL;
                            var_82 = indirect_placeholder_8(0UL, 4246959UL, 0UL, var_77, var_80, var_78, var_79);
                            rdx_0_in_in_in = local_sp_1 + (-17L);
                            rcx_1 = var_82.field_2;
                            local_sp_3 = var_81;
                        }
                        rdx_0 = (uint64_t)*(unsigned char *)rdx_0_in_in_in;
                        var_83 = *(uint32_t *)6355388UL + 1U;
                        *(uint32_t *)6355388UL = var_83;
                        var_35 = var_83;
                        rdx_3 = rdx_0;
                        rcx_4 = rcx_1;
                        local_sp_6 = local_sp_3;
                        var_36 = (uint64_t)var_35 << 32UL;
                    }
            }
            *(uint64_t *)(local_sp_5 + (-8L)) = 4202126UL;
            indirect_placeholder();
            return;
        }
        break;
    }
}

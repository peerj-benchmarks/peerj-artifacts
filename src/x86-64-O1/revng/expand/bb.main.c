typedef unsigned char __u_char;
typedef unsigned short __u_short;
typedef unsigned int __u_int;
typedef unsigned long __u_long;
typedef signed char __int8_t;
typedef unsigned char __uint8_t;
typedef short __int16_t;
typedef unsigned short __uint16_t;
typedef int __int32_t;
typedef unsigned int __uint32_t;
typedef long __int64_t;
typedef unsigned long __uint64_t;
typedef __int8_t __int_least8_t;
typedef __uint8_t __uint_least8_t;
typedef __int16_t __int_least16_t;
typedef __uint16_t __uint_least16_t;
typedef __int32_t __int_least32_t;
typedef __uint32_t __uint_least32_t;
typedef __int64_t __int_least64_t;
typedef __uint64_t __uint_least64_t;
typedef long __quad_t;
typedef unsigned long __u_quad_t;
typedef long __intmax_t;
typedef unsigned long __uintmax_t;
typedef unsigned long __dev_t;
typedef unsigned int __uid_t;
typedef unsigned int __gid_t;
typedef unsigned long __ino_t;
typedef unsigned long __ino64_t;
typedef unsigned int __mode_t;
typedef unsigned long __nlink_t;
typedef long __off_t;
typedef long __off64_t;
typedef int __pid_t;
typedef struct {
    int __val[2];
} __fsid_t;
typedef long __clock_t;
typedef unsigned long __rlim_t;
typedef unsigned long __rlim64_t;
typedef unsigned int __id_t;
typedef long __time_t;
typedef unsigned int __useconds_t;
typedef long __suseconds_t;
typedef int __daddr_t;
typedef int __key_t;
typedef int __clockid_t;
typedef void *__timer_t;
typedef long __blksize_t;
typedef long __blkcnt_t;
typedef long __blkcnt64_t;
typedef unsigned long __fsblkcnt_t;
typedef unsigned long __fsblkcnt64_t;
typedef unsigned long __fsfilcnt_t;
typedef unsigned long __fsfilcnt64_t;
typedef long __fsword_t;
typedef long __ssize_t;
typedef long __syscall_slong_t;
typedef unsigned long __syscall_ulong_t;
typedef __off64_t __loff_t;
typedef char *__caddr_t;
typedef long __intptr_t;
typedef unsigned int __socklen_t;
typedef int __sig_atomic_t;
typedef __int8_t int8_t;
typedef __int16_t int16_t;
typedef __int32_t int32_t;
typedef __int64_t int64_t;
typedef __uint8_t uint8_t;
typedef __uint16_t uint16_t;
typedef __uint32_t uint32_t;
typedef __uint64_t uint64_t;
typedef __int_least8_t int_least8_t;
typedef __int_least16_t int_least16_t;
typedef __int_least32_t int_least32_t;
typedef __int_least64_t int_least64_t;
typedef __uint_least8_t uint_least8_t;
typedef __uint_least16_t uint_least16_t;
typedef __uint_least32_t uint_least32_t;
typedef __uint_least64_t uint_least64_t;
typedef signed char int_fast8_t;
typedef long int_fast16_t;
typedef long int_fast32_t;
typedef long int_fast64_t;
typedef unsigned char uint_fast8_t;
typedef unsigned long uint_fast16_t;
typedef unsigned long uint_fast32_t;
typedef unsigned long uint_fast64_t;
typedef long intptr_t;
typedef unsigned long uintptr_t;
typedef __intmax_t intmax_t;
typedef __uintmax_t uintmax_t;
struct indirect_placeholder_24_ret_type;
struct indirect_placeholder_25_ret_type;
struct indirect_placeholder_26_ret_type;
struct indirect_placeholder_27_ret_type;
struct indirect_placeholder_28_ret_type;
struct indirect_placeholder_29_ret_type;
struct indirect_placeholder_30_ret_type;
struct indirect_placeholder_33_ret_type;
struct indirect_placeholder_24_ret_type {
    uint64_t field_0;
    uint64_t field_1;
    uint64_t field_2;
    uint64_t field_3;
    uint64_t field_4;
    uint64_t field_5;
    uint64_t field_6;
};
struct indirect_placeholder_25_ret_type {
    uint64_t field_0;
    uint64_t field_1;
    uint64_t field_2;
    uint64_t field_3;
    uint64_t field_4;
    uint64_t field_5;
    uint64_t field_6;
};
struct indirect_placeholder_26_ret_type {
    uint64_t field_0;
    uint64_t field_1;
    uint64_t field_2;
    uint64_t field_3;
    uint64_t field_4;
    uint64_t field_5;
    uint64_t field_6;
};
struct indirect_placeholder_27_ret_type {
    uint64_t field_0;
    uint64_t field_1;
    uint64_t field_2;
    uint64_t field_3;
    uint64_t field_4;
};
struct indirect_placeholder_28_ret_type {
    uint64_t field_0;
    uint64_t field_1;
    uint64_t field_2;
    uint64_t field_3;
    uint64_t field_4;
};
struct indirect_placeholder_29_ret_type {
    uint64_t field_0;
    uint64_t field_1;
    uint64_t field_2;
    uint64_t field_3;
    uint64_t field_4;
    uint64_t field_5;
    uint64_t field_6;
};
struct indirect_placeholder_30_ret_type {
    uint64_t field_0;
    uint64_t field_1;
    uint64_t field_2;
    uint64_t field_3;
    uint64_t field_4;
};
struct indirect_placeholder_33_ret_type {
    uint64_t field_0;
    uint64_t field_1;
    uint64_t field_2;
    uint64_t field_3;
};
extern void abort(void);
extern uint64_t revng_init_local_sp(uint64_t param_0);
extern uint64_t init_rbx(void);
extern void indirect_placeholder(void);
extern uint64_t init_rbp(void);
extern uint64_t init_r13(void);
extern uint64_t init_r12(void);
extern void indirect_placeholder_23(uint64_t param_0);
extern void indirect_placeholder_31(uint64_t param_0, uint64_t param_1, uint64_t param_2);
extern void indirect_placeholder_3(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint64_t param_3);
extern struct indirect_placeholder_24_ret_type indirect_placeholder_24(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint64_t param_3, uint64_t param_4, uint64_t param_5, uint64_t param_6);
extern struct indirect_placeholder_25_ret_type indirect_placeholder_25(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint64_t param_3, uint64_t param_4, uint64_t param_5, uint64_t param_6);
extern struct indirect_placeholder_26_ret_type indirect_placeholder_26(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint64_t param_3, uint64_t param_4, uint64_t param_5, uint64_t param_6);
extern struct indirect_placeholder_27_ret_type indirect_placeholder_27(uint64_t param_0);
extern struct indirect_placeholder_28_ret_type indirect_placeholder_28(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint64_t param_3, uint64_t param_4, uint64_t param_5, uint64_t param_6);
extern struct indirect_placeholder_29_ret_type indirect_placeholder_29(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint64_t param_3, uint64_t param_4, uint64_t param_5, uint64_t param_6);
extern struct indirect_placeholder_30_ret_type indirect_placeholder_30(uint64_t param_0);
extern void indirect_placeholder_32(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint64_t param_3, uint64_t param_4, uint64_t param_5, uint64_t param_6);
extern struct indirect_placeholder_33_ret_type indirect_placeholder_33(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint64_t param_3, uint64_t param_4);
typedef _Bool bool;
void bb_main(uint64_t rdi, uint64_t rsi) {
    uint64_t var_0;
    uint64_t var_1;
    uint64_t var_2;
    uint64_t var_3;
    uint64_t var_4;
    struct indirect_placeholder_33_ret_type var_9;
    struct indirect_placeholder_25_ret_type var_65;
    uint64_t r13_1;
    uint64_t var_5;
    uint64_t var_6;
    uint64_t var_7;
    uint64_t local_sp_13;
    uint64_t rax_8;
    uint64_t rbx_5;
    uint64_t var_73;
    uint64_t var_74;
    uint64_t r10_1;
    uint64_t r8_8;
    struct indirect_placeholder_24_ret_type var_75;
    uint64_t local_sp_8;
    uint64_t local_sp_0;
    uint64_t rax_0;
    uint64_t rcx_0;
    uint64_t r9_0;
    uint64_t r8_0;
    uint64_t r8_1;
    uint64_t var_70;
    uint64_t local_sp_9;
    uint64_t local_sp_10_ph;
    uint64_t rax_2;
    uint64_t rbx_1;
    uint64_t var_63;
    uint64_t var_64;
    uint64_t r9_2;
    uint64_t r8_2;
    uint64_t var_66;
    uint64_t var_67;
    uint64_t var_68;
    uint64_t var_69;
    uint64_t r13_3;
    uint64_t var_56;
    struct indirect_placeholder_26_ret_type var_57;
    uint64_t var_58;
    uint64_t var_59;
    uint64_t var_60;
    uint64_t var_61;
    uint64_t local_sp_2;
    uint64_t r13_3_ph;
    uint64_t local_sp_1;
    uint64_t rax_1;
    uint64_t rcx_2;
    uint64_t rbx_0;
    uint64_t rcx_1;
    uint64_t r9_1;
    uint64_t local_sp_4;
    uint64_t var_62;
    uint64_t rcx_10_ph;
    uint64_t var_36;
    uint64_t local_sp_10;
    uint64_t rax_10;
    uint64_t var_32;
    uint64_t rcx_10;
    struct indirect_placeholder_27_ret_type var_33;
    uint64_t var_34;
    uint64_t var_35;
    uint64_t local_sp_3;
    uint64_t rax_3;
    uint64_t r13_0;
    uint64_t r10_3;
    uint64_t rcx_3;
    uint64_t r9_10;
    uint64_t r10_0;
    uint64_t r8_10;
    uint64_t r9_3;
    uint64_t r8_3;
    uint32_t var_37;
    uint64_t var_45;
    uint64_t var_46;
    uint64_t var_47;
    struct indirect_placeholder_28_ret_type var_48;
    uint64_t var_49;
    uint64_t var_50;
    uint64_t var_51;
    uint64_t var_52;
    uint64_t var_53;
    uint64_t var_54;
    uint64_t var_55;
    uint64_t rax_9;
    uint64_t r13_2;
    uint64_t rcx_9;
    uint64_t r10_2;
    uint64_t r9_9;
    uint64_t r8_9;
    uint64_t var_41;
    uint64_t *var_42;
    uint64_t var_43;
    uint64_t var_44;
    uint64_t rax_4;
    uint64_t rbx_2;
    uint64_t rbp_0;
    uint64_t rcx_4;
    uint64_t r9_4;
    uint64_t r8_4;
    uint32_t _pre182;
    uint32_t _pre_pre_phi;
    uint64_t var_38;
    uint64_t local_sp_5;
    uint64_t var_39;
    struct indirect_placeholder_29_ret_type var_40;
    uint64_t rax_5;
    uint64_t rcx_5;
    uint64_t r9_5;
    uint64_t r8_5;
    uint32_t _pre_phi;
    uint64_t local_sp_6;
    uint64_t rax_6;
    uint64_t rbx_3;
    uint64_t rbp_1;
    uint64_t rcx_6;
    uint64_t r9_6;
    uint64_t r8_6;
    uint64_t var_71;
    uint64_t local_sp_7;
    uint64_t rax_7;
    uint64_t rbx_4;
    uint64_t rbp_2;
    uint64_t r12_0;
    uint64_t rcx_7;
    uint64_t r9_7;
    uint64_t r8_7;
    uint64_t rbp_3;
    uint64_t r12_1;
    uint64_t rcx_8;
    uint64_t r9_8;
    uint64_t var_72;
    uint64_t rax_10_ph;
    uint64_t rbx_6_ph;
    uint64_t r12_2_ph;
    uint64_t r10_3_ph;
    uint64_t r9_10_ph;
    uint64_t r8_10_ph;
    uint64_t var_30;
    uint32_t var_31;
    uint64_t local_sp_11;
    uint64_t var_25;
    uint64_t var_26;
    uint64_t rdi1_0;
    uint64_t var_27;
    struct indirect_placeholder_30_ret_type var_28;
    uint64_t var_29;
    uint64_t local_sp_12;
    uint64_t var_20;
    uint64_t var_21;
    uint64_t var_22;
    uint64_t var_15;
    uint64_t var_16;
    uint64_t var_17;
    uint64_t var_18;
    uint64_t var_19;
    uint64_t local_sp_13_be;
    uint64_t var_23;
    uint64_t var_24;
    uint64_t var_8;
    uint64_t var_10;
    uint64_t var_11;
    uint64_t var_12;
    uint64_t var_13;
    uint32_t var_14;
    unsigned int loop_state_var;
    bool switch_state_var;
    var_0 = revng_init_local_sp(0UL);
    var_1 = init_rbx();
    var_2 = init_rbp();
    var_3 = init_r13();
    var_4 = init_r12();
    *(uint64_t *)(var_0 + (-8L)) = var_3;
    *(uint64_t *)(var_0 + (-16L)) = var_4;
    *(uint64_t *)(var_0 + (-24L)) = var_2;
    *(uint64_t *)(var_0 + (-32L)) = var_1;
    var_5 = (uint64_t)(uint32_t)rdi;
    var_6 = *(uint64_t *)rsi;
    *(uint64_t *)(var_0 + (-80L)) = 4202221UL;
    indirect_placeholder_23(var_6);
    *(uint64_t *)(var_0 + (-88L)) = 4202236UL;
    indirect_placeholder();
    var_7 = var_0 + (-96L);
    *(uint64_t *)var_7 = 4202246UL;
    indirect_placeholder();
    *(unsigned char *)6359124UL = (unsigned char)'\x01';
    local_sp_13 = var_7;
    rax_3 = 0UL;
    r13_0 = 0UL;
    r12_0 = 1UL;
    rbx_6_ph = 0UL;
    r12_2_ph = 1UL;
    while (1U)
        {
            var_8 = local_sp_13 + (-8L);
            *(uint64_t *)var_8 = 4202474UL;
            var_9 = indirect_placeholder_33(4250208UL, var_5, 4250048UL, rsi, 0UL);
            var_10 = var_9.field_0;
            var_11 = var_9.field_1;
            var_12 = var_9.field_2;
            var_13 = var_9.field_3;
            var_14 = (uint32_t)var_10;
            local_sp_13_be = var_8;
            local_sp_12 = var_8;
            if ((uint64_t)(var_14 + 1U) == 0UL) {
                if ((int)var_14 > (int)57U) {
                    if ((uint64_t)(var_14 + (-105)) == 0UL) {
                        *(unsigned char *)6359124UL = (unsigned char)'\x00';
                    } else {
                        if ((uint64_t)(var_14 + (-116)) != 0UL) {
                            loop_state_var = 0U;
                            break;
                        }
                        var_23 = *(uint64_t *)6359648UL;
                        var_24 = local_sp_13 + (-16L);
                        *(uint64_t *)var_24 = 4202326UL;
                        indirect_placeholder_3(var_23, var_11, var_12, var_13);
                        local_sp_13_be = var_24;
                    }
                } else {
                    if ((int)var_14 >= (int)48U) {
                        if ((uint64_t)(var_14 + 131U) != 0UL) {
                            if ((uint64_t)(var_14 + 130U) != 0UL) {
                                loop_state_var = 0U;
                                break;
                            }
                            *(uint64_t *)(local_sp_13 + (-16L)) = 4202382UL;
                            indirect_placeholder_31(rsi, var_5, 0UL);
                            abort();
                        }
                        var_20 = *(uint64_t *)6358864UL;
                        var_21 = *(uint64_t *)6358976UL;
                        *(uint64_t *)(local_sp_13 + (-16L)) = 4202428UL;
                        indirect_placeholder_32(0UL, 4249190UL, var_21, var_20, 4249088UL, 0UL, 4249231UL);
                        var_22 = local_sp_13 + (-24L);
                        *(uint64_t *)var_22 = 4202438UL;
                        indirect_placeholder();
                        local_sp_12 = var_22;
                        loop_state_var = 0U;
                        break;
                    }
                    var_15 = *(uint64_t *)6359648UL;
                    if (var_15 == 0UL) {
                        var_18 = local_sp_13 + 8UL;
                        *(unsigned char *)var_18 = (unsigned char)var_10;
                        *(unsigned char *)(local_sp_13 + 9UL) = (unsigned char)'\x00';
                        var_19 = local_sp_13 + (-16L);
                        *(uint64_t *)var_19 = 4202370UL;
                        indirect_placeholder_3(var_18, var_11, var_12, var_13);
                        local_sp_13_be = var_19;
                    } else {
                        var_16 = var_15 + (-1L);
                        var_17 = local_sp_13 + (-16L);
                        *(uint64_t *)var_17 = 4202349UL;
                        indirect_placeholder_3(var_16, var_11, var_12, var_13);
                        local_sp_13_be = var_17;
                    }
                }
                local_sp_13 = local_sp_13_be;
                continue;
            }
            *(uint64_t *)(local_sp_13 + (-16L)) = 4202488UL;
            indirect_placeholder_31(var_11, var_12, var_13);
            var_25 = rdi << 32UL;
            var_26 = (uint64_t)*(uint32_t *)6358972UL << 32UL;
            rdi1_0 = ((long)var_25 > (long)var_26) ? ((uint64_t)((long)var_26 >> (long)29UL) + rsi) : 0UL;
            *(uint64_t *)(local_sp_13 + (-24L)) = 4202514UL;
            indirect_placeholder_23(rdi1_0);
            var_27 = local_sp_13 + (-32L);
            *(uint64_t *)var_27 = 4202524UL;
            var_28 = indirect_placeholder_30(0UL);
            var_29 = var_28.field_0;
            local_sp_9 = var_27;
            rax_9 = var_29;
            r13_2 = var_29;
            local_sp_11 = var_27;
            if (var_29 != 0UL) {
                loop_state_var = 1U;
                break;
            }
            rcx_9 = var_28.field_1;
            r10_2 = var_28.field_2;
            r9_9 = var_28.field_3;
            r8_9 = var_28.field_4;
            while (1U)
                {
                    *(uint64_t *)(local_sp_9 + 16UL) = 0UL;
                    local_sp_10_ph = local_sp_9;
                    r13_3_ph = r13_2;
                    rcx_10_ph = rcx_9;
                    rax_10_ph = rax_9;
                    r10_3_ph = r10_2;
                    r9_10_ph = r9_9;
                    r8_10_ph = r8_9;
                    while (1U)
                        {
                            rbx_5 = rbx_6_ph;
                            r13_3 = r13_3_ph;
                            local_sp_10 = local_sp_10_ph;
                            rax_10 = rax_10_ph;
                            rcx_10 = rcx_10_ph;
                            r10_3 = r10_3_ph;
                            r9_10 = r9_10_ph;
                            r8_10 = r8_10_ph;
                            r12_1 = r12_2_ph;
                            while (1U)
                                {
                                    var_30 = local_sp_10 + (-8L);
                                    *(uint64_t *)var_30 = 4202564UL;
                                    indirect_placeholder();
                                    var_31 = (uint32_t)rax_10;
                                    r13_1 = r13_3;
                                    rax_8 = rax_10;
                                    r10_1 = r10_3;
                                    r8_8 = r8_10;
                                    local_sp_8 = var_30;
                                    local_sp_3 = var_30;
                                    rcx_3 = rcx_10;
                                    r10_0 = r10_3;
                                    r9_3 = r9_10;
                                    r8_3 = r8_10;
                                    rcx_8 = rcx_10;
                                    r9_8 = r9_10;
                                    if ((int)var_31 <= (int)4294967295U) {
                                        loop_state_var = 1U;
                                        break;
                                    }
                                    var_32 = local_sp_10 + (-16L);
                                    *(uint64_t *)var_32 = 4202582UL;
                                    var_33 = indirect_placeholder_27(r13_3);
                                    var_34 = var_33.field_0;
                                    r13_3 = var_34;
                                    local_sp_10 = var_32;
                                    rax_10 = var_34;
                                    local_sp_3 = var_32;
                                    local_sp_11 = var_32;
                                    if (var_34 != 0UL) {
                                        loop_state_var = 0U;
                                        break;
                                    }
                                    rcx_10 = var_33.field_1;
                                    r10_3 = var_33.field_2;
                                    r9_10 = var_33.field_3;
                                    r8_10 = var_33.field_4;
                                    continue;
                                }
                            switch_state_var = 0;
                            switch (loop_state_var) {
                              case 0U:
                                {
                                    var_36 = (uint64_t)var_31;
                                    rcx_3 = var_33.field_1;
                                    r10_0 = var_33.field_2;
                                    r9_3 = var_33.field_3;
                                    r8_3 = var_33.field_4;
                                    if ((uint64_t)(unsigned char)r12_2_ph != 0UL) {
                                        loop_state_var = 0U;
                                        switch_state_var = 1;
                                        break;
                                    }
                                }
                                break;
                              case 1U:
                                {
                                    var_35 = (uint64_t)var_31;
                                    var_36 = var_35;
                                    rax_3 = rax_10;
                                    r13_0 = r13_3;
                                    rbp_3 = var_35;
                                    if ((uint64_t)(unsigned char)r12_2_ph != 0UL) {
                                        var_72 = local_sp_8 + (-8L);
                                        *(uint64_t *)var_72 = 4202846UL;
                                        indirect_placeholder();
                                        local_sp_0 = var_72;
                                        rax_0 = rax_8;
                                        rcx_0 = rcx_8;
                                        r9_0 = r9_8;
                                        r8_0 = r8_8;
                                        r13_3_ph = r13_1;
                                        r13_2 = r13_1;
                                        r10_2 = r10_1;
                                        rbx_6_ph = rbx_5;
                                        r12_2_ph = r12_1;
                                        r10_3_ph = r10_1;
                                        if ((int)(uint32_t)rax_8 > (int)4294967295U) {
                                            *(uint64_t *)(local_sp_8 + (-16L)) = 4202855UL;
                                            indirect_placeholder();
                                            var_73 = (uint64_t)*(uint32_t *)rax_8;
                                            var_74 = local_sp_8 + (-24L);
                                            *(uint64_t *)var_74 = 4202877UL;
                                            var_75 = indirect_placeholder_24(0UL, 4249270UL, 1UL, rcx_8, var_73, r9_8, r8_8);
                                            local_sp_0 = var_74;
                                            rax_0 = var_75.field_0;
                                            rcx_0 = var_75.field_3;
                                            r9_0 = var_75.field_5;
                                            r8_0 = var_75.field_6;
                                        }
                                        local_sp_9 = local_sp_0;
                                        local_sp_10_ph = local_sp_0;
                                        rcx_10_ph = rcx_0;
                                        rax_9 = rax_0;
                                        rcx_9 = rcx_0;
                                        r9_9 = r9_0;
                                        r8_9 = r8_0;
                                        rax_10_ph = rax_0;
                                        r9_10_ph = r9_0;
                                        r8_10_ph = r8_0;
                                        if ((uint64_t)((uint32_t)rbp_3 + (-10)) == 0UL) {
                                            continue;
                                        }
                                        loop_state_var = 1U;
                                        switch_state_var = 1;
                                        break;
                                    }
                                    var_37 = (uint32_t)var_36;
                                    r13_1 = r13_0;
                                    r10_1 = r10_0;
                                    local_sp_4 = local_sp_3;
                                    rbp_0 = var_36;
                                    rcx_4 = rcx_3;
                                    r9_4 = r9_3;
                                    r8_4 = r8_3;
                                    _pre_pre_phi = var_37;
                                    local_sp_5 = local_sp_3;
                                    rax_5 = rax_3;
                                    rcx_5 = rcx_3;
                                    r9_5 = r9_3;
                                    r8_5 = r8_3;
                                    _pre_phi = var_37;
                                    rbp_1 = var_36;
                                    rbp_2 = var_36;
                                    r12_1 = 1UL;
                                    if ((uint64_t)(var_37 + (-9)) != 0UL) {
                                        if ((uint64_t)(var_37 + (-8)) == 0UL) {
                                            var_41 = rbx_6_ph + (rbx_6_ph != 0UL);
                                            var_42 = (uint64_t *)(local_sp_3 + 16UL);
                                            var_43 = *var_42;
                                            var_44 = var_43 + (var_43 != 0UL);
                                            *var_42 = var_44;
                                            rax_4 = var_44;
                                            rbx_2 = var_41;
                                            rax_8 = rax_4;
                                            rbx_5 = rbx_2;
                                            r8_8 = r8_4;
                                            local_sp_8 = local_sp_4;
                                            local_sp_6 = local_sp_4;
                                            rax_6 = rax_4;
                                            rbx_3 = rbx_2;
                                            rbp_1 = rbp_0;
                                            rcx_6 = rcx_4;
                                            r9_6 = r9_4;
                                            r8_6 = r8_4;
                                            rbp_3 = rbp_0;
                                            rcx_8 = rcx_4;
                                            r9_8 = r9_4;
                                            if (*(unsigned char *)6359124UL != '\x00') {
                                                _pre182 = (uint32_t)rbp_0;
                                                _pre_pre_phi = _pre182;
                                                var_71 = local_sp_6 + (-8L);
                                                *(uint64_t *)var_71 = 4202821UL;
                                                indirect_placeholder();
                                                _pre_phi = _pre_pre_phi;
                                                local_sp_7 = var_71;
                                                rax_7 = rax_6;
                                                rbx_4 = rbx_3;
                                                rbp_2 = rbp_1;
                                                r12_0 = ((uint64_t)(uint32_t)rax_6 != 0UL);
                                                rcx_7 = rcx_6;
                                                r9_7 = r9_6;
                                                r8_7 = r8_6;
                                                rax_8 = rax_7;
                                                rbx_5 = rbx_4;
                                                r8_8 = r8_7;
                                                local_sp_8 = local_sp_7;
                                                rbp_3 = rbp_2;
                                                r12_1 = r12_0;
                                                rcx_8 = rcx_7;
                                                r9_8 = r9_7;
                                                local_sp_11 = local_sp_7;
                                                if ((int)_pre_phi >= (int)0U) {
                                                    loop_state_var = 0U;
                                                    switch_state_var = 1;
                                                    break;
                                                }
                                            }
                                        }
                                        var_38 = rbx_6_ph + 1UL;
                                        rbx_3 = var_38;
                                        rbx_4 = var_38;
                                        if (var_38 == 0UL) {
                                            var_39 = local_sp_3 + (-8L);
                                            *(uint64_t *)var_39 = 4202799UL;
                                            var_40 = indirect_placeholder_29(0UL, 4249247UL, 1UL, rcx_3, 0UL, r9_3, r8_3);
                                            local_sp_5 = var_39;
                                            rax_5 = var_40.field_0;
                                            rcx_5 = var_40.field_3;
                                            r9_5 = var_40.field_5;
                                            r8_5 = var_40.field_6;
                                        }
                                        local_sp_6 = local_sp_5;
                                        rax_6 = rax_5;
                                        rcx_6 = rcx_5;
                                        r9_6 = r9_5;
                                        r8_6 = r8_5;
                                        local_sp_7 = local_sp_5;
                                        rax_7 = rax_5;
                                        rcx_7 = rcx_5;
                                        r9_7 = r9_5;
                                        r8_7 = r8_5;
                                        if (*(unsigned char *)6359124UL == '\x00') {
                                            var_71 = local_sp_6 + (-8L);
                                            *(uint64_t *)var_71 = 4202821UL;
                                            indirect_placeholder();
                                            _pre_phi = _pre_pre_phi;
                                            local_sp_7 = var_71;
                                            rax_7 = rax_6;
                                            rbx_4 = rbx_3;
                                            rbp_2 = rbp_1;
                                            r12_0 = ((uint64_t)(uint32_t)rax_6 != 0UL);
                                            rcx_7 = rcx_6;
                                            r9_7 = r9_6;
                                            r8_7 = r8_6;
                                        }
                                        rax_8 = rax_7;
                                        rbx_5 = rbx_4;
                                        r8_8 = r8_7;
                                        local_sp_8 = local_sp_7;
                                        rbp_3 = rbp_2;
                                        r12_1 = r12_0;
                                        rcx_8 = rcx_7;
                                        r9_8 = r9_7;
                                        local_sp_11 = local_sp_7;
                                        if ((int)_pre_phi >= (int)0U) {
                                            loop_state_var = 0U;
                                            switch_state_var = 1;
                                            break;
                                        }
                                        var_72 = local_sp_8 + (-8L);
                                        *(uint64_t *)var_72 = 4202846UL;
                                        indirect_placeholder();
                                        local_sp_0 = var_72;
                                        rax_0 = rax_8;
                                        rcx_0 = rcx_8;
                                        r9_0 = r9_8;
                                        r8_0 = r8_8;
                                        r13_3_ph = r13_1;
                                        r13_2 = r13_1;
                                        r10_2 = r10_1;
                                        rbx_6_ph = rbx_5;
                                        r12_2_ph = r12_1;
                                        r10_3_ph = r10_1;
                                        if ((int)(uint32_t)rax_8 > (int)4294967295U) {
                                            *(uint64_t *)(local_sp_8 + (-16L)) = 4202855UL;
                                            indirect_placeholder();
                                            var_73 = (uint64_t)*(uint32_t *)rax_8;
                                            var_74 = local_sp_8 + (-24L);
                                            *(uint64_t *)var_74 = 4202877UL;
                                            var_75 = indirect_placeholder_24(0UL, 4249270UL, 1UL, rcx_8, var_73, r9_8, r8_8);
                                            local_sp_0 = var_74;
                                            rax_0 = var_75.field_0;
                                            rcx_0 = var_75.field_3;
                                            r9_0 = var_75.field_5;
                                            r8_0 = var_75.field_6;
                                        }
                                        local_sp_9 = local_sp_0;
                                        local_sp_10_ph = local_sp_0;
                                        rcx_10_ph = rcx_0;
                                        rax_9 = rax_0;
                                        rcx_9 = rcx_0;
                                        r9_9 = r9_0;
                                        r8_9 = r8_0;
                                        rax_10_ph = rax_0;
                                        r9_10_ph = r9_0;
                                        r8_10_ph = r8_0;
                                        if ((uint64_t)((uint32_t)rbp_3 + (-10)) == 0UL) {
                                            continue;
                                        }
                                        loop_state_var = 1U;
                                        switch_state_var = 1;
                                        break;
                                    }
                                    var_45 = local_sp_3 + 15UL;
                                    var_46 = local_sp_3 + 16UL;
                                    var_47 = local_sp_3 + (-8L);
                                    *(uint64_t *)var_47 = 4202618UL;
                                    var_48 = indirect_placeholder_28(var_45, rbx_6_ph, var_36, rbx_6_ph, r10_0, var_46, r9_3);
                                    var_49 = var_48.field_0;
                                    var_50 = var_48.field_1;
                                    var_51 = var_48.field_2;
                                    var_52 = var_48.field_3;
                                    var_53 = var_48.field_4;
                                    var_54 = var_50 + 1UL;
                                    var_55 = (*(unsigned char *)(local_sp_3 + 7UL) == '\x00') ? var_49 : var_54;
                                    r8_1 = var_53;
                                    rbx_1 = var_50;
                                    local_sp_1 = var_47;
                                    rax_1 = var_54;
                                    rbx_0 = var_50;
                                    rcx_1 = var_51;
                                    r9_1 = var_52;
                                    rbp_0 = 32UL;
                                    if (var_50 > var_55) {
                                        var_56 = local_sp_3 + (-16L);
                                        *(uint64_t *)var_56 = 4202664UL;
                                        var_57 = indirect_placeholder_26(0UL, 4249247UL, 1UL, var_51, 0UL, var_52, var_53);
                                        var_58 = var_57.field_0;
                                        var_59 = var_57.field_3;
                                        var_60 = var_57.field_5;
                                        var_61 = var_57.field_6;
                                        rax_2 = var_58;
                                        r9_2 = var_60;
                                        r8_2 = var_61;
                                        local_sp_2 = var_56;
                                        rcx_2 = var_59;
                                        var_70 = rbx_0 + 1UL;
                                        rax_2 = rax_1;
                                        rbx_1 = var_70;
                                        r9_2 = r9_1;
                                        r8_2 = r8_1;
                                        local_sp_2 = local_sp_1;
                                        rcx_2 = rcx_1;
                                        local_sp_4 = local_sp_1;
                                        rax_4 = rax_1;
                                        rbx_2 = var_70;
                                        rcx_4 = rcx_1;
                                        r9_4 = r9_1;
                                        r8_4 = r8_1;
                                        do {
                                            var_62 = local_sp_2 + (-8L);
                                            *(uint64_t *)var_62 = 4202674UL;
                                            indirect_placeholder();
                                            r8_1 = r8_2;
                                            local_sp_1 = var_62;
                                            rax_1 = rax_2;
                                            rbx_0 = rbx_1;
                                            rcx_1 = rcx_2;
                                            r9_1 = r9_2;
                                            if ((int)(uint32_t)rax_2 > (int)4294967295U) {
                                                *(uint64_t *)(local_sp_2 + (-16L)) = 4202683UL;
                                                indirect_placeholder();
                                                var_63 = (uint64_t)*(uint32_t *)rax_2;
                                                var_64 = local_sp_2 + (-24L);
                                                *(uint64_t *)var_64 = 4202705UL;
                                                var_65 = indirect_placeholder_25(0UL, 4249270UL, 1UL, rcx_2, var_63, r9_2, r8_2);
                                                var_66 = var_65.field_0;
                                                var_67 = var_65.field_3;
                                                var_68 = var_65.field_5;
                                                var_69 = var_65.field_6;
                                                r8_1 = var_69;
                                                local_sp_1 = var_64;
                                                rax_1 = var_66;
                                                rcx_1 = var_67;
                                                r9_1 = var_68;
                                            }
                                            var_70 = rbx_0 + 1UL;
                                            rax_2 = rax_1;
                                            rbx_1 = var_70;
                                            r9_2 = r9_1;
                                            r8_2 = r8_1;
                                            local_sp_2 = local_sp_1;
                                            rcx_2 = rcx_1;
                                            local_sp_4 = local_sp_1;
                                            rax_4 = rax_1;
                                            rbx_2 = var_70;
                                            rcx_4 = rcx_1;
                                            r9_4 = r9_1;
                                            r8_4 = r8_1;
                                        } while (var_55 <= var_70);
                                    } else {
                                        var_70 = rbx_0 + 1UL;
                                        rax_2 = rax_1;
                                        rbx_1 = var_70;
                                        r9_2 = r9_1;
                                        r8_2 = r8_1;
                                        local_sp_2 = local_sp_1;
                                        rcx_2 = rcx_1;
                                        local_sp_4 = local_sp_1;
                                        rax_4 = rax_1;
                                        rbx_2 = var_70;
                                        rcx_4 = rcx_1;
                                        r9_4 = r9_1;
                                        r8_4 = r8_1;
                                        if (var_55 <= var_70) {
                                            var_70 = rbx_0 + 1UL;
                                            rax_2 = rax_1;
                                            rbx_1 = var_70;
                                            r9_2 = r9_1;
                                            r8_2 = r8_1;
                                            local_sp_2 = local_sp_1;
                                            rcx_2 = rcx_1;
                                            local_sp_4 = local_sp_1;
                                            rax_4 = rax_1;
                                            rbx_2 = var_70;
                                            rcx_4 = rcx_1;
                                            r9_4 = r9_1;
                                            r8_4 = r8_1;
                                            do {
                                                var_62 = local_sp_2 + (-8L);
                                                *(uint64_t *)var_62 = 4202674UL;
                                                indirect_placeholder();
                                                r8_1 = r8_2;
                                                local_sp_1 = var_62;
                                                rax_1 = rax_2;
                                                rbx_0 = rbx_1;
                                                rcx_1 = rcx_2;
                                                r9_1 = r9_2;
                                                if ((int)(uint32_t)rax_2 > (int)4294967295U) {
                                                    *(uint64_t *)(local_sp_2 + (-16L)) = 4202683UL;
                                                    indirect_placeholder();
                                                    var_63 = (uint64_t)*(uint32_t *)rax_2;
                                                    var_64 = local_sp_2 + (-24L);
                                                    *(uint64_t *)var_64 = 4202705UL;
                                                    var_65 = indirect_placeholder_25(0UL, 4249270UL, 1UL, rcx_2, var_63, r9_2, r8_2);
                                                    var_66 = var_65.field_0;
                                                    var_67 = var_65.field_3;
                                                    var_68 = var_65.field_5;
                                                    var_69 = var_65.field_6;
                                                    r8_1 = var_69;
                                                    local_sp_1 = var_64;
                                                    rax_1 = var_66;
                                                    rcx_1 = var_67;
                                                    r9_1 = var_68;
                                                }
                                                var_70 = rbx_0 + 1UL;
                                                rax_2 = rax_1;
                                                rbx_1 = var_70;
                                                r9_2 = r9_1;
                                                r8_2 = r8_1;
                                                local_sp_2 = local_sp_1;
                                                rcx_2 = rcx_1;
                                                local_sp_4 = local_sp_1;
                                                rax_4 = rax_1;
                                                rbx_2 = var_70;
                                                rcx_4 = rcx_1;
                                                r9_4 = r9_1;
                                                r8_4 = r8_1;
                                            } while (var_55 <= var_70);
                                        }
                                    }
                                    rax_8 = rax_4;
                                    rbx_5 = rbx_2;
                                    r8_8 = r8_4;
                                    local_sp_8 = local_sp_4;
                                    local_sp_6 = local_sp_4;
                                    rax_6 = rax_4;
                                    rbx_3 = rbx_2;
                                    rbp_1 = rbp_0;
                                    rcx_6 = rcx_4;
                                    r9_6 = r9_4;
                                    r8_6 = r8_4;
                                    rbp_3 = rbp_0;
                                    rcx_8 = rcx_4;
                                    r9_8 = r9_4;
                                    if (*(unsigned char *)6359124UL == '\x00') {
                                        var_72 = local_sp_8 + (-8L);
                                        *(uint64_t *)var_72 = 4202846UL;
                                        indirect_placeholder();
                                        local_sp_0 = var_72;
                                        rax_0 = rax_8;
                                        rcx_0 = rcx_8;
                                        r9_0 = r9_8;
                                        r8_0 = r8_8;
                                        r13_3_ph = r13_1;
                                        r13_2 = r13_1;
                                        r10_2 = r10_1;
                                        rbx_6_ph = rbx_5;
                                        r12_2_ph = r12_1;
                                        r10_3_ph = r10_1;
                                        if ((int)(uint32_t)rax_8 > (int)4294967295U) {
                                            *(uint64_t *)(local_sp_8 + (-16L)) = 4202855UL;
                                            indirect_placeholder();
                                            var_73 = (uint64_t)*(uint32_t *)rax_8;
                                            var_74 = local_sp_8 + (-24L);
                                            *(uint64_t *)var_74 = 4202877UL;
                                            var_75 = indirect_placeholder_24(0UL, 4249270UL, 1UL, rcx_8, var_73, r9_8, r8_8);
                                            local_sp_0 = var_74;
                                            rax_0 = var_75.field_0;
                                            rcx_0 = var_75.field_3;
                                            r9_0 = var_75.field_5;
                                            r8_0 = var_75.field_6;
                                        }
                                        local_sp_9 = local_sp_0;
                                        local_sp_10_ph = local_sp_0;
                                        rcx_10_ph = rcx_0;
                                        rax_9 = rax_0;
                                        rcx_9 = rcx_0;
                                        r9_9 = r9_0;
                                        r8_9 = r8_0;
                                        rax_10_ph = rax_0;
                                        r9_10_ph = r9_0;
                                        r8_10_ph = r8_0;
                                        if ((uint64_t)((uint32_t)rbp_3 + (-10)) == 0UL) {
                                            continue;
                                        }
                                        loop_state_var = 1U;
                                        switch_state_var = 1;
                                        break;
                                    }
                                    _pre182 = (uint32_t)rbp_0;
                                    _pre_pre_phi = _pre182;
                                    var_71 = local_sp_6 + (-8L);
                                    *(uint64_t *)var_71 = 4202821UL;
                                    indirect_placeholder();
                                    _pre_phi = _pre_pre_phi;
                                    local_sp_7 = var_71;
                                    rax_7 = rax_6;
                                    rbx_4 = rbx_3;
                                    rbp_2 = rbp_1;
                                    r12_0 = ((uint64_t)(uint32_t)rax_6 != 0UL);
                                    rcx_7 = rcx_6;
                                    r9_7 = r9_6;
                                    r8_7 = r8_6;
                                    rax_8 = rax_7;
                                    rbx_5 = rbx_4;
                                    r8_8 = r8_7;
                                    local_sp_8 = local_sp_7;
                                    rbp_3 = rbp_2;
                                    r12_1 = r12_0;
                                    rcx_8 = rcx_7;
                                    r9_8 = r9_7;
                                    local_sp_11 = local_sp_7;
                                    if ((int)_pre_phi < (int)0U) {
                                        loop_state_var = 0U;
                                        switch_state_var = 1;
                                        break;
                                    }
                                }
                                break;
                            }
                            if (switch_state_var)
                                break;
                        }
                    switch_state_var = 0;
                    switch (loop_state_var) {
                      case 0U:
                        {
                            switch_state_var = 1;
                            break;
                        }
                        break;
                      case 1U:
                        {
                            continue;
                        }
                        break;
                    }
                    if (switch_state_var)
                        break;
                }
            loop_state_var = 1U;
            break;
        }
    switch (loop_state_var) {
      case 0U:
        {
            *(uint64_t *)(local_sp_12 + (-8L)) = 4202448UL;
            indirect_placeholder_31(rsi, var_5, 1UL);
            abort();
        }
        break;
      case 1U:
        {
            *(uint64_t *)(local_sp_11 + (-8L)) = 4202935UL;
            indirect_placeholder();
            return;
        }
        break;
    }
}

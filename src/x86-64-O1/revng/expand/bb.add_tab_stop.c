typedef unsigned char __u_char;
typedef unsigned short __u_short;
typedef unsigned int __u_int;
typedef unsigned long __u_long;
typedef signed char __int8_t;
typedef unsigned char __uint8_t;
typedef short __int16_t;
typedef unsigned short __uint16_t;
typedef int __int32_t;
typedef unsigned int __uint32_t;
typedef long __int64_t;
typedef unsigned long __uint64_t;
typedef __int8_t __int_least8_t;
typedef __uint8_t __uint_least8_t;
typedef __int16_t __int_least16_t;
typedef __uint16_t __uint_least16_t;
typedef __int32_t __int_least32_t;
typedef __uint32_t __uint_least32_t;
typedef __int64_t __int_least64_t;
typedef __uint64_t __uint_least64_t;
typedef long __quad_t;
typedef unsigned long __u_quad_t;
typedef long __intmax_t;
typedef unsigned long __uintmax_t;
typedef unsigned long __dev_t;
typedef unsigned int __uid_t;
typedef unsigned int __gid_t;
typedef unsigned long __ino_t;
typedef unsigned long __ino64_t;
typedef unsigned int __mode_t;
typedef unsigned long __nlink_t;
typedef long __off_t;
typedef long __off64_t;
typedef int __pid_t;
typedef struct {
    int __val[2];
} __fsid_t;
typedef long __clock_t;
typedef unsigned long __rlim_t;
typedef unsigned long __rlim64_t;
typedef unsigned int __id_t;
typedef long __time_t;
typedef unsigned int __useconds_t;
typedef long __suseconds_t;
typedef int __daddr_t;
typedef int __key_t;
typedef int __clockid_t;
typedef void *__timer_t;
typedef long __blksize_t;
typedef long __blkcnt_t;
typedef long __blkcnt64_t;
typedef unsigned long __fsblkcnt_t;
typedef unsigned long __fsblkcnt64_t;
typedef unsigned long __fsfilcnt_t;
typedef unsigned long __fsfilcnt64_t;
typedef long __fsword_t;
typedef long __ssize_t;
typedef long __syscall_slong_t;
typedef unsigned long __syscall_ulong_t;
typedef __off64_t __loff_t;
typedef char *__caddr_t;
typedef long __intptr_t;
typedef unsigned int __socklen_t;
typedef int __sig_atomic_t;
typedef __int8_t int8_t;
typedef __int16_t int16_t;
typedef __int32_t int32_t;
typedef __int64_t int64_t;
typedef __uint8_t uint8_t;
typedef __uint16_t uint16_t;
typedef __uint32_t uint32_t;
typedef __uint64_t uint64_t;
typedef __int_least8_t int_least8_t;
typedef __int_least16_t int_least16_t;
typedef __int_least32_t int_least32_t;
typedef __int_least64_t int_least64_t;
typedef __uint_least8_t uint_least8_t;
typedef __uint_least16_t uint_least16_t;
typedef __uint_least32_t uint_least32_t;
typedef __uint_least64_t uint_least64_t;
typedef signed char int_fast8_t;
typedef long int_fast16_t;
typedef long int_fast32_t;
typedef long int_fast64_t;
typedef unsigned char uint_fast8_t;
typedef unsigned long uint_fast16_t;
typedef unsigned long uint_fast32_t;
typedef unsigned long uint_fast64_t;
typedef long intptr_t;
typedef unsigned long uintptr_t;
typedef __intmax_t intmax_t;
typedef __uintmax_t uintmax_t;
typedef _Bool bool;
struct bb_add_tab_stop_ret_type;
struct bb_add_tab_stop_ret_type {
    uint64_t field_0;
    uint64_t field_1;
    uint64_t field_2;
};
extern void abort(void);
extern uint64_t revng_init_local_sp(uint64_t param_0);
extern uint64_t init_rbx(void);
extern uint64_t init_rbp(void);
extern uint64_t init_cc_src2(void);
extern uint64_t helper_cc_compute_c_wrapper(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint32_t param_3);
extern void indirect_placeholder_6(uint64_t param_0, uint64_t param_1);
extern uint64_t indirect_placeholder_7(uint64_t param_0, uint64_t param_1);
extern uint64_t init_rcx(void);
struct bb_add_tab_stop_ret_type bb_add_tab_stop(uint64_t rdi, uint64_t r9, uint64_t r8) {
    uint64_t var_5;
    bool var_6;
    uint64_t rcx_0;
    uint64_t var_7;
    uint64_t var_8;
    uint64_t var_9;
    uint64_t var_10;
    uint64_t rbp_0;
    uint64_t var_0;
    uint64_t var_1;
    uint64_t var_2;
    uint64_t var_3;
    uint64_t var_4;
    uint64_t var_12;
    uint64_t rcx_3;
    uint64_t rax_0;
    uint64_t var_11;
    uint64_t var_15;
    uint64_t rcx_1;
    uint64_t _pre;
    uint64_t rsi_0;
    uint64_t var_13;
    uint64_t var_14;
    uint64_t rbp_1;
    uint64_t rcx_2;
    uint64_t var_16;
    struct bb_add_tab_stop_ret_type mrv;
    struct bb_add_tab_stop_ret_type mrv1;
    struct bb_add_tab_stop_ret_type mrv2;
    var_0 = revng_init_local_sp(0UL);
    var_1 = init_rbx();
    var_2 = init_rbp();
    var_3 = init_rcx();
    var_4 = init_cc_src2();
    *(uint64_t *)(var_0 + (-8L)) = var_2;
    *(uint64_t *)(var_0 + (-16L)) = var_1;
    var_5 = *(uint64_t *)6359152UL;
    var_6 = (var_5 == 0UL);
    rcx_0 = var_3;
    rbp_0 = 0UL;
    rax_0 = 0UL;
    var_15 = var_5;
    rsi_0 = 16UL;
    if (!var_6) {
        var_7 = *(uint64_t *)6359168UL;
        var_8 = var_5 << 3UL;
        var_9 = var_8 + var_7;
        var_10 = *(uint64_t *)(var_9 + (-8L));
        rcx_0 = var_8;
        var_12 = var_7;
        rcx_3 = var_8;
        rax_0 = var_10;
        rcx_1 = var_8;
        if (var_10 <= rdi) {
            if (var_5 != *(uint64_t *)6359160UL) {
                *(uint64_t *)6359152UL = (var_5 + 1UL);
                *(uint64_t *)var_9 = rdi;
                mrv.field_0 = rcx_3;
                mrv1 = mrv;
                mrv1.field_1 = r9;
                mrv2 = mrv1;
                mrv2.field_2 = r8;
                return mrv2;
            }
            rbp_1 = rbp_0;
            rcx_2 = rcx_1;
            if (var_12 == 0UL) {
                rsi_0 = var_5;
                if (!var_6 && var_5 > 1152921504606846975UL) {
                    *(uint64_t *)(var_0 + (-32L)) = 4203068UL;
                    indirect_placeholder_6(r9, r8);
                    abort();
                }
            }
            if (var_5 > 768614336404564649UL) {
                *(uint64_t *)(var_0 + (-32L)) = 4203088UL;
                indirect_placeholder_6(r9, r8);
                abort();
            }
            rsi_0 = (var_5 + (var_5 >> 1UL)) + 1UL;
            *(uint64_t *)6359160UL = rsi_0;
            var_13 = rsi_0 << 3UL;
            *(uint64_t *)(var_0 + (-32L)) = 4203122UL;
            var_14 = indirect_placeholder_7(var_12, var_13);
            *(uint64_t *)6359168UL = var_14;
            var_15 = *(uint64_t *)6359152UL;
            *(uint64_t *)6359152UL = (var_15 + 1UL);
            *(uint64_t *)((var_15 << 3UL) + *(uint64_t *)6359168UL) = rdi;
            var_16 = helper_cc_compute_c_wrapper(*(uint64_t *)6359616UL - rbp_1, rbp_1, var_4, 17U);
            rcx_3 = rcx_2;
            if (var_16 != 0UL) {
                *(uint64_t *)6359616UL = rbp_1;
            }
            mrv.field_0 = rcx_3;
            mrv1 = mrv;
            mrv1.field_1 = r9;
            mrv2 = mrv1;
            mrv2.field_2 = r8;
            return mrv2;
        }
    }
    var_11 = rdi - rax_0;
    rbp_0 = var_11;
    rcx_1 = rcx_0;
    rbp_1 = var_11;
    rcx_2 = rcx_0;
    if (var_5 == *(uint64_t *)6359160UL) {
        *(uint64_t *)6359152UL = (var_15 + 1UL);
        *(uint64_t *)((var_15 << 3UL) + *(uint64_t *)6359168UL) = rdi;
        var_16 = helper_cc_compute_c_wrapper(*(uint64_t *)6359616UL - rbp_1, rbp_1, var_4, 17U);
        rcx_3 = rcx_2;
        if (var_16 != 0UL) {
            *(uint64_t *)6359616UL = rbp_1;
        }
        mrv.field_0 = rcx_3;
        mrv1 = mrv;
        mrv1.field_1 = r9;
        mrv2 = mrv1;
        mrv2.field_2 = r8;
        return mrv2;
    }
    _pre = *(uint64_t *)6359168UL;
    var_12 = _pre;
    rbp_1 = rbp_0;
    rcx_2 = rcx_1;
    if (var_12 == 0UL) {
        rsi_0 = var_5;
        if (!var_6 && var_5 > 1152921504606846975UL) {
            *(uint64_t *)(var_0 + (-32L)) = 4203068UL;
            indirect_placeholder_6(r9, r8);
            abort();
        }
    }
    if (var_5 > 768614336404564649UL) {
        *(uint64_t *)(var_0 + (-32L)) = 4203088UL;
        indirect_placeholder_6(r9, r8);
        abort();
    }
    rsi_0 = (var_5 + (var_5 >> 1UL)) + 1UL;
    *(uint64_t *)6359160UL = rsi_0;
    var_13 = rsi_0 << 3UL;
    *(uint64_t *)(var_0 + (-32L)) = 4203122UL;
    var_14 = indirect_placeholder_7(var_12, var_13);
    *(uint64_t *)6359168UL = var_14;
    var_15 = *(uint64_t *)6359152UL;
}

typedef unsigned char __u_char;
typedef unsigned short __u_short;
typedef unsigned int __u_int;
typedef unsigned long __u_long;
typedef signed char __int8_t;
typedef unsigned char __uint8_t;
typedef short __int16_t;
typedef unsigned short __uint16_t;
typedef int __int32_t;
typedef unsigned int __uint32_t;
typedef long __int64_t;
typedef unsigned long __uint64_t;
typedef __int8_t __int_least8_t;
typedef __uint8_t __uint_least8_t;
typedef __int16_t __int_least16_t;
typedef __uint16_t __uint_least16_t;
typedef __int32_t __int_least32_t;
typedef __uint32_t __uint_least32_t;
typedef __int64_t __int_least64_t;
typedef __uint64_t __uint_least64_t;
typedef long __quad_t;
typedef unsigned long __u_quad_t;
typedef long __intmax_t;
typedef unsigned long __uintmax_t;
typedef unsigned long __dev_t;
typedef unsigned int __uid_t;
typedef unsigned int __gid_t;
typedef unsigned long __ino_t;
typedef unsigned long __ino64_t;
typedef unsigned int __mode_t;
typedef unsigned long __nlink_t;
typedef long __off_t;
typedef long __off64_t;
typedef int __pid_t;
typedef struct {
    int __val[2];
} __fsid_t;
typedef long __clock_t;
typedef unsigned long __rlim_t;
typedef unsigned long __rlim64_t;
typedef unsigned int __id_t;
typedef long __time_t;
typedef unsigned int __useconds_t;
typedef long __suseconds_t;
typedef int __daddr_t;
typedef int __key_t;
typedef int __clockid_t;
typedef void *__timer_t;
typedef long __blksize_t;
typedef long __blkcnt_t;
typedef long __blkcnt64_t;
typedef unsigned long __fsblkcnt_t;
typedef unsigned long __fsblkcnt64_t;
typedef unsigned long __fsfilcnt_t;
typedef unsigned long __fsfilcnt64_t;
typedef long __fsword_t;
typedef long __ssize_t;
typedef long __syscall_slong_t;
typedef unsigned long __syscall_ulong_t;
typedef __off64_t __loff_t;
typedef char *__caddr_t;
typedef long __intptr_t;
typedef unsigned int __socklen_t;
typedef int __sig_atomic_t;
typedef __int8_t int8_t;
typedef __int16_t int16_t;
typedef __int32_t int32_t;
typedef __int64_t int64_t;
typedef __uint8_t uint8_t;
typedef __uint16_t uint16_t;
typedef __uint32_t uint32_t;
typedef __uint64_t uint64_t;
typedef __int_least8_t int_least8_t;
typedef __int_least16_t int_least16_t;
typedef __int_least32_t int_least32_t;
typedef __int_least64_t int_least64_t;
typedef __uint_least8_t uint_least8_t;
typedef __uint_least16_t uint_least16_t;
typedef __uint_least32_t uint_least32_t;
typedef __uint_least64_t uint_least64_t;
typedef signed char int_fast8_t;
typedef long int_fast16_t;
typedef long int_fast32_t;
typedef long int_fast64_t;
typedef unsigned char uint_fast8_t;
typedef unsigned long uint_fast16_t;
typedef unsigned long uint_fast32_t;
typedef unsigned long uint_fast64_t;
typedef long intptr_t;
typedef unsigned long uintptr_t;
typedef __intmax_t intmax_t;
typedef __uintmax_t uintmax_t;
typedef _Bool bool;
extern uint64_t revng_init_local_sp(uint64_t param_0);
extern void indirect_placeholder(void);
void bb_usage(uint64_t rbx, uint64_t rbp, uint64_t rdi) {
    uint64_t var_0;
    bool var_1;
    uint64_t var_2;
    uint64_t *var_3;
    uint64_t local_sp_5;
    uint64_t local_sp_0;
    uint64_t var_25;
    uint64_t local_sp_1;
    uint64_t var_26;
    uint64_t *var_4;
    uint64_t *var_5;
    uint64_t *var_6;
    uint64_t *var_7;
    uint64_t *var_8;
    uint64_t *var_9;
    uint64_t *var_10;
    uint64_t *var_11;
    uint64_t *var_12;
    uint64_t *var_13;
    uint64_t *var_14;
    uint64_t *var_15;
    uint64_t *var_16;
    uint64_t var_17;
    uint64_t *var_18;
    uint64_t *var_19;
    uint64_t *var_20;
    uint64_t var_21;
    uint64_t *var_22;
    uint64_t var_23;
    uint64_t *var_24;
    uint64_t var_27;
    var_0 = revng_init_local_sp(0UL);
    *(uint64_t *)(var_0 + (-8L)) = rbp;
    *(uint64_t *)(var_0 + (-16L)) = rbx;
    var_1 = ((uint64_t)(uint32_t)rdi == 0UL);
    var_2 = var_0 + (-144L);
    var_3 = (uint64_t *)var_2;
    local_sp_5 = var_2;
    if (!var_1) {
        *var_3 = 4207868UL;
        indirect_placeholder();
        var_27 = local_sp_5 + (-8L);
        *(uint64_t *)var_27 = 4208663UL;
        indirect_placeholder();
        local_sp_0 = var_27;
        while (1U)
            {
                var_25 = local_sp_0 + (-8L);
                *(uint64_t *)var_25 = 4208688UL;
                indirect_placeholder();
                local_sp_1 = var_25;
                var_26 = local_sp_1 + (-8L);
                *(uint64_t *)var_26 = 4208656UL;
                indirect_placeholder();
                local_sp_5 = var_26;
                var_27 = local_sp_5 + (-8L);
                *(uint64_t *)var_27 = 4208663UL;
                indirect_placeholder();
                local_sp_0 = var_27;
                continue;
            }
    }
    *var_3 = 4207895UL;
    indirect_placeholder();
    *(uint64_t *)(var_0 + (-152L)) = 4207915UL;
    indirect_placeholder();
    *(uint64_t *)(var_0 + (-160L)) = 4207928UL;
    indirect_placeholder();
    *(uint64_t *)(var_0 + (-168L)) = 4207941UL;
    indirect_placeholder();
    *(uint64_t *)(var_0 + (-176L)) = 4207954UL;
    indirect_placeholder();
    *(uint64_t *)(var_0 + (-184L)) = 4207967UL;
    indirect_placeholder();
    *(uint64_t *)(var_0 + (-192L)) = 4207980UL;
    indirect_placeholder();
    *(uint64_t *)(var_0 + (-200L)) = 4207993UL;
    indirect_placeholder();
    *(uint64_t *)(var_0 + (-208L)) = 4208006UL;
    indirect_placeholder();
    *(uint64_t *)(var_0 + (-216L)) = 4208019UL;
    indirect_placeholder();
    *(uint64_t *)(var_0 + (-224L)) = 4208032UL;
    indirect_placeholder();
    *(uint64_t *)(var_0 + (-232L)) = 4208045UL;
    indirect_placeholder();
    *(uint64_t *)(var_0 + (-240L)) = 4208058UL;
    indirect_placeholder();
    *(uint64_t *)(var_0 + (-248L)) = 4208071UL;
    indirect_placeholder();
    *(uint64_t *)(var_0 + (-256L)) = 4208084UL;
    indirect_placeholder();
    *(uint64_t *)(var_0 + (-264L)) = 4208097UL;
    indirect_placeholder();
    var_4 = (uint64_t *)(var_0 + (-272L));
    *var_4 = 4208110UL;
    indirect_placeholder();
    var_5 = (uint64_t *)(var_0 + (-280L));
    *var_5 = 4208123UL;
    indirect_placeholder();
    var_6 = (uint64_t *)(var_0 + (-288L));
    *var_6 = 4208136UL;
    indirect_placeholder();
    var_7 = (uint64_t *)(var_0 + (-296L));
    *var_7 = 4208149UL;
    indirect_placeholder();
    var_8 = (uint64_t *)(var_0 + (-304L));
    *var_8 = 4208162UL;
    indirect_placeholder();
    var_9 = (uint64_t *)(var_0 + (-312L));
    *var_9 = 4208175UL;
    indirect_placeholder();
    var_10 = (uint64_t *)(var_0 + (-320L));
    *var_10 = 4208188UL;
    indirect_placeholder();
    var_11 = (uint64_t *)(var_0 + (-328L));
    *var_11 = 4208201UL;
    indirect_placeholder();
    var_12 = (uint64_t *)(var_0 + (-336L));
    *var_12 = 4208214UL;
    indirect_placeholder();
    var_13 = (uint64_t *)(var_0 + (-344L));
    *var_13 = 4208227UL;
    indirect_placeholder();
    var_14 = (uint64_t *)(var_0 + (-352L));
    *var_14 = 4208240UL;
    indirect_placeholder();
    var_15 = (uint64_t *)(var_0 + (-360L));
    *var_15 = 4208253UL;
    indirect_placeholder();
    var_16 = (uint64_t *)(var_0 + (-368L));
    *var_16 = 4208275UL;
    indirect_placeholder();
    var_17 = *(uint64_t *)6387248UL;
    var_18 = (uint64_t *)(var_0 + (-376L));
    *var_18 = var_17;
    var_19 = (uint64_t *)(var_0 + (-384L));
    *var_19 = var_17;
    var_20 = (uint64_t *)(var_0 + (-392L));
    *var_20 = var_17;
    var_21 = var_0 + (-400L);
    var_22 = (uint64_t *)var_21;
    *var_22 = var_17;
    var_23 = var_0 + (-408L);
    var_24 = (uint64_t *)var_23;
    *var_24 = 4208313UL;
    indirect_placeholder();
    *var_18 = 4263031UL;
    *var_16 = 4263033UL;
    *var_15 = 4263130UL;
    *var_14 = 4263049UL;
    *var_13 = 4263071UL;
    *var_12 = 4263081UL;
    *var_11 = 4263096UL;
    *var_10 = 4263081UL;
    *var_9 = 4263106UL;
    *var_8 = 4263081UL;
    *var_7 = 4263116UL;
    *var_6 = 4263081UL;
    *var_5 = 0UL;
    *var_4 = 0UL;
    *var_19 = 4208481UL;
    indirect_placeholder();
    local_sp_0 = var_21;
    local_sp_1 = var_23;
    if (*var_16 == 0UL) {
        *var_20 = 4208725UL;
        indirect_placeholder();
        *var_22 = 4208740UL;
        indirect_placeholder();
    } else {
        *var_20 = 4208523UL;
        indirect_placeholder();
        *var_22 = 4208538UL;
        indirect_placeholder();
        *var_24 = 4208617UL;
        indirect_placeholder();
        var_26 = local_sp_1 + (-8L);
        *(uint64_t *)var_26 = 4208656UL;
        indirect_placeholder();
        local_sp_5 = var_26;
        var_27 = local_sp_5 + (-8L);
        *(uint64_t *)var_27 = 4208663UL;
        indirect_placeholder();
        local_sp_0 = var_27;
    }
    while (1U)
        {
            var_25 = local_sp_0 + (-8L);
            *(uint64_t *)var_25 = 4208688UL;
            indirect_placeholder();
            local_sp_1 = var_25;
            var_26 = local_sp_1 + (-8L);
            *(uint64_t *)var_26 = 4208656UL;
            indirect_placeholder();
            local_sp_5 = var_26;
            var_27 = local_sp_5 + (-8L);
            *(uint64_t *)var_27 = 4208663UL;
            indirect_placeholder();
            local_sp_0 = var_27;
            continue;
        }
}

typedef unsigned char __u_char;
typedef unsigned short __u_short;
typedef unsigned int __u_int;
typedef unsigned long __u_long;
typedef signed char __int8_t;
typedef unsigned char __uint8_t;
typedef short __int16_t;
typedef unsigned short __uint16_t;
typedef int __int32_t;
typedef unsigned int __uint32_t;
typedef long __int64_t;
typedef unsigned long __uint64_t;
typedef __int8_t __int_least8_t;
typedef __uint8_t __uint_least8_t;
typedef __int16_t __int_least16_t;
typedef __uint16_t __uint_least16_t;
typedef __int32_t __int_least32_t;
typedef __uint32_t __uint_least32_t;
typedef __int64_t __int_least64_t;
typedef __uint64_t __uint_least64_t;
typedef long __quad_t;
typedef unsigned long __u_quad_t;
typedef long __intmax_t;
typedef unsigned long __uintmax_t;
typedef unsigned long __dev_t;
typedef unsigned int __uid_t;
typedef unsigned int __gid_t;
typedef unsigned long __ino_t;
typedef unsigned long __ino64_t;
typedef unsigned int __mode_t;
typedef unsigned long __nlink_t;
typedef long __off_t;
typedef long __off64_t;
typedef int __pid_t;
typedef struct {
    int __val[2];
} __fsid_t;
typedef long __clock_t;
typedef unsigned long __rlim_t;
typedef unsigned long __rlim64_t;
typedef unsigned int __id_t;
typedef long __time_t;
typedef unsigned int __useconds_t;
typedef long __suseconds_t;
typedef int __daddr_t;
typedef int __key_t;
typedef int __clockid_t;
typedef void *__timer_t;
typedef long __blksize_t;
typedef long __blkcnt_t;
typedef long __blkcnt64_t;
typedef unsigned long __fsblkcnt_t;
typedef unsigned long __fsblkcnt64_t;
typedef unsigned long __fsfilcnt_t;
typedef unsigned long __fsfilcnt64_t;
typedef long __fsword_t;
typedef long __ssize_t;
typedef long __syscall_slong_t;
typedef unsigned long __syscall_ulong_t;
typedef __off64_t __loff_t;
typedef char *__caddr_t;
typedef long __intptr_t;
typedef unsigned int __socklen_t;
typedef int __sig_atomic_t;
typedef __int8_t int8_t;
typedef __int16_t int16_t;
typedef __int32_t int32_t;
typedef __int64_t int64_t;
typedef __uint8_t uint8_t;
typedef __uint16_t uint16_t;
typedef __uint32_t uint32_t;
typedef __uint64_t uint64_t;
typedef __int_least8_t int_least8_t;
typedef __int_least16_t int_least16_t;
typedef __int_least32_t int_least32_t;
typedef __int_least64_t int_least64_t;
typedef __uint_least8_t uint_least8_t;
typedef __uint_least16_t uint_least16_t;
typedef __uint_least32_t uint_least32_t;
typedef __uint_least64_t uint_least64_t;
typedef signed char int_fast8_t;
typedef long int_fast16_t;
typedef long int_fast32_t;
typedef long int_fast64_t;
typedef unsigned char uint_fast8_t;
typedef unsigned long uint_fast16_t;
typedef unsigned long uint_fast32_t;
typedef unsigned long uint_fast64_t;
typedef long intptr_t;
typedef unsigned long uintptr_t;
typedef __intmax_t intmax_t;
typedef __uintmax_t uintmax_t;
typedef _Bool bool;
struct indirect_placeholder_27_ret_type;
struct indirect_placeholder_26_ret_type;
struct indirect_placeholder_27_ret_type {
    uint64_t field_0;
    uint64_t field_1;
    uint64_t field_2;
};
struct indirect_placeholder_26_ret_type {
    uint64_t field_0;
    uint64_t field_1;
    uint64_t field_2;
    uint64_t field_3;
    uint64_t field_4;
};
extern void abort(void);
extern uint64_t revng_init_local_sp(uint64_t param_0);
extern uint64_t init_rbx(void);
extern void indirect_placeholder_1(void);
extern uint64_t init_rbp(void);
extern uint64_t init_r12(void);
extern uint64_t indirect_placeholder(uint64_t param_0);
extern uint64_t indirect_placeholder_2(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint64_t param_3, uint64_t param_4);
extern void indirect_placeholder_12(uint64_t param_0);
extern void indirect_placeholder_10(uint64_t param_0, uint64_t param_1);
extern void indirect_placeholder_25(uint64_t param_0, uint64_t param_1, uint64_t param_2);
extern struct indirect_placeholder_27_ret_type indirect_placeholder_27(uint64_t param_0);
extern struct indirect_placeholder_26_ret_type indirect_placeholder_26(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint64_t param_3, uint64_t param_4, uint64_t param_5, uint64_t param_6);
extern void indirect_placeholder_28(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint64_t param_3, uint64_t param_4, uint64_t param_5, uint64_t param_6);
void bb_main(uint64_t rdi, uint64_t rsi) {
    uint32_t var_10;
    uint32_t var_16;
    uint64_t var_17;
    uint32_t var_18;
    uint64_t var_19;
    uint64_t local_sp_1;
    uint64_t local_sp_0;
    uint64_t var_9;
    uint64_t var_0;
    uint64_t var_1;
    uint64_t var_2;
    uint64_t var_3;
    uint32_t var_4;
    uint64_t var_5;
    uint64_t var_6;
    uint64_t var_7;
    uint64_t var_8;
    uint64_t r12_0;
    uint64_t var_20;
    struct indirect_placeholder_27_ret_type var_21;
    uint64_t var_22;
    uint64_t var_23;
    uint64_t var_24;
    uint64_t var_25;
    uint64_t var_14;
    bool var_15;
    uint32_t storemerge;
    uint64_t var_11;
    uint64_t var_12;
    uint64_t var_13;
    uint64_t r12_0_be;
    unsigned int loop_state_var;
    var_0 = revng_init_local_sp(0UL);
    var_1 = init_rbx();
    var_2 = init_rbp();
    var_3 = init_r12();
    *(uint64_t *)(var_0 + (-8L)) = var_3;
    *(uint64_t *)(var_0 + (-16L)) = var_2;
    *(uint64_t *)(var_0 + (-24L)) = var_1;
    var_4 = (uint32_t)rdi;
    var_5 = (uint64_t)var_4;
    var_6 = *(uint64_t *)rsi;
    *(uint64_t *)(var_0 + (-32L)) = 4205745UL;
    indirect_placeholder_12(var_6);
    *(uint64_t *)(var_0 + (-40L)) = 4205760UL;
    indirect_placeholder_1();
    var_7 = var_0 + (-48L);
    *(uint64_t *)var_7 = 4205770UL;
    indirect_placeholder_1();
    local_sp_1 = var_7;
    r12_0 = 1UL;
    while (1U)
        {
            var_8 = local_sp_1 + (-8L);
            *(uint64_t *)var_8 = 4206392UL;
            var_9 = indirect_placeholder_2(4252123UL, var_5, 4253824UL, rsi, 0UL);
            var_10 = (uint32_t)var_9;
            r12_0_be = r12_0;
            local_sp_0 = var_8;
            local_sp_1 = var_8;
            if ((uint64_t)(var_10 + 1U) != 0UL) {
                if ((uint64_t)(unsigned char)r12_0 != 0UL) {
                    loop_state_var = 0U;
                    break;
                }
                *(unsigned char *)6363145UL = (unsigned char)'\x01';
                *(unsigned char *)6363156UL = (unsigned char)'\x01';
                loop_state_var = 0U;
                break;
            }
            if ((uint64_t)(var_10 + (-109)) == 0UL) {
                *(unsigned char *)6363144UL = (unsigned char)'\x01';
            } else {
                r12_0_be = 0UL;
                if ((int)var_10 > (int)109U) {
                    r12_0_be = r12_0;
                    if ((uint64_t)(var_10 + (-115)) == 0UL) {
                        *(unsigned char *)6363156UL = (unsigned char)'\x01';
                    } else {
                        if ((int)var_10 > (int)115U) {
                            if ((uint64_t)(var_10 + (-117)) == 0UL) {
                                *(unsigned char *)6363145UL = (unsigned char)'\x01';
                                *(unsigned char *)6363155UL = (unsigned char)'\x01';
                            } else {
                                if ((int)var_10 < (int)117U) {
                                    *(unsigned char *)6363147UL = (unsigned char)'\x01';
                                } else {
                                    if ((uint64_t)(var_10 + (-119)) == 0UL) {
                                        *(unsigned char *)6363153UL = (unsigned char)'\x01';
                                        r12_0_be = r12_0;
                                    } else {
                                        if ((uint64_t)(var_10 + (-128)) != 0UL) {
                                            loop_state_var = 1U;
                                            break;
                                        }
                                        *(unsigned char *)6363158UL = (unsigned char)'\x01';
                                    }
                                }
                            }
                        } else {
                            if ((uint64_t)(var_10 + (-113)) == 0UL) {
                                *(unsigned char *)6363157UL = (unsigned char)'\x01';
                            } else {
                                if ((int)var_10 > (int)113U) {
                                    *(unsigned char *)6363146UL = (unsigned char)'\x01';
                                    *(unsigned char *)6363155UL = (unsigned char)'\x01';
                                } else {
                                    if ((uint64_t)(var_10 + (-112)) != 0UL) {
                                        loop_state_var = 1U;
                                        break;
                                    }
                                    *(unsigned char *)6363148UL = (unsigned char)'\x01';
                                }
                            }
                        }
                    }
                } else {
                    if ((uint64_t)(var_10 + (-84)) == 0UL) {
                        *(unsigned char *)6363153UL = (unsigned char)'\x01';
                        r12_0_be = r12_0;
                    } else {
                        if ((int)var_10 > (int)84U) {
                            if ((uint64_t)(var_10 + (-98)) == 0UL) {
                                *(unsigned char *)6363151UL = (unsigned char)'\x01';
                            } else {
                                if ((int)var_10 > (int)98U) {
                                    if ((uint64_t)(var_10 + (-100)) == 0UL) {
                                        *(unsigned char *)6363150UL = (unsigned char)'\x01';
                                        *(unsigned char *)6363155UL = (unsigned char)'\x01';
                                        *(unsigned char *)6363152UL = (unsigned char)'\x01';
                                    } else {
                                        if ((uint64_t)(var_10 + (-108)) != 0UL) {
                                            loop_state_var = 1U;
                                            break;
                                        }
                                        *(unsigned char *)6363149UL = (unsigned char)'\x01';
                                        *(unsigned char *)6363155UL = (unsigned char)'\x01';
                                    }
                                } else {
                                    if ((uint64_t)(var_10 + (-97)) != 0UL) {
                                        loop_state_var = 1U;
                                        break;
                                    }
                                    *(unsigned char *)6363151UL = (unsigned char)'\x01';
                                    *(unsigned char *)6363150UL = (unsigned char)'\x01';
                                    *(unsigned char *)6363149UL = (unsigned char)'\x01';
                                    *(unsigned char *)6363148UL = (unsigned char)'\x01';
                                    *(unsigned char *)6363146UL = (unsigned char)'\x01';
                                    *(unsigned char *)6363147UL = (unsigned char)'\x01';
                                    *(unsigned char *)6363145UL = (unsigned char)'\x01';
                                    *(unsigned char *)6363153UL = (unsigned char)'\x01';
                                    *(unsigned char *)6363155UL = (unsigned char)'\x01';
                                    *(unsigned char *)6363152UL = (unsigned char)'\x01';
                                }
                            }
                        } else {
                            r12_0_be = r12_0;
                            if ((uint64_t)(var_10 + 130U) == 0UL) {
                                *(uint64_t *)(local_sp_1 + (-16L)) = 4206293UL;
                                indirect_placeholder_25(rsi, var_5, 0UL);
                                abort();
                            }
                            if ((uint64_t)(var_10 + (-72)) != 0UL) {
                                if ((uint64_t)(var_10 + 131U) != 0UL) {
                                    loop_state_var = 1U;
                                    break;
                                }
                                *(uint64_t *)(local_sp_1 + (-16L)) = 0UL;
                                *(uint64_t *)(local_sp_1 + (-24L)) = 4252109UL;
                                var_11 = *(uint64_t *)6362704UL;
                                var_12 = *(uint64_t *)6362880UL;
                                *(uint64_t *)(local_sp_1 + (-32L)) = 4206346UL;
                                indirect_placeholder_28(0UL, 4252035UL, var_12, var_11, 4251906UL, 4252076UL, 4252092UL);
                                var_13 = local_sp_1 + (-40L);
                                *(uint64_t *)var_13 = 4206356UL;
                                indirect_placeholder_1();
                                local_sp_0 = var_13;
                                loop_state_var = 1U;
                                break;
                            }
                            *(unsigned char *)6363154UL = (unsigned char)'\x01';
                        }
                    }
                }
            }
            r12_0 = r12_0_be;
            continue;
        }
    switch (loop_state_var) {
      case 1U:
        {
            *(uint64_t *)(local_sp_0 + (-8L)) = 4206366UL;
            indirect_placeholder_25(rsi, var_5, 1UL);
            abort();
        }
        break;
      case 0U:
        {
            if (*(unsigned char *)6363152UL == '\x00') {
                *(unsigned char *)6363156UL = (unsigned char)'\x00';
            }
            *(uint64_t *)(local_sp_1 + (-16L)) = 4206446UL;
            var_14 = indirect_placeholder(2UL);
            var_15 = ((uint64_t)(unsigned char)var_14 == 0UL);
            storemerge = var_15 ? 12U : 16U;
            *(uint64_t *)6363136UL = (var_15 ? 4252153UL : 4252138UL);
            *(uint32_t *)6363128UL = storemerge;
            var_16 = *(uint32_t *)6362812UL;
            var_17 = (uint64_t)var_16;
            var_18 = var_4 - var_16;
            var_19 = (uint64_t)var_18;
            if ((uint64_t)(var_18 + (-1)) == 0UL) {
                var_25 = *(uint64_t *)((uint64_t)((long)(var_17 << 32UL) >> (long)29UL) + rsi);
                *(uint64_t *)(local_sp_1 + (-24L)) = 4206564UL;
                indirect_placeholder_10(var_25, 0UL);
                return;
            }
            if ((int)var_18 > (int)1U) {
                if ((uint64_t)(var_18 + (-2)) != 0UL) {
                    *(unsigned char *)6363144UL = (unsigned char)'\x01';
                    *(uint64_t *)(local_sp_1 + (-24L)) = 4206546UL;
                    indirect_placeholder_10(4251925UL, 1UL);
                    return;
                }
            }
            if ((int)var_18 >= (int)4294967295U) {
                *(uint64_t *)(local_sp_1 + (-24L)) = 4206546UL;
                indirect_placeholder_10(4251925UL, 1UL);
                return;
            }
            var_20 = *(uint64_t *)(((uint64_t)((long)(var_17 << 32UL) >> (long)29UL) + rsi) + 16UL);
            *(uint64_t *)(local_sp_1 + (-24L)) = 4206578UL;
            var_21 = indirect_placeholder_27(var_20);
            var_22 = var_21.field_0;
            var_23 = var_21.field_1;
            var_24 = var_21.field_2;
            *(uint64_t *)(local_sp_1 + (-32L)) = 4206606UL;
            indirect_placeholder_26(0UL, 4252165UL, 0UL, var_22, 0UL, var_23, var_24);
            *(uint64_t *)(local_sp_1 + (-40L)) = 4206616UL;
            indirect_placeholder_25(rsi, var_19, 1UL);
            abort();
        }
        break;
    }
}

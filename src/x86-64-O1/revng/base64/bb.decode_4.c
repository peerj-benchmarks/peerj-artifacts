typedef unsigned char __u_char;
typedef unsigned short __u_short;
typedef unsigned int __u_int;
typedef unsigned long __u_long;
typedef signed char __int8_t;
typedef unsigned char __uint8_t;
typedef short __int16_t;
typedef unsigned short __uint16_t;
typedef int __int32_t;
typedef unsigned int __uint32_t;
typedef long __int64_t;
typedef unsigned long __uint64_t;
typedef __int8_t __int_least8_t;
typedef __uint8_t __uint_least8_t;
typedef __int16_t __int_least16_t;
typedef __uint16_t __uint_least16_t;
typedef __int32_t __int_least32_t;
typedef __uint32_t __uint_least32_t;
typedef __int64_t __int_least64_t;
typedef __uint64_t __uint_least64_t;
typedef long __quad_t;
typedef unsigned long __u_quad_t;
typedef long __intmax_t;
typedef unsigned long __uintmax_t;
typedef unsigned long __dev_t;
typedef unsigned int __uid_t;
typedef unsigned int __gid_t;
typedef unsigned long __ino_t;
typedef unsigned long __ino64_t;
typedef unsigned int __mode_t;
typedef unsigned long __nlink_t;
typedef long __off_t;
typedef long __off64_t;
typedef int __pid_t;
typedef struct {
    int __val[2];
} __fsid_t;
typedef long __clock_t;
typedef unsigned long __rlim_t;
typedef unsigned long __rlim64_t;
typedef unsigned int __id_t;
typedef long __time_t;
typedef unsigned int __useconds_t;
typedef long __suseconds_t;
typedef int __daddr_t;
typedef int __key_t;
typedef int __clockid_t;
typedef void *__timer_t;
typedef long __blksize_t;
typedef long __blkcnt_t;
typedef long __blkcnt64_t;
typedef unsigned long __fsblkcnt_t;
typedef unsigned long __fsblkcnt64_t;
typedef unsigned long __fsfilcnt_t;
typedef unsigned long __fsfilcnt64_t;
typedef long __fsword_t;
typedef long __ssize_t;
typedef long __syscall_slong_t;
typedef unsigned long __syscall_ulong_t;
typedef __off64_t __loff_t;
typedef char *__caddr_t;
typedef long __intptr_t;
typedef unsigned int __socklen_t;
typedef int __sig_atomic_t;
typedef __int8_t int8_t;
typedef __int16_t int16_t;
typedef __int32_t int32_t;
typedef __int64_t int64_t;
typedef __uint8_t uint8_t;
typedef __uint16_t uint16_t;
typedef __uint32_t uint32_t;
typedef __uint64_t uint64_t;
typedef __int_least8_t int_least8_t;
typedef __int_least16_t int_least16_t;
typedef __int_least32_t int_least32_t;
typedef __int_least64_t int_least64_t;
typedef __uint_least8_t uint_least8_t;
typedef __uint_least16_t uint_least16_t;
typedef __uint_least32_t uint_least32_t;
typedef __uint_least64_t uint_least64_t;
typedef signed char int_fast8_t;
typedef long int_fast16_t;
typedef long int_fast32_t;
typedef long int_fast64_t;
typedef unsigned char uint_fast8_t;
typedef unsigned long uint_fast16_t;
typedef unsigned long uint_fast32_t;
typedef unsigned long uint_fast64_t;
typedef long intptr_t;
typedef unsigned long uintptr_t;
typedef __intmax_t intmax_t;
typedef __uintmax_t uintmax_t;
extern uint64_t revng_init_local_sp(uint64_t param_0);
uint64_t bb_decode_4(uint64_t rdx, uint64_t rdi, uint64_t rcx, uint64_t rsi) {
    uint64_t *_cast;
    uint64_t var_0;
    unsigned char var_1;
    uint64_t var_2;
    unsigned char *var_3;
    unsigned char var_4;
    uint64_t var_5;
    uint64_t *var_6;
    uint64_t var_8;
    uint64_t var_7;
    uint64_t r9_0;
    unsigned char *var_9;
    unsigned char var_10;
    uint64_t r9_2;
    unsigned char var_11;
    uint64_t var_13;
    uint64_t var_12;
    uint64_t r9_1;
    unsigned char var_14;
    unsigned char var_15;
    revng_init_local_sp(0UL);
    _cast = (uint64_t *)rdx;
    var_0 = *_cast;
    var_8 = 0UL;
    r9_0 = var_0;
    var_13 = 0UL;
    if (rsi > 1UL) {
        return 0UL;
    }
    var_6 = (uint64_t *)rcx;
    if (*var_6 == 0UL) {
        *(unsigned char *)var_0 = ((unsigned char)(var_2 << 2UL) | (unsigned char)(uint64_t)((long)(var_5 << 56UL) >> (long)60UL));
        var_7 = *var_6 + (-1L);
        *var_6 = var_7;
        var_8 = var_7;
        r9_0 = var_0 + 1UL;
    }
    r9_1 = r9_0;
    r9_2 = r9_0;
    if (rsi == 2UL) {
        *_cast = r9_0;
        return 0UL;
    }
    var_9 = (unsigned char *)(rdi + 2UL);
    var_10 = *var_9;
    if ((uint64_t)(var_10 + '\xc3') == 0UL) {
        if (rsi == 4UL) {
            *_cast = r9_0;
            return 0UL;
        }
        if (*(unsigned char *)(rdi + 3UL) != '=') {
            *_cast = r9_0;
            return 0UL;
        }
    }
    var_11 = *(unsigned char *)((uint64_t)var_10 + 4252352UL);
    if ((signed char)var_11 > '\xff') {
        *_cast = r9_0;
        return 0UL;
    }
    if (var_8 == 0UL) {
        *(unsigned char *)r9_0 = ((unsigned char)(uint64_t)((long)((uint64_t)var_11 << 56UL) >> (long)58UL) | (unsigned char)((uint64_t)*(unsigned char *)((uint64_t)*var_3 + 4252352UL) << 4UL));
        var_12 = *var_6 + (-1L);
        *var_6 = var_12;
        var_13 = var_12;
        r9_1 = r9_0 + 1UL;
    }
    r9_2 = r9_1;
    if (rsi == 3UL) {
        *_cast = r9_1;
        return 0UL;
    }
    var_14 = *(unsigned char *)(rdi + 3UL);
    if ((uint64_t)(var_14 + '\xc3') == 0UL) {
        if (rsi == 4UL) {
            *_cast = r9_1;
            return 0UL;
        }
    }
    var_15 = *(unsigned char *)((uint64_t)var_14 + 4252352UL);
    if ((signed char)var_15 > '\xff') {
        *_cast = r9_1;
        return 0UL;
    }
    if (var_13 != 0UL) {
        *_cast = r9_2;
        return 1UL;
    }
    *(unsigned char *)r9_1 = ((*(unsigned char *)((uint64_t)*var_9 + 4252352UL) << '\x06') | var_15);
    *var_6 = (*var_6 + (-1L));
    r9_2 = r9_1 + 1UL;
    *_cast = r9_2;
    return 1UL;
}

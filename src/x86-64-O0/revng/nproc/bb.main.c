typedef unsigned char __u_char;
typedef unsigned short __u_short;
typedef unsigned int __u_int;
typedef unsigned long __u_long;
typedef signed char __int8_t;
typedef unsigned char __uint8_t;
typedef short __int16_t;
typedef unsigned short __uint16_t;
typedef int __int32_t;
typedef unsigned int __uint32_t;
typedef long __int64_t;
typedef unsigned long __uint64_t;
typedef __int8_t __int_least8_t;
typedef __uint8_t __uint_least8_t;
typedef __int16_t __int_least16_t;
typedef __uint16_t __uint_least16_t;
typedef __int32_t __int_least32_t;
typedef __uint32_t __uint_least32_t;
typedef __int64_t __int_least64_t;
typedef __uint64_t __uint_least64_t;
typedef long __quad_t;
typedef unsigned long __u_quad_t;
typedef long __intmax_t;
typedef unsigned long __uintmax_t;
typedef unsigned long __dev_t;
typedef unsigned int __uid_t;
typedef unsigned int __gid_t;
typedef unsigned long __ino_t;
typedef unsigned long __ino64_t;
typedef unsigned int __mode_t;
typedef unsigned long __nlink_t;
typedef long __off_t;
typedef long __off64_t;
typedef int __pid_t;
typedef struct {
    int __val[2];
} __fsid_t;
typedef long __clock_t;
typedef unsigned long __rlim_t;
typedef unsigned long __rlim64_t;
typedef unsigned int __id_t;
typedef long __time_t;
typedef unsigned int __useconds_t;
typedef long __suseconds_t;
typedef int __daddr_t;
typedef int __key_t;
typedef int __clockid_t;
typedef void *__timer_t;
typedef long __blksize_t;
typedef long __blkcnt_t;
typedef long __blkcnt64_t;
typedef unsigned long __fsblkcnt_t;
typedef unsigned long __fsblkcnt64_t;
typedef unsigned long __fsfilcnt_t;
typedef unsigned long __fsfilcnt64_t;
typedef long __fsword_t;
typedef long __ssize_t;
typedef long __syscall_slong_t;
typedef unsigned long __syscall_ulong_t;
typedef __off64_t __loff_t;
typedef char *__caddr_t;
typedef long __intptr_t;
typedef unsigned int __socklen_t;
typedef int __sig_atomic_t;
typedef __int8_t int8_t;
typedef __int16_t int16_t;
typedef __int32_t int32_t;
typedef __int64_t int64_t;
typedef __uint8_t uint8_t;
typedef __uint16_t uint16_t;
typedef __uint32_t uint32_t;
typedef __uint64_t uint64_t;
typedef __int_least8_t int_least8_t;
typedef __int_least16_t int_least16_t;
typedef __int_least32_t int_least32_t;
typedef __int_least64_t int_least64_t;
typedef __uint_least8_t uint_least8_t;
typedef __uint_least16_t uint_least16_t;
typedef __uint_least32_t uint_least32_t;
typedef __uint_least64_t uint_least64_t;
typedef signed char int_fast8_t;
typedef long int_fast16_t;
typedef long int_fast32_t;
typedef long int_fast64_t;
typedef unsigned char uint_fast8_t;
typedef unsigned long uint_fast16_t;
typedef unsigned long uint_fast32_t;
typedef unsigned long uint_fast64_t;
typedef long intptr_t;
typedef unsigned long uintptr_t;
typedef __intmax_t intmax_t;
typedef __uintmax_t uintmax_t;
struct indirect_placeholder_11_ret_type;
struct indirect_placeholder_13_ret_type;
struct indirect_placeholder_11_ret_type {
    uint64_t field_0;
    uint64_t field_1;
    uint64_t field_2;
};
struct indirect_placeholder_13_ret_type {
    uint64_t field_0;
    uint64_t field_1;
};
extern void abort(void);
extern uint64_t revng_init_local_sp(uint64_t param_0);
extern uint64_t init_rbp(void);
extern uint64_t init_cc_src2(void);
extern uint64_t helper_cc_compute_c_wrapper(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint32_t param_3);
extern void indirect_placeholder(void);
extern void indirect_placeholder_10(uint64_t param_0, uint64_t param_1);
extern uint64_t indirect_placeholder_3(uint64_t param_0);
extern void indirect_placeholder_9(uint64_t param_0);
extern uint64_t indirect_placeholder_6(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint64_t param_3, uint64_t param_4, uint64_t param_5);
extern void indirect_placeholder_4(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint64_t param_3, uint64_t param_4, uint64_t param_5, uint64_t param_6);
extern struct indirect_placeholder_11_ret_type indirect_placeholder_11(uint64_t param_0);
extern void indirect_placeholder_12(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint64_t param_3, uint64_t param_4, uint64_t param_5);
extern struct indirect_placeholder_13_ret_type indirect_placeholder_13(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint64_t param_3, uint64_t param_4);
void bb_main(uint64_t rsi, uint64_t rdi) {
    struct indirect_placeholder_13_ret_type var_15;
    uint64_t var_0;
    uint64_t var_1;
    uint64_t var_2;
    uint64_t var_3;
    uint32_t *var_4;
    uint64_t var_5;
    uint64_t *var_6;
    uint64_t *var_7;
    uint64_t var_8;
    uint64_t var_9;
    uint32_t *var_10;
    uint32_t *var_11;
    uint64_t local_sp_2;
    uint32_t var_22;
    uint64_t var_23;
    struct indirect_placeholder_11_ret_type var_24;
    uint64_t var_25;
    uint64_t var_26;
    uint64_t var_27;
    uint64_t var_28;
    uint64_t var_29;
    uint64_t *var_30;
    uint64_t var_31;
    uint64_t var_12;
    uint64_t var_17;
    uint64_t var_18;
    uint64_t local_sp_0;
    uint64_t var_19;
    uint64_t var_20;
    uint64_t var_21;
    uint64_t local_sp_1;
    uint64_t var_13;
    uint64_t var_14;
    uint32_t var_16;
    unsigned int loop_state_var;
    var_0 = revng_init_local_sp(0UL);
    var_1 = init_rbp();
    var_2 = init_cc_src2();
    var_3 = var_0 + (-8L);
    *(uint64_t *)var_3 = var_1;
    var_4 = (uint32_t *)(var_0 + (-44L));
    *var_4 = (uint32_t)rdi;
    var_5 = var_0 + (-56L);
    var_6 = (uint64_t *)var_5;
    *var_6 = rsi;
    var_7 = (uint64_t *)(var_0 + (-24L));
    *var_7 = 0UL;
    var_8 = **(uint64_t **)var_5;
    *(uint64_t *)(var_0 + (-64L)) = 4201968UL;
    indirect_placeholder_9(var_8);
    *(uint64_t *)(var_0 + (-72L)) = 4201983UL;
    indirect_placeholder();
    var_9 = var_0 + (-80L);
    *(uint64_t *)var_9 = 4201993UL;
    indirect_placeholder();
    var_10 = (uint32_t *)(var_0 + (-28L));
    *var_10 = 2U;
    var_11 = (uint32_t *)(var_0 + (-32L));
    local_sp_2 = var_9;
    while (1U)
        {
            var_12 = *var_6;
            var_13 = (uint64_t)*var_4;
            var_14 = local_sp_2 + (-8L);
            *(uint64_t *)var_14 = 4202030UL;
            var_15 = indirect_placeholder_13(4267643UL, 4267776UL, var_12, var_13, 0UL);
            var_16 = (uint32_t)var_15.field_0;
            *var_11 = var_16;
            local_sp_0 = var_14;
            if (var_16 != 4294967295U) {
                var_22 = *(uint32_t *)6378616UL;
                if ((uint64_t)(*var_4 - var_22) != 0UL) {
                    var_28 = (uint64_t)*var_10;
                    *(uint64_t *)(local_sp_2 + (-16L)) = 4202336UL;
                    var_29 = indirect_placeholder_3(var_28);
                    var_30 = (uint64_t *)(var_0 + (-16L));
                    *var_30 = var_29;
                    var_31 = helper_cc_compute_c_wrapper(*var_7 - var_29, var_29, var_2, 17U);
                    if (var_31 != 0UL) {
                        *var_30 = 1UL;
                        loop_state_var = 1U;
                        break;
                    }
                    *var_30 = (*var_30 - *var_7);
                    loop_state_var = 1U;
                    break;
                }
                var_23 = *(uint64_t *)(*var_6 + ((uint64_t)var_22 << 3UL));
                *(uint64_t *)(local_sp_2 + (-16L)) = 4202288UL;
                var_24 = indirect_placeholder_11(var_23);
                var_25 = var_24.field_0;
                var_26 = var_24.field_1;
                var_27 = var_24.field_2;
                *(uint64_t *)(local_sp_2 + (-24L)) = 4202316UL;
                indirect_placeholder_4(0UL, 4268389UL, var_25, 0UL, 0UL, var_26, var_27);
                *(uint64_t *)(local_sp_2 + (-32L)) = 4202326UL;
                indirect_placeholder_10(var_3, 1UL);
                abort();
            }
            if ((uint64_t)(var_16 + 130U) == 0UL) {
                *(uint64_t *)(local_sp_2 + (-16L)) = 4202118UL;
                indirect_placeholder_10(var_3, 0UL);
                abort();
            }
            if ((int)var_16 > (int)4294967166U) {
                if ((uint64_t)(var_16 + (-128)) != 0UL) {
                    if ((uint64_t)(var_16 + (-129)) != 0UL) {
                        loop_state_var = 0U;
                        break;
                    }
                    var_19 = *(uint64_t *)6379288UL;
                    var_20 = local_sp_2 + (-16L);
                    *(uint64_t *)var_20 = 4202233UL;
                    var_21 = indirect_placeholder_6(18446744073709551615UL, 4267643UL, 0UL, var_19, 0UL, 4268374UL);
                    *var_7 = var_21;
                    local_sp_1 = var_20;
                    local_sp_2 = local_sp_1;
                    continue;
                }
            }
            if ((uint64_t)(var_16 + 131U) != 0UL) {
                loop_state_var = 0U;
                break;
            }
            var_17 = *(uint64_t *)6378464UL;
            *(uint64_t *)(local_sp_2 + (-16L)) = 4202170UL;
            indirect_placeholder_12(0UL, 4267480UL, var_17, 4268350UL, 0UL, 4268356UL);
            var_18 = local_sp_2 + (-24L);
            *(uint64_t *)var_18 = 4202180UL;
            indirect_placeholder();
            local_sp_0 = var_18;
            *var_10 = 0U;
            local_sp_1 = local_sp_0;
            local_sp_2 = local_sp_1;
            continue;
        }
    switch (loop_state_var) {
      case 0U:
        {
            *(uint64_t *)(local_sp_2 + (-16L)) = 4202249UL;
            indirect_placeholder_10(var_3, 1UL);
            abort();
        }
        break;
      case 1U:
        {
            *(uint64_t *)(local_sp_2 + (-24L)) = 4202390UL;
            indirect_placeholder();
            return;
        }
        break;
    }
}

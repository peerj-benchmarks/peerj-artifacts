typedef unsigned char __u_char;
typedef unsigned short __u_short;
typedef unsigned int __u_int;
typedef unsigned long __u_long;
typedef signed char __int8_t;
typedef unsigned char __uint8_t;
typedef short __int16_t;
typedef unsigned short __uint16_t;
typedef int __int32_t;
typedef unsigned int __uint32_t;
typedef long __int64_t;
typedef unsigned long __uint64_t;
typedef __int8_t __int_least8_t;
typedef __uint8_t __uint_least8_t;
typedef __int16_t __int_least16_t;
typedef __uint16_t __uint_least16_t;
typedef __int32_t __int_least32_t;
typedef __uint32_t __uint_least32_t;
typedef __int64_t __int_least64_t;
typedef __uint64_t __uint_least64_t;
typedef long __quad_t;
typedef unsigned long __u_quad_t;
typedef long __intmax_t;
typedef unsigned long __uintmax_t;
typedef unsigned long __dev_t;
typedef unsigned int __uid_t;
typedef unsigned int __gid_t;
typedef unsigned long __ino_t;
typedef unsigned long __ino64_t;
typedef unsigned int __mode_t;
typedef unsigned long __nlink_t;
typedef long __off_t;
typedef long __off64_t;
typedef int __pid_t;
typedef struct {
    int __val[2];
} __fsid_t;
typedef long __clock_t;
typedef unsigned long __rlim_t;
typedef unsigned long __rlim64_t;
typedef unsigned int __id_t;
typedef long __time_t;
typedef unsigned int __useconds_t;
typedef long __suseconds_t;
typedef int __daddr_t;
typedef int __key_t;
typedef int __clockid_t;
typedef void *__timer_t;
typedef long __blksize_t;
typedef long __blkcnt_t;
typedef long __blkcnt64_t;
typedef unsigned long __fsblkcnt_t;
typedef unsigned long __fsblkcnt64_t;
typedef unsigned long __fsfilcnt_t;
typedef unsigned long __fsfilcnt64_t;
typedef long __fsword_t;
typedef long __ssize_t;
typedef long __syscall_slong_t;
typedef unsigned long __syscall_ulong_t;
typedef __off64_t __loff_t;
typedef char *__caddr_t;
typedef long __intptr_t;
typedef unsigned int __socklen_t;
typedef int __sig_atomic_t;
typedef __int8_t int8_t;
typedef __int16_t int16_t;
typedef __int32_t int32_t;
typedef __int64_t int64_t;
typedef __uint8_t uint8_t;
typedef __uint16_t uint16_t;
typedef __uint32_t uint32_t;
typedef __uint64_t uint64_t;
typedef __int_least8_t int_least8_t;
typedef __int_least16_t int_least16_t;
typedef __int_least32_t int_least32_t;
typedef __int_least64_t int_least64_t;
typedef __uint_least8_t uint_least8_t;
typedef __uint_least16_t uint_least16_t;
typedef __uint_least32_t uint_least32_t;
typedef __uint_least64_t uint_least64_t;
typedef signed char int_fast8_t;
typedef long int_fast16_t;
typedef long int_fast32_t;
typedef long int_fast64_t;
typedef unsigned char uint_fast8_t;
typedef unsigned long uint_fast16_t;
typedef unsigned long uint_fast32_t;
typedef unsigned long uint_fast64_t;
typedef long intptr_t;
typedef unsigned long uintptr_t;
typedef __intmax_t intmax_t;
typedef __uintmax_t uintmax_t;
typedef _Bool bool;
struct indirect_placeholder_58_ret_type;
struct indirect_placeholder_55_ret_type;
struct indirect_placeholder_57_ret_type;
struct indirect_placeholder_56_ret_type;
struct indirect_placeholder_58_ret_type {
    uint64_t field_0;
    uint64_t field_1;
    uint64_t field_2;
};
struct indirect_placeholder_55_ret_type {
    uint64_t field_0;
    uint64_t field_1;
    uint64_t field_2;
    uint64_t field_3;
};
struct indirect_placeholder_57_ret_type {
    uint64_t field_0;
    uint64_t field_1;
    uint64_t field_2;
    uint64_t field_3;
};
struct indirect_placeholder_56_ret_type {
    uint64_t field_0;
    uint64_t field_1;
    uint64_t field_2;
};
extern uint64_t revng_init_local_sp(uint64_t param_0);
extern uint64_t init_rbp(void);
extern uint64_t indirect_placeholder_3(uint64_t param_0, uint64_t param_1);
extern void indirect_placeholder(void);
extern uint64_t indirect_placeholder_2(uint64_t param_0, uint64_t param_1, uint64_t param_2);
extern uint64_t indirect_placeholder_14(uint64_t param_0);
extern void indirect_placeholder_12(uint64_t param_0);
extern struct indirect_placeholder_58_ret_type indirect_placeholder_58(uint64_t param_0, uint64_t param_1, uint64_t param_2);
extern struct indirect_placeholder_55_ret_type indirect_placeholder_55(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint64_t param_3, uint64_t param_4, uint64_t param_5, uint64_t param_6);
extern struct indirect_placeholder_57_ret_type indirect_placeholder_57(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint64_t param_3, uint64_t param_4, uint64_t param_5, uint64_t param_6);
extern struct indirect_placeholder_56_ret_type indirect_placeholder_56(uint64_t param_0, uint64_t param_1, uint64_t param_2);
uint64_t bb_docolon(uint64_t rdi, uint64_t rsi) {
    uint64_t var_0;
    uint64_t var_1;
    uint64_t *var_2;
    uint64_t *var_3;
    uint64_t var_4;
    uint64_t var_5;
    uint64_t var_6;
    uint64_t *var_7;
    uint64_t *var_8;
    uint64_t *var_9;
    uint64_t var_10;
    uint64_t var_11;
    uint64_t *var_12;
    uint64_t var_13;
    uint64_t var_14;
    uint64_t var_15;
    struct indirect_placeholder_58_ret_type var_16;
    uint64_t var_17;
    uint64_t local_sp_4;
    uint64_t var_33;
    uint64_t var_34;
    uint64_t var_35;
    uint64_t var_36;
    uint64_t var_37;
    uint64_t local_sp_3;
    uint64_t local_sp_1;
    uint64_t local_sp_2;
    uint64_t var_45;
    uint64_t var_30;
    uint64_t var_31;
    uint64_t var_32;
    bool var_39;
    uint64_t var_40;
    uint64_t *var_41;
    uint64_t var_42;
    uint64_t var_43;
    uint64_t var_38;
    uint64_t storemerge;
    uint64_t var_44;
    uint64_t var_18;
    uint64_t var_19;
    uint64_t var_20;
    unsigned char *var_21;
    uint64_t var_22;
    uint64_t var_23;
    uint64_t var_24;
    struct indirect_placeholder_56_ret_type var_25;
    uint64_t var_26;
    uint64_t var_27;
    uint64_t var_28;
    uint64_t *var_29;
    var_0 = revng_init_local_sp(0UL);
    var_1 = init_rbp();
    *(uint64_t *)(var_0 + (-8L)) = var_1;
    var_2 = (uint64_t *)(var_0 + (-400L));
    *var_2 = rdi;
    var_3 = (uint64_t *)(var_0 + (-408L));
    *var_3 = rsi;
    var_4 = *var_2;
    *(uint64_t *)(var_0 + (-416L)) = 4206824UL;
    indirect_placeholder_12(var_4);
    var_5 = *var_3;
    *(uint64_t *)(var_0 + (-424L)) = 4206839UL;
    indirect_placeholder_12(var_5);
    var_6 = var_0 + (-392L);
    var_7 = (uint64_t *)var_6;
    *var_7 = 0UL;
    var_8 = (uint64_t *)(var_0 + (-384L));
    *var_8 = 0UL;
    var_9 = (uint64_t *)(var_0 + (-376L));
    *var_9 = 0UL;
    var_10 = var_0 + (-104L);
    *(uint64_t *)var_10 = 0UL;
    *(uint64_t *)(var_0 + (-96L)) = 0UL;
    var_11 = var_0 + (-360L);
    var_12 = (uint64_t *)(var_0 + (-72L));
    *var_12 = var_11;
    *(uint64_t *)(var_0 + (-64L)) = 0UL;
    *(uint64_t *)6487520UL = 710UL;
    var_13 = *(uint64_t *)(*var_3 + 8UL);
    *(uint64_t *)(var_0 + (-432L)) = 4206937UL;
    indirect_placeholder();
    var_14 = *(uint64_t *)(*var_3 + 8UL);
    var_15 = var_0 + (-440L);
    *(uint64_t *)var_15 = 4206966UL;
    var_16 = indirect_placeholder_58(var_10, var_14, var_13);
    var_17 = var_16.field_0;
    *(uint64_t *)(var_0 + (-24L)) = var_17;
    storemerge = 75UL;
    local_sp_4 = var_15;
    if (var_17 == 0UL) {
        var_18 = var_16.field_2;
        var_19 = var_16.field_1;
        var_20 = var_0 + (-448L);
        *(uint64_t *)var_20 = 4207009UL;
        indirect_placeholder_57(0UL, 4365861UL, var_17, 2UL, 0UL, var_19, var_18);
        local_sp_4 = var_20;
    }
    var_21 = (unsigned char *)(var_0 + (-48L));
    *var_21 = (*var_21 & '\x7f');
    var_22 = *(uint64_t *)(*var_2 + 8UL);
    *(uint64_t *)(local_sp_4 + (-8L)) = 4207038UL;
    indirect_placeholder();
    var_23 = *(uint64_t *)(*var_2 + 8UL);
    var_24 = local_sp_4 + (-16L);
    *(uint64_t *)var_24 = 4207082UL;
    var_25 = indirect_placeholder_56(var_22, var_10, var_6);
    var_26 = var_25.field_0;
    var_27 = var_25.field_1;
    var_28 = var_25.field_2;
    var_29 = (uint64_t *)(var_0 + (-32L));
    *var_29 = var_26;
    local_sp_3 = var_24;
    if ((long)var_26 >= (long)0UL) {
        if (*(uint64_t *)(var_0 + (-56L)) == 0UL) {
            *(uint64_t *)(local_sp_4 + (-24L)) = 4207187UL;
            indirect_placeholder();
            var_33 = *var_29;
            var_34 = *(uint64_t *)(*var_2 + 8UL);
            *(uint64_t *)(local_sp_4 + (-32L)) = 4207225UL;
            var_35 = indirect_placeholder_3(var_34, var_33);
            *(uint64_t *)(var_0 + (-40L)) = var_35;
            var_36 = local_sp_4 + (-40L);
            *(uint64_t *)var_36 = 4207241UL;
            var_37 = indirect_placeholder_14(var_35);
            *(uint64_t *)(var_0 + (-16L)) = var_37;
            local_sp_1 = var_36;
        } else {
            *(unsigned char *)(*(uint64_t *)(*var_2 + 8UL) + *(uint64_t *)(*var_9 + 8UL)) = (unsigned char)'\x00';
            var_30 = *(uint64_t *)(*var_2 + 8UL) + *(uint64_t *)(*var_8 + 8UL);
            var_31 = local_sp_4 + (-24L);
            *(uint64_t *)var_31 = 4207173UL;
            var_32 = indirect_placeholder_2(0UL, var_30, var_23);
            *(uint64_t *)(var_0 + (-16L)) = var_32;
            local_sp_1 = var_31;
        }
        local_sp_2 = local_sp_1;
        if (*var_7 != 0UL) {
            *(uint64_t *)(local_sp_1 + (-8L)) = 4207365UL;
            indirect_placeholder();
            var_45 = local_sp_1 + (-16L);
            *(uint64_t *)var_45 = 4207380UL;
            indirect_placeholder();
            local_sp_2 = var_45;
        }
        *var_12 = 0UL;
        *(uint64_t *)(local_sp_2 + (-8L)) = 4207400UL;
        indirect_placeholder_12(var_10);
        return *(uint64_t *)(var_0 + (-16L));
    }
    var_39 = (*(uint64_t *)(var_0 + (-56L)) == 0UL);
    var_40 = local_sp_4 + (-24L);
    var_41 = (uint64_t *)var_40;
    local_sp_1 = var_40;
    if (var_39) {
        *var_41 = 4207289UL;
        var_43 = indirect_placeholder_2(0UL, 0UL, var_23);
        *(uint64_t *)(var_0 + (-16L)) = var_43;
    } else {
        *var_41 = 4207273UL;
        var_42 = indirect_placeholder_2(0UL, 4363643UL, var_23);
        *(uint64_t *)(var_0 + (-16L)) = var_42;
    }
}

typedef unsigned char __u_char;
typedef unsigned short __u_short;
typedef unsigned int __u_int;
typedef unsigned long __u_long;
typedef signed char __int8_t;
typedef unsigned char __uint8_t;
typedef short __int16_t;
typedef unsigned short __uint16_t;
typedef int __int32_t;
typedef unsigned int __uint32_t;
typedef long __int64_t;
typedef unsigned long __uint64_t;
typedef __int8_t __int_least8_t;
typedef __uint8_t __uint_least8_t;
typedef __int16_t __int_least16_t;
typedef __uint16_t __uint_least16_t;
typedef __int32_t __int_least32_t;
typedef __uint32_t __uint_least32_t;
typedef __int64_t __int_least64_t;
typedef __uint64_t __uint_least64_t;
typedef long __quad_t;
typedef unsigned long __u_quad_t;
typedef long __intmax_t;
typedef unsigned long __uintmax_t;
typedef unsigned long __dev_t;
typedef unsigned int __uid_t;
typedef unsigned int __gid_t;
typedef unsigned long __ino_t;
typedef unsigned long __ino64_t;
typedef unsigned int __mode_t;
typedef unsigned long __nlink_t;
typedef long __off_t;
typedef long __off64_t;
typedef int __pid_t;
typedef struct {
    int __val[2];
} __fsid_t;
typedef long __clock_t;
typedef unsigned long __rlim_t;
typedef unsigned long __rlim64_t;
typedef unsigned int __id_t;
typedef long __time_t;
typedef unsigned int __useconds_t;
typedef long __suseconds_t;
typedef int __daddr_t;
typedef int __key_t;
typedef int __clockid_t;
typedef void *__timer_t;
typedef long __blksize_t;
typedef long __blkcnt_t;
typedef long __blkcnt64_t;
typedef unsigned long __fsblkcnt_t;
typedef unsigned long __fsblkcnt64_t;
typedef unsigned long __fsfilcnt_t;
typedef unsigned long __fsfilcnt64_t;
typedef long __fsword_t;
typedef long __ssize_t;
typedef long __syscall_slong_t;
typedef unsigned long __syscall_ulong_t;
typedef __off64_t __loff_t;
typedef char *__caddr_t;
typedef long __intptr_t;
typedef unsigned int __socklen_t;
typedef int __sig_atomic_t;
typedef __int8_t int8_t;
typedef __int16_t int16_t;
typedef __int32_t int32_t;
typedef __int64_t int64_t;
typedef __uint8_t uint8_t;
typedef __uint16_t uint16_t;
typedef __uint32_t uint32_t;
typedef __uint64_t uint64_t;
typedef __int_least8_t int_least8_t;
typedef __int_least16_t int_least16_t;
typedef __int_least32_t int_least32_t;
typedef __int_least64_t int_least64_t;
typedef __uint_least8_t uint_least8_t;
typedef __uint_least16_t uint_least16_t;
typedef __uint_least32_t uint_least32_t;
typedef __uint_least64_t uint_least64_t;
typedef signed char int_fast8_t;
typedef long int_fast16_t;
typedef long int_fast32_t;
typedef long int_fast64_t;
typedef unsigned char uint_fast8_t;
typedef unsigned long uint_fast16_t;
typedef unsigned long uint_fast32_t;
typedef unsigned long uint_fast64_t;
typedef long intptr_t;
typedef unsigned long uintptr_t;
typedef __intmax_t intmax_t;
typedef __uintmax_t uintmax_t;
typedef _Bool bool;
extern uint64_t revng_init_local_sp(uint64_t param_0);
extern uint64_t init_rbp(void);
extern uint64_t indirect_placeholder_3(uint64_t param_0, uint64_t param_1);
extern void indirect_placeholder(void);
extern void indirect_placeholder_12(uint64_t param_0);
uint64_t bb_mbs_logical_cspn(uint64_t rdi, uint64_t rsi) {
    uint64_t var_0;
    uint64_t var_1;
    uint64_t *var_2;
    uint64_t var_3;
    uint64_t *var_4;
    uint64_t *var_5;
    unsigned char var_6;
    uint64_t rax_0;
    uint64_t local_sp_4;
    uint64_t local_sp_5;
    uint64_t local_sp_3;
    uint64_t local_sp_262;
    uint64_t var_27;
    uint64_t var_28;
    uint64_t var_25;
    unsigned char var_26;
    uint64_t var_29;
    uint64_t var_30;
    uint64_t var_31;
    uint64_t var_32;
    uint64_t var_24;
    uint64_t var_9;
    uint64_t *var_10;
    uint64_t var_11;
    uint64_t var_12;
    unsigned char *var_13;
    unsigned char *var_14;
    uint32_t *var_15;
    uint64_t *var_16;
    uint64_t *var_17;
    uint64_t var_18;
    unsigned char *var_19;
    unsigned char *var_20;
    unsigned char *var_21;
    uint32_t *var_22;
    uint64_t *var_23;
    bool var_7;
    uint64_t var_8;
    unsigned int loop_state_var;
    bool switch_state_var;
    var_0 = revng_init_local_sp(0UL);
    var_1 = init_rbp();
    *(uint64_t *)(var_0 + (-8L)) = var_1;
    var_2 = (uint64_t *)(var_0 + (-160L));
    *var_2 = rdi;
    var_3 = var_0 + (-168L);
    var_4 = (uint64_t *)var_3;
    *var_4 = rsi;
    var_5 = (uint64_t *)(var_0 + (-16L));
    *var_5 = 0UL;
    var_6 = **(unsigned char **)var_3;
    rax_0 = 0UL;
    if (var_6 == '\x00') {
        return rax_0;
    }
    *(uint64_t *)(var_0 + (-176L)) = 4203874UL;
    indirect_placeholder();
    var_7 = (var_6 == '\x01');
    var_8 = *var_2;
    if (!var_7) {
        var_9 = var_0 + (-136L);
        var_10 = (uint64_t *)var_9;
        *var_10 = var_8;
        var_11 = var_0 + (-152L);
        *(unsigned char *)var_11 = (unsigned char)'\x00';
        var_12 = var_0 + (-184L);
        *(uint64_t *)var_12 = 4203931UL;
        indirect_placeholder();
        var_13 = (unsigned char *)(var_0 + (-140L));
        *var_13 = (unsigned char)'\x00';
        var_14 = (unsigned char *)(var_0 + (-120L));
        var_15 = (uint32_t *)(var_0 + (-116L));
        var_16 = (uint64_t *)(var_0 + (-128L));
        var_17 = (uint64_t *)(var_0 + (-72L));
        var_18 = var_0 + (-88L);
        var_19 = (unsigned char *)var_18;
        var_20 = (unsigned char *)(var_0 + (-76L));
        var_21 = (unsigned char *)(var_0 + (-56L));
        var_22 = (uint32_t *)(var_0 + (-52L));
        var_23 = (uint64_t *)(var_0 + (-64L));
        local_sp_5 = var_12;
        while (1U)
            {
                *(uint64_t *)(local_sp_5 + (-8L)) = 4204253UL;
                indirect_placeholder_12(var_11);
                if (*var_14 != '\x01') {
                    if (*var_15 == 0U) {
                        break;
                    }
                }
                *var_5 = (*var_5 + 1UL);
                if (*var_16 == 1UL) {
                    var_29 = (uint64_t)(uint32_t)(uint64_t)**(unsigned char **)var_9;
                    var_30 = *var_4;
                    var_31 = local_sp_5 + (-16L);
                    *(uint64_t *)var_31 = 4203985UL;
                    var_32 = indirect_placeholder_3(var_30, var_29);
                    local_sp_4 = var_31;
                    if (var_32 != 0UL) {
                        rax_0 = *var_5;
                        break;
                    }
                }
                *var_17 = *var_4;
                *var_19 = (unsigned char)'\x00';
                var_24 = local_sp_5 + (-16L);
                *(uint64_t *)var_24 = 4204044UL;
                indirect_placeholder();
                *var_20 = (unsigned char)'\x00';
                local_sp_3 = var_24;
                while (1U)
                    {
                        var_25 = local_sp_3 + (-8L);
                        *(uint64_t *)var_25 = 4204178UL;
                        indirect_placeholder_12(var_18);
                        var_26 = *var_21;
                        local_sp_262 = var_25;
                        local_sp_4 = var_25;
                        if (var_26 != '\x01') {
                            if (*var_22 != 0U) {
                                loop_state_var = 0U;
                                break;
                            }
                        }
                        if (var_26 != '\x00') {
                            if (*var_14 != '\x00') {
                                if ((uint64_t)(*var_22 - *var_15) != 0UL) {
                                    loop_state_var = 1U;
                                    break;
                                }
                                *var_17 = (*var_23 + *var_17);
                                *var_20 = (unsigned char)'\x00';
                                local_sp_3 = local_sp_262;
                                continue;
                            }
                        }
                        if (*var_23 != *var_16) {
                            var_27 = *var_17;
                            var_28 = local_sp_3 + (-16L);
                            *(uint64_t *)var_28 = 4204115UL;
                            indirect_placeholder();
                            local_sp_262 = var_28;
                            if ((uint64_t)(uint32_t)var_27 != 0UL) {
                                loop_state_var = 1U;
                                break;
                            }
                        }
                        *var_17 = (*var_23 + *var_17);
                        *var_20 = (unsigned char)'\x00';
                        local_sp_3 = local_sp_262;
                        continue;
                    }
                switch_state_var = 0;
                switch (loop_state_var) {
                  case 0U:
                    {
                        *var_10 = (*var_16 + *var_10);
                        *var_13 = (unsigned char)'\x00';
                        local_sp_5 = local_sp_4;
                        continue;
                    }
                    break;
                  case 1U:
                    {
                        rax_0 = *var_5;
                        switch_state_var = 1;
                        break;
                    }
                    break;
                }
                if (switch_state_var)
                    break;
            }
    }
    *(uint64_t *)(var_0 + (-184L)) = 4204323UL;
    indirect_placeholder();
    *(uint64_t *)(var_0 + (-24L)) = var_8;
    rax_0 = (*(unsigned char *)(var_8 + *var_2) == '\x00') ? 0UL : (var_8 + 1UL);
    return rax_0;
}

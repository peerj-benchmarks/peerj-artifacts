typedef unsigned char __u_char;
typedef unsigned short __u_short;
typedef unsigned int __u_int;
typedef unsigned long __u_long;
typedef signed char __int8_t;
typedef unsigned char __uint8_t;
typedef short __int16_t;
typedef unsigned short __uint16_t;
typedef int __int32_t;
typedef unsigned int __uint32_t;
typedef long __int64_t;
typedef unsigned long __uint64_t;
typedef __int8_t __int_least8_t;
typedef __uint8_t __uint_least8_t;
typedef __int16_t __int_least16_t;
typedef __uint16_t __uint_least16_t;
typedef __int32_t __int_least32_t;
typedef __uint32_t __uint_least32_t;
typedef __int64_t __int_least64_t;
typedef __uint64_t __uint_least64_t;
typedef long __quad_t;
typedef unsigned long __u_quad_t;
typedef long __intmax_t;
typedef unsigned long __uintmax_t;
typedef unsigned long __dev_t;
typedef unsigned int __uid_t;
typedef unsigned int __gid_t;
typedef unsigned long __ino_t;
typedef unsigned long __ino64_t;
typedef unsigned int __mode_t;
typedef unsigned long __nlink_t;
typedef long __off_t;
typedef long __off64_t;
typedef int __pid_t;
typedef struct {
    int __val[2];
} __fsid_t;
typedef long __clock_t;
typedef unsigned long __rlim_t;
typedef unsigned long __rlim64_t;
typedef unsigned int __id_t;
typedef long __time_t;
typedef unsigned int __useconds_t;
typedef long __suseconds_t;
typedef int __daddr_t;
typedef int __key_t;
typedef int __clockid_t;
typedef void *__timer_t;
typedef long __blksize_t;
typedef long __blkcnt_t;
typedef long __blkcnt64_t;
typedef unsigned long __fsblkcnt_t;
typedef unsigned long __fsblkcnt64_t;
typedef unsigned long __fsfilcnt_t;
typedef unsigned long __fsfilcnt64_t;
typedef long __fsword_t;
typedef long __ssize_t;
typedef long __syscall_slong_t;
typedef unsigned long __syscall_ulong_t;
typedef __off64_t __loff_t;
typedef char *__caddr_t;
typedef long __intptr_t;
typedef unsigned int __socklen_t;
typedef int __sig_atomic_t;
typedef __int8_t int8_t;
typedef __int16_t int16_t;
typedef __int32_t int32_t;
typedef __int64_t int64_t;
typedef __uint8_t uint8_t;
typedef __uint16_t uint16_t;
typedef __uint32_t uint32_t;
typedef __uint64_t uint64_t;
typedef __int_least8_t int_least8_t;
typedef __int_least16_t int_least16_t;
typedef __int_least32_t int_least32_t;
typedef __int_least64_t int_least64_t;
typedef __uint_least8_t uint_least8_t;
typedef __uint_least16_t uint_least16_t;
typedef __uint_least32_t uint_least32_t;
typedef __uint_least64_t uint_least64_t;
typedef signed char int_fast8_t;
typedef long int_fast16_t;
typedef long int_fast32_t;
typedef long int_fast64_t;
typedef unsigned char uint_fast8_t;
typedef unsigned long uint_fast16_t;
typedef unsigned long uint_fast32_t;
typedef unsigned long uint_fast64_t;
typedef long intptr_t;
typedef unsigned long uintptr_t;
typedef __intmax_t intmax_t;
typedef __uintmax_t uintmax_t;
struct bb_print_esc_ret_type;
struct indirect_placeholder_19_ret_type;
struct indirect_placeholder_20_ret_type;
struct indirect_placeholder_21_ret_type;
struct bb_print_esc_ret_type {
    uint64_t field_0;
    uint64_t field_1;
    uint64_t field_2;
    uint64_t field_3;
};
struct indirect_placeholder_19_ret_type {
    uint64_t field_0;
    uint64_t field_1;
};
struct indirect_placeholder_20_ret_type {
    uint64_t field_0;
    uint64_t field_1;
};
struct indirect_placeholder_21_ret_type {
    uint64_t field_0;
    uint64_t field_1;
};
extern uint64_t revng_init_local_sp(uint64_t param_0);
extern uint64_t init_rbp(void);
extern void indirect_placeholder_2(void);
extern uint64_t indirect_placeholder_9(uint64_t param_0);
extern void indirect_placeholder_12(uint64_t param_0);
extern void indirect_placeholder_10(uint64_t param_0, uint64_t param_1, uint64_t param_2);
extern struct indirect_placeholder_19_ret_type indirect_placeholder_19(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint64_t param_3, uint64_t param_4, uint64_t param_5, uint64_t param_6);
extern struct indirect_placeholder_20_ret_type indirect_placeholder_20(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint64_t param_3, uint64_t param_4, uint64_t param_5, uint64_t param_6);
extern struct indirect_placeholder_21_ret_type indirect_placeholder_21(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint64_t param_3, uint64_t param_4, uint64_t param_5, uint64_t param_6);
struct bb_print_esc_ret_type bb_print_esc(uint64_t rcx, uint64_t rdi, uint64_t r8, uint64_t rsi, uint64_t r9) {
    struct indirect_placeholder_20_ret_type var_48;
    uint64_t var_0;
    uint64_t var_1;
    uint64_t var_2;
    uint64_t *var_3;
    unsigned char *var_4;
    uint64_t var_5;
    uint64_t var_6;
    uint64_t *var_7;
    uint32_t *var_8;
    unsigned char **var_9;
    unsigned char var_10;
    uint64_t r86_3;
    uint64_t var_34;
    uint64_t r98_3;
    struct indirect_placeholder_19_ret_type var_35;
    uint64_t local_sp_0;
    uint64_t r86_0;
    uint64_t r98_0;
    uint32_t var_36;
    unsigned char var_37;
    uint32_t rax_0_in;
    uint64_t local_sp_5;
    uint64_t var_15;
    uint64_t var_16;
    uint64_t local_sp_3;
    uint32_t var_27;
    uint64_t rcx4_0;
    uint64_t r98_2;
    uint64_t r98_4;
    uint64_t r86_1;
    uint64_t r98_1;
    struct bb_print_esc_ret_type mrv;
    struct bb_print_esc_ret_type mrv1;
    struct bb_print_esc_ret_type mrv2;
    struct bb_print_esc_ret_type mrv3;
    uint32_t var_44;
    unsigned char var_45;
    uint32_t rax_1_in;
    uint64_t local_sp_1;
    uint32_t *var_38;
    uint32_t var_39;
    uint64_t var_40;
    uint64_t var_41;
    uint64_t var_42;
    uint64_t var_43;
    uint64_t local_sp_2;
    uint32_t var_46;
    uint64_t var_47;
    uint64_t r86_2;
    uint32_t *var_11;
    uint64_t storemerge31;
    unsigned char var_12;
    unsigned char var_17;
    uint64_t var_13;
    uint64_t var_14;
    uint64_t local_sp_4;
    unsigned char *var_18;
    uint32_t *var_19;
    uint32_t spec_select;
    uint32_t *var_20;
    uint32_t var_21;
    unsigned char var_22;
    uint64_t spec_select166;
    uint64_t var_23;
    uint64_t var_24;
    uint64_t var_25;
    struct indirect_placeholder_21_ret_type var_26;
    uint64_t local_sp_6;
    uint64_t r86_4;
    uint64_t var_28;
    uint64_t var_29;
    uint64_t var_30;
    uint64_t var_31;
    uint64_t var_32;
    uint64_t var_33;
    var_0 = revng_init_local_sp(0UL);
    var_1 = init_rbp();
    *(uint64_t *)(var_0 + (-8L)) = var_1;
    var_2 = var_0 + (-56L);
    var_3 = (uint64_t *)(var_0 + (-48L));
    *var_3 = rdi;
    var_4 = (unsigned char *)(var_0 + (-52L));
    *var_4 = (unsigned char)rsi;
    var_5 = *var_3 + 1UL;
    var_6 = var_0 + (-16L);
    var_7 = (uint64_t *)var_6;
    *var_7 = var_5;
    var_8 = (uint32_t *)(var_0 + (-20L));
    *var_8 = 0U;
    var_9 = (unsigned char **)var_6;
    var_10 = **var_9;
    r86_3 = r8;
    r98_3 = r9;
    rcx4_0 = rcx;
    r98_2 = r9;
    r86_1 = r8;
    r98_1 = r9;
    local_sp_1 = var_2;
    r86_2 = r8;
    storemerge31 = 0UL;
    var_17 = (unsigned char)'\x00';
    local_sp_4 = var_2;
    if (var_10 == 'x') {
        var_38 = (uint32_t *)(var_0 + (-24L));
        *var_38 = 0U;
        *var_7 = (*var_7 + 1UL);
        while (1U)
            {
                var_39 = *var_38;
                var_46 = var_39;
                local_sp_2 = local_sp_1;
                if ((int)var_39 <= (int)1U) {
                    break;
                }
                var_40 = (uint64_t)(uint32_t)(uint64_t)**var_9;
                *(uint64_t *)(local_sp_1 + (-8L)) = 4203357UL;
                var_41 = indirect_placeholder_9(var_40);
                var_42 = (uint64_t)(unsigned char)var_41;
                var_43 = local_sp_1 + (-16L);
                *(uint64_t *)var_43 = 4203367UL;
                indirect_placeholder_2();
                local_sp_1 = var_43;
                local_sp_2 = var_43;
                if (var_42 != 0UL) {
                    var_46 = *var_38;
                    break;
                }
                var_44 = *var_8 << 4U;
                var_45 = **var_9;
                if (((signed char)var_45 <= '`') || ((signed char)var_45 > 'f')) {
                    rax_1_in = (uint32_t)var_45 + (-87);
                } else {
                    if (((signed char)var_45 <= '@') || ((signed char)var_45 > 'F')) {
                        rax_1_in = (uint32_t)var_45 + (-48);
                    } else {
                        rax_1_in = (uint32_t)var_45 + (-55);
                    }
                }
                *var_8 = (rax_1_in + var_44);
                *var_38 = (*var_38 + 1U);
                *var_7 = (*var_7 + 1UL);
                continue;
            }
        local_sp_3 = local_sp_2;
        if (var_46 == 0U) {
            var_47 = local_sp_2 + (-8L);
            *(uint64_t *)var_47 = 4203406UL;
            var_48 = indirect_placeholder_20(0UL, 4269560UL, rcx, 1UL, r8, 0UL, r9);
            local_sp_3 = var_47;
            r86_2 = var_48.field_0;
            r98_2 = var_48.field_1;
        }
        *(uint64_t *)(local_sp_3 + (-8L)) = 4203416UL;
        indirect_placeholder_2();
        r86_1 = r86_2;
        r98_1 = r98_2;
    } else {
        if (((signed char)var_10 <= '/') || ((signed char)var_10 > '7')) {
            if (var_10 != '\x00') {
                var_13 = (uint64_t)(uint32_t)(uint64_t)var_10;
                var_14 = var_0 + (-64L);
                *(uint64_t *)var_14 = 4203604UL;
                indirect_placeholder_2();
                local_sp_4 = var_14;
                if (var_13 != 0UL) {
                    var_15 = *var_7;
                    *var_7 = (var_15 + 1UL);
                    var_16 = (uint64_t)(uint32_t)(uint64_t)*(unsigned char *)var_15;
                    *(uint64_t *)(var_0 + (-72L)) = 4203634UL;
                    indirect_placeholder_12(var_16);
                    mrv.field_0 = (uint64_t)(((uint32_t)*var_7 - (uint32_t)*var_3) + (-1));
                    mrv1 = mrv;
                    mrv1.field_1 = rcx4_0;
                    mrv2 = mrv1;
                    mrv2.field_2 = r86_1;
                    mrv3 = mrv2;
                    mrv3.field_3 = r98_1;
                    return mrv3;
                }
                var_17 = **var_9;
            }
            var_18 = (unsigned char *)(var_0 + (-29L));
            *var_18 = var_17;
            var_19 = (uint32_t *)(var_0 + (-28L));
            *var_19 = 0U;
            spec_select = (*var_18 == 'u') ? 4U : 8U;
            var_20 = (uint32_t *)(var_0 + (-24L));
            *var_20 = spec_select;
            *var_7 = (*var_7 + 1UL);
            r86_0 = r86_3;
            r98_0 = r98_3;
            r98_4 = r98_3;
            local_sp_6 = local_sp_5;
            r86_4 = r86_3;
            while ((int)*var_20 <= (int)0U)
                {
                    var_30 = (uint64_t)(uint32_t)(uint64_t)**var_9;
                    *(uint64_t *)(local_sp_5 + (-8L)) = 4203730UL;
                    var_31 = indirect_placeholder_9(var_30);
                    var_32 = (uint64_t)(unsigned char)var_31;
                    var_33 = local_sp_5 + (-16L);
                    *(uint64_t *)var_33 = 4203740UL;
                    indirect_placeholder_2();
                    local_sp_0 = var_33;
                    if (var_32 == 0UL) {
                        var_34 = local_sp_5 + (-24L);
                        *(uint64_t *)var_34 = 4203769UL;
                        var_35 = indirect_placeholder_19(0UL, 4269560UL, rcx, 1UL, r86_3, 0UL, r98_3);
                        local_sp_0 = var_34;
                        r86_0 = var_35.field_0;
                        r98_0 = var_35.field_1;
                    }
                    var_36 = *var_19 << 4U;
                    var_37 = **var_9;
                    local_sp_5 = local_sp_0;
                    r86_3 = r86_0;
                    r98_3 = r98_0;
                    if (((signed char)var_37 <= '`') || ((signed char)var_37 > 'f')) {
                        rax_0_in = (uint32_t)var_37 + (-87);
                    } else {
                        if (((signed char)var_37 <= '@') || ((signed char)var_37 > 'F')) {
                            rax_0_in = (uint32_t)var_37 + (-48);
                        } else {
                            rax_0_in = (uint32_t)var_37 + (-55);
                        }
                    }
                    *var_19 = (rax_0_in + var_36);
                    *var_20 = (*var_20 + (-1));
                    *var_7 = (*var_7 + 1UL);
                    r86_0 = r86_3;
                    r98_0 = r98_3;
                    r98_4 = r98_3;
                    local_sp_6 = local_sp_5;
                    r86_4 = r86_3;
                }
            var_21 = *var_19;
            var_27 = var_21;
            if (var_21 > 159U) {
                if ((var_21 <= 55295U) || (var_21 > 57343U)) {
                    var_22 = *var_18;
                    spec_select166 = (var_22 == 'u') ? 4UL : 8UL;
                    var_23 = (uint64_t)(uint32_t)(uint64_t)var_22;
                    var_24 = (uint64_t)var_21;
                    var_25 = local_sp_5 + (-8L);
                    *(uint64_t *)var_25 = 4203991UL;
                    var_26 = indirect_placeholder_21(0UL, 4269616UL, var_23, 1UL, spec_select166, 0UL, var_24);
                    var_27 = *var_19;
                    local_sp_6 = var_25;
                    r86_4 = var_26.field_0;
                    r98_4 = var_26.field_1;
                }
            } else {
                if ((var_21 <= 55295U) || (var_21 > 57343U)) {
                    var_22 = *var_18;
                    spec_select166 = (var_22 == 'u') ? 4UL : 8UL;
                    var_23 = (uint64_t)(uint32_t)(uint64_t)var_22;
                    var_24 = (uint64_t)var_21;
                    var_25 = local_sp_5 + (-8L);
                    *(uint64_t *)var_25 = 4203991UL;
                    var_26 = indirect_placeholder_21(0UL, 4269616UL, var_23, 1UL, spec_select166, 0UL, var_24);
                    var_27 = *var_19;
                    local_sp_6 = var_25;
                    r86_4 = var_26.field_0;
                    r98_4 = var_26.field_1;
                }
            }
            var_28 = *(uint64_t *)6379008UL;
            var_29 = (uint64_t)var_27;
            *(uint64_t *)(local_sp_6 + (-8L)) = 4204016UL;
            indirect_placeholder_10(0UL, var_28, var_29);
            rcx4_0 = var_29;
            r86_1 = r86_4;
            r98_1 = r98_4;
        } else {
            var_11 = (uint32_t *)(var_0 + (-24L));
            *var_11 = 0U;
            if (*var_4 == '\x00') {
            } else {
                storemerge31 = 1UL;
                if (**var_9 == '0') {
                }
            }
            *var_7 = (*var_7 + storemerge31);
            while ((int)*var_11 <= (int)2U)
                {
                    var_12 = **var_9;
                    if (((signed char)var_12 <= '/') || ((signed char)var_12 > '7')) {
                        break;
                    }
                    *var_8 = (((uint32_t)var_12 + (-48)) + (*var_8 << 3U));
                    *var_11 = (*var_11 + 1U);
                    *var_7 = (*var_7 + 1UL);
                }
            *(uint64_t *)(var_0 + (-64L)) = 4203566UL;
            indirect_placeholder_2();
        }
    }
    mrv.field_0 = (uint64_t)(((uint32_t)*var_7 - (uint32_t)*var_3) + (-1));
    mrv1 = mrv;
    mrv1.field_1 = rcx4_0;
    mrv2 = mrv1;
    mrv2.field_2 = r86_1;
    mrv3 = mrv2;
    mrv3.field_3 = r98_1;
    return mrv3;
}

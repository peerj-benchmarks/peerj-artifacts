typedef unsigned char __u_char;
typedef unsigned short __u_short;
typedef unsigned int __u_int;
typedef unsigned long __u_long;
typedef signed char __int8_t;
typedef unsigned char __uint8_t;
typedef short __int16_t;
typedef unsigned short __uint16_t;
typedef int __int32_t;
typedef unsigned int __uint32_t;
typedef long __int64_t;
typedef unsigned long __uint64_t;
typedef __int8_t __int_least8_t;
typedef __uint8_t __uint_least8_t;
typedef __int16_t __int_least16_t;
typedef __uint16_t __uint_least16_t;
typedef __int32_t __int_least32_t;
typedef __uint32_t __uint_least32_t;
typedef __int64_t __int_least64_t;
typedef __uint64_t __uint_least64_t;
typedef long __quad_t;
typedef unsigned long __u_quad_t;
typedef long __intmax_t;
typedef unsigned long __uintmax_t;
typedef unsigned long __dev_t;
typedef unsigned int __uid_t;
typedef unsigned int __gid_t;
typedef unsigned long __ino_t;
typedef unsigned long __ino64_t;
typedef unsigned int __mode_t;
typedef unsigned long __nlink_t;
typedef long __off_t;
typedef long __off64_t;
typedef int __pid_t;
typedef struct {
    int __val[2];
} __fsid_t;
typedef long __clock_t;
typedef unsigned long __rlim_t;
typedef unsigned long __rlim64_t;
typedef unsigned int __id_t;
typedef long __time_t;
typedef unsigned int __useconds_t;
typedef long __suseconds_t;
typedef int __daddr_t;
typedef int __key_t;
typedef int __clockid_t;
typedef void *__timer_t;
typedef long __blksize_t;
typedef long __blkcnt_t;
typedef long __blkcnt64_t;
typedef unsigned long __fsblkcnt_t;
typedef unsigned long __fsblkcnt64_t;
typedef unsigned long __fsfilcnt_t;
typedef unsigned long __fsfilcnt64_t;
typedef long __fsword_t;
typedef long __ssize_t;
typedef long __syscall_slong_t;
typedef unsigned long __syscall_ulong_t;
typedef __off64_t __loff_t;
typedef char *__caddr_t;
typedef long __intptr_t;
typedef unsigned int __socklen_t;
typedef int __sig_atomic_t;
typedef __int8_t int8_t;
typedef __int16_t int16_t;
typedef __int32_t int32_t;
typedef __int64_t int64_t;
typedef __uint8_t uint8_t;
typedef __uint16_t uint16_t;
typedef __uint32_t uint32_t;
typedef __uint64_t uint64_t;
typedef __int_least8_t int_least8_t;
typedef __int_least16_t int_least16_t;
typedef __int_least32_t int_least32_t;
typedef __int_least64_t int_least64_t;
typedef __uint_least8_t uint_least8_t;
typedef __uint_least16_t uint_least16_t;
typedef __uint_least32_t uint_least32_t;
typedef __uint_least64_t uint_least64_t;
typedef signed char int_fast8_t;
typedef long int_fast16_t;
typedef long int_fast32_t;
typedef long int_fast64_t;
typedef unsigned char uint_fast8_t;
typedef unsigned long uint_fast16_t;
typedef unsigned long uint_fast32_t;
typedef unsigned long uint_fast64_t;
typedef long intptr_t;
typedef unsigned long uintptr_t;
typedef __intmax_t intmax_t;
typedef __uintmax_t uintmax_t;
typedef _Bool bool;
struct indirect_placeholder_11_ret_type;
struct indirect_placeholder_13_ret_type;
struct indirect_placeholder_12_ret_type;
struct indirect_placeholder_14_ret_type;
struct indirect_placeholder_15_ret_type;
struct indirect_placeholder_10_ret_type;
struct indirect_placeholder_16_ret_type;
struct indirect_placeholder_18_ret_type;
struct indirect_placeholder_20_ret_type;
struct indirect_placeholder_11_ret_type {
    uint64_t field_0;
    uint64_t field_1;
    uint64_t field_2;
};
struct indirect_placeholder_13_ret_type {
    uint64_t field_0;
    uint64_t field_1;
};
struct indirect_placeholder_12_ret_type {
    uint64_t field_0;
    uint64_t field_1;
    uint64_t field_2;
};
struct indirect_placeholder_14_ret_type {
    uint64_t field_0;
    uint64_t field_1;
    uint64_t field_2;
};
struct indirect_placeholder_15_ret_type {
    uint64_t field_0;
    uint64_t field_1;
    uint64_t field_2;
};
struct indirect_placeholder_10_ret_type {
    uint64_t field_0;
    uint64_t field_1;
    uint64_t field_2;
};
struct indirect_placeholder_16_ret_type {
    uint64_t field_0;
    uint64_t field_1;
    uint64_t field_2;
};
struct indirect_placeholder_18_ret_type {
    uint64_t field_0;
    uint64_t field_1;
    uint64_t field_2;
    uint64_t field_3;
};
struct indirect_placeholder_20_ret_type {
    uint64_t field_0;
    uint64_t field_1;
    uint64_t field_2;
    uint64_t field_3;
};
extern void abort(void);
extern uint64_t revng_init_local_sp(uint64_t param_0);
extern uint64_t init_rbp(void);
extern uint64_t indirect_placeholder_2(uint64_t param_0, uint64_t param_1);
extern void indirect_placeholder_1(void);
extern void indirect_placeholder_8(uint64_t param_0, uint64_t param_1);
extern uint64_t indirect_placeholder_17(void);
extern uint64_t init_rbx(void);
extern uint64_t indirect_placeholder_3(uint64_t param_0);
extern void indirect_placeholder_9(uint64_t param_0);
extern struct indirect_placeholder_11_ret_type indirect_placeholder_11(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint64_t param_3, uint64_t param_4, uint64_t param_5, uint64_t param_6);
extern struct indirect_placeholder_13_ret_type indirect_placeholder_13(uint64_t param_0);
extern struct indirect_placeholder_12_ret_type indirect_placeholder_12(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint64_t param_3, uint64_t param_4, uint64_t param_5, uint64_t param_6);
extern struct indirect_placeholder_14_ret_type indirect_placeholder_14(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint64_t param_3, uint64_t param_4, uint64_t param_5, uint64_t param_6);
extern struct indirect_placeholder_15_ret_type indirect_placeholder_15(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint64_t param_3, uint64_t param_4, uint64_t param_5, uint64_t param_6);
extern struct indirect_placeholder_10_ret_type indirect_placeholder_10(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint64_t param_3, uint64_t param_4, uint64_t param_5, uint64_t param_6);
extern struct indirect_placeholder_16_ret_type indirect_placeholder_16(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint64_t param_3, uint64_t param_4, uint64_t param_5, uint64_t param_6);
extern struct indirect_placeholder_18_ret_type indirect_placeholder_18(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint64_t param_3, uint64_t param_4);
extern void indirect_placeholder_19(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint64_t param_3, uint64_t param_4, uint64_t param_5);
extern struct indirect_placeholder_20_ret_type indirect_placeholder_20(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint64_t param_3, uint64_t param_4);
void bb_main(uint64_t rsi, uint64_t rdi) {
    struct indirect_placeholder_20_ret_type var_16;
    struct indirect_placeholder_11_ret_type var_75;
    struct indirect_placeholder_18_ret_type var_27;
    uint64_t var_72;
    uint64_t var_73;
    uint64_t var_74;
    uint64_t r9_1;
    uint64_t local_sp_5;
    uint64_t rcx_1;
    uint64_t var_52;
    uint64_t var_53;
    struct indirect_placeholder_13_ret_type var_54;
    uint64_t local_sp_0;
    uint64_t var_55;
    uint64_t var_56;
    uint64_t var_57;
    uint64_t var_58;
    struct indirect_placeholder_12_ret_type var_59;
    uint64_t rcx_0;
    uint64_t var_43;
    uint64_t *var_44;
    uint64_t r9_0;
    uint32_t var_60;
    uint32_t var_45;
    uint32_t var_42;
    uint32_t _pre;
    uint64_t var_38;
    uint32_t var_39;
    uint64_t var_40;
    uint64_t var_41;
    uint64_t local_sp_1;
    uint64_t local_sp_2;
    uint64_t r8_1;
    uint64_t var_46;
    uint64_t var_47;
    uint64_t var_48;
    uint64_t var_49;
    uint64_t var_50;
    uint64_t var_51;
    uint64_t _pre_phi213;
    uint64_t _pre_phi211;
    uint64_t local_sp_3;
    bool var_61;
    unsigned char *var_62;
    unsigned char var_63;
    uint64_t var_64;
    bool var_65;
    uint64_t var_66;
    uint64_t var_67;
    uint64_t *var_68;
    uint64_t local_sp_4;
    uint64_t r8_0;
    uint64_t var_0;
    uint64_t var_1;
    uint64_t var_2;
    uint64_t var_3;
    uint32_t *var_4;
    uint64_t var_5;
    uint64_t *var_6;
    unsigned char *var_7;
    unsigned char *var_8;
    uint64_t *var_9;
    uint64_t var_10;
    uint64_t var_11;
    uint32_t *var_12;
    uint64_t local_sp_9;
    uint64_t var_71;
    uint64_t var_69;
    uint64_t var_70;
    uint64_t r8_2;
    uint64_t r9_2;
    uint32_t *var_76;
    uint64_t var_77;
    uint64_t var_78;
    uint64_t var_79;
    uint64_t var_34;
    uint64_t var_35;
    uint64_t var_36;
    uint64_t var_37;
    uint64_t local_sp_7;
    uint64_t local_sp_7_be;
    uint64_t var_32;
    uint64_t var_33;
    uint64_t local_sp_6;
    uint32_t var_21;
    uint64_t var_22;
    uint64_t var_23;
    uint64_t var_24;
    uint64_t var_25;
    uint64_t var_26;
    uint64_t var_28;
    uint64_t var_29;
    uint64_t var_30;
    uint32_t var_31;
    uint64_t local_sp_8;
    uint64_t var_18;
    uint64_t var_19;
    uint64_t *var_20;
    uint64_t var_13;
    uint64_t var_14;
    uint64_t var_15;
    uint32_t var_17;
    unsigned int loop_state_var;
    bool switch_state_var;
    var_0 = revng_init_local_sp(0UL);
    var_1 = init_rbp();
    var_2 = init_rbx();
    var_3 = var_0 + (-8L);
    *(uint64_t *)var_3 = var_1;
    *(uint64_t *)(var_0 + (-16L)) = var_2;
    var_4 = (uint32_t *)(var_0 + (-76L));
    *var_4 = (uint32_t)rdi;
    var_5 = var_0 + (-88L);
    var_6 = (uint64_t *)var_5;
    *var_6 = rsi;
    var_7 = (unsigned char *)(var_0 + (-25L));
    *var_7 = (unsigned char)'\x00';
    var_8 = (unsigned char *)(var_0 + (-26L));
    *var_8 = (unsigned char)'\x00';
    var_9 = (uint64_t *)(var_0 + (-40L));
    *var_9 = 0UL;
    var_10 = **(uint64_t **)var_5;
    *(uint64_t *)(var_0 + (-96L)) = 4201899UL;
    indirect_placeholder_9(var_10);
    *(uint64_t *)(var_0 + (-104L)) = 4201914UL;
    indirect_placeholder_1();
    *(uint64_t *)(var_0 + (-112L)) = 4201924UL;
    indirect_placeholder_9(125UL);
    var_11 = var_0 + (-120L);
    *(uint64_t *)var_11 = 4201934UL;
    indirect_placeholder_1();
    var_12 = (uint32_t *)(var_0 + (-52L));
    local_sp_9 = var_11;
    while (1U)
        {
            var_13 = *var_6;
            var_14 = (uint64_t)*var_4;
            var_15 = local_sp_9 + (-8L);
            *(uint64_t *)var_15 = 4202132UL;
            var_16 = indirect_placeholder_20(4268053UL, 4267200UL, 0UL, var_13, var_14);
            var_17 = (uint32_t)var_16.field_0;
            *var_12 = var_17;
            local_sp_6 = var_15;
            local_sp_8 = var_15;
            local_sp_9 = var_15;
            if (var_17 != 4294967295U) {
                var_21 = *(uint32_t *)6375960UL;
                if ((long)((uint64_t)var_21 << 32UL) >= (long)((uint64_t)*var_4 << 32UL)) {
                    loop_state_var = 1U;
                    break;
                }
                var_22 = *(uint64_t *)(*var_6 + ((uint64_t)var_21 << 3UL));
                var_23 = local_sp_9 + (-16L);
                *(uint64_t *)var_23 = 4202195UL;
                indirect_placeholder_1();
                local_sp_6 = var_23;
                if ((uint64_t)(uint32_t)var_22 != 0UL) {
                    loop_state_var = 1U;
                    break;
                }
                *var_7 = (unsigned char)'\x01';
                loop_state_var = 1U;
                break;
            }
            if ((uint64_t)(var_17 + (-48)) == 0UL) {
                *var_8 = (unsigned char)'\x01';
                continue;
            }
            if ((int)var_17 > (int)48U) {
                if ((uint64_t)(var_17 + (-105)) == 0UL) {
                    *var_7 = (unsigned char)'\x01';
                    continue;
                }
                if ((uint64_t)(var_17 + (-117)) != 0UL) {
                    continue;
                }
                if ((uint64_t)(var_17 + (-67)) != 0UL) {
                    loop_state_var = 0U;
                    break;
                }
                *var_9 = *(uint64_t *)6376672UL;
                continue;
            }
            if ((uint64_t)(var_17 + 131U) != 0UL) {
                if ((uint64_t)(var_17 + 130U) != 0UL) {
                    loop_state_var = 0U;
                    break;
                }
                *(uint64_t *)(local_sp_9 + (-16L)) = 4202020UL;
                indirect_placeholder_8(var_3, 0UL);
                abort();
            }
            var_18 = *(uint64_t *)6375808UL;
            var_19 = local_sp_9 + (-24L);
            var_20 = (uint64_t *)var_19;
            *var_20 = 0UL;
            *(uint64_t *)(local_sp_9 + (-32L)) = 4202078UL;
            indirect_placeholder_19(0UL, 4266912UL, var_18, 4268036UL, 4268016UL, 4268020UL);
            *var_20 = 4202092UL;
            indirect_placeholder_1();
            local_sp_8 = var_19;
            loop_state_var = 0U;
            break;
        }
    switch (loop_state_var) {
      case 0U:
        {
            *(uint64_t *)(local_sp_8 + (-8L)) = 4202102UL;
            indirect_placeholder_8(var_3, 125UL);
            abort();
        }
        break;
      case 1U:
        {
            local_sp_7 = local_sp_6;
            if (*var_7 == '\x00') {
                *(uint64_t *)6376064UL = 6376208UL;
            }
            *(uint32_t *)6375960UL = 0U;
            while (1U)
                {
                    switch_state_var = 1;
                    break;
                }
            var_38 = var_27.field_1;
            var_39 = *(uint32_t *)6375960UL;
            _pre = var_39;
            rcx_1 = var_38;
            if ((long)((uint64_t)var_39 << 32UL) >= (long)((uint64_t)*var_4 << 32UL)) {
                var_40 = *(uint64_t *)(*var_6 + ((uint64_t)var_39 << 3UL));
                var_41 = local_sp_7 + (-16L);
                *(uint64_t *)var_41 = 4202396UL;
                indirect_placeholder_1();
                local_sp_1 = var_41;
                if ((uint64_t)(uint32_t)var_40 == 0UL) {
                    var_42 = *(uint32_t *)6375960UL + 1U;
                    *(uint32_t *)6375960UL = var_42;
                    _pre = var_42;
                } else {
                    _pre = *(uint32_t *)6375960UL;
                }
            }
            var_43 = var_0 + (-64L);
            var_44 = (uint64_t *)var_43;
            var_45 = _pre;
            local_sp_2 = local_sp_1;
            while (1U)
                {
                    var_46 = (uint64_t)var_45;
                    var_47 = (uint64_t)*var_4;
                    var_48 = var_46 << 32UL;
                    var_49 = var_47 << 32UL;
                    r9_0 = r9_1;
                    _pre_phi213 = var_49;
                    _pre_phi211 = var_48;
                    local_sp_3 = local_sp_2;
                    r8_0 = r8_1;
                    r8_2 = r8_1;
                    r9_2 = r9_1;
                    if ((long)var_48 >= (long)var_49) {
                        break;
                    }
                    var_50 = *(uint64_t *)(*var_6 + ((uint64_t)var_45 << 3UL));
                    var_51 = local_sp_2 + (-8L);
                    *(uint64_t *)var_51 = 4202599UL;
                    indirect_placeholder_1();
                    *var_44 = var_50;
                    local_sp_3 = var_51;
                    if (var_50 != 0UL) {
                        _pre_phi213 = (uint64_t)*var_4 << 32UL;
                        _pre_phi211 = (uint64_t)*(uint32_t *)6375960UL << 32UL;
                        break;
                    }
                    var_52 = *(uint64_t *)(*var_6 + ((uint64_t)*(uint32_t *)6375960UL << 3UL));
                    var_53 = local_sp_2 + (-16L);
                    *(uint64_t *)var_53 = 4202454UL;
                    var_54 = indirect_placeholder_13(var_52);
                    local_sp_0 = var_53;
                    rcx_0 = var_54.field_1;
                    if ((uint64_t)(uint32_t)var_54.field_0 == 0UL) {
                        **(unsigned char **)var_43 = (unsigned char)'\x00';
                        var_55 = *(uint64_t *)(*var_6 + ((uint64_t)*(uint32_t *)6375960UL << 3UL));
                        *(uint64_t *)(local_sp_2 + (-24L)) = 4202499UL;
                        var_56 = indirect_placeholder_3(var_55);
                        *(uint64_t *)(local_sp_2 + (-32L)) = 4202507UL;
                        indirect_placeholder_1();
                        var_57 = (uint64_t)*(uint32_t *)var_56;
                        var_58 = local_sp_2 + (-40L);
                        *(uint64_t *)var_58 = 4202534UL;
                        var_59 = indirect_placeholder_12(0UL, 4268079UL, var_56, r8_1, var_57, 125UL, r9_1);
                        local_sp_0 = var_58;
                        rcx_0 = var_59.field_0;
                        r8_0 = var_59.field_1;
                        r9_0 = var_59.field_2;
                    }
                    var_60 = *(uint32_t *)6375960UL + 1U;
                    *(uint32_t *)6375960UL = var_60;
                    r9_1 = r9_0;
                    rcx_1 = rcx_0;
                    var_45 = var_60;
                    local_sp_2 = local_sp_0;
                    r8_1 = r8_0;
                    continue;
                }
            var_61 = ((long)_pre_phi211 < (long)_pre_phi213);
            var_62 = (unsigned char *)(var_0 + (-65L));
            var_63 = var_61;
            *var_62 = var_63;
            local_sp_4 = local_sp_3;
            local_sp_5 = local_sp_3;
            if ((*var_8 == '\x00') || (var_63 == '\x00')) {
                *(uint64_t *)(local_sp_3 + (-8L)) = 4202666UL;
                indirect_placeholder_14(0UL, 4268096UL, rcx_1, r8_1, 0UL, 0UL, r9_1);
                *(uint64_t *)(local_sp_3 + (-16L)) = 4202676UL;
                indirect_placeholder_8(var_3, 125UL);
                abort();
            }
            var_64 = *var_9;
            var_65 = (var_64 == 0UL);
            if (!var_65) {
                if (var_61) {
                    *(uint64_t *)(local_sp_3 + (-8L)) = 4202719UL;
                    indirect_placeholder_15(0UL, 4268136UL, rcx_1, r8_1, 0UL, 0UL, r9_1);
                    *(uint64_t *)(local_sp_3 + (-16L)) = 4202729UL;
                    indirect_placeholder_8(var_3, 125UL);
                    abort();
                }
            }
            if (!var_61) {
                var_66 = *(uint64_t *)6376064UL;
                var_67 = var_0 + (-48L);
                var_68 = (uint64_t *)var_67;
                *var_68 = var_66;
                while (**(uint64_t **)var_67 != 0UL)
                    {
                        *var_68 = (*var_68 + 8UL);
                        var_69 = local_sp_4 + (-8L);
                        *(uint64_t *)var_69 = 4202806UL;
                        indirect_placeholder_1();
                        local_sp_4 = var_69;
                    }
            }
            var_70 = local_sp_3 + (-8L);
            *(uint64_t *)var_70 = 4202847UL;
            indirect_placeholder_1();
            local_sp_5 = var_70;
            if (!var_65 && (uint64_t)(uint32_t)var_64 == 0UL) {
                var_71 = *var_9;
                *(uint64_t *)(local_sp_3 + (-16L)) = 4202868UL;
                var_72 = indirect_placeholder_2(var_71, 4UL);
                *(uint64_t *)(local_sp_3 + (-24L)) = 4202876UL;
                indirect_placeholder_1();
                var_73 = (uint64_t)*(uint32_t *)var_72;
                var_74 = local_sp_3 + (-32L);
                *(uint64_t *)var_74 = 4202903UL;
                var_75 = indirect_placeholder_11(0UL, 4268180UL, var_72, r8_1, var_73, 125UL, r9_1);
                local_sp_5 = var_74;
                r8_2 = var_75.field_1;
                r9_2 = var_75.field_2;
            }
            var_76 = *(uint32_t **)(*var_6 + ((uint64_t)*(uint32_t *)6375960UL << 3UL));
            *(uint64_t *)(local_sp_5 + (-8L)) = 4202963UL;
            indirect_placeholder_1();
            *(uint64_t *)(local_sp_5 + (-16L)) = 4202968UL;
            indirect_placeholder_1();
            *(uint32_t *)(var_0 + (-72L)) = ((*var_76 == 2U) ? 127U : 126U);
            var_77 = *(uint64_t *)(*var_6 + ((uint64_t)*(uint32_t *)6375960UL << 3UL));
            *(uint64_t *)(local_sp_5 + (-24L)) = 4203024UL;
            var_78 = indirect_placeholder_3(var_77);
            *(uint64_t *)(local_sp_5 + (-32L)) = 4203032UL;
            indirect_placeholder_1();
            var_79 = (uint64_t)*(uint32_t *)var_78;
            *(uint64_t *)(local_sp_5 + (-40L)) = 4203059UL;
            indirect_placeholder_10(0UL, 4268210UL, var_78, r8_2, var_79, 0UL, r9_2);
            return;
        }
        break;
    }
}

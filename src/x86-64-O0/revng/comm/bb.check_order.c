typedef unsigned char __u_char;
typedef unsigned short __u_short;
typedef unsigned int __u_int;
typedef unsigned long __u_long;
typedef signed char __int8_t;
typedef unsigned char __uint8_t;
typedef short __int16_t;
typedef unsigned short __uint16_t;
typedef int __int32_t;
typedef unsigned int __uint32_t;
typedef long __int64_t;
typedef unsigned long __uint64_t;
typedef __int8_t __int_least8_t;
typedef __uint8_t __uint_least8_t;
typedef __int16_t __int_least16_t;
typedef __uint16_t __uint_least16_t;
typedef __int32_t __int_least32_t;
typedef __uint32_t __uint_least32_t;
typedef __int64_t __int_least64_t;
typedef __uint64_t __uint_least64_t;
typedef long __quad_t;
typedef unsigned long __u_quad_t;
typedef long __intmax_t;
typedef unsigned long __uintmax_t;
typedef unsigned long __dev_t;
typedef unsigned int __uid_t;
typedef unsigned int __gid_t;
typedef unsigned long __ino_t;
typedef unsigned long __ino64_t;
typedef unsigned int __mode_t;
typedef unsigned long __nlink_t;
typedef long __off_t;
typedef long __off64_t;
typedef int __pid_t;
typedef struct {
    int __val[2];
} __fsid_t;
typedef long __clock_t;
typedef unsigned long __rlim_t;
typedef unsigned long __rlim64_t;
typedef unsigned int __id_t;
typedef long __time_t;
typedef unsigned int __useconds_t;
typedef long __suseconds_t;
typedef int __daddr_t;
typedef int __key_t;
typedef int __clockid_t;
typedef void *__timer_t;
typedef long __blksize_t;
typedef long __blkcnt_t;
typedef long __blkcnt64_t;
typedef unsigned long __fsblkcnt_t;
typedef unsigned long __fsblkcnt64_t;
typedef unsigned long __fsfilcnt_t;
typedef unsigned long __fsfilcnt64_t;
typedef long __fsword_t;
typedef long __ssize_t;
typedef long __syscall_slong_t;
typedef unsigned long __syscall_ulong_t;
typedef __off64_t __loff_t;
typedef char *__caddr_t;
typedef long __intptr_t;
typedef unsigned int __socklen_t;
typedef int __sig_atomic_t;
typedef __int8_t int8_t;
typedef __int16_t int16_t;
typedef __int32_t int32_t;
typedef __int64_t int64_t;
typedef __uint8_t uint8_t;
typedef __uint16_t uint16_t;
typedef __uint32_t uint32_t;
typedef __uint64_t uint64_t;
typedef __int_least8_t int_least8_t;
typedef __int_least16_t int_least16_t;
typedef __int_least32_t int_least32_t;
typedef __int_least64_t int_least64_t;
typedef __uint_least8_t uint_least8_t;
typedef __uint_least16_t uint_least16_t;
typedef __uint_least32_t uint_least32_t;
typedef __uint_least64_t uint_least64_t;
typedef signed char int_fast8_t;
typedef long int_fast16_t;
typedef long int_fast32_t;
typedef long int_fast64_t;
typedef unsigned char uint_fast8_t;
typedef unsigned long uint_fast16_t;
typedef unsigned long uint_fast32_t;
typedef unsigned long uint_fast64_t;
typedef long intptr_t;
typedef unsigned long uintptr_t;
typedef __intmax_t intmax_t;
typedef __uintmax_t uintmax_t;
typedef _Bool bool;
struct indirect_placeholder_65_ret_type;
struct indirect_placeholder_66_ret_type;
struct indirect_placeholder_65_ret_type {
    uint64_t field_0;
    uint64_t field_1;
    uint64_t field_2;
};
struct indirect_placeholder_66_ret_type {
    uint64_t field_0;
    uint64_t field_1;
    uint64_t field_2;
};
extern uint64_t revng_init_local_sp(uint64_t param_0);
extern uint64_t init_rbp(void);
extern uint64_t indirect_placeholder_12(uint64_t param_0, uint64_t param_1, uint64_t param_2);
extern uint64_t init_r9(void);
extern uint64_t init_r8(void);
extern struct indirect_placeholder_65_ret_type indirect_placeholder_65(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint64_t param_3, uint64_t param_4, uint64_t param_5, uint64_t param_6);
extern struct indirect_placeholder_66_ret_type indirect_placeholder_66(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint64_t param_3);
void bb_check_order(uint64_t rdx, uint64_t rdi, uint64_t rsi) {
    uint64_t r8_0;
    uint64_t var_0;
    uint64_t var_1;
    uint64_t var_2;
    uint64_t var_3;
    uint64_t *var_4;
    uint64_t *var_5;
    uint32_t *var_6;
    uint32_t var_7;
    uint32_t *_pre_phi32;
    uint64_t local_sp_0;
    uint64_t r9_0;
    uint64_t var_24;
    uint64_t var_25;
    bool var_8;
    uint64_t var_9;
    uint64_t var_10;
    uint64_t var_11;
    uint64_t var_12;
    uint64_t var_13;
    uint64_t var_14;
    uint64_t var_15;
    struct indirect_placeholder_66_ret_type var_16;
    uint64_t var_17;
    uint64_t var_18;
    uint64_t var_19;
    uint32_t *var_20;
    uint64_t var_21;
    uint64_t var_22;
    uint32_t *var_23;
    var_0 = revng_init_local_sp(0UL);
    var_1 = init_rbp();
    var_2 = init_r9();
    var_3 = init_r8();
    *(uint64_t *)(var_0 + (-8L)) = var_1;
    var_4 = (uint64_t *)(var_0 + (-32L));
    *var_4 = rdi;
    var_5 = (uint64_t *)(var_0 + (-40L));
    *var_5 = rsi;
    var_6 = (uint32_t *)(var_0 + (-44L));
    var_7 = (uint32_t)rdx;
    *var_6 = var_7;
    r9_0 = var_2;
    r8_0 = var_3;
    switch (*(uint32_t *)6382936UL) {
      case 2U:
        {
            return;
        }
        break;
      case 1U:
        {
            if (*(unsigned char *)((uint64_t)((long)(((uint64_t)var_7 << 32UL) + (-4294967296L)) >> (long)32UL) + 6382933UL) != '\x01') {
                var_8 = (*(unsigned char *)6382928UL == '\x00');
                var_9 = *var_5;
                var_10 = *(uint64_t *)(var_9 + 8UL) + (-1L);
                var_11 = *(uint64_t *)(var_9 + 16UL);
                var_12 = *var_4;
                var_13 = *(uint64_t *)(var_12 + 8UL) + (-1L);
                if (var_8) {
                    var_21 = var_0 + (-64L);
                    *(uint64_t *)var_21 = 4202539UL;
                    var_22 = indirect_placeholder_12(var_11, var_10, var_13);
                    var_23 = (uint32_t *)(var_0 + (-12L));
                    *var_23 = (uint32_t)var_22;
                    _pre_phi32 = var_23;
                    local_sp_0 = var_21;
                } else {
                    var_14 = *(uint64_t *)(var_12 + 16UL);
                    var_15 = var_0 + (-64L);
                    *(uint64_t *)var_15 = 4202486UL;
                    var_16 = indirect_placeholder_66(var_11, var_10, var_14, var_13);
                    var_17 = var_16.field_0;
                    var_18 = var_16.field_1;
                    var_19 = var_16.field_2;
                    var_20 = (uint32_t *)(var_0 + (-12L));
                    *var_20 = (uint32_t)var_17;
                    _pre_phi32 = var_20;
                    local_sp_0 = var_15;
                    r9_0 = var_18;
                    r8_0 = var_19;
                }
                if ((int)*_pre_phi32 > (int)0U) {
                    var_24 = (*(uint32_t *)6382936UL == 1U);
                    var_25 = (uint64_t)*var_6;
                    *(uint64_t *)(local_sp_0 + (-8L)) = 4202590UL;
                    indirect_placeholder_65(0UL, 4273720UL, var_25, var_24, 0UL, r9_0, r8_0);
                    *(unsigned char *)((uint64_t)((long)(((uint64_t)*var_6 << 32UL) + (-4294967296L)) >> (long)32UL) + 6382933UL) = (unsigned char)'\x01';
                }
            }
        }
        break;
      default:
        {
            if (*(unsigned char *)6382932UL == '\x00') {
                return;
            }
            if (*(unsigned char *)((uint64_t)((long)(((uint64_t)var_7 << 32UL) + (-4294967296L)) >> (long)32UL) + 6382933UL) != '\x01') {
                var_8 = (*(unsigned char *)6382928UL == '\x00');
                var_9 = *var_5;
                var_10 = *(uint64_t *)(var_9 + 8UL) + (-1L);
                var_11 = *(uint64_t *)(var_9 + 16UL);
                var_12 = *var_4;
                var_13 = *(uint64_t *)(var_12 + 8UL) + (-1L);
                if (var_8) {
                    var_14 = *(uint64_t *)(var_12 + 16UL);
                    var_15 = var_0 + (-64L);
                    *(uint64_t *)var_15 = 4202486UL;
                    var_16 = indirect_placeholder_66(var_11, var_10, var_14, var_13);
                    var_17 = var_16.field_0;
                    var_18 = var_16.field_1;
                    var_19 = var_16.field_2;
                    var_20 = (uint32_t *)(var_0 + (-12L));
                    *var_20 = (uint32_t)var_17;
                    _pre_phi32 = var_20;
                    local_sp_0 = var_15;
                    r9_0 = var_18;
                    r8_0 = var_19;
                } else {
                    var_21 = var_0 + (-64L);
                    *(uint64_t *)var_21 = 4202539UL;
                    var_22 = indirect_placeholder_12(var_11, var_10, var_13);
                    var_23 = (uint32_t *)(var_0 + (-12L));
                    *var_23 = (uint32_t)var_22;
                    _pre_phi32 = var_23;
                    local_sp_0 = var_21;
                }
                if ((int)*_pre_phi32 > (int)0U) {
                    var_24 = (*(uint32_t *)6382936UL == 1U);
                    var_25 = (uint64_t)*var_6;
                    *(uint64_t *)(local_sp_0 + (-8L)) = 4202590UL;
                    indirect_placeholder_65(0UL, 4273720UL, var_25, var_24, 0UL, r9_0, r8_0);
                    *(unsigned char *)((uint64_t)((long)(((uint64_t)*var_6 << 32UL) + (-4294967296L)) >> (long)32UL) + 6382933UL) = (unsigned char)'\x01';
                }
            }
        }
        break;
    }
}

typedef unsigned char __u_char;
typedef unsigned short __u_short;
typedef unsigned int __u_int;
typedef unsigned long __u_long;
typedef signed char __int8_t;
typedef unsigned char __uint8_t;
typedef short __int16_t;
typedef unsigned short __uint16_t;
typedef int __int32_t;
typedef unsigned int __uint32_t;
typedef long __int64_t;
typedef unsigned long __uint64_t;
typedef __int8_t __int_least8_t;
typedef __uint8_t __uint_least8_t;
typedef __int16_t __int_least16_t;
typedef __uint16_t __uint_least16_t;
typedef __int32_t __int_least32_t;
typedef __uint32_t __uint_least32_t;
typedef __int64_t __int_least64_t;
typedef __uint64_t __uint_least64_t;
typedef long __quad_t;
typedef unsigned long __u_quad_t;
typedef long __intmax_t;
typedef unsigned long __uintmax_t;
typedef unsigned long __dev_t;
typedef unsigned int __uid_t;
typedef unsigned int __gid_t;
typedef unsigned long __ino_t;
typedef unsigned long __ino64_t;
typedef unsigned int __mode_t;
typedef unsigned long __nlink_t;
typedef long __off_t;
typedef long __off64_t;
typedef int __pid_t;
typedef struct {
    int __val[2];
} __fsid_t;
typedef long __clock_t;
typedef unsigned long __rlim_t;
typedef unsigned long __rlim64_t;
typedef unsigned int __id_t;
typedef long __time_t;
typedef unsigned int __useconds_t;
typedef long __suseconds_t;
typedef int __daddr_t;
typedef int __key_t;
typedef int __clockid_t;
typedef void *__timer_t;
typedef long __blksize_t;
typedef long __blkcnt_t;
typedef long __blkcnt64_t;
typedef unsigned long __fsblkcnt_t;
typedef unsigned long __fsblkcnt64_t;
typedef unsigned long __fsfilcnt_t;
typedef unsigned long __fsfilcnt64_t;
typedef long __fsword_t;
typedef long __ssize_t;
typedef long __syscall_slong_t;
typedef unsigned long __syscall_ulong_t;
typedef __off64_t __loff_t;
typedef char *__caddr_t;
typedef long __intptr_t;
typedef unsigned int __socklen_t;
typedef int __sig_atomic_t;
typedef __int8_t int8_t;
typedef __int16_t int16_t;
typedef __int32_t int32_t;
typedef __int64_t int64_t;
typedef __uint8_t uint8_t;
typedef __uint16_t uint16_t;
typedef __uint32_t uint32_t;
typedef __uint64_t uint64_t;
typedef __int_least8_t int_least8_t;
typedef __int_least16_t int_least16_t;
typedef __int_least32_t int_least32_t;
typedef __int_least64_t int_least64_t;
typedef __uint_least8_t uint_least8_t;
typedef __uint_least16_t uint_least16_t;
typedef __uint_least32_t uint_least32_t;
typedef __uint_least64_t uint_least64_t;
typedef signed char int_fast8_t;
typedef long int_fast16_t;
typedef long int_fast32_t;
typedef long int_fast64_t;
typedef unsigned char uint_fast8_t;
typedef unsigned long uint_fast16_t;
typedef unsigned long uint_fast32_t;
typedef unsigned long uint_fast64_t;
typedef long intptr_t;
typedef unsigned long uintptr_t;
typedef __intmax_t intmax_t;
typedef __uintmax_t uintmax_t;
typedef _Bool bool;
extern void abort(void);
extern uint64_t revng_init_local_sp(uint64_t param_0);
extern void indirect_placeholder_1(void);
extern void indirect_placeholder_13(uint64_t param_0);
void bb_usage(uint64_t rbp, uint64_t rdi) {
    uint64_t var_0;
    uint32_t *var_1;
    uint32_t var_2;
    bool var_3;
    uint64_t var_4;
    uint64_t *var_5;
    uint64_t local_sp_0;
    uint64_t var_6;
    var_0 = revng_init_local_sp(0UL);
    *(uint64_t *)(var_0 + (-8L)) = rbp;
    var_1 = (uint32_t *)(var_0 + (-12L));
    var_2 = (uint32_t)rdi;
    *var_1 = var_2;
    var_3 = (var_2 == 0U);
    var_4 = var_0 + (-32L);
    var_5 = (uint64_t *)var_4;
    local_sp_0 = var_4;
    if (var_3) {
        *var_5 = 4201801UL;
        indirect_placeholder_1();
        *(uint64_t *)(var_0 + (-40L)) = 4201821UL;
        indirect_placeholder_1();
        *(uint64_t *)(var_0 + (-48L)) = 4201841UL;
        indirect_placeholder_1();
        *(uint64_t *)(var_0 + (-56L)) = 4201861UL;
        indirect_placeholder_1();
        *(uint64_t *)(var_0 + (-64L)) = 4201881UL;
        indirect_placeholder_1();
        *(uint64_t *)(var_0 + (-72L)) = 4201901UL;
        indirect_placeholder_1();
        *(uint64_t *)(var_0 + (-80L)) = 4201921UL;
        indirect_placeholder_1();
        *(uint64_t *)(var_0 + (-88L)) = 4201941UL;
        indirect_placeholder_1();
        *(uint64_t *)(var_0 + (-96L)) = 4201961UL;
        indirect_placeholder_1();
        *(uint64_t *)(var_0 + (-104L)) = 4201981UL;
        indirect_placeholder_1();
        *(uint64_t *)(var_0 + (-112L)) = 4202001UL;
        indirect_placeholder_1();
        *(uint64_t *)(var_0 + (-120L)) = 4202021UL;
        indirect_placeholder_1();
        *(uint64_t *)(var_0 + (-128L)) = 4202053UL;
        indirect_placeholder_1();
        var_6 = var_0 + (-136L);
        *(uint64_t *)var_6 = 4202063UL;
        indirect_placeholder_13(4273708UL);
        local_sp_0 = var_6;
    } else {
        *var_5 = 4201771UL;
        indirect_placeholder_1();
    }
    *(uint64_t *)(local_sp_0 + (-8L)) = 4202073UL;
    indirect_placeholder_1();
    abort();
}

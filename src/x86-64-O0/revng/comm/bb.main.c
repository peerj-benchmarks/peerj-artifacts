typedef unsigned char __u_char;
typedef unsigned short __u_short;
typedef unsigned int __u_int;
typedef unsigned long __u_long;
typedef signed char __int8_t;
typedef unsigned char __uint8_t;
typedef short __int16_t;
typedef unsigned short __uint16_t;
typedef int __int32_t;
typedef unsigned int __uint32_t;
typedef long __int64_t;
typedef unsigned long __uint64_t;
typedef __int8_t __int_least8_t;
typedef __uint8_t __uint_least8_t;
typedef __int16_t __int_least16_t;
typedef __uint16_t __uint_least16_t;
typedef __int32_t __int_least32_t;
typedef __uint32_t __uint_least32_t;
typedef __int64_t __int_least64_t;
typedef __uint64_t __uint_least64_t;
typedef long __quad_t;
typedef unsigned long __u_quad_t;
typedef long __intmax_t;
typedef unsigned long __uintmax_t;
typedef unsigned long __dev_t;
typedef unsigned int __uid_t;
typedef unsigned int __gid_t;
typedef unsigned long __ino_t;
typedef unsigned long __ino64_t;
typedef unsigned int __mode_t;
typedef unsigned long __nlink_t;
typedef long __off_t;
typedef long __off64_t;
typedef int __pid_t;
typedef struct {
    int __val[2];
} __fsid_t;
typedef long __clock_t;
typedef unsigned long __rlim_t;
typedef unsigned long __rlim64_t;
typedef unsigned int __id_t;
typedef long __time_t;
typedef unsigned int __useconds_t;
typedef long __suseconds_t;
typedef int __daddr_t;
typedef int __key_t;
typedef int __clockid_t;
typedef void *__timer_t;
typedef long __blksize_t;
typedef long __blkcnt_t;
typedef long __blkcnt64_t;
typedef unsigned long __fsblkcnt_t;
typedef unsigned long __fsblkcnt64_t;
typedef unsigned long __fsfilcnt_t;
typedef unsigned long __fsfilcnt64_t;
typedef long __fsword_t;
typedef long __ssize_t;
typedef long __syscall_slong_t;
typedef unsigned long __syscall_ulong_t;
typedef __off64_t __loff_t;
typedef char *__caddr_t;
typedef long __intptr_t;
typedef unsigned int __socklen_t;
typedef int __sig_atomic_t;
typedef __int8_t int8_t;
typedef __int16_t int16_t;
typedef __int32_t int32_t;
typedef __int64_t int64_t;
typedef __uint8_t uint8_t;
typedef __uint16_t uint16_t;
typedef __uint32_t uint32_t;
typedef __uint64_t uint64_t;
typedef __int_least8_t int_least8_t;
typedef __int_least16_t int_least16_t;
typedef __int_least32_t int_least32_t;
typedef __int_least64_t int_least64_t;
typedef __uint_least8_t uint_least8_t;
typedef __uint_least16_t uint_least16_t;
typedef __uint_least32_t uint_least32_t;
typedef __uint_least64_t uint_least64_t;
typedef signed char int_fast8_t;
typedef long int_fast16_t;
typedef long int_fast32_t;
typedef long int_fast64_t;
typedef unsigned char uint_fast8_t;
typedef unsigned long uint_fast16_t;
typedef unsigned long uint_fast32_t;
typedef unsigned long uint_fast64_t;
typedef long intptr_t;
typedef unsigned long uintptr_t;
typedef __intmax_t intmax_t;
typedef __uintmax_t uintmax_t;
typedef _Bool bool;
struct indirect_placeholder_30_ret_type;
struct indirect_placeholder_25_ret_type;
struct indirect_placeholder_26_ret_type;
struct indirect_placeholder_24_ret_type;
struct indirect_placeholder_27_ret_type;
struct indirect_placeholder_23_ret_type;
struct indirect_placeholder_28_ret_type;
struct indirect_placeholder_30_ret_type {
    uint64_t field_0;
    uint64_t field_1;
    uint64_t field_2;
    uint64_t field_3;
};
struct indirect_placeholder_25_ret_type {
    uint64_t field_0;
    uint64_t field_1;
    uint64_t field_2;
};
struct indirect_placeholder_26_ret_type {
    uint64_t field_0;
    uint64_t field_1;
    uint64_t field_2;
};
struct indirect_placeholder_24_ret_type {
    uint64_t field_0;
    uint64_t field_1;
    uint64_t field_2;
};
struct indirect_placeholder_27_ret_type {
    uint64_t field_0;
    uint64_t field_1;
    uint64_t field_2;
};
struct indirect_placeholder_23_ret_type {
    uint64_t field_0;
    uint64_t field_1;
    uint64_t field_2;
};
struct indirect_placeholder_28_ret_type {
    uint64_t field_0;
    uint64_t field_1;
    uint64_t field_2;
};
extern void abort(void);
extern uint64_t revng_init_local_sp(uint64_t param_0);
extern uint64_t init_rbp(void);
extern void indirect_placeholder_1(void);
extern void indirect_placeholder_15(uint64_t param_0, uint64_t param_1);
extern void indirect_placeholder_13(uint64_t param_0);
extern uint64_t indirect_placeholder_31(uint64_t param_0);
extern struct indirect_placeholder_30_ret_type indirect_placeholder_30(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint64_t param_3, uint64_t param_4);
extern struct indirect_placeholder_25_ret_type indirect_placeholder_25(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint64_t param_3, uint64_t param_4, uint64_t param_5, uint64_t param_6);
extern struct indirect_placeholder_26_ret_type indirect_placeholder_26(uint64_t param_0);
extern struct indirect_placeholder_24_ret_type indirect_placeholder_24(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint64_t param_3, uint64_t param_4, uint64_t param_5, uint64_t param_6);
extern struct indirect_placeholder_27_ret_type indirect_placeholder_27(uint64_t param_0);
extern struct indirect_placeholder_23_ret_type indirect_placeholder_23(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint64_t param_3, uint64_t param_4, uint64_t param_5, uint64_t param_6);
extern struct indirect_placeholder_28_ret_type indirect_placeholder_28(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint64_t param_3, uint64_t param_4, uint64_t param_5, uint64_t param_6);
extern void indirect_placeholder_29(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint64_t param_3, uint64_t param_4, uint64_t param_5);
void bb_main(uint64_t rdi, uint64_t rsi) {
    struct indirect_placeholder_30_ret_type var_13;
    uint64_t var_22;
    uint64_t var_23;
    uint64_t var_25;
    uint64_t var_26;
    uint64_t var_10;
    uint64_t var_11;
    uint64_t var_12;
    uint64_t var_14;
    uint64_t var_15;
    uint64_t var_16;
    uint64_t var_17;
    uint32_t var_18;
    uint64_t var_0;
    uint64_t var_1;
    uint64_t var_2;
    uint32_t *var_3;
    uint64_t var_4;
    uint64_t *var_5;
    uint64_t var_6;
    uint64_t var_7;
    uint64_t var_8;
    uint32_t *var_9;
    uint64_t local_sp_4;
    uint64_t local_sp_0;
    uint32_t var_27;
    uint32_t var_28;
    uint32_t var_29;
    uint64_t var_30;
    uint64_t var_31;
    struct indirect_placeholder_26_ret_type var_32;
    uint64_t var_33;
    uint64_t var_34;
    uint64_t var_35;
    uint64_t var_36;
    bool var_37;
    uint64_t var_38;
    uint64_t var_40;
    struct indirect_placeholder_27_ret_type var_41;
    uint64_t var_42;
    uint64_t var_43;
    uint64_t var_44;
    uint64_t var_39;
    uint64_t var_24;
    uint64_t local_sp_2;
    uint64_t local_sp_1;
    uint64_t var_19;
    uint64_t var_20;
    uint64_t *var_21;
    uint64_t rax_0;
    uint64_t local_sp_3;
    uint64_t local_sp_4_be;
    unsigned int loop_state_var;
    var_0 = revng_init_local_sp(0UL);
    var_1 = init_rbp();
    var_2 = var_0 + (-8L);
    *(uint64_t *)var_2 = var_1;
    var_3 = (uint32_t *)(var_0 + (-28L));
    *var_3 = (uint32_t)rdi;
    var_4 = var_0 + (-40L);
    var_5 = (uint64_t *)var_4;
    *var_5 = rsi;
    var_6 = **(uint64_t **)var_4;
    *(uint64_t *)(var_0 + (-48L)) = 4205096UL;
    indirect_placeholder_13(var_6);
    *(uint64_t *)(var_0 + (-56L)) = 4205111UL;
    indirect_placeholder_1();
    *(uint64_t *)(var_0 + (-64L)) = 4205121UL;
    var_7 = indirect_placeholder_31(3UL);
    *(unsigned char *)6382928UL = (unsigned char)var_7;
    var_8 = var_0 + (-72L);
    *(uint64_t *)var_8 = 4205137UL;
    indirect_placeholder_1();
    *(unsigned char *)6382929UL = (unsigned char)'\x01';
    *(unsigned char *)6382930UL = (unsigned char)'\x01';
    *(unsigned char *)6382931UL = (unsigned char)'\x01';
    *(unsigned char *)6382932UL = (unsigned char)'\x00';
    *(unsigned char *)6382934UL = (unsigned char)'\x00';
    *(unsigned char *)6382933UL = (unsigned char)'\x00';
    *(uint32_t *)6382936UL = 0U;
    *(unsigned char *)6382935UL = (unsigned char)'\x00';
    var_9 = (uint32_t *)(var_0 + (-12L));
    rax_0 = 1UL;
    local_sp_4 = var_8;
    while (1U)
        {
            var_10 = *var_5;
            var_11 = (uint64_t)*var_3;
            var_12 = local_sp_4 + (-8L);
            *(uint64_t *)var_12 = 4205661UL;
            var_13 = indirect_placeholder_30(4273849UL, 4272192UL, var_11, var_10, 0UL);
            var_14 = var_13.field_0;
            var_15 = var_13.field_1;
            var_16 = var_13.field_2;
            var_17 = var_13.field_3;
            var_18 = (uint32_t)var_14;
            *var_9 = var_18;
            local_sp_4_be = var_12;
            local_sp_1 = var_12;
            local_sp_2 = var_12;
            if (var_18 != 4294967295U) {
                if (*(uint64_t *)6382944UL != 0UL) {
                    loop_state_var = 0U;
                    break;
                }
                *(uint64_t *)6382944UL = 1UL;
                loop_state_var = 0U;
                break;
            }
            if ((uint64_t)(var_18 + (-51)) == 0UL) {
                *(unsigned char *)6382931UL = (unsigned char)'\x00';
            } else {
                if ((int)var_18 > (int)51U) {
                    if ((uint64_t)(var_18 + (-129)) == 0UL) {
                        *(uint32_t *)6382936UL = 2U;
                    } else {
                        if ((int)var_18 > (int)129U) {
                            if ((uint64_t)(var_18 + (-130)) == 0UL) {
                                var_22 = *(uint64_t *)6382568UL;
                                var_23 = local_sp_4 + (-16L);
                                *(uint64_t *)var_23 = 4205442UL;
                                indirect_placeholder_1();
                                local_sp_2 = var_23;
                                if (*(uint64_t *)6382944UL != 0UL & (uint64_t)(uint32_t)var_22 == 0UL) {
                                    var_24 = local_sp_4 + (-24L);
                                    *(uint64_t *)var_24 = 4205471UL;
                                    indirect_placeholder_28(0UL, 4273776UL, var_15, 1UL, 0UL, var_16, var_17);
                                    local_sp_2 = var_24;
                                }
                                *(uint64_t *)6382568UL = *(uint64_t *)6383416UL;
                                local_sp_3 = local_sp_2;
                                if (**(unsigned char **)6383416UL != '\x00') {
                                    var_25 = *(uint64_t *)6383416UL;
                                    var_26 = local_sp_2 + (-8L);
                                    *(uint64_t *)var_26 = 4205514UL;
                                    indirect_placeholder_1();
                                    local_sp_3 = var_26;
                                    rax_0 = var_25;
                                }
                                *(uint64_t *)6382944UL = rax_0;
                                local_sp_4_be = local_sp_3;
                            } else {
                                if ((uint64_t)(var_18 + (-131)) != 0UL) {
                                    loop_state_var = 1U;
                                    break;
                                }
                                *(unsigned char *)6382935UL = (unsigned char)'\x01';
                            }
                        } else {
                            if ((uint64_t)(var_18 + (-122)) == 0UL) {
                                *(unsigned char *)6382560UL = (unsigned char)'\x00';
                            } else {
                                if ((uint64_t)(var_18 + (-128)) != 0UL) {
                                    loop_state_var = 1U;
                                    break;
                                }
                                *(uint32_t *)6382936UL = 1U;
                            }
                        }
                    }
                } else {
                    if ((uint64_t)(var_18 + 130U) == 0UL) {
                        *(uint64_t *)(local_sp_4 + (-16L)) = 4205549UL;
                        indirect_placeholder_15(var_2, 0UL);
                        abort();
                    }
                    if ((int)var_18 <= (int)4294967166U) {
                        if (var_18 != 4294967165U) {
                            loop_state_var = 1U;
                            break;
                        }
                        var_19 = *(uint64_t *)6382576UL;
                        var_20 = local_sp_4 + (-24L);
                        var_21 = (uint64_t *)var_20;
                        *var_21 = 0UL;
                        *(uint64_t *)(local_sp_4 + (-32L)) = 4205607UL;
                        indirect_placeholder_29(0UL, 4271832UL, var_19, 4273708UL, 4273813UL, 4273829UL);
                        *var_21 = 4205621UL;
                        indirect_placeholder_1();
                        local_sp_1 = var_20;
                        loop_state_var = 1U;
                        break;
                    }
                    if ((uint64_t)(var_18 + (-49)) == 0UL) {
                        *(unsigned char *)6382929UL = (unsigned char)'\x00';
                    } else {
                        if ((uint64_t)(var_18 + (-50)) != 0UL) {
                            loop_state_var = 1U;
                            break;
                        }
                        *(unsigned char *)6382930UL = (unsigned char)'\x00';
                    }
                }
            }
            local_sp_4 = local_sp_4_be;
            continue;
        }
    switch (loop_state_var) {
      case 1U:
        {
            *(uint64_t *)(local_sp_1 + (-8L)) = 4205631UL;
            indirect_placeholder_15(var_2, 1UL);
            abort();
        }
        break;
      case 0U:
        {
            var_27 = *(uint32_t *)6382712UL;
            var_28 = *var_3;
            var_29 = var_28 - var_27;
            if ((int)var_29 > (int)1U) {
                var_37 = ((int)var_29 > (int)2U);
                var_38 = (uint64_t)var_27 << 3UL;
                if (!var_37) {
                    var_39 = *var_5 + var_38;
                    *(uint64_t *)(local_sp_4 + (-16L)) = 4205947UL;
                    indirect_placeholder_13(var_39);
                    return;
                }
                var_40 = *(uint64_t *)(*var_5 + (var_38 + 16UL));
                *(uint64_t *)(local_sp_4 + (-16L)) = 4205878UL;
                var_41 = indirect_placeholder_27(var_40);
                var_42 = var_41.field_0;
                var_43 = var_41.field_1;
                var_44 = var_41.field_2;
                *(uint64_t *)(local_sp_4 + (-24L)) = 4205906UL;
                indirect_placeholder_23(0UL, 4273895UL, var_42, 0UL, 0UL, var_43, var_44);
                *(uint64_t *)(local_sp_4 + (-32L)) = 4205916UL;
                indirect_placeholder_15(var_2, 1UL);
                abort();
            }
            if ((int)var_27 < (int)var_28) {
                var_31 = *(uint64_t *)(*var_5 + (((uint64_t)var_28 << 3UL) + (-8L)));
                *(uint64_t *)(local_sp_4 + (-16L)) = 4205784UL;
                var_32 = indirect_placeholder_26(var_31);
                var_33 = var_32.field_0;
                var_34 = var_32.field_1;
                var_35 = var_32.field_2;
                var_36 = local_sp_4 + (-24L);
                *(uint64_t *)var_36 = 4205812UL;
                indirect_placeholder_24(0UL, 4273870UL, var_33, 0UL, 0UL, var_34, var_35);
                local_sp_0 = var_36;
            } else {
                var_30 = local_sp_4 + (-16L);
                *(uint64_t *)var_30 = 4205751UL;
                indirect_placeholder_25(0UL, 4273854UL, var_15, 0UL, 0UL, var_16, var_17);
                local_sp_0 = var_30;
            }
            *(uint64_t *)(local_sp_0 + (-8L)) = 4205822UL;
            indirect_placeholder_15(var_2, 1UL);
            abort();
        }
        break;
    }
}

typedef unsigned char __u_char;
typedef unsigned short __u_short;
typedef unsigned int __u_int;
typedef unsigned long __u_long;
typedef signed char __int8_t;
typedef unsigned char __uint8_t;
typedef short __int16_t;
typedef unsigned short __uint16_t;
typedef int __int32_t;
typedef unsigned int __uint32_t;
typedef long __int64_t;
typedef unsigned long __uint64_t;
typedef __int8_t __int_least8_t;
typedef __uint8_t __uint_least8_t;
typedef __int16_t __int_least16_t;
typedef __uint16_t __uint_least16_t;
typedef __int32_t __int_least32_t;
typedef __uint32_t __uint_least32_t;
typedef __int64_t __int_least64_t;
typedef __uint64_t __uint_least64_t;
typedef long __quad_t;
typedef unsigned long __u_quad_t;
typedef long __intmax_t;
typedef unsigned long __uintmax_t;
typedef unsigned long __dev_t;
typedef unsigned int __uid_t;
typedef unsigned int __gid_t;
typedef unsigned long __ino_t;
typedef unsigned long __ino64_t;
typedef unsigned int __mode_t;
typedef unsigned long __nlink_t;
typedef long __off_t;
typedef long __off64_t;
typedef int __pid_t;
typedef struct {
    int __val[2];
} __fsid_t;
typedef long __clock_t;
typedef unsigned long __rlim_t;
typedef unsigned long __rlim64_t;
typedef unsigned int __id_t;
typedef long __time_t;
typedef unsigned int __useconds_t;
typedef long __suseconds_t;
typedef int __daddr_t;
typedef int __key_t;
typedef int __clockid_t;
typedef void *__timer_t;
typedef long __blksize_t;
typedef long __blkcnt_t;
typedef long __blkcnt64_t;
typedef unsigned long __fsblkcnt_t;
typedef unsigned long __fsblkcnt64_t;
typedef unsigned long __fsfilcnt_t;
typedef unsigned long __fsfilcnt64_t;
typedef long __fsword_t;
typedef long __ssize_t;
typedef long __syscall_slong_t;
typedef unsigned long __syscall_ulong_t;
typedef __off64_t __loff_t;
typedef char *__caddr_t;
typedef long __intptr_t;
typedef unsigned int __socklen_t;
typedef int __sig_atomic_t;
typedef __int8_t int8_t;
typedef __int16_t int16_t;
typedef __int32_t int32_t;
typedef __int64_t int64_t;
typedef __uint8_t uint8_t;
typedef __uint16_t uint16_t;
typedef __uint32_t uint32_t;
typedef __uint64_t uint64_t;
typedef __int_least8_t int_least8_t;
typedef __int_least16_t int_least16_t;
typedef __int_least32_t int_least32_t;
typedef __int_least64_t int_least64_t;
typedef __uint_least8_t uint_least8_t;
typedef __uint_least16_t uint_least16_t;
typedef __uint_least32_t uint_least32_t;
typedef __uint_least64_t uint_least64_t;
typedef signed char int_fast8_t;
typedef long int_fast16_t;
typedef long int_fast32_t;
typedef long int_fast64_t;
typedef unsigned char uint_fast8_t;
typedef unsigned long uint_fast16_t;
typedef unsigned long uint_fast32_t;
typedef unsigned long uint_fast64_t;
typedef long intptr_t;
typedef unsigned long uintptr_t;
typedef __intmax_t intmax_t;
typedef __uintmax_t uintmax_t;
struct indirect_placeholder_133_ret_type;
struct indirect_placeholder_134_ret_type;
struct indirect_placeholder_133_ret_type {
    uint64_t field_0;
    uint64_t field_1;
    uint64_t field_2;
};
struct indirect_placeholder_134_ret_type {
    uint64_t field_0;
    uint64_t field_1;
    uint64_t field_2;
    uint64_t field_3;
    uint64_t field_4;
};
extern uint64_t revng_init_local_sp(uint64_t param_0);
extern uint64_t init_rbp(void);
extern void indirect_placeholder(void);
extern uint64_t indirect_placeholder_1(uint64_t param_0, uint64_t param_1);
extern uint64_t init_rbx(void);
extern uint64_t init_r10(void);
extern uint64_t indirect_placeholder_7(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint64_t param_3);
extern uint64_t init_r9(void);
extern struct indirect_placeholder_133_ret_type indirect_placeholder_133(uint64_t param_0, uint64_t param_1);
extern struct indirect_placeholder_134_ret_type indirect_placeholder_134(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint64_t param_3, uint64_t param_4, uint64_t param_5, uint64_t param_6, uint64_t param_7);
typedef _Bool bool;
uint64_t bb_calc_eclosure_iter(uint64_t rdx, uint64_t rcx, uint64_t rdi, uint64_t rsi) {
    uint64_t *_pre142;
    uint64_t var_0;
    uint64_t var_1;
    uint64_t var_2;
    uint64_t var_3;
    uint64_t var_4;
    uint64_t *var_5;
    uint64_t var_6;
    uint64_t *var_7;
    uint64_t *var_8;
    unsigned char *var_9;
    unsigned char *var_10;
    uint64_t var_11;
    uint64_t var_12;
    uint64_t var_13;
    struct indirect_placeholder_133_ret_type var_14;
    uint64_t var_15;
    uint32_t *var_16;
    uint32_t var_17;
    uint64_t *_pre138;
    uint64_t var_52;
    uint64_t local_sp_3;
    uint64_t *_pre146;
    uint64_t *_pre_phi147;
    uint64_t var_56;
    uint64_t *var_57;
    uint64_t *var_58;
    uint64_t *var_59;
    uint64_t *_pre_phi143;
    uint64_t *_pre_phi139;
    uint64_t var_60;
    uint64_t local_sp_2;
    uint64_t var_50;
    uint64_t local_sp_4;
    uint32_t rax_0_shrunk;
    uint64_t **var_18;
    uint64_t var_19;
    uint64_t var_20;
    uint64_t var_21;
    uint32_t var_22;
    uint64_t _pre_phi137;
    uint64_t var_23;
    uint64_t var_24;
    uint64_t var_25;
    uint64_t var_26;
    struct indirect_placeholder_134_ret_type var_27;
    uint32_t var_28;
    uint64_t var_29;
    uint64_t _pre131;
    uint64_t _pre132;
    uint64_t local_sp_0;
    uint64_t *var_30;
    uint64_t *var_31;
    uint64_t var_32;
    uint64_t *var_33;
    uint64_t *var_34;
    uint64_t *var_35;
    uint64_t var_36;
    uint64_t local_sp_1;
    uint64_t var_37;
    uint64_t var_38;
    uint64_t var_39;
    uint64_t var_40;
    uint64_t var_41;
    uint64_t var_42;
    uint64_t *var_43;
    uint64_t var_51;
    uint64_t var_44;
    uint64_t var_45;
    uint32_t var_46;
    uint64_t var_47;
    uint64_t var_48;
    uint32_t var_49;
    uint64_t var_53;
    unsigned char *var_54;
    unsigned char var_55;
    unsigned int loop_state_var;
    bool switch_state_var;
    var_0 = revng_init_local_sp(0UL);
    var_1 = init_rbp();
    var_2 = init_rbx();
    var_3 = init_r9();
    var_4 = init_r10();
    *(uint64_t *)(var_0 + (-8L)) = var_1;
    var_5 = (uint64_t *)(var_0 + (-112L));
    *var_5 = rdi;
    var_6 = var_0 + (-120L);
    var_7 = (uint64_t *)var_6;
    *var_7 = rsi;
    var_8 = (uint64_t *)(var_0 + (-128L));
    *var_8 = rdx;
    var_9 = (unsigned char *)(var_0 + (-132L));
    *var_9 = (unsigned char)rcx;
    var_10 = (unsigned char *)(var_0 + (-17L));
    *var_10 = (unsigned char)'\x00';
    var_11 = *(uint64_t *)(((*var_8 * 24UL) + *(uint64_t *)(*var_7 + 40UL)) + 8UL) + 1UL;
    var_12 = var_0 + (-72L);
    var_13 = var_0 + (-144L);
    *(uint64_t *)var_13 = 4263341UL;
    var_14 = indirect_placeholder_133(var_12, var_11);
    var_15 = var_14.field_0;
    var_16 = (uint32_t *)(var_0 + (-24L));
    var_17 = (uint32_t)var_15;
    *var_16 = var_17;
    rax_0_shrunk = var_17;
    local_sp_0 = var_13;
    var_36 = 0UL;
    if (var_17 == 0U) {
        return (uint64_t)rax_0_shrunk;
    }
    *(uint64_t *)(((*var_8 * 24UL) + *(uint64_t *)(*var_7 + 48UL)) + 8UL) = 18446744073709551615UL;
    var_18 = (uint64_t **)var_6;
    var_19 = **var_18;
    var_20 = *var_8;
    var_21 = (var_19 + (var_20 << 4UL)) + 8UL;
    var_22 = *(uint32_t *)var_21;
    rax_0_shrunk = 12U;
    _pre_phi137 = var_21;
    var_29 = var_20;
    var_23 = *var_7;
    var_24 = (var_20 * 24UL) + *(uint64_t *)(var_23 + 40UL);
    if ((var_22 & 261888U) != 0U & *(uint64_t *)(var_24 + 8UL) != 0UL & (*(unsigned char *)(((**(uint64_t **)(var_24 + 16UL) << 4UL) + var_19) + 10UL) & '\x04') != '\x00') {
        var_25 = (uint64_t)(uint32_t)((uint16_t)(var_22 >> 8U) & (unsigned short)1023U);
        var_26 = var_0 + (-152L);
        *(uint64_t *)var_26 = 4263596UL;
        var_27 = indirect_placeholder_134(var_20, var_20, var_25, var_2, var_23, var_20, var_3, var_4);
        var_28 = (uint32_t)var_27.field_0;
        *var_16 = var_28;
        rax_0_shrunk = var_28;
        local_sp_0 = var_26;
        if (var_28 != 0U) {
            return (uint64_t)rax_0_shrunk;
        }
        _pre131 = **var_18;
        _pre132 = *var_8;
        _pre_phi137 = (_pre131 + (_pre132 << 4UL)) + 8UL;
        var_29 = _pre132;
    }
    local_sp_1 = local_sp_0;
    var_52 = var_29;
    local_sp_4 = local_sp_0;
    if ((*(unsigned char *)_pre_phi137 & '\b') != '\x00') {
        var_30 = (uint64_t *)(var_0 + (-16L));
        *var_30 = 0UL;
        var_31 = (uint64_t *)(var_0 + (-32L));
        var_32 = var_0 + (-104L);
        var_33 = (uint64_t *)var_32;
        var_34 = (uint64_t *)(var_0 + (-96L));
        var_35 = (uint64_t *)(var_0 + (-88L));
        while (1U)
            {
                var_37 = *(uint64_t *)(*var_7 + 40UL);
                var_38 = *var_8;
                var_39 = (var_38 * 24UL) + var_37;
                local_sp_2 = local_sp_1;
                local_sp_3 = local_sp_1;
                var_52 = var_38;
                local_sp_4 = local_sp_1;
                if ((long)*(uint64_t *)(var_39 + 8UL) <= (long)var_36) {
                    loop_state_var = 1U;
                    break;
                }
                var_40 = *(uint64_t *)(*(uint64_t *)(var_39 + 16UL) + (var_36 << 3UL));
                *var_31 = var_40;
                var_41 = *var_7;
                var_42 = (var_40 * 24UL) + *(uint64_t *)(var_41 + 48UL);
                var_43 = (uint64_t *)(var_42 + 8UL);
                switch_state_var = 0;
                switch (*var_43) {
                  case 18446744073709551615UL:
                    {
                        *var_10 = (unsigned char)'\x01';
                    }
                    break;
                  case 0UL:
                    {
                        var_44 = local_sp_1 + (-8L);
                        *(uint64_t *)var_44 = 4263830UL;
                        var_45 = indirect_placeholder_7(var_40, 0UL, var_32, var_41);
                        var_46 = (uint32_t)var_45;
                        *var_16 = var_46;
                        rax_0_shrunk = var_46;
                        local_sp_3 = var_44;
                        if (var_46 != 0U) {
                            loop_state_var = 0U;
                            switch_state_var = 1;
                            break;
                        }
                        var_47 = local_sp_3 + (-8L);
                        *(uint64_t *)var_47 = 4263926UL;
                        var_48 = indirect_placeholder_1(var_12, var_32);
                        var_49 = (uint32_t)var_48;
                        *var_16 = var_49;
                        rax_0_shrunk = var_49;
                        local_sp_2 = var_47;
                        if (var_49 != 0U) {
                            loop_state_var = 0U;
                            switch_state_var = 1;
                            break;
                        }
                        if (*(uint64_t *)(((*var_31 * 24UL) + *(uint64_t *)(*var_7 + 48UL)) + 8UL) == 0UL) {
                            *var_10 = (unsigned char)'\x01';
                            var_50 = local_sp_3 + (-16L);
                            *(uint64_t *)var_50 = 4264005UL;
                            indirect_placeholder();
                            local_sp_2 = var_50;
                        }
                    }
                    break;
                  default:
                    {
                        *var_33 = *(uint64_t *)var_42;
                        *var_34 = *var_43;
                        *var_35 = *(uint64_t *)(var_42 + 16UL);
                        var_47 = local_sp_3 + (-8L);
                        *(uint64_t *)var_47 = 4263926UL;
                        var_48 = indirect_placeholder_1(var_12, var_32);
                        var_49 = (uint32_t)var_48;
                        *var_16 = var_49;
                        rax_0_shrunk = var_49;
                        local_sp_2 = var_47;
                        if (var_49 == 0U) {
                            loop_state_var = 0U;
                            switch_state_var = 1;
                            break;
                        }
                        if (*(uint64_t *)(((*var_31 * 24UL) + *(uint64_t *)(*var_7 + 48UL)) + 8UL) == 0UL) {
                            *var_10 = (unsigned char)'\x01';
                            var_50 = local_sp_3 + (-16L);
                            *(uint64_t *)var_50 = 4264005UL;
                            indirect_placeholder();
                            local_sp_2 = var_50;
                        }
                    }
                    break;
                }
                if (switch_state_var)
                    break;
                var_51 = *var_30 + 1UL;
                *var_30 = var_51;
                var_36 = var_51;
                local_sp_1 = local_sp_2;
                continue;
            }
        switch (loop_state_var) {
          case 0U:
            {
                return (uint64_t)rax_0_shrunk;
            }
            break;
          case 1U:
            {
                break;
            }
            break;
        }
    }
    *(uint64_t *)(local_sp_4 + (-8L)) = 4264071UL;
    var_53 = indirect_placeholder_1(var_12, var_52);
    var_54 = (unsigned char *)(var_0 + (-33L));
    var_55 = (unsigned char)var_53;
    *var_54 = var_55;
    if (var_55 == '\x01') {
        return;
    }
    rax_0_shrunk = 0U;
    if (*var_10 != '\x00') {
        if (*var_9 != '\x01') {
            *(uint64_t *)(((*var_8 * 24UL) + *(uint64_t *)(*var_7 + 48UL)) + 8UL) = 0UL;
            _pre138 = (uint64_t *)var_12;
            _pre142 = (uint64_t *)(var_0 + (-64L));
            _pre146 = (uint64_t *)(var_0 + (-56L));
            _pre_phi147 = _pre146;
            _pre_phi143 = _pre142;
            _pre_phi139 = _pre138;
            var_60 = *var_5;
            *(uint64_t *)var_60 = *_pre_phi139;
            *(uint64_t *)(var_60 + 8UL) = *_pre_phi143;
            *(uint64_t *)(var_60 + 16UL) = *_pre_phi147;
            return (uint64_t)rax_0_shrunk;
        }
    }
    var_56 = (*var_8 * 24UL) + *(uint64_t *)(*var_7 + 48UL);
    var_57 = (uint64_t *)var_12;
    *(uint64_t *)var_56 = *var_57;
    var_58 = (uint64_t *)(var_0 + (-64L));
    *(uint64_t *)(var_56 + 8UL) = *var_58;
    var_59 = (uint64_t *)(var_0 + (-56L));
    *(uint64_t *)(var_56 + 16UL) = *var_59;
    _pre_phi147 = var_59;
    _pre_phi143 = var_58;
    _pre_phi139 = var_57;
    var_60 = *var_5;
    *(uint64_t *)var_60 = *_pre_phi139;
    *(uint64_t *)(var_60 + 8UL) = *_pre_phi143;
    *(uint64_t *)(var_60 + 16UL) = *_pre_phi147;
}

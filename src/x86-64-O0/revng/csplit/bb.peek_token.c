typedef unsigned char __u_char;
typedef unsigned short __u_short;
typedef unsigned int __u_int;
typedef unsigned long __u_long;
typedef signed char __int8_t;
typedef unsigned char __uint8_t;
typedef short __int16_t;
typedef unsigned short __uint16_t;
typedef int __int32_t;
typedef unsigned int __uint32_t;
typedef long __int64_t;
typedef unsigned long __uint64_t;
typedef __int8_t __int_least8_t;
typedef __uint8_t __uint_least8_t;
typedef __int16_t __int_least16_t;
typedef __uint16_t __uint_least16_t;
typedef __int32_t __int_least32_t;
typedef __uint32_t __uint_least32_t;
typedef __int64_t __int_least64_t;
typedef __uint64_t __uint_least64_t;
typedef long __quad_t;
typedef unsigned long __u_quad_t;
typedef long __intmax_t;
typedef unsigned long __uintmax_t;
typedef unsigned long __dev_t;
typedef unsigned int __uid_t;
typedef unsigned int __gid_t;
typedef unsigned long __ino_t;
typedef unsigned long __ino64_t;
typedef unsigned int __mode_t;
typedef unsigned long __nlink_t;
typedef long __off_t;
typedef long __off64_t;
typedef int __pid_t;
typedef struct {
    int __val[2];
} __fsid_t;
typedef long __clock_t;
typedef unsigned long __rlim_t;
typedef unsigned long __rlim64_t;
typedef unsigned int __id_t;
typedef long __time_t;
typedef unsigned int __useconds_t;
typedef long __suseconds_t;
typedef int __daddr_t;
typedef int __key_t;
typedef int __clockid_t;
typedef void *__timer_t;
typedef long __blksize_t;
typedef long __blkcnt_t;
typedef long __blkcnt64_t;
typedef unsigned long __fsblkcnt_t;
typedef unsigned long __fsblkcnt64_t;
typedef unsigned long __fsfilcnt_t;
typedef unsigned long __fsfilcnt64_t;
typedef long __fsword_t;
typedef long __ssize_t;
typedef long __syscall_slong_t;
typedef unsigned long __syscall_ulong_t;
typedef __off64_t __loff_t;
typedef char *__caddr_t;
typedef long __intptr_t;
typedef unsigned int __socklen_t;
typedef int __sig_atomic_t;
typedef __int8_t int8_t;
typedef __int16_t int16_t;
typedef __int32_t int32_t;
typedef __int64_t int64_t;
typedef __uint8_t uint8_t;
typedef __uint16_t uint16_t;
typedef __uint32_t uint32_t;
typedef __uint64_t uint64_t;
typedef __int_least8_t int_least8_t;
typedef __int_least16_t int_least16_t;
typedef __int_least32_t int_least32_t;
typedef __int_least64_t int_least64_t;
typedef __uint_least8_t uint_least8_t;
typedef __uint_least16_t uint_least16_t;
typedef __uint_least32_t uint_least32_t;
typedef __uint_least64_t uint_least64_t;
typedef signed char int_fast8_t;
typedef long int_fast16_t;
typedef long int_fast32_t;
typedef long int_fast64_t;
typedef unsigned char uint_fast8_t;
typedef unsigned long uint_fast16_t;
typedef unsigned long uint_fast32_t;
typedef unsigned long uint_fast64_t;
typedef long intptr_t;
typedef unsigned long uintptr_t;
typedef __intmax_t intmax_t;
typedef __uintmax_t uintmax_t;
extern void function_dispatcher(unsigned char *param_0);
extern uint64_t revng_init_local_sp(uint64_t param_0);
extern uint64_t init_rbp(void);
extern void indirect_placeholder(void);
extern uint64_t indirect_placeholder_1(uint64_t param_0, uint64_t param_1);
uint64_t bb_peek_token(uint64_t rdx, uint64_t rdi, uint64_t rsi) {
    uint64_t var_0;
    uint64_t var_1;
    uint64_t var_2;
    uint64_t *var_3;
    uint64_t *var_4;
    uint64_t var_5;
    uint64_t var_6;
    uint64_t var_7;
    unsigned char storemerge4;
    unsigned char *var_18;
    unsigned char storemerge;
    unsigned char var_23;
    unsigned char *var_24;
    uint64_t var_25;
    uint64_t rax_0;
    unsigned char var_31;
    unsigned char *var_32;
    unsigned char *var_37;
    uint64_t var_38;
    uint64_t var_33;
    uint64_t var_34;
    uint32_t *var_35;
    uint32_t var_36;
    unsigned char var_30;
    unsigned char var_8;
    unsigned char *var_9;
    unsigned char **var_10;
    unsigned char *var_11;
    unsigned char *var_12;
    uint64_t var_13;
    uint64_t var_14;
    unsigned char *var_15;
    uint64_t var_26;
    unsigned char *var_27;
    unsigned char var_28;
    uint64_t var_29;
    uint64_t var_16;
    uint64_t var_19;
    uint64_t var_20;
    uint32_t *var_21;
    uint32_t var_22;
    unsigned char var_17;
    uint64_t storemerge5_in_in;
    uint64_t storemerge5;
    var_0 = revng_init_local_sp(0UL);
    var_1 = init_rbp();
    *(uint64_t *)(var_0 + (-8L)) = var_1;
    var_2 = var_0 + (-48L);
    var_3 = (uint64_t *)var_2;
    *var_3 = rdi;
    var_4 = (uint64_t *)(var_0 + (-56L));
    *var_4 = rsi;
    *(uint64_t *)(var_0 + (-64L)) = rdx;
    var_5 = *var_4;
    var_6 = *(uint64_t *)(var_5 + 104UL);
    var_7 = *(uint64_t *)(var_5 + 72UL);
    storemerge4 = (unsigned char)'@';
    storemerge = (unsigned char)'@';
    var_23 = (unsigned char)'@';
    rax_0 = 1UL;
    var_31 = (unsigned char)'@';
    if ((long)var_6 <= (long)var_7) {
        *(unsigned char *)(*var_3 + 8UL) = (unsigned char)'\x02';
        rax_0 = 0UL;
        return rax_0;
    }
    var_8 = *(unsigned char *)(var_7 + *(uint64_t *)(var_5 + 8UL));
    var_9 = (unsigned char *)(var_0 + (-9L));
    *var_9 = var_8;
    var_10 = (unsigned char **)var_2;
    **var_10 = var_8;
    var_11 = (unsigned char *)(*var_3 + 10UL);
    *var_11 = (*var_11 & '\xbf');
    var_12 = (unsigned char *)(*var_3 + 10UL);
    *var_12 = (*var_12 & '\xdf');
    var_13 = *var_4;
    if ((int)*(uint32_t *)(var_13 + 144UL) <= (int)1U) {
        var_14 = *(uint64_t *)(var_13 + 72UL);
        if (var_14 != *(uint64_t *)(var_13 + 48UL) & *(uint32_t *)((var_14 << 2UL) + *(uint64_t *)(var_13 + 16UL)) != 4294967295U) {
            *(unsigned char *)(*var_3 + 8UL) = (unsigned char)'\x01';
            var_15 = (unsigned char *)(*var_3 + 10UL);
            *var_15 = (*var_15 | ' ');
            return rax_0;
        }
    }
    rax_0 = 2UL;
    if (*var_9 == '\\') {
        if ((long)(*(uint64_t *)(var_13 + 72UL) + 1UL) >= (long)*(uint64_t *)(var_13 + 88UL)) {
            *(unsigned char *)(*var_3 + 8UL) = (unsigned char)'$';
            return rax_0;
        }
        *(uint64_t *)(var_0 + (-80L)) = 4264598UL;
        var_26 = indirect_placeholder_1(var_13, 1UL);
        var_27 = (unsigned char *)(var_0 + (-10L));
        var_28 = (unsigned char)var_26;
        *var_27 = var_28;
        **var_10 = var_28;
        *(unsigned char *)(*var_3 + 8UL) = (unsigned char)'\x01';
        var_29 = *var_4;
        if ((int)*(uint32_t *)(var_29 + 144UL) > (int)1U) {
            var_33 = *(uint64_t *)(var_29 + 72UL) + 1UL;
            *(uint64_t *)(var_0 + (-88L)) = 4264661UL;
            var_34 = indirect_placeholder_1(var_29, var_33);
            var_35 = (uint32_t *)(var_0 + (-16L));
            var_36 = (uint32_t)var_34;
            *var_35 = var_36;
            *(uint64_t *)(var_0 + (-96L)) = 4264674UL;
            indirect_placeholder();
            if (var_36 == 0U) {
            } else {
                storemerge = (unsigned char)'\x00';
                if (*var_35 == 95U) {
                }
            }
            var_37 = (unsigned char *)(*var_3 + 10UL);
            *var_37 = (storemerge | (*var_37 & '\xbf'));
        } else {
            var_30 = *var_27;
            *(uint64_t *)(var_0 + (-88L)) = 4264738UL;
            indirect_placeholder();
            if (var_30 == '\x00') {
                var_31 = (*var_27 == '_') ? '@' : '\x00';
            }
            var_32 = (unsigned char *)(*var_3 + 10UL);
            *var_32 = (var_31 | (*var_32 & '\xbf'));
        }
        var_38 = (uint64_t)((uint32_t)(uint64_t)*var_27 + (-39));
        if (var_38 <= 86UL) {
            return rax_0;
        }
        storemerge5_in_in = (var_38 << 3UL) + 4377688UL;
    } else {
        *(unsigned char *)(*var_3 + 8UL) = (unsigned char)'\x01';
        var_16 = *var_4;
        if ((int)*(uint32_t *)(var_16 + 144UL) > (int)1U) {
            var_19 = *(uint64_t *)(var_16 + 72UL);
            *(uint64_t *)(var_0 + (-80L)) = 4265569UL;
            var_20 = indirect_placeholder_1(var_16, var_19);
            var_21 = (uint32_t *)(var_0 + (-20L));
            var_22 = (uint32_t)var_20;
            *var_21 = var_22;
            *(uint64_t *)(var_0 + (-88L)) = 4265582UL;
            indirect_placeholder();
            if (var_22 == 0U) {
                var_23 = (*var_21 == 95U) ? '@' : '\x00';
            }
            var_24 = (unsigned char *)(*var_3 + 10UL);
            *var_24 = (var_23 | (*var_24 & '\xbf'));
        } else {
            var_17 = **var_10;
            *(uint64_t *)(var_0 + (-80L)) = 4265652UL;
            indirect_placeholder();
            if (var_17 == '\x00') {
            } else {
                storemerge4 = (unsigned char)'\x00';
                if (**var_10 == '_') {
                }
            }
            var_18 = (unsigned char *)(*var_3 + 10UL);
            *var_18 = (storemerge4 | (*var_18 & '\xbf'));
        }
        var_25 = (uint64_t)((uint32_t)(uint64_t)*var_9 + (-10));
        if (var_25 <= 115UL) {
            return rax_0;
        }
        storemerge5_in_in = (var_25 << 3UL) + 4378384UL;
    }
    storemerge5 = *(uint64_t *)storemerge5_in_in;
    function_dispatcher((unsigned char *)(0UL));
    return storemerge5;
}

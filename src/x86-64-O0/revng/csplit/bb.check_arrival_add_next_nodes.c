typedef unsigned char __u_char;
typedef unsigned short __u_short;
typedef unsigned int __u_int;
typedef unsigned long __u_long;
typedef signed char __int8_t;
typedef unsigned char __uint8_t;
typedef short __int16_t;
typedef unsigned short __uint16_t;
typedef int __int32_t;
typedef unsigned int __uint32_t;
typedef long __int64_t;
typedef unsigned long __uint64_t;
typedef __int8_t __int_least8_t;
typedef __uint8_t __uint_least8_t;
typedef __int16_t __int_least16_t;
typedef __uint16_t __uint_least16_t;
typedef __int32_t __int_least32_t;
typedef __uint32_t __uint_least32_t;
typedef __int64_t __int_least64_t;
typedef __uint64_t __uint_least64_t;
typedef long __quad_t;
typedef unsigned long __u_quad_t;
typedef long __intmax_t;
typedef unsigned long __uintmax_t;
typedef unsigned long __dev_t;
typedef unsigned int __uid_t;
typedef unsigned int __gid_t;
typedef unsigned long __ino_t;
typedef unsigned long __ino64_t;
typedef unsigned int __mode_t;
typedef unsigned long __nlink_t;
typedef long __off_t;
typedef long __off64_t;
typedef int __pid_t;
typedef struct {
    int __val[2];
} __fsid_t;
typedef long __clock_t;
typedef unsigned long __rlim_t;
typedef unsigned long __rlim64_t;
typedef unsigned int __id_t;
typedef long __time_t;
typedef unsigned int __useconds_t;
typedef long __suseconds_t;
typedef int __daddr_t;
typedef int __key_t;
typedef int __clockid_t;
typedef void *__timer_t;
typedef long __blksize_t;
typedef long __blkcnt_t;
typedef long __blkcnt64_t;
typedef unsigned long __fsblkcnt_t;
typedef unsigned long __fsblkcnt64_t;
typedef unsigned long __fsfilcnt_t;
typedef unsigned long __fsfilcnt64_t;
typedef long __fsword_t;
typedef long __ssize_t;
typedef long __syscall_slong_t;
typedef unsigned long __syscall_ulong_t;
typedef __off64_t __loff_t;
typedef char *__caddr_t;
typedef long __intptr_t;
typedef unsigned int __socklen_t;
typedef int __sig_atomic_t;
typedef __int8_t int8_t;
typedef __int16_t int16_t;
typedef __int32_t int32_t;
typedef __int64_t int64_t;
typedef __uint8_t uint8_t;
typedef __uint16_t uint16_t;
typedef __uint32_t uint32_t;
typedef __uint64_t uint64_t;
typedef __int_least8_t int_least8_t;
typedef __int_least16_t int_least16_t;
typedef __int_least32_t int_least32_t;
typedef __int_least64_t int_least64_t;
typedef __uint_least8_t uint_least8_t;
typedef __uint_least16_t uint_least16_t;
typedef __uint_least32_t uint_least32_t;
typedef __uint_least64_t uint_least64_t;
typedef signed char int_fast8_t;
typedef long int_fast16_t;
typedef long int_fast32_t;
typedef long int_fast64_t;
typedef unsigned char uint_fast8_t;
typedef unsigned long uint_fast16_t;
typedef unsigned long uint_fast32_t;
typedef unsigned long uint_fast64_t;
typedef long intptr_t;
typedef unsigned long uintptr_t;
typedef __intmax_t intmax_t;
typedef __uintmax_t uintmax_t;
struct indirect_placeholder_207_ret_type;
struct indirect_placeholder_207_ret_type {
    uint64_t field_0;
    uint64_t field_1;
};
extern uint64_t revng_init_local_sp(uint64_t param_0);
extern uint64_t init_rbp(void);
extern void indirect_placeholder(void);
extern uint64_t indirect_placeholder_1(uint64_t param_0, uint64_t param_1);
extern uint64_t indirect_placeholder_8(uint64_t param_0, uint64_t param_1, uint64_t param_2);
extern uint64_t init_rbx(void);
extern struct indirect_placeholder_207_ret_type indirect_placeholder_207(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint64_t param_3);
extern uint64_t indirect_placeholder_7(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint64_t param_3);
uint64_t bb_check_arrival_add_next_nodes(uint64_t rdx, uint64_t rcx, uint64_t rdi, uint64_t rsi) {
    uint64_t var_0;
    uint64_t var_1;
    uint64_t var_2;
    uint64_t *var_3;
    uint64_t *var_4;
    uint64_t *var_5;
    uint64_t *var_6;
    uint64_t var_7;
    uint64_t var_8;
    uint64_t *var_9;
    uint64_t var_10;
    uint32_t *var_11;
    uint64_t var_12;
    uint64_t *var_13;
    uint32_t *var_14;
    uint64_t *var_15;
    uint64_t *var_16;
    uint64_t *var_17;
    uint64_t *var_18;
    uint64_t *var_19;
    uint64_t var_20;
    unsigned char *var_21;
    uint64_t var_22;
    uint64_t local_sp_4;
    uint64_t local_sp_0;
    uint64_t var_54;
    uint64_t local_sp_3;
    uint64_t var_40;
    uint64_t var_41;
    uint64_t var_42;
    struct indirect_placeholder_207_ret_type var_43;
    uint32_t rax_0_shrunk;
    uint64_t var_31;
    uint64_t var_32;
    uint64_t local_sp_1;
    uint64_t var_33;
    uint64_t var_34;
    uint64_t var_35;
    uint32_t var_36;
    uint64_t var_37;
    uint64_t var_38;
    unsigned char var_39;
    uint64_t local_sp_2;
    uint64_t var_23;
    uint64_t **var_24;
    uint64_t var_25;
    uint64_t var_26;
    uint64_t var_27;
    uint64_t var_28;
    uint64_t var_29;
    uint32_t var_30;
    uint64_t var_44;
    uint64_t var_45;
    uint64_t var_46;
    uint64_t var_47;
    uint64_t var_48;
    uint64_t var_49;
    uint64_t var_50;
    uint64_t var_51;
    uint64_t var_52;
    unsigned char var_53;
    var_0 = revng_init_local_sp(0UL);
    var_1 = init_rbp();
    var_2 = init_rbx();
    *(uint64_t *)(var_0 + (-8L)) = var_1;
    *(uint64_t *)(var_0 + (-16L)) = var_2;
    var_3 = (uint64_t *)(var_0 + (-128L));
    *var_3 = rdi;
    var_4 = (uint64_t *)(var_0 + (-136L));
    *var_4 = rsi;
    var_5 = (uint64_t *)(var_0 + (-144L));
    *var_5 = rdx;
    var_6 = (uint64_t *)(var_0 + (-152L));
    *var_6 = rcx;
    var_7 = *(uint64_t *)(*var_3 + 152UL);
    var_8 = var_0 + (-48L);
    var_9 = (uint64_t *)var_8;
    *var_9 = var_7;
    var_10 = var_0 + (-92L);
    var_11 = (uint32_t *)var_10;
    *var_11 = 0U;
    var_12 = var_0 + (-160L);
    *(uint64_t *)var_12 = 4305789UL;
    indirect_placeholder();
    var_13 = (uint64_t *)(var_0 + (-32L));
    *var_13 = 0UL;
    var_14 = (uint32_t *)(var_0 + (-36L));
    var_15 = (uint64_t *)(var_0 + (-56L));
    var_16 = (uint64_t *)(var_0 + (-72L));
    var_17 = (uint64_t *)(var_0 + (-80L));
    var_18 = (uint64_t *)(var_0 + (-88L));
    var_19 = (uint64_t *)(var_0 + (-112L));
    var_20 = var_0 + (-120L);
    var_21 = (unsigned char *)(var_0 + (-57L));
    rax_0_shrunk = 12U;
    var_22 = 0UL;
    local_sp_2 = var_12;
    while (1U)
        {
            local_sp_3 = local_sp_2;
            if ((long)*(uint64_t *)(*var_5 + 8UL) <= (long)var_22) {
                *(uint64_t *)(local_sp_2 + (-8L)) = 4306403UL;
                indirect_placeholder();
                rax_0_shrunk = 0U;
                break;
            }
            *var_14 = 0U;
            var_23 = *(uint64_t *)(*(uint64_t *)(*var_5 + 16UL) + (*var_13 << 3UL));
            *var_15 = var_23;
            var_24 = (uint64_t **)var_8;
            if ((*(unsigned char *)((**var_24 + (var_23 << 4UL)) + 10UL) & '\x10') == '\x00') {
                local_sp_4 = local_sp_3;
                if (*var_14 != 0U) {
                    var_44 = (*var_15 << 4UL) + **var_24;
                    var_45 = *var_4;
                    var_46 = *var_3;
                    var_47 = local_sp_3 + (-8L);
                    *(uint64_t *)var_47 = 4306284UL;
                    var_48 = indirect_placeholder_8(var_45, var_46, var_44);
                    local_sp_0 = var_47;
                    local_sp_4 = var_47;
                    if ((uint64_t)(unsigned char)var_48 != 0UL) {
                        var_54 = *var_13 + 1UL;
                        *var_13 = var_54;
                        var_22 = var_54;
                        local_sp_2 = local_sp_0;
                        continue;
                    }
                }
                var_49 = *(uint64_t *)(*(uint64_t *)(*var_9 + 24UL) + (*var_15 << 3UL));
                var_50 = *var_6;
                var_51 = local_sp_4 + (-8L);
                *(uint64_t *)var_51 = 4306328UL;
                var_52 = indirect_placeholder_1(var_50, var_49);
                var_53 = (unsigned char)var_52;
                *var_21 = var_53;
                local_sp_0 = var_51;
                if (var_53 == '\x01') {
                    var_54 = *var_13 + 1UL;
                    *var_13 = var_54;
                    var_22 = var_54;
                    local_sp_2 = local_sp_0;
                    continue;
                }
                *(uint64_t *)(local_sp_4 + (-16L)) = 4306358UL;
                indirect_placeholder();
                break;
            }
            var_25 = *var_3;
            var_26 = *var_4;
            var_27 = *var_9;
            var_28 = local_sp_2 + (-8L);
            *(uint64_t *)var_28 = 4305895UL;
            var_29 = indirect_placeholder_7(var_25, var_26, var_27, var_23);
            var_30 = (uint32_t)var_29;
            *var_14 = var_30;
            local_sp_1 = var_28;
            local_sp_3 = var_28;
            if ((int)var_30 <= (int)1U) {
                *var_16 = *(uint64_t *)(*(uint64_t *)(*var_9 + 24UL) + (*var_15 << 3UL));
                var_31 = *var_4 + (uint64_t)*var_14;
                *var_17 = var_31;
                *var_18 = *(uint64_t *)(*(uint64_t *)(*var_3 + 184UL) + (var_31 << 3UL));
                *var_19 = 0UL;
                var_32 = *var_18;
                if (var_32 != 0UL) {
                    var_33 = var_32 + 8UL;
                    var_34 = local_sp_2 + (-16L);
                    *(uint64_t *)var_34 = 4306018UL;
                    var_35 = indirect_placeholder_1(var_20, var_33);
                    var_36 = (uint32_t)var_35;
                    *var_11 = var_36;
                    local_sp_1 = var_34;
                    if (var_36 != 0U) {
                        *(uint64_t *)(local_sp_2 + (-24L)) = 4306049UL;
                        indirect_placeholder();
                        rax_0_shrunk = *var_11;
                        break;
                    }
                }
                var_37 = *var_16;
                *(uint64_t *)(local_sp_1 + (-8L)) = 4306076UL;
                var_38 = indirect_placeholder_1(var_20, var_37);
                var_39 = (unsigned char)var_38;
                *var_21 = var_39;
                if (var_39 != '\x01') {
                    *(uint64_t *)(local_sp_1 + (-16L)) = 4306106UL;
                    indirect_placeholder();
                    break;
                }
                var_40 = (*var_17 << 3UL) + *(uint64_t *)(*var_3 + 184UL);
                var_41 = *var_9;
                var_42 = local_sp_1 + (-16L);
                *(uint64_t *)var_42 = 4306162UL;
                var_43 = indirect_placeholder_207(var_20, var_40, var_10, var_41);
                *(uint64_t *)var_43.field_1 = var_43.field_0;
                local_sp_3 = var_42;
                if (*(uint64_t *)(*(uint64_t *)(*var_3 + 184UL) + (*var_17 << 3UL)) != 0UL & *var_11 != 0U) {
                    *(uint64_t *)(local_sp_1 + (-24L)) = 4306232UL;
                    indirect_placeholder();
                    rax_0_shrunk = *var_11;
                    break;
                }
            }
        }
    return (uint64_t)rax_0_shrunk;
}

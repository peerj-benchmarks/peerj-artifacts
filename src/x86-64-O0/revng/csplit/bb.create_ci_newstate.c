typedef unsigned char __u_char;
typedef unsigned short __u_short;
typedef unsigned int __u_int;
typedef unsigned long __u_long;
typedef signed char __int8_t;
typedef unsigned char __uint8_t;
typedef short __int16_t;
typedef unsigned short __uint16_t;
typedef int __int32_t;
typedef unsigned int __uint32_t;
typedef long __int64_t;
typedef unsigned long __uint64_t;
typedef __int8_t __int_least8_t;
typedef __uint8_t __uint_least8_t;
typedef __int16_t __int_least16_t;
typedef __uint16_t __uint_least16_t;
typedef __int32_t __int_least32_t;
typedef __uint32_t __uint_least32_t;
typedef __int64_t __int_least64_t;
typedef __uint64_t __uint_least64_t;
typedef long __quad_t;
typedef unsigned long __u_quad_t;
typedef long __intmax_t;
typedef unsigned long __uintmax_t;
typedef unsigned long __dev_t;
typedef unsigned int __uid_t;
typedef unsigned int __gid_t;
typedef unsigned long __ino_t;
typedef unsigned long __ino64_t;
typedef unsigned int __mode_t;
typedef unsigned long __nlink_t;
typedef long __off_t;
typedef long __off64_t;
typedef int __pid_t;
typedef struct {
    int __val[2];
} __fsid_t;
typedef long __clock_t;
typedef unsigned long __rlim_t;
typedef unsigned long __rlim64_t;
typedef unsigned int __id_t;
typedef long __time_t;
typedef unsigned int __useconds_t;
typedef long __suseconds_t;
typedef int __daddr_t;
typedef int __key_t;
typedef int __clockid_t;
typedef void *__timer_t;
typedef long __blksize_t;
typedef long __blkcnt_t;
typedef long __blkcnt64_t;
typedef unsigned long __fsblkcnt_t;
typedef unsigned long __fsblkcnt64_t;
typedef unsigned long __fsfilcnt_t;
typedef unsigned long __fsfilcnt64_t;
typedef long __fsword_t;
typedef long __ssize_t;
typedef long __syscall_slong_t;
typedef unsigned long __syscall_ulong_t;
typedef __off64_t __loff_t;
typedef char *__caddr_t;
typedef long __intptr_t;
typedef unsigned int __socklen_t;
typedef int __sig_atomic_t;
typedef __int8_t int8_t;
typedef __int16_t int16_t;
typedef __int32_t int32_t;
typedef __int64_t int64_t;
typedef __uint8_t uint8_t;
typedef __uint16_t uint16_t;
typedef __uint32_t uint32_t;
typedef __uint64_t uint64_t;
typedef __int_least8_t int_least8_t;
typedef __int_least16_t int_least16_t;
typedef __int_least32_t int_least32_t;
typedef __int_least64_t int_least64_t;
typedef __uint_least8_t uint_least8_t;
typedef __uint_least16_t uint_least16_t;
typedef __uint_least32_t uint_least32_t;
typedef __uint_least64_t uint_least64_t;
typedef signed char int_fast8_t;
typedef long int_fast16_t;
typedef long int_fast32_t;
typedef long int_fast64_t;
typedef unsigned char uint_fast8_t;
typedef unsigned long uint_fast16_t;
typedef unsigned long uint_fast32_t;
typedef unsigned long uint_fast64_t;
typedef long intptr_t;
typedef unsigned long uintptr_t;
typedef __intmax_t intmax_t;
typedef __uintmax_t uintmax_t;
struct indirect_placeholder_123_ret_type;
struct indirect_placeholder_123_ret_type {
    uint64_t field_0;
    uint64_t field_1;
    uint64_t field_2;
    uint64_t field_3;
    uint64_t field_4;
    uint64_t field_5;
};
extern uint64_t revng_init_local_sp(uint64_t param_0);
extern uint64_t init_rbp(void);
extern void indirect_placeholder(void);
extern uint64_t indirect_placeholder_1(uint64_t param_0, uint64_t param_1);
extern uint64_t indirect_placeholder_8(uint64_t param_0, uint64_t param_1, uint64_t param_2);
extern struct indirect_placeholder_123_ret_type indirect_placeholder_123(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint64_t param_3, uint64_t param_4, uint64_t param_5, uint64_t param_6);
uint64_t bb_create_ci_newstate(uint64_t rdx, uint64_t rcx, uint64_t r8, uint64_t rbx, uint64_t rdi, uint64_t rsi, uint64_t r9, uint64_t r10) {
    struct indirect_placeholder_123_ret_type var_6;
    uint64_t var_0;
    uint64_t var_1;
    uint64_t var_2;
    uint64_t *var_3;
    uint64_t *var_4;
    uint64_t *var_5;
    uint64_t var_7;
    uint64_t *var_8;
    uint64_t rax_0;
    uint64_t var_29;
    uint64_t var_25;
    uint64_t var_14;
    uint64_t *var_15;
    uint64_t *var_16;
    uint32_t *var_17;
    uint64_t var_18;
    uint64_t var_19;
    uint64_t var_20;
    uint64_t var_21;
    uint64_t var_22;
    uint64_t var_23;
    uint32_t var_24;
    uint64_t var_26;
    uint32_t var_27;
    uint64_t var_28;
    unsigned char *var_30;
    unsigned char var_31;
    unsigned char *var_33;
    unsigned char *var_32;
    unsigned char *var_34;
    uint64_t var_35;
    uint64_t var_9;
    uint64_t var_10;
    uint64_t var_11;
    uint32_t *var_12;
    uint32_t var_13;
    var_0 = revng_init_local_sp(0UL);
    var_1 = init_rbp();
    *(uint64_t *)(var_0 + (-8L)) = var_1;
    var_2 = var_0 + (-64L);
    var_3 = (uint64_t *)var_2;
    *var_3 = rdi;
    var_4 = (uint64_t *)(var_0 + (-72L));
    *var_4 = rsi;
    var_5 = (uint64_t *)(var_0 + (-80L));
    *var_5 = rdx;
    *(uint64_t *)(var_0 + (-96L)) = 4249746UL;
    var_6 = indirect_placeholder_123(rcx, r8, rbx, 112UL, 1UL, r9, r10);
    var_7 = var_6.field_0;
    var_8 = (uint64_t *)(var_0 + (-24L));
    *var_8 = var_7;
    var_25 = 0UL;
    var_18 = 0UL;
    rax_0 = 0UL;
    if (var_7 == 0UL) {
        return rax_0;
    }
    var_9 = var_7 + 8UL;
    var_10 = *var_4;
    *(uint64_t *)(var_0 + (-104L)) = 4249799UL;
    var_11 = indirect_placeholder_1(var_9, var_10);
    var_12 = (uint32_t *)(var_0 + (-28L));
    var_13 = (uint32_t)var_11;
    *var_12 = var_13;
    if (var_13 == 0U) {
        *(uint64_t *)(var_0 + (-112L)) = 4249829UL;
        indirect_placeholder();
    } else {
        var_14 = *var_8;
        *(uint64_t *)(var_14 + 80UL) = (var_14 + 8UL);
        var_15 = (uint64_t *)(var_0 + (-16L));
        *var_15 = 0UL;
        var_16 = (uint64_t *)(var_0 + (-40L));
        var_17 = (uint32_t *)(var_0 + (-44L));
        var_19 = *var_4;
        while ((long)*(uint64_t *)(var_19 + 8UL) <= (long)var_18)
            {
                var_26 = (*(uint64_t *)(*(uint64_t *)(var_19 + 16UL) + (var_18 << 3UL)) << 4UL) + **(uint64_t **)var_2;
                *var_16 = var_26;
                var_27 = (uint32_t)*(unsigned char *)(var_26 + 8UL);
                *var_17 = var_27;
                if (var_27 == 1U) {
                    var_28 = *var_16;
                    var_29 = var_28;
                    if ((*(uint32_t *)(var_28 + 8UL) & 261888U) != 0U) {
                        var_35 = *var_15 + 1UL;
                        *var_15 = var_35;
                        var_18 = var_35;
                        var_19 = *var_4;
                        continue;
                    }
                }
                var_29 = *var_16;
                var_30 = (unsigned char *)(*var_8 + 104UL);
                var_31 = *var_30;
                *var_30 = (((var_31 | (*(unsigned char *)(var_29 + 10UL) << '\x01')) & ' ') | (var_31 & '\xdf'));
                switch (*var_17) {
                  case 2U:
                    {
                        var_33 = (unsigned char *)(*var_8 + 104UL);
                        *var_33 = (*var_33 | '\x10');
                    }
                    break;
                  case 4U:
                    {
                        var_32 = (unsigned char *)(*var_8 + 104UL);
                        *var_32 = (*var_32 | '@');
                    }
                    break;
                  case 12U:
                    {
                        var_34 = (unsigned char *)(*var_8 + 104UL);
                        *var_34 = (*var_34 | '\x80');
                    }
                    break;
                  default:
                    {
                        if ((*(uint32_t *)(*var_16 + 8UL) & 261888U) != 0U) {
                            var_35 = *var_15 + 1UL;
                            *var_15 = var_35;
                            var_18 = var_35;
                            var_19 = *var_4;
                            continue;
                        }
                        var_34 = (unsigned char *)(*var_8 + 104UL);
                        *var_34 = (*var_34 | '\x80');
                    }
                    break;
                }
                var_35 = *var_15 + 1UL;
                *var_15 = var_35;
                var_18 = var_35;
                var_19 = *var_4;
            }
        var_20 = *var_5;
        var_21 = *var_8;
        var_22 = *var_3;
        *(uint64_t *)(var_0 + (-112L)) = 4250136UL;
        var_23 = indirect_placeholder_8(var_20, var_22, var_21);
        var_24 = (uint32_t)var_23;
        *var_12 = var_24;
        if (var_24 == 0U) {
            var_25 = *var_8;
        } else {
            *(uint64_t *)(var_0 + (-120L)) = 4250166UL;
            indirect_placeholder();
            *var_8 = 0UL;
        }
        rax_0 = var_25;
    }
    return rax_0;
}

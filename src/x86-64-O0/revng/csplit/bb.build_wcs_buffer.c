typedef unsigned char __u_char;
typedef unsigned short __u_short;
typedef unsigned int __u_int;
typedef unsigned long __u_long;
typedef signed char __int8_t;
typedef unsigned char __uint8_t;
typedef short __int16_t;
typedef unsigned short __uint16_t;
typedef int __int32_t;
typedef unsigned int __uint32_t;
typedef long __int64_t;
typedef unsigned long __uint64_t;
typedef __int8_t __int_least8_t;
typedef __uint8_t __uint_least8_t;
typedef __int16_t __int_least16_t;
typedef __uint16_t __uint_least16_t;
typedef __int32_t __int_least32_t;
typedef __uint32_t __uint_least32_t;
typedef __int64_t __int_least64_t;
typedef __uint64_t __uint_least64_t;
typedef long __quad_t;
typedef unsigned long __u_quad_t;
typedef long __intmax_t;
typedef unsigned long __uintmax_t;
typedef unsigned long __dev_t;
typedef unsigned int __uid_t;
typedef unsigned int __gid_t;
typedef unsigned long __ino_t;
typedef unsigned long __ino64_t;
typedef unsigned int __mode_t;
typedef unsigned long __nlink_t;
typedef long __off_t;
typedef long __off64_t;
typedef int __pid_t;
typedef struct {
    int __val[2];
} __fsid_t;
typedef long __clock_t;
typedef unsigned long __rlim_t;
typedef unsigned long __rlim64_t;
typedef unsigned int __id_t;
typedef long __time_t;
typedef unsigned int __useconds_t;
typedef long __suseconds_t;
typedef int __daddr_t;
typedef int __key_t;
typedef int __clockid_t;
typedef void *__timer_t;
typedef long __blksize_t;
typedef long __blkcnt_t;
typedef long __blkcnt64_t;
typedef unsigned long __fsblkcnt_t;
typedef unsigned long __fsblkcnt64_t;
typedef unsigned long __fsfilcnt_t;
typedef unsigned long __fsfilcnt64_t;
typedef long __fsword_t;
typedef long __ssize_t;
typedef long __syscall_slong_t;
typedef unsigned long __syscall_ulong_t;
typedef __off64_t __loff_t;
typedef char *__caddr_t;
typedef long __intptr_t;
typedef unsigned int __socklen_t;
typedef int __sig_atomic_t;
typedef __int8_t int8_t;
typedef __int16_t int16_t;
typedef __int32_t int32_t;
typedef __int64_t int64_t;
typedef __uint8_t uint8_t;
typedef __uint16_t uint16_t;
typedef __uint32_t uint32_t;
typedef __uint64_t uint64_t;
typedef __int_least8_t int_least8_t;
typedef __int_least16_t int_least16_t;
typedef __int_least32_t int_least32_t;
typedef __int_least64_t int_least64_t;
typedef __uint_least8_t uint_least8_t;
typedef __uint_least16_t uint_least16_t;
typedef __uint_least32_t uint_least32_t;
typedef __uint_least64_t uint_least64_t;
typedef signed char int_fast8_t;
typedef long int_fast16_t;
typedef long int_fast32_t;
typedef long int_fast64_t;
typedef unsigned char uint_fast8_t;
typedef unsigned long uint_fast16_t;
typedef unsigned long uint_fast32_t;
typedef unsigned long uint_fast64_t;
typedef long intptr_t;
typedef unsigned long uintptr_t;
typedef __intmax_t intmax_t;
typedef __uintmax_t uintmax_t;
extern uint64_t revng_init_local_sp(uint64_t param_0);
extern uint64_t init_rbp(void);
extern uint64_t indirect_placeholder_8(uint64_t param_0, uint64_t param_1, uint64_t param_2);
typedef _Bool bool;
void bb_build_wcs_buffer(uint64_t rdi) {
    uint64_t var_0;
    uint64_t var_1;
    uint64_t var_2;
    uint64_t var_3;
    uint64_t var_4;
    uint64_t *var_5;
    uint64_t var_6;
    uint64_t var_7;
    uint64_t var_8;
    uint64_t *var_9;
    uint64_t var_10;
    uint64_t *var_11;
    uint64_t *var_12;
    uint64_t *var_13;
    uint64_t *var_14;
    uint32_t *var_15;
    uint32_t *var_16;
    uint64_t var_17;
    uint64_t var_18;
    uint64_t *var_19;
    uint32_t *var_20;
    uint64_t var_21;
    uint64_t var_37;
    uint64_t var_48;
    uint32_t var_38;
    uint64_t var_39;
    uint64_t var_40;
    uint64_t var_41;
    uint64_t var_33;
    uint32_t var_24;
    uint64_t var_42;
    uint64_t var_43;
    uint64_t var_44;
    uint64_t local_sp_0;
    uint64_t var_45;
    uint64_t var_46;
    uint64_t var_47;
    uint64_t var_22;
    uint64_t var_23;
    uint64_t var_25;
    uint64_t var_26;
    uint32_t var_27;
    uint64_t var_28;
    uint64_t var_29;
    unsigned char var_30;
    uint32_t var_31;
    uint64_t var_32;
    uint64_t var_34;
    uint64_t var_35;
    uint64_t var_36;
    bool switch_state_var;
    var_0 = revng_init_local_sp(0UL);
    var_1 = init_rbp();
    var_2 = var_0 + (-8L);
    *(uint64_t *)var_2 = var_1;
    var_3 = var_0 + (-184L);
    var_4 = var_0 + (-176L);
    var_5 = (uint64_t *)var_4;
    *var_5 = rdi;
    var_6 = *(uint64_t *)(rdi + 64UL);
    var_7 = *(uint64_t *)(rdi + 88UL);
    var_8 = ((long)var_6 > (long)var_7) ? var_7 : var_6;
    var_9 = (uint64_t *)(var_0 + (-48L));
    *var_9 = var_8;
    var_10 = *(uint64_t *)(*var_5 + 48UL);
    var_11 = (uint64_t *)(var_0 + (-16L));
    *var_11 = var_10;
    var_12 = (uint64_t *)(var_0 + (-56L));
    var_13 = (uint64_t *)(var_0 + (-152L));
    var_14 = (uint64_t *)(var_0 + (-32L));
    var_15 = (uint32_t *)(var_0 + (-36L));
    var_16 = (uint32_t *)(var_0 + (-60L));
    var_17 = var_0 + (-136L);
    var_18 = var_0 + (-156L);
    var_19 = (uint64_t *)(var_0 + (-24L));
    var_20 = (uint32_t *)var_18;
    var_21 = var_10;
    local_sp_0 = var_3;
    var_24 = 0U;
    var_33 = var_17;
    var_22 = *var_9;
    var_48 = var_21;
    while ((long)var_21 >= (long)var_22)
        {
            *var_12 = (var_22 - var_21);
            *var_13 = *(uint64_t *)(*var_5 + 32UL);
            var_23 = *var_5;
            if (*(uint64_t *)(var_23 + 120UL) == 0UL) {
                var_32 = (*var_11 + *(uint64_t *)(var_23 + 40UL)) + **(uint64_t **)var_4;
                *var_14 = var_32;
                var_33 = var_32;
            } else {
                *var_15 = 0U;
                var_25 = *var_5;
                while ((long)((uint64_t)*(uint32_t *)(var_25 + 144UL) << 32UL) <= (long)((uint64_t)var_24 << 32UL))
                    {
                        var_26 = (uint64_t)var_24;
                        if ((long)*var_12 > (long)var_26) {
                            break;
                        }
                        var_27 = (uint32_t)*(unsigned char *)(((*(uint64_t *)(var_25 + 40UL) + *var_11) + var_26) + **(uint64_t **)var_4);
                        *var_16 = var_27;
                        var_28 = *var_5;
                        var_29 = *(uint64_t *)(var_28 + 8UL) + (*var_11 + (uint64_t)*var_15);
                        var_30 = *(unsigned char *)(*(uint64_t *)(var_28 + 120UL) + (uint64_t)var_27);
                        *(unsigned char *)var_29 = var_30;
                        *(unsigned char *)((var_2 + (uint64_t)*var_15) + (-128L)) = var_30;
                        var_31 = *var_15 + 1U;
                        *var_15 = var_31;
                        var_24 = var_31;
                        var_25 = *var_5;
                    }
                *var_14 = var_17;
            }
            var_34 = *var_12;
            var_35 = local_sp_0 + (-8L);
            *(uint64_t *)var_35 = 4234502UL;
            var_36 = indirect_placeholder_8(var_34, var_18, var_33);
            *var_19 = var_36;
            local_sp_0 = var_35;
            switch_state_var = 0;
            switch (var_36) {
              case 18446744073709551615UL:
              case 0UL:
                {
                    *var_19 = 1UL;
                    var_38 = (uint32_t)*(unsigned char *)((*var_11 + *(uint64_t *)(*var_5 + 40UL)) + **(uint64_t **)var_4);
                    *var_20 = var_38;
                    var_39 = *var_5;
                    var_40 = *(uint64_t *)(var_39 + 120UL);
                    var_41 = var_39;
                    if (var_40 != 0UL) {
                        *var_20 = (uint32_t)*(unsigned char *)(var_40 + (uint64_t)var_38);
                        var_41 = *var_5;
                    }
                    *(uint64_t *)(var_41 + 32UL) = *var_13;
                }
                break;
              case 18446744073709551614UL:
                {
                    var_37 = *var_5;
                    if ((long)*(uint64_t *)(var_37 + 64UL) < (long)*(uint64_t *)(var_37 + 88UL)) {
                        if (var_36 != 18446744073709551614UL) {
                            *(uint64_t *)(*var_5 + 32UL) = *var_13;
                            var_48 = *var_11;
                            switch_state_var = 1;
                            break;
                        }
                        var_42 = *(uint64_t *)(*var_5 + 16UL);
                        var_43 = *var_11;
                        *var_11 = (var_43 + 1UL);
                        *(uint32_t *)((var_43 << 2UL) + var_42) = *var_20;
                        var_44 = (*var_19 + *var_11) + (-1L);
                        *var_12 = var_44;
                        var_45 = var_44;
                        var_46 = *var_11;
                        var_21 = var_46;
                        while ((long)var_46 >= (long)var_45)
                            {
                                var_47 = *(uint64_t *)(*var_5 + 16UL);
                                *var_11 = (var_46 + 1UL);
                                *(uint32_t *)((var_46 << 2UL) + var_47) = 4294967295U;
                                var_45 = *var_12;
                                var_46 = *var_11;
                                var_21 = var_46;
                            }
                        var_22 = *var_9;
                        var_48 = var_21;
                        continue;
                    }
                    *var_19 = 1UL;
                    var_38 = (uint32_t)*(unsigned char *)((*var_11 + *(uint64_t *)(*var_5 + 40UL)) + **(uint64_t **)var_4);
                    *var_20 = var_38;
                    var_39 = *var_5;
                    var_40 = *(uint64_t *)(var_39 + 120UL);
                    var_41 = var_39;
                    if (var_40 == 0UL) {
                        *var_20 = (uint32_t)*(unsigned char *)(var_40 + (uint64_t)var_38);
                        var_41 = *var_5;
                    }
                    *(uint64_t *)(var_41 + 32UL) = *var_13;
                }
                break;
              default:
                {
                    if (var_36 != 18446744073709551614UL) {
                        *(uint64_t *)(*var_5 + 32UL) = *var_13;
                        var_48 = *var_11;
                        switch_state_var = 1;
                        break;
                    }
                }
                break;
            }
            if (switch_state_var)
                break;
            var_42 = *(uint64_t *)(*var_5 + 16UL);
            var_43 = *var_11;
            *var_11 = (var_43 + 1UL);
            *(uint32_t *)((var_43 << 2UL) + var_42) = *var_20;
            var_44 = (*var_19 + *var_11) + (-1L);
            *var_12 = var_44;
            var_45 = var_44;
            var_46 = *var_11;
            var_21 = var_46;
            while ((long)var_46 >= (long)var_45)
                {
                    var_47 = *(uint64_t *)(*var_5 + 16UL);
                    *var_11 = (var_46 + 1UL);
                    *(uint32_t *)((var_46 << 2UL) + var_47) = 4294967295U;
                    var_45 = *var_12;
                    var_46 = *var_11;
                    var_21 = var_46;
                }
            var_22 = *var_9;
            var_48 = var_21;
        }
    *(uint64_t *)(*var_5 + 48UL) = var_48;
    *(uint64_t *)(*var_5 + 56UL) = *var_11;
    return;
}

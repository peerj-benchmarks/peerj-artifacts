typedef unsigned char __u_char;
typedef unsigned short __u_short;
typedef unsigned int __u_int;
typedef unsigned long __u_long;
typedef signed char __int8_t;
typedef unsigned char __uint8_t;
typedef short __int16_t;
typedef unsigned short __uint16_t;
typedef int __int32_t;
typedef unsigned int __uint32_t;
typedef long __int64_t;
typedef unsigned long __uint64_t;
typedef __int8_t __int_least8_t;
typedef __uint8_t __uint_least8_t;
typedef __int16_t __int_least16_t;
typedef __uint16_t __uint_least16_t;
typedef __int32_t __int_least32_t;
typedef __uint32_t __uint_least32_t;
typedef __int64_t __int_least64_t;
typedef __uint64_t __uint_least64_t;
typedef long __quad_t;
typedef unsigned long __u_quad_t;
typedef long __intmax_t;
typedef unsigned long __uintmax_t;
typedef unsigned long __dev_t;
typedef unsigned int __uid_t;
typedef unsigned int __gid_t;
typedef unsigned long __ino_t;
typedef unsigned long __ino64_t;
typedef unsigned int __mode_t;
typedef unsigned long __nlink_t;
typedef long __off_t;
typedef long __off64_t;
typedef int __pid_t;
typedef struct {
    int __val[2];
} __fsid_t;
typedef long __clock_t;
typedef unsigned long __rlim_t;
typedef unsigned long __rlim64_t;
typedef unsigned int __id_t;
typedef long __time_t;
typedef unsigned int __useconds_t;
typedef long __suseconds_t;
typedef int __daddr_t;
typedef int __key_t;
typedef int __clockid_t;
typedef void *__timer_t;
typedef long __blksize_t;
typedef long __blkcnt_t;
typedef long __blkcnt64_t;
typedef unsigned long __fsblkcnt_t;
typedef unsigned long __fsblkcnt64_t;
typedef unsigned long __fsfilcnt_t;
typedef unsigned long __fsfilcnt64_t;
typedef long __fsword_t;
typedef long __ssize_t;
typedef long __syscall_slong_t;
typedef unsigned long __syscall_ulong_t;
typedef __off64_t __loff_t;
typedef char *__caddr_t;
typedef long __intptr_t;
typedef unsigned int __socklen_t;
typedef int __sig_atomic_t;
typedef __int8_t int8_t;
typedef __int16_t int16_t;
typedef __int32_t int32_t;
typedef __int64_t int64_t;
typedef __uint8_t uint8_t;
typedef __uint16_t uint16_t;
typedef __uint32_t uint32_t;
typedef __uint64_t uint64_t;
typedef __int_least8_t int_least8_t;
typedef __int_least16_t int_least16_t;
typedef __int_least32_t int_least32_t;
typedef __int_least64_t int_least64_t;
typedef __uint_least8_t uint_least8_t;
typedef __uint_least16_t uint_least16_t;
typedef __uint_least32_t uint_least32_t;
typedef __uint_least64_t uint_least64_t;
typedef signed char int_fast8_t;
typedef long int_fast16_t;
typedef long int_fast32_t;
typedef long int_fast64_t;
typedef unsigned char uint_fast8_t;
typedef unsigned long uint_fast16_t;
typedef unsigned long uint_fast32_t;
typedef unsigned long uint_fast64_t;
typedef long intptr_t;
typedef unsigned long uintptr_t;
typedef __intmax_t intmax_t;
typedef __uintmax_t uintmax_t;
extern uint64_t revng_init_local_sp(uint64_t param_0);
extern uint64_t init_rbp(void);
extern uint64_t indirect_placeholder_1(uint64_t param_0, uint64_t param_1);
extern uint64_t indirect_placeholder_7(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint64_t param_3);
typedef _Bool bool;
uint64_t bb_check_subexp_limits(uint64_t rdx, uint64_t rcx, uint64_t r8, uint64_t rdi, uint64_t rsi, uint64_t r9) {
    uint64_t var_0;
    uint64_t var_1;
    uint64_t var_2;
    uint64_t var_3;
    uint64_t *var_4;
    uint64_t *var_5;
    uint64_t *var_6;
    uint64_t *var_7;
    uint64_t *var_8;
    uint64_t *var_9;
    uint64_t *var_10;
    uint64_t var_11;
    uint64_t *var_12;
    uint64_t *var_13;
    uint64_t *var_14;
    uint64_t *var_15;
    uint32_t *var_16;
    uint32_t *var_17;
    uint64_t *var_18;
    uint64_t *var_19;
    uint64_t *var_20;
    uint32_t *var_21;
    uint64_t *var_22;
    uint64_t var_61;
    uint64_t var_62;
    uint64_t var_63;
    uint64_t var_64;
    uint64_t var_65;
    uint64_t var_66;
    uint32_t var_67;
    uint64_t var_57;
    uint64_t var_58;
    uint64_t var_59;
    uint64_t var_60;
    uint64_t local_sp_0;
    uint64_t var_68;
    uint64_t var_50;
    uint64_t local_sp_7;
    uint64_t var_23;
    uint64_t local_sp_1;
    uint64_t var_24;
    uint32_t rax_0_shrunk;
    uint64_t var_25;
    uint64_t var_26;
    uint64_t var_27;
    uint64_t var_30;
    uint64_t **var_28;
    uint64_t var_42;
    uint64_t var_43;
    uint64_t var_44;
    uint64_t local_sp_2;
    uint64_t var_45;
    uint64_t var_46;
    uint64_t var_47;
    uint64_t var_48;
    uint32_t var_49;
    uint64_t local_sp_3;
    uint64_t var_51;
    uint64_t var_52;
    uint64_t var_53;
    uint64_t var_54;
    uint64_t var_55;
    uint64_t var_56;
    uint64_t var_69;
    uint32_t var_70;
    uint64_t var_71;
    uint64_t var_72;
    uint64_t var_73;
    uint64_t var_74;
    uint64_t local_sp_6;
    uint64_t var_75;
    uint64_t var_29;
    uint64_t local_sp_5;
    uint64_t var_31;
    uint32_t var_32;
    uint64_t var_33;
    uint64_t var_34;
    uint64_t var_41;
    uint64_t var_35;
    uint64_t var_36;
    uint64_t var_37;
    uint64_t var_38;
    uint64_t var_39;
    uint32_t var_40;
    uint64_t var_76;
    unsigned int loop_state_var;
    bool switch_state_var;
    var_0 = revng_init_local_sp(0UL);
    var_1 = init_rbp();
    *(uint64_t *)(var_0 + (-8L)) = var_1;
    var_2 = var_0 + (-152L);
    var_3 = var_0 + (-112L);
    var_4 = (uint64_t *)var_3;
    *var_4 = rdi;
    var_5 = (uint64_t *)(var_0 + (-120L));
    *var_5 = rsi;
    var_6 = (uint64_t *)(var_0 + (-128L));
    *var_6 = rdx;
    var_7 = (uint64_t *)(var_0 + (-136L));
    *var_7 = rcx;
    var_8 = (uint64_t *)(var_0 + (-144L));
    *var_8 = r8;
    var_9 = (uint64_t *)var_2;
    *var_9 = r9;
    var_10 = (uint64_t *)(var_0 + (-24L));
    *var_10 = 0UL;
    var_11 = var_0 + (-48L);
    var_12 = (uint64_t *)var_11;
    var_13 = (uint64_t *)(var_0 + (-56L));
    var_14 = (uint64_t *)(var_0 + (-16L));
    var_15 = (uint64_t *)(var_0 + (-64L));
    var_16 = (uint32_t *)(var_0 + (-68L));
    var_17 = (uint32_t *)(var_0 + (-72L));
    var_18 = (uint64_t *)(var_0 + (-32L));
    var_19 = (uint64_t *)(var_0 + (-40L));
    var_20 = (uint64_t *)(var_0 + (-80L));
    var_21 = (uint32_t *)(var_0 + (-84L));
    var_22 = (uint64_t *)(var_0 + (-96L));
    var_50 = 0UL;
    var_23 = 0UL;
    local_sp_1 = var_2;
    rax_0_shrunk = 0U;
    var_42 = 0UL;
    var_29 = 0UL;
    var_24 = *var_7;
    local_sp_2 = local_sp_1;
    local_sp_5 = local_sp_1;
    local_sp_7 = local_sp_1;
    while ((long)*(uint64_t *)(var_24 + 8UL) <= (long)var_23)
        {
            var_25 = *var_8 + (*(uint64_t *)(*(uint64_t *)(var_24 + 16UL) + (var_23 << 3UL)) * 40UL);
            *var_12 = var_25;
            var_26 = *(uint64_t *)(var_25 + 16UL);
            var_27 = *var_9;
            if ((long)var_26 < (long)var_27) {
                var_76 = *var_10 + 1UL;
                *var_10 = var_76;
                var_23 = var_76;
                local_sp_1 = local_sp_7;
                var_24 = *var_7;
                local_sp_2 = local_sp_1;
                local_sp_5 = local_sp_1;
                local_sp_7 = local_sp_1;
                continue;
            }
            if ((long)*(uint64_t *)(var_25 + 8UL) >= (long)var_27) {
                var_28 = (uint64_t **)var_3;
                *var_13 = *(uint64_t *)((**(uint64_t **)var_11 << 4UL) + **var_28);
                if (*(uint64_t *)(*var_12 + 24UL) != *var_9) {
                    *var_14 = 0UL;
                    while (1U)
                        {
                            var_30 = *var_5;
                            local_sp_6 = local_sp_5;
                            local_sp_7 = local_sp_5;
                            if ((long)*(uint64_t *)(var_30 + 8UL) <= (long)var_29) {
                                loop_state_var = 1U;
                                break;
                            }
                            var_31 = *(uint64_t *)(*(uint64_t *)(var_30 + 16UL) + (var_29 << 3UL));
                            *var_15 = var_31;
                            var_32 = (uint32_t)*(unsigned char *)((**var_28 + (var_31 << 4UL)) + 8UL);
                            *var_16 = var_32;
                            if ((var_32 + (-8)) >= 2U) {
                                var_33 = **var_28;
                                var_34 = *var_15;
                                var_35 = *var_6;
                                var_36 = *var_5;
                                var_37 = *var_4;
                                var_38 = local_sp_5 + (-8L);
                                *(uint64_t *)var_38 = 4296844UL;
                                var_39 = indirect_placeholder_7(var_36, var_35, var_37, var_34);
                                var_40 = (uint32_t)var_39;
                                *var_17 = var_40;
                                rax_0_shrunk = var_40;
                                local_sp_6 = var_38;
                                if (*(uint64_t *)(var_33 + (var_34 << 4UL)) != *var_13 & var_40 != 0U) {
                                    loop_state_var = 0U;
                                    break;
                                }
                            }
                            var_41 = *var_14 + 1UL;
                            *var_14 = var_41;
                            var_29 = var_41;
                            local_sp_5 = local_sp_6;
                            continue;
                        }
                    switch_state_var = 1;
                    break;
                }
                *var_18 = 18446744073709551615UL;
                *var_19 = 18446744073709551615UL;
                *var_14 = 0UL;
                var_43 = *var_5;
                while ((long)*(uint64_t *)(var_43 + 8UL) <= (long)var_42)
                    {
                        var_69 = *(uint64_t *)(*(uint64_t *)(var_43 + 16UL) + (var_42 << 3UL));
                        *var_20 = var_69;
                        var_70 = (uint32_t)*(unsigned char *)((**var_28 + (var_69 << 4UL)) + 8UL);
                        *var_21 = var_70;
                        if (var_70 != 8U) {
                            var_71 = **var_28;
                            var_72 = *var_20;
                            if (*(uint64_t *)(var_71 + (var_72 << 4UL)) != *var_13) {
                                *var_18 = var_72;
                                var_75 = *var_14 + 1UL;
                                *var_14 = var_75;
                                var_42 = var_75;
                                var_43 = *var_5;
                                continue;
                            }
                        }
                        var_73 = **var_28;
                        var_74 = *var_20;
                        if (var_70 != 9U & *(uint64_t *)(var_73 + (var_74 << 4UL)) == *var_13) {
                            *var_19 = var_74;
                        }
                        var_75 = *var_14 + 1UL;
                        *var_14 = var_75;
                        var_42 = var_75;
                        var_43 = *var_5;
                    }
                var_44 = *var_18;
                if ((long)var_44 >= (long)0UL) {
                    var_45 = *var_6;
                    var_46 = *var_4;
                    var_47 = local_sp_1 + (-8L);
                    *(uint64_t *)var_47 = 4296455UL;
                    var_48 = indirect_placeholder_7(var_43, var_45, var_46, var_44);
                    var_49 = (uint32_t)var_48;
                    *var_17 = var_49;
                    rax_0_shrunk = var_49;
                    local_sp_2 = var_47;
                    if (var_49 == 0U) {
                        break;
                    }
                }
                local_sp_3 = local_sp_2;
                local_sp_7 = local_sp_2;
                if ((long)*var_19 >= (long)0UL) {
                    *var_14 = 0UL;
                    while (1U)
                        {
                            var_51 = *var_5;
                            local_sp_7 = local_sp_3;
                            if ((long)*(uint64_t *)(var_51 + 8UL) <= (long)var_50) {
                                loop_state_var = 1U;
                                break;
                            }
                            var_52 = *(uint64_t *)(*(uint64_t *)(var_51 + 16UL) + (var_50 << 3UL));
                            *var_22 = var_52;
                            var_53 = (var_52 * 24UL) + *(uint64_t *)(*var_4 + 56UL);
                            var_54 = *var_19;
                            var_55 = local_sp_3 + (-8L);
                            *(uint64_t *)var_55 = 4296575UL;
                            var_56 = indirect_placeholder_1(var_53, var_54);
                            local_sp_0 = var_55;
                            var_57 = (*var_22 * 24UL) + *(uint64_t *)(*var_4 + 48UL);
                            var_58 = *var_19;
                            var_59 = local_sp_3 + (-16L);
                            *(uint64_t *)var_59 = 4296624UL;
                            var_60 = indirect_placeholder_1(var_57, var_58);
                            local_sp_0 = var_59;
                            if (var_56 != 0UL & var_60 != 0UL) {
                                var_61 = *var_6;
                                var_62 = *var_5;
                                var_63 = *var_22;
                                var_64 = *var_4;
                                var_65 = local_sp_3 + (-24L);
                                *(uint64_t *)var_65 = 4296653UL;
                                var_66 = indirect_placeholder_7(var_62, var_61, var_64, var_63);
                                var_67 = (uint32_t)var_66;
                                *var_17 = var_67;
                                local_sp_0 = var_65;
                                rax_0_shrunk = var_67;
                                if (var_67 != 0U) {
                                    loop_state_var = 0U;
                                    break;
                                }
                                *var_14 = (*var_14 + (-1L));
                            }
                            var_68 = *var_14 + 1UL;
                            *var_14 = var_68;
                            var_50 = var_68;
                            local_sp_3 = local_sp_0;
                            continue;
                        }
                    switch_state_var = 0;
                    switch (loop_state_var) {
                      case 0U:
                        {
                            switch_state_var = 1;
                            break;
                        }
                        break;
                      case 1U:
                        {
                            break;
                        }
                        break;
                    }
                    if (switch_state_var)
                        break;
                }
            }
        }
    return (uint64_t)rax_0_shrunk;
}

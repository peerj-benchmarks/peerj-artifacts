typedef unsigned char __u_char;
typedef unsigned short __u_short;
typedef unsigned int __u_int;
typedef unsigned long __u_long;
typedef signed char __int8_t;
typedef unsigned char __uint8_t;
typedef short __int16_t;
typedef unsigned short __uint16_t;
typedef int __int32_t;
typedef unsigned int __uint32_t;
typedef long __int64_t;
typedef unsigned long __uint64_t;
typedef __int8_t __int_least8_t;
typedef __uint8_t __uint_least8_t;
typedef __int16_t __int_least16_t;
typedef __uint16_t __uint_least16_t;
typedef __int32_t __int_least32_t;
typedef __uint32_t __uint_least32_t;
typedef __int64_t __int_least64_t;
typedef __uint64_t __uint_least64_t;
typedef long __quad_t;
typedef unsigned long __u_quad_t;
typedef long __intmax_t;
typedef unsigned long __uintmax_t;
typedef unsigned long __dev_t;
typedef unsigned int __uid_t;
typedef unsigned int __gid_t;
typedef unsigned long __ino_t;
typedef unsigned long __ino64_t;
typedef unsigned int __mode_t;
typedef unsigned long __nlink_t;
typedef long __off_t;
typedef long __off64_t;
typedef int __pid_t;
typedef struct {
    int __val[2];
} __fsid_t;
typedef long __clock_t;
typedef unsigned long __rlim_t;
typedef unsigned long __rlim64_t;
typedef unsigned int __id_t;
typedef long __time_t;
typedef unsigned int __useconds_t;
typedef long __suseconds_t;
typedef int __daddr_t;
typedef int __key_t;
typedef int __clockid_t;
typedef void *__timer_t;
typedef long __blksize_t;
typedef long __blkcnt_t;
typedef long __blkcnt64_t;
typedef unsigned long __fsblkcnt_t;
typedef unsigned long __fsblkcnt64_t;
typedef unsigned long __fsfilcnt_t;
typedef unsigned long __fsfilcnt64_t;
typedef long __fsword_t;
typedef long __ssize_t;
typedef long __syscall_slong_t;
typedef unsigned long __syscall_ulong_t;
typedef __off64_t __loff_t;
typedef char *__caddr_t;
typedef long __intptr_t;
typedef unsigned int __socklen_t;
typedef int __sig_atomic_t;
typedef __int8_t int8_t;
typedef __int16_t int16_t;
typedef __int32_t int32_t;
typedef __int64_t int64_t;
typedef __uint8_t uint8_t;
typedef __uint16_t uint16_t;
typedef __uint32_t uint32_t;
typedef __uint64_t uint64_t;
typedef __int_least8_t int_least8_t;
typedef __int_least16_t int_least16_t;
typedef __int_least32_t int_least32_t;
typedef __int_least64_t int_least64_t;
typedef __uint_least8_t uint_least8_t;
typedef __uint_least16_t uint_least16_t;
typedef __uint_least32_t uint_least32_t;
typedef __uint_least64_t uint_least64_t;
typedef signed char int_fast8_t;
typedef long int_fast16_t;
typedef long int_fast32_t;
typedef long int_fast64_t;
typedef unsigned char uint_fast8_t;
typedef unsigned long uint_fast16_t;
typedef unsigned long uint_fast32_t;
typedef unsigned long uint_fast64_t;
typedef long intptr_t;
typedef unsigned long uintptr_t;
typedef __intmax_t intmax_t;
typedef __uintmax_t uintmax_t;
typedef _Bool bool;
extern uint64_t revng_init_local_sp(uint64_t param_0);
extern uint64_t init_rbp(void);
extern void indirect_placeholder(void);
extern uint64_t indirect_placeholder_1(uint64_t param_0, uint64_t param_1);
extern uint64_t indirect_placeholder_8(uint64_t param_0, uint64_t param_1, uint64_t param_2);
extern uint64_t indirect_placeholder_7(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint64_t param_3);
extern uint64_t indirect_placeholder_10(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint64_t param_3, uint64_t param_4, uint64_t param_5);
uint64_t bb_proceed_next_node(uint64_t rdx, uint64_t rcx, uint64_t r8, uint64_t rdi, uint64_t rsi, uint64_t r9) {
    uint64_t var_0;
    uint64_t var_1;
    uint64_t var_2;
    uint64_t *var_3;
    uint64_t *var_4;
    uint64_t *var_5;
    uint64_t var_6;
    uint64_t *var_7;
    uint64_t *var_8;
    uint64_t var_9;
    uint64_t var_10;
    uint64_t *var_11;
    uint64_t **var_12;
    uint64_t var_76;
    uint64_t rax_0;
    uint64_t var_71;
    uint64_t *var_72;
    uint64_t var_29;
    uint64_t var_73;
    uint64_t var_74;
    uint64_t var_75;
    uint64_t local_sp_2;
    uint64_t var_37;
    uint64_t var_38;
    uint64_t var_39;
    uint64_t var_40;
    uint64_t var_41;
    uint64_t var_42;
    uint64_t var_43;
    uint64_t var_44;
    uint64_t var_34;
    uint64_t var_46;
    uint64_t var_28;
    uint64_t var_35;
    uint64_t var_36;
    uint64_t *var_25;
    uint64_t *var_26;
    uint64_t *var_27;
    uint64_t local_sp_0;
    uint64_t local_sp_3;
    uint64_t var_45;
    uint64_t var_30;
    uint64_t var_31;
    uint64_t var_32;
    uint64_t var_33;
    uint64_t var_13;
    uint64_t **var_14;
    uint64_t var_15;
    uint64_t *var_16;
    uint64_t *var_87;
    uint64_t var_88;
    uint64_t var_17;
    uint64_t *var_18;
    uint64_t var_19;
    uint64_t var_20;
    uint64_t var_21;
    uint64_t var_22;
    unsigned char *var_23;
    unsigned char var_24;
    uint64_t *var_47;
    uint32_t *var_48;
    uint32_t var_49;
    uint64_t var_50;
    uint64_t var_51;
    uint64_t var_52;
    uint64_t var_53;
    uint64_t var_54;
    uint64_t var_55;
    uint64_t var_56;
    uint64_t var_57;
    uint64_t var_58;
    uint64_t *var_59;
    uint64_t var_60;
    uint64_t var_61;
    uint64_t var_65;
    uint64_t var_62;
    uint64_t var_63;
    uint64_t var_64;
    uint64_t var_66;
    uint64_t local_sp_1;
    uint64_t var_67;
    uint64_t var_68;
    unsigned char *var_69;
    unsigned char var_70;
    uint64_t **_pre_phi;
    uint64_t **var_77;
    uint64_t var_78;
    uint64_t var_79;
    uint64_t var_80;
    uint64_t var_81;
    uint64_t var_82;
    uint64_t var_83;
    uint64_t *var_84;
    uint64_t var_85;
    bool var_86;
    uint64_t var_89;
    uint64_t var_90;
    uint64_t var_91;
    uint64_t var_92;
    uint64_t var_93;
    uint64_t var_94;
    uint64_t var_95;
    unsigned int loop_state_var;
    var_0 = revng_init_local_sp(0UL);
    var_1 = init_rbp();
    *(uint64_t *)(var_0 + (-8L)) = var_1;
    var_2 = var_0 + (-168L);
    var_3 = (uint64_t *)(var_0 + (-128L));
    *var_3 = rdi;
    var_4 = (uint64_t *)(var_0 + (-136L));
    *var_4 = rsi;
    var_5 = (uint64_t *)(var_0 + (-144L));
    *var_5 = rdx;
    var_6 = var_0 + (-152L);
    *(uint64_t *)var_6 = rcx;
    var_7 = (uint64_t *)(var_0 + (-160L));
    *var_7 = r8;
    var_8 = (uint64_t *)var_2;
    *var_8 = r9;
    var_9 = *(uint64_t *)(*var_3 + 152UL);
    var_10 = var_0 + (-40L);
    var_11 = (uint64_t *)var_10;
    *var_11 = var_9;
    var_12 = (uint64_t **)var_10;
    var_28 = 0UL;
    local_sp_1 = var_2;
    local_sp_2 = var_2;
    rax_0 = 18446744073709551615UL;
    if ((*(unsigned char *)((**var_12 + (*var_7 << 4UL)) + 8UL) & '\b') == '\x00') {
        var_47 = (uint64_t *)(var_0 + (-32L));
        *var_47 = 0UL;
        var_48 = (uint32_t *)(var_0 + (-76L));
        var_49 = (uint32_t)*(unsigned char *)((**var_12 + (*var_7 << 4UL)) + 8UL);
        *var_48 = var_49;
        var_50 = **var_12;
        var_51 = *var_7;
        var_52 = var_50 + (var_51 << 4UL);
        if ((*(unsigned char *)(var_52 + 10UL) & '\x10') == '\x00') {
            rax_0 = 18446744073709551614UL;
            if (var_49 != 4U) {
                var_58 = *(uint64_t *)var_52 + 1UL;
                var_59 = (uint64_t *)(var_0 + (-88L));
                *var_59 = var_58;
                var_60 = *var_5 + (var_58 << 4UL);
                var_61 = *(uint64_t *)(var_60 + 8UL) - *(uint64_t *)var_60;
                *var_47 = var_61;
                var_65 = var_61;
                if (*(uint64_t *)(var_0 | 8UL) != 0UL) {
                    var_62 = *var_5 + (*var_59 << 4UL);
                    var_65 = 0UL;
                    if (*(uint64_t *)var_62 == 18446744073709551615UL) {
                        return rax_0;
                    }
                    if (*(uint64_t *)(var_62 + 8UL) == 18446744073709551615UL) {
                        return rax_0;
                    }
                    if (var_61 != 0UL) {
                        *(uint64_t *)(var_0 + (-96L)) = *(uint64_t *)(*var_3 + 8UL);
                        var_63 = *var_47;
                        var_64 = var_0 + (-176L);
                        *(uint64_t *)var_64 = 4288990UL;
                        indirect_placeholder();
                        local_sp_1 = var_64;
                        if ((uint64_t)(uint32_t)var_63 != 0UL) {
                            return rax_0;
                        }
                        var_65 = *var_47;
                    }
                }
                local_sp_2 = local_sp_1;
                if (var_65 != 0UL) {
                    var_66 = *var_7;
                    var_67 = *var_8;
                    *(uint64_t *)(local_sp_1 + (-8L)) = 4289042UL;
                    var_68 = indirect_placeholder_1(var_67, var_66);
                    var_69 = (unsigned char *)(var_0 + (-57L));
                    var_70 = (unsigned char)var_68;
                    *var_69 = var_70;
                    if (var_70 == '\x01') {
                        return rax_0;
                    }
                    var_71 = **(uint64_t **)(((*var_7 * 24UL) + *(uint64_t *)(*var_11 + 40UL)) + 16UL);
                    var_72 = (uint64_t *)(var_0 + (-104L));
                    *var_72 = var_71;
                    var_73 = *(uint64_t *)((**(uint64_t **)var_6 << 3UL) + *(uint64_t *)(*var_3 + 184UL)) + 8UL;
                    var_74 = local_sp_1 + (-16L);
                    *(uint64_t *)var_74 = 4289164UL;
                    var_75 = indirect_placeholder_1(var_73, var_71);
                    local_sp_2 = var_74;
                    if (var_75 != 0UL) {
                        var_76 = *var_72;
                        rax_0 = var_76;
                        return rax_0;
                    }
                }
            }
        }
        var_53 = **(uint64_t **)var_6;
        var_54 = *var_3;
        var_55 = *var_11;
        var_56 = var_0 + (-176L);
        *(uint64_t *)var_56 = 4288705UL;
        var_57 = indirect_placeholder_7(var_54, var_53, var_55, var_51);
        *var_47 = (uint64_t)((long)(var_57 << 32UL) >> (long)32UL);
        local_sp_2 = var_56;
        local_sp_3 = local_sp_2;
        if (*var_47 == 0UL) {
            var_77 = (uint64_t **)var_6;
            var_78 = **var_77;
            var_79 = (*var_7 << 4UL) + **var_12;
            var_80 = *var_3;
            var_81 = local_sp_2 + (-8L);
            *(uint64_t *)var_81 = 4289231UL;
            var_82 = indirect_placeholder_8(var_78, var_80, var_79);
            _pre_phi = var_77;
            local_sp_3 = var_81;
            if ((uint64_t)(unsigned char)var_82 == 0UL) {
                return rax_0;
            }
        }
        _pre_phi = (uint64_t **)var_6;
        var_83 = *(uint64_t *)(*(uint64_t *)(*var_11 + 24UL) + (*var_7 << 3UL));
        var_84 = (uint64_t *)(var_0 + (-112L));
        *var_84 = var_83;
        var_85 = *var_47;
        var_86 = (var_85 == 0UL);
        var_87 = *_pre_phi;
        var_88 = *var_87;
        *var_87 = (var_86 ? (var_88 + 1UL) : (var_85 + var_88));
        if (*(uint64_t *)(var_0 | 8UL) != 0UL) {
            var_89 = **_pre_phi;
            var_90 = *var_3;
            if ((long)var_89 > (long)*(uint64_t *)(var_90 + 168UL)) {
                return rax_0;
            }
            var_91 = *(uint64_t *)((var_89 << 3UL) + *(uint64_t *)(var_90 + 184UL));
            if (var_91 == 0UL) {
                return rax_0;
            }
            var_92 = var_91 + 8UL;
            var_93 = *var_84;
            *(uint64_t *)(local_sp_3 + (-8L)) = 4289437UL;
            var_94 = indirect_placeholder_1(var_92, var_93);
            if (var_94 != 0UL) {
                return rax_0;
            }
        }
        *(uint64_t *)(*var_8 + 8UL) = 0UL;
        var_95 = *var_84;
        rax_0 = var_95;
    } else {
        var_13 = *(uint64_t *)(*var_3 + 184UL);
        var_14 = (uint64_t **)var_6;
        var_15 = *(uint64_t *)((**var_14 << 3UL) + var_13) + 8UL;
        var_16 = (uint64_t *)(var_0 + (-48L));
        *var_16 = var_15;
        var_17 = (*var_7 * 24UL) + *(uint64_t *)(*var_11 + 40UL);
        var_18 = (uint64_t *)(var_0 + (-56L));
        *var_18 = var_17;
        var_19 = *var_7;
        var_20 = *var_8;
        var_21 = var_0 + (-176L);
        *(uint64_t *)var_21 = 4288335UL;
        var_22 = indirect_placeholder_1(var_20, var_19);
        var_23 = (unsigned char *)(var_0 + (-57L));
        var_24 = (unsigned char)var_22;
        *var_23 = var_24;
        local_sp_0 = var_21;
        rax_0 = 18446744073709551614UL;
        if (var_24 == '\x01') {
            return rax_0;
        }
        var_25 = (uint64_t *)(var_0 + (-24L));
        *var_25 = 18446744073709551615UL;
        var_26 = (uint64_t *)(var_0 + (-16L));
        *var_26 = 0UL;
        var_27 = (uint64_t *)(var_0 + (-72L));
        while (1U)
            {
                var_29 = *var_18;
                if ((long)*(uint64_t *)(var_29 + 8UL) <= (long)var_28) {
                    loop_state_var = 0U;
                    break;
                }
                var_30 = *(uint64_t *)(*(uint64_t *)(var_29 + 16UL) + (var_28 << 3UL));
                *var_27 = var_30;
                var_31 = *var_16;
                var_32 = local_sp_0 + (-8L);
                *(uint64_t *)var_32 = 4288431UL;
                var_33 = indirect_placeholder_1(var_31, var_30);
                local_sp_0 = var_32;
                if (var_33 != 0UL) {
                    var_34 = *var_25;
                    if (var_34 != 18446744073709551615UL) {
                        var_35 = *var_8;
                        *(uint64_t *)(local_sp_0 + (-16L)) = 4288480UL;
                        var_36 = indirect_placeholder_1(var_35, var_34);
                        if (var_36 == 0UL) {
                            var_37 = *var_27;
                            rax_0 = var_37;
                            loop_state_var = 1U;
                            break;
                        }
                        var_38 = *(uint64_t *)(var_0 | 8UL);
                        if (var_38 != 0UL) {
                            loop_state_var = 0U;
                            break;
                        }
                        var_39 = **var_14;
                        var_40 = *var_8;
                        var_41 = *var_5;
                        var_42 = *var_4;
                        var_43 = *var_27;
                        *(uint64_t *)(local_sp_0 + (-24L)) = 4288551UL;
                        var_44 = indirect_placeholder_10(var_43, var_42, var_41, var_38, var_39, var_40);
                        if ((uint64_t)(uint32_t)var_44 != 0UL) {
                            loop_state_var = 1U;
                            break;
                        }
                        loop_state_var = 0U;
                        break;
                    }
                    *var_25 = *var_27;
                }
                var_46 = *var_26 + 1UL;
                *var_26 = var_46;
                var_28 = var_46;
                continue;
            }
        var_45 = *var_25;
        rax_0 = var_45;
    }
}

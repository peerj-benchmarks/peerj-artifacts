typedef unsigned char __u_char;
typedef unsigned short __u_short;
typedef unsigned int __u_int;
typedef unsigned long __u_long;
typedef signed char __int8_t;
typedef unsigned char __uint8_t;
typedef short __int16_t;
typedef unsigned short __uint16_t;
typedef int __int32_t;
typedef unsigned int __uint32_t;
typedef long __int64_t;
typedef unsigned long __uint64_t;
typedef __int8_t __int_least8_t;
typedef __uint8_t __uint_least8_t;
typedef __int16_t __int_least16_t;
typedef __uint16_t __uint_least16_t;
typedef __int32_t __int_least32_t;
typedef __uint32_t __uint_least32_t;
typedef __int64_t __int_least64_t;
typedef __uint64_t __uint_least64_t;
typedef long __quad_t;
typedef unsigned long __u_quad_t;
typedef long __intmax_t;
typedef unsigned long __uintmax_t;
typedef unsigned long __dev_t;
typedef unsigned int __uid_t;
typedef unsigned int __gid_t;
typedef unsigned long __ino_t;
typedef unsigned long __ino64_t;
typedef unsigned int __mode_t;
typedef unsigned long __nlink_t;
typedef long __off_t;
typedef long __off64_t;
typedef int __pid_t;
typedef struct {
    int __val[2];
} __fsid_t;
typedef long __clock_t;
typedef unsigned long __rlim_t;
typedef unsigned long __rlim64_t;
typedef unsigned int __id_t;
typedef long __time_t;
typedef unsigned int __useconds_t;
typedef long __suseconds_t;
typedef int __daddr_t;
typedef int __key_t;
typedef int __clockid_t;
typedef void *__timer_t;
typedef long __blksize_t;
typedef long __blkcnt_t;
typedef long __blkcnt64_t;
typedef unsigned long __fsblkcnt_t;
typedef unsigned long __fsblkcnt64_t;
typedef unsigned long __fsfilcnt_t;
typedef unsigned long __fsfilcnt64_t;
typedef long __fsword_t;
typedef long __ssize_t;
typedef long __syscall_slong_t;
typedef unsigned long __syscall_ulong_t;
typedef __off64_t __loff_t;
typedef char *__caddr_t;
typedef long __intptr_t;
typedef unsigned int __socklen_t;
typedef int __sig_atomic_t;
typedef __int8_t int8_t;
typedef __int16_t int16_t;
typedef __int32_t int32_t;
typedef __int64_t int64_t;
typedef __uint8_t uint8_t;
typedef __uint16_t uint16_t;
typedef __uint32_t uint32_t;
typedef __uint64_t uint64_t;
typedef __int_least8_t int_least8_t;
typedef __int_least16_t int_least16_t;
typedef __int_least32_t int_least32_t;
typedef __int_least64_t int_least64_t;
typedef __uint_least8_t uint_least8_t;
typedef __uint_least16_t uint_least16_t;
typedef __uint_least32_t uint_least32_t;
typedef __uint_least64_t uint_least64_t;
typedef signed char int_fast8_t;
typedef long int_fast16_t;
typedef long int_fast32_t;
typedef long int_fast64_t;
typedef unsigned char uint_fast8_t;
typedef unsigned long uint_fast16_t;
typedef unsigned long uint_fast32_t;
typedef unsigned long uint_fast64_t;
typedef long intptr_t;
typedef unsigned long uintptr_t;
typedef __intmax_t intmax_t;
typedef __uintmax_t uintmax_t;
typedef _Bool bool;
struct indirect_placeholder_117_ret_type;
struct indirect_placeholder_117_ret_type {
    uint64_t field_0;
    uint64_t field_1;
    uint64_t field_2;
};
extern uint64_t revng_init_local_sp(uint64_t param_0);
extern uint64_t init_rbp(void);
extern void indirect_placeholder(void);
extern uint64_t indirect_placeholder_11(uint64_t param_0);
extern uint64_t init_rbx(void);
extern uint64_t init_r10(void);
extern uint64_t init_r9(void);
extern void indirect_placeholder_9(uint64_t param_0);
extern uint64_t indirect_placeholder_12(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint64_t param_3, uint64_t param_4, uint64_t param_5, uint64_t param_6);
extern struct indirect_placeholder_117_ret_type indirect_placeholder_117(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint64_t param_3, uint64_t param_4, uint64_t param_5);
uint64_t bb_re_string_construct(uint64_t rdx, uint64_t rcx, uint64_t r8, uint64_t rdi, uint64_t rsi) {
    uint64_t var_45;
    uint64_t local_sp_1;
    uint64_t var_0;
    uint64_t var_1;
    uint64_t var_2;
    uint64_t var_3;
    uint64_t var_4;
    uint64_t *var_5;
    uint64_t var_6;
    uint64_t *var_7;
    uint64_t *var_8;
    uint64_t *var_9;
    uint64_t *var_10;
    unsigned char *var_11;
    uint64_t var_12;
    uint64_t var_13;
    uint64_t var_14;
    uint64_t var_15;
    uint64_t var_16;
    uint64_t var_17;
    uint64_t var_18;
    struct indirect_placeholder_117_ret_type var_19;
    uint64_t var_20;
    uint64_t var_21;
    uint64_t var_22;
    uint64_t local_sp_0;
    uint64_t var_40;
    uint32_t rax_0_shrunk;
    uint64_t var_41;
    uint64_t var_42;
    uint64_t var_43;
    uint64_t var_44;
    uint32_t var_46;
    uint64_t var_23;
    uint64_t var_24;
    uint64_t var_25;
    uint64_t var_26;
    uint64_t var_27;
    uint32_t *var_28;
    uint32_t var_29;
    uint64_t var_30;
    bool var_31;
    uint64_t var_32;
    bool var_33;
    bool var_34;
    uint32_t *var_36;
    uint64_t var_37;
    uint64_t var_38;
    uint32_t var_39;
    uint64_t var_35;
    uint64_t var_50;
    bool var_47;
    uint64_t var_48;
    uint64_t var_49;
    var_0 = revng_init_local_sp(0UL);
    var_1 = init_rbp();
    var_2 = init_rbx();
    var_3 = init_r9();
    var_4 = init_r10();
    *(uint64_t *)(var_0 + (-8L)) = var_1;
    var_5 = (uint64_t *)(var_0 + (-32L));
    *var_5 = rdi;
    var_6 = var_0 + (-40L);
    var_7 = (uint64_t *)var_6;
    *var_7 = rsi;
    var_8 = (uint64_t *)(var_0 + (-48L));
    *var_8 = rdx;
    var_9 = (uint64_t *)(var_0 + (-56L));
    *var_9 = rcx;
    var_10 = (uint64_t *)(var_0 + (-72L));
    *var_10 = var_3;
    var_11 = (unsigned char *)(var_0 + (-60L));
    *var_11 = (unsigned char)r8;
    *(uint64_t *)(var_0 + (-80L)) = 4233108UL;
    indirect_placeholder();
    var_12 = (uint64_t)*var_11;
    var_13 = *var_10;
    var_14 = *var_9;
    var_15 = *var_5;
    var_16 = *var_8;
    var_17 = *var_7;
    var_18 = var_0 + (-88L);
    *(uint64_t *)var_18 = 4233146UL;
    var_19 = indirect_placeholder_117(var_15, var_14, var_12, var_17, var_16, var_13);
    var_20 = var_19.field_1;
    var_21 = var_19.field_2;
    var_22 = *var_8;
    local_sp_0 = var_18;
    rax_0_shrunk = 0U;
    if ((long)var_22 <= (long)0UL) {
        var_23 = var_19.field_0;
        var_24 = var_22 + 1UL;
        var_25 = *var_5;
        var_26 = var_0 + (-96L);
        *(uint64_t *)var_26 = 4233176UL;
        var_27 = indirect_placeholder_12(var_23, var_20, var_2, var_25, var_24, var_21, var_4);
        var_28 = (uint32_t *)(var_0 + (-12L));
        var_29 = (uint32_t)var_27;
        *var_28 = var_29;
        local_sp_0 = var_26;
        rax_0_shrunk = var_29;
        if (var_29 == 0U) {
            return (uint64_t)rax_0_shrunk;
        }
    }
    var_30 = *var_5;
    var_31 = (*(unsigned char *)(var_30 + 139UL) == '\x00');
    var_32 = var_30 + 8UL;
    *(uint64_t *)var_32 = *(uint64_t *)(var_31 ? var_6 : var_32);
    var_33 = (*var_11 == '\x00');
    var_34 = ((int)*(uint32_t *)(*var_10 + 180UL) > (int)1U);
    local_sp_1 = local_sp_0;
    if (var_33) {
        if (var_34) {
            var_50 = *var_5;
            *(uint64_t *)(local_sp_0 + (-8L)) = 4233458UL;
            indirect_placeholder_9(var_50);
        } else {
            var_47 = (*var_9 == 0UL);
            var_48 = *var_5;
            if (var_47) {
                *(uint64_t *)(var_48 + 48UL) = *(uint64_t *)(var_48 + 64UL);
                var_49 = *var_5;
                *(uint64_t *)(var_49 + 56UL) = *(uint64_t *)(var_49 + 64UL);
            } else {
                *(uint64_t *)(local_sp_0 + (-8L)) = 4233479UL;
                indirect_placeholder_9(var_48);
            }
        }
    } else {
        if (!var_34) {
            var_36 = (uint32_t *)(var_0 + (-12L));
            var_37 = *var_5;
            *(uint64_t *)(local_sp_1 + (-8L)) = 4233280UL;
            var_38 = indirect_placeholder_11(var_37);
            var_39 = (uint32_t)var_38;
            *var_36 = var_39;
            rax_0_shrunk = var_39;
            while (var_39 != 0U)
                {
                    var_40 = *var_5;
                    rax_0_shrunk = 0U;
                    if ((long)*(uint64_t *)(var_40 + 56UL) < (long)*var_8) {
                        break;
                    }
                    var_41 = *(uint64_t *)(var_40 + 64UL);
                    var_42 = *(uint64_t *)(var_40 + 48UL);
                    if ((long)var_41 > (long)(var_42 + (uint64_t)*(uint32_t *)(*var_10 + 180UL))) {
                        break;
                    }
                    var_43 = var_41 << 1UL;
                    var_44 = local_sp_1 + (-16L);
                    *(uint64_t *)var_44 = 4233387UL;
                    var_45 = indirect_placeholder_12(var_42, var_20, var_2, var_40, var_43, var_21, var_4);
                    var_46 = (uint32_t)var_45;
                    *var_36 = var_46;
                    local_sp_1 = var_44;
                    rax_0_shrunk = var_46;
                    if (var_46 == 0U) {
                        break;
                    }
                    var_37 = *var_5;
                    *(uint64_t *)(local_sp_1 + (-8L)) = 4233280UL;
                    var_38 = indirect_placeholder_11(var_37);
                    var_39 = (uint32_t)var_38;
                    *var_36 = var_39;
                    rax_0_shrunk = var_39;
                }
        }
        var_35 = *var_5;
        *(uint64_t *)(local_sp_0 + (-8L)) = 4233429UL;
        indirect_placeholder_9(var_35);
    }
    return (uint64_t)rax_0_shrunk;
}

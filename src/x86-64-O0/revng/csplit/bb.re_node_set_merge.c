typedef unsigned char __u_char;
typedef unsigned short __u_short;
typedef unsigned int __u_int;
typedef unsigned long __u_long;
typedef signed char __int8_t;
typedef unsigned char __uint8_t;
typedef short __int16_t;
typedef unsigned short __uint16_t;
typedef int __int32_t;
typedef unsigned int __uint32_t;
typedef long __int64_t;
typedef unsigned long __uint64_t;
typedef __int8_t __int_least8_t;
typedef __uint8_t __uint_least8_t;
typedef __int16_t __int_least16_t;
typedef __uint16_t __uint_least16_t;
typedef __int32_t __int_least32_t;
typedef __uint32_t __uint_least32_t;
typedef __int64_t __int_least64_t;
typedef __uint64_t __uint_least64_t;
typedef long __quad_t;
typedef unsigned long __u_quad_t;
typedef long __intmax_t;
typedef unsigned long __uintmax_t;
typedef unsigned long __dev_t;
typedef unsigned int __uid_t;
typedef unsigned int __gid_t;
typedef unsigned long __ino_t;
typedef unsigned long __ino64_t;
typedef unsigned int __mode_t;
typedef unsigned long __nlink_t;
typedef long __off_t;
typedef long __off64_t;
typedef int __pid_t;
typedef struct {
    int __val[2];
} __fsid_t;
typedef long __clock_t;
typedef unsigned long __rlim_t;
typedef unsigned long __rlim64_t;
typedef unsigned int __id_t;
typedef long __time_t;
typedef unsigned int __useconds_t;
typedef long __suseconds_t;
typedef int __daddr_t;
typedef int __key_t;
typedef int __clockid_t;
typedef void *__timer_t;
typedef long __blksize_t;
typedef long __blkcnt_t;
typedef long __blkcnt64_t;
typedef unsigned long __fsblkcnt_t;
typedef unsigned long __fsblkcnt64_t;
typedef unsigned long __fsfilcnt_t;
typedef unsigned long __fsfilcnt64_t;
typedef long __fsword_t;
typedef long __ssize_t;
typedef long __syscall_slong_t;
typedef unsigned long __syscall_ulong_t;
typedef __off64_t __loff_t;
typedef char *__caddr_t;
typedef long __intptr_t;
typedef unsigned int __socklen_t;
typedef int __sig_atomic_t;
typedef __int8_t int8_t;
typedef __int16_t int16_t;
typedef __int32_t int32_t;
typedef __int64_t int64_t;
typedef __uint8_t uint8_t;
typedef __uint16_t uint16_t;
typedef __uint32_t uint32_t;
typedef __uint64_t uint64_t;
typedef __int_least8_t int_least8_t;
typedef __int_least16_t int_least16_t;
typedef __int_least32_t int_least32_t;
typedef __int_least64_t int_least64_t;
typedef __uint_least8_t uint_least8_t;
typedef __uint_least16_t uint_least16_t;
typedef __uint_least32_t uint_least32_t;
typedef __uint_least64_t uint_least64_t;
typedef signed char int_fast8_t;
typedef long int_fast16_t;
typedef long int_fast32_t;
typedef long int_fast64_t;
typedef unsigned char uint_fast8_t;
typedef unsigned long uint_fast16_t;
typedef unsigned long uint_fast32_t;
typedef unsigned long uint_fast64_t;
typedef long intptr_t;
typedef unsigned long uintptr_t;
typedef __intmax_t intmax_t;
typedef __uintmax_t uintmax_t;
typedef _Bool bool;
extern uint64_t revng_init_local_sp(uint64_t param_0);
extern uint64_t init_rbp(void);
extern void indirect_placeholder(void);
extern uint64_t indirect_placeholder_1(uint64_t param_0, uint64_t param_1);
uint64_t bb_re_node_set_merge(uint64_t rdi, uint64_t rsi) {
    uint64_t *_pre_phi142;
    uint64_t var_0;
    uint64_t var_1;
    uint64_t var_2;
    uint64_t var_3;
    uint64_t *var_4;
    uint64_t *var_5;
    uint64_t rax_0;
    uint64_t var_31;
    uint64_t *var_6;
    uint64_t var_7;
    uint64_t **var_8;
    uint64_t var_9;
    uint64_t var_10;
    uint64_t *var_11;
    uint64_t var_12;
    uint64_t *var_13;
    uint64_t var_14;
    uint64_t var_15;
    uint64_t var_16;
    uint64_t var_17;
    uint64_t *_pre_phi138;
    uint64_t local_sp_0;
    uint64_t var_18;
    bool var_19;
    uint64_t var_20;
    uint64_t var_21;
    uint64_t *var_22;
    uint64_t var_23;
    uint64_t *var_24;
    uint64_t var_25;
    uint64_t *var_26;
    uint64_t var_27;
    bool var_28;
    uint64_t var_29;
    uint64_t var_30;
    uint64_t var_32;
    uint64_t local_sp_1;
    uint64_t var_33;
    uint64_t var_34;
    uint64_t var_35;
    uint64_t var_36;
    uint64_t var_37;
    uint64_t var_38;
    uint64_t var_39;
    uint64_t *var_40;
    uint64_t *var_41;
    uint64_t var_42;
    uint64_t var_43;
    uint64_t var_44;
    uint64_t var_45;
    bool var_46;
    uint64_t var_47;
    uint64_t var_49;
    uint64_t var_50;
    uint64_t var_51;
    uint64_t var_48;
    var_0 = revng_init_local_sp(0UL);
    var_1 = init_rbp();
    *(uint64_t *)(var_0 + (-8L)) = var_1;
    var_2 = var_0 + (-72L);
    var_3 = var_0 + (-64L);
    var_4 = (uint64_t *)var_3;
    *var_4 = rdi;
    var_5 = (uint64_t *)var_2;
    *var_5 = rsi;
    local_sp_0 = var_2;
    rax_0 = 0UL;
    var_6 = (uint64_t *)(rsi + 8UL);
    var_7 = *var_6;
    _pre_phi142 = var_6;
    if (~(rsi != 0UL & var_7 != 0UL)) {
        return rax_0;
    }
    var_8 = (uint64_t **)var_3;
    var_9 = **var_8;
    var_10 = var_7 << 1UL;
    var_11 = (uint64_t *)(*var_4 + 8UL);
    _pre_phi138 = var_11;
    rax_0 = 12UL;
    if ((long)var_9 >= (long)(*var_11 + var_10)) {
        var_12 = (var_9 + var_7) << 1UL;
        var_13 = (uint64_t *)(var_0 + (-48L));
        *var_13 = var_12;
        var_14 = var_12 << 3UL;
        var_15 = *(uint64_t *)(*var_4 + 16UL);
        var_16 = var_0 + (-80L);
        *(uint64_t *)var_16 = 4245557UL;
        var_17 = indirect_placeholder_1(var_15, var_14);
        *(uint64_t *)(var_0 + (-56L)) = var_17;
        local_sp_0 = var_16;
        if (var_17 != 0UL) {
            return rax_0;
        }
        *(uint64_t *)(*var_4 + 16UL) = var_17;
        **var_8 = *var_13;
        _pre_phi142 = (uint64_t *)(*var_5 + 8UL);
        _pre_phi138 = (uint64_t *)(*var_4 + 8UL);
    }
    var_18 = *_pre_phi138;
    var_19 = (var_18 == 0UL);
    var_20 = *_pre_phi142;
    local_sp_1 = local_sp_0;
    if (!var_19) {
        var_21 = (var_20 << 1UL) + var_18;
        var_22 = (uint64_t *)(var_0 + (-32L));
        *var_22 = var_21;
        var_23 = *(uint64_t *)(*var_5 + 8UL) + (-1L);
        var_24 = (uint64_t *)(var_0 + (-16L));
        *var_24 = var_23;
        var_25 = *(uint64_t *)(*var_4 + 8UL) + (-1L);
        var_26 = (uint64_t *)(var_0 + (-24L));
        *var_26 = var_25;
        var_27 = *var_24;
        var_28 = ((long)var_27 < (long)0UL);
        while (!var_28)
            {
                var_29 = *var_26;
                if ((long)var_29 > (long)18446744073709551615UL) {
                    break;
                }
                var_30 = *(uint64_t *)(*var_4 + 16UL);
                var_31 = *(uint64_t *)(var_30 + (var_29 << 3UL));
                var_32 = *(uint64_t *)(*(uint64_t *)(*var_5 + 16UL) + (var_27 << 3UL));
                if (var_31 == var_32) {
                    *var_24 = (var_27 + (-1L));
                    *var_26 = (*var_26 + (-1L));
                } else {
                    if ((long)var_31 < (long)var_32) {
                        var_33 = *var_22 + (-1L);
                        *var_22 = var_33;
                        var_34 = (var_33 << 3UL) + var_30;
                        var_35 = *(uint64_t *)(*var_5 + 16UL);
                        var_36 = *var_24;
                        *var_24 = (var_36 + (-1L));
                        *(uint64_t *)var_34 = *(uint64_t *)((var_36 << 3UL) + var_35);
                    } else {
                        *var_26 = (var_29 + (-1L));
                    }
                }
                var_27 = *var_24;
                var_28 = ((long)var_27 < (long)0UL);
            }
        if (var_28) {
            *var_22 = (*var_22 + (var_27 ^ (-1L)));
            var_37 = local_sp_0 + (-8L);
            *(uint64_t *)var_37 = 4246032UL;
            indirect_placeholder();
            local_sp_1 = var_37;
        }
        *var_26 = (*(uint64_t *)(*var_4 + 8UL) + (-1L));
        var_38 = ((*(uint64_t *)(*var_5 + 8UL) << 1UL) + *(uint64_t *)(*var_4 + 8UL)) + (-1L);
        *var_24 = var_38;
        var_39 = (var_38 - *var_22) + 1UL;
        var_40 = (uint64_t *)(var_0 + (-40L));
        *var_40 = var_39;
        if (var_39 != 0UL) {
            var_41 = (uint64_t *)(*var_4 + 8UL);
            *var_41 = (*var_41 + var_39);
            while (1U)
                {
                    var_42 = *(uint64_t *)(*var_4 + 16UL);
                    var_43 = *(uint64_t *)(var_42 + (*var_24 << 3UL));
                    var_44 = *var_26;
                    var_45 = *(uint64_t *)(var_42 + (var_44 << 3UL));
                    var_46 = ((long)var_43 > (long)var_45);
                    var_47 = *var_40;
                    if (var_46) {
                        *var_40 = (var_47 + (-1L));
                        var_49 = var_42 + ((var_47 + *var_26) << 3UL);
                        var_50 = *(uint64_t *)(*var_4 + 16UL);
                        var_51 = *var_24;
                        *var_24 = (var_51 + (-1L));
                        *(uint64_t *)var_49 = *(uint64_t *)((var_51 << 3UL) + var_50);
                        if (*var_40 != 0UL) {
                            continue;
                        }
                        break;
                    }
                    *(uint64_t *)(((var_47 + var_44) << 3UL) + var_42) = var_45;
                    var_48 = *var_26 + (-1L);
                    *var_26 = var_48;
                    if ((long)var_48 <= (long)18446744073709551615UL) {
                        continue;
                    }
                    *(uint64_t *)(local_sp_1 + (-8L)) = 4246372UL;
                    indirect_placeholder();
                    break;
                }
        }
    }
    *_pre_phi138 = var_20;
    *(uint64_t *)(local_sp_0 + (-8L)) = 4245691UL;
    indirect_placeholder();
    return rax_0;
}

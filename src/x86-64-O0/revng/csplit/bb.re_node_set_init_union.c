typedef unsigned char __u_char;
typedef unsigned short __u_short;
typedef unsigned int __u_int;
typedef unsigned long __u_long;
typedef signed char __int8_t;
typedef unsigned char __uint8_t;
typedef short __int16_t;
typedef unsigned short __uint16_t;
typedef int __int32_t;
typedef unsigned int __uint32_t;
typedef long __int64_t;
typedef unsigned long __uint64_t;
typedef __int8_t __int_least8_t;
typedef __uint8_t __uint_least8_t;
typedef __int16_t __int_least16_t;
typedef __uint16_t __uint_least16_t;
typedef __int32_t __int_least32_t;
typedef __uint32_t __uint_least32_t;
typedef __int64_t __int_least64_t;
typedef __uint64_t __uint_least64_t;
typedef long __quad_t;
typedef unsigned long __u_quad_t;
typedef long __intmax_t;
typedef unsigned long __uintmax_t;
typedef unsigned long __dev_t;
typedef unsigned int __uid_t;
typedef unsigned int __gid_t;
typedef unsigned long __ino_t;
typedef unsigned long __ino64_t;
typedef unsigned int __mode_t;
typedef unsigned long __nlink_t;
typedef long __off_t;
typedef long __off64_t;
typedef int __pid_t;
typedef struct {
    int __val[2];
} __fsid_t;
typedef long __clock_t;
typedef unsigned long __rlim_t;
typedef unsigned long __rlim64_t;
typedef unsigned int __id_t;
typedef long __time_t;
typedef unsigned int __useconds_t;
typedef long __suseconds_t;
typedef int __daddr_t;
typedef int __key_t;
typedef int __clockid_t;
typedef void *__timer_t;
typedef long __blksize_t;
typedef long __blkcnt_t;
typedef long __blkcnt64_t;
typedef unsigned long __fsblkcnt_t;
typedef unsigned long __fsblkcnt64_t;
typedef unsigned long __fsfilcnt_t;
typedef unsigned long __fsfilcnt64_t;
typedef long __fsword_t;
typedef long __ssize_t;
typedef long __syscall_slong_t;
typedef unsigned long __syscall_ulong_t;
typedef __off64_t __loff_t;
typedef char *__caddr_t;
typedef long __intptr_t;
typedef unsigned int __socklen_t;
typedef int __sig_atomic_t;
typedef __int8_t int8_t;
typedef __int16_t int16_t;
typedef __int32_t int32_t;
typedef __int64_t int64_t;
typedef __uint8_t uint8_t;
typedef __uint16_t uint16_t;
typedef __uint32_t uint32_t;
typedef __uint64_t uint64_t;
typedef __int_least8_t int_least8_t;
typedef __int_least16_t int_least16_t;
typedef __int_least32_t int_least32_t;
typedef __int_least64_t int_least64_t;
typedef __uint_least8_t uint_least8_t;
typedef __uint_least16_t uint_least16_t;
typedef __uint_least32_t uint_least32_t;
typedef __uint_least64_t uint_least64_t;
typedef signed char int_fast8_t;
typedef long int_fast16_t;
typedef long int_fast32_t;
typedef long int_fast64_t;
typedef unsigned char uint_fast8_t;
typedef unsigned long uint_fast16_t;
typedef unsigned long uint_fast32_t;
typedef unsigned long uint_fast64_t;
typedef long intptr_t;
typedef unsigned long uintptr_t;
typedef __intmax_t intmax_t;
typedef __uintmax_t uintmax_t;
typedef _Bool bool;
struct indirect_placeholder_118_ret_type;
struct indirect_placeholder_118_ret_type {
    uint64_t field_0;
    uint64_t field_1;
};
extern uint64_t revng_init_local_sp(uint64_t param_0);
extern uint64_t init_rbp(void);
extern void indirect_placeholder(void);
extern uint64_t indirect_placeholder_1(uint64_t param_0, uint64_t param_1);
extern uint64_t helper_cc_compute_all_wrapper(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint32_t param_3);
extern struct indirect_placeholder_118_ret_type indirect_placeholder_118(uint64_t param_0);
uint64_t bb_re_node_set_init_union(uint64_t rdx, uint64_t rdi, uint64_t rsi) {
    uint64_t var_26;
    uint64_t var_0;
    uint64_t var_1;
    uint64_t var_2;
    uint64_t *var_3;
    uint64_t *var_4;
    uint64_t *var_5;
    uint64_t var_6;
    uint64_t *var_14;
    uint64_t *var_15;
    uint64_t *var_16;
    uint64_t var_17;
    uint64_t var_18;
    bool var_19;
    uint64_t var_20;
    uint64_t var_21;
    uint64_t var_22;
    uint64_t var_27;
    uint64_t var_33;
    uint64_t var_34;
    uint64_t var_35;
    uint64_t var_36;
    uint64_t var_37;
    uint64_t var_28;
    uint64_t var_29;
    uint64_t var_30;
    uint64_t var_31;
    uint64_t var_32;
    uint64_t var_24;
    uint64_t var_25;
    uint64_t var_23;
    uint64_t rax_0;
    uint64_t var_7;
    uint64_t var_8;
    uint64_t var_9;
    uint64_t var_10;
    uint64_t **var_11;
    uint64_t var_12;
    struct indirect_placeholder_118_ret_type var_13;
    uint64_t var_38;
    uint64_t var_39;
    uint64_t var_40;
    uint64_t var_41;
    uint64_t var_42;
    uint64_t var_43;
    uint64_t var_44;
    uint64_t var_45;
    uint64_t var_46;
    uint64_t var_47;
    var_0 = revng_init_local_sp(0UL);
    var_1 = init_rbp();
    *(uint64_t *)(var_0 + (-8L)) = var_1;
    var_2 = var_0 + (-48L);
    var_3 = (uint64_t *)var_2;
    *var_3 = rdi;
    var_4 = (uint64_t *)(var_0 + (-56L));
    *var_4 = rsi;
    var_5 = (uint64_t *)(var_0 + (-64L));
    *var_5 = rdx;
    var_6 = *var_4;
    var_17 = 0UL;
    rax_0 = 0UL;
    if (var_6 == 0UL) {
        var_38 = *var_4;
        if (var_38 != 0UL) {
            var_39 = helper_cc_compute_all_wrapper(*(uint64_t *)(var_38 + 8UL), 0UL, 0UL, 25U);
            if ((uint64_t)(((unsigned char)(var_39 >> 4UL) ^ (unsigned char)var_39) & '\xc0') != 0UL) {
                var_40 = *var_4;
                var_41 = *var_3;
                *(uint64_t *)(var_0 + (-80L)) = 4244863UL;
                var_42 = indirect_placeholder_1(var_41, var_40);
                rax_0 = var_42;
                return rax_0;
            }
        }
        var_43 = *var_5;
        if (var_43 == 0UL) {
            *(uint64_t *)(var_0 + (-80L)) = 4244934UL;
            indirect_placeholder();
            return rax_0;
        }
        var_44 = helper_cc_compute_all_wrapper(*(uint64_t *)(var_43 + 8UL), 0UL, 0UL, 25U);
        if ((uint64_t)(((unsigned char)(var_44 >> 4UL) ^ (unsigned char)var_44) & '\xc0') != 0UL) {
            var_45 = *var_5;
            var_46 = *var_3;
            *(uint64_t *)(var_0 + (-80L)) = 4244907UL;
            var_47 = indirect_placeholder_1(var_46, var_45);
            rax_0 = var_47;
            return rax_0;
        }
    }
    var_7 = helper_cc_compute_all_wrapper(*(uint64_t *)(var_6 + 8UL), 0UL, 0UL, 25U);
    rax_0 = 12UL;
    var_8 = *var_5;
    var_9 = helper_cc_compute_all_wrapper(*(uint64_t *)(var_8 + 8UL), 0UL, 0UL, 25U);
    if (~((uint64_t)(((unsigned char)(var_7 >> 4UL) ^ (unsigned char)var_7) & '\xc0') != 0UL & var_8 != 0UL & (uint64_t)(((unsigned char)(var_9 >> 4UL) ^ (unsigned char)var_9) & '\xc0') != 0UL)) {
        return;
    }
    var_10 = *(uint64_t *)(*var_4 + 8UL) + *(uint64_t *)(*var_5 + 8UL);
    var_11 = (uint64_t **)var_2;
    **var_11 = var_10;
    var_12 = **var_11 << 3UL;
    *(uint64_t *)(var_0 + (-80L)) = 4244751UL;
    var_13 = indirect_placeholder_118(var_12);
    *(uint64_t *)(*var_3 + 16UL) = var_13.field_0;
    if (*(uint64_t *)(*var_3 + 16UL) != 0UL) {
        var_14 = (uint64_t *)(var_0 + (-32L));
        *var_14 = 0UL;
        var_15 = (uint64_t *)(var_0 + (-24L));
        *var_15 = 0UL;
        var_16 = (uint64_t *)(var_0 + (-16L));
        *var_16 = 0UL;
        rax_0 = 0UL;
        var_18 = *var_4;
        var_19 = ((long)*(uint64_t *)(var_18 + 8UL) > (long)var_17);
        while (!var_19)
            {
                var_20 = *var_5;
                var_21 = *(uint64_t *)(var_20 + 8UL);
                var_22 = *var_15;
                if ((long)var_21 > (long)var_22) {
                    break;
                }
                var_26 = *(uint64_t *)(*(uint64_t *)(var_18 + 16UL) + (var_17 << 3UL));
                var_27 = *(uint64_t *)(*(uint64_t *)(var_20 + 16UL) + (var_22 << 3UL));
                if ((long)var_26 > (long)var_27) {
                    var_33 = *(uint64_t *)(*var_3 + 16UL);
                    var_34 = *var_14;
                    *var_14 = (var_34 + 1UL);
                    var_35 = var_33 + (var_34 << 3UL);
                    var_36 = *(uint64_t *)(*var_5 + 16UL);
                    var_37 = *var_15;
                    *var_15 = (var_37 + 1UL);
                    *(uint64_t *)var_35 = *(uint64_t *)((var_37 << 3UL) + var_36);
                } else {
                    if (var_26 == var_27) {
                        *var_15 = (var_22 + 1UL);
                    }
                    var_28 = *(uint64_t *)(*var_3 + 16UL);
                    var_29 = *var_14;
                    *var_14 = (var_29 + 1UL);
                    var_30 = var_28 + (var_29 << 3UL);
                    var_31 = *(uint64_t *)(*var_4 + 16UL);
                    var_32 = *var_16;
                    *var_16 = (var_32 + 1UL);
                    *(uint64_t *)var_30 = *(uint64_t *)((var_32 << 3UL) + var_31);
                }
                var_17 = *var_16;
                var_18 = *var_4;
                var_19 = ((long)*(uint64_t *)(var_18 + 8UL) > (long)var_17);
            }
        if (var_19) {
            *(uint64_t *)(var_0 + (-88L)) = 4245284UL;
            indirect_placeholder();
            var_24 = *var_14 + (*(uint64_t *)(*var_4 + 8UL) - *var_16);
            *var_14 = var_24;
            var_25 = var_24;
        } else {
            if ((long)*(uint64_t *)(*var_5 + 8UL) > (long)*var_15) {
                *(uint64_t *)(var_0 + (-88L)) = 4245385UL;
                indirect_placeholder();
                var_23 = *var_14 + (*(uint64_t *)(*var_5 + 8UL) - *var_15);
                *var_14 = var_23;
                var_25 = var_23;
            } else {
                var_25 = *var_14;
            }
        }
        *(uint64_t *)(*var_3 + 8UL) = var_25;
    }
    return rax_0;
}

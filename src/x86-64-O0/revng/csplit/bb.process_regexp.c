typedef unsigned char __u_char;
typedef unsigned short __u_short;
typedef unsigned int __u_int;
typedef unsigned long __u_long;
typedef signed char __int8_t;
typedef unsigned char __uint8_t;
typedef short __int16_t;
typedef unsigned short __uint16_t;
typedef int __int32_t;
typedef unsigned int __uint32_t;
typedef long __int64_t;
typedef unsigned long __uint64_t;
typedef __int8_t __int_least8_t;
typedef __uint8_t __uint_least8_t;
typedef __int16_t __int_least16_t;
typedef __uint16_t __uint_least16_t;
typedef __int32_t __int_least32_t;
typedef __uint32_t __uint_least32_t;
typedef __int64_t __int_least64_t;
typedef __uint64_t __uint_least64_t;
typedef long __quad_t;
typedef unsigned long __u_quad_t;
typedef long __intmax_t;
typedef unsigned long __uintmax_t;
typedef unsigned long __dev_t;
typedef unsigned int __uid_t;
typedef unsigned int __gid_t;
typedef unsigned long __ino_t;
typedef unsigned long __ino64_t;
typedef unsigned int __mode_t;
typedef unsigned long __nlink_t;
typedef long __off_t;
typedef long __off64_t;
typedef int __pid_t;
typedef struct {
    int __val[2];
} __fsid_t;
typedef long __clock_t;
typedef unsigned long __rlim_t;
typedef unsigned long __rlim64_t;
typedef unsigned int __id_t;
typedef long __time_t;
typedef unsigned int __useconds_t;
typedef long __suseconds_t;
typedef int __daddr_t;
typedef int __key_t;
typedef int __clockid_t;
typedef void *__timer_t;
typedef long __blksize_t;
typedef long __blkcnt_t;
typedef long __blkcnt64_t;
typedef unsigned long __fsblkcnt_t;
typedef unsigned long __fsblkcnt64_t;
typedef unsigned long __fsfilcnt_t;
typedef unsigned long __fsfilcnt64_t;
typedef long __fsword_t;
typedef long __ssize_t;
typedef long __syscall_slong_t;
typedef unsigned long __syscall_ulong_t;
typedef __off64_t __loff_t;
typedef char *__caddr_t;
typedef long __intptr_t;
typedef unsigned int __socklen_t;
typedef int __sig_atomic_t;
typedef __int8_t int8_t;
typedef __int16_t int16_t;
typedef __int32_t int32_t;
typedef __int64_t int64_t;
typedef __uint8_t uint8_t;
typedef __uint16_t uint16_t;
typedef __uint32_t uint32_t;
typedef __uint64_t uint64_t;
typedef __int_least8_t int_least8_t;
typedef __int_least16_t int_least16_t;
typedef __int_least32_t int_least32_t;
typedef __int_least64_t int_least64_t;
typedef __uint_least8_t uint_least8_t;
typedef __uint_least16_t uint_least16_t;
typedef __uint_least32_t uint_least32_t;
typedef __uint_least64_t uint_least64_t;
typedef signed char int_fast8_t;
typedef long int_fast16_t;
typedef long int_fast32_t;
typedef long int_fast64_t;
typedef unsigned char uint_fast8_t;
typedef unsigned long uint_fast16_t;
typedef unsigned long uint_fast32_t;
typedef unsigned long uint_fast64_t;
typedef long intptr_t;
typedef unsigned long uintptr_t;
typedef __intmax_t intmax_t;
typedef __uintmax_t uintmax_t;
typedef _Bool bool;
struct indirect_placeholder_88_ret_type;
struct indirect_placeholder_89_ret_type;
struct indirect_placeholder_90_ret_type;
struct indirect_placeholder_91_ret_type;
struct indirect_placeholder_88_ret_type {
    uint64_t field_0;
    uint64_t field_1;
    uint64_t field_2;
    uint64_t field_3;
};
struct indirect_placeholder_89_ret_type {
    uint64_t field_0;
    uint64_t field_1;
};
struct indirect_placeholder_90_ret_type {
    uint64_t field_0;
    uint64_t field_1;
    uint64_t field_2;
    uint64_t field_3;
};
struct indirect_placeholder_91_ret_type {
    uint64_t field_0;
    uint64_t field_1;
};
extern void abort(void);
extern uint64_t revng_init_local_sp(uint64_t param_0);
extern uint64_t init_rbp(void);
extern uint64_t indirect_placeholder_3(void);
extern void indirect_placeholder(void);
extern uint64_t indirect_placeholder_8(uint64_t param_0, uint64_t param_1, uint64_t param_2);
extern uint64_t indirect_placeholder_11(uint64_t param_0);
extern uint64_t helper_cc_compute_all_wrapper(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint32_t param_3);
extern void indirect_placeholder_9(uint64_t param_0);
extern void indirect_placeholder_69(uint64_t param_0, uint64_t param_1, uint64_t param_2);
extern void indirect_placeholder_17(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint64_t param_3);
extern struct indirect_placeholder_88_ret_type indirect_placeholder_88(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint64_t param_3, uint64_t param_4, uint64_t param_5, uint64_t param_6);
extern struct indirect_placeholder_89_ret_type indirect_placeholder_89(uint64_t param_0, uint64_t param_1, uint64_t param_2);
extern struct indirect_placeholder_90_ret_type indirect_placeholder_90(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint64_t param_3, uint64_t param_4, uint64_t param_5, uint64_t param_6);
extern struct indirect_placeholder_91_ret_type indirect_placeholder_91(uint64_t param_0, uint64_t param_1, uint64_t param_2);
void bb_process_regexp(uint64_t rdi, uint64_t rsi) {
    uint64_t var_0;
    uint64_t var_1;
    uint64_t var_2;
    uint64_t var_3;
    uint64_t var_4;
    uint64_t *var_5;
    uint64_t *var_6;
    unsigned char var_7;
    unsigned char *var_8;
    uint64_t local_sp_5;
    uint64_t var_56;
    uint64_t var_51;
    uint64_t var_57;
    uint64_t var_60;
    uint64_t var_32;
    uint64_t local_sp_0;
    uint64_t var_58;
    uint64_t var_59;
    uint64_t local_sp_1;
    uint64_t var_61;
    uint64_t var_62;
    uint64_t var_43;
    uint64_t var_44;
    uint64_t var_45;
    uint64_t var_46;
    uint64_t var_47;
    uint64_t var_48;
    struct indirect_placeholder_89_ret_type var_49;
    uint64_t var_50;
    uint64_t local_sp_8;
    uint64_t local_sp_6;
    uint64_t local_sp_7_be;
    uint64_t local_sp_7;
    uint64_t var_33;
    uint64_t var_30;
    uint64_t var_31;
    uint64_t *_pre_phi144;
    uint64_t local_sp_2;
    uint64_t var_52;
    uint64_t *var_53;
    uint64_t var_54;
    uint64_t var_55;
    uint64_t var_34;
    uint64_t var_37;
    uint64_t local_sp_3;
    uint64_t var_35;
    uint64_t var_36;
    uint64_t local_sp_4;
    uint64_t var_38;
    uint64_t var_39;
    uint64_t var_20;
    uint64_t *var_21;
    uint64_t var_22;
    uint64_t var_23;
    uint64_t var_24;
    uint64_t var_25;
    uint64_t var_26;
    uint64_t var_27;
    struct indirect_placeholder_91_ret_type var_28;
    uint64_t var_29;
    uint64_t var_9;
    uint64_t var_10;
    uint64_t **var_11;
    bool var_12;
    uint64_t var_13;
    uint64_t *var_14;
    uint64_t *var_15;
    uint64_t *var_16;
    uint64_t var_17;
    uint64_t var_18;
    uint64_t var_19;
    uint64_t var_40;
    uint64_t var_41;
    uint64_t var_42;
    unsigned int loop_state_var;
    bool switch_state_var;
    var_0 = revng_init_local_sp(0UL);
    var_1 = init_rbp();
    var_2 = var_0 + (-8L);
    *(uint64_t *)var_2 = var_1;
    var_3 = var_0 + (-72L);
    var_4 = var_0 + (-64L);
    var_5 = (uint64_t *)var_4;
    *var_5 = rdi;
    var_6 = (uint64_t *)var_3;
    *var_6 = rsi;
    var_7 = *(unsigned char *)(*var_5 + 29UL);
    var_8 = (unsigned char *)(var_0 + (-17L));
    *var_8 = var_7;
    local_sp_5 = var_3;
    if (var_7 == '\x01') {
        var_9 = var_0 + (-80L);
        *(uint64_t *)var_9 = 4207525UL;
        indirect_placeholder();
        local_sp_5 = var_9;
    }
    local_sp_6 = local_sp_5;
    if (*(unsigned char *)6493875UL != '\x00' & *(uint64_t *)6493792UL == 0UL) {
        var_10 = local_sp_5 + (-8L);
        *(uint64_t *)var_10 = 4207553UL;
        indirect_placeholder_3();
        local_sp_6 = var_10;
    }
    var_11 = (uint64_t **)var_4;
    var_12 = ((long)**var_11 < (long)0UL);
    var_13 = var_0 + (-32L);
    var_14 = (uint64_t *)var_13;
    var_15 = (uint64_t *)(var_0 + (-16L));
    var_16 = (uint64_t *)(var_0 + (-40L));
    local_sp_7 = local_sp_6;
    local_sp_8 = local_sp_6;
    if (var_12) {
        while (1U)
            {
                var_17 = *(uint64_t *)6493792UL + 1UL;
                *(uint64_t *)6493792UL = var_17;
                var_18 = local_sp_7 + (-8L);
                *(uint64_t *)var_18 = 4207602UL;
                var_19 = indirect_placeholder_11(var_17);
                *var_14 = var_19;
                local_sp_3 = var_18;
                local_sp_4 = var_18;
                if (var_19 != 0UL) {
                    var_34 = *var_5;
                    var_37 = var_34;
                    if (*(unsigned char *)(var_34 + 28UL) != '\x00') {
                        loop_state_var = 0U;
                        break;
                    }
                    if (*var_8 != '\x01') {
                        loop_state_var = 1U;
                        break;
                    }
                    *(uint64_t *)(local_sp_7 + (-16L)) = 4207641UL;
                    indirect_placeholder();
                    var_35 = local_sp_7 + (-24L);
                    *(uint64_t *)var_35 = 4207646UL;
                    indirect_placeholder();
                    local_sp_3 = var_35;
                    loop_state_var = 1U;
                    break;
                }
                var_20 = **(uint64_t **)var_13;
                *var_15 = var_20;
                var_21 = (uint64_t *)(*var_14 + 8UL);
                var_22 = *var_21;
                var_23 = var_20 + (-1L);
                _pre_phi144 = var_21;
                var_24 = var_20;
                if (*(unsigned char *)(var_22 + var_23) == '\n') {
                    *var_15 = var_23;
                    _pre_phi144 = (uint64_t *)(*var_14 + 8UL);
                    var_24 = var_23;
                }
                var_25 = *_pre_phi144;
                var_26 = *var_5 + 32UL;
                var_27 = local_sp_7 + (-16L);
                *(uint64_t *)var_27 = 4207767UL;
                var_28 = indirect_placeholder_91(var_24, var_26, 0UL);
                var_29 = var_28.field_0;
                *var_16 = var_29;
                local_sp_2 = var_27;
                switch_state_var = 0;
                switch (var_29) {
                  case 18446744073709551614UL:
                    {
                        var_33 = var_28.field_1;
                        *(uint64_t *)(local_sp_7 + (-24L)) = 4207803UL;
                        indirect_placeholder_90(0UL, 4369392UL, 0UL, var_24, 0UL, 0UL, var_33);
                        *(uint64_t *)(local_sp_7 + (-32L)) = 4207808UL;
                        indirect_placeholder_11(var_2);
                        abort();
                    }
                    break;
                  case 18446744073709551615UL:
                    {
                        var_30 = local_sp_7 + (-24L);
                        *(uint64_t *)var_30 = 4207820UL;
                        var_31 = indirect_placeholder_8(0UL, var_24, var_25);
                        *var_14 = var_31;
                        local_sp_7_be = var_30;
                        if (*var_8 == '\x01') {
                            var_32 = local_sp_7 + (-32L);
                            *(uint64_t *)var_32 = 4207847UL;
                            indirect_placeholder_9(var_31);
                            local_sp_7_be = var_32;
                        }
                        local_sp_7 = local_sp_7_be;
                        continue;
                    }
                    break;
                  default:
                    {
                        loop_state_var = 2U;
                        switch_state_var = 1;
                        break;
                    }
                    break;
                }
                if (switch_state_var)
                    break;
            }
        switch (loop_state_var) {
          case 2U:
            {
                break;
            }
            break;
          case 1U:
          case 0U:
            {
                switch (loop_state_var) {
                  case 0U:
                    {
                        var_38 = (uint64_t)*var_8;
                        var_39 = *var_6;
                        *(uint64_t *)(local_sp_4 + (-8L)) = 4207679UL;
                        indirect_placeholder_17(var_2, var_38, var_37, var_39);
                        abort();
                    }
                    break;
                  case 1U:
                    {
                        var_36 = local_sp_3 + (-8L);
                        *(uint64_t *)var_36 = 4207656UL;
                        indirect_placeholder();
                        var_37 = *var_5;
                        local_sp_4 = var_36;
                    }
                    break;
                }
            }
            break;
        }
    } else {
        while (1U)
            {
                var_40 = *(uint64_t *)6493792UL + 1UL;
                *(uint64_t *)6493792UL = var_40;
                var_41 = local_sp_8 + (-8L);
                *(uint64_t *)var_41 = 4207896UL;
                var_42 = indirect_placeholder_11(var_40);
                *var_14 = var_42;
                local_sp_0 = var_41;
                local_sp_1 = var_41;
                if (var_42 == 0UL) {
                    var_43 = **(uint64_t **)var_13;
                    *var_15 = var_43;
                    var_44 = *(uint64_t *)(*var_14 + 8UL);
                    var_45 = var_43 + (-1L);
                    var_46 = var_43;
                    if (*(unsigned char *)(var_44 + var_45) == '\n') {
                        *var_15 = var_45;
                        var_46 = var_45;
                    }
                    var_47 = *var_5 + 32UL;
                    var_48 = local_sp_8 + (-16L);
                    *(uint64_t *)var_48 = 4208061UL;
                    var_49 = indirect_placeholder_89(var_46, var_47, 0UL);
                    var_50 = var_49.field_0;
                    *var_16 = var_50;
                    local_sp_2 = var_48;
                    local_sp_8 = var_48;
                    switch_state_var = 0;
                    switch (var_50) {
                      case 18446744073709551615UL:
                        {
                            continue;
                        }
                        break;
                      case 18446744073709551614UL:
                        {
                            var_51 = var_49.field_1;
                            *(uint64_t *)(local_sp_8 + (-24L)) = 4208097UL;
                            indirect_placeholder_88(0UL, 4369392UL, 0UL, var_46, 0UL, 0UL, var_51);
                            *(uint64_t *)(local_sp_8 + (-32L)) = 4208102UL;
                            indirect_placeholder_11(var_2);
                            abort();
                        }
                        break;
                      default:
                        {
                            loop_state_var = 2U;
                            switch_state_var = 1;
                            break;
                        }
                        break;
                    }
                    if (switch_state_var)
                        break;
                }
                var_57 = *var_5;
                var_60 = var_57;
                if (*(unsigned char *)(var_57 + 28UL) != '\x00') {
                    loop_state_var = 1U;
                    break;
                }
                if (*var_8 != '\x01') {
                    loop_state_var = 0U;
                    break;
                }
                *(uint64_t *)(local_sp_8 + (-16L)) = 4207935UL;
                indirect_placeholder();
                var_58 = local_sp_8 + (-24L);
                *(uint64_t *)var_58 = 4207940UL;
                indirect_placeholder();
                local_sp_0 = var_58;
                loop_state_var = 0U;
                break;
            }
        switch (loop_state_var) {
          case 2U:
            {
                var_52 = *(uint64_t *)6493792UL + **var_11;
                var_53 = (uint64_t *)(var_0 + (-48L));
                *var_53 = var_52;
                var_54 = (uint64_t)*(uint32_t *)(*var_5 + 24UL);
                var_55 = (uint64_t)*var_8;
                *(uint64_t *)(local_sp_2 + (-8L)) = 4208165UL;
                indirect_placeholder_69(var_54, var_52, var_55);
                if (*var_8 == '\x01') {
                    *(uint64_t *)(local_sp_2 + (-16L)) = 4208181UL;
                    indirect_placeholder();
                }
                var_56 = helper_cc_compute_all_wrapper(**var_11, 0UL, 0UL, 25U);
                if ((uint64_t)(((unsigned char)(var_56 >> 4UL) ^ (unsigned char)var_56) & '\xc0') == 0UL) {
                    *(uint64_t *)6493792UL = *var_53;
                }
                return;
            }
            break;
          case 1U:
          case 0U:
            {
                switch (loop_state_var) {
                  case 0U:
                    {
                        var_59 = local_sp_0 + (-8L);
                        *(uint64_t *)var_59 = 4207950UL;
                        indirect_placeholder();
                        var_60 = *var_5;
                        local_sp_1 = var_59;
                    }
                    break;
                  case 1U:
                    {
                        var_61 = (uint64_t)*var_8;
                        var_62 = *var_6;
                        *(uint64_t *)(local_sp_1 + (-8L)) = 4207973UL;
                        indirect_placeholder_17(var_2, var_61, var_60, var_62);
                        abort();
                    }
                    break;
                }
            }
            break;
        }
    }
}

typedef unsigned char __u_char;
typedef unsigned short __u_short;
typedef unsigned int __u_int;
typedef unsigned long __u_long;
typedef signed char __int8_t;
typedef unsigned char __uint8_t;
typedef short __int16_t;
typedef unsigned short __uint16_t;
typedef int __int32_t;
typedef unsigned int __uint32_t;
typedef long __int64_t;
typedef unsigned long __uint64_t;
typedef __int8_t __int_least8_t;
typedef __uint8_t __uint_least8_t;
typedef __int16_t __int_least16_t;
typedef __uint16_t __uint_least16_t;
typedef __int32_t __int_least32_t;
typedef __uint32_t __uint_least32_t;
typedef __int64_t __int_least64_t;
typedef __uint64_t __uint_least64_t;
typedef long __quad_t;
typedef unsigned long __u_quad_t;
typedef long __intmax_t;
typedef unsigned long __uintmax_t;
typedef unsigned long __dev_t;
typedef unsigned int __uid_t;
typedef unsigned int __gid_t;
typedef unsigned long __ino_t;
typedef unsigned long __ino64_t;
typedef unsigned int __mode_t;
typedef unsigned long __nlink_t;
typedef long __off_t;
typedef long __off64_t;
typedef int __pid_t;
typedef struct {
    int __val[2];
} __fsid_t;
typedef long __clock_t;
typedef unsigned long __rlim_t;
typedef unsigned long __rlim64_t;
typedef unsigned int __id_t;
typedef long __time_t;
typedef unsigned int __useconds_t;
typedef long __suseconds_t;
typedef int __daddr_t;
typedef int __key_t;
typedef int __clockid_t;
typedef void *__timer_t;
typedef long __blksize_t;
typedef long __blkcnt_t;
typedef long __blkcnt64_t;
typedef unsigned long __fsblkcnt_t;
typedef unsigned long __fsblkcnt64_t;
typedef unsigned long __fsfilcnt_t;
typedef unsigned long __fsfilcnt64_t;
typedef long __fsword_t;
typedef long __ssize_t;
typedef long __syscall_slong_t;
typedef unsigned long __syscall_ulong_t;
typedef __off64_t __loff_t;
typedef char *__caddr_t;
typedef long __intptr_t;
typedef unsigned int __socklen_t;
typedef int __sig_atomic_t;
typedef __int8_t int8_t;
typedef __int16_t int16_t;
typedef __int32_t int32_t;
typedef __int64_t int64_t;
typedef __uint8_t uint8_t;
typedef __uint16_t uint16_t;
typedef __uint32_t uint32_t;
typedef __uint64_t uint64_t;
typedef __int_least8_t int_least8_t;
typedef __int_least16_t int_least16_t;
typedef __int_least32_t int_least32_t;
typedef __int_least64_t int_least64_t;
typedef __uint_least8_t uint_least8_t;
typedef __uint_least16_t uint_least16_t;
typedef __uint_least32_t uint_least32_t;
typedef __uint_least64_t uint_least64_t;
typedef signed char int_fast8_t;
typedef long int_fast16_t;
typedef long int_fast32_t;
typedef long int_fast64_t;
typedef unsigned char uint_fast8_t;
typedef unsigned long uint_fast16_t;
typedef unsigned long uint_fast32_t;
typedef unsigned long uint_fast64_t;
typedef long intptr_t;
typedef unsigned long uintptr_t;
typedef __intmax_t intmax_t;
typedef __uintmax_t uintmax_t;
struct helper_divq_EAX_wrapper_ret_type;
struct type_5;
struct indirect_placeholder_71_ret_type;
struct indirect_placeholder_72_ret_type;
struct indirect_placeholder_73_ret_type;
struct helper_divq_EAX_wrapper_ret_type {
    uint64_t field_0;
    uint64_t field_1;
    uint64_t field_2;
    uint32_t field_3;
    uint64_t field_4;
    uint32_t field_5;
    uint64_t field_6;
    uint32_t field_7;
    uint64_t field_8;
    uint32_t field_9;
    uint32_t field_10;
    unsigned char field_11;
    uint32_t field_12;
};
struct type_5 {
};
struct indirect_placeholder_71_ret_type {
    uint64_t field_0;
    uint64_t field_1;
    uint64_t field_2;
    uint64_t field_3;
    uint64_t field_4;
    uint64_t field_5;
};
struct indirect_placeholder_72_ret_type {
    uint64_t field_0;
    uint64_t field_1;
    uint64_t field_2;
    uint64_t field_3;
    uint64_t field_4;
    uint64_t field_5;
};
struct indirect_placeholder_73_ret_type {
    uint64_t field_0;
    uint64_t field_1;
};
extern uint64_t revng_init_local_sp(uint64_t param_0);
extern uint64_t init_rbp(void);
extern void indirect_placeholder(void);
extern uint64_t init_cc_src2(void);
extern uint64_t helper_cc_compute_c_wrapper(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint32_t param_3);
extern uint64_t init_rbx(void);
extern uint64_t init_r10(void);
extern uint64_t init_state_0x9018(void);
extern uint32_t init_state_0x9010(void);
extern uint64_t init_state_0x8408(void);
extern uint64_t init_state_0x8328(void);
extern uint64_t init_state_0x82d8(void);
extern uint32_t init_state_0x9080(void);
extern uint32_t init_state_0x8248(void);
extern struct helper_divq_EAX_wrapper_ret_type helper_divq_EAX_wrapper(struct type_5 *param_0, uint64_t param_1, uint64_t param_2, uint64_t param_3, uint64_t param_4, uint64_t param_5, uint64_t param_6, uint64_t param_7, uint64_t param_8, uint64_t param_9, uint64_t param_10, uint64_t param_11, uint64_t param_12, uint64_t param_13, uint32_t param_14, uint64_t param_15, uint64_t param_16, uint64_t param_17, uint32_t param_18, uint32_t param_19);
extern uint64_t init_rcx(void);
extern uint64_t init_r8(void);
extern uint64_t init_r9(void);
extern struct indirect_placeholder_71_ret_type indirect_placeholder_71(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint64_t param_3, uint64_t param_4, uint64_t param_5, uint64_t param_6);
extern struct indirect_placeholder_72_ret_type indirect_placeholder_72(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint64_t param_3, uint64_t param_4, uint64_t param_5, uint64_t param_6);
extern struct indirect_placeholder_73_ret_type indirect_placeholder_73(uint64_t param_0);
typedef _Bool bool;
uint64_t bb_init_dfa(uint64_t rdi, uint64_t rsi) {
    struct indirect_placeholder_71_ret_type var_48;
    uint64_t var_0;
    uint64_t var_1;
    uint64_t var_2;
    uint64_t var_3;
    uint64_t var_4;
    uint64_t var_5;
    uint64_t var_6;
    uint64_t var_7;
    uint64_t var_8;
    uint32_t var_9;
    uint64_t var_10;
    uint64_t var_11;
    uint64_t var_12;
    uint32_t var_13;
    uint32_t var_14;
    uint64_t var_15;
    uint64_t var_16;
    uint64_t *var_17;
    uint64_t *var_18;
    uint64_t *_cast4;
    uint32_t var_57;
    uint32_t var_54;
    unsigned char *var_58;
    uint32_t *var_49;
    uint32_t *var_50;
    uint32_t *var_51;
    uint32_t *var_52;
    uint32_t var_53;
    uint64_t local_sp_0;
    uint64_t local_sp_1;
    uint32_t var_59;
    uint32_t var_55;
    uint64_t var_56;
    unsigned char *var_44;
    uint64_t local_sp_2;
    unsigned char *var_45;
    uint64_t var_46;
    uint64_t var_47;
    uint64_t rax_1;
    uint64_t var_42;
    uint64_t var_43;
    uint64_t var_29;
    struct indirect_placeholder_72_ret_type var_31;
    uint64_t var_32;
    uint64_t var_33;
    uint64_t var_34;
    uint64_t var_35;
    uint64_t var_36;
    uint64_t var_37;
    uint64_t var_38;
    uint64_t var_39;
    uint64_t var_40;
    uint64_t var_41;
    uint64_t var_30;
    struct helper_divq_EAX_wrapper_ret_type var_22;
    uint64_t rax_0;
    uint64_t var_23;
    uint64_t var_24;
    struct indirect_placeholder_73_ret_type var_25;
    uint64_t var_26;
    uint64_t **var_27;
    uint64_t *var_28;
    uint64_t *var_19;
    uint64_t var_20;
    struct helper_divq_EAX_wrapper_ret_type var_21;
    var_0 = revng_init_local_sp(0UL);
    var_1 = init_rbp();
    var_2 = init_cc_src2();
    var_3 = init_rcx();
    var_4 = init_r8();
    var_5 = init_rbx();
    var_6 = init_r9();
    var_7 = init_r10();
    var_8 = init_state_0x9018();
    var_9 = init_state_0x9010();
    var_10 = init_state_0x8408();
    var_11 = init_state_0x8328();
    var_12 = init_state_0x82d8();
    var_13 = init_state_0x9080();
    var_14 = init_state_0x8248();
    var_15 = var_0 + (-8L);
    *(uint64_t *)var_15 = var_1;
    var_16 = var_0 + (-80L);
    var_17 = (uint64_t *)var_16;
    *var_17 = rdi;
    var_18 = (uint64_t *)(var_0 + (-88L));
    *var_18 = rsi;
    *(uint64_t *)(var_0 + (-40L)) = 8UL;
    var_54 = 0U;
    var_29 = 1UL;
    rax_0 = 4611686018427387903UL;
    rax_1 = 12UL;
    if (!0) {
        if (0) {
            helper_cc_compute_c_wrapper(18446744073709551608UL, 16UL, var_2, 17U);
        } else {
        }
    }
    var_19 = (uint64_t *)(var_0 + (-48L));
    *var_19 = 24UL;
    var_20 = *var_17;
    *(uint64_t *)(var_0 + (-96L)) = 4254707UL;
    indirect_placeholder();
    *(uint32_t *)(*var_17 + 128UL) = 15U;
    var_21 = helper_divq_EAX_wrapper((struct type_5 *)(0UL), *var_19, 4254733UL, 18446744073709551615UL, var_15, 0UL, var_3, var_4, var_5, var_20, 0UL, var_6, var_7, var_8, var_9, var_10, var_11, var_12, var_13, var_14);
    if ((long)var_21.field_1 < (long)0UL) {
        var_22 = helper_divq_EAX_wrapper((struct type_5 *)(0UL), *var_19, 4254754UL, 18446744073709551615UL, var_15, 0UL, var_3, var_4, var_5, var_20, 0UL, var_6, var_7, var_21.field_6, var_21.field_7, var_10, var_11, var_21.field_8, var_21.field_9, var_21.field_10);
        rax_0 = var_22.field_1 >> 1UL;
    }
    var_23 = *var_18;
    if (rax_0 > var_23) {
        return rax_1;
    }
    *(uint64_t *)(*var_17 + 8UL) = (var_23 + 1UL);
    var_24 = *(uint64_t *)(*var_17 + 8UL) << 4UL;
    *(uint64_t *)(var_0 + (-104L)) = 4254834UL;
    var_25 = indirect_placeholder_73(var_24);
    var_26 = var_25.field_0;
    var_27 = (uint64_t **)var_16;
    **var_27 = var_26;
    var_28 = (uint64_t *)(var_0 + (-16L));
    *var_28 = 1UL;
    rax_1 = 0UL;
    while (var_29 <= *var_18)
        {
            var_30 = var_29 << 1UL;
            *var_28 = var_30;
            var_29 = var_30;
        }
    switch (*(unsigned char *)(var_39 + 2UL)) {
      case 'f':
      case 'F':
        {
            var_42 = (uint64_t)((uint32_t)var_39 + (uint32_t)((*(unsigned char *)(var_39 + 3UL) == '-') ? 4UL : 3UL));
            var_43 = var_0 + (-136L);
            *(uint64_t *)var_43 = 4255082UL;
            indirect_placeholder();
            local_sp_2 = var_43;
            if (var_42 == 0UL) {
                var_44 = (unsigned char *)(*var_17 + 176UL);
                *var_44 = (*var_44 | '\x04');
            }
        }
        break;
    }
}

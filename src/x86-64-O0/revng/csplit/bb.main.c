typedef unsigned char __u_char;
typedef unsigned short __u_short;
typedef unsigned int __u_int;
typedef unsigned long __u_long;
typedef signed char __int8_t;
typedef unsigned char __uint8_t;
typedef short __int16_t;
typedef unsigned short __uint16_t;
typedef int __int32_t;
typedef unsigned int __uint32_t;
typedef long __int64_t;
typedef unsigned long __uint64_t;
typedef __int8_t __int_least8_t;
typedef __uint8_t __uint_least8_t;
typedef __int16_t __int_least16_t;
typedef __uint16_t __uint_least16_t;
typedef __int32_t __int_least32_t;
typedef __uint32_t __uint_least32_t;
typedef __int64_t __int_least64_t;
typedef __uint64_t __uint_least64_t;
typedef long __quad_t;
typedef unsigned long __u_quad_t;
typedef long __intmax_t;
typedef unsigned long __uintmax_t;
typedef unsigned long __dev_t;
typedef unsigned int __uid_t;
typedef unsigned int __gid_t;
typedef unsigned long __ino_t;
typedef unsigned long __ino64_t;
typedef unsigned int __mode_t;
typedef unsigned long __nlink_t;
typedef long __off_t;
typedef long __off64_t;
typedef int __pid_t;
typedef struct {
    int __val[2];
} __fsid_t;
typedef long __clock_t;
typedef unsigned long __rlim_t;
typedef unsigned long __rlim64_t;
typedef unsigned int __id_t;
typedef long __time_t;
typedef unsigned int __useconds_t;
typedef long __suseconds_t;
typedef int __daddr_t;
typedef int __key_t;
typedef int __clockid_t;
typedef void *__timer_t;
typedef long __blksize_t;
typedef long __blkcnt_t;
typedef long __blkcnt64_t;
typedef unsigned long __fsblkcnt_t;
typedef unsigned long __fsblkcnt64_t;
typedef unsigned long __fsfilcnt_t;
typedef unsigned long __fsfilcnt64_t;
typedef long __fsword_t;
typedef long __ssize_t;
typedef long __syscall_slong_t;
typedef unsigned long __syscall_ulong_t;
typedef __off64_t __loff_t;
typedef char *__caddr_t;
typedef long __intptr_t;
typedef unsigned int __socklen_t;
typedef int __sig_atomic_t;
typedef __int8_t int8_t;
typedef __int16_t int16_t;
typedef __int32_t int32_t;
typedef __int64_t int64_t;
typedef __uint8_t uint8_t;
typedef __uint16_t uint16_t;
typedef __uint32_t uint32_t;
typedef __uint64_t uint64_t;
typedef __int_least8_t int_least8_t;
typedef __int_least16_t int_least16_t;
typedef __int_least32_t int_least32_t;
typedef __int_least64_t int_least64_t;
typedef __uint_least8_t uint_least8_t;
typedef __uint_least16_t uint_least16_t;
typedef __uint_least32_t uint_least32_t;
typedef __uint_least64_t uint_least64_t;
typedef signed char int_fast8_t;
typedef long int_fast16_t;
typedef long int_fast32_t;
typedef long int_fast64_t;
typedef unsigned char uint_fast8_t;
typedef unsigned long uint_fast16_t;
typedef unsigned long uint_fast32_t;
typedef unsigned long uint_fast64_t;
typedef long intptr_t;
typedef unsigned long uintptr_t;
typedef __intmax_t intmax_t;
typedef __uintmax_t uintmax_t;
struct indirect_placeholder_24_ret_type;
struct indirect_placeholder_27_ret_type;
struct indirect_placeholder_26_ret_type;
struct indirect_placeholder_29_ret_type;
struct indirect_placeholder_30_ret_type;
struct indirect_placeholder_28_ret_type;
struct indirect_placeholder_32_ret_type;
struct indirect_placeholder_24_ret_type {
    uint64_t field_0;
    uint64_t field_1;
    uint64_t field_2;
    uint64_t field_3;
};
struct indirect_placeholder_27_ret_type {
    uint64_t field_0;
    uint64_t field_1;
    uint64_t field_2;
};
struct indirect_placeholder_26_ret_type {
    uint64_t field_0;
    uint64_t field_1;
    uint64_t field_2;
};
struct indirect_placeholder_29_ret_type {
    uint64_t field_0;
    uint64_t field_1;
    uint64_t field_2;
    uint64_t field_3;
};
struct indirect_placeholder_30_ret_type {
    uint64_t field_0;
    uint64_t field_1;
    uint64_t field_2;
};
struct indirect_placeholder_28_ret_type {
    uint64_t field_0;
    uint64_t field_1;
    uint64_t field_2;
    uint64_t field_3;
};
struct indirect_placeholder_32_ret_type {
    uint64_t field_0;
    uint64_t field_1;
    uint64_t field_2;
    uint64_t field_3;
};
extern void abort(void);
extern uint64_t revng_init_local_sp(uint64_t param_0);
extern uint64_t init_rbp(void);
extern void indirect_placeholder(void);
extern uint64_t init_cc_src2(void);
extern uint64_t helper_cc_compute_c_wrapper(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint32_t param_3);
extern uint64_t indirect_placeholder_8(uint64_t param_0, uint64_t param_1, uint64_t param_2);
extern void indirect_placeholder_5(uint64_t param_0, uint64_t param_1);
extern uint64_t indirect_placeholder_11(uint64_t param_0);
extern uint64_t indirect_placeholder_10(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint64_t param_3, uint64_t param_4, uint64_t param_5);
extern void indirect_placeholder_9(uint64_t param_0);
extern void indirect_placeholder_31(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint64_t param_3, uint64_t param_4, uint64_t param_5);
extern struct indirect_placeholder_24_ret_type indirect_placeholder_24(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint64_t param_3, uint64_t param_4, uint64_t param_5, uint64_t param_6);
extern struct indirect_placeholder_27_ret_type indirect_placeholder_27(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint64_t param_3);
extern struct indirect_placeholder_26_ret_type indirect_placeholder_26(uint64_t param_0);
extern void indirect_placeholder_25(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint64_t param_3, uint64_t param_4, uint64_t param_5, uint64_t param_6);
extern struct indirect_placeholder_29_ret_type indirect_placeholder_29(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint64_t param_3, uint64_t param_4, uint64_t param_5, uint64_t param_6);
extern struct indirect_placeholder_30_ret_type indirect_placeholder_30(uint64_t param_0);
extern struct indirect_placeholder_28_ret_type indirect_placeholder_28(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint64_t param_3, uint64_t param_4, uint64_t param_5, uint64_t param_6);
extern struct indirect_placeholder_32_ret_type indirect_placeholder_32(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint64_t param_3, uint64_t param_4);
void bb_main(uint64_t rdi, uint64_t rsi) {
    struct indirect_placeholder_32_ret_type var_13;
    uint64_t var_0;
    uint64_t var_1;
    uint64_t var_2;
    uint64_t var_3;
    uint32_t *var_4;
    uint64_t var_5;
    uint64_t *var_6;
    uint64_t var_7;
    uint64_t var_8;
    uint32_t *var_9;
    uint64_t local_sp_7;
    uint64_t rcx_1;
    uint64_t local_sp_3;
    uint64_t var_68;
    uint32_t var_65;
    uint64_t var_66;
    uint32_t rax_0_in;
    uint64_t local_sp_0;
    uint64_t rcx_0;
    uint64_t rax_0;
    uint32_t var_67;
    uint32_t var_62;
    uint64_t var_59;
    uint64_t local_sp_1;
    uint32_t var_60;
    uint32_t var_57;
    uint64_t local_sp_2;
    uint64_t var_61;
    uint64_t rax_1;
    uint32_t var_63;
    uint64_t var_64;
    uint64_t var_58;
    uint64_t var_37;
    struct indirect_placeholder_27_ret_type var_38;
    uint64_t rax_2;
    uint32_t var_39;
    uint64_t var_12;
    uint64_t local_sp_4;
    uint64_t r8_0;
    uint64_t r9_0;
    uint64_t *var_40;
    uint64_t var_41;
    uint64_t var_42;
    uint64_t var_43;
    uint32_t var_44;
    uint64_t var_45;
    struct indirect_placeholder_26_ret_type var_46;
    uint64_t var_47;
    uint64_t var_48;
    uint64_t var_49;
    uint64_t var_50;
    uint64_t var_51;
    uint64_t var_52;
    uint64_t var_53;
    uint32_t *var_54;
    uint64_t var_55;
    uint64_t *var_56;
    uint64_t local_sp_5;
    uint64_t var_21;
    uint64_t var_22;
    uint64_t var_23;
    uint32_t var_24;
    uint32_t var_25;
    uint64_t var_26;
    uint64_t var_27;
    struct indirect_placeholder_30_ret_type var_28;
    uint64_t var_29;
    uint64_t var_30;
    uint64_t var_31;
    uint64_t var_32;
    uint64_t var_33;
    uint64_t var_34;
    uint64_t *var_35;
    uint64_t var_36;
    uint64_t local_sp_6;
    uint64_t var_15;
    uint64_t var_16;
    uint64_t *var_17;
    uint64_t var_18;
    uint64_t var_19;
    uint64_t var_20;
    uint64_t local_sp_7_be;
    uint64_t var_10;
    uint64_t var_11;
    uint32_t var_14;
    unsigned int loop_state_var;
    var_0 = revng_init_local_sp(0UL);
    var_1 = init_rbp();
    var_2 = init_cc_src2();
    var_3 = var_0 + (-8L);
    *(uint64_t *)var_3 = var_1;
    var_4 = (uint32_t *)(var_0 + (-188L));
    *var_4 = (uint32_t)rdi;
    var_5 = var_0 + (-200L);
    var_6 = (uint64_t *)var_5;
    *var_6 = rsi;
    var_7 = **(uint64_t **)var_5;
    *(uint64_t *)(var_0 + (-208L)) = 4212165UL;
    indirect_placeholder_9(var_7);
    *(uint64_t *)(var_0 + (-216L)) = 4212180UL;
    indirect_placeholder();
    var_8 = var_0 + (-224L);
    *(uint64_t *)var_8 = 4212190UL;
    indirect_placeholder();
    *(uint64_t *)6493864UL = *var_6;
    *(uint64_t *)6493880UL = 0UL;
    *(uint64_t *)6493888UL = 0UL;
    *(unsigned char *)6493872UL = (unsigned char)'\x00';
    *(unsigned char *)6493873UL = (unsigned char)'\x01';
    *(unsigned char *)6493875UL = (unsigned char)'\x00';
    *(uint64_t *)6493816UL = 4370150UL;
    var_9 = (uint32_t *)(var_0 + (-16L));
    rax_0_in = 0U;
    var_57 = 0U;
    var_62 = 0U;
    local_sp_7 = var_8;
    while (1U)
        {
            var_10 = *var_6;
            var_11 = (uint64_t)*var_4;
            var_12 = local_sp_7 + (-8L);
            *(uint64_t *)var_12 = 4212630UL;
            var_13 = indirect_placeholder_32(4370203UL, 4368832UL, 0UL, var_11, var_10);
            var_14 = (uint32_t)var_13.field_0;
            *var_9 = var_14;
            local_sp_7_be = var_12;
            local_sp_6 = var_12;
            if (var_14 != 4294967295U) {
                var_21 = var_13.field_1;
                var_22 = var_13.field_2;
                var_23 = var_13.field_3;
                var_24 = *(uint32_t *)6493528UL;
                var_25 = *var_4;
                r8_0 = var_22;
                r9_0 = var_23;
                if ((int)(var_25 - var_24) <= (int)1U) {
                    var_33 = *(uint64_t *)6493816UL;
                    var_34 = local_sp_7 + (-16L);
                    *(uint64_t *)var_34 = 4212795UL;
                    indirect_placeholder();
                    var_35 = (uint64_t *)(var_0 + (-24L));
                    *var_35 = var_33;
                    var_36 = *(uint64_t *)6493824UL;
                    local_sp_4 = var_34;
                    if (var_36 != 0UL) {
                        var_39 = *(uint32_t *)6493376UL;
                        rax_2 = (var_39 > 9U) ? (uint64_t)var_39 : 10UL;
                        loop_state_var = 0U;
                        break;
                    }
                    var_37 = local_sp_7 + (-24L);
                    *(uint64_t *)var_37 = 4212826UL;
                    var_38 = indirect_placeholder_27(var_21, var_22, var_36, var_23);
                    rax_2 = var_38.field_0;
                    local_sp_4 = var_37;
                    r8_0 = var_38.field_1;
                    r9_0 = var_38.field_2;
                    loop_state_var = 0U;
                    break;
                }
                if ((int)var_24 < (int)var_25) {
                    var_26 = local_sp_7 + (-16L);
                    *(uint64_t *)var_26 = 4212703UL;
                    indirect_placeholder_29(0UL, 4370214UL, var_21, var_22, 0UL, 0UL, var_23);
                    local_sp_5 = var_26;
                    loop_state_var = 2U;
                    break;
                }
                var_27 = *(uint64_t *)(*var_6 + (((uint64_t)var_25 << 3UL) + (-8L)));
                *(uint64_t *)(local_sp_7 + (-16L)) = 4212742UL;
                var_28 = indirect_placeholder_30(var_27);
                var_29 = var_28.field_0;
                var_30 = var_28.field_1;
                var_31 = var_28.field_2;
                var_32 = local_sp_7 + (-24L);
                *(uint64_t *)var_32 = 4212770UL;
                indirect_placeholder_28(0UL, 4370230UL, var_29, var_30, 0UL, 0UL, var_31);
                local_sp_5 = var_32;
                loop_state_var = 2U;
                break;
            }
            if ((uint64_t)(var_14 + (-107)) == 0UL) {
                *(unsigned char *)6493873UL = (unsigned char)'\x00';
            } else {
                if ((int)var_14 > (int)107U) {
                    if ((uint64_t)(var_14 + (-115)) == 0UL) {
                        *(unsigned char *)6493872UL = (unsigned char)'\x01';
                    } else {
                        if ((int)var_14 > (int)115U) {
                            if ((uint64_t)(var_14 + (-122)) == 0UL) {
                                *(unsigned char *)6493874UL = (unsigned char)'\x01';
                            } else {
                                if ((uint64_t)(var_14 + (-128)) != 0UL) {
                                    loop_state_var = 1U;
                                    break;
                                }
                                *(unsigned char *)6493875UL = (unsigned char)'\x01';
                            }
                        } else {
                            if ((uint64_t)(var_14 + (-110)) == 0UL) {
                                var_18 = *(uint64_t *)6494584UL;
                                var_19 = local_sp_7 + (-16L);
                                *(uint64_t *)var_19 = 4212467UL;
                                var_20 = indirect_placeholder_10(2147483647UL, 4368643UL, 4370153UL, var_18, 0UL, 0UL);
                                *(uint32_t *)6493376UL = (uint32_t)var_20;
                                local_sp_7_be = var_19;
                            } else {
                                if ((uint64_t)(var_14 + (-113)) != 0UL) {
                                    loop_state_var = 1U;
                                    break;
                                }
                                *(unsigned char *)6493872UL = (unsigned char)'\x01';
                            }
                        }
                    }
                } else {
                    if ((uint64_t)(var_14 + 130U) == 0UL) {
                        *(uint64_t *)(local_sp_7 + (-16L)) = 4212512UL;
                        indirect_placeholder_5(var_3, 0UL);
                        abort();
                    }
                    if ((int)var_14 <= (int)4294967166U) {
                        if (var_14 != 4294967165U) {
                            loop_state_var = 1U;
                            break;
                        }
                        var_15 = *(uint64_t *)6493384UL;
                        var_16 = local_sp_7 + (-24L);
                        var_17 = (uint64_t *)var_16;
                        *var_17 = 0UL;
                        *(uint64_t *)(local_sp_7 + (-32L)) = 4212570UL;
                        indirect_placeholder_31(0UL, 4368480UL, var_15, 4370184UL, 4370196UL, 4370168UL);
                        *var_17 = 4212584UL;
                        indirect_placeholder();
                        local_sp_6 = var_16;
                        loop_state_var = 1U;
                        break;
                    }
                    if ((uint64_t)(var_14 + (-98)) == 0UL) {
                        *(uint64_t *)6493824UL = *(uint64_t *)6494584UL;
                    } else {
                        if ((uint64_t)(var_14 + (-102)) != 0UL) {
                            loop_state_var = 1U;
                            break;
                        }
                        *(uint64_t *)6493816UL = *(uint64_t *)6494584UL;
                    }
                }
            }
            local_sp_7 = local_sp_7_be;
            continue;
        }
    switch (loop_state_var) {
      case 1U:
        {
            *(uint64_t *)(local_sp_6 + (-8L)) = 4212594UL;
            indirect_placeholder_5(var_3, 1UL);
            abort();
        }
        break;
      case 2U:
        {
            *(uint64_t *)(local_sp_5 + (-8L)) = 4212780UL;
            indirect_placeholder_5(var_3, 1UL);
            abort();
        }
        break;
      case 0U:
        {
            var_40 = (uint64_t *)(var_0 + (-32L));
            *var_40 = rax_2;
            var_41 = helper_cc_compute_c_wrapper((18446744073709551614UL - (-2L)) - rax_2, rax_2, var_2, 17U);
            if (var_41 == 0UL) {
                *(uint64_t *)(local_sp_4 + (-8L)) = 4212880UL;
                indirect_placeholder_8(var_3, r8_0, r9_0);
                abort();
            }
            var_42 = (*var_40 + *var_35) + 1UL;
            *(uint64_t *)(local_sp_4 + (-8L)) = 4212903UL;
            var_43 = indirect_placeholder_11(var_42);
            *(uint64_t *)6493808UL = var_43;
            var_44 = *(uint32_t *)6493528UL;
            *(uint32_t *)6493528UL = (var_44 + 1U);
            var_45 = *(uint64_t *)(*var_6 + ((uint64_t)var_44 << 3UL));
            *(uint64_t *)(local_sp_4 + (-16L)) = 4212956UL;
            var_46 = indirect_placeholder_26(var_45);
            var_47 = var_46.field_0;
            var_48 = var_46.field_1;
            var_49 = var_46.field_2;
            var_50 = (uint64_t)*(uint32_t *)6493528UL;
            var_51 = *var_6;
            var_52 = (uint64_t)*var_4;
            *(uint64_t *)(local_sp_4 + (-24L)) = 4212984UL;
            indirect_placeholder_25(var_51, var_50, var_47, var_52, var_50, var_48, var_49);
            var_53 = local_sp_4 + (-32L);
            *(uint64_t *)var_53 = 4212994UL;
            indirect_placeholder();
            var_54 = (uint32_t *)(var_0 + (-12L));
            *var_54 = 0U;
            var_55 = var_0 + (-184L);
            var_56 = (uint64_t *)var_55;
            local_sp_2 = var_53;
            rcx_1 = var_50;
            local_sp_3 = local_sp_2;
            while ((int)var_57 <= (int)10U)
                {
                    var_58 = local_sp_2 + (-8L);
                    *(uint64_t *)var_58 = 4213034UL;
                    indirect_placeholder();
                    local_sp_1 = var_58;
                    if (*var_56 == 1UL) {
                        var_59 = local_sp_2 + (-16L);
                        *(uint64_t *)var_59 = 4213071UL;
                        indirect_placeholder();
                        local_sp_1 = var_59;
                    }
                    var_60 = *var_54 + 1U;
                    *var_54 = var_60;
                    var_57 = var_60;
                    local_sp_2 = local_sp_1;
                    local_sp_3 = local_sp_2;
                }
            *var_56 = 4203971UL;
            *(uint64_t *)(var_0 + (-176L)) = *(uint64_t *)6493952UL;
            *(uint64_t *)(var_0 + (-168L)) = *(uint64_t *)6493960UL;
            *(uint64_t *)(var_0 + (-160L)) = *(uint64_t *)6493968UL;
            *(uint64_t *)(var_0 + (-152L)) = *(uint64_t *)6493976UL;
            *(uint64_t *)(var_0 + (-144L)) = *(uint64_t *)6493984UL;
            *(uint64_t *)(var_0 + (-136L)) = *(uint64_t *)6493992UL;
            *(uint64_t *)(var_0 + (-128L)) = *(uint64_t *)6494000UL;
            *(uint64_t *)(var_0 + (-120L)) = *(uint64_t *)6494008UL;
            *(uint64_t *)(var_0 + (-112L)) = *(uint64_t *)6494016UL;
            *(uint64_t *)(var_0 + (-104L)) = *(uint64_t *)6494024UL;
            *(uint64_t *)(var_0 + (-96L)) = *(uint64_t *)6494032UL;
            *(uint64_t *)(var_0 + (-88L)) = *(uint64_t *)6494040UL;
            *(uint64_t *)(var_0 + (-80L)) = *(uint64_t *)6494048UL;
            *(uint64_t *)(var_0 + (-72L)) = *(uint64_t *)6494056UL;
            *(uint64_t *)(var_0 + (-64L)) = *(uint64_t *)6494064UL;
            var_61 = *(uint64_t *)6494072UL;
            *(uint64_t *)(var_0 + (-56L)) = var_61;
            *(uint32_t *)(var_0 + (-48L)) = 0U;
            *var_54 = 0U;
            rax_1 = var_61;
            rcx_0 = rcx_1;
            while ((int)var_62 <= (int)10U)
                {
                    var_63 = *(uint32_t *)(((uint64_t)var_62 << 2UL) + 4371552UL);
                    var_64 = local_sp_3 + (-8L);
                    *(uint64_t *)var_64 = 4213323UL;
                    indirect_placeholder();
                    local_sp_0 = var_64;
                    if (var_63 == 0U) {
                        var_65 = *(uint32_t *)(((uint64_t)*var_54 << 2UL) + 4371552UL);
                        var_66 = local_sp_3 + (-16L);
                        *(uint64_t *)var_66 = 4213361UL;
                        indirect_placeholder();
                        rax_0_in = var_65;
                        local_sp_0 = var_66;
                        rcx_0 = var_55;
                    }
                    rax_0 = (uint64_t)rax_0_in;
                    var_67 = *var_54 + 1U;
                    *var_54 = var_67;
                    var_62 = var_67;
                    rax_1 = rax_0;
                    local_sp_3 = local_sp_0;
                    rcx_1 = rcx_0;
                    rcx_0 = rcx_1;
                }
            *(uint64_t *)(local_sp_3 + (-8L)) = 4213376UL;
            indirect_placeholder();
            *(uint64_t *)(local_sp_3 + (-16L)) = 4213386UL;
            indirect_placeholder();
            if ((uint64_t)(uint32_t)rax_1 == 0UL) {
                return;
            }
            *(uint64_t *)(local_sp_3 + (-24L)) = 4213395UL;
            indirect_placeholder();
            var_68 = (uint64_t)*(uint32_t *)rax_1;
            *(uint64_t *)(local_sp_3 + (-32L)) = 4213419UL;
            indirect_placeholder_24(0UL, 4369204UL, rcx_1, var_47, 0UL, var_68, var_48);
            *(uint64_t *)(local_sp_3 + (-40L)) = 4213424UL;
            indirect_placeholder_11(var_3);
            abort();
        }
        break;
    }
}

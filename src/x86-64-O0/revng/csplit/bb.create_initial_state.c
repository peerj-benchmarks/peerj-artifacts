typedef unsigned char __u_char;
typedef unsigned short __u_short;
typedef unsigned int __u_int;
typedef unsigned long __u_long;
typedef signed char __int8_t;
typedef unsigned char __uint8_t;
typedef short __int16_t;
typedef unsigned short __uint16_t;
typedef int __int32_t;
typedef unsigned int __uint32_t;
typedef long __int64_t;
typedef unsigned long __uint64_t;
typedef __int8_t __int_least8_t;
typedef __uint8_t __uint_least8_t;
typedef __int16_t __int_least16_t;
typedef __uint16_t __uint_least16_t;
typedef __int32_t __int_least32_t;
typedef __uint32_t __uint_least32_t;
typedef __int64_t __int_least64_t;
typedef __uint64_t __uint_least64_t;
typedef long __quad_t;
typedef unsigned long __u_quad_t;
typedef long __intmax_t;
typedef unsigned long __uintmax_t;
typedef unsigned long __dev_t;
typedef unsigned int __uid_t;
typedef unsigned int __gid_t;
typedef unsigned long __ino_t;
typedef unsigned long __ino64_t;
typedef unsigned int __mode_t;
typedef unsigned long __nlink_t;
typedef long __off_t;
typedef long __off64_t;
typedef int __pid_t;
typedef struct {
    int __val[2];
} __fsid_t;
typedef long __clock_t;
typedef unsigned long __rlim_t;
typedef unsigned long __rlim64_t;
typedef unsigned int __id_t;
typedef long __time_t;
typedef unsigned int __useconds_t;
typedef long __suseconds_t;
typedef int __daddr_t;
typedef int __key_t;
typedef int __clockid_t;
typedef void *__timer_t;
typedef long __blksize_t;
typedef long __blkcnt_t;
typedef long __blkcnt64_t;
typedef unsigned long __fsblkcnt_t;
typedef unsigned long __fsblkcnt64_t;
typedef unsigned long __fsfilcnt_t;
typedef unsigned long __fsfilcnt64_t;
typedef long __fsword_t;
typedef long __ssize_t;
typedef long __syscall_slong_t;
typedef unsigned long __syscall_ulong_t;
typedef __off64_t __loff_t;
typedef char *__caddr_t;
typedef long __intptr_t;
typedef unsigned int __socklen_t;
typedef int __sig_atomic_t;
typedef __int8_t int8_t;
typedef __int16_t int16_t;
typedef __int32_t int32_t;
typedef __int64_t int64_t;
typedef __uint8_t uint8_t;
typedef __uint16_t uint16_t;
typedef __uint32_t uint32_t;
typedef __uint64_t uint64_t;
typedef __int_least8_t int_least8_t;
typedef __int_least16_t int_least16_t;
typedef __int_least32_t int_least32_t;
typedef __int_least64_t int_least64_t;
typedef __uint_least8_t uint_least8_t;
typedef __uint_least16_t uint_least16_t;
typedef __uint_least32_t uint_least32_t;
typedef __uint_least64_t uint_least64_t;
typedef signed char int_fast8_t;
typedef long int_fast16_t;
typedef long int_fast32_t;
typedef long int_fast64_t;
typedef unsigned char uint_fast8_t;
typedef unsigned long uint_fast16_t;
typedef unsigned long uint_fast32_t;
typedef unsigned long uint_fast64_t;
typedef long intptr_t;
typedef unsigned long uintptr_t;
typedef __intmax_t intmax_t;
typedef __uintmax_t uintmax_t;
struct indirect_placeholder_137_ret_type;
struct indirect_placeholder_136_ret_type;
struct indirect_placeholder_135_ret_type;
struct indirect_placeholder_138_ret_type;
struct indirect_placeholder_137_ret_type {
    uint64_t field_0;
    uint64_t field_1;
    uint64_t field_2;
    uint64_t field_3;
    uint64_t field_4;
};
struct indirect_placeholder_136_ret_type {
    uint64_t field_0;
    uint64_t field_1;
    uint64_t field_2;
    uint64_t field_3;
    uint64_t field_4;
};
struct indirect_placeholder_135_ret_type {
    uint64_t field_0;
    uint64_t field_1;
    uint64_t field_2;
    uint64_t field_3;
    uint64_t field_4;
};
struct indirect_placeholder_138_ret_type {
    uint64_t field_0;
    uint64_t field_1;
    uint64_t field_2;
    uint64_t field_3;
    uint64_t field_4;
};
extern uint64_t revng_init_local_sp(uint64_t param_0);
extern uint64_t init_rbp(void);
extern void indirect_placeholder(void);
extern uint64_t indirect_placeholder_1(uint64_t param_0, uint64_t param_1);
extern uint64_t helper_cc_compute_all_wrapper(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint32_t param_3);
extern struct indirect_placeholder_137_ret_type indirect_placeholder_137(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint64_t param_3);
extern struct indirect_placeholder_136_ret_type indirect_placeholder_136(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint64_t param_3);
extern struct indirect_placeholder_135_ret_type indirect_placeholder_135(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint64_t param_3);
extern struct indirect_placeholder_138_ret_type indirect_placeholder_138(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint64_t param_3);
uint64_t bb_create_initial_state(uint64_t rdi) {
    uint64_t var_0;
    uint64_t var_1;
    uint64_t var_2;
    uint64_t *var_3;
    uint64_t var_4;
    uint64_t *var_5;
    uint64_t var_6;
    uint64_t var_7;
    uint64_t var_8;
    uint64_t var_9;
    uint64_t var_10;
    uint32_t *var_11;
    uint32_t var_12;
    uint32_t rax_0_shrunk;
    uint64_t local_sp_0;
    uint32_t var_37;
    uint32_t var_40;
    struct indirect_placeholder_137_ret_type var_30;
    uint64_t var_31;
    struct indirect_placeholder_136_ret_type var_32;
    uint64_t var_33;
    uint64_t var_34;
    struct indirect_placeholder_135_ret_type var_35;
    uint64_t var_36;
    uint64_t var_38;
    uint64_t var_39;
    uint64_t local_sp_2;
    uint64_t var_53;
    uint64_t var_54;
    uint64_t var_55;
    uint32_t var_56;
    uint64_t var_13;
    uint64_t local_sp_3;
    uint64_t *var_14;
    uint64_t *var_15;
    uint64_t *var_16;
    uint64_t *var_17;
    uint32_t *var_18;
    uint64_t *var_19;
    uint64_t var_20;
    uint64_t *var_21;
    uint64_t *var_22;
    uint32_t *var_23;
    uint64_t var_24;
    uint64_t local_sp_1;
    uint64_t var_41;
    uint64_t **var_42;
    uint32_t var_43;
    uint64_t var_44;
    uint64_t var_45;
    uint64_t var_46;
    uint64_t var_48;
    uint64_t var_49;
    uint64_t var_50;
    uint64_t var_51;
    uint64_t var_52;
    uint64_t var_57;
    uint64_t var_47;
    uint64_t var_25;
    uint64_t var_26;
    struct indirect_placeholder_138_ret_type var_27;
    uint64_t var_28;
    uint64_t var_29;
    unsigned int loop_state_var;
    var_0 = revng_init_local_sp(0UL);
    var_1 = init_rbp();
    *(uint64_t *)(var_0 + (-8L)) = var_1;
    var_2 = var_0 + (-112L);
    var_3 = (uint64_t *)var_2;
    *var_3 = rdi;
    var_4 = *(uint64_t *)(*(uint64_t *)(*(uint64_t *)(rdi + 104UL) + 24UL) + 56UL);
    var_5 = (uint64_t *)(var_0 + (-32L));
    *var_5 = var_4;
    *(uint64_t *)(*var_3 + 144UL) = var_4;
    var_6 = (*var_5 * 24UL) + *(uint64_t *)(*var_3 + 48UL);
    var_7 = var_0 + (-104L);
    var_8 = var_0 + (-128L);
    *(uint64_t *)var_8 = 4256100UL;
    var_9 = indirect_placeholder_1(var_7, var_6);
    var_10 = var_0 + (-72L);
    var_11 = (uint32_t *)var_10;
    var_12 = (uint32_t)var_9;
    *var_11 = var_12;
    rax_0_shrunk = var_12;
    local_sp_3 = var_8;
    var_24 = 0UL;
    local_sp_1 = var_8;
    var_44 = 0UL;
    if (var_12 == 0U) {
        return (uint64_t)rax_0_shrunk;
    }
    var_13 = helper_cc_compute_all_wrapper(*(uint64_t *)(*var_3 + 152UL), 0UL, 0UL, 25U);
    rax_0_shrunk = 0U;
    if ((uint64_t)(((unsigned char)(var_13 >> 4UL) ^ (unsigned char)var_13) & '\xc0') != 0UL) {
        var_14 = (uint64_t *)(var_0 + (-16L));
        *var_14 = 0UL;
        var_15 = (uint64_t *)(var_0 + (-96L));
        var_16 = (uint64_t *)(var_0 + (-88L));
        var_17 = (uint64_t *)(var_0 + (-40L));
        var_18 = (uint32_t *)(var_0 + (-44L));
        var_19 = (uint64_t *)(var_0 + (-24L));
        var_20 = var_0 + (-56L);
        var_21 = (uint64_t *)var_20;
        var_22 = (uint64_t *)(var_0 + (-64L));
        var_23 = (uint32_t *)(var_0 + (-68L));
        while (1U)
            {
                local_sp_2 = local_sp_1;
                local_sp_3 = local_sp_1;
                if ((long)*var_15 <= (long)var_24) {
                    loop_state_var = 1U;
                    break;
                }
                var_41 = *(uint64_t *)(*var_16 + (var_24 << 3UL));
                *var_17 = var_41;
                var_42 = (uint64_t **)var_2;
                var_43 = (uint32_t)*(unsigned char *)((**var_42 + (var_41 << 4UL)) + 8UL);
                *var_18 = var_43;
                if (var_43 != 4U) {
                    *var_19 = 0UL;
                    while (1U)
                        {
                            var_45 = *var_15;
                            var_48 = var_44;
                            var_49 = var_45;
                            if ((long)var_45 <= (long)var_44) {
                                loop_state_var = 1U;
                                break;
                            }
                            var_46 = **var_42 + (*(uint64_t *)(*var_16 + (var_44 << 3UL)) << 4UL);
                            *var_21 = var_46;
                            if (*(unsigned char *)(var_46 + 8UL) != '\t') {
                                if (**(uint64_t **)var_20 != *(uint64_t *)(**var_42 + (*var_17 << 4UL))) {
                                    loop_state_var = 0U;
                                    break;
                                }
                            }
                            var_47 = *var_19 + 1UL;
                            *var_19 = var_47;
                            var_44 = var_47;
                            continue;
                        }
                    switch (loop_state_var) {
                      case 0U:
                        {
                            var_48 = *var_19;
                            var_49 = *var_15;
                        }
                        break;
                      case 1U:
                        {
                        }
                        break;
                    }
                    var_50 = **(uint64_t **)(((*var_17 * 24UL) + *(uint64_t *)(*var_3 + 40UL)) + 16UL);
                    *var_22 = var_50;
                    var_51 = local_sp_1 + (-8L);
                    *(uint64_t *)var_51 = 4256412UL;
                    var_52 = indirect_placeholder_1(var_7, var_50);
                    local_sp_2 = var_51;
                    if (var_49 != var_48 & *var_18 != 4U & var_52 != 0UL) {
                        var_53 = (*var_22 * 24UL) + *(uint64_t *)(*var_3 + 48UL);
                        var_54 = local_sp_1 + (-16L);
                        *(uint64_t *)var_54 = 4256461UL;
                        var_55 = indirect_placeholder_1(var_7, var_53);
                        var_56 = (uint32_t)var_55;
                        *var_23 = var_56;
                        rax_0_shrunk = var_56;
                        local_sp_2 = var_54;
                        if (var_56 != 0U) {
                            loop_state_var = 0U;
                            break;
                        }
                        *var_14 = 0UL;
                    }
                }
                var_57 = *var_14 + 1UL;
                *var_14 = var_57;
                var_24 = var_57;
                local_sp_1 = local_sp_2;
                continue;
            }
        switch (loop_state_var) {
          case 0U:
            {
                return (uint64_t)rax_0_shrunk;
            }
            break;
          case 1U:
            {
                break;
            }
            break;
        }
    }
    var_25 = *var_3;
    var_26 = local_sp_3 + (-8L);
    *(uint64_t *)var_26 = 4256530UL;
    var_27 = indirect_placeholder_138(var_7, 0UL, var_10, var_25);
    *(uint64_t *)(*var_3 + 72UL) = var_27.field_0;
    var_28 = *var_3;
    var_29 = *(uint64_t *)(var_28 + 72UL);
    local_sp_0 = var_26;
    if (var_29 == 0UL) {
        var_40 = *var_11;
        rax_0_shrunk = var_40;
    } else {
        if ((signed char)*(unsigned char *)(var_29 + 104UL) <= '\xff') {
            *(uint64_t *)(local_sp_3 + (-16L)) = 4256619UL;
            var_30 = indirect_placeholder_137(var_7, 1UL, var_10, var_28);
            *(uint64_t *)(*var_3 + 80UL) = var_30.field_0;
            var_31 = *var_3;
            *(uint64_t *)(local_sp_3 + (-24L)) = 4256655UL;
            var_32 = indirect_placeholder_136(var_7, 2UL, var_10, var_31);
            *(uint64_t *)(*var_3 + 88UL) = var_32.field_0;
            var_33 = *var_3;
            var_34 = local_sp_3 + (-32L);
            *(uint64_t *)var_34 = 4256691UL;
            var_35 = indirect_placeholder_135(var_7, 6UL, var_10, var_33);
            *(uint64_t *)(*var_3 + 96UL) = var_35.field_0;
            var_36 = *var_3;
            local_sp_0 = var_34;
            if (*(uint64_t *)(var_36 + 80UL) != 0UL) {
                var_37 = *var_11;
                rax_0_shrunk = var_37;
                return (uint64_t)rax_0_shrunk;
            }
            if (*(uint64_t *)(var_36 + 88UL) != 0UL) {
                var_37 = *var_11;
                rax_0_shrunk = var_37;
                return (uint64_t)rax_0_shrunk;
            }
            if (*(uint64_t *)(var_36 + 96UL) != 0UL) {
                var_37 = *var_11;
                rax_0_shrunk = var_37;
                return (uint64_t)rax_0_shrunk;
            }
        }
        *(uint64_t *)(var_28 + 96UL) = var_29;
        var_38 = *var_3;
        *(uint64_t *)(var_38 + 88UL) = *(uint64_t *)(var_38 + 96UL);
        var_39 = *var_3;
        *(uint64_t *)(var_39 + 80UL) = *(uint64_t *)(var_39 + 88UL);
    }
}

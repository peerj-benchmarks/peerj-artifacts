typedef unsigned char __u_char;
typedef unsigned short __u_short;
typedef unsigned int __u_int;
typedef unsigned long __u_long;
typedef signed char __int8_t;
typedef unsigned char __uint8_t;
typedef short __int16_t;
typedef unsigned short __uint16_t;
typedef int __int32_t;
typedef unsigned int __uint32_t;
typedef long __int64_t;
typedef unsigned long __uint64_t;
typedef __int8_t __int_least8_t;
typedef __uint8_t __uint_least8_t;
typedef __int16_t __int_least16_t;
typedef __uint16_t __uint_least16_t;
typedef __int32_t __int_least32_t;
typedef __uint32_t __uint_least32_t;
typedef __int64_t __int_least64_t;
typedef __uint64_t __uint_least64_t;
typedef long __quad_t;
typedef unsigned long __u_quad_t;
typedef long __intmax_t;
typedef unsigned long __uintmax_t;
typedef unsigned long __dev_t;
typedef unsigned int __uid_t;
typedef unsigned int __gid_t;
typedef unsigned long __ino_t;
typedef unsigned long __ino64_t;
typedef unsigned int __mode_t;
typedef unsigned long __nlink_t;
typedef long __off_t;
typedef long __off64_t;
typedef int __pid_t;
typedef struct {
    int __val[2];
} __fsid_t;
typedef long __clock_t;
typedef unsigned long __rlim_t;
typedef unsigned long __rlim64_t;
typedef unsigned int __id_t;
typedef long __time_t;
typedef unsigned int __useconds_t;
typedef long __suseconds_t;
typedef int __daddr_t;
typedef int __key_t;
typedef int __clockid_t;
typedef void *__timer_t;
typedef long __blksize_t;
typedef long __blkcnt_t;
typedef long __blkcnt64_t;
typedef unsigned long __fsblkcnt_t;
typedef unsigned long __fsblkcnt64_t;
typedef unsigned long __fsfilcnt_t;
typedef unsigned long __fsfilcnt64_t;
typedef long __fsword_t;
typedef long __ssize_t;
typedef long __syscall_slong_t;
typedef unsigned long __syscall_ulong_t;
typedef __off64_t __loff_t;
typedef char *__caddr_t;
typedef long __intptr_t;
typedef unsigned int __socklen_t;
typedef int __sig_atomic_t;
typedef __int8_t int8_t;
typedef __int16_t int16_t;
typedef __int32_t int32_t;
typedef __int64_t int64_t;
typedef __uint8_t uint8_t;
typedef __uint16_t uint16_t;
typedef __uint32_t uint32_t;
typedef __uint64_t uint64_t;
typedef __int_least8_t int_least8_t;
typedef __int_least16_t int_least16_t;
typedef __int_least32_t int_least32_t;
typedef __int_least64_t int_least64_t;
typedef __uint_least8_t uint_least8_t;
typedef __uint_least16_t uint_least16_t;
typedef __uint_least32_t uint_least32_t;
typedef __uint_least64_t uint_least64_t;
typedef signed char int_fast8_t;
typedef long int_fast16_t;
typedef long int_fast32_t;
typedef long int_fast64_t;
typedef unsigned char uint_fast8_t;
typedef unsigned long uint_fast16_t;
typedef unsigned long uint_fast32_t;
typedef unsigned long uint_fast64_t;
typedef long intptr_t;
typedef unsigned long uintptr_t;
typedef __intmax_t intmax_t;
typedef __uintmax_t uintmax_t;
typedef _Bool bool;
extern uint64_t revng_init_local_sp(uint64_t param_0);
extern uint64_t init_rbp(void);
extern void indirect_placeholder(void);
extern uint64_t indirect_placeholder_1(uint64_t param_0, uint64_t param_1);
uint64_t bb_re_node_set_add_intersect(uint64_t rdx, uint64_t rdi, uint64_t rsi) {
    uint64_t var_49;
    uint64_t var_37;
    uint64_t var_36;
    uint64_t var_0;
    uint64_t var_1;
    uint64_t var_2;
    uint64_t var_3;
    uint64_t *var_4;
    uint64_t *var_5;
    uint64_t *var_6;
    uint64_t *var_7;
    uint64_t var_8;
    uint64_t rax_0;
    uint64_t var_32;
    uint64_t *_pre_phi170;
    uint64_t *var_9;
    uint64_t var_10;
    uint64_t var_11;
    uint64_t *var_12;
    uint64_t var_13;
    uint64_t **var_14;
    uint64_t var_15;
    uint64_t var_16;
    uint64_t *var_17;
    uint64_t var_18;
    uint64_t var_19;
    uint64_t var_20;
    uint64_t var_21;
    uint64_t *_pre_phi166;
    uint64_t *_pre_phi162;
    uint64_t local_sp_0;
    uint64_t var_22;
    uint64_t *var_23;
    uint64_t var_24;
    uint64_t *var_25;
    uint64_t var_26;
    uint64_t *var_27;
    uint64_t var_28;
    uint64_t *var_29;
    uint64_t var_30;
    uint64_t var_31;
    uint64_t var_33;
    uint64_t var_34;
    uint64_t var_35;
    bool var_39;
    uint64_t var_38;
    uint64_t var_40;
    uint64_t *_pre_phi174;
    uint64_t *var_41;
    uint64_t var_42;
    uint64_t var_43;
    uint64_t var_44;
    uint64_t var_47;
    uint64_t var_45;
    uint64_t var_46;
    uint64_t _pre155;
    uint64_t var_48;
    uint64_t var_50;
    uint64_t *var_51;
    uint64_t var_52;
    uint64_t *var_53;
    uint64_t *var_54;
    uint64_t var_55;
    uint64_t var_56;
    uint64_t _pre156;
    uint64_t _pre157;
    uint64_t var_57;
    uint64_t var_58;
    uint64_t var_59;
    uint64_t var_60;
    uint64_t var_61;
    uint64_t var_63;
    uint64_t var_64;
    uint64_t var_65;
    uint64_t var_66;
    uint64_t var_62;
    var_0 = revng_init_local_sp(0UL);
    var_1 = init_rbp();
    *(uint64_t *)(var_0 + (-8L)) = var_1;
    var_2 = var_0 + (-104L);
    var_3 = var_0 + (-80L);
    var_4 = (uint64_t *)var_3;
    *var_4 = rdi;
    var_5 = (uint64_t *)(var_0 + (-88L));
    *var_5 = rsi;
    var_6 = (uint64_t *)(var_0 + (-96L));
    *var_6 = rdx;
    var_7 = (uint64_t *)(*var_5 + 8UL);
    var_8 = *var_7;
    _pre_phi166 = var_7;
    local_sp_0 = var_2;
    rax_0 = 0UL;
    var_9 = (uint64_t *)(rdx + 8UL);
    var_10 = *var_9;
    _pre_phi170 = var_9;
    if (~(var_8 != 0UL & var_10 != 0UL)) {
        return rax_0;
    }
    var_11 = var_8 + var_10;
    var_12 = (uint64_t *)(*var_4 + 8UL);
    var_13 = var_11 + *var_12;
    var_14 = (uint64_t **)var_3;
    var_15 = **var_14;
    _pre_phi162 = var_12;
    rax_0 = 12UL;
    if ((long)var_13 <= (long)var_15) {
        var_16 = var_15 + var_11;
        var_17 = (uint64_t *)(var_0 + (-64L));
        *var_17 = var_16;
        var_18 = var_16 << 3UL;
        var_19 = *(uint64_t *)(*var_4 + 16UL);
        var_20 = var_0 + (-112L);
        *(uint64_t *)var_20 = 4243803UL;
        var_21 = indirect_placeholder_1(var_19, var_18);
        *(uint64_t *)(var_0 + (-72L)) = var_21;
        local_sp_0 = var_20;
        if (var_21 != 0UL) {
            return rax_0;
        }
        *(uint64_t *)(*var_4 + 16UL) = var_21;
        **var_14 = *var_17;
        _pre_phi170 = (uint64_t *)(*var_6 + 8UL);
        _pre_phi166 = (uint64_t *)(*var_5 + 8UL);
        _pre_phi162 = (uint64_t *)(*var_4 + 8UL);
    }
    var_22 = *_pre_phi170 + (*_pre_phi162 + *_pre_phi166);
    var_23 = (uint64_t *)(var_0 + (-56L));
    *var_23 = var_22;
    var_24 = *(uint64_t *)(*var_5 + 8UL) + (-1L);
    var_25 = (uint64_t *)(var_0 + (-16L));
    *var_25 = var_24;
    var_26 = *(uint64_t *)(*var_6 + 8UL) + (-1L);
    var_27 = (uint64_t *)(var_0 + (-24L));
    *var_27 = var_26;
    var_28 = *(uint64_t *)(*var_4 + 8UL) + (-1L);
    var_29 = (uint64_t *)(var_0 + (-40L));
    *var_29 = var_28;
    while (1U)
        {
            var_30 = *(uint64_t *)(*var_5 + 16UL);
            var_31 = *var_25;
            var_32 = *(uint64_t *)(var_30 + (var_31 << 3UL));
            var_33 = *(uint64_t *)(*var_6 + 16UL);
            var_34 = *var_27;
            var_35 = *(uint64_t *)(var_33 + (var_34 << 3UL));
            if (var_32 != var_35) {
                if ((long)var_32 >= (long)var_35) {
                    var_36 = var_31 + (-1L);
                    *var_25 = var_36;
                    if ((long)var_36 <= (long)18446744073709551615UL) {
                        continue;
                    }
                    break;
                }
                var_37 = var_34 + (-1L);
                *var_27 = var_37;
                if ((long)var_37 <= (long)18446744073709551615UL) {
                    continue;
                }
                break;
            }
            var_38 = *var_29;
            var_39 = ((long)var_38 < (long)0UL);
            while (!var_39)
                {
                    if ((long)*(uint64_t *)(*(uint64_t *)(*var_4 + 16UL) + (var_38 << 3UL)) > (long)*(uint64_t *)(*(uint64_t *)(*var_5 + 16UL) + (*var_25 << 3UL))) {
                        break;
                    }
                    var_40 = var_38 + (-1L);
                    *var_29 = var_40;
                    var_38 = var_40;
                    var_39 = ((long)var_38 < (long)0UL);
                }
            if (!var_39) {
                var_41 = (uint64_t *)(*var_4 + 16UL);
                var_42 = *(uint64_t *)(*var_41 + (var_38 << 3UL));
                var_43 = *(uint64_t *)(*var_5 + 16UL);
                var_44 = *var_25;
                _pre_phi174 = var_41;
                var_47 = var_44;
                if (var_42 != *(uint64_t *)(var_43 + (var_44 << 3UL))) {
                    var_48 = var_47 + (-1L);
                    *var_25 = var_48;
                    if ((long)var_48 < (long)0UL) {
                        break;
                    }
                    var_49 = *var_27 + (-1L);
                    *var_27 = var_49;
                    if ((long)var_49 >= (long)0UL) {
                        continue;
                    }
                    break;
                }
            }
            _pre_phi174 = (uint64_t *)(*var_4 + 16UL);
        }
    *var_29 = (*(uint64_t *)(*var_4 + 8UL) + (-1L));
    var_50 = (*(uint64_t *)(*var_6 + 8UL) + (*(uint64_t *)(*var_4 + 8UL) + *(uint64_t *)(*var_5 + 8UL))) + (-1L);
    var_51 = (uint64_t *)(var_0 + (-32L));
    *var_51 = var_50;
    var_52 = (var_50 - *var_23) + 1UL;
    var_53 = (uint64_t *)(var_0 + (-48L));
    *var_53 = var_52;
    var_54 = (uint64_t *)(*var_4 + 8UL);
    *var_54 = (*var_54 + var_52);
    var_55 = *var_53;
    var_57 = var_55;
    var_56 = *var_29;
    var_58 = var_56;
    if ((long)var_55 <= (long)0UL & (long)var_56 < (long)0UL) {
        while (1U)
            {
                var_59 = *(uint64_t *)(*var_4 + 16UL);
                var_60 = *(uint64_t *)(var_59 + (*var_51 << 3UL));
                var_61 = *(uint64_t *)(var_59 + (var_58 << 3UL));
                if ((long)var_60 > (long)var_61) {
                    *var_53 = (var_57 + (-1L));
                    var_63 = var_59 + ((var_57 + *var_29) << 3UL);
                    var_64 = *(uint64_t *)(*var_4 + 16UL);
                    var_65 = *var_51;
                    *var_51 = (var_65 + (-1L));
                    *(uint64_t *)var_63 = *(uint64_t *)((var_65 << 3UL) + var_64);
                    var_66 = *var_53;
                    _pre157 = var_66;
                    if (var_66 != 0UL) {
                        break;
                    }
                    _pre156 = *var_29;
                } else {
                    *(uint64_t *)(((var_57 + var_58) << 3UL) + var_59) = var_61;
                    var_62 = *var_29 + (-1L);
                    *var_29 = var_62;
                    _pre156 = var_62;
                    if ((long)var_62 <= (long)18446744073709551615UL) {
                        break;
                    }
                    _pre157 = *var_53;
                }
                var_57 = _pre157;
                var_58 = _pre156;
                continue;
            }
    }
    *(uint64_t *)(local_sp_0 + (-8L)) = 4244627UL;
    indirect_placeholder();
    return rax_0;
}

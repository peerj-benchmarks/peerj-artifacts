typedef unsigned char __u_char;
typedef unsigned short __u_short;
typedef unsigned int __u_int;
typedef unsigned long __u_long;
typedef signed char __int8_t;
typedef unsigned char __uint8_t;
typedef short __int16_t;
typedef unsigned short __uint16_t;
typedef int __int32_t;
typedef unsigned int __uint32_t;
typedef long __int64_t;
typedef unsigned long __uint64_t;
typedef __int8_t __int_least8_t;
typedef __uint8_t __uint_least8_t;
typedef __int16_t __int_least16_t;
typedef __uint16_t __uint_least16_t;
typedef __int32_t __int_least32_t;
typedef __uint32_t __uint_least32_t;
typedef __int64_t __int_least64_t;
typedef __uint64_t __uint_least64_t;
typedef long __quad_t;
typedef unsigned long __u_quad_t;
typedef long __intmax_t;
typedef unsigned long __uintmax_t;
typedef unsigned long __dev_t;
typedef unsigned int __uid_t;
typedef unsigned int __gid_t;
typedef unsigned long __ino_t;
typedef unsigned long __ino64_t;
typedef unsigned int __mode_t;
typedef unsigned long __nlink_t;
typedef long __off_t;
typedef long __off64_t;
typedef int __pid_t;
typedef struct {
    int __val[2];
} __fsid_t;
typedef long __clock_t;
typedef unsigned long __rlim_t;
typedef unsigned long __rlim64_t;
typedef unsigned int __id_t;
typedef long __time_t;
typedef unsigned int __useconds_t;
typedef long __suseconds_t;
typedef int __daddr_t;
typedef int __key_t;
typedef int __clockid_t;
typedef void *__timer_t;
typedef long __blksize_t;
typedef long __blkcnt_t;
typedef long __blkcnt64_t;
typedef unsigned long __fsblkcnt_t;
typedef unsigned long __fsblkcnt64_t;
typedef unsigned long __fsfilcnt_t;
typedef unsigned long __fsfilcnt64_t;
typedef long __fsword_t;
typedef long __ssize_t;
typedef long __syscall_slong_t;
typedef unsigned long __syscall_ulong_t;
typedef __off64_t __loff_t;
typedef char *__caddr_t;
typedef long __intptr_t;
typedef unsigned int __socklen_t;
typedef int __sig_atomic_t;
typedef __int8_t int8_t;
typedef __int16_t int16_t;
typedef __int32_t int32_t;
typedef __int64_t int64_t;
typedef __uint8_t uint8_t;
typedef __uint16_t uint16_t;
typedef __uint32_t uint32_t;
typedef __uint64_t uint64_t;
typedef __int_least8_t int_least8_t;
typedef __int_least16_t int_least16_t;
typedef __int_least32_t int_least32_t;
typedef __int_least64_t int_least64_t;
typedef __uint_least8_t uint_least8_t;
typedef __uint_least16_t uint_least16_t;
typedef __uint_least32_t uint_least32_t;
typedef __uint_least64_t uint_least64_t;
typedef signed char int_fast8_t;
typedef long int_fast16_t;
typedef long int_fast32_t;
typedef long int_fast64_t;
typedef unsigned char uint_fast8_t;
typedef unsigned long uint_fast16_t;
typedef unsigned long uint_fast32_t;
typedef unsigned long uint_fast64_t;
typedef long intptr_t;
typedef unsigned long uintptr_t;
typedef __intmax_t intmax_t;
typedef __uintmax_t uintmax_t;
typedef _Bool bool;
struct indirect_placeholder_127_ret_type;
struct indirect_placeholder_132_ret_type;
struct indirect_placeholder_131_ret_type;
struct indirect_placeholder_130_ret_type;
struct indirect_placeholder_128_ret_type;
struct indirect_placeholder_129_ret_type;
struct indirect_placeholder_127_ret_type {
    uint64_t field_0;
    uint64_t field_1;
};
struct indirect_placeholder_132_ret_type {
    uint64_t field_0;
    uint64_t field_1;
};
struct indirect_placeholder_131_ret_type {
    uint64_t field_0;
    uint64_t field_1;
};
struct indirect_placeholder_130_ret_type {
    uint64_t field_0;
    uint64_t field_1;
};
struct indirect_placeholder_128_ret_type {
    uint64_t field_0;
    uint64_t field_1;
};
struct indirect_placeholder_129_ret_type {
    uint64_t field_0;
    uint64_t field_1;
};
extern uint64_t revng_init_local_sp(uint64_t param_0);
extern uint64_t init_rbp(void);
extern void indirect_placeholder(void);
extern uint64_t indirect_placeholder_1(uint64_t param_0, uint64_t param_1);
extern uint64_t init_cc_src2(void);
extern uint64_t helper_cc_compute_c_wrapper(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint32_t param_3);
extern uint64_t indirect_placeholder_11(uint64_t param_0);
extern struct indirect_placeholder_127_ret_type indirect_placeholder_127(uint64_t param_0);
extern struct indirect_placeholder_132_ret_type indirect_placeholder_132(uint64_t param_0);
extern struct indirect_placeholder_131_ret_type indirect_placeholder_131(uint64_t param_0);
extern struct indirect_placeholder_130_ret_type indirect_placeholder_130(uint64_t param_0);
extern struct indirect_placeholder_128_ret_type indirect_placeholder_128(uint64_t param_0);
extern struct indirect_placeholder_129_ret_type indirect_placeholder_129(uint64_t param_0);
uint64_t bb_analyze(uint64_t rdi) {
    uint64_t var_0;
    uint64_t var_1;
    uint64_t var_2;
    uint64_t var_3;
    uint64_t *var_4;
    uint64_t var_5;
    uint64_t *var_6;
    uint64_t var_7;
    struct indirect_placeholder_127_ret_type var_8;
    uint64_t var_9;
    struct indirect_placeholder_132_ret_type var_10;
    uint64_t var_11;
    struct indirect_placeholder_131_ret_type var_12;
    uint64_t var_13;
    struct indirect_placeholder_130_ret_type var_14;
    uint64_t var_15;
    uint32_t rax_0_shrunk;
    uint64_t var_60;
    uint32_t var_61;
    uint32_t var_62;
    uint64_t var_53;
    uint64_t var_54;
    uint64_t var_56;
    uint64_t var_55;
    uint64_t var_57;
    struct indirect_placeholder_128_ret_type var_58;
    uint64_t var_59;
    uint64_t var_50;
    uint64_t var_51;
    uint32_t var_52;
    uint64_t var_45;
    uint64_t var_46;
    uint64_t var_47;
    uint64_t var_48;
    uint32_t var_49;
    uint64_t var_42;
    uint64_t var_43;
    uint32_t var_44;
    uint64_t var_30;
    uint64_t var_31;
    uint64_t var_32;
    uint64_t var_36;
    uint64_t var_33;
    uint64_t var_34;
    uint64_t var_37;
    uint64_t var_35;
    uint64_t *var_19;
    uint64_t var_20;
    uint64_t var_21;
    uint64_t var_22;
    bool var_23;
    uint64_t var_24;
    uint64_t var_28;
    uint64_t var_29;
    uint64_t var_25;
    uint64_t var_26;
    uint64_t var_27;
    uint64_t local_sp_0;
    uint64_t var_38;
    uint64_t var_39;
    uint32_t *var_40;
    uint32_t var_41;
    uint64_t var_16;
    uint64_t var_17;
    struct indirect_placeholder_129_ret_type var_18;
    var_0 = revng_init_local_sp(0UL);
    var_1 = init_rbp();
    var_2 = init_cc_src2();
    *(uint64_t *)(var_0 + (-8L)) = var_1;
    var_3 = var_0 + (-48L);
    var_4 = (uint64_t *)var_3;
    *var_4 = rdi;
    var_5 = **(uint64_t **)var_3;
    var_6 = (uint64_t *)(var_0 + (-32L));
    *var_6 = var_5;
    var_7 = *(uint64_t *)(var_5 + 8UL) << 3UL;
    *(uint64_t *)(var_0 + (-64L)) = 4257498UL;
    var_8 = indirect_placeholder_127(var_7);
    *(uint64_t *)(*var_6 + 24UL) = var_8.field_0;
    var_9 = *(uint64_t *)(*var_6 + 8UL) << 3UL;
    *(uint64_t *)(var_0 + (-72L)) = 4257529UL;
    var_10 = indirect_placeholder_132(var_9);
    *(uint64_t *)(*var_6 + 32UL) = var_10.field_0;
    var_11 = *(uint64_t *)(*var_6 + 8UL) * 24UL;
    *(uint64_t *)(var_0 + (-80L)) = 4257569UL;
    var_12 = indirect_placeholder_131(var_11);
    *(uint64_t *)(*var_6 + 40UL) = var_12.field_0;
    var_13 = *(uint64_t *)(*var_6 + 8UL) * 24UL;
    *(uint64_t *)(var_0 + (-88L)) = 4257609UL;
    var_14 = indirect_placeholder_130(var_13);
    *(uint64_t *)(*var_6 + 48UL) = var_14.field_0;
    var_15 = *var_6;
    var_62 = 0U;
    var_30 = 0UL;
    var_20 = 0UL;
    rax_0_shrunk = 12U;
    if (*(uint64_t *)(var_15 + 24UL) == 0UL) {
        return (uint64_t)rax_0_shrunk;
    }
    if (~(*(uint64_t *)(var_15 + 32UL) != 0UL & *(uint64_t *)(var_15 + 40UL) != 0UL & *(uint64_t *)(var_15 + 48UL) != 0UL)) {
        return;
    }
    var_16 = *(uint64_t *)(*var_4 + 48UL) << 3UL;
    var_17 = var_0 + (-96L);
    *(uint64_t *)var_17 = 4257770UL;
    var_18 = indirect_placeholder_129(var_16);
    *(uint64_t *)(*var_6 + 224UL) = var_18.field_0;
    local_sp_0 = var_17;
    if (*(uint64_t *)(*var_6 + 224UL) != 0UL) {
        var_19 = (uint64_t *)(var_0 + (-24L));
        *var_19 = 0UL;
        var_21 = *(uint64_t *)(*var_4 + 48UL);
        var_22 = helper_cc_compute_c_wrapper(var_20 - var_21, var_21, var_2, 17U);
        var_23 = (var_22 == 0UL);
        var_24 = *var_6;
        while (!var_23)
            {
                var_25 = *(uint64_t *)(var_24 + 224UL);
                var_26 = *var_19;
                *(uint64_t *)((var_26 << 3UL) + var_25) = var_26;
                var_27 = *var_19 + 1UL;
                *var_19 = var_27;
                var_20 = var_27;
                var_21 = *(uint64_t *)(*var_4 + 48UL);
                var_22 = helper_cc_compute_c_wrapper(var_20 - var_21, var_21, var_2, 17U);
                var_23 = (var_22 == 0UL);
                var_24 = *var_6;
            }
        var_28 = *(uint64_t *)(var_24 + 104UL);
        var_29 = var_0 + (-104L);
        *(uint64_t *)var_29 = 4257890UL;
        indirect_placeholder_1(var_28, 4258782UL);
        *var_19 = 0UL;
        local_sp_0 = var_29;
        while (1U)
            {
                var_31 = *(uint64_t *)(*var_4 + 48UL);
                var_32 = helper_cc_compute_c_wrapper(var_30 - var_31, var_31, var_2, 17U);
                if (var_32 != 0UL) {
                    var_36 = *var_19;
                    break;
                }
                var_33 = *(uint64_t *)(*var_6 + 224UL);
                var_34 = *var_19;
                var_36 = var_34;
                if (*(uint64_t *)(var_33 + (var_34 << 3UL)) != var_34) {
                    break;
                }
                var_35 = var_34 + 1UL;
                *var_19 = var_35;
                var_30 = var_35;
                continue;
            }
        if (var_36 == *(uint64_t *)(*var_4 + 48UL)) {
            var_37 = var_0 + (-112L);
            *(uint64_t *)var_37 = 4257991UL;
            indirect_placeholder();
            *(uint64_t *)(*var_6 + 224UL) = 0UL;
            local_sp_0 = var_37;
        }
    }
    var_38 = *(uint64_t *)(*var_6 + 104UL);
    *(uint64_t *)(local_sp_0 + (-8L)) = 4258031UL;
    var_39 = indirect_placeholder_1(var_38, 4259157UL);
    var_40 = (uint32_t *)(var_0 + (-12L));
    var_41 = (uint32_t)var_39;
    *var_40 = var_41;
    rax_0_shrunk = var_41;
    var_42 = *(uint64_t *)(*var_6 + 104UL);
    *(uint64_t *)(local_sp_0 + (-16L)) = 4258082UL;
    var_43 = indirect_placeholder_1(var_42, 4259880UL);
    var_44 = (uint32_t)var_43;
    *var_40 = var_44;
    rax_0_shrunk = var_44;
    var_45 = *(uint64_t *)(*var_6 + 104UL);
    *(uint64_t *)(local_sp_0 + (-24L)) = 4258133UL;
    indirect_placeholder_1(var_45, 4260122UL);
    var_46 = *var_6;
    var_47 = *(uint64_t *)(var_46 + 104UL);
    *(uint64_t *)(local_sp_0 + (-32L)) = 4258158UL;
    var_48 = indirect_placeholder_1(var_47, 4260297UL);
    var_49 = (uint32_t)var_48;
    *var_40 = var_49;
    rax_0_shrunk = var_49;
    var_50 = *var_6;
    *(uint64_t *)(local_sp_0 + (-40L)) = 4258196UL;
    var_51 = indirect_placeholder_1(var_46, var_50);
    var_52 = (uint32_t)var_51;
    *var_40 = var_52;
    rax_0_shrunk = var_52;
    if (var_41 != 0U & var_44 != 0U & var_49 != 0U & var_52 != 0U) {
        var_53 = *var_4;
        rax_0_shrunk = 12U;
        if ((*(unsigned char *)(var_53 + 56UL) & '\x10') == '\x00') {
            if (*(uint64_t *)(var_53 + 48UL) != 0UL) {
                var_55 = *var_6;
                var_56 = var_55;
                if (*(uint64_t *)(var_55 + 152UL) != 0UL) {
                    rax_0_shrunk = var_62;
                    return (uint64_t)rax_0_shrunk;
                }
            }
            var_54 = *var_6;
            var_56 = var_54;
            var_55 = *var_6;
            var_56 = var_55;
            if ((*(unsigned char *)(var_54 + 176UL) & '\x01') != '\x00' & *(uint64_t *)(var_55 + 152UL) != 0UL) {
                rax_0_shrunk = var_62;
                return (uint64_t)rax_0_shrunk;
            }
        }
        var_55 = *var_6;
        var_56 = var_55;
        if (*(uint64_t *)(var_55 + 152UL) != 0UL) {
            rax_0_shrunk = var_62;
            return (uint64_t)rax_0_shrunk;
        }
        var_57 = *(uint64_t *)(var_56 + 16UL) * 24UL;
        *(uint64_t *)(local_sp_0 + (-48L)) = 4258313UL;
        var_58 = indirect_placeholder_128(var_57);
        *(uint64_t *)(*var_6 + 56UL) = var_58.field_0;
        var_59 = *var_6;
        if (*(uint64_t *)(var_59 + 56UL) != 0UL) {
            *(uint64_t *)(local_sp_0 + (-56L)) = 4258365UL;
            var_60 = indirect_placeholder_11(var_59);
            var_61 = (uint32_t)var_60;
            *var_40 = var_61;
            var_62 = var_61;
            rax_0_shrunk = var_62;
        }
    }
}

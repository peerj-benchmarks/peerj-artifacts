typedef unsigned char __u_char;
typedef unsigned short __u_short;
typedef unsigned int __u_int;
typedef unsigned long __u_long;
typedef signed char __int8_t;
typedef unsigned char __uint8_t;
typedef short __int16_t;
typedef unsigned short __uint16_t;
typedef int __int32_t;
typedef unsigned int __uint32_t;
typedef long __int64_t;
typedef unsigned long __uint64_t;
typedef __int8_t __int_least8_t;
typedef __uint8_t __uint_least8_t;
typedef __int16_t __int_least16_t;
typedef __uint16_t __uint_least16_t;
typedef __int32_t __int_least32_t;
typedef __uint32_t __uint_least32_t;
typedef __int64_t __int_least64_t;
typedef __uint64_t __uint_least64_t;
typedef long __quad_t;
typedef unsigned long __u_quad_t;
typedef long __intmax_t;
typedef unsigned long __uintmax_t;
typedef unsigned long __dev_t;
typedef unsigned int __uid_t;
typedef unsigned int __gid_t;
typedef unsigned long __ino_t;
typedef unsigned long __ino64_t;
typedef unsigned int __mode_t;
typedef unsigned long __nlink_t;
typedef long __off_t;
typedef long __off64_t;
typedef int __pid_t;
typedef struct {
    int __val[2];
} __fsid_t;
typedef long __clock_t;
typedef unsigned long __rlim_t;
typedef unsigned long __rlim64_t;
typedef unsigned int __id_t;
typedef long __time_t;
typedef unsigned int __useconds_t;
typedef long __suseconds_t;
typedef int __daddr_t;
typedef int __key_t;
typedef int __clockid_t;
typedef void *__timer_t;
typedef long __blksize_t;
typedef long __blkcnt_t;
typedef long __blkcnt64_t;
typedef unsigned long __fsblkcnt_t;
typedef unsigned long __fsblkcnt64_t;
typedef unsigned long __fsfilcnt_t;
typedef unsigned long __fsfilcnt64_t;
typedef long __fsword_t;
typedef long __ssize_t;
typedef long __syscall_slong_t;
typedef unsigned long __syscall_ulong_t;
typedef __off64_t __loff_t;
typedef char *__caddr_t;
typedef long __intptr_t;
typedef unsigned int __socklen_t;
typedef int __sig_atomic_t;
typedef __int8_t int8_t;
typedef __int16_t int16_t;
typedef __int32_t int32_t;
typedef __int64_t int64_t;
typedef __uint8_t uint8_t;
typedef __uint16_t uint16_t;
typedef __uint32_t uint32_t;
typedef __uint64_t uint64_t;
typedef __int_least8_t int_least8_t;
typedef __int_least16_t int_least16_t;
typedef __int_least32_t int_least32_t;
typedef __int_least64_t int_least64_t;
typedef __uint_least8_t uint_least8_t;
typedef __uint_least16_t uint_least16_t;
typedef __uint_least32_t uint_least32_t;
typedef __uint_least64_t uint_least64_t;
typedef signed char int_fast8_t;
typedef long int_fast16_t;
typedef long int_fast32_t;
typedef long int_fast64_t;
typedef unsigned char uint_fast8_t;
typedef unsigned long uint_fast16_t;
typedef unsigned long uint_fast32_t;
typedef unsigned long uint_fast64_t;
typedef long intptr_t;
typedef unsigned long uintptr_t;
typedef __intmax_t intmax_t;
typedef __uintmax_t uintmax_t;
extern uint64_t revng_init_local_sp(uint64_t param_0);
extern uint64_t init_rbp(void);
extern uint64_t indirect_placeholder_1(uint64_t param_0, uint64_t param_1);
extern uint64_t indirect_placeholder_13(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint64_t param_3, uint64_t param_4);
typedef _Bool bool;
uint64_t bb_check_arrival_expand_ecl_sub(uint64_t rdx, uint64_t rcx, uint64_t r8, uint64_t rdi, uint64_t rsi) {
    uint64_t var_35;
    uint64_t var_0;
    uint64_t var_1;
    uint64_t var_2;
    uint64_t var_3;
    uint64_t *var_4;
    uint64_t *var_5;
    uint64_t *var_6;
    uint64_t *var_7;
    uint32_t *var_8;
    uint64_t var_9;
    uint64_t *var_10;
    unsigned char *var_11;
    uint32_t *var_12;
    uint64_t var_13;
    uint64_t var_16;
    uint64_t var_17;
    uint64_t var_18;
    uint64_t var_19;
    uint32_t var_20;
    uint64_t rax_0;
    uint64_t local_sp_0;
    uint64_t var_25;
    uint64_t *var_26;
    uint64_t var_27;
    uint64_t var_28;
    uint64_t var_29;
    uint64_t _pre_phi67;
    uint64_t var_30;
    uint64_t var_31;
    uint64_t var_32;
    uint64_t var_33;
    uint64_t var_34;
    uint32_t var_36;
    uint64_t _pre66;
    uint64_t _pre62;
    uint64_t *_pre64;
    uint64_t *_pre_phi65;
    uint64_t var_37;
    uint64_t var_38;
    uint64_t var_39;
    unsigned char var_40;
    uint64_t var_21;
    uint64_t var_22;
    uint64_t var_23;
    unsigned char var_24;
    uint64_t local_sp_1;
    uint64_t var_14;
    uint64_t var_15;
    unsigned int loop_state_var;
    bool switch_state_var;
    var_0 = revng_init_local_sp(0UL);
    var_1 = init_rbp();
    *(uint64_t *)(var_0 + (-8L)) = var_1;
    var_2 = var_0 + (-72L);
    var_3 = var_0 + (-32L);
    var_4 = (uint64_t *)var_3;
    *var_4 = rdi;
    var_5 = (uint64_t *)(var_0 + (-40L));
    *var_5 = rsi;
    var_6 = (uint64_t *)(var_0 + (-48L));
    *var_6 = rdx;
    var_7 = (uint64_t *)(var_0 + (-56L));
    *var_7 = rcx;
    var_8 = (uint32_t *)(var_0 + (-60L));
    *var_8 = (uint32_t)r8;
    var_9 = *var_6;
    var_10 = (uint64_t *)(var_0 + (-16L));
    *var_10 = var_9;
    var_11 = (unsigned char *)(var_0 + (-17L));
    var_12 = (uint32_t *)(var_0 + (-24L));
    rax_0 = 12UL;
    var_13 = var_9;
    local_sp_1 = var_2;
    while (1U)
        {
            var_14 = *var_5;
            *(uint64_t *)(local_sp_1 + (-8L)) = 4307219UL;
            var_15 = indirect_placeholder_1(var_14, var_13);
            if (var_15 != 0UL) {
                loop_state_var = 1U;
                break;
            }
            var_16 = **(uint64_t **)var_3;
            var_17 = *var_10;
            var_18 = var_16 + (var_17 << 4UL);
            var_19 = (uint64_t)*(unsigned char *)(var_18 + 8UL);
            var_20 = *var_8;
            if ((uint64_t)((uint32_t)var_19 - var_20) != 0UL) {
                if (*(uint64_t *)var_18 != *var_7) {
                    if (var_20 != 9U) {
                        loop_state_var = 1U;
                        break;
                    }
                    var_38 = *var_5;
                    *(uint64_t *)(local_sp_1 + (-16L)) = 4306913UL;
                    var_39 = indirect_placeholder_1(var_38, var_17);
                    var_40 = (unsigned char)var_39;
                    *var_11 = var_40;
                    if (var_40 != '\x01') {
                        loop_state_var = 1U;
                        break;
                    }
                    loop_state_var = 0U;
                    break;
                }
            }
            var_21 = *var_5;
            var_22 = local_sp_1 + (-16L);
            *(uint64_t *)var_22 = 4306965UL;
            var_23 = indirect_placeholder_1(var_21, var_17);
            var_24 = (unsigned char)var_23;
            *var_11 = var_24;
            local_sp_0 = var_22;
            if (var_24 != '\x01') {
                loop_state_var = 0U;
                break;
            }
            var_25 = *var_4;
            var_26 = (uint64_t *)(var_25 + 40UL);
            var_27 = *var_26;
            var_28 = *var_10 * 24UL;
            var_29 = var_28 + var_27;
            _pre_phi67 = var_28;
            _pre_phi65 = var_26;
            switch_state_var = 0;
            switch (*(uint64_t *)(var_29 + 8UL)) {
              case 2UL:
                {
                    var_30 = *(uint64_t *)(*(uint64_t *)(var_29 + 16UL) + 8UL);
                    var_31 = (uint64_t)*var_8;
                    var_32 = *var_7;
                    var_33 = *var_5;
                    var_34 = local_sp_1 + (-24L);
                    *(uint64_t *)var_34 = 4307138UL;
                    var_35 = indirect_placeholder_13(var_30, var_32, var_31, var_25, var_33);
                    var_36 = (uint32_t)var_35;
                    *var_12 = var_36;
                    local_sp_0 = var_34;
                    if (var_36 != 0U) {
                        rax_0 = (uint64_t)var_36;
                        loop_state_var = 0U;
                        switch_state_var = 1;
                        break;
                    }
                    _pre62 = *var_10;
                    _pre64 = (uint64_t *)(*var_4 + 40UL);
                    _pre66 = _pre62 * 24UL;
                    _pre_phi67 = _pre66;
                    _pre_phi65 = _pre64;
                    var_37 = **(uint64_t **)((_pre_phi67 + *_pre_phi65) + 16UL);
                    *var_10 = var_37;
                    var_13 = var_37;
                    local_sp_1 = local_sp_0;
                    continue;
                }
                break;
              case 0UL:
                {
                    loop_state_var = 1U;
                    switch_state_var = 1;
                    break;
                }
                break;
              default:
                {
                    var_37 = **(uint64_t **)((_pre_phi67 + *_pre_phi65) + 16UL);
                    *var_10 = var_37;
                    var_13 = var_37;
                    local_sp_1 = local_sp_0;
                    continue;
                }
                break;
            }
            if (switch_state_var)
                break;
        }
    switch (loop_state_var) {
      case 0U:
        {
            return rax_0;
        }
        break;
      case 1U:
        {
            rax_0 = 0UL;
        }
        break;
    }
}

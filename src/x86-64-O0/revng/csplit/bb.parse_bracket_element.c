typedef unsigned char __u_char;
typedef unsigned short __u_short;
typedef unsigned int __u_int;
typedef unsigned long __u_long;
typedef signed char __int8_t;
typedef unsigned char __uint8_t;
typedef short __int16_t;
typedef unsigned short __uint16_t;
typedef int __int32_t;
typedef unsigned int __uint32_t;
typedef long __int64_t;
typedef unsigned long __uint64_t;
typedef __int8_t __int_least8_t;
typedef __uint8_t __uint_least8_t;
typedef __int16_t __int_least16_t;
typedef __uint16_t __uint_least16_t;
typedef __int32_t __int_least32_t;
typedef __uint32_t __uint_least32_t;
typedef __int64_t __int_least64_t;
typedef __uint64_t __uint_least64_t;
typedef long __quad_t;
typedef unsigned long __u_quad_t;
typedef long __intmax_t;
typedef unsigned long __uintmax_t;
typedef unsigned long __dev_t;
typedef unsigned int __uid_t;
typedef unsigned int __gid_t;
typedef unsigned long __ino_t;
typedef unsigned long __ino64_t;
typedef unsigned int __mode_t;
typedef unsigned long __nlink_t;
typedef long __off_t;
typedef long __off64_t;
typedef int __pid_t;
typedef struct {
    int __val[2];
} __fsid_t;
typedef long __clock_t;
typedef unsigned long __rlim_t;
typedef unsigned long __rlim64_t;
typedef unsigned int __id_t;
typedef long __time_t;
typedef unsigned int __useconds_t;
typedef long __suseconds_t;
typedef int __daddr_t;
typedef int __key_t;
typedef int __clockid_t;
typedef void *__timer_t;
typedef long __blksize_t;
typedef long __blkcnt_t;
typedef long __blkcnt64_t;
typedef unsigned long __fsblkcnt_t;
typedef unsigned long __fsblkcnt64_t;
typedef unsigned long __fsfilcnt_t;
typedef unsigned long __fsfilcnt64_t;
typedef long __fsword_t;
typedef long __ssize_t;
typedef long __syscall_slong_t;
typedef unsigned long __syscall_ulong_t;
typedef __off64_t __loff_t;
typedef char *__caddr_t;
typedef long __intptr_t;
typedef unsigned int __socklen_t;
typedef int __sig_atomic_t;
typedef __int8_t int8_t;
typedef __int16_t int16_t;
typedef __int32_t int32_t;
typedef __int64_t int64_t;
typedef __uint8_t uint8_t;
typedef __uint16_t uint16_t;
typedef __uint32_t uint32_t;
typedef __uint64_t uint64_t;
typedef __int_least8_t int_least8_t;
typedef __int_least16_t int_least16_t;
typedef __int_least32_t int_least32_t;
typedef __int_least64_t int_least64_t;
typedef __uint_least8_t uint_least8_t;
typedef __uint_least16_t uint_least16_t;
typedef __uint_least32_t uint_least32_t;
typedef __uint_least64_t uint_least64_t;
typedef signed char int_fast8_t;
typedef long int_fast16_t;
typedef long int_fast32_t;
typedef long int_fast64_t;
typedef unsigned char uint_fast8_t;
typedef unsigned long uint_fast16_t;
typedef unsigned long uint_fast32_t;
typedef unsigned long uint_fast64_t;
typedef long intptr_t;
typedef unsigned long uintptr_t;
typedef __intmax_t intmax_t;
typedef __uintmax_t uintmax_t;
extern uint64_t revng_init_local_sp(uint64_t param_0);
extern uint64_t init_rbp(void);
extern uint64_t indirect_placeholder_1(uint64_t param_0, uint64_t param_1);
extern uint64_t indirect_placeholder_8(uint64_t param_0, uint64_t param_1, uint64_t param_2);
uint64_t bb_parse_bracket_element(uint64_t rdx, uint64_t rcx, uint64_t r8, uint64_t rdi, uint64_t rsi, uint64_t r9) {
    uint64_t var_0;
    uint64_t var_1;
    uint64_t var_2;
    uint64_t *var_3;
    uint64_t *var_4;
    uint64_t var_5;
    uint64_t *var_6;
    uint32_t *var_7;
    uint64_t *var_8;
    uint32_t var_9;
    unsigned char *var_10;
    uint64_t var_11;
    uint64_t var_12;
    uint64_t var_13;
    uint32_t *var_14;
    uint32_t var_15;
    uint64_t var_24;
    uint64_t var_25;
    uint64_t var_26;
    uint64_t *var_27;
    uint64_t rax_0;
    uint64_t *var_16;
    uint64_t var_17;
    uint64_t var_18;
    uint64_t var_19;
    uint64_t var_20;
    uint64_t var_21;
    uint64_t var_22;
    uint64_t var_23;
    var_0 = revng_init_local_sp(0UL);
    var_1 = init_rbp();
    *(uint64_t *)(var_0 + (-8L)) = var_1;
    var_2 = var_0 + (-48L);
    var_3 = (uint64_t *)var_2;
    *var_3 = rdi;
    var_4 = (uint64_t *)(var_0 + (-56L));
    *var_4 = rsi;
    var_5 = var_0 + (-64L);
    var_6 = (uint64_t *)var_5;
    *var_6 = rdx;
    var_7 = (uint32_t *)(var_0 + (-68L));
    *var_7 = (uint32_t)rcx;
    *(uint64_t *)(var_0 + (-80L)) = r8;
    var_8 = (uint64_t *)(var_0 + (-88L));
    *var_8 = r9;
    var_9 = *(uint32_t *)(var_0 | 8UL);
    var_10 = (unsigned char *)(var_0 + (-72L));
    *var_10 = (unsigned char)var_9;
    var_11 = *var_4;
    var_12 = *(uint64_t *)(var_11 + 72UL);
    *(uint64_t *)(var_0 + (-96L)) = 4275567UL;
    var_13 = indirect_placeholder_1(var_11, var_12);
    var_14 = (uint32_t *)(var_0 + (-12L));
    var_15 = (uint32_t)var_13;
    *var_14 = var_15;
    rax_0 = 0UL;
    if ((int)var_15 > (int)1U) {
        **(uint32_t **)var_2 = 1U;
        var_24 = *var_4;
        var_25 = *(uint64_t *)(var_24 + 72UL);
        *(uint64_t *)(var_0 + (-104L)) = 4275609UL;
        var_26 = indirect_placeholder_1(var_24, var_25);
        *(uint32_t *)(*var_3 + 8UL) = (uint32_t)var_26;
        var_27 = (uint64_t *)(*var_4 + 72UL);
        *var_27 = (*var_27 + (uint64_t)*var_14);
    } else {
        var_16 = (uint64_t *)(*var_4 + 72UL);
        *var_16 = (*var_16 + (uint64_t)*var_7);
        var_17 = *var_6;
        rax_0 = 11UL;
        switch (*(unsigned char *)(var_17 + 8UL)) {
          case '\x1e':
          case '\x1c':
          case '\x1a':
            {
                var_21 = *var_4;
                var_22 = *var_3;
                *(uint64_t *)(var_0 + (-104L)) = 4275735UL;
                var_23 = indirect_placeholder_8(var_17, var_22, var_21);
                rax_0 = var_23;
            }
            break;
          case '\x16':
            {
                if (*var_10 != '\x01') {
                    var_18 = *var_8;
                    var_19 = *var_4;
                    var_20 = var_0 + (-40L);
                    *(uint64_t *)(var_0 + (-104L)) = 4275792UL;
                    indirect_placeholder_8(var_18, var_20, var_19);
                    if (*(unsigned char *)(var_0 + (-32L)) == '\x15') {
                        return rax_0;
                    }
                }
                **(uint32_t **)var_2 = 0U;
                *(unsigned char *)(*var_3 + 8UL) = **(unsigned char **)var_5;
            }
            break;
          default:
            {
                **(uint32_t **)var_2 = 0U;
                *(unsigned char *)(*var_3 + 8UL) = **(unsigned char **)var_5;
            }
            break;
        }
    }
}

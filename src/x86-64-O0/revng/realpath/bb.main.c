typedef unsigned char __u_char;
typedef unsigned short __u_short;
typedef unsigned int __u_int;
typedef unsigned long __u_long;
typedef signed char __int8_t;
typedef unsigned char __uint8_t;
typedef short __int16_t;
typedef unsigned short __uint16_t;
typedef int __int32_t;
typedef unsigned int __uint32_t;
typedef long __int64_t;
typedef unsigned long __uint64_t;
typedef __int8_t __int_least8_t;
typedef __uint8_t __uint_least8_t;
typedef __int16_t __int_least16_t;
typedef __uint16_t __uint_least16_t;
typedef __int32_t __int_least32_t;
typedef __uint32_t __uint_least32_t;
typedef __int64_t __int_least64_t;
typedef __uint64_t __uint_least64_t;
typedef long __quad_t;
typedef unsigned long __u_quad_t;
typedef long __intmax_t;
typedef unsigned long __uintmax_t;
typedef unsigned long __dev_t;
typedef unsigned int __uid_t;
typedef unsigned int __gid_t;
typedef unsigned long __ino_t;
typedef unsigned long __ino64_t;
typedef unsigned int __mode_t;
typedef unsigned long __nlink_t;
typedef long __off_t;
typedef long __off64_t;
typedef int __pid_t;
typedef struct {
    int __val[2];
} __fsid_t;
typedef long __clock_t;
typedef unsigned long __rlim_t;
typedef unsigned long __rlim64_t;
typedef unsigned int __id_t;
typedef long __time_t;
typedef unsigned int __useconds_t;
typedef long __suseconds_t;
typedef int __daddr_t;
typedef int __key_t;
typedef int __clockid_t;
typedef void *__timer_t;
typedef long __blksize_t;
typedef long __blkcnt_t;
typedef long __blkcnt64_t;
typedef unsigned long __fsblkcnt_t;
typedef unsigned long __fsblkcnt64_t;
typedef unsigned long __fsfilcnt_t;
typedef unsigned long __fsfilcnt64_t;
typedef long __fsword_t;
typedef long __ssize_t;
typedef long __syscall_slong_t;
typedef unsigned long __syscall_ulong_t;
typedef __off64_t __loff_t;
typedef char *__caddr_t;
typedef long __intptr_t;
typedef unsigned int __socklen_t;
typedef int __sig_atomic_t;
typedef __int8_t int8_t;
typedef __int16_t int16_t;
typedef __int32_t int32_t;
typedef __int64_t int64_t;
typedef __uint8_t uint8_t;
typedef __uint16_t uint16_t;
typedef __uint32_t uint32_t;
typedef __uint64_t uint64_t;
typedef __int_least8_t int_least8_t;
typedef __int_least16_t int_least16_t;
typedef __int_least32_t int_least32_t;
typedef __int_least64_t int_least64_t;
typedef __uint_least8_t uint_least8_t;
typedef __uint_least16_t uint_least16_t;
typedef __uint_least32_t uint_least32_t;
typedef __uint_least64_t uint_least64_t;
typedef signed char int_fast8_t;
typedef long int_fast16_t;
typedef long int_fast32_t;
typedef long int_fast64_t;
typedef unsigned char uint_fast8_t;
typedef unsigned long uint_fast16_t;
typedef unsigned long uint_fast32_t;
typedef unsigned long uint_fast64_t;
typedef long intptr_t;
typedef unsigned long uintptr_t;
typedef __intmax_t intmax_t;
typedef __uintmax_t uintmax_t;
typedef _Bool bool;
struct indirect_placeholder_21_ret_type;
struct indirect_placeholder_22_ret_type;
struct indirect_placeholder_23_ret_type;
struct indirect_placeholder_24_ret_type;
struct indirect_placeholder_26_ret_type;
struct indirect_placeholder_21_ret_type {
    uint64_t field_0;
    uint64_t field_1;
    uint64_t field_2;
};
struct indirect_placeholder_22_ret_type {
    uint64_t field_0;
    uint64_t field_1;
    uint64_t field_2;
};
struct indirect_placeholder_23_ret_type {
    uint64_t field_0;
    uint64_t field_1;
    uint64_t field_2;
};
struct indirect_placeholder_24_ret_type {
    uint64_t field_0;
    uint64_t field_1;
    uint64_t field_2;
};
struct indirect_placeholder_26_ret_type {
    uint64_t field_0;
    uint64_t field_1;
    uint64_t field_2;
    uint64_t field_3;
};
extern void abort(void);
extern uint64_t revng_init_local_sp(uint64_t param_0);
extern uint64_t init_rbp(void);
extern uint64_t indirect_placeholder_1(uint64_t param_0, uint64_t param_1);
extern void indirect_placeholder(void);
extern void indirect_placeholder_4(uint64_t param_0, uint64_t param_1);
extern uint64_t init_rbx(void);
extern uint64_t indirect_placeholder_6(uint64_t param_0);
extern void indirect_placeholder_20(uint64_t param_0);
extern void indirect_placeholder_12(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint64_t param_3, uint64_t param_4, uint64_t param_5, uint64_t param_6);
extern struct indirect_placeholder_21_ret_type indirect_placeholder_21(uint64_t param_0, uint64_t param_1, uint64_t param_2);
extern struct indirect_placeholder_22_ret_type indirect_placeholder_22(uint64_t param_0, uint64_t param_1, uint64_t param_2);
extern struct indirect_placeholder_23_ret_type indirect_placeholder_23(uint64_t param_0, uint64_t param_1, uint64_t param_2);
extern struct indirect_placeholder_24_ret_type indirect_placeholder_24(uint64_t param_0, uint64_t param_1, uint64_t param_2);
extern void indirect_placeholder_25(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint64_t param_3, uint64_t param_4, uint64_t param_5);
extern struct indirect_placeholder_26_ret_type indirect_placeholder_26(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint64_t param_3, uint64_t param_4);
void bb_main(uint64_t rdi, uint64_t rsi) {
    uint64_t var_38;
    uint64_t var_39;
    struct indirect_placeholder_26_ret_type var_17;
    uint64_t var_0;
    uint64_t var_1;
    uint64_t var_2;
    uint64_t var_3;
    uint32_t *var_4;
    uint64_t var_5;
    uint64_t *var_6;
    unsigned char *var_7;
    uint32_t *var_8;
    uint64_t *var_9;
    uint64_t *var_10;
    uint64_t var_11;
    uint64_t var_12;
    uint32_t *var_13;
    uint64_t local_sp_7;
    uint64_t local_sp_4;
    uint64_t var_72;
    uint64_t var_62;
    struct indirect_placeholder_21_ret_type var_63;
    uint64_t var_64;
    uint64_t var_65;
    uint64_t var_66;
    uint64_t var_67;
    uint64_t local_sp_1;
    uint64_t var_52;
    struct indirect_placeholder_22_ret_type var_53;
    uint64_t var_54;
    uint64_t var_55;
    uint64_t var_56;
    uint64_t var_57;
    uint64_t var_58;
    uint64_t local_sp_0;
    uint64_t var_59;
    uint64_t var_60;
    uint64_t var_61;
    uint64_t var_68;
    uint64_t var_69;
    uint64_t var_70;
    uint64_t var_71;
    uint64_t var_41;
    struct indirect_placeholder_23_ret_type var_42;
    uint64_t var_43;
    uint64_t var_44;
    uint64_t var_45;
    uint64_t var_46;
    uint64_t local_sp_3;
    uint64_t var_31;
    struct indirect_placeholder_24_ret_type var_32;
    uint64_t var_33;
    uint64_t var_34;
    uint64_t var_35;
    uint64_t var_36;
    uint64_t var_37;
    uint64_t local_sp_2;
    uint64_t var_40;
    uint64_t var_21;
    uint64_t var_22;
    uint64_t var_23;
    uint64_t var_24;
    bool var_25;
    unsigned char *var_26;
    uint64_t var_27;
    uint64_t var_28;
    uint64_t var_29;
    uint64_t var_30;
    uint64_t var_47;
    uint64_t var_48;
    uint64_t var_49;
    uint64_t var_50;
    uint64_t *var_51;
    uint32_t var_73;
    uint64_t local_sp_5;
    uint64_t var_74;
    uint64_t var_75;
    uint64_t var_76;
    uint64_t var_77;
    uint32_t var_78;
    uint64_t local_sp_6;
    uint64_t var_19;
    uint64_t var_20;
    uint64_t var_14;
    uint64_t var_15;
    uint64_t var_16;
    uint32_t var_18;
    unsigned int loop_state_var;
    var_0 = revng_init_local_sp(0UL);
    var_1 = init_rbp();
    var_2 = init_rbx();
    var_3 = var_0 + (-8L);
    *(uint64_t *)var_3 = var_1;
    *(uint64_t *)(var_0 + (-16L)) = var_2;
    var_4 = (uint32_t *)(var_0 + (-76L));
    *var_4 = (uint32_t)rdi;
    var_5 = var_0 + (-88L);
    var_6 = (uint64_t *)var_5;
    *var_6 = rsi;
    var_7 = (unsigned char *)(var_0 + (-25L));
    *var_7 = (unsigned char)'\x01';
    var_8 = (uint32_t *)(var_0 + (-32L));
    *var_8 = 1U;
    var_9 = (uint64_t *)(var_0 + (-40L));
    *var_9 = 0UL;
    var_10 = (uint64_t *)(var_0 + (-48L));
    *var_10 = 0UL;
    var_11 = **(uint64_t **)var_5;
    *(uint64_t *)(var_0 + (-96L)) = 4203292UL;
    indirect_placeholder_20(var_11);
    *(uint64_t *)(var_0 + (-104L)) = 4203307UL;
    indirect_placeholder();
    var_12 = var_0 + (-112L);
    *(uint64_t *)var_12 = 4203317UL;
    indirect_placeholder();
    var_13 = (uint32_t *)(var_0 + (-52L));
    local_sp_7 = var_12;
    while (1U)
        {
            var_14 = *var_6;
            var_15 = (uint64_t)*var_4;
            var_16 = local_sp_7 + (-8L);
            *(uint64_t *)var_16 = 4203347UL;
            var_17 = indirect_placeholder_26(4281329UL, 4280000UL, var_15, var_14, 0UL);
            var_18 = (uint32_t)var_17.field_0;
            *var_13 = var_18;
            local_sp_3 = var_16;
            local_sp_6 = var_16;
            local_sp_7 = var_16;
            if (var_18 != 4294967295U) {
                if ((long)((uint64_t)*(uint32_t *)6391288UL << 32UL) >= (long)((uint64_t)*var_4 << 32UL)) {
                    var_24 = *var_10;
                    if (var_24 != 0UL) {
                        loop_state_var = 0U;
                        break;
                    }
                    if (*var_9 != 0UL) {
                        loop_state_var = 0U;
                        break;
                    }
                    *var_9 = var_24;
                    loop_state_var = 0U;
                    break;
                }
                var_21 = var_17.field_3;
                var_22 = var_17.field_2;
                var_23 = var_17.field_1;
                *(uint64_t *)(local_sp_7 + (-16L)) = 4203709UL;
                indirect_placeholder_12(0UL, 4281351UL, var_23, 0UL, 0UL, var_22, var_21);
                *(uint64_t *)(local_sp_7 + (-24L)) = 4203719UL;
                indirect_placeholder_4(var_3, 1UL);
                abort();
            }
            if ((uint64_t)(var_18 + (-109)) == 0UL) {
                *var_8 = ((*var_8 & (-4)) | 2U);
                continue;
            }
            if ((int)var_18 > (int)109U) {
                if ((uint64_t)(var_18 + (-122)) == 0UL) {
                    *(unsigned char *)6391505UL = (unsigned char)'\x01';
                    continue;
                }
                if ((int)var_18 > (int)122U) {
                    if ((uint64_t)(var_18 + (-128)) == 0UL) {
                        *var_9 = *(uint64_t *)6391976UL;
                        continue;
                    }
                    if ((uint64_t)(var_18 + (-129)) != 0UL) {
                        loop_state_var = 1U;
                        break;
                    }
                    *var_10 = *(uint64_t *)6391976UL;
                    continue;
                }
                if ((uint64_t)(var_18 + (-113)) == 0UL) {
                    *(unsigned char *)6391136UL = (unsigned char)'\x00';
                    continue;
                }
                if ((uint64_t)(var_18 + (-115)) != 0UL) {
                    loop_state_var = 1U;
                    break;
                }
                *var_8 = (*var_8 | 4U);
                *(unsigned char *)6391504UL = (unsigned char)'\x00';
                continue;
            }
            if ((uint64_t)(var_18 + (-76)) == 0UL) {
                *var_8 = (*var_8 | 4U);
                *(unsigned char *)6391504UL = (unsigned char)'\x01';
                continue;
            }
            if ((int)var_18 > (int)76U) {
                if ((uint64_t)(var_18 + (-80)) == 0UL) {
                    *var_8 = (*var_8 & (-5));
                    *(unsigned char *)6391504UL = (unsigned char)'\x00';
                    continue;
                }
                if ((uint64_t)(var_18 + (-101)) != 0UL) {
                    loop_state_var = 1U;
                    break;
                }
                *var_8 = (*var_8 & (-4));
                continue;
            }
            if ((uint64_t)(var_18 + 131U) != 0UL) {
                if ((uint64_t)(var_18 + 130U) != 0UL) {
                    loop_state_var = 1U;
                    break;
                }
                *(uint64_t *)(local_sp_7 + (-16L)) = 4203607UL;
                indirect_placeholder_4(var_3, 0UL);
                abort();
            }
            var_19 = *(uint64_t *)6391144UL;
            *(uint64_t *)(local_sp_7 + (-16L)) = 4203659UL;
            indirect_placeholder_25(0UL, 4279640UL, var_19, 4281302UL, 0UL, 4281337UL);
            var_20 = local_sp_7 + (-24L);
            *(uint64_t *)var_20 = 4203669UL;
            indirect_placeholder();
            local_sp_6 = var_20;
            loop_state_var = 1U;
            break;
        }
    switch (loop_state_var) {
      case 1U:
        {
            *(uint64_t *)(local_sp_6 + (-8L)) = 4203679UL;
            indirect_placeholder_4(var_3, 1UL);
            abort();
        }
        break;
      case 0U:
        {
            var_25 = ((*var_8 & 3U) == 0U);
            var_26 = (unsigned char *)(var_0 + (-53L));
            *var_26 = var_25;
            var_27 = *var_9;
            if (var_27 != 0UL) {
                var_28 = (uint64_t)*var_8;
                var_29 = local_sp_7 + (-16L);
                *(uint64_t *)var_29 = 4203783UL;
                var_30 = indirect_placeholder_1(var_27, var_28);
                *(uint64_t *)6391512UL = var_30;
                local_sp_2 = var_29;
                if (var_30 == 0UL) {
                    var_31 = *var_9;
                    *(uint64_t *)(local_sp_7 + (-24L)) = 4203824UL;
                    var_32 = indirect_placeholder_24(var_31, 0UL, 3UL);
                    var_33 = var_32.field_0;
                    var_34 = var_32.field_1;
                    var_35 = var_32.field_2;
                    *(uint64_t *)(local_sp_7 + (-32L)) = 4203832UL;
                    indirect_placeholder();
                    var_36 = (uint64_t)*(uint32_t *)var_33;
                    var_37 = local_sp_7 + (-40L);
                    *(uint64_t *)var_37 = 4203859UL;
                    indirect_placeholder_12(0UL, 4281326UL, var_33, 1UL, var_36, var_34, var_35);
                    local_sp_2 = var_37;
                }
                local_sp_3 = local_sp_2;
                var_38 = *(uint64_t *)6391512UL;
                var_39 = local_sp_2 + (-8L);
                *(uint64_t *)var_39 = 4203880UL;
                var_40 = indirect_placeholder_6(var_38);
                local_sp_3 = var_39;
                if (*var_26 != '\x00' & (uint64_t)(unsigned char)var_40 == 1UL) {
                    var_41 = *var_9;
                    *(uint64_t *)(local_sp_2 + (-16L)) = 4203909UL;
                    var_42 = indirect_placeholder_23(var_41, 0UL, 3UL);
                    var_43 = var_42.field_0;
                    var_44 = var_42.field_1;
                    var_45 = var_42.field_2;
                    var_46 = local_sp_2 + (-24L);
                    *(uint64_t *)var_46 = 4203937UL;
                    indirect_placeholder_12(0UL, 4281326UL, var_43, 1UL, 20UL, var_44, var_45);
                    local_sp_3 = var_46;
                }
            }
            var_47 = *var_10;
            local_sp_4 = local_sp_3;
            if (var_47 == *var_9) {
                *(uint64_t *)6391520UL = *(uint64_t *)6391512UL;
            } else {
                if (var_47 != 0UL) {
                    var_48 = (uint64_t)*var_8;
                    var_49 = local_sp_3 + (-8L);
                    *(uint64_t *)var_49 = 4203994UL;
                    var_50 = indirect_placeholder_1(var_47, var_48);
                    var_51 = (uint64_t *)(var_0 + (-64L));
                    *var_51 = var_50;
                    local_sp_0 = var_49;
                    if (var_50 == 0UL) {
                        var_52 = *var_10;
                        *(uint64_t *)(local_sp_3 + (-16L)) = 4204027UL;
                        var_53 = indirect_placeholder_22(var_52, 0UL, 3UL);
                        var_54 = var_53.field_0;
                        var_55 = var_53.field_1;
                        var_56 = var_53.field_2;
                        *(uint64_t *)(local_sp_3 + (-24L)) = 4204035UL;
                        indirect_placeholder();
                        var_57 = (uint64_t)*(uint32_t *)var_54;
                        var_58 = local_sp_3 + (-32L);
                        *(uint64_t *)var_58 = 4204062UL;
                        indirect_placeholder_12(0UL, 4281326UL, var_54, 1UL, var_57, var_55, var_56);
                        local_sp_0 = var_58;
                    }
                    local_sp_1 = local_sp_0;
                    var_59 = *var_51;
                    var_60 = local_sp_0 + (-8L);
                    *(uint64_t *)var_60 = 4204080UL;
                    var_61 = indirect_placeholder_6(var_59);
                    local_sp_1 = var_60;
                    if (*var_26 != '\x00' & (uint64_t)(unsigned char)var_61 == 1UL) {
                        var_62 = *var_10;
                        *(uint64_t *)(local_sp_0 + (-16L)) = 4204109UL;
                        var_63 = indirect_placeholder_21(var_62, 0UL, 3UL);
                        var_64 = var_63.field_0;
                        var_65 = var_63.field_1;
                        var_66 = var_63.field_2;
                        var_67 = local_sp_0 + (-24L);
                        *(uint64_t *)var_67 = 4204137UL;
                        indirect_placeholder_12(0UL, 4281326UL, var_64, 1UL, 20UL, var_65, var_66);
                        local_sp_1 = var_67;
                    }
                    var_68 = *(uint64_t *)6391512UL;
                    var_69 = *var_51;
                    var_70 = local_sp_1 + (-8L);
                    *(uint64_t *)var_70 = 4204159UL;
                    var_71 = indirect_placeholder_1(var_69, var_68);
                    local_sp_4 = var_70;
                    if ((uint64_t)(unsigned char)var_71 == 0UL) {
                        var_72 = local_sp_1 + (-16L);
                        *(uint64_t *)var_72 = 4204188UL;
                        indirect_placeholder();
                        *(uint64_t *)6391520UL = *(uint64_t *)6391512UL;
                        *(uint64_t *)6391512UL = 0UL;
                        local_sp_4 = var_72;
                    } else {
                        *(uint64_t *)6391520UL = *var_51;
                    }
                }
            }
            var_73 = *(uint32_t *)6391288UL;
            local_sp_5 = local_sp_4;
            while ((long)((uint64_t)var_73 << 32UL) >= (long)((uint64_t)*var_4 << 32UL))
                {
                    var_74 = *(uint64_t *)(*var_6 + ((uint64_t)var_73 << 3UL));
                    var_75 = (uint64_t)*var_8;
                    var_76 = local_sp_5 + (-8L);
                    *(uint64_t *)var_76 = 4204254UL;
                    var_77 = indirect_placeholder_1(var_74, var_75);
                    *var_7 = ((var_77 & (uint64_t)*var_7) != 0UL);
                    var_78 = *(uint32_t *)6391288UL + 1U;
                    *(uint32_t *)6391288UL = var_78;
                    var_73 = var_78;
                    local_sp_5 = var_76;
                }
            return;
        }
        break;
    }
}

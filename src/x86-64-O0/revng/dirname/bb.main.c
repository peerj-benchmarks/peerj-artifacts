typedef unsigned char __u_char;
typedef unsigned short __u_short;
typedef unsigned int __u_int;
typedef unsigned long __u_long;
typedef signed char __int8_t;
typedef unsigned char __uint8_t;
typedef short __int16_t;
typedef unsigned short __uint16_t;
typedef int __int32_t;
typedef unsigned int __uint32_t;
typedef long __int64_t;
typedef unsigned long __uint64_t;
typedef __int8_t __int_least8_t;
typedef __uint8_t __uint_least8_t;
typedef __int16_t __int_least16_t;
typedef __uint16_t __uint_least16_t;
typedef __int32_t __int_least32_t;
typedef __uint32_t __uint_least32_t;
typedef __int64_t __int_least64_t;
typedef __uint64_t __uint_least64_t;
typedef long __quad_t;
typedef unsigned long __u_quad_t;
typedef long __intmax_t;
typedef unsigned long __uintmax_t;
typedef unsigned long __dev_t;
typedef unsigned int __uid_t;
typedef unsigned int __gid_t;
typedef unsigned long __ino_t;
typedef unsigned long __ino64_t;
typedef unsigned int __mode_t;
typedef unsigned long __nlink_t;
typedef long __off_t;
typedef long __off64_t;
typedef int __pid_t;
typedef struct {
    int __val[2];
} __fsid_t;
typedef long __clock_t;
typedef unsigned long __rlim_t;
typedef unsigned long __rlim64_t;
typedef unsigned int __id_t;
typedef long __time_t;
typedef unsigned int __useconds_t;
typedef long __suseconds_t;
typedef int __daddr_t;
typedef int __key_t;
typedef int __clockid_t;
typedef void *__timer_t;
typedef long __blksize_t;
typedef long __blkcnt_t;
typedef long __blkcnt64_t;
typedef unsigned long __fsblkcnt_t;
typedef unsigned long __fsblkcnt64_t;
typedef unsigned long __fsfilcnt_t;
typedef unsigned long __fsfilcnt64_t;
typedef long __fsword_t;
typedef long __ssize_t;
typedef long __syscall_slong_t;
typedef unsigned long __syscall_ulong_t;
typedef __off64_t __loff_t;
typedef char *__caddr_t;
typedef long __intptr_t;
typedef unsigned int __socklen_t;
typedef int __sig_atomic_t;
typedef __int8_t int8_t;
typedef __int16_t int16_t;
typedef __int32_t int32_t;
typedef __int64_t int64_t;
typedef __uint8_t uint8_t;
typedef __uint16_t uint16_t;
typedef __uint32_t uint32_t;
typedef __uint64_t uint64_t;
typedef __int_least8_t int_least8_t;
typedef __int_least16_t int_least16_t;
typedef __int_least32_t int_least32_t;
typedef __int_least64_t int_least64_t;
typedef __uint_least8_t uint_least8_t;
typedef __uint_least16_t uint_least16_t;
typedef __uint_least32_t uint_least32_t;
typedef __uint_least64_t uint_least64_t;
typedef signed char int_fast8_t;
typedef long int_fast16_t;
typedef long int_fast32_t;
typedef long int_fast64_t;
typedef unsigned char uint_fast8_t;
typedef unsigned long uint_fast16_t;
typedef unsigned long uint_fast32_t;
typedef unsigned long uint_fast64_t;
typedef long intptr_t;
typedef unsigned long uintptr_t;
typedef __intmax_t intmax_t;
typedef __uintmax_t uintmax_t;
struct indirect_placeholder_8_ret_type;
struct indirect_placeholder_8_ret_type {
    uint64_t field_0;
    uint64_t field_1;
    uint64_t field_2;
    uint64_t field_3;
};
extern void abort(void);
extern uint64_t revng_init_local_sp(uint64_t param_0);
extern uint64_t init_rbp(void);
extern void indirect_placeholder_1(void);
extern void indirect_placeholder_4(uint64_t param_0, uint64_t param_1);
extern uint64_t indirect_placeholder_6(void);
extern void indirect_placeholder_3(uint64_t param_0);
extern void indirect_placeholder_5(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint64_t param_3, uint64_t param_4, uint64_t param_5, uint64_t param_6);
extern void indirect_placeholder_7(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint64_t param_3, uint64_t param_4, uint64_t param_5);
extern struct indirect_placeholder_8_ret_type indirect_placeholder_8(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint64_t param_3, uint64_t param_4);
void bb_main(uint64_t rdi, uint64_t rsi) {
    struct indirect_placeholder_8_ret_type var_13;
    uint64_t var_0;
    uint64_t var_1;
    uint64_t var_2;
    uint32_t *var_3;
    uint64_t var_4;
    uint64_t *var_5;
    unsigned char *var_6;
    uint64_t var_7;
    uint64_t var_8;
    uint32_t *var_9;
    uint64_t local_sp_2;
    uint64_t var_26;
    uint32_t var_27;
    uint32_t var_23;
    uint32_t var_18;
    uint64_t var_19;
    uint32_t var_20;
    uint64_t var_28;
    uint64_t var_29;
    uint64_t var_30;
    uint64_t *var_21;
    uint64_t *var_22;
    uint32_t var_24;
    uint64_t local_sp_0;
    uint64_t var_25;
    uint64_t local_sp_1;
    uint64_t var_15;
    uint64_t var_16;
    uint64_t *var_17;
    uint64_t var_10;
    uint64_t var_11;
    uint64_t var_12;
    uint32_t var_14;
    unsigned int loop_state_var;
    var_0 = revng_init_local_sp(0UL);
    var_1 = init_rbp();
    var_2 = var_0 + (-8L);
    *(uint64_t *)var_2 = var_1;
    var_3 = (uint32_t *)(var_0 + (-44L));
    *var_3 = (uint32_t)rdi;
    var_4 = var_0 + (-56L);
    var_5 = (uint64_t *)var_4;
    *var_5 = rsi;
    var_6 = (unsigned char *)(var_0 + (-9L));
    *var_6 = (unsigned char)'\x00';
    var_7 = **(uint64_t **)var_4;
    *(uint64_t *)(var_0 + (-64L)) = 4201686UL;
    indirect_placeholder_3(var_7);
    *(uint64_t *)(var_0 + (-72L)) = 4201701UL;
    indirect_placeholder_1();
    var_8 = var_0 + (-80L);
    *(uint64_t *)var_8 = 4201711UL;
    indirect_placeholder_1();
    var_9 = (uint32_t *)(var_0 + (-36L));
    local_sp_2 = var_8;
    while (1U)
        {
            var_10 = *var_5;
            var_11 = (uint64_t)*var_3;
            var_12 = local_sp_2 + (-8L);
            *(uint64_t *)var_12 = 4201741UL;
            var_13 = indirect_placeholder_8(4266066UL, 4265408UL, var_11, var_10, 0UL);
            var_14 = (uint32_t)var_13.field_0;
            *var_9 = var_14;
            local_sp_0 = var_12;
            local_sp_1 = var_12;
            local_sp_2 = var_12;
            if (var_14 != 4294967295U) {
                var_18 = *(uint32_t *)6374488UL;
                var_19 = (uint64_t)var_18;
                var_20 = *var_3;
                var_23 = var_20;
                var_24 = var_18;
                if ((long)((var_19 << 32UL) + 4294967296UL) <= (long)((uint64_t)var_20 << 32UL)) {
                    var_21 = (uint64_t *)(var_0 + (-24L));
                    var_22 = (uint64_t *)(var_0 + (-32L));
                    loop_state_var = 0U;
                    break;
                }
                var_28 = var_13.field_3;
                var_29 = var_13.field_2;
                var_30 = var_13.field_1;
                *(uint64_t *)(local_sp_2 + (-16L)) = 4201923UL;
                indirect_placeholder_5(0UL, 4266097UL, var_30, 0UL, 0UL, var_29, var_28);
                *(uint64_t *)(local_sp_2 + (-24L)) = 4201933UL;
                indirect_placeholder_4(var_2, 1UL);
                abort();
            }
            if ((uint64_t)(var_14 + 130U) == 0UL) {
                *(uint64_t *)(local_sp_2 + (-16L)) = 4201811UL;
                indirect_placeholder_4(var_2, 0UL);
                abort();
            }
            if ((uint64_t)(var_14 + (-122)) == 0UL) {
                *var_6 = (unsigned char)'\x01';
                continue;
            }
            if (var_14 != 4294967165U) {
                loop_state_var = 1U;
                break;
            }
            var_15 = *(uint64_t *)6374336UL;
            var_16 = local_sp_2 + (-24L);
            var_17 = (uint64_t *)var_16;
            *var_17 = 0UL;
            *(uint64_t *)(local_sp_2 + (-32L)) = 4201869UL;
            indirect_placeholder_7(0UL, 4265112UL, var_15, 4266058UL, 4266068UL, 4266081UL);
            *var_17 = 4201883UL;
            indirect_placeholder_1();
            local_sp_1 = var_16;
            loop_state_var = 1U;
            break;
        }
    switch (loop_state_var) {
      case 1U:
        {
            *(uint64_t *)(local_sp_1 + (-8L)) = 4201893UL;
            indirect_placeholder_4(var_2, 1UL);
            abort();
        }
        break;
      case 0U:
        {
            while ((long)((uint64_t)var_24 << 32UL) >= (long)((uint64_t)var_23 << 32UL))
                {
                    *var_21 = *(uint64_t *)(*var_5 + ((uint64_t)var_24 << 3UL));
                    *(uint64_t *)(local_sp_0 + (-8L)) = 4201980UL;
                    var_25 = indirect_placeholder_6();
                    *var_22 = var_25;
                    if (var_25 == 0UL) {
                        *var_21 = 4266113UL;
                        *var_22 = 1UL;
                    }
                    *(uint64_t *)(local_sp_0 + (-16L)) = 4202035UL;
                    indirect_placeholder_1();
                    var_26 = local_sp_0 + (-24L);
                    *(uint64_t *)var_26 = 4202060UL;
                    indirect_placeholder_1();
                    var_27 = *(uint32_t *)6374488UL + 1U;
                    *(uint32_t *)6374488UL = var_27;
                    var_23 = *var_3;
                    var_24 = var_27;
                    local_sp_0 = var_26;
                }
            return;
        }
        break;
    }
}

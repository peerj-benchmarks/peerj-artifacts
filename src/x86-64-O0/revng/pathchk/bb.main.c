typedef unsigned char __u_char;
typedef unsigned short __u_short;
typedef unsigned int __u_int;
typedef unsigned long __u_long;
typedef signed char __int8_t;
typedef unsigned char __uint8_t;
typedef short __int16_t;
typedef unsigned short __uint16_t;
typedef int __int32_t;
typedef unsigned int __uint32_t;
typedef long __int64_t;
typedef unsigned long __uint64_t;
typedef __int8_t __int_least8_t;
typedef __uint8_t __uint_least8_t;
typedef __int16_t __int_least16_t;
typedef __uint16_t __uint_least16_t;
typedef __int32_t __int_least32_t;
typedef __uint32_t __uint_least32_t;
typedef __int64_t __int_least64_t;
typedef __uint64_t __uint_least64_t;
typedef long __quad_t;
typedef unsigned long __u_quad_t;
typedef long __intmax_t;
typedef unsigned long __uintmax_t;
typedef unsigned long __dev_t;
typedef unsigned int __uid_t;
typedef unsigned int __gid_t;
typedef unsigned long __ino_t;
typedef unsigned long __ino64_t;
typedef unsigned int __mode_t;
typedef unsigned long __nlink_t;
typedef long __off_t;
typedef long __off64_t;
typedef int __pid_t;
typedef struct {
    int __val[2];
} __fsid_t;
typedef long __clock_t;
typedef unsigned long __rlim_t;
typedef unsigned long __rlim64_t;
typedef unsigned int __id_t;
typedef long __time_t;
typedef unsigned int __useconds_t;
typedef long __suseconds_t;
typedef int __daddr_t;
typedef int __key_t;
typedef int __clockid_t;
typedef void *__timer_t;
typedef long __blksize_t;
typedef long __blkcnt_t;
typedef long __blkcnt64_t;
typedef unsigned long __fsblkcnt_t;
typedef unsigned long __fsblkcnt64_t;
typedef unsigned long __fsfilcnt_t;
typedef unsigned long __fsfilcnt64_t;
typedef long __fsword_t;
typedef long __ssize_t;
typedef long __syscall_slong_t;
typedef unsigned long __syscall_ulong_t;
typedef __off64_t __loff_t;
typedef char *__caddr_t;
typedef long __intptr_t;
typedef unsigned int __socklen_t;
typedef int __sig_atomic_t;
typedef __int8_t int8_t;
typedef __int16_t int16_t;
typedef __int32_t int32_t;
typedef __int64_t int64_t;
typedef __uint8_t uint8_t;
typedef __uint16_t uint16_t;
typedef __uint32_t uint32_t;
typedef __uint64_t uint64_t;
typedef __int_least8_t int_least8_t;
typedef __int_least16_t int_least16_t;
typedef __int_least32_t int_least32_t;
typedef __int_least64_t int_least64_t;
typedef __uint_least8_t uint_least8_t;
typedef __uint_least16_t uint_least16_t;
typedef __uint_least32_t uint_least32_t;
typedef __uint_least64_t uint_least64_t;
typedef signed char int_fast8_t;
typedef long int_fast16_t;
typedef long int_fast32_t;
typedef long int_fast64_t;
typedef unsigned char uint_fast8_t;
typedef unsigned long uint_fast16_t;
typedef unsigned long uint_fast32_t;
typedef unsigned long uint_fast64_t;
typedef long intptr_t;
typedef unsigned long uintptr_t;
typedef __intmax_t intmax_t;
typedef __uintmax_t uintmax_t;
struct indirect_placeholder_17_ret_type;
struct indirect_placeholder_17_ret_type {
    uint64_t field_0;
    uint64_t field_1;
    uint64_t field_2;
    uint64_t field_3;
};
extern void abort(void);
extern uint64_t revng_init_local_sp(uint64_t param_0);
extern uint64_t init_rbp(void);
extern void indirect_placeholder(void);
extern uint64_t indirect_placeholder_11(uint64_t param_0, uint64_t param_1, uint64_t param_2);
extern void indirect_placeholder_9(uint64_t param_0, uint64_t param_1);
extern void indirect_placeholder_15(uint64_t param_0);
extern void indirect_placeholder_5(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint64_t param_3, uint64_t param_4, uint64_t param_5, uint64_t param_6);
extern void indirect_placeholder_16(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint64_t param_3, uint64_t param_4, uint64_t param_5);
extern struct indirect_placeholder_17_ret_type indirect_placeholder_17(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint64_t param_3, uint64_t param_4);
void bb_main(uint64_t rdi, uint64_t rsi) {
    struct indirect_placeholder_17_ret_type var_15;
    uint64_t var_0;
    uint64_t var_1;
    uint64_t var_2;
    uint32_t *var_3;
    uint64_t var_4;
    uint64_t *var_5;
    unsigned char *var_6;
    unsigned char *var_7;
    unsigned char *var_8;
    uint64_t var_9;
    uint64_t var_10;
    uint32_t *var_11;
    uint64_t local_sp_2;
    uint32_t var_20;
    uint32_t var_21;
    uint64_t var_30;
    uint64_t var_31;
    uint64_t var_32;
    uint32_t var_22;
    uint32_t var_23;
    uint64_t local_sp_0;
    uint64_t var_24;
    uint64_t var_25;
    uint64_t var_26;
    uint64_t var_27;
    uint64_t var_28;
    uint32_t var_29;
    uint64_t local_sp_1;
    uint64_t var_17;
    uint64_t var_18;
    uint64_t *var_19;
    uint64_t var_12;
    uint64_t var_13;
    uint64_t var_14;
    uint32_t var_16;
    unsigned int loop_state_var;
    var_0 = revng_init_local_sp(0UL);
    var_1 = init_rbp();
    var_2 = var_0 + (-8L);
    *(uint64_t *)var_2 = var_1;
    var_3 = (uint32_t *)(var_0 + (-28L));
    *var_3 = (uint32_t)rdi;
    var_4 = var_0 + (-40L);
    var_5 = (uint64_t *)var_4;
    *var_5 = rsi;
    var_6 = (unsigned char *)(var_0 + (-9L));
    *var_6 = (unsigned char)'\x01';
    var_7 = (unsigned char *)(var_0 + (-10L));
    *var_7 = (unsigned char)'\x00';
    var_8 = (unsigned char *)(var_0 + (-11L));
    *var_8 = (unsigned char)'\x00';
    var_9 = **(uint64_t **)var_4;
    *(uint64_t *)(var_0 + (-48L)) = 4201760UL;
    indirect_placeholder_15(var_9);
    *(uint64_t *)(var_0 + (-56L)) = 4201775UL;
    indirect_placeholder();
    var_10 = var_0 + (-64L);
    *(uint64_t *)var_10 = 4201785UL;
    indirect_placeholder();
    var_11 = (uint32_t *)(var_0 + (-16L));
    local_sp_2 = var_10;
    while (1U)
        {
            var_12 = *var_5;
            var_13 = (uint64_t)*var_3;
            var_14 = local_sp_2 + (-8L);
            *(uint64_t *)var_14 = 4201976UL;
            var_15 = indirect_placeholder_17(4268551UL, 4267968UL, var_13, var_12, 0UL);
            var_16 = (uint32_t)var_15.field_0;
            *var_11 = var_16;
            local_sp_0 = var_14;
            local_sp_1 = var_14;
            local_sp_2 = var_14;
            if (var_16 != 4294967295U) {
                var_20 = *(uint32_t *)6378616UL;
                var_21 = *var_3;
                var_22 = var_21;
                var_23 = var_20;
                if ((uint64_t)(var_20 - var_21) != 0UL) {
                    loop_state_var = 1U;
                    break;
                }
                var_30 = var_15.field_3;
                var_31 = var_15.field_2;
                var_32 = var_15.field_1;
                *(uint64_t *)(local_sp_2 + (-16L)) = 4202025UL;
                indirect_placeholder_5(0UL, 4268555UL, var_32, 0UL, 0UL, var_31, var_30);
                *(uint64_t *)(local_sp_2 + (-24L)) = 4202035UL;
                indirect_placeholder_9(var_2, 1UL);
                abort();
            }
            if ((uint64_t)(var_16 + (-80)) == 0UL) {
                *var_8 = (unsigned char)'\x01';
                continue;
            }
            if ((int)var_16 > (int)80U) {
                if ((uint64_t)(var_16 + (-112)) == 0UL) {
                    *var_7 = (unsigned char)'\x01';
                    continue;
                }
                if ((uint64_t)(var_16 + (-128)) != 0UL) {
                    loop_state_var = 0U;
                    break;
                }
                *var_7 = (unsigned char)'\x01';
                *var_8 = (unsigned char)'\x01';
                continue;
            }
            if ((uint64_t)(var_16 + 131U) == 0UL) {
                if ((uint64_t)(var_16 + 130U) != 0UL) {
                    loop_state_var = 0U;
                    break;
                }
                *(uint64_t *)(local_sp_2 + (-16L)) = 4201863UL;
                indirect_placeholder_9(var_2, 0UL);
                abort();
            }
            var_17 = *(uint64_t *)6378464UL;
            *(uint64_t *)(local_sp_2 + (-16L)) = 0UL;
            var_18 = local_sp_2 + (-24L);
            var_19 = (uint64_t *)var_18;
            *var_19 = 4268538UL;
            *(uint64_t *)(local_sp_2 + (-32L)) = 4201922UL;
            indirect_placeholder_16(0UL, 4267672UL, var_17, 4268502UL, 4268526UL, 4268510UL);
            *var_19 = 4201936UL;
            indirect_placeholder();
            local_sp_1 = var_18;
            loop_state_var = 0U;
            break;
        }
    switch (loop_state_var) {
      case 0U:
        {
            *(uint64_t *)(local_sp_1 + (-8L)) = 4201946UL;
            indirect_placeholder_9(var_2, 1UL);
            abort();
        }
        break;
      case 1U:
        {
            while ((long)((uint64_t)var_23 << 32UL) >= (long)((uint64_t)var_22 << 32UL))
                {
                    var_24 = (uint64_t)*var_8;
                    var_25 = (uint64_t)*var_7;
                    var_26 = *(uint64_t *)(*var_5 + ((uint64_t)var_23 << 3UL));
                    var_27 = local_sp_0 + (-8L);
                    *(uint64_t *)var_27 = 4202081UL;
                    var_28 = indirect_placeholder_11(var_24, var_26, var_25);
                    *var_6 = ((var_28 & (uint64_t)*var_6) != 0UL);
                    var_29 = *(uint32_t *)6378616UL + 1U;
                    *(uint32_t *)6378616UL = var_29;
                    var_22 = *var_3;
                    var_23 = var_29;
                    local_sp_0 = var_27;
                }
            return;
        }
        break;
    }
}

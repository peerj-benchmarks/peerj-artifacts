typedef unsigned char __u_char;
typedef unsigned short __u_short;
typedef unsigned int __u_int;
typedef unsigned long __u_long;
typedef signed char __int8_t;
typedef unsigned char __uint8_t;
typedef short __int16_t;
typedef unsigned short __uint16_t;
typedef int __int32_t;
typedef unsigned int __uint32_t;
typedef long __int64_t;
typedef unsigned long __uint64_t;
typedef __int8_t __int_least8_t;
typedef __uint8_t __uint_least8_t;
typedef __int16_t __int_least16_t;
typedef __uint16_t __uint_least16_t;
typedef __int32_t __int_least32_t;
typedef __uint32_t __uint_least32_t;
typedef __int64_t __int_least64_t;
typedef __uint64_t __uint_least64_t;
typedef long __quad_t;
typedef unsigned long __u_quad_t;
typedef long __intmax_t;
typedef unsigned long __uintmax_t;
typedef unsigned long __dev_t;
typedef unsigned int __uid_t;
typedef unsigned int __gid_t;
typedef unsigned long __ino_t;
typedef unsigned long __ino64_t;
typedef unsigned int __mode_t;
typedef unsigned long __nlink_t;
typedef long __off_t;
typedef long __off64_t;
typedef int __pid_t;
typedef struct {
    int __val[2];
} __fsid_t;
typedef long __clock_t;
typedef unsigned long __rlim_t;
typedef unsigned long __rlim64_t;
typedef unsigned int __id_t;
typedef long __time_t;
typedef unsigned int __useconds_t;
typedef long __suseconds_t;
typedef int __daddr_t;
typedef int __key_t;
typedef int __clockid_t;
typedef void *__timer_t;
typedef long __blksize_t;
typedef long __blkcnt_t;
typedef long __blkcnt64_t;
typedef unsigned long __fsblkcnt_t;
typedef unsigned long __fsblkcnt64_t;
typedef unsigned long __fsfilcnt_t;
typedef unsigned long __fsfilcnt64_t;
typedef long __fsword_t;
typedef long __ssize_t;
typedef long __syscall_slong_t;
typedef unsigned long __syscall_ulong_t;
typedef __off64_t __loff_t;
typedef char *__caddr_t;
typedef long __intptr_t;
typedef unsigned int __socklen_t;
typedef int __sig_atomic_t;
typedef __int8_t int8_t;
typedef __int16_t int16_t;
typedef __int32_t int32_t;
typedef __int64_t int64_t;
typedef __uint8_t uint8_t;
typedef __uint16_t uint16_t;
typedef __uint32_t uint32_t;
typedef __uint64_t uint64_t;
typedef __int_least8_t int_least8_t;
typedef __int_least16_t int_least16_t;
typedef __int_least32_t int_least32_t;
typedef __int_least64_t int_least64_t;
typedef __uint_least8_t uint_least8_t;
typedef __uint_least16_t uint_least16_t;
typedef __uint_least32_t uint_least32_t;
typedef __uint_least64_t uint_least64_t;
typedef signed char int_fast8_t;
typedef long int_fast16_t;
typedef long int_fast32_t;
typedef long int_fast64_t;
typedef unsigned char uint_fast8_t;
typedef unsigned long uint_fast16_t;
typedef unsigned long uint_fast32_t;
typedef unsigned long uint_fast64_t;
typedef long intptr_t;
typedef unsigned long uintptr_t;
typedef __intmax_t intmax_t;
typedef __uintmax_t uintmax_t;
typedef _Bool bool;
struct indirect_placeholder_12_ret_type;
struct indirect_placeholder_13_ret_type;
struct indirect_placeholder_12_ret_type {
    uint64_t field_0;
    uint64_t field_1;
    uint64_t field_2;
};
struct indirect_placeholder_13_ret_type {
    uint64_t field_0;
    uint64_t field_1;
    uint64_t field_2;
    uint64_t field_3;
};
extern uint64_t revng_init_local_sp(uint64_t param_0);
extern uint64_t init_rbp(void);
extern uint64_t indirect_placeholder_1(uint64_t param_0, uint64_t param_1);
extern uint64_t init_cc_src2(void);
extern uint64_t helper_cc_compute_c_wrapper(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint32_t param_3);
extern void indirect_placeholder(void);
extern uint64_t indirect_placeholder_11(uint64_t param_0, uint64_t param_1, uint64_t param_2);
extern uint64_t init_rbx(void);
extern uint64_t indirect_placeholder_2(uint64_t param_0);
extern uint64_t init_r8(void);
extern uint64_t init_r9(void);
extern void indirect_placeholder_5(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint64_t param_3, uint64_t param_4, uint64_t param_5, uint64_t param_6);
extern struct indirect_placeholder_12_ret_type indirect_placeholder_12(uint64_t param_0, uint64_t param_1);
extern struct indirect_placeholder_13_ret_type indirect_placeholder_13(uint64_t param_0);
uint64_t bb_validate_file_name(uint64_t rdx, uint64_t rdi, uint64_t rsi) {
    uint64_t var_0;
    uint64_t var_1;
    uint64_t var_2;
    uint64_t var_3;
    uint64_t var_4;
    uint64_t var_5;
    uint64_t var_6;
    uint64_t *var_7;
    uint64_t var_8;
    unsigned char *var_9;
    unsigned char *var_10;
    uint64_t var_11;
    uint64_t var_12;
    uint64_t *var_13;
    unsigned char *var_14;
    uint64_t local_sp_8;
    uint64_t var_79;
    uint64_t *var_94;
    uint64_t var_95;
    uint64_t *var_96;
    unsigned char var_97;
    unsigned char *var_98;
    uint64_t var_99;
    uint64_t var_100;
    uint64_t var_101;
    uint64_t var_102;
    uint64_t var_103;
    uint64_t var_75;
    uint64_t var_104;
    uint64_t var_105;
    uint64_t var_106;
    uint64_t r9_0;
    uint64_t local_sp_0;
    uint64_t var_87;
    uint64_t var_88;
    uint64_t var_89;
    uint64_t var_90;
    uint64_t var_91;
    bool var_92;
    uint64_t var_93;
    uint64_t var_80;
    unsigned char var_81;
    uint64_t var_82;
    uint64_t var_83;
    uint64_t var_84;
    uint64_t var_85;
    uint64_t var_86;
    uint64_t var_62;
    uint64_t var_63;
    uint64_t local_sp_3;
    uint64_t var_43;
    unsigned char var_65;
    uint64_t var_64;
    uint64_t var_59;
    uint64_t var_38;
    uint64_t var_39;
    uint64_t var_40;
    uint64_t var_36;
    uint32_t var_37;
    uint64_t *var_42;
    uint64_t var_41;
    uint64_t local_sp_1;
    uint64_t *_pre_phi210;
    uint64_t rcx_0;
    uint64_t var_27;
    uint64_t var_28;
    uint64_t var_29;
    uint64_t var_26;
    uint64_t local_sp_2;
    bool var_30;
    unsigned char var_53;
    uint64_t *var_31;
    uint64_t var_44;
    uint64_t *var_45;
    uint64_t var_46;
    uint64_t *var_47;
    uint64_t var_48;
    struct indirect_placeholder_12_ret_type var_49;
    uint64_t var_50;
    uint64_t var_51;
    uint64_t var_52;
    unsigned char *var_54;
    uint64_t local_sp_4;
    uint64_t var_55;
    uint64_t var_56;
    uint64_t *var_57;
    uint64_t *var_58;
    uint64_t local_sp_5;
    uint64_t var_60;
    uint64_t var_61;
    uint64_t *var_66;
    uint64_t local_sp_6;
    uint64_t spec_select206;
    uint64_t *var_67;
    uint64_t var_68;
    uint64_t var_69;
    uint64_t *var_70;
    uint64_t *var_71;
    unsigned char *var_72;
    uint64_t *var_73;
    uint64_t *var_74;
    uint64_t local_sp_7;
    uint64_t var_76;
    uint64_t var_77;
    unsigned char **var_78;
    uint64_t spec_select207;
    uint64_t *var_32;
    uint64_t var_33;
    uint64_t var_34;
    uint64_t *var_35;
    uint64_t rax_0;
    uint64_t var_15;
    uint64_t var_16;
    struct indirect_placeholder_13_ret_type var_17;
    uint64_t r8_0;
    bool var_18;
    uint64_t var_19;
    uint64_t var_20;
    uint64_t var_21;
    uint64_t var_22;
    uint64_t var_23;
    uint64_t var_24;
    uint64_t var_25;
    var_0 = revng_init_local_sp(0UL);
    var_1 = init_rbp();
    var_2 = init_cc_src2();
    var_3 = init_rbx();
    var_4 = init_r8();
    var_5 = init_r9();
    *(uint64_t *)(var_0 + (-8L)) = var_1;
    *(uint64_t *)(var_0 + (-16L)) = var_3;
    var_6 = var_0 + (-320L);
    var_7 = (uint64_t *)var_6;
    *var_7 = rdi;
    var_8 = (uint64_t)(uint32_t)rsi;
    var_9 = (unsigned char *)(var_0 + (-324L));
    *var_9 = (unsigned char)rsi;
    var_10 = (unsigned char *)(var_0 + (-328L));
    *var_10 = (unsigned char)rdx;
    var_11 = *var_7;
    var_12 = var_0 + (-336L);
    *(uint64_t *)var_12 = 4202655UL;
    indirect_placeholder();
    var_13 = (uint64_t *)(var_0 + (-72L));
    *var_13 = var_11;
    var_14 = (unsigned char *)(var_0 + (-34L));
    *var_14 = (unsigned char)'\x00';
    local_sp_8 = var_12;
    r9_0 = var_5;
    var_65 = (unsigned char)'\x01';
    rcx_0 = var_8;
    var_53 = (unsigned char)'\x00';
    rax_0 = 0UL;
    r8_0 = var_4;
    if (*var_10 != '\x00') {
        var_15 = *var_7;
        var_16 = var_0 + (-344L);
        *(uint64_t *)var_16 = 4202687UL;
        var_17 = indirect_placeholder_13(var_15);
        local_sp_8 = var_16;
        rcx_0 = var_17.field_1;
        r8_0 = var_17.field_2;
        r9_0 = var_17.field_3;
        if ((uint64_t)(unsigned char)var_17.field_0 == 1UL) {
            return rax_0;
        }
    }
    var_18 = (*var_9 == '\x00');
    rax_0 = 1UL;
    if (var_18) {
        if (*var_10 != '\x00' & *var_13 != 0UL) {
            *(uint64_t *)(local_sp_8 + (-8L)) = 4202754UL;
            indirect_placeholder_5(0UL, 4268737UL, rcx_0, 0UL, 0UL, r8_0, r9_0);
            return rax_0;
        }
    }
    if (*var_13 != 0UL) {
        *(uint64_t *)(local_sp_8 + (-8L)) = 4202754UL;
        indirect_placeholder_5(0UL, 4268737UL, rcx_0, 0UL, 0UL, r8_0, r9_0);
        return rax_0;
    }
    if (!var_18) {
        var_19 = *var_13;
        var_20 = *var_7;
        var_21 = local_sp_8 + (-8L);
        *(uint64_t *)var_21 = 4202795UL;
        var_22 = indirect_placeholder_1(var_20, var_19);
        local_sp_2 = var_21;
        if ((uint64_t)(unsigned char)var_22 == 1UL) {
            return rax_0;
        }
        var_30 = (*var_9 == '\x00');
        local_sp_3 = local_sp_2;
        local_sp_4 = local_sp_2;
        if (var_30) {
            if (*var_14 != '\x01' & *var_13 <= 1023UL) {
                if (var_30) {
                    var_31 = (uint64_t *)(var_0 + (-48L));
                    *var_31 = 256UL;
                    _pre_phi210 = var_31;
                } else {
                    spec_select207 = (**(unsigned char **)var_6 == '/') ? 4268756UL : 4268758UL;
                    var_32 = (uint64_t *)(var_0 + (-80L));
                    *var_32 = spec_select207;
                    *(uint64_t *)(local_sp_2 + (-8L)) = 4203035UL;
                    indirect_placeholder();
                    *(uint32_t *)spec_select207 = 0U;
                    var_33 = *var_32;
                    var_34 = local_sp_2 + (-16L);
                    *(uint64_t *)var_34 = 4203058UL;
                    indirect_placeholder();
                    var_35 = (uint64_t *)(var_0 + (-88L));
                    *var_35 = var_33;
                    var_41 = var_33;
                    local_sp_1 = var_34;
                    if ((long)var_33 <= (long)18446744073709551615UL) {
                        var_36 = local_sp_2 + (-24L);
                        *(uint64_t *)var_36 = 4203074UL;
                        indirect_placeholder();
                        var_37 = *(uint32_t *)var_33;
                        local_sp_1 = var_36;
                        if (var_37 != 0U) {
                            var_38 = (uint64_t)var_37;
                            *(uint64_t *)(local_sp_2 + (-32L)) = 4203085UL;
                            indirect_placeholder();
                            var_39 = (uint64_t)*(uint32_t *)var_38;
                            var_40 = *var_32;
                            *(uint64_t *)(local_sp_2 + (-40L)) = 4203116UL;
                            indirect_placeholder_5(0UL, 4268760UL, var_40, 0UL, var_39, r8_0, r9_0);
                            return rax_0;
                        }
                        var_41 = *var_35;
                    }
                    var_42 = (uint64_t *)(var_0 + (-48L));
                    *var_42 = var_41;
                    _pre_phi210 = var_42;
                    local_sp_3 = local_sp_1;
                }
                var_43 = *_pre_phi210;
                var_44 = *var_13;
                local_sp_4 = local_sp_3;
                if (var_43 <= var_44) {
                    var_45 = (uint64_t *)(var_0 + (-96L));
                    *var_45 = var_44;
                    var_46 = *_pre_phi210 + (-1L);
                    var_47 = (uint64_t *)(var_0 + (-104L));
                    *var_47 = var_46;
                    var_48 = *var_7;
                    *(uint64_t *)(local_sp_3 + (-8L)) = 4203184UL;
                    var_49 = indirect_placeholder_12(4UL, var_48);
                    var_50 = var_49.field_0;
                    var_51 = *var_45;
                    var_52 = *var_47;
                    *(uint64_t *)(local_sp_3 + (-16L)) = 4203229UL;
                    indirect_placeholder_5(0UL, 4268816UL, var_52, 0UL, 0UL, var_51, var_50);
                    return rax_0;
                }
                var_53 = *var_9;
            }
        } else {
            if (var_30) {
                var_31 = (uint64_t *)(var_0 + (-48L));
                *var_31 = 256UL;
                _pre_phi210 = var_31;
            } else {
                spec_select207 = (**(unsigned char **)var_6 == '/') ? 4268756UL : 4268758UL;
                var_32 = (uint64_t *)(var_0 + (-80L));
                *var_32 = spec_select207;
                *(uint64_t *)(local_sp_2 + (-8L)) = 4203035UL;
                indirect_placeholder();
                *(uint32_t *)spec_select207 = 0U;
                var_33 = *var_32;
                var_34 = local_sp_2 + (-16L);
                *(uint64_t *)var_34 = 4203058UL;
                indirect_placeholder();
                var_35 = (uint64_t *)(var_0 + (-88L));
                *var_35 = var_33;
                var_41 = var_33;
                local_sp_1 = var_34;
                if ((long)var_33 <= (long)18446744073709551615UL) {
                    var_36 = local_sp_2 + (-24L);
                    *(uint64_t *)var_36 = 4203074UL;
                    indirect_placeholder();
                    var_37 = *(uint32_t *)var_33;
                    local_sp_1 = var_36;
                    if (var_37 != 0U) {
                        var_38 = (uint64_t)var_37;
                        *(uint64_t *)(local_sp_2 + (-32L)) = 4203085UL;
                        indirect_placeholder();
                        var_39 = (uint64_t)*(uint32_t *)var_38;
                        var_40 = *var_32;
                        *(uint64_t *)(local_sp_2 + (-40L)) = 4203116UL;
                        indirect_placeholder_5(0UL, 4268760UL, var_40, 0UL, var_39, r8_0, r9_0);
                        return rax_0;
                    }
                    var_41 = *var_35;
                }
                var_42 = (uint64_t *)(var_0 + (-48L));
                *var_42 = var_41;
                _pre_phi210 = var_42;
                local_sp_3 = local_sp_1;
            }
            var_43 = *_pre_phi210;
            var_44 = *var_13;
            local_sp_4 = local_sp_3;
            if (var_43 <= var_44) {
                var_45 = (uint64_t *)(var_0 + (-96L));
                *var_45 = var_44;
                var_46 = *_pre_phi210 + (-1L);
                var_47 = (uint64_t *)(var_0 + (-104L));
                *var_47 = var_46;
                var_48 = *var_7;
                *(uint64_t *)(local_sp_3 + (-8L)) = 4203184UL;
                var_49 = indirect_placeholder_12(4UL, var_48);
                var_50 = var_49.field_0;
                var_51 = *var_45;
                var_52 = *var_47;
                *(uint64_t *)(local_sp_3 + (-16L)) = 4203229UL;
                indirect_placeholder_5(0UL, 4268816UL, var_52, 0UL, 0UL, var_51, var_50);
                return rax_0;
            }
            var_53 = *var_9;
        }
        var_54 = (unsigned char *)(var_0 + (-33L));
        *var_54 = var_53;
        local_sp_5 = local_sp_4;
        local_sp_6 = local_sp_4;
        var_65 = var_53;
        if (var_53 != '\x01' & *var_14 != '\x01') {
            var_55 = *var_7;
            var_56 = var_0 + (-32L);
            var_57 = (uint64_t *)var_56;
            *var_57 = var_55;
            var_58 = (uint64_t *)(var_0 + (-112L));
            var_59 = var_55;
            var_65 = (unsigned char)'\x01';
            while (1U)
                {
                    var_60 = local_sp_5 + (-8L);
                    *(uint64_t *)var_60 = 4203336UL;
                    var_61 = indirect_placeholder_2(var_59);
                    *var_57 = var_61;
                    local_sp_6 = var_60;
                    if (**(unsigned char **)var_56 != '\x00') {
                        var_65 = *var_54;
                        break;
                    }
                    var_62 = local_sp_5 + (-16L);
                    *(uint64_t *)var_62 = 4203296UL;
                    var_63 = indirect_placeholder_2(var_61);
                    *var_58 = var_63;
                    local_sp_5 = var_62;
                    local_sp_6 = var_62;
                    if (var_63 <= 255UL) {
                        *var_54 = (unsigned char)'\x01';
                        break;
                    }
                    var_64 = *var_57 + var_63;
                    *var_57 = var_64;
                    var_59 = var_64;
                    continue;
                }
        }
        local_sp_7 = local_sp_6;
        if (var_65 != '\x00') {
            var_66 = (uint64_t *)(var_0 + (-56L));
            *var_66 = 255UL;
            spec_select206 = (*var_9 == '\x00') ? 0UL : 14UL;
            var_67 = (uint64_t *)(var_0 + (-64L));
            *var_67 = spec_select206;
            var_68 = *var_7;
            var_69 = var_0 + (-32L);
            var_70 = (uint64_t *)var_69;
            *var_70 = var_68;
            var_71 = (uint64_t *)(var_0 + (-152L));
            var_72 = (unsigned char *)(var_0 + (-153L));
            var_73 = (uint64_t *)(var_0 + (-168L));
            var_74 = (uint64_t *)(var_0 + (-120L));
            var_75 = var_68;
            var_76 = local_sp_7 + (-8L);
            *(uint64_t *)var_76 = 4203873UL;
            var_77 = indirect_placeholder_2(var_75);
            *var_70 = var_77;
            var_78 = (unsigned char **)var_69;
            local_sp_0 = var_76;
            while (**var_78 != '\x00')
                {
                    var_79 = *var_67;
                    rax_0 = 0UL;
                    if (var_79 != 0UL) {
                        *var_66 = var_79;
                        var_88 = *var_70;
                        var_89 = local_sp_0 + (-8L);
                        *(uint64_t *)var_89 = 4203701UL;
                        var_90 = indirect_placeholder_2(var_88);
                        *var_74 = var_90;
                        var_91 = helper_cc_compute_c_wrapper(*var_66 - var_90, var_90, var_2, 17U);
                        var_92 = (var_91 == 0UL);
                        var_93 = *var_74;
                        local_sp_7 = var_89;
                        if (var_92) {
                            var_103 = *var_70 + var_93;
                            *var_70 = var_103;
                            var_75 = var_103;
                            var_76 = local_sp_7 + (-8L);
                            *(uint64_t *)var_76 = 4203873UL;
                            var_77 = indirect_placeholder_2(var_75);
                            *var_70 = var_77;
                            var_78 = (unsigned char **)var_69;
                            local_sp_0 = var_76;
                            continue;
                        }
                        var_94 = (uint64_t *)(var_0 + (-128L));
                        *var_94 = var_93;
                        var_95 = *var_66;
                        var_96 = (uint64_t *)(var_0 + (-136L));
                        *var_96 = var_95;
                        var_97 = *(unsigned char *)(*var_94 + *var_70);
                        var_98 = (unsigned char *)(var_0 + (-137L));
                        *var_98 = var_97;
                        *(unsigned char *)(*var_94 + *var_70) = (unsigned char)'\x00';
                        var_99 = *var_70;
                        *(uint64_t *)(local_sp_0 + (-16L)) = 4203781UL;
                        var_100 = indirect_placeholder_2(var_99);
                        var_101 = *var_94;
                        var_102 = *var_96;
                        *(uint64_t *)(local_sp_0 + (-24L)) = 4203826UL;
                        indirect_placeholder_5(0UL, 4268872UL, var_102, 0UL, 0UL, var_101, var_100);
                        *(unsigned char *)(*var_70 + *var_94) = *var_98;
                        break;
                    }
                    var_80 = *var_7;
                    *var_71 = ((var_77 == var_80) ? 4268758UL : var_80);
                    var_81 = **var_78;
                    var_82 = (uint64_t)var_81;
                    *var_72 = var_81;
                    *(uint64_t *)(local_sp_7 + (-16L)) = 4203482UL;
                    indirect_placeholder();
                    *(uint32_t *)var_82 = 0U;
                    **var_78 = (unsigned char)'\x00';
                    var_83 = *var_71;
                    var_84 = local_sp_7 + (-24L);
                    *(uint64_t *)var_84 = 4203515UL;
                    indirect_placeholder();
                    *var_73 = var_83;
                    var_85 = *var_70;
                    *(unsigned char *)var_85 = *var_72;
                    var_86 = *var_73;
                    local_sp_0 = var_84;
                    if ((long)var_86 < (long)0UL) {
                        switch (*(uint32_t *)var_85) {
                          case 0U:
                            {
                                *var_66 = 18446744073709551615UL;
                            }
                            break;
                          case 2U:
                            {
                                *var_67 = *var_66;
                            }
                            break;
                        }
                    } else {
                        *var_66 = var_86;
                    }
                }
        }
        return rax_0;
    }
    var_23 = *var_7;
    var_24 = local_sp_8 + (-8L);
    *(uint64_t *)var_24 = 4202841UL;
    var_25 = indirect_placeholder_2(var_23);
    local_sp_2 = var_24;
    if ((uint64_t)(uint32_t)var_25 != 0UL) {
        var_26 = local_sp_8 + (-16L);
        *(uint64_t *)var_26 = 4202856UL;
        indirect_placeholder();
        local_sp_2 = var_26;
        if (*(uint32_t *)var_25 != 2U) {
            var_27 = *var_7;
            *(uint64_t *)(local_sp_8 + (-24L)) = 4202895UL;
            var_28 = indirect_placeholder_11(var_27, 0UL, 3UL);
            *(uint64_t *)(local_sp_8 + (-32L)) = 4202903UL;
            indirect_placeholder();
            var_29 = (uint64_t)*(uint32_t *)var_28;
            *(uint64_t *)(local_sp_8 + (-40L)) = 4202930UL;
            indirect_placeholder_5(0UL, 4268753UL, var_28, 0UL, var_29, r8_0, r9_0);
            return rax_0;
        }
        if (*var_13 != 0UL) {
            var_27 = *var_7;
            *(uint64_t *)(local_sp_8 + (-24L)) = 4202895UL;
            var_28 = indirect_placeholder_11(var_27, 0UL, 3UL);
            *(uint64_t *)(local_sp_8 + (-32L)) = 4202903UL;
            indirect_placeholder();
            var_29 = (uint64_t)*(uint32_t *)var_28;
            *(uint64_t *)(local_sp_8 + (-40L)) = 4202930UL;
            indirect_placeholder_5(0UL, 4268753UL, var_28, 0UL, var_29, r8_0, r9_0);
            return rax_0;
        }
    }
    *var_14 = (unsigned char)'\x01';
}

typedef unsigned char __u_char;
typedef unsigned short __u_short;
typedef unsigned int __u_int;
typedef unsigned long __u_long;
typedef signed char __int8_t;
typedef unsigned char __uint8_t;
typedef short __int16_t;
typedef unsigned short __uint16_t;
typedef int __int32_t;
typedef unsigned int __uint32_t;
typedef long __int64_t;
typedef unsigned long __uint64_t;
typedef __int8_t __int_least8_t;
typedef __uint8_t __uint_least8_t;
typedef __int16_t __int_least16_t;
typedef __uint16_t __uint_least16_t;
typedef __int32_t __int_least32_t;
typedef __uint32_t __uint_least32_t;
typedef __int64_t __int_least64_t;
typedef __uint64_t __uint_least64_t;
typedef long __quad_t;
typedef unsigned long __u_quad_t;
typedef long __intmax_t;
typedef unsigned long __uintmax_t;
typedef unsigned long __dev_t;
typedef unsigned int __uid_t;
typedef unsigned int __gid_t;
typedef unsigned long __ino_t;
typedef unsigned long __ino64_t;
typedef unsigned int __mode_t;
typedef unsigned long __nlink_t;
typedef long __off_t;
typedef long __off64_t;
typedef int __pid_t;
typedef struct {
    int __val[2];
} __fsid_t;
typedef long __clock_t;
typedef unsigned long __rlim_t;
typedef unsigned long __rlim64_t;
typedef unsigned int __id_t;
typedef long __time_t;
typedef unsigned int __useconds_t;
typedef long __suseconds_t;
typedef int __daddr_t;
typedef int __key_t;
typedef int __clockid_t;
typedef void *__timer_t;
typedef long __blksize_t;
typedef long __blkcnt_t;
typedef long __blkcnt64_t;
typedef unsigned long __fsblkcnt_t;
typedef unsigned long __fsblkcnt64_t;
typedef unsigned long __fsfilcnt_t;
typedef unsigned long __fsfilcnt64_t;
typedef long __fsword_t;
typedef long __ssize_t;
typedef long __syscall_slong_t;
typedef unsigned long __syscall_ulong_t;
typedef __off64_t __loff_t;
typedef char *__caddr_t;
typedef long __intptr_t;
typedef unsigned int __socklen_t;
typedef int __sig_atomic_t;
typedef __int8_t int8_t;
typedef __int16_t int16_t;
typedef __int32_t int32_t;
typedef __int64_t int64_t;
typedef __uint8_t uint8_t;
typedef __uint16_t uint16_t;
typedef __uint32_t uint32_t;
typedef __uint64_t uint64_t;
typedef __int_least8_t int_least8_t;
typedef __int_least16_t int_least16_t;
typedef __int_least32_t int_least32_t;
typedef __int_least64_t int_least64_t;
typedef __uint_least8_t uint_least8_t;
typedef __uint_least16_t uint_least16_t;
typedef __uint_least32_t uint_least32_t;
typedef __uint_least64_t uint_least64_t;
typedef signed char int_fast8_t;
typedef long int_fast16_t;
typedef long int_fast32_t;
typedef long int_fast64_t;
typedef unsigned char uint_fast8_t;
typedef unsigned long uint_fast16_t;
typedef unsigned long uint_fast32_t;
typedef unsigned long uint_fast64_t;
typedef long intptr_t;
typedef unsigned long uintptr_t;
typedef __intmax_t intmax_t;
typedef __uintmax_t uintmax_t;
typedef _Bool bool;
struct bb_default_sort_size_ret_type;
struct helper_pxor_xmm_wrapper_ret_type;
struct type_6;
struct type_8;
struct helper_ucomisd_wrapper_ret_type;
struct helper_ucomisd_wrapper_230_ret_type;
struct helper_mulsd_wrapper_ret_type;
struct helper_subsd_wrapper_ret_type;
struct helper_mulsd_wrapper_229_ret_type;
struct helper_cvtsq2sd_wrapper_ret_type;
struct helper_addsd_wrapper_ret_type;
struct helper_divsd_wrapper_ret_type;
struct helper_cvttsd2sq_wrapper_ret_type;
struct helper_cvttsd2sq_wrapper_231_ret_type;
struct bb_default_sort_size_ret_type {
    uint64_t field_0;
    uint64_t field_1;
    uint64_t field_2;
};
struct helper_pxor_xmm_wrapper_ret_type {
    uint64_t field_0;
    uint64_t field_1;
};
struct type_6 {
};
struct type_8 {
};
struct helper_ucomisd_wrapper_ret_type {
    uint64_t field_0;
    unsigned char field_1;
};
struct helper_ucomisd_wrapper_230_ret_type {
    uint64_t field_0;
    unsigned char field_1;
};
struct helper_mulsd_wrapper_ret_type {
    uint64_t field_0;
    unsigned char field_1;
};
struct helper_subsd_wrapper_ret_type {
    uint64_t field_0;
    unsigned char field_1;
};
struct helper_mulsd_wrapper_229_ret_type {
    uint64_t field_0;
    unsigned char field_1;
};
struct helper_cvtsq2sd_wrapper_ret_type {
    uint64_t field_0;
    unsigned char field_1;
};
struct helper_addsd_wrapper_ret_type {
    uint64_t field_0;
    unsigned char field_1;
};
struct helper_divsd_wrapper_ret_type {
    uint64_t field_0;
    unsigned char field_1;
};
struct helper_cvttsd2sq_wrapper_ret_type {
    uint64_t field_0;
    unsigned char field_1;
};
struct helper_cvttsd2sq_wrapper_231_ret_type {
    uint64_t field_0;
    unsigned char field_1;
};
extern uint64_t revng_init_local_sp(uint64_t param_0);
extern uint64_t init_rbp(void);
extern uint64_t init_cc_src2(void);
extern uint64_t helper_cc_compute_c_wrapper(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint32_t param_3);
extern uint64_t init_state_0x8558(void);
extern unsigned char init_state_0x8549(void);
extern unsigned char init_state_0x854c(void);
extern unsigned char init_state_0x8548(void);
extern unsigned char init_state_0x854b(void);
extern unsigned char init_state_0x8547(void);
extern unsigned char init_state_0x854d(void);
extern void indirect_placeholder_2(void);
extern struct helper_pxor_xmm_wrapper_ret_type helper_pxor_xmm_wrapper(struct type_6 *param_0, struct type_8 *param_1, struct type_8 *param_2, uint64_t param_3, uint64_t param_4);
extern struct helper_ucomisd_wrapper_ret_type helper_ucomisd_wrapper(struct type_6 *param_0, struct type_8 *param_1, struct type_8 *param_2, uint64_t param_3, uint64_t param_4, unsigned char param_5, unsigned char param_6);
extern struct helper_ucomisd_wrapper_230_ret_type helper_ucomisd_wrapper_230(struct type_6 *param_0, struct type_8 *param_1, struct type_8 *param_2, uint64_t param_3, uint64_t param_4, unsigned char param_5, unsigned char param_6);
extern struct helper_mulsd_wrapper_ret_type helper_mulsd_wrapper(struct type_6 *param_0, struct type_8 *param_1, struct type_8 *param_2, uint64_t param_3, uint64_t param_4, unsigned char param_5, unsigned char param_6, unsigned char param_7, unsigned char param_8, unsigned char param_9, unsigned char param_10);
extern struct helper_subsd_wrapper_ret_type helper_subsd_wrapper(struct type_6 *param_0, struct type_8 *param_1, struct type_8 *param_2, uint64_t param_3, uint64_t param_4, unsigned char param_5, unsigned char param_6, unsigned char param_7, unsigned char param_8, unsigned char param_9, unsigned char param_10);
extern struct helper_mulsd_wrapper_229_ret_type helper_mulsd_wrapper_229(struct type_6 *param_0, struct type_8 *param_1, struct type_8 *param_2, uint64_t param_3, uint64_t param_4, unsigned char param_5, unsigned char param_6, unsigned char param_7, unsigned char param_8, unsigned char param_9, unsigned char param_10);
extern struct helper_cvtsq2sd_wrapper_ret_type helper_cvtsq2sd_wrapper(struct type_6 *param_0, struct type_8 *param_1, uint64_t param_2, unsigned char param_3, unsigned char param_4, unsigned char param_5, unsigned char param_6);
extern struct helper_addsd_wrapper_ret_type helper_addsd_wrapper(struct type_6 *param_0, struct type_8 *param_1, struct type_8 *param_2, uint64_t param_3, unsigned char param_4, unsigned char param_5, unsigned char param_6, unsigned char param_7, unsigned char param_8, unsigned char param_9);
extern struct helper_divsd_wrapper_ret_type helper_divsd_wrapper(struct type_6 *param_0, struct type_8 *param_1, struct type_8 *param_2, uint64_t param_3, uint64_t param_4, unsigned char param_5, unsigned char param_6, unsigned char param_7, unsigned char param_8, unsigned char param_9, unsigned char param_10);
extern struct helper_cvttsd2sq_wrapper_ret_type helper_cvttsd2sq_wrapper(struct type_6 *param_0, struct type_8 *param_1, uint64_t param_2, unsigned char param_3, unsigned char param_4);
extern struct helper_cvttsd2sq_wrapper_231_ret_type helper_cvttsd2sq_wrapper_231(struct type_6 *param_0, struct type_8 *param_1, uint64_t param_2, unsigned char param_3, unsigned char param_4);
struct bb_default_sort_size_ret_type bb_default_sort_size(void) {
    struct helper_cvttsd2sq_wrapper_ret_type var_48;
    struct helper_pxor_xmm_wrapper_ret_type var_30;
    struct helper_pxor_xmm_wrapper_ret_type var_26;
    struct helper_cvttsd2sq_wrapper_ret_type var_43;
    uint64_t var_0;
    uint64_t var_1;
    uint64_t var_2;
    uint64_t var_3;
    unsigned char var_4;
    unsigned char var_5;
    unsigned char var_6;
    unsigned char var_7;
    unsigned char var_8;
    unsigned char var_9;
    uint64_t *var_10;
    struct helper_ucomisd_wrapper_ret_type var_15;
    uint64_t var_11;
    uint64_t *var_12;
    uint64_t *var_13;
    struct helper_divsd_wrapper_ret_type var_14;
    uint64_t var_16;
    unsigned char var_17;
    unsigned char state_0x8549_0;
    struct helper_divsd_wrapper_ret_type var_18;
    uint64_t storemerge;
    uint64_t *var_19;
    uint64_t var_20;
    uint64_t var_21;
    struct helper_mulsd_wrapper_ret_type var_22;
    uint64_t var_23;
    unsigned char var_24;
    uint64_t var_25;
    uint64_t var_27;
    struct helper_cvtsq2sd_wrapper_ret_type var_28;
    uint64_t state_0x8558_0;
    uint64_t var_29;
    uint64_t var_31;
    struct helper_cvtsq2sd_wrapper_ret_type var_32;
    struct helper_addsd_wrapper_ret_type var_33;
    uint64_t state_0x8560_0;
    uint64_t cc_dst_0;
    unsigned char storemerge2;
    struct helper_ucomisd_wrapper_ret_type var_34;
    uint64_t var_35;
    unsigned char var_36;
    uint64_t var_52;
    struct helper_mulsd_wrapper_229_ret_type var_37;
    uint64_t var_38;
    struct helper_ucomisd_wrapper_230_ret_type var_39;
    uint64_t var_40;
    unsigned char var_41;
    uint64_t var_42;
    uint64_t var_44;
    unsigned char var_45;
    uint64_t var_51;
    struct helper_subsd_wrapper_ret_type var_46;
    uint64_t var_47;
    unsigned char var_49;
    uint64_t var_50;
    uint64_t state_0x8558_1;
    unsigned char state_0x8549_1;
    uint64_t state_0x8558_2;
    uint64_t state_0x8560_1;
    unsigned char state_0x8549_2;
    struct helper_cvtsq2sd_wrapper_ret_type var_53;
    uint64_t state_0x8558_3;
    uint64_t var_54;
    struct helper_cvtsq2sd_wrapper_ret_type var_55;
    struct helper_addsd_wrapper_ret_type var_56;
    uint64_t cc_dst_1;
    unsigned char storemerge1;
    struct helper_ucomisd_wrapper_230_ret_type var_57;
    uint64_t var_70;
    struct helper_ucomisd_wrapper_230_ret_type var_58;
    uint64_t var_59;
    unsigned char var_60;
    uint64_t var_61;
    bool var_62;
    uint64_t var_63;
    struct helper_cvttsd2sq_wrapper_231_ret_type var_64;
    uint64_t var_65;
    uint64_t var_69;
    struct helper_subsd_wrapper_ret_type var_66;
    struct helper_cvttsd2sq_wrapper_ret_type var_67;
    uint64_t var_68;
    uint64_t var_71;
    uint64_t var_72;
    struct bb_default_sort_size_ret_type mrv;
    struct bb_default_sort_size_ret_type mrv1;
    struct bb_default_sort_size_ret_type mrv2;
    var_0 = revng_init_local_sp(0UL);
    var_1 = init_rbp();
    var_2 = init_cc_src2();
    var_3 = init_state_0x8558();
    var_4 = init_state_0x8549();
    var_5 = init_state_0x854c();
    var_6 = init_state_0x8548();
    var_7 = init_state_0x854b();
    var_8 = init_state_0x8547();
    var_9 = init_state_0x854d();
    *(uint64_t *)(var_0 + (-8L)) = var_1;
    var_10 = (uint64_t *)(var_0 + (-16L));
    *var_10 = 18446744073709551615UL;
    *(uint64_t *)(var_0 + (-80L)) = 4213162UL;
    indirect_placeholder_2();
    *(uint64_t *)(var_0 + (-88L)) = 4213201UL;
    indirect_placeholder_2();
    *var_10 = (*var_10 >> 1UL);
    var_11 = var_0 + (-56L);
    *(uint64_t *)(var_0 + (-96L)) = 4213251UL;
    indirect_placeholder_2();
    *(uint64_t *)(var_0 + (-104L)) = 4213312UL;
    indirect_placeholder_2();
    var_12 = (uint64_t *)(var_0 + (-24L));
    *var_12 = var_3;
    *(uint64_t *)(var_0 + (-112L)) = 4213326UL;
    indirect_placeholder_2();
    var_13 = (uint64_t *)(var_0 + (-32L));
    *var_13 = var_3;
    var_14 = helper_divsd_wrapper((struct type_6 *)(0UL), (struct type_8 *)(776UL), (struct type_8 *)(840UL), var_3, *(uint64_t *)4350896UL, var_4, var_5, var_6, var_7, var_8, var_9);
    var_15 = helper_ucomisd_wrapper((struct type_6 *)(0UL), (struct type_8 *)(840UL), (struct type_8 *)(776UL), var_14.field_0, *var_12, var_14.field_1, var_5);
    var_16 = var_15.field_0;
    var_17 = var_15.field_1;
    state_0x8549_0 = var_17;
    state_0x8560_1 = 0UL;
    if ((var_16 & 65UL) == 0UL) {
        storemerge = *var_12;
    } else {
        var_18 = helper_divsd_wrapper((struct type_6 *)(0UL), (struct type_8 *)(776UL), (struct type_8 *)(840UL), *var_13, *(uint64_t *)4350896UL, var_17, var_5, var_6, var_7, var_8, var_9);
        state_0x8549_0 = var_18.field_1;
        storemerge = var_18.field_0;
    }
    var_19 = (uint64_t *)(var_0 + (-40L));
    *var_19 = storemerge;
    var_20 = *var_13;
    var_21 = *(uint64_t *)4350904UL;
    var_22 = helper_mulsd_wrapper((struct type_6 *)(0UL), (struct type_8 *)(840UL), (struct type_8 *)(776UL), var_21, var_20, state_0x8549_0, var_5, var_6, var_7, var_8, var_9);
    var_23 = var_22.field_0;
    var_24 = var_22.field_1;
    var_25 = *var_10;
    cc_dst_0 = var_25;
    if ((long)var_25 < (long)0UL) {
        var_29 = (var_25 >> 1UL) | (var_25 & 1UL);
        var_30 = helper_pxor_xmm_wrapper((struct type_6 *)(0UL), (struct type_8 *)(776UL), (struct type_8 *)(776UL), var_21, 0UL);
        var_31 = var_30.field_1;
        var_32 = helper_cvtsq2sd_wrapper((struct type_6 *)(0UL), (struct type_8 *)(776UL), var_29, var_24, var_6, var_7, var_8);
        var_33 = helper_addsd_wrapper((struct type_6 *)(0UL), (struct type_8 *)(776UL), (struct type_8 *)(776UL), var_32.field_0, var_32.field_1, var_5, var_6, var_7, var_8, var_9);
        state_0x8558_0 = var_33.field_0;
        state_0x8560_0 = var_31;
        cc_dst_0 = var_29;
        storemerge2 = var_33.field_1;
    } else {
        var_26 = helper_pxor_xmm_wrapper((struct type_6 *)(0UL), (struct type_8 *)(776UL), (struct type_8 *)(776UL), var_21, 0UL);
        var_27 = var_26.field_1;
        var_28 = helper_cvtsq2sd_wrapper((struct type_6 *)(0UL), (struct type_8 *)(776UL), var_25, var_24, var_6, var_7, var_8);
        state_0x8558_0 = var_28.field_0;
        state_0x8560_0 = var_27;
        storemerge2 = var_28.field_1;
    }
    var_34 = helper_ucomisd_wrapper((struct type_6 *)(0UL), (struct type_8 *)(776UL), (struct type_8 *)(840UL), state_0x8558_0, var_23, storemerge2, var_5);
    var_35 = var_34.field_0;
    var_36 = var_34.field_1;
    state_0x8558_2 = state_0x8558_0;
    state_0x8549_2 = var_36;
    if ((var_35 & 65UL) == 0UL) {
        var_52 = *var_10;
        state_0x8560_1 = state_0x8560_0;
    } else {
        var_37 = helper_mulsd_wrapper_229((struct type_6 *)(0UL), (struct type_8 *)(776UL), (struct type_8 *)(840UL), *(uint64_t *)4350904UL, *var_13, var_36, var_5, var_6, var_7, var_8, var_9);
        var_38 = var_37.field_0;
        var_39 = helper_ucomisd_wrapper_230((struct type_6 *)(0UL), (struct type_8 *)(776UL), (struct type_8 *)(2824UL), var_38, *(uint64_t *)4350888UL, var_37.field_1, var_5);
        var_40 = var_39.field_0;
        var_41 = var_39.field_1;
        var_42 = helper_cc_compute_c_wrapper(cc_dst_0, var_40, var_2, 1U);
        state_0x8558_1 = var_38;
        if (var_42 == 0UL) {
            var_46 = helper_subsd_wrapper((struct type_6 *)(0UL), (struct type_8 *)(776UL), (struct type_8 *)(840UL), var_38, *(uint64_t *)4350888UL, var_41, var_5, var_6, var_7, var_8, var_9);
            var_47 = var_46.field_0;
            var_48 = helper_cvttsd2sq_wrapper((struct type_6 *)(0UL), (struct type_8 *)(776UL), var_47, var_46.field_1, var_5);
            var_49 = var_48.field_1;
            var_50 = var_48.field_0 ^ (-9223372036854775808L);
            *var_10 = var_50;
            var_51 = var_50;
            state_0x8558_1 = var_47;
            state_0x8549_1 = var_49;
        } else {
            var_43 = helper_cvttsd2sq_wrapper((struct type_6 *)(0UL), (struct type_8 *)(776UL), var_38, var_41, var_5);
            var_44 = var_43.field_0;
            var_45 = var_43.field_1;
            *var_10 = var_44;
            var_51 = var_44;
            state_0x8549_1 = var_45;
        }
        var_52 = var_51;
        state_0x8558_2 = state_0x8558_1;
        state_0x8549_2 = state_0x8549_1;
    }
    cc_dst_1 = var_52;
    if ((long)var_52 < (long)0UL) {
        var_54 = (var_52 >> 1UL) | (var_52 & 1UL);
        helper_pxor_xmm_wrapper((struct type_6 *)(0UL), (struct type_8 *)(776UL), (struct type_8 *)(776UL), state_0x8558_2, state_0x8560_1);
        var_55 = helper_cvtsq2sd_wrapper((struct type_6 *)(0UL), (struct type_8 *)(776UL), var_54, state_0x8549_2, var_6, var_7, var_8);
        var_56 = helper_addsd_wrapper((struct type_6 *)(0UL), (struct type_8 *)(776UL), (struct type_8 *)(776UL), var_55.field_0, var_55.field_1, var_5, var_6, var_7, var_8, var_9);
        state_0x8558_3 = var_56.field_0;
        cc_dst_1 = var_54;
        storemerge1 = var_56.field_1;
    } else {
        helper_pxor_xmm_wrapper((struct type_6 *)(0UL), (struct type_8 *)(776UL), (struct type_8 *)(776UL), state_0x8558_2, state_0x8560_1);
        var_53 = helper_cvtsq2sd_wrapper((struct type_6 *)(0UL), (struct type_8 *)(776UL), var_52, state_0x8549_2, var_6, var_7, var_8);
        state_0x8558_3 = var_53.field_0;
        storemerge1 = var_53.field_1;
    }
    var_57 = helper_ucomisd_wrapper_230((struct type_6 *)(0UL), (struct type_8 *)(776UL), (struct type_8 *)(2824UL), state_0x8558_3, *var_19, storemerge1, var_5);
    if ((var_57.field_0 & 65UL) == 0UL) {
        var_70 = *var_10;
    } else {
        var_58 = helper_ucomisd_wrapper_230((struct type_6 *)(0UL), (struct type_8 *)(776UL), (struct type_8 *)(2824UL), *var_19, *(uint64_t *)4350888UL, var_57.field_1, var_5);
        var_59 = var_58.field_0;
        var_60 = var_58.field_1;
        var_61 = helper_cc_compute_c_wrapper(cc_dst_1, var_59, var_2, 1U);
        var_62 = (var_61 == 0UL);
        var_63 = *var_19;
        if (var_62) {
            var_66 = helper_subsd_wrapper((struct type_6 *)(0UL), (struct type_8 *)(776UL), (struct type_8 *)(840UL), var_63, *(uint64_t *)4350888UL, var_60, var_5, var_6, var_7, var_8, var_9);
            var_67 = helper_cvttsd2sq_wrapper((struct type_6 *)(0UL), (struct type_8 *)(776UL), var_66.field_0, var_66.field_1, var_5);
            var_68 = var_67.field_0 ^ (-9223372036854775808L);
            *var_10 = var_68;
            var_69 = var_68;
        } else {
            *(uint64_t *)(var_0 + (-64L)) = var_63;
            var_64 = helper_cvttsd2sq_wrapper_231((struct type_6 *)(0UL), (struct type_8 *)(2824UL), var_63, var_60, var_5);
            var_65 = var_64.field_0;
            *var_10 = var_65;
            var_69 = var_65;
        }
        var_70 = var_69;
    }
    var_71 = (uint64_t)*(uint32_t *)6473484UL * 34UL;
    var_72 = helper_cc_compute_c_wrapper(var_71 - var_70, var_70, var_2, 17U);
    mrv.field_0 = (var_72 == 0UL) ? var_71 : var_70;
    mrv1 = mrv;
    mrv1.field_1 = 5UL;
    mrv2 = mrv1;
    mrv2.field_2 = var_11;
    return mrv2;
}

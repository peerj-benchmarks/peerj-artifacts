typedef unsigned char __u_char;
typedef unsigned short __u_short;
typedef unsigned int __u_int;
typedef unsigned long __u_long;
typedef signed char __int8_t;
typedef unsigned char __uint8_t;
typedef short __int16_t;
typedef unsigned short __uint16_t;
typedef int __int32_t;
typedef unsigned int __uint32_t;
typedef long __int64_t;
typedef unsigned long __uint64_t;
typedef __int8_t __int_least8_t;
typedef __uint8_t __uint_least8_t;
typedef __int16_t __int_least16_t;
typedef __uint16_t __uint_least16_t;
typedef __int32_t __int_least32_t;
typedef __uint32_t __uint_least32_t;
typedef __int64_t __int_least64_t;
typedef __uint64_t __uint_least64_t;
typedef long __quad_t;
typedef unsigned long __u_quad_t;
typedef long __intmax_t;
typedef unsigned long __uintmax_t;
typedef unsigned long __dev_t;
typedef unsigned int __uid_t;
typedef unsigned int __gid_t;
typedef unsigned long __ino_t;
typedef unsigned long __ino64_t;
typedef unsigned int __mode_t;
typedef unsigned long __nlink_t;
typedef long __off_t;
typedef long __off64_t;
typedef int __pid_t;
typedef struct {
    int __val[2];
} __fsid_t;
typedef long __clock_t;
typedef unsigned long __rlim_t;
typedef unsigned long __rlim64_t;
typedef unsigned int __id_t;
typedef long __time_t;
typedef unsigned int __useconds_t;
typedef long __suseconds_t;
typedef int __daddr_t;
typedef int __key_t;
typedef int __clockid_t;
typedef void *__timer_t;
typedef long __blksize_t;
typedef long __blkcnt_t;
typedef long __blkcnt64_t;
typedef unsigned long __fsblkcnt_t;
typedef unsigned long __fsblkcnt64_t;
typedef unsigned long __fsfilcnt_t;
typedef unsigned long __fsfilcnt64_t;
typedef long __fsword_t;
typedef long __ssize_t;
typedef long __syscall_slong_t;
typedef unsigned long __syscall_ulong_t;
typedef __off64_t __loff_t;
typedef char *__caddr_t;
typedef long __intptr_t;
typedef unsigned int __socklen_t;
typedef int __sig_atomic_t;
typedef __int8_t int8_t;
typedef __int16_t int16_t;
typedef __int32_t int32_t;
typedef __int64_t int64_t;
typedef __uint8_t uint8_t;
typedef __uint16_t uint16_t;
typedef __uint32_t uint32_t;
typedef __uint64_t uint64_t;
typedef __int_least8_t int_least8_t;
typedef __int_least16_t int_least16_t;
typedef __int_least32_t int_least32_t;
typedef __int_least64_t int_least64_t;
typedef __uint_least8_t uint_least8_t;
typedef __uint_least16_t uint_least16_t;
typedef __uint_least32_t uint_least32_t;
typedef __uint_least64_t uint_least64_t;
typedef signed char int_fast8_t;
typedef long int_fast16_t;
typedef long int_fast32_t;
typedef long int_fast64_t;
typedef unsigned char uint_fast8_t;
typedef unsigned long uint_fast16_t;
typedef unsigned long uint_fast32_t;
typedef unsigned long uint_fast64_t;
typedef long intptr_t;
typedef unsigned long uintptr_t;
typedef __intmax_t intmax_t;
typedef __uintmax_t uintmax_t;
typedef _Bool bool;
extern uint64_t revng_init_local_sp(uint64_t param_0);
extern uint64_t init_rbp(void);
extern uint64_t indirect_placeholder(uint64_t param_0, uint64_t param_1);
extern uint64_t init_cc_src2(void);
extern uint64_t helper_cc_compute_c_wrapper(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint32_t param_3);
extern void indirect_placeholder_2(void);
extern uint64_t indirect_placeholder_3(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint64_t param_3);
uint64_t bb_compare(uint64_t rdi, uint64_t rsi) {
    uint32_t *_pre_phi44;
    bool var_32;
    uint32_t var_33;
    uint32_t spec_select;
    uint64_t var_18;
    uint64_t var_19;
    uint64_t var_20;
    uint64_t var_21;
    uint32_t *var_22;
    uint64_t var_23;
    uint32_t *var_24;
    uint32_t var_25;
    uint32_t rax_0_in;
    uint64_t var_0;
    uint64_t var_1;
    uint64_t var_2;
    uint64_t var_3;
    uint64_t var_4;
    uint64_t *var_5;
    uint64_t *var_6;
    uint64_t local_sp_0;
    uint64_t var_26;
    uint64_t var_27;
    uint64_t var_28;
    uint32_t storemerge;
    uint64_t var_7;
    uint64_t var_8;
    uint64_t var_9;
    uint32_t *var_10;
    uint32_t var_11;
    uint64_t var_12;
    uint64_t *var_13;
    uint64_t var_14;
    uint64_t var_15;
    uint64_t *var_16;
    uint64_t var_17;
    uint32_t var_30;
    uint32_t *var_31;
    uint32_t *var_29;
    var_0 = revng_init_local_sp(0UL);
    var_1 = init_rbp();
    var_2 = init_cc_src2();
    *(uint64_t *)(var_0 + (-8L)) = var_1;
    var_3 = var_0 + (-56L);
    var_4 = var_0 + (-48L);
    var_5 = (uint64_t *)var_4;
    *var_5 = rdi;
    var_6 = (uint64_t *)var_3;
    *var_6 = rsi;
    storemerge = 4294967295U;
    local_sp_0 = var_3;
    if (*(uint64_t *)6475048UL != 0UL) {
        var_7 = *var_5;
        var_8 = var_0 + (-64L);
        *(uint64_t *)var_8 = 4225432UL;
        var_9 = indirect_placeholder(var_7, rsi);
        var_10 = (uint32_t *)(var_0 + (-12L));
        var_11 = (uint32_t)var_9;
        *var_10 = var_11;
        local_sp_0 = var_8;
        rax_0_in = var_11;
        if (var_11 != 0U) {
            return (uint64_t)rax_0_in;
        }
        if (*(unsigned char *)6475042UL != '\x00') {
            return (uint64_t)rax_0_in;
        }
        if (*(unsigned char *)6475041UL != '\x00') {
            return (uint64_t)rax_0_in;
        }
    }
    var_12 = *(uint64_t *)(*var_5 + 8UL) + (-1L);
    var_13 = (uint64_t *)(var_0 + (-24L));
    *var_13 = var_12;
    var_14 = *(uint64_t *)(*var_6 + 8UL);
    var_15 = var_14 + (-1L);
    var_16 = (uint64_t *)(var_0 + (-32L));
    *var_16 = var_15;
    var_17 = *var_13;
    if (var_17 == 0UL) {
        var_30 = (var_15 != 0UL);
        var_31 = (uint32_t *)(var_0 + (-12L));
        *var_31 = var_30;
        _pre_phi44 = var_31;
    } else {
        if (var_15 == 0UL) {
            var_29 = (uint32_t *)(var_0 + (-12L));
            *var_29 = 1U;
            _pre_phi44 = var_29;
        } else {
            if (*(unsigned char *)6473928UL == '\x00') {
                var_18 = **(uint64_t **)var_3;
                var_19 = var_17 + 1UL;
                var_20 = **(uint64_t **)var_4;
                *(uint64_t *)(local_sp_0 + (-8L)) = 4225599UL;
                var_21 = indirect_placeholder_3(var_18, var_14, var_20, var_19);
                var_22 = (uint32_t *)(var_0 + (-12L));
                *var_22 = (uint32_t)var_21;
                _pre_phi44 = var_22;
            } else {
                var_23 = **(uint64_t **)var_4;
                *(uint64_t *)(local_sp_0 + (-8L)) = 4225645UL;
                indirect_placeholder_2();
                var_24 = (uint32_t *)(var_0 + (-12L));
                var_25 = (uint32_t)var_23;
                *var_24 = var_25;
                _pre_phi44 = var_24;
                if (var_25 != 0U) {
                    var_26 = *var_13;
                    var_27 = *var_16;
                    var_28 = helper_cc_compute_c_wrapper(var_26 - var_27, var_27, var_2, 17U);
                    if (var_28 == 0UL) {
                        storemerge = (*var_13 != *var_16);
                    }
                    *var_24 = storemerge;
                }
            }
        }
    }
    var_32 = (*(unsigned char *)6475040UL == '\x00');
    var_33 = *_pre_phi44;
    spec_select = var_32 ? var_33 : (0U - var_33);
    rax_0_in = spec_select;
    return (uint64_t)rax_0_in;
}

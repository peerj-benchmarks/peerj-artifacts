typedef unsigned char __u_char;
typedef unsigned short __u_short;
typedef unsigned int __u_int;
typedef unsigned long __u_long;
typedef signed char __int8_t;
typedef unsigned char __uint8_t;
typedef short __int16_t;
typedef unsigned short __uint16_t;
typedef int __int32_t;
typedef unsigned int __uint32_t;
typedef long __int64_t;
typedef unsigned long __uint64_t;
typedef __int8_t __int_least8_t;
typedef __uint8_t __uint_least8_t;
typedef __int16_t __int_least16_t;
typedef __uint16_t __uint_least16_t;
typedef __int32_t __int_least32_t;
typedef __uint32_t __uint_least32_t;
typedef __int64_t __int_least64_t;
typedef __uint64_t __uint_least64_t;
typedef long __quad_t;
typedef unsigned long __u_quad_t;
typedef long __intmax_t;
typedef unsigned long __uintmax_t;
typedef unsigned long __dev_t;
typedef unsigned int __uid_t;
typedef unsigned int __gid_t;
typedef unsigned long __ino_t;
typedef unsigned long __ino64_t;
typedef unsigned int __mode_t;
typedef unsigned long __nlink_t;
typedef long __off_t;
typedef long __off64_t;
typedef int __pid_t;
typedef struct {
    int __val[2];
} __fsid_t;
typedef long __clock_t;
typedef unsigned long __rlim_t;
typedef unsigned long __rlim64_t;
typedef unsigned int __id_t;
typedef long __time_t;
typedef unsigned int __useconds_t;
typedef long __suseconds_t;
typedef int __daddr_t;
typedef int __key_t;
typedef int __clockid_t;
typedef void *__timer_t;
typedef long __blksize_t;
typedef long __blkcnt_t;
typedef long __blkcnt64_t;
typedef unsigned long __fsblkcnt_t;
typedef unsigned long __fsblkcnt64_t;
typedef unsigned long __fsfilcnt_t;
typedef unsigned long __fsfilcnt64_t;
typedef long __fsword_t;
typedef long __ssize_t;
typedef long __syscall_slong_t;
typedef unsigned long __syscall_ulong_t;
typedef __off64_t __loff_t;
typedef char *__caddr_t;
typedef long __intptr_t;
typedef unsigned int __socklen_t;
typedef int __sig_atomic_t;
typedef __int8_t int8_t;
typedef __int16_t int16_t;
typedef __int32_t int32_t;
typedef __int64_t int64_t;
typedef __uint8_t uint8_t;
typedef __uint16_t uint16_t;
typedef __uint32_t uint32_t;
typedef __uint64_t uint64_t;
typedef __int_least8_t int_least8_t;
typedef __int_least16_t int_least16_t;
typedef __int_least32_t int_least32_t;
typedef __int_least64_t int_least64_t;
typedef __uint_least8_t uint_least8_t;
typedef __uint_least16_t uint_least16_t;
typedef __uint_least32_t uint_least32_t;
typedef __uint_least64_t uint_least64_t;
typedef signed char int_fast8_t;
typedef long int_fast16_t;
typedef long int_fast32_t;
typedef long int_fast64_t;
typedef unsigned char uint_fast8_t;
typedef unsigned long uint_fast16_t;
typedef unsigned long uint_fast32_t;
typedef unsigned long uint_fast64_t;
typedef long intptr_t;
typedef unsigned long uintptr_t;
typedef __intmax_t intmax_t;
typedef __uintmax_t uintmax_t;
struct bb_stream_open_ret_type;
struct indirect_placeholder_167_ret_type;
struct indirect_placeholder_166_ret_type;
struct bb_stream_open_ret_type {
    uint64_t field_0;
    uint64_t field_1;
};
struct indirect_placeholder_167_ret_type {
    uint64_t field_0;
    uint64_t field_1;
    uint64_t field_2;
};
struct indirect_placeholder_166_ret_type {
    uint64_t field_0;
    uint64_t field_1;
    uint64_t field_2;
    uint64_t field_3;
};
extern uint64_t revng_init_local_sp(uint64_t param_0);
extern uint64_t init_rbp(void);
extern void indirect_placeholder_2(void);
extern uint64_t init_rbx(void);
extern uint64_t init_r10(void);
extern void indirect_placeholder_5(uint64_t param_0, uint64_t param_1);
extern struct indirect_placeholder_167_ret_type indirect_placeholder_167(uint64_t param_0, uint64_t param_1, uint64_t param_2);
extern struct indirect_placeholder_166_ret_type indirect_placeholder_166(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint64_t param_3, uint64_t param_4, uint64_t param_5, uint64_t param_6);
typedef _Bool bool;
struct bb_stream_open_ret_type bb_stream_open(uint64_t rdi, uint64_t rsi) {
    uint64_t var_0;
    uint64_t var_1;
    uint64_t var_2;
    uint64_t var_3;
    uint64_t *var_4;
    uint64_t var_5;
    uint64_t var_6;
    struct indirect_placeholder_167_ret_type var_7;
    uint64_t var_8;
    uint64_t var_9;
    uint64_t var_10;
    uint64_t var_11;
    struct indirect_placeholder_166_ret_type var_12;
    uint64_t r10_0;
    uint64_t var_19;
    uint64_t *var_20;
    uint64_t *_pre_phi42;
    uint64_t local_sp_1;
    uint64_t var_21;
    uint64_t var_17;
    uint64_t *var_18;
    uint64_t var_15;
    uint64_t var_16;
    uint64_t *_pre_phi40;
    uint64_t r10_1;
    uint64_t var_13;
    uint64_t *var_14;
    uint64_t var_22;
    struct bb_stream_open_ret_type mrv;
    struct bb_stream_open_ret_type mrv1;
    var_0 = revng_init_local_sp(0UL);
    var_1 = init_r10();
    var_2 = init_rbx();
    var_3 = init_rbp();
    *(uint64_t *)(var_0 + (-8L)) = var_3;
    *(uint64_t *)(var_0 + (-16L)) = var_2;
    var_4 = (uint64_t *)(var_0 + (-48L));
    *var_4 = rdi;
    var_5 = var_0 + (-56L);
    *(uint64_t *)var_5 = rsi;
    r10_0 = var_1;
    r10_1 = var_1;
    switch (**(unsigned char **)var_5) {
      case 'r':
        {
            var_15 = *var_4;
            var_16 = var_0 + (-64L);
            *(uint64_t *)var_16 = 4209318UL;
            indirect_placeholder_2();
            local_sp_1 = var_16;
            if ((uint64_t)(uint32_t)var_15 == 0UL) {
                *(unsigned char *)6475043UL = (unsigned char)'\x01';
                var_19 = *(uint64_t *)6473736UL;
                var_20 = (uint64_t *)(var_0 + (-32L));
                *var_20 = var_19;
                _pre_phi42 = var_20;
            } else {
                *(uint64_t *)(var_0 + (-72L)) = 4209364UL;
                indirect_placeholder_2();
                *(uint32_t *)(var_0 + (-36L)) = 0U;
                var_17 = var_0 + (-80L);
                *(uint64_t *)var_17 = 4209390UL;
                indirect_placeholder_2();
                var_18 = (uint64_t *)(var_0 + (-32L));
                *var_18 = 0UL;
                _pre_phi42 = var_18;
                local_sp_1 = var_17;
            }
            var_21 = *_pre_phi42;
            *(uint64_t *)(local_sp_1 + (-8L)) = 4209418UL;
            indirect_placeholder_5(var_21, 2UL);
            _pre_phi40 = _pre_phi42;
        }
        break;
      case 'w':
        {
            if (*var_4 != 0UL) {
                *(uint64_t *)(var_0 + (-64L)) = 4209456UL;
                indirect_placeholder_2();
                r10_0 = (uint64_t)0L;
                if (0) {
                } else {
                    var_6 = *var_4;
                    *(uint64_t *)(var_0 + (-72L)) = 4209482UL;
                    var_7 = indirect_placeholder_167(var_6, 0UL, 3UL);
                    var_8 = var_7.field_0;
                    var_9 = var_7.field_1;
                    var_10 = var_7.field_2;
                    *(uint64_t *)(var_0 + (-80L)) = 4209490UL;
                    indirect_placeholder_2();
                    var_11 = (uint64_t)*(uint32_t *)var_9;
                    *(uint64_t *)(var_0 + (-88L)) = 4209517UL;
                    var_12 = indirect_placeholder_166(var_8, 0UL, 4346101UL, var_9, var_10, 2UL, var_11);
                    r10_0 = var_12.field_0;
                }
            }
            var_13 = *(uint64_t *)6473728UL;
            var_14 = (uint64_t *)(var_0 + (-32L));
            *var_14 = var_13;
            _pre_phi40 = var_14;
            r10_1 = r10_0;
        }
        break;
      default:
        {
            *(uint64_t *)(var_0 + (-64L)) = 4209555UL;
            indirect_placeholder_2();
            _pre_phi40 = (uint64_t *)(var_0 + (-32L));
        }
        break;
    }
    var_22 = *_pre_phi40;
    mrv.field_0 = r10_1;
    mrv1 = mrv;
    mrv1.field_1 = var_22;
    return mrv1;
}

typedef unsigned char __u_char;
typedef unsigned short __u_short;
typedef unsigned int __u_int;
typedef unsigned long __u_long;
typedef signed char __int8_t;
typedef unsigned char __uint8_t;
typedef short __int16_t;
typedef unsigned short __uint16_t;
typedef int __int32_t;
typedef unsigned int __uint32_t;
typedef long __int64_t;
typedef unsigned long __uint64_t;
typedef __int8_t __int_least8_t;
typedef __uint8_t __uint_least8_t;
typedef __int16_t __int_least16_t;
typedef __uint16_t __uint_least16_t;
typedef __int32_t __int_least32_t;
typedef __uint32_t __uint_least32_t;
typedef __int64_t __int_least64_t;
typedef __uint64_t __uint_least64_t;
typedef long __quad_t;
typedef unsigned long __u_quad_t;
typedef long __intmax_t;
typedef unsigned long __uintmax_t;
typedef unsigned long __dev_t;
typedef unsigned int __uid_t;
typedef unsigned int __gid_t;
typedef unsigned long __ino_t;
typedef unsigned long __ino64_t;
typedef unsigned int __mode_t;
typedef unsigned long __nlink_t;
typedef long __off_t;
typedef long __off64_t;
typedef int __pid_t;
typedef struct {
    int __val[2];
} __fsid_t;
typedef long __clock_t;
typedef unsigned long __rlim_t;
typedef unsigned long __rlim64_t;
typedef unsigned int __id_t;
typedef long __time_t;
typedef unsigned int __useconds_t;
typedef long __suseconds_t;
typedef int __daddr_t;
typedef int __key_t;
typedef int __clockid_t;
typedef void *__timer_t;
typedef long __blksize_t;
typedef long __blkcnt_t;
typedef long __blkcnt64_t;
typedef unsigned long __fsblkcnt_t;
typedef unsigned long __fsblkcnt64_t;
typedef unsigned long __fsfilcnt_t;
typedef unsigned long __fsfilcnt64_t;
typedef long __fsword_t;
typedef long __ssize_t;
typedef long __syscall_slong_t;
typedef unsigned long __syscall_ulong_t;
typedef __off64_t __loff_t;
typedef char *__caddr_t;
typedef long __intptr_t;
typedef unsigned int __socklen_t;
typedef int __sig_atomic_t;
typedef __int8_t int8_t;
typedef __int16_t int16_t;
typedef __int32_t int32_t;
typedef __int64_t int64_t;
typedef __uint8_t uint8_t;
typedef __uint16_t uint16_t;
typedef __uint32_t uint32_t;
typedef __uint64_t uint64_t;
typedef __int_least8_t int_least8_t;
typedef __int_least16_t int_least16_t;
typedef __int_least32_t int_least32_t;
typedef __int_least64_t int_least64_t;
typedef __uint_least8_t uint_least8_t;
typedef __uint_least16_t uint_least16_t;
typedef __uint_least32_t uint_least32_t;
typedef __uint_least64_t uint_least64_t;
typedef signed char int_fast8_t;
typedef long int_fast16_t;
typedef long int_fast32_t;
typedef long int_fast64_t;
typedef unsigned char uint_fast8_t;
typedef unsigned long uint_fast16_t;
typedef unsigned long uint_fast32_t;
typedef unsigned long uint_fast64_t;
typedef long intptr_t;
typedef unsigned long uintptr_t;
typedef __intmax_t intmax_t;
typedef __uintmax_t uintmax_t;
extern uint64_t revng_init_local_sp(uint64_t param_0);
extern uint64_t init_rbp(void);
extern uint64_t init_cc_src2(void);
extern uint64_t helper_cc_compute_c_wrapper(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint32_t param_3);
uint64_t bb_traverse_raw_number(uint64_t rdi) {
    uint64_t var_0;
    uint64_t var_1;
    uint64_t var_2;
    uint64_t var_3;
    uint64_t **var_4;
    uint64_t var_5;
    uint64_t var_6;
    uint64_t *var_7;
    unsigned char *var_8;
    unsigned char *var_9;
    unsigned char *var_10;
    uint64_t var_11;
    unsigned char var_12;
    uint64_t var_13;
    uint32_t var_14;
    uint64_t var_17;
    unsigned char var_18;
    uint64_t var_19;
    uint64_t var_20;
    uint64_t var_15;
    unsigned char var_16;
    var_0 = revng_init_local_sp(0UL);
    var_1 = init_rbp();
    var_2 = init_cc_src2();
    *(uint64_t *)(var_0 + (-8L)) = var_1;
    var_3 = var_0 + (-32L);
    *(uint64_t *)var_3 = rdi;
    var_4 = (uint64_t **)var_3;
    var_5 = **var_4;
    var_6 = var_0 + (-16L);
    var_7 = (uint64_t *)var_6;
    *var_7 = var_5;
    var_8 = (unsigned char *)(var_0 + (-17L));
    *var_8 = (unsigned char)'\x00';
    var_9 = (unsigned char *)(var_0 + (-18L));
    *var_9 = (unsigned char)'\x00';
    var_10 = (unsigned char *)(var_0 + (-19L));
    var_11 = *var_7;
    *var_7 = (var_11 + 1UL);
    var_12 = *(unsigned char *)var_11;
    *var_10 = var_12;
    var_13 = (uint64_t)var_12;
    var_14 = (uint32_t)var_13;
    while ((uint64_t)((var_14 + (-48)) & (-2)) <= 9UL)
        {
            var_15 = helper_cc_compute_c_wrapper((uint64_t)*var_8 - var_13, var_13, var_2, 14U);
            if (var_15 == 0UL) {
                *var_8 = *var_10;
            }
            var_16 = ((uint64_t)((uint32_t)(uint64_t)**(unsigned char **)var_6 - *(uint32_t *)6473924UL) == 0UL);
            *var_9 = var_16;
            if (var_16 == '\x00') {
                *var_7 = (*var_7 + 1UL);
            }
            var_11 = *var_7;
            *var_7 = (var_11 + 1UL);
            var_12 = *(unsigned char *)var_11;
            *var_10 = var_12;
            var_13 = (uint64_t)var_12;
            var_14 = (uint32_t)var_13;
        }
    if (*var_9 == '\x00') {
        **var_4 = (*var_7 + (-2L));
    } else {
        if ((uint64_t)(var_14 - *(uint32_t *)6473920UL) != 0UL) {
            var_17 = *var_7;
            *var_7 = (var_17 + 1UL);
            var_18 = *(unsigned char *)var_17;
            *var_10 = var_18;
            var_19 = (uint64_t)var_18;
            while ((uint64_t)(((uint32_t)var_19 + (-48)) & (-2)) <= 9UL)
                {
                    var_20 = helper_cc_compute_c_wrapper((uint64_t)*var_8 - var_19, var_19, var_2, 14U);
                    if (var_20 == 0UL) {
                        *var_8 = *var_10;
                    }
                    var_17 = *var_7;
                    *var_7 = (var_17 + 1UL);
                    var_18 = *(unsigned char *)var_17;
                    *var_10 = var_18;
                    var_19 = (uint64_t)var_18;
                }
        }
        **var_4 = (*var_7 + (-1L));
    }
    return (uint64_t)*var_8;
}

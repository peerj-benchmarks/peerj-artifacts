typedef unsigned char __u_char;
typedef unsigned short __u_short;
typedef unsigned int __u_int;
typedef unsigned long __u_long;
typedef signed char __int8_t;
typedef unsigned char __uint8_t;
typedef short __int16_t;
typedef unsigned short __uint16_t;
typedef int __int32_t;
typedef unsigned int __uint32_t;
typedef long __int64_t;
typedef unsigned long __uint64_t;
typedef __int8_t __int_least8_t;
typedef __uint8_t __uint_least8_t;
typedef __int16_t __int_least16_t;
typedef __uint16_t __uint_least16_t;
typedef __int32_t __int_least32_t;
typedef __uint32_t __uint_least32_t;
typedef __int64_t __int_least64_t;
typedef __uint64_t __uint_least64_t;
typedef long __quad_t;
typedef unsigned long __u_quad_t;
typedef long __intmax_t;
typedef unsigned long __uintmax_t;
typedef unsigned long __dev_t;
typedef unsigned int __uid_t;
typedef unsigned int __gid_t;
typedef unsigned long __ino_t;
typedef unsigned long __ino64_t;
typedef unsigned int __mode_t;
typedef unsigned long __nlink_t;
typedef long __off_t;
typedef long __off64_t;
typedef int __pid_t;
typedef struct {
    int __val[2];
} __fsid_t;
typedef long __clock_t;
typedef unsigned long __rlim_t;
typedef unsigned long __rlim64_t;
typedef unsigned int __id_t;
typedef long __time_t;
typedef unsigned int __useconds_t;
typedef long __suseconds_t;
typedef int __daddr_t;
typedef int __key_t;
typedef int __clockid_t;
typedef void *__timer_t;
typedef long __blksize_t;
typedef long __blkcnt_t;
typedef long __blkcnt64_t;
typedef unsigned long __fsblkcnt_t;
typedef unsigned long __fsblkcnt64_t;
typedef unsigned long __fsfilcnt_t;
typedef unsigned long __fsfilcnt64_t;
typedef long __fsword_t;
typedef long __ssize_t;
typedef long __syscall_slong_t;
typedef unsigned long __syscall_ulong_t;
typedef __off64_t __loff_t;
typedef char *__caddr_t;
typedef long __intptr_t;
typedef unsigned int __socklen_t;
typedef int __sig_atomic_t;
typedef __int8_t int8_t;
typedef __int16_t int16_t;
typedef __int32_t int32_t;
typedef __int64_t int64_t;
typedef __uint8_t uint8_t;
typedef __uint16_t uint16_t;
typedef __uint32_t uint32_t;
typedef __uint64_t uint64_t;
typedef __int_least8_t int_least8_t;
typedef __int_least16_t int_least16_t;
typedef __int_least32_t int_least32_t;
typedef __int_least64_t int_least64_t;
typedef __uint_least8_t uint_least8_t;
typedef __uint_least16_t uint_least16_t;
typedef __uint_least32_t uint_least32_t;
typedef __uint_least64_t uint_least64_t;
typedef signed char int_fast8_t;
typedef long int_fast16_t;
typedef long int_fast32_t;
typedef long int_fast64_t;
typedef unsigned char uint_fast8_t;
typedef unsigned long uint_fast16_t;
typedef unsigned long uint_fast32_t;
typedef unsigned long uint_fast64_t;
typedef long intptr_t;
typedef unsigned long uintptr_t;
typedef __intmax_t intmax_t;
typedef __uintmax_t uintmax_t;
typedef _Bool bool;
struct helper_fldt_ST0_wrapper_ret_type;
struct type_6;
struct helper_fpop_wrapper_ret_type;
struct helper_fmov_FT0_STN_wrapper_ret_type;
struct helper_fucomi_ST0_FT0_wrapper_ret_type;
struct helper_fmov_STN_ST0_wrapper_ret_type;
struct helper_fxchg_ST0_STN_wrapper_ret_type;
struct helper_fldt_ST0_wrapper_ret_type {
    uint32_t field_0;
    uint64_t field_1;
    uint64_t field_2;
    uint64_t field_3;
    uint64_t field_4;
    uint64_t field_5;
    uint64_t field_6;
    uint64_t field_7;
    uint64_t field_8;
    uint16_t field_9;
    uint16_t field_10;
    uint16_t field_11;
    uint16_t field_12;
    uint16_t field_13;
    uint16_t field_14;
    uint16_t field_15;
    uint16_t field_16;
    unsigned char field_17;
    unsigned char field_18;
    unsigned char field_19;
    unsigned char field_20;
    unsigned char field_21;
    unsigned char field_22;
    unsigned char field_23;
    unsigned char field_24;
};
struct type_6 {
};
struct helper_fpop_wrapper_ret_type {
    uint32_t field_0;
    unsigned char field_1;
    unsigned char field_2;
    unsigned char field_3;
    unsigned char field_4;
    unsigned char field_5;
    unsigned char field_6;
    unsigned char field_7;
    unsigned char field_8;
};
struct helper_fmov_FT0_STN_wrapper_ret_type {
    uint64_t field_0;
    uint16_t field_1;
};
struct helper_fucomi_ST0_FT0_wrapper_ret_type {
    uint64_t field_0;
    unsigned char field_1;
};
struct helper_fmov_STN_ST0_wrapper_ret_type {
    uint64_t field_0;
    uint64_t field_1;
    uint64_t field_2;
    uint64_t field_3;
    uint64_t field_4;
    uint64_t field_5;
    uint64_t field_6;
    uint64_t field_7;
    uint16_t field_8;
    uint16_t field_9;
    uint16_t field_10;
    uint16_t field_11;
    uint16_t field_12;
    uint16_t field_13;
    uint16_t field_14;
    uint16_t field_15;
};
struct helper_fxchg_ST0_STN_wrapper_ret_type {
    uint64_t field_0;
    uint64_t field_1;
    uint64_t field_2;
    uint64_t field_3;
    uint64_t field_4;
    uint64_t field_5;
    uint64_t field_6;
    uint64_t field_7;
    uint16_t field_8;
    uint16_t field_9;
    uint16_t field_10;
    uint16_t field_11;
    uint16_t field_12;
    uint16_t field_13;
    uint16_t field_14;
    uint16_t field_15;
};
extern uint64_t revng_init_local_sp(uint64_t param_0);
extern uint64_t init_rbp(void);
extern uint64_t init_cc_src2(void);
extern void indirect_placeholder_2(void);
extern struct helper_fldt_ST0_wrapper_ret_type helper_fldt_ST0_wrapper(struct type_6 *param_0, uint64_t param_1, uint32_t param_2);
extern unsigned char init_state_0x852a(void);
extern uint32_t init_state_0x8480(void);
extern uint64_t init_state_0x8490(void);
extern uint64_t init_state_0x84a0(void);
extern uint16_t init_state_0x84b8(void);
extern uint64_t init_state_0x84b0(void);
extern uint64_t init_state_0x84c0(void);
extern uint64_t init_state_0x84d0(void);
extern uint64_t init_state_0x84e0(void);
extern uint64_t init_state_0x84f0(void);
extern uint64_t init_state_0x8500(void);
extern uint16_t init_state_0x8498(void);
extern uint16_t init_state_0x84a8(void);
extern uint16_t init_state_0x84c8(void);
extern uint16_t init_state_0x84d8(void);
extern uint16_t init_state_0x84e8(void);
extern uint16_t init_state_0x84f8(void);
extern uint16_t init_state_0x8508(void);
extern void helper_fstt_ST0_wrapper(struct type_6 *param_0, uint64_t param_1, uint32_t param_2, uint64_t param_3, uint64_t param_4, uint64_t param_5, uint64_t param_6, uint64_t param_7, uint64_t param_8, uint64_t param_9, uint64_t param_10, uint16_t param_11, uint16_t param_12, uint16_t param_13, uint16_t param_14, uint16_t param_15, uint16_t param_16, uint16_t param_17, uint16_t param_18);
extern struct helper_fpop_wrapper_ret_type helper_fpop_wrapper(struct type_6 *param_0, uint32_t param_1);
extern struct helper_fmov_FT0_STN_wrapper_ret_type helper_fmov_FT0_STN_wrapper(struct type_6 *param_0, uint32_t param_1, uint32_t param_2, uint64_t param_3, uint64_t param_4, uint64_t param_5, uint64_t param_6, uint64_t param_7, uint64_t param_8, uint64_t param_9, uint64_t param_10, uint16_t param_11, uint16_t param_12, uint16_t param_13, uint16_t param_14, uint16_t param_15, uint16_t param_16, uint16_t param_17, uint16_t param_18);
extern struct helper_fucomi_ST0_FT0_wrapper_ret_type helper_fucomi_ST0_FT0_wrapper(struct type_6 *param_0, uint64_t param_1, uint64_t param_2, uint32_t param_3, uint64_t param_4, unsigned char param_5, uint32_t param_6, uint64_t param_7, uint64_t param_8, uint64_t param_9, uint64_t param_10, uint64_t param_11, uint64_t param_12, uint64_t param_13, uint64_t param_14, uint16_t param_15, uint16_t param_16, uint16_t param_17, uint16_t param_18, uint16_t param_19, uint16_t param_20, uint16_t param_21, uint16_t param_22, uint64_t param_23, uint16_t param_24);
extern struct helper_fmov_STN_ST0_wrapper_ret_type helper_fmov_STN_ST0_wrapper(struct type_6 *param_0, uint32_t param_1, uint32_t param_2, uint64_t param_3, uint64_t param_4, uint64_t param_5, uint64_t param_6, uint64_t param_7, uint64_t param_8, uint64_t param_9, uint64_t param_10, uint16_t param_11, uint16_t param_12, uint16_t param_13, uint16_t param_14, uint16_t param_15, uint16_t param_16, uint16_t param_17, uint16_t param_18);
extern struct helper_fxchg_ST0_STN_wrapper_ret_type helper_fxchg_ST0_STN_wrapper(struct type_6 *param_0, uint32_t param_1, uint32_t param_2, uint64_t param_3, uint64_t param_4, uint64_t param_5, uint64_t param_6, uint64_t param_7, uint64_t param_8, uint64_t param_9, uint64_t param_10, uint16_t param_11, uint16_t param_12, uint16_t param_13, uint16_t param_14, uint16_t param_15, uint16_t param_16, uint16_t param_17, uint16_t param_18);
extern void indirect_placeholder_5(uint64_t param_0, uint64_t param_1);
uint64_t bb_general_numcompare(uint64_t rdi, uint64_t rsi) {
    unsigned char state_0x852a_0;
    uint64_t var_0;
    uint64_t var_1;
    uint64_t var_2;
    unsigned char var_3;
    uint32_t var_4;
    uint64_t var_5;
    uint64_t var_6;
    uint64_t var_7;
    uint64_t var_8;
    uint64_t var_9;
    uint64_t var_10;
    uint64_t var_11;
    uint64_t var_12;
    uint16_t var_13;
    uint16_t var_14;
    uint16_t var_15;
    uint16_t var_16;
    uint16_t var_17;
    uint16_t var_18;
    uint16_t var_19;
    uint16_t var_20;
    uint64_t *var_21;
    uint64_t *var_22;
    uint64_t var_23;
    struct helper_fpop_wrapper_ret_type var_24;
    uint64_t *var_26;
    uint32_t var_25;
    uint64_t var_27;
    uint32_t *var_28;
    uint32_t var_29;
    uint64_t var_30;
    struct helper_fpop_wrapper_ret_type var_31;
    uint64_t var_33;
    uint32_t var_32;
    uint32_t var_34;
    uint64_t var_35;
    bool var_36;
    uint64_t var_37;
    uint64_t var_38;
    uint64_t rax_0;
    uint64_t var_39;
    struct helper_fldt_ST0_wrapper_ret_type var_40;
    struct helper_fldt_ST0_wrapper_ret_type var_41;
    uint32_t var_42;
    uint64_t var_44;
    uint64_t var_45;
    uint64_t var_46;
    uint64_t var_47;
    struct helper_fxchg_ST0_STN_wrapper_ret_type var_43;
    uint64_t var_48;
    uint64_t var_49;
    uint64_t var_50;
    uint64_t var_51;
    uint16_t var_52;
    uint16_t var_53;
    uint16_t var_54;
    uint16_t var_55;
    uint16_t var_56;
    uint16_t var_57;
    uint16_t var_58;
    uint16_t var_59;
    struct helper_fmov_FT0_STN_wrapper_ret_type var_60;
    uint64_t var_62;
    struct helper_fpop_wrapper_ret_type var_63;
    struct helper_fucomi_ST0_FT0_wrapper_ret_type var_61;
    uint32_t var_64;
    struct helper_fpop_wrapper_ret_type var_65;
    unsigned char var_66;
    struct helper_fldt_ST0_wrapper_ret_type var_67;
    struct helper_fldt_ST0_wrapper_ret_type var_68;
    uint32_t var_69;
    uint32_t var_173;
    struct helper_fpop_wrapper_ret_type var_172;
    uint64_t var_71;
    uint64_t var_72;
    uint64_t var_73;
    uint64_t var_74;
    struct helper_fxchg_ST0_STN_wrapper_ret_type var_70;
    uint64_t var_75;
    uint64_t var_76;
    uint64_t var_77;
    uint64_t var_78;
    uint16_t var_79;
    uint16_t var_80;
    uint16_t var_81;
    uint16_t var_82;
    uint16_t var_83;
    uint16_t var_84;
    uint16_t var_85;
    uint16_t var_86;
    struct helper_fmov_FT0_STN_wrapper_ret_type var_87;
    uint64_t var_89;
    struct helper_fpop_wrapper_ret_type var_90;
    struct helper_fucomi_ST0_FT0_wrapper_ret_type var_88;
    uint32_t var_91;
    struct helper_fpop_wrapper_ret_type var_92;
    unsigned char var_93;
    struct helper_fldt_ST0_wrapper_ret_type var_94;
    struct helper_fldt_ST0_wrapper_ret_type var_95;
    uint32_t var_96;
    uint64_t var_97;
    uint64_t var_98;
    uint64_t var_99;
    uint64_t var_100;
    uint64_t var_101;
    uint64_t var_102;
    uint64_t var_103;
    uint64_t var_104;
    uint16_t var_105;
    uint16_t var_106;
    uint16_t var_107;
    uint16_t var_108;
    uint16_t var_109;
    uint16_t var_110;
    uint16_t var_111;
    uint16_t var_112;
    struct helper_fmov_FT0_STN_wrapper_ret_type var_113;
    uint64_t var_115;
    unsigned char var_116;
    struct helper_fpop_wrapper_ret_type var_117;
    struct helper_fucomi_ST0_FT0_wrapper_ret_type var_114;
    uint32_t var_118;
    struct helper_fpop_wrapper_ret_type var_119;
    uint32_t var_120;
    uint64_t cc_src_0;
    struct helper_fldt_ST0_wrapper_ret_type var_121;
    struct helper_fldt_ST0_wrapper_ret_type var_122;
    uint32_t var_123;
    uint64_t var_124;
    uint64_t var_125;
    uint64_t var_126;
    uint64_t var_127;
    uint64_t var_128;
    uint64_t var_129;
    uint64_t var_130;
    uint64_t var_131;
    uint16_t var_132;
    uint16_t var_133;
    uint16_t var_134;
    uint16_t var_135;
    uint16_t var_136;
    uint16_t var_137;
    uint16_t var_138;
    uint16_t var_139;
    struct helper_fmov_FT0_STN_wrapper_ret_type var_140;
    uint64_t var_142;
    unsigned char var_143;
    struct helper_fpop_wrapper_ret_type var_144;
    struct helper_fucomi_ST0_FT0_wrapper_ret_type var_141;
    uint32_t var_145;
    struct helper_fpop_wrapper_ret_type var_146;
    uint32_t state_0x8480_0;
    struct helper_fldt_ST0_wrapper_ret_type var_147;
    struct helper_fldt_ST0_wrapper_ret_type var_148;
    uint32_t var_149;
    uint64_t var_150;
    uint64_t var_151;
    uint64_t var_152;
    uint64_t var_153;
    uint64_t var_154;
    uint64_t var_155;
    uint64_t var_156;
    uint64_t var_157;
    uint16_t var_158;
    uint16_t var_159;
    uint16_t var_160;
    uint16_t var_161;
    uint16_t var_162;
    uint16_t var_163;
    uint16_t var_164;
    uint16_t var_165;
    struct helper_fmov_FT0_STN_wrapper_ret_type var_166;
    uint64_t var_168;
    unsigned char var_169;
    struct helper_fpop_wrapper_ret_type var_170;
    struct helper_fucomi_ST0_FT0_wrapper_ret_type var_167;
    uint32_t var_171;
    uint64_t cc_src_1;
    struct helper_fldt_ST0_wrapper_ret_type var_174;
    struct helper_fldt_ST0_wrapper_ret_type var_175;
    uint32_t var_176;
    uint64_t var_177;
    uint64_t var_178;
    uint64_t var_179;
    uint64_t var_180;
    uint64_t var_181;
    uint64_t var_182;
    uint64_t var_183;
    uint64_t var_184;
    uint16_t var_185;
    uint16_t var_186;
    uint16_t var_187;
    uint16_t var_188;
    uint16_t var_189;
    uint16_t var_190;
    uint16_t var_191;
    uint16_t var_192;
    struct helper_fmov_FT0_STN_wrapper_ret_type var_193;
    uint64_t var_195;
    unsigned char var_196;
    struct helper_fpop_wrapper_ret_type var_197;
    struct helper_fucomi_ST0_FT0_wrapper_ret_type var_194;
    uint32_t var_198;
    struct helper_fpop_wrapper_ret_type var_199;
    unsigned char state_0x852a_1;
    uint32_t state_0x8480_1;
    struct helper_fldt_ST0_wrapper_ret_type var_200;
    struct helper_fldt_ST0_wrapper_ret_type var_201;
    uint32_t var_202;
    uint64_t var_203;
    uint64_t var_204;
    uint64_t var_205;
    uint64_t var_206;
    uint64_t var_207;
    uint64_t var_208;
    uint64_t var_209;
    uint64_t var_210;
    uint16_t var_211;
    uint16_t var_212;
    uint16_t var_213;
    uint16_t var_214;
    uint16_t var_215;
    uint16_t var_216;
    uint16_t var_217;
    uint16_t var_218;
    struct helper_fmov_FT0_STN_wrapper_ret_type var_219;
    uint64_t var_221;
    struct helper_fpop_wrapper_ret_type var_222;
    struct helper_fucomi_ST0_FT0_wrapper_ret_type var_220;
    uint32_t var_223;
    struct helper_fpop_wrapper_ret_type var_224;
    unsigned char var_225;
    struct helper_fldt_ST0_wrapper_ret_type var_226;
    struct helper_fldt_ST0_wrapper_ret_type var_227;
    uint32_t var_228;
    uint64_t var_229;
    uint64_t var_230;
    uint64_t var_231;
    uint64_t var_232;
    uint64_t var_233;
    uint64_t var_234;
    uint64_t var_235;
    uint64_t var_236;
    uint16_t var_237;
    uint16_t var_238;
    uint16_t var_239;
    uint16_t var_240;
    uint16_t var_241;
    uint16_t var_242;
    uint16_t var_243;
    uint16_t var_244;
    struct helper_fmov_FT0_STN_wrapper_ret_type var_245;
    uint64_t var_247;
    struct helper_fpop_wrapper_ret_type var_248;
    struct helper_fucomi_ST0_FT0_wrapper_ret_type var_246;
    uint32_t var_249;
    uint64_t var_250;
    uint64_t var_251;
    var_0 = revng_init_local_sp(0UL);
    var_1 = init_rbp();
    var_2 = init_cc_src2();
    var_3 = init_state_0x852a();
    var_4 = init_state_0x8480();
    var_5 = init_state_0x8490();
    var_6 = init_state_0x84a0();
    var_7 = init_state_0x84b0();
    var_8 = init_state_0x84c0();
    var_9 = init_state_0x84d0();
    var_10 = init_state_0x84e0();
    var_11 = init_state_0x84f0();
    var_12 = init_state_0x8500();
    var_13 = init_state_0x8498();
    var_14 = init_state_0x84a8();
    var_15 = init_state_0x84b8();
    var_16 = init_state_0x84c8();
    var_17 = init_state_0x84d8();
    var_18 = init_state_0x84e8();
    var_19 = init_state_0x84f8();
    var_20 = init_state_0x8508();
    *(uint64_t *)(var_0 + (-8L)) = var_1;
    var_21 = (uint64_t *)(var_0 + (-64L));
    *var_21 = rdi;
    var_22 = (uint64_t *)(var_0 + (-72L));
    *var_22 = rsi;
    *(uint64_t *)(var_0 + (-96L)) = 4217283UL;
    indirect_placeholder_2();
    var_23 = var_0 + (-88L);
    helper_fstt_ST0_wrapper((struct type_6 *)(0UL), var_23, var_4, var_5, var_6, var_7, var_8, var_9, var_10, var_11, var_12, var_13, var_14, var_15, var_16, var_17, var_18, var_19, var_20);
    var_24 = helper_fpop_wrapper((struct type_6 *)(0UL), var_4);
    var_25 = var_24.field_0;
    var_26 = (uint64_t *)var_23;
    var_27 = *var_26;
    var_28 = (uint32_t *)(var_0 + (-80L));
    var_29 = *var_28;
    var_30 = var_0 + (-24L);
    *(uint64_t *)var_30 = var_27;
    *(uint32_t *)(var_0 + (-16L)) = var_29;
    *(uint64_t *)(var_0 + (-104L)) = 4217319UL;
    indirect_placeholder_2();
    helper_fstt_ST0_wrapper((struct type_6 *)(0UL), var_23, var_25, var_5, var_6, var_7, var_8, var_9, var_10, var_11, var_12, var_13, var_14, var_15, var_16, var_17, var_18, var_19, var_20);
    var_31 = helper_fpop_wrapper((struct type_6 *)(0UL), var_25);
    var_32 = var_31.field_0;
    var_33 = *var_26;
    var_34 = *var_28;
    var_35 = var_0 + (-40L);
    *(uint64_t *)var_35 = var_33;
    *(uint32_t *)(var_0 + (-32L)) = var_34;
    var_36 = (*var_21 == *(uint64_t *)(var_0 + (-48L)));
    var_37 = *(uint64_t *)(var_0 + (-56L));
    var_38 = *var_22;
    rax_0 = 1UL;
    if (var_36) {
        return (var_38 == var_37) ? 0UL : 4294967295UL;
    }
    var_39 = var_38 - var_37;
    if (var_39 == 0UL) {
        return rax_0;
    }
    var_40 = helper_fldt_ST0_wrapper((struct type_6 *)(0UL), var_35, var_32);
    var_41 = helper_fldt_ST0_wrapper((struct type_6 *)(0UL), var_30, var_40.field_0);
    var_42 = var_41.field_0;
    var_43 = helper_fxchg_ST0_STN_wrapper((struct type_6 *)(0UL), 1U, var_42, var_41.field_1, var_41.field_2, var_41.field_3, var_41.field_4, var_41.field_5, var_41.field_6, var_41.field_7, var_41.field_8, var_41.field_9, var_41.field_10, var_41.field_11, var_41.field_12, var_41.field_13, var_41.field_14, var_41.field_15, var_41.field_16);
    var_44 = var_43.field_0;
    var_45 = var_43.field_1;
    var_46 = var_43.field_2;
    var_47 = var_43.field_3;
    var_48 = var_43.field_4;
    var_49 = var_43.field_5;
    var_50 = var_43.field_6;
    var_51 = var_43.field_7;
    var_52 = var_43.field_8;
    var_53 = var_43.field_9;
    var_54 = var_43.field_10;
    var_55 = var_43.field_11;
    var_56 = var_43.field_12;
    var_57 = var_43.field_13;
    var_58 = var_43.field_14;
    var_59 = var_43.field_15;
    var_60 = helper_fmov_FT0_STN_wrapper((struct type_6 *)(0UL), 1U, var_42, var_44, var_45, var_46, var_47, var_48, var_49, var_50, var_51, var_52, var_53, var_54, var_55, var_56, var_57, var_58, var_59);
    var_61 = helper_fucomi_ST0_FT0_wrapper((struct type_6 *)(0UL), var_37, var_39, 17U, var_2, var_3, var_42, var_44, var_45, var_46, var_47, var_48, var_49, var_50, var_51, var_52, var_53, var_54, var_55, var_56, var_57, var_58, var_59, var_60.field_0, var_60.field_1);
    var_62 = var_61.field_0;
    var_63 = helper_fpop_wrapper((struct type_6 *)(0UL), var_42);
    var_64 = var_63.field_0;
    helper_fmov_STN_ST0_wrapper((struct type_6 *)(0UL), 0U, var_64, var_44, var_45, var_46, var_47, var_48, var_49, var_50, var_51, var_52, var_53, var_54, var_55, var_56, var_57, var_58, var_59);
    var_65 = helper_fpop_wrapper((struct type_6 *)(0UL), var_64);
    rax_0 = 4294967295UL;
    var_66 = var_61.field_1;
    var_67 = helper_fldt_ST0_wrapper((struct type_6 *)(0UL), var_30, var_65.field_0);
    var_68 = helper_fldt_ST0_wrapper((struct type_6 *)(0UL), var_35, var_67.field_0);
    var_69 = var_68.field_0;
    var_70 = helper_fxchg_ST0_STN_wrapper((struct type_6 *)(0UL), 1U, var_69, var_68.field_1, var_68.field_2, var_68.field_3, var_68.field_4, var_68.field_5, var_68.field_6, var_68.field_7, var_68.field_8, var_68.field_9, var_68.field_10, var_68.field_11, var_68.field_12, var_68.field_13, var_68.field_14, var_68.field_15, var_68.field_16);
    var_71 = var_70.field_0;
    var_72 = var_70.field_1;
    var_73 = var_70.field_2;
    var_74 = var_70.field_3;
    var_75 = var_70.field_4;
    var_76 = var_70.field_5;
    var_77 = var_70.field_6;
    var_78 = var_70.field_7;
    var_79 = var_70.field_8;
    var_80 = var_70.field_9;
    var_81 = var_70.field_10;
    var_82 = var_70.field_11;
    var_83 = var_70.field_12;
    var_84 = var_70.field_13;
    var_85 = var_70.field_14;
    var_86 = var_70.field_15;
    var_87 = helper_fmov_FT0_STN_wrapper((struct type_6 *)(0UL), 1U, var_69, var_71, var_72, var_73, var_74, var_75, var_76, var_77, var_78, var_79, var_80, var_81, var_82, var_83, var_84, var_85, var_86);
    var_88 = helper_fucomi_ST0_FT0_wrapper((struct type_6 *)(0UL), var_62, var_39, 1U, var_2, var_66, var_69, var_71, var_72, var_73, var_74, var_75, var_76, var_77, var_78, var_79, var_80, var_81, var_82, var_83, var_84, var_85, var_86, var_87.field_0, var_87.field_1);
    var_89 = var_88.field_0;
    var_90 = helper_fpop_wrapper((struct type_6 *)(0UL), var_69);
    var_91 = var_90.field_0;
    helper_fmov_STN_ST0_wrapper((struct type_6 *)(0UL), 0U, var_91, var_71, var_72, var_73, var_74, var_75, var_76, var_77, var_78, var_79, var_80, var_81, var_82, var_83, var_84, var_85, var_86);
    var_92 = helper_fpop_wrapper((struct type_6 *)(0UL), var_91);
    rax_0 = 1UL;
    if (~((var_62 & 65UL) != 0UL & (var_89 & 65UL) != 0UL)) {
        return;
    }
    var_93 = var_88.field_1;
    var_94 = helper_fldt_ST0_wrapper((struct type_6 *)(0UL), var_30, var_92.field_0);
    var_95 = helper_fldt_ST0_wrapper((struct type_6 *)(0UL), var_35, var_94.field_0);
    var_96 = var_95.field_0;
    var_97 = var_95.field_1;
    var_98 = var_95.field_2;
    var_99 = var_95.field_3;
    var_100 = var_95.field_4;
    var_101 = var_95.field_5;
    var_102 = var_95.field_6;
    var_103 = var_95.field_7;
    var_104 = var_95.field_8;
    var_105 = var_95.field_9;
    var_106 = var_95.field_10;
    var_107 = var_95.field_11;
    var_108 = var_95.field_12;
    var_109 = var_95.field_13;
    var_110 = var_95.field_14;
    var_111 = var_95.field_15;
    var_112 = var_95.field_16;
    var_113 = helper_fmov_FT0_STN_wrapper((struct type_6 *)(0UL), 1U, var_96, var_97, var_98, var_99, var_100, var_101, var_102, var_103, var_104, var_105, var_106, var_107, var_108, var_109, var_110, var_111, var_112);
    var_114 = helper_fucomi_ST0_FT0_wrapper((struct type_6 *)(0UL), var_89, var_39, 1U, var_2, var_93, var_96, var_97, var_98, var_99, var_100, var_101, var_102, var_103, var_104, var_105, var_106, var_107, var_108, var_109, var_110, var_111, var_112, var_113.field_0, var_113.field_1);
    var_115 = var_114.field_0;
    var_116 = var_114.field_1;
    var_117 = helper_fpop_wrapper((struct type_6 *)(0UL), var_96);
    var_118 = var_117.field_0;
    helper_fmov_STN_ST0_wrapper((struct type_6 *)(0UL), 0U, var_118, var_97, var_98, var_99, var_100, var_101, var_102, var_103, var_104, var_105, var_106, var_107, var_108, var_109, var_110, var_111, var_112);
    var_119 = helper_fpop_wrapper((struct type_6 *)(0UL), var_118);
    var_120 = var_119.field_0;
    cc_src_0 = var_115;
    state_0x852a_0 = var_116;
    state_0x8480_0 = var_120;
    rax_0 = 0UL;
    if ((var_115 & 4UL) != 0UL) {
        var_121 = helper_fldt_ST0_wrapper((struct type_6 *)(0UL), var_30, var_120);
        var_122 = helper_fldt_ST0_wrapper((struct type_6 *)(0UL), var_35, var_121.field_0);
        var_123 = var_122.field_0;
        var_124 = var_122.field_1;
        var_125 = var_122.field_2;
        var_126 = var_122.field_3;
        var_127 = var_122.field_4;
        var_128 = var_122.field_5;
        var_129 = var_122.field_6;
        var_130 = var_122.field_7;
        var_131 = var_122.field_8;
        var_132 = var_122.field_9;
        var_133 = var_122.field_10;
        var_134 = var_122.field_11;
        var_135 = var_122.field_12;
        var_136 = var_122.field_13;
        var_137 = var_122.field_14;
        var_138 = var_122.field_15;
        var_139 = var_122.field_16;
        var_140 = helper_fmov_FT0_STN_wrapper((struct type_6 *)(0UL), 1U, var_123, var_124, var_125, var_126, var_127, var_128, var_129, var_130, var_131, var_132, var_133, var_134, var_135, var_136, var_137, var_138, var_139);
        var_141 = helper_fucomi_ST0_FT0_wrapper((struct type_6 *)(0UL), var_115, var_39, 1U, var_2, var_116, var_123, var_124, var_125, var_126, var_127, var_128, var_129, var_130, var_131, var_132, var_133, var_134, var_135, var_136, var_137, var_138, var_139, var_140.field_0, var_140.field_1);
        var_142 = var_141.field_0;
        var_143 = var_141.field_1;
        var_144 = helper_fpop_wrapper((struct type_6 *)(0UL), var_123);
        var_145 = var_144.field_0;
        helper_fmov_STN_ST0_wrapper((struct type_6 *)(0UL), 0U, var_145, var_124, var_125, var_126, var_127, var_128, var_129, var_130, var_131, var_132, var_133, var_134, var_135, var_136, var_137, var_138, var_139);
        var_146 = helper_fpop_wrapper((struct type_6 *)(0UL), var_145);
        cc_src_0 = var_142;
        state_0x852a_0 = var_143;
        state_0x8480_0 = var_146.field_0;
        if ((var_142 & 64UL) == 0UL) {
            return rax_0;
        }
    }
    var_147 = helper_fldt_ST0_wrapper((struct type_6 *)(0UL), var_35, state_0x8480_0);
    var_148 = helper_fldt_ST0_wrapper((struct type_6 *)(0UL), var_35, var_147.field_0);
    var_149 = var_148.field_0;
    var_150 = var_148.field_1;
    var_151 = var_148.field_2;
    var_152 = var_148.field_3;
    var_153 = var_148.field_4;
    var_154 = var_148.field_5;
    var_155 = var_148.field_6;
    var_156 = var_148.field_7;
    var_157 = var_148.field_8;
    var_158 = var_148.field_9;
    var_159 = var_148.field_10;
    var_160 = var_148.field_11;
    var_161 = var_148.field_12;
    var_162 = var_148.field_13;
    var_163 = var_148.field_14;
    var_164 = var_148.field_15;
    var_165 = var_148.field_16;
    var_166 = helper_fmov_FT0_STN_wrapper((struct type_6 *)(0UL), 1U, var_149, var_150, var_151, var_152, var_153, var_154, var_155, var_156, var_157, var_158, var_159, var_160, var_161, var_162, var_163, var_164, var_165);
    var_167 = helper_fucomi_ST0_FT0_wrapper((struct type_6 *)(0UL), cc_src_0, var_39, 1U, var_2, state_0x852a_0, var_149, var_150, var_151, var_152, var_153, var_154, var_155, var_156, var_157, var_158, var_159, var_160, var_161, var_162, var_163, var_164, var_165, var_166.field_0, var_166.field_1);
    var_168 = var_167.field_0;
    var_169 = var_167.field_1;
    var_170 = helper_fpop_wrapper((struct type_6 *)(0UL), var_149);
    var_171 = var_170.field_0;
    helper_fmov_STN_ST0_wrapper((struct type_6 *)(0UL), 0U, var_171, var_150, var_151, var_152, var_153, var_154, var_155, var_156, var_157, var_158, var_159, var_160, var_161, var_162, var_163, var_164, var_165);
    var_172 = helper_fpop_wrapper((struct type_6 *)(0UL), var_171);
    var_173 = var_172.field_0;
    cc_src_1 = var_168;
    state_0x852a_1 = var_169;
    state_0x8480_1 = var_173;
    rax_0 = 4294967295UL;
    if ((var_168 & 4UL) != 0UL) {
        var_174 = helper_fldt_ST0_wrapper((struct type_6 *)(0UL), var_35, var_173);
        var_175 = helper_fldt_ST0_wrapper((struct type_6 *)(0UL), var_35, var_174.field_0);
        var_176 = var_175.field_0;
        var_177 = var_175.field_1;
        var_178 = var_175.field_2;
        var_179 = var_175.field_3;
        var_180 = var_175.field_4;
        var_181 = var_175.field_5;
        var_182 = var_175.field_6;
        var_183 = var_175.field_7;
        var_184 = var_175.field_8;
        var_185 = var_175.field_9;
        var_186 = var_175.field_10;
        var_187 = var_175.field_11;
        var_188 = var_175.field_12;
        var_189 = var_175.field_13;
        var_190 = var_175.field_14;
        var_191 = var_175.field_15;
        var_192 = var_175.field_16;
        var_193 = helper_fmov_FT0_STN_wrapper((struct type_6 *)(0UL), 1U, var_176, var_177, var_178, var_179, var_180, var_181, var_182, var_183, var_184, var_185, var_186, var_187, var_188, var_189, var_190, var_191, var_192);
        var_194 = helper_fucomi_ST0_FT0_wrapper((struct type_6 *)(0UL), var_168, var_39, 1U, var_2, var_169, var_176, var_177, var_178, var_179, var_180, var_181, var_182, var_183, var_184, var_185, var_186, var_187, var_188, var_189, var_190, var_191, var_192, var_193.field_0, var_193.field_1);
        var_195 = var_194.field_0;
        var_196 = var_194.field_1;
        var_197 = helper_fpop_wrapper((struct type_6 *)(0UL), var_176);
        var_198 = var_197.field_0;
        helper_fmov_STN_ST0_wrapper((struct type_6 *)(0UL), 0U, var_198, var_177, var_178, var_179, var_180, var_181, var_182, var_183, var_184, var_185, var_186, var_187, var_188, var_189, var_190, var_191, var_192);
        var_199 = helper_fpop_wrapper((struct type_6 *)(0UL), var_198);
        cc_src_1 = var_195;
        state_0x852a_1 = var_196;
        state_0x8480_1 = var_199.field_0;
        if ((var_195 & 64UL) == 0UL) {
            return rax_0;
        }
    }
    var_200 = helper_fldt_ST0_wrapper((struct type_6 *)(0UL), var_30, state_0x8480_1);
    var_201 = helper_fldt_ST0_wrapper((struct type_6 *)(0UL), var_30, var_200.field_0);
    var_202 = var_201.field_0;
    var_203 = var_201.field_1;
    var_204 = var_201.field_2;
    var_205 = var_201.field_3;
    var_206 = var_201.field_4;
    var_207 = var_201.field_5;
    var_208 = var_201.field_6;
    var_209 = var_201.field_7;
    var_210 = var_201.field_8;
    var_211 = var_201.field_9;
    var_212 = var_201.field_10;
    var_213 = var_201.field_11;
    var_214 = var_201.field_12;
    var_215 = var_201.field_13;
    var_216 = var_201.field_14;
    var_217 = var_201.field_15;
    var_218 = var_201.field_16;
    var_219 = helper_fmov_FT0_STN_wrapper((struct type_6 *)(0UL), 1U, var_202, var_203, var_204, var_205, var_206, var_207, var_208, var_209, var_210, var_211, var_212, var_213, var_214, var_215, var_216, var_217, var_218);
    var_220 = helper_fucomi_ST0_FT0_wrapper((struct type_6 *)(0UL), cc_src_1, var_39, 1U, var_2, state_0x852a_1, var_202, var_203, var_204, var_205, var_206, var_207, var_208, var_209, var_210, var_211, var_212, var_213, var_214, var_215, var_216, var_217, var_218, var_219.field_0, var_219.field_1);
    var_221 = var_220.field_0;
    var_222 = helper_fpop_wrapper((struct type_6 *)(0UL), var_202);
    var_223 = var_222.field_0;
    helper_fmov_STN_ST0_wrapper((struct type_6 *)(0UL), 0U, var_223, var_203, var_204, var_205, var_206, var_207, var_208, var_209, var_210, var_211, var_212, var_213, var_214, var_215, var_216, var_217, var_218);
    var_224 = helper_fpop_wrapper((struct type_6 *)(0UL), var_223);
    rax_0 = 1UL;
    if ((var_221 & 4UL) != 0UL) {
        var_225 = var_220.field_1;
        var_226 = helper_fldt_ST0_wrapper((struct type_6 *)(0UL), var_30, var_224.field_0);
        var_227 = helper_fldt_ST0_wrapper((struct type_6 *)(0UL), var_30, var_226.field_0);
        var_228 = var_227.field_0;
        var_229 = var_227.field_1;
        var_230 = var_227.field_2;
        var_231 = var_227.field_3;
        var_232 = var_227.field_4;
        var_233 = var_227.field_5;
        var_234 = var_227.field_6;
        var_235 = var_227.field_7;
        var_236 = var_227.field_8;
        var_237 = var_227.field_9;
        var_238 = var_227.field_10;
        var_239 = var_227.field_11;
        var_240 = var_227.field_12;
        var_241 = var_227.field_13;
        var_242 = var_227.field_14;
        var_243 = var_227.field_15;
        var_244 = var_227.field_16;
        var_245 = helper_fmov_FT0_STN_wrapper((struct type_6 *)(0UL), 1U, var_228, var_229, var_230, var_231, var_232, var_233, var_234, var_235, var_236, var_237, var_238, var_239, var_240, var_241, var_242, var_243, var_244);
        var_246 = helper_fucomi_ST0_FT0_wrapper((struct type_6 *)(0UL), var_221, var_39, 1U, var_2, var_225, var_228, var_229, var_230, var_231, var_232, var_233, var_234, var_235, var_236, var_237, var_238, var_239, var_240, var_241, var_242, var_243, var_244, var_245.field_0, var_245.field_1);
        var_247 = var_246.field_0;
        var_248 = helper_fpop_wrapper((struct type_6 *)(0UL), var_228);
        var_249 = var_248.field_0;
        helper_fmov_STN_ST0_wrapper((struct type_6 *)(0UL), 0U, var_249, var_229, var_230, var_231, var_232, var_233, var_234, var_235, var_236, var_237, var_238, var_239, var_240, var_241, var_242, var_243, var_244);
        helper_fpop_wrapper((struct type_6 *)(0UL), var_249);
        if ((var_247 & 64UL) != 0UL) {
            return rax_0;
        }
    }
    var_250 = *var_22;
    var_251 = *var_21;
    *(uint64_t *)(var_0 + (-112L)) = 4217532UL;
    indirect_placeholder_5(var_251, var_250);
    rax_0 = var_251;
}

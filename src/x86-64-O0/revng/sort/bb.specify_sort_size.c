typedef unsigned char __u_char;
typedef unsigned short __u_short;
typedef unsigned int __u_int;
typedef unsigned long __u_long;
typedef signed char __int8_t;
typedef unsigned char __uint8_t;
typedef short __int16_t;
typedef unsigned short __uint16_t;
typedef int __int32_t;
typedef unsigned int __uint32_t;
typedef long __int64_t;
typedef unsigned long __uint64_t;
typedef __int8_t __int_least8_t;
typedef __uint8_t __uint_least8_t;
typedef __int16_t __int_least16_t;
typedef __uint16_t __uint_least16_t;
typedef __int32_t __int_least32_t;
typedef __uint32_t __uint_least32_t;
typedef __int64_t __int_least64_t;
typedef __uint64_t __uint_least64_t;
typedef long __quad_t;
typedef unsigned long __u_quad_t;
typedef long __intmax_t;
typedef unsigned long __uintmax_t;
typedef unsigned long __dev_t;
typedef unsigned int __uid_t;
typedef unsigned int __gid_t;
typedef unsigned long __ino_t;
typedef unsigned long __ino64_t;
typedef unsigned int __mode_t;
typedef unsigned long __nlink_t;
typedef long __off_t;
typedef long __off64_t;
typedef int __pid_t;
typedef struct {
    int __val[2];
} __fsid_t;
typedef long __clock_t;
typedef unsigned long __rlim_t;
typedef unsigned long __rlim64_t;
typedef unsigned int __id_t;
typedef long __time_t;
typedef unsigned int __useconds_t;
typedef long __suseconds_t;
typedef int __daddr_t;
typedef int __key_t;
typedef int __clockid_t;
typedef void *__timer_t;
typedef long __blksize_t;
typedef long __blkcnt_t;
typedef long __blkcnt64_t;
typedef unsigned long __fsblkcnt_t;
typedef unsigned long __fsblkcnt64_t;
typedef unsigned long __fsfilcnt_t;
typedef unsigned long __fsfilcnt64_t;
typedef long __fsword_t;
typedef long __ssize_t;
typedef long __syscall_slong_t;
typedef unsigned long __syscall_ulong_t;
typedef __off64_t __loff_t;
typedef char *__caddr_t;
typedef long __intptr_t;
typedef unsigned int __socklen_t;
typedef int __sig_atomic_t;
typedef __int8_t int8_t;
typedef __int16_t int16_t;
typedef __int32_t int32_t;
typedef __int64_t int64_t;
typedef __uint8_t uint8_t;
typedef __uint16_t uint16_t;
typedef __uint32_t uint32_t;
typedef __uint64_t uint64_t;
typedef __int_least8_t int_least8_t;
typedef __int_least16_t int_least16_t;
typedef __int_least32_t int_least32_t;
typedef __int_least64_t int_least64_t;
typedef __uint_least8_t uint_least8_t;
typedef __uint_least16_t uint_least16_t;
typedef __uint_least32_t uint_least32_t;
typedef __uint_least64_t uint_least64_t;
typedef signed char int_fast8_t;
typedef long int_fast16_t;
typedef long int_fast32_t;
typedef long int_fast64_t;
typedef unsigned char uint_fast8_t;
typedef unsigned long uint_fast16_t;
typedef unsigned long uint_fast32_t;
typedef unsigned long uint_fast64_t;
typedef long intptr_t;
typedef unsigned long uintptr_t;
typedef __intmax_t intmax_t;
typedef __uintmax_t uintmax_t;
typedef _Bool bool;
struct bb_specify_sort_size_ret_type;
struct helper_pxor_xmm_wrapper_ret_type;
struct type_6;
struct type_8;
struct helper_ucomisd_wrapper_230_ret_type;
struct helper_subsd_wrapper_ret_type;
struct helper_mulsd_wrapper_229_ret_type;
struct helper_cvtsq2sd_wrapper_ret_type;
struct helper_addsd_wrapper_ret_type;
struct helper_divsd_wrapper_ret_type;
struct helper_cvttsd2sq_wrapper_ret_type;
struct helper_cvttsd2sq_wrapper_231_ret_type;
struct bb_specify_sort_size_ret_type {
    uint64_t field_0;
    uint64_t field_1;
    uint64_t field_2;
    uint64_t field_3;
};
struct helper_pxor_xmm_wrapper_ret_type {
    uint64_t field_0;
    uint64_t field_1;
};
struct type_6 {
};
struct type_8 {
};
struct helper_ucomisd_wrapper_230_ret_type {
    uint64_t field_0;
    unsigned char field_1;
};
struct helper_subsd_wrapper_ret_type {
    uint64_t field_0;
    unsigned char field_1;
};
struct helper_mulsd_wrapper_229_ret_type {
    uint64_t field_0;
    unsigned char field_1;
};
struct helper_cvtsq2sd_wrapper_ret_type {
    uint64_t field_0;
    unsigned char field_1;
};
struct helper_addsd_wrapper_ret_type {
    uint64_t field_0;
    unsigned char field_1;
};
struct helper_divsd_wrapper_ret_type {
    uint64_t field_0;
    unsigned char field_1;
};
struct helper_cvttsd2sq_wrapper_ret_type {
    uint64_t field_0;
    unsigned char field_1;
};
struct helper_cvttsd2sq_wrapper_231_ret_type {
    uint64_t field_0;
    unsigned char field_1;
};
extern void abort(void);
extern uint64_t revng_init_local_sp(uint64_t param_0);
extern uint64_t init_rbp(void);
extern uint64_t init_cc_src2(void);
extern uint64_t helper_cc_compute_c_wrapper(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint32_t param_3);
extern uint64_t init_state_0x8558(void);
extern unsigned char init_state_0x8549(void);
extern unsigned char init_state_0x854c(void);
extern unsigned char init_state_0x8548(void);
extern unsigned char init_state_0x854b(void);
extern unsigned char init_state_0x8547(void);
extern unsigned char init_state_0x854d(void);
extern void indirect_placeholder_2(void);
extern struct helper_pxor_xmm_wrapper_ret_type helper_pxor_xmm_wrapper(struct type_6 *param_0, struct type_8 *param_1, struct type_8 *param_2, uint64_t param_3, uint64_t param_4);
extern struct helper_ucomisd_wrapper_230_ret_type helper_ucomisd_wrapper_230(struct type_6 *param_0, struct type_8 *param_1, struct type_8 *param_2, uint64_t param_3, uint64_t param_4, unsigned char param_5, unsigned char param_6);
extern struct helper_subsd_wrapper_ret_type helper_subsd_wrapper(struct type_6 *param_0, struct type_8 *param_1, struct type_8 *param_2, uint64_t param_3, uint64_t param_4, unsigned char param_5, unsigned char param_6, unsigned char param_7, unsigned char param_8, unsigned char param_9, unsigned char param_10);
extern struct helper_mulsd_wrapper_229_ret_type helper_mulsd_wrapper_229(struct type_6 *param_0, struct type_8 *param_1, struct type_8 *param_2, uint64_t param_3, uint64_t param_4, unsigned char param_5, unsigned char param_6, unsigned char param_7, unsigned char param_8, unsigned char param_9, unsigned char param_10);
extern uint64_t indirect_placeholder_16(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint64_t param_3, uint64_t param_4);
extern uint64_t init_state_0x8560(void);
extern struct helper_cvtsq2sd_wrapper_ret_type helper_cvtsq2sd_wrapper(struct type_6 *param_0, struct type_8 *param_1, uint64_t param_2, unsigned char param_3, unsigned char param_4, unsigned char param_5, unsigned char param_6);
extern uint64_t init_r10(void);
extern uint64_t init_r9(void);
extern struct helper_addsd_wrapper_ret_type helper_addsd_wrapper(struct type_6 *param_0, struct type_8 *param_1, struct type_8 *param_2, uint64_t param_3, unsigned char param_4, unsigned char param_5, unsigned char param_6, unsigned char param_7, unsigned char param_8, unsigned char param_9);
extern void indirect_placeholder_68(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint64_t param_3, uint64_t param_4, uint64_t param_5);
extern struct helper_divsd_wrapper_ret_type helper_divsd_wrapper(struct type_6 *param_0, struct type_8 *param_1, struct type_8 *param_2, uint64_t param_3, uint64_t param_4, unsigned char param_5, unsigned char param_6, unsigned char param_7, unsigned char param_8, unsigned char param_9, unsigned char param_10);
extern struct helper_cvttsd2sq_wrapper_ret_type helper_cvttsd2sq_wrapper(struct type_6 *param_0, struct type_8 *param_1, uint64_t param_2, unsigned char param_3, unsigned char param_4);
extern struct helper_cvttsd2sq_wrapper_231_ret_type helper_cvttsd2sq_wrapper_231(struct type_6 *param_0, struct type_8 *param_1, uint64_t param_2, unsigned char param_3, unsigned char param_4);
struct bb_specify_sort_size_ret_type bb_specify_sort_size(uint64_t rdx, uint64_t rdi, uint64_t rsi) {
    struct helper_cvttsd2sq_wrapper_231_ret_type var_48;
    uint64_t var_0;
    uint64_t var_1;
    uint64_t var_2;
    uint64_t var_3;
    uint64_t var_4;
    uint64_t var_5;
    uint64_t var_6;
    unsigned char var_7;
    unsigned char var_8;
    unsigned char var_9;
    unsigned char var_10;
    unsigned char var_11;
    unsigned char var_12;
    uint64_t var_13;
    uint32_t *var_14;
    struct helper_cvtsq2sd_wrapper_ret_type var_32;
    uint64_t *var_15;
    unsigned char *var_16;
    uint64_t var_17;
    uint64_t var_18;
    uint64_t var_19;
    uint64_t var_20;
    uint64_t var_21;
    uint32_t *var_22;
    uint32_t var_23;
    uint32_t var_26;
    uint64_t state_0x8558_0;
    uint64_t var_33;
    struct helper_cvtsq2sd_wrapper_ret_type var_34;
    struct helper_addsd_wrapper_ret_type var_35;
    uint64_t cc_dst_0;
    unsigned char storemerge;
    struct helper_mulsd_wrapper_229_ret_type var_36;
    struct helper_divsd_wrapper_ret_type var_37;
    uint64_t var_38;
    unsigned char var_39;
    uint64_t *var_40;
    struct helper_ucomisd_wrapper_230_ret_type var_41;
    struct helper_ucomisd_wrapper_230_ret_type var_42;
    uint64_t var_43;
    unsigned char var_44;
    uint64_t var_45;
    bool var_46;
    uint64_t var_47;
    uint64_t rax_0;
    struct helper_subsd_wrapper_ret_type var_49;
    struct helper_cvttsd2sq_wrapper_ret_type var_50;
    uint32_t var_51;
    uint64_t *var_24;
    uint64_t var_25;
    uint64_t var_27;
    uint32_t var_28;
    uint64_t local_sp_0;
    uint64_t var_29;
    uint64_t *var_30;
    uint64_t var_31;
    uint32_t var_57;
    uint64_t *var_52;
    uint64_t var_53;
    uint64_t var_54;
    uint64_t var_55;
    uint64_t var_56;
    uint64_t var_62;
    uint64_t var_63;
    struct bb_specify_sort_size_ret_type mrv;
    struct bb_specify_sort_size_ret_type mrv1;
    struct bb_specify_sort_size_ret_type mrv2;
    struct bb_specify_sort_size_ret_type mrv3;
    uint64_t var_58;
    uint64_t var_59;
    uint64_t var_60;
    uint64_t var_61;
    var_0 = revng_init_local_sp(0UL);
    var_1 = init_r10();
    var_2 = init_r9();
    var_3 = init_rbp();
    var_4 = init_cc_src2();
    var_5 = init_state_0x8558();
    var_6 = init_state_0x8560();
    var_7 = init_state_0x8549();
    var_8 = init_state_0x854c();
    var_9 = init_state_0x8548();
    var_10 = init_state_0x854b();
    var_11 = init_state_0x8547();
    var_12 = init_state_0x854d();
    var_13 = var_0 + (-8L);
    *(uint64_t *)var_13 = var_3;
    var_14 = (uint32_t *)(var_0 + (-44L));
    *var_14 = (uint32_t)rdi;
    var_15 = (uint64_t *)(var_0 + (-56L));
    *var_15 = rdx;
    var_16 = (unsigned char *)(var_0 + (-48L));
    *var_16 = (unsigned char)rsi;
    var_17 = var_0 + (-32L);
    var_18 = var_0 + (-40L);
    var_19 = *var_15;
    var_20 = var_0 + (-80L);
    *(uint64_t *)var_20 = 4212505UL;
    var_21 = indirect_placeholder_16(10UL, var_17, 4346536UL, var_19, var_18);
    var_22 = (uint32_t *)(var_0 + (-12L));
    var_23 = (uint32_t)var_21;
    *var_22 = var_23;
    var_26 = var_23;
    local_sp_0 = var_20;
    var_26 = 0U;
    if (var_23 != 0U & (uint64_t)(((uint32_t)(uint64_t)*(unsigned char *)(*(uint64_t *)var_18 + (-1L)) + (-48)) & (-2)) <= 9UL) {
        var_24 = (uint64_t *)var_17;
        var_25 = *var_24;
        var_26 = 1U;
        if (var_25 > 18014398509481983UL) {
            *var_22 = 1U;
        } else {
            *var_24 = (var_25 << 10UL);
            var_26 = *var_22;
        }
    }
    var_51 = var_26;
    var_27 = *(uint64_t *)var_18;
    var_51 = 2U;
    if (var_26 != 2U & (uint64_t)(((uint32_t)(uint64_t)*(unsigned char *)(var_27 + (-1L)) + (-48)) & (-2)) <= 9UL & *(unsigned char *)(var_27 + 1UL) != '\x00') {
        var_28 = (uint32_t)(uint64_t)**(unsigned char **)var_18;
        var_51 = 0U;
        if ((uint64_t)(var_28 + (-37)) == 0UL) {
            var_29 = var_0 + (-88L);
            *(uint64_t *)var_29 = 4212672UL;
            indirect_placeholder_2();
            var_30 = (uint64_t *)var_17;
            var_31 = *var_30;
            cc_dst_0 = var_31;
            local_sp_0 = var_29;
            if ((long)var_31 < (long)0UL) {
                var_33 = (var_31 >> 1UL) | (var_31 & 1UL);
                helper_pxor_xmm_wrapper((struct type_6 *)(0UL), (struct type_8 *)(776UL), (struct type_8 *)(776UL), var_5, var_6);
                var_34 = helper_cvtsq2sd_wrapper((struct type_6 *)(0UL), (struct type_8 *)(776UL), var_33, var_7, var_9, var_10, var_11);
                var_35 = helper_addsd_wrapper((struct type_6 *)(0UL), (struct type_8 *)(776UL), (struct type_8 *)(776UL), var_34.field_0, var_34.field_1, var_8, var_9, var_10, var_11, var_12);
                state_0x8558_0 = var_35.field_0;
                cc_dst_0 = var_33;
                storemerge = var_35.field_1;
            } else {
                helper_pxor_xmm_wrapper((struct type_6 *)(0UL), (struct type_8 *)(776UL), (struct type_8 *)(776UL), var_5, var_6);
                var_32 = helper_cvtsq2sd_wrapper((struct type_6 *)(0UL), (struct type_8 *)(776UL), var_31, var_7, var_9, var_10, var_11);
                state_0x8558_0 = var_32.field_0;
                storemerge = var_32.field_1;
            }
            var_36 = helper_mulsd_wrapper_229((struct type_6 *)(0UL), (struct type_8 *)(776UL), (struct type_8 *)(840UL), state_0x8558_0, var_5, storemerge, var_8, var_9, var_10, var_11, var_12);
            var_37 = helper_divsd_wrapper((struct type_6 *)(0UL), (struct type_8 *)(776UL), (struct type_8 *)(840UL), var_36.field_0, *(uint64_t *)4350872UL, var_36.field_1, var_8, var_9, var_10, var_11, var_12);
            var_38 = var_37.field_0;
            var_39 = var_37.field_1;
            var_40 = (uint64_t *)(var_0 + (-24L));
            *var_40 = var_38;
            var_41 = helper_ucomisd_wrapper_230((struct type_6 *)(0UL), (struct type_8 *)(776UL), (struct type_8 *)(2824UL), *(uint64_t *)4350880UL, var_38, var_39, var_8);
            if ((var_41.field_0 & 65UL) == 0UL) {
                *var_22 = 1U;
                var_51 = 1U;
            } else {
                var_42 = helper_ucomisd_wrapper_230((struct type_6 *)(0UL), (struct type_8 *)(776UL), (struct type_8 *)(2824UL), *var_40, *(uint64_t *)4350888UL, var_41.field_1, var_8);
                var_43 = var_42.field_0;
                var_44 = var_42.field_1;
                var_45 = helper_cc_compute_c_wrapper(cc_dst_0, var_43, var_4, 1U);
                var_46 = (var_45 == 0UL);
                var_47 = *var_40;
                if (var_46) {
                    var_49 = helper_subsd_wrapper((struct type_6 *)(0UL), (struct type_8 *)(776UL), (struct type_8 *)(840UL), var_47, *(uint64_t *)4350888UL, var_44, var_8, var_9, var_10, var_11, var_12);
                    var_50 = helper_cvttsd2sq_wrapper((struct type_6 *)(0UL), (struct type_8 *)(776UL), var_49.field_0, var_49.field_1, var_8);
                    rax_0 = var_50.field_0 ^ (-9223372036854775808L);
                } else {
                    *(uint64_t *)(var_0 + (-64L)) = var_47;
                    var_48 = helper_cvttsd2sq_wrapper_231((struct type_6 *)(0UL), (struct type_8 *)(2824UL), var_47, var_44, var_8);
                    rax_0 = var_48.field_0;
                }
                *var_30 = rax_0;
                *var_22 = 0U;
            }
        } else {
            if ((uint64_t)(var_28 + (-98)) == 0UL) {
                *var_22 = 0U;
                var_51 = 0U;
            }
        }
    }
    var_57 = var_51;
    if (var_51 == 0U) {
        var_58 = (uint64_t)(uint32_t)(uint64_t)*var_16;
        var_59 = *var_15;
        var_60 = (uint64_t)*var_14;
        var_61 = (uint64_t)var_57;
        *(uint64_t *)(local_sp_0 + (-8L)) = 4212979UL;
        indirect_placeholder_68(var_13, var_58, 4344832UL, var_59, var_61, var_60);
        abort();
    }
    var_52 = (uint64_t *)var_17;
    var_53 = *var_52;
    var_54 = *(uint64_t *)6475008UL;
    var_55 = helper_cc_compute_c_wrapper(var_53 - var_54, var_54, var_4, 17U);
    var_57 = 1U;
    if (var_55 == 0UL) {
        mrv.field_0 = var_1;
        mrv1 = mrv;
        mrv1.field_1 = var_2;
        mrv2 = mrv1;
        mrv2.field_2 = var_17;
        mrv3 = mrv2;
        mrv3.field_3 = 4346536UL;
        return mrv3;
    }
    var_56 = *var_52;
    *(uint64_t *)6475008UL = var_56;
    if (var_56 != *var_52) {
        var_62 = (uint64_t)*(uint32_t *)6473484UL * 34UL;
        var_63 = helper_cc_compute_c_wrapper(var_62 - var_56, var_56, var_4, 17U);
        *(uint64_t *)6475008UL = ((var_63 == 0UL) ? var_62 : var_56);
        mrv.field_0 = var_1;
        mrv1 = mrv;
        mrv1.field_1 = var_2;
        mrv2 = mrv1;
        mrv2.field_2 = var_17;
        mrv3 = mrv2;
        mrv3.field_3 = 4346536UL;
        return mrv3;
    }
    *var_22 = 1U;
    var_58 = (uint64_t)(uint32_t)(uint64_t)*var_16;
    var_59 = *var_15;
    var_60 = (uint64_t)*var_14;
    var_61 = (uint64_t)var_57;
    *(uint64_t *)(local_sp_0 + (-8L)) = 4212979UL;
    indirect_placeholder_68(var_13, var_58, 4344832UL, var_59, var_61, var_60);
    abort();
}

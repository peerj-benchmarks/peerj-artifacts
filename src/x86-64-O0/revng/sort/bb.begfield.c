typedef unsigned char __u_char;
typedef unsigned short __u_short;
typedef unsigned int __u_int;
typedef unsigned long __u_long;
typedef signed char __int8_t;
typedef unsigned char __uint8_t;
typedef short __int16_t;
typedef unsigned short __uint16_t;
typedef int __int32_t;
typedef unsigned int __uint32_t;
typedef long __int64_t;
typedef unsigned long __uint64_t;
typedef __int8_t __int_least8_t;
typedef __uint8_t __uint_least8_t;
typedef __int16_t __int_least16_t;
typedef __uint16_t __uint_least16_t;
typedef __int32_t __int_least32_t;
typedef __uint32_t __uint_least32_t;
typedef __int64_t __int_least64_t;
typedef __uint64_t __uint_least64_t;
typedef long __quad_t;
typedef unsigned long __u_quad_t;
typedef long __intmax_t;
typedef unsigned long __uintmax_t;
typedef unsigned long __dev_t;
typedef unsigned int __uid_t;
typedef unsigned int __gid_t;
typedef unsigned long __ino_t;
typedef unsigned long __ino64_t;
typedef unsigned int __mode_t;
typedef unsigned long __nlink_t;
typedef long __off_t;
typedef long __off64_t;
typedef int __pid_t;
typedef struct {
    int __val[2];
} __fsid_t;
typedef long __clock_t;
typedef unsigned long __rlim_t;
typedef unsigned long __rlim64_t;
typedef unsigned int __id_t;
typedef long __time_t;
typedef unsigned int __useconds_t;
typedef long __suseconds_t;
typedef int __daddr_t;
typedef int __key_t;
typedef int __clockid_t;
typedef void *__timer_t;
typedef long __blksize_t;
typedef long __blkcnt_t;
typedef long __blkcnt64_t;
typedef unsigned long __fsblkcnt_t;
typedef unsigned long __fsblkcnt64_t;
typedef unsigned long __fsfilcnt_t;
typedef unsigned long __fsfilcnt64_t;
typedef long __fsword_t;
typedef long __ssize_t;
typedef long __syscall_slong_t;
typedef unsigned long __syscall_ulong_t;
typedef __off64_t __loff_t;
typedef char *__caddr_t;
typedef long __intptr_t;
typedef unsigned int __socklen_t;
typedef int __sig_atomic_t;
typedef __int8_t int8_t;
typedef __int16_t int16_t;
typedef __int32_t int32_t;
typedef __int64_t int64_t;
typedef __uint8_t uint8_t;
typedef __uint16_t uint16_t;
typedef __uint32_t uint32_t;
typedef __uint64_t uint64_t;
typedef __int_least8_t int_least8_t;
typedef __int_least16_t int_least16_t;
typedef __int_least32_t int_least32_t;
typedef __int_least64_t int_least64_t;
typedef __uint_least8_t uint_least8_t;
typedef __uint_least16_t uint_least16_t;
typedef __uint_least32_t uint_least32_t;
typedef __uint_least64_t uint_least64_t;
typedef signed char int_fast8_t;
typedef long int_fast16_t;
typedef long int_fast32_t;
typedef long int_fast64_t;
typedef unsigned char uint_fast8_t;
typedef unsigned long uint_fast16_t;
typedef unsigned long uint_fast32_t;
typedef unsigned long uint_fast64_t;
typedef long intptr_t;
typedef unsigned long uintptr_t;
typedef __intmax_t intmax_t;
typedef __uintmax_t uintmax_t;
extern uint64_t revng_init_local_sp(uint64_t param_0);
extern uint64_t init_rbp(void);
extern uint64_t init_cc_src2(void);
extern uint64_t helper_cc_compute_c_wrapper(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint32_t param_3);
extern uint64_t indirect_placeholder_30(uint64_t param_0);
uint64_t bb_begfield(uint64_t rdi, uint64_t rsi) {
    uint64_t var_0;
    uint64_t var_1;
    uint64_t var_2;
    uint64_t var_3;
    uint64_t var_4;
    uint64_t *var_5;
    uint64_t *var_6;
    uint64_t var_7;
    uint64_t var_8;
    uint64_t *var_9;
    uint64_t var_10;
    uint64_t *var_11;
    uint64_t var_12;
    uint64_t *var_13;
    uint64_t var_14;
    uint64_t *var_15;
    uint64_t local_sp_0;
    uint64_t var_51;
    uint64_t var_45;
    uint64_t var_38;
    uint64_t local_sp_0_be;
    uint64_t var_44;
    uint64_t var_27;
    uint64_t var_37;
    uint64_t var_31;
    uint64_t var_16;
    uint64_t var_17;
    uint64_t var_18;
    uint64_t local_sp_4;
    uint64_t var_19;
    uint64_t var_20;
    uint64_t var_21;
    uint64_t var_22;
    uint64_t var_23;
    uint64_t var_24;
    uint64_t var_25;
    uint64_t var_26;
    uint64_t var_28;
    uint64_t var_29;
    uint64_t var_30;
    uint64_t local_sp_1;
    uint64_t var_32;
    uint64_t var_33;
    uint64_t local_sp_2;
    uint64_t var_34;
    uint64_t var_35;
    uint64_t var_36;
    uint64_t local_sp_3;
    uint64_t var_39;
    uint64_t var_40;
    uint64_t var_41;
    uint64_t var_42;
    uint64_t var_43;
    uint64_t local_sp_5;
    uint64_t var_46;
    uint64_t var_47;
    uint64_t var_48;
    uint64_t var_49;
    uint64_t var_50;
    uint64_t var_52;
    uint64_t var_53;
    uint64_t var_54;
    var_0 = revng_init_local_sp(0UL);
    var_1 = init_rbp();
    var_2 = init_cc_src2();
    *(uint64_t *)(var_0 + (-8L)) = var_1;
    var_3 = var_0 + (-56L);
    var_4 = var_0 + (-48L);
    var_5 = (uint64_t *)var_4;
    *var_5 = rdi;
    var_6 = (uint64_t *)var_3;
    *var_6 = rsi;
    var_7 = **(uint64_t **)var_4;
    var_8 = var_0 + (-16L);
    var_9 = (uint64_t *)var_8;
    *var_9 = var_7;
    var_10 = var_7 + (*(uint64_t *)(*var_5 + 8UL) + (-1L));
    var_11 = (uint64_t *)(var_0 + (-32L));
    *var_11 = var_10;
    var_12 = **(uint64_t **)var_3;
    var_13 = (uint64_t *)(var_0 + (-24L));
    *var_13 = var_12;
    var_14 = *(uint64_t *)(*var_6 + 8UL);
    var_15 = (uint64_t *)(var_0 + (-40L));
    *var_15 = var_14;
    local_sp_0 = var_3;
    local_sp_4 = var_3;
    if (*(uint32_t *)6473480UL != 128U) {
        var_16 = *var_9;
        var_17 = *var_11;
        var_18 = helper_cc_compute_c_wrapper(var_16 - var_17, var_17, var_2, 17U);
        while (var_18 != 0UL)
            {
                var_19 = *var_13;
                *var_13 = (var_19 + (-1L));
                if (var_19 == 0UL) {
                    break;
                }
                var_20 = *var_9;
                var_21 = *var_11;
                var_22 = helper_cc_compute_c_wrapper(var_20 - var_21, var_21, var_2, 17U);
                while (var_22 != 0UL)
                    {
                        if ((uint64_t)((uint32_t)(uint64_t)**(unsigned char **)var_8 - *(uint32_t *)6473480UL) == 0UL) {
                            break;
                        }
                        var_23 = *var_9 + 1UL;
                        *var_9 = var_23;
                        var_20 = var_23;
                        var_21 = *var_11;
                        var_22 = helper_cc_compute_c_wrapper(var_20 - var_21, var_21, var_2, 17U);
                    }
                var_24 = *var_9;
                var_25 = *var_11;
                var_26 = helper_cc_compute_c_wrapper(var_24 - var_25, var_25, var_2, 17U);
                if (var_26 == 0UL) {
                    *var_9 = (*var_9 + 1UL);
                }
                var_16 = *var_9;
                var_17 = *var_11;
                var_18 = helper_cc_compute_c_wrapper(var_16 - var_17, var_17, var_2, 17U);
            }
    }
    var_27 = *var_9;
    var_28 = *var_11;
    var_29 = helper_cc_compute_c_wrapper(var_27 - var_28, var_28, var_2, 17U);
    local_sp_1 = local_sp_0;
    local_sp_4 = local_sp_0;
    while (var_29 != 0UL)
        {
            var_30 = *var_13;
            *var_13 = (var_30 + (-1L));
            if (var_30 == 0UL) {
                break;
            }
            var_31 = *var_9;
            var_32 = *var_11;
            var_33 = helper_cc_compute_c_wrapper(var_31 - var_32, var_32, var_2, 17U);
            local_sp_2 = local_sp_1;
            while (var_33 != 0UL)
                {
                    var_34 = (uint64_t)(uint32_t)(uint64_t)**(unsigned char **)var_8;
                    var_35 = local_sp_1 + (-8L);
                    *(uint64_t *)var_35 = 4214674UL;
                    var_36 = indirect_placeholder_30(var_34);
                    local_sp_1 = var_35;
                    local_sp_2 = var_35;
                    if (*(unsigned char *)((uint64_t)(unsigned char)var_36 | 6473984UL) == '\x00') {
                        break;
                    }
                    var_37 = *var_9 + 1UL;
                    *var_9 = var_37;
                    var_31 = var_37;
                    var_32 = *var_11;
                    var_33 = helper_cc_compute_c_wrapper(var_31 - var_32, var_32, var_2, 17U);
                    local_sp_2 = local_sp_1;
                }
            var_38 = *var_9;
            local_sp_3 = local_sp_2;
            var_39 = *var_11;
            var_40 = helper_cc_compute_c_wrapper(var_38 - var_39, var_39, var_2, 17U);
            local_sp_0_be = local_sp_3;
            while (var_40 != 0UL)
                {
                    var_41 = (uint64_t)(uint32_t)(uint64_t)**(unsigned char **)var_8;
                    var_42 = local_sp_3 + (-8L);
                    *(uint64_t *)var_42 = 4214724UL;
                    var_43 = indirect_placeholder_30(var_41);
                    local_sp_0_be = var_42;
                    local_sp_3 = var_42;
                    if (*(unsigned char *)((uint64_t)(unsigned char)var_43 | 6473984UL) == '\x01') {
                        break;
                    }
                    var_44 = *var_9 + 1UL;
                    *var_9 = var_44;
                    var_38 = var_44;
                    var_39 = *var_11;
                    var_40 = helper_cc_compute_c_wrapper(var_38 - var_39, var_39, var_2, 17U);
                    local_sp_0_be = local_sp_3;
                }
            local_sp_0 = local_sp_0_be;
            var_27 = *var_9;
            var_28 = *var_11;
            var_29 = helper_cc_compute_c_wrapper(var_27 - var_28, var_28, var_2, 17U);
            local_sp_1 = local_sp_0;
            local_sp_4 = local_sp_0;
        }
    local_sp_5 = local_sp_4;
    if (*(unsigned char *)(*var_6 + 48UL) == '\x00') {
        var_52 = *var_9 + *var_15;
        var_53 = *var_11;
        var_54 = (var_52 > var_53) ? var_53 : var_52;
        *var_9 = var_54;
        return var_54;
    }
    var_45 = *var_9;
    var_46 = *var_11;
    var_47 = helper_cc_compute_c_wrapper(var_45 - var_46, var_46, var_2, 17U);
    while (var_47 != 0UL)
        {
            var_48 = (uint64_t)(uint32_t)(uint64_t)**(unsigned char **)var_8;
            var_49 = local_sp_5 + (-8L);
            *(uint64_t *)var_49 = 4214820UL;
            var_50 = indirect_placeholder_30(var_48);
            local_sp_5 = var_49;
            if (*(unsigned char *)((uint64_t)(unsigned char)var_50 | 6473984UL) == '\x00') {
                break;
            }
            var_51 = *var_9 + 1UL;
            *var_9 = var_51;
            var_45 = var_51;
            var_46 = *var_11;
            var_47 = helper_cc_compute_c_wrapper(var_45 - var_46, var_46, var_2, 17U);
        }
    var_52 = *var_9 + *var_15;
    var_53 = *var_11;
    var_54 = (var_52 > var_53) ? var_53 : var_52;
    *var_9 = var_54;
    return var_54;
}

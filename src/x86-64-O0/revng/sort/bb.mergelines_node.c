typedef unsigned char __u_char;
typedef unsigned short __u_short;
typedef unsigned int __u_int;
typedef unsigned long __u_long;
typedef signed char __int8_t;
typedef unsigned char __uint8_t;
typedef short __int16_t;
typedef unsigned short __uint16_t;
typedef int __int32_t;
typedef unsigned int __uint32_t;
typedef long __int64_t;
typedef unsigned long __uint64_t;
typedef __int8_t __int_least8_t;
typedef __uint8_t __uint_least8_t;
typedef __int16_t __int_least16_t;
typedef __uint16_t __uint_least16_t;
typedef __int32_t __int_least32_t;
typedef __uint32_t __uint_least32_t;
typedef __int64_t __int_least64_t;
typedef __uint64_t __uint_least64_t;
typedef long __quad_t;
typedef unsigned long __u_quad_t;
typedef long __intmax_t;
typedef unsigned long __uintmax_t;
typedef unsigned long __dev_t;
typedef unsigned int __uid_t;
typedef unsigned int __gid_t;
typedef unsigned long __ino_t;
typedef unsigned long __ino64_t;
typedef unsigned int __mode_t;
typedef unsigned long __nlink_t;
typedef long __off_t;
typedef long __off64_t;
typedef int __pid_t;
typedef struct {
    int __val[2];
} __fsid_t;
typedef long __clock_t;
typedef unsigned long __rlim_t;
typedef unsigned long __rlim64_t;
typedef unsigned int __id_t;
typedef long __time_t;
typedef unsigned int __useconds_t;
typedef long __suseconds_t;
typedef int __daddr_t;
typedef int __key_t;
typedef int __clockid_t;
typedef void *__timer_t;
typedef long __blksize_t;
typedef long __blkcnt_t;
typedef long __blkcnt64_t;
typedef unsigned long __fsblkcnt_t;
typedef unsigned long __fsblkcnt64_t;
typedef unsigned long __fsfilcnt_t;
typedef unsigned long __fsfilcnt64_t;
typedef long __fsword_t;
typedef long __ssize_t;
typedef long __syscall_slong_t;
typedef unsigned long __syscall_ulong_t;
typedef __off64_t __loff_t;
typedef char *__caddr_t;
typedef long __intptr_t;
typedef unsigned int __socklen_t;
typedef int __sig_atomic_t;
typedef __int8_t int8_t;
typedef __int16_t int16_t;
typedef __int32_t int32_t;
typedef __int64_t int64_t;
typedef __uint8_t uint8_t;
typedef __uint16_t uint16_t;
typedef __uint32_t uint32_t;
typedef __uint64_t uint64_t;
typedef __int_least8_t int_least8_t;
typedef __int_least16_t int_least16_t;
typedef __int_least32_t int_least32_t;
typedef __int_least64_t int_least64_t;
typedef __uint_least8_t uint_least8_t;
typedef __uint_least16_t uint_least16_t;
typedef __uint_least32_t uint_least32_t;
typedef __uint_least64_t uint_least64_t;
typedef signed char int_fast8_t;
typedef long int_fast16_t;
typedef long int_fast32_t;
typedef long int_fast64_t;
typedef unsigned char uint_fast8_t;
typedef unsigned long uint_fast16_t;
typedef unsigned long uint_fast32_t;
typedef unsigned long uint_fast64_t;
typedef long intptr_t;
typedef unsigned long uintptr_t;
typedef __intmax_t intmax_t;
typedef __uintmax_t uintmax_t;
typedef _Bool bool;
extern uint64_t revng_init_local_sp(uint64_t param_0);
extern uint64_t init_rbp(void);
extern uint64_t indirect_placeholder(uint64_t param_0, uint64_t param_1);
extern uint64_t helper_cc_compute_all_wrapper(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint32_t param_3);
extern void indirect_placeholder_1(uint64_t param_0, uint64_t param_1, uint64_t param_2);
void bb_mergelines_node(uint64_t rdx, uint64_t rcx, uint64_t rdi, uint64_t rsi) {
    uint64_t *var_64;
    uint64_t var_0;
    uint64_t var_1;
    uint64_t var_2;
    uint64_t var_3;
    uint64_t *var_4;
    uint64_t *var_5;
    uint64_t *var_6;
    uint64_t *var_7;
    uint64_t **var_8;
    uint64_t var_9;
    uint64_t *var_10;
    uint64_t var_11;
    uint64_t *var_12;
    uint64_t var_13;
    uint64_t *var_14;
    uint64_t var_15;
    uint64_t var_16;
    uint64_t *var_23;
    uint64_t var_24;
    uint64_t var_25;
    uint64_t var_26;
    uint64_t var_27;
    uint64_t local_sp_1_be;
    uint64_t *var_28;
    uint64_t var_29;
    uint64_t var_30;
    uint64_t var_31;
    uint64_t var_32;
    uint64_t var_65;
    uint64_t var_66;
    uint64_t local_sp_0;
    uint64_t *var_67;
    uint64_t var_68;
    uint64_t var_69;
    uint64_t var_53;
    uint64_t *var_54;
    uint64_t var_55;
    uint64_t var_56;
    uint64_t var_57;
    uint64_t var_33;
    uint64_t var_70;
    uint64_t var_58;
    uint64_t var_59;
    uint64_t var_60;
    uint64_t var_61;
    uint64_t var_62;
    bool var_63;
    uint64_t *var_72;
    uint64_t var_71;
    uint64_t var_73;
    uint64_t *var_74;
    uint64_t var_75;
    uint64_t *_pre_phi232;
    uint64_t var_81;
    uint64_t var_82;
    uint64_t *var_83;
    uint64_t var_84;
    uint64_t var_85;
    uint64_t var_76;
    uint64_t local_sp_1;
    uint64_t var_77;
    uint64_t *var_78;
    uint64_t var_79;
    uint64_t var_80;
    uint64_t var_17;
    uint64_t var_18;
    uint64_t var_19;
    uint64_t var_20;
    uint64_t var_21;
    uint64_t var_22;
    uint64_t *var_35;
    uint64_t var_34;
    uint64_t var_36;
    uint64_t *var_37;
    uint64_t var_38;
    uint64_t *_pre_phi230;
    uint64_t var_46;
    uint64_t local_sp_2;
    uint64_t var_47;
    uint64_t *var_48;
    uint64_t var_49;
    uint64_t var_50;
    uint64_t var_51;
    uint64_t var_52;
    uint64_t var_39;
    uint64_t local_sp_3;
    uint64_t var_40;
    uint64_t *var_41;
    uint64_t var_42;
    uint64_t var_43;
    uint64_t var_44;
    uint64_t var_45;
    uint64_t *var_86;
    uint64_t *var_87;
    unsigned int loop_state_var;
    var_0 = revng_init_local_sp(0UL);
    var_1 = init_rbp();
    *(uint64_t *)(var_0 + (-8L)) = var_1;
    var_2 = var_0 + (-88L);
    var_3 = var_0 + (-64L);
    var_4 = (uint64_t *)var_3;
    *var_4 = rdi;
    var_5 = (uint64_t *)(var_0 + (-72L));
    *var_5 = rsi;
    var_6 = (uint64_t *)(var_0 + (-80L));
    *var_6 = rdx;
    var_7 = (uint64_t *)var_2;
    *var_7 = rcx;
    var_8 = (uint64_t **)var_3;
    var_9 = **var_8;
    var_10 = (uint64_t *)(var_0 + (-32L));
    *var_10 = var_9;
    var_11 = *(uint64_t *)(*var_4 + 8UL);
    var_12 = (uint64_t *)(var_0 + (-40L));
    *var_12 = var_11;
    var_13 = (*var_5 >> (((uint64_t)(*(uint32_t *)(*var_4 + 80UL) + 1U) << 1UL) & 62UL)) + 1UL;
    var_14 = (uint64_t *)(var_0 + (-16L));
    *var_14 = var_13;
    var_15 = *var_4;
    local_sp_0 = var_2;
    var_16 = var_15;
    local_sp_1 = var_2;
    if (*(uint32_t *)(var_15 + 80UL) > 1U) {
        var_53 = **(uint64_t **)(var_15 + 32UL);
        var_54 = (uint64_t *)(var_0 + (-24L));
        *var_54 = var_53;
        while (1U)
            {
                var_55 = **var_8;
                var_56 = *var_4;
                var_70 = var_55;
                if (var_55 != *(uint64_t *)(var_56 + 16UL)) {
                    loop_state_var = 0U;
                    break;
                }
                if (*(uint64_t *)(var_56 + 8UL) != *(uint64_t *)(var_56 + 24UL)) {
                    loop_state_var = 0U;
                    break;
                }
                var_57 = *var_14;
                *var_14 = (var_57 + (-1L));
                if (var_57 != 0UL) {
                    var_70 = **var_8;
                    loop_state_var = 1U;
                    break;
                }
                var_58 = *(uint64_t *)(*var_4 + 8UL) + (-32L);
                var_59 = **var_8 + (-32L);
                var_60 = local_sp_0 + (-8L);
                *(uint64_t *)var_60 = 4232782UL;
                var_61 = indirect_placeholder(var_59, var_58);
                var_62 = helper_cc_compute_all_wrapper(var_61, 0UL, 0UL, 24U);
                var_63 = ((uint64_t)(((unsigned char)(var_62 >> 4UL) ^ (unsigned char)var_62) & '\xc0') == 0UL);
                *var_54 = (*var_54 + (-32L));
                local_sp_0 = var_60;
                if (var_63) {
                    var_67 = (uint64_t *)(*var_4 + 8UL);
                    *var_67 = (*var_67 + (-32L));
                    var_68 = *(uint64_t *)(*var_4 + 8UL);
                    var_69 = *var_54;
                    *(uint64_t *)var_69 = *(uint64_t *)var_68;
                    *(uint64_t *)(var_69 + 8UL) = *(uint64_t *)(var_68 + 8UL);
                    *(uint64_t *)(var_69 + 16UL) = *(uint64_t *)(var_68 + 16UL);
                    *(uint64_t *)(var_69 + 24UL) = *(uint64_t *)(var_68 + 24UL);
                } else {
                    var_64 = *var_8;
                    *var_64 = (*var_64 + (-32L));
                    var_65 = **var_8;
                    var_66 = *var_54;
                    *(uint64_t *)var_66 = *(uint64_t *)var_65;
                    *(uint64_t *)(var_66 + 8UL) = *(uint64_t *)(var_65 + 8UL);
                    *(uint64_t *)(var_66 + 16UL) = *(uint64_t *)(var_65 + 16UL);
                    *(uint64_t *)(var_66 + 24UL) = *(uint64_t *)(var_65 + 24UL);
                }
                continue;
            }
        switch (loop_state_var) {
          case 0U:
            {
            }
            break;
          case 1U:
            {
                var_71 = (uint64_t)((long)(*var_10 - var_70) >> (long)5UL);
                var_72 = (uint64_t *)(var_0 + (-48L));
                *var_72 = var_71;
                var_73 = (uint64_t)((long)(*var_12 - *(uint64_t *)(*var_4 + 8UL)) >> (long)5UL);
                var_74 = (uint64_t *)(var_0 + (-56L));
                *var_74 = var_73;
                var_75 = *var_4;
                var_81 = var_75;
                var_76 = var_75;
                _pre_phi232 = var_74;
                _pre_phi230 = var_72;
                if (*(uint64_t *)(var_75 + 48UL) == var_73) {
                    while (**var_8 != *(uint64_t *)(var_81 + 16UL))
                        {
                            var_82 = *var_14;
                            *var_14 = (var_82 + (-1L));
                            if (var_82 == 0UL) {
                                break;
                            }
                            *var_54 = (*var_54 + (-32L));
                            var_83 = *var_8;
                            *var_83 = (*var_83 + (-32L));
                            var_84 = **var_8;
                            var_85 = *var_54;
                            *(uint64_t *)var_85 = *(uint64_t *)var_84;
                            *(uint64_t *)(var_85 + 8UL) = *(uint64_t *)(var_84 + 8UL);
                            *(uint64_t *)(var_85 + 16UL) = *(uint64_t *)(var_84 + 16UL);
                            *(uint64_t *)(var_85 + 24UL) = *(uint64_t *)(var_84 + 24UL);
                            var_81 = *var_4;
                        }
                }
                if (*(uint64_t *)(var_75 + 40UL) == *var_72) {
                    while (*(uint64_t *)(var_76 + 8UL) != *(uint64_t *)(var_76 + 24UL))
                        {
                            var_77 = *var_14;
                            *var_14 = (var_77 + (-1L));
                            if (var_77 == 0UL) {
                                break;
                            }
                            *var_54 = (*var_54 + (-32L));
                            var_78 = (uint64_t *)(*var_4 + 8UL);
                            *var_78 = (*var_78 + (-32L));
                            var_79 = *(uint64_t *)(*var_4 + 8UL);
                            var_80 = *var_54;
                            *(uint64_t *)var_80 = *(uint64_t *)var_79;
                            *(uint64_t *)(var_80 + 8UL) = *(uint64_t *)(var_79 + 8UL);
                            *(uint64_t *)(var_80 + 16UL) = *(uint64_t *)(var_79 + 16UL);
                            *(uint64_t *)(var_80 + 24UL) = *(uint64_t *)(var_79 + 24UL);
                            var_76 = *var_4;
                        }
                }
                **(uint64_t **)(*var_4 + 32UL) = *var_54;
            }
            break;
        }
    } else {
        while (1U)
            {
                var_17 = **var_8;
                var_33 = var_17;
                local_sp_2 = local_sp_1;
                local_sp_3 = local_sp_1;
                if (var_17 != *(uint64_t *)(var_16 + 16UL)) {
                    loop_state_var = 0U;
                    break;
                }
                if (*(uint64_t *)(var_16 + 8UL) != *(uint64_t *)(var_16 + 24UL)) {
                    loop_state_var = 0U;
                    break;
                }
                var_18 = *var_14;
                *var_14 = (var_18 + (-1L));
                if (var_18 != 0UL) {
                    var_33 = **var_8;
                    loop_state_var = 1U;
                    break;
                }
                var_19 = *(uint64_t *)(*var_4 + 8UL) + (-32L);
                var_20 = **var_8 + (-32L);
                *(uint64_t *)(local_sp_1 + (-8L)) = 4233335UL;
                var_21 = indirect_placeholder(var_20, var_19);
                var_22 = helper_cc_compute_all_wrapper(var_21, 0UL, 0UL, 24U);
                if ((uint64_t)(((unsigned char)(var_22 >> 4UL) ^ (unsigned char)var_22) & '\xc0') == 0UL) {
                    var_28 = (uint64_t *)(*var_4 + 8UL);
                    *var_28 = (*var_28 + (-32L));
                    var_29 = *(uint64_t *)(*var_4 + 8UL);
                    var_30 = *var_7;
                    var_31 = *var_6;
                    var_32 = local_sp_1 + (-16L);
                    *(uint64_t *)var_32 = 4233432UL;
                    indirect_placeholder_1(var_30, var_29, var_31);
                    local_sp_1_be = var_32;
                } else {
                    var_23 = *var_8;
                    *var_23 = (*var_23 + (-32L));
                    var_24 = **var_8;
                    var_25 = *var_7;
                    var_26 = *var_6;
                    var_27 = local_sp_1 + (-16L);
                    *(uint64_t *)var_27 = 4233383UL;
                    indirect_placeholder_1(var_25, var_24, var_26);
                    local_sp_1_be = var_27;
                }
                var_16 = *var_4;
                local_sp_1 = local_sp_1_be;
                continue;
            }
        switch (loop_state_var) {
          case 0U:
            {
            }
            break;
          case 1U:
            {
                var_34 = (uint64_t)((long)(*var_10 - var_33) >> (long)5UL);
                var_35 = (uint64_t *)(var_0 + (-48L));
                *var_35 = var_34;
                var_36 = (uint64_t)((long)(*var_12 - *(uint64_t *)(*var_4 + 8UL)) >> (long)5UL);
                var_37 = (uint64_t *)(var_0 + (-56L));
                *var_37 = var_36;
                var_38 = *var_4;
                var_46 = var_38;
                var_39 = var_38;
                _pre_phi232 = var_37;
                _pre_phi230 = var_35;
                if (*(uint64_t *)(var_38 + 48UL) == var_36) {
                    while (**var_8 != *(uint64_t *)(var_46 + 16UL))
                        {
                            var_47 = *var_14;
                            *var_14 = (var_47 + (-1L));
                            if (var_47 == 0UL) {
                                break;
                            }
                            var_48 = *var_8;
                            *var_48 = (*var_48 + (-32L));
                            var_49 = **var_8;
                            var_50 = *var_7;
                            var_51 = *var_6;
                            var_52 = local_sp_2 + (-8L);
                            *(uint64_t *)var_52 = 4233605UL;
                            indirect_placeholder_1(var_50, var_49, var_51);
                            var_46 = *var_4;
                            local_sp_2 = var_52;
                        }
                }
                if (*(uint64_t *)(var_38 + 40UL) == *var_35) {
                    while (*(uint64_t *)(var_39 + 8UL) != *(uint64_t *)(var_39 + 24UL))
                        {
                            var_40 = *var_14;
                            *var_14 = (var_40 + (-1L));
                            if (var_40 == 0UL) {
                                break;
                            }
                            var_41 = (uint64_t *)(*var_4 + 8UL);
                            *var_41 = (*var_41 + (-32L));
                            var_42 = *(uint64_t *)(*var_4 + 8UL);
                            var_43 = *var_7;
                            var_44 = *var_6;
                            var_45 = local_sp_3 + (-8L);
                            *(uint64_t *)var_45 = 4233707UL;
                            indirect_placeholder_1(var_43, var_42, var_44);
                            var_39 = *var_4;
                            local_sp_3 = var_45;
                        }
                }
                *_pre_phi230 = (uint64_t)((long)(*var_10 - **var_8) >> (long)5UL);
                *_pre_phi232 = (uint64_t)((long)(*var_12 - *(uint64_t *)(*var_4 + 8UL)) >> (long)5UL);
                var_86 = (uint64_t *)(*var_4 + 40UL);
                *var_86 = (*var_86 - *_pre_phi230);
                var_87 = (uint64_t *)(*var_4 + 48UL);
                *var_87 = (*var_87 - *_pre_phi232);
                return;
            }
            break;
        }
    }
}

typedef unsigned char __u_char;
typedef unsigned short __u_short;
typedef unsigned int __u_int;
typedef unsigned long __u_long;
typedef signed char __int8_t;
typedef unsigned char __uint8_t;
typedef short __int16_t;
typedef unsigned short __uint16_t;
typedef int __int32_t;
typedef unsigned int __uint32_t;
typedef long __int64_t;
typedef unsigned long __uint64_t;
typedef __int8_t __int_least8_t;
typedef __uint8_t __uint_least8_t;
typedef __int16_t __int_least16_t;
typedef __uint16_t __uint_least16_t;
typedef __int32_t __int_least32_t;
typedef __uint32_t __uint_least32_t;
typedef __int64_t __int_least64_t;
typedef __uint64_t __uint_least64_t;
typedef long __quad_t;
typedef unsigned long __u_quad_t;
typedef long __intmax_t;
typedef unsigned long __uintmax_t;
typedef unsigned long __dev_t;
typedef unsigned int __uid_t;
typedef unsigned int __gid_t;
typedef unsigned long __ino_t;
typedef unsigned long __ino64_t;
typedef unsigned int __mode_t;
typedef unsigned long __nlink_t;
typedef long __off_t;
typedef long __off64_t;
typedef int __pid_t;
typedef struct {
    int __val[2];
} __fsid_t;
typedef long __clock_t;
typedef unsigned long __rlim_t;
typedef unsigned long __rlim64_t;
typedef unsigned int __id_t;
typedef long __time_t;
typedef unsigned int __useconds_t;
typedef long __suseconds_t;
typedef int __daddr_t;
typedef int __key_t;
typedef int __clockid_t;
typedef void *__timer_t;
typedef long __blksize_t;
typedef long __blkcnt_t;
typedef long __blkcnt64_t;
typedef unsigned long __fsblkcnt_t;
typedef unsigned long __fsblkcnt64_t;
typedef unsigned long __fsfilcnt_t;
typedef unsigned long __fsfilcnt64_t;
typedef long __fsword_t;
typedef long __ssize_t;
typedef long __syscall_slong_t;
typedef unsigned long __syscall_ulong_t;
typedef __off64_t __loff_t;
typedef char *__caddr_t;
typedef long __intptr_t;
typedef unsigned int __socklen_t;
typedef int __sig_atomic_t;
typedef __int8_t int8_t;
typedef __int16_t int16_t;
typedef __int32_t int32_t;
typedef __int64_t int64_t;
typedef __uint8_t uint8_t;
typedef __uint16_t uint16_t;
typedef __uint32_t uint32_t;
typedef __uint64_t uint64_t;
typedef __int_least8_t int_least8_t;
typedef __int_least16_t int_least16_t;
typedef __int_least32_t int_least32_t;
typedef __int_least64_t int_least64_t;
typedef __uint_least8_t uint_least8_t;
typedef __uint_least16_t uint_least16_t;
typedef __uint_least32_t uint_least32_t;
typedef __uint_least64_t uint_least64_t;
typedef signed char int_fast8_t;
typedef long int_fast16_t;
typedef long int_fast32_t;
typedef long int_fast64_t;
typedef unsigned char uint_fast8_t;
typedef unsigned long uint_fast16_t;
typedef unsigned long uint_fast32_t;
typedef unsigned long uint_fast64_t;
typedef long intptr_t;
typedef unsigned long uintptr_t;
typedef __intmax_t intmax_t;
typedef __uintmax_t uintmax_t;
typedef _Bool bool;
struct helper_divq_EAX_wrapper_ret_type;
struct type_5;
struct indirect_placeholder_107_ret_type;
struct indirect_placeholder_108_ret_type;
struct indirect_placeholder_109_ret_type;
struct helper_divq_EAX_wrapper_ret_type {
    uint64_t field_0;
    uint64_t field_1;
    uint64_t field_2;
    uint32_t field_3;
    uint64_t field_4;
    uint32_t field_5;
    uint64_t field_6;
    uint32_t field_7;
    uint64_t field_8;
    uint32_t field_9;
    uint32_t field_10;
    unsigned char field_11;
    uint32_t field_12;
};
struct type_5 {
};
struct indirect_placeholder_107_ret_type {
    uint64_t field_0;
    uint64_t field_1;
    uint64_t field_2;
};
struct indirect_placeholder_108_ret_type {
    uint64_t field_0;
    uint64_t field_1;
};
struct indirect_placeholder_109_ret_type {
    uint64_t field_0;
    uint64_t field_1;
};
extern void abort(void);
extern uint64_t revng_init_local_sp(uint64_t param_0);
extern uint64_t init_rbp(void);
extern uint64_t indirect_placeholder(uint64_t param_0, uint64_t param_1);
extern uint64_t init_cc_src2(void);
extern uint64_t helper_cc_compute_c_wrapper(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint32_t param_3);
extern void indirect_placeholder_2(void);
extern uint64_t init_rbx(void);
extern uint64_t init_r10(void);
extern uint64_t init_state_0x9018(void);
extern uint32_t init_state_0x9010(void);
extern uint64_t init_state_0x8408(void);
extern uint64_t init_state_0x8328(void);
extern uint64_t init_state_0x82d8(void);
extern uint32_t init_state_0x9080(void);
extern uint32_t init_state_0x8248(void);
extern struct helper_divq_EAX_wrapper_ret_type helper_divq_EAX_wrapper(struct type_5 *param_0, uint64_t param_1, uint64_t param_2, uint64_t param_3, uint64_t param_4, uint64_t param_5, uint64_t param_6, uint64_t param_7, uint64_t param_8, uint64_t param_9, uint64_t param_10, uint64_t param_11, uint64_t param_12, uint64_t param_13, uint32_t param_14, uint64_t param_15, uint64_t param_16, uint64_t param_17, uint32_t param_18, uint32_t param_19);
extern uint64_t init_r9(void);
extern uint64_t init_r8(void);
extern uint64_t indirect_placeholder_30(uint64_t param_0);
extern void indirect_placeholder_4(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint64_t param_3);
extern uint64_t indirect_placeholder_95(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint64_t param_3, uint64_t param_4, uint64_t param_5, uint64_t param_6, uint64_t param_7);
extern struct indirect_placeholder_107_ret_type indirect_placeholder_107(uint64_t param_0, uint64_t param_1, uint64_t param_2);
extern struct indirect_placeholder_108_ret_type indirect_placeholder_108(uint64_t param_0);
extern struct indirect_placeholder_109_ret_type indirect_placeholder_109(uint64_t param_0);
uint64_t bb_fillbuf(uint64_t rdx, uint64_t rdi, uint64_t rsi) {
    uint64_t rdi2_0;
    uint64_t var_0;
    uint64_t var_1;
    uint64_t var_2;
    uint64_t var_3;
    uint64_t var_4;
    uint64_t var_5;
    uint64_t var_6;
    uint64_t var_7;
    uint32_t var_8;
    uint64_t var_9;
    uint64_t var_10;
    uint64_t var_11;
    uint32_t var_12;
    uint32_t var_13;
    uint64_t var_14;
    uint64_t var_15;
    uint64_t var_16;
    uint64_t *var_17;
    uint64_t *var_18;
    uint64_t *var_19;
    uint64_t var_20;
    uint64_t var_21;
    uint64_t *var_22;
    unsigned char var_23;
    unsigned char *var_24;
    uint64_t var_25;
    uint64_t *var_26;
    uint64_t var_27;
    uint64_t *var_28;
    uint64_t var_29;
    uint64_t rax_2;
    uint64_t state_0x82d8_1;
    uint64_t var_104;
    uint64_t var_105;
    uint64_t var_106;
    uint64_t rsi3_1;
    uint64_t var_97;
    uint64_t var_98;
    uint64_t var_82;
    uint64_t var_83;
    uint64_t var_84;
    uint64_t var_85;
    uint64_t local_sp_3;
    uint64_t local_sp_1;
    uint64_t var_86;
    uint64_t var_87;
    uint64_t var_88;
    uint64_t rax_0;
    uint64_t local_sp_0;
    bool var_89;
    uint64_t var_90;
    uint64_t local_sp_8;
    uint32_t state_0x9010_0;
    uint64_t local_sp_6;
    uint64_t var_91;
    uint64_t var_92;
    uint64_t var_93;
    uint64_t local_sp_2;
    uint64_t var_94;
    uint64_t var_95;
    uint64_t var_96;
    uint64_t local_sp_5;
    uint64_t var_73;
    uint64_t var_74;
    uint32_t state_0x9080_2;
    uint64_t var_70;
    uint64_t var_71;
    uint64_t var_72;
    uint64_t var_69;
    uint64_t local_sp_4;
    uint64_t var_75;
    uint64_t var_76;
    uint64_t var_77;
    uint64_t var_78;
    uint64_t var_79;
    struct indirect_placeholder_107_ret_type var_80;
    uint64_t var_81;
    uint64_t rax_1;
    uint64_t state_0x9018_2;
    uint64_t rsi3_0;
    uint32_t state_0x9010_2;
    uint64_t state_0x9018_0;
    uint64_t state_0x82d8_0;
    uint32_t state_0x8248_2;
    uint32_t state_0x9080_0;
    uint32_t state_0x8248_0;
    uint64_t var_55;
    uint64_t var_56;
    uint64_t var_57;
    uint64_t var_99;
    uint64_t var_100;
    uint64_t state_0x9018_1;
    uint32_t state_0x9010_1;
    uint64_t local_sp_7;
    uint32_t state_0x9080_1;
    uint32_t state_0x8248_1;
    uint64_t var_101;
    struct indirect_placeholder_108_ret_type var_102;
    uint64_t var_103;
    uint64_t var_58;
    uint64_t var_59;
    uint64_t var_61;
    uint64_t var_62;
    uint32_t var_63;
    uint64_t var_64;
    struct helper_divq_EAX_wrapper_ret_type var_60;
    uint32_t var_65;
    uint32_t var_66;
    uint64_t var_67;
    uint64_t var_68;
    uint64_t var_30;
    uint64_t var_31;
    uint64_t rsi3_1_ph;
    uint64_t var_32;
    uint64_t var_33;
    uint64_t var_34;
    uint64_t local_sp_8_ph;
    uint64_t *var_35;
    uint64_t *var_36;
    uint64_t var_37;
    uint64_t *var_38;
    uint64_t *var_39;
    uint64_t var_40;
    uint64_t *var_41;
    uint64_t *var_42;
    uint64_t *var_43;
    uint64_t *var_44;
    uint64_t var_45;
    uint64_t *var_46;
    uint64_t var_47;
    uint64_t *var_48;
    uint64_t state_0x82d8_2;
    uint64_t **var_49;
    uint64_t var_50;
    uint64_t var_51;
    struct indirect_placeholder_109_ret_type var_52;
    uint64_t var_53;
    uint64_t var_54;
    unsigned int loop_state_var;
    bool switch_state_var;
    var_0 = revng_init_local_sp(0UL);
    var_1 = init_r10();
    var_2 = init_r9();
    var_3 = init_rbx();
    var_4 = init_rbp();
    var_5 = init_cc_src2();
    var_6 = init_r8();
    var_7 = init_state_0x9018();
    var_8 = init_state_0x9010();
    var_9 = init_state_0x8408();
    var_10 = init_state_0x8328();
    var_11 = init_state_0x82d8();
    var_12 = init_state_0x9080();
    var_13 = init_state_0x8248();
    var_14 = var_0 + (-8L);
    *(uint64_t *)var_14 = var_4;
    var_15 = var_0 + (-152L);
    var_16 = var_0 + (-128L);
    var_17 = (uint64_t *)var_16;
    *var_17 = rdi;
    var_18 = (uint64_t *)(var_0 + (-136L));
    *var_18 = rsi;
    var_19 = (uint64_t *)(var_0 + (-144L));
    *var_19 = rdx;
    var_20 = *(uint64_t *)6475048UL;
    var_21 = var_0 + (-64L);
    var_22 = (uint64_t *)var_21;
    *var_22 = var_20;
    var_23 = *(unsigned char *)6473216UL;
    var_24 = (unsigned char *)(var_0 + (-65L));
    *var_24 = var_23;
    var_25 = *(uint64_t *)(*var_17 + 40UL);
    var_26 = (uint64_t *)(var_0 + (-80L));
    *var_26 = var_25;
    var_27 = *(uint64_t *)6473472UL + (-34L);
    var_28 = (uint64_t *)(var_0 + (-16L));
    *var_28 = var_27;
    var_29 = *var_17;
    rax_2 = 0UL;
    state_0x9080_2 = var_12;
    state_0x9018_2 = var_7;
    state_0x9010_2 = var_8;
    state_0x8248_2 = var_13;
    rsi3_1_ph = rsi;
    local_sp_8_ph = var_15;
    state_0x82d8_2 = var_11;
    if (*(unsigned char *)(var_29 + 48UL) == '\x00') {
        return rax_2;
    }
    var_30 = *(uint64_t *)(var_29 + 8UL);
    var_31 = *(uint64_t *)(var_29 + 32UL);
    rax_2 = 1UL;
    if (var_30 == var_31) {
        var_32 = **(uint64_t **)var_16 + (var_30 - var_31);
        var_33 = var_0 + (-160L);
        *(uint64_t *)var_33 = 4215483UL;
        indirect_placeholder_2();
        var_34 = *var_17;
        *(uint64_t *)(var_34 + 8UL) = *(uint64_t *)(var_34 + 32UL);
        *(uint64_t *)(*var_17 + 16UL) = 0UL;
        rsi3_1_ph = var_32;
        local_sp_8_ph = var_33;
    }
    var_35 = (uint64_t *)(var_0 + (-24L));
    var_36 = (uint64_t *)(var_0 + (-88L));
    var_37 = var_0 + (-32L);
    var_38 = (uint64_t *)var_37;
    var_39 = (uint64_t *)(var_0 + (-40L));
    var_40 = var_0 + (-48L);
    var_41 = (uint64_t *)var_40;
    var_42 = (uint64_t *)(var_0 + (-96L));
    var_43 = (uint64_t *)(var_0 + (-104L));
    var_44 = (uint64_t *)(var_0 + (-56L));
    var_45 = var_0 + (-112L);
    var_46 = (uint64_t *)var_45;
    var_47 = var_0 + (-120L);
    var_48 = (uint64_t *)var_47;
    rsi3_1 = rsi3_1_ph;
    local_sp_8 = local_sp_8_ph;
    while (1U)
        {
            var_49 = (uint64_t **)var_16;
            *var_35 = (*(uint64_t *)(*var_17 + 8UL) + **var_49);
            var_50 = *var_17;
            var_51 = local_sp_8 + (-8L);
            *(uint64_t *)var_51 = 4215545UL;
            var_52 = indirect_placeholder_109(var_50);
            var_53 = var_52.field_0;
            var_54 = var_52.field_1;
            *var_36 = var_53;
            *var_38 = (var_53 - (*(uint64_t *)(*var_17 + 16UL) << 5UL));
            *var_39 = ((*var_36 - (*var_26 * *(uint64_t *)(*var_17 + 16UL))) - *var_35);
            rdi2_0 = var_54;
            rsi3_1 = var_47;
            state_0x9010_0 = state_0x9010_2;
            local_sp_6 = var_51;
            rsi3_0 = rsi3_1;
            state_0x9018_0 = state_0x9018_2;
            state_0x82d8_0 = state_0x82d8_2;
            state_0x9080_0 = state_0x9080_2;
            state_0x8248_0 = state_0x8248_2;
            if (*(uint64_t *)(*var_17 + 16UL) == 0UL) {
                rax_1 = **var_49;
            } else {
                rax_1 = *(uint64_t *)(*var_38 + 8UL) + **(uint64_t **)var_37;
            }
            *var_41 = rax_1;
            while (1U)
                {
                    var_55 = *var_26 + 1UL;
                    var_56 = *var_39;
                    var_57 = helper_cc_compute_c_wrapper(var_55 - var_56, var_56, var_5, 17U);
                    state_0x82d8_1 = state_0x82d8_0;
                    state_0x9018_1 = state_0x9018_0;
                    state_0x9010_1 = state_0x9010_0;
                    local_sp_7 = local_sp_6;
                    state_0x9080_1 = state_0x9080_0;
                    state_0x8248_1 = state_0x8248_0;
                    if (var_57 != 0UL) {
                        loop_state_var = 0U;
                        break;
                    }
                    var_58 = *var_39 + (-1L);
                    var_59 = *var_26 + 1UL;
                    var_60 = helper_divq_EAX_wrapper((struct type_5 *)(0UL), var_59, 4215691UL, var_1, var_2, var_3, var_58, var_14, 0UL, var_59, var_6, rdi2_0, rsi3_0, state_0x9018_0, state_0x9010_0, var_9, var_10, state_0x82d8_0, state_0x9080_0, state_0x8248_0);
                    var_61 = var_60.field_1;
                    var_62 = var_60.field_6;
                    var_63 = var_60.field_7;
                    var_64 = var_60.field_8;
                    var_65 = var_60.field_9;
                    var_66 = var_60.field_10;
                    *var_42 = var_61;
                    var_67 = *var_35;
                    var_68 = local_sp_6 + (-8L);
                    *(uint64_t *)var_68 = 4215723UL;
                    indirect_placeholder_2();
                    *var_43 = var_67;
                    *var_44 = (var_67 + *var_35);
                    *var_39 = (*var_39 - *var_43);
                    rax_2 = 0UL;
                    state_0x82d8_1 = var_64;
                    state_0x9010_0 = var_63;
                    local_sp_4 = var_68;
                    state_0x9018_0 = var_62;
                    state_0x82d8_0 = var_64;
                    state_0x9080_0 = var_65;
                    state_0x8248_0 = var_66;
                    state_0x9018_1 = var_62;
                    state_0x9010_1 = var_63;
                    state_0x9080_1 = var_65;
                    state_0x8248_1 = var_66;
                    if (*var_43 != *var_42) {
                        var_69 = *var_18;
                        *(uint64_t *)(local_sp_6 + (-16L)) = 4215772UL;
                        indirect_placeholder_2();
                        if ((uint64_t)(uint32_t)var_69 == 0UL) {
                            var_70 = *var_19;
                            *(uint64_t *)(local_sp_6 + (-24L)) = 4215796UL;
                            indirect_placeholder_4(var_3, var_14, 4346599UL, var_70);
                            abort();
                        }
                        var_71 = *var_18;
                        var_72 = local_sp_6 + (-24L);
                        *(uint64_t *)var_72 = 4215808UL;
                        indirect_placeholder_2();
                        local_sp_4 = var_72;
                        if ((uint64_t)(uint32_t)var_71 != 0UL) {
                            *(unsigned char *)(*var_17 + 48UL) = (unsigned char)'\x01';
                            var_73 = **var_49;
                            var_74 = *var_44;
                            if (var_73 != var_74) {
                                loop_state_var = 2U;
                                break;
                            }
                            if (*var_41 != var_74 & (uint64_t)(*(unsigned char *)(var_74 + (-1L)) - *var_24) == 0UL) {
                                *var_44 = (var_74 + 1UL);
                                *(unsigned char *)var_74 = *var_24;
                            }
                        }
                    }
                    local_sp_5 = local_sp_4;
                    var_75 = *var_44;
                    var_76 = *var_35;
                    var_77 = var_75 - var_76;
                    var_78 = (uint64_t)(uint32_t)(uint64_t)*var_24;
                    var_79 = local_sp_5 + (-8L);
                    *(uint64_t *)var_79 = 4216193UL;
                    var_80 = indirect_placeholder_107(var_77, var_76, var_78);
                    var_81 = var_80.field_0;
                    *var_46 = var_81;
                    local_sp_0 = var_79;
                    local_sp_3 = var_79;
                    local_sp_6 = var_79;
                    local_sp_7 = var_79;
                    while (var_81 != 0UL)
                        {
                            **(unsigned char **)var_45 = (unsigned char)'\x00';
                            *var_35 = (*var_46 + 1UL);
                            *var_38 = (*var_38 + (-32L));
                            **(uint64_t **)var_37 = *var_41;
                            *(uint64_t *)(*var_38 + 8UL) = (*var_35 - *var_41);
                            var_82 = *(uint64_t *)(*var_38 + 8UL);
                            var_83 = *var_28;
                            var_84 = helper_cc_compute_c_wrapper(var_82 - var_83, var_83, var_5, 17U);
                            *var_28 = ((var_84 == 0UL) ? var_82 : var_83);
                            *var_39 = (*var_39 - *var_26);
                            var_85 = *var_22;
                            if (var_85 != 0UL) {
                                if (*(uint64_t *)(var_85 + 16UL) == 18446744073709551615UL) {
                                    rax_0 = *var_46;
                                } else {
                                    var_86 = *var_38;
                                    var_87 = local_sp_5 + (-16L);
                                    *(uint64_t *)var_87 = 4216027UL;
                                    var_88 = indirect_placeholder(var_86, var_85);
                                    rax_0 = var_88;
                                    local_sp_0 = var_87;
                                }
                                *(uint64_t *)(*var_38 + 24UL) = rax_0;
                                var_89 = (**(uint64_t **)var_21 == 18446744073709551615UL);
                                var_90 = *var_22;
                                local_sp_1 = local_sp_0;
                                local_sp_2 = local_sp_0;
                                if (var_89) {
                                    var_91 = *var_38;
                                    var_92 = local_sp_0 + (-8L);
                                    *(uint64_t *)var_92 = 4216073UL;
                                    var_93 = indirect_placeholder(var_91, var_90);
                                    *(uint64_t *)(*var_38 + 16UL) = var_93;
                                    local_sp_3 = var_92;
                                } else {
                                    if (*(unsigned char *)(var_90 + 48UL) != '\x00') {
                                        var_94 = (uint64_t)(uint32_t)(uint64_t)**(unsigned char **)var_40;
                                        var_95 = local_sp_1 + (-8L);
                                        *(uint64_t *)var_95 = 4216122UL;
                                        var_96 = indirect_placeholder_30(var_94);
                                        local_sp_1 = var_95;
                                        local_sp_2 = var_95;
                                        while (*(unsigned char *)((uint64_t)(unsigned char)var_96 | 6473984UL) != '\x00')
                                            {
                                                *var_41 = (*var_41 + 1UL);
                                                var_94 = (uint64_t)(uint32_t)(uint64_t)**(unsigned char **)var_40;
                                                var_95 = local_sp_1 + (-8L);
                                                *(uint64_t *)var_95 = 4216122UL;
                                                var_96 = indirect_placeholder_30(var_94);
                                                local_sp_1 = var_95;
                                                local_sp_2 = var_95;
                                            }
                                    }
                                    *(uint64_t *)(*var_38 + 16UL) = *var_41;
                                    local_sp_3 = local_sp_2;
                                }
                            }
                            *var_41 = *var_35;
                            local_sp_5 = local_sp_3;
                            var_75 = *var_44;
                            var_76 = *var_35;
                            var_77 = var_75 - var_76;
                            var_78 = (uint64_t)(uint32_t)(uint64_t)*var_24;
                            var_79 = local_sp_5 + (-8L);
                            *(uint64_t *)var_79 = 4216193UL;
                            var_80 = indirect_placeholder_107(var_77, var_76, var_78);
                            var_81 = var_80.field_0;
                            *var_46 = var_81;
                            local_sp_0 = var_79;
                            local_sp_3 = var_79;
                            local_sp_6 = var_79;
                            local_sp_7 = var_79;
                        }
                    var_97 = *var_44;
                    *var_35 = var_97;
                    var_98 = *var_17;
                    var_99 = var_98;
                    var_100 = var_97;
                    if (*(unsigned char *)(var_98 + 48UL) != '\x00') {
                        loop_state_var = 1U;
                        break;
                    }
                    rdi2_0 = var_80.field_1;
                    rsi3_0 = var_80.field_2;
                    continue;
                }
            switch_state_var = 0;
            switch (loop_state_var) {
              case 2U:
                {
                    switch_state_var = 1;
                    break;
                }
                break;
              case 1U:
              case 0U:
                {
                    switch (loop_state_var) {
                      case 1U:
                        {
                        }
                        break;
                      case 0U:
                        {
                            var_99 = *var_17;
                            var_100 = *var_35;
                        }
                        break;
                    }
                    *(uint64_t *)(var_99 + 8UL) = (var_100 - **var_49);
                    var_101 = *var_17;
                    *(uint64_t *)(local_sp_7 + (-8L)) = 4216288UL;
                    var_102 = indirect_placeholder_108(var_101);
                    *(uint64_t *)(*var_17 + 16UL) = (uint64_t)((long)(var_102.field_0 - *var_38) >> (long)5UL);
                    var_103 = *var_17;
                    state_0x9080_2 = state_0x9080_1;
                    state_0x9018_2 = state_0x9018_1;
                    state_0x9010_2 = state_0x9010_1;
                    state_0x8248_2 = state_0x8248_1;
                    state_0x82d8_2 = state_0x82d8_1;
                    if (*(uint64_t *)(var_103 + 16UL) != 0UL) {
                        *(uint64_t *)(var_103 + 32UL) = (*var_35 - *var_41);
                        *(uint64_t *)6473472UL = (*var_28 + 34UL);
                        switch_state_var = 1;
                        break;
                    }
                    *var_48 = (*(uint64_t *)(var_103 + 24UL) >> 5UL);
                    var_104 = **var_49;
                    var_105 = local_sp_7 + (-16L);
                    *(uint64_t *)var_105 = 4216419UL;
                    var_106 = indirect_placeholder_95(var_1, var_2, var_3, 32UL, var_47, var_6, var_104, var_47);
                    **var_49 = var_106;
                    *(uint64_t *)(*var_17 + 24UL) = (*var_48 << 5UL);
                    local_sp_8 = var_105;
                    continue;
                }
                break;
            }
            if (switch_state_var)
                break;
        }
    return rax_2;
}

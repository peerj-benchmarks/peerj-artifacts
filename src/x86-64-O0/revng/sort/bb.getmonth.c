typedef unsigned char __u_char;
typedef unsigned short __u_short;
typedef unsigned int __u_int;
typedef unsigned long __u_long;
typedef signed char __int8_t;
typedef unsigned char __uint8_t;
typedef short __int16_t;
typedef unsigned short __uint16_t;
typedef int __int32_t;
typedef unsigned int __uint32_t;
typedef long __int64_t;
typedef unsigned long __uint64_t;
typedef __int8_t __int_least8_t;
typedef __uint8_t __uint_least8_t;
typedef __int16_t __int_least16_t;
typedef __uint16_t __uint_least16_t;
typedef __int32_t __int_least32_t;
typedef __uint32_t __uint_least32_t;
typedef __int64_t __int_least64_t;
typedef __uint64_t __uint_least64_t;
typedef long __quad_t;
typedef unsigned long __u_quad_t;
typedef long __intmax_t;
typedef unsigned long __uintmax_t;
typedef unsigned long __dev_t;
typedef unsigned int __uid_t;
typedef unsigned int __gid_t;
typedef unsigned long __ino_t;
typedef unsigned long __ino64_t;
typedef unsigned int __mode_t;
typedef unsigned long __nlink_t;
typedef long __off_t;
typedef long __off64_t;
typedef int __pid_t;
typedef struct {
    int __val[2];
} __fsid_t;
typedef long __clock_t;
typedef unsigned long __rlim_t;
typedef unsigned long __rlim64_t;
typedef unsigned int __id_t;
typedef long __time_t;
typedef unsigned int __useconds_t;
typedef long __suseconds_t;
typedef int __daddr_t;
typedef int __key_t;
typedef int __clockid_t;
typedef void *__timer_t;
typedef long __blksize_t;
typedef long __blkcnt_t;
typedef long __blkcnt64_t;
typedef unsigned long __fsblkcnt_t;
typedef unsigned long __fsblkcnt64_t;
typedef unsigned long __fsfilcnt_t;
typedef unsigned long __fsfilcnt64_t;
typedef long __fsword_t;
typedef long __ssize_t;
typedef long __syscall_slong_t;
typedef unsigned long __syscall_ulong_t;
typedef __off64_t __loff_t;
typedef char *__caddr_t;
typedef long __intptr_t;
typedef unsigned int __socklen_t;
typedef int __sig_atomic_t;
typedef __int8_t int8_t;
typedef __int16_t int16_t;
typedef __int32_t int32_t;
typedef __int64_t int64_t;
typedef __uint8_t uint8_t;
typedef __uint16_t uint16_t;
typedef __uint32_t uint32_t;
typedef __uint64_t uint64_t;
typedef __int_least8_t int_least8_t;
typedef __int_least16_t int_least16_t;
typedef __int_least32_t int_least32_t;
typedef __int_least64_t int_least64_t;
typedef __uint_least8_t uint_least8_t;
typedef __uint_least16_t uint_least16_t;
typedef __uint_least32_t uint_least32_t;
typedef __uint_least64_t uint_least64_t;
typedef signed char int_fast8_t;
typedef long int_fast16_t;
typedef long int_fast32_t;
typedef long int_fast64_t;
typedef unsigned char uint_fast8_t;
typedef unsigned long uint_fast16_t;
typedef unsigned long uint_fast32_t;
typedef unsigned long uint_fast64_t;
typedef long intptr_t;
typedef unsigned long uintptr_t;
typedef __intmax_t intmax_t;
typedef __uintmax_t uintmax_t;
extern uint64_t revng_init_local_sp(uint64_t param_0);
extern uint64_t init_rbp(void);
extern uint64_t init_cc_src2(void);
extern uint64_t helper_cc_compute_c_wrapper(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint32_t param_3);
extern uint64_t init_rbx(void);
extern uint64_t indirect_placeholder_30(uint64_t param_0);
typedef _Bool bool;
uint64_t bb_getmonth(uint64_t rdi, uint64_t rsi) {
    uint64_t storemerge;
    uint64_t var_0;
    uint64_t var_1;
    uint64_t var_2;
    uint64_t var_3;
    uint64_t var_4;
    uint64_t var_5;
    uint64_t *var_6;
    uint64_t *var_7;
    uint64_t *var_8;
    uint64_t *var_9;
    uint64_t local_sp_3;
    uint64_t var_37;
    uint64_t var_38;
    uint64_t local_sp_2;
    uint64_t var_29;
    uint64_t var_39;
    uint64_t local_sp_0;
    uint64_t var_40;
    uint64_t local_sp_1;
    uint64_t var_30;
    uint64_t var_31;
    uint64_t var_32;
    uint64_t var_33;
    uint64_t var_34;
    uint64_t var_35;
    uint64_t var_36;
    uint64_t *var_13;
    uint64_t var_14;
    uint64_t *var_15;
    uint64_t var_16;
    uint64_t *var_17;
    unsigned char **var_18;
    unsigned char **var_19;
    uint64_t var_20;
    uint64_t var_21;
    uint64_t var_22;
    uint64_t var_23;
    uint64_t var_24;
    uint64_t var_25;
    uint64_t var_26;
    uint64_t var_27;
    uint64_t var_28;
    uint64_t var_10;
    uint64_t var_11;
    uint64_t var_12;
    unsigned int loop_state_var;
    bool switch_state_var;
    var_0 = revng_init_local_sp(0UL);
    var_1 = init_rbx();
    var_2 = init_rbp();
    var_3 = init_cc_src2();
    *(uint64_t *)(var_0 + (-8L)) = var_2;
    *(uint64_t *)(var_0 + (-16L)) = var_1;
    var_4 = var_0 + (-80L);
    var_5 = var_0 + (-72L);
    var_6 = (uint64_t *)var_5;
    *var_6 = rdi;
    var_7 = (uint64_t *)var_4;
    *var_7 = rsi;
    var_8 = (uint64_t *)(var_0 + (-24L));
    *var_8 = 0UL;
    var_9 = (uint64_t *)(var_0 + (-32L));
    *var_9 = 12UL;
    storemerge = 0UL;
    local_sp_3 = var_4;
    var_10 = (uint64_t)(uint32_t)(uint64_t)**(unsigned char **)var_5;
    var_11 = local_sp_3 + (-8L);
    *(uint64_t *)var_11 = 4217613UL;
    var_12 = indirect_placeholder_30(var_10);
    local_sp_1 = var_11;
    local_sp_3 = var_11;
    while (*(unsigned char *)((uint64_t)(unsigned char)var_12 | 6473984UL) != '\x00')
        {
            *var_6 = (*var_6 + 1UL);
            var_10 = (uint64_t)(uint32_t)(uint64_t)**(unsigned char **)var_5;
            var_11 = local_sp_3 + (-8L);
            *(uint64_t *)var_11 = 4217613UL;
            var_12 = indirect_placeholder_30(var_10);
            local_sp_1 = var_11;
            local_sp_3 = var_11;
        }
    var_13 = (uint64_t *)(var_0 + (-56L));
    var_14 = var_0 + (-40L);
    var_15 = (uint64_t *)var_14;
    var_16 = var_0 + (-48L);
    var_17 = (uint64_t *)var_16;
    var_18 = (unsigned char **)var_16;
    var_19 = (unsigned char **)var_14;
    while (1U)
        {
            *var_13 = ((*var_9 + *var_8) >> 1UL);
            *var_15 = *var_6;
            *var_17 = *(uint64_t *)((*var_13 << 4UL) + 6473280UL);
            local_sp_2 = local_sp_1;
            while (1U)
                {
                    if (**var_18 != '\x00') {
                        if (*var_7 != 0UL) {
                            loop_state_var = 0U;
                            break;
                        }
                        **(uint64_t **)var_4 = *var_15;
                        loop_state_var = 0U;
                        break;
                    }
                    var_20 = (uint64_t)(uint32_t)(uint64_t)**var_19;
                    *(uint64_t *)(local_sp_2 + (-8L)) = 4217744UL;
                    var_21 = indirect_placeholder_30(var_20);
                    var_22 = (uint64_t)(uint32_t)(uint64_t)*(unsigned char *)((uint64_t)(unsigned char)var_21 | 6474752UL);
                    *(uint64_t *)(local_sp_2 + (-16L)) = 4217766UL;
                    var_23 = indirect_placeholder_30(var_22);
                    var_24 = (uint64_t)(uint32_t)var_23;
                    var_25 = (uint64_t)(uint32_t)(uint64_t)**var_18;
                    var_26 = local_sp_2 + (-24L);
                    *(uint64_t *)var_26 = 4217785UL;
                    var_27 = indirect_placeholder_30(var_25);
                    var_28 = helper_cc_compute_c_wrapper(var_24 - var_27, var_27, var_3, 14U);
                    local_sp_0 = var_26;
                    if (var_28 != 0UL) {
                        loop_state_var = 2U;
                        break;
                    }
                    var_30 = (uint64_t)(uint32_t)(uint64_t)**var_19;
                    *(uint64_t *)(local_sp_2 + (-32L)) = 4217816UL;
                    var_31 = indirect_placeholder_30(var_30);
                    var_32 = (uint64_t)(uint32_t)(uint64_t)*(unsigned char *)((uint64_t)(unsigned char)var_31 | 6474752UL);
                    *(uint64_t *)(local_sp_2 + (-40L)) = 4217838UL;
                    var_33 = indirect_placeholder_30(var_32);
                    var_34 = (uint64_t)(uint32_t)(uint64_t)**var_18;
                    var_35 = local_sp_2 + (-48L);
                    *(uint64_t *)var_35 = 4217857UL;
                    var_36 = indirect_placeholder_30(var_34);
                    local_sp_0 = var_35;
                    local_sp_2 = var_35;
                    if ((uint64_t)(unsigned char)var_33 <= (uint64_t)(unsigned char)var_36) {
                        loop_state_var = 1U;
                        break;
                    }
                    *var_15 = (*var_15 + 1UL);
                    *var_17 = (*var_17 + 1UL);
                    continue;
                }
            switch_state_var = 0;
            switch (loop_state_var) {
              case 0U:
                {
                    storemerge = (uint64_t)*(uint32_t *)((*var_13 << 4UL) + 6473288UL);
                    switch_state_var = 1;
                    break;
                }
                break;
              case 2U:
              case 1U:
                {
                    switch (loop_state_var) {
                      case 1U:
                        {
                            var_37 = *var_13 + 1UL;
                            *var_8 = var_37;
                            var_38 = *var_9;
                            var_39 = var_37;
                        }
                        break;
                      case 2U:
                        {
                            var_29 = *var_13;
                            *var_9 = var_29;
                            var_38 = var_29;
                            var_39 = *var_8;
                        }
                        break;
                    }
                    var_40 = helper_cc_compute_c_wrapper(var_39 - var_38, var_38, var_3, 17U);
                    local_sp_1 = local_sp_0;
                    if (var_40 != 0UL) {
                        continue;
                    }
                    switch_state_var = 1;
                    break;
                }
                break;
            }
            if (switch_state_var)
                break;
        }
    return storemerge;
}

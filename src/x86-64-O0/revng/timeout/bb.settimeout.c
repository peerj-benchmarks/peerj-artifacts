typedef unsigned char __u_char;
typedef unsigned short __u_short;
typedef unsigned int __u_int;
typedef unsigned long __u_long;
typedef signed char __int8_t;
typedef unsigned char __uint8_t;
typedef short __int16_t;
typedef unsigned short __uint16_t;
typedef int __int32_t;
typedef unsigned int __uint32_t;
typedef long __int64_t;
typedef unsigned long __uint64_t;
typedef __int8_t __int_least8_t;
typedef __uint8_t __uint_least8_t;
typedef __int16_t __int_least16_t;
typedef __uint16_t __uint_least16_t;
typedef __int32_t __int_least32_t;
typedef __uint32_t __uint_least32_t;
typedef __int64_t __int_least64_t;
typedef __uint64_t __uint_least64_t;
typedef long __quad_t;
typedef unsigned long __u_quad_t;
typedef long __intmax_t;
typedef unsigned long __uintmax_t;
typedef unsigned long __dev_t;
typedef unsigned int __uid_t;
typedef unsigned int __gid_t;
typedef unsigned long __ino_t;
typedef unsigned long __ino64_t;
typedef unsigned int __mode_t;
typedef unsigned long __nlink_t;
typedef long __off_t;
typedef long __off64_t;
typedef int __pid_t;
typedef struct {
    int __val[2];
} __fsid_t;
typedef long __clock_t;
typedef unsigned long __rlim_t;
typedef unsigned long __rlim64_t;
typedef unsigned int __id_t;
typedef long __time_t;
typedef unsigned int __useconds_t;
typedef long __suseconds_t;
typedef int __daddr_t;
typedef int __key_t;
typedef int __clockid_t;
typedef void *__timer_t;
typedef long __blksize_t;
typedef long __blkcnt_t;
typedef long __blkcnt64_t;
typedef unsigned long __fsblkcnt_t;
typedef unsigned long __fsblkcnt64_t;
typedef unsigned long __fsfilcnt_t;
typedef unsigned long __fsfilcnt64_t;
typedef long __fsword_t;
typedef long __ssize_t;
typedef long __syscall_slong_t;
typedef unsigned long __syscall_ulong_t;
typedef __off64_t __loff_t;
typedef char *__caddr_t;
typedef long __intptr_t;
typedef unsigned int __socklen_t;
typedef int __sig_atomic_t;
typedef __int8_t int8_t;
typedef __int16_t int16_t;
typedef __int32_t int32_t;
typedef __int64_t int64_t;
typedef __uint8_t uint8_t;
typedef __uint16_t uint16_t;
typedef __uint32_t uint32_t;
typedef __uint64_t uint64_t;
typedef __int_least8_t int_least8_t;
typedef __int_least16_t int_least16_t;
typedef __int_least32_t int_least32_t;
typedef __int_least64_t int_least64_t;
typedef __uint_least8_t uint_least8_t;
typedef __uint_least16_t uint_least16_t;
typedef __uint_least32_t uint_least32_t;
typedef __uint_least64_t uint_least64_t;
typedef signed char int_fast8_t;
typedef long int_fast16_t;
typedef long int_fast32_t;
typedef long int_fast64_t;
typedef unsigned char uint_fast8_t;
typedef unsigned long uint_fast16_t;
typedef unsigned long uint_fast32_t;
typedef unsigned long uint_fast64_t;
typedef long intptr_t;
typedef unsigned long uintptr_t;
typedef __intmax_t intmax_t;
typedef __uintmax_t uintmax_t;
struct helper_pxor_xmm_wrapper_56_ret_type;
struct type_3;
struct type_5;
struct helper_ucomisd_wrapper_ret_type;
struct helper_ucomisd_wrapper_55_ret_type;
struct helper_cvtsq2sd_wrapper_ret_type;
struct indirect_placeholder_11_ret_type;
struct helper_cvttsd2sq_wrapper_ret_type;
struct helper_pxor_xmm_wrapper_56_ret_type {
    uint64_t field_0;
    uint64_t field_1;
};
struct type_3 {
};
struct type_5 {
};
struct helper_ucomisd_wrapper_ret_type {
    uint64_t field_0;
    unsigned char field_1;
};
struct helper_ucomisd_wrapper_55_ret_type {
    uint64_t field_0;
    unsigned char field_1;
};
struct helper_cvtsq2sd_wrapper_ret_type {
    uint64_t field_0;
    unsigned char field_1;
};
struct indirect_placeholder_11_ret_type {
    uint64_t field_0;
    uint64_t field_1;
};
struct helper_cvttsd2sq_wrapper_ret_type {
    uint64_t field_0;
    unsigned char field_1;
};
extern uint64_t revng_init_local_sp(uint64_t param_0);
extern uint64_t init_rbp(void);
extern void indirect_placeholder(void);
extern struct helper_pxor_xmm_wrapper_56_ret_type helper_pxor_xmm_wrapper_56(struct type_3 *param_0, struct type_5 *param_1, struct type_5 *param_2, uint64_t param_3, uint64_t param_4);
extern struct helper_ucomisd_wrapper_ret_type helper_ucomisd_wrapper(struct type_3 *param_0, struct type_5 *param_1, struct type_5 *param_2, uint64_t param_3, uint64_t param_4, unsigned char param_5, unsigned char param_6);
extern uint64_t init_cc_src2(void);
extern uint64_t helper_cc_compute_c_wrapper(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint32_t param_3);
extern uint64_t init_state_0x8558(void);
extern unsigned char init_state_0x8549(void);
extern unsigned char init_state_0x854c(void);
extern unsigned char init_state_0x8548(void);
extern unsigned char init_state_0x854b(void);
extern unsigned char init_state_0x8547(void);
extern struct helper_ucomisd_wrapper_55_ret_type helper_ucomisd_wrapper_55(struct type_3 *param_0, struct type_5 *param_1, struct type_5 *param_2, uint64_t param_3, uint64_t param_4, unsigned char param_5, unsigned char param_6);
extern struct helper_cvtsq2sd_wrapper_ret_type helper_cvtsq2sd_wrapper(struct type_3 *param_0, struct type_5 *param_1, uint64_t param_2, unsigned char param_3, unsigned char param_4, unsigned char param_5, unsigned char param_6);
extern uint64_t init_r9(void);
extern uint64_t init_r8(void);
extern void indirect_placeholder_2(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint64_t param_3, uint64_t param_4, uint64_t param_5, uint64_t param_6);
extern uint64_t init_rcx(void);
extern struct indirect_placeholder_11_ret_type indirect_placeholder_11(void);
extern struct helper_cvttsd2sq_wrapper_ret_type helper_cvttsd2sq_wrapper(struct type_3 *param_0, struct type_5 *param_1, uint64_t param_2, unsigned char param_3, unsigned char param_4);
void bb_settimeout(uint64_t rdi) {
    struct helper_cvttsd2sq_wrapper_ret_type var_35;
    uint64_t var_0;
    uint64_t var_1;
    uint64_t var_2;
    uint64_t var_3;
    uint64_t var_4;
    uint64_t var_5;
    uint64_t var_6;
    unsigned char var_7;
    unsigned char var_8;
    unsigned char var_9;
    unsigned char var_10;
    unsigned char var_11;
    uint64_t *var_12;
    unsigned char *var_13;
    uint64_t var_14;
    uint64_t *var_15;
    struct indirect_placeholder_11_ret_type var_16;
    uint64_t var_17;
    uint64_t var_18;
    uint64_t *var_19;
    uint64_t *var_20;
    uint64_t var_21;
    uint64_t var_22;
    uint64_t var_23;
    unsigned char var_24;
    uint64_t cc_dst_0;
    uint64_t var_28;
    uint64_t var_29;
    uint64_t var_25;
    uint64_t var_26;
    uint64_t var_27;
    uint64_t local_sp_1;
    uint64_t var_30;
    struct helper_ucomisd_wrapper_55_ret_type var_31;
    uint64_t var_32;
    unsigned char var_33;
    uint64_t var_34;
    uint64_t var_36;
    unsigned char var_37;
    uint32_t *var_38;
    uint32_t var_39;
    uint64_t var_40;
    struct helper_cvtsq2sd_wrapper_ret_type var_41;
    struct helper_ucomisd_wrapper_ret_type var_42;
    var_0 = revng_init_local_sp(0UL);
    var_1 = init_state_0x8558();
    var_2 = init_rbp();
    var_3 = init_cc_src2();
    var_4 = init_rcx();
    var_5 = init_r9();
    var_6 = init_r8();
    var_7 = init_state_0x8549();
    var_8 = init_state_0x854c();
    var_9 = init_state_0x8548();
    var_10 = init_state_0x854b();
    var_11 = init_state_0x8547();
    *(uint64_t *)(var_0 + (-8L)) = var_2;
    var_12 = (uint64_t *)(var_0 + (-96L));
    *var_12 = var_1;
    var_13 = (unsigned char *)(var_0 + (-100L));
    *var_13 = (unsigned char)rdi;
    var_14 = *var_12;
    var_15 = (uint64_t *)(var_0 + (-112L));
    *var_15 = var_14;
    *(uint64_t *)(var_0 + (-128L)) = 4203072UL;
    var_16 = indirect_placeholder_11();
    var_17 = var_16.field_0;
    var_18 = var_16.field_1;
    var_19 = (uint64_t *)(var_0 + (-40L));
    *var_19 = var_17;
    var_20 = (uint64_t *)(var_0 + (-32L));
    *var_20 = var_18;
    *(uint64_t *)(var_0 + (-72L)) = 0UL;
    *(uint64_t *)(var_0 + (-64L)) = 0UL;
    var_21 = *var_20;
    *(uint64_t *)(var_0 + (-56L)) = *var_19;
    *(uint64_t *)(var_0 + (-48L)) = var_21;
    var_22 = var_0 + (-80L);
    var_23 = var_0 + (-136L);
    *(uint64_t *)var_23 = 4203134UL;
    indirect_placeholder();
    var_24 = *var_13;
    cc_dst_0 = (uint64_t)var_24;
    local_sp_1 = var_23;
    var_25 = var_0 + (-144L);
    *(uint64_t *)var_25 = 4203232UL;
    indirect_placeholder();
    var_26 = (uint64_t)*(uint32_t *)var_22;
    var_27 = var_26 + (-38L);
    cc_dst_0 = var_27;
    local_sp_1 = var_25;
    if (var_24 != '\x00' & (uint64_t)(uint32_t)var_27 == 0UL) {
        *(uint64_t *)(var_0 + (-152L)) = 4203244UL;
        indirect_placeholder();
        var_28 = (uint64_t)*(uint32_t *)var_26;
        var_29 = var_0 + (-160L);
        *(uint64_t *)var_29 = 4203268UL;
        indirect_placeholder_2(0UL, 4272279UL, var_4, var_28, 0UL, var_5, var_6);
        local_sp_1 = var_29;
    }
    var_30 = *var_12;
    var_31 = helper_ucomisd_wrapper_55((struct type_3 *)(0UL), (struct type_5 *)(776UL), (struct type_5 *)(2824UL), var_30, *(uint64_t *)4274256UL, var_7, var_8);
    var_32 = helper_cc_compute_c_wrapper(cc_dst_0, var_31.field_0, var_3, 1U);
    if (var_32 == 0UL) {
        *(uint32_t *)(var_0 + (-12L)) = 4294967295U;
    } else {
        var_33 = var_31.field_1;
        var_34 = *var_12;
        *var_15 = var_34;
        var_35 = helper_cvttsd2sq_wrapper((struct type_3 *)(0UL), (struct type_5 *)(2824UL), var_34, var_33, var_8);
        var_36 = var_35.field_0;
        var_37 = var_35.field_1;
        var_38 = (uint32_t *)(var_0 + (-16L));
        var_39 = (uint32_t)var_36;
        *var_38 = var_39;
        var_40 = (uint64_t)var_39;
        helper_pxor_xmm_wrapper_56((struct type_3 *)(0UL), (struct type_5 *)(776UL), (struct type_5 *)(776UL), var_30, 0UL);
        var_41 = helper_cvtsq2sd_wrapper((struct type_3 *)(0UL), (struct type_5 *)(776UL), var_40, var_37, var_9, var_10, var_11);
        var_42 = helper_ucomisd_wrapper((struct type_3 *)(0UL), (struct type_5 *)(840UL), (struct type_5 *)(776UL), var_41.field_0, *var_12, var_41.field_1, var_8);
        *(uint32_t *)(var_0 + (-12L)) = (*var_38 + ((var_42.field_0 & 65UL) == 0UL));
    }
    *(uint64_t *)(local_sp_1 + (-8L)) = 4203386UL;
    indirect_placeholder();
    return;
}

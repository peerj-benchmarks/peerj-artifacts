typedef unsigned char __u_char;
typedef unsigned short __u_short;
typedef unsigned int __u_int;
typedef unsigned long __u_long;
typedef signed char __int8_t;
typedef unsigned char __uint8_t;
typedef short __int16_t;
typedef unsigned short __uint16_t;
typedef int __int32_t;
typedef unsigned int __uint32_t;
typedef long __int64_t;
typedef unsigned long __uint64_t;
typedef __int8_t __int_least8_t;
typedef __uint8_t __uint_least8_t;
typedef __int16_t __int_least16_t;
typedef __uint16_t __uint_least16_t;
typedef __int32_t __int_least32_t;
typedef __uint32_t __uint_least32_t;
typedef __int64_t __int_least64_t;
typedef __uint64_t __uint_least64_t;
typedef long __quad_t;
typedef unsigned long __u_quad_t;
typedef long __intmax_t;
typedef unsigned long __uintmax_t;
typedef unsigned long __dev_t;
typedef unsigned int __uid_t;
typedef unsigned int __gid_t;
typedef unsigned long __ino_t;
typedef unsigned long __ino64_t;
typedef unsigned int __mode_t;
typedef unsigned long __nlink_t;
typedef long __off_t;
typedef long __off64_t;
typedef int __pid_t;
typedef struct {
    int __val[2];
} __fsid_t;
typedef long __clock_t;
typedef unsigned long __rlim_t;
typedef unsigned long __rlim64_t;
typedef unsigned int __id_t;
typedef long __time_t;
typedef unsigned int __useconds_t;
typedef long __suseconds_t;
typedef int __daddr_t;
typedef int __key_t;
typedef int __clockid_t;
typedef void *__timer_t;
typedef long __blksize_t;
typedef long __blkcnt_t;
typedef long __blkcnt64_t;
typedef unsigned long __fsblkcnt_t;
typedef unsigned long __fsblkcnt64_t;
typedef unsigned long __fsfilcnt_t;
typedef unsigned long __fsfilcnt64_t;
typedef long __fsword_t;
typedef long __ssize_t;
typedef long __syscall_slong_t;
typedef unsigned long __syscall_ulong_t;
typedef __off64_t __loff_t;
typedef char *__caddr_t;
typedef long __intptr_t;
typedef unsigned int __socklen_t;
typedef int __sig_atomic_t;
typedef __int8_t int8_t;
typedef __int16_t int16_t;
typedef __int32_t int32_t;
typedef __int64_t int64_t;
typedef __uint8_t uint8_t;
typedef __uint16_t uint16_t;
typedef __uint32_t uint32_t;
typedef __uint64_t uint64_t;
typedef __int_least8_t int_least8_t;
typedef __int_least16_t int_least16_t;
typedef __int_least32_t int_least32_t;
typedef __int_least64_t int_least64_t;
typedef __uint_least8_t uint_least8_t;
typedef __uint_least16_t uint_least16_t;
typedef __uint_least32_t uint_least32_t;
typedef __uint_least64_t uint_least64_t;
typedef signed char int_fast8_t;
typedef long int_fast16_t;
typedef long int_fast32_t;
typedef long int_fast64_t;
typedef unsigned char uint_fast8_t;
typedef unsigned long uint_fast16_t;
typedef unsigned long uint_fast32_t;
typedef unsigned long uint_fast64_t;
typedef long intptr_t;
typedef unsigned long uintptr_t;
typedef __intmax_t intmax_t;
typedef __uintmax_t uintmax_t;
typedef _Bool bool;
struct indirect_placeholder_37_ret_type;
struct indirect_placeholder_39_ret_type;
struct indirect_placeholder_37_ret_type {
    uint64_t field_0;
    uint64_t field_1;
    uint64_t field_2;
};
struct indirect_placeholder_39_ret_type {
    uint64_t field_0;
    uint64_t field_1;
};
extern void abort(void);
extern uint64_t revng_init_local_sp(uint64_t param_0);
extern uint64_t init_rbp(void);
extern void indirect_placeholder_1(uint64_t param_0, uint64_t param_1);
extern void indirect_placeholder(void);
extern uint64_t indirect_placeholder_12(void);
extern uint64_t indirect_placeholder_6(uint64_t param_0, uint64_t param_1);
extern uint64_t init_state_0x8558(void);
extern uint64_t init_rbx(void);
extern void indirect_placeholder_5(uint64_t param_0);
extern void indirect_placeholder_2(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint64_t param_3, uint64_t param_4, uint64_t param_5, uint64_t param_6);
extern struct indirect_placeholder_37_ret_type indirect_placeholder_37(uint64_t param_0);
extern void indirect_placeholder_38(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint64_t param_3, uint64_t param_4, uint64_t param_5);
extern struct indirect_placeholder_39_ret_type indirect_placeholder_39(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint64_t param_3, uint64_t param_4);
void bb_main(uint64_t rsi, uint64_t rdi) {
    uint64_t var_0;
    uint64_t var_1;
    uint64_t var_2;
    uint64_t var_3;
    uint64_t var_4;
    struct indirect_placeholder_39_ret_type var_16;
    uint32_t *var_48;
    uint32_t *var_5;
    uint64_t var_6;
    uint64_t *var_7;
    uint64_t **var_8;
    uint64_t var_9;
    uint64_t var_10;
    uint32_t *var_11;
    uint64_t var_12;
    uint64_t local_sp_4;
    uint64_t var_51;
    uint64_t var_42;
    uint64_t var_43;
    uint64_t var_44;
    uint32_t *var_45;
    uint32_t var_46;
    uint32_t var_47;
    uint32_t var_40;
    uint64_t local_sp_0;
    uint64_t var_49;
    uint64_t var_50;
    uint64_t var_52;
    uint64_t var_53;
    uint64_t local_sp_1;
    uint64_t var_41;
    uint64_t var_61;
    bool var_34;
    uint64_t *var_35;
    uint32_t *var_54;
    uint64_t var_55;
    struct indirect_placeholder_37_ret_type var_56;
    uint64_t var_57;
    uint64_t var_58;
    uint64_t var_59;
    uint64_t var_60;
    uint64_t var_36;
    uint64_t var_37;
    uint64_t var_38;
    uint32_t *var_39;
    uint64_t var_31;
    uint64_t local_sp_2;
    uint32_t var_32;
    uint64_t var_33;
    uint64_t var_26;
    uint32_t var_27;
    uint64_t var_28;
    uint64_t var_29;
    uint64_t *var_30;
    uint64_t local_sp_4_be;
    uint64_t local_sp_3;
    uint64_t var_18;
    uint64_t var_19;
    uint64_t var_20;
    uint64_t var_21;
    uint64_t var_22;
    uint64_t var_23;
    uint64_t var_24;
    uint32_t var_25;
    uint64_t var_13;
    uint64_t var_14;
    uint64_t var_15;
    uint32_t var_17;
    unsigned int loop_state_var;
    var_0 = revng_init_local_sp(0UL);
    var_1 = init_state_0x8558();
    var_2 = init_rbp();
    var_3 = init_rbx();
    var_4 = var_0 + (-8L);
    *(uint64_t *)var_4 = var_2;
    *(uint64_t *)(var_0 + (-16L)) = var_3;
    var_5 = (uint32_t *)(var_0 + (-220L));
    *var_5 = (uint32_t)rdi;
    var_6 = var_0 + (-232L);
    var_7 = (uint64_t *)var_6;
    *var_7 = rsi;
    var_8 = (uint64_t **)var_6;
    var_9 = **var_8;
    *(uint64_t *)(var_0 + (-256L)) = 4205138UL;
    indirect_placeholder_5(var_9);
    *(uint64_t *)(var_0 + (-264L)) = 4205153UL;
    indirect_placeholder();
    *(uint64_t *)(var_0 + (-272L)) = 4205163UL;
    indirect_placeholder_5(125UL);
    var_10 = var_0 + (-280L);
    *(uint64_t *)var_10 = 4205173UL;
    indirect_placeholder();
    var_11 = (uint32_t *)(var_0 + (-28L));
    var_12 = var_0 + (-72L);
    local_sp_4 = var_10;
    while (1U)
        {
            var_13 = *var_7;
            var_14 = (uint64_t)*var_5;
            var_15 = local_sp_4 + (-8L);
            *(uint64_t *)var_15 = 4205475UL;
            var_16 = indirect_placeholder_39(4274091UL, 4272000UL, var_13, var_14, 0UL);
            var_17 = (uint32_t)var_16.field_0;
            *var_11 = var_17;
            local_sp_4_be = var_15;
            local_sp_3 = var_15;
            if (var_17 != 4294967295U) {
                var_26 = var_16.field_1;
                var_27 = *(uint32_t *)6382936UL;
                if ((int)(*var_5 - var_27) > (int)1U) {
                    *(uint64_t *)(local_sp_4 + (-16L)) = 4205519UL;
                    indirect_placeholder_1(var_4, 125UL);
                    abort();
                }
                *(uint32_t *)6382936UL = (var_27 + 1U);
                var_28 = *(uint64_t *)(*var_7 + ((uint64_t)var_27 << 3UL));
                var_29 = local_sp_4 + (-16L);
                *(uint64_t *)var_29 = 4205565UL;
                indirect_placeholder_5(var_28);
                var_30 = (uint64_t *)(var_0 + (-40L));
                *var_30 = var_1;
                *var_7 = (*var_7 + ((uint64_t)*(uint32_t *)6382936UL << 3UL));
                *(uint64_t *)6383592UL = **var_8;
                local_sp_2 = var_29;
                if (*(unsigned char *)6383584UL != '\x01') {
                    loop_state_var = 1U;
                    break;
                }
                var_31 = local_sp_4 + (-24L);
                *(uint64_t *)var_31 = 4205639UL;
                indirect_placeholder();
                local_sp_2 = var_31;
                loop_state_var = 1U;
                break;
            }
            if ((uint64_t)(var_17 + (-115)) == 0UL) {
                var_22 = *(uint64_t *)6384088UL;
                var_23 = local_sp_4 + (-16L);
                *(uint64_t *)var_23 = 4205301UL;
                var_24 = indirect_placeholder_6(var_12, var_22);
                var_25 = (uint32_t)var_24;
                *(uint32_t *)6382784UL = var_25;
                local_sp_4_be = var_23;
                if ((uint64_t)(var_25 + 1U) == 0UL) {
                    *(uint64_t *)(local_sp_4 + (-24L)) = 4205328UL;
                    indirect_placeholder_1(var_4, 125UL);
                    abort();
                }
            }
            if ((int)var_17 > (int)115U) {
                if ((uint64_t)(var_17 + (-128)) == 0UL) {
                    *(unsigned char *)6383584UL = (unsigned char)'\x01';
                } else {
                    if ((uint64_t)(var_17 + (-129)) == 0UL) {
                        *(unsigned char *)6383585UL = (unsigned char)'\x01';
                    } else {
                        if ((uint64_t)(var_17 + (-118)) != 0UL) {
                            loop_state_var = 0U;
                            break;
                        }
                        *(unsigned char *)6383586UL = (unsigned char)'\x01';
                    }
                }
            } else {
                if ((uint64_t)(var_17 + 130U) == 0UL) {
                    *(uint64_t *)(local_sp_4 + (-16L)) = 4205367UL;
                    indirect_placeholder_1(var_4, 0UL);
                    abort();
                }
                if ((uint64_t)(var_17 + (-107)) != 0UL) {
                    if (var_17 != 4294967165U) {
                        loop_state_var = 0U;
                        break;
                    }
                    var_18 = *(uint64_t *)6382792UL;
                    *(uint64_t *)(local_sp_4 + (-16L)) = 4205419UL;
                    indirect_placeholder_38(0UL, 4271712UL, var_18, 4273983UL, 0UL, 4274077UL);
                    var_19 = local_sp_4 + (-24L);
                    *(uint64_t *)var_19 = 4205429UL;
                    indirect_placeholder();
                    local_sp_3 = var_19;
                    loop_state_var = 0U;
                    break;
                }
                var_20 = *(uint64_t *)6384088UL;
                var_21 = local_sp_4 + (-16L);
                *(uint64_t *)var_21 = 4205262UL;
                indirect_placeholder_5(var_20);
                *(uint64_t *)6383576UL = var_1;
                local_sp_4_be = var_21;
            }
            local_sp_4 = local_sp_4_be;
            continue;
        }
    switch (loop_state_var) {
      case 0U:
        {
            *(uint64_t *)(local_sp_3 + (-8L)) = 4205439UL;
            indirect_placeholder_1(var_4, 125UL);
            abort();
        }
        break;
      case 1U:
        {
            var_32 = *(uint32_t *)6382784UL;
            var_33 = (uint64_t)var_32;
            *(uint64_t *)(local_sp_2 + (-8L)) = 4205652UL;
            indirect_placeholder_5(var_33);
            *(uint64_t *)(local_sp_2 + (-16L)) = 4205667UL;
            indirect_placeholder();
            *(uint64_t *)(local_sp_2 + (-24L)) = 4205682UL;
            indirect_placeholder();
            *(uint64_t *)(local_sp_2 + (-32L)) = 4205687UL;
            indirect_placeholder();
            *(uint64_t *)(local_sp_2 + (-40L)) = 4205692UL;
            indirect_placeholder();
            *(uint32_t *)6383572UL = var_32;
            if ((uint64_t)(var_32 + 1U) == 0UL) {
                *(uint64_t *)(local_sp_2 + (-48L)) = 4205714UL;
                indirect_placeholder();
                var_61 = (uint64_t)*(uint32_t *)var_33;
                *(uint64_t *)(local_sp_2 + (-56L)) = 4205738UL;
                indirect_placeholder_2(0UL, 4274098UL, 4272000UL, var_61, 0UL, var_26, 0UL);
            } else {
                var_34 = (var_32 == 0U);
                var_35 = (uint64_t *)(local_sp_2 + (-48L));
                if (var_34) {
                    *var_35 = 4205777UL;
                    indirect_placeholder();
                    *(uint64_t *)(local_sp_2 + (-56L)) = 4205792UL;
                    indirect_placeholder();
                    var_54 = **(uint32_t ***)var_6;
                    *(uint64_t *)(local_sp_2 + (-64L)) = 4205820UL;
                    indirect_placeholder();
                    *(uint64_t *)(local_sp_2 + (-72L)) = 4205825UL;
                    indirect_placeholder();
                    *(uint32_t *)(var_0 + (-44L)) = ((*var_54 == 2U) ? 127U : 126U);
                    var_55 = *(uint64_t *)6383592UL;
                    *(uint64_t *)(local_sp_2 + (-80L)) = 4205862UL;
                    var_56 = indirect_placeholder_37(var_55);
                    var_57 = var_56.field_0;
                    var_58 = var_56.field_1;
                    var_59 = var_56.field_2;
                    *(uint64_t *)(local_sp_2 + (-88L)) = 4205870UL;
                    indirect_placeholder();
                    var_60 = (uint64_t)*(uint32_t *)var_57;
                    *(uint64_t *)(local_sp_2 + (-96L)) = 4205897UL;
                    indirect_placeholder_2(0UL, 4274122UL, var_57, var_60, 0UL, var_58, var_59);
                } else {
                    *var_35 = 4205915UL;
                    indirect_placeholder_5(14UL);
                    *(uint64_t *)(var_0 + (-240L)) = *var_30;
                    *(uint64_t *)(local_sp_2 + (-56L)) = 4205944UL;
                    indirect_placeholder_5(1UL);
                    var_36 = (uint64_t)*(uint32_t *)6382784UL;
                    var_37 = var_0 + (-216L);
                    var_38 = local_sp_2 + (-64L);
                    *(uint64_t *)var_38 = 4205967UL;
                    indirect_placeholder_1(var_37, var_36);
                    var_39 = (uint32_t *)(var_0 + (-48L));
                    local_sp_1 = var_38;
                    var_40 = *(uint32_t *)6383572UL;
                    var_41 = local_sp_1 + (-8L);
                    *(uint64_t *)var_41 = 4206009UL;
                    indirect_placeholder();
                    *var_39 = var_40;
                    local_sp_0 = var_41;
                    while (var_40 != 0U)
                        {
                            var_53 = local_sp_1 + (-16L);
                            *(uint64_t *)var_53 = 4205984UL;
                            indirect_placeholder();
                            local_sp_1 = var_53;
                            var_40 = *(uint32_t *)6383572UL;
                            var_41 = local_sp_1 + (-8L);
                            *(uint64_t *)var_41 = 4206009UL;
                            indirect_placeholder();
                            *var_39 = var_40;
                            local_sp_0 = var_41;
                        }
                    var_42 = var_0 + (-76L);
                    if ((int)var_40 > (int)4294967295U) {
                        var_43 = (uint64_t)var_40;
                        *(uint64_t *)(local_sp_1 + (-16L)) = 4206029UL;
                        indirect_placeholder();
                        var_44 = (uint64_t)*(uint32_t *)var_43;
                        *(uint64_t *)(local_sp_1 + (-24L)) = 4206053UL;
                        indirect_placeholder_2(0UL, 4274147UL, var_42, var_44, 0UL, var_26, 0UL);
                        *(uint32_t *)var_42 = 125U;
                    } else {
                        var_45 = (uint32_t *)var_42;
                        var_46 = *var_45;
                        var_47 = var_46 & 127U;
                        if (var_47 == 0U) {
                            *var_45 = (uint32_t)(unsigned char)(var_46 >> 8U);
                        } else {
                            if (((uint32_t)(uint16_t)var_46 + (-1)) > 254U) {
                                var_52 = (uint64_t)var_46;
                                *(uint64_t *)(local_sp_1 + (-16L)) = 4206251UL;
                                indirect_placeholder_2(0UL, 4274216UL, var_52, 0UL, 0UL, var_26, 0UL);
                                *var_45 = 1U;
                            } else {
                                var_48 = (uint32_t *)(var_0 + (-52L));
                                *var_48 = var_47;
                                if ((signed char)(unsigned char)*var_45 > '\xff') {
                                    var_49 = local_sp_1 + (-16L);
                                    *(uint64_t *)var_49 = 4206156UL;
                                    indirect_placeholder_2(0UL, 4274176UL, var_42, 0UL, 0UL, var_26, 0UL);
                                    local_sp_0 = var_49;
                                }
                                *(uint64_t *)(local_sp_0 + (-8L)) = 4206171UL;
                                var_50 = indirect_placeholder_12();
                                if (*(uint32_t *)6383568UL != 0U & (uint64_t)(unsigned char)var_50 == 0UL) {
                                    *(uint64_t *)(local_sp_0 + (-16L)) = 4206190UL;
                                    indirect_placeholder();
                                    var_51 = (uint64_t)*var_48;
                                    *(uint64_t *)(local_sp_0 + (-24L)) = 4206200UL;
                                    indirect_placeholder_5(var_51);
                                    *(uint64_t *)(local_sp_0 + (-32L)) = 4206210UL;
                                    indirect_placeholder();
                                }
                                *var_45 = (*var_48 + 128U);
                            }
                        }
                    }
                    if (*(uint32_t *)6383568UL != 0U & *(unsigned char *)6383585UL == '\x01') {
                        *(uint32_t *)var_42 = 124U;
                    }
                }
            }
            return;
        }
        break;
    }
}

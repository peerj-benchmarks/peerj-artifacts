typedef unsigned char __u_char;
typedef unsigned short __u_short;
typedef unsigned int __u_int;
typedef unsigned long __u_long;
typedef signed char __int8_t;
typedef unsigned char __uint8_t;
typedef short __int16_t;
typedef unsigned short __uint16_t;
typedef int __int32_t;
typedef unsigned int __uint32_t;
typedef long __int64_t;
typedef unsigned long __uint64_t;
typedef __int8_t __int_least8_t;
typedef __uint8_t __uint_least8_t;
typedef __int16_t __int_least16_t;
typedef __uint16_t __uint_least16_t;
typedef __int32_t __int_least32_t;
typedef __uint32_t __uint_least32_t;
typedef __int64_t __int_least64_t;
typedef __uint64_t __uint_least64_t;
typedef long __quad_t;
typedef unsigned long __u_quad_t;
typedef long __intmax_t;
typedef unsigned long __uintmax_t;
typedef unsigned long __dev_t;
typedef unsigned int __uid_t;
typedef unsigned int __gid_t;
typedef unsigned long __ino_t;
typedef unsigned long __ino64_t;
typedef unsigned int __mode_t;
typedef unsigned long __nlink_t;
typedef long __off_t;
typedef long __off64_t;
typedef int __pid_t;
typedef struct {
    int __val[2];
} __fsid_t;
typedef long __clock_t;
typedef unsigned long __rlim_t;
typedef unsigned long __rlim64_t;
typedef unsigned int __id_t;
typedef long __time_t;
typedef unsigned int __useconds_t;
typedef long __suseconds_t;
typedef int __daddr_t;
typedef int __key_t;
typedef int __clockid_t;
typedef void *__timer_t;
typedef long __blksize_t;
typedef long __blkcnt_t;
typedef long __blkcnt64_t;
typedef unsigned long __fsblkcnt_t;
typedef unsigned long __fsblkcnt64_t;
typedef unsigned long __fsfilcnt_t;
typedef unsigned long __fsfilcnt64_t;
typedef long __fsword_t;
typedef long __ssize_t;
typedef long __syscall_slong_t;
typedef unsigned long __syscall_ulong_t;
typedef __off64_t __loff_t;
typedef char *__caddr_t;
typedef long __intptr_t;
typedef unsigned int __socklen_t;
typedef int __sig_atomic_t;
typedef __int8_t int8_t;
typedef __int16_t int16_t;
typedef __int32_t int32_t;
typedef __int64_t int64_t;
typedef __uint8_t uint8_t;
typedef __uint16_t uint16_t;
typedef __uint32_t uint32_t;
typedef __uint64_t uint64_t;
typedef __int_least8_t int_least8_t;
typedef __int_least16_t int_least16_t;
typedef __int_least32_t int_least32_t;
typedef __int_least64_t int_least64_t;
typedef __uint_least8_t uint_least8_t;
typedef __uint_least16_t uint_least16_t;
typedef __uint_least32_t uint_least32_t;
typedef __uint_least64_t uint_least64_t;
typedef signed char int_fast8_t;
typedef long int_fast16_t;
typedef long int_fast32_t;
typedef long int_fast64_t;
typedef unsigned char uint_fast8_t;
typedef unsigned long uint_fast16_t;
typedef unsigned long uint_fast32_t;
typedef unsigned long uint_fast64_t;
typedef long intptr_t;
typedef unsigned long uintptr_t;
typedef __intmax_t intmax_t;
typedef __uintmax_t uintmax_t;
typedef _Bool bool;
extern void abort(void);
extern uint64_t revng_init_local_sp(uint64_t param_0);
extern void indirect_placeholder_3(void);
extern void indirect_placeholder_18(uint64_t param_0);
void bb_usage(uint64_t rbp, uint64_t rdi) {
    uint64_t var_0;
    uint32_t *var_1;
    uint32_t var_2;
    bool var_3;
    uint64_t var_4;
    uint64_t *var_5;
    uint64_t local_sp_0;
    uint64_t var_6;
    var_0 = revng_init_local_sp(0UL);
    *(uint64_t *)(var_0 + (-8L)) = rbp;
    var_1 = (uint32_t *)(var_0 + (-12L));
    var_2 = (uint32_t)rdi;
    *var_1 = var_2;
    var_3 = (var_2 == 0U);
    var_4 = var_0 + (-32L);
    var_5 = (uint64_t *)var_4;
    local_sp_0 = var_4;
    if (var_3) {
        *var_5 = 4202745UL;
        indirect_placeholder_3();
        *(uint64_t *)(var_0 + (-40L)) = 4202765UL;
        indirect_placeholder_3();
        *(uint64_t *)(var_0 + (-48L)) = 4202770UL;
        indirect_placeholder_3();
        *(uint64_t *)(var_0 + (-56L)) = 4202790UL;
        indirect_placeholder_3();
        *(uint64_t *)(var_0 + (-64L)) = 4202810UL;
        indirect_placeholder_3();
        *(uint64_t *)(var_0 + (-72L)) = 4202830UL;
        indirect_placeholder_3();
        *(uint64_t *)(var_0 + (-80L)) = 4202850UL;
        indirect_placeholder_3();
        *(uint64_t *)(var_0 + (-88L)) = 4202870UL;
        indirect_placeholder_3();
        *(uint64_t *)(var_0 + (-96L)) = 4202890UL;
        indirect_placeholder_3();
        *(uint64_t *)(var_0 + (-104L)) = 4202910UL;
        indirect_placeholder_3();
        *(uint64_t *)(var_0 + (-112L)) = 4202930UL;
        indirect_placeholder_3();
        *(uint64_t *)(var_0 + (-120L)) = 4202950UL;
        indirect_placeholder_3();
        *(uint64_t *)(var_0 + (-128L)) = 4202970UL;
        indirect_placeholder_3();
        *(uint64_t *)(var_0 + (-136L)) = 4202990UL;
        indirect_placeholder_3();
        *(uint64_t *)(var_0 + (-144L)) = 4203010UL;
        indirect_placeholder_3();
        *(uint64_t *)(var_0 + (-152L)) = 4203030UL;
        indirect_placeholder_3();
        *(uint64_t *)(var_0 + (-160L)) = 4203050UL;
        indirect_placeholder_3();
        *(uint64_t *)(var_0 + (-168L)) = 4203070UL;
        indirect_placeholder_3();
        *(uint64_t *)(var_0 + (-176L)) = 4203090UL;
        indirect_placeholder_3();
        *(uint64_t *)(var_0 + (-184L)) = 4203110UL;
        indirect_placeholder_3();
        *(uint64_t *)(var_0 + (-192L)) = 4203151UL;
        indirect_placeholder_3();
        *(uint64_t *)(var_0 + (-200L)) = 4203171UL;
        indirect_placeholder_3();
        *(uint64_t *)(var_0 + (-208L)) = 4203191UL;
        indirect_placeholder_3();
        *(uint64_t *)(var_0 + (-216L)) = 4203211UL;
        indirect_placeholder_3();
        *(uint64_t *)(var_0 + (-224L)) = 4203231UL;
        indirect_placeholder_3();
        *(uint64_t *)(var_0 + (-232L)) = 4203251UL;
        indirect_placeholder_3();
        *(uint64_t *)(var_0 + (-240L)) = 4203271UL;
        indirect_placeholder_3();
        *(uint64_t *)(var_0 + (-248L)) = 4203291UL;
        indirect_placeholder_3();
        *(uint64_t *)(var_0 + (-256L)) = 4203311UL;
        indirect_placeholder_3();
        *(uint64_t *)(var_0 + (-264L)) = 4203331UL;
        indirect_placeholder_3();
        *(uint64_t *)(var_0 + (-272L)) = 4203351UL;
        indirect_placeholder_3();
        *(uint64_t *)(var_0 + (-280L)) = 4203371UL;
        indirect_placeholder_3();
        *(uint64_t *)(var_0 + (-288L)) = 4203391UL;
        indirect_placeholder_3();
        *(uint64_t *)(var_0 + (-296L)) = 4203411UL;
        indirect_placeholder_3();
        *(uint64_t *)(var_0 + (-304L)) = 4203431UL;
        indirect_placeholder_3();
        *(uint64_t *)(var_0 + (-312L)) = 4203451UL;
        indirect_placeholder_3();
        *(uint64_t *)(var_0 + (-320L)) = 4203471UL;
        indirect_placeholder_3();
        *(uint64_t *)(var_0 + (-328L)) = 4203491UL;
        indirect_placeholder_3();
        *(uint64_t *)(var_0 + (-336L)) = 4203511UL;
        indirect_placeholder_3();
        *(uint64_t *)(var_0 + (-344L)) = 4203531UL;
        indirect_placeholder_3();
        *(uint64_t *)(var_0 + (-352L)) = 4203551UL;
        indirect_placeholder_3();
        *(uint64_t *)(var_0 + (-360L)) = 4203571UL;
        indirect_placeholder_3();
        *(uint64_t *)(var_0 + (-368L)) = 4203591UL;
        indirect_placeholder_3();
        *(uint64_t *)(var_0 + (-376L)) = 4203611UL;
        indirect_placeholder_3();
        *(uint64_t *)(var_0 + (-384L)) = 4203631UL;
        indirect_placeholder_3();
        *(uint64_t *)(var_0 + (-392L)) = 4203651UL;
        indirect_placeholder_3();
        *(uint64_t *)(var_0 + (-400L)) = 4203671UL;
        indirect_placeholder_3();
        *(uint64_t *)(var_0 + (-408L)) = 4203691UL;
        indirect_placeholder_3();
        *(uint64_t *)(var_0 + (-416L)) = 4203711UL;
        indirect_placeholder_3();
        *(uint64_t *)(var_0 + (-424L)) = 4203731UL;
        indirect_placeholder_3();
        *(uint64_t *)(var_0 + (-432L)) = 4203751UL;
        indirect_placeholder_3();
        *(uint64_t *)(var_0 + (-440L)) = 4203771UL;
        indirect_placeholder_3();
        *(uint64_t *)(var_0 + (-448L)) = 4203791UL;
        indirect_placeholder_3();
        *(uint64_t *)(var_0 + (-456L)) = 4203811UL;
        indirect_placeholder_3();
        *(uint64_t *)(var_0 + (-464L)) = 4203831UL;
        indirect_placeholder_3();
        *(uint64_t *)(var_0 + (-472L)) = 4203851UL;
        indirect_placeholder_3();
        *(uint64_t *)(var_0 + (-480L)) = 4203871UL;
        indirect_placeholder_3();
        *(uint64_t *)(var_0 + (-488L)) = 4203891UL;
        indirect_placeholder_3();
        *(uint64_t *)(var_0 + (-496L)) = 4203911UL;
        indirect_placeholder_3();
        *(uint64_t *)(var_0 + (-504L)) = 4203931UL;
        indirect_placeholder_3();
        *(uint64_t *)(var_0 + (-512L)) = 4203951UL;
        indirect_placeholder_3();
        *(uint64_t *)(var_0 + (-520L)) = 4203971UL;
        indirect_placeholder_3();
        *(uint64_t *)(var_0 + (-528L)) = 4203991UL;
        indirect_placeholder_3();
        *(uint64_t *)(var_0 + (-536L)) = 4204011UL;
        indirect_placeholder_3();
        *(uint64_t *)(var_0 + (-544L)) = 4204031UL;
        indirect_placeholder_3();
        *(uint64_t *)(var_0 + (-552L)) = 4204051UL;
        indirect_placeholder_3();
        *(uint64_t *)(var_0 + (-560L)) = 4204071UL;
        indirect_placeholder_3();
        *(uint64_t *)(var_0 + (-568L)) = 4204091UL;
        indirect_placeholder_3();
        *(uint64_t *)(var_0 + (-576L)) = 4204111UL;
        indirect_placeholder_3();
        *(uint64_t *)(var_0 + (-584L)) = 4204131UL;
        indirect_placeholder_3();
        *(uint64_t *)(var_0 + (-592L)) = 4204151UL;
        indirect_placeholder_3();
        *(uint64_t *)(var_0 + (-600L)) = 4204171UL;
        indirect_placeholder_3();
        *(uint64_t *)(var_0 + (-608L)) = 4204191UL;
        indirect_placeholder_3();
        *(uint64_t *)(var_0 + (-616L)) = 4204211UL;
        indirect_placeholder_3();
        *(uint64_t *)(var_0 + (-624L)) = 4204231UL;
        indirect_placeholder_3();
        *(uint64_t *)(var_0 + (-632L)) = 4204256UL;
        indirect_placeholder_3();
        *(uint64_t *)(var_0 + (-640L)) = 4204276UL;
        indirect_placeholder_3();
        *(uint64_t *)(var_0 + (-648L)) = 4204296UL;
        indirect_placeholder_3();
        *(uint64_t *)(var_0 + (-656L)) = 4204326UL;
        indirect_placeholder_3();
        *(uint64_t *)(var_0 + (-664L)) = 4204346UL;
        indirect_placeholder_3();
        var_6 = var_0 + (-672L);
        *(uint64_t *)var_6 = 4204356UL;
        indirect_placeholder_18(4293557UL);
        local_sp_0 = var_6;
    } else {
        *var_5 = 4202701UL;
        indirect_placeholder_3();
    }
    *(uint64_t *)(local_sp_0 + (-8L)) = 4204366UL;
    indirect_placeholder_3();
    abort();
}

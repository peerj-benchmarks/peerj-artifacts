typedef unsigned char __u_char;
typedef unsigned short __u_short;
typedef unsigned int __u_int;
typedef unsigned long __u_long;
typedef signed char __int8_t;
typedef unsigned char __uint8_t;
typedef short __int16_t;
typedef unsigned short __uint16_t;
typedef int __int32_t;
typedef unsigned int __uint32_t;
typedef long __int64_t;
typedef unsigned long __uint64_t;
typedef __int8_t __int_least8_t;
typedef __uint8_t __uint_least8_t;
typedef __int16_t __int_least16_t;
typedef __uint16_t __uint_least16_t;
typedef __int32_t __int_least32_t;
typedef __uint32_t __uint_least32_t;
typedef __int64_t __int_least64_t;
typedef __uint64_t __uint_least64_t;
typedef long __quad_t;
typedef unsigned long __u_quad_t;
typedef long __intmax_t;
typedef unsigned long __uintmax_t;
typedef unsigned long __dev_t;
typedef unsigned int __uid_t;
typedef unsigned int __gid_t;
typedef unsigned long __ino_t;
typedef unsigned long __ino64_t;
typedef unsigned int __mode_t;
typedef unsigned long __nlink_t;
typedef long __off_t;
typedef long __off64_t;
typedef int __pid_t;
typedef struct {
    int __val[2];
} __fsid_t;
typedef long __clock_t;
typedef unsigned long __rlim_t;
typedef unsigned long __rlim64_t;
typedef unsigned int __id_t;
typedef long __time_t;
typedef unsigned int __useconds_t;
typedef long __suseconds_t;
typedef int __daddr_t;
typedef int __key_t;
typedef int __clockid_t;
typedef void *__timer_t;
typedef long __blksize_t;
typedef long __blkcnt_t;
typedef long __blkcnt64_t;
typedef unsigned long __fsblkcnt_t;
typedef unsigned long __fsblkcnt64_t;
typedef unsigned long __fsfilcnt_t;
typedef unsigned long __fsfilcnt64_t;
typedef long __fsword_t;
typedef long __ssize_t;
typedef long __syscall_slong_t;
typedef unsigned long __syscall_ulong_t;
typedef __off64_t __loff_t;
typedef char *__caddr_t;
typedef long __intptr_t;
typedef unsigned int __socklen_t;
typedef int __sig_atomic_t;
typedef __int8_t int8_t;
typedef __int16_t int16_t;
typedef __int32_t int32_t;
typedef __int64_t int64_t;
typedef __uint8_t uint8_t;
typedef __uint16_t uint16_t;
typedef __uint32_t uint32_t;
typedef __uint64_t uint64_t;
typedef __int_least8_t int_least8_t;
typedef __int_least16_t int_least16_t;
typedef __int_least32_t int_least32_t;
typedef __int_least64_t int_least64_t;
typedef __uint_least8_t uint_least8_t;
typedef __uint_least16_t uint_least16_t;
typedef __uint_least32_t uint_least32_t;
typedef __uint_least64_t uint_least64_t;
typedef signed char int_fast8_t;
typedef long int_fast16_t;
typedef long int_fast32_t;
typedef long int_fast64_t;
typedef unsigned char uint_fast8_t;
typedef unsigned long uint_fast16_t;
typedef unsigned long uint_fast32_t;
typedef unsigned long uint_fast64_t;
typedef long intptr_t;
typedef unsigned long uintptr_t;
typedef __intmax_t intmax_t;
typedef __uintmax_t uintmax_t;
struct indirect_placeholder_88_ret_type;
struct indirect_placeholder_89_ret_type;
struct indirect_placeholder_88_ret_type {
    uint64_t field_0;
    uint64_t field_1;
    uint64_t field_2;
    uint64_t field_3;
};
struct indirect_placeholder_89_ret_type {
    uint64_t field_0;
    uint64_t field_1;
    uint64_t field_2;
    uint64_t field_3;
};
extern uint64_t revng_init_local_sp(uint64_t param_0);
extern uint64_t init_rbp(void);
extern uint64_t indirect_placeholder(uint64_t param_0, uint64_t param_1);
extern void indirect_placeholder_3(void);
extern uint64_t indirect_placeholder_8(uint64_t param_0);
extern void indirect_placeholder_6(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint64_t param_3, uint64_t param_4, uint64_t param_5, uint64_t param_6);
extern struct indirect_placeholder_88_ret_type indirect_placeholder_88(uint64_t param_0, uint64_t param_1);
extern struct indirect_placeholder_89_ret_type indirect_placeholder_89(uint64_t param_0, uint64_t param_1);
void bb_display_all(uint64_t rsi, uint64_t rdi) {
    uint64_t local_sp_4;
    uint64_t var_36;
    uint64_t var_37;
    uint64_t var_38;
    uint64_t var_39;
    uint64_t var_40;
    uint64_t var_41;
    uint64_t var_0;
    uint64_t var_1;
    uint64_t *var_2;
    uint64_t *var_3;
    uint32_t *var_4;
    uint64_t var_5;
    uint64_t var_6;
    struct indirect_placeholder_89_ret_type var_7;
    uint64_t var_8;
    uint64_t var_9;
    uint64_t var_10;
    uint64_t var_11;
    uint64_t var_12;
    uint64_t var_13;
    uint32_t *var_14;
    uint32_t var_15;
    uint64_t var_42;
    uint32_t *_pre_phi111;
    uint64_t var_43;
    uint64_t local_sp_0;
    uint64_t rcx_0;
    uint64_t var_44;
    uint64_t var_45;
    uint64_t var_48;
    uint64_t var_49;
    uint64_t local_sp_3;
    uint64_t var_46;
    uint64_t var_47;
    uint64_t rcx_4;
    uint64_t var_28;
    uint64_t local_sp_1;
    uint64_t var_29;
    uint64_t *var_30;
    uint64_t *var_31;
    uint32_t var_32;
    uint64_t rcx_2;
    uint64_t local_sp_2;
    uint64_t rcx_1;
    uint64_t var_33;
    uint64_t var_21;
    uint32_t var_50;
    uint32_t *var_34;
    uint64_t var_35;
    uint64_t storemerge;
    uint64_t var_24;
    uint64_t var_25;
    uint64_t var_26;
    uint64_t var_27;
    uint64_t var_17;
    uint64_t var_18;
    uint64_t local_sp_5;
    uint64_t rcx_3;
    uint32_t var_23;
    uint64_t var_19;
    uint64_t var_20;
    uint64_t var_22;
    uint64_t local_sp_6;
    uint64_t var_16;
    var_0 = revng_init_local_sp(0UL);
    var_1 = init_rbp();
    *(uint64_t *)(var_0 + (-8L)) = var_1;
    var_2 = (uint64_t *)(var_0 + (-48L));
    *var_2 = rdi;
    var_3 = (uint64_t *)(var_0 + (-56L));
    *var_3 = rsi;
    var_4 = (uint32_t *)(var_0 + (-16L));
    *var_4 = 0U;
    var_5 = *var_2;
    *(uint64_t *)(var_0 + (-64L)) = 4211638UL;
    indirect_placeholder_88(1UL, var_5);
    var_6 = *var_3;
    *(uint64_t *)(var_0 + (-72L)) = 4211655UL;
    var_7 = indirect_placeholder_89(var_6, 1UL);
    var_8 = var_7.field_0;
    var_9 = var_7.field_1;
    var_10 = var_7.field_2;
    var_11 = var_7.field_3;
    var_12 = (uint64_t)*(unsigned char *)(*var_2 + 16UL);
    *(uint64_t *)(var_0 + (-80L)) = 4211683UL;
    indirect_placeholder_6(var_8, 0UL, var_9, var_10, var_12, 4294120UL, var_11);
    var_13 = var_0 + (-88L);
    *(uint64_t *)var_13 = 4211693UL;
    indirect_placeholder_3();
    *(uint32_t *)6407588UL = 0U;
    var_14 = (uint32_t *)(var_0 + (-12L));
    *var_14 = 0U;
    var_32 = 0U;
    var_15 = 0U;
    local_sp_6 = var_13;
    rcx_4 = var_10;
    var_16 = *(uint64_t *)(((uint64_t)var_15 * 24UL) + 4284928UL);
    *(uint64_t *)(local_sp_6 + (-8L)) = 4211906UL;
    indirect_placeholder_3();
    rcx_1 = rcx_4;
    rcx_3 = rcx_4;
    while ((uint64_t)(uint32_t)var_16 != 0UL)
        {
            var_17 = *(uint64_t *)(((uint64_t)*var_14 * 24UL) + 4284928UL);
            var_18 = local_sp_6 + (-16L);
            *(uint64_t *)var_18 = 4211756UL;
            indirect_placeholder_3();
            local_sp_5 = var_18;
            if ((uint64_t)(uint32_t)var_17 == 0UL) {
                var_19 = (uint64_t)*(unsigned char *)((*(uint64_t *)(((uint64_t)*var_14 * 24UL) + 4284944UL) + *var_2) + 17UL);
                *(uint64_t *)(local_sp_6 + (-24L)) = 4211809UL;
                var_20 = indirect_placeholder_8(var_19);
                var_21 = *(uint64_t *)(((uint64_t)*var_14 * 24UL) + 4284928UL);
                var_22 = local_sp_6 + (-32L);
                *(uint64_t *)var_22 = 4211861UL;
                indirect_placeholder_6(var_8, 0UL, var_20, var_20, var_21, 4294131UL, var_11);
                local_sp_5 = var_22;
                rcx_3 = var_20;
            }
            var_23 = *var_14 + 1U;
            *var_14 = var_23;
            var_15 = var_23;
            local_sp_6 = local_sp_5;
            rcx_4 = rcx_3;
            var_16 = *(uint64_t *)(((uint64_t)var_15 * 24UL) + 4284928UL);
            *(uint64_t *)(local_sp_6 + (-8L)) = 4211906UL;
            indirect_placeholder_3();
            rcx_1 = rcx_4;
            rcx_3 = rcx_4;
        }
    var_24 = *var_2;
    var_25 = (uint64_t)*(unsigned char *)(var_24 + 22UL);
    var_26 = (uint64_t)*(unsigned char *)(var_24 + 23UL);
    var_27 = local_sp_6 + (-16L);
    *(uint64_t *)var_27 = 4211954UL;
    indirect_placeholder_6(var_8, 0UL, var_25, rcx_4, var_26, 4294185UL, var_11);
    local_sp_1 = var_27;
    if (*(uint32_t *)6407588UL == 0U) {
        var_28 = local_sp_6 + (-24L);
        *(uint64_t *)var_28 = 4211974UL;
        indirect_placeholder_3();
        local_sp_1 = var_28;
    }
    *(uint32_t *)6407588UL = 0U;
    *var_14 = 0U;
    var_29 = var_0 + (-24L);
    var_30 = (uint64_t *)var_29;
    var_31 = (uint64_t *)(var_0 + (-32L));
    local_sp_2 = local_sp_1;
    var_33 = (uint64_t)var_32 << 5UL;
    rcx_0 = rcx_1;
    local_sp_3 = local_sp_2;
    rcx_2 = rcx_1;
    local_sp_4 = local_sp_2;
    while (*(uint64_t *)(var_33 + 4282048UL) != 0UL)
        {
            if ((*(unsigned char *)(var_33 + 4282060UL) & '\b') != '\x00') {
                var_34 = (uint32_t *)(var_33 + 4282056UL);
                _pre_phi111 = var_34;
                if ((uint64_t)(*var_34 - *var_4) == 0UL) {
                    var_35 = local_sp_2 + (-8L);
                    *(uint64_t *)var_35 = 4212063UL;
                    indirect_placeholder_3();
                    *(uint32_t *)6407588UL = 0U;
                    *var_4 = *(uint32_t *)(((uint64_t)*var_14 << 5UL) + 4282056UL);
                    _pre_phi111 = (uint32_t *)(((uint64_t)*var_14 << 5UL) + 4282056UL);
                    local_sp_4 = var_35;
                }
                var_36 = (uint64_t)*_pre_phi111;
                var_37 = *var_2;
                var_38 = local_sp_4 + (-8L);
                *(uint64_t *)var_38 = 4212126UL;
                var_39 = indirect_placeholder(var_37, var_36);
                *var_30 = var_39;
                var_40 = (uint64_t)*var_14 << 5UL;
                var_41 = var_40 + 4282072UL;
                storemerge = *(uint64_t *)((*(uint64_t *)var_41 == 0UL) ? (var_40 + 4282064UL) : var_41);
                *var_31 = storemerge;
                var_43 = storemerge;
                local_sp_0 = var_38;
                if (*var_30 == 0UL) {
                    var_42 = local_sp_4 + (-16L);
                    *(uint64_t *)var_42 = 4212229UL;
                    indirect_placeholder_3();
                    var_43 = *var_31;
                    local_sp_0 = var_42;
                    rcx_0 = 4295424UL;
                }
                var_44 = var_43 & (uint64_t)**(uint32_t **)var_29;
                var_45 = (uint64_t)*var_14 << 5UL;
                local_sp_3 = local_sp_0;
                rcx_2 = rcx_0;
                if (var_44 == *(uint64_t *)(var_45 + 4282064UL)) {
                    var_48 = *(uint64_t *)(var_45 + 4282048UL);
                    var_49 = local_sp_0 + (-8L);
                    *(uint64_t *)var_49 = 4212306UL;
                    indirect_placeholder_6(var_8, 0UL, var_44, rcx_0, var_48, 4293901UL, var_11);
                    local_sp_3 = var_49;
                } else {
                    if ((*(unsigned char *)(var_45 + 4282060UL) & '\x04') == '\x00') {
                        var_46 = *(uint64_t *)(var_45 + 4282048UL);
                        var_47 = local_sp_0 + (-8L);
                        *(uint64_t *)var_47 = 4212373UL;
                        indirect_placeholder_6(var_8, 0UL, var_44, rcx_0, var_46, 4294181UL, var_11);
                        local_sp_3 = var_47;
                    }
                }
            }
            var_50 = *var_14 + 1U;
            *var_14 = var_50;
            var_32 = var_50;
            local_sp_2 = local_sp_3;
            rcx_1 = rcx_2;
            var_33 = (uint64_t)var_32 << 5UL;
            rcx_0 = rcx_1;
            local_sp_3 = local_sp_2;
            rcx_2 = rcx_1;
            local_sp_4 = local_sp_2;
        }
    *(uint64_t *)(local_sp_2 + (-8L)) = 4212414UL;
    indirect_placeholder_3();
    *(uint32_t *)6407588UL = 0U;
    return;
}

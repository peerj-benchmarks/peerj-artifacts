typedef unsigned char __u_char;
typedef unsigned short __u_short;
typedef unsigned int __u_int;
typedef unsigned long __u_long;
typedef signed char __int8_t;
typedef unsigned char __uint8_t;
typedef short __int16_t;
typedef unsigned short __uint16_t;
typedef int __int32_t;
typedef unsigned int __uint32_t;
typedef long __int64_t;
typedef unsigned long __uint64_t;
typedef __int8_t __int_least8_t;
typedef __uint8_t __uint_least8_t;
typedef __int16_t __int_least16_t;
typedef __uint16_t __uint_least16_t;
typedef __int32_t __int_least32_t;
typedef __uint32_t __uint_least32_t;
typedef __int64_t __int_least64_t;
typedef __uint64_t __uint_least64_t;
typedef long __quad_t;
typedef unsigned long __u_quad_t;
typedef long __intmax_t;
typedef unsigned long __uintmax_t;
typedef unsigned long __dev_t;
typedef unsigned int __uid_t;
typedef unsigned int __gid_t;
typedef unsigned long __ino_t;
typedef unsigned long __ino64_t;
typedef unsigned int __mode_t;
typedef unsigned long __nlink_t;
typedef long __off_t;
typedef long __off64_t;
typedef int __pid_t;
typedef struct {
    int __val[2];
} __fsid_t;
typedef long __clock_t;
typedef unsigned long __rlim_t;
typedef unsigned long __rlim64_t;
typedef unsigned int __id_t;
typedef long __time_t;
typedef unsigned int __useconds_t;
typedef long __suseconds_t;
typedef int __daddr_t;
typedef int __key_t;
typedef int __clockid_t;
typedef void *__timer_t;
typedef long __blksize_t;
typedef long __blkcnt_t;
typedef long __blkcnt64_t;
typedef unsigned long __fsblkcnt_t;
typedef unsigned long __fsblkcnt64_t;
typedef unsigned long __fsfilcnt_t;
typedef unsigned long __fsfilcnt64_t;
typedef long __fsword_t;
typedef long __ssize_t;
typedef long __syscall_slong_t;
typedef unsigned long __syscall_ulong_t;
typedef __off64_t __loff_t;
typedef char *__caddr_t;
typedef long __intptr_t;
typedef unsigned int __socklen_t;
typedef int __sig_atomic_t;
typedef __int8_t int8_t;
typedef __int16_t int16_t;
typedef __int32_t int32_t;
typedef __int64_t int64_t;
typedef __uint8_t uint8_t;
typedef __uint16_t uint16_t;
typedef __uint32_t uint32_t;
typedef __uint64_t uint64_t;
typedef __int_least8_t int_least8_t;
typedef __int_least16_t int_least16_t;
typedef __int_least32_t int_least32_t;
typedef __int_least64_t int_least64_t;
typedef __uint_least8_t uint_least8_t;
typedef __uint_least16_t uint_least16_t;
typedef __uint_least32_t uint_least32_t;
typedef __uint_least64_t uint_least64_t;
typedef signed char int_fast8_t;
typedef long int_fast16_t;
typedef long int_fast32_t;
typedef long int_fast64_t;
typedef unsigned char uint_fast8_t;
typedef unsigned long uint_fast16_t;
typedef unsigned long uint_fast32_t;
typedef unsigned long uint_fast64_t;
typedef long intptr_t;
typedef unsigned long uintptr_t;
typedef __intmax_t intmax_t;
typedef __uintmax_t uintmax_t;
typedef _Bool bool;
struct indirect_placeholder_6_ret_type;
struct indirect_placeholder_13_ret_type;
struct indirect_placeholder_9_ret_type;
struct indirect_placeholder_10_ret_type;
struct indirect_placeholder_12_ret_type;
struct indirect_placeholder_6_ret_type {
    uint64_t field_0;
    uint64_t field_1;
    uint64_t field_2;
    uint64_t field_3;
    uint64_t field_4;
};
struct indirect_placeholder_13_ret_type {
    uint64_t field_0;
    uint64_t field_1;
    uint64_t field_2;
    uint64_t field_3;
    uint64_t field_4;
};
struct indirect_placeholder_9_ret_type {
    uint64_t field_0;
    uint64_t field_1;
    uint64_t field_2;
};
struct indirect_placeholder_10_ret_type {
    uint64_t field_0;
    uint64_t field_1;
    uint64_t field_2;
};
struct indirect_placeholder_12_ret_type {
    uint64_t field_0;
    uint64_t field_1;
    uint64_t field_2;
};
extern void abort(void);
extern uint64_t revng_init_local_sp(uint64_t param_0);
extern uint64_t init_rbp(void);
extern uint64_t indirect_placeholder(uint64_t param_0, uint64_t param_1);
extern uint64_t init_cc_src2(void);
extern uint64_t helper_cc_compute_c_wrapper(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint32_t param_3);
extern void indirect_placeholder_1(void);
extern uint64_t init_rbx(void);
extern uint64_t indirect_placeholder_11(void);
extern void indirect_placeholder_7(uint64_t param_0);
extern void indirect_placeholder_8(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint64_t param_3, uint64_t param_4, uint64_t param_5, uint64_t param_6);
extern struct indirect_placeholder_6_ret_type indirect_placeholder_6(uint64_t param_0);
extern struct indirect_placeholder_13_ret_type indirect_placeholder_13(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint64_t param_3, uint64_t param_4, uint64_t param_5, uint64_t param_6);
extern struct indirect_placeholder_9_ret_type indirect_placeholder_9(uint64_t param_0, uint64_t param_1, uint64_t param_2);
extern struct indirect_placeholder_10_ret_type indirect_placeholder_10(uint64_t param_0, uint64_t param_1, uint64_t param_2);
extern struct indirect_placeholder_12_ret_type indirect_placeholder_12(uint64_t param_0, uint64_t param_1, uint64_t param_2);
void bb_paste_parallel(uint64_t rdi, uint64_t rsi) {
    uint64_t var_71;
    uint64_t local_sp_13;
    uint64_t var_102;
    uint64_t var_103;
    uint64_t var_0;
    uint64_t var_1;
    uint64_t var_2;
    uint64_t var_3;
    uint64_t var_4;
    uint64_t *var_5;
    uint64_t *var_6;
    unsigned char *var_7;
    uint64_t var_8;
    struct indirect_placeholder_6_ret_type var_9;
    uint64_t var_10;
    uint64_t var_11;
    uint64_t var_12;
    uint64_t var_13;
    uint64_t var_14;
    uint64_t *var_15;
    uint64_t var_16;
    uint64_t var_17;
    struct indirect_placeholder_13_ret_type var_18;
    uint64_t var_19;
    uint64_t var_20;
    uint64_t var_21;
    uint64_t var_22;
    uint64_t *var_23;
    unsigned char *var_24;
    uint64_t *var_25;
    uint64_t var_26;
    uint64_t local_sp_12;
    uint64_t var_85;
    struct indirect_placeholder_9_ret_type var_86;
    uint64_t var_87;
    uint64_t var_88;
    uint64_t var_89;
    uint64_t var_90;
    uint64_t var_91;
    uint64_t local_sp_0;
    uint64_t var_92;
    uint64_t var_74;
    struct indirect_placeholder_10_ret_type var_75;
    uint64_t var_76;
    uint64_t var_77;
    uint64_t var_78;
    uint64_t var_79;
    uint64_t var_80;
    uint64_t local_sp_1;
    bool var_81;
    uint64_t var_82;
    uint64_t *var_83;
    uint64_t var_84;
    uint64_t *_cast3_pre_phi;
    uint64_t var_63;
    uint64_t var_64;
    uint64_t local_sp_2;
    uint64_t local_sp_3;
    unsigned char var_70;
    uint64_t var_65;
    uint32_t var_66;
    uint64_t var_67;
    uint64_t var_68;
    uint64_t var_69;
    uint64_t var_44;
    uint64_t var_45;
    uint64_t var_46;
    uint64_t r9_2;
    uint64_t var_36;
    struct indirect_placeholder_12_ret_type var_37;
    uint64_t var_38;
    uint64_t var_39;
    uint64_t var_40;
    uint64_t var_41;
    uint64_t var_42;
    uint64_t var_100;
    uint32_t var_101;
    uint64_t r8_2;
    uint64_t r9_0;
    uint64_t local_sp_6;
    uint64_t r8_0;
    uint64_t local_sp_4;
    uint64_t var_43;
    uint64_t rcx_0;
    uint64_t r9_1;
    uint64_t r8_1;
    uint64_t local_sp_5;
    uint64_t var_47;
    uint64_t var_31;
    uint64_t var_32;
    uint64_t var_33;
    uint64_t var_34;
    uint64_t var_35;
    uint64_t *_cast2;
    uint64_t rcx_1;
    uint64_t var_27;
    uint64_t var_28;
    uint64_t local_sp_7;
    uint64_t var_48;
    unsigned char *var_49;
    uint64_t var_50;
    uint64_t *var_51;
    uint64_t *var_52;
    uint64_t *var_53;
    unsigned char *var_54;
    uint32_t *var_55;
    uint32_t *var_56;
    unsigned char *var_57;
    uint64_t local_sp_10;
    uint64_t local_sp_8;
    uint64_t var_58;
    uint64_t local_sp_9;
    uint64_t var_59;
    uint64_t var_60;
    uint64_t var_61;
    uint64_t var_62;
    uint64_t var_72;
    uint64_t var_73;
    uint64_t local_sp_11;
    uint64_t var_96;
    uint64_t var_97;
    uint64_t var_98;
    uint64_t var_99;
    unsigned char **var_93;
    uint64_t var_94;
    uint64_t var_95;
    unsigned char var_104;
    uint64_t local_sp_14;
    uint64_t var_105;
    uint64_t var_106;
    uint64_t var_107;
    unsigned char storemerge;
    uint64_t local_sp_15;
    uint64_t var_111;
    uint32_t var_108;
    uint64_t var_109;
    uint64_t var_110;
    uint64_t var_29;
    uint64_t var_30;
    unsigned int loop_state_var;
    var_0 = revng_init_local_sp(0UL);
    var_1 = init_rbp();
    var_2 = init_cc_src2();
    var_3 = init_rbx();
    var_4 = var_0 + (-8L);
    *(uint64_t *)var_4 = var_1;
    *(uint64_t *)(var_0 + (-16L)) = var_3;
    var_5 = (uint64_t *)(var_0 + (-128L));
    *var_5 = rdi;
    var_6 = (uint64_t *)(var_0 + (-136L));
    *var_6 = rsi;
    var_7 = (unsigned char *)(var_0 + (-25L));
    *var_7 = (unsigned char)'\x01';
    var_8 = *var_5 + 2UL;
    *(uint64_t *)(var_0 + (-144L)) = 4202311UL;
    var_9 = indirect_placeholder_6(var_8);
    var_10 = var_9.field_0;
    var_11 = var_9.field_1;
    var_12 = var_9.field_2;
    var_13 = var_9.field_3;
    var_14 = var_9.field_4;
    var_15 = (uint64_t *)(var_0 + (-96L));
    *var_15 = var_10;
    var_16 = *var_5 + 1UL;
    var_17 = var_0 + (-152L);
    *(uint64_t *)var_17 = 4202336UL;
    var_18 = indirect_placeholder_13(var_11, var_3, var_12, var_16, var_13, 8UL, var_14);
    var_19 = var_18.field_0;
    var_20 = var_18.field_2;
    var_21 = var_18.field_3;
    var_22 = var_18.field_4;
    var_23 = (uint64_t *)(var_0 + (-104L));
    *var_23 = var_19;
    var_24 = (unsigned char *)(var_0 + (-41L));
    *var_24 = (unsigned char)'\x00';
    var_25 = (uint64_t *)(var_0 + (-40L));
    *var_25 = 0UL;
    var_26 = 0UL;
    var_70 = (unsigned char)'\x00';
    r9_2 = var_21;
    r8_2 = var_22;
    local_sp_6 = var_17;
    rcx_1 = var_20;
    var_58 = 0UL;
    var_27 = *var_5;
    var_28 = helper_cc_compute_c_wrapper(var_26 - var_27, var_27, var_2, 17U);
    r9_0 = r9_2;
    r8_0 = r8_2;
    rcx_0 = rcx_1;
    r9_1 = r9_2;
    r8_1 = r8_2;
    local_sp_7 = local_sp_6;
    while (var_28 != 0UL)
        {
            var_29 = *(uint64_t *)(*var_6 + (*var_25 << 3UL));
            var_30 = local_sp_6 + (-8L);
            *(uint64_t *)var_30 = 4202392UL;
            indirect_placeholder_1();
            local_sp_5 = var_30;
            if ((uint64_t)(uint32_t)var_29 == 0UL) {
                *(unsigned char *)6378832UL = (unsigned char)'\x01';
                *(uint64_t *)((*var_25 << 3UL) + *var_23) = *(uint64_t *)6378696UL;
            } else {
                var_31 = *var_25 << 3UL;
                var_32 = *var_23 + var_31;
                var_33 = *(uint64_t *)(*var_6 + var_31);
                var_34 = local_sp_6 + (-16L);
                *(uint64_t *)var_34 = 4202492UL;
                indirect_placeholder_1();
                *(uint64_t *)var_32 = var_33;
                var_35 = *var_25 << 3UL;
                _cast2 = (uint64_t *)(*var_23 + var_35);
                _cast3_pre_phi = _cast2;
                local_sp_4 = var_34;
                if (*_cast2 == 0UL) {
                    var_36 = *(uint64_t *)(*var_6 + var_35);
                    *(uint64_t *)(local_sp_6 + (-24L)) = 4202562UL;
                    var_37 = indirect_placeholder_12(var_36, 0UL, 3UL);
                    var_38 = var_37.field_0;
                    var_39 = var_37.field_1;
                    var_40 = var_37.field_2;
                    *(uint64_t *)(local_sp_6 + (-32L)) = 4202570UL;
                    indirect_placeholder_1();
                    var_41 = (uint64_t)*(uint32_t *)var_38;
                    var_42 = local_sp_6 + (-40L);
                    *(uint64_t *)var_42 = 4202597UL;
                    indirect_placeholder_8(0UL, 4269136UL, var_38, var_39, 1UL, var_40, var_41);
                    _cast3_pre_phi = (uint64_t *)(*var_23 + (*var_25 << 3UL));
                    r9_0 = var_39;
                    r8_0 = var_40;
                    local_sp_4 = var_42;
                }
                var_43 = *_cast3_pre_phi;
                *(uint64_t *)(local_sp_4 + (-8L)) = 4202627UL;
                indirect_placeholder_1();
                r9_1 = r9_0;
                r8_1 = r8_0;
                if ((uint64_t)(uint32_t)var_43 == 0UL) {
                    *var_24 = (unsigned char)'\x01';
                }
                var_44 = *(uint64_t *)(*var_23 + (*var_25 << 3UL));
                var_45 = local_sp_4 + (-16L);
                *(uint64_t *)var_45 = 4202670UL;
                var_46 = indirect_placeholder(var_44, 2UL);
                rcx_0 = var_46;
                local_sp_5 = var_45;
            }
            var_47 = *var_25 + 1UL;
            *var_25 = var_47;
            var_26 = var_47;
            r9_2 = r9_1;
            r8_2 = r8_1;
            local_sp_6 = local_sp_5;
            rcx_1 = rcx_0;
            var_27 = *var_5;
            var_28 = helper_cc_compute_c_wrapper(var_26 - var_27, var_27, var_2, 17U);
            r9_0 = r9_2;
            r8_0 = r8_2;
            rcx_0 = rcx_1;
            r9_1 = r9_2;
            r8_1 = r8_2;
            local_sp_7 = local_sp_6;
        }
    if (*var_24 != '\x00' & *(unsigned char *)6378832UL == '\x00') {
        var_48 = local_sp_6 + (-8L);
        *(uint64_t *)var_48 = 4202731UL;
        indirect_placeholder_8(0UL, 4269139UL, rcx_1, r9_2, 1UL, r8_2, 0UL);
        local_sp_7 = var_48;
    }
    var_49 = (unsigned char *)(var_0 + (-42L));
    var_50 = var_0 + (-56L);
    var_51 = (uint64_t *)var_50;
    var_52 = (uint64_t *)(var_0 + (-64L));
    var_53 = (uint64_t *)(var_0 + (-72L));
    var_54 = (unsigned char *)(var_0 + (-81L));
    var_55 = (uint32_t *)(var_0 + (-76L));
    var_56 = (uint32_t *)(var_0 + (-80L));
    var_57 = (unsigned char *)(var_0 + (-105L));
    local_sp_8 = local_sp_7;
    local_sp_9 = local_sp_8;
    while (*var_25 != 0UL)
        {
            *var_49 = (unsigned char)'\x00';
            *var_51 = *(uint64_t *)6378840UL;
            *var_52 = 0UL;
            *var_53 = 0UL;
            var_59 = *var_5;
            var_60 = helper_cc_compute_c_wrapper(var_58 - var_59, var_59, var_2, 17U);
            local_sp_8 = local_sp_9;
            local_sp_10 = local_sp_9;
            while (var_60 != 0UL)
                {
                    if (*var_25 == 0UL) {
                        break;
                    }
                    *var_54 = (unsigned char)'\x00';
                    var_61 = *(uint64_t *)(*var_23 + (*var_53 << 3UL));
                    if (var_61 == 0UL) {
                        local_sp_11 = local_sp_10;
                        local_sp_13 = local_sp_10;
                        if (var_70 == '\x01') {
                            *var_49 = (unsigned char)'\x01';
                            if ((*var_53 + 1UL) == *var_5) {
                                var_108 = *var_55;
                                if (var_108 == 4294967295U) {
                                    storemerge = *(unsigned char *)6378464UL;
                                } else {
                                    storemerge = (unsigned char)var_108;
                                }
                                *var_57 = storemerge;
                                var_109 = (uint64_t)(uint32_t)(uint64_t)storemerge;
                                var_110 = local_sp_10 + (-8L);
                                *(uint64_t *)var_110 = 4203682UL;
                                indirect_placeholder_7(var_109);
                                local_sp_15 = var_110;
                            } else {
                                var_100 = (uint64_t)*(unsigned char *)6378464UL;
                                var_101 = *var_55;
                                if (((uint64_t)((uint32_t)var_100 - var_101) == 0UL) || (var_101 == 4294967295U)) {
                                    var_102 = (uint64_t)(uint32_t)((int)(var_101 << 24U) >> (int)24U);
                                    var_103 = local_sp_10 + (-8L);
                                    *(uint64_t *)var_103 = 4203591UL;
                                    indirect_placeholder_7(var_102);
                                    local_sp_13 = var_103;
                                }
                                var_104 = **(unsigned char **)var_50;
                                local_sp_14 = local_sp_13;
                                if (var_104 == '\x00') {
                                    var_105 = (uint64_t)(uint32_t)(uint64_t)var_104;
                                    var_106 = local_sp_13 + (-8L);
                                    *(uint64_t *)var_106 = 4203619UL;
                                    indirect_placeholder_7(var_105);
                                    local_sp_14 = var_106;
                                }
                                var_107 = *var_51 + 1UL;
                                *var_51 = var_107;
                                local_sp_15 = local_sp_14;
                                if (var_107 == *(uint64_t *)6378848UL) {
                                    *var_51 = *(uint64_t *)6378840UL;
                                }
                            }
                        } else {
                            var_71 = *var_53;
                            var_72 = *(uint64_t *)(*var_23 + (var_71 << 3UL));
                            var_92 = var_71;
                            if (var_72 != 0UL) {
                                var_73 = local_sp_10 + (-8L);
                                *(uint64_t *)var_73 = 4203071UL;
                                indirect_placeholder_1();
                                local_sp_1 = var_73;
                                if ((uint64_t)(uint32_t)var_72 == 0UL) {
                                    var_74 = *(uint64_t *)(*var_6 + (*var_53 << 3UL));
                                    *(uint64_t *)(local_sp_10 + (-16L)) = 4203115UL;
                                    var_75 = indirect_placeholder_10(var_74, 0UL, 3UL);
                                    var_76 = var_75.field_0;
                                    var_77 = var_75.field_1;
                                    var_78 = var_75.field_2;
                                    var_79 = (uint64_t)*var_56;
                                    var_80 = local_sp_10 + (-24L);
                                    *(uint64_t *)var_80 = 4203146UL;
                                    indirect_placeholder_8(0UL, 4269136UL, var_76, var_77, 0UL, var_78, var_79);
                                    *var_7 = (unsigned char)'\x00';
                                    local_sp_1 = var_80;
                                }
                                var_81 = (*(uint64_t *)(*var_23 + (*var_53 << 3UL)) == *(uint64_t *)6378696UL);
                                var_82 = local_sp_1 + (-8L);
                                var_83 = (uint64_t *)var_82;
                                local_sp_0 = var_82;
                                if (var_81) {
                                    *var_83 = 4203214UL;
                                    indirect_placeholder_1();
                                } else {
                                    *var_83 = 4203246UL;
                                    var_84 = indirect_placeholder_11();
                                    if ((uint64_t)((uint32_t)var_84 + 1U) == 0UL) {
                                        var_85 = *(uint64_t *)(*var_6 + (*var_53 << 3UL));
                                        *(uint64_t *)(local_sp_1 + (-16L)) = 4203291UL;
                                        var_86 = indirect_placeholder_9(var_85, 0UL, 3UL);
                                        var_87 = var_86.field_0;
                                        var_88 = var_86.field_1;
                                        var_89 = var_86.field_2;
                                        *(uint64_t *)(local_sp_1 + (-24L)) = 4203299UL;
                                        indirect_placeholder_1();
                                        var_90 = (uint64_t)*(uint32_t *)var_87;
                                        var_91 = local_sp_1 + (-32L);
                                        *(uint64_t *)var_91 = 4203326UL;
                                        indirect_placeholder_8(0UL, 4269136UL, var_87, var_88, 0UL, var_89, var_90);
                                        *var_7 = (unsigned char)'\x00';
                                        local_sp_0 = var_91;
                                    }
                                }
                                *(uint64_t *)(*var_23 + (*var_53 << 3UL)) = 0UL;
                                *var_25 = (*var_25 + (-1L));
                                var_92 = *var_53;
                                local_sp_11 = local_sp_0;
                            }
                            local_sp_12 = local_sp_11;
                            local_sp_15 = local_sp_11;
                            if ((var_92 + 1UL) == *var_5) {
                                if (*var_49 != '\x00') {
                                    if (*var_52 != 0UL) {
                                        var_96 = *var_15;
                                        var_97 = local_sp_11 + (-8L);
                                        *(uint64_t *)var_97 = 4203416UL;
                                        indirect_placeholder_1();
                                        local_sp_12 = var_97;
                                        if (var_96 != *var_52) {
                                            *(uint64_t *)(local_sp_11 + (-16L)) = 4203427UL;
                                            indirect_placeholder_7(var_4);
                                            abort();
                                        }
                                        *var_52 = 0UL;
                                    }
                                    var_98 = (uint64_t)(uint32_t)(uint64_t)*(unsigned char *)6378464UL;
                                    var_99 = local_sp_12 + (-8L);
                                    *(uint64_t *)var_99 = 4203452UL;
                                    indirect_placeholder_7(var_98);
                                    local_sp_15 = var_99;
                                }
                            } else {
                                var_93 = (unsigned char **)var_50;
                                if (**var_93 == '\x00') {
                                    var_94 = *var_52;
                                    *var_52 = (var_94 + 1UL);
                                    *(unsigned char *)(*var_15 + var_94) = **var_93;
                                }
                                var_95 = *var_51 + 1UL;
                                *var_51 = var_95;
                                if (var_95 == *(uint64_t *)6378848UL) {
                                    *var_51 = *(uint64_t *)6378840UL;
                                }
                            }
                        }
                        var_111 = *var_53 + 1UL;
                        *var_53 = var_111;
                        var_58 = var_111;
                        local_sp_9 = local_sp_15;
                        var_59 = *var_5;
                        var_60 = helper_cc_compute_c_wrapper(var_58 - var_59, var_59, var_2, 17U);
                        local_sp_8 = local_sp_9;
                        local_sp_10 = local_sp_9;
                        continue;
                    }
                    *(uint64_t *)(local_sp_9 + (-8L)) = 4202837UL;
                    indirect_placeholder_1();
                    *var_55 = (uint32_t)var_61;
                    var_62 = local_sp_9 + (-16L);
                    *(uint64_t *)var_62 = 4202845UL;
                    indirect_placeholder_1();
                    *var_56 = *(uint32_t *)var_61;
                    local_sp_2 = var_62;
                    var_70 = (unsigned char)'\x01';
                    if (*var_55 != 4294967295U & *var_52 != 0UL) {
                        var_63 = *var_15;
                        var_64 = local_sp_9 + (-24L);
                        *(uint64_t *)var_64 = 4202891UL;
                        indirect_placeholder_1();
                        local_sp_2 = var_64;
                        if (var_63 != *var_52) {
                            *(uint64_t *)(local_sp_9 + (-32L)) = 4202902UL;
                            indirect_placeholder_7(var_4);
                            abort();
                        }
                        *var_52 = 0UL;
                    }
                    local_sp_3 = local_sp_2;
                    while (1U)
                        {
                            local_sp_10 = local_sp_3;
                            if (*var_55 != 4294967295U) {
                                loop_state_var = 0U;
                                break;
                            }
                            *var_54 = (unsigned char)'\x01';
                            var_65 = (uint64_t)*(unsigned char *)6378464UL;
                            var_66 = *var_55;
                            if ((uint64_t)((uint32_t)var_65 - var_66) != 0UL) {
                                loop_state_var = 1U;
                                break;
                            }
                            var_67 = (uint64_t)(uint32_t)((int)(var_66 << 24U) >> (int)24U);
                            *(uint64_t *)(local_sp_3 + (-8L)) = 4202946UL;
                            indirect_placeholder_7(var_67);
                            var_68 = *(uint64_t *)(*var_23 + (*var_53 << 3UL));
                            *(uint64_t *)(local_sp_3 + (-16L)) = 4202976UL;
                            indirect_placeholder_1();
                            *var_55 = (uint32_t)var_68;
                            var_69 = local_sp_3 + (-24L);
                            *(uint64_t *)var_69 = 4202984UL;
                            indirect_placeholder_1();
                            *var_56 = *(uint32_t *)var_68;
                            local_sp_3 = var_69;
                            continue;
                        }
                    var_70 = *var_54;
                }
            local_sp_9 = local_sp_8;
        }
    *(uint64_t *)(local_sp_8 + (-8L)) = 4203731UL;
    indirect_placeholder_1();
    *(uint64_t *)(local_sp_8 + (-16L)) = 4203743UL;
    indirect_placeholder_1();
    return;
}

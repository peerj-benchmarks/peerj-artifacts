typedef unsigned char __u_char;
typedef unsigned short __u_short;
typedef unsigned int __u_int;
typedef unsigned long __u_long;
typedef signed char __int8_t;
typedef unsigned char __uint8_t;
typedef short __int16_t;
typedef unsigned short __uint16_t;
typedef int __int32_t;
typedef unsigned int __uint32_t;
typedef long __int64_t;
typedef unsigned long __uint64_t;
typedef __int8_t __int_least8_t;
typedef __uint8_t __uint_least8_t;
typedef __int16_t __int_least16_t;
typedef __uint16_t __uint_least16_t;
typedef __int32_t __int_least32_t;
typedef __uint32_t __uint_least32_t;
typedef __int64_t __int_least64_t;
typedef __uint64_t __uint_least64_t;
typedef long __quad_t;
typedef unsigned long __u_quad_t;
typedef long __intmax_t;
typedef unsigned long __uintmax_t;
typedef unsigned long __dev_t;
typedef unsigned int __uid_t;
typedef unsigned int __gid_t;
typedef unsigned long __ino_t;
typedef unsigned long __ino64_t;
typedef unsigned int __mode_t;
typedef unsigned long __nlink_t;
typedef long __off_t;
typedef long __off64_t;
typedef int __pid_t;
typedef struct {
    int __val[2];
} __fsid_t;
typedef long __clock_t;
typedef unsigned long __rlim_t;
typedef unsigned long __rlim64_t;
typedef unsigned int __id_t;
typedef long __time_t;
typedef unsigned int __useconds_t;
typedef long __suseconds_t;
typedef int __daddr_t;
typedef int __key_t;
typedef int __clockid_t;
typedef void *__timer_t;
typedef long __blksize_t;
typedef long __blkcnt_t;
typedef long __blkcnt64_t;
typedef unsigned long __fsblkcnt_t;
typedef unsigned long __fsblkcnt64_t;
typedef unsigned long __fsfilcnt_t;
typedef unsigned long __fsfilcnt64_t;
typedef long __fsword_t;
typedef long __ssize_t;
typedef long __syscall_slong_t;
typedef unsigned long __syscall_ulong_t;
typedef __off64_t __loff_t;
typedef char *__caddr_t;
typedef long __intptr_t;
typedef unsigned int __socklen_t;
typedef int __sig_atomic_t;
typedef __int8_t int8_t;
typedef __int16_t int16_t;
typedef __int32_t int32_t;
typedef __int64_t int64_t;
typedef __uint8_t uint8_t;
typedef __uint16_t uint16_t;
typedef __uint32_t uint32_t;
typedef __uint64_t uint64_t;
typedef __int_least8_t int_least8_t;
typedef __int_least16_t int_least16_t;
typedef __int_least32_t int_least32_t;
typedef __int_least64_t int_least64_t;
typedef __uint_least8_t uint_least8_t;
typedef __uint_least16_t uint_least16_t;
typedef __uint_least32_t uint_least32_t;
typedef __uint_least64_t uint_least64_t;
typedef signed char int_fast8_t;
typedef long int_fast16_t;
typedef long int_fast32_t;
typedef long int_fast64_t;
typedef unsigned char uint_fast8_t;
typedef unsigned long uint_fast16_t;
typedef unsigned long uint_fast32_t;
typedef unsigned long uint_fast64_t;
typedef long intptr_t;
typedef unsigned long uintptr_t;
typedef __intmax_t intmax_t;
typedef __uintmax_t uintmax_t;
typedef _Bool bool;
extern uint64_t revng_init_local_sp(uint64_t param_0);
extern uint64_t init_rbp(void);
extern uint64_t init_cc_src2(void);
extern uint64_t helper_cc_compute_c_wrapper(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint32_t param_3);
extern uint64_t helper_cc_compute_all_wrapper(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint32_t param_3);
uint64_t bb_compute_number_width(uint64_t rsi, uint64_t rdi) {
    uint64_t var_0;
    uint64_t var_1;
    uint64_t var_2;
    uint64_t *var_3;
    uint64_t var_4;
    uint64_t *var_5;
    uint32_t *var_6;
    uint32_t var_22;
    uint64_t var_7;
    uint32_t *var_8;
    uint64_t *var_9;
    uint64_t *var_10;
    uint64_t var_11;
    uint64_t var_12;
    uint64_t var_13;
    uint32_t var_18;
    uint64_t var_19;
    uint64_t var_16;
    bool var_17;
    uint32_t var_20;
    uint64_t var_21;
    uint64_t var_14;
    uint64_t var_15;
    var_0 = revng_init_local_sp(0UL);
    var_1 = init_rbp();
    var_2 = init_cc_src2();
    *(uint64_t *)(var_0 + (-8L)) = var_1;
    var_3 = (uint64_t *)(var_0 + (-48L));
    *var_3 = rdi;
    var_4 = var_0 + (-56L);
    var_5 = (uint64_t *)var_4;
    *var_5 = rsi;
    var_6 = (uint32_t *)(var_0 + (-12L));
    *var_6 = 1U;
    var_11 = 0UL;
    var_22 = 1U;
    if (*var_3 == 0UL) {
        return (uint64_t)var_22;
    }
    var_7 = helper_cc_compute_all_wrapper((uint64_t)**(uint32_t **)var_4, 0UL, 0UL, 24U);
    if ((uint64_t)(((unsigned char)(var_7 >> 4UL) ^ (unsigned char)var_7) & '\xc0') == 0UL) {
        var_22 = *var_6;
    } else {
        var_8 = (uint32_t *)(var_0 + (-16L));
        *var_8 = 1U;
        var_9 = (uint64_t *)(var_0 + (-24L));
        *var_9 = 0UL;
        var_10 = (uint64_t *)(var_0 + (-32L));
        *var_10 = 0UL;
        var_12 = *var_3;
        var_13 = helper_cc_compute_c_wrapper(var_11 - var_12, var_12, var_2, 17U);
        while (var_13 != 0UL)
            {
                var_14 = *var_5 + (*var_10 * 152UL);
                if (*(uint32_t *)var_14 != 0U) {
                    if ((uint32_t)((uint16_t)*(uint32_t *)(var_14 + 32UL) & (unsigned short)61440U) == 32768U) {
                        *var_9 = (*var_9 + *(uint64_t *)(var_14 + 56UL));
                    } else {
                        *var_8 = 7U;
                    }
                }
                var_15 = *var_10 + 1UL;
                *var_10 = var_15;
                var_11 = var_15;
                var_12 = *var_3;
                var_13 = helper_cc_compute_c_wrapper(var_11 - var_12, var_12, var_2, 17U);
            }
        var_16 = *var_9;
        var_17 = (var_16 > 9UL);
        var_18 = *var_6;
        var_22 = var_18;
        while (!var_17)
            {
                *var_6 = (var_18 + 1U);
                var_21 = (uint64_t)(((unsigned __int128)*var_9 * 14757395258967641293ULL) >> 67ULL);
                *var_9 = var_21;
                var_16 = var_21;
                var_17 = (var_16 > 9UL);
                var_18 = *var_6;
                var_22 = var_18;
            }
        var_19 = (uint64_t)var_18;
        var_20 = *var_8;
        if ((long)(var_19 << 32UL) < (long)((uint64_t)var_20 << 32UL)) {
            *var_6 = var_20;
            var_22 = var_20;
        }
    }
    return (uint64_t)var_22;
}

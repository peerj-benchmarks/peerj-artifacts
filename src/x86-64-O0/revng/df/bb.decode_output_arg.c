typedef unsigned char __u_char;
typedef unsigned short __u_short;
typedef unsigned int __u_int;
typedef unsigned long __u_long;
typedef signed char __int8_t;
typedef unsigned char __uint8_t;
typedef short __int16_t;
typedef unsigned short __uint16_t;
typedef int __int32_t;
typedef unsigned int __uint32_t;
typedef long __int64_t;
typedef unsigned long __uint64_t;
typedef __int8_t __int_least8_t;
typedef __uint8_t __uint_least8_t;
typedef __int16_t __int_least16_t;
typedef __uint16_t __uint_least16_t;
typedef __int32_t __int_least32_t;
typedef __uint32_t __uint_least32_t;
typedef __int64_t __int_least64_t;
typedef __uint64_t __uint_least64_t;
typedef long __quad_t;
typedef unsigned long __u_quad_t;
typedef long __intmax_t;
typedef unsigned long __uintmax_t;
typedef unsigned long __dev_t;
typedef unsigned int __uid_t;
typedef unsigned int __gid_t;
typedef unsigned long __ino_t;
typedef unsigned long __ino64_t;
typedef unsigned int __mode_t;
typedef unsigned long __nlink_t;
typedef long __off_t;
typedef long __off64_t;
typedef int __pid_t;
typedef struct {
    int __val[2];
} __fsid_t;
typedef long __clock_t;
typedef unsigned long __rlim_t;
typedef unsigned long __rlim64_t;
typedef unsigned int __id_t;
typedef long __time_t;
typedef unsigned int __useconds_t;
typedef long __suseconds_t;
typedef int __daddr_t;
typedef int __key_t;
typedef int __clockid_t;
typedef void *__timer_t;
typedef long __blksize_t;
typedef long __blkcnt_t;
typedef long __blkcnt64_t;
typedef unsigned long __fsblkcnt_t;
typedef unsigned long __fsblkcnt64_t;
typedef unsigned long __fsfilcnt_t;
typedef unsigned long __fsfilcnt64_t;
typedef long __fsword_t;
typedef long __ssize_t;
typedef long __syscall_slong_t;
typedef unsigned long __syscall_ulong_t;
typedef __off64_t __loff_t;
typedef char *__caddr_t;
typedef long __intptr_t;
typedef unsigned int __socklen_t;
typedef int __sig_atomic_t;
typedef __int8_t int8_t;
typedef __int16_t int16_t;
typedef __int32_t int32_t;
typedef __int64_t int64_t;
typedef __uint8_t uint8_t;
typedef __uint16_t uint16_t;
typedef __uint32_t uint32_t;
typedef __uint64_t uint64_t;
typedef __int_least8_t int_least8_t;
typedef __int_least16_t int_least16_t;
typedef __int_least32_t int_least32_t;
typedef __int_least64_t int_least64_t;
typedef __uint_least8_t uint_least8_t;
typedef __uint_least16_t uint_least16_t;
typedef __uint_least32_t uint_least32_t;
typedef __uint_least64_t uint_least64_t;
typedef signed char int_fast8_t;
typedef long int_fast16_t;
typedef long int_fast32_t;
typedef long int_fast64_t;
typedef unsigned char uint_fast8_t;
typedef unsigned long uint_fast16_t;
typedef unsigned long uint_fast32_t;
typedef unsigned long uint_fast64_t;
typedef long intptr_t;
typedef unsigned long uintptr_t;
typedef __intmax_t intmax_t;
typedef __uintmax_t uintmax_t;
typedef _Bool bool;
struct indirect_placeholder_103_ret_type;
struct indirect_placeholder_102_ret_type;
struct indirect_placeholder_104_ret_type;
struct indirect_placeholder_101_ret_type;
struct indirect_placeholder_105_ret_type;
struct indirect_placeholder_106_ret_type;
struct indirect_placeholder_107_ret_type;
struct indirect_placeholder_103_ret_type {
    uint64_t field_0;
    uint64_t field_1;
    uint64_t field_2;
};
struct indirect_placeholder_102_ret_type {
    uint64_t field_0;
    uint64_t field_1;
    uint64_t field_2;
    uint64_t field_3;
};
struct indirect_placeholder_104_ret_type {
    uint64_t field_0;
    uint64_t field_1;
    uint64_t field_2;
};
struct indirect_placeholder_101_ret_type {
    uint64_t field_0;
    uint64_t field_1;
    uint64_t field_2;
    uint64_t field_3;
};
struct indirect_placeholder_105_ret_type {
    uint64_t field_0;
    uint64_t field_1;
    uint64_t field_2;
    uint64_t field_3;
};
struct indirect_placeholder_106_ret_type {
    uint64_t field_0;
    uint64_t field_1;
    uint64_t field_2;
    uint64_t field_3;
};
struct indirect_placeholder_107_ret_type {
    uint64_t field_0;
    uint64_t field_1;
    uint64_t field_2;
    uint64_t field_3;
};
extern void abort(void);
extern uint64_t revng_init_local_sp(uint64_t param_0);
extern uint64_t init_rbp(void);
extern void indirect_placeholder_1(void);
extern uint64_t init_rdi(void);
extern void indirect_placeholder_17(uint64_t param_0, uint64_t param_1);
extern uint64_t init_rbx(void);
extern uint64_t init_r10(void);
extern uint64_t indirect_placeholder_2(uint64_t param_0);
extern uint64_t init_r9(void);
extern uint64_t init_r8(void);
extern struct indirect_placeholder_103_ret_type indirect_placeholder_103(uint64_t param_0);
extern struct indirect_placeholder_102_ret_type indirect_placeholder_102(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint64_t param_3, uint64_t param_4, uint64_t param_5, uint64_t param_6);
extern struct indirect_placeholder_104_ret_type indirect_placeholder_104(uint64_t param_0);
extern struct indirect_placeholder_101_ret_type indirect_placeholder_101(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint64_t param_3, uint64_t param_4, uint64_t param_5, uint64_t param_6);
extern struct indirect_placeholder_105_ret_type indirect_placeholder_105(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint64_t param_3, uint64_t param_4, uint64_t param_5);
extern struct indirect_placeholder_106_ret_type indirect_placeholder_106(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint64_t param_3, uint64_t param_4, uint64_t param_5);
extern struct indirect_placeholder_107_ret_type indirect_placeholder_107(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint64_t param_3, uint64_t param_4, uint64_t param_5);
void bb_decode_output_arg(void) {
    uint64_t var_0;
    uint64_t var_1;
    uint64_t var_2;
    uint64_t var_3;
    uint64_t var_4;
    uint64_t var_5;
    uint64_t var_6;
    uint64_t var_7;
    uint64_t var_8;
    uint64_t var_9;
    uint64_t *var_10;
    uint64_t *var_11;
    uint32_t *var_12;
    uint32_t *var_13;
    uint64_t var_14;
    uint64_t local_sp_0;
    uint64_t r9_1;
    uint64_t r10_0;
    uint64_t r8_1;
    uint64_t r9_0;
    uint64_t rbx_1;
    uint64_t r8_0;
    uint64_t rbx_0;
    uint64_t var_37;
    uint32_t var_22;
    uint32_t var_21;
    uint32_t var_16;
    uint64_t local_sp_1;
    uint64_t local_sp_2;
    uint64_t var_38;
    struct indirect_placeholder_103_ret_type var_39;
    uint64_t var_40;
    uint64_t var_41;
    uint64_t var_42;
    uint64_t var_23;
    uint64_t var_24;
    uint64_t var_25;
    struct indirect_placeholder_104_ret_type var_26;
    uint64_t var_27;
    uint64_t var_28;
    uint64_t var_29;
    uint64_t var_30;
    struct indirect_placeholder_105_ret_type var_31;
    uint64_t var_32;
    struct indirect_placeholder_106_ret_type var_33;
    uint64_t var_34;
    struct indirect_placeholder_107_ret_type var_35;
    uint64_t var_36;
    uint64_t var_17;
    uint64_t var_18;
    bool var_19;
    uint32_t var_20;
    uint64_t local_sp_3;
    uint64_t r10_1;
    uint64_t var_15;
    unsigned int loop_state_var;
    var_0 = revng_init_local_sp(0UL);
    var_1 = init_rbp();
    var_2 = init_rdi();
    var_3 = init_r10();
    var_4 = init_r9();
    var_5 = init_r8();
    var_6 = init_rbx();
    var_7 = var_0 + (-8L);
    *(uint64_t *)var_7 = var_1;
    *(uint64_t *)(var_0 + (-48L)) = var_2;
    var_8 = var_0 + (-64L);
    *(uint64_t *)var_8 = 4204894UL;
    var_9 = indirect_placeholder_2(var_2);
    *(uint64_t *)(var_0 + (-40L)) = var_9;
    var_10 = (uint64_t *)(var_0 + (-16L));
    *var_10 = var_9;
    var_11 = (uint64_t *)(var_0 + (-24L));
    var_12 = (uint32_t *)(var_0 + (-28L));
    var_13 = (uint32_t *)(var_0 + (-32L));
    var_14 = var_9;
    r9_1 = var_4;
    r8_1 = var_5;
    rbx_1 = var_6;
    var_16 = 0U;
    local_sp_3 = var_8;
    r10_1 = var_3;
    while (1U)
        {
            var_15 = local_sp_3 + (-8L);
            *(uint64_t *)var_15 = 4204923UL;
            indirect_placeholder_1();
            *var_11 = var_14;
            r10_0 = r10_1;
            r9_0 = r9_1;
            r8_0 = r8_1;
            rbx_0 = rbx_1;
            local_sp_1 = var_15;
            if (var_14 == 0UL) {
                *var_11 = (var_14 + 1UL);
                *(unsigned char *)var_14 = (unsigned char)'\x00';
            }
            *var_12 = 12U;
            *var_13 = 0U;
            while (1U)
                {
                    local_sp_2 = local_sp_1;
                    if (var_16 <= 11U) {
                        loop_state_var = 0U;
                        break;
                    }
                    var_17 = *(uint64_t *)(((uint64_t)var_16 * 48UL) + 6433032UL);
                    var_18 = local_sp_1 + (-8L);
                    *(uint64_t *)var_18 = 4205006UL;
                    indirect_placeholder_1();
                    var_19 = ((uint64_t)(uint32_t)var_17 == 0UL);
                    var_20 = *var_13;
                    local_sp_1 = var_18;
                    var_22 = var_20;
                    local_sp_2 = var_18;
                    if (!var_19) {
                        loop_state_var = 1U;
                        break;
                    }
                    var_21 = var_20 + 1U;
                    *var_13 = var_21;
                    var_16 = var_21;
                    continue;
                }
            switch (loop_state_var) {
              case 0U:
                {
                    var_22 = *var_12;
                }
                break;
              case 1U:
                {
                    *var_12 = var_20;
                }
                break;
            }
            if (var_22 == 12U) {
                var_38 = *var_10;
                *(uint64_t *)(local_sp_2 + (-8L)) = 4205046UL;
                var_39 = indirect_placeholder_103(var_38);
                var_40 = var_39.field_0;
                var_41 = var_39.field_1;
                var_42 = var_39.field_2;
                *(uint64_t *)(local_sp_2 + (-16L)) = 4205074UL;
                indirect_placeholder_102(0UL, 4312248UL, var_40, 0UL, 0UL, var_41, var_42);
                *(uint64_t *)(local_sp_2 + (-24L)) = 4205084UL;
                indirect_placeholder_17(var_7, 1UL);
                abort();
            }
            var_23 = (uint64_t)var_22;
            var_24 = var_23 * 48UL;
            if (*(unsigned char *)(var_24 + 6433068UL) == '\x00') {
                var_25 = *(uint64_t *)(var_24 + 6433032UL);
                *(uint64_t *)(local_sp_2 + (-8L)) = 4205148UL;
                var_26 = indirect_placeholder_104(var_25);
                var_27 = var_26.field_0;
                var_28 = var_26.field_1;
                var_29 = var_26.field_2;
                *(uint64_t *)(local_sp_2 + (-16L)) = 4205176UL;
                indirect_placeholder_101(0UL, 4312288UL, var_27, 0UL, 0UL, var_28, var_29);
                *(uint64_t *)(local_sp_2 + (-24L)) = 4205186UL;
                indirect_placeholder_17(var_7, 1UL);
                abort();
            }
            if (var_22 > 11U) {
                var_36 = local_sp_2 + (-8L);
                *(uint64_t *)var_36 = 4205281UL;
                indirect_placeholder_1();
                local_sp_0 = var_36;
            } else {
                switch (*(uint64_t *)((var_23 << 3UL) + 4312368UL)) {
                  case 4205205UL:
                    {
                        var_34 = local_sp_2 + (-8L);
                        *(uint64_t *)var_34 = 4205220UL;
                        var_35 = indirect_placeholder_107(var_23, 0UL, r10_1, r9_1, r8_1, rbx_1);
                        local_sp_0 = var_34;
                        r10_0 = var_35.field_0;
                        r9_0 = var_35.field_1;
                        r8_0 = var_35.field_2;
                        rbx_0 = var_35.field_3;
                    }
                    break;
                  case 4205222UL:
                    {
                        var_32 = local_sp_2 + (-8L);
                        *(uint64_t *)var_32 = 4205237UL;
                        var_33 = indirect_placeholder_106(var_23, 4312334UL, r10_1, r9_1, r8_1, rbx_1);
                        local_sp_0 = var_32;
                        r10_0 = var_33.field_0;
                        r9_0 = var_33.field_1;
                        r8_0 = var_33.field_2;
                        rbx_0 = var_33.field_3;
                    }
                    break;
                  case 4205239UL:
                    {
                        var_30 = local_sp_2 + (-8L);
                        *(uint64_t *)var_30 = 4205254UL;
                        var_31 = indirect_placeholder_105(var_23, 4312339UL, r10_1, r9_1, r8_1, rbx_1);
                        local_sp_0 = var_30;
                        r10_0 = var_31.field_0;
                        r9_0 = var_31.field_1;
                        r8_0 = var_31.field_2;
                        rbx_0 = var_31.field_3;
                    }
                    break;
                  default:
                    {
                        abort();
                    }
                    break;
                }
            }
        }
}

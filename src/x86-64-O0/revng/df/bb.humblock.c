typedef unsigned char __u_char;
typedef unsigned short __u_short;
typedef unsigned int __u_int;
typedef unsigned long __u_long;
typedef signed char __int8_t;
typedef unsigned char __uint8_t;
typedef short __int16_t;
typedef unsigned short __uint16_t;
typedef int __int32_t;
typedef unsigned int __uint32_t;
typedef long __int64_t;
typedef unsigned long __uint64_t;
typedef __int8_t __int_least8_t;
typedef __uint8_t __uint_least8_t;
typedef __int16_t __int_least16_t;
typedef __uint16_t __uint_least16_t;
typedef __int32_t __int_least32_t;
typedef __uint32_t __uint_least32_t;
typedef __int64_t __int_least64_t;
typedef __uint64_t __uint_least64_t;
typedef long __quad_t;
typedef unsigned long __u_quad_t;
typedef long __intmax_t;
typedef unsigned long __uintmax_t;
typedef unsigned long __dev_t;
typedef unsigned int __uid_t;
typedef unsigned int __gid_t;
typedef unsigned long __ino_t;
typedef unsigned long __ino64_t;
typedef unsigned int __mode_t;
typedef unsigned long __nlink_t;
typedef long __off_t;
typedef long __off64_t;
typedef int __pid_t;
typedef struct {
    int __val[2];
} __fsid_t;
typedef long __clock_t;
typedef unsigned long __rlim_t;
typedef unsigned long __rlim64_t;
typedef unsigned int __id_t;
typedef long __time_t;
typedef unsigned int __useconds_t;
typedef long __suseconds_t;
typedef int __daddr_t;
typedef int __key_t;
typedef int __clockid_t;
typedef void *__timer_t;
typedef long __blksize_t;
typedef long __blkcnt_t;
typedef long __blkcnt64_t;
typedef unsigned long __fsblkcnt_t;
typedef unsigned long __fsblkcnt64_t;
typedef unsigned long __fsfilcnt_t;
typedef unsigned long __fsfilcnt64_t;
typedef long __fsword_t;
typedef long __ssize_t;
typedef long __syscall_slong_t;
typedef unsigned long __syscall_ulong_t;
typedef __off64_t __loff_t;
typedef char *__caddr_t;
typedef long __intptr_t;
typedef unsigned int __socklen_t;
typedef int __sig_atomic_t;
typedef __int8_t int8_t;
typedef __int16_t int16_t;
typedef __int32_t int32_t;
typedef __int64_t int64_t;
typedef __uint8_t uint8_t;
typedef __uint16_t uint16_t;
typedef __uint32_t uint32_t;
typedef __uint64_t uint64_t;
typedef __int_least8_t int_least8_t;
typedef __int_least16_t int_least16_t;
typedef __int_least32_t int_least32_t;
typedef __int_least64_t int_least64_t;
typedef __uint_least8_t uint_least8_t;
typedef __uint_least16_t uint_least16_t;
typedef __uint_least32_t uint_least32_t;
typedef __uint_least64_t uint_least64_t;
typedef signed char int_fast8_t;
typedef long int_fast16_t;
typedef long int_fast32_t;
typedef long int_fast64_t;
typedef unsigned char uint_fast8_t;
typedef unsigned long uint_fast16_t;
typedef unsigned long uint_fast32_t;
typedef unsigned long uint_fast64_t;
typedef long intptr_t;
typedef unsigned long uintptr_t;
typedef __intmax_t intmax_t;
typedef __uintmax_t uintmax_t;
extern uint64_t revng_init_local_sp(uint64_t param_0);
extern uint64_t init_rbp(void);
extern void indirect_placeholder_1(void);
extern uint64_t indirect_placeholder_3(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint64_t param_3, uint64_t param_4);
extern uint64_t indirect_placeholder_29(void);
extern uint64_t indirect_placeholder_30(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint64_t param_3);
extern uint64_t init_rax(void);
uint64_t bb_humblock(uint64_t rdx, uint64_t rdi, uint64_t rsi) {
    uint64_t var_22;
    uint64_t var_0;
    uint64_t var_1;
    uint64_t var_2;
    uint64_t var_3;
    uint64_t var_4;
    uint64_t *var_5;
    uint64_t var_6;
    uint64_t *var_7;
    uint64_t var_8;
    uint32_t *var_9;
    uint64_t var_10;
    uint64_t var_12;
    uint64_t var_25;
    uint64_t storemerge;
    uint64_t *var_26;
    unsigned char var_27;
    uint64_t var_28;
    uint64_t var_29;
    uint32_t var_30;
    uint64_t var_31;
    unsigned char *_cast4;
    unsigned char *_cast5_pre_phi;
    uint32_t var_32;
    uint64_t _pre;
    uint32_t var_33;
    uint64_t var_34;
    uint64_t var_19;
    uint64_t var_20;
    uint64_t var_21;
    uint32_t *var_23;
    uint32_t var_24;
    uint64_t var_35;
    uint64_t var_11;
    uint64_t local_sp_0;
    unsigned char **var_13;
    uint64_t var_15;
    uint64_t var_14;
    uint64_t var_16;
    uint32_t *var_17;
    uint32_t var_18;
    unsigned int loop_state_var;
    var_0 = revng_init_local_sp(0UL);
    var_1 = init_rax();
    var_2 = init_rbp();
    *(uint64_t *)(var_0 + (-8L)) = var_2;
    var_3 = var_0 + (-72L);
    var_4 = var_0 + (-48L);
    var_5 = (uint64_t *)var_4;
    *var_5 = rdi;
    var_6 = var_0 + (-56L);
    var_7 = (uint64_t *)var_6;
    *var_7 = rsi;
    var_8 = var_0 + (-64L);
    *(uint64_t *)var_8 = rdx;
    var_9 = (uint32_t *)(var_0 + (-12L));
    *var_9 = 0U;
    var_10 = *var_5;
    storemerge = 0UL;
    var_12 = var_10;
    local_sp_0 = var_3;
    if (var_10 != 0UL) {
        var_11 = var_0 + (-80L);
        *(uint64_t *)var_11 = 4228816UL;
        indirect_placeholder_1();
        *var_5 = var_1;
        var_12 = var_1;
        local_sp_0 = var_11;
        if (var_1 != 0UL) {
            *(uint64_t *)(var_0 + (-88L)) = 4228837UL;
            indirect_placeholder_1();
            *var_5 = 0UL;
            *(uint64_t *)(var_0 + (-96L)) = 4228853UL;
            var_35 = indirect_placeholder_29();
            **(uint64_t **)var_6 = var_35;
            **(uint32_t **)var_8 = *var_9;
            return storemerge;
        }
    }
    var_13 = (unsigned char **)var_4;
    var_15 = var_12;
    if (**var_13 == '\'') {
        *var_9 = (*var_9 | 4U);
        var_14 = *var_5 + 1UL;
        *var_5 = var_14;
        var_15 = var_14;
    }
    *(uint64_t *)(local_sp_0 + (-8L)) = 4228915UL;
    var_16 = indirect_placeholder_30(4315464UL, 4UL, var_15, 4315440UL);
    var_17 = (uint32_t *)(var_0 + (-16L));
    var_18 = (uint32_t)var_16;
    *var_17 = var_18;
    if ((int)var_18 >= (int)0U) {
        *var_9 = (*(uint32_t *)(((uint64_t)var_18 << 2UL) + 4315464UL) | *var_9);
        **(uint64_t **)var_6 = 1UL;
        **(uint32_t **)var_8 = *var_9;
        return storemerge;
    }
    var_19 = *var_7;
    var_20 = var_0 + (-32L);
    var_21 = *var_5;
    *(uint64_t *)(local_sp_0 + (-16L)) = 4228989UL;
    var_22 = indirect_placeholder_3(0UL, var_19, var_21, var_20, 4315509UL);
    var_23 = (uint32_t *)(var_0 + (-20L));
    var_24 = (uint32_t)var_22;
    *var_23 = var_24;
    if (var_24 != 0U) {
        **(uint32_t **)var_8 = 0U;
        var_25 = (uint64_t)*var_23;
        storemerge = var_25;
        return storemerge;
    }
    var_26 = (uint64_t *)var_20;
    while (1U)
        {
            var_27 = **var_13;
            if (!(((signed char)var_27 <= '/') || ((signed char)var_27 > '9'))) {
                loop_state_var = 1U;
                break;
            }
            var_28 = *var_26;
            var_29 = *var_5;
            if (var_29 == var_28) {
                *var_5 = (var_29 + 1UL);
                continue;
            }
            var_30 = *var_9 | 128U;
            *var_9 = var_30;
            var_31 = *var_26;
            _cast4 = (unsigned char *)(var_31 + (-1L));
            _cast5_pre_phi = _cast4;
            var_33 = var_30;
            var_34 = var_31;
            if (*_cast4 != 'B') {
                loop_state_var = 0U;
                break;
            }
            var_32 = var_30 | 256U;
            *var_9 = var_32;
            _pre = *var_26;
            _cast5_pre_phi = (unsigned char *)(_pre + (-1L));
            var_33 = var_32;
            var_34 = _pre;
            loop_state_var = 0U;
            break;
        }
    if (*_cast5_pre_phi == 'B') {
        *var_9 = (var_33 | 32U);
    } else {
        if (*(unsigned char *)(var_34 + (-2L)) == 'i') {
            *var_9 = (var_33 | 32U);
        }
    }
}

typedef unsigned char __u_char;
typedef unsigned short __u_short;
typedef unsigned int __u_int;
typedef unsigned long __u_long;
typedef signed char __int8_t;
typedef unsigned char __uint8_t;
typedef short __int16_t;
typedef unsigned short __uint16_t;
typedef int __int32_t;
typedef unsigned int __uint32_t;
typedef long __int64_t;
typedef unsigned long __uint64_t;
typedef __int8_t __int_least8_t;
typedef __uint8_t __uint_least8_t;
typedef __int16_t __int_least16_t;
typedef __uint16_t __uint_least16_t;
typedef __int32_t __int_least32_t;
typedef __uint32_t __uint_least32_t;
typedef __int64_t __int_least64_t;
typedef __uint64_t __uint_least64_t;
typedef long __quad_t;
typedef unsigned long __u_quad_t;
typedef long __intmax_t;
typedef unsigned long __uintmax_t;
typedef unsigned long __dev_t;
typedef unsigned int __uid_t;
typedef unsigned int __gid_t;
typedef unsigned long __ino_t;
typedef unsigned long __ino64_t;
typedef unsigned int __mode_t;
typedef unsigned long __nlink_t;
typedef long __off_t;
typedef long __off64_t;
typedef int __pid_t;
typedef struct {
    int __val[2];
} __fsid_t;
typedef long __clock_t;
typedef unsigned long __rlim_t;
typedef unsigned long __rlim64_t;
typedef unsigned int __id_t;
typedef long __time_t;
typedef unsigned int __useconds_t;
typedef long __suseconds_t;
typedef int __daddr_t;
typedef int __key_t;
typedef int __clockid_t;
typedef void *__timer_t;
typedef long __blksize_t;
typedef long __blkcnt_t;
typedef long __blkcnt64_t;
typedef unsigned long __fsblkcnt_t;
typedef unsigned long __fsblkcnt64_t;
typedef unsigned long __fsfilcnt_t;
typedef unsigned long __fsfilcnt64_t;
typedef long __fsword_t;
typedef long __ssize_t;
typedef long __syscall_slong_t;
typedef unsigned long __syscall_ulong_t;
typedef __off64_t __loff_t;
typedef char *__caddr_t;
typedef long __intptr_t;
typedef unsigned int __socklen_t;
typedef int __sig_atomic_t;
typedef __int8_t int8_t;
typedef __int16_t int16_t;
typedef __int32_t int32_t;
typedef __int64_t int64_t;
typedef __uint8_t uint8_t;
typedef __uint16_t uint16_t;
typedef __uint32_t uint32_t;
typedef __uint64_t uint64_t;
typedef __int_least8_t int_least8_t;
typedef __int_least16_t int_least16_t;
typedef __int_least32_t int_least32_t;
typedef __int_least64_t int_least64_t;
typedef __uint_least8_t uint_least8_t;
typedef __uint_least16_t uint_least16_t;
typedef __uint_least32_t uint_least32_t;
typedef __uint_least64_t uint_least64_t;
typedef signed char int_fast8_t;
typedef long int_fast16_t;
typedef long int_fast32_t;
typedef long int_fast64_t;
typedef unsigned char uint_fast8_t;
typedef unsigned long uint_fast16_t;
typedef unsigned long uint_fast32_t;
typedef unsigned long uint_fast64_t;
typedef long intptr_t;
typedef unsigned long uintptr_t;
typedef __intmax_t intmax_t;
typedef __uintmax_t uintmax_t;
typedef _Bool bool;
extern uint64_t revng_init_local_sp(uint64_t param_0);
extern uint64_t init_rbp(void);
extern uint64_t init_cc_src2(void);
extern uint64_t helper_cc_compute_c_wrapper(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint32_t param_3);
extern void indirect_placeholder_1(void);
extern uint64_t indirect_placeholder_13(uint64_t param_0, uint64_t param_1, uint64_t param_2);
uint64_t bb_two_way_short_needle(uint64_t rdx, uint64_t rcx, uint64_t rdi, uint64_t rsi) {
    uint64_t var_0;
    uint64_t var_1;
    uint64_t var_2;
    uint64_t *var_3;
    uint64_t *var_4;
    uint64_t *var_5;
    uint64_t *var_6;
    uint64_t var_7;
    uint64_t var_8;
    uint64_t var_9;
    uint64_t *var_10;
    uint64_t var_11;
    uint64_t var_12;
    uint64_t var_28;
    uint64_t var_29;
    uint64_t var_30;
    uint64_t var_31;
    uint64_t var_32;
    uint64_t var_33;
    uint64_t var_36;
    uint64_t var_34;
    uint64_t var_35;
    uint64_t var_38;
    bool var_37;
    uint64_t var_39;
    uint64_t var_45;
    uint64_t var_40;
    bool var_41;
    uint64_t var_42;
    uint64_t var_43;
    uint64_t var_21;
    uint64_t var_44;
    uint64_t var_57;
    uint64_t var_58;
    uint64_t var_59;
    uint64_t var_60;
    uint64_t var_61;
    uint64_t var_62;
    uint64_t var_63;
    uint64_t var_64;
    uint64_t var_65;
    uint64_t var_68;
    uint64_t var_66;
    uint64_t var_67;
    uint64_t var_70;
    bool var_69;
    uint64_t local_sp_0;
    uint64_t var_71;
    uint64_t var_72;
    uint64_t var_73;
    uint64_t var_74;
    uint64_t var_77;
    uint64_t var_75;
    uint64_t var_76;
    uint64_t var_50;
    uint64_t rax_0;
    uint64_t *var_46;
    uint64_t *var_47;
    uint64_t *var_48;
    uint64_t *var_49;
    uint64_t var_51;
    uint64_t var_52;
    uint64_t var_53;
    uint64_t var_54;
    uint64_t var_55;
    uint64_t var_56;
    uint64_t var_13;
    uint64_t var_14;
    uint64_t var_15;
    uint64_t var_16;
    uint64_t var_17;
    uint64_t *var_18;
    uint64_t *var_19;
    uint64_t *var_20;
    uint64_t local_sp_1;
    uint64_t var_22;
    uint64_t var_23;
    uint64_t var_24;
    uint64_t var_25;
    uint64_t var_26;
    uint64_t var_27;
    unsigned int loop_state_var;
    var_0 = revng_init_local_sp(0UL);
    var_1 = init_rbp();
    var_2 = init_cc_src2();
    *(uint64_t *)(var_0 + (-8L)) = var_1;
    var_3 = (uint64_t *)(var_0 + (-64L));
    *var_3 = rdi;
    var_4 = (uint64_t *)(var_0 + (-72L));
    *var_4 = rsi;
    var_5 = (uint64_t *)(var_0 + (-80L));
    *var_5 = rdx;
    var_6 = (uint64_t *)(var_0 + (-88L));
    *var_6 = rcx;
    var_7 = var_0 + (-48L);
    var_8 = *var_5;
    *(uint64_t *)(var_0 + (-96L)) = 4253783UL;
    var_9 = indirect_placeholder_13(var_7, var_8, rcx);
    var_10 = (uint64_t *)(var_0 + (-40L));
    *var_10 = var_9;
    var_11 = *var_5;
    var_12 = var_0 + (-104L);
    *(uint64_t *)var_12 = 4253818UL;
    indirect_placeholder_1();
    var_21 = 0UL;
    local_sp_0 = var_12;
    var_50 = 0UL;
    rax_0 = 0UL;
    local_sp_1 = var_12;
    if ((uint64_t)(uint32_t)var_11 != 0UL) {
        var_13 = *var_6;
        var_14 = *var_10;
        var_15 = var_13 - var_14;
        var_16 = helper_cc_compute_c_wrapper(var_15 - var_14, var_14, var_2, 17U);
        var_17 = ((var_16 == 0UL) ? var_15 : var_14) + 1UL;
        var_18 = (uint64_t *)var_7;
        *var_18 = var_17;
        var_19 = (uint64_t *)(var_0 + (-24L));
        *var_19 = 0UL;
        var_20 = (uint64_t *)(var_0 + (-16L));
        var_22 = *var_6 + var_21;
        var_23 = *var_4;
        var_24 = var_22 - var_23;
        var_25 = *var_3 + var_23;
        var_26 = local_sp_1 + (-8L);
        *(uint64_t *)var_26 = 4254463UL;
        var_27 = indirect_placeholder_13(var_24, var_25, 0UL);
        local_sp_1 = var_26;
        while (var_27 != 0UL)
            {
                var_28 = *var_6 + *var_19;
                *var_4 = var_28;
                if (var_28 == 0UL) {
                    break;
                }
                var_29 = *var_10;
                *var_20 = var_29;
                var_30 = var_29;
                while (1U)
                    {
                        var_31 = *var_6;
                        var_32 = helper_cc_compute_c_wrapper(var_30 - var_31, var_31, var_2, 17U);
                        if (var_32 != 0UL) {
                            loop_state_var = 1U;
                            break;
                        }
                        var_33 = *var_5;
                        var_34 = *var_20;
                        var_36 = var_34;
                        if ((uint64_t)(*(unsigned char *)(var_34 + var_33) - *(unsigned char *)(*var_3 + (var_34 + *var_19))) != 0UL) {
                            loop_state_var = 0U;
                            break;
                        }
                        var_35 = var_34 + 1UL;
                        *var_20 = var_35;
                        var_30 = var_35;
                        continue;
                    }
                switch (loop_state_var) {
                  case 0U:
                    {
                    }
                    break;
                  case 1U:
                    {
                        var_36 = *var_20;
                    }
                    break;
                }
                var_37 = (*var_6 > var_36);
                var_38 = *var_10;
                if (var_37) {
                    var_44 = (*var_19 + (var_36 - var_38)) + 1UL;
                    *var_19 = var_44;
                    var_45 = var_44;
                } else {
                    var_39 = var_38 + (-1L);
                    *var_20 = var_39;
                    var_40 = var_39;
                    var_41 = (var_40 == 18446744073709551615UL);
                    while (!var_41)
                        {
                            if ((uint64_t)(*(unsigned char *)(var_40 + *var_5) - *(unsigned char *)(*var_3 + (var_40 + *var_19))) == 0UL) {
                                break;
                            }
                            var_42 = var_40 + (-1L);
                            *var_20 = var_42;
                            var_40 = var_42;
                            var_41 = (var_40 == 18446744073709551615UL);
                        }
                    if (!var_41) {
                        rax_0 = *var_19 + *var_3;
                        break;
                    }
                    var_43 = *var_19 + *var_18;
                    *var_19 = var_43;
                    var_45 = var_43;
                }
                var_21 = var_45;
                var_22 = *var_6 + var_21;
                var_23 = *var_4;
                var_24 = var_22 - var_23;
                var_25 = *var_3 + var_23;
                var_26 = local_sp_1 + (-8L);
                *(uint64_t *)var_26 = 4254463UL;
                var_27 = indirect_placeholder_13(var_24, var_25, 0UL);
                local_sp_1 = var_26;
            }
    }
    var_46 = (uint64_t *)(var_0 + (-32L));
    *var_46 = 0UL;
    var_47 = (uint64_t *)(var_0 + (-24L));
    *var_47 = 0UL;
    var_48 = (uint64_t *)(var_0 + (-16L));
    var_49 = (uint64_t *)var_7;
    var_51 = *var_6 + var_50;
    var_52 = *var_4;
    var_53 = var_51 - var_52;
    var_54 = *var_3 + var_52;
    var_55 = local_sp_0 + (-8L);
    *(uint64_t *)var_55 = 4254147UL;
    var_56 = indirect_placeholder_13(var_53, var_54, 0UL);
    local_sp_0 = var_55;
    while (var_56 != 0UL)
        {
            var_57 = *var_6 + *var_47;
            *var_4 = var_57;
            if (var_57 == 0UL) {
                break;
            }
            var_58 = *var_46;
            var_59 = *var_10;
            var_60 = helper_cc_compute_c_wrapper(var_59 - var_58, var_58, var_2, 17U);
            var_61 = (var_60 == 0UL) ? var_59 : var_58;
            *var_48 = var_61;
            var_62 = var_61;
            while (1U)
                {
                    var_63 = *var_6;
                    var_64 = helper_cc_compute_c_wrapper(var_62 - var_63, var_63, var_2, 17U);
                    if (var_64 != 0UL) {
                        loop_state_var = 0U;
                        break;
                    }
                    var_65 = *var_5;
                    var_66 = *var_48;
                    var_68 = var_66;
                    if ((uint64_t)(*(unsigned char *)(var_66 + var_65) - *(unsigned char *)(*var_3 + (var_66 + *var_47))) != 0UL) {
                        loop_state_var = 1U;
                        break;
                    }
                    var_67 = var_66 + 1UL;
                    *var_48 = var_67;
                    var_62 = var_67;
                    continue;
                }
            switch (loop_state_var) {
              case 0U:
                {
                    var_68 = *var_48;
                }
                break;
              case 1U:
                {
                }
                break;
            }
            var_69 = (*var_6 > var_68);
            var_70 = *var_10;
            if (var_69) {
                *var_47 = ((*var_47 + (var_68 - var_70)) + 1UL);
                *var_46 = 0UL;
            } else {
                var_71 = var_70 + (-1L);
                *var_48 = var_71;
                var_72 = var_71;
                var_73 = var_72 + 1UL;
                var_74 = *var_46;
                while (var_73 <= var_74)
                    {
                        if ((uint64_t)(*(unsigned char *)(var_72 + *var_5) - *(unsigned char *)(*var_3 + (var_72 + *var_47))) == 0UL) {
                            break;
                        }
                        var_77 = var_72 + (-1L);
                        *var_48 = var_77;
                        var_72 = var_77;
                        var_73 = var_72 + 1UL;
                        var_74 = *var_46;
                    }
                var_75 = var_74 + 1UL;
                var_76 = helper_cc_compute_c_wrapper(var_73 - var_75, var_75, var_2, 17U);
                if (var_76 != 0UL) {
                    rax_0 = *var_47 + *var_3;
                    break;
                }
                *var_47 = (*var_47 + *var_49);
                *var_46 = (*var_6 - *var_49);
            }
            var_50 = *var_47;
            var_51 = *var_6 + var_50;
            var_52 = *var_4;
            var_53 = var_51 - var_52;
            var_54 = *var_3 + var_52;
            var_55 = local_sp_0 + (-8L);
            *(uint64_t *)var_55 = 4254147UL;
            var_56 = indirect_placeholder_13(var_53, var_54, 0UL);
            local_sp_0 = var_55;
        }
    return rax_0;
}

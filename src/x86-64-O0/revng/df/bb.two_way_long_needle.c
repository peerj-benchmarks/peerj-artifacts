typedef unsigned char __u_char;
typedef unsigned short __u_short;
typedef unsigned int __u_int;
typedef unsigned long __u_long;
typedef signed char __int8_t;
typedef unsigned char __uint8_t;
typedef short __int16_t;
typedef unsigned short __uint16_t;
typedef int __int32_t;
typedef unsigned int __uint32_t;
typedef long __int64_t;
typedef unsigned long __uint64_t;
typedef __int8_t __int_least8_t;
typedef __uint8_t __uint_least8_t;
typedef __int16_t __int_least16_t;
typedef __uint16_t __uint_least16_t;
typedef __int32_t __int_least32_t;
typedef __uint32_t __uint_least32_t;
typedef __int64_t __int_least64_t;
typedef __uint64_t __uint_least64_t;
typedef long __quad_t;
typedef unsigned long __u_quad_t;
typedef long __intmax_t;
typedef unsigned long __uintmax_t;
typedef unsigned long __dev_t;
typedef unsigned int __uid_t;
typedef unsigned int __gid_t;
typedef unsigned long __ino_t;
typedef unsigned long __ino64_t;
typedef unsigned int __mode_t;
typedef unsigned long __nlink_t;
typedef long __off_t;
typedef long __off64_t;
typedef int __pid_t;
typedef struct {
    int __val[2];
} __fsid_t;
typedef long __clock_t;
typedef unsigned long __rlim_t;
typedef unsigned long __rlim64_t;
typedef unsigned int __id_t;
typedef long __time_t;
typedef unsigned int __useconds_t;
typedef long __suseconds_t;
typedef int __daddr_t;
typedef int __key_t;
typedef int __clockid_t;
typedef void *__timer_t;
typedef long __blksize_t;
typedef long __blkcnt_t;
typedef long __blkcnt64_t;
typedef unsigned long __fsblkcnt_t;
typedef unsigned long __fsblkcnt64_t;
typedef unsigned long __fsfilcnt_t;
typedef unsigned long __fsfilcnt64_t;
typedef long __fsword_t;
typedef long __ssize_t;
typedef long __syscall_slong_t;
typedef unsigned long __syscall_ulong_t;
typedef __off64_t __loff_t;
typedef char *__caddr_t;
typedef long __intptr_t;
typedef unsigned int __socklen_t;
typedef int __sig_atomic_t;
typedef __int8_t int8_t;
typedef __int16_t int16_t;
typedef __int32_t int32_t;
typedef __int64_t int64_t;
typedef __uint8_t uint8_t;
typedef __uint16_t uint16_t;
typedef __uint32_t uint32_t;
typedef __uint64_t uint64_t;
typedef __int_least8_t int_least8_t;
typedef __int_least16_t int_least16_t;
typedef __int_least32_t int_least32_t;
typedef __int_least64_t int_least64_t;
typedef __uint_least8_t uint_least8_t;
typedef __uint_least16_t uint_least16_t;
typedef __uint_least32_t uint_least32_t;
typedef __uint_least64_t uint_least64_t;
typedef signed char int_fast8_t;
typedef long int_fast16_t;
typedef long int_fast32_t;
typedef long int_fast64_t;
typedef unsigned char uint_fast8_t;
typedef unsigned long uint_fast16_t;
typedef unsigned long uint_fast32_t;
typedef unsigned long uint_fast64_t;
typedef long intptr_t;
typedef unsigned long uintptr_t;
typedef __intmax_t intmax_t;
typedef __uintmax_t uintmax_t;
typedef _Bool bool;
extern uint64_t revng_init_local_sp(uint64_t param_0);
extern uint64_t init_rbp(void);
extern uint64_t init_cc_src2(void);
extern uint64_t helper_cc_compute_c_wrapper(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint32_t param_3);
extern void indirect_placeholder_1(void);
extern uint64_t indirect_placeholder_13(uint64_t param_0, uint64_t param_1, uint64_t param_2);
uint64_t bb_two_way_long_needle(uint64_t rdx, uint64_t rcx, uint64_t rdi, uint64_t rsi) {
    uint64_t var_0;
    uint64_t var_1;
    uint64_t var_2;
    uint64_t var_3;
    uint64_t *var_4;
    uint64_t *var_5;
    uint64_t *var_6;
    uint64_t *var_7;
    uint64_t var_8;
    uint64_t var_9;
    uint64_t var_10;
    uint64_t *var_11;
    uint64_t *var_12;
    uint64_t var_38;
    uint64_t rax_0;
    uint64_t var_39;
    uint64_t var_40;
    uint64_t var_46;
    uint64_t var_51;
    uint64_t var_31;
    uint64_t var_41;
    uint64_t var_42;
    bool var_43;
    uint64_t var_52;
    uint64_t var_44;
    uint64_t var_45;
    bool var_47;
    uint64_t var_48;
    uint64_t var_49;
    uint64_t var_50;
    uint64_t var_64;
    uint64_t var_65;
    bool var_66;
    uint64_t var_67;
    uint64_t var_68;
    uint64_t var_69;
    uint64_t var_57;
    uint64_t var_70;
    uint64_t var_71;
    uint64_t var_72;
    uint64_t var_73;
    bool var_74;
    uint64_t var_83;
    uint64_t var_75;
    uint64_t var_76;
    uint64_t var_77;
    uint64_t var_78;
    uint64_t var_79;
    uint64_t var_82;
    uint64_t var_80;
    uint64_t var_81;
    uint64_t local_sp_0;
    uint64_t *var_53;
    uint64_t *var_54;
    uint64_t *var_55;
    uint64_t *var_56;
    uint64_t var_58;
    uint64_t var_59;
    uint64_t var_60;
    uint64_t var_61;
    uint64_t var_62;
    uint64_t var_63;
    uint64_t var_23;
    uint64_t var_24;
    uint64_t var_25;
    uint64_t var_26;
    uint64_t var_27;
    uint64_t *var_28;
    uint64_t *var_29;
    uint64_t *var_30;
    uint64_t local_sp_1;
    uint64_t var_32;
    uint64_t var_33;
    uint64_t var_34;
    uint64_t var_35;
    uint64_t var_36;
    uint64_t var_37;
    uint64_t var_13;
    uint64_t var_15;
    uint64_t var_16;
    uint64_t var_17;
    bool var_18;
    uint64_t var_19;
    uint64_t var_22;
    uint64_t var_20;
    uint64_t var_21;
    uint64_t var_14;
    var_0 = revng_init_local_sp(0UL);
    var_1 = init_rbp();
    var_2 = init_cc_src2();
    var_3 = var_0 + (-8L);
    *(uint64_t *)var_3 = var_1;
    var_4 = (uint64_t *)(var_0 + (-2128L));
    *var_4 = rdi;
    var_5 = (uint64_t *)(var_0 + (-2136L));
    *var_5 = rsi;
    var_6 = (uint64_t *)(var_0 + (-2144L));
    *var_6 = rdx;
    var_7 = (uint64_t *)(var_0 + (-2152L));
    *var_7 = rcx;
    var_8 = var_0 + (-64L);
    var_9 = *var_6;
    *(uint64_t *)(var_0 + (-2160L)) = 4254569UL;
    var_10 = indirect_placeholder_13(var_8, var_9, rcx);
    var_11 = (uint64_t *)(var_0 + (-48L));
    *var_11 = var_10;
    var_12 = (uint64_t *)(var_0 + (-16L));
    *var_12 = 0UL;
    rax_0 = 0UL;
    var_31 = 0UL;
    var_57 = 0UL;
    var_13 = 0UL;
    var_15 = 0UL;
    while (var_13 <= 255UL)
        {
            *(uint64_t *)(((var_13 << 3UL) + var_3) + (-2112L)) = *var_7;
            var_14 = *var_12 + 1UL;
            *var_12 = var_14;
            var_13 = var_14;
        }
    *var_12 = 0UL;
    var_16 = *var_7;
    var_17 = helper_cc_compute_c_wrapper(var_15 - var_16, var_16, var_2, 17U);
    var_18 = (var_17 == 0UL);
    var_19 = *var_6;
    while (!var_18)
        {
            var_20 = *var_12;
            *(uint64_t *)((((uint64_t)*(unsigned char *)(var_20 + var_19) << 3UL) + var_3) + (-2112L)) = (*var_7 + (var_20 ^ (-1L)));
            var_21 = *var_12 + 1UL;
            *var_12 = var_21;
            var_15 = var_21;
            var_16 = *var_7;
            var_17 = helper_cc_compute_c_wrapper(var_15 - var_16, var_16, var_2, 17U);
            var_18 = (var_17 == 0UL);
            var_19 = *var_6;
        }
    var_22 = var_0 + (-2168L);
    *(uint64_t *)var_22 = 4254728UL;
    indirect_placeholder_1();
    local_sp_0 = var_22;
    local_sp_1 = var_22;
    if ((uint64_t)(uint32_t)var_19 != 0UL) {
        var_23 = *var_7;
        var_24 = *var_11;
        var_25 = var_23 - var_24;
        var_26 = helper_cc_compute_c_wrapper(var_25 - var_24, var_24, var_2, 17U);
        var_27 = ((var_26 == 0UL) ? var_25 : var_24) + 1UL;
        var_28 = (uint64_t *)var_8;
        *var_28 = var_27;
        var_29 = (uint64_t *)(var_0 + (-24L));
        *var_29 = 0UL;
        var_30 = (uint64_t *)(var_0 + (-56L));
        var_32 = *var_7 + var_31;
        var_33 = *var_5;
        var_34 = var_32 - var_33;
        var_35 = *var_4 + var_33;
        var_36 = local_sp_1 + (-8L);
        *(uint64_t *)var_36 = 4255655UL;
        var_37 = indirect_placeholder_13(var_34, var_35, 0UL);
        local_sp_1 = var_36;
        while (var_37 != 0UL)
            {
                var_38 = *var_7 + *var_29;
                *var_5 = var_38;
                if (var_38 == 0UL) {
                    break;
                }
                var_39 = *(uint64_t *)((((uint64_t)*(unsigned char *)(*var_4 + ((*var_7 + *var_29) + (-1L))) << 3UL) + var_3) + (-2112L));
                *var_30 = var_39;
                if (var_39 == 0UL) {
                    var_40 = *var_29 + var_39;
                    *var_29 = var_40;
                    var_51 = var_40;
                } else {
                    var_41 = *var_11;
                    *var_12 = var_41;
                    var_42 = var_41;
                    var_43 = ((*var_7 + (-1L)) > var_42);
                    while (!var_43)
                        {
                            if ((uint64_t)(*(unsigned char *)(var_42 + *var_6) - *(unsigned char *)(*var_4 + (var_42 + *var_29))) == 0UL) {
                                break;
                            }
                            var_52 = var_42 + 1UL;
                            *var_12 = var_52;
                            var_42 = var_52;
                            var_43 = ((*var_7 + (-1L)) > var_42);
                        }
                    var_44 = *var_11;
                    if (var_43) {
                        var_50 = (*var_29 + (var_42 - var_44)) + 1UL;
                        *var_29 = var_50;
                        var_51 = var_50;
                    } else {
                        var_45 = var_44 + (-1L);
                        *var_12 = var_45;
                        var_46 = var_45;
                        var_47 = (var_46 == 18446744073709551615UL);
                        while (!var_47)
                            {
                                if ((uint64_t)(*(unsigned char *)(var_46 + *var_6) - *(unsigned char *)(*var_4 + (var_46 + *var_29))) == 0UL) {
                                    break;
                                }
                                var_48 = var_46 + (-1L);
                                *var_12 = var_48;
                                var_46 = var_48;
                                var_47 = (var_46 == 18446744073709551615UL);
                            }
                        if (!var_47) {
                            rax_0 = *var_29 + *var_4;
                            break;
                        }
                        var_49 = *var_29 + *var_28;
                        *var_29 = var_49;
                        var_51 = var_49;
                    }
                }
                var_31 = var_51;
                var_32 = *var_7 + var_31;
                var_33 = *var_5;
                var_34 = var_32 - var_33;
                var_35 = *var_4 + var_33;
                var_36 = local_sp_1 + (-8L);
                *(uint64_t *)var_36 = 4255655UL;
                var_37 = indirect_placeholder_13(var_34, var_35, 0UL);
                local_sp_1 = var_36;
            }
    }
    var_53 = (uint64_t *)(var_0 + (-32L));
    *var_53 = 0UL;
    var_54 = (uint64_t *)(var_0 + (-24L));
    *var_54 = 0UL;
    var_55 = (uint64_t *)(var_0 + (-40L));
    var_56 = (uint64_t *)var_8;
    var_58 = *var_7 + var_57;
    var_59 = *var_5;
    var_60 = var_58 - var_59;
    var_61 = *var_4 + var_59;
    var_62 = local_sp_0 + (-8L);
    *(uint64_t *)var_62 = 4255215UL;
    var_63 = indirect_placeholder_13(var_60, var_61, 0UL);
    local_sp_0 = var_62;
    while (var_63 != 0UL)
        {
            var_64 = *var_7 + *var_54;
            *var_5 = var_64;
            if (var_64 == 0UL) {
                break;
            }
            var_65 = *(uint64_t *)((((uint64_t)*(unsigned char *)(*var_4 + ((*var_7 + *var_54) + (-1L))) << 3UL) + var_3) + (-2112L));
            *var_55 = var_65;
            var_66 = (var_65 == 0UL);
            var_67 = *var_53;
            if (var_66) {
                var_70 = *var_11;
                var_71 = helper_cc_compute_c_wrapper(var_70 - var_67, var_67, var_2, 17U);
                var_72 = (var_71 == 0UL) ? var_70 : var_67;
                *var_12 = var_72;
                var_73 = var_72;
                var_74 = ((*var_7 + (-1L)) > var_73);
                while (!var_74)
                    {
                        if ((uint64_t)(*(unsigned char *)(var_73 + *var_6) - *(unsigned char *)(*var_4 + (var_73 + *var_54))) == 0UL) {
                            break;
                        }
                        var_83 = var_73 + 1UL;
                        *var_12 = var_83;
                        var_73 = var_83;
                        var_74 = ((*var_7 + (-1L)) > var_73);
                    }
                var_75 = *var_11;
                if (var_74) {
                    *var_54 = ((*var_54 + (var_73 - var_75)) + 1UL);
                    *var_53 = 0UL;
                } else {
                    var_76 = var_75 + (-1L);
                    *var_12 = var_76;
                    var_77 = var_76;
                    var_78 = var_77 + 1UL;
                    var_79 = *var_53;
                    while (var_78 <= var_79)
                        {
                            if ((uint64_t)(*(unsigned char *)(var_77 + *var_6) - *(unsigned char *)(*var_4 + (var_77 + *var_54))) == 0UL) {
                                break;
                            }
                            var_82 = var_77 + (-1L);
                            *var_12 = var_82;
                            var_77 = var_82;
                            var_78 = var_77 + 1UL;
                            var_79 = *var_53;
                        }
                    var_80 = var_79 + 1UL;
                    var_81 = helper_cc_compute_c_wrapper(var_78 - var_80, var_80, var_2, 17U);
                    if (var_81 != 0UL) {
                        rax_0 = *var_54 + *var_4;
                        break;
                    }
                    *var_54 = (*var_54 + *var_56);
                    *var_53 = (*var_7 - *var_56);
                }
            } else {
                var_68 = *var_56;
                var_69 = helper_cc_compute_c_wrapper(var_65 - var_68, var_68, var_2, 17U);
                if (var_67 != 0UL & var_69 == 0UL) {
                    *var_55 = (*var_7 - *var_56);
                }
                *var_53 = 0UL;
                *var_54 = (*var_54 + *var_55);
            }
            var_57 = *var_54;
            var_58 = *var_7 + var_57;
            var_59 = *var_5;
            var_60 = var_58 - var_59;
            var_61 = *var_4 + var_59;
            var_62 = local_sp_0 + (-8L);
            *(uint64_t *)var_62 = 4255215UL;
            var_63 = indirect_placeholder_13(var_60, var_61, 0UL);
            local_sp_0 = var_62;
        }
    return rax_0;
}

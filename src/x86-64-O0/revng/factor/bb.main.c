typedef unsigned char __u_char;
typedef unsigned short __u_short;
typedef unsigned int __u_int;
typedef unsigned long __u_long;
typedef signed char __int8_t;
typedef unsigned char __uint8_t;
typedef short __int16_t;
typedef unsigned short __uint16_t;
typedef int __int32_t;
typedef unsigned int __uint32_t;
typedef long __int64_t;
typedef unsigned long __uint64_t;
typedef __int8_t __int_least8_t;
typedef __uint8_t __uint_least8_t;
typedef __int16_t __int_least16_t;
typedef __uint16_t __uint_least16_t;
typedef __int32_t __int_least32_t;
typedef __uint32_t __uint_least32_t;
typedef __int64_t __int_least64_t;
typedef __uint64_t __uint_least64_t;
typedef long __quad_t;
typedef unsigned long __u_quad_t;
typedef long __intmax_t;
typedef unsigned long __uintmax_t;
typedef unsigned long __dev_t;
typedef unsigned int __uid_t;
typedef unsigned int __gid_t;
typedef unsigned long __ino_t;
typedef unsigned long __ino64_t;
typedef unsigned int __mode_t;
typedef unsigned long __nlink_t;
typedef long __off_t;
typedef long __off64_t;
typedef int __pid_t;
typedef struct {
    int __val[2];
} __fsid_t;
typedef long __clock_t;
typedef unsigned long __rlim_t;
typedef unsigned long __rlim64_t;
typedef unsigned int __id_t;
typedef long __time_t;
typedef unsigned int __useconds_t;
typedef long __suseconds_t;
typedef int __daddr_t;
typedef int __key_t;
typedef int __clockid_t;
typedef void *__timer_t;
typedef long __blksize_t;
typedef long __blkcnt_t;
typedef long __blkcnt64_t;
typedef unsigned long __fsblkcnt_t;
typedef unsigned long __fsblkcnt64_t;
typedef unsigned long __fsfilcnt_t;
typedef unsigned long __fsfilcnt64_t;
typedef long __fsword_t;
typedef long __ssize_t;
typedef long __syscall_slong_t;
typedef unsigned long __syscall_ulong_t;
typedef __off64_t __loff_t;
typedef char *__caddr_t;
typedef long __intptr_t;
typedef unsigned int __socklen_t;
typedef int __sig_atomic_t;
typedef __int8_t int8_t;
typedef __int16_t int16_t;
typedef __int32_t int32_t;
typedef __int64_t int64_t;
typedef __uint8_t uint8_t;
typedef __uint16_t uint16_t;
typedef __uint32_t uint32_t;
typedef __uint64_t uint64_t;
typedef __int_least8_t int_least8_t;
typedef __int_least16_t int_least16_t;
typedef __int_least32_t int_least32_t;
typedef __int_least64_t int_least64_t;
typedef __uint_least8_t uint_least8_t;
typedef __uint_least16_t uint_least16_t;
typedef __uint_least32_t uint_least32_t;
typedef __uint_least64_t uint_least64_t;
typedef signed char int_fast8_t;
typedef long int_fast16_t;
typedef long int_fast32_t;
typedef long int_fast64_t;
typedef unsigned char uint_fast8_t;
typedef unsigned long uint_fast16_t;
typedef unsigned long uint_fast32_t;
typedef unsigned long uint_fast64_t;
typedef long intptr_t;
typedef unsigned long uintptr_t;
typedef __intmax_t intmax_t;
typedef __uintmax_t uintmax_t;
struct indirect_placeholder_57_ret_type;
struct indirect_placeholder_57_ret_type {
    uint64_t field_0;
    uint64_t field_1;
};
extern void abort(void);
extern uint64_t revng_init_local_sp(uint64_t param_0);
extern uint64_t init_rbp(void);
extern uint64_t indirect_placeholder_1(uint64_t param_0, uint64_t param_1);
extern void indirect_placeholder(void);
extern void indirect_placeholder_39(uint64_t param_0, uint64_t param_1);
extern uint64_t indirect_placeholder_32(void);
extern uint64_t init_rbx(void);
extern void indirect_placeholder_49(uint64_t param_0);
extern uint64_t indirect_placeholder_31(uint64_t param_0);
extern void indirect_placeholder_56(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint64_t param_3, uint64_t param_4, uint64_t param_5);
extern struct indirect_placeholder_57_ret_type indirect_placeholder_57(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint64_t param_3, uint64_t param_4);
void bb_main(uint64_t rdi, uint64_t rsi) {
    struct indirect_placeholder_57_ret_type var_13;
    uint64_t var_0;
    uint64_t var_1;
    uint64_t var_2;
    uint64_t var_3;
    uint32_t *var_4;
    uint64_t var_5;
    uint64_t *var_6;
    uint64_t var_7;
    uint64_t var_8;
    uint32_t *var_9;
    uint64_t local_sp_2;
    uint32_t var_28;
    uint32_t var_24;
    uint64_t var_20;
    unsigned char *var_21;
    uint32_t var_22;
    uint32_t *var_23;
    uint64_t local_sp_0;
    uint64_t var_25;
    uint64_t var_26;
    uint64_t var_27;
    uint64_t local_sp_1;
    uint64_t var_15;
    uint64_t var_16;
    uint64_t var_17;
    uint64_t var_18;
    uint64_t *var_19;
    uint64_t var_10;
    uint64_t var_11;
    uint64_t var_12;
    uint32_t var_14;
    unsigned int loop_state_var;
    var_0 = revng_init_local_sp(0UL);
    var_1 = init_rbp();
    var_2 = init_rbx();
    var_3 = var_0 + (-8L);
    *(uint64_t *)var_3 = var_1;
    *(uint64_t *)(var_0 + (-16L)) = var_2;
    var_4 = (uint32_t *)(var_0 + (-44L));
    *var_4 = (uint32_t)rdi;
    var_5 = var_0 + (-56L);
    var_6 = (uint64_t *)var_5;
    *var_6 = rsi;
    var_7 = **(uint64_t **)var_5;
    *(uint64_t *)(var_0 + (-64L)) = 4216911UL;
    indirect_placeholder_49(var_7);
    *(uint64_t *)(var_0 + (-72L)) = 4216926UL;
    indirect_placeholder();
    *(uint64_t *)(var_0 + (-80L)) = 4216931UL;
    indirect_placeholder();
    *(uint64_t *)(var_0 + (-88L)) = 4216941UL;
    indirect_placeholder();
    var_8 = var_0 + (-96L);
    *(uint64_t *)var_8 = 4216951UL;
    indirect_placeholder();
    var_9 = (uint32_t *)(var_0 + (-36L));
    local_sp_2 = var_8;
    while (1U)
        {
            var_10 = *var_6;
            var_11 = (uint64_t)*var_4;
            var_12 = local_sp_2 + (-8L);
            *(uint64_t *)var_12 = 4217143UL;
            var_13 = indirect_placeholder_57(4291579UL, 4291712UL, var_11, var_10, 0UL);
            var_14 = (uint32_t)var_13.field_0;
            *var_9 = var_14;
            local_sp_0 = var_12;
            local_sp_1 = var_12;
            local_sp_2 = var_12;
            if (var_14 != 4294967295U) {
                if ((int)*(uint32_t *)6415512UL >= (int)*var_4) {
                    var_21 = (unsigned char *)(var_0 + (-25L));
                    *var_21 = (unsigned char)'\x01';
                    var_22 = *(uint32_t *)6415512UL;
                    var_23 = (uint32_t *)(var_0 + (-32L));
                    *var_23 = var_22;
                    var_24 = var_22;
                    loop_state_var = 0U;
                    break;
                }
                *(uint64_t *)(local_sp_2 + (-16L)) = 4217172UL;
                var_20 = indirect_placeholder_32();
                *(unsigned char *)(var_0 + (-25L)) = (unsigned char)var_20;
                loop_state_var = 2U;
                break;
            }
            if ((uint64_t)(var_14 + 130U) == 0UL) {
                *(uint64_t *)(local_sp_2 + (-16L)) = 4217001UL;
                indirect_placeholder_39(var_3, 0UL);
                abort();
            }
            if ((uint64_t)(var_14 + (-128)) == 0UL) {
                *(unsigned char *)6415696UL = (unsigned char)'\x01';
                continue;
            }
            if (var_14 != 4294967165U) {
                loop_state_var = 1U;
                break;
            }
            *(uint64_t *)(local_sp_2 + (-16L)) = 4217016UL;
            var_15 = indirect_placeholder_1(4304911UL, 4304897UL);
            *(uint64_t *)(local_sp_2 + (-24L)) = 4217034UL;
            var_16 = indirect_placeholder_1(4304943UL, 4304924UL);
            var_17 = *(uint64_t *)6415368UL;
            *(uint64_t *)(local_sp_2 + (-32L)) = 0UL;
            var_18 = local_sp_2 + (-40L);
            var_19 = (uint64_t *)var_18;
            *var_19 = var_15;
            *(uint64_t *)(local_sp_2 + (-48L)) = 4217089UL;
            indirect_placeholder_56(0UL, 4291416UL, var_17, 4304886UL, var_16, 4304961UL);
            *var_19 = 4217103UL;
            indirect_placeholder();
            local_sp_1 = var_18;
            loop_state_var = 1U;
            break;
        }
    switch (loop_state_var) {
      case 1U:
        {
            *(uint64_t *)(local_sp_1 + (-8L)) = 4217113UL;
            indirect_placeholder_39(var_3, 1UL);
            abort();
        }
        break;
      case 2U:
      case 0U:
        {
            switch (loop_state_var) {
              case 0U:
                {
                    while ((long)((uint64_t)var_24 << 32UL) >= (long)((uint64_t)*var_4 << 32UL))
                        {
                            var_25 = *(uint64_t *)(*var_6 + ((uint64_t)var_24 << 3UL));
                            var_26 = local_sp_0 + (-8L);
                            *(uint64_t *)var_26 = 4217223UL;
                            var_27 = indirect_placeholder_31(var_25);
                            local_sp_0 = var_26;
                            if ((uint64_t)(unsigned char)var_27 == 1UL) {
                                *var_21 = (unsigned char)'\x00';
                            }
                            var_28 = *var_23 + 1U;
                            *var_23 = var_28;
                            var_24 = var_28;
                        }
                }
                break;
              case 2U:
                {
                    return;
                }
                break;
            }
        }
        break;
    }
}

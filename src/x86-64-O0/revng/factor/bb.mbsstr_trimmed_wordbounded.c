typedef unsigned char __u_char;
typedef unsigned short __u_short;
typedef unsigned int __u_int;
typedef unsigned long __u_long;
typedef signed char __int8_t;
typedef unsigned char __uint8_t;
typedef short __int16_t;
typedef unsigned short __uint16_t;
typedef int __int32_t;
typedef unsigned int __uint32_t;
typedef long __int64_t;
typedef unsigned long __uint64_t;
typedef __int8_t __int_least8_t;
typedef __uint8_t __uint_least8_t;
typedef __int16_t __int_least16_t;
typedef __uint16_t __uint_least16_t;
typedef __int32_t __int_least32_t;
typedef __uint32_t __uint_least32_t;
typedef __int64_t __int_least64_t;
typedef __uint64_t __uint_least64_t;
typedef long __quad_t;
typedef unsigned long __u_quad_t;
typedef long __intmax_t;
typedef unsigned long __uintmax_t;
typedef unsigned long __dev_t;
typedef unsigned int __uid_t;
typedef unsigned int __gid_t;
typedef unsigned long __ino_t;
typedef unsigned long __ino64_t;
typedef unsigned int __mode_t;
typedef unsigned long __nlink_t;
typedef long __off_t;
typedef long __off64_t;
typedef int __pid_t;
typedef struct {
    int __val[2];
} __fsid_t;
typedef long __clock_t;
typedef unsigned long __rlim_t;
typedef unsigned long __rlim64_t;
typedef unsigned int __id_t;
typedef long __time_t;
typedef unsigned int __useconds_t;
typedef long __suseconds_t;
typedef int __daddr_t;
typedef int __key_t;
typedef int __clockid_t;
typedef void *__timer_t;
typedef long __blksize_t;
typedef long __blkcnt_t;
typedef long __blkcnt64_t;
typedef unsigned long __fsblkcnt_t;
typedef unsigned long __fsblkcnt64_t;
typedef unsigned long __fsfilcnt_t;
typedef unsigned long __fsfilcnt64_t;
typedef long __fsword_t;
typedef long __ssize_t;
typedef long __syscall_slong_t;
typedef unsigned long __syscall_ulong_t;
typedef __off64_t __loff_t;
typedef char *__caddr_t;
typedef long __intptr_t;
typedef unsigned int __socklen_t;
typedef int __sig_atomic_t;
typedef __int8_t int8_t;
typedef __int16_t int16_t;
typedef __int32_t int32_t;
typedef __int64_t int64_t;
typedef __uint8_t uint8_t;
typedef __uint16_t uint16_t;
typedef __uint32_t uint32_t;
typedef __uint64_t uint64_t;
typedef __int_least8_t int_least8_t;
typedef __int_least16_t int_least16_t;
typedef __int_least32_t int_least32_t;
typedef __int_least64_t int_least64_t;
typedef __uint_least8_t uint_least8_t;
typedef __uint_least16_t uint_least16_t;
typedef __uint_least32_t uint_least32_t;
typedef __uint_least64_t uint_least64_t;
typedef signed char int_fast8_t;
typedef long int_fast16_t;
typedef long int_fast32_t;
typedef long int_fast64_t;
typedef unsigned char uint_fast8_t;
typedef unsigned long uint_fast16_t;
typedef unsigned long uint_fast32_t;
typedef unsigned long uint_fast64_t;
typedef long intptr_t;
typedef unsigned long uintptr_t;
typedef __intmax_t intmax_t;
typedef __uintmax_t uintmax_t;
extern uint64_t revng_init_local_sp(uint64_t param_0);
extern uint64_t init_rbp(void);
extern uint64_t indirect_placeholder_1(uint64_t param_0, uint64_t param_1);
extern uint64_t init_cc_src2(void);
extern uint64_t helper_cc_compute_c_wrapper(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint32_t param_3);
extern void indirect_placeholder(void);
extern uint64_t init_rdi(void);
extern void indirect_placeholder_49(uint64_t param_0);
uint64_t bb_mbsstr_trimmed_wordbounded(uint64_t rsi) {
    uint64_t *var_5;
    uint64_t local_sp_2;
    uint64_t var_72;
    uint64_t local_sp_6;
    uint64_t local_sp_3;
    uint64_t var_64;
    uint64_t local_sp_4;
    uint64_t var_65;
    uint64_t var_66;
    uint64_t var_67;
    uint32_t var_68;
    uint64_t var_69;
    uint64_t local_sp_5;
    uint64_t var_63;
    uint64_t var_70;
    uint64_t var_59;
    uint64_t var_60;
    uint64_t var_61;
    uint64_t var_62;
    uint64_t var_50;
    uint64_t var_51;
    uint64_t var_52;
    unsigned char var_53;
    uint64_t var_54;
    uint64_t var_55;
    uint64_t var_56;
    uint64_t var_49;
    uint64_t var_0;
    uint64_t var_1;
    uint64_t var_2;
    uint64_t var_3;
    uint64_t var_4;
    uint64_t var_6;
    uint64_t var_7;
    uint64_t *var_8;
    unsigned char *var_9;
    uint64_t var_10;
    uint64_t *var_11;
    unsigned char *var_12;
    uint64_t var_13;
    uint64_t *var_14;
    unsigned char *var_15;
    uint64_t *var_16;
    uint64_t var_17;
    unsigned char *var_18;
    unsigned char *var_19;
    unsigned char *var_20;
    uint64_t var_21;
    unsigned char *var_22;
    uint32_t *var_23;
    uint64_t var_24;
    uint64_t *var_25;
    uint64_t *var_26;
    uint64_t *var_27;
    uint64_t *var_28;
    uint64_t var_29;
    uint64_t *var_30;
    uint64_t *var_31;
    uint64_t *var_32;
    uint64_t *var_33;
    uint64_t var_34;
    uint64_t *var_35;
    uint64_t *var_36;
    uint64_t *var_37;
    unsigned char *var_38;
    uint32_t *var_39;
    unsigned char *var_40;
    unsigned char *var_41;
    uint32_t *var_42;
    unsigned char *var_43;
    unsigned char *var_44;
    uint64_t local_sp_8;
    uint64_t var_57;
    uint64_t local_sp_7;
    unsigned char var_58;
    uint64_t local_sp_0;
    uint64_t local_sp_9;
    uint64_t local_sp_8_be;
    uint64_t local_sp_1;
    uint32_t var_74;
    uint64_t var_75;
    uint64_t var_76;
    uint64_t var_73;
    uint64_t var_71;
    uint64_t var_45;
    uint64_t var_46;
    uint64_t var_47;
    uint64_t var_48;
    var_0 = revng_init_local_sp(0UL);
    var_1 = init_rbp();
    var_2 = init_cc_src2();
    var_3 = init_rdi();
    *(uint64_t *)(var_0 + (-8L)) = var_1;
    var_4 = var_0 + (-176L);
    var_5 = (uint64_t *)var_4;
    *var_5 = var_3;
    *(uint64_t *)(var_0 + (-184L)) = rsi;
    var_6 = var_0 + (-192L);
    *(uint64_t *)var_6 = 4218026UL;
    var_7 = indirect_placeholder_1(rsi, 2UL);
    var_8 = (uint64_t *)(var_0 + (-24L));
    *var_8 = var_7;
    var_9 = (unsigned char *)(var_0 + (-9L));
    *var_9 = (unsigned char)'\x00';
    var_10 = var_0 + (-32L);
    var_11 = (uint64_t *)var_10;
    var_12 = (unsigned char *)(var_0 + (-12L));
    var_13 = var_0 + (-40L);
    var_14 = (uint64_t *)var_13;
    var_15 = (unsigned char *)(var_0 + (-13L));
    var_16 = (uint64_t *)(var_0 + (-152L));
    var_17 = var_0 + (-168L);
    var_18 = (unsigned char *)var_17;
    var_19 = (unsigned char *)(var_0 + (-156L));
    var_20 = (unsigned char *)(var_0 + (-10L));
    var_21 = var_0 + (-136L);
    var_22 = (unsigned char *)var_21;
    var_23 = (uint32_t *)(var_0 + (-132L));
    var_24 = var_0 + (-104L);
    var_25 = (uint64_t *)var_24;
    var_26 = (uint64_t *)(var_0 + (-144L));
    var_27 = (uint64_t *)(var_0 + (-96L));
    var_28 = (uint64_t *)var_21;
    var_29 = var_0 + (-88L);
    var_30 = (uint64_t *)var_29;
    var_31 = (uint64_t *)(var_0 + (-128L));
    var_32 = (uint64_t *)(var_0 + (-80L));
    var_33 = (uint64_t *)(var_0 + (-120L));
    var_34 = var_0 + (-72L);
    var_35 = (uint64_t *)var_34;
    var_36 = (uint64_t *)(var_0 + (-112L));
    var_37 = (uint64_t *)(var_0 + (-64L));
    var_38 = (unsigned char *)var_29;
    var_39 = (uint32_t *)(var_0 + (-84L));
    var_40 = (unsigned char *)var_24;
    var_41 = (unsigned char *)(var_0 + (-92L));
    var_42 = (uint32_t *)(var_0 + (-68L));
    var_43 = (unsigned char *)var_34;
    var_44 = (unsigned char *)(var_0 + (-11L));
    var_58 = (unsigned char)'\x01';
    local_sp_8 = var_6;
    local_sp_9 = local_sp_8;
    while (**(unsigned char **)var_4 != '\x00')
        {
            var_49 = local_sp_8 + (-16L);
            *(uint64_t *)var_49 = 4218082UL;
            indirect_placeholder();
            local_sp_7 = var_49;
            if (var_48 <= 1UL) {
                *var_12 = (unsigned char)'\x01';
                var_50 = *var_5;
                var_51 = *var_11;
                var_52 = helper_cc_compute_c_wrapper(var_50 - var_51, var_51, var_2, 17U);
                var_53 = *(unsigned char *)(*var_11 + (-1L));
                var_54 = local_sp_8 + (-24L);
                *(uint64_t *)var_54 = 4218931UL;
                indirect_placeholder();
                local_sp_7 = var_54;
                if (var_52 != 0UL & var_53 == '\x00') {
                    *var_12 = (unsigned char)'\x00';
                }
                var_55 = *var_8;
                var_56 = local_sp_7 + (-8L);
                *(uint64_t *)var_56 = 4218951UL;
                indirect_placeholder();
                *var_14 = (*var_11 + var_55);
                *var_15 = (unsigned char)'\x01';
                local_sp_0 = var_56;
                if (**(unsigned char **)var_13 == '\x00') {
                    var_57 = local_sp_7 + (-16L);
                    *(uint64_t *)var_57 = 4218997UL;
                    indirect_placeholder();
                    *var_15 = (unsigned char)'\x00';
                    var_58 = (unsigned char)'\x00';
                    local_sp_0 = var_57;
                }
                local_sp_8_be = local_sp_0;
                local_sp_9 = local_sp_0;
                if (*var_12 != '\x00') {
                    if (var_58 != '\x00') {
                        *var_9 = (unsigned char)'\x01';
                        break;
                    }
                }
                if (**(unsigned char **)var_10 == '\x00') {
                    break;
                }
                *var_5 = (*var_11 + 1UL);
                local_sp_8 = local_sp_8_be;
                local_sp_9 = local_sp_8;
                continue;
            }
            *var_16 = *var_5;
            *var_18 = (unsigned char)'\x00';
            var_59 = local_sp_8 + (-24L);
            *(uint64_t *)var_59 = 4218142UL;
            indirect_placeholder();
            *var_19 = (unsigned char)'\x00';
            *var_20 = (unsigned char)'\x01';
            var_60 = *var_16;
            var_61 = *var_11;
            var_62 = helper_cc_compute_c_wrapper(var_60 - var_61, var_61, var_2, 17U);
            local_sp_5 = var_59;
            local_sp_6 = var_59;
            if (var_62 != 0UL) {
                *var_25 = *var_16;
                *var_27 = *var_26;
                *var_30 = *var_28;
                *var_32 = *var_31;
                *var_35 = *var_33;
                *var_37 = *var_36;
                *var_16 = (*var_26 + *var_16);
                *var_19 = (unsigned char)'\x00';
                var_65 = *var_16;
                var_66 = *var_11;
                var_67 = helper_cc_compute_c_wrapper(var_65 - var_66, var_66, var_2, 17U);
                local_sp_5 = local_sp_4;
                local_sp_6 = local_sp_4;
                do {
                    var_63 = local_sp_5 + (-8L);
                    *(uint64_t *)var_63 = 4218185UL;
                    indirect_placeholder_49(var_17);
                    local_sp_4 = var_63;
                    if (*var_22 != '\x01' & *var_23 == 0U) {
                        var_64 = local_sp_5 + (-16L);
                        *(uint64_t *)var_64 = 4218224UL;
                        indirect_placeholder();
                        local_sp_4 = var_64;
                    }
                    *var_25 = *var_16;
                    *var_27 = *var_26;
                    *var_30 = *var_28;
                    *var_32 = *var_31;
                    *var_35 = *var_33;
                    *var_37 = *var_36;
                    *var_16 = (*var_26 + *var_16);
                    *var_19 = (unsigned char)'\x00';
                    var_65 = *var_16;
                    var_66 = *var_11;
                    var_67 = helper_cc_compute_c_wrapper(var_65 - var_66, var_66, var_2, 17U);
                    local_sp_5 = local_sp_4;
                    local_sp_6 = local_sp_4;
                } while (var_67 != 0UL);
                var_68 = *var_39;
                var_69 = local_sp_4 + (-8L);
                *(uint64_t *)var_69 = 4218344UL;
                indirect_placeholder();
                local_sp_6 = var_69;
                if (*var_38 != '\x00' & var_68 == 0U) {
                    *var_20 = (unsigned char)'\x00';
                }
            }
            *var_16 = *var_11;
            *var_18 = (unsigned char)'\x00';
            *(uint64_t *)(local_sp_6 + (-8L)) = 4218399UL;
            indirect_placeholder();
            *var_19 = (unsigned char)'\x00';
            *var_30 = *var_8;
            *var_40 = (unsigned char)'\x00';
            var_70 = local_sp_6 + (-16L);
            *(uint64_t *)var_70 = 4218444UL;
            indirect_placeholder();
            *var_41 = (unsigned char)'\x00';
            local_sp_3 = var_70;
            while (1U)
                {
                    *(uint64_t *)(local_sp_3 + (-8L)) = 4218566UL;
                    indirect_placeholder_49(var_24);
                    if (*var_43 != '\x01') {
                        if (*var_42 == 0U) {
                            break;
                        }
                    }
                    var_71 = local_sp_3 + (-16L);
                    *(uint64_t *)var_71 = 4218465UL;
                    indirect_placeholder_49(var_17);
                    local_sp_2 = var_71;
                    if (*var_22 != '\x01' & *var_23 == 0U) {
                        var_72 = local_sp_3 + (-24L);
                        *(uint64_t *)var_72 = 4218504UL;
                        indirect_placeholder();
                        local_sp_2 = var_72;
                    }
                    *var_16 = (*var_26 + *var_16);
                    *var_19 = (unsigned char)'\x00';
                    *var_30 = (*var_32 + *var_30);
                    *var_41 = (unsigned char)'\x00';
                    local_sp_3 = local_sp_2;
                    continue;
                }
            *var_44 = (unsigned char)'\x01';
            var_73 = local_sp_3 + (-16L);
            *(uint64_t *)var_73 = 4218623UL;
            indirect_placeholder_49(var_17);
            local_sp_1 = var_73;
            if (*var_22 == '\x01') {
                *var_25 = *var_16;
                *var_27 = *var_26;
                *var_30 = *var_28;
                *var_32 = *var_31;
                *var_35 = *var_33;
                *var_37 = *var_36;
                var_74 = *var_39;
                var_75 = local_sp_3 + (-24L);
                *(uint64_t *)var_75 = 4218729UL;
                indirect_placeholder();
                local_sp_1 = var_75;
                if (*var_23 != 0U & *var_38 != '\x00' & var_74 == 0U) {
                    *var_44 = (unsigned char)'\x00';
                }
            } else {
                *var_25 = *var_16;
                *var_27 = *var_26;
                *var_30 = *var_28;
                *var_32 = *var_31;
                *var_35 = *var_33;
                *var_37 = *var_36;
                var_74 = *var_39;
                var_75 = local_sp_3 + (-24L);
                *(uint64_t *)var_75 = 4218729UL;
                indirect_placeholder();
                local_sp_1 = var_75;
                if (*var_38 != '\x00' & var_74 == 0U) {
                    *var_44 = (unsigned char)'\x00';
                }
            }
            local_sp_9 = local_sp_1;
            if (*var_20 != '\x00') {
                if (*var_44 != '\x00') {
                    *var_9 = (unsigned char)'\x01';
                    break;
                }
            }
            *var_16 = *var_11;
            *var_18 = (unsigned char)'\x00';
            *(uint64_t *)(local_sp_1 + (-8L)) = 4218805UL;
            indirect_placeholder();
            *var_19 = (unsigned char)'\x00';
            var_76 = local_sp_1 + (-16L);
            *(uint64_t *)var_76 = 4218827UL;
            indirect_placeholder_49(var_17);
            local_sp_8_be = var_76;
            local_sp_9 = var_76;
            if (*var_22 != '\x01') {
                if (*var_23 != 0U) {
                    break;
                }
            }
            *var_5 = (*var_11 + *var_26);
        }
    *(uint64_t *)(local_sp_9 + (-8L)) = 4219081UL;
    indirect_placeholder();
    return (uint64_t)*var_9;
}

typedef unsigned char __u_char;
typedef unsigned short __u_short;
typedef unsigned int __u_int;
typedef unsigned long __u_long;
typedef signed char __int8_t;
typedef unsigned char __uint8_t;
typedef short __int16_t;
typedef unsigned short __uint16_t;
typedef int __int32_t;
typedef unsigned int __uint32_t;
typedef long __int64_t;
typedef unsigned long __uint64_t;
typedef __int8_t __int_least8_t;
typedef __uint8_t __uint_least8_t;
typedef __int16_t __int_least16_t;
typedef __uint16_t __uint_least16_t;
typedef __int32_t __int_least32_t;
typedef __uint32_t __uint_least32_t;
typedef __int64_t __int_least64_t;
typedef __uint64_t __uint_least64_t;
typedef long __quad_t;
typedef unsigned long __u_quad_t;
typedef long __intmax_t;
typedef unsigned long __uintmax_t;
typedef unsigned long __dev_t;
typedef unsigned int __uid_t;
typedef unsigned int __gid_t;
typedef unsigned long __ino_t;
typedef unsigned long __ino64_t;
typedef unsigned int __mode_t;
typedef unsigned long __nlink_t;
typedef long __off_t;
typedef long __off64_t;
typedef int __pid_t;
typedef struct {
    int __val[2];
} __fsid_t;
typedef long __clock_t;
typedef unsigned long __rlim_t;
typedef unsigned long __rlim64_t;
typedef unsigned int __id_t;
typedef long __time_t;
typedef unsigned int __useconds_t;
typedef long __suseconds_t;
typedef int __daddr_t;
typedef int __key_t;
typedef int __clockid_t;
typedef void *__timer_t;
typedef long __blksize_t;
typedef long __blkcnt_t;
typedef long __blkcnt64_t;
typedef unsigned long __fsblkcnt_t;
typedef unsigned long __fsblkcnt64_t;
typedef unsigned long __fsfilcnt_t;
typedef unsigned long __fsfilcnt64_t;
typedef long __fsword_t;
typedef long __ssize_t;
typedef long __syscall_slong_t;
typedef unsigned long __syscall_ulong_t;
typedef __off64_t __loff_t;
typedef char *__caddr_t;
typedef long __intptr_t;
typedef unsigned int __socklen_t;
typedef int __sig_atomic_t;
typedef __int8_t int8_t;
typedef __int16_t int16_t;
typedef __int32_t int32_t;
typedef __int64_t int64_t;
typedef __uint8_t uint8_t;
typedef __uint16_t uint16_t;
typedef __uint32_t uint32_t;
typedef __uint64_t uint64_t;
typedef __int_least8_t int_least8_t;
typedef __int_least16_t int_least16_t;
typedef __int_least32_t int_least32_t;
typedef __int_least64_t int_least64_t;
typedef __uint_least8_t uint_least8_t;
typedef __uint_least16_t uint_least16_t;
typedef __uint_least32_t uint_least32_t;
typedef __uint_least64_t uint_least64_t;
typedef signed char int_fast8_t;
typedef long int_fast16_t;
typedef long int_fast32_t;
typedef long int_fast64_t;
typedef unsigned char uint_fast8_t;
typedef unsigned long uint_fast16_t;
typedef unsigned long uint_fast32_t;
typedef unsigned long uint_fast64_t;
typedef long intptr_t;
typedef unsigned long uintptr_t;
typedef __intmax_t intmax_t;
typedef __uintmax_t uintmax_t;
typedef _Bool bool;
struct indirect_placeholder_38_ret_type;
struct indirect_placeholder_38_ret_type {
    uint64_t field_0;
    uint64_t field_1;
    uint64_t field_2;
};
extern uint64_t revng_init_local_sp(uint64_t param_0);
extern uint64_t init_rbp(void);
extern uint64_t init_cc_src2(void);
extern uint64_t helper_cc_compute_c_wrapper(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint32_t param_3);
extern void indirect_placeholder(void);
extern struct indirect_placeholder_38_ret_type indirect_placeholder_38(uint64_t param_0, uint64_t param_1);
uint64_t bb_gcd2_odd(uint64_t rdx, uint64_t rcx, uint64_t rdi, uint64_t rsi, uint64_t r8) {
    uint64_t var_12;
    uint64_t var_0;
    uint64_t var_1;
    uint64_t var_2;
    uint64_t var_3;
    uint64_t var_4;
    uint64_t *var_5;
    uint64_t *var_6;
    uint64_t *var_7;
    uint64_t *var_8;
    uint64_t local_sp_0;
    uint64_t var_9;
    uint64_t var_10;
    uint64_t var_11;
    uint64_t rax_0;
    uint64_t var_13;
    uint64_t cc_src2_1;
    uint64_t _pre65;
    uint64_t var_14;
    uint64_t cc_src2_0;
    uint64_t var_15;
    uint64_t var_37;
    uint64_t var_38;
    struct indirect_placeholder_38_ret_type var_39;
    uint64_t _pre69;
    uint64_t _pre70;
    uint64_t var_29;
    bool var_16;
    uint64_t var_17;
    uint64_t var_18;
    uint64_t var_21;
    uint64_t var_22;
    uint64_t var_19;
    uint64_t var_20;
    uint64_t var_23;
    uint64_t var_24;
    uint64_t var_25;
    uint64_t var_26;
    uint64_t var_27;
    uint64_t var_28;
    uint64_t _pre65_pre;
    uint64_t var_30;
    uint64_t var_31;
    uint64_t var_32;
    uint64_t var_33;
    uint64_t var_34;
    uint64_t var_35;
    uint64_t var_36;
    uint64_t var_40;
    unsigned int loop_state_var;
    var_0 = revng_init_local_sp(0UL);
    var_1 = init_rbp();
    var_2 = init_cc_src2();
    *(uint64_t *)(var_0 + (-8L)) = var_1;
    var_3 = var_0 + (-56L);
    var_4 = var_0 + (-16L);
    *(uint64_t *)var_4 = rdi;
    var_5 = (uint64_t *)(var_0 + (-24L));
    *var_5 = rsi;
    var_6 = (uint64_t *)(var_0 + (-32L));
    *var_6 = rdx;
    var_7 = (uint64_t *)(var_0 + (-40L));
    *var_7 = rcx;
    var_8 = (uint64_t *)(var_0 + (-48L));
    *var_8 = r8;
    local_sp_0 = var_3;
    cc_src2_0 = var_2;
    if ((r8 & 1UL) == 0UL) {
        var_9 = var_0 + (-64L);
        *(uint64_t *)var_9 = 4202627UL;
        indirect_placeholder();
        local_sp_0 = var_9;
    }
    var_10 = *var_6;
    var_11 = *var_5;
    var_12 = var_11;
    var_13 = var_10;
    if ((var_10 | var_11) == 0UL) {
        **(uint64_t **)var_4 = *var_7;
        rax_0 = *var_8;
    } else {
        var_14 = var_12;
        while ((var_13 & 1UL) != 0UL)
            {
                *var_6 = ((var_13 >> 1UL) | (var_12 << 63UL));
                var_40 = *var_5 >> 1UL;
                *var_5 = var_40;
                var_12 = var_40;
                var_13 = *var_6;
                var_14 = var_12;
            }
        while (1U)
            {
                var_15 = *var_7;
                if ((var_15 | var_14) != 0UL) {
                    **(uint64_t **)var_4 = 0UL;
                    var_37 = *var_6;
                    var_38 = *var_8;
                    *(uint64_t *)(local_sp_0 + (-8L)) = 4202749UL;
                    var_39 = indirect_placeholder_38(var_38, var_37);
                    rax_0 = var_39.field_0;
                    loop_state_var = 0U;
                    break;
                }
                if (var_14 > var_15) {
                    _pre69 = *var_6;
                    _pre70 = *var_8;
                    var_29 = _pre70;
                    var_30 = _pre69;
                    var_31 = var_30 - var_29;
                    var_32 = helper_cc_compute_c_wrapper(var_31, var_29, cc_src2_0, 17U);
                    *var_5 = ((var_14 - var_15) - var_32);
                    *var_6 = var_31;
                    var_33 = var_31;
                    var_34 = *var_5;
                    cc_src2_1 = var_32;
                    *var_6 = ((var_33 >> 1UL) | (var_34 << 63UL));
                    var_35 = *var_5 >> 1UL;
                    *var_5 = var_35;
                    var_36 = *var_6;
                    var_33 = var_36;
                    var_34 = var_35;
                    _pre65 = var_35;
                    do {
                        *var_6 = ((var_33 >> 1UL) | (var_34 << 63UL));
                        var_35 = *var_5 >> 1UL;
                        *var_5 = var_35;
                        var_36 = *var_6;
                        var_33 = var_36;
                        var_34 = var_35;
                        _pre65 = var_35;
                    } while ((var_36 & 1UL) != 0UL);
                } else {
                    var_16 = (var_14 == var_15);
                    if (!var_16) {
                        var_17 = *var_6;
                        var_18 = *var_8;
                        var_29 = var_18;
                        var_30 = var_17;
                        if (var_17 <= var_18) {
                            var_31 = var_30 - var_29;
                            var_32 = helper_cc_compute_c_wrapper(var_31, var_29, cc_src2_0, 17U);
                            *var_5 = ((var_14 - var_15) - var_32);
                            *var_6 = var_31;
                            var_33 = var_31;
                            var_34 = *var_5;
                            cc_src2_1 = var_32;
                            *var_6 = ((var_33 >> 1UL) | (var_34 << 63UL));
                            var_35 = *var_5 >> 1UL;
                            *var_5 = var_35;
                            var_36 = *var_6;
                            var_33 = var_36;
                            var_34 = var_35;
                            _pre65 = var_35;
                            do {
                                *var_6 = ((var_33 >> 1UL) | (var_34 << 63UL));
                                var_35 = *var_5 >> 1UL;
                                *var_5 = var_35;
                                var_36 = *var_6;
                                var_33 = var_36;
                                var_34 = var_35;
                                _pre65 = var_35;
                            } while ((var_36 & 1UL) != 0UL);
                            var_14 = _pre65;
                            cc_src2_0 = cc_src2_1;
                            continue;
                        }
                    }
                    if (var_15 <= var_14) {
                        if (!var_16) {
                            loop_state_var = 1U;
                            break;
                        }
                        var_19 = *var_8;
                        var_20 = *var_6;
                        var_21 = var_20;
                        var_22 = var_19;
                        if (var_19 <= var_20) {
                            loop_state_var = 1U;
                            break;
                        }
                    }
                    var_21 = *var_6;
                    var_22 = *var_8;
                }
            }
        **(uint64_t **)var_4 = var_14;
        rax_0 = *var_6;
    }
}

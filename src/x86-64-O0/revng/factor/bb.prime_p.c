typedef unsigned char __u_char;
typedef unsigned short __u_short;
typedef unsigned int __u_int;
typedef unsigned long __u_long;
typedef signed char __int8_t;
typedef unsigned char __uint8_t;
typedef short __int16_t;
typedef unsigned short __uint16_t;
typedef int __int32_t;
typedef unsigned int __uint32_t;
typedef long __int64_t;
typedef unsigned long __uint64_t;
typedef __int8_t __int_least8_t;
typedef __uint8_t __uint_least8_t;
typedef __int16_t __int_least16_t;
typedef __uint16_t __uint_least16_t;
typedef __int32_t __int_least32_t;
typedef __uint32_t __uint_least32_t;
typedef __int64_t __int_least64_t;
typedef __uint64_t __uint_least64_t;
typedef long __quad_t;
typedef unsigned long __u_quad_t;
typedef long __intmax_t;
typedef unsigned long __uintmax_t;
typedef unsigned long __dev_t;
typedef unsigned int __uid_t;
typedef unsigned int __gid_t;
typedef unsigned long __ino_t;
typedef unsigned long __ino64_t;
typedef unsigned int __mode_t;
typedef unsigned long __nlink_t;
typedef long __off_t;
typedef long __off64_t;
typedef int __pid_t;
typedef struct {
    int __val[2];
} __fsid_t;
typedef long __clock_t;
typedef unsigned long __rlim_t;
typedef unsigned long __rlim64_t;
typedef unsigned int __id_t;
typedef long __time_t;
typedef unsigned int __useconds_t;
typedef long __suseconds_t;
typedef int __daddr_t;
typedef int __key_t;
typedef int __clockid_t;
typedef void *__timer_t;
typedef long __blksize_t;
typedef long __blkcnt_t;
typedef long __blkcnt64_t;
typedef unsigned long __fsblkcnt_t;
typedef unsigned long __fsblkcnt64_t;
typedef unsigned long __fsfilcnt_t;
typedef unsigned long __fsfilcnt64_t;
typedef long __fsword_t;
typedef long __ssize_t;
typedef long __syscall_slong_t;
typedef unsigned long __syscall_ulong_t;
typedef __off64_t __loff_t;
typedef char *__caddr_t;
typedef long __intptr_t;
typedef unsigned int __socklen_t;
typedef int __sig_atomic_t;
typedef __int8_t int8_t;
typedef __int16_t int16_t;
typedef __int32_t int32_t;
typedef __int64_t int64_t;
typedef __uint8_t uint8_t;
typedef __uint16_t uint16_t;
typedef __uint32_t uint32_t;
typedef __uint64_t uint64_t;
typedef __int_least8_t int_least8_t;
typedef __int_least16_t int_least16_t;
typedef __int_least32_t int_least32_t;
typedef __int_least64_t int_least64_t;
typedef __uint_least8_t uint_least8_t;
typedef __uint_least16_t uint_least16_t;
typedef __uint_least32_t uint_least32_t;
typedef __uint_least64_t uint_least64_t;
typedef signed char int_fast8_t;
typedef long int_fast16_t;
typedef long int_fast32_t;
typedef long int_fast64_t;
typedef unsigned char uint_fast8_t;
typedef unsigned long uint_fast16_t;
typedef unsigned long uint_fast32_t;
typedef unsigned long uint_fast64_t;
typedef long intptr_t;
typedef unsigned long uintptr_t;
typedef __intmax_t intmax_t;
typedef __uintmax_t uintmax_t;
struct bb_prime_p_ret_type;
struct helper_divq_EAX_wrapper_ret_type;
struct type_6;
struct indirect_placeholder_27_ret_type;
struct indirect_placeholder_28_ret_type;
struct indirect_placeholder_29_ret_type;
struct indirect_placeholder_30_ret_type;
struct bb_prime_p_ret_type {
    uint64_t field_0;
    uint64_t field_1;
    uint64_t field_2;
    uint64_t field_3;
    uint64_t field_4;
    uint64_t field_5;
    uint64_t field_6;
    uint64_t field_7;
};
struct helper_divq_EAX_wrapper_ret_type {
    uint64_t field_0;
    uint64_t field_1;
    uint64_t field_2;
    uint32_t field_3;
    uint64_t field_4;
    uint32_t field_5;
    uint64_t field_6;
    uint32_t field_7;
    uint64_t field_8;
    uint32_t field_9;
    uint32_t field_10;
    unsigned char field_11;
    uint32_t field_12;
};
struct type_6 {
};
struct indirect_placeholder_27_ret_type {
    uint64_t field_0;
    uint64_t field_1;
    uint64_t field_2;
    uint64_t field_3;
    uint64_t field_4;
    uint64_t field_5;
    uint64_t field_6;
};
struct indirect_placeholder_28_ret_type {
    uint64_t field_0;
    uint64_t field_1;
    uint64_t field_2;
    uint64_t field_3;
};
struct indirect_placeholder_29_ret_type {
    uint64_t field_0;
    uint64_t field_1;
    uint64_t field_2;
    uint64_t field_3;
    uint64_t field_4;
    uint64_t field_5;
};
struct indirect_placeholder_30_ret_type {
    uint64_t field_0;
    uint64_t field_1;
    uint64_t field_2;
    uint64_t field_3;
    uint64_t field_4;
    uint64_t field_5;
};
extern uint64_t revng_init_local_sp(uint64_t param_0);
extern uint64_t init_rbp(void);
extern uint64_t init_cc_src2(void);
extern uint64_t helper_cc_compute_c_wrapper(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint32_t param_3);
extern void indirect_placeholder(void);
extern uint64_t init_rsi(void);
extern uint64_t init_rbx(void);
extern uint64_t init_r10(void);
extern uint64_t init_state_0x9018(void);
extern uint32_t init_state_0x9010(void);
extern uint64_t init_state_0x8408(void);
extern uint64_t init_state_0x8328(void);
extern uint64_t init_state_0x82d8(void);
extern uint32_t init_state_0x9080(void);
extern uint32_t init_state_0x8248(void);
extern struct helper_divq_EAX_wrapper_ret_type helper_divq_EAX_wrapper(struct type_6 *param_0, uint64_t param_1, uint64_t param_2, uint64_t param_3, uint64_t param_4, uint64_t param_5, uint64_t param_6, uint64_t param_7, uint64_t param_8, uint64_t param_9, uint64_t param_10, uint64_t param_11, uint64_t param_12, uint64_t param_13, uint32_t param_14, uint64_t param_15, uint64_t param_16, uint64_t param_17, uint32_t param_18, uint32_t param_19);
extern uint64_t init_r9(void);
extern uint64_t init_r8(void);
extern void indirect_placeholder_23(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint64_t param_3, uint64_t param_4, uint64_t param_5, uint64_t param_6);
extern uint64_t init_rcx(void);
extern struct indirect_placeholder_27_ret_type indirect_placeholder_27(uint64_t param_0, uint64_t param_1, uint64_t param_2);
extern struct indirect_placeholder_28_ret_type indirect_placeholder_28(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint64_t param_3);
extern struct indirect_placeholder_29_ret_type indirect_placeholder_29(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint64_t param_3, uint64_t param_4, uint64_t param_5);
extern struct indirect_placeholder_30_ret_type indirect_placeholder_30(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint64_t param_3, uint64_t param_4, uint64_t param_5);
struct bb_prime_p_ret_type bb_prime_p(uint64_t rdx, uint64_t rdi) {
    uint32_t var_140;
    uint64_t var_0;
    uint64_t var_1;
    uint64_t var_2;
    uint64_t var_3;
    uint64_t var_4;
    uint64_t var_5;
    uint64_t var_6;
    uint64_t var_7;
    uint64_t var_8;
    uint64_t var_9;
    uint32_t var_10;
    uint64_t var_11;
    uint64_t var_12;
    uint64_t var_13;
    uint32_t var_14;
    uint32_t var_15;
    uint64_t var_16;
    uint64_t var_17;
    uint64_t *var_18;
    uint64_t var_156;
    uint64_t var_157;
    uint64_t var_158;
    uint64_t var_159;
    uint64_t var_160;
    uint32_t var_161;
    uint32_t var_96;
    uint64_t rcx_0;
    uint64_t var_79;
    uint64_t var_80;
    uint64_t var_81;
    struct indirect_placeholder_27_ret_type var_82;
    uint64_t rdi9_0;
    uint64_t rbx_0;
    uint64_t local_sp_5;
    uint32_t state_0x9010_0;
    uint64_t rsi_0;
    uint64_t r10_0;
    uint64_t r9_0;
    uint64_t r8_0;
    uint64_t local_sp_0;
    uint32_t *var_83;
    unsigned char *var_84;
    uint32_t *var_85;
    unsigned char *var_86;
    uint64_t *var_87;
    uint64_t *var_88;
    uint64_t *var_89;
    uint64_t *var_90;
    uint64_t *var_91;
    uint64_t *var_92;
    uint64_t *var_93;
    uint32_t *var_94;
    uint64_t *var_95;
    uint64_t state_0x9018_3;
    uint64_t cc_src2_0;
    uint64_t rcx_1;
    uint64_t rdi9_1;
    uint64_t rsi_1;
    uint64_t r9_1;
    uint64_t r8_1;
    uint32_t state_0x9010_3;
    uint64_t state_0x9018_0;
    unsigned char var_118;
    uint64_t local_sp_1;
    uint32_t state_0x9080_3;
    uint64_t state_0x82d8_0;
    uint32_t state_0x8248_3;
    uint32_t state_0x9080_0;
    uint32_t state_0x8248_0;
    uint64_t rcx_3;
    uint32_t var_97;
    uint64_t rcx_2;
    uint64_t rdi9_2;
    uint64_t rsi_2;
    uint64_t r8_2;
    uint64_t state_0x9018_1;
    uint32_t state_0x9010_1;
    uint64_t local_sp_2;
    uint64_t state_0x82d8_1;
    uint32_t state_0x9080_1;
    uint32_t state_0x8248_1;
    struct bb_prime_p_ret_type mrv;
    struct bb_prime_p_ret_type mrv1;
    struct bb_prime_p_ret_type mrv2;
    struct bb_prime_p_ret_type mrv3;
    struct bb_prime_p_ret_type mrv4;
    struct bb_prime_p_ret_type mrv5;
    struct bb_prime_p_ret_type mrv6;
    struct bb_prime_p_ret_type mrv7;
    uint64_t var_98;
    uint64_t var_99;
    uint64_t var_101;
    uint64_t var_107;
    struct helper_divq_EAX_wrapper_ret_type var_100;
    uint64_t var_102;
    uint32_t var_103;
    uint64_t var_104;
    uint32_t var_105;
    uint32_t var_106;
    uint64_t var_108;
    uint64_t var_109;
    uint64_t var_110;
    uint64_t var_111;
    struct indirect_placeholder_28_ret_type var_112;
    uint64_t var_113;
    uint64_t var_114;
    uint64_t var_115;
    uint32_t var_116;
    unsigned char var_117;
    uint64_t rdi9_3;
    uint64_t rsi_3;
    uint64_t r8_3;
    uint64_t state_0x9018_2;
    uint32_t state_0x9010_2;
    uint64_t local_sp_3;
    uint64_t state_0x82d8_2;
    uint32_t state_0x9080_2;
    uint32_t state_0x8248_2;
    uint64_t var_119;
    uint64_t *var_38;
    unsigned __int128 var_120;
    uint64_t var_121;
    uint64_t var_122;
    uint64_t var_142;
    uint64_t var_143;
    uint32_t var_144;
    uint64_t var_145;
    struct helper_divq_EAX_wrapper_ret_type var_141;
    uint32_t var_146;
    uint32_t var_147;
    uint64_t var_148;
    uint64_t var_123;
    uint64_t var_124;
    uint64_t local_sp_4;
    uint64_t var_125;
    uint64_t cc_src2_3;
    uint32_t var_126;
    uint64_t cc_src2_1;
    uint64_t cc_src2_2;
    uint64_t state_0x82d8_3;
    uint64_t var_149;
    uint64_t var_150;
    uint64_t var_151;
    uint64_t var_152;
    uint64_t var_153;
    uint64_t var_154;
    struct indirect_placeholder_29_ret_type var_155;
    uint64_t var_127;
    uint64_t var_128;
    uint64_t var_129;
    uint64_t var_133;
    uint64_t var_130;
    uint64_t var_131;
    uint64_t var_132;
    uint64_t rcx_4;
    uint64_t rdi9_4;
    uint64_t var_134;
    uint64_t var_135;
    uint64_t var_136;
    uint64_t var_137;
    uint64_t var_138;
    uint64_t var_139;
    uint64_t rbx_1;
    uint64_t rsi_4;
    uint64_t r10_1;
    uint64_t r9_2;
    uint64_t r8_4;
    uint64_t rax_0;
    uint64_t var_19;
    uint64_t *var_20;
    uint32_t *var_21;
    uint64_t var_22;
    uint64_t *var_23;
    uint64_t var_24;
    uint64_t *var_25;
    uint64_t var_26;
    uint64_t *var_27;
    uint64_t var_28;
    uint64_t var_29;
    uint64_t var_30;
    uint64_t *var_31;
    uint64_t var_32;
    uint64_t var_34;
    uint64_t var_33;
    uint64_t local_sp_6;
    uint64_t *var_35;
    uint64_t *var_36;
    uint64_t *var_37;
    uint64_t *var_39;
    uint32_t *var_40;
    uint64_t cc_src2_5;
    uint32_t var_41;
    uint64_t cc_src2_4;
    uint64_t var_56;
    uint64_t *var_57;
    uint64_t var_58;
    uint64_t var_59;
    uint64_t var_60;
    uint64_t var_61;
    uint64_t var_62;
    uint64_t var_63;
    uint64_t var_64;
    uint64_t *var_65;
    uint64_t var_66;
    uint64_t var_67;
    uint64_t var_68;
    uint64_t var_69;
    uint64_t var_70;
    uint64_t var_71;
    struct indirect_placeholder_30_ret_type var_72;
    uint64_t var_73;
    uint64_t var_74;
    uint64_t var_75;
    uint64_t var_76;
    uint64_t var_77;
    uint64_t var_78;
    uint64_t var_42;
    uint64_t var_43;
    uint64_t var_44;
    uint64_t var_48;
    uint64_t var_45;
    uint64_t var_46;
    uint64_t var_47;
    uint64_t var_49;
    uint64_t var_50;
    uint64_t var_51;
    uint64_t var_52;
    uint64_t var_53;
    uint64_t var_54;
    uint32_t var_55;
    unsigned int loop_state_var;
    var_0 = revng_init_local_sp(0UL);
    var_1 = init_rbp();
    var_2 = init_cc_src2();
    var_3 = init_rcx();
    var_4 = init_rbx();
    var_5 = init_rsi();
    var_6 = init_r10();
    var_7 = init_r9();
    var_8 = init_r8();
    var_9 = init_state_0x9018();
    var_10 = init_state_0x9010();
    var_11 = init_state_0x8408();
    var_12 = init_state_0x8328();
    var_13 = init_state_0x82d8();
    var_14 = init_state_0x9080();
    var_15 = init_state_0x8248();
    var_16 = var_0 + (-8L);
    *(uint64_t *)var_16 = var_1;
    var_17 = var_0 + (-488L);
    var_18 = (uint64_t *)(var_0 + (-480L));
    *var_18 = rdi;
    var_96 = 0U;
    rbx_0 = var_4;
    state_0x9010_0 = var_10;
    r10_0 = var_6;
    state_0x9018_0 = var_9;
    var_118 = (unsigned char)'\x00';
    state_0x82d8_0 = var_13;
    state_0x9080_0 = var_14;
    state_0x8248_0 = var_15;
    var_97 = 0U;
    var_126 = 64U;
    rcx_4 = var_3;
    rdi9_4 = rdi;
    rbx_1 = var_4;
    rsi_4 = var_5;
    r10_1 = var_6;
    r9_2 = var_7;
    r8_4 = var_8;
    rax_0 = 0UL;
    local_sp_6 = var_17;
    var_41 = 64U;
    cc_src2_4 = var_2;
    rax_0 = 1UL;
    if (~(rdi <= 1UL & rdi <= 25030008UL)) {
        mrv.field_0 = rax_0;
        mrv1 = mrv;
        mrv1.field_1 = rcx_4;
        mrv2 = mrv1;
        mrv2.field_2 = rdi9_4;
        mrv3 = mrv2;
        mrv3.field_3 = rbx_1;
        mrv4 = mrv3;
        mrv4.field_4 = rsi_4;
        mrv5 = mrv4;
        mrv5.field_5 = r10_1;
        mrv6 = mrv5;
        mrv6.field_6 = r9_2;
        mrv7 = mrv6;
        mrv7.field_7 = r8_4;
        return mrv7;
    }
    var_19 = rdi + (-1L);
    var_20 = (uint64_t *)(var_0 + (-32L));
    *var_20 = var_19;
    var_21 = (uint32_t *)(var_0 + (-12L));
    *var_21 = 0U;
    rax_0 = 0UL;
    var_22 = *var_20;
    while ((var_22 & 1UL) != 0UL)
        {
            *var_20 = (var_22 >> 1UL);
            *var_21 = (*var_21 + 1U);
            var_22 = *var_20;
        }
    var_23 = (uint64_t *)(var_0 + (-40L));
    *var_23 = 2UL;
    var_24 = *var_18;
    var_25 = (uint64_t *)(var_0 + (-152L));
    *var_25 = var_24;
    var_26 = (uint64_t)*(unsigned char *)(((var_24 >> 1UL) & 127UL) | 4304128UL);
    var_27 = (uint64_t *)(var_0 + (-160L));
    *var_27 = var_26;
    var_28 = (var_26 << 1UL) - (*var_25 * (var_26 * var_26));
    *var_27 = var_28;
    var_29 = (var_28 << 1UL) - (*var_25 * (var_28 * var_28));
    *var_27 = var_29;
    var_30 = (var_29 << 1UL) - (*var_25 * (var_29 * var_29));
    *var_27 = var_30;
    var_31 = (uint64_t *)(var_0 + (-168L));
    *var_31 = var_30;
    var_32 = *var_18;
    var_34 = var_32;
    if (var_32 > 1UL) {
        var_33 = var_0 + (-496L);
        *(uint64_t *)var_33 = 4206944UL;
        indirect_placeholder();
        var_34 = *var_18;
        local_sp_6 = var_33;
    }
    var_35 = (uint64_t *)(var_0 + (-48L));
    *var_35 = var_34;
    var_36 = (uint64_t *)(var_0 + (-56L));
    *var_36 = 0UL;
    var_37 = (uint64_t *)(var_0 + (-72L));
    *var_37 = 1UL;
    var_38 = (uint64_t *)(var_0 + (-80L));
    *var_38 = 0UL;
    var_39 = (uint64_t *)(var_0 + (-64L));
    *var_39 = 0UL;
    var_40 = (uint32_t *)(var_0 + (-84L));
    *var_40 = 64U;
    cc_src2_0 = cc_src2_4;
    cc_src2_5 = cc_src2_4;
    while (var_41 != 0U)
        {
            *var_36 = ((*var_36 >> 1UL) | (*var_35 << 63UL));
            *var_35 = (*var_35 >> 1UL);
            var_42 = *var_39 << 1UL;
            *var_39 = var_42;
            var_43 = *var_37;
            var_44 = *var_35;
            var_48 = var_42;
            if (var_43 <= var_44) {
                if (var_43 != var_44) {
                    var_55 = *var_40 + (-1);
                    *var_40 = var_55;
                    var_41 = var_55;
                    cc_src2_4 = cc_src2_5;
                    cc_src2_0 = cc_src2_4;
                    cc_src2_5 = cc_src2_4;
                    continue;
                }
                var_45 = *var_38;
                var_46 = *var_36;
                var_47 = helper_cc_compute_c_wrapper(var_45 - var_46, var_46, cc_src2_4, 17U);
                if (var_47 != 0UL) {
                    var_55 = *var_40 + (-1);
                    *var_40 = var_55;
                    var_41 = var_55;
                    cc_src2_4 = cc_src2_5;
                    cc_src2_0 = cc_src2_4;
                    cc_src2_5 = cc_src2_4;
                    continue;
                }
                var_48 = *var_39;
            }
            *var_39 = (var_48 + 1UL);
            var_49 = *var_37;
            var_50 = *var_38;
            var_51 = *var_36;
            var_52 = var_50 - var_51;
            var_53 = *var_35;
            var_54 = helper_cc_compute_c_wrapper(var_52, var_51, cc_src2_4, 17U);
            *var_37 = ((var_49 - var_53) - var_54);
            *var_38 = var_52;
            cc_src2_5 = var_54;
            var_55 = *var_40 + (-1);
            *var_40 = var_55;
            var_41 = var_55;
            cc_src2_4 = cc_src2_5;
            cc_src2_0 = cc_src2_4;
            cc_src2_5 = cc_src2_4;
        }
    var_56 = *var_38;
    var_57 = (uint64_t *)(var_0 + (-176L));
    *var_57 = var_56;
    *(uint64_t *)(var_0 + (-184L)) = *var_39;
    var_58 = *var_18;
    var_59 = *var_57;
    var_60 = ((var_58 - var_59) > var_59);
    *(uint64_t *)(var_0 + (-192L)) = var_60;
    var_61 = *var_18;
    var_62 = var_61 & var_60;
    var_63 = *var_57;
    var_64 = (var_63 - var_61) + (var_62 + var_63);
    var_65 = (uint64_t *)(var_0 + (-24L));
    *var_65 = var_64;
    var_66 = (uint64_t)*var_21;
    var_67 = *var_57;
    var_68 = *var_20;
    var_69 = *var_31;
    var_70 = *var_18;
    var_71 = local_sp_6 + (-8L);
    *(uint64_t *)var_71 = 4207254UL;
    var_72 = indirect_placeholder_30(var_64, var_68, var_70, var_69, var_67, var_66);
    var_73 = var_72.field_0;
    var_74 = var_72.field_1;
    var_75 = var_72.field_2;
    var_76 = var_72.field_3;
    var_77 = var_72.field_4;
    var_78 = var_72.field_5;
    rcx_0 = var_74;
    rdi9_0 = var_75;
    rsi_0 = var_76;
    r9_0 = var_77;
    r8_0 = var_78;
    local_sp_0 = var_71;
    rcx_4 = var_74;
    rdi9_4 = var_75;
    rsi_4 = var_76;
    r9_2 = var_77;
    r8_4 = var_78;
    if ((uint64_t)(unsigned char)var_73 == 1UL) {
        mrv.field_0 = rax_0;
        mrv1 = mrv;
        mrv1.field_1 = rcx_4;
        mrv2 = mrv1;
        mrv2.field_2 = rdi9_4;
        mrv3 = mrv2;
        mrv3.field_3 = rbx_1;
        mrv4 = mrv3;
        mrv4.field_4 = rsi_4;
        mrv5 = mrv4;
        mrv5.field_5 = r10_1;
        mrv6 = mrv5;
        mrv6.field_6 = r9_2;
        mrv7 = mrv6;
        mrv7.field_7 = r8_4;
        return mrv7;
    }
    rdi9_4 = 0UL;
    rsi_4 = 0UL;
    rax_0 = 1UL;
    if (*(unsigned char *)6415360UL == '\x00') {
        var_79 = *var_18 + (-1L);
        var_80 = var_0 + (-472L);
        var_81 = local_sp_6 + (-16L);
        *(uint64_t *)var_81 = 4207316UL;
        var_82 = indirect_placeholder_27(var_80, 0UL, var_79);
        rcx_0 = var_82.field_0;
        rdi9_0 = var_82.field_1;
        rbx_0 = var_82.field_2;
        rsi_0 = var_82.field_3;
        r10_0 = var_82.field_4;
        r9_0 = var_82.field_5;
        r8_0 = var_82.field_6;
        local_sp_0 = var_81;
    }
    var_83 = (uint32_t *)(var_0 + (-88L));
    *var_83 = 0U;
    var_84 = (unsigned char *)(var_0 + (-13L));
    var_85 = (uint32_t *)(var_0 + (-92L));
    var_86 = (unsigned char *)(var_0 + (-222L));
    var_87 = (uint64_t *)(var_0 + (-200L));
    var_88 = (uint64_t *)(var_0 + (-208L));
    var_89 = (uint64_t *)(var_0 + (-104L));
    var_90 = (uint64_t *)(var_0 + (-112L));
    var_91 = (uint64_t *)(var_0 + (-128L));
    var_92 = (uint64_t *)(var_0 + (-136L));
    var_93 = (uint64_t *)(var_0 + (-120L));
    var_94 = (uint32_t *)(var_0 + (-140L));
    var_95 = (uint64_t *)(var_0 + (-216L));
    rcx_1 = rcx_0;
    rdi9_1 = rdi9_0;
    rsi_1 = rsi_0;
    r9_1 = r9_0;
    r8_1 = r8_0;
    local_sp_1 = local_sp_0;
    rbx_1 = rbx_0;
    r10_1 = r10_0;
    while (1U)
        {
            rcx_3 = rcx_1;
            rcx_2 = rcx_1;
            rdi9_2 = rdi9_1;
            rsi_2 = rsi_1;
            r8_2 = r8_1;
            state_0x9018_1 = state_0x9018_0;
            state_0x9010_1 = state_0x9010_0;
            local_sp_2 = local_sp_1;
            state_0x82d8_1 = state_0x82d8_0;
            state_0x9080_1 = state_0x9080_0;
            state_0x8248_1 = state_0x8248_0;
            rdi9_3 = rdi9_1;
            rsi_3 = rsi_1;
            r8_3 = r8_1;
            state_0x9018_2 = state_0x9018_0;
            state_0x9010_2 = state_0x9010_0;
            local_sp_3 = local_sp_1;
            state_0x82d8_2 = state_0x82d8_0;
            state_0x9080_2 = state_0x9080_0;
            state_0x8248_2 = state_0x8248_0;
            cc_src2_1 = cc_src2_0;
            cc_src2_2 = cc_src2_0;
            rcx_4 = rcx_1;
            r9_2 = r9_1;
            r8_4 = r8_1;
            if (var_96 <= 667U) {
                *(uint64_t *)(local_sp_1 + (-8L)) = 4207921UL;
                indirect_placeholder_23(0UL, 4304376UL, rcx_1, 0UL, 0UL, r9_1, r8_1);
                *(uint64_t *)(local_sp_1 + (-16L)) = 4207926UL;
                indirect_placeholder();
                break;
            }
            if (*(unsigned char *)6415360UL == '\x00') {
                var_117 = (var_96 == 24U);
                *var_84 = var_117;
                var_118 = var_117;
            } else {
                *var_84 = (unsigned char)'\x01';
                *var_85 = 0U;
                while (1U)
                    {
                        rcx_3 = rcx_2;
                        rdi9_3 = rdi9_2;
                        rsi_3 = rsi_2;
                        r8_3 = r8_2;
                        state_0x9018_2 = state_0x9018_1;
                        state_0x9010_2 = state_0x9010_1;
                        local_sp_3 = local_sp_2;
                        state_0x82d8_2 = state_0x82d8_1;
                        state_0x9080_2 = state_0x9080_1;
                        state_0x8248_2 = state_0x8248_1;
                        if (var_97 >= (uint32_t)*var_86) {
                            loop_state_var = 0U;
                            break;
                        }
                        if (*var_84 != '\x00') {
                            loop_state_var = 1U;
                            break;
                        }
                        var_98 = *var_18 + (-1L);
                        var_99 = *(uint64_t *)(((((uint64_t)var_97 << 3UL) + 16UL) + var_16) + (-464L));
                        var_100 = helper_divq_EAX_wrapper((struct type_6 *)(0UL), var_99, 4207387UL, var_98, var_16, 0UL, var_99, rdi9_2, rbx_0, rsi_2, r10_0, r9_1, r8_2, state_0x9018_1, state_0x9010_1, var_11, var_12, state_0x82d8_1, state_0x9080_1, state_0x8248_1);
                        var_101 = var_100.field_1;
                        var_102 = var_100.field_6;
                        var_103 = var_100.field_7;
                        var_104 = var_100.field_8;
                        var_105 = var_100.field_9;
                        var_106 = var_100.field_10;
                        var_107 = *var_57;
                        var_108 = *var_31;
                        var_109 = *var_18;
                        var_110 = *var_65;
                        var_111 = local_sp_2 + (-8L);
                        *(uint64_t *)var_111 = 4207432UL;
                        var_112 = indirect_placeholder_28(var_109, var_110, var_101, var_107);
                        var_113 = var_112.field_1;
                        var_114 = var_112.field_2;
                        var_115 = var_112.field_3;
                        *var_84 = (var_112.field_0 != *var_57);
                        var_116 = *var_85 + 1U;
                        *var_85 = var_116;
                        var_97 = var_116;
                        rcx_2 = var_108;
                        rdi9_2 = var_113;
                        rsi_2 = var_114;
                        r8_2 = var_115;
                        state_0x9018_1 = var_102;
                        state_0x9010_1 = var_103;
                        local_sp_2 = var_111;
                        state_0x82d8_1 = var_104;
                        state_0x9080_1 = var_105;
                        state_0x8248_1 = var_106;
                        continue;
                    }
                var_118 = *var_84;
            }
        }
    mrv.field_0 = rax_0;
    mrv1 = mrv;
    mrv1.field_1 = rcx_4;
    mrv2 = mrv1;
    mrv2.field_2 = rdi9_4;
    mrv3 = mrv2;
    mrv3.field_3 = rbx_1;
    mrv4 = mrv3;
    mrv4.field_4 = rsi_4;
    mrv5 = mrv4;
    mrv5.field_5 = r10_1;
    mrv6 = mrv5;
    mrv6.field_6 = r9_2;
    mrv7 = mrv6;
    mrv7.field_7 = r8_4;
    return mrv7;
}

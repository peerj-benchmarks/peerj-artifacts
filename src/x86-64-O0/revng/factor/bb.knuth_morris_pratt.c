typedef unsigned char __u_char;
typedef unsigned short __u_short;
typedef unsigned int __u_int;
typedef unsigned long __u_long;
typedef signed char __int8_t;
typedef unsigned char __uint8_t;
typedef short __int16_t;
typedef unsigned short __uint16_t;
typedef int __int32_t;
typedef unsigned int __uint32_t;
typedef long __int64_t;
typedef unsigned long __uint64_t;
typedef __int8_t __int_least8_t;
typedef __uint8_t __uint_least8_t;
typedef __int16_t __int_least16_t;
typedef __uint16_t __uint_least16_t;
typedef __int32_t __int_least32_t;
typedef __uint32_t __uint_least32_t;
typedef __int64_t __int_least64_t;
typedef __uint64_t __uint_least64_t;
typedef long __quad_t;
typedef unsigned long __u_quad_t;
typedef long __intmax_t;
typedef unsigned long __uintmax_t;
typedef unsigned long __dev_t;
typedef unsigned int __uid_t;
typedef unsigned int __gid_t;
typedef unsigned long __ino_t;
typedef unsigned long __ino64_t;
typedef unsigned int __mode_t;
typedef unsigned long __nlink_t;
typedef long __off_t;
typedef long __off64_t;
typedef int __pid_t;
typedef struct {
    int __val[2];
} __fsid_t;
typedef long __clock_t;
typedef unsigned long __rlim_t;
typedef unsigned long __rlim64_t;
typedef unsigned int __id_t;
typedef long __time_t;
typedef unsigned int __useconds_t;
typedef long __suseconds_t;
typedef int __daddr_t;
typedef int __key_t;
typedef int __clockid_t;
typedef void *__timer_t;
typedef long __blksize_t;
typedef long __blkcnt_t;
typedef long __blkcnt64_t;
typedef unsigned long __fsblkcnt_t;
typedef unsigned long __fsblkcnt64_t;
typedef unsigned long __fsfilcnt_t;
typedef unsigned long __fsfilcnt64_t;
typedef long __fsword_t;
typedef long __ssize_t;
typedef long __syscall_slong_t;
typedef unsigned long __syscall_ulong_t;
typedef __off64_t __loff_t;
typedef char *__caddr_t;
typedef long __intptr_t;
typedef unsigned int __socklen_t;
typedef int __sig_atomic_t;
typedef __int8_t int8_t;
typedef __int16_t int16_t;
typedef __int32_t int32_t;
typedef __int64_t int64_t;
typedef __uint8_t uint8_t;
typedef __uint16_t uint16_t;
typedef __uint32_t uint32_t;
typedef __uint64_t uint64_t;
typedef __int_least8_t int_least8_t;
typedef __int_least16_t int_least16_t;
typedef __int_least32_t int_least32_t;
typedef __int_least64_t int_least64_t;
typedef __uint_least8_t uint_least8_t;
typedef __uint_least16_t uint_least16_t;
typedef __uint_least32_t uint_least32_t;
typedef __uint_least64_t uint_least64_t;
typedef signed char int_fast8_t;
typedef long int_fast16_t;
typedef long int_fast32_t;
typedef long int_fast64_t;
typedef unsigned char uint_fast8_t;
typedef unsigned long uint_fast16_t;
typedef unsigned long uint_fast32_t;
typedef unsigned long uint_fast64_t;
typedef long intptr_t;
typedef unsigned long uintptr_t;
typedef __intmax_t intmax_t;
typedef __uintmax_t uintmax_t;
struct helper_divq_EAX_wrapper_ret_type;
struct type_4;
struct helper_divq_EAX_wrapper_ret_type {
    uint64_t field_0;
    uint64_t field_1;
    uint64_t field_2;
    uint32_t field_3;
    uint64_t field_4;
    uint32_t field_5;
    uint64_t field_6;
    uint32_t field_7;
    uint64_t field_8;
    uint32_t field_9;
    uint32_t field_10;
    unsigned char field_11;
    uint32_t field_12;
};
struct type_4 {
};
extern uint64_t revng_init_local_sp(uint64_t param_0);
extern uint64_t init_rbp(void);
extern uint64_t init_cc_src2(void);
extern uint64_t helper_cc_compute_c_wrapper(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint32_t param_3);
extern uint64_t init_state_0x9018(void);
extern uint32_t init_state_0x9010(void);
extern uint64_t init_state_0x8408(void);
extern uint64_t init_state_0x8328(void);
extern uint64_t init_state_0x82d8(void);
extern uint32_t init_state_0x9080(void);
extern uint32_t init_state_0x8248(void);
extern struct helper_divq_EAX_wrapper_ret_type helper_divq_EAX_wrapper(struct type_4 *param_0, uint64_t param_1, uint64_t param_2, uint64_t param_3, uint64_t param_4, uint64_t param_5, uint64_t param_6, uint64_t param_7, uint64_t param_8, uint64_t param_9, uint64_t param_10, uint64_t param_11, uint64_t param_12, uint64_t param_13, uint32_t param_14, uint64_t param_15, uint64_t param_16, uint64_t param_17, uint32_t param_18, uint32_t param_19);
extern void indirect_placeholder_49(uint64_t param_0);
extern uint64_t indirect_placeholder_31(uint64_t param_0);
uint64_t bb_knuth_morris_pratt(uint64_t rdx, uint64_t rcx, uint64_t rdi, uint64_t rbx, uint64_t rsi, uint64_t r10, uint64_t r9, uint64_t r8) {
    uint64_t var_0;
    uint64_t var_1;
    uint64_t var_2;
    uint64_t var_3;
    uint32_t var_4;
    uint64_t var_5;
    uint64_t var_6;
    uint64_t var_7;
    uint32_t var_8;
    uint32_t var_9;
    uint64_t var_10;
    uint64_t var_11;
    uint64_t *var_12;
    uint64_t *var_13;
    uint64_t *var_14;
    uint64_t var_15;
    uint64_t *var_16;
    uint64_t rax_0;
    uint64_t var_17;
    uint64_t var_19;
    struct helper_divq_EAX_wrapper_ret_type var_18;
    uint64_t var_20;
    uint64_t var_21;
    uint64_t local_sp_0;
    uint64_t *var_22;
    uint64_t storemerge;
    uint64_t *var_23;
    uint64_t *var_24;
    unsigned char *var_25;
    uint64_t var_26;
    uint64_t var_27;
    uint64_t var_28;
    uint64_t **var_37;
    uint64_t *var_38;
    uint64_t var_39;
    uint64_t *var_40;
    uint64_t var_41;
    uint64_t var_42;
    uint64_t *var_43;
    unsigned char var_44;
    uint64_t var_45;
    uint64_t var_46;
    uint64_t var_48;
    uint64_t var_47;
    unsigned char var_29;
    unsigned char var_30;
    uint64_t var_31;
    uint64_t var_34;
    uint64_t var_35;
    uint64_t var_33;
    uint64_t var_36;
    uint64_t var_32;
    unsigned int loop_state_var;
    var_0 = revng_init_local_sp(0UL);
    var_1 = init_rbp();
    var_2 = init_cc_src2();
    var_3 = init_state_0x9018();
    var_4 = init_state_0x9010();
    var_5 = init_state_0x8408();
    var_6 = init_state_0x8328();
    var_7 = init_state_0x82d8();
    var_8 = init_state_0x9080();
    var_9 = init_state_0x8248();
    var_10 = var_0 + (-8L);
    *(uint64_t *)var_10 = var_1;
    var_11 = var_0 + (-104L);
    var_12 = (uint64_t *)(var_0 + (-80L));
    *var_12 = rdi;
    var_13 = (uint64_t *)(var_0 + (-88L));
    *var_13 = rsi;
    var_14 = (uint64_t *)(var_0 + (-96L));
    *var_14 = rdx;
    *(uint64_t *)var_11 = rcx;
    var_15 = *var_14;
    var_16 = (uint64_t *)(var_0 + (-56L));
    *var_16 = var_15;
    rax_0 = 0UL;
    local_sp_0 = var_11;
    storemerge = 0UL;
    var_26 = 2UL;
    if (var_15 <= 1152921504606846975UL) {
        var_17 = var_15 << 3UL;
        if (var_17 > 4015UL) {
            var_20 = var_0 + (-112L);
            *(uint64_t *)var_20 = 4236029UL;
            var_21 = indirect_placeholder_31(var_17);
            rax_0 = var_21;
            local_sp_0 = var_20;
        } else {
            var_18 = helper_divq_EAX_wrapper((struct type_4 *)(0UL), 16UL, 4235982UL, var_17 + 46UL, var_10, 0UL, 16UL, rdi, rbx, rsi, r10, r9, r8, var_3, var_4, var_5, var_6, var_7, var_8, var_9);
            var_19 = var_11 - (var_18.field_1 << 4UL);
            rax_0 = (var_19 + 31UL) & (-16L);
            local_sp_0 = var_19;
        }
    }
    var_22 = (uint64_t *)(var_0 + (-64L));
    *var_22 = rax_0;
    if (rax_0 == 0UL) {
        return storemerge;
    }
    *(uint64_t *)(rax_0 + 8UL) = 1UL;
    var_23 = (uint64_t *)(var_0 + (-24L));
    *var_23 = 0UL;
    var_24 = (uint64_t *)(var_0 + (-16L));
    *var_24 = 2UL;
    var_25 = (unsigned char *)(var_0 + (-65L));
    storemerge = 1UL;
    var_27 = *var_16;
    var_28 = helper_cc_compute_c_wrapper(var_26 - var_27, var_27, var_2, 17U);
    while (var_28 != 0UL)
        {
            var_29 = *(unsigned char *)(*var_13 + (*var_24 + (-1L)));
            *var_25 = var_29;
            var_30 = var_29;
            var_31 = *var_23;
            while (1U)
                {
                    if ((uint64_t)(*(unsigned char *)(var_31 + *var_13) - var_30) != 0UL) {
                        loop_state_var = 0U;
                        break;
                    }
                    if (var_31 != 0UL) {
                        loop_state_var = 1U;
                        break;
                    }
                    var_32 = var_31 - *(uint64_t *)(*var_22 + (var_31 << 3UL));
                    *var_23 = var_32;
                    var_30 = *var_25;
                    var_31 = var_32;
                    continue;
                }
            switch (loop_state_var) {
              case 0U:
                {
                    var_34 = (*var_24 << 3UL) + *var_22;
                    var_35 = var_31 + 1UL;
                    *var_23 = var_35;
                    *(uint64_t *)var_34 = (*var_24 - var_35);
                }
                break;
              case 1U:
                {
                    var_33 = *var_24;
                    *(uint64_t *)((var_33 << 3UL) + *var_22) = var_33;
                }
                break;
            }
            var_36 = *var_24 + 1UL;
            *var_24 = var_36;
            var_26 = var_36;
            var_27 = *var_16;
            var_28 = helper_cc_compute_c_wrapper(var_26 - var_27, var_27, var_2, 17U);
        }
    var_37 = (uint64_t **)var_11;
    **var_37 = 0UL;
    var_38 = (uint64_t *)(var_0 + (-32L));
    *var_38 = 0UL;
    var_39 = *var_12;
    var_40 = (uint64_t *)(var_0 + (-40L));
    *var_40 = var_39;
    var_41 = *var_12;
    var_42 = var_0 + (-48L);
    var_43 = (uint64_t *)var_42;
    *var_43 = var_41;
    var_44 = **(unsigned char **)var_42;
    while (var_44 != '\x00')
        {
            var_45 = *var_13;
            var_46 = *var_38;
            if ((uint64_t)(*(unsigned char *)(var_46 + var_45) - var_44) == 0UL) {
                *var_38 = (var_46 + 1UL);
                *var_43 = (*var_43 + 1UL);
                if (*var_38 != *var_16) {
                    **var_37 = *var_40;
                    break;
                }
                var_44 = **(unsigned char **)var_42;
                continue;
            }
            if (var_46 == 0UL) {
                *var_40 = (*var_40 + 1UL);
                *var_43 = (*var_43 + 1UL);
            } else {
                *var_40 = (*var_40 + *(uint64_t *)(*var_22 + (var_46 << 3UL)));
                var_47 = *var_38;
                *var_38 = (var_47 - *(uint64_t *)(*var_22 + (var_47 << 3UL)));
            }
            var_44 = **(unsigned char **)var_42;
        }
    var_48 = *var_22;
    *(uint64_t *)(local_sp_0 + (-8L)) = 4236448UL;
    indirect_placeholder_49(var_48);
    return storemerge;
}

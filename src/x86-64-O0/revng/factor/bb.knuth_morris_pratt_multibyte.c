typedef unsigned char __u_char;
typedef unsigned short __u_short;
typedef unsigned int __u_int;
typedef unsigned long __u_long;
typedef signed char __int8_t;
typedef unsigned char __uint8_t;
typedef short __int16_t;
typedef unsigned short __uint16_t;
typedef int __int32_t;
typedef unsigned int __uint32_t;
typedef long __int64_t;
typedef unsigned long __uint64_t;
typedef __int8_t __int_least8_t;
typedef __uint8_t __uint_least8_t;
typedef __int16_t __int_least16_t;
typedef __uint16_t __uint_least16_t;
typedef __int32_t __int_least32_t;
typedef __uint32_t __uint_least32_t;
typedef __int64_t __int_least64_t;
typedef __uint64_t __uint_least64_t;
typedef long __quad_t;
typedef unsigned long __u_quad_t;
typedef long __intmax_t;
typedef unsigned long __uintmax_t;
typedef unsigned long __dev_t;
typedef unsigned int __uid_t;
typedef unsigned int __gid_t;
typedef unsigned long __ino_t;
typedef unsigned long __ino64_t;
typedef unsigned int __mode_t;
typedef unsigned long __nlink_t;
typedef long __off_t;
typedef long __off64_t;
typedef int __pid_t;
typedef struct {
    int __val[2];
} __fsid_t;
typedef long __clock_t;
typedef unsigned long __rlim_t;
typedef unsigned long __rlim64_t;
typedef unsigned int __id_t;
typedef long __time_t;
typedef unsigned int __useconds_t;
typedef long __suseconds_t;
typedef int __daddr_t;
typedef int __key_t;
typedef int __clockid_t;
typedef void *__timer_t;
typedef long __blksize_t;
typedef long __blkcnt_t;
typedef long __blkcnt64_t;
typedef unsigned long __fsblkcnt_t;
typedef unsigned long __fsblkcnt64_t;
typedef unsigned long __fsfilcnt_t;
typedef unsigned long __fsfilcnt64_t;
typedef long __fsword_t;
typedef long __ssize_t;
typedef long __syscall_slong_t;
typedef unsigned long __syscall_ulong_t;
typedef __off64_t __loff_t;
typedef char *__caddr_t;
typedef long __intptr_t;
typedef unsigned int __socklen_t;
typedef int __sig_atomic_t;
typedef __int8_t int8_t;
typedef __int16_t int16_t;
typedef __int32_t int32_t;
typedef __int64_t int64_t;
typedef __uint8_t uint8_t;
typedef __uint16_t uint16_t;
typedef __uint32_t uint32_t;
typedef __uint64_t uint64_t;
typedef __int_least8_t int_least8_t;
typedef __int_least16_t int_least16_t;
typedef __int_least32_t int_least32_t;
typedef __int_least64_t int_least64_t;
typedef __uint_least8_t uint_least8_t;
typedef __uint_least16_t uint_least16_t;
typedef __uint_least32_t uint_least32_t;
typedef __uint_least64_t uint_least64_t;
typedef signed char int_fast8_t;
typedef long int_fast16_t;
typedef long int_fast32_t;
typedef long int_fast64_t;
typedef unsigned char uint_fast8_t;
typedef unsigned long uint_fast16_t;
typedef unsigned long uint_fast32_t;
typedef unsigned long uint_fast64_t;
typedef long intptr_t;
typedef unsigned long uintptr_t;
typedef __intmax_t intmax_t;
typedef __uintmax_t uintmax_t;
struct helper_divq_EAX_wrapper_ret_type;
struct type_5;
struct indirect_placeholder_50_ret_type;
struct helper_divq_EAX_wrapper_ret_type {
    uint64_t field_0;
    uint64_t field_1;
    uint64_t field_2;
    uint32_t field_3;
    uint64_t field_4;
    uint32_t field_5;
    uint64_t field_6;
    uint32_t field_7;
    uint64_t field_8;
    uint32_t field_9;
    uint32_t field_10;
    unsigned char field_11;
    uint32_t field_12;
};
struct type_5 {
};
struct indirect_placeholder_50_ret_type {
    uint64_t field_0;
    uint64_t field_1;
    uint64_t field_2;
};
extern uint64_t revng_init_local_sp(uint64_t param_0);
extern uint64_t init_rbp(void);
extern uint64_t init_cc_src2(void);
extern uint64_t helper_cc_compute_c_wrapper(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint32_t param_3);
extern void indirect_placeholder(void);
extern void indirect_placeholder_39(uint64_t param_0, uint64_t param_1);
extern uint64_t init_rbx(void);
extern uint64_t init_r10(void);
extern uint64_t init_state_0x9018(void);
extern uint32_t init_state_0x9010(void);
extern uint64_t init_state_0x8408(void);
extern uint64_t init_state_0x8328(void);
extern uint64_t init_state_0x82d8(void);
extern uint32_t init_state_0x9080(void);
extern uint32_t init_state_0x8248(void);
extern struct helper_divq_EAX_wrapper_ret_type helper_divq_EAX_wrapper(struct type_5 *param_0, uint64_t param_1, uint64_t param_2, uint64_t param_3, uint64_t param_4, uint64_t param_5, uint64_t param_6, uint64_t param_7, uint64_t param_8, uint64_t param_9, uint64_t param_10, uint64_t param_11, uint64_t param_12, uint64_t param_13, uint32_t param_14, uint64_t param_15, uint64_t param_16, uint64_t param_17, uint32_t param_18, uint32_t param_19);
extern uint64_t init_r9(void);
extern uint64_t init_r8(void);
extern struct indirect_placeholder_50_ret_type indirect_placeholder_50(uint64_t param_0);
extern void indirect_placeholder_49(uint64_t param_0);
extern uint64_t indirect_placeholder_31(uint64_t param_0);
uint64_t bb_knuth_morris_pratt_multibyte(uint64_t rdx, uint64_t rdi, uint64_t rsi) {
    uint64_t var_0;
    uint64_t var_1;
    uint64_t var_2;
    uint64_t var_3;
    uint64_t var_4;
    uint64_t var_5;
    uint64_t var_6;
    uint64_t var_7;
    uint32_t var_8;
    uint64_t var_9;
    uint64_t var_10;
    uint64_t var_11;
    uint32_t var_12;
    uint32_t var_13;
    uint64_t var_14;
    uint64_t *var_15;
    uint64_t *var_16;
    uint64_t var_17;
    uint64_t var_18;
    uint64_t var_19;
    struct indirect_placeholder_50_ret_type var_20;
    uint64_t var_21;
    uint64_t var_22;
    uint64_t var_23;
    uint64_t *var_24;
    uint64_t rax_0;
    uint64_t local_sp_5;
    uint64_t var_81;
    uint64_t var_82;
    uint64_t local_sp_1;
    uint64_t var_92;
    uint64_t local_sp_0;
    uint64_t var_93;
    uint64_t var_87;
    uint64_t local_sp_7_be;
    uint64_t var_89;
    uint64_t var_90;
    uint64_t var_85;
    uint64_t var_83;
    uint64_t var_84;
    uint64_t local_sp_4178;
    uint64_t _pre196;
    uint64_t var_94;
    uint64_t storemerge12;
    uint64_t local_sp_4174;
    uint64_t var_86;
    uint64_t local_sp_6;
    uint64_t var_88;
    uint64_t var_91;
    uint64_t local_sp_7;
    uint64_t var_79;
    unsigned char var_80;
    uint64_t *var_48;
    uint64_t *var_49;
    uint64_t var_50;
    uint64_t *var_51;
    uint64_t local_sp_12182;
    uint64_t var_52;
    uint64_t local_sp_8;
    uint64_t var_53;
    uint64_t var_54;
    uint64_t *var_33;
    uint64_t **var_68;
    uint64_t *var_69;
    uint64_t var_70;
    uint64_t *var_71;
    uint64_t var_72;
    unsigned char *var_73;
    uint64_t var_74;
    uint64_t *var_75;
    unsigned char *var_76;
    uint32_t *var_77;
    uint64_t *var_78;
    uint64_t var_55;
    uint64_t var_56;
    uint64_t local_sp_9;
    uint64_t _pre190;
    uint64_t _pre_phi199;
    uint64_t var_57;
    uint64_t var_58;
    uint64_t var_59;
    uint64_t var_62;
    uint64_t var_60;
    uint64_t var_61;
    uint64_t var_64;
    uint64_t _pre192;
    uint64_t local_sp_12187;
    uint64_t local_sp_12183;
    uint64_t var_65;
    uint64_t var_66;
    uint64_t var_63;
    uint64_t var_67;
    uint64_t var_46;
    uint64_t var_47;
    uint64_t local_sp_13;
    uint64_t var_45;
    uint64_t var_25;
    uint64_t var_27;
    struct helper_divq_EAX_wrapper_ret_type var_26;
    uint64_t var_28;
    uint64_t var_29;
    uint64_t local_sp_14;
    uint64_t *var_30;
    uint64_t *var_31;
    uint64_t var_32;
    uint64_t *var_34;
    uint64_t var_35;
    uint64_t var_36;
    uint64_t *var_37;
    uint64_t var_38;
    unsigned char *var_39;
    uint64_t var_40;
    unsigned char *var_41;
    uint32_t *var_42;
    unsigned char *var_43;
    uint64_t *var_44;
    unsigned int loop_state_var;
    var_0 = revng_init_local_sp(0UL);
    var_1 = init_rbp();
    var_2 = init_cc_src2();
    var_3 = init_rbx();
    var_4 = init_r10();
    var_5 = init_r9();
    var_6 = init_r8();
    var_7 = init_state_0x9018();
    var_8 = init_state_0x9010();
    var_9 = init_state_0x8408();
    var_10 = init_state_0x8328();
    var_11 = init_state_0x82d8();
    var_12 = init_state_0x9080();
    var_13 = init_state_0x8248();
    var_14 = var_0 + (-8L);
    *(uint64_t *)var_14 = var_1;
    var_15 = (uint64_t *)(var_0 + (-240L));
    *var_15 = rdi;
    var_16 = (uint64_t *)(var_0 + (-248L));
    *var_16 = rsi;
    var_17 = var_0 + (-256L);
    *(uint64_t *)var_17 = rdx;
    var_18 = *var_16;
    var_19 = var_0 + (-272L);
    *(uint64_t *)var_19 = 4236502UL;
    var_20 = indirect_placeholder_50(var_18);
    var_21 = var_20.field_0;
    var_22 = var_20.field_1;
    var_23 = var_20.field_2;
    var_24 = (uint64_t *)(var_0 + (-56L));
    *var_24 = var_21;
    var_52 = 2UL;
    rax_0 = 0UL;
    local_sp_14 = var_19;
    storemerge12 = 0UL;
    if (var_21 <= 164703072086692425UL) {
        var_25 = var_21 * 56UL;
        if (var_25 > 4015UL) {
            var_28 = var_0 + (-280L);
            *(uint64_t *)var_28 = 4236669UL;
            var_29 = indirect_placeholder_31(var_25);
            rax_0 = var_29;
            local_sp_14 = var_28;
        } else {
            var_26 = helper_divq_EAX_wrapper((struct type_5 *)(0UL), 16UL, 4236608UL, var_25 + 46UL, var_14, 0UL, 16UL, var_22, var_3, var_23, var_4, var_5, var_6, var_7, var_8, var_9, var_10, var_11, var_12, var_13);
            var_27 = var_19 - (var_26.field_1 << 4UL);
            rax_0 = (var_27 + 31UL) & (-16L);
            local_sp_14 = var_27;
        }
    }
    var_30 = (uint64_t *)(var_0 + (-64L));
    *var_30 = rax_0;
    if (rax_0 == 0UL) {
        return storemerge12;
    }
    var_31 = (uint64_t *)(var_0 + (-72L));
    *var_31 = rax_0;
    var_32 = rax_0 + (*var_24 * 48UL);
    *(uint64_t *)(var_0 + (-80L)) = var_32;
    var_33 = (uint64_t *)(var_0 + (-88L));
    *var_33 = var_32;
    var_34 = (uint64_t *)(var_0 + (-16L));
    *var_34 = 0UL;
    var_35 = *var_16;
    var_36 = var_0 + (-152L);
    var_37 = (uint64_t *)var_36;
    *var_37 = var_35;
    var_38 = var_0 + (-168L);
    var_39 = (unsigned char *)var_38;
    *var_39 = (unsigned char)'\x00';
    var_40 = local_sp_14 + (-8L);
    *(uint64_t *)var_40 = 4236802UL;
    indirect_placeholder();
    var_41 = (unsigned char *)(var_0 + (-156L));
    *var_41 = (unsigned char)'\x00';
    var_42 = (uint32_t *)(var_0 + (-132L));
    var_43 = (unsigned char *)(var_0 + (-136L));
    var_44 = (uint64_t *)(var_0 + (-144L));
    local_sp_13 = var_40;
    storemerge12 = 1UL;
    while (1U)
        {
            var_45 = local_sp_13 + (-8L);
            *(uint64_t *)var_45 = 4236911UL;
            indirect_placeholder_49(var_38);
            local_sp_8 = var_45;
            if (*var_43 != '\x01') {
                if (*var_42 == 0U) {
                    break;
                }
            }
            var_46 = *var_31 + (*var_34 * 48UL);
            var_47 = local_sp_13 + (-16L);
            *(uint64_t *)var_47 = 4236860UL;
            indirect_placeholder_39(var_46, var_36);
            *var_37 = (*var_44 + *var_37);
            *var_41 = (unsigned char)'\x00';
            *var_34 = (*var_34 + 1UL);
            local_sp_13 = var_47;
            continue;
        }
    *(uint64_t *)(*var_33 + 8UL) = 1UL;
    var_48 = (uint64_t *)(var_0 + (-32L));
    *var_48 = 0UL;
    var_49 = (uint64_t *)(var_0 + (-24L));
    *var_49 = 2UL;
    var_50 = var_0 + (-96L);
    var_51 = (uint64_t *)var_50;
    var_53 = *var_24;
    var_54 = helper_cc_compute_c_wrapper(var_52 - var_53, var_53, var_2, 17U);
    local_sp_9 = local_sp_8;
    while (var_54 != 0UL)
        {
            var_55 = *var_31 + ((*var_49 * 48UL) + (-48L));
            *var_51 = var_55;
            var_56 = var_55;
            while (1U)
                {
                    local_sp_12187 = local_sp_9;
                    local_sp_12183 = local_sp_9;
                    if (*(unsigned char *)(var_56 + 16UL) != '\x00') {
                        var_57 = *var_48;
                        var_58 = *var_31 + (var_57 * 48UL);
                        _pre_phi199 = var_58;
                        var_59 = var_57;
                        var_64 = var_57;
                        var_62 = var_57;
                        if (*(unsigned char *)(var_58 + 16UL) != '\x00') {
                            if ((uint64_t)(*(uint32_t *)(var_56 + 20UL) - *(uint32_t *)(var_58 + 20UL)) != 0UL) {
                                loop_state_var = 1U;
                                break;
                            }
                            local_sp_9 = local_sp_12183;
                            local_sp_12182 = local_sp_12183;
                            if (var_62 != 0UL) {
                                loop_state_var = 0U;
                                break;
                            }
                            *var_48 = (var_62 - *(uint64_t *)(*var_33 + (var_62 << 3UL)));
                            var_56 = *var_51;
                            continue;
                        }
                    }
                    _pre190 = *var_48;
                    _pre_phi199 = *var_31 + (_pre190 * 48UL);
                    var_59 = _pre190;
                }
            switch (loop_state_var) {
              case 0U:
                {
                    var_63 = *var_49;
                    *(uint64_t *)((var_63 << 3UL) + *var_33) = var_63;
                }
                break;
              case 2U:
              case 1U:
                {
                    switch (loop_state_var) {
                      case 2U:
                        {
                            var_64 = *var_48;
                        }
                        break;
                      case 1U:
                        {
                        }
                        break;
                    }
                    var_65 = (*var_49 << 3UL) + *var_33;
                    var_66 = var_64 + 1UL;
                    *var_48 = var_66;
                    *(uint64_t *)var_65 = (*var_49 - var_66);
                    local_sp_12182 = local_sp_12187;
                }
                break;
            }
            var_67 = *var_49 + 1UL;
            *var_49 = var_67;
            var_52 = var_67;
            local_sp_8 = local_sp_12182;
            var_53 = *var_24;
            var_54 = helper_cc_compute_c_wrapper(var_52 - var_53, var_53, var_2, 17U);
            local_sp_9 = local_sp_8;
        }
    var_68 = (uint64_t **)var_17;
    **var_68 = 0UL;
    var_69 = (uint64_t *)(var_0 + (-40L));
    *var_69 = 0UL;
    var_70 = *var_15;
    var_71 = (uint64_t *)(var_0 + (-216L));
    *var_71 = var_70;
    var_72 = var_0 + (-232L);
    *(unsigned char *)var_72 = (unsigned char)'\x00';
    *(uint64_t *)(local_sp_8 + (-8L)) = 4237428UL;
    indirect_placeholder();
    var_73 = (unsigned char *)(var_0 + (-220L));
    *var_73 = (unsigned char)'\x00';
    *var_37 = *var_15;
    *var_39 = (unsigned char)'\x00';
    var_74 = local_sp_8 + (-16L);
    *(uint64_t *)var_74 = 4237485UL;
    indirect_placeholder();
    *var_41 = (unsigned char)'\x00';
    var_75 = (uint64_t *)(var_0 + (-48L));
    var_76 = (unsigned char *)(var_0 + (-200L));
    var_77 = (uint32_t *)(var_0 + (-196L));
    var_78 = (uint64_t *)(var_0 + (-208L));
    local_sp_7 = var_74;
    while (1U)
        {
            var_79 = local_sp_7 + (-8L);
            *(uint64_t *)var_79 = 4238092UL;
            indirect_placeholder_49(var_38);
            var_80 = *var_43;
            local_sp_4178 = var_79;
            local_sp_5 = var_79;
            local_sp_4174 = var_79;
            if (var_80 != '\x01') {
                if (*var_42 == 0U) {
                    break;
                }
            }
            var_81 = *var_69;
            var_82 = *var_31 + (var_81 * 48UL);
            var_93 = var_81;
            var_85 = var_81;
            if (*(unsigned char *)(var_82 + 16UL) != '\x00') {
                if (*(uint64_t *)(var_82 + 8UL) != *var_44) {
                    var_83 = *(uint64_t *)var_82;
                    var_84 = local_sp_7 + (-16L);
                    *(uint64_t *)var_84 = 4237708UL;
                    indirect_placeholder();
                    local_sp_4178 = var_84;
                    local_sp_4174 = var_84;
                    if ((uint64_t)(uint32_t)var_83 != 0UL) {
                        _pre196 = *var_69;
                        var_93 = _pre196;
                        *var_69 = (var_93 + 1UL);
                        *var_37 = (*var_44 + *var_37);
                        *var_41 = (unsigned char)'\x00';
                        local_sp_7_be = local_sp_4178;
                        local_sp_5 = local_sp_4178;
                        if (*var_69 != *var_24) {
                            local_sp_7 = local_sp_7_be;
                            continue;
                        }
                        **var_68 = *var_71;
                        break;
                    }
                    var_85 = *var_69;
                }
                local_sp_6 = local_sp_4174;
                if (var_85 != 0UL) {
                    var_86 = *(uint64_t *)(*var_33 + (var_85 << 3UL));
                    *var_75 = var_86;
                    *var_69 = (*var_69 - var_86);
                    var_87 = *var_75;
                    local_sp_7_be = local_sp_6;
                    while (var_87 != 0UL)
                        {
                            var_88 = local_sp_6 + (-8L);
                            *(uint64_t *)var_88 = 4237865UL;
                            indirect_placeholder_49(var_72);
                            local_sp_1 = var_88;
                            if (*var_76 != '\x01' & *var_77 == 0U) {
                                var_89 = local_sp_6 + (-16L);
                                *(uint64_t *)var_89 = 4237910UL;
                                indirect_placeholder();
                                local_sp_1 = var_89;
                            }
                            *var_71 = (*var_78 + *var_71);
                            *var_73 = (unsigned char)'\x00';
                            var_90 = *var_75 + (-1L);
                            *var_75 = var_90;
                            var_87 = var_90;
                            local_sp_6 = local_sp_1;
                            local_sp_7_be = local_sp_6;
                        }
                }
                var_91 = local_sp_4174 + (-8L);
                *(uint64_t *)var_91 = 4237970UL;
                indirect_placeholder_49(var_72);
                local_sp_0 = var_91;
                if (*var_76 != '\x01' & *var_77 == 0U) {
                    var_92 = local_sp_4174 + (-16L);
                    *(uint64_t *)var_92 = 4238015UL;
                    indirect_placeholder();
                    local_sp_0 = var_92;
                }
                *var_71 = (*var_78 + *var_71);
                *var_73 = (unsigned char)'\x00';
                *var_37 = (*var_44 + *var_37);
                *var_41 = (unsigned char)'\x00';
                local_sp_7_be = local_sp_0;
                local_sp_7 = local_sp_7_be;
                continue;
            }
            if (var_80 != '\x00') {
                if ((uint64_t)(*(uint32_t *)(var_82 + 20UL) - *var_42) != 0UL) {
                    *var_69 = (var_93 + 1UL);
                    *var_37 = (*var_44 + *var_37);
                    *var_41 = (unsigned char)'\x00';
                    local_sp_7_be = local_sp_4178;
                    local_sp_5 = local_sp_4178;
                    if (*var_69 == *var_24) {
                        local_sp_7 = local_sp_7_be;
                        continue;
                    }
                    **var_68 = *var_71;
                    break;
                }
            }
            if (*(uint64_t *)(var_82 + 8UL) != *var_44) {
                var_83 = *(uint64_t *)var_82;
                var_84 = local_sp_7 + (-16L);
                *(uint64_t *)var_84 = 4237708UL;
                indirect_placeholder();
                local_sp_4178 = var_84;
                local_sp_4174 = var_84;
                if ((uint64_t)(uint32_t)var_83 != 0UL) {
                    _pre196 = *var_69;
                    var_93 = _pre196;
                    *var_69 = (var_93 + 1UL);
                    *var_37 = (*var_44 + *var_37);
                    *var_41 = (unsigned char)'\x00';
                    local_sp_7_be = local_sp_4178;
                    local_sp_5 = local_sp_4178;
                    if (*var_69 == *var_24) {
                        local_sp_7 = local_sp_7_be;
                        continue;
                    }
                    **var_68 = *var_71;
                    break;
                }
                var_85 = *var_69;
            }
        }
    var_94 = *var_30;
    *(uint64_t *)(local_sp_5 + (-8L)) = 4238142UL;
    indirect_placeholder_49(var_94);
    return storemerge12;
}

typedef unsigned char __u_char;
typedef unsigned short __u_short;
typedef unsigned int __u_int;
typedef unsigned long __u_long;
typedef signed char __int8_t;
typedef unsigned char __uint8_t;
typedef short __int16_t;
typedef unsigned short __uint16_t;
typedef int __int32_t;
typedef unsigned int __uint32_t;
typedef long __int64_t;
typedef unsigned long __uint64_t;
typedef __int8_t __int_least8_t;
typedef __uint8_t __uint_least8_t;
typedef __int16_t __int_least16_t;
typedef __uint16_t __uint_least16_t;
typedef __int32_t __int_least32_t;
typedef __uint32_t __uint_least32_t;
typedef __int64_t __int_least64_t;
typedef __uint64_t __uint_least64_t;
typedef long __quad_t;
typedef unsigned long __u_quad_t;
typedef long __intmax_t;
typedef unsigned long __uintmax_t;
typedef unsigned long __dev_t;
typedef unsigned int __uid_t;
typedef unsigned int __gid_t;
typedef unsigned long __ino_t;
typedef unsigned long __ino64_t;
typedef unsigned int __mode_t;
typedef unsigned long __nlink_t;
typedef long __off_t;
typedef long __off64_t;
typedef int __pid_t;
typedef struct {
    int __val[2];
} __fsid_t;
typedef long __clock_t;
typedef unsigned long __rlim_t;
typedef unsigned long __rlim64_t;
typedef unsigned int __id_t;
typedef long __time_t;
typedef unsigned int __useconds_t;
typedef long __suseconds_t;
typedef int __daddr_t;
typedef int __key_t;
typedef int __clockid_t;
typedef void *__timer_t;
typedef long __blksize_t;
typedef long __blkcnt_t;
typedef long __blkcnt64_t;
typedef unsigned long __fsblkcnt_t;
typedef unsigned long __fsblkcnt64_t;
typedef unsigned long __fsfilcnt_t;
typedef unsigned long __fsfilcnt64_t;
typedef long __fsword_t;
typedef long __ssize_t;
typedef long __syscall_slong_t;
typedef unsigned long __syscall_ulong_t;
typedef __off64_t __loff_t;
typedef char *__caddr_t;
typedef long __intptr_t;
typedef unsigned int __socklen_t;
typedef int __sig_atomic_t;
typedef __int8_t int8_t;
typedef __int16_t int16_t;
typedef __int32_t int32_t;
typedef __int64_t int64_t;
typedef __uint8_t uint8_t;
typedef __uint16_t uint16_t;
typedef __uint32_t uint32_t;
typedef __uint64_t uint64_t;
typedef __int_least8_t int_least8_t;
typedef __int_least16_t int_least16_t;
typedef __int_least32_t int_least32_t;
typedef __int_least64_t int_least64_t;
typedef __uint_least8_t uint_least8_t;
typedef __uint_least16_t uint_least16_t;
typedef __uint_least32_t uint_least32_t;
typedef __uint_least64_t uint_least64_t;
typedef signed char int_fast8_t;
typedef long int_fast16_t;
typedef long int_fast32_t;
typedef long int_fast64_t;
typedef unsigned char uint_fast8_t;
typedef unsigned long uint_fast16_t;
typedef unsigned long uint_fast32_t;
typedef unsigned long uint_fast64_t;
typedef long intptr_t;
typedef unsigned long uintptr_t;
typedef __intmax_t intmax_t;
typedef __uintmax_t uintmax_t;
typedef _Bool bool;
extern void abort(void);
extern uint64_t revng_init_local_sp(uint64_t param_0);
extern uint64_t init_rbp(void);
extern uint64_t init_cc_src2(void);
extern uint64_t helper_cc_compute_c_wrapper(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint32_t param_3);
extern void indirect_placeholder(void);
extern uint64_t init_rbx(void);
extern uint64_t init_r9(void);
extern uint64_t init_r8(void);
extern void indirect_placeholder_49(uint64_t param_0);
extern uint64_t indirect_placeholder_31(uint64_t param_0);
extern void indirect_placeholder_12(uint64_t param_0, uint64_t param_1, uint64_t param_2);
uint64_t bb_trim2(uint64_t rdi, uint64_t rsi) {
    uint64_t local_sp_13;
    uint64_t var_0;
    uint64_t var_1;
    uint64_t var_2;
    uint64_t var_3;
    uint64_t var_4;
    uint64_t var_5;
    uint64_t var_6;
    uint64_t *var_7;
    uint32_t *var_8;
    uint64_t var_9;
    uint64_t *var_10;
    bool var_11;
    uint64_t var_12;
    uint64_t *var_13;
    uint64_t var_37;
    uint64_t var_30;
    uint64_t local_sp_0;
    uint64_t var_31;
    uint64_t var_32;
    unsigned char **var_33;
    uint64_t var_34;
    uint64_t var_35;
    uint64_t var_36;
    uint64_t local_sp_2;
    uint64_t local_sp_8;
    uint32_t var_76;
    uint64_t var_77;
    uint32_t var_78;
    uint64_t local_sp_1;
    uint32_t var_81;
    uint64_t local_sp_9;
    uint64_t var_71;
    uint32_t var_79;
    uint64_t var_80;
    uint64_t local_sp_3;
    uint32_t var_84;
    uint32_t var_82;
    uint64_t var_83;
    uint64_t local_sp_5;
    uint32_t var_87;
    uint32_t var_85;
    uint64_t var_86;
    uint64_t local_sp_6;
    uint64_t local_sp_7;
    uint32_t var_88;
    uint64_t var_89;
    uint64_t var_72;
    uint64_t var_73;
    uint64_t var_74;
    uint32_t var_75;
    uint32_t var_53;
    uint64_t var_54;
    uint64_t local_sp_11;
    uint64_t local_sp_10;
    uint64_t var_49;
    uint64_t var_50;
    uint64_t var_51;
    uint64_t var_52;
    uint64_t var_55;
    uint32_t var_56;
    uint64_t var_38;
    uint64_t *var_39;
    uint64_t var_40;
    uint64_t var_41;
    uint64_t var_42;
    uint64_t *var_43;
    uint64_t var_44;
    unsigned char *var_45;
    uint32_t *var_46;
    unsigned char *var_47;
    uint64_t *var_48;
    uint64_t local_sp_12;
    uint32_t *var_57;
    uint64_t var_58;
    uint64_t *var_59;
    uint64_t var_60;
    uint64_t var_61;
    uint64_t var_62;
    uint64_t *var_63;
    uint64_t var_64;
    unsigned char *var_65;
    unsigned char *var_66;
    uint32_t *var_67;
    uint64_t var_68;
    uint64_t *var_69;
    uint64_t *var_70;
    uint32_t var_24;
    uint64_t var_16;
    uint64_t var_17;
    uint64_t *var_18;
    unsigned char var_19;
    uint64_t local_sp_14;
    uint64_t var_20;
    uint64_t var_21;
    uint64_t var_22;
    uint64_t var_23;
    uint64_t local_sp_15;
    uint64_t var_25;
    uint64_t var_26;
    uint64_t var_27;
    uint64_t var_28;
    uint64_t *var_29;
    bool var_14;
    bool var_15;
    var_0 = revng_init_local_sp(0UL);
    var_1 = init_rbp();
    var_2 = init_cc_src2();
    var_3 = init_rbx();
    var_4 = init_r9();
    var_5 = init_r8();
    var_6 = var_0 + (-8L);
    *(uint64_t *)var_6 = var_1;
    *(uint64_t *)(var_0 + (-16L)) = var_3;
    var_7 = (uint64_t *)(var_0 + (-144L));
    *var_7 = rdi;
    var_8 = (uint32_t *)(var_0 + (-148L));
    *var_8 = (uint32_t)rsi;
    var_9 = *var_7;
    *(uint64_t *)(var_0 + (-160L)) = 4225423UL;
    indirect_placeholder();
    var_10 = (uint64_t *)(var_0 + (-56L));
    *var_10 = var_9;
    var_11 = (var_9 == 0UL);
    var_12 = var_0 + (-168L);
    var_13 = (uint64_t *)var_12;
    local_sp_13 = var_12;
    var_56 = 0U;
    local_sp_12 = var_12;
    var_24 = 0U;
    local_sp_15 = var_12;
    if (var_11) {
        *var_13 = 4225439UL;
        indirect_placeholder_12(var_6, var_4, var_5);
        abort();
    }
    *var_13 = 4225444UL;
    indirect_placeholder();
    var_14 = (var_9 > 1UL);
    var_15 = (*var_8 == 0U);
    if (!var_14) {
        if (!var_15) {
            var_16 = *var_10;
            var_17 = var_0 + (-48L);
            var_18 = (uint64_t *)var_17;
            *var_18 = var_16;
            var_19 = **(unsigned char **)var_17;
            local_sp_14 = local_sp_13;
            while (var_19 != '\x00')
                {
                    var_20 = (uint64_t)var_19;
                    var_21 = local_sp_13 + (-8L);
                    *(uint64_t *)var_21 = 4226041UL;
                    var_22 = indirect_placeholder_31(var_20);
                    local_sp_13 = var_21;
                    local_sp_14 = var_21;
                    if ((uint64_t)(uint32_t)var_22 == 0UL) {
                        break;
                    }
                    *var_18 = (*var_18 + 1UL);
                    var_19 = **(unsigned char **)var_17;
                    local_sp_14 = local_sp_13;
                }
            *(uint64_t *)(local_sp_14 + (-8L)) = 4226057UL;
            indirect_placeholder();
            var_23 = local_sp_14 + (-16L);
            *(uint64_t *)var_23 = 4226080UL;
            indirect_placeholder();
            var_24 = *var_8;
            local_sp_15 = var_23;
        }
        if (var_24 != 1U) {
            var_25 = *var_10;
            var_26 = local_sp_15 + (-8L);
            *(uint64_t *)var_26 = 4226101UL;
            indirect_placeholder();
            var_27 = *var_10 + (var_25 + (-1L));
            var_28 = var_0 + (-48L);
            var_29 = (uint64_t *)var_28;
            *var_29 = var_27;
            var_30 = var_27;
            local_sp_0 = var_26;
            var_31 = *var_10;
            var_32 = helper_cc_compute_c_wrapper(var_30 - var_31, var_31, var_2, 17U);
            while (var_32 != 0UL)
                {
                    var_33 = (unsigned char **)var_28;
                    var_34 = (uint64_t)**var_33;
                    var_35 = local_sp_0 + (-8L);
                    *(uint64_t *)var_35 = 4226157UL;
                    var_36 = indirect_placeholder_31(var_34);
                    local_sp_0 = var_35;
                    if ((uint64_t)(uint32_t)var_36 == 0UL) {
                        break;
                    }
                    **var_33 = (unsigned char)'\x00';
                    var_37 = *var_29 + (-1L);
                    *var_29 = var_37;
                    var_30 = var_37;
                    var_31 = *var_10;
                    var_32 = helper_cc_compute_c_wrapper(var_30 - var_31, var_31, var_2, 17U);
                }
        }
    }
    if (!var_15) {
        var_38 = *var_10;
        var_39 = (uint64_t *)(var_0 + (-112L));
        *var_39 = var_38;
        var_40 = *var_10;
        *(uint64_t *)(var_0 + (-176L)) = 4225491UL;
        indirect_placeholder();
        var_41 = var_40 + var_38;
        var_42 = var_0 + (-136L);
        var_43 = (uint64_t *)var_42;
        *var_43 = var_41;
        *(unsigned char *)(var_0 + (-128L)) = (unsigned char)'\x00';
        var_44 = var_0 + (-184L);
        *(uint64_t *)var_44 = 4225528UL;
        indirect_placeholder();
        var_45 = (unsigned char *)(var_0 + (-116L));
        *var_45 = (unsigned char)'\x00';
        var_46 = (uint32_t *)(var_0 + (-92L));
        var_47 = (unsigned char *)(var_0 + (-96L));
        var_48 = (uint64_t *)(var_0 + (-104L));
        local_sp_10 = var_44;
        var_49 = *var_39;
        var_50 = *var_43;
        var_51 = helper_cc_compute_c_wrapper(var_49 - var_50, var_50, var_2, 17U);
        local_sp_11 = local_sp_10;
        while (var_51 != 0UL)
            {
                var_52 = local_sp_10 + (-8L);
                *(uint64_t *)var_52 = 4225578UL;
                indirect_placeholder_49(var_42);
                local_sp_11 = var_52;
                if (*var_47 == '\x00') {
                    break;
                }
                var_53 = *var_46;
                var_54 = local_sp_10 + (-16L);
                *(uint64_t *)var_54 = 4225596UL;
                indirect_placeholder();
                local_sp_10 = var_54;
                local_sp_11 = var_54;
                if (var_53 == 0U) {
                    break;
                }
                *var_39 = (*var_48 + *var_39);
                *var_45 = (unsigned char)'\x00';
                var_49 = *var_39;
                var_50 = *var_43;
                var_51 = helper_cc_compute_c_wrapper(var_49 - var_50, var_50, var_2, 17U);
                local_sp_11 = local_sp_10;
            }
        *(uint64_t *)(local_sp_11 + (-8L)) = 4225612UL;
        indirect_placeholder();
        var_55 = local_sp_11 + (-16L);
        *(uint64_t *)var_55 = 4225635UL;
        indirect_placeholder();
        var_56 = *var_8;
        local_sp_12 = var_55;
    }
    if (var_56 == 1U) {
        return *var_10;
    }
    var_57 = (uint32_t *)(var_0 + (-28L));
    *var_57 = 0U;
    var_58 = *var_10;
    var_59 = (uint64_t *)(var_0 + (-112L));
    *var_59 = var_58;
    var_60 = *var_10;
    *(uint64_t *)(local_sp_12 + (-8L)) = 4225679UL;
    indirect_placeholder();
    var_61 = var_60 + var_58;
    var_62 = var_0 + (-136L);
    var_63 = (uint64_t *)var_62;
    *var_63 = var_61;
    *(unsigned char *)(var_0 + (-128L)) = (unsigned char)'\x00';
    var_64 = local_sp_12 + (-16L);
    *(uint64_t *)var_64 = 4225716UL;
    indirect_placeholder();
    var_65 = (unsigned char *)(var_0 + (-116L));
    *var_65 = (unsigned char)'\x00';
    var_66 = (unsigned char *)(var_0 + (-96L));
    var_67 = (uint32_t *)(var_0 + (-92L));
    var_68 = var_0 + (-40L);
    var_69 = (uint64_t *)var_68;
    var_70 = (uint64_t *)(var_0 + (-104L));
    local_sp_9 = var_64;
    var_71 = *var_59;
    var_72 = *var_63;
    var_73 = helper_cc_compute_c_wrapper(var_71 - var_72, var_72, var_2, 17U);
    while (var_73 != 0UL)
        {
            var_74 = local_sp_9 + (-8L);
            *(uint64_t *)var_74 = 4225951UL;
            indirect_placeholder_49(var_62);
            var_75 = *var_57;
            var_78 = var_75;
            local_sp_1 = var_74;
            var_78 = 0U;
            if (var_75 != 0U & *var_66 != '\x00') {
                var_76 = *var_67;
                var_77 = local_sp_9 + (-16L);
                *(uint64_t *)var_77 = 4225743UL;
                indirect_placeholder();
                local_sp_1 = var_77;
                local_sp_8 = var_77;
                if (var_76 != 0U) {
                    *var_59 = (*var_70 + *var_59);
                    *var_65 = (unsigned char)'\x00';
                    local_sp_9 = local_sp_8;
                    var_71 = *var_59;
                    var_72 = *var_63;
                    var_73 = helper_cc_compute_c_wrapper(var_71 - var_72, var_72, var_2, 17U);
                    continue;
                }
                var_78 = *var_57;
            }
            local_sp_2 = local_sp_1;
            var_81 = var_78;
            local_sp_3 = local_sp_1;
            if (var_78 != 0U) {
                if (*var_66 != '\x01') {
                    *var_57 = 1U;
                    local_sp_8 = local_sp_2;
                    *var_59 = (*var_70 + *var_59);
                    *var_65 = (unsigned char)'\x00';
                    local_sp_9 = local_sp_8;
                    var_71 = *var_59;
                    var_72 = *var_63;
                    var_73 = helper_cc_compute_c_wrapper(var_71 - var_72, var_72, var_2, 17U);
                    continue;
                }
                var_79 = *var_67;
                var_80 = local_sp_1 + (-8L);
                *(uint64_t *)var_80 = 4225779UL;
                indirect_placeholder();
                local_sp_2 = var_80;
                local_sp_3 = var_80;
                if (var_79 != 0U) {
                    *var_57 = 1U;
                    local_sp_8 = local_sp_2;
                    *var_59 = (*var_70 + *var_59);
                    *var_65 = (unsigned char)'\x00';
                    local_sp_9 = local_sp_8;
                    var_71 = *var_59;
                    var_72 = *var_63;
                    var_73 = helper_cc_compute_c_wrapper(var_71 - var_72, var_72, var_2, 17U);
                    continue;
                }
                var_81 = *var_57;
            }
            var_84 = var_81;
            local_sp_5 = local_sp_3;
            local_sp_8 = local_sp_3;
            if (var_81 != 1U) {
                if (*var_66 != '\x01') {
                    *var_59 = (*var_70 + *var_59);
                    *var_65 = (unsigned char)'\x00';
                    local_sp_9 = local_sp_8;
                    var_71 = *var_59;
                    var_72 = *var_63;
                    var_73 = helper_cc_compute_c_wrapper(var_71 - var_72, var_72, var_2, 17U);
                    continue;
                }
                var_82 = *var_67;
                var_83 = local_sp_3 + (-8L);
                *(uint64_t *)var_83 = 4225819UL;
                indirect_placeholder();
                local_sp_5 = var_83;
                local_sp_8 = var_83;
                if (var_82 != 0U) {
                    *var_59 = (*var_70 + *var_59);
                    *var_65 = (unsigned char)'\x00';
                    local_sp_9 = local_sp_8;
                    var_71 = *var_59;
                    var_72 = *var_63;
                    var_73 = helper_cc_compute_c_wrapper(var_71 - var_72, var_72, var_2, 17U);
                    continue;
                }
                var_84 = *var_57;
            }
            var_87 = var_84;
            local_sp_6 = local_sp_5;
            var_87 = 1U;
            if (var_84 != 1U & *var_66 != '\x00') {
                var_85 = *var_67;
                var_86 = local_sp_5 + (-8L);
                *(uint64_t *)var_86 = 4225849UL;
                indirect_placeholder();
                local_sp_6 = var_86;
                local_sp_8 = var_86;
                if (var_85 != 0U) {
                    *var_57 = 2U;
                    *var_69 = *var_59;
                    *var_59 = (*var_70 + *var_59);
                    *var_65 = (unsigned char)'\x00';
                    local_sp_9 = local_sp_8;
                    var_71 = *var_59;
                    var_72 = *var_63;
                    var_73 = helper_cc_compute_c_wrapper(var_71 - var_72, var_72, var_2, 17U);
                    continue;
                }
                var_87 = *var_57;
            }
            local_sp_7 = local_sp_6;
            if (var_87 == 2U) {
                *var_57 = 1U;
                local_sp_8 = local_sp_7;
            } else {
                if (*var_66 == '\x00') {
                    *var_57 = 1U;
                    local_sp_8 = local_sp_7;
                } else {
                    var_88 = *var_67;
                    var_89 = local_sp_6 + (-8L);
                    *(uint64_t *)var_89 = 4225894UL;
                    indirect_placeholder();
                    local_sp_7 = var_89;
                    local_sp_8 = var_89;
                    if (var_88 == 0U) {
                        *var_57 = 1U;
                        local_sp_8 = local_sp_7;
                    }
                }
            }
            *var_59 = (*var_70 + *var_59);
            *var_65 = (unsigned char)'\x00';
            local_sp_9 = local_sp_8;
            var_71 = *var_59;
            var_72 = *var_63;
            var_73 = helper_cc_compute_c_wrapper(var_71 - var_72, var_72, var_2, 17U);
        }
    if (*var_57 == 2U) {
        **(unsigned char **)var_68 = (unsigned char)'\x00';
    }
    return *var_10;
}

typedef unsigned char __u_char;
typedef unsigned short __u_short;
typedef unsigned int __u_int;
typedef unsigned long __u_long;
typedef signed char __int8_t;
typedef unsigned char __uint8_t;
typedef short __int16_t;
typedef unsigned short __uint16_t;
typedef int __int32_t;
typedef unsigned int __uint32_t;
typedef long __int64_t;
typedef unsigned long __uint64_t;
typedef __int8_t __int_least8_t;
typedef __uint8_t __uint_least8_t;
typedef __int16_t __int_least16_t;
typedef __uint16_t __uint_least16_t;
typedef __int32_t __int_least32_t;
typedef __uint32_t __uint_least32_t;
typedef __int64_t __int_least64_t;
typedef __uint64_t __uint_least64_t;
typedef long __quad_t;
typedef unsigned long __u_quad_t;
typedef long __intmax_t;
typedef unsigned long __uintmax_t;
typedef unsigned long __dev_t;
typedef unsigned int __uid_t;
typedef unsigned int __gid_t;
typedef unsigned long __ino_t;
typedef unsigned long __ino64_t;
typedef unsigned int __mode_t;
typedef unsigned long __nlink_t;
typedef long __off_t;
typedef long __off64_t;
typedef int __pid_t;
typedef struct {
    int __val[2];
} __fsid_t;
typedef long __clock_t;
typedef unsigned long __rlim_t;
typedef unsigned long __rlim64_t;
typedef unsigned int __id_t;
typedef long __time_t;
typedef unsigned int __useconds_t;
typedef long __suseconds_t;
typedef int __daddr_t;
typedef int __key_t;
typedef int __clockid_t;
typedef void *__timer_t;
typedef long __blksize_t;
typedef long __blkcnt_t;
typedef long __blkcnt64_t;
typedef unsigned long __fsblkcnt_t;
typedef unsigned long __fsblkcnt64_t;
typedef unsigned long __fsfilcnt_t;
typedef unsigned long __fsfilcnt64_t;
typedef long __fsword_t;
typedef long __ssize_t;
typedef long __syscall_slong_t;
typedef unsigned long __syscall_ulong_t;
typedef __off64_t __loff_t;
typedef char *__caddr_t;
typedef long __intptr_t;
typedef unsigned int __socklen_t;
typedef int __sig_atomic_t;
typedef __int8_t int8_t;
typedef __int16_t int16_t;
typedef __int32_t int32_t;
typedef __int64_t int64_t;
typedef __uint8_t uint8_t;
typedef __uint16_t uint16_t;
typedef __uint32_t uint32_t;
typedef __uint64_t uint64_t;
typedef __int_least8_t int_least8_t;
typedef __int_least16_t int_least16_t;
typedef __int_least32_t int_least32_t;
typedef __int_least64_t int_least64_t;
typedef __uint_least8_t uint_least8_t;
typedef __uint_least16_t uint_least16_t;
typedef __uint_least32_t uint_least32_t;
typedef __uint_least64_t uint_least64_t;
typedef signed char int_fast8_t;
typedef long int_fast16_t;
typedef long int_fast32_t;
typedef long int_fast64_t;
typedef unsigned char uint_fast8_t;
typedef unsigned long uint_fast16_t;
typedef unsigned long uint_fast32_t;
typedef unsigned long uint_fast64_t;
typedef long intptr_t;
typedef unsigned long uintptr_t;
typedef __intmax_t intmax_t;
typedef __uintmax_t uintmax_t;
struct helper_divq_EAX_wrapper_ret_type;
struct type_5;
struct helper_idivq_EAX_wrapper_ret_type;
struct indirect_placeholder_58_ret_type;
struct indirect_placeholder_60_ret_type;
struct indirect_placeholder_61_ret_type;
struct indirect_placeholder_62_ret_type;
struct indirect_placeholder_63_ret_type;
struct indirect_placeholder_64_ret_type;
struct indirect_placeholder_65_ret_type;
struct helper_divq_EAX_wrapper_ret_type {
    uint64_t field_0;
    uint64_t field_1;
    uint32_t field_2;
    uint64_t field_3;
    uint64_t field_4;
    uint64_t field_5;
    uint32_t field_6;
    uint64_t field_7;
    uint32_t field_8;
    uint32_t field_9;
    unsigned char field_10;
    uint32_t field_11;
    uint32_t field_12;
};
struct type_5 {
};
struct helper_idivq_EAX_wrapper_ret_type {
    uint64_t field_0;
    uint64_t field_1;
    uint32_t field_2;
    uint64_t field_3;
    uint64_t field_4;
    uint64_t field_5;
    uint32_t field_6;
    uint64_t field_7;
    uint32_t field_8;
    uint32_t field_9;
    unsigned char field_10;
    uint32_t field_11;
    uint32_t field_12;
};
struct indirect_placeholder_58_ret_type {
    uint64_t field_0;
    uint64_t field_1;
    uint64_t field_2;
};
struct indirect_placeholder_60_ret_type {
    uint64_t field_0;
    uint64_t field_1;
    uint64_t field_2;
};
struct indirect_placeholder_61_ret_type {
    uint64_t field_0;
    uint64_t field_1;
    uint64_t field_2;
};
struct indirect_placeholder_62_ret_type {
    uint64_t field_0;
    uint64_t field_1;
    uint64_t field_2;
};
struct indirect_placeholder_63_ret_type {
    uint64_t field_0;
    uint64_t field_1;
    uint64_t field_2;
};
struct indirect_placeholder_64_ret_type {
    uint64_t field_0;
    uint64_t field_1;
    uint64_t field_2;
};
struct indirect_placeholder_65_ret_type {
    uint64_t field_0;
    uint64_t field_1;
};
extern uint64_t revng_init_local_sp(uint64_t param_0);
extern uint64_t init_rbp(void);
extern uint64_t init_cc_src2(void);
extern uint64_t helper_cc_compute_c_wrapper(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint32_t param_3);
extern void indirect_placeholder(void);
extern uint64_t init_rbx(void);
extern uint64_t init_r10(void);
extern uint64_t init_state_0x9018(void);
extern uint32_t init_state_0x9010(void);
extern uint64_t init_state_0x8408(void);
extern uint64_t init_state_0x8328(void);
extern uint64_t init_state_0x82d8(void);
extern uint32_t init_state_0x9080(void);
extern uint32_t init_state_0x8248(void);
extern struct helper_divq_EAX_wrapper_ret_type helper_divq_EAX_wrapper(struct type_5 *param_0, uint64_t param_1, uint64_t param_2, uint64_t param_3, uint64_t param_4, uint64_t param_5, uint64_t param_6, uint64_t param_7, uint64_t param_8, uint64_t param_9, uint64_t param_10, uint64_t param_11, uint64_t param_12, uint64_t param_13, uint32_t param_14, uint64_t param_15, uint64_t param_16, uint64_t param_17, uint32_t param_18, uint32_t param_19);
extern uint64_t init_r9(void);
extern struct helper_idivq_EAX_wrapper_ret_type helper_idivq_EAX_wrapper(struct type_5 *param_0, uint64_t param_1, uint64_t param_2, uint64_t param_3, uint64_t param_4, uint64_t param_5, uint64_t param_6, uint64_t param_7, uint64_t param_8, uint64_t param_9, uint64_t param_10, uint64_t param_11, uint64_t param_12, uint64_t param_13, uint32_t param_14, uint64_t param_15, uint64_t param_16, uint64_t param_17, uint32_t param_18, uint32_t param_19);
extern void indirect_placeholder_38(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint64_t param_3, uint64_t param_4, uint64_t param_5, uint64_t param_6);
extern struct indirect_placeholder_58_ret_type indirect_placeholder_58(uint64_t param_0, uint64_t param_1);
extern struct indirect_placeholder_60_ret_type indirect_placeholder_60(uint64_t param_0, uint64_t param_1);
extern struct indirect_placeholder_61_ret_type indirect_placeholder_61(uint64_t param_0, uint64_t param_1);
extern uint64_t helper_cc_compute_all_wrapper(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint32_t param_3);
extern struct indirect_placeholder_62_ret_type indirect_placeholder_62(uint64_t param_0, uint64_t param_1);
extern struct indirect_placeholder_63_ret_type indirect_placeholder_63(uint64_t param_0, uint64_t param_1);
extern struct indirect_placeholder_64_ret_type indirect_placeholder_64(uint64_t param_0, uint64_t param_1);
extern struct indirect_placeholder_65_ret_type indirect_placeholder_65(uint64_t param_0);
uint64_t bb_do_ftruncate(uint64_t rdx, uint64_t rcx, uint64_t r8, uint64_t rsi, uint64_t rdi) {
    uint64_t var_0;
    uint64_t var_1;
    uint64_t var_2;
    uint64_t var_3;
    uint64_t var_4;
    uint64_t var_5;
    uint64_t var_6;
    uint32_t var_7;
    uint64_t var_8;
    uint64_t var_9;
    uint64_t var_10;
    uint32_t var_11;
    uint32_t var_12;
    uint64_t var_13;
    uint64_t var_14;
    uint32_t *var_15;
    uint64_t *var_16;
    uint64_t *var_17;
    uint64_t *var_18;
    uint32_t *var_19;
    uint32_t var_20;
    uint64_t var_100;
    struct indirect_placeholder_58_ret_type var_101;
    uint64_t var_102;
    uint64_t var_103;
    uint64_t var_104;
    uint64_t local_sp_3;
    uint64_t var_105;
    uint64_t var_54;
    uint64_t var_55;
    uint64_t *var_56;
    uint64_t var_25;
    uint64_t var_57;
    struct indirect_placeholder_60_ret_type var_58;
    uint64_t var_59;
    uint64_t var_60;
    uint64_t var_61;
    uint64_t *_pre_phi221;
    uint64_t rdi5_1;
    uint64_t rsi4_0;
    uint64_t rdi5_0;
    uint64_t local_sp_0;
    uint64_t rsi4_2;
    uint64_t var_62;
    uint64_t var_63;
    uint64_t *var_64;
    uint64_t rax_0;
    uint64_t rdi5_2;
    uint64_t local_sp_2;
    struct indirect_placeholder_61_ret_type var_26;
    uint64_t var_27;
    uint64_t var_28;
    uint64_t var_29;
    uint64_t var_30;
    unsigned char var_31;
    uint64_t rsi4_1;
    uint64_t local_sp_1;
    uint64_t state_0x9018_0;
    uint64_t *var_32;
    uint64_t var_33;
    uint64_t storemerge;
    uint64_t var_34;
    uint64_t *var_35;
    struct helper_idivq_EAX_wrapper_ret_type var_36;
    uint64_t var_38;
    uint64_t var_39;
    struct helper_idivq_EAX_wrapper_ret_type var_37;
    uint32_t var_40;
    uint32_t var_41;
    uint64_t var_42;
    uint32_t var_43;
    uint64_t var_44;
    uint64_t var_45;
    struct indirect_placeholder_62_ret_type var_46;
    uint64_t var_47;
    uint64_t var_48;
    uint64_t var_49;
    uint32_t state_0x9010_0;
    uint64_t state_0x82d8_0;
    uint32_t state_0x9080_0;
    uint32_t state_0x8248_0;
    uint64_t var_50;
    uint64_t var_82;
    uint64_t var_83;
    uint64_t var_84;
    uint64_t var_85;
    uint64_t *var_86;
    uint64_t *_pre_phi219;
    uint64_t var_78;
    uint64_t var_79;
    uint64_t var_80;
    uint64_t *var_81;
    uint64_t var_74;
    struct helper_divq_EAX_wrapper_ret_type var_75;
    uint64_t var_76;
    uint64_t *var_77;
    uint64_t var_65;
    struct helper_divq_EAX_wrapper_ret_type var_66;
    uint64_t var_67;
    uint64_t var_68;
    struct indirect_placeholder_63_ret_type var_69;
    uint64_t var_70;
    uint64_t var_71;
    uint64_t var_72;
    uint64_t *var_73;
    uint64_t var_87;
    uint64_t var_88;
    uint64_t var_89;
    uint64_t var_92;
    struct indirect_placeholder_64_ret_type var_93;
    uint64_t var_94;
    uint64_t var_95;
    uint64_t var_96;
    uint64_t var_90;
    uint64_t *var_91;
    uint64_t var_51;
    uint64_t var_52;
    struct indirect_placeholder_65_ret_type var_53;
    uint64_t var_97;
    uint64_t *var_98;
    uint32_t var_99;
    uint64_t var_21;
    uint32_t var_22;
    uint64_t var_23;
    uint64_t var_24;
    var_0 = revng_init_local_sp(0UL);
    var_1 = init_rbp();
    var_2 = init_cc_src2();
    var_3 = init_r9();
    var_4 = init_r10();
    var_5 = init_rbx();
    var_6 = init_state_0x9018();
    var_7 = init_state_0x9010();
    var_8 = init_state_0x8408();
    var_9 = init_state_0x8328();
    var_10 = init_state_0x82d8();
    var_11 = init_state_0x9080();
    var_12 = init_state_0x8248();
    var_13 = var_0 + (-8L);
    *(uint64_t *)var_13 = var_1;
    *(uint64_t *)(var_0 + (-16L)) = var_5;
    var_14 = var_0 + (-248L);
    var_15 = (uint32_t *)(var_0 + (-220L));
    *var_15 = (uint32_t)rdi;
    var_16 = (uint64_t *)(var_0 + (-232L));
    *var_16 = rsi;
    var_17 = (uint64_t *)(var_0 + (-240L));
    *var_17 = rdx;
    var_18 = (uint64_t *)var_14;
    *var_18 = rcx;
    var_19 = (uint32_t *)(var_0 + (-224L));
    var_20 = (uint32_t)r8;
    *var_19 = var_20;
    rdi5_1 = rdi;
    rsi4_0 = 0UL;
    rax_0 = 0UL;
    var_31 = (unsigned char)'\x00';
    rsi4_1 = rsi;
    local_sp_1 = var_14;
    state_0x9018_0 = var_6;
    storemerge = 512UL;
    state_0x9010_0 = var_7;
    state_0x82d8_0 = var_10;
    state_0x9080_0 = var_11;
    state_0x8248_0 = var_12;
    if (*(unsigned char *)6382929UL == '\x00') {
        if (var_20 != 0U & (long)*var_18 <= (long)18446744073709551615UL) {
            var_21 = var_0 + (-216L);
            var_22 = *var_15;
            var_23 = (uint64_t)var_22;
            var_24 = var_0 + (-256L);
            *(uint64_t *)var_24 = 4202158UL;
            indirect_placeholder();
            rsi4_1 = var_21;
            rdi5_1 = var_23;
            local_sp_1 = var_24;
            if (var_22 != 0U) {
                var_25 = *var_16;
                *(uint64_t *)(var_0 + (-264L)) = 4202182UL;
                var_26 = indirect_placeholder_61(var_25, 4UL);
                var_27 = var_26.field_0;
                var_28 = var_26.field_1;
                var_29 = var_26.field_2;
                *(uint64_t *)(var_0 + (-272L)) = 4202190UL;
                indirect_placeholder();
                var_30 = (uint64_t)*(uint32_t *)var_27;
                *(uint64_t *)(var_0 + (-280L)) = 4202217UL;
                indirect_placeholder_38(0UL, 4271520UL, var_27, var_28, var_29, var_30, 0UL);
                return rax_0;
            }
            var_31 = *(unsigned char *)6382929UL;
        }
    } else {
        var_21 = var_0 + (-216L);
        var_22 = *var_15;
        var_23 = (uint64_t)var_22;
        var_24 = var_0 + (-256L);
        *(uint64_t *)var_24 = 4202158UL;
        indirect_placeholder();
        rsi4_1 = var_21;
        rdi5_1 = var_23;
        local_sp_1 = var_24;
        if (var_22 != 0U) {
            var_25 = *var_16;
            *(uint64_t *)(var_0 + (-264L)) = 4202182UL;
            var_26 = indirect_placeholder_61(var_25, 4UL);
            var_27 = var_26.field_0;
            var_28 = var_26.field_1;
            var_29 = var_26.field_2;
            *(uint64_t *)(var_0 + (-272L)) = 4202190UL;
            indirect_placeholder();
            var_30 = (uint64_t)*(uint32_t *)var_27;
            *(uint64_t *)(var_0 + (-280L)) = 4202217UL;
            indirect_placeholder_38(0UL, 4271520UL, var_27, var_28, var_29, var_30, 0UL);
            return rax_0;
        }
        var_31 = *(unsigned char *)6382929UL;
    }
    local_sp_3 = local_sp_1;
    rsi4_2 = rsi4_1;
    rax_0 = 1UL;
    rdi5_2 = rdi5_1;
    local_sp_2 = local_sp_1;
    if (var_31 != '\x00') {
        var_32 = (uint64_t *)(var_0 + (-160L));
        var_33 = helper_cc_compute_all_wrapper(*var_32, 0UL, 0UL, 25U);
        if ((uint64_t)(((unsigned char)(var_33 >> 4UL) ^ (unsigned char)var_33) & '\xc0') == 0UL) {
            var_34 = *var_32;
            storemerge = (var_34 > 2305843009213693952UL) ? 512UL : var_34;
        }
        var_35 = (uint64_t *)(var_0 + (-56L));
        *var_35 = storemerge;
        var_36 = helper_idivq_EAX_wrapper((struct type_5 *)(0UL), storemerge, 9223372036854775808UL, var_13, 4202309UL, 18446744073709551615UL, rcx, var_3, r8, rsi4_1, rdi5_1, var_4, var_5, var_6, var_7, var_8, var_9, var_10, var_11, var_12);
        if ((long)var_36.field_0 <= (long)*var_17) {
            var_45 = *var_16;
            *(uint64_t *)(local_sp_1 + (-8L)) = 4202367UL;
            var_46 = indirect_placeholder_62(var_45, 4UL);
            var_47 = var_46.field_0;
            var_48 = *var_35;
            var_49 = *var_17;
            *(uint64_t *)(local_sp_1 + (-16L)) = 4202415UL;
            indirect_placeholder_38(0UL, 4271536UL, var_49, var_47, var_48, 0UL, 0UL);
            return rax_0;
        }
        var_37 = helper_idivq_EAX_wrapper((struct type_5 *)(0UL), *var_35, 9223372036854775807UL, var_13, 4202334UL, 0UL, rcx, var_3, r8, rsi4_1, rdi5_1, var_4, var_5, var_36.field_5, var_36.field_6, var_8, var_9, var_36.field_7, var_36.field_8, var_36.field_9);
        var_38 = var_37.field_0;
        var_39 = *var_17;
        if ((long)var_38 >= (long)var_39) {
            var_45 = *var_16;
            *(uint64_t *)(local_sp_1 + (-8L)) = 4202367UL;
            var_46 = indirect_placeholder_62(var_45, 4UL);
            var_47 = var_46.field_0;
            var_48 = *var_35;
            var_49 = *var_17;
            *(uint64_t *)(local_sp_1 + (-16L)) = 4202415UL;
            indirect_placeholder_38(0UL, 4271536UL, var_49, var_47, var_48, 0UL, 0UL);
            return rax_0;
        }
        var_40 = var_37.field_9;
        var_41 = var_37.field_8;
        var_42 = var_37.field_7;
        var_43 = var_37.field_6;
        var_44 = var_37.field_5;
        *var_17 = (*var_35 * var_39);
        state_0x9018_0 = var_44;
        state_0x9010_0 = var_43;
        state_0x82d8_0 = var_42;
        state_0x9080_0 = var_41;
        state_0x8248_0 = var_40;
    }
    if (*var_19 == 0U) {
        var_97 = *var_17;
        var_98 = (uint64_t *)(var_0 + (-32L));
        *var_98 = var_97;
        _pre_phi219 = var_98;
    } else {
        var_50 = *var_18;
        if ((long)var_50 < (long)0UL) {
            var_51 = var_0 + (-216L);
            var_52 = local_sp_1 + (-8L);
            *(uint64_t *)var_52 = 4202498UL;
            var_53 = indirect_placeholder_65(var_51);
            local_sp_0 = var_52;
            if ((uint64_t)(unsigned char)var_53.field_0 != 0UL) {
                var_54 = var_53.field_1;
                var_55 = *(uint64_t *)(var_0 + (-168L));
                var_56 = (uint64_t *)(var_0 + (-48L));
                *var_56 = var_55;
                _pre_phi221 = var_56;
                rsi4_0 = rsi4_1;
                rdi5_0 = var_54;
                if ((long)var_55 <= (long)18446744073709551615UL) {
                    var_57 = *var_16;
                    *(uint64_t *)(local_sp_1 + (-16L)) = 4202544UL;
                    var_58 = indirect_placeholder_60(var_57, 4UL);
                    var_59 = var_58.field_0;
                    var_60 = var_58.field_1;
                    var_61 = var_58.field_2;
                    *(uint64_t *)(local_sp_1 + (-24L)) = 4202572UL;
                    indirect_placeholder_38(0UL, 4271584UL, var_59, var_60, var_61, 0UL, 0UL);
                    return rax_0;
                }
            }
            var_62 = (uint64_t)*var_15;
            var_63 = local_sp_1 + (-16L);
            *(uint64_t *)var_63 = 4202605UL;
            indirect_placeholder();
            var_64 = (uint64_t *)(var_0 + (-48L));
            *var_64 = var_62;
            _pre_phi221 = var_64;
            rdi5_0 = var_62;
            local_sp_0 = var_63;
        } else {
            *(uint64_t *)(var_0 + (-40L)) = var_50;
            local_sp_3 = local_sp_2;
            switch (*var_19) {
              case 5U:
                {
                    var_65 = *var_17;
                    var_66 = helper_divq_EAX_wrapper((struct type_5 *)(0UL), var_65, (*(uint64_t *)(var_0 + (-40L)) + var_65) + (-1L), var_13, 4202855UL, 0UL, rcx, var_3, r8, rsi4_2, rdi5_2, var_4, var_65, state_0x9018_0, state_0x9010_0, var_8, var_9, state_0x82d8_0, state_0x9080_0, state_0x8248_0);
                    var_67 = var_66.field_0 * *var_17;
                    *(uint64_t *)(var_0 + (-64L)) = var_67;
                    if ((long)var_67 <= (long)18446744073709551615UL) {
                        var_68 = *var_16;
                        *(uint64_t *)(local_sp_2 + (-8L)) = 4202905UL;
                        var_69 = indirect_placeholder_63(var_68, 4UL);
                        var_70 = var_69.field_0;
                        var_71 = var_69.field_1;
                        var_72 = var_69.field_2;
                        *(uint64_t *)(local_sp_2 + (-16L)) = 4202933UL;
                        indirect_placeholder_38(0UL, 4271656UL, var_70, var_71, var_72, 0UL, 0UL);
                        return rax_0;
                    }
                    var_73 = (uint64_t *)(var_0 + (-32L));
                    *var_73 = var_67;
                    _pre_phi219 = var_73;
                }
                break;
              case 4U:
              case 3U:
              case 2U:
                {
                    switch (*var_19) {
                      case 4U:
                        {
                            var_74 = *var_17;
                            var_75 = helper_divq_EAX_wrapper((struct type_5 *)(0UL), var_74, *(uint64_t *)(var_0 + (-40L)), var_13, 4202786UL, 0UL, rcx, var_3, r8, rsi4_2, rdi5_2, var_4, var_74, state_0x9018_0, state_0x9010_0, var_8, var_9, state_0x82d8_0, state_0x9080_0, state_0x8248_0);
                            var_76 = var_75.field_0 * *var_17;
                            var_77 = (uint64_t *)(var_0 + (-32L));
                            *var_77 = var_76;
                            _pre_phi219 = var_77;
                        }
                        break;
                      case 3U:
                        {
                            var_78 = *var_17;
                            var_79 = *(uint64_t *)(var_0 + (-40L));
                            var_80 = (var_78 > var_79) ? var_79 : var_78;
                            var_81 = (uint64_t *)(var_0 + (-32L));
                            *var_81 = var_80;
                            _pre_phi219 = var_81;
                        }
                        break;
                      case 2U:
                        {
                            var_82 = *var_17;
                            var_83 = *(uint64_t *)(var_0 + (-40L));
                            var_84 = helper_cc_compute_c_wrapper(var_82 - var_83, var_83, var_2, 17U);
                            var_85 = (var_84 == 0UL) ? var_82 : var_83;
                            var_86 = (uint64_t *)(var_0 + (-32L));
                            *var_86 = var_85;
                            _pre_phi219 = var_86;
                        }
                        break;
                    }
                }
                break;
              default:
                {
                    var_87 = *(uint64_t *)(var_0 + (-40L));
                    var_88 = 9223372036854775807UL - var_87;
                    var_89 = *var_17;
                    if ((long)var_88 >= (long)var_89) {
                        var_92 = *var_16;
                        *(uint64_t *)(local_sp_2 + (-8L)) = 4203002UL;
                        var_93 = indirect_placeholder_64(var_92, 4UL);
                        var_94 = var_93.field_0;
                        var_95 = var_93.field_1;
                        var_96 = var_93.field_2;
                        *(uint64_t *)(local_sp_2 + (-16L)) = 4203030UL;
                        indirect_placeholder_38(0UL, 4271696UL, var_94, var_95, var_96, 0UL, 0UL);
                        return rax_0;
                    }
                    var_90 = var_87 + var_89;
                    var_91 = (uint64_t *)(var_0 + (-32L));
                    *var_91 = var_90;
                    _pre_phi219 = var_91;
                }
                break;
            }
        }
    }
}

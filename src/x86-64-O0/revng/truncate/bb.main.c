typedef unsigned char __u_char;
typedef unsigned short __u_short;
typedef unsigned int __u_int;
typedef unsigned long __u_long;
typedef signed char __int8_t;
typedef unsigned char __uint8_t;
typedef short __int16_t;
typedef unsigned short __uint16_t;
typedef int __int32_t;
typedef unsigned int __uint32_t;
typedef long __int64_t;
typedef unsigned long __uint64_t;
typedef __int8_t __int_least8_t;
typedef __uint8_t __uint_least8_t;
typedef __int16_t __int_least16_t;
typedef __uint16_t __uint_least16_t;
typedef __int32_t __int_least32_t;
typedef __uint32_t __uint_least32_t;
typedef __int64_t __int_least64_t;
typedef __uint64_t __uint_least64_t;
typedef long __quad_t;
typedef unsigned long __u_quad_t;
typedef long __intmax_t;
typedef unsigned long __uintmax_t;
typedef unsigned long __dev_t;
typedef unsigned int __uid_t;
typedef unsigned int __gid_t;
typedef unsigned long __ino_t;
typedef unsigned long __ino64_t;
typedef unsigned int __mode_t;
typedef unsigned long __nlink_t;
typedef long __off_t;
typedef long __off64_t;
typedef int __pid_t;
typedef struct {
    int __val[2];
} __fsid_t;
typedef long __clock_t;
typedef unsigned long __rlim_t;
typedef unsigned long __rlim64_t;
typedef unsigned int __id_t;
typedef long __time_t;
typedef unsigned int __useconds_t;
typedef long __suseconds_t;
typedef int __daddr_t;
typedef int __key_t;
typedef int __clockid_t;
typedef void *__timer_t;
typedef long __blksize_t;
typedef long __blkcnt_t;
typedef long __blkcnt64_t;
typedef unsigned long __fsblkcnt_t;
typedef unsigned long __fsblkcnt64_t;
typedef unsigned long __fsfilcnt_t;
typedef unsigned long __fsfilcnt64_t;
typedef long __fsword_t;
typedef long __ssize_t;
typedef long __syscall_slong_t;
typedef unsigned long __syscall_ulong_t;
typedef __off64_t __loff_t;
typedef char *__caddr_t;
typedef long __intptr_t;
typedef unsigned int __socklen_t;
typedef int __sig_atomic_t;
typedef __int8_t int8_t;
typedef __int16_t int16_t;
typedef __int32_t int32_t;
typedef __int64_t int64_t;
typedef __uint8_t uint8_t;
typedef __uint16_t uint16_t;
typedef __uint32_t uint32_t;
typedef __uint64_t uint64_t;
typedef __int_least8_t int_least8_t;
typedef __int_least16_t int_least16_t;
typedef __int_least32_t int_least32_t;
typedef __int_least64_t int_least64_t;
typedef __uint_least8_t uint_least8_t;
typedef __uint_least16_t uint_least16_t;
typedef __uint_least32_t uint_least32_t;
typedef __uint_least64_t uint_least64_t;
typedef signed char int_fast8_t;
typedef long int_fast16_t;
typedef long int_fast32_t;
typedef long int_fast64_t;
typedef unsigned char uint_fast8_t;
typedef unsigned long uint_fast16_t;
typedef unsigned long uint_fast32_t;
typedef unsigned long uint_fast64_t;
typedef long intptr_t;
typedef unsigned long uintptr_t;
typedef __intmax_t intmax_t;
typedef __uintmax_t uintmax_t;
typedef _Bool bool;
struct indirect_placeholder_43_ret_type;
struct indirect_placeholder_45_ret_type;
struct indirect_placeholder_46_ret_type;
struct indirect_placeholder_47_ret_type;
struct indirect_placeholder_51_ret_type;
struct indirect_placeholder_50_ret_type;
struct indirect_placeholder_52_ret_type;
struct indirect_placeholder_49_ret_type;
struct indirect_placeholder_53_ret_type;
struct indirect_placeholder_48_ret_type;
struct indirect_placeholder_54_ret_type;
struct indirect_placeholder_56_ret_type;
struct indirect_placeholder_43_ret_type {
    uint64_t field_0;
    uint64_t field_1;
    uint64_t field_2;
};
struct indirect_placeholder_45_ret_type {
    uint64_t field_0;
    uint64_t field_1;
    uint64_t field_2;
};
struct indirect_placeholder_46_ret_type {
    uint64_t field_0;
    uint64_t field_1;
    uint64_t field_2;
};
struct indirect_placeholder_47_ret_type {
    uint64_t field_0;
    uint64_t field_1;
};
struct indirect_placeholder_51_ret_type {
    uint64_t field_0;
    uint64_t field_1;
};
struct indirect_placeholder_50_ret_type {
    uint64_t field_0;
    uint64_t field_1;
};
struct indirect_placeholder_52_ret_type {
    uint64_t field_0;
    uint64_t field_1;
};
struct indirect_placeholder_49_ret_type {
    uint64_t field_0;
    uint64_t field_1;
};
struct indirect_placeholder_53_ret_type {
    uint64_t field_0;
    uint64_t field_1;
};
struct indirect_placeholder_48_ret_type {
    uint64_t field_0;
    uint64_t field_1;
};
struct indirect_placeholder_54_ret_type {
    uint64_t field_0;
    uint64_t field_1;
    uint64_t field_2;
    uint64_t field_3;
};
struct indirect_placeholder_56_ret_type {
    uint64_t field_0;
    uint64_t field_1;
    uint64_t field_2;
    uint64_t field_3;
};
extern void abort(void);
extern uint64_t revng_init_local_sp(uint64_t param_0);
extern uint64_t init_rbp(void);
extern void indirect_placeholder(void);
extern uint64_t indirect_placeholder_41(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint64_t param_3, uint64_t param_4);
extern void indirect_placeholder_14(uint64_t param_0, uint64_t param_1);
extern uint64_t init_rbx(void);
extern uint64_t indirect_placeholder_9(uint64_t param_0);
extern void indirect_placeholder_27(uint64_t param_0);
extern void indirect_placeholder_38(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint64_t param_3, uint64_t param_4, uint64_t param_5, uint64_t param_6);
extern struct indirect_placeholder_43_ret_type indirect_placeholder_43(uint64_t param_0, uint64_t param_1);
extern struct indirect_placeholder_45_ret_type indirect_placeholder_45(uint64_t param_0, uint64_t param_1);
extern struct indirect_placeholder_46_ret_type indirect_placeholder_46(uint64_t param_0, uint64_t param_1);
extern struct indirect_placeholder_47_ret_type indirect_placeholder_47(uint64_t param_0);
extern struct indirect_placeholder_51_ret_type indirect_placeholder_51(uint64_t param_0, uint64_t param_1);
extern struct indirect_placeholder_50_ret_type indirect_placeholder_50(uint64_t param_0, uint64_t param_1);
extern struct indirect_placeholder_52_ret_type indirect_placeholder_52(uint64_t param_0, uint64_t param_1);
extern struct indirect_placeholder_49_ret_type indirect_placeholder_49(uint64_t param_0, uint64_t param_1);
extern struct indirect_placeholder_53_ret_type indirect_placeholder_53(uint64_t param_0, uint64_t param_1);
extern struct indirect_placeholder_48_ret_type indirect_placeholder_48(uint64_t param_0, uint64_t param_1);
extern struct indirect_placeholder_54_ret_type indirect_placeholder_54(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint64_t param_3, uint64_t param_4, uint64_t param_5);
extern void indirect_placeholder_55(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint64_t param_3, uint64_t param_4, uint64_t param_5);
extern struct indirect_placeholder_56_ret_type indirect_placeholder_56(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint64_t param_3, uint64_t param_4);
void bb_main(uint64_t rsi, uint64_t rdi) {
    uint64_t var_99;
    uint64_t var_92;
    uint64_t var_38;
    uint64_t var_39;
    struct indirect_placeholder_56_ret_type var_19;
    uint64_t var_107;
    uint64_t var_0;
    uint64_t var_1;
    uint64_t var_2;
    uint64_t var_3;
    uint32_t *var_4;
    uint64_t var_5;
    uint64_t *var_6;
    unsigned char *var_7;
    unsigned char *var_8;
    uint64_t *var_9;
    uint32_t *var_10;
    uint32_t *var_11;
    uint64_t var_12;
    uint64_t var_13;
    uint32_t *var_14;
    uint64_t *var_15;
    uint64_t local_sp_11;
    uint64_t var_110;
    struct indirect_placeholder_43_ret_type var_111;
    uint64_t var_112;
    uint64_t var_113;
    uint64_t var_114;
    uint64_t var_115;
    uint64_t var_116;
    uint64_t local_sp_6_be;
    uint64_t local_sp_6;
    uint64_t var_91;
    uint64_t var_89;
    uint64_t var_90;
    uint64_t local_sp_2;
    uint64_t var_81;
    struct indirect_placeholder_45_ret_type var_93;
    uint64_t var_94;
    uint64_t var_95;
    uint64_t var_96;
    uint64_t var_97;
    uint64_t var_98;
    uint64_t local_sp_3;
    uint64_t local_sp_5;
    uint32_t *var_82;
    uint64_t *var_83;
    uint32_t var_84;
    uint32_t *var_85;
    uint32_t var_86;
    uint64_t var_87;
    uint64_t var_88;
    uint64_t var_71;
    struct indirect_placeholder_46_ret_type var_72;
    uint64_t var_73;
    uint64_t var_74;
    uint64_t var_75;
    uint64_t var_76;
    uint64_t var_77;
    uint64_t local_sp_4;
    uint64_t var_78;
    uint64_t var_79;
    struct indirect_placeholder_47_ret_type var_80;
    uint64_t var_46;
    uint64_t var_47;
    uint64_t var_48;
    uint32_t var_49;
    bool var_50;
    unsigned char var_51;
    struct indirect_placeholder_51_ret_type var_57;
    uint64_t var_58;
    struct indirect_placeholder_50_ret_type var_59;
    uint64_t var_60;
    uint64_t var_61;
    struct indirect_placeholder_52_ret_type var_52;
    uint64_t var_53;
    struct indirect_placeholder_49_ret_type var_54;
    uint64_t var_55;
    uint64_t var_56;
    struct indirect_placeholder_53_ret_type var_62;
    uint64_t var_63;
    struct indirect_placeholder_48_ret_type var_64;
    uint64_t var_65;
    uint64_t var_66;
    uint64_t *var_67;
    uint64_t var_68;
    uint64_t var_69;
    uint64_t var_70;
    uint64_t *var_100;
    uint64_t var_101;
    uint64_t var_102;
    uint64_t var_103;
    uint64_t var_104;
    uint64_t var_105;
    uint64_t var_106;
    uint32_t var_108;
    uint64_t var_109;
    uint64_t var_45;
    uint64_t local_sp_7;
    uint64_t local_sp_11_be;
    uint64_t var_35;
    uint64_t var_36;
    uint64_t var_37;
    struct indirect_placeholder_54_ret_type var_40;
    uint64_t var_41;
    uint64_t var_42;
    uint64_t var_43;
    uint64_t var_44;
    uint64_t local_sp_8;
    unsigned char var_28;
    uint32_t var_29;
    uint64_t var_30;
    uint64_t var_31;
    uint64_t var_32;
    uint64_t var_33;
    uint64_t var_34;
    uint64_t local_sp_10;
    uint64_t local_sp_9;
    uint64_t var_21;
    uint64_t var_22;
    uint64_t var_23;
    uint64_t var_24;
    uint64_t var_25;
    uint64_t var_26;
    uint64_t var_27;
    uint64_t var_16;
    uint64_t var_17;
    uint64_t var_18;
    uint32_t var_20;
    unsigned int loop_state_var;
    var_0 = revng_init_local_sp(0UL);
    var_1 = init_rbp();
    var_2 = init_rbx();
    var_3 = var_0 + (-8L);
    *(uint64_t *)var_3 = var_1;
    *(uint64_t *)(var_0 + (-16L)) = var_2;
    var_4 = (uint32_t *)(var_0 + (-252L));
    *var_4 = (uint32_t)rdi;
    var_5 = var_0 + (-264L);
    var_6 = (uint64_t *)var_5;
    *var_6 = rsi;
    var_7 = (unsigned char *)(var_0 + (-25L));
    *var_7 = (unsigned char)'\x00';
    var_8 = (unsigned char *)(var_0 + (-26L));
    *var_8 = (unsigned char)'\x00';
    var_9 = (uint64_t *)(var_0 + (-48L));
    *var_9 = 18446744073709551615UL;
    var_10 = (uint32_t *)(var_0 + (-52L));
    *var_10 = 0U;
    var_11 = (uint32_t *)(var_0 + (-68L));
    *var_11 = 4294967295U;
    var_12 = **(uint64_t **)var_5;
    *(uint64_t *)(var_0 + (-272L)) = 4203270UL;
    indirect_placeholder_27(var_12);
    *(uint64_t *)(var_0 + (-280L)) = 4203285UL;
    indirect_placeholder();
    var_13 = var_0 + (-288L);
    *(uint64_t *)var_13 = 4203295UL;
    indirect_placeholder();
    var_14 = (uint32_t *)(var_0 + (-72L));
    var_15 = (uint64_t *)(var_0 + (-40L));
    local_sp_11 = var_13;
    while (1U)
        {
            var_16 = *var_6;
            var_17 = (uint64_t)*var_4;
            var_18 = local_sp_11 + (-8L);
            *(uint64_t *)var_18 = 4203958UL;
            var_19 = indirect_placeholder_56(4271874UL, 4270400UL, 0UL, var_16, var_17);
            var_20 = (uint32_t)var_19.field_0;
            *var_14 = var_20;
            local_sp_5 = var_18;
            local_sp_11_be = var_18;
            local_sp_9 = var_18;
            local_sp_10 = var_18;
            if (var_20 != 4294967295U) {
                var_46 = var_19.field_1;
                var_47 = var_19.field_2;
                var_48 = var_19.field_3;
                *var_6 = (*var_6 + ((uint64_t)*(uint32_t *)6382712UL << 3UL));
                var_49 = *var_4 - *(uint32_t *)6382712UL;
                *var_4 = var_49;
                var_50 = (*(uint64_t *)6382936UL == 0UL);
                var_51 = *var_7;
                if (!var_50) {
                    if (var_51 != '\x01') {
                        loop_state_var = 0U;
                        break;
                    }
                    *(uint64_t *)(local_sp_11 + (-16L)) = 4204040UL;
                    var_57 = indirect_placeholder_51(4271881UL, 1UL);
                    var_58 = var_57.field_0;
                    *(uint64_t *)(local_sp_11 + (-24L)) = 4204058UL;
                    var_59 = indirect_placeholder_50(4271893UL, 0UL);
                    var_60 = var_59.field_0;
                    var_61 = var_59.field_1;
                    *(uint64_t *)(local_sp_11 + (-32L)) = 4204089UL;
                    indirect_placeholder_38(0UL, 4271904UL, var_60, var_61, var_58, 0UL, 0UL);
                    *(uint64_t *)(local_sp_11 + (-40L)) = 4204099UL;
                    indirect_placeholder_14(var_3, 1UL);
                    abort();
                }
                if (var_51 != '\x00') {
                    loop_state_var = 0U;
                    break;
                }
                if (*var_10 != 0U) {
                    loop_state_var = 0U;
                    break;
                }
                *(uint64_t *)(local_sp_11 + (-16L)) = 4204138UL;
                var_52 = indirect_placeholder_52(4271881UL, 1UL);
                var_53 = var_52.field_0;
                *(uint64_t *)(local_sp_11 + (-24L)) = 4204156UL;
                var_54 = indirect_placeholder_49(4271893UL, 0UL);
                var_55 = var_54.field_0;
                var_56 = var_54.field_1;
                *(uint64_t *)(local_sp_11 + (-32L)) = 4204187UL;
                indirect_placeholder_38(0UL, 4271944UL, var_55, var_56, var_53, 0UL, 0UL);
                *(uint64_t *)(local_sp_11 + (-40L)) = 4204197UL;
                indirect_placeholder_14(var_3, 1UL);
                abort();
            }
            if ((uint64_t)(var_20 + (-99)) == 0UL) {
                *(unsigned char *)6382928UL = (unsigned char)'\x01';
            } else {
                if ((int)var_20 <= (int)99U) {
                    if ((uint64_t)(var_20 + 131U) != 0UL) {
                        if ((uint64_t)(var_20 + 130U) != 0UL) {
                            loop_state_var = 1U;
                            break;
                        }
                        *(uint64_t *)(local_sp_11 + (-16L)) = 4203850UL;
                        indirect_placeholder_14(var_3, 0UL);
                        abort();
                    }
                    var_21 = *(uint64_t *)6382560UL;
                    *(uint64_t *)(local_sp_11 + (-16L)) = 4203902UL;
                    indirect_placeholder_55(0UL, 4270080UL, var_21, 0UL, 4271860UL, 4271511UL);
                    var_22 = local_sp_11 + (-24L);
                    *(uint64_t *)var_22 = 4203912UL;
                    indirect_placeholder();
                    local_sp_9 = var_22;
                    loop_state_var = 1U;
                    break;
                }
                if ((uint64_t)(var_20 + (-114)) == 0UL) {
                    *(uint64_t *)6382936UL = *(uint64_t *)6383384UL;
                } else {
                    if ((uint64_t)(var_20 + (-115)) == 0UL) {
                        if ((uint64_t)(var_20 + (-111)) != 0UL) {
                            loop_state_var = 1U;
                            break;
                        }
                        *(unsigned char *)6382929UL = (unsigned char)'\x01';
                    } else {
                        var_23 = (uint64_t)(uint32_t)(uint64_t)**(unsigned char **)6383384UL;
                        *(uint64_t *)(local_sp_10 + (-8L)) = 4203441UL;
                        var_24 = indirect_placeholder_9(var_23);
                        var_25 = (uint64_t)(unsigned char)var_24;
                        var_26 = local_sp_10 + (-16L);
                        *(uint64_t *)var_26 = 4203451UL;
                        var_27 = indirect_placeholder_9(var_25);
                        local_sp_8 = var_26;
                        local_sp_10 = var_26;
                        while ((uint64_t)(uint32_t)var_27 != 0UL)
                            {
                                *(uint64_t *)6383384UL = (*(uint64_t *)6383384UL + 1UL);
                                var_23 = (uint64_t)(uint32_t)(uint64_t)**(unsigned char **)6383384UL;
                                *(uint64_t *)(local_sp_10 + (-8L)) = 4203441UL;
                                var_24 = indirect_placeholder_9(var_23);
                                var_25 = (uint64_t)(unsigned char)var_24;
                                var_26 = local_sp_10 + (-16L);
                                *(uint64_t *)var_26 = 4203451UL;
                                var_27 = indirect_placeholder_9(var_25);
                                local_sp_8 = var_26;
                                local_sp_10 = var_26;
                            }
                        var_28 = **(unsigned char **)6383384UL;
                        var_29 = (uint32_t)(uint64_t)var_28;
                        if ((uint64_t)(var_29 + (-47)) == 0UL) {
                            *var_10 = 4U;
                            *(uint64_t *)6383384UL = (*(uint64_t *)6383384UL + 1UL);
                        } else {
                            if ((signed char)var_28 > '/') {
                                if ((uint64_t)(var_29 + (-60)) == 0UL) {
                                    *var_10 = 3U;
                                    *(uint64_t *)6383384UL = (*(uint64_t *)6383384UL + 1UL);
                                } else {
                                    if ((uint64_t)(var_29 + (-62)) == 0UL) {
                                        *var_10 = 2U;
                                        *(uint64_t *)6383384UL = (*(uint64_t *)6383384UL + 1UL);
                                    }
                                }
                            } else {
                                if ((uint64_t)(var_29 + (-37)) == 0UL) {
                                    *var_10 = 5U;
                                    *(uint64_t *)6383384UL = (*(uint64_t *)6383384UL + 1UL);
                                }
                            }
                        }
                        var_30 = (uint64_t)(uint32_t)(uint64_t)**(unsigned char **)6383384UL;
                        *(uint64_t *)(local_sp_8 + (-8L)) = 4203644UL;
                        var_31 = indirect_placeholder_9(var_30);
                        var_32 = (uint64_t)(unsigned char)var_31;
                        var_33 = local_sp_8 + (-16L);
                        *(uint64_t *)var_33 = 4203654UL;
                        var_34 = indirect_placeholder_9(var_32);
                        local_sp_8 = var_33;
                        while ((uint64_t)(uint32_t)var_34 != 0UL)
                            {
                                *(uint64_t *)6383384UL = (*(uint64_t *)6383384UL + 1UL);
                                var_30 = (uint64_t)(uint32_t)(uint64_t)**(unsigned char **)6383384UL;
                                *(uint64_t *)(local_sp_8 + (-8L)) = 4203644UL;
                                var_31 = indirect_placeholder_9(var_30);
                                var_32 = (uint64_t)(unsigned char)var_31;
                                var_33 = local_sp_8 + (-16L);
                                *(uint64_t *)var_33 = 4203654UL;
                                var_34 = indirect_placeholder_9(var_32);
                                local_sp_8 = var_33;
                            }
                        if (*var_10 != 0U) {
                            var_35 = var_19.field_1;
                            var_36 = var_19.field_2;
                            var_37 = var_19.field_3;
                            *(uint64_t *)(local_sp_8 + (-24L)) = 4203717UL;
                            indirect_placeholder_38(0UL, 4271776UL, var_35, var_36, var_37, 0UL, 0UL);
                            *(uint64_t *)(local_sp_8 + (-32L)) = 4203727UL;
                            indirect_placeholder_14(var_3, 1UL);
                            abort();
                        }
                        *var_10 = 1U;
                    }
                }
            }
        }
    switch (loop_state_var) {
      case 1U:
        {
            *(uint64_t *)(local_sp_9 + (-8L)) = 4203922UL;
            indirect_placeholder_14(var_3, 1UL);
            abort();
        }
        break;
      case 0U:
        {
            if (*(unsigned char *)6382929UL != '\x00') {
                if (var_51 == '\x01') {
                    *(uint64_t *)(local_sp_11 + (-16L)) = 4204234UL;
                    var_62 = indirect_placeholder_53(4271893UL, 1UL);
                    var_63 = var_62.field_0;
                    *(uint64_t *)(local_sp_11 + (-24L)) = 4204252UL;
                    var_64 = indirect_placeholder_48(4271983UL, 0UL);
                    var_65 = var_64.field_0;
                    var_66 = var_64.field_1;
                    *(uint64_t *)(local_sp_11 + (-32L)) = 4204283UL;
                    indirect_placeholder_38(0UL, 4272000UL, var_65, var_66, var_63, 0UL, 0UL);
                    *(uint64_t *)(local_sp_11 + (-40L)) = 4204293UL;
                    indirect_placeholder_14(var_3, 1UL);
                    abort();
                }
            }
            if ((int)var_49 > (int)0U) {
                *(uint64_t *)(local_sp_11 + (-16L)) = 4204327UL;
                indirect_placeholder_38(0UL, 4272032UL, var_46, var_47, var_48, 0UL, 0UL);
                *(uint64_t *)(local_sp_11 + (-24L)) = 4204337UL;
                indirect_placeholder_14(var_3, 1UL);
                abort();
            }
            if (!var_50) {
                var_67 = (uint64_t *)(var_0 + (-64L));
                *var_67 = 18446744073709551615UL;
                var_68 = *(uint64_t *)6382936UL;
                var_69 = local_sp_11 + (-16L);
                *(uint64_t *)var_69 = 4204386UL;
                var_70 = indirect_placeholder_9(var_68);
                local_sp_4 = var_69;
                if ((uint64_t)(uint32_t)var_70 == 0UL) {
                    var_71 = *(uint64_t *)6382936UL;
                    *(uint64_t *)(local_sp_11 + (-24L)) = 4204410UL;
                    var_72 = indirect_placeholder_46(var_71, 4UL);
                    var_73 = var_72.field_0;
                    var_74 = var_72.field_1;
                    var_75 = var_72.field_2;
                    *(uint64_t *)(local_sp_11 + (-32L)) = 4204418UL;
                    indirect_placeholder();
                    var_76 = (uint64_t)*(uint32_t *)var_73;
                    var_77 = local_sp_11 + (-40L);
                    *(uint64_t *)var_77 = 4204445UL;
                    indirect_placeholder_38(0UL, 4272053UL, var_73, var_74, var_75, var_76, 1UL);
                    local_sp_4 = var_77;
                }
                var_78 = var_0 + (-248L);
                var_79 = local_sp_4 + (-8L);
                *(uint64_t *)var_79 = 4204460UL;
                var_80 = indirect_placeholder_47(var_78);
                local_sp_2 = var_79;
                if ((uint64_t)(unsigned char)var_80.field_0 == 0UL) {
                    var_81 = *(uint64_t *)(var_0 + (-200L));
                    *var_67 = var_81;
                    var_91 = var_81;
                } else {
                    *(uint64_t *)(local_sp_4 + (-16L)) = 4204502UL;
                    indirect_placeholder();
                    var_82 = (uint32_t *)(var_0 + (-76L));
                    *var_82 = 0U;
                    *(uint64_t *)(local_sp_4 + (-24L)) = 4204531UL;
                    indirect_placeholder();
                    var_83 = (uint64_t *)(var_0 + (-88L));
                    *var_83 = 0UL;
                    *(uint64_t *)(local_sp_4 + (-32L)) = 4204540UL;
                    indirect_placeholder();
                    var_84 = *(volatile uint32_t *)(uint32_t *)0UL;
                    var_85 = (uint32_t *)(var_0 + (-92L));
                    *var_85 = var_84;
                    var_86 = *var_82;
                    var_87 = local_sp_4 + (-40L);
                    *(uint64_t *)var_87 = 4204555UL;
                    indirect_placeholder();
                    var_88 = *var_83;
                    var_91 = var_88;
                    local_sp_2 = var_87;
                    if ((long)var_88 < (long)0UL) {
                        var_89 = (uint64_t)var_86;
                        var_90 = local_sp_4 + (-48L);
                        *(uint64_t *)var_90 = 4204577UL;
                        indirect_placeholder();
                        *(uint32_t *)var_89 = *var_85;
                        var_91 = *var_67;
                        local_sp_2 = var_90;
                    } else {
                        *var_67 = var_88;
                    }
                }
                var_99 = var_91;
                local_sp_3 = local_sp_2;
                if ((long)var_91 > (long)18446744073709551615UL) {
                    var_92 = *(uint64_t *)6382936UL;
                    *(uint64_t *)(local_sp_2 + (-8L)) = 4204612UL;
                    var_93 = indirect_placeholder_45(var_92, 4UL);
                    var_94 = var_93.field_0;
                    var_95 = var_93.field_1;
                    var_96 = var_93.field_2;
                    *(uint64_t *)(local_sp_2 + (-16L)) = 4204620UL;
                    indirect_placeholder();
                    var_97 = (uint64_t)*(uint32_t *)var_94;
                    var_98 = local_sp_2 + (-24L);
                    *(uint64_t *)var_98 = 4204647UL;
                    indirect_placeholder_38(0UL, 4271626UL, var_94, var_95, var_96, var_97, 1UL);
                    var_99 = *var_67;
                    local_sp_3 = var_98;
                }
                local_sp_5 = local_sp_3;
                if (*var_7 == '\x01') {
                    *var_9 = var_99;
                } else {
                    *var_15 = var_99;
                }
            }
            *(uint32_t *)(var_0 + (-96L)) = ((*(unsigned char *)6382928UL == '\x00') ? 2113U : 2049U);
            var_100 = (uint64_t *)(var_0 + (-104L));
            local_sp_6 = local_sp_5;
            var_101 = *var_6;
            *var_6 = (var_101 + 8UL);
            var_102 = *(uint64_t *)var_101;
            *var_100 = var_102;
            while (var_102 != 0UL)
                {
                    *(uint64_t *)(local_sp_6 + (-8L)) = 4204734UL;
                    indirect_placeholder();
                    *var_11 = 0U;
                    var_103 = (uint64_t)*var_10;
                    var_104 = *var_9;
                    var_105 = *var_15;
                    var_106 = *var_100;
                    *(uint64_t *)(local_sp_6 + (-16L)) = 4204861UL;
                    var_107 = indirect_placeholder_41(var_105, var_104, var_103, var_106, 0UL);
                    *var_8 = ((((uint64_t)(unsigned char)var_107 ^ 1UL) | (uint64_t)*var_8) != 0UL);
                    var_108 = *var_11;
                    var_109 = local_sp_6 + (-24L);
                    *(uint64_t *)var_109 = 4204891UL;
                    indirect_placeholder();
                    local_sp_6_be = var_109;
                    if (var_108 == 0U) {
                        var_110 = *var_100;
                        *(uint64_t *)(local_sp_6 + (-32L)) = 4204912UL;
                        var_111 = indirect_placeholder_43(var_110, 4UL);
                        var_112 = var_111.field_0;
                        var_113 = var_111.field_1;
                        var_114 = var_111.field_2;
                        *(uint64_t *)(local_sp_6 + (-40L)) = 4204920UL;
                        indirect_placeholder();
                        var_115 = (uint64_t)*(uint32_t *)var_112;
                        var_116 = local_sp_6 + (-48L);
                        *(uint64_t *)var_116 = 4204947UL;
                        indirect_placeholder_38(0UL, 4272095UL, var_112, var_113, var_114, var_115, 0UL);
                        *var_8 = (unsigned char)'\x01';
                        local_sp_6_be = var_116;
                    }
                    local_sp_6 = local_sp_6_be;
                    var_101 = *var_6;
                    *var_6 = (var_101 + 8UL);
                    var_102 = *(uint64_t *)var_101;
                    *var_100 = var_102;
                }
            return;
        }
        break;
    }
}

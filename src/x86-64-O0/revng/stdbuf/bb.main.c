typedef unsigned char __u_char;
typedef unsigned short __u_short;
typedef unsigned int __u_int;
typedef unsigned long __u_long;
typedef signed char __int8_t;
typedef unsigned char __uint8_t;
typedef short __int16_t;
typedef unsigned short __uint16_t;
typedef int __int32_t;
typedef unsigned int __uint32_t;
typedef long __int64_t;
typedef unsigned long __uint64_t;
typedef __int8_t __int_least8_t;
typedef __uint8_t __uint_least8_t;
typedef __int16_t __int_least16_t;
typedef __uint16_t __uint_least16_t;
typedef __int32_t __int_least32_t;
typedef __uint32_t __uint_least32_t;
typedef __int64_t __int_least64_t;
typedef __uint64_t __uint_least64_t;
typedef long __quad_t;
typedef unsigned long __u_quad_t;
typedef long __intmax_t;
typedef unsigned long __uintmax_t;
typedef unsigned long __dev_t;
typedef unsigned int __uid_t;
typedef unsigned int __gid_t;
typedef unsigned long __ino_t;
typedef unsigned long __ino64_t;
typedef unsigned int __mode_t;
typedef unsigned long __nlink_t;
typedef long __off_t;
typedef long __off64_t;
typedef int __pid_t;
typedef struct {
    int __val[2];
} __fsid_t;
typedef long __clock_t;
typedef unsigned long __rlim_t;
typedef unsigned long __rlim64_t;
typedef unsigned int __id_t;
typedef long __time_t;
typedef unsigned int __useconds_t;
typedef long __suseconds_t;
typedef int __daddr_t;
typedef int __key_t;
typedef int __clockid_t;
typedef void *__timer_t;
typedef long __blksize_t;
typedef long __blkcnt_t;
typedef long __blkcnt64_t;
typedef unsigned long __fsblkcnt_t;
typedef unsigned long __fsblkcnt64_t;
typedef unsigned long __fsfilcnt_t;
typedef unsigned long __fsfilcnt64_t;
typedef long __fsword_t;
typedef long __ssize_t;
typedef long __syscall_slong_t;
typedef unsigned long __syscall_ulong_t;
typedef __off64_t __loff_t;
typedef char *__caddr_t;
typedef long __intptr_t;
typedef unsigned int __socklen_t;
typedef int __sig_atomic_t;
typedef __int8_t int8_t;
typedef __int16_t int16_t;
typedef __int32_t int32_t;
typedef __int64_t int64_t;
typedef __uint8_t uint8_t;
typedef __uint16_t uint16_t;
typedef __uint32_t uint32_t;
typedef __uint64_t uint64_t;
typedef __int_least8_t int_least8_t;
typedef __int_least16_t int_least16_t;
typedef __int_least32_t int_least32_t;
typedef __int_least64_t int_least64_t;
typedef __uint_least8_t uint_least8_t;
typedef __uint_least16_t uint_least16_t;
typedef __uint_least32_t uint_least32_t;
typedef __uint_least64_t uint_least64_t;
typedef signed char int_fast8_t;
typedef long int_fast16_t;
typedef long int_fast32_t;
typedef long int_fast64_t;
typedef unsigned char uint_fast8_t;
typedef unsigned long uint_fast16_t;
typedef unsigned long uint_fast32_t;
typedef unsigned long uint_fast64_t;
typedef long intptr_t;
typedef unsigned long uintptr_t;
typedef __intmax_t intmax_t;
typedef __uintmax_t uintmax_t;
typedef _Bool bool;
struct indirect_placeholder_54_ret_type;
struct indirect_placeholder_56_ret_type;
struct indirect_placeholder_55_ret_type;
struct indirect_placeholder_57_ret_type;
struct indirect_placeholder_51_ret_type;
struct indirect_placeholder_50_ret_type;
struct indirect_placeholder_49_ret_type;
struct indirect_placeholder_52_ret_type;
struct indirect_placeholder_53_ret_type;
struct indirect_placeholder_59_ret_type;
struct indirect_placeholder_54_ret_type {
    uint64_t field_0;
    uint64_t field_1;
    uint64_t field_2;
    uint64_t field_3;
};
struct indirect_placeholder_56_ret_type {
    uint64_t field_0;
    uint64_t field_1;
    uint64_t field_2;
};
struct indirect_placeholder_55_ret_type {
    uint64_t field_0;
    uint64_t field_1;
};
struct indirect_placeholder_57_ret_type {
    uint64_t field_0;
    uint64_t field_1;
};
struct indirect_placeholder_51_ret_type {
    uint64_t field_0;
    uint64_t field_1;
    uint64_t field_2;
};
struct indirect_placeholder_50_ret_type {
    uint64_t field_0;
    uint64_t field_1;
    uint64_t field_2;
};
struct indirect_placeholder_49_ret_type {
    uint64_t field_0;
    uint64_t field_1;
};
struct indirect_placeholder_52_ret_type {
    uint64_t field_0;
    uint64_t field_1;
};
struct indirect_placeholder_53_ret_type {
    uint64_t field_0;
    uint64_t field_1;
};
struct indirect_placeholder_59_ret_type {
    uint64_t field_0;
    uint64_t field_1;
    uint64_t field_2;
    uint64_t field_3;
};
extern void abort(void);
extern uint64_t revng_init_local_sp(uint64_t param_0);
extern uint64_t init_rbp(void);
extern uint64_t indirect_placeholder_2(uint64_t param_0, uint64_t param_1);
extern void indirect_placeholder_1(void);
extern void indirect_placeholder_36(uint64_t param_0, uint64_t param_1);
extern uint64_t init_rbx(void);
extern uint64_t indirect_placeholder_10(uint64_t param_0);
extern void indirect_placeholder_14(uint64_t param_0);
extern struct indirect_placeholder_54_ret_type indirect_placeholder_54(void);
extern struct indirect_placeholder_56_ret_type indirect_placeholder_56(uint64_t param_0);
extern struct indirect_placeholder_55_ret_type indirect_placeholder_55(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint64_t param_3, uint64_t param_4, uint64_t param_5, uint64_t param_6);
extern struct indirect_placeholder_57_ret_type indirect_placeholder_57(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint64_t param_3, uint64_t param_4, uint64_t param_5, uint64_t param_6);
extern struct indirect_placeholder_51_ret_type indirect_placeholder_51(uint64_t param_0);
extern struct indirect_placeholder_50_ret_type indirect_placeholder_50(uint64_t param_0);
extern struct indirect_placeholder_49_ret_type indirect_placeholder_49(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint64_t param_3, uint64_t param_4, uint64_t param_5, uint64_t param_6);
extern struct indirect_placeholder_52_ret_type indirect_placeholder_52(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint64_t param_3, uint64_t param_4, uint64_t param_5, uint64_t param_6);
extern struct indirect_placeholder_53_ret_type indirect_placeholder_53(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint64_t param_3, uint64_t param_4, uint64_t param_5, uint64_t param_6);
extern void indirect_placeholder_58(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint64_t param_3, uint64_t param_4, uint64_t param_5);
extern struct indirect_placeholder_59_ret_type indirect_placeholder_59(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint64_t param_3, uint64_t param_4);
void bb_main(uint64_t rdi, uint64_t rsi) {
    struct indirect_placeholder_59_ret_type var_15;
    struct indirect_placeholder_54_ret_type var_49;
    uint64_t var_17;
    uint64_t var_36;
    struct indirect_placeholder_56_ret_type var_37;
    uint64_t var_38;
    uint64_t var_39;
    uint64_t var_40;
    uint64_t var_41;
    uint64_t var_42;
    uint64_t var_32;
    uint64_t var_33;
    uint64_t var_34;
    uint64_t var_35;
    uint64_t local_sp_1;
    uint64_t var_43;
    uint64_t var_44;
    uint64_t var_30;
    uint64_t var_31;
    uint64_t local_sp_3;
    uint32_t var_24;
    uint64_t local_sp_2;
    uint64_t var_0;
    uint64_t var_1;
    uint64_t var_2;
    uint64_t var_3;
    uint32_t *var_4;
    uint64_t var_5;
    uint64_t *var_6;
    uint64_t **var_7;
    uint64_t var_8;
    uint64_t var_9;
    uint32_t *var_10;
    uint32_t *var_11;
    uint64_t local_sp_5;
    uint64_t var_55;
    struct indirect_placeholder_51_ret_type var_56;
    uint64_t local_sp_0;
    uint32_t *var_57;
    uint64_t var_58;
    struct indirect_placeholder_50_ret_type var_59;
    uint64_t var_60;
    uint64_t var_61;
    uint64_t var_62;
    uint64_t var_63;
    uint64_t var_50;
    uint64_t var_51;
    uint64_t var_52;
    uint64_t var_53;
    uint64_t var_54;
    uint32_t var_45;
    uint64_t var_46;
    uint64_t var_47;
    uint64_t var_48;
    uint64_t var_25;
    uint64_t var_26;
    uint64_t var_27;
    bool var_28;
    uint64_t var_29;
    uint64_t var_23;
    uint64_t local_sp_4;
    uint64_t var_18;
    uint64_t var_19;
    uint64_t var_20;
    uint64_t var_21;
    uint32_t var_22;
    uint64_t var_12;
    uint64_t var_13;
    uint64_t var_14;
    uint32_t var_16;
    unsigned int loop_state_var;
    var_0 = revng_init_local_sp(0UL);
    var_1 = init_rbp();
    var_2 = init_rbx();
    var_3 = var_0 + (-8L);
    *(uint64_t *)var_3 = var_1;
    *(uint64_t *)(var_0 + (-16L)) = var_2;
    var_4 = (uint32_t *)(var_0 + (-44L));
    *var_4 = (uint32_t)rdi;
    var_5 = var_0 + (-56L);
    var_6 = (uint64_t *)var_5;
    *var_6 = rsi;
    var_7 = (uint64_t **)var_5;
    var_8 = **var_7;
    *(uint64_t *)(var_0 + (-64L)) = 4203682UL;
    indirect_placeholder_14(var_8);
    *(uint64_t *)(var_0 + (-72L)) = 4203697UL;
    indirect_placeholder_1();
    *(uint64_t *)(var_0 + (-80L)) = 4203707UL;
    indirect_placeholder_14(125UL);
    var_9 = var_0 + (-88L);
    *(uint64_t *)var_9 = 4203717UL;
    indirect_placeholder_1();
    var_10 = (uint32_t *)(var_0 + (-28L));
    var_11 = (uint32_t *)(var_0 + (-32L));
    local_sp_5 = var_9;
    while (1U)
        {
            var_12 = *var_6;
            var_13 = (uint64_t)*var_4;
            var_14 = local_sp_5 + (-8L);
            *(uint64_t *)var_14 = 4204234UL;
            var_15 = indirect_placeholder_59(4273876UL, 4272128UL, var_13, var_12, 0UL);
            var_16 = (uint32_t)var_15.field_0;
            *var_10 = var_16;
            local_sp_4 = var_14;
            if (var_16 != 4294967295U) {
                *var_6 = (*var_6 + ((uint64_t)*(uint32_t *)6383832UL << 3UL));
                var_45 = *var_4 - *(uint32_t *)6383832UL;
                *var_4 = var_45;
                if ((int)var_45 > (int)0U) {
                    var_46 = var_15.field_3;
                    var_47 = var_15.field_2;
                    var_48 = var_15.field_1;
                    *(uint64_t *)(local_sp_5 + (-16L)) = 4204303UL;
                    indirect_placeholder_53(0UL, 4273884UL, var_48, 0UL, 0UL, var_47, var_46);
                    *(uint64_t *)(local_sp_5 + (-24L)) = 4204313UL;
                    indirect_placeholder_36(var_3, 125UL);
                    abort();
                }
                *(uint64_t *)(local_sp_5 + (-16L)) = 4204318UL;
                var_49 = indirect_placeholder_54();
                if ((uint64_t)(unsigned char)var_49.field_0 != 1UL) {
                    var_53 = *(uint64_t *)6384280UL;
                    var_54 = local_sp_5 + (-24L);
                    *(uint64_t *)var_54 = 4204375UL;
                    indirect_placeholder_14(var_53);
                    local_sp_0 = var_54;
                    if (*(uint64_t *)6384128UL != 0UL) {
                        loop_state_var = 0U;
                        break;
                    }
                    var_55 = local_sp_5 + (-32L);
                    *(uint64_t *)var_55 = 4204397UL;
                    var_56 = indirect_placeholder_51(4273952UL);
                    *(uint64_t *)6384128UL = var_56.field_0;
                    local_sp_0 = var_55;
                    loop_state_var = 0U;
                    break;
                }
                var_50 = var_49.field_3;
                var_51 = var_49.field_2;
                var_52 = var_49.field_1;
                *(uint64_t *)(local_sp_5 + (-24L)) = 4204350UL;
                indirect_placeholder_52(0UL, 4273904UL, var_52, 0UL, 0UL, var_51, var_50);
                *(uint64_t *)(local_sp_5 + (-32L)) = 4204360UL;
                indirect_placeholder_36(var_3, 125UL);
                abort();
            }
            var_17 = (uint64_t)var_16;
            if ((uint64_t)(var_16 + (-101)) != 0UL) {
                if ((int)var_16 > (int)101U) {
                    if ((uint64_t)(var_16 + (-105)) != 0UL & (uint64_t)(var_16 + (-111)) != 0UL) {
                        loop_state_var = 1U;
                        break;
                    }
                }
                if ((uint64_t)(var_16 + 131U) != 0UL) {
                    if ((uint64_t)(var_16 + 130U) != 0UL) {
                        loop_state_var = 1U;
                        break;
                    }
                    *(uint64_t *)(local_sp_5 + (-16L)) = 4204132UL;
                    indirect_placeholder_36(var_3, 0UL);
                    abort();
                }
                var_18 = *(uint64_t *)6383680UL;
                *(uint64_t *)(local_sp_5 + (-16L)) = 4204184UL;
                indirect_placeholder_58(0UL, 4271840UL, var_18, 4273475UL, 0UL, 4273862UL);
                var_19 = local_sp_5 + (-24L);
                *(uint64_t *)var_19 = 4204194UL;
                indirect_placeholder_1();
                local_sp_4 = var_19;
                loop_state_var = 1U;
                break;
            }
            var_20 = local_sp_5 + (-16L);
            *(uint64_t *)var_20 = 4203786UL;
            var_21 = indirect_placeholder_10(var_17);
            var_22 = (uint32_t)var_21;
            *var_11 = var_22;
            var_24 = var_22;
            local_sp_2 = var_20;
            if (var_22 > 2U) {
                var_23 = local_sp_5 + (-24L);
                *(uint64_t *)var_23 = 4203828UL;
                indirect_placeholder_1();
                var_24 = *var_11;
                local_sp_2 = var_23;
            }
            *(uint32_t *)(((uint64_t)var_24 * 24UL) + 6384200UL) = *var_10;
            local_sp_3 = local_sp_2;
            var_25 = (uint64_t)(uint32_t)(uint64_t)**(unsigned char **)6384736UL;
            var_26 = local_sp_3 + (-8L);
            *(uint64_t *)var_26 = 4203903UL;
            var_27 = indirect_placeholder_10(var_25);
            var_28 = ((uint64_t)(unsigned char)var_27 == 0UL);
            var_29 = *(uint64_t *)6384736UL;
            local_sp_3 = var_26;
            while (!var_28)
                {
                    *(uint64_t *)6384736UL = (var_29 + 1UL);
                    var_25 = (uint64_t)(uint32_t)(uint64_t)**(unsigned char **)6384736UL;
                    var_26 = local_sp_3 + (-8L);
                    *(uint64_t *)var_26 = 4203903UL;
                    var_27 = indirect_placeholder_10(var_25);
                    var_28 = ((uint64_t)(unsigned char)var_27 == 0UL);
                    var_29 = *(uint64_t *)6384736UL;
                    local_sp_3 = var_26;
                }
            *(uint64_t *)(((uint64_t)*var_11 * 24UL) + 6384208UL) = var_29;
            if (*var_10 != 105U) {
                if (**(unsigned char **)6384736UL == 'L') {
                    var_43 = var_15.field_2;
                    var_44 = var_15.field_3;
                    *(uint64_t *)(local_sp_3 + (-16L)) = 4203987UL;
                    indirect_placeholder_57(0UL, 4273808UL, var_29, 0UL, 0UL, var_43, var_44);
                    *(uint64_t *)(local_sp_3 + (-24L)) = 4203997UL;
                    indirect_placeholder_36(var_3, 125UL);
                    abort();
                }
            }
            var_30 = *(uint64_t *)6384736UL;
            var_31 = local_sp_3 + (-16L);
            *(uint64_t *)var_31 = 4204017UL;
            indirect_placeholder_1();
            local_sp_1 = var_31;
            var_32 = ((uint64_t)*var_11 * 24UL) + 6384192UL;
            var_33 = *(uint64_t *)6384736UL;
            var_34 = local_sp_3 + (-24L);
            *(uint64_t *)var_34 = 4204065UL;
            var_35 = indirect_placeholder_2(var_33, var_32);
            local_sp_1 = var_34;
            if ((uint64_t)(uint32_t)var_30 != 0UL & (uint64_t)((uint32_t)var_35 + 1U) == 0UL) {
                var_36 = *(uint64_t *)6384736UL;
                *(uint64_t *)(local_sp_3 + (-32L)) = 4204085UL;
                var_37 = indirect_placeholder_56(var_36);
                var_38 = var_37.field_0;
                var_39 = var_37.field_1;
                var_40 = var_37.field_2;
                *(uint64_t *)(local_sp_3 + (-40L)) = 4204093UL;
                indirect_placeholder_1();
                var_41 = (uint64_t)*(uint32_t *)var_38;
                var_42 = local_sp_3 + (-48L);
                *(uint64_t *)var_42 = 4204120UL;
                indirect_placeholder_55(0UL, 4273846UL, var_38, 125UL, var_41, var_39, var_40);
                local_sp_1 = var_42;
            }
            local_sp_5 = local_sp_1;
            continue;
        }
    switch (loop_state_var) {
      case 0U:
        {
            *(uint64_t *)(local_sp_0 + (-8L)) = 4204409UL;
            indirect_placeholder_1();
            *(uint64_t *)(local_sp_0 + (-16L)) = 4204424UL;
            indirect_placeholder_1();
            var_57 = **(uint32_t ***)var_5;
            *(uint64_t *)(local_sp_0 + (-24L)) = 4204446UL;
            indirect_placeholder_1();
            *(uint64_t *)(local_sp_0 + (-32L)) = 4204451UL;
            indirect_placeholder_1();
            *(uint32_t *)(var_0 + (-36L)) = ((*var_57 == 2U) ? 127U : 126U);
            var_58 = **var_7;
            *(uint64_t *)(local_sp_0 + (-40L)) = 4204488UL;
            var_59 = indirect_placeholder_50(var_58);
            var_60 = var_59.field_0;
            var_61 = var_59.field_1;
            var_62 = var_59.field_2;
            *(uint64_t *)(local_sp_0 + (-48L)) = 4204496UL;
            indirect_placeholder_1();
            var_63 = (uint64_t)*(uint32_t *)var_60;
            *(uint64_t *)(local_sp_0 + (-56L)) = 4204523UL;
            indirect_placeholder_49(0UL, 4274034UL, var_60, 0UL, var_63, var_61, var_62);
            return;
        }
        break;
      case 1U:
        {
            *(uint64_t *)(local_sp_4 + (-8L)) = 4204204UL;
            indirect_placeholder_36(var_3, 125UL);
            abort();
        }
        break;
    }
}

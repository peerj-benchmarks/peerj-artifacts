typedef unsigned char __u_char;
typedef unsigned short __u_short;
typedef unsigned int __u_int;
typedef unsigned long __u_long;
typedef signed char __int8_t;
typedef unsigned char __uint8_t;
typedef short __int16_t;
typedef unsigned short __uint16_t;
typedef int __int32_t;
typedef unsigned int __uint32_t;
typedef long __int64_t;
typedef unsigned long __uint64_t;
typedef __int8_t __int_least8_t;
typedef __uint8_t __uint_least8_t;
typedef __int16_t __int_least16_t;
typedef __uint16_t __uint_least16_t;
typedef __int32_t __int_least32_t;
typedef __uint32_t __uint_least32_t;
typedef __int64_t __int_least64_t;
typedef __uint64_t __uint_least64_t;
typedef long __quad_t;
typedef unsigned long __u_quad_t;
typedef long __intmax_t;
typedef unsigned long __uintmax_t;
typedef unsigned long __dev_t;
typedef unsigned int __uid_t;
typedef unsigned int __gid_t;
typedef unsigned long __ino_t;
typedef unsigned long __ino64_t;
typedef unsigned int __mode_t;
typedef unsigned long __nlink_t;
typedef long __off_t;
typedef long __off64_t;
typedef int __pid_t;
typedef struct {
    int __val[2];
} __fsid_t;
typedef long __clock_t;
typedef unsigned long __rlim_t;
typedef unsigned long __rlim64_t;
typedef unsigned int __id_t;
typedef long __time_t;
typedef unsigned int __useconds_t;
typedef long __suseconds_t;
typedef int __daddr_t;
typedef int __key_t;
typedef int __clockid_t;
typedef void *__timer_t;
typedef long __blksize_t;
typedef long __blkcnt_t;
typedef long __blkcnt64_t;
typedef unsigned long __fsblkcnt_t;
typedef unsigned long __fsblkcnt64_t;
typedef unsigned long __fsfilcnt_t;
typedef unsigned long __fsfilcnt64_t;
typedef long __fsword_t;
typedef long __ssize_t;
typedef long __syscall_slong_t;
typedef unsigned long __syscall_ulong_t;
typedef __off64_t __loff_t;
typedef char *__caddr_t;
typedef long __intptr_t;
typedef unsigned int __socklen_t;
typedef int __sig_atomic_t;
typedef __int8_t int8_t;
typedef __int16_t int16_t;
typedef __int32_t int32_t;
typedef __int64_t int64_t;
typedef __uint8_t uint8_t;
typedef __uint16_t uint16_t;
typedef __uint32_t uint32_t;
typedef __uint64_t uint64_t;
typedef __int_least8_t int_least8_t;
typedef __int_least16_t int_least16_t;
typedef __int_least32_t int_least32_t;
typedef __int_least64_t int_least64_t;
typedef __uint_least8_t uint_least8_t;
typedef __uint_least16_t uint_least16_t;
typedef __uint_least32_t uint_least32_t;
typedef __uint_least64_t uint_least64_t;
typedef signed char int_fast8_t;
typedef long int_fast16_t;
typedef long int_fast32_t;
typedef long int_fast64_t;
typedef unsigned char uint_fast8_t;
typedef unsigned long uint_fast16_t;
typedef unsigned long uint_fast32_t;
typedef unsigned long uint_fast64_t;
typedef long intptr_t;
typedef unsigned long uintptr_t;
typedef __intmax_t intmax_t;
typedef __uintmax_t uintmax_t;
typedef _Bool bool;
struct indirect_placeholder_26_ret_type;
struct indirect_placeholder_25_ret_type;
struct indirect_placeholder_29_ret_type;
struct indirect_placeholder_28_ret_type;
struct indirect_placeholder_30_ret_type;
struct indirect_placeholder_31_ret_type;
struct indirect_placeholder_32_ret_type;
struct indirect_placeholder_33_ret_type;
struct indirect_placeholder_26_ret_type {
    uint64_t field_0;
    uint64_t field_1;
    uint64_t field_2;
};
struct indirect_placeholder_25_ret_type {
    uint64_t field_0;
    uint64_t field_1;
};
struct indirect_placeholder_29_ret_type {
    uint64_t field_0;
    uint64_t field_1;
    uint64_t field_2;
};
struct indirect_placeholder_28_ret_type {
    uint64_t field_0;
    uint64_t field_1;
};
struct indirect_placeholder_30_ret_type {
    uint64_t field_0;
    uint64_t field_1;
    uint64_t field_2;
};
struct indirect_placeholder_31_ret_type {
    uint64_t field_0;
    uint64_t field_1;
    uint64_t field_2;
};
struct indirect_placeholder_32_ret_type {
    uint64_t field_0;
    uint64_t field_1;
    uint64_t field_2;
};
struct indirect_placeholder_33_ret_type {
    uint64_t field_0;
    uint64_t field_1;
    uint64_t field_2;
};
extern void abort(void);
extern uint64_t revng_init_local_sp(uint64_t param_0);
extern uint64_t init_rbp(void);
extern void indirect_placeholder_1(void);
extern uint64_t init_rbx(void);
extern uint64_t indirect_placeholder_10(uint64_t param_0);
extern void indirect_placeholder_27(uint64_t param_0, uint64_t param_1, uint64_t param_2);
extern uint64_t init_r9(void);
extern uint64_t init_r8(void);
extern struct indirect_placeholder_26_ret_type indirect_placeholder_26(uint64_t param_0);
extern struct indirect_placeholder_25_ret_type indirect_placeholder_25(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint64_t param_3, uint64_t param_4, uint64_t param_5, uint64_t param_6);
extern struct indirect_placeholder_29_ret_type indirect_placeholder_29(uint64_t param_0);
extern struct indirect_placeholder_28_ret_type indirect_placeholder_28(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint64_t param_3, uint64_t param_4, uint64_t param_5, uint64_t param_6);
extern struct indirect_placeholder_30_ret_type indirect_placeholder_30(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint64_t param_3, uint64_t param_4, uint64_t param_5, uint64_t param_6);
extern struct indirect_placeholder_31_ret_type indirect_placeholder_31(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint64_t param_3, uint64_t param_4, uint64_t param_5, uint64_t param_6);
extern struct indirect_placeholder_32_ret_type indirect_placeholder_32(uint64_t param_0);
extern struct indirect_placeholder_33_ret_type indirect_placeholder_33(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint64_t param_3, uint64_t param_4, uint64_t param_5, uint64_t param_6);
void bb_set_LD_PRELOAD(void) {
    uint64_t var_0;
    uint64_t var_1;
    uint64_t var_2;
    uint64_t var_3;
    uint64_t var_4;
    uint64_t var_5;
    uint64_t *var_6;
    uint64_t var_7;
    uint64_t *var_8;
    uint64_t var_9;
    uint64_t var_10;
    uint64_t var_11;
    uint64_t *var_12;
    uint64_t var_13;
    uint32_t *var_14;
    uint64_t *var_15;
    uint64_t local_sp_3;
    uint64_t var_60;
    struct indirect_placeholder_26_ret_type var_61;
    uint64_t var_62;
    uint64_t var_63;
    uint64_t var_64;
    uint64_t var_65;
    uint32_t var_53;
    uint64_t r9_0;
    uint64_t r8_0;
    bool var_54;
    uint64_t *var_55;
    uint64_t *var_56;
    uint64_t var_57;
    uint64_t var_58;
    uint32_t var_59;
    uint64_t r9_3;
    struct indirect_placeholder_29_ret_type var_27;
    uint64_t var_28;
    uint64_t var_29;
    uint64_t var_30;
    uint64_t var_31;
    struct indirect_placeholder_28_ret_type var_32;
    uint64_t r8_2;
    uint64_t var_39;
    uint64_t local_sp_1;
    uint64_t r9_1;
    uint64_t r8_1;
    uint64_t var_26;
    uint64_t var_23;
    uint64_t var_24;
    uint64_t var_25;
    uint64_t var_38;
    uint64_t local_sp_2;
    uint64_t r9_2;
    bool var_40;
    uint64_t var_41;
    uint64_t var_42;
    uint64_t var_43;
    uint64_t *var_44;
    struct indirect_placeholder_30_ret_type var_45;
    uint64_t var_46;
    uint64_t var_47;
    uint32_t var_48;
    struct indirect_placeholder_31_ret_type var_49;
    uint64_t var_50;
    uint64_t var_51;
    uint32_t var_52;
    uint64_t r8_3;
    uint64_t var_33;
    struct indirect_placeholder_32_ret_type var_34;
    uint64_t var_35;
    uint64_t var_36;
    uint64_t var_37;
    uint64_t **var_16;
    uint64_t var_17;
    struct indirect_placeholder_33_ret_type var_18;
    uint64_t var_19;
    uint64_t var_20;
    uint64_t var_21;
    uint32_t var_22;
    var_0 = revng_init_local_sp(0UL);
    var_1 = init_rbp();
    var_2 = init_r9();
    var_3 = init_r8();
    var_4 = init_rbx();
    var_5 = var_0 + (-8L);
    *(uint64_t *)var_5 = var_1;
    *(uint64_t *)(var_0 + (-16L)) = var_4;
    var_6 = (uint64_t *)(var_0 + (-48L));
    *var_6 = 4273504UL;
    var_7 = var_0 + (-256L);
    *(uint64_t *)var_7 = 4202884UL;
    indirect_placeholder_1();
    var_8 = (uint64_t *)(var_0 + (-56L));
    *var_8 = 4273504UL;
    var_9 = *(uint64_t *)6384128UL;
    var_10 = var_0 + (-88L);
    *(uint64_t *)var_10 = var_9;
    *(uint64_t *)(var_0 + (-80L)) = 4273520UL;
    *(uint64_t *)(var_0 + (-72L)) = 0UL;
    var_11 = var_0 + (-40L);
    var_12 = (uint64_t *)var_11;
    *var_12 = var_10;
    var_13 = var_0 + (-96L);
    var_14 = (uint32_t *)(var_0 + (-28L));
    var_15 = (uint64_t *)var_13;
    local_sp_3 = var_7;
    r9_3 = var_2;
    r8_3 = var_3;
    while (1U)
        {
            if (***(unsigned char ***)var_11 != '\x00') {
                var_33 = local_sp_3 + (-8L);
                *(uint64_t *)var_33 = 4202947UL;
                var_34 = indirect_placeholder_32(4273606UL);
                var_35 = var_34.field_0;
                var_36 = var_34.field_1;
                var_37 = var_34.field_2;
                *var_15 = var_35;
                var_38 = var_35;
                local_sp_2 = var_33;
                r9_2 = var_36;
                r8_2 = var_37;
                break;
            }
            var_16 = (uint64_t **)var_11;
            var_17 = **var_16;
            *(uint64_t *)(local_sp_3 + (-8L)) = 4202990UL;
            var_18 = indirect_placeholder_33(0UL, var_17, 4273606UL, var_13, 4273619UL, r9_3, r8_3);
            var_19 = var_18.field_0;
            var_20 = var_18.field_1;
            var_21 = var_18.field_2;
            var_22 = (uint32_t)var_19;
            *var_14 = var_22;
            r9_1 = var_20;
            r8_1 = var_21;
            r9_2 = var_20;
            r8_2 = var_21;
            if ((int)var_22 > (int)4294967295U) {
                *(uint64_t *)(local_sp_3 + (-16L)) = 4203004UL;
                indirect_placeholder_27(var_5, var_20, var_21);
                abort();
            }
            var_23 = *var_15;
            var_24 = local_sp_3 + (-16L);
            *(uint64_t *)var_24 = 4203026UL;
            var_25 = indirect_placeholder_10(var_23);
            local_sp_2 = var_24;
            if ((uint64_t)(uint32_t)var_25 != 0UL) {
                var_38 = *var_15;
                break;
            }
            var_26 = local_sp_3 + (-24L);
            *(uint64_t *)var_26 = 4203044UL;
            indirect_placeholder_1();
            *var_12 = (*var_12 + 8UL);
            local_sp_1 = var_26;
            if (**var_16 == 0UL) {
                *(uint64_t *)(local_sp_3 + (-32L)) = 4203071UL;
                var_27 = indirect_placeholder_29(4273606UL);
                var_28 = var_27.field_0;
                var_29 = var_27.field_1;
                var_30 = var_27.field_2;
                var_31 = local_sp_3 + (-40L);
                *(uint64_t *)var_31 = 4203099UL;
                var_32 = indirect_placeholder_28(0UL, 4273625UL, var_28, 125UL, 0UL, var_29, var_30);
                local_sp_1 = var_31;
                r9_1 = var_32.field_0;
                r8_1 = var_32.field_1;
            }
            local_sp_3 = local_sp_1;
            r9_3 = r9_1;
            r8_3 = r8_1;
            continue;
        }
    var_39 = *var_8;
    var_40 = (var_39 == 0UL);
    var_41 = *var_6;
    var_42 = var_0 + (-64L);
    var_43 = local_sp_2 + (-8L);
    var_44 = (uint64_t *)var_43;
    if (var_40) {
        *var_44 = 4203183UL;
        var_49 = indirect_placeholder_31(0UL, var_41, var_38, var_42, 4273652UL, r9_2, r8_2);
        var_50 = var_49.field_1;
        var_51 = var_49.field_2;
        var_52 = (uint32_t)var_49.field_0;
        *var_14 = var_52;
        var_53 = var_52;
        r9_0 = var_50;
        r8_0 = var_51;
    } else {
        *var_44 = 4203148UL;
        var_45 = indirect_placeholder_30(0UL, var_41, var_39, var_42, 4273643UL, r9_2, var_38);
        var_46 = var_45.field_1;
        var_47 = var_45.field_2;
        var_48 = (uint32_t)var_45.field_0;
        *var_14 = var_48;
        var_53 = var_48;
        r9_0 = var_46;
        r8_0 = var_47;
    }
    var_54 = ((int)var_53 > (int)4294967295U);
    var_55 = (uint64_t *)(var_43 + (-8L));
    if (var_54) {
        *var_55 = 4203197UL;
        indirect_placeholder_27(var_5, r9_0, r8_0);
        abort();
    }
    *var_55 = 4203209UL;
    indirect_placeholder_1();
    var_56 = (uint64_t *)var_42;
    var_57 = *var_56;
    *(uint64_t *)(var_43 + (-16L)) = 4203221UL;
    var_58 = indirect_placeholder_10(var_57);
    var_59 = (uint32_t)var_58;
    *var_14 = var_59;
    if (var_59 == 0U) {
        return;
    }
    var_60 = *var_56;
    *(uint64_t *)(var_43 + (-24L)) = 4203242UL;
    var_61 = indirect_placeholder_26(var_60);
    var_62 = var_61.field_0;
    var_63 = var_61.field_1;
    var_64 = var_61.field_2;
    *(uint64_t *)(var_43 + (-32L)) = 4203250UL;
    indirect_placeholder_1();
    var_65 = (uint64_t)*(uint32_t *)var_62;
    *(uint64_t *)(var_43 + (-40L)) = 4203277UL;
    indirect_placeholder_25(0UL, 4273664UL, var_62, 125UL, var_65, var_63, var_64);
    return;
}

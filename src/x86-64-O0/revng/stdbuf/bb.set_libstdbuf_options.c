typedef unsigned char __u_char;
typedef unsigned short __u_short;
typedef unsigned int __u_int;
typedef unsigned long __u_long;
typedef signed char __int8_t;
typedef unsigned char __uint8_t;
typedef short __int16_t;
typedef unsigned short __uint16_t;
typedef int __int32_t;
typedef unsigned int __uint32_t;
typedef long __int64_t;
typedef unsigned long __uint64_t;
typedef __int8_t __int_least8_t;
typedef __uint8_t __uint_least8_t;
typedef __int16_t __int_least16_t;
typedef __uint16_t __uint_least16_t;
typedef __int32_t __int_least32_t;
typedef __uint32_t __uint_least32_t;
typedef __int64_t __int_least64_t;
typedef __uint64_t __uint_least64_t;
typedef long __quad_t;
typedef unsigned long __u_quad_t;
typedef long __intmax_t;
typedef unsigned long __uintmax_t;
typedef unsigned long __dev_t;
typedef unsigned int __uid_t;
typedef unsigned int __gid_t;
typedef unsigned long __ino_t;
typedef unsigned long __ino64_t;
typedef unsigned int __mode_t;
typedef unsigned long __nlink_t;
typedef long __off_t;
typedef long __off64_t;
typedef int __pid_t;
typedef struct {
    int __val[2];
} __fsid_t;
typedef long __clock_t;
typedef unsigned long __rlim_t;
typedef unsigned long __rlim64_t;
typedef unsigned int __id_t;
typedef long __time_t;
typedef unsigned int __useconds_t;
typedef long __suseconds_t;
typedef int __daddr_t;
typedef int __key_t;
typedef int __clockid_t;
typedef void *__timer_t;
typedef long __blksize_t;
typedef long __blkcnt_t;
typedef long __blkcnt64_t;
typedef unsigned long __fsblkcnt_t;
typedef unsigned long __fsblkcnt64_t;
typedef unsigned long __fsfilcnt_t;
typedef unsigned long __fsfilcnt64_t;
typedef long __fsword_t;
typedef long __ssize_t;
typedef long __syscall_slong_t;
typedef unsigned long __syscall_ulong_t;
typedef __off64_t __loff_t;
typedef char *__caddr_t;
typedef long __intptr_t;
typedef unsigned int __socklen_t;
typedef int __sig_atomic_t;
typedef __int8_t int8_t;
typedef __int16_t int16_t;
typedef __int32_t int32_t;
typedef __int64_t int64_t;
typedef __uint8_t uint8_t;
typedef __uint16_t uint16_t;
typedef __uint32_t uint32_t;
typedef __uint64_t uint64_t;
typedef __int_least8_t int_least8_t;
typedef __int_least16_t int_least16_t;
typedef __int_least32_t int_least32_t;
typedef __int_least64_t int_least64_t;
typedef __uint_least8_t uint_least8_t;
typedef __uint_least16_t uint_least16_t;
typedef __uint_least32_t uint_least32_t;
typedef __uint_least64_t uint_least64_t;
typedef signed char int_fast8_t;
typedef long int_fast16_t;
typedef long int_fast32_t;
typedef long int_fast64_t;
typedef unsigned char uint_fast8_t;
typedef unsigned long uint_fast16_t;
typedef unsigned long uint_fast32_t;
typedef unsigned long uint_fast64_t;
typedef long intptr_t;
typedef unsigned long uintptr_t;
typedef __intmax_t intmax_t;
typedef __uintmax_t uintmax_t;
struct bb_set_libstdbuf_options_ret_type;
struct indirect_placeholder_44_ret_type;
struct indirect_placeholder_43_ret_type;
struct indirect_placeholder_46_ret_type;
struct indirect_placeholder_45_ret_type;
struct bb_set_libstdbuf_options_ret_type {
    uint64_t field_0;
    uint64_t field_1;
    uint64_t field_2;
    uint64_t field_3;
};
struct indirect_placeholder_44_ret_type {
    uint64_t field_0;
    uint64_t field_1;
    uint64_t field_2;
};
struct indirect_placeholder_43_ret_type {
    uint64_t field_0;
    uint64_t field_1;
};
struct indirect_placeholder_46_ret_type {
    uint64_t field_0;
    uint64_t field_1;
    uint64_t field_2;
};
struct indirect_placeholder_45_ret_type {
    uint64_t field_0;
    uint64_t field_1;
    uint64_t field_2;
};
extern void abort(void);
extern uint64_t revng_init_local_sp(uint64_t param_0);
extern uint64_t init_rbp(void);
extern void indirect_placeholder_1(void);
extern uint64_t init_rbx(void);
extern uint64_t indirect_placeholder_10(uint64_t param_0);
extern void indirect_placeholder_27(uint64_t param_0, uint64_t param_1, uint64_t param_2);
extern uint64_t init_r9(void);
extern uint64_t init_r8(void);
extern uint64_t init_rcx(void);
extern struct indirect_placeholder_44_ret_type indirect_placeholder_44(uint64_t param_0);
extern struct indirect_placeholder_43_ret_type indirect_placeholder_43(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint64_t param_3, uint64_t param_4, uint64_t param_5, uint64_t param_6);
extern struct indirect_placeholder_46_ret_type indirect_placeholder_46(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint64_t param_3, uint64_t param_4, uint64_t param_5, uint64_t param_6);
extern struct indirect_placeholder_45_ret_type indirect_placeholder_45(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint64_t param_3, uint64_t param_4, uint64_t param_5, uint64_t param_6);
struct bb_set_libstdbuf_options_ret_type bb_set_libstdbuf_options(void) {
    struct indirect_placeholder_46_ret_type var_24;
    uint64_t var_0;
    uint64_t var_1;
    uint64_t var_2;
    uint64_t var_3;
    uint64_t var_4;
    uint64_t var_5;
    uint64_t var_6;
    uint64_t var_7;
    unsigned char *var_8;
    uint64_t *var_9;
    uint32_t *var_10;
    uint64_t *var_11;
    uint64_t var_12;
    uint64_t rcx_1;
    uint64_t var_32;
    struct indirect_placeholder_44_ret_type var_33;
    uint64_t var_34;
    uint64_t var_35;
    uint64_t var_36;
    uint64_t var_37;
    uint64_t var_38;
    struct indirect_placeholder_43_ret_type var_39;
    uint64_t local_sp_0;
    uint64_t r9_1;
    uint64_t rcx_0;
    uint64_t r8_1;
    uint64_t r9_0;
    uint64_t r8_0;
    uint64_t var_40;
    uint32_t var_28;
    uint64_t local_sp_1;
    uint64_t var_29;
    uint64_t var_30;
    uint64_t var_31;
    uint64_t rcx_3;
    uint64_t local_sp_2;
    uint64_t r9_3;
    uint64_t rcx_2;
    uint64_t r8_3;
    uint64_t r9_2;
    uint64_t r8_2;
    struct bb_set_libstdbuf_options_ret_type mrv;
    struct bb_set_libstdbuf_options_ret_type mrv1;
    struct bb_set_libstdbuf_options_ret_type mrv2;
    struct bb_set_libstdbuf_options_ret_type mrv3;
    uint64_t var_13;
    uint64_t var_14;
    uint64_t var_22;
    uint64_t var_23;
    uint64_t var_25;
    uint64_t var_26;
    uint32_t var_27;
    uint64_t var_15;
    uint64_t var_16;
    uint64_t var_17;
    struct indirect_placeholder_45_ret_type var_18;
    uint64_t var_19;
    uint64_t var_20;
    uint32_t var_21;
    uint64_t local_sp_3;
    uint64_t var_41;
    var_0 = revng_init_local_sp(0UL);
    var_1 = init_rbp();
    var_2 = init_rcx();
    var_3 = init_r9();
    var_4 = init_r8();
    var_5 = init_rbx();
    var_6 = var_0 + (-8L);
    *(uint64_t *)var_6 = var_1;
    *(uint64_t *)(var_0 + (-16L)) = var_5;
    var_7 = var_0 + (-56L);
    var_8 = (unsigned char *)(var_0 + (-25L));
    *var_8 = (unsigned char)'\x00';
    var_9 = (uint64_t *)(var_0 + (-40L));
    *var_9 = 0UL;
    var_10 = (uint32_t *)(var_0 + (-44L));
    var_11 = (uint64_t *)var_7;
    var_12 = 0UL;
    local_sp_2 = var_7;
    rcx_2 = var_2;
    r9_2 = var_3;
    r8_2 = var_4;
    while (1U)
        {
            var_40 = var_12;
            rcx_3 = rcx_2;
            r9_3 = r9_2;
            r8_3 = r8_2;
            local_sp_3 = local_sp_2;
            if (var_12 > 2UL) {
                mrv.field_0 = (uint64_t)*var_8;
                mrv1 = mrv;
                mrv1.field_1 = rcx_2;
                mrv2 = mrv1;
                mrv2.field_2 = r9_2;
                mrv3 = mrv2;
                mrv3.field_3 = r8_2;
                return mrv3;
            }
            var_13 = var_12 * 24UL;
            var_14 = var_13 + 6384208UL;
            if (*(uint64_t *)var_14 != 0UL) {
                if (**(unsigned char **)var_14 == 'L') {
                    var_22 = (uint64_t)*(uint32_t *)(var_13 + 6384200UL);
                    *(uint64_t *)(local_sp_2 + (-8L)) = 4203414UL;
                    indirect_placeholder_1();
                    var_23 = local_sp_2 + (-16L);
                    *(uint64_t *)var_23 = 4203445UL;
                    var_24 = indirect_placeholder_46(0UL, 4273705UL, var_22, var_7, 4273714UL, r9_2, r8_2);
                    var_25 = var_24.field_1;
                    var_26 = var_24.field_2;
                    var_27 = (uint32_t)var_24.field_0;
                    *var_10 = var_27;
                    rcx_1 = var_22;
                    r9_1 = var_25;
                    r8_1 = var_26;
                    var_28 = var_27;
                    local_sp_1 = var_23;
                } else {
                    var_15 = *(uint64_t *)(var_13 + 6384192UL);
                    var_16 = (uint64_t)*(uint32_t *)(var_13 + 6384200UL);
                    *(uint64_t *)(local_sp_2 + (-8L)) = 4203509UL;
                    indirect_placeholder_1();
                    var_17 = local_sp_2 + (-16L);
                    *(uint64_t *)var_17 = 4203543UL;
                    var_18 = indirect_placeholder_45(0UL, 4273705UL, var_16, var_7, 4273721UL, r9_2, var_15);
                    var_19 = var_18.field_1;
                    var_20 = var_18.field_2;
                    var_21 = (uint32_t)var_18.field_0;
                    *var_10 = var_21;
                    rcx_1 = var_16;
                    r9_1 = var_19;
                    r8_1 = var_20;
                    var_28 = var_21;
                    local_sp_1 = var_17;
                }
                rcx_0 = rcx_1;
                r9_0 = r9_1;
                r8_0 = r8_1;
                if ((int)var_28 > (int)4294967295U) {
                    *(uint64_t *)(local_sp_1 + (-8L)) = 4203557UL;
                    indirect_placeholder_27(var_6, r9_1, r8_1);
                    abort();
                }
                var_29 = *var_11;
                var_30 = local_sp_1 + (-8L);
                *(uint64_t *)var_30 = 4203569UL;
                var_31 = indirect_placeholder_10(var_29);
                local_sp_0 = var_30;
                if ((uint64_t)(uint32_t)var_31 != 0UL) {
                    var_32 = *var_11;
                    *(uint64_t *)(local_sp_1 + (-16L)) = 4203585UL;
                    var_33 = indirect_placeholder_44(var_32);
                    var_34 = var_33.field_0;
                    var_35 = var_33.field_1;
                    var_36 = var_33.field_2;
                    *(uint64_t *)(local_sp_1 + (-24L)) = 4203593UL;
                    indirect_placeholder_1();
                    var_37 = (uint64_t)*(uint32_t *)var_34;
                    var_38 = local_sp_1 + (-32L);
                    *(uint64_t *)var_38 = 4203620UL;
                    var_39 = indirect_placeholder_43(0UL, 4273664UL, var_34, 125UL, var_37, var_35, var_36);
                    local_sp_0 = var_38;
                    rcx_0 = var_34;
                    r9_0 = var_39.field_0;
                    r8_0 = var_39.field_1;
                }
                *var_8 = (unsigned char)'\x01';
                var_40 = *var_9;
                rcx_3 = rcx_0;
                r9_3 = r9_0;
                r8_3 = r8_0;
                local_sp_3 = local_sp_0;
            }
            var_41 = var_40 + 1UL;
            *var_9 = var_41;
            var_12 = var_41;
            local_sp_2 = local_sp_3;
            rcx_2 = rcx_3;
            r9_2 = r9_3;
            r8_2 = r8_3;
            continue;
        }
}

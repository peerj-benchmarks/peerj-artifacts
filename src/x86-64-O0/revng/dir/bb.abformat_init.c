typedef unsigned char __u_char;
typedef unsigned short __u_short;
typedef unsigned int __u_int;
typedef unsigned long __u_long;
typedef signed char __int8_t;
typedef unsigned char __uint8_t;
typedef short __int16_t;
typedef unsigned short __uint16_t;
typedef int __int32_t;
typedef unsigned int __uint32_t;
typedef long __int64_t;
typedef unsigned long __uint64_t;
typedef __int8_t __int_least8_t;
typedef __uint8_t __uint_least8_t;
typedef __int16_t __int_least16_t;
typedef __uint16_t __uint_least16_t;
typedef __int32_t __int_least32_t;
typedef __uint32_t __uint_least32_t;
typedef __int64_t __int_least64_t;
typedef __uint64_t __uint_least64_t;
typedef long __quad_t;
typedef unsigned long __u_quad_t;
typedef long __intmax_t;
typedef unsigned long __uintmax_t;
typedef unsigned long __dev_t;
typedef unsigned int __uid_t;
typedef unsigned int __gid_t;
typedef unsigned long __ino_t;
typedef unsigned long __ino64_t;
typedef unsigned int __mode_t;
typedef unsigned long __nlink_t;
typedef long __off_t;
typedef long __off64_t;
typedef int __pid_t;
typedef struct {
    int __val[2];
} __fsid_t;
typedef long __clock_t;
typedef unsigned long __rlim_t;
typedef unsigned long __rlim64_t;
typedef unsigned int __id_t;
typedef long __time_t;
typedef unsigned int __useconds_t;
typedef long __suseconds_t;
typedef int __daddr_t;
typedef int __key_t;
typedef int __clockid_t;
typedef void *__timer_t;
typedef long __blksize_t;
typedef long __blkcnt_t;
typedef long __blkcnt64_t;
typedef unsigned long __fsblkcnt_t;
typedef unsigned long __fsblkcnt64_t;
typedef unsigned long __fsfilcnt_t;
typedef unsigned long __fsfilcnt64_t;
typedef long __fsword_t;
typedef long __ssize_t;
typedef long __syscall_slong_t;
typedef unsigned long __syscall_ulong_t;
typedef __off64_t __loff_t;
typedef char *__caddr_t;
typedef long __intptr_t;
typedef unsigned int __socklen_t;
typedef int __sig_atomic_t;
typedef __int8_t int8_t;
typedef __int16_t int16_t;
typedef __int32_t int32_t;
typedef __int64_t int64_t;
typedef __uint8_t uint8_t;
typedef __uint16_t uint16_t;
typedef __uint32_t uint32_t;
typedef __uint64_t uint64_t;
typedef __int_least8_t int_least8_t;
typedef __int_least16_t int_least16_t;
typedef __int_least32_t int_least32_t;
typedef __int_least64_t int_least64_t;
typedef __uint_least8_t uint_least8_t;
typedef __uint_least16_t uint_least16_t;
typedef __uint_least32_t uint_least32_t;
typedef __uint_least64_t uint_least64_t;
typedef signed char int_fast8_t;
typedef long int_fast16_t;
typedef long int_fast32_t;
typedef long int_fast64_t;
typedef unsigned char uint_fast8_t;
typedef unsigned long uint_fast16_t;
typedef unsigned long uint_fast32_t;
typedef unsigned long uint_fast64_t;
typedef long intptr_t;
typedef unsigned long uintptr_t;
typedef __intmax_t intmax_t;
typedef __uintmax_t uintmax_t;
typedef _Bool bool;
struct indirect_placeholder_82_ret_type;
struct indirect_placeholder_83_ret_type;
struct indirect_placeholder_84_ret_type;
struct indirect_placeholder_82_ret_type {
    uint64_t field_0;
    uint64_t field_1;
    uint64_t field_2;
};
struct indirect_placeholder_83_ret_type {
    uint64_t field_0;
    uint64_t field_1;
    uint64_t field_2;
};
struct indirect_placeholder_84_ret_type {
    uint64_t field_0;
    uint64_t field_1;
    uint64_t field_2;
};
extern uint64_t revng_init_local_sp(uint64_t param_0);
extern uint64_t init_rbp(void);
extern uint64_t indirect_placeholder_1(uint64_t param_0);
extern struct indirect_placeholder_82_ret_type indirect_placeholder_82(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint64_t param_3, uint64_t param_4, uint64_t param_5, uint64_t param_6);
extern struct indirect_placeholder_83_ret_type indirect_placeholder_83(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint64_t param_3, uint64_t param_4, uint64_t param_5, uint64_t param_6);
extern struct indirect_placeholder_84_ret_type indirect_placeholder_84(uint64_t param_0);
void bb_abformat_init(uint64_t r9, uint64_t r8) {
    struct indirect_placeholder_82_ret_type var_38;
    uint64_t var_0;
    uint64_t var_1;
    uint64_t var_2;
    uint64_t var_3;
    uint32_t *var_4;
    uint32_t var_42;
    uint64_t local_sp_0;
    uint64_t r91_0;
    uint64_t r82_0;
    uint32_t var_43;
    uint32_t var_21;
    uint64_t var_12;
    uint64_t var_13;
    uint32_t *var_14;
    uint64_t *var_15;
    uint32_t *var_16;
    uint64_t *var_17;
    uint32_t *var_18;
    uint32_t *var_19;
    uint32_t var_20;
    uint64_t r91_2;
    uint64_t local_sp_1;
    uint64_t r82_2;
    uint64_t r91_1;
    uint64_t r82_1;
    uint64_t local_sp_2;
    uint32_t var_44;
    uint64_t var_22;
    uint64_t var_23;
    bool var_24;
    uint64_t var_25;
    uint64_t var_37;
    uint64_t var_39;
    uint64_t var_40;
    uint32_t var_41;
    uint64_t var_26;
    uint32_t var_27;
    uint64_t var_28;
    uint64_t var_29;
    uint64_t var_30;
    uint64_t var_31;
    struct indirect_placeholder_83_ret_type var_32;
    uint64_t var_33;
    uint64_t var_34;
    uint64_t var_35;
    uint32_t var_36;
    uint32_t var_5;
    uint64_t local_sp_3;
    uint64_t var_10;
    struct indirect_placeholder_84_ret_type var_11;
    uint64_t var_6;
    uint64_t var_7;
    uint64_t var_8;
    uint32_t var_9;
    unsigned int loop_state_var;
    bool switch_state_var;
    var_0 = revng_init_local_sp(0UL);
    var_1 = init_rbp();
    var_2 = var_0 + (-8L);
    *(uint64_t *)var_2 = var_1;
    var_3 = var_0 + (-1608L);
    var_4 = (uint32_t *)(var_0 + (-12L));
    *var_4 = 0U;
    var_20 = 0U;
    var_21 = 0U;
    var_5 = 0U;
    local_sp_3 = var_3;
    while ((int)var_5 <= (int)1U)
        {
            var_6 = *(uint64_t *)(((uint64_t)var_5 << 3UL) + 6514560UL);
            var_7 = local_sp_3 + (-8L);
            *(uint64_t *)var_7 = 4207727UL;
            var_8 = indirect_placeholder_1(var_6);
            *(uint64_t *)((((uint64_t)*var_4 << 3UL) + var_2) + (-64L)) = var_8;
            var_9 = *var_4 + 1U;
            *var_4 = var_9;
            var_5 = var_9;
            local_sp_3 = var_7;
        }
    if (*(uint64_t *)(var_0 + (-72L)) != 0UL) {
        if (*(uint64_t *)(var_0 + (-64L)) == 0UL) {
            return;
        }
    }
    var_10 = local_sp_3 + (-8L);
    *(uint64_t *)var_10 = 4207788UL;
    var_11 = indirect_placeholder_84(var_3);
    local_sp_1 = var_10;
    if ((uint64_t)(unsigned char)var_11.field_1 == 1UL) {
        return;
    }
    var_12 = var_11.field_2;
    var_13 = var_11.field_0;
    var_14 = (uint32_t *)(var_0 + (-16L));
    *var_14 = 0U;
    var_15 = (uint64_t *)(var_0 + (-32L));
    var_16 = (uint32_t *)(var_0 + (-20L));
    var_17 = (uint64_t *)(var_0 + (-40L));
    var_18 = (uint32_t *)(var_0 + (-44L));
    var_19 = (uint32_t *)(var_0 + (-24L));
    r91_1 = var_13;
    r82_1 = var_12;
    while (1U)
        {
            local_sp_2 = local_sp_1;
            r91_2 = r91_1;
            r82_2 = r82_1;
            if ((int)var_20 <= (int)1U) {
                *(unsigned char *)6519296UL = (unsigned char)'\x01';
                break;
            }
            *var_15 = *(uint64_t *)(((uint64_t)var_20 << 3UL) + 6514560UL);
            *var_16 = 0U;
            while (1U)
                {
                    local_sp_1 = local_sp_2;
                    r91_1 = r91_2;
                    r82_1 = r82_2;
                    if ((int)var_21 <= (int)11U) {
                        loop_state_var = 0U;
                        break;
                    }
                    var_22 = ((((uint64_t)*var_14 * 12UL) + (uint64_t)var_21) << 7UL) + 6516224UL;
                    *var_17 = var_22;
                    var_23 = *(uint64_t *)((((uint64_t)*var_14 << 3UL) + var_2) + (-64L));
                    var_24 = (var_23 == 0UL);
                    var_25 = *var_15;
                    if (var_24) {
                        var_37 = local_sp_2 + (-8L);
                        *(uint64_t *)var_37 = 4207932UL;
                        var_38 = indirect_placeholder_82(r91_2, 0UL, 4371596UL, var_25, var_22, 128UL, r82_2);
                        var_39 = var_38.field_0;
                        var_40 = var_38.field_2;
                        var_41 = (uint32_t)var_38.field_1;
                        *var_19 = var_41;
                        var_42 = var_41;
                        local_sp_0 = var_37;
                        r91_0 = var_39;
                        r82_0 = var_40;
                    } else {
                        var_26 = var_23 - var_25;
                        if ((long)var_26 <= (long)128UL) {
                            loop_state_var = 1U;
                            break;
                        }
                        var_27 = (uint32_t)var_26;
                        *var_18 = var_27;
                        var_28 = ((uint64_t)*var_16 << 7UL) + var_3;
                        var_29 = *var_15;
                        var_30 = (uint64_t)var_27;
                        var_31 = *var_17;
                        *(uint64_t *)(local_sp_2 + (-16L)) = (*(uint64_t *)((((uint64_t)*var_14 << 3UL) + var_2) + (-64L)) + 2UL);
                        *(uint64_t *)(local_sp_2 + (-24L)) = 4208084UL;
                        var_32 = indirect_placeholder_83(var_28, 0UL, 4371599UL, var_30, var_31, 128UL, var_29);
                        var_33 = var_32.field_0;
                        var_34 = var_32.field_2;
                        var_35 = local_sp_2 + (-8L);
                        var_36 = (uint32_t)var_32.field_1;
                        *var_19 = var_36;
                        var_42 = var_36;
                        local_sp_0 = var_35;
                        r91_0 = var_33;
                        r82_0 = var_34;
                    }
                    local_sp_2 = local_sp_0;
                    r91_2 = r91_0;
                    r82_2 = r82_0;
                    if (!(((int)var_42 < (int)0U) || ((int)var_42 > (int)127U))) {
                        loop_state_var = 1U;
                        break;
                    }
                    var_43 = *var_16 + 1U;
                    *var_16 = var_43;
                    var_21 = var_43;
                    continue;
                }
            switch_state_var = 0;
            switch (loop_state_var) {
              case 1U:
                {
                    switch_state_var = 1;
                    break;
                }
                break;
              case 0U:
                {
                    var_44 = *var_14 + 1U;
                    *var_14 = var_44;
                    var_20 = var_44;
                    continue;
                }
                break;
            }
            if (switch_state_var)
                break;
        }
    return;
}

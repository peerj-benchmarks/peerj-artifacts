typedef unsigned char __u_char;
typedef unsigned short __u_short;
typedef unsigned int __u_int;
typedef unsigned long __u_long;
typedef signed char __int8_t;
typedef unsigned char __uint8_t;
typedef short __int16_t;
typedef unsigned short __uint16_t;
typedef int __int32_t;
typedef unsigned int __uint32_t;
typedef long __int64_t;
typedef unsigned long __uint64_t;
typedef __int8_t __int_least8_t;
typedef __uint8_t __uint_least8_t;
typedef __int16_t __int_least16_t;
typedef __uint16_t __uint_least16_t;
typedef __int32_t __int_least32_t;
typedef __uint32_t __uint_least32_t;
typedef __int64_t __int_least64_t;
typedef __uint64_t __uint_least64_t;
typedef long __quad_t;
typedef unsigned long __u_quad_t;
typedef long __intmax_t;
typedef unsigned long __uintmax_t;
typedef unsigned long __dev_t;
typedef unsigned int __uid_t;
typedef unsigned int __gid_t;
typedef unsigned long __ino_t;
typedef unsigned long __ino64_t;
typedef unsigned int __mode_t;
typedef unsigned long __nlink_t;
typedef long __off_t;
typedef long __off64_t;
typedef int __pid_t;
typedef struct {
    int __val[2];
} __fsid_t;
typedef long __clock_t;
typedef unsigned long __rlim_t;
typedef unsigned long __rlim64_t;
typedef unsigned int __id_t;
typedef long __time_t;
typedef unsigned int __useconds_t;
typedef long __suseconds_t;
typedef int __daddr_t;
typedef int __key_t;
typedef int __clockid_t;
typedef void *__timer_t;
typedef long __blksize_t;
typedef long __blkcnt_t;
typedef long __blkcnt64_t;
typedef unsigned long __fsblkcnt_t;
typedef unsigned long __fsblkcnt64_t;
typedef unsigned long __fsfilcnt_t;
typedef unsigned long __fsfilcnt64_t;
typedef long __fsword_t;
typedef long __ssize_t;
typedef long __syscall_slong_t;
typedef unsigned long __syscall_ulong_t;
typedef __off64_t __loff_t;
typedef char *__caddr_t;
typedef long __intptr_t;
typedef unsigned int __socklen_t;
typedef int __sig_atomic_t;
typedef __int8_t int8_t;
typedef __int16_t int16_t;
typedef __int32_t int32_t;
typedef __int64_t int64_t;
typedef __uint8_t uint8_t;
typedef __uint16_t uint16_t;
typedef __uint32_t uint32_t;
typedef __uint64_t uint64_t;
typedef __int_least8_t int_least8_t;
typedef __int_least16_t int_least16_t;
typedef __int_least32_t int_least32_t;
typedef __int_least64_t int_least64_t;
typedef __uint_least8_t uint_least8_t;
typedef __uint_least16_t uint_least16_t;
typedef __uint_least32_t uint_least32_t;
typedef __uint_least64_t uint_least64_t;
typedef signed char int_fast8_t;
typedef long int_fast16_t;
typedef long int_fast32_t;
typedef long int_fast64_t;
typedef unsigned char uint_fast8_t;
typedef unsigned long uint_fast16_t;
typedef unsigned long uint_fast32_t;
typedef unsigned long uint_fast64_t;
typedef long intptr_t;
typedef unsigned long uintptr_t;
typedef __intmax_t intmax_t;
typedef __uintmax_t uintmax_t;
typedef _Bool bool;
extern uint64_t revng_init_local_sp(uint64_t param_0);
extern uint64_t init_rbp(void);
extern void indirect_placeholder_2(void);
extern uint64_t indirect_placeholder_1(uint64_t param_0);
extern uint64_t indirect_placeholder_8(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint64_t param_3);
uint64_t bb_filevercmp(uint64_t rdi, uint64_t rsi) {
    uint64_t var_0;
    uint64_t var_1;
    uint64_t var_2;
    uint64_t *var_3;
    uint64_t var_4;
    uint64_t *var_5;
    uint64_t var_6;
    uint32_t *var_7;
    uint32_t var_8;
    uint32_t rax_0_shrunk;
    uint32_t var_43;
    uint64_t var_35;
    uint64_t var_36;
    uint64_t var_33;
    uint64_t var_34;
    uint64_t local_sp_0;
    uint64_t _pre;
    uint64_t var_37;
    uint64_t var_38;
    uint64_t var_39;
    uint64_t var_40;
    uint32_t *var_41;
    uint32_t var_42;
    bool var_15;
    uint64_t var_16;
    uint64_t var_17;
    uint64_t *var_18;
    uint64_t var_19;
    uint64_t var_20;
    uint64_t *var_21;
    uint64_t var_22;
    uint64_t var_23;
    uint64_t *var_24;
    uint64_t var_25;
    uint64_t var_26;
    uint64_t var_27;
    uint64_t *var_28;
    uint64_t var_29;
    uint64_t *var_30;
    uint64_t var_31;
    uint64_t *var_32;
    uint64_t var_14;
    uint64_t var_13;
    uint64_t var_12;
    unsigned char **var_9;
    unsigned char **var_10;
    uint64_t var_11;
    var_0 = revng_init_local_sp(0UL);
    var_1 = init_rbp();
    *(uint64_t *)(var_0 + (-8L)) = var_1;
    var_2 = var_0 + (-80L);
    var_3 = (uint64_t *)var_2;
    *var_3 = rdi;
    var_4 = var_0 + (-88L);
    var_5 = (uint64_t *)var_4;
    *var_5 = rsi;
    var_6 = *var_3;
    *(uint64_t *)(var_0 + (-96L)) = 4248602UL;
    indirect_placeholder_2();
    var_7 = (uint32_t *)(var_0 + (-28L));
    var_8 = (uint32_t)var_6;
    *var_7 = var_8;
    rax_0_shrunk = 0U;
    if (var_8 == 0U) {
        return (uint64_t)rax_0_shrunk;
    }
    var_9 = (unsigned char **)var_2;
    rax_0_shrunk = 4294967295U;
    var_10 = (unsigned char **)var_4;
    rax_0_shrunk = 1U;
    var_11 = *var_3;
    *(uint64_t *)(var_0 + (-104L)) = 4248680UL;
    indirect_placeholder_2();
    rax_0_shrunk = 4294967295U;
    var_12 = *var_5;
    *(uint64_t *)(var_0 + (-112L)) = 4248711UL;
    indirect_placeholder_2();
    rax_0_shrunk = 1U;
    var_13 = *var_3;
    *(uint64_t *)(var_0 + (-120L)) = 4248742UL;
    indirect_placeholder_2();
    rax_0_shrunk = 4294967295U;
    var_14 = *var_5;
    *(uint64_t *)(var_0 + (-128L)) = 4248773UL;
    indirect_placeholder_2();
    rax_0_shrunk = 1U;
    if (~(**var_9 != '\x00' & **var_10 != '\x00' & (uint64_t)(uint32_t)var_11 != 0UL & (uint64_t)(uint32_t)var_12 != 0UL & (uint64_t)(uint32_t)var_13 != 0UL & (uint64_t)(uint32_t)var_14 != 0UL)) {
        return;
    }
    var_15 = (**var_9 == '.');
    rax_0_shrunk = 4294967295U;
    if (!var_15) {
        if (**var_10 == '.') {
            return (uint64_t)rax_0_shrunk;
        }
    }
    rax_0_shrunk = 1U;
    if (!var_15) {
        if (**var_10 == '.') {
            return (uint64_t)rax_0_shrunk;
        }
    }
    if (!var_15 && **var_10 == '.') {
        *var_3 = (*var_3 + 1UL);
        *var_5 = (*var_5 + 1UL);
    }
    var_16 = *var_3;
    var_17 = var_0 + (-64L);
    var_18 = (uint64_t *)var_17;
    *var_18 = var_16;
    var_19 = *var_5;
    var_20 = var_0 + (-72L);
    var_21 = (uint64_t *)var_20;
    *var_21 = var_19;
    *(uint64_t *)(var_0 + (-136L)) = 4248911UL;
    var_22 = indirect_placeholder_1(var_17);
    var_23 = var_0 + (-40L);
    var_24 = (uint64_t *)var_23;
    *var_24 = var_22;
    var_25 = var_0 + (-144L);
    *(uint64_t *)var_25 = 4248927UL;
    var_26 = indirect_placeholder_1(var_20);
    var_27 = var_0 + (-48L);
    var_28 = (uint64_t *)var_27;
    *var_28 = var_26;
    var_29 = *(uint64_t *)((*var_24 == 0UL) ? var_17 : var_23) - *var_3;
    var_30 = (uint64_t *)(var_0 + (-16L));
    *var_30 = var_29;
    var_31 = *(uint64_t *)((*var_28 == 0UL) ? var_20 : var_27) - *var_5;
    var_32 = (uint64_t *)(var_0 + (-24L));
    *var_32 = var_31;
    var_36 = var_31;
    local_sp_0 = var_25;
    if (*var_24 != 0UL) {
        if (*var_28 != 0UL) {
            var_37 = *var_5;
            var_38 = *var_30;
            var_39 = *var_3;
            *(uint64_t *)(local_sp_0 + (-8L)) = 4249116UL;
            var_40 = indirect_placeholder_8(var_37, var_36, var_39, var_38);
            var_41 = (uint32_t *)(var_0 + (-52L));
            var_42 = (uint32_t)var_40;
            *var_41 = var_42;
            rax_0_shrunk = var_42;
            if (var_42 == 0U) {
                var_43 = *var_7;
                rax_0_shrunk = var_43;
            }
            return (uint64_t)rax_0_shrunk;
        }
    }
    if (*var_30 != var_31) {
        var_33 = *var_3;
        var_34 = var_0 + (-152L);
        *(uint64_t *)var_34 = 4249046UL;
        indirect_placeholder_2();
        local_sp_0 = var_34;
        if ((uint64_t)(uint32_t)var_33 == 0UL) {
            *var_30 = (*var_18 - *var_3);
            var_35 = *var_21 - *var_5;
            *var_32 = var_35;
            var_36 = var_35;
        } else {
            _pre = *var_32;
            var_36 = _pre;
        }
    }
    var_37 = *var_5;
    var_38 = *var_30;
    var_39 = *var_3;
    *(uint64_t *)(local_sp_0 + (-8L)) = 4249116UL;
    var_40 = indirect_placeholder_8(var_37, var_36, var_39, var_38);
    var_41 = (uint32_t *)(var_0 + (-52L));
    var_42 = (uint32_t)var_40;
    *var_41 = var_42;
    rax_0_shrunk = var_42;
    if (var_42 == 0U) {
        var_43 = *var_7;
        rax_0_shrunk = var_43;
    }
}

typedef unsigned char __u_char;
typedef unsigned short __u_short;
typedef unsigned int __u_int;
typedef unsigned long __u_long;
typedef signed char __int8_t;
typedef unsigned char __uint8_t;
typedef short __int16_t;
typedef unsigned short __uint16_t;
typedef int __int32_t;
typedef unsigned int __uint32_t;
typedef long __int64_t;
typedef unsigned long __uint64_t;
typedef __int8_t __int_least8_t;
typedef __uint8_t __uint_least8_t;
typedef __int16_t __int_least16_t;
typedef __uint16_t __uint_least16_t;
typedef __int32_t __int_least32_t;
typedef __uint32_t __uint_least32_t;
typedef __int64_t __int_least64_t;
typedef __uint64_t __uint_least64_t;
typedef long __quad_t;
typedef unsigned long __u_quad_t;
typedef long __intmax_t;
typedef unsigned long __uintmax_t;
typedef unsigned long __dev_t;
typedef unsigned int __uid_t;
typedef unsigned int __gid_t;
typedef unsigned long __ino_t;
typedef unsigned long __ino64_t;
typedef unsigned int __mode_t;
typedef unsigned long __nlink_t;
typedef long __off_t;
typedef long __off64_t;
typedef int __pid_t;
typedef struct {
    int __val[2];
} __fsid_t;
typedef long __clock_t;
typedef unsigned long __rlim_t;
typedef unsigned long __rlim64_t;
typedef unsigned int __id_t;
typedef long __time_t;
typedef unsigned int __useconds_t;
typedef long __suseconds_t;
typedef int __daddr_t;
typedef int __key_t;
typedef int __clockid_t;
typedef void *__timer_t;
typedef long __blksize_t;
typedef long __blkcnt_t;
typedef long __blkcnt64_t;
typedef unsigned long __fsblkcnt_t;
typedef unsigned long __fsblkcnt64_t;
typedef unsigned long __fsfilcnt_t;
typedef unsigned long __fsfilcnt64_t;
typedef long __fsword_t;
typedef long __ssize_t;
typedef long __syscall_slong_t;
typedef unsigned long __syscall_ulong_t;
typedef __off64_t __loff_t;
typedef char *__caddr_t;
typedef long __intptr_t;
typedef unsigned int __socklen_t;
typedef int __sig_atomic_t;
typedef __int8_t int8_t;
typedef __int16_t int16_t;
typedef __int32_t int32_t;
typedef __int64_t int64_t;
typedef __uint8_t uint8_t;
typedef __uint16_t uint16_t;
typedef __uint32_t uint32_t;
typedef __uint64_t uint64_t;
typedef __int_least8_t int_least8_t;
typedef __int_least16_t int_least16_t;
typedef __int_least32_t int_least32_t;
typedef __int_least64_t int_least64_t;
typedef __uint_least8_t uint_least8_t;
typedef __uint_least16_t uint_least16_t;
typedef __uint_least32_t uint_least32_t;
typedef __uint_least64_t uint_least64_t;
typedef signed char int_fast8_t;
typedef long int_fast16_t;
typedef long int_fast32_t;
typedef long int_fast64_t;
typedef unsigned char uint_fast8_t;
typedef unsigned long uint_fast16_t;
typedef unsigned long uint_fast32_t;
typedef unsigned long uint_fast64_t;
typedef long intptr_t;
typedef unsigned long uintptr_t;
typedef __intmax_t intmax_t;
typedef __uintmax_t uintmax_t;
struct indirect_placeholder_16_ret_type;
struct indirect_placeholder_16_ret_type {
    uint64_t field_0;
    uint64_t field_1;
};
extern void abort(void);
extern uint64_t revng_init_local_sp(uint64_t param_0);
extern uint64_t init_rbp(void);
extern uint64_t indirect_placeholder_3(uint64_t param_0, uint64_t param_1);
extern void indirect_placeholder(void);
extern void indirect_placeholder_5(uint64_t param_0, uint64_t param_1);
extern uint64_t indirect_placeholder_2(void);
extern void indirect_placeholder_8(uint64_t param_0);
extern uint64_t indirect_placeholder_12(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint64_t param_3, uint64_t param_4, uint64_t param_5);
extern void indirect_placeholder_9(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint64_t param_3, uint64_t param_4, uint64_t param_5, uint64_t param_6);
extern struct indirect_placeholder_16_ret_type indirect_placeholder_16(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint64_t param_3, uint64_t param_4);
extern void indirect_placeholder_15(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint64_t param_3, uint64_t param_4, uint64_t param_5);
void bb_main(uint64_t rdi, uint64_t rsi) {
    struct indirect_placeholder_16_ret_type var_16;
    uint64_t local_sp_3_be;
    uint64_t var_23;
    uint64_t var_24;
    uint64_t var_13;
    uint64_t var_14;
    uint64_t var_15;
    uint32_t var_17;
    uint64_t var_0;
    uint64_t var_1;
    uint64_t var_2;
    uint32_t *var_3;
    uint64_t var_4;
    uint64_t *var_5;
    uint64_t *var_6;
    uint64_t var_7;
    uint64_t var_8;
    uint32_t *var_9;
    uint64_t var_10;
    unsigned char *var_11;
    unsigned char *var_12;
    uint64_t local_sp_3;
    uint64_t var_39;
    uint64_t var_25;
    uint64_t var_35;
    uint64_t var_36;
    uint64_t var_37;
    uint64_t local_sp_1;
    unsigned char *var_26;
    uint32_t var_27;
    uint32_t *var_28;
    uint32_t var_29;
    uint64_t local_sp_0;
    uint64_t local_sp_2;
    uint64_t var_38;
    uint64_t var_30;
    uint64_t var_31;
    uint64_t var_32;
    uint64_t var_33;
    uint32_t var_34;
    uint64_t var_20;
    uint64_t var_21;
    uint64_t var_18;
    uint64_t var_19;
    uint64_t var_22;
    unsigned int loop_state_var;
    var_0 = revng_init_local_sp(0UL);
    var_1 = init_rbp();
    var_2 = var_0 + (-8L);
    *(uint64_t *)var_2 = var_1;
    var_3 = (uint32_t *)(var_0 + (-44L));
    *var_3 = (uint32_t)rdi;
    var_4 = var_0 + (-56L);
    var_5 = (uint64_t *)var_4;
    *var_5 = rsi;
    var_6 = (uint64_t *)(var_0 + (-16L));
    *var_6 = 80UL;
    var_7 = **(uint64_t **)var_4;
    *(uint64_t *)(var_0 + (-64L)) = 4203426UL;
    indirect_placeholder_8(var_7);
    *(uint64_t *)(var_0 + (-72L)) = 4203441UL;
    indirect_placeholder();
    var_8 = var_0 + (-80L);
    *(uint64_t *)var_8 = 4203451UL;
    indirect_placeholder();
    *(unsigned char *)6379346UL = (unsigned char)'\x00';
    *(unsigned char *)6379345UL = (unsigned char)'\x00';
    *(unsigned char *)6379344UL = (unsigned char)'\x00';
    var_9 = (uint32_t *)(var_0 + (-28L));
    var_10 = var_0 + (-40L);
    var_11 = (unsigned char *)var_10;
    var_12 = (unsigned char *)(var_0 + (-39L));
    var_22 = var_10;
    local_sp_3 = var_8;
    while (1U)
        {
            var_13 = *var_5;
            var_14 = (uint64_t)*var_3;
            var_15 = local_sp_3 + (-8L);
            *(uint64_t *)var_15 = 4203787UL;
            var_16 = indirect_placeholder_16(4269376UL, 4269504UL, var_14, var_13, 0UL);
            var_17 = (uint32_t)var_16.field_0;
            *var_9 = var_17;
            local_sp_0 = var_15;
            local_sp_2 = var_15;
            local_sp_3_be = var_15;
            if (var_17 != 4294967295U) {
                var_25 = var_16.field_1;
                if ((uint64_t)(*var_3 - *(uint32_t *)6379160UL) != 0UL) {
                    var_26 = (unsigned char *)(var_0 + (-21L));
                    *var_26 = (unsigned char)'\x01';
                    var_27 = *(uint32_t *)6379160UL;
                    var_28 = (uint32_t *)(var_0 + (-20L));
                    *var_28 = var_27;
                    var_29 = var_27;
                    loop_state_var = 2U;
                    break;
                }
                var_35 = *var_6;
                var_36 = local_sp_3 + (-16L);
                *(uint64_t *)var_36 = 4203828UL;
                var_37 = indirect_placeholder_3(4270099UL, var_35);
                *(unsigned char *)(var_0 + (-21L)) = (unsigned char)var_37;
                local_sp_1 = var_36;
                loop_state_var = 1U;
                break;
            }
            if ((int)var_17 > (int)57U) {
                if ((uint64_t)(var_17 + (-115)) != 0UL) {
                    *(unsigned char *)6379344UL = (unsigned char)'\x01';
                    local_sp_3 = local_sp_3_be;
                    continue;
                }
                if ((uint64_t)(var_17 + (-119)) != 0UL) {
                    if ((uint64_t)(var_17 + (-98)) != 0UL) {
                        loop_state_var = 0U;
                        break;
                    }
                    *(unsigned char *)6379345UL = (unsigned char)'\x01';
                    local_sp_3 = local_sp_3_be;
                    continue;
                }
                var_22 = *(uint64_t *)6379800UL;
            } else {
                if ((int)var_17 >= (int)48U) {
                    if ((uint64_t)(var_17 + 131U) != 0UL) {
                        if ((uint64_t)(var_17 + 130U) != 0UL) {
                            loop_state_var = 0U;
                            break;
                        }
                        *(uint64_t *)(local_sp_3 + (-16L)) = 4203685UL;
                        indirect_placeholder_5(var_2, 0UL);
                        abort();
                    }
                    var_20 = *(uint64_t *)6379008UL;
                    *(uint64_t *)(local_sp_3 + (-16L)) = 4203737UL;
                    indirect_placeholder_15(0UL, 4269144UL, var_20, 4270094UL, 0UL, 4270132UL);
                    var_21 = local_sp_3 + (-24L);
                    *(uint64_t *)var_21 = 4203747UL;
                    indirect_placeholder();
                    local_sp_2 = var_21;
                    loop_state_var = 0U;
                    break;
                }
                var_18 = *(uint64_t *)6379800UL;
                if (var_18 == 0UL) {
                    *var_11 = (unsigned char)var_17;
                    *var_12 = (unsigned char)'\x00';
                    *(uint64_t *)6379800UL = var_10;
                } else {
                    var_19 = var_18 + (-1L);
                    *(uint64_t *)6379800UL = var_19;
                    var_22 = var_19;
                }
            }
            var_23 = local_sp_3 + (-16L);
            *(uint64_t *)var_23 = 4203669UL;
            var_24 = indirect_placeholder_12(18446744073709551606UL, 4269307UL, var_22, 1UL, 0UL, 4270106UL);
            *var_6 = var_24;
            local_sp_3_be = var_23;
            local_sp_3 = local_sp_3_be;
            continue;
        }
    switch (loop_state_var) {
      case 0U:
        {
            *(uint64_t *)(local_sp_2 + (-8L)) = 4203757UL;
            indirect_placeholder_5(var_2, 1UL);
            abort();
        }
        break;
      case 2U:
      case 1U:
        {
            switch (loop_state_var) {
              case 2U:
                {
                    local_sp_1 = local_sp_0;
                    while ((long)((uint64_t)var_29 << 32UL) >= (long)((uint64_t)*var_3 << 32UL))
                        {
                            var_30 = *(uint64_t *)(*var_5 + ((uint64_t)var_29 << 3UL));
                            var_31 = *var_6;
                            var_32 = local_sp_0 + (-8L);
                            *(uint64_t *)var_32 = 4203886UL;
                            var_33 = indirect_placeholder_3(var_30, var_31);
                            *var_26 = ((var_33 & (uint64_t)*var_26) != 0UL);
                            var_34 = *var_28 + 1U;
                            *var_28 = var_34;
                            var_29 = var_34;
                            local_sp_0 = var_32;
                            local_sp_1 = local_sp_0;
                        }
                }
                break;
              case 1U:
                {
                    *(uint64_t *)(local_sp_1 + (-8L)) = 4203941UL;
                    var_38 = indirect_placeholder_2();
                    if (*(unsigned char *)6379346UL != '\x00' & (uint64_t)((uint32_t)var_38 + 1U) == 0UL) {
                        *(uint64_t *)(local_sp_1 + (-16L)) = 4203951UL;
                        indirect_placeholder();
                        var_39 = (uint64_t)*(uint32_t *)var_38;
                        *(uint64_t *)(local_sp_1 + (-24L)) = 4203975UL;
                        indirect_placeholder_9(0UL, 4270099UL, 4269504UL, 1UL, var_39, var_25, 0UL);
                    }
                    return;
                }
                break;
            }
        }
        break;
    }
}

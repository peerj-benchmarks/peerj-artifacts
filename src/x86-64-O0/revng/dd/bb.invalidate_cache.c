typedef unsigned char __u_char;
typedef unsigned short __u_short;
typedef unsigned int __u_int;
typedef unsigned long __u_long;
typedef signed char __int8_t;
typedef unsigned char __uint8_t;
typedef short __int16_t;
typedef unsigned short __uint16_t;
typedef int __int32_t;
typedef unsigned int __uint32_t;
typedef long __int64_t;
typedef unsigned long __uint64_t;
typedef __int8_t __int_least8_t;
typedef __uint8_t __uint_least8_t;
typedef __int16_t __int_least16_t;
typedef __uint16_t __uint_least16_t;
typedef __int32_t __int_least32_t;
typedef __uint32_t __uint_least32_t;
typedef __int64_t __int_least64_t;
typedef __uint64_t __uint_least64_t;
typedef long __quad_t;
typedef unsigned long __u_quad_t;
typedef long __intmax_t;
typedef unsigned long __uintmax_t;
typedef unsigned long __dev_t;
typedef unsigned int __uid_t;
typedef unsigned int __gid_t;
typedef unsigned long __ino_t;
typedef unsigned long __ino64_t;
typedef unsigned int __mode_t;
typedef unsigned long __nlink_t;
typedef long __off_t;
typedef long __off64_t;
typedef int __pid_t;
typedef struct {
    int __val[2];
} __fsid_t;
typedef long __clock_t;
typedef unsigned long __rlim_t;
typedef unsigned long __rlim64_t;
typedef unsigned int __id_t;
typedef long __time_t;
typedef unsigned int __useconds_t;
typedef long __suseconds_t;
typedef int __daddr_t;
typedef int __key_t;
typedef int __clockid_t;
typedef void *__timer_t;
typedef long __blksize_t;
typedef long __blkcnt_t;
typedef long __blkcnt64_t;
typedef unsigned long __fsblkcnt_t;
typedef unsigned long __fsblkcnt64_t;
typedef unsigned long __fsfilcnt_t;
typedef unsigned long __fsfilcnt64_t;
typedef long __fsword_t;
typedef long __ssize_t;
typedef long __syscall_slong_t;
typedef unsigned long __syscall_ulong_t;
typedef __off64_t __loff_t;
typedef char *__caddr_t;
typedef long __intptr_t;
typedef unsigned int __socklen_t;
typedef int __sig_atomic_t;
typedef __int8_t int8_t;
typedef __int16_t int16_t;
typedef __int32_t int32_t;
typedef __int64_t int64_t;
typedef __uint8_t uint8_t;
typedef __uint16_t uint16_t;
typedef __uint32_t uint32_t;
typedef __uint64_t uint64_t;
typedef __int_least8_t int_least8_t;
typedef __int_least16_t int_least16_t;
typedef __int_least32_t int_least32_t;
typedef __int_least64_t int_least64_t;
typedef __uint_least8_t uint_least8_t;
typedef __uint_least16_t uint_least16_t;
typedef __uint_least32_t uint_least32_t;
typedef __uint_least64_t uint_least64_t;
typedef signed char int_fast8_t;
typedef long int_fast16_t;
typedef long int_fast32_t;
typedef long int_fast64_t;
typedef unsigned char uint_fast8_t;
typedef unsigned long uint_fast16_t;
typedef unsigned long uint_fast32_t;
typedef unsigned long uint_fast64_t;
typedef long intptr_t;
typedef unsigned long uintptr_t;
typedef __intmax_t intmax_t;
typedef __uintmax_t uintmax_t;
typedef _Bool bool;
struct bb_invalidate_cache_ret_type;
struct helper_divq_EAX_wrapper_ret_type;
struct type_6;
struct indirect_placeholder_18_ret_type;
struct indirect_placeholder_17_ret_type;
struct bb_invalidate_cache_ret_type {
    uint64_t field_0;
    uint64_t field_1;
    uint64_t field_2;
    uint64_t field_3;
};
struct helper_divq_EAX_wrapper_ret_type {
    uint64_t field_0;
    uint64_t field_1;
    uint64_t field_2;
    uint32_t field_3;
    uint64_t field_4;
    uint32_t field_5;
    uint64_t field_6;
    uint32_t field_7;
    uint64_t field_8;
    uint32_t field_9;
    unsigned char field_10;
    uint32_t field_11;
    uint32_t field_12;
};
struct type_6 {
};
struct indirect_placeholder_18_ret_type {
    uint64_t field_0;
    uint64_t field_1;
};
struct indirect_placeholder_17_ret_type {
    uint64_t field_0;
    uint64_t field_1;
};
extern uint64_t revng_init_local_sp(uint64_t param_0);
extern uint64_t init_rbp(void);
extern void indirect_placeholder(void);
extern uint64_t init_rbx(void);
extern uint64_t init_r10(void);
extern uint32_t init_state_0x8248(void);
extern uint64_t init_state_0x9018(void);
extern uint32_t init_state_0x9010(void);
extern uint64_t init_state_0x8408(void);
extern uint64_t init_state_0x8328(void);
extern uint64_t init_state_0x82d8(void);
extern uint32_t init_state_0x9080(void);
extern struct helper_divq_EAX_wrapper_ret_type helper_divq_EAX_wrapper(struct type_6 *param_0, uint64_t param_1, uint64_t param_2, uint64_t param_3, uint64_t param_4, uint64_t param_5, uint64_t param_6, uint64_t param_7, uint64_t param_8, uint64_t param_9, uint64_t param_10, uint64_t param_11, uint64_t param_12, uint32_t param_13, uint64_t param_14, uint32_t param_15, uint64_t param_16, uint64_t param_17, uint64_t param_18, uint32_t param_19);
extern uint64_t init_r9(void);
extern uint64_t init_r8(void);
extern uint64_t init_rcx(void);
extern struct indirect_placeholder_18_ret_type indirect_placeholder_18(uint64_t param_0, uint64_t param_1);
extern struct indirect_placeholder_17_ret_type indirect_placeholder_17(uint64_t param_0, uint64_t param_1);
struct bb_invalidate_cache_ret_type bb_invalidate_cache(uint64_t rsi, uint64_t rdi) {
    uint64_t var_0;
    uint64_t var_1;
    uint64_t var_2;
    uint64_t var_3;
    uint64_t var_4;
    uint64_t var_5;
    uint64_t var_6;
    uint32_t var_7;
    uint64_t var_8;
    uint32_t var_9;
    uint64_t var_10;
    uint64_t var_11;
    uint64_t var_12;
    uint32_t var_13;
    uint64_t var_14;
    uint32_t *var_15;
    uint64_t *var_16;
    uint32_t *var_17;
    bool storemerge_in;
    unsigned char *var_18;
    uint64_t var_19;
    uint64_t var_20;
    uint64_t var_21;
    struct indirect_placeholder_18_ret_type var_22;
    uint64_t var_23;
    uint64_t var_24;
    uint64_t *var_25;
    uint64_t var_26;
    bool var_27;
    bool var_28;
    uint64_t rcx_1;
    bool var_29;
    uint64_t rdi5_0;
    uint64_t var_30;
    uint64_t var_31;
    struct indirect_placeholder_17_ret_type var_32;
    uint64_t rax_0;
    uint64_t local_sp_0;
    uint64_t *var_33;
    uint32_t var_34;
    unsigned char var_41;
    uint64_t var_42;
    uint64_t var_43;
    uint64_t *var_44;
    uint64_t *_pre_phi59;
    uint64_t *var_45;
    uint64_t var_46;
    uint64_t var_35;
    uint64_t var_39;
    uint64_t rax_3;
    struct bb_invalidate_cache_ret_type mrv;
    struct bb_invalidate_cache_ret_type mrv1;
    struct bb_invalidate_cache_ret_type mrv2;
    uint64_t var_36;
    uint64_t var_37;
    uint64_t rdi5_1;
    uint64_t var_38;
    uint64_t local_sp_1;
    uint64_t *var_40;
    uint64_t rdi5_2;
    uint64_t rax_1;
    uint64_t local_sp_2;
    uint64_t var_47;
    uint32_t var_56;
    uint64_t var_48;
    uint64_t var_49;
    uint64_t var_50;
    uint64_t var_51;
    uint64_t var_52;
    struct helper_divq_EAX_wrapper_ret_type var_53;
    uint32_t var_54;
    uint64_t var_55;
    uint64_t rcx_0;
    uint64_t rax_2;
    uint64_t var_57;
    struct bb_invalidate_cache_ret_type mrv3;
    var_0 = revng_init_local_sp(0UL);
    var_1 = init_rbp();
    var_2 = init_rcx();
    var_3 = init_r10();
    var_4 = init_r9();
    var_5 = init_r8();
    var_6 = init_rbx();
    var_7 = init_state_0x8248();
    var_8 = init_state_0x9018();
    var_9 = init_state_0x9010();
    var_10 = init_state_0x8408();
    var_11 = init_state_0x8328();
    var_12 = init_state_0x82d8();
    var_13 = init_state_0x9080();
    var_14 = var_0 + (-8L);
    *(uint64_t *)var_14 = var_1;
    var_15 = (uint32_t *)(var_0 + (-60L));
    *var_15 = (uint32_t)rdi;
    var_16 = (uint64_t *)(var_0 + (-72L));
    *var_16 = rsi;
    var_17 = (uint32_t *)(var_0 + (-12L));
    *var_17 = 4294967295U;
    storemerge_in = (*(unsigned char *)(*var_15 == 0U) ? 6412234UL : 6412235UL != '\x00');
    var_18 = (unsigned char *)(var_0 + (-41L));
    *var_18 = storemerge_in;
    var_19 = *var_16;
    var_20 = (uint64_t)*var_15;
    var_21 = var_0 + (-80L);
    *(uint64_t *)var_21 = 4207173UL;
    var_22 = indirect_placeholder_18(var_19, var_20);
    var_23 = var_22.field_0;
    var_24 = var_22.field_1;
    var_25 = (uint64_t *)(var_0 + (-32L));
    *var_25 = var_23;
    var_26 = *var_16;
    var_27 = (var_26 != 0UL);
    var_28 = (var_23 == 0UL);
    rcx_1 = var_2;
    rdi5_0 = var_24;
    rax_0 = 0UL;
    local_sp_0 = var_21;
    var_39 = 18446744073709551615UL;
    rax_3 = 1UL;
    var_49 = 0UL;
    rcx_0 = 4UL;
    if (var_27 && var_28) {
        mrv.field_0 = rax_3;
        mrv1 = mrv;
        mrv1.field_1 = rcx_1;
        mrv2 = mrv1;
        mrv2.field_2 = var_4;
        mrv3 = mrv2;
        mrv3.field_3 = var_5;
        return mrv3;
    }
    var_29 = (var_26 == 0UL);
    if (!var_29) {
        if (!var_28 && *var_18 == '\x01') {
            mrv.field_0 = rax_3;
            mrv1 = mrv;
            mrv1.field_1 = rcx_1;
            mrv2 = mrv1;
            mrv2.field_2 = var_4;
            mrv3 = mrv2;
            mrv3.field_3 = var_5;
            return mrv3;
        }
    }
    if (var_29) {
        var_30 = (uint64_t)*var_15;
        var_31 = var_0 + (-88L);
        *(uint64_t *)var_31 = 4207258UL;
        var_32 = indirect_placeholder_17(0UL, var_30);
        rdi5_0 = var_32.field_1;
        rax_0 = var_32.field_0;
        local_sp_0 = var_31;
    }
    var_33 = (uint64_t *)(var_0 + (-40L));
    *var_33 = rax_0;
    var_34 = *var_15;
    rdi5_1 = rdi5_0;
    local_sp_1 = local_sp_0;
    rdi5_2 = rdi5_0;
    local_sp_2 = local_sp_0;
    if (var_34 == 0U) {
        var_41 = *(unsigned char *)6412012UL;
        var_42 = (uint64_t)var_41;
        rax_1 = var_42;
        if (var_41 == '\x00') {
            var_45 = (uint64_t *)(var_0 + (-24L));
            *var_45 = 18446744073709551615UL;
            var_46 = local_sp_0 + (-8L);
            *(uint64_t *)var_46 = 4207312UL;
            indirect_placeholder();
            *(uint32_t *)var_42 = 29U;
            _pre_phi59 = var_45;
            local_sp_2 = var_46;
        } else {
            var_43 = *(uint64_t *)6412024UL;
            var_44 = (uint64_t *)(var_0 + (-24L));
            *var_44 = var_43;
            _pre_phi59 = var_44;
            rax_1 = var_43;
        }
    } else {
        var_35 = *(uint64_t *)6411408UL;
        if (var_35 != 18446744073709551615UL) {
            var_39 = var_35;
            if ((long)var_35 > (long)18446744073709551615UL) {
                var_36 = (uint64_t)var_34;
                var_37 = local_sp_0 + (-8L);
                *(uint64_t *)var_37 = 4207365UL;
                indirect_placeholder();
                *(uint64_t *)6411408UL = var_36;
                var_39 = var_36;
                rdi5_1 = var_36;
                local_sp_1 = var_37;
            } else {
                if (*var_16 == 0UL) {
                    var_38 = var_35 + (*var_25 + rax_0);
                    *(uint64_t *)6411408UL = var_38;
                    var_39 = var_38;
                }
            }
        }
        var_40 = (uint64_t *)(var_0 + (-24L));
        *var_40 = var_39;
        _pre_phi59 = var_40;
        rdi5_2 = rdi5_1;
        rax_1 = var_39;
        local_sp_2 = local_sp_1;
    }
    var_47 = *_pre_phi59;
    var_50 = var_47;
    rax_2 = rax_1;
    if ((long)var_47 < (long)0UL) {
        var_56 = *var_17;
        rcx_0 = var_2;
    } else {
        if (*var_16 == 0UL) {
            var_49 = *var_25;
        } else {
            var_48 = *var_25;
            var_49 = var_48;
            if (var_48 != 0UL & *var_18 == '\x00') {
                *var_33 = var_48;
                *var_25 = 0UL;
                var_49 = 0UL;
                var_50 = *_pre_phi59;
            }
        }
        var_51 = (var_50 - var_49) - *var_33;
        *_pre_phi59 = var_51;
        if (*var_25 == 0UL) {
            var_52 = *(uint64_t *)6411856UL;
            var_53 = helper_divq_EAX_wrapper((struct type_6 *)(0UL), var_52, 4207506UL, var_51, var_14, 0UL, var_51, var_52, rdi5_2, var_3, var_4, var_5, var_6, var_7, var_8, var_9, var_10, var_11, var_12, var_13);
            *_pre_phi59 = (var_51 - var_53.field_4);
        }
        var_54 = *var_15;
        var_55 = (uint64_t)var_54;
        *(uint64_t *)(local_sp_2 + (-8L)) = 4207545UL;
        indirect_placeholder();
        *var_17 = var_54;
        var_56 = var_54;
        rax_2 = var_55;
    }
    var_57 = (rax_2 & (-256L)) | (var_56 != 4294967295U);
    rcx_1 = rcx_0;
    rax_3 = var_57;
}

typedef unsigned char __u_char;
typedef unsigned short __u_short;
typedef unsigned int __u_int;
typedef unsigned long __u_long;
typedef signed char __int8_t;
typedef unsigned char __uint8_t;
typedef short __int16_t;
typedef unsigned short __uint16_t;
typedef int __int32_t;
typedef unsigned int __uint32_t;
typedef long __int64_t;
typedef unsigned long __uint64_t;
typedef __int8_t __int_least8_t;
typedef __uint8_t __uint_least8_t;
typedef __int16_t __int_least16_t;
typedef __uint16_t __uint_least16_t;
typedef __int32_t __int_least32_t;
typedef __uint32_t __uint_least32_t;
typedef __int64_t __int_least64_t;
typedef __uint64_t __uint_least64_t;
typedef long __quad_t;
typedef unsigned long __u_quad_t;
typedef long __intmax_t;
typedef unsigned long __uintmax_t;
typedef unsigned long __dev_t;
typedef unsigned int __uid_t;
typedef unsigned int __gid_t;
typedef unsigned long __ino_t;
typedef unsigned long __ino64_t;
typedef unsigned int __mode_t;
typedef unsigned long __nlink_t;
typedef long __off_t;
typedef long __off64_t;
typedef int __pid_t;
typedef struct {
    int __val[2];
} __fsid_t;
typedef long __clock_t;
typedef unsigned long __rlim_t;
typedef unsigned long __rlim64_t;
typedef unsigned int __id_t;
typedef long __time_t;
typedef unsigned int __useconds_t;
typedef long __suseconds_t;
typedef int __daddr_t;
typedef int __key_t;
typedef int __clockid_t;
typedef void *__timer_t;
typedef long __blksize_t;
typedef long __blkcnt_t;
typedef long __blkcnt64_t;
typedef unsigned long __fsblkcnt_t;
typedef unsigned long __fsblkcnt64_t;
typedef unsigned long __fsfilcnt_t;
typedef unsigned long __fsfilcnt64_t;
typedef long __fsword_t;
typedef long __ssize_t;
typedef long __syscall_slong_t;
typedef unsigned long __syscall_ulong_t;
typedef __off64_t __loff_t;
typedef char *__caddr_t;
typedef long __intptr_t;
typedef unsigned int __socklen_t;
typedef int __sig_atomic_t;
typedef __int8_t int8_t;
typedef __int16_t int16_t;
typedef __int32_t int32_t;
typedef __int64_t int64_t;
typedef __uint8_t uint8_t;
typedef __uint16_t uint16_t;
typedef __uint32_t uint32_t;
typedef __uint64_t uint64_t;
typedef __int_least8_t int_least8_t;
typedef __int_least16_t int_least16_t;
typedef __int_least32_t int_least32_t;
typedef __int_least64_t int_least64_t;
typedef __uint_least8_t uint_least8_t;
typedef __uint_least16_t uint_least16_t;
typedef __uint_least32_t uint_least32_t;
typedef __uint_least64_t uint_least64_t;
typedef signed char int_fast8_t;
typedef long int_fast16_t;
typedef long int_fast32_t;
typedef long int_fast64_t;
typedef unsigned char uint_fast8_t;
typedef unsigned long uint_fast16_t;
typedef unsigned long uint_fast32_t;
typedef unsigned long uint_fast64_t;
typedef long intptr_t;
typedef unsigned long uintptr_t;
typedef __intmax_t intmax_t;
typedef __uintmax_t uintmax_t;
struct bb_print_xfer_stats_ret_type;
struct helper_pxor_xmm_wrapper_ret_type;
struct type_5;
struct type_7;
struct helper_cvtsq2sd_wrapper_ret_type;
struct indirect_placeholder_29_ret_type;
struct helper_addsd_wrapper_199_ret_type;
struct helper_divsd_wrapper_ret_type;
struct indirect_placeholder_30_ret_type;
struct indirect_placeholder_31_ret_type;
struct indirect_placeholder_33_ret_type;
struct indirect_placeholder_32_ret_type;
struct bb_print_xfer_stats_ret_type {
    uint64_t field_0;
    uint64_t field_1;
};
struct helper_pxor_xmm_wrapper_ret_type {
    uint64_t field_0;
    uint64_t field_1;
};
struct type_5 {
};
struct type_7 {
};
struct helper_cvtsq2sd_wrapper_ret_type {
    uint64_t field_0;
    unsigned char field_1;
};
struct indirect_placeholder_29_ret_type {
    uint64_t field_0;
    uint64_t field_1;
    uint64_t field_2;
};
struct helper_addsd_wrapper_199_ret_type {
    uint64_t field_0;
    unsigned char field_1;
};
struct helper_divsd_wrapper_ret_type {
    uint64_t field_0;
    unsigned char field_1;
};
struct indirect_placeholder_30_ret_type {
    uint64_t field_0;
    uint64_t field_1;
    uint64_t field_2;
};
struct indirect_placeholder_31_ret_type {
    uint64_t field_0;
    uint64_t field_1;
    uint64_t field_2;
};
struct indirect_placeholder_33_ret_type {
    uint64_t field_0;
    uint64_t field_1;
    uint64_t field_2;
};
struct indirect_placeholder_32_ret_type {
    uint64_t field_0;
    uint64_t field_1;
    uint64_t field_2;
};
extern uint64_t revng_init_local_sp(uint64_t param_0);
extern uint64_t init_rbp(void);
extern void indirect_placeholder(void);
extern uint64_t init_state_0x8558(void);
extern unsigned char init_state_0x8549(void);
extern unsigned char init_state_0x8548(void);
extern unsigned char init_state_0x854c(void);
extern unsigned char init_state_0x854b(void);
extern unsigned char init_state_0x8547(void);
extern unsigned char init_state_0x854d(void);
extern struct helper_pxor_xmm_wrapper_ret_type helper_pxor_xmm_wrapper(struct type_5 *param_0, struct type_7 *param_1, struct type_7 *param_2, uint64_t param_3, uint64_t param_4);
extern uint64_t init_state_0x8560(void);
extern struct helper_cvtsq2sd_wrapper_ret_type helper_cvtsq2sd_wrapper(struct type_5 *param_0, struct type_7 *param_1, uint64_t param_2, unsigned char param_3, unsigned char param_4, unsigned char param_5, unsigned char param_6);
extern uint64_t init_rbx(void);
extern uint64_t indirect_placeholder_6(void);
extern uint64_t indirect_placeholder_20(uint64_t param_0);
extern struct indirect_placeholder_29_ret_type indirect_placeholder_29(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint64_t param_3, uint64_t param_4, uint64_t param_5, uint64_t param_6);
extern struct helper_addsd_wrapper_199_ret_type helper_addsd_wrapper_199(struct type_5 *param_0, struct type_7 *param_1, struct type_7 *param_2, uint64_t param_3, unsigned char param_4, unsigned char param_5, unsigned char param_6, unsigned char param_7, unsigned char param_8, unsigned char param_9);
extern struct helper_divsd_wrapper_ret_type helper_divsd_wrapper(struct type_5 *param_0, struct type_7 *param_1, struct type_7 *param_2, uint64_t param_3, uint64_t param_4, unsigned char param_5, unsigned char param_6, unsigned char param_7, unsigned char param_8, unsigned char param_9, unsigned char param_10);
extern struct indirect_placeholder_30_ret_type indirect_placeholder_30(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint64_t param_3, uint64_t param_4);
extern struct indirect_placeholder_31_ret_type indirect_placeholder_31(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint64_t param_3, uint64_t param_4, uint64_t param_5, uint64_t param_6);
extern struct indirect_placeholder_33_ret_type indirect_placeholder_33(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint64_t param_3, uint64_t param_4);
extern struct indirect_placeholder_32_ret_type indirect_placeholder_32(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint64_t param_3, uint64_t param_4);
struct bb_print_xfer_stats_ret_type bb_print_xfer_stats(uint64_t rdi) {
    struct indirect_placeholder_32_ret_type var_20;
    struct indirect_placeholder_33_ret_type var_17;
    struct indirect_placeholder_30_ret_type var_46;
    struct indirect_placeholder_29_ret_type var_54;
    uint64_t var_0;
    uint64_t var_1;
    uint64_t var_2;
    uint64_t var_3;
    uint64_t var_4;
    unsigned char var_5;
    unsigned char var_6;
    unsigned char var_7;
    unsigned char var_8;
    unsigned char var_9;
    unsigned char var_10;
    uint64_t *var_11;
    uint64_t var_61;
    uint64_t var_62;
    uint64_t var_63;
    uint64_t r9_0;
    uint64_t r8_0;
    uint64_t local_sp_0;
    uint32_t *var_64;
    uint64_t local_sp_1;
    uint32_t var_65;
    struct bb_print_xfer_stats_ret_type mrv;
    struct bb_print_xfer_stats_ret_type mrv1;
    uint64_t var_57;
    uint64_t var_58;
    uint64_t var_59;
    uint64_t var_60;
    uint64_t state_0x8558_0;
    uint64_t r9_1;
    uint64_t r8_1;
    uint64_t var_51;
    uint64_t var_50;
    uint64_t local_sp_2;
    uint64_t spec_select;
    uint64_t var_52;
    uint64_t var_53;
    uint64_t var_55;
    uint64_t var_56;
    uint64_t *var_34;
    uint64_t var_35;
    uint64_t *var_36;
    uint64_t var_37;
    struct helper_cvtsq2sd_wrapper_ret_type var_38;
    uint64_t var_39;
    struct helper_cvtsq2sd_wrapper_ret_type var_40;
    struct helper_addsd_wrapper_199_ret_type var_41;
    unsigned char storemerge2;
    struct helper_divsd_wrapper_ret_type var_42;
    uint64_t var_43;
    uint64_t var_44;
    uint64_t var_45;
    uint64_t var_47;
    uint64_t var_48;
    uint64_t var_49;
    uint64_t var_26;
    uint64_t var_27;
    uint64_t var_28;
    uint64_t var_29;
    uint64_t var_30;
    struct indirect_placeholder_31_ret_type var_31;
    uint64_t var_32;
    uint64_t var_33;
    uint64_t var_12;
    uint64_t var_13;
    uint64_t rax_1;
    uint64_t local_sp_3;
    uint64_t *var_14;
    uint64_t var_15;
    uint64_t var_16;
    uint64_t var_18;
    uint64_t var_19;
    uint64_t var_21;
    uint64_t *var_22;
    uint64_t var_23;
    uint64_t *var_24;
    uint32_t *var_25;
    var_0 = revng_init_local_sp(0UL);
    var_1 = init_state_0x8558();
    var_2 = init_state_0x8560();
    var_3 = init_rbp();
    var_4 = init_rbx();
    var_5 = init_state_0x8549();
    var_6 = init_state_0x8548();
    var_7 = init_state_0x854c();
    var_8 = init_state_0x854b();
    var_9 = init_state_0x8547();
    var_10 = init_state_0x854d();
    *(uint64_t *)(var_0 + (-8L)) = var_3;
    *(uint64_t *)(var_0 + (-16L)) = var_4;
    var_11 = (uint64_t *)(var_0 + (-736L));
    *var_11 = rdi;
    var_65 = 0U;
    var_51 = 0UL;
    rax_1 = rdi;
    if (rdi == 0UL) {
        var_12 = var_0 + (-752L);
        *(uint64_t *)var_12 = 4205087UL;
        var_13 = indirect_placeholder_6();
        rax_1 = var_13;
        local_sp_3 = var_12;
    } else {
        local_sp_3 = var_0 + (-744L);
    }
    var_14 = (uint64_t *)(var_0 + (-48L));
    *var_14 = rax_1;
    var_15 = *(uint64_t *)6411984UL;
    var_16 = var_0 + (-696L);
    *(uint64_t *)(local_sp_3 + (-8L)) = 4205138UL;
    var_17 = indirect_placeholder_33(465UL, 1UL, var_16, var_15, 1UL);
    *(uint64_t *)(var_0 + (-56L)) = var_17.field_0;
    var_18 = *(uint64_t *)6411984UL;
    var_19 = var_0 + (-498L);
    *(uint64_t *)(local_sp_3 + (-16L)) = 4205187UL;
    var_20 = indirect_placeholder_32(497UL, 1UL, var_19, var_18, 1UL);
    var_21 = var_20.field_0;
    var_22 = (uint64_t *)(var_0 + (-64L));
    *var_22 = var_21;
    var_23 = var_0 + (-300L);
    var_24 = (uint64_t *)(var_0 + (-72L));
    *var_24 = var_23;
    var_25 = (uint32_t *)(var_0 + (-76L));
    *var_25 = 198U;
    if ((long)*(uint64_t *)6411992UL < (long)*var_14) {
        var_26 = var_20.field_2;
        var_27 = var_20.field_1;
        *(uint64_t *)(var_0 + (-32L)) = 0UL;
        var_28 = (uint64_t)*var_25;
        var_29 = *var_24;
        var_30 = local_sp_3 + (-24L);
        *(uint64_t *)var_30 = 4205445UL;
        var_31 = indirect_placeholder_31(0UL, 4295778UL, 4295769UL, var_28, var_29, var_27, var_26);
        var_32 = var_31.field_1;
        var_33 = var_31.field_2;
        *(uint64_t *)(var_0 + (-40L)) = *var_24;
        r9_1 = var_32;
        r8_1 = var_33;
        local_sp_1 = var_30;
    } else {
        var_34 = (uint64_t *)(var_0 + (-88L));
        *var_34 = 4741671816366391296UL;
        var_35 = *var_14;
        var_36 = (uint64_t *)(var_0 + (-96L));
        *var_36 = var_35;
        var_37 = var_35 - *(uint64_t *)6411992UL;
        *var_36 = var_37;
        if ((long)var_37 < (long)0UL) {
            var_39 = (var_37 >> 1UL) | (var_37 & 1UL);
            helper_pxor_xmm_wrapper((struct type_5 *)(0UL), (struct type_7 *)(776UL), (struct type_7 *)(776UL), var_1, var_2);
            var_40 = helper_cvtsq2sd_wrapper((struct type_5 *)(0UL), (struct type_7 *)(776UL), var_39, var_5, var_6, var_8, var_9);
            var_41 = helper_addsd_wrapper_199((struct type_5 *)(0UL), (struct type_7 *)(776UL), (struct type_7 *)(776UL), var_40.field_0, var_40.field_1, var_6, var_7, var_8, var_9, var_10);
            state_0x8558_0 = var_41.field_0;
            storemerge2 = var_41.field_1;
        } else {
            helper_pxor_xmm_wrapper((struct type_5 *)(0UL), (struct type_7 *)(776UL), (struct type_7 *)(776UL), var_1, var_2);
            var_38 = helper_cvtsq2sd_wrapper((struct type_5 *)(0UL), (struct type_7 *)(776UL), var_37, var_5, var_6, var_8, var_9);
            state_0x8558_0 = var_38.field_0;
            storemerge2 = var_38.field_1;
        }
        var_42 = helper_divsd_wrapper((struct type_5 *)(0UL), (struct type_7 *)(776UL), (struct type_7 *)(2824UL), state_0x8558_0, *var_34, storemerge2, var_6, var_7, var_8, var_9, var_10);
        *(uint64_t *)(var_0 + (-32L)) = var_42.field_0;
        var_43 = *(uint64_t *)6411984UL;
        var_44 = *var_36;
        var_45 = *var_24;
        *(uint64_t *)(local_sp_3 + (-24L)) = 4205360UL;
        var_46 = indirect_placeholder_30(465UL, 1000000000UL, var_45, var_43, var_44);
        var_47 = var_46.field_1;
        var_48 = var_46.field_2;
        *(uint64_t *)(var_0 + (-40L)) = var_46.field_0;
        var_49 = local_sp_3 + (-32L);
        *(uint64_t *)var_49 = 4205401UL;
        indirect_placeholder();
        r9_1 = var_47;
        r8_1 = var_48;
        local_sp_1 = var_49;
    }
    local_sp_2 = local_sp_1;
    if (*var_11 == 0UL) {
        var_50 = local_sp_1 + (-8L);
        *(uint64_t *)var_50 = 4205483UL;
        indirect_placeholder();
        var_51 = *var_11;
        local_sp_2 = var_50;
    }
    spec_select = (var_51 == 0UL) ? 4295792UL : 4295785UL;
    var_52 = *(uint64_t *)(var_0 + (-32L));
    var_53 = var_0 + (-728L);
    *(uint64_t *)(var_0 + (-744L)) = var_52;
    *(uint64_t *)(local_sp_2 + (-8L)) = 4205552UL;
    var_54 = indirect_placeholder_29(1UL, spec_select, spec_select, 24UL, var_53, r9_1, r8_1);
    var_55 = var_54.field_1;
    *(uint64_t *)(local_sp_2 + (-16L)) = 4205564UL;
    var_56 = indirect_placeholder_6();
    r9_0 = var_53;
    r8_0 = var_53;
    if ((uint64_t)(unsigned char)var_56 == 0UL) {
        var_57 = *(uint64_t *)6411984UL;
        *(uint64_t *)(local_sp_2 + (-24L)) = 4205590UL;
        indirect_placeholder_20(var_57);
        var_58 = *(uint64_t *)(var_0 + (-40L));
        var_59 = local_sp_2 + (-32L);
        *(uint64_t *)var_59 = 4205648UL;
        indirect_placeholder();
        r9_0 = var_55;
        r8_0 = var_58;
        local_sp_0 = var_59;
    } else {
        *(uint64_t *)(local_sp_2 + (-24L)) = 4205665UL;
        var_60 = indirect_placeholder_6();
        if ((uint64_t)(unsigned char)var_60 == 0UL) {
            var_63 = *var_22;
            *(uint64_t *)(local_sp_2 + (-40L)) = *(uint64_t *)(var_0 + (-40L));
            *(uint64_t *)(local_sp_2 + (-48L)) = 4205784UL;
            indirect_placeholder();
            r8_0 = var_63;
            local_sp_0 = local_sp_2 + (-32L);
        } else {
            var_61 = *(uint64_t *)(var_0 + (-40L));
            var_62 = local_sp_2 + (-32L);
            *(uint64_t *)var_62 = 4205722UL;
            indirect_placeholder();
            r9_0 = var_61;
            local_sp_0 = var_62;
        }
    }
    var_64 = (uint32_t *)(var_0 + (-100L));
    *var_64 = 0U;
    if (*var_11 == 0UL) {
        *(uint64_t *)(local_sp_0 + (-8L)) = 4205890UL;
        indirect_placeholder();
    } else {
        if ((int)*(uint32_t *)6412008UL > (int)0U) {
            *(uint64_t *)(local_sp_0 + (-8L)) = 4205859UL;
            indirect_placeholder();
            var_65 = *var_64;
        }
        *(uint32_t *)6412008UL = var_65;
    }
    mrv.field_0 = r9_0;
    mrv1 = mrv;
    mrv1.field_1 = r8_0;
    return mrv1;
}

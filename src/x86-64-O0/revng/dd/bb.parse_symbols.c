typedef unsigned char __u_char;
typedef unsigned short __u_short;
typedef unsigned int __u_int;
typedef unsigned long __u_long;
typedef signed char __int8_t;
typedef unsigned char __uint8_t;
typedef short __int16_t;
typedef unsigned short __uint16_t;
typedef int __int32_t;
typedef unsigned int __uint32_t;
typedef long __int64_t;
typedef unsigned long __uint64_t;
typedef __int8_t __int_least8_t;
typedef __uint8_t __uint_least8_t;
typedef __int16_t __int_least16_t;
typedef __uint16_t __uint_least16_t;
typedef __int32_t __int_least32_t;
typedef __uint32_t __uint_least32_t;
typedef __int64_t __int_least64_t;
typedef __uint64_t __uint_least64_t;
typedef long __quad_t;
typedef unsigned long __u_quad_t;
typedef long __intmax_t;
typedef unsigned long __uintmax_t;
typedef unsigned long __dev_t;
typedef unsigned int __uid_t;
typedef unsigned int __gid_t;
typedef unsigned long __ino_t;
typedef unsigned long __ino64_t;
typedef unsigned int __mode_t;
typedef unsigned long __nlink_t;
typedef long __off_t;
typedef long __off64_t;
typedef int __pid_t;
typedef struct {
    int __val[2];
} __fsid_t;
typedef long __clock_t;
typedef unsigned long __rlim_t;
typedef unsigned long __rlim64_t;
typedef unsigned int __id_t;
typedef long __time_t;
typedef unsigned int __useconds_t;
typedef long __suseconds_t;
typedef int __daddr_t;
typedef int __key_t;
typedef int __clockid_t;
typedef void *__timer_t;
typedef long __blksize_t;
typedef long __blkcnt_t;
typedef long __blkcnt64_t;
typedef unsigned long __fsblkcnt_t;
typedef unsigned long __fsblkcnt64_t;
typedef unsigned long __fsfilcnt_t;
typedef unsigned long __fsfilcnt64_t;
typedef long __fsword_t;
typedef long __ssize_t;
typedef long __syscall_slong_t;
typedef unsigned long __syscall_ulong_t;
typedef __off64_t __loff_t;
typedef char *__caddr_t;
typedef long __intptr_t;
typedef unsigned int __socklen_t;
typedef int __sig_atomic_t;
typedef __int8_t int8_t;
typedef __int16_t int16_t;
typedef __int32_t int32_t;
typedef __int64_t int64_t;
typedef __uint8_t uint8_t;
typedef __uint16_t uint16_t;
typedef __uint32_t uint32_t;
typedef __uint64_t uint64_t;
typedef __int_least8_t int_least8_t;
typedef __int_least16_t int_least16_t;
typedef __int_least32_t int_least32_t;
typedef __int_least64_t int_least64_t;
typedef __uint_least8_t uint_least8_t;
typedef __uint_least16_t uint_least16_t;
typedef __uint_least32_t uint_least32_t;
typedef __uint_least64_t uint_least64_t;
typedef signed char int_fast8_t;
typedef long int_fast16_t;
typedef long int_fast32_t;
typedef long int_fast64_t;
typedef unsigned char uint_fast8_t;
typedef unsigned long uint_fast16_t;
typedef unsigned long uint_fast32_t;
typedef unsigned long uint_fast64_t;
typedef long intptr_t;
typedef unsigned long uintptr_t;
typedef __intmax_t intmax_t;
typedef __uintmax_t uintmax_t;
typedef _Bool bool;
struct bb_parse_symbols_ret_type;
struct indirect_placeholder_67_ret_type;
struct indirect_placeholder_66_ret_type;
struct bb_parse_symbols_ret_type {
    uint64_t field_0;
    uint64_t field_1;
    uint64_t field_2;
    uint64_t field_3;
    uint64_t field_4;
    uint64_t field_5;
};
struct indirect_placeholder_67_ret_type {
    uint64_t field_0;
    uint64_t field_1;
};
struct indirect_placeholder_66_ret_type {
    uint64_t field_0;
    uint64_t field_1;
    uint64_t field_2;
    uint64_t field_3;
    uint64_t field_4;
    uint64_t field_5;
};
extern void abort(void);
extern uint64_t revng_init_local_sp(uint64_t param_0);
extern uint64_t init_rbp(void);
extern void indirect_placeholder(void);
extern uint64_t indirect_placeholder_8(uint64_t param_0, uint64_t param_1, uint64_t param_2);
extern uint64_t init_r10(void);
extern void indirect_placeholder_3(uint64_t param_0, uint64_t param_1);
extern uint64_t init_r9(void);
extern uint64_t init_r8(void);
extern struct indirect_placeholder_67_ret_type indirect_placeholder_67(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint64_t param_3);
extern struct indirect_placeholder_66_ret_type indirect_placeholder_66(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint64_t param_3, uint64_t param_4, uint64_t param_5, uint64_t param_6);
struct bb_parse_symbols_ret_type bb_parse_symbols(uint64_t rdx, uint64_t rcx, uint64_t rsi, uint64_t rdi) {
    uint32_t var_24;
    uint64_t var_0;
    uint64_t var_1;
    uint64_t var_2;
    uint64_t var_3;
    uint64_t var_4;
    uint64_t var_5;
    uint64_t var_6;
    uint64_t *var_7;
    uint64_t *var_8;
    uint64_t *var_9;
    unsigned char *var_10;
    uint32_t *var_11;
    uint64_t *var_12;
    uint64_t var_13;
    uint64_t *var_14;
    uint64_t var_15;
    uint32_t var_22;
    uint32_t var_23;
    uint64_t var_25;
    struct bb_parse_symbols_ret_type mrv;
    struct bb_parse_symbols_ret_type mrv1;
    struct bb_parse_symbols_ret_type mrv2;
    struct bb_parse_symbols_ret_type mrv3;
    struct bb_parse_symbols_ret_type mrv4;
    struct bb_parse_symbols_ret_type mrv5;
    uint64_t var_26;
    uint64_t var_28;
    bool var_29;
    uint64_t var_30;
    uint64_t rax_0;
    uint64_t local_sp_0;
    uint64_t var_32;
    struct indirect_placeholder_67_ret_type var_33;
    uint64_t var_34;
    uint64_t var_35;
    uint64_t var_36;
    uint64_t var_31;
    uint64_t var_27;
    uint64_t var_18;
    uint64_t local_sp_1;
    uint64_t var_19;
    uint64_t var_20;
    uint64_t var_21;
    uint64_t local_sp_2;
    uint64_t var_16;
    uint64_t var_17;
    unsigned int loop_state_var;
    var_0 = revng_init_local_sp(0UL);
    var_1 = init_rbp();
    var_2 = init_r10();
    var_3 = init_r9();
    var_4 = init_r8();
    var_5 = var_0 + (-8L);
    *(uint64_t *)var_5 = var_1;
    var_6 = var_0 + (-72L);
    var_7 = (uint64_t *)(var_0 + (-48L));
    *var_7 = rdi;
    var_8 = (uint64_t *)(var_0 + (-56L));
    *var_8 = rsi;
    var_9 = (uint64_t *)var_6;
    *var_9 = rcx;
    var_10 = (unsigned char *)(var_0 + (-60L));
    *var_10 = (unsigned char)rdx;
    var_11 = (uint32_t *)(var_0 + (-12L));
    *var_11 = 0U;
    var_12 = (uint64_t *)(var_0 + (-32L));
    var_13 = var_0 + (-24L);
    var_14 = (uint64_t *)var_13;
    var_15 = *var_7;
    local_sp_2 = var_6;
    while (1U)
        {
            var_16 = local_sp_2 + (-8L);
            *(uint64_t *)var_16 = 4208985UL;
            indirect_placeholder();
            *var_12 = var_15;
            var_17 = *var_8;
            *var_14 = var_17;
            var_18 = var_17;
            local_sp_1 = var_16;
            while (1U)
                {
                    var_19 = *var_7;
                    var_20 = local_sp_1 + (-8L);
                    *(uint64_t *)var_20 = 4209158UL;
                    var_21 = indirect_placeholder_8(44UL, var_18, var_19);
                    local_sp_0 = var_20;
                    local_sp_1 = var_20;
                    local_sp_2 = var_20;
                    if ((uint64_t)(unsigned char)var_21 != 1UL) {
                        var_22 = *(uint32_t *)(*var_14 + 12UL);
                        var_24 = var_22;
                        if (var_22 != 0U) {
                            loop_state_var = 1U;
                            break;
                        }
                    }
                    if (**(unsigned char **)var_13 == '\x00') {
                        var_27 = *var_14 + 16UL;
                        *var_14 = var_27;
                        var_18 = var_27;
                        continue;
                    }
                    var_28 = *var_12;
                    var_29 = (var_28 == 0UL);
                    var_30 = *var_7;
                    rax_0 = var_30;
                    if (!var_29) {
                        rax_0 = var_28 - var_30;
                        loop_state_var = 0U;
                        break;
                    }
                    var_31 = local_sp_1 + (-16L);
                    *(uint64_t *)var_31 = 4209048UL;
                    indirect_placeholder();
                    local_sp_0 = var_31;
                    loop_state_var = 0U;
                    break;
                }
            switch (loop_state_var) {
              case 0U:
                {
                    *(uint64_t *)(var_0 + (-40L)) = rax_0;
                    var_32 = *var_7;
                    *(uint64_t *)(local_sp_0 + (-8L)) = 4209081UL;
                    var_33 = indirect_placeholder_67(var_32, rax_0, 8UL, 0UL);
                    var_34 = var_33.field_0;
                    var_35 = var_33.field_1;
                    var_36 = *var_9;
                    *(uint64_t *)(local_sp_0 + (-16L)) = 4209119UL;
                    indirect_placeholder_66(0UL, 4296246UL, var_36, 0UL, 0UL, var_35, var_34);
                    *(uint64_t *)(local_sp_0 + (-24L)) = 4209129UL;
                    indirect_placeholder_3(var_5, 1UL);
                    abort();
                }
                break;
              case 1U:
                {
                    if (*var_10 == '\x00') {
                        var_23 = var_22 | *var_11;
                        *var_11 = var_23;
                        var_24 = var_23;
                    } else {
                        *var_11 = var_22;
                    }
                    var_25 = *var_12;
                    if (var_25 != 0UL) {
                        var_26 = var_25 + 1UL;
                        *var_7 = var_26;
                        var_15 = var_26;
                        continue;
                    }
                    mrv.field_0 = (uint64_t)var_24;
                    mrv1 = mrv;
                    mrv1.field_1 = var_18;
                    mrv2 = mrv1;
                    mrv2.field_2 = var_19;
                    mrv3 = mrv2;
                    mrv3.field_3 = var_2;
                    mrv4 = mrv3;
                    mrv4.field_4 = var_3;
                    mrv5 = mrv4;
                    mrv5.field_5 = var_4;
                    return mrv5;
                }
                break;
            }
        }
}

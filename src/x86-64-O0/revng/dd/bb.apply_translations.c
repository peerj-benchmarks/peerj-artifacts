typedef unsigned char __u_char;
typedef unsigned short __u_short;
typedef unsigned int __u_int;
typedef unsigned long __u_long;
typedef signed char __int8_t;
typedef unsigned char __uint8_t;
typedef short __int16_t;
typedef unsigned short __uint16_t;
typedef int __int32_t;
typedef unsigned int __uint32_t;
typedef long __int64_t;
typedef unsigned long __uint64_t;
typedef __int8_t __int_least8_t;
typedef __uint8_t __uint_least8_t;
typedef __int16_t __int_least16_t;
typedef __uint16_t __uint_least16_t;
typedef __int32_t __int_least32_t;
typedef __uint32_t __uint_least32_t;
typedef __int64_t __int_least64_t;
typedef __uint64_t __uint_least64_t;
typedef long __quad_t;
typedef unsigned long __u_quad_t;
typedef long __intmax_t;
typedef unsigned long __uintmax_t;
typedef unsigned long __dev_t;
typedef unsigned int __uid_t;
typedef unsigned int __gid_t;
typedef unsigned long __ino_t;
typedef unsigned long __ino64_t;
typedef unsigned int __mode_t;
typedef unsigned long __nlink_t;
typedef long __off_t;
typedef long __off64_t;
typedef int __pid_t;
typedef struct {
    int __val[2];
} __fsid_t;
typedef long __clock_t;
typedef unsigned long __rlim_t;
typedef unsigned long __rlim64_t;
typedef unsigned int __id_t;
typedef long __time_t;
typedef unsigned int __useconds_t;
typedef long __suseconds_t;
typedef int __daddr_t;
typedef int __key_t;
typedef int __clockid_t;
typedef void *__timer_t;
typedef long __blksize_t;
typedef long __blkcnt_t;
typedef long __blkcnt64_t;
typedef unsigned long __fsblkcnt_t;
typedef unsigned long __fsblkcnt64_t;
typedef unsigned long __fsfilcnt_t;
typedef unsigned long __fsfilcnt64_t;
typedef long __fsword_t;
typedef long __ssize_t;
typedef long __syscall_slong_t;
typedef unsigned long __syscall_ulong_t;
typedef __off64_t __loff_t;
typedef char *__caddr_t;
typedef long __intptr_t;
typedef unsigned int __socklen_t;
typedef int __sig_atomic_t;
typedef __int8_t int8_t;
typedef __int16_t int16_t;
typedef __int32_t int32_t;
typedef __int64_t int64_t;
typedef __uint8_t uint8_t;
typedef __uint16_t uint16_t;
typedef __uint32_t uint32_t;
typedef __uint64_t uint64_t;
typedef __int_least8_t int_least8_t;
typedef __int_least16_t int_least16_t;
typedef __int_least32_t int_least32_t;
typedef __int_least64_t int_least64_t;
typedef __uint_least8_t uint_least8_t;
typedef __uint_least16_t uint_least16_t;
typedef __uint_least32_t uint_least32_t;
typedef __uint_least64_t uint_least64_t;
typedef signed char int_fast8_t;
typedef long int_fast16_t;
typedef long int_fast32_t;
typedef long int_fast64_t;
typedef unsigned char uint_fast8_t;
typedef unsigned long uint_fast16_t;
typedef unsigned long uint_fast32_t;
typedef unsigned long uint_fast64_t;
typedef long intptr_t;
typedef unsigned long uintptr_t;
typedef __intmax_t intmax_t;
typedef __uintmax_t uintmax_t;
extern uint64_t revng_init_local_sp(uint64_t param_0);
extern uint64_t init_rbp(void);
extern void indirect_placeholder(void);
extern void indirect_placeholder_28(uint64_t param_0);
void bb_apply_translations(void) {
    uint64_t var_0;
    uint64_t var_1;
    uint64_t var_2;
    uint32_t var_3;
    uint32_t var_5;
    uint64_t var_4;
    uint64_t local_sp_0;
    uint32_t *var_6;
    uint32_t var_7;
    uint64_t local_sp_1;
    uint64_t local_sp_3;
    unsigned char var_8;
    uint64_t var_9;
    uint32_t var_10;
    uint32_t *var_11;
    uint32_t var_12;
    uint64_t local_sp_2;
    unsigned char var_13;
    uint64_t var_14;
    uint32_t var_15;
    uint32_t var_16;
    var_0 = revng_init_local_sp(0UL);
    var_1 = init_rbp();
    *(uint64_t *)(var_0 + (-8L)) = var_1;
    var_2 = var_0 + (-24L);
    var_3 = *(uint32_t *)6411936UL;
    var_5 = var_3;
    local_sp_0 = var_2;
    var_7 = 0U;
    var_12 = 0U;
    if ((var_3 & 1U) == 0U) {
        var_4 = var_0 + (-32L);
        *(uint64_t *)var_4 = 4212009UL;
        indirect_placeholder_28(4292096UL);
        var_5 = *(uint32_t *)6411936UL;
        local_sp_0 = var_4;
    }
    local_sp_1 = local_sp_0;
    local_sp_2 = local_sp_0;
    local_sp_3 = local_sp_0;
    if ((var_5 & 64U) == 0U) {
        if ((var_5 & 32U) != 0U) {
            var_11 = (uint32_t *)(var_0 + (-12L));
            *var_11 = 0U;
            local_sp_3 = local_sp_2;
            while ((int)var_12 <= (int)255U)
                {
                    var_13 = *(unsigned char *)((uint64_t)var_12 + 6412288UL);
                    var_14 = local_sp_2 + (-8L);
                    *(uint64_t *)var_14 = 4212132UL;
                    indirect_placeholder();
                    *(unsigned char *)((uint64_t)*var_11 + 6412288UL) = var_13;
                    var_15 = *var_11 + 1U;
                    *var_11 = var_15;
                    var_12 = var_15;
                    local_sp_2 = var_14;
                    local_sp_3 = local_sp_2;
                }
            *(unsigned char *)6411948UL = (unsigned char)'\x01';
        }
    } else {
        var_6 = (uint32_t *)(var_0 + (-12L));
        *var_6 = 0U;
        local_sp_3 = local_sp_1;
        while ((int)var_7 <= (int)255U)
            {
                var_8 = *(unsigned char *)((uint64_t)var_7 + 6412288UL);
                var_9 = local_sp_1 + (-8L);
                *(uint64_t *)var_9 = 4212053UL;
                indirect_placeholder();
                *(unsigned char *)((uint64_t)*var_6 + 6412288UL) = var_8;
                var_10 = *var_6 + 1U;
                *var_6 = var_10;
                var_7 = var_10;
                local_sp_1 = var_9;
                local_sp_3 = local_sp_1;
            }
        *(unsigned char *)6411948UL = (unsigned char)'\x01';
    }
    var_16 = *(uint32_t *)6411936UL;
    if ((var_16 & 2U) == 0U) {
        *(uint64_t *)(local_sp_3 + (-8L)) = 4212188UL;
        indirect_placeholder_28(4291584UL);
        *(unsigned char *)6411404UL = (unsigned char)'%';
        *(unsigned char *)6411405UL = (unsigned char)'@';
    } else {
        if ((var_16 & 4U) == 0U) {
            *(uint64_t *)(local_sp_3 + (-8L)) = 4212235UL;
            indirect_placeholder_28(4291840UL);
            *(unsigned char *)6411404UL = (unsigned char)'%';
            *(unsigned char *)6411405UL = (unsigned char)'@';
        }
    }
    return;
}

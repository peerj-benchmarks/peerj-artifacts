typedef unsigned char __u_char;
typedef unsigned short __u_short;
typedef unsigned int __u_int;
typedef unsigned long __u_long;
typedef signed char __int8_t;
typedef unsigned char __uint8_t;
typedef short __int16_t;
typedef unsigned short __uint16_t;
typedef int __int32_t;
typedef unsigned int __uint32_t;
typedef long __int64_t;
typedef unsigned long __uint64_t;
typedef __int8_t __int_least8_t;
typedef __uint8_t __uint_least8_t;
typedef __int16_t __int_least16_t;
typedef __uint16_t __uint_least16_t;
typedef __int32_t __int_least32_t;
typedef __uint32_t __uint_least32_t;
typedef __int64_t __int_least64_t;
typedef __uint64_t __uint_least64_t;
typedef long __quad_t;
typedef unsigned long __u_quad_t;
typedef long __intmax_t;
typedef unsigned long __uintmax_t;
typedef unsigned long __dev_t;
typedef unsigned int __uid_t;
typedef unsigned int __gid_t;
typedef unsigned long __ino_t;
typedef unsigned long __ino64_t;
typedef unsigned int __mode_t;
typedef unsigned long __nlink_t;
typedef long __off_t;
typedef long __off64_t;
typedef int __pid_t;
typedef struct {
    int __val[2];
} __fsid_t;
typedef long __clock_t;
typedef unsigned long __rlim_t;
typedef unsigned long __rlim64_t;
typedef unsigned int __id_t;
typedef long __time_t;
typedef unsigned int __useconds_t;
typedef long __suseconds_t;
typedef int __daddr_t;
typedef int __key_t;
typedef int __clockid_t;
typedef void *__timer_t;
typedef long __blksize_t;
typedef long __blkcnt_t;
typedef long __blkcnt64_t;
typedef unsigned long __fsblkcnt_t;
typedef unsigned long __fsblkcnt64_t;
typedef unsigned long __fsfilcnt_t;
typedef unsigned long __fsfilcnt64_t;
typedef long __fsword_t;
typedef long __ssize_t;
typedef long __syscall_slong_t;
typedef unsigned long __syscall_ulong_t;
typedef __off64_t __loff_t;
typedef char *__caddr_t;
typedef long __intptr_t;
typedef unsigned int __socklen_t;
typedef int __sig_atomic_t;
typedef __int8_t int8_t;
typedef __int16_t int16_t;
typedef __int32_t int32_t;
typedef __int64_t int64_t;
typedef __uint8_t uint8_t;
typedef __uint16_t uint16_t;
typedef __uint32_t uint32_t;
typedef __uint64_t uint64_t;
typedef __int_least8_t int_least8_t;
typedef __int_least16_t int_least16_t;
typedef __int_least32_t int_least32_t;
typedef __int_least64_t int_least64_t;
typedef __uint_least8_t uint_least8_t;
typedef __uint_least16_t uint_least16_t;
typedef __uint_least32_t uint_least32_t;
typedef __uint_least64_t uint_least64_t;
typedef signed char int_fast8_t;
typedef long int_fast16_t;
typedef long int_fast32_t;
typedef long int_fast64_t;
typedef unsigned char uint_fast8_t;
typedef unsigned long uint_fast16_t;
typedef unsigned long uint_fast32_t;
typedef unsigned long uint_fast64_t;
typedef long intptr_t;
typedef unsigned long uintptr_t;
typedef __intmax_t intmax_t;
typedef __uintmax_t uintmax_t;
struct indirect_placeholder_183_ret_type;
struct indirect_placeholder_184_ret_type;
struct indirect_placeholder_182_ret_type;
struct indirect_placeholder_185_ret_type;
struct indirect_placeholder_183_ret_type {
    uint64_t field_0;
    uint64_t field_1;
    uint64_t field_2;
    uint64_t field_3;
};
struct indirect_placeholder_184_ret_type {
    uint64_t field_0;
    uint64_t field_1;
    uint64_t field_2;
};
struct indirect_placeholder_182_ret_type {
    uint64_t field_0;
    uint64_t field_1;
    uint64_t field_2;
    uint64_t field_3;
    uint64_t field_4;
    uint64_t field_5;
};
struct indirect_placeholder_185_ret_type {
    uint64_t field_0;
    uint64_t field_1;
    uint64_t field_2;
    uint64_t field_3;
};
extern uint64_t revng_init_local_sp(uint64_t param_0);
extern uint64_t init_rbp(void);
extern void indirect_placeholder(void);
extern uint64_t init_rbx(void);
extern struct indirect_placeholder_183_ret_type indirect_placeholder_183(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint64_t param_3, uint64_t param_4, uint64_t param_5, uint64_t param_6);
extern struct indirect_placeholder_184_ret_type indirect_placeholder_184(uint64_t param_0, uint64_t param_1);
extern struct indirect_placeholder_182_ret_type indirect_placeholder_182(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint64_t param_3, uint64_t param_4, uint64_t param_5, uint64_t param_6);
extern struct indirect_placeholder_185_ret_type indirect_placeholder_185(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint64_t param_3, uint64_t param_4, uint64_t param_5, uint64_t param_6);
void bb_set_fd_flags(uint64_t rdx, uint64_t rcx, uint64_t rsi, uint64_t rdi, uint64_t r9, uint64_t r8) {
    uint64_t var_0;
    uint64_t var_1;
    uint64_t var_2;
    uint32_t *var_3;
    uint32_t *var_4;
    uint64_t *var_5;
    uint32_t var_6;
    struct indirect_placeholder_183_ret_type var_32;
    unsigned char var_33;
    uint64_t local_sp_0;
    uint32_t var_23;
    uint64_t var_24;
    uint64_t var_25;
    uint32_t var_26;
    uint32_t var_27;
    uint32_t var_20;
    uint32_t var_21;
    uint64_t var_22;
    unsigned char var_28;
    uint64_t local_sp_1;
    uint64_t var_29;
    uint64_t var_30;
    uint64_t var_31;
    uint64_t local_sp_2;
    uint64_t var_34;
    struct indirect_placeholder_184_ret_type var_35;
    uint64_t var_36;
    uint64_t var_37;
    uint64_t var_38;
    uint64_t var_39;
    uint64_t var_7;
    uint64_t var_8;
    struct indirect_placeholder_185_ret_type var_9;
    uint64_t var_10;
    uint64_t var_11;
    uint64_t var_12;
    uint64_t var_13;
    uint32_t *var_14;
    uint32_t var_15;
    uint32_t var_16;
    uint32_t *var_17;
    unsigned char *var_18;
    uint32_t var_19;
    var_0 = revng_init_local_sp(0UL);
    var_1 = init_rbp();
    var_2 = init_rbx();
    *(uint64_t *)(var_0 + (-8L)) = var_1;
    *(uint64_t *)(var_0 + (-16L)) = var_2;
    var_3 = (uint32_t *)(var_0 + (-188L));
    *var_3 = (uint32_t)rdi;
    var_4 = (uint32_t *)(var_0 + (-192L));
    *var_4 = (uint32_t)rsi;
    var_5 = (uint64_t *)(var_0 + (-200L));
    *var_5 = rdx;
    var_6 = *var_4 & (-131329);
    *var_4 = var_6;
    var_28 = (unsigned char)'\x01';
    var_33 = (unsigned char)'\x00';
    if (var_6 == 0U) {
        return;
    }
    var_7 = (uint64_t)*var_3;
    var_8 = var_0 + (-208L);
    *(uint64_t *)var_8 = 4215305UL;
    var_9 = indirect_placeholder_185(0UL, rdx, rcx, 3UL, var_7, r9, r8);
    var_10 = var_9.field_0;
    var_11 = var_9.field_1;
    var_12 = var_9.field_2;
    var_13 = var_9.field_3;
    var_14 = (uint32_t *)(var_0 + (-36L));
    var_15 = (uint32_t)var_10;
    *var_14 = var_15;
    var_16 = var_15 | *var_4;
    var_17 = (uint32_t *)(var_0 + (-28L));
    *var_17 = var_16;
    var_18 = (unsigned char *)(var_0 + (-29L));
    *var_18 = (unsigned char)'\x01';
    var_19 = *var_14;
    local_sp_1 = var_8;
    local_sp_2 = var_8;
    if ((int)var_19 > (int)4294967295U) {
        *var_18 = (unsigned char)'\x00';
    } else {
        var_20 = *var_17;
        var_27 = var_20;
        var_33 = (unsigned char)'\x01';
        if ((uint64_t)(var_19 - var_20) != 0UL) {
            var_33 = (unsigned char)'\x00';
            if ((var_20 & 65536U) != 0U) {
                var_21 = *var_3;
                var_22 = var_0 + (-216L);
                *(uint64_t *)var_22 = 4215386UL;
                indirect_placeholder();
                local_sp_0 = var_22;
                if (var_21 == 0U) {
                    *var_18 = (unsigned char)'\x00';
                } else {
                    var_23 = (uint32_t)((uint16_t)*(uint32_t *)(var_0 + (-160L)) & (unsigned short)61440U);
                    var_24 = (uint64_t)var_23;
                    if ((*var_17 & 65536U) != 0U & (uint64_t)((var_23 + (-16384)) & (-4096)) == 0UL) {
                        var_25 = var_0 + (-224L);
                        *(uint64_t *)var_25 = 4215431UL;
                        indirect_placeholder();
                        *(uint32_t *)var_24 = 20U;
                        *var_18 = (unsigned char)'\x00';
                        local_sp_0 = var_25;
                    }
                }
                var_26 = *var_17 & (-65537);
                *var_17 = var_26;
                var_27 = var_26;
                var_28 = *var_18;
                local_sp_1 = local_sp_0;
            }
            local_sp_2 = local_sp_1;
            var_29 = (uint64_t)var_27;
            var_33 = var_28;
            if (var_28 != '\x00' & (uint64_t)(*var_14 - var_27) != 0UL) {
                var_30 = (uint64_t)*var_3;
                var_31 = local_sp_1 + (-8L);
                *(uint64_t *)var_31 = 4215488UL;
                var_32 = indirect_placeholder_183(0UL, var_29, var_11, 4UL, var_30, var_12, var_13);
                var_33 = (unsigned char)'\x00';
                local_sp_2 = var_31;
                if ((uint64_t)((uint32_t)var_32.field_0 + 1U) == 0UL) {
                    *var_18 = (unsigned char)'\x00';
                } else {
                    var_33 = *var_18;
                }
            }
        }
    }
    if (var_33 != '\x01') {
        return;
    }
    var_34 = *var_5;
    *(uint64_t *)(local_sp_2 + (-8L)) = 4215528UL;
    var_35 = indirect_placeholder_184(var_34, 4UL);
    var_36 = var_35.field_0;
    var_37 = var_35.field_1;
    var_38 = var_35.field_2;
    *(uint64_t *)(local_sp_2 + (-16L)) = 4215536UL;
    indirect_placeholder();
    var_39 = (uint64_t)*(uint32_t *)var_36;
    *(uint64_t *)(local_sp_2 + (-24L)) = 4215563UL;
    indirect_placeholder_182(0UL, 4297072UL, var_36, var_39, 1UL, var_37, var_38);
    return;
}

typedef unsigned char __u_char;
typedef unsigned short __u_short;
typedef unsigned int __u_int;
typedef unsigned long __u_long;
typedef signed char __int8_t;
typedef unsigned char __uint8_t;
typedef short __int16_t;
typedef unsigned short __uint16_t;
typedef int __int32_t;
typedef unsigned int __uint32_t;
typedef long __int64_t;
typedef unsigned long __uint64_t;
typedef __int8_t __int_least8_t;
typedef __uint8_t __uint_least8_t;
typedef __int16_t __int_least16_t;
typedef __uint16_t __uint_least16_t;
typedef __int32_t __int_least32_t;
typedef __uint32_t __uint_least32_t;
typedef __int64_t __int_least64_t;
typedef __uint64_t __uint_least64_t;
typedef long __quad_t;
typedef unsigned long __u_quad_t;
typedef long __intmax_t;
typedef unsigned long __uintmax_t;
typedef unsigned long __dev_t;
typedef unsigned int __uid_t;
typedef unsigned int __gid_t;
typedef unsigned long __ino_t;
typedef unsigned long __ino64_t;
typedef unsigned int __mode_t;
typedef unsigned long __nlink_t;
typedef long __off_t;
typedef long __off64_t;
typedef int __pid_t;
typedef struct {
    int __val[2];
} __fsid_t;
typedef long __clock_t;
typedef unsigned long __rlim_t;
typedef unsigned long __rlim64_t;
typedef unsigned int __id_t;
typedef long __time_t;
typedef unsigned int __useconds_t;
typedef long __suseconds_t;
typedef int __daddr_t;
typedef int __key_t;
typedef int __clockid_t;
typedef void *__timer_t;
typedef long __blksize_t;
typedef long __blkcnt_t;
typedef long __blkcnt64_t;
typedef unsigned long __fsblkcnt_t;
typedef unsigned long __fsblkcnt64_t;
typedef unsigned long __fsfilcnt_t;
typedef unsigned long __fsfilcnt64_t;
typedef long __fsword_t;
typedef long __ssize_t;
typedef long __syscall_slong_t;
typedef unsigned long __syscall_ulong_t;
typedef __off64_t __loff_t;
typedef char *__caddr_t;
typedef long __intptr_t;
typedef unsigned int __socklen_t;
typedef int __sig_atomic_t;
typedef __int8_t int8_t;
typedef __int16_t int16_t;
typedef __int32_t int32_t;
typedef __int64_t int64_t;
typedef __uint8_t uint8_t;
typedef __uint16_t uint16_t;
typedef __uint32_t uint32_t;
typedef __uint64_t uint64_t;
typedef __int_least8_t int_least8_t;
typedef __int_least16_t int_least16_t;
typedef __int_least32_t int_least32_t;
typedef __int_least64_t int_least64_t;
typedef __uint_least8_t uint_least8_t;
typedef __uint_least16_t uint_least16_t;
typedef __uint_least32_t uint_least32_t;
typedef __uint_least64_t uint_least64_t;
typedef signed char int_fast8_t;
typedef long int_fast16_t;
typedef long int_fast32_t;
typedef long int_fast64_t;
typedef unsigned char uint_fast8_t;
typedef unsigned long uint_fast16_t;
typedef unsigned long uint_fast32_t;
typedef unsigned long uint_fast64_t;
typedef long intptr_t;
typedef unsigned long uintptr_t;
typedef __intmax_t intmax_t;
typedef __uintmax_t uintmax_t;
struct indirect_placeholder_9_ret_type;
struct indirect_placeholder_9_ret_type {
    uint64_t field_0;
    uint64_t field_1;
    uint64_t field_2;
};
extern uint64_t revng_init_local_sp(uint64_t param_0);
extern uint64_t init_rbp(void);
extern uint64_t indirect_placeholder_2(uint64_t param_0, uint64_t param_1);
extern void indirect_placeholder(void);
extern uint64_t indirect_placeholder_5(uint64_t param_0, uint64_t param_1, uint64_t param_2);
extern uint64_t init_rbx(void);
extern uint64_t init_r9(void);
extern uint64_t init_r8(void);
extern void indirect_placeholder_7(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint64_t param_3, uint64_t param_4, uint64_t param_5, uint64_t param_6);
extern struct indirect_placeholder_9_ret_type indirect_placeholder_9(uint64_t param_0, uint64_t param_1, uint64_t param_2);
typedef _Bool bool;
uint64_t bb_simple_cat(uint64_t rdi, uint64_t rsi) {
    uint64_t var_0;
    uint64_t var_1;
    uint64_t var_2;
    uint64_t var_3;
    uint64_t var_4;
    uint64_t var_5;
    uint64_t *var_6;
    uint64_t *var_7;
    uint64_t *var_8;
    uint64_t *var_9;
    uint64_t var_10;
    uint64_t var_23;
    uint64_t var_24;
    uint64_t local_sp_0;
    uint64_t var_14;
    struct indirect_placeholder_9_ret_type var_15;
    uint64_t var_16;
    uint64_t var_17;
    uint64_t var_18;
    uint64_t var_19;
    uint64_t storemerge;
    uint64_t var_20;
    uint64_t var_21;
    uint64_t var_22;
    uint64_t local_sp_1;
    uint64_t var_11;
    uint64_t var_12;
    uint64_t var_13;
    bool switch_state_var;
    var_0 = revng_init_local_sp(0UL);
    var_1 = init_rbp();
    var_2 = init_r9();
    var_3 = init_r8();
    var_4 = init_rbx();
    *(uint64_t *)(var_0 + (-8L)) = var_1;
    *(uint64_t *)(var_0 + (-16L)) = var_4;
    var_5 = var_0 + (-56L);
    var_6 = (uint64_t *)(var_0 + (-48L));
    *var_6 = rdi;
    var_7 = (uint64_t *)var_5;
    *var_7 = rsi;
    var_8 = (uint64_t *)(var_0 + (-32L));
    var_9 = (uint64_t *)(var_0 + (-40L));
    storemerge = 1UL;
    var_10 = rsi;
    local_sp_1 = var_5;
    while (1U)
        {
            var_11 = (uint64_t)*(uint32_t *)6379928UL;
            var_12 = *var_6;
            *(uint64_t *)(local_sp_1 + (-8L)) = 4202497UL;
            var_13 = indirect_placeholder_2(var_10, var_11);
            *var_8 = var_13;
            switch_state_var = 0;
            switch (var_13) {
              case 0UL:
                {
                    switch_state_var = 1;
                    break;
                }
                break;
              case 18446744073709551615UL:
                {
                    var_14 = *(uint64_t *)6379920UL;
                    *(uint64_t *)(local_sp_1 + (-16L)) = 4202533UL;
                    var_15 = indirect_placeholder_9(var_14, 0UL, 3UL);
                    var_16 = var_15.field_0;
                    var_17 = var_15.field_1;
                    var_18 = var_15.field_2;
                    *(uint64_t *)(local_sp_1 + (-24L)) = 4202541UL;
                    indirect_placeholder();
                    var_19 = (uint64_t)*(uint32_t *)var_16;
                    *(uint64_t *)(local_sp_1 + (-32L)) = 4202568UL;
                    indirect_placeholder_7(0UL, 4270925UL, var_16, 0UL, var_19, var_17, var_18);
                    storemerge = 0UL;
                    switch_state_var = 1;
                    break;
                }
                break;
              default:
                {
                    *var_9 = var_13;
                    var_20 = *var_6;
                    var_21 = local_sp_1 + (-16L);
                    *(uint64_t *)var_21 = 4202618UL;
                    var_22 = indirect_placeholder_5(var_13, 1UL, var_20);
                    local_sp_0 = var_21;
                    if (var_22 == *var_9) {
                        *(uint64_t *)(local_sp_1 + (-24L)) = 4202629UL;
                        indirect_placeholder();
                        var_23 = (uint64_t)*(uint32_t *)var_22;
                        var_24 = local_sp_1 + (-32L);
                        *(uint64_t *)var_24 = 4202653UL;
                        indirect_placeholder_7(0UL, 4270928UL, var_12, 1UL, var_23, var_2, var_3);
                        local_sp_0 = var_24;
                    }
                    var_10 = *var_7;
                    local_sp_1 = local_sp_0;
                    continue;
                }
                break;
            }
            if (switch_state_var)
                break;
        }
    return storemerge;
}

typedef unsigned char __u_char;
typedef unsigned short __u_short;
typedef unsigned int __u_int;
typedef unsigned long __u_long;
typedef signed char __int8_t;
typedef unsigned char __uint8_t;
typedef short __int16_t;
typedef unsigned short __uint16_t;
typedef int __int32_t;
typedef unsigned int __uint32_t;
typedef long __int64_t;
typedef unsigned long __uint64_t;
typedef __int8_t __int_least8_t;
typedef __uint8_t __uint_least8_t;
typedef __int16_t __int_least16_t;
typedef __uint16_t __uint_least16_t;
typedef __int32_t __int_least32_t;
typedef __uint32_t __uint_least32_t;
typedef __int64_t __int_least64_t;
typedef __uint64_t __uint_least64_t;
typedef long __quad_t;
typedef unsigned long __u_quad_t;
typedef long __intmax_t;
typedef unsigned long __uintmax_t;
typedef unsigned long __dev_t;
typedef unsigned int __uid_t;
typedef unsigned int __gid_t;
typedef unsigned long __ino_t;
typedef unsigned long __ino64_t;
typedef unsigned int __mode_t;
typedef unsigned long __nlink_t;
typedef long __off_t;
typedef long __off64_t;
typedef int __pid_t;
typedef struct {
    int __val[2];
} __fsid_t;
typedef long __clock_t;
typedef unsigned long __rlim_t;
typedef unsigned long __rlim64_t;
typedef unsigned int __id_t;
typedef long __time_t;
typedef unsigned int __useconds_t;
typedef long __suseconds_t;
typedef int __daddr_t;
typedef int __key_t;
typedef int __clockid_t;
typedef void *__timer_t;
typedef long __blksize_t;
typedef long __blkcnt_t;
typedef long __blkcnt64_t;
typedef unsigned long __fsblkcnt_t;
typedef unsigned long __fsblkcnt64_t;
typedef unsigned long __fsfilcnt_t;
typedef unsigned long __fsfilcnt64_t;
typedef long __fsword_t;
typedef long __ssize_t;
typedef long __syscall_slong_t;
typedef unsigned long __syscall_ulong_t;
typedef __off64_t __loff_t;
typedef char *__caddr_t;
typedef long __intptr_t;
typedef unsigned int __socklen_t;
typedef int __sig_atomic_t;
typedef __int8_t int8_t;
typedef __int16_t int16_t;
typedef __int32_t int32_t;
typedef __int64_t int64_t;
typedef __uint8_t uint8_t;
typedef __uint16_t uint16_t;
typedef __uint32_t uint32_t;
typedef __uint64_t uint64_t;
typedef __int_least8_t int_least8_t;
typedef __int_least16_t int_least16_t;
typedef __int_least32_t int_least32_t;
typedef __int_least64_t int_least64_t;
typedef __uint_least8_t uint_least8_t;
typedef __uint_least16_t uint_least16_t;
typedef __uint_least32_t uint_least32_t;
typedef __uint_least64_t uint_least64_t;
typedef signed char int_fast8_t;
typedef long int_fast16_t;
typedef long int_fast32_t;
typedef long int_fast64_t;
typedef unsigned char uint_fast8_t;
typedef unsigned long uint_fast16_t;
typedef unsigned long uint_fast32_t;
typedef unsigned long uint_fast64_t;
typedef long intptr_t;
typedef unsigned long uintptr_t;
typedef __intmax_t intmax_t;
typedef __uintmax_t uintmax_t;
extern uint64_t revng_init_local_sp(uint64_t param_0);
extern uint64_t init_rbp(void);
extern uint64_t indirect_placeholder_1(uint64_t param_0, uint64_t param_1);
extern uint64_t init_cc_src2(void);
extern uint64_t helper_cc_compute_c_wrapper(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint32_t param_3);
extern void indirect_placeholder(void);
extern uint64_t indirect_placeholder_18(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint64_t param_3);
uint64_t bb_mgetgroups(uint64_t rdx, uint64_t rdi, uint64_t rsi) {
    uint64_t var_0;
    uint64_t var_1;
    uint64_t var_2;
    uint64_t *var_3;
    uint32_t *var_4;
    uint64_t var_5;
    uint64_t rax_0;
    uint64_t local_sp_0;
    uint32_t *var_49;
    uint32_t var_50;
    uint32_t var_51;
    uint32_t *var_52;
    uint32_t *var_53;
    uint64_t rax_2;
    uint32_t var_54;
    uint32_t var_55;
    uint32_t var_70;
    uint32_t **var_56;
    uint32_t var_57;
    uint32_t *var_58;
    uint64_t var_59;
    uint64_t *var_60;
    uint64_t var_61;
    uint64_t var_62;
    uint64_t *var_63;
    uint64_t var_64;
    uint64_t var_65;
    uint64_t var_66;
    uint32_t **var_67;
    uint32_t var_68;
    uint64_t var_69;
    uint64_t var_39;
    uint64_t var_40;
    uint64_t var_41;
    uint64_t var_42;
    uint64_t var_43;
    uint32_t var_44;
    uint64_t var_45;
    uint64_t var_46;
    uint64_t var_47;
    uint64_t var_48;
    uint64_t var_31;
    uint64_t var_32;
    uint64_t var_30;
    uint32_t var_34;
    uint32_t var_33;
    uint64_t var_35;
    uint64_t var_36;
    uint64_t var_37;
    uint64_t *var_38;
    uint32_t var_23;
    uint32_t *var_24;
    uint32_t *var_25;
    uint64_t local_sp_2;
    uint32_t var_16;
    uint32_t var_17;
    uint32_t var_18;
    uint64_t var_19;
    uint64_t var_20;
    uint64_t var_21;
    uint64_t var_22;
    uint32_t *var_11;
    uint32_t *var_12;
    uint64_t *var_13;
    uint64_t var_14;
    uint32_t var_15;
    uint32_t *var_6;
    uint64_t var_7;
    uint64_t var_8;
    uint64_t var_9;
    uint64_t *var_10;
    uint64_t var_26;
    uint64_t var_27;
    uint32_t *var_28;
    uint32_t var_29;
    var_0 = revng_init_local_sp(0UL);
    var_1 = init_rbp();
    var_2 = init_cc_src2();
    *(uint64_t *)(var_0 + (-8L)) = var_1;
    var_3 = (uint64_t *)(var_0 + (-96L));
    *var_3 = rdi;
    var_4 = (uint32_t *)(var_0 + (-100L));
    *var_4 = (uint32_t)rsi;
    var_5 = var_0 + (-112L);
    *(uint64_t *)var_5 = rdx;
    rax_2 = 4294967295UL;
    if (*var_3 != 0UL) {
        var_6 = (uint32_t *)(var_0 + (-76L));
        *var_6 = 10U;
        var_7 = var_0 + (-128L);
        *(uint64_t *)var_7 = 4234458UL;
        var_8 = indirect_placeholder_1(0UL, 10UL);
        var_9 = var_0 + (-24L);
        var_10 = (uint64_t *)var_9;
        *var_10 = var_8;
        local_sp_2 = var_7;
        if (var_8 != 0UL) {
            var_11 = (uint32_t *)(var_0 + (-36L));
            var_12 = (uint32_t *)(var_0 + (-12L));
            var_13 = (uint64_t *)(var_0 + (-48L));
            while (1U)
                {
                    *var_11 = *var_6;
                    var_14 = *var_3;
                    *(uint64_t *)(local_sp_2 + (-8L)) = 4234508UL;
                    indirect_placeholder();
                    var_15 = (uint32_t)var_14;
                    *var_12 = var_15;
                    if ((int)var_15 > (int)4294967295U) {
                        var_18 = *var_6;
                    } else {
                        var_16 = *var_6;
                        var_18 = var_16;
                        if ((uint64_t)(*var_11 - var_16) == 0UL) {
                            var_17 = var_16 << 1U;
                            *var_6 = var_17;
                            var_18 = var_17;
                        }
                    }
                    var_19 = (uint64_t)var_18;
                    var_20 = *var_10;
                    var_21 = local_sp_2 + (-16L);
                    *(uint64_t *)var_21 = 4234554UL;
                    var_22 = indirect_placeholder_1(var_20, var_19);
                    *var_13 = var_22;
                    local_sp_2 = var_21;
                    if (var_22 == 0UL) {
                        *var_10 = var_22;
                        if ((int)*var_12 < (int)0U) {
                            continue;
                        }
                        **(uint64_t **)var_5 = var_22;
                        rax_2 = (uint64_t)*var_6;
                        break;
                    }
                    *(uint64_t *)(local_sp_2 + (-24L)) = 4234570UL;
                    indirect_placeholder();
                    var_23 = *(volatile uint32_t *)(uint32_t *)0UL;
                    var_24 = (uint32_t *)(var_0 + (-52L));
                    *var_24 = var_23;
                    var_25 = *(uint32_t **)var_9;
                    *(uint64_t *)(local_sp_2 + (-32L)) = 4234587UL;
                    indirect_placeholder();
                    *(uint64_t *)(local_sp_2 + (-40L)) = 4234592UL;
                    indirect_placeholder();
                    *var_25 = *var_24;
                    break;
                }
        }
    }
    var_26 = var_0 + (-128L);
    *(uint64_t *)var_26 = 4234699UL;
    var_27 = indirect_placeholder_1(0UL, 0UL);
    var_28 = (uint32_t *)(var_0 + (-76L));
    var_29 = (uint32_t)var_27;
    *var_28 = var_29;
    var_34 = var_29;
    if ((int)var_29 > (int)4294967295U) {
        if (var_29 == 0U) {
            var_33 = var_29 + 1U;
            *var_28 = var_33;
            var_34 = var_33;
        } else {
            if (*var_3 != 0UL & *var_4 == 4294967295U) {
                var_33 = var_29 + 1U;
                *var_28 = var_33;
                var_34 = var_33;
            }
        }
        var_35 = (uint64_t)var_34;
        *(uint64_t *)(var_26 + (-8L)) = 4234839UL;
        var_36 = indirect_placeholder_1(0UL, var_35);
        var_37 = var_0 + (-24L);
        var_38 = (uint64_t *)var_37;
        *var_38 = var_36;
        if (var_36 != 0UL) {
            var_39 = *var_3;
            if (var_39 == 0UL) {
                var_44 = *var_4;
                var_45 = ((var_44 == 4294967295U) ? 0UL : 4UL) + var_36;
                var_46 = (uint64_t)(*var_28 + (uint32_t)(var_44 != 4294967295U));
                var_47 = var_26 + (-16L);
                *(uint64_t *)var_47 = 4234942UL;
                var_48 = indirect_placeholder_1(var_46, var_45);
                rax_0 = var_48;
                local_sp_0 = var_47;
            } else {
                var_40 = (uint64_t)*var_28;
                var_41 = (uint64_t)*var_4;
                var_42 = var_26 + (-16L);
                *(uint64_t *)var_42 = 4234888UL;
                var_43 = indirect_placeholder_18(var_39, var_41, var_40, var_36);
                rax_0 = var_43;
                local_sp_0 = var_42;
            }
            var_49 = (uint32_t *)(var_0 + (-12L));
            var_50 = (uint32_t)rax_0;
            *var_49 = var_50;
            if ((int)var_50 > (int)4294967295U) {
                *(uint64_t *)(local_sp_0 + (-8L)) = 4234956UL;
                indirect_placeholder();
                var_51 = *(uint32_t *)rax_0;
                var_52 = (uint32_t *)(var_0 + (-56L));
                *var_52 = var_51;
                var_53 = *(uint32_t **)var_37;
                *(uint64_t *)(local_sp_0 + (-16L)) = 4234973UL;
                indirect_placeholder();
                *(uint64_t *)(local_sp_0 + (-24L)) = 4234978UL;
                indirect_placeholder();
                *var_53 = *var_52;
            } else {
                var_54 = *var_4;
                if (*var_3 != 0UL & var_54 == 4294967295U) {
                    **(uint32_t **)var_37 = var_54;
                    *var_49 = (*var_49 + 1U);
                }
                **(uint64_t **)var_5 = *var_38;
                var_55 = *var_49;
                var_70 = var_55;
                if ((int)var_55 <= (int)1U) {
                    var_56 = (uint32_t **)var_37;
                    var_57 = **var_56;
                    var_58 = (uint32_t *)(var_0 + (-60L));
                    *var_58 = var_57;
                    var_59 = *var_38 + ((uint64_t)*var_49 << 2UL);
                    var_60 = (uint64_t *)(var_0 + (-72L));
                    *var_60 = var_59;
                    var_61 = *var_38 + 4UL;
                    var_62 = var_0 + (-32L);
                    var_63 = (uint64_t *)var_62;
                    *var_63 = var_61;
                    var_64 = var_61;
                    var_65 = *var_60;
                    var_66 = helper_cc_compute_c_wrapper(var_64 - var_65, var_65, var_2, 17U);
                    while (var_66 != 0UL)
                        {
                            var_67 = (uint32_t **)var_62;
                            var_68 = **var_67;
                            if ((uint64_t)(var_68 - *var_58) == 0UL) {
                                *var_49 = (*var_49 + (-1));
                            } else {
                                if ((uint64_t)(var_68 - **var_56) == 0UL) {
                                    *var_49 = (*var_49 + (-1));
                                } else {
                                    *var_38 = (*var_38 + 4UL);
                                    **var_56 = **var_67;
                                }
                            }
                            var_69 = *var_63 + 4UL;
                            *var_63 = var_69;
                            var_64 = var_69;
                            var_65 = *var_60;
                            var_66 = helper_cc_compute_c_wrapper(var_64 - var_65, var_65, var_2, 17U);
                        }
                    var_70 = *var_49;
                }
                rax_2 = (uint64_t)var_70;
            }
        }
    } else {
        var_30 = (uint64_t)var_29;
        *(uint64_t *)(var_26 + (-8L)) = 4234714UL;
        indirect_placeholder();
        *(uint64_t *)(var_26 + (-16L)) = 4234736UL;
        var_31 = indirect_placeholder_1(0UL, 1UL);
        var_32 = var_0 + (-24L);
        *(uint64_t *)var_32 = var_31;
        if (*(uint32_t *)var_30 != 38U & var_31 == 0UL) {
            **(uint64_t **)var_5 = var_31;
            **(uint32_t **)var_32 = *var_4;
            rax_2 = (*var_4 != 4294967295U);
        }
    }
    return rax_2;
}

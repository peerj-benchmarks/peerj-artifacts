typedef unsigned char __u_char;
typedef unsigned short __u_short;
typedef unsigned int __u_int;
typedef unsigned long __u_long;
typedef signed char __int8_t;
typedef unsigned char __uint8_t;
typedef short __int16_t;
typedef unsigned short __uint16_t;
typedef int __int32_t;
typedef unsigned int __uint32_t;
typedef long __int64_t;
typedef unsigned long __uint64_t;
typedef __int8_t __int_least8_t;
typedef __uint8_t __uint_least8_t;
typedef __int16_t __int_least16_t;
typedef __uint16_t __uint_least16_t;
typedef __int32_t __int_least32_t;
typedef __uint32_t __uint_least32_t;
typedef __int64_t __int_least64_t;
typedef __uint64_t __uint_least64_t;
typedef long __quad_t;
typedef unsigned long __u_quad_t;
typedef long __intmax_t;
typedef unsigned long __uintmax_t;
typedef unsigned long __dev_t;
typedef unsigned int __uid_t;
typedef unsigned int __gid_t;
typedef unsigned long __ino_t;
typedef unsigned long __ino64_t;
typedef unsigned int __mode_t;
typedef unsigned long __nlink_t;
typedef long __off_t;
typedef long __off64_t;
typedef int __pid_t;
typedef struct {
    int __val[2];
} __fsid_t;
typedef long __clock_t;
typedef unsigned long __rlim_t;
typedef unsigned long __rlim64_t;
typedef unsigned int __id_t;
typedef long __time_t;
typedef unsigned int __useconds_t;
typedef long __suseconds_t;
typedef int __daddr_t;
typedef int __key_t;
typedef int __clockid_t;
typedef void *__timer_t;
typedef long __blksize_t;
typedef long __blkcnt_t;
typedef long __blkcnt64_t;
typedef unsigned long __fsblkcnt_t;
typedef unsigned long __fsblkcnt64_t;
typedef unsigned long __fsfilcnt_t;
typedef unsigned long __fsfilcnt64_t;
typedef long __fsword_t;
typedef long __ssize_t;
typedef long __syscall_slong_t;
typedef unsigned long __syscall_ulong_t;
typedef __off64_t __loff_t;
typedef char *__caddr_t;
typedef long __intptr_t;
typedef unsigned int __socklen_t;
typedef int __sig_atomic_t;
typedef __int8_t int8_t;
typedef __int16_t int16_t;
typedef __int32_t int32_t;
typedef __int64_t int64_t;
typedef __uint8_t uint8_t;
typedef __uint16_t uint16_t;
typedef __uint32_t uint32_t;
typedef __uint64_t uint64_t;
typedef __int_least8_t int_least8_t;
typedef __int_least16_t int_least16_t;
typedef __int_least32_t int_least32_t;
typedef __int_least64_t int_least64_t;
typedef __uint_least8_t uint_least8_t;
typedef __uint_least16_t uint_least16_t;
typedef __uint_least32_t uint_least32_t;
typedef __uint_least64_t uint_least64_t;
typedef signed char int_fast8_t;
typedef long int_fast16_t;
typedef long int_fast32_t;
typedef long int_fast64_t;
typedef unsigned char uint_fast8_t;
typedef unsigned long uint_fast16_t;
typedef unsigned long uint_fast32_t;
typedef unsigned long uint_fast64_t;
typedef long intptr_t;
typedef unsigned long uintptr_t;
typedef __intmax_t intmax_t;
typedef __uintmax_t uintmax_t;
struct indirect_placeholder_37_ret_type;
struct indirect_placeholder_38_ret_type;
struct indirect_placeholder_39_ret_type;
struct indirect_placeholder_40_ret_type;
struct indirect_placeholder_42_ret_type;
struct indirect_placeholder_37_ret_type {
    uint64_t field_0;
    uint64_t field_1;
    uint64_t field_2;
};
struct indirect_placeholder_38_ret_type {
    uint64_t field_0;
    uint64_t field_1;
    uint64_t field_2;
};
struct indirect_placeholder_39_ret_type {
    uint64_t field_0;
    uint64_t field_1;
    uint64_t field_2;
};
struct indirect_placeholder_40_ret_type {
    uint64_t field_0;
    uint64_t field_1;
    uint64_t field_2;
};
struct indirect_placeholder_42_ret_type {
    uint64_t field_0;
    uint64_t field_1;
};
extern void abort(void);
extern uint64_t revng_init_local_sp(uint64_t param_0);
extern uint64_t init_rbp(void);
extern void indirect_placeholder_1(void);
extern uint64_t indirect_placeholder_5(uint64_t param_0, uint64_t param_1, uint64_t param_2);
extern uint64_t init_rbx(void);
extern void indirect_placeholder_2(uint64_t param_0, uint64_t param_1);
extern uint64_t indirect_placeholder_13(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint64_t param_3, uint64_t param_4, uint64_t param_5);
extern void indirect_placeholder_6(uint64_t param_0);
extern uint64_t indirect_placeholder_16(uint64_t param_0);
extern void indirect_placeholder_41(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint64_t param_3, uint64_t param_4, uint64_t param_5);
extern struct indirect_placeholder_37_ret_type indirect_placeholder_37(uint64_t param_0);
extern uint64_t indirect_placeholder_3(void);
extern uint64_t init_r12(void);
extern void indirect_placeholder_32(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint64_t param_3, uint64_t param_4, uint64_t param_5, uint64_t param_6);
extern struct indirect_placeholder_38_ret_type indirect_placeholder_38(uint64_t param_0);
extern struct indirect_placeholder_39_ret_type indirect_placeholder_39(uint64_t param_0);
extern struct indirect_placeholder_40_ret_type indirect_placeholder_40(uint64_t param_0);
extern struct indirect_placeholder_42_ret_type indirect_placeholder_42(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint64_t param_3, uint64_t param_4);
void bb_main(uint64_t rsi, uint64_t rdi) {
    uint64_t var_80;
    uint64_t var_81;
    uint64_t var_82;
    uint64_t var_83;
    uint64_t var_84;
    uint64_t var_85;
    uint32_t var_86;
    uint32_t var_87;
    struct indirect_placeholder_37_ret_type var_51;
    uint64_t var_52;
    uint64_t var_53;
    uint64_t var_54;
    uint64_t var_55;
    uint64_t var_48;
    uint64_t var_49;
    uint64_t local_sp_6;
    uint64_t local_sp_7_be;
    uint64_t var_46;
    struct indirect_placeholder_42_ret_type var_15;
    uint64_t var_0;
    uint64_t var_1;
    uint64_t var_2;
    uint64_t var_3;
    uint64_t var_4;
    uint32_t *var_5;
    uint64_t var_6;
    uint64_t *var_7;
    unsigned char *var_8;
    uint64_t var_9;
    uint64_t var_10;
    uint32_t *var_11;
    uint64_t local_sp_7;
    uint64_t var_97;
    uint64_t var_94;
    uint64_t var_95;
    uint64_t local_sp_1;
    uint32_t var_88;
    uint32_t var_89;
    uint64_t local_sp_0;
    uint64_t var_50;
    uint64_t var_96;
    uint64_t var_90;
    uint64_t var_91;
    uint64_t var_92;
    uint32_t var_93;
    uint64_t var_68;
    uint64_t var_69;
    uint64_t *var_70;
    uint64_t var_71;
    uint64_t var_72;
    uint64_t var_73;
    uint64_t var_74;
    uint64_t var_75;
    uint64_t var_76;
    uint64_t var_77;
    uint64_t var_78;
    uint64_t var_79;
    uint64_t var_47;
    uint64_t var_30;
    struct indirect_placeholder_38_ret_type var_31;
    uint64_t var_32;
    uint64_t var_33;
    uint64_t var_34;
    uint64_t var_35;
    uint64_t var_38;
    struct indirect_placeholder_39_ret_type var_39;
    uint64_t var_40;
    uint64_t var_41;
    uint64_t var_42;
    uint64_t var_43;
    uint64_t var_22;
    struct indirect_placeholder_40_ret_type var_23;
    uint64_t var_24;
    uint64_t var_25;
    uint64_t var_26;
    uint64_t var_27;
    uint64_t var_17;
    uint64_t var_18;
    uint64_t *var_19;
    uint64_t var_20;
    uint64_t var_21;
    uint64_t var_28;
    uint64_t var_29;
    uint64_t var_36;
    uint64_t var_37;
    uint64_t var_44;
    uint64_t var_45;
    uint64_t var_56;
    uint64_t var_57;
    uint64_t var_58;
    uint64_t var_59;
    uint64_t var_60;
    uint64_t var_61;
    uint64_t var_62;
    uint64_t var_63;
    uint64_t var_64;
    uint64_t var_65;
    uint64_t var_66;
    uint64_t var_67;
    uint64_t var_12;
    uint64_t var_13;
    uint64_t var_14;
    uint32_t var_16;
    unsigned int loop_state_var;
    var_0 = revng_init_local_sp(0UL);
    var_1 = init_rbp();
    var_2 = init_rbx();
    var_3 = init_r12();
    var_4 = var_0 + (-8L);
    *(uint64_t *)var_4 = var_1;
    *(uint64_t *)(var_0 + (-16L)) = var_3;
    *(uint64_t *)(var_0 + (-24L)) = var_2;
    var_5 = (uint32_t *)(var_0 + (-44L));
    *var_5 = (uint32_t)rdi;
    var_6 = var_0 + (-56L);
    var_7 = (uint64_t *)var_6;
    *var_7 = rsi;
    var_8 = (unsigned char *)(var_0 + (-25L));
    *var_8 = (unsigned char)'\x01';
    var_9 = **(uint64_t **)var_6;
    *(uint64_t *)(var_0 + (-64L)) = 4205296UL;
    indirect_placeholder_6(var_9);
    *(uint64_t *)(var_0 + (-72L)) = 4205311UL;
    indirect_placeholder_1();
    var_10 = var_0 + (-80L);
    *(uint64_t *)var_10 = 4205321UL;
    indirect_placeholder_1();
    *(unsigned char *)6482664UL = (unsigned char)'\x00';
    var_11 = (uint32_t *)(var_0 + (-32L));
    local_sp_7 = var_10;
    while (1U)
        {
            var_12 = *var_7;
            var_13 = (uint64_t)*var_5;
            var_14 = local_sp_7 + (-8L);
            *(uint64_t *)var_14 = 4206284UL;
            var_15 = indirect_placeholder_42(4361176UL, 4358720UL, var_12, var_13, 0UL);
            var_16 = (uint32_t)var_15.field_0;
            *var_11 = var_16;
            local_sp_7_be = var_14;
            local_sp_6 = var_14;
            if (var_16 != 4294967295U) {
                var_68 = var_15.field_1;
                if (*var_8 == '\x01') {
                    *(uint64_t *)(local_sp_7 + (-16L)) = 4206318UL;
                    indirect_placeholder_2(var_4, 1UL);
                    abort();
                }
                var_69 = *(uint64_t *)6481056UL;
                *(uint64_t *)(local_sp_7 + (-16L)) = 4206333UL;
                indirect_placeholder_1();
                var_70 = (uint64_t *)(var_0 + (-40L));
                *var_70 = var_69;
                var_71 = var_69 * 3UL;
                *(uint64_t *)6482576UL = var_71;
                var_72 = var_71 + 1UL;
                *(uint64_t *)(local_sp_7 + (-24L)) = 4206376UL;
                var_73 = indirect_placeholder_16(var_72);
                *(uint64_t *)6482568UL = var_73;
                *(uint64_t *)(local_sp_7 + (-32L)) = 4206422UL;
                indirect_placeholder_1();
                *(uint64_t *)(local_sp_7 + (-40L)) = 4206433UL;
                indirect_placeholder_1();
                *(uint64_t *)(local_sp_7 + (-48L)) = 4206444UL;
                indirect_placeholder_1();
                var_74 = *var_70 << 1UL;
                *(uint64_t *)6482592UL = var_74;
                var_75 = var_74 | 1UL;
                *(uint64_t *)(local_sp_7 + (-56L)) = 4206477UL;
                var_76 = indirect_placeholder_16(var_75);
                *(uint64_t *)6482584UL = var_76;
                *(uint64_t *)(local_sp_7 + (-64L)) = 4206516UL;
                indirect_placeholder_1();
                *(uint64_t *)(local_sp_7 + (-72L)) = 4206527UL;
                indirect_placeholder_1();
                var_77 = *var_70;
                *(uint64_t *)6482608UL = var_77;
                var_78 = var_77 + 1UL;
                *(uint64_t *)(local_sp_7 + (-80L)) = 4206557UL;
                var_79 = indirect_placeholder_16(var_78);
                *(uint64_t *)6482600UL = var_79;
                *(uint64_t *)(local_sp_7 + (-88L)) = 4206589UL;
                indirect_placeholder_1();
                *(uint64_t *)(local_sp_7 + (-96L)) = 4206599UL;
                indirect_placeholder_1();
                var_80 = *(uint64_t *)6481048UL;
                *(uint64_t *)(local_sp_7 + (-104L)) = 4206614UL;
                indirect_placeholder_1();
                *var_70 = var_80;
                var_81 = (var_80 + (uint64_t)*(uint32_t *)6481096UL) + 1UL;
                *(uint64_t *)(local_sp_7 + (-112L)) = 4206646UL;
                var_82 = indirect_placeholder_16(var_81);
                *(uint64_t *)6482648UL = var_82;
                var_83 = local_sp_7 + (-120L);
                *(uint64_t *)var_83 = 4206689UL;
                indirect_placeholder_1();
                var_84 = *(uint64_t *)6482648UL;
                var_85 = (uint64_t)*(uint32_t *)6481096UL;
                *(unsigned char *)(var_84 + (*var_70 + var_85)) = (unsigned char)'\x00';
                *(uint64_t *)6482656UL = *(uint64_t *)6481064UL;
                *(uint64_t *)6481536UL = *(uint64_t *)6481024UL;
                *(uint64_t *)6482560UL = 6481600UL;
                var_86 = *(uint32_t *)6481272UL;
                var_87 = *var_5;
                var_88 = var_87;
                var_89 = var_86;
                local_sp_0 = var_83;
                if ((uint64_t)(var_86 - var_87) != 0UL) {
                    loop_state_var = 1U;
                    break;
                }
                var_94 = local_sp_7 + (-128L);
                *(uint64_t *)var_94 = 4206778UL;
                var_95 = indirect_placeholder_16(4360843UL);
                *var_8 = (unsigned char)var_95;
                local_sp_1 = var_94;
                loop_state_var = 0U;
                break;
            }
            if ((uint64_t)(var_16 + (-105)) == 0UL) {
                var_65 = *(uint64_t *)6483160UL;
                var_66 = local_sp_7 + (-16L);
                *(uint64_t *)var_66 = 4205836UL;
                var_67 = indirect_placeholder_13(9223372036854775807UL, 4358395UL, 1UL, var_65, 4360992UL, 0UL);
                *(uint64_t *)6481072UL = var_67;
                local_sp_7_be = var_66;
            } else {
                if ((int)var_16 > (int)105U) {
                    if ((uint64_t)(var_16 + (-112)) != 0UL) {
                        *(unsigned char *)6481080UL = (unsigned char)'\x00';
                        local_sp_7 = local_sp_7_be;
                        continue;
                    }
                    if ((int)var_16 > (int)112U) {
                        if ((uint64_t)(var_16 + (-118)) != 0UL) {
                            var_62 = *(uint64_t *)6483160UL;
                            var_63 = local_sp_7 + (-16L);
                            *(uint64_t *)var_63 = 4205777UL;
                            var_64 = indirect_placeholder_13(9223372036854775807UL, 4358395UL, 9223372036854775808UL, var_62, 4360963UL, 0UL);
                            *(uint64_t *)6481064UL = var_64;
                            local_sp_7_be = var_63;
                            local_sp_7 = local_sp_7_be;
                            continue;
                        }
                        if ((uint64_t)(var_16 + (-119)) != 0UL) {
                            var_59 = *(uint64_t *)6483160UL;
                            var_60 = local_sp_7 + (-16L);
                            *(uint64_t *)var_60 = 4205980UL;
                            var_61 = indirect_placeholder_13(2147483647UL, 4358395UL, 1UL, var_59, 4361064UL, 0UL);
                            *(uint32_t *)6481096UL = (uint32_t)var_61;
                            local_sp_7_be = var_60;
                            local_sp_7 = local_sp_7_be;
                            continue;
                        }
                        if ((uint64_t)(var_16 + (-115)) != 0UL) {
                            *(uint64_t *)6481048UL = *(uint64_t *)6483160UL;
                            local_sp_7 = local_sp_7_be;
                            continue;
                        }
                    }
                    if ((uint64_t)(var_16 + (-108)) != 0UL) {
                        var_56 = *(uint64_t *)6483160UL;
                        var_57 = local_sp_7 + (-16L);
                        *(uint64_t *)var_57 = 4205907UL;
                        var_58 = indirect_placeholder_13(9223372036854775807UL, 4358395UL, 1UL, var_56, 4361024UL, 0UL);
                        *(uint64_t *)6481088UL = var_58;
                        local_sp_7_be = var_57;
                        local_sp_7 = local_sp_7_be;
                        continue;
                    }
                    if ((uint64_t)(var_16 + (-110)) != 0UL) {
                        var_44 = *(uint64_t *)6483160UL;
                        var_45 = local_sp_7 + (-16L);
                        *(uint64_t *)var_45 = 4206011UL;
                        indirect_placeholder_1();
                        local_sp_7_be = var_45;
                        if ((uint64_t)(uint32_t)var_44 == 0UL) {
                            *(uint64_t *)6481104UL = 4358466UL;
                        } else {
                            var_46 = *(uint64_t *)6483160UL;
                            var_47 = local_sp_7 + (-24L);
                            *(uint64_t *)var_47 = 4206048UL;
                            indirect_placeholder_1();
                            local_sp_7_be = var_47;
                            if ((uint64_t)(uint32_t)var_46 == 0UL) {
                                *(uint64_t *)6481104UL = 4358451UL;
                            } else {
                                var_48 = *(uint64_t *)6483160UL;
                                var_49 = local_sp_7 + (-32L);
                                *(uint64_t *)var_49 = 4206085UL;
                                indirect_placeholder_1();
                                local_sp_7_be = var_49;
                                if ((uint64_t)(uint32_t)var_48 == 0UL) {
                                    *(uint64_t *)6481104UL = 4358458UL;
                                } else {
                                    var_50 = *(uint64_t *)6483160UL;
                                    *(uint64_t *)(local_sp_7 + (-40L)) = 4206117UL;
                                    var_51 = indirect_placeholder_37(var_50);
                                    var_52 = var_51.field_0;
                                    var_53 = var_51.field_1;
                                    var_54 = var_51.field_2;
                                    var_55 = local_sp_7 + (-48L);
                                    *(uint64_t *)var_55 = 4206145UL;
                                    indirect_placeholder_32(0UL, 4361112UL, var_52, 0UL, 0UL, var_53, var_54);
                                    *var_8 = (unsigned char)'\x00';
                                    local_sp_7_be = var_55;
                                }
                            }
                        }
                        local_sp_7 = local_sp_7_be;
                        continue;
                    }
                }
                if ((uint64_t)(var_16 + (-98)) != 0UL) {
                    var_36 = local_sp_7 + (-16L);
                    *(uint64_t *)var_36 = 4205587UL;
                    var_37 = indirect_placeholder_5(6481792UL, 6481600UL, 6481024UL);
                    local_sp_7_be = var_36;
                    if ((uint64_t)(unsigned char)var_37 == 1UL) {
                        var_38 = *(uint64_t *)6483160UL;
                        *(uint64_t *)(local_sp_7 + (-24L)) = 4205609UL;
                        var_39 = indirect_placeholder_39(var_38);
                        var_40 = var_39.field_0;
                        var_41 = var_39.field_1;
                        var_42 = var_39.field_2;
                        var_43 = local_sp_7 + (-32L);
                        *(uint64_t *)var_43 = 4205637UL;
                        indirect_placeholder_32(0UL, 4360888UL, var_40, 0UL, 0UL, var_41, var_42);
                        *var_8 = (unsigned char)'\x00';
                        local_sp_7_be = var_43;
                    }
                    local_sp_7 = local_sp_7_be;
                    continue;
                }
                if ((int)var_16 > (int)98U) {
                    if ((uint64_t)(var_16 + (-102)) != 0UL) {
                        var_28 = local_sp_7 + (-16L);
                        *(uint64_t *)var_28 = 4205666UL;
                        var_29 = indirect_placeholder_5(6482304UL, 6481728UL, 6481040UL);
                        local_sp_7_be = var_28;
                        if ((uint64_t)(unsigned char)var_29 == 1UL) {
                            var_30 = *(uint64_t *)6483160UL;
                            *(uint64_t *)(local_sp_7 + (-24L)) = 4205688UL;
                            var_31 = indirect_placeholder_38(var_30);
                            var_32 = var_31.field_0;
                            var_33 = var_31.field_1;
                            var_34 = var_31.field_2;
                            var_35 = local_sp_7 + (-32L);
                            *(uint64_t *)var_35 = 4205716UL;
                            indirect_placeholder_32(0UL, 4360928UL, var_32, 0UL, 0UL, var_33, var_34);
                            *var_8 = (unsigned char)'\x00';
                            local_sp_7_be = var_35;
                        }
                        local_sp_7 = local_sp_7_be;
                        continue;
                    }
                    if ((uint64_t)(var_16 + (-104)) != 0UL) {
                        var_20 = local_sp_7 + (-16L);
                        *(uint64_t *)var_20 = 4205508UL;
                        var_21 = indirect_placeholder_5(6482048UL, 6481664UL, 6481032UL);
                        local_sp_7_be = var_20;
                        if ((uint64_t)(unsigned char)var_21 == 1UL) {
                            var_22 = *(uint64_t *)6483160UL;
                            *(uint64_t *)(local_sp_7 + (-24L)) = 4205530UL;
                            var_23 = indirect_placeholder_40(var_22);
                            var_24 = var_23.field_0;
                            var_25 = var_23.field_1;
                            var_26 = var_23.field_2;
                            var_27 = local_sp_7 + (-32L);
                            *(uint64_t *)var_27 = 4205558UL;
                            indirect_placeholder_32(0UL, 4360848UL, var_24, 0UL, 0UL, var_25, var_26);
                            *var_8 = (unsigned char)'\x00';
                            local_sp_7_be = var_27;
                        }
                        local_sp_7 = local_sp_7_be;
                        continue;
                    }
                    if ((uint64_t)(var_16 + (-100)) == 0UL) {
                        *(uint64_t *)6481056UL = *(uint64_t *)6483160UL;
                        local_sp_7 = local_sp_7_be;
                        continue;
                    }
                }
                if ((uint64_t)(var_16 + 131U) != 0UL) {
                    if ((uint64_t)(var_16 + 130U) == 0UL) {
                        *(uint64_t *)(local_sp_7 + (-16L)) = 4206177UL;
                        indirect_placeholder_2(var_4, 0UL);
                        abort();
                    }
                }
                var_17 = *(uint64_t *)6481112UL;
                var_18 = local_sp_7 + (-24L);
                var_19 = (uint64_t *)var_18;
                *var_19 = 0UL;
                *(uint64_t *)(local_sp_7 + (-32L)) = 4206235UL;
                indirect_placeholder_41(0UL, 4358232UL, var_17, 4360779UL, 4361162UL, 4361146UL);
                *var_19 = 4206249UL;
                indirect_placeholder_1();
                local_sp_6 = var_18;
            }
        }
    switch (loop_state_var) {
      case 1U:
        {
            local_sp_1 = local_sp_0;
            while ((long)((uint64_t)var_89 << 32UL) >= (long)((uint64_t)var_88 << 32UL))
                {
                    var_90 = *(uint64_t *)(*var_7 + ((uint64_t)var_89 << 3UL));
                    var_91 = local_sp_0 + (-8L);
                    *(uint64_t *)var_91 = 4206819UL;
                    var_92 = indirect_placeholder_16(var_90);
                    *var_8 = ((var_92 & (uint64_t)*var_8) != 0UL);
                    var_93 = *(uint32_t *)6481272UL + 1U;
                    *(uint32_t *)6481272UL = var_93;
                    var_88 = *var_5;
                    var_89 = var_93;
                    local_sp_0 = var_91;
                    local_sp_1 = local_sp_0;
                }
        }
        break;
      case 0U:
        {
            *(uint64_t *)(local_sp_1 + (-8L)) = 4206888UL;
            var_96 = indirect_placeholder_3();
            if (*(unsigned char *)6482664UL != '\x00' & (uint64_t)((uint32_t)var_96 + 1U) == 0UL) {
                *(uint64_t *)(local_sp_1 + (-16L)) = 4206898UL;
                indirect_placeholder_1();
                var_97 = (uint64_t)*(uint32_t *)var_96;
                *(uint64_t *)(local_sp_1 + (-24L)) = 4206922UL;
                indirect_placeholder_32(0UL, 4360843UL, var_85, var_97, 1UL, 0UL, var_68);
            }
            return;
        }
        break;
    }
}

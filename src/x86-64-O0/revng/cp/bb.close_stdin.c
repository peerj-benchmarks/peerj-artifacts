typedef unsigned char __u_char;
typedef unsigned short __u_short;
typedef unsigned int __u_int;
typedef unsigned long __u_long;
typedef signed char __int8_t;
typedef unsigned char __uint8_t;
typedef short __int16_t;
typedef unsigned short __uint16_t;
typedef int __int32_t;
typedef unsigned int __uint32_t;
typedef long __int64_t;
typedef unsigned long __uint64_t;
typedef __int8_t __int_least8_t;
typedef __uint8_t __uint_least8_t;
typedef __int16_t __int_least16_t;
typedef __uint16_t __uint_least16_t;
typedef __int32_t __int_least32_t;
typedef __uint32_t __uint_least32_t;
typedef __int64_t __int_least64_t;
typedef __uint64_t __uint_least64_t;
typedef long __quad_t;
typedef unsigned long __u_quad_t;
typedef long __intmax_t;
typedef unsigned long __uintmax_t;
typedef unsigned long __dev_t;
typedef unsigned int __uid_t;
typedef unsigned int __gid_t;
typedef unsigned long __ino_t;
typedef unsigned long __ino64_t;
typedef unsigned int __mode_t;
typedef unsigned long __nlink_t;
typedef long __off_t;
typedef long __off64_t;
typedef int __pid_t;
typedef struct {
    int __val[2];
} __fsid_t;
typedef long __clock_t;
typedef unsigned long __rlim_t;
typedef unsigned long __rlim64_t;
typedef unsigned int __id_t;
typedef long __time_t;
typedef unsigned int __useconds_t;
typedef long __suseconds_t;
typedef int __daddr_t;
typedef int __key_t;
typedef int __clockid_t;
typedef void *__timer_t;
typedef long __blksize_t;
typedef long __blkcnt_t;
typedef long __blkcnt64_t;
typedef unsigned long __fsblkcnt_t;
typedef unsigned long __fsblkcnt64_t;
typedef unsigned long __fsfilcnt_t;
typedef unsigned long __fsfilcnt64_t;
typedef long __fsword_t;
typedef long __ssize_t;
typedef long __syscall_slong_t;
typedef unsigned long __syscall_ulong_t;
typedef __off64_t __loff_t;
typedef char *__caddr_t;
typedef long __intptr_t;
typedef unsigned int __socklen_t;
typedef int __sig_atomic_t;
typedef __int8_t int8_t;
typedef __int16_t int16_t;
typedef __int32_t int32_t;
typedef __int64_t int64_t;
typedef __uint8_t uint8_t;
typedef __uint16_t uint16_t;
typedef __uint32_t uint32_t;
typedef __uint64_t uint64_t;
typedef __int_least8_t int_least8_t;
typedef __int_least16_t int_least16_t;
typedef __int_least32_t int_least32_t;
typedef __int_least64_t int_least64_t;
typedef __uint_least8_t uint_least8_t;
typedef __uint_least16_t uint_least16_t;
typedef __uint_least32_t uint_least32_t;
typedef __uint_least64_t uint_least64_t;
typedef signed char int_fast8_t;
typedef long int_fast16_t;
typedef long int_fast32_t;
typedef long int_fast64_t;
typedef unsigned char uint_fast8_t;
typedef unsigned long uint_fast16_t;
typedef unsigned long uint_fast32_t;
typedef unsigned long uint_fast64_t;
typedef long intptr_t;
typedef unsigned long uintptr_t;
typedef __intmax_t intmax_t;
typedef __uintmax_t uintmax_t;
typedef _Bool bool;
struct indirect_placeholder_298_ret_type;
struct indirect_placeholder_297_ret_type;
struct indirect_placeholder_298_ret_type {
    uint64_t field_0;
    uint64_t field_1;
    uint64_t field_2;
};
struct indirect_placeholder_297_ret_type {
    uint64_t field_0;
    uint64_t field_1;
    uint64_t field_2;
};
extern uint64_t revng_init_local_sp(uint64_t param_0);
extern uint64_t init_rbp(void);
extern void indirect_placeholder(void);
extern uint64_t indirect_placeholder_4(uint64_t param_0, uint64_t param_1, uint64_t param_2);
extern uint64_t indirect_placeholder_2(uint64_t param_0);
extern uint64_t init_rbx(void);
extern uint64_t init_r8(void);
extern uint64_t init_r9(void);
extern uint64_t indirect_placeholder_17(void);
extern struct indirect_placeholder_298_ret_type indirect_placeholder_298(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint64_t param_3, uint64_t param_4, uint64_t param_5, uint64_t param_6);
extern struct indirect_placeholder_297_ret_type indirect_placeholder_297(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint64_t param_3, uint64_t param_4, uint64_t param_5, uint64_t param_6);
void bb_close_stdin(void) {
    uint64_t var_0;
    uint64_t var_1;
    uint64_t var_2;
    uint64_t var_3;
    uint64_t var_4;
    unsigned char *var_5;
    uint64_t var_6;
    uint64_t var_7;
    uint64_t local_sp_1;
    unsigned char var_15;
    uint64_t local_sp_0;
    uint64_t *var_16;
    uint64_t var_17;
    bool var_18;
    uint64_t *var_19;
    uint64_t var_20;
    uint64_t var_21;
    uint64_t var_22;
    uint64_t var_23;
    uint64_t var_24;
    uint64_t var_25;
    uint64_t var_26;
    uint64_t var_10;
    uint64_t var_11;
    uint64_t var_12;
    uint64_t var_8;
    uint64_t var_9;
    uint64_t var_13;
    uint64_t var_14;
    var_0 = revng_init_local_sp(0UL);
    var_1 = init_rbp();
    var_2 = init_rbx();
    var_3 = init_r8();
    var_4 = init_r9();
    *(uint64_t *)(var_0 + (-8L)) = var_1;
    *(uint64_t *)(var_0 + (-16L)) = var_2;
    var_5 = (unsigned char *)(var_0 + (-25L));
    *var_5 = (unsigned char)'\x00';
    var_6 = *(uint64_t *)6473416UL;
    var_7 = var_0 + (-48L);
    *(uint64_t *)var_7 = 4245935UL;
    indirect_placeholder();
    var_15 = (unsigned char)'\x01';
    local_sp_1 = var_7;
    var_8 = var_0 + (-56L);
    *(uint64_t *)var_8 = 4245965UL;
    var_9 = indirect_placeholder_17();
    local_sp_1 = var_8;
    var_10 = *(uint64_t *)6473416UL;
    var_11 = var_0 + (-64L);
    *(uint64_t *)var_11 = 4245984UL;
    var_12 = indirect_placeholder_4(1UL, var_10, 0UL);
    local_sp_1 = var_11;
    if (var_6 != 0UL & (uint64_t)(uint32_t)var_9 != 0UL & (uint64_t)(uint32_t)var_12 == 0UL) {
        *var_5 = (unsigned char)'\x01';
    }
    var_13 = local_sp_1 + (-8L);
    *(uint64_t *)var_13 = 4246007UL;
    var_14 = indirect_placeholder_17();
    local_sp_0 = var_13;
    if ((uint64_t)(uint32_t)var_14 == 0UL) {
        var_15 = *var_5;
    } else {
        *var_5 = (unsigned char)'\x01';
    }
    if (var_15 != '\x00') {
        var_16 = (uint64_t *)(var_0 + (-40L));
        *var_16 = 4355235UL;
        var_17 = *(uint64_t *)6474896UL;
        var_18 = (var_17 == 0UL);
        var_19 = (uint64_t *)(local_sp_1 + (-16L));
        if (var_18) {
            *var_19 = 4246105UL;
            indirect_placeholder();
            var_24 = (uint64_t)*(volatile uint32_t *)(uint32_t *)0UL;
            var_25 = *var_16;
            var_26 = local_sp_1 + (-24L);
            *(uint64_t *)var_26 = 4246136UL;
            indirect_placeholder_297(0UL, 4355261UL, var_25, 0UL, var_24, var_3, var_4);
            local_sp_0 = var_26;
        } else {
            *var_19 = 4246056UL;
            var_20 = indirect_placeholder_2(var_17);
            *(uint64_t *)(local_sp_1 + (-24L)) = 4246064UL;
            indirect_placeholder();
            var_21 = (uint64_t)*(uint32_t *)var_20;
            var_22 = *var_16;
            var_23 = local_sp_1 + (-32L);
            *(uint64_t *)var_23 = 4246098UL;
            indirect_placeholder_298(0UL, 4355254UL, var_20, 0UL, var_21, var_22, var_4);
            local_sp_0 = var_23;
        }
    }
    *(uint64_t *)(local_sp_0 + (-8L)) = 4246141UL;
    indirect_placeholder();
    if (*var_5 == '\x00') {
        return;
    }
    *(uint64_t *)(local_sp_0 + (-16L)) = 4246160UL;
    indirect_placeholder();
    return;
}

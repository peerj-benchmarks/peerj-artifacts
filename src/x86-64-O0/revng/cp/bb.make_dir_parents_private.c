typedef unsigned char __u_char;
typedef unsigned short __u_short;
typedef unsigned int __u_int;
typedef unsigned long __u_long;
typedef signed char __int8_t;
typedef unsigned char __uint8_t;
typedef short __int16_t;
typedef unsigned short __uint16_t;
typedef int __int32_t;
typedef unsigned int __uint32_t;
typedef long __int64_t;
typedef unsigned long __uint64_t;
typedef __int8_t __int_least8_t;
typedef __uint8_t __uint_least8_t;
typedef __int16_t __int_least16_t;
typedef __uint16_t __uint_least16_t;
typedef __int32_t __int_least32_t;
typedef __uint32_t __uint_least32_t;
typedef __int64_t __int_least64_t;
typedef __uint64_t __uint_least64_t;
typedef long __quad_t;
typedef unsigned long __u_quad_t;
typedef long __intmax_t;
typedef unsigned long __uintmax_t;
typedef unsigned long __dev_t;
typedef unsigned int __uid_t;
typedef unsigned int __gid_t;
typedef unsigned long __ino_t;
typedef unsigned long __ino64_t;
typedef unsigned int __mode_t;
typedef unsigned long __nlink_t;
typedef long __off_t;
typedef long __off64_t;
typedef int __pid_t;
typedef struct {
    int __val[2];
} __fsid_t;
typedef long __clock_t;
typedef unsigned long __rlim_t;
typedef unsigned long __rlim64_t;
typedef unsigned int __id_t;
typedef long __time_t;
typedef unsigned int __useconds_t;
typedef long __suseconds_t;
typedef int __daddr_t;
typedef int __key_t;
typedef int __clockid_t;
typedef void *__timer_t;
typedef long __blksize_t;
typedef long __blkcnt_t;
typedef long __blkcnt64_t;
typedef unsigned long __fsblkcnt_t;
typedef unsigned long __fsblkcnt64_t;
typedef unsigned long __fsfilcnt_t;
typedef unsigned long __fsfilcnt64_t;
typedef long __fsword_t;
typedef long __ssize_t;
typedef long __syscall_slong_t;
typedef unsigned long __syscall_ulong_t;
typedef __off64_t __loff_t;
typedef char *__caddr_t;
typedef long __intptr_t;
typedef unsigned int __socklen_t;
typedef int __sig_atomic_t;
typedef __int8_t int8_t;
typedef __int16_t int16_t;
typedef __int32_t int32_t;
typedef __int64_t int64_t;
typedef __uint8_t uint8_t;
typedef __uint16_t uint16_t;
typedef __uint32_t uint32_t;
typedef __uint64_t uint64_t;
typedef __int_least8_t int_least8_t;
typedef __int_least16_t int_least16_t;
typedef __int_least32_t int_least32_t;
typedef __int_least64_t int_least64_t;
typedef __uint_least8_t uint_least8_t;
typedef __uint_least16_t uint_least16_t;
typedef __uint_least32_t uint_least32_t;
typedef __uint_least64_t uint_least64_t;
typedef signed char int_fast8_t;
typedef long int_fast16_t;
typedef long int_fast32_t;
typedef long int_fast64_t;
typedef unsigned char uint_fast8_t;
typedef unsigned long uint_fast16_t;
typedef unsigned long uint_fast32_t;
typedef unsigned long uint_fast64_t;
typedef long intptr_t;
typedef unsigned long uintptr_t;
typedef __intmax_t intmax_t;
typedef __uintmax_t uintmax_t;
struct helper_divq_EAX_wrapper_ret_type;
struct type_5;
struct indirect_placeholder_190_ret_type;
struct indirect_placeholder_189_ret_type;
struct indirect_placeholder_192_ret_type;
struct indirect_placeholder_191_ret_type;
struct indirect_placeholder_194_ret_type;
struct indirect_placeholder_193_ret_type;
struct indirect_placeholder_195_ret_type;
struct indirect_placeholder_188_ret_type;
struct indirect_placeholder_196_ret_type;
struct indirect_placeholder_198_ret_type;
struct indirect_placeholder_197_ret_type;
struct indirect_placeholder_199_ret_type;
struct indirect_placeholder_200_ret_type;
struct indirect_placeholder_201_ret_type;
struct indirect_placeholder_187_ret_type;
struct helper_divq_EAX_wrapper_ret_type {
    uint64_t field_0;
    uint64_t field_1;
    uint64_t field_2;
    uint32_t field_3;
    uint64_t field_4;
    uint32_t field_5;
    uint64_t field_6;
    uint32_t field_7;
    uint64_t field_8;
    uint32_t field_9;
    unsigned char field_10;
    uint32_t field_11;
    uint32_t field_12;
};
struct type_5 {
};
struct indirect_placeholder_190_ret_type {
    uint64_t field_0;
    uint64_t field_1;
    uint64_t field_2;
};
struct indirect_placeholder_189_ret_type {
    uint64_t field_0;
    uint64_t field_1;
    uint64_t field_2;
};
struct indirect_placeholder_192_ret_type {
    uint64_t field_0;
    uint64_t field_1;
    uint64_t field_2;
};
struct indirect_placeholder_191_ret_type {
    uint64_t field_0;
    uint64_t field_1;
    uint64_t field_2;
};
struct indirect_placeholder_194_ret_type {
    uint64_t field_0;
    uint64_t field_1;
    uint64_t field_2;
};
struct indirect_placeholder_193_ret_type {
    uint64_t field_0;
    uint64_t field_1;
    uint64_t field_2;
};
struct indirect_placeholder_195_ret_type {
    uint64_t field_0;
    uint64_t field_1;
    uint64_t field_2;
};
struct indirect_placeholder_188_ret_type {
    uint64_t field_0;
    uint64_t field_1;
    uint64_t field_2;
};
struct indirect_placeholder_196_ret_type {
    uint64_t field_0;
    uint64_t field_1;
    uint64_t field_2;
};
struct indirect_placeholder_198_ret_type {
    uint64_t field_0;
    uint64_t field_1;
    uint64_t field_2;
};
struct indirect_placeholder_197_ret_type {
    uint64_t field_0;
    uint64_t field_1;
    uint64_t field_2;
};
struct indirect_placeholder_199_ret_type {
    uint64_t field_0;
    uint64_t field_1;
    uint64_t field_2;
    uint64_t field_3;
    uint64_t field_4;
};
struct indirect_placeholder_200_ret_type {
    uint64_t field_0;
    uint64_t field_1;
    uint64_t field_2;
};
struct indirect_placeholder_201_ret_type {
    uint64_t field_0;
    uint64_t field_1;
    uint64_t field_2;
};
struct indirect_placeholder_187_ret_type {
    uint64_t field_0;
    uint64_t field_1;
    uint64_t field_2;
};
extern uint64_t revng_init_local_sp(uint64_t param_0);
extern uint64_t init_rbp(void);
extern uint64_t indirect_placeholder_1(uint64_t param_0, uint64_t param_1);
extern void indirect_placeholder(void);
extern uint64_t indirect_placeholder_2(uint64_t param_0);
extern uint64_t init_rbx(void);
extern uint64_t init_r10(void);
extern uint32_t init_state_0x8248(void);
extern uint64_t init_state_0x9018(void);
extern uint32_t init_state_0x9010(void);
extern uint64_t init_state_0x8408(void);
extern uint64_t init_state_0x8328(void);
extern uint64_t init_state_0x82d8(void);
extern uint32_t init_state_0x9080(void);
extern struct helper_divq_EAX_wrapper_ret_type helper_divq_EAX_wrapper(struct type_5 *param_0, uint64_t param_1, uint64_t param_2, uint64_t param_3, uint64_t param_4, uint64_t param_5, uint64_t param_6, uint64_t param_7, uint64_t param_8, uint64_t param_9, uint64_t param_10, uint64_t param_11, uint64_t param_12, uint32_t param_13, uint64_t param_14, uint32_t param_15, uint64_t param_16, uint64_t param_17, uint64_t param_18, uint32_t param_19);
extern uint64_t indirect_placeholder_17(void);
extern struct indirect_placeholder_190_ret_type indirect_placeholder_190(uint64_t param_0, uint64_t param_1);
extern struct indirect_placeholder_189_ret_type indirect_placeholder_189(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint64_t param_3, uint64_t param_4, uint64_t param_5, uint64_t param_6);
extern struct indirect_placeholder_192_ret_type indirect_placeholder_192(uint64_t param_0, uint64_t param_1);
extern struct indirect_placeholder_191_ret_type indirect_placeholder_191(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint64_t param_3, uint64_t param_4, uint64_t param_5, uint64_t param_6);
extern struct indirect_placeholder_194_ret_type indirect_placeholder_194(uint64_t param_0, uint64_t param_1);
extern struct indirect_placeholder_193_ret_type indirect_placeholder_193(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint64_t param_3, uint64_t param_4, uint64_t param_5, uint64_t param_6);
extern struct indirect_placeholder_195_ret_type indirect_placeholder_195(uint64_t param_0, uint64_t param_1);
extern struct indirect_placeholder_188_ret_type indirect_placeholder_188(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint64_t param_3, uint64_t param_4, uint64_t param_5, uint64_t param_6);
extern struct indirect_placeholder_196_ret_type indirect_placeholder_196(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint64_t param_3);
extern struct indirect_placeholder_198_ret_type indirect_placeholder_198(uint64_t param_0, uint64_t param_1);
extern struct indirect_placeholder_197_ret_type indirect_placeholder_197(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint64_t param_3, uint64_t param_4, uint64_t param_5, uint64_t param_6);
extern struct indirect_placeholder_199_ret_type indirect_placeholder_199(uint64_t param_0);
extern struct indirect_placeholder_200_ret_type indirect_placeholder_200(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint64_t param_3, uint64_t param_4);
extern struct indirect_placeholder_201_ret_type indirect_placeholder_201(uint64_t param_0, uint64_t param_1);
extern struct indirect_placeholder_187_ret_type indirect_placeholder_187(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint64_t param_3, uint64_t param_4, uint64_t param_5, uint64_t param_6);
uint64_t bb_make_dir_parents_private(uint64_t rdx, uint64_t rcx, uint64_t rdi, uint64_t rsi, uint64_t r8, uint64_t r9) {
    struct indirect_placeholder_200_ret_type var_79;
    uint64_t var_0;
    uint64_t var_1;
    uint64_t var_2;
    uint64_t var_3;
    uint32_t var_4;
    uint64_t var_5;
    uint32_t var_6;
    uint64_t var_7;
    uint64_t var_8;
    uint64_t var_9;
    uint32_t var_10;
    uint64_t var_11;
    uint64_t *var_12;
    uint64_t *var_13;
    uint64_t *var_14;
    uint64_t var_15;
    uint64_t var_16;
    uint64_t *var_17;
    uint64_t var_18;
    uint64_t var_19;
    uint32_t var_21;
    uint64_t var_22;
    uint32_t var_23;
    struct helper_divq_EAX_wrapper_ret_type var_20;
    uint64_t var_24;
    uint32_t var_25;
    uint64_t var_26;
    uint64_t var_27;
    uint64_t *var_28;
    uint64_t var_29;
    uint64_t *var_30;
    uint64_t var_31;
    uint64_t var_32;
    uint64_t var_33;
    uint64_t *var_34;
    uint64_t var_36;
    struct helper_divq_EAX_wrapper_ret_type var_35;
    uint64_t *var_37;
    uint64_t **var_38;
    uint64_t var_39;
    uint64_t var_40;
    uint64_t var_41;
    uint64_t var_56;
    uint64_t var_57;
    uint64_t var_58;
    unsigned char var_59;
    uint64_t var_135;
    uint64_t local_sp_5;
    uint64_t rax_2;
    uint64_t var_115;
    struct indirect_placeholder_190_ret_type var_116;
    uint64_t var_117;
    uint64_t var_118;
    uint64_t var_119;
    uint64_t var_120;
    uint64_t var_98;
    struct indirect_placeholder_192_ret_type var_99;
    uint64_t var_100;
    uint64_t var_101;
    uint64_t var_102;
    uint64_t var_103;
    uint64_t local_sp_1;
    uint32_t var_104;
    uint32_t var_105;
    uint64_t local_sp_8;
    uint32_t var_106;
    uint64_t var_131;
    uint64_t var_129;
    uint32_t var_107;
    uint32_t _pre_phi288;
    uint64_t var_108;
    uint64_t var_109;
    uint32_t var_110;
    uint32_t _pre284;
    uint64_t local_sp_4;
    uint32_t var_111;
    uint32_t var_112;
    uint64_t local_sp_0;
    unsigned char **_pre_phi;
    uint64_t var_113;
    uint64_t var_114;
    uint64_t var_88;
    struct indirect_placeholder_194_ret_type var_89;
    uint64_t var_90;
    uint64_t var_91;
    uint64_t var_92;
    uint64_t var_93;
    uint64_t local_sp_3;
    uint64_t var_94;
    uint64_t var_95;
    uint64_t var_96;
    uint64_t var_97;
    unsigned char **var_80;
    uint32_t var_81;
    uint64_t var_82;
    uint32_t rax_0;
    uint32_t storemerge11;
    uint32_t var_83;
    uint64_t var_84;
    uint64_t var_85;
    uint64_t var_86;
    uint64_t var_87;
    uint64_t var_121;
    struct indirect_placeholder_195_ret_type var_122;
    uint64_t var_123;
    uint64_t var_124;
    uint64_t var_125;
    unsigned char **var_126;
    uint64_t var_127;
    unsigned char *_pre_phi292;
    unsigned char *var_128;
    uint64_t var_130;
    struct indirect_placeholder_196_ret_type var_132;
    uint64_t var_133;
    uint64_t var_55;
    uint64_t var_64;
    uint32_t rax_1;
    uint64_t local_sp_6;
    uint64_t var_65;
    struct indirect_placeholder_198_ret_type var_66;
    uint64_t var_67;
    uint64_t var_68;
    uint64_t var_69;
    uint64_t var_70;
    struct indirect_placeholder_199_ret_type var_71;
    uint64_t var_72;
    unsigned char var_73;
    uint64_t var_60;
    uint64_t local_sp_7;
    uint64_t var_74;
    uint64_t storemerge;
    uint64_t var_75;
    uint64_t var_76;
    uint64_t var_77;
    uint64_t var_78;
    uint64_t var_61;
    uint64_t var_62;
    uint64_t var_63;
    uint64_t var_42;
    uint64_t var_43;
    uint64_t *var_44;
    uint64_t var_45;
    unsigned char **var_46;
    unsigned char *var_47;
    uint32_t *var_48;
    uint32_t *var_49;
    uint64_t *var_50;
    uint32_t *var_51;
    uint32_t *var_52;
    uint32_t *var_53;
    uint32_t *var_54;
    uint64_t var_134;
    struct indirect_placeholder_201_ret_type var_136;
    uint64_t var_137;
    uint64_t var_138;
    uint64_t var_139;
    var_0 = revng_init_local_sp(0UL);
    var_1 = init_rbp();
    var_2 = init_rbx();
    var_3 = init_r10();
    var_4 = init_state_0x8248();
    var_5 = init_state_0x9018();
    var_6 = init_state_0x9010();
    var_7 = init_state_0x8408();
    var_8 = init_state_0x8328();
    var_9 = init_state_0x82d8();
    var_10 = init_state_0x9080();
    var_11 = var_0 + (-8L);
    *(uint64_t *)var_11 = var_1;
    *(uint64_t *)(var_0 + (-16L)) = var_2;
    var_12 = (uint64_t *)(var_0 + (-400L));
    *var_12 = rdi;
    var_13 = (uint64_t *)(var_0 + (-408L));
    *var_13 = rsi;
    var_14 = (uint64_t *)(var_0 + (-416L));
    *var_14 = rdx;
    var_15 = var_0 + (-424L);
    *(uint64_t *)var_15 = rcx;
    var_16 = var_0 + (-432L);
    *(uint64_t *)var_16 = r8;
    var_17 = (uint64_t *)(var_0 + (-440L));
    *var_17 = r9;
    var_18 = *var_12;
    var_19 = var_0 + (-448L);
    *(uint64_t *)var_19 = 4207699UL;
    indirect_placeholder();
    var_20 = helper_divq_EAX_wrapper((struct type_5 *)(0UL), 16UL, 4207729UL, var_18 + 31UL, var_11, 0UL, rcx, var_18, 16UL, rsi, r8, var_3, r9, var_4, var_5, var_6, var_7, var_8, var_9, var_10);
    var_21 = var_20.field_5;
    var_22 = var_20.field_6;
    var_23 = var_20.field_7;
    var_24 = var_20.field_8;
    var_25 = var_20.field_9;
    var_26 = var_19 - (var_20.field_1 << 4UL);
    var_27 = *var_12;
    *(uint64_t *)(var_26 + (-8L)) = 4207775UL;
    indirect_placeholder();
    var_28 = (uint64_t *)(var_0 + (-56L));
    *var_28 = var_27;
    var_29 = *var_13 + var_27;
    var_30 = (uint64_t *)(var_0 + (-64L));
    *var_30 = var_29;
    var_31 = *var_28;
    var_32 = var_26 + (-16L);
    *(uint64_t *)var_32 = 4207809UL;
    var_33 = indirect_placeholder_17();
    var_34 = (uint64_t *)(var_0 + (-72L));
    *var_34 = var_33;
    var_35 = helper_divq_EAX_wrapper((struct type_5 *)(0UL), 16UL, 4207847UL, var_33 + 31UL, var_11, 0UL, rcx, var_31, 16UL, var_27, r8, var_3, r9, var_21, var_22, var_23, var_7, var_8, var_24, var_25);
    var_36 = var_32 - (var_35.field_1 << 4UL);
    var_37 = (uint64_t *)(var_0 + (-80L));
    *var_37 = var_36;
    *(uint64_t *)(var_36 + (-8L)) = 4207899UL;
    indirect_placeholder();
    *(unsigned char *)(*var_34 + *var_37) = (unsigned char)'\x00';
    var_38 = (uint64_t **)var_15;
    **var_38 = 0UL;
    var_39 = *var_37;
    var_40 = var_36 + (-16L);
    *(uint64_t *)var_40 = 4207949UL;
    var_41 = indirect_placeholder_2(var_39);
    rax_2 = 1UL;
    local_sp_8 = var_40;
    rax_0 = 63U;
    storemerge11 = 511U;
    var_73 = (unsigned char)'\x00';
    storemerge = 0UL;
    if ((uint64_t)(uint32_t)var_41 == 0UL) {
        rax_2 = 0UL;
        if ((uint32_t)((uint16_t)*(uint32_t *)(var_0 + (-224L)) & (unsigned short)61440U) == 16384U) {
            **(unsigned char **)var_16 = (unsigned char)'\x00';
        } else {
            var_135 = *var_37;
            *(uint64_t *)(var_36 + (-24L)) = 4209265UL;
            var_136 = indirect_placeholder_201(4UL, var_135);
            var_137 = var_136.field_0;
            var_138 = var_136.field_1;
            var_139 = var_136.field_2;
            *(uint64_t *)(var_36 + (-32L)) = 4209293UL;
            indirect_placeholder_187(0UL, 4348696UL, var_137, 0UL, 0UL, var_138, var_139);
        }
    } else {
        var_42 = *var_30;
        var_43 = var_0 + (-32L);
        var_44 = (uint64_t *)var_43;
        *var_44 = var_42;
        var_45 = var_42;
        var_46 = (unsigned char **)var_43;
        var_55 = var_45;
        while (**var_46 != '/')
            {
                var_134 = var_45 + 1UL;
                *var_44 = var_134;
                var_45 = var_134;
                var_46 = (unsigned char **)var_43;
                var_55 = var_45;
            }
        var_47 = (unsigned char *)(var_0 + (-81L));
        var_48 = (uint32_t *)(var_0 + (-368L));
        var_49 = (uint32_t *)(var_0 + (-96L));
        var_50 = (uint64_t *)(var_0 + (-40L));
        var_51 = (uint32_t *)(var_0 + (-88L));
        var_52 = (uint32_t *)(var_0 + (-44L));
        var_53 = (uint32_t *)(var_0 + (-92L));
        var_54 = (uint32_t *)(var_0 + (-224L));
        *(uint64_t *)(local_sp_8 + (-8L)) = 4209213UL;
        indirect_placeholder();
        *var_44 = var_55;
        while (var_55 != 0UL)
            {
                **var_46 = (unsigned char)'\x00';
                var_56 = *var_28;
                var_57 = local_sp_8 + (-16L);
                *(uint64_t *)var_57 = 4208017UL;
                var_58 = indirect_placeholder_2(var_56);
                var_59 = ((uint64_t)(uint32_t)var_58 != 0UL);
                *var_47 = var_59;
                local_sp_7 = var_57;
                rax_2 = 0UL;
                if (var_59 == '\x00') {
                    var_60 = *var_17;
                    if (*(unsigned char *)(var_60 + 29UL) == '\x00') {
                        if (*(unsigned char *)(var_60 + 30UL) == '\x00') {
                            if (*(unsigned char *)(var_60 + 31UL) != '\x00') {
                                var_61 = *var_30;
                                var_62 = local_sp_8 + (-24L);
                                *(uint64_t *)var_62 = 4208102UL;
                                var_63 = indirect_placeholder_2(var_61);
                                local_sp_6 = var_62;
                                if ((uint64_t)(uint32_t)var_63 == 0UL) {
                                    rax_1 = ((uint32_t)((uint16_t)*var_48 & (unsigned short)61440U) == 16384U) ? 0U : 20U;
                                } else {
                                    var_64 = local_sp_8 + (-32L);
                                    *(uint64_t *)var_64 = 4208111UL;
                                    indirect_placeholder();
                                    rax_1 = *(uint32_t *)var_63;
                                    local_sp_6 = var_64;
                                }
                                *var_49 = rax_1;
                                if (rax_1 != 0U) {
                                    var_65 = *var_30;
                                    *(uint64_t *)(local_sp_6 + (-8L)) = 4208171UL;
                                    var_66 = indirect_placeholder_198(4UL, var_65);
                                    var_67 = var_66.field_0;
                                    var_68 = var_66.field_1;
                                    var_69 = var_66.field_2;
                                    var_70 = (uint64_t)*var_49;
                                    *(uint64_t *)(local_sp_6 + (-16L)) = 4208202UL;
                                    indirect_placeholder_197(0UL, 4348608UL, var_67, 0UL, var_70, var_68, var_69);
                                    break;
                                }
                                *(uint64_t *)(local_sp_6 + (-8L)) = 4208222UL;
                                var_71 = indirect_placeholder_199(168UL);
                                *var_50 = var_71.field_0;
                                var_72 = local_sp_6 + (-16L);
                                *(uint64_t *)var_72 = 4208256UL;
                                indirect_placeholder();
                                *(uint64_t *)(*var_50 + 152UL) = (*var_44 - *var_28);
                                *(unsigned char *)(*var_50 + 144UL) = (unsigned char)'\x00';
                                *(uint64_t *)(*var_50 + 160UL) = **var_38;
                                **var_38 = *var_50;
                                var_73 = *var_47;
                                local_sp_7 = var_72;
                            }
                        } else {
                            var_61 = *var_30;
                            var_62 = local_sp_8 + (-24L);
                            *(uint64_t *)var_62 = 4208102UL;
                            var_63 = indirect_placeholder_2(var_61);
                            local_sp_6 = var_62;
                            if ((uint64_t)(uint32_t)var_63 == 0UL) {
                                var_64 = local_sp_8 + (-32L);
                                *(uint64_t *)var_64 = 4208111UL;
                                indirect_placeholder();
                                rax_1 = *(uint32_t *)var_63;
                                local_sp_6 = var_64;
                            } else {
                                rax_1 = ((uint32_t)((uint16_t)*var_48 & (unsigned short)61440U) == 16384U) ? 0U : 20U;
                            }
                            *var_49 = rax_1;
                            if (rax_1 != 0U) {
                                var_65 = *var_30;
                                *(uint64_t *)(local_sp_6 + (-8L)) = 4208171UL;
                                var_66 = indirect_placeholder_198(4UL, var_65);
                                var_67 = var_66.field_0;
                                var_68 = var_66.field_1;
                                var_69 = var_66.field_2;
                                var_70 = (uint64_t)*var_49;
                                *(uint64_t *)(local_sp_6 + (-16L)) = 4208202UL;
                                indirect_placeholder_197(0UL, 4348608UL, var_67, 0UL, var_70, var_68, var_69);
                                break;
                            }
                            *(uint64_t *)(local_sp_6 + (-8L)) = 4208222UL;
                            var_71 = indirect_placeholder_199(168UL);
                            *var_50 = var_71.field_0;
                            var_72 = local_sp_6 + (-16L);
                            *(uint64_t *)var_72 = 4208256UL;
                            indirect_placeholder();
                            *(uint64_t *)(*var_50 + 152UL) = (*var_44 - *var_28);
                            *(unsigned char *)(*var_50 + 144UL) = (unsigned char)'\x00';
                            *(uint64_t *)(*var_50 + 160UL) = **var_38;
                            **var_38 = *var_50;
                            var_73 = *var_47;
                            local_sp_7 = var_72;
                        }
                    } else {
                        var_61 = *var_30;
                        var_62 = local_sp_8 + (-24L);
                        *(uint64_t *)var_62 = 4208102UL;
                        var_63 = indirect_placeholder_2(var_61);
                        local_sp_6 = var_62;
                        if ((uint64_t)(uint32_t)var_63 == 0UL) {
                            var_64 = local_sp_8 + (-32L);
                            *(uint64_t *)var_64 = 4208111UL;
                            indirect_placeholder();
                            rax_1 = *(uint32_t *)var_63;
                            local_sp_6 = var_64;
                        } else {
                            rax_1 = ((uint32_t)((uint16_t)*var_48 & (unsigned short)61440U) == 16384U) ? 0U : 20U;
                        }
                        *var_49 = rax_1;
                        if (rax_1 != 0U) {
                            var_65 = *var_30;
                            *(uint64_t *)(local_sp_6 + (-8L)) = 4208171UL;
                            var_66 = indirect_placeholder_198(4UL, var_65);
                            var_67 = var_66.field_0;
                            var_68 = var_66.field_1;
                            var_69 = var_66.field_2;
                            var_70 = (uint64_t)*var_49;
                            *(uint64_t *)(local_sp_6 + (-16L)) = 4208202UL;
                            indirect_placeholder_197(0UL, 4348608UL, var_67, 0UL, var_70, var_68, var_69);
                            break;
                        }
                        *(uint64_t *)(local_sp_6 + (-8L)) = 4208222UL;
                        var_71 = indirect_placeholder_199(168UL);
                        *var_50 = var_71.field_0;
                        var_72 = local_sp_6 + (-16L);
                        *(uint64_t *)var_72 = 4208256UL;
                        indirect_placeholder();
                        *(uint64_t *)(*var_50 + 152UL) = (*var_44 - *var_28);
                        *(unsigned char *)(*var_50 + 144UL) = (unsigned char)'\x00';
                        *(uint64_t *)(*var_50 + 160UL) = **var_38;
                        **var_38 = *var_50;
                        var_73 = *var_47;
                        local_sp_7 = var_72;
                    }
                } else {
                    var_61 = *var_30;
                    var_62 = local_sp_8 + (-24L);
                    *(uint64_t *)var_62 = 4208102UL;
                    var_63 = indirect_placeholder_2(var_61);
                    local_sp_6 = var_62;
                    if ((uint64_t)(uint32_t)var_63 == 0UL) {
                        var_64 = local_sp_8 + (-32L);
                        *(uint64_t *)var_64 = 4208111UL;
                        indirect_placeholder();
                        rax_1 = *(uint32_t *)var_63;
                        local_sp_6 = var_64;
                    } else {
                        rax_1 = ((uint32_t)((uint16_t)*var_48 & (unsigned short)61440U) == 16384U) ? 0U : 20U;
                    }
                    *var_49 = rax_1;
                    if (rax_1 != 0U) {
                        var_65 = *var_30;
                        *(uint64_t *)(local_sp_6 + (-8L)) = 4208171UL;
                        var_66 = indirect_placeholder_198(4UL, var_65);
                        var_67 = var_66.field_0;
                        var_68 = var_66.field_1;
                        var_69 = var_66.field_2;
                        var_70 = (uint64_t)*var_49;
                        *(uint64_t *)(local_sp_6 + (-16L)) = 4208202UL;
                        indirect_placeholder_197(0UL, 4348608UL, var_67, 0UL, var_70, var_68, var_69);
                        break;
                    }
                    *(uint64_t *)(local_sp_6 + (-8L)) = 4208222UL;
                    var_71 = indirect_placeholder_199(168UL);
                    *var_50 = var_71.field_0;
                    var_72 = local_sp_6 + (-16L);
                    *(uint64_t *)var_72 = 4208256UL;
                    indirect_placeholder();
                    *(uint64_t *)(*var_50 + 152UL) = (*var_44 - *var_28);
                    *(unsigned char *)(*var_50 + 144UL) = (unsigned char)'\x00';
                    *(uint64_t *)(*var_50 + 160UL) = **var_38;
                    **var_38 = *var_50;
                    var_73 = *var_47;
                    local_sp_7 = var_72;
                }
                var_74 = (uint64_t)var_73;
                if (var_73 == '\x00') {
                    storemerge = (uint64_t)*(uint32_t *)(*var_50 + 24UL);
                }
                var_75 = *var_17;
                var_76 = *var_28;
                var_77 = *var_30;
                var_78 = local_sp_7 + (-8L);
                *(uint64_t *)var_78 = 4208381UL;
                var_79 = indirect_placeholder_200(storemerge, var_74, var_77, var_76, var_75);
                local_sp_4 = var_78;
                if ((uint64_t)(unsigned char)var_79.field_0 == 1UL) {
                    break;
                }
                if (*var_47 != '\x00') {
                    var_80 = (unsigned char **)var_16;
                    **var_80 = (unsigned char)'\x01';
                    var_81 = *(uint32_t *)(*var_50 + 24UL);
                    *var_51 = var_81;
                    var_82 = *var_17;
                    _pre_phi = var_80;
                    if (*(unsigned char *)(var_82 + 29UL) == '\x01') {
                        rax_0 = (*(unsigned char *)(var_82 + 30UL) == '\x00') ? 0U : 18U;
                    }
                    *var_52 = (rax_0 & var_81);
                    if (*(unsigned char *)(*var_17 + 32UL) == '\x00') {
                        storemerge11 = *var_51;
                    }
                    *var_53 = storemerge11;
                    var_83 = (uint32_t)(((uint16_t)storemerge11 & ((uint16_t)*var_52 ^ (unsigned short)4095U)) & (unsigned short)4095U);
                    *var_53 = var_83;
                    var_84 = (uint64_t)var_83;
                    var_85 = *var_28;
                    var_86 = local_sp_7 + (-16L);
                    *(uint64_t *)var_86 = 4208547UL;
                    var_87 = indirect_placeholder_1(var_85, var_84);
                    local_sp_3 = var_86;
                    if ((uint64_t)(uint32_t)var_87 != 0UL) {
                        var_88 = *var_28;
                        *(uint64_t *)(local_sp_7 + (-24L)) = 4208568UL;
                        var_89 = indirect_placeholder_194(4UL, var_88);
                        var_90 = var_89.field_0;
                        var_91 = var_89.field_1;
                        var_92 = var_89.field_2;
                        *(uint64_t *)(local_sp_7 + (-32L)) = 4208576UL;
                        indirect_placeholder();
                        var_93 = (uint64_t)*(uint32_t *)var_90;
                        *(uint64_t *)(local_sp_7 + (-40L)) = 4208603UL;
                        indirect_placeholder_193(0UL, 4348639UL, var_90, 0UL, var_93, var_91, var_92);
                        break;
                    }
                    if (*var_14 == 0UL) {
                        var_94 = local_sp_7 + (-24L);
                        *(uint64_t *)var_94 = 4208654UL;
                        indirect_placeholder();
                        local_sp_3 = var_94;
                    }
                    var_95 = *var_28;
                    var_96 = local_sp_3 + (-8L);
                    *(uint64_t *)var_96 = 4208676UL;
                    var_97 = indirect_placeholder_2(var_95);
                    local_sp_0 = var_96;
                    local_sp_1 = var_96;
                    if ((uint64_t)(uint32_t)var_97 != 0UL) {
                        var_98 = *var_28;
                        *(uint64_t *)(local_sp_3 + (-16L)) = 4208697UL;
                        var_99 = indirect_placeholder_192(4UL, var_98);
                        var_100 = var_99.field_0;
                        var_101 = var_99.field_1;
                        var_102 = var_99.field_2;
                        *(uint64_t *)(local_sp_3 + (-24L)) = 4208705UL;
                        indirect_placeholder();
                        var_103 = (uint64_t)*(uint32_t *)var_100;
                        *(uint64_t *)(local_sp_3 + (-32L)) = 4208732UL;
                        indirect_placeholder_191(0UL, 4348608UL, var_100, 0UL, var_103, var_101, var_102);
                        break;
                    }
                    if (*(unsigned char *)(*var_17 + 30UL) != '\x01') {
                        var_104 = *var_54;
                        var_105 = var_104 ^ (-1);
                        var_106 = *var_52;
                        var_107 = var_106 & var_105;
                        _pre_phi288 = var_107;
                        var_111 = var_106;
                        var_112 = var_104;
                        if (var_107 == 0U) {
                            var_108 = local_sp_3 + (-16L);
                            *(uint64_t *)var_108 = 4208780UL;
                            var_109 = indirect_placeholder_17();
                            var_110 = *var_52 & ((uint32_t)var_109 ^ (-1));
                            *var_52 = var_110;
                            _pre284 = *var_54;
                            _pre_phi288 = var_110 & (_pre284 ^ (-1));
                            var_111 = var_110;
                            var_112 = _pre284;
                            local_sp_0 = var_108;
                        }
                        local_sp_1 = local_sp_0;
                        if ((_pre_phi288 == 0U) && ((uint32_t)((uint16_t)var_112 & (unsigned short)448U) == 448U)) {
                            *(uint32_t *)(*var_50 + 24UL) = (var_112 | var_111);
                            *(unsigned char *)(*var_50 + 144UL) = (unsigned char)'\x01';
                        }
                    }
                    local_sp_4 = local_sp_1;
                    var_113 = *var_28;
                    var_114 = local_sp_1 + (-8L);
                    *(uint64_t *)var_114 = 4208892UL;
                    indirect_placeholder();
                    local_sp_4 = var_114;
                    if ((uint32_t)((uint16_t)*var_54 & (unsigned short)448U) != 448U & (uint64_t)(uint32_t)var_113 != 0UL) {
                        var_115 = *var_28;
                        *(uint64_t *)(local_sp_1 + (-16L)) = 4208913UL;
                        var_116 = indirect_placeholder_190(4UL, var_115);
                        var_117 = var_116.field_0;
                        var_118 = var_116.field_1;
                        var_119 = var_116.field_2;
                        *(uint64_t *)(local_sp_1 + (-24L)) = 4208921UL;
                        indirect_placeholder();
                        var_120 = (uint64_t)*(uint32_t *)var_117;
                        *(uint64_t *)(local_sp_1 + (-32L)) = 4208948UL;
                        indirect_placeholder_189(0UL, 4348664UL, var_117, 0UL, var_120, var_118, var_119);
                        break;
                    }
                }
                if ((uint32_t)((uint16_t)*var_54 & (unsigned short)61440U) != 16384U) {
                    var_121 = *var_28;
                    *(uint64_t *)(local_sp_7 + (-16L)) = 4208995UL;
                    var_122 = indirect_placeholder_195(4UL, var_121);
                    var_123 = var_122.field_0;
                    var_124 = var_122.field_1;
                    var_125 = var_122.field_2;
                    *(uint64_t *)(local_sp_7 + (-24L)) = 4209023UL;
                    indirect_placeholder_188(0UL, 4348696UL, var_123, 0UL, 0UL, var_124, var_125);
                    break;
                }
                var_126 = (unsigned char **)var_16;
                **var_126 = (unsigned char)'\x00';
                _pre_phi = var_126;
            }
    }
    return rax_2;
}

typedef unsigned char __u_char;
typedef unsigned short __u_short;
typedef unsigned int __u_int;
typedef unsigned long __u_long;
typedef signed char __int8_t;
typedef unsigned char __uint8_t;
typedef short __int16_t;
typedef unsigned short __uint16_t;
typedef int __int32_t;
typedef unsigned int __uint32_t;
typedef long __int64_t;
typedef unsigned long __uint64_t;
typedef __int8_t __int_least8_t;
typedef __uint8_t __uint_least8_t;
typedef __int16_t __int_least16_t;
typedef __uint16_t __uint_least16_t;
typedef __int32_t __int_least32_t;
typedef __uint32_t __uint_least32_t;
typedef __int64_t __int_least64_t;
typedef __uint64_t __uint_least64_t;
typedef long __quad_t;
typedef unsigned long __u_quad_t;
typedef long __intmax_t;
typedef unsigned long __uintmax_t;
typedef unsigned long __dev_t;
typedef unsigned int __uid_t;
typedef unsigned int __gid_t;
typedef unsigned long __ino_t;
typedef unsigned long __ino64_t;
typedef unsigned int __mode_t;
typedef unsigned long __nlink_t;
typedef long __off_t;
typedef long __off64_t;
typedef int __pid_t;
typedef struct {
    int __val[2];
} __fsid_t;
typedef long __clock_t;
typedef unsigned long __rlim_t;
typedef unsigned long __rlim64_t;
typedef unsigned int __id_t;
typedef long __time_t;
typedef unsigned int __useconds_t;
typedef long __suseconds_t;
typedef int __daddr_t;
typedef int __key_t;
typedef int __clockid_t;
typedef void *__timer_t;
typedef long __blksize_t;
typedef long __blkcnt_t;
typedef long __blkcnt64_t;
typedef unsigned long __fsblkcnt_t;
typedef unsigned long __fsblkcnt64_t;
typedef unsigned long __fsfilcnt_t;
typedef unsigned long __fsfilcnt64_t;
typedef long __fsword_t;
typedef long __ssize_t;
typedef long __syscall_slong_t;
typedef unsigned long __syscall_ulong_t;
typedef __off64_t __loff_t;
typedef char *__caddr_t;
typedef long __intptr_t;
typedef unsigned int __socklen_t;
typedef int __sig_atomic_t;
typedef __int8_t int8_t;
typedef __int16_t int16_t;
typedef __int32_t int32_t;
typedef __int64_t int64_t;
typedef __uint8_t uint8_t;
typedef __uint16_t uint16_t;
typedef __uint32_t uint32_t;
typedef __uint64_t uint64_t;
typedef __int_least8_t int_least8_t;
typedef __int_least16_t int_least16_t;
typedef __int_least32_t int_least32_t;
typedef __int_least64_t int_least64_t;
typedef __uint_least8_t uint_least8_t;
typedef __uint_least16_t uint_least16_t;
typedef __uint_least32_t uint_least32_t;
typedef __uint_least64_t uint_least64_t;
typedef signed char int_fast8_t;
typedef long int_fast16_t;
typedef long int_fast32_t;
typedef long int_fast64_t;
typedef unsigned char uint_fast8_t;
typedef unsigned long uint_fast16_t;
typedef unsigned long uint_fast32_t;
typedef unsigned long uint_fast64_t;
typedef long intptr_t;
typedef unsigned long uintptr_t;
typedef __intmax_t intmax_t;
typedef __uintmax_t uintmax_t;
struct bb_backupfile_internal_ret_type;
struct indirect_placeholder_155_ret_type;
struct bb_backupfile_internal_ret_type {
    uint64_t field_0;
    uint64_t field_1;
    uint64_t field_2;
};
struct indirect_placeholder_155_ret_type {
    uint64_t field_0;
    uint64_t field_1;
};
extern uint64_t revng_init_local_sp(uint64_t param_0);
extern uint64_t init_rbp(void);
extern uint64_t init_cc_src2(void);
extern uint64_t helper_cc_compute_c_wrapper(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint32_t param_3);
extern void indirect_placeholder(void);
extern uint64_t indirect_placeholder_18(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint64_t param_3, uint64_t param_4);
extern uint64_t indirect_placeholder_2(uint64_t param_0);
extern uint64_t init_r8(void);
extern uint64_t init_r9(void);
extern void indirect_placeholder_118(uint64_t param_0);
extern void indirect_placeholder_22(uint64_t param_0, uint64_t param_1, uint64_t param_2);
extern struct indirect_placeholder_155_ret_type indirect_placeholder_155(uint64_t param_0);
struct bb_backupfile_internal_ret_type bb_backupfile_internal(uint64_t rdx, uint64_t rdi, uint64_t rsi) {
    uint64_t var_49;
    uint64_t var_50;
    uint64_t local_sp_4;
    uint32_t var_51;
    uint32_t var_52;
    uint64_t var_53;
    uint64_t var_54;
    uint64_t var_55;
    uint64_t var_56;
    uint64_t var_57;
    uint64_t var_48;
    uint64_t var_37;
    uint64_t var_58;
    uint64_t r8_3;
    uint64_t var_0;
    uint64_t var_1;
    uint64_t var_2;
    uint64_t var_3;
    uint64_t var_4;
    uint64_t *var_5;
    uint32_t *var_6;
    unsigned char *var_7;
    uint64_t var_8;
    uint64_t var_9;
    uint64_t var_10;
    uint64_t *var_11;
    uint64_t var_12;
    uint64_t var_13;
    uint64_t var_14;
    uint64_t *var_15;
    uint64_t var_16;
    uint64_t var_18;
    uint64_t local_sp_0;
    uint64_t var_61;
    uint32_t *var_62;
    uint64_t rax_1;
    uint64_t var_59;
    uint32_t var_60;
    uint64_t local_sp_2;
    uint64_t var_39;
    uint64_t local_sp_5;
    uint32_t *var_40;
    uint64_t local_sp_1;
    uint64_t var_41;
    uint64_t var_42;
    uint64_t var_43;
    uint64_t var_44;
    uint64_t var_45;
    uint64_t var_46;
    uint64_t var_47;
    uint64_t r8_0;
    uint64_t r8_1;
    uint64_t local_sp_3;
    uint64_t rax_0;
    uint64_t var_33;
    uint64_t var_34;
    uint64_t var_35;
    uint64_t var_36;
    uint32_t var_38;
    uint64_t r8_2;
    struct bb_backupfile_internal_ret_type mrv;
    struct bb_backupfile_internal_ret_type mrv1;
    struct bb_backupfile_internal_ret_type mrv2;
    uint64_t var_28;
    uint64_t *var_29;
    uint32_t *var_30;
    uint32_t *var_31;
    uint32_t *var_32;
    uint64_t var_17;
    uint64_t local_sp_6;
    uint64_t var_19;
    uint64_t *var_20;
    uint64_t spec_store_select;
    uint64_t var_21;
    uint64_t *var_22;
    uint64_t var_23;
    struct indirect_placeholder_155_ret_type var_24;
    uint64_t var_25;
    uint64_t var_26;
    uint64_t *var_27;
    unsigned int loop_state_var;
    var_0 = revng_init_local_sp(0UL);
    var_1 = init_rbp();
    var_2 = init_cc_src2();
    var_3 = init_r8();
    var_4 = init_r9();
    *(uint64_t *)(var_0 + (-8L)) = var_1;
    var_5 = (uint64_t *)(var_0 + (-96L));
    *var_5 = rdi;
    var_6 = (uint32_t *)(var_0 + (-100L));
    *var_6 = (uint32_t)rsi;
    var_7 = (unsigned char *)(var_0 + (-104L));
    *var_7 = (unsigned char)rdx;
    var_8 = *var_5;
    *(uint64_t *)(var_0 + (-112L)) = 4242511UL;
    var_9 = indirect_placeholder_2(var_8);
    var_10 = var_9 - *var_5;
    var_11 = (uint64_t *)(var_0 + (-16L));
    *var_11 = var_10;
    var_12 = *var_5 + var_10;
    var_13 = var_0 + (-120L);
    *(uint64_t *)var_13 = 4242547UL;
    indirect_placeholder();
    var_14 = *var_11 + var_12;
    var_15 = (uint64_t *)(var_0 + (-40L));
    *var_15 = var_14;
    var_16 = *(uint64_t *)6474888UL;
    r8_3 = var_3;
    var_18 = var_16;
    rax_1 = 0UL;
    rax_0 = 4294967295UL;
    r8_2 = var_3;
    local_sp_6 = var_13;
    if (var_16 == 0UL) {
        var_17 = var_0 + (-128L);
        *(uint64_t *)var_17 = 4242583UL;
        indirect_placeholder_118(0UL);
        var_18 = *(uint64_t *)6474888UL;
        local_sp_6 = var_17;
    }
    *(uint64_t *)(local_sp_6 + (-8L)) = 4242598UL;
    indirect_placeholder();
    var_19 = var_18 + 1UL;
    *(uint64_t *)(var_0 + (-48L)) = var_19;
    var_20 = (uint64_t *)(var_0 + (-24L));
    spec_store_select = (var_19 > 8UL) ? var_19 : 9UL;
    *var_20 = spec_store_select;
    var_21 = (spec_store_select + *var_15) + 1UL;
    var_22 = (uint64_t *)(var_0 + (-56L));
    *var_22 = var_21;
    var_23 = local_sp_6 + (-16L);
    *(uint64_t *)var_23 = 4242660UL;
    var_24 = indirect_placeholder_155(var_21);
    var_25 = var_24.field_0;
    var_26 = var_0 + (-72L);
    var_27 = (uint64_t *)var_26;
    *var_27 = var_25;
    local_sp_5 = var_23;
    if (var_25 == 0UL) {
        mrv.field_0 = rax_1;
        mrv1 = mrv;
        mrv1.field_1 = r8_2;
        mrv2 = mrv1;
        mrv2.field_2 = var_4;
        return mrv2;
    }
    var_28 = var_0 + (-80L);
    var_29 = (uint64_t *)var_28;
    *var_29 = 0UL;
    var_30 = (uint32_t *)(var_0 + (-28L));
    var_31 = (uint32_t *)(var_0 + (-60L));
    var_32 = (uint32_t *)(var_0 + (-64L));
    r8_0 = var_28;
    r8_2 = var_28;
    while (1U)
        {
            *(uint64_t *)(local_sp_5 + (-8L)) = 4242717UL;
            indirect_placeholder();
            if (*var_6 == 1U) {
                var_48 = local_sp_5 + (-16L);
                *(uint64_t *)var_48 = 4242756UL;
                indirect_placeholder();
                r8_0 = r8_3;
                local_sp_2 = var_48;
            } else {
                var_33 = *var_22;
                var_34 = *var_11;
                var_35 = *var_15;
                var_36 = local_sp_5 + (-16L);
                *(uint64_t *)var_36 = 4242792UL;
                var_37 = indirect_placeholder_18(var_35, var_34, var_26, var_33, var_28);
                var_38 = (uint32_t)var_37;
                local_sp_1 = var_36;
                local_sp_2 = var_36;
                if ((uint64_t)(var_38 + (-1)) == 0UL) {
                    var_45 = *var_27;
                    var_46 = *var_15;
                    var_47 = local_sp_5 + (-24L);
                    *(uint64_t *)var_47 = 4242917UL;
                    indirect_placeholder_22(126UL, var_45, var_46);
                    local_sp_2 = var_47;
                } else {
                    var_39 = helper_cc_compute_c_wrapper(var_37 + (-1L), 1UL, var_2, 16U);
                    if (var_39 != 0UL) {
                        if ((uint64_t)(var_38 + (-2)) != 0UL) {
                            if ((uint64_t)(var_38 + (-3)) != 0UL) {
                                var_40 = *(uint32_t **)var_26;
                                *(uint64_t *)(local_sp_5 + (-24L)) = 4242931UL;
                                indirect_placeholder();
                                *(uint64_t *)(local_sp_5 + (-32L)) = 4242936UL;
                                indirect_placeholder();
                                *var_40 = 12U;
                                loop_state_var = 0U;
                                break;
                            }
                        }
                        if (*var_6 == 2U) {
                            *var_6 = 1U;
                            var_41 = local_sp_5 + (-24L);
                            *(uint64_t *)var_41 = 4242867UL;
                            indirect_placeholder();
                            local_sp_1 = var_41;
                        }
                        var_42 = *var_27;
                        var_43 = *var_15;
                        var_44 = local_sp_1 + (-8L);
                        *(uint64_t *)var_44 = 4242891UL;
                        indirect_placeholder_22(126UL, var_42, var_43);
                        local_sp_2 = var_44;
                    }
                }
            }
        }
    switch (loop_state_var) {
      case 0U:
        {
            break;
        }
        break;
      case 2U:
        {
            var_62 = *(uint32_t **)var_26;
            *(uint64_t *)(local_sp_0 + (-8L)) = 4243127UL;
            indirect_placeholder();
            *(uint64_t *)(local_sp_0 + (-16L)) = 4243132UL;
            indirect_placeholder();
            *var_62 = *var_32;
        }
        break;
      case 1U:
        {
            r8_2 = r8_1;
            if (*var_29 != 0UL) {
                *(uint64_t *)(local_sp_3 + (-8L)) = 4243173UL;
                indirect_placeholder();
            }
            rax_1 = *var_27;
        }
        break;
    }
}

typedef unsigned char __u_char;
typedef unsigned short __u_short;
typedef unsigned int __u_int;
typedef unsigned long __u_long;
typedef signed char __int8_t;
typedef unsigned char __uint8_t;
typedef short __int16_t;
typedef unsigned short __uint16_t;
typedef int __int32_t;
typedef unsigned int __uint32_t;
typedef long __int64_t;
typedef unsigned long __uint64_t;
typedef __int8_t __int_least8_t;
typedef __uint8_t __uint_least8_t;
typedef __int16_t __int_least16_t;
typedef __uint16_t __uint_least16_t;
typedef __int32_t __int_least32_t;
typedef __uint32_t __uint_least32_t;
typedef __int64_t __int_least64_t;
typedef __uint64_t __uint_least64_t;
typedef long __quad_t;
typedef unsigned long __u_quad_t;
typedef long __intmax_t;
typedef unsigned long __uintmax_t;
typedef unsigned long __dev_t;
typedef unsigned int __uid_t;
typedef unsigned int __gid_t;
typedef unsigned long __ino_t;
typedef unsigned long __ino64_t;
typedef unsigned int __mode_t;
typedef unsigned long __nlink_t;
typedef long __off_t;
typedef long __off64_t;
typedef int __pid_t;
typedef struct {
    int __val[2];
} __fsid_t;
typedef long __clock_t;
typedef unsigned long __rlim_t;
typedef unsigned long __rlim64_t;
typedef unsigned int __id_t;
typedef long __time_t;
typedef unsigned int __useconds_t;
typedef long __suseconds_t;
typedef int __daddr_t;
typedef int __key_t;
typedef int __clockid_t;
typedef void *__timer_t;
typedef long __blksize_t;
typedef long __blkcnt_t;
typedef long __blkcnt64_t;
typedef unsigned long __fsblkcnt_t;
typedef unsigned long __fsblkcnt64_t;
typedef unsigned long __fsfilcnt_t;
typedef unsigned long __fsfilcnt64_t;
typedef long __fsword_t;
typedef long __ssize_t;
typedef long __syscall_slong_t;
typedef unsigned long __syscall_ulong_t;
typedef __off64_t __loff_t;
typedef char *__caddr_t;
typedef long __intptr_t;
typedef unsigned int __socklen_t;
typedef int __sig_atomic_t;
typedef __int8_t int8_t;
typedef __int16_t int16_t;
typedef __int32_t int32_t;
typedef __int64_t int64_t;
typedef __uint8_t uint8_t;
typedef __uint16_t uint16_t;
typedef __uint32_t uint32_t;
typedef __uint64_t uint64_t;
typedef __int_least8_t int_least8_t;
typedef __int_least16_t int_least16_t;
typedef __int_least32_t int_least32_t;
typedef __int_least64_t int_least64_t;
typedef __uint_least8_t uint_least8_t;
typedef __uint_least16_t uint_least16_t;
typedef __uint_least32_t uint_least32_t;
typedef __uint_least64_t uint_least64_t;
typedef signed char int_fast8_t;
typedef long int_fast16_t;
typedef long int_fast32_t;
typedef long int_fast64_t;
typedef unsigned char uint_fast8_t;
typedef unsigned long uint_fast16_t;
typedef unsigned long uint_fast32_t;
typedef unsigned long uint_fast64_t;
typedef long intptr_t;
typedef unsigned long uintptr_t;
typedef __intmax_t intmax_t;
typedef __uintmax_t uintmax_t;
struct indirect_placeholder_268_ret_type;
struct indirect_placeholder_267_ret_type;
struct indirect_placeholder_266_ret_type;
struct indirect_placeholder_269_ret_type;
struct indirect_placeholder_268_ret_type {
    uint64_t field_0;
    uint64_t field_1;
};
struct indirect_placeholder_267_ret_type {
    uint64_t field_0;
    uint64_t field_1;
};
struct indirect_placeholder_266_ret_type {
    uint64_t field_0;
    uint64_t field_1;
    uint64_t field_2;
};
struct indirect_placeholder_269_ret_type {
    uint64_t field_0;
    uint64_t field_1;
    uint64_t field_2;
};
extern uint64_t revng_init_local_sp(uint64_t param_0);
extern uint64_t init_rbp(void);
extern void indirect_placeholder(void);
extern uint64_t init_rbx(void);
extern uint64_t indirect_placeholder_50(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint64_t param_3, uint64_t param_4, uint64_t param_5);
extern uint64_t init_r12(void);
extern struct indirect_placeholder_268_ret_type indirect_placeholder_268(uint64_t param_0, uint64_t param_1, uint64_t param_2);
extern struct indirect_placeholder_267_ret_type indirect_placeholder_267(uint64_t param_0, uint64_t param_1, uint64_t param_2);
extern struct indirect_placeholder_266_ret_type indirect_placeholder_266(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint64_t param_3, uint64_t param_4, uint64_t param_5, uint64_t param_6);
extern struct indirect_placeholder_269_ret_type indirect_placeholder_269(uint64_t param_0, uint64_t param_1);
uint64_t bb_create_hard_link(uint64_t rdx, uint64_t rcx, uint64_t rdi, uint64_t rsi, uint64_t r8) {
    uint64_t var_0;
    uint64_t var_1;
    uint64_t var_2;
    uint64_t var_3;
    uint64_t *var_4;
    uint64_t *var_5;
    unsigned char *var_6;
    unsigned char *var_7;
    unsigned char *var_8;
    unsigned char var_9;
    uint64_t var_10;
    uint64_t spec_select;
    uint64_t var_11;
    uint64_t var_12;
    uint64_t var_13;
    uint32_t *var_14;
    uint32_t var_15;
    uint64_t var_16;
    struct indirect_placeholder_268_ret_type var_17;
    uint64_t var_18;
    uint64_t var_19;
    struct indirect_placeholder_267_ret_type var_20;
    uint64_t var_21;
    uint64_t var_22;
    uint64_t var_23;
    uint64_t storemerge1;
    uint64_t var_24;
    var_0 = revng_init_local_sp(0UL);
    var_1 = init_rbp();
    var_2 = init_rbx();
    var_3 = init_r12();
    *(uint64_t *)(var_0 + (-8L)) = var_1;
    *(uint64_t *)(var_0 + (-16L)) = var_3;
    *(uint64_t *)(var_0 + (-24L)) = var_2;
    var_4 = (uint64_t *)(var_0 + (-48L));
    *var_4 = rdi;
    var_5 = (uint64_t *)(var_0 + (-56L));
    *var_5 = rsi;
    var_6 = (unsigned char *)(var_0 + (-60L));
    *var_6 = (unsigned char)rdx;
    var_7 = (unsigned char *)(var_0 + (-64L));
    *var_7 = (unsigned char)rcx;
    var_8 = (unsigned char *)(var_0 + (-68L));
    var_9 = (unsigned char)r8;
    *var_8 = var_9;
    var_10 = (uint64_t)*var_6;
    spec_select = (var_9 == '\x00') ? 0UL : 1024UL;
    var_11 = *var_5;
    var_12 = *var_4;
    *(uint64_t *)(var_0 + (-80L)) = 4225724UL;
    var_13 = indirect_placeholder_50(4294967196UL, var_11, 4294967196UL, var_12, spec_select, var_10);
    var_14 = (uint32_t *)(var_0 + (-28L));
    var_15 = (uint32_t)var_13;
    *var_14 = var_15;
    storemerge1 = 1UL;
    if ((int)var_15 > (int)4294967295U) {
        var_16 = *var_4;
        *(uint64_t *)(var_0 + (-88L)) = 4225755UL;
        var_17 = indirect_placeholder_268(var_16, 1UL, 4UL);
        var_18 = var_17.field_0;
        var_19 = *var_5;
        *(uint64_t *)(var_0 + (-96L)) = 4225780UL;
        var_20 = indirect_placeholder_267(var_19, 0UL, 4UL);
        var_21 = var_20.field_0;
        var_22 = var_20.field_1;
        *(uint64_t *)(var_0 + (-104L)) = 4225788UL;
        indirect_placeholder();
        var_23 = (uint64_t)*(uint32_t *)var_21;
        *(uint64_t *)(var_0 + (-112L)) = 4225818UL;
        indirect_placeholder_266(0UL, 4353016UL, var_21, 0UL, var_23, var_18, var_22);
        storemerge1 = 0UL;
    } else {
        if ((int)var_15 <= (int)0U & *var_7 == '\x00') {
            var_24 = *var_5;
            *(uint64_t *)(var_0 + (-88L)) = 4225854UL;
            indirect_placeholder_269(4UL, var_24);
            *(uint64_t *)(var_0 + (-96L)) = 4225872UL;
            indirect_placeholder();
        }
    }
    return storemerge1;
}

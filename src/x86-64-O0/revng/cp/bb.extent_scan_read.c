typedef unsigned char __u_char;
typedef unsigned short __u_short;
typedef unsigned int __u_int;
typedef unsigned long __u_long;
typedef signed char __int8_t;
typedef unsigned char __uint8_t;
typedef short __int16_t;
typedef unsigned short __uint16_t;
typedef int __int32_t;
typedef unsigned int __uint32_t;
typedef long __int64_t;
typedef unsigned long __uint64_t;
typedef __int8_t __int_least8_t;
typedef __uint8_t __uint_least8_t;
typedef __int16_t __int_least16_t;
typedef __uint16_t __uint_least16_t;
typedef __int32_t __int_least32_t;
typedef __uint32_t __uint_least32_t;
typedef __int64_t __int_least64_t;
typedef __uint64_t __uint_least64_t;
typedef long __quad_t;
typedef unsigned long __u_quad_t;
typedef long __intmax_t;
typedef unsigned long __uintmax_t;
typedef unsigned long __dev_t;
typedef unsigned int __uid_t;
typedef unsigned int __gid_t;
typedef unsigned long __ino_t;
typedef unsigned long __ino64_t;
typedef unsigned int __mode_t;
typedef unsigned long __nlink_t;
typedef long __off_t;
typedef long __off64_t;
typedef int __pid_t;
typedef struct {
    int __val[2];
} __fsid_t;
typedef long __clock_t;
typedef unsigned long __rlim_t;
typedef unsigned long __rlim64_t;
typedef unsigned int __id_t;
typedef long __time_t;
typedef unsigned int __useconds_t;
typedef long __suseconds_t;
typedef int __daddr_t;
typedef int __key_t;
typedef int __clockid_t;
typedef void *__timer_t;
typedef long __blksize_t;
typedef long __blkcnt_t;
typedef long __blkcnt64_t;
typedef unsigned long __fsblkcnt_t;
typedef unsigned long __fsblkcnt64_t;
typedef unsigned long __fsfilcnt_t;
typedef unsigned long __fsfilcnt64_t;
typedef long __fsword_t;
typedef long __ssize_t;
typedef long __syscall_slong_t;
typedef unsigned long __syscall_ulong_t;
typedef __off64_t __loff_t;
typedef char *__caddr_t;
typedef long __intptr_t;
typedef unsigned int __socklen_t;
typedef int __sig_atomic_t;
typedef __int8_t int8_t;
typedef __int16_t int16_t;
typedef __int32_t int32_t;
typedef __int64_t int64_t;
typedef __uint8_t uint8_t;
typedef __uint16_t uint16_t;
typedef __uint32_t uint32_t;
typedef __uint64_t uint64_t;
typedef __int_least8_t int_least8_t;
typedef __int_least16_t int_least16_t;
typedef __int_least32_t int_least32_t;
typedef __int_least64_t int_least64_t;
typedef __uint_least8_t uint_least8_t;
typedef __uint_least16_t uint_least16_t;
typedef __uint_least32_t uint_least32_t;
typedef __uint_least64_t uint_least64_t;
typedef signed char int_fast8_t;
typedef long int_fast16_t;
typedef long int_fast32_t;
typedef long int_fast64_t;
typedef unsigned char uint_fast8_t;
typedef unsigned long uint_fast16_t;
typedef unsigned long uint_fast32_t;
typedef unsigned long uint_fast64_t;
typedef long intptr_t;
typedef unsigned long uintptr_t;
typedef __intmax_t intmax_t;
typedef __uintmax_t uintmax_t;
typedef _Bool bool;
struct bb_extent_scan_read_ret_type;
struct bb_extent_scan_read_ret_type {
    uint64_t field_0;
    uint64_t field_1;
    uint64_t field_2;
    uint64_t field_3;
    uint64_t field_4;
    uint64_t field_5;
};
extern uint64_t revng_init_local_sp(uint64_t param_0);
extern uint64_t init_rbp(void);
extern void indirect_placeholder(void);
extern uint64_t init_rcx(void);
extern uint64_t init_rbx(void);
extern uint64_t init_r10(void);
extern uint64_t init_r8(void);
extern uint64_t init_r9(void);
extern uint64_t indirect_placeholder_110(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint64_t param_3, uint64_t param_4, uint64_t param_5, uint64_t param_6, uint64_t param_7);
struct bb_extent_scan_read_ret_type bb_extent_scan_read(uint64_t rdi) {
    uint64_t var_0;
    uint64_t var_1;
    uint64_t var_2;
    uint64_t var_3;
    uint64_t var_4;
    uint64_t var_5;
    uint64_t var_6;
    uint64_t var_7;
    uint64_t *var_8;
    uint32_t *var_9;
    uint64_t var_10;
    uint64_t var_11;
    uint64_t *var_12;
    uint64_t var_13;
    uint64_t var_14;
    uint64_t *var_15;
    uint64_t *var_16;
    uint64_t *var_17;
    uint32_t *var_18;
    uint64_t *var_19;
    uint64_t *var_20;
    uint64_t rcx_5;
    uint32_t *_pre_phi185;
    uint64_t rcx_3;
    uint32_t var_34;
    uint64_t rcx_0;
    uint64_t local_sp_0;
    uint32_t var_35;
    uint32_t var_39;
    uint64_t var_36;
    uint64_t rcx_1;
    uint64_t var_37;
    uint32_t var_38;
    uint64_t var_40;
    uint64_t rcx_4;
    uint64_t var_41;
    uint64_t local_sp_1;
    uint64_t var_42;
    uint32_t var_43;
    bool var_44;
    uint64_t var_45;
    uint32_t var_46;
    uint64_t var_47;
    uint64_t var_48;
    uint64_t *var_49;
    uint64_t var_50;
    uint64_t var_51;
    uint64_t rcx_2;
    uint64_t **_pre_phi187;
    uint64_t **_pre186;
    uint64_t **var_52;
    uint64_t var_53;
    uint64_t var_56;
    uint64_t var_57;
    uint64_t var_55;
    uint64_t var_58;
    uint64_t var_59;
    uint64_t var_63;
    uint64_t local_sp_3;
    uint64_t var_60;
    uint64_t *var_61;
    uint64_t var_54;
    uint32_t var_62;
    uint64_t var_21;
    uint32_t *var_22;
    uint32_t var_23;
    bool var_24;
    uint64_t var_25;
    uint64_t rax_0;
    uint64_t var_64;
    struct bb_extent_scan_read_ret_type mrv;
    struct bb_extent_scan_read_ret_type mrv1;
    struct bb_extent_scan_read_ret_type mrv2;
    struct bb_extent_scan_read_ret_type mrv3;
    struct bb_extent_scan_read_ret_type mrv4;
    struct bb_extent_scan_read_ret_type mrv5;
    uint64_t *var_26;
    uint64_t var_27;
    uint64_t *_pre_phi181;
    uint64_t local_sp_2;
    uint64_t var_28;
    uint64_t var_29;
    uint64_t var_30;
    uint64_t var_31;
    uint64_t var_32;
    uint64_t var_33;
    unsigned int loop_state_var;
    bool switch_state_var;
    var_0 = revng_init_local_sp(0UL);
    var_1 = init_rbp();
    var_2 = init_rcx();
    var_3 = init_rbx();
    var_4 = init_r8();
    var_5 = init_r10();
    var_6 = init_r9();
    *(uint64_t *)(var_0 + (-8L)) = var_1;
    var_7 = var_0 + (-4184L);
    var_8 = (uint64_t *)(var_0 + (-4176L));
    *var_8 = rdi;
    var_9 = (uint32_t *)(var_0 + (-12L));
    *var_9 = 0U;
    var_10 = *(uint64_t *)(*var_8 + 40UL);
    var_11 = var_0 + (-24L);
    var_12 = (uint64_t *)var_11;
    *var_12 = var_10;
    var_13 = var_0 + (-4168L);
    var_14 = var_0 + (-48L);
    var_15 = (uint64_t *)var_14;
    var_16 = (uint64_t *)(var_0 + (-56L));
    var_17 = (uint64_t *)(var_0 + (-64L));
    var_18 = (uint32_t *)(var_0 + (-28L));
    var_19 = (uint64_t *)(var_0 + (-40L));
    var_20 = (uint64_t *)(var_0 + (-72L));
    var_34 = 0U;
    rcx_5 = var_2;
    local_sp_3 = var_7;
    rax_0 = 0UL;
    while (1U)
        {
            *var_15 = var_13;
            *var_16 = (var_13 + 32UL);
            *(uint64_t *)(local_sp_3 + (-8L)) = 4237061UL;
            indirect_placeholder();
            **(uint64_t **)var_14 = *(uint64_t *)(*var_8 + 8UL);
            *(uint32_t *)(*var_15 + 16UL) = *(uint32_t *)(*var_8 + 16UL);
            *(uint32_t *)(*var_15 + 24UL) = 72U;
            *(uint64_t *)(*var_15 + 8UL) = (*(uint64_t *)(*var_8 + 8UL) ^ (-1L));
            var_21 = local_sp_3 + (-16L);
            *(uint64_t *)var_21 = 4237165UL;
            indirect_placeholder();
            var_22 = (uint32_t *)(*var_15 + 20UL);
            var_23 = *var_22;
            var_24 = (var_23 == 0U);
            var_25 = *var_8;
            rcx_4 = rcx_5;
            _pre_phi185 = var_22;
            local_sp_2 = var_21;
            if (!var_24) {
                *(unsigned char *)(var_25 + 33UL) = (unsigned char)'\x01';
                var_64 = *(uint64_t *)(*var_8 + 8UL);
                rax_0 = (var_64 & (-256L)) | (var_64 != 0UL);
                break;
            }
            var_26 = (uint64_t *)(var_25 + 24UL);
            _pre_phi181 = var_26;
            if (*var_26 > ((uint64_t)var_23 ^ (-1L))) {
                var_27 = local_sp_3 + (-24L);
                *(uint64_t *)var_27 = 4237303UL;
                indirect_placeholder();
                _pre_phi185 = (uint32_t *)(*var_15 + 20UL);
                _pre_phi181 = (uint64_t *)(*var_8 + 24UL);
                local_sp_2 = var_27;
            }
            *_pre_phi181 = (*_pre_phi181 + (uint64_t)*_pre_phi185);
            *var_17 = ((uint64_t)((long)(*var_12 - *(uint64_t *)(*var_8 + 40UL)) >> (long)3UL) * 12297829382473034411UL);
            var_28 = *var_8;
            var_29 = *(uint64_t *)(var_28 + 24UL);
            var_30 = *(uint64_t *)(var_28 + 40UL);
            var_31 = local_sp_2 + (-8L);
            *(uint64_t *)var_31 = 4237424UL;
            var_32 = indirect_placeholder_110(24UL, var_29, var_30, var_3, var_29, var_4, var_5, var_6);
            *(uint64_t *)(*var_8 + 40UL) = var_32;
            var_33 = *(uint64_t *)(*var_8 + 40UL);
            *var_12 = ((*var_17 * 24UL) + var_33);
            *var_18 = 0U;
            rcx_0 = var_33;
            local_sp_0 = var_31;
            while (1U)
                {
                    rcx_1 = rcx_0;
                    local_sp_1 = local_sp_0;
                    local_sp_3 = local_sp_0;
                    if (*(uint32_t *)(*var_15 + 20UL) <= var_34) {
                        loop_state_var = 1U;
                        break;
                    }
                    var_41 = *var_16 + ((uint64_t)var_34 * 56UL);
                    if (*(uint64_t *)var_41 > (9223372036854775807UL - *(uint64_t *)(var_41 + 16UL))) {
                        var_42 = local_sp_0 + (-8L);
                        *(uint64_t *)var_42 = 4237595UL;
                        indirect_placeholder();
                        local_sp_1 = var_42;
                    }
                    var_43 = *var_9;
                    var_44 = (var_43 == 0U);
                    local_sp_0 = local_sp_1;
                    if (!var_44) {
                        var_45 = *var_12;
                        var_46 = *(uint32_t *)(var_45 + 16UL);
                        var_47 = *var_16 + ((uint64_t)*var_18 * 56UL);
                        var_48 = **(uint64_t **)var_11;
                        var_49 = (uint64_t *)(var_45 + 8UL);
                        var_50 = *var_49;
                        rcx_3 = var_50;
                        if ((uint64_t)(var_46 - (*(uint32_t *)(var_47 + 40UL) & (-2))) != 0UL & (var_50 + var_48) != *(uint64_t *)var_47) {
                            *var_49 = (*(uint64_t *)(var_47 + 16UL) + var_50);
                            *(uint32_t *)(*var_12 + 16UL) = *(uint32_t *)((*var_16 + ((uint64_t)*var_18 * 56UL)) + 40UL);
                            var_62 = *var_18 + 1U;
                            *var_18 = var_62;
                            var_34 = var_62;
                            rcx_0 = rcx_3;
                            continue;
                        }
                    }
                    if (var_44) {
                        var_51 = *(uint64_t *)(*var_8 + 8UL);
                        rcx_2 = var_51;
                        if (var_51 <= *(uint64_t *)(*var_16 + ((uint64_t)*var_18 * 56UL))) {
                            if (!var_44) {
                                _pre186 = (uint64_t **)var_11;
                                _pre_phi187 = _pre186;
                                var_54 = *(uint64_t *)(*var_8 + 40UL);
                                *var_12 = (((uint64_t)var_43 * 24UL) + var_54);
                                **_pre_phi187 = *(uint64_t *)(*var_16 + ((uint64_t)*var_18 * 56UL));
                                *(uint64_t *)(*var_12 + 8UL) = *(uint64_t *)((*var_16 + ((uint64_t)*var_18 * 56UL)) + 16UL);
                                *(uint32_t *)(*var_12 + 16UL) = *(uint32_t *)((*var_16 + ((uint64_t)*var_18 * 56UL)) + 40UL);
                                *var_9 = (*var_9 + 1U);
                                rcx_3 = var_54;
                                var_62 = *var_18 + 1U;
                                *var_18 = var_62;
                                var_34 = var_62;
                                rcx_0 = rcx_3;
                                continue;
                            }
                            var_52 = (uint64_t **)var_11;
                            var_53 = *(uint64_t *)(*var_12 + 8UL) + **var_52;
                            rcx_2 = var_53;
                            _pre_phi187 = var_52;
                            if (var_53 <= *(uint64_t *)(*var_16 + ((uint64_t)*var_18 * 56UL))) {
                                var_54 = *(uint64_t *)(*var_8 + 40UL);
                                *var_12 = (((uint64_t)var_43 * 24UL) + var_54);
                                **_pre_phi187 = *(uint64_t *)(*var_16 + ((uint64_t)*var_18 * 56UL));
                                *(uint64_t *)(*var_12 + 8UL) = *(uint64_t *)((*var_16 + ((uint64_t)*var_18 * 56UL)) + 16UL);
                                *(uint32_t *)(*var_12 + 16UL) = *(uint32_t *)((*var_16 + ((uint64_t)*var_18 * 56UL)) + 40UL);
                                *var_9 = (*var_9 + 1U);
                                rcx_3 = var_54;
                                var_62 = *var_18 + 1U;
                                *var_18 = var_62;
                                var_34 = var_62;
                                rcx_0 = rcx_3;
                                continue;
                            }
                        }
                    }
                    if (var_44) {
                        _pre186 = (uint64_t **)var_11;
                        _pre_phi187 = _pre186;
                        var_54 = *(uint64_t *)(*var_8 + 40UL);
                        *var_12 = (((uint64_t)var_43 * 24UL) + var_54);
                        **_pre_phi187 = *(uint64_t *)(*var_16 + ((uint64_t)*var_18 * 56UL));
                        *(uint64_t *)(*var_12 + 8UL) = *(uint64_t *)((*var_16 + ((uint64_t)*var_18 * 56UL)) + 16UL);
                        *(uint32_t *)(*var_12 + 16UL) = *(uint32_t *)((*var_16 + ((uint64_t)*var_18 * 56UL)) + 40UL);
                        *var_9 = (*var_9 + 1U);
                        rcx_3 = var_54;
                        var_62 = *var_18 + 1U;
                        *var_18 = var_62;
                        var_34 = var_62;
                        rcx_0 = rcx_3;
                        continue;
                    }
                    var_52 = (uint64_t **)var_11;
                    var_53 = *(uint64_t *)(*var_12 + 8UL) + **var_52;
                    rcx_2 = var_53;
                    _pre_phi187 = var_52;
                    if (var_53 > *(uint64_t *)(*var_16 + ((uint64_t)*var_18 * 56UL))) {
                        rcx_4 = rcx_2;
                        if (var_44) {
                            var_56 = *(uint64_t *)(*var_8 + 8UL);
                            *var_19 = var_56;
                            var_57 = var_56;
                        } else {
                            var_55 = *(uint64_t *)(*var_12 + 8UL) + **(uint64_t **)var_11;
                            *var_19 = var_55;
                            var_57 = var_55;
                        }
                        var_58 = var_57 - *(uint64_t *)(*var_16 + ((uint64_t)*var_18 * 56UL));
                        *var_20 = var_58;
                        var_59 = *var_16 + ((uint64_t)*var_18 * 56UL);
                        if (*(uint64_t *)(var_59 + 16UL) <= var_58) {
                            var_63 = *var_8;
                            if (*(uint64_t *)(var_63 + 8UL) != 0UL) {
                                loop_state_var = 0U;
                                break;
                            }
                            *(unsigned char *)(var_63 + 32UL) = (unsigned char)'\x01';
                            loop_state_var = 0U;
                            break;
                        }
                        *(uint64_t *)var_59 = *var_19;
                        var_60 = (uint64_t)*var_18 * 56UL;
                        var_61 = (uint64_t *)((var_60 + *var_16) + 16UL);
                        *var_61 = (*var_61 - *var_20);
                        *var_18 = (*var_18 + (-1));
                        rcx_3 = var_60;
                        var_62 = *var_18 + 1U;
                        *var_18 = var_62;
                        var_34 = var_62;
                        rcx_0 = rcx_3;
                        continue;
                    }
                    var_54 = *(uint64_t *)(*var_8 + 40UL);
                    *var_12 = (((uint64_t)var_43 * 24UL) + var_54);
                    **_pre_phi187 = *(uint64_t *)(*var_16 + ((uint64_t)*var_18 * 56UL));
                    *(uint64_t *)(*var_12 + 8UL) = *(uint64_t *)((*var_16 + ((uint64_t)*var_18 * 56UL)) + 16UL);
                    *(uint32_t *)(*var_12 + 16UL) = *(uint32_t *)((*var_16 + ((uint64_t)*var_18 * 56UL)) + 40UL);
                    *var_9 = (*var_9 + 1U);
                    rcx_3 = var_54;
                    var_62 = *var_18 + 1U;
                    *var_18 = var_62;
                    var_34 = var_62;
                    rcx_0 = rcx_3;
                    continue;
                }
            switch_state_var = 0;
            switch (loop_state_var) {
              case 0U:
                {
                    switch_state_var = 1;
                    break;
                }
                break;
              case 1U:
                {
                    rax_0 = 1UL;
                    if ((*(uint32_t *)(*var_12 + 16UL) & 1U) == 0U) {
                        *(unsigned char *)(*var_8 + 33UL) = (unsigned char)'\x01';
                    }
                    var_35 = *var_9;
                    var_39 = var_35;
                    var_36 = *var_8;
                    if (var_35 <= 72U & *(unsigned char *)(var_36 + 33UL) == '\x01') {
                        var_37 = *(uint64_t *)(var_36 + 40UL);
                        var_38 = var_35 + (-1);
                        *var_9 = var_38;
                        *var_12 = ((((uint64_t)var_38 * 24UL) + (-24L)) + var_37);
                        var_39 = *var_9;
                        rcx_1 = var_37;
                    }
                    *(uint64_t *)(*var_8 + 24UL) = (uint64_t)var_39;
                    var_40 = *var_8;
                    rcx_5 = rcx_1;
                    rcx_4 = rcx_1;
                    if (*(unsigned char *)(var_40 + 33UL) == '\x00') {
                        switch_state_var = 1;
                        break;
                    }
                    *(uint64_t *)(var_40 + 8UL) = (**(uint64_t **)var_11 + *(uint64_t *)(*var_12 + 8UL));
                    if (*var_9 <= 71U) {
                        continue;
                    }
                    switch_state_var = 1;
                    break;
                }
                break;
            }
            if (switch_state_var)
                break;
        }
    mrv.field_0 = rax_0;
    mrv1 = mrv;
    mrv1.field_1 = rcx_4;
    mrv2 = mrv1;
    mrv2.field_2 = var_3;
    mrv3 = mrv2;
    mrv3.field_3 = var_4;
    mrv4 = mrv3;
    mrv4.field_4 = var_5;
    mrv5 = mrv4;
    mrv5.field_5 = var_6;
    return mrv5;
}

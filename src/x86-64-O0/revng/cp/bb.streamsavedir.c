typedef unsigned char __u_char;
typedef unsigned short __u_short;
typedef unsigned int __u_int;
typedef unsigned long __u_long;
typedef signed char __int8_t;
typedef unsigned char __uint8_t;
typedef short __int16_t;
typedef unsigned short __uint16_t;
typedef int __int32_t;
typedef unsigned int __uint32_t;
typedef long __int64_t;
typedef unsigned long __uint64_t;
typedef __int8_t __int_least8_t;
typedef __uint8_t __uint_least8_t;
typedef __int16_t __int_least16_t;
typedef __uint16_t __uint_least16_t;
typedef __int32_t __int_least32_t;
typedef __uint32_t __uint_least32_t;
typedef __int64_t __int_least64_t;
typedef __uint64_t __uint_least64_t;
typedef long __quad_t;
typedef unsigned long __u_quad_t;
typedef long __intmax_t;
typedef unsigned long __uintmax_t;
typedef unsigned long __dev_t;
typedef unsigned int __uid_t;
typedef unsigned int __gid_t;
typedef unsigned long __ino_t;
typedef unsigned long __ino64_t;
typedef unsigned int __mode_t;
typedef unsigned long __nlink_t;
typedef long __off_t;
typedef long __off64_t;
typedef int __pid_t;
typedef struct {
    int __val[2];
} __fsid_t;
typedef long __clock_t;
typedef unsigned long __rlim_t;
typedef unsigned long __rlim64_t;
typedef unsigned int __id_t;
typedef long __time_t;
typedef unsigned int __useconds_t;
typedef long __suseconds_t;
typedef int __daddr_t;
typedef int __key_t;
typedef int __clockid_t;
typedef void *__timer_t;
typedef long __blksize_t;
typedef long __blkcnt_t;
typedef long __blkcnt64_t;
typedef unsigned long __fsblkcnt_t;
typedef unsigned long __fsblkcnt64_t;
typedef unsigned long __fsfilcnt_t;
typedef unsigned long __fsfilcnt64_t;
typedef long __fsword_t;
typedef long __ssize_t;
typedef long __syscall_slong_t;
typedef unsigned long __syscall_ulong_t;
typedef __off64_t __loff_t;
typedef char *__caddr_t;
typedef long __intptr_t;
typedef unsigned int __socklen_t;
typedef int __sig_atomic_t;
typedef __int8_t int8_t;
typedef __int16_t int16_t;
typedef __int32_t int32_t;
typedef __int64_t int64_t;
typedef __uint8_t uint8_t;
typedef __uint16_t uint16_t;
typedef __uint32_t uint32_t;
typedef __uint64_t uint64_t;
typedef __int_least8_t int_least8_t;
typedef __int_least16_t int_least16_t;
typedef __int_least32_t int_least32_t;
typedef __int_least64_t int_least64_t;
typedef __uint_least8_t uint_least8_t;
typedef __uint_least16_t uint_least16_t;
typedef __uint_least32_t uint_least32_t;
typedef __uint_least64_t uint_least64_t;
typedef signed char int_fast8_t;
typedef long int_fast16_t;
typedef long int_fast32_t;
typedef long int_fast64_t;
typedef unsigned char uint_fast8_t;
typedef unsigned long uint_fast16_t;
typedef unsigned long uint_fast32_t;
typedef unsigned long uint_fast64_t;
typedef long intptr_t;
typedef unsigned long uintptr_t;
typedef __intmax_t intmax_t;
typedef __uintmax_t uintmax_t;
struct indirect_placeholder_311_ret_type;
struct indirect_placeholder_311_ret_type {
    uint64_t field_0;
    uint64_t field_1;
    uint64_t field_2;
    uint64_t field_3;
    uint64_t field_4;
};
extern void abort(void);
extern uint64_t revng_init_local_sp(uint64_t param_0);
extern uint64_t init_rbp(void);
extern uint64_t indirect_placeholder_1(uint64_t param_0, uint64_t param_1);
extern uint64_t init_cc_src2(void);
extern uint64_t helper_cc_compute_c_wrapper(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint32_t param_3);
extern void indirect_placeholder(void);
extern uint64_t indirect_placeholder_2(uint64_t param_0);
extern uint64_t init_rbx(void);
extern uint64_t init_r10(void);
extern uint64_t init_r8(void);
extern uint64_t init_r9(void);
extern void indirect_placeholder_22(uint64_t param_0, uint64_t param_1, uint64_t param_2);
extern uint64_t indirect_placeholder_110(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint64_t param_3, uint64_t param_4, uint64_t param_5, uint64_t param_6, uint64_t param_7);
extern struct indirect_placeholder_311_ret_type indirect_placeholder_311(uint64_t param_0);
uint64_t bb_streamsavedir(uint64_t rdi, uint64_t rsi) {
    uint64_t var_0;
    uint64_t var_1;
    uint64_t var_2;
    uint64_t var_3;
    uint64_t var_4;
    uint64_t var_5;
    uint64_t var_6;
    uint64_t var_7;
    uint64_t *var_8;
    uint32_t *var_9;
    uint64_t var_10;
    uint64_t *var_11;
    uint64_t *var_12;
    uint64_t *var_13;
    uint64_t *var_14;
    uint64_t *var_15;
    uint64_t *var_16;
    uint64_t var_17;
    uint64_t *var_18;
    uint64_t rax_2;
    uint64_t local_sp_4;
    uint64_t var_65;
    uint64_t local_sp_0;
    uint64_t var_66;
    uint64_t var_67;
    uint64_t var_68;
    uint64_t var_69;
    uint64_t var_70;
    uint64_t local_sp_3;
    uint64_t var_40;
    uint64_t local_sp_1;
    uint64_t rbx_0;
    uint64_t var_54;
    uint64_t rax_1;
    uint64_t var_35;
    uint64_t var_36;
    uint64_t rbx_2;
    uint64_t var_37;
    uint64_t var_38;
    uint64_t var_39;
    uint64_t local_sp_2;
    uint64_t var_41;
    uint64_t var_42;
    uint64_t var_43;
    uint64_t var_44;
    uint64_t var_45;
    uint64_t var_46;
    uint64_t var_47;
    uint64_t var_48;
    uint64_t var_49;
    uint64_t local_sp_6;
    uint64_t var_50;
    uint64_t var_51;
    uint64_t var_52;
    uint64_t var_53;
    uint32_t *var_58;
    uint64_t var_59;
    uint64_t var_60;
    uint64_t var_61;
    struct indirect_placeholder_311_ret_type var_62;
    uint64_t *var_63;
    uint64_t *var_64;
    uint64_t var_71;
    uint64_t var_72;
    uint64_t var_73;
    uint64_t var_74;
    uint64_t var_55;
    uint32_t var_56;
    uint32_t *var_57;
    uint64_t var_30;
    uint64_t rax_0;
    unsigned char var_31;
    uint64_t var_32;
    uint64_t var_33;
    uint64_t var_34;
    uint64_t local_sp_5;
    uint64_t rbx_1;
    uint64_t rax_3;
    uint64_t var_19;
    uint64_t *var_20;
    uint64_t var_21;
    uint64_t *var_22;
    uint64_t *var_23;
    uint64_t var_24;
    uint64_t *var_25;
    uint64_t var_26;
    uint64_t *var_27;
    uint64_t var_28;
    uint64_t var_29;
    unsigned int loop_state_var;
    var_0 = revng_init_local_sp(0UL);
    var_1 = init_rbp();
    var_2 = init_cc_src2();
    var_3 = init_rbx();
    var_4 = init_r8();
    var_5 = init_r10();
    var_6 = init_r9();
    var_7 = var_0 + (-8L);
    *(uint64_t *)var_7 = var_1;
    *(uint64_t *)(var_0 + (-16L)) = var_3;
    var_8 = (uint64_t *)(var_0 + (-160L));
    *var_8 = rdi;
    var_9 = (uint32_t *)(var_0 + (-164L));
    *var_9 = (uint32_t)rsi;
    var_10 = var_0 + (-32L);
    var_11 = (uint64_t *)var_10;
    *var_11 = 0UL;
    var_12 = (uint64_t *)(var_0 + (-40L));
    *var_12 = 0UL;
    var_13 = (uint64_t *)(var_0 + (-48L));
    *var_13 = 0UL;
    var_14 = (uint64_t *)(var_0 + (-56L));
    *var_14 = 0UL;
    var_15 = (uint64_t *)(var_0 + (-64L));
    *var_15 = 0UL;
    var_16 = (uint64_t *)(var_0 + (-72L));
    *var_16 = 0UL;
    var_17 = *(uint64_t *)(((uint64_t)*var_9 << 3UL) + 4357520UL);
    var_18 = (uint64_t *)(var_0 + (-88L));
    *var_18 = var_17;
    rax_2 = 0UL;
    var_65 = 0UL;
    rbx_2 = var_3;
    rax_0 = 0UL;
    rax_3 = var_17;
    if (*var_8 == 0UL) {
        return rax_2;
    }
    var_19 = var_0 + (-168L);
    var_20 = (uint64_t *)(var_0 + (-96L));
    var_21 = var_0 + (-104L);
    var_22 = (uint64_t *)var_21;
    var_23 = (uint64_t *)(var_0 + (-112L));
    var_24 = var_0 + (-136L);
    var_25 = (uint64_t *)var_24;
    var_26 = var_0 + (-144L);
    var_27 = (uint64_t *)var_26;
    local_sp_6 = var_19;
    while (1U)
        {
            *(uint64_t *)(local_sp_6 + (-8L)) = 4261872UL;
            indirect_placeholder();
            *(uint32_t *)rax_3 = 0U;
            var_28 = *var_8;
            var_29 = local_sp_6 + (-16L);
            *(uint64_t *)var_29 = 4261893UL;
            indirect_placeholder();
            *var_20 = var_28;
            rbx_0 = rbx_2;
            local_sp_5 = var_29;
            rbx_1 = rbx_2;
            if (var_28 != 0UL) {
                var_55 = local_sp_6 + (-24L);
                *(uint64_t *)var_55 = 4261910UL;
                indirect_placeholder();
                var_56 = *(volatile uint32_t *)(uint32_t *)0UL;
                var_57 = (uint32_t *)(var_0 + (-116L));
                *var_57 = var_56;
                local_sp_4 = var_55;
                if (var_56 == 0U) {
                    *(uint64_t *)(local_sp_6 + (-32L)) = 4262280UL;
                    indirect_placeholder();
                    var_58 = *(uint32_t **)var_10;
                    *(uint64_t *)(local_sp_6 + (-40L)) = 4262292UL;
                    indirect_placeholder();
                    *(uint64_t *)(local_sp_6 + (-48L)) = 4262297UL;
                    indirect_placeholder();
                    *var_58 = *var_57;
                    loop_state_var = 1U;
                    break;
                }
                if (*var_18 != 0UL) {
                    if (*var_15 != 0UL) {
                        loop_state_var = 0U;
                        break;
                    }
                    var_59 = local_sp_6 + (-32L);
                    *(uint64_t *)var_59 = 4262361UL;
                    indirect_placeholder();
                    local_sp_4 = var_59;
                    loop_state_var = 0U;
                    break;
                }
                var_71 = *var_16;
                if (var_71 != *var_12) {
                    loop_state_var = 2U;
                    break;
                }
                var_72 = var_71 + 1UL;
                var_73 = *var_11;
                *(uint64_t *)(local_sp_6 + (-32L)) = 4262564UL;
                var_74 = indirect_placeholder_1(var_73, var_72);
                *var_11 = var_74;
                loop_state_var = 2U;
                break;
            }
            var_30 = var_28 + 19UL;
            *var_22 = var_30;
            if (**(unsigned char **)var_21 == '.') {
                rax_0 = (*(unsigned char *)(var_30 + 1UL) == '.') ? 2UL : 1UL;
            }
            var_31 = *(unsigned char *)(rax_0 + var_30);
            rax_1 = (uint64_t)var_31;
            if (var_31 != '\x00') {
                var_32 = *var_20;
                var_33 = local_sp_6 + (-24L);
                *(uint64_t *)var_33 = 4262021UL;
                indirect_placeholder();
                var_34 = var_32 + 20UL;
                *var_23 = var_34;
                local_sp_2 = var_33;
                local_sp_3 = var_33;
                if (*var_18 == 0UL) {
                    var_45 = *var_12;
                    var_46 = *var_16;
                    if ((var_45 - var_46) <= var_34) {
                        var_47 = var_34 + var_46;
                        *var_27 = var_47;
                        var_48 = *var_16;
                        var_49 = helper_cc_compute_c_wrapper(var_47 - var_48, var_48, var_2, 17U);
                        if (var_49 != 0UL) {
                            *(uint64_t *)(local_sp_6 + (-32L)) = 4262182UL;
                            indirect_placeholder_22(var_7, var_4, var_6);
                            abort();
                        }
                        var_50 = *var_11;
                        var_51 = local_sp_6 + (-32L);
                        *(uint64_t *)var_51 = 4262209UL;
                        var_52 = indirect_placeholder_110(1UL, var_26, var_50, rbx_2, var_26, var_4, var_5, var_6);
                        *var_11 = var_52;
                        *var_12 = *var_27;
                        local_sp_3 = var_51;
                    }
                    var_53 = local_sp_3 + (-8L);
                    *(uint64_t *)var_53 = 4262255UL;
                    indirect_placeholder();
                    local_sp_1 = var_53;
                } else {
                    var_35 = *var_14;
                    var_36 = *var_15;
                    var_40 = var_36;
                    if (var_35 == var_36) {
                        *var_25 = var_35;
                        var_37 = *var_13;
                        var_38 = local_sp_6 + (-32L);
                        *(uint64_t *)var_38 = 4262078UL;
                        var_39 = indirect_placeholder_110(8UL, var_24, var_37, rbx_2, var_24, var_4, var_5, var_6);
                        *var_13 = var_39;
                        *var_14 = *var_25;
                        var_40 = *var_15;
                        local_sp_2 = var_38;
                    }
                    var_41 = *var_13 + (var_40 << 3UL);
                    var_42 = *var_22;
                    var_43 = local_sp_2 + (-8L);
                    *(uint64_t *)var_43 = 4262122UL;
                    var_44 = indirect_placeholder_2(var_42);
                    *(uint64_t *)var_41 = var_44;
                    *var_15 = (*var_15 + 1UL);
                    local_sp_1 = var_43;
                    rbx_0 = var_41;
                }
                var_54 = *var_23;
                *var_16 = (*var_16 + var_54);
                rax_1 = var_54;
                local_sp_5 = local_sp_1;
                rbx_1 = rbx_0;
            }
            rax_3 = rax_1;
            local_sp_6 = local_sp_5;
            rbx_2 = rbx_1;
            continue;
        }
    switch (loop_state_var) {
      case 2U:
        {
            *(unsigned char *)(*var_16 + *var_11) = (unsigned char)'\x00';
            rax_2 = *var_11;
        }
        break;
      case 0U:
        {
            var_60 = *var_16 + 1UL;
            var_61 = local_sp_4 + (-8L);
            *(uint64_t *)var_61 = 4262377UL;
            var_62 = indirect_placeholder_311(var_60);
            *var_11 = var_62.field_0;
            *var_16 = 0UL;
            var_63 = (uint64_t *)(var_0 + (-80L));
            *var_63 = 0UL;
            var_64 = (uint64_t *)(var_0 + (-128L));
            local_sp_0 = var_61;
            var_66 = *var_15;
            var_67 = helper_cc_compute_c_wrapper(var_65 - var_66, var_66, var_2, 17U);
            while (var_67 != 0UL)
                {
                    var_68 = *var_16 + *var_11;
                    *var_64 = var_68;
                    *(uint64_t *)(local_sp_0 + (-8L)) = 4262451UL;
                    indirect_placeholder();
                    *var_16 = (*var_16 + ((var_68 - *var_64) + 1UL));
                    var_69 = local_sp_0 + (-16L);
                    *(uint64_t *)var_69 = 4262502UL;
                    indirect_placeholder();
                    var_70 = *var_63 + 1UL;
                    *var_63 = var_70;
                    var_65 = var_70;
                    local_sp_0 = var_69;
                    var_66 = *var_15;
                    var_67 = helper_cc_compute_c_wrapper(var_65 - var_66, var_66, var_2, 17U);
                }
            *(uint64_t *)(local_sp_0 + (-8L)) = 4262529UL;
            indirect_placeholder();
        }
        break;
    }
}

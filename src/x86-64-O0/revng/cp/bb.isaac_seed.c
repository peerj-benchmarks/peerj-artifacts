typedef unsigned char __u_char;
typedef unsigned short __u_short;
typedef unsigned int __u_int;
typedef unsigned long __u_long;
typedef signed char __int8_t;
typedef unsigned char __uint8_t;
typedef short __int16_t;
typedef unsigned short __uint16_t;
typedef int __int32_t;
typedef unsigned int __uint32_t;
typedef long __int64_t;
typedef unsigned long __uint64_t;
typedef __int8_t __int_least8_t;
typedef __uint8_t __uint_least8_t;
typedef __int16_t __int_least16_t;
typedef __uint16_t __uint_least16_t;
typedef __int32_t __int_least32_t;
typedef __uint32_t __uint_least32_t;
typedef __int64_t __int_least64_t;
typedef __uint64_t __uint_least64_t;
typedef long __quad_t;
typedef unsigned long __u_quad_t;
typedef long __intmax_t;
typedef unsigned long __uintmax_t;
typedef unsigned long __dev_t;
typedef unsigned int __uid_t;
typedef unsigned int __gid_t;
typedef unsigned long __ino_t;
typedef unsigned long __ino64_t;
typedef unsigned int __mode_t;
typedef unsigned long __nlink_t;
typedef long __off_t;
typedef long __off64_t;
typedef int __pid_t;
typedef struct {
    int __val[2];
} __fsid_t;
typedef long __clock_t;
typedef unsigned long __rlim_t;
typedef unsigned long __rlim64_t;
typedef unsigned int __id_t;
typedef long __time_t;
typedef unsigned int __useconds_t;
typedef long __suseconds_t;
typedef int __daddr_t;
typedef int __key_t;
typedef int __clockid_t;
typedef void *__timer_t;
typedef long __blksize_t;
typedef long __blkcnt_t;
typedef long __blkcnt64_t;
typedef unsigned long __fsblkcnt_t;
typedef unsigned long __fsblkcnt64_t;
typedef unsigned long __fsfilcnt_t;
typedef unsigned long __fsfilcnt64_t;
typedef long __fsword_t;
typedef long __ssize_t;
typedef long __syscall_slong_t;
typedef unsigned long __syscall_ulong_t;
typedef __off64_t __loff_t;
typedef char *__caddr_t;
typedef long __intptr_t;
typedef unsigned int __socklen_t;
typedef int __sig_atomic_t;
typedef __int8_t int8_t;
typedef __int16_t int16_t;
typedef __int32_t int32_t;
typedef __int64_t int64_t;
typedef __uint8_t uint8_t;
typedef __uint16_t uint16_t;
typedef __uint32_t uint32_t;
typedef __uint64_t uint64_t;
typedef __int_least8_t int_least8_t;
typedef __int_least16_t int_least16_t;
typedef __int_least32_t int_least32_t;
typedef __int_least64_t int_least64_t;
typedef __uint_least8_t uint_least8_t;
typedef __uint_least16_t uint_least16_t;
typedef __uint_least32_t uint_least32_t;
typedef __uint_least64_t uint_least64_t;
typedef signed char int_fast8_t;
typedef long int_fast16_t;
typedef long int_fast32_t;
typedef long int_fast64_t;
typedef unsigned char uint_fast8_t;
typedef unsigned long uint_fast16_t;
typedef unsigned long uint_fast32_t;
typedef unsigned long uint_fast64_t;
typedef long intptr_t;
typedef unsigned long uintptr_t;
typedef __intmax_t intmax_t;
typedef __uintmax_t uintmax_t;
typedef _Bool bool;
extern uint64_t revng_init_local_sp(uint64_t param_0);
extern uint64_t init_rbp(void);
extern uint64_t indirect_placeholder_2(uint64_t param_0);
void bb_isaac_seed(uint64_t rdi) {
    uint64_t var_0;
    uint64_t var_1;
    uint64_t var_2;
    uint64_t *var_3;
    uint64_t *var_4;
    uint64_t *var_5;
    uint64_t *var_6;
    uint64_t *var_7;
    uint64_t *var_8;
    uint64_t *var_9;
    uint64_t *var_10;
    uint64_t *var_11;
    uint32_t *var_12;
    uint32_t var_13;
    uint64_t local_sp_0;
    uint32_t *var_24;
    uint32_t var_25;
    uint64_t local_sp_1;
    bool var_26;
    uint64_t var_27;
    uint64_t var_38;
    uint64_t var_39;
    uint64_t var_28;
    uint64_t var_29;
    uint64_t var_30;
    uint64_t var_31;
    uint64_t var_32;
    uint64_t var_33;
    uint64_t var_34;
    uint64_t var_35;
    uint64_t var_36;
    uint32_t var_37;
    uint64_t var_14;
    uint64_t var_15;
    uint64_t var_16;
    uint64_t var_17;
    uint64_t var_18;
    uint64_t var_19;
    uint64_t var_20;
    uint64_t var_21;
    uint64_t var_22;
    uint32_t var_23;
    var_0 = revng_init_local_sp(0UL);
    var_1 = init_rbp();
    *(uint64_t *)(var_0 + (-8L)) = var_1;
    var_2 = var_0 + (-96L);
    var_3 = (uint64_t *)var_2;
    *var_3 = rdi;
    var_4 = (uint64_t *)(var_0 + (-16L));
    *var_4 = 7240739780546808700UL;
    var_5 = (uint64_t *)(var_0 + (-24L));
    *var_5 = 13400657653193689186UL;
    var_6 = (uint64_t *)(var_0 + (-32L));
    *var_6 = 10092185256905347744UL;
    var_7 = (uint64_t *)(var_0 + (-40L));
    *var_7 = 12869931497269318948UL;
    var_8 = (uint64_t *)(var_0 + (-48L));
    *var_8 = 9435133421607575758UL;
    var_9 = (uint64_t *)(var_0 + (-56L));
    *var_9 = 5259722845879046933UL;
    var_10 = (uint64_t *)(var_0 + (-64L));
    *var_10 = 12580906657422019053UL;
    var_11 = (uint64_t *)(var_0 + (-72L));
    *var_11 = 11021839149480329387UL;
    var_12 = (uint32_t *)(var_0 + (-76L));
    *var_12 = 0U;
    var_13 = 0U;
    local_sp_0 = var_2;
    var_25 = 0U;
    local_sp_1 = local_sp_0;
    while ((int)var_13 <= (int)255U)
        {
            *var_4 = (*var_4 + *(uint64_t *)(((uint64_t)var_13 << 3UL) + *var_3));
            *var_5 = (*var_5 + *(uint64_t *)((uint64_t)((long)(((uint64_t)*var_12 << 32UL) + 4294967296UL) >> (long)29UL) + *var_3));
            *var_6 = (*var_6 + *(uint64_t *)((uint64_t)((long)(((uint64_t)*var_12 << 32UL) + 8589934592UL) >> (long)29UL) + *var_3));
            *var_7 = (*var_7 + *(uint64_t *)((uint64_t)((long)(((uint64_t)*var_12 << 32UL) + 12884901888UL) >> (long)29UL) + *var_3));
            *var_8 = (*var_8 + *(uint64_t *)((uint64_t)((long)(((uint64_t)*var_12 << 32UL) + 17179869184UL) >> (long)29UL) + *var_3));
            *var_9 = (*var_9 + *(uint64_t *)((uint64_t)((long)(((uint64_t)*var_12 << 32UL) + 21474836480UL) >> (long)29UL) + *var_3));
            *var_10 = (*var_10 + *(uint64_t *)((uint64_t)((long)(((uint64_t)*var_12 << 32UL) + 25769803776UL) >> (long)29UL) + *var_3));
            *var_11 = (*var_11 + *(uint64_t *)((uint64_t)((long)(((uint64_t)*var_12 << 32UL) + 30064771072UL) >> (long)29UL) + *var_3));
            *var_4 = (*var_4 - *var_8);
            var_14 = *var_11;
            *(uint64_t *)(local_sp_0 + (-8L)) = 4291856UL;
            var_15 = indirect_placeholder_2(var_14);
            *var_9 = (*var_9 ^ (var_15 >> 9UL));
            *var_11 = (*var_11 + *var_4);
            *var_5 = (*var_5 - *var_9);
            *var_10 = (*var_10 ^ (*var_4 << 9UL));
            *var_4 = (*var_4 + *var_5);
            *var_6 = (*var_6 - *var_10);
            var_16 = *var_5;
            *(uint64_t *)(local_sp_0 + (-16L)) = 4291920UL;
            var_17 = indirect_placeholder_2(var_16);
            *var_11 = (*var_11 ^ (var_17 >> 23UL));
            *var_5 = (*var_5 + *var_6);
            *var_7 = (*var_7 - *var_11);
            *var_4 = (*var_4 ^ (*var_6 << 15UL));
            *var_6 = (*var_6 + *var_7);
            *var_8 = (*var_8 - *var_4);
            var_18 = *var_7;
            *(uint64_t *)(local_sp_0 + (-24L)) = 4291984UL;
            var_19 = indirect_placeholder_2(var_18);
            *var_5 = (*var_5 ^ (var_19 >> 14UL));
            *var_7 = (*var_7 + *var_8);
            *var_9 = (*var_9 - *var_5);
            *var_6 = (*var_6 ^ (*var_8 << 20UL));
            *var_8 = (*var_8 + *var_9);
            *var_10 = (*var_10 - *var_6);
            var_20 = *var_9;
            var_21 = local_sp_0 + (-32L);
            *(uint64_t *)var_21 = 4292048UL;
            var_22 = indirect_placeholder_2(var_20);
            *var_7 = (*var_7 ^ (var_22 >> 17UL));
            *var_9 = (*var_9 + *var_10);
            *var_11 = (*var_11 - *var_7);
            *var_8 = (*var_8 ^ (*var_10 << 14UL));
            *var_10 = (*var_10 + *var_11);
            *(uint64_t *)(((uint64_t)*var_12 << 3UL) + *var_3) = *var_4;
            *(uint64_t *)((uint64_t)((long)(((uint64_t)*var_12 << 32UL) + 4294967296UL) >> (long)29UL) + *var_3) = *var_5;
            *(uint64_t *)((uint64_t)((long)(((uint64_t)*var_12 << 32UL) + 8589934592UL) >> (long)29UL) + *var_3) = *var_6;
            *(uint64_t *)((uint64_t)((long)(((uint64_t)*var_12 << 32UL) + 12884901888UL) >> (long)29UL) + *var_3) = *var_7;
            *(uint64_t *)((uint64_t)((long)(((uint64_t)*var_12 << 32UL) + 17179869184UL) >> (long)29UL) + *var_3) = *var_8;
            *(uint64_t *)((uint64_t)((long)(((uint64_t)*var_12 << 32UL) + 21474836480UL) >> (long)29UL) + *var_3) = *var_9;
            *(uint64_t *)((uint64_t)((long)(((uint64_t)*var_12 << 32UL) + 25769803776UL) >> (long)29UL) + *var_3) = *var_10;
            *(uint64_t *)((uint64_t)((long)(((uint64_t)*var_12 << 32UL) + 30064771072UL) >> (long)29UL) + *var_3) = *var_11;
            var_23 = *var_12 + 8U;
            *var_12 = var_23;
            var_13 = var_23;
            local_sp_0 = var_21;
            local_sp_1 = local_sp_0;
        }
    var_24 = (uint32_t *)(var_0 + (-80L));
    *var_24 = 0U;
    var_26 = ((int)var_25 > (int)255U);
    var_27 = *var_3;
    while (!var_26)
        {
            *var_4 = (*var_4 + *(uint64_t *)(((uint64_t)var_25 << 3UL) + var_27));
            *var_5 = (*var_5 + *(uint64_t *)((uint64_t)((long)(((uint64_t)*var_24 << 32UL) + 4294967296UL) >> (long)29UL) + *var_3));
            *var_6 = (*var_6 + *(uint64_t *)((uint64_t)((long)(((uint64_t)*var_24 << 32UL) + 8589934592UL) >> (long)29UL) + *var_3));
            *var_7 = (*var_7 + *(uint64_t *)((uint64_t)((long)(((uint64_t)*var_24 << 32UL) + 12884901888UL) >> (long)29UL) + *var_3));
            *var_8 = (*var_8 + *(uint64_t *)((uint64_t)((long)(((uint64_t)*var_24 << 32UL) + 17179869184UL) >> (long)29UL) + *var_3));
            *var_9 = (*var_9 + *(uint64_t *)((uint64_t)((long)(((uint64_t)*var_24 << 32UL) + 21474836480UL) >> (long)29UL) + *var_3));
            *var_10 = (*var_10 + *(uint64_t *)((uint64_t)((long)(((uint64_t)*var_24 << 32UL) + 25769803776UL) >> (long)29UL) + *var_3));
            *var_11 = (*var_11 + *(uint64_t *)((uint64_t)((long)(((uint64_t)*var_24 << 32UL) + 30064771072UL) >> (long)29UL) + *var_3));
            *var_4 = (*var_4 - *var_8);
            var_28 = *var_11;
            *(uint64_t *)(local_sp_1 + (-8L)) = 4292471UL;
            var_29 = indirect_placeholder_2(var_28);
            *var_9 = (*var_9 ^ (var_29 >> 9UL));
            *var_11 = (*var_11 + *var_4);
            *var_5 = (*var_5 - *var_9);
            *var_10 = (*var_10 ^ (*var_4 << 9UL));
            *var_4 = (*var_4 + *var_5);
            *var_6 = (*var_6 - *var_10);
            var_30 = *var_5;
            *(uint64_t *)(local_sp_1 + (-16L)) = 4292535UL;
            var_31 = indirect_placeholder_2(var_30);
            *var_11 = (*var_11 ^ (var_31 >> 23UL));
            *var_5 = (*var_5 + *var_6);
            *var_7 = (*var_7 - *var_11);
            *var_4 = (*var_4 ^ (*var_6 << 15UL));
            *var_6 = (*var_6 + *var_7);
            *var_8 = (*var_8 - *var_4);
            var_32 = *var_7;
            *(uint64_t *)(local_sp_1 + (-24L)) = 4292599UL;
            var_33 = indirect_placeholder_2(var_32);
            *var_5 = (*var_5 ^ (var_33 >> 14UL));
            *var_7 = (*var_7 + *var_8);
            *var_9 = (*var_9 - *var_5);
            *var_6 = (*var_6 ^ (*var_8 << 20UL));
            *var_8 = (*var_8 + *var_9);
            *var_10 = (*var_10 - *var_6);
            var_34 = *var_9;
            var_35 = local_sp_1 + (-32L);
            *(uint64_t *)var_35 = 4292663UL;
            var_36 = indirect_placeholder_2(var_34);
            *var_7 = (*var_7 ^ (var_36 >> 17UL));
            *var_9 = (*var_9 + *var_10);
            *var_11 = (*var_11 - *var_7);
            *var_8 = (*var_8 ^ (*var_10 << 14UL));
            *var_10 = (*var_10 + *var_11);
            *(uint64_t *)(((uint64_t)*var_24 << 3UL) + *var_3) = *var_4;
            *(uint64_t *)((uint64_t)((long)(((uint64_t)*var_24 << 32UL) + 4294967296UL) >> (long)29UL) + *var_3) = *var_5;
            *(uint64_t *)((uint64_t)((long)(((uint64_t)*var_24 << 32UL) + 8589934592UL) >> (long)29UL) + *var_3) = *var_6;
            *(uint64_t *)((uint64_t)((long)(((uint64_t)*var_24 << 32UL) + 12884901888UL) >> (long)29UL) + *var_3) = *var_7;
            *(uint64_t *)((uint64_t)((long)(((uint64_t)*var_24 << 32UL) + 17179869184UL) >> (long)29UL) + *var_3) = *var_8;
            *(uint64_t *)((uint64_t)((long)(((uint64_t)*var_24 << 32UL) + 21474836480UL) >> (long)29UL) + *var_3) = *var_9;
            *(uint64_t *)((uint64_t)((long)(((uint64_t)*var_24 << 32UL) + 25769803776UL) >> (long)29UL) + *var_3) = *var_10;
            *(uint64_t *)((uint64_t)((long)(((uint64_t)*var_24 << 32UL) + 30064771072UL) >> (long)29UL) + *var_3) = *var_11;
            var_37 = *var_24 + 8U;
            *var_24 = var_37;
            var_25 = var_37;
            local_sp_1 = var_35;
            var_26 = ((int)var_25 > (int)255U);
            var_27 = *var_3;
        }
    *(uint64_t *)(var_27 + 2064UL) = 0UL;
    var_38 = *var_3;
    *(uint64_t *)(var_38 + 2056UL) = *(uint64_t *)(var_38 + 2064UL);
    var_39 = *var_3;
    *(uint64_t *)(var_39 + 2048UL) = *(uint64_t *)(var_39 + 2056UL);
    return;
}

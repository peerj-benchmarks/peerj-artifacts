typedef unsigned char __u_char;
typedef unsigned short __u_short;
typedef unsigned int __u_int;
typedef unsigned long __u_long;
typedef signed char __int8_t;
typedef unsigned char __uint8_t;
typedef short __int16_t;
typedef unsigned short __uint16_t;
typedef int __int32_t;
typedef unsigned int __uint32_t;
typedef long __int64_t;
typedef unsigned long __uint64_t;
typedef __int8_t __int_least8_t;
typedef __uint8_t __uint_least8_t;
typedef __int16_t __int_least16_t;
typedef __uint16_t __uint_least16_t;
typedef __int32_t __int_least32_t;
typedef __uint32_t __uint_least32_t;
typedef __int64_t __int_least64_t;
typedef __uint64_t __uint_least64_t;
typedef long __quad_t;
typedef unsigned long __u_quad_t;
typedef long __intmax_t;
typedef unsigned long __uintmax_t;
typedef unsigned long __dev_t;
typedef unsigned int __uid_t;
typedef unsigned int __gid_t;
typedef unsigned long __ino_t;
typedef unsigned long __ino64_t;
typedef unsigned int __mode_t;
typedef unsigned long __nlink_t;
typedef long __off_t;
typedef long __off64_t;
typedef int __pid_t;
typedef struct {
    int __val[2];
} __fsid_t;
typedef long __clock_t;
typedef unsigned long __rlim_t;
typedef unsigned long __rlim64_t;
typedef unsigned int __id_t;
typedef long __time_t;
typedef unsigned int __useconds_t;
typedef long __suseconds_t;
typedef int __daddr_t;
typedef int __key_t;
typedef int __clockid_t;
typedef void *__timer_t;
typedef long __blksize_t;
typedef long __blkcnt_t;
typedef long __blkcnt64_t;
typedef unsigned long __fsblkcnt_t;
typedef unsigned long __fsblkcnt64_t;
typedef unsigned long __fsfilcnt_t;
typedef unsigned long __fsfilcnt64_t;
typedef long __fsword_t;
typedef long __ssize_t;
typedef long __syscall_slong_t;
typedef unsigned long __syscall_ulong_t;
typedef __off64_t __loff_t;
typedef char *__caddr_t;
typedef long __intptr_t;
typedef unsigned int __socklen_t;
typedef int __sig_atomic_t;
typedef __int8_t int8_t;
typedef __int16_t int16_t;
typedef __int32_t int32_t;
typedef __int64_t int64_t;
typedef __uint8_t uint8_t;
typedef __uint16_t uint16_t;
typedef __uint32_t uint32_t;
typedef __uint64_t uint64_t;
typedef __int_least8_t int_least8_t;
typedef __int_least16_t int_least16_t;
typedef __int_least32_t int_least32_t;
typedef __int_least64_t int_least64_t;
typedef __uint_least8_t uint_least8_t;
typedef __uint_least16_t uint_least16_t;
typedef __uint_least32_t uint_least32_t;
typedef __uint_least64_t uint_least64_t;
typedef signed char int_fast8_t;
typedef long int_fast16_t;
typedef long int_fast32_t;
typedef long int_fast64_t;
typedef unsigned char uint_fast8_t;
typedef unsigned long uint_fast16_t;
typedef unsigned long uint_fast32_t;
typedef unsigned long uint_fast64_t;
typedef long intptr_t;
typedef unsigned long uintptr_t;
typedef __intmax_t intmax_t;
typedef __uintmax_t uintmax_t;
struct bb_set_process_security_ctx_ret_type;
struct indirect_placeholder_183_ret_type;
struct indirect_placeholder_182_ret_type;
struct indirect_placeholder_185_ret_type;
struct indirect_placeholder_186_ret_type;
struct indirect_placeholder_184_ret_type;
struct bb_set_process_security_ctx_ret_type {
    uint64_t field_0;
    uint64_t field_1;
    uint64_t field_2;
};
struct indirect_placeholder_183_ret_type {
    uint64_t field_0;
    uint64_t field_1;
    uint64_t field_2;
};
struct indirect_placeholder_182_ret_type {
    uint64_t field_0;
    uint64_t field_1;
    uint64_t field_2;
};
struct indirect_placeholder_185_ret_type {
    uint64_t field_0;
    uint64_t field_1;
    uint64_t field_2;
};
struct indirect_placeholder_186_ret_type {
    uint64_t field_0;
    uint64_t field_1;
    uint64_t field_2;
};
struct indirect_placeholder_184_ret_type {
    uint64_t field_0;
    uint64_t field_1;
    uint64_t field_2;
};
extern uint64_t revng_init_local_sp(uint64_t param_0);
extern uint64_t init_rbp(void);
extern uint64_t indirect_placeholder_1(uint64_t param_0, uint64_t param_1);
extern void indirect_placeholder(void);
extern uint64_t indirect_placeholder_2(uint64_t param_0);
extern uint64_t init_rbx(void);
extern uint64_t init_r9(void);
extern void indirect_placeholder_118(uint64_t param_0);
extern struct indirect_placeholder_183_ret_type indirect_placeholder_183(uint64_t param_0, uint64_t param_1);
extern struct indirect_placeholder_182_ret_type indirect_placeholder_182(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint64_t param_3, uint64_t param_4, uint64_t param_5, uint64_t param_6);
extern struct indirect_placeholder_185_ret_type indirect_placeholder_185(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint64_t param_3, uint64_t param_4, uint64_t param_5, uint64_t param_6);
extern struct indirect_placeholder_186_ret_type indirect_placeholder_186(uint64_t param_0, uint64_t param_1);
extern struct indirect_placeholder_184_ret_type indirect_placeholder_184(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint64_t param_3, uint64_t param_4, uint64_t param_5, uint64_t param_6);
struct bb_set_process_security_ctx_ret_type bb_set_process_security_ctx(uint64_t rdx, uint64_t rcx, uint64_t rdi, uint64_t rsi, uint64_t r8) {
    struct indirect_placeholder_182_ret_type var_55;
    uint64_t var_0;
    uint64_t var_1;
    uint64_t var_2;
    uint64_t var_3;
    uint64_t *var_4;
    uint64_t *var_5;
    uint32_t *var_6;
    uint64_t *var_7;
    unsigned char *var_8;
    unsigned char var_9;
    uint64_t var_10;
    uint64_t var_49;
    struct indirect_placeholder_183_ret_type var_50;
    uint64_t var_51;
    uint64_t var_52;
    uint64_t var_53;
    uint64_t var_54;
    uint64_t var_56;
    uint64_t var_57;
    uint64_t r87_4;
    uint64_t var_47;
    uint64_t var_48;
    uint64_t r87_1;
    uint64_t r9_1;
    uint64_t local_sp_1;
    uint64_t local_sp_0;
    uint64_t var_22;
    uint64_t var_23;
    uint64_t var_24;
    uint64_t r87_0;
    uint64_t r9_0;
    uint64_t var_30;
    uint64_t rax_0;
    uint64_t var_25;
    uint64_t var_26;
    uint64_t var_27;
    uint64_t var_28;
    struct indirect_placeholder_185_ret_type var_29;
    uint64_t local_sp_2;
    uint64_t r9_5;
    struct bb_set_process_security_ctx_ret_type mrv;
    struct bb_set_process_security_ctx_ret_type mrv1;
    struct bb_set_process_security_ctx_ret_type mrv2;
    uint64_t var_31;
    uint64_t *var_18;
    uint64_t var_19;
    uint64_t var_20;
    uint64_t var_21;
    uint64_t local_sp_3;
    uint64_t r87_2;
    uint64_t var_32;
    uint64_t var_33;
    uint64_t var_34;
    uint64_t r9_2;
    uint64_t var_35;
    struct indirect_placeholder_186_ret_type var_36;
    uint64_t var_37;
    uint64_t var_38;
    uint64_t var_39;
    uint64_t var_40;
    struct indirect_placeholder_184_ret_type var_41;
    uint64_t var_42;
    uint64_t var_43;
    unsigned char storemerge;
    unsigned char *var_11;
    unsigned char var_12;
    unsigned char storemerge1;
    unsigned char *var_13;
    uint64_t var_14;
    uint64_t var_15;
    uint64_t var_16;
    uint64_t var_17;
    uint64_t var_44;
    uint64_t var_45;
    uint64_t var_46;
    uint64_t r9_4;
    uint64_t r87_5;
    var_0 = revng_init_local_sp(0UL);
    var_1 = init_rbp();
    var_2 = init_rbx();
    var_3 = init_r9();
    *(uint64_t *)(var_0 + (-8L)) = var_1;
    *(uint64_t *)(var_0 + (-16L)) = var_2;
    var_4 = (uint64_t *)(var_0 + (-48L));
    *var_4 = rdi;
    var_5 = (uint64_t *)(var_0 + (-56L));
    *var_5 = rsi;
    var_6 = (uint32_t *)(var_0 + (-60L));
    *var_6 = (uint32_t)rdx;
    var_7 = (uint64_t *)(var_0 + (-72L));
    *var_7 = r8;
    var_8 = (unsigned char *)(var_0 + (-64L));
    var_9 = (unsigned char)rcx;
    *var_8 = var_9;
    var_10 = *var_7;
    r87_4 = r8;
    r87_1 = r8;
    r9_1 = var_3;
    r87_0 = r8;
    r9_0 = var_3;
    rax_0 = 0UL;
    r87_2 = r8;
    r9_2 = var_3;
    storemerge = (unsigned char)'\x01';
    storemerge1 = (unsigned char)'\x00';
    r9_4 = var_3;
    if (*(unsigned char *)(var_10 + 37UL) == '\x00') {
        var_44 = (uint64_t)*var_6;
        var_45 = *var_5;
        *(uint64_t *)(var_0 + (-80L)) = 4219159UL;
        var_46 = indirect_placeholder_1(var_45, var_44);
        *(uint64_t *)(var_0 + (-88L)) = 4219168UL;
        indirect_placeholder();
        var_47 = (uint64_t)*(uint32_t *)var_46;
        *(uint64_t *)(var_0 + (-96L)) = 4219177UL;
        var_48 = indirect_placeholder_2(var_47);
        if (!((*(unsigned char *)(var_10 + 33UL) == '\x00') || (var_9 == '\x00')) & (int)(uint32_t)var_46 <= (int)4294967295U && (uint64_t)(unsigned char)var_48 == 1UL) {
            var_49 = *var_5;
            *(uint64_t *)(var_0 + (-104L)) = 4219201UL;
            var_50 = indirect_placeholder_183(4UL, var_49);
            var_51 = var_50.field_0;
            var_52 = var_50.field_1;
            var_53 = var_50.field_2;
            *(uint64_t *)(var_0 + (-112L)) = 4219209UL;
            indirect_placeholder();
            var_54 = (uint64_t)*(uint32_t *)var_51;
            *(uint64_t *)(var_0 + (-120L)) = 4219236UL;
            var_55 = indirect_placeholder_182(0UL, 4352384UL, var_51, 0UL, var_54, var_52, var_53);
            var_56 = var_55.field_1;
            var_57 = var_55.field_2;
            r87_4 = var_56;
            r9_4 = var_57;
        }
        rax_0 = 1UL;
        r87_5 = r87_4;
        r9_5 = r9_4;
    } else {
        if (*(unsigned char *)(var_10 + 35UL) == '\x01') {
        } else {
            storemerge = (unsigned char)'\x00';
            if (*(unsigned char *)(var_10 + 38UL) == '\x00') {
            }
        }
        var_11 = (unsigned char *)(var_0 + (-25L));
        var_12 = storemerge & '\x01';
        *var_11 = var_12;
        if (var_12 == '\x01') {
        } else {
            storemerge1 = (unsigned char)'\x01';
            if (*(unsigned char *)(*var_7 + 41UL) == '\x01') {
            }
        }
        var_13 = (unsigned char *)(var_0 + (-26L));
        *var_13 = (storemerge1 & '\x01');
        var_14 = var_0 + (-40L);
        var_15 = *var_4;
        var_16 = var_0 + (-80L);
        *(uint64_t *)var_16 = 4218866UL;
        var_17 = indirect_placeholder_1(var_15, var_14);
        local_sp_3 = var_16;
        if ((int)(uint32_t)var_17 < (int)0U) {
            if (*var_11 != '\x00') {
                if (*var_13 != '\x00') {
                    r87_4 = r87_2;
                    r9_4 = r9_2;
                    r87_5 = r87_2;
                    r9_5 = r9_2;
                    if (*(unsigned char *)(*var_7 + 38UL) == '\x00') {
                        rax_0 = 1UL;
                        r87_5 = r87_4;
                        r9_5 = r9_4;
                    }
                    mrv.field_0 = rax_0;
                    mrv1 = mrv;
                    mrv1.field_1 = r87_5;
                    mrv2 = mrv1;
                    mrv2.field_2 = r9_5;
                    return mrv2;
                }
                *(uint64_t *)(var_0 + (-88L)) = 4219035UL;
                indirect_placeholder();
                var_32 = (uint64_t)*(uint32_t *)var_17;
                var_33 = var_0 + (-96L);
                *(uint64_t *)var_33 = 4219044UL;
                var_34 = indirect_placeholder_2(var_32);
                local_sp_3 = var_33;
                if ((uint64_t)(unsigned char)var_34 != 1UL) {
                    r87_4 = r87_2;
                    r9_4 = r9_2;
                    r87_5 = r87_2;
                    r9_5 = r9_2;
                    if (*(unsigned char *)(*var_7 + 38UL) == '\x00') {
                        rax_0 = 1UL;
                        r87_5 = r87_4;
                        r9_5 = r9_4;
                    }
                    mrv.field_0 = rax_0;
                    mrv1 = mrv;
                    mrv1.field_1 = r87_5;
                    mrv2 = mrv1;
                    mrv2.field_2 = r9_5;
                    return mrv2;
                }
            }
            var_35 = *var_4;
            *(uint64_t *)(local_sp_3 + (-8L)) = 4219068UL;
            var_36 = indirect_placeholder_186(4UL, var_35);
            var_37 = var_36.field_0;
            var_38 = var_36.field_1;
            var_39 = var_36.field_2;
            *(uint64_t *)(local_sp_3 + (-16L)) = 4219076UL;
            indirect_placeholder();
            var_40 = (uint64_t)*(uint32_t *)var_37;
            *(uint64_t *)(local_sp_3 + (-24L)) = 4219103UL;
            var_41 = indirect_placeholder_184(0UL, 4352344UL, var_37, 0UL, var_40, var_38, var_39);
            var_42 = var_41.field_1;
            var_43 = var_41.field_2;
            r87_2 = var_42;
            r9_2 = var_43;
            r87_4 = r87_2;
            r9_4 = r9_2;
            r87_5 = r87_2;
            r9_5 = r9_2;
            if (*(unsigned char *)(*var_7 + 38UL) == '\x00') {
                rax_0 = 1UL;
                r87_5 = r87_4;
                r9_5 = r9_4;
            }
        } else {
            var_18 = (uint64_t *)var_14;
            var_19 = *var_18;
            var_20 = var_0 + (-88L);
            *(uint64_t *)var_20 = 4218886UL;
            var_21 = indirect_placeholder_2(var_19);
            local_sp_0 = var_20;
            local_sp_1 = var_20;
            local_sp_2 = var_20;
            if ((int)(uint32_t)var_21 <= (int)4294967295U) {
                if (*var_11 == '\x00') {
                    var_25 = *var_18;
                    *(uint64_t *)(local_sp_1 + (-8L)) = 4218935UL;
                    var_26 = indirect_placeholder_2(var_25);
                    *(uint64_t *)(local_sp_1 + (-16L)) = 4218943UL;
                    indirect_placeholder();
                    var_27 = (uint64_t)*(uint32_t *)var_26;
                    var_28 = local_sp_1 + (-24L);
                    *(uint64_t *)var_28 = 4218970UL;
                    var_29 = indirect_placeholder_185(0UL, 4352288UL, var_26, 0UL, var_27, r8, var_3);
                    local_sp_0 = var_28;
                    r87_0 = var_29.field_1;
                    r9_0 = var_29.field_2;
                } else {
                    *(uint64_t *)(var_0 + (-96L)) = 4218907UL;
                    indirect_placeholder();
                    var_22 = (uint64_t)*(uint32_t *)var_21;
                    var_23 = var_0 + (-104L);
                    *(uint64_t *)var_23 = 4218916UL;
                    var_24 = indirect_placeholder_2(var_22);
                    local_sp_0 = var_23;
                    local_sp_1 = var_23;
                    if (*var_13 != '\x00' & (uint64_t)(unsigned char)var_24 == 1UL) {
                        var_25 = *var_18;
                        *(uint64_t *)(local_sp_1 + (-8L)) = 4218935UL;
                        var_26 = indirect_placeholder_2(var_25);
                        *(uint64_t *)(local_sp_1 + (-16L)) = 4218943UL;
                        indirect_placeholder();
                        var_27 = (uint64_t)*(uint32_t *)var_26;
                        var_28 = local_sp_1 + (-24L);
                        *(uint64_t *)var_28 = 4218970UL;
                        var_29 = indirect_placeholder_185(0UL, 4352288UL, var_26, 0UL, var_27, r8, var_3);
                        local_sp_0 = var_28;
                        r87_0 = var_29.field_1;
                        r9_0 = var_29.field_2;
                    }
                }
                r87_1 = r87_0;
                r9_1 = r9_0;
                local_sp_2 = local_sp_0;
                r9_5 = r9_0;
                r87_5 = r87_0;
                if (*(unsigned char *)(*var_7 + 38UL) != '\x00') {
                    var_30 = *var_18;
                    *(uint64_t *)(local_sp_0 + (-8L)) = 4218994UL;
                    indirect_placeholder_118(var_30);
                    mrv.field_0 = rax_0;
                    mrv1 = mrv;
                    mrv1.field_1 = r87_5;
                    mrv2 = mrv1;
                    mrv2.field_2 = r9_5;
                    return mrv2;
                }
            }
            var_31 = *var_18;
            *(uint64_t *)(local_sp_2 + (-8L)) = 4219016UL;
            indirect_placeholder_118(var_31);
            r87_4 = r87_1;
            r9_4 = r9_1;
            rax_0 = 1UL;
            r87_5 = r87_4;
            r9_5 = r9_4;
        }
    }
}

typedef unsigned char __u_char;
typedef unsigned short __u_short;
typedef unsigned int __u_int;
typedef unsigned long __u_long;
typedef signed char __int8_t;
typedef unsigned char __uint8_t;
typedef short __int16_t;
typedef unsigned short __uint16_t;
typedef int __int32_t;
typedef unsigned int __uint32_t;
typedef long __int64_t;
typedef unsigned long __uint64_t;
typedef __int8_t __int_least8_t;
typedef __uint8_t __uint_least8_t;
typedef __int16_t __int_least16_t;
typedef __uint16_t __uint_least16_t;
typedef __int32_t __int_least32_t;
typedef __uint32_t __uint_least32_t;
typedef __int64_t __int_least64_t;
typedef __uint64_t __uint_least64_t;
typedef long __quad_t;
typedef unsigned long __u_quad_t;
typedef long __intmax_t;
typedef unsigned long __uintmax_t;
typedef unsigned long __dev_t;
typedef unsigned int __uid_t;
typedef unsigned int __gid_t;
typedef unsigned long __ino_t;
typedef unsigned long __ino64_t;
typedef unsigned int __mode_t;
typedef unsigned long __nlink_t;
typedef long __off_t;
typedef long __off64_t;
typedef int __pid_t;
typedef struct {
    int __val[2];
} __fsid_t;
typedef long __clock_t;
typedef unsigned long __rlim_t;
typedef unsigned long __rlim64_t;
typedef unsigned int __id_t;
typedef long __time_t;
typedef unsigned int __useconds_t;
typedef long __suseconds_t;
typedef int __daddr_t;
typedef int __key_t;
typedef int __clockid_t;
typedef void *__timer_t;
typedef long __blksize_t;
typedef long __blkcnt_t;
typedef long __blkcnt64_t;
typedef unsigned long __fsblkcnt_t;
typedef unsigned long __fsblkcnt64_t;
typedef unsigned long __fsfilcnt_t;
typedef unsigned long __fsfilcnt64_t;
typedef long __fsword_t;
typedef long __ssize_t;
typedef long __syscall_slong_t;
typedef unsigned long __syscall_ulong_t;
typedef __off64_t __loff_t;
typedef char *__caddr_t;
typedef long __intptr_t;
typedef unsigned int __socklen_t;
typedef int __sig_atomic_t;
typedef __int8_t int8_t;
typedef __int16_t int16_t;
typedef __int32_t int32_t;
typedef __int64_t int64_t;
typedef __uint8_t uint8_t;
typedef __uint16_t uint16_t;
typedef __uint32_t uint32_t;
typedef __uint64_t uint64_t;
typedef __int_least8_t int_least8_t;
typedef __int_least16_t int_least16_t;
typedef __int_least32_t int_least32_t;
typedef __int_least64_t int_least64_t;
typedef __uint_least8_t uint_least8_t;
typedef __uint_least16_t uint_least16_t;
typedef __uint_least32_t uint_least32_t;
typedef __uint_least64_t uint_least64_t;
typedef signed char int_fast8_t;
typedef long int_fast16_t;
typedef long int_fast32_t;
typedef long int_fast64_t;
typedef unsigned char uint_fast8_t;
typedef unsigned long uint_fast16_t;
typedef unsigned long uint_fast32_t;
typedef unsigned long uint_fast64_t;
typedef long intptr_t;
typedef unsigned long uintptr_t;
typedef __intmax_t intmax_t;
typedef __uintmax_t uintmax_t;
typedef _Bool bool;
struct indirect_placeholder_135_ret_type;
struct indirect_placeholder_136_ret_type;
struct indirect_placeholder_135_ret_type {
    uint64_t field_0;
    uint64_t field_1;
};
struct indirect_placeholder_136_ret_type {
    uint64_t field_0;
    uint64_t field_1;
};
extern uint64_t revng_init_local_sp(uint64_t param_0);
extern uint64_t init_rbp(void);
extern uint64_t indirect_placeholder_1(uint64_t param_0, uint64_t param_1);
extern void indirect_placeholder(void);
extern uint64_t indirect_placeholder_4(uint64_t param_0, uint64_t param_1, uint64_t param_2);
extern uint64_t indirect_placeholder_2(uint64_t param_0);
extern uint64_t init_rbx(void);
extern struct indirect_placeholder_135_ret_type indirect_placeholder_135(uint64_t param_0);
extern struct indirect_placeholder_136_ret_type indirect_placeholder_136(uint64_t param_0);
uint64_t bb_lutimens(uint64_t rdi, uint64_t rsi) {
    uint64_t var_0;
    uint64_t var_1;
    uint64_t var_2;
    uint64_t var_3;
    uint64_t *var_4;
    uint64_t *var_5;
    bool var_6;
    uint64_t var_7;
    uint64_t spec_select;
    uint64_t var_8;
    uint64_t *var_9;
    uint32_t *var_10;
    uint32_t var_19;
    uint64_t var_39;
    uint64_t local_sp_3;
    uint32_t var_40;
    uint64_t local_sp_0;
    uint64_t var_41;
    uint64_t local_sp_4;
    uint64_t var_42;
    uint64_t var_23;
    uint64_t *var_30;
    uint64_t var_31;
    uint64_t var_32;
    struct indirect_placeholder_135_ret_type var_33;
    uint64_t var_34;
    uint64_t local_sp_1;
    uint64_t *var_24;
    uint64_t var_25;
    uint64_t var_26;
    uint64_t var_27;
    struct indirect_placeholder_136_ret_type var_28;
    uint64_t var_29;
    uint64_t var_11;
    uint64_t var_12;
    uint64_t var_13;
    uint64_t var_14;
    uint64_t var_15;
    uint64_t var_16;
    uint64_t var_17;
    uint32_t var_18;
    uint64_t local_sp_2;
    uint64_t rax_0;
    uint64_t var_20;
    uint64_t var_21;
    uint64_t var_22;
    uint64_t var_35;
    uint64_t var_36;
    uint32_t *var_37;
    uint32_t var_38;
    uint64_t local_sp_5;
    uint64_t var_43;
    uint64_t var_44;
    uint64_t var_45;
    uint64_t local_sp_6;
    uint64_t var_46;
    uint64_t var_47;
    uint64_t var_48;
    uint32_t var_49;
    uint64_t var_50;
    uint64_t var_51;
    uint64_t var_52;
    uint64_t var_53;
    var_0 = revng_init_local_sp(0UL);
    var_1 = init_rbp();
    var_2 = init_rbx();
    *(uint64_t *)(var_0 + (-8L)) = var_1;
    *(uint64_t *)(var_0 + (-16L)) = var_2;
    var_3 = var_0 + (-248L);
    var_4 = (uint64_t *)(var_0 + (-240L));
    *var_4 = rdi;
    var_5 = (uint64_t *)var_3;
    *var_5 = rsi;
    var_6 = (rsi == 0UL);
    var_7 = var_0 + (-72L);
    spec_select = var_6 ? 0UL : var_7;
    var_8 = var_0 + (-80L);
    var_9 = (uint64_t *)var_8;
    *var_9 = spec_select;
    var_10 = (uint32_t *)(var_0 + (-28L));
    *var_10 = 0U;
    var_19 = 0U;
    local_sp_2 = var_3;
    rax_0 = 4294967295UL;
    if (*var_9 == 0UL) {
        var_11 = *var_5;
        var_12 = *(uint64_t *)(var_11 + 8UL);
        *(uint64_t *)var_7 = *(uint64_t *)var_11;
        *(uint64_t *)(var_0 + (-64L)) = var_12;
        var_13 = *var_5;
        var_14 = *(uint64_t *)(var_13 + 24UL);
        *(uint64_t *)(var_0 + (-56L)) = *(uint64_t *)(var_13 + 16UL);
        *(uint64_t *)(var_0 + (-48L)) = var_14;
        var_15 = *var_9;
        var_16 = var_0 + (-256L);
        *(uint64_t *)var_16 = 4267458UL;
        var_17 = indirect_placeholder_2(var_15);
        var_18 = (uint32_t)var_17;
        *var_10 = var_18;
        var_19 = var_18;
        local_sp_2 = var_16;
    }
    local_sp_3 = local_sp_2;
    local_sp_4 = local_sp_2;
    if ((int)var_19 > (int)4294967295U) {
        return rax_0;
    }
    rax_0 = 0UL;
    if ((int)*(uint32_t *)6475284UL >= (int)0U) {
        if (var_19 != 2U) {
            var_20 = *var_4;
            var_21 = local_sp_2 + (-8L);
            *(uint64_t *)var_21 = 4267526UL;
            var_22 = indirect_placeholder_2(var_20);
            local_sp_1 = var_21;
            if ((uint64_t)(uint32_t)var_22 == 0UL) {
                return rax_0;
            }
            var_23 = *var_9;
            if (*(uint64_t *)(var_23 + 8UL) == 1073741822UL) {
                var_30 = *(uint64_t **)var_8;
                var_31 = var_0 + (-232L);
                var_32 = local_sp_2 + (-16L);
                *(uint64_t *)var_32 = 4267575UL;
                var_33 = indirect_placeholder_135(var_31);
                var_34 = var_33.field_1;
                *var_30 = var_33.field_0;
                *(uint64_t *)((uint64_t)var_30 + 8UL) = var_34;
                local_sp_1 = var_32;
            } else {
                var_24 = (uint64_t *)(var_23 + 24UL);
                if (*var_24 == 1073741822UL) {
                    var_25 = var_23 + 16UL;
                    var_26 = var_0 + (-232L);
                    var_27 = local_sp_2 + (-16L);
                    *(uint64_t *)var_27 = 4267627UL;
                    var_28 = indirect_placeholder_136(var_26);
                    var_29 = var_28.field_1;
                    *(uint64_t *)var_25 = var_28.field_0;
                    *var_24 = var_29;
                    local_sp_1 = var_27;
                }
            }
            *var_10 = (*var_10 + 1U);
            local_sp_3 = local_sp_1;
        }
        var_35 = *var_4;
        var_36 = local_sp_3 + (-8L);
        *(uint64_t *)var_36 = 4267667UL;
        indirect_placeholder();
        var_37 = (uint32_t *)(var_0 + (-32L));
        var_38 = (uint32_t)var_35;
        *var_37 = var_38;
        var_40 = var_38;
        local_sp_0 = var_36;
        if ((int)var_38 > (int)0U) {
            var_39 = local_sp_3 + (-16L);
            *(uint64_t *)var_39 = 4267681UL;
            indirect_placeholder();
            *(uint32_t *)var_35 = 38U;
            var_40 = *var_37;
            local_sp_0 = var_39;
        }
        if (var_40 != 0U) {
            *(uint32_t *)6475280UL = 1U;
            *(uint32_t *)6475284UL = 1U;
            var_42 = (uint64_t)*var_37;
            rax_0 = var_42;
            return rax_0;
        }
        var_41 = local_sp_0 + (-8L);
        *(uint64_t *)var_41 = 4267698UL;
        indirect_placeholder();
        local_sp_4 = var_41;
        if (*(uint32_t *)var_35 != 38U) {
            *(uint32_t *)6475280UL = 1U;
            *(uint32_t *)6475284UL = 1U;
            var_42 = (uint64_t)*var_37;
            rax_0 = var_42;
            return rax_0;
        }
    }
    *(uint32_t *)6475284UL = 4294967295U;
    local_sp_5 = local_sp_4;
    if (*var_10 != 3U) {
        var_43 = *var_4;
        var_44 = local_sp_4 + (-8L);
        *(uint64_t *)var_44 = 4267774UL;
        var_45 = indirect_placeholder_2(var_43);
        local_sp_5 = var_44;
        if ((uint64_t)(uint32_t)var_45 == 0UL) {
            return rax_0;
        }
    }
    local_sp_6 = local_sp_5;
    if (*var_9 != 0UL) {
        var_46 = var_0 + (-232L);
        var_47 = local_sp_5 + (-8L);
        *(uint64_t *)var_47 = 4267816UL;
        var_48 = indirect_placeholder_1(var_46, var_8);
        local_sp_6 = var_47;
        if ((uint64_t)(unsigned char)var_48 == 0UL) {
            return rax_0;
        }
    }
    var_49 = (uint32_t)((uint16_t)*(uint32_t *)(var_0 + (-208L)) & (unsigned short)61440U);
    var_50 = (uint64_t)var_49;
    if ((uint64_t)((var_49 + (-40960)) & (-4096)) == 0UL) {
        *(uint64_t *)(local_sp_6 + (-8L)) = 4267876UL;
        indirect_placeholder();
        *(uint32_t *)var_50 = 38U;
    } else {
        var_51 = *var_9;
        var_52 = *var_4;
        *(uint64_t *)(local_sp_6 + (-8L)) = 4267869UL;
        var_53 = indirect_placeholder_4(var_51, 4294967295UL, var_52);
        rax_0 = var_53;
    }
}

typedef unsigned char __u_char;
typedef unsigned short __u_short;
typedef unsigned int __u_int;
typedef unsigned long __u_long;
typedef signed char __int8_t;
typedef unsigned char __uint8_t;
typedef short __int16_t;
typedef unsigned short __uint16_t;
typedef int __int32_t;
typedef unsigned int __uint32_t;
typedef long __int64_t;
typedef unsigned long __uint64_t;
typedef __int8_t __int_least8_t;
typedef __uint8_t __uint_least8_t;
typedef __int16_t __int_least16_t;
typedef __uint16_t __uint_least16_t;
typedef __int32_t __int_least32_t;
typedef __uint32_t __uint_least32_t;
typedef __int64_t __int_least64_t;
typedef __uint64_t __uint_least64_t;
typedef long __quad_t;
typedef unsigned long __u_quad_t;
typedef long __intmax_t;
typedef unsigned long __uintmax_t;
typedef unsigned long __dev_t;
typedef unsigned int __uid_t;
typedef unsigned int __gid_t;
typedef unsigned long __ino_t;
typedef unsigned long __ino64_t;
typedef unsigned int __mode_t;
typedef unsigned long __nlink_t;
typedef long __off_t;
typedef long __off64_t;
typedef int __pid_t;
typedef struct {
    int __val[2];
} __fsid_t;
typedef long __clock_t;
typedef unsigned long __rlim_t;
typedef unsigned long __rlim64_t;
typedef unsigned int __id_t;
typedef long __time_t;
typedef unsigned int __useconds_t;
typedef long __suseconds_t;
typedef int __daddr_t;
typedef int __key_t;
typedef int __clockid_t;
typedef void *__timer_t;
typedef long __blksize_t;
typedef long __blkcnt_t;
typedef long __blkcnt64_t;
typedef unsigned long __fsblkcnt_t;
typedef unsigned long __fsblkcnt64_t;
typedef unsigned long __fsfilcnt_t;
typedef unsigned long __fsfilcnt64_t;
typedef long __fsword_t;
typedef long __ssize_t;
typedef long __syscall_slong_t;
typedef unsigned long __syscall_ulong_t;
typedef __off64_t __loff_t;
typedef char *__caddr_t;
typedef long __intptr_t;
typedef unsigned int __socklen_t;
typedef int __sig_atomic_t;
typedef __int8_t int8_t;
typedef __int16_t int16_t;
typedef __int32_t int32_t;
typedef __int64_t int64_t;
typedef __uint8_t uint8_t;
typedef __uint16_t uint16_t;
typedef __uint32_t uint32_t;
typedef __uint64_t uint64_t;
typedef __int_least8_t int_least8_t;
typedef __int_least16_t int_least16_t;
typedef __int_least32_t int_least32_t;
typedef __int_least64_t int_least64_t;
typedef __uint_least8_t uint_least8_t;
typedef __uint_least16_t uint_least16_t;
typedef __uint_least32_t uint_least32_t;
typedef __uint_least64_t uint_least64_t;
typedef signed char int_fast8_t;
typedef long int_fast16_t;
typedef long int_fast32_t;
typedef long int_fast64_t;
typedef unsigned char uint_fast8_t;
typedef unsigned long uint_fast16_t;
typedef unsigned long uint_fast32_t;
typedef unsigned long uint_fast64_t;
typedef long intptr_t;
typedef unsigned long uintptr_t;
typedef __intmax_t intmax_t;
typedef __uintmax_t uintmax_t;
typedef _Bool bool;
extern uint64_t revng_init_local_sp(uint64_t param_0);
extern uint64_t init_rbp(void);
extern uint64_t indirect_placeholder_1(uint64_t param_0, uint64_t param_1);
extern uint64_t init_cc_src2(void);
extern uint64_t helper_cc_compute_c_wrapper(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint32_t param_3);
extern void indirect_placeholder(void);
extern uint64_t helper_cc_compute_all_wrapper(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint32_t param_3);
extern uint64_t indirect_placeholder_17(void);
uint64_t bb_numbered_backup(uint64_t rdx, uint64_t rcx, uint64_t rdi, uint64_t rsi, uint64_t r8) {
    uint64_t var_0;
    uint64_t var_1;
    uint64_t var_2;
    uint64_t var_3;
    uint64_t *var_4;
    uint64_t *var_5;
    uint64_t *var_6;
    uint64_t var_7;
    uint32_t *var_8;
    uint64_t **var_9;
    uint64_t var_10;
    uint64_t *var_11;
    uint64_t **var_12;
    uint64_t var_13;
    uint64_t *var_14;
    uint64_t *var_15;
    uint64_t var_16;
    uint64_t *var_17;
    bool var_18;
    uint64_t var_19;
    uint64_t *var_20;
    uint32_t var_67;
    uint32_t rax_0_shrunk;
    uint64_t var_36;
    uint64_t var_37;
    uint64_t var_38;
    uint64_t var_39;
    uint64_t local_sp_1;
    uint64_t var_64;
    unsigned char *var_65;
    unsigned char var_66;
    uint64_t local_sp_5_be;
    uint64_t local_sp_5;
    unsigned char var_42;
    uint64_t var_43;
    uint64_t var_44;
    unsigned char var_45;
    uint64_t var_47;
    uint64_t local_sp_0;
    uint64_t var_48;
    uint64_t var_49;
    uint64_t var_50;
    uint64_t var_51;
    uint64_t var_52;
    uint64_t var_53;
    uint64_t var_55;
    uint64_t var_54;
    uint64_t var_56;
    uint64_t var_57;
    uint64_t var_58;
    uint64_t var_59;
    uint64_t var_60;
    unsigned char **var_61;
    uint64_t var_62;
    uint64_t var_63;
    uint64_t var_46;
    uint64_t var_40;
    uint64_t var_41;
    uint32_t var_68;
    uint64_t local_sp_4;
    uint64_t local_sp_3;
    uint64_t var_23;
    uint64_t var_24;
    uint64_t var_25;
    uint64_t *var_26;
    uint64_t var_27;
    uint64_t *var_28;
    unsigned char *var_29;
    uint64_t *var_30;
    uint64_t *var_31;
    uint64_t *var_32;
    uint64_t var_33;
    uint64_t *var_34;
    uint64_t var_35;
    uint64_t var_21;
    uint64_t var_22;
    var_0 = revng_init_local_sp(0UL);
    var_1 = init_rbp();
    var_2 = init_cc_src2();
    *(uint64_t *)(var_0 + (-8L)) = var_1;
    var_3 = var_0 + (-128L);
    *(uint64_t *)var_3 = rdi;
    var_4 = (uint64_t *)(var_0 + (-136L));
    *var_4 = rsi;
    var_5 = (uint64_t *)(var_0 + (-144L));
    *var_5 = rdx;
    var_6 = (uint64_t *)(var_0 + (-152L));
    *var_6 = rcx;
    var_7 = var_0 + (-160L);
    *(uint64_t *)var_7 = r8;
    var_8 = (uint32_t *)(var_0 + (-12L));
    *var_8 = 2U;
    var_9 = (uint64_t **)var_7;
    var_10 = **var_9;
    var_11 = (uint64_t *)(var_0 + (-24L));
    *var_11 = var_10;
    var_12 = (uint64_t **)var_3;
    var_13 = **var_12;
    var_14 = (uint64_t *)(var_0 + (-32L));
    *var_14 = var_13;
    var_15 = (uint64_t *)(var_0 + (-40L));
    *var_15 = 1UL;
    *(uint64_t *)(var_0 + (-80L)) = (*var_14 + *var_6);
    *(uint64_t *)(var_0 + (-176L)) = 4241622UL;
    var_16 = indirect_placeholder_17();
    var_17 = (uint64_t *)(var_0 + (-88L));
    *var_17 = var_16;
    var_18 = (*var_11 == 0UL);
    var_19 = var_0 + (-184L);
    var_20 = (uint64_t *)var_19;
    var_43 = 1UL;
    rax_0_shrunk = 3U;
    local_sp_4 = var_19;
    if (var_18) {
        *var_20 = 4241645UL;
        indirect_placeholder();
    } else {
        *var_20 = 4241674UL;
        indirect_placeholder();
        *(uint64_t *)(var_0 + (-192L)) = 4241691UL;
        indirect_placeholder();
        var_21 = var_0 + (-200L);
        *(uint64_t *)var_21 = 4241703UL;
        var_22 = indirect_placeholder_17();
        *var_11 = var_22;
        local_sp_3 = var_21;
        var_23 = var_0 + (-208L);
        *(uint64_t *)var_23 = 4241719UL;
        indirect_placeholder();
        local_sp_3 = var_23;
        if (var_22 != 0UL & *(volatile uint32_t *)(uint32_t *)0UL == 12U) {
            *var_8 = 3U;
        }
        *(uint64_t *)(local_sp_3 + (-8L)) = 4241757UL;
        indirect_placeholder();
        var_24 = local_sp_3 + (-16L);
        *(uint64_t *)var_24 = 4241781UL;
        indirect_placeholder();
        var_25 = *var_11;
        local_sp_4 = var_24;
        if (var_25 != 0UL) {
            var_68 = *var_8;
            rax_0_shrunk = var_68;
            return (uint64_t)rax_0_shrunk;
        }
        **var_9 = var_25;
    }
    var_26 = (uint64_t *)(var_0 + (-96L));
    var_27 = var_0 + (-104L);
    var_28 = (uint64_t *)var_27;
    var_29 = (unsigned char *)(var_0 + (-49L));
    var_30 = (uint64_t *)(var_0 + (-64L));
    var_31 = (uint64_t *)(var_0 + (-72L));
    var_32 = (uint64_t *)(var_0 + (-112L));
    var_33 = var_0 + (-48L);
    var_34 = (uint64_t *)var_33;
    local_sp_5 = local_sp_4;
    while (1U)
        {
            var_35 = *var_11;
            *(uint64_t *)(local_sp_5 + (-8L)) = 4242448UL;
            indirect_placeholder();
            *var_26 = var_35;
            if (var_35 != 0UL) {
                **var_12 = *var_14;
                var_67 = *var_8;
                rax_0_shrunk = var_67;
                break;
            }
            var_36 = var_35 + 19UL;
            var_37 = local_sp_5 + (-16L);
            *(uint64_t *)var_37 = 4241831UL;
            indirect_placeholder();
            var_38 = *var_17 + 4UL;
            var_39 = helper_cc_compute_c_wrapper(var_36 - var_38, var_38, var_2, 17U);
            local_sp_5_be = var_37;
            if (var_39 == 0UL) {
                local_sp_5 = local_sp_5_be;
                continue;
            }
            var_40 = (uint64_t)((uint32_t)*var_14 + (uint32_t)*var_6);
            var_41 = local_sp_5 + (-24L);
            *(uint64_t *)var_41 = 4241893UL;
            indirect_placeholder();
            local_sp_5_be = var_41;
            local_sp_0 = var_41;
            *var_28 = ((*var_26 + (*var_17 + 18UL)) + 3UL);
            var_42 = **(unsigned char **)var_27;
            if (var_40 != 0UL & !(((signed char)var_42 <= '0') || ((signed char)var_42 > '9'))) {
                *var_29 = (var_42 == '9');
                *var_30 = 1UL;
                var_44 = *var_28;
                var_45 = *(unsigned char *)(var_43 + var_44);
                while ((uint64_t)(((uint32_t)(uint64_t)var_45 + (-48)) & (-2)) <= 9UL)
                    {
                        *var_29 = (*var_29 & (var_45 == '9'));
                        var_46 = *var_30 + 1UL;
                        *var_30 = var_46;
                        var_43 = var_46;
                        var_44 = *var_28;
                        var_45 = *(unsigned char *)(var_43 + var_44);
                    }
                if (var_45 != '~' & *(unsigned char *)(var_44 + (var_43 + 1UL)) != '\x00') {
                    var_47 = helper_cc_compute_c_wrapper(*var_15 - var_43, var_43, var_2, 17U);
                    if (var_47 != 0UL) {
                        if (*var_15 != *var_30) {
                            local_sp_5 = local_sp_5_be;
                            continue;
                        }
                        var_48 = *var_28;
                        var_49 = local_sp_5 + (-32L);
                        *(uint64_t *)var_49 = 4242145UL;
                        indirect_placeholder();
                        var_50 = helper_cc_compute_all_wrapper(var_48, 0UL, 0UL, 24U);
                        local_sp_5_be = var_49;
                        local_sp_0 = var_49;
                        if ((uint64_t)(((unsigned char)(var_50 >> 4UL) ^ (unsigned char)var_50) & '\xc0') != 0UL) {
                            local_sp_5 = local_sp_5_be;
                            continue;
                        }
                    }
                    *var_15 = (*var_30 + (uint64_t)*var_29);
                    *var_8 = (uint32_t)*var_29;
                    var_51 = (*var_15 + *var_5) + 4UL;
                    *var_31 = var_51;
                    var_52 = helper_cc_compute_c_wrapper(*var_4 - var_51, var_51, var_2, 17U);
                    local_sp_1 = local_sp_0;
                    if (var_52 != 0UL) {
                        var_53 = *var_31;
                        var_55 = var_53;
                        if (var_53 > 4611686018427387903UL) {
                            var_54 = var_53 << 1UL;
                            *var_31 = var_54;
                            var_55 = var_54;
                        }
                        var_56 = *var_14;
                        var_57 = local_sp_0 + (-8L);
                        *(uint64_t *)var_57 = 4242247UL;
                        var_58 = indirect_placeholder_1(var_56, var_55);
                        *var_32 = var_58;
                        local_sp_1 = var_57;
                        if (var_58 != 0UL) {
                            **var_12 = *var_14;
                            break;
                        }
                        *var_14 = var_58;
                        *var_4 = *var_31;
                    }
                    var_59 = *var_5 + *var_14;
                    *var_34 = (var_59 + 1UL);
                    *(unsigned char *)var_59 = (unsigned char)'.';
                    var_60 = *var_34;
                    *var_34 = (var_60 + 1UL);
                    *(unsigned char *)var_60 = (unsigned char)'~';
                    var_61 = (unsigned char **)var_33;
                    **var_61 = (unsigned char)'0';
                    *var_34 = (*var_34 + (uint64_t)*var_29);
                    var_62 = local_sp_1 + (-8L);
                    *(uint64_t *)var_62 = 4242385UL;
                    indirect_placeholder();
                    var_63 = *var_34 + *var_30;
                    *var_34 = var_63;
                    var_64 = var_63;
                    local_sp_5_be = var_62;
                    *var_34 = (var_64 + (-1L));
                    var_65 = *var_61;
                    var_66 = *var_65;
                    while (var_66 != '9')
                        {
                            *var_65 = (unsigned char)'0';
                            var_64 = *var_34;
                            *var_34 = (var_64 + (-1L));
                            var_65 = *var_61;
                            var_66 = *var_65;
                        }
                    *var_65 = (var_66 + '\x01');
                }
            }
        }
    return (uint64_t)rax_0_shrunk;
}

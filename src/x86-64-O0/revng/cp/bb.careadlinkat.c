typedef unsigned char __u_char;
typedef unsigned short __u_short;
typedef unsigned int __u_int;
typedef unsigned long __u_long;
typedef signed char __int8_t;
typedef unsigned char __uint8_t;
typedef short __int16_t;
typedef unsigned short __uint16_t;
typedef int __int32_t;
typedef unsigned int __uint32_t;
typedef long __int64_t;
typedef unsigned long __uint64_t;
typedef __int8_t __int_least8_t;
typedef __uint8_t __uint_least8_t;
typedef __int16_t __int_least16_t;
typedef __uint16_t __uint_least16_t;
typedef __int32_t __int_least32_t;
typedef __uint32_t __uint_least32_t;
typedef __int64_t __int_least64_t;
typedef __uint64_t __uint_least64_t;
typedef long __quad_t;
typedef unsigned long __u_quad_t;
typedef long __intmax_t;
typedef unsigned long __uintmax_t;
typedef unsigned long __dev_t;
typedef unsigned int __uid_t;
typedef unsigned int __gid_t;
typedef unsigned long __ino_t;
typedef unsigned long __ino64_t;
typedef unsigned int __mode_t;
typedef unsigned long __nlink_t;
typedef long __off_t;
typedef long __off64_t;
typedef int __pid_t;
typedef struct {
    int __val[2];
} __fsid_t;
typedef long __clock_t;
typedef unsigned long __rlim_t;
typedef unsigned long __rlim64_t;
typedef unsigned int __id_t;
typedef long __time_t;
typedef unsigned int __useconds_t;
typedef long __suseconds_t;
typedef int __daddr_t;
typedef int __key_t;
typedef int __clockid_t;
typedef void *__timer_t;
typedef long __blksize_t;
typedef long __blkcnt_t;
typedef long __blkcnt64_t;
typedef unsigned long __fsblkcnt_t;
typedef unsigned long __fsblkcnt64_t;
typedef unsigned long __fsfilcnt_t;
typedef unsigned long __fsfilcnt64_t;
typedef long __fsword_t;
typedef long __ssize_t;
typedef long __syscall_slong_t;
typedef unsigned long __syscall_ulong_t;
typedef __off64_t __loff_t;
typedef char *__caddr_t;
typedef long __intptr_t;
typedef unsigned int __socklen_t;
typedef int __sig_atomic_t;
typedef __int8_t int8_t;
typedef __int16_t int16_t;
typedef __int32_t int32_t;
typedef __int64_t int64_t;
typedef __uint8_t uint8_t;
typedef __uint16_t uint16_t;
typedef __uint32_t uint32_t;
typedef __uint64_t uint64_t;
typedef __int_least8_t int_least8_t;
typedef __int_least16_t int_least16_t;
typedef __int_least32_t int_least32_t;
typedef __int_least64_t int_least64_t;
typedef __uint_least8_t uint_least8_t;
typedef __uint_least16_t uint_least16_t;
typedef __uint_least32_t uint_least32_t;
typedef __uint_least64_t uint_least64_t;
typedef signed char int_fast8_t;
typedef long int_fast16_t;
typedef long int_fast32_t;
typedef long int_fast64_t;
typedef unsigned char uint_fast8_t;
typedef unsigned long uint_fast16_t;
typedef unsigned long uint_fast32_t;
typedef unsigned long uint_fast64_t;
typedef long intptr_t;
typedef unsigned long uintptr_t;
typedef __intmax_t intmax_t;
typedef __uintmax_t uintmax_t;
typedef _Bool bool;
extern uint64_t revng_init_local_sp(uint64_t param_0);
extern uint64_t init_rbp(void);
extern uint64_t init_cc_src2(void);
extern uint64_t helper_cc_compute_c_wrapper(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint32_t param_3);
extern void indirect_placeholder(void);
uint64_t bb_careadlinkat(uint64_t rdx, uint64_t rcx, uint64_t rdi, uint64_t rsi, uint64_t r8, uint64_t r9) {
    uint64_t var_0;
    uint64_t var_1;
    uint64_t var_2;
    uint64_t var_3;
    uint64_t *var_4;
    uint64_t *var_5;
    uint64_t var_6;
    uint64_t *var_7;
    uint64_t *var_8;
    uint64_t *var_9;
    uint64_t var_34;
    uint64_t var_35;
    uint32_t *var_21;
    uint64_t var_19;
    uint32_t var_20;
    uint64_t local_sp_4;
    uint64_t var_22;
    uint64_t local_sp_0;
    uint64_t var_23;
    uint64_t var_24;
    uint64_t var_25;
    uint64_t var_31;
    uint64_t var_32;
    uint64_t *var_33;
    uint64_t local_sp_2;
    uint64_t var_26;
    uint64_t var_27;
    uint64_t var_28;
    uint64_t local_sp_3;
    uint64_t var_29;
    uint64_t var_30;
    uint64_t rax_0;
    uint64_t local_sp_1;
    uint64_t var_36;
    uint64_t var_37;
    uint64_t var_38;
    uint64_t var_39;
    bool var_40;
    uint64_t var_41;
    uint64_t var_42;
    uint64_t var_43;
    bool var_44;
    uint64_t var_45;
    uint64_t var_46;
    uint64_t var_47;
    uint64_t var_48;
    uint64_t var_49;
    uint64_t var_10;
    uint64_t *var_11;
    uint64_t var_12;
    uint64_t *var_13;
    uint64_t *var_14;
    uint32_t *var_15;
    uint64_t *var_16;
    uint64_t var_17;
    uint64_t var_18;
    unsigned int loop_state_var;
    var_0 = revng_init_local_sp(0UL);
    var_1 = init_rbp();
    var_2 = init_cc_src2();
    *(uint64_t *)(var_0 + (-8L)) = var_1;
    var_3 = var_0 + (-1144L);
    *(uint32_t *)(var_0 + (-1100L)) = (uint32_t)rdi;
    *(uint64_t *)(var_0 + (-1112L)) = rsi;
    var_4 = (uint64_t *)(var_0 + (-1120L));
    *var_4 = rdx;
    var_5 = (uint64_t *)(var_0 + (-1128L));
    *var_5 = rcx;
    var_6 = var_0 + (-1136L);
    var_7 = (uint64_t *)var_6;
    *var_7 = r8;
    var_8 = (uint64_t *)var_3;
    *var_8 = r9;
    var_9 = (uint64_t *)(var_0 + (-32L));
    *var_9 = 9223372036854775808UL;
    rax_0 = 0UL;
    local_sp_4 = var_3;
    if (*var_7 == 0UL) {
        *var_7 = 4360832UL;
    }
    if (*var_5 == 0UL) {
        *var_4 = (var_0 + (-1096L));
        *var_5 = 1024UL;
    }
    var_10 = *var_4;
    var_11 = (uint64_t *)(var_0 + (-16L));
    *var_11 = var_10;
    var_12 = *var_5;
    var_13 = (uint64_t *)(var_0 + (-24L));
    *var_13 = var_12;
    var_14 = (uint64_t *)(var_0 + (-40L));
    var_15 = (uint32_t *)(var_0 + (-68L));
    var_16 = (uint64_t *)(var_0 + (-48L));
    while (1U)
        {
            var_17 = *var_8;
            var_18 = local_sp_4 + (-8L);
            *(uint64_t *)var_18 = 4285081UL;
            indirect_placeholder();
            *var_14 = var_17;
            var_22 = var_17;
            local_sp_0 = var_18;
            if ((long)var_17 <= (long)18446744073709551615UL) {
                var_19 = local_sp_4 + (-16L);
                *(uint64_t *)var_19 = 4285097UL;
                indirect_placeholder();
                var_20 = *(uint32_t *)var_17;
                *var_15 = var_20;
                local_sp_0 = var_19;
                if (var_20 != 34U) {
                    if (*var_11 != *var_4) {
                        loop_state_var = 2U;
                        break;
                    }
                    var_21 = *(uint32_t **)(*var_7 + 16UL);
                    *(uint64_t *)(local_sp_4 + (-24L)) = 4285141UL;
                    indirect_placeholder();
                    *(uint64_t *)(local_sp_4 + (-32L)) = 4285146UL;
                    indirect_placeholder();
                    *var_21 = *var_15;
                    loop_state_var = 2U;
                    break;
                }
                var_22 = *var_14;
            }
            *var_16 = var_22;
            var_23 = *var_13;
            var_24 = helper_cc_compute_c_wrapper(var_22 - var_23, var_23, var_2, 17U);
            local_sp_1 = local_sp_0;
            if (var_24 == 0UL) {
                if (*var_11 == *var_4) {
                    var_36 = local_sp_0 + (-8L);
                    *(uint64_t *)var_36 = 4285424UL;
                    indirect_placeholder();
                    local_sp_1 = var_36;
                }
                var_37 = *var_9 >> 1UL;
                var_38 = *var_13;
                var_39 = helper_cc_compute_c_wrapper(var_37 - var_38, var_38, var_2, 17U);
                var_40 = (var_39 == 0UL);
                var_41 = *var_13;
                local_sp_2 = local_sp_1;
                if (var_40) {
                    *var_13 = (var_41 << 1UL);
                } else {
                    var_42 = *var_9;
                    var_43 = helper_cc_compute_c_wrapper(var_41 - var_42, var_42, var_2, 17U);
                    var_44 = (var_43 == 0UL);
                    var_45 = *var_9;
                    if (!var_44) {
                        if (var_45 != 18446744073709551615UL) {
                            loop_state_var = 0U;
                            break;
                        }
                        *(uint64_t *)(local_sp_1 + (-8L)) = 4285475UL;
                        indirect_placeholder();
                        *(uint32_t *)var_41 = 36U;
                        loop_state_var = 2U;
                        break;
                    }
                    *var_13 = var_45;
                }
                var_46 = **(uint64_t **)var_6;
                var_47 = local_sp_1 + (-8L);
                *(uint64_t *)var_47 = 4285509UL;
                indirect_placeholder();
                *var_11 = var_46;
                local_sp_2 = var_47;
                local_sp_4 = var_47;
                if (var_46 == 0UL) {
                    continue;
                }
                loop_state_var = 0U;
                break;
            }
            var_25 = *var_16;
            *var_16 = (var_25 + 1UL);
            *(unsigned char *)(var_25 + *var_11) = (unsigned char)'\x00';
            if (*var_11 == (var_0 + (-1096L))) {
                var_31 = **(uint64_t **)var_6;
                var_32 = local_sp_0 + (-8L);
                *(uint64_t *)var_32 = 4285240UL;
                indirect_placeholder();
                var_33 = (uint64_t *)(var_0 + (-56L));
                *var_33 = var_31;
                *var_13 = *var_16;
                local_sp_2 = var_32;
                if (*var_33 != 0UL) {
                    loop_state_var = 0U;
                    break;
                }
                *(uint64_t *)(local_sp_0 + (-16L)) = 4285287UL;
                indirect_placeholder();
                var_34 = *var_33;
                *var_11 = var_34;
                var_35 = var_34;
                loop_state_var = 1U;
                break;
            }
            var_26 = *var_16;
            var_27 = *var_13;
            var_28 = helper_cc_compute_c_wrapper(var_26 - var_27, var_27, var_2, 17U);
            if (var_28 == 0UL) {
                var_35 = *var_11;
                loop_state_var = 1U;
                break;
            }
            var_29 = *var_11;
            var_35 = var_29;
            if (var_29 != *var_4) {
                loop_state_var = 1U;
                break;
            }
            var_30 = *(uint64_t *)(*var_7 + 8UL);
            if (var_30 != 0UL) {
                loop_state_var = 1U;
                break;
            }
            *(uint64_t *)(local_sp_0 + (-8L)) = 4285363UL;
            indirect_placeholder();
            *(uint64_t *)(var_0 + (-64L)) = var_30;
            *var_11 = var_30;
            var_35 = var_30;
            loop_state_var = 1U;
            break;
        }
    switch (loop_state_var) {
      case 1U:
        {
            rax_0 = var_35;
        }
        break;
      case 2U:
        {
            return rax_0;
        }
        break;
      case 0U:
        {
            var_48 = *(uint64_t *)(*var_7 + 24UL);
            local_sp_3 = local_sp_2;
            if (var_48 != 0UL) {
                var_49 = local_sp_2 + (-8L);
                *(uint64_t *)var_49 = 4285560UL;
                indirect_placeholder();
                local_sp_3 = var_49;
            }
            *(uint64_t *)(local_sp_3 + (-8L)) = 4285565UL;
            indirect_placeholder();
            *(uint32_t *)var_48 = 12U;
        }
        break;
    }
}

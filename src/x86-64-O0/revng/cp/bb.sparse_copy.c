typedef unsigned char __u_char;
typedef unsigned short __u_short;
typedef unsigned int __u_int;
typedef unsigned long __u_long;
typedef signed char __int8_t;
typedef unsigned char __uint8_t;
typedef short __int16_t;
typedef unsigned short __uint16_t;
typedef int __int32_t;
typedef unsigned int __uint32_t;
typedef long __int64_t;
typedef unsigned long __uint64_t;
typedef __int8_t __int_least8_t;
typedef __uint8_t __uint_least8_t;
typedef __int16_t __int_least16_t;
typedef __uint16_t __uint_least16_t;
typedef __int32_t __int_least32_t;
typedef __uint32_t __uint_least32_t;
typedef __int64_t __int_least64_t;
typedef __uint64_t __uint_least64_t;
typedef long __quad_t;
typedef unsigned long __u_quad_t;
typedef long __intmax_t;
typedef unsigned long __uintmax_t;
typedef unsigned long __dev_t;
typedef unsigned int __uid_t;
typedef unsigned int __gid_t;
typedef unsigned long __ino_t;
typedef unsigned long __ino64_t;
typedef unsigned int __mode_t;
typedef unsigned long __nlink_t;
typedef long __off_t;
typedef long __off64_t;
typedef int __pid_t;
typedef struct {
    int __val[2];
} __fsid_t;
typedef long __clock_t;
typedef unsigned long __rlim_t;
typedef unsigned long __rlim64_t;
typedef unsigned int __id_t;
typedef long __time_t;
typedef unsigned int __useconds_t;
typedef long __suseconds_t;
typedef int __daddr_t;
typedef int __key_t;
typedef int __clockid_t;
typedef void *__timer_t;
typedef long __blksize_t;
typedef long __blkcnt_t;
typedef long __blkcnt64_t;
typedef unsigned long __fsblkcnt_t;
typedef unsigned long __fsblkcnt64_t;
typedef unsigned long __fsfilcnt_t;
typedef unsigned long __fsfilcnt64_t;
typedef long __fsword_t;
typedef long __ssize_t;
typedef long __syscall_slong_t;
typedef unsigned long __syscall_ulong_t;
typedef __off64_t __loff_t;
typedef char *__caddr_t;
typedef long __intptr_t;
typedef unsigned int __socklen_t;
typedef int __sig_atomic_t;
typedef __int8_t int8_t;
typedef __int16_t int16_t;
typedef __int32_t int32_t;
typedef __int64_t int64_t;
typedef __uint8_t uint8_t;
typedef __uint16_t uint16_t;
typedef __uint32_t uint32_t;
typedef __uint64_t uint64_t;
typedef __int_least8_t int_least8_t;
typedef __int_least16_t int_least16_t;
typedef __int_least32_t int_least32_t;
typedef __int_least64_t int_least64_t;
typedef __uint_least8_t uint_least8_t;
typedef __uint_least16_t uint_least16_t;
typedef __uint_least32_t uint_least32_t;
typedef __uint_least64_t uint_least64_t;
typedef signed char int_fast8_t;
typedef long int_fast16_t;
typedef long int_fast32_t;
typedef long int_fast64_t;
typedef unsigned char uint_fast8_t;
typedef unsigned long uint_fast16_t;
typedef unsigned long uint_fast32_t;
typedef unsigned long uint_fast64_t;
typedef long intptr_t;
typedef unsigned long uintptr_t;
typedef __intmax_t intmax_t;
typedef __uintmax_t uintmax_t;
struct indirect_placeholder_131_ret_type;
struct indirect_placeholder_130_ret_type;
struct indirect_placeholder_134_ret_type;
struct indirect_placeholder_129_ret_type;
struct indirect_placeholder_131_ret_type {
    uint64_t field_0;
    uint64_t field_1;
    uint64_t field_2;
};
struct indirect_placeholder_130_ret_type {
    uint64_t field_0;
    uint64_t field_1;
    uint64_t field_2;
};
struct indirect_placeholder_134_ret_type {
    uint64_t field_0;
    uint64_t field_1;
    uint64_t field_2;
};
struct indirect_placeholder_129_ret_type {
    uint64_t field_0;
    uint64_t field_1;
    uint64_t field_2;
};
extern uint64_t revng_init_local_sp(uint64_t param_0);
extern uint64_t init_rbp(void);
extern uint64_t indirect_placeholder_1(uint64_t param_0, uint64_t param_1);
extern void indirect_placeholder(void);
extern uint64_t indirect_placeholder_4(uint64_t param_0, uint64_t param_1, uint64_t param_2);
extern uint64_t init_rbx(void);
extern uint64_t indirect_placeholder_29(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint64_t param_3);
extern struct indirect_placeholder_131_ret_type indirect_placeholder_131(uint64_t param_0, uint64_t param_1);
extern struct indirect_placeholder_130_ret_type indirect_placeholder_130(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint64_t param_3, uint64_t param_4, uint64_t param_5, uint64_t param_6);
extern struct indirect_placeholder_134_ret_type indirect_placeholder_134(uint64_t param_0, uint64_t param_1);
extern struct indirect_placeholder_129_ret_type indirect_placeholder_129(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint64_t param_3, uint64_t param_4, uint64_t param_5, uint64_t param_6);
typedef _Bool bool;
uint64_t bb_sparse_copy(uint64_t rdx, uint64_t rcx, uint64_t rdi, uint64_t rsi, uint64_t r8, uint64_t r9) {
    uint64_t local_sp_0;
    uint64_t var_0;
    uint64_t var_1;
    uint64_t var_2;
    uint64_t var_3;
    uint32_t *var_4;
    uint32_t *var_5;
    uint64_t *var_6;
    uint64_t var_7;
    uint64_t var_8;
    uint64_t *var_9;
    unsigned char *var_10;
    unsigned char **var_11;
    uint64_t **var_12;
    unsigned char *var_13;
    uint64_t *var_14;
    uint64_t *var_15;
    uint64_t *var_16;
    uint64_t *var_17;
    uint64_t *var_18;
    uint64_t *var_19;
    unsigned char *var_20;
    unsigned char *var_21;
    unsigned char *var_22;
    uint64_t *var_23;
    uint64_t local_sp_4;
    uint64_t var_44;
    struct indirect_placeholder_131_ret_type var_45;
    uint64_t var_46;
    uint64_t var_47;
    uint64_t var_48;
    uint64_t var_49;
    uint64_t rax_0;
    uint64_t local_sp_3;
    uint64_t *var_26;
    uint64_t local_sp_1;
    uint64_t var_27;
    uint64_t var_28;
    uint64_t var_29;
    unsigned char var_34;
    uint64_t local_sp_2;
    uint64_t var_30;
    uint64_t var_31;
    uint64_t var_32;
    unsigned char var_33;
    unsigned char storemerge4;
    uint64_t var_35;
    uint64_t var_36;
    unsigned char storemerge5;
    unsigned char var_37;
    unsigned char var_38;
    uint64_t var_56;
    uint64_t var_57;
    uint64_t var_58;
    struct indirect_placeholder_134_ret_type var_59;
    uint64_t var_60;
    uint64_t var_61;
    uint64_t var_62;
    uint64_t var_39;
    uint64_t var_40;
    uint64_t var_41;
    uint64_t var_42;
    uint64_t var_43;
    uint64_t var_50;
    uint64_t var_51;
    uint64_t var_52;
    uint64_t var_53;
    uint64_t var_54;
    uint64_t var_55;
    uint64_t local_sp_5;
    uint64_t var_63;
    uint64_t var_64;
    uint64_t var_65;
    uint64_t var_66;
    uint64_t var_67;
    uint64_t var_24;
    uint64_t var_25;
    unsigned int loop_state_var;
    bool switch_state_var;
    var_0 = revng_init_local_sp(0UL);
    var_1 = init_rbp();
    var_2 = init_rbx();
    *(uint64_t *)(var_0 + (-8L)) = var_1;
    *(uint64_t *)(var_0 + (-16L)) = var_2;
    var_3 = var_0 + (-136L);
    var_4 = (uint32_t *)(var_0 + (-92L));
    *var_4 = (uint32_t)rdi;
    var_5 = (uint32_t *)(var_0 + (-96L));
    *var_5 = (uint32_t)rsi;
    var_6 = (uint64_t *)(var_0 + (-104L));
    *var_6 = rdx;
    var_7 = var_0 + (-112L);
    *(uint64_t *)var_7 = rcx;
    var_8 = var_0 + (-120L);
    var_9 = (uint64_t *)var_8;
    *var_9 = r8;
    var_10 = (unsigned char *)(var_0 + (-124L));
    *var_10 = (unsigned char)r9;
    var_11 = (unsigned char **)(var_0 | 40UL);
    **var_11 = (unsigned char)'\x00';
    var_12 = (uint64_t **)(var_0 | 32UL);
    **var_12 = 0UL;
    var_13 = (unsigned char *)(var_0 + (-25L));
    *var_13 = (unsigned char)'\x00';
    var_14 = (uint64_t *)(var_0 + (-40L));
    *var_14 = 0UL;
    var_15 = (uint64_t *)(var_0 | 24UL);
    var_16 = (uint64_t *)(var_0 + (-48L));
    var_17 = (uint64_t *)(var_0 + (-56L));
    var_18 = (uint64_t *)(var_0 + (-64L));
    var_19 = (uint64_t *)(var_0 + (-72L));
    var_20 = (unsigned char *)(var_0 + (-73L));
    var_21 = (unsigned char *)(var_0 + (-74L));
    var_22 = (unsigned char *)(var_0 + (-75L));
    var_23 = (uint64_t *)(var_0 | 16UL);
    storemerge4 = (unsigned char)'\x00';
    storemerge5 = (unsigned char)'\x01';
    local_sp_4 = var_3;
    rax_0 = 0UL;
    while (1U)
        {
            local_sp_5 = local_sp_4;
            var_24 = (uint64_t)*var_4;
            var_25 = local_sp_4 + (-8L);
            *(uint64_t *)var_25 = 4215008UL;
            indirect_placeholder();
            *var_16 = var_24;
            local_sp_1 = var_25;
            local_sp_5 = var_25;
            if (!(*var_15 == 0UL && var_24 == 0UL)) {
                loop_state_var = 0U;
                break;
            }
            *var_15 = (*var_15 - var_24);
            var_26 = *var_12;
            *var_26 = (*var_26 + *var_16);
            *var_17 = *(uint64_t *)((*var_9 == 0UL) ? var_7 : var_8);
            *var_18 = *var_6;
            *var_19 = *var_6;
            while (1U)
                {
                    local_sp_2 = local_sp_1;
                    local_sp_4 = local_sp_1;
                    if (*var_16 != 0UL) {
                        loop_state_var = 1U;
                        break;
                    }
                    *var_20 = *var_13;
                    var_27 = *var_16;
                    var_28 = *var_17;
                    var_29 = (var_27 > var_28) ? var_28 : var_27;
                    *var_17 = var_29;
                    if ((*var_9 == 0UL) || (var_29 == 0UL)) {
                        var_34 = *var_13;
                    } else {
                        var_30 = *var_18;
                        var_31 = local_sp_1 + (-8L);
                        *(uint64_t *)var_31 = 4215237UL;
                        var_32 = indirect_placeholder_1(var_30, var_29);
                        var_33 = (unsigned char)var_32;
                        *var_13 = var_33;
                        var_34 = var_33;
                        local_sp_2 = var_31;
                    }
                    local_sp_3 = local_sp_2;
                    if ((uint64_t)(var_34 - *var_20) == 0UL) {
                    } else {
                        storemerge4 = (unsigned char)'\x01';
                        if (*var_14 == 0UL) {
                        }
                    }
                    *var_21 = (storemerge4 & '\x01');
                    var_35 = *var_16;
                    var_36 = *var_17;
                    if (var_35 == var_36) {
                        storemerge5 = (var_36 == 0UL) ? '\x01' : '\x00';
                    } else {
                        if (*var_13 == '\x01') {
                            storemerge5 = (var_36 == 0UL) ? '\x01' : '\x00';
                        }
                    }
                    var_37 = storemerge5 & '\x01';
                    *var_22 = var_37;
                    var_38 = *var_21;
                    if ((var_38 == '\x00') && (var_37 == '\x00')) {
                        var_56 = *var_14;
                        var_57 = *var_17;
                        if (var_56 <= (9223372036854775807UL - var_57)) {
                            var_58 = *(uint64_t *)(var_0 | 8UL);
                            *(uint64_t *)(local_sp_2 + (-8L)) = 4215628UL;
                            var_59 = indirect_placeholder_134(4UL, var_58);
                            var_60 = var_59.field_0;
                            var_61 = var_59.field_1;
                            var_62 = var_59.field_2;
                            *(uint64_t *)(local_sp_2 + (-16L)) = 4215656UL;
                            indirect_placeholder_129(0UL, 4352104UL, var_60, 0UL, 0UL, var_61, var_62);
                            loop_state_var = 0U;
                            break;
                        }
                        *var_14 = (var_57 + var_56);
                    } else {
                        if (var_38 == '\x01') {
                            *var_14 = (*var_17 + *var_14);
                        }
                        if (*var_20 == '\x01') {
                            var_50 = (uint64_t)*var_10;
                            var_51 = *var_14;
                            var_52 = *var_23;
                            var_53 = (uint64_t)*var_5;
                            var_54 = local_sp_2 + (-8L);
                            *(uint64_t *)var_54 = 4215492UL;
                            var_55 = indirect_placeholder_29(var_50, var_51, var_53, var_52);
                            local_sp_0 = var_54;
                            if ((uint64_t)(unsigned char)var_55 != 1UL) {
                                loop_state_var = 0U;
                                break;
                            }
                        }
                        var_39 = *var_14;
                        var_40 = *var_19;
                        var_41 = (uint64_t)*var_5;
                        var_42 = local_sp_2 + (-8L);
                        *(uint64_t *)var_42 = 4215396UL;
                        var_43 = indirect_placeholder_4(var_39, var_41, var_40);
                        local_sp_0 = var_42;
                        if (var_43 != *var_14) {
                            var_44 = *var_23;
                            *(uint64_t *)(local_sp_2 + (-16L)) = 4215425UL;
                            var_45 = indirect_placeholder_131(4UL, var_44);
                            var_46 = var_45.field_0;
                            var_47 = var_45.field_1;
                            var_48 = var_45.field_2;
                            *(uint64_t *)(local_sp_2 + (-24L)) = 4215433UL;
                            indirect_placeholder();
                            var_49 = (uint64_t)*(uint32_t *)var_46;
                            *(uint64_t *)(local_sp_2 + (-32L)) = 4215460UL;
                            indirect_placeholder_130(0UL, 4352087UL, var_46, 0UL, var_49, var_47, var_48);
                            loop_state_var = 0U;
                            break;
                        }
                        *var_19 = *var_18;
                        *var_14 = *var_17;
                        local_sp_3 = local_sp_0;
                        if (*var_22 != '\x00') {
                            if (*var_17 == 0UL) {
                                *var_16 = 0UL;
                            }
                            if (*var_21 == '\x00') {
                                *var_14 = 0UL;
                            } else {
                                *var_17 = 0UL;
                            }
                        }
                    }
                }
            switch_state_var = 0;
            switch (loop_state_var) {
              case 1U:
                {
                    **var_11 = *var_13;
                    continue;
                }
                break;
              case 0U:
                {
                    loop_state_var = 1U;
                    switch_state_var = 1;
                    break;
                }
                break;
            }
            if (switch_state_var)
                break;
        }
    switch (loop_state_var) {
      case 1U:
        {
            return rax_0;
        }
        break;
      case 0U:
        {
            rax_0 = 1UL;
            if (*var_13 == '\x00') {
            }
        }
        break;
    }
}

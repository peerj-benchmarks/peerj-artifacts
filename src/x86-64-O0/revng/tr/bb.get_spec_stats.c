typedef unsigned char __u_char;
typedef unsigned short __u_short;
typedef unsigned int __u_int;
typedef unsigned long __u_long;
typedef signed char __int8_t;
typedef unsigned char __uint8_t;
typedef short __int16_t;
typedef unsigned short __uint16_t;
typedef int __int32_t;
typedef unsigned int __uint32_t;
typedef long __int64_t;
typedef unsigned long __uint64_t;
typedef __int8_t __int_least8_t;
typedef __uint8_t __uint_least8_t;
typedef __int16_t __int_least16_t;
typedef __uint16_t __uint_least16_t;
typedef __int32_t __int_least32_t;
typedef __uint32_t __uint_least32_t;
typedef __int64_t __int_least64_t;
typedef __uint64_t __uint_least64_t;
typedef long __quad_t;
typedef unsigned long __u_quad_t;
typedef long __intmax_t;
typedef unsigned long __uintmax_t;
typedef unsigned long __dev_t;
typedef unsigned int __uid_t;
typedef unsigned int __gid_t;
typedef unsigned long __ino_t;
typedef unsigned long __ino64_t;
typedef unsigned int __mode_t;
typedef unsigned long __nlink_t;
typedef long __off_t;
typedef long __off64_t;
typedef int __pid_t;
typedef struct {
    int __val[2];
} __fsid_t;
typedef long __clock_t;
typedef unsigned long __rlim_t;
typedef unsigned long __rlim64_t;
typedef unsigned int __id_t;
typedef long __time_t;
typedef unsigned int __useconds_t;
typedef long __suseconds_t;
typedef int __daddr_t;
typedef int __key_t;
typedef int __clockid_t;
typedef void *__timer_t;
typedef long __blksize_t;
typedef long __blkcnt_t;
typedef long __blkcnt64_t;
typedef unsigned long __fsblkcnt_t;
typedef unsigned long __fsblkcnt64_t;
typedef unsigned long __fsfilcnt_t;
typedef unsigned long __fsfilcnt64_t;
typedef long __fsword_t;
typedef long __ssize_t;
typedef long __syscall_slong_t;
typedef unsigned long __syscall_ulong_t;
typedef __off64_t __loff_t;
typedef char *__caddr_t;
typedef long __intptr_t;
typedef unsigned int __socklen_t;
typedef int __sig_atomic_t;
typedef __int8_t int8_t;
typedef __int16_t int16_t;
typedef __int32_t int32_t;
typedef __int64_t int64_t;
typedef __uint8_t uint8_t;
typedef __uint16_t uint16_t;
typedef __uint32_t uint32_t;
typedef __uint64_t uint64_t;
typedef __int_least8_t int_least8_t;
typedef __int_least16_t int_least16_t;
typedef __int_least32_t int_least32_t;
typedef __int_least64_t int_least64_t;
typedef __uint_least8_t uint_least8_t;
typedef __uint_least16_t uint_least16_t;
typedef __uint_least32_t uint_least32_t;
typedef __uint_least64_t uint_least64_t;
typedef signed char int_fast8_t;
typedef long int_fast16_t;
typedef long int_fast32_t;
typedef long int_fast64_t;
typedef unsigned char uint_fast8_t;
typedef unsigned long uint_fast16_t;
typedef unsigned long uint_fast32_t;
typedef unsigned long uint_fast64_t;
typedef long intptr_t;
typedef unsigned long uintptr_t;
typedef __intmax_t intmax_t;
typedef __uintmax_t uintmax_t;
typedef _Bool bool;
struct indirect_placeholder_47_ret_type;
struct indirect_placeholder_47_ret_type {
    uint64_t field_0;
    uint64_t field_1;
    uint64_t field_2;
};
extern void abort(void);
extern uint64_t revng_init_local_sp(uint64_t param_0);
extern uint64_t init_rbp(void);
extern uint64_t indirect_placeholder_2(uint64_t param_0, uint64_t param_1);
extern uint64_t init_cc_src2(void);
extern uint64_t helper_cc_compute_c_wrapper(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint32_t param_3);
extern void indirect_placeholder_1(void);
extern struct indirect_placeholder_47_ret_type indirect_placeholder_47(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint64_t param_3, uint64_t param_4, uint64_t param_5, uint64_t param_6);
void bb_get_spec_stats(uint64_t rcx, uint64_t rdi, uint64_t r9, uint64_t r8) {
    struct indirect_placeholder_47_ret_type var_42;
    uint64_t var_14;
    uint64_t var_0;
    uint64_t var_1;
    uint64_t var_2;
    uint64_t var_3;
    uint64_t var_4;
    uint64_t *var_5;
    uint64_t *var_6;
    uint64_t var_7;
    uint64_t var_8;
    uint64_t *var_9;
    uint64_t *var_10;
    uint32_t *var_11;
    uint32_t *var_12;
    uint64_t *var_13;
    uint32_t var_24;
    uint32_t var_19;
    uint32_t var_32;
    uint32_t var_25;
    uint64_t rcx1_3;
    uint64_t local_sp_0;
    uint64_t r93_1;
    uint64_t rcx1_0;
    uint64_t r84_1;
    uint64_t r93_0;
    uint64_t r84_0;
    uint32_t var_15;
    uint64_t rcx1_2;
    uint64_t var_16;
    uint64_t var_17;
    uint64_t local_sp_4;
    uint64_t *var_18;
    uint64_t local_sp_1;
    uint64_t local_sp_2;
    uint64_t var_20;
    uint64_t var_21;
    uint64_t var_22;
    uint64_t var_23;
    bool var_26;
    uint32_t var_27;
    uint64_t var_28;
    uint64_t var_29;
    uint64_t var_30;
    uint64_t var_31;
    uint64_t var_33;
    uint64_t var_34;
    uint64_t var_35;
    uint64_t var_36;
    uint64_t local_sp_3;
    uint64_t var_37;
    uint64_t rcx1_1;
    uint64_t var_38;
    uint64_t var_40;
    uint64_t var_43;
    uint64_t local_sp_5;
    uint64_t var_44;
    uint64_t var_41;
    uint64_t var_39;
    var_0 = revng_init_local_sp(0UL);
    var_1 = init_rbp();
    var_2 = init_cc_src2();
    *(uint64_t *)(var_0 + (-8L)) = var_1;
    var_3 = var_0 + (-72L);
    var_4 = var_0 + (-64L);
    var_5 = (uint64_t *)var_4;
    *var_5 = rdi;
    var_6 = (uint64_t *)(var_0 + (-24L));
    *var_6 = 0UL;
    *(uint64_t *)(*var_5 + 32UL) = 0UL;
    *(unsigned char *)(*var_5 + 48UL) = (unsigned char)'\x00';
    *(unsigned char *)(*var_5 + 50UL) = (unsigned char)'\x00';
    *(unsigned char *)(*var_5 + 49UL) = (unsigned char)'\x00';
    var_7 = *(uint64_t *)(**(uint64_t **)var_4 + 8UL);
    var_8 = var_0 + (-16L);
    var_9 = (uint64_t *)var_8;
    *var_9 = var_7;
    var_10 = (uint64_t *)(var_0 + (-32L));
    var_11 = (uint32_t *)(var_0 + (-40L));
    var_12 = (uint32_t *)(var_0 + (-36L));
    var_13 = (uint64_t *)(var_0 + (-48L));
    var_14 = var_7;
    var_19 = 0U;
    var_25 = 0U;
    local_sp_0 = var_3;
    rcx1_0 = rcx;
    r93_0 = r9;
    r84_0 = r8;
    while (1U)
        {
            r93_1 = r93_0;
            r84_1 = r84_0;
            rcx1_2 = rcx1_0;
            local_sp_4 = local_sp_0;
            local_sp_1 = local_sp_0;
            local_sp_2 = local_sp_0;
            local_sp_3 = local_sp_0;
            rcx1_1 = rcx1_0;
            if (var_14 == 0UL) {
                *(uint64_t *)(*var_5 + 24UL) = *var_6;
                return;
            }
            *var_10 = 0UL;
            var_15 = **(uint32_t **)var_8;
            if (var_15 > 4U) {
                var_39 = local_sp_0 + (-8L);
                *(uint64_t *)var_39 = 4209383UL;
                indirect_placeholder_1();
                local_sp_4 = var_39;
            } else {
                switch (*(uint64_t *)(((uint64_t)var_15 << 3UL) + 4282640UL)) {
                  case 4209055UL:
                    {
                        *var_10 = 1UL;
                    }
                    break;
                  case 4209068UL:
                    {
                        var_33 = *var_9;
                        var_34 = (uint64_t)*(unsigned char *)(var_33 + 17UL);
                        var_35 = (uint64_t)*(unsigned char *)(var_33 + 16UL);
                        var_36 = helper_cc_compute_c_wrapper(var_34 - var_35, var_35, var_2, 14U);
                        if (var_36 != 0UL) {
                            var_37 = local_sp_0 + (-8L);
                            *(uint64_t *)var_37 = 4209113UL;
                            indirect_placeholder_1();
                            local_sp_3 = var_37;
                            rcx1_1 = 4283958UL;
                        }
                        var_38 = *var_9;
                        *var_10 = (uint64_t)((long)(((uint64_t)((uint32_t)(uint64_t)*(unsigned char *)(var_38 + 17UL) - (uint32_t)(uint64_t)*(unsigned char *)(var_38 + 16UL)) << 32UL) + 4294967296UL) >> (long)32UL);
                        local_sp_4 = local_sp_3;
                        rcx1_2 = rcx1_1;
                    }
                    break;
                  case 4209156UL:
                    {
                        *(unsigned char *)(*var_5 + 49UL) = (unsigned char)'\x01';
                        *var_12 = 0U;
                        var_26 = ((int)var_25 > (int)255U);
                        var_27 = *(uint32_t *)(*var_9 + 16UL);
                        var_28 = (uint64_t)var_27;
                        local_sp_4 = local_sp_2;
                        while (!var_26)
                            {
                                var_29 = (uint64_t)(uint32_t)(unsigned char)var_25;
                                var_30 = local_sp_2 + (-8L);
                                *(uint64_t *)var_30 = 4209195UL;
                                var_31 = indirect_placeholder_2(var_28, var_29);
                                local_sp_2 = var_30;
                                if ((uint64_t)(unsigned char)var_31 == 0UL) {
                                    *var_10 = (*var_10 + 1UL);
                                }
                                var_32 = *var_12 + 1U;
                                *var_12 = var_32;
                                var_25 = var_32;
                                var_26 = ((int)var_25 > (int)255U);
                                var_27 = *(uint32_t *)(*var_9 + 16UL);
                                var_28 = (uint64_t)var_27;
                                local_sp_4 = local_sp_2;
                            }
                        if ((uint64_t)(var_27 + (-6)) != 0UL & (uint64_t)(var_27 + (-10)) == 0UL) {
                            *(unsigned char *)(*var_5 + 50UL) = (unsigned char)'\x01';
                        }
                    }
                    break;
                  case 4209250UL:
                    {
                        *var_11 = 0U;
                        local_sp_4 = local_sp_1;
                        while ((int)var_19 <= (int)255U)
                            {
                                var_20 = (uint64_t)*(unsigned char *)(*var_9 + 16UL);
                                var_21 = (uint64_t)(uint32_t)(unsigned char)var_19;
                                var_22 = local_sp_1 + (-8L);
                                *(uint64_t *)var_22 = 4209285UL;
                                var_23 = indirect_placeholder_2(var_20, var_21);
                                local_sp_1 = var_22;
                                if ((uint64_t)(unsigned char)var_23 == 0UL) {
                                    *var_10 = (*var_10 + 1UL);
                                }
                                var_24 = *var_11 + 1U;
                                *var_11 = var_24;
                                var_19 = var_24;
                                local_sp_4 = local_sp_1;
                            }
                        *(unsigned char *)(*var_5 + 48UL) = (unsigned char)'\x01';
                    }
                    break;
                  case 4209317UL:
                    {
                        var_16 = *var_9;
                        var_17 = *(uint64_t *)(var_16 + 24UL);
                        if (var_17 == 0UL) {
                            *(uint64_t *)(*var_5 + 40UL) = var_16;
                            var_18 = (uint64_t *)(*var_5 + 32UL);
                            *var_18 = (*var_18 + 1UL);
                        } else {
                            *var_10 = var_17;
                        }
                    }
                    break;
                  default:
                    {
                        abort();
                    }
                    break;
                }
            }
        }
}

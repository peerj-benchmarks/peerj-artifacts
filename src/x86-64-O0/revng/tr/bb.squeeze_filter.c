typedef unsigned char __u_char;
typedef unsigned short __u_short;
typedef unsigned int __u_int;
typedef unsigned long __u_long;
typedef signed char __int8_t;
typedef unsigned char __uint8_t;
typedef short __int16_t;
typedef unsigned short __uint16_t;
typedef int __int32_t;
typedef unsigned int __uint32_t;
typedef long __int64_t;
typedef unsigned long __uint64_t;
typedef __int8_t __int_least8_t;
typedef __uint8_t __uint_least8_t;
typedef __int16_t __int_least16_t;
typedef __uint16_t __uint_least16_t;
typedef __int32_t __int_least32_t;
typedef __uint32_t __uint_least32_t;
typedef __int64_t __int_least64_t;
typedef __uint64_t __uint_least64_t;
typedef long __quad_t;
typedef unsigned long __u_quad_t;
typedef long __intmax_t;
typedef unsigned long __uintmax_t;
typedef unsigned long __dev_t;
typedef unsigned int __uid_t;
typedef unsigned int __gid_t;
typedef unsigned long __ino_t;
typedef unsigned long __ino64_t;
typedef unsigned int __mode_t;
typedef unsigned long __nlink_t;
typedef long __off_t;
typedef long __off64_t;
typedef int __pid_t;
typedef struct {
    int __val[2];
} __fsid_t;
typedef long __clock_t;
typedef unsigned long __rlim_t;
typedef unsigned long __rlim64_t;
typedef unsigned int __id_t;
typedef long __time_t;
typedef unsigned int __useconds_t;
typedef long __suseconds_t;
typedef int __daddr_t;
typedef int __key_t;
typedef int __clockid_t;
typedef void *__timer_t;
typedef long __blksize_t;
typedef long __blkcnt_t;
typedef long __blkcnt64_t;
typedef unsigned long __fsblkcnt_t;
typedef unsigned long __fsblkcnt64_t;
typedef unsigned long __fsfilcnt_t;
typedef unsigned long __fsfilcnt64_t;
typedef long __fsword_t;
typedef long __ssize_t;
typedef long __syscall_slong_t;
typedef unsigned long __syscall_ulong_t;
typedef __off64_t __loff_t;
typedef char *__caddr_t;
typedef long __intptr_t;
typedef unsigned int __socklen_t;
typedef int __sig_atomic_t;
typedef __int8_t int8_t;
typedef __int16_t int16_t;
typedef __int32_t int32_t;
typedef __int64_t int64_t;
typedef __uint8_t uint8_t;
typedef __uint16_t uint16_t;
typedef __uint32_t uint32_t;
typedef __uint64_t uint64_t;
typedef __int_least8_t int_least8_t;
typedef __int_least16_t int_least16_t;
typedef __int_least32_t int_least32_t;
typedef __int_least64_t int_least64_t;
typedef __uint_least8_t uint_least8_t;
typedef __uint_least16_t uint_least16_t;
typedef __uint_least32_t uint_least32_t;
typedef __uint_least64_t uint_least64_t;
typedef signed char int_fast8_t;
typedef long int_fast16_t;
typedef long int_fast32_t;
typedef long int_fast64_t;
typedef unsigned char uint_fast8_t;
typedef unsigned long uint_fast16_t;
typedef unsigned long uint_fast32_t;
typedef unsigned long uint_fast64_t;
typedef long intptr_t;
typedef unsigned long uintptr_t;
typedef __intmax_t intmax_t;
typedef __uintmax_t uintmax_t;
struct indirect_placeholder_37_ret_type;
struct indirect_placeholder_37_ret_type {
    uint64_t field_0;
    uint64_t field_1;
    uint64_t field_2;
};
extern uint64_t revng_init_local_sp(uint64_t param_0);
extern uint64_t init_rbp(void);
extern uint64_t init_cc_src2(void);
extern uint64_t helper_cc_compute_c_wrapper(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint32_t param_3);
extern void indirect_placeholder_1(void);
extern uint64_t init_rdi(void);
extern uint64_t init_rsi(void);
extern uint64_t indirect_placeholder_38(uint64_t param_0);
extern uint64_t init_r9(void);
extern uint64_t init_r8(void);
extern struct indirect_placeholder_37_ret_type indirect_placeholder_37(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint64_t param_3, uint64_t param_4, uint64_t param_5, uint64_t param_6);
void bb_squeeze_filter(uint64_t rdx) {
    struct indirect_placeholder_37_ret_type var_45;
    uint64_t var_16;
    uint64_t var_0;
    uint64_t var_1;
    uint64_t var_2;
    uint64_t var_3;
    uint64_t var_4;
    uint64_t var_5;
    uint64_t var_6;
    uint64_t var_7;
    uint64_t *var_8;
    uint64_t *var_9;
    uint32_t *var_10;
    uint32_t *var_11;
    uint64_t *var_12;
    uint64_t *var_13;
    uint64_t *var_14;
    uint64_t *var_15;
    uint64_t var_43;
    uint64_t var_44;
    uint64_t local_sp_5;
    uint64_t var_32;
    uint64_t var_33;
    uint64_t var_27;
    uint64_t var_21;
    uint64_t var_20;
    uint64_t r9_1;
    uint64_t local_sp_0;
    uint64_t r8_1;
    uint64_t r9_0;
    uint64_t r8_0;
    uint64_t var_17;
    uint64_t local_sp_1;
    uint64_t var_18;
    uint64_t var_19;
    uint64_t local_sp_2;
    uint64_t var_22;
    uint64_t var_23;
    uint64_t local_sp_3;
    uint64_t var_47;
    uint64_t var_24;
    uint64_t var_25;
    uint64_t var_26;
    uint64_t var_28;
    uint64_t var_29;
    uint64_t var_30;
    uint64_t var_31;
    uint64_t local_sp_4;
    uint64_t var_34;
    uint64_t var_35;
    uint64_t var_39;
    uint64_t var_40;
    uint64_t var_36;
    uint64_t var_37;
    uint64_t var_38;
    uint64_t var_41;
    uint64_t var_42;
    uint64_t var_46;
    uint64_t var_48;
    uint64_t var_52;
    uint64_t var_49;
    uint64_t var_50;
    uint64_t var_51;
    uint64_t var_53;
    uint64_t var_54;
    unsigned int loop_state_var;
    var_0 = revng_init_local_sp(0UL);
    var_1 = init_rbp();
    var_2 = init_cc_src2();
    var_3 = init_rdi();
    var_4 = init_rsi();
    var_5 = init_r9();
    var_6 = init_r8();
    *(uint64_t *)(var_0 + (-8L)) = var_1;
    var_7 = var_0 + (-88L);
    var_8 = (uint64_t *)(var_0 + (-64L));
    *var_8 = var_3;
    *(uint64_t *)(var_0 + (-72L)) = var_4;
    var_9 = (uint64_t *)(var_0 + (-80L));
    *var_9 = rdx;
    var_10 = (uint32_t *)(var_0 + (-44L));
    *var_10 = 2147483647U;
    var_11 = (uint32_t *)(var_0 + (-12L));
    *var_11 = 2147483647U;
    var_12 = (uint64_t *)(var_0 + (-24L));
    *var_12 = 0UL;
    var_13 = (uint64_t *)(var_0 + (-32L));
    *var_13 = 0UL;
    var_14 = (uint64_t *)(var_0 + (-56L));
    var_15 = (uint64_t *)(var_0 + (-40L));
    var_16 = 0UL;
    var_20 = 0UL;
    local_sp_0 = var_7;
    r9_0 = var_5;
    r8_0 = var_6;
    var_38 = 0UL;
    while (1U)
        {
            var_17 = helper_cc_compute_c_wrapper(*var_12 - var_16, var_16, var_2, 17U);
            local_sp_1 = local_sp_0;
            r9_1 = r9_0;
            r8_1 = r8_0;
            if (var_17 == 0UL) {
                var_20 = *var_12;
            } else {
                var_18 = *var_9;
                var_19 = local_sp_0 + (-8L);
                *(uint64_t *)var_19 = 4210776UL;
                indirect_placeholder_1();
                *var_13 = var_18;
                local_sp_1 = var_19;
                if (var_18 != 0UL) {
                    break;
                }
                *var_12 = 0UL;
            }
            *var_14 = var_20;
            local_sp_2 = local_sp_1;
            local_sp_5 = local_sp_1;
            if ((uint64_t)(*var_11 - *var_10) != 0UL) {
                var_21 = *var_12;
                var_22 = *var_13;
                var_23 = helper_cc_compute_c_wrapper(var_21 - var_22, var_22, var_2, 17U);
                local_sp_3 = local_sp_2;
                while (var_23 != 0UL)
                    {
                        var_24 = (uint64_t)(uint32_t)(uint64_t)*(unsigned char *)(*var_12 + *var_8);
                        var_25 = local_sp_2 + (-8L);
                        *(uint64_t *)var_25 = 4210861UL;
                        var_26 = indirect_placeholder_38(var_24);
                        local_sp_2 = var_25;
                        local_sp_3 = var_25;
                        if (*(unsigned char *)((uint64_t)(unsigned char)var_26 | 6396416UL) == '\x01') {
                            break;
                        }
                        var_27 = *var_12 + 2UL;
                        *var_12 = var_27;
                        var_21 = var_27;
                        var_22 = *var_13;
                        var_23 = helper_cc_compute_c_wrapper(var_21 - var_22, var_22, var_2, 17U);
                        local_sp_3 = local_sp_2;
                    }
                var_28 = *var_12;
                var_33 = var_28;
                local_sp_4 = local_sp_3;
                if (var_28 != *var_13) {
                    var_29 = (uint64_t)(uint32_t)(uint64_t)*(unsigned char *)(*var_8 + (var_28 + (-1L)));
                    var_30 = local_sp_3 + (-8L);
                    *(uint64_t *)var_30 = 4210918UL;
                    var_31 = indirect_placeholder_38(var_29);
                    local_sp_4 = var_30;
                    if (*(unsigned char *)((uint64_t)(unsigned char)var_31 | 6396416UL) == '\x00') {
                        var_33 = *var_12;
                    } else {
                        var_32 = *var_12 + (-1L);
                        *var_12 = var_32;
                        var_33 = var_32;
                    }
                }
                var_34 = *var_13;
                var_35 = helper_cc_compute_c_wrapper(var_33 - var_34, var_34, var_2, 17U);
                local_sp_5 = local_sp_4;
                if (var_35 == 0UL) {
                    var_39 = *var_13 - *var_14;
                    *var_15 = var_39;
                    var_40 = var_39;
                } else {
                    *var_11 = (uint32_t)*(unsigned char *)(*var_12 + *var_8);
                    var_36 = *var_12 - *var_14;
                    *var_15 = (var_36 + 1UL);
                    var_37 = *var_12;
                    var_38 = var_37;
                    if (var_37 != 0UL & (uint64_t)((uint32_t)(uint64_t)*(unsigned char *)(*var_8 + (var_37 + (-1L))) - *var_11) == 0UL) {
                        *var_15 = var_36;
                        var_38 = *var_12;
                    }
                    *var_12 = (var_38 + 1UL);
                    var_40 = *var_15;
                }
                var_41 = *(uint64_t *)6395136UL;
                var_42 = local_sp_4 + (-8L);
                *(uint64_t *)var_42 = 4211088UL;
                indirect_placeholder_1();
                local_sp_5 = var_42;
                if (var_40 != 0UL & var_40 == *var_15) {
                    *(uint64_t *)(local_sp_4 + (-16L)) = 4211099UL;
                    indirect_placeholder_1();
                    var_43 = (uint64_t)*(uint32_t *)var_40;
                    var_44 = local_sp_4 + (-24L);
                    *(uint64_t *)var_44 = 4211123UL;
                    var_45 = indirect_placeholder_37(0UL, 4283391UL, var_41, 1UL, var_43, r9_0, r8_0);
                    local_sp_5 = var_44;
                    r9_1 = var_45.field_1;
                    r8_1 = var_45.field_2;
                }
            }
            local_sp_0 = local_sp_5;
            r9_0 = r9_1;
            r8_0 = r8_1;
            if ((uint64_t)(*var_11 - *var_10) != 0UL) {
                var_46 = *var_12;
                while (1U)
                    {
                        var_47 = *var_13;
                        var_48 = helper_cc_compute_c_wrapper(var_46 - var_47, var_47, var_2, 17U);
                        if (var_48 != 0UL) {
                            loop_state_var = 1U;
                            break;
                        }
                        var_49 = *var_8;
                        var_50 = *var_12;
                        var_52 = var_50;
                        if ((uint64_t)((uint32_t)(uint64_t)*(unsigned char *)(var_50 + var_49) - *var_11) != 0UL) {
                            loop_state_var = 0U;
                            break;
                        }
                        var_51 = var_50 + 1UL;
                        *var_12 = var_51;
                        var_46 = var_51;
                        continue;
                    }
                switch (loop_state_var) {
                  case 0U:
                    {
                    }
                    break;
                  case 1U:
                    {
                        var_52 = *var_12;
                    }
                    break;
                }
                var_53 = *var_13;
                var_54 = helper_cc_compute_c_wrapper(var_52 - var_53, var_53, var_2, 17U);
                if (var_54 == 0UL) {
                    *var_11 = *var_10;
                }
            }
            var_16 = *var_13;
            continue;
        }
    return;
}

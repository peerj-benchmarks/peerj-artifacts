typedef unsigned char __u_char;
typedef unsigned short __u_short;
typedef unsigned int __u_int;
typedef unsigned long __u_long;
typedef signed char __int8_t;
typedef unsigned char __uint8_t;
typedef short __int16_t;
typedef unsigned short __uint16_t;
typedef int __int32_t;
typedef unsigned int __uint32_t;
typedef long __int64_t;
typedef unsigned long __uint64_t;
typedef __int8_t __int_least8_t;
typedef __uint8_t __uint_least8_t;
typedef __int16_t __int_least16_t;
typedef __uint16_t __uint_least16_t;
typedef __int32_t __int_least32_t;
typedef __uint32_t __uint_least32_t;
typedef __int64_t __int_least64_t;
typedef __uint64_t __uint_least64_t;
typedef long __quad_t;
typedef unsigned long __u_quad_t;
typedef long __intmax_t;
typedef unsigned long __uintmax_t;
typedef unsigned long __dev_t;
typedef unsigned int __uid_t;
typedef unsigned int __gid_t;
typedef unsigned long __ino_t;
typedef unsigned long __ino64_t;
typedef unsigned int __mode_t;
typedef unsigned long __nlink_t;
typedef long __off_t;
typedef long __off64_t;
typedef int __pid_t;
typedef struct {
    int __val[2];
} __fsid_t;
typedef long __clock_t;
typedef unsigned long __rlim_t;
typedef unsigned long __rlim64_t;
typedef unsigned int __id_t;
typedef long __time_t;
typedef unsigned int __useconds_t;
typedef long __suseconds_t;
typedef int __daddr_t;
typedef int __key_t;
typedef int __clockid_t;
typedef void *__timer_t;
typedef long __blksize_t;
typedef long __blkcnt_t;
typedef long __blkcnt64_t;
typedef unsigned long __fsblkcnt_t;
typedef unsigned long __fsblkcnt64_t;
typedef unsigned long __fsfilcnt_t;
typedef unsigned long __fsfilcnt64_t;
typedef long __fsword_t;
typedef long __ssize_t;
typedef long __syscall_slong_t;
typedef unsigned long __syscall_ulong_t;
typedef __off64_t __loff_t;
typedef char *__caddr_t;
typedef long __intptr_t;
typedef unsigned int __socklen_t;
typedef int __sig_atomic_t;
typedef __int8_t int8_t;
typedef __int16_t int16_t;
typedef __int32_t int32_t;
typedef __int64_t int64_t;
typedef __uint8_t uint8_t;
typedef __uint16_t uint16_t;
typedef __uint32_t uint32_t;
typedef __uint64_t uint64_t;
typedef __int_least8_t int_least8_t;
typedef __int_least16_t int_least16_t;
typedef __int_least32_t int_least32_t;
typedef __int_least64_t int_least64_t;
typedef __uint_least8_t uint_least8_t;
typedef __uint_least16_t uint_least16_t;
typedef __uint_least32_t uint_least32_t;
typedef __uint_least64_t uint_least64_t;
typedef signed char int_fast8_t;
typedef long int_fast16_t;
typedef long int_fast32_t;
typedef long int_fast64_t;
typedef unsigned char uint_fast8_t;
typedef unsigned long uint_fast16_t;
typedef unsigned long uint_fast32_t;
typedef unsigned long uint_fast64_t;
typedef long intptr_t;
typedef unsigned long uintptr_t;
typedef __intmax_t intmax_t;
typedef __uintmax_t uintmax_t;
struct bb_validate_case_classes_ret_type;
struct indirect_placeholder_44_ret_type;
struct indirect_placeholder_46_ret_type;
struct indirect_placeholder_45_ret_type;
struct bb_validate_case_classes_ret_type {
    uint64_t field_0;
    uint64_t field_1;
    uint64_t field_2;
};
struct indirect_placeholder_44_ret_type {
    uint64_t field_0;
    uint64_t field_1;
    uint64_t field_2;
};
struct indirect_placeholder_46_ret_type {
    uint64_t field_0;
    uint64_t field_1;
};
struct indirect_placeholder_45_ret_type {
    uint64_t field_0;
    uint64_t field_1;
};
extern uint64_t revng_init_local_sp(uint64_t param_0);
extern uint64_t init_rbp(void);
extern void indirect_placeholder_1(void);
extern void indirect_placeholder_3(uint64_t param_0);
extern uint64_t init_r9(void);
extern uint64_t init_r8(void);
extern struct indirect_placeholder_44_ret_type indirect_placeholder_44(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint64_t param_3, uint64_t param_4, uint64_t param_5, uint64_t param_6);
extern struct indirect_placeholder_46_ret_type indirect_placeholder_46(uint64_t param_0, uint64_t param_1);
extern struct indirect_placeholder_45_ret_type indirect_placeholder_45(uint64_t param_0, uint64_t param_1);
struct bb_validate_case_classes_ret_type bb_validate_case_classes(uint64_t rcx, uint64_t rdi, uint64_t rsi) {
    uint64_t rcx3_0;
    uint64_t var_0;
    uint64_t var_1;
    uint64_t var_2;
    uint64_t var_3;
    uint64_t var_4;
    uint64_t *var_5;
    uint64_t *var_6;
    uint64_t var_7;
    uint64_t *var_8;
    uint64_t var_9;
    uint64_t *var_10;
    uint32_t *var_11;
    struct indirect_placeholder_44_ret_type var_40;
    uint32_t *var_12;
    uint64_t var_13;
    uint64_t *var_14;
    uint64_t var_15;
    uint64_t *var_16;
    uint64_t var_17;
    uint64_t *var_18;
    uint64_t var_19;
    uint64_t *var_20;
    unsigned char *var_21;
    unsigned char *var_22;
    uint64_t rcx3_3;
    uint64_t r8_1;
    uint64_t rcx3_1;
    uint64_t var_39;
    uint64_t local_sp_0;
    uint64_t r9_0;
    uint64_t r8_0;
    uint64_t local_sp_1;
    uint64_t var_41;
    uint64_t var_42;
    uint64_t var_43;
    uint64_t *var_44;
    uint64_t *var_45;
    uint64_t local_sp_2;
    uint32_t *var_23;
    uint32_t var_26;
    uint32_t var_24;
    uint64_t var_28;
    uint64_t var_29;
    uint32_t *var_30;
    uint32_t *var_31;
    uint64_t r9_1;
    uint64_t var_32;
    struct indirect_placeholder_46_ret_type var_33;
    uint64_t var_34;
    uint64_t var_35;
    struct indirect_placeholder_45_ret_type var_36;
    uint64_t var_37;
    uint64_t var_38;
    uint64_t var_46;
    uint32_t var_25;
    uint64_t var_47;
    uint64_t rcx3_2;
    uint64_t r9_2;
    uint64_t r8_2;
    struct bb_validate_case_classes_ret_type mrv;
    struct bb_validate_case_classes_ret_type mrv1;
    struct bb_validate_case_classes_ret_type mrv2;
    uint32_t var_27;
    var_0 = revng_init_local_sp(0UL);
    var_1 = init_rbp();
    var_2 = init_r9();
    var_3 = init_r8();
    *(uint64_t *)(var_0 + (-8L)) = var_1;
    var_4 = var_0 + (-104L);
    var_5 = (uint64_t *)(var_0 + (-96L));
    *var_5 = rdi;
    var_6 = (uint64_t *)var_4;
    *var_6 = rsi;
    var_7 = var_0 + (-16L);
    var_8 = (uint64_t *)var_7;
    *var_8 = 0UL;
    var_9 = var_0 + (-24L);
    var_10 = (uint64_t *)var_9;
    *var_10 = 0UL;
    var_11 = (uint32_t *)(var_0 + (-28L));
    *var_11 = 0U;
    var_12 = (uint32_t *)(var_0 + (-32L));
    *var_12 = 0U;
    var_13 = *(uint64_t *)(*var_5 + 24UL);
    var_14 = (uint64_t *)(var_0 + (-48L));
    *var_14 = var_13;
    var_15 = *(uint64_t *)(*var_6 + 24UL);
    var_16 = (uint64_t *)(var_0 + (-56L));
    *var_16 = var_15;
    var_17 = *(uint64_t *)(*var_5 + 8UL);
    var_18 = (uint64_t *)(var_0 + (-64L));
    *var_18 = var_17;
    var_19 = *(uint64_t *)(*var_6 + 8UL);
    var_20 = (uint64_t *)(var_0 + (-72L));
    *var_20 = var_19;
    var_21 = (unsigned char *)(var_0 + (-33L));
    *var_21 = (unsigned char)'\x01';
    var_22 = (unsigned char *)(var_0 + (-34L));
    *var_22 = (unsigned char)'\x01';
    rcx3_3 = rcx;
    r8_1 = var_3;
    rcx3_1 = rcx;
    local_sp_2 = var_4;
    var_24 = 0U;
    r9_1 = var_2;
    rcx3_2 = 4283936UL;
    r9_2 = var_2;
    r8_2 = var_3;
    if (*(unsigned char *)(*var_6 + 49UL) == '\x01') {
        mrv.field_0 = rcx3_3;
        mrv1 = mrv;
        mrv1.field_1 = r9_2;
        mrv2 = mrv1;
        mrv2.field_2 = r8_2;
        return mrv2;
    }
    var_23 = (uint32_t *)(var_0 + (-40L));
    *var_23 = 0U;
    var_25 = var_24;
    while ((int)var_24 <= (int)255U)
        {
            if ((var_24 + (-65)) > 25U) {
                *var_8 = (*var_8 + 1UL);
                var_25 = *var_23;
            }
            var_26 = var_25;
            if ((var_25 + (-97)) > 25U) {
                *var_10 = (*var_10 + 1UL);
                var_26 = *var_23;
            }
            var_27 = var_26 + 1U;
            *var_23 = var_27;
            var_24 = var_27;
            var_25 = var_24;
        }
    *(uint64_t *)(*var_5 + 16UL) = 18446744073709551614UL;
    *(uint64_t *)(*var_6 + 16UL) = 18446744073709551614UL;
    var_28 = var_0 + (-76L);
    var_29 = var_0 + (-80L);
    var_30 = (uint32_t *)var_29;
    var_31 = (uint32_t *)var_28;
    r9_0 = r9_1;
    r8_0 = r8_1;
    r9_2 = r9_1;
    r8_2 = r8_1;
    while (*var_11 != 4294967295U)
        {
            if (*var_12 == 4294967295U) {
                break;
            }
            var_32 = *var_5;
            *(uint64_t *)(local_sp_2 + (-8L)) = 4208605UL;
            var_33 = indirect_placeholder_46(var_32, var_28);
            *var_11 = (uint32_t)var_33.field_0;
            var_34 = *var_6;
            var_35 = local_sp_2 + (-16L);
            *(uint64_t *)var_35 = 4208627UL;
            var_36 = indirect_placeholder_45(var_34, var_29);
            var_37 = var_36.field_0;
            var_38 = var_36.field_1;
            *var_12 = (uint32_t)var_37;
            local_sp_0 = var_35;
            rcx3_0 = var_38;
            if (*var_22 != '\x00' & *var_30 != 2U) {
                if (*var_21 == '\x01') {
                    var_39 = local_sp_2 + (-24L);
                    *(uint64_t *)var_39 = 4208688UL;
                    var_40 = indirect_placeholder_44(0UL, 4282456UL, var_38, 1UL, 0UL, r9_1, r8_1);
                    local_sp_0 = var_39;
                    rcx3_0 = var_40.field_0;
                    r9_0 = var_40.field_1;
                    r8_0 = var_40.field_2;
                } else {
                    if (*var_31 == 2U) {
                        var_39 = local_sp_2 + (-24L);
                        *(uint64_t *)var_39 = 4208688UL;
                        var_40 = indirect_placeholder_44(0UL, 4282456UL, var_38, 1UL, 0UL, r9_1, r8_1);
                        local_sp_0 = var_39;
                        rcx3_0 = var_40.field_0;
                        r9_0 = var_40.field_1;
                        r8_0 = var_40.field_2;
                    }
                }
            }
            local_sp_1 = local_sp_0;
            rcx3_1 = rcx3_0;
            r9_1 = r9_0;
            r8_1 = r8_0;
            if (*var_30 == 2U) {
                var_41 = *var_5;
                *(uint64_t *)(local_sp_0 + (-8L)) = 4208708UL;
                indirect_placeholder_3(var_41);
                var_42 = *var_6;
                var_43 = local_sp_0 + (-16L);
                *(uint64_t *)var_43 = 4208720UL;
                indirect_placeholder_3(var_42);
                var_44 = (uint64_t *)(*var_5 + 24UL);
                *var_44 = (*var_44 - (*(uint64_t *)((*var_31 == 1U) ? var_7 : var_9) + (-1L)));
                var_45 = (uint64_t *)(*var_6 + 24UL);
                *var_45 = (*var_45 - (*(uint64_t *)((*var_30 == 1U) ? var_7 : var_9) + (-1L)));
                local_sp_1 = var_43;
            }
            *var_21 = (*(uint64_t *)(*var_5 + 16UL) == 18446744073709551615UL);
            *var_22 = (*(uint64_t *)(*var_6 + 16UL) == 18446744073709551615UL);
            local_sp_2 = local_sp_1;
            r9_0 = r9_1;
            r8_0 = r8_1;
            r9_2 = r9_1;
            r8_2 = r8_1;
        }
    var_46 = *var_5;
    var_47 = var_46;
    if (*(uint64_t *)(var_46 + 24UL) > *var_14) {
        *(uint64_t *)(local_sp_2 + (-8L)) = 4208915UL;
        indirect_placeholder_1();
        var_47 = *var_5;
    } else {
        rcx3_2 = rcx3_1;
        if (*(uint64_t *)(*var_6 + 24UL) > *var_16) {
            *(uint64_t *)(local_sp_2 + (-8L)) = 4208915UL;
            indirect_placeholder_1();
            var_47 = *var_5;
        }
    }
    *(uint64_t *)(var_47 + 8UL) = *var_18;
    *(uint64_t *)(*var_6 + 8UL) = *var_20;
    rcx3_3 = rcx3_2;
    mrv.field_0 = rcx3_3;
    mrv1 = mrv;
    mrv1.field_1 = r9_2;
    mrv2 = mrv1;
    mrv2.field_2 = r8_2;
    return mrv2;
}

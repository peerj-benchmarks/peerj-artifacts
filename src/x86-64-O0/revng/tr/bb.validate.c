typedef unsigned char __u_char;
typedef unsigned short __u_short;
typedef unsigned int __u_int;
typedef unsigned long __u_long;
typedef signed char __int8_t;
typedef unsigned char __uint8_t;
typedef short __int16_t;
typedef unsigned short __uint16_t;
typedef int __int32_t;
typedef unsigned int __uint32_t;
typedef long __int64_t;
typedef unsigned long __uint64_t;
typedef __int8_t __int_least8_t;
typedef __uint8_t __uint_least8_t;
typedef __int16_t __int_least16_t;
typedef __uint16_t __uint_least16_t;
typedef __int32_t __int_least32_t;
typedef __uint32_t __uint_least32_t;
typedef __int64_t __int_least64_t;
typedef __uint64_t __uint_least64_t;
typedef long __quad_t;
typedef unsigned long __u_quad_t;
typedef long __intmax_t;
typedef unsigned long __uintmax_t;
typedef unsigned long __dev_t;
typedef unsigned int __uid_t;
typedef unsigned int __gid_t;
typedef unsigned long __ino_t;
typedef unsigned long __ino64_t;
typedef unsigned int __mode_t;
typedef unsigned long __nlink_t;
typedef long __off_t;
typedef long __off64_t;
typedef int __pid_t;
typedef struct {
    int __val[2];
} __fsid_t;
typedef long __clock_t;
typedef unsigned long __rlim_t;
typedef unsigned long __rlim64_t;
typedef unsigned int __id_t;
typedef long __time_t;
typedef unsigned int __useconds_t;
typedef long __suseconds_t;
typedef int __daddr_t;
typedef int __key_t;
typedef int __clockid_t;
typedef void *__timer_t;
typedef long __blksize_t;
typedef long __blkcnt_t;
typedef long __blkcnt64_t;
typedef unsigned long __fsblkcnt_t;
typedef unsigned long __fsblkcnt64_t;
typedef unsigned long __fsfilcnt_t;
typedef unsigned long __fsfilcnt64_t;
typedef long __fsword_t;
typedef long __ssize_t;
typedef long __syscall_slong_t;
typedef unsigned long __syscall_ulong_t;
typedef __off64_t __loff_t;
typedef char *__caddr_t;
typedef long __intptr_t;
typedef unsigned int __socklen_t;
typedef int __sig_atomic_t;
typedef __int8_t int8_t;
typedef __int16_t int16_t;
typedef __int32_t int32_t;
typedef __int64_t int64_t;
typedef __uint8_t uint8_t;
typedef __uint16_t uint16_t;
typedef __uint32_t uint32_t;
typedef __uint64_t uint64_t;
typedef __int_least8_t int_least8_t;
typedef __int_least16_t int_least16_t;
typedef __int_least32_t int_least32_t;
typedef __int_least64_t int_least64_t;
typedef __uint_least8_t uint_least8_t;
typedef __uint_least16_t uint_least16_t;
typedef __uint_least32_t uint_least32_t;
typedef __uint_least64_t uint_least64_t;
typedef signed char int_fast8_t;
typedef long int_fast16_t;
typedef long int_fast32_t;
typedef long int_fast64_t;
typedef unsigned char uint_fast8_t;
typedef unsigned long uint_fast16_t;
typedef unsigned long uint_fast32_t;
typedef unsigned long uint_fast64_t;
typedef long intptr_t;
typedef unsigned long uintptr_t;
typedef __intmax_t intmax_t;
typedef __uintmax_t uintmax_t;
struct indirect_placeholder_32_ret_type;
struct indirect_placeholder_25_ret_type;
struct indirect_placeholder_26_ret_type;
struct indirect_placeholder_27_ret_type;
struct indirect_placeholder_28_ret_type;
struct indirect_placeholder_29_ret_type;
struct indirect_placeholder_30_ret_type;
struct indirect_placeholder_31_ret_type;
struct indirect_placeholder_33_ret_type;
struct indirect_placeholder_34_ret_type;
struct indirect_placeholder_35_ret_type;
struct indirect_placeholder_36_ret_type;
struct indirect_placeholder_32_ret_type {
    uint64_t field_0;
    uint64_t field_1;
    uint64_t field_2;
};
struct indirect_placeholder_25_ret_type {
    uint64_t field_0;
    uint64_t field_1;
    uint64_t field_2;
};
struct indirect_placeholder_26_ret_type {
    uint64_t field_0;
    uint64_t field_1;
    uint64_t field_2;
};
struct indirect_placeholder_27_ret_type {
    uint64_t field_0;
    uint64_t field_1;
    uint64_t field_2;
};
struct indirect_placeholder_28_ret_type {
    uint64_t field_0;
    uint64_t field_1;
};
struct indirect_placeholder_29_ret_type {
    uint64_t field_0;
    uint64_t field_1;
    uint64_t field_2;
};
struct indirect_placeholder_30_ret_type {
    uint64_t field_0;
    uint64_t field_1;
    uint64_t field_2;
};
struct indirect_placeholder_31_ret_type {
    uint64_t field_0;
    uint64_t field_1;
    uint64_t field_2;
};
struct indirect_placeholder_33_ret_type {
    uint64_t field_0;
    uint64_t field_1;
    uint64_t field_2;
};
struct indirect_placeholder_34_ret_type {
    uint64_t field_0;
    uint64_t field_1;
    uint64_t field_2;
};
struct indirect_placeholder_35_ret_type {
    uint64_t field_0;
    uint64_t field_1;
    uint64_t field_2;
};
struct indirect_placeholder_36_ret_type {
    uint64_t field_0;
    uint64_t field_1;
    uint64_t field_2;
};
extern uint64_t revng_init_local_sp(uint64_t param_0);
extern uint64_t init_rbp(void);
extern struct indirect_placeholder_32_ret_type indirect_placeholder_32(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint64_t param_3, uint64_t param_4, uint64_t param_5, uint64_t param_6);
extern struct indirect_placeholder_25_ret_type indirect_placeholder_25(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint64_t param_3);
extern struct indirect_placeholder_26_ret_type indirect_placeholder_26(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint64_t param_3, uint64_t param_4, uint64_t param_5, uint64_t param_6);
extern struct indirect_placeholder_27_ret_type indirect_placeholder_27(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint64_t param_3, uint64_t param_4);
extern struct indirect_placeholder_28_ret_type indirect_placeholder_28(void);
extern struct indirect_placeholder_29_ret_type indirect_placeholder_29(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint64_t param_3, uint64_t param_4, uint64_t param_5, uint64_t param_6);
extern struct indirect_placeholder_30_ret_type indirect_placeholder_30(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint64_t param_3, uint64_t param_4, uint64_t param_5, uint64_t param_6);
extern struct indirect_placeholder_31_ret_type indirect_placeholder_31(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint64_t param_3, uint64_t param_4, uint64_t param_5, uint64_t param_6);
extern struct indirect_placeholder_33_ret_type indirect_placeholder_33(uint64_t param_0, uint64_t param_1, uint64_t param_2);
extern struct indirect_placeholder_34_ret_type indirect_placeholder_34(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint64_t param_3, uint64_t param_4, uint64_t param_5, uint64_t param_6);
extern struct indirect_placeholder_35_ret_type indirect_placeholder_35(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint64_t param_3, uint64_t param_4, uint64_t param_5, uint64_t param_6);
extern struct indirect_placeholder_36_ret_type indirect_placeholder_36(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint64_t param_3, uint64_t param_4);
void bb_validate(uint64_t rcx, uint64_t rdi, uint64_t rsi, uint64_t r9, uint64_t r8) {
    uint64_t var_25;
    struct indirect_placeholder_32_ret_type var_26;
    struct indirect_placeholder_36_ret_type var_15;
    struct indirect_placeholder_27_ret_type var_43;
    uint64_t var_40;
    uint64_t var_0;
    uint64_t var_1;
    uint64_t *var_2;
    uint64_t *var_3;
    uint64_t var_4;
    uint64_t var_5;
    struct indirect_placeholder_25_ret_type var_6;
    uint64_t var_7;
    uint64_t var_8;
    uint64_t var_9;
    struct indirect_placeholder_29_ret_type var_48;
    uint64_t local_sp_7;
    uint64_t rcx1_5;
    uint64_t var_38;
    struct indirect_placeholder_26_ret_type var_39;
    uint64_t var_41;
    uint64_t local_sp_0;
    uint64_t rcx1_0;
    uint64_t r94_0;
    uint64_t r85_0;
    uint64_t var_42;
    uint64_t var_27;
    uint64_t local_sp_1;
    uint64_t rcx1_1;
    uint64_t r94_1;
    uint64_t r85_1;
    uint64_t local_sp_5;
    uint64_t var_28;
    uint64_t var_44;
    uint64_t local_sp_2;
    uint64_t var_45;
    struct indirect_placeholder_28_ret_type var_46;
    uint64_t rcx1_2;
    uint64_t var_47;
    uint64_t local_sp_6;
    uint64_t rcx1_7;
    uint64_t var_19;
    struct indirect_placeholder_30_ret_type var_20;
    uint64_t local_sp_3;
    uint64_t rcx1_3;
    uint64_t r94_2;
    uint64_t r85_2;
    uint64_t var_21;
    uint64_t var_24;
    uint64_t var_22;
    struct indirect_placeholder_31_ret_type var_23;
    uint64_t local_sp_4;
    uint64_t rcx1_4;
    uint64_t r94_3;
    uint64_t r85_3;
    uint64_t var_29;
    struct indirect_placeholder_33_ret_type var_30;
    uint64_t var_31;
    uint64_t var_32;
    uint64_t var_33;
    uint64_t var_34;
    uint64_t var_35;
    uint64_t var_36;
    uint64_t var_37;
    uint64_t rcx1_6;
    uint64_t r94_4;
    uint64_t r85_4;
    uint64_t var_10;
    struct indirect_placeholder_35_ret_type var_11;
    uint64_t r94_5;
    uint64_t r85_5;
    uint64_t var_12;
    uint64_t var_13;
    uint64_t var_14;
    uint64_t var_16;
    uint64_t var_17;
    uint64_t var_18;
    var_0 = revng_init_local_sp(0UL);
    var_1 = init_rbp();
    *(uint64_t *)(var_0 + (-8L)) = var_1;
    var_2 = (uint64_t *)(var_0 + (-16L));
    *var_2 = rdi;
    var_3 = (uint64_t *)(var_0 + (-24L));
    *var_3 = rsi;
    var_4 = *var_2;
    var_5 = var_0 + (-32L);
    *(uint64_t *)var_5 = 4210258UL;
    var_6 = indirect_placeholder_25(rcx, var_4, r9, r8);
    var_7 = var_6.field_0;
    var_8 = var_6.field_1;
    var_9 = var_6.field_2;
    local_sp_7 = var_5;
    rcx1_7 = var_7;
    r94_5 = var_8;
    r85_5 = var_9;
    if (*(uint64_t *)(*var_2 + 32UL) == 0UL) {
        var_10 = var_0 + (-40L);
        *(uint64_t *)var_10 = 4210296UL;
        var_11 = indirect_placeholder_35(0UL, 4282888UL, var_7, 1UL, 0UL, var_8, var_9);
        local_sp_7 = var_10;
        rcx1_7 = var_11.field_0;
        r94_5 = var_11.field_1;
        r85_5 = var_11.field_2;
    }
    var_12 = *var_3;
    if (var_12 == 0UL) {
        return;
    }
    var_13 = *(uint64_t *)(*var_2 + 24UL);
    var_14 = local_sp_7 + (-8L);
    *(uint64_t *)var_14 = 4210330UL;
    var_15 = indirect_placeholder_36(rcx1_7, var_12, var_13, r94_5, r85_5);
    var_16 = var_15.field_0;
    var_17 = var_15.field_1;
    var_18 = var_15.field_2;
    local_sp_3 = var_14;
    rcx1_3 = var_16;
    r94_2 = var_17;
    r85_2 = var_18;
    if (*(uint64_t *)(*var_3 + 32UL) > 1UL) {
        var_19 = local_sp_7 + (-16L);
        *(uint64_t *)var_19 = 4210369UL;
        var_20 = indirect_placeholder_30(0UL, 4282944UL, var_16, 1UL, 0UL, var_17, var_18);
        local_sp_3 = var_19;
        rcx1_3 = var_20.field_0;
        r94_2 = var_20.field_1;
        r85_2 = var_20.field_2;
    }
    local_sp_6 = local_sp_3;
    local_sp_4 = local_sp_3;
    rcx1_4 = rcx1_3;
    r94_3 = r94_2;
    r85_3 = r85_2;
    rcx1_6 = rcx1_3;
    r94_4 = r94_2;
    r85_4 = r85_2;
    if (*(unsigned char *)6395332UL == '\x00') {
        if (*(uint64_t *)(*var_3 + 32UL) == 0UL) {
            *(uint64_t *)(local_sp_6 + (-8L)) = 4210695UL;
            indirect_placeholder_34(0UL, 4283328UL, rcx1_6, 1UL, 0UL, r94_4, r85_4);
        }
    } else {
        var_21 = *var_3;
        var_24 = var_21;
        if (*(unsigned char *)(var_21 + 48UL) == '\x00') {
            var_22 = local_sp_3 + (-8L);
            *(uint64_t *)var_22 = 4210421UL;
            var_23 = indirect_placeholder_31(0UL, 4283000UL, rcx1_3, 1UL, 0UL, r94_2, r85_2);
            var_24 = *var_3;
            local_sp_4 = var_22;
            rcx1_4 = var_23.field_0;
            r94_3 = var_23.field_1;
            r85_3 = var_23.field_2;
        }
        var_27 = var_24;
        local_sp_5 = local_sp_4;
        rcx1_5 = rcx1_4;
        if (*(unsigned char *)(var_24 + 50UL) == '\x00') {
            var_25 = local_sp_4 + (-8L);
            *(uint64_t *)var_25 = 4210458UL;
            var_26 = indirect_placeholder_32(0UL, 4283064UL, rcx1_4, 1UL, 0UL, r94_3, r85_3);
            var_27 = *var_3;
            local_sp_5 = var_25;
            rcx1_5 = var_26.field_0;
        }
        var_28 = *var_2;
        var_29 = local_sp_5 + (-8L);
        *(uint64_t *)var_29 = 4210477UL;
        var_30 = indirect_placeholder_33(rcx1_5, var_28, var_27);
        var_31 = var_30.field_0;
        var_32 = var_30.field_1;
        var_33 = var_30.field_2;
        var_34 = *var_2;
        var_35 = *(uint64_t *)(var_34 + 24UL);
        var_36 = *var_3;
        var_37 = *(uint64_t *)(var_36 + 24UL);
        var_40 = var_34;
        var_41 = var_36;
        local_sp_0 = var_29;
        rcx1_0 = var_31;
        r94_0 = var_32;
        r85_0 = var_33;
        local_sp_1 = var_29;
        rcx1_1 = var_31;
        r94_1 = var_32;
        r85_1 = var_33;
        if (var_35 <= var_37 & *(unsigned char *)6395331UL != '\x01') {
            if (var_37 == 0UL) {
                var_38 = local_sp_5 + (-16L);
                *(uint64_t *)var_38 = 4210550UL;
                var_39 = indirect_placeholder_26(0UL, 4283160UL, var_31, 1UL, 0UL, var_32, var_33);
                var_40 = *var_2;
                var_41 = *var_3;
                local_sp_0 = var_38;
                rcx1_0 = var_39.field_0;
                r94_0 = var_39.field_1;
                r85_0 = var_39.field_2;
            }
            var_42 = local_sp_0 + (-8L);
            *(uint64_t *)var_42 = 4210569UL;
            var_43 = indirect_placeholder_27(rcx1_0, var_40, var_41, r94_0, r85_0);
            local_sp_1 = var_42;
            rcx1_1 = var_43.field_0;
            r94_1 = var_43.field_1;
            r85_1 = var_43.field_2;
        }
        local_sp_2 = local_sp_1;
        rcx1_2 = rcx1_1;
        if (*(unsigned char *)6395330UL == '\x00') {
            return;
        }
        var_44 = *var_2;
        if (*(unsigned char *)(var_44 + 49UL) == '\x00') {
            return;
        }
        if (*(uint64_t *)(*var_3 + 24UL) != *(uint64_t *)(var_44 + 24UL)) {
            var_45 = local_sp_1 + (-8L);
            *(uint64_t *)var_45 = 4210625UL;
            var_46 = indirect_placeholder_28();
            local_sp_2 = var_45;
            rcx1_2 = var_46.field_1;
            if ((uint64_t)(unsigned char)var_46.field_0 != 1UL) {
                return;
            }
        }
        var_47 = local_sp_2 + (-8L);
        *(uint64_t *)var_47 = 4210657UL;
        var_48 = indirect_placeholder_29(0UL, 4283216UL, rcx1_2, 1UL, 0UL, r94_1, r85_1);
        local_sp_6 = var_47;
        rcx1_6 = var_48.field_0;
        r94_4 = var_48.field_1;
        r85_4 = var_48.field_2;
    }
}

typedef unsigned char __u_char;
typedef unsigned short __u_short;
typedef unsigned int __u_int;
typedef unsigned long __u_long;
typedef signed char __int8_t;
typedef unsigned char __uint8_t;
typedef short __int16_t;
typedef unsigned short __uint16_t;
typedef int __int32_t;
typedef unsigned int __uint32_t;
typedef long __int64_t;
typedef unsigned long __uint64_t;
typedef __int8_t __int_least8_t;
typedef __uint8_t __uint_least8_t;
typedef __int16_t __int_least16_t;
typedef __uint16_t __uint_least16_t;
typedef __int32_t __int_least32_t;
typedef __uint32_t __uint_least32_t;
typedef __int64_t __int_least64_t;
typedef __uint64_t __uint_least64_t;
typedef long __quad_t;
typedef unsigned long __u_quad_t;
typedef long __intmax_t;
typedef unsigned long __uintmax_t;
typedef unsigned long __dev_t;
typedef unsigned int __uid_t;
typedef unsigned int __gid_t;
typedef unsigned long __ino_t;
typedef unsigned long __ino64_t;
typedef unsigned int __mode_t;
typedef unsigned long __nlink_t;
typedef long __off_t;
typedef long __off64_t;
typedef int __pid_t;
typedef struct {
    int __val[2];
} __fsid_t;
typedef long __clock_t;
typedef unsigned long __rlim_t;
typedef unsigned long __rlim64_t;
typedef unsigned int __id_t;
typedef long __time_t;
typedef unsigned int __useconds_t;
typedef long __suseconds_t;
typedef int __daddr_t;
typedef int __key_t;
typedef int __clockid_t;
typedef void *__timer_t;
typedef long __blksize_t;
typedef long __blkcnt_t;
typedef long __blkcnt64_t;
typedef unsigned long __fsblkcnt_t;
typedef unsigned long __fsblkcnt64_t;
typedef unsigned long __fsfilcnt_t;
typedef unsigned long __fsfilcnt64_t;
typedef long __fsword_t;
typedef long __ssize_t;
typedef long __syscall_slong_t;
typedef unsigned long __syscall_ulong_t;
typedef __off64_t __loff_t;
typedef char *__caddr_t;
typedef long __intptr_t;
typedef unsigned int __socklen_t;
typedef int __sig_atomic_t;
typedef __int8_t int8_t;
typedef __int16_t int16_t;
typedef __int32_t int32_t;
typedef __int64_t int64_t;
typedef __uint8_t uint8_t;
typedef __uint16_t uint16_t;
typedef __uint32_t uint32_t;
typedef __uint64_t uint64_t;
typedef __int_least8_t int_least8_t;
typedef __int_least16_t int_least16_t;
typedef __int_least32_t int_least32_t;
typedef __int_least64_t int_least64_t;
typedef __uint_least8_t uint_least8_t;
typedef __uint_least16_t uint_least16_t;
typedef __uint_least32_t uint_least32_t;
typedef __uint_least64_t uint_least64_t;
typedef signed char int_fast8_t;
typedef long int_fast16_t;
typedef long int_fast32_t;
typedef long int_fast64_t;
typedef unsigned char uint_fast8_t;
typedef unsigned long uint_fast16_t;
typedef unsigned long uint_fast32_t;
typedef unsigned long uint_fast64_t;
typedef long intptr_t;
typedef unsigned long uintptr_t;
typedef __intmax_t intmax_t;
typedef __uintmax_t uintmax_t;
struct indirect_placeholder_82_ret_type;
struct indirect_placeholder_81_ret_type;
struct indirect_placeholder_79_ret_type;
struct indirect_placeholder_80_ret_type;
struct indirect_placeholder_82_ret_type {
    uint64_t field_0;
    uint64_t field_1;
    uint64_t field_2;
    uint64_t field_3;
    uint64_t field_4;
};
struct indirect_placeholder_81_ret_type {
    uint64_t field_0;
    uint64_t field_1;
    uint64_t field_2;
    uint64_t field_3;
};
struct indirect_placeholder_79_ret_type {
    uint64_t field_0;
    uint64_t field_1;
    uint64_t field_2;
};
struct indirect_placeholder_80_ret_type {
    uint64_t field_0;
    uint64_t field_1;
    uint64_t field_2;
};
extern uint64_t revng_init_local_sp(uint64_t param_0);
extern uint64_t init_rbp(void);
extern void indirect_placeholder_1(void);
extern uint64_t init_rbx(void);
extern struct indirect_placeholder_82_ret_type indirect_placeholder_82(uint64_t param_0);
extern struct indirect_placeholder_81_ret_type indirect_placeholder_81(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint64_t param_3, uint64_t param_4, uint64_t param_5, uint64_t param_6);
extern struct indirect_placeholder_79_ret_type indirect_placeholder_79(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint64_t param_3, uint64_t param_4, uint64_t param_5, uint64_t param_6);
extern struct indirect_placeholder_80_ret_type indirect_placeholder_80(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint64_t param_3, uint64_t param_4, uint64_t param_5, uint64_t param_6);
uint64_t bb_unquote(uint64_t rdi, uint64_t rsi) {
    struct indirect_placeholder_79_ret_type var_46;
    uint64_t var_0;
    uint64_t var_1;
    uint64_t var_2;
    uint64_t *var_3;
    uint64_t var_4;
    uint64_t *var_5;
    uint64_t var_6;
    uint64_t *var_7;
    struct indirect_placeholder_82_ret_type var_8;
    uint64_t var_9;
    uint64_t var_10;
    uint64_t var_11;
    uint64_t var_12;
    uint64_t var_13;
    uint64_t **var_14;
    uint64_t var_15;
    uint64_t var_16;
    struct indirect_placeholder_81_ret_type var_17;
    uint64_t var_18;
    uint64_t var_19;
    uint64_t var_20;
    uint32_t *var_21;
    uint32_t *var_22;
    unsigned char *var_23;
    uint32_t *var_24;
    uint64_t rcx_1;
    uint32_t var_25;
    uint64_t local_sp_2;
    uint64_t rcx_0;
    uint64_t r9_2;
    uint64_t local_sp_0;
    uint64_t r8_2;
    uint64_t r9_0;
    uint64_t r8_0;
    unsigned char var_26;
    unsigned char var_32;
    uint32_t var_33;
    uint64_t r9_1;
    uint64_t var_45;
    uint64_t var_47;
    uint64_t var_48;
    uint64_t local_sp_1;
    uint32_t var_34;
    uint32_t var_35;
    uint32_t var_36;
    unsigned char var_37;
    uint32_t var_38;
    uint64_t var_39;
    uint64_t var_40;
    uint64_t var_41;
    uint64_t var_42;
    uint64_t var_43;
    struct indirect_placeholder_80_ret_type var_44;
    uint64_t r8_1;
    uint64_t var_49;
    uint32_t var_50;
    uint64_t var_51;
    uint64_t var_27;
    uint32_t var_28;
    uint64_t var_29;
    uint64_t var_30;
    uint64_t var_31;
    uint32_t var_52;
    var_0 = revng_init_local_sp(0UL);
    var_1 = init_rbp();
    var_2 = init_rbx();
    *(uint64_t *)(var_0 + (-8L)) = var_1;
    var_3 = (uint64_t *)(var_0 + (-48L));
    *var_3 = rdi;
    var_4 = var_0 + (-56L);
    var_5 = (uint64_t *)var_4;
    *var_5 = rsi;
    var_6 = *var_3;
    *(uint64_t *)(var_0 + (-64L)) = 4203178UL;
    indirect_placeholder_1();
    var_7 = (uint64_t *)(var_0 + (-32L));
    *var_7 = var_6;
    *(uint64_t *)(var_0 + (-72L)) = 4203194UL;
    var_8 = indirect_placeholder_82(var_6);
    var_9 = var_8.field_0;
    var_10 = var_8.field_1;
    var_11 = var_8.field_2;
    var_12 = var_8.field_3;
    var_13 = var_8.field_4;
    var_14 = (uint64_t **)var_4;
    **var_14 = var_9;
    var_15 = *var_7;
    var_16 = var_0 + (-80L);
    *(uint64_t *)var_16 = 4203221UL;
    var_17 = indirect_placeholder_81(var_10, var_2, var_15, 1UL, var_11, var_12, var_13);
    var_18 = var_17.field_1;
    var_19 = var_17.field_2;
    var_20 = var_17.field_3;
    *(uint64_t *)(*var_5 + 8UL) = var_17.field_0;
    var_21 = (uint32_t *)(var_0 + (-12L));
    *var_21 = 0U;
    var_22 = (uint32_t *)(var_0 + (-16L));
    *var_22 = 0U;
    var_23 = (unsigned char *)(var_0 + (-17L));
    var_24 = (uint32_t *)(var_0 + (-36L));
    var_25 = 0U;
    rcx_0 = var_18;
    local_sp_0 = var_16;
    r9_0 = var_19;
    r8_0 = var_20;
    var_26 = *(unsigned char *)(*var_3 + (uint64_t)var_25);
    local_sp_2 = local_sp_0;
    r9_2 = r9_0;
    r8_2 = r8_0;
    r9_1 = r9_0;
    local_sp_1 = local_sp_0;
    r8_1 = r8_0;
    while (var_26 != '\x00')
        {
            if ((uint64_t)((uint32_t)(uint64_t)var_26 + (-92)) == 0UL) {
                var_27 = **var_14;
                var_28 = *var_21;
                var_29 = (uint64_t)var_28;
                *var_21 = (var_28 + 1U);
                var_30 = var_27 + var_29;
                var_31 = (uint64_t)*var_22;
                *(unsigned char *)var_30 = *(unsigned char *)(*var_3 + var_31);
                rcx_1 = var_31;
            } else {
                *(unsigned char *)(*(uint64_t *)(*var_5 + 8UL) + (uint64_t)*var_21) = (unsigned char)'\x01';
                var_32 = *(unsigned char *)(*var_3 + (uint64_t)(*var_22 + 1U));
                var_33 = (uint32_t)(uint64_t)var_32;
                if ((uint64_t)(var_33 + (-98)) == 0UL) {
                    *var_23 = (unsigned char)'\b';
                } else {
                    if ((signed char)var_32 > 'b') {
                        if ((uint64_t)(var_33 + (-114)) == 0UL) {
                            *var_23 = (unsigned char)'\r';
                        } else {
                            if ((signed char)var_32 > 'r') {
                                if ((uint64_t)(var_33 + (-116)) == 0UL) {
                                    *var_23 = (unsigned char)'\t';
                                } else {
                                    if ((uint64_t)(var_33 + (-118)) == 0UL) {
                                        *var_23 = (unsigned char)'\v';
                                    } else {
                                        *var_23 = var_32;
                                    }
                                }
                            } else {
                                if ((uint64_t)(var_33 + (-102)) == 0UL) {
                                    *var_23 = (unsigned char)'\f';
                                } else {
                                    if ((uint64_t)(var_33 + (-110)) == 0UL) {
                                        *var_23 = (unsigned char)'\n';
                                    } else {
                                        *var_23 = var_32;
                                    }
                                }
                            }
                        }
                    } else {
                        if ((signed char)var_32 > '7') {
                            if ((uint64_t)(var_33 + (-92)) == 0UL) {
                                *var_23 = (unsigned char)'\\';
                            } else {
                                if ((uint64_t)(var_33 + (-97)) == 0UL) {
                                    *var_23 = (unsigned char)'\a';
                                } else {
                                    *var_23 = var_32;
                                }
                            }
                        } else {
                            if ((signed char)var_32 < '0') {
                                if ((uint64_t)var_33 == 0UL) {
                                    var_45 = local_sp_0 + (-8L);
                                    *(uint64_t *)var_45 = 4203861UL;
                                    var_46 = indirect_placeholder_79(0UL, 4281912UL, rcx_0, 0UL, 0UL, r9_0, r8_0);
                                    var_47 = var_46.field_1;
                                    var_48 = var_46.field_2;
                                    *(unsigned char *)(*(uint64_t *)(*var_5 + 8UL) + (uint64_t)*var_21) = (unsigned char)'\x00';
                                    *var_22 = (*var_22 + (-1));
                                    *var_23 = (unsigned char)'\\';
                                    local_sp_1 = var_45;
                                    r9_1 = var_47;
                                    r8_1 = var_48;
                                } else {
                                    *var_23 = var_32;
                                }
                            } else {
                                *var_23 = (var_32 + '\xd0');
                                var_34 = (uint32_t)*(unsigned char *)(*var_3 + (uint64_t)(*var_22 + 2U)) + (-48);
                                *var_24 = var_34;
                                *var_23 = ((*var_23 << '\x03') + (unsigned char)var_34);
                                var_35 = *var_22 + 1U;
                                *var_22 = var_35;
                                var_36 = (uint32_t)*(unsigned char *)(*var_3 + (uint64_t)(var_35 + 2U)) + (-48);
                                *var_24 = var_36;
                                if (!(((int)var_34 < (int)0U) || ((int)var_34 > (int)7U)) && !(((int)var_36 < (int)0U) || ((int)var_36 > (int)7U))) {
                                    var_37 = *var_23;
                                    if ((int)(((uint32_t)var_37 << 3U) + var_36) > (int)255U) {
                                        var_38 = *var_22;
                                        var_39 = (uint64_t)(var_38 + 2U);
                                        var_40 = *var_3;
                                        var_41 = (uint64_t)(uint32_t)(uint64_t)*(unsigned char *)(var_40 + var_39);
                                        var_42 = (uint64_t)(uint32_t)(uint64_t)*(unsigned char *)(var_40 + (uint64_t)(var_38 + 1U));
                                        var_43 = (uint64_t)(uint32_t)(uint64_t)*(unsigned char *)(var_40 + (uint64_t)var_38);
                                        *(uint64_t *)(local_sp_0 + (-16L)) = var_41;
                                        *(uint64_t *)(local_sp_0 + (-24L)) = var_42;
                                        *(uint64_t *)(local_sp_0 + (-32L)) = var_43;
                                        *(uint64_t *)(local_sp_0 + (-40L)) = 4203828UL;
                                        var_44 = indirect_placeholder_80(0UL, 4281808UL, var_43, 0UL, 0UL, var_41, var_42);
                                        local_sp_1 = local_sp_0 + (-8L);
                                        r9_1 = var_44.field_1;
                                        r8_1 = var_44.field_2;
                                    } else {
                                        *var_23 = ((var_37 << '\x03') + (unsigned char)var_36);
                                        *var_22 = (*var_22 + 1U);
                                    }
                                }
                            }
                        }
                    }
                }
                *var_22 = (*var_22 + 1U);
                var_49 = **var_14;
                var_50 = *var_21;
                var_51 = (uint64_t)var_50;
                *var_21 = (var_50 + 1U);
                *(unsigned char *)(var_49 + var_51) = *var_23;
                rcx_1 = var_49;
                local_sp_2 = local_sp_1;
                r9_2 = r9_1;
                r8_2 = r8_1;
            }
            var_52 = *var_22 + 1U;
            *var_22 = var_52;
            var_25 = var_52;
            rcx_0 = rcx_1;
            local_sp_0 = local_sp_2;
            r9_0 = r9_2;
            r8_0 = r8_2;
            var_26 = *(unsigned char *)(*var_3 + (uint64_t)var_25);
            local_sp_2 = local_sp_0;
            r9_2 = r9_0;
            r8_2 = r8_0;
            r9_1 = r9_0;
            local_sp_1 = local_sp_0;
            r8_1 = r8_0;
        }
    *(uint64_t *)(*var_5 + 16UL) = (uint64_t)*var_21;
    return 1UL;
}

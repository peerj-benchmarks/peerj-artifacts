typedef unsigned char __u_char;
typedef unsigned short __u_short;
typedef unsigned int __u_int;
typedef unsigned long __u_long;
typedef signed char __int8_t;
typedef unsigned char __uint8_t;
typedef short __int16_t;
typedef unsigned short __uint16_t;
typedef int __int32_t;
typedef unsigned int __uint32_t;
typedef long __int64_t;
typedef unsigned long __uint64_t;
typedef __int8_t __int_least8_t;
typedef __uint8_t __uint_least8_t;
typedef __int16_t __int_least16_t;
typedef __uint16_t __uint_least16_t;
typedef __int32_t __int_least32_t;
typedef __uint32_t __uint_least32_t;
typedef __int64_t __int_least64_t;
typedef __uint64_t __uint_least64_t;
typedef long __quad_t;
typedef unsigned long __u_quad_t;
typedef long __intmax_t;
typedef unsigned long __uintmax_t;
typedef unsigned long __dev_t;
typedef unsigned int __uid_t;
typedef unsigned int __gid_t;
typedef unsigned long __ino_t;
typedef unsigned long __ino64_t;
typedef unsigned int __mode_t;
typedef unsigned long __nlink_t;
typedef long __off_t;
typedef long __off64_t;
typedef int __pid_t;
typedef struct {
    int __val[2];
} __fsid_t;
typedef long __clock_t;
typedef unsigned long __rlim_t;
typedef unsigned long __rlim64_t;
typedef unsigned int __id_t;
typedef long __time_t;
typedef unsigned int __useconds_t;
typedef long __suseconds_t;
typedef int __daddr_t;
typedef int __key_t;
typedef int __clockid_t;
typedef void *__timer_t;
typedef long __blksize_t;
typedef long __blkcnt_t;
typedef long __blkcnt64_t;
typedef unsigned long __fsblkcnt_t;
typedef unsigned long __fsblkcnt64_t;
typedef unsigned long __fsfilcnt_t;
typedef unsigned long __fsfilcnt64_t;
typedef long __fsword_t;
typedef long __ssize_t;
typedef long __syscall_slong_t;
typedef unsigned long __syscall_ulong_t;
typedef __off64_t __loff_t;
typedef char *__caddr_t;
typedef long __intptr_t;
typedef unsigned int __socklen_t;
typedef int __sig_atomic_t;
typedef __int8_t int8_t;
typedef __int16_t int16_t;
typedef __int32_t int32_t;
typedef __int64_t int64_t;
typedef __uint8_t uint8_t;
typedef __uint16_t uint16_t;
typedef __uint32_t uint32_t;
typedef __uint64_t uint64_t;
typedef __int_least8_t int_least8_t;
typedef __int_least16_t int_least16_t;
typedef __int_least32_t int_least32_t;
typedef __int_least64_t int_least64_t;
typedef __uint_least8_t uint_least8_t;
typedef __uint_least16_t uint_least16_t;
typedef __uint_least32_t uint_least32_t;
typedef __uint_least64_t uint_least64_t;
typedef signed char int_fast8_t;
typedef long int_fast16_t;
typedef long int_fast32_t;
typedef long int_fast64_t;
typedef unsigned char uint_fast8_t;
typedef unsigned long uint_fast16_t;
typedef unsigned long uint_fast32_t;
typedef unsigned long uint_fast64_t;
typedef long intptr_t;
typedef unsigned long uintptr_t;
typedef __intmax_t intmax_t;
typedef __uintmax_t uintmax_t;
struct indirect_placeholder_63_ret_type;
struct indirect_placeholder_62_ret_type;
struct indirect_placeholder_61_ret_type;
struct indirect_placeholder_64_ret_type;
struct indirect_placeholder_63_ret_type {
    uint64_t field_0;
    uint64_t field_1;
    uint64_t field_2;
};
struct indirect_placeholder_62_ret_type {
    uint64_t field_0;
    uint64_t field_1;
    uint64_t field_2;
};
struct indirect_placeholder_61_ret_type {
    uint64_t field_0;
    uint64_t field_1;
    uint64_t field_2;
};
struct indirect_placeholder_64_ret_type {
    uint64_t field_0;
    uint64_t field_1;
    uint64_t field_2;
};
extern uint64_t revng_init_local_sp(uint64_t param_0);
extern uint64_t init_rbp(void);
extern uint64_t init_cc_src2(void);
extern uint64_t helper_cc_compute_c_wrapper(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint32_t param_3);
extern void indirect_placeholder_1(void);
extern uint64_t indirect_placeholder_48(uint64_t param_0, uint64_t param_1, uint64_t param_2);
extern uint64_t init_rbx(void);
extern uint64_t init_r10(void);
extern uint64_t init_r9(void);
extern struct indirect_placeholder_63_ret_type indirect_placeholder_63(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint64_t param_3, uint64_t param_4, uint64_t param_5, uint64_t param_6);
extern struct indirect_placeholder_62_ret_type indirect_placeholder_62(uint64_t param_0);
extern struct indirect_placeholder_61_ret_type indirect_placeholder_61(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint64_t param_3, uint64_t param_4, uint64_t param_5, uint64_t param_6);
extern struct indirect_placeholder_64_ret_type indirect_placeholder_64(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint64_t param_3, uint64_t param_4);
uint64_t bb_find_bracketed_repeat(uint64_t rdx, uint64_t rcx, uint64_t rdi, uint64_t rsi, uint64_t r8) {
    struct indirect_placeholder_64_ret_type var_34;
    uint64_t rax_0;
    uint64_t var_0;
    uint64_t var_1;
    uint64_t var_2;
    uint64_t var_3;
    uint64_t var_4;
    uint64_t var_5;
    uint64_t var_6;
    uint64_t var_7;
    uint64_t *var_8;
    uint64_t *var_9;
    uint64_t var_10;
    uint64_t var_11;
    uint64_t *var_12;
    struct indirect_placeholder_63_ret_type var_39;
    uint64_t var_13;
    uint64_t var_14;
    uint64_t var_15;
    uint64_t var_16;
    uint64_t local_sp_0;
    uint64_t var_37;
    uint64_t var_38;
    uint64_t var_40;
    struct indirect_placeholder_62_ret_type var_41;
    uint64_t var_42;
    uint64_t var_43;
    uint64_t var_44;
    uint64_t var_32;
    uint64_t var_21;
    uint64_t *var_22;
    uint64_t var_23;
    uint64_t var_24;
    uint64_t **var_25;
    uint64_t var_27;
    uint64_t *var_28;
    uint64_t var_29;
    uint64_t var_30;
    uint64_t *var_31;
    uint64_t spec_select;
    uint64_t var_33;
    uint64_t var_35;
    uint64_t var_36;
    uint64_t var_26;
    uint64_t var_17;
    uint64_t var_18;
    uint64_t var_19;
    uint64_t var_20;
    unsigned int loop_state_var;
    var_0 = revng_init_local_sp(0UL);
    var_1 = init_rbp();
    var_2 = init_cc_src2();
    var_3 = init_rbx();
    var_4 = init_r10();
    var_5 = init_r9();
    *(uint64_t *)(var_0 + (-8L)) = var_1;
    var_6 = var_0 + (-104L);
    var_7 = var_0 + (-64L);
    var_8 = (uint64_t *)var_7;
    *var_8 = rdi;
    var_9 = (uint64_t *)(var_0 + (-72L));
    *var_9 = rsi;
    var_10 = var_0 + (-80L);
    *(uint64_t *)var_10 = rdx;
    var_11 = var_0 + (-88L);
    var_12 = (uint64_t *)var_11;
    *var_12 = rcx;
    var_13 = var_0 + (-96L);
    *(uint64_t *)var_13 = r8;
    var_14 = *var_9 + 1UL;
    var_15 = *(uint64_t *)(*var_8 + 16UL);
    var_16 = helper_cc_compute_c_wrapper(var_14 - var_15, var_15, var_2, 17U);
    rax_0 = 4294967295UL;
    local_sp_0 = var_6;
    if (var_16 == 0UL) {
        var_17 = var_0 + (-112L);
        *(uint64_t *)var_17 = 4205708UL;
        indirect_placeholder_1();
        local_sp_0 = var_17;
    }
    var_18 = *var_9 + 1UL;
    var_19 = *var_8;
    *(uint64_t *)(local_sp_0 + (-8L)) = 4205739UL;
    var_20 = indirect_placeholder_48(42UL, var_19, var_18);
    if ((uint64_t)(unsigned char)var_20 == 1UL) {
        return rax_0;
    }
    var_21 = *var_9 + 2UL;
    var_22 = (uint64_t *)(var_0 + (-16L));
    *var_22 = var_21;
    var_23 = var_21;
    while (1U)
        {
            var_24 = *var_8;
            if (*(uint64_t *)(var_24 + 16UL) <= var_23) {
                loop_state_var = 0U;
                break;
            }
            if (*(unsigned char *)(var_23 + *(uint64_t *)(var_24 + 8UL)) != '\x01') {
                loop_state_var = 0U;
                break;
            }
            var_25 = (uint64_t **)var_7;
            rax_0 = 0UL;
            if (*(unsigned char *)(var_23 + **var_25) == ']') {
                var_26 = var_23 + 1UL;
                *var_22 = var_26;
                var_23 = var_26;
                continue;
            }
            var_27 = (var_23 - *var_9) + (-2L);
            var_28 = (uint64_t *)(var_0 + (-24L));
            *var_28 = var_27;
            **(unsigned char **)var_10 = *(unsigned char *)(*var_9 + **var_25);
            if (*var_28 != 0UL) {
                **(uint64_t **)var_11 = 0UL;
                loop_state_var = 1U;
                break;
            }
            var_29 = **var_25 + (*var_9 + 2UL);
            var_30 = var_0 + (-32L);
            var_31 = (uint64_t *)var_30;
            *var_31 = var_29;
            spec_select = (**(unsigned char **)var_30 == '0') ? 8UL : 10UL;
            var_32 = *var_12;
            var_33 = var_0 + (-48L);
            *(uint64_t *)(local_sp_0 + (-16L)) = 4205938UL;
            var_34 = indirect_placeholder_64(spec_select, var_32, var_29, var_33, 0UL);
            var_35 = var_34.field_1;
            var_36 = var_34.field_2;
            rax_0 = 4294967294UL;
            if ((uint64_t)(uint32_t)var_34.field_0 != 0UL) {
                loop_state_var = 2U;
                break;
            }
            if (**(uint64_t **)var_11 != 18446744073709551615UL) {
                loop_state_var = 2U;
                break;
            }
            if ((*var_31 + *var_28) != *(uint64_t *)var_33) {
                loop_state_var = 2U;
                break;
            }
            loop_state_var = 1U;
            break;
        }
    switch (loop_state_var) {
      case 0U:
        {
            break;
        }
        break;
      case 1U:
        {
            **(uint64_t **)var_13 = *var_22;
        }
        break;
      case 2U:
        {
            var_37 = *var_28;
            var_38 = *var_31;
            *(uint64_t *)(local_sp_0 + (-24L)) = 4205994UL;
            var_39 = indirect_placeholder_63(var_35, var_3, var_38, var_37, var_4, var_5, var_36);
            var_40 = var_39.field_0;
            *(uint64_t *)(var_0 + (-40L)) = var_40;
            *(uint64_t *)(local_sp_0 + (-32L)) = 4206010UL;
            var_41 = indirect_placeholder_62(var_40);
            var_42 = var_41.field_0;
            var_43 = var_41.field_1;
            var_44 = var_41.field_2;
            *(uint64_t *)(local_sp_0 + (-40L)) = 4206038UL;
            indirect_placeholder_61(0UL, 4282128UL, var_42, 0UL, 0UL, var_43, var_44);
            *(uint64_t *)(local_sp_0 + (-48L)) = 4206050UL;
            indirect_placeholder_1();
        }
        break;
    }
}

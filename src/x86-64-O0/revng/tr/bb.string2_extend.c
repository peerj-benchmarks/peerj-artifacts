typedef unsigned char __u_char;
typedef unsigned short __u_short;
typedef unsigned int __u_int;
typedef unsigned long __u_long;
typedef signed char __int8_t;
typedef unsigned char __uint8_t;
typedef short __int16_t;
typedef unsigned short __uint16_t;
typedef int __int32_t;
typedef unsigned int __uint32_t;
typedef long __int64_t;
typedef unsigned long __uint64_t;
typedef __int8_t __int_least8_t;
typedef __uint8_t __uint_least8_t;
typedef __int16_t __int_least16_t;
typedef __uint16_t __uint_least16_t;
typedef __int32_t __int_least32_t;
typedef __uint32_t __uint_least32_t;
typedef __int64_t __int_least64_t;
typedef __uint64_t __uint_least64_t;
typedef long __quad_t;
typedef unsigned long __u_quad_t;
typedef long __intmax_t;
typedef unsigned long __uintmax_t;
typedef unsigned long __dev_t;
typedef unsigned int __uid_t;
typedef unsigned int __gid_t;
typedef unsigned long __ino_t;
typedef unsigned long __ino64_t;
typedef unsigned int __mode_t;
typedef unsigned long __nlink_t;
typedef long __off_t;
typedef long __off64_t;
typedef int __pid_t;
typedef struct {
    int __val[2];
} __fsid_t;
typedef long __clock_t;
typedef unsigned long __rlim_t;
typedef unsigned long __rlim64_t;
typedef unsigned int __id_t;
typedef long __time_t;
typedef unsigned int __useconds_t;
typedef long __suseconds_t;
typedef int __daddr_t;
typedef int __key_t;
typedef int __clockid_t;
typedef void *__timer_t;
typedef long __blksize_t;
typedef long __blkcnt_t;
typedef long __blkcnt64_t;
typedef unsigned long __fsblkcnt_t;
typedef unsigned long __fsblkcnt64_t;
typedef unsigned long __fsfilcnt_t;
typedef unsigned long __fsfilcnt64_t;
typedef long __fsword_t;
typedef long __ssize_t;
typedef long __syscall_slong_t;
typedef unsigned long __syscall_ulong_t;
typedef __off64_t __loff_t;
typedef char *__caddr_t;
typedef long __intptr_t;
typedef unsigned int __socklen_t;
typedef int __sig_atomic_t;
typedef __int8_t int8_t;
typedef __int16_t int16_t;
typedef __int32_t int32_t;
typedef __int64_t int64_t;
typedef __uint8_t uint8_t;
typedef __uint16_t uint16_t;
typedef __uint32_t uint32_t;
typedef __uint64_t uint64_t;
typedef __int_least8_t int_least8_t;
typedef __int_least16_t int_least16_t;
typedef __int_least32_t int_least32_t;
typedef __int_least64_t int_least64_t;
typedef __uint_least8_t uint_least8_t;
typedef __uint_least16_t uint_least16_t;
typedef __uint_least32_t uint_least32_t;
typedef __uint_least64_t uint_least64_t;
typedef signed char int_fast8_t;
typedef long int_fast16_t;
typedef long int_fast32_t;
typedef long int_fast64_t;
typedef unsigned char uint_fast8_t;
typedef unsigned long uint_fast16_t;
typedef unsigned long uint_fast32_t;
typedef unsigned long uint_fast64_t;
typedef long intptr_t;
typedef unsigned long uintptr_t;
typedef __intmax_t intmax_t;
typedef __uintmax_t uintmax_t;
struct bb_string2_extend_ret_type;
struct indirect_placeholder_21_ret_type;
struct bb_string2_extend_ret_type {
    uint64_t field_0;
    uint64_t field_1;
    uint64_t field_2;
};
struct indirect_placeholder_21_ret_type {
    uint64_t field_0;
    uint64_t field_1;
    uint64_t field_2;
};
extern void abort(void);
extern uint64_t revng_init_local_sp(uint64_t param_0);
extern uint64_t init_rbp(void);
extern void indirect_placeholder_1(void);
extern void indirect_placeholder_22(uint64_t param_0, uint64_t param_1, uint64_t param_2);
extern struct indirect_placeholder_21_ret_type indirect_placeholder_21(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint64_t param_3, uint64_t param_4, uint64_t param_5, uint64_t param_6);
struct bb_string2_extend_ret_type bb_string2_extend(uint64_t rcx, uint64_t rdi, uint64_t rsi, uint64_t r9, uint64_t r8) {
    struct indirect_placeholder_21_ret_type var_19;
    uint64_t var_0;
    uint64_t var_1;
    uint64_t var_2;
    uint64_t *var_3;
    uint64_t *var_4;
    uint64_t var_6;
    uint64_t var_5;
    uint64_t local_sp_0;
    uint64_t rcx3_0;
    uint64_t var_7;
    uint64_t *var_8;
    uint64_t *_pre_phi45;
    uint64_t local_sp_5;
    uint64_t var_9;
    uint64_t _pre40;
    uint64_t var_10;
    uint64_t local_sp_1;
    uint64_t rcx3_1;
    uint64_t var_12;
    uint64_t var_11;
    uint64_t local_sp_2;
    uint64_t rcx3_2;
    uint64_t var_13;
    uint64_t var_14;
    uint64_t *var_15;
    uint32_t var_16;
    unsigned char *_pre_phi49;
    uint64_t local_sp_4;
    uint64_t r96_1;
    uint64_t var_17;
    uint64_t var_20;
    uint64_t local_sp_3;
    uint64_t r96_0;
    uint64_t r87_0;
    unsigned char var_21;
    unsigned char *var_22;
    uint64_t var_18;
    unsigned char var_23;
    unsigned char *var_24;
    unsigned char var_25;
    unsigned char *var_26;
    uint64_t r87_1;
    uint64_t var_28;
    uint64_t var_29;
    uint64_t var_30;
    uint64_t var_31;
    struct bb_string2_extend_ret_type mrv;
    struct bb_string2_extend_ret_type mrv1;
    struct bb_string2_extend_ret_type mrv2;
    uint64_t var_27;
    var_0 = revng_init_local_sp(0UL);
    var_1 = init_rbp();
    *(uint64_t *)(var_0 + (-8L)) = var_1;
    var_2 = var_0 + (-40L);
    var_3 = (uint64_t *)(var_0 + (-32L));
    *var_3 = rdi;
    var_4 = (uint64_t *)var_2;
    *var_4 = rsi;
    var_6 = rsi;
    local_sp_0 = var_2;
    rcx3_0 = rcx;
    r96_1 = r9;
    r96_0 = r9;
    r87_0 = r8;
    r87_1 = r8;
    if (*(unsigned char *)6395332UL == '\x00') {
        var_5 = var_0 + (-48L);
        *(uint64_t *)var_5 = 4209866UL;
        indirect_placeholder_1();
        var_6 = *var_4;
        local_sp_0 = var_5;
        rcx3_0 = 4283973UL;
    }
    var_7 = *(uint64_t *)(*var_3 + 24UL);
    var_8 = (uint64_t *)(var_6 + 24UL);
    _pre_phi45 = var_8;
    var_10 = var_6;
    local_sp_1 = local_sp_0;
    rcx3_1 = rcx3_0;
    if (var_7 > *var_8) {
        var_9 = local_sp_0 + (-8L);
        *(uint64_t *)var_9 = 4209915UL;
        indirect_placeholder_1();
        _pre40 = *var_4;
        _pre_phi45 = (uint64_t *)(_pre40 + 24UL);
        var_10 = _pre40;
        local_sp_1 = var_9;
        rcx3_1 = 4283973UL;
    }
    var_12 = var_10;
    local_sp_2 = local_sp_1;
    rcx3_2 = rcx3_1;
    if (*_pre_phi45 == 0UL) {
        var_11 = local_sp_1 + (-8L);
        *(uint64_t *)var_11 = 4209956UL;
        indirect_placeholder_1();
        var_12 = *var_4;
        local_sp_2 = var_11;
        rcx3_2 = 4283973UL;
    }
    var_13 = *(uint64_t *)(var_12 + 8UL);
    var_14 = var_0 + (-24L);
    var_15 = (uint64_t *)var_14;
    *var_15 = var_13;
    var_16 = **(uint32_t **)var_14;
    var_20 = var_13;
    local_sp_3 = local_sp_2;
    local_sp_4 = local_sp_2;
    local_sp_5 = local_sp_2;
    if (var_16 <= 4U) {
        var_27 = local_sp_5 + (-8L);
        *(uint64_t *)var_27 = 4210068UL;
        indirect_placeholder_1();
        _pre_phi49 = (unsigned char *)(var_0 + (-9L));
        local_sp_4 = var_27;
        var_28 = *(uint64_t *)(*var_3 + 24UL);
        var_29 = *var_4;
        var_30 = var_28 - *(uint64_t *)(var_29 + 24UL);
        var_31 = (uint64_t)*_pre_phi49;
        *(uint64_t *)(local_sp_4 + (-8L)) = 4210105UL;
        indirect_placeholder_22(var_30, var_29, var_31);
        *(uint64_t *)(*var_4 + 24UL) = *(uint64_t *)(*var_3 + 24UL);
        mrv.field_0 = var_31;
        mrv1 = mrv;
        mrv1.field_1 = r96_1;
        mrv2 = mrv1;
        mrv2.field_2 = r87_1;
        return mrv2;
    }
    switch (*(uint64_t *)(((uint64_t)var_16 << 3UL) + 4282848UL)) {
      case 4209994UL:
        {
            var_25 = *(unsigned char *)(var_13 + 16UL);
            var_26 = (unsigned char *)(var_0 + (-9L));
            *var_26 = var_25;
            _pre_phi49 = var_26;
        }
        break;
      case 4210007UL:
        {
            var_23 = *(unsigned char *)(var_13 + 17UL);
            var_24 = (unsigned char *)(var_0 + (-9L));
            *var_24 = var_23;
            _pre_phi49 = var_24;
        }
        break;
      case 4210058UL:
        {
            var_17 = local_sp_2 + (-8L);
            *(uint64_t *)var_17 = 4210063UL;
            indirect_placeholder_1();
            local_sp_5 = var_17;
            var_27 = local_sp_5 + (-8L);
            *(uint64_t *)var_27 = 4210068UL;
            indirect_placeholder_1();
            _pre_phi49 = (unsigned char *)(var_0 + (-9L));
            local_sp_4 = var_27;
        }
        break;
      case 4210045UL:
      case 4210020UL:
        {
            switch (*(uint64_t *)(((uint64_t)var_16 << 3UL) + 4282848UL)) {
              case 4210045UL:
                {
                    var_21 = *(unsigned char *)(var_20 + 16UL);
                    var_22 = (unsigned char *)(var_0 + (-9L));
                    *var_22 = var_21;
                    _pre_phi49 = var_22;
                    local_sp_4 = local_sp_3;
                    r96_1 = r96_0;
                    r87_1 = r87_0;
                }
                break;
              case 4210020UL:
                {
                    var_18 = local_sp_2 + (-8L);
                    *(uint64_t *)var_18 = 4210045UL;
                    var_19 = indirect_placeholder_21(0UL, 4282736UL, rcx3_2, 1UL, 0UL, r9, r8);
                    var_20 = *var_15;
                    local_sp_3 = var_18;
                    r96_0 = var_19.field_1;
                    r87_0 = var_19.field_2;
                }
                break;
            }
        }
        break;
      default:
        {
            abort();
        }
        break;
    }
}

typedef unsigned char __u_char;
typedef unsigned short __u_short;
typedef unsigned int __u_int;
typedef unsigned long __u_long;
typedef signed char __int8_t;
typedef unsigned char __uint8_t;
typedef short __int16_t;
typedef unsigned short __uint16_t;
typedef int __int32_t;
typedef unsigned int __uint32_t;
typedef long __int64_t;
typedef unsigned long __uint64_t;
typedef __int8_t __int_least8_t;
typedef __uint8_t __uint_least8_t;
typedef __int16_t __int_least16_t;
typedef __uint16_t __uint_least16_t;
typedef __int32_t __int_least32_t;
typedef __uint32_t __uint_least32_t;
typedef __int64_t __int_least64_t;
typedef __uint64_t __uint_least64_t;
typedef long __quad_t;
typedef unsigned long __u_quad_t;
typedef long __intmax_t;
typedef unsigned long __uintmax_t;
typedef unsigned long __dev_t;
typedef unsigned int __uid_t;
typedef unsigned int __gid_t;
typedef unsigned long __ino_t;
typedef unsigned long __ino64_t;
typedef unsigned int __mode_t;
typedef unsigned long __nlink_t;
typedef long __off_t;
typedef long __off64_t;
typedef int __pid_t;
typedef struct {
    int __val[2];
} __fsid_t;
typedef long __clock_t;
typedef unsigned long __rlim_t;
typedef unsigned long __rlim64_t;
typedef unsigned int __id_t;
typedef long __time_t;
typedef unsigned int __useconds_t;
typedef long __suseconds_t;
typedef int __daddr_t;
typedef int __key_t;
typedef int __clockid_t;
typedef void *__timer_t;
typedef long __blksize_t;
typedef long __blkcnt_t;
typedef long __blkcnt64_t;
typedef unsigned long __fsblkcnt_t;
typedef unsigned long __fsblkcnt64_t;
typedef unsigned long __fsfilcnt_t;
typedef unsigned long __fsfilcnt64_t;
typedef long __fsword_t;
typedef long __ssize_t;
typedef long __syscall_slong_t;
typedef unsigned long __syscall_ulong_t;
typedef __off64_t __loff_t;
typedef char *__caddr_t;
typedef long __intptr_t;
typedef unsigned int __socklen_t;
typedef int __sig_atomic_t;
typedef __int8_t int8_t;
typedef __int16_t int16_t;
typedef __int32_t int32_t;
typedef __int64_t int64_t;
typedef __uint8_t uint8_t;
typedef __uint16_t uint16_t;
typedef __uint32_t uint32_t;
typedef __uint64_t uint64_t;
typedef __int_least8_t int_least8_t;
typedef __int_least16_t int_least16_t;
typedef __int_least32_t int_least32_t;
typedef __int_least64_t int_least64_t;
typedef __uint_least8_t uint_least8_t;
typedef __uint_least16_t uint_least16_t;
typedef __uint_least32_t uint_least32_t;
typedef __uint_least64_t uint_least64_t;
typedef signed char int_fast8_t;
typedef long int_fast16_t;
typedef long int_fast32_t;
typedef long int_fast64_t;
typedef unsigned char uint_fast8_t;
typedef unsigned long uint_fast16_t;
typedef unsigned long uint_fast32_t;
typedef unsigned long uint_fast64_t;
typedef long intptr_t;
typedef unsigned long uintptr_t;
typedef __intmax_t intmax_t;
typedef __uintmax_t uintmax_t;
typedef _Bool bool;
struct indirect_placeholder_13_ret_type;
struct indirect_placeholder_14_ret_type;
struct indirect_placeholder_15_ret_type;
struct indirect_placeholder_17_ret_type;
struct indirect_placeholder_18_ret_type;
struct indirect_placeholder_19_ret_type;
struct indirect_placeholder_21_ret_type;
struct indirect_placeholder_22_ret_type;
struct indirect_placeholder_24_ret_type;
struct indirect_placeholder_13_ret_type {
    uint64_t field_0;
    uint64_t field_1;
};
struct indirect_placeholder_14_ret_type {
    uint64_t field_0;
    uint64_t field_1;
    uint64_t field_2;
};
struct indirect_placeholder_15_ret_type {
    uint64_t field_0;
    uint64_t field_1;
    uint64_t field_2;
};
struct indirect_placeholder_17_ret_type {
    uint64_t field_0;
    uint64_t field_1;
    uint64_t field_2;
};
struct indirect_placeholder_18_ret_type {
    uint64_t field_0;
    uint64_t field_1;
    uint64_t field_2;
};
struct indirect_placeholder_19_ret_type {
    uint64_t field_0;
    uint64_t field_1;
    uint64_t field_2;
};
struct indirect_placeholder_21_ret_type {
    uint64_t field_0;
    uint64_t field_1;
    uint64_t field_2;
};
struct indirect_placeholder_22_ret_type {
    uint64_t field_0;
    uint64_t field_1;
    uint64_t field_2;
};
struct indirect_placeholder_24_ret_type {
    uint64_t field_0;
    uint64_t field_1;
    uint64_t field_2;
    uint64_t field_3;
};
extern void abort(void);
extern uint64_t revng_init_local_sp(uint64_t param_0);
extern uint64_t init_rbp(void);
extern void indirect_placeholder_1(void);
extern uint64_t indirect_placeholder_8(uint64_t param_0, uint64_t param_1);
extern uint64_t indirect_placeholder_20(uint64_t param_0, uint64_t param_1, uint64_t param_2);
extern uint64_t init_rbx(void);
extern uint64_t indirect_placeholder_3(uint64_t param_0);
extern void indirect_placeholder_12(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint64_t param_3, uint64_t param_4, uint64_t param_5, uint64_t param_6);
extern void indirect_placeholder_11(uint64_t param_0);
extern void indirect_placeholder_9(uint64_t param_0, uint64_t param_1);
extern uint64_t indirect_placeholder_5(void);
extern uint64_t indirect_placeholder_16(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint64_t param_3);
extern struct indirect_placeholder_13_ret_type indirect_placeholder_13(uint64_t param_0);
extern struct indirect_placeholder_14_ret_type indirect_placeholder_14(uint64_t param_0);
extern struct indirect_placeholder_15_ret_type indirect_placeholder_15(uint64_t param_0);
extern struct indirect_placeholder_17_ret_type indirect_placeholder_17(uint64_t param_0);
extern struct indirect_placeholder_18_ret_type indirect_placeholder_18(uint64_t param_0);
extern struct indirect_placeholder_19_ret_type indirect_placeholder_19(uint64_t param_0);
extern struct indirect_placeholder_21_ret_type indirect_placeholder_21(uint64_t param_0);
extern struct indirect_placeholder_22_ret_type indirect_placeholder_22(uint64_t param_0);
extern void indirect_placeholder_23(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint64_t param_3, uint64_t param_4, uint64_t param_5);
extern struct indirect_placeholder_24_ret_type indirect_placeholder_24(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint64_t param_3, uint64_t param_4);
void bb_main(uint64_t rdi, uint64_t rsi) {
    uint64_t local_sp_2;
    struct indirect_placeholder_24_ret_type var_22;
    uint64_t var_0;
    uint64_t var_1;
    uint64_t var_2;
    uint64_t var_3;
    uint32_t *var_4;
    uint64_t var_5;
    uint64_t *var_6;
    uint64_t var_7;
    uint64_t *var_8;
    unsigned char *var_9;
    uint64_t *var_10;
    unsigned char *var_11;
    unsigned char *var_12;
    unsigned char *var_13;
    unsigned char *var_14;
    uint32_t *var_15;
    uint64_t var_16;
    uint64_t var_17;
    uint32_t *var_18;
    uint64_t local_sp_14;
    uint64_t r8_2;
    uint64_t var_146;
    uint64_t var_147;
    uint64_t r9_2;
    uint32_t var_142;
    uint32_t *var_143;
    uint64_t var_112;
    uint64_t var_144;
    struct indirect_placeholder_13_ret_type var_145;
    uint64_t var_141;
    uint64_t r8_6;
    uint64_t var_133;
    uint64_t r9_1;
    uint64_t local_sp_0;
    uint64_t local_sp_1;
    uint64_t _pre_phi;
    uint64_t var_134;
    struct indirect_placeholder_14_ret_type var_135;
    uint64_t var_136;
    uint64_t var_137;
    uint64_t var_138;
    uint64_t var_139;
    uint64_t var_140;
    uint64_t r9_6;
    uint64_t r8_0;
    uint64_t r9_0;
    uint64_t var_123;
    struct indirect_placeholder_15_ret_type var_124;
    uint64_t var_125;
    uint64_t var_126;
    uint64_t var_127;
    uint64_t var_128;
    uint64_t var_129;
    uint64_t r8_1;
    uint64_t local_sp_11;
    uint64_t local_sp_5;
    uint64_t r8_4;
    uint64_t r9_4;
    uint64_t local_sp_3;
    uint64_t var_120;
    uint32_t *var_121;
    uint32_t var_122;
    uint64_t var_130;
    uint32_t *var_131;
    uint32_t var_132;
    uint64_t rax_1;
    uint64_t storemerge;
    uint64_t local_sp_6;
    uint64_t rax_0;
    struct indirect_placeholder_17_ret_type var_90;
    uint64_t var_91;
    uint64_t var_92;
    uint64_t var_93;
    uint64_t var_94;
    uint64_t local_sp_7;
    uint64_t r9_8;
    uint64_t var_85;
    uint64_t r8_8;
    uint64_t var_86;
    uint64_t var_87;
    uint64_t var_88;
    uint64_t var_89;
    uint64_t var_78;
    struct indirect_placeholder_18_ret_type var_79;
    uint64_t var_80;
    uint64_t var_81;
    uint64_t var_82;
    uint64_t var_83;
    uint64_t *_pre_phi355;
    uint64_t var_84;
    uint64_t var_95;
    unsigned char var_96;
    uint64_t var_97;
    uint64_t var_100;
    struct indirect_placeholder_19_ret_type var_101;
    uint64_t var_102;
    uint64_t var_103;
    uint64_t var_104;
    uint64_t var_105;
    uint64_t r8_5;
    uint64_t r9_5;
    uint64_t var_106;
    uint64_t var_107;
    uint64_t var_108;
    uint64_t *var_109;
    uint64_t var_110;
    uint64_t var_111;
    uint64_t var_98;
    uint64_t var_99;
    uint64_t local_sp_8;
    uint64_t var_113;
    bool var_114;
    uint64_t var_115;
    uint64_t var_116;
    uint64_t local_sp_10;
    uint64_t var_117;
    uint64_t var_118;
    uint64_t *var_119;
    uint64_t r9_9;
    struct indirect_placeholder_21_ret_type var_67;
    uint64_t var_68;
    uint64_t var_69;
    uint64_t var_70;
    uint64_t var_71;
    uint64_t var_58;
    uint64_t var_59;
    uint64_t var_60;
    uint64_t var_57;
    uint64_t local_sp_9;
    uint64_t var_61;
    uint64_t *var_62;
    uint64_t local_sp_12;
    uint64_t *_pre_phi351;
    uint64_t r8_7;
    uint64_t r9_7;
    uint64_t var_63;
    uint64_t var_64;
    uint64_t var_65;
    uint64_t var_66;
    uint64_t var_72;
    uint64_t var_73;
    uint64_t var_74;
    uint64_t var_75;
    uint64_t var_76;
    uint64_t *var_77;
    uint64_t var_41;
    uint64_t var_42;
    uint64_t r8_9;
    uint64_t var_49;
    uint64_t *var_50;
    uint64_t var_51;
    uint64_t var_52;
    uint64_t *var_53;
    uint64_t var_54;
    uint64_t var_43;
    struct indirect_placeholder_22_ret_type var_44;
    uint64_t var_45;
    uint64_t var_46;
    uint64_t var_47;
    uint64_t var_48;
    uint64_t var_27;
    uint64_t var_28;
    uint32_t var_29;
    uint64_t var_148;
    uint64_t var_33;
    uint64_t var_34;
    uint64_t *var_35;
    uint64_t *_pre_phi349;
    uint64_t var_30;
    uint64_t var_31;
    uint64_t *var_32;
    bool var_36;
    uint64_t var_37;
    uint64_t var_38;
    uint64_t *var_39;
    uint64_t *var_40;
    uint64_t _pre;
    uint64_t var_55;
    uint64_t var_56;
    uint64_t local_sp_13;
    uint64_t var_24;
    uint64_t var_25;
    uint64_t *var_26;
    uint64_t var_19;
    uint64_t var_20;
    uint64_t var_21;
    uint32_t var_23;
    unsigned int loop_state_var;
    var_0 = revng_init_local_sp(0UL);
    var_1 = init_rbp();
    var_2 = init_rbx();
    var_3 = var_0 + (-8L);
    *(uint64_t *)var_3 = var_1;
    *(uint64_t *)(var_0 + (-16L)) = var_2;
    var_4 = (uint32_t *)(var_0 + (-156L));
    *var_4 = (uint32_t)rdi;
    var_5 = var_0 + (-168L);
    var_6 = (uint64_t *)var_5;
    *var_6 = rsi;
    var_7 = var_0 + (-40L);
    var_8 = (uint64_t *)var_7;
    *var_8 = 0UL;
    var_9 = (unsigned char *)(var_0 + (-41L));
    *var_9 = (unsigned char)'\x00';
    var_10 = (uint64_t *)(var_0 + (-64L));
    *var_10 = 0UL;
    var_11 = (unsigned char *)(var_0 + (-65L));
    *var_11 = (unsigned char)'\x00';
    var_12 = (unsigned char *)(var_0 + (-66L));
    *var_12 = (unsigned char)'\x00';
    var_13 = (unsigned char *)(var_0 + (-67L));
    *var_13 = (unsigned char)'\x00';
    var_14 = (unsigned char *)(var_0 + (-68L));
    *var_14 = (unsigned char)'\x00';
    var_15 = (uint32_t *)(var_0 + (-72L));
    *var_15 = 0U;
    var_16 = **(uint64_t **)var_5;
    *(uint64_t *)(var_0 + (-176L)) = 4203256UL;
    indirect_placeholder_11(var_16);
    *(uint64_t *)(var_0 + (-184L)) = 4203271UL;
    indirect_placeholder_1();
    var_17 = var_0 + (-192L);
    *(uint64_t *)var_17 = 4203281UL;
    indirect_placeholder_1();
    var_18 = (uint32_t *)(var_0 + (-84L));
    storemerge = 4279650UL;
    local_sp_14 = var_17;
    while (1U)
        {
            var_19 = *var_6;
            var_20 = (uint64_t)*var_4;
            var_21 = local_sp_14 + (-8L);
            *(uint64_t *)var_21 = 4203574UL;
            var_22 = indirect_placeholder_24(4279493UL, 4277760UL, 0UL, var_20, var_19);
            var_23 = (uint32_t)var_22.field_0;
            *var_18 = var_23;
            local_sp_13 = var_21;
            local_sp_14 = var_21;
            if (var_23 != 4294967295U) {
                var_27 = var_22.field_2;
                var_28 = var_22.field_3;
                var_29 = *var_4 - *(uint32_t *)6391000UL;
                *(uint32_t *)(var_0 + (-88L)) = var_29;
                r8_7 = var_27;
                r9_7 = var_28;
                r8_9 = var_27;
                r9_9 = var_28;
                if (var_29 > 1U) {
                    var_148 = var_22.field_1;
                    *(uint64_t *)(local_sp_14 + (-16L)) = 4203637UL;
                    indirect_placeholder_12(0UL, 4279501UL, var_148, var_27, var_28, 0UL, 0UL);
                    *(uint64_t *)(local_sp_14 + (-24L)) = 4203647UL;
                    indirect_placeholder_9(var_3, 1UL);
                    abort();
                }
                if (var_29 != 0U) {
                    *var_11 = (unsigned char)'\x01';
                    var_33 = *(uint64_t *)6390848UL;
                    var_34 = var_0 + (-56L);
                    var_35 = (uint64_t *)var_34;
                    *var_35 = var_33;
                    _pre_phi349 = var_35;
                    _pre_phi = var_34;
                    loop_state_var = 1U;
                    break;
                }
                var_30 = *(uint64_t *)(*var_6 + ((uint64_t)*(uint32_t *)6391000UL << 3UL));
                var_31 = var_0 + (-56L);
                var_32 = (uint64_t *)var_31;
                *var_32 = var_30;
                _pre_phi349 = var_32;
                _pre_phi = var_31;
                loop_state_var = 1U;
                break;
            }
            if ((uint64_t)(var_23 + (-112)) == 0UL) {
                *var_8 = *(uint64_t *)6391704UL;
                *var_11 = (unsigned char)'\x01';
                continue;
            }
            if ((int)var_23 <= (int)112U) {
                if ((uint64_t)(var_23 + 130U) == 0UL) {
                    *(uint64_t *)(local_sp_14 + (-16L)) = 4203456UL;
                    indirect_placeholder_9(var_3, 0UL);
                    abort();
                }
                if ((int)var_23 <= (int)4294967166U) {
                    if (var_23 != 4294967165U) {
                        loop_state_var = 2U;
                        break;
                    }
                    loop_state_var = 0U;
                    break;
                }
                if ((uint64_t)(var_23 + (-86)) != 0UL) {
                    loop_state_var = 2U;
                    break;
                }
                if ((uint64_t)(var_23 + (-100)) != 0UL) {
                    loop_state_var = 0U;
                    break;
                }
                *var_13 = (unsigned char)'\x01';
                continue;
            }
            if ((uint64_t)(var_23 + (-116)) == 0UL) {
                *var_11 = (unsigned char)'\x01';
                *var_12 = (unsigned char)'\x01';
                continue;
            }
            if ((int)var_23 <= (int)116U) {
                if ((uint64_t)(var_23 + (-113)) != 0UL) {
                    loop_state_var = 0U;
                    break;
                }
                *var_9 = (unsigned char)'\x01';
                continue;
            }
            if ((uint64_t)(var_23 + (-117)) == 0UL) {
                *var_14 = (unsigned char)'\x01';
                continue;
            }
            if ((uint64_t)(var_23 + (-128)) != 0UL) {
                loop_state_var = 0U;
                break;
            }
            *var_10 = *(uint64_t *)6391704UL;
            continue;
        }
    switch (loop_state_var) {
      case 1U:
        {
            var_36 = (*var_10 == 0UL);
            var_37 = *_pre_phi349;
            var_38 = local_sp_14 + (-16L);
            var_39 = (uint64_t *)var_38;
            local_sp_12 = var_38;
            if (var_36) {
                *var_39 = 4203936UL;
                var_55 = indirect_placeholder_3(var_37);
                *_pre_phi349 = var_55;
                var_56 = local_sp_14 + (-24L);
                *(uint64_t *)var_56 = 4203957UL;
                indirect_placeholder_1();
                *var_10 = var_55;
                local_sp_9 = var_56;
                if (var_55 == 0UL) {
                    var_58 = *_pre_phi349;
                    var_59 = local_sp_14 + (-32L);
                    *(uint64_t *)var_59 = 4203985UL;
                    indirect_placeholder_1();
                    *var_10 = var_58;
                    var_60 = var_58;
                    local_sp_9 = var_59;
                } else {
                    var_57 = var_55 + 1UL;
                    *var_10 = var_57;
                    var_60 = var_57;
                }
                var_61 = local_sp_9 + (-8L);
                *(uint64_t *)var_61 = 4204008UL;
                indirect_placeholder_1();
                var_62 = (uint64_t *)(var_0 + (-80L));
                *var_62 = var_60;
                _pre_phi351 = var_62;
                local_sp_10 = var_61;
            } else {
                *var_39 = 4203726UL;
                indirect_placeholder_1();
                var_40 = (uint64_t *)(var_0 + (-96L));
                *var_40 = var_37;
                if (var_37 == 0UL) {
                    _pre = *_pre_phi349;
                    var_43 = _pre;
                    *(uint64_t *)(local_sp_14 + (-24L)) = 4203771UL;
                    var_44 = indirect_placeholder_22(var_43);
                    var_45 = var_44.field_0;
                    var_46 = var_44.field_1;
                    var_47 = var_44.field_2;
                    var_48 = local_sp_14 + (-32L);
                    *(uint64_t *)var_48 = 4203799UL;
                    indirect_placeholder_12(0UL, 4279520UL, var_45, var_46, var_47, 1UL, 0UL);
                    local_sp_12 = var_48;
                    r8_9 = var_46;
                    r9_9 = var_47;
                } else {
                    var_41 = var_37 + (-1L);
                    var_42 = *_pre_phi349;
                    var_43 = var_42;
                    if (*(unsigned char *)(var_42 + var_41) == 'X') {
                        *(uint64_t *)(local_sp_14 + (-24L)) = 4203771UL;
                        var_44 = indirect_placeholder_22(var_43);
                        var_45 = var_44.field_0;
                        var_46 = var_44.field_1;
                        var_47 = var_44.field_2;
                        var_48 = local_sp_14 + (-32L);
                        *(uint64_t *)var_48 = 4203799UL;
                        indirect_placeholder_12(0UL, 4279520UL, var_45, var_46, var_47, 1UL, 0UL);
                        local_sp_12 = var_48;
                        r8_9 = var_46;
                        r9_9 = var_47;
                    }
                }
                var_49 = *var_10;
                *(uint64_t *)(local_sp_12 + (-8L)) = 4203811UL;
                indirect_placeholder_1();
                var_50 = (uint64_t *)(var_0 + (-80L));
                *var_50 = var_49;
                var_51 = (var_49 + *var_40) + 1UL;
                *(uint64_t *)(local_sp_12 + (-16L)) = 4203838UL;
                var_52 = indirect_placeholder_3(var_51);
                var_53 = (uint64_t *)(var_0 + (-104L));
                *var_53 = var_52;
                *(uint64_t *)(local_sp_12 + (-24L)) = 4203865UL;
                indirect_placeholder_1();
                var_54 = local_sp_12 + (-32L);
                *(uint64_t *)var_54 = 4203899UL;
                indirect_placeholder_1();
                *_pre_phi349 = *var_53;
                *var_10 = (*var_40 + *var_53);
                _pre_phi351 = var_50;
                local_sp_10 = var_54;
                r8_7 = r8_9;
                r9_7 = r9_9;
            }
            local_sp_11 = local_sp_10;
            r8_8 = r8_7;
            r9_8 = r9_7;
            var_63 = *var_10;
            var_64 = local_sp_10 + (-8L);
            *(uint64_t *)var_64 = 4204031UL;
            var_65 = indirect_placeholder_3(var_63);
            var_66 = *var_10;
            local_sp_11 = var_64;
            if (*_pre_phi351 != 0UL & var_65 == var_66) {
                *(uint64_t *)(local_sp_10 + (-16L)) = 4204049UL;
                var_67 = indirect_placeholder_21(var_66);
                var_68 = var_67.field_0;
                var_69 = var_67.field_1;
                var_70 = var_67.field_2;
                var_71 = local_sp_10 + (-24L);
                *(uint64_t *)var_71 = 4204077UL;
                indirect_placeholder_12(0UL, 4279568UL, var_68, var_69, var_70, 1UL, 0UL);
                local_sp_11 = var_71;
                r8_8 = var_69;
                r9_8 = var_70;
            }
            var_72 = *var_10;
            var_73 = *_pre_phi349;
            var_74 = var_72 - var_73;
            var_75 = local_sp_11 + (-8L);
            *(uint64_t *)var_75 = 4204109UL;
            var_76 = indirect_placeholder_8(var_73, var_74);
            var_77 = (uint64_t *)(var_0 + (-112L));
            *var_77 = var_76;
            local_sp_5 = var_75;
            rax_0 = var_76;
            r8_4 = r8_8;
            r9_4 = r9_8;
            if (var_76 > 2UL) {
                var_78 = *_pre_phi349;
                *(uint64_t *)(local_sp_11 + (-16L)) = 4204132UL;
                var_79 = indirect_placeholder_18(var_78);
                var_80 = var_79.field_0;
                var_81 = var_79.field_1;
                var_82 = var_79.field_2;
                var_83 = local_sp_11 + (-24L);
                *(uint64_t *)var_83 = 4204160UL;
                indirect_placeholder_12(0UL, 4279616UL, var_80, var_81, var_82, 1UL, 0UL);
                local_sp_5 = var_83;
                rax_0 = 0UL;
                r8_4 = var_81;
                r9_4 = var_82;
            }
            r8_6 = r8_4;
            r9_6 = r9_4;
            rax_1 = rax_0;
            local_sp_6 = local_sp_5;
            r8_5 = r8_4;
            r9_5 = r9_4;
            local_sp_8 = local_sp_5;
            if (*var_11 == '\x00') {
                _pre_phi355 = (uint64_t *)(var_0 + (-104L));
                var_112 = *_pre_phi349;
            } else {
                if (*var_12 == '\x00') {
                    var_95 = *var_8;
                    if (var_95 == 0UL) {
                        var_96 = **(unsigned char **)var_7;
                        var_97 = (uint64_t)var_96;
                        rax_1 = var_97;
                        if (var_96 == '\x00') {
                            *(uint64_t *)(var_0 + (-32L)) = var_95;
                        } else {
                            var_98 = local_sp_5 + (-8L);
                            *(uint64_t *)var_98 = 4204356UL;
                            indirect_placeholder_1();
                            var_99 = var_0 + (-128L);
                            *(uint64_t *)var_99 = rax_1;
                            local_sp_6 = var_98;
                            if (rax_1 != 0UL) {
                                storemerge = (**(unsigned char **)var_99 == '\x00') ? 4279650UL : rax_1;
                            }
                            *(uint64_t *)(var_0 + (-32L)) = storemerge;
                        }
                    } else {
                        var_98 = local_sp_5 + (-8L);
                        *(uint64_t *)var_98 = 4204356UL;
                        indirect_placeholder_1();
                        var_99 = var_0 + (-128L);
                        *(uint64_t *)var_99 = rax_1;
                        local_sp_6 = var_98;
                        if (rax_1 == 0UL) {
                            storemerge = (**(unsigned char **)var_99 == '\x00') ? 4279650UL : rax_1;
                        }
                        *(uint64_t *)(var_0 + (-32L)) = storemerge;
                    }
                    local_sp_7 = local_sp_6;
                    if (**(unsigned char **)_pre_phi == '/') {
                        var_100 = *_pre_phi349;
                        *(uint64_t *)(local_sp_6 + (-8L)) = 4204416UL;
                        var_101 = indirect_placeholder_19(var_100);
                        var_102 = var_101.field_0;
                        var_103 = var_101.field_1;
                        var_104 = var_101.field_2;
                        var_105 = local_sp_6 + (-16L);
                        *(uint64_t *)var_105 = 4204444UL;
                        indirect_placeholder_12(0UL, 4279712UL, var_102, var_103, var_104, 1UL, 0UL);
                        local_sp_7 = var_105;
                        r8_5 = var_103;
                        r9_5 = var_104;
                    }
                } else {
                    *(uint64_t *)(local_sp_5 + (-8L)) = 4204190UL;
                    indirect_placeholder_1();
                    var_84 = var_0 + (-120L);
                    *(uint64_t *)var_84 = rax_0;
                    if (rax_0 == 0UL) {
                        if (**(unsigned char **)var_84 == '\x00') {
                            *(uint64_t *)(var_0 + (-32L)) = rax_0;
                        } else {
                            var_85 = *var_8;
                            if (var_85 == 0UL) {
                                *(uint64_t *)(var_0 + (-32L)) = 4279650UL;
                            } else {
                                if (**(unsigned char **)var_7 == '\x00') {
                                    *(uint64_t *)(var_0 + (-32L)) = 4279650UL;
                                } else {
                                    *(uint64_t *)(var_0 + (-32L)) = var_85;
                                }
                            }
                        }
                    } else {
                        var_85 = *var_8;
                        if (var_85 == 0UL) {
                            *(uint64_t *)(var_0 + (-32L)) = 4279650UL;
                        } else {
                            if (**(unsigned char **)var_7 == '\x00') {
                                *(uint64_t *)(var_0 + (-32L)) = var_85;
                            } else {
                                *(uint64_t *)(var_0 + (-32L)) = 4279650UL;
                            }
                        }
                    }
                    var_86 = *_pre_phi349;
                    var_87 = local_sp_5 + (-16L);
                    *(uint64_t *)var_87 = 4204270UL;
                    var_88 = indirect_placeholder_3(var_86);
                    var_89 = *_pre_phi349;
                    local_sp_7 = var_87;
                    if (var_88 == var_89) {
                        *(uint64_t *)(local_sp_5 + (-24L)) = 4204288UL;
                        var_90 = indirect_placeholder_17(var_89);
                        var_91 = var_90.field_0;
                        var_92 = var_90.field_1;
                        var_93 = var_90.field_2;
                        var_94 = local_sp_5 + (-32L);
                        *(uint64_t *)var_94 = 4204316UL;
                        indirect_placeholder_12(0UL, 4279656UL, var_91, var_92, var_93, 1UL, 0UL);
                        local_sp_7 = var_94;
                        r8_5 = var_92;
                        r9_5 = var_93;
                    }
                }
                var_106 = *_pre_phi349;
                var_107 = *(uint64_t *)(var_0 + (-32L));
                *(uint64_t *)(local_sp_7 + (-8L)) = 4204468UL;
                var_108 = indirect_placeholder_20(0UL, var_107, var_106);
                var_109 = (uint64_t *)(var_0 + (-104L));
                *var_109 = var_108;
                var_110 = local_sp_7 + (-16L);
                *(uint64_t *)var_110 = 4204484UL;
                indirect_placeholder_1();
                var_111 = *var_109;
                *_pre_phi349 = var_111;
                var_112 = var_111;
                r8_6 = r8_5;
                r9_6 = r9_5;
                _pre_phi355 = var_109;
                local_sp_8 = var_110;
            }
            *(uint64_t *)(local_sp_8 + (-8L)) = 4204504UL;
            var_113 = indirect_placeholder_3(var_112);
            *_pre_phi355 = var_113;
            var_114 = (*var_13 == '\x00');
            var_115 = (uint64_t)*var_14;
            var_116 = *var_77;
            var_117 = *_pre_phi351;
            var_118 = local_sp_8 + (-16L);
            var_119 = (uint64_t *)var_118;
            local_sp_2 = var_118;
            r8_2 = r8_6;
            r9_2 = r9_6;
            r9_1 = r9_6;
            local_sp_0 = var_118;
            r8_0 = r8_6;
            r9_0 = r9_6;
            r8_1 = r8_6;
            local_sp_3 = var_118;
            if (var_114) {
                *var_119 = 4204640UL;
                var_130 = indirect_placeholder_16(var_116, var_115, var_113, var_117);
                var_131 = (uint32_t *)(var_0 + (-136L));
                var_132 = (uint32_t)var_130;
                *var_131 = var_132;
                if ((int)var_132 < (int)0U) {
                    var_133 = local_sp_8 + (-24L);
                    *(uint64_t *)var_133 = 4204670UL;
                    indirect_placeholder_1();
                    local_sp_0 = var_133;
                    local_sp_3 = var_133;
                    if (*var_14 != '\x01' & var_132 != 0U) {
                        local_sp_1 = local_sp_0;
                        if (*var_9 != '\x01') {
                            var_134 = *_pre_phi349;
                            *(uint64_t *)(local_sp_0 + (-8L)) = 4204697UL;
                            var_135 = indirect_placeholder_14(var_134);
                            var_136 = var_135.field_0;
                            var_137 = var_135.field_1;
                            var_138 = var_135.field_2;
                            *(uint64_t *)(local_sp_0 + (-16L)) = 4204705UL;
                            indirect_placeholder_1();
                            var_139 = (uint64_t)*(uint32_t *)var_136;
                            var_140 = local_sp_0 + (-24L);
                            *(uint64_t *)var_140 = 4204732UL;
                            indirect_placeholder_12(0UL, 4279824UL, var_136, var_137, var_138, 0UL, var_139);
                            local_sp_1 = var_140;
                            r8_0 = var_137;
                            r9_0 = var_138;
                        }
                        *var_15 = 1U;
                        local_sp_3 = local_sp_1;
                        r8_2 = r8_0;
                        r9_2 = r9_0;
                    }
                } else {
                    local_sp_1 = local_sp_0;
                    if (*var_9 == '\x01') {
                        var_134 = *_pre_phi349;
                        *(uint64_t *)(local_sp_0 + (-8L)) = 4204697UL;
                        var_135 = indirect_placeholder_14(var_134);
                        var_136 = var_135.field_0;
                        var_137 = var_135.field_1;
                        var_138 = var_135.field_2;
                        *(uint64_t *)(local_sp_0 + (-16L)) = 4204705UL;
                        indirect_placeholder_1();
                        var_139 = (uint64_t)*(uint32_t *)var_136;
                        var_140 = local_sp_0 + (-24L);
                        *(uint64_t *)var_140 = 4204732UL;
                        indirect_placeholder_12(0UL, 4279824UL, var_136, var_137, var_138, 0UL, var_139);
                        local_sp_1 = var_140;
                        r8_0 = var_137;
                        r9_0 = var_138;
                    }
                    *var_15 = 1U;
                    local_sp_3 = local_sp_1;
                    r8_2 = r8_0;
                    r9_2 = r9_0;
                }
            } else {
                *var_119 = 4204538UL;
                var_120 = indirect_placeholder_16(var_116, var_115, var_113, var_117);
                var_121 = (uint32_t *)(var_0 + (-132L));
                var_122 = (uint32_t)var_120;
                *var_121 = var_122;
                if (var_122 != 0U) {
                    if (*var_9 != '\x01') {
                        var_123 = *_pre_phi349;
                        *(uint64_t *)(local_sp_8 + (-24L)) = 4204570UL;
                        var_124 = indirect_placeholder_15(var_123);
                        var_125 = var_124.field_0;
                        var_126 = var_124.field_1;
                        var_127 = var_124.field_2;
                        *(uint64_t *)(local_sp_8 + (-32L)) = 4204578UL;
                        indirect_placeholder_1();
                        var_128 = (uint64_t)*(uint32_t *)var_125;
                        var_129 = local_sp_8 + (-40L);
                        *(uint64_t *)var_129 = 4204605UL;
                        indirect_placeholder_12(0UL, 4279776UL, var_125, var_126, var_127, 0UL, var_128);
                        local_sp_2 = var_129;
                        r8_1 = var_126;
                        r9_1 = var_127;
                    }
                    *var_15 = 1U;
                    local_sp_3 = local_sp_2;
                    r8_2 = r8_1;
                    r9_2 = r9_1;
                }
            }
            *(uint64_t *)(local_sp_3 + (-8L)) = 4204757UL;
            indirect_placeholder_1();
            *(unsigned char *)6391184UL = (unsigned char)'\x01';
            *(uint64_t *)(local_sp_3 + (-16L)) = 4204790UL;
            var_141 = indirect_placeholder_5();
            if (*var_15 != 0U & *var_14 != '\x01' & (uint64_t)(uint32_t)var_141 != 0UL) {
                *(uint64_t *)(local_sp_3 + (-24L)) = 4204799UL;
                indirect_placeholder_1();
                var_142 = *(uint32_t *)var_141;
                var_143 = (uint32_t *)(var_0 + (-140L));
                *var_143 = var_142;
                var_144 = *_pre_phi355;
                *(uint64_t *)(local_sp_3 + (-32L)) = 4204819UL;
                var_145 = indirect_placeholder_13(var_144);
                if (*var_9 != '\x01') {
                    var_146 = var_145.field_1;
                    var_147 = (uint64_t)*var_143;
                    *(uint64_t *)(local_sp_3 + (-40L)) = 4204858UL;
                    indirect_placeholder_12(0UL, 4279862UL, var_146, r8_2, r9_2, 0UL, var_147);
                }
                *var_15 = 1U;
            }
            return;
        }
        break;
      case 2U:
      case 0U:
        {
            switch (loop_state_var) {
              case 0U:
                {
                    *(uint64_t *)(local_sp_13 + (-8L)) = 4203538UL;
                    indirect_placeholder_9(var_3, 1UL);
                    abort();
                }
                break;
              case 2U:
                {
                    var_24 = *(uint64_t *)6390856UL;
                    var_25 = local_sp_14 + (-24L);
                    var_26 = (uint64_t *)var_25;
                    *var_26 = 0UL;
                    *(uint64_t *)(local_sp_14 + (-32L)) = 4203514UL;
                    indirect_placeholder_23(0UL, 4277464UL, var_24, 4279480UL, 4279469UL, 4279462UL);
                    *var_26 = 4203528UL;
                    indirect_placeholder_1();
                    local_sp_13 = var_25;
                }
                break;
            }
        }
        break;
    }
}

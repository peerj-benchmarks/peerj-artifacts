typedef unsigned char __u_char;
typedef unsigned short __u_short;
typedef unsigned int __u_int;
typedef unsigned long __u_long;
typedef signed char __int8_t;
typedef unsigned char __uint8_t;
typedef short __int16_t;
typedef unsigned short __uint16_t;
typedef int __int32_t;
typedef unsigned int __uint32_t;
typedef long __int64_t;
typedef unsigned long __uint64_t;
typedef __int8_t __int_least8_t;
typedef __uint8_t __uint_least8_t;
typedef __int16_t __int_least16_t;
typedef __uint16_t __uint_least16_t;
typedef __int32_t __int_least32_t;
typedef __uint32_t __uint_least32_t;
typedef __int64_t __int_least64_t;
typedef __uint64_t __uint_least64_t;
typedef long __quad_t;
typedef unsigned long __u_quad_t;
typedef long __intmax_t;
typedef unsigned long __uintmax_t;
typedef unsigned long __dev_t;
typedef unsigned int __uid_t;
typedef unsigned int __gid_t;
typedef unsigned long __ino_t;
typedef unsigned long __ino64_t;
typedef unsigned int __mode_t;
typedef unsigned long __nlink_t;
typedef long __off_t;
typedef long __off64_t;
typedef int __pid_t;
typedef struct {
    int __val[2];
} __fsid_t;
typedef long __clock_t;
typedef unsigned long __rlim_t;
typedef unsigned long __rlim64_t;
typedef unsigned int __id_t;
typedef long __time_t;
typedef unsigned int __useconds_t;
typedef long __suseconds_t;
typedef int __daddr_t;
typedef int __key_t;
typedef int __clockid_t;
typedef void *__timer_t;
typedef long __blksize_t;
typedef long __blkcnt_t;
typedef long __blkcnt64_t;
typedef unsigned long __fsblkcnt_t;
typedef unsigned long __fsblkcnt64_t;
typedef unsigned long __fsfilcnt_t;
typedef unsigned long __fsfilcnt64_t;
typedef long __fsword_t;
typedef long __ssize_t;
typedef long __syscall_slong_t;
typedef unsigned long __syscall_ulong_t;
typedef __off64_t __loff_t;
typedef char *__caddr_t;
typedef long __intptr_t;
typedef unsigned int __socklen_t;
typedef int __sig_atomic_t;
typedef __int8_t int8_t;
typedef __int16_t int16_t;
typedef __int32_t int32_t;
typedef __int64_t int64_t;
typedef __uint8_t uint8_t;
typedef __uint16_t uint16_t;
typedef __uint32_t uint32_t;
typedef __uint64_t uint64_t;
typedef __int_least8_t int_least8_t;
typedef __int_least16_t int_least16_t;
typedef __int_least32_t int_least32_t;
typedef __int_least64_t int_least64_t;
typedef __uint_least8_t uint_least8_t;
typedef __uint_least16_t uint_least16_t;
typedef __uint_least32_t uint_least32_t;
typedef __uint_least64_t uint_least64_t;
typedef signed char int_fast8_t;
typedef long int_fast16_t;
typedef long int_fast32_t;
typedef long int_fast64_t;
typedef unsigned char uint_fast8_t;
typedef unsigned long uint_fast16_t;
typedef unsigned long uint_fast32_t;
typedef unsigned long uint_fast64_t;
typedef long intptr_t;
typedef unsigned long uintptr_t;
typedef __intmax_t intmax_t;
typedef __uintmax_t uintmax_t;
typedef _Bool bool;
struct indirect_placeholder_68_ret_type;
struct indirect_placeholder_67_ret_type;
struct indirect_placeholder_69_ret_type;
struct indirect_placeholder_66_ret_type;
struct indirect_placeholder_70_ret_type;
struct indirect_placeholder_65_ret_type;
struct indirect_placeholder_68_ret_type {
    uint64_t field_0;
    uint64_t field_1;
    uint64_t field_2;
};
struct indirect_placeholder_67_ret_type {
    uint64_t field_0;
    uint64_t field_1;
    uint64_t field_2;
};
struct indirect_placeholder_69_ret_type {
    uint64_t field_0;
    uint64_t field_1;
    uint64_t field_2;
};
struct indirect_placeholder_66_ret_type {
    uint64_t field_0;
    uint64_t field_1;
    uint64_t field_2;
};
struct indirect_placeholder_70_ret_type {
    uint64_t field_0;
    uint64_t field_1;
    uint64_t field_2;
};
struct indirect_placeholder_65_ret_type {
    uint64_t field_0;
    uint64_t field_1;
    uint64_t field_2;
};
extern uint64_t revng_init_local_sp(uint64_t param_0);
extern uint64_t init_rbp(void);
extern void indirect_placeholder(void);
extern uint64_t indirect_placeholder_44(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint64_t param_3, uint64_t param_4);
extern uint64_t init_rbx(void);
extern uint64_t indirect_placeholder_9(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint64_t param_3);
extern struct indirect_placeholder_68_ret_type indirect_placeholder_68(uint64_t param_0, uint64_t param_1);
extern struct indirect_placeholder_67_ret_type indirect_placeholder_67(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint64_t param_3, uint64_t param_4, uint64_t param_5, uint64_t param_6);
extern struct indirect_placeholder_69_ret_type indirect_placeholder_69(uint64_t param_0, uint64_t param_1);
extern struct indirect_placeholder_66_ret_type indirect_placeholder_66(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint64_t param_3, uint64_t param_4, uint64_t param_5, uint64_t param_6);
extern struct indirect_placeholder_70_ret_type indirect_placeholder_70(uint64_t param_0, uint64_t param_1);
extern struct indirect_placeholder_65_ret_type indirect_placeholder_65(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint64_t param_3, uint64_t param_4, uint64_t param_5, uint64_t param_6);
uint64_t bb_touch(uint64_t rdi) {
    uint64_t var_29;
    uint64_t var_0;
    uint64_t var_1;
    uint64_t var_2;
    uint64_t *var_3;
    uint32_t *var_4;
    uint32_t *var_5;
    uint64_t *var_6;
    uint64_t var_7;
    uint64_t var_8;
    uint64_t rax_0;
    uint64_t var_39;
    struct indirect_placeholder_68_ret_type var_40;
    uint64_t var_41;
    uint64_t var_42;
    uint64_t var_43;
    uint64_t var_44;
    uint64_t var_37;
    uint64_t var_38;
    unsigned char var_34;
    uint64_t var_35;
    uint64_t var_36;
    uint64_t storemerge1;
    uint64_t local_sp_0;
    uint64_t var_13;
    uint64_t var_45;
    struct indirect_placeholder_69_ret_type var_46;
    uint64_t var_47;
    uint64_t var_48;
    uint64_t var_49;
    uint64_t var_50;
    unsigned char var_51;
    uint64_t local_sp_1;
    uint64_t var_52;
    uint64_t var_53;
    uint64_t var_54;
    struct indirect_placeholder_70_ret_type var_55;
    uint64_t var_56;
    uint64_t var_57;
    uint64_t var_58;
    uint64_t var_59;
    uint64_t var_22;
    uint64_t local_sp_2;
    uint64_t var_19;
    uint32_t var_20;
    uint64_t var_21;
    uint64_t var_16;
    uint32_t var_17;
    uint64_t var_18;
    uint32_t var_14;
    uint64_t var_15;
    uint64_t var_9;
    uint64_t var_10;
    uint64_t var_11;
    uint32_t var_12;
    uint64_t local_sp_4;
    uint64_t local_sp_3;
    uint64_t var_23;
    uint32_t _pre_pre;
    uint32_t _pre;
    uint32_t var_24;
    uint32_t var_25;
    uint64_t storemerge;
    uint64_t var_26;
    uint64_t var_27;
    uint64_t var_28;
    uint32_t var_30;
    bool var_31;
    unsigned char *var_32;
    unsigned char var_33;
    var_0 = revng_init_local_sp(0UL);
    var_1 = init_rbx();
    var_2 = init_rbp();
    *(uint64_t *)(var_0 + (-8L)) = var_2;
    *(uint64_t *)(var_0 + (-16L)) = var_1;
    var_3 = (uint64_t *)(var_0 + (-64L));
    *var_3 = rdi;
    var_4 = (uint32_t *)(var_0 + (-28L));
    *var_4 = 4294967295U;
    var_5 = (uint32_t *)(var_0 + (-32L));
    *var_5 = 0U;
    var_6 = (uint64_t *)(var_0 + (-40L));
    *var_6 = 6440640UL;
    var_7 = *var_3;
    var_8 = var_0 + (-80L);
    *(uint64_t *)var_8 = 4203012UL;
    indirect_placeholder();
    rax_0 = 1UL;
    storemerge1 = 0UL;
    local_sp_2 = var_8;
    var_25 = 4294967295U;
    storemerge = 0UL;
    if ((uint64_t)(uint32_t)var_7 == 0UL) {
        *var_4 = 1U;
    } else {
        var_9 = *var_3;
        var_10 = var_0 + (-88L);
        *(uint64_t *)var_10 = 4203080UL;
        var_11 = indirect_placeholder_9(2369UL, 438UL, var_9, 0UL);
        var_12 = (uint32_t)var_11;
        *var_4 = var_12;
        local_sp_2 = var_10;
        var_13 = var_0 + (-96L);
        *(uint64_t *)var_13 = 4203094UL;
        indirect_placeholder();
        var_14 = *(uint32_t *)var_11;
        var_15 = (uint64_t)var_14;
        local_sp_2 = var_13;
        var_16 = var_0 + (-104L);
        *(uint64_t *)var_16 = 4203106UL;
        indirect_placeholder();
        var_17 = *(uint32_t *)var_15;
        var_18 = (uint64_t)var_17;
        local_sp_2 = var_16;
        var_19 = var_0 + (-112L);
        *(uint64_t *)var_19 = 4203118UL;
        indirect_placeholder();
        var_20 = *(uint32_t *)var_18;
        var_21 = (uint64_t)var_20;
        local_sp_2 = var_19;
        if (*(unsigned char *)6440612UL != '\x01' & *(unsigned char *)6440614UL != '\x01' & var_12 != 4294967295U & (uint64_t)(var_14 + (-21)) != 0UL & (uint64_t)(var_17 + (-22)) != 0UL & (uint64_t)(var_20 + (-1)) == 0UL) {
            var_22 = var_0 + (-120L);
            *(uint64_t *)var_22 = 4203130UL;
            indirect_placeholder();
            *var_5 = *(uint32_t *)var_21;
            local_sp_2 = var_22;
        }
    }
    local_sp_3 = local_sp_2;
    local_sp_4 = local_sp_2;
    switch (*(uint32_t *)6440608UL) {
      case 2U:
        {
            *(uint64_t *)6440648UL = 1073741822UL;
        }
        break;
      case 3U:
        {
            if (*(unsigned char *)6440615UL == '\x00') {
                *var_6 = 0UL;
            }
            if (*(unsigned char *)6440614UL == '\x00') {
                _pre_pre = *var_4;
                _pre = _pre_pre;
                var_25 = _pre;
            } else {
                var_24 = *var_4;
                _pre = var_24;
                storemerge = 256UL;
                if (var_24 == 4294967295U) {
                    var_25 = _pre;
                }
            }
            if (var_25 == 1U) {
                storemerge1 = *var_3;
            }
            var_26 = *var_6;
            var_27 = (uint64_t)var_25;
            var_28 = local_sp_4 + (-8L);
            *(uint64_t *)var_28 = 4203311UL;
            var_29 = indirect_placeholder_44(storemerge, storemerge1, var_26, 4294967196UL, var_27);
            var_30 = (uint32_t)var_29;
            var_31 = ((uint64_t)var_30 == 0UL);
            var_32 = (unsigned char *)(var_0 + (-41L));
            var_33 = var_31;
            *var_32 = var_33;
            local_sp_0 = var_28;
            switch (*var_4) {
              case 0U:
                {
                    var_37 = (uint64_t)(var_30 & (-256)) | var_31;
                    var_38 = local_sp_4 + (-16L);
                    *(uint64_t *)var_38 = 4203335UL;
                    indirect_placeholder();
                    local_sp_0 = var_38;
                    rax_0 = 0UL;
                    if (var_37 != 0UL) {
                        var_39 = *var_3;
                        *(uint64_t *)(local_sp_4 + (-24L)) = 4203356UL;
                        var_40 = indirect_placeholder_68(var_39, 4UL);
                        var_41 = var_40.field_0;
                        var_42 = var_40.field_1;
                        var_43 = var_40.field_2;
                        *(uint64_t *)(local_sp_4 + (-32L)) = 4203364UL;
                        indirect_placeholder();
                        var_44 = (uint64_t)*(uint32_t *)var_43;
                        *(uint64_t *)(local_sp_4 + (-40L)) = 4203391UL;
                        indirect_placeholder_67(var_41, var_42, 0UL, 4317346UL, var_43, var_44, 0UL);
                        return rax_0;
                    }
                }
                break;
              case 1U:
                {
                    var_34 = var_33 ^ '\x01';
                    var_35 = (uint64_t)var_34;
                    var_36 = local_sp_4 + (-16L);
                    *(uint64_t *)var_36 = 4203423UL;
                    indirect_placeholder();
                    local_sp_0 = var_36;
                    if (var_34 != '\x00' & *(uint32_t *)var_35 != 9U & *(unsigned char *)6440612UL == '\x00') {
                        return rax_0;
                    }
                }
                break;
              default:
                {
                    local_sp_1 = local_sp_0;
                    if (*var_32 == '\x01') {
                        return rax_0;
                    }
                    rax_0 = 0UL;
                    if (*var_5 == 0U) {
                        var_51 = *(unsigned char *)6440612UL;
                        rax_0 = 1UL;
                        if (var_51 != '\x00') {
                            var_52 = (uint64_t)var_51;
                            var_53 = local_sp_0 + (-8L);
                            *(uint64_t *)var_53 = 4203538UL;
                            indirect_placeholder();
                            local_sp_1 = var_53;
                            if (*(uint32_t *)var_52 != 2U) {
                                return rax_0;
                            }
                        }
                        var_54 = *var_3;
                        *(uint64_t *)(local_sp_1 + (-8L)) = 4203569UL;
                        var_55 = indirect_placeholder_70(var_54, 4UL);
                        var_56 = var_55.field_0;
                        var_57 = var_55.field_1;
                        var_58 = var_55.field_2;
                        *(uint64_t *)(local_sp_1 + (-16L)) = 4203577UL;
                        indirect_placeholder();
                        var_59 = (uint64_t)*(uint32_t *)var_58;
                        *(uint64_t *)(local_sp_1 + (-24L)) = 4203604UL;
                        indirect_placeholder_65(var_56, var_57, 0UL, 4317381UL, var_58, var_59, 0UL);
                    } else {
                        var_45 = *var_3;
                        *(uint64_t *)(local_sp_0 + (-8L)) = 4203489UL;
                        var_46 = indirect_placeholder_69(var_45, 4UL);
                        var_47 = var_46.field_0;
                        var_48 = var_46.field_1;
                        var_49 = var_46.field_2;
                        var_50 = (uint64_t)*var_5;
                        *(uint64_t *)(local_sp_0 + (-16L)) = 4203520UL;
                        indirect_placeholder_66(var_47, var_48, 0UL, 4317365UL, var_49, var_50, 0UL);
                    }
                }
                break;
            }
        }
        break;
      default:
        {
            *(uint64_t *)6440664UL = 1073741822UL;
            local_sp_4 = local_sp_3;
        }
        break;
    }
}

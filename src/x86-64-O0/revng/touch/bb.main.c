typedef unsigned char __u_char;
typedef unsigned short __u_short;
typedef unsigned int __u_int;
typedef unsigned long __u_long;
typedef signed char __int8_t;
typedef unsigned char __uint8_t;
typedef short __int16_t;
typedef unsigned short __uint16_t;
typedef int __int32_t;
typedef unsigned int __uint32_t;
typedef long __int64_t;
typedef unsigned long __uint64_t;
typedef __int8_t __int_least8_t;
typedef __uint8_t __uint_least8_t;
typedef __int16_t __int_least16_t;
typedef __uint16_t __uint_least16_t;
typedef __int32_t __int_least32_t;
typedef __uint32_t __uint_least32_t;
typedef __int64_t __int_least64_t;
typedef __uint64_t __uint_least64_t;
typedef long __quad_t;
typedef unsigned long __u_quad_t;
typedef long __intmax_t;
typedef unsigned long __uintmax_t;
typedef unsigned long __dev_t;
typedef unsigned int __uid_t;
typedef unsigned int __gid_t;
typedef unsigned long __ino_t;
typedef unsigned long __ino64_t;
typedef unsigned int __mode_t;
typedef unsigned long __nlink_t;
typedef long __off_t;
typedef long __off64_t;
typedef int __pid_t;
typedef struct {
    int __val[2];
} __fsid_t;
typedef long __clock_t;
typedef unsigned long __rlim_t;
typedef unsigned long __rlim64_t;
typedef unsigned int __id_t;
typedef long __time_t;
typedef unsigned int __useconds_t;
typedef long __suseconds_t;
typedef int __daddr_t;
typedef int __key_t;
typedef int __clockid_t;
typedef void *__timer_t;
typedef long __blksize_t;
typedef long __blkcnt_t;
typedef long __blkcnt64_t;
typedef unsigned long __fsblkcnt_t;
typedef unsigned long __fsblkcnt64_t;
typedef unsigned long __fsfilcnt_t;
typedef unsigned long __fsfilcnt64_t;
typedef long __fsword_t;
typedef long __ssize_t;
typedef long __syscall_slong_t;
typedef unsigned long __syscall_ulong_t;
typedef __off64_t __loff_t;
typedef char *__caddr_t;
typedef long __intptr_t;
typedef unsigned int __socklen_t;
typedef int __sig_atomic_t;
typedef __int8_t int8_t;
typedef __int16_t int16_t;
typedef __int32_t int32_t;
typedef __int64_t int64_t;
typedef __uint8_t uint8_t;
typedef __uint16_t uint16_t;
typedef __uint32_t uint32_t;
typedef __uint64_t uint64_t;
typedef __int_least8_t int_least8_t;
typedef __int_least16_t int_least16_t;
typedef __int_least32_t int_least32_t;
typedef __int_least64_t int_least64_t;
typedef __uint_least8_t uint_least8_t;
typedef __uint_least16_t uint_least16_t;
typedef __uint_least32_t uint_least32_t;
typedef __uint_least64_t uint_least64_t;
typedef signed char int_fast8_t;
typedef long int_fast16_t;
typedef long int_fast32_t;
typedef long int_fast64_t;
typedef unsigned char uint_fast8_t;
typedef unsigned long uint_fast16_t;
typedef unsigned long uint_fast32_t;
typedef unsigned long uint_fast64_t;
typedef long intptr_t;
typedef unsigned long uintptr_t;
typedef __intmax_t intmax_t;
typedef __uintmax_t uintmax_t;
typedef _Bool bool;
struct indirect_placeholder_46_ret_type;
struct indirect_placeholder_47_ret_type;
struct indirect_placeholder_48_ret_type;
struct indirect_placeholder_50_ret_type;
struct indirect_placeholder_51_ret_type;
struct indirect_placeholder_54_ret_type;
struct indirect_placeholder_53_ret_type;
struct indirect_placeholder_55_ret_type;
struct indirect_placeholder_52_ret_type;
struct indirect_placeholder_56_ret_type;
struct indirect_placeholder_49_ret_type;
struct indirect_placeholder_57_ret_type;
struct indirect_placeholder_58_ret_type;
struct indirect_placeholder_60_ret_type;
struct indirect_placeholder_59_ret_type;
struct indirect_placeholder_62_ret_type;
struct indirect_placeholder_63_ret_type;
struct indirect_placeholder_46_ret_type {
    uint64_t field_0;
    uint64_t field_1;
    uint64_t field_2;
};
struct indirect_placeholder_47_ret_type {
    uint64_t field_0;
    uint64_t field_1;
    uint64_t field_2;
    uint64_t field_3;
};
struct indirect_placeholder_48_ret_type {
    uint64_t field_0;
    uint64_t field_1;
    uint64_t field_2;
};
struct indirect_placeholder_50_ret_type {
    uint64_t field_0;
    uint64_t field_1;
    uint64_t field_2;
};
struct indirect_placeholder_51_ret_type {
    uint64_t field_0;
    uint64_t field_1;
    uint64_t field_2;
};
struct indirect_placeholder_54_ret_type {
    uint64_t field_0;
    uint64_t field_1;
    uint64_t field_2;
};
struct indirect_placeholder_53_ret_type {
    uint64_t field_0;
    uint64_t field_1;
    uint64_t field_2;
};
struct indirect_placeholder_55_ret_type {
    uint64_t field_0;
    uint64_t field_1;
};
struct indirect_placeholder_52_ret_type {
    uint64_t field_0;
    uint64_t field_1;
};
struct indirect_placeholder_56_ret_type {
    uint64_t field_0;
    uint64_t field_1;
    uint64_t field_2;
};
struct indirect_placeholder_49_ret_type {
    uint64_t field_0;
    uint64_t field_1;
    uint64_t field_2;
};
struct indirect_placeholder_57_ret_type {
    uint64_t field_0;
    uint64_t field_1;
};
struct indirect_placeholder_58_ret_type {
    uint64_t field_0;
    uint64_t field_1;
    uint64_t field_2;
};
struct indirect_placeholder_60_ret_type {
    uint64_t field_0;
    uint64_t field_1;
    uint64_t field_2;
};
struct indirect_placeholder_59_ret_type {
    uint64_t field_0;
    uint64_t field_1;
    uint64_t field_2;
};
struct indirect_placeholder_62_ret_type {
    uint64_t field_0;
    uint64_t field_1;
    uint64_t field_2;
    uint64_t field_3;
};
struct indirect_placeholder_63_ret_type {
    uint64_t field_0;
    uint64_t field_1;
    uint64_t field_2;
    uint64_t field_3;
};
extern void abort(void);
extern uint64_t revng_init_local_sp(uint64_t param_0);
extern uint64_t init_rbp(void);
extern void indirect_placeholder(void);
extern void indirect_placeholder_35(uint64_t param_0, uint64_t param_1);
extern uint64_t indirect_placeholder_20(void);
extern uint64_t indirect_placeholder_19(uint64_t param_0);
extern uint64_t init_rbx(void);
extern void indirect_placeholder_61(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint64_t param_3, uint64_t param_4, uint64_t param_5);
extern void indirect_placeholder_23(uint64_t param_0);
extern uint64_t indirect_placeholder_16(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint64_t param_3, uint64_t param_4, uint64_t param_5);
extern struct indirect_placeholder_46_ret_type indirect_placeholder_46(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint64_t param_3, uint64_t param_4, uint64_t param_5, uint64_t param_6);
extern struct indirect_placeholder_47_ret_type indirect_placeholder_47(uint64_t param_0, uint64_t param_1, uint64_t param_2);
extern struct indirect_placeholder_48_ret_type indirect_placeholder_48(uint64_t param_0, uint64_t param_1, uint64_t param_2);
extern struct indirect_placeholder_50_ret_type indirect_placeholder_50(uint64_t param_0, uint64_t param_1, uint64_t param_2);
extern struct indirect_placeholder_51_ret_type indirect_placeholder_51(uint64_t param_0, uint64_t param_1, uint64_t param_2);
extern struct indirect_placeholder_54_ret_type indirect_placeholder_54(uint64_t param_0, uint64_t param_1);
extern struct indirect_placeholder_53_ret_type indirect_placeholder_53(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint64_t param_3, uint64_t param_4, uint64_t param_5, uint64_t param_6);
extern struct indirect_placeholder_55_ret_type indirect_placeholder_55(uint64_t param_0);
extern struct indirect_placeholder_52_ret_type indirect_placeholder_52(uint64_t param_0);
extern struct indirect_placeholder_56_ret_type indirect_placeholder_56(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint64_t param_3, uint64_t param_4, uint64_t param_5, uint64_t param_6);
extern struct indirect_placeholder_49_ret_type indirect_placeholder_49(uint64_t param_0, uint64_t param_1, uint64_t param_2);
extern struct indirect_placeholder_57_ret_type indirect_placeholder_57(void);
extern struct indirect_placeholder_58_ret_type indirect_placeholder_58(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint64_t param_3, uint64_t param_4, uint64_t param_5, uint64_t param_6);
extern struct indirect_placeholder_60_ret_type indirect_placeholder_60(uint64_t param_0);
extern struct indirect_placeholder_59_ret_type indirect_placeholder_59(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint64_t param_3, uint64_t param_4, uint64_t param_5, uint64_t param_6);
extern struct indirect_placeholder_62_ret_type indirect_placeholder_62(uint64_t param_0, uint64_t param_1, uint64_t param_2);
extern struct indirect_placeholder_63_ret_type indirect_placeholder_63(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint64_t param_3, uint64_t param_4);
void bb_main(uint64_t rsi, uint64_t rdi) {
    struct indirect_placeholder_46_ret_type var_111;
    struct indirect_placeholder_63_ret_type var_16;
    uint64_t var_0;
    uint64_t var_1;
    uint64_t var_2;
    uint64_t var_3;
    uint32_t *var_4;
    uint64_t var_5;
    uint64_t *var_6;
    unsigned char *var_7;
    unsigned char *var_8;
    uint64_t *var_9;
    uint64_t var_10;
    uint64_t var_11;
    uint32_t *var_12;
    uint64_t local_sp_10;
    uint64_t r9_4;
    uint64_t var_103;
    uint64_t var_104;
    uint64_t var_105;
    uint64_t var_106;
    uint64_t var_107;
    uint64_t var_108;
    uint64_t var_109;
    uint64_t var_110;
    uint64_t var_100;
    uint64_t var_101;
    uint64_t var_102;
    uint64_t local_sp_0;
    uint64_t r9_0;
    uint64_t r8_0;
    uint64_t rcx_0;
    uint64_t r9_3;
    uint64_t local_sp_6;
    uint64_t var_98;
    uint64_t var_99;
    uint64_t var_91;
    uint64_t var_92;
    struct indirect_placeholder_47_ret_type var_93;
    uint64_t var_94;
    uint64_t var_95;
    uint64_t var_96;
    uint64_t var_97;
    uint64_t local_sp_5;
    uint64_t var_74;
    uint64_t var_75;
    uint64_t *var_76;
    uint64_t var_77;
    uint64_t var_78;
    uint64_t *var_79;
    uint64_t var_80;
    uint64_t *var_81;
    uint64_t var_82;
    uint64_t var_83;
    uint64_t var_84;
    struct indirect_placeholder_48_ret_type var_85;
    uint64_t var_86;
    uint64_t var_87;
    uint64_t var_88;
    uint32_t var_59;
    uint32_t var_62;
    uint64_t var_60;
    struct indirect_placeholder_50_ret_type var_61;
    uint64_t local_sp_1;
    uint64_t r8_3;
    uint64_t r9_1;
    uint64_t rcx_3;
    uint64_t r8_1;
    uint64_t rcx_1;
    uint64_t var_63;
    uint64_t var_64;
    struct indirect_placeholder_51_ret_type var_65;
    uint64_t storemerge;
    uint64_t local_sp_4;
    uint64_t var_44;
    struct indirect_placeholder_54_ret_type var_45;
    uint64_t var_46;
    uint64_t var_47;
    uint64_t var_48;
    uint64_t var_49;
    uint64_t var_50;
    struct indirect_placeholder_53_ret_type var_51;
    uint64_t var_52;
    struct indirect_placeholder_55_ret_type var_53;
    uint64_t var_54;
    uint64_t var_55;
    struct indirect_placeholder_52_ret_type var_56;
    uint64_t var_57;
    uint64_t var_58;
    uint64_t var_34;
    uint64_t var_35;
    uint64_t var_36;
    unsigned char var_37;
    bool var_38;
    uint64_t var_39;
    uint64_t var_40;
    uint64_t *var_41;
    uint64_t var_42;
    uint64_t var_43;
    uint64_t var_66;
    uint64_t var_67;
    uint64_t var_68;
    struct indirect_placeholder_49_ret_type var_69;
    uint64_t var_70;
    uint64_t var_71;
    uint64_t var_72;
    uint64_t var_73;
    uint64_t r8_4;
    uint64_t rcx_4;
    uint64_t var_89;
    struct indirect_placeholder_57_ret_type var_90;
    uint64_t r9_5;
    uint64_t r8_5;
    uint64_t rcx_5;
    uint32_t var_112;
    uint32_t var_113;
    uint32_t var_114;
    uint32_t var_115;
    uint64_t local_sp_7;
    uint64_t var_116;
    uint64_t var_117;
    uint64_t var_118;
    uint32_t var_119;
    uint64_t local_sp_10_be;
    uint64_t var_28;
    struct indirect_placeholder_60_ret_type var_29;
    uint64_t var_30;
    uint64_t var_31;
    uint64_t var_32;
    uint64_t var_33;
    uint64_t local_sp_8;
    uint64_t local_sp_9;
    uint64_t var_18;
    uint64_t var_19;
    uint64_t *var_20;
    uint64_t var_21;
    uint64_t var_22;
    uint64_t var_23;
    uint64_t var_24;
    uint64_t var_25;
    uint64_t var_26;
    struct indirect_placeholder_62_ret_type var_27;
    uint64_t var_13;
    uint64_t var_14;
    uint64_t var_15;
    uint32_t var_17;
    unsigned int loop_state_var;
    var_0 = revng_init_local_sp(0UL);
    var_1 = init_rbx();
    var_2 = init_rbp();
    var_3 = var_0 + (-8L);
    *(uint64_t *)var_3 = var_2;
    *(uint64_t *)(var_0 + (-16L)) = var_1;
    var_4 = (uint32_t *)(var_0 + (-252L));
    *var_4 = (uint32_t)rdi;
    var_5 = var_0 + (-264L);
    var_6 = (uint64_t *)var_5;
    *var_6 = rsi;
    var_7 = (unsigned char *)(var_0 + (-25L));
    *var_7 = (unsigned char)'\x00';
    var_8 = (unsigned char *)(var_0 + (-26L));
    *var_8 = (unsigned char)'\x01';
    var_9 = (uint64_t *)(var_0 + (-40L));
    *var_9 = 0UL;
    var_10 = **(uint64_t **)var_5;
    *(uint64_t *)(var_0 + (-272L)) = 4203926UL;
    indirect_placeholder_23(var_10);
    *(uint64_t *)(var_0 + (-280L)) = 4203941UL;
    indirect_placeholder();
    var_11 = var_0 + (-288L);
    *(uint64_t *)var_11 = 4203951UL;
    indirect_placeholder();
    *(uint32_t *)6440608UL = 0U;
    *(unsigned char *)6440613UL = (unsigned char)'\x00';
    *(unsigned char *)6440612UL = (unsigned char)'\x00';
    var_12 = (uint32_t *)(var_0 + (-44L));
    var_37 = (unsigned char)'\x00';
    local_sp_10 = var_11;
    while (1U)
        {
            var_13 = *var_6;
            var_14 = (uint64_t)*var_4;
            var_15 = local_sp_10 + (-8L);
            *(uint64_t *)var_15 = 4204538UL;
            var_16 = indirect_placeholder_63(0UL, 4318853UL, 4316928UL, var_13, var_14);
            var_17 = (uint32_t)var_16.field_2;
            *var_12 = var_17;
            local_sp_5 = var_15;
            local_sp_9 = var_15;
            local_sp_10_be = var_15;
            if (var_17 != 4294967295U) {
                var_34 = var_16.field_0;
                var_35 = var_16.field_1;
                var_36 = var_16.field_3;
                r9_4 = var_34;
                r9_3 = var_34;
                r8_3 = var_35;
                rcx_3 = var_36;
                r8_4 = var_35;
                rcx_4 = var_36;
                if (*(uint32_t *)6440608UL != 0U) {
                    loop_state_var = 0U;
                    break;
                }
                *(uint32_t *)6440608UL = 3U;
                loop_state_var = 0U;
                break;
            }
            if ((uint64_t)(var_17 + (-102)) != 0UL) {
                if ((int)var_17 > (int)102U) {
                    if ((uint64_t)(var_17 + (-114)) == 0UL) {
                        *(unsigned char *)6440613UL = (unsigned char)'\x01';
                        *(uint64_t *)6440672UL = *(uint64_t *)6443296UL;
                    } else {
                        if ((int)var_17 > (int)114U) {
                            if ((uint64_t)(var_17 + (-116)) == 0UL) {
                                var_25 = *(uint64_t *)6443296UL;
                                var_26 = local_sp_10 + (-16L);
                                *(uint64_t *)var_26 = 4204232UL;
                                var_27 = indirect_placeholder_62(6UL, var_25, 6440640UL);
                                local_sp_8 = var_26;
                                if ((uint64_t)(unsigned char)var_27.field_2 != 1UL) {
                                    var_28 = *(uint64_t *)6443296UL;
                                    *(uint64_t *)(local_sp_10 + (-24L)) = 4204254UL;
                                    var_29 = indirect_placeholder_60(var_28);
                                    var_30 = var_29.field_0;
                                    var_31 = var_29.field_1;
                                    var_32 = var_29.field_2;
                                    var_33 = local_sp_10 + (-32L);
                                    *(uint64_t *)var_33 = 4204282UL;
                                    indirect_placeholder_59(var_30, var_31, 0UL, 4317284UL, var_32, 0UL, 1UL);
                                    local_sp_8 = var_33;
                                }
                                *(uint64_t *)6440648UL = 0UL;
                                *(uint64_t *)6440656UL = *(uint64_t *)6440640UL;
                                *(uint64_t *)6440664UL = 0UL;
                                *var_7 = (unsigned char)'\x01';
                                local_sp_10_be = local_sp_8;
                            } else {
                                if ((uint64_t)(var_17 + (-128)) != 0UL) {
                                    loop_state_var = 1U;
                                    break;
                                }
                                var_21 = *(uint64_t *)6440232UL;
                                var_22 = *(uint64_t *)6443296UL;
                                var_23 = local_sp_10 + (-16L);
                                *(uint64_t *)var_23 = 4204376UL;
                                var_24 = indirect_placeholder_16(var_21, 4UL, 4317216UL, 4317264UL, var_22, 4318780UL);
                                *(uint32_t *)6440608UL = (*(uint32_t *)6440608UL | *(uint32_t *)((var_24 << 2UL) + 4317264UL));
                                local_sp_10_be = var_23;
                            }
                        } else {
                            if ((uint64_t)(var_17 + (-104)) == 0UL) {
                                *(unsigned char *)6440614UL = (unsigned char)'\x01';
                            } else {
                                if ((uint64_t)(var_17 + (-109)) != 0UL) {
                                    loop_state_var = 1U;
                                    break;
                                }
                                *(uint32_t *)6440608UL = (*(uint32_t *)6440608UL | 2U);
                            }
                        }
                    }
                } else {
                    if ((uint64_t)(var_17 + (-97)) == 0UL) {
                        *(uint32_t *)6440608UL = (*(uint32_t *)6440608UL | 1U);
                    } else {
                        if ((int)var_17 <= (int)97U) {
                            if ((uint64_t)(var_17 + 131U) != 0UL) {
                                if ((uint64_t)(var_17 + 130U) != 0UL) {
                                    loop_state_var = 1U;
                                    break;
                                }
                                *(uint64_t *)(local_sp_10 + (-16L)) = 4204409UL;
                                indirect_placeholder_35(var_3, 0UL);
                                abort();
                            }
                            var_18 = *(uint64_t *)6440224UL;
                            *(uint64_t *)(local_sp_10 + (-16L)) = 0UL;
                            var_19 = local_sp_10 + (-24L);
                            var_20 = (uint64_t *)var_19;
                            *var_20 = 4318813UL;
                            *(uint64_t *)(local_sp_10 + (-32L)) = 4318825UL;
                            *(uint64_t *)(local_sp_10 + (-40L)) = 4318841UL;
                            *(uint64_t *)(local_sp_10 + (-48L)) = 4204478UL;
                            indirect_placeholder_61(4318787UL, 4318802UL, 0UL, 4316640UL, var_18, 4318774UL);
                            *var_20 = 4204492UL;
                            indirect_placeholder();
                            local_sp_9 = var_19;
                            loop_state_var = 1U;
                            break;
                        }
                        if ((uint64_t)(var_17 + (-99)) == 0UL) {
                            *(unsigned char *)6440612UL = (unsigned char)'\x01';
                        } else {
                            if ((uint64_t)(var_17 + (-100)) != 0UL) {
                                loop_state_var = 1U;
                                break;
                            }
                            *var_9 = *(uint64_t *)6443296UL;
                        }
                    }
                }
            }
            local_sp_10 = local_sp_10_be;
            continue;
        }
    switch (loop_state_var) {
      case 1U:
        {
            *(uint64_t *)(local_sp_9 + (-8L)) = 4204502UL;
            indirect_placeholder_35(var_3, 1UL);
            abort();
        }
        break;
      case 0U:
        {
            if (*var_7 != '\x00') {
                if (*(unsigned char *)6440613UL == '\x00') {
                    *(uint64_t *)(local_sp_10 + (-16L)) = 4204620UL;
                    indirect_placeholder_56(var_34, var_35, 0UL, 4318872UL, var_36, 0UL, 0UL);
                    *(uint64_t *)(local_sp_10 + (-24L)) = 4204630UL;
                    indirect_placeholder_35(var_3, 1UL);
                    abort();
                }
                if (*var_9 == 0UL) {
                    *(uint64_t *)(local_sp_10 + (-16L)) = 4204620UL;
                    indirect_placeholder_56(var_34, var_35, 0UL, 4318872UL, var_36, 0UL, 0UL);
                    *(uint64_t *)(local_sp_10 + (-24L)) = 4204630UL;
                    indirect_placeholder_35(var_3, 1UL);
                    abort();
                }
            }
            var_37 = *(unsigned char *)6440613UL;
        }
        break;
    }
}

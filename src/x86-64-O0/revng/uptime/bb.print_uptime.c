typedef unsigned char __u_char;
typedef unsigned short __u_short;
typedef unsigned int __u_int;
typedef unsigned long __u_long;
typedef signed char __int8_t;
typedef unsigned char __uint8_t;
typedef short __int16_t;
typedef unsigned short __uint16_t;
typedef int __int32_t;
typedef unsigned int __uint32_t;
typedef long __int64_t;
typedef unsigned long __uint64_t;
typedef __int8_t __int_least8_t;
typedef __uint8_t __uint_least8_t;
typedef __int16_t __int_least16_t;
typedef __uint16_t __uint_least16_t;
typedef __int32_t __int_least32_t;
typedef __uint32_t __uint_least32_t;
typedef __int64_t __int_least64_t;
typedef __uint64_t __uint_least64_t;
typedef long __quad_t;
typedef unsigned long __u_quad_t;
typedef long __intmax_t;
typedef unsigned long __uintmax_t;
typedef unsigned long __dev_t;
typedef unsigned int __uid_t;
typedef unsigned int __gid_t;
typedef unsigned long __ino_t;
typedef unsigned long __ino64_t;
typedef unsigned int __mode_t;
typedef unsigned long __nlink_t;
typedef long __off_t;
typedef long __off64_t;
typedef int __pid_t;
typedef struct {
    int __val[2];
} __fsid_t;
typedef long __clock_t;
typedef unsigned long __rlim_t;
typedef unsigned long __rlim64_t;
typedef unsigned int __id_t;
typedef long __time_t;
typedef unsigned int __useconds_t;
typedef long __suseconds_t;
typedef int __daddr_t;
typedef int __key_t;
typedef int __clockid_t;
typedef void *__timer_t;
typedef long __blksize_t;
typedef long __blkcnt_t;
typedef long __blkcnt64_t;
typedef unsigned long __fsblkcnt_t;
typedef unsigned long __fsblkcnt64_t;
typedef unsigned long __fsfilcnt_t;
typedef unsigned long __fsfilcnt64_t;
typedef long __fsword_t;
typedef long __ssize_t;
typedef long __syscall_slong_t;
typedef unsigned long __syscall_ulong_t;
typedef __off64_t __loff_t;
typedef char *__caddr_t;
typedef long __intptr_t;
typedef unsigned int __socklen_t;
typedef int __sig_atomic_t;
typedef __int8_t int8_t;
typedef __int16_t int16_t;
typedef __int32_t int32_t;
typedef __int64_t int64_t;
typedef __uint8_t uint8_t;
typedef __uint16_t uint16_t;
typedef __uint32_t uint32_t;
typedef __uint64_t uint64_t;
typedef __int_least8_t int_least8_t;
typedef __int_least16_t int_least16_t;
typedef __int_least32_t int_least32_t;
typedef __int_least64_t int_least64_t;
typedef __uint_least8_t uint_least8_t;
typedef __uint_least16_t uint_least16_t;
typedef __uint_least32_t uint_least32_t;
typedef __uint_least64_t uint_least64_t;
typedef signed char int_fast8_t;
typedef long int_fast16_t;
typedef long int_fast32_t;
typedef long int_fast64_t;
typedef unsigned char uint_fast8_t;
typedef unsigned long uint_fast16_t;
typedef unsigned long uint_fast32_t;
typedef unsigned long uint_fast64_t;
typedef long intptr_t;
typedef unsigned long uintptr_t;
typedef __intmax_t intmax_t;
typedef __uintmax_t uintmax_t;
typedef _Bool bool;
extern uint64_t revng_init_local_sp(uint64_t param_0);
extern uint64_t init_rbp(void);
extern uint64_t indirect_placeholder(uint64_t param_0, uint64_t param_1);
extern void indirect_placeholder_1(void);
extern uint64_t init_r9(void);
extern uint64_t init_r8(void);
extern uint64_t indirect_placeholder_21(void);
extern uint64_t indirect_placeholder_24(uint64_t param_0);
extern void indirect_placeholder_50(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint64_t param_3, uint64_t param_4, uint64_t param_5, uint64_t param_6);
extern uint64_t init_rcx(void);
void bb_print_uptime(uint64_t rdi, uint64_t rsi) {
    uint64_t var_0;
    uint64_t var_1;
    uint64_t var_2;
    uint64_t var_3;
    uint64_t var_4;
    uint64_t *var_5;
    uint64_t var_6;
    uint64_t *var_7;
    uint64_t *var_8;
    uint64_t *var_9;
    uint64_t *var_10;
    uint64_t var_40;
    uint32_t var_41;
    uint64_t local_sp_0;
    uint32_t var_43;
    uint64_t var_42;
    uint64_t local_sp_1;
    uint32_t var_45;
    uint64_t var_44;
    uint64_t local_sp_2;
    uint64_t local_sp_3;
    uint64_t var_36;
    uint64_t var_37;
    uint32_t *var_38;
    uint32_t var_39;
    uint64_t var_35;
    uint64_t var_30;
    bool var_31;
    uint64_t var_32;
    uint64_t *var_33;
    uint64_t var_34;
    uint64_t var_16;
    uint64_t var_17;
    uint64_t var_18;
    uint64_t var_19;
    uint64_t local_sp_5;
    uint64_t var_20;
    uint64_t var_21;
    uint64_t *var_22;
    uint64_t var_23;
    uint32_t var_24;
    uint64_t var_25;
    uint64_t var_26;
    bool var_27;
    uint64_t var_28;
    uint64_t *var_29;
    uint64_t var_11;
    uint64_t var_13;
    uint64_t *var_14;
    uint64_t var_15;
    uint16_t **_pre89_pre_phi;
    uint16_t **_pre_phi;
    uint16_t **_pre90;
    uint16_t **var_12;
    uint64_t storemerge;
    var_0 = revng_init_local_sp(0UL);
    var_1 = init_rbp();
    var_2 = init_rcx();
    var_3 = init_r9();
    var_4 = init_r8();
    *(uint64_t *)(var_0 + (-8L)) = var_1;
    var_5 = (uint64_t *)(var_0 + (-112L));
    *var_5 = rdi;
    var_6 = var_0 + (-120L);
    var_7 = (uint64_t *)var_6;
    *var_7 = rsi;
    var_8 = (uint64_t *)(var_0 + (-16L));
    *var_8 = 0UL;
    var_9 = (uint64_t *)(var_0 + (-24L));
    *var_9 = 0UL;
    var_10 = (uint64_t *)(var_0 + (-32L));
    *var_10 = 0UL;
    var_19 = 0UL;
    storemerge = 0UL;
    var_11 = *var_5;
    *var_5 = (var_11 + (-1L));
    while (var_11 != 0UL)
        {
            if (*(unsigned char *)(*var_7 + 44UL) == '\x00') {
                _pre90 = (uint16_t **)var_6;
                _pre89_pre_phi = _pre90;
                _pre_phi = _pre89_pre_phi;
            } else {
                var_12 = (uint16_t **)var_6;
                _pre89_pre_phi = var_12;
                _pre_phi = var_12;
                storemerge = 1UL;
                if (**var_12 == (unsigned short)7U) {
                    _pre_phi = _pre89_pre_phi;
                }
            }
            *var_8 = (*var_8 + storemerge);
            if (**_pre_phi == (unsigned short)2U) {
                *var_9 = *(uint64_t *)(*var_7 + 344UL);
            }
            *var_7 = (*var_7 + 400UL);
            var_11 = *var_5;
            *var_5 = (var_11 + (-1L));
        }
    var_13 = var_0 + (-144L);
    *(uint64_t *)var_13 = 4202572UL;
    indirect_placeholder_1();
    var_14 = (uint64_t *)(var_0 + (-72L));
    *var_14 = 0UL;
    var_15 = *var_9;
    var_18 = var_15;
    local_sp_5 = var_13;
    if (var_15 == 0UL) {
        *(uint64_t *)(var_0 + (-152L)) = 4202588UL;
        indirect_placeholder_1();
        var_16 = (uint64_t)*(volatile uint32_t *)(uint32_t *)0UL;
        var_17 = var_0 + (-160L);
        *(uint64_t *)var_17 = 4202612UL;
        indirect_placeholder_50(0UL, 4284384UL, var_2, 1UL, var_16, var_3, var_4);
        var_18 = *var_9;
        var_19 = *var_14;
        local_sp_5 = var_17;
    }
    var_20 = var_19 - var_18;
    *var_10 = var_20;
    var_21 = (uint64_t)((long)(uint64_t)(((unsigned __int128)var_20 * 1749024623285053783ULL) >> 64ULL) >> (long)13UL) - (uint64_t)((long)var_20 >> (long)63UL);
    var_22 = (uint64_t *)(var_0 + (-40L));
    *var_22 = var_21;
    var_23 = *var_10 + (var_21 * 18446744073709465216UL);
    var_24 = (uint32_t)(uint64_t)(((unsigned __int128)var_23 * 5247073869855161349ULL) >> 74ULL) - (uint32_t)(uint64_t)((long)var_23 >> (long)63UL);
    *(uint32_t *)(var_0 + (-44L)) = var_24;
    var_25 = ((*var_22 * 18446744073709465216UL) + *var_10) - (uint64_t)((long)((uint64_t)var_24 * 15461882265600UL) >> (long)32UL);
    *(uint32_t *)(var_0 + (-48L)) = ((uint32_t)((var_25 + (uint64_t)(((unsigned __int128)var_25 * 18446744073709551615ULL) >> 64ULL)) >> 5UL) - (uint32_t)(uint64_t)((long)var_25 >> (long)63UL));
    *(uint64_t *)(local_sp_5 + (-8L)) = 4202810UL;
    var_26 = indirect_placeholder_21();
    *(uint64_t *)(var_0 + (-56L)) = var_26;
    var_27 = (var_26 == 0UL);
    var_28 = local_sp_5 + (-16L);
    var_29 = (uint64_t *)var_28;
    if (var_27) {
        *var_29 = 4202873UL;
        indirect_placeholder_1();
    } else {
        *var_29 = 4202856UL;
        indirect_placeholder(0UL, 0UL);
    }
    if (*var_10 == 18446744073709551615UL) {
        var_35 = var_28 + (-8L);
        *(uint64_t *)var_35 = 4202895UL;
        indirect_placeholder_1();
        local_sp_3 = var_35;
    } else {
        var_30 = *var_22;
        var_31 = ((long)var_30 > (long)0UL);
        var_32 = var_28 + (-8L);
        var_33 = (uint64_t *)var_32;
        local_sp_3 = var_32;
        if (var_31) {
            *var_33 = 4202916UL;
            indirect_placeholder_24(var_30);
            var_34 = var_28 + (-16L);
            *(uint64_t *)var_34 = 4202957UL;
            indirect_placeholder_1();
            local_sp_3 = var_34;
        } else {
            *var_33 = 4202982UL;
            indirect_placeholder_1();
        }
    }
    var_36 = *var_8;
    *(uint64_t *)(local_sp_3 + (-8L)) = 4202994UL;
    indirect_placeholder_24(var_36);
    *(uint64_t *)(local_sp_3 + (-16L)) = 4203032UL;
    indirect_placeholder_1();
    var_37 = local_sp_3 + (-24L);
    *(uint64_t *)var_37 = 4203049UL;
    indirect_placeholder_1();
    var_38 = (uint32_t *)(var_0 + (-60L));
    var_39 = (uint32_t)var_0 + (-104);
    *var_38 = var_39;
    var_41 = var_39;
    local_sp_0 = var_37;
    if ((int)var_39 > (int)0U) {
        *(uint64_t *)(var_0 + (-128L)) = *(uint64_t *)(var_0 + (-104L));
        var_40 = local_sp_3 + (-32L);
        *(uint64_t *)var_40 = 4203104UL;
        indirect_placeholder_1();
        var_41 = *var_38;
        local_sp_0 = var_40;
    }
    var_43 = var_41;
    local_sp_1 = local_sp_0;
    if ((int)var_41 > (int)1U) {
        *(uint64_t *)(var_0 + (-128L)) = *(uint64_t *)(var_0 + (-96L));
        var_42 = local_sp_0 + (-8L);
        *(uint64_t *)var_42 = 4203138UL;
        indirect_placeholder_1();
        var_43 = *var_38;
        local_sp_1 = var_42;
    }
    var_45 = var_43;
    local_sp_2 = local_sp_1;
    if ((int)var_43 > (int)2U) {
        *(uint64_t *)(var_0 + (-128L)) = *(uint64_t *)(var_0 + (-88L));
        var_44 = local_sp_1 + (-8L);
        *(uint64_t *)var_44 = 4203172UL;
        indirect_placeholder_1();
        var_45 = *var_38;
        local_sp_2 = var_44;
    }
    if ((int)var_45 <= (int)0U) {
        return;
    }
    *(uint64_t *)(local_sp_2 + (-8L)) = 4203188UL;
    indirect_placeholder_1();
    return;
}

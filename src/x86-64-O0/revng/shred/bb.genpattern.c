typedef unsigned char __u_char;
typedef unsigned short __u_short;
typedef unsigned int __u_int;
typedef unsigned long __u_long;
typedef signed char __int8_t;
typedef unsigned char __uint8_t;
typedef short __int16_t;
typedef unsigned short __uint16_t;
typedef int __int32_t;
typedef unsigned int __uint32_t;
typedef long __int64_t;
typedef unsigned long __uint64_t;
typedef __int8_t __int_least8_t;
typedef __uint8_t __uint_least8_t;
typedef __int16_t __int_least16_t;
typedef __uint16_t __uint_least16_t;
typedef __int32_t __int_least32_t;
typedef __uint32_t __uint_least32_t;
typedef __int64_t __int_least64_t;
typedef __uint64_t __uint_least64_t;
typedef long __quad_t;
typedef unsigned long __u_quad_t;
typedef long __intmax_t;
typedef unsigned long __uintmax_t;
typedef unsigned long __dev_t;
typedef unsigned int __uid_t;
typedef unsigned int __gid_t;
typedef unsigned long __ino_t;
typedef unsigned long __ino64_t;
typedef unsigned int __mode_t;
typedef unsigned long __nlink_t;
typedef long __off_t;
typedef long __off64_t;
typedef int __pid_t;
typedef struct {
    int __val[2];
} __fsid_t;
typedef long __clock_t;
typedef unsigned long __rlim_t;
typedef unsigned long __rlim64_t;
typedef unsigned int __id_t;
typedef long __time_t;
typedef unsigned int __useconds_t;
typedef long __suseconds_t;
typedef int __daddr_t;
typedef int __key_t;
typedef int __clockid_t;
typedef void *__timer_t;
typedef long __blksize_t;
typedef long __blkcnt_t;
typedef long __blkcnt64_t;
typedef unsigned long __fsblkcnt_t;
typedef unsigned long __fsblkcnt64_t;
typedef unsigned long __fsfilcnt_t;
typedef unsigned long __fsfilcnt64_t;
typedef long __fsword_t;
typedef long __ssize_t;
typedef long __syscall_slong_t;
typedef unsigned long __syscall_ulong_t;
typedef __off64_t __loff_t;
typedef char *__caddr_t;
typedef long __intptr_t;
typedef unsigned int __socklen_t;
typedef int __sig_atomic_t;
typedef __int8_t int8_t;
typedef __int16_t int16_t;
typedef __int32_t int32_t;
typedef __int64_t int64_t;
typedef __uint8_t uint8_t;
typedef __uint16_t uint16_t;
typedef __uint32_t uint32_t;
typedef __uint64_t uint64_t;
typedef __int_least8_t int_least8_t;
typedef __int_least16_t int_least16_t;
typedef __int_least32_t int_least32_t;
typedef __int_least64_t int_least64_t;
typedef __uint_least8_t uint_least8_t;
typedef __uint_least16_t uint_least16_t;
typedef __uint_least32_t uint_least32_t;
typedef __uint_least64_t uint_least64_t;
typedef signed char int_fast8_t;
typedef long int_fast16_t;
typedef long int_fast32_t;
typedef long int_fast64_t;
typedef unsigned char uint_fast8_t;
typedef unsigned long uint_fast16_t;
typedef unsigned long uint_fast32_t;
typedef unsigned long uint_fast64_t;
typedef long intptr_t;
typedef unsigned long uintptr_t;
typedef __intmax_t intmax_t;
typedef __uintmax_t uintmax_t;
struct indirect_placeholder_19_ret_type;
struct indirect_placeholder_20_ret_type;
struct indirect_placeholder_19_ret_type {
    uint64_t field_0;
    uint64_t field_1;
    uint64_t field_2;
    uint64_t field_3;
    uint64_t field_4;
    uint64_t field_5;
};
struct indirect_placeholder_20_ret_type {
    uint64_t field_0;
    uint64_t field_1;
    uint64_t field_2;
    uint64_t field_3;
    uint64_t field_4;
    uint64_t field_5;
};
extern uint64_t revng_init_local_sp(uint64_t param_0);
extern uint64_t init_rbp(void);
extern uint64_t init_cc_src2(void);
extern uint64_t helper_cc_compute_c_wrapper(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint32_t param_3);
extern void indirect_placeholder(void);
extern struct indirect_placeholder_19_ret_type indirect_placeholder_19(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint64_t param_3, uint64_t param_4, uint64_t param_5);
extern struct indirect_placeholder_20_ret_type indirect_placeholder_20(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint64_t param_3, uint64_t param_4, uint64_t param_5);
void bb_genpattern(uint64_t rdx, uint64_t rcx, uint64_t rdi, uint64_t rsi, uint64_t r10, uint64_t r9, uint64_t r8, uint64_t rbx) {
    uint64_t local_sp_0;
    uint64_t var_0;
    uint64_t var_1;
    uint64_t var_2;
    uint64_t *var_3;
    uint64_t *var_4;
    uint64_t var_5;
    uint64_t var_6;
    uint64_t *var_7;
    uint64_t *var_8;
    uint64_t var_9;
    uint64_t *var_10;
    uint64_t var_11;
    uint64_t *var_12;
    uint32_t *var_13;
    uint64_t rcx2_1;
    uint64_t rcx2_0;
    uint64_t var_14;
    uint32_t var_15;
    uint32_t var_16;
    uint64_t var_17;
    uint64_t var_18;
    uint64_t var_19;
    uint64_t var_20;
    uint64_t var_44;
    uint64_t local_sp_1;
    uint64_t var_41;
    uint64_t var_21;
    uint64_t var_22;
    uint64_t var_23;
    uint64_t var_24;
    uint64_t var_26;
    uint64_t var_25;
    uint64_t local_sp_4;
    uint64_t var_28;
    uint32_t var_29;
    uint64_t rcx2_4;
    uint64_t local_sp_2;
    uint64_t r105_2;
    uint64_t rcx2_2;
    uint64_t r96_2;
    uint64_t r105_0;
    uint64_t r87_2;
    uint64_t r96_0;
    uint64_t rbx8_2;
    uint64_t r87_0;
    uint64_t rbx8_0;
    uint64_t var_30;
    uint64_t local_sp_3;
    uint64_t var_31;
    struct indirect_placeholder_19_ret_type var_32;
    uint64_t var_33;
    uint64_t var_34;
    uint64_t var_35;
    uint64_t var_36;
    uint64_t var_37;
    uint64_t var_38;
    uint64_t var_39;
    uint64_t var_40;
    uint64_t rcx2_3;
    uint64_t r105_1;
    uint64_t r96_1;
    uint64_t r87_1;
    uint64_t rbx8_1;
    uint32_t var_42;
    uint64_t var_43;
    uint64_t local_sp_5;
    uint64_t rcx2_5;
    uint64_t r105_3;
    uint64_t var_27;
    uint64_t r96_3;
    uint64_t r87_3;
    uint64_t rbx8_3;
    uint64_t var_45;
    uint64_t *var_46;
    uint64_t var_47;
    uint64_t *var_48;
    uint64_t *var_49;
    uint64_t local_sp_7;
    uint64_t var_50;
    uint64_t rcx2_7;
    uint64_t local_sp_6;
    uint64_t r105_5;
    uint64_t rcx2_6;
    uint64_t r96_5;
    uint64_t r105_4;
    uint64_t r87_5;
    uint64_t r96_4;
    uint64_t rbx8_5;
    uint64_t r87_4;
    uint64_t rbx8_4;
    uint64_t var_51;
    uint64_t var_52;
    uint64_t var_53;
    uint64_t var_54;
    uint64_t var_55;
    uint64_t var_56;
    uint64_t var_57;
    uint64_t var_58;
    uint64_t var_70;
    uint64_t var_59;
    uint64_t var_60;
    struct indirect_placeholder_20_ret_type var_61;
    uint64_t var_62;
    uint64_t var_63;
    uint64_t var_64;
    uint64_t var_65;
    uint64_t var_66;
    uint64_t var_67;
    uint64_t var_68;
    uint64_t var_69;
    unsigned int loop_state_var;
    var_0 = revng_init_local_sp(0UL);
    var_1 = init_rbp();
    var_2 = init_cc_src2();
    *(uint64_t *)(var_0 + (-8L)) = var_1;
    var_3 = (uint64_t *)(var_0 + (-80L));
    *var_3 = rdi;
    var_4 = (uint64_t *)(var_0 + (-88L));
    *var_4 = rsi;
    *(uint64_t *)(var_0 + (-96L)) = rdx;
    rcx2_0 = rcx;
    r105_0 = r10;
    r96_0 = r9;
    r87_0 = r8;
    rbx8_0 = rbx;
    r105_3 = r10;
    r96_3 = r9;
    r87_3 = r8;
    rbx8_3 = rbx;
    var_50 = 0UL;
    if (*var_4 == 0UL) {
        return;
    }
    var_5 = var_0 + (-104L);
    var_6 = var_0 + (-24L);
    var_7 = (uint64_t *)var_6;
    *var_7 = 4294784UL;
    var_8 = (uint64_t *)(var_0 + (-16L));
    *var_8 = 0UL;
    var_9 = *var_3;
    var_10 = (uint64_t *)(var_0 + (-32L));
    *var_10 = var_9;
    var_11 = *var_4;
    var_12 = (uint64_t *)(var_0 + (-40L));
    *var_12 = var_11;
    var_13 = (uint32_t *)(var_0 + (-60L));
    local_sp_0 = var_5;
    while (1U)
        {
            var_14 = *var_7;
            *var_7 = (var_14 + 4UL);
            var_15 = *(uint32_t *)var_14;
            *var_13 = var_15;
            rcx2_1 = rcx2_0;
            local_sp_1 = local_sp_0;
            local_sp_2 = local_sp_0;
            rcx2_2 = rcx2_0;
            local_sp_5 = local_sp_0;
            rcx2_5 = rcx2_0;
            if (var_15 == 0U) {
                *var_7 = 4294784UL;
            } else {
                if ((int)var_15 > (int)4294967295U) {
                    var_21 = (uint64_t)var_15;
                    var_22 = *var_12;
                    var_26 = var_22;
                    if (var_22 >= var_21) {
                        if (var_22 <= 1UL) {
                            loop_state_var = 1U;
                            break;
                        }
                        var_25 = helper_cc_compute_c_wrapper((var_22 * 3UL) - var_21, var_21, var_2, 17U);
                        if (var_25 != 0UL) {
                            var_26 = *var_12;
                            loop_state_var = 1U;
                            break;
                        }
                        var_28 = *var_12;
                        var_29 = *var_13;
                        loop_state_var = 2U;
                        break;
                    }
                    var_23 = *var_7;
                    var_24 = local_sp_0 + (-8L);
                    *(uint64_t *)var_24 = 4207403UL;
                    indirect_placeholder();
                    *var_7 = (*var_7 + ((uint64_t)*var_13 << 2UL));
                    *var_10 = (*var_10 + ((uint64_t)*var_13 << 2UL));
                    *var_12 = (*var_12 - (uint64_t)*var_13);
                    local_sp_1 = var_24;
                    rcx2_1 = var_23;
                } else {
                    var_16 = 0U - var_15;
                    *var_13 = var_16;
                    var_17 = (uint64_t)var_16;
                    var_18 = *var_12;
                    var_19 = helper_cc_compute_c_wrapper(var_17 - var_18, var_18, var_2, 17U);
                    if (var_19 != 0UL) {
                        var_20 = *var_8 + *var_12;
                        *var_8 = var_20;
                        var_44 = var_20;
                        loop_state_var = 0U;
                        break;
                    }
                    *var_8 = (*var_8 + (uint64_t)*var_13);
                    *var_12 = (*var_12 - (uint64_t)*var_13);
                }
            }
            local_sp_0 = local_sp_1;
            rcx2_0 = rcx2_1;
            continue;
        }
    switch (loop_state_var) {
      case 1U:
        {
            var_27 = *var_8 + var_26;
            *var_8 = var_27;
            var_44 = var_27;
        }
        break;
      case 0U:
        {
            var_45 = *var_4 - var_44;
            var_46 = (uint64_t *)(var_0 + (-56L));
            *var_46 = var_45;
            var_47 = *var_8 + (-1L);
            *var_8 = var_47;
            var_48 = (uint64_t *)(var_0 + (-48L));
            *var_48 = var_47;
            *var_12 = 0UL;
            var_49 = (uint64_t *)(var_0 + (-72L));
            local_sp_6 = local_sp_5;
            rcx2_6 = rcx2_5;
            r105_4 = r105_3;
            r96_4 = r96_3;
            r87_4 = r87_3;
            rbx8_4 = rbx8_3;
            var_51 = *var_4;
            var_52 = helper_cc_compute_c_wrapper(var_50 - var_51, var_51, var_2, 17U);
            local_sp_7 = local_sp_6;
            r105_5 = r105_4;
            r96_5 = r96_4;
            r87_5 = r87_4;
            rbx8_5 = rbx8_4;
            while (var_52 != 0UL)
                {
                    var_53 = *var_48;
                    if (var_53 > *var_8) {
                        var_59 = *var_46 - *var_12;
                        var_60 = local_sp_6 + (-8L);
                        *(uint64_t *)var_60 = 4207739UL;
                        var_61 = indirect_placeholder_20(rcx2_6, var_59, r105_4, r96_4, r87_4, rbx8_4);
                        var_62 = var_61.field_2;
                        var_63 = var_61.field_3;
                        var_64 = var_61.field_4;
                        var_65 = var_61.field_5;
                        *var_49 = (*var_12 + var_61.field_0);
                        *var_13 = *(uint32_t *)(*var_3 + (*var_12 << 2UL));
                        var_66 = *var_12 << 2UL;
                        var_67 = *var_3;
                        var_68 = var_66 + var_67;
                        var_69 = *var_49 << 2UL;
                        *(uint32_t *)var_68 = *(uint32_t *)(var_67 + var_69);
                        *(uint32_t *)((*var_49 << 2UL) + *var_3) = *var_13;
                        local_sp_7 = var_60;
                        rcx2_7 = var_69;
                        r105_5 = var_62;
                        r96_5 = var_63;
                        r87_5 = var_64;
                        rbx8_5 = var_65;
                    } else {
                        *var_48 = ((var_53 + *var_4) + (-1L));
                        var_54 = *var_46;
                        *var_46 = (var_54 + 1UL);
                        var_55 = var_54 << 2UL;
                        var_56 = *var_3;
                        var_57 = var_55 + var_56;
                        var_58 = *var_12 << 2UL;
                        *(uint32_t *)var_57 = *(uint32_t *)(var_56 + var_58);
                        *(uint32_t *)(*var_3 + (*var_12 << 2UL)) = 4294967295U;
                        rcx2_7 = var_58;
                    }
                    *var_48 = (*var_48 - *var_8);
                    var_70 = *var_12 + 1UL;
                    *var_12 = var_70;
                    var_50 = var_70;
                    local_sp_6 = local_sp_7;
                    rcx2_6 = rcx2_7;
                    r105_4 = r105_5;
                    r96_4 = r96_5;
                    r87_4 = r87_5;
                    rbx8_4 = rbx8_5;
                    var_51 = *var_4;
                    var_52 = helper_cc_compute_c_wrapper(var_50 - var_51, var_51, var_2, 17U);
                    local_sp_7 = local_sp_6;
                    r105_5 = r105_4;
                    r96_5 = r96_4;
                    r87_5 = r87_4;
                    rbx8_5 = rbx8_4;
                }
        }
        break;
      case 2U:
        {
            while (1U)
                {
                    var_30 = (uint64_t)var_29;
                    local_sp_3 = local_sp_2;
                    rcx2_3 = rcx2_2;
                    r105_1 = r105_0;
                    r96_1 = r96_0;
                    r87_1 = r87_0;
                    rbx8_1 = rbx8_0;
                    if (var_28 != var_30) {
                        var_31 = local_sp_2 + (-8L);
                        *(uint64_t *)var_31 = 4207512UL;
                        var_32 = indirect_placeholder_19(rcx2_2, var_30, r105_0, r96_0, r87_0, rbx8_0);
                        var_33 = var_32.field_0;
                        var_34 = var_32.field_1;
                        var_35 = var_32.field_2;
                        var_36 = var_32.field_3;
                        var_37 = var_32.field_4;
                        var_38 = var_32.field_5;
                        var_39 = *var_12;
                        var_40 = helper_cc_compute_c_wrapper(var_33 - var_39, var_39, var_2, 17U);
                        local_sp_4 = var_31;
                        rcx2_4 = var_34;
                        r105_2 = var_35;
                        r96_2 = var_36;
                        r87_2 = var_37;
                        rbx8_2 = var_38;
                        local_sp_3 = var_31;
                        rcx2_3 = var_34;
                        r105_1 = var_35;
                        r96_1 = var_36;
                        r87_1 = var_37;
                        rbx8_1 = var_38;
                        if (var_40 != 0UL) {
                            *var_7 = (*var_7 + 4UL);
                            var_42 = *var_13 + (-1);
                            *var_13 = var_42;
                            var_43 = *var_12;
                            var_28 = var_43;
                            var_29 = var_42;
                            local_sp_2 = local_sp_4;
                            rcx2_2 = rcx2_4;
                            r105_0 = r105_2;
                            r96_0 = r96_2;
                            r87_0 = r87_2;
                            rbx8_0 = rbx8_2;
                            local_sp_5 = local_sp_4;
                            rcx2_5 = rcx2_4;
                            r105_3 = r105_2;
                            r96_3 = r96_2;
                            r87_3 = r87_2;
                            rbx8_3 = rbx8_2;
                            if (var_43 != 0UL) {
                                continue;
                            }
                            break;
                        }
                    }
                    var_41 = *var_10;
                    *var_10 = (var_41 + 4UL);
                    *(uint32_t *)var_41 = **(uint32_t **)var_6;
                    *var_12 = (*var_12 + (-1L));
                    local_sp_4 = local_sp_3;
                    rcx2_4 = rcx2_3;
                    r105_2 = r105_1;
                    r96_2 = r96_1;
                    r87_2 = r87_1;
                    rbx8_2 = rbx8_1;
                    *var_7 = (*var_7 + 4UL);
                    var_42 = *var_13 + (-1);
                    *var_13 = var_42;
                    var_43 = *var_12;
                    var_28 = var_43;
                    var_29 = var_42;
                    local_sp_2 = local_sp_4;
                    rcx2_2 = rcx2_4;
                    r105_0 = r105_2;
                    r96_0 = r96_2;
                    r87_0 = r87_2;
                    rbx8_0 = rbx8_2;
                    local_sp_5 = local_sp_4;
                    rcx2_5 = rcx2_4;
                    r105_3 = r105_2;
                    r96_3 = r96_2;
                    r87_3 = r87_2;
                    rbx8_3 = rbx8_2;
                    if (var_43 != 0UL) {
                        continue;
                    }
                    break;
                }
            var_44 = *var_8;
        }
        break;
    }
}

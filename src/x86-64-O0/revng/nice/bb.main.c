typedef unsigned char __u_char;
typedef unsigned short __u_short;
typedef unsigned int __u_int;
typedef unsigned long __u_long;
typedef signed char __int8_t;
typedef unsigned char __uint8_t;
typedef short __int16_t;
typedef unsigned short __uint16_t;
typedef int __int32_t;
typedef unsigned int __uint32_t;
typedef long __int64_t;
typedef unsigned long __uint64_t;
typedef __int8_t __int_least8_t;
typedef __uint8_t __uint_least8_t;
typedef __int16_t __int_least16_t;
typedef __uint16_t __uint_least16_t;
typedef __int32_t __int_least32_t;
typedef __uint32_t __uint_least32_t;
typedef __int64_t __int_least64_t;
typedef __uint64_t __uint_least64_t;
typedef long __quad_t;
typedef unsigned long __u_quad_t;
typedef long __intmax_t;
typedef unsigned long __uintmax_t;
typedef unsigned long __dev_t;
typedef unsigned int __uid_t;
typedef unsigned int __gid_t;
typedef unsigned long __ino_t;
typedef unsigned long __ino64_t;
typedef unsigned int __mode_t;
typedef unsigned long __nlink_t;
typedef long __off_t;
typedef long __off64_t;
typedef int __pid_t;
typedef struct {
    int __val[2];
} __fsid_t;
typedef long __clock_t;
typedef unsigned long __rlim_t;
typedef unsigned long __rlim64_t;
typedef unsigned int __id_t;
typedef long __time_t;
typedef unsigned int __useconds_t;
typedef long __suseconds_t;
typedef int __daddr_t;
typedef int __key_t;
typedef int __clockid_t;
typedef void *__timer_t;
typedef long __blksize_t;
typedef long __blkcnt_t;
typedef long __blkcnt64_t;
typedef unsigned long __fsblkcnt_t;
typedef unsigned long __fsblkcnt64_t;
typedef unsigned long __fsfilcnt_t;
typedef unsigned long __fsfilcnt64_t;
typedef long __fsword_t;
typedef long __ssize_t;
typedef long __syscall_slong_t;
typedef unsigned long __syscall_ulong_t;
typedef __off64_t __loff_t;
typedef char *__caddr_t;
typedef long __intptr_t;
typedef unsigned int __socklen_t;
typedef int __sig_atomic_t;
typedef __int8_t int8_t;
typedef __int16_t int16_t;
typedef __int32_t int32_t;
typedef __int64_t int64_t;
typedef __uint8_t uint8_t;
typedef __uint16_t uint16_t;
typedef __uint32_t uint32_t;
typedef __uint64_t uint64_t;
typedef __int_least8_t int_least8_t;
typedef __int_least16_t int_least16_t;
typedef __int_least32_t int_least32_t;
typedef __int_least64_t int_least64_t;
typedef __uint_least8_t uint_least8_t;
typedef __uint_least16_t uint_least16_t;
typedef __uint_least32_t uint_least32_t;
typedef __uint_least64_t uint_least64_t;
typedef signed char int_fast8_t;
typedef long int_fast16_t;
typedef long int_fast32_t;
typedef long int_fast64_t;
typedef unsigned char uint_fast8_t;
typedef unsigned long uint_fast16_t;
typedef unsigned long uint_fast32_t;
typedef unsigned long uint_fast64_t;
typedef long intptr_t;
typedef unsigned long uintptr_t;
typedef __intmax_t intmax_t;
typedef __uintmax_t uintmax_t;
typedef _Bool bool;
struct indirect_placeholder_51_ret_type;
struct indirect_placeholder_50_ret_type;
struct indirect_placeholder_49_ret_type;
struct indirect_placeholder_52_ret_type;
struct indirect_placeholder_53_ret_type;
struct indirect_placeholder_55_ret_type;
struct indirect_placeholder_54_ret_type;
struct indirect_placeholder_57_ret_type;
struct indirect_placeholder_58_ret_type;
struct indirect_placeholder_59_ret_type;
struct indirect_placeholder_51_ret_type {
    uint64_t field_0;
    uint64_t field_1;
    uint64_t field_2;
};
struct indirect_placeholder_50_ret_type {
    uint64_t field_0;
    uint64_t field_1;
    uint64_t field_2;
};
struct indirect_placeholder_49_ret_type {
    uint64_t field_0;
    uint64_t field_1;
    uint64_t field_2;
};
struct indirect_placeholder_52_ret_type {
    uint64_t field_0;
    uint64_t field_1;
    uint64_t field_2;
};
struct indirect_placeholder_53_ret_type {
    uint64_t field_0;
    uint64_t field_1;
    uint64_t field_2;
};
struct indirect_placeholder_55_ret_type {
    uint64_t field_0;
    uint64_t field_1;
    uint64_t field_2;
};
struct indirect_placeholder_54_ret_type {
    uint64_t field_0;
    uint64_t field_1;
    uint64_t field_2;
};
struct indirect_placeholder_57_ret_type {
    uint64_t field_0;
    uint64_t field_1;
    uint64_t field_2;
};
struct indirect_placeholder_58_ret_type {
    uint64_t field_0;
    uint64_t field_1;
    uint64_t field_2;
};
struct indirect_placeholder_59_ret_type {
    uint64_t field_0;
    uint64_t field_1;
    uint64_t field_2;
    uint64_t field_3;
};
extern void abort(void);
extern uint64_t revng_init_local_sp(uint64_t param_0);
extern uint64_t init_rbp(void);
extern void indirect_placeholder(void);
extern void indirect_placeholder_10(uint64_t param_0, uint64_t param_1);
extern uint64_t init_rbx(void);
extern uint64_t indirect_placeholder_7(uint64_t param_0);
extern void indirect_placeholder_5(uint64_t param_0);
extern uint64_t init_r8(void);
extern uint64_t init_r9(void);
extern uint64_t init_rcx(void);
extern struct indirect_placeholder_51_ret_type indirect_placeholder_51(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint64_t param_3, uint64_t param_4, uint64_t param_5, uint64_t param_6);
extern struct indirect_placeholder_50_ret_type indirect_placeholder_50(uint64_t param_0);
extern struct indirect_placeholder_49_ret_type indirect_placeholder_49(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint64_t param_3, uint64_t param_4, uint64_t param_5, uint64_t param_6);
extern struct indirect_placeholder_52_ret_type indirect_placeholder_52(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint64_t param_3, uint64_t param_4, uint64_t param_5, uint64_t param_6);
extern struct indirect_placeholder_53_ret_type indirect_placeholder_53(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint64_t param_3, uint64_t param_4, uint64_t param_5, uint64_t param_6);
extern struct indirect_placeholder_55_ret_type indirect_placeholder_55(uint64_t param_0);
extern struct indirect_placeholder_54_ret_type indirect_placeholder_54(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint64_t param_3, uint64_t param_4, uint64_t param_5, uint64_t param_6);
extern void indirect_placeholder_56(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint64_t param_3, uint64_t param_4, uint64_t param_5);
extern struct indirect_placeholder_57_ret_type indirect_placeholder_57(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint64_t param_3, uint64_t param_4);
extern struct indirect_placeholder_58_ret_type indirect_placeholder_58(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint64_t param_3, uint64_t param_4, uint64_t param_5, uint64_t param_6);
extern struct indirect_placeholder_59_ret_type indirect_placeholder_59(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint64_t param_3, uint64_t param_4);
void bb_main(uint64_t rsi, uint64_t rdi) {
    uint64_t var_23;
    uint64_t var_24;
    struct indirect_placeholder_59_ret_type var_29;
    struct indirect_placeholder_57_ret_type var_41;
    uint64_t var_70;
    uint64_t storemerge11;
    uint64_t r8_0;
    uint64_t r9_0;
    uint64_t var_71;
    uint64_t var_72;
    uint64_t local_sp_0;
    uint32_t *var_73;
    uint64_t var_74;
    struct indirect_placeholder_50_ret_type var_75;
    uint64_t var_76;
    uint64_t var_77;
    uint64_t var_78;
    uint64_t var_79;
    uint64_t rcx_4;
    uint64_t var_57;
    uint64_t var_58;
    uint64_t var_59;
    uint64_t r8_4;
    uint64_t r9_4;
    struct indirect_placeholder_52_ret_type var_60;
    uint64_t var_55;
    uint32_t var_56;
    uint64_t local_sp_1;
    uint64_t local_sp_4;
    uint64_t var_61;
    uint64_t var_62;
    unsigned char *var_63;
    unsigned char var_64;
    unsigned char var_65;
    uint64_t var_66;
    uint64_t var_86;
    uint64_t var_87;
    uint64_t var_88;
    uint64_t var_84;
    uint32_t var_85;
    uint64_t local_sp_2;
    uint64_t rcx_3;
    uint64_t var_42;
    struct indirect_placeholder_55_ret_type var_43;
    uint64_t var_44;
    uint64_t var_45;
    uint64_t var_46;
    uint64_t var_47;
    struct indirect_placeholder_54_ret_type var_48;
    uint64_t local_sp_3;
    uint64_t rcx_1;
    uint64_t r8_1;
    uint64_t r9_1;
    uint64_t var_49;
    uint32_t storemerge10;
    uint64_t local_sp_7;
    uint64_t var_35;
    uint64_t var_0;
    uint64_t var_1;
    uint64_t var_2;
    uint64_t var_3;
    uint64_t var_4;
    uint64_t var_5;
    uint64_t var_6;
    uint32_t *var_7;
    uint64_t var_8;
    uint64_t *var_9;
    uint32_t *var_10;
    uint64_t *var_11;
    uint64_t **var_12;
    uint64_t var_13;
    uint64_t var_14;
    uint32_t *var_15;
    uint64_t var_16;
    uint64_t *var_17;
    uint32_t *var_18;
    uint64_t var_19;
    uint64_t *var_20;
    uint32_t *var_21;
    uint32_t _pre;
    uint64_t local_sp_5_ph;
    uint64_t rcx_2_ph;
    uint64_t r8_2_ph;
    uint64_t r9_2_ph;
    uint64_t rcx_0;
    uint32_t *var_67;
    uint64_t var_68;
    uint64_t var_69;
    uint64_t var_36;
    uint32_t var_37;
    uint64_t local_sp_6;
    uint32_t var_22;
    uint64_t r8_3;
    uint64_t r9_3;
    uint64_t var_38;
    uint64_t var_39;
    uint64_t var_40;
    uint32_t var_50;
    uint64_t var_51;
    bool var_80;
    uint64_t *var_81;
    uint32_t *var_82;
    uint64_t var_83;
    uint32_t *var_52;
    uint64_t var_53;
    uint32_t *var_54;
    uint64_t storemerge;
    uint32_t var_25;
    uint64_t var_26;
    uint64_t var_27;
    uint64_t var_28;
    uint64_t var_30;
    uint64_t var_31;
    uint64_t var_32;
    uint64_t var_33;
    uint32_t var_34;
    unsigned int loop_state_var;
    bool switch_state_var;
    var_0 = revng_init_local_sp(0UL);
    var_1 = init_rbp();
    var_2 = init_rcx();
    var_3 = init_r8();
    var_4 = init_r9();
    var_5 = init_rbx();
    var_6 = var_0 + (-8L);
    *(uint64_t *)var_6 = var_1;
    *(uint64_t *)(var_0 + (-16L)) = var_5;
    var_7 = (uint32_t *)(var_0 + (-108L));
    *var_7 = (uint32_t)rdi;
    var_8 = var_0 + (-120L);
    var_9 = (uint64_t *)var_8;
    *var_9 = rsi;
    var_10 = (uint32_t *)(var_0 + (-28L));
    *var_10 = 10U;
    var_11 = (uint64_t *)(var_0 + (-40L));
    *var_11 = 0UL;
    var_12 = (uint64_t **)var_8;
    var_13 = **var_12;
    *(uint64_t *)(var_0 + (-128L)) = 4201999UL;
    indirect_placeholder_5(var_13);
    *(uint64_t *)(var_0 + (-136L)) = 4202014UL;
    indirect_placeholder();
    *(uint64_t *)(var_0 + (-144L)) = 4202024UL;
    indirect_placeholder_5(125UL);
    var_14 = var_0 + (-152L);
    *(uint64_t *)var_14 = 4202034UL;
    indirect_placeholder();
    var_15 = (uint32_t *)(var_0 + (-44L));
    *var_15 = 1U;
    var_16 = var_0 + (-56L);
    var_17 = (uint64_t *)var_16;
    var_18 = (uint32_t *)(var_0 + (-60L));
    var_19 = var_0 + (-72L);
    var_20 = (uint64_t *)var_19;
    var_21 = (uint32_t *)(var_0 + (-76L));
    storemerge10 = 4294967257U;
    _pre = 1U;
    local_sp_5_ph = var_14;
    rcx_2_ph = var_2;
    r8_2_ph = var_3;
    r9_2_ph = var_4;
    storemerge = 1UL;
    while (1U)
        {
            rcx_3 = rcx_2_ph;
            local_sp_6 = local_sp_5_ph;
            var_22 = _pre;
            r8_3 = r8_2_ph;
            r9_3 = r9_2_ph;
            while (1U)
                {
                    if ((long)((uint64_t)var_22 << 32UL) >= (long)((uint64_t)*var_7 << 32UL)) {
                        loop_state_var = 0U;
                        break;
                    }
                    var_23 = *(uint64_t *)(*var_9 + ((uint64_t)var_22 << 3UL));
                    *var_17 = var_23;
                    if (**(unsigned char **)var_16 != '-') {
                        loop_state_var = 1U;
                        break;
                    }
                    storemerge = 2UL;
                }
            switch_state_var = 0;
            switch (loop_state_var) {
              case 1U:
                {
                    *var_18 = ((1U - *var_15) + *var_7);
                    *var_20 = (*var_9 + (((uint64_t)*var_15 << 3UL) + (-8L)));
                    **(uint64_t **)var_19 = **var_12;
                    *(uint32_t *)6378616UL = 0U;
                    var_26 = *var_20;
                    var_27 = (uint64_t)*var_18;
                    var_28 = local_sp_5_ph + (-8L);
                    *(uint64_t *)var_28 = 4202270UL;
                    var_29 = indirect_placeholder_59(4268263UL, 4267456UL, 0UL, var_26, var_27);
                    var_30 = var_29.field_0;
                    var_31 = var_29.field_1;
                    var_32 = var_29.field_2;
                    var_33 = var_29.field_3;
                    *var_21 = (uint32_t)var_30;
                    *var_15 = ((*(uint32_t *)6378616UL + (-1)) + *var_15);
                    var_34 = *var_21;
                    local_sp_4 = var_28;
                    rcx_3 = var_31;
                    local_sp_5_ph = var_28;
                    rcx_2_ph = var_31;
                    r8_2_ph = var_32;
                    r9_2_ph = var_33;
                    var_37 = var_34;
                    local_sp_6 = var_28;
                    r8_3 = var_32;
                    r9_3 = var_33;
                    if ((uint64_t)(var_34 + 130U) == 0UL) {
                        *(uint64_t *)(local_sp_5_ph + (-16L)) = 4202344UL;
                        indirect_placeholder_10(var_6, 0UL);
                        abort();
                    }
                    if ((int)var_34 > (int)4294967166U) {
                        if ((uint64_t)(var_34 + 1U) != 0UL) {
                            if ((uint64_t)(var_34 + (-110)) != 0UL) {
                                loop_state_var = 0U;
                                switch_state_var = 1;
                                break;
                            }
                            *var_11 = *(uint64_t *)6379288UL;
                            var_37 = *var_21;
                        }
                        if (var_37 != 4294967295U) {
                            loop_state_var = 1U;
                            switch_state_var = 1;
                            break;
                        }
                        _pre = *var_15;
                        continue;
                    }
                    if (var_34 != 4294967165U) {
                        loop_state_var = 0U;
                        switch_state_var = 1;
                        break;
                    }
                    var_35 = *(uint64_t *)6378464UL;
                    *(uint64_t *)(local_sp_5_ph + (-16L)) = 4202396UL;
                    indirect_placeholder_56(0UL, 4267168UL, var_35, 4268267UL, 4268062UL, 0UL);
                    var_36 = local_sp_5_ph + (-24L);
                    *(uint64_t *)var_36 = 4202406UL;
                    indirect_placeholder();
                    local_sp_4 = var_36;
                    loop_state_var = 0U;
                    switch_state_var = 1;
                    break;
                }
                break;
              case 0U:
                {
                    loop_state_var = 1U;
                    switch_state_var = 1;
                    break;
                }
                break;
            }
            if (switch_state_var)
                break;
        }
    switch (loop_state_var) {
      case 0U:
        {
            *(uint64_t *)(local_sp_4 + (-8L)) = 4202416UL;
            indirect_placeholder_10(var_6, 125UL);
            abort();
        }
        break;
      case 1U:
        {
            var_38 = *var_11;
            rcx_4 = rcx_3;
            r8_4 = r8_3;
            r9_4 = r9_3;
            r9_1 = r9_3;
            local_sp_7 = local_sp_6;
            if (var_38 != 0UL) {
                var_39 = var_0 + (-96L);
                var_40 = local_sp_6 + (-8L);
                *(uint64_t *)var_40 = 4202479UL;
                var_41 = indirect_placeholder_57(10UL, var_39, 4267331UL, 0UL, var_38);
                local_sp_3 = var_40;
                rcx_1 = var_41.field_1;
                r8_1 = var_41.field_2;
                if ((uint64_t)((uint32_t)var_41.field_0 & (-2)) == 0UL) {
                    var_42 = *var_11;
                    *(uint64_t *)(local_sp_6 + (-16L)) = 4202496UL;
                    var_43 = indirect_placeholder_55(var_42);
                    var_44 = var_43.field_0;
                    var_45 = var_43.field_1;
                    var_46 = var_43.field_2;
                    var_47 = local_sp_6 + (-24L);
                    *(uint64_t *)var_47 = 4202524UL;
                    var_48 = indirect_placeholder_54(0UL, 4268283UL, var_44, var_45, 0UL, 125UL, var_46);
                    local_sp_3 = var_47;
                    rcx_1 = var_48.field_0;
                    r8_1 = var_48.field_1;
                    r9_1 = var_48.field_2;
                }
                var_49 = *(uint64_t *)var_39;
                local_sp_7 = local_sp_3;
                rcx_4 = rcx_1;
                r8_4 = r8_1;
                r9_4 = r9_1;
                if (!((var_49 != 18446744073709551577UL) && ((long)var_49 < (long)18446744073709551578UL))) {
                    storemerge10 = (uint32_t)(((long)var_49 < (long)39UL) ? var_49 : 39UL);
                }
                *var_10 = storemerge10;
            }
            var_50 = *var_15;
            var_51 = (uint64_t)var_50;
            rcx_0 = rcx_4;
            r8_0 = r8_4;
            r9_0 = r9_4;
            if ((uint64_t)(var_50 - *var_7) == 0UL) {
                var_80 = (*var_11 == 0UL);
                var_81 = (uint64_t *)(local_sp_7 + (-8L));
                if (var_80) {
                    *var_81 = 4202615UL;
                    indirect_placeholder_58(0UL, 4268312UL, rcx_4, r8_4, 0UL, 0UL, r9_4);
                    *(uint64_t *)(local_sp_7 + (-16L)) = 4202625UL;
                    indirect_placeholder_10(var_6, 125UL);
                    abort();
                }
                *var_81 = 4202630UL;
                indirect_placeholder();
                var_82 = (uint32_t *)var_51;
                *var_82 = 0U;
                var_83 = local_sp_7 + (-16L);
                *(uint64_t *)var_83 = 4202651UL;
                indirect_placeholder();
                *(uint32_t *)(var_0 + (-80L)) = var_50;
                local_sp_2 = var_83;
                var_84 = local_sp_7 + (-24L);
                *(uint64_t *)var_84 = 4202665UL;
                indirect_placeholder();
                var_85 = *var_82;
                local_sp_2 = var_84;
                if (var_50 != 4294967295U & var_85 == 0U) {
                    var_86 = (uint64_t)var_85;
                    *(uint64_t *)(local_sp_7 + (-32L)) = 4202676UL;
                    indirect_placeholder();
                    var_87 = (uint64_t)*(uint32_t *)var_86;
                    var_88 = local_sp_7 + (-40L);
                    *(uint64_t *)var_88 = 4202700UL;
                    indirect_placeholder_53(0UL, 4268355UL, rcx_4, r8_4, var_87, 125UL, r9_4);
                    local_sp_2 = var_88;
                }
                *(uint64_t *)(local_sp_2 + (-8L)) = 4202720UL;
                indirect_placeholder();
                return;
            }
            *(uint64_t *)(local_sp_7 + (-8L)) = 4202735UL;
            indirect_placeholder();
            var_52 = (uint32_t *)var_51;
            *var_52 = 0U;
            var_53 = local_sp_7 + (-16L);
            *(uint64_t *)var_53 = 4202756UL;
            indirect_placeholder();
            var_54 = (uint32_t *)(var_0 + (-80L));
            *var_54 = var_50;
            local_sp_1 = var_53;
            var_55 = local_sp_7 + (-24L);
            *(uint64_t *)var_55 = 4202770UL;
            indirect_placeholder();
            var_56 = *var_52;
            local_sp_1 = var_55;
            if (var_50 != 4294967295U & var_56 == 0U) {
                var_57 = (uint64_t)var_56;
                *(uint64_t *)(local_sp_7 + (-32L)) = 4202781UL;
                indirect_placeholder();
                var_58 = (uint64_t)*(uint32_t *)var_57;
                var_59 = local_sp_7 + (-40L);
                *(uint64_t *)var_59 = 4202805UL;
                var_60 = indirect_placeholder_52(0UL, 4268355UL, rcx_4, r8_4, var_58, 125UL, r9_4);
                local_sp_1 = var_59;
                rcx_0 = var_60.field_0;
                r8_0 = var_60.field_1;
                r9_0 = var_60.field_2;
            }
            var_61 = (uint64_t)(*var_10 + *var_54);
            var_62 = local_sp_1 + (-8L);
            *(uint64_t *)var_62 = 4202830UL;
            indirect_placeholder();
            var_63 = (unsigned char *)(var_0 + (-81L));
            var_64 = (var_61 == 0UL);
            *var_63 = var_64;
            var_65 = var_64 ^ '\x01';
            var_66 = (uint64_t)var_65;
            local_sp_0 = var_62;
            if (var_65 != '\x00') {
                *(uint64_t *)(local_sp_1 + (-16L)) = 4202854UL;
                indirect_placeholder();
                var_67 = (uint32_t *)var_66;
                var_68 = (uint64_t)*var_67;
                *(uint64_t *)(local_sp_1 + (-24L)) = 4202861UL;
                indirect_placeholder();
                var_69 = (uint64_t)*var_67;
                *(uint64_t *)(local_sp_1 + (-32L)) = 4202870UL;
                var_70 = indirect_placeholder_7(var_69);
                storemerge11 = ((uint64_t)(unsigned char)var_70 == 0UL) ? 125UL : 0UL;
                *(uint64_t *)(local_sp_1 + (-40L)) = 4202905UL;
                indirect_placeholder_51(0UL, 4268379UL, rcx_0, r8_0, var_68, storemerge11, r9_0);
                var_71 = *(uint64_t *)6378720UL;
                var_72 = local_sp_1 + (-48L);
                *(uint64_t *)var_72 = 4202920UL;
                indirect_placeholder();
                local_sp_0 = var_72;
                if ((uint64_t)(uint32_t)var_71 == 0UL) {
                    return;
                }
            }
            var_73 = *(uint32_t **)(*var_9 + ((uint64_t)*var_15 << 3UL));
            *(uint64_t *)(local_sp_0 + (-8L)) = 4202988UL;
            indirect_placeholder();
            *(uint64_t *)(local_sp_0 + (-16L)) = 4202993UL;
            indirect_placeholder();
            *(uint32_t *)(var_0 + (-88L)) = ((*var_73 == 2U) ? 127U : 126U);
            var_74 = *(uint64_t *)(*var_9 + ((uint64_t)*var_15 << 3UL));
            *(uint64_t *)(local_sp_0 + (-24L)) = 4203046UL;
            var_75 = indirect_placeholder_50(var_74);
            var_76 = var_75.field_0;
            var_77 = var_75.field_1;
            var_78 = var_75.field_2;
            *(uint64_t *)(local_sp_0 + (-32L)) = 4203054UL;
            indirect_placeholder();
            var_79 = (uint64_t)*(uint32_t *)var_76;
            *(uint64_t *)(local_sp_0 + (-40L)) = 4203081UL;
            indirect_placeholder_49(0UL, 4268399UL, var_76, var_77, var_79, 0UL, var_78);
            return;
        }
        break;
    }
}

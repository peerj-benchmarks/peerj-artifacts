typedef unsigned char __u_char;
typedef unsigned short __u_short;
typedef unsigned int __u_int;
typedef unsigned long __u_long;
typedef signed char __int8_t;
typedef unsigned char __uint8_t;
typedef short __int16_t;
typedef unsigned short __uint16_t;
typedef int __int32_t;
typedef unsigned int __uint32_t;
typedef long __int64_t;
typedef unsigned long __uint64_t;
typedef __int8_t __int_least8_t;
typedef __uint8_t __uint_least8_t;
typedef __int16_t __int_least16_t;
typedef __uint16_t __uint_least16_t;
typedef __int32_t __int_least32_t;
typedef __uint32_t __uint_least32_t;
typedef __int64_t __int_least64_t;
typedef __uint64_t __uint_least64_t;
typedef long __quad_t;
typedef unsigned long __u_quad_t;
typedef long __intmax_t;
typedef unsigned long __uintmax_t;
typedef unsigned long __dev_t;
typedef unsigned int __uid_t;
typedef unsigned int __gid_t;
typedef unsigned long __ino_t;
typedef unsigned long __ino64_t;
typedef unsigned int __mode_t;
typedef unsigned long __nlink_t;
typedef long __off_t;
typedef long __off64_t;
typedef int __pid_t;
typedef struct {
    int __val[2];
} __fsid_t;
typedef long __clock_t;
typedef unsigned long __rlim_t;
typedef unsigned long __rlim64_t;
typedef unsigned int __id_t;
typedef long __time_t;
typedef unsigned int __useconds_t;
typedef long __suseconds_t;
typedef int __daddr_t;
typedef int __key_t;
typedef int __clockid_t;
typedef void *__timer_t;
typedef long __blksize_t;
typedef long __blkcnt_t;
typedef long __blkcnt64_t;
typedef unsigned long __fsblkcnt_t;
typedef unsigned long __fsblkcnt64_t;
typedef unsigned long __fsfilcnt_t;
typedef unsigned long __fsfilcnt64_t;
typedef long __fsword_t;
typedef long __ssize_t;
typedef long __syscall_slong_t;
typedef unsigned long __syscall_ulong_t;
typedef __off64_t __loff_t;
typedef char *__caddr_t;
typedef long __intptr_t;
typedef unsigned int __socklen_t;
typedef int __sig_atomic_t;
typedef __int8_t int8_t;
typedef __int16_t int16_t;
typedef __int32_t int32_t;
typedef __int64_t int64_t;
typedef __uint8_t uint8_t;
typedef __uint16_t uint16_t;
typedef __uint32_t uint32_t;
typedef __uint64_t uint64_t;
typedef __int_least8_t int_least8_t;
typedef __int_least16_t int_least16_t;
typedef __int_least32_t int_least32_t;
typedef __int_least64_t int_least64_t;
typedef __uint_least8_t uint_least8_t;
typedef __uint_least16_t uint_least16_t;
typedef __uint_least32_t uint_least32_t;
typedef __uint_least64_t uint_least64_t;
typedef signed char int_fast8_t;
typedef long int_fast16_t;
typedef long int_fast32_t;
typedef long int_fast64_t;
typedef unsigned char uint_fast8_t;
typedef unsigned long uint_fast16_t;
typedef unsigned long uint_fast32_t;
typedef unsigned long uint_fast64_t;
typedef long intptr_t;
typedef unsigned long uintptr_t;
typedef __intmax_t intmax_t;
typedef __uintmax_t uintmax_t;
typedef _Bool bool;
extern uint64_t revng_init_local_sp(uint64_t param_0);
extern uint64_t init_rbp(void);
extern uint64_t indirect_placeholder_4(uint64_t param_0, uint64_t param_1);
extern uint64_t init_cc_src2(void);
extern uint64_t helper_cc_compute_c_wrapper(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint32_t param_3);
extern void indirect_placeholder(void);
extern uint64_t indirect_placeholder_3(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint64_t param_3, uint64_t param_4);
extern uint64_t indirect_placeholder_2(uint64_t param_0, uint64_t param_1, uint64_t param_2);
extern uint64_t init_rbx(void);
extern void indirect_placeholder_5(uint64_t param_0, uint64_t param_1);
extern uint64_t init_r8(void);
extern uint64_t init_r9(void);
extern void indirect_placeholder_1(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint64_t param_3, uint64_t param_4, uint64_t param_5, uint64_t param_6);
uint64_t bb_sysv_sum_file(uint64_t rdi, uint64_t rsi) {
    uint32_t *var_14;
    uint64_t var_15;
    uint64_t var_16;
    uint32_t *var_17;
    uint64_t var_0;
    uint64_t var_1;
    uint64_t var_2;
    uint64_t var_3;
    uint64_t var_4;
    uint64_t var_5;
    uint64_t *var_6;
    uint32_t *var_7;
    uint64_t *var_8;
    uint32_t *var_9;
    uint64_t var_10;
    bool var_11;
    unsigned char *var_12;
    unsigned char var_13;
    uint64_t var_36;
    uint64_t local_sp_1;
    uint64_t local_sp_0;
    uint64_t rax_0;
    uint64_t var_29;
    uint64_t var_30;
    uint64_t var_31;
    uint32_t *_pre_phi105;
    uint64_t local_sp_2_ph;
    uint32_t var_27;
    uint64_t var_28;
    uint32_t var_32;
    uint32_t narrow;
    uint64_t var_33;
    uint64_t var_34;
    uint64_t var_35;
    uint64_t var_24;
    uint64_t var_25;
    uint64_t var_26;
    uint64_t var_37;
    uint64_t var_38;
    uint64_t var_39;
    uint64_t local_sp_2;
    uint64_t var_40;
    uint64_t *var_18;
    uint64_t *var_19;
    uint64_t var_20;
    uint64_t var_21;
    uint64_t var_22;
    uint64_t var_23;
    unsigned int loop_state_var;
    bool switch_state_var;
    var_0 = revng_init_local_sp(0UL);
    var_1 = init_rbp();
    var_2 = init_cc_src2();
    var_3 = init_r8();
    var_4 = init_r9();
    var_5 = init_rbx();
    *(uint64_t *)(var_0 + (-8L)) = var_1;
    *(uint64_t *)(var_0 + (-16L)) = var_5;
    var_6 = (uint64_t *)(var_0 + (-8496L));
    *var_6 = rdi;
    var_7 = (uint32_t *)(var_0 + (-8500L));
    *var_7 = (uint32_t)rsi;
    var_8 = (uint64_t *)(var_0 + (-40L));
    *var_8 = 0UL;
    var_9 = (uint32_t *)(var_0 + (-44L));
    *var_9 = 0U;
    var_10 = *var_6;
    *(uint64_t *)(var_0 + (-8512L)) = 4202742UL;
    indirect_placeholder();
    var_11 = ((uint64_t)(uint32_t)var_10 == 0UL);
    var_12 = (unsigned char *)(var_0 + (-57L));
    var_13 = var_11;
    *var_12 = var_13;
    var_37 = 0UL;
    rax_0 = 0UL;
    if (var_13 == '\x00') {
        var_16 = var_0 + (-8520L);
        *(uint64_t *)var_16 = 4202812UL;
        indirect_placeholder();
        var_17 = (uint32_t *)(var_0 + (-28L));
        *var_17 = 0U;
        _pre_phi105 = var_17;
        local_sp_2_ph = var_16;
    } else {
        var_14 = (uint32_t *)(var_0 + (-28L));
        *var_14 = 0U;
        *(unsigned char *)6379472UL = (unsigned char)'\x01';
        var_15 = var_0 + (-8520L);
        *(uint64_t *)var_15 = 4202785UL;
        indirect_placeholder_5(0UL, 0UL);
        _pre_phi105 = var_14;
        local_sp_2_ph = var_15;
    }
    var_18 = (uint64_t *)(var_0 + (-72L));
    var_19 = (uint64_t *)(var_0 + (-56L));
    var_20 = var_0 + (-8280L);
    local_sp_2 = local_sp_2_ph;
    while (1U)
        {
            var_21 = (uint64_t)*_pre_phi105;
            var_22 = local_sp_2 + (-8L);
            *(uint64_t *)var_22 = 4202916UL;
            var_23 = indirect_placeholder_4(8192UL, var_21);
            *var_18 = var_23;
            local_sp_1 = var_22;
            local_sp_2 = var_22;
            switch_state_var = 0;
            switch (var_23) {
              case 0UL:
                {
                    rax_0 = 1UL;
                    var_27 = *_pre_phi105;
                    var_28 = local_sp_2 + (-16L);
                    *(uint64_t *)var_28 = 4203117UL;
                    indirect_placeholder();
                    local_sp_1 = var_28;
                    if (!(*var_12 == '\x01' && var_27 == 0U)) {
                        loop_state_var = 1U;
                        switch_state_var = 1;
                        break;
                    }
                    var_29 = *var_6;
                    *(uint64_t *)(local_sp_2 + (-24L)) = 4203146UL;
                    var_30 = indirect_placeholder_2(var_29, 0UL, 3UL);
                    *(uint64_t *)(local_sp_2 + (-32L)) = 4203154UL;
                    indirect_placeholder();
                    var_31 = (uint64_t)*(uint32_t *)var_30;
                    *(uint64_t *)(local_sp_2 + (-40L)) = 4203181UL;
                    indirect_placeholder_1(0UL, 4271022UL, var_30, 0UL, var_3, var_31, var_4);
                    loop_state_var = 0U;
                    switch_state_var = 1;
                    break;
                }
                break;
              case 18446744073709551615UL:
                {
                    var_24 = *var_6;
                    *(uint64_t *)(local_sp_2 + (-16L)) = 4202980UL;
                    var_25 = indirect_placeholder_2(var_24, 0UL, 3UL);
                    *(uint64_t *)(local_sp_2 + (-24L)) = 4202988UL;
                    indirect_placeholder();
                    var_26 = (uint64_t)*(uint32_t *)var_25;
                    *(uint64_t *)(local_sp_2 + (-32L)) = 4203015UL;
                    indirect_placeholder_1(0UL, 4271022UL, var_25, 0UL, var_3, var_26, var_4);
                    if (*var_12 != '\x01') {
                        loop_state_var = 0U;
                        switch_state_var = 1;
                        break;
                    }
                    *(uint64_t *)(local_sp_2 + (-40L)) = 4203036UL;
                    indirect_placeholder();
                    loop_state_var = 0U;
                    switch_state_var = 1;
                    break;
                }
                break;
              default:
                {
                    *var_19 = 0UL;
                    var_38 = *var_18;
                    var_39 = helper_cc_compute_c_wrapper(var_37 - var_38, var_38, var_2, 17U);
                    while (var_39 != 0UL)
                        {
                            *var_9 = (*var_9 + (uint32_t)*(unsigned char *)(*var_19 + var_20));
                            var_40 = *var_19 + 1UL;
                            *var_19 = var_40;
                            var_37 = var_40;
                            var_38 = *var_18;
                            var_39 = helper_cc_compute_c_wrapper(var_37 - var_38, var_38, var_2, 17U);
                        }
                    *var_8 = (*var_8 + *var_18);
                    continue;
                }
                break;
            }
            if (switch_state_var)
                break;
        }
    switch (loop_state_var) {
      case 0U:
        {
            return rax_0;
        }
        break;
      case 1U:
        {
            var_32 = *var_9;
            narrow = (uint32_t)(uint16_t)var_32 + (var_32 >> 16U);
            *(uint32_t *)(var_0 + (-76L)) = narrow;
            *(uint32_t *)(var_0 + (-80L)) = ((uint32_t)(uint16_t)narrow + (uint32_t)(uint64_t)((long)((uint64_t)narrow << 32UL) >> (long)48UL));
            var_33 = var_0 + (-8488L);
            var_34 = *var_8;
            *(uint64_t *)(local_sp_1 + (-8L)) = 4203260UL;
            indirect_placeholder_3(0UL, 1UL, var_34, 512UL, var_33);
            var_35 = local_sp_1 + (-16L);
            *(uint64_t *)var_35 = 4203283UL;
            indirect_placeholder();
            local_sp_0 = var_35;
            if (*var_7 != 0U) {
                var_36 = local_sp_1 + (-24L);
                *(uint64_t *)var_36 = 4203317UL;
                indirect_placeholder();
                local_sp_0 = var_36;
            }
            *(uint64_t *)(local_sp_0 + (-8L)) = 4203327UL;
            indirect_placeholder();
        }
        break;
    }
}

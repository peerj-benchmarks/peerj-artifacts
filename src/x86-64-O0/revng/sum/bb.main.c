typedef unsigned char __u_char;
typedef unsigned short __u_short;
typedef unsigned int __u_int;
typedef unsigned long __u_long;
typedef signed char __int8_t;
typedef unsigned char __uint8_t;
typedef short __int16_t;
typedef unsigned short __uint16_t;
typedef int __int32_t;
typedef unsigned int __uint32_t;
typedef long __int64_t;
typedef unsigned long __uint64_t;
typedef __int8_t __int_least8_t;
typedef __uint8_t __uint_least8_t;
typedef __int16_t __int_least16_t;
typedef __uint16_t __uint_least16_t;
typedef __int32_t __int_least32_t;
typedef __uint32_t __uint_least32_t;
typedef __int64_t __int_least64_t;
typedef __uint64_t __uint_least64_t;
typedef long __quad_t;
typedef unsigned long __u_quad_t;
typedef long __intmax_t;
typedef unsigned long __uintmax_t;
typedef unsigned long __dev_t;
typedef unsigned int __uid_t;
typedef unsigned int __gid_t;
typedef unsigned long __ino_t;
typedef unsigned long __ino64_t;
typedef unsigned int __mode_t;
typedef unsigned long __nlink_t;
typedef long __off_t;
typedef long __off64_t;
typedef int __pid_t;
typedef struct {
    int __val[2];
} __fsid_t;
typedef long __clock_t;
typedef unsigned long __rlim_t;
typedef unsigned long __rlim64_t;
typedef unsigned int __id_t;
typedef long __time_t;
typedef unsigned int __useconds_t;
typedef long __suseconds_t;
typedef int __daddr_t;
typedef int __key_t;
typedef int __clockid_t;
typedef void *__timer_t;
typedef long __blksize_t;
typedef long __blkcnt_t;
typedef long __blkcnt64_t;
typedef unsigned long __fsblkcnt_t;
typedef unsigned long __fsblkcnt64_t;
typedef unsigned long __fsfilcnt_t;
typedef unsigned long __fsfilcnt64_t;
typedef long __fsword_t;
typedef long __ssize_t;
typedef long __syscall_slong_t;
typedef unsigned long __syscall_ulong_t;
typedef __off64_t __loff_t;
typedef char *__caddr_t;
typedef long __intptr_t;
typedef unsigned int __socklen_t;
typedef int __sig_atomic_t;
typedef __int8_t int8_t;
typedef __int16_t int16_t;
typedef __int32_t int32_t;
typedef __int64_t int64_t;
typedef __uint8_t uint8_t;
typedef __uint16_t uint16_t;
typedef __uint32_t uint32_t;
typedef __uint64_t uint64_t;
typedef __int_least8_t int_least8_t;
typedef __int_least16_t int_least16_t;
typedef __int_least32_t int_least32_t;
typedef __int_least64_t int_least64_t;
typedef __uint_least8_t uint_least8_t;
typedef __uint_least16_t uint_least16_t;
typedef __uint_least32_t uint_least32_t;
typedef __uint_least64_t uint_least64_t;
typedef signed char int_fast8_t;
typedef long int_fast16_t;
typedef long int_fast32_t;
typedef long int_fast64_t;
typedef unsigned char uint_fast8_t;
typedef unsigned long uint_fast16_t;
typedef unsigned long uint_fast32_t;
typedef unsigned long uint_fast64_t;
typedef long intptr_t;
typedef unsigned long uintptr_t;
typedef __intmax_t intmax_t;
typedef __uintmax_t uintmax_t;
struct indirect_placeholder_14_ret_type;
struct indirect_placeholder_14_ret_type {
    uint64_t field_0;
    uint64_t field_1;
};
extern void abort(void);
extern uint64_t revng_init_local_sp(uint64_t param_0);
extern uint64_t init_rbp(void);
extern void indirect_placeholder(void);
extern uint64_t indirect_placeholder_2(uint64_t param_0, uint64_t param_1, uint64_t param_2);
extern uint64_t init_rbx(void);
extern void indirect_placeholder_5(uint64_t param_0, uint64_t param_1);
extern uint64_t indirect_placeholder_9(void);
extern void indirect_placeholder_6(uint64_t param_0);
extern void indirect_placeholder_1(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint64_t param_3, uint64_t param_4, uint64_t param_5, uint64_t param_6);
extern void indirect_placeholder_13(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint64_t param_3, uint64_t param_4, uint64_t param_5);
extern struct indirect_placeholder_14_ret_type indirect_placeholder_14(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint64_t param_3, uint64_t param_4);
void bb_main(uint64_t rdi, uint64_t rsi) {
    struct indirect_placeholder_14_ret_type var_14;
    uint32_t var_24;
    uint64_t var_0;
    uint64_t var_1;
    uint64_t var_2;
    uint64_t var_3;
    uint32_t *var_4;
    uint64_t var_5;
    uint64_t *var_6;
    uint64_t *var_7;
    uint64_t var_8;
    uint64_t var_9;
    uint32_t *var_10;
    uint64_t local_sp_3;
    uint64_t var_26;
    uint64_t var_27;
    uint64_t var_19;
    uint32_t var_20;
    uint64_t var_21;
    uint64_t var_22;
    uint64_t local_sp_1;
    unsigned char *var_23;
    uint64_t local_sp_0;
    uint64_t var_25;
    uint64_t var_28;
    uint64_t var_29;
    uint32_t var_30;
    uint64_t local_sp_2;
    uint64_t var_16;
    uint64_t var_17;
    uint64_t *var_18;
    uint64_t var_11;
    uint64_t var_12;
    uint64_t var_13;
    uint32_t var_15;
    unsigned int loop_state_var;
    var_0 = revng_init_local_sp(0UL);
    var_1 = init_rbp();
    var_2 = init_rbx();
    var_3 = var_0 + (-8L);
    *(uint64_t *)var_3 = var_1;
    *(uint64_t *)(var_0 + (-16L)) = var_2;
    var_4 = (uint32_t *)(var_0 + (-60L));
    *var_4 = (uint32_t)rdi;
    var_5 = var_0 + (-72L);
    var_6 = (uint64_t *)var_5;
    *var_6 = rsi;
    var_7 = (uint64_t *)(var_0 + (-40L));
    *var_7 = 4202073UL;
    var_8 = **(uint64_t **)var_5;
    *(uint64_t *)(var_0 + (-80L)) = 4203381UL;
    indirect_placeholder_6(var_8);
    *(uint64_t *)(var_0 + (-88L)) = 4203396UL;
    indirect_placeholder();
    *(uint64_t *)(var_0 + (-96L)) = 4203406UL;
    indirect_placeholder();
    var_9 = var_0 + (-104L);
    *(uint64_t *)var_9 = 4203436UL;
    indirect_placeholder();
    *(unsigned char *)6379472UL = (unsigned char)'\x00';
    var_10 = (uint32_t *)(var_0 + (-44L));
    local_sp_3 = var_9;
    while (1U)
        {
            var_11 = *var_6;
            var_12 = (uint64_t)*var_4;
            var_13 = local_sp_3 + (-8L);
            *(uint64_t *)var_13 = 4203628UL;
            var_14 = indirect_placeholder_14(4271078UL, 4270528UL, var_12, 0UL, var_11);
            var_15 = (uint32_t)var_14.field_0;
            *var_10 = var_15;
            local_sp_0 = var_13;
            local_sp_2 = var_13;
            local_sp_3 = var_13;
            if (var_15 != 4294967295U) {
                var_19 = var_14.field_1;
                var_20 = *var_4 - *(uint32_t *)6379288UL;
                *(uint32_t *)(var_0 + (-48L)) = var_20;
                if ((int)var_20 <= (int)0U) {
                    var_23 = (unsigned char *)(var_0 + (-25L));
                    *var_23 = (unsigned char)'\x01';
                    var_24 = *(uint32_t *)6379288UL;
                    loop_state_var = 1U;
                    break;
                }
                var_21 = *var_7;
                var_22 = local_sp_3 + (-16L);
                *(uint64_t *)var_22 = 4203679UL;
                indirect_placeholder();
                *(unsigned char *)(var_0 + (-25L)) = (unsigned char)var_21;
                local_sp_1 = var_22;
                loop_state_var = 2U;
                break;
            }
            if ((uint64_t)(var_15 + 130U) == 0UL) {
                *(uint64_t *)(local_sp_3 + (-16L)) = 4203516UL;
                indirect_placeholder_5(var_3, 0UL);
                abort();
            }
            if ((int)var_15 > (int)4294967166U) {
                if ((uint64_t)(var_15 + (-114)) == 0UL) {
                    *var_7 = 4202073UL;
                    continue;
                }
                if ((uint64_t)(var_15 + (-115)) != 0UL) {
                    loop_state_var = 0U;
                    break;
                }
                *var_7 = 4202682UL;
                continue;
            }
            if (var_15 != 4294967165U) {
                loop_state_var = 0U;
                break;
            }
            var_16 = *(uint64_t *)6379136UL;
            var_17 = local_sp_3 + (-24L);
            var_18 = (uint64_t *)var_17;
            *var_18 = 0UL;
            *(uint64_t *)(local_sp_3 + (-32L)) = 4203574UL;
            indirect_placeholder_13(0UL, 4270288UL, var_16, 4271060UL, 4271014UL, 4271044UL);
            *var_18 = 4203588UL;
            indirect_placeholder();
            local_sp_2 = var_17;
            loop_state_var = 0U;
            break;
        }
    switch (loop_state_var) {
      case 0U:
        {
            *(uint64_t *)(local_sp_2 + (-8L)) = 4203598UL;
            indirect_placeholder_5(var_3, 1UL);
            abort();
        }
        break;
      case 2U:
      case 1U:
        {
            switch (loop_state_var) {
              case 1U:
                {
                    local_sp_1 = local_sp_0;
                    while ((long)((uint64_t)var_24 << 32UL) >= (long)((uint64_t)*var_4 << 32UL))
                        {
                            var_28 = *var_7;
                            var_29 = local_sp_0 + (-8L);
                            *(uint64_t *)var_29 = 4203730UL;
                            indirect_placeholder();
                            *var_23 = ((var_28 & (uint64_t)*var_23) != 0UL);
                            var_30 = *(uint32_t *)6379288UL + 1U;
                            *(uint32_t *)6379288UL = var_30;
                            var_24 = var_30;
                            local_sp_0 = var_29;
                            local_sp_1 = local_sp_0;
                        }
                }
                break;
              case 2U:
                {
                    *(uint64_t *)(local_sp_1 + (-8L)) = 4203799UL;
                    var_25 = indirect_placeholder_9();
                    if (*(unsigned char *)6379472UL != '\x00' & (uint64_t)((uint32_t)var_25 + 1U) == 0UL) {
                        *(uint64_t *)(local_sp_1 + (-16L)) = 4203824UL;
                        var_26 = indirect_placeholder_2(4271018UL, 0UL, 3UL);
                        *(uint64_t *)(local_sp_1 + (-24L)) = 4203832UL;
                        indirect_placeholder();
                        var_27 = (uint64_t)*(uint32_t *)var_26;
                        *(uint64_t *)(local_sp_1 + (-32L)) = 4203859UL;
                        indirect_placeholder_1(0UL, 4271022UL, var_26, 1UL, 0UL, var_27, var_19);
                    }
                    return;
                }
                break;
            }
        }
        break;
    }
}

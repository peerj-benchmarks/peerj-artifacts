typedef unsigned char __u_char;
typedef unsigned short __u_short;
typedef unsigned int __u_int;
typedef unsigned long __u_long;
typedef signed char __int8_t;
typedef unsigned char __uint8_t;
typedef short __int16_t;
typedef unsigned short __uint16_t;
typedef int __int32_t;
typedef unsigned int __uint32_t;
typedef long __int64_t;
typedef unsigned long __uint64_t;
typedef __int8_t __int_least8_t;
typedef __uint8_t __uint_least8_t;
typedef __int16_t __int_least16_t;
typedef __uint16_t __uint_least16_t;
typedef __int32_t __int_least32_t;
typedef __uint32_t __uint_least32_t;
typedef __int64_t __int_least64_t;
typedef __uint64_t __uint_least64_t;
typedef long __quad_t;
typedef unsigned long __u_quad_t;
typedef long __intmax_t;
typedef unsigned long __uintmax_t;
typedef unsigned long __dev_t;
typedef unsigned int __uid_t;
typedef unsigned int __gid_t;
typedef unsigned long __ino_t;
typedef unsigned long __ino64_t;
typedef unsigned int __mode_t;
typedef unsigned long __nlink_t;
typedef long __off_t;
typedef long __off64_t;
typedef int __pid_t;
typedef struct {
    int __val[2];
} __fsid_t;
typedef long __clock_t;
typedef unsigned long __rlim_t;
typedef unsigned long __rlim64_t;
typedef unsigned int __id_t;
typedef long __time_t;
typedef unsigned int __useconds_t;
typedef long __suseconds_t;
typedef int __daddr_t;
typedef int __key_t;
typedef int __clockid_t;
typedef void *__timer_t;
typedef long __blksize_t;
typedef long __blkcnt_t;
typedef long __blkcnt64_t;
typedef unsigned long __fsblkcnt_t;
typedef unsigned long __fsblkcnt64_t;
typedef unsigned long __fsfilcnt_t;
typedef unsigned long __fsfilcnt64_t;
typedef long __fsword_t;
typedef long __ssize_t;
typedef long __syscall_slong_t;
typedef unsigned long __syscall_ulong_t;
typedef __off64_t __loff_t;
typedef char *__caddr_t;
typedef long __intptr_t;
typedef unsigned int __socklen_t;
typedef int __sig_atomic_t;
typedef __int8_t int8_t;
typedef __int16_t int16_t;
typedef __int32_t int32_t;
typedef __int64_t int64_t;
typedef __uint8_t uint8_t;
typedef __uint16_t uint16_t;
typedef __uint32_t uint32_t;
typedef __uint64_t uint64_t;
typedef __int_least8_t int_least8_t;
typedef __int_least16_t int_least16_t;
typedef __int_least32_t int_least32_t;
typedef __int_least64_t int_least64_t;
typedef __uint_least8_t uint_least8_t;
typedef __uint_least16_t uint_least16_t;
typedef __uint_least32_t uint_least32_t;
typedef __uint_least64_t uint_least64_t;
typedef signed char int_fast8_t;
typedef long int_fast16_t;
typedef long int_fast32_t;
typedef long int_fast64_t;
typedef unsigned char uint_fast8_t;
typedef unsigned long uint_fast16_t;
typedef unsigned long uint_fast32_t;
typedef unsigned long uint_fast64_t;
typedef long intptr_t;
typedef unsigned long uintptr_t;
typedef __intmax_t intmax_t;
typedef __uintmax_t uintmax_t;
struct helper_pxor_xmm_wrapper_85_ret_type;
struct type_3;
struct type_5;
struct helper_cvtsq2ss_wrapper_ret_type;
struct helper_mulss_wrapper_ret_type;
struct helper_addss_wrapper_ret_type;
struct helper_ucomiss_wrapper_ret_type;
struct helper_cvttss2sq_wrapper_ret_type;
struct helper_subss_wrapper_ret_type;
struct indirect_placeholder_73_ret_type;
struct helper_pxor_xmm_wrapper_85_ret_type {
    uint64_t field_0;
    uint64_t field_1;
};
struct type_3 {
};
struct type_5 {
};
struct helper_cvtsq2ss_wrapper_ret_type {
    uint64_t field_0;
    unsigned char field_1;
};
struct helper_mulss_wrapper_ret_type {
    uint64_t field_0;
    unsigned char field_1;
};
struct helper_addss_wrapper_ret_type {
    uint64_t field_0;
    unsigned char field_1;
};
struct helper_ucomiss_wrapper_ret_type {
    uint64_t field_0;
    unsigned char field_1;
};
struct helper_cvttss2sq_wrapper_ret_type {
    uint64_t field_0;
    unsigned char field_1;
};
struct helper_subss_wrapper_ret_type {
    uint64_t field_0;
    unsigned char field_1;
};
struct indirect_placeholder_73_ret_type {
    uint64_t field_0;
    uint64_t field_1;
};
extern uint64_t revng_init_local_sp(uint64_t param_0);
extern uint64_t init_rbp(void);
extern uint64_t init_state_0x8558(void);
extern unsigned char init_state_0x8549(void);
extern unsigned char init_state_0x854c(void);
extern unsigned char init_state_0x8548(void);
extern unsigned char init_state_0x854b(void);
extern unsigned char init_state_0x8547(void);
extern unsigned char init_state_0x854d(void);
extern uint64_t init_cc_src2(void);
extern struct helper_pxor_xmm_wrapper_85_ret_type helper_pxor_xmm_wrapper_85(struct type_3 *param_0, struct type_5 *param_1, struct type_5 *param_2, uint64_t param_3, uint64_t param_4);
extern uint64_t helper_cc_compute_c_wrapper(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint32_t param_3);
extern uint64_t indirect_placeholder_1(uint64_t param_0, uint64_t param_1);
extern uint64_t init_state_0x8560(void);
extern struct helper_cvtsq2ss_wrapper_ret_type helper_cvtsq2ss_wrapper(struct type_3 *param_0, struct type_5 *param_1, uint64_t param_2, unsigned char param_3, unsigned char param_4, unsigned char param_5, unsigned char param_6);
extern struct helper_mulss_wrapper_ret_type helper_mulss_wrapper(struct type_3 *param_0, struct type_5 *param_1, struct type_5 *param_2, uint64_t param_3, uint64_t param_4, unsigned char param_5, unsigned char param_6, unsigned char param_7, unsigned char param_8, unsigned char param_9, unsigned char param_10);
extern uint64_t init_rbx(void);
extern uint64_t init_state_0x8d58(void);
extern struct helper_addss_wrapper_ret_type helper_addss_wrapper(struct type_3 *param_0, struct type_5 *param_1, struct type_5 *param_2, uint64_t param_3, unsigned char param_4, unsigned char param_5, unsigned char param_6, unsigned char param_7, unsigned char param_8, unsigned char param_9);
extern struct helper_ucomiss_wrapper_ret_type helper_ucomiss_wrapper(struct type_3 *param_0, struct type_5 *param_1, struct type_5 *param_2, uint64_t param_3, uint64_t param_4, unsigned char param_5, unsigned char param_6);
extern struct helper_cvttss2sq_wrapper_ret_type helper_cvttss2sq_wrapper(struct type_3 *param_0, struct type_5 *param_1, uint64_t param_2, unsigned char param_3, unsigned char param_4);
extern struct helper_subss_wrapper_ret_type helper_subss_wrapper(struct type_3 *param_0, struct type_5 *param_1, struct type_5 *param_2, uint64_t param_3, uint64_t param_4, unsigned char param_5, unsigned char param_6, unsigned char param_7, unsigned char param_8, unsigned char param_9, unsigned char param_10);
extern struct indirect_placeholder_73_ret_type indirect_placeholder_73(uint64_t param_0);
uint64_t bb_convert_to_decimal(uint64_t rdx, uint64_t rdi, uint64_t rsi) {
    uint64_t *var_47;
    uint64_t *var_48;
    uint64_t var_63;
    uint64_t var_49;
    struct helper_cvtsq2ss_wrapper_ret_type var_20;
    uint64_t var_0;
    uint64_t var_1;
    uint64_t var_2;
    uint64_t var_3;
    uint64_t var_4;
    uint64_t var_5;
    uint64_t var_6;
    unsigned char var_7;
    unsigned char var_8;
    unsigned char var_9;
    unsigned char var_10;
    unsigned char var_11;
    unsigned char var_12;
    uint64_t *var_13;
    uint64_t *var_14;
    uint64_t *var_15;
    uint64_t var_16;
    uint64_t *var_17;
    uint64_t var_18;
    uint64_t *var_19;
    uint64_t var_39;
    uint64_t *var_40;
    uint64_t rax_0;
    uint64_t var_41;
    uint32_t *var_44;
    uint64_t var_45;
    uint64_t *var_46;
    uint64_t var_65;
    uint64_t var_64;
    uint64_t var_66;
    uint64_t var_50;
    uint64_t var_67;
    uint64_t var_51;
    uint64_t var_57;
    uint64_t var_61;
    uint64_t var_62;
    uint64_t var_58;
    uint64_t var_59;
    uint64_t var_60;
    uint64_t var_52;
    uint32_t **var_53;
    uint64_t var_54;
    uint64_t var_55;
    uint64_t var_56;
    uint64_t var_42;
    uint64_t var_43;
    uint64_t state_0x8558_0;
    uint64_t var_21;
    struct helper_cvtsq2ss_wrapper_ret_type var_22;
    struct helper_addss_wrapper_ret_type var_23;
    uint64_t cc_dst_0;
    unsigned char storemerge;
    struct helper_mulss_wrapper_ret_type var_24;
    uint64_t var_25;
    struct helper_ucomiss_wrapper_ret_type var_26;
    uint64_t var_27;
    unsigned char var_28;
    uint64_t var_29;
    struct helper_cvttss2sq_wrapper_ret_type var_30;
    struct helper_subss_wrapper_ret_type var_31;
    struct helper_cvttss2sq_wrapper_ret_type var_32;
    uint64_t var_33;
    uint64_t var_34;
    uint64_t var_35;
    struct indirect_placeholder_73_ret_type var_36;
    uint64_t var_37;
    uint64_t *var_38;
    var_0 = revng_init_local_sp(0UL);
    var_1 = init_rbp();
    var_2 = init_state_0x8558();
    var_3 = init_state_0x8560();
    var_4 = init_cc_src2();
    var_5 = init_rbx();
    var_6 = init_state_0x8d58();
    var_7 = init_state_0x8549();
    var_8 = init_state_0x854c();
    var_9 = init_state_0x8548();
    var_10 = init_state_0x854b();
    var_11 = init_state_0x8547();
    var_12 = init_state_0x854d();
    *(uint64_t *)(var_0 + (-8L)) = var_1;
    *(uint64_t *)(var_0 + (-16L)) = var_5;
    var_13 = (uint64_t *)(var_0 + (-120L));
    *var_13 = rdi;
    var_14 = (uint64_t *)(var_0 + (-112L));
    *var_14 = rsi;
    var_15 = (uint64_t *)(var_0 + (-128L));
    *var_15 = rdx;
    var_16 = *var_14;
    var_17 = (uint64_t *)(var_0 + (-72L));
    *var_17 = var_16;
    var_18 = *var_13;
    var_19 = (uint64_t *)(var_0 + (-32L));
    *var_19 = var_18;
    var_57 = 9UL;
    var_67 = 0UL;
    cc_dst_0 = var_18;
    if ((long)var_18 < (long)0UL) {
        var_21 = (var_18 >> 1UL) | (var_18 & 1UL);
        helper_pxor_xmm_wrapper_85((struct type_3 *)(0UL), (struct type_5 *)(776UL), (struct type_5 *)(776UL), var_2, var_3);
        var_22 = helper_cvtsq2ss_wrapper((struct type_3 *)(0UL), (struct type_5 *)(776UL), var_21, var_7, var_9, var_10, var_11);
        var_23 = helper_addss_wrapper((struct type_3 *)(0UL), (struct type_5 *)(776UL), (struct type_5 *)(776UL), var_22.field_0, var_22.field_1, var_8, var_9, var_10, var_11, var_12);
        state_0x8558_0 = var_23.field_0;
        cc_dst_0 = var_21;
        storemerge = var_23.field_1;
    } else {
        helper_pxor_xmm_wrapper_85((struct type_3 *)(0UL), (struct type_5 *)(776UL), (struct type_5 *)(776UL), var_2, var_3);
        var_20 = helper_cvtsq2ss_wrapper((struct type_3 *)(0UL), (struct type_5 *)(776UL), var_18, var_7, var_9, var_10, var_11);
        state_0x8558_0 = var_20.field_0;
        storemerge = var_20.field_1;
    }
    var_24 = helper_mulss_wrapper((struct type_3 *)(0UL), (struct type_5 *)(776UL), (struct type_5 *)(840UL), state_0x8558_0, (uint64_t)*(uint32_t *)4281264UL, storemerge, var_8, var_9, var_10, var_11, var_12);
    var_25 = var_24.field_0;
    var_26 = helper_ucomiss_wrapper((struct type_3 *)(0UL), (struct type_5 *)(776UL), (struct type_5 *)(2824UL), var_25, (var_6 & (-4294967296L)) | (uint64_t)*(uint32_t *)4281268UL, var_24.field_1, var_8);
    var_27 = var_26.field_0;
    var_28 = var_26.field_1;
    var_29 = helper_cc_compute_c_wrapper(cc_dst_0, var_27, var_4, 1U);
    if (var_29 == 0UL) {
        var_31 = helper_subss_wrapper((struct type_3 *)(0UL), (struct type_5 *)(776UL), (struct type_5 *)(840UL), var_25, (uint64_t)*(uint32_t *)4281268UL, var_28, var_8, var_9, var_10, var_11, var_12);
        var_32 = helper_cvttss2sq_wrapper((struct type_3 *)(0UL), (struct type_5 *)(776UL), var_31.field_0, var_31.field_1, var_8);
        rax_0 = var_32.field_0 ^ (-9223372036854775808L);
    } else {
        var_30 = helper_cvttss2sq_wrapper((struct type_3 *)(0UL), (struct type_5 *)(776UL), var_25, var_28, var_8);
        rax_0 = var_30.field_0;
    }
    var_33 = (rax_0 * 9UL) + 9UL;
    *(uint64_t *)(var_0 + (-80L)) = var_33;
    var_34 = *var_15;
    *(uint64_t *)(var_0 + (-144L)) = 4228129UL;
    var_35 = indirect_placeholder_1(var_33, var_34);
    *(uint64_t *)(var_0 + (-152L)) = 4228137UL;
    var_36 = indirect_placeholder_73(var_35);
    var_37 = var_36.field_0;
    var_38 = (uint64_t *)(var_0 + (-88L));
    *var_38 = var_37;
    if (var_37 == 0UL) {
        return var_67;
    }
    var_39 = var_0 + (-40L);
    var_40 = (uint64_t *)var_39;
    *var_40 = var_37;
    var_41 = *var_15;
    while (var_41 != 0UL)
        {
            var_42 = *var_40;
            *var_40 = (var_42 + 1UL);
            *(unsigned char *)var_42 = (unsigned char)'0';
            var_43 = *var_15 + (-1L);
            *var_15 = var_43;
            var_41 = var_43;
        }
    var_44 = (uint32_t *)(var_0 + (-44L));
    var_45 = var_0 + (-56L);
    var_46 = (uint64_t *)var_45;
    var_47 = (uint64_t *)(var_0 + (-64L));
    var_48 = (uint64_t *)(var_0 + (-96L));
    var_49 = *var_19;
    while (var_49 != 0UL)
        {
            *var_44 = 0U;
            *var_46 = (*var_17 + (*var_19 << 2UL));
            var_50 = *var_19;
            *var_47 = var_50;
            var_51 = var_50;
            while (var_51 != 0UL)
                {
                    var_52 = (uint64_t)*var_44 << 32UL;
                    *var_46 = (*var_46 + (-4L));
                    var_53 = (uint32_t **)var_45;
                    var_54 = var_52 | (uint64_t)**var_53;
                    *var_48 = var_54;
                    **var_53 = (uint32_t)(((unsigned __int128)(var_54 >> 9UL) * 19342813113834067ULL) >> 75ULL);
                    var_55 = *var_48;
                    *var_44 = ((uint32_t)var_55 + ((uint32_t)(uint64_t)(((unsigned __int128)(var_55 >> 9UL) * 19342813113834067ULL) >> 75ULL) * 3294967296U));
                    var_56 = *var_47 + (-1L);
                    *var_47 = var_56;
                    var_51 = var_56;
                }
            *var_47 = 9UL;
            while (var_57 != 0UL)
                {
                    var_58 = *var_40;
                    *var_40 = (var_58 + 1UL);
                    var_59 = (uint64_t)*var_44;
                    *(unsigned char *)var_58 = ((((unsigned char)((var_59 * 3435973837UL) >> 35UL) * '\xf6') + (unsigned char)var_59) + '0');
                    *var_44 = (uint32_t)(((uint64_t)*var_44 * 3435973837UL) >> 35UL);
                    var_60 = *var_47 + (-1L);
                    *var_47 = var_60;
                    var_57 = var_60;
                }
            var_61 = *var_19;
            var_63 = var_61;
            if (*(uint32_t *)(*var_17 + ((var_61 << 2UL) + (-4L))) == 0U) {
                var_62 = var_61 + (-1L);
                *var_19 = var_62;
                var_63 = var_62;
            }
            var_49 = var_63;
        }
    var_64 = *var_40;
    var_65 = *var_38;
    while (var_64 <= var_65)
        {
            var_66 = var_64 + (-1L);
            var_64 = var_66;
            if (*(unsigned char *)var_66 == '0') {
                break;
            }
            *var_40 = var_66;
            var_65 = *var_38;
        }
    if (var_64 == var_65) {
        *var_40 = (var_64 + 1UL);
        *(unsigned char *)var_64 = (unsigned char)'0';
    }
    **(unsigned char **)var_39 = (unsigned char)'\x00';
    var_67 = *var_38;
    return var_67;
}

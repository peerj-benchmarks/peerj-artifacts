typedef unsigned char __u_char;
typedef unsigned short __u_short;
typedef unsigned int __u_int;
typedef unsigned long __u_long;
typedef signed char __int8_t;
typedef unsigned char __uint8_t;
typedef short __int16_t;
typedef unsigned short __uint16_t;
typedef int __int32_t;
typedef unsigned int __uint32_t;
typedef long __int64_t;
typedef unsigned long __uint64_t;
typedef __int8_t __int_least8_t;
typedef __uint8_t __uint_least8_t;
typedef __int16_t __int_least16_t;
typedef __uint16_t __uint_least16_t;
typedef __int32_t __int_least32_t;
typedef __uint32_t __uint_least32_t;
typedef __int64_t __int_least64_t;
typedef __uint64_t __uint_least64_t;
typedef long __quad_t;
typedef unsigned long __u_quad_t;
typedef long __intmax_t;
typedef unsigned long __uintmax_t;
typedef unsigned long __dev_t;
typedef unsigned int __uid_t;
typedef unsigned int __gid_t;
typedef unsigned long __ino_t;
typedef unsigned long __ino64_t;
typedef unsigned int __mode_t;
typedef unsigned long __nlink_t;
typedef long __off_t;
typedef long __off64_t;
typedef int __pid_t;
typedef struct {
    int __val[2];
} __fsid_t;
typedef long __clock_t;
typedef unsigned long __rlim_t;
typedef unsigned long __rlim64_t;
typedef unsigned int __id_t;
typedef long __time_t;
typedef unsigned int __useconds_t;
typedef long __suseconds_t;
typedef int __daddr_t;
typedef int __key_t;
typedef int __clockid_t;
typedef void *__timer_t;
typedef long __blksize_t;
typedef long __blkcnt_t;
typedef long __blkcnt64_t;
typedef unsigned long __fsblkcnt_t;
typedef unsigned long __fsblkcnt64_t;
typedef unsigned long __fsfilcnt_t;
typedef unsigned long __fsfilcnt64_t;
typedef long __fsword_t;
typedef long __ssize_t;
typedef long __syscall_slong_t;
typedef unsigned long __syscall_ulong_t;
typedef __off64_t __loff_t;
typedef char *__caddr_t;
typedef long __intptr_t;
typedef unsigned int __socklen_t;
typedef int __sig_atomic_t;
typedef __int8_t int8_t;
typedef __int16_t int16_t;
typedef __int32_t int32_t;
typedef __int64_t int64_t;
typedef __uint8_t uint8_t;
typedef __uint16_t uint16_t;
typedef __uint32_t uint32_t;
typedef __uint64_t uint64_t;
typedef __int_least8_t int_least8_t;
typedef __int_least16_t int_least16_t;
typedef __int_least32_t int_least32_t;
typedef __int_least64_t int_least64_t;
typedef __uint_least8_t uint_least8_t;
typedef __uint_least16_t uint_least16_t;
typedef __uint_least32_t uint_least32_t;
typedef __uint_least64_t uint_least64_t;
typedef signed char int_fast8_t;
typedef long int_fast16_t;
typedef long int_fast32_t;
typedef long int_fast64_t;
typedef unsigned char uint_fast8_t;
typedef unsigned long uint_fast16_t;
typedef unsigned long uint_fast32_t;
typedef unsigned long uint_fast64_t;
typedef long intptr_t;
typedef unsigned long uintptr_t;
typedef __intmax_t intmax_t;
typedef __uintmax_t uintmax_t;
typedef _Bool bool;
extern uint64_t revng_init_local_sp(uint64_t param_0);
extern uint64_t init_rbp(void);
uint64_t bb_fraccompare(uint64_t rdx, uint64_t rdi, uint64_t rsi) {
    uint64_t var_11;
    uint64_t var_12;
    uint64_t var_24;
    uint64_t rax_0;
    uint64_t var_0;
    uint64_t var_1;
    uint64_t var_2;
    uint64_t *var_3;
    uint64_t var_4;
    uint64_t *var_5;
    unsigned char *var_6;
    unsigned char var_7;
    unsigned char **var_8;
    unsigned char **var_9;
    unsigned char var_13;
    uint64_t var_14;
    unsigned char var_15;
    bool var_16;
    uint32_t var_17;
    bool var_18;
    uint32_t var_19;
    uint64_t var_20;
    uint64_t var_23;
    uint64_t var_10;
    unsigned char var_21;
    uint64_t var_22;
    unsigned char var_25;
    uint64_t var_26;
    uint64_t var_27;
    unsigned int loop_state_var;
    var_0 = revng_init_local_sp(0UL);
    var_1 = init_rbp();
    *(uint64_t *)(var_0 + (-8L)) = var_1;
    var_2 = var_0 + (-16L);
    var_3 = (uint64_t *)var_2;
    *var_3 = rdi;
    var_4 = var_0 + (-24L);
    var_5 = (uint64_t *)var_4;
    *var_5 = rsi;
    var_6 = (unsigned char *)(var_0 + (-28L));
    var_7 = (unsigned char)rdx;
    *var_6 = var_7;
    var_8 = (unsigned char **)var_2;
    rax_0 = 0UL;
    if ((uint64_t)(**var_8 - var_7) == 0UL) {
        var_10 = *var_3;
        *var_3 = (var_10 + 1UL);
        if ((uint64_t)(*(unsigned char *)var_10 - *var_6) != 0UL) {
            var_21 = **var_8;
            while (var_21 != '0')
                {
                    *var_3 = (*var_3 + 1UL);
                    var_21 = **var_8;
                }
            var_22 = ((uint64_t)(((uint32_t)(uint64_t)var_21 + (-48)) & (-2)) < 10UL);
            rax_0 = var_22;
            return rax_0;
        }
        var_11 = *var_5;
        var_12 = var_11 + 1UL;
        *var_5 = var_12;
        var_23 = var_12;
        if ((uint64_t)(*(unsigned char *)var_11 - *var_6) == 0UL) {
            return rax_0;
        }
        var_24 = var_23;
        var_25 = **(unsigned char **)var_4;
        while (var_25 != '0')
            {
                var_27 = var_24 + 1UL;
                *var_5 = var_27;
                var_24 = var_27;
                var_25 = **(unsigned char **)var_4;
            }
        var_26 = ((uint64_t)(((uint32_t)(uint64_t)var_25 + (-48)) & (-2)) < 10UL) ? 4294967295UL : 0UL;
        rax_0 = var_26;
        return rax_0;
    }
    var_9 = (unsigned char **)var_4;
    if ((uint64_t)(**var_9 - var_7) == 0UL) {
        return;
    }
    while (1U)
        {
            *var_3 = (*var_3 + 1UL);
            var_13 = **var_8;
            var_14 = *var_5 + 1UL;
            *var_5 = var_14;
            var_15 = **var_9;
            var_16 = ((uint64_t)(var_13 - var_15) == 0UL);
            var_17 = (uint32_t)(uint64_t)**var_8;
            var_18 = ((uint64_t)((var_17 + (-48)) & (-2)) > 9UL);
            var_23 = var_14;
            if (var_16) {
                if (var_18) {
                    continue;
                }
                loop_state_var = 1U;
                break;
            }
            if (!var_18) {
                loop_state_var = 0U;
                break;
            }
            var_19 = (uint32_t)(uint64_t)var_15;
            if ((uint64_t)((var_19 + (-48)) & (-2)) <= 9UL) {
                loop_state_var = 0U;
                break;
            }
            var_20 = (uint64_t)(var_17 - var_19);
            rax_0 = var_20;
            loop_state_var = 1U;
            break;
        }
    switch (loop_state_var) {
      case 1U:
        {
            return rax_0;
        }
        break;
      case 0U:
        {
            if (!var_18) {
                var_21 = **var_8;
                while (var_21 != '0')
                    {
                        *var_3 = (*var_3 + 1UL);
                        var_21 = **var_8;
                    }
                var_22 = ((uint64_t)(((uint32_t)(uint64_t)var_21 + (-48)) & (-2)) < 10UL);
                rax_0 = var_22;
                return rax_0;
            }
            if ((uint64_t)(((uint32_t)(uint64_t)var_15 + (-48)) & (-2)) > 9UL) {
                return rax_0;
            }
            var_24 = var_23;
            var_25 = **(unsigned char **)var_4;
            while (var_25 != '0')
                {
                    var_27 = var_24 + 1UL;
                    *var_5 = var_27;
                    var_24 = var_27;
                    var_25 = **(unsigned char **)var_4;
                }
            var_26 = ((uint64_t)(((uint32_t)(uint64_t)var_25 + (-48)) & (-2)) < 10UL) ? 4294967295UL : 0UL;
            rax_0 = var_26;
            return rax_0;
        }
        break;
    }
}

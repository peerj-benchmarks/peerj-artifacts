typedef unsigned char __u_char;
typedef unsigned short __u_short;
typedef unsigned int __u_int;
typedef unsigned long __u_long;
typedef signed char __int8_t;
typedef unsigned char __uint8_t;
typedef short __int16_t;
typedef unsigned short __uint16_t;
typedef int __int32_t;
typedef unsigned int __uint32_t;
typedef long __int64_t;
typedef unsigned long __uint64_t;
typedef __int8_t __int_least8_t;
typedef __uint8_t __uint_least8_t;
typedef __int16_t __int_least16_t;
typedef __uint16_t __uint_least16_t;
typedef __int32_t __int_least32_t;
typedef __uint32_t __uint_least32_t;
typedef __int64_t __int_least64_t;
typedef __uint64_t __uint_least64_t;
typedef long __quad_t;
typedef unsigned long __u_quad_t;
typedef long __intmax_t;
typedef unsigned long __uintmax_t;
typedef unsigned long __dev_t;
typedef unsigned int __uid_t;
typedef unsigned int __gid_t;
typedef unsigned long __ino_t;
typedef unsigned long __ino64_t;
typedef unsigned int __mode_t;
typedef unsigned long __nlink_t;
typedef long __off_t;
typedef long __off64_t;
typedef int __pid_t;
typedef struct {
    int __val[2];
} __fsid_t;
typedef long __clock_t;
typedef unsigned long __rlim_t;
typedef unsigned long __rlim64_t;
typedef unsigned int __id_t;
typedef long __time_t;
typedef unsigned int __useconds_t;
typedef long __suseconds_t;
typedef int __daddr_t;
typedef int __key_t;
typedef int __clockid_t;
typedef void *__timer_t;
typedef long __blksize_t;
typedef long __blkcnt_t;
typedef long __blkcnt64_t;
typedef unsigned long __fsblkcnt_t;
typedef unsigned long __fsblkcnt64_t;
typedef unsigned long __fsfilcnt_t;
typedef unsigned long __fsfilcnt64_t;
typedef long __fsword_t;
typedef long __ssize_t;
typedef long __syscall_slong_t;
typedef unsigned long __syscall_ulong_t;
typedef __off64_t __loff_t;
typedef char *__caddr_t;
typedef long __intptr_t;
typedef unsigned int __socklen_t;
typedef int __sig_atomic_t;
typedef __int8_t int8_t;
typedef __int16_t int16_t;
typedef __int32_t int32_t;
typedef __int64_t int64_t;
typedef __uint8_t uint8_t;
typedef __uint16_t uint16_t;
typedef __uint32_t uint32_t;
typedef __uint64_t uint64_t;
typedef __int_least8_t int_least8_t;
typedef __int_least16_t int_least16_t;
typedef __int_least32_t int_least32_t;
typedef __int_least64_t int_least64_t;
typedef __uint_least8_t uint_least8_t;
typedef __uint_least16_t uint_least16_t;
typedef __uint_least32_t uint_least32_t;
typedef __uint_least64_t uint_least64_t;
typedef signed char int_fast8_t;
typedef long int_fast16_t;
typedef long int_fast32_t;
typedef long int_fast64_t;
typedef unsigned char uint_fast8_t;
typedef unsigned long uint_fast16_t;
typedef unsigned long uint_fast32_t;
typedef unsigned long uint_fast64_t;
typedef long intptr_t;
typedef unsigned long uintptr_t;
typedef __intmax_t intmax_t;
typedef __uintmax_t uintmax_t;
extern uint64_t revng_init_local_sp(uint64_t param_0);
extern uint64_t init_rbp(void);
extern uint64_t init_cc_src2(void);
extern uint64_t helper_cc_compute_c_wrapper(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint32_t param_3);
extern uint64_t indirect_placeholder_6(uint64_t param_0, uint64_t param_1, uint64_t param_2);
uint64_t bb_numcompare(uint64_t rdx, uint64_t rcx, uint64_t rdi, uint64_t rsi) {
    uint64_t var_0;
    uint64_t var_1;
    uint64_t var_2;
    uint64_t var_3;
    uint64_t *var_4;
    uint64_t var_5;
    uint64_t *var_6;
    uint32_t *var_7;
    uint32_t *var_8;
    unsigned char **var_9;
    unsigned char var_10;
    unsigned char *var_11;
    unsigned char **var_12;
    unsigned char var_13;
    unsigned char *var_14;
    unsigned char var_15;
    unsigned char var_54;
    uint32_t var_55;
    uint32_t var_56;
    uint32_t _pre_phi180;
    unsigned char var_57;
    uint64_t rax_0;
    unsigned char var_58;
    uint32_t var_59;
    uint32_t var_60;
    uint32_t _pre_phi184;
    unsigned char var_61;
    uint64_t var_62;
    unsigned char var_63;
    unsigned char var_64;
    unsigned char var_65;
    unsigned char var_66;
    uint64_t var_67;
    uint64_t var_68;
    uint32_t _pre_phi;
    uint32_t var_69;
    unsigned char var_70;
    unsigned char var_71;
    uint32_t var_72;
    uint32_t _pre_phi176;
    uint32_t var_73;
    uint32_t var_22;
    uint64_t var_85;
    uint64_t var_86;
    uint64_t var_87;
    uint64_t var_88;
    uint32_t var_74;
    uint32_t *var_75;
    uint64_t *var_76;
    uint64_t *var_78;
    uint64_t var_79;
    uint64_t var_82;
    uint64_t var_83;
    uint64_t var_84;
    unsigned char var_80;
    uint64_t var_81;
    unsigned char var_77;
    unsigned char var_44;
    uint32_t var_45;
    uint32_t var_46;
    uint32_t _pre_phi188;
    unsigned char var_47;
    unsigned char var_16;
    unsigned char var_48;
    uint32_t var_49;
    uint32_t var_50;
    uint32_t _pre_phi192;
    unsigned char var_51;
    uint64_t var_52;
    unsigned char var_53;
    unsigned char var_17;
    unsigned char var_18;
    unsigned char var_19;
    uint64_t var_20;
    uint64_t var_21;
    uint32_t _pre_phi194;
    unsigned char var_23;
    unsigned char var_24;
    uint32_t var_25;
    uint32_t _pre_phi196;
    uint32_t var_26;
    uint64_t var_38;
    uint64_t var_39;
    uint64_t var_40;
    uint64_t var_41;
    uint32_t var_27;
    uint32_t *var_28;
    uint64_t *var_29;
    uint64_t *var_31;
    uint64_t var_32;
    uint64_t var_35;
    uint64_t var_36;
    uint64_t spec_select171;
    uint64_t var_37;
    unsigned char var_33;
    uint64_t var_34;
    unsigned char var_30;
    unsigned char var_42;
    unsigned char var_43;
    var_0 = revng_init_local_sp(0UL);
    var_1 = init_rbp();
    var_2 = init_cc_src2();
    *(uint64_t *)(var_0 + (-8L)) = var_1;
    var_3 = var_0 + (-48L);
    var_4 = (uint64_t *)var_3;
    *var_4 = rdi;
    var_5 = var_0 + (-56L);
    var_6 = (uint64_t *)var_5;
    *var_6 = rsi;
    var_7 = (uint32_t *)(var_0 + (-60L));
    *var_7 = (uint32_t)rdx;
    var_8 = (uint32_t *)(var_0 + (-64L));
    *var_8 = (uint32_t)rcx;
    var_9 = (unsigned char **)var_3;
    var_10 = **var_9;
    var_11 = (unsigned char *)(var_0 + (-9L));
    *var_11 = var_10;
    var_12 = (unsigned char **)var_5;
    var_13 = **var_12;
    var_14 = (unsigned char *)(var_0 + (-10L));
    *var_14 = var_13;
    var_15 = *var_11;
    var_79 = 0UL;
    var_16 = var_15;
    var_32 = 0UL;
    rax_0 = 0UL;
    if (var_15 == '-') {
        if (var_13 == '-') {
            while (1U)
                {
                    if (var_16 != '0') {
                        if ((uint64_t)((uint32_t)(uint64_t)var_16 - *var_8) == 0UL) {
                            break;
                        }
                    }
                    *var_4 = (*var_4 + 1UL);
                    var_43 = **var_9;
                    *var_11 = var_43;
                    var_16 = var_43;
                    continue;
                }
            var_17 = *var_14;
            while (1U)
                {
                    var_18 = var_17;
                    if (var_17 != '0') {
                        if ((uint64_t)((uint32_t)(uint64_t)var_17 - *var_8) == 0UL) {
                            break;
                        }
                    }
                    *var_6 = (*var_6 + 1UL);
                    var_42 = **var_12;
                    *var_14 = var_42;
                    var_17 = var_42;
                    continue;
                }
            while (1U)
                {
                    var_19 = *var_11;
                    var_20 = (uint64_t)var_19;
                    var_21 = (uint64_t)var_18;
                    if ((uint64_t)(var_19 - var_18) != 0UL) {
                        _pre_phi194 = (uint32_t)var_20;
                        break;
                    }
                    var_22 = (uint32_t)var_20;
                    _pre_phi194 = var_22;
                    if ((uint64_t)((var_22 + (-48)) & (-2)) <= 9UL) {
                        break;
                    }
                    *var_4 = (*var_4 + 1UL);
                    var_23 = **var_9;
                    *var_11 = var_23;
                    do {
                        *var_4 = (*var_4 + 1UL);
                        var_23 = **var_9;
                        *var_11 = var_23;
                    } while ((uint64_t)((uint32_t)(uint64_t)var_23 - *var_8) != 0UL);
                    *var_6 = (*var_6 + 1UL);
                    var_24 = **var_12;
                    *var_14 = var_24;
                    var_18 = var_24;
                    do {
                        *var_6 = (*var_6 + 1UL);
                        var_24 = **var_12;
                        *var_14 = var_24;
                        var_18 = var_24;
                    } while ((uint64_t)((uint32_t)(uint64_t)var_24 - *var_8) != 0UL);
                    continue;
                }
            var_25 = *var_7;
            if ((uint64_t)(_pre_phi194 - var_25) == 0UL) {
                var_26 = (uint32_t)var_21;
                _pre_phi196 = var_26;
                if ((uint64_t)((var_26 + (-48)) & (-2)) <= 9UL) {
                    var_38 = (uint64_t)(uint32_t)((int)(var_25 << 24U) >> (int)24U);
                    var_39 = *var_6;
                    var_40 = *var_4;
                    *(uint64_t *)(var_0 + (-72L)) = 4217168UL;
                    var_41 = indirect_placeholder_6(var_38, var_40, var_39);
                    rax_0 = var_41;
                    return rax_0;
                }
            }
            _pre_phi196 = (uint32_t)var_21;
            if ((uint64_t)(_pre_phi196 - var_25) != 0UL) {
                if ((uint64_t)((_pre_phi194 + (-48)) & (-2)) <= 9UL) {
                    var_38 = (uint64_t)(uint32_t)((int)(var_25 << 24U) >> (int)24U);
                    var_39 = *var_6;
                    var_40 = *var_4;
                    *(uint64_t *)(var_0 + (-72L)) = 4217168UL;
                    var_41 = indirect_placeholder_6(var_38, var_40, var_39);
                    rax_0 = var_41;
                    return rax_0;
                }
            }
            var_27 = (uint32_t)var_19 - (uint32_t)var_18;
            var_28 = (uint32_t *)(var_0 + (-36L));
            *var_28 = var_27;
            var_29 = (uint64_t *)(var_0 + (-24L));
            *var_29 = 0UL;
            while ((uint64_t)(((uint32_t)(uint64_t)*var_11 + (-48)) & (-2)) <= 9UL)
                {
                    *var_4 = (*var_4 + 1UL);
                    var_30 = **var_9;
                    *var_11 = var_30;
                    do {
                        *var_4 = (*var_4 + 1UL);
                        var_30 = **var_9;
                        *var_11 = var_30;
                    } while ((uint64_t)((uint32_t)(uint64_t)var_30 - *var_8) != 0UL);
                    *var_29 = (*var_29 + 1UL);
                }
            var_31 = (uint64_t *)(var_0 + (-32L));
            *var_31 = 0UL;
            while ((uint64_t)(((uint32_t)(uint64_t)*var_14 + (-48)) & (-2)) <= 9UL)
                {
                    *var_6 = (*var_6 + 1UL);
                    var_33 = **var_12;
                    *var_14 = var_33;
                    do {
                        *var_6 = (*var_6 + 1UL);
                        var_33 = **var_12;
                        *var_14 = var_33;
                    } while ((uint64_t)((uint32_t)(uint64_t)var_33 - *var_8) != 0UL);
                    var_34 = *var_31 + 1UL;
                    *var_31 = var_34;
                    var_32 = var_34;
                }
            var_35 = *var_29;
            if (var_35 == var_32) {
                var_36 = helper_cc_compute_c_wrapper(var_35 - var_32, var_32, var_2, 17U);
                spec_select171 = (var_36 == 0UL) ? 1UL : 4294967295UL;
                rax_0 = spec_select171;
            } else {
                if (var_32 == 0UL) {
                    var_37 = (uint64_t)*var_28;
                    rax_0 = var_37;
                }
            }
        } else {
            while (1U)
                {
                    *var_6 = (*var_6 + 1UL);
                    var_44 = **var_12;
                    *var_14 = var_44;
                    rax_0 = 1UL;
                    if (var_44 != '0') {
                        continue;
                    }
                    var_45 = *var_8;
                    var_46 = (uint32_t)(uint64_t)var_44;
                    _pre_phi188 = var_46;
                    if ((uint64_t)(var_46 - var_45) != 0UL) {
                        continue;
                    }
                    break;
                }
            if ((uint64_t)(var_46 - *var_7) != 0UL) {
                *var_6 = (*var_6 + 1UL);
                var_47 = **var_12;
                *var_14 = var_47;
                do {
                    *var_6 = (*var_6 + 1UL);
                    var_47 = **var_12;
                    *var_14 = var_47;
                } while (var_47 != '0');
                _pre_phi188 = (uint32_t)(uint64_t)var_47;
            }
            if ((uint64_t)((_pre_phi188 + (-48)) & (-2)) <= 9UL) {
                var_48 = *var_11;
                while (1U)
                    {
                        if (var_48 != '0') {
                            var_49 = *var_8;
                            var_50 = (uint32_t)(uint64_t)var_48;
                            _pre_phi192 = var_50;
                            if ((uint64_t)(var_50 - var_49) == 0UL) {
                                break;
                            }
                        }
                        *var_4 = (*var_4 + 1UL);
                        var_53 = **var_9;
                        *var_11 = var_53;
                        var_48 = var_53;
                        continue;
                    }
                if ((uint64_t)(var_50 - *var_7) != 0UL) {
                    *var_4 = (*var_4 + 1UL);
                    var_51 = **var_9;
                    *var_11 = var_51;
                    do {
                        *var_4 = (*var_4 + 1UL);
                        var_51 = **var_9;
                        *var_11 = var_51;
                    } while (var_51 != '0');
                    _pre_phi192 = (uint32_t)(uint64_t)var_51;
                }
                var_52 = ((uint64_t)((_pre_phi192 + (-48)) & (-2)) < 10UL);
                rax_0 = var_52;
            }
        }
    } else {
        while (1U)
            {
                *var_4 = (*var_4 + 1UL);
                var_54 = **var_9;
                *var_11 = var_54;
                rax_0 = 4294967295UL;
                if (var_54 != '0') {
                    continue;
                }
                var_55 = *var_8;
                var_56 = (uint32_t)(uint64_t)var_54;
                _pre_phi180 = var_56;
                if ((uint64_t)(var_56 - var_55) != 0UL) {
                    continue;
                }
                break;
            }
        if (*var_14 == '-') {
            if ((uint64_t)(var_56 - *var_7) != 0UL) {
                *var_4 = (*var_4 + 1UL);
                var_57 = **var_9;
                *var_11 = var_57;
                do {
                    *var_4 = (*var_4 + 1UL);
                    var_57 = **var_9;
                    *var_11 = var_57;
                } while (var_57 != '0');
                _pre_phi180 = (uint32_t)(uint64_t)var_57;
            }
            if ((uint64_t)((_pre_phi180 + (-48)) & (-2)) <= 9UL) {
                var_58 = *var_14;
                while (1U)
                    {
                        if (var_58 != '0') {
                            var_59 = *var_8;
                            var_60 = (uint32_t)(uint64_t)var_58;
                            _pre_phi184 = var_60;
                            if ((uint64_t)(var_60 - var_59) == 0UL) {
                                break;
                            }
                        }
                        *var_6 = (*var_6 + 1UL);
                        var_63 = **var_12;
                        *var_14 = var_63;
                        var_58 = var_63;
                        continue;
                    }
                if ((uint64_t)(var_60 - *var_7) != 0UL) {
                    *var_6 = (*var_6 + 1UL);
                    var_61 = **var_12;
                    *var_14 = var_61;
                    do {
                        *var_6 = (*var_6 + 1UL);
                        var_61 = **var_12;
                        *var_14 = var_61;
                    } while (var_61 != '0');
                    _pre_phi184 = (uint32_t)(uint64_t)var_61;
                }
                var_62 = ((uint64_t)((_pre_phi184 + (-48)) & (-2)) < 10UL) ? 4294967295UL : 0UL;
                rax_0 = var_62;
            }
        } else {
            while (1U)
                {
                    *var_6 = (*var_6 + 1UL);
                    var_64 = **var_12;
                    *var_14 = var_64;
                    var_65 = var_64;
                    if (var_64 != '0') {
                        continue;
                    }
                    if ((uint64_t)((uint32_t)(uint64_t)var_64 - *var_8) != 0UL) {
                        continue;
                    }
                    break;
                }
            while (1U)
                {
                    var_66 = *var_11;
                    var_67 = (uint64_t)var_66;
                    var_68 = (uint64_t)var_65;
                    if ((uint64_t)(var_66 - var_65) != 0UL) {
                        _pre_phi = (uint32_t)var_67;
                        break;
                    }
                    var_69 = (uint32_t)var_67;
                    _pre_phi = var_69;
                    if ((uint64_t)((var_69 + (-48)) & (-2)) <= 9UL) {
                        break;
                    }
                    *var_4 = (*var_4 + 1UL);
                    var_70 = **var_9;
                    *var_11 = var_70;
                    do {
                        *var_4 = (*var_4 + 1UL);
                        var_70 = **var_9;
                        *var_11 = var_70;
                    } while ((uint64_t)((uint32_t)(uint64_t)var_70 - *var_8) != 0UL);
                    *var_6 = (*var_6 + 1UL);
                    var_71 = **var_12;
                    *var_14 = var_71;
                    var_65 = var_71;
                    do {
                        *var_6 = (*var_6 + 1UL);
                        var_71 = **var_12;
                        *var_14 = var_71;
                        var_65 = var_71;
                    } while ((uint64_t)((uint32_t)(uint64_t)var_71 - *var_8) != 0UL);
                    continue;
                }
            var_72 = *var_7;
            if ((uint64_t)(_pre_phi - var_72) == 0UL) {
                var_73 = (uint32_t)var_68;
                _pre_phi176 = var_73;
                if ((uint64_t)((var_73 + (-48)) & (-2)) <= 9UL) {
                    var_85 = (uint64_t)(uint32_t)((int)(var_72 << 24U) >> (int)24U);
                    var_86 = *var_4;
                    var_87 = *var_6;
                    *(uint64_t *)(var_0 + (-72L)) = 4216604UL;
                    var_88 = indirect_placeholder_6(var_85, var_87, var_86);
                    rax_0 = var_88;
                    return rax_0;
                }
            }
            _pre_phi176 = (uint32_t)var_68;
            if ((uint64_t)(_pre_phi176 - var_72) != 0UL) {
                if ((uint64_t)((_pre_phi + (-48)) & (-2)) <= 9UL) {
                    var_85 = (uint64_t)(uint32_t)((int)(var_72 << 24U) >> (int)24U);
                    var_86 = *var_4;
                    var_87 = *var_6;
                    *(uint64_t *)(var_0 + (-72L)) = 4216604UL;
                    var_88 = indirect_placeholder_6(var_85, var_87, var_86);
                    rax_0 = var_88;
                    return rax_0;
                }
            }
            var_74 = (uint32_t)var_65 - (uint32_t)var_66;
            var_75 = (uint32_t *)(var_0 + (-36L));
            *var_75 = var_74;
            var_76 = (uint64_t *)(var_0 + (-24L));
            *var_76 = 0UL;
            while ((uint64_t)(((uint32_t)(uint64_t)*var_11 + (-48)) & (-2)) <= 9UL)
                {
                    *var_4 = (*var_4 + 1UL);
                    var_77 = **var_9;
                    *var_11 = var_77;
                    do {
                        *var_4 = (*var_4 + 1UL);
                        var_77 = **var_9;
                        *var_11 = var_77;
                    } while ((uint64_t)((uint32_t)(uint64_t)var_77 - *var_8) != 0UL);
                    *var_76 = (*var_76 + 1UL);
                }
            var_78 = (uint64_t *)(var_0 + (-32L));
            *var_78 = 0UL;
            while ((uint64_t)(((uint32_t)(uint64_t)*var_14 + (-48)) & (-2)) <= 9UL)
                {
                    *var_6 = (*var_6 + 1UL);
                    var_80 = **var_12;
                    *var_14 = var_80;
                    do {
                        *var_6 = (*var_6 + 1UL);
                        var_80 = **var_12;
                        *var_14 = var_80;
                    } while ((uint64_t)((uint32_t)(uint64_t)var_80 - *var_8) != 0UL);
                    var_81 = *var_78 + 1UL;
                    *var_78 = var_81;
                    var_79 = var_81;
                }
            var_82 = *var_76;
            if (var_82 == var_79) {
                var_83 = helper_cc_compute_c_wrapper(var_82 - var_79, var_79, var_2, 17U);
                return (var_83 == 0UL) ? 4294967295UL : 1UL;
            }
            if (var_79 == 0UL) {
                var_84 = (uint64_t)*var_75;
                rax_0 = var_84;
            }
        }
    }
}

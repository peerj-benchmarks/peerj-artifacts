typedef unsigned char __u_char;
typedef unsigned short __u_short;
typedef unsigned int __u_int;
typedef unsigned long __u_long;
typedef signed char __int8_t;
typedef unsigned char __uint8_t;
typedef short __int16_t;
typedef unsigned short __uint16_t;
typedef int __int32_t;
typedef unsigned int __uint32_t;
typedef long __int64_t;
typedef unsigned long __uint64_t;
typedef __int8_t __int_least8_t;
typedef __uint8_t __uint_least8_t;
typedef __int16_t __int_least16_t;
typedef __uint16_t __uint_least16_t;
typedef __int32_t __int_least32_t;
typedef __uint32_t __uint_least32_t;
typedef __int64_t __int_least64_t;
typedef __uint64_t __uint_least64_t;
typedef long __quad_t;
typedef unsigned long __u_quad_t;
typedef long __intmax_t;
typedef unsigned long __uintmax_t;
typedef unsigned long __dev_t;
typedef unsigned int __uid_t;
typedef unsigned int __gid_t;
typedef unsigned long __ino_t;
typedef unsigned long __ino64_t;
typedef unsigned int __mode_t;
typedef unsigned long __nlink_t;
typedef long __off_t;
typedef long __off64_t;
typedef int __pid_t;
typedef struct {
    int __val[2];
} __fsid_t;
typedef long __clock_t;
typedef unsigned long __rlim_t;
typedef unsigned long __rlim64_t;
typedef unsigned int __id_t;
typedef long __time_t;
typedef unsigned int __useconds_t;
typedef long __suseconds_t;
typedef int __daddr_t;
typedef int __key_t;
typedef int __clockid_t;
typedef void *__timer_t;
typedef long __blksize_t;
typedef long __blkcnt_t;
typedef long __blkcnt64_t;
typedef unsigned long __fsblkcnt_t;
typedef unsigned long __fsblkcnt64_t;
typedef unsigned long __fsfilcnt_t;
typedef unsigned long __fsfilcnt64_t;
typedef long __fsword_t;
typedef long __ssize_t;
typedef long __syscall_slong_t;
typedef unsigned long __syscall_ulong_t;
typedef __off64_t __loff_t;
typedef char *__caddr_t;
typedef long __intptr_t;
typedef unsigned int __socklen_t;
typedef int __sig_atomic_t;
typedef __int8_t int8_t;
typedef __int16_t int16_t;
typedef __int32_t int32_t;
typedef __int64_t int64_t;
typedef __uint8_t uint8_t;
typedef __uint16_t uint16_t;
typedef __uint32_t uint32_t;
typedef __uint64_t uint64_t;
typedef __int_least8_t int_least8_t;
typedef __int_least16_t int_least16_t;
typedef __int_least32_t int_least32_t;
typedef __int_least64_t int_least64_t;
typedef __uint_least8_t uint_least8_t;
typedef __uint_least16_t uint_least16_t;
typedef __uint_least32_t uint_least32_t;
typedef __uint_least64_t uint_least64_t;
typedef signed char int_fast8_t;
typedef long int_fast16_t;
typedef long int_fast32_t;
typedef long int_fast64_t;
typedef unsigned char uint_fast8_t;
typedef unsigned long uint_fast16_t;
typedef unsigned long uint_fast32_t;
typedef unsigned long uint_fast64_t;
typedef long intptr_t;
typedef unsigned long uintptr_t;
typedef __intmax_t intmax_t;
typedef __uintmax_t uintmax_t;
typedef _Bool bool;
extern void abort(void);
extern uint64_t revng_init_local_sp(uint64_t param_0);
extern uint64_t init_rbp(void);
extern uint64_t init_cc_src2(void);
extern uint64_t helper_cc_compute_c_wrapper(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint32_t param_3);
uint64_t bb_printf_fetchargs(uint64_t rdi, uint64_t rsi) {
    uint32_t **var_158;
    uint64_t var_23;
    bool var_24;
    uint64_t var_25;
    uint64_t var_26;
    uint32_t *var_27;
    uint32_t var_28;
    uint64_t var_29;
    uint64_t var_0;
    uint64_t var_1;
    uint64_t var_2;
    uint64_t var_3;
    uint64_t *var_4;
    uint64_t var_5;
    uint64_t *var_6;
    uint64_t *var_7;
    uint64_t var_8;
    uint64_t var_9;
    uint64_t *var_10;
    uint64_t storemerge;
    uint32_t var_11;
    uint32_t **var_12;
    uint64_t var_13;
    bool var_14;
    uint64_t var_15;
    uint64_t var_16;
    uint32_t *var_17;
    uint32_t var_18;
    uint64_t var_19;
    uint64_t rax_0;
    uint64_t *var_20;
    uint64_t var_21;
    uint32_t **var_22;
    uint64_t rax_9;
    uint64_t *var_112;
    uint64_t var_113;
    uint64_t var_97;
    uint64_t rax_8;
    uint64_t var_98;
    uint32_t *var_99;
    uint32_t var_100;
    uint64_t var_101;
    uint64_t var_199;
    bool var_200;
    uint64_t var_201;
    uint64_t rax_1;
    uint64_t *var_30;
    uint32_t **var_94;
    uint64_t var_95;
    bool var_96;
    uint64_t var_31;
    uint32_t **var_32;
    uint64_t var_33;
    bool var_34;
    uint64_t var_35;
    uint64_t var_36;
    uint32_t *var_37;
    uint32_t var_38;
    uint64_t var_39;
    uint64_t rax_2;
    uint64_t *var_40;
    uint64_t var_41;
    uint32_t **var_42;
    uint64_t var_43;
    bool var_44;
    uint64_t var_45;
    uint64_t var_46;
    uint32_t *var_47;
    uint32_t var_48;
    uint64_t var_49;
    uint64_t rax_3;
    uint64_t *var_50;
    uint64_t var_51;
    uint64_t *var_102;
    uint32_t **var_52;
    uint64_t var_53;
    bool var_54;
    uint64_t var_55;
    uint64_t var_56;
    uint32_t *var_57;
    uint32_t var_58;
    uint64_t var_59;
    uint64_t rax_4;
    uint64_t *var_60;
    uint64_t var_61;
    uint32_t **var_62;
    uint64_t var_63;
    bool var_64;
    uint64_t var_65;
    uint64_t var_66;
    uint32_t *var_67;
    uint32_t var_68;
    uint64_t var_69;
    uint64_t rax_5;
    uint64_t *var_70;
    uint64_t var_71;
    uint32_t **var_72;
    uint64_t var_73;
    bool var_74;
    uint64_t var_75;
    uint64_t var_76;
    uint32_t *var_77;
    uint32_t var_78;
    uint64_t var_79;
    uint64_t rax_6;
    uint64_t *var_80;
    uint64_t var_81;
    uint64_t *var_82;
    uint32_t **var_83;
    uint64_t var_84;
    bool var_85;
    uint64_t var_86;
    uint64_t var_87;
    uint32_t *var_88;
    uint32_t var_89;
    uint64_t var_90;
    uint64_t rax_7;
    uint64_t *var_91;
    uint64_t var_92;
    uint64_t *var_93;
    uint64_t var_103;
    uint32_t **var_104;
    uint64_t var_105;
    bool var_106;
    uint64_t var_107;
    uint64_t var_108;
    uint32_t *var_109;
    uint32_t var_110;
    uint64_t var_111;
    uint64_t *var_114;
    uint64_t var_115;
    uint32_t var_116;
    uint64_t var_117;
    uint64_t var_118;
    uint64_t var_119;
    bool var_120;
    uint64_t var_121;
    uint64_t var_122;
    uint32_t *var_123;
    uint32_t var_124;
    uint64_t var_125;
    uint64_t rax_10;
    uint64_t *var_126;
    uint64_t var_127;
    uint32_t **var_128;
    uint64_t var_129;
    bool var_130;
    uint64_t var_131;
    uint64_t var_132;
    uint32_t *var_133;
    uint32_t var_134;
    uint64_t var_135;
    uint64_t rax_11;
    uint64_t *var_136;
    uint64_t var_137;
    uint32_t **var_138;
    uint64_t var_139;
    bool var_140;
    uint64_t var_141;
    uint64_t var_142;
    uint32_t *var_143;
    uint32_t var_144;
    uint64_t var_145;
    uint64_t rax_12;
    uint64_t *var_146;
    uint64_t var_147;
    uint32_t **var_148;
    uint64_t var_149;
    bool var_150;
    uint64_t var_151;
    uint64_t var_152;
    uint32_t *var_153;
    uint32_t var_154;
    uint64_t var_155;
    uint64_t rax_13;
    uint64_t *var_156;
    uint64_t var_157;
    uint64_t var_159;
    bool var_160;
    uint64_t var_161;
    uint64_t var_162;
    uint32_t *var_163;
    uint32_t var_164;
    uint64_t var_165;
    uint64_t rax_14;
    uint64_t *var_166;
    uint64_t var_167;
    uint32_t **var_168;
    uint64_t var_169;
    bool var_170;
    uint64_t var_171;
    uint64_t var_172;
    uint32_t *var_173;
    uint32_t var_174;
    uint64_t var_175;
    uint64_t rax_15;
    uint64_t *var_176;
    uint64_t var_177;
    uint32_t **var_178;
    uint64_t var_179;
    bool var_180;
    uint64_t var_181;
    uint64_t var_182;
    uint32_t *var_183;
    uint32_t var_184;
    uint64_t var_185;
    uint64_t rax_16;
    uint64_t *var_186;
    uint64_t var_187;
    uint32_t **var_188;
    uint64_t var_189;
    bool var_190;
    uint64_t var_191;
    uint64_t var_192;
    uint32_t *var_193;
    uint32_t var_194;
    uint64_t var_195;
    uint64_t rax_17;
    uint64_t *var_196;
    uint64_t var_197;
    uint32_t **var_198;
    uint64_t var_202;
    uint32_t *var_203;
    uint32_t var_204;
    uint64_t var_205;
    uint64_t rax_18;
    uint64_t *var_206;
    uint64_t var_207;
    uint32_t **var_208;
    uint64_t var_209;
    bool var_210;
    uint64_t var_211;
    uint64_t var_212;
    uint32_t *var_213;
    uint32_t var_214;
    uint64_t var_215;
    uint64_t rax_19;
    uint64_t *var_216;
    uint64_t var_217;
    uint32_t **var_218;
    uint64_t var_219;
    bool var_220;
    uint64_t var_221;
    uint64_t var_222;
    uint32_t *var_223;
    uint32_t var_224;
    uint64_t var_225;
    uint64_t rax_20;
    uint64_t *var_226;
    uint64_t var_227;
    bool switch_state_var;
    var_0 = revng_init_local_sp(0UL);
    var_1 = init_rbp();
    var_2 = init_cc_src2();
    *(uint64_t *)(var_0 + (-8L)) = var_1;
    var_3 = var_0 + (-32L);
    var_4 = (uint64_t *)var_3;
    *var_4 = rdi;
    var_5 = var_0 + (-40L);
    var_6 = (uint64_t *)var_5;
    *var_6 = rsi;
    var_7 = (uint64_t *)(var_0 + (-16L));
    *var_7 = 0UL;
    var_8 = *(uint64_t *)(*var_6 + 8UL);
    var_9 = var_0 + (-24L);
    var_10 = (uint64_t *)var_9;
    *var_10 = var_8;
    storemerge = 0UL;
    while (**(uint64_t **)var_5 <= *var_7)
        {
            switch_state_var = 0;
            switch (*(uint64_t *)(((uint64_t)var_11 << 3UL) + 4281688UL)) {
              case 4267025UL:
                {
                    switch_state_var = 1;
                    break;
                }
                break;
              case 4266945UL:
              case 4266865UL:
              case 4266782UL:
              case 4266699UL:
              case 4266616UL:
              case 4266533UL:
              case 4266420UL:
              case 4266307UL:
              case 4266226UL:
              case 4266145UL:
              case 4266095UL:
              case 4266006UL:
              case 4265923UL:
              case 4265840UL:
              case 4265757UL:
              case 4265674UL:
              case 4265593UL:
              case 4265512UL:
              case 4265428UL:
              case 4265344UL:
              case 4265261UL:
              case 4265178UL:
                {
                    switch (*(uint64_t *)(((uint64_t)var_11 << 3UL) + 4281688UL)) {
                      case 4266095UL:
                        {
                            var_114 = (uint64_t *)(*var_4 + 8UL);
                            var_115 = (*var_114 + 15UL) & (-16L);
                            *var_114 = (var_115 + 16UL);
                            var_116 = *(uint32_t *)(var_115 | 8UL);
                            var_117 = *(uint64_t *)var_115;
                            var_118 = *var_10;
                            *(uint64_t *)(var_118 + 16UL) = var_117;
                            *(uint32_t *)(var_118 + 24UL) = var_116;
                        }
                        break;
                      case 4266006UL:
                        {
                            var_119 = helper_cc_compute_c_wrapper((uint64_t)*(uint32_t *)(*var_4 + 4UL) + (-176L), 176UL, var_2, 16U);
                            var_120 = (var_119 == 0UL);
                            var_121 = *var_4;
                            if (var_120) {
                                var_126 = (uint64_t *)(var_121 + 8UL);
                                var_127 = *var_126;
                                *var_126 = (var_127 + 8UL);
                                rax_10 = var_127;
                            } else {
                                var_122 = *(uint64_t *)(var_121 + 16UL);
                                var_123 = (uint32_t *)(var_121 + 4UL);
                                var_124 = *var_123;
                                var_125 = var_122 + (uint64_t)var_124;
                                *var_123 = (var_124 + 16U);
                                rax_10 = var_125;
                            }
                            *(uint64_t *)(*var_10 + 16UL) = *(uint64_t *)rax_10;
                        }
                        break;
                      case 4266616UL:
                        {
                            var_52 = (uint32_t **)var_3;
                            var_53 = helper_cc_compute_c_wrapper((uint64_t)**var_52 + (-48L), 48UL, var_2, 16U);
                            var_54 = (var_53 == 0UL);
                            var_55 = *var_4;
                            if (var_54) {
                                var_60 = (uint64_t *)(var_55 + 8UL);
                                var_61 = *var_60;
                                *var_60 = (var_61 + 8UL);
                                rax_4 = var_61;
                            } else {
                                var_56 = *(uint64_t *)(var_55 + 16UL);
                                var_57 = *var_52;
                                var_58 = *var_57;
                                var_59 = var_56 + (uint64_t)var_58;
                                *var_57 = (var_58 + 8U);
                                rax_4 = var_59;
                            }
                            *(uint64_t *)(*var_10 + 16UL) = *(uint64_t *)rax_4;
                        }
                        break;
                      case 4266699UL:
                        {
                            var_42 = (uint32_t **)var_3;
                            var_43 = helper_cc_compute_c_wrapper((uint64_t)**var_42 + (-48L), 48UL, var_2, 16U);
                            var_44 = (var_43 == 0UL);
                            var_45 = *var_4;
                            if (var_44) {
                                var_50 = (uint64_t *)(var_45 + 8UL);
                                var_51 = *var_50;
                                *var_50 = (var_51 + 8UL);
                                rax_3 = var_51;
                            } else {
                                var_46 = *(uint64_t *)(var_45 + 16UL);
                                var_47 = *var_42;
                                var_48 = *var_47;
                                var_49 = var_46 + (uint64_t)var_48;
                                *var_47 = (var_48 + 8U);
                                rax_3 = var_49;
                            }
                            *(uint64_t *)(*var_10 + 16UL) = *(uint64_t *)rax_3;
                        }
                        break;
                      case 4266533UL:
                        {
                            var_62 = (uint32_t **)var_3;
                            var_63 = helper_cc_compute_c_wrapper((uint64_t)**var_62 + (-48L), 48UL, var_2, 16U);
                            var_64 = (var_63 == 0UL);
                            var_65 = *var_4;
                            if (var_64) {
                                var_70 = (uint64_t *)(var_65 + 8UL);
                                var_71 = *var_70;
                                *var_70 = (var_71 + 8UL);
                                rax_5 = var_71;
                            } else {
                                var_66 = *(uint64_t *)(var_65 + 16UL);
                                var_67 = *var_62;
                                var_68 = *var_67;
                                var_69 = var_66 + (uint64_t)var_68;
                                *var_67 = (var_68 + 8U);
                                rax_5 = var_69;
                            }
                            *(uint64_t *)(*var_10 + 16UL) = *(uint64_t *)rax_5;
                        }
                        break;
                      case 4266307UL:
                        {
                            var_83 = (uint32_t **)var_3;
                            var_84 = helper_cc_compute_c_wrapper((uint64_t)**var_83 + (-48L), 48UL, var_2, 16U);
                            var_85 = (var_84 == 0UL);
                            var_86 = *var_4;
                            if (var_85) {
                                var_91 = (uint64_t *)(var_86 + 8UL);
                                var_92 = *var_91;
                                *var_91 = (var_92 + 8UL);
                                rax_7 = var_92;
                            } else {
                                var_87 = *(uint64_t *)(var_86 + 16UL);
                                var_88 = *var_83;
                                var_89 = *var_88;
                                var_90 = var_87 + (uint64_t)var_89;
                                *var_88 = (var_89 + 8U);
                                rax_7 = var_90;
                            }
                            *(uint64_t *)(*var_10 + 16UL) = *(uint64_t *)rax_7;
                            var_93 = (uint64_t *)(*var_10 + 16UL);
                            if (*var_93 == 0UL) {
                                *var_93 = 4281680UL;
                            }
                        }
                        break;
                      case 4265261UL:
                        {
                            var_208 = (uint32_t **)var_3;
                            var_209 = helper_cc_compute_c_wrapper((uint64_t)**var_208 + (-48L), 48UL, var_2, 16U);
                            var_210 = (var_209 == 0UL);
                            var_211 = *var_4;
                            if (var_210) {
                                var_216 = (uint64_t *)(var_211 + 8UL);
                                var_217 = *var_216;
                                *var_216 = (var_217 + 8UL);
                                rax_19 = var_217;
                            } else {
                                var_212 = *(uint64_t *)(var_211 + 16UL);
                                var_213 = *var_208;
                                var_214 = *var_213;
                                var_215 = var_212 + (uint64_t)var_214;
                                *var_213 = (var_214 + 8U);
                                rax_19 = var_215;
                            }
                            *(unsigned char *)(*var_10 + 16UL) = (unsigned char)*(uint32_t *)rax_19;
                        }
                        break;
                      case 4265344UL:
                        {
                            var_198 = (uint32_t **)var_3;
                            var_199 = helper_cc_compute_c_wrapper((uint64_t)**var_198 + (-48L), 48UL, var_2, 16U);
                            var_200 = (var_199 == 0UL);
                            var_201 = *var_4;
                            if (var_200) {
                                var_206 = (uint64_t *)(var_201 + 8UL);
                                var_207 = *var_206;
                                *var_206 = (var_207 + 8UL);
                                rax_18 = var_207;
                            } else {
                                var_202 = *(uint64_t *)(var_201 + 16UL);
                                var_203 = *var_198;
                                var_204 = *var_203;
                                var_205 = var_202 + (uint64_t)var_204;
                                *var_203 = (var_204 + 8U);
                                rax_18 = var_205;
                            }
                            *(uint16_t *)(*var_10 + 16UL) = (uint16_t)*(uint32_t *)rax_18;
                        }
                        break;
                      case 4266420UL:
                        {
                            var_72 = (uint32_t **)var_3;
                            var_73 = helper_cc_compute_c_wrapper((uint64_t)**var_72 + (-48L), 48UL, var_2, 16U);
                            var_74 = (var_73 == 0UL);
                            var_75 = *var_4;
                            if (var_74) {
                                var_80 = (uint64_t *)(var_75 + 8UL);
                                var_81 = *var_80;
                                *var_80 = (var_81 + 8UL);
                                rax_6 = var_81;
                            } else {
                                var_76 = *(uint64_t *)(var_75 + 16UL);
                                var_77 = *var_72;
                                var_78 = *var_77;
                                var_79 = var_76 + (uint64_t)var_78;
                                *var_77 = (var_78 + 8U);
                                rax_6 = var_79;
                            }
                            *(uint64_t *)(*var_10 + 16UL) = *(uint64_t *)rax_6;
                            var_82 = (uint64_t *)(*var_10 + 16UL);
                            if (*var_82 == 0UL) {
                                *var_82 = 4281872UL;
                            }
                        }
                        break;
                      case 4265178UL:
                        {
                            var_218 = (uint32_t **)var_3;
                            var_219 = helper_cc_compute_c_wrapper((uint64_t)**var_218 + (-48L), 48UL, var_2, 16U);
                            var_220 = (var_219 == 0UL);
                            var_221 = *var_4;
                            if (var_220) {
                                var_226 = (uint64_t *)(var_221 + 8UL);
                                var_227 = *var_226;
                                *var_226 = (var_227 + 8UL);
                                rax_20 = var_227;
                            } else {
                                var_222 = *(uint64_t *)(var_221 + 16UL);
                                var_223 = *var_218;
                                var_224 = *var_223;
                                var_225 = var_222 + (uint64_t)var_224;
                                *var_223 = (var_224 + 8U);
                                rax_20 = var_225;
                            }
                            *(unsigned char *)(*var_10 + 16UL) = (unsigned char)*(uint32_t *)rax_20;
                        }
                        break;
                      case 4265512UL:
                        {
                            var_178 = (uint32_t **)var_3;
                            var_179 = helper_cc_compute_c_wrapper((uint64_t)**var_178 + (-48L), 48UL, var_2, 16U);
                            var_180 = (var_179 == 0UL);
                            var_181 = *var_4;
                            if (var_180) {
                                var_186 = (uint64_t *)(var_181 + 8UL);
                                var_187 = *var_186;
                                *var_186 = (var_187 + 8UL);
                                rax_16 = var_187;
                            } else {
                                var_182 = *(uint64_t *)(var_181 + 16UL);
                                var_183 = *var_178;
                                var_184 = *var_183;
                                var_185 = var_182 + (uint64_t)var_184;
                                *var_183 = (var_184 + 8U);
                                rax_16 = var_185;
                            }
                            *(uint32_t *)(*var_10 + 16UL) = *(uint32_t *)rax_16;
                        }
                        break;
                      case 4266782UL:
                        {
                            var_32 = (uint32_t **)var_3;
                            var_33 = helper_cc_compute_c_wrapper((uint64_t)**var_32 + (-48L), 48UL, var_2, 16U);
                            var_34 = (var_33 == 0UL);
                            var_35 = *var_4;
                            if (var_34) {
                                var_40 = (uint64_t *)(var_35 + 8UL);
                                var_41 = *var_40;
                                *var_40 = (var_41 + 8UL);
                                rax_2 = var_41;
                            } else {
                                var_36 = *(uint64_t *)(var_35 + 16UL);
                                var_37 = *var_32;
                                var_38 = *var_37;
                                var_39 = var_36 + (uint64_t)var_38;
                                *var_37 = (var_38 + 8U);
                                rax_2 = var_39;
                            }
                            *(uint64_t *)(*var_10 + 16UL) = *(uint64_t *)rax_2;
                        }
                        break;
                      case 4266145UL:
                        {
                            var_104 = (uint32_t **)var_3;
                            var_105 = helper_cc_compute_c_wrapper((uint64_t)**var_104 + (-48L), 48UL, var_2, 16U);
                            var_106 = (var_105 == 0UL);
                            var_107 = *var_4;
                            if (var_106) {
                                var_112 = (uint64_t *)(var_107 + 8UL);
                                var_113 = *var_112;
                                *var_112 = (var_113 + 8UL);
                                rax_9 = var_113;
                            } else {
                                var_108 = *(uint64_t *)(var_107 + 16UL);
                                var_109 = *var_104;
                                var_110 = *var_109;
                                var_111 = var_108 + (uint64_t)var_110;
                                *var_109 = (var_110 + 8U);
                                rax_9 = var_111;
                            }
                            *(uint32_t *)(*var_10 + 16UL) = *(uint32_t *)rax_9;
                        }
                        break;
                      case 4265593UL:
                        {
                            var_168 = (uint32_t **)var_3;
                            var_169 = helper_cc_compute_c_wrapper((uint64_t)**var_168 + (-48L), 48UL, var_2, 16U);
                            var_170 = (var_169 == 0UL);
                            var_171 = *var_4;
                            if (var_170) {
                                var_176 = (uint64_t *)(var_171 + 8UL);
                                var_177 = *var_176;
                                *var_176 = (var_177 + 8UL);
                                rax_15 = var_177;
                            } else {
                                var_172 = *(uint64_t *)(var_171 + 16UL);
                                var_173 = *var_168;
                                var_174 = *var_173;
                                var_175 = var_172 + (uint64_t)var_174;
                                *var_173 = (var_174 + 8U);
                                rax_15 = var_175;
                            }
                            *(uint32_t *)(*var_10 + 16UL) = *(uint32_t *)rax_15;
                        }
                        break;
                      case 4266945UL:
                        {
                            var_12 = (uint32_t **)var_3;
                            var_13 = helper_cc_compute_c_wrapper((uint64_t)**var_12 + (-48L), 48UL, var_2, 16U);
                            var_14 = (var_13 == 0UL);
                            var_15 = *var_4;
                            if (var_14) {
                                var_20 = (uint64_t *)(var_15 + 8UL);
                                var_21 = *var_20;
                                *var_20 = (var_21 + 8UL);
                                rax_0 = var_21;
                            } else {
                                var_16 = *(uint64_t *)(var_15 + 16UL);
                                var_17 = *var_12;
                                var_18 = *var_17;
                                var_19 = var_16 + (uint64_t)var_18;
                                *var_17 = (var_18 + 8U);
                                rax_0 = var_19;
                            }
                            *(uint64_t *)(*var_10 + 16UL) = *(uint64_t *)rax_0;
                        }
                        break;
                      case 4265428UL:
                        {
                            var_188 = (uint32_t **)var_3;
                            var_189 = helper_cc_compute_c_wrapper((uint64_t)**var_188 + (-48L), 48UL, var_2, 16U);
                            var_190 = (var_189 == 0UL);
                            var_191 = *var_4;
                            if (var_190) {
                                var_196 = (uint64_t *)(var_191 + 8UL);
                                var_197 = *var_196;
                                *var_196 = (var_197 + 8UL);
                                rax_17 = var_197;
                            } else {
                                var_192 = *(uint64_t *)(var_191 + 16UL);
                                var_193 = *var_188;
                                var_194 = *var_193;
                                var_195 = var_192 + (uint64_t)var_194;
                                *var_193 = (var_194 + 8U);
                                rax_17 = var_195;
                            }
                            *(uint16_t *)(*var_10 + 16UL) = (uint16_t)*(uint32_t *)rax_17;
                        }
                        break;
                      case 4265923UL:
                        {
                            var_128 = (uint32_t **)var_3;
                            var_129 = helper_cc_compute_c_wrapper((uint64_t)**var_128 + (-48L), 48UL, var_2, 16U);
                            var_130 = (var_129 == 0UL);
                            var_131 = *var_4;
                            if (var_130) {
                                var_136 = (uint64_t *)(var_131 + 8UL);
                                var_137 = *var_136;
                                *var_136 = (var_137 + 8UL);
                                rax_11 = var_137;
                            } else {
                                var_132 = *(uint64_t *)(var_131 + 16UL);
                                var_133 = *var_128;
                                var_134 = *var_133;
                                var_135 = var_132 + (uint64_t)var_134;
                                *var_133 = (var_134 + 8U);
                                rax_11 = var_135;
                            }
                            *(uint64_t *)(*var_10 + 16UL) = *(uint64_t *)rax_11;
                        }
                        break;
                      case 4265757UL:
                        {
                            var_148 = (uint32_t **)var_3;
                            var_149 = helper_cc_compute_c_wrapper((uint64_t)**var_148 + (-48L), 48UL, var_2, 16U);
                            var_150 = (var_149 == 0UL);
                            var_151 = *var_4;
                            if (var_150) {
                                var_156 = (uint64_t *)(var_151 + 8UL);
                                var_157 = *var_156;
                                *var_156 = (var_157 + 8UL);
                                rax_13 = var_157;
                            } else {
                                var_152 = *(uint64_t *)(var_151 + 16UL);
                                var_153 = *var_148;
                                var_154 = *var_153;
                                var_155 = var_152 + (uint64_t)var_154;
                                *var_153 = (var_154 + 8U);
                                rax_13 = var_155;
                            }
                            *(uint64_t *)(*var_10 + 16UL) = *(uint64_t *)rax_13;
                        }
                        break;
                      case 4265674UL:
                        {
                            var_158 = (uint32_t **)var_3;
                            var_159 = helper_cc_compute_c_wrapper((uint64_t)**var_158 + (-48L), 48UL, var_2, 16U);
                            var_160 = (var_159 == 0UL);
                            var_161 = *var_4;
                            if (var_160) {
                                var_166 = (uint64_t *)(var_161 + 8UL);
                                var_167 = *var_166;
                                *var_166 = (var_167 + 8UL);
                                rax_14 = var_167;
                            } else {
                                var_162 = *(uint64_t *)(var_161 + 16UL);
                                var_163 = *var_158;
                                var_164 = *var_163;
                                var_165 = var_162 + (uint64_t)var_164;
                                *var_163 = (var_164 + 8U);
                                rax_14 = var_165;
                            }
                            *(uint64_t *)(*var_10 + 16UL) = *(uint64_t *)rax_14;
                        }
                        break;
                      case 4266226UL:
                        {
                            var_94 = (uint32_t **)var_3;
                            var_95 = helper_cc_compute_c_wrapper((uint64_t)**var_94 + (-48L), 48UL, var_2, 16U);
                            var_96 = (var_95 == 0UL);
                            var_97 = *var_4;
                            if (var_96) {
                                var_102 = (uint64_t *)(var_97 + 8UL);
                                var_103 = *var_102;
                                *var_102 = (var_103 + 8UL);
                                rax_8 = var_103;
                            } else {
                                var_98 = *(uint64_t *)(var_97 + 16UL);
                                var_99 = *var_94;
                                var_100 = *var_99;
                                var_101 = var_98 + (uint64_t)var_100;
                                *var_99 = (var_100 + 8U);
                                rax_8 = var_101;
                            }
                            *(uint32_t *)(*var_10 + 16UL) = *(uint32_t *)rax_8;
                        }
                        break;
                      case 4266865UL:
                        {
                            var_22 = (uint32_t **)var_3;
                            var_23 = helper_cc_compute_c_wrapper((uint64_t)**var_22 + (-48L), 48UL, var_2, 16U);
                            var_24 = (var_23 == 0UL);
                            var_25 = *var_4;
                            if (var_24) {
                                var_30 = (uint64_t *)(var_25 + 8UL);
                                var_31 = *var_30;
                                *var_30 = (var_31 + 8UL);
                                rax_1 = var_31;
                            } else {
                                var_26 = *(uint64_t *)(var_25 + 16UL);
                                var_27 = *var_22;
                                var_28 = *var_27;
                                var_29 = var_26 + (uint64_t)var_28;
                                *var_27 = (var_28 + 8U);
                                rax_1 = var_29;
                            }
                            *(uint64_t *)(*var_10 + 16UL) = *(uint64_t *)rax_1;
                        }
                        break;
                      case 4265840UL:
                        {
                            var_138 = (uint32_t **)var_3;
                            var_139 = helper_cc_compute_c_wrapper((uint64_t)**var_138 + (-48L), 48UL, var_2, 16U);
                            var_140 = (var_139 == 0UL);
                            var_141 = *var_4;
                            if (var_140) {
                                var_146 = (uint64_t *)(var_141 + 8UL);
                                var_147 = *var_146;
                                *var_146 = (var_147 + 8UL);
                                rax_12 = var_147;
                            } else {
                                var_142 = *(uint64_t *)(var_141 + 16UL);
                                var_143 = *var_138;
                                var_144 = *var_143;
                                var_145 = var_142 + (uint64_t)var_144;
                                *var_143 = (var_144 + 8U);
                                rax_12 = var_145;
                            }
                            *(uint64_t *)(*var_10 + 16UL) = *(uint64_t *)rax_12;
                        }
                        break;
                    }
                    *var_7 = (*var_7 + 1UL);
                    *var_10 = (*var_10 + 32UL);
                    continue;
                }
                break;
              default:
                {
                    abort();
                }
                break;
            }
            if (switch_state_var)
                break;
        }
    return storemerge;
}

typedef unsigned char __u_char;
typedef unsigned short __u_short;
typedef unsigned int __u_int;
typedef unsigned long __u_long;
typedef signed char __int8_t;
typedef unsigned char __uint8_t;
typedef short __int16_t;
typedef unsigned short __uint16_t;
typedef int __int32_t;
typedef unsigned int __uint32_t;
typedef long __int64_t;
typedef unsigned long __uint64_t;
typedef __int8_t __int_least8_t;
typedef __uint8_t __uint_least8_t;
typedef __int16_t __int_least16_t;
typedef __uint16_t __uint_least16_t;
typedef __int32_t __int_least32_t;
typedef __uint32_t __uint_least32_t;
typedef __int64_t __int_least64_t;
typedef __uint64_t __uint_least64_t;
typedef long __quad_t;
typedef unsigned long __u_quad_t;
typedef long __intmax_t;
typedef unsigned long __uintmax_t;
typedef unsigned long __dev_t;
typedef unsigned int __uid_t;
typedef unsigned int __gid_t;
typedef unsigned long __ino_t;
typedef unsigned long __ino64_t;
typedef unsigned int __mode_t;
typedef unsigned long __nlink_t;
typedef long __off_t;
typedef long __off64_t;
typedef int __pid_t;
typedef struct {
    int __val[2];
} __fsid_t;
typedef long __clock_t;
typedef unsigned long __rlim_t;
typedef unsigned long __rlim64_t;
typedef unsigned int __id_t;
typedef long __time_t;
typedef unsigned int __useconds_t;
typedef long __suseconds_t;
typedef int __daddr_t;
typedef int __key_t;
typedef int __clockid_t;
typedef void *__timer_t;
typedef long __blksize_t;
typedef long __blkcnt_t;
typedef long __blkcnt64_t;
typedef unsigned long __fsblkcnt_t;
typedef unsigned long __fsblkcnt64_t;
typedef unsigned long __fsfilcnt_t;
typedef unsigned long __fsfilcnt64_t;
typedef long __fsword_t;
typedef long __ssize_t;
typedef long __syscall_slong_t;
typedef unsigned long __syscall_ulong_t;
typedef __off64_t __loff_t;
typedef char *__caddr_t;
typedef long __intptr_t;
typedef unsigned int __socklen_t;
typedef int __sig_atomic_t;
typedef __int8_t int8_t;
typedef __int16_t int16_t;
typedef __int32_t int32_t;
typedef __int64_t int64_t;
typedef __uint8_t uint8_t;
typedef __uint16_t uint16_t;
typedef __uint32_t uint32_t;
typedef __uint64_t uint64_t;
typedef __int_least8_t int_least8_t;
typedef __int_least16_t int_least16_t;
typedef __int_least32_t int_least32_t;
typedef __int_least64_t int_least64_t;
typedef __uint_least8_t uint_least8_t;
typedef __uint_least16_t uint_least16_t;
typedef __uint_least32_t uint_least32_t;
typedef __uint_least64_t uint_least64_t;
typedef signed char int_fast8_t;
typedef long int_fast16_t;
typedef long int_fast32_t;
typedef long int_fast64_t;
typedef unsigned char uint_fast8_t;
typedef unsigned long uint_fast16_t;
typedef unsigned long uint_fast32_t;
typedef unsigned long uint_fast64_t;
typedef long intptr_t;
typedef unsigned long uintptr_t;
typedef __intmax_t intmax_t;
typedef __uintmax_t uintmax_t;
typedef _Bool bool;
struct indirect_placeholder_50_ret_type;
struct indirect_placeholder_49_ret_type;
struct indirect_placeholder_51_ret_type;
struct indirect_placeholder_50_ret_type {
    uint64_t field_0;
    uint64_t field_1;
    uint64_t field_2;
};
struct indirect_placeholder_49_ret_type {
    uint64_t field_0;
    uint64_t field_1;
    uint64_t field_2;
};
struct indirect_placeholder_51_ret_type {
    uint64_t field_0;
    uint64_t field_1;
    uint64_t field_2;
};
extern uint64_t revng_init_local_sp(uint64_t param_0);
extern uint64_t init_rbp(void);
extern void indirect_placeholder(void);
extern uint64_t indirect_placeholder_18(void);
extern struct indirect_placeholder_50_ret_type indirect_placeholder_50(uint64_t param_0);
extern struct indirect_placeholder_49_ret_type indirect_placeholder_49(uint64_t param_0);
extern struct indirect_placeholder_51_ret_type indirect_placeholder_51(uint64_t param_0);
uint64_t bb_posixtest(uint64_t rdi) {
    uint64_t var_0;
    uint64_t var_1;
    uint64_t var_2;
    uint32_t *var_3;
    uint32_t var_4;
    uint64_t var_15;
    unsigned char *var_16;
    uint64_t var_10;
    uint64_t var_11;
    uint64_t local_sp_0;
    uint64_t var_17;
    bool var_18;
    unsigned char *var_19;
    uint64_t var_8;
    uint64_t var_9;
    uint64_t local_sp_1;
    unsigned char *_pre_phi34;
    uint64_t var_5;
    unsigned char *var_6;
    uint64_t var_12;
    uint64_t var_13;
    unsigned char *var_14;
    uint64_t var_7;
    uint64_t var_20;
    unsigned char *var_21;
    uint64_t var_22;
    unsigned char *var_23;
    var_0 = revng_init_local_sp(0UL);
    var_1 = init_rbp();
    *(uint64_t *)(var_0 + (-8L)) = var_1;
    var_2 = var_0 + (-40L);
    var_3 = (uint32_t *)(var_0 + (-28L));
    var_4 = (uint32_t)rdi;
    *var_3 = var_4;
    local_sp_0 = var_2;
    if ((uint64_t)(var_4 + (-2)) == 0UL) {
        *(uint64_t *)(var_0 + (-48L)) = 4209069UL;
        var_22 = indirect_placeholder_18();
        var_23 = (unsigned char *)(var_0 + (-9L));
        *var_23 = (unsigned char)var_22;
        _pre_phi34 = var_23;
    } else {
        if ((int)var_4 > (int)2U) {
            if ((uint64_t)(var_4 + (-3)) != 0UL) {
                *(uint64_t *)(var_0 + (-48L)) = 4209082UL;
                var_20 = indirect_placeholder_18();
                var_21 = (unsigned char *)(var_0 + (-9L));
                *var_21 = (unsigned char)var_20;
                _pre_phi34 = var_21;
                return (uint64_t)*_pre_phi34;
            }
            if ((uint64_t)(var_4 + (-4)) != 0UL) {
                var_7 = *(uint64_t *)(*(uint64_t *)6386968UL + ((uint64_t)*(uint32_t *)6386960UL << 3UL));
                *(uint64_t *)(var_0 + (-48L)) = 4209129UL;
                indirect_placeholder();
                if ((uint64_t)(uint32_t)var_7 != 0UL) {
                    *(uint64_t *)(var_0 + (-56L)) = 4209143UL;
                    indirect_placeholder_51(1UL);
                    *(uint64_t *)(var_0 + (-64L)) = 4209148UL;
                    var_17 = indirect_placeholder_18();
                    var_18 = ((uint64_t)(unsigned char)var_17 == 0UL);
                    var_19 = (unsigned char *)(var_0 + (-9L));
                    *var_19 = (var_18 & '\x01');
                    _pre_phi34 = var_19;
                    return (uint64_t)*_pre_phi34;
                }
                var_8 = *(uint64_t *)(*(uint64_t *)6386968UL + ((uint64_t)*(uint32_t *)6386960UL << 3UL));
                var_9 = var_0 + (-56L);
                *(uint64_t *)var_9 = 4209213UL;
                indirect_placeholder();
                local_sp_0 = var_9;
                var_10 = *(uint64_t *)(*(uint64_t *)6386968UL + (((uint64_t)*(uint32_t *)6386960UL << 3UL) + 24UL));
                var_11 = var_0 + (-64L);
                *(uint64_t *)var_11 = 4209260UL;
                indirect_placeholder();
                local_sp_0 = var_11;
                if ((uint64_t)(uint32_t)var_8 != 0UL & (uint64_t)(uint32_t)var_10 != 0UL) {
                    *(uint64_t *)(var_0 + (-72L)) = 4209274UL;
                    indirect_placeholder_50(0UL);
                    *(uint64_t *)(var_0 + (-80L)) = 4209279UL;
                    var_15 = indirect_placeholder_18();
                    var_16 = (unsigned char *)(var_0 + (-9L));
                    *var_16 = (unsigned char)var_15;
                    *(uint64_t *)(var_0 + (-88L)) = 4209292UL;
                    indirect_placeholder_49(0UL);
                    _pre_phi34 = var_16;
                    return (uint64_t)*_pre_phi34;
                }
            }
        }
        if ((uint64_t)(var_4 + (-1)) != 0UL) {
            *(uint64_t *)(var_0 + (-48L)) = 4209056UL;
            var_5 = indirect_placeholder_18();
            var_6 = (unsigned char *)(var_0 + (-9L));
            *var_6 = (unsigned char)var_5;
            _pre_phi34 = var_6;
            return (uint64_t)*_pre_phi34;
        }
        local_sp_1 = local_sp_0;
        if ((int)*var_3 <= (int)0U) {
            var_12 = local_sp_0 + (-8L);
            *(uint64_t *)var_12 = 4209305UL;
            indirect_placeholder();
            local_sp_1 = var_12;
        }
        *(uint64_t *)(local_sp_1 + (-8L)) = 4209310UL;
        var_13 = indirect_placeholder_18();
        var_14 = (unsigned char *)(var_0 + (-9L));
        *var_14 = (unsigned char)var_13;
        _pre_phi34 = var_14;
    }
}

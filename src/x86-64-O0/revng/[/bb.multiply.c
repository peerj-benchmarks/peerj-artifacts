typedef unsigned char __u_char;
typedef unsigned short __u_short;
typedef unsigned int __u_int;
typedef unsigned long __u_long;
typedef signed char __int8_t;
typedef unsigned char __uint8_t;
typedef short __int16_t;
typedef unsigned short __uint16_t;
typedef int __int32_t;
typedef unsigned int __uint32_t;
typedef long __int64_t;
typedef unsigned long __uint64_t;
typedef __int8_t __int_least8_t;
typedef __uint8_t __uint_least8_t;
typedef __int16_t __int_least16_t;
typedef __uint16_t __uint_least16_t;
typedef __int32_t __int_least32_t;
typedef __uint32_t __uint_least32_t;
typedef __int64_t __int_least64_t;
typedef __uint64_t __uint_least64_t;
typedef long __quad_t;
typedef unsigned long __u_quad_t;
typedef long __intmax_t;
typedef unsigned long __uintmax_t;
typedef unsigned long __dev_t;
typedef unsigned int __uid_t;
typedef unsigned int __gid_t;
typedef unsigned long __ino_t;
typedef unsigned long __ino64_t;
typedef unsigned int __mode_t;
typedef unsigned long __nlink_t;
typedef long __off_t;
typedef long __off64_t;
typedef int __pid_t;
typedef struct {
    int __val[2];
} __fsid_t;
typedef long __clock_t;
typedef unsigned long __rlim_t;
typedef unsigned long __rlim64_t;
typedef unsigned int __id_t;
typedef long __time_t;
typedef unsigned int __useconds_t;
typedef long __suseconds_t;
typedef int __daddr_t;
typedef int __key_t;
typedef int __clockid_t;
typedef void *__timer_t;
typedef long __blksize_t;
typedef long __blkcnt_t;
typedef long __blkcnt64_t;
typedef unsigned long __fsblkcnt_t;
typedef unsigned long __fsblkcnt64_t;
typedef unsigned long __fsfilcnt_t;
typedef unsigned long __fsfilcnt64_t;
typedef long __fsword_t;
typedef long __ssize_t;
typedef long __syscall_slong_t;
typedef unsigned long __syscall_ulong_t;
typedef __off64_t __loff_t;
typedef char *__caddr_t;
typedef long __intptr_t;
typedef unsigned int __socklen_t;
typedef int __sig_atomic_t;
typedef __int8_t int8_t;
typedef __int16_t int16_t;
typedef __int32_t int32_t;
typedef __int64_t int64_t;
typedef __uint8_t uint8_t;
typedef __uint16_t uint16_t;
typedef __uint32_t uint32_t;
typedef __uint64_t uint64_t;
typedef __int_least8_t int_least8_t;
typedef __int_least16_t int_least16_t;
typedef __int_least32_t int_least32_t;
typedef __int_least64_t int_least64_t;
typedef __uint_least8_t uint_least8_t;
typedef __uint_least16_t uint_least16_t;
typedef __uint_least32_t uint_least32_t;
typedef __uint_least64_t uint_least64_t;
typedef signed char int_fast8_t;
typedef long int_fast16_t;
typedef long int_fast32_t;
typedef long int_fast64_t;
typedef unsigned char uint_fast8_t;
typedef unsigned long uint_fast16_t;
typedef unsigned long uint_fast32_t;
typedef unsigned long uint_fast64_t;
typedef long intptr_t;
typedef unsigned long uintptr_t;
typedef __intmax_t intmax_t;
typedef __uintmax_t uintmax_t;
typedef _Bool bool;
struct indirect_placeholder_76_ret_type;
struct indirect_placeholder_77_ret_type;
struct indirect_placeholder_76_ret_type {
    uint64_t field_0;
    uint64_t field_1;
};
struct indirect_placeholder_77_ret_type {
    uint64_t field_0;
    uint64_t field_1;
};
extern uint64_t revng_init_local_sp(uint64_t param_0);
extern uint64_t init_rbp(void);
extern uint64_t init_cc_src2(void);
extern uint64_t helper_cc_compute_c_wrapper(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint32_t param_3);
extern struct indirect_placeholder_76_ret_type indirect_placeholder_76(uint64_t param_0);
extern struct indirect_placeholder_77_ret_type indirect_placeholder_77(uint64_t param_0);
uint64_t bb_multiply(uint64_t rdx, uint64_t rcx, uint64_t rdi, uint64_t rsi, uint64_t r8) {
    uint64_t var_32;
    uint64_t var_33;
    uint64_t var_34;
    uint64_t var_0;
    uint64_t var_1;
    uint64_t var_2;
    uint64_t *var_3;
    uint64_t *var_4;
    uint64_t *var_5;
    uint64_t *var_6;
    uint64_t var_7;
    uint64_t *var_8;
    uint64_t var_9;
    uint64_t var_10;
    bool var_11;
    uint64_t *var_12;
    uint64_t var_21;
    uint64_t *var_22;
    uint64_t var_23;
    uint64_t *var_25;
    uint64_t *var_26;
    uint32_t *var_27;
    uint64_t *var_28;
    uint64_t *var_29;
    uint64_t *var_30;
    uint32_t *var_31;
    uint64_t var_43;
    uint64_t var_44;
    uint64_t storemerge;
    uint64_t var_35;
    uint64_t var_36;
    uint64_t var_37;
    uint64_t var_42;
    uint32_t var_38;
    uint64_t var_39;
    uint64_t var_40;
    uint64_t var_41;
    uint64_t var_24;
    uint64_t var_46;
    uint64_t var_13;
    struct indirect_placeholder_76_ret_type var_45;
    uint64_t *var_14;
    uint64_t var_15;
    uint64_t *var_16;
    uint64_t var_17;
    struct indirect_placeholder_77_ret_type var_18;
    uint64_t var_19;
    uint64_t *var_20;
    var_0 = revng_init_local_sp(0UL);
    var_1 = init_rbp();
    var_2 = init_cc_src2();
    *(uint64_t *)(var_0 + (-8L)) = var_1;
    var_3 = (uint64_t *)(var_0 + (-120L));
    *var_3 = rdi;
    var_4 = (uint64_t *)(var_0 + (-112L));
    *var_4 = rsi;
    var_5 = (uint64_t *)(var_0 + (-136L));
    *var_5 = rdx;
    var_6 = (uint64_t *)(var_0 + (-128L));
    *var_6 = rcx;
    var_7 = var_0 + (-144L);
    var_8 = (uint64_t *)var_7;
    *var_8 = r8;
    var_9 = *var_3;
    var_10 = *var_5;
    var_11 = (var_9 > var_10);
    var_12 = (uint64_t *)(var_0 + (-32L));
    var_32 = 0UL;
    var_35 = 0UL;
    storemerge = 0UL;
    if (var_11) {
        *var_12 = var_10;
        *(uint64_t *)(var_0 + (-16L)) = *var_6;
        *(uint64_t *)(var_0 + (-40L)) = *var_3;
        *(uint64_t *)(var_0 + (-24L)) = *var_4;
    } else {
        *var_12 = var_9;
        *(uint64_t *)(var_0 + (-16L)) = *var_4;
        *(uint64_t *)(var_0 + (-40L)) = *var_5;
        *(uint64_t *)(var_0 + (-24L)) = *var_6;
    }
    var_13 = *var_12;
    if (var_13 == 0UL) {
        **(uint64_t **)var_7 = 0UL;
        *(uint64_t *)(var_0 + (-160L)) = 4224727UL;
        var_45 = indirect_placeholder_76(1UL);
        *(uint64_t *)(*var_8 + 8UL) = var_45.field_0;
    } else {
        var_14 = (uint64_t *)(var_0 + (-40L));
        var_15 = *var_14 + var_13;
        var_16 = (uint64_t *)(var_0 + (-48L));
        *var_16 = var_15;
        var_17 = var_15 << 2UL;
        *(uint64_t *)(var_0 + (-160L)) = 4224777UL;
        var_18 = indirect_placeholder_77(var_17);
        var_19 = var_18.field_0;
        var_20 = (uint64_t *)(var_0 + (-88L));
        *var_20 = var_19;
        if (var_19 == 0UL) {
            return storemerge;
        }
        var_21 = *var_14;
        var_22 = (uint64_t *)(var_0 + (-56L));
        *var_22 = var_21;
        var_23 = var_21;
        while (var_23 != 0UL)
            {
                var_24 = var_23 + (-1L);
                *var_22 = var_24;
                *(uint32_t *)(*var_20 + (var_24 << 2UL)) = 0U;
                var_23 = *var_22;
            }
        var_25 = (uint64_t *)(var_0 + (-64L));
        *var_25 = 0UL;
        var_26 = (uint64_t *)(var_0 + (-16L));
        var_27 = (uint32_t *)(var_0 + (-92L));
        var_28 = (uint64_t *)(var_0 + (-80L));
        var_29 = (uint64_t *)(var_0 + (-72L));
        var_30 = (uint64_t *)(var_0 + (-24L));
        var_31 = (uint32_t *)(var_0 + (-96L));
        var_33 = *var_12;
        var_34 = helper_cc_compute_c_wrapper(var_32 - var_33, var_33, var_2, 17U);
        while (var_34 != 0UL)
            {
                *var_27 = *(uint32_t *)(*var_26 + (*var_25 << 2UL));
                *var_28 = 0UL;
                *var_29 = 0UL;
                var_36 = *var_14;
                var_37 = helper_cc_compute_c_wrapper(var_35 - var_36, var_36, var_2, 17U);
                while (var_37 != 0UL)
                    {
                        var_38 = *(uint32_t *)(*var_30 + (*var_29 << 2UL));
                        *var_31 = var_38;
                        var_39 = *var_28 + ((uint64_t)*var_27 * (uint64_t)var_38);
                        *var_28 = var_39;
                        var_40 = var_39 + (uint64_t)*(uint32_t *)(*var_20 + ((*var_29 + *var_25) << 2UL));
                        *var_28 = var_40;
                        *(uint32_t *)(*var_20 + ((*var_29 + *var_25) << 2UL)) = (uint32_t)var_40;
                        *var_28 = (*var_28 >> 32UL);
                        var_41 = *var_29 + 1UL;
                        *var_29 = var_41;
                        var_35 = var_41;
                        var_36 = *var_14;
                        var_37 = helper_cc_compute_c_wrapper(var_35 - var_36, var_36, var_2, 17U);
                    }
                *(uint32_t *)(*var_20 + ((*var_14 + *var_25) << 2UL)) = (uint32_t)*var_28;
                var_42 = *var_25 + 1UL;
                *var_25 = var_42;
                var_32 = var_42;
                var_33 = *var_12;
                var_34 = helper_cc_compute_c_wrapper(var_32 - var_33, var_33, var_2, 17U);
            }
        var_43 = *var_16;
        while (var_43 != 0UL)
            {
                if (*(uint32_t *)(*var_20 + ((var_43 << 2UL) + (-4L))) == 0U) {
                    break;
                }
                var_44 = var_43 + (-1L);
                *var_16 = var_44;
                var_43 = var_44;
            }
        **(uint64_t **)var_7 = var_43;
        *(uint64_t *)(*var_8 + 8UL) = *var_20;
    }
    var_46 = *(uint64_t *)(*var_8 + 8UL);
    storemerge = var_46;
    return storemerge;
}

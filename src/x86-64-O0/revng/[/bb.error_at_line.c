typedef unsigned char __u_char;
typedef unsigned short __u_short;
typedef unsigned int __u_int;
typedef unsigned long __u_long;
typedef signed char __int8_t;
typedef unsigned char __uint8_t;
typedef short __int16_t;
typedef unsigned short __uint16_t;
typedef int __int32_t;
typedef unsigned int __uint32_t;
typedef long __int64_t;
typedef unsigned long __uint64_t;
typedef __int8_t __int_least8_t;
typedef __uint8_t __uint_least8_t;
typedef __int16_t __int_least16_t;
typedef __uint16_t __uint_least16_t;
typedef __int32_t __int_least32_t;
typedef __uint32_t __uint_least32_t;
typedef __int64_t __int_least64_t;
typedef __uint64_t __uint_least64_t;
typedef long __quad_t;
typedef unsigned long __u_quad_t;
typedef long __intmax_t;
typedef unsigned long __uintmax_t;
typedef unsigned long __dev_t;
typedef unsigned int __uid_t;
typedef unsigned int __gid_t;
typedef unsigned long __ino_t;
typedef unsigned long __ino64_t;
typedef unsigned int __mode_t;
typedef unsigned long __nlink_t;
typedef long __off_t;
typedef long __off64_t;
typedef int __pid_t;
typedef struct {
    int __val[2];
} __fsid_t;
typedef long __clock_t;
typedef unsigned long __rlim_t;
typedef unsigned long __rlim64_t;
typedef unsigned int __id_t;
typedef long __time_t;
typedef unsigned int __useconds_t;
typedef long __suseconds_t;
typedef int __daddr_t;
typedef int __key_t;
typedef int __clockid_t;
typedef void *__timer_t;
typedef long __blksize_t;
typedef long __blkcnt_t;
typedef long __blkcnt64_t;
typedef unsigned long __fsblkcnt_t;
typedef unsigned long __fsblkcnt64_t;
typedef unsigned long __fsfilcnt_t;
typedef unsigned long __fsfilcnt64_t;
typedef long __fsword_t;
typedef long __ssize_t;
typedef long __syscall_slong_t;
typedef unsigned long __syscall_ulong_t;
typedef __off64_t __loff_t;
typedef char *__caddr_t;
typedef long __intptr_t;
typedef unsigned int __socklen_t;
typedef int __sig_atomic_t;
typedef __int8_t int8_t;
typedef __int16_t int16_t;
typedef __int32_t int32_t;
typedef __int64_t int64_t;
typedef __uint8_t uint8_t;
typedef __uint16_t uint16_t;
typedef __uint32_t uint32_t;
typedef __uint64_t uint64_t;
typedef __int_least8_t int_least8_t;
typedef __int_least16_t int_least16_t;
typedef __int_least32_t int_least32_t;
typedef __int_least64_t int_least64_t;
typedef __uint_least8_t uint_least8_t;
typedef __uint_least16_t uint_least16_t;
typedef __uint_least32_t uint_least32_t;
typedef __uint_least64_t uint_least64_t;
typedef signed char int_fast8_t;
typedef long int_fast16_t;
typedef long int_fast32_t;
typedef long int_fast64_t;
typedef unsigned char uint_fast8_t;
typedef unsigned long uint_fast16_t;
typedef unsigned long uint_fast32_t;
typedef unsigned long uint_fast64_t;
typedef long intptr_t;
typedef unsigned long uintptr_t;
typedef __intmax_t intmax_t;
typedef __uintmax_t uintmax_t;
typedef _Bool bool;
extern uint64_t revng_init_local_sp(uint64_t param_0);
extern uint64_t init_rbp(void);
extern uint64_t init_state_0x8558(void);
extern void indirect_placeholder(void);
extern uint64_t init_state_0x8598(void);
extern uint64_t init_state_0x85a0(void);
extern uint64_t init_state_0x8658(void);
extern uint64_t init_state_0x8660(void);
extern uint64_t init_state_0x8618(void);
extern uint64_t init_state_0x8620(void);
extern uint64_t init_state_0x8560(void);
extern uint64_t indirect_placeholder_18(void);
extern uint64_t init_state_0x85d8(void);
extern uint64_t init_state_0x85e0(void);
extern uint64_t init_state_0x8720(void);
extern uint64_t init_state_0x8698(void);
extern uint64_t init_state_0x86a0(void);
extern uint64_t init_state_0x86d8(void);
extern uint64_t init_state_0x86e0(void);
extern uint64_t init_state_0x8718(void);
extern void indirect_placeholder_2(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint64_t param_3);
void bb_error_at_line(uint64_t rax, uint64_t rdx, uint64_t rcx, uint64_t rdi, uint64_t rsi, uint64_t r9, uint64_t r8) {
    uint64_t local_sp_0;
    uint64_t var_0;
    uint64_t var_1;
    uint64_t var_2;
    uint64_t var_3;
    uint64_t var_4;
    uint64_t var_5;
    uint64_t var_6;
    uint64_t var_7;
    uint64_t var_8;
    uint64_t var_9;
    uint64_t var_10;
    uint64_t var_11;
    uint64_t var_12;
    uint64_t var_13;
    uint64_t var_14;
    uint64_t var_15;
    uint64_t var_16;
    uint64_t var_17;
    uint64_t var_18;
    uint32_t *var_19;
    uint32_t *var_20;
    uint64_t *var_21;
    uint32_t *var_22;
    uint64_t local_sp_2;
    uint64_t var_31;
    uint64_t var_32;
    uint64_t var_33;
    uint64_t var_34;
    uint64_t var_30;
    uint64_t *var_23;
    uint64_t local_sp_1;
    uint64_t var_24;
    uint64_t var_25;
    uint64_t var_26;
    bool var_27;
    uint64_t var_28;
    uint64_t *var_29;
    var_0 = revng_init_local_sp(0UL);
    var_1 = init_rbp();
    var_2 = init_state_0x8558();
    var_3 = init_state_0x8598();
    var_4 = init_state_0x85a0();
    var_5 = init_state_0x8560();
    var_6 = init_state_0x85d8();
    var_7 = init_state_0x85e0();
    var_8 = init_state_0x8618();
    var_9 = init_state_0x8620();
    var_10 = init_state_0x8658();
    var_11 = init_state_0x8660();
    var_12 = init_state_0x8698();
    var_13 = init_state_0x86a0();
    var_14 = init_state_0x86d8();
    var_15 = init_state_0x86e0();
    var_16 = init_state_0x8718();
    var_17 = init_state_0x8720();
    *(uint64_t *)(var_0 + (-8L)) = var_1;
    var_18 = var_0 + (-248L);
    var_19 = (uint32_t *)(var_0 + (-220L));
    *var_19 = (uint32_t)rdi;
    var_20 = (uint32_t *)(var_0 + (-224L));
    *var_20 = (uint32_t)rsi;
    var_21 = (uint64_t *)(var_0 + (-232L));
    *var_21 = rdx;
    var_22 = (uint32_t *)(var_0 + (-236L));
    *var_22 = (uint32_t)rcx;
    *(uint64_t *)(var_0 + (-144L)) = r9;
    local_sp_1 = var_18;
    local_sp_2 = var_18;
    if ((uint64_t)(unsigned char)rax == 0UL) {
        *(uint64_t *)(var_0 + (-136L)) = var_2;
        *(uint64_t *)(var_0 + (-128L)) = var_5;
        *(uint64_t *)(var_0 + (-120L)) = var_3;
        *(uint64_t *)(var_0 + (-112L)) = var_4;
        *(uint64_t *)(var_0 + (-104L)) = var_6;
        *(uint64_t *)(var_0 + (-96L)) = var_7;
        *(uint64_t *)(var_0 + (-88L)) = var_8;
        *(uint64_t *)(var_0 + (-80L)) = var_9;
        *(uint64_t *)(var_0 + (-72L)) = var_10;
        *(uint64_t *)(var_0 + (-64L)) = var_11;
        *(uint64_t *)(var_0 + (-56L)) = var_12;
        *(uint64_t *)(var_0 + (-48L)) = var_13;
        *(uint64_t *)(var_0 + (-40L)) = var_14;
        *(uint64_t *)(var_0 + (-32L)) = var_15;
        *(uint64_t *)(var_0 + (-24L)) = var_16;
        *(uint64_t *)(var_0 + (-16L)) = var_17;
    }
    var_23 = (uint64_t *)var_18;
    *var_23 = r8;
    if (*(uint32_t *)6387368UL == 0U) {
        *(uint64_t *)(local_sp_2 + (-8L)) = 4220835UL;
        indirect_placeholder();
        var_27 = (*(uint64_t *)6387360UL == 0UL);
        var_28 = local_sp_2 + (-16L);
        var_29 = (uint64_t *)var_28;
        local_sp_0 = var_28;
        if (var_27) {
            *var_29 = 4220863UL;
            indirect_placeholder_18();
            var_30 = local_sp_2 + (-24L);
            *(uint64_t *)var_30 = 4220891UL;
            indirect_placeholder();
            local_sp_0 = var_30;
        } else {
            *var_29 = 4220856UL;
            indirect_placeholder();
        }
        *(uint64_t *)(local_sp_0 + (-8L)) = 4220946UL;
        indirect_placeholder();
        var_31 = var_0 + (-208L);
        *(uint32_t *)var_31 = 40U;
        *(uint32_t *)(var_0 + (-204L)) = 48U;
        *(uint64_t *)(var_0 + (-200L)) = (var_0 | 8UL);
        *(uint64_t *)(var_0 + (-192L)) = (var_0 + (-184L));
        var_32 = *var_23;
        var_33 = (uint64_t)*var_20;
        var_34 = (uint64_t)*var_19;
        *(uint64_t *)(local_sp_0 + (-16L)) = 4221024UL;
        indirect_placeholder_2(var_32, var_31, var_34, var_33);
        return;
    }
    if ((uint64_t)(*(uint32_t *)6387328UL - *var_22) != 0UL) {
        var_24 = *(uint64_t *)6387336UL;
        var_25 = *var_21;
        if (var_25 == var_24) {
            return;
        }
        var_26 = var_0 + (-256L);
        *(uint64_t *)var_26 = 4220795UL;
        indirect_placeholder();
        local_sp_1 = var_26;
        if (!((var_24 == 0UL) || (var_25 == 0UL)) && (uint64_t)(uint32_t)var_24 == 0UL) {
            return;
        }
    }
    *(uint64_t *)6387336UL = *var_21;
    *(uint32_t *)6387328UL = *var_22;
    local_sp_2 = local_sp_1;
}

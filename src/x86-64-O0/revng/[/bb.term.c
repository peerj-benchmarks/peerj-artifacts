typedef unsigned char __u_char;
typedef unsigned short __u_short;
typedef unsigned int __u_int;
typedef unsigned long __u_long;
typedef signed char __int8_t;
typedef unsigned char __uint8_t;
typedef short __int16_t;
typedef unsigned short __uint16_t;
typedef int __int32_t;
typedef unsigned int __uint32_t;
typedef long __int64_t;
typedef unsigned long __uint64_t;
typedef __int8_t __int_least8_t;
typedef __uint8_t __uint_least8_t;
typedef __int16_t __int_least16_t;
typedef __uint16_t __uint_least16_t;
typedef __int32_t __int_least32_t;
typedef __uint32_t __uint_least32_t;
typedef __int64_t __int_least64_t;
typedef __uint64_t __uint_least64_t;
typedef long __quad_t;
typedef unsigned long __u_quad_t;
typedef long __intmax_t;
typedef unsigned long __uintmax_t;
typedef unsigned long __dev_t;
typedef unsigned int __uid_t;
typedef unsigned int __gid_t;
typedef unsigned long __ino_t;
typedef unsigned long __ino64_t;
typedef unsigned int __mode_t;
typedef unsigned long __nlink_t;
typedef long __off_t;
typedef long __off64_t;
typedef int __pid_t;
typedef struct {
    int __val[2];
} __fsid_t;
typedef long __clock_t;
typedef unsigned long __rlim_t;
typedef unsigned long __rlim64_t;
typedef unsigned int __id_t;
typedef long __time_t;
typedef unsigned int __useconds_t;
typedef long __suseconds_t;
typedef int __daddr_t;
typedef int __key_t;
typedef int __clockid_t;
typedef void *__timer_t;
typedef long __blksize_t;
typedef long __blkcnt_t;
typedef long __blkcnt64_t;
typedef unsigned long __fsblkcnt_t;
typedef unsigned long __fsblkcnt64_t;
typedef unsigned long __fsfilcnt_t;
typedef unsigned long __fsfilcnt64_t;
typedef long __fsword_t;
typedef long __ssize_t;
typedef long __syscall_slong_t;
typedef unsigned long __syscall_ulong_t;
typedef __off64_t __loff_t;
typedef char *__caddr_t;
typedef long __intptr_t;
typedef unsigned int __socklen_t;
typedef int __sig_atomic_t;
typedef __int8_t int8_t;
typedef __int16_t int16_t;
typedef __int32_t int32_t;
typedef __int64_t int64_t;
typedef __uint8_t uint8_t;
typedef __uint16_t uint16_t;
typedef __uint32_t uint32_t;
typedef __uint64_t uint64_t;
typedef __int_least8_t int_least8_t;
typedef __int_least16_t int_least16_t;
typedef __int_least32_t int_least32_t;
typedef __int_least64_t int_least64_t;
typedef __uint_least8_t uint_least8_t;
typedef __uint_least16_t uint_least16_t;
typedef __uint_least32_t uint_least32_t;
typedef __uint_least64_t uint_least64_t;
typedef signed char int_fast8_t;
typedef long int_fast16_t;
typedef long int_fast32_t;
typedef long int_fast64_t;
typedef unsigned char uint_fast8_t;
typedef unsigned long uint_fast16_t;
typedef unsigned long uint_fast32_t;
typedef unsigned long uint_fast64_t;
typedef long intptr_t;
typedef unsigned long uintptr_t;
typedef __intmax_t intmax_t;
typedef __uintmax_t uintmax_t;
typedef _Bool bool;
struct indirect_placeholder_39_ret_type;
struct indirect_placeholder_41_ret_type;
struct indirect_placeholder_42_ret_type;
struct indirect_placeholder_43_ret_type;
struct indirect_placeholder_40_ret_type;
struct indirect_placeholder_44_ret_type;
struct indirect_placeholder_45_ret_type;
struct indirect_placeholder_46_ret_type;
struct indirect_placeholder_39_ret_type {
    uint64_t field_0;
    uint64_t field_1;
    uint64_t field_2;
    uint64_t field_3;
    uint64_t field_4;
};
struct indirect_placeholder_41_ret_type {
    uint64_t field_0;
    uint64_t field_1;
    uint64_t field_2;
    uint64_t field_3;
    uint64_t field_4;
};
struct indirect_placeholder_42_ret_type {
    uint64_t field_0;
    uint64_t field_1;
    uint64_t field_2;
};
struct indirect_placeholder_43_ret_type {
    uint64_t field_0;
    uint64_t field_1;
    uint64_t field_2;
    uint64_t field_3;
};
struct indirect_placeholder_40_ret_type {
    uint64_t field_0;
    uint64_t field_1;
    uint64_t field_2;
    uint64_t field_3;
};
struct indirect_placeholder_44_ret_type {
    uint64_t field_0;
    uint64_t field_1;
    uint64_t field_2;
};
struct indirect_placeholder_45_ret_type {
    uint64_t field_0;
    uint64_t field_1;
    uint64_t field_2;
};
struct indirect_placeholder_46_ret_type {
    uint64_t field_0;
    uint64_t field_1;
    uint64_t field_2;
};
extern struct indirect_placeholder_39_ret_type indirect_placeholder_39(uint64_t param_0);
extern void abort(void);
extern uint64_t revng_init_local_sp(uint64_t param_0);
extern uint64_t init_rbp(void);
extern void indirect_placeholder(void);
extern uint64_t indirect_placeholder_18(void);
extern uint64_t init_rbx(void);
extern uint64_t indirect_placeholder_5(uint64_t param_0);
extern void indirect_placeholder_21(uint64_t param_0);
extern uint64_t indirect_placeholder_31(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint64_t param_3);
extern uint64_t init_r9(void);
extern uint64_t init_r8(void);
extern uint64_t init_rcx(void);
extern void indirect_placeholder_35(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint64_t param_3, uint64_t param_4, uint64_t param_5, uint64_t param_6, uint64_t param_7);
extern struct indirect_placeholder_41_ret_type indirect_placeholder_41(uint64_t param_0);
extern struct indirect_placeholder_42_ret_type indirect_placeholder_42(uint64_t param_0);
extern struct indirect_placeholder_43_ret_type indirect_placeholder_43(uint64_t param_0, uint64_t param_1);
extern struct indirect_placeholder_40_ret_type indirect_placeholder_40(uint64_t param_0, uint64_t param_1);
extern struct indirect_placeholder_44_ret_type indirect_placeholder_44(uint64_t param_0);
extern struct indirect_placeholder_45_ret_type indirect_placeholder_45(uint64_t param_0);
extern struct indirect_placeholder_46_ret_type indirect_placeholder_46(uint64_t param_0);
uint64_t bb_term(void) {
    uint64_t var_34;
    struct indirect_placeholder_39_ret_type var_35;
    uint64_t var_36;
    uint64_t var_37;
    uint64_t var_38;
    uint64_t var_39;
    uint64_t var_40;
    uint64_t r9_0;
    uint64_t r8_0;
    uint64_t var_25;
    unsigned char *var_26;
    uint64_t rcx_0;
    uint64_t var_19;
    unsigned char *var_20;
    uint64_t var_17;
    uint64_t var_0;
    uint64_t var_1;
    uint64_t var_2;
    uint64_t var_3;
    uint64_t var_4;
    uint64_t var_5;
    uint64_t var_6;
    uint64_t var_7;
    unsigned char *var_8;
    uint64_t var_32;
    unsigned char *var_33;
    uint64_t var_18;
    uint64_t local_sp_3;
    unsigned char *_pre_phi115;
    struct indirect_placeholder_41_ret_type var_66;
    uint64_t var_67;
    uint64_t var_68;
    uint64_t var_69;
    uint64_t var_70;
    uint64_t var_71;
    struct indirect_placeholder_43_ret_type var_59;
    uint64_t var_60;
    struct indirect_placeholder_40_ret_type var_61;
    uint64_t var_62;
    uint64_t var_63;
    uint64_t var_64;
    uint64_t var_65;
    uint32_t var_50;
    uint32_t var_52;
    uint32_t var_53;
    uint32_t var_51;
    uint32_t var_45;
    uint64_t local_sp_0;
    uint64_t var_15;
    uint64_t var_46;
    uint64_t var_47;
    uint64_t var_48;
    uint64_t var_49;
    uint64_t local_sp_1;
    uint64_t var_54;
    uint64_t var_55;
    unsigned char *var_56;
    uint64_t var_57;
    uint64_t var_58;
    uint64_t local_sp_2;
    uint32_t var_9;
    uint64_t var_10;
    uint32_t var_11;
    bool var_12;
    uint64_t var_13;
    uint64_t var_72;
    struct indirect_placeholder_44_ret_type var_73;
    uint64_t var_74;
    uint64_t var_75;
    uint64_t var_76;
    uint64_t var_14;
    uint64_t var_43;
    uint32_t *var_44;
    uint64_t var_16;
    uint32_t var_21;
    uint32_t var_22;
    uint32_t var_27;
    uint64_t var_23;
    uint64_t var_24;
    uint64_t local_sp_4;
    uint64_t var_28;
    unsigned char var_29;
    uint64_t var_30;
    uint64_t var_31;
    bool var_41;
    unsigned char *var_42;
    var_0 = revng_init_local_sp(0UL);
    var_1 = init_rbp();
    var_2 = init_rcx();
    var_3 = init_rbx();
    var_4 = init_r9();
    var_5 = init_r8();
    var_6 = var_0 + (-8L);
    *(uint64_t *)var_6 = var_1;
    *(uint64_t *)(var_0 + (-16L)) = var_3;
    var_7 = var_0 + (-40L);
    var_8 = (unsigned char *)(var_0 + (-26L));
    *var_8 = (unsigned char)'\x00';
    r9_0 = var_4;
    r8_0 = var_5;
    rcx_0 = var_2;
    var_45 = 1U;
    local_sp_2 = var_7;
    var_9 = *(uint32_t *)6386960UL;
    var_10 = (uint64_t)var_9;
    var_11 = *(uint32_t *)6386964UL;
    var_12 = ((long)(var_10 << 32UL) < (long)((uint64_t)var_11 << 32UL));
    local_sp_3 = local_sp_2;
    while (!var_12)
        {
            var_72 = local_sp_2 + (-8L);
            *(uint64_t *)var_72 = 4202666UL;
            var_73 = indirect_placeholder_44(1UL);
            var_74 = var_73.field_0;
            var_75 = var_73.field_1;
            var_76 = var_73.field_2;
            *var_8 = ((*var_8 == '\x00') & '\x01');
            local_sp_2 = var_72;
            rcx_0 = var_74;
            r9_0 = var_75;
            r8_0 = var_76;
            var_9 = *(uint32_t *)6386960UL;
            var_10 = (uint64_t)var_9;
            var_11 = *(uint32_t *)6386964UL;
            var_12 = ((long)(var_10 << 32UL) < (long)((uint64_t)var_11 << 32UL));
            local_sp_3 = local_sp_2;
        }
    if (var_12) {
        *(uint64_t *)(local_sp_2 + (-8L)) = 4202795UL;
        indirect_placeholder_21(var_6);
        abort();
    }
    var_14 = *(uint64_t *)6386968UL + ((uint64_t)var_9 << 3UL);
    if (**(unsigned char **)var_14 != '(') {
        if (*(unsigned char *)(*(uint64_t *)var_14 + 1UL) != '\x00') {
            var_43 = local_sp_2 + (-8L);
            *(uint64_t *)var_43 = 4202883UL;
            indirect_placeholder_45(1UL);
            var_44 = (uint32_t *)(var_0 + (-32L));
            *var_44 = 1U;
            local_sp_0 = var_43;
            var_46 = (uint64_t)*(uint32_t *)6386964UL;
            var_47 = ((uint64_t)*(uint32_t *)6386960UL + (uint64_t)var_45) << 32UL;
            var_53 = var_45;
            local_sp_1 = local_sp_0;
            while ((long)var_47 >= (long)(var_46 << 32UL))
                {
                    var_48 = *(uint64_t *)(*(uint64_t *)6386968UL + (uint64_t)((long)var_47 >> (long)29UL));
                    var_49 = local_sp_0 + (-8L);
                    *(uint64_t *)var_49 = 4202988UL;
                    indirect_placeholder();
                    local_sp_0 = var_49;
                    local_sp_1 = var_49;
                    if ((uint64_t)(uint32_t)var_48 != 0UL) {
                        var_53 = *var_44;
                        break;
                    }
                    var_50 = *var_44;
                    if (var_50 != 4U) {
                        var_52 = *(uint32_t *)6386964UL - *(uint32_t *)6386960UL;
                        *var_44 = var_52;
                        var_53 = var_52;
                        break;
                    }
                    var_51 = var_50 + 1U;
                    *var_44 = var_51;
                    var_45 = var_51;
                    var_46 = (uint64_t)*(uint32_t *)6386964UL;
                    var_47 = ((uint64_t)*(uint32_t *)6386960UL + (uint64_t)var_45) << 32UL;
                    var_53 = var_45;
                    local_sp_1 = local_sp_0;
                }
            var_54 = (uint64_t)var_53;
            *(uint64_t *)(local_sp_1 + (-8L)) = 4203002UL;
            var_55 = indirect_placeholder_5(var_54);
            var_56 = (unsigned char *)(var_0 + (-25L));
            *var_56 = (unsigned char)var_55;
            var_57 = *(uint64_t *)6386968UL + ((uint64_t)*(uint32_t *)6386960UL << 3UL);
            var_58 = *(uint64_t *)var_57;
            _pre_phi115 = var_56;
            if (var_58 == 0UL) {
                *(uint64_t *)(local_sp_1 + (-16L)) = 4203046UL;
                var_66 = indirect_placeholder_41(4272524UL);
                var_67 = var_66.field_0;
                var_68 = var_66.field_1;
                var_69 = var_66.field_2;
                var_70 = var_66.field_3;
                var_71 = var_66.field_4;
                *(uint64_t *)(local_sp_1 + (-24L)) = 4203064UL;
                indirect_placeholder_35(0UL, var_6, var_68, var_69, 4272526UL, var_67, var_70, var_71);
                abort();
            }
            if (**(unsigned char **)var_57 != ')') {
                if (*(unsigned char *)(var_58 + 1UL) != '\x00') {
                    *(uint64_t *)(local_sp_1 + (-16L)) = 4203222UL;
                    indirect_placeholder_42(0UL);
                    return (*_pre_phi115 != *var_8);
                }
            }
            *(uint64_t *)(local_sp_1 + (-16L)) = 4203173UL;
            var_59 = indirect_placeholder_43(1UL, var_58);
            var_60 = var_59.field_0;
            *(uint64_t *)(local_sp_1 + (-24L)) = 4203191UL;
            var_61 = indirect_placeholder_40(0UL, 4272524UL);
            var_62 = var_61.field_0;
            var_63 = var_61.field_1;
            var_64 = var_61.field_2;
            var_65 = var_61.field_3;
            *(uint64_t *)(local_sp_1 + (-32L)) = 4203212UL;
            indirect_placeholder_35(0UL, var_6, var_60, var_63, 4272538UL, var_62, var_64, var_65);
            abort();
        }
    }
    if ((int)(var_11 - var_9) <= (int)3U) {
        var_15 = *(uint64_t *)var_14;
        var_16 = local_sp_2 + (-8L);
        *(uint64_t *)var_16 = 4203287UL;
        indirect_placeholder();
        local_sp_3 = var_16;
        var_17 = local_sp_2 + (-16L);
        *(uint64_t *)var_17 = 4203329UL;
        var_18 = indirect_placeholder_18();
        local_sp_3 = var_17;
        if ((uint64_t)(uint32_t)var_15 != 0UL & (uint64_t)(unsigned char)var_18 != 0UL) {
            *(uint64_t *)(local_sp_2 + (-24L)) = 4203343UL;
            var_19 = indirect_placeholder_31(rcx_0, 1UL, r9_0, r8_0);
            var_20 = (unsigned char *)(var_0 + (-25L));
            *var_20 = (unsigned char)var_19;
            _pre_phi115 = var_20;
            return (*_pre_phi115 != *var_8);
        }
    }
    var_21 = *(uint32_t *)6386964UL;
    var_22 = *(uint32_t *)6386960UL;
    var_27 = var_22;
    local_sp_4 = local_sp_3;
    if ((int)(var_21 - var_22) <= (int)2U) {
        var_23 = local_sp_3 + (-8L);
        *(uint64_t *)var_23 = 4203410UL;
        var_24 = indirect_placeholder_18();
        local_sp_4 = var_23;
        if ((uint64_t)(unsigned char)var_24 != 0UL) {
            *(uint64_t *)(local_sp_3 + (-16L)) = 4203424UL;
            var_25 = indirect_placeholder_31(rcx_0, 0UL, r9_0, r8_0);
            var_26 = (unsigned char *)(var_0 + (-25L));
            *var_26 = (unsigned char)var_25;
            _pre_phi115 = var_26;
            return (*_pre_phi115 != *var_8);
        }
        var_27 = *(uint32_t *)6386960UL;
    }
    var_28 = *(uint64_t *)6386968UL + ((uint64_t)var_27 << 3UL);
    var_29 = **(unsigned char **)var_28;
    if (var_29 == '-') {
        var_41 = (var_29 != '\x00');
        var_42 = (unsigned char *)(var_0 + (-25L));
        *var_42 = var_41;
        *(uint64_t *)(local_sp_4 + (-8L)) = 4203694UL;
        indirect_placeholder_46(0UL);
        _pre_phi115 = var_42;
        return (*_pre_phi115 != *var_8);
    }
    var_30 = *(uint64_t *)var_28;
    if (~(*(unsigned char *)(var_30 + 1UL) != '\x00' & *(unsigned char *)(var_30 + 2UL) != '\x00')) {
        return;
    }
    *(uint64_t *)(local_sp_4 + (-8L)) = 4203581UL;
    var_31 = indirect_placeholder_5(var_30);
    if ((uint64_t)(unsigned char)var_31 != 0UL) {
        *(uint64_t *)(local_sp_4 + (-16L)) = 4203590UL;
        var_32 = indirect_placeholder_18();
        var_33 = (unsigned char *)(var_0 + (-25L));
        *var_33 = (unsigned char)var_32;
        _pre_phi115 = var_33;
        return (*_pre_phi115 != *var_8);
    }
    var_34 = *(uint64_t *)(*(uint64_t *)6386968UL + ((uint64_t)*(uint32_t *)6386960UL << 3UL));
    *(uint64_t *)(local_sp_4 + (-16L)) = 4203629UL;
    var_35 = indirect_placeholder_39(var_34);
    var_36 = var_35.field_0;
    var_37 = var_35.field_1;
    var_38 = var_35.field_2;
    var_39 = var_35.field_3;
    var_40 = var_35.field_4;
    *(uint64_t *)(local_sp_4 + (-24L)) = 4203647UL;
    indirect_placeholder_35(0UL, var_6, var_37, var_38, 4272563UL, var_36, var_39, var_40);
    abort();
}

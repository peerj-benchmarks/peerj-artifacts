typedef unsigned char __u_char;
typedef unsigned short __u_short;
typedef unsigned int __u_int;
typedef unsigned long __u_long;
typedef signed char __int8_t;
typedef unsigned char __uint8_t;
typedef short __int16_t;
typedef unsigned short __uint16_t;
typedef int __int32_t;
typedef unsigned int __uint32_t;
typedef long __int64_t;
typedef unsigned long __uint64_t;
typedef __int8_t __int_least8_t;
typedef __uint8_t __uint_least8_t;
typedef __int16_t __int_least16_t;
typedef __uint16_t __uint_least16_t;
typedef __int32_t __int_least32_t;
typedef __uint32_t __uint_least32_t;
typedef __int64_t __int_least64_t;
typedef __uint64_t __uint_least64_t;
typedef long __quad_t;
typedef unsigned long __u_quad_t;
typedef long __intmax_t;
typedef unsigned long __uintmax_t;
typedef unsigned long __dev_t;
typedef unsigned int __uid_t;
typedef unsigned int __gid_t;
typedef unsigned long __ino_t;
typedef unsigned long __ino64_t;
typedef unsigned int __mode_t;
typedef unsigned long __nlink_t;
typedef long __off_t;
typedef long __off64_t;
typedef int __pid_t;
typedef struct {
    int __val[2];
} __fsid_t;
typedef long __clock_t;
typedef unsigned long __rlim_t;
typedef unsigned long __rlim64_t;
typedef unsigned int __id_t;
typedef long __time_t;
typedef unsigned int __useconds_t;
typedef long __suseconds_t;
typedef int __daddr_t;
typedef int __key_t;
typedef int __clockid_t;
typedef void *__timer_t;
typedef long __blksize_t;
typedef long __blkcnt_t;
typedef long __blkcnt64_t;
typedef unsigned long __fsblkcnt_t;
typedef unsigned long __fsblkcnt64_t;
typedef unsigned long __fsfilcnt_t;
typedef unsigned long __fsfilcnt64_t;
typedef long __fsword_t;
typedef long __ssize_t;
typedef long __syscall_slong_t;
typedef unsigned long __syscall_ulong_t;
typedef __off64_t __loff_t;
typedef char *__caddr_t;
typedef long __intptr_t;
typedef unsigned int __socklen_t;
typedef int __sig_atomic_t;
typedef __int8_t int8_t;
typedef __int16_t int16_t;
typedef __int32_t int32_t;
typedef __int64_t int64_t;
typedef __uint8_t uint8_t;
typedef __uint16_t uint16_t;
typedef __uint32_t uint32_t;
typedef __uint64_t uint64_t;
typedef __int_least8_t int_least8_t;
typedef __int_least16_t int_least16_t;
typedef __int_least32_t int_least32_t;
typedef __int_least64_t int_least64_t;
typedef __uint_least8_t uint_least8_t;
typedef __uint_least16_t uint_least16_t;
typedef __uint_least32_t uint_least32_t;
typedef __uint_least64_t uint_least64_t;
typedef signed char int_fast8_t;
typedef long int_fast16_t;
typedef long int_fast32_t;
typedef long int_fast64_t;
typedef unsigned char uint_fast8_t;
typedef unsigned long uint_fast16_t;
typedef unsigned long uint_fast32_t;
typedef unsigned long uint_fast64_t;
typedef long intptr_t;
typedef unsigned long uintptr_t;
typedef __intmax_t intmax_t;
typedef __uintmax_t uintmax_t;
typedef _Bool bool;
struct indirect_placeholder_35_ret_type;
struct indirect_placeholder_32_ret_type;
struct indirect_placeholder_33_ret_type;
struct indirect_placeholder_34_ret_type;
struct indirect_placeholder_31_ret_type;
struct indirect_placeholder_35_ret_type {
    uint64_t field_0;
    uint64_t field_1;
    uint64_t field_2;
};
struct indirect_placeholder_32_ret_type {
    uint64_t field_0;
    uint64_t field_1;
    uint64_t field_2;
};
struct indirect_placeholder_33_ret_type {
    uint64_t field_0;
    uint64_t field_1;
    uint64_t field_2;
};
struct indirect_placeholder_34_ret_type {
    uint64_t field_0;
    uint64_t field_1;
    uint64_t field_2;
    uint64_t field_3;
    uint64_t field_4;
    uint64_t field_5;
};
struct indirect_placeholder_31_ret_type {
    uint64_t field_0;
    uint64_t field_1;
    uint64_t field_2;
};
extern uint64_t revng_init_local_sp(uint64_t param_0);
extern uint64_t init_rbp(void);
extern uint64_t indirect_placeholder_3(uint64_t param_0, uint64_t param_1);
extern void indirect_placeholder(void);
extern void indirect_placeholder_6(uint64_t param_0, uint64_t param_1);
extern uint64_t init_rbx(void);
extern void indirect_placeholder_1(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint64_t param_3, uint64_t param_4, uint64_t param_5, uint64_t param_6);
extern struct indirect_placeholder_35_ret_type indirect_placeholder_35(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint64_t param_3);
extern struct indirect_placeholder_32_ret_type indirect_placeholder_32(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint64_t param_3);
extern struct indirect_placeholder_33_ret_type indirect_placeholder_33(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint64_t param_3);
extern struct indirect_placeholder_34_ret_type indirect_placeholder_34(uint64_t param_0, uint64_t param_1);
extern struct indirect_placeholder_31_ret_type indirect_placeholder_31(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint64_t param_3);
uint64_t bb_elide_tail_lines_seekable(uint64_t rdx, uint64_t rcx, uint64_t rdi, uint64_t rsi, uint64_t r8) {
    uint64_t var_0;
    uint64_t var_1;
    uint64_t var_2;
    uint64_t var_3;
    uint64_t *var_4;
    uint32_t *var_5;
    uint64_t *var_6;
    uint64_t *var_7;
    uint64_t *var_8;
    uint64_t var_9;
    uint64_t var_10;
    uint64_t var_11;
    uint64_t *var_12;
    uint64_t spec_store_select;
    uint64_t var_13;
    uint64_t var_14;
    uint64_t var_15;
    struct indirect_placeholder_35_ret_type var_16;
    uint64_t var_17;
    uint64_t rax_0;
    uint64_t var_58;
    uint64_t var_59;
    uint64_t var_60;
    uint64_t var_61;
    uint64_t var_55;
    uint64_t var_56;
    uint64_t var_57;
    uint64_t var_29;
    uint64_t var_30;
    uint64_t var_43;
    uint64_t var_44;
    uint64_t var_37;
    uint64_t var_38;
    uint64_t var_39;
    uint64_t var_40;
    uint32_t *var_41;
    uint32_t var_42;
    uint64_t local_sp_4;
    uint64_t var_62;
    uint64_t var_63;
    uint64_t var_64;
    bool var_22;
    unsigned char *var_23;
    uint64_t var_24;
    uint64_t var_25;
    uint64_t local_sp_0;
    uint64_t *var_26;
    uint64_t var_27;
    uint64_t *var_28;
    uint64_t var_50;
    uint64_t local_sp_1;
    uint64_t var_51;
    uint64_t var_52;
    uint64_t var_53;
    struct indirect_placeholder_32_ret_type var_54;
    uint64_t local_sp_3;
    uint64_t var_32;
    uint64_t var_33;
    uint64_t var_34;
    uint64_t var_35;
    struct indirect_placeholder_33_ret_type var_36;
    uint64_t var_45;
    uint64_t var_46;
    uint64_t var_47;
    uint64_t var_48;
    struct indirect_placeholder_31_ret_type var_49;
    uint64_t var_31;
    uint64_t var_18;
    uint64_t var_19;
    uint64_t var_20;
    uint64_t var_21;
    unsigned int loop_state_var;
    bool switch_state_var;
    var_0 = revng_init_local_sp(0UL);
    var_1 = init_rbp();
    var_2 = init_rbx();
    var_3 = var_0 + (-8L);
    *(uint64_t *)var_3 = var_1;
    *(uint64_t *)(var_0 + (-16L)) = var_2;
    var_4 = (uint64_t *)(var_0 + (-1104L));
    *var_4 = rdi;
    var_5 = (uint32_t *)(var_0 + (-1108L));
    *var_5 = (uint32_t)rsi;
    var_6 = (uint64_t *)(var_0 + (-1120L));
    *var_6 = rdx;
    var_7 = (uint64_t *)(var_0 + (-1128L));
    *var_7 = rcx;
    *(uint64_t *)(var_0 + (-1136L)) = r8;
    var_8 = (uint64_t *)(var_0 + (-40L));
    *var_8 = r8;
    var_9 = r8 - *var_7;
    var_10 = (uint64_t)((long)var_9 >> (long)63UL) >> 54UL;
    var_11 = (uint64_t)(((uint16_t)var_9 + (uint16_t)var_10) & (unsigned short)1023U) - var_10;
    var_12 = (uint64_t *)(var_0 + (-32L));
    spec_store_select = (var_11 == 0UL) ? 1024UL : var_11;
    *var_12 = spec_store_select;
    var_13 = *var_8 - spec_store_select;
    *var_8 = var_13;
    var_14 = *var_4;
    var_15 = (uint64_t)*var_5;
    *(uint64_t *)(var_0 + (-1152L)) = 4206085UL;
    var_16 = indirect_placeholder_35(0UL, var_14, var_15, var_13);
    var_17 = var_16.field_1;
    rax_0 = 0UL;
    if ((long)var_16.field_0 > (long)18446744073709551615UL) {
        return rax_0;
    }
    var_18 = *var_12;
    var_19 = (uint64_t)*var_5;
    var_20 = var_0 + (-1160L);
    *(uint64_t *)var_20 = 4206127UL;
    var_21 = indirect_placeholder_3(var_18, var_19);
    *var_12 = var_21;
    local_sp_0 = var_20;
    rax_0 = 1UL;
    if (var_21 != 18446744073709551615UL) {
        var_22 = (*var_6 == 0UL);
        var_23 = (unsigned char *)(var_0 + (-49L));
        *var_23 = var_22;
        var_24 = *var_6;
        var_25 = *var_12;
        if (var_24 != 0UL & var_25 != 0UL & (uint64_t)(*(unsigned char *)(((var_25 + (-1L)) + var_3) + (-1088L)) - *(unsigned char *)6388626UL) == 0UL) {
            *var_6 = (var_24 + (-1L));
        }
        var_26 = (uint64_t *)(var_0 + (-48L));
        var_27 = var_0 + (-1096L);
        var_28 = (uint64_t *)(var_0 + (-64L));
        var_29 = *var_12;
        while (1U)
            {
                *var_26 = var_29;
                var_30 = var_29;
                local_sp_1 = local_sp_0;
                while (1U)
                    {
                        local_sp_3 = local_sp_1;
                        if (var_30 != 0UL) {
                            loop_state_var = 2U;
                            break;
                        }
                        if (*var_23 == '\x00') {
                            var_31 = local_sp_1 + (-8L);
                            *(uint64_t *)var_31 = 4206326UL;
                            indirect_placeholder();
                            *var_28 = var_27;
                            *var_26 = 0UL;
                            local_sp_3 = var_31;
                        } else {
                            *var_26 = (var_30 + (-1L));
                        }
                        var_32 = *var_6;
                        *var_6 = (var_32 + (-1L));
                        local_sp_1 = local_sp_3;
                        local_sp_4 = local_sp_3;
                        if (var_32 == 0UL) {
                            var_30 = *var_26;
                            continue;
                        }
                        var_33 = *var_7;
                        if ((long)var_33 >= (long)*var_8) {
                            loop_state_var = 1U;
                            break;
                        }
                        var_34 = *var_4;
                        var_35 = (uint64_t)*var_5;
                        *(uint64_t *)(local_sp_3 + (-8L)) = 4206438UL;
                        var_36 = indirect_placeholder_33(0UL, var_34, var_35, var_33);
                        if ((long)var_36.field_0 <= (long)18446744073709551615UL) {
                            loop_state_var = 0U;
                            break;
                        }
                        var_37 = *var_8 - *var_7;
                        var_38 = (uint64_t)*var_5;
                        var_39 = local_sp_3 + (-16L);
                        *(uint64_t *)var_39 = 4206483UL;
                        var_40 = indirect_placeholder_3(var_38, var_37);
                        var_41 = (uint32_t *)(var_0 + (-68L));
                        var_42 = (uint32_t)var_40;
                        *var_41 = var_42;
                        local_sp_4 = var_39;
                        if (var_42 != 0U) {
                            loop_state_var = 1U;
                            break;
                        }
                        var_43 = *var_4;
                        var_44 = (uint64_t)var_42;
                        *(uint64_t *)(local_sp_3 + (-24L)) = 4206512UL;
                        indirect_placeholder_6(var_44, var_43);
                        loop_state_var = 0U;
                        break;
                    }
                switch_state_var = 0;
                switch (loop_state_var) {
                  case 0U:
                    {
                        switch_state_var = 1;
                        break;
                    }
                    break;
                  case 2U:
                    {
                        var_50 = *var_8;
                        if (var_50 == *var_7) {
                            switch_state_var = 1;
                            break;
                        }
                        var_51 = var_50 + (-1024L);
                        *var_8 = var_51;
                        var_52 = *var_4;
                        var_53 = (uint64_t)*var_5;
                        *(uint64_t *)(local_sp_1 + (-8L)) = 4206680UL;
                        var_54 = indirect_placeholder_32(0UL, var_52, var_53, var_51);
                        rax_0 = 0UL;
                        if ((long)var_54.field_0 > (long)18446744073709551615UL) {
                            switch_state_var = 1;
                            break;
                        }
                        var_55 = (uint64_t)*var_5;
                        var_56 = local_sp_1 + (-16L);
                        *(uint64_t *)var_56 = 4206720UL;
                        var_57 = indirect_placeholder_3(1024UL, var_55);
                        *var_12 = var_57;
                        var_29 = var_57;
                        local_sp_0 = var_56;
                        rax_0 = 1UL;
                        switch_state_var = 0;
                        switch (var_57) {
                          case 0UL:
                            {
                                switch_state_var = 1;
                                break;
                            }
                            break;
                          case 18446744073709551615UL:
                            {
                                var_58 = var_54.field_1;
                                var_59 = *var_4;
                                *(uint64_t *)(local_sp_1 + (-24L)) = 4206751UL;
                                var_60 = indirect_placeholder_3(4UL, var_59);
                                *(uint64_t *)(local_sp_1 + (-32L)) = 4206759UL;
                                indirect_placeholder();
                                var_61 = (uint64_t)*(uint32_t *)var_60;
                                *(uint64_t *)(local_sp_1 + (-40L)) = 4206786UL;
                                indirect_placeholder_1(0UL, 4277750UL, var_60, 0UL, var_58, var_61, r8);
                                rax_0 = 0UL;
                                switch_state_var = 1;
                                break;
                            }
                            break;
                          default:
                            {
                                continue;
                            }
                            break;
                        }
                        if (switch_state_var)
                            break;
                    }
                    break;
                  case 1U:
                    {
                        var_45 = *var_26 + 1UL;
                        *(uint64_t *)(local_sp_4 + (-8L)) = 4206548UL;
                        indirect_placeholder_34(var_27, var_45);
                        var_46 = (*var_26 + *var_8) + 1UL;
                        var_47 = *var_4;
                        var_48 = (uint64_t)*var_5;
                        *(uint64_t *)(local_sp_4 + (-16L)) = 4206594UL;
                        var_49 = indirect_placeholder_31(0UL, var_47, var_48, var_46);
                        rax_0 = (var_49.field_0 >> 63UL) ^ 1UL;
                        switch_state_var = 1;
                        break;
                    }
                    break;
                }
                if (switch_state_var)
                    break;
            }
    }
    var_62 = *var_4;
    *(uint64_t *)(var_0 + (-1168L)) = 4206158UL;
    var_63 = indirect_placeholder_3(4UL, var_62);
    *(uint64_t *)(var_0 + (-1176L)) = 4206166UL;
    indirect_placeholder();
    var_64 = (uint64_t)*(uint32_t *)var_63;
    *(uint64_t *)(var_0 + (-1184L)) = 4206193UL;
    indirect_placeholder_1(0UL, 4277750UL, var_63, 0UL, var_17, var_64, r8);
    return rax_0;
}

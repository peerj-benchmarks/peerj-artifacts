typedef unsigned char __u_char;
typedef unsigned short __u_short;
typedef unsigned int __u_int;
typedef unsigned long __u_long;
typedef signed char __int8_t;
typedef unsigned char __uint8_t;
typedef short __int16_t;
typedef unsigned short __uint16_t;
typedef int __int32_t;
typedef unsigned int __uint32_t;
typedef long __int64_t;
typedef unsigned long __uint64_t;
typedef __int8_t __int_least8_t;
typedef __uint8_t __uint_least8_t;
typedef __int16_t __int_least16_t;
typedef __uint16_t __uint_least16_t;
typedef __int32_t __int_least32_t;
typedef __uint32_t __uint_least32_t;
typedef __int64_t __int_least64_t;
typedef __uint64_t __uint_least64_t;
typedef long __quad_t;
typedef unsigned long __u_quad_t;
typedef long __intmax_t;
typedef unsigned long __uintmax_t;
typedef unsigned long __dev_t;
typedef unsigned int __uid_t;
typedef unsigned int __gid_t;
typedef unsigned long __ino_t;
typedef unsigned long __ino64_t;
typedef unsigned int __mode_t;
typedef unsigned long __nlink_t;
typedef long __off_t;
typedef long __off64_t;
typedef int __pid_t;
typedef struct {
    int __val[2];
} __fsid_t;
typedef long __clock_t;
typedef unsigned long __rlim_t;
typedef unsigned long __rlim64_t;
typedef unsigned int __id_t;
typedef long __time_t;
typedef unsigned int __useconds_t;
typedef long __suseconds_t;
typedef int __daddr_t;
typedef int __key_t;
typedef int __clockid_t;
typedef void *__timer_t;
typedef long __blksize_t;
typedef long __blkcnt_t;
typedef long __blkcnt64_t;
typedef unsigned long __fsblkcnt_t;
typedef unsigned long __fsblkcnt64_t;
typedef unsigned long __fsfilcnt_t;
typedef unsigned long __fsfilcnt64_t;
typedef long __fsword_t;
typedef long __ssize_t;
typedef long __syscall_slong_t;
typedef unsigned long __syscall_ulong_t;
typedef __off64_t __loff_t;
typedef char *__caddr_t;
typedef long __intptr_t;
typedef unsigned int __socklen_t;
typedef int __sig_atomic_t;
typedef __int8_t int8_t;
typedef __int16_t int16_t;
typedef __int32_t int32_t;
typedef __int64_t int64_t;
typedef __uint8_t uint8_t;
typedef __uint16_t uint16_t;
typedef __uint32_t uint32_t;
typedef __uint64_t uint64_t;
typedef __int_least8_t int_least8_t;
typedef __int_least16_t int_least16_t;
typedef __int_least32_t int_least32_t;
typedef __int_least64_t int_least64_t;
typedef __uint_least8_t uint_least8_t;
typedef __uint_least16_t uint_least16_t;
typedef __uint_least32_t uint_least32_t;
typedef __uint_least64_t uint_least64_t;
typedef signed char int_fast8_t;
typedef long int_fast16_t;
typedef long int_fast32_t;
typedef long int_fast64_t;
typedef unsigned char uint_fast8_t;
typedef unsigned long uint_fast16_t;
typedef unsigned long uint_fast32_t;
typedef unsigned long uint_fast64_t;
typedef long intptr_t;
typedef unsigned long uintptr_t;
typedef __intmax_t intmax_t;
typedef __uintmax_t uintmax_t;
struct indirect_placeholder_45_ret_type;
struct indirect_placeholder_47_ret_type;
struct indirect_placeholder_45_ret_type {
    uint64_t field_0;
    uint64_t field_1;
};
struct indirect_placeholder_47_ret_type {
    uint64_t field_0;
    uint64_t field_1;
    uint64_t field_2;
};
extern void abort(void);
extern void function_dispatcher(unsigned char *param_0);
extern uint64_t revng_init_local_sp(uint64_t param_0);
extern uint64_t init_rbp(void);
extern uint64_t indirect_placeholder_3(uint64_t param_0, uint64_t param_1);
extern void indirect_placeholder(void);
extern void indirect_placeholder_6(uint64_t param_0, uint64_t param_1);
extern void indirect_placeholder_10(uint64_t param_0);
extern uint64_t indirect_placeholder_30(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint64_t param_3);
extern uint64_t init_r9(void);
extern uint64_t init_r8(void);
extern void indirect_placeholder_1(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint64_t param_3, uint64_t param_4, uint64_t param_5, uint64_t param_6);
extern struct indirect_placeholder_45_ret_type indirect_placeholder_45(uint64_t param_0);
extern void indirect_placeholder_46(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint64_t param_3, uint64_t param_4, uint64_t param_5);
extern struct indirect_placeholder_47_ret_type indirect_placeholder_47(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint64_t param_3, uint64_t param_4);
typedef _Bool bool;
void bb_main(uint64_t rdi, uint64_t rsi) {
    uint64_t var_0;
    uint64_t var_1;
    uint64_t var_2;
    uint64_t var_3;
    uint64_t var_4;
    uint32_t *var_5;
    uint64_t var_6;
    uint64_t *var_7;
    uint32_t *var_8;
    unsigned char *var_9;
    struct indirect_placeholder_47_ret_type var_43;
    uint64_t *var_10;
    unsigned char *var_11;
    unsigned char *var_12;
    uint64_t **var_13;
    uint64_t var_14;
    uint64_t var_15;
    uint32_t var_16;
    uint32_t var_37;
    uint64_t storemerge;
    uint64_t var_74;
    uint64_t local_sp_0;
    uint64_t var_75;
    uint64_t var_76;
    uint64_t var_77;
    uint64_t var_78;
    uint64_t var_79;
    uint64_t var_80;
    uint64_t var_81;
    uint64_t local_sp_1;
    uint64_t var_63;
    uint64_t var_64;
    uint64_t var_65;
    struct indirect_placeholder_45_ret_type var_66;
    uint64_t var_67;
    uint64_t var_68;
    uint64_t var_69;
    uint32_t var_70;
    uint64_t *var_71;
    uint64_t var_72;
    uint64_t *var_73;
    uint32_t var_54;
    uint64_t var_45;
    uint64_t var_46;
    uint64_t *var_47;
    unsigned char var_48;
    uint64_t var_50;
    uint64_t var_49;
    uint64_t var_51;
    uint64_t var_52;
    uint64_t var_53;
    uint64_t local_sp_5_be;
    uint64_t local_sp_2;
    uint64_t r9_1;
    uint64_t r8_1;
    uint64_t local_sp_3;
    uint64_t var_55;
    uint64_t var_56;
    unsigned char var_57;
    uint64_t var_59;
    uint64_t var_58;
    uint64_t var_60;
    uint64_t var_61;
    uint64_t var_62;
    uint32_t var_39;
    uint64_t var_17;
    uint64_t var_18;
    uint64_t var_19;
    uint64_t *var_20;
    uint64_t *var_21;
    unsigned char *var_22;
    uint64_t var_23;
    uint64_t var_24;
    unsigned char **var_25;
    uint64_t var_26;
    uint64_t *var_27;
    unsigned char var_28;
    unsigned char **var_31;
    uint64_t var_32;
    uint64_t var_33;
    uint64_t var_34;
    uint64_t var_35;
    uint32_t var_36;
    uint32_t var_29;
    uint64_t var_30;
    uint64_t local_sp_4;
    uint32_t *var_38;
    uint64_t local_sp_5;
    uint64_t var_40;
    uint64_t var_41;
    uint64_t var_42;
    uint32_t var_44;
    unsigned int loop_state_var;
    bool switch_state_var;
    var_0 = revng_init_local_sp(0UL);
    var_1 = init_rbp();
    var_2 = init_r9();
    var_3 = init_r8();
    var_4 = var_0 + (-8L);
    *(uint64_t *)var_4 = var_1;
    var_5 = (uint32_t *)(var_0 + (-124L));
    *var_5 = (uint32_t)rdi;
    var_6 = var_0 + (-136L);
    var_7 = (uint64_t *)var_6;
    *var_7 = rsi;
    var_8 = (uint32_t *)(var_0 + (-12L));
    *var_8 = 0U;
    var_9 = (unsigned char *)(var_0 + (-13L));
    *var_9 = (unsigned char)'\x01';
    var_10 = (uint64_t *)(var_0 + (-32L));
    *var_10 = 10UL;
    var_11 = (unsigned char *)(var_0 + (-33L));
    *var_11 = (unsigned char)'\x01';
    var_12 = (unsigned char *)(var_0 + (-34L));
    *var_12 = (unsigned char)'\x00';
    var_13 = (uint64_t **)var_6;
    var_14 = **var_13;
    *(uint64_t *)(var_0 + (-144L)) = 4208586UL;
    indirect_placeholder_10(var_14);
    *(uint64_t *)(var_0 + (-152L)) = 4208601UL;
    indirect_placeholder();
    var_15 = var_0 + (-160L);
    *(uint64_t *)var_15 = 4208611UL;
    indirect_placeholder();
    *(unsigned char *)6388627UL = (unsigned char)'\x00';
    *(unsigned char *)6388625UL = (unsigned char)'\x00';
    *(unsigned char *)6388626UL = (unsigned char)'\n';
    var_16 = *var_5;
    var_74 = 0UL;
    storemerge = 4278352UL;
    var_37 = var_16;
    local_sp_4 = var_15;
    var_17 = *var_7 + 8UL;
    var_18 = *(uint64_t *)var_17 + 1UL;
    if ((int)var_16 <= (int)1U & **(unsigned char **)var_17 != '-' & (uint64_t)(((uint32_t)(uint64_t)*(unsigned char *)var_18 + (-48)) & (-2)) <= 9UL) {
        var_19 = var_0 + (-48L);
        var_20 = (uint64_t *)var_19;
        *var_20 = var_18;
        var_21 = (uint64_t *)(var_0 + (-64L));
        *var_21 = var_18;
        var_22 = (unsigned char *)(var_0 + (-49L));
        *var_22 = (unsigned char)'\x00';
        var_23 = *var_20;
        var_24 = var_23 + 1UL;
        *var_20 = var_24;
        var_25 = (unsigned char **)var_19;
        var_23 = var_24;
        do {
            var_24 = var_23 + 1UL;
            *var_20 = var_24;
            var_25 = (unsigned char **)var_19;
            var_23 = var_24;
        } while ((uint64_t)(((uint32_t)(uint64_t)**var_25 + (-48)) & (-2)) <= 9UL);
        var_26 = var_0 + (-72L);
        var_27 = (uint64_t *)var_26;
        *var_27 = var_24;
        var_28 = **var_25;
        if (var_28 != '\x00') {
            var_29 = (uint32_t)(uint64_t)var_28;
            if ((uint64_t)(var_29 + (-98)) <= 24UL) {
                function_dispatcher((unsigned char *)(0UL));
                return;
            }
            var_30 = (uint64_t)var_29;
            *(uint64_t *)(var_0 + (-168L)) = 4208888UL;
            indirect_placeholder_1(0UL, 4278058UL, var_30, 0UL, var_2, 0UL, var_3);
            *(uint64_t *)(var_0 + (-176L)) = 4208898UL;
            indirect_placeholder_6(var_4, 1UL);
            abort();
        }
        var_31 = (unsigned char **)var_26;
        **var_31 = *var_22;
        if (*var_22 != '\x00') {
            *var_27 = (*var_27 + 1UL);
            **var_31 = (unsigned char)'\x00';
        }
        var_32 = (uint64_t)*var_11;
        var_33 = *var_21;
        var_34 = var_0 + (-168L);
        *(uint64_t *)var_34 = 4208964UL;
        var_35 = indirect_placeholder_3(var_32, var_33);
        *var_10 = var_35;
        *(uint64_t *)(*var_7 + 8UL) = **var_13;
        *var_7 = (*var_7 + 8UL);
        var_36 = *var_5 + (-1);
        *var_5 = var_36;
        var_37 = var_36;
        local_sp_4 = var_34;
    }
    var_38 = (uint32_t *)(var_0 + (-76L));
    var_39 = var_37;
    local_sp_5 = local_sp_4;
    while (1U)
        {
            var_40 = *var_7;
            var_41 = (uint64_t)var_39;
            var_42 = local_sp_5 + (-8L);
            *(uint64_t *)var_42 = 4209460UL;
            var_43 = indirect_placeholder_47(4278117UL, 4276352UL, var_41, var_40, 0UL);
            var_44 = (uint32_t)var_43.field_0;
            *var_38 = var_44;
            local_sp_1 = var_42;
            var_54 = var_44;
            local_sp_2 = var_42;
            local_sp_5_be = var_42;
            if (var_44 != 4294967295U) {
                switch_state_var = 0;
                switch (*var_8) {
                  case 0U:
                    {
                        if ((long)(((uint64_t)*var_5 << 32UL) + (-4294967296L)) <= (long)((uint64_t)*(uint32_t *)6388408UL << 32UL)) {
                            loop_state_var = 3U;
                            switch_state_var = 1;
                            break;
                        }
                        loop_state_var = 2U;
                        switch_state_var = 1;
                        break;
                    }
                    break;
                  case 1U:
                    {
                        loop_state_var = 3U;
                        switch_state_var = 1;
                        break;
                    }
                    break;
                  default:
                    {
                        loop_state_var = 2U;
                        switch_state_var = 1;
                        break;
                    }
                    break;
                }
                if (switch_state_var)
                    break;
            }
            if ((uint64_t)(var_44 + (-110)) == 0UL) {
                *var_11 = (unsigned char)'\x01';
                var_57 = (**(unsigned char **)6389080UL == '-');
                *var_12 = var_57;
                if (var_57 == '\x00') {
                    var_59 = *(uint64_t *)6389080UL;
                } else {
                    var_58 = *(uint64_t *)6389080UL + 1UL;
                    *(uint64_t *)6389080UL = var_58;
                    var_59 = var_58;
                }
                var_60 = (uint64_t)*var_11;
                var_61 = local_sp_5 + (-16L);
                *(uint64_t *)var_61 = 4209252UL;
                var_62 = indirect_placeholder_3(var_60, var_59);
                *var_10 = var_62;
                local_sp_5_be = var_61;
            } else {
                if ((int)var_44 > (int)110U) {
                    if ((uint64_t)(var_44 + (-118)) == 0UL) {
                        *var_8 = 1U;
                    } else {
                        if ((int)var_44 > (int)118U) {
                            if ((uint64_t)(var_44 + (-122)) == 0UL) {
                                *(unsigned char *)6388626UL = (unsigned char)'\x00';
                            } else {
                                if ((uint64_t)(var_44 + (-128)) != 0UL) {
                                    loop_state_var = 1U;
                                    break;
                                }
                                *(unsigned char *)6388624UL = (unsigned char)'\x01';
                            }
                        } else {
                            if ((uint64_t)(var_44 + (-113)) != 0UL) {
                                loop_state_var = 1U;
                                break;
                            }
                            *var_8 = 2U;
                        }
                    }
                } else {
                    if ((uint64_t)(var_44 + 130U) == 0UL) {
                        *(uint64_t *)(local_sp_5 + (-16L)) = 4209307UL;
                        indirect_placeholder_6(var_4, 0UL);
                        abort();
                    }
                    if ((uint64_t)(var_44 + (-99)) != 0UL) {
                        r9_1 = var_43.field_1;
                        r8_1 = var_43.field_2;
                        if (var_44 != 4294967165U) {
                            loop_state_var = 0U;
                            break;
                        }
                        var_45 = *(uint64_t *)6388264UL;
                        var_46 = local_sp_5 + (-24L);
                        var_47 = (uint64_t *)var_46;
                        *var_47 = 0UL;
                        *(uint64_t *)(local_sp_5 + (-32L)) = 4209365UL;
                        indirect_placeholder_46(0UL, 4275992UL, var_45, 4278088UL, 4277745UL, 4278101UL);
                        *var_47 = 4209379UL;
                        indirect_placeholder();
                        var_54 = *var_38;
                        local_sp_2 = var_46;
                        r9_1 = 4278088UL;
                        r8_1 = 4278101UL;
                        loop_state_var = 0U;
                        break;
                    }
                    *var_11 = (unsigned char)'\x00';
                    var_48 = (**(unsigned char **)6389080UL == '-');
                    *var_12 = var_48;
                    if (var_48 == '\x00') {
                        var_50 = *(uint64_t *)6389080UL;
                    } else {
                        var_49 = *(uint64_t *)6389080UL + 1UL;
                        *(uint64_t *)6389080UL = var_49;
                        var_50 = var_49;
                    }
                    var_51 = (uint64_t)*var_11;
                    var_52 = local_sp_5 + (-16L);
                    *(uint64_t *)var_52 = 4209176UL;
                    var_53 = indirect_placeholder_3(var_51, var_50);
                    *var_10 = var_53;
                    local_sp_5_be = var_52;
                }
            }
            var_39 = *var_5;
            local_sp_5 = local_sp_5_be;
            continue;
        }
    switch (loop_state_var) {
      case 3U:
      case 2U:
        {
            switch (loop_state_var) {
              case 3U:
                {
                    *(unsigned char *)6388625UL = (unsigned char)'\x01';
                }
                break;
              case 2U:
                {
                    var_63 = *var_10;
                    if (*var_11 != '\x01' & *var_12 != '\x00' & (long)var_63 > (long)18446744073709551615UL) {
                        var_64 = var_0 + (-120L);
                        *(uint64_t *)(local_sp_5 + (-16L)) = 4209553UL;
                        var_65 = indirect_placeholder_3(var_63, var_64);
                        *(uint64_t *)(local_sp_5 + (-24L)) = 4209561UL;
                        var_66 = indirect_placeholder_45(var_65);
                        var_67 = var_66.field_0;
                        var_68 = var_66.field_1;
                        var_69 = local_sp_5 + (-32L);
                        *(uint64_t *)var_69 = 4209594UL;
                        indirect_placeholder_1(0UL, 4278135UL, 4278021UL, 1UL, var_68, 75UL, var_67);
                        local_sp_1 = var_69;
                    }
                    var_70 = *(uint32_t *)6388408UL;
                    if ((long)((uint64_t)var_70 << 32UL) < (long)((uint64_t)*var_5 << 32UL)) {
                        storemerge = *var_7 + ((uint64_t)var_70 << 3UL);
                    }
                    var_71 = (uint64_t *)(var_0 + (-88L));
                    *var_71 = storemerge;
                    var_72 = local_sp_1 + (-8L);
                    *(uint64_t *)var_72 = 4209654UL;
                    indirect_placeholder_6(1UL, 0UL);
                    var_73 = (uint64_t *)(var_0 + (-24L));
                    *var_73 = 0UL;
                    local_sp_0 = var_72;
                    var_75 = *(uint64_t *)(*var_71 + (var_74 << 3UL));
                    while (var_75 != 0UL)
                        {
                            var_76 = (uint64_t)*var_12;
                            var_77 = (uint64_t)*var_11;
                            var_78 = *var_10;
                            var_79 = local_sp_0 + (-8L);
                            *(uint64_t *)var_79 = 4209706UL;
                            var_80 = indirect_placeholder_30(var_77, var_76, var_75, var_78);
                            *var_9 = ((var_80 & (uint64_t)*var_9) != 0UL);
                            var_81 = *var_73 + 1UL;
                            *var_73 = var_81;
                            var_74 = var_81;
                            local_sp_0 = var_79;
                            var_75 = *(uint64_t *)(*var_71 + (var_74 << 3UL));
                        }
                    if (*(unsigned char *)6388627UL != '\x00') {
                        *(uint64_t *)(local_sp_0 + (-8L)) = 4209776UL;
                        indirect_placeholder();
                    }
                    return;
                }
                break;
            }
        }
        break;
      case 1U:
      case 0U:
        {
            switch (loop_state_var) {
              case 1U:
                {
                    r9_1 = var_43.field_1;
                    r8_1 = var_43.field_2;
                }
                break;
              case 0U:
                {
                    local_sp_3 = local_sp_2;
                    if ((var_54 + (-48)) <= 9U) {
                        var_55 = (uint64_t)var_54;
                        var_56 = local_sp_2 + (-8L);
                        *(uint64_t *)var_56 = 4209420UL;
                        indirect_placeholder_1(0UL, 4278058UL, var_55, 0UL, r9_1, 0UL, r8_1);
                        local_sp_3 = var_56;
                    }
                    *(uint64_t *)(local_sp_3 + (-8L)) = 4209430UL;
                    indirect_placeholder_6(var_4, 1UL);
                    abort();
                }
                break;
            }
        }
        break;
    }
}

typedef unsigned char __u_char;
typedef unsigned short __u_short;
typedef unsigned int __u_int;
typedef unsigned long __u_long;
typedef signed char __int8_t;
typedef unsigned char __uint8_t;
typedef short __int16_t;
typedef unsigned short __uint16_t;
typedef int __int32_t;
typedef unsigned int __uint32_t;
typedef long __int64_t;
typedef unsigned long __uint64_t;
typedef __int8_t __int_least8_t;
typedef __uint8_t __uint_least8_t;
typedef __int16_t __int_least16_t;
typedef __uint16_t __uint_least16_t;
typedef __int32_t __int_least32_t;
typedef __uint32_t __uint_least32_t;
typedef __int64_t __int_least64_t;
typedef __uint64_t __uint_least64_t;
typedef long __quad_t;
typedef unsigned long __u_quad_t;
typedef long __intmax_t;
typedef unsigned long __uintmax_t;
typedef unsigned long __dev_t;
typedef unsigned int __uid_t;
typedef unsigned int __gid_t;
typedef unsigned long __ino_t;
typedef unsigned long __ino64_t;
typedef unsigned int __mode_t;
typedef unsigned long __nlink_t;
typedef long __off_t;
typedef long __off64_t;
typedef int __pid_t;
typedef struct {
    int __val[2];
} __fsid_t;
typedef long __clock_t;
typedef unsigned long __rlim_t;
typedef unsigned long __rlim64_t;
typedef unsigned int __id_t;
typedef long __time_t;
typedef unsigned int __useconds_t;
typedef long __suseconds_t;
typedef int __daddr_t;
typedef int __key_t;
typedef int __clockid_t;
typedef void *__timer_t;
typedef long __blksize_t;
typedef long __blkcnt_t;
typedef long __blkcnt64_t;
typedef unsigned long __fsblkcnt_t;
typedef unsigned long __fsblkcnt64_t;
typedef unsigned long __fsfilcnt_t;
typedef unsigned long __fsfilcnt64_t;
typedef long __fsword_t;
typedef long __ssize_t;
typedef long __syscall_slong_t;
typedef unsigned long __syscall_ulong_t;
typedef __off64_t __loff_t;
typedef char *__caddr_t;
typedef long __intptr_t;
typedef unsigned int __socklen_t;
typedef int __sig_atomic_t;
typedef __int8_t int8_t;
typedef __int16_t int16_t;
typedef __int32_t int32_t;
typedef __int64_t int64_t;
typedef __uint8_t uint8_t;
typedef __uint16_t uint16_t;
typedef __uint32_t uint32_t;
typedef __uint64_t uint64_t;
typedef __int_least8_t int_least8_t;
typedef __int_least16_t int_least16_t;
typedef __int_least32_t int_least32_t;
typedef __int_least64_t int_least64_t;
typedef __uint_least8_t uint_least8_t;
typedef __uint_least16_t uint_least16_t;
typedef __uint_least32_t uint_least32_t;
typedef __uint_least64_t uint_least64_t;
typedef signed char int_fast8_t;
typedef long int_fast16_t;
typedef long int_fast32_t;
typedef long int_fast64_t;
typedef unsigned char uint_fast8_t;
typedef unsigned long uint_fast16_t;
typedef unsigned long uint_fast32_t;
typedef unsigned long uint_fast64_t;
typedef long intptr_t;
typedef unsigned long uintptr_t;
typedef __intmax_t intmax_t;
typedef __uintmax_t uintmax_t;
extern uint64_t revng_init_local_sp(uint64_t param_0);
extern uint64_t init_rbp(void);
extern uint64_t indirect_placeholder_1(uint64_t param_0, uint64_t param_1);
extern void indirect_placeholder(void);
extern void indirect_placeholder_13(uint64_t param_0);
uint64_t bb_main(uint64_t rdi, uint64_t rsi) {
    uint64_t var_0;
    uint64_t var_1;
    uint32_t *var_2;
    uint64_t var_3;
    uint64_t *var_4;
    uint32_t *var_5;
    uint64_t var_6;
    uint64_t var_7;
    uint64_t var_8;
    uint64_t var_9;
    uint64_t var_10;
    uint32_t var_11;
    uint32_t spec_store_select;
    uint32_t var_23;
    uint64_t var_14;
    uint32_t var_15;
    uint64_t local_sp_1;
    uint32_t var_17;
    uint64_t var_16;
    uint64_t local_sp_2;
    uint32_t var_19;
    uint64_t var_18;
    uint64_t local_sp_3;
    uint32_t var_21;
    uint64_t var_20;
    uint64_t local_sp_4;
    uint64_t var_22;
    uint64_t var_12;
    uint32_t var_13;
    uint64_t local_sp_5;
    uint32_t var_25;
    uint64_t var_24;
    uint64_t local_sp_6;
    uint32_t var_27;
    uint64_t var_26;
    uint64_t local_sp_7;
    uint64_t local_sp_8;
    uint64_t var_28;
    var_0 = revng_init_local_sp(0UL);
    var_1 = init_rbp();
    *(uint64_t *)(var_0 + (-8L)) = var_1;
    var_2 = (uint32_t *)(var_0 + (-428L));
    *var_2 = (uint32_t)rdi;
    var_3 = var_0 + (-440L);
    var_4 = (uint64_t *)var_3;
    *var_4 = rsi;
    var_5 = (uint32_t *)(var_0 + (-12L));
    *var_5 = 0U;
    var_6 = **(uint64_t **)var_3;
    *(uint64_t *)(var_0 + (-448L)) = 4202408UL;
    indirect_placeholder_13(var_6);
    *(uint64_t *)(var_0 + (-456L)) = 4202423UL;
    indirect_placeholder();
    *(uint64_t *)(var_0 + (-464L)) = 4202433UL;
    indirect_placeholder();
    var_7 = *var_4;
    var_8 = (uint64_t)*var_2;
    var_9 = var_0 + (-472L);
    *(uint64_t *)var_9 = 4202456UL;
    var_10 = indirect_placeholder_1(var_8, var_7);
    var_11 = (uint32_t)var_10;
    spec_store_select = (var_11 == 0U) ? 1U : var_11;
    *var_5 = spec_store_select;
    var_23 = spec_store_select;
    local_sp_5 = var_9;
    if ((spec_store_select & 31U) != 0U) {
        var_12 = var_0 + (-480L);
        *(uint64_t *)var_12 = 4202501UL;
        indirect_placeholder();
        var_13 = *var_5;
        var_15 = var_13;
        local_sp_1 = var_12;
        if ((var_13 & 1U) == 0U) {
            var_14 = var_0 + (-488L);
            *(uint64_t *)var_14 = 4202560UL;
            indirect_placeholder();
            var_15 = *var_5;
            local_sp_1 = var_14;
        }
        var_17 = var_15;
        local_sp_2 = local_sp_1;
        if ((var_15 & 2U) == 0U) {
            var_16 = local_sp_1 + (-8L);
            *(uint64_t *)var_16 = 4202589UL;
            indirect_placeholder();
            var_17 = *var_5;
            local_sp_2 = var_16;
        }
        var_19 = var_17;
        local_sp_3 = local_sp_2;
        if ((var_17 & 4U) == 0U) {
            var_18 = local_sp_2 + (-8L);
            *(uint64_t *)var_18 = 4202620UL;
            indirect_placeholder();
            var_19 = *var_5;
            local_sp_3 = var_18;
        }
        var_21 = var_19;
        local_sp_4 = local_sp_3;
        if ((var_19 & 8U) == 0U) {
            var_20 = local_sp_3 + (-8L);
            *(uint64_t *)var_20 = 4202651UL;
            indirect_placeholder();
            var_21 = *var_5;
            local_sp_4 = var_20;
        }
        var_23 = var_21;
        local_sp_5 = local_sp_4;
        if ((var_21 & 16U) == 0U) {
            var_22 = local_sp_4 + (-8L);
            *(uint64_t *)var_22 = 4202682UL;
            indirect_placeholder();
            var_23 = *var_5;
            local_sp_5 = var_22;
        }
    }
    var_25 = var_23;
    local_sp_6 = local_sp_5;
    *(uint64_t *)(var_0 + (-24L)) = 4267559UL;
    var_25 = 4294967295U;
    if ((var_23 & 32U) != 0U & *var_5 == 4294967295U) {
        var_24 = local_sp_5 + (-8L);
        *(uint64_t *)var_24 = 4202728UL;
        indirect_placeholder();
        var_25 = *var_5;
        local_sp_6 = var_24;
    }
    var_27 = var_25;
    local_sp_7 = local_sp_6;
    *(uint64_t *)(var_0 + (-32L)) = 4267559UL;
    var_27 = 4294967295U;
    if ((var_25 & 64U) != 0U & *var_5 == 4294967295U) {
        var_26 = local_sp_6 + (-8L);
        *(uint64_t *)var_26 = 4202774UL;
        indirect_placeholder();
        var_27 = *var_5;
        local_sp_7 = var_26;
    }
    local_sp_8 = local_sp_7;
    if ((signed char)(unsigned char)var_27 > '\xff') {
        *(uint64_t *)(local_sp_8 + (-8L)) = 4202806UL;
        indirect_placeholder();
        return 0UL;
    }
    var_28 = local_sp_7 + (-8L);
    *(uint64_t *)var_28 = 4202796UL;
    indirect_placeholder();
    local_sp_8 = var_28;
    *(uint64_t *)(local_sp_8 + (-8L)) = 4202806UL;
    indirect_placeholder();
    return 0UL;
}

typedef unsigned char __u_char;
typedef unsigned short __u_short;
typedef unsigned int __u_int;
typedef unsigned long __u_long;
typedef signed char __int8_t;
typedef unsigned char __uint8_t;
typedef short __int16_t;
typedef unsigned short __uint16_t;
typedef int __int32_t;
typedef unsigned int __uint32_t;
typedef long __int64_t;
typedef unsigned long __uint64_t;
typedef __int8_t __int_least8_t;
typedef __uint8_t __uint_least8_t;
typedef __int16_t __int_least16_t;
typedef __uint16_t __uint_least16_t;
typedef __int32_t __int_least32_t;
typedef __uint32_t __uint_least32_t;
typedef __int64_t __int_least64_t;
typedef __uint64_t __uint_least64_t;
typedef long __quad_t;
typedef unsigned long __u_quad_t;
typedef long __intmax_t;
typedef unsigned long __uintmax_t;
typedef unsigned long __dev_t;
typedef unsigned int __uid_t;
typedef unsigned int __gid_t;
typedef unsigned long __ino_t;
typedef unsigned long __ino64_t;
typedef unsigned int __mode_t;
typedef unsigned long __nlink_t;
typedef long __off_t;
typedef long __off64_t;
typedef int __pid_t;
typedef struct {
    int __val[2];
} __fsid_t;
typedef long __clock_t;
typedef unsigned long __rlim_t;
typedef unsigned long __rlim64_t;
typedef unsigned int __id_t;
typedef long __time_t;
typedef unsigned int __useconds_t;
typedef long __suseconds_t;
typedef int __daddr_t;
typedef int __key_t;
typedef int __clockid_t;
typedef void *__timer_t;
typedef long __blksize_t;
typedef long __blkcnt_t;
typedef long __blkcnt64_t;
typedef unsigned long __fsblkcnt_t;
typedef unsigned long __fsblkcnt64_t;
typedef unsigned long __fsfilcnt_t;
typedef unsigned long __fsfilcnt64_t;
typedef long __fsword_t;
typedef long __ssize_t;
typedef long __syscall_slong_t;
typedef unsigned long __syscall_ulong_t;
typedef __off64_t __loff_t;
typedef char *__caddr_t;
typedef long __intptr_t;
typedef unsigned int __socklen_t;
typedef int __sig_atomic_t;
typedef __int8_t int8_t;
typedef __int16_t int16_t;
typedef __int32_t int32_t;
typedef __int64_t int64_t;
typedef __uint8_t uint8_t;
typedef __uint16_t uint16_t;
typedef __uint32_t uint32_t;
typedef __uint64_t uint64_t;
typedef __int_least8_t int_least8_t;
typedef __int_least16_t int_least16_t;
typedef __int_least32_t int_least32_t;
typedef __int_least64_t int_least64_t;
typedef __uint_least8_t uint_least8_t;
typedef __uint_least16_t uint_least16_t;
typedef __uint_least32_t uint_least32_t;
typedef __uint_least64_t uint_least64_t;
typedef signed char int_fast8_t;
typedef long int_fast16_t;
typedef long int_fast32_t;
typedef long int_fast64_t;
typedef unsigned char uint_fast8_t;
typedef unsigned long uint_fast16_t;
typedef unsigned long uint_fast32_t;
typedef unsigned long uint_fast64_t;
typedef long intptr_t;
typedef unsigned long uintptr_t;
typedef __intmax_t intmax_t;
typedef __uintmax_t uintmax_t;
struct indirect_placeholder_6_ret_type;
struct indirect_placeholder_7_ret_type;
struct indirect_placeholder_8_ret_type;
struct indirect_placeholder_6_ret_type {
    uint64_t field_0;
    uint64_t field_1;
    uint64_t field_2;
};
struct indirect_placeholder_7_ret_type {
    uint64_t field_0;
    uint64_t field_1;
};
struct indirect_placeholder_8_ret_type {
    uint64_t field_0;
    uint64_t field_1;
};
extern void abort(void);
extern uint64_t revng_init_local_sp(uint64_t param_0);
extern uint64_t init_rbp(void);
extern void indirect_placeholder(void);
extern void indirect_placeholder_3(uint64_t param_0, uint64_t param_1);
extern void indirect_placeholder_4(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint64_t param_3, uint64_t param_4, uint64_t param_5, uint64_t param_6);
extern void indirect_placeholder_5(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint64_t param_3, uint64_t param_4, uint64_t param_5);
extern struct indirect_placeholder_6_ret_type indirect_placeholder_6(uint64_t param_0);
extern struct indirect_placeholder_7_ret_type indirect_placeholder_7(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint64_t param_3, uint64_t param_4);
extern struct indirect_placeholder_8_ret_type indirect_placeholder_8(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint64_t param_3, uint64_t param_4);
uint64_t bb_decode_switches(uint64_t rdi, uint64_t rsi) {
    struct indirect_placeholder_8_ret_type var_11;
    struct indirect_placeholder_7_ret_type var_18;
    uint64_t var_0;
    uint64_t var_1;
    uint64_t var_2;
    uint32_t *var_3;
    uint64_t var_4;
    uint64_t *var_5;
    uint32_t *var_6;
    uint64_t local_sp_0;
    uint64_t var_13;
    uint64_t spec_select;
    uint64_t var_14;
    uint64_t local_sp_3;
    uint64_t var_8;
    uint64_t local_sp_1;
    uint64_t var_21;
    uint64_t spec_select99;
    uint64_t var_22;
    uint64_t *var_23;
    uint64_t local_sp_2;
    uint32_t var_24;
    uint64_t var_25;
    struct indirect_placeholder_6_ret_type var_26;
    uint64_t var_27;
    uint64_t var_28;
    uint64_t var_29;
    uint64_t var_15;
    uint64_t var_16;
    uint64_t var_17;
    uint32_t *var_19;
    uint32_t var_20;
    uint32_t *var_7;
    uint64_t var_9;
    uint64_t var_10;
    uint32_t var_12;
    unsigned int loop_state_var;
    var_0 = revng_init_local_sp(0UL);
    var_1 = init_rbp();
    var_2 = var_0 + (-8L);
    *(uint64_t *)var_2 = var_1;
    var_3 = (uint32_t *)(var_0 + (-28L));
    *var_3 = (uint32_t)rdi;
    var_4 = var_0 + (-40L);
    var_5 = (uint64_t *)var_4;
    *var_5 = rsi;
    var_6 = (uint32_t *)(var_0 + (-12L));
    *var_6 = 0U;
    local_sp_3 = var_4;
    if (*(uint32_t *)6374912UL != 2U) {
        var_7 = (uint32_t *)(var_0 + (-16L));
        while (1U)
            {
                var_8 = *var_5;
                var_9 = (uint64_t)*var_3;
                var_10 = local_sp_3 + (-8L);
                *(uint64_t *)var_10 = 4202258UL;
                var_11 = indirect_placeholder_8(4267499UL, 4266048UL, var_9, var_8, 0UL);
                var_12 = (uint32_t)var_11.field_0;
                *var_7 = var_12;
                local_sp_0 = var_10;
                local_sp_2 = var_10;
                local_sp_3 = var_10;
                if (var_12 != 4294967295U) {
                    loop_state_var = 1U;
                    break;
                }
                if ((uint64_t)(var_12 + (-110)) == 0UL) {
                    *var_6 = (*var_6 | 2U);
                    continue;
                }
                if ((int)var_12 > (int)110U) {
                    if ((uint64_t)(var_12 + (-114)) == 0UL) {
                        *var_6 = (*var_6 | 4U);
                        continue;
                    }
                    if ((int)var_12 <= (int)114U) {
                        if ((uint64_t)(var_12 + (-111)) == 0UL) {
                            *var_6 = (*var_6 | 128U);
                            continue;
                        }
                        if ((uint64_t)(var_12 + (-112)) != 0UL) {
                            loop_state_var = 0U;
                            break;
                        }
                        *var_6 = (*var_6 | 32U);
                        continue;
                    }
                    if ((uint64_t)(var_12 + (-115)) == 0UL) {
                        *var_6 = (*var_6 | 1U);
                        continue;
                    }
                    if ((uint64_t)(var_12 + (-118)) != 0UL) {
                        loop_state_var = 0U;
                        break;
                    }
                    *var_6 = (*var_6 | 8U);
                    continue;
                }
                if ((uint64_t)(var_12 + (-97)) == 0UL) {
                    *var_6 = 4294967295U;
                    continue;
                }
                if ((int)var_12 > (int)97U) {
                    if ((uint64_t)(var_12 + (-105)) == 0UL) {
                        *var_6 = (*var_6 | 64U);
                        continue;
                    }
                    if ((uint64_t)(var_12 + (-109)) != 0UL) {
                        loop_state_var = 0U;
                        break;
                    }
                    *var_6 = (*var_6 | 16U);
                    continue;
                }
                if ((uint64_t)(var_12 + 131U) == 0UL) {
                    if ((uint64_t)(var_12 + 130U) != 0UL) {
                        loop_state_var = 0U;
                        break;
                    }
                    *(uint64_t *)(local_sp_3 + (-16L)) = 4202138UL;
                    indirect_placeholder_3(var_2, 0UL);
                    abort();
                }
                var_13 = *(uint64_t *)6374920UL;
                spec_select = (*(uint32_t *)6374912UL == 1U) ? 4267462UL : 4267468UL;
                *(uint64_t *)(local_sp_3 + (-16L)) = 4202208UL;
                indirect_placeholder_5(0UL, 4265688UL, var_13, spec_select, 0UL, 4267483UL);
                var_14 = local_sp_3 + (-24L);
                *(uint64_t *)var_14 = 4202218UL;
                indirect_placeholder();
                local_sp_0 = var_14;
                loop_state_var = 0U;
                break;
            }
        switch (loop_state_var) {
          case 0U:
            {
                *(uint64_t *)(local_sp_0 + (-8L)) = 4202228UL;
                indirect_placeholder_3(var_2, 1UL);
                abort();
            }
            break;
          case 1U:
            {
                break;
            }
            break;
        }
    }
    var_15 = *var_5;
    var_16 = (uint64_t)*var_3;
    var_17 = var_0 + (-48L);
    *(uint64_t *)var_17 = 4201798UL;
    var_18 = indirect_placeholder_7(4265851UL, 4266496UL, var_16, var_15, 0UL);
    var_19 = (uint32_t *)(var_0 + (-16L));
    var_20 = (uint32_t)var_18.field_0;
    *var_19 = var_20;
    local_sp_1 = var_17;
    local_sp_2 = var_17;
    if (var_20 != 4294967295U) {
        if ((uint64_t)(var_20 + 131U) != 0UL) {
            if ((uint64_t)(var_20 + 130U) != 0UL) {
                *(uint64_t *)(var_0 + (-56L)) = 4201834UL;
                indirect_placeholder_3(var_2, 0UL);
                abort();
            }
        }
        var_21 = *(uint64_t *)6374920UL;
        spec_select99 = (*(uint32_t *)6374912UL == 1U) ? 4267462UL : 4267468UL;
        var_22 = var_0 + (-64L);
        var_23 = (uint64_t *)var_22;
        *var_23 = 0UL;
        *(uint64_t *)(var_0 + (-72L)) = 4201910UL;
        indirect_placeholder_5(0UL, 4265688UL, var_21, spec_select99, 4267473UL, 4267483UL);
        *var_23 = 4201924UL;
        indirect_placeholder();
        local_sp_1 = var_22;
    }
    *var_6 = 16U;
}

typedef unsigned char __u_char;
typedef unsigned short __u_short;
typedef unsigned int __u_int;
typedef unsigned long __u_long;
typedef signed char __int8_t;
typedef unsigned char __uint8_t;
typedef short __int16_t;
typedef unsigned short __uint16_t;
typedef int __int32_t;
typedef unsigned int __uint32_t;
typedef long __int64_t;
typedef unsigned long __uint64_t;
typedef __int8_t __int_least8_t;
typedef __uint8_t __uint_least8_t;
typedef __int16_t __int_least16_t;
typedef __uint16_t __uint_least16_t;
typedef __int32_t __int_least32_t;
typedef __uint32_t __uint_least32_t;
typedef __int64_t __int_least64_t;
typedef __uint64_t __uint_least64_t;
typedef long __quad_t;
typedef unsigned long __u_quad_t;
typedef long __intmax_t;
typedef unsigned long __uintmax_t;
typedef unsigned long __dev_t;
typedef unsigned int __uid_t;
typedef unsigned int __gid_t;
typedef unsigned long __ino_t;
typedef unsigned long __ino64_t;
typedef unsigned int __mode_t;
typedef unsigned long __nlink_t;
typedef long __off_t;
typedef long __off64_t;
typedef int __pid_t;
typedef struct {
    int __val[2];
} __fsid_t;
typedef long __clock_t;
typedef unsigned long __rlim_t;
typedef unsigned long __rlim64_t;
typedef unsigned int __id_t;
typedef long __time_t;
typedef unsigned int __useconds_t;
typedef long __suseconds_t;
typedef int __daddr_t;
typedef int __key_t;
typedef int __clockid_t;
typedef void *__timer_t;
typedef long __blksize_t;
typedef long __blkcnt_t;
typedef long __blkcnt64_t;
typedef unsigned long __fsblkcnt_t;
typedef unsigned long __fsblkcnt64_t;
typedef unsigned long __fsfilcnt_t;
typedef unsigned long __fsfilcnt64_t;
typedef long __fsword_t;
typedef long __ssize_t;
typedef long __syscall_slong_t;
typedef unsigned long __syscall_ulong_t;
typedef __off64_t __loff_t;
typedef char *__caddr_t;
typedef long __intptr_t;
typedef unsigned int __socklen_t;
typedef int __sig_atomic_t;
typedef __int8_t int8_t;
typedef __int16_t int16_t;
typedef __int32_t int32_t;
typedef __int64_t int64_t;
typedef __uint8_t uint8_t;
typedef __uint16_t uint16_t;
typedef __uint32_t uint32_t;
typedef __uint64_t uint64_t;
typedef __int_least8_t int_least8_t;
typedef __int_least16_t int_least16_t;
typedef __int_least32_t int_least32_t;
typedef __int_least64_t int_least64_t;
typedef __uint_least8_t uint_least8_t;
typedef __uint_least16_t uint_least16_t;
typedef __uint_least32_t uint_least32_t;
typedef __uint_least64_t uint_least64_t;
typedef signed char int_fast8_t;
typedef long int_fast16_t;
typedef long int_fast32_t;
typedef long int_fast64_t;
typedef unsigned char uint_fast8_t;
typedef unsigned long uint_fast16_t;
typedef unsigned long uint_fast32_t;
typedef unsigned long uint_fast64_t;
typedef long intptr_t;
typedef unsigned long uintptr_t;
typedef __intmax_t intmax_t;
typedef __uintmax_t uintmax_t;
extern void abort(void);
extern uint64_t revng_init_local_sp(uint64_t param_0);
extern uint64_t init_rbp(void);
extern void indirect_placeholder(void);
uint64_t bb_savewd_restore(uint64_t rsi, uint64_t rdi) {
    uint64_t var_0;
    uint64_t var_1;
    uint64_t var_2;
    uint64_t var_3;
    uint64_t *var_4;
    uint32_t **var_5;
    uint32_t var_6;
    uint32_t *var_12;
    uint32_t var_13;
    uint32_t var_14;
    uint64_t var_15;
    uint64_t rax_0;
    uint64_t var_16;
    uint64_t var_17;
    uint64_t local_sp_1_be;
    uint64_t local_sp_0;
    uint64_t var_18;
    uint32_t var_11;
    uint64_t var_20;
    uint32_t var_21;
    uint32_t *var_22;
    uint64_t var_23;
    uint64_t var_24;
    uint64_t local_sp_2;
    uint32_t var_7;
    uint32_t *var_8;
    uint32_t var_10;
    uint64_t var_9;
    uint64_t local_sp_1;
    uint64_t rax_1;
    uint32_t var_19;
    var_0 = revng_init_local_sp(0UL);
    var_1 = init_rbp();
    *(uint64_t *)(var_0 + (-8L)) = var_1;
    var_2 = var_0 + (-40L);
    var_3 = var_0 + (-32L);
    var_4 = (uint64_t *)var_3;
    *var_4 = rdi;
    *(uint32_t *)(var_0 + (-36L)) = (uint32_t)rsi;
    var_5 = (uint32_t **)var_3;
    var_6 = **var_5;
    local_sp_0 = var_2;
    rax_0 = 0UL;
    local_sp_2 = var_2;
    rax_1 = 4214010UL;
    if (var_6 > 4U) {
        *(uint64_t *)(var_0 + (-48L)) = 4214211UL;
        indirect_placeholder();
    } else {
        switch (*(uint64_t *)(((uint64_t)var_6 << 3UL) + 4280112UL)) {
          case 4214010UL:
            {
                *(uint64_t *)(local_sp_2 + (-8L)) = 4214015UL;
                indirect_placeholder();
                *(uint32_t *)rax_1 = *(uint32_t *)(*var_4 + 4UL);
                rax_0 = 4294967295UL;
            }
            break;
          case 4214213UL:
            {
                break;
            }
            break;
          case 4213933UL:
            {
                var_19 = *(uint32_t *)(*var_4 + 4UL);
                *(uint64_t *)(var_0 + (-48L)) = 4213947UL;
                indirect_placeholder();
                if (var_19 != 0U) {
                    **var_5 = 1U;
                    return rax_0;
                }
                var_20 = (uint64_t)var_19;
                *(uint64_t *)(var_0 + (-56L)) = 4213971UL;
                indirect_placeholder();
                var_21 = *(uint32_t *)var_20;
                var_22 = (uint32_t *)(var_0 + (-12L));
                *var_22 = var_21;
                var_23 = var_0 + (-64L);
                *(uint64_t *)var_23 = 4213990UL;
                indirect_placeholder();
                **var_5 = 4U;
                var_24 = *var_4;
                *(uint32_t *)(var_24 + 4UL) = *var_22;
                local_sp_2 = var_23;
                rax_1 = var_24;
                *(uint64_t *)(local_sp_2 + (-8L)) = 4214015UL;
                indirect_placeholder();
                *(uint32_t *)rax_1 = *(uint32_t *)(*var_4 + 4UL);
                rax_0 = 4294967295UL;
            }
            break;
          case 4214037UL:
            {
                var_7 = *(uint32_t *)(*var_4 + 4UL);
                var_8 = (uint32_t *)(var_0 + (-16L));
                *var_8 = var_7;
                var_10 = var_7;
                if (var_7 == 0U) {
                    var_9 = var_0 + (-48L);
                    *(uint64_t *)var_9 = 4214063UL;
                    indirect_placeholder();
                    var_10 = *var_8;
                    local_sp_0 = var_9;
                }
                var_11 = var_10;
                local_sp_1 = local_sp_0;
                if ((int)var_10 <= (int)0U) {
                    *(uint64_t *)(local_sp_1 + (-8L)) = 4214133UL;
                    indirect_placeholder();
                    while ((int)var_11 >= (int)0U)
                        {
                            var_16 = (uint64_t)var_11;
                            var_17 = local_sp_1 + (-16L);
                            *(uint64_t *)var_17 = 4214076UL;
                            indirect_placeholder();
                            local_sp_1_be = var_17;
                            if (*(uint32_t *)var_16 == 4U) {
                                var_18 = local_sp_1 + (-24L);
                                *(uint64_t *)var_18 = 4214108UL;
                                indirect_placeholder();
                                local_sp_1_be = var_18;
                            }
                            var_11 = *var_8;
                            local_sp_1 = local_sp_1_be;
                            *(uint64_t *)(local_sp_1 + (-8L)) = 4214133UL;
                            indirect_placeholder();
                        }
                    *(uint32_t *)(*var_4 + 4UL) = 4294967295U;
                    var_12 = (uint32_t *)(var_0 + (-20L));
                    var_13 = *var_12;
                    var_14 = var_13;
                    if ((var_13 & 127U) != 0U) {
                        *(uint64_t *)(local_sp_1 + (-16L)) = 4214171UL;
                        indirect_placeholder();
                        var_14 = *var_12;
                    }
                    var_15 = (uint64_t)(uint32_t)(unsigned char)(var_14 >> 8U);
                    rax_0 = var_15;
                }
            }
            break;
          default:
            {
                abort();
            }
            break;
        }
    }
}

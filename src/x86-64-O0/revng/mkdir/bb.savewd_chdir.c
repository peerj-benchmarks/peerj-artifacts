typedef unsigned char __u_char;
typedef unsigned short __u_short;
typedef unsigned int __u_int;
typedef unsigned long __u_long;
typedef signed char __int8_t;
typedef unsigned char __uint8_t;
typedef short __int16_t;
typedef unsigned short __uint16_t;
typedef int __int32_t;
typedef unsigned int __uint32_t;
typedef long __int64_t;
typedef unsigned long __uint64_t;
typedef __int8_t __int_least8_t;
typedef __uint8_t __uint_least8_t;
typedef __int16_t __int_least16_t;
typedef __uint16_t __uint_least16_t;
typedef __int32_t __int_least32_t;
typedef __uint32_t __uint_least32_t;
typedef __int64_t __int_least64_t;
typedef __uint64_t __uint_least64_t;
typedef long __quad_t;
typedef unsigned long __u_quad_t;
typedef long __intmax_t;
typedef unsigned long __uintmax_t;
typedef unsigned long __dev_t;
typedef unsigned int __uid_t;
typedef unsigned int __gid_t;
typedef unsigned long __ino_t;
typedef unsigned long __ino64_t;
typedef unsigned int __mode_t;
typedef unsigned long __nlink_t;
typedef long __off_t;
typedef long __off64_t;
typedef int __pid_t;
typedef struct {
    int __val[2];
} __fsid_t;
typedef long __clock_t;
typedef unsigned long __rlim_t;
typedef unsigned long __rlim64_t;
typedef unsigned int __id_t;
typedef long __time_t;
typedef unsigned int __useconds_t;
typedef long __suseconds_t;
typedef int __daddr_t;
typedef int __key_t;
typedef int __clockid_t;
typedef void *__timer_t;
typedef long __blksize_t;
typedef long __blkcnt_t;
typedef long __blkcnt64_t;
typedef unsigned long __fsblkcnt_t;
typedef unsigned long __fsblkcnt64_t;
typedef unsigned long __fsfilcnt_t;
typedef unsigned long __fsfilcnt64_t;
typedef long __fsword_t;
typedef long __ssize_t;
typedef long __syscall_slong_t;
typedef unsigned long __syscall_ulong_t;
typedef __off64_t __loff_t;
typedef char *__caddr_t;
typedef long __intptr_t;
typedef unsigned int __socklen_t;
typedef int __sig_atomic_t;
typedef __int8_t int8_t;
typedef __int16_t int16_t;
typedef __int32_t int32_t;
typedef __int64_t int64_t;
typedef __uint8_t uint8_t;
typedef __uint16_t uint16_t;
typedef __uint32_t uint32_t;
typedef __uint64_t uint64_t;
typedef __int_least8_t int_least8_t;
typedef __int_least16_t int_least16_t;
typedef __int_least32_t int_least32_t;
typedef __int_least64_t int_least64_t;
typedef __uint_least8_t uint_least8_t;
typedef __uint_least16_t uint_least16_t;
typedef __uint_least32_t uint_least32_t;
typedef __uint_least64_t uint_least64_t;
typedef signed char int_fast8_t;
typedef long int_fast16_t;
typedef long int_fast32_t;
typedef long int_fast64_t;
typedef unsigned char uint_fast8_t;
typedef unsigned long uint_fast16_t;
typedef unsigned long uint_fast32_t;
typedef unsigned long uint_fast64_t;
typedef long intptr_t;
typedef unsigned long uintptr_t;
typedef __intmax_t intmax_t;
typedef __uintmax_t uintmax_t;
extern void abort(void);
extern uint64_t revng_init_local_sp(uint64_t param_0);
extern uint64_t init_rbp(void);
extern void indirect_placeholder(void);
extern uint64_t indirect_placeholder_4(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint64_t param_3, uint64_t param_4);
extern uint64_t init_rbx(void);
extern uint64_t init_rax(void);
uint64_t bb_savewd_chdir(uint64_t rdx, uint64_t rcx, uint64_t rsi, uint64_t rdi, uint64_t r9, uint64_t r8) {
    uint32_t var_33;
    uint64_t var_34;
    uint64_t var_26;
    uint64_t var_0;
    uint64_t var_1;
    uint64_t var_2;
    uint64_t var_3;
    uint64_t var_4;
    uint64_t var_5;
    uint64_t *var_6;
    uint64_t *var_7;
    uint32_t *var_8;
    uint64_t *var_9;
    uint32_t *var_10;
    uint32_t *var_11;
    uint64_t rdx1_1;
    uint64_t rax_3;
    uint64_t rax_0;
    uint64_t local_sp_0;
    uint32_t var_32;
    uint64_t rax_4;
    uint64_t rax_1;
    uint32_t var_35;
    uint64_t var_36;
    uint64_t var_37;
    uint64_t var_38;
    uint64_t var_39;
    uint32_t var_27;
    uint64_t var_28;
    uint64_t var_29;
    uint64_t var_30;
    uint64_t var_31;
    uint64_t var_13;
    uint64_t var_14;
    uint64_t var_15;
    uint32_t var_16;
    uint64_t var_17;
    uint32_t var_18;
    uint64_t rdx1_0;
    uint64_t rax_2;
    uint64_t local_sp_1;
    uint64_t var_19;
    uint32_t var_20;
    uint64_t var_21;
    uint64_t spec_select;
    uint64_t var_12;
    uint64_t local_sp_2;
    uint64_t local_sp_3;
    uint32_t var_22;
    uint64_t var_23;
    uint64_t var_24;
    uint64_t var_25;
    uint32_t var_40;
    uint32_t *var_41;
    uint64_t var_42;
    var_0 = revng_init_local_sp(0UL);
    var_1 = init_rax();
    var_2 = init_rbp();
    var_3 = init_rbx();
    *(uint64_t *)(var_0 + (-8L)) = var_2;
    *(uint64_t *)(var_0 + (-16L)) = var_3;
    var_4 = var_0 + (-72L);
    var_5 = var_0 + (-48L);
    var_6 = (uint64_t *)var_5;
    *var_6 = rdi;
    var_7 = (uint64_t *)(var_0 + (-56L));
    *var_7 = rsi;
    var_8 = (uint32_t *)(var_0 + (-60L));
    *var_8 = (uint32_t)rdx;
    var_9 = (uint64_t *)var_4;
    *var_9 = rcx;
    var_10 = (uint32_t *)(var_0 + (-28L));
    *var_10 = 4294967295U;
    var_11 = (uint32_t *)(var_0 + (-32L));
    *var_11 = 0U;
    rdx1_1 = rdx;
    rax_3 = var_1;
    var_18 = 0U;
    rax_2 = 0UL;
    local_sp_2 = var_4;
    if (*var_9 != 0UL) {
        spec_select = ((*var_8 & 1U) == 0U) ? 2164992UL : 2296064UL;
        var_12 = var_0 + (-80L);
        *(uint64_t *)var_12 = 4213566UL;
        indirect_placeholder();
        *var_10 = 0U;
        rdx1_0 = spec_select;
        local_sp_1 = var_12;
        if (*var_9 == 0UL) {
            **(uint32_t **)var_4 = 0U;
            var_13 = *var_9;
            var_14 = var_13 + 4UL;
            var_15 = var_0 + (-88L);
            *(uint64_t *)var_15 = 4213598UL;
            indirect_placeholder();
            var_16 = *(uint32_t *)var_13;
            var_17 = (uint64_t)var_16;
            *(uint32_t *)var_14 = var_16;
            var_18 = *var_10;
            rdx1_0 = 0UL;
            rax_2 = var_17;
            local_sp_1 = var_15;
        }
        rdx1_1 = rdx1_0;
        rax_3 = rax_2;
        local_sp_2 = local_sp_1;
        var_19 = local_sp_1 + (-8L);
        *(uint64_t *)var_19 = 4213613UL;
        indirect_placeholder();
        var_20 = *(uint32_t *)rax_2;
        var_21 = (uint64_t)var_20;
        rax_3 = var_21;
        local_sp_2 = var_19;
        if ((int)var_18 <= (int)4294967295U & (uint64_t)(var_20 + (-13)) == 0UL) {
            *var_11 = 4294967295U;
        }
    }
    rax_4 = rax_3;
    local_sp_3 = local_sp_2;
    if (*var_11 == 0U) {
        if ((int)*var_10 >= (int)0U & *var_9 == 0UL) {
            *(uint64_t *)(local_sp_3 + (-8L)) = 4213853UL;
            indirect_placeholder();
            var_40 = *(uint32_t *)rax_4;
            var_41 = (uint32_t *)(var_0 + (-36L));
            *var_41 = var_40;
            var_42 = (uint64_t)*var_10;
            *(uint64_t *)(local_sp_3 + (-16L)) = 4213868UL;
            indirect_placeholder();
            *(uint64_t *)(local_sp_3 + (-24L)) = 4213873UL;
            indirect_placeholder();
            *(uint32_t *)var_42 = *var_41;
        }
        return (uint64_t)*var_11;
    }
    if ((int)*var_10 >= (int)0U) {
        var_22 = *var_8 & 2U;
        var_23 = (uint64_t)var_22;
        rax_4 = var_23;
        if (var_22 != 0U) {
            if ((int)*var_10 >= (int)0U & *var_9 == 0UL) {
                *(uint64_t *)(local_sp_3 + (-8L)) = 4213853UL;
                indirect_placeholder();
                var_40 = *(uint32_t *)rax_4;
                var_41 = (uint32_t *)(var_0 + (-36L));
                *var_41 = var_40;
                var_42 = (uint64_t)*var_10;
                *(uint64_t *)(local_sp_3 + (-16L)) = 4213868UL;
                indirect_placeholder();
                *(uint64_t *)(local_sp_3 + (-24L)) = 4213873UL;
                indirect_placeholder();
                *(uint32_t *)var_42 = *var_41;
            }
            return (uint64_t)*var_11;
        }
    }
    var_24 = *var_6;
    var_25 = local_sp_2 + (-8L);
    *(uint64_t *)var_25 = 4213669UL;
    var_26 = indirect_placeholder_4(rdx1_1, rcx, var_24, r9, r8);
    rax_4 = var_26;
    local_sp_3 = var_25;
    if ((uint64_t)(unsigned char)var_26 == 0UL) {
        var_27 = *var_10;
        if ((int)var_27 > (int)4294967295U) {
            var_30 = (uint64_t)var_27;
            var_31 = local_sp_2 + (-16L);
            *(uint64_t *)var_31 = 4213723UL;
            indirect_placeholder();
            rax_0 = var_30;
            local_sp_0 = var_31;
        } else {
            var_28 = *var_7;
            var_29 = local_sp_2 + (-16L);
            *(uint64_t *)var_29 = 4213711UL;
            indirect_placeholder();
            rax_0 = var_28;
            local_sp_0 = var_29;
        }
        var_32 = (uint32_t)rax_0;
        *var_11 = var_32;
        rax_4 = rax_0;
        local_sp_3 = local_sp_0;
        if (var_32 != 0U) {
            var_33 = **(uint32_t **)var_5;
            var_34 = (uint64_t)var_33;
            rax_1 = var_34;
            rax_4 = 4213834UL;
            if (var_33 > 5U) {
                rax_1 = 4213807UL;
                switch (*(uint64_t *)((var_34 << 3UL) + 4280032UL)) {
                  case 4213755UL:
                    {
                        var_38 = *var_6;
                        *(uint32_t *)var_38 = 2U;
                        rax_4 = var_38;
                    }
                    break;
                  case 4213807UL:
                    {
                        var_39 = local_sp_0 + (-8L);
                        *(uint64_t *)var_39 = 4213832UL;
                        indirect_placeholder();
                        rax_4 = rax_1;
                        local_sp_3 = var_39;
                    }
                    break;
                  case 4213834UL:
                    {
                        break;
                    }
                    break;
                  case 4213767UL:
                    {
                        var_35 = *(uint32_t *)(*var_6 + 4UL);
                        var_36 = (uint64_t)var_35;
                        rax_4 = var_36;
                        if (var_35 == 0U) {
                            var_37 = local_sp_0 + (-8L);
                            *(uint64_t *)var_37 = 4213803UL;
                            indirect_placeholder();
                            local_sp_3 = var_37;
                        }
                    }
                    break;
                  default:
                    {
                        abort();
                    }
                    break;
                }
            } else {
                var_39 = local_sp_0 + (-8L);
                *(uint64_t *)var_39 = 4213832UL;
                indirect_placeholder();
                rax_4 = rax_1;
                local_sp_3 = var_39;
            }
        }
    } else {
        *var_9 = 0UL;
        *var_11 = 4294967294U;
    }
}

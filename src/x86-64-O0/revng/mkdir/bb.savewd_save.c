typedef unsigned char __u_char;
typedef unsigned short __u_short;
typedef unsigned int __u_int;
typedef unsigned long __u_long;
typedef signed char __int8_t;
typedef unsigned char __uint8_t;
typedef short __int16_t;
typedef unsigned short __uint16_t;
typedef int __int32_t;
typedef unsigned int __uint32_t;
typedef long __int64_t;
typedef unsigned long __uint64_t;
typedef __int8_t __int_least8_t;
typedef __uint8_t __uint_least8_t;
typedef __int16_t __int_least16_t;
typedef __uint16_t __uint_least16_t;
typedef __int32_t __int_least32_t;
typedef __uint32_t __uint_least32_t;
typedef __int64_t __int_least64_t;
typedef __uint64_t __uint_least64_t;
typedef long __quad_t;
typedef unsigned long __u_quad_t;
typedef long __intmax_t;
typedef unsigned long __uintmax_t;
typedef unsigned long __dev_t;
typedef unsigned int __uid_t;
typedef unsigned int __gid_t;
typedef unsigned long __ino_t;
typedef unsigned long __ino64_t;
typedef unsigned int __mode_t;
typedef unsigned long __nlink_t;
typedef long __off_t;
typedef long __off64_t;
typedef int __pid_t;
typedef struct {
    int __val[2];
} __fsid_t;
typedef long __clock_t;
typedef unsigned long __rlim_t;
typedef unsigned long __rlim64_t;
typedef unsigned int __id_t;
typedef long __time_t;
typedef unsigned int __useconds_t;
typedef long __suseconds_t;
typedef int __daddr_t;
typedef int __key_t;
typedef int __clockid_t;
typedef void *__timer_t;
typedef long __blksize_t;
typedef long __blkcnt_t;
typedef long __blkcnt64_t;
typedef unsigned long __fsblkcnt_t;
typedef unsigned long __fsblkcnt64_t;
typedef unsigned long __fsfilcnt_t;
typedef unsigned long __fsfilcnt64_t;
typedef long __fsword_t;
typedef long __ssize_t;
typedef long __syscall_slong_t;
typedef unsigned long __syscall_ulong_t;
typedef __off64_t __loff_t;
typedef char *__caddr_t;
typedef long __intptr_t;
typedef unsigned int __socklen_t;
typedef int __sig_atomic_t;
typedef __int8_t int8_t;
typedef __int16_t int16_t;
typedef __int32_t int32_t;
typedef __int64_t int64_t;
typedef __uint8_t uint8_t;
typedef __uint16_t uint16_t;
typedef __uint32_t uint32_t;
typedef __uint64_t uint64_t;
typedef __int_least8_t int_least8_t;
typedef __int_least16_t int_least16_t;
typedef __int_least32_t int_least32_t;
typedef __int_least64_t int_least64_t;
typedef __uint_least8_t uint_least8_t;
typedef __uint_least16_t uint_least16_t;
typedef __uint_least32_t uint_least32_t;
typedef __uint_least64_t uint_least64_t;
typedef signed char int_fast8_t;
typedef long int_fast16_t;
typedef long int_fast32_t;
typedef long int_fast64_t;
typedef unsigned char uint_fast8_t;
typedef unsigned long uint_fast16_t;
typedef unsigned long uint_fast32_t;
typedef unsigned long uint_fast64_t;
typedef long intptr_t;
typedef unsigned long uintptr_t;
typedef __intmax_t intmax_t;
typedef __uintmax_t uintmax_t;
extern void abort(void);
extern uint64_t revng_init_local_sp(uint64_t param_0);
extern uint64_t init_rbp(void);
extern void indirect_placeholder(void);
extern uint64_t indirect_placeholder_22(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint64_t param_3, uint64_t param_4, uint64_t param_5, uint64_t param_6);
extern uint64_t helper_cc_compute_all_wrapper(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint32_t param_3);
uint64_t bb_savewd_save(uint64_t rdx, uint64_t rcx, uint64_t rdi, uint64_t r9, uint64_t r8) {
    uint64_t var_0;
    uint64_t var_1;
    uint64_t var_2;
    uint64_t *var_3;
    uint32_t **var_4;
    uint32_t var_5;
    uint64_t var_17;
    uint64_t storemerge;
    uint32_t *_cast1;
    uint32_t *_cast3;
    uint64_t var_13;
    uint64_t local_sp_0;
    uint64_t _pre;
    uint64_t var_14;
    uint64_t var_10;
    uint32_t var_11;
    uint64_t var_12;
    uint64_t var_6;
    uint64_t local_sp_1;
    uint32_t var_15;
    uint32_t var_16;
    uint64_t var_7;
    uint32_t *var_8;
    uint32_t var_9;
    var_0 = revng_init_local_sp(0UL);
    var_1 = init_rbp();
    *(uint64_t *)(var_0 + (-8L)) = var_1;
    var_2 = var_0 + (-32L);
    var_3 = (uint64_t *)var_2;
    *var_3 = rdi;
    var_4 = (uint32_t **)var_2;
    var_5 = **var_4;
    var_14 = rdi;
    storemerge = 0UL;
    if (var_5 > 5U) {
        *(uint64_t *)(var_0 + (-48L)) = 4213470UL;
        indirect_placeholder();
    } else {
        var_6 = var_0 + (-40L);
        local_sp_1 = var_6;
        switch (*(uint64_t *)(((uint64_t)var_5 << 3UL) + 4279960UL)) {
          case 4213472UL:
            {
                break;
            }
            break;
          case 4213235UL:
            {
                *(uint64_t *)(var_0 + (-48L)) = 4213255UL;
                var_7 = indirect_placeholder_22(0UL, rdx, rcx, 2097152UL, 4279936UL, r9, r8);
                var_8 = (uint32_t *)(var_0 + (-12L));
                var_9 = (uint32_t)var_7;
                *var_8 = var_9;
                if ((int)var_9 >= (int)0U) {
                    **var_4 = 1U;
                    *(uint32_t *)(*var_3 + 4UL) = *var_8;
                    return storemerge;
                }
                var_10 = var_0 + (-56L);
                *(uint64_t *)var_10 = 4213294UL;
                indirect_placeholder();
                var_11 = *(uint32_t *)var_7;
                var_12 = (uint64_t)var_11;
                local_sp_0 = var_10;
                if ((uint64_t)(var_11 + (-13)) != 0UL) {
                    var_13 = var_0 + (-64L);
                    *(uint64_t *)var_13 = 4213306UL;
                    indirect_placeholder();
                    local_sp_0 = var_13;
                    if (*(uint32_t *)var_12 != 116U) {
                        _cast3 = (uint32_t *)*var_3;
                        *_cast3 = 4U;
                        *(uint64_t *)(var_0 + (-72L)) = 4213328UL;
                        indirect_placeholder();
                        *(uint32_t *)(*var_3 + 4UL) = *_cast3;
                        return storemerge;
                    }
                }
                **var_4 = 3U;
                *(uint32_t *)(*var_3 + 4UL) = 4294967295U;
                _pre = *var_3;
                var_14 = _pre;
                local_sp_1 = local_sp_0;
                var_15 = *(uint32_t *)(var_14 + 4UL);
                *(uint64_t *)(local_sp_1 + (-8L)) = 4213379UL;
                indirect_placeholder();
                *(uint32_t *)(*var_3 + 4UL) = var_15;
                var_16 = *(uint32_t *)(*var_3 + 4UL);
                var_17 = helper_cc_compute_all_wrapper((uint64_t)var_16, 0UL, 0UL, 24U);
                storemerge = 1UL;
                if ((int)var_15 <= (int)4294967295U & var_16 != 0U & (uint64_t)(((unsigned char)(var_17 >> 4UL) ^ (unsigned char)var_17) & '\xc0') == 0UL) {
                    _cast1 = (uint32_t *)*var_3;
                    *_cast1 = 4U;
                    *(uint64_t *)(local_sp_1 + (-16L)) = 4213432UL;
                    indirect_placeholder();
                    *(uint32_t *)(*var_3 + 4UL) = *_cast1;
                    storemerge = 0UL;
                }
            }
            break;
          case 4213363UL:
            {
                var_15 = *(uint32_t *)(var_14 + 4UL);
                *(uint64_t *)(local_sp_1 + (-8L)) = 4213379UL;
                indirect_placeholder();
                *(uint32_t *)(*var_3 + 4UL) = var_15;
                var_16 = *(uint32_t *)(*var_3 + 4UL);
                var_17 = helper_cc_compute_all_wrapper((uint64_t)var_16, 0UL, 0UL, 24U);
                storemerge = 1UL;
                if ((int)var_15 <= (int)4294967295U & var_16 != 0U & (uint64_t)(((unsigned char)(var_17 >> 4UL) ^ (unsigned char)var_17) & '\xc0') == 0UL) {
                    _cast1 = (uint32_t *)*var_3;
                    *_cast1 = 4U;
                    *(uint64_t *)(local_sp_1 + (-16L)) = 4213432UL;
                    indirect_placeholder();
                    *(uint32_t *)(*var_3 + 4UL) = *_cast1;
                    storemerge = 0UL;
                }
            }
            break;
          default:
            {
                abort();
            }
            break;
        }
    }
}

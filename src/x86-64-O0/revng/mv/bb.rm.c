typedef unsigned char __u_char;
typedef unsigned short __u_short;
typedef unsigned int __u_int;
typedef unsigned long __u_long;
typedef signed char __int8_t;
typedef unsigned char __uint8_t;
typedef short __int16_t;
typedef unsigned short __uint16_t;
typedef int __int32_t;
typedef unsigned int __uint32_t;
typedef long __int64_t;
typedef unsigned long __uint64_t;
typedef __int8_t __int_least8_t;
typedef __uint8_t __uint_least8_t;
typedef __int16_t __int_least16_t;
typedef __uint16_t __uint_least16_t;
typedef __int32_t __int_least32_t;
typedef __uint32_t __uint_least32_t;
typedef __int64_t __int_least64_t;
typedef __uint64_t __uint_least64_t;
typedef long __quad_t;
typedef unsigned long __u_quad_t;
typedef long __intmax_t;
typedef unsigned long __uintmax_t;
typedef unsigned long __dev_t;
typedef unsigned int __uid_t;
typedef unsigned int __gid_t;
typedef unsigned long __ino_t;
typedef unsigned long __ino64_t;
typedef unsigned int __mode_t;
typedef unsigned long __nlink_t;
typedef long __off_t;
typedef long __off64_t;
typedef int __pid_t;
typedef struct {
    int __val[2];
} __fsid_t;
typedef long __clock_t;
typedef unsigned long __rlim_t;
typedef unsigned long __rlim64_t;
typedef unsigned int __id_t;
typedef long __time_t;
typedef unsigned int __useconds_t;
typedef long __suseconds_t;
typedef int __daddr_t;
typedef int __key_t;
typedef int __clockid_t;
typedef void *__timer_t;
typedef long __blksize_t;
typedef long __blkcnt_t;
typedef long __blkcnt64_t;
typedef unsigned long __fsblkcnt_t;
typedef unsigned long __fsblkcnt64_t;
typedef unsigned long __fsfilcnt_t;
typedef unsigned long __fsfilcnt64_t;
typedef long __fsword_t;
typedef long __ssize_t;
typedef long __syscall_slong_t;
typedef unsigned long __syscall_ulong_t;
typedef __off64_t __loff_t;
typedef char *__caddr_t;
typedef long __intptr_t;
typedef unsigned int __socklen_t;
typedef int __sig_atomic_t;
typedef __int8_t int8_t;
typedef __int16_t int16_t;
typedef __int32_t int32_t;
typedef __int64_t int64_t;
typedef __uint8_t uint8_t;
typedef __uint16_t uint16_t;
typedef __uint32_t uint32_t;
typedef __uint64_t uint64_t;
typedef __int_least8_t int_least8_t;
typedef __int_least16_t int_least16_t;
typedef __int_least32_t int_least32_t;
typedef __int_least64_t int_least64_t;
typedef __uint_least8_t uint_least8_t;
typedef __uint_least16_t uint_least16_t;
typedef __uint_least32_t uint_least32_t;
typedef __uint_least64_t uint_least64_t;
typedef signed char int_fast8_t;
typedef long int_fast16_t;
typedef long int_fast32_t;
typedef long int_fast64_t;
typedef unsigned char uint_fast8_t;
typedef unsigned long uint_fast16_t;
typedef unsigned long uint_fast32_t;
typedef unsigned long uint_fast64_t;
typedef long intptr_t;
typedef unsigned long uintptr_t;
typedef __intmax_t intmax_t;
typedef __uintmax_t uintmax_t;
struct indirect_placeholder_210_ret_type;
struct indirect_placeholder_211_ret_type;
struct indirect_placeholder_212_ret_type;
struct indirect_placeholder_213_ret_type;
struct indirect_placeholder_210_ret_type {
    uint64_t field_0;
    uint64_t field_1;
    uint64_t field_2;
};
struct indirect_placeholder_211_ret_type {
    uint64_t field_0;
    uint64_t field_1;
    uint64_t field_2;
};
struct indirect_placeholder_212_ret_type {
    uint64_t field_0;
    uint64_t field_1;
    uint64_t field_2;
};
struct indirect_placeholder_213_ret_type {
    uint64_t field_0;
    uint64_t field_1;
    uint64_t field_2;
};
extern uint64_t revng_init_local_sp(uint64_t param_0);
extern uint64_t init_rbp(void);
extern void indirect_placeholder_1(void);
extern uint64_t indirect_placeholder_11(uint64_t param_0, uint64_t param_1, uint64_t param_2);
extern uint64_t indirect_placeholder_12(void);
extern struct indirect_placeholder_210_ret_type indirect_placeholder_210(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint64_t param_3, uint64_t param_4, uint64_t param_5, uint64_t param_6);
extern struct indirect_placeholder_211_ret_type indirect_placeholder_211(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint64_t param_3, uint64_t param_4, uint64_t param_5, uint64_t param_6);
extern struct indirect_placeholder_212_ret_type indirect_placeholder_212(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint64_t param_3, uint64_t param_4);
extern struct indirect_placeholder_213_ret_type indirect_placeholder_213(uint64_t param_0, uint64_t param_1, uint64_t param_2);
uint64_t bb_rm(uint64_t rsi, uint64_t rdi) {
    struct indirect_placeholder_212_ret_type var_22;
    struct indirect_placeholder_211_ret_type var_33;
    uint64_t var_0;
    uint64_t var_1;
    uint64_t var_2;
    uint64_t *var_3;
    uint64_t *var_4;
    uint32_t *var_5;
    uint32_t var_39;
    uint64_t rcx_1;
    uint64_t var_38;
    uint64_t r8_0;
    uint64_t r9_0;
    uint64_t rcx_2;
    uint64_t var_26;
    uint32_t var_27;
    uint64_t local_sp_0;
    uint64_t rcx_0;
    uint64_t var_17;
    uint64_t var_30;
    uint64_t var_31;
    uint64_t var_32;
    uint64_t r8_1;
    uint64_t r9_1;
    uint64_t var_34;
    uint64_t var_35;
    uint64_t var_36;
    uint64_t local_sp_1;
    uint64_t var_37;
    uint64_t var_28;
    uint32_t var_29;
    uint64_t var_19;
    uint64_t var_20;
    uint64_t var_21;
    uint64_t var_23;
    uint64_t var_24;
    uint32_t var_25;
    uint64_t local_sp_2;
    uint64_t var_18;
    uint32_t *var_6;
    uint32_t spec_store_select;
    uint64_t var_7;
    uint64_t var_8;
    uint64_t var_9;
    struct indirect_placeholder_213_ret_type var_10;
    uint64_t var_11;
    uint64_t var_12;
    uint64_t var_13;
    uint64_t *var_14;
    uint64_t *var_15;
    uint32_t *var_16;
    var_0 = revng_init_local_sp(0UL);
    var_1 = init_rbp();
    *(uint64_t *)(var_0 + (-8L)) = var_1;
    var_2 = var_0 + (-48L);
    var_3 = (uint64_t *)var_2;
    *var_3 = rdi;
    var_4 = (uint64_t *)(var_0 + (-56L));
    *var_4 = rsi;
    var_5 = (uint32_t *)(var_0 + (-12L));
    *var_5 = 2U;
    var_39 = 2U;
    if (**(uint64_t **)var_2 == 0UL) {
        return (uint64_t)var_39;
    }
    var_6 = (uint32_t *)(var_0 + (-16L));
    *var_6 = 536U;
    spec_store_select = (*(unsigned char *)(*var_4 + 8UL) == '\x00') ? 536U : 600U;
    *var_6 = spec_store_select;
    var_7 = (uint64_t)spec_store_select;
    var_8 = *var_3;
    var_9 = var_0 + (-64L);
    *(uint64_t *)var_9 = 4213483UL;
    var_10 = indirect_placeholder_213(0UL, var_7, var_8);
    var_11 = var_10.field_0;
    var_12 = var_10.field_1;
    var_13 = var_10.field_2;
    var_14 = (uint64_t *)(var_0 + (-24L));
    *var_14 = var_11;
    var_15 = (uint64_t *)(var_0 + (-32L));
    var_16 = (uint32_t *)(var_0 + (-36L));
    var_39 = 4U;
    rcx_2 = var_7;
    var_17 = var_11;
    r8_1 = var_12;
    r9_1 = var_13;
    local_sp_2 = var_9;
    *(uint64_t *)(local_sp_2 + (-8L)) = 4213499UL;
    var_18 = indirect_placeholder_11(r8_1, var_17, r9_1);
    *var_15 = var_18;
    rcx_0 = var_18;
    rcx_1 = rcx_2;
    r8_0 = r8_1;
    r9_0 = r9_1;
    while (var_18 != 0UL)
        {
            var_19 = *var_4;
            var_20 = *var_14;
            var_21 = local_sp_2 + (-16L);
            *(uint64_t *)var_21 = 4213584UL;
            var_22 = indirect_placeholder_212(var_19, r8_1, var_18, var_20, r9_1);
            var_23 = var_22.field_1;
            var_24 = var_22.field_2;
            var_25 = (uint32_t)var_22.field_0;
            *var_16 = var_25;
            var_27 = var_25;
            local_sp_0 = var_21;
            r8_1 = var_23;
            r9_1 = var_24;
            if ((var_25 + (-2)) < 3U) {
                var_26 = local_sp_2 + (-24L);
                *(uint64_t *)var_26 = 4213630UL;
                indirect_placeholder_1();
                var_27 = *var_16;
                local_sp_0 = var_26;
                rcx_0 = 4358087UL;
            }
            local_sp_2 = local_sp_0;
            rcx_2 = rcx_0;
            switch (var_27) {
              case 4U:
                {
                    *var_5 = var_27;
                }
                break;
              case 3U:
                {
                    if (*var_5 == 2U) {
                        *var_5 = var_27;
                    }
                }
                break;
              default:
                {
                    var_17 = *var_14;
                    *(uint64_t *)(local_sp_2 + (-8L)) = 4213499UL;
                    var_18 = indirect_placeholder_11(r8_1, var_17, r9_1);
                    *var_15 = var_18;
                    rcx_0 = var_18;
                    rcx_1 = rcx_2;
                    r8_0 = r8_1;
                    r9_0 = r9_1;
                    continue;
                }
                break;
            }
        }
    var_28 = local_sp_2 + (-16L);
    *(uint64_t *)var_28 = 4213515UL;
    indirect_placeholder_1();
    var_29 = *(volatile uint32_t *)(uint32_t *)0UL;
    local_sp_1 = var_28;
    if (var_29 == 0U) {
        var_30 = (uint64_t)var_29;
        *(uint64_t *)(local_sp_2 + (-24L)) = 4213526UL;
        indirect_placeholder_1();
        var_31 = (uint64_t)*(uint32_t *)var_30;
        var_32 = local_sp_2 + (-32L);
        *(uint64_t *)var_32 = 4213550UL;
        var_33 = indirect_placeholder_211(0UL, 4358024UL, rcx_2, r8_1, var_31, 0UL, r9_1);
        var_34 = var_33.field_0;
        var_35 = var_33.field_1;
        var_36 = var_33.field_2;
        *var_5 = 4U;
        local_sp_1 = var_32;
        rcx_1 = var_34;
        r8_0 = var_35;
        r9_0 = var_36;
    }
    *(uint64_t *)(local_sp_1 + (-8L)) = 4213674UL;
    var_37 = indirect_placeholder_12();
    if ((uint64_t)(uint32_t)var_37 == 0UL) {
        var_39 = *var_5;
    } else {
        *(uint64_t *)(local_sp_1 + (-16L)) = 4213683UL;
        indirect_placeholder_1();
        var_38 = (uint64_t)*(uint32_t *)var_37;
        *(uint64_t *)(local_sp_1 + (-24L)) = 4213707UL;
        indirect_placeholder_210(0UL, 4358070UL, rcx_1, r8_0, var_38, 0UL, r9_0);
        *var_5 = 4U;
    }
    return (uint64_t)var_39;
}

typedef unsigned char __u_char;
typedef unsigned short __u_short;
typedef unsigned int __u_int;
typedef unsigned long __u_long;
typedef signed char __int8_t;
typedef unsigned char __uint8_t;
typedef short __int16_t;
typedef unsigned short __uint16_t;
typedef int __int32_t;
typedef unsigned int __uint32_t;
typedef long __int64_t;
typedef unsigned long __uint64_t;
typedef __int8_t __int_least8_t;
typedef __uint8_t __uint_least8_t;
typedef __int16_t __int_least16_t;
typedef __uint16_t __uint_least16_t;
typedef __int32_t __int_least32_t;
typedef __uint32_t __uint_least32_t;
typedef __int64_t __int_least64_t;
typedef __uint64_t __uint_least64_t;
typedef long __quad_t;
typedef unsigned long __u_quad_t;
typedef long __intmax_t;
typedef unsigned long __uintmax_t;
typedef unsigned long __dev_t;
typedef unsigned int __uid_t;
typedef unsigned int __gid_t;
typedef unsigned long __ino_t;
typedef unsigned long __ino64_t;
typedef unsigned int __mode_t;
typedef unsigned long __nlink_t;
typedef long __off_t;
typedef long __off64_t;
typedef int __pid_t;
typedef struct {
    int __val[2];
} __fsid_t;
typedef long __clock_t;
typedef unsigned long __rlim_t;
typedef unsigned long __rlim64_t;
typedef unsigned int __id_t;
typedef long __time_t;
typedef unsigned int __useconds_t;
typedef long __suseconds_t;
typedef int __daddr_t;
typedef int __key_t;
typedef int __clockid_t;
typedef void *__timer_t;
typedef long __blksize_t;
typedef long __blkcnt_t;
typedef long __blkcnt64_t;
typedef unsigned long __fsblkcnt_t;
typedef unsigned long __fsblkcnt64_t;
typedef unsigned long __fsfilcnt_t;
typedef unsigned long __fsfilcnt64_t;
typedef long __fsword_t;
typedef long __ssize_t;
typedef long __syscall_slong_t;
typedef unsigned long __syscall_ulong_t;
typedef __off64_t __loff_t;
typedef char *__caddr_t;
typedef long __intptr_t;
typedef unsigned int __socklen_t;
typedef int __sig_atomic_t;
typedef __int8_t int8_t;
typedef __int16_t int16_t;
typedef __int32_t int32_t;
typedef __int64_t int64_t;
typedef __uint8_t uint8_t;
typedef __uint16_t uint16_t;
typedef __uint32_t uint32_t;
typedef __uint64_t uint64_t;
typedef __int_least8_t int_least8_t;
typedef __int_least16_t int_least16_t;
typedef __int_least32_t int_least32_t;
typedef __int_least64_t int_least64_t;
typedef __uint_least8_t uint_least8_t;
typedef __uint_least16_t uint_least16_t;
typedef __uint_least32_t uint_least32_t;
typedef __uint_least64_t uint_least64_t;
typedef signed char int_fast8_t;
typedef long int_fast16_t;
typedef long int_fast32_t;
typedef long int_fast64_t;
typedef unsigned char uint_fast8_t;
typedef unsigned long uint_fast16_t;
typedef unsigned long uint_fast32_t;
typedef unsigned long uint_fast64_t;
typedef long intptr_t;
typedef unsigned long uintptr_t;
typedef __intmax_t intmax_t;
typedef __uintmax_t uintmax_t;
struct bb_target_directory_operand_ret_type;
struct indirect_placeholder_215_ret_type;
struct indirect_placeholder_214_ret_type;
struct bb_target_directory_operand_ret_type {
    uint64_t field_0;
    uint64_t field_1;
    uint64_t field_2;
    uint64_t field_3;
};
struct indirect_placeholder_215_ret_type {
    uint64_t field_0;
    uint64_t field_1;
    uint64_t field_2;
};
struct indirect_placeholder_214_ret_type {
    uint64_t field_0;
    uint64_t field_1;
    uint64_t field_2;
};
extern uint64_t revng_init_local_sp(uint64_t param_0);
extern uint64_t init_rbp(void);
extern void indirect_placeholder_1(void);
extern uint64_t init_rcx(void);
extern uint64_t indirect_placeholder_10(uint64_t param_0);
extern uint64_t init_r8(void);
extern uint64_t init_r9(void);
extern struct indirect_placeholder_215_ret_type indirect_placeholder_215(uint64_t param_0, uint64_t param_1);
extern struct indirect_placeholder_214_ret_type indirect_placeholder_214(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint64_t param_3, uint64_t param_4, uint64_t param_5, uint64_t param_6);
struct bb_target_directory_operand_ret_type bb_target_directory_operand(uint64_t rdi) {
    struct indirect_placeholder_214_ret_type var_18;
    uint64_t var_0;
    uint64_t var_1;
    uint64_t var_2;
    uint64_t var_3;
    uint64_t var_4;
    uint64_t *var_5;
    uint64_t var_6;
    uint64_t var_7;
    uint64_t local_sp_0;
    uint64_t var_8;
    uint32_t storemerge1;
    uint32_t *var_9;
    unsigned char storemerge;
    unsigned char *var_10;
    unsigned char var_11;
    unsigned char var_19;
    uint64_t var_12;
    struct indirect_placeholder_215_ret_type var_13;
    uint64_t var_14;
    uint64_t var_15;
    uint64_t var_16;
    uint64_t var_17;
    uint64_t rcx_0;
    uint64_t r8_0;
    uint64_t r9_0;
    struct bb_target_directory_operand_ret_type mrv;
    struct bb_target_directory_operand_ret_type mrv1;
    struct bb_target_directory_operand_ret_type mrv2;
    struct bb_target_directory_operand_ret_type mrv3;
    var_0 = revng_init_local_sp(0UL);
    var_1 = init_rbp();
    var_2 = init_rcx();
    var_3 = init_r8();
    var_4 = init_r9();
    *(uint64_t *)(var_0 + (-8L)) = var_1;
    var_5 = (uint64_t *)(var_0 + (-176L));
    *var_5 = rdi;
    var_6 = var_0 + (-192L);
    *(uint64_t *)var_6 = 4207357UL;
    var_7 = indirect_placeholder_10(rdi);
    local_sp_0 = var_6;
    storemerge1 = 0U;
    storemerge = (unsigned char)'\x00';
    rcx_0 = var_2;
    r8_0 = var_3;
    r9_0 = var_4;
    if ((uint64_t)(uint32_t)var_7 == 0UL) {
        var_8 = var_0 + (-200L);
        *(uint64_t *)var_8 = 4207366UL;
        indirect_placeholder_1();
        local_sp_0 = var_8;
        storemerge1 = *(uint32_t *)var_7;
    }
    var_9 = (uint32_t *)(var_0 + (-12L));
    *var_9 = storemerge1;
    if (storemerge1 != 0U) {
        mrv.field_0 = (uint64_t)var_19;
        mrv1 = mrv;
        mrv1.field_1 = rcx_0;
        mrv2 = mrv1;
        mrv2.field_2 = r8_0;
        mrv3 = mrv2;
        mrv3.field_3 = r9_0;
        return mrv3;
    }
    mrv.field_0 = (uint64_t)var_19;
    mrv1 = mrv;
    mrv1.field_1 = rcx_0;
    mrv2 = mrv1;
    mrv2.field_2 = r8_0;
    mrv3 = mrv2;
    mrv3.field_3 = r9_0;
    return mrv3;
}

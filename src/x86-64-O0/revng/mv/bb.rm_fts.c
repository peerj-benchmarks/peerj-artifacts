typedef unsigned char __u_char;
typedef unsigned short __u_short;
typedef unsigned int __u_int;
typedef unsigned long __u_long;
typedef signed char __int8_t;
typedef unsigned char __uint8_t;
typedef short __int16_t;
typedef unsigned short __uint16_t;
typedef int __int32_t;
typedef unsigned int __uint32_t;
typedef long __int64_t;
typedef unsigned long __uint64_t;
typedef __int8_t __int_least8_t;
typedef __uint8_t __uint_least8_t;
typedef __int16_t __int_least16_t;
typedef __uint16_t __uint_least16_t;
typedef __int32_t __int_least32_t;
typedef __uint32_t __uint_least32_t;
typedef __int64_t __int_least64_t;
typedef __uint64_t __uint_least64_t;
typedef long __quad_t;
typedef unsigned long __u_quad_t;
typedef long __intmax_t;
typedef unsigned long __uintmax_t;
typedef unsigned long __dev_t;
typedef unsigned int __uid_t;
typedef unsigned int __gid_t;
typedef unsigned long __ino_t;
typedef unsigned long __ino64_t;
typedef unsigned int __mode_t;
typedef unsigned long __nlink_t;
typedef long __off_t;
typedef long __off64_t;
typedef int __pid_t;
typedef struct {
    int __val[2];
} __fsid_t;
typedef long __clock_t;
typedef unsigned long __rlim_t;
typedef unsigned long __rlim64_t;
typedef unsigned int __id_t;
typedef long __time_t;
typedef unsigned int __useconds_t;
typedef long __suseconds_t;
typedef int __daddr_t;
typedef int __key_t;
typedef int __clockid_t;
typedef void *__timer_t;
typedef long __blksize_t;
typedef long __blkcnt_t;
typedef long __blkcnt64_t;
typedef unsigned long __fsblkcnt_t;
typedef unsigned long __fsblkcnt64_t;
typedef unsigned long __fsfilcnt_t;
typedef unsigned long __fsfilcnt64_t;
typedef long __fsword_t;
typedef long __ssize_t;
typedef long __syscall_slong_t;
typedef unsigned long __syscall_ulong_t;
typedef __off64_t __loff_t;
typedef char *__caddr_t;
typedef long __intptr_t;
typedef unsigned int __socklen_t;
typedef int __sig_atomic_t;
typedef __int8_t int8_t;
typedef __int16_t int16_t;
typedef __int32_t int32_t;
typedef __int64_t int64_t;
typedef __uint8_t uint8_t;
typedef __uint16_t uint16_t;
typedef __uint32_t uint32_t;
typedef __uint64_t uint64_t;
typedef __int_least8_t int_least8_t;
typedef __int_least16_t int_least16_t;
typedef __int_least32_t int_least32_t;
typedef __int_least64_t int_least64_t;
typedef __uint_least8_t uint_least8_t;
typedef __uint_least16_t uint_least16_t;
typedef __uint_least32_t uint_least32_t;
typedef __uint_least64_t uint_least64_t;
typedef signed char int_fast8_t;
typedef long int_fast16_t;
typedef long int_fast32_t;
typedef long int_fast64_t;
typedef unsigned char uint_fast8_t;
typedef unsigned long uint_fast16_t;
typedef unsigned long uint_fast32_t;
typedef unsigned long uint_fast64_t;
typedef long intptr_t;
typedef unsigned long uintptr_t;
typedef __intmax_t intmax_t;
typedef __uintmax_t uintmax_t;
struct bb_rm_fts_ret_type;
struct indirect_placeholder_150_ret_type;
struct indirect_placeholder_147_ret_type;
struct indirect_placeholder_151_ret_type;
struct indirect_placeholder_132_ret_type;
struct indirect_placeholder_139_ret_type;
struct indirect_placeholder_141_ret_type;
struct indirect_placeholder_140_ret_type;
struct indirect_placeholder_142_ret_type;
struct indirect_placeholder_138_ret_type;
struct indirect_placeholder_137_ret_type;
struct indirect_placeholder_146_ret_type;
struct indirect_placeholder_145_ret_type;
struct indirect_placeholder_144_ret_type;
struct indirect_placeholder_143_ret_type;
struct indirect_placeholder_148_ret_type;
struct indirect_placeholder_133_ret_type;
struct indirect_placeholder_149_ret_type;
struct indirect_placeholder_134_ret_type;
struct indirect_placeholder_136_ret_type;
struct indirect_placeholder_135_ret_type;
struct bb_rm_fts_ret_type {
    uint64_t field_0;
    uint64_t field_1;
    uint64_t field_2;
};
struct indirect_placeholder_150_ret_type {
    uint64_t field_0;
    uint64_t field_1;
    uint64_t field_2;
};
struct indirect_placeholder_147_ret_type {
    uint64_t field_0;
    uint64_t field_1;
    uint64_t field_2;
};
struct indirect_placeholder_151_ret_type {
    uint64_t field_0;
    uint64_t field_1;
    uint64_t field_2;
};
struct indirect_placeholder_132_ret_type {
    uint64_t field_0;
    uint64_t field_1;
    uint64_t field_2;
};
struct indirect_placeholder_139_ret_type {
    uint64_t field_0;
    uint64_t field_1;
    uint64_t field_2;
};
struct indirect_placeholder_141_ret_type {
    uint64_t field_0;
    uint64_t field_1;
    uint64_t field_2;
};
struct indirect_placeholder_140_ret_type {
    uint64_t field_0;
    uint64_t field_1;
    uint64_t field_2;
};
struct indirect_placeholder_142_ret_type {
    uint64_t field_0;
    uint64_t field_1;
};
struct indirect_placeholder_138_ret_type {
    uint64_t field_0;
    uint64_t field_1;
};
struct indirect_placeholder_137_ret_type {
    uint64_t field_0;
    uint64_t field_1;
    uint64_t field_2;
};
struct indirect_placeholder_146_ret_type {
    uint64_t field_0;
    uint64_t field_1;
};
struct indirect_placeholder_145_ret_type {
    uint64_t field_0;
    uint64_t field_1;
};
struct indirect_placeholder_144_ret_type {
    uint64_t field_0;
    uint64_t field_1;
};
struct indirect_placeholder_143_ret_type {
    uint64_t field_0;
    uint64_t field_1;
    uint64_t field_2;
};
struct indirect_placeholder_148_ret_type {
    uint64_t field_0;
    uint64_t field_1;
    uint64_t field_2;
};
struct indirect_placeholder_133_ret_type {
    uint64_t field_0;
    uint64_t field_1;
    uint64_t field_2;
};
struct indirect_placeholder_149_ret_type {
    uint64_t field_0;
    uint64_t field_1;
    uint64_t field_2;
};
struct indirect_placeholder_134_ret_type {
    uint64_t field_0;
    uint64_t field_1;
    uint64_t field_2;
};
struct indirect_placeholder_136_ret_type {
    uint64_t field_0;
    uint64_t field_1;
    uint64_t field_2;
};
struct indirect_placeholder_135_ret_type {
    uint64_t field_0;
    uint64_t field_1;
    uint64_t field_2;
};
extern void abort(void);
extern uint64_t revng_init_local_sp(uint64_t param_0);
extern uint64_t init_rbp(void);
extern void indirect_placeholder_1(void);
extern uint64_t indirect_placeholder_10(uint64_t param_0);
extern uint64_t init_rbx(void);
extern void indirect_placeholder_110(uint64_t param_0);
extern void indirect_placeholder_49(uint64_t param_0, uint64_t param_1);
extern uint64_t indirect_placeholder_45(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint64_t param_3, uint64_t param_4, uint64_t param_5);
extern uint64_t init_r12(void);
extern uint64_t indirect_placeholder_24(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint64_t param_3);
extern uint64_t helper_cc_compute_all_wrapper(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint32_t param_3);
extern struct indirect_placeholder_150_ret_type indirect_placeholder_150(uint64_t param_0, uint64_t param_1);
extern struct indirect_placeholder_147_ret_type indirect_placeholder_147(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint64_t param_3, uint64_t param_4, uint64_t param_5, uint64_t param_6);
extern struct indirect_placeholder_151_ret_type indirect_placeholder_151(uint64_t param_0, uint64_t param_1, uint64_t param_2);
extern struct indirect_placeholder_132_ret_type indirect_placeholder_132(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint64_t param_3, uint64_t param_4, uint64_t param_5, uint64_t param_6);
extern struct indirect_placeholder_139_ret_type indirect_placeholder_139(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint64_t param_3, uint64_t param_4, uint64_t param_5, uint64_t param_6);
extern struct indirect_placeholder_141_ret_type indirect_placeholder_141(uint64_t param_0, uint64_t param_1);
extern struct indirect_placeholder_140_ret_type indirect_placeholder_140(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint64_t param_3, uint64_t param_4, uint64_t param_5, uint64_t param_6);
extern struct indirect_placeholder_142_ret_type indirect_placeholder_142(uint64_t param_0, uint64_t param_1, uint64_t param_2);
extern struct indirect_placeholder_138_ret_type indirect_placeholder_138(uint64_t param_0, uint64_t param_1, uint64_t param_2);
extern struct indirect_placeholder_137_ret_type indirect_placeholder_137(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint64_t param_3, uint64_t param_4, uint64_t param_5, uint64_t param_6);
extern struct indirect_placeholder_146_ret_type indirect_placeholder_146(uint64_t param_0, uint64_t param_1, uint64_t param_2);
extern struct indirect_placeholder_145_ret_type indirect_placeholder_145(uint64_t param_0, uint64_t param_1, uint64_t param_2);
extern struct indirect_placeholder_144_ret_type indirect_placeholder_144(uint64_t param_0, uint64_t param_1, uint64_t param_2);
extern struct indirect_placeholder_143_ret_type indirect_placeholder_143(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint64_t param_3, uint64_t param_4, uint64_t param_5, uint64_t param_6);
extern struct indirect_placeholder_148_ret_type indirect_placeholder_148(uint64_t param_0, uint64_t param_1, uint64_t param_2);
extern struct indirect_placeholder_133_ret_type indirect_placeholder_133(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint64_t param_3, uint64_t param_4, uint64_t param_5, uint64_t param_6);
extern struct indirect_placeholder_149_ret_type indirect_placeholder_149(uint64_t param_0, uint64_t param_1, uint64_t param_2);
extern struct indirect_placeholder_134_ret_type indirect_placeholder_134(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint64_t param_3, uint64_t param_4, uint64_t param_5, uint64_t param_6);
extern struct indirect_placeholder_136_ret_type indirect_placeholder_136(uint64_t param_0, uint64_t param_1);
extern struct indirect_placeholder_135_ret_type indirect_placeholder_135(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint64_t param_3, uint64_t param_4, uint64_t param_5, uint64_t param_6);
struct bb_rm_fts_ret_type bb_rm_fts(uint64_t rdx, uint64_t r8, uint64_t rsi, uint64_t rdi, uint64_t r9) {
    uint64_t var_47;
    uint64_t var_48;
    uint32_t *var_49;
    uint64_t var_75;
    unsigned char *var_57;
    uint64_t var_58;
    uint64_t var_59;
    uint64_t var_60;
    uint64_t var_61;
    uint64_t local_sp_2;
    unsigned char *_pre271;
    uint32_t spec_select;
    uint32_t *var_62;
    uint64_t var_63;
    struct indirect_placeholder_150_ret_type var_64;
    uint64_t var_65;
    uint64_t var_66;
    uint64_t var_67;
    uint64_t var_68;
    struct indirect_placeholder_147_ret_type var_69;
    uint64_t var_70;
    uint64_t var_71;
    uint64_t var_72;
    uint64_t var_73;
    uint64_t var_74;
    uint64_t local_sp_3;
    uint64_t var_76;
    uint64_t var_77;
    uint64_t var_78;
    uint64_t var_79;
    uint64_t var_118;
    uint64_t local_sp_4;
    uint64_t var_119;
    uint64_t var_120;
    uint64_t var_121;
    uint64_t var_122;
    uint32_t *var_123;
    uint32_t var_124;
    uint64_t var_138;
    struct indirect_placeholder_151_ret_type var_139;
    uint64_t var_140;
    uint64_t var_141;
    struct indirect_placeholder_132_ret_type var_142;
    uint64_t var_143;
    uint64_t var_144;
    struct indirect_placeholder_139_ret_type var_112;
    struct indirect_placeholder_140_ret_type var_111;
    uint64_t var_0;
    uint64_t var_1;
    uint64_t var_2;
    uint64_t var_3;
    uint64_t var_4;
    uint64_t *var_5;
    uint64_t *var_6;
    uint64_t *var_7;
    uint64_t var_8;
    uint16_t var_9;
    uint64_t var_51;
    uint64_t var_52;
    uint64_t var_53;
    uint64_t var_54;
    uint64_t var_55;
    uint64_t var_56;
    uint64_t r84_0;
    uint64_t var_125;
    uint64_t var_126;
    uint64_t var_127;
    uint64_t var_128;
    uint64_t var_129;
    uint64_t var_130;
    uint64_t var_131;
    uint64_t var_117;
    uint32_t var_132;
    uint64_t local_sp_0;
    uint32_t var_136;
    uint64_t var_133;
    uint64_t var_134;
    uint64_t var_135;
    uint64_t var_137;
    uint64_t local_sp_1;
    uint64_t rcx_0;
    uint64_t var_41;
    uint64_t r97_0;
    uint64_t var_113;
    uint64_t var_114;
    uint64_t var_115;
    uint64_t var_116;
    uint64_t var_105;
    struct indirect_placeholder_141_ret_type var_106;
    uint64_t var_107;
    uint64_t var_108;
    uint64_t var_109;
    uint64_t var_110;
    struct indirect_placeholder_142_ret_type var_97;
    uint64_t var_98;
    uint64_t var_99;
    struct indirect_placeholder_138_ret_type var_100;
    uint64_t var_101;
    uint64_t var_102;
    uint64_t var_103;
    struct indirect_placeholder_137_ret_type var_104;
    uint64_t var_80;
    struct indirect_placeholder_146_ret_type var_81;
    uint64_t var_82;
    struct indirect_placeholder_145_ret_type var_83;
    uint64_t var_84;
    struct indirect_placeholder_144_ret_type var_85;
    uint64_t var_86;
    struct indirect_placeholder_143_ret_type var_87;
    uint64_t var_88;
    uint64_t var_89;
    uint64_t var_90;
    uint64_t var_91;
    uint64_t var_92;
    uint64_t var_93;
    uint64_t var_94;
    uint32_t var_50;
    uint64_t var_95;
    uint64_t var_96;
    unsigned char *_pre_phi272;
    uint64_t rax_0;
    uint64_t r84_1;
    uint64_t r97_1;
    struct bb_rm_fts_ret_type mrv;
    struct bb_rm_fts_ret_type mrv1;
    struct bb_rm_fts_ret_type mrv2;
    uint64_t var_10;
    struct indirect_placeholder_148_ret_type var_11;
    uint64_t var_12;
    uint64_t var_13;
    uint64_t var_14;
    uint64_t var_15;
    struct indirect_placeholder_133_ret_type var_16;
    uint64_t var_17;
    uint64_t var_18;
    uint64_t var_19;
    uint64_t var_20;
    uint64_t var_21;
    struct indirect_placeholder_149_ret_type var_22;
    uint64_t var_23;
    uint64_t var_24;
    uint64_t var_25;
    struct indirect_placeholder_134_ret_type var_26;
    uint64_t var_27;
    uint64_t var_28;
    uint64_t var_29;
    uint64_t var_30;
    uint64_t var_31;
    uint64_t var_32;
    uint64_t var_33;
    struct indirect_placeholder_136_ret_type var_34;
    uint64_t var_35;
    uint64_t var_36;
    uint64_t var_37;
    struct indirect_placeholder_135_ret_type var_38;
    uint64_t var_39;
    uint64_t var_40;
    unsigned char storemerge;
    unsigned char *var_42;
    unsigned char var_43;
    uint64_t var_44;
    uint64_t var_45;
    uint64_t var_46;
    var_0 = revng_init_local_sp(0UL);
    var_1 = init_rbp();
    var_2 = init_rbx();
    var_3 = init_r12();
    *(uint64_t *)(var_0 + (-8L)) = var_1;
    *(uint64_t *)(var_0 + (-16L)) = var_3;
    *(uint64_t *)(var_0 + (-24L)) = var_2;
    var_4 = var_0 + (-88L);
    var_5 = (uint64_t *)(var_0 + (-64L));
    *var_5 = rdi;
    var_6 = (uint64_t *)(var_0 + (-72L));
    *var_6 = rsi;
    var_7 = (uint64_t *)(var_0 + (-80L));
    *var_7 = rdx;
    var_8 = *var_6;
    var_9 = *(uint16_t *)(var_8 + 112UL);
    var_75 = var_8;
    local_sp_2 = var_4;
    local_sp_3 = var_4;
    var_136 = 2U;
    var_41 = var_8;
    rax_0 = 4UL;
    r84_1 = 3UL;
    r97_1 = 0UL;
    storemerge = (unsigned char)'\x00';
    if (var_9 <= (unsigned short)13U) {
        var_138 = *(uint64_t *)(var_8 + 56UL);
        *(uint64_t *)(var_0 + (-96L)) = 4213335UL;
        var_139 = indirect_placeholder_151(var_138, 3UL, 0UL);
        var_140 = var_139.field_0;
        var_141 = (uint64_t)*(uint16_t *)(*var_6 + 112UL);
        *(uint64_t *)(var_0 + (-104L)) = 4213385UL;
        var_142 = indirect_placeholder_132(0UL, 4357856UL, var_141, var_140, 0UL, 0UL, 4357831UL);
        var_143 = var_142.field_1;
        var_144 = var_142.field_2;
        *(uint64_t *)(var_0 + (-112L)) = 4213390UL;
        indirect_placeholder_1();
        rax_0 = 0UL;
        r84_1 = var_143;
        r97_1 = var_144;
        mrv.field_0 = rax_0;
        mrv1 = mrv;
        mrv1.field_1 = r84_1;
        mrv2 = mrv1;
        mrv2.field_2 = r97_1;
        return mrv2;
    }
    switch (*(uint64_t *)(((uint64_t)var_9 << 3UL) + 4357912UL)) {
      case 4213139UL:
        {
            var_21 = *(uint64_t *)(var_8 + 56UL);
            *(uint64_t *)(var_0 + (-96L)) = 4213165UL;
            var_22 = indirect_placeholder_149(var_21, 3UL, 0UL);
            var_23 = var_22.field_0;
            var_24 = var_22.field_1;
            var_25 = var_22.field_2;
            *(uint64_t *)(var_0 + (-104L)) = 4213193UL;
            var_26 = indirect_placeholder_134(0UL, 4357624UL, var_23, var_24, 0UL, 0UL, var_25);
            var_27 = var_26.field_1;
            var_28 = var_26.field_2;
            var_29 = *var_6;
            var_30 = *var_5;
            *(uint64_t *)(var_0 + (-112L)) = 4213212UL;
            indirect_placeholder_49(var_29, var_30);
            r84_1 = var_27;
            r97_1 = var_28;
        }
        break;
      case 4213222UL:
        {
            var_10 = *(uint64_t *)(var_8 + 56UL);
            *(uint64_t *)(var_0 + (-96L)) = 4213248UL;
            var_11 = indirect_placeholder_148(var_10, 3UL, 0UL);
            var_12 = var_11.field_0;
            var_13 = var_11.field_1;
            var_14 = var_11.field_2;
            var_15 = (uint64_t)*(uint32_t *)(*var_6 + 64UL);
            *(uint64_t *)(var_0 + (-104L)) = 4213283UL;
            var_16 = indirect_placeholder_133(0UL, 4357810UL, var_12, var_13, var_15, 0UL, var_14);
            var_17 = var_16.field_1;
            var_18 = var_16.field_2;
            var_19 = *var_6;
            var_20 = *var_5;
            *(uint64_t *)(var_0 + (-112L)) = 4213302UL;
            indirect_placeholder_49(var_19, var_20);
            r84_1 = var_17;
            r97_1 = var_18;
        }
        break;
      case 4213309UL:
        {
            var_138 = *(uint64_t *)(var_8 + 56UL);
            *(uint64_t *)(var_0 + (-96L)) = 4213335UL;
            var_139 = indirect_placeholder_151(var_138, 3UL, 0UL);
            var_140 = var_139.field_0;
            var_141 = (uint64_t)*(uint16_t *)(*var_6 + 112UL);
            *(uint64_t *)(var_0 + (-104L)) = 4213385UL;
            var_142 = indirect_placeholder_132(0UL, 4357856UL, var_141, var_140, 0UL, 0UL, 4357831UL);
            var_143 = var_142.field_1;
            var_144 = var_142.field_2;
            *(uint64_t *)(var_0 + (-112L)) = 4213390UL;
            indirect_placeholder_1();
            rax_0 = 0UL;
            r84_1 = var_143;
            r97_1 = var_144;
        }
        break;
      case 4212072UL:
        {
            r84_1 = 2UL;
            if (*(unsigned char *)(rdx + 9UL) != '\x01') {
                var_57 = (unsigned char *)(rdx + 10UL);
                _pre_phi272 = var_57;
                if (*var_57 != '\x01') {
                    spec_select = (*_pre_phi272 == '\x00') ? 21U : 39U;
                    var_62 = (uint32_t *)(var_0 + (-32L));
                    *var_62 = spec_select;
                    var_63 = *(uint64_t *)(*var_6 + 56UL);
                    *(uint64_t *)(local_sp_2 + (-8L)) = 4212186UL;
                    var_64 = indirect_placeholder_150(var_63, 4UL);
                    var_65 = var_64.field_0;
                    var_66 = var_64.field_1;
                    var_67 = var_64.field_2;
                    var_68 = (uint64_t)*var_62;
                    *(uint64_t *)(local_sp_2 + (-16L)) = 4212217UL;
                    var_69 = indirect_placeholder_147(0UL, 4357152UL, var_65, var_66, var_68, 0UL, var_67);
                    var_70 = var_69.field_1;
                    var_71 = var_69.field_2;
                    var_72 = *var_6;
                    *(uint64_t *)(local_sp_2 + (-24L)) = 4212229UL;
                    indirect_placeholder_110(var_72);
                    var_73 = *var_6;
                    var_74 = *var_5;
                    *(uint64_t *)(local_sp_2 + (-32L)) = 4212248UL;
                    indirect_placeholder_49(var_73, var_74);
                    r84_1 = var_70;
                    r97_1 = var_71;
                    mrv.field_0 = rax_0;
                    mrv1 = mrv;
                    mrv1.field_1 = r84_1;
                    mrv2 = mrv1;
                    mrv2.field_2 = r97_1;
                    return mrv2;
                }
                var_58 = *(uint64_t *)(var_8 + 48UL);
                var_59 = (uint64_t)*(uint32_t *)(*var_5 + 44UL);
                var_60 = var_0 + (-96L);
                *(uint64_t *)var_60 = 4212131UL;
                var_61 = indirect_placeholder_24(r8, var_58, var_59, r9);
                local_sp_2 = var_60;
                local_sp_3 = var_60;
                if ((uint64_t)(unsigned char)var_61 != 1UL) {
                    _pre271 = (unsigned char *)(*var_7 + 10UL);
                    _pre_phi272 = _pre271;
                    spec_select = (*_pre_phi272 == '\x00') ? 21U : 39U;
                    var_62 = (uint32_t *)(var_0 + (-32L));
                    *var_62 = spec_select;
                    var_63 = *(uint64_t *)(*var_6 + 56UL);
                    *(uint64_t *)(local_sp_2 + (-8L)) = 4212186UL;
                    var_64 = indirect_placeholder_150(var_63, 4UL);
                    var_65 = var_64.field_0;
                    var_66 = var_64.field_1;
                    var_67 = var_64.field_2;
                    var_68 = (uint64_t)*var_62;
                    *(uint64_t *)(local_sp_2 + (-16L)) = 4212217UL;
                    var_69 = indirect_placeholder_147(0UL, 4357152UL, var_65, var_66, var_68, 0UL, var_67);
                    var_70 = var_69.field_1;
                    var_71 = var_69.field_2;
                    var_72 = *var_6;
                    *(uint64_t *)(local_sp_2 + (-24L)) = 4212229UL;
                    indirect_placeholder_110(var_72);
                    var_73 = *var_6;
                    var_74 = *var_5;
                    *(uint64_t *)(local_sp_2 + (-32L)) = 4212248UL;
                    indirect_placeholder_49(var_73, var_74);
                    r84_1 = var_70;
                    r97_1 = var_71;
                    mrv.field_0 = rax_0;
                    mrv1 = mrv;
                    mrv1.field_1 = r84_1;
                    mrv2 = mrv1;
                    mrv2.field_2 = r97_1;
                    return mrv2;
                }
                var_75 = *var_6;
            }
            var_117 = var_75;
            local_sp_4 = local_sp_3;
            if (*(uint64_t *)(var_75 + 88UL) == 0UL) {
                var_76 = *(uint64_t *)(var_75 + 48UL);
                *(uint64_t *)(local_sp_3 + (-8L)) = 4212291UL;
                var_77 = indirect_placeholder_10(var_76);
                var_78 = local_sp_3 + (-16L);
                *(uint64_t *)var_78 = 4212299UL;
                var_79 = indirect_placeholder_10(var_77);
                local_sp_4 = var_78;
                if ((uint64_t)(unsigned char)var_79 != 0UL) {
                    var_80 = *(uint64_t *)(*var_6 + 56UL);
                    *(uint64_t *)(local_sp_3 + (-24L)) = 4212333UL;
                    var_81 = indirect_placeholder_146(var_80, 4UL, 2UL);
                    var_82 = var_81.field_0;
                    *(uint64_t *)(local_sp_3 + (-32L)) = 4212356UL;
                    var_83 = indirect_placeholder_145(4357344UL, 4UL, 1UL);
                    var_84 = var_83.field_0;
                    *(uint64_t *)(local_sp_3 + (-40L)) = 4212379UL;
                    var_85 = indirect_placeholder_144(4357347UL, 4UL, 0UL);
                    var_86 = var_85.field_0;
                    *(uint64_t *)(local_sp_3 + (-48L)) = 4212413UL;
                    var_87 = indirect_placeholder_143(0UL, 4357352UL, var_86, var_84, 0UL, 0UL, var_82);
                    var_88 = var_87.field_1;
                    var_89 = var_87.field_2;
                    var_90 = *var_6;
                    var_91 = *var_5;
                    *(uint64_t *)(local_sp_3 + (-56L)) = 4212432UL;
                    indirect_placeholder_49(var_90, var_91);
                    r84_1 = var_88;
                    r97_1 = var_89;
                    mrv.field_0 = rax_0;
                    mrv1 = mrv;
                    mrv1.field_1 = r84_1;
                    mrv2 = mrv1;
                    mrv2.field_2 = r97_1;
                    return mrv2;
                }
                var_92 = *var_7;
                var_93 = var_92 + 16UL;
                var_94 = *(uint64_t *)var_93;
                var_118 = var_92;
                if (var_94 != 0UL) {
                    var_95 = *var_6;
                    var_117 = var_95;
                    if (*(uint64_t *)(var_95 + 128UL) != **(uint64_t **)var_93 & *(uint64_t *)(var_95 + 120UL) != *(uint64_t *)(var_94 + 8UL)) {
                        var_96 = *(uint64_t *)(var_95 + 56UL);
                        *(uint64_t *)(local_sp_3 + (-24L)) = 4212540UL;
                        indirect_placeholder_1();
                        if ((uint64_t)(uint32_t)var_96 == 0UL) {
                            var_105 = *(uint64_t *)(*var_6 + 56UL);
                            *(uint64_t *)(local_sp_3 + (-32L)) = 4212565UL;
                            var_106 = indirect_placeholder_141(var_105, 4UL);
                            var_107 = var_106.field_0;
                            var_108 = var_106.field_1;
                            var_109 = var_106.field_2;
                            var_110 = local_sp_3 + (-40L);
                            *(uint64_t *)var_110 = 4212593UL;
                            var_111 = indirect_placeholder_140(0UL, 4357408UL, var_107, var_108, 0UL, 0UL, var_109);
                            local_sp_1 = var_110;
                            rcx_0 = var_111.field_0;
                            r84_0 = var_111.field_1;
                            r97_0 = var_111.field_2;
                        } else {
                            *(uint64_t *)(local_sp_3 + (-32L)) = 4212615UL;
                            var_97 = indirect_placeholder_142(4357403UL, 4UL, 1UL);
                            var_98 = var_97.field_0;
                            var_99 = *(uint64_t *)(*var_6 + 56UL);
                            *(uint64_t *)(local_sp_3 + (-40L)) = 4212644UL;
                            var_100 = indirect_placeholder_138(var_99, 4UL, 0UL);
                            var_101 = var_100.field_0;
                            var_102 = var_100.field_1;
                            var_103 = local_sp_3 + (-48L);
                            *(uint64_t *)var_103 = 4212675UL;
                            var_104 = indirect_placeholder_137(0UL, 4357456UL, var_101, var_98, 0UL, 0UL, var_102);
                            local_sp_1 = var_103;
                            rcx_0 = var_104.field_0;
                            r84_0 = var_104.field_1;
                            r97_0 = var_104.field_2;
                        }
                        *(uint64_t *)(local_sp_1 + (-8L)) = 4212700UL;
                        var_112 = indirect_placeholder_139(0UL, 4357520UL, rcx_0, r84_0, 0UL, 0UL, r97_0);
                        var_113 = var_112.field_1;
                        var_114 = var_112.field_2;
                        var_115 = *var_6;
                        var_116 = *var_5;
                        *(uint64_t *)(local_sp_1 + (-16L)) = 4212719UL;
                        indirect_placeholder_49(var_115, var_116);
                        r84_1 = var_113;
                        r97_1 = var_114;
                        mrv.field_0 = rax_0;
                        mrv1 = mrv;
                        mrv1.field_1 = r84_1;
                        mrv2 = mrv1;
                        mrv2.field_2 = r97_1;
                        return mrv2;
                    }
                }
                var_117 = *var_6;
            } else {
                var_118 = *var_7;
                var_119 = var_0 + (-44L);
                var_120 = *var_5;
                var_121 = local_sp_4 + (-8L);
                *(uint64_t *)var_121 = 4212770UL;
                var_122 = indirect_placeholder_45(1UL, var_118, 2UL, var_117, var_120, var_119);
                var_123 = (uint32_t *)(var_0 + (-28L));
                var_124 = (uint32_t)var_122;
                *var_123 = var_124;
                var_132 = var_124;
                local_sp_0 = var_121;
                r97_1 = var_119;
                var_132 = 2U;
                if (var_124 != 2U & *(uint32_t *)var_119 == 4U) {
                    var_125 = *var_7;
                    var_126 = *var_6;
                    var_127 = *var_5;
                    *(uint64_t *)(local_sp_4 + (-16L)) = 4212812UL;
                    var_128 = indirect_placeholder_24(var_125, 1UL, var_126, var_127);
                    *var_123 = (uint32_t)var_128;
                    var_129 = *var_6;
                    var_130 = *var_5;
                    var_131 = local_sp_4 + (-24L);
                    *(uint64_t *)var_131 = 4212834UL;
                    indirect_placeholder_49(var_129, var_130);
                    var_132 = *var_123;
                    local_sp_0 = var_131;
                }
                if (var_132 != 2U) {
                    var_133 = *var_6;
                    *(uint64_t *)(local_sp_0 + (-8L)) = 4212852UL;
                    indirect_placeholder_110(var_133);
                    var_134 = *var_6;
                    var_135 = *var_5;
                    *(uint64_t *)(local_sp_0 + (-16L)) = 4212871UL;
                    indirect_placeholder_49(var_134, var_135);
                    var_136 = *var_123;
                }
                var_137 = (uint64_t)var_136;
                rax_0 = var_137;
            }
        }
        break;
      case 4212879UL:
        {
            if (var_9 == (unsigned short)6U) {
                storemerge = (unsigned char)'\x01';
            } else {
                if (*(unsigned char *)(rdx + 8UL) != '\x00') {
                    var_31 = helper_cc_compute_all_wrapper(*(uint64_t *)(var_8 + 88UL), 0UL, 0UL, 25U);
                    if ((uint64_t)(((unsigned char)(var_31 >> 4UL) ^ (unsigned char)var_31) & '\xc0') == 0UL) {
                        var_32 = *var_6;
                        var_41 = var_32;
                        if (*(uint64_t *)(var_32 + 120UL) != *(uint64_t *)(*var_5 + 24UL)) {
                            *(uint64_t *)(var_0 + (-96L)) = 4212951UL;
                            indirect_placeholder_110(var_32);
                            var_33 = *(uint64_t *)(*var_6 + 56UL);
                            *(uint64_t *)(var_0 + (-104L)) = 4212972UL;
                            var_34 = indirect_placeholder_136(var_33, 4UL);
                            var_35 = var_34.field_0;
                            var_36 = var_34.field_1;
                            var_37 = var_34.field_2;
                            *(uint64_t *)(var_0 + (-112L)) = 4213000UL;
                            var_38 = indirect_placeholder_135(0UL, 4357576UL, var_35, var_36, 0UL, 0UL, var_37);
                            var_39 = var_38.field_1;
                            var_40 = var_38.field_2;
                            r84_1 = var_39;
                            r97_1 = var_40;
                            mrv.field_0 = rax_0;
                            mrv1 = mrv;
                            mrv1.field_1 = r84_1;
                            mrv2 = mrv1;
                            mrv2.field_2 = r97_1;
                            return mrv2;
                        }
                    }
                    var_41 = *var_6;
                }
            }
        }
        break;
      default:
        {
            abort();
        }
        break;
    }
    mrv.field_0 = rax_0;
    mrv1 = mrv;
    mrv1.field_1 = r84_1;
    mrv2 = mrv1;
    mrv2.field_2 = r97_1;
    return mrv2;
}

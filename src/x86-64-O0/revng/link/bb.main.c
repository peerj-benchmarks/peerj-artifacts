typedef unsigned char __u_char;
typedef unsigned short __u_short;
typedef unsigned int __u_int;
typedef unsigned long __u_long;
typedef signed char __int8_t;
typedef unsigned char __uint8_t;
typedef short __int16_t;
typedef unsigned short __uint16_t;
typedef int __int32_t;
typedef unsigned int __uint32_t;
typedef long __int64_t;
typedef unsigned long __uint64_t;
typedef __int8_t __int_least8_t;
typedef __uint8_t __uint_least8_t;
typedef __int16_t __int_least16_t;
typedef __uint16_t __uint_least16_t;
typedef __int32_t __int_least32_t;
typedef __uint32_t __uint_least32_t;
typedef __int64_t __int_least64_t;
typedef __uint64_t __uint_least64_t;
typedef long __quad_t;
typedef unsigned long __u_quad_t;
typedef long __intmax_t;
typedef unsigned long __uintmax_t;
typedef unsigned long __dev_t;
typedef unsigned int __uid_t;
typedef unsigned int __gid_t;
typedef unsigned long __ino_t;
typedef unsigned long __ino64_t;
typedef unsigned int __mode_t;
typedef unsigned long __nlink_t;
typedef long __off_t;
typedef long __off64_t;
typedef int __pid_t;
typedef struct {
    int __val[2];
} __fsid_t;
typedef long __clock_t;
typedef unsigned long __rlim_t;
typedef unsigned long __rlim64_t;
typedef unsigned int __id_t;
typedef long __time_t;
typedef unsigned int __useconds_t;
typedef long __suseconds_t;
typedef int __daddr_t;
typedef int __key_t;
typedef int __clockid_t;
typedef void *__timer_t;
typedef long __blksize_t;
typedef long __blkcnt_t;
typedef long __blkcnt64_t;
typedef unsigned long __fsblkcnt_t;
typedef unsigned long __fsblkcnt64_t;
typedef unsigned long __fsfilcnt_t;
typedef unsigned long __fsfilcnt64_t;
typedef long __fsword_t;
typedef long __ssize_t;
typedef long __syscall_slong_t;
typedef unsigned long __syscall_ulong_t;
typedef __off64_t __loff_t;
typedef char *__caddr_t;
typedef long __intptr_t;
typedef unsigned int __socklen_t;
typedef int __sig_atomic_t;
typedef __int8_t int8_t;
typedef __int16_t int16_t;
typedef __int32_t int32_t;
typedef __int64_t int64_t;
typedef __uint8_t uint8_t;
typedef __uint16_t uint16_t;
typedef __uint32_t uint32_t;
typedef __uint64_t uint64_t;
typedef __int_least8_t int_least8_t;
typedef __int_least16_t int_least16_t;
typedef __int_least32_t int_least32_t;
typedef __int_least64_t int_least64_t;
typedef __uint_least8_t uint_least8_t;
typedef __uint_least16_t uint_least16_t;
typedef __uint_least32_t uint_least32_t;
typedef __uint_least64_t uint_least64_t;
typedef signed char int_fast8_t;
typedef long int_fast16_t;
typedef long int_fast32_t;
typedef long int_fast64_t;
typedef unsigned char uint_fast8_t;
typedef unsigned long uint_fast16_t;
typedef unsigned long uint_fast32_t;
typedef unsigned long uint_fast64_t;
typedef long intptr_t;
typedef unsigned long uintptr_t;
typedef __intmax_t intmax_t;
typedef __uintmax_t uintmax_t;
typedef _Bool bool;
struct indirect_placeholder_12_ret_type;
struct indirect_placeholder_9_ret_type;
struct indirect_placeholder_10_ret_type;
struct indirect_placeholder_12_ret_type {
    uint64_t field_0;
    uint64_t field_1;
    uint64_t field_2;
    uint64_t field_3;
};
struct indirect_placeholder_9_ret_type {
    uint64_t field_0;
    uint64_t field_1;
    uint64_t field_2;
};
struct indirect_placeholder_10_ret_type {
    uint64_t field_0;
    uint64_t field_1;
    uint64_t field_2;
};
extern void abort(void);
extern uint64_t revng_init_local_sp(uint64_t param_0);
extern uint64_t init_rbp(void);
extern uint64_t indirect_placeholder_11(uint64_t param_0, uint64_t param_1);
extern void indirect_placeholder(void);
extern uint64_t indirect_placeholder_7(uint64_t param_0, uint64_t param_1, uint64_t param_2);
extern void indirect_placeholder_8(uint64_t param_0, uint64_t param_1);
extern uint64_t init_rbx(void);
extern void indirect_placeholder_5(uint64_t param_0);
extern void indirect_placeholder_6(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint64_t param_3, uint64_t param_4, uint64_t param_5, uint64_t param_6);
extern uint64_t init_r12(void);
extern struct indirect_placeholder_12_ret_type indirect_placeholder_12(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint64_t param_3, uint64_t param_4);
extern struct indirect_placeholder_9_ret_type indirect_placeholder_9(uint64_t param_0);
extern struct indirect_placeholder_10_ret_type indirect_placeholder_10(uint64_t param_0);
void bb_main(uint64_t rsi, uint64_t rdi) {
    struct indirect_placeholder_12_ret_type var_17;
    uint64_t var_0;
    uint64_t var_1;
    uint64_t var_2;
    uint64_t var_3;
    uint64_t var_4;
    uint32_t *var_5;
    uint64_t var_6;
    uint64_t *var_7;
    uint64_t var_8;
    uint64_t var_9;
    uint64_t var_10;
    uint64_t var_11;
    uint64_t *var_12;
    uint64_t var_13;
    uint64_t *var_14;
    uint64_t var_15;
    uint64_t var_16;
    uint64_t var_18;
    uint64_t var_19;
    uint64_t var_20;
    uint64_t var_21;
    uint64_t var_33;
    uint64_t var_34;
    uint64_t var_35;
    uint64_t var_36;
    uint64_t var_37;
    uint64_t local_sp_0;
    uint32_t var_22;
    uint64_t var_23;
    uint64_t var_24;
    uint64_t var_25;
    uint64_t var_43;
    struct indirect_placeholder_9_ret_type var_44;
    uint64_t var_45;
    uint64_t var_46;
    uint64_t var_47;
    uint64_t var_48;
    bool var_26;
    uint64_t var_27;
    uint64_t var_38;
    struct indirect_placeholder_10_ret_type var_39;
    uint64_t var_40;
    uint64_t var_41;
    uint64_t var_42;
    uint64_t var_28;
    uint64_t var_29;
    uint64_t var_30;
    uint64_t var_31;
    uint64_t var_32;
    var_0 = revng_init_local_sp(0UL);
    var_1 = init_rbp();
    var_2 = init_rbx();
    var_3 = init_r12();
    var_4 = var_0 + (-8L);
    *(uint64_t *)var_4 = var_1;
    *(uint64_t *)(var_0 + (-16L)) = var_3;
    *(uint64_t *)(var_0 + (-24L)) = var_2;
    var_5 = (uint32_t *)(var_0 + (-28L));
    *var_5 = (uint32_t)rdi;
    var_6 = var_0 + (-40L);
    var_7 = (uint64_t *)var_6;
    *var_7 = rsi;
    var_8 = **(uint64_t **)var_6;
    *(uint64_t *)(var_0 + (-48L)) = 4201678UL;
    indirect_placeholder_5(var_8);
    *(uint64_t *)(var_0 + (-56L)) = 4201693UL;
    indirect_placeholder();
    *(uint64_t *)(var_0 + (-64L)) = 4201703UL;
    indirect_placeholder();
    var_9 = *(uint64_t *)6374720UL;
    var_10 = *var_7;
    var_11 = (uint64_t)*var_5;
    *(uint64_t *)(var_0 + (-72L)) = 0UL;
    var_12 = (uint64_t *)(var_0 + (-80L));
    *var_12 = 4266859UL;
    var_13 = var_0 + (-88L);
    var_14 = (uint64_t *)var_13;
    *var_14 = 4201755UL;
    indirect_placeholder_6(0UL, 4266854UL, 4266328UL, var_10, var_11, 4201482UL, var_9);
    var_15 = *var_7;
    var_16 = (uint64_t)*var_5;
    *var_12 = 4201789UL;
    var_17 = indirect_placeholder_12(4266491UL, 4266560UL, var_15, var_16, 0UL);
    var_18 = var_17.field_0;
    var_19 = var_17.field_1;
    var_20 = var_17.field_2;
    var_21 = var_17.field_3;
    local_sp_0 = var_13;
    if ((uint64_t)((uint32_t)var_18 + 1U) == 0UL) {
        *var_14 = 4201804UL;
        indirect_placeholder_8(var_4, 1UL);
        abort();
    }
    var_22 = *(uint32_t *)6374872UL;
    var_23 = (uint64_t)*var_5;
    var_24 = (uint64_t)(var_22 + 2U) << 32UL;
    var_25 = var_23 << 32UL;
    if ((long)var_24 > (long)var_25) {
        if ((long)((uint64_t)(var_22 + 1U) << 32UL) > (long)var_25) {
            *var_14 = 4201857UL;
            indirect_placeholder_6(0UL, 4266873UL, var_19, 0UL, 0UL, var_20, var_21);
        } else {
            var_43 = *(uint64_t *)(*var_7 + ((uint64_t)var_22 << 3UL));
            *var_14 = 4201893UL;
            var_44 = indirect_placeholder_9(var_43);
            var_45 = var_44.field_0;
            var_46 = var_44.field_1;
            var_47 = var_44.field_2;
            var_48 = var_0 + (-96L);
            *(uint64_t *)var_48 = 4201921UL;
            indirect_placeholder_6(0UL, 4266889UL, var_45, 0UL, 0UL, var_46, var_47);
            local_sp_0 = var_48;
        }
        *(uint64_t *)(local_sp_0 + (-8L)) = 4201931UL;
        indirect_placeholder_8(var_4, 1UL);
        abort();
    }
    var_26 = ((long)var_24 < (long)var_25);
    var_27 = (uint64_t)var_22 << 3UL;
    if (var_26) {
        var_38 = *(uint64_t *)(*var_7 + (var_27 + 16UL));
        *var_14 = 4201983UL;
        var_39 = indirect_placeholder_10(var_38);
        var_40 = var_39.field_0;
        var_41 = var_39.field_1;
        var_42 = var_39.field_2;
        *(uint64_t *)(var_0 + (-96L)) = 4202011UL;
        indirect_placeholder_6(0UL, 4266914UL, var_40, 0UL, 0UL, var_41, var_42);
        *(uint64_t *)(var_0 + (-104L)) = 4202021UL;
        indirect_placeholder_8(var_4, 1UL);
        abort();
    }
    var_28 = var_27 + 8UL;
    var_29 = *var_7;
    var_30 = *(uint64_t *)(var_29 + var_28);
    var_31 = *(uint64_t *)(var_29 + var_27);
    *var_14 = 4202088UL;
    var_32 = indirect_placeholder_11(var_30, var_31);
    if ((uint64_t)(uint32_t)var_32 == 0UL) {
        return;
    }
    var_33 = *(uint64_t *)(*var_7 + ((uint64_t)*(uint32_t *)6374872UL << 3UL));
    *(uint64_t *)(var_0 + (-96L)) = 4202140UL;
    var_34 = indirect_placeholder_7(var_33, 4UL, 1UL);
    var_35 = *(uint64_t *)(*var_7 + (((uint64_t)*(uint32_t *)6374872UL << 3UL) + 8UL));
    *(uint64_t *)(var_0 + (-104L)) = 4202191UL;
    var_36 = indirect_placeholder_7(var_35, 4UL, 0UL);
    *(uint64_t *)(var_0 + (-112L)) = 4202199UL;
    indirect_placeholder();
    var_37 = (uint64_t)*(uint32_t *)var_36;
    *(uint64_t *)(var_0 + (-120L)) = 4202229UL;
    indirect_placeholder_6(0UL, 4266931UL, var_36, var_37, 1UL, var_20, var_34);
    return;
}

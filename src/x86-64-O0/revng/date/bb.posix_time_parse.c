typedef unsigned char __u_char;
typedef unsigned short __u_short;
typedef unsigned int __u_int;
typedef unsigned long __u_long;
typedef signed char __int8_t;
typedef unsigned char __uint8_t;
typedef short __int16_t;
typedef unsigned short __uint16_t;
typedef int __int32_t;
typedef unsigned int __uint32_t;
typedef long __int64_t;
typedef unsigned long __uint64_t;
typedef __int8_t __int_least8_t;
typedef __uint8_t __uint_least8_t;
typedef __int16_t __int_least16_t;
typedef __uint16_t __uint_least16_t;
typedef __int32_t __int_least32_t;
typedef __uint32_t __uint_least32_t;
typedef __int64_t __int_least64_t;
typedef __uint64_t __uint_least64_t;
typedef long __quad_t;
typedef unsigned long __u_quad_t;
typedef long __intmax_t;
typedef unsigned long __uintmax_t;
typedef unsigned long __dev_t;
typedef unsigned int __uid_t;
typedef unsigned int __gid_t;
typedef unsigned long __ino_t;
typedef unsigned long __ino64_t;
typedef unsigned int __mode_t;
typedef unsigned long __nlink_t;
typedef long __off_t;
typedef long __off64_t;
typedef int __pid_t;
typedef struct {
    int __val[2];
} __fsid_t;
typedef long __clock_t;
typedef unsigned long __rlim_t;
typedef unsigned long __rlim64_t;
typedef unsigned int __id_t;
typedef long __time_t;
typedef unsigned int __useconds_t;
typedef long __suseconds_t;
typedef int __daddr_t;
typedef int __key_t;
typedef int __clockid_t;
typedef void *__timer_t;
typedef long __blksize_t;
typedef long __blkcnt_t;
typedef long __blkcnt64_t;
typedef unsigned long __fsblkcnt_t;
typedef unsigned long __fsblkcnt64_t;
typedef unsigned long __fsfilcnt_t;
typedef unsigned long __fsfilcnt64_t;
typedef long __fsword_t;
typedef long __ssize_t;
typedef long __syscall_slong_t;
typedef unsigned long __syscall_ulong_t;
typedef __off64_t __loff_t;
typedef char *__caddr_t;
typedef long __intptr_t;
typedef unsigned int __socklen_t;
typedef int __sig_atomic_t;
typedef __int8_t int8_t;
typedef __int16_t int16_t;
typedef __int32_t int32_t;
typedef __int64_t int64_t;
typedef __uint8_t uint8_t;
typedef __uint16_t uint16_t;
typedef __uint32_t uint32_t;
typedef __uint64_t uint64_t;
typedef __int_least8_t int_least8_t;
typedef __int_least16_t int_least16_t;
typedef __int_least32_t int_least32_t;
typedef __int_least64_t int_least64_t;
typedef __uint_least8_t uint_least8_t;
typedef __uint_least16_t uint_least16_t;
typedef __uint_least32_t uint_least32_t;
typedef __uint_least64_t uint_least64_t;
typedef signed char int_fast8_t;
typedef long int_fast16_t;
typedef long int_fast32_t;
typedef long int_fast64_t;
typedef unsigned char uint_fast8_t;
typedef unsigned long uint_fast16_t;
typedef unsigned long uint_fast32_t;
typedef unsigned long uint_fast64_t;
typedef long intptr_t;
typedef unsigned long uintptr_t;
typedef __intmax_t intmax_t;
typedef __uintmax_t uintmax_t;
typedef _Bool bool;
extern uint64_t revng_init_local_sp(uint64_t param_0);
extern uint64_t init_rbp(void);
extern void indirect_placeholder_2(void);
extern uint64_t init_cc_src2(void);
extern uint64_t helper_cc_compute_c_wrapper(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint32_t param_3);
extern uint64_t indirect_placeholder_32(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint64_t param_3);
uint64_t bb_posix_time_parse(uint64_t rdx, uint64_t rsi, uint64_t rdi) {
    uint64_t var_21;
    uint64_t var_22;
    uint64_t var_0;
    uint64_t var_1;
    uint64_t var_2;
    uint64_t var_3;
    uint64_t var_4;
    uint64_t *var_5;
    uint64_t *var_6;
    uint32_t *var_7;
    uint64_t *var_8;
    uint64_t var_9;
    uint64_t var_10;
    uint64_t *var_11;
    uint64_t *var_12;
    uint64_t var_16;
    uint64_t var_40;
    uint64_t var_15;
    uint64_t rax_0;
    uint64_t var_13;
    uint64_t var_14;
    uint64_t *var_17;
    uint64_t local_sp_0;
    uint64_t var_18;
    uint64_t var_19;
    uint64_t var_20;
    uint64_t var_24;
    uint64_t var_25;
    uint64_t var_26;
    uint64_t var_32;
    uint64_t *var_33;
    uint32_t var_34;
    uint64_t var_35;
    uint64_t var_36;
    uint64_t var_37;
    uint64_t var_38;
    uint64_t var_39;
    uint64_t local_sp_1;
    uint64_t var_41;
    uint64_t var_42;
    uint64_t var_43;
    uint64_t var_44;
    uint32_t var_45;
    uint64_t var_46;
    uint64_t var_47;
    uint64_t var_48;
    uint64_t var_49;
    uint64_t var_50;
    uint32_t var_51;
    uint64_t var_52;
    uint32_t var_53;
    uint64_t var_27;
    uint64_t var_28;
    uint64_t var_29;
    uint32_t var_30;
    uint64_t var_31;
    uint64_t var_23;
    unsigned int loop_state_var;
    var_0 = revng_init_local_sp(0UL);
    var_1 = init_rbp();
    var_2 = init_cc_src2();
    var_3 = var_0 + (-8L);
    *(uint64_t *)var_3 = var_1;
    var_4 = var_0 + (-80L);
    var_5 = (uint64_t *)var_4;
    *var_5 = rdi;
    var_6 = (uint64_t *)(var_0 + (-88L));
    *var_6 = rsi;
    var_7 = (uint32_t *)(var_0 + (-92L));
    *var_7 = (uint32_t)rdx;
    var_8 = (uint64_t *)(var_0 + (-16L));
    *var_8 = 0UL;
    var_9 = *var_6;
    var_10 = var_0 + (-112L);
    *(uint64_t *)var_10 = 4245552UL;
    indirect_placeholder_2();
    var_11 = (uint64_t *)(var_0 + (-48L));
    *var_11 = var_9;
    var_12 = (uint64_t *)(var_0 + (-40L));
    *var_12 = var_9;
    var_16 = var_9;
    rax_0 = 0UL;
    local_sp_0 = var_10;
    var_18 = 0UL;
    var_24 = 0UL;
    if ((*var_7 & 4U) == 0U) {
        local_sp_1 = local_sp_0;
        if ((((var_16 <= 7UL) || (var_16 > 12UL)) ^ 1) && ((var_16 & 1UL) == 0UL)) {
            return rax_0;
        }
        var_17 = (uint64_t *)(var_0 + (-32L));
        *var_17 = 0UL;
        rax_0 = 1UL;
        while (1U)
            {
                var_19 = *var_12;
                var_20 = helper_cc_compute_c_wrapper(var_18 - var_19, var_19, var_2, 17U);
                if (var_20 == 0UL) {
                    var_21 = *var_6;
                    var_22 = *var_17;
                    if ((uint64_t)(((uint32_t)(uint64_t)*(unsigned char *)(var_22 + var_21) + (-48)) & (-2)) <= 9UL) {
                        loop_state_var = 0U;
                        break;
                    }
                    var_23 = var_22 + 1UL;
                    *var_17 = var_23;
                    var_18 = var_23;
                    continue;
                }
                *var_12 = (*var_12 >> 1UL);
                *var_17 = 0UL;
                loop_state_var = 1U;
                break;
            }
        var_25 = *var_12;
        var_26 = helper_cc_compute_c_wrapper(var_24 - var_25, var_25, var_2, 17U);
        while (var_26 != 0UL)
            {
                var_27 = *var_17;
                var_28 = var_27 << 1UL;
                var_29 = *var_6;
                var_30 = (uint32_t)(uint64_t)*(unsigned char *)(var_29 + var_28) + (-48);
                *(uint32_t *)(((var_27 << 2UL) + var_3) + (-64L)) = (((uint32_t)((uint64_t)(((uint32_t)((uint64_t)var_30 << 2UL) & (-4)) + var_30) << 1UL) + (uint32_t)(uint64_t)*(unsigned char *)(var_29 + (var_28 | 1UL))) + (-48));
                var_31 = *var_17 + 1UL;
                *var_17 = var_31;
                var_24 = var_31;
                var_25 = *var_12;
                var_26 = helper_cc_compute_c_wrapper(var_24 - var_25, var_25, var_2, 17U);
            }
        var_32 = var_0 + (-72L);
        var_33 = (uint64_t *)(var_0 + (-24L));
        *var_33 = var_32;
        var_34 = *var_7;
        var_40 = var_32;
        if ((var_34 & 1U) != 0U) {
            var_35 = *var_12 + (-4L);
            var_36 = (uint64_t)var_34;
            var_37 = *var_5;
            var_38 = local_sp_0 + (-8L);
            *(uint64_t *)var_38 = 4245898UL;
            var_39 = indirect_placeholder_32(var_35, var_36, var_32, var_37);
            local_sp_1 = var_38;
            if ((uint64_t)(unsigned char)var_39 != 1UL) {
                return rax_0;
            }
            *var_33 = (*var_33 + ((*var_12 << 2UL) + (-16L)));
            *var_12 = 4UL;
            var_40 = *var_33;
        }
        *var_33 = (var_40 + 4UL);
        *(uint32_t *)(*var_5 + 16UL) = (*(uint32_t *)var_40 + (-1));
        var_41 = *var_33;
        *var_33 = (var_41 + 4UL);
        *(uint32_t *)(*var_5 + 12UL) = *(uint32_t *)var_41;
        var_42 = *var_33;
        *var_33 = (var_42 + 4UL);
        *(uint32_t *)(*var_5 + 8UL) = *(uint32_t *)var_42;
        var_43 = *var_33;
        *var_33 = (var_43 + 4UL);
        *(uint32_t *)(*var_5 + 4UL) = *(uint32_t *)var_43;
        var_44 = *var_12 + (-4L);
        *var_12 = var_44;
        var_45 = *var_7;
        if ((var_45 & 1U) != 0U) {
            var_46 = (uint64_t)var_45;
            var_47 = *var_33;
            var_48 = *var_5;
            *(uint64_t *)(local_sp_1 + (-8L)) = 4246064UL;
            var_49 = indirect_placeholder_32(var_44, var_46, var_47, var_48);
            if ((uint64_t)(unsigned char)var_49 == 1UL) {
                return rax_0;
            }
        }
        var_50 = *var_8;
        if (var_50 == 0UL) {
            **(uint32_t **)var_4 = 0U;
        } else {
            var_51 = (uint32_t)(uint64_t)*(unsigned char *)(var_50 + 1UL) + (-48);
            var_52 = (uint64_t)var_51;
            var_53 = (uint32_t)(uint64_t)*(unsigned char *)(var_50 + 2UL);
            if ((uint64_t)(var_51 & (-2)) <= 9UL & (uint64_t)((var_53 + (-48)) & (-2)) > 9UL) {
                **(uint32_t **)var_4 = (((uint32_t)((uint64_t)(((uint32_t)(var_52 << 2UL) & (-4)) + var_51) << 1UL) + var_53) + (-48));
                rax_0 = 1UL;
            }
        }
    } else {
        var_13 = *var_6;
        var_14 = var_0 + (-120L);
        *(uint64_t *)var_14 = 4245591UL;
        indirect_placeholder_2();
        *var_8 = var_13;
        local_sp_0 = var_14;
        if (var_13 != 0UL) {
            var_15 = var_13 - *var_6;
            *var_12 = var_15;
            var_16 = var_15;
            if ((*var_11 - var_15) == 3UL) {
                return rax_0;
            }
        }
        var_16 = *var_12;
    }
}

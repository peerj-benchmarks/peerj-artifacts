typedef unsigned char __u_char;
typedef unsigned short __u_short;
typedef unsigned int __u_int;
typedef unsigned long __u_long;
typedef signed char __int8_t;
typedef unsigned char __uint8_t;
typedef short __int16_t;
typedef unsigned short __uint16_t;
typedef int __int32_t;
typedef unsigned int __uint32_t;
typedef long __int64_t;
typedef unsigned long __uint64_t;
typedef __int8_t __int_least8_t;
typedef __uint8_t __uint_least8_t;
typedef __int16_t __int_least16_t;
typedef __uint16_t __uint_least16_t;
typedef __int32_t __int_least32_t;
typedef __uint32_t __uint_least32_t;
typedef __int64_t __int_least64_t;
typedef __uint64_t __uint_least64_t;
typedef long __quad_t;
typedef unsigned long __u_quad_t;
typedef long __intmax_t;
typedef unsigned long __uintmax_t;
typedef unsigned long __dev_t;
typedef unsigned int __uid_t;
typedef unsigned int __gid_t;
typedef unsigned long __ino_t;
typedef unsigned long __ino64_t;
typedef unsigned int __mode_t;
typedef unsigned long __nlink_t;
typedef long __off_t;
typedef long __off64_t;
typedef int __pid_t;
typedef struct {
    int __val[2];
} __fsid_t;
typedef long __clock_t;
typedef unsigned long __rlim_t;
typedef unsigned long __rlim64_t;
typedef unsigned int __id_t;
typedef long __time_t;
typedef unsigned int __useconds_t;
typedef long __suseconds_t;
typedef int __daddr_t;
typedef int __key_t;
typedef int __clockid_t;
typedef void *__timer_t;
typedef long __blksize_t;
typedef long __blkcnt_t;
typedef long __blkcnt64_t;
typedef unsigned long __fsblkcnt_t;
typedef unsigned long __fsblkcnt64_t;
typedef unsigned long __fsfilcnt_t;
typedef unsigned long __fsfilcnt64_t;
typedef long __fsword_t;
typedef long __ssize_t;
typedef long __syscall_slong_t;
typedef unsigned long __syscall_ulong_t;
typedef __off64_t __loff_t;
typedef char *__caddr_t;
typedef long __intptr_t;
typedef unsigned int __socklen_t;
typedef int __sig_atomic_t;
typedef __int8_t int8_t;
typedef __int16_t int16_t;
typedef __int32_t int32_t;
typedef __int64_t int64_t;
typedef __uint8_t uint8_t;
typedef __uint16_t uint16_t;
typedef __uint32_t uint32_t;
typedef __uint64_t uint64_t;
typedef __int_least8_t int_least8_t;
typedef __int_least16_t int_least16_t;
typedef __int_least32_t int_least32_t;
typedef __int_least64_t int_least64_t;
typedef __uint_least8_t uint_least8_t;
typedef __uint_least16_t uint_least16_t;
typedef __uint_least32_t uint_least32_t;
typedef __uint_least64_t uint_least64_t;
typedef signed char int_fast8_t;
typedef long int_fast16_t;
typedef long int_fast32_t;
typedef long int_fast64_t;
typedef unsigned char uint_fast8_t;
typedef unsigned long uint_fast16_t;
typedef unsigned long uint_fast32_t;
typedef unsigned long uint_fast64_t;
typedef long intptr_t;
typedef unsigned long uintptr_t;
typedef __intmax_t intmax_t;
typedef __uintmax_t uintmax_t;
typedef _Bool bool;
struct indirect_placeholder_174_ret_type;
struct indirect_placeholder_175_ret_type;
struct indirect_placeholder_176_ret_type;
struct indirect_placeholder_173_ret_type;
struct indirect_placeholder_178_ret_type;
struct indirect_placeholder_177_ret_type;
struct indirect_placeholder_184_ret_type;
struct indirect_placeholder_183_ret_type;
struct indirect_placeholder_182_ret_type;
struct indirect_placeholder_181_ret_type;
struct indirect_placeholder_180_ret_type;
struct indirect_placeholder_179_ret_type;
struct indirect_placeholder_174_ret_type {
    uint64_t field_0;
    uint64_t field_1;
    uint64_t field_2;
    uint64_t field_3;
    uint64_t field_4;
    uint64_t field_5;
};
struct indirect_placeholder_175_ret_type {
    uint64_t field_0;
    uint64_t field_1;
    uint64_t field_2;
    uint64_t field_3;
    uint64_t field_4;
    uint64_t field_5;
};
struct indirect_placeholder_176_ret_type {
    uint64_t field_0;
    uint64_t field_1;
    uint64_t field_2;
    uint64_t field_3;
    uint64_t field_4;
    uint64_t field_5;
};
struct indirect_placeholder_173_ret_type {
    uint64_t field_0;
    uint64_t field_1;
    uint64_t field_2;
    uint64_t field_3;
    uint64_t field_4;
    uint64_t field_5;
};
struct indirect_placeholder_178_ret_type {
    uint64_t field_0;
    uint64_t field_1;
    uint64_t field_2;
    uint64_t field_3;
    uint64_t field_4;
    uint64_t field_5;
};
struct indirect_placeholder_177_ret_type {
    uint64_t field_0;
    uint64_t field_1;
    uint64_t field_2;
    uint64_t field_3;
    uint64_t field_4;
    uint64_t field_5;
};
struct indirect_placeholder_184_ret_type {
    uint64_t field_0;
    uint64_t field_1;
    uint64_t field_2;
    uint64_t field_3;
    uint64_t field_4;
    uint64_t field_5;
};
struct indirect_placeholder_183_ret_type {
    uint64_t field_0;
    uint64_t field_1;
    uint64_t field_2;
    uint64_t field_3;
    uint64_t field_4;
};
struct indirect_placeholder_182_ret_type {
    uint64_t field_0;
    uint64_t field_1;
    uint64_t field_2;
    uint64_t field_3;
    uint64_t field_4;
    uint64_t field_5;
};
struct indirect_placeholder_181_ret_type {
    uint64_t field_0;
    uint64_t field_1;
    uint64_t field_2;
    uint64_t field_3;
    uint64_t field_4;
};
struct indirect_placeholder_180_ret_type {
    uint64_t field_0;
    uint64_t field_1;
    uint64_t field_2;
    uint64_t field_3;
    uint64_t field_4;
    uint64_t field_5;
};
struct indirect_placeholder_179_ret_type {
    uint64_t field_0;
    uint64_t field_1;
    uint64_t field_2;
    uint64_t field_3;
    uint64_t field_4;
};
extern uint64_t revng_init_local_sp(uint64_t param_0);
extern uint64_t init_rbp(void);
extern struct indirect_placeholder_174_ret_type indirect_placeholder_174(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint64_t param_3, uint64_t param_4, uint64_t param_5, uint64_t param_6);
extern struct indirect_placeholder_175_ret_type indirect_placeholder_175(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint64_t param_3, uint64_t param_4, uint64_t param_5, uint64_t param_6);
extern struct indirect_placeholder_176_ret_type indirect_placeholder_176(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint64_t param_3, uint64_t param_4, uint64_t param_5, uint64_t param_6);
extern struct indirect_placeholder_173_ret_type indirect_placeholder_173(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint64_t param_3, uint64_t param_4, uint64_t param_5, uint64_t param_6);
extern struct indirect_placeholder_178_ret_type indirect_placeholder_178(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint64_t param_3, uint64_t param_4, uint64_t param_5, uint64_t param_6);
extern struct indirect_placeholder_177_ret_type indirect_placeholder_177(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint64_t param_3, uint64_t param_4, uint64_t param_5, uint64_t param_6);
extern struct indirect_placeholder_184_ret_type indirect_placeholder_184(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint64_t param_3, uint64_t param_4, uint64_t param_5, uint64_t param_6);
extern struct indirect_placeholder_183_ret_type indirect_placeholder_183(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint64_t param_3);
extern struct indirect_placeholder_182_ret_type indirect_placeholder_182(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint64_t param_3, uint64_t param_4, uint64_t param_5, uint64_t param_6);
extern struct indirect_placeholder_181_ret_type indirect_placeholder_181(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint64_t param_3);
extern struct indirect_placeholder_180_ret_type indirect_placeholder_180(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint64_t param_3, uint64_t param_4, uint64_t param_5, uint64_t param_6);
extern struct indirect_placeholder_179_ret_type indirect_placeholder_179(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint64_t param_3, uint64_t param_4, uint64_t param_5, uint64_t param_6);
void bb_debug_mktime_not_ok(uint64_t r9, uint64_t rdx, uint64_t rcx, uint64_t rsi, uint64_t rdi, uint64_t r8) {
    struct indirect_placeholder_174_ret_type var_70;
    uint64_t r91_0;
    uint64_t var_0;
    uint64_t var_1;
    uint64_t var_2;
    uint64_t var_3;
    uint64_t *var_4;
    uint64_t var_5;
    uint64_t *var_6;
    uint64_t *var_7;
    unsigned char *var_8;
    bool var_9;
    unsigned char *var_10;
    bool var_11;
    unsigned char *var_12;
    bool var_13;
    unsigned char *var_14;
    bool var_15;
    unsigned char *var_16;
    bool var_17;
    unsigned char *var_18;
    uint32_t var_19;
    uint64_t var_20;
    bool var_21;
    unsigned char *var_22;
    unsigned char var_23;
    uint64_t local_sp_0;
    uint64_t rsi4_1;
    uint64_t r86_1;
    uint64_t rdx2_0;
    uint64_t rcx3_0;
    uint64_t rsi4_0;
    uint64_t r86_0;
    uint64_t local_sp_1;
    uint64_t var_71;
    struct indirect_placeholder_175_ret_type var_72;
    uint64_t r91_1;
    uint64_t rdx2_1;
    uint64_t rcx3_1;
    uint64_t *var_42;
    struct indirect_placeholder_176_ret_type var_73;
    uint64_t var_74;
    uint64_t var_75;
    uint64_t var_76;
    uint64_t var_77;
    uint64_t storemerge;
    uint32_t var_55;
    uint32_t var_56;
    uint32_t var_57;
    struct indirect_placeholder_178_ret_type var_58;
    uint64_t var_59;
    uint64_t var_60;
    uint64_t var_61;
    uint64_t var_62;
    uint64_t var_63;
    struct indirect_placeholder_177_ret_type var_64;
    uint64_t var_65;
    uint64_t var_66;
    uint64_t var_67;
    uint64_t var_68;
    uint64_t var_69;
    unsigned char storemerge8;
    unsigned char *var_24;
    uint64_t var_25;
    uint64_t var_26;
    uint64_t var_27;
    struct indirect_placeholder_183_ret_type var_28;
    uint64_t var_29;
    uint64_t var_30;
    uint64_t var_31;
    uint64_t var_32;
    uint64_t var_33;
    uint64_t var_34;
    uint64_t var_35;
    struct indirect_placeholder_181_ret_type var_36;
    uint64_t var_37;
    uint64_t var_38;
    uint64_t var_39;
    uint64_t var_40;
    uint64_t var_41;
    uint64_t storemerge7;
    uint64_t spec_select;
    uint64_t storemerge5;
    uint64_t spec_select167;
    uint64_t storemerge3;
    uint64_t spec_select168;
    uint64_t var_43;
    uint64_t *var_44;
    uint64_t var_45;
    uint64_t *var_46;
    struct indirect_placeholder_179_ret_type var_47;
    uint64_t var_48;
    uint64_t var_49;
    uint64_t var_50;
    uint64_t var_51;
    uint64_t var_52;
    uint32_t *var_53;
    uint32_t var_54;
    var_0 = revng_init_local_sp(0UL);
    var_1 = init_rbp();
    var_2 = var_0 + (-8L);
    *(uint64_t *)var_2 = var_1;
    var_3 = var_0 + (-128L);
    var_4 = (uint64_t *)var_3;
    *var_4 = rdi;
    var_5 = var_0 + (-136L);
    var_6 = (uint64_t *)var_5;
    *var_6 = rsi;
    var_7 = (uint64_t *)(var_0 + (-144L));
    *var_7 = rdx;
    var_8 = (unsigned char *)(var_0 + (-148L));
    *var_8 = (unsigned char)rcx;
    var_9 = ((uint64_t)(**(uint32_t **)var_3 - **(uint32_t **)var_5) == 0UL);
    var_10 = (unsigned char *)(var_0 + (-13L));
    *var_10 = var_9;
    var_11 = ((uint64_t)(*(uint32_t *)(*var_4 + 4UL) - *(uint32_t *)(*var_6 + 4UL)) == 0UL);
    var_12 = (unsigned char *)(var_0 + (-14L));
    *var_12 = var_11;
    var_13 = ((uint64_t)(*(uint32_t *)(*var_4 + 8UL) - *(uint32_t *)(*var_6 + 8UL)) == 0UL);
    var_14 = (unsigned char *)(var_0 + (-15L));
    *var_14 = var_13;
    var_15 = ((uint64_t)(*(uint32_t *)(*var_4 + 12UL) - *(uint32_t *)(*var_6 + 12UL)) == 0UL);
    var_16 = (unsigned char *)(var_0 + (-16L));
    *var_16 = var_15;
    var_17 = ((uint64_t)(*(uint32_t *)(*var_4 + 16UL) - *(uint32_t *)(*var_6 + 16UL)) == 0UL);
    var_18 = (unsigned char *)(var_0 + (-17L));
    *var_18 = var_17;
    var_19 = *(uint32_t *)(*var_4 + 20UL);
    var_20 = (uint64_t)var_19;
    var_21 = ((uint64_t)(var_19 - *(uint32_t *)(*var_6 + 20UL)) == 0UL);
    var_22 = (unsigned char *)(var_0 + (-18L));
    var_23 = var_21;
    *var_22 = var_23;
    storemerge8 = (unsigned char)'\x00';
    if (*var_10 == '\x00') {
    }
    var_24 = (unsigned char *)(var_0 + (-19L));
    *var_24 = (storemerge8 & '\x01');
    if (*(unsigned char *)(*var_7 + 217UL) == '\x01') {
        return;
    }
    *(uint64_t *)(var_0 + (-160L)) = 4236131UL;
    indirect_placeholder_184(r9, 0UL, var_20, rcx, rsi, 4340952UL, r8);
    var_25 = var_0 + (-120L);
    var_26 = *var_7;
    var_27 = *var_4;
    *(uint64_t *)(var_0 + (-168L)) = 4236159UL;
    var_28 = indirect_placeholder_183(var_25, 100UL, var_26, var_27);
    var_29 = var_28.field_0;
    var_30 = var_28.field_1;
    var_31 = var_28.field_2;
    var_32 = var_28.field_3;
    var_33 = var_28.field_4;
    *(uint64_t *)(var_0 + (-176L)) = 4236177UL;
    indirect_placeholder_182(var_29, 0UL, var_31, var_32, var_30, 4340985UL, var_33);
    var_34 = *var_7;
    var_35 = *var_6;
    *(uint64_t *)(var_0 + (-184L)) = 4236205UL;
    var_36 = indirect_placeholder_181(var_25, 100UL, var_34, var_35);
    var_37 = var_36.field_0;
    var_38 = var_36.field_1;
    var_39 = var_36.field_2;
    var_40 = var_36.field_3;
    var_41 = var_36.field_4;
    *(uint64_t *)(var_0 + (-192L)) = 4236223UL;
    indirect_placeholder_180(var_37, 0UL, var_39, var_40, var_38, 4341015UL, var_41);
    storemerge7 = (*var_10 == '\x00') ? 4341045UL : 4335188UL;
    spec_select = (*var_12 == '\x00') ? 4341045UL : 4335188UL;
    storemerge5 = (*var_14 == '\x00') ? 4341045UL : 4335188UL;
    spec_select167 = (*var_16 == '\x00') ? 4341045UL : 4335188UL;
    storemerge3 = (*var_18 == '\x00') ? 4341045UL : 4335188UL;
    spec_select168 = (*var_22 == '\x00') ? 4341048UL : 4335188UL;
    var_42 = (uint64_t *)(var_0 + (-208L));
    *var_42 = storemerge7;
    var_43 = var_0 + (-216L);
    var_44 = (uint64_t *)var_43;
    *var_44 = spec_select;
    var_45 = var_0 + (-224L);
    var_46 = (uint64_t *)var_45;
    *var_46 = storemerge5;
    *(uint64_t *)(var_0 + (-232L)) = 4236372UL;
    var_47 = indirect_placeholder_179(spec_select167, 0UL, 4341056UL, spec_select168, 100UL, var_25, storemerge3);
    var_48 = var_47.field_0;
    var_49 = var_47.field_1;
    var_50 = var_47.field_2;
    var_51 = var_47.field_3;
    var_52 = var_47.field_4;
    var_53 = (uint32_t *)(var_0 + (-12L));
    var_54 = (uint32_t)var_49;
    *var_53 = var_54;
    local_sp_0 = var_43;
    var_55 = var_54;
    if ((int)var_54 >= (int)0U) {
        if (var_54 > 99U) {
            *var_53 = 99U;
            var_55 = 99U;
        }
        var_56 = var_55;
        while ((int)var_56 <= (int)0U)
            {
                if (*(unsigned char *)(((uint64_t)((long)(((uint64_t)var_56 << 32UL) + (-4294967296L)) >> (long)32UL) + var_2) + (-112L)) == ' ') {
                    break;
                }
                var_57 = var_56 + (-1);
                *var_53 = var_57;
                var_56 = var_57;
            }
        *(unsigned char *)((var_2 + (uint64_t)var_56) + (-112L)) = (unsigned char)'\x00';
    }
    *var_42 = 4236461UL;
    var_58 = indirect_placeholder_178(var_48, 0UL, var_50, var_51, var_25, 4341113UL, var_52);
    var_59 = var_58.field_0;
    var_60 = var_58.field_2;
    var_61 = var_58.field_3;
    var_62 = var_58.field_4;
    var_63 = var_58.field_5;
    *var_44 = 4236476UL;
    var_64 = indirect_placeholder_177(var_59, 0UL, var_60, var_61, var_62, 4341117UL, var_63);
    var_65 = var_64.field_0;
    var_66 = var_64.field_2;
    var_67 = var_64.field_3;
    var_68 = var_64.field_4;
    var_69 = var_64.field_5;
    r91_0 = var_65;
    rdx2_0 = var_66;
    rcx3_0 = var_67;
    rsi4_0 = var_68;
    r86_0 = var_69;
    if (*var_24 == '\x00') {
        *var_46 = 4236497UL;
        var_70 = indirect_placeholder_174(var_65, 0UL, var_66, var_67, var_68, 4341144UL, var_69);
        r91_0 = var_70.field_0;
        local_sp_0 = var_45;
        rdx2_0 = var_70.field_2;
        rcx3_0 = var_70.field_3;
        rsi4_0 = var_70.field_4;
        r86_0 = var_70.field_5;
    }
    rsi4_1 = rsi4_0;
    r86_1 = r86_0;
    local_sp_1 = local_sp_0;
    r91_1 = r91_0;
    rdx2_1 = rdx2_0;
    rcx3_1 = rcx3_0;
    if (*var_16 != '\x01' & *var_18 == '\x01') {
        var_71 = local_sp_0 + (-8L);
        *(uint64_t *)var_71 = 4236534UL;
        var_72 = indirect_placeholder_175(r91_0, 0UL, rdx2_0, rcx3_0, rsi4_0, 4341200UL, r86_0);
        rsi4_1 = var_72.field_4;
        r86_1 = var_72.field_5;
        local_sp_1 = var_71;
        r91_1 = var_72.field_0;
        rdx2_1 = var_72.field_2;
        rcx3_1 = var_72.field_3;
    }
    *(uint64_t *)(local_sp_1 + (-8L)) = 4236549UL;
    var_73 = indirect_placeholder_176(r91_1, 0UL, rdx2_1, rcx3_1, rsi4_1, 4341240UL, r86_1);
    var_74 = var_73.field_0;
    var_75 = var_73.field_2;
    var_76 = var_73.field_3;
    var_77 = var_73.field_5;
    storemerge = (*var_8 == '\x00') ? 4341292UL : 4341273UL;
    *(uint64_t *)(local_sp_1 + (-16L)) = 4236588UL;
    indirect_placeholder_173(var_74, 0UL, var_75, var_76, storemerge, 4341309UL, var_77);
    return;
}

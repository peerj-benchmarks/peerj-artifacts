typedef unsigned char __u_char;
typedef unsigned short __u_short;
typedef unsigned int __u_int;
typedef unsigned long __u_long;
typedef signed char __int8_t;
typedef unsigned char __uint8_t;
typedef short __int16_t;
typedef unsigned short __uint16_t;
typedef int __int32_t;
typedef unsigned int __uint32_t;
typedef long __int64_t;
typedef unsigned long __uint64_t;
typedef __int8_t __int_least8_t;
typedef __uint8_t __uint_least8_t;
typedef __int16_t __int_least16_t;
typedef __uint16_t __uint_least16_t;
typedef __int32_t __int_least32_t;
typedef __uint32_t __uint_least32_t;
typedef __int64_t __int_least64_t;
typedef __uint64_t __uint_least64_t;
typedef long __quad_t;
typedef unsigned long __u_quad_t;
typedef long __intmax_t;
typedef unsigned long __uintmax_t;
typedef unsigned long __dev_t;
typedef unsigned int __uid_t;
typedef unsigned int __gid_t;
typedef unsigned long __ino_t;
typedef unsigned long __ino64_t;
typedef unsigned int __mode_t;
typedef unsigned long __nlink_t;
typedef long __off_t;
typedef long __off64_t;
typedef int __pid_t;
typedef struct {
    int __val[2];
} __fsid_t;
typedef long __clock_t;
typedef unsigned long __rlim_t;
typedef unsigned long __rlim64_t;
typedef unsigned int __id_t;
typedef long __time_t;
typedef unsigned int __useconds_t;
typedef long __suseconds_t;
typedef int __daddr_t;
typedef int __key_t;
typedef int __clockid_t;
typedef void *__timer_t;
typedef long __blksize_t;
typedef long __blkcnt_t;
typedef long __blkcnt64_t;
typedef unsigned long __fsblkcnt_t;
typedef unsigned long __fsblkcnt64_t;
typedef unsigned long __fsfilcnt_t;
typedef unsigned long __fsfilcnt64_t;
typedef long __fsword_t;
typedef long __ssize_t;
typedef long __syscall_slong_t;
typedef unsigned long __syscall_ulong_t;
typedef __off64_t __loff_t;
typedef char *__caddr_t;
typedef long __intptr_t;
typedef unsigned int __socklen_t;
typedef int __sig_atomic_t;
typedef __int8_t int8_t;
typedef __int16_t int16_t;
typedef __int32_t int32_t;
typedef __int64_t int64_t;
typedef __uint8_t uint8_t;
typedef __uint16_t uint16_t;
typedef __uint32_t uint32_t;
typedef __uint64_t uint64_t;
typedef __int_least8_t int_least8_t;
typedef __int_least16_t int_least16_t;
typedef __int_least32_t int_least32_t;
typedef __int_least64_t int_least64_t;
typedef __uint_least8_t uint_least8_t;
typedef __uint_least16_t uint_least16_t;
typedef __uint_least32_t uint_least32_t;
typedef __uint_least64_t uint_least64_t;
typedef signed char int_fast8_t;
typedef long int_fast16_t;
typedef long int_fast32_t;
typedef long int_fast64_t;
typedef unsigned char uint_fast8_t;
typedef unsigned long uint_fast16_t;
typedef unsigned long uint_fast32_t;
typedef unsigned long uint_fast64_t;
typedef long intptr_t;
typedef unsigned long uintptr_t;
typedef __intmax_t intmax_t;
typedef __uintmax_t uintmax_t;
struct indirect_placeholder_199_ret_type;
struct indirect_placeholder_199_ret_type {
    uint64_t field_0;
    uint64_t field_1;
    uint64_t field_2;
};
extern uint64_t revng_init_local_sp(uint64_t param_0);
extern uint64_t init_rbp(void);
extern void indirect_placeholder_2(void);
extern uint64_t init_cc_src2(void);
extern uint64_t helper_cc_compute_c_wrapper(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint32_t param_3);
extern void indirect_placeholder_22(uint64_t param_0);
extern struct indirect_placeholder_199_ret_type indirect_placeholder_199(uint64_t param_0);
uint64_t bb_save_abbr(uint64_t rsi, uint64_t rdi) {
    uint64_t var_0;
    uint64_t var_1;
    uint64_t var_2;
    uint64_t var_3;
    uint64_t var_4;
    uint64_t *var_5;
    uint64_t *var_6;
    uint64_t var_7;
    uint64_t *var_8;
    uint64_t var_9;
    uint64_t *var_10;
    uint64_t var_11;
    uint64_t rax_0;
    unsigned char **var_15;
    uint64_t var_18;
    uint64_t var_21;
    uint64_t var_23;
    uint64_t var_22;
    uint64_t var_14;
    uint64_t var_32;
    uint64_t var_33;
    uint64_t var_34;
    struct indirect_placeholder_199_ret_type var_35;
    uint64_t var_36;
    uint64_t **var_37;
    uint64_t var_38;
    uint64_t var_16;
    uint64_t var_17;
    uint64_t var_24;
    uint64_t var_25;
    uint64_t *var_26;
    uint64_t var_27;
    uint64_t *var_28;
    uint64_t var_29;
    uint64_t var_30;
    uint64_t var_31;
    uint64_t var_19;
    uint64_t var_20;
    uint64_t var_12;
    uint64_t var_13;
    uint64_t local_sp_0;
    unsigned int loop_state_var;
    var_0 = revng_init_local_sp(0UL);
    var_1 = init_rbp();
    var_2 = init_cc_src2();
    *(uint64_t *)(var_0 + (-8L)) = var_1;
    var_3 = var_0 + (-56L);
    var_4 = var_0 + (-48L);
    var_5 = (uint64_t *)var_4;
    *var_5 = rdi;
    var_6 = (uint64_t *)var_3;
    *var_6 = rsi;
    var_7 = var_0 + (-24L);
    var_8 = (uint64_t *)var_7;
    *var_8 = 0UL;
    var_9 = var_0 + (-16L);
    var_10 = (uint64_t *)var_9;
    *var_10 = 4347136UL;
    var_11 = *(uint64_t *)(*var_6 + 48UL);
    *var_8 = var_11;
    local_sp_0 = var_3;
    rax_0 = 1UL;
    if (var_11 == 0UL) {
        return rax_0;
    }
    var_12 = *var_6;
    if (!!((var_12 <= var_11) && ((var_12 + 56UL) > var_11))) {
        return;
    }
    rax_0 = 0UL;
    if (**(unsigned char **)var_7 == '\x00') {
        *(uint64_t *)(*var_6 + 48UL) = *var_10;
        rax_0 = 1UL;
    } else {
        var_13 = *var_5 + 9UL;
        *var_10 = var_13;
        var_14 = var_13;
        while (1U)
            {
                *(uint64_t *)(local_sp_0 + (-8L)) = 4266101UL;
                indirect_placeholder_2();
                if ((uint64_t)(uint32_t)var_14 != 0UL) {
                    loop_state_var = 0U;
                    break;
                }
                var_15 = (unsigned char **)var_9;
                if (**var_15 == '\x00') {
                    var_16 = *var_5;
                    var_17 = var_16 + 9UL;
                    var_18 = var_17;
                    if (var_17 != *var_10) {
                        loop_state_var = 1U;
                        break;
                    }
                    if (*(unsigned char *)(var_16 + 8UL) != '\x00') {
                        loop_state_var = 1U;
                        break;
                    }
                }
                var_18 = *var_10;
                var_19 = local_sp_0 + (-16L);
                *(uint64_t *)var_19 = 4266028UL;
                indirect_placeholder_2();
                var_20 = *var_10 + (var_18 + 1UL);
                *var_10 = var_20;
                var_23 = var_20;
                local_sp_0 = var_19;
                var_21 = **(uint64_t **)var_4;
                if (**var_15 != '\x00' & var_21 == 0UL) {
                    *var_5 = var_21;
                    var_22 = var_21 + 9UL;
                    *var_10 = var_22;
                    var_23 = var_22;
                }
                var_14 = var_23;
                continue;
            }
        var_24 = *var_8;
        *(uint64_t *)(local_sp_0 + (-16L)) = 4265838UL;
        indirect_placeholder_2();
        var_25 = var_24 + 1UL;
        var_26 = (uint64_t *)(var_0 + (-32L));
        *var_26 = var_25;
        var_27 = *var_10 - (*var_5 + 9UL);
        var_28 = (uint64_t *)(var_0 + (-40L));
        *var_28 = var_27;
        var_29 = var_27 ^ (-1L);
        var_30 = *var_26;
        var_31 = helper_cc_compute_c_wrapper(var_29 - var_30, var_30, var_2, 17U);
        if (var_31 != 0UL) {
            *(uint64_t *)(local_sp_0 + (-24L)) = 4265883UL;
            indirect_placeholder_2();
            *(uint32_t *)var_29 = 12U;
            return rax_0;
        }
        var_32 = *var_28;
        var_33 = *var_26;
        if ((var_33 + var_32) > 118UL) {
            *(uint64_t *)(local_sp_0 + (-24L)) = 4265939UL;
            indirect_placeholder_22(var_33);
        } else {
            var_34 = *var_8;
            *(uint64_t *)(local_sp_0 + (-24L)) = 4265956UL;
            var_35 = indirect_placeholder_199(var_34);
            var_36 = var_35.field_0;
            var_37 = (uint64_t **)var_4;
            **var_37 = var_36;
            var_38 = **var_37;
            *var_5 = var_38;
            if (var_38 != 0UL) {
                return rax_0;
            }
            *(unsigned char *)(var_38 + 8UL) = (unsigned char)'\x00';
            *var_10 = (*var_5 + 9UL);
        }
    }
}

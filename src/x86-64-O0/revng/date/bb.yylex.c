typedef unsigned char __u_char;
typedef unsigned short __u_short;
typedef unsigned int __u_int;
typedef unsigned long __u_long;
typedef signed char __int8_t;
typedef unsigned char __uint8_t;
typedef short __int16_t;
typedef unsigned short __uint16_t;
typedef int __int32_t;
typedef unsigned int __uint32_t;
typedef long __int64_t;
typedef unsigned long __uint64_t;
typedef __int8_t __int_least8_t;
typedef __uint8_t __uint_least8_t;
typedef __int16_t __int_least16_t;
typedef __uint16_t __uint_least16_t;
typedef __int32_t __int_least32_t;
typedef __uint32_t __uint_least32_t;
typedef __int64_t __int_least64_t;
typedef __uint64_t __uint_least64_t;
typedef long __quad_t;
typedef unsigned long __u_quad_t;
typedef long __intmax_t;
typedef unsigned long __uintmax_t;
typedef unsigned long __dev_t;
typedef unsigned int __uid_t;
typedef unsigned int __gid_t;
typedef unsigned long __ino_t;
typedef unsigned long __ino64_t;
typedef unsigned int __mode_t;
typedef unsigned long __nlink_t;
typedef long __off_t;
typedef long __off64_t;
typedef int __pid_t;
typedef struct {
    int __val[2];
} __fsid_t;
typedef long __clock_t;
typedef unsigned long __rlim_t;
typedef unsigned long __rlim64_t;
typedef unsigned int __id_t;
typedef long __time_t;
typedef unsigned int __useconds_t;
typedef long __suseconds_t;
typedef int __daddr_t;
typedef int __key_t;
typedef int __clockid_t;
typedef void *__timer_t;
typedef long __blksize_t;
typedef long __blkcnt_t;
typedef long __blkcnt64_t;
typedef unsigned long __fsblkcnt_t;
typedef unsigned long __fsblkcnt64_t;
typedef unsigned long __fsfilcnt_t;
typedef unsigned long __fsfilcnt64_t;
typedef long __fsword_t;
typedef long __ssize_t;
typedef long __syscall_slong_t;
typedef unsigned long __syscall_ulong_t;
typedef __off64_t __loff_t;
typedef char *__caddr_t;
typedef long __intptr_t;
typedef unsigned int __socklen_t;
typedef int __sig_atomic_t;
typedef __int8_t int8_t;
typedef __int16_t int16_t;
typedef __int32_t int32_t;
typedef __int64_t int64_t;
typedef __uint8_t uint8_t;
typedef __uint16_t uint16_t;
typedef __uint32_t uint32_t;
typedef __uint64_t uint64_t;
typedef __int_least8_t int_least8_t;
typedef __int_least16_t int_least16_t;
typedef __int_least32_t int_least32_t;
typedef __int_least64_t int_least64_t;
typedef __uint_least8_t uint_least8_t;
typedef __uint_least16_t uint_least16_t;
typedef __uint_least32_t uint_least32_t;
typedef __uint_least64_t uint_least64_t;
typedef signed char int_fast8_t;
typedef long int_fast16_t;
typedef long int_fast32_t;
typedef long int_fast64_t;
typedef unsigned char uint_fast8_t;
typedef unsigned long uint_fast16_t;
typedef unsigned long uint_fast32_t;
typedef unsigned long uint_fast64_t;
typedef long intptr_t;
typedef unsigned long uintptr_t;
typedef __intmax_t intmax_t;
typedef __uintmax_t uintmax_t;
typedef _Bool bool;
struct bb_yylex_ret_type;
struct indirect_placeholder_136_ret_type;
struct indirect_placeholder_137_ret_type;
struct bb_yylex_ret_type {
    uint64_t field_0;
    uint64_t field_1;
    uint64_t field_2;
    uint64_t field_3;
};
struct indirect_placeholder_136_ret_type {
    uint64_t field_0;
    uint64_t field_1;
    uint64_t field_2;
    uint64_t field_3;
    uint64_t field_4;
    uint64_t field_5;
};
struct indirect_placeholder_137_ret_type {
    uint64_t field_0;
    uint64_t field_1;
    uint64_t field_2;
};
extern uint64_t revng_init_local_sp(uint64_t param_0);
extern uint64_t init_rbp(void);
extern uint64_t init_cc_src2(void);
extern uint64_t helper_cc_compute_c_wrapper(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint32_t param_3);
extern uint64_t indirect_placeholder_19(uint64_t param_0);
extern uint64_t init_r9(void);
extern uint64_t init_r8(void);
extern uint64_t init_rcx(void);
extern struct indirect_placeholder_136_ret_type indirect_placeholder_136(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint64_t param_3, uint64_t param_4, uint64_t param_5, uint64_t param_6);
extern struct indirect_placeholder_137_ret_type indirect_placeholder_137(uint64_t param_0, uint64_t param_1);
struct bb_yylex_ret_type bb_yylex(uint64_t rsi, uint64_t rdi) {
    uint64_t local_sp_8;
    uint64_t **var_25;
    uint64_t *var_26;
    uint64_t var_0;
    uint64_t var_1;
    uint64_t var_2;
    uint64_t var_3;
    uint64_t var_4;
    uint64_t var_5;
    uint64_t var_6;
    uint64_t var_7;
    uint64_t *var_8;
    uint64_t *var_9;
    unsigned char *var_10;
    uint64_t *var_11;
    uint64_t *var_12;
    uint32_t *var_13;
    uint64_t local_sp_9;
    uint64_t r9_1;
    struct indirect_placeholder_136_ret_type var_97;
    uint64_t var_98;
    uint64_t var_99;
    uint64_t var_100;
    uint64_t var_96;
    uint64_t var_90;
    struct indirect_placeholder_137_ret_type var_91;
    uint64_t var_92;
    uint64_t var_93;
    uint64_t var_94;
    uint64_t *var_95;
    uint64_t var_82;
    uint64_t var_78;
    uint64_t var_79;
    uint64_t *var_80;
    uint64_t var_81;
    uint64_t local_sp_0;
    uint64_t var_83;
    uint64_t var_84;
    uint64_t *var_85;
    unsigned char var_86;
    uint64_t var_87;
    uint64_t var_88;
    uint64_t var_89;
    uint64_t *var_101;
    uint64_t var_102;
    uint64_t var_103;
    uint64_t var_104;
    uint64_t var_105;
    uint64_t var_106;
    uint64_t *var_107;
    uint64_t var_108;
    uint64_t var_109;
    unsigned char var_110;
    uint64_t var_113;
    uint64_t var_112;
    uint64_t var_111;
    uint64_t local_sp_6;
    uint64_t var_74;
    uint64_t spec_select;
    uint64_t local_sp_4;
    uint64_t local_sp_3;
    uint64_t local_sp_2;
    uint64_t var_66;
    uint32_t var_67;
    uint32_t var_62;
    uint64_t var_56;
    uint64_t *var_57;
    uint64_t var_58;
    uint32_t var_59;
    uint32_t *var_60;
    uint32_t *var_61;
    uint64_t local_sp_1;
    struct bb_yylex_ret_type mrv2;
    struct bb_yylex_ret_type mrv3;
    uint64_t var_68;
    uint64_t var_69;
    uint64_t var_70;
    uint64_t var_71;
    uint64_t var_72;
    uint64_t var_73;
    uint64_t var_63;
    uint64_t var_64;
    uint64_t var_65;
    uint64_t var_53;
    uint64_t var_54;
    uint64_t var_55;
    uint64_t spec_select250;
    uint64_t var_51;
    uint64_t var_52;
    uint64_t local_sp_5;
    uint64_t rcx_1;
    uint64_t var_31;
    uint64_t var_32;
    uint64_t var_33;
    uint64_t **_pre_phi;
    unsigned char var_23;
    uint64_t var_75;
    uint64_t var_76;
    uint64_t var_77;
    unsigned char var_24;
    uint64_t var_34;
    uint64_t var_35;
    uint64_t *var_36;
    uint64_t local_sp_7;
    uint64_t var_37;
    uint64_t storemerge30;
    bool var_38;
    uint64_t var_39;
    bool var_40;
    unsigned char var_41;
    uint32_t var_42;
    bool var_43;
    uint64_t var_44;
    uint64_t storemerge32;
    uint64_t storemerge33;
    uint64_t storemerge38;
    bool var_45;
    uint64_t rax_0;
    uint64_t rcx_2;
    uint64_t r8_1;
    struct bb_yylex_ret_type mrv;
    struct bb_yylex_ret_type mrv1;
    unsigned char **var_46;
    unsigned char var_47;
    uint64_t var_48;
    uint64_t var_49;
    uint64_t var_50;
    unsigned char var_27;
    uint64_t var_28;
    uint64_t var_29;
    uint64_t var_30;
    uint64_t var_20;
    uint64_t var_21;
    uint64_t var_22;
    uint64_t *var_19;
    uint64_t local_sp_10;
    uint64_t rcx_3;
    unsigned char ***var_14;
    unsigned char var_15;
    uint64_t var_16;
    uint64_t var_17;
    uint64_t var_18;
    unsigned int loop_state_var;
    bool switch_state_var;
    var_0 = revng_init_local_sp(0UL);
    var_1 = init_r9();
    var_2 = init_rbp();
    var_3 = init_cc_src2();
    var_4 = init_rcx();
    var_5 = init_r8();
    *(uint64_t *)(var_0 + (-8L)) = var_2;
    var_6 = var_0 + (-120L);
    var_7 = var_0 + (-112L);
    var_8 = (uint64_t *)var_7;
    *var_8 = rdi;
    var_9 = (uint64_t *)var_6;
    *var_9 = rsi;
    var_10 = (unsigned char *)(var_0 + (-9L));
    var_11 = (uint64_t *)(var_0 + (-64L));
    var_12 = (uint64_t *)(var_0 + (-80L));
    var_13 = (uint32_t *)(var_0 + (-28L));
    local_sp_9 = var_6;
    r9_1 = var_1;
    var_62 = 2U;
    rax_0 = 63UL;
    r8_1 = var_5;
    rcx_3 = var_4;
    while (1U)
        {
            rcx_1 = rcx_3;
            rcx_2 = rcx_3;
            local_sp_10 = local_sp_9;
            var_14 = (unsigned char ***)var_6;
            var_15 = ***var_14;
            *var_10 = var_15;
            var_16 = (uint64_t)var_15;
            var_17 = local_sp_10 + (-8L);
            *(uint64_t *)var_17 = 4233782UL;
            var_18 = indirect_placeholder_19(var_16);
            local_sp_10 = var_17;
            while ((uint64_t)(unsigned char)var_18 != 0UL)
                {
                    var_19 = *(uint64_t **)var_6;
                    *var_19 = (*var_19 + 1UL);
                    var_14 = (unsigned char ***)var_6;
                    var_15 = ***var_14;
                    *var_10 = var_15;
                    var_16 = (uint64_t)var_15;
                    var_17 = local_sp_10 + (-8L);
                    *(uint64_t *)var_17 = 4233782UL;
                    var_18 = indirect_placeholder_19(var_16);
                    local_sp_10 = var_17;
                }
            var_20 = (uint64_t)*var_10;
            var_21 = local_sp_10 + (-16L);
            *(uint64_t *)var_21 = 4233797UL;
            var_22 = indirect_placeholder_19(var_20);
            local_sp_6 = var_21;
            local_sp_8 = var_21;
            if ((uint64_t)(unsigned char)var_22 == 0UL) {
                *var_13 = ((var_24 == '-') ? 4294967295U : 1U);
                var_25 = (uint64_t **)var_6;
                var_26 = *var_25;
                *var_26 = (*var_26 + 1UL);
                var_27 = ***var_14;
                *var_10 = var_27;
                var_28 = (uint64_t)var_27;
                var_29 = local_sp_8 + (-8L);
                *(uint64_t *)var_29 = 4233903UL;
                var_30 = indirect_placeholder_19(var_28);
                _pre_phi = var_25;
                local_sp_8 = var_29;
                do {
                    var_25 = (uint64_t **)var_6;
                    var_26 = *var_25;
                    *var_26 = (*var_26 + 1UL);
                    var_27 = ***var_14;
                    *var_10 = var_27;
                    var_28 = (uint64_t)var_27;
                    var_29 = local_sp_8 + (-8L);
                    *(uint64_t *)var_29 = 4233903UL;
                    var_30 = indirect_placeholder_19(var_28);
                    _pre_phi = var_25;
                    local_sp_8 = var_29;
                } while ((uint64_t)(unsigned char)var_30 != 0UL);
                var_31 = (uint64_t)*var_10;
                var_32 = local_sp_8 + (-16L);
                *(uint64_t *)var_32 = 4233918UL;
                var_33 = indirect_placeholder_19(var_31);
                local_sp_5 = var_32;
                local_sp_6 = var_32;
                if ((uint64_t)(unsigned char)var_33 != 1UL) {
                    loop_state_var = 2U;
                    switch_state_var = 1;
                    break;
                }
                local_sp_9 = local_sp_5;
                rcx_3 = rcx_1;
                continue;
            }
            break;
        }
    switch (loop_state_var) {
      case 1U:
        {
            mrv.field_0 = r9_1;
            mrv1 = mrv;
            mrv1.field_1 = rax_0;
            mrv2 = mrv1;
            mrv2.field_2 = rcx_2;
            mrv3 = mrv2;
            mrv3.field_3 = r8_1;
            return mrv3;
        }
        break;
      case 2U:
        {
            var_34 = **_pre_phi;
            var_35 = var_0 + (-24L);
            var_36 = (uint64_t *)var_35;
            *var_36 = var_34;
            local_sp_7 = local_sp_6;
            while (1U)
                {
                    var_37 = *var_12;
                    if ((long)var_37 > (long)18446744073709551615UL) {
                        storemerge30 = ((long)var_37 > (long)922337203685477580UL) | 922337203685477376UL;
                    } else {
                        storemerge30 = ((long)var_37 < (long)17524406870024074036UL) | (-922337203685477632L);
                    }
                    var_38 = ((uint64_t)(unsigned char)storemerge30 == 0UL);
                    var_39 = var_37 * 10UL;
                    *var_12 = var_39;
                    if (!var_38) {
                        loop_state_var = 2U;
                        break;
                    }
                    var_40 = ((int)*var_13 > (int)4294967295U);
                    var_41 = *var_10;
                    var_42 = (uint32_t)var_41;
                    var_43 = ((int)(var_40 ? (var_42 + (-48)) : (48U - var_42)) > (int)4294967295U);
                    var_44 = (uint64_t)var_41;
                    if (var_43) {
                        if (var_40) {
                            storemerge38 = 9223372036854775807UL - (uint64_t)((long)((var_44 << 32UL) + (-206158430208L)) >> (long)32UL);
                        } else {
                            storemerge38 = var_44 + 9223372036854775759UL;
                        }
                        storemerge33 = (storemerge38 & (-256L)) | ((long)storemerge38 < (long)var_39);
                    } else {
                        if (var_40) {
                            storemerge32 = 9223372036854775808UL - (-9223372036854775808L);
                        } else {
                            storemerge32 = var_44 + 9223372036854775760UL;
                        }
                        storemerge33 = (storemerge32 & (-256L)) | ((long)storemerge32 > (long)var_39);
                    }
                    var_45 = ((uint64_t)(unsigned char)storemerge33 == 0UL);
                    *var_12 = ((var_40 ? (var_44 + (-48L)) : (48UL - var_44)) + var_39);
                    if (!var_45) {
                        loop_state_var = 2U;
                        break;
                    }
                    *var_36 = (*var_36 + 1UL);
                    var_46 = (unsigned char **)var_35;
                    var_47 = **var_46;
                    *var_10 = var_47;
                    var_48 = (uint64_t)var_47;
                    var_49 = local_sp_7 + (-8L);
                    *(uint64_t *)var_49 = 4234408UL;
                    var_50 = indirect_placeholder_19(var_48);
                    local_sp_7 = var_49;
                    if ((uint64_t)(unsigned char)var_50 == 0UL) {
                        continue;
                    }
                    var_51 = (uint64_t)(uint32_t)(uint64_t)*(unsigned char *)(*var_36 + 1UL);
                    *(uint64_t *)(local_sp_7 + (-16L)) = 4234453UL;
                    var_52 = indirect_placeholder_19(var_51);
                    if ((uint64_t)(unsigned char)var_52 != 0UL) {
                        loop_state_var = 1U;
                        switch_state_var = 1;
                        break;
                    }
                    var_53 = *var_12;
                    var_54 = local_sp_7 + (-24L);
                    *(uint64_t *)var_54 = 4234473UL;
                    var_55 = indirect_placeholder_19(var_53);
                    local_sp_1 = var_54;
                    if ((uint64_t)(unsigned char)var_55 != 0UL) {
                        loop_state_var = 2U;
                        switch_state_var = 1;
                        break;
                    }
                    var_56 = *var_12;
                    var_57 = (uint64_t *)(var_0 + (-40L));
                    *var_57 = var_56;
                    var_58 = *var_36 + 1UL;
                    *var_36 = (var_58 + 1UL);
                    var_59 = (uint32_t)*(unsigned char *)var_58 + (-48);
                    var_60 = (uint32_t *)(var_0 + (-44L));
                    *var_60 = var_59;
                    var_61 = (uint32_t *)(var_0 + (-48L));
                    *var_61 = 2U;
                    loop_state_var = 0U;
                    switch_state_var = 1;
                    break;
                }
            switch (loop_state_var) {
              case 1U:
                {
                    **(unsigned char **)var_7 = (unsigned char)(*var_13 >> 31U);
                    *(uint64_t *)(*var_8 + 8UL) = *var_12;
                    *(uint64_t *)(*var_8 + 16UL) = (*var_36 - **_pre_phi);
                    **_pre_phi = *var_36;
                    spec_select250 = (*var_13 == 0U) ? 275UL : 274UL;
                    rax_0 = spec_select250;
                }
                break;
              case 0U:
                {
                    local_sp_2 = local_sp_1;
                    local_sp_3 = local_sp_1;
                    while ((int)var_62 <= (int)9U)
                        {
                            *var_60 = (*var_60 * 10U);
                            var_63 = (uint64_t)(uint32_t)(uint64_t)**var_46;
                            var_64 = local_sp_1 + (-8L);
                            *(uint64_t *)var_64 = 4234565UL;
                            var_65 = indirect_placeholder_19(var_63);
                            local_sp_1 = var_64;
                            if ((uint64_t)(unsigned char)var_65 == 0UL) {
                                var_66 = *var_36;
                                *var_36 = (var_66 + 1UL);
                                *var_60 = (*var_60 + ((uint32_t)*(unsigned char *)var_66 + (-48)));
                            }
                            var_67 = *var_61 + 1U;
                            *var_61 = var_67;
                            var_62 = var_67;
                            local_sp_2 = local_sp_1;
                            local_sp_3 = local_sp_1;
                        }
                    if ((int)*var_13 > (int)4294967295U) {
                        var_68 = (uint64_t)(uint32_t)(uint64_t)**var_46;
                        var_69 = local_sp_2 + (-8L);
                        *(uint64_t *)var_69 = 4234650UL;
                        var_70 = indirect_placeholder_19(var_68);
                        local_sp_2 = var_69;
                        local_sp_3 = var_69;
                        while ((uint64_t)(unsigned char)var_70 != 0UL)
                            {
                                if (**var_46 != '0') {
                                    *var_60 = (*var_60 + 1U);
                                    break;
                                }
                                *var_36 = (*var_36 + 1UL);
                                var_68 = (uint64_t)(uint32_t)(uint64_t)**var_46;
                                var_69 = local_sp_2 + (-8L);
                                *(uint64_t *)var_69 = 4234650UL;
                                var_70 = indirect_placeholder_19(var_68);
                                local_sp_2 = var_69;
                                local_sp_3 = var_69;
                            }
                    }
                    local_sp_4 = local_sp_3;
                    var_71 = (uint64_t)(uint32_t)(uint64_t)**var_46;
                    var_72 = local_sp_4 + (-8L);
                    *(uint64_t *)var_72 = 4234678UL;
                    var_73 = indirect_placeholder_19(var_71);
                    local_sp_4 = var_72;
                    while ((uint64_t)(unsigned char)var_73 != 0UL)
                        {
                            *var_36 = (*var_36 + 1UL);
                            var_71 = (uint64_t)(uint32_t)(uint64_t)**var_46;
                            var_72 = local_sp_4 + (-8L);
                            *(uint64_t *)var_72 = 4234678UL;
                            var_73 = indirect_placeholder_19(var_71);
                            local_sp_4 = var_72;
                        }
                    if ((int)*var_13 <= (int)4294967295U & *var_60 != 0U) {
                        var_74 = *var_57;
                        if (var_74 != 9223372036854775808UL) {
                            mrv.field_0 = r9_1;
                            mrv1 = mrv;
                            mrv1.field_1 = rax_0;
                            mrv2 = mrv1;
                            mrv2.field_2 = rcx_2;
                            mrv3 = mrv2;
                            mrv3.field_3 = r8_1;
                            return mrv3;
                        }
                        *var_57 = (var_74 + (-1L));
                        *var_60 = (1000000000U - *var_60);
                    }
                    **(uint64_t **)var_7 = *var_57;
                    *(uint64_t *)(*var_8 + 8UL) = (uint64_t)*var_60;
                    **_pre_phi = *var_36;
                    spec_select = (*var_13 == 0U) ? 277UL : 276UL;
                    rax_0 = spec_select;
                }
                break;
            }
        }
        break;
      case 0U:
        {
            while (1U)
                {
                    var_83 = helper_cc_compute_c_wrapper(var_82 - var_81, var_81, var_3, 17U);
                    if (var_83 == 0UL) {
                        var_84 = *var_80;
                        *var_80 = (var_84 + 1UL);
                        *(unsigned char *)var_84 = *var_10;
                    }
                    var_85 = *(uint64_t **)var_6;
                    *var_85 = (*var_85 + 1UL);
                    var_86 = ***var_14;
                    *var_10 = var_86;
                    var_87 = (uint64_t)var_86;
                    var_88 = local_sp_0 + (-8L);
                    *(uint64_t *)var_88 = 4234985UL;
                    var_89 = indirect_placeholder_19(var_87);
                    local_sp_0 = var_88;
                    if ((uint64_t)(unsigned char)var_89 != 0UL) {
                        if (*var_10 == '.') {
                            break;
                        }
                    }
                    var_82 = *var_80;
                    continue;
                }
            **(unsigned char **)var_79 = (unsigned char)'\x00';
            var_90 = *var_9;
            *(uint64_t *)(local_sp_0 + (-16L)) = 4235021UL;
            var_91 = indirect_placeholder_137(var_78, var_90);
            var_92 = var_91.field_0;
            var_93 = var_91.field_1;
            var_94 = var_91.field_2;
            var_95 = (uint64_t *)(var_0 + (-72L));
            *var_95 = var_92;
            rcx_2 = var_94;
            if (var_92 == 0UL) {
                **(uint64_t **)var_7 = (uint64_t)*(uint32_t *)(var_92 + 12UL);
                var_96 = (uint64_t)*(uint32_t *)(*var_95 + 8UL);
                rax_0 = var_96;
            } else {
                if (*(unsigned char *)(*var_9 + 217UL) == '\x00') {
                    *(uint64_t *)(local_sp_0 + (-24L)) = 4235069UL;
                    var_97 = indirect_placeholder_136(var_1, 0UL, var_93, var_94, var_78, 4340856UL, var_5);
                    var_98 = var_97.field_0;
                    var_99 = var_97.field_3;
                    var_100 = var_97.field_5;
                    r9_1 = var_98;
                    rcx_2 = var_99;
                    r8_1 = var_100;
                }
            }
        }
        break;
    }
}

typedef unsigned char __u_char;
typedef unsigned short __u_short;
typedef unsigned int __u_int;
typedef unsigned long __u_long;
typedef signed char __int8_t;
typedef unsigned char __uint8_t;
typedef short __int16_t;
typedef unsigned short __uint16_t;
typedef int __int32_t;
typedef unsigned int __uint32_t;
typedef long __int64_t;
typedef unsigned long __uint64_t;
typedef __int8_t __int_least8_t;
typedef __uint8_t __uint_least8_t;
typedef __int16_t __int_least16_t;
typedef __uint16_t __uint_least16_t;
typedef __int32_t __int_least32_t;
typedef __uint32_t __uint_least32_t;
typedef __int64_t __int_least64_t;
typedef __uint64_t __uint_least64_t;
typedef long __quad_t;
typedef unsigned long __u_quad_t;
typedef long __intmax_t;
typedef unsigned long __uintmax_t;
typedef unsigned long __dev_t;
typedef unsigned int __uid_t;
typedef unsigned int __gid_t;
typedef unsigned long __ino_t;
typedef unsigned long __ino64_t;
typedef unsigned int __mode_t;
typedef unsigned long __nlink_t;
typedef long __off_t;
typedef long __off64_t;
typedef int __pid_t;
typedef struct {
    int __val[2];
} __fsid_t;
typedef long __clock_t;
typedef unsigned long __rlim_t;
typedef unsigned long __rlim64_t;
typedef unsigned int __id_t;
typedef long __time_t;
typedef unsigned int __useconds_t;
typedef long __suseconds_t;
typedef int __daddr_t;
typedef int __key_t;
typedef int __clockid_t;
typedef void *__timer_t;
typedef long __blksize_t;
typedef long __blkcnt_t;
typedef long __blkcnt64_t;
typedef unsigned long __fsblkcnt_t;
typedef unsigned long __fsblkcnt64_t;
typedef unsigned long __fsfilcnt_t;
typedef unsigned long __fsfilcnt64_t;
typedef long __fsword_t;
typedef long __ssize_t;
typedef long __syscall_slong_t;
typedef unsigned long __syscall_ulong_t;
typedef __off64_t __loff_t;
typedef char *__caddr_t;
typedef long __intptr_t;
typedef unsigned int __socklen_t;
typedef int __sig_atomic_t;
typedef __int8_t int8_t;
typedef __int16_t int16_t;
typedef __int32_t int32_t;
typedef __int64_t int64_t;
typedef __uint8_t uint8_t;
typedef __uint16_t uint16_t;
typedef __uint32_t uint32_t;
typedef __uint64_t uint64_t;
typedef __int_least8_t int_least8_t;
typedef __int_least16_t int_least16_t;
typedef __int_least32_t int_least32_t;
typedef __int_least64_t int_least64_t;
typedef __uint_least8_t uint_least8_t;
typedef __uint_least16_t uint_least16_t;
typedef __uint_least32_t uint_least32_t;
typedef __uint_least64_t uint_least64_t;
typedef signed char int_fast8_t;
typedef long int_fast16_t;
typedef long int_fast32_t;
typedef long int_fast64_t;
typedef unsigned char uint_fast8_t;
typedef unsigned long uint_fast16_t;
typedef unsigned long uint_fast32_t;
typedef unsigned long uint_fast64_t;
typedef long intptr_t;
typedef unsigned long uintptr_t;
typedef __intmax_t intmax_t;
typedef __uintmax_t uintmax_t;
struct bb_ydhms_diff_ret_type;
struct indirect_placeholder_188_ret_type;
struct indirect_placeholder_193_ret_type;
struct indirect_placeholder_192_ret_type;
struct indirect_placeholder_191_ret_type;
struct indirect_placeholder_190_ret_type;
struct indirect_placeholder_189_ret_type;
struct bb_ydhms_diff_ret_type {
    uint64_t field_0;
    uint64_t field_1;
    uint64_t field_2;
    uint64_t field_3;
};
struct indirect_placeholder_188_ret_type {
    uint64_t field_0;
    uint64_t field_1;
    uint64_t field_2;
    uint64_t field_3;
    uint64_t field_4;
};
struct indirect_placeholder_193_ret_type {
    uint64_t field_0;
    uint64_t field_1;
    uint64_t field_2;
    uint64_t field_3;
    uint64_t field_4;
};
struct indirect_placeholder_192_ret_type {
    uint64_t field_0;
    uint64_t field_1;
    uint64_t field_2;
    uint64_t field_3;
    uint64_t field_4;
};
struct indirect_placeholder_191_ret_type {
    uint64_t field_0;
    uint64_t field_1;
    uint64_t field_2;
    uint64_t field_3;
    uint64_t field_4;
};
struct indirect_placeholder_190_ret_type {
    uint64_t field_0;
    uint64_t field_1;
    uint64_t field_2;
    uint64_t field_3;
    uint64_t field_4;
};
struct indirect_placeholder_189_ret_type {
    uint64_t field_0;
    uint64_t field_1;
    uint64_t field_2;
    uint64_t field_3;
    uint64_t field_4;
};
extern uint64_t revng_init_local_sp(uint64_t param_0);
extern uint64_t init_rbp(void);
extern uint64_t init_rbx(void);
extern struct indirect_placeholder_188_ret_type indirect_placeholder_188(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint64_t param_3, uint64_t param_4, uint64_t param_5);
extern struct indirect_placeholder_193_ret_type indirect_placeholder_193(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint64_t param_3, uint64_t param_4, uint64_t param_5);
extern struct indirect_placeholder_192_ret_type indirect_placeholder_192(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint64_t param_3, uint64_t param_4, uint64_t param_5);
extern struct indirect_placeholder_191_ret_type indirect_placeholder_191(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint64_t param_3, uint64_t param_4, uint64_t param_5);
extern struct indirect_placeholder_190_ret_type indirect_placeholder_190(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint64_t param_3, uint64_t param_4, uint64_t param_5);
extern struct indirect_placeholder_189_ret_type indirect_placeholder_189(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint64_t param_3, uint64_t param_4, uint64_t param_5);
struct bb_ydhms_diff_ret_type bb_ydhms_diff(uint64_t r10, uint64_t r9, uint64_t rdx, uint64_t rcx, uint64_t rsi, uint64_t rdi, uint64_t r8) {
    uint64_t var_0;
    uint64_t var_1;
    uint64_t var_2;
    uint64_t *var_3;
    uint64_t *var_4;
    uint32_t *var_5;
    uint32_t *var_6;
    uint32_t *var_7;
    uint32_t *var_8;
    uint64_t var_9;
    struct indirect_placeholder_188_ret_type var_10;
    uint64_t var_11;
    uint64_t var_12;
    uint64_t var_13;
    uint64_t var_14;
    struct indirect_placeholder_193_ret_type var_15;
    uint64_t var_16;
    uint64_t var_17;
    uint64_t var_18;
    uint64_t var_19;
    uint64_t var_20;
    uint32_t var_21;
    uint32_t *var_22;
    uint64_t var_23;
    struct indirect_placeholder_192_ret_type var_24;
    uint64_t var_25;
    uint64_t var_26;
    uint64_t var_27;
    uint64_t var_28;
    struct indirect_placeholder_191_ret_type var_29;
    uint64_t var_30;
    uint64_t var_31;
    uint64_t var_32;
    uint64_t var_33;
    uint64_t var_34;
    uint32_t var_35;
    uint32_t *var_36;
    uint32_t var_37;
    uint32_t var_38;
    uint32_t var_39;
    uint32_t *var_40;
    uint32_t var_41;
    uint32_t var_42;
    uint32_t var_43;
    uint32_t *var_44;
    uint64_t var_45;
    struct indirect_placeholder_190_ret_type var_46;
    uint64_t var_47;
    uint64_t var_48;
    uint64_t var_49;
    uint64_t var_50;
    uint64_t var_51;
    uint32_t *var_52;
    uint64_t var_53;
    struct indirect_placeholder_189_ret_type var_54;
    uint64_t var_55;
    uint64_t var_56;
    uint64_t var_57;
    uint32_t *var_58;
    uint32_t var_59;
    uint32_t var_60;
    uint32_t *var_61;
    uint64_t var_62;
    uint64_t var_63;
    uint64_t var_64;
    uint64_t var_65;
    uint64_t var_66;
    struct bb_ydhms_diff_ret_type mrv;
    struct bb_ydhms_diff_ret_type mrv1;
    struct bb_ydhms_diff_ret_type mrv2;
    struct bb_ydhms_diff_ret_type mrv3;
    var_0 = revng_init_local_sp(0UL);
    var_1 = init_rbp();
    var_2 = init_rbx();
    *(uint64_t *)(var_0 + (-8L)) = var_1;
    *(uint64_t *)(var_0 + (-16L)) = var_2;
    var_3 = (uint64_t *)(var_0 + (-104L));
    *var_3 = rdi;
    var_4 = (uint64_t *)(var_0 + (-112L));
    *var_4 = rsi;
    var_5 = (uint32_t *)(var_0 + (-116L));
    *var_5 = (uint32_t)rdx;
    var_6 = (uint32_t *)(var_0 + (-120L));
    *var_6 = (uint32_t)rcx;
    var_7 = (uint32_t *)(var_0 + (-124L));
    *var_7 = (uint32_t)r8;
    var_8 = (uint32_t *)(var_0 + (-128L));
    *var_8 = (uint32_t)r9;
    var_9 = *var_3;
    *(uint64_t *)(var_0 + (-136L)) = 4260513UL;
    var_10 = indirect_placeholder_188(r10, r9, 2UL, var_9, r8, var_2);
    var_11 = var_10.field_0;
    var_12 = var_10.field_1;
    var_13 = var_10.field_3;
    var_14 = (uint64_t)(uint32_t)var_10.field_2;
    *(uint64_t *)(var_0 + (-144L)) = 4260530UL;
    var_15 = indirect_placeholder_193(var_11, var_12, 2UL, 1900UL, var_13, var_14);
    var_16 = var_15.field_0;
    var_17 = var_15.field_1;
    var_18 = var_15.field_2;
    var_19 = var_15.field_3;
    var_20 = var_15.field_4;
    var_21 = ((uint32_t)var_18 + (uint32_t)var_20) + (uint32_t)((*var_3 & 3UL) == 0UL);
    var_22 = (uint32_t *)(var_0 + (-20L));
    *var_22 = var_21;
    var_23 = (uint64_t)*var_8;
    *(uint64_t *)(var_0 + (-152L)) = 4260574UL;
    var_24 = indirect_placeholder_192(var_16, var_17, 2UL, var_23, var_19, var_20);
    var_25 = var_24.field_0;
    var_26 = var_24.field_1;
    var_27 = var_24.field_3;
    var_28 = (uint64_t)(uint32_t)var_24.field_2;
    *(uint64_t *)(var_0 + (-160L)) = 4260591UL;
    var_29 = indirect_placeholder_191(var_25, var_26, 2UL, 1900UL, var_27, var_28);
    var_30 = var_29.field_0;
    var_31 = var_29.field_1;
    var_32 = var_29.field_2;
    var_33 = var_29.field_3;
    var_34 = var_29.field_4;
    var_35 = ((uint32_t)var_32 + (uint32_t)var_34) + (uint32_t)((*var_8 & 3U) == 0U);
    var_36 = (uint32_t *)(var_0 + (-24L));
    *var_36 = var_35;
    var_37 = *var_22;
    var_38 = (uint32_t)(uint64_t)((long)((uint64_t)var_37 * 1374389535UL) >> (long)35UL) - (uint32_t)(uint64_t)((long)((uint64_t)var_37 << 32UL) >> (long)63UL);
    var_39 = var_38 - (uint32_t)(((uint64_t)(((((uint32_t)((uint64_t)var_38 << 2UL) & (-4)) + var_38) * 4294967291U) + var_37) >> 31UL) & 1UL);
    var_40 = (uint32_t *)(var_0 + (-28L));
    *var_40 = var_39;
    var_41 = *var_36;
    var_42 = (uint32_t)(uint64_t)((long)((uint64_t)var_41 * 1374389535UL) >> (long)35UL) - (uint32_t)(uint64_t)((long)((uint64_t)var_41 << 32UL) >> (long)63UL);
    var_43 = var_42 - (uint32_t)(((uint64_t)(((((uint32_t)((uint64_t)var_42 << 2UL) & (-4)) + var_42) * 4294967291U) + var_41) >> 31UL) & 1UL);
    var_44 = (uint32_t *)(var_0 + (-32L));
    *var_44 = var_43;
    var_45 = (uint64_t)*var_40;
    *(uint64_t *)(var_0 + (-168L)) = 4260795UL;
    var_46 = indirect_placeholder_190(var_30, var_31, 2UL, var_45, var_33, var_34);
    var_47 = var_46.field_0;
    var_48 = var_46.field_1;
    var_49 = var_46.field_2;
    var_50 = var_46.field_3;
    var_51 = var_46.field_4;
    var_52 = (uint32_t *)(var_0 + (-36L));
    *var_52 = (uint32_t)var_49;
    var_53 = (uint64_t)*var_44;
    *(uint64_t *)(var_0 + (-176L)) = 4260816UL;
    var_54 = indirect_placeholder_189(var_47, var_48, 2UL, var_53, var_50, var_51);
    var_55 = var_54.field_0;
    var_56 = var_54.field_1;
    var_57 = var_54.field_3;
    var_58 = (uint32_t *)(var_0 + (-40L));
    var_59 = (uint32_t)var_54.field_2;
    *var_58 = var_59;
    var_60 = (*var_52 - var_59) + ((*var_22 - *var_36) + (*var_44 - *var_40));
    var_61 = (uint32_t *)(var_0 + (-44L));
    *var_61 = var_60;
    var_62 = *var_3 - (uint64_t)*var_8;
    *(uint64_t *)(var_0 + (-56L)) = var_62;
    var_63 = (((var_62 * 365UL) + *var_4) - (uint64_t)*(uint32_t *)(var_0 | 8UL)) + (uint64_t)*var_61;
    *(uint64_t *)(var_0 + (-64L)) = var_63;
    var_64 = ((var_63 * 24UL) + (uint64_t)*var_5) - (uint64_t)*(uint32_t *)(var_0 | 16UL);
    *(uint64_t *)(var_0 + (-72L)) = var_64;
    var_65 = ((var_64 * 60UL) + (uint64_t)*var_6) - (uint64_t)*(uint32_t *)(var_0 | 24UL);
    *(uint64_t *)(var_0 + (-80L)) = var_65;
    var_66 = ((var_65 * 60UL) + (uint64_t)*var_7) - (uint64_t)*(uint32_t *)(var_0 | 32UL);
    *(uint64_t *)(var_0 + (-88L)) = var_66;
    mrv.field_0 = var_55;
    mrv1 = mrv;
    mrv1.field_1 = var_56;
    mrv2 = mrv1;
    mrv2.field_2 = var_66;
    mrv3 = mrv2;
    mrv3.field_3 = var_57;
    return mrv3;
}

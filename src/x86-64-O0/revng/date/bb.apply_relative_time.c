typedef unsigned char __u_char;
typedef unsigned short __u_short;
typedef unsigned int __u_int;
typedef unsigned long __u_long;
typedef signed char __int8_t;
typedef unsigned char __uint8_t;
typedef short __int16_t;
typedef unsigned short __uint16_t;
typedef int __int32_t;
typedef unsigned int __uint32_t;
typedef long __int64_t;
typedef unsigned long __uint64_t;
typedef __int8_t __int_least8_t;
typedef __uint8_t __uint_least8_t;
typedef __int16_t __int_least16_t;
typedef __uint16_t __uint_least16_t;
typedef __int32_t __int_least32_t;
typedef __uint32_t __uint_least32_t;
typedef __int64_t __int_least64_t;
typedef __uint64_t __uint_least64_t;
typedef long __quad_t;
typedef unsigned long __u_quad_t;
typedef long __intmax_t;
typedef unsigned long __uintmax_t;
typedef unsigned long __dev_t;
typedef unsigned int __uid_t;
typedef unsigned int __gid_t;
typedef unsigned long __ino_t;
typedef unsigned long __ino64_t;
typedef unsigned int __mode_t;
typedef unsigned long __nlink_t;
typedef long __off_t;
typedef long __off64_t;
typedef int __pid_t;
typedef struct {
    int __val[2];
} __fsid_t;
typedef long __clock_t;
typedef unsigned long __rlim_t;
typedef unsigned long __rlim64_t;
typedef unsigned int __id_t;
typedef long __time_t;
typedef unsigned int __useconds_t;
typedef long __suseconds_t;
typedef int __daddr_t;
typedef int __key_t;
typedef int __clockid_t;
typedef void *__timer_t;
typedef long __blksize_t;
typedef long __blkcnt_t;
typedef long __blkcnt64_t;
typedef unsigned long __fsblkcnt_t;
typedef unsigned long __fsblkcnt64_t;
typedef unsigned long __fsfilcnt_t;
typedef unsigned long __fsfilcnt64_t;
typedef long __fsword_t;
typedef long __ssize_t;
typedef long __syscall_slong_t;
typedef unsigned long __syscall_ulong_t;
typedef __off64_t __loff_t;
typedef char *__caddr_t;
typedef long __intptr_t;
typedef unsigned int __socklen_t;
typedef int __sig_atomic_t;
typedef __int8_t int8_t;
typedef __int16_t int16_t;
typedef __int32_t int32_t;
typedef __int64_t int64_t;
typedef __uint8_t uint8_t;
typedef __uint16_t uint16_t;
typedef __uint32_t uint32_t;
typedef __uint64_t uint64_t;
typedef __int_least8_t int_least8_t;
typedef __int_least16_t int_least16_t;
typedef __int_least32_t int_least32_t;
typedef __int_least64_t int_least64_t;
typedef __uint_least8_t uint_least8_t;
typedef __uint_least16_t uint_least16_t;
typedef __uint_least32_t uint_least32_t;
typedef __uint_least64_t uint_least64_t;
typedef signed char int_fast8_t;
typedef long int_fast16_t;
typedef long int_fast32_t;
typedef long int_fast64_t;
typedef unsigned char uint_fast8_t;
typedef unsigned long uint_fast16_t;
typedef unsigned long uint_fast32_t;
typedef unsigned long uint_fast64_t;
typedef long intptr_t;
typedef unsigned long uintptr_t;
typedef __intmax_t intmax_t;
typedef __uintmax_t uintmax_t;
typedef _Bool bool;
struct bb_apply_relative_time_ret_type;
struct bb_apply_relative_time_ret_type {
    uint64_t field_0;
    uint64_t field_1;
    uint64_t field_2;
};
extern uint64_t revng_init_local_sp(uint64_t param_0);
extern uint64_t init_rbp(void);
extern uint64_t init_rcx(void);
struct bb_apply_relative_time_ret_type bb_apply_relative_time(uint64_t rsi, uint64_t rdi) {
    uint64_t var_0;
    uint64_t var_1;
    uint64_t var_2;
    uint64_t *var_3;
    uint32_t *var_4;
    uint32_t var_5;
    bool var_6;
    uint32_t var_7;
    bool var_8;
    uint64_t var_129;
    uint64_t var_9;
    uint32_t *var_10;
    uint32_t var_11;
    uint32_t *_pre_phi296;
    uint32_t *var_12;
    uint32_t var_13;
    uint32_t var_14;
    uint64_t storemerge;
    bool var_15;
    uint64_t _;
    uint64_t var_16;
    uint64_t var_33;
    uint64_t var_17;
    uint64_t *var_18;
    uint64_t var_19;
    uint64_t *_pre_phi298;
    uint64_t *var_20;
    uint64_t var_21;
    uint64_t var_22;
    uint64_t rcx_0;
    uint64_t storemerge2;
    bool var_23;
    uint64_t var_24;
    uint64_t var_25;
    uint64_t var_26;
    uint64_t *var_27;
    uint64_t var_28;
    uint64_t *_pre_phi300;
    uint64_t *var_29;
    uint64_t var_30;
    uint64_t var_31;
    uint64_t rcx_1;
    uint64_t storemerge4;
    bool var_32;
    uint64_t var_34;
    uint64_t var_35;
    uint64_t *var_36;
    uint64_t var_37;
    uint64_t *_pre_phi302;
    uint64_t *var_38;
    uint64_t var_39;
    uint64_t var_40;
    uint64_t rcx_2;
    uint64_t storemerge6;
    uint64_t var_80;
    bool var_41;
    uint64_t var_42;
    uint64_t var_43;
    uint64_t var_44;
    uint64_t *var_45;
    uint64_t var_46;
    uint64_t *_pre_phi304;
    uint64_t *var_47;
    uint64_t var_48;
    uint64_t var_49;
    uint64_t rcx_3;
    uint64_t storemerge8;
    bool var_50;
    uint64_t _272;
    uint64_t var_51;
    uint64_t var_52;
    uint64_t *var_53;
    uint64_t var_54;
    uint64_t *_pre_phi306;
    uint64_t *var_55;
    uint64_t var_56;
    uint64_t var_57;
    uint64_t rcx_4;
    uint64_t storemerge10;
    bool var_58;
    uint64_t _273;
    uint64_t _masked35;
    uint64_t var_59;
    bool var_124;
    uint64_t var_60;
    uint64_t *var_61;
    uint64_t var_62;
    uint64_t *_pre_phi308;
    uint64_t *var_63;
    uint64_t var_64;
    uint64_t var_65;
    uint64_t rcx_5;
    uint64_t storemerge12;
    bool var_66;
    uint64_t var_67;
    uint64_t rdx_2;
    uint32_t *var_68;
    uint32_t var_69;
    uint64_t var_70;
    uint64_t var_71;
    uint32_t *_pre_phi282;
    uint64_t var_72;
    uint32_t *var_73;
    uint32_t var_74;
    uint32_t var_75;
    uint64_t rcx_6;
    uint64_t storemerge16;
    bool var_76;
    uint64_t _275;
    uint64_t var_77;
    uint64_t *var_78;
    uint64_t var_79;
    uint64_t *_pre_phi284;
    uint64_t var_81;
    uint64_t *var_82;
    uint64_t var_83;
    uint64_t var_84;
    uint64_t rcx_7;
    uint64_t storemerge18;
    bool var_85;
    uint64_t var_86;
    uint64_t var_87;
    uint64_t *var_88;
    uint64_t var_89;
    uint64_t var_90;
    uint64_t *_pre_phi286;
    uint64_t var_91;
    uint64_t *var_92;
    uint64_t var_93;
    uint64_t var_94;
    uint64_t rcx_8;
    uint64_t storemerge20;
    bool var_95;
    uint64_t var_96;
    uint64_t var_97;
    uint64_t *var_98;
    uint64_t var_99;
    uint64_t var_100;
    uint64_t *_pre_phi288;
    uint64_t var_101;
    uint64_t *var_102;
    uint64_t var_103;
    uint64_t var_104;
    uint64_t rcx_9;
    uint64_t storemerge22;
    bool var_105;
    uint64_t var_106;
    uint64_t var_107;
    uint64_t *var_108;
    uint64_t var_109;
    uint64_t var_110;
    uint64_t *_pre_phi290;
    uint64_t var_111;
    uint64_t *var_112;
    uint64_t var_113;
    uint64_t var_114;
    uint64_t rcx_10;
    uint64_t storemerge24;
    bool var_115;
    uint64_t _279;
    uint64_t var_116;
    uint64_t *var_117;
    uint64_t var_118;
    uint64_t var_119;
    uint64_t *_pre_phi292;
    uint64_t var_120;
    uint64_t *var_121;
    uint64_t var_122;
    uint64_t var_123;
    uint64_t rcx_11;
    uint64_t storemerge26;
    uint64_t _280;
    uint64_t _masked40;
    uint64_t var_125;
    uint64_t *var_126;
    uint64_t var_127;
    uint64_t var_128;
    uint64_t *_pre_phi294;
    struct bb_apply_relative_time_ret_type mrv;
    struct bb_apply_relative_time_ret_type mrv1;
    struct bb_apply_relative_time_ret_type mrv2;
    uint64_t *var_130;
    uint64_t var_131;
    uint64_t var_132;
    uint64_t rcx_12;
    uint64_t storemerge28;
    bool var_133;
    uint64_t var_134;
    uint64_t rcx_13;
    uint64_t storemerge14_in_in;
    uint64_t storemerge15;
    var_0 = revng_init_local_sp(0UL);
    var_1 = init_rbp();
    var_2 = init_rcx();
    *(uint64_t *)(var_0 + (-8L)) = var_1;
    var_3 = (uint64_t *)(var_0 + (-16L));
    *var_3 = rdi;
    var_4 = (uint32_t *)(var_0 + (-20L));
    var_5 = (uint32_t)rsi;
    *var_4 = var_5;
    var_6 = ((int)var_5 > (int)4294967295U);
    var_7 = *(uint32_t *)(var_0 | 56UL);
    var_8 = ((int)var_7 > (int)4294967295U);
    rcx_6 = var_2;
    storemerge15 = 0UL;
    if (var_6) {
        if (var_8) {
            var_72 = (uint64_t)(2147483647U - var_7);
            var_73 = (uint32_t *)(*var_3 + 152UL);
            var_74 = *var_73;
            _pre_phi282 = var_73;
            var_75 = var_74;
            storemerge16 = (uint64_t)(var_74 & (-256)) | ((long)(var_72 << 32UL) < (long)((uint64_t)var_74 << 32UL));
        } else {
            var_68 = (uint32_t *)(*var_3 + 152UL);
            var_69 = *var_68;
            var_70 = (uint64_t)var_69;
            var_71 = 2147483648UL - (uint64_t)var_7;
            _pre_phi282 = var_68;
            var_75 = var_69;
            rcx_6 = (uint64_t)(uint32_t)var_71;
            storemerge16 = (uint64_t)(var_69 & (-256)) | ((long)(var_70 << 32UL) < (long)(var_71 << 32UL));
        }
        var_76 = ((uint64_t)(unsigned char)storemerge16 == 0UL);
        *_pre_phi282 = (var_75 + var_7);
        _275 = var_76 ? 0UL : 1UL;
        var_77 = *(uint64_t *)(var_0 | 48UL);
        rcx_7 = rcx_6;
        if ((long)var_77 > (long)18446744073709551615UL) {
            var_81 = 9223372036854775807UL - var_77;
            var_82 = (uint64_t *)(*var_3 + 144UL);
            var_83 = *var_82;
            _pre_phi284 = var_82;
            var_84 = var_83;
            storemerge18 = (var_83 & (-256L)) | ((long)var_81 < (long)var_83);
        } else {
            var_78 = (uint64_t *)(*var_3 + 144UL);
            var_79 = *var_78;
            var_80 = 9223372036854775808UL - (-9223372036854775808L);
            _pre_phi284 = var_78;
            var_84 = var_79;
            rcx_7 = var_80;
            storemerge18 = (var_79 & (-256L)) | ((long)var_79 < (long)var_80);
        }
        var_85 = ((uint64_t)(unsigned char)storemerge18 == 0UL);
        *_pre_phi284 = (var_77 + var_84);
        var_86 = _275 | (var_85 ? 0UL : 1UL);
        var_87 = *(uint64_t *)(var_0 | 40UL);
        rcx_8 = rcx_7;
        if ((long)var_87 > (long)18446744073709551615UL) {
            var_91 = 9223372036854775807UL - var_87;
            var_92 = (uint64_t *)(*var_3 + 136UL);
            var_93 = *var_92;
            _pre_phi286 = var_92;
            var_94 = var_93;
            storemerge20 = (var_93 & (-256L)) | ((long)var_91 < (long)var_93);
        } else {
            var_88 = (uint64_t *)(*var_3 + 136UL);
            var_89 = *var_88;
            var_90 = 9223372036854775808UL - (-9223372036854775808L);
            _pre_phi286 = var_88;
            var_94 = var_89;
            rcx_8 = var_90;
            storemerge20 = (var_89 & (-256L)) | ((long)var_89 < (long)var_90);
        }
        var_95 = ((uint64_t)(unsigned char)storemerge20 == 0UL);
        *_pre_phi286 = (var_87 + var_94);
        var_96 = var_86 | (var_95 ? 0UL : 1UL);
        var_97 = *(uint64_t *)(var_0 | 32UL);
        rcx_9 = rcx_8;
        if ((long)var_97 > (long)18446744073709551615UL) {
            var_101 = 9223372036854775807UL - var_97;
            var_102 = (uint64_t *)(*var_3 + 128UL);
            var_103 = *var_102;
            _pre_phi288 = var_102;
            var_104 = var_103;
            storemerge22 = (var_103 & (-256L)) | ((long)var_101 < (long)var_103);
        } else {
            var_98 = (uint64_t *)(*var_3 + 128UL);
            var_99 = *var_98;
            var_100 = 9223372036854775808UL - (-9223372036854775808L);
            _pre_phi288 = var_98;
            var_104 = var_99;
            rcx_9 = var_100;
            storemerge22 = (var_99 & (-256L)) | ((long)var_99 < (long)var_100);
        }
        var_105 = ((uint64_t)(unsigned char)storemerge22 == 0UL);
        *_pre_phi288 = (var_97 + var_104);
        var_106 = var_96 | (var_105 ? 0UL : 1UL);
        var_107 = *(uint64_t *)(var_0 | 24UL);
        rcx_10 = rcx_9;
        if ((long)var_107 > (long)18446744073709551615UL) {
            var_111 = 9223372036854775807UL - var_107;
            var_112 = (uint64_t *)(*var_3 + 120UL);
            var_113 = *var_112;
            _pre_phi290 = var_112;
            var_114 = var_113;
            storemerge24 = (var_113 & (-256L)) | ((long)var_111 < (long)var_113);
        } else {
            var_108 = (uint64_t *)(*var_3 + 120UL);
            var_109 = *var_108;
            var_110 = 9223372036854775808UL - (-9223372036854775808L);
            _pre_phi290 = var_108;
            var_114 = var_109;
            rcx_10 = var_110;
            storemerge24 = (var_109 & (-256L)) | ((long)var_109 < (long)var_110);
        }
        var_115 = ((uint64_t)(unsigned char)storemerge24 == 0UL);
        *_pre_phi290 = (var_107 + var_114);
        _279 = var_115 ? 0UL : 1UL;
        var_116 = *(uint64_t *)(var_0 | 16UL);
        rcx_11 = rcx_10;
        if ((long)var_116 > (long)18446744073709551615UL) {
            var_120 = 9223372036854775807UL - var_116;
            var_121 = (uint64_t *)(*var_3 + 112UL);
            var_122 = *var_121;
            _pre_phi292 = var_121;
            var_123 = var_122;
            storemerge26 = (var_122 & (-256L)) | ((long)var_120 < (long)var_122);
        } else {
            var_117 = (uint64_t *)(*var_3 + 112UL);
            var_118 = *var_117;
            var_119 = 9223372036854775808UL - (-9223372036854775808L);
            _pre_phi292 = var_117;
            var_123 = var_118;
            rcx_11 = var_119;
            storemerge26 = (var_118 & (-256L)) | ((long)var_118 < (long)var_119);
        }
        var_124 = ((uint64_t)(unsigned char)storemerge26 == 0UL);
        *_pre_phi292 = (var_116 + var_123);
        _280 = var_124 ? 0UL : 1UL;
        _masked40 = var_106 | _279;
        var_125 = *(uint64_t *)(var_0 | 8UL);
        rcx_12 = rcx_11;
        if ((long)var_125 > (long)18446744073709551615UL) {
            var_129 = 9223372036854775807UL - var_125;
            var_130 = (uint64_t *)(*var_3 + 104UL);
            var_131 = *var_130;
            _pre_phi294 = var_130;
            var_132 = var_131;
            storemerge28 = (var_131 & (-256L)) | ((long)var_129 < (long)var_131);
        } else {
            var_126 = (uint64_t *)(*var_3 + 104UL);
            var_127 = *var_126;
            var_128 = 9223372036854775808UL - (-9223372036854775808L);
            _pre_phi294 = var_126;
            var_132 = var_127;
            rcx_12 = var_128;
            storemerge28 = (var_127 & (-256L)) | ((long)var_127 < (long)var_128);
        }
        var_133 = ((uint64_t)(unsigned char)storemerge28 == 0UL);
        var_134 = var_125 + var_132;
        *_pre_phi294 = var_134;
        rdx_2 = var_134;
        rcx_13 = rcx_12;
        storemerge14_in_in = (var_133 ? 0UL : 1UL) | (_masked40 | _280);
    } else {
        if (var_8) {
            var_12 = (uint32_t *)(*var_3 + 152UL);
            var_13 = *var_12;
            _pre_phi296 = var_12;
            var_14 = var_13;
            storemerge = (uint64_t)(var_13 & (-256)) | ((long)((uint64_t)var_13 << 32UL) < (long)(((uint64_t)var_7 << 32UL) ^ (-9223372036854775808L)));
        } else {
            var_9 = (uint64_t)(var_7 + 2147483647U);
            var_10 = (uint32_t *)(*var_3 + 152UL);
            var_11 = *var_10;
            _pre_phi296 = var_10;
            var_14 = var_11;
            storemerge = (uint64_t)(var_11 & (-256)) | ((long)(var_9 << 32UL) < (long)((uint64_t)var_11 << 32UL));
        }
        var_15 = ((uint64_t)(unsigned char)storemerge == 0UL);
        *_pre_phi296 = (var_14 - var_7);
        _ = var_15 ? 0UL : 1UL;
        var_16 = *(uint64_t *)(var_0 | 48UL);
        rcx_0 = var_16;
        if ((long)var_16 > (long)18446744073709551615UL) {
            var_20 = (uint64_t *)(*var_3 + 144UL);
            var_21 = *var_20;
            _pre_phi298 = var_20;
            var_22 = var_21;
            storemerge2 = (var_21 & (-256L)) | ((long)var_21 < (long)(var_16 ^ (-9223372036854775808L)));
        } else {
            var_17 = var_16 + 9223372036854775807UL;
            var_18 = (uint64_t *)(*var_3 + 144UL);
            var_19 = *var_18;
            _pre_phi298 = var_18;
            var_22 = var_19;
            rcx_0 = var_2;
            storemerge2 = (var_19 & (-256L)) | ((long)var_17 < (long)var_19);
        }
        var_23 = ((uint64_t)(unsigned char)storemerge2 == 0UL);
        *_pre_phi298 = (var_22 - var_16);
        var_24 = _ | (var_23 ? 0UL : 1UL);
        var_25 = *(uint64_t *)(var_0 | 40UL);
        rcx_1 = var_25;
        if ((long)var_25 > (long)18446744073709551615UL) {
            var_29 = (uint64_t *)(*var_3 + 136UL);
            var_30 = *var_29;
            _pre_phi300 = var_29;
            var_31 = var_30;
            storemerge4 = (var_30 & (-256L)) | ((long)var_30 < (long)(var_25 ^ (-9223372036854775808L)));
        } else {
            var_26 = var_25 + 9223372036854775807UL;
            var_27 = (uint64_t *)(*var_3 + 136UL);
            var_28 = *var_27;
            _pre_phi300 = var_27;
            var_31 = var_28;
            rcx_1 = rcx_0;
            storemerge4 = (var_28 & (-256L)) | ((long)var_26 < (long)var_28);
        }
        var_32 = ((uint64_t)(unsigned char)storemerge4 == 0UL);
        *_pre_phi300 = (var_31 - var_25);
        var_33 = var_24 | (var_32 ? 0UL : 1UL);
        var_34 = *(uint64_t *)(var_0 | 32UL);
        rcx_2 = var_34;
        if ((long)var_34 > (long)18446744073709551615UL) {
            var_38 = (uint64_t *)(*var_3 + 128UL);
            var_39 = *var_38;
            _pre_phi302 = var_38;
            var_40 = var_39;
            storemerge6 = (var_39 & (-256L)) | ((long)var_39 < (long)(var_34 ^ (-9223372036854775808L)));
        } else {
            var_35 = var_34 + 9223372036854775807UL;
            var_36 = (uint64_t *)(*var_3 + 128UL);
            var_37 = *var_36;
            _pre_phi302 = var_36;
            var_40 = var_37;
            rcx_2 = rcx_1;
            storemerge6 = (var_37 & (-256L)) | ((long)var_35 < (long)var_37);
        }
        var_41 = ((uint64_t)(unsigned char)storemerge6 == 0UL);
        *_pre_phi302 = (var_40 - var_34);
        var_42 = var_33 | (var_41 ? 0UL : 1UL);
        var_43 = *(uint64_t *)(var_0 | 24UL);
        rcx_3 = var_43;
        if ((long)var_43 > (long)18446744073709551615UL) {
            var_47 = (uint64_t *)(*var_3 + 120UL);
            var_48 = *var_47;
            _pre_phi304 = var_47;
            var_49 = var_48;
            storemerge8 = (var_48 & (-256L)) | ((long)var_48 < (long)(var_43 ^ (-9223372036854775808L)));
        } else {
            var_44 = var_43 + 9223372036854775807UL;
            var_45 = (uint64_t *)(*var_3 + 120UL);
            var_46 = *var_45;
            _pre_phi304 = var_45;
            var_49 = var_46;
            rcx_3 = rcx_2;
            storemerge8 = (var_46 & (-256L)) | ((long)var_44 < (long)var_46);
        }
        var_50 = ((uint64_t)(unsigned char)storemerge8 == 0UL);
        *_pre_phi304 = (var_49 - var_43);
        _272 = var_50 ? 0UL : 1UL;
        var_51 = *(uint64_t *)(var_0 | 16UL);
        rcx_4 = var_51;
        if ((long)var_51 > (long)18446744073709551615UL) {
            var_55 = (uint64_t *)(*var_3 + 112UL);
            var_56 = *var_55;
            _pre_phi306 = var_55;
            var_57 = var_56;
            storemerge10 = (var_56 & (-256L)) | ((long)var_56 < (long)(var_51 ^ (-9223372036854775808L)));
        } else {
            var_52 = var_51 + 9223372036854775807UL;
            var_53 = (uint64_t *)(*var_3 + 112UL);
            var_54 = *var_53;
            _pre_phi306 = var_53;
            var_57 = var_54;
            rcx_4 = rcx_3;
            storemerge10 = (var_54 & (-256L)) | ((long)var_52 < (long)var_54);
        }
        var_58 = ((uint64_t)(unsigned char)storemerge10 == 0UL);
        *_pre_phi306 = (var_57 - var_51);
        _273 = var_58 ? 0UL : 1UL;
        _masked35 = var_42 | _272;
        var_59 = *(uint64_t *)(var_0 | 8UL);
        rcx_5 = var_59;
        if ((long)var_59 > (long)18446744073709551615UL) {
            var_63 = (uint64_t *)(*var_3 + 104UL);
            var_64 = *var_63;
            _pre_phi308 = var_63;
            var_65 = var_64;
            storemerge12 = (var_64 & (-256L)) | ((long)var_64 < (long)(var_59 ^ (-9223372036854775808L)));
        } else {
            var_60 = var_59 + 9223372036854775807UL;
            var_61 = (uint64_t *)(*var_3 + 104UL);
            var_62 = *var_61;
            _pre_phi308 = var_61;
            var_65 = var_62;
            rcx_5 = rcx_4;
            storemerge12 = (var_62 & (-256L)) | ((long)var_60 < (long)var_62);
        }
        var_66 = ((uint64_t)(unsigned char)storemerge12 == 0UL);
        var_67 = var_65 - var_59;
        *_pre_phi308 = var_67;
        rdx_2 = var_67;
        rcx_13 = rcx_5;
        storemerge14_in_in = (var_66 ? 0UL : 1UL) | (_masked35 | _273);
    }
    if (storemerge14_in_in != 0UL) {
        mrv.field_0 = storemerge15;
        mrv1 = mrv;
        mrv1.field_1 = rdx_2;
        mrv2 = mrv1;
        mrv2.field_2 = rcx_13;
        return mrv2;
    }
    *(unsigned char *)(*var_3 + 161UL) = (unsigned char)'\x01';
    storemerge15 = 1UL;
    mrv.field_0 = storemerge15;
    mrv1 = mrv;
    mrv1.field_1 = rdx_2;
    mrv2 = mrv1;
    mrv2.field_2 = rcx_13;
    return mrv2;
}

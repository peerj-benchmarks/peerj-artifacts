typedef unsigned char __u_char;
typedef unsigned short __u_short;
typedef unsigned int __u_int;
typedef unsigned long __u_long;
typedef signed char __int8_t;
typedef unsigned char __uint8_t;
typedef short __int16_t;
typedef unsigned short __uint16_t;
typedef int __int32_t;
typedef unsigned int __uint32_t;
typedef long __int64_t;
typedef unsigned long __uint64_t;
typedef __int8_t __int_least8_t;
typedef __uint8_t __uint_least8_t;
typedef __int16_t __int_least16_t;
typedef __uint16_t __uint_least16_t;
typedef __int32_t __int_least32_t;
typedef __uint32_t __uint_least32_t;
typedef __int64_t __int_least64_t;
typedef __uint64_t __uint_least64_t;
typedef long __quad_t;
typedef unsigned long __u_quad_t;
typedef long __intmax_t;
typedef unsigned long __uintmax_t;
typedef unsigned long __dev_t;
typedef unsigned int __uid_t;
typedef unsigned int __gid_t;
typedef unsigned long __ino_t;
typedef unsigned long __ino64_t;
typedef unsigned int __mode_t;
typedef unsigned long __nlink_t;
typedef long __off_t;
typedef long __off64_t;
typedef int __pid_t;
typedef struct {
    int __val[2];
} __fsid_t;
typedef long __clock_t;
typedef unsigned long __rlim_t;
typedef unsigned long __rlim64_t;
typedef unsigned int __id_t;
typedef long __time_t;
typedef unsigned int __useconds_t;
typedef long __suseconds_t;
typedef int __daddr_t;
typedef int __key_t;
typedef int __clockid_t;
typedef void *__timer_t;
typedef long __blksize_t;
typedef long __blkcnt_t;
typedef long __blkcnt64_t;
typedef unsigned long __fsblkcnt_t;
typedef unsigned long __fsblkcnt64_t;
typedef unsigned long __fsfilcnt_t;
typedef unsigned long __fsfilcnt64_t;
typedef long __fsword_t;
typedef long __ssize_t;
typedef long __syscall_slong_t;
typedef unsigned long __syscall_ulong_t;
typedef __off64_t __loff_t;
typedef char *__caddr_t;
typedef long __intptr_t;
typedef unsigned int __socklen_t;
typedef int __sig_atomic_t;
typedef __int8_t int8_t;
typedef __int16_t int16_t;
typedef __int32_t int32_t;
typedef __int64_t int64_t;
typedef __uint8_t uint8_t;
typedef __uint16_t uint16_t;
typedef __uint32_t uint32_t;
typedef __uint64_t uint64_t;
typedef __int_least8_t int_least8_t;
typedef __int_least16_t int_least16_t;
typedef __int_least32_t int_least32_t;
typedef __int_least64_t int_least64_t;
typedef __uint_least8_t uint_least8_t;
typedef __uint_least16_t uint_least16_t;
typedef __uint_least32_t uint_least32_t;
typedef __uint_least64_t uint_least64_t;
typedef signed char int_fast8_t;
typedef long int_fast16_t;
typedef long int_fast32_t;
typedef long int_fast64_t;
typedef unsigned char uint_fast8_t;
typedef unsigned long uint_fast16_t;
typedef unsigned long uint_fast32_t;
typedef unsigned long uint_fast64_t;
typedef long intptr_t;
typedef unsigned long uintptr_t;
typedef __intmax_t intmax_t;
typedef __uintmax_t uintmax_t;
typedef _Bool bool;
struct bb_time_zone_hhmm_ret_type;
struct bb_time_zone_hhmm_ret_type {
    uint64_t field_0;
    uint64_t field_1;
};
extern uint64_t revng_init_local_sp(uint64_t param_0);
extern uint64_t init_rbp(void);
extern uint64_t init_rcx(void);
struct bb_time_zone_hhmm_ret_type bb_time_zone_hhmm(uint64_t rsi, uint64_t rdi) {
    uint64_t var_0;
    uint64_t var_1;
    uint64_t var_2;
    uint64_t *var_3;
    uint64_t *var_4;
    unsigned char *var_5;
    uint64_t *var_6;
    bool var_7;
    uint64_t var_8;
    uint64_t var_9;
    uint64_t var_10;
    uint64_t var_11;
    unsigned char var_31;
    uint64_t storemerge1;
    bool var_12;
    uint64_t var_13;
    uint64_t *var_14;
    bool var_15;
    uint64_t var_16;
    bool var_17;
    uint64_t var_18;
    uint64_t var_19;
    uint64_t var_22;
    uint64_t var_20;
    uint64_t var_21;
    uint64_t storemerge3;
    bool var_23;
    uint64_t rax_0;
    uint64_t var_24;
    uint64_t var_25;
    uint64_t var_28;
    uint64_t var_26;
    uint64_t var_27;
    uint64_t storemerge5;
    bool var_29;
    unsigned char var_30;
    uint64_t rcx_0;
    uint64_t storemerge;
    uint64_t var_32;
    struct bb_time_zone_hhmm_ret_type mrv;
    struct bb_time_zone_hhmm_ret_type mrv1;
    var_0 = revng_init_local_sp(0UL);
    var_1 = init_rbp();
    var_2 = init_rcx();
    *(uint64_t *)(var_0 + (-8L)) = var_1;
    var_3 = (uint64_t *)(var_0 + (-32L));
    *var_3 = rdi;
    var_4 = (uint64_t *)(var_0 + (-40L));
    *var_4 = rsi;
    var_5 = (unsigned char *)(var_0 + (-9L));
    *var_5 = (unsigned char)'\x00';
    rcx_0 = var_2;
    storemerge = 0UL;
    if ((long)*(uint64_t *)(var_0 | 24UL) <= (long)2UL & (long)*var_4 > (long)18446744073709551615UL) {
        var_6 = (uint64_t *)(var_0 | 16UL);
        *var_6 = (*var_6 * 100UL);
    }
    var_7 = ((long)*var_4 > (long)18446744073709551615UL);
    var_8 = *(uint64_t *)(var_0 | 16UL);
    if (var_7) {
        var_9 = (uint64_t)((long)(var_8 + (uint64_t)(((unsigned __int128)var_8 * 18446744073709551615ULL) >> 64ULL)) >> (long)6UL) - (uint64_t)((long)var_8 >> (long)63UL);
        var_10 = var_9 * 60UL;
        var_11 = var_8 + (var_9 * 18446744073709551516UL);
        *(uint64_t *)(var_0 + (-24L)) = (var_11 + var_10);
        var_31 = *var_5;
        rcx_0 = var_11;
    } else {
        if ((long)var_8 > (long)18446744073709551615UL) {
            storemerge1 = ((long)var_8 > (long)153722867280912930UL) | 153722867280912896UL;
        } else {
            storemerge1 = ((long)var_8 < (long)18293021206428638686UL) | (-153722867280913152L);
        }
        var_12 = ((uint64_t)(unsigned char)storemerge1 == 0UL);
        var_13 = var_8 * 60UL;
        var_14 = (uint64_t *)(var_0 + (-24L));
        *var_14 = var_13;
        *var_5 = (((var_12 ? 0UL : 1UL) | (uint64_t)*var_5) != 0UL);
        var_15 = (*(unsigned char *)(var_0 | 8UL) == '\x00');
        var_16 = *var_4;
        var_17 = ((long)var_16 > (long)18446744073709551615UL);
        if (var_15) {
            if (var_17) {
                var_26 = 9223372036854775807UL - var_16;
                var_27 = *var_14;
                var_28 = var_27;
                storemerge5 = (var_27 & (-256L)) | ((long)var_26 < (long)var_27);
            } else {
                var_24 = 9223372036854775808UL - (-9223372036854775808L);
                var_25 = *var_14;
                var_28 = var_25;
                storemerge5 = (var_25 & (-256L)) | ((long)var_24 > (long)var_25);
            }
            var_29 = ((uint64_t)(unsigned char)storemerge5 == 0UL);
            *var_14 = (var_16 + var_28);
            rax_0 = var_29 ? 0UL : 1UL;
        } else {
            if (var_17) {
                var_20 = var_16 ^ (-9223372036854775808L);
                var_21 = *var_14;
                var_22 = var_21;
                storemerge3 = (var_21 & (-256L)) | ((long)var_20 > (long)var_21);
            } else {
                var_18 = var_16 + 9223372036854775807UL;
                var_19 = *var_14;
                var_22 = var_19;
                storemerge3 = (var_19 & (-256L)) | ((long)var_18 < (long)var_19);
            }
            var_23 = ((uint64_t)(unsigned char)storemerge3 == 0UL);
            *var_14 = (var_22 - var_16);
            rax_0 = var_23 ? 0UL : 1UL;
        }
        var_30 = ((rax_0 | (uint64_t)*var_5) != 0UL);
        *var_5 = var_30;
        var_31 = var_30;
    }
    var_32 = *(uint64_t *)(var_0 + (-24L));
    if (~(var_31 != '\x00' & ((long)var_32 < (long)18446744073709550176UL) || ((long)var_32 > (long)1440UL))) {
        mrv.field_0 = storemerge;
        mrv1 = mrv;
        mrv1.field_1 = rcx_0;
        return mrv1;
    }
    *(uint32_t *)(*var_3 + 24UL) = ((uint32_t)var_32 * 60U);
    storemerge = 1UL;
    mrv.field_0 = storemerge;
    mrv1 = mrv;
    mrv1.field_1 = rcx_0;
    return mrv1;
}

typedef unsigned char __u_char;
typedef unsigned short __u_short;
typedef unsigned int __u_int;
typedef unsigned long __u_long;
typedef signed char __int8_t;
typedef unsigned char __uint8_t;
typedef short __int16_t;
typedef unsigned short __uint16_t;
typedef int __int32_t;
typedef unsigned int __uint32_t;
typedef long __int64_t;
typedef unsigned long __uint64_t;
typedef __int8_t __int_least8_t;
typedef __uint8_t __uint_least8_t;
typedef __int16_t __int_least16_t;
typedef __uint16_t __uint_least16_t;
typedef __int32_t __int_least32_t;
typedef __uint32_t __uint_least32_t;
typedef __int64_t __int_least64_t;
typedef __uint64_t __uint_least64_t;
typedef long __quad_t;
typedef unsigned long __u_quad_t;
typedef long __intmax_t;
typedef unsigned long __uintmax_t;
typedef unsigned long __dev_t;
typedef unsigned int __uid_t;
typedef unsigned int __gid_t;
typedef unsigned long __ino_t;
typedef unsigned long __ino64_t;
typedef unsigned int __mode_t;
typedef unsigned long __nlink_t;
typedef long __off_t;
typedef long __off64_t;
typedef int __pid_t;
typedef struct {
    int __val[2];
} __fsid_t;
typedef long __clock_t;
typedef unsigned long __rlim_t;
typedef unsigned long __rlim64_t;
typedef unsigned int __id_t;
typedef long __time_t;
typedef unsigned int __useconds_t;
typedef long __suseconds_t;
typedef int __daddr_t;
typedef int __key_t;
typedef int __clockid_t;
typedef void *__timer_t;
typedef long __blksize_t;
typedef long __blkcnt_t;
typedef long __blkcnt64_t;
typedef unsigned long __fsblkcnt_t;
typedef unsigned long __fsblkcnt64_t;
typedef unsigned long __fsfilcnt_t;
typedef unsigned long __fsfilcnt64_t;
typedef long __fsword_t;
typedef long __ssize_t;
typedef long __syscall_slong_t;
typedef unsigned long __syscall_ulong_t;
typedef __off64_t __loff_t;
typedef char *__caddr_t;
typedef long __intptr_t;
typedef unsigned int __socklen_t;
typedef int __sig_atomic_t;
typedef __int8_t int8_t;
typedef __int16_t int16_t;
typedef __int32_t int32_t;
typedef __int64_t int64_t;
typedef __uint8_t uint8_t;
typedef __uint16_t uint16_t;
typedef __uint32_t uint32_t;
typedef __uint64_t uint64_t;
typedef __int_least8_t int_least8_t;
typedef __int_least16_t int_least16_t;
typedef __int_least32_t int_least32_t;
typedef __int_least64_t int_least64_t;
typedef __uint_least8_t uint_least8_t;
typedef __uint_least16_t uint_least16_t;
typedef __uint_least32_t uint_least32_t;
typedef __uint_least64_t uint_least64_t;
typedef signed char int_fast8_t;
typedef long int_fast16_t;
typedef long int_fast32_t;
typedef long int_fast64_t;
typedef unsigned char uint_fast8_t;
typedef unsigned long uint_fast16_t;
typedef unsigned long uint_fast32_t;
typedef unsigned long uint_fast64_t;
typedef long intptr_t;
typedef unsigned long uintptr_t;
typedef __intmax_t intmax_t;
typedef __uintmax_t uintmax_t;
typedef _Bool bool;
extern uint64_t revng_init_local_sp(uint64_t param_0);
extern uint64_t init_rbp(void);
extern uint64_t indirect_placeholder_2(uint64_t param_0, uint64_t param_1);
extern uint64_t init_cc_src2(void);
extern uint64_t helper_cc_compute_c_wrapper(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint32_t param_3);
extern void indirect_placeholder(void);
extern uint64_t indirect_placeholder_20(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint64_t param_3, uint64_t param_4);
extern uint64_t indirect_placeholder_31(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint64_t param_3);
extern uint64_t indirect_placeholder_1(uint64_t param_0);
uint64_t bb_split_3(uint64_t rdx, uint64_t rcx, uint64_t rsi, uint64_t rdi, uint64_t r8) {
    uint64_t var_82;
    uint64_t var_62;
    uint64_t var_0;
    uint64_t var_1;
    uint64_t var_2;
    uint64_t *var_3;
    uint64_t *var_4;
    uint64_t var_5;
    uint64_t *var_6;
    uint64_t var_7;
    uint64_t var_8;
    uint64_t *var_9;
    unsigned char *var_10;
    uint64_t *var_11;
    uint64_t var_36;
    uint64_t var_37;
    uint64_t rax_0;
    uint64_t var_38;
    uint64_t storemerge;
    uint64_t var_39;
    uint64_t var_40;
    uint64_t var_41;
    uint64_t var_42;
    uint64_t var_43;
    uint64_t var_44;
    uint64_t var_32;
    uint64_t var_33;
    uint64_t var_34;
    uint64_t var_35;
    uint64_t local_sp_1;
    uint64_t *var_63;
    uint64_t var_64;
    uint64_t var_65;
    uint64_t var_66;
    uint64_t var_67;
    uint64_t var_70;
    uint64_t var_68;
    uint64_t var_58;
    uint64_t var_59;
    uint64_t var_60;
    uint64_t var_61;
    uint64_t var_69;
    uint64_t local_sp_0;
    uint64_t var_71;
    uint64_t var_72;
    unsigned char *_cast25;
    unsigned char *_cast26_pre_phi;
    uint64_t var_73;
    uint64_t var_74;
    uint64_t var_75;
    uint64_t var_76;
    uint64_t var_77;
    uint64_t var_78;
    uint64_t var_79;
    uint64_t var_80;
    uint64_t var_81;
    uint64_t var_45;
    uint64_t var_46;
    uint64_t *var_47;
    uint64_t var_48;
    unsigned char var_49;
    uint64_t var_83;
    bool var_50;
    unsigned char *var_51;
    bool var_52;
    unsigned char *var_53;
    uint64_t var_54;
    uint64_t var_55;
    uint64_t var_56;
    uint64_t var_57;
    uint64_t var_18;
    uint64_t var_19;
    uint64_t var_20;
    uint64_t var_21;
    uint64_t var_22;
    uint64_t var_23;
    uint64_t **var_24;
    uint64_t var_25;
    uint64_t *var_26;
    uint64_t var_27;
    unsigned char var_28;
    uint64_t var_29;
    bool var_30;
    uint64_t var_31;
    uint64_t var_12;
    unsigned char var_13;
    uint64_t var_15;
    uint64_t *var_16;
    uint64_t var_17;
    uint64_t var_14;
    bool switch_state_var;
    var_0 = revng_init_local_sp(0UL);
    var_1 = init_rbp();
    var_2 = init_cc_src2();
    *(uint64_t *)(var_0 + (-8L)) = var_1;
    var_3 = (uint64_t *)(var_0 + (-80L));
    *var_3 = rdi;
    var_4 = (uint64_t *)(var_0 + (-88L));
    *var_4 = rsi;
    var_5 = var_0 + (-96L);
    var_6 = (uint64_t *)var_5;
    *var_6 = rdx;
    var_7 = var_0 + (-104L);
    *(uint64_t *)var_7 = rcx;
    var_8 = var_0 + (-112L);
    var_9 = (uint64_t *)var_8;
    *var_9 = r8;
    var_10 = (unsigned char *)(var_0 + (-9L));
    *var_10 = (unsigned char)'\x00';
    var_11 = (uint64_t *)(var_0 + (-24L));
    *var_11 = 0UL;
    rax_0 = 0UL;
    storemerge = 0UL;
    var_12 = 0UL;
    while (1U)
        {
            var_14 = var_12 + 1UL;
            *var_11 = var_14;
            var_12 = var_14;
            continue;
        }
    var_15 = (uint64_t)var_13;
    if ((uint64_t)(var_13 + '\xa4') == 0UL) {
        *var_11 = (var_12 + 1UL);
        *var_10 = (unsigned char)'\x01';
    }
    *(uint64_t *)(var_0 + (-128L)) = 4203541UL;
    indirect_placeholder();
    var_16 = (uint64_t *)(var_0 + (-40L));
    *var_16 = var_15;
    var_17 = var_0 + (-136L);
    *(uint64_t *)var_17 = 4203577UL;
    indirect_placeholder();
    local_sp_1 = var_17;
    if ((uint64_t)(uint32_t)var_15 == 0UL) {
        var_45 = *var_11 + *var_16;
        *var_11 = var_45;
        var_46 = *var_3 + (var_45 - *var_16);
        var_47 = (uint64_t *)(var_0 + (-48L));
        *var_47 = var_46;
        var_48 = *var_11;
        while (1U)
            {
                switch_state_var = 1;
                break;
            }
        var_50 = (var_49 == '-');
        var_51 = (unsigned char *)(var_0 + (-49L));
        *var_51 = var_50;
        var_52 = (*(unsigned char *)(*var_11 + *var_3) == '(');
        var_53 = (unsigned char *)(var_0 + (-50L));
        *var_53 = var_52;
        var_54 = *var_11;
        *var_11 = (var_54 + 1UL);
        *(unsigned char *)(var_54 + *var_3) = (unsigned char)'\x00';
        var_55 = *var_47;
        var_56 = var_0 + (-144L);
        *(uint64_t *)var_56 = 4203805UL;
        var_57 = indirect_placeholder_31(0UL, 0UL, 4307488UL, var_55);
        *(uint64_t *)(var_0 + (-64L)) = var_57;
        local_sp_0 = var_56;
        if ((long)var_57 <= (long)18446744073709551615UL) {
            *(uint32_t *)6423984UL = (uint32_t)var_57;
            if (*var_53 == '\x00') {
                var_58 = *var_11 + (-1L);
                *var_11 = var_58;
                *(unsigned char *)(var_58 + *var_3) = (unsigned char)'(';
            }
            if (*var_51 == '\x00') {
                var_69 = *(uint64_t *)(((uint64_t)*(uint32_t *)6423984UL << 3UL) + 6423600UL) << 3UL;
                *(uint64_t *)6423992UL = var_69;
                var_70 = var_69;
            } else {
                var_59 = *var_11 + *var_3;
                var_60 = var_0 + (-72L);
                var_61 = var_0 + (-152L);
                *(uint64_t *)var_61 = 4203911UL;
                var_62 = indirect_placeholder_20(0UL, var_60, 0UL, var_59, 0UL);
                local_sp_0 = var_61;
                if ((uint64_t)(uint32_t)var_62 == 0UL) {
                    return rax_0;
                }
                var_63 = (uint64_t *)var_60;
                var_64 = *var_63;
                if (var_64 == 0UL) {
                    return rax_0;
                }
                var_65 = helper_cc_compute_c_wrapper((*(uint64_t *)(((uint64_t)*(uint32_t *)6423984UL << 3UL) + 6423600UL) << 3UL) - var_64, var_64, var_2, 17U);
                if (var_65 == 0UL) {
                    return rax_0;
                }
                var_66 = *var_63;
                if ((var_66 & 7UL) == 0UL) {
                    return rax_0;
                }
                *(uint64_t *)6423992UL = var_66;
                var_67 = *var_11;
                while ((uint64_t)(((uint32_t)(uint64_t)*(unsigned char *)(var_67 + *var_3) + (-48)) & (-2)) <= 9UL)
                    {
                        var_68 = var_67 + 1UL;
                        *var_11 = var_68;
                        var_67 = var_68;
                    }
                var_70 = *(uint64_t *)6423992UL;
            }
            *(uint64_t *)6423968UL = (var_70 >> 2UL);
            var_71 = *var_3;
            var_72 = *var_11;
            _cast25 = (unsigned char *)(var_72 + var_71);
            _cast26_pre_phi = _cast25;
            var_74 = var_72;
            if (*_cast25 == ' ') {
                var_73 = var_72 + 1UL;
                *var_11 = var_73;
                _cast26_pre_phi = (unsigned char *)(var_73 + *var_3);
                var_74 = var_73;
            }
            if (*_cast26_pre_phi == '(') {
                *var_11 = (var_74 + 1UL);
                **(uint32_t **)var_7 = 0U;
                var_75 = (uint64_t)*var_10;
                var_76 = *var_4;
                var_77 = *var_11;
                var_78 = var_76 - var_77;
                var_79 = var_77 + *var_3;
                var_80 = *var_9;
                var_81 = *var_6;
                *(uint64_t *)(local_sp_0 + (-8L)) = 4204168UL;
                var_82 = indirect_placeholder_20(var_81, var_80, var_78, var_79, var_75);
                rax_0 = var_82;
            }
        }
    } else {
        var_18 = *var_4;
        var_19 = *var_11;
        var_20 = var_18 - var_19;
        var_21 = *(uint64_t *)6423960UL + (*(unsigned char *)(var_19 + *var_3) == '\\');
        var_22 = helper_cc_compute_c_wrapper(var_20 - var_21, var_21, var_2, 17U);
        if (var_22 != 0UL) {
            var_23 = *var_3 + *var_11;
            var_24 = (uint64_t **)var_5;
            **var_24 = var_23;
            var_25 = **var_24;
            var_26 = (uint64_t *)(var_0 + (-32L));
            *var_26 = var_25;
            *(uint64_t *)6423968UL = storemerge;
            var_27 = *var_26;
            *var_26 = (var_27 + 1UL);
            var_28 = *(unsigned char *)var_27;
            var_29 = local_sp_1 + (-8L);
            *(uint64_t *)var_29 = 4204324UL;
            indirect_placeholder();
            var_30 = (var_28 == '\x00');
            var_31 = *(uint64_t *)6423968UL;
            local_sp_1 = var_29;
            while (!var_30)
                {
                    storemerge = var_31 + 1UL;
                    *(uint64_t *)6423968UL = storemerge;
                    var_27 = *var_26;
                    *var_26 = (var_27 + 1UL);
                    var_28 = *(unsigned char *)var_27;
                    var_29 = local_sp_1 + (-8L);
                    *(uint64_t *)var_29 = 4204324UL;
                    indirect_placeholder();
                    var_30 = (var_28 == '\x00');
                    var_31 = *(uint64_t *)6423968UL;
                    local_sp_1 = var_29;
                }
            var_32 = helper_cc_compute_c_wrapper((*(uint64_t *)(((uint64_t)*(uint32_t *)6423984UL << 3UL) + 6423600UL) << 1UL) - var_31, var_31, var_2, 17U);
            if (!((var_31 > 1UL) && ((var_31 & 1UL) == 0UL)) & var_32 != 0UL) {
                *var_11 = (var_33 + 1UL);
                *(unsigned char *)(var_33 + *var_3) = (unsigned char)'\x00';
                var_34 = **var_24;
                *(uint64_t *)(local_sp_1 + (-16L)) = 4204510UL;
                var_35 = indirect_placeholder_1(var_34);
                if ((uint64_t)(unsigned char)var_35 != 1UL) {
                    var_36 = *var_4;
                    var_37 = *var_11;
                    rax_0 = 1UL;
                    if ((var_36 - var_37) == 1UL) {
                        if (*(uint32_t *)6423584UL == 1U) {
                            *(uint32_t *)6423584UL = 0U;
                            var_38 = *var_11;
                            *var_11 = (var_38 + 1UL);
                            **(uint32_t **)var_7 = (*(unsigned char *)(var_38 + *var_3) == '*');
                        }
                    } else {
                        rax_0 = 0UL;
                        if (*(uint32_t *)6423584UL == 0U) {
                            return rax_0;
                        }
                        *(uint32_t *)6423584UL = 1U;
                    }
                    **(uint64_t **)var_8 = (*var_3 + *var_11);
                    if (*var_10 == '\x00') {
                        var_39 = *var_4;
                        var_40 = *var_11;
                        var_41 = var_39 - var_40;
                        var_42 = var_40 + *var_3;
                        *(uint64_t *)(local_sp_1 + (-24L)) = 4204720UL;
                        var_43 = indirect_placeholder_2(var_41, var_42);
                        var_44 = (var_43 & (-256L)) | (var_43 != 0UL);
                        rax_0 = var_44;
                    }
                }
            }
        }
    }
}

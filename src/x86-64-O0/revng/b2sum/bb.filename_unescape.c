typedef unsigned char __u_char;
typedef unsigned short __u_short;
typedef unsigned int __u_int;
typedef unsigned long __u_long;
typedef signed char __int8_t;
typedef unsigned char __uint8_t;
typedef short __int16_t;
typedef unsigned short __uint16_t;
typedef int __int32_t;
typedef unsigned int __uint32_t;
typedef long __int64_t;
typedef unsigned long __uint64_t;
typedef __int8_t __int_least8_t;
typedef __uint8_t __uint_least8_t;
typedef __int16_t __int_least16_t;
typedef __uint16_t __uint_least16_t;
typedef __int32_t __int_least32_t;
typedef __uint32_t __uint_least32_t;
typedef __int64_t __int_least64_t;
typedef __uint64_t __uint_least64_t;
typedef long __quad_t;
typedef unsigned long __u_quad_t;
typedef long __intmax_t;
typedef unsigned long __uintmax_t;
typedef unsigned long __dev_t;
typedef unsigned int __uid_t;
typedef unsigned int __gid_t;
typedef unsigned long __ino_t;
typedef unsigned long __ino64_t;
typedef unsigned int __mode_t;
typedef unsigned long __nlink_t;
typedef long __off_t;
typedef long __off64_t;
typedef int __pid_t;
typedef struct {
    int __val[2];
} __fsid_t;
typedef long __clock_t;
typedef unsigned long __rlim_t;
typedef unsigned long __rlim64_t;
typedef unsigned int __id_t;
typedef long __time_t;
typedef unsigned int __useconds_t;
typedef long __suseconds_t;
typedef int __daddr_t;
typedef int __key_t;
typedef int __clockid_t;
typedef void *__timer_t;
typedef long __blksize_t;
typedef long __blkcnt_t;
typedef long __blkcnt64_t;
typedef unsigned long __fsblkcnt_t;
typedef unsigned long __fsblkcnt64_t;
typedef unsigned long __fsfilcnt_t;
typedef unsigned long __fsfilcnt64_t;
typedef long __fsword_t;
typedef long __ssize_t;
typedef long __syscall_slong_t;
typedef unsigned long __syscall_ulong_t;
typedef __off64_t __loff_t;
typedef char *__caddr_t;
typedef long __intptr_t;
typedef unsigned int __socklen_t;
typedef int __sig_atomic_t;
typedef __int8_t int8_t;
typedef __int16_t int16_t;
typedef __int32_t int32_t;
typedef __int64_t int64_t;
typedef __uint8_t uint8_t;
typedef __uint16_t uint16_t;
typedef __uint32_t uint32_t;
typedef __uint64_t uint64_t;
typedef __int_least8_t int_least8_t;
typedef __int_least16_t int_least16_t;
typedef __int_least32_t int_least32_t;
typedef __int_least64_t int_least64_t;
typedef __uint_least8_t uint_least8_t;
typedef __uint_least16_t uint_least16_t;
typedef __uint_least32_t uint_least32_t;
typedef __uint_least64_t uint_least64_t;
typedef signed char int_fast8_t;
typedef long int_fast16_t;
typedef long int_fast32_t;
typedef long int_fast64_t;
typedef unsigned char uint_fast8_t;
typedef unsigned long uint_fast16_t;
typedef unsigned long uint_fast32_t;
typedef unsigned long uint_fast64_t;
typedef long intptr_t;
typedef unsigned long uintptr_t;
typedef __intmax_t intmax_t;
typedef __uintmax_t uintmax_t;
typedef _Bool bool;
extern uint64_t revng_init_local_sp(uint64_t param_0);
extern uint64_t init_rbp(void);
extern uint64_t init_cc_src2(void);
extern uint64_t helper_cc_compute_c_wrapper(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint32_t param_3);
uint64_t bb_filename_unescape(uint64_t rsi, uint64_t rdi) {
    uint64_t var_0;
    uint64_t var_1;
    uint64_t var_2;
    uint64_t *var_3;
    uint64_t *var_4;
    uint64_t var_5;
    uint64_t var_6;
    uint64_t *var_7;
    uint64_t *var_8;
    uint64_t var_9;
    uint64_t var_10;
    uint64_t var_11;
    bool var_12;
    uint64_t var_13;
    uint64_t var_22;
    uint64_t rax_0;
    uint64_t var_14;
    uint32_t var_15;
    uint64_t var_17;
    uint32_t var_18;
    uint64_t var_19;
    uint64_t var_20;
    uint64_t var_16;
    uint64_t var_21;
    unsigned int loop_state_var;
    var_0 = revng_init_local_sp(0UL);
    var_1 = init_rbp();
    var_2 = init_cc_src2();
    *(uint64_t *)(var_0 + (-8L)) = var_1;
    var_3 = (uint64_t *)(var_0 + (-32L));
    *var_3 = rdi;
    var_4 = (uint64_t *)(var_0 + (-40L));
    *var_4 = rsi;
    var_5 = *var_3;
    var_6 = var_0 + (-16L);
    var_7 = (uint64_t *)var_6;
    *var_7 = var_5;
    var_8 = (uint64_t *)(var_0 + (-24L));
    *var_8 = 0UL;
    var_9 = 0UL;
    rax_0 = 0UL;
    while (1U)
        {
            var_10 = *var_4;
            var_11 = helper_cc_compute_c_wrapper(var_9 - var_10, var_10, var_2, 17U);
            var_12 = (var_11 == 0UL);
            var_13 = *var_3;
            var_22 = var_13;
            if (!var_12) {
                if ((*var_4 + var_13) <= *var_7) {
                    loop_state_var = 1U;
                    break;
                }
                **(unsigned char **)var_6 = (unsigned char)'\x00';
                var_22 = *var_3;
                loop_state_var = 1U;
                break;
            }
            var_14 = *var_8;
            var_15 = (uint32_t)(uint64_t)*(unsigned char *)(var_14 + var_13);
            if ((uint64_t)var_15 != 0UL) {
                loop_state_var = 0U;
                break;
            }
            if ((uint64_t)(var_15 + (-92)) == 0UL) {
                var_16 = *var_7;
                *var_7 = (var_16 + 1UL);
                *(unsigned char *)var_16 = *(unsigned char *)(*var_8 + *var_3);
            } else {
                if ((*var_4 + (-1L)) != var_14) {
                    loop_state_var = 0U;
                    break;
                }
                var_17 = var_14 + 1UL;
                *var_8 = var_17;
                var_18 = (uint32_t)(uint64_t)*(unsigned char *)(var_17 + *var_3);
                if ((uint64_t)(var_18 + (-92)) == 0UL) {
                    var_20 = *var_7;
                    *var_7 = (var_20 + 1UL);
                    *(unsigned char *)var_20 = (unsigned char)'\\';
                } else {
                    if ((uint64_t)(var_18 + (-110)) != 0UL) {
                        loop_state_var = 0U;
                        break;
                    }
                    var_19 = *var_7;
                    *var_7 = (var_19 + 1UL);
                    *(unsigned char *)var_19 = (unsigned char)'\n';
                }
            }
            var_21 = *var_8 + 1UL;
            *var_8 = var_21;
            var_9 = var_21;
            continue;
        }
    switch (loop_state_var) {
      case 0U:
        {
            return rax_0;
        }
        break;
      case 1U:
        {
            rax_0 = var_22;
        }
        break;
    }
}

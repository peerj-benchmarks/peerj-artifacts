typedef unsigned char __u_char;
typedef unsigned short __u_short;
typedef unsigned int __u_int;
typedef unsigned long __u_long;
typedef signed char __int8_t;
typedef unsigned char __uint8_t;
typedef short __int16_t;
typedef unsigned short __uint16_t;
typedef int __int32_t;
typedef unsigned int __uint32_t;
typedef long __int64_t;
typedef unsigned long __uint64_t;
typedef __int8_t __int_least8_t;
typedef __uint8_t __uint_least8_t;
typedef __int16_t __int_least16_t;
typedef __uint16_t __uint_least16_t;
typedef __int32_t __int_least32_t;
typedef __uint32_t __uint_least32_t;
typedef __int64_t __int_least64_t;
typedef __uint64_t __uint_least64_t;
typedef long __quad_t;
typedef unsigned long __u_quad_t;
typedef long __intmax_t;
typedef unsigned long __uintmax_t;
typedef unsigned long __dev_t;
typedef unsigned int __uid_t;
typedef unsigned int __gid_t;
typedef unsigned long __ino_t;
typedef unsigned long __ino64_t;
typedef unsigned int __mode_t;
typedef unsigned long __nlink_t;
typedef long __off_t;
typedef long __off64_t;
typedef int __pid_t;
typedef struct {
    int __val[2];
} __fsid_t;
typedef long __clock_t;
typedef unsigned long __rlim_t;
typedef unsigned long __rlim64_t;
typedef unsigned int __id_t;
typedef long __time_t;
typedef unsigned int __useconds_t;
typedef long __suseconds_t;
typedef int __daddr_t;
typedef int __key_t;
typedef int __clockid_t;
typedef void *__timer_t;
typedef long __blksize_t;
typedef long __blkcnt_t;
typedef long __blkcnt64_t;
typedef unsigned long __fsblkcnt_t;
typedef unsigned long __fsblkcnt64_t;
typedef unsigned long __fsfilcnt_t;
typedef unsigned long __fsfilcnt64_t;
typedef long __fsword_t;
typedef long __ssize_t;
typedef long __syscall_slong_t;
typedef unsigned long __syscall_ulong_t;
typedef __off64_t __loff_t;
typedef char *__caddr_t;
typedef long __intptr_t;
typedef unsigned int __socklen_t;
typedef int __sig_atomic_t;
typedef __int8_t int8_t;
typedef __int16_t int16_t;
typedef __int32_t int32_t;
typedef __int64_t int64_t;
typedef __uint8_t uint8_t;
typedef __uint16_t uint16_t;
typedef __uint32_t uint32_t;
typedef __uint64_t uint64_t;
typedef __int_least8_t int_least8_t;
typedef __int_least16_t int_least16_t;
typedef __int_least32_t int_least32_t;
typedef __int_least64_t int_least64_t;
typedef __uint_least8_t uint_least8_t;
typedef __uint_least16_t uint_least16_t;
typedef __uint_least32_t uint_least32_t;
typedef __uint_least64_t uint_least64_t;
typedef signed char int_fast8_t;
typedef long int_fast16_t;
typedef long int_fast32_t;
typedef long int_fast64_t;
typedef unsigned char uint_fast8_t;
typedef unsigned long uint_fast16_t;
typedef unsigned long uint_fast32_t;
typedef unsigned long uint_fast64_t;
typedef long intptr_t;
typedef unsigned long uintptr_t;
typedef __intmax_t intmax_t;
typedef __uintmax_t uintmax_t;
typedef _Bool bool;
struct indirect_placeholder_35_ret_type;
struct indirect_placeholder_39_ret_type;
struct indirect_placeholder_38_ret_type;
struct indirect_placeholder_37_ret_type;
struct indirect_placeholder_36_ret_type;
struct indirect_placeholder_40_ret_type;
struct indirect_placeholder_41_ret_type;
struct indirect_placeholder_42_ret_type;
struct indirect_placeholder_43_ret_type;
struct indirect_placeholder_44_ret_type;
struct indirect_placeholder_45_ret_type;
struct indirect_placeholder_46_ret_type;
struct indirect_placeholder_47_ret_type;
struct indirect_placeholder_48_ret_type;
struct indirect_placeholder_51_ret_type;
struct indirect_placeholder_50_ret_type;
struct indirect_placeholder_49_ret_type;
struct indirect_placeholder_54_ret_type;
struct indirect_placeholder_35_ret_type {
    uint64_t field_0;
    uint64_t field_1;
    uint64_t field_2;
    uint64_t field_3;
};
struct indirect_placeholder_39_ret_type {
    uint64_t field_0;
    uint64_t field_1;
    uint64_t field_2;
};
struct indirect_placeholder_38_ret_type {
    uint64_t field_0;
    uint64_t field_1;
    uint64_t field_2;
    uint64_t field_3;
};
struct indirect_placeholder_37_ret_type {
    uint64_t field_0;
    uint64_t field_1;
    uint64_t field_2;
};
struct indirect_placeholder_36_ret_type {
    uint64_t field_0;
    uint64_t field_1;
    uint64_t field_2;
    uint64_t field_3;
};
struct indirect_placeholder_40_ret_type {
    uint64_t field_0;
    uint64_t field_1;
    uint64_t field_2;
    uint64_t field_3;
};
struct indirect_placeholder_41_ret_type {
    uint64_t field_0;
    uint64_t field_1;
    uint64_t field_2;
    uint64_t field_3;
};
struct indirect_placeholder_42_ret_type {
    uint64_t field_0;
    uint64_t field_1;
    uint64_t field_2;
    uint64_t field_3;
};
struct indirect_placeholder_43_ret_type {
    uint64_t field_0;
    uint64_t field_1;
    uint64_t field_2;
    uint64_t field_3;
};
struct indirect_placeholder_44_ret_type {
    uint64_t field_0;
    uint64_t field_1;
    uint64_t field_2;
    uint64_t field_3;
};
struct indirect_placeholder_45_ret_type {
    uint64_t field_0;
    uint64_t field_1;
    uint64_t field_2;
    uint64_t field_3;
};
struct indirect_placeholder_46_ret_type {
    uint64_t field_0;
    uint64_t field_1;
    uint64_t field_2;
    uint64_t field_3;
};
struct indirect_placeholder_47_ret_type {
    uint64_t field_0;
    uint64_t field_1;
    uint64_t field_2;
    uint64_t field_3;
};
struct indirect_placeholder_48_ret_type {
    uint64_t field_0;
    uint64_t field_1;
    uint64_t field_2;
    uint64_t field_3;
    uint64_t field_4;
};
struct indirect_placeholder_51_ret_type {
    uint64_t field_0;
    uint64_t field_1;
    uint64_t field_2;
};
struct indirect_placeholder_50_ret_type {
    uint64_t field_0;
    uint64_t field_1;
    uint64_t field_2;
    uint64_t field_3;
};
struct indirect_placeholder_49_ret_type {
    uint64_t field_0;
    uint64_t field_1;
    uint64_t field_2;
    uint64_t field_3;
};
struct indirect_placeholder_54_ret_type {
    uint64_t field_0;
    uint64_t field_1;
    uint64_t field_2;
    uint64_t field_3;
};
extern void abort(void);
extern uint64_t revng_init_local_sp(uint64_t param_0);
extern uint64_t init_rbp(void);
extern uint64_t init_cc_src2(void);
extern uint64_t helper_cc_compute_c_wrapper(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint32_t param_3);
extern void indirect_placeholder(void);
extern uint64_t indirect_placeholder_20(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint64_t param_3, uint64_t param_4);
extern uint64_t init_rbx(void);
extern void indirect_placeholder_16(uint64_t param_0, uint64_t param_1);
extern uint64_t indirect_placeholder_13(void);
extern void indirect_placeholder_32(uint64_t param_0);
extern uint64_t indirect_placeholder_53(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint64_t param_3, uint64_t param_4, uint64_t param_5);
extern uint64_t indirect_placeholder_1(uint64_t param_0);
extern uint64_t indirect_placeholder_3(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint64_t param_3, uint64_t param_4, uint64_t param_5, uint64_t param_6);
extern struct indirect_placeholder_35_ret_type indirect_placeholder_35(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint64_t param_3, uint64_t param_4, uint64_t param_5, uint64_t param_6);
extern struct indirect_placeholder_39_ret_type indirect_placeholder_39(uint64_t param_0);
extern struct indirect_placeholder_38_ret_type indirect_placeholder_38(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint64_t param_3, uint64_t param_4, uint64_t param_5, uint64_t param_6);
extern struct indirect_placeholder_37_ret_type indirect_placeholder_37(uint64_t param_0);
extern struct indirect_placeholder_36_ret_type indirect_placeholder_36(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint64_t param_3, uint64_t param_4, uint64_t param_5, uint64_t param_6);
extern struct indirect_placeholder_40_ret_type indirect_placeholder_40(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint64_t param_3, uint64_t param_4, uint64_t param_5, uint64_t param_6);
extern struct indirect_placeholder_41_ret_type indirect_placeholder_41(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint64_t param_3, uint64_t param_4, uint64_t param_5, uint64_t param_6);
extern struct indirect_placeholder_42_ret_type indirect_placeholder_42(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint64_t param_3, uint64_t param_4, uint64_t param_5, uint64_t param_6);
extern struct indirect_placeholder_43_ret_type indirect_placeholder_43(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint64_t param_3, uint64_t param_4, uint64_t param_5, uint64_t param_6);
extern struct indirect_placeholder_44_ret_type indirect_placeholder_44(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint64_t param_3, uint64_t param_4, uint64_t param_5, uint64_t param_6);
extern struct indirect_placeholder_45_ret_type indirect_placeholder_45(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint64_t param_3, uint64_t param_4, uint64_t param_5, uint64_t param_6);
extern struct indirect_placeholder_46_ret_type indirect_placeholder_46(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint64_t param_3, uint64_t param_4, uint64_t param_5, uint64_t param_6);
extern struct indirect_placeholder_47_ret_type indirect_placeholder_47(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint64_t param_3, uint64_t param_4, uint64_t param_5, uint64_t param_6);
extern struct indirect_placeholder_48_ret_type indirect_placeholder_48(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint64_t param_3);
extern struct indirect_placeholder_51_ret_type indirect_placeholder_51(uint64_t param_0);
extern struct indirect_placeholder_50_ret_type indirect_placeholder_50(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint64_t param_3, uint64_t param_4, uint64_t param_5, uint64_t param_6);
extern struct indirect_placeholder_49_ret_type indirect_placeholder_49(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint64_t param_3, uint64_t param_4, uint64_t param_5, uint64_t param_6);
extern void indirect_placeholder_52(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint64_t param_3, uint64_t param_4, uint64_t param_5);
extern struct indirect_placeholder_54_ret_type indirect_placeholder_54(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint64_t param_3, uint64_t param_4);
void bb_main(uint64_t rcx, uint64_t rsi, uint64_t rdi, uint64_t r10, uint64_t r9, uint64_t r8) {
    struct indirect_placeholder_54_ret_type var_24;
    uint64_t var_9;
    uint64_t var_88;
    uint64_t local_sp_3;
    uint64_t local_sp_0;
    uint64_t var_0;
    uint64_t var_1;
    uint64_t var_2;
    uint64_t var_3;
    uint64_t var_4;
    uint32_t *var_5;
    uint64_t var_6;
    uint64_t *var_7;
    uint64_t var_8;
    uint64_t *var_10;
    unsigned char *var_11;
    unsigned char *var_12;
    uint32_t *var_13;
    unsigned char *var_14;
    uint64_t var_15;
    uint64_t var_16;
    uint64_t *var_17;
    uint64_t *var_18;
    uint32_t *var_19;
    uint64_t local_sp_14;
    uint64_t rcx1_2;
    uint64_t var_120;
    uint64_t r104_2;
    uint64_t r86_2;
    uint64_t var_105;
    uint64_t var_106;
    uint64_t var_107;
    uint64_t var_108;
    unsigned char var_109;
    uint64_t var_100;
    uint64_t var_101;
    uint64_t local_sp_2;
    uint64_t local_sp_1;
    unsigned char storemerge;
    unsigned char var_102;
    uint64_t local_sp_9;
    uint64_t r104_4;
    uint64_t var_103;
    uint64_t var_104;
    uint64_t local_sp_4;
    uint64_t local_sp_5;
    uint64_t var_110;
    uint64_t var_111;
    uint64_t local_sp_6;
    uint64_t local_sp_7;
    uint64_t var_112;
    uint64_t var_113;
    uint64_t var_114;
    uint64_t var_115;
    uint64_t var_117;
    uint64_t var_118;
    uint64_t var_98;
    uint64_t var_99;
    uint64_t local_sp_8;
    uint64_t rcx1_0;
    uint64_t r95_2;
    uint64_t r104_0;
    uint64_t r95_0;
    uint64_t r86_0;
    uint64_t var_116;
    uint64_t var_83;
    uint64_t var_43;
    uint64_t var_44;
    uint64_t var_45;
    uint64_t var_46;
    uint64_t var_47;
    uint64_t var_48;
    uint64_t var_49;
    struct indirect_placeholder_39_ret_type var_50;
    uint64_t var_51;
    uint64_t var_52;
    uint64_t var_53;
    uint64_t var_54;
    uint64_t var_55;
    uint64_t var_56;
    struct indirect_placeholder_37_ret_type var_57;
    uint64_t var_58;
    uint64_t var_59;
    uint64_t var_60;
    struct indirect_placeholder_36_ret_type var_61;
    uint64_t rcx1_1;
    uint64_t r104_1;
    uint64_t r95_1;
    uint64_t r86_1;
    uint64_t var_62;
    uint64_t var_64;
    uint64_t var_63;
    bool var_65;
    uint32_t var_66;
    unsigned char var_67;
    uint64_t var_68;
    uint64_t *var_69;
    uint32_t var_70;
    uint32_t var_73;
    uint64_t var_71;
    uint64_t var_72;
    uint64_t local_sp_10;
    uint64_t var_74;
    uint64_t var_75;
    uint64_t *var_76;
    uint64_t *var_77;
    uint64_t var_78;
    uint32_t *var_79;
    uint64_t var_80;
    unsigned char *var_81;
    uint64_t *var_82;
    uint64_t local_sp_11;
    uint64_t var_84;
    uint64_t var_85;
    uint64_t var_119;
    uint64_t var_86;
    uint64_t var_87;
    uint64_t var_89;
    uint64_t var_90;
    uint64_t var_91;
    struct indirect_placeholder_48_ret_type var_92;
    uint64_t var_93;
    uint64_t var_94;
    uint64_t var_95;
    uint64_t var_96;
    uint64_t var_97;
    struct indirect_placeholder_51_ret_type var_33;
    uint64_t var_34;
    uint64_t var_35;
    uint64_t var_36;
    struct indirect_placeholder_50_ret_type var_37;
    uint64_t var_38;
    uint64_t var_39;
    uint64_t var_40;
    uint64_t var_41;
    struct indirect_placeholder_49_ret_type var_42;
    uint64_t local_sp_14_be;
    uint64_t r104_4_be;
    uint64_t local_sp_13;
    uint64_t var_26;
    uint64_t var_27;
    uint64_t *var_28;
    uint64_t var_29;
    uint64_t var_30;
    uint64_t var_31;
    uint64_t var_32;
    uint64_t var_20;
    uint64_t var_21;
    uint64_t var_22;
    uint64_t var_23;
    uint32_t var_25;
    unsigned int loop_state_var;
    var_0 = revng_init_local_sp(0UL);
    var_1 = init_rbp();
    var_2 = init_cc_src2();
    var_3 = init_rbx();
    var_4 = var_0 + (-8L);
    *(uint64_t *)var_4 = var_1;
    *(uint64_t *)(var_0 + (-16L)) = var_3;
    var_5 = (uint32_t *)(var_0 + (-204L));
    *var_5 = (uint32_t)rdi;
    var_6 = var_0 + (-216L);
    var_7 = (uint64_t *)var_6;
    *var_7 = rsi;
    var_8 = var_0 + (-184L);
    *(uint64_t *)(var_0 + (-224L)) = 4207277UL;
    var_9 = indirect_placeholder_3(rcx, 8UL, var_8, r10, r9, r8, var_3);
    var_10 = (uint64_t *)(var_0 + (-80L));
    *var_10 = var_9;
    var_11 = (unsigned char *)(var_0 + (-25L));
    *var_11 = (unsigned char)'\x00';
    var_12 = (unsigned char *)(var_0 + (-26L));
    *var_12 = (unsigned char)'\x01';
    var_13 = (uint32_t *)(var_0 + (-32L));
    *var_13 = 4294967295U;
    var_14 = (unsigned char *)(var_0 + (-33L));
    *var_14 = (unsigned char)'\x00';
    var_15 = **(uint64_t **)var_6;
    *(uint64_t *)(var_0 + (-232L)) = 4207318UL;
    indirect_placeholder_32(var_15);
    *(uint64_t *)(var_0 + (-240L)) = 4207333UL;
    indirect_placeholder();
    *(uint64_t *)(var_0 + (-248L)) = 4207343UL;
    indirect_placeholder();
    var_16 = var_0 + (-256L);
    *(uint64_t *)var_16 = 4207373UL;
    indirect_placeholder();
    var_17 = (uint64_t *)(var_0 + (-88L));
    *var_17 = 4309951UL;
    var_18 = (uint64_t *)(var_0 + (-48L));
    *var_18 = 4307379UL;
    var_19 = (uint32_t *)(var_0 + (-92L));
    local_sp_14 = var_16;
    var_109 = (unsigned char)'\x00';
    storemerge = (unsigned char)'\x01';
    r104_4 = r10;
    var_111 = 0UL;
    var_67 = (unsigned char)'\x01';
    while (1U)
        {
            var_20 = *var_17;
            var_21 = *var_7;
            var_22 = (uint64_t)*var_5;
            var_23 = local_sp_14 + (-8L);
            *(uint64_t *)var_23 = 4207968UL;
            var_24 = indirect_placeholder_54(var_20, 4307648UL, var_21, var_22, 0UL);
            var_25 = (uint32_t)var_24.field_0;
            *var_19 = var_25;
            local_sp_9 = var_23;
            r104_1 = r104_4;
            local_sp_14_be = var_23;
            r104_4_be = r104_4;
            local_sp_13 = var_23;
            if (var_25 != 4294967295U) {
                var_43 = var_24.field_1;
                var_44 = var_24.field_2;
                var_45 = var_24.field_3;
                *(uint64_t *)6423960UL = 3UL;
                var_46 = *(uint64_t *)(((uint64_t)*(uint32_t *)6423984UL << 3UL) + 6423600UL) << 3UL;
                var_47 = *(uint64_t *)6423992UL;
                var_48 = helper_cc_compute_c_wrapper(var_46 - var_47, var_47, var_2, 17U);
                rcx1_1 = var_43;
                r95_1 = var_44;
                r86_1 = var_45;
                if (var_48 != 0UL) {
                    loop_state_var = 1U;
                    break;
                }
                var_49 = *var_18;
                *(uint64_t *)(local_sp_14 + (-16L)) = 4208040UL;
                var_50 = indirect_placeholder_39(var_49);
                var_51 = var_50.field_0;
                var_52 = var_50.field_1;
                var_53 = var_50.field_2;
                *(uint64_t *)(local_sp_14 + (-24L)) = 4208068UL;
                indirect_placeholder_38(0UL, 4309973UL, var_51, 0UL, 0UL, var_52, var_53);
                var_54 = (uint64_t)*(uint32_t *)6423984UL << 3UL;
                var_55 = *(uint64_t *)(var_54 + 6423600UL) << 3UL;
                var_56 = *(uint64_t *)(var_54 + 4307456UL);
                *(uint64_t *)(local_sp_14 + (-32L)) = 4208116UL;
                var_57 = indirect_placeholder_37(var_56);
                var_58 = var_57.field_0;
                var_59 = var_57.field_1;
                var_60 = local_sp_14 + (-40L);
                *(uint64_t *)var_60 = 4208147UL;
                var_61 = indirect_placeholder_36(0UL, 4310056UL, var_58, 0UL, 1UL, var_59, var_55);
                local_sp_9 = var_60;
                rcx1_1 = var_61.field_0;
                r104_1 = var_61.field_1;
                r95_1 = var_61.field_2;
                r86_1 = var_61.field_3;
                loop_state_var = 1U;
                break;
            }
            if ((uint64_t)(var_25 + (-116)) == 0UL) {
                *var_13 = 0U;
            } else {
                if ((int)var_25 > (int)116U) {
                    if ((uint64_t)(var_25 + (-129)) == 0UL) {
                        *(unsigned char *)6423976UL = (unsigned char)'\x01';
                        *(unsigned char *)6423977UL = (unsigned char)'\x00';
                        *(unsigned char *)6423979UL = (unsigned char)'\x00';
                    } else {
                        if ((int)var_25 > (int)129U) {
                            if ((uint64_t)(var_25 + (-131)) == 0UL) {
                                *(unsigned char *)6423980UL = (unsigned char)'\x01';
                            } else {
                                if ((int)var_25 < (int)131U) {
                                    *(unsigned char *)6423976UL = (unsigned char)'\x00';
                                    *(unsigned char *)6423977UL = (unsigned char)'\x00';
                                    *(unsigned char *)6423979UL = (unsigned char)'\x01';
                                } else {
                                    if ((uint64_t)(var_25 + (-132)) != 0UL) {
                                        loop_state_var = 0U;
                                        break;
                                    }
                                    *var_14 = (unsigned char)'\x01';
                                    *var_13 = 1U;
                                }
                            }
                        } else {
                            if ((uint64_t)(var_25 + (-119)) == 0UL) {
                                *(unsigned char *)6423976UL = (unsigned char)'\x00';
                                *(unsigned char *)6423977UL = (unsigned char)'\x01';
                                *(unsigned char *)6423979UL = (unsigned char)'\x00';
                            } else {
                                if ((uint64_t)(var_25 + (-128)) != 0UL) {
                                    loop_state_var = 0U;
                                    break;
                                }
                                *(unsigned char *)6423978UL = (unsigned char)'\x01';
                            }
                        }
                    }
                } else {
                    if ((uint64_t)(var_25 + (-98)) == 0UL) {
                        *var_13 = 1U;
                    } else {
                        if ((int)var_25 <= (int)98U) {
                            if ((uint64_t)(var_25 + 131U) != 0UL) {
                                if ((uint64_t)(var_25 + 130U) != 0UL) {
                                    loop_state_var = 0U;
                                    break;
                                }
                                *(uint64_t *)(local_sp_14 + (-16L)) = 4207851UL;
                                indirect_placeholder_16(var_4, 0UL);
                                abort();
                            }
                            var_26 = *(uint64_t *)6423616UL;
                            var_27 = local_sp_14 + (-24L);
                            var_28 = (uint64_t *)var_27;
                            *var_28 = 0UL;
                            *(uint64_t *)(local_sp_14 + (-32L)) = 4207909UL;
                            indirect_placeholder_52(0UL, 4307216UL, var_26, 4309417UL, 4310022UL, 4310035UL);
                            *var_28 = 4207923UL;
                            indirect_placeholder();
                            local_sp_13 = var_27;
                            loop_state_var = 0U;
                            break;
                        }
                        if ((uint64_t)(var_25 + (-99)) == 0UL) {
                            *var_11 = (unsigned char)'\x01';
                        } else {
                            if ((uint64_t)(var_25 + (-108)) != 0UL) {
                                loop_state_var = 0U;
                                break;
                            }
                            var_29 = *(uint64_t *)6424504UL;
                            var_30 = local_sp_14 + (-16L);
                            *(uint64_t *)var_30 = 4207596UL;
                            var_31 = indirect_placeholder_53(18446744073709551615UL, 4307379UL, 0UL, var_29, 0UL, 4309958UL);
                            *(uint64_t *)6423992UL = var_31;
                            var_32 = *(uint64_t *)6424504UL;
                            *var_18 = var_32;
                            local_sp_14_be = var_30;
                            if ((*(uint64_t *)6423992UL & 7UL) == 0UL) {
                                *(uint64_t *)(local_sp_14 + (-24L)) = 4207641UL;
                                var_33 = indirect_placeholder_51(var_32);
                                var_34 = var_33.field_0;
                                var_35 = var_33.field_1;
                                var_36 = var_33.field_2;
                                *(uint64_t *)(local_sp_14 + (-32L)) = 4207669UL;
                                var_37 = indirect_placeholder_50(0UL, 4309973UL, var_34, 0UL, 0UL, var_35, var_36);
                                var_38 = var_37.field_0;
                                var_39 = var_37.field_2;
                                var_40 = var_37.field_3;
                                var_41 = local_sp_14 + (-40L);
                                *(uint64_t *)var_41 = 4207694UL;
                                var_42 = indirect_placeholder_49(0UL, 4309992UL, var_38, 0UL, 1UL, var_39, var_40);
                                local_sp_14_be = var_41;
                                r104_4_be = var_42.field_1;
                            }
                        }
                    }
                }
            }
            local_sp_14 = local_sp_14_be;
            r104_4 = r104_4_be;
            continue;
        }
    switch (loop_state_var) {
      case 0U:
        {
            *(uint64_t *)(local_sp_13 + (-8L)) = 4207933UL;
            indirect_placeholder_16(var_4, 1UL);
            abort();
        }
        break;
      case 1U:
        {
            var_62 = *(uint64_t *)6423992UL;
            rcx1_2 = rcx1_1;
            r104_2 = r104_1;
            r86_2 = r86_1;
            r95_2 = r95_1;
            var_64 = var_62;
            local_sp_10 = local_sp_9;
            var_64 = 0UL;
            if (var_62 != 0UL & *var_11 == '\x01') {
                var_63 = *(uint64_t *)(((uint64_t)*(uint32_t *)6423984UL << 3UL) + 6423600UL) << 3UL;
                *(uint64_t *)6423992UL = var_63;
                var_64 = var_63;
            }
            *(uint64_t *)6423968UL = (var_64 >> 2UL);
            var_65 = (*var_14 == '\x00');
            if (!var_65) {
                if (*var_13 == 0U) {
                    *(uint64_t *)(local_sp_9 + (-8L)) = 4208252UL;
                    indirect_placeholder_40(0UL, 4310104UL, rcx1_1, 0UL, 0UL, r95_1, r86_1);
                    *(uint64_t *)(local_sp_9 + (-16L)) = 4208262UL;
                    indirect_placeholder_16(var_4, 1UL);
                    abort();
                }
            }
            if (!var_65) {
                if (*var_11 == '\x00') {
                    *(uint64_t *)(local_sp_9 + (-8L)) = 4208299UL;
                    indirect_placeholder_41(0UL, 4310144UL, rcx1_1, 0UL, 0UL, r95_1, r86_1);
                    *(uint64_t *)(local_sp_9 + (-16L)) = 4208309UL;
                    indirect_placeholder_16(var_4, 1UL);
                    abort();
                }
            }
            var_66 = *var_13;
            if ((int)var_66 >= (int)0U) {
                if (*var_11 == '\x00') {
                    *(uint64_t *)(local_sp_9 + (-8L)) = 4208346UL;
                    indirect_placeholder_42(0UL, 4310208UL, rcx1_1, 0UL, 0UL, r95_1, r86_1);
                    *(uint64_t *)(local_sp_9 + (-16L)) = 4208356UL;
                    indirect_placeholder_16(var_4, 1UL);
                    abort();
                }
            }
            if (*(unsigned char *)6423978UL != '\x00') {
                if (*var_11 == '\x01') {
                    *(uint64_t *)(local_sp_9 + (-8L)) = 4208403UL;
                    indirect_placeholder_43(0UL, 4310288UL, rcx1_1, 0UL, 0UL, r95_1, r86_1);
                    *(uint64_t *)(local_sp_9 + (-16L)) = 4208413UL;
                    indirect_placeholder_16(var_4, 1UL);
                    abort();
                }
            }
            if (*(unsigned char *)6423976UL != '\x00') {
                if (*var_11 == '\x01') {
                    *(uint64_t *)(local_sp_9 + (-8L)) = 4208460UL;
                    indirect_placeholder_44(0UL, 4310360UL, rcx1_1, 0UL, 0UL, r95_1, r86_1);
                    *(uint64_t *)(local_sp_9 + (-16L)) = 4208470UL;
                    indirect_placeholder_16(var_4, 1UL);
                    abort();
                }
            }
            if (*(unsigned char *)6423977UL != '\x00') {
                if (*var_11 == '\x01') {
                    *(uint64_t *)(local_sp_9 + (-8L)) = 4208517UL;
                    indirect_placeholder_45(0UL, 4310424UL, rcx1_1, 0UL, 0UL, r95_1, r86_1);
                    *(uint64_t *)(local_sp_9 + (-16L)) = 4208527UL;
                    indirect_placeholder_16(var_4, 1UL);
                    abort();
                }
            }
            if (*(unsigned char *)6423979UL != '\x00') {
                if (*var_11 == '\x01') {
                    *(uint64_t *)(local_sp_9 + (-8L)) = 4208574UL;
                    indirect_placeholder_46(0UL, 4310488UL, rcx1_1, 0UL, 0UL, r95_1, r86_1);
                    *(uint64_t *)(local_sp_9 + (-16L)) = 4208584UL;
                    indirect_placeholder_16(var_4, 1UL);
                    abort();
                }
            }
            var_67 = *var_11;
        }
        break;
    }
}

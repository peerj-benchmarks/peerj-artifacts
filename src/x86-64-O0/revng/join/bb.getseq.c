typedef unsigned char __u_char;
typedef unsigned short __u_short;
typedef unsigned int __u_int;
typedef unsigned long __u_long;
typedef signed char __int8_t;
typedef unsigned char __uint8_t;
typedef short __int16_t;
typedef unsigned short __uint16_t;
typedef int __int32_t;
typedef unsigned int __uint32_t;
typedef long __int64_t;
typedef unsigned long __uint64_t;
typedef __int8_t __int_least8_t;
typedef __uint8_t __uint_least8_t;
typedef __int16_t __int_least16_t;
typedef __uint16_t __uint_least16_t;
typedef __int32_t __int_least32_t;
typedef __uint32_t __uint_least32_t;
typedef __int64_t __int_least64_t;
typedef __uint64_t __uint_least64_t;
typedef long __quad_t;
typedef unsigned long __u_quad_t;
typedef long __intmax_t;
typedef unsigned long __uintmax_t;
typedef unsigned long __dev_t;
typedef unsigned int __uid_t;
typedef unsigned int __gid_t;
typedef unsigned long __ino_t;
typedef unsigned long __ino64_t;
typedef unsigned int __mode_t;
typedef unsigned long __nlink_t;
typedef long __off_t;
typedef long __off64_t;
typedef int __pid_t;
typedef struct {
    int __val[2];
} __fsid_t;
typedef long __clock_t;
typedef unsigned long __rlim_t;
typedef unsigned long __rlim64_t;
typedef unsigned int __id_t;
typedef long __time_t;
typedef unsigned int __useconds_t;
typedef long __suseconds_t;
typedef int __daddr_t;
typedef int __key_t;
typedef int __clockid_t;
typedef void *__timer_t;
typedef long __blksize_t;
typedef long __blkcnt_t;
typedef long __blkcnt64_t;
typedef unsigned long __fsblkcnt_t;
typedef unsigned long __fsblkcnt64_t;
typedef unsigned long __fsfilcnt_t;
typedef unsigned long __fsfilcnt64_t;
typedef long __fsword_t;
typedef long __ssize_t;
typedef long __syscall_slong_t;
typedef unsigned long __syscall_ulong_t;
typedef __off64_t __loff_t;
typedef char *__caddr_t;
typedef long __intptr_t;
typedef unsigned int __socklen_t;
typedef int __sig_atomic_t;
typedef __int8_t int8_t;
typedef __int16_t int16_t;
typedef __int32_t int32_t;
typedef __int64_t int64_t;
typedef __uint8_t uint8_t;
typedef __uint16_t uint16_t;
typedef __uint32_t uint32_t;
typedef __uint64_t uint64_t;
typedef __int_least8_t int_least8_t;
typedef __int_least16_t int_least16_t;
typedef __int_least32_t int_least32_t;
typedef __int_least64_t int_least64_t;
typedef __uint_least8_t uint_least8_t;
typedef __uint_least16_t uint_least16_t;
typedef __uint_least32_t uint_least32_t;
typedef __uint_least64_t uint_least64_t;
typedef signed char int_fast8_t;
typedef long int_fast16_t;
typedef long int_fast32_t;
typedef long int_fast64_t;
typedef unsigned char uint_fast8_t;
typedef unsigned long uint_fast16_t;
typedef unsigned long uint_fast32_t;
typedef unsigned long uint_fast64_t;
typedef long intptr_t;
typedef unsigned long uintptr_t;
typedef __intmax_t intmax_t;
typedef __uintmax_t uintmax_t;
struct bb_getseq_ret_type;
struct indirect_placeholder_24_ret_type;
struct indirect_placeholder_25_ret_type;
struct bb_getseq_ret_type {
    uint64_t field_0;
    uint64_t field_1;
    uint64_t field_2;
    uint64_t field_3;
    uint64_t field_4;
};
struct indirect_placeholder_24_ret_type {
    uint64_t field_0;
    uint64_t field_1;
    uint64_t field_2;
    uint64_t field_3;
    uint64_t field_4;
};
struct indirect_placeholder_25_ret_type {
    uint64_t field_0;
    uint64_t field_1;
    uint64_t field_2;
    uint64_t field_3;
    uint64_t field_4;
};
extern uint64_t revng_init_local_sp(uint64_t param_0);
extern uint64_t init_rbp(void);
extern struct indirect_placeholder_24_ret_type indirect_placeholder_24(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint64_t param_3, uint64_t param_4, uint64_t param_5, uint64_t param_6, uint64_t param_7);
extern struct indirect_placeholder_25_ret_type indirect_placeholder_25(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint64_t param_3, uint64_t param_4, uint64_t param_5, uint64_t param_6, uint64_t param_7);
struct bb_getseq_ret_type bb_getseq(uint64_t rdx, uint64_t rdi, uint64_t rsi, uint64_t r8, uint64_t r10, uint64_t r9, uint64_t rbx) {
    struct bb_getseq_ret_type mrv;
    uint64_t var_0;
    uint64_t var_1;
    uint64_t var_2;
    uint64_t *var_3;
    uint64_t var_4;
    uint64_t *var_5;
    uint32_t *var_6;
    uint32_t var_7;
    uint64_t **var_8;
    uint64_t var_9;
    uint64_t var_10;
    uint64_t var_11;
    uint32_t var_23;
    uint64_t *var_34;
    uint64_t storemerge;
    struct bb_getseq_ret_type mrv1;
    struct bb_getseq_ret_type mrv2;
    struct bb_getseq_ret_type mrv3;
    struct bb_getseq_ret_type mrv4;
    uint64_t var_21;
    uint64_t var_22;
    uint64_t var_35;
    uint64_t var_12;
    uint64_t var_13;
    struct indirect_placeholder_24_ret_type var_14;
    uint64_t var_15;
    uint64_t var_16;
    uint64_t var_17;
    uint64_t var_18;
    uint64_t var_19;
    uint64_t *var_20;
    uint64_t var_24;
    uint64_t var_25;
    uint64_t local_sp_0;
    uint64_t r88_0;
    uint64_t r109_0;
    uint64_t r910_0;
    uint64_t rbx11_0;
    uint64_t var_26;
    uint64_t var_27;
    uint64_t var_28;
    struct indirect_placeholder_25_ret_type var_29;
    uint64_t var_30;
    uint64_t var_31;
    uint64_t var_32;
    uint64_t var_33;
    var_0 = revng_init_local_sp(0UL);
    var_1 = init_rbp();
    *(uint64_t *)(var_0 + (-8L)) = var_1;
    var_2 = var_0 + (-56L);
    var_3 = (uint64_t *)(var_0 + (-32L));
    *var_3 = rdi;
    var_4 = var_0 + (-40L);
    var_5 = (uint64_t *)var_4;
    *var_5 = rsi;
    var_6 = (uint32_t *)(var_0 + (-44L));
    var_7 = (uint32_t)rdx;
    *var_6 = var_7;
    var_8 = (uint64_t **)var_4;
    var_9 = **var_8;
    var_10 = *var_5;
    var_11 = var_10 + 8UL;
    var_23 = var_7;
    storemerge = 0UL;
    var_24 = var_9;
    var_25 = var_10;
    local_sp_0 = var_2;
    r88_0 = r8;
    r109_0 = r10;
    r910_0 = r9;
    rbx11_0 = rbx;
    if (var_9 != *(uint64_t *)var_11) {
        var_12 = *(uint64_t *)(var_10 + 16UL);
        var_13 = var_0 + (-64L);
        *(uint64_t *)var_13 = 4204684UL;
        var_14 = indirect_placeholder_24(8UL, var_11, var_12, var_11, r8, r10, r9, rbx);
        var_15 = var_14.field_1;
        var_16 = var_14.field_2;
        var_17 = var_14.field_3;
        var_18 = var_14.field_4;
        *(uint64_t *)(*var_5 + 16UL) = var_14.field_0;
        var_19 = **var_8;
        var_20 = (uint64_t *)(var_0 + (-16L));
        *var_20 = var_19;
        var_21 = var_19;
        local_sp_0 = var_13;
        r88_0 = var_15;
        r109_0 = var_16;
        r910_0 = var_17;
        rbx11_0 = var_18;
        var_22 = *var_5;
        var_25 = var_22;
        while (*(uint64_t *)(var_22 + 8UL) <= var_21)
            {
                *(uint64_t *)(*(uint64_t *)(var_22 + 16UL) + (var_21 << 3UL)) = 0UL;
                var_35 = *var_20 + 1UL;
                *var_20 = var_35;
                var_21 = var_35;
                var_22 = *var_5;
                var_25 = var_22;
            }
        var_23 = *var_6;
        var_24 = **var_8;
    }
    var_26 = (var_24 << 3UL) + *(uint64_t *)(var_25 + 16UL);
    var_27 = (uint64_t)var_23;
    var_28 = *var_3;
    *(uint64_t *)(local_sp_0 + (-8L)) = 4204794UL;
    var_29 = indirect_placeholder_25(var_27, var_26, var_28, var_26, r88_0, r109_0, r910_0, rbx11_0);
    var_30 = var_29.field_1;
    var_31 = var_29.field_2;
    var_32 = var_29.field_3;
    var_33 = var_29.field_4;
    if ((uint64_t)(unsigned char)var_29.field_0 == 0UL) {
        mrv.field_0 = storemerge;
        mrv1 = mrv;
        mrv1.field_1 = var_30;
        mrv2 = mrv1;
        mrv2.field_2 = var_31;
        mrv3 = mrv2;
        mrv3.field_3 = var_32;
        mrv4 = mrv3;
        mrv4.field_4 = var_33;
        return mrv4;
    }
    var_34 = *var_8;
    *var_34 = (*var_34 + 1UL);
    storemerge = 1UL;
    mrv.field_0 = storemerge;
    mrv1 = mrv;
    mrv1.field_1 = var_30;
    mrv2 = mrv1;
    mrv2.field_2 = var_31;
    mrv3 = mrv2;
    mrv3.field_3 = var_32;
    mrv4 = mrv3;
    mrv4.field_4 = var_33;
    return mrv4;
}

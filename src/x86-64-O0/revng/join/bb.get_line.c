typedef unsigned char __u_char;
typedef unsigned short __u_short;
typedef unsigned int __u_int;
typedef unsigned long __u_long;
typedef signed char __int8_t;
typedef unsigned char __uint8_t;
typedef short __int16_t;
typedef unsigned short __uint16_t;
typedef int __int32_t;
typedef unsigned int __uint32_t;
typedef long __int64_t;
typedef unsigned long __uint64_t;
typedef __int8_t __int_least8_t;
typedef __uint8_t __uint_least8_t;
typedef __int16_t __int_least16_t;
typedef __uint16_t __uint_least16_t;
typedef __int32_t __int_least32_t;
typedef __uint32_t __uint_least32_t;
typedef __int64_t __int_least64_t;
typedef __uint64_t __uint_least64_t;
typedef long __quad_t;
typedef unsigned long __u_quad_t;
typedef long __intmax_t;
typedef unsigned long __uintmax_t;
typedef unsigned long __dev_t;
typedef unsigned int __uid_t;
typedef unsigned int __gid_t;
typedef unsigned long __ino_t;
typedef unsigned long __ino64_t;
typedef unsigned int __mode_t;
typedef unsigned long __nlink_t;
typedef long __off_t;
typedef long __off64_t;
typedef int __pid_t;
typedef struct {
    int __val[2];
} __fsid_t;
typedef long __clock_t;
typedef unsigned long __rlim_t;
typedef unsigned long __rlim64_t;
typedef unsigned int __id_t;
typedef long __time_t;
typedef unsigned int __useconds_t;
typedef long __suseconds_t;
typedef int __daddr_t;
typedef int __key_t;
typedef int __clockid_t;
typedef void *__timer_t;
typedef long __blksize_t;
typedef long __blkcnt_t;
typedef long __blkcnt64_t;
typedef unsigned long __fsblkcnt_t;
typedef unsigned long __fsblkcnt64_t;
typedef unsigned long __fsfilcnt_t;
typedef unsigned long __fsfilcnt64_t;
typedef long __fsword_t;
typedef long __ssize_t;
typedef long __syscall_slong_t;
typedef unsigned long __syscall_ulong_t;
typedef __off64_t __loff_t;
typedef char *__caddr_t;
typedef long __intptr_t;
typedef unsigned int __socklen_t;
typedef int __sig_atomic_t;
typedef __int8_t int8_t;
typedef __int16_t int16_t;
typedef __int32_t int32_t;
typedef __int64_t int64_t;
typedef __uint8_t uint8_t;
typedef __uint16_t uint16_t;
typedef __uint32_t uint32_t;
typedef __uint64_t uint64_t;
typedef __int_least8_t int_least8_t;
typedef __int_least16_t int_least16_t;
typedef __int_least32_t int_least32_t;
typedef __int_least64_t int_least64_t;
typedef __uint_least8_t uint_least8_t;
typedef __uint_least16_t uint_least16_t;
typedef __uint_least32_t uint_least32_t;
typedef __uint_least64_t uint_least64_t;
typedef signed char int_fast8_t;
typedef long int_fast16_t;
typedef long int_fast32_t;
typedef long int_fast64_t;
typedef unsigned char uint_fast8_t;
typedef unsigned long uint_fast16_t;
typedef unsigned long uint_fast32_t;
typedef unsigned long uint_fast64_t;
typedef long intptr_t;
typedef unsigned long uintptr_t;
typedef __intmax_t intmax_t;
typedef __uintmax_t uintmax_t;
struct bb_get_line_ret_type;
struct indirect_placeholder_5_ret_type;
struct indirect_placeholder_7_ret_type;
struct bb_get_line_ret_type {
    uint64_t field_0;
    uint64_t field_1;
    uint64_t field_2;
    uint64_t field_3;
    uint64_t field_4;
};
struct indirect_placeholder_5_ret_type {
    uint64_t field_0;
    uint64_t field_1;
    uint64_t field_2;
};
struct indirect_placeholder_7_ret_type {
    uint64_t field_0;
    uint64_t field_1;
    uint64_t field_2;
    uint64_t field_3;
    uint64_t field_4;
};
extern uint64_t revng_init_local_sp(uint64_t param_0);
extern uint64_t init_rbp(void);
extern void indirect_placeholder(void);
extern void indirect_placeholder_3(uint64_t param_0);
extern uint64_t indirect_placeholder_8(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint64_t param_3, uint64_t param_4, uint64_t param_5);
extern void indirect_placeholder_4(uint64_t param_0, uint64_t param_1, uint64_t param_2);
extern void indirect_placeholder_6(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint64_t param_3, uint64_t param_4);
extern struct indirect_placeholder_5_ret_type indirect_placeholder_5(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint64_t param_3, uint64_t param_4, uint64_t param_5, uint64_t param_6);
extern struct indirect_placeholder_7_ret_type indirect_placeholder_7(uint64_t param_0, uint64_t param_1, uint64_t param_2);
struct bb_get_line_ret_type bb_get_line(uint64_t rdx, uint64_t rcx, uint64_t rdi, uint64_t rsi, uint64_t r8, uint64_t r10, uint64_t r9, uint64_t rbx) {
    struct indirect_placeholder_5_ret_type var_34;
    uint64_t var_0;
    uint64_t var_1;
    uint64_t *var_2;
    uint64_t var_3;
    uint64_t *var_4;
    uint32_t *var_5;
    uint64_t **var_6;
    uint64_t var_7;
    uint64_t *var_8;
    uint64_t var_10;
    uint64_t var_29;
    uint64_t r911_0;
    uint64_t *_pre_phi78;
    uint64_t r911_1;
    uint64_t r89_0;
    uint64_t storemerge;
    struct bb_get_line_ret_type mrv;
    struct bb_get_line_ret_type mrv1;
    struct bb_get_line_ret_type mrv2;
    struct bb_get_line_ret_type mrv3;
    struct bb_get_line_ret_type mrv4;
    uint64_t var_32;
    uint64_t var_33;
    uint64_t local_sp_0;
    uint64_t r89_1;
    uint64_t var_35;
    uint64_t var_30;
    uint64_t var_31;
    uint64_t *var_24;
    uint64_t var_25;
    uint64_t var_26;
    uint64_t *var_27;
    uint64_t var_28;
    uint64_t var_15;
    uint64_t local_sp_1;
    uint64_t var_16;
    uint64_t var_17;
    struct indirect_placeholder_7_ret_type var_18;
    uint64_t var_19;
    uint64_t var_20;
    uint64_t var_21;
    uint64_t var_22;
    uint64_t var_23;
    uint64_t *var_9;
    uint64_t var_11;
    uint64_t var_12;
    uint64_t var_13;
    uint64_t var_14;
    var_0 = revng_init_local_sp(0UL);
    var_1 = init_rbp();
    *(uint64_t *)(var_0 + (-8L)) = var_1;
    var_2 = (uint64_t *)(var_0 + (-32L));
    *var_2 = rdi;
    var_3 = var_0 + (-40L);
    var_4 = (uint64_t *)var_3;
    *var_4 = rsi;
    var_5 = (uint32_t *)(var_0 + (-44L));
    *var_5 = (uint32_t)rdx;
    var_6 = (uint64_t **)var_3;
    var_7 = **var_6;
    var_8 = (uint64_t *)(var_0 + (-16L));
    *var_8 = var_7;
    storemerge = 0UL;
    var_10 = var_7;
    if (*(uint64_t *)((uint64_t)((long)(((uint64_t)*var_5 << 32UL) + (-4294967296L)) >> (long)29UL) + 6392416UL) == var_7) {
        var_9 = (uint64_t *)(var_0 + (-24L));
        *var_9 = var_7;
        *var_8 = *(uint64_t *)((uint64_t)((long)(((uint64_t)*var_5 << 32UL) + (-4294967296L)) >> (long)29UL) + 6392464UL);
        *(uint64_t *)((uint64_t)((long)(((uint64_t)*var_5 << 32UL) + (-4294967296L)) >> (long)29UL) + 6392464UL) = *var_9;
        **var_6 = *var_8;
        var_10 = *var_8;
    }
    if (var_10 == 0UL) {
        var_12 = *var_4;
        var_13 = var_0 + (-64L);
        *(uint64_t *)var_13 = 4204252UL;
        var_14 = indirect_placeholder_8(rcx, var_12, r8, r10, r9, rbx);
        *var_8 = var_14;
        var_15 = var_14;
        local_sp_1 = var_13;
    } else {
        var_11 = var_0 + (-64L);
        *(uint64_t *)var_11 = 4204238UL;
        indirect_placeholder_3(var_10);
        var_15 = *var_8;
        local_sp_1 = var_11;
    }
    var_16 = (uint64_t)(uint32_t)(uint64_t)*(unsigned char *)6392028UL;
    var_17 = *var_2;
    *(uint64_t *)(local_sp_1 + (-8L)) = 4204285UL;
    var_18 = indirect_placeholder_7(var_16, var_15, var_17);
    var_19 = var_18.field_0;
    var_20 = var_18.field_1;
    var_21 = var_18.field_2;
    var_22 = var_18.field_3;
    var_23 = var_18.field_4;
    r89_0 = var_20;
    r911_0 = var_22;
    r89_1 = var_20;
    r911_1 = var_22;
    if (var_19 == 0UL) {
        var_30 = *var_2;
        var_31 = local_sp_1 + (-16L);
        *(uint64_t *)var_31 = 4204302UL;
        indirect_placeholder();
        local_sp_0 = var_31;
        if ((uint64_t)(uint32_t)var_30 != 0UL) {
            *(uint64_t *)(local_sp_1 + (-24L)) = 4204311UL;
            indirect_placeholder();
            var_32 = (uint64_t)*(uint32_t *)var_30;
            var_33 = local_sp_1 + (-32L);
            *(uint64_t *)var_33 = 4204335UL;
            var_34 = indirect_placeholder_5(0UL, 4281251UL, var_17, 1UL, var_32, var_20, var_22);
            local_sp_0 = var_33;
            r89_1 = var_34.field_1;
            r911_1 = var_34.field_2;
        }
        var_35 = *var_8;
        *(uint64_t *)(local_sp_0 + (-8L)) = 4204347UL;
        indirect_placeholder_3(var_35);
        r89_0 = r89_1;
        r911_0 = r911_1;
    } else {
        var_24 = (uint64_t *)((uint64_t)((long)((uint64_t)(*var_5 + (-1)) << 32UL) >> (long)29UL) + 6392432UL);
        *var_24 = (*var_24 + 1UL);
        var_25 = *var_8;
        *(uint64_t *)(local_sp_1 + (-16L)) = 4204397UL;
        indirect_placeholder_6(var_25, var_20, var_21, var_22, var_23);
        var_26 = (uint64_t)*var_5;
        var_27 = (uint64_t *)((uint64_t)((long)((var_26 << 32UL) + (-4294967296L)) >> (long)29UL) + 6392416UL);
        var_28 = *var_27;
        _pre_phi78 = var_27;
        storemerge = 1UL;
        if (var_28 != 0UL) {
            var_29 = *var_8;
            *(uint64_t *)(local_sp_1 + (-24L)) = 4204452UL;
            indirect_placeholder_4(var_26, var_28, var_29);
            _pre_phi78 = (uint64_t *)((uint64_t)((long)(((uint64_t)*var_5 << 32UL) + (-4294967296L)) >> (long)29UL) + 6392416UL);
        }
        *_pre_phi78 = *var_8;
    }
    mrv.field_0 = storemerge;
    mrv1 = mrv;
    mrv1.field_1 = r89_0;
    mrv2 = mrv1;
    mrv2.field_2 = var_21;
    mrv3 = mrv2;
    mrv3.field_3 = r911_0;
    mrv4 = mrv3;
    mrv4.field_4 = var_23;
    return mrv4;
}

typedef unsigned char __u_char;
typedef unsigned short __u_short;
typedef unsigned int __u_int;
typedef unsigned long __u_long;
typedef signed char __int8_t;
typedef unsigned char __uint8_t;
typedef short __int16_t;
typedef unsigned short __uint16_t;
typedef int __int32_t;
typedef unsigned int __uint32_t;
typedef long __int64_t;
typedef unsigned long __uint64_t;
typedef __int8_t __int_least8_t;
typedef __uint8_t __uint_least8_t;
typedef __int16_t __int_least16_t;
typedef __uint16_t __uint_least16_t;
typedef __int32_t __int_least32_t;
typedef __uint32_t __uint_least32_t;
typedef __int64_t __int_least64_t;
typedef __uint64_t __uint_least64_t;
typedef long __quad_t;
typedef unsigned long __u_quad_t;
typedef long __intmax_t;
typedef unsigned long __uintmax_t;
typedef unsigned long __dev_t;
typedef unsigned int __uid_t;
typedef unsigned int __gid_t;
typedef unsigned long __ino_t;
typedef unsigned long __ino64_t;
typedef unsigned int __mode_t;
typedef unsigned long __nlink_t;
typedef long __off_t;
typedef long __off64_t;
typedef int __pid_t;
typedef struct {
    int __val[2];
} __fsid_t;
typedef long __clock_t;
typedef unsigned long __rlim_t;
typedef unsigned long __rlim64_t;
typedef unsigned int __id_t;
typedef long __time_t;
typedef unsigned int __useconds_t;
typedef long __suseconds_t;
typedef int __daddr_t;
typedef int __key_t;
typedef int __clockid_t;
typedef void *__timer_t;
typedef long __blksize_t;
typedef long __blkcnt_t;
typedef long __blkcnt64_t;
typedef unsigned long __fsblkcnt_t;
typedef unsigned long __fsblkcnt64_t;
typedef unsigned long __fsfilcnt_t;
typedef unsigned long __fsfilcnt64_t;
typedef long __fsword_t;
typedef long __ssize_t;
typedef long __syscall_slong_t;
typedef unsigned long __syscall_ulong_t;
typedef __off64_t __loff_t;
typedef char *__caddr_t;
typedef long __intptr_t;
typedef unsigned int __socklen_t;
typedef int __sig_atomic_t;
typedef __int8_t int8_t;
typedef __int16_t int16_t;
typedef __int32_t int32_t;
typedef __int64_t int64_t;
typedef __uint8_t uint8_t;
typedef __uint16_t uint16_t;
typedef __uint32_t uint32_t;
typedef __uint64_t uint64_t;
typedef __int_least8_t int_least8_t;
typedef __int_least16_t int_least16_t;
typedef __int_least32_t int_least32_t;
typedef __int_least64_t int_least64_t;
typedef __uint_least8_t uint_least8_t;
typedef __uint_least16_t uint_least16_t;
typedef __uint_least32_t uint_least32_t;
typedef __uint_least64_t uint_least64_t;
typedef signed char int_fast8_t;
typedef long int_fast16_t;
typedef long int_fast32_t;
typedef long int_fast64_t;
typedef unsigned char uint_fast8_t;
typedef unsigned long uint_fast16_t;
typedef unsigned long uint_fast32_t;
typedef unsigned long uint_fast64_t;
typedef long intptr_t;
typedef unsigned long uintptr_t;
typedef __intmax_t intmax_t;
typedef __uintmax_t uintmax_t;
typedef _Bool bool;
struct indirect_placeholder_39_ret_type;
struct indirect_placeholder_38_ret_type;
struct indirect_placeholder_27_ret_type;
struct indirect_placeholder_28_ret_type;
struct indirect_placeholder_29_ret_type;
struct indirect_placeholder_30_ret_type;
struct indirect_placeholder_31_ret_type;
struct indirect_placeholder_32_ret_type;
struct indirect_placeholder_33_ret_type;
struct indirect_placeholder_34_ret_type;
struct indirect_placeholder_35_ret_type;
struct indirect_placeholder_36_ret_type;
struct indirect_placeholder_37_ret_type;
struct indirect_placeholder_39_ret_type {
    uint64_t field_0;
    uint64_t field_1;
    uint64_t field_2;
    uint64_t field_3;
    uint64_t field_4;
};
struct indirect_placeholder_38_ret_type {
    uint64_t field_0;
    uint64_t field_1;
    uint64_t field_2;
    uint64_t field_3;
    uint64_t field_4;
};
struct indirect_placeholder_27_ret_type {
    uint64_t field_0;
    uint64_t field_1;
    uint64_t field_2;
    uint64_t field_3;
};
struct indirect_placeholder_28_ret_type {
    uint64_t field_0;
    uint64_t field_1;
    uint64_t field_2;
    uint64_t field_3;
    uint64_t field_4;
};
struct indirect_placeholder_29_ret_type {
    uint64_t field_0;
    uint64_t field_1;
    uint64_t field_2;
    uint64_t field_3;
};
struct indirect_placeholder_30_ret_type {
    uint64_t field_0;
    uint64_t field_1;
    uint64_t field_2;
    uint64_t field_3;
    uint64_t field_4;
};
struct indirect_placeholder_31_ret_type {
    uint64_t field_0;
    uint64_t field_1;
    uint64_t field_2;
    uint64_t field_3;
    uint64_t field_4;
};
struct indirect_placeholder_32_ret_type {
    uint64_t field_0;
    uint64_t field_1;
    uint64_t field_2;
    uint64_t field_3;
    uint64_t field_4;
};
struct indirect_placeholder_33_ret_type {
    uint64_t field_0;
    uint64_t field_1;
    uint64_t field_2;
    uint64_t field_3;
    uint64_t field_4;
};
struct indirect_placeholder_34_ret_type {
    uint64_t field_0;
    uint64_t field_1;
    uint64_t field_2;
    uint64_t field_3;
    uint64_t field_4;
};
struct indirect_placeholder_35_ret_type {
    uint64_t field_0;
    uint64_t field_1;
    uint64_t field_2;
    uint64_t field_3;
};
struct indirect_placeholder_36_ret_type {
    uint64_t field_0;
    uint64_t field_1;
    uint64_t field_2;
    uint64_t field_3;
    uint64_t field_4;
};
struct indirect_placeholder_37_ret_type {
    uint64_t field_0;
    uint64_t field_1;
    uint64_t field_2;
    uint64_t field_3;
    uint64_t field_4;
};
extern uint64_t revng_init_local_sp(uint64_t param_0);
extern uint64_t init_rbp(void);
extern void indirect_placeholder(void);
extern uint64_t init_r8(void);
extern uint64_t init_r9(void);
extern uint64_t init_rbx(void);
extern uint64_t init_r10(void);
extern void indirect_placeholder_26(uint64_t param_0, uint64_t param_1);
extern void indirect_placeholder_3(uint64_t param_0);
extern struct indirect_placeholder_39_ret_type indirect_placeholder_39(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint64_t param_3, uint64_t param_4, uint64_t param_5, uint64_t param_6);
extern struct indirect_placeholder_38_ret_type indirect_placeholder_38(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint64_t param_3, uint64_t param_4, uint64_t param_5, uint64_t param_6);
extern struct indirect_placeholder_27_ret_type indirect_placeholder_27(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint64_t param_3);
extern struct indirect_placeholder_28_ret_type indirect_placeholder_28(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint64_t param_3, uint64_t param_4, uint64_t param_5, uint64_t param_6, uint64_t param_7);
extern struct indirect_placeholder_29_ret_type indirect_placeholder_29(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint64_t param_3);
extern struct indirect_placeholder_30_ret_type indirect_placeholder_30(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint64_t param_3, uint64_t param_4, uint64_t param_5, uint64_t param_6, uint64_t param_7);
extern struct indirect_placeholder_31_ret_type indirect_placeholder_31(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint64_t param_3, uint64_t param_4, uint64_t param_5, uint64_t param_6, uint64_t param_7);
extern struct indirect_placeholder_32_ret_type indirect_placeholder_32(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint64_t param_3, uint64_t param_4, uint64_t param_5, uint64_t param_6, uint64_t param_7);
extern struct indirect_placeholder_33_ret_type indirect_placeholder_33(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint64_t param_3, uint64_t param_4, uint64_t param_5, uint64_t param_6, uint64_t param_7);
extern struct indirect_placeholder_34_ret_type indirect_placeholder_34(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint64_t param_3, uint64_t param_4, uint64_t param_5, uint64_t param_6, uint64_t param_7);
extern struct indirect_placeholder_35_ret_type indirect_placeholder_35(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint64_t param_3);
extern struct indirect_placeholder_36_ret_type indirect_placeholder_36(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint64_t param_3, uint64_t param_4, uint64_t param_5, uint64_t param_6, uint64_t param_7);
extern struct indirect_placeholder_37_ret_type indirect_placeholder_37(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint64_t param_3, uint64_t param_4, uint64_t param_5, uint64_t param_6, uint64_t param_7);
void bb_system_join(uint64_t rdi, uint64_t rsi) {
    struct indirect_placeholder_38_ret_type var_20;
    uint64_t local_sp_18;
    uint64_t var_0;
    uint64_t var_1;
    uint64_t var_2;
    uint64_t var_3;
    uint64_t var_4;
    uint64_t var_5;
    uint64_t *var_6;
    uint64_t *var_7;
    uint64_t var_8;
    uint64_t var_9;
    uint64_t var_10;
    uint64_t var_11;
    struct indirect_placeholder_39_ret_type var_12;
    uint64_t var_13;
    uint64_t var_14;
    uint64_t var_15;
    uint64_t var_16;
    uint64_t var_17;
    uint64_t var_18;
    uint64_t var_19;
    uint64_t var_21;
    uint64_t var_22;
    uint64_t var_23;
    uint64_t var_24;
    uint64_t local_sp_0;
    uint64_t r8_9;
    uint64_t var_142;
    uint64_t var_143;
    uint64_t local_sp_19;
    uint64_t local_sp_2;
    uint64_t local_sp_1;
    uint64_t r8_7;
    uint64_t var_130;
    uint64_t var_131;
    uint64_t local_sp_16;
    uint64_t local_sp_15;
    uint64_t local_sp_7;
    uint64_t local_sp_12;
    uint64_t var_90;
    uint64_t var_91;
    uint64_t var_92;
    uint64_t r8_0;
    uint64_t r10_0;
    uint64_t r9_0;
    uint64_t local_sp_5;
    uint64_t var_99;
    uint64_t local_sp_3;
    uint64_t *_pre_phi;
    uint64_t var_100;
    uint64_t local_sp_4;
    uint64_t var_101;
    uint64_t var_102;
    uint64_t var_103;
    uint64_t var_104;
    uint64_t var_105;
    uint64_t local_sp_9;
    uint64_t local_sp_8;
    uint64_t local_sp_13_be;
    uint64_t r8_6_be;
    uint64_t r10_6_be;
    uint64_t r9_6_be;
    uint64_t rbx_4_be;
    uint64_t local_sp_13;
    uint64_t r8_2;
    uint64_t var_93;
    uint64_t var_94;
    uint64_t var_95;
    uint64_t var_96;
    uint64_t var_97;
    struct indirect_placeholder_27_ret_type var_98;
    uint64_t local_sp_10;
    uint64_t var_76;
    uint64_t var_77;
    uint64_t var_78;
    uint64_t local_sp_6;
    uint64_t r8_1;
    uint64_t r10_1;
    uint64_t r9_1;
    uint64_t r10_2;
    uint64_t r9_2;
    uint64_t rbx_0;
    uint64_t var_85;
    uint64_t var_86;
    struct indirect_placeholder_28_ret_type var_87;
    uint64_t var_88;
    uint64_t var_89;
    uint64_t r8_3;
    uint64_t var_79;
    uint64_t var_80;
    uint64_t var_81;
    uint64_t var_82;
    uint64_t var_83;
    struct indirect_placeholder_29_ret_type var_84;
    uint64_t r8_6;
    uint64_t var_62;
    uint64_t var_63;
    uint64_t var_64;
    uint64_t var_65;
    struct indirect_placeholder_30_ret_type var_66;
    uint64_t var_67;
    uint64_t var_68;
    uint64_t var_69;
    uint64_t var_70;
    uint64_t var_106;
    uint64_t var_107;
    uint64_t var_108;
    uint64_t var_109;
    struct indirect_placeholder_31_ret_type var_110;
    uint64_t var_111;
    uint64_t var_112;
    uint64_t var_113;
    uint64_t var_114;
    uint64_t r10_3;
    uint64_t r9_3;
    uint64_t rbx_1;
    uint64_t var_71;
    uint64_t var_72;
    struct indirect_placeholder_32_ret_type var_73;
    uint64_t var_74;
    uint64_t var_75;
    uint64_t var_32;
    uint64_t var_33;
    struct indirect_placeholder_33_ret_type var_34;
    uint64_t storemerge;
    uint64_t local_sp_11;
    uint64_t r8_4;
    uint64_t r10_4;
    uint64_t r9_4;
    uint64_t rbx_2;
    uint64_t *_pre_phi319;
    uint64_t var_35;
    uint64_t var_36;
    struct indirect_placeholder_34_ret_type var_37;
    uint64_t storemerge22;
    uint64_t storemerge21;
    uint64_t storemerge20;
    uint64_t *var_25;
    bool var_26;
    uint64_t *var_27;
    uint64_t *var_28;
    uint64_t *var_29;
    uint64_t var_30;
    uint64_t var_31;
    uint64_t r8_5;
    uint64_t r10_5;
    uint64_t r9_5;
    uint64_t rbx_3;
    uint64_t var_38;
    uint64_t var_39;
    uint32_t *var_40;
    unsigned char *var_41;
    uint64_t *var_42;
    unsigned char *var_43;
    uint64_t *var_44;
    uint64_t *var_45;
    uint64_t *var_46;
    uint64_t *var_47;
    uint64_t *var_48;
    uint64_t r10_6;
    uint64_t r9_6;
    uint64_t rbx_4;
    uint64_t var_49;
    uint64_t var_50;
    uint64_t **var_51;
    uint64_t var_52;
    uint64_t **var_53;
    uint64_t var_54;
    uint64_t var_55;
    struct indirect_placeholder_35_ret_type var_56;
    uint64_t var_57;
    uint64_t var_58;
    uint64_t var_59;
    uint64_t var_60;
    uint32_t var_61;
    uint64_t var_115;
    uint64_t *var_116;
    unsigned char *var_117;
    unsigned char var_118;
    bool var_119;
    uint64_t local_sp_14;
    uint64_t var_120;
    uint64_t var_121;
    uint64_t r10_7;
    uint64_t r9_7;
    uint64_t rbx_5;
    uint64_t var_122;
    uint64_t var_123;
    struct indirect_placeholder_36_ret_type var_124;
    uint64_t var_125;
    uint64_t var_126;
    uint64_t var_127;
    uint64_t var_128;
    uint64_t var_129;
    uint64_t r8_8;
    uint64_t r10_8;
    uint64_t r9_8;
    uint64_t rbx_6;
    bool var_132;
    uint64_t local_sp_17;
    uint64_t var_133;
    uint64_t var_134;
    uint64_t r10_9;
    uint64_t r9_9;
    uint64_t rbx_7;
    uint64_t var_135;
    uint64_t var_136;
    struct indirect_placeholder_37_ret_type var_137;
    uint64_t var_138;
    uint64_t var_139;
    uint64_t var_140;
    uint64_t var_141;
    uint64_t var_144;
    unsigned int loop_state_var;
    var_0 = revng_init_local_sp(0UL);
    var_1 = init_rbp();
    var_2 = init_r8();
    var_3 = init_r10();
    var_4 = init_r9();
    var_5 = init_rbx();
    *(uint64_t *)(var_0 + (-8L)) = var_1;
    var_6 = (uint64_t *)(var_0 + (-160L));
    *var_6 = rdi;
    var_7 = (uint64_t *)(var_0 + (-168L));
    *var_7 = rsi;
    var_8 = *var_6;
    *(uint64_t *)(var_0 + (-176L)) = 4205844UL;
    indirect_placeholder_26(var_8, 2UL);
    var_9 = *var_7;
    *(uint64_t *)(var_0 + (-184L)) = 4205864UL;
    indirect_placeholder_26(var_9, 2UL);
    var_10 = var_0 + (-104L);
    *(uint64_t *)(var_0 + (-192L)) = 4205876UL;
    indirect_placeholder_3(var_10);
    var_11 = *var_6;
    *(uint64_t *)(var_0 + (-200L)) = 4205903UL;
    var_12 = indirect_placeholder_39(1UL, var_11, var_10, var_2, var_3, var_4, var_5);
    var_13 = var_12.field_1;
    var_14 = var_12.field_2;
    var_15 = var_12.field_3;
    var_16 = var_12.field_4;
    var_17 = var_0 + (-136L);
    *(uint64_t *)(var_0 + (-208L)) = 4205915UL;
    indirect_placeholder_3(var_17);
    var_18 = *var_7;
    var_19 = var_0 + (-216L);
    *(uint64_t *)var_19 = 4205942UL;
    var_20 = indirect_placeholder_38(2UL, var_18, var_17, var_13, var_14, var_15, var_16);
    var_21 = var_20.field_1;
    var_22 = var_20.field_2;
    var_23 = var_20.field_3;
    var_24 = var_20.field_4;
    local_sp_12 = var_19;
    var_99 = 0UL;
    var_100 = 0UL;
    storemerge = 6392576UL;
    r8_4 = var_21;
    r10_4 = var_22;
    r9_4 = var_23;
    rbx_2 = var_24;
    storemerge22 = 0UL;
    storemerge21 = 0UL;
    storemerge20 = 6392576UL;
    r8_5 = var_21;
    r10_5 = var_22;
    r9_5 = var_23;
    rbx_3 = var_24;
    var_118 = (unsigned char)'\x00';
    if (*(unsigned char *)6392496UL != '\x00') {
        if (*(uint64_t *)var_10 == 0UL) {
            storemerge22 = *(uint64_t *)(**(uint64_t **)(var_0 + (-88L)) + 24UL);
        }
        *(uint64_t *)6392504UL = storemerge22;
        if (*(uint64_t *)var_17 != 0UL) {
            storemerge21 = *(uint64_t *)(**(uint64_t **)(var_0 + (-120L)) + 24UL);
        }
        *(uint64_t *)6392512UL = storemerge21;
    }
    if (*(unsigned char *)6392625UL == '\x00') {
        _pre_phi319 = (uint64_t *)var_10;
        _pre_phi = (uint64_t *)var_17;
    } else {
        var_25 = (uint64_t *)var_10;
        var_26 = (*var_25 == 0UL);
        _pre_phi319 = var_25;
        if (var_26) {
            var_27 = (uint64_t *)var_17;
            _pre_phi = var_27;
            if (*var_27 != 0UL) {
                if (var_26) {
                    storemerge20 = **(uint64_t **)(var_0 + (-88L));
                }
                var_28 = (uint64_t *)(var_0 + (-48L));
                *var_28 = storemerge20;
                var_29 = (uint64_t *)var_17;
                _pre_phi = var_29;
                if (*var_29 == 0UL) {
                    storemerge = **(uint64_t **)(var_0 + (-120L));
                }
                *(uint64_t *)(var_0 + (-56L)) = storemerge;
                var_30 = *var_28;
                var_31 = var_0 + (-224L);
                *(uint64_t *)var_31 = 4206131UL;
                indirect_placeholder_26(var_30, storemerge);
                *(uint64_t *)6392416UL = 0UL;
                *(uint64_t *)6392424UL = 0UL;
                local_sp_11 = var_31;
                if (*var_25 == 0UL) {
                    var_32 = *var_6;
                    var_33 = var_0 + (-232L);
                    *(uint64_t *)var_33 = 4206191UL;
                    var_34 = indirect_placeholder_33(1UL, 1UL, var_32, var_10, var_21, var_22, var_23, var_24);
                    local_sp_11 = var_33;
                    r8_4 = var_34.field_1;
                    r10_4 = var_34.field_2;
                    r9_4 = var_34.field_3;
                    rbx_2 = var_34.field_4;
                }
                local_sp_12 = local_sp_11;
                r8_5 = r8_4;
                r10_5 = r10_4;
                r9_5 = r9_4;
                rbx_3 = rbx_2;
                if (*var_29 == 0UL) {
                    var_35 = *var_7;
                    var_36 = local_sp_11 + (-8L);
                    *(uint64_t *)var_36 = 4206229UL;
                    var_37 = indirect_placeholder_34(1UL, 2UL, var_35, var_17, r8_4, r10_4, r9_4, rbx_2);
                    local_sp_12 = var_36;
                    r8_5 = var_37.field_1;
                    r10_5 = var_37.field_2;
                    r9_5 = var_37.field_3;
                    rbx_3 = var_37.field_4;
                }
            }
        } else {
            if (var_26) {
                storemerge20 = **(uint64_t **)(var_0 + (-88L));
            }
            var_28 = (uint64_t *)(var_0 + (-48L));
            *var_28 = storemerge20;
            var_29 = (uint64_t *)var_17;
            _pre_phi = var_29;
            if (*var_29 == 0UL) {
                storemerge = **(uint64_t **)(var_0 + (-120L));
            }
            *(uint64_t *)(var_0 + (-56L)) = storemerge;
            var_30 = *var_28;
            var_31 = var_0 + (-224L);
            *(uint64_t *)var_31 = 4206131UL;
            indirect_placeholder_26(var_30, storemerge);
            *(uint64_t *)6392416UL = 0UL;
            *(uint64_t *)6392424UL = 0UL;
            local_sp_11 = var_31;
            if (*var_25 == 0UL) {
                var_32 = *var_6;
                var_33 = var_0 + (-232L);
                *(uint64_t *)var_33 = 4206191UL;
                var_34 = indirect_placeholder_33(1UL, 1UL, var_32, var_10, var_21, var_22, var_23, var_24);
                local_sp_11 = var_33;
                r8_4 = var_34.field_1;
                r10_4 = var_34.field_2;
                r9_4 = var_34.field_3;
                rbx_2 = var_34.field_4;
            }
            local_sp_12 = local_sp_11;
            r8_5 = r8_4;
            r10_5 = r10_4;
            r9_5 = r9_4;
            rbx_3 = rbx_2;
            if (*var_29 == 0UL) {
                var_35 = *var_7;
                var_36 = local_sp_11 + (-8L);
                *(uint64_t *)var_36 = 4206229UL;
                var_37 = indirect_placeholder_34(1UL, 2UL, var_35, var_17, r8_4, r10_4, r9_4, rbx_2);
                local_sp_12 = var_36;
                r8_5 = var_37.field_1;
                r10_5 = var_37.field_2;
                r9_5 = var_37.field_3;
                rbx_3 = var_37.field_4;
            }
        }
    }
    var_38 = var_0 + (-120L);
    var_39 = var_0 + (-88L);
    var_40 = (uint32_t *)(var_0 + (-60L));
    var_41 = (unsigned char *)(var_0 + (-9L));
    var_42 = (uint64_t *)var_39;
    var_43 = (unsigned char *)(var_0 + (-10L));
    var_44 = (uint64_t *)var_38;
    var_45 = (uint64_t *)(var_0 + (-24L));
    var_46 = (uint64_t *)(var_0 + (-32L));
    var_47 = (uint64_t *)(var_0 + (-72L));
    var_48 = (uint64_t *)(var_0 + (-80L));
    local_sp_13 = local_sp_12;
    r8_6 = r8_5;
    r10_6 = r10_5;
    r9_6 = r9_5;
    rbx_4 = rbx_3;
    r8_7 = r8_6;
    local_sp_16 = local_sp_13;
    rbx_1 = rbx_4;
    local_sp_14 = local_sp_13;
    r10_7 = r10_6;
    r9_7 = r9_6;
    rbx_5 = rbx_4;
    r8_8 = r8_6;
    r10_8 = r10_6;
    r9_8 = r9_6;
    rbx_6 = rbx_4;
    while (*_pre_phi319 != 0UL)
        {
            var_49 = *(uint64_t *)6392008UL;
            var_50 = *(uint64_t *)6392000UL;
            var_51 = (uint64_t **)var_38;
            var_52 = **var_51;
            var_53 = (uint64_t **)var_39;
            var_54 = **var_53;
            var_55 = local_sp_13 + (-8L);
            *(uint64_t *)var_55 = 4206270UL;
            var_56 = indirect_placeholder_35(var_50, var_49, var_54, var_52);
            var_57 = var_56.field_0;
            var_58 = var_56.field_1;
            var_59 = var_56.field_2;
            var_60 = var_56.field_3;
            var_61 = (uint32_t)var_57;
            *var_40 = var_61;
            local_sp_9 = var_55;
            local_sp_8 = var_55;
            local_sp_10 = var_55;
            r8_3 = var_58;
            r10_3 = var_59;
            r9_3 = var_60;
            if ((int)var_61 > (int)4294967295U) {
                if ((int)var_61 > (int)0U) {
                    if (*(unsigned char *)6392482UL != '\x00') {
                        var_106 = **var_51;
                        var_107 = local_sp_13 + (-16L);
                        *(uint64_t *)var_107 = 4206388UL;
                        indirect_placeholder_26(6392576UL, var_106);
                        local_sp_9 = var_107;
                    }
                    var_108 = *var_7;
                    var_109 = local_sp_9 + (-8L);
                    *(uint64_t *)var_109 = 4206417UL;
                    var_110 = indirect_placeholder_31(1UL, 2UL, var_108, var_17, var_58, var_59, var_60, rbx_4);
                    var_111 = var_110.field_1;
                    var_112 = var_110.field_2;
                    var_113 = var_110.field_3;
                    var_114 = var_110.field_4;
                    *(unsigned char *)6392484UL = (unsigned char)'\x01';
                    local_sp_13_be = var_109;
                    r8_6_be = var_111;
                    r10_6_be = var_112;
                    r9_6_be = var_113;
                    rbx_4_be = var_114;
                } else {
                    *var_41 = (unsigned char)'\x00';
                    while (1U)
                        {
                            var_71 = *var_6;
                            var_72 = local_sp_10 + (-8L);
                            *(uint64_t *)var_72 = 4206462UL;
                            var_73 = indirect_placeholder_32(0UL, 1UL, var_71, var_10, r8_3, r10_3, r9_3, rbx_1);
                            var_74 = var_73.field_0;
                            var_75 = var_73.field_4;
                            local_sp_6 = var_72;
                            rbx_0 = var_75;
                            rbx_1 = var_75;
                            if ((uint64_t)(unsigned char)var_74 != 1UL) {
                                loop_state_var = 0U;
                                break;
                            }
                            var_79 = *(uint64_t *)6392008UL;
                            var_80 = *(uint64_t *)6392000UL;
                            var_81 = **var_51;
                            var_82 = *(uint64_t *)(*var_42 + ((*_pre_phi319 << 3UL) + (-8L)));
                            var_83 = local_sp_10 + (-16L);
                            *(uint64_t *)var_83 = 4206538UL;
                            var_84 = indirect_placeholder_29(var_80, var_79, var_82, var_81);
                            local_sp_6 = var_83;
                            local_sp_10 = var_83;
                            if ((uint64_t)(uint32_t)var_84.field_0 != 0UL) {
                                loop_state_var = 1U;
                                break;
                            }
                            r8_3 = var_84.field_1;
                            r10_3 = var_84.field_2;
                            r9_3 = var_84.field_3;
                            continue;
                        }
                    switch (loop_state_var) {
                      case 0U:
                        {
                            var_76 = var_73.field_1;
                            var_77 = var_73.field_2;
                            var_78 = var_73.field_3;
                            *var_41 = (unsigned char)'\x01';
                            *_pre_phi319 = (*_pre_phi319 + 1UL);
                            r8_1 = var_76;
                            r10_1 = var_77;
                            r9_1 = var_78;
                        }
                        break;
                      case 1U:
                        {
                            r8_1 = var_84.field_1;
                            r10_1 = var_84.field_2;
                            r9_1 = var_84.field_3;
                        }
                        break;
                    }
                    *var_43 = (unsigned char)'\x00';
                    local_sp_7 = local_sp_6;
                    r8_2 = r8_1;
                    r10_2 = r10_1;
                    r9_2 = r9_1;
                    while (1U)
                        {
                            var_85 = *var_7;
                            var_86 = local_sp_7 + (-8L);
                            *(uint64_t *)var_86 = 4206575UL;
                            var_87 = indirect_placeholder_28(0UL, 2UL, var_85, var_17, r8_2, r10_2, r9_2, rbx_0);
                            var_88 = var_87.field_0;
                            var_89 = var_87.field_4;
                            local_sp_2 = var_86;
                            rbx_4_be = var_89;
                            rbx_0 = var_89;
                            if ((uint64_t)(unsigned char)var_88 != 1UL) {
                                loop_state_var = 1U;
                                break;
                            }
                            var_93 = *(uint64_t *)6392008UL;
                            var_94 = *(uint64_t *)6392000UL;
                            var_95 = *(uint64_t *)(*var_44 + ((*_pre_phi << 3UL) + (-8L)));
                            var_96 = **var_53;
                            var_97 = local_sp_7 + (-16L);
                            *(uint64_t *)var_97 = 4206651UL;
                            var_98 = indirect_placeholder_27(var_94, var_93, var_96, var_95);
                            local_sp_2 = var_97;
                            local_sp_7 = var_97;
                            if ((uint64_t)(uint32_t)var_98.field_0 != 0UL) {
                                loop_state_var = 0U;
                                break;
                            }
                            r8_2 = var_98.field_1;
                            r10_2 = var_98.field_2;
                            r9_2 = var_98.field_3;
                            continue;
                        }
                    switch (loop_state_var) {
                      case 0U:
                        {
                            r8_0 = var_98.field_1;
                            r10_0 = var_98.field_2;
                            r9_0 = var_98.field_3;
                        }
                        break;
                      case 1U:
                        {
                            var_90 = var_87.field_1;
                            var_91 = var_87.field_2;
                            var_92 = var_87.field_3;
                            *var_43 = (unsigned char)'\x01';
                            *_pre_phi = (*_pre_phi + 1UL);
                            r8_0 = var_90;
                            r10_0 = var_91;
                            r9_0 = var_92;
                        }
                        break;
                    }
                    local_sp_5 = local_sp_2;
                    local_sp_3 = local_sp_2;
                    r8_6_be = r8_0;
                    r10_6_be = r10_0;
                    r9_6_be = r9_0;
                    if (*(unsigned char *)6392483UL != '\x00') {
                        *var_45 = 0UL;
                        local_sp_4 = local_sp_3;
                        local_sp_5 = local_sp_3;
                        while ((*_pre_phi319 + (-1L)) <= var_99)
                            {
                                *var_46 = 0UL;
                                local_sp_3 = local_sp_4;
                                while ((*_pre_phi + (-1L)) <= var_100)
                                    {
                                        var_102 = *(uint64_t *)(*var_44 + (var_100 << 3UL));
                                        var_103 = *(uint64_t *)(*var_42 + (*var_45 << 3UL));
                                        var_104 = local_sp_4 + (-8L);
                                        *(uint64_t *)var_104 = 4206733UL;
                                        indirect_placeholder_26(var_103, var_102);
                                        var_105 = *var_46 + 1UL;
                                        *var_46 = var_105;
                                        var_100 = var_105;
                                        local_sp_4 = var_104;
                                        local_sp_3 = local_sp_4;
                                    }
                                var_101 = *var_45 + 1UL;
                                *var_45 = var_101;
                                var_99 = var_101;
                                local_sp_4 = local_sp_3;
                                local_sp_5 = local_sp_3;
                            }
                    }
                    local_sp_13_be = local_sp_5;
                    if (*var_41 == '\x01') {
                        *_pre_phi319 = 0UL;
                    } else {
                        *var_47 = **var_53;
                        **var_53 = *(uint64_t *)(*var_42 + ((*_pre_phi319 << 3UL) + (-8L)));
                        *(uint64_t *)(((*_pre_phi319 << 3UL) + (-8L)) + *var_42) = *var_47;
                        *_pre_phi319 = 1UL;
                    }
                    if (*var_43 == '\x01') {
                        *_pre_phi = 0UL;
                    } else {
                        *var_48 = **var_51;
                        **var_51 = *(uint64_t *)(*var_44 + ((*_pre_phi << 3UL) + (-8L)));
                        *(uint64_t *)(((*_pre_phi << 3UL) + (-8L)) + *var_44) = *var_48;
                        *_pre_phi = 1UL;
                    }
                }
            } else {
                if (*(unsigned char *)6392481UL != '\x00') {
                    var_62 = **var_53;
                    var_63 = local_sp_13 + (-16L);
                    *(uint64_t *)var_63 = 4206310UL;
                    indirect_placeholder_26(var_62, 6392576UL);
                    local_sp_8 = var_63;
                }
                var_64 = *var_6;
                var_65 = local_sp_8 + (-8L);
                *(uint64_t *)var_65 = 4206339UL;
                var_66 = indirect_placeholder_30(1UL, 1UL, var_64, var_10, var_58, var_59, var_60, rbx_4);
                var_67 = var_66.field_1;
                var_68 = var_66.field_2;
                var_69 = var_66.field_3;
                var_70 = var_66.field_4;
                *(unsigned char *)6392484UL = (unsigned char)'\x01';
                local_sp_13_be = var_65;
                r8_6_be = var_67;
                r10_6_be = var_68;
                r9_6_be = var_69;
                rbx_4_be = var_70;
            }
            local_sp_13 = local_sp_13_be;
            r8_6 = r8_6_be;
            r10_6 = r10_6_be;
            r9_6 = r9_6_be;
            rbx_4 = rbx_4_be;
            r8_7 = r8_6;
            local_sp_16 = local_sp_13;
            rbx_1 = rbx_4;
            local_sp_14 = local_sp_13;
            r10_7 = r10_6;
            r9_7 = r9_6;
            rbx_5 = rbx_4;
            r8_8 = r8_6;
            r10_8 = r10_6;
            r9_8 = r9_6;
            rbx_6 = rbx_4;
        }
    var_115 = var_0 + (-144L);
    var_116 = (uint64_t *)var_115;
    *var_116 = 0UL;
    var_117 = (unsigned char *)(var_0 + (-33L));
    *var_117 = (unsigned char)'\x00';
    if (*(uint32_t *)6392552UL != 2U) {
        var_118 = (unsigned char)'\x01';
        if (*(unsigned char *)6392485UL == '\x01') {
            *var_117 = (unsigned char)'\x01';
        } else {
            if (*(unsigned char *)6392486UL == '\x01') {
                *var_117 = (unsigned char)'\x01';
            }
        }
    }
    var_119 = (*(unsigned char *)6392481UL == '\x00');
    if (!(var_119 && var_118 == '\x00') & *_pre_phi319 != 0UL) {
        if (var_119) {
            var_120 = **(uint64_t **)var_39;
            var_121 = local_sp_13 + (-8L);
            *(uint64_t *)var_121 = 4207106UL;
            indirect_placeholder_26(var_120, 6392576UL);
            local_sp_14 = var_121;
        }
        local_sp_15 = local_sp_14;
        if (*_pre_phi == 0UL) {
            *(unsigned char *)6392484UL = (unsigned char)'\x01';
        }
        var_122 = *var_6;
        var_123 = local_sp_15 + (-8L);
        *(uint64_t *)var_123 = 4207212UL;
        var_124 = indirect_placeholder_36(1UL, var_115, var_122, var_115, r8_7, r10_7, r9_7, rbx_5);
        var_125 = var_124.field_0;
        var_126 = var_124.field_1;
        var_127 = var_124.field_2;
        var_128 = var_124.field_3;
        var_129 = var_124.field_4;
        local_sp_1 = var_123;
        r8_7 = var_126;
        local_sp_16 = var_123;
        r10_7 = var_127;
        r9_7 = var_128;
        rbx_5 = var_129;
        r8_8 = var_126;
        r10_8 = var_127;
        r9_8 = var_128;
        rbx_6 = var_129;
        while ((uint64_t)(unsigned char)var_125 != 0UL)
            {
                if (*(unsigned char *)6392481UL == '\x00') {
                    var_130 = *var_116;
                    var_131 = local_sp_15 + (-16L);
                    *(uint64_t *)var_131 = 4207155UL;
                    indirect_placeholder_26(var_130, 6392576UL);
                    local_sp_1 = var_131;
                }
                local_sp_15 = local_sp_1;
                local_sp_16 = local_sp_1;
                if (*(unsigned char *)6392485UL != '\x00') {
                    var_122 = *var_6;
                    var_123 = local_sp_15 + (-8L);
                    *(uint64_t *)var_123 = 4207212UL;
                    var_124 = indirect_placeholder_36(1UL, var_115, var_122, var_115, r8_7, r10_7, r9_7, rbx_5);
                    var_125 = var_124.field_0;
                    var_126 = var_124.field_1;
                    var_127 = var_124.field_2;
                    var_128 = var_124.field_3;
                    var_129 = var_124.field_4;
                    local_sp_1 = var_123;
                    r8_7 = var_126;
                    local_sp_16 = var_123;
                    r10_7 = var_127;
                    r9_7 = var_128;
                    rbx_5 = var_129;
                    r8_8 = var_126;
                    r10_8 = var_127;
                    r9_8 = var_128;
                    rbx_6 = var_129;
                    continue;
                }
                if (*(unsigned char *)6392481UL == '\x01') {
                    break;
                }
                var_122 = *var_6;
                var_123 = local_sp_15 + (-8L);
                *(uint64_t *)var_123 = 4207212UL;
                var_124 = indirect_placeholder_36(1UL, var_115, var_122, var_115, r8_7, r10_7, r9_7, rbx_5);
                var_125 = var_124.field_0;
                var_126 = var_124.field_1;
                var_127 = var_124.field_2;
                var_128 = var_124.field_3;
                var_129 = var_124.field_4;
                local_sp_1 = var_123;
                r8_7 = var_126;
                local_sp_16 = var_123;
                r10_7 = var_127;
                r9_7 = var_128;
                rbx_5 = var_129;
                r8_8 = var_126;
                r10_8 = var_127;
                r9_8 = var_128;
                rbx_6 = var_129;
            }
    }
    var_132 = (*(unsigned char *)6392482UL == '\x00');
    r8_9 = r8_8;
    local_sp_19 = local_sp_16;
    local_sp_17 = local_sp_16;
    r10_9 = r10_8;
    r9_9 = r9_8;
    rbx_7 = rbx_6;
    if (!var_132) {
        if (*var_117 == '\x00') {
            var_144 = *var_116;
            *(uint64_t *)(local_sp_19 + (-8L)) = 4207406UL;
            indirect_placeholder_3(var_144);
            *(uint64_t *)(local_sp_19 + (-16L)) = 4207421UL;
            indirect_placeholder();
            *(uint64_t *)(local_sp_19 + (-24L)) = 4207433UL;
            indirect_placeholder_3(var_10);
            *(uint64_t *)(local_sp_19 + (-32L)) = 4207445UL;
            indirect_placeholder_3(var_17);
            return;
        }
    }
    if (*_pre_phi == 0UL) {
        var_144 = *var_116;
        *(uint64_t *)(local_sp_19 + (-8L)) = 4207406UL;
        indirect_placeholder_3(var_144);
        *(uint64_t *)(local_sp_19 + (-16L)) = 4207421UL;
        indirect_placeholder();
        *(uint64_t *)(local_sp_19 + (-24L)) = 4207433UL;
        indirect_placeholder_3(var_10);
        *(uint64_t *)(local_sp_19 + (-32L)) = 4207445UL;
        indirect_placeholder_3(var_17);
        return;
    }
    if (var_132) {
        var_133 = **(uint64_t **)var_38;
        var_134 = local_sp_16 + (-8L);
        *(uint64_t *)var_134 = 4207281UL;
        indirect_placeholder_26(6392576UL, var_133);
        local_sp_17 = var_134;
    }
    local_sp_18 = local_sp_17;
    if (*_pre_phi319 == 0UL) {
        *(unsigned char *)6392484UL = (unsigned char)'\x01';
    }
    var_135 = *var_7;
    var_136 = local_sp_18 + (-8L);
    *(uint64_t *)var_136 = 4207387UL;
    var_137 = indirect_placeholder_37(2UL, var_115, var_135, var_115, r8_9, r10_9, r9_9, rbx_7);
    var_138 = var_137.field_1;
    var_139 = var_137.field_2;
    var_140 = var_137.field_3;
    var_141 = var_137.field_4;
    local_sp_0 = var_136;
    r8_9 = var_138;
    local_sp_19 = var_136;
    r10_9 = var_139;
    r9_9 = var_140;
    rbx_7 = var_141;
    while ((uint64_t)(unsigned char)var_137.field_0 != 0UL)
        {
            if (*(unsigned char *)6392482UL == '\x00') {
                var_142 = *var_116;
                var_143 = local_sp_18 + (-16L);
                *(uint64_t *)var_143 = 4207330UL;
                indirect_placeholder_26(6392576UL, var_142);
                local_sp_0 = var_143;
            }
            local_sp_18 = local_sp_0;
            local_sp_19 = local_sp_0;
            if (*(unsigned char *)6392486UL != '\x00') {
                var_135 = *var_7;
                var_136 = local_sp_18 + (-8L);
                *(uint64_t *)var_136 = 4207387UL;
                var_137 = indirect_placeholder_37(2UL, var_115, var_135, var_115, r8_9, r10_9, r9_9, rbx_7);
                var_138 = var_137.field_1;
                var_139 = var_137.field_2;
                var_140 = var_137.field_3;
                var_141 = var_137.field_4;
                local_sp_0 = var_136;
                r8_9 = var_138;
                local_sp_19 = var_136;
                r10_9 = var_139;
                r9_9 = var_140;
                rbx_7 = var_141;
                continue;
            }
            if (*(unsigned char *)6392482UL == '\x01') {
                break;
            }
            var_135 = *var_7;
            var_136 = local_sp_18 + (-8L);
            *(uint64_t *)var_136 = 4207387UL;
            var_137 = indirect_placeholder_37(2UL, var_115, var_135, var_115, r8_9, r10_9, r9_9, rbx_7);
            var_138 = var_137.field_1;
            var_139 = var_137.field_2;
            var_140 = var_137.field_3;
            var_141 = var_137.field_4;
            local_sp_0 = var_136;
            r8_9 = var_138;
            local_sp_19 = var_136;
            r10_9 = var_139;
            r9_9 = var_140;
            rbx_7 = var_141;
        }
    var_144 = *var_116;
    *(uint64_t *)(local_sp_19 + (-8L)) = 4207406UL;
    indirect_placeholder_3(var_144);
    *(uint64_t *)(local_sp_19 + (-16L)) = 4207421UL;
    indirect_placeholder();
    *(uint64_t *)(local_sp_19 + (-24L)) = 4207433UL;
    indirect_placeholder_3(var_10);
    *(uint64_t *)(local_sp_19 + (-32L)) = 4207445UL;
    indirect_placeholder_3(var_17);
    return;
}

typedef unsigned char __u_char;
typedef unsigned short __u_short;
typedef unsigned int __u_int;
typedef unsigned long __u_long;
typedef signed char __int8_t;
typedef unsigned char __uint8_t;
typedef short __int16_t;
typedef unsigned short __uint16_t;
typedef int __int32_t;
typedef unsigned int __uint32_t;
typedef long __int64_t;
typedef unsigned long __uint64_t;
typedef __int8_t __int_least8_t;
typedef __uint8_t __uint_least8_t;
typedef __int16_t __int_least16_t;
typedef __uint16_t __uint_least16_t;
typedef __int32_t __int_least32_t;
typedef __uint32_t __uint_least32_t;
typedef __int64_t __int_least64_t;
typedef __uint64_t __uint_least64_t;
typedef long __quad_t;
typedef unsigned long __u_quad_t;
typedef long __intmax_t;
typedef unsigned long __uintmax_t;
typedef unsigned long __dev_t;
typedef unsigned int __uid_t;
typedef unsigned int __gid_t;
typedef unsigned long __ino_t;
typedef unsigned long __ino64_t;
typedef unsigned int __mode_t;
typedef unsigned long __nlink_t;
typedef long __off_t;
typedef long __off64_t;
typedef int __pid_t;
typedef struct {
    int __val[2];
} __fsid_t;
typedef long __clock_t;
typedef unsigned long __rlim_t;
typedef unsigned long __rlim64_t;
typedef unsigned int __id_t;
typedef long __time_t;
typedef unsigned int __useconds_t;
typedef long __suseconds_t;
typedef int __daddr_t;
typedef int __key_t;
typedef int __clockid_t;
typedef void *__timer_t;
typedef long __blksize_t;
typedef long __blkcnt_t;
typedef long __blkcnt64_t;
typedef unsigned long __fsblkcnt_t;
typedef unsigned long __fsblkcnt64_t;
typedef unsigned long __fsfilcnt_t;
typedef unsigned long __fsfilcnt64_t;
typedef long __fsword_t;
typedef long __ssize_t;
typedef long __syscall_slong_t;
typedef unsigned long __syscall_ulong_t;
typedef __off64_t __loff_t;
typedef char *__caddr_t;
typedef long __intptr_t;
typedef unsigned int __socklen_t;
typedef int __sig_atomic_t;
typedef __int8_t int8_t;
typedef __int16_t int16_t;
typedef __int32_t int32_t;
typedef __int64_t int64_t;
typedef __uint8_t uint8_t;
typedef __uint16_t uint16_t;
typedef __uint32_t uint32_t;
typedef __uint64_t uint64_t;
typedef __int_least8_t int_least8_t;
typedef __int_least16_t int_least16_t;
typedef __int_least32_t int_least32_t;
typedef __int_least64_t int_least64_t;
typedef __uint_least8_t uint_least8_t;
typedef __uint_least16_t uint_least16_t;
typedef __uint_least32_t uint_least32_t;
typedef __uint_least64_t uint_least64_t;
typedef signed char int_fast8_t;
typedef long int_fast16_t;
typedef long int_fast32_t;
typedef long int_fast64_t;
typedef unsigned char uint_fast8_t;
typedef unsigned long uint_fast16_t;
typedef unsigned long uint_fast32_t;
typedef unsigned long uint_fast64_t;
typedef long intptr_t;
typedef unsigned long uintptr_t;
typedef __intmax_t intmax_t;
typedef __uintmax_t uintmax_t;
typedef _Bool bool;
struct indirect_placeholder_19_ret_type;
struct indirect_placeholder_20_ret_type;
struct indirect_placeholder_21_ret_type;
struct indirect_placeholder_22_ret_type;
struct indirect_placeholder_19_ret_type {
    uint64_t field_0;
    uint64_t field_1;
    uint64_t field_2;
    uint64_t field_3;
};
struct indirect_placeholder_20_ret_type {
    uint64_t field_0;
    uint64_t field_1;
    uint64_t field_2;
    uint64_t field_3;
};
struct indirect_placeholder_21_ret_type {
    uint64_t field_0;
    uint64_t field_1;
    uint64_t field_2;
    uint64_t field_3;
};
struct indirect_placeholder_22_ret_type {
    uint64_t field_0;
    uint64_t field_1;
    uint64_t field_2;
    uint64_t field_3;
};
extern uint64_t revng_init_local_sp(uint64_t param_0);
extern uint64_t init_rbp(void);
extern uint64_t init_cc_src2(void);
extern uint64_t helper_cc_compute_c_wrapper(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint32_t param_3);
extern void indirect_placeholder(void);
extern uint64_t init_rbx(void);
extern uint64_t init_r10(void);
extern uint64_t init_r9(void);
extern uint64_t init_r8(void);
extern uint64_t indirect_placeholder_17(uint64_t param_0);
extern uint64_t init_rcx(void);
extern struct indirect_placeholder_19_ret_type indirect_placeholder_19(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint64_t param_3, uint64_t param_4, uint64_t param_5, uint64_t param_6);
extern struct indirect_placeholder_20_ret_type indirect_placeholder_20(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint64_t param_3, uint64_t param_4, uint64_t param_5, uint64_t param_6);
extern struct indirect_placeholder_21_ret_type indirect_placeholder_21(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint64_t param_3, uint64_t param_4, uint64_t param_5, uint64_t param_6);
extern struct indirect_placeholder_22_ret_type indirect_placeholder_22(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint64_t param_3, uint64_t param_4, uint64_t param_5, uint64_t param_6);
void bb_unexpand(void) {
    struct indirect_placeholder_20_ret_type var_48;
    struct indirect_placeholder_19_ret_type var_66;
    uint64_t var_0;
    uint64_t var_1;
    uint64_t var_2;
    uint64_t var_3;
    uint64_t var_4;
    uint64_t var_5;
    uint64_t var_6;
    uint64_t var_7;
    uint64_t var_8;
    uint64_t *var_9;
    uint64_t var_64;
    uint64_t var_65;
    uint64_t r8_3;
    uint64_t local_sp_3;
    uint64_t rcx_0;
    uint64_t r9_3;
    uint64_t r10_0;
    uint64_t r9_0;
    uint64_t r8_0;
    uint64_t local_sp_0;
    uint64_t rcx_4;
    unsigned char var_43;
    uint64_t r10_7;
    uint64_t rcx_3;
    uint64_t var_44;
    uint64_t var_45;
    uint64_t var_46;
    uint64_t rcx_1;
    uint64_t var_47;
    uint64_t r9_7;
    uint64_t r10_1;
    uint64_t r9_1;
    uint64_t r8_1;
    uint64_t local_sp_1;
    uint64_t var_49;
    uint64_t var_51;
    uint64_t r10_5;
    uint64_t _pre124;
    uint64_t var_50;
    uint64_t var_52;
    uint64_t rcx_2;
    uint64_t r9_5;
    uint64_t r10_2;
    uint64_t r8_5;
    uint64_t r9_2;
    uint64_t local_sp_6;
    uint64_t r8_2;
    uint64_t local_sp_2;
    uint64_t rcx_6;
    uint64_t rcx_7;
    uint64_t r8_7;
    uint64_t var_36;
    uint64_t var_37;
    uint64_t local_sp_8;
    struct indirect_placeholder_21_ret_type var_38;
    uint64_t var_39;
    uint64_t var_40;
    uint64_t var_41;
    uint64_t var_42;
    bool var_53;
    uint64_t var_54;
    uint64_t var_58;
    uint64_t var_59;
    uint64_t var_55;
    uint64_t var_56;
    struct indirect_placeholder_22_ret_type var_57;
    uint64_t r10_3;
    uint64_t var_60;
    uint64_t var_61;
    uint64_t var_62;
    uint64_t var_63;
    uint64_t r10_4;
    uint64_t r9_4;
    uint64_t r8_4;
    uint64_t local_sp_4;
    unsigned char storemerge;
    uint64_t rcx_5;
    uint64_t var_30;
    uint64_t var_31;
    uint64_t var_32;
    uint64_t local_sp_5;
    uint64_t var_27;
    uint32_t var_33;
    uint64_t var_34;
    unsigned char var_35;
    bool var_67;
    uint64_t var_68;
    uint64_t *var_69;
    uint64_t r10_6;
    uint64_t r9_6;
    uint64_t r8_6;
    uint64_t local_sp_7;
    uint64_t local_sp_9;
    uint64_t var_28;
    uint32_t var_29;
    uint64_t var_10;
    uint64_t var_11;
    uint64_t var_12;
    uint64_t var_13;
    uint64_t *var_14;
    unsigned char *var_15;
    uint64_t *var_16;
    uint64_t *var_17;
    uint64_t var_18;
    uint64_t *var_19;
    unsigned char *var_20;
    unsigned char *var_21;
    uint64_t *var_22;
    uint32_t *var_23;
    unsigned char *var_24;
    uint64_t var_25;
    unsigned char *var_26;
    unsigned int loop_state_var;
    bool switch_state_var;
    var_0 = revng_init_local_sp(0UL);
    var_1 = init_rbp();
    var_2 = init_cc_src2();
    var_3 = init_rcx();
    var_4 = init_r10();
    var_5 = init_r9();
    var_6 = init_r8();
    var_7 = init_rbx();
    *(uint64_t *)(var_0 + (-8L)) = var_1;
    *(uint64_t *)(var_0 + (-96L)) = 4202306UL;
    var_8 = indirect_placeholder_17(0UL);
    var_9 = (uint64_t *)(var_0 + (-16L));
    *var_9 = var_8;
    var_43 = (unsigned char)'\x00';
    rcx_6 = var_3;
    storemerge = (unsigned char)'\x01';
    r10_6 = var_4;
    r9_6 = var_5;
    r8_6 = var_6;
    if (var_8 == 0UL) {
        return;
    }
    var_10 = *(uint64_t *)6380736UL;
    var_11 = var_0 + (-104L);
    *(uint64_t *)var_11 = 4202337UL;
    var_12 = indirect_placeholder_17(var_10);
    var_13 = var_0 + (-56L);
    var_14 = (uint64_t *)var_13;
    *var_14 = var_12;
    var_15 = (unsigned char *)(var_0 + (-21L));
    var_16 = (uint64_t *)(var_0 + (-32L));
    var_17 = (uint64_t *)(var_0 + (-64L));
    var_18 = var_0 + (-80L);
    var_19 = (uint64_t *)var_18;
    var_20 = (unsigned char *)(var_0 + (-33L));
    var_21 = (unsigned char *)(var_0 + (-34L));
    var_22 = (uint64_t *)(var_0 + (-48L));
    var_23 = (uint32_t *)(var_0 + (-20L));
    var_24 = (unsigned char *)(var_0 + (-65L));
    var_25 = var_0 + (-81L);
    var_26 = (unsigned char *)var_25;
    local_sp_7 = var_11;
    while (1U)
        {
            *var_15 = (unsigned char)'\x01';
            *var_16 = 0UL;
            *var_17 = 0UL;
            *var_19 = 0UL;
            *var_20 = (unsigned char)'\x00';
            *var_21 = (unsigned char)'\x01';
            *var_22 = 0UL;
            r10_7 = r10_6;
            r9_7 = r9_6;
            rcx_7 = rcx_6;
            r8_7 = r8_6;
            local_sp_8 = local_sp_7;
            while (1U)
                {
                    r8_3 = r8_7;
                    r9_3 = r9_7;
                    rcx_3 = rcx_7;
                    r10_1 = r10_7;
                    r10_5 = r10_7;
                    r9_5 = r9_7;
                    r8_5 = r8_7;
                    r10_3 = r10_7;
                    rcx_5 = rcx_7;
                    var_27 = *var_9;
                    local_sp_9 = local_sp_8;
                    var_28 = local_sp_9 + (-8L);
                    *(uint64_t *)var_28 = 4202400UL;
                    indirect_placeholder();
                    var_29 = (uint32_t)var_27;
                    *var_23 = var_29;
                    local_sp_5 = var_28;
                    while ((int)var_29 <= (int)4294967295U)
                        {
                            var_30 = *var_9;
                            var_31 = local_sp_9 + (-16L);
                            *(uint64_t *)var_31 = 4202421UL;
                            var_32 = indirect_placeholder_17(var_30);
                            *var_9 = var_32;
                            local_sp_5 = var_31;
                            var_27 = var_32;
                            local_sp_9 = var_31;
                            if (var_32 == 0UL) {
                                break;
                            }
                            var_28 = local_sp_9 + (-8L);
                            *(uint64_t *)var_28 = 4202400UL;
                            indirect_placeholder();
                            var_29 = (uint32_t)var_27;
                            *var_23 = var_29;
                            local_sp_5 = var_28;
                        }
                    local_sp_6 = local_sp_5;
                    if (*var_15 != '\x00') {
                        var_33 = *var_23;
                        var_34 = local_sp_5 + (-8L);
                        *(uint64_t *)var_34 = 4202452UL;
                        indirect_placeholder();
                        var_35 = (var_33 != 0U);
                        *var_24 = var_35;
                        local_sp_3 = var_34;
                        if (var_35 == '\x00') {
                            var_53 = (*var_23 == 8U);
                            var_54 = *var_16;
                            if (var_53) {
                                var_58 = var_54 + (var_54 != 0UL);
                                *var_16 = var_58;
                                *var_17 = var_58;
                                var_59 = *var_19;
                                *var_19 = (var_59 + (var_59 != 0UL));
                            } else {
                                var_55 = var_54 + 1UL;
                                *var_16 = var_55;
                                if (var_55 == 0UL) {
                                    var_56 = local_sp_5 + (-16L);
                                    *(uint64_t *)var_56 = 4202778UL;
                                    var_57 = indirect_placeholder_22(0UL, 4270823UL, rcx_7, 0UL, 1UL, r9_7, r8_7);
                                    r8_3 = var_57.field_3;
                                    local_sp_3 = var_56;
                                    r9_3 = var_57.field_2;
                                    rcx_3 = var_57.field_0;
                                    r10_3 = var_57.field_1;
                                }
                            }
                        } else {
                            var_36 = *var_16;
                            var_37 = local_sp_5 + (-16L);
                            *(uint64_t *)var_37 = 4202493UL;
                            var_38 = indirect_placeholder_21(var_25, var_18, var_36, r10_7, r9_7, r8_7, var_7);
                            var_39 = var_38.field_0;
                            var_40 = var_38.field_1;
                            var_41 = var_38.field_2;
                            var_42 = var_38.field_3;
                            *var_17 = var_39;
                            r8_3 = var_42;
                            local_sp_3 = var_37;
                            r9_3 = var_41;
                            rcx_3 = var_40;
                            rcx_1 = var_40;
                            r9_1 = var_41;
                            r8_1 = var_42;
                            local_sp_1 = var_37;
                            if (*var_26 == '\x00') {
                                var_43 = *var_15;
                            } else {
                                *var_15 = (unsigned char)'\x00';
                            }
                            if (var_43 != '\x00') {
                                var_44 = *var_17;
                                var_45 = *var_16;
                                var_46 = helper_cc_compute_c_wrapper(var_44 - var_45, var_45, var_2, 17U);
                                if (var_46 == 0UL) {
                                    var_47 = local_sp_5 + (-24L);
                                    *(uint64_t *)var_47 = 4202554UL;
                                    var_48 = indirect_placeholder_20(0UL, 4270823UL, var_40, 0UL, 1UL, var_41, var_42);
                                    rcx_1 = var_48.field_0;
                                    r10_1 = var_48.field_1;
                                    r9_1 = var_48.field_2;
                                    r8_1 = var_48.field_3;
                                    local_sp_1 = var_47;
                                }
                                r8_3 = r8_1;
                                local_sp_3 = local_sp_1;
                                r9_3 = r9_1;
                                rcx_3 = rcx_1;
                                rcx_2 = rcx_1;
                                r10_2 = r10_1;
                                r9_2 = r9_1;
                                r8_2 = r8_1;
                                local_sp_2 = local_sp_1;
                                r10_3 = r10_1;
                                if (*var_23 == 9U) {
                                    *var_16 = *var_17;
                                    if (*var_22 == 0UL) {
                                        **(unsigned char **)var_13 = (unsigned char)'\t';
                                    }
                                } else {
                                    var_49 = *var_16 + 1UL;
                                    *var_16 = var_49;
                                    if (*var_21 != '\x01') {
                                        _pre124 = *var_17;
                                        var_51 = _pre124;
                                        if (var_49 == var_51) {
                                            *var_20 = (unsigned char)'\x01';
                                        }
                                        var_52 = *var_22;
                                        *var_22 = (var_52 + 1UL);
                                        *(unsigned char *)(var_52 + *var_14) = (unsigned char)*var_23;
                                        *var_21 = (unsigned char)'\x01';
                                        r10_7 = r10_2;
                                        r9_7 = r9_2;
                                        rcx_6 = rcx_2;
                                        rcx_7 = rcx_2;
                                        r8_7 = r8_2;
                                        local_sp_8 = local_sp_2;
                                        r10_6 = r10_2;
                                        r9_6 = r9_2;
                                        r8_6 = r8_2;
                                        local_sp_7 = local_sp_2;
                                        if (*var_23 == 10U) {
                                            continue;
                                        }
                                        loop_state_var = 0U;
                                        break;
                                    }
                                    var_50 = *var_17;
                                    var_51 = var_50;
                                    if (var_49 != var_50) {
                                        if (var_49 == var_51) {
                                            *var_20 = (unsigned char)'\x01';
                                        }
                                        var_52 = *var_22;
                                        *var_22 = (var_52 + 1UL);
                                        *(unsigned char *)(var_52 + *var_14) = (unsigned char)*var_23;
                                        *var_21 = (unsigned char)'\x01';
                                        r10_7 = r10_2;
                                        r9_7 = r9_2;
                                        rcx_6 = rcx_2;
                                        rcx_7 = rcx_2;
                                        r8_7 = r8_2;
                                        local_sp_8 = local_sp_2;
                                        r10_6 = r10_2;
                                        r9_6 = r9_2;
                                        r8_6 = r8_2;
                                        local_sp_7 = local_sp_2;
                                        if (*var_23 == 10U) {
                                            continue;
                                        }
                                        loop_state_var = 0U;
                                        break;
                                    }
                                    *var_23 = 9U;
                                    **(unsigned char **)var_13 = (unsigned char)'\t';
                                }
                                *var_22 = (uint64_t)*var_20;
                            }
                        }
                        var_60 = *var_22;
                        r10_0 = r10_3;
                        r9_0 = r9_3;
                        r8_0 = r8_3;
                        rcx_4 = rcx_3;
                        r10_4 = r10_3;
                        r9_4 = r9_3;
                        r8_4 = r8_3;
                        local_sp_4 = local_sp_3;
                        if (var_60 != 0UL) {
                            if (var_60 <= 1UL & *var_20 == '\x00') {
                                **(unsigned char **)var_13 = (unsigned char)'\t';
                            }
                            var_61 = *(uint64_t *)6380096UL;
                            var_62 = *var_14;
                            var_63 = local_sp_3 + (-8L);
                            *(uint64_t *)var_63 = 4202833UL;
                            indirect_placeholder();
                            rcx_0 = var_61;
                            local_sp_0 = var_63;
                            if (var_62 != *var_22) {
                                *(uint64_t *)(local_sp_3 + (-16L)) = 4202844UL;
                                indirect_placeholder();
                                var_64 = (uint64_t)*(uint32_t *)var_62;
                                var_65 = local_sp_3 + (-24L);
                                *(uint64_t *)var_65 = 4202868UL;
                                var_66 = indirect_placeholder_19(0UL, 4270846UL, var_61, var_64, 1UL, r9_3, r8_3);
                                rcx_0 = var_66.field_0;
                                r10_0 = var_66.field_1;
                                r9_0 = var_66.field_2;
                                r8_0 = var_66.field_3;
                                local_sp_0 = var_65;
                            }
                            *var_22 = 0UL;
                            *var_20 = (unsigned char)'\x00';
                            rcx_4 = rcx_0;
                            r10_4 = r10_0;
                            r9_4 = r9_0;
                            r8_4 = r8_0;
                            local_sp_4 = local_sp_0;
                        }
                        *var_21 = *var_24;
                        r10_5 = r10_4;
                        r9_5 = r9_4;
                        r8_5 = r8_4;
                        local_sp_6 = local_sp_4;
                        rcx_5 = rcx_4;
                        if (*(unsigned char *)6380240UL == '\x00') {
                        } else {
                            storemerge = (unsigned char)'\x00';
                            if (*var_24 == '\x00') {
                            }
                        }
                        *var_15 = (storemerge & *var_15);
                    }
                    var_67 = ((int)*var_23 > (int)4294967295U);
                    var_68 = local_sp_6 + (-8L);
                    var_69 = (uint64_t *)var_68;
                    rcx_2 = rcx_5;
                    r10_2 = r10_5;
                    r9_2 = r9_5;
                    r8_2 = r8_5;
                    local_sp_2 = var_68;
                    if (!var_67) {
                        *var_69 = 4202948UL;
                        indirect_placeholder();
                        loop_state_var = 1U;
                        break;
                    }
                    *var_69 = 4202960UL;
                    indirect_placeholder();
                    r10_7 = r10_2;
                    r9_7 = r9_2;
                    rcx_6 = rcx_2;
                    rcx_7 = rcx_2;
                    r8_7 = r8_2;
                    local_sp_8 = local_sp_2;
                    r10_6 = r10_2;
                    r9_6 = r9_2;
                    r8_6 = r8_2;
                    local_sp_7 = local_sp_2;
                    if (*var_23 == 10U) {
                        continue;
                    }
                    loop_state_var = 0U;
                    break;
                }
            switch_state_var = 0;
            switch (loop_state_var) {
              case 1U:
                {
                    switch_state_var = 1;
                    break;
                }
                break;
              case 0U:
                {
                    continue;
                }
                break;
            }
            if (switch_state_var)
                break;
        }
    return;
}

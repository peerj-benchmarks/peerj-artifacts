typedef unsigned char __u_char;
typedef unsigned short __u_short;
typedef unsigned int __u_int;
typedef unsigned long __u_long;
typedef signed char __int8_t;
typedef unsigned char __uint8_t;
typedef short __int16_t;
typedef unsigned short __uint16_t;
typedef int __int32_t;
typedef unsigned int __uint32_t;
typedef long __int64_t;
typedef unsigned long __uint64_t;
typedef __int8_t __int_least8_t;
typedef __uint8_t __uint_least8_t;
typedef __int16_t __int_least16_t;
typedef __uint16_t __uint_least16_t;
typedef __int32_t __int_least32_t;
typedef __uint32_t __uint_least32_t;
typedef __int64_t __int_least64_t;
typedef __uint64_t __uint_least64_t;
typedef long __quad_t;
typedef unsigned long __u_quad_t;
typedef long __intmax_t;
typedef unsigned long __uintmax_t;
typedef unsigned long __dev_t;
typedef unsigned int __uid_t;
typedef unsigned int __gid_t;
typedef unsigned long __ino_t;
typedef unsigned long __ino64_t;
typedef unsigned int __mode_t;
typedef unsigned long __nlink_t;
typedef long __off_t;
typedef long __off64_t;
typedef int __pid_t;
typedef struct {
    int __val[2];
} __fsid_t;
typedef long __clock_t;
typedef unsigned long __rlim_t;
typedef unsigned long __rlim64_t;
typedef unsigned int __id_t;
typedef long __time_t;
typedef unsigned int __useconds_t;
typedef long __suseconds_t;
typedef int __daddr_t;
typedef int __key_t;
typedef int __clockid_t;
typedef void *__timer_t;
typedef long __blksize_t;
typedef long __blkcnt_t;
typedef long __blkcnt64_t;
typedef unsigned long __fsblkcnt_t;
typedef unsigned long __fsblkcnt64_t;
typedef unsigned long __fsfilcnt_t;
typedef unsigned long __fsfilcnt64_t;
typedef long __fsword_t;
typedef long __ssize_t;
typedef long __syscall_slong_t;
typedef unsigned long __syscall_ulong_t;
typedef __off64_t __loff_t;
typedef char *__caddr_t;
typedef long __intptr_t;
typedef unsigned int __socklen_t;
typedef int __sig_atomic_t;
typedef __int8_t int8_t;
typedef __int16_t int16_t;
typedef __int32_t int32_t;
typedef __int64_t int64_t;
typedef __uint8_t uint8_t;
typedef __uint16_t uint16_t;
typedef __uint32_t uint32_t;
typedef __uint64_t uint64_t;
typedef __int_least8_t int_least8_t;
typedef __int_least16_t int_least16_t;
typedef __int_least32_t int_least32_t;
typedef __int_least64_t int_least64_t;
typedef __uint_least8_t uint_least8_t;
typedef __uint_least16_t uint_least16_t;
typedef __uint_least32_t uint_least32_t;
typedef __uint_least64_t uint_least64_t;
typedef signed char int_fast8_t;
typedef long int_fast16_t;
typedef long int_fast32_t;
typedef long int_fast64_t;
typedef unsigned char uint_fast8_t;
typedef unsigned long uint_fast16_t;
typedef unsigned long uint_fast32_t;
typedef unsigned long uint_fast64_t;
typedef long intptr_t;
typedef unsigned long uintptr_t;
typedef __intmax_t intmax_t;
typedef __uintmax_t uintmax_t;
typedef _Bool bool;
struct indirect_placeholder_5_ret_type;
struct indirect_placeholder_7_ret_type;
struct indirect_placeholder_8_ret_type;
struct indirect_placeholder_10_ret_type;
struct indirect_placeholder_11_ret_type;
struct indirect_placeholder_13_ret_type;
struct indirect_placeholder_14_ret_type;
struct indirect_placeholder_15_ret_type;
struct indirect_placeholder_17_ret_type;
struct indirect_placeholder_5_ret_type {
    uint64_t field_0;
    uint64_t field_1;
    uint64_t field_2;
    uint64_t field_3;
};
struct indirect_placeholder_7_ret_type {
    uint64_t field_0;
    uint64_t field_1;
    uint64_t field_2;
    uint64_t field_3;
};
struct indirect_placeholder_8_ret_type {
    uint64_t field_0;
    uint64_t field_1;
    uint64_t field_2;
    uint64_t field_3;
};
struct indirect_placeholder_10_ret_type {
    uint64_t field_0;
    uint64_t field_1;
    uint64_t field_2;
    uint64_t field_3;
};
struct indirect_placeholder_11_ret_type {
    uint64_t field_0;
    uint64_t field_1;
    uint64_t field_2;
    uint64_t field_3;
};
struct indirect_placeholder_13_ret_type {
    uint64_t field_0;
    uint64_t field_1;
    uint64_t field_2;
    uint64_t field_3;
};
struct indirect_placeholder_14_ret_type {
    uint64_t field_0;
    uint64_t field_1;
    uint64_t field_2;
    uint64_t field_3;
};
struct indirect_placeholder_15_ret_type {
    uint64_t field_0;
    uint64_t field_1;
    uint64_t field_2;
    uint64_t field_3;
};
struct indirect_placeholder_17_ret_type {
    uint64_t field_0;
    uint64_t field_1;
    uint64_t field_2;
    uint64_t field_3;
};
extern void abort(void);
extern uint64_t revng_init_local_sp(uint64_t param_0);
extern uint64_t init_rbp(void);
extern uint64_t indirect_placeholder_6(uint64_t param_0, uint64_t param_1);
extern uint64_t init_cc_src2(void);
extern void indirect_placeholder(void);
extern uint64_t indirect_placeholder_9(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint64_t param_3, uint64_t param_4);
extern void indirect_placeholder_12(uint64_t param_0, uint64_t param_1);
extern uint64_t indirect_placeholder_3(void);
extern uint64_t init_rbx(void);
extern uint64_t init_r10(void);
extern uint64_t indirect_placeholder_1(uint64_t param_0);
extern void indirect_placeholder_4(uint64_t param_0);
extern struct indirect_placeholder_5_ret_type indirect_placeholder_5(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint64_t param_3, uint64_t param_4, uint64_t param_5, uint64_t param_6);
extern struct indirect_placeholder_7_ret_type indirect_placeholder_7(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint64_t param_3, uint64_t param_4, uint64_t param_5, uint64_t param_6);
extern struct indirect_placeholder_8_ret_type indirect_placeholder_8(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint64_t param_3, uint64_t param_4, uint64_t param_5, uint64_t param_6);
extern struct indirect_placeholder_10_ret_type indirect_placeholder_10(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint64_t param_3, uint64_t param_4, uint64_t param_5, uint64_t param_6);
extern struct indirect_placeholder_11_ret_type indirect_placeholder_11(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint64_t param_3, uint64_t param_4, uint64_t param_5, uint64_t param_6);
extern struct indirect_placeholder_13_ret_type indirect_placeholder_13(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint64_t param_3, uint64_t param_4, uint64_t param_5, uint64_t param_6);
extern uint64_t helper_cc_compute_all_wrapper(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint32_t param_3);
extern struct indirect_placeholder_14_ret_type indirect_placeholder_14(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint64_t param_3, uint64_t param_4, uint64_t param_5);
extern struct indirect_placeholder_15_ret_type indirect_placeholder_15(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint64_t param_3, uint64_t param_4, uint64_t param_5, uint64_t param_6);
extern void indirect_placeholder_16(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint64_t param_3, uint64_t param_4, uint64_t param_5);
extern struct indirect_placeholder_17_ret_type indirect_placeholder_17(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint64_t param_3, uint64_t param_4);
void bb_main(uint64_t rdi, uint64_t rsi) {
    uint32_t var_76;
    uint32_t var_47;
    uint64_t local_sp_2;
    uint64_t local_sp_3;
    uint64_t var_48;
    uint64_t var_49;
    uint64_t var_69;
    struct indirect_placeholder_17_ret_type var_19;
    struct indirect_placeholder_5_ret_type var_94;
    uint64_t r9_0;
    uint64_t var_0;
    uint64_t var_1;
    uint64_t var_2;
    uint64_t var_3;
    uint64_t var_4;
    uint64_t var_5;
    uint32_t *var_6;
    uint64_t var_7;
    uint64_t *var_8;
    uint64_t *var_9;
    uint32_t *var_10;
    uint64_t *var_11;
    unsigned char *var_12;
    uint64_t var_13;
    uint64_t var_14;
    uint32_t *var_15;
    uint64_t local_sp_9_ph;
    uint64_t var_90;
    uint64_t var_91;
    uint64_t var_92;
    uint64_t var_93;
    uint64_t r8_3;
    uint64_t var_95;
    uint64_t var_96;
    uint64_t local_sp_0;
    uint64_t var_81;
    uint64_t var_82;
    uint64_t var_83;
    uint64_t var_84;
    struct indirect_placeholder_7_ret_type var_85;
    uint64_t var_86;
    uint64_t var_87;
    uint64_t var_88;
    uint64_t var_89;
    uint64_t r8_0;
    uint32_t var_97;
    uint32_t var_73;
    uint64_t r9_1;
    uint64_t var_71;
    struct indirect_placeholder_8_ret_type var_72;
    uint64_t local_sp_4;
    uint64_t rcx_0;
    uint64_t var_61;
    uint64_t var_62;
    uint64_t var_63;
    struct indirect_placeholder_10_ret_type var_64;
    uint64_t rax_0;
    uint64_t local_sp_1;
    uint32_t *var_65;
    uint64_t var_66;
    uint64_t var_67;
    uint64_t var_68;
    uint64_t var_70;
    uint64_t var_50;
    uint64_t var_51;
    uint32_t var_20;
    struct indirect_placeholder_11_ret_type var_52;
    uint64_t var_41;
    uint32_t var_42;
    uint64_t var_43;
    uint64_t var_44;
    uint64_t var_45;
    uint32_t var_46;
    uint64_t var_32;
    uint64_t var_33;
    uint64_t var_34;
    uint32_t *var_35;
    uint64_t var_36;
    bool var_37;
    uint64_t var_38;
    uint64_t var_39;
    uint64_t *var_40;
    uint64_t r10_0;
    uint64_t r9_2;
    uint64_t r8_1;
    uint64_t rbx_0;
    uint32_t *var_53;
    uint64_t var_54;
    uint64_t var_55;
    uint64_t var_56;
    struct indirect_placeholder_14_ret_type var_57;
    uint64_t var_58;
    uint64_t var_59;
    uint64_t *var_60;
    uint64_t r9_3;
    uint64_t r8_2;
    uint64_t local_sp_5;
    uint64_t r9_4;
    uint64_t var_74;
    uint64_t var_75;
    uint64_t local_sp_6;
    uint64_t var_77;
    uint64_t var_78;
    uint64_t var_79;
    uint64_t var_80;
    uint64_t local_sp_7;
    uint64_t var_30;
    struct indirect_placeholder_15_ret_type var_31;
    uint64_t r10_1;
    uint64_t r10_2_ph;
    uint64_t local_sp_9;
    uint64_t local_sp_8;
    uint64_t var_21;
    uint64_t var_22;
    uint64_t var_23;
    uint64_t var_24;
    uint64_t var_25;
    uint64_t var_26;
    uint64_t var_27;
    bool var_28;
    uint64_t var_29;
    uint64_t var_16;
    uint64_t var_17;
    uint64_t var_18;
    unsigned int loop_state_var;
    bool switch_state_var;
    var_0 = revng_init_local_sp(0UL);
    var_1 = init_rbp();
    var_2 = init_cc_src2();
    var_3 = init_r10();
    var_4 = init_rbx();
    var_5 = var_0 + (-8L);
    *(uint64_t *)var_5 = var_1;
    *(uint64_t *)(var_0 + (-16L)) = var_4;
    var_6 = (uint32_t *)(var_0 + (-92L));
    *var_6 = (uint32_t)rdi;
    var_7 = var_0 + (-104L);
    var_8 = (uint64_t *)var_7;
    *var_8 = rsi;
    var_9 = (uint64_t *)(var_0 + (-40L));
    *var_9 = 0UL;
    var_10 = (uint32_t *)(var_0 + (-44L));
    *var_10 = 0U;
    var_11 = (uint64_t *)(var_0 + (-56L));
    *var_11 = 0UL;
    var_12 = (unsigned char *)(var_0 + (-61L));
    *var_12 = (unsigned char)'\x00';
    var_13 = **(uint64_t **)var_7;
    *(uint64_t *)(var_0 + (-112L)) = 4201923UL;
    indirect_placeholder_4(var_13);
    *(uint64_t *)(var_0 + (-120L)) = 4201938UL;
    indirect_placeholder();
    var_14 = var_0 + (-128L);
    *(uint64_t *)var_14 = 4201948UL;
    indirect_placeholder();
    var_15 = (uint32_t *)(var_0 + (-68L));
    rbx_0 = var_4;
    local_sp_9_ph = var_14;
    r10_2_ph = var_3;
    while (1U)
        {
            r10_0 = r10_2_ph;
            r10_1 = r10_2_ph;
            local_sp_9 = local_sp_9_ph;
            while (1U)
                {
                    var_16 = *var_8;
                    var_17 = (uint64_t)*var_6;
                    var_18 = local_sp_9 + (-8L);
                    *(uint64_t *)var_18 = 4202185UL;
                    var_19 = indirect_placeholder_17(4269129UL, 4268352UL, var_17, var_16, 0UL);
                    var_20 = (uint32_t)var_19.field_0;
                    *var_15 = var_20;
                    local_sp_3 = var_18;
                    local_sp_8 = var_18;
                    local_sp_9 = var_18;
                    if (var_20 == 4294967295U) {
                        if ((uint64_t)(var_20 + 130U) == 0UL) {
                            *(uint64_t *)(local_sp_9 + (-16L)) = 4202083UL;
                            indirect_placeholder_12(var_5, 0UL);
                            abort();
                        }
                        if ((int)var_20 > (int)4294967166U) {
                            if ((uint64_t)(var_20 + (-90)) != 0UL) {
                                loop_state_var = 1U;
                                break;
                            }
                            if ((uint64_t)(var_20 + (-109)) != 0UL) {
                                loop_state_var = 0U;
                                break;
                            }
                            *var_9 = *(uint64_t *)6379288UL;
                            continue;
                        }
                        if (var_20 != 4294967165U) {
                            loop_state_var = 0U;
                            break;
                        }
                        var_21 = *(uint64_t *)6378464UL;
                        *(uint64_t *)(local_sp_9 + (-16L)) = 4202135UL;
                        indirect_placeholder_16(0UL, 4268064UL, var_21, 4269030UL, 0UL, 4269113UL);
                        var_22 = local_sp_9 + (-24L);
                        *(uint64_t *)var_22 = 4202145UL;
                        indirect_placeholder();
                        local_sp_8 = var_22;
                        loop_state_var = 0U;
                        break;
                    }
                    var_32 = var_19.field_1;
                    var_33 = var_19.field_2;
                    var_34 = var_19.field_3;
                    rcx_0 = var_32;
                    r9_2 = var_33;
                    r8_1 = var_34;
                    if ((uint64_t)(*(uint32_t *)6378616UL - *var_6) == 0UL) {
                        *(uint64_t *)(local_sp_9 + (-16L)) = 4202234UL;
                        indirect_placeholder_13(0UL, 4269133UL, var_32, 0UL, 0UL, var_33, var_34);
                        *(uint64_t *)(local_sp_9 + (-24L)) = 4202244UL;
                        indirect_placeholder_12(var_5, 1UL);
                        abort();
                    }
                    if (*var_11 != 0UL) {
                        loop_state_var = 2U;
                        break;
                    }
                    var_35 = (uint32_t *)(var_0 + (-60L));
                    *var_35 = 0U;
                    *(uint64_t *)(local_sp_9 + (-16L)) = 4202263UL;
                    var_36 = indirect_placeholder_3();
                    var_37 = ((uint64_t)(unsigned char)var_36 == 0UL);
                    var_38 = *var_11;
                    var_39 = local_sp_9 + (-24L);
                    var_40 = (uint64_t *)var_39;
                    local_sp_2 = var_39;
                    if (!var_37) {
                        *var_40 = 4202279UL;
                        var_41 = indirect_placeholder_1(var_38);
                        var_42 = (uint32_t)var_41;
                        *var_35 = var_42;
                        var_47 = var_42;
                        loop_state_var = 3U;
                        break;
                    }
                    *var_40 = 4202296UL;
                    var_43 = indirect_placeholder_1(var_38);
                    var_44 = local_sp_9 + (-32L);
                    *(uint64_t *)var_44 = 4202304UL;
                    var_45 = indirect_placeholder_1(var_43);
                    var_46 = (uint32_t)var_45;
                    *var_35 = var_46;
                    var_47 = var_46;
                    local_sp_2 = var_44;
                    loop_state_var = 3U;
                    break;
                }
            switch_state_var = 0;
            switch (loop_state_var) {
              case 0U:
                {
                    *(uint64_t *)(local_sp_8 + (-8L)) = 4202155UL;
                    indirect_placeholder_12(var_5, 1UL);
                    abort();
                }
                break;
              case 2U:
                {
                    switch_state_var = 1;
                    break;
                }
                break;
              case 1U:
                {
                    var_23 = var_19.field_1;
                    var_24 = var_19.field_2;
                    var_25 = var_19.field_3;
                    var_26 = local_sp_9 + (-16L);
                    *(uint64_t *)var_26 = 4202017UL;
                    var_27 = indirect_placeholder_3();
                    var_28 = ((uint64_t)(unsigned char)var_27 == 0UL);
                    var_29 = *(uint64_t *)6379288UL;
                    local_sp_7 = var_26;
                    if (var_28) {
                        *var_11 = var_29;
                    } else {
                        if (var_29 == 0UL) {
                            var_30 = local_sp_9 + (-24L);
                            *(uint64_t *)var_30 = 4202071UL;
                            var_31 = indirect_placeholder_15(0UL, 4269040UL, var_23, 0UL, 0UL, var_24, var_25);
                            local_sp_7 = var_30;
                            r10_1 = var_31.field_1;
                        }
                    }
                    local_sp_9_ph = local_sp_7;
                    r10_2_ph = r10_1;
                    continue;
                }
                break;
              case 3U:
                {
                    local_sp_3 = local_sp_2;
                    if ((int)var_47 > (int)4294967295U) {
                        switch_state_var = 1;
                        break;
                    }
                    var_48 = *var_11;
                    *(uint64_t *)(local_sp_2 + (-8L)) = 4202325UL;
                    var_49 = indirect_placeholder_1(var_48);
                    *(uint64_t *)(local_sp_2 + (-16L)) = 4202333UL;
                    indirect_placeholder();
                    var_50 = (uint64_t)*(uint32_t *)var_49;
                    var_51 = local_sp_2 + (-24L);
                    *(uint64_t *)var_51 = 4202360UL;
                    var_52 = indirect_placeholder_11(0UL, 4269152UL, var_49, 1UL, var_50, var_33, var_34);
                    local_sp_3 = var_51;
                    rcx_0 = var_52.field_0;
                    r10_0 = var_52.field_1;
                    r9_2 = var_52.field_2;
                    r8_1 = var_52.field_3;
                    rbx_0 = var_49;
                    switch_state_var = 1;
                    break;
                }
                break;
            }
            if (switch_state_var)
                break;
        }
    var_53 = (uint32_t *)(var_0 + (-28L));
    *var_53 = 438U;
    var_54 = helper_cc_compute_all_wrapper(*var_9, 0UL, var_2, 17U);
    local_sp_4 = local_sp_3;
    r9_3 = r9_2;
    r8_2 = r8_1;
    if ((var_54 & 64UL) != 0UL) {
        var_55 = *var_9;
        var_56 = local_sp_3 + (-8L);
        *(uint64_t *)var_56 = 4202390UL;
        var_57 = indirect_placeholder_14(rcx_0, var_55, r10_0, r9_2, r8_1, rbx_0);
        var_58 = var_57.field_0;
        var_59 = var_57.field_2;
        var_60 = (uint64_t *)(var_0 + (-80L));
        *var_60 = var_58;
        rax_0 = var_58;
        local_sp_1 = var_56;
        r9_1 = var_59;
        r8_2 = 0UL;
        if (var_58 == 0UL) {
            var_61 = var_57.field_3;
            var_62 = var_57.field_1;
            var_63 = local_sp_3 + (-16L);
            *(uint64_t *)var_63 = 4202426UL;
            var_64 = indirect_placeholder_10(0UL, 4269202UL, var_62, 1UL, 0UL, var_59, var_61);
            rax_0 = 0UL;
            local_sp_1 = var_63;
            r9_1 = var_64.field_2;
        }
        *(uint64_t *)(local_sp_1 + (-8L)) = 4202436UL;
        indirect_placeholder();
        var_65 = (uint32_t *)(var_0 + (-84L));
        *var_65 = (uint32_t)rax_0;
        *(uint64_t *)(local_sp_1 + (-16L)) = 4202449UL;
        indirect_placeholder();
        var_66 = *var_60;
        var_67 = (uint64_t)*var_65;
        var_68 = (uint64_t)*var_53;
        *(uint64_t *)(local_sp_1 + (-24L)) = 4202477UL;
        var_69 = indirect_placeholder_9(var_67, var_66, var_68, 0UL, 0UL);
        *var_53 = (uint32_t)var_69;
        var_70 = local_sp_1 + (-32L);
        *(uint64_t *)var_70 = 4202492UL;
        indirect_placeholder();
        local_sp_4 = var_70;
        r9_3 = r9_1;
        if (*var_53 < 512U) {
            var_71 = local_sp_1 + (-40L);
            *(uint64_t *)var_71 = 4202529UL;
            var_72 = indirect_placeholder_8(0UL, 4269216UL, var_66, 1UL, 0UL, r9_1, 0UL);
            local_sp_4 = var_71;
            r9_3 = var_72.field_2;
            r8_2 = var_72.field_3;
        }
    }
    var_73 = *(uint32_t *)6378616UL;
    local_sp_5 = local_sp_4;
    r9_4 = r9_3;
    r8_3 = r8_2;
    r9_0 = r9_4;
    r8_0 = r8_3;
    var_76 = var_73;
    local_sp_6 = local_sp_5;
    while ((long)((uint64_t)var_73 << 32UL) >= (long)((uint64_t)*var_6 << 32UL))
        {
            if (*var_12 == '\x00') {
                var_74 = *(uint64_t *)(*var_8 + ((uint64_t)var_73 << 3UL));
                var_75 = local_sp_5 + (-8L);
                *(uint64_t *)var_75 = 4202579UL;
                indirect_placeholder_6(var_74, 4096UL);
                var_76 = *(uint32_t *)6378616UL;
                local_sp_6 = var_75;
            }
            var_77 = *(uint64_t *)(*var_8 + ((uint64_t)var_76 << 3UL));
            var_78 = (uint64_t)*var_53;
            var_79 = local_sp_6 + (-8L);
            *(uint64_t *)var_79 = 4202618UL;
            var_80 = indirect_placeholder_6(var_77, var_78);
            local_sp_0 = var_79;
            if ((uint64_t)(uint32_t)var_80 == 0UL) {
                var_81 = *(uint64_t *)(*var_8 + ((uint64_t)*(uint32_t *)6378616UL << 3UL));
                *(uint64_t *)(local_sp_6 + (-16L)) = 4202661UL;
                var_82 = indirect_placeholder_6(4UL, var_81);
                *(uint64_t *)(local_sp_6 + (-24L)) = 4202669UL;
                indirect_placeholder();
                var_83 = (uint64_t)*(uint32_t *)var_82;
                var_84 = local_sp_6 + (-32L);
                *(uint64_t *)var_84 = 4202696UL;
                var_85 = indirect_placeholder_7(0UL, 4269260UL, var_82, 0UL, var_83, r9_4, r8_3);
                var_86 = var_85.field_2;
                var_87 = var_85.field_3;
                *var_10 = 1U;
                local_sp_0 = var_84;
                r9_0 = var_86;
                r8_0 = var_87;
            } else {
                var_88 = *(uint64_t *)(*var_8 + ((uint64_t)*(uint32_t *)6378616UL << 3UL));
                var_89 = local_sp_6 + (-16L);
                *(uint64_t *)var_89 = 4202754UL;
                indirect_placeholder();
                local_sp_0 = var_89;
                if (*var_9 != 0UL & (uint64_t)(uint32_t)var_88 == 0UL) {
                    var_90 = *(uint64_t *)(*var_8 + ((uint64_t)*(uint32_t *)6378616UL << 3UL));
                    *(uint64_t *)(local_sp_6 + (-24L)) = 4202797UL;
                    var_91 = indirect_placeholder_6(4UL, var_90);
                    *(uint64_t *)(local_sp_6 + (-32L)) = 4202805UL;
                    indirect_placeholder();
                    var_92 = (uint64_t)*(uint32_t *)var_91;
                    var_93 = local_sp_6 + (-40L);
                    *(uint64_t *)var_93 = 4202832UL;
                    var_94 = indirect_placeholder_5(0UL, 4269282UL, var_91, 0UL, var_92, r9_4, r8_3);
                    var_95 = var_94.field_2;
                    var_96 = var_94.field_3;
                    *var_10 = 1U;
                    local_sp_0 = var_93;
                    r9_0 = var_95;
                    r8_0 = var_96;
                }
            }
            var_97 = *(uint32_t *)6378616UL + 1U;
            *(uint32_t *)6378616UL = var_97;
            var_73 = var_97;
            local_sp_5 = local_sp_0;
            r9_4 = r9_0;
            r8_3 = r8_0;
            r9_0 = r9_4;
            r8_0 = r8_3;
            var_76 = var_73;
            local_sp_6 = local_sp_5;
        }
    return;
}

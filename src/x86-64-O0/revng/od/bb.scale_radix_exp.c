typedef unsigned char __u_char;
typedef unsigned short __u_short;
typedef unsigned int __u_int;
typedef unsigned long __u_long;
typedef signed char __int8_t;
typedef unsigned char __uint8_t;
typedef short __int16_t;
typedef unsigned short __uint16_t;
typedef int __int32_t;
typedef unsigned int __uint32_t;
typedef long __int64_t;
typedef unsigned long __uint64_t;
typedef __int8_t __int_least8_t;
typedef __uint8_t __uint_least8_t;
typedef __int16_t __int_least16_t;
typedef __uint16_t __uint_least16_t;
typedef __int32_t __int_least32_t;
typedef __uint32_t __uint_least32_t;
typedef __int64_t __int_least64_t;
typedef __uint64_t __uint_least64_t;
typedef long __quad_t;
typedef unsigned long __u_quad_t;
typedef long __intmax_t;
typedef unsigned long __uintmax_t;
typedef unsigned long __dev_t;
typedef unsigned int __uid_t;
typedef unsigned int __gid_t;
typedef unsigned long __ino_t;
typedef unsigned long __ino64_t;
typedef unsigned int __mode_t;
typedef unsigned long __nlink_t;
typedef long __off_t;
typedef long __off64_t;
typedef int __pid_t;
typedef struct {
    int __val[2];
} __fsid_t;
typedef long __clock_t;
typedef unsigned long __rlim_t;
typedef unsigned long __rlim64_t;
typedef unsigned int __id_t;
typedef long __time_t;
typedef unsigned int __useconds_t;
typedef long __suseconds_t;
typedef int __daddr_t;
typedef int __key_t;
typedef int __clockid_t;
typedef void *__timer_t;
typedef long __blksize_t;
typedef long __blkcnt_t;
typedef long __blkcnt64_t;
typedef unsigned long __fsblkcnt_t;
typedef unsigned long __fsblkcnt64_t;
typedef unsigned long __fsfilcnt_t;
typedef unsigned long __fsfilcnt64_t;
typedef long __fsword_t;
typedef long __ssize_t;
typedef long __syscall_slong_t;
typedef unsigned long __syscall_ulong_t;
typedef __off64_t __loff_t;
typedef char *__caddr_t;
typedef long __intptr_t;
typedef unsigned int __socklen_t;
typedef int __sig_atomic_t;
typedef __int8_t int8_t;
typedef __int16_t int16_t;
typedef __int32_t int32_t;
typedef __int64_t int64_t;
typedef __uint8_t uint8_t;
typedef __uint16_t uint16_t;
typedef __uint32_t uint32_t;
typedef __uint64_t uint64_t;
typedef __int_least8_t int_least8_t;
typedef __int_least16_t int_least16_t;
typedef __int_least32_t int_least32_t;
typedef __int_least64_t int_least64_t;
typedef __uint_least8_t uint_least8_t;
typedef __uint_least16_t uint_least16_t;
typedef __uint_least32_t uint_least32_t;
typedef __uint_least64_t uint_least64_t;
typedef signed char int_fast8_t;
typedef long int_fast16_t;
typedef long int_fast32_t;
typedef long int_fast64_t;
typedef unsigned char uint_fast8_t;
typedef unsigned long uint_fast16_t;
typedef unsigned long uint_fast32_t;
typedef unsigned long uint_fast64_t;
typedef long intptr_t;
typedef unsigned long uintptr_t;
typedef __intmax_t intmax_t;
typedef __uintmax_t uintmax_t;
struct helper_pxor_xmm_wrapper_ret_type;
struct type_4;
struct type_6;
struct helper_ucomisd_wrapper_132_ret_type;
struct helper_ucomisd_wrapper_ret_type;
struct helper_mulsd_wrapper_ret_type;
struct helper_cvtsi2sd_wrapper_ret_type;
struct helper_divsd_wrapper_ret_type;
struct helper_pxor_xmm_wrapper_ret_type {
    uint64_t field_0;
    uint64_t field_1;
};
struct type_4 {
};
struct type_6 {
};
struct helper_ucomisd_wrapper_132_ret_type {
    uint64_t field_0;
    unsigned char field_1;
};
struct helper_ucomisd_wrapper_ret_type {
    uint64_t field_0;
    unsigned char field_1;
};
struct helper_mulsd_wrapper_ret_type {
    uint64_t field_0;
    unsigned char field_1;
};
struct helper_cvtsi2sd_wrapper_ret_type {
    uint64_t field_0;
};
struct helper_divsd_wrapper_ret_type {
    uint64_t field_0;
    unsigned char field_1;
};
extern uint64_t revng_init_local_sp(uint64_t param_0);
extern uint64_t init_rbp(void);
extern void indirect_placeholder(void);
extern uint64_t init_state_0x8558(void);
extern unsigned char init_state_0x8549(void);
extern unsigned char init_state_0x854c(void);
extern unsigned char init_state_0x8548(void);
extern unsigned char init_state_0x854b(void);
extern unsigned char init_state_0x8547(void);
extern unsigned char init_state_0x854d(void);
extern struct helper_pxor_xmm_wrapper_ret_type helper_pxor_xmm_wrapper(struct type_4 *param_0, struct type_6 *param_1, struct type_6 *param_2, uint64_t param_3, uint64_t param_4);
extern struct helper_ucomisd_wrapper_132_ret_type helper_ucomisd_wrapper_132(struct type_4 *param_0, struct type_6 *param_1, struct type_6 *param_2, uint64_t param_3, uint64_t param_4, unsigned char param_5, unsigned char param_6);
extern struct helper_ucomisd_wrapper_ret_type helper_ucomisd_wrapper(struct type_4 *param_0, struct type_6 *param_1, struct type_6 *param_2, uint64_t param_3, uint64_t param_4, unsigned char param_5, unsigned char param_6);
extern struct helper_mulsd_wrapper_ret_type helper_mulsd_wrapper(struct type_4 *param_0, struct type_6 *param_1, struct type_6 *param_2, uint64_t param_3, uint64_t param_4, unsigned char param_5, unsigned char param_6, unsigned char param_7, unsigned char param_8, unsigned char param_9, unsigned char param_10);
extern uint64_t init_state_0x8560(void);
extern struct helper_cvtsi2sd_wrapper_ret_type helper_cvtsi2sd_wrapper(struct type_4 *param_0, struct type_6 *param_1, uint32_t param_2);
extern struct helper_divsd_wrapper_ret_type helper_divsd_wrapper(struct type_4 *param_0, struct type_6 *param_1, struct type_6 *param_2, uint64_t param_3, uint64_t param_4, unsigned char param_5, unsigned char param_6, unsigned char param_7, unsigned char param_8, unsigned char param_9, unsigned char param_10);
void bb_scale_radix_exp(uint64_t rdi, uint64_t rsi) {
    struct helper_pxor_xmm_wrapper_ret_type var_27;
    uint64_t var_0;
    uint64_t var_1;
    uint64_t var_2;
    uint64_t var_3;
    unsigned char var_4;
    unsigned char var_5;
    unsigned char var_6;
    unsigned char var_7;
    unsigned char var_8;
    unsigned char var_9;
    uint64_t *var_10;
    uint32_t *var_11;
    struct helper_ucomisd_wrapper_ret_type var_18;
    uint64_t *var_12;
    uint64_t var_70;
    uint64_t var_13;
    uint64_t *var_14;
    struct helper_pxor_xmm_wrapper_ret_type var_15;
    uint64_t var_16;
    uint64_t var_17;
    uint64_t var_19;
    unsigned char var_20;
    uint64_t state_0x8560_0;
    struct helper_pxor_xmm_wrapper_ret_type var_21;
    uint64_t var_22;
    uint64_t var_23;
    struct helper_ucomisd_wrapper_ret_type var_24;
    unsigned char state_0x8549_0;
    uint64_t state_0x8558_0;
    uint64_t var_25;
    uint64_t var_26;
    unsigned char state_0x8549_1_be;
    uint64_t state_0x8560_1;
    uint64_t state_0x8558_1_be;
    unsigned char state_0x8549_1;
    uint64_t state_0x8558_1;
    uint64_t var_28;
    struct helper_cvtsi2sd_wrapper_ret_type var_29;
    uint64_t var_30;
    struct helper_divsd_wrapper_ret_type var_31;
    uint64_t var_32;
    unsigned char var_33;
    struct helper_pxor_xmm_wrapper_ret_type var_34;
    uint64_t var_35;
    uint64_t var_36;
    struct helper_ucomisd_wrapper_ret_type var_37;
    uint64_t var_38;
    unsigned char var_39;
    struct helper_pxor_xmm_wrapper_ret_type var_40;
    uint64_t var_41;
    uint64_t var_42;
    struct helper_ucomisd_wrapper_ret_type var_43;
    uint64_t var_44;
    unsigned char var_45;
    uint64_t state_0x8560_1_be;
    struct helper_pxor_xmm_wrapper_ret_type var_46;
    uint64_t var_47;
    struct helper_ucomisd_wrapper_ret_type var_48;
    unsigned char var_49;
    struct helper_pxor_xmm_wrapper_ret_type var_50;
    uint64_t var_51;
    struct helper_ucomisd_wrapper_ret_type var_52;
    uint64_t var_53;
    uint64_t state_0x8560_2;
    unsigned char state_0x8549_2;
    uint64_t state_0x8558_2;
    uint64_t rax_0;
    struct helper_cvtsi2sd_wrapper_ret_type var_54;
    struct helper_divsd_wrapper_ret_type var_55;
    uint64_t var_56;
    struct helper_ucomisd_wrapper_ret_type var_57;
    unsigned char var_58;
    struct helper_cvtsi2sd_wrapper_ret_type var_59;
    struct helper_divsd_wrapper_ret_type var_60;
    uint64_t var_61;
    struct helper_ucomisd_wrapper_132_ret_type var_62;
    unsigned char var_63;
    struct helper_pxor_xmm_wrapper_ret_type var_64;
    uint64_t var_65;
    struct helper_cvtsi2sd_wrapper_ret_type var_66;
    uint64_t var_67;
    struct helper_mulsd_wrapper_ret_type var_68;
    unsigned char var_69;
    unsigned int loop_state_var;
    var_0 = revng_init_local_sp(0UL);
    var_1 = init_rbp();
    var_2 = init_state_0x8558();
    var_3 = init_state_0x8560();
    var_4 = init_state_0x8549();
    var_5 = init_state_0x854c();
    var_6 = init_state_0x8548();
    var_7 = init_state_0x854b();
    var_8 = init_state_0x8547();
    var_9 = init_state_0x854d();
    *(uint64_t *)(var_0 + (-8L)) = var_1;
    var_10 = (uint64_t *)(var_0 + (-32L));
    *var_10 = var_2;
    var_11 = (uint32_t *)(var_0 + (-36L));
    *var_11 = (uint32_t)rdi;
    *(uint64_t *)(var_0 + (-48L)) = rsi;
    var_12 = (uint64_t *)(var_0 + (-16L));
    *var_12 = rsi;
    rax_0 = 18442240474082181120UL;
    if (*var_11 == 2U) {
        var_70 = *var_10;
        *(uint64_t *)(var_0 + (-56L)) = var_70;
        *(uint64_t *)(var_0 + (-64L)) = 4235051UL;
        indirect_placeholder();
        rax_0 = var_70;
    } else {
        var_13 = *var_10;
        var_14 = (uint64_t *)(var_0 + (-24L));
        *var_14 = var_13;
        var_15 = helper_pxor_xmm_wrapper((struct type_4 *)(0UL), (struct type_6 *)(776UL), (struct type_6 *)(776UL), var_2, var_3);
        var_16 = var_15.field_0;
        var_17 = var_15.field_1;
        var_18 = helper_ucomisd_wrapper((struct type_4 *)(0UL), (struct type_6 *)(776UL), (struct type_6 *)(2824UL), var_16, *var_14, var_4, var_5);
        var_19 = var_18.field_0;
        var_20 = var_18.field_1;
        state_0x8560_0 = var_17;
        state_0x8549_0 = var_20;
        state_0x8558_0 = var_16;
        if ((var_19 & 4UL) != 0UL) {
            var_21 = helper_pxor_xmm_wrapper((struct type_4 *)(0UL), (struct type_6 *)(776UL), (struct type_6 *)(776UL), var_16, var_17);
            var_22 = var_21.field_0;
            var_23 = var_21.field_1;
            var_24 = helper_ucomisd_wrapper((struct type_4 *)(0UL), (struct type_6 *)(776UL), (struct type_6 *)(2824UL), var_22, *var_14, var_20, var_5);
            state_0x8560_0 = var_23;
            state_0x8549_0 = var_24.field_1;
            state_0x8558_0 = var_22;
            if ((var_24.field_0 & 64UL) != 0UL) {
                rax_0 = *var_14;
                *(uint64_t *)(var_0 + (-56L)) = rax_0;
                return;
            }
        }
        var_25 = *var_12;
        var_26 = var_25;
        state_0x8560_1 = state_0x8560_0;
        state_0x8549_1 = state_0x8549_0;
        state_0x8558_1 = state_0x8558_0;
        var_53 = var_25;
        state_0x8560_2 = state_0x8560_0;
        state_0x8549_2 = state_0x8549_0;
        state_0x8558_2 = state_0x8558_0;
        if ((long)var_25 > (long)18446744073709551615UL) {
            while (1U)
                {
                    *var_12 = (var_26 + 1UL);
                    if (var_26 != 0UL) {
                        loop_state_var = 1U;
                        break;
                    }
                    var_27 = helper_pxor_xmm_wrapper((struct type_4 *)(0UL), (struct type_6 *)(776UL), (struct type_6 *)(776UL), state_0x8558_1, state_0x8560_1);
                    var_28 = var_27.field_1;
                    var_29 = helper_cvtsi2sd_wrapper((struct type_4 *)(0UL), (struct type_6 *)(776UL), *var_11);
                    var_30 = var_29.field_0;
                    var_31 = helper_divsd_wrapper((struct type_4 *)(0UL), (struct type_6 *)(840UL), (struct type_6 *)(776UL), var_30, *var_14, state_0x8549_1, var_5, var_6, var_7, var_8, var_9);
                    var_32 = var_31.field_0;
                    var_33 = var_31.field_1;
                    *var_14 = var_32;
                    var_34 = helper_pxor_xmm_wrapper((struct type_4 *)(0UL), (struct type_6 *)(776UL), (struct type_6 *)(776UL), var_30, var_28);
                    var_35 = var_34.field_0;
                    var_36 = var_34.field_1;
                    var_37 = helper_ucomisd_wrapper((struct type_4 *)(0UL), (struct type_6 *)(776UL), (struct type_6 *)(2824UL), var_35, *var_14, var_33, var_5);
                    var_38 = var_37.field_0;
                    var_39 = var_37.field_1;
                    state_0x8560_1_be = var_36;
                    state_0x8549_1_be = var_39;
                    state_0x8558_1_be = var_35;
                    var_40 = helper_pxor_xmm_wrapper((struct type_4 *)(0UL), (struct type_6 *)(776UL), (struct type_6 *)(776UL), var_35, var_36);
                    var_41 = var_40.field_0;
                    var_42 = var_40.field_1;
                    var_43 = helper_ucomisd_wrapper((struct type_4 *)(0UL), (struct type_6 *)(776UL), (struct type_6 *)(2824UL), var_41, *var_14, var_39, var_5);
                    var_44 = var_43.field_0;
                    var_45 = var_43.field_1;
                    state_0x8560_1_be = var_42;
                    state_0x8549_1_be = var_45;
                    state_0x8558_1_be = var_41;
                    if ((var_38 & 4UL) != 0UL & (var_44 & 64UL) != 0UL) {
                        var_46 = helper_pxor_xmm_wrapper((struct type_4 *)(0UL), (struct type_6 *)(776UL), (struct type_6 *)(776UL), var_41, var_42);
                        var_47 = var_46.field_0;
                        var_48 = helper_ucomisd_wrapper((struct type_4 *)(0UL), (struct type_6 *)(776UL), (struct type_6 *)(2824UL), var_47, *var_10, var_45, var_5);
                        if ((var_48.field_0 & 4UL) != 0UL) {
                            loop_state_var = 0U;
                            break;
                        }
                        var_49 = var_48.field_1;
                        var_50 = helper_pxor_xmm_wrapper((struct type_4 *)(0UL), (struct type_6 *)(776UL), (struct type_6 *)(776UL), var_47, var_46.field_1);
                        var_51 = var_50.field_0;
                        var_52 = helper_ucomisd_wrapper((struct type_4 *)(0UL), (struct type_6 *)(776UL), (struct type_6 *)(2824UL), var_51, *var_10, var_49, var_5);
                        state_0x8558_1_be = var_51;
                        if ((var_52.field_0 & 64UL) != 0UL) {
                            loop_state_var = 0U;
                            break;
                        }
                        state_0x8560_1_be = var_50.field_1;
                        state_0x8549_1_be = var_52.field_1;
                    }
                    var_26 = *var_12;
                    state_0x8560_1 = state_0x8560_1_be;
                    state_0x8549_1 = state_0x8549_1_be;
                    state_0x8558_1 = state_0x8558_1_be;
                    continue;
                }
            switch (loop_state_var) {
              case 0U:
                {
                    *(uint64_t *)(var_0 + (-64L)) = 4235184UL;
                    indirect_placeholder();
                    *(uint32_t *)var_32 = 34U;
                }
                break;
              case 1U:
                {
                    rax_0 = *var_14;
                }
                break;
            }
        } else {
            while (1U)
                {
                    *var_12 = (var_53 + (-1L));
                    if (var_53 != 0UL) {
                        loop_state_var = 0U;
                        break;
                    }
                    helper_pxor_xmm_wrapper((struct type_4 *)(0UL), (struct type_6 *)(776UL), (struct type_6 *)(776UL), state_0x8558_2, state_0x8560_2);
                    var_54 = helper_cvtsi2sd_wrapper((struct type_4 *)(0UL), (struct type_6 *)(776UL), *var_11);
                    var_55 = helper_divsd_wrapper((struct type_4 *)(0UL), (struct type_6 *)(840UL), (struct type_6 *)(776UL), var_54.field_0, *(uint64_t *)4300752UL, state_0x8549_2, var_5, var_6, var_7, var_8, var_9);
                    var_56 = var_55.field_0;
                    var_57 = helper_ucomisd_wrapper((struct type_4 *)(0UL), (struct type_6 *)(776UL), (struct type_6 *)(2824UL), var_56, *var_14, var_55.field_1, var_5);
                    if ((var_57.field_0 & 65UL) != 0UL) {
                        *(uint64_t *)(var_0 + (-64L)) = 4235260UL;
                        indirect_placeholder();
                        *(uint32_t *)var_53 = 34U;
                        loop_state_var = 1U;
                        break;
                    }
                    var_58 = var_57.field_1;
                    helper_pxor_xmm_wrapper((struct type_4 *)(0UL), (struct type_6 *)(776UL), (struct type_6 *)(776UL), var_56, 0UL);
                    var_59 = helper_cvtsi2sd_wrapper((struct type_4 *)(0UL), (struct type_6 *)(776UL), *var_11);
                    var_60 = helper_divsd_wrapper((struct type_4 *)(0UL), (struct type_6 *)(840UL), (struct type_6 *)(776UL), var_59.field_0, *(uint64_t *)4300760UL, var_58, var_5, var_6, var_7, var_8, var_9);
                    var_61 = var_60.field_0;
                    var_62 = helper_ucomisd_wrapper_132((struct type_4 *)(0UL), (struct type_6 *)(840UL), (struct type_6 *)(776UL), var_61, *var_14, var_60.field_1, var_5);
                    rax_0 = 9218868437227405312UL;
                    if ((var_62.field_0 & 65UL) != 0UL) {
                        *(uint64_t *)(var_0 + (-64L)) = 4235319UL;
                        indirect_placeholder();
                        *(uint32_t *)var_53 = 34U;
                        loop_state_var = 1U;
                        break;
                    }
                    var_63 = var_62.field_1;
                    var_64 = helper_pxor_xmm_wrapper((struct type_4 *)(0UL), (struct type_6 *)(776UL), (struct type_6 *)(776UL), var_61, 0UL);
                    var_65 = var_64.field_1;
                    var_66 = helper_cvtsi2sd_wrapper((struct type_4 *)(0UL), (struct type_6 *)(776UL), *var_11);
                    var_67 = var_66.field_0;
                    var_68 = helper_mulsd_wrapper((struct type_4 *)(0UL), (struct type_6 *)(840UL), (struct type_6 *)(776UL), var_67, *var_14, var_63, var_5, var_6, var_7, var_8, var_9);
                    var_69 = var_68.field_1;
                    *var_14 = var_68.field_0;
                    var_53 = *var_12;
                    state_0x8560_2 = var_65;
                    state_0x8549_2 = var_69;
                    state_0x8558_2 = var_67;
                    continue;
                }
            rax_0 = *var_14;
        }
    }
}

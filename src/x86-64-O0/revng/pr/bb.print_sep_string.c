typedef unsigned char __u_char;
typedef unsigned short __u_short;
typedef unsigned int __u_int;
typedef unsigned long __u_long;
typedef signed char __int8_t;
typedef unsigned char __uint8_t;
typedef short __int16_t;
typedef unsigned short __uint16_t;
typedef int __int32_t;
typedef unsigned int __uint32_t;
typedef long __int64_t;
typedef unsigned long __uint64_t;
typedef __int8_t __int_least8_t;
typedef __uint8_t __uint_least8_t;
typedef __int16_t __int_least16_t;
typedef __uint16_t __uint_least16_t;
typedef __int32_t __int_least32_t;
typedef __uint32_t __uint_least32_t;
typedef __int64_t __int_least64_t;
typedef __uint64_t __uint_least64_t;
typedef long __quad_t;
typedef unsigned long __u_quad_t;
typedef long __intmax_t;
typedef unsigned long __uintmax_t;
typedef unsigned long __dev_t;
typedef unsigned int __uid_t;
typedef unsigned int __gid_t;
typedef unsigned long __ino_t;
typedef unsigned long __ino64_t;
typedef unsigned int __mode_t;
typedef unsigned long __nlink_t;
typedef long __off_t;
typedef long __off64_t;
typedef int __pid_t;
typedef struct {
    int __val[2];
} __fsid_t;
typedef long __clock_t;
typedef unsigned long __rlim_t;
typedef unsigned long __rlim64_t;
typedef unsigned int __id_t;
typedef long __time_t;
typedef unsigned int __useconds_t;
typedef long __suseconds_t;
typedef int __daddr_t;
typedef int __key_t;
typedef int __clockid_t;
typedef void *__timer_t;
typedef long __blksize_t;
typedef long __blkcnt_t;
typedef long __blkcnt64_t;
typedef unsigned long __fsblkcnt_t;
typedef unsigned long __fsblkcnt64_t;
typedef unsigned long __fsfilcnt_t;
typedef unsigned long __fsfilcnt64_t;
typedef long __fsword_t;
typedef long __ssize_t;
typedef long __syscall_slong_t;
typedef unsigned long __syscall_ulong_t;
typedef __off64_t __loff_t;
typedef char *__caddr_t;
typedef long __intptr_t;
typedef unsigned int __socklen_t;
typedef int __sig_atomic_t;
typedef __int8_t int8_t;
typedef __int16_t int16_t;
typedef __int32_t int32_t;
typedef __int64_t int64_t;
typedef __uint8_t uint8_t;
typedef __uint16_t uint16_t;
typedef __uint32_t uint32_t;
typedef __uint64_t uint64_t;
typedef __int_least8_t int_least8_t;
typedef __int_least16_t int_least16_t;
typedef __int_least32_t int_least32_t;
typedef __int_least64_t int_least64_t;
typedef __uint_least8_t uint_least8_t;
typedef __uint_least16_t uint_least16_t;
typedef __uint_least32_t uint_least32_t;
typedef __uint_least64_t uint_least64_t;
typedef signed char int_fast8_t;
typedef long int_fast16_t;
typedef long int_fast32_t;
typedef long int_fast64_t;
typedef unsigned char uint_fast8_t;
typedef unsigned long uint_fast16_t;
typedef unsigned long uint_fast32_t;
typedef unsigned long uint_fast64_t;
typedef long intptr_t;
typedef unsigned long uintptr_t;
typedef __intmax_t intmax_t;
typedef __uintmax_t uintmax_t;
struct bb_print_sep_string_ret_type;
struct indirect_placeholder_58_ret_type;
struct indirect_placeholder_59_ret_type;
struct indirect_placeholder_60_ret_type;
struct bb_print_sep_string_ret_type {
    uint64_t field_0;
    uint64_t field_1;
    uint64_t field_2;
    uint64_t field_3;
};
struct indirect_placeholder_58_ret_type {
    uint64_t field_0;
    uint64_t field_1;
    uint64_t field_2;
    uint64_t field_3;
    uint64_t field_4;
    uint64_t field_5;
};
struct indirect_placeholder_59_ret_type {
    uint64_t field_0;
    uint64_t field_1;
    uint64_t field_2;
    uint64_t field_3;
    uint64_t field_4;
    uint64_t field_5;
};
struct indirect_placeholder_60_ret_type {
    uint64_t field_0;
    uint64_t field_1;
    uint64_t field_2;
    uint64_t field_3;
    uint64_t field_4;
    uint64_t field_5;
};
extern uint64_t revng_init_local_sp(uint64_t param_0);
extern uint64_t init_rbp(void);
extern void indirect_placeholder(void);
extern uint64_t helper_cc_compute_all_wrapper(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint32_t param_3);
extern struct indirect_placeholder_58_ret_type indirect_placeholder_58(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint64_t param_3, uint64_t param_4);
extern struct indirect_placeholder_59_ret_type indirect_placeholder_59(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint64_t param_3, uint64_t param_4);
extern struct indirect_placeholder_60_ret_type indirect_placeholder_60(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint64_t param_3, uint64_t param_4);
typedef _Bool bool;
struct bb_print_sep_string_ret_type bb_print_sep_string(uint64_t r10, uint64_t r9, uint64_t r8, uint64_t rbx, uint64_t rdi) {
    struct indirect_placeholder_60_ret_type var_21;
    struct indirect_placeholder_58_ret_type var_9;
    struct indirect_placeholder_59_ret_type var_17;
    uint64_t var_0;
    uint64_t var_1;
    uint64_t var_2;
    uint64_t var_3;
    uint64_t *var_4;
    uint32_t var_5;
    uint32_t *var_6;
    uint64_t var_7;
    uint64_t var_8;
    uint64_t r104_1;
    struct bb_print_sep_string_ret_type mrv1;
    struct bb_print_sep_string_ret_type mrv2;
    uint64_t local_sp_2;
    uint32_t var_10;
    uint64_t r104_3;
    uint64_t local_sp_0;
    uint64_t r95_3;
    uint64_t r104_0;
    uint64_t r86_3;
    uint64_t r95_0;
    uint64_t rbx7_3;
    uint64_t r86_0;
    uint64_t rdi8_2;
    uint64_t rbx7_0;
    uint64_t rdi8_0;
    uint64_t var_11;
    uint64_t r95_1;
    uint64_t r86_1;
    uint64_t rbx7_1;
    struct bb_print_sep_string_ret_type mrv;
    uint64_t r104_4;
    uint64_t var_22;
    struct bb_print_sep_string_ret_type mrv3;
    uint64_t local_sp_3;
    uint64_t local_sp_1_ph;
    uint64_t r95_4;
    uint64_t r104_2_ph;
    uint64_t r86_4;
    uint64_t r95_2_ph;
    uint64_t rbx7_4;
    uint64_t r86_2_ph;
    uint64_t rbx7_2_ph;
    uint64_t rdi8_1_ph;
    uint32_t var_12;
    uint64_t var_13;
    uint64_t var_14;
    uint64_t var_15;
    uint64_t var_16;
    uint32_t var_18;
    uint64_t var_19;
    uint64_t var_20;
    uint64_t var_23;
    uint64_t var_24;
    unsigned int loop_state_var;
    bool switch_state_var;
    var_0 = revng_init_local_sp(0UL);
    var_1 = init_rbp();
    *(uint64_t *)(var_0 + (-8L)) = var_1;
    var_2 = *(uint64_t *)6430304UL;
    var_3 = var_0 + (-16L);
    var_4 = (uint64_t *)var_3;
    *var_4 = var_2;
    var_5 = *(uint32_t *)6430828UL;
    var_6 = (uint32_t *)(var_0 + (-20L));
    *var_6 = var_5;
    var_7 = helper_cc_compute_all_wrapper((uint64_t)*(uint32_t *)6430832UL, 0UL, 0UL, 24U);
    r104_1 = r10;
    r104_0 = r10;
    r95_0 = r9;
    r86_0 = r8;
    rbx7_0 = rbx;
    rdi8_0 = rdi;
    r95_1 = r9;
    r86_1 = r8;
    rbx7_1 = rbx;
    if ((uint64_t)(((unsigned char)(var_7 >> 4UL) ^ (unsigned char)var_7) & '\xc0') == 0UL) {
        var_10 = *(uint32_t *)6430832UL;
        local_sp_0 = var_0 + (-24L);
        var_11 = helper_cc_compute_all_wrapper((uint64_t)var_10, 0UL, 0UL, 24U);
        r104_1 = r104_0;
        r95_1 = r95_0;
        r86_1 = r86_0;
        rbx7_1 = rbx7_0;
        local_sp_1_ph = local_sp_0;
        r104_2_ph = r104_0;
        r95_2_ph = r95_0;
        r86_2_ph = r86_0;
        rbx7_2_ph = rbx7_0;
        rdi8_1_ph = rdi8_0;
        while ((uint64_t)(((unsigned char)(var_11 >> 4UL) ^ (unsigned char)var_11) & '\xc0') != 0UL)
            {
                while (1U)
                    {
                        local_sp_2 = local_sp_1_ph;
                        r104_3 = r104_2_ph;
                        r95_3 = r95_2_ph;
                        r86_3 = r86_2_ph;
                        rbx7_3 = rbx7_2_ph;
                        rdi8_2 = rdi8_1_ph;
                        r104_4 = r104_2_ph;
                        local_sp_3 = local_sp_1_ph;
                        r95_4 = r95_2_ph;
                        r86_4 = r86_2_ph;
                        rbx7_4 = rbx7_2_ph;
                        while (1U)
                            {
                                var_12 = *var_6;
                                var_13 = (uint64_t)var_12;
                                *var_6 = (var_12 + (-1));
                                var_14 = helper_cc_compute_all_wrapper(var_13, 0UL, 0UL, 24U);
                                if ((uint64_t)(((unsigned char)(var_14 >> 4UL) ^ (unsigned char)var_14) & '\xc0') != 0UL) {
                                    loop_state_var = 0U;
                                    break;
                                }
                                if (**(unsigned char **)var_3 != ' ') {
                                    loop_state_var = 1U;
                                    break;
                                }
                                *var_4 = (*var_4 + 1UL);
                                *(uint32_t *)6430748UL = (*(uint32_t *)6430748UL + 1U);
                                continue;
                            }
                        switch_state_var = 0;
                        switch (loop_state_var) {
                          case 0U:
                            {
                                switch_state_var = 1;
                                break;
                            }
                            break;
                          case 1U:
                            {
                                var_19 = helper_cc_compute_all_wrapper((uint64_t)*(uint32_t *)6430748UL, 0UL, 0UL, 24U);
                                if ((uint64_t)(((unsigned char)(var_19 >> 4UL) ^ (unsigned char)var_19) & '\xc0') == 0UL) {
                                    var_20 = local_sp_1_ph + (-8L);
                                    *(uint64_t *)var_20 = 4213467UL;
                                    var_21 = indirect_placeholder_60(r104_2_ph, r95_2_ph, r86_2_ph, rbx7_2_ph, rdi8_1_ph);
                                    r104_4 = var_21.field_0;
                                    local_sp_3 = var_20;
                                    r95_4 = var_21.field_1;
                                    r86_4 = var_21.field_2;
                                    rbx7_4 = var_21.field_4;
                                }
                                var_22 = *var_4;
                                *var_4 = (var_22 + 1UL);
                                var_23 = (uint64_t)(uint32_t)(uint64_t)*(unsigned char *)var_22;
                                var_24 = local_sp_3 + (-8L);
                                *(uint64_t *)var_24 = 4213492UL;
                                indirect_placeholder();
                                *(uint32_t *)6430756UL = (*(uint32_t *)6430756UL + 1U);
                                local_sp_1_ph = var_24;
                                r104_2_ph = r104_4;
                                r95_2_ph = r95_4;
                                r86_2_ph = r86_4;
                                rbx7_2_ph = rbx7_4;
                                rdi8_1_ph = var_23;
                                continue;
                            }
                            break;
                        }
                        if (switch_state_var)
                            break;
                    }
                var_15 = helper_cc_compute_all_wrapper((uint64_t)*(uint32_t *)6430748UL, 0UL, 0UL, 24U);
                if ((uint64_t)(((unsigned char)(var_15 >> 4UL) ^ (unsigned char)var_15) & '\xc0') == 0UL) {
                    var_16 = local_sp_1_ph + (-8L);
                    *(uint64_t *)var_16 = 4213535UL;
                    var_17 = indirect_placeholder_59(r104_2_ph, r95_2_ph, r86_2_ph, rbx7_2_ph, rdi8_1_ph);
                    local_sp_2 = var_16;
                    r104_3 = var_17.field_0;
                    r95_3 = var_17.field_1;
                    r86_3 = var_17.field_2;
                    rbx7_3 = var_17.field_4;
                    rdi8_2 = var_17.field_5;
                }
                var_18 = *(uint32_t *)6430832UL + (-1);
                *(uint32_t *)6430832UL = var_18;
                var_10 = var_18;
                local_sp_0 = local_sp_2;
                r104_0 = r104_3;
                r95_0 = r95_3;
                r86_0 = r86_3;
                rbx7_0 = rbx7_3;
                rdi8_0 = rdi8_2;
                var_11 = helper_cc_compute_all_wrapper((uint64_t)var_10, 0UL, 0UL, 24U);
                r104_1 = r104_0;
                r95_1 = r95_0;
                r86_1 = r86_0;
                rbx7_1 = rbx7_0;
                local_sp_1_ph = local_sp_0;
                r104_2_ph = r104_0;
                r95_2_ph = r95_0;
                r86_2_ph = r86_0;
                rbx7_2_ph = rbx7_0;
                rdi8_1_ph = rdi8_0;
            }
    }
    var_8 = helper_cc_compute_all_wrapper((uint64_t)*(uint32_t *)6430748UL, 0UL, 0UL, 24U);
    if ((uint64_t)(((unsigned char)(var_8 >> 4UL) ^ (unsigned char)var_8) & '\xc0') != 0UL) {
        mrv.field_0 = r104_1;
        mrv1 = mrv;
        mrv1.field_1 = r95_1;
        mrv2 = mrv1;
        mrv2.field_2 = r86_1;
        mrv3 = mrv2;
        mrv3.field_3 = rbx7_1;
        return mrv3;
    }
    *(uint64_t *)(var_0 + (-32L)) = 4213407UL;
    var_9 = indirect_placeholder_58(r10, r9, r8, rbx, rdi);
    r104_1 = var_9.field_0;
    r95_1 = var_9.field_1;
    r86_1 = var_9.field_2;
    rbx7_1 = var_9.field_4;
    mrv.field_0 = r104_1;
    mrv1 = mrv;
    mrv1.field_1 = r95_1;
    mrv2 = mrv1;
    mrv2.field_2 = r86_1;
    mrv3 = mrv2;
    mrv3.field_3 = rbx7_1;
    return mrv3;
}

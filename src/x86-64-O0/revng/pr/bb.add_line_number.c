typedef unsigned char __u_char;
typedef unsigned short __u_short;
typedef unsigned int __u_int;
typedef unsigned long __u_long;
typedef signed char __int8_t;
typedef unsigned char __uint8_t;
typedef short __int16_t;
typedef unsigned short __uint16_t;
typedef int __int32_t;
typedef unsigned int __uint32_t;
typedef long __int64_t;
typedef unsigned long __uint64_t;
typedef __int8_t __int_least8_t;
typedef __uint8_t __uint_least8_t;
typedef __int16_t __int_least16_t;
typedef __uint16_t __uint_least16_t;
typedef __int32_t __int_least32_t;
typedef __uint32_t __uint_least32_t;
typedef __int64_t __int_least64_t;
typedef __uint64_t __uint_least64_t;
typedef long __quad_t;
typedef unsigned long __u_quad_t;
typedef long __intmax_t;
typedef unsigned long __uintmax_t;
typedef unsigned long __dev_t;
typedef unsigned int __uid_t;
typedef unsigned int __gid_t;
typedef unsigned long __ino_t;
typedef unsigned long __ino64_t;
typedef unsigned int __mode_t;
typedef unsigned long __nlink_t;
typedef long __off_t;
typedef long __off64_t;
typedef int __pid_t;
typedef struct {
    int __val[2];
} __fsid_t;
typedef long __clock_t;
typedef unsigned long __rlim_t;
typedef unsigned long __rlim64_t;
typedef unsigned int __id_t;
typedef long __time_t;
typedef unsigned int __useconds_t;
typedef long __suseconds_t;
typedef int __daddr_t;
typedef int __key_t;
typedef int __clockid_t;
typedef void *__timer_t;
typedef long __blksize_t;
typedef long __blkcnt_t;
typedef long __blkcnt64_t;
typedef unsigned long __fsblkcnt_t;
typedef unsigned long __fsblkcnt64_t;
typedef unsigned long __fsfilcnt_t;
typedef unsigned long __fsfilcnt64_t;
typedef long __fsword_t;
typedef long __ssize_t;
typedef long __syscall_slong_t;
typedef unsigned long __syscall_ulong_t;
typedef __off64_t __loff_t;
typedef char *__caddr_t;
typedef long __intptr_t;
typedef unsigned int __socklen_t;
typedef int __sig_atomic_t;
typedef __int8_t int8_t;
typedef __int16_t int16_t;
typedef __int32_t int32_t;
typedef __int64_t int64_t;
typedef __uint8_t uint8_t;
typedef __uint16_t uint16_t;
typedef __uint32_t uint32_t;
typedef __uint64_t uint64_t;
typedef __int_least8_t int_least8_t;
typedef __int_least16_t int_least16_t;
typedef __int_least32_t int_least32_t;
typedef __int_least64_t int_least64_t;
typedef __uint_least8_t uint_least8_t;
typedef __uint_least16_t uint_least16_t;
typedef __uint_least32_t uint_least32_t;
typedef __uint_least64_t uint_least64_t;
typedef signed char int_fast8_t;
typedef long int_fast16_t;
typedef long int_fast32_t;
typedef long int_fast64_t;
typedef unsigned char uint_fast8_t;
typedef unsigned long uint_fast16_t;
typedef unsigned long uint_fast32_t;
typedef unsigned long uint_fast64_t;
typedef long intptr_t;
typedef unsigned long uintptr_t;
typedef __intmax_t intmax_t;
typedef __uintmax_t uintmax_t;
typedef _Bool bool;
struct helper_idivl_EAX_wrapper_ret_type;
struct type_5;
struct helper_idivl_EAX_wrapper_ret_type {
    uint64_t field_0;
    uint64_t field_1;
    uint64_t field_2;
    uint32_t field_3;
    uint64_t field_4;
    uint32_t field_5;
    uint32_t field_6;
    uint64_t field_7;
    uint32_t field_8;
    uint64_t field_9;
    uint32_t field_10;
    unsigned char field_11;
    uint32_t field_12;
};
struct type_5 {
};
extern uint64_t revng_init_local_sp(uint64_t param_0);
extern uint64_t init_rbp(void);
extern void indirect_placeholder(void);
extern uint64_t init_rbx(void);
extern uint64_t init_r10(void);
extern uint32_t init_state_0x8248(void);
extern uint64_t init_state_0x9018(void);
extern uint32_t init_state_0x9010(void);
extern uint64_t init_state_0x8408(void);
extern uint64_t init_state_0x8328(void);
extern uint64_t init_state_0x82d8(void);
extern uint32_t init_state_0x9080(void);
extern uint64_t init_r9(void);
extern uint64_t init_r8(void);
extern uint64_t helper_cc_compute_all_wrapper(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint32_t param_3);
extern struct helper_idivl_EAX_wrapper_ret_type helper_idivl_EAX_wrapper(struct type_5 *param_0, uint64_t param_1, uint64_t param_2, uint64_t param_3, uint64_t param_4, uint64_t param_5, uint64_t param_6, uint64_t param_7, uint64_t param_8, uint64_t param_9, uint64_t param_10, uint64_t param_11, uint64_t param_12, uint32_t param_13, uint64_t param_14, uint32_t param_15, uint64_t param_16, uint64_t param_17, uint64_t param_18, uint32_t param_19);
void bb_add_line_number(uint64_t rdi) {
    uint64_t var_0;
    uint64_t var_1;
    uint64_t var_2;
    uint64_t var_3;
    uint64_t var_4;
    uint64_t var_5;
    uint32_t var_6;
    uint64_t var_7;
    uint32_t var_8;
    uint64_t var_9;
    uint64_t var_10;
    uint64_t var_11;
    uint32_t var_12;
    uint64_t var_13;
    uint64_t var_14;
    uint32_t *var_15;
    uint64_t var_16;
    uint64_t *var_17;
    uint32_t var_18;
    uint32_t *var_19;
    uint64_t var_23;
    uint32_t var_24;
    uint64_t var_25;
    uint64_t var_26;
    struct helper_idivl_EAX_wrapper_ret_type var_27;
    uint32_t var_20;
    uint64_t local_sp_0;
    bool var_21;
    unsigned char var_22;
    uint32_t var_28;
    uint32_t var_29;
    uint64_t local_sp_1;
    uint64_t var_30;
    uint64_t var_31;
    uint64_t var_32;
    uint64_t var_33;
    uint32_t var_34;
    var_0 = revng_init_local_sp(0UL);
    var_1 = init_r10();
    var_2 = init_r9();
    var_3 = init_r8();
    var_4 = init_rbp();
    var_5 = init_rbx();
    var_6 = init_state_0x8248();
    var_7 = init_state_0x9018();
    var_8 = init_state_0x9010();
    var_9 = init_state_0x8408();
    var_10 = init_state_0x8328();
    var_11 = init_state_0x82d8();
    var_12 = init_state_0x9080();
    var_13 = var_0 + (-8L);
    *(uint64_t *)var_13 = var_4;
    *(uint64_t *)(var_0 + (-48L)) = rdi;
    var_14 = var_0 + (-64L);
    *(uint64_t *)var_14 = 4212200UL;
    indirect_placeholder();
    var_15 = (uint32_t *)(var_0 + (-28L));
    *var_15 = 0U;
    *(uint32_t *)6430792UL = (*(uint32_t *)6430792UL + 1U);
    var_16 = (uint64_t)((long)((uint64_t)(*var_15 - *(uint32_t *)6430296UL) << 32UL) >> (long)32UL) + *(uint64_t *)6430808UL;
    var_17 = (uint64_t *)(var_0 + (-24L));
    *var_17 = var_16;
    var_18 = *(uint32_t *)6430296UL;
    var_19 = (uint32_t *)(var_0 + (-12L));
    *var_19 = var_18;
    var_20 = var_18;
    local_sp_0 = var_14;
    local_sp_1 = local_sp_0;
    while ((int)var_20 <= (int)0U)
        {
            *var_17 = (*var_17 + 1UL);
            var_33 = local_sp_0 + (-8L);
            *(uint64_t *)var_33 = 4212288UL;
            indirect_placeholder();
            var_34 = *var_19 + (-1);
            *var_19 = var_34;
            var_20 = var_34;
            local_sp_0 = var_33;
            local_sp_1 = local_sp_0;
        }
    var_21 = ((int)*(uint32_t *)6430268UL > (int)1U);
    var_22 = *(unsigned char *)6430280UL;
    if (var_21) {
        if (var_22 != '\t') {
            var_28 = *(uint32_t *)6430800UL - *(uint32_t *)6430296UL;
            *var_19 = var_28;
            var_29 = var_28;
            var_30 = (uint64_t)var_29;
            *var_19 = (var_29 + (-1));
            var_31 = helper_cc_compute_all_wrapper(var_30, 0UL, 0UL, 24U);
            while ((uint64_t)(((unsigned char)(var_31 >> 4UL) ^ (unsigned char)var_31) & '\xc0') != 0UL)
                {
                    var_32 = local_sp_1 + (-8L);
                    *(uint64_t *)var_32 = 4212356UL;
                    indirect_placeholder();
                    var_29 = *var_19;
                    local_sp_1 = var_32;
                    var_30 = (uint64_t)var_29;
                    *var_19 = (var_29 + (-1));
                    var_31 = helper_cc_compute_all_wrapper(var_30, 0UL, 0UL, 24U);
                }
        }
        *(uint64_t *)(local_sp_0 + (-8L)) = 4212393UL;
        indirect_placeholder();
    } else {
        *(uint64_t *)(local_sp_0 + (-8L)) = 4212417UL;
        indirect_placeholder();
        if (*(unsigned char *)6430280UL == '\t') {
            var_23 = (uint64_t)(uint32_t)(uint64_t)var_22;
            var_24 = *(uint32_t *)6430264UL;
            var_25 = (uint64_t)var_24;
            var_26 = (uint64_t)*(uint32_t *)6430756UL;
            var_27 = helper_idivl_EAX_wrapper((struct type_5 *)(0UL), var_25, 4212447UL, var_1, var_2, var_3, var_26, var_13, (uint64_t)(uint32_t)(uint64_t)((long)(var_26 << 32UL) >> (long)63UL), var_25, var_5, var_25, var_23, var_6, var_7, var_8, var_9, var_10, var_11, var_12);
            *(uint32_t *)6430756UL = (*(uint32_t *)6430756UL + (var_24 - (uint32_t)var_27.field_4));
        }
    }
    if (~(*(unsigned char *)6430736UL != '\x00' & *(unsigned char *)6430720UL == '\x01')) {
        return;
    }
    *(uint32_t *)6430760UL = (*(uint32_t *)6430800UL + *(uint32_t *)6430760UL);
    return;
}

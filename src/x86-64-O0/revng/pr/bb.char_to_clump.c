typedef unsigned char __u_char;
typedef unsigned short __u_short;
typedef unsigned int __u_int;
typedef unsigned long __u_long;
typedef signed char __int8_t;
typedef unsigned char __uint8_t;
typedef short __int16_t;
typedef unsigned short __uint16_t;
typedef int __int32_t;
typedef unsigned int __uint32_t;
typedef long __int64_t;
typedef unsigned long __uint64_t;
typedef __int8_t __int_least8_t;
typedef __uint8_t __uint_least8_t;
typedef __int16_t __int_least16_t;
typedef __uint16_t __uint_least16_t;
typedef __int32_t __int_least32_t;
typedef __uint32_t __uint_least32_t;
typedef __int64_t __int_least64_t;
typedef __uint64_t __uint_least64_t;
typedef long __quad_t;
typedef unsigned long __u_quad_t;
typedef long __intmax_t;
typedef unsigned long __uintmax_t;
typedef unsigned long __dev_t;
typedef unsigned int __uid_t;
typedef unsigned int __gid_t;
typedef unsigned long __ino_t;
typedef unsigned long __ino64_t;
typedef unsigned int __mode_t;
typedef unsigned long __nlink_t;
typedef long __off_t;
typedef long __off64_t;
typedef int __pid_t;
typedef struct {
    int __val[2];
} __fsid_t;
typedef long __clock_t;
typedef unsigned long __rlim_t;
typedef unsigned long __rlim64_t;
typedef unsigned int __id_t;
typedef long __time_t;
typedef unsigned int __useconds_t;
typedef long __suseconds_t;
typedef int __daddr_t;
typedef int __key_t;
typedef int __clockid_t;
typedef void *__timer_t;
typedef long __blksize_t;
typedef long __blkcnt_t;
typedef long __blkcnt64_t;
typedef unsigned long __fsblkcnt_t;
typedef unsigned long __fsblkcnt64_t;
typedef unsigned long __fsfilcnt_t;
typedef unsigned long __fsfilcnt64_t;
typedef long __fsword_t;
typedef long __ssize_t;
typedef long __syscall_slong_t;
typedef unsigned long __syscall_ulong_t;
typedef __off64_t __loff_t;
typedef char *__caddr_t;
typedef long __intptr_t;
typedef unsigned int __socklen_t;
typedef int __sig_atomic_t;
typedef __int8_t int8_t;
typedef __int16_t int16_t;
typedef __int32_t int32_t;
typedef __int64_t int64_t;
typedef __uint8_t uint8_t;
typedef __uint16_t uint16_t;
typedef __uint32_t uint32_t;
typedef __uint64_t uint64_t;
typedef __int_least8_t int_least8_t;
typedef __int_least16_t int_least16_t;
typedef __int_least32_t int_least32_t;
typedef __int_least64_t int_least64_t;
typedef __uint_least8_t uint_least8_t;
typedef __uint_least16_t uint_least16_t;
typedef __uint_least32_t uint_least32_t;
typedef __uint_least64_t uint_least64_t;
typedef signed char int_fast8_t;
typedef long int_fast16_t;
typedef long int_fast32_t;
typedef long int_fast64_t;
typedef unsigned char uint_fast8_t;
typedef unsigned long uint_fast16_t;
typedef unsigned long uint_fast32_t;
typedef unsigned long uint_fast64_t;
typedef long intptr_t;
typedef unsigned long uintptr_t;
typedef __intmax_t intmax_t;
typedef __uintmax_t uintmax_t;
typedef _Bool bool;
struct bb_char_to_clump_ret_type;
struct helper_idivl_EAX_wrapper_ret_type;
struct type_6;
struct bb_char_to_clump_ret_type {
    uint64_t field_0;
    uint64_t field_1;
    uint64_t field_2;
    uint64_t field_3;
    uint64_t field_4;
    uint64_t field_5;
};
struct helper_idivl_EAX_wrapper_ret_type {
    uint64_t field_0;
    uint64_t field_1;
    uint64_t field_2;
    uint32_t field_3;
    uint64_t field_4;
    uint32_t field_5;
    uint32_t field_6;
    uint64_t field_7;
    uint32_t field_8;
    uint64_t field_9;
    uint32_t field_10;
    unsigned char field_11;
    uint32_t field_12;
};
struct type_6 {
};
extern uint64_t revng_init_local_sp(uint64_t param_0);
extern uint64_t init_rbp(void);
extern void indirect_placeholder(void);
extern uint32_t init_state_0x8248(void);
extern uint64_t init_state_0x9018(void);
extern uint32_t init_state_0x9010(void);
extern uint64_t init_state_0x8408(void);
extern uint64_t init_state_0x8328(void);
extern uint64_t init_state_0x82d8(void);
extern uint32_t init_state_0x9080(void);
extern struct helper_idivl_EAX_wrapper_ret_type helper_idivl_EAX_wrapper(struct type_6 *param_0, uint64_t param_1, uint64_t param_2, uint64_t param_3, uint64_t param_4, uint64_t param_5, uint64_t param_6, uint64_t param_7, uint64_t param_8, uint64_t param_9, uint64_t param_10, uint64_t param_11, uint64_t param_12, uint32_t param_13, uint64_t param_14, uint32_t param_15, uint64_t param_16, uint64_t param_17, uint64_t param_18, uint32_t param_19);
struct bb_char_to_clump_ret_type bb_char_to_clump(uint64_t r10, uint64_t r9, uint64_t r8, uint64_t rcx, uint64_t rbx, uint64_t rsi, uint64_t rdi) {
    uint64_t var_0;
    uint64_t var_1;
    uint32_t var_2;
    uint64_t var_3;
    uint32_t var_4;
    uint64_t var_5;
    uint64_t var_6;
    uint64_t var_7;
    uint32_t var_8;
    uint64_t var_9;
    unsigned char *var_10;
    unsigned char var_11;
    unsigned char *var_12;
    uint64_t var_13;
    uint64_t var_14;
    uint64_t *var_15;
    uint32_t *var_16;
    unsigned char var_17;
    unsigned char var_18;
    uint64_t var_19;
    uint64_t _pre_phi95;
    uint32_t var_38;
    uint32_t *_pre_phi97;
    uint64_t var_39;
    uint32_t var_40;
    uint32_t var_21;
    uint32_t var_29;
    uint64_t var_30;
    uint32_t var_31;
    uint32_t var_20;
    unsigned char _pre;
    unsigned char _pre87;
    unsigned char var_22;
    unsigned char var_23;
    uint32_t *var_25;
    uint64_t var_26;
    uint64_t var_27;
    uint32_t *var_28;
    bool var_32;
    uint32_t *var_33;
    uint64_t rdi12_0;
    uint64_t var_34;
    uint64_t var_35;
    uint64_t var_36;
    uint32_t *var_37;
    bool var_41;
    uint32_t *var_42;
    uint32_t *var_24;
    uint64_t var_43;
    struct helper_idivl_EAX_wrapper_ret_type var_44;
    uint64_t var_45;
    uint32_t var_46;
    uint32_t *var_47;
    uint32_t var_48;
    uint32_t *var_49;
    uint32_t var_50;
    uint64_t var_51;
    uint32_t var_52;
    uint32_t var_53;
    bool var_54;
    uint32_t var_57;
    uint32_t _pre88;
    uint64_t var_55;
    uint32_t var_56;
    uint64_t var_58;
    struct bb_char_to_clump_ret_type mrv;
    struct bb_char_to_clump_ret_type mrv1;
    struct bb_char_to_clump_ret_type mrv2;
    struct bb_char_to_clump_ret_type mrv3;
    struct bb_char_to_clump_ret_type mrv4;
    struct bb_char_to_clump_ret_type mrv5;
    var_0 = revng_init_local_sp(0UL);
    var_1 = init_rbp();
    var_2 = init_state_0x8248();
    var_3 = init_state_0x9018();
    var_4 = init_state_0x9010();
    var_5 = init_state_0x8408();
    var_6 = init_state_0x8328();
    var_7 = init_state_0x82d8();
    var_8 = init_state_0x9080();
    var_9 = var_0 + (-8L);
    *(uint64_t *)var_9 = var_1;
    var_10 = (unsigned char *)(var_0 + (-44L));
    var_11 = (unsigned char)rdi;
    *var_10 = var_11;
    var_12 = (unsigned char *)(var_0 + (-33L));
    *var_12 = var_11;
    var_13 = *(uint64_t *)6430896UL;
    var_14 = var_0 + (-16L);
    var_15 = (uint64_t *)var_14;
    *var_15 = var_13;
    var_16 = (uint32_t *)(var_0 + (-32L));
    *var_16 = 8U;
    var_17 = *(unsigned char *)6430252UL;
    var_18 = *var_10;
    var_19 = (uint64_t)(var_18 - var_17);
    _pre_phi95 = var_19;
    var_38 = 0U;
    var_21 = 8U;
    var_29 = 0U;
    var_22 = var_18;
    rdi12_0 = rdi;
    if (var_19 == 0UL) {
        var_20 = *(uint32_t *)6430256UL;
        *var_16 = var_20;
        _pre = *(unsigned char *)6430252UL;
        _pre87 = *var_10;
        _pre_phi95 = (uint64_t)(_pre87 - _pre);
        var_21 = var_20;
        var_22 = _pre87;
    }
    if ((_pre_phi95 == 0UL) || (var_22 == '\t')) {
        var_43 = (uint64_t)*(uint32_t *)6430760UL;
        var_44 = helper_idivl_EAX_wrapper((struct type_6 *)(0UL), (uint64_t)var_21, 4215898UL, r10, r9, r8, var_43, var_9, (uint64_t)(uint32_t)(uint64_t)((long)(var_43 << 32UL) >> (long)63UL), rcx, rbx, rsi, rdi, var_2, var_3, var_4, var_5, var_6, var_7, var_8);
        var_45 = var_44.field_4;
        var_46 = *var_16;
        var_47 = (uint32_t *)(var_0 + (-24L));
        var_48 = var_46 - (uint32_t)var_45;
        *var_47 = var_48;
        var_50 = var_48;
        _pre_phi97 = var_47;
        if (*(unsigned char *)6430744UL == '\x00') {
            **(unsigned char **)var_14 = *var_10;
            *(uint32_t *)(var_0 + (-28L)) = 1U;
        } else {
            var_49 = (uint32_t *)(var_0 + (-20L));
            *var_49 = var_48;
            while (var_50 != 0U)
                {
                    var_51 = *var_15;
                    *var_15 = (var_51 + 1UL);
                    *(unsigned char *)var_51 = (unsigned char)' ';
                    var_52 = *var_49 + (-1);
                    *var_49 = var_52;
                    var_50 = var_52;
                }
            *(uint32_t *)(var_0 + (-28L)) = *var_47;
        }
    } else {
        var_23 = *var_12;
        if ((uint64_t)((uint32_t)(uint64_t)var_23 + (-32)) > 94UL) {
            var_24 = (uint32_t *)(var_0 + (-24L));
            *var_24 = 1U;
            *(uint32_t *)(var_0 + (-28L)) = 1U;
            **(unsigned char **)var_14 = *var_10;
            _pre_phi97 = var_24;
        } else {
            if (*(unsigned char *)6430816UL != '\x00') {
                var_25 = (uint32_t *)(var_0 + (-24L));
                *var_25 = 4U;
                *(uint32_t *)(var_0 + (-28L)) = 4U;
                var_26 = *var_15;
                *var_15 = (var_26 + 1UL);
                *(unsigned char *)var_26 = (unsigned char)'\\';
                var_27 = var_0 + (-40L);
                *(uint64_t *)(var_0 + (-64L)) = 4216068UL;
                indirect_placeholder();
                var_28 = (uint32_t *)(var_0 + (-20L));
                *var_28 = 0U;
                _pre_phi97 = var_25;
                rdi12_0 = var_27;
                while ((int)var_29 <= (int)2U)
                    {
                        var_30 = *var_15;
                        *var_15 = (var_30 + 1UL);
                        *(unsigned char *)var_30 = *(unsigned char *)((var_9 + (uint64_t)*var_28) + (-32L));
                        var_31 = *var_28 + 1U;
                        *var_28 = var_31;
                        var_29 = var_31;
                    }
            }
            if (*(unsigned char *)6430817UL == '\x00') {
                var_41 = (var_22 == '\b');
                var_42 = (uint32_t *)(var_0 + (-24L));
                _pre_phi97 = var_42;
                if (var_41) {
                    *var_42 = 4294967295U;
                    *(uint32_t *)(var_0 + (-28L)) = 1U;
                    **(unsigned char **)var_14 = *var_10;
                } else {
                    *var_42 = 0U;
                    *(uint32_t *)(var_0 + (-28L)) = 1U;
                    **(unsigned char **)var_14 = *var_10;
                }
            } else {
                var_32 = ((signed char)var_23 < '\x00');
                var_33 = (uint32_t *)(var_0 + (-24L));
                _pre_phi97 = var_33;
                if (!var_32) {
                    *var_33 = 4U;
                    *(uint32_t *)(var_0 + (-28L)) = 4U;
                    var_35 = *var_15;
                    *var_15 = (var_35 + 1UL);
                    *(unsigned char *)var_35 = (unsigned char)'\\';
                    var_36 = var_0 + (-40L);
                    *(uint64_t *)(var_0 + (-64L)) = 4216244UL;
                    indirect_placeholder();
                    var_37 = (uint32_t *)(var_0 + (-20L));
                    *var_37 = 0U;
                    rdi12_0 = var_36;
                    while ((int)var_38 <= (int)2U)
                        {
                            var_39 = *var_15;
                            *var_15 = (var_39 + 1UL);
                            *(unsigned char *)var_39 = *(unsigned char *)((var_9 + (uint64_t)*var_37) + (-32L));
                            var_40 = *var_37 + 1U;
                            *var_37 = var_40;
                            var_38 = var_40;
                        }
                }
                *var_33 = 2U;
                *(uint32_t *)(var_0 + (-28L)) = 2U;
                var_34 = *var_15;
                *var_15 = (var_34 + 1UL);
                *(unsigned char *)var_34 = (unsigned char)'^';
                **(unsigned char **)var_14 = (*var_10 ^ '@');
            }
        }
    }
    var_53 = *_pre_phi97;
    var_54 = ((int)var_53 > (int)4294967295U);
    if (!var_54) {
        if (*(uint32_t *)6430760UL != 0U) {
            *(uint32_t *)(var_0 + (-28L)) = 0U;
            *(uint32_t *)6430760UL = 0U;
            var_58 = (uint64_t)*(uint32_t *)(var_0 + (-28L));
            mrv.field_0 = r10;
            mrv1 = mrv;
            mrv1.field_1 = r9;
            mrv2 = mrv1;
            mrv2.field_2 = r8;
            mrv3 = mrv2;
            mrv3.field_3 = var_58;
            mrv4 = mrv3;
            mrv4.field_4 = rbx;
            mrv5 = mrv4;
            mrv5.field_5 = rdi12_0;
            return mrv5;
        }
    }
    if (var_54) {
        _pre88 = *(uint32_t *)6430760UL;
        var_57 = _pre88;
        *(uint32_t *)6430760UL = (var_57 + var_53);
    } else {
        var_55 = (uint64_t)(0U - var_53);
        var_56 = *(uint32_t *)6430760UL;
        var_57 = var_56;
        if ((long)(var_55 << 32UL) < (long)((uint64_t)var_56 << 32UL)) {
            *(uint32_t *)6430760UL = (var_57 + var_53);
        } else {
            *(uint32_t *)6430760UL = 0U;
        }
    }
    var_58 = (uint64_t)*(uint32_t *)(var_0 + (-28L));
    mrv.field_0 = r10;
    mrv1 = mrv;
    mrv1.field_1 = r9;
    mrv2 = mrv1;
    mrv2.field_2 = r8;
    mrv3 = mrv2;
    mrv3.field_3 = var_58;
    mrv4 = mrv3;
    mrv4.field_4 = rbx;
    mrv5 = mrv4;
    mrv5.field_5 = rdi12_0;
    return mrv5;
}

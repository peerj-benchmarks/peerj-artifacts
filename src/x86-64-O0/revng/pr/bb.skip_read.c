typedef unsigned char __u_char;
typedef unsigned short __u_short;
typedef unsigned int __u_int;
typedef unsigned long __u_long;
typedef signed char __int8_t;
typedef unsigned char __uint8_t;
typedef short __int16_t;
typedef unsigned short __uint16_t;
typedef int __int32_t;
typedef unsigned int __uint32_t;
typedef long __int64_t;
typedef unsigned long __uint64_t;
typedef __int8_t __int_least8_t;
typedef __uint8_t __uint_least8_t;
typedef __int16_t __int_least16_t;
typedef __uint16_t __uint_least16_t;
typedef __int32_t __int_least32_t;
typedef __uint32_t __uint_least32_t;
typedef __int64_t __int_least64_t;
typedef __uint64_t __uint_least64_t;
typedef long __quad_t;
typedef unsigned long __u_quad_t;
typedef long __intmax_t;
typedef unsigned long __uintmax_t;
typedef unsigned long __dev_t;
typedef unsigned int __uid_t;
typedef unsigned int __gid_t;
typedef unsigned long __ino_t;
typedef unsigned long __ino64_t;
typedef unsigned int __mode_t;
typedef unsigned long __nlink_t;
typedef long __off_t;
typedef long __off64_t;
typedef int __pid_t;
typedef struct {
    int __val[2];
} __fsid_t;
typedef long __clock_t;
typedef unsigned long __rlim_t;
typedef unsigned long __rlim64_t;
typedef unsigned int __id_t;
typedef long __time_t;
typedef unsigned int __useconds_t;
typedef long __suseconds_t;
typedef int __daddr_t;
typedef int __key_t;
typedef int __clockid_t;
typedef void *__timer_t;
typedef long __blksize_t;
typedef long __blkcnt_t;
typedef long __blkcnt64_t;
typedef unsigned long __fsblkcnt_t;
typedef unsigned long __fsblkcnt64_t;
typedef unsigned long __fsfilcnt_t;
typedef unsigned long __fsfilcnt64_t;
typedef long __fsword_t;
typedef long __ssize_t;
typedef long __syscall_slong_t;
typedef unsigned long __syscall_ulong_t;
typedef __off64_t __loff_t;
typedef char *__caddr_t;
typedef long __intptr_t;
typedef unsigned int __socklen_t;
typedef int __sig_atomic_t;
typedef __int8_t int8_t;
typedef __int16_t int16_t;
typedef __int32_t int32_t;
typedef __int64_t int64_t;
typedef __uint8_t uint8_t;
typedef __uint16_t uint16_t;
typedef __uint32_t uint32_t;
typedef __uint64_t uint64_t;
typedef __int_least8_t int_least8_t;
typedef __int_least16_t int_least16_t;
typedef __int_least32_t int_least32_t;
typedef __int_least64_t int_least64_t;
typedef __uint_least8_t uint_least8_t;
typedef __uint_least16_t uint_least16_t;
typedef __uint_least32_t uint_least32_t;
typedef __uint_least64_t uint_least64_t;
typedef signed char int_fast8_t;
typedef long int_fast16_t;
typedef long int_fast32_t;
typedef long int_fast64_t;
typedef unsigned char uint_fast8_t;
typedef unsigned long uint_fast16_t;
typedef unsigned long uint_fast32_t;
typedef unsigned long uint_fast64_t;
typedef long intptr_t;
typedef unsigned long uintptr_t;
typedef __intmax_t intmax_t;
typedef __uintmax_t uintmax_t;
extern uint64_t revng_init_local_sp(uint64_t param_0);
extern uint64_t init_rbp(void);
extern void indirect_placeholder(void);
extern void indirect_placeholder_4(uint64_t param_0);
typedef _Bool bool;
void bb_skip_read(uint64_t rsi, uint64_t rdi) {
    uint64_t var_0;
    uint64_t var_1;
    uint64_t var_2;
    uint64_t *var_3;
    uint32_t *var_4;
    uint64_t var_5;
    uint64_t *var_6;
    unsigned char *var_7;
    uint64_t var_8;
    uint64_t var_9;
    uint32_t *var_10;
    uint32_t var_11;
    uint64_t local_sp_1;
    uint64_t var_28;
    uint64_t local_sp_0;
    uint64_t var_29;
    uint64_t var_15;
    uint64_t var_16;
    uint64_t var_12;
    uint64_t var_13;
    uint32_t var_14;
    uint32_t var_17;
    uint64_t local_sp_2;
    uint64_t var_19;
    uint64_t *var_20;
    uint32_t var_21;
    uint32_t *var_22;
    uint32_t var_23;
    uint32_t var_24;
    uint64_t var_25;
    uint64_t var_26;
    uint32_t var_27;
    uint64_t var_18;
    uint64_t var_30;
    uint64_t var_31;
    uint32_t var_32;
    unsigned int loop_state_var;
    bool switch_state_var;
    var_0 = revng_init_local_sp(0UL);
    var_1 = init_rbp();
    *(uint64_t *)(var_0 + (-8L)) = var_1;
    var_2 = var_0 + (-48L);
    var_3 = (uint64_t *)var_2;
    *var_3 = rdi;
    var_4 = (uint32_t *)(var_0 + (-52L));
    *var_4 = (uint32_t)rsi;
    var_5 = **(uint64_t **)var_2;
    var_6 = (uint64_t *)(var_0 + (-40L));
    *var_6 = var_5;
    var_7 = (unsigned char *)(var_0 + (-17L));
    *var_7 = (unsigned char)'\x00';
    var_8 = *var_6;
    var_9 = var_0 + (-64L);
    *(uint64_t *)var_9 = 4212853UL;
    indirect_placeholder();
    var_10 = (uint32_t *)(var_0 + (-12L));
    var_11 = (uint32_t)var_8;
    *var_10 = var_11;
    local_sp_1 = var_9;
    var_12 = *var_6;
    var_13 = var_0 + (-72L);
    *(uint64_t *)var_13 = 4212886UL;
    indirect_placeholder();
    var_14 = (uint32_t)var_12;
    *var_10 = var_14;
    local_sp_1 = var_13;
    if (var_11 != 12U & *(unsigned char *)(*var_3 + 57UL) != '\x00' & var_14 == 10U) {
        var_15 = *var_6;
        var_16 = var_0 + (-80L);
        *(uint64_t *)var_16 = 4212907UL;
        indirect_placeholder();
        *var_10 = (uint32_t)var_15;
        local_sp_1 = var_16;
    }
    *(unsigned char *)(*var_3 + 57UL) = (unsigned char)'\x00';
    local_sp_2 = local_sp_1;
    if (*var_10 == 12U) {
        *var_7 = (unsigned char)'\x01';
    }
    if (*(unsigned char *)6430904UL == '\x00') {
        *(unsigned char *)(*var_3 + 57UL) = (unsigned char)'\x01';
    }
    var_17 = *var_10;
    while (1U)
        {
            switch_state_var = 0;
            switch (var_17) {
              case 4294967295U:
                {
                    var_18 = *var_3;
                    *(uint64_t *)(local_sp_2 + (-8L)) = 4213112UL;
                    indirect_placeholder_4(var_18);
                    loop_state_var = 0U;
                    switch_state_var = 1;
                    break;
                }
                break;
              case 12U:
                {
                    if (*(unsigned char *)6430904UL != '\x00') {
                        loop_state_var = 1U;
                        switch_state_var = 1;
                        break;
                    }
                    if (*(unsigned char *)6430720UL != '\x01') {
                        *(unsigned char *)(*var_3 + 57UL) = (unsigned char)'\x00';
                        loop_state_var = 1U;
                        switch_state_var = 1;
                        break;
                    }
                    var_19 = *(uint64_t *)6430672UL;
                    var_20 = (uint64_t *)(var_0 + (-32L));
                    *var_20 = var_19;
                    var_21 = *(uint32_t *)6430268UL;
                    var_22 = (uint32_t *)(var_0 + (-16L));
                    *var_22 = var_21;
                    var_23 = var_21;
                    loop_state_var = 2U;
                    switch_state_var = 1;
                    break;
                }
                break;
              case 10U:
                {
                    loop_state_var = 0U;
                    switch_state_var = 1;
                    break;
                }
                break;
              default:
                {
                    var_30 = *var_6;
                    var_31 = local_sp_2 + (-8L);
                    *(uint64_t *)var_31 = 4213126UL;
                    indirect_placeholder();
                    var_32 = (uint32_t)var_30;
                    *var_10 = var_32;
                    var_17 = var_32;
                    local_sp_2 = var_31;
                    continue;
                }
                break;
            }
            if (switch_state_var)
                break;
        }
    switch (loop_state_var) {
      case 0U:
        {
            if (*(unsigned char *)6430288UL == '\x00') {
                return;
            }
            if (*(unsigned char *)6430720UL != '\x01') {
                if (*var_4 == 1U) {
                    return;
                }
            }
            if (*var_7 == '\x01') {
                *(uint32_t *)6430284UL = (*(uint32_t *)6430284UL + 1U);
            }
        }
        break;
      case 2U:
      case 1U:
        {
            switch (loop_state_var) {
              case 2U:
                {
                    while (var_23 != 0U)
                        {
                            *(unsigned char *)(*var_20 + 57UL) = (unsigned char)'\x00';
                            *var_20 = (*var_20 + 64UL);
                            var_24 = *var_22 + (-1);
                            *var_22 = var_24;
                            var_23 = var_24;
                        }
                }
                break;
              case 1U:
                {
                    var_25 = *var_6;
                    var_26 = local_sp_2 + (-8L);
                    *(uint64_t *)var_26 = 4213054UL;
                    indirect_placeholder();
                    var_27 = (uint32_t)var_25;
                    *var_10 = var_27;
                    local_sp_0 = var_26;
                    if (var_27 != 10U) {
                        var_28 = local_sp_2 + (-16L);
                        *(uint64_t *)var_28 = 4213080UL;
                        indirect_placeholder();
                        local_sp_0 = var_28;
                    }
                    var_29 = *var_3;
                    *(uint64_t *)(local_sp_0 + (-8L)) = 4213092UL;
                    indirect_placeholder_4(var_29);
                }
                break;
            }
        }
        break;
    }
}

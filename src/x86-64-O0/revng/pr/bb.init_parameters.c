typedef unsigned char __u_char;
typedef unsigned short __u_short;
typedef unsigned int __u_int;
typedef unsigned long __u_long;
typedef signed char __int8_t;
typedef unsigned char __uint8_t;
typedef short __int16_t;
typedef unsigned short __uint16_t;
typedef int __int32_t;
typedef unsigned int __uint32_t;
typedef long __int64_t;
typedef unsigned long __uint64_t;
typedef __int8_t __int_least8_t;
typedef __uint8_t __uint_least8_t;
typedef __int16_t __int_least16_t;
typedef __uint16_t __uint_least16_t;
typedef __int32_t __int_least32_t;
typedef __uint32_t __uint_least32_t;
typedef __int64_t __int_least64_t;
typedef __uint64_t __uint_least64_t;
typedef long __quad_t;
typedef unsigned long __u_quad_t;
typedef long __intmax_t;
typedef unsigned long __uintmax_t;
typedef unsigned long __dev_t;
typedef unsigned int __uid_t;
typedef unsigned int __gid_t;
typedef unsigned long __ino_t;
typedef unsigned long __ino64_t;
typedef unsigned int __mode_t;
typedef unsigned long __nlink_t;
typedef long __off_t;
typedef long __off64_t;
typedef int __pid_t;
typedef struct {
    int __val[2];
} __fsid_t;
typedef long __clock_t;
typedef unsigned long __rlim_t;
typedef unsigned long __rlim64_t;
typedef unsigned int __id_t;
typedef long __time_t;
typedef unsigned int __useconds_t;
typedef long __suseconds_t;
typedef int __daddr_t;
typedef int __key_t;
typedef int __clockid_t;
typedef void *__timer_t;
typedef long __blksize_t;
typedef long __blkcnt_t;
typedef long __blkcnt64_t;
typedef unsigned long __fsblkcnt_t;
typedef unsigned long __fsblkcnt64_t;
typedef unsigned long __fsfilcnt_t;
typedef unsigned long __fsfilcnt64_t;
typedef long __fsword_t;
typedef long __ssize_t;
typedef long __syscall_slong_t;
typedef unsigned long __syscall_ulong_t;
typedef __off64_t __loff_t;
typedef char *__caddr_t;
typedef long __intptr_t;
typedef unsigned int __socklen_t;
typedef int __sig_atomic_t;
typedef __int8_t int8_t;
typedef __int16_t int16_t;
typedef __int32_t int32_t;
typedef __int64_t int64_t;
typedef __uint8_t uint8_t;
typedef __uint16_t uint16_t;
typedef __uint32_t uint32_t;
typedef __uint64_t uint64_t;
typedef __int_least8_t int_least8_t;
typedef __int_least16_t int_least16_t;
typedef __int_least32_t int_least32_t;
typedef __int_least64_t int_least64_t;
typedef __uint_least8_t uint_least8_t;
typedef __uint_least16_t uint_least16_t;
typedef __uint_least32_t uint_least32_t;
typedef __uint_least64_t uint_least64_t;
typedef signed char int_fast8_t;
typedef long int_fast16_t;
typedef long int_fast32_t;
typedef long int_fast64_t;
typedef unsigned char uint_fast8_t;
typedef unsigned long uint_fast16_t;
typedef unsigned long uint_fast32_t;
typedef unsigned long uint_fast64_t;
typedef long intptr_t;
typedef unsigned long uintptr_t;
typedef __intmax_t intmax_t;
typedef __uintmax_t uintmax_t;
typedef _Bool bool;
struct helper_idivl_EAX_wrapper_ret_type;
struct type_5;
struct indirect_placeholder_34_ret_type;
struct helper_idivl_EAX_wrapper_ret_type {
    uint64_t field_0;
    uint64_t field_1;
    uint64_t field_2;
    uint32_t field_3;
    uint64_t field_4;
    uint32_t field_5;
    uint32_t field_6;
    uint64_t field_7;
    uint32_t field_8;
    uint64_t field_9;
    uint32_t field_10;
    unsigned char field_11;
    uint32_t field_12;
};
struct type_5 {
};
struct indirect_placeholder_34_ret_type {
    uint64_t field_0;
    uint64_t field_1;
    uint64_t field_2;
    uint64_t field_3;
};
extern uint64_t revng_init_local_sp(uint64_t param_0);
extern uint64_t init_rbp(void);
extern uint64_t init_cc_src2(void);
extern uint64_t helper_cc_compute_c_wrapper(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint32_t param_3);
extern void indirect_placeholder(void);
extern uint32_t init_state_0x8248(void);
extern uint64_t init_state_0x9018(void);
extern uint32_t init_state_0x9010(void);
extern uint64_t init_state_0x8408(void);
extern uint64_t init_state_0x8328(void);
extern uint64_t init_state_0x82d8(void);
extern uint32_t init_state_0x9080(void);
extern uint64_t indirect_placeholder_2(uint64_t param_0);
extern uint64_t helper_cc_compute_all_wrapper(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint32_t param_3);
extern struct helper_idivl_EAX_wrapper_ret_type helper_idivl_EAX_wrapper(struct type_5 *param_0, uint64_t param_1, uint64_t param_2, uint64_t param_3, uint64_t param_4, uint64_t param_5, uint64_t param_6, uint64_t param_7, uint64_t param_8, uint64_t param_9, uint64_t param_10, uint64_t param_11, uint64_t param_12, uint32_t param_13, uint64_t param_14, uint32_t param_15, uint64_t param_16, uint64_t param_17, uint64_t param_18, uint32_t param_19);
extern struct indirect_placeholder_34_ret_type indirect_placeholder_34(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint64_t param_3, uint64_t param_4, uint64_t param_5, uint64_t param_6);
void bb_init_parameters(uint64_t r10, uint64_t r9, uint64_t r8, uint64_t rcx, uint64_t rbx, uint64_t rsi, uint64_t rdi) {
    uint32_t var_26;
    uint64_t var_27;
    uint64_t var_28;
    uint64_t var_30;
    uint32_t var_31;
    uint64_t var_32;
    uint32_t var_33;
    struct helper_idivl_EAX_wrapper_ret_type var_29;
    uint64_t var_34;
    uint32_t var_35;
    uint64_t var_36;
    uint64_t rax_0;
    uint32_t state_0x9080_4155;
    uint64_t var_37;
    uint64_t var_39;
    uint32_t var_40;
    uint64_t var_41;
    uint32_t var_42;
    struct helper_idivl_EAX_wrapper_ret_type var_38;
    uint64_t var_43;
    uint32_t var_44;
    uint32_t state_0x9080_4169;
    uint32_t var_45;
    uint64_t var_46;
    uint64_t var_47;
    uint64_t var_49;
    uint32_t var_50;
    uint64_t var_51;
    uint32_t var_52;
    struct helper_idivl_EAX_wrapper_ret_type var_48;
    uint64_t var_53;
    uint32_t var_54;
    uint64_t var_0;
    uint64_t var_1;
    uint64_t var_2;
    uint32_t var_3;
    uint64_t var_4;
    uint32_t var_5;
    uint64_t var_6;
    uint64_t var_7;
    uint64_t var_8;
    uint32_t var_9;
    uint64_t var_10;
    uint64_t var_11;
    uint32_t *var_12;
    uint32_t *var_13;
    uint32_t var_14;
    uint64_t var_15;
    uint64_t var_16;
    uint64_t var_90;
    uint32_t var_17;
    uint32_t var_18;
    bool var_19;
    unsigned char var_20;
    uint32_t state_0x9010_1;
    uint64_t state_0x82d8_1;
    uint32_t state_0x9080_1;
    uint32_t var_25;
    uint32_t state_0x8248_1;
    uint32_t *var_21;
    bool var_22;
    uint64_t var_23;
    struct helper_idivl_EAX_wrapper_ret_type var_24;
    uint32_t state_0x8248_0;
    uint64_t state_0x9018_0;
    uint32_t state_0x9010_0;
    uint64_t state_0x82d8_0;
    uint32_t state_0x9080_0;
    uint64_t storemerge_in;
    uint32_t storemerge;
    uint64_t state_0x9018_1;
    uint64_t var_55;
    uint64_t var_56;
    uint64_t var_58;
    uint32_t var_59;
    uint64_t var_60;
    uint32_t var_61;
    struct helper_idivl_EAX_wrapper_ret_type var_57;
    uint64_t var_62;
    uint32_t var_63;
    uint32_t var_64;
    uint64_t var_65;
    uint64_t rsi6_2;
    uint32_t state_0x8248_4;
    uint64_t state_0x9018_4;
    uint32_t state_0x9010_4;
    uint64_t state_0x82d8_4;
    uint32_t state_0x9080_4;
    uint32_t *var_66;
    uint64_t state_0x82d8_4168;
    uint32_t state_0x9010_4167;
    uint64_t state_0x9018_4166;
    uint32_t state_0x8248_4165;
    uint64_t rsi6_2164;
    uint32_t *_pre_phi190;
    uint64_t state_0x82d8_4153;
    uint32_t state_0x9010_4151;
    uint64_t state_0x9018_4149;
    uint32_t state_0x8248_4147;
    uint64_t rsi6_2145;
    uint32_t var_67;
    uint32_t *var_68;
    uint64_t rsi6_2144187;
    uint32_t state_0x8248_4146185;
    uint64_t state_0x9018_4148183;
    uint32_t state_0x9010_4150181;
    uint64_t state_0x82d8_4152179;
    uint32_t state_0x9080_4154177;
    uint32_t var_69;
    uint64_t var_70;
    uint32_t var_71;
    uint64_t var_72;
    uint32_t var_73;
    uint64_t var_74;
    uint32_t var_79;
    uint32_t var_75;
    uint64_t var_76;
    uint32_t var_77;
    uint64_t var_78;
    uint32_t var_80;
    uint64_t storemerge4;
    bool var_81;
    uint32_t var_82;
    uint32_t *var_83;
    uint32_t var_84;
    uint64_t var_85;
    uint64_t var_86;
    uint32_t var_88;
    struct helper_idivl_EAX_wrapper_ret_type var_87;
    uint64_t var_89;
    uint64_t local_sp_0;
    uint64_t local_sp_1;
    uint64_t var_91;
    uint64_t var_92;
    uint64_t var_93;
    uint64_t var_94;
    uint64_t var_95;
    uint32_t var_96;
    uint64_t var_97;
    uint64_t var_98;
    var_0 = revng_init_local_sp(0UL);
    var_1 = init_rbp();
    var_2 = init_cc_src2();
    var_3 = init_state_0x8248();
    var_4 = init_state_0x9018();
    var_5 = init_state_0x9010();
    var_6 = init_state_0x8408();
    var_7 = init_state_0x8328();
    var_8 = init_state_0x82d8();
    var_9 = init_state_0x9080();
    var_10 = var_0 + (-8L);
    *(uint64_t *)var_10 = var_1;
    var_11 = var_0 + (-40L);
    var_12 = (uint32_t *)(var_0 + (-28L));
    *var_12 = (uint32_t)rdi;
    var_13 = (uint32_t *)(var_0 + (-12L));
    *var_13 = 0U;
    var_14 = *(uint32_t *)6430244UL + (-10);
    var_15 = (uint64_t)var_14;
    *(uint32_t *)6430732UL = var_14;
    var_16 = helper_cc_compute_all_wrapper(var_15, 0UL, 0UL, 24U);
    state_0x9010_1 = var_5;
    state_0x82d8_1 = var_8;
    state_0x9080_1 = var_9;
    state_0x8248_1 = var_3;
    state_0x8248_0 = var_3;
    state_0x9018_0 = var_4;
    state_0x9010_0 = var_5;
    state_0x82d8_0 = var_8;
    state_0x9080_0 = var_9;
    state_0x9018_1 = var_4;
    rsi6_2 = rsi;
    rsi6_2145 = rsi;
    local_sp_0 = var_11;
    if ((uint64_t)(((unsigned char)(var_16 >> 4UL) ^ (unsigned char)var_16) & '\xc0') == 0UL) {
        *(unsigned char *)6430240UL = (unsigned char)'\x00';
        *(unsigned char *)6430725UL = (unsigned char)'\x01';
    }
    if (*(unsigned char *)6430240UL == '\x01') {
        *(uint32_t *)6430732UL = *(uint32_t *)6430244UL;
    }
    if (*(unsigned char *)6430818UL == '\x00') {
        var_17 = *(uint32_t *)6430732UL;
        *(uint32_t *)6430732UL = (uint32_t)(uint64_t)((long)((uint64_t)((uint32_t)((uint64_t)var_17 >> 31UL) + var_17) << 32UL) >> (long)33UL);
    }
    var_18 = *var_12;
    if (var_18 == 0U) {
        *(unsigned char *)6430720UL = (unsigned char)'\x00';
    } else {
        if (*(unsigned char *)6430720UL == '\x00') {
            *(uint32_t *)6430268UL = var_18;
        }
    }
    if (*(unsigned char *)6430241UL == '\x00') {
        *(unsigned char *)6430731UL = (unsigned char)'\x01';
    }
    if ((int)*(uint32_t *)6430268UL > (int)1U) {
        *(unsigned char *)6430241UL = (unsigned char)'\x00';
    } else {
        var_19 = (*(unsigned char *)6430825UL == '\x01');
        var_20 = *(unsigned char *)6430737UL;
        if (var_19) {
            *(uint64_t *)6430304UL = *(uint64_t *)(var_20 == '\x00') ? 6430312UL : 6430320UL;
            *(uint32_t *)6430828UL = 1U;
            *(unsigned char *)6430825UL = (unsigned char)'\x01';
        } else {
            if (var_20 != '\x01' & *(uint32_t *)6430828UL != 1U & **(unsigned char **)6430304UL == '\t') {
                *(uint64_t *)6430304UL = *(uint64_t *)6430312UL;
            }
        }
        *(unsigned char *)6430736UL = (unsigned char)'\x01';
        *(unsigned char *)6430745UL = (unsigned char)'\x01';
    }
    if (*(unsigned char *)6430737UL == '\x00') {
        *(unsigned char *)6430736UL = (unsigned char)'\x00';
    }
    if (*(unsigned char *)6430796UL != '\x00') {
        var_21 = (uint32_t *)(var_0 + (-16L));
        *var_21 = 8U;
        *(uint32_t *)6430284UL = *(uint32_t *)6430292UL;
        var_22 = (*(unsigned char *)6430280UL == '\t');
        var_23 = (uint64_t)*(uint32_t *)6430296UL;
        if (var_22) {
            var_24 = helper_idivl_EAX_wrapper((struct type_5 *)(0UL), (uint64_t)*var_21, 4206421UL, r10, r9, r8, var_23, var_10, (uint64_t)(uint32_t)(uint64_t)((long)(var_23 << 32UL) >> (long)63UL), rcx, rbx, rsi, rdi, var_3, var_4, var_5, var_6, var_7, var_8, var_9);
            state_0x8248_0 = var_24.field_6;
            state_0x9018_0 = var_24.field_7;
            state_0x9010_0 = var_24.field_8;
            state_0x82d8_0 = var_24.field_9;
            state_0x9080_0 = var_24.field_10;
            storemerge_in = (uint64_t)(*var_21 - (uint32_t)var_24.field_4) + (uint64_t)*(uint32_t *)6430296UL;
        } else {
            storemerge_in = var_23 + 1UL;
        }
        storemerge = (uint32_t)storemerge_in;
        *(uint32_t *)6430800UL = storemerge;
        state_0x9010_1 = state_0x9010_0;
        state_0x82d8_1 = state_0x82d8_0;
        state_0x9080_1 = state_0x9080_0;
        state_0x8248_1 = state_0x8248_0;
        state_0x9018_1 = state_0x9018_0;
        if (*(unsigned char *)6430720UL == '\x00') {
            *var_13 = storemerge;
        }
    }
    var_25 = *(uint32_t *)6430828UL;
    state_0x9080_4155 = state_0x9080_1;
    state_0x82d8_4153 = state_0x82d8_1;
    state_0x9010_4151 = state_0x9010_1;
    state_0x9018_4149 = state_0x9018_1;
    state_0x8248_4147 = state_0x8248_1;
    if ((int)var_25 > (int)4294967295U) {
        if (var_25 == 0U) {
            var_67 = (*(uint32_t *)6430268UL + (-1)) * *(uint32_t *)6430828UL;
            var_68 = (uint32_t *)(var_0 + (-20L));
            *var_68 = var_67;
            _pre_phi190 = var_68;
            rsi6_2144187 = rsi6_2145;
            state_0x8248_4146185 = state_0x8248_4147;
            state_0x9018_4148183 = state_0x9018_4149;
            state_0x9010_4150181 = state_0x9010_4151;
            state_0x82d8_4152179 = state_0x82d8_4153;
            state_0x9080_4154177 = state_0x9080_4155;
        } else {
            var_45 = *(uint32_t *)6430268UL + (-1);
            if ((int)var_45 > (int)4294967295U) {
                var_56 = (uint64_t)var_25;
                var_57 = helper_idivl_EAX_wrapper((struct type_5 *)(0UL), var_56, 4206660UL, r10, r9, r8, 2147483647UL, var_10, 0UL, var_56, rbx, rsi, rdi, state_0x8248_1, state_0x9018_1, state_0x9010_1, var_6, var_7, state_0x82d8_1, state_0x9080_1);
                var_58 = var_57.field_1;
                var_59 = var_57.field_6;
                var_60 = var_57.field_7;
                var_61 = var_57.field_8;
                var_62 = var_57.field_9;
                var_63 = var_57.field_10;
                var_64 = *(uint32_t *)6430268UL + (-1);
                var_65 = (uint64_t)(var_64 & (-256)) | ((long)(var_58 << 32UL) < (long)((uint64_t)var_64 << 32UL));
                rax_0 = var_65;
                state_0x8248_4 = var_59;
                state_0x9018_4 = var_60;
                state_0x9010_4 = var_61;
                state_0x82d8_4 = var_62;
                state_0x9080_4 = var_63;
            } else {
                var_46 = (uint64_t)var_45;
                var_47 = (uint64_t)var_25;
                var_48 = helper_idivl_EAX_wrapper((struct type_5 *)(0UL), var_47, 4206639UL, r10, r9, r8, 2147483648UL, var_10, 4294967295UL, var_46, rbx, var_47, rdi, state_0x8248_1, state_0x9018_1, state_0x9010_1, var_6, var_7, state_0x82d8_1, state_0x9080_1);
                var_49 = var_48.field_1;
                var_50 = var_48.field_6;
                var_51 = var_48.field_7;
                var_52 = var_48.field_8;
                var_53 = var_48.field_9;
                var_54 = var_48.field_10;
                var_55 = (var_49 & (-256L)) | ((long)(var_46 << 32UL) < (long)(var_49 << 32UL));
                rax_0 = var_55;
                rsi6_2 = var_47;
                state_0x8248_4 = var_50;
                state_0x9018_4 = var_51;
                state_0x9010_4 = var_52;
                state_0x82d8_4 = var_53;
                state_0x9080_4 = var_54;
            }
            state_0x9080_4155 = state_0x9080_4;
            state_0x9080_4169 = state_0x9080_4;
            state_0x82d8_4168 = state_0x82d8_4;
            state_0x9010_4167 = state_0x9010_4;
            state_0x9018_4166 = state_0x9018_4;
            state_0x8248_4165 = state_0x8248_4;
            rsi6_2164 = rsi6_2;
            state_0x82d8_4153 = state_0x82d8_4;
            state_0x9010_4151 = state_0x9010_4;
            state_0x9018_4149 = state_0x9018_4;
            state_0x8248_4147 = state_0x8248_4;
            rsi6_2145 = rsi6_2;
            if ((uint64_t)(unsigned char)rax_0 == 0UL) {
                var_67 = (*(uint32_t *)6430268UL + (-1)) * *(uint32_t *)6430828UL;
                var_68 = (uint32_t *)(var_0 + (-20L));
                *var_68 = var_67;
                _pre_phi190 = var_68;
                rsi6_2144187 = rsi6_2145;
                state_0x8248_4146185 = state_0x8248_4147;
                state_0x9018_4148183 = state_0x9018_4149;
                state_0x9010_4150181 = state_0x9010_4151;
                state_0x82d8_4152179 = state_0x82d8_4153;
                state_0x9080_4154177 = state_0x9080_4155;
            } else {
                var_66 = (uint32_t *)(var_0 + (-20L));
                *var_66 = 2147483647U;
                _pre_phi190 = var_66;
                rsi6_2144187 = rsi6_2164;
                state_0x8248_4146185 = state_0x8248_4165;
                state_0x9018_4148183 = state_0x9018_4166;
                state_0x9010_4150181 = state_0x9010_4167;
                state_0x82d8_4152179 = state_0x82d8_4168;
                state_0x9080_4154177 = state_0x9080_4169;
            }
        }
    } else {
        var_26 = *(uint32_t *)6430268UL + (-1);
        if ((int)var_26 > (int)4294967295U) {
            if (var_25 == 4294967295U) {
                var_67 = (*(uint32_t *)6430268UL + (-1)) * *(uint32_t *)6430828UL;
                var_68 = (uint32_t *)(var_0 + (-20L));
                *var_68 = var_67;
                _pre_phi190 = var_68;
                rsi6_2144187 = rsi6_2145;
                state_0x8248_4146185 = state_0x8248_4147;
                state_0x9018_4148183 = state_0x9018_4149;
                state_0x9010_4150181 = state_0x9010_4151;
                state_0x82d8_4152179 = state_0x82d8_4153;
                state_0x9080_4154177 = state_0x9080_4155;
            } else {
                var_37 = (uint64_t)var_25;
                var_38 = helper_idivl_EAX_wrapper((struct type_5 *)(0UL), var_37, 4206561UL, r10, r9, r8, 2147483648UL, var_10, 4294967295UL, rcx, rbx, var_37, rdi, state_0x8248_1, state_0x9018_1, state_0x9010_1, var_6, var_7, state_0x82d8_1, state_0x9080_1);
                var_39 = var_38.field_1;
                var_40 = var_38.field_6;
                var_41 = var_38.field_7;
                var_42 = var_38.field_8;
                var_43 = var_38.field_9;
                var_44 = var_38.field_10;
                state_0x9080_4155 = var_44;
                state_0x9080_4169 = var_44;
                state_0x82d8_4168 = var_43;
                state_0x9010_4167 = var_42;
                state_0x9018_4166 = var_41;
                state_0x8248_4165 = var_40;
                rsi6_2164 = var_37;
                state_0x82d8_4153 = var_43;
                state_0x9010_4151 = var_42;
                state_0x9018_4149 = var_41;
                state_0x8248_4147 = var_40;
                rsi6_2145 = var_37;
                if ((long)(var_39 << 32UL) < (long)(((uint64_t)*(uint32_t *)6430268UL << 32UL) + (-4294967296L))) {
                    var_66 = (uint32_t *)(var_0 + (-20L));
                    *var_66 = 2147483647U;
                    _pre_phi190 = var_66;
                    rsi6_2144187 = rsi6_2164;
                    state_0x8248_4146185 = state_0x8248_4165;
                    state_0x9018_4148183 = state_0x9018_4166;
                    state_0x9010_4150181 = state_0x9010_4167;
                    state_0x82d8_4152179 = state_0x82d8_4168;
                    state_0x9080_4154177 = state_0x9080_4169;
                } else {
                    var_67 = (*(uint32_t *)6430268UL + (-1)) * *(uint32_t *)6430828UL;
                    var_68 = (uint32_t *)(var_0 + (-20L));
                    *var_68 = var_67;
                    _pre_phi190 = var_68;
                    rsi6_2144187 = rsi6_2145;
                    state_0x8248_4146185 = state_0x8248_4147;
                    state_0x9018_4148183 = state_0x9018_4149;
                    state_0x9010_4150181 = state_0x9010_4151;
                    state_0x82d8_4152179 = state_0x82d8_4153;
                    state_0x9080_4154177 = state_0x9080_4155;
                }
            }
        } else {
            var_27 = (uint64_t)var_26;
            var_28 = (uint64_t)var_25;
            var_29 = helper_idivl_EAX_wrapper((struct type_5 *)(0UL), var_28, 4206526UL, r10, r9, r8, 2147483647UL, var_10, 0UL, var_27, rbx, var_28, rdi, state_0x8248_1, state_0x9018_1, state_0x9010_1, var_6, var_7, state_0x82d8_1, state_0x9080_1);
            var_30 = var_29.field_1;
            var_31 = var_29.field_6;
            var_32 = var_29.field_7;
            var_33 = var_29.field_8;
            var_34 = var_29.field_9;
            var_35 = var_29.field_10;
            var_36 = (var_30 & (-256L)) | ((long)(var_27 << 32UL) < (long)(var_30 << 32UL));
            rax_0 = var_36;
            rsi6_2 = var_28;
            state_0x8248_4 = var_31;
            state_0x9018_4 = var_32;
            state_0x9010_4 = var_33;
            state_0x82d8_4 = var_34;
            state_0x9080_4 = var_35;
            state_0x9080_4155 = state_0x9080_4;
            state_0x9080_4169 = state_0x9080_4;
            state_0x82d8_4168 = state_0x82d8_4;
            state_0x9010_4167 = state_0x9010_4;
            state_0x9018_4166 = state_0x9018_4;
            state_0x8248_4165 = state_0x8248_4;
            rsi6_2164 = rsi6_2;
            state_0x82d8_4153 = state_0x82d8_4;
            state_0x9010_4151 = state_0x9010_4;
            state_0x9018_4149 = state_0x9018_4;
            state_0x8248_4147 = state_0x8248_4;
            rsi6_2145 = rsi6_2;
            if ((uint64_t)(unsigned char)rax_0 == 0UL) {
                var_67 = (*(uint32_t *)6430268UL + (-1)) * *(uint32_t *)6430828UL;
                var_68 = (uint32_t *)(var_0 + (-20L));
                *var_68 = var_67;
                _pre_phi190 = var_68;
                rsi6_2144187 = rsi6_2145;
                state_0x8248_4146185 = state_0x8248_4147;
                state_0x9018_4148183 = state_0x9018_4149;
                state_0x9010_4150181 = state_0x9010_4151;
                state_0x82d8_4152179 = state_0x82d8_4153;
                state_0x9080_4154177 = state_0x9080_4155;
            } else {
                var_66 = (uint32_t *)(var_0 + (-20L));
                *var_66 = 2147483647U;
                _pre_phi190 = var_66;
                rsi6_2144187 = rsi6_2164;
                state_0x8248_4146185 = state_0x8248_4165;
                state_0x9018_4148183 = state_0x9018_4166;
                state_0x9010_4150181 = state_0x9010_4167;
                state_0x82d8_4152179 = state_0x82d8_4168;
                state_0x9080_4154177 = state_0x9080_4169;
            }
        }
    }
    var_69 = *_pre_phi190;
    if ((int)var_69 > (int)4294967295U) {
        var_75 = *(uint32_t *)6430248UL;
        var_76 = (uint64_t)var_75;
        var_77 = *var_13;
        var_78 = var_76 - (uint64_t)var_77;
        var_79 = var_77;
        var_80 = var_75;
        storemerge4 = (uint64_t)((uint32_t)var_78 & (-256)) | ((long)(var_78 << 32UL) < (long)(((uint64_t)var_69 << 32UL) ^ (-9223372036854775808L)));
    } else {
        var_70 = (uint64_t)(var_69 + 2147483647U);
        var_71 = *(uint32_t *)6430248UL;
        var_72 = (uint64_t)var_71;
        var_73 = *var_13;
        var_74 = var_72 - (uint64_t)var_73;
        var_79 = var_73;
        var_80 = var_71;
        storemerge4 = (uint64_t)((uint32_t)var_74 & (-256)) | ((long)(var_70 << 32UL) < (long)(var_74 << 32UL));
    }
    var_81 = ((uint64_t)(unsigned char)storemerge4 == 0UL);
    var_82 = (var_80 - var_79) - var_69;
    var_83 = (uint32_t *)(var_0 + (-24L));
    var_84 = var_82;
    if (var_81) {
        *var_83 = var_82;
    } else {
        *var_83 = 0U;
        var_84 = 0U;
    }
    var_85 = (uint64_t)var_84;
    var_86 = (uint64_t)*(uint32_t *)6430268UL;
    var_87 = helper_idivl_EAX_wrapper((struct type_5 *)(0UL), var_86, 4206892UL, r10, r9, r8, var_85, var_10, (uint64_t)(uint32_t)(uint64_t)((long)(var_85 << 32UL) >> (long)63UL), var_86, rbx, rsi6_2144187, rdi, state_0x8248_4146185, state_0x9018_4148183, state_0x9010_4150181, var_6, var_7, state_0x82d8_4152179, state_0x9080_4154177);
    var_88 = (uint32_t)var_87.field_1;
    *(uint32_t *)6430740UL = var_88;
    var_89 = helper_cc_compute_all_wrapper((uint64_t)var_88, 0UL, 0UL, 24U);
    if ((uint64_t)(((unsigned char)(var_89 >> 4UL) ^ (unsigned char)var_89) & '\xc0') == 0UL) {
        var_90 = var_0 + (-48L);
        *(uint64_t *)var_90 = 4206935UL;
        indirect_placeholder_34(r9, r8, 0UL, 4307645UL, var_86, 0UL, 1UL);
        local_sp_0 = var_90;
    }
    local_sp_1 = local_sp_0;
    if (*(unsigned char *)6430796UL == '\x00') {
        *(uint64_t *)(local_sp_1 + (-8L)) = 4207014UL;
        indirect_placeholder();
        var_96 = *(uint32_t *)6430256UL;
        var_97 = (uint64_t)(((int)var_96 > (int)8U) ? var_96 : 8U);
        *(uint64_t *)(local_sp_1 + (-16L)) = 4207041UL;
        var_98 = indirect_placeholder_2(var_97);
        *(uint64_t *)6430896UL = var_98;
        return;
    }
    *(uint64_t *)(local_sp_0 + (-8L)) = 4206961UL;
    indirect_placeholder();
    var_91 = (uint64_t)*(uint32_t *)6430296UL;
    var_92 = helper_cc_compute_c_wrapper(var_91 + (-11L), 11UL, var_2, 16U);
    var_93 = (var_92 == 0UL) ? (var_91 + 1UL) : 12UL;
    var_94 = local_sp_0 + (-16L);
    *(uint64_t *)var_94 = 4206992UL;
    var_95 = indirect_placeholder_2(var_93);
    *(uint64_t *)6430808UL = var_95;
    local_sp_1 = var_94;
    *(uint64_t *)(local_sp_1 + (-8L)) = 4207014UL;
    indirect_placeholder();
    var_96 = *(uint32_t *)6430256UL;
    var_97 = (uint64_t)(((int)var_96 > (int)8U) ? var_96 : 8U);
    *(uint64_t *)(local_sp_1 + (-16L)) = 4207041UL;
    var_98 = indirect_placeholder_2(var_97);
    *(uint64_t *)6430896UL = var_98;
    return;
}

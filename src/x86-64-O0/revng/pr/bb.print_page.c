typedef unsigned char __u_char;
typedef unsigned short __u_short;
typedef unsigned int __u_int;
typedef unsigned long __u_long;
typedef signed char __int8_t;
typedef unsigned char __uint8_t;
typedef short __int16_t;
typedef unsigned short __uint16_t;
typedef int __int32_t;
typedef unsigned int __uint32_t;
typedef long __int64_t;
typedef unsigned long __uint64_t;
typedef __int8_t __int_least8_t;
typedef __uint8_t __uint_least8_t;
typedef __int16_t __int_least16_t;
typedef __uint16_t __uint_least16_t;
typedef __int32_t __int_least32_t;
typedef __uint32_t __uint_least32_t;
typedef __int64_t __int_least64_t;
typedef __uint64_t __uint_least64_t;
typedef long __quad_t;
typedef unsigned long __u_quad_t;
typedef long __intmax_t;
typedef unsigned long __uintmax_t;
typedef unsigned long __dev_t;
typedef unsigned int __uid_t;
typedef unsigned int __gid_t;
typedef unsigned long __ino_t;
typedef unsigned long __ino64_t;
typedef unsigned int __mode_t;
typedef unsigned long __nlink_t;
typedef long __off_t;
typedef long __off64_t;
typedef int __pid_t;
typedef struct {
    int __val[2];
} __fsid_t;
typedef long __clock_t;
typedef unsigned long __rlim_t;
typedef unsigned long __rlim64_t;
typedef unsigned int __id_t;
typedef long __time_t;
typedef unsigned int __useconds_t;
typedef long __suseconds_t;
typedef int __daddr_t;
typedef int __key_t;
typedef int __clockid_t;
typedef void *__timer_t;
typedef long __blksize_t;
typedef long __blkcnt_t;
typedef long __blkcnt64_t;
typedef unsigned long __fsblkcnt_t;
typedef unsigned long __fsblkcnt64_t;
typedef unsigned long __fsfilcnt_t;
typedef unsigned long __fsfilcnt64_t;
typedef long __fsword_t;
typedef long __ssize_t;
typedef long __syscall_slong_t;
typedef unsigned long __syscall_ulong_t;
typedef __off64_t __loff_t;
typedef char *__caddr_t;
typedef long __intptr_t;
typedef unsigned int __socklen_t;
typedef int __sig_atomic_t;
typedef __int8_t int8_t;
typedef __int16_t int16_t;
typedef __int32_t int32_t;
typedef __int64_t int64_t;
typedef __uint8_t uint8_t;
typedef __uint16_t uint16_t;
typedef __uint32_t uint32_t;
typedef __uint64_t uint64_t;
typedef __int_least8_t int_least8_t;
typedef __int_least16_t int_least16_t;
typedef __int_least32_t int_least32_t;
typedef __int_least64_t int_least64_t;
typedef __uint_least8_t uint_least8_t;
typedef __uint_least16_t uint_least16_t;
typedef __uint_least32_t uint_least32_t;
typedef __uint_least64_t uint_least64_t;
typedef signed char int_fast8_t;
typedef long int_fast16_t;
typedef long int_fast32_t;
typedef long int_fast64_t;
typedef unsigned char uint_fast8_t;
typedef unsigned long uint_fast16_t;
typedef unsigned long uint_fast32_t;
typedef unsigned long uint_fast64_t;
typedef long intptr_t;
typedef unsigned long uintptr_t;
typedef __intmax_t intmax_t;
typedef __uintmax_t uintmax_t;
struct indirect_placeholder_15_ret_type;
struct indirect_placeholder_17_ret_type;
struct indirect_placeholder_18_ret_type;
struct indirect_placeholder_15_ret_type {
    uint64_t field_0;
    uint64_t field_1;
    uint64_t field_2;
    uint64_t field_3;
};
struct indirect_placeholder_17_ret_type {
    uint64_t field_0;
    uint64_t field_1;
    uint64_t field_2;
    uint64_t field_3;
    uint64_t field_4;
};
struct indirect_placeholder_18_ret_type {
    uint64_t field_0;
    uint64_t field_1;
    uint64_t field_2;
    uint64_t field_3;
    uint64_t field_4;
};
extern uint64_t revng_init_local_sp(uint64_t param_0);
extern uint64_t init_rbp(void);
extern void indirect_placeholder(void);
extern uint64_t indirect_placeholder_16(void);
extern void indirect_placeholder_4(uint64_t param_0);
extern uint64_t helper_cc_compute_all_wrapper(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint32_t param_3);
extern void indirect_placeholder_14(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint64_t param_3, uint64_t param_4);
extern struct indirect_placeholder_15_ret_type indirect_placeholder_15(uint64_t param_0);
extern struct indirect_placeholder_17_ret_type indirect_placeholder_17(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint64_t param_3, uint64_t param_4);
extern struct indirect_placeholder_18_ret_type indirect_placeholder_18(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint64_t param_3, uint64_t param_4);
uint64_t bb_print_page(uint64_t r10, uint64_t r9, uint64_t r8, uint64_t rcx, uint64_t rbx) {
    struct indirect_placeholder_18_ret_type var_17;
    struct indirect_placeholder_17_ret_type var_34;
    uint64_t var_0;
    uint64_t var_1;
    uint64_t var_2;
    uint64_t var_3;
    uint64_t rax_0;
    uint64_t r101_3;
    uint64_t r83_0;
    uint64_t local_sp_7;
    uint64_t local_sp_6_be;
    uint64_t var_38;
    uint32_t var_11;
    uint64_t r92_4;
    uint64_t var_24;
    uint64_t var_25;
    struct indirect_placeholder_15_ret_type var_26;
    uint64_t local_sp_0;
    uint64_t r83_4;
    uint64_t r101_0;
    uint64_t rbx5_2;
    uint64_t r92_0;
    uint64_t local_sp_4;
    uint32_t *var_27;
    uint64_t var_28;
    uint64_t local_sp_1;
    uint64_t var_29;
    uint64_t var_30;
    uint64_t local_sp_3;
    uint64_t local_sp_5;
    uint64_t var_31;
    uint32_t var_32;
    uint64_t var_33;
    uint64_t local_sp_2;
    uint64_t r92_3;
    uint64_t r101_1;
    uint64_t r83_3;
    uint64_t r92_1;
    uint64_t rbx5_1;
    uint64_t r83_1;
    uint64_t rbx5_0;
    uint64_t r101_2;
    uint64_t r92_2;
    uint64_t r83_2;
    uint64_t var_35;
    uint64_t var_36;
    uint64_t var_37;
    uint64_t var_14;
    uint64_t var_15;
    uint64_t var_16;
    uint64_t var_18;
    uint64_t var_19;
    uint64_t var_20;
    uint64_t var_21;
    uint64_t var_22;
    uint64_t var_23;
    uint32_t var_8;
    unsigned char *var_4;
    uint32_t var_5;
    uint32_t *var_6;
    uint32_t var_7;
    uint32_t *var_9;
    uint64_t *var_10;
    uint64_t local_sp_6;
    uint64_t r101_4;
    uint64_t var_12;
    uint64_t var_13;
    uint64_t var_39;
    uint32_t var_43;
    uint64_t var_40;
    uint32_t var_41;
    uint32_t var_42;
    uint64_t var_44;
    unsigned char var_45;
    uint64_t var_46;
    uint64_t var_47;
    uint64_t local_sp_8;
    uint64_t var_48;
    uint64_t var_49;
    var_0 = revng_init_local_sp(0UL);
    var_1 = init_rbp();
    *(uint64_t *)(var_0 + (-8L)) = var_1;
    *(uint64_t *)(var_0 + (-48L)) = 4210021UL;
    indirect_placeholder_14(r10, r9, r8, rcx, rbx);
    var_2 = var_0 + (-56L);
    *(uint64_t *)var_2 = 4210026UL;
    var_3 = indirect_placeholder_16();
    rax_0 = 0UL;
    r92_4 = r9;
    r83_4 = r8;
    rbx5_2 = rbx;
    local_sp_6 = var_2;
    r101_4 = r10;
    if ((uint64_t)(uint32_t)var_3 == 0UL) {
        return rax_0;
    }
    if (*(unsigned char *)6430240UL == '\x00') {
        *(unsigned char *)6430727UL = (unsigned char)'\x01';
    }
    *(unsigned char *)6430840UL = (unsigned char)'\x00';
    var_4 = (unsigned char *)(var_0 + (-25L));
    *var_4 = (unsigned char)'\x00';
    var_5 = *(uint32_t *)6430732UL;
    var_6 = (uint32_t *)(var_0 + (-16L));
    *var_6 = var_5;
    var_8 = var_5;
    if (*(unsigned char *)6430818UL == '\x00') {
        var_7 = var_5 << 1U;
        *var_6 = var_7;
        var_8 = var_7;
    }
    var_9 = (uint32_t *)(var_0 + (-12L));
    var_10 = (uint64_t *)(var_0 + (-24L));
    var_11 = var_8;
    local_sp_7 = local_sp_6;
    r101_1 = r101_4;
    r92_1 = r92_4;
    r83_1 = r83_4;
    rbx5_0 = rbx5_2;
    while ((int)var_11 <= (int)0U)
        {
            var_12 = local_sp_6 + (-8L);
            *(uint64_t *)var_12 = 4210608UL;
            var_13 = indirect_placeholder_16();
            local_sp_2 = var_12;
            local_sp_7 = var_12;
            if ((uint64_t)(uint32_t)var_13 == 0UL) {
                break;
            }
            *(uint32_t *)6430756UL = 0U;
            *(uint32_t *)6430748UL = 0U;
            *(uint32_t *)6430832UL = 0U;
            *(unsigned char *)6430840UL = (unsigned char)'\x00';
            *(unsigned char *)6430721UL = (unsigned char)'\x00';
            *(unsigned char *)6430722UL = (unsigned char)'\x01';
            *var_9 = 1U;
            *var_10 = *(uint64_t *)6430672UL;
            r101_3 = r101_1;
            r83_0 = r83_1;
            r101_0 = r101_1;
            rbx5_2 = rbx5_0;
            r92_0 = r92_1;
            local_sp_3 = local_sp_2;
            local_sp_5 = local_sp_2;
            r92_3 = r92_1;
            r83_3 = r83_1;
            rbx5_1 = rbx5_0;
            r101_2 = r101_1;
            r92_2 = r92_1;
            r83_2 = r83_1;
            while ((int)*(uint32_t *)6430268UL >= (int)*var_9)
                {
                    *(uint32_t *)6430760UL = 0U;
                    var_14 = helper_cc_compute_all_wrapper((uint64_t)*(uint32_t *)(*var_10 + 48UL), 0UL, 0UL, 24U);
                    if ((uint64_t)(((unsigned char)(var_14 >> 4UL) ^ (unsigned char)var_14) & '\xc0') != 0UL) {
                        var_15 = *var_10;
                        if (*(uint32_t *)(var_15 + 16UL) != 1U) {
                            if (*(unsigned char *)6430720UL != '\x00') {
                                if (*(unsigned char *)6430722UL == '\x00') {
                                    var_16 = local_sp_2 + (-8L);
                                    *(uint64_t *)var_16 = 4210466UL;
                                    var_17 = indirect_placeholder_18(r101_1, r92_1, r83_1, rbx5_0, var_15);
                                    var_18 = var_17.field_0;
                                    var_19 = var_17.field_1;
                                    var_20 = var_17.field_2;
                                    var_21 = var_17.field_3;
                                    r101_3 = var_18;
                                    local_sp_5 = var_16;
                                    r92_3 = var_19;
                                    r83_3 = var_20;
                                    rbx5_1 = var_21;
                                } else {
                                    *(unsigned char *)6430721UL = (unsigned char)'\x01';
                                }
                            }
                            local_sp_2 = local_sp_5;
                            r101_1 = r101_3;
                            r92_1 = r92_3;
                            r83_1 = r83_3;
                            rbx5_0 = rbx5_1;
                            if (*(unsigned char *)6430825UL == '\x00') {
                                *(uint32_t *)6430832UL = (*(uint32_t *)6430832UL + 1U);
                            }
                            *var_9 = (*var_9 + 1U);
                            *var_10 = (*var_10 + 64UL);
                            r101_3 = r101_1;
                            r83_0 = r83_1;
                            r101_0 = r101_1;
                            rbx5_2 = rbx5_0;
                            r92_0 = r92_1;
                            local_sp_3 = local_sp_2;
                            local_sp_5 = local_sp_2;
                            r92_3 = r92_1;
                            r83_3 = r83_1;
                            rbx5_1 = rbx5_0;
                            r101_2 = r101_1;
                            r92_2 = r92_1;
                            r83_2 = r83_1;
                            continue;
                        }
                    }
                    *(unsigned char *)6430723UL = (unsigned char)'\x00';
                    *(uint32_t *)6430836UL = *(uint32_t *)(*var_10 + 52UL);
                    var_22 = *(uint64_t *)(*var_10 + 24UL);
                    var_23 = local_sp_2 + (-8L);
                    *(uint64_t *)var_23 = 4210245UL;
                    indirect_placeholder();
                    local_sp_0 = var_23;
                    if ((uint64_t)(unsigned char)var_22 == 1UL) {
                        var_24 = *var_10;
                        var_25 = local_sp_2 + (-16L);
                        *(uint64_t *)var_25 = 4210264UL;
                        var_26 = indirect_placeholder_15(var_24);
                        local_sp_0 = var_25;
                        r101_0 = var_26.field_0;
                        r92_0 = var_26.field_1;
                        r83_0 = var_26.field_2;
                    }
                    *var_4 = ((*(unsigned char *)6430840UL | *var_4) != '\x00');
                    var_27 = (uint32_t *)(*var_10 + 48UL);
                    *var_27 = (*var_27 + (-1));
                    var_28 = helper_cc_compute_all_wrapper((uint64_t)*(uint32_t *)(*var_10 + 48UL), 0UL, 0UL, 24U);
                    r101_3 = r101_0;
                    local_sp_1 = local_sp_0;
                    r92_3 = r92_0;
                    r83_3 = r83_0;
                    r101_2 = r101_0;
                    r92_2 = r92_0;
                    r83_2 = r83_0;
                    if ((uint64_t)(((unsigned char)(var_28 >> 4UL) ^ (unsigned char)var_28) & '\xc0') != 0UL) {
                        var_29 = local_sp_0 + (-8L);
                        *(uint64_t *)var_29 = 4210321UL;
                        var_30 = indirect_placeholder_16();
                        local_sp_1 = var_29;
                        local_sp_3 = var_29;
                        if ((uint64_t)(uint32_t)var_30 == 0UL) {
                            break;
                        }
                    }
                    local_sp_5 = local_sp_1;
                    if (*(unsigned char *)6430720UL == '\x00') {
                        local_sp_2 = local_sp_5;
                        r101_1 = r101_3;
                        r92_1 = r92_3;
                        r83_1 = r83_3;
                        rbx5_0 = rbx5_1;
                        if (*(unsigned char *)6430825UL == '\x00') {
                            *(uint32_t *)6430832UL = (*(uint32_t *)6430832UL + 1U);
                        }
                        *var_9 = (*var_9 + 1U);
                        *var_10 = (*var_10 + 64UL);
                        r101_3 = r101_1;
                        r83_0 = r83_1;
                        r101_0 = r101_1;
                        rbx5_2 = rbx5_0;
                        r92_0 = r92_1;
                        local_sp_3 = local_sp_2;
                        local_sp_5 = local_sp_2;
                        r92_3 = r92_1;
                        r83_3 = r83_1;
                        rbx5_1 = rbx5_0;
                        r101_2 = r101_1;
                        r92_2 = r92_1;
                        r83_2 = r83_1;
                        continue;
                    }
                    var_31 = *var_10;
                    var_32 = *(uint32_t *)(var_31 + 16UL);
                    if (var_32 != 0U) {
                        if (*(unsigned char *)6430722UL == '\x00') {
                            switch (var_32) {
                              case 3U:
                                {
                                    var_33 = local_sp_1 + (-8L);
                                    *(uint64_t *)var_33 = 4210419UL;
                                    var_34 = indirect_placeholder_17(r101_0, r92_0, r83_0, rbx5_0, var_31);
                                    r101_3 = var_34.field_0;
                                    local_sp_5 = var_33;
                                    r92_3 = var_34.field_1;
                                    r83_3 = var_34.field_2;
                                    rbx5_1 = var_34.field_3;
                                }
                                break;
                              case 2U:
                                {
                                    if (*(unsigned char *)6430723UL == '\x00') {
                                        var_33 = local_sp_1 + (-8L);
                                        *(uint64_t *)var_33 = 4210419UL;
                                        var_34 = indirect_placeholder_17(r101_0, r92_0, r83_0, rbx5_0, var_31);
                                        r101_3 = var_34.field_0;
                                        local_sp_5 = var_33;
                                        r92_3 = var_34.field_1;
                                        r83_3 = var_34.field_2;
                                        rbx5_1 = var_34.field_3;
                                    }
                                }
                                break;
                              default:
                                {
                                    break;
                                }
                                break;
                            }
                        } else {
                            *(unsigned char *)6430721UL = (unsigned char)'\x01';
                        }
                    }
                }
            local_sp_4 = local_sp_3;
            r101_4 = r101_2;
            r92_4 = r92_2;
            r83_4 = r83_2;
            if (*(unsigned char *)6430840UL == '\x00') {
                var_35 = local_sp_3 + (-8L);
                *(uint64_t *)var_35 = 4210537UL;
                indirect_placeholder();
                *var_6 = (*var_6 + (-1));
                local_sp_4 = var_35;
            }
            var_36 = local_sp_4 + (-8L);
            *(uint64_t *)var_36 = 4210546UL;
            var_37 = indirect_placeholder_16();
            local_sp_6_be = var_36;
            local_sp_7 = var_36;
            if ((uint64_t)(uint32_t)var_37 != 0UL) {
                if (*(unsigned char *)6430240UL == '\x01') {
                    break;
                }
            }
            if (*(unsigned char *)6430818UL != '\x00' & *var_4 == '\x00') {
                var_38 = local_sp_4 + (-16L);
                *(uint64_t *)var_38 = 4210593UL;
                indirect_placeholder();
                *var_6 = (*var_6 + (-1));
                local_sp_6_be = var_38;
            }
            var_11 = *var_6;
            local_sp_6 = local_sp_6_be;
            local_sp_7 = local_sp_6;
            r101_1 = r101_4;
            r92_1 = r92_4;
            r83_1 = r83_4;
            rbx5_0 = rbx5_2;
        }
    local_sp_8 = local_sp_7;
    if (*var_6 != 0U) {
        *var_9 = 1U;
        var_39 = *(uint64_t *)6430672UL;
        *var_10 = var_39;
        var_40 = var_39;
        var_41 = *(uint32_t *)6430268UL;
        var_42 = *var_9;
        var_43 = var_42;
        while ((int)var_41 >= (int)var_42)
            {
                if (*(uint32_t *)(var_40 + 16UL) == 0U) {
                    *(unsigned char *)(var_40 + 57UL) = (unsigned char)'\x01';
                    var_43 = *var_9;
                }
                *var_9 = (var_43 + 1U);
                var_44 = *var_10 + 64UL;
                *var_10 = var_44;
                var_40 = var_44;
                var_41 = *(uint32_t *)6430268UL;
                var_42 = *var_9;
                var_43 = var_42;
            }
    }
    var_45 = *var_4;
    *(unsigned char *)6430840UL = var_45;
    if (var_45 == '\x00') {
        if (*(unsigned char *)6430240UL == '\x00') {
            var_46 = (uint64_t)(*var_6 + 5U);
            var_47 = local_sp_7 + (-8L);
            *(uint64_t *)var_47 = 4210726UL;
            indirect_placeholder_4(var_46);
            local_sp_8 = var_47;
        } else {
            if (*(unsigned char *)6430725UL != '\x00' & *(unsigned char *)6430726UL == '\x00') {
                var_48 = local_sp_7 + (-8L);
                *(uint64_t *)var_48 = 4210760UL;
                indirect_placeholder();
                *(unsigned char *)6430726UL = (unsigned char)'\x00';
                local_sp_8 = var_48;
            }
        }
    } else {
        if (*(unsigned char *)6430725UL != '\x00' & *(unsigned char *)6430726UL == '\x00') {
            var_48 = local_sp_7 + (-8L);
            *(uint64_t *)var_48 = 4210760UL;
            indirect_placeholder();
            *(unsigned char *)6430726UL = (unsigned char)'\x00';
            local_sp_8 = var_48;
        }
    }
    var_49 = *(uint64_t *)6430784UL + 1UL;
    *(uint64_t *)6430784UL = var_49;
    if (var_49 <= *(uint64_t *)6430272UL) {
        return rax_0;
    }
    *(uint64_t *)(local_sp_8 + (-8L)) = 4210816UL;
    indirect_placeholder();
    rax_0 = 1UL;
    return rax_0;
}

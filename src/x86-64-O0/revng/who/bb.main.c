typedef unsigned char __u_char;
typedef unsigned short __u_short;
typedef unsigned int __u_int;
typedef unsigned long __u_long;
typedef signed char __int8_t;
typedef unsigned char __uint8_t;
typedef short __int16_t;
typedef unsigned short __uint16_t;
typedef int __int32_t;
typedef unsigned int __uint32_t;
typedef long __int64_t;
typedef unsigned long __uint64_t;
typedef __int8_t __int_least8_t;
typedef __uint8_t __uint_least8_t;
typedef __int16_t __int_least16_t;
typedef __uint16_t __uint_least16_t;
typedef __int32_t __int_least32_t;
typedef __uint32_t __uint_least32_t;
typedef __int64_t __int_least64_t;
typedef __uint64_t __uint_least64_t;
typedef long __quad_t;
typedef unsigned long __u_quad_t;
typedef long __intmax_t;
typedef unsigned long __uintmax_t;
typedef unsigned long __dev_t;
typedef unsigned int __uid_t;
typedef unsigned int __gid_t;
typedef unsigned long __ino_t;
typedef unsigned long __ino64_t;
typedef unsigned int __mode_t;
typedef unsigned long __nlink_t;
typedef long __off_t;
typedef long __off64_t;
typedef int __pid_t;
typedef struct {
    int __val[2];
} __fsid_t;
typedef long __clock_t;
typedef unsigned long __rlim_t;
typedef unsigned long __rlim64_t;
typedef unsigned int __id_t;
typedef long __time_t;
typedef unsigned int __useconds_t;
typedef long __suseconds_t;
typedef int __daddr_t;
typedef int __key_t;
typedef int __clockid_t;
typedef void *__timer_t;
typedef long __blksize_t;
typedef long __blkcnt_t;
typedef long __blkcnt64_t;
typedef unsigned long __fsblkcnt_t;
typedef unsigned long __fsblkcnt64_t;
typedef unsigned long __fsfilcnt_t;
typedef unsigned long __fsfilcnt64_t;
typedef long __fsword_t;
typedef long __ssize_t;
typedef long __syscall_slong_t;
typedef unsigned long __syscall_ulong_t;
typedef __off64_t __loff_t;
typedef char *__caddr_t;
typedef long __intptr_t;
typedef unsigned int __socklen_t;
typedef int __sig_atomic_t;
typedef __int8_t int8_t;
typedef __int16_t int16_t;
typedef __int32_t int32_t;
typedef __int64_t int64_t;
typedef __uint8_t uint8_t;
typedef __uint16_t uint16_t;
typedef __uint32_t uint32_t;
typedef __uint64_t uint64_t;
typedef __int_least8_t int_least8_t;
typedef __int_least16_t int_least16_t;
typedef __int_least32_t int_least32_t;
typedef __int_least64_t int_least64_t;
typedef __uint_least8_t uint_least8_t;
typedef __uint_least16_t uint_least16_t;
typedef __uint_least32_t uint_least32_t;
typedef __uint_least64_t uint_least64_t;
typedef signed char int_fast8_t;
typedef long int_fast16_t;
typedef long int_fast32_t;
typedef long int_fast64_t;
typedef unsigned char uint_fast8_t;
typedef unsigned long uint_fast16_t;
typedef unsigned long uint_fast32_t;
typedef unsigned long uint_fast64_t;
typedef long intptr_t;
typedef unsigned long uintptr_t;
typedef __intmax_t intmax_t;
typedef __uintmax_t uintmax_t;
typedef _Bool bool;
struct indirect_placeholder_35_ret_type;
struct indirect_placeholder_36_ret_type;
struct indirect_placeholder_35_ret_type {
    uint64_t field_0;
    uint64_t field_1;
    uint64_t field_2;
};
struct indirect_placeholder_36_ret_type {
    uint64_t field_0;
    uint64_t field_1;
};
extern void abort(void);
extern uint64_t revng_init_local_sp(uint64_t param_0);
extern uint64_t init_rbp(void);
extern void indirect_placeholder(void);
extern void indirect_placeholder_34(uint64_t param_0, uint64_t param_1);
extern uint64_t indirect_placeholder_3(uint64_t param_0);
extern void indirect_placeholder_15(uint64_t param_0);
extern void indirect_placeholder_31(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint64_t param_3, uint64_t param_4, uint64_t param_5, uint64_t param_6);
extern struct indirect_placeholder_35_ret_type indirect_placeholder_35(uint64_t param_0);
extern void indirect_placeholder_2(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint64_t param_3, uint64_t param_4, uint64_t param_5);
extern struct indirect_placeholder_36_ret_type indirect_placeholder_36(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint64_t param_3, uint64_t param_4);
void bb_main(uint64_t rsi, uint64_t rdi) {
    struct indirect_placeholder_36_ret_type var_13;
    uint64_t var_0;
    uint64_t var_1;
    uint64_t var_2;
    uint32_t *var_3;
    uint64_t var_4;
    uint64_t *var_5;
    unsigned char *var_6;
    uint64_t var_7;
    uint64_t var_8;
    uint32_t *var_9;
    uint64_t local_sp_1;
    uint64_t var_10;
    uint64_t var_22;
    struct indirect_placeholder_35_ret_type var_23;
    uint64_t var_24;
    uint64_t var_25;
    uint64_t var_26;
    uint64_t var_27;
    uint64_t var_18;
    bool var_19;
    uint32_t storemerge;
    uint32_t var_20;
    uint32_t var_21;
    uint64_t local_sp_0;
    uint64_t var_15;
    uint64_t var_16;
    uint64_t *var_17;
    uint64_t var_11;
    uint64_t var_12;
    uint32_t var_14;
    unsigned int loop_state_var;
    var_0 = revng_init_local_sp(0UL);
    var_1 = init_rbp();
    var_2 = var_0 + (-8L);
    *(uint64_t *)var_2 = var_1;
    var_3 = (uint32_t *)(var_0 + (-28L));
    *var_3 = (uint32_t)rdi;
    var_4 = var_0 + (-40L);
    var_5 = (uint64_t *)var_4;
    *var_5 = rsi;
    var_6 = (unsigned char *)(var_0 + (-9L));
    *var_6 = (unsigned char)'\x01';
    var_7 = **(uint64_t **)var_4;
    *(uint64_t *)(var_0 + (-48L)) = 4206848UL;
    indirect_placeholder_15(var_7);
    *(uint64_t *)(var_0 + (-56L)) = 4206863UL;
    indirect_placeholder();
    var_8 = var_0 + (-64L);
    *(uint64_t *)var_8 = 4206873UL;
    indirect_placeholder();
    var_9 = (uint32_t *)(var_0 + (-16L));
    local_sp_1 = var_8;
    while (1U)
        {
            var_10 = *var_5;
            var_11 = (uint64_t)*var_3;
            var_12 = local_sp_1 + (-8L);
            *(uint64_t *)var_12 = 4207493UL;
            var_13 = indirect_placeholder_36(4275464UL, 4273280UL, var_10, var_11, 0UL);
            var_14 = (uint32_t)var_13.field_0;
            *var_9 = var_14;
            local_sp_0 = var_12;
            local_sp_1 = var_12;
            if (var_14 != 4294967295U) {
                if (*var_6 != '\x00') {
                    loop_state_var = 0U;
                    break;
                }
                *(unsigned char *)6384621UL = (unsigned char)'\x01';
                *(unsigned char *)6384610UL = (unsigned char)'\x01';
                loop_state_var = 0U;
                break;
            }
            if ((uint64_t)(var_14 + (-109)) == 0UL) {
                *(unsigned char *)6384622UL = (unsigned char)'\x01';
            } else {
                if ((int)var_14 > (int)109U) {
                    if ((uint64_t)(var_14 + (-115)) != 0UL) {
                        *(unsigned char *)6384610UL = (unsigned char)'\x01';
                        continue;
                    }
                    if ((int)var_14 <= (int)115U) {
                        if ((uint64_t)(var_14 + (-113)) == 0UL) {
                            *(unsigned char *)6384609UL = (unsigned char)'\x01';
                            continue;
                        }
                        if ((int)var_14 > (int)113U) {
                            *(unsigned char *)6384620UL = (unsigned char)'\x01';
                            *(unsigned char *)6384611UL = (unsigned char)'\x01';
                            *var_6 = (unsigned char)'\x00';
                            continue;
                        }
                        if ((uint64_t)(var_14 + (-112)) != 0UL) {
                            loop_state_var = 1U;
                            break;
                        }
                        *(unsigned char *)6384618UL = (unsigned char)'\x01';
                        *var_6 = (unsigned char)'\x00';
                        continue;
                    }
                    if ((uint64_t)(var_14 + (-117)) != 0UL) {
                        *(unsigned char *)6384621UL = (unsigned char)'\x01';
                        *(unsigned char *)6384611UL = (unsigned char)'\x01';
                        *var_6 = (unsigned char)'\x00';
                        continue;
                    }
                    if ((int)var_14 >= (int)117U) {
                        *(unsigned char *)6384619UL = (unsigned char)'\x01';
                        *var_6 = (unsigned char)'\x00';
                        continue;
                    }
                    if ((uint64_t)(var_14 + (-119)) != 0UL) {
                        if ((uint64_t)(var_14 + (-128)) != 0UL) {
                            loop_state_var = 1U;
                            break;
                        }
                        *(unsigned char *)6384608UL = (unsigned char)'\x01';
                        continue;
                    }
                }
                if ((uint64_t)(var_14 + (-84)) != 0UL) {
                    if ((int)var_14 <= (int)84U) {
                        if ((uint64_t)(var_14 + 130U) == 0UL) {
                            *(uint64_t *)(local_sp_1 + (-16L)) = 4207380UL;
                            indirect_placeholder_34(var_2, 0UL);
                            abort();
                        }
                        if ((uint64_t)(var_14 + (-72)) == 0UL) {
                            *(unsigned char *)6384612UL = (unsigned char)'\x01';
                            continue;
                        }
                        if (var_14 != 4294967165U) {
                            loop_state_var = 1U;
                            break;
                        }
                        var_15 = *(uint64_t *)6384272UL;
                        *(uint64_t *)(local_sp_1 + (-16L)) = 0UL;
                        var_16 = local_sp_1 + (-24L);
                        var_17 = (uint64_t *)var_16;
                        *var_17 = 4275450UL;
                        *(uint64_t *)(local_sp_1 + (-32L)) = 4207439UL;
                        indirect_placeholder_2(0UL, 4272920UL, var_15, 4275413UL, 4275417UL, 4275433UL);
                        *var_17 = 4207453UL;
                        indirect_placeholder();
                        local_sp_0 = var_16;
                        loop_state_var = 1U;
                        break;
                    }
                    if ((uint64_t)(var_14 + (-98)) == 0UL) {
                        *(unsigned char *)6384615UL = (unsigned char)'\x01';
                        *var_6 = (unsigned char)'\x00';
                        continue;
                    }
                    if ((int)var_14 > (int)98U) {
                        if ((uint64_t)(var_14 + (-100)) == 0UL) {
                            *(unsigned char *)6384616UL = (unsigned char)'\x01';
                            *(unsigned char *)6384611UL = (unsigned char)'\x01';
                            *(unsigned char *)6384614UL = (unsigned char)'\x01';
                            *var_6 = (unsigned char)'\x00';
                            continue;
                        }
                        if ((uint64_t)(var_14 + (-108)) != 0UL) {
                            loop_state_var = 1U;
                            break;
                        }
                        *(unsigned char *)6384617UL = (unsigned char)'\x01';
                        *(unsigned char *)6384611UL = (unsigned char)'\x01';
                        *var_6 = (unsigned char)'\x00';
                        continue;
                    }
                    if ((uint64_t)(var_14 + (-97)) != 0UL) {
                        loop_state_var = 1U;
                        break;
                    }
                    *(unsigned char *)6384615UL = (unsigned char)'\x01';
                    *(unsigned char *)6384616UL = (unsigned char)'\x01';
                    *(unsigned char *)6384617UL = (unsigned char)'\x01';
                    *(unsigned char *)6384618UL = (unsigned char)'\x01';
                    *(unsigned char *)6384620UL = (unsigned char)'\x01';
                    *(unsigned char *)6384619UL = (unsigned char)'\x01';
                    *(unsigned char *)6384621UL = (unsigned char)'\x01';
                    *(unsigned char *)6384613UL = (unsigned char)'\x01';
                    *(unsigned char *)6384611UL = (unsigned char)'\x01';
                    *(unsigned char *)6384614UL = (unsigned char)'\x01';
                    *var_6 = (unsigned char)'\x00';
                    continue;
                }
                *(unsigned char *)6384613UL = (unsigned char)'\x01';
            }
        }
    switch (loop_state_var) {
      case 1U:
        {
            *(uint64_t *)(local_sp_0 + (-8L)) = 4207463UL;
            indirect_placeholder_34(var_2, 1UL);
            abort();
        }
        break;
      case 0U:
        {
            if (*(unsigned char *)6384614UL == '\x00') {
                *(unsigned char *)6384610UL = (unsigned char)'\x00';
            }
            *(uint64_t *)(local_sp_1 + (-16L)) = 4207554UL;
            var_18 = indirect_placeholder_3(2UL);
            var_19 = ((uint64_t)(unsigned char)var_18 == 0UL);
            storemerge = var_19 ? 12U : 16U;
            *(uint64_t *)6384624UL = (var_19 ? 4275494UL : 4275479UL);
            *(uint32_t *)6384632UL = storemerge;
            var_20 = *(uint32_t *)6384408UL;
            var_21 = *var_3 - var_20;
            if ((uint64_t)(var_21 + (-1)) == 0UL) {
                var_27 = *(uint64_t *)(*var_5 + ((uint64_t)var_20 << 3UL));
                *(uint64_t *)(local_sp_1 + (-24L)) = 4207700UL;
                indirect_placeholder_34(0UL, var_27);
                return;
            }
            if ((int)var_21 > (int)1U) {
                if ((uint64_t)(var_21 + (-2)) != 0UL) {
                    *(unsigned char *)6384622UL = (unsigned char)'\x01';
                    *(uint64_t *)(local_sp_1 + (-24L)) = 4207659UL;
                    indirect_placeholder_34(1UL, 4275269UL);
                    return;
                }
            }
            if ((int)var_21 >= (int)4294967295U) {
                *(uint64_t *)(local_sp_1 + (-24L)) = 4207659UL;
                indirect_placeholder_34(1UL, 4275269UL);
                return;
            }
            var_22 = *(uint64_t *)(*var_5 + (((uint64_t)var_20 << 3UL) + 16UL));
            *(uint64_t *)(local_sp_1 + (-24L)) = 4207740UL;
            var_23 = indirect_placeholder_35(var_22);
            var_24 = var_23.field_0;
            var_25 = var_23.field_1;
            var_26 = var_23.field_2;
            *(uint64_t *)(local_sp_1 + (-32L)) = 4207768UL;
            indirect_placeholder_31(0UL, 4275506UL, var_24, 0UL, 0UL, var_25, var_26);
            *(uint64_t *)(local_sp_1 + (-40L)) = 4207778UL;
            indirect_placeholder_34(var_2, 1UL);
            abort();
        }
        break;
    }
}

typedef unsigned char __u_char;
typedef unsigned short __u_short;
typedef unsigned int __u_int;
typedef unsigned long __u_long;
typedef signed char __int8_t;
typedef unsigned char __uint8_t;
typedef short __int16_t;
typedef unsigned short __uint16_t;
typedef int __int32_t;
typedef unsigned int __uint32_t;
typedef long __int64_t;
typedef unsigned long __uint64_t;
typedef __int8_t __int_least8_t;
typedef __uint8_t __uint_least8_t;
typedef __int16_t __int_least16_t;
typedef __uint16_t __uint_least16_t;
typedef __int32_t __int_least32_t;
typedef __uint32_t __uint_least32_t;
typedef __int64_t __int_least64_t;
typedef __uint64_t __uint_least64_t;
typedef long __quad_t;
typedef unsigned long __u_quad_t;
typedef long __intmax_t;
typedef unsigned long __uintmax_t;
typedef unsigned long __dev_t;
typedef unsigned int __uid_t;
typedef unsigned int __gid_t;
typedef unsigned long __ino_t;
typedef unsigned long __ino64_t;
typedef unsigned int __mode_t;
typedef unsigned long __nlink_t;
typedef long __off_t;
typedef long __off64_t;
typedef int __pid_t;
typedef struct {
    int __val[2];
} __fsid_t;
typedef long __clock_t;
typedef unsigned long __rlim_t;
typedef unsigned long __rlim64_t;
typedef unsigned int __id_t;
typedef long __time_t;
typedef unsigned int __useconds_t;
typedef long __suseconds_t;
typedef int __daddr_t;
typedef int __key_t;
typedef int __clockid_t;
typedef void *__timer_t;
typedef long __blksize_t;
typedef long __blkcnt_t;
typedef long __blkcnt64_t;
typedef unsigned long __fsblkcnt_t;
typedef unsigned long __fsblkcnt64_t;
typedef unsigned long __fsfilcnt_t;
typedef unsigned long __fsfilcnt64_t;
typedef long __fsword_t;
typedef long __ssize_t;
typedef long __syscall_slong_t;
typedef unsigned long __syscall_ulong_t;
typedef __off64_t __loff_t;
typedef char *__caddr_t;
typedef long __intptr_t;
typedef unsigned int __socklen_t;
typedef int __sig_atomic_t;
typedef __int8_t int8_t;
typedef __int16_t int16_t;
typedef __int32_t int32_t;
typedef __int64_t int64_t;
typedef __uint8_t uint8_t;
typedef __uint16_t uint16_t;
typedef __uint32_t uint32_t;
typedef __uint64_t uint64_t;
typedef __int_least8_t int_least8_t;
typedef __int_least16_t int_least16_t;
typedef __int_least32_t int_least32_t;
typedef __int_least64_t int_least64_t;
typedef __uint_least8_t uint_least8_t;
typedef __uint_least16_t uint_least16_t;
typedef __uint_least32_t uint_least32_t;
typedef __uint_least64_t uint_least64_t;
typedef signed char int_fast8_t;
typedef long int_fast16_t;
typedef long int_fast32_t;
typedef long int_fast64_t;
typedef unsigned char uint_fast8_t;
typedef unsigned long uint_fast16_t;
typedef unsigned long uint_fast32_t;
typedef unsigned long uint_fast64_t;
typedef long intptr_t;
typedef unsigned long uintptr_t;
typedef __intmax_t intmax_t;
typedef __uintmax_t uintmax_t;
struct indirect_placeholder_35_ret_type;
struct indirect_placeholder_34_ret_type;
struct indirect_placeholder_37_ret_type;
struct indirect_placeholder_38_ret_type;
struct indirect_placeholder_39_ret_type;
struct indirect_placeholder_40_ret_type;
struct indirect_placeholder_41_ret_type;
struct indirect_placeholder_42_ret_type;
struct indirect_placeholder_43_ret_type;
struct indirect_placeholder_33_ret_type;
struct indirect_placeholder_44_ret_type;
struct indirect_placeholder_46_ret_type;
struct indirect_placeholder_36_ret_type;
struct indirect_placeholder_35_ret_type {
    uint64_t field_0;
    uint64_t field_1;
    uint64_t field_2;
};
struct indirect_placeholder_34_ret_type {
    uint64_t field_0;
    uint64_t field_1;
    uint64_t field_2;
};
struct indirect_placeholder_37_ret_type {
    uint64_t field_0;
    uint64_t field_1;
};
struct indirect_placeholder_38_ret_type {
    uint64_t field_0;
    uint64_t field_1;
};
struct indirect_placeholder_39_ret_type {
    uint64_t field_0;
    uint64_t field_1;
};
struct indirect_placeholder_40_ret_type {
    uint64_t field_0;
    uint64_t field_1;
};
struct indirect_placeholder_41_ret_type {
    uint64_t field_0;
    uint64_t field_1;
};
struct indirect_placeholder_42_ret_type {
    uint64_t field_0;
    uint64_t field_1;
};
struct indirect_placeholder_43_ret_type {
    uint64_t field_0;
    uint64_t field_1;
};
struct indirect_placeholder_33_ret_type {
    uint64_t field_0;
    uint64_t field_1;
};
struct indirect_placeholder_44_ret_type {
    uint64_t field_0;
    uint64_t field_1;
};
struct indirect_placeholder_46_ret_type {
    uint64_t field_0;
    uint64_t field_1;
};
struct indirect_placeholder_36_ret_type {
    uint64_t field_0;
    uint64_t field_1;
    uint64_t field_2;
};
extern void abort(void);
extern uint64_t revng_init_local_sp(uint64_t param_0);
extern uint64_t init_rbp(void);
extern uint64_t indirect_placeholder(uint64_t param_0, uint64_t param_1);
extern void indirect_placeholder_1(void);
extern uint64_t indirect_placeholder_10(uint64_t param_0, uint64_t param_1, uint64_t param_2);
extern uint64_t init_rbx(void);
extern uint64_t indirect_placeholder_45(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint64_t param_3, uint64_t param_4, uint64_t param_5);
extern void indirect_placeholder_32(uint64_t param_0);
extern uint64_t indirect_placeholder_13(uint64_t param_0);
extern uint64_t helper_cc_compute_all_wrapper(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint32_t param_3);
extern void indirect_placeholder_12(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint64_t param_3, uint64_t param_4, uint64_t param_5, uint64_t param_6);
extern void indirect_placeholder_31(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint64_t param_3, uint64_t param_4);
extern struct indirect_placeholder_35_ret_type indirect_placeholder_35(uint64_t param_0, uint64_t param_1);
extern struct indirect_placeholder_34_ret_type indirect_placeholder_34(uint64_t param_0, uint64_t param_1);
extern struct indirect_placeholder_37_ret_type indirect_placeholder_37(uint64_t param_0);
extern struct indirect_placeholder_38_ret_type indirect_placeholder_38(uint64_t param_0);
extern struct indirect_placeholder_39_ret_type indirect_placeholder_39(uint64_t param_0);
extern struct indirect_placeholder_40_ret_type indirect_placeholder_40(uint64_t param_0);
extern struct indirect_placeholder_41_ret_type indirect_placeholder_41(uint64_t param_0);
extern struct indirect_placeholder_42_ret_type indirect_placeholder_42(uint64_t param_0);
extern struct indirect_placeholder_43_ret_type indirect_placeholder_43(uint64_t param_0, uint64_t param_1, uint64_t param_2);
extern struct indirect_placeholder_33_ret_type indirect_placeholder_33(uint64_t param_0, uint64_t param_1);
extern struct indirect_placeholder_44_ret_type indirect_placeholder_44(uint64_t param_0, uint64_t param_1, uint64_t param_2);
extern struct indirect_placeholder_46_ret_type indirect_placeholder_46(uint64_t param_0);
extern struct indirect_placeholder_36_ret_type indirect_placeholder_36(uint64_t param_0, uint64_t param_1);
uint64_t bb_print_stat(uint64_t rdx, uint64_t rcx, uint64_t rdi, uint64_t rsi, uint64_t r9, uint64_t r8) {
    uint64_t var_0;
    uint64_t var_1;
    uint64_t var_2;
    uint64_t *var_3;
    uint64_t *var_4;
    uint32_t *var_5;
    uint32_t *var_6;
    uint64_t *var_7;
    uint64_t *var_8;
    unsigned char *var_9;
    uint32_t var_10;
    uint64_t var_70;
    uint64_t var_71;
    uint64_t var_72;
    uint64_t var_113;
    struct indirect_placeholder_35_ret_type var_114;
    uint64_t var_115;
    uint64_t var_116;
    uint64_t var_117;
    uint64_t var_118;
    uint64_t storemerge;
    uint64_t var_109;
    uint64_t var_110;
    uint64_t var_111;
    uint64_t var_112;
    uint64_t var_105;
    uint64_t var_106;
    uint64_t var_107;
    uint64_t *var_108;
    uint64_t var_120;
    uint64_t var_11;
    uint64_t var_12;
    uint64_t var_13;
    uint64_t var_14;
    uint64_t var_15;
    struct indirect_placeholder_37_ret_type var_16;
    uint64_t var_17;
    uint64_t var_18;
    uint64_t var_19;
    uint64_t var_20;
    uint64_t var_21;
    uint64_t var_22;
    struct indirect_placeholder_38_ret_type var_23;
    uint64_t var_24;
    uint64_t var_25;
    uint64_t var_26;
    uint64_t var_27;
    struct indirect_placeholder_39_ret_type var_28;
    uint64_t var_29;
    uint64_t var_30;
    uint64_t var_31;
    uint64_t var_32;
    uint64_t var_87;
    uint64_t var_33;
    uint64_t var_34;
    struct indirect_placeholder_40_ret_type var_35;
    uint64_t var_36;
    uint64_t var_37;
    uint64_t var_38;
    uint64_t var_39;
    struct indirect_placeholder_41_ret_type var_40;
    uint64_t var_41;
    uint64_t var_42;
    uint64_t var_43;
    uint64_t var_44;
    uint64_t var_45;
    uint64_t var_46;
    struct indirect_placeholder_42_ret_type var_47;
    uint64_t var_48;
    uint64_t var_49;
    uint64_t var_50;
    uint64_t var_51;
    uint64_t var_52;
    uint64_t var_53;
    struct indirect_placeholder_43_ret_type var_54;
    uint64_t var_55;
    uint64_t var_56;
    struct indirect_placeholder_33_ret_type var_57;
    uint64_t var_58;
    uint64_t var_59;
    uint64_t var_60;
    uint64_t var_61;
    uint64_t var_62;
    uint64_t var_63;
    uint64_t var_64;
    uint64_t var_65;
    struct indirect_placeholder_44_ret_type var_66;
    uint64_t var_67;
    uint64_t var_68;
    uint64_t *var_69;
    uint64_t var_73;
    uint64_t var_74;
    uint64_t var_75;
    uint64_t var_76;
    uint64_t var_77;
    uint64_t var_78;
    uint64_t var_79;
    uint64_t var_80;
    uint64_t var_81;
    uint64_t var_82;
    uint64_t var_83;
    uint64_t var_84;
    uint64_t var_85;
    uint64_t var_86;
    uint64_t var_88;
    uint64_t var_89;
    uint64_t var_90;
    uint64_t var_91;
    uint64_t var_92;
    uint64_t var_93;
    uint64_t var_94;
    uint64_t var_95;
    uint64_t var_96;
    uint64_t var_97;
    uint64_t var_98;
    uint64_t var_99;
    uint64_t var_100;
    uint64_t var_101;
    uint64_t var_102;
    uint64_t var_103;
    uint64_t var_104;
    uint64_t var_119;
    var_0 = revng_init_local_sp(0UL);
    var_1 = init_rbp();
    var_2 = init_rbx();
    *(uint64_t *)(var_0 + (-8L)) = var_1;
    *(uint64_t *)(var_0 + (-16L)) = var_2;
    var_3 = (uint64_t *)(var_0 + (-96L));
    *var_3 = rdi;
    var_4 = (uint64_t *)(var_0 + (-104L));
    *var_4 = rsi;
    var_5 = (uint32_t *)(var_0 + (-108L));
    *var_5 = (uint32_t)rdx;
    var_6 = (uint32_t *)(var_0 + (-112L));
    *var_6 = (uint32_t)rcx;
    var_7 = (uint64_t *)(var_0 + (-120L));
    *var_7 = r8;
    *(uint64_t *)(var_0 + (-128L)) = r9;
    var_8 = (uint64_t *)(var_0 + (-40L));
    *var_8 = r9;
    var_9 = (unsigned char *)(var_0 + (-25L));
    *var_9 = (unsigned char)'\x00';
    var_10 = *var_5 + (-65);
    storemerge = 1UL;
    if (var_10 <= 57U) {
        *(uint64_t *)(var_0 + (-144L)) = 4213084UL;
        indirect_placeholder_1();
        var_120 = (uint64_t)*var_9;
        storemerge = var_120;
        return storemerge;
    }
    switch (*(uint64_t *)(((uint64_t)var_10 << 3UL) + 4320840UL)) {
      case 4211446UL:
        {
            *(uint64_t *)(var_0 + (-144L)) = 4211456UL;
            var_100 = indirect_placeholder_13(0UL);
            var_101 = (uint64_t)(uint32_t)var_100;
            var_102 = *var_7;
            *(uint64_t *)(var_0 + (-152L)) = 4211472UL;
            indirect_placeholder_36(var_101, var_102);
            var_103 = *var_4;
            *(uint64_t *)(var_0 + (-160L)) = 4211494UL;
            indirect_placeholder_32(var_103);
            var_104 = *var_8;
            if ((uint32_t)((uint16_t)*(uint32_t *)(var_104 + 24UL) & (unsigned short)61440U) != 40960U) {
                var_105 = *(uint64_t *)(var_104 + 48UL);
                var_106 = *var_7;
                *(uint64_t *)(var_0 + (-168L)) = 4211543UL;
                var_107 = indirect_placeholder(var_106, var_105);
                var_108 = (uint64_t *)(var_0 + (-48L));
                *var_108 = var_107;
                if (var_107 != 0UL) {
                    var_113 = *var_7;
                    *(uint64_t *)(var_0 + (-176L)) = 4211571UL;
                    var_114 = indirect_placeholder_35(4UL, var_113);
                    var_115 = var_114.field_0;
                    var_116 = var_114.field_1;
                    var_117 = var_114.field_2;
                    *(uint64_t *)(var_0 + (-184L)) = 4211579UL;
                    indirect_placeholder_1();
                    var_118 = (uint64_t)*(uint32_t *)var_115;
                    *(uint64_t *)(var_0 + (-192L)) = 4211606UL;
                    indirect_placeholder_12(0UL, 4320793UL, var_115, 0UL, var_118, var_116, var_117);
                    return storemerge;
                }
                *(uint64_t *)(var_0 + (-176L)) = 4211631UL;
                indirect_placeholder_1();
                *(uint64_t *)(var_0 + (-184L)) = 4211641UL;
                var_109 = indirect_placeholder_13(0UL);
                var_110 = (uint64_t)(uint32_t)var_109;
                var_111 = *var_108;
                *(uint64_t *)(var_0 + (-192L)) = 4211657UL;
                indirect_placeholder_34(var_110, var_111);
                var_112 = *var_4;
                *(uint64_t *)(var_0 + (-200L)) = 4211679UL;
                indirect_placeholder_32(var_112);
                *(uint64_t *)(var_0 + (-208L)) = 4211691UL;
                indirect_placeholder_1();
            }
        }
        break;
      case 4213064UL:
      case 4213022UL:
      case 4212976UL:
      case 4212920UL:
      case 4212871UL:
      case 4212812UL:
      case 4212763UL:
      case 4212704UL:
      case 4212626UL:
      case 4212513UL:
      case 4212432UL:
      case 4212397UL:
      case 4212368UL:
      case 4212336UL:
      case 4212281UL:
      case 4212235UL:
      case 4212171UL:
      case 4212108UL:
      case 4212075UL:
      case 4212012UL:
      case 4211979UL:
      case 4211947UL:
      case 4211908UL:
      case 4211875UL:
      case 4211836UL:
      case 4211795UL:
      case 4211763UL:
      case 4211732UL:
      case 4211701UL:
      case 4211418UL:
        {
            switch (*(uint64_t *)(((uint64_t)var_10 << 3UL) + 4320840UL)) {
              case 4213022UL:
                {
                    var_11 = *var_7;
                    var_12 = *var_4;
                    var_13 = *var_3;
                    *(uint64_t *)(var_0 + (-144L)) = 4213045UL;
                    var_14 = indirect_placeholder_10(var_11, var_13, var_12);
                    *var_9 = (((uint64_t)(unsigned char)var_14 | (uint64_t)*var_9) != 0UL);
                }
                break;
              case 4212812UL:
                {
                    var_34 = *var_8;
                    *(uint64_t *)(var_0 + (-144L)) = 4212824UL;
                    var_35 = indirect_placeholder_40(var_34);
                    var_36 = var_35.field_0;
                    var_37 = var_35.field_1;
                    *(uint64_t *)(var_0 + (-152L)) = 4212844UL;
                    indirect_placeholder_10(var_36, var_37, var_37);
                    var_38 = *var_4;
                    *(uint64_t *)(var_0 + (-160L)) = 4212866UL;
                    indirect_placeholder_32(var_38);
                }
                break;
              case 4212626UL:
                {
                    var_51 = *var_8;
                    var_52 = *var_7;
                    var_53 = (uint64_t)*var_6;
                    *(uint64_t *)(var_0 + (-144L)) = 4212647UL;
                    var_54 = indirect_placeholder_43(var_51, var_53, var_52);
                    var_55 = var_54.field_0;
                    var_56 = var_54.field_1;
                    *(uint64_t *)(var_0 + (-152L)) = 4212667UL;
                    var_57 = indirect_placeholder_33(var_55, var_56);
                    var_58 = var_57.field_0;
                    var_59 = var_57.field_1;
                    var_60 = *var_8;
                    var_61 = *var_4;
                    var_62 = *var_3;
                    *(uint64_t *)(var_0 + (-160L)) = 4212699UL;
                    indirect_placeholder_31(var_60, var_58, var_62, var_61, var_59);
                }
                break;
              case 4211908UL:
                {
                    var_91 = *var_8;
                    *(uint64_t *)(var_0 + (-144L)) = 4211920UL;
                    indirect_placeholder_13(var_91);
                    var_92 = *var_4;
                    *(uint64_t *)(var_0 + (-152L)) = 4211942UL;
                    indirect_placeholder_32(var_92);
                }
                break;
              case 4211875UL:
                {
                    var_93 = *var_4;
                    *(uint64_t *)(var_0 + (-144L)) = 4211903UL;
                    indirect_placeholder_32(var_93);
                }
                break;
              case 4212920UL:
                {
                    var_22 = *var_8;
                    *(uint64_t *)(var_0 + (-144L)) = 4212932UL;
                    var_23 = indirect_placeholder_38(var_22);
                    var_24 = var_23.field_0;
                    var_25 = var_23.field_1;
                    *(uint64_t *)(var_0 + (-152L)) = 4212952UL;
                    indirect_placeholder_10(var_24, var_25, var_25);
                    var_26 = *var_4;
                    *(uint64_t *)(var_0 + (-160L)) = 4212974UL;
                    indirect_placeholder_32(var_26);
                }
                break;
              case 4212171UL:
                {
                    var_83 = *var_4;
                    *(uint64_t *)(var_0 + (-144L)) = 4212230UL;
                    indirect_placeholder_32(var_83);
                }
                break;
              case 4211732UL:
                {
                    var_98 = *var_4;
                    *(uint64_t *)(var_0 + (-144L)) = 4211758UL;
                    indirect_placeholder_32(var_98);
                }
                break;
              case 4212108UL:
                {
                    var_84 = (uint64_t)*(uint32_t *)(*var_8 + 32UL);
                    *(uint64_t *)(var_0 + (-144L)) = 4212122UL;
                    indirect_placeholder_1();
                    *(uint64_t *)(var_0 + (-64L)) = var_84;
                    var_85 = *var_4;
                    *(uint64_t *)(var_0 + (-152L)) = 4212166UL;
                    indirect_placeholder_32(var_85);
                }
                break;
              case 4212432UL:
                {
                    helper_cc_compute_all_wrapper(*(uint64_t *)(*var_8 + 56UL), 0UL, 0UL, 25U);
                    var_73 = *var_4;
                    *(uint64_t *)(var_0 + (-144L)) = 4212508UL;
                    indirect_placeholder_32(var_73);
                }
                break;
              case 4212397UL:
                {
                    var_74 = *var_4;
                    *(uint64_t *)(var_0 + (-144L)) = 4212427UL;
                    indirect_placeholder_32(var_74);
                }
                break;
              case 4212336UL:
                {
                    var_76 = *var_4;
                    *(uint64_t *)(var_0 + (-144L)) = 4212363UL;
                    indirect_placeholder_13(var_76);
                }
                break;
              case 4211795UL:
                {
                    var_96 = *var_4;
                    *(uint64_t *)(var_0 + (-144L)) = 4211831UL;
                    indirect_placeholder_32(var_96);
                }
                break;
              case 4211836UL:
                {
                    var_94 = *var_8;
                    *(uint64_t *)(var_0 + (-144L)) = 4211848UL;
                    indirect_placeholder_46(var_94);
                    var_95 = *var_4;
                    *(uint64_t *)(var_0 + (-152L)) = 4211870UL;
                    indirect_placeholder_32(var_95);
                }
                break;
              case 4212281UL:
                {
                    var_77 = *var_4;
                    *(uint64_t *)(var_0 + (-144L)) = 4212331UL;
                    indirect_placeholder_32(var_77);
                }
                break;
              case 4212704UL:
                {
                    var_46 = *var_8;
                    *(uint64_t *)(var_0 + (-144L)) = 4212716UL;
                    var_47 = indirect_placeholder_42(var_46);
                    var_48 = var_47.field_0;
                    var_49 = var_47.field_1;
                    *(uint64_t *)(var_0 + (-152L)) = 4212736UL;
                    indirect_placeholder_10(var_48, var_49, var_49);
                    var_50 = *var_4;
                    *(uint64_t *)(var_0 + (-160L)) = 4212758UL;
                    indirect_placeholder_32(var_50);
                }
                break;
              case 4212012UL:
                {
                    var_87 = (uint64_t)*(uint32_t *)(*var_8 + 28UL);
                    *(uint64_t *)(var_0 + (-144L)) = 4212026UL;
                    indirect_placeholder_1();
                    *(uint64_t *)(var_0 + (-56L)) = var_87;
                    var_88 = *var_4;
                    *(uint64_t *)(var_0 + (-152L)) = 4212070UL;
                    indirect_placeholder_32(var_88);
                }
                break;
              case 4213064UL:
                {
                    *(uint64_t *)(var_0 + (-144L)) = 4213084UL;
                    indirect_placeholder_1();
                }
                break;
              case 4211979UL:
                {
                    var_89 = *var_4;
                    *(uint64_t *)(var_0 + (-144L)) = 4212007UL;
                    indirect_placeholder_32(var_89);
                }
                break;
              case 4211418UL:
                {
                    var_119 = *var_4;
                    *(uint64_t *)(var_0 + (-144L)) = 4211441UL;
                    indirect_placeholder_32(var_119);
                }
                break;
              case 4212368UL:
                {
                    var_75 = *var_4;
                    *(uint64_t *)(var_0 + (-144L)) = 4212392UL;
                    indirect_placeholder_32(var_75);
                }
                break;
              case 4211763UL:
                {
                    var_97 = *var_4;
                    *(uint64_t *)(var_0 + (-144L)) = 4211790UL;
                    indirect_placeholder_32(var_97);
                }
                break;
              case 4212075UL:
                {
                    var_86 = *var_4;
                    *(uint64_t *)(var_0 + (-144L)) = 4212103UL;
                    indirect_placeholder_32(var_86);
                }
                break;
              case 4212235UL:
                {
                    var_78 = *var_8;
                    var_79 = *var_4;
                    var_80 = *var_3;
                    var_81 = *var_7;
                    *(uint64_t *)(var_0 + (-144L)) = 4212259UL;
                    var_82 = indirect_placeholder_45(var_79, var_78, var_81, var_80, r9, r8);
                    *var_9 = (((uint64_t)(unsigned char)var_82 | (uint64_t)*var_9) != 0UL);
                }
                break;
              case 4212871UL:
                {
                    var_27 = *var_8;
                    *(uint64_t *)(var_0 + (-144L)) = 4212883UL;
                    var_28 = indirect_placeholder_39(var_27);
                    var_29 = var_28.field_0;
                    var_30 = var_28.field_1;
                    var_31 = *var_8;
                    var_32 = *var_4;
                    var_33 = *var_3;
                    *(uint64_t *)(var_0 + (-152L)) = 4212915UL;
                    indirect_placeholder_31(var_31, var_29, var_33, var_32, var_30);
                }
                break;
              case 4211947UL:
                {
                    var_90 = *var_4;
                    *(uint64_t *)(var_0 + (-144L)) = 4211974UL;
                    indirect_placeholder_32(var_90);
                }
                break;
              case 4211701UL:
                {
                    var_99 = *var_4;
                    *(uint64_t *)(var_0 + (-144L)) = 4211727UL;
                    indirect_placeholder_32(var_99);
                }
                break;
              case 4212763UL:
                {
                    var_39 = *var_8;
                    *(uint64_t *)(var_0 + (-144L)) = 4212775UL;
                    var_40 = indirect_placeholder_41(var_39);
                    var_41 = var_40.field_0;
                    var_42 = var_40.field_1;
                    var_43 = *var_8;
                    var_44 = *var_4;
                    var_45 = *var_3;
                    *(uint64_t *)(var_0 + (-152L)) = 4212807UL;
                    indirect_placeholder_31(var_43, var_41, var_45, var_44, var_42);
                }
                break;
              case 4212976UL:
                {
                    var_15 = *var_8;
                    *(uint64_t *)(var_0 + (-144L)) = 4212988UL;
                    var_16 = indirect_placeholder_37(var_15);
                    var_17 = var_16.field_0;
                    var_18 = var_16.field_1;
                    var_19 = *var_8;
                    var_20 = *var_4;
                    var_21 = *var_3;
                    *(uint64_t *)(var_0 + (-152L)) = 4213020UL;
                    indirect_placeholder_31(var_19, var_17, var_21, var_20, var_18);
                }
                break;
              case 4212513UL:
                {
                    var_63 = *var_8;
                    var_64 = *var_7;
                    var_65 = (uint64_t)*var_6;
                    *(uint64_t *)(var_0 + (-144L)) = 4212534UL;
                    var_66 = indirect_placeholder_44(var_63, var_65, var_64);
                    var_67 = var_66.field_0;
                    var_68 = var_66.field_1;
                    var_69 = (uint64_t *)(var_0 + (-88L));
                    *var_69 = var_67;
                    *(uint64_t *)(var_0 + (-80L)) = var_68;
                    if ((long)var_68 > (long)18446744073709551615UL) {
                        var_71 = *var_69;
                        *(uint64_t *)(var_0 + (-152L)) = 4212599UL;
                        indirect_placeholder(var_71, var_68);
                        var_72 = *var_4;
                        *(uint64_t *)(var_0 + (-160L)) = 4212621UL;
                        indirect_placeholder_32(var_72);
                    } else {
                        var_70 = *var_4;
                        *(uint64_t *)(var_0 + (-152L)) = 4212575UL;
                        indirect_placeholder_32(var_70);
                    }
                }
                break;
            }
        }
        break;
      default:
        {
            abort();
        }
        break;
    }
    var_120 = (uint64_t)*var_9;
    storemerge = var_120;
    return storemerge;
}

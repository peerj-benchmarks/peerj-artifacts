typedef unsigned char __u_char;
typedef unsigned short __u_short;
typedef unsigned int __u_int;
typedef unsigned long __u_long;
typedef signed char __int8_t;
typedef unsigned char __uint8_t;
typedef short __int16_t;
typedef unsigned short __uint16_t;
typedef int __int32_t;
typedef unsigned int __uint32_t;
typedef long __int64_t;
typedef unsigned long __uint64_t;
typedef __int8_t __int_least8_t;
typedef __uint8_t __uint_least8_t;
typedef __int16_t __int_least16_t;
typedef __uint16_t __uint_least16_t;
typedef __int32_t __int_least32_t;
typedef __uint32_t __uint_least32_t;
typedef __int64_t __int_least64_t;
typedef __uint64_t __uint_least64_t;
typedef long __quad_t;
typedef unsigned long __u_quad_t;
typedef long __intmax_t;
typedef unsigned long __uintmax_t;
typedef unsigned long __dev_t;
typedef unsigned int __uid_t;
typedef unsigned int __gid_t;
typedef unsigned long __ino_t;
typedef unsigned long __ino64_t;
typedef unsigned int __mode_t;
typedef unsigned long __nlink_t;
typedef long __off_t;
typedef long __off64_t;
typedef int __pid_t;
typedef struct {
    int __val[2];
} __fsid_t;
typedef long __clock_t;
typedef unsigned long __rlim_t;
typedef unsigned long __rlim64_t;
typedef unsigned int __id_t;
typedef long __time_t;
typedef unsigned int __useconds_t;
typedef long __suseconds_t;
typedef int __daddr_t;
typedef int __key_t;
typedef int __clockid_t;
typedef void *__timer_t;
typedef long __blksize_t;
typedef long __blkcnt_t;
typedef long __blkcnt64_t;
typedef unsigned long __fsblkcnt_t;
typedef unsigned long __fsblkcnt64_t;
typedef unsigned long __fsfilcnt_t;
typedef unsigned long __fsfilcnt64_t;
typedef long __fsword_t;
typedef long __ssize_t;
typedef long __syscall_slong_t;
typedef unsigned long __syscall_ulong_t;
typedef __off64_t __loff_t;
typedef char *__caddr_t;
typedef long __intptr_t;
typedef unsigned int __socklen_t;
typedef int __sig_atomic_t;
typedef __int8_t int8_t;
typedef __int16_t int16_t;
typedef __int32_t int32_t;
typedef __int64_t int64_t;
typedef __uint8_t uint8_t;
typedef __uint16_t uint16_t;
typedef __uint32_t uint32_t;
typedef __uint64_t uint64_t;
typedef __int_least8_t int_least8_t;
typedef __int_least16_t int_least16_t;
typedef __int_least32_t int_least32_t;
typedef __int_least64_t int_least64_t;
typedef __uint_least8_t uint_least8_t;
typedef __uint_least16_t uint_least16_t;
typedef __uint_least32_t uint_least32_t;
typedef __uint_least64_t uint_least64_t;
typedef signed char int_fast8_t;
typedef long int_fast16_t;
typedef long int_fast32_t;
typedef long int_fast64_t;
typedef unsigned char uint_fast8_t;
typedef unsigned long uint_fast16_t;
typedef unsigned long uint_fast32_t;
typedef unsigned long uint_fast64_t;
typedef long intptr_t;
typedef unsigned long uintptr_t;
typedef __intmax_t intmax_t;
typedef __uintmax_t uintmax_t;
typedef _Bool bool;
struct indirect_placeholder_27_ret_type;
struct indirect_placeholder_29_ret_type;
struct indirect_placeholder_27_ret_type {
    uint64_t field_0;
    uint64_t field_1;
    uint64_t field_2;
};
struct indirect_placeholder_29_ret_type {
    uint64_t field_0;
    uint64_t field_1;
    uint64_t field_2;
};
extern uint64_t revng_init_local_sp(uint64_t param_0);
extern uint64_t init_rbp(void);
extern void indirect_placeholder_1(void);
extern uint64_t indirect_placeholder_28(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint64_t param_3, uint64_t param_4);
extern uint64_t init_rbx(void);
extern void indirect_placeholder_12(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint64_t param_3, uint64_t param_4, uint64_t param_5, uint64_t param_6);
extern struct indirect_placeholder_27_ret_type indirect_placeholder_27(uint64_t param_0, uint64_t param_1);
extern struct indirect_placeholder_29_ret_type indirect_placeholder_29(uint64_t param_0, uint64_t param_1);
uint64_t bb_do_statfs(uint64_t rdi, uint64_t rsi) {
    uint64_t var_18;
    uint64_t var_0;
    uint64_t var_1;
    uint64_t var_2;
    uint64_t *var_3;
    uint64_t *var_4;
    uint64_t var_5;
    bool var_6;
    uint64_t var_7;
    uint64_t *var_8;
    uint64_t var_9;
    struct indirect_placeholder_27_ret_type var_10;
    uint64_t var_11;
    uint64_t var_12;
    uint64_t var_13;
    uint64_t var_14;
    uint64_t rax_0;
    uint64_t var_15;
    uint64_t var_16;
    uint64_t var_17;
    unsigned char *var_19;
    unsigned char var_20;
    struct indirect_placeholder_29_ret_type var_21;
    uint64_t var_22;
    uint64_t var_23;
    uint64_t var_24;
    var_0 = revng_init_local_sp(0UL);
    var_1 = init_rbp();
    var_2 = init_rbx();
    *(uint64_t *)(var_0 + (-8L)) = var_1;
    *(uint64_t *)(var_0 + (-16L)) = var_2;
    var_3 = (uint64_t *)(var_0 + (-160L));
    *var_3 = rdi;
    var_4 = (uint64_t *)(var_0 + (-168L));
    *var_4 = rsi;
    var_5 = *var_3;
    *(uint64_t *)(var_0 + (-176L)) = 4214385UL;
    indirect_placeholder_1();
    var_6 = ((uint64_t)(uint32_t)var_5 == 0UL);
    var_7 = *var_3;
    var_8 = (uint64_t *)(var_0 + (-184L));
    rax_0 = 0UL;
    if (var_6) {
        *var_8 = 4214409UL;
        var_21 = indirect_placeholder_29(4UL, var_7);
        var_22 = var_21.field_0;
        var_23 = var_21.field_1;
        var_24 = var_21.field_2;
        *(uint64_t *)(var_0 + (-192L)) = 4214437UL;
        indirect_placeholder_12(0UL, 4322088UL, var_22, 0UL, 0UL, var_23, var_24);
    } else {
        *var_8 = 4214472UL;
        indirect_placeholder_1();
        if ((uint64_t)(uint32_t)var_7 == 0UL) {
            var_15 = var_0 + (-152L);
            var_16 = *var_3;
            var_17 = *var_4;
            *(uint64_t *)(var_0 + (-192L)) = 4214580UL;
            var_18 = indirect_placeholder_28(var_16, 4209785UL, var_17, 4294967295UL, var_15);
            var_19 = (unsigned char *)(var_0 + (-25L));
            var_20 = (unsigned char)var_18;
            *var_19 = var_20;
            rax_0 = (var_20 == '\x00');
        } else {
            var_9 = *var_3;
            *(uint64_t *)(var_0 + (-192L)) = 4214496UL;
            var_10 = indirect_placeholder_27(4UL, var_9);
            var_11 = var_10.field_0;
            var_12 = var_10.field_1;
            var_13 = var_10.field_2;
            *(uint64_t *)(var_0 + (-200L)) = 4214504UL;
            indirect_placeholder_1();
            var_14 = (uint64_t)*(uint32_t *)var_11;
            *(uint64_t *)(var_0 + (-208L)) = 4214531UL;
            indirect_placeholder_12(0UL, 4322160UL, var_11, 0UL, var_14, var_12, var_13);
        }
    }
    return rax_0;
}

typedef unsigned char __u_char;
typedef unsigned short __u_short;
typedef unsigned int __u_int;
typedef unsigned long __u_long;
typedef signed char __int8_t;
typedef unsigned char __uint8_t;
typedef short __int16_t;
typedef unsigned short __uint16_t;
typedef int __int32_t;
typedef unsigned int __uint32_t;
typedef long __int64_t;
typedef unsigned long __uint64_t;
typedef __int8_t __int_least8_t;
typedef __uint8_t __uint_least8_t;
typedef __int16_t __int_least16_t;
typedef __uint16_t __uint_least16_t;
typedef __int32_t __int_least32_t;
typedef __uint32_t __uint_least32_t;
typedef __int64_t __int_least64_t;
typedef __uint64_t __uint_least64_t;
typedef long __quad_t;
typedef unsigned long __u_quad_t;
typedef long __intmax_t;
typedef unsigned long __uintmax_t;
typedef unsigned long __dev_t;
typedef unsigned int __uid_t;
typedef unsigned int __gid_t;
typedef unsigned long __ino_t;
typedef unsigned long __ino64_t;
typedef unsigned int __mode_t;
typedef unsigned long __nlink_t;
typedef long __off_t;
typedef long __off64_t;
typedef int __pid_t;
typedef struct {
    int __val[2];
} __fsid_t;
typedef long __clock_t;
typedef unsigned long __rlim_t;
typedef unsigned long __rlim64_t;
typedef unsigned int __id_t;
typedef long __time_t;
typedef unsigned int __useconds_t;
typedef long __suseconds_t;
typedef int __daddr_t;
typedef int __key_t;
typedef int __clockid_t;
typedef void *__timer_t;
typedef long __blksize_t;
typedef long __blkcnt_t;
typedef long __blkcnt64_t;
typedef unsigned long __fsblkcnt_t;
typedef unsigned long __fsblkcnt64_t;
typedef unsigned long __fsfilcnt_t;
typedef unsigned long __fsfilcnt64_t;
typedef long __fsword_t;
typedef long __ssize_t;
typedef long __syscall_slong_t;
typedef unsigned long __syscall_ulong_t;
typedef __off64_t __loff_t;
typedef char *__caddr_t;
typedef long __intptr_t;
typedef unsigned int __socklen_t;
typedef int __sig_atomic_t;
typedef __int8_t int8_t;
typedef __int16_t int16_t;
typedef __int32_t int32_t;
typedef __int64_t int64_t;
typedef __uint8_t uint8_t;
typedef __uint16_t uint16_t;
typedef __uint32_t uint32_t;
typedef __uint64_t uint64_t;
typedef __int_least8_t int_least8_t;
typedef __int_least16_t int_least16_t;
typedef __int_least32_t int_least32_t;
typedef __int_least64_t int_least64_t;
typedef __uint_least8_t uint_least8_t;
typedef __uint_least16_t uint_least16_t;
typedef __uint_least32_t uint_least32_t;
typedef __uint_least64_t uint_least64_t;
typedef signed char int_fast8_t;
typedef long int_fast16_t;
typedef long int_fast32_t;
typedef long int_fast64_t;
typedef unsigned char uint_fast8_t;
typedef unsigned long uint_fast16_t;
typedef unsigned long uint_fast32_t;
typedef unsigned long uint_fast64_t;
typedef long intptr_t;
typedef unsigned long uintptr_t;
typedef __intmax_t intmax_t;
typedef __uintmax_t uintmax_t;
typedef _Bool bool;
struct indirect_placeholder_89_ret_type;
struct indirect_placeholder_89_ret_type {
    uint64_t field_0;
    uint64_t field_1;
    uint64_t field_2;
    uint64_t field_3;
};
extern void abort(void);
extern uint64_t revng_init_local_sp(uint64_t param_0);
extern uint64_t init_rbp(void);
extern uint64_t indirect_placeholder(uint64_t param_0, uint64_t param_1);
extern void indirect_placeholder_1(void);
extern uint64_t indirect_placeholder_10(uint64_t param_0, uint64_t param_1, uint64_t param_2);
extern void indirect_placeholder_2(uint64_t param_0, uint64_t param_1);
extern void indirect_placeholder_32(uint64_t param_0);
extern void indirect_placeholder_12(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint64_t param_3, uint64_t param_4, uint64_t param_5, uint64_t param_6);
extern void indirect_placeholder_88(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint64_t param_3, uint64_t param_4, uint64_t param_5);
extern struct indirect_placeholder_89_ret_type indirect_placeholder_89(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint64_t param_3, uint64_t param_4);
void bb_main(uint64_t rdi, uint64_t rsi) {
    struct indirect_placeholder_89_ret_type var_17;
    uint64_t var_0;
    uint64_t var_1;
    uint64_t var_2;
    uint32_t *var_3;
    uint64_t var_4;
    uint64_t *var_5;
    unsigned char *var_6;
    unsigned char *var_7;
    uint64_t *var_8;
    unsigned char *var_9;
    uint64_t var_10;
    uint64_t var_11;
    uint64_t storemerge;
    uint64_t local_sp_0;
    uint64_t storemerge7_in;
    uint32_t var_47;
    uint32_t var_37;
    uint64_t var_24;
    uint64_t local_sp_1;
    uint64_t var_25;
    uint64_t *var_26;
    uint64_t *_pre_phi133;
    uint64_t local_sp_2;
    uint32_t var_35;
    uint32_t *var_36;
    uint64_t local_sp_3;
    bool var_38;
    uint64_t var_39;
    uint64_t var_40;
    uint64_t var_41;
    uint64_t var_42;
    uint64_t var_43;
    uint64_t var_44;
    uint64_t var_45;
    uint64_t var_46;
    uint64_t var_48;
    uint64_t var_49;
    uint64_t var_50;
    uint64_t var_21;
    uint64_t var_22;
    uint64_t var_23;
    uint64_t var_27;
    uint64_t var_28;
    uint64_t var_29;
    uint64_t var_30;
    uint64_t var_31;
    uint64_t var_32;
    uint64_t var_33;
    uint64_t *var_34;
    uint64_t local_sp_4;
    uint64_t var_19;
    uint64_t var_20;
    uint64_t local_sp_5;
    uint64_t var_14;
    uint64_t var_15;
    uint64_t var_16;
    uint32_t var_18;
    uint64_t var_12;
    uint32_t *var_13;
    unsigned int loop_state_var;
    var_0 = revng_init_local_sp(0UL);
    var_1 = init_rbp();
    var_2 = var_0 + (-8L);
    *(uint64_t *)var_2 = var_1;
    var_3 = (uint32_t *)(var_0 + (-60L));
    *var_3 = (uint32_t)rdi;
    var_4 = var_0 + (-72L);
    var_5 = (uint64_t *)var_4;
    *var_5 = rsi;
    var_6 = (unsigned char *)(var_0 + (-9L));
    *var_6 = (unsigned char)'\x00';
    var_7 = (unsigned char *)(var_0 + (-10L));
    *var_7 = (unsigned char)'\x00';
    var_8 = (uint64_t *)(var_0 + (-24L));
    *var_8 = 0UL;
    var_9 = (unsigned char *)(var_0 + (-33L));
    *var_9 = (unsigned char)'\x01';
    var_10 = **(uint64_t **)var_4;
    *(uint64_t *)(var_0 + (-80L)) = 4215759UL;
    indirect_placeholder_32(var_10);
    *(uint64_t *)(var_0 + (-88L)) = 4215774UL;
    indirect_placeholder_1();
    *(uint64_t *)(var_0 + (-96L)) = 4215779UL;
    indirect_placeholder_1();
    var_11 = var_0 + (-48L);
    *(uint64_t *)var_11 = var_10;
    storemerge = 4325607UL;
    if (***(unsigned char ***)var_11 == '\x00') {
        storemerge = **(uint64_t **)var_11;
    }
    *(uint64_t *)6442024UL = storemerge;
    *(uint64_t *)(var_0 + (-104L)) = 4215833UL;
    indirect_placeholder_1();
    *(uint64_t *)6442032UL = storemerge;
    var_12 = var_0 + (-112L);
    *(uint64_t *)var_12 = 4215850UL;
    indirect_placeholder_1();
    var_13 = (uint32_t *)(var_0 + (-52L));
    local_sp_5 = var_12;
    while (1U)
        {
            var_14 = *var_5;
            var_15 = (uint64_t)*var_3;
            var_16 = local_sp_5 + (-8L);
            *(uint64_t *)var_16 = 4216112UL;
            var_17 = indirect_placeholder_89(4325626UL, 4319168UL, var_15, var_14, 0UL);
            var_18 = (uint32_t)var_17.field_0;
            *var_13 = var_18;
            local_sp_4 = var_16;
            local_sp_5 = var_16;
            if (var_18 != 4294967295U) {
                if ((uint64_t)(*var_3 - *(uint32_t *)6441784UL) == 0UL) {
                    var_48 = var_17.field_3;
                    var_49 = var_17.field_2;
                    var_50 = var_17.field_1;
                    *(uint64_t *)(local_sp_5 + (-16L)) = 4216161UL;
                    indirect_placeholder_12(0UL, 4325632UL, var_50, 0UL, 0UL, var_49, var_48);
                    *(uint64_t *)(local_sp_5 + (-24L)) = 4216171UL;
                    indirect_placeholder_2(var_2, 1UL);
                    abort();
                }
                var_21 = *var_8;
                if (var_21 == 0UL) {
                    var_22 = local_sp_5 + (-16L);
                    *(uint64_t *)var_22 = 4216195UL;
                    var_23 = indirect_placeholder(var_21, 4325648UL);
                    local_sp_1 = var_22;
                    if (var_23 != 0UL) {
                        loop_state_var = 1U;
                        break;
                    }
                    var_24 = local_sp_5 + (-24L);
                    *(uint64_t *)var_24 = 4216205UL;
                    indirect_placeholder_1();
                    local_sp_1 = var_24;
                    loop_state_var = 1U;
                    break;
                }
                var_27 = (uint64_t)*var_7;
                var_28 = (uint64_t)*var_6;
                *(uint64_t *)(local_sp_5 + (-16L)) = 4216237UL;
                var_29 = indirect_placeholder_10(0UL, var_28, var_27);
                *var_8 = var_29;
                var_30 = (uint64_t)*var_7;
                var_31 = (uint64_t)*var_6;
                var_32 = local_sp_5 + (-24L);
                *(uint64_t *)var_32 = 4216263UL;
                var_33 = indirect_placeholder_10(1UL, var_31, var_30);
                var_34 = (uint64_t *)(var_0 + (-32L));
                *var_34 = var_33;
                _pre_phi133 = var_34;
                local_sp_2 = var_32;
                loop_state_var = 0U;
                break;
            }
            if ((uint64_t)(var_18 + (-99)) == 0UL) {
                *var_8 = *(uint64_t *)6444832UL;
                *(unsigned char *)6442017UL = (unsigned char)'\x00';
                *(uint64_t *)6441632UL = 4325609UL;
                continue;
            }
            if ((int)var_18 > (int)99U) {
                if ((uint64_t)(var_18 + (-116)) == 0UL) {
                    *var_7 = (unsigned char)'\x01';
                    continue;
                }
                if ((uint64_t)(var_18 + (-128)) != 0UL) {
                    if ((uint64_t)(var_18 + (-102)) != 0UL) {
                        loop_state_var = 2U;
                        break;
                    }
                    *var_6 = (unsigned char)'\x01';
                    continue;
                }
                *var_8 = *(uint64_t *)6444832UL;
                *(unsigned char *)6442017UL = (unsigned char)'\x01';
                *(uint64_t *)6441632UL = 4318787UL;
                continue;
            }
            if ((uint64_t)(var_18 + 130U) == 0UL) {
                *(uint64_t *)(local_sp_5 + (-16L)) = 4216010UL;
                indirect_placeholder_2(var_2, 0UL);
                abort();
            }
            if ((uint64_t)(var_18 + (-76)) == 0UL) {
                *(unsigned char *)6442016UL = (unsigned char)'\x01';
                continue;
            }
            if (var_18 != 4294967165U) {
                loop_state_var = 2U;
                break;
            }
            var_19 = *(uint64_t *)6441640UL;
            *(uint64_t *)(local_sp_5 + (-16L)) = 4216062UL;
            indirect_placeholder_88(0UL, 4318624UL, var_19, 4325404UL, 0UL, 4325611UL);
            var_20 = local_sp_5 + (-24L);
            *(uint64_t *)var_20 = 4216072UL;
            indirect_placeholder_1();
            local_sp_4 = var_20;
            loop_state_var = 2U;
            break;
        }
    switch (loop_state_var) {
      case 2U:
        {
            *(uint64_t *)(local_sp_4 + (-8L)) = 4216082UL;
            indirect_placeholder_2(var_2, 1UL);
            abort();
        }
        break;
      case 1U:
      case 0U:
        {
            switch (loop_state_var) {
              case 1U:
                {
                    var_25 = *var_8;
                    var_26 = (uint64_t *)(var_0 + (-32L));
                    *var_26 = var_25;
                    _pre_phi133 = var_26;
                    local_sp_2 = local_sp_1;
                }
                break;
              case 0U:
                {
                    var_35 = *(uint32_t *)6441784UL;
                    var_36 = (uint32_t *)(var_0 + (-40L));
                    *var_36 = var_35;
                    var_37 = var_35;
                    local_sp_3 = local_sp_2;
                    while ((long)((uint64_t)var_37 << 32UL) >= (long)((uint64_t)*var_3 << 32UL))
                        {
                            var_38 = (*var_6 == '\x00');
                            var_39 = *(uint64_t *)(*var_5 + ((uint64_t)var_37 << 3UL));
                            if (var_38) {
                                var_43 = *_pre_phi133;
                                var_44 = *var_8;
                                var_45 = local_sp_3 + (-8L);
                                *(uint64_t *)var_45 = 4216369UL;
                                var_46 = indirect_placeholder_10(var_43, var_39, var_44);
                                local_sp_0 = var_45;
                                storemerge7_in = var_46;
                            } else {
                                var_40 = *var_8;
                                var_41 = local_sp_3 + (-8L);
                                *(uint64_t *)var_41 = 4216322UL;
                                var_42 = indirect_placeholder(var_39, var_40);
                                local_sp_0 = var_41;
                                storemerge7_in = var_42;
                            }
                            *var_9 = ((storemerge7_in & (uint64_t)*var_9) != 0UL);
                            var_47 = *var_36 + 1U;
                            *var_36 = var_47;
                            var_37 = var_47;
                            local_sp_3 = local_sp_0;
                        }
                    return;
                }
                break;
            }
        }
        break;
    }
}

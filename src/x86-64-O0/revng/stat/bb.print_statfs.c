typedef unsigned char __u_char;
typedef unsigned short __u_short;
typedef unsigned int __u_int;
typedef unsigned long __u_long;
typedef signed char __int8_t;
typedef unsigned char __uint8_t;
typedef short __int16_t;
typedef unsigned short __uint16_t;
typedef int __int32_t;
typedef unsigned int __uint32_t;
typedef long __int64_t;
typedef unsigned long __uint64_t;
typedef __int8_t __int_least8_t;
typedef __uint8_t __uint_least8_t;
typedef __int16_t __int_least16_t;
typedef __uint16_t __uint_least16_t;
typedef __int32_t __int_least32_t;
typedef __uint32_t __uint_least32_t;
typedef __int64_t __int_least64_t;
typedef __uint64_t __uint_least64_t;
typedef long __quad_t;
typedef unsigned long __u_quad_t;
typedef long __intmax_t;
typedef unsigned long __uintmax_t;
typedef unsigned long __dev_t;
typedef unsigned int __uid_t;
typedef unsigned int __gid_t;
typedef unsigned long __ino_t;
typedef unsigned long __ino64_t;
typedef unsigned int __mode_t;
typedef unsigned long __nlink_t;
typedef long __off_t;
typedef long __off64_t;
typedef int __pid_t;
typedef struct {
    int __val[2];
} __fsid_t;
typedef long __clock_t;
typedef unsigned long __rlim_t;
typedef unsigned long __rlim64_t;
typedef unsigned int __id_t;
typedef long __time_t;
typedef unsigned int __useconds_t;
typedef long __suseconds_t;
typedef int __daddr_t;
typedef int __key_t;
typedef int __clockid_t;
typedef void *__timer_t;
typedef long __blksize_t;
typedef long __blkcnt_t;
typedef long __blkcnt64_t;
typedef unsigned long __fsblkcnt_t;
typedef unsigned long __fsblkcnt64_t;
typedef unsigned long __fsfilcnt_t;
typedef unsigned long __fsfilcnt64_t;
typedef long __fsword_t;
typedef long __ssize_t;
typedef long __syscall_slong_t;
typedef unsigned long __syscall_ulong_t;
typedef __off64_t __loff_t;
typedef char *__caddr_t;
typedef long __intptr_t;
typedef unsigned int __socklen_t;
typedef int __sig_atomic_t;
typedef __int8_t int8_t;
typedef __int16_t int16_t;
typedef __int32_t int32_t;
typedef __int64_t int64_t;
typedef __uint8_t uint8_t;
typedef __uint16_t uint16_t;
typedef __uint32_t uint32_t;
typedef __uint64_t uint64_t;
typedef __int_least8_t int_least8_t;
typedef __int_least16_t int_least16_t;
typedef __int_least32_t int_least32_t;
typedef __int_least64_t int_least64_t;
typedef __uint_least8_t uint_least8_t;
typedef __uint_least16_t uint_least16_t;
typedef __uint_least32_t uint_least32_t;
typedef __uint_least64_t uint_least64_t;
typedef signed char int_fast8_t;
typedef long int_fast16_t;
typedef long int_fast32_t;
typedef long int_fast64_t;
typedef unsigned char uint_fast8_t;
typedef unsigned long uint_fast16_t;
typedef unsigned long uint_fast32_t;
typedef unsigned long uint_fast64_t;
typedef long intptr_t;
typedef unsigned long uintptr_t;
typedef __intmax_t intmax_t;
typedef __uintmax_t uintmax_t;
extern void abort(void);
extern uint64_t revng_init_local_sp(uint64_t param_0);
extern uint64_t init_rbp(void);
extern void indirect_placeholder_1(void);
extern void indirect_placeholder_32(uint64_t param_0);
extern uint64_t indirect_placeholder_13(uint64_t param_0);
uint64_t bb_print_statfs(uint64_t rdx, uint64_t rcx, uint64_t rdi, uint64_t rsi, uint64_t r9, uint64_t r8) {
    uint64_t var_0;
    uint64_t var_1;
    uint64_t *var_2;
    uint32_t *var_3;
    uint64_t *var_4;
    unsigned char *var_5;
    uint32_t var_6;
    uint64_t var_7;
    uint64_t var_8;
    uint64_t var_9;
    uint64_t *var_10;
    uint64_t var_11;
    uint64_t var_12;
    uint64_t var_13;
    uint64_t var_14;
    uint64_t var_15;
    uint64_t var_16;
    uint64_t var_17;
    uint64_t var_18;
    uint64_t var_19;
    uint64_t var_20;
    uint64_t *var_21;
    uint64_t *var_22;
    uint32_t *var_23;
    uint32_t *var_24;
    uint64_t *var_25;
    uint32_t var_26;
    uint64_t var_27;
    uint32_t var_28;
    uint64_t var_30;
    uint32_t var_31;
    uint64_t var_29;
    uint64_t var_32;
    var_0 = revng_init_local_sp(0UL);
    var_1 = init_rbp();
    *(uint64_t *)(var_0 + (-8L)) = var_1;
    *(uint64_t *)(var_0 + (-80L)) = rdi;
    var_2 = (uint64_t *)(var_0 + (-88L));
    *var_2 = rsi;
    var_3 = (uint32_t *)(var_0 + (-92L));
    *var_3 = (uint32_t)rdx;
    *(uint32_t *)(var_0 + (-96L)) = (uint32_t)rcx;
    *(uint64_t *)(var_0 + (-104L)) = r8;
    *(uint64_t *)(var_0 + (-112L)) = r9;
    var_4 = (uint64_t *)(var_0 + (-40L));
    *var_4 = r9;
    var_5 = (unsigned char *)(var_0 + (-41L));
    *var_5 = (unsigned char)'\x00';
    var_6 = *var_3 + (-83);
    var_26 = 0U;
    if (var_6 <= 33U) {
        *(uint64_t *)(var_0 + (-128L)) = 4210405UL;
        indirect_placeholder_1();
        return (uint64_t)*var_5;
    }
    switch (*(uint64_t *)(((uint64_t)var_6 << 3UL) + 4320368UL)) {
      case 4210353UL:
        {
            var_7 = *var_2;
            *(uint64_t *)(var_0 + (-128L)) = 4210383UL;
            indirect_placeholder_13(var_7);
        }
        break;
      case 4210201UL:
        {
            var_13 = *var_2;
            *(uint64_t *)(var_0 + (-128L)) = 4210231UL;
            indirect_placeholder_13(var_13);
        }
        break;
      case 4210166UL:
        {
            var_14 = *var_2;
            *(uint64_t *)(var_0 + (-128L)) = 4210196UL;
            indirect_placeholder_13(var_14);
        }
        break;
      case 4210029UL:
        {
            var_19 = *var_2;
            *(uint64_t *)(var_0 + (-128L)) = 4210056UL;
            indirect_placeholder_32(var_19);
        }
        break;
      case 4210236UL:
        {
            var_12 = *var_2;
            *(uint64_t *)(var_0 + (-128L)) = 4210263UL;
            indirect_placeholder_32(var_12);
        }
        break;
      case 4210385UL:
        {
            *(uint64_t *)(var_0 + (-128L)) = 4210405UL;
            indirect_placeholder_1();
        }
        break;
      case 4209854UL:
        {
            var_32 = *var_2;
            *(uint64_t *)(var_0 + (-128L)) = 4209877UL;
            indirect_placeholder_32(var_32);
        }
        break;
      case 4210324UL:
        {
            var_8 = *var_2;
            *(uint64_t *)(var_0 + (-128L)) = 4210351UL;
            indirect_placeholder_32(var_8);
        }
        break;
      case 4210131UL:
        {
            var_15 = *var_2;
            *(uint64_t *)(var_0 + (-128L)) = 4210161UL;
            indirect_placeholder_13(var_15);
        }
        break;
      case 4210061UL:
        {
            var_18 = *var_2;
            *(uint64_t *)(var_0 + (-128L)) = 4210087UL;
            indirect_placeholder_32(var_18);
        }
        break;
      case 4210092UL:
        {
            var_16 = *var_4;
            *(uint64_t *)(var_0 + (-128L)) = 4210104UL;
            indirect_placeholder_13(var_16);
            var_17 = *var_2;
            *(uint64_t *)(var_0 + (-136L)) = 4210126UL;
            indirect_placeholder_32(var_17);
        }
        break;
      case 4209882UL:
        {
            var_20 = *var_4 + 56UL;
            var_21 = (uint64_t *)(var_0 + (-56L));
            *var_21 = var_20;
            var_22 = (uint64_t *)(var_0 + (-16L));
            *var_22 = 0UL;
            var_23 = (uint32_t *)(var_0 + (-60L));
            *var_23 = 2U;
            var_24 = (uint32_t *)(var_0 + (-20L));
            *var_24 = 0U;
            var_25 = (uint64_t *)(var_0 + (-72L));
            var_27 = (uint64_t)var_26;
            var_28 = *var_23;
            while ((long)(var_27 << 32UL) >= (long)((uint64_t)var_28 << 32UL))
                {
                    if (((uint64_t)var_26 & 4611686018427387902UL) == 0UL) {
                        break;
                    }
                    var_30 = (uint64_t)*(uint32_t *)(*var_21 + (uint64_t)((long)((uint64_t)((var_28 + (-1)) - var_26) << 32UL) >> (long)30UL));
                    *var_25 = var_30;
                    *var_22 = (*var_22 | (var_30 << (((uint64_t)*var_24 << 5UL) & 32UL)));
                    var_31 = *var_24 + 1U;
                    *var_24 = var_31;
                    var_26 = var_31;
                    var_27 = (uint64_t)var_26;
                    var_28 = *var_23;
                }
            var_29 = *var_2;
            *(uint64_t *)(var_0 + (-128L)) = 4210024UL;
            indirect_placeholder_32(var_29);
        }
        break;
      case 4210268UL:
        {
            var_9 = *(uint64_t *)(*var_4 + 72UL);
            var_10 = (uint64_t *)(var_0 + (-32L));
            *var_10 = var_9;
            if (var_9 == 0UL) {
                *var_10 = *(uint64_t *)(*var_4 + 8UL);
            }
            var_11 = *var_2;
            *(uint64_t *)(var_0 + (-128L)) = 4210322UL;
            indirect_placeholder_32(var_11);
        }
        break;
      default:
        {
            abort();
        }
        break;
    }
    return (uint64_t)*var_5;
}

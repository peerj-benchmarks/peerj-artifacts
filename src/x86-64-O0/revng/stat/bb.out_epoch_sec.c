typedef unsigned char __u_char;
typedef unsigned short __u_short;
typedef unsigned int __u_int;
typedef unsigned long __u_long;
typedef signed char __int8_t;
typedef unsigned char __uint8_t;
typedef short __int16_t;
typedef unsigned short __uint16_t;
typedef int __int32_t;
typedef unsigned int __uint32_t;
typedef long __int64_t;
typedef unsigned long __uint64_t;
typedef __int8_t __int_least8_t;
typedef __uint8_t __uint_least8_t;
typedef __int16_t __int_least16_t;
typedef __uint16_t __uint_least16_t;
typedef __int32_t __int_least32_t;
typedef __uint32_t __uint_least32_t;
typedef __int64_t __int_least64_t;
typedef __uint64_t __uint_least64_t;
typedef long __quad_t;
typedef unsigned long __u_quad_t;
typedef long __intmax_t;
typedef unsigned long __uintmax_t;
typedef unsigned long __dev_t;
typedef unsigned int __uid_t;
typedef unsigned int __gid_t;
typedef unsigned long __ino_t;
typedef unsigned long __ino64_t;
typedef unsigned int __mode_t;
typedef unsigned long __nlink_t;
typedef long __off_t;
typedef long __off64_t;
typedef int __pid_t;
typedef struct {
    int __val[2];
} __fsid_t;
typedef long __clock_t;
typedef unsigned long __rlim_t;
typedef unsigned long __rlim64_t;
typedef unsigned int __id_t;
typedef long __time_t;
typedef unsigned int __useconds_t;
typedef long __suseconds_t;
typedef int __daddr_t;
typedef int __key_t;
typedef int __clockid_t;
typedef void *__timer_t;
typedef long __blksize_t;
typedef long __blkcnt_t;
typedef long __blkcnt64_t;
typedef unsigned long __fsblkcnt_t;
typedef unsigned long __fsblkcnt64_t;
typedef unsigned long __fsfilcnt_t;
typedef unsigned long __fsfilcnt64_t;
typedef long __fsword_t;
typedef long __ssize_t;
typedef long __syscall_slong_t;
typedef unsigned long __syscall_ulong_t;
typedef __off64_t __loff_t;
typedef char *__caddr_t;
typedef long __intptr_t;
typedef unsigned int __socklen_t;
typedef int __sig_atomic_t;
typedef __int8_t int8_t;
typedef __int16_t int16_t;
typedef __int32_t int32_t;
typedef __int64_t int64_t;
typedef __uint8_t uint8_t;
typedef __uint16_t uint16_t;
typedef __uint32_t uint32_t;
typedef __uint64_t uint64_t;
typedef __int_least8_t int_least8_t;
typedef __int_least16_t int_least16_t;
typedef __int_least32_t int_least32_t;
typedef __int_least64_t int_least64_t;
typedef __uint_least8_t uint_least8_t;
typedef __uint_least16_t uint_least16_t;
typedef __uint_least32_t uint_least32_t;
typedef __uint_least64_t uint_least64_t;
typedef signed char int_fast8_t;
typedef long int_fast16_t;
typedef long int_fast32_t;
typedef long int_fast64_t;
typedef unsigned char uint_fast8_t;
typedef unsigned long uint_fast16_t;
typedef unsigned long uint_fast32_t;
typedef unsigned long uint_fast64_t;
typedef long intptr_t;
typedef unsigned long uintptr_t;
typedef __intmax_t intmax_t;
typedef __uintmax_t uintmax_t;
typedef _Bool bool;
struct indirect_placeholder_63_ret_type;
struct helper_idivq_EAX_wrapper_ret_type;
struct type_6;
struct helper_idivl_EAX_wrapper_ret_type;
struct indirect_placeholder_63_ret_type {
    uint64_t field_0;
    uint64_t field_1;
    uint64_t field_2;
};
struct helper_idivq_EAX_wrapper_ret_type {
    uint64_t field_0;
    uint64_t field_1;
    uint64_t field_2;
    uint32_t field_3;
    uint64_t field_4;
    uint64_t field_5;
    uint32_t field_6;
    uint64_t field_7;
    uint32_t field_8;
    uint32_t field_9;
    unsigned char field_10;
    uint32_t field_11;
    uint32_t field_12;
};
struct type_6 {
};
struct helper_idivl_EAX_wrapper_ret_type {
    uint64_t field_0;
    uint64_t field_1;
    uint64_t field_2;
    uint32_t field_3;
    uint64_t field_4;
    uint64_t field_5;
    uint32_t field_6;
    uint64_t field_7;
    uint32_t field_8;
    uint32_t field_9;
    unsigned char field_10;
    uint32_t field_11;
    uint32_t field_12;
};
extern uint64_t revng_init_local_sp(uint64_t param_0);
extern uint64_t init_rbp(void);
extern uint64_t init_cc_src2(void);
extern uint64_t helper_cc_compute_c_wrapper(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint32_t param_3);
extern void indirect_placeholder_1(void);
extern uint64_t init_rbx(void);
extern uint64_t init_r10(void);
extern uint64_t init_state_0x9018(void);
extern uint32_t init_state_0x9010(void);
extern uint64_t init_state_0x8408(void);
extern uint64_t init_state_0x8328(void);
extern uint64_t init_state_0x82d8(void);
extern uint32_t init_state_0x9080(void);
extern uint32_t init_state_0x8248(void);
extern uint64_t init_r9(void);
extern uint64_t indirect_placeholder_13(uint64_t param_0);
extern struct indirect_placeholder_63_ret_type indirect_placeholder_63(uint64_t param_0, uint64_t param_1, uint64_t param_2);
extern struct helper_idivq_EAX_wrapper_ret_type helper_idivq_EAX_wrapper(struct type_6 *param_0, uint64_t param_1, uint64_t param_2, uint64_t param_3, uint64_t param_4, uint64_t param_5, uint64_t param_6, uint64_t param_7, uint64_t param_8, uint64_t param_9, uint64_t param_10, uint64_t param_11, uint64_t param_12, uint64_t param_13, uint32_t param_14, uint64_t param_15, uint64_t param_16, uint64_t param_17, uint32_t param_18, uint32_t param_19);
extern struct helper_idivl_EAX_wrapper_ret_type helper_idivl_EAX_wrapper(struct type_6 *param_0, uint64_t param_1, uint64_t param_2, uint64_t param_3, uint64_t param_4, uint64_t param_5, uint64_t param_6, uint64_t param_7, uint64_t param_8, uint64_t param_9, uint64_t param_10, uint64_t param_11, uint64_t param_12, uint64_t param_13, uint32_t param_14, uint64_t param_15, uint64_t param_16, uint64_t param_17, uint32_t param_18, uint32_t param_19);
void bb_out_epoch_sec(uint64_t rdx, uint64_t rcx, uint64_t rdi, uint64_t rsi, uint64_t r8) {
    uint64_t var_0;
    uint64_t var_1;
    uint64_t var_2;
    uint64_t var_3;
    uint64_t var_4;
    uint64_t var_5;
    uint64_t var_6;
    uint32_t var_7;
    uint64_t var_8;
    uint64_t var_9;
    uint64_t var_10;
    uint32_t var_11;
    uint32_t var_12;
    uint64_t var_13;
    uint64_t *var_14;
    uint64_t *var_15;
    uint64_t *var_16;
    uint64_t *var_17;
    uint64_t var_18;
    uint64_t var_19;
    uint64_t var_20;
    struct indirect_placeholder_63_ret_type var_21;
    uint64_t var_22;
    uint64_t var_23;
    uint64_t var_24;
    uint64_t var_25;
    uint64_t *var_26;
    uint64_t var_27;
    uint64_t *var_28;
    uint32_t *var_29;
    uint32_t *var_30;
    unsigned char *var_31;
    uint64_t var_32;
    uint64_t rdi3_2;
    uint64_t rax_0;
    uint32_t *var_101;
    uint32_t var_102;
    uint32_t narrow;
    uint32_t *var_103;
    uint32_t var_104;
    uint32_t narrow178;
    uint64_t var_105;
    uint64_t var_106;
    uint32_t storemerge8;
    uint64_t var_107;
    uint64_t var_108;
    uint64_t var_109;
    uint64_t var_44;
    uint32_t var_45;
    uint64_t var_46;
    uint64_t var_47;
    uint32_t storemerge9;
    uint64_t var_38;
    uint32_t var_48;
    uint64_t var_49;
    uint64_t *var_50;
    uint64_t var_51;
    uint64_t *var_52;
    uint64_t var_53;
    uint64_t var_54;
    uint64_t var_55;
    uint64_t var_59;
    uint64_t var_60;
    uint64_t rdi3_0;
    uint64_t var_61;
    uint64_t rsi4_0;
    uint64_t local_sp_1;
    unsigned char **var_56;
    uint64_t var_57;
    uint64_t var_58;
    uint64_t var_33;
    uint64_t var_34;
    uint32_t var_35;
    uint32_t var_36;
    uint64_t rdi3_1;
    uint64_t rsi4_1;
    uint64_t local_sp_2;
    uint64_t var_37;
    uint64_t *var_39;
    uint64_t var_40;
    uint64_t var_41;
    uint64_t var_42;
    uint32_t var_43;
    uint64_t rsi4_2;
    uint64_t local_sp_3;
    uint32_t *var_62;
    uint32_t var_63;
    uint32_t *var_64;
    uint32_t var_65;
    uint64_t var_67;
    uint64_t var_68;
    struct helper_idivq_EAX_wrapper_ret_type var_69;
    uint64_t var_70;
    uint64_t var_71;
    uint32_t var_72;
    uint64_t var_73;
    uint32_t var_74;
    uint32_t var_75;
    uint32_t *var_76;
    unsigned char *var_77;
    unsigned char var_94;
    uint64_t var_79;
    uint32_t var_80;
    uint64_t var_81;
    struct helper_idivl_EAX_wrapper_ret_type var_78;
    uint32_t var_82;
    uint32_t var_83;
    uint32_t *var_84;
    uint32_t var_85;
    uint32_t var_86;
    uint64_t var_87;
    uint64_t var_88;
    uint64_t var_89;
    struct helper_idivq_EAX_wrapper_ret_type var_90;
    uint32_t var_91;
    uint64_t var_92;
    unsigned char var_93;
    bool var_95;
    uint64_t var_96;
    uint64_t var_97;
    uint64_t *var_98;
    uint64_t var_99;
    uint64_t var_100;
    uint32_t var_66;
    var_0 = revng_init_local_sp(0UL);
    var_1 = init_rbp();
    var_2 = init_cc_src2();
    var_3 = init_r10();
    var_4 = init_r9();
    var_5 = init_rbx();
    var_6 = init_state_0x9018();
    var_7 = init_state_0x9010();
    var_8 = init_state_0x8408();
    var_9 = init_state_0x8328();
    var_10 = init_state_0x82d8();
    var_11 = init_state_0x9080();
    var_12 = init_state_0x8248();
    var_13 = var_0 + (-8L);
    *(uint64_t *)var_13 = var_1;
    *(uint64_t *)(var_0 + (-16L)) = var_5;
    var_14 = (uint64_t *)(var_0 + (-160L));
    *var_14 = rdi;
    var_15 = (uint64_t *)(var_0 + (-168L));
    *var_15 = rsi;
    *(uint64_t *)(var_0 + (-176L)) = rdx;
    var_16 = (uint64_t *)(var_0 + (-200L));
    *var_16 = rcx;
    var_17 = (uint64_t *)(var_0 + (-192L));
    *var_17 = r8;
    var_18 = *var_15;
    var_19 = *var_14;
    var_20 = var_0 + (-208L);
    *(uint64_t *)var_20 = 4208543UL;
    var_21 = indirect_placeholder_63(var_18, var_19, 46UL);
    var_22 = var_21.field_0;
    var_23 = var_21.field_1;
    var_24 = var_21.field_2;
    var_25 = var_0 + (-96L);
    var_26 = (uint64_t *)var_25;
    *var_26 = var_22;
    var_27 = *var_15;
    var_28 = (uint64_t *)(var_0 + (-32L));
    *var_28 = var_27;
    var_29 = (uint32_t *)(var_0 + (-36L));
    *var_29 = 0U;
    var_30 = (uint32_t *)(var_0 + (-40L));
    *var_30 = 0U;
    var_31 = (unsigned char *)(var_0 + (-41L));
    *var_31 = (unsigned char)'\x00';
    var_32 = *var_26;
    rdi3_2 = var_23;
    storemerge8 = 0U;
    rsi4_0 = 0UL;
    var_36 = 9U;
    rdi3_1 = var_23;
    rsi4_1 = var_24;
    local_sp_2 = var_20;
    rsi4_2 = var_24;
    local_sp_3 = var_20;
    var_94 = (unsigned char)'\x00';
    if (var_32 != 0UL) {
        *var_28 = (var_32 - *var_14);
        *(unsigned char *)(*var_15 + *var_14) = (unsigned char)'\x00';
        var_33 = *var_26 + 1UL;
        if ((uint64_t)(((uint32_t)(uint64_t)*(unsigned char *)var_33 + (-48)) & (-2)) > 9UL) {
            *var_30 = 9U;
        } else {
            var_34 = var_0 + (-216L);
            *(uint64_t *)var_34 = 4208676UL;
            indirect_placeholder_1();
            *(uint64_t *)(var_0 + (-104L)) = var_33;
            var_35 = (uint32_t)(((long)var_33 > (long)2147483647UL) ? 2147483647UL : var_33);
            *var_30 = var_35;
            var_36 = var_35;
            rdi3_1 = var_33;
            rsi4_1 = 0UL;
            local_sp_2 = var_34;
        }
        rdi3_2 = rdi3_1;
        rsi4_2 = rsi4_1;
        local_sp_3 = local_sp_2;
        var_37 = *var_26;
        if (var_36 != 0U & (uint64_t)(((uint32_t)(uint64_t)*(unsigned char *)(var_37 + (-1L)) + (-48)) & (-2)) <= 9UL) {
            var_38 = var_0 + (-56L);
            var_39 = (uint64_t *)var_38;
            *var_39 = var_37;
            **(unsigned char **)var_25 = (unsigned char)'\x00';
            var_40 = *var_39;
            rsi4_2 = 0UL;
            var_41 = var_40 + (-1L);
            *var_39 = var_41;
            rdi3_0 = var_41;
            var_40 = var_41;
            rdi3_2 = var_41;
            do {
                var_41 = var_40 + (-1L);
                *var_39 = var_41;
                rdi3_0 = var_41;
                var_40 = var_41;
                rdi3_2 = var_41;
            } while ((uint64_t)(((uint32_t)(uint64_t)*(unsigned char *)(var_41 + (-1L)) + (-48)) & (-2)) <= 9UL);
            var_42 = local_sp_2 + (-8L);
            *(uint64_t *)var_42 = 4208810UL;
            indirect_placeholder_1();
            *(uint64_t *)(var_0 + (-112L)) = var_41;
            var_43 = (uint32_t)(((long)var_41 > (long)2147483647UL) ? 2147483647UL : var_41);
            *var_29 = var_43;
            local_sp_1 = var_42;
            local_sp_3 = var_42;
            var_44 = *var_39 + (**(unsigned char **)var_38 == '0');
            *var_39 = var_44;
            *var_28 = (var_44 - *var_14);
            var_45 = *var_29;
            var_46 = (uint64_t)var_45;
            var_47 = *(uint64_t *)6442032UL;
            storemerge9 = (var_47 < var_46) ? (var_45 - (uint32_t)var_47) : 0U;
            *(uint32_t *)(var_0 + (-116L)) = storemerge9;
            var_48 = storemerge9 - *var_30;
            *(uint32_t *)(var_0 + (-120L)) = var_48;
            if ((int)var_43 <= (int)1U & (int)storemerge9 <= (int)1U & (int)var_48 <= (int)1U) {
                var_49 = *var_14;
                var_50 = (uint64_t *)(var_0 + (-64L));
                *var_50 = var_49;
                var_51 = var_0 + (-72L);
                var_52 = (uint64_t *)var_51;
                *var_52 = var_49;
                var_53 = var_49;
                var_54 = *var_39;
                var_55 = helper_cc_compute_c_wrapper(var_53 - var_54, var_54, var_2, 17U);
                while (var_55 != 0UL)
                    {
                        var_56 = (unsigned char **)var_51;
                        if (**var_56 == '-') {
                            *var_31 = (unsigned char)'\x01';
                        } else {
                            var_57 = *var_50;
                            *var_50 = (var_57 + 1UL);
                            *(unsigned char *)var_57 = **var_56;
                        }
                        var_58 = *var_52 + 1UL;
                        *var_52 = var_58;
                        var_53 = var_58;
                        var_54 = *var_39;
                        var_55 = helper_cc_compute_c_wrapper(var_53 - var_54, var_54, var_2, 17U);
                    }
                var_59 = *var_50;
                var_60 = var_59 - *var_14;
                if (*var_31 != '\x01') {
                    var_61 = local_sp_2 + (-16L);
                    *(uint64_t *)var_61 = 4209081UL;
                    indirect_placeholder_1();
                    rdi3_0 = var_59;
                    rsi4_0 = 4320308UL;
                    local_sp_1 = var_61;
                }
                *var_28 = var_60;
                rdi3_2 = rdi3_0;
                rsi4_2 = rsi4_0;
                local_sp_3 = local_sp_1;
            }
        }
    }
    var_62 = (uint32_t *)(var_0 + (-76L));
    *var_62 = 1U;
    var_63 = *var_30;
    var_64 = (uint32_t *)(var_0 + (-80L));
    *var_64 = var_63;
    var_65 = var_63;
    while ((int)var_65 <= (int)8U)
        {
            *var_62 = (*var_62 * 10U);
            var_66 = *var_64 + 1U;
            *var_64 = var_66;
            var_65 = var_66;
        }
    var_67 = *var_17;
    var_68 = (uint64_t)*var_62;
    var_69 = helper_idivq_EAX_wrapper((struct type_6 *)(0UL), var_68, 4209152UL, var_67, var_13, (uint64_t)((long)var_67 >> (long)63UL), r8, rdi3_2, rsi4_2, var_3, var_4, r8, var_68, var_6, var_7, var_8, var_9, var_10, var_11, var_12);
    var_70 = var_69.field_1;
    var_71 = var_69.field_5;
    var_72 = var_69.field_6;
    var_73 = var_69.field_7;
    var_74 = var_69.field_8;
    var_75 = var_69.field_9;
    var_76 = (uint32_t *)(var_0 + (-84L));
    *var_76 = (uint32_t)var_70;
    var_77 = (unsigned char *)(var_0 + (-85L));
    *var_77 = (unsigned char)'\x00';
    if ((long)*var_16 <= (long)18446744073709551615UL & *var_17 == 0UL) {
        var_78 = helper_idivl_EAX_wrapper((struct type_6 *)(0UL), (uint64_t)*var_62, 4209192UL, 1000000000UL, var_13, 0UL, r8, rdi3_2, rsi4_2, var_3, var_4, r8, var_68, var_71, var_72, var_8, var_9, var_73, var_74, var_75);
        var_79 = var_78.field_5;
        var_80 = var_78.field_6;
        var_81 = var_78.field_7;
        var_82 = var_78.field_8;
        var_83 = var_78.field_9;
        var_84 = (uint32_t *)(var_0 + (-124L));
        var_85 = (uint32_t)var_78.field_1;
        *var_84 = var_85;
        var_86 = var_85 - *var_76;
        var_87 = (uint64_t)var_86;
        var_88 = *var_17;
        var_89 = (uint64_t)*var_62;
        var_90 = helper_idivq_EAX_wrapper((struct type_6 *)(0UL), var_89, 4209221UL, var_88, var_13, (uint64_t)((long)var_88 >> (long)63UL), var_87, rdi3_2, var_89, var_3, var_4, r8, var_68, var_79, var_80, var_8, var_9, var_81, var_82, var_83);
        var_91 = var_86 + (uint32_t)(var_90.field_4 != 0UL);
        *var_76 = var_91;
        var_92 = *var_16 + (var_91 != 0U);
        *var_16 = var_92;
        var_93 = (var_92 == 0UL);
        *var_77 = var_93;
        var_94 = var_93;
    }
    var_95 = (var_94 == '\x00');
    var_96 = *var_28;
    var_97 = local_sp_3 + (-8L);
    var_98 = (uint64_t *)var_97;
    if (var_95) {
        *var_98 = 4209345UL;
        var_100 = indirect_placeholder_13(var_96);
        rax_0 = var_100;
    } else {
        *var_98 = 4209314UL;
        var_99 = indirect_placeholder_13(var_96);
        rax_0 = var_99;
    }
    var_101 = (uint32_t *)(var_0 + (-128L));
    *var_101 = (uint32_t)rax_0;
    var_102 = *var_30;
    if (var_102 == 0U) {
        return;
    }
    narrow = ((int)var_102 > (int)9U) ? 9U : var_102;
    var_103 = (uint32_t *)(var_0 + (-132L));
    *var_103 = narrow;
    *(uint32_t *)(var_0 + (-136L)) = (*var_30 - narrow);
    var_104 = *var_101;
    narrow178 = ((int)var_104 < (int)0U) ? 0U : var_104;
    *(uint32_t *)(var_0 + (-140L)) = narrow178;
    var_105 = (uint64_t)narrow178;
    var_106 = (uint64_t)*var_29;
    var_107 = var_106 - var_105;
    var_108 = (uint64_t)((long)(var_107 << 32UL) >> (long)32UL);
    var_109 = *(uint64_t *)6442032UL;
    if ((long)(var_105 << 32UL) >= (long)(var_106 << 32UL) & var_108 > var_109) {
        storemerge8 = ((uint32_t)var_107 - (uint32_t)var_109) - *var_103;
    }
    *(uint32_t *)(var_0 + (-144L)) = storemerge8;
    *(uint64_t *)(var_97 + (-16L)) = 0UL;
    *(uint64_t *)(var_97 + (-24L)) = 4209529UL;
    indirect_placeholder_1();
    return;
}

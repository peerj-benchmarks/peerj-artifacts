typedef unsigned char __u_char;
typedef unsigned short __u_short;
typedef unsigned int __u_int;
typedef unsigned long __u_long;
typedef signed char __int8_t;
typedef unsigned char __uint8_t;
typedef short __int16_t;
typedef unsigned short __uint16_t;
typedef int __int32_t;
typedef unsigned int __uint32_t;
typedef long __int64_t;
typedef unsigned long __uint64_t;
typedef __int8_t __int_least8_t;
typedef __uint8_t __uint_least8_t;
typedef __int16_t __int_least16_t;
typedef __uint16_t __uint_least16_t;
typedef __int32_t __int_least32_t;
typedef __uint32_t __uint_least32_t;
typedef __int64_t __int_least64_t;
typedef __uint64_t __uint_least64_t;
typedef long __quad_t;
typedef unsigned long __u_quad_t;
typedef long __intmax_t;
typedef unsigned long __uintmax_t;
typedef unsigned long __dev_t;
typedef unsigned int __uid_t;
typedef unsigned int __gid_t;
typedef unsigned long __ino_t;
typedef unsigned long __ino64_t;
typedef unsigned int __mode_t;
typedef unsigned long __nlink_t;
typedef long __off_t;
typedef long __off64_t;
typedef int __pid_t;
typedef struct {
    int __val[2];
} __fsid_t;
typedef long __clock_t;
typedef unsigned long __rlim_t;
typedef unsigned long __rlim64_t;
typedef unsigned int __id_t;
typedef long __time_t;
typedef unsigned int __useconds_t;
typedef long __suseconds_t;
typedef int __daddr_t;
typedef int __key_t;
typedef int __clockid_t;
typedef void *__timer_t;
typedef long __blksize_t;
typedef long __blkcnt_t;
typedef long __blkcnt64_t;
typedef unsigned long __fsblkcnt_t;
typedef unsigned long __fsblkcnt64_t;
typedef unsigned long __fsfilcnt_t;
typedef unsigned long __fsfilcnt64_t;
typedef long __fsword_t;
typedef long __ssize_t;
typedef long __syscall_slong_t;
typedef unsigned long __syscall_ulong_t;
typedef __off64_t __loff_t;
typedef char *__caddr_t;
typedef long __intptr_t;
typedef unsigned int __socklen_t;
typedef int __sig_atomic_t;
typedef __int8_t int8_t;
typedef __int16_t int16_t;
typedef __int32_t int32_t;
typedef __int64_t int64_t;
typedef __uint8_t uint8_t;
typedef __uint16_t uint16_t;
typedef __uint32_t uint32_t;
typedef __uint64_t uint64_t;
typedef __int_least8_t int_least8_t;
typedef __int_least16_t int_least16_t;
typedef __int_least32_t int_least32_t;
typedef __int_least64_t int_least64_t;
typedef __uint_least8_t uint_least8_t;
typedef __uint_least16_t uint_least16_t;
typedef __uint_least32_t uint_least32_t;
typedef __uint_least64_t uint_least64_t;
typedef signed char int_fast8_t;
typedef long int_fast16_t;
typedef long int_fast32_t;
typedef long int_fast64_t;
typedef unsigned char uint_fast8_t;
typedef unsigned long uint_fast16_t;
typedef unsigned long uint_fast32_t;
typedef unsigned long uint_fast64_t;
typedef long intptr_t;
typedef unsigned long uintptr_t;
typedef __intmax_t intmax_t;
typedef __uintmax_t uintmax_t;
struct indirect_placeholder_16_ret_type;
struct indirect_placeholder_14_ret_type;
struct indirect_placeholder_15_ret_type;
struct indirect_placeholder_16_ret_type {
    uint64_t field_0;
    uint64_t field_1;
    uint64_t field_2;
    uint64_t field_3;
};
struct indirect_placeholder_14_ret_type {
    uint64_t field_0;
    uint64_t field_1;
    uint64_t field_2;
};
struct indirect_placeholder_15_ret_type {
    uint64_t field_0;
    uint64_t field_1;
    uint64_t field_2;
};
extern uint64_t revng_init_local_sp(uint64_t param_0);
extern uint64_t init_rbp(void);
extern void indirect_placeholder_1(void);
extern uint64_t indirect_placeholder_13(uint64_t param_0);
extern void indirect_placeholder_12(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint64_t param_3, uint64_t param_4, uint64_t param_5, uint64_t param_6);
extern struct indirect_placeholder_16_ret_type indirect_placeholder_16(uint64_t param_0);
extern struct indirect_placeholder_14_ret_type indirect_placeholder_14(uint64_t param_0);
extern struct indirect_placeholder_15_ret_type indirect_placeholder_15(uint64_t param_0, uint64_t param_1, uint64_t param_2);
uint64_t bb_print_it(uint64_t rdx, uint64_t rcx, uint64_t rdi, uint64_t rsi, uint64_t r8) {
    unsigned char **var_28;
    unsigned char var_29;
    uint32_t var_30;
    uint64_t var_31;
    uint64_t var_32;
    uint64_t var_33;
    uint64_t var_0;
    uint64_t var_1;
    uint64_t *var_2;
    uint32_t *var_3;
    uint64_t *var_4;
    uint64_t *var_5;
    uint64_t *var_6;
    unsigned char *var_7;
    uint64_t var_8;
    uint64_t var_9;
    uint64_t var_10;
    struct indirect_placeholder_16_ret_type var_11;
    uint64_t var_12;
    uint64_t var_13;
    uint64_t var_14;
    uint64_t var_15;
    uint64_t *var_16;
    uint64_t var_17;
    uint64_t var_18;
    uint64_t *var_19;
    uint32_t *var_20;
    uint32_t *var_21;
    uint32_t *var_22;
    uint64_t *var_23;
    uint64_t var_24;
    uint64_t *var_25;
    uint32_t *var_26;
    uint64_t var_27;
    uint32_t var_46;
    unsigned char var_47;
    uint32_t rax_0_in;
    uint64_t rcx2_3;
    uint64_t var_48;
    unsigned char var_41;
    uint32_t rax_1_in;
    uint64_t var_62;
    uint64_t var_42;
    uint64_t var_43;
    uint64_t var_44;
    uint64_t var_45;
    uint64_t local_sp_3;
    uint64_t local_sp_2;
    uint64_t r9_3;
    uint64_t r9_2;
    uint64_t rcx2_1;
    uint64_t r85_3;
    uint64_t r85_2;
    uint64_t r9_1;
    uint64_t r85_1;
    uint64_t var_81;
    uint64_t var_76;
    uint64_t var_77;
    uint64_t var_78;
    uint64_t var_79;
    uint64_t var_80;
    uint64_t var_68;
    uint64_t var_69;
    struct indirect_placeholder_14_ret_type var_70;
    uint64_t var_71;
    uint64_t var_72;
    uint64_t var_73;
    uint64_t var_74;
    uint64_t rcx2_2;
    uint64_t var_75;
    uint64_t var_63;
    uint64_t var_64;
    uint64_t local_sp_4;
    uint64_t var_65;
    uint64_t var_66;
    uint64_t var_67;
    uint64_t local_sp_5;
    unsigned char var_34;
    unsigned char var_35;
    uint64_t var_36;
    unsigned char var_49;
    uint64_t var_37;
    uint64_t var_38;
    uint64_t var_39;
    uint64_t var_40;
    uint64_t local_sp_6;
    uint64_t var_56;
    uint64_t var_50;
    uint64_t var_51;
    struct indirect_placeholder_15_ret_type var_52;
    uint64_t var_53;
    uint64_t var_54;
    uint64_t var_55;
    uint64_t var_57;
    uint64_t var_58;
    uint64_t var_59;
    uint64_t var_60;
    unsigned char **var_61;
    var_0 = revng_init_local_sp(0UL);
    var_1 = init_rbp();
    *(uint64_t *)(var_0 + (-8L)) = var_1;
    var_2 = (uint64_t *)(var_0 + (-96L));
    *var_2 = rdi;
    var_3 = (uint32_t *)(var_0 + (-100L));
    *var_3 = (uint32_t)rsi;
    var_4 = (uint64_t *)(var_0 + (-112L));
    *var_4 = rdx;
    var_5 = (uint64_t *)(var_0 + (-120L));
    *var_5 = rcx;
    var_6 = (uint64_t *)(var_0 + (-128L));
    *var_6 = r8;
    var_7 = (unsigned char *)(var_0 + (-9L));
    *var_7 = (unsigned char)'\x00';
    var_8 = *var_2;
    *(uint64_t *)(var_0 + (-144L)) = 4213271UL;
    indirect_placeholder_1();
    var_9 = var_8 + 3UL;
    *(uint64_t *)(var_0 + (-56L)) = var_9;
    var_10 = var_0 + (-152L);
    *(uint64_t *)var_10 = 4213291UL;
    var_11 = indirect_placeholder_16(var_9);
    var_12 = var_11.field_0;
    var_13 = var_11.field_1;
    var_14 = var_11.field_2;
    var_15 = var_11.field_3;
    var_16 = (uint64_t *)(var_0 + (-64L));
    *var_16 = var_12;
    var_17 = *var_2;
    var_18 = var_0 + (-24L);
    var_19 = (uint64_t *)var_18;
    *var_19 = var_17;
    var_20 = (uint32_t *)(var_0 + (-36L));
    var_21 = (uint32_t *)(var_0 + (-40L));
    var_22 = (uint32_t *)(var_0 + (-44L));
    var_23 = (uint64_t *)(var_0 + (-72L));
    var_24 = var_0 + (-32L);
    var_25 = (uint64_t *)var_24;
    var_26 = (uint32_t *)(var_0 + (-76L));
    var_27 = var_17;
    rcx2_3 = var_13;
    r9_3 = var_14;
    r85_3 = var_15;
    local_sp_5 = var_10;
    var_28 = (unsigned char **)var_18;
    var_29 = **var_28;
    r9_2 = r9_3;
    rcx2_1 = rcx2_3;
    r85_2 = r85_3;
    r9_1 = r9_3;
    r85_1 = r85_3;
    local_sp_6 = local_sp_5;
    while (var_29 != '\x00')
        {
            var_30 = (uint32_t)(uint64_t)var_29;
            if ((uint64_t)(var_30 + (-37)) != 0UL) {
                if ((uint64_t)(var_30 + (-92)) == 0UL) {
                    var_31 = local_sp_5 + (-8L);
                    *(uint64_t *)var_31 = 4214275UL;
                    indirect_placeholder_1();
                    local_sp_2 = var_31;
                } else {
                    if (*(unsigned char *)6442017UL == '\x01') {
                        var_32 = local_sp_5 + (-8L);
                        *(uint64_t *)var_32 = 4213708UL;
                        indirect_placeholder_1();
                        local_sp_2 = var_32;
                    } else {
                        var_33 = var_27 + 1UL;
                        *var_19 = var_33;
                        var_34 = **var_28;
                        var_49 = var_34;
                        if (((signed char)var_34 <= '/') || ((signed char)var_34 > '7')) {
                            if (var_34 != 'x') {
                                var_37 = (uint64_t)(uint32_t)(uint64_t)*(unsigned char *)(var_33 + 1UL);
                                *(uint64_t *)(local_sp_5 + (-8L)) = 4213891UL;
                                var_38 = indirect_placeholder_13(var_37);
                                var_39 = (uint64_t)(unsigned char)var_38;
                                var_40 = local_sp_5 + (-16L);
                                *(uint64_t *)var_40 = 4213901UL;
                                indirect_placeholder_1();
                                local_sp_6 = var_40;
                                if (var_39 != 0UL) {
                                    var_41 = *(unsigned char *)(*var_19 + 1UL);
                                    if (((signed char)var_41 <= '`') || ((signed char)var_41 > 'f')) {
                                        rax_1_in = (uint32_t)var_41 + (-87);
                                    } else {
                                        if (((signed char)var_41 <= '@') || ((signed char)var_41 > 'F')) {
                                            rax_1_in = (uint32_t)var_41 + (-48);
                                        } else {
                                            rax_1_in = (uint32_t)var_41 + (-55);
                                        }
                                    }
                                    *var_22 = rax_1_in;
                                    var_42 = *var_19 + 1UL;
                                    *var_19 = var_42;
                                    var_43 = (uint64_t)(uint32_t)(uint64_t)*(unsigned char *)(var_42 + 1UL);
                                    *(uint64_t *)(local_sp_5 + (-24L)) = 4214053UL;
                                    var_44 = indirect_placeholder_13(var_43);
                                    var_45 = (uint64_t)(unsigned char)var_44;
                                    *(uint64_t *)(local_sp_5 + (-32L)) = 4214063UL;
                                    indirect_placeholder_1();
                                    if (var_45 != 0UL) {
                                        *var_19 = (*var_19 + 1UL);
                                        var_46 = *var_22 << 4U;
                                        var_47 = **var_28;
                                        if (((signed char)var_47 <= '`') || ((signed char)var_47 > 'f')) {
                                            rax_0_in = (uint32_t)var_47 + (-87);
                                        } else {
                                            if (((signed char)var_47 <= '@') || ((signed char)var_47 > 'F')) {
                                                rax_0_in = (uint32_t)var_47 + (-48);
                                            } else {
                                                rax_0_in = (uint32_t)var_47 + (-55);
                                            }
                                        }
                                        *var_22 = (rax_0_in + var_46);
                                    }
                                    var_48 = local_sp_5 + (-40L);
                                    *(uint64_t *)var_48 = 4214182UL;
                                    indirect_placeholder_1();
                                    local_sp_2 = var_48;
                                    var_81 = *var_19 + 1UL;
                                    *var_19 = var_81;
                                    var_27 = var_81;
                                    rcx2_3 = rcx2_1;
                                    r9_3 = r9_1;
                                    r85_3 = r85_1;
                                    local_sp_5 = local_sp_2;
                                    var_28 = (unsigned char **)var_18;
                                    var_29 = **var_28;
                                    r9_2 = r9_3;
                                    rcx2_1 = rcx2_3;
                                    r85_2 = r85_3;
                                    r9_1 = r9_3;
                                    r85_1 = r85_3;
                                    local_sp_6 = local_sp_5;
                                    continue;
                                }
                                var_49 = **var_28;
                            }
                            if (var_49 == '\x00') {
                                *(uint64_t *)(local_sp_6 + (-8L)) = 4214220UL;
                                indirect_placeholder_12(0UL, 4322048UL, rcx2_3, 0UL, 0UL, r9_3, r85_3);
                                var_56 = local_sp_6 + (-16L);
                                *(uint64_t *)var_56 = 4214230UL;
                                indirect_placeholder_1();
                                *var_19 = (*var_19 + (-1L));
                                local_sp_2 = var_56;
                            } else {
                                var_50 = (uint64_t)(uint32_t)(uint64_t)var_49;
                                var_51 = local_sp_6 + (-8L);
                                *(uint64_t *)var_51 = 4214254UL;
                                var_52 = indirect_placeholder_15(var_50, r9_3, r85_3);
                                var_53 = var_52.field_0;
                                var_54 = var_52.field_1;
                                var_55 = var_52.field_2;
                                local_sp_2 = var_51;
                                rcx2_1 = var_53;
                                r9_1 = var_54;
                                r85_1 = var_55;
                            }
                        } else {
                            *var_20 = ((uint32_t)var_34 + (-48));
                            *var_21 = 1U;
                            *var_19 = (*var_19 + 1UL);
                            while ((int)*var_21 <= (int)2U)
                                {
                                    var_35 = **var_28;
                                    if (((signed char)var_35 <= '/') || ((signed char)var_35 > '7')) {
                                        break;
                                    }
                                    *var_20 = (((uint32_t)var_35 + (-48)) + (*var_20 << 3U));
                                    *var_21 = (*var_21 + 1U);
                                    *var_19 = (*var_19 + 1UL);
                                }
                            var_36 = local_sp_5 + (-8L);
                            *(uint64_t *)var_36 = 4213845UL;
                            indirect_placeholder_1();
                            *var_19 = (*var_19 + (-1L));
                            local_sp_2 = var_36;
                        }
                    }
                }
                var_81 = *var_19 + 1UL;
                *var_19 = var_81;
                var_27 = var_81;
                rcx2_3 = rcx2_1;
                r9_3 = r9_1;
                r85_3 = r85_1;
                local_sp_5 = local_sp_2;
                var_28 = (unsigned char **)var_18;
                var_29 = **var_28;
                r9_2 = r9_3;
                rcx2_1 = rcx2_3;
                r85_2 = r85_3;
                r9_1 = r9_3;
                r85_1 = r85_3;
                local_sp_6 = local_sp_5;
                continue;
            }
            var_57 = var_27 + 1UL;
            *(uint64_t *)(local_sp_5 + (-8L)) = 4213358UL;
            indirect_placeholder_1();
            *var_23 = var_57;
            var_58 = *var_19 + (var_57 + 1UL);
            *var_25 = var_58;
            var_59 = local_sp_5 + (-16L);
            *(uint64_t *)var_59 = 4213398UL;
            indirect_placeholder_1();
            var_60 = *var_25 + var_58;
            *var_25 = var_60;
            var_61 = (unsigned char **)var_24;
            var_64 = var_60;
            local_sp_4 = var_59;
            if (**var_61 == '.') {
                var_62 = local_sp_5 + (-24L);
                *(uint64_t *)var_62 = 4213434UL;
                indirect_placeholder_1();
                var_63 = *var_25 + (var_60 + 2UL);
                *var_25 = var_63;
                var_64 = var_63;
                local_sp_4 = var_62;
            }
            switch (*var_26) {
              case 0U:
                {
                    *var_19 = (var_67 + (-1L));
                }
                break;
              case 37U:
                {
                    var_68 = *var_23;
                    if (var_68 != 0UL) {
                        *(unsigned char *)((var_68 + 1UL) + *var_16) = **var_61;
                        *(unsigned char *)(*var_16 + (*var_23 + 2UL)) = (unsigned char)'\x00';
                        var_69 = *var_16;
                        *(uint64_t *)(local_sp_4 + (-16L)) = 4213589UL;
                        var_70 = indirect_placeholder_14(var_69);
                        var_71 = var_70.field_0;
                        var_72 = var_70.field_1;
                        var_73 = var_70.field_2;
                        var_74 = local_sp_4 + (-24L);
                        *(uint64_t *)var_74 = 4213617UL;
                        indirect_placeholder_12(0UL, 4322024UL, var_71, 1UL, 0UL, var_72, var_73);
                        local_sp_3 = var_74;
                        rcx2_2 = var_71;
                        r9_2 = var_72;
                        r85_2 = var_73;
                    }
                    var_75 = local_sp_3 + (-8L);
                    *(uint64_t *)var_75 = 4213627UL;
                    indirect_placeholder_1();
                    local_sp_2 = var_75;
                    rcx2_1 = rcx2_2;
                    r9_1 = r9_2;
                    r85_1 = r85_2;
                }
                break;
            }
        }
    *(uint64_t *)(local_sp_5 + (-8L)) = 4214308UL;
    indirect_placeholder_1();
    *(uint64_t *)(local_sp_5 + (-16L)) = 4214333UL;
    indirect_placeholder_1();
    return (uint64_t)*var_7;
}

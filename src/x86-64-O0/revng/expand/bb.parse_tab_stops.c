typedef unsigned char __u_char;
typedef unsigned short __u_short;
typedef unsigned int __u_int;
typedef unsigned long __u_long;
typedef signed char __int8_t;
typedef unsigned char __uint8_t;
typedef short __int16_t;
typedef unsigned short __uint16_t;
typedef int __int32_t;
typedef unsigned int __uint32_t;
typedef long __int64_t;
typedef unsigned long __uint64_t;
typedef __int8_t __int_least8_t;
typedef __uint8_t __uint_least8_t;
typedef __int16_t __int_least16_t;
typedef __uint16_t __uint_least16_t;
typedef __int32_t __int_least32_t;
typedef __uint32_t __uint_least32_t;
typedef __int64_t __int_least64_t;
typedef __uint64_t __uint_least64_t;
typedef long __quad_t;
typedef unsigned long __u_quad_t;
typedef long __intmax_t;
typedef unsigned long __uintmax_t;
typedef unsigned long __dev_t;
typedef unsigned int __uid_t;
typedef unsigned int __gid_t;
typedef unsigned long __ino_t;
typedef unsigned long __ino64_t;
typedef unsigned int __mode_t;
typedef unsigned long __nlink_t;
typedef long __off_t;
typedef long __off64_t;
typedef int __pid_t;
typedef struct {
    int __val[2];
} __fsid_t;
typedef long __clock_t;
typedef unsigned long __rlim_t;
typedef unsigned long __rlim64_t;
typedef unsigned int __id_t;
typedef long __time_t;
typedef unsigned int __useconds_t;
typedef long __suseconds_t;
typedef int __daddr_t;
typedef int __key_t;
typedef int __clockid_t;
typedef void *__timer_t;
typedef long __blksize_t;
typedef long __blkcnt_t;
typedef long __blkcnt64_t;
typedef unsigned long __fsblkcnt_t;
typedef unsigned long __fsblkcnt64_t;
typedef unsigned long __fsfilcnt_t;
typedef unsigned long __fsfilcnt64_t;
typedef long __fsword_t;
typedef long __ssize_t;
typedef long __syscall_slong_t;
typedef unsigned long __syscall_ulong_t;
typedef __off64_t __loff_t;
typedef char *__caddr_t;
typedef long __intptr_t;
typedef unsigned int __socklen_t;
typedef int __sig_atomic_t;
typedef __int8_t int8_t;
typedef __int16_t int16_t;
typedef __int32_t int32_t;
typedef __int64_t int64_t;
typedef __uint8_t uint8_t;
typedef __uint16_t uint16_t;
typedef __uint32_t uint32_t;
typedef __uint64_t uint64_t;
typedef __int_least8_t int_least8_t;
typedef __int_least16_t int_least16_t;
typedef __int_least32_t int_least32_t;
typedef __int_least64_t int_least64_t;
typedef __uint_least8_t uint_least8_t;
typedef __uint_least16_t uint_least16_t;
typedef __uint_least32_t uint_least32_t;
typedef __uint_least64_t uint_least64_t;
typedef signed char int_fast8_t;
typedef long int_fast16_t;
typedef long int_fast32_t;
typedef long int_fast64_t;
typedef unsigned char uint_fast8_t;
typedef unsigned long uint_fast16_t;
typedef unsigned long uint_fast32_t;
typedef unsigned long uint_fast64_t;
typedef long intptr_t;
typedef unsigned long uintptr_t;
typedef __intmax_t intmax_t;
typedef __uintmax_t uintmax_t;
typedef _Bool bool;
struct indirect_placeholder_37_ret_type;
struct indirect_placeholder_36_ret_type;
struct indirect_placeholder_38_ret_type;
struct indirect_placeholder_35_ret_type;
struct indirect_placeholder_34_ret_type;
struct indirect_placeholder_33_ret_type;
struct indirect_placeholder_39_ret_type;
struct indirect_placeholder_32_ret_type;
struct indirect_placeholder_40_ret_type;
struct indirect_placeholder_41_ret_type;
struct indirect_placeholder_42_ret_type;
struct indirect_placeholder_43_ret_type;
struct indirect_placeholder_44_ret_type;
struct indirect_placeholder_45_ret_type;
struct indirect_placeholder_37_ret_type {
    uint64_t field_0;
    uint64_t field_1;
    uint64_t field_2;
};
struct indirect_placeholder_36_ret_type {
    uint64_t field_0;
    uint64_t field_1;
    uint64_t field_2;
    uint64_t field_3;
};
struct indirect_placeholder_38_ret_type {
    uint64_t field_0;
    uint64_t field_1;
    uint64_t field_2;
};
struct indirect_placeholder_35_ret_type {
    uint64_t field_0;
    uint64_t field_1;
    uint64_t field_2;
    uint64_t field_3;
};
struct indirect_placeholder_34_ret_type {
    uint64_t field_0;
    uint64_t field_1;
    uint64_t field_2;
};
struct indirect_placeholder_33_ret_type {
    uint64_t field_0;
    uint64_t field_1;
    uint64_t field_2;
    uint64_t field_3;
};
struct indirect_placeholder_39_ret_type {
    uint64_t field_0;
    uint64_t field_1;
    uint64_t field_2;
};
struct indirect_placeholder_32_ret_type {
    uint64_t field_0;
    uint64_t field_1;
    uint64_t field_2;
    uint64_t field_3;
};
struct indirect_placeholder_40_ret_type {
    uint64_t field_0;
    uint64_t field_1;
    uint64_t field_2;
    uint64_t field_3;
    uint64_t field_4;
};
struct indirect_placeholder_41_ret_type {
    uint64_t field_0;
    uint64_t field_1;
    uint64_t field_2;
    uint64_t field_3;
    uint64_t field_4;
};
struct indirect_placeholder_42_ret_type {
    uint64_t field_0;
    uint64_t field_1;
    uint64_t field_2;
    uint64_t field_3;
    uint64_t field_4;
};
struct indirect_placeholder_43_ret_type {
    uint64_t field_0;
    uint64_t field_1;
    uint64_t field_2;
    uint64_t field_3;
    uint64_t field_4;
};
struct indirect_placeholder_44_ret_type {
    uint64_t field_0;
    uint64_t field_1;
    uint64_t field_2;
    uint64_t field_3;
    uint64_t field_4;
};
struct indirect_placeholder_45_ret_type {
    uint64_t field_0;
    uint64_t field_1;
    uint64_t field_2;
    uint64_t field_3;
    uint64_t field_4;
};
extern uint64_t revng_init_local_sp(uint64_t param_0);
extern uint64_t init_rbp(void);
extern uint64_t init_cc_src2(void);
extern uint64_t helper_cc_compute_c_wrapper(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint32_t param_3);
extern void indirect_placeholder_1(void);
extern uint64_t indirect_placeholder_14(void);
extern uint64_t indirect_placeholder_15(uint64_t param_0);
extern struct indirect_placeholder_37_ret_type indirect_placeholder_37(uint64_t param_0);
extern struct indirect_placeholder_36_ret_type indirect_placeholder_36(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint64_t param_3, uint64_t param_4, uint64_t param_5, uint64_t param_6);
extern struct indirect_placeholder_38_ret_type indirect_placeholder_38(uint64_t param_0);
extern struct indirect_placeholder_35_ret_type indirect_placeholder_35(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint64_t param_3, uint64_t param_4, uint64_t param_5, uint64_t param_6);
extern struct indirect_placeholder_34_ret_type indirect_placeholder_34(uint64_t param_0, uint64_t param_1);
extern struct indirect_placeholder_33_ret_type indirect_placeholder_33(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint64_t param_3, uint64_t param_4, uint64_t param_5, uint64_t param_6);
extern struct indirect_placeholder_39_ret_type indirect_placeholder_39(uint64_t param_0);
extern struct indirect_placeholder_32_ret_type indirect_placeholder_32(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint64_t param_3, uint64_t param_4, uint64_t param_5, uint64_t param_6);
extern struct indirect_placeholder_40_ret_type indirect_placeholder_40(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint64_t param_3);
extern struct indirect_placeholder_41_ret_type indirect_placeholder_41(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint64_t param_3);
extern struct indirect_placeholder_42_ret_type indirect_placeholder_42(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint64_t param_3, uint64_t param_4, uint64_t param_5);
extern struct indirect_placeholder_43_ret_type indirect_placeholder_43(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint64_t param_3);
extern struct indirect_placeholder_44_ret_type indirect_placeholder_44(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint64_t param_3);
extern struct indirect_placeholder_45_ret_type indirect_placeholder_45(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint64_t param_3, uint64_t param_4, uint64_t param_5);
void bb_parse_tab_stops(uint64_t rcx, uint64_t rdi, uint64_t r10, uint64_t r9, uint64_t r8, uint64_t rbx) {
    struct indirect_placeholder_36_ret_type var_62;
    uint64_t var_0;
    uint64_t var_1;
    uint64_t var_2;
    uint64_t var_3;
    uint64_t var_4;
    uint64_t *var_5;
    unsigned char *var_6;
    uint64_t *var_7;
    unsigned char *var_8;
    unsigned char *var_9;
    uint64_t *var_10;
    unsigned char *var_11;
    uint64_t *var_12;
    uint64_t *var_13;
    uint64_t rcx1_5;
    unsigned char var_92;
    unsigned char var_44;
    uint64_t local_sp_0;
    uint64_t r103_5;
    uint64_t var_56;
    struct indirect_placeholder_37_ret_type var_57;
    uint64_t var_58;
    uint64_t var_59;
    uint64_t var_60;
    uint64_t var_61;
    uint64_t var_63;
    uint64_t var_64;
    uint64_t var_65;
    uint64_t var_66;
    uint64_t r94_5;
    uint64_t rcx1_0;
    uint64_t r85_5;
    uint64_t r103_0;
    uint64_t rbx6_2;
    uint64_t r94_0;
    uint64_t r85_0;
    uint64_t local_sp_7;
    uint64_t local_sp_1;
    uint64_t r85_3;
    uint64_t var_45;
    struct indirect_placeholder_38_ret_type var_46;
    uint64_t var_47;
    uint64_t var_48;
    uint64_t var_49;
    uint64_t var_50;
    struct indirect_placeholder_35_ret_type var_51;
    uint64_t var_52;
    uint64_t var_53;
    uint64_t var_54;
    uint64_t var_55;
    uint64_t rcx1_1;
    uint64_t r103_1;
    uint64_t r94_1;
    uint64_t r85_1;
    uint64_t var_67;
    uint64_t var_68;
    uint64_t var_69;
    uint64_t var_70;
    struct indirect_placeholder_34_ret_type var_71;
    uint64_t var_72;
    uint64_t var_73;
    uint64_t var_74;
    struct indirect_placeholder_33_ret_type var_75;
    uint64_t var_76;
    uint64_t var_77;
    uint64_t var_78;
    uint64_t var_79;
    uint64_t var_80;
    uint64_t var_81;
    struct indirect_placeholder_39_ret_type var_82;
    uint64_t var_83;
    uint64_t var_84;
    uint64_t var_85;
    uint64_t var_86;
    struct indirect_placeholder_32_ret_type var_87;
    uint64_t var_88;
    uint64_t var_89;
    uint64_t var_90;
    uint64_t var_91;
    uint64_t local_sp_2;
    uint64_t rcx1_2;
    uint64_t r103_2;
    uint64_t r94_2;
    uint64_t r85_2;
    uint64_t rbx6_0;
    unsigned char **var_14;
    unsigned char var_15;
    uint64_t rcx1_3;
    uint64_t r103_3;
    uint64_t local_sp_5;
    uint64_t local_sp_3;
    uint64_t r94_3;
    unsigned char var_103;
    uint64_t var_93;
    uint64_t var_94;
    struct indirect_placeholder_40_ret_type var_95;
    unsigned char var_96;
    bool var_97;
    uint64_t var_98;
    uint64_t var_99;
    uint64_t *var_100;
    struct indirect_placeholder_41_ret_type var_101;
    unsigned char var_102;
    uint64_t local_sp_4;
    uint64_t var_16;
    uint64_t var_17;
    uint64_t var_18;
    uint64_t var_19;
    uint64_t local_sp_6;
    uint64_t var_20;
    uint64_t var_21;
    struct indirect_placeholder_43_ret_type var_22;
    uint64_t var_23;
    uint64_t var_24;
    uint64_t var_25;
    uint64_t var_26;
    uint64_t var_27;
    bool var_28;
    uint64_t var_29;
    uint64_t var_30;
    uint64_t *var_31;
    struct indirect_placeholder_44_ret_type var_32;
    uint64_t var_33;
    uint64_t var_34;
    uint64_t var_35;
    uint64_t var_36;
    uint64_t var_37;
    struct indirect_placeholder_45_ret_type var_38;
    uint64_t var_39;
    uint64_t var_40;
    uint64_t var_41;
    uint64_t var_42;
    uint64_t var_43;
    uint64_t rcx1_4;
    uint64_t r103_4;
    uint64_t r94_4;
    uint64_t r85_4;
    uint64_t rbx6_1;
    bool switch_state_var;
    var_0 = revng_init_local_sp(0UL);
    var_1 = init_rbp();
    var_2 = init_cc_src2();
    *(uint64_t *)(var_0 + (-8L)) = var_1;
    var_3 = var_0 + (-72L);
    var_4 = var_0 + (-64L);
    var_5 = (uint64_t *)var_4;
    *var_5 = rdi;
    var_6 = (unsigned char *)(var_0 + (-9L));
    *var_6 = (unsigned char)'\x00';
    var_7 = (uint64_t *)(var_0 + (-56L));
    *var_7 = 0UL;
    var_8 = (unsigned char *)(var_0 + (-10L));
    *var_8 = (unsigned char)'\x00';
    var_9 = (unsigned char *)(var_0 + (-11L));
    *var_9 = (unsigned char)'\x00';
    var_10 = (uint64_t *)(var_0 + (-24L));
    *var_10 = 0UL;
    var_11 = (unsigned char *)(var_0 + (-25L));
    *var_11 = (unsigned char)'\x01';
    var_12 = (uint64_t *)(var_0 + (-40L));
    var_13 = (uint64_t *)(var_0 + (-48L));
    var_92 = (unsigned char)'\x00';
    local_sp_2 = var_3;
    rcx1_2 = rcx;
    r103_2 = r10;
    r94_2 = r9;
    r85_2 = r8;
    rbx6_0 = rbx;
    var_103 = (unsigned char)'\x00';
    while (1U)
        {
            var_14 = (unsigned char **)var_4;
            var_15 = **var_14;
            rcx1_5 = rcx1_2;
            r103_5 = r103_2;
            r94_5 = r94_2;
            rcx1_0 = rcx1_2;
            r85_5 = r85_2;
            r103_0 = r103_2;
            rbx6_2 = rbx6_0;
            r94_0 = r94_2;
            r85_0 = r85_2;
            r85_3 = r85_2;
            rcx1_1 = rcx1_2;
            r103_1 = r103_2;
            r94_1 = r94_2;
            r85_1 = r85_2;
            rcx1_3 = rcx1_2;
            r103_3 = r103_2;
            local_sp_5 = local_sp_2;
            local_sp_3 = local_sp_2;
            r94_3 = r94_2;
            rcx1_4 = rcx1_2;
            r103_4 = r103_2;
            r94_4 = r94_2;
            r85_4 = r85_2;
            rbx6_1 = rbx6_0;
            switch_state_var = 0;
            switch (var_15) {
              case ',':
                {
                    local_sp_6 = local_sp_5;
                    if (*var_6 == '\x00') {
                        *var_6 = (unsigned char)'\x00';
                        rcx1_5 = rcx1_4;
                        r103_5 = r103_4;
                        r94_5 = r94_4;
                        r85_5 = r85_4;
                        rbx6_2 = rbx6_1;
                        local_sp_7 = local_sp_6;
                        *var_5 = (*var_5 + 1UL);
                        local_sp_2 = local_sp_7;
                        rcx1_2 = rcx1_5;
                        r103_2 = r103_5;
                        r94_2 = r94_5;
                        r85_2 = r85_5;
                        rbx6_0 = rbx6_2;
                        continue;
                    }
                    if (*var_8 != '\x00') {
                        var_20 = *var_7;
                        var_21 = local_sp_5 + (-8L);
                        *(uint64_t *)var_21 = 4203571UL;
                        var_22 = indirect_placeholder_43(rcx1_2, var_20, r94_2, r85_2);
                        var_23 = var_22.field_0;
                        var_24 = var_22.field_1;
                        var_25 = var_22.field_2;
                        var_26 = var_22.field_3;
                        var_27 = var_22.field_4;
                        r85_3 = var_27;
                        rcx1_3 = var_24;
                        r103_3 = var_25;
                        local_sp_3 = var_21;
                        r94_3 = var_26;
                        local_sp_6 = var_21;
                        rcx1_4 = var_24;
                        r103_4 = var_25;
                        r94_4 = var_26;
                        r85_4 = var_27;
                        if ((uint64_t)(unsigned char)var_23 != 1UL) {
                            *var_11 = (unsigned char)'\x00';
                            switch_state_var = 1;
                            break;
                        }
                    }
                    var_28 = (*var_9 == '\x00');
                    var_29 = *var_7;
                    var_30 = local_sp_5 + (-8L);
                    var_31 = (uint64_t *)var_30;
                    local_sp_3 = var_30;
                    local_sp_6 = var_30;
                    if (!var_28) {
                        *var_31 = 4203605UL;
                        var_32 = indirect_placeholder_44(rcx1_2, var_29, r94_2, r85_2);
                        var_33 = var_32.field_0;
                        var_34 = var_32.field_1;
                        var_35 = var_32.field_2;
                        var_36 = var_32.field_3;
                        var_37 = var_32.field_4;
                        r85_3 = var_37;
                        rcx1_3 = var_34;
                        r103_3 = var_35;
                        r94_3 = var_36;
                        rcx1_4 = var_34;
                        r103_4 = var_35;
                        r94_4 = var_36;
                        r85_4 = var_37;
                        if ((uint64_t)(unsigned char)var_33 != 1UL) {
                            *var_11 = (unsigned char)'\x00';
                            switch_state_var = 1;
                            break;
                        }
                    }
                    *var_31 = 4203633UL;
                    var_38 = indirect_placeholder_45(rcx1_2, var_29, r103_2, r94_2, r85_2, rbx6_0);
                    var_39 = var_38.field_0;
                    var_40 = var_38.field_1;
                    var_41 = var_38.field_2;
                    var_42 = var_38.field_3;
                    var_43 = var_38.field_4;
                    rcx1_4 = var_39;
                    r103_4 = var_40;
                    r94_4 = var_41;
                    r85_4 = var_42;
                    rbx6_1 = var_43;
                }
                break;
              case '\x00':
                {
                    var_92 = *var_11;
                    switch_state_var = 1;
                    break;
                }
                break;
              default:
                {
                    var_16 = (uint64_t)(uint32_t)(uint64_t)var_15;
                    *(uint64_t *)(local_sp_2 + (-8L)) = 4203533UL;
                    var_17 = indirect_placeholder_15(var_16);
                    var_18 = (uint64_t)(unsigned char)var_17;
                    var_19 = local_sp_2 + (-16L);
                    *(uint64_t *)var_19 = 4203543UL;
                    indirect_placeholder_1();
                    local_sp_0 = var_19;
                    local_sp_1 = var_19;
                    local_sp_5 = var_19;
                    local_sp_7 = var_19;
                    if (var_18 != 0UL) {
                        local_sp_6 = local_sp_5;
                        if (*var_6 != '\x00') {
                            *var_6 = (unsigned char)'\x00';
                            rcx1_5 = rcx1_4;
                            r103_5 = r103_4;
                            r94_5 = r94_4;
                            r85_5 = r85_4;
                            rbx6_2 = rbx6_1;
                            local_sp_7 = local_sp_6;
                            *var_5 = (*var_5 + 1UL);
                            local_sp_2 = local_sp_7;
                            rcx1_2 = rcx1_5;
                            r103_2 = r103_5;
                            r94_2 = r94_5;
                            r85_2 = r85_5;
                            rbx6_0 = rbx6_2;
                            continue;
                        }
                        if (*var_8 == '\x00') {
                            var_20 = *var_7;
                            var_21 = local_sp_5 + (-8L);
                            *(uint64_t *)var_21 = 4203571UL;
                            var_22 = indirect_placeholder_43(rcx1_2, var_20, r94_2, r85_2);
                            var_23 = var_22.field_0;
                            var_24 = var_22.field_1;
                            var_25 = var_22.field_2;
                            var_26 = var_22.field_3;
                            var_27 = var_22.field_4;
                            r85_3 = var_27;
                            rcx1_3 = var_24;
                            r103_3 = var_25;
                            local_sp_3 = var_21;
                            r94_3 = var_26;
                            local_sp_6 = var_21;
                            rcx1_4 = var_24;
                            r103_4 = var_25;
                            r94_4 = var_26;
                            r85_4 = var_27;
                            if ((uint64_t)(unsigned char)var_23 == 1UL) {
                                *var_11 = (unsigned char)'\x00';
                                switch_state_var = 1;
                                break;
                            }
                            *var_6 = (unsigned char)'\x00';
                            rcx1_5 = rcx1_4;
                            r103_5 = r103_4;
                            r94_5 = r94_4;
                            r85_5 = r85_4;
                            rbx6_2 = rbx6_1;
                            local_sp_7 = local_sp_6;
                            *var_5 = (*var_5 + 1UL);
                            local_sp_2 = local_sp_7;
                            rcx1_2 = rcx1_5;
                            r103_2 = r103_5;
                            r94_2 = r94_5;
                            r85_2 = r85_5;
                            rbx6_0 = rbx6_2;
                            continue;
                        }
                        var_28 = (*var_9 == '\x00');
                        var_29 = *var_7;
                        var_30 = local_sp_5 + (-8L);
                        var_31 = (uint64_t *)var_30;
                        local_sp_3 = var_30;
                        local_sp_6 = var_30;
                        if (var_28) {
                            *var_31 = 4203605UL;
                            var_32 = indirect_placeholder_44(rcx1_2, var_29, r94_2, r85_2);
                            var_33 = var_32.field_0;
                            var_34 = var_32.field_1;
                            var_35 = var_32.field_2;
                            var_36 = var_32.field_3;
                            var_37 = var_32.field_4;
                            r85_3 = var_37;
                            rcx1_3 = var_34;
                            r103_3 = var_35;
                            r94_3 = var_36;
                            rcx1_4 = var_34;
                            r103_4 = var_35;
                            r94_4 = var_36;
                            r85_4 = var_37;
                            if ((uint64_t)(unsigned char)var_33 == 1UL) {
                                *var_11 = (unsigned char)'\x00';
                                switch_state_var = 1;
                                break;
                            }
                        }
                        *var_31 = 4203633UL;
                        var_38 = indirect_placeholder_45(rcx1_2, var_29, r103_2, r94_2, r85_2, rbx6_0);
                        var_39 = var_38.field_0;
                        var_40 = var_38.field_1;
                        var_41 = var_38.field_2;
                        var_42 = var_38.field_3;
                        var_43 = var_38.field_4;
                        rcx1_4 = var_39;
                        r103_4 = var_40;
                        r94_4 = var_41;
                        r85_4 = var_42;
                        rbx6_1 = var_43;
                        *var_6 = (unsigned char)'\x00';
                        rcx1_5 = rcx1_4;
                        r103_5 = r103_4;
                        r94_5 = r94_4;
                        r85_5 = r85_4;
                        rbx6_2 = rbx6_1;
                        local_sp_7 = local_sp_6;
                        *var_5 = (*var_5 + 1UL);
                        local_sp_2 = local_sp_7;
                        rcx1_2 = rcx1_5;
                        r103_2 = r103_5;
                        r94_2 = r94_5;
                        r85_2 = r85_5;
                        rbx6_0 = rbx6_2;
                        continue;
                    }
                    switch (var_44) {
                      case '+':
                        {
                            if (*var_6 != '\x00') {
                                var_45 = *var_5;
                                *(uint64_t *)(local_sp_2 + (-24L)) = 4203745UL;
                                var_46 = indirect_placeholder_38(var_45);
                                var_47 = var_46.field_0;
                                var_48 = var_46.field_1;
                                var_49 = var_46.field_2;
                                var_50 = local_sp_2 + (-32L);
                                *(uint64_t *)var_50 = 4203773UL;
                                var_51 = indirect_placeholder_35(0UL, 4270368UL, var_47, 0UL, 0UL, var_48, var_49);
                                var_52 = var_51.field_0;
                                var_53 = var_51.field_1;
                                var_54 = var_51.field_2;
                                var_55 = var_51.field_3;
                                *var_11 = (unsigned char)'\x00';
                                local_sp_1 = var_50;
                                rcx1_1 = var_52;
                                r103_1 = var_53;
                                r94_1 = var_54;
                                r85_1 = var_55;
                            }
                            *var_9 = (unsigned char)'\x01';
                            *var_8 = (unsigned char)'\x00';
                            rcx1_5 = rcx1_1;
                            r103_5 = r103_1;
                            r94_5 = r94_1;
                            r85_5 = r85_1;
                            local_sp_7 = local_sp_1;
                        }
                        break;
                      case '/':
                        {
                            if (*var_6 != '\x00') {
                                var_56 = *var_5;
                                *(uint64_t *)(local_sp_2 + (-24L)) = 4203671UL;
                                var_57 = indirect_placeholder_37(var_56);
                                var_58 = var_57.field_0;
                                var_59 = var_57.field_1;
                                var_60 = var_57.field_2;
                                var_61 = local_sp_2 + (-32L);
                                *(uint64_t *)var_61 = 4203699UL;
                                var_62 = indirect_placeholder_36(0UL, 4270320UL, var_58, 0UL, 0UL, var_59, var_60);
                                var_63 = var_62.field_0;
                                var_64 = var_62.field_1;
                                var_65 = var_62.field_2;
                                var_66 = var_62.field_3;
                                *var_11 = (unsigned char)'\x00';
                                local_sp_0 = var_61;
                                rcx1_0 = var_63;
                                r103_0 = var_64;
                                r94_0 = var_65;
                                r85_0 = var_66;
                            }
                            *var_8 = (unsigned char)'\x01';
                            *var_9 = (unsigned char)'\x00';
                            rcx1_5 = rcx1_0;
                            r103_5 = r103_0;
                            r94_5 = r94_0;
                            r85_5 = r85_0;
                            local_sp_7 = local_sp_0;
                        }
                        break;
                    }
                    *var_5 = (*var_5 + 1UL);
                    local_sp_2 = local_sp_7;
                    rcx1_2 = rcx1_5;
                    r103_2 = r103_5;
                    r94_2 = r94_5;
                    r85_2 = r85_5;
                    rbx6_0 = rbx6_2;
                    continue;
                }
                break;
            }
            if (switch_state_var)
                break;
        }
    local_sp_4 = local_sp_3;
    var_103 = var_92;
    if (var_92 != '\x00' & *var_6 != '\x00') {
        if (*var_8 == '\x00') {
            var_93 = *var_7;
            var_94 = local_sp_3 + (-8L);
            *(uint64_t *)var_94 = 4204186UL;
            var_95 = indirect_placeholder_40(rcx1_3, var_93, r94_3, r85_3);
            var_96 = ((var_95.field_0 & (uint64_t)*var_11) != 0UL);
            *var_11 = var_96;
            var_103 = var_96;
            local_sp_4 = var_94;
        } else {
            var_97 = (*var_9 == '\x00');
            var_98 = *var_7;
            var_99 = local_sp_3 + (-8L);
            var_100 = (uint64_t *)var_99;
            local_sp_4 = var_99;
            if (var_97) {
                *var_100 = 4204254UL;
                indirect_placeholder_42(rcx1_3, var_98, r103_3, r94_3, r85_3, rbx6_0);
                var_103 = *var_11;
            } else {
                *var_100 = 4204223UL;
                var_101 = indirect_placeholder_41(rcx1_3, var_98, r94_3, r85_3);
                var_102 = ((var_101.field_0 & (uint64_t)*var_11) != 0UL);
                *var_11 = var_102;
                var_103 = var_102;
            }
        }
    }
    if (var_103 == '\x01') {
        return;
    }
    *(uint64_t *)(local_sp_4 + (-8L)) = 4204275UL;
    indirect_placeholder_1();
    return;
}

typedef unsigned char __u_char;
typedef unsigned short __u_short;
typedef unsigned int __u_int;
typedef unsigned long __u_long;
typedef signed char __int8_t;
typedef unsigned char __uint8_t;
typedef short __int16_t;
typedef unsigned short __uint16_t;
typedef int __int32_t;
typedef unsigned int __uint32_t;
typedef long __int64_t;
typedef unsigned long __uint64_t;
typedef __int8_t __int_least8_t;
typedef __uint8_t __uint_least8_t;
typedef __int16_t __int_least16_t;
typedef __uint16_t __uint_least16_t;
typedef __int32_t __int_least32_t;
typedef __uint32_t __uint_least32_t;
typedef __int64_t __int_least64_t;
typedef __uint64_t __uint_least64_t;
typedef long __quad_t;
typedef unsigned long __u_quad_t;
typedef long __intmax_t;
typedef unsigned long __uintmax_t;
typedef unsigned long __dev_t;
typedef unsigned int __uid_t;
typedef unsigned int __gid_t;
typedef unsigned long __ino_t;
typedef unsigned long __ino64_t;
typedef unsigned int __mode_t;
typedef unsigned long __nlink_t;
typedef long __off_t;
typedef long __off64_t;
typedef int __pid_t;
typedef struct {
    int __val[2];
} __fsid_t;
typedef long __clock_t;
typedef unsigned long __rlim_t;
typedef unsigned long __rlim64_t;
typedef unsigned int __id_t;
typedef long __time_t;
typedef unsigned int __useconds_t;
typedef long __suseconds_t;
typedef int __daddr_t;
typedef int __key_t;
typedef int __clockid_t;
typedef void *__timer_t;
typedef long __blksize_t;
typedef long __blkcnt_t;
typedef long __blkcnt64_t;
typedef unsigned long __fsblkcnt_t;
typedef unsigned long __fsblkcnt64_t;
typedef unsigned long __fsfilcnt_t;
typedef unsigned long __fsfilcnt64_t;
typedef long __fsword_t;
typedef long __ssize_t;
typedef long __syscall_slong_t;
typedef unsigned long __syscall_ulong_t;
typedef __off64_t __loff_t;
typedef char *__caddr_t;
typedef long __intptr_t;
typedef unsigned int __socklen_t;
typedef int __sig_atomic_t;
typedef __int8_t int8_t;
typedef __int16_t int16_t;
typedef __int32_t int32_t;
typedef __int64_t int64_t;
typedef __uint8_t uint8_t;
typedef __uint16_t uint16_t;
typedef __uint32_t uint32_t;
typedef __uint64_t uint64_t;
typedef __int_least8_t int_least8_t;
typedef __int_least16_t int_least16_t;
typedef __int_least32_t int_least32_t;
typedef __int_least64_t int_least64_t;
typedef __uint_least8_t uint_least8_t;
typedef __uint_least16_t uint_least16_t;
typedef __uint_least32_t uint_least32_t;
typedef __uint_least64_t uint_least64_t;
typedef signed char int_fast8_t;
typedef long int_fast16_t;
typedef long int_fast32_t;
typedef long int_fast64_t;
typedef unsigned char uint_fast8_t;
typedef unsigned long uint_fast16_t;
typedef unsigned long uint_fast32_t;
typedef unsigned long uint_fast64_t;
typedef long intptr_t;
typedef unsigned long uintptr_t;
typedef __intmax_t intmax_t;
typedef __uintmax_t uintmax_t;
struct indirect_placeholder_12_ret_type;
struct indirect_placeholder_12_ret_type {
    uint64_t field_0;
    uint64_t field_1;
    uint64_t field_2;
    uint64_t field_3;
};
extern void abort(void);
extern uint64_t revng_init_local_sp(uint64_t param_0);
extern uint64_t init_rbp(void);
extern void indirect_placeholder_1(void);
extern uint64_t init_rbx(void);
extern uint64_t init_r10(void);
extern void indirect_placeholder_9(uint64_t param_0, uint64_t param_1);
extern void indirect_placeholder_10(uint64_t param_0);
extern void indirect_placeholder_6(uint64_t param_0, uint64_t param_1, uint64_t param_2);
extern void indirect_placeholder_11(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint64_t param_3, uint64_t param_4, uint64_t param_5);
extern struct indirect_placeholder_12_ret_type indirect_placeholder_12(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint64_t param_3, uint64_t param_4);
void bb_main(uint64_t rsi, uint64_t rdi) {
    struct indirect_placeholder_12_ret_type var_17;
    uint64_t var_0;
    uint64_t var_1;
    uint64_t var_2;
    uint64_t var_3;
    uint64_t var_4;
    uint32_t *var_5;
    uint64_t var_6;
    uint64_t *var_7;
    uint64_t var_8;
    uint64_t var_9;
    uint32_t *var_10;
    uint64_t var_11;
    unsigned char *var_12;
    unsigned char *var_13;
    uint64_t local_sp_2;
    uint64_t storemerge;
    uint32_t var_31;
    uint64_t local_sp_2_be;
    uint64_t local_sp_1;
    uint64_t var_27;
    uint64_t var_28;
    uint64_t var_23;
    uint64_t var_24;
    uint64_t var_25;
    uint64_t var_26;
    uint64_t var_29;
    uint64_t var_30;
    uint64_t var_14;
    uint64_t var_15;
    uint64_t var_16;
    uint64_t var_18;
    uint64_t var_19;
    uint64_t var_20;
    uint64_t var_21;
    uint32_t var_22;
    unsigned int loop_state_var;
    var_0 = revng_init_local_sp(0UL);
    var_1 = init_rbp();
    var_2 = init_r10();
    var_3 = init_rbx();
    var_4 = var_0 + (-8L);
    *(uint64_t *)var_4 = var_1;
    var_5 = (uint32_t *)(var_0 + (-28L));
    *var_5 = (uint32_t)rdi;
    var_6 = var_0 + (-40L);
    var_7 = (uint64_t *)var_6;
    *var_7 = rsi;
    var_8 = **(uint64_t **)var_6;
    *(uint64_t *)(var_0 + (-48L)) = 4202714UL;
    indirect_placeholder_10(var_8);
    *(uint64_t *)(var_0 + (-56L)) = 4202729UL;
    indirect_placeholder_1();
    var_9 = var_0 + (-64L);
    *(uint64_t *)var_9 = 4202739UL;
    indirect_placeholder_1();
    *(unsigned char *)6379536UL = (unsigned char)'\x01';
    var_10 = (uint32_t *)(var_0 + (-12L));
    var_11 = var_0 + (-24L);
    var_12 = (unsigned char *)var_11;
    var_13 = (unsigned char *)(var_0 + (-23L));
    storemerge = 0UL;
    local_sp_2 = var_9;
    while (1U)
        {
            var_14 = *var_7;
            var_15 = (uint64_t)*var_5;
            var_16 = local_sp_2 + (-8L);
            *(uint64_t *)var_16 = 4202999UL;
            var_17 = indirect_placeholder_12(4269568UL, 4269632UL, var_14, var_15, 0UL);
            var_18 = var_17.field_0;
            var_19 = var_17.field_1;
            var_20 = var_17.field_2;
            var_21 = var_17.field_3;
            var_22 = (uint32_t)var_18;
            *var_10 = var_22;
            local_sp_2_be = var_16;
            local_sp_1 = var_16;
            if (var_22 != 4294967295U) {
                *(uint64_t *)(local_sp_2 + (-16L)) = 4203017UL;
                indirect_placeholder_6(var_19, var_20, var_21);
                var_31 = *(uint32_t *)6379320UL;
                if ((long)((uint64_t)var_31 << 32UL) >= (long)((uint64_t)*var_5 << 32UL)) {
                    loop_state_var = 1U;
                    break;
                }
                storemerge = *var_7 + ((uint64_t)var_31 << 3UL);
                loop_state_var = 1U;
                break;
            }
            if ((int)var_22 > (int)57U) {
                if ((uint64_t)(var_22 + (-105)) == 0UL) {
                    *(unsigned char *)6379536UL = (unsigned char)'\x00';
                } else {
                    if ((uint64_t)(var_22 + (-116)) != 0UL) {
                        loop_state_var = 0U;
                        break;
                    }
                    var_29 = *(uint64_t *)6380064UL;
                    var_30 = local_sp_2 + (-16L);
                    *(uint64_t *)var_30 = 4202825UL;
                    indirect_placeholder_11(var_19, var_29, var_2, var_20, var_21, var_3);
                    local_sp_2_be = var_30;
                }
            } else {
                if ((int)var_22 >= (int)48U) {
                    if ((uint64_t)(var_22 + 131U) == 0UL) {
                        if ((uint64_t)(var_22 + 130U) != 0UL) {
                            loop_state_var = 0U;
                            break;
                        }
                        *(uint64_t *)(local_sp_2 + (-16L)) = 4202897UL;
                        indirect_placeholder_9(var_4, 0UL);
                        abort();
                    }
                    var_27 = *(uint64_t *)6379184UL;
                    *(uint64_t *)(local_sp_2 + (-16L)) = 4202949UL;
                    indirect_placeholder_11(0UL, 4269336UL, var_27, 4270158UL, 0UL, 4270200UL);
                    var_28 = local_sp_2 + (-24L);
                    *(uint64_t *)var_28 = 4202959UL;
                    indirect_placeholder_1();
                    local_sp_1 = var_28;
                    loop_state_var = 0U;
                    break;
                }
                var_23 = *(uint64_t *)6380064UL;
                if (var_23 == 0UL) {
                    *var_12 = (unsigned char)var_22;
                    *var_13 = (unsigned char)'\x00';
                    var_26 = local_sp_2 + (-16L);
                    *(uint64_t *)var_26 = 4202885UL;
                    indirect_placeholder_11(var_19, var_11, var_2, var_20, var_21, var_3);
                    local_sp_2_be = var_26;
                } else {
                    var_24 = var_23 + (-1L);
                    var_25 = local_sp_2 + (-16L);
                    *(uint64_t *)var_25 = 4202861UL;
                    indirect_placeholder_11(var_19, var_24, var_2, var_20, var_21, var_3);
                    local_sp_2_be = var_25;
                }
            }
            local_sp_2 = local_sp_2_be;
            continue;
        }
    switch (loop_state_var) {
      case 0U:
        {
            *(uint64_t *)(local_sp_1 + (-8L)) = 4202969UL;
            indirect_placeholder_9(var_4, 1UL);
            abort();
        }
        break;
      case 1U:
        {
            *(uint64_t *)(local_sp_2 + (-24L)) = 4203066UL;
            indirect_placeholder_10(storemerge);
            *(uint64_t *)(local_sp_2 + (-32L)) = 4203071UL;
            indirect_placeholder_1();
            *(uint64_t *)(local_sp_2 + (-40L)) = 4203076UL;
            indirect_placeholder_1();
            return;
        }
        break;
    }
}

typedef unsigned char __u_char;
typedef unsigned short __u_short;
typedef unsigned int __u_int;
typedef unsigned long __u_long;
typedef signed char __int8_t;
typedef unsigned char __uint8_t;
typedef short __int16_t;
typedef unsigned short __uint16_t;
typedef int __int32_t;
typedef unsigned int __uint32_t;
typedef long __int64_t;
typedef unsigned long __uint64_t;
typedef __int8_t __int_least8_t;
typedef __uint8_t __uint_least8_t;
typedef __int16_t __int_least16_t;
typedef __uint16_t __uint_least16_t;
typedef __int32_t __int_least32_t;
typedef __uint32_t __uint_least32_t;
typedef __int64_t __int_least64_t;
typedef __uint64_t __uint_least64_t;
typedef long __quad_t;
typedef unsigned long __u_quad_t;
typedef long __intmax_t;
typedef unsigned long __uintmax_t;
typedef unsigned long __dev_t;
typedef unsigned int __uid_t;
typedef unsigned int __gid_t;
typedef unsigned long __ino_t;
typedef unsigned long __ino64_t;
typedef unsigned int __mode_t;
typedef unsigned long __nlink_t;
typedef long __off_t;
typedef long __off64_t;
typedef int __pid_t;
typedef struct {
    int __val[2];
} __fsid_t;
typedef long __clock_t;
typedef unsigned long __rlim_t;
typedef unsigned long __rlim64_t;
typedef unsigned int __id_t;
typedef long __time_t;
typedef unsigned int __useconds_t;
typedef long __suseconds_t;
typedef int __daddr_t;
typedef int __key_t;
typedef int __clockid_t;
typedef void *__timer_t;
typedef long __blksize_t;
typedef long __blkcnt_t;
typedef long __blkcnt64_t;
typedef unsigned long __fsblkcnt_t;
typedef unsigned long __fsblkcnt64_t;
typedef unsigned long __fsfilcnt_t;
typedef unsigned long __fsfilcnt64_t;
typedef long __fsword_t;
typedef long __ssize_t;
typedef long __syscall_slong_t;
typedef unsigned long __syscall_ulong_t;
typedef __off64_t __loff_t;
typedef char *__caddr_t;
typedef long __intptr_t;
typedef unsigned int __socklen_t;
typedef int __sig_atomic_t;
typedef __int8_t int8_t;
typedef __int16_t int16_t;
typedef __int32_t int32_t;
typedef __int64_t int64_t;
typedef __uint8_t uint8_t;
typedef __uint16_t uint16_t;
typedef __uint32_t uint32_t;
typedef __uint64_t uint64_t;
typedef __int_least8_t int_least8_t;
typedef __int_least16_t int_least16_t;
typedef __int_least32_t int_least32_t;
typedef __int_least64_t int_least64_t;
typedef __uint_least8_t uint_least8_t;
typedef __uint_least16_t uint_least16_t;
typedef __uint_least32_t uint_least32_t;
typedef __uint_least64_t uint_least64_t;
typedef signed char int_fast8_t;
typedef long int_fast16_t;
typedef long int_fast32_t;
typedef long int_fast64_t;
typedef unsigned char uint_fast8_t;
typedef unsigned long uint_fast16_t;
typedef unsigned long uint_fast32_t;
typedef unsigned long uint_fast64_t;
typedef long intptr_t;
typedef unsigned long uintptr_t;
typedef __intmax_t intmax_t;
typedef __uintmax_t uintmax_t;
typedef _Bool bool;
struct indirect_placeholder_63_ret_type;
struct indirect_placeholder_64_ret_type;
struct indirect_placeholder_63_ret_type {
    uint64_t field_0;
    uint64_t field_1;
    uint64_t field_2;
};
struct indirect_placeholder_64_ret_type {
    uint64_t field_0;
    uint64_t field_1;
    uint64_t field_2;
};
extern uint64_t revng_init_local_sp(uint64_t param_0);
extern uint64_t init_rbp(void);
extern void indirect_placeholder_1(void);
extern uint64_t indirect_placeholder_10(uint64_t param_0);
extern void indirect_placeholder_9(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint64_t param_3, uint64_t param_4, uint64_t param_5, uint64_t param_6);
extern struct indirect_placeholder_63_ret_type indirect_placeholder_63(uint64_t param_0, uint64_t param_1);
extern struct indirect_placeholder_64_ret_type indirect_placeholder_64(uint64_t param_0, uint64_t param_1);
uint64_t bb_target_directory_operand(uint64_t rdi) {
    uint64_t var_0;
    uint64_t var_1;
    uint64_t *var_2;
    uint64_t var_3;
    uint64_t *var_4;
    uint32_t storemerge1;
    uint64_t var_20;
    struct indirect_placeholder_63_ret_type var_21;
    uint64_t var_22;
    uint64_t var_23;
    uint64_t var_24;
    uint64_t var_25;
    uint64_t var_26;
    uint64_t local_sp_2;
    uint64_t rax_0;
    uint32_t *var_12;
    uint32_t var_13;
    uint64_t local_sp_1;
    uint64_t var_14;
    unsigned char storemerge;
    uint32_t *var_15;
    uint64_t storemerge2;
    unsigned char *var_16;
    uint64_t var_17;
    uint64_t var_18;
    uint64_t var_19;
    unsigned char var_27;
    unsigned char var_34;
    uint64_t var_28;
    struct indirect_placeholder_64_ret_type var_29;
    uint64_t var_30;
    uint64_t var_31;
    uint64_t var_32;
    uint64_t var_33;
    unsigned char *var_5;
    bool var_6;
    uint64_t var_7;
    uint64_t var_8;
    uint64_t *var_9;
    uint64_t var_10;
    uint64_t var_11;
    var_0 = revng_init_local_sp(0UL);
    var_1 = init_rbp();
    *(uint64_t *)(var_0 + (-8L)) = var_1;
    var_2 = (uint64_t *)(var_0 + (-192L));
    *var_2 = rdi;
    *(uint64_t *)(var_0 + (-208L)) = 4204323UL;
    var_3 = indirect_placeholder_10(rdi);
    var_4 = (uint64_t *)(var_0 + (-16L));
    *var_4 = var_3;
    *(uint64_t *)(var_0 + (-216L)) = 4204339UL;
    indirect_placeholder_1();
    *(uint64_t *)(var_0 + (-24L)) = var_3;
    storemerge1 = 0U;
    storemerge2 = 0UL;
    storemerge = (unsigned char)'\x01';
    if (var_3 == 0UL) {
    }
    var_5 = (unsigned char *)(var_0 + (-25L));
    *var_5 = (storemerge & '\x01');
    var_6 = (*(unsigned char *)6420737UL == '\x00');
    var_7 = *var_2;
    var_8 = var_0 + (-224L);
    var_9 = (uint64_t *)var_8;
    local_sp_1 = var_8;
    if (var_6) {
        *var_9 = 4204454UL;
        var_11 = indirect_placeholder_10(var_7);
        rax_0 = var_11;
    } else {
        *var_9 = 4204427UL;
        var_10 = indirect_placeholder_10(var_7);
        rax_0 = var_10;
    }
    var_12 = (uint32_t *)(var_0 + (-32L));
    var_13 = (uint32_t)rax_0;
    *var_12 = var_13;
    if (var_13 == 0U) {
        var_14 = var_8 + (-8L);
        *(uint64_t *)var_14 = 4204468UL;
        indirect_placeholder_1();
        local_sp_1 = var_14;
        storemerge1 = *(uint32_t *)rax_0;
    }
    var_15 = (uint32_t *)(var_0 + (-36L));
    *var_15 = storemerge1;
    local_sp_2 = local_sp_1;
    if (storemerge1 == 0U) {
    }
    var_16 = (unsigned char *)(var_0 + (-37L));
    *var_16 = ((unsigned char)storemerge2 & '\x01');
    *(uint64_t *)(local_sp_1 + (-8L)) = 4204534UL;
    indirect_placeholder_1();
    var_17 = (uint64_t)*(uint32_t *)storemerge2;
    var_18 = local_sp_1 + (-16L);
    *(uint64_t *)var_18 = 4204543UL;
    var_19 = indirect_placeholder_10(var_17);
    local_sp_2 = var_18;
    if (*var_15 != 0U & (uint64_t)(unsigned char)var_19 == 1UL) {
        var_20 = *var_2;
        *(uint64_t *)(local_sp_1 + (-24L)) = 4204570UL;
        var_21 = indirect_placeholder_63(4UL, var_20);
        var_22 = var_21.field_0;
        var_23 = var_21.field_1;
        var_24 = var_21.field_2;
        var_25 = (uint64_t)*var_15;
        var_26 = local_sp_1 + (-32L);
        *(uint64_t *)var_26 = 4204601UL;
        indirect_placeholder_9(0UL, 4304000UL, var_22, 1UL, var_25, var_23, var_24);
        local_sp_2 = var_26;
    }
    var_27 = *var_16;
    var_34 = var_27;
    if (var_27 >= *var_5) {
        return (uint64_t)var_34;
    }
    var_28 = *var_2;
    *(uint64_t *)(local_sp_2 + (-8L)) = 4204633UL;
    var_29 = indirect_placeholder_64(4UL, var_28);
    var_30 = var_29.field_0;
    var_31 = var_29.field_1;
    var_32 = var_29.field_2;
    var_33 = (uint64_t)*var_15;
    *(uint64_t *)(local_sp_2 + (-16L)) = 4204664UL;
    indirect_placeholder_9(0UL, 4304020UL, var_30, 1UL, var_33, var_31, var_32);
    var_34 = *var_16;
    return (uint64_t)var_34;
}

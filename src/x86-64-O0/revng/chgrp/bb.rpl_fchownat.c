typedef unsigned char __u_char;
typedef unsigned short __u_short;
typedef unsigned int __u_int;
typedef unsigned long __u_long;
typedef signed char __int8_t;
typedef unsigned char __uint8_t;
typedef short __int16_t;
typedef unsigned short __uint16_t;
typedef int __int32_t;
typedef unsigned int __uint32_t;
typedef long __int64_t;
typedef unsigned long __uint64_t;
typedef __int8_t __int_least8_t;
typedef __uint8_t __uint_least8_t;
typedef __int16_t __int_least16_t;
typedef __uint16_t __uint_least16_t;
typedef __int32_t __int_least32_t;
typedef __uint32_t __uint_least32_t;
typedef __int64_t __int_least64_t;
typedef __uint64_t __uint_least64_t;
typedef long __quad_t;
typedef unsigned long __u_quad_t;
typedef long __intmax_t;
typedef unsigned long __uintmax_t;
typedef unsigned long __dev_t;
typedef unsigned int __uid_t;
typedef unsigned int __gid_t;
typedef unsigned long __ino_t;
typedef unsigned long __ino64_t;
typedef unsigned int __mode_t;
typedef unsigned long __nlink_t;
typedef long __off_t;
typedef long __off64_t;
typedef int __pid_t;
typedef struct {
    int __val[2];
} __fsid_t;
typedef long __clock_t;
typedef unsigned long __rlim_t;
typedef unsigned long __rlim64_t;
typedef unsigned int __id_t;
typedef long __time_t;
typedef unsigned int __useconds_t;
typedef long __suseconds_t;
typedef int __daddr_t;
typedef int __key_t;
typedef int __clockid_t;
typedef void *__timer_t;
typedef long __blksize_t;
typedef long __blkcnt_t;
typedef long __blkcnt64_t;
typedef unsigned long __fsblkcnt_t;
typedef unsigned long __fsblkcnt64_t;
typedef unsigned long __fsfilcnt_t;
typedef unsigned long __fsfilcnt64_t;
typedef long __fsword_t;
typedef long __ssize_t;
typedef long __syscall_slong_t;
typedef unsigned long __syscall_ulong_t;
typedef __off64_t __loff_t;
typedef char *__caddr_t;
typedef long __intptr_t;
typedef unsigned int __socklen_t;
typedef int __sig_atomic_t;
typedef __int8_t int8_t;
typedef __int16_t int16_t;
typedef __int32_t int32_t;
typedef __int64_t int64_t;
typedef __uint8_t uint8_t;
typedef __uint16_t uint16_t;
typedef __uint32_t uint32_t;
typedef __uint64_t uint64_t;
typedef __int_least8_t int_least8_t;
typedef __int_least16_t int_least16_t;
typedef __int_least32_t int_least32_t;
typedef __int_least64_t int_least64_t;
typedef __uint_least8_t uint_least8_t;
typedef __uint_least16_t uint_least16_t;
typedef __uint_least32_t uint_least32_t;
typedef __uint_least64_t uint_least64_t;
typedef signed char int_fast8_t;
typedef long int_fast16_t;
typedef long int_fast32_t;
typedef long int_fast64_t;
typedef unsigned char uint_fast8_t;
typedef unsigned long uint_fast16_t;
typedef unsigned long uint_fast32_t;
typedef unsigned long uint_fast64_t;
typedef long intptr_t;
typedef unsigned long uintptr_t;
typedef __intmax_t intmax_t;
typedef __uintmax_t uintmax_t;
typedef _Bool bool;
struct indirect_placeholder_9_ret_type;
struct indirect_placeholder_9_ret_type {
    uint64_t field_0;
    uint64_t field_1;
};
extern void abort(void);
extern uint64_t revng_init_local_sp(uint64_t param_0);
extern uint64_t init_rbp(void);
extern void indirect_placeholder(void);
extern uint64_t indirect_placeholder_8(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint64_t param_3, uint64_t param_4);
extern uint64_t indirect_placeholder_3(uint64_t param_0, uint64_t param_1, uint64_t param_2);
extern void indirect_placeholder_7(uint64_t param_0);
extern uint64_t init_r9(void);
extern uint64_t indirect_placeholder_2(uint64_t param_0);
extern void indirect_placeholder_5(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint64_t param_3);
extern struct indirect_placeholder_9_ret_type indirect_placeholder_9(uint64_t param_0, uint64_t param_1, uint64_t param_2);
void bb_rpl_fchownat(uint64_t rdx, uint64_t rcx, uint64_t rdi, uint64_t rsi, uint64_t r8) {
    uint64_t var_36;
    uint64_t var_0;
    uint64_t var_1;
    uint64_t var_2;
    uint64_t var_3;
    uint32_t *var_4;
    uint64_t var_5;
    uint64_t *var_6;
    uint32_t *var_7;
    uint32_t *var_8;
    uint32_t *var_9;
    uint32_t var_10;
    uint32_t var_11;
    uint32_t storemerge;
    uint64_t var_58;
    uint64_t rax_0;
    uint32_t *var_51;
    uint32_t var_52;
    uint64_t local_sp_1;
    uint64_t var_53;
    uint32_t *var_54;
    uint64_t var_55;
    bool var_56;
    uint64_t *var_57;
    uint64_t var_40;
    uint32_t var_41;
    uint32_t *var_42;
    bool var_43;
    uint64_t var_44;
    uint64_t var_45;
    uint64_t var_46;
    uint64_t var_47;
    uint64_t *var_48;
    uint64_t var_50;
    uint64_t var_49;
    uint64_t var_37;
    uint32_t var_38;
    uint32_t *var_39;
    uint64_t var_34;
    uint64_t rax_1;
    uint64_t local_sp_2;
    uint64_t rax_2;
    uint32_t *var_29;
    uint64_t var_30;
    uint32_t var_31;
    uint32_t *var_32;
    uint64_t var_33;
    bool var_22;
    uint64_t var_23;
    uint64_t var_24;
    uint64_t var_25;
    uint64_t *var_26;
    uint64_t var_28;
    uint64_t var_27;
    uint64_t rdx1_1;
    uint64_t rcx2_1;
    uint64_t local_sp_4;
    uint64_t var_35;
    uint64_t var_12;
    uint32_t var_13;
    uint64_t var_14;
    uint64_t var_15;
    uint64_t var_16;
    uint64_t var_17;
    struct indirect_placeholder_9_ret_type var_18;
    uint64_t var_19;
    uint64_t var_20;
    uint64_t *var_21;
    bool var_59;
    uint64_t var_60;
    uint64_t var_61;
    uint64_t var_62;
    uint64_t *var_63;
    var_0 = revng_init_local_sp(0UL);
    var_1 = init_rbp();
    var_2 = init_r9();
    var_3 = var_0 + (-8L);
    *(uint64_t *)var_3 = var_1;
    var_4 = (uint32_t *)(var_0 + (-4092L));
    *var_4 = (uint32_t)rdi;
    var_5 = var_0 + (-4104L);
    var_6 = (uint64_t *)var_5;
    *var_6 = rsi;
    var_7 = (uint32_t *)(var_0 + (-4096L));
    *var_7 = (uint32_t)rdx;
    var_8 = (uint32_t *)(var_0 + (-4108L));
    *var_8 = (uint32_t)rcx;
    var_9 = (uint32_t *)(var_0 + (-4112L));
    var_10 = (uint32_t)r8;
    *var_9 = var_10;
    var_11 = var_10 & (-257);
    storemerge = 0U;
    if (var_11 != 0U) {
        var_12 = (uint64_t)var_11;
        *(uint64_t *)(var_0 + (-4128L)) = 4219595UL;
        indirect_placeholder();
        *(uint32_t *)var_12 = 22U;
        return;
    }
    var_13 = *var_4;
    if (var_13 != 4294967196U) {
        if (**(unsigned char **)var_5 != '/') {
            var_14 = *var_6;
            var_15 = (uint64_t)var_13;
            var_16 = var_0 + (-4088L);
            var_17 = var_0 + (-4128L);
            *(uint64_t *)var_17 = 4219744UL;
            var_18 = indirect_placeholder_9(var_14, var_16, var_15);
            var_19 = var_18.field_0;
            var_20 = var_18.field_1;
            var_21 = (uint64_t *)(var_0 + (-16L));
            *var_21 = var_19;
            rax_1 = var_16;
            rdx1_1 = var_20;
            rcx2_1 = var_15;
            local_sp_4 = var_17;
            if (var_19 != 0UL) {
                var_22 = (*var_9 == 256U);
                var_23 = (uint64_t)*var_8;
                var_24 = (uint64_t)*var_7;
                var_25 = var_0 + (-4136L);
                var_26 = (uint64_t *)var_25;
                rdx1_1 = var_23;
                rcx2_1 = var_24;
                if (var_22) {
                    *var_26 = 4219797UL;
                    var_28 = indirect_placeholder_3(var_23, var_19, var_24);
                    rax_2 = var_28;
                } else {
                    *var_26 = 4219825UL;
                    var_27 = indirect_placeholder_3(var_23, var_19, var_24);
                    rax_2 = var_27;
                }
                var_29 = (uint32_t *)(var_0 + (-20L));
                *var_29 = (uint32_t)rax_2;
                var_30 = var_25 + (-8L);
                *(uint64_t *)var_30 = 4219833UL;
                indirect_placeholder();
                var_31 = *(uint32_t *)rax_2;
                var_32 = (uint32_t *)(var_0 + (-24L));
                *var_32 = var_31;
                var_33 = *var_21;
                local_sp_2 = var_30;
                if (var_33 == var_16) {
                    var_34 = var_25 + (-16L);
                    *(uint64_t *)var_34 = 4219863UL;
                    indirect_placeholder();
                    rax_1 = var_33;
                    local_sp_2 = var_34;
                }
                local_sp_4 = local_sp_2;
                if (*var_29 == 4294967295U) {
                    return;
                }
                break;
            }
            var_35 = var_0 + (-56L);
            *(uint64_t *)(local_sp_4 + (-8L)) = 4219946UL;
            var_36 = indirect_placeholder_8(rdx1_1, rcx2_1, var_35, var_2, r8);
            if ((uint64_t)(uint32_t)var_36 == 0UL) {
                *(uint64_t *)(local_sp_4 + (-16L)) = 4219955UL;
                indirect_placeholder();
                var_37 = (uint64_t)*(uint32_t *)var_36;
                *(uint64_t *)(local_sp_4 + (-24L)) = 4219964UL;
                indirect_placeholder_5(var_3, var_37, var_2, r8);
                abort();
            }
            var_38 = *var_4;
            if ((int)var_38 >= (int)0U) {
                var_39 = (uint32_t *)var_35;
                if ((uint64_t)(*var_39 - var_38) != 0UL) {
                    *(uint64_t *)(local_sp_4 + (-16L)) = 4219996UL;
                    indirect_placeholder_7(var_35);
                    *(uint64_t *)(local_sp_4 + (-24L)) = 4220001UL;
                    indirect_placeholder();
                    *var_39 = 9U;
                    return;
                }
            }
            *(uint64_t *)(local_sp_4 + (-16L)) = 4220030UL;
            indirect_placeholder();
            if (var_38 != 0U) {
                var_40 = (uint64_t)var_38;
                *(uint64_t *)(local_sp_4 + (-24L)) = 4220039UL;
                indirect_placeholder();
                var_41 = *(uint32_t *)var_40;
                var_42 = (uint32_t *)(var_0 + (-28L));
                *var_42 = var_41;
                *(uint64_t *)(local_sp_4 + (-32L)) = 4220056UL;
                indirect_placeholder_7(var_35);
                *(uint64_t *)(local_sp_4 + (-40L)) = 4220061UL;
                indirect_placeholder();
                *(uint32_t *)var_35 = *var_42;
                return;
            }
            var_43 = (*var_9 == 256U);
            var_44 = (uint64_t)*var_8;
            var_45 = (uint64_t)*var_7;
            var_46 = *var_6;
            var_47 = local_sp_4 + (-24L);
            var_48 = (uint64_t *)var_47;
            local_sp_1 = var_47;
            if (var_43) {
                *var_48 = 4220120UL;
                var_50 = indirect_placeholder_3(var_44, var_46, var_45);
                rax_0 = var_50;
            } else {
                *var_48 = 4220151UL;
                var_49 = indirect_placeholder_3(var_44, var_46, var_45);
                rax_0 = var_49;
            }
            var_51 = (uint32_t *)(var_0 + (-32L));
            var_52 = (uint32_t)rax_0;
            *var_51 = var_52;
            if (var_52 == 4294967295U) {
                var_53 = var_47 + (-8L);
                *(uint64_t *)var_53 = 4220165UL;
                indirect_placeholder();
                local_sp_1 = var_53;
                storemerge = *(uint32_t *)rax_0;
            }
            var_54 = (uint32_t *)(var_0 + (-28L));
            *var_54 = storemerge;
            *(uint64_t *)(local_sp_1 + (-8L)) = 4220189UL;
            var_55 = indirect_placeholder_2(var_35);
            var_56 = ((uint64_t)(uint32_t)var_55 == 0UL);
            var_57 = (uint64_t *)(local_sp_1 + (-16L));
            if (!var_56) {
                *var_57 = 4220219UL;
                indirect_placeholder_7(var_35);
                if (*var_54 == 0U) {
                    *(uint64_t *)(local_sp_1 + (-24L)) = 4220230UL;
                    indirect_placeholder();
                    *(uint32_t *)var_35 = *var_54;
                }
                return;
            }
            *var_57 = 4220198UL;
            indirect_placeholder();
            var_58 = (uint64_t)*(uint32_t *)var_55;
            *(uint64_t *)(local_sp_1 + (-24L)) = 4220207UL;
            indirect_placeholder_5(var_3, var_58, var_2, r8);
            abort();
        }
    }
    var_59 = (var_10 == 256U);
    var_60 = (uint64_t)*var_8;
    var_61 = (uint64_t)*var_7;
    var_62 = *var_6;
    var_63 = (uint64_t *)(var_0 + (-4128L));
    if (var_59) {
        *var_63 = 4219675UL;
        indirect_placeholder_3(var_60, var_62, var_61);
    } else {
        *var_63 = 4219709UL;
        indirect_placeholder_3(var_60, var_62, var_61);
    }
}

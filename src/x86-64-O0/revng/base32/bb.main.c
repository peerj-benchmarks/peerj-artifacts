typedef unsigned char __u_char;
typedef unsigned short __u_short;
typedef unsigned int __u_int;
typedef unsigned long __u_long;
typedef signed char __int8_t;
typedef unsigned char __uint8_t;
typedef short __int16_t;
typedef unsigned short __uint16_t;
typedef int __int32_t;
typedef unsigned int __uint32_t;
typedef long __int64_t;
typedef unsigned long __uint64_t;
typedef __int8_t __int_least8_t;
typedef __uint8_t __uint_least8_t;
typedef __int16_t __int_least16_t;
typedef __uint16_t __uint_least16_t;
typedef __int32_t __int_least32_t;
typedef __uint32_t __uint_least32_t;
typedef __int64_t __int_least64_t;
typedef __uint64_t __uint_least64_t;
typedef long __quad_t;
typedef unsigned long __u_quad_t;
typedef long __intmax_t;
typedef unsigned long __uintmax_t;
typedef unsigned long __dev_t;
typedef unsigned int __uid_t;
typedef unsigned int __gid_t;
typedef unsigned long __ino_t;
typedef unsigned long __ino64_t;
typedef unsigned int __mode_t;
typedef unsigned long __nlink_t;
typedef long __off_t;
typedef long __off64_t;
typedef int __pid_t;
typedef struct {
    int __val[2];
} __fsid_t;
typedef long __clock_t;
typedef unsigned long __rlim_t;
typedef unsigned long __rlim64_t;
typedef unsigned int __id_t;
typedef long __time_t;
typedef unsigned int __useconds_t;
typedef long __suseconds_t;
typedef int __daddr_t;
typedef int __key_t;
typedef int __clockid_t;
typedef void *__timer_t;
typedef long __blksize_t;
typedef long __blkcnt_t;
typedef long __blkcnt64_t;
typedef unsigned long __fsblkcnt_t;
typedef unsigned long __fsblkcnt64_t;
typedef unsigned long __fsfilcnt_t;
typedef unsigned long __fsfilcnt64_t;
typedef long __fsword_t;
typedef long __ssize_t;
typedef long __syscall_slong_t;
typedef unsigned long __syscall_ulong_t;
typedef __off64_t __loff_t;
typedef char *__caddr_t;
typedef long __intptr_t;
typedef unsigned int __socklen_t;
typedef int __sig_atomic_t;
typedef __int8_t int8_t;
typedef __int16_t int16_t;
typedef __int32_t int32_t;
typedef __int64_t int64_t;
typedef __uint8_t uint8_t;
typedef __uint16_t uint16_t;
typedef __uint32_t uint32_t;
typedef __uint64_t uint64_t;
typedef __int_least8_t int_least8_t;
typedef __int_least16_t int_least16_t;
typedef __int_least32_t int_least32_t;
typedef __int_least64_t int_least64_t;
typedef __uint_least8_t uint_least8_t;
typedef __uint_least16_t uint_least16_t;
typedef __uint_least32_t uint_least32_t;
typedef __uint_least64_t uint_least64_t;
typedef signed char int_fast8_t;
typedef long int_fast16_t;
typedef long int_fast32_t;
typedef long int_fast64_t;
typedef unsigned char uint_fast8_t;
typedef unsigned long uint_fast16_t;
typedef unsigned long uint_fast32_t;
typedef unsigned long uint_fast64_t;
typedef long intptr_t;
typedef unsigned long uintptr_t;
typedef __intmax_t intmax_t;
typedef __uintmax_t uintmax_t;
struct indirect_placeholder_3_ret_type;
struct indirect_placeholder_1_ret_type;
struct indirect_placeholder_8_ret_type;
struct indirect_placeholder_10_ret_type;
struct indirect_placeholder_9_ret_type;
struct indirect_placeholder_13_ret_type;
struct indirect_placeholder_3_ret_type {
    uint64_t field_0;
    uint64_t field_1;
    uint64_t field_2;
};
struct indirect_placeholder_1_ret_type {
    uint64_t field_0;
    uint64_t field_1;
    uint64_t field_2;
};
struct indirect_placeholder_8_ret_type {
    uint64_t field_0;
    uint64_t field_1;
    uint64_t field_2;
};
struct indirect_placeholder_10_ret_type {
    uint64_t field_0;
    uint64_t field_1;
    uint64_t field_2;
};
struct indirect_placeholder_9_ret_type {
    uint64_t field_0;
    uint64_t field_1;
    uint64_t field_2;
};
struct indirect_placeholder_13_ret_type {
    uint64_t field_0;
    uint64_t field_1;
};
extern void abort(void);
extern uint64_t revng_init_local_sp(uint64_t param_0);
extern uint64_t init_rbp(void);
extern void indirect_placeholder_2(void);
extern uint64_t indirect_placeholder_4(uint64_t param_0, uint64_t param_1, uint64_t param_2);
extern uint64_t init_rbx(void);
extern void indirect_placeholder_6(uint64_t param_0, uint64_t param_1);
extern uint64_t indirect_placeholder_5(void);
extern void indirect_placeholder(uint64_t param_0);
extern uint64_t indirect_placeholder_12(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint64_t param_3, uint64_t param_4, uint64_t param_5);
extern void indirect_placeholder_7(uint64_t param_0, uint64_t param_1, uint64_t param_2);
extern struct indirect_placeholder_3_ret_type indirect_placeholder_3(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint64_t param_3, uint64_t param_4, uint64_t param_5, uint64_t param_6);
extern struct indirect_placeholder_1_ret_type indirect_placeholder_1(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint64_t param_3, uint64_t param_4, uint64_t param_5, uint64_t param_6);
extern struct indirect_placeholder_8_ret_type indirect_placeholder_8(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint64_t param_3, uint64_t param_4, uint64_t param_5, uint64_t param_6);
extern struct indirect_placeholder_10_ret_type indirect_placeholder_10(uint64_t param_0);
extern struct indirect_placeholder_9_ret_type indirect_placeholder_9(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint64_t param_3, uint64_t param_4, uint64_t param_5, uint64_t param_6);
extern void indirect_placeholder_11(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint64_t param_3, uint64_t param_4, uint64_t param_5);
extern struct indirect_placeholder_13_ret_type indirect_placeholder_13(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint64_t param_3, uint64_t param_4);
void bb_main(uint64_t rdi, uint64_t rsi) {
    uint64_t var_23;
    uint32_t var_24;
    uint64_t r9_1;
    uint64_t local_sp_1;
    uint64_t var_49;
    struct indirect_placeholder_3_ret_type var_54;
    struct indirect_placeholder_13_ret_type var_16;
    uint64_t local_sp_2;
    uint64_t var_0;
    uint64_t var_1;
    uint64_t var_2;
    uint64_t var_3;
    uint32_t *var_4;
    uint64_t var_5;
    uint64_t *var_6;
    unsigned char *var_7;
    unsigned char *var_8;
    uint64_t *var_9;
    uint64_t var_10;
    uint64_t var_11;
    uint32_t *var_12;
    uint64_t local_sp_4;
    uint64_t rcx_0;
    uint64_t var_52;
    uint64_t var_53;
    uint64_t r8_1;
    uint64_t local_sp_0;
    uint64_t r9_0;
    uint64_t r8_0;
    uint64_t var_55;
    uint64_t var_56;
    uint64_t var_57;
    uint64_t var_50;
    uint64_t var_51;
    uint64_t var_42;
    uint64_t var_43;
    uint64_t var_44;
    uint64_t var_45;
    uint64_t var_46;
    uint64_t var_47;
    uint64_t var_48;
    uint64_t var_33;
    uint64_t var_34;
    uint64_t var_35;
    uint64_t var_36;
    struct indirect_placeholder_8_ret_type var_37;
    uint64_t *_pre_phi154;
    uint64_t var_41;
    uint64_t var_38;
    uint64_t var_39;
    uint64_t *var_40;
    uint64_t var_30;
    uint64_t var_31;
    uint64_t *var_32;
    uint64_t var_13;
    uint32_t var_25;
    uint64_t var_58;
    struct indirect_placeholder_10_ret_type var_59;
    uint64_t var_60;
    uint64_t var_61;
    uint64_t var_62;
    uint64_t var_27;
    uint64_t *var_28;
    uint64_t *_pre_phi152;
    uint64_t *var_26;
    uint64_t var_29;
    uint64_t local_sp_3;
    uint64_t var_18;
    uint64_t var_19;
    uint64_t var_20;
    uint64_t var_21;
    uint64_t var_22;
    uint64_t local_sp_4_be;
    uint64_t var_14;
    uint64_t var_15;
    uint32_t var_17;
    unsigned int loop_state_var;
    var_0 = revng_init_local_sp(0UL);
    var_1 = init_rbp();
    var_2 = init_rbx();
    var_3 = var_0 + (-8L);
    *(uint64_t *)var_3 = var_1;
    *(uint64_t *)(var_0 + (-16L)) = var_2;
    var_4 = (uint32_t *)(var_0 + (-76L));
    *var_4 = (uint32_t)rdi;
    var_5 = var_0 + (-88L);
    var_6 = (uint64_t *)var_5;
    *var_6 = rsi;
    var_7 = (unsigned char *)(var_0 + (-41L));
    *var_7 = (unsigned char)'\x00';
    var_8 = (unsigned char *)(var_0 + (-42L));
    *var_8 = (unsigned char)'\x00';
    var_9 = (uint64_t *)(var_0 + (-56L));
    *var_9 = 76UL;
    var_10 = **(uint64_t **)var_5;
    *(uint64_t *)(var_0 + (-96L)) = 4203871UL;
    indirect_placeholder(var_10);
    *(uint64_t *)(var_0 + (-104L)) = 4203886UL;
    indirect_placeholder_2();
    var_11 = var_0 + (-112L);
    *(uint64_t *)var_11 = 4203896UL;
    indirect_placeholder_2();
    var_12 = (uint32_t *)(var_0 + (-60L));
    r8_1 = 0UL;
    local_sp_4 = var_11;
    while (1U)
        {
            var_13 = *var_6;
            var_14 = (uint64_t)*var_4;
            var_15 = local_sp_4 + (-8L);
            *(uint64_t *)var_15 = 4204125UL;
            var_16 = indirect_placeholder_13(4274443UL, 4273408UL, var_14, var_13, 0UL);
            var_17 = (uint32_t)var_16.field_0;
            *var_12 = var_17;
            local_sp_3 = var_15;
            local_sp_4_be = var_15;
            if (var_17 == 4294967295U) {
                if ((uint64_t)(var_17 + (-100)) == 0UL) {
                    *var_7 = (unsigned char)'\x01';
                } else {
                    if ((int)var_17 <= (int)100U) {
                        if ((uint64_t)(var_17 + 131U) != 0UL) {
                            if ((uint64_t)(var_17 + 130U) != 0UL) {
                                loop_state_var = 0U;
                                break;
                            }
                            *(uint64_t *)(local_sp_4 + (-16L)) = 4204023UL;
                            indirect_placeholder_6(var_3, 0UL);
                            abort();
                        }
                        var_18 = *(uint64_t *)6384160UL;
                        *(uint64_t *)(local_sp_4 + (-16L)) = 4204075UL;
                        indirect_placeholder_11(0UL, 4273112UL, var_18, 4274094UL, 0UL, 4274427UL);
                        var_19 = local_sp_4 + (-24L);
                        *(uint64_t *)var_19 = 4204085UL;
                        indirect_placeholder_2();
                        local_sp_3 = var_19;
                        loop_state_var = 0U;
                        break;
                    }
                    if ((uint64_t)(var_17 + (-105)) == 0UL) {
                        *var_8 = (unsigned char)'\x01';
                    } else {
                        if ((uint64_t)(var_17 + (-119)) != 0UL) {
                            loop_state_var = 0U;
                            break;
                        }
                        var_20 = *(uint64_t *)6384984UL;
                        var_21 = local_sp_4 + (-16L);
                        *(uint64_t *)var_21 = 4204001UL;
                        var_22 = indirect_placeholder_12(18446744073709551615UL, 4273275UL, var_20, 0UL, 0UL, 4274409UL);
                        *var_9 = var_22;
                        local_sp_4_be = var_21;
                    }
                }
                local_sp_4 = local_sp_4_be;
                continue;
            }
            var_23 = var_16.field_1;
            var_24 = *(uint32_t *)6384312UL;
            var_25 = *var_4;
            r9_1 = var_23;
            if ((int)(var_25 - var_24) <= (int)1U) {
                if ((long)((uint64_t)var_24 << 32UL) < (long)((uint64_t)var_25 << 32UL)) {
                    var_26 = (uint64_t *)(var_0 + (-40L));
                    *var_26 = 4274465UL;
                    _pre_phi152 = var_26;
                    loop_state_var = 1U;
                    break;
                }
                var_27 = *(uint64_t *)(*var_6 + ((uint64_t)var_24 << 3UL));
                var_28 = (uint64_t *)(var_0 + (-40L));
                *var_28 = var_27;
                _pre_phi152 = var_28;
                loop_state_var = 1U;
                break;
            }
            var_58 = *(uint64_t *)(*var_6 + ((uint64_t)var_24 << 3UL));
            *(uint64_t *)(local_sp_4 + (-16L)) = 4204190UL;
            var_59 = indirect_placeholder_10(var_58);
            var_60 = var_59.field_0;
            var_61 = var_59.field_1;
            var_62 = var_59.field_2;
            *(uint64_t *)(local_sp_4 + (-24L)) = 4204218UL;
            indirect_placeholder_9(0UL, 4274448UL, var_60, 0UL, 0UL, var_61, var_62);
            *(uint64_t *)(local_sp_4 + (-32L)) = 4204228UL;
            indirect_placeholder_6(var_3, 1UL);
            abort();
        }
    switch (loop_state_var) {
      case 0U:
        {
            *(uint64_t *)(local_sp_3 + (-8L)) = 4204095UL;
            indirect_placeholder_6(var_3, 1UL);
            abort();
        }
        break;
      case 1U:
        {
            var_29 = *_pre_phi152;
            *(uint64_t *)(local_sp_4 + (-16L)) = 4204296UL;
            indirect_placeholder_2();
            if ((uint64_t)(uint32_t)var_29 == 0UL) {
                var_38 = local_sp_4 + (-24L);
                *(uint64_t *)var_38 = 4204315UL;
                indirect_placeholder_6(0UL, 0UL);
                var_39 = *(uint64_t *)6384392UL;
                var_40 = (uint64_t *)(var_0 + (-32L));
                *var_40 = var_39;
                _pre_phi154 = var_40;
                local_sp_2 = var_38;
            } else {
                var_30 = *_pre_phi152;
                var_31 = local_sp_4 + (-24L);
                *(uint64_t *)var_31 = 4204345UL;
                indirect_placeholder_2();
                var_32 = (uint64_t *)(var_0 + (-32L));
                *var_32 = var_30;
                _pre_phi154 = var_32;
                local_sp_2 = var_31;
                if (var_30 == 0UL) {
                    var_33 = *_pre_phi152;
                    *(uint64_t *)(local_sp_4 + (-32L)) = 4204378UL;
                    var_34 = indirect_placeholder_4(var_33, 0UL, 3UL);
                    *(uint64_t *)(local_sp_4 + (-40L)) = 4204386UL;
                    indirect_placeholder_2();
                    var_35 = (uint64_t)*(uint32_t *)var_34;
                    var_36 = local_sp_4 + (-48L);
                    *(uint64_t *)var_36 = 4204413UL;
                    var_37 = indirect_placeholder_8(0UL, 4274470UL, var_34, 1UL, var_35, var_23, 0UL);
                    local_sp_2 = var_36;
                    r9_1 = var_37.field_1;
                    r8_1 = var_37.field_2;
                }
            }
            var_41 = *_pre_phi154;
            *(uint64_t *)(local_sp_2 + (-8L)) = 4204430UL;
            indirect_placeholder_6(var_41, 2UL);
            r9_0 = r9_1;
            r8_0 = r8_1;
            if (*var_7 == '\x00') {
                var_45 = *(uint64_t *)6384384UL;
                var_46 = *var_9;
                var_47 = *_pre_phi154;
                var_48 = local_sp_2 + (-16L);
                *(uint64_t *)var_48 = 4204490UL;
                indirect_placeholder_7(var_46, var_47, var_45);
                local_sp_1 = var_48;
                rcx_0 = var_45;
            } else {
                var_42 = (uint64_t)*var_8;
                var_43 = *(uint64_t *)6384384UL;
                var_44 = local_sp_2 + (-16L);
                *(uint64_t *)var_44 = 4204462UL;
                indirect_placeholder_6(var_42, var_43);
                local_sp_1 = var_44;
                rcx_0 = var_43;
            }
            *(uint64_t *)(local_sp_1 + (-8L)) = 4204502UL;
            var_49 = indirect_placeholder_5();
            if ((uint64_t)((uint32_t)var_49 + 1U) != 0UL) {
                var_50 = *_pre_phi152;
                var_51 = local_sp_1 + (-16L);
                *(uint64_t *)var_51 = 4204524UL;
                indirect_placeholder_2();
                local_sp_0 = var_51;
                if ((uint64_t)(uint32_t)var_50 == 0UL) {
                    *(uint64_t *)(local_sp_1 + (-24L)) = 4204533UL;
                    indirect_placeholder_2();
                    var_52 = (uint64_t)*(uint32_t *)var_50;
                    var_53 = local_sp_1 + (-32L);
                    *(uint64_t *)var_53 = 4204557UL;
                    var_54 = indirect_placeholder_3(0UL, 4274473UL, rcx_0, 1UL, var_52, r9_1, r8_1);
                    local_sp_0 = var_53;
                    r9_0 = var_54.field_1;
                    r8_0 = var_54.field_2;
                }
                var_55 = *_pre_phi152;
                *(uint64_t *)(local_sp_0 + (-8L)) = 4204579UL;
                var_56 = indirect_placeholder_4(var_55, 0UL, 3UL);
                *(uint64_t *)(local_sp_0 + (-16L)) = 4204587UL;
                indirect_placeholder_2();
                var_57 = (uint64_t)*(uint32_t *)var_56;
                *(uint64_t *)(local_sp_0 + (-24L)) = 4204614UL;
                indirect_placeholder_1(0UL, 4274470UL, var_56, 1UL, var_57, r9_0, r8_0);
            }
            return;
        }
        break;
    }
}

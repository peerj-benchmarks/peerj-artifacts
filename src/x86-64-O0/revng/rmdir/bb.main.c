typedef unsigned char __u_char;
typedef unsigned short __u_short;
typedef unsigned int __u_int;
typedef unsigned long __u_long;
typedef signed char __int8_t;
typedef unsigned char __uint8_t;
typedef short __int16_t;
typedef unsigned short __uint16_t;
typedef int __int32_t;
typedef unsigned int __uint32_t;
typedef long __int64_t;
typedef unsigned long __uint64_t;
typedef __int8_t __int_least8_t;
typedef __uint8_t __uint_least8_t;
typedef __int16_t __int_least16_t;
typedef __uint16_t __uint_least16_t;
typedef __int32_t __int_least32_t;
typedef __uint32_t __uint_least32_t;
typedef __int64_t __int_least64_t;
typedef __uint64_t __uint_least64_t;
typedef long __quad_t;
typedef unsigned long __u_quad_t;
typedef long __intmax_t;
typedef unsigned long __uintmax_t;
typedef unsigned long __dev_t;
typedef unsigned int __uid_t;
typedef unsigned int __gid_t;
typedef unsigned long __ino_t;
typedef unsigned long __ino64_t;
typedef unsigned int __mode_t;
typedef unsigned long __nlink_t;
typedef long __off_t;
typedef long __off64_t;
typedef int __pid_t;
typedef struct {
    int __val[2];
} __fsid_t;
typedef long __clock_t;
typedef unsigned long __rlim_t;
typedef unsigned long __rlim64_t;
typedef unsigned int __id_t;
typedef long __time_t;
typedef unsigned int __useconds_t;
typedef long __suseconds_t;
typedef int __daddr_t;
typedef int __key_t;
typedef int __clockid_t;
typedef void *__timer_t;
typedef long __blksize_t;
typedef long __blkcnt_t;
typedef long __blkcnt64_t;
typedef unsigned long __fsblkcnt_t;
typedef unsigned long __fsblkcnt64_t;
typedef unsigned long __fsfilcnt_t;
typedef unsigned long __fsfilcnt64_t;
typedef long __fsword_t;
typedef long __ssize_t;
typedef long __syscall_slong_t;
typedef unsigned long __syscall_ulong_t;
typedef __off64_t __loff_t;
typedef char *__caddr_t;
typedef long __intptr_t;
typedef unsigned int __socklen_t;
typedef int __sig_atomic_t;
typedef __int8_t int8_t;
typedef __int16_t int16_t;
typedef __int32_t int32_t;
typedef __int64_t int64_t;
typedef __uint8_t uint8_t;
typedef __uint16_t uint16_t;
typedef __uint32_t uint32_t;
typedef __uint64_t uint64_t;
typedef __int_least8_t int_least8_t;
typedef __int_least16_t int_least16_t;
typedef __int_least32_t int_least32_t;
typedef __int_least64_t int_least64_t;
typedef __uint_least8_t uint_least8_t;
typedef __uint_least16_t uint_least16_t;
typedef __uint_least32_t uint_least32_t;
typedef __uint_least64_t uint_least64_t;
typedef signed char int_fast8_t;
typedef long int_fast16_t;
typedef long int_fast32_t;
typedef long int_fast64_t;
typedef unsigned char uint_fast8_t;
typedef unsigned long uint_fast16_t;
typedef unsigned long uint_fast32_t;
typedef unsigned long uint_fast64_t;
typedef long intptr_t;
typedef unsigned long uintptr_t;
typedef __intmax_t intmax_t;
typedef __uintmax_t uintmax_t;
struct indirect_placeholder_33_ret_type;
struct indirect_placeholder_34_ret_type;
struct indirect_placeholder_36_ret_type;
struct indirect_placeholder_33_ret_type {
    uint64_t field_0;
    uint64_t field_1;
    uint64_t field_2;
    uint64_t field_3;
};
struct indirect_placeholder_34_ret_type {
    uint64_t field_0;
    uint64_t field_1;
    uint64_t field_2;
    uint64_t field_3;
};
struct indirect_placeholder_36_ret_type {
    uint64_t field_0;
    uint64_t field_1;
    uint64_t field_2;
    uint64_t field_3;
};
extern void abort(void);
extern uint64_t revng_init_local_sp(uint64_t param_0);
extern uint64_t init_rbp(void);
extern uint64_t indirect_placeholder_2(uint64_t param_0, uint64_t param_1);
extern void indirect_placeholder(void);
extern void indirect_placeholder_32(uint64_t param_0, uint64_t param_1);
extern uint64_t indirect_placeholder_14(void);
extern uint64_t init_rbx(void);
extern uint64_t indirect_placeholder_13(uint64_t param_0);
extern void indirect_placeholder_17(uint64_t param_0);
extern void indirect_placeholder_28(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint64_t param_3, uint64_t param_4, uint64_t param_5, uint64_t param_6);
extern struct indirect_placeholder_33_ret_type indirect_placeholder_33(uint64_t param_0, uint64_t param_1);
extern struct indirect_placeholder_34_ret_type indirect_placeholder_34(uint64_t param_0, uint64_t param_1);
extern void indirect_placeholder_35(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint64_t param_3, uint64_t param_4, uint64_t param_5);
extern struct indirect_placeholder_36_ret_type indirect_placeholder_36(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint64_t param_3, uint64_t param_4);
void bb_main(uint64_t rdi, uint64_t rsi) {
    struct indirect_placeholder_36_ret_type var_14;
    uint64_t var_0;
    uint64_t var_1;
    uint64_t var_2;
    uint64_t var_3;
    uint32_t *var_4;
    uint64_t var_5;
    uint64_t *var_6;
    unsigned char *var_7;
    uint64_t var_8;
    uint64_t var_9;
    uint32_t *var_10;
    uint64_t local_sp_4;
    uint64_t var_37;
    struct indirect_placeholder_33_ret_type var_38;
    uint64_t var_39;
    uint64_t var_40;
    uint64_t var_41;
    uint64_t var_42;
    uint64_t var_43;
    uint64_t local_sp_0;
    uint64_t var_33;
    uint64_t var_34;
    uint64_t var_35;
    uint64_t var_36;
    uint64_t var_44;
    uint64_t var_45;
    uint64_t var_46;
    uint32_t var_47;
    uint32_t var_21;
    uint32_t var_18;
    uint32_t var_19;
    uint64_t var_48;
    uint64_t var_49;
    uint64_t var_50;
    uint64_t *var_20;
    uint32_t var_22;
    uint64_t local_sp_1;
    uint64_t local_sp_2;
    uint64_t var_23;
    struct indirect_placeholder_34_ret_type var_24;
    uint64_t var_25;
    uint64_t var_26;
    uint64_t var_27;
    uint64_t var_28;
    uint64_t var_29;
    uint64_t var_30;
    uint64_t var_31;
    uint64_t var_32;
    uint64_t local_sp_3;
    uint64_t var_16;
    uint64_t var_17;
    uint64_t var_11;
    uint64_t var_12;
    uint64_t var_13;
    uint32_t var_15;
    unsigned int loop_state_var;
    var_0 = revng_init_local_sp(0UL);
    var_1 = init_rbp();
    var_2 = init_rbx();
    var_3 = var_0 + (-8L);
    *(uint64_t *)var_3 = var_1;
    *(uint64_t *)(var_0 + (-16L)) = var_2;
    var_4 = (uint32_t *)(var_0 + (-44L));
    *var_4 = (uint32_t)rdi;
    var_5 = var_0 + (-56L);
    var_6 = (uint64_t *)var_5;
    *var_6 = rsi;
    var_7 = (unsigned char *)(var_0 + (-25L));
    *var_7 = (unsigned char)'\x01';
    var_8 = **(uint64_t **)var_5;
    *(uint64_t *)(var_0 + (-64L)) = 4202785UL;
    indirect_placeholder_17(var_8);
    *(uint64_t *)(var_0 + (-72L)) = 4202800UL;
    indirect_placeholder();
    var_9 = var_0 + (-80L);
    *(uint64_t *)var_9 = 4202810UL;
    indirect_placeholder();
    *(unsigned char *)6378832UL = (unsigned char)'\x00';
    var_10 = (uint32_t *)(var_0 + (-32L));
    local_sp_4 = var_9;
    while (1U)
        {
            var_11 = *var_6;
            var_12 = (uint64_t)*var_4;
            var_13 = local_sp_4 + (-8L);
            *(uint64_t *)var_13 = 4203004UL;
            var_14 = indirect_placeholder_36(4269860UL, 4268736UL, var_12, var_11, 0UL);
            var_15 = (uint32_t)var_14.field_0;
            *var_10 = var_15;
            local_sp_1 = var_13;
            local_sp_3 = var_13;
            local_sp_4 = var_13;
            if (var_15 != 4294967295U) {
                var_18 = *(uint32_t *)6378616UL;
                var_19 = *var_4;
                var_21 = var_19;
                var_22 = var_18;
                if ((uint64_t)(var_18 - var_19) != 0UL) {
                    var_20 = (uint64_t *)(var_0 + (-40L));
                    loop_state_var = 1U;
                    break;
                }
                var_48 = var_14.field_3;
                var_49 = var_14.field_2;
                var_50 = var_14.field_1;
                *(uint64_t *)(local_sp_4 + (-16L)) = 4203053UL;
                indirect_placeholder_28(0UL, 4269863UL, var_50, 0UL, 0UL, var_49, var_48);
                *(uint64_t *)(local_sp_4 + (-24L)) = 4203063UL;
                indirect_placeholder_32(var_3, 1UL);
                abort();
            }
            if ((uint64_t)(var_15 + (-112)) == 0UL) {
                *(unsigned char *)6378832UL = (unsigned char)'\x01';
                continue;
            }
            if ((int)var_15 > (int)112U) {
                if ((uint64_t)(var_15 + (-118)) == 0UL) {
                    *(unsigned char *)6378834UL = (unsigned char)'\x01';
                    continue;
                }
                if ((uint64_t)(var_15 + (-128)) != 0UL) {
                    loop_state_var = 0U;
                    break;
                }
                *(unsigned char *)6378833UL = (unsigned char)'\x01';
                continue;
            }
            if ((uint64_t)(var_15 + 131U) == 0UL) {
                if ((uint64_t)(var_15 + 130U) != 0UL) {
                    loop_state_var = 0U;
                    break;
                }
                *(uint64_t *)(local_sp_4 + (-16L)) = 4202902UL;
                indirect_placeholder_32(var_3, 0UL);
                abort();
            }
            var_16 = *(uint64_t *)6378464UL;
            *(uint64_t *)(local_sp_4 + (-16L)) = 4202954UL;
            indirect_placeholder_35(0UL, 4268440UL, var_16, 4269838UL, 4269844UL, 0UL);
            var_17 = local_sp_4 + (-24L);
            *(uint64_t *)var_17 = 4202964UL;
            indirect_placeholder();
            local_sp_3 = var_17;
            loop_state_var = 0U;
            break;
        }
    switch (loop_state_var) {
      case 0U:
        {
            *(uint64_t *)(local_sp_3 + (-8L)) = 4202974UL;
            indirect_placeholder_32(var_3, 1UL);
            abort();
        }
        break;
      case 1U:
        {
            local_sp_2 = local_sp_1;
            while ((long)((uint64_t)var_22 << 32UL) >= (long)((uint64_t)var_21 << 32UL))
                {
                    var_23 = *(uint64_t *)(*var_6 + ((uint64_t)var_22 << 3UL));
                    *var_20 = var_23;
                    if (*(unsigned char *)6378834UL == '\x00') {
                        *(uint64_t *)(local_sp_1 + (-8L)) = 4203126UL;
                        var_24 = indirect_placeholder_34(4UL, var_23);
                        var_25 = var_24.field_0;
                        var_26 = var_24.field_1;
                        var_27 = var_24.field_2;
                        var_28 = var_24.field_3;
                        var_29 = *(uint64_t *)6378688UL;
                        var_30 = local_sp_1 + (-16L);
                        *(uint64_t *)var_30 = 4203154UL;
                        indirect_placeholder_28(0UL, var_25, var_26, var_29, 4269208UL, var_27, var_28);
                        local_sp_2 = var_30;
                    }
                    var_31 = local_sp_2 + (-8L);
                    *(uint64_t *)var_31 = 4203166UL;
                    var_32 = indirect_placeholder_14();
                    local_sp_0 = var_31;
                    if ((uint64_t)(uint32_t)var_32 == 0UL) {
                        if (*(unsigned char *)6378832UL == '\x00') {
                            var_44 = *var_20;
                            var_45 = local_sp_2 + (-16L);
                            *(uint64_t *)var_45 = 4203278UL;
                            var_46 = indirect_placeholder_13(var_44);
                            *var_7 = ((var_46 & (uint64_t)*var_7) != 0UL);
                            local_sp_0 = var_45;
                        }
                    } else {
                        *(uint64_t *)(local_sp_2 + (-16L)) = 4203175UL;
                        indirect_placeholder();
                        var_33 = (uint64_t)*(uint32_t *)var_32;
                        var_34 = *var_20;
                        var_35 = local_sp_2 + (-24L);
                        *(uint64_t *)var_35 = 4203191UL;
                        var_36 = indirect_placeholder_2(var_33, var_34);
                        local_sp_0 = var_35;
                        if ((uint64_t)(unsigned char)var_36 == 0UL) {
                            var_37 = *var_20;
                            *(uint64_t *)(local_sp_2 + (-32L)) = 4203214UL;
                            var_38 = indirect_placeholder_33(4UL, var_37);
                            var_39 = var_38.field_0;
                            var_40 = var_38.field_2;
                            var_41 = var_38.field_3;
                            *(uint64_t *)(local_sp_2 + (-40L)) = 4203222UL;
                            indirect_placeholder();
                            var_42 = (uint64_t)*(uint32_t *)var_39;
                            var_43 = local_sp_2 + (-48L);
                            *(uint64_t *)var_43 = 4203249UL;
                            indirect_placeholder_28(0UL, 4269879UL, var_39, 0UL, var_42, var_40, var_41);
                            *var_7 = (unsigned char)'\x00';
                            local_sp_0 = var_43;
                        }
                    }
                    var_47 = *(uint32_t *)6378616UL + 1U;
                    *(uint32_t *)6378616UL = var_47;
                    var_21 = *var_4;
                    var_22 = var_47;
                    local_sp_1 = local_sp_0;
                    local_sp_2 = local_sp_1;
                }
            return;
        }
        break;
    }
}

typedef unsigned char __u_char;
typedef unsigned short __u_short;
typedef unsigned int __u_int;
typedef unsigned long __u_long;
typedef signed char __int8_t;
typedef unsigned char __uint8_t;
typedef short __int16_t;
typedef unsigned short __uint16_t;
typedef int __int32_t;
typedef unsigned int __uint32_t;
typedef long __int64_t;
typedef unsigned long __uint64_t;
typedef __int8_t __int_least8_t;
typedef __uint8_t __uint_least8_t;
typedef __int16_t __int_least16_t;
typedef __uint16_t __uint_least16_t;
typedef __int32_t __int_least32_t;
typedef __uint32_t __uint_least32_t;
typedef __int64_t __int_least64_t;
typedef __uint64_t __uint_least64_t;
typedef long __quad_t;
typedef unsigned long __u_quad_t;
typedef long __intmax_t;
typedef unsigned long __uintmax_t;
typedef unsigned long __dev_t;
typedef unsigned int __uid_t;
typedef unsigned int __gid_t;
typedef unsigned long __ino_t;
typedef unsigned long __ino64_t;
typedef unsigned int __mode_t;
typedef unsigned long __nlink_t;
typedef long __off_t;
typedef long __off64_t;
typedef int __pid_t;
typedef struct {
    int __val[2];
} __fsid_t;
typedef long __clock_t;
typedef unsigned long __rlim_t;
typedef unsigned long __rlim64_t;
typedef unsigned int __id_t;
typedef long __time_t;
typedef unsigned int __useconds_t;
typedef long __suseconds_t;
typedef int __daddr_t;
typedef int __key_t;
typedef int __clockid_t;
typedef void *__timer_t;
typedef long __blksize_t;
typedef long __blkcnt_t;
typedef long __blkcnt64_t;
typedef unsigned long __fsblkcnt_t;
typedef unsigned long __fsblkcnt64_t;
typedef unsigned long __fsfilcnt_t;
typedef unsigned long __fsfilcnt64_t;
typedef long __fsword_t;
typedef long __ssize_t;
typedef long __syscall_slong_t;
typedef unsigned long __syscall_ulong_t;
typedef __off64_t __loff_t;
typedef char *__caddr_t;
typedef long __intptr_t;
typedef unsigned int __socklen_t;
typedef int __sig_atomic_t;
typedef __int8_t int8_t;
typedef __int16_t int16_t;
typedef __int32_t int32_t;
typedef __int64_t int64_t;
typedef __uint8_t uint8_t;
typedef __uint16_t uint16_t;
typedef __uint32_t uint32_t;
typedef __uint64_t uint64_t;
typedef __int_least8_t int_least8_t;
typedef __int_least16_t int_least16_t;
typedef __int_least32_t int_least32_t;
typedef __int_least64_t int_least64_t;
typedef __uint_least8_t uint_least8_t;
typedef __uint_least16_t uint_least16_t;
typedef __uint_least32_t uint_least32_t;
typedef __uint_least64_t uint_least64_t;
typedef signed char int_fast8_t;
typedef long int_fast16_t;
typedef long int_fast32_t;
typedef long int_fast64_t;
typedef unsigned char uint_fast8_t;
typedef unsigned long uint_fast16_t;
typedef unsigned long uint_fast32_t;
typedef unsigned long uint_fast64_t;
typedef long intptr_t;
typedef unsigned long uintptr_t;
typedef __intmax_t intmax_t;
typedef __uintmax_t uintmax_t;
typedef _Bool bool;
struct indirect_placeholder_54_ret_type;
struct indirect_placeholder_55_ret_type;
struct indirect_placeholder_57_ret_type;
struct indirect_placeholder_54_ret_type {
    uint64_t field_0;
    uint64_t field_1;
    uint64_t field_2;
};
struct indirect_placeholder_55_ret_type {
    uint64_t field_0;
    uint64_t field_1;
    uint64_t field_2;
};
struct indirect_placeholder_57_ret_type {
    uint64_t field_0;
    uint64_t field_1;
    uint64_t field_2;
    uint64_t field_3;
};
extern void abort(void);
extern uint64_t revng_init_local_sp(uint64_t param_0);
extern uint64_t init_rbp(void);
extern uint64_t indirect_placeholder_1(uint64_t param_0, uint64_t param_1);
extern void indirect_placeholder(void);
extern void indirect_placeholder_11(uint64_t param_0, uint64_t param_1);
extern void indirect_placeholder_14(uint64_t param_0);
extern struct indirect_placeholder_54_ret_type indirect_placeholder_54(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint64_t param_3, uint64_t param_4, uint64_t param_5, uint64_t param_6);
extern struct indirect_placeholder_55_ret_type indirect_placeholder_55(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint64_t param_3, uint64_t param_4, uint64_t param_5, uint64_t param_6);
extern void indirect_placeholder_56(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint64_t param_3, uint64_t param_4, uint64_t param_5);
extern struct indirect_placeholder_57_ret_type indirect_placeholder_57(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint64_t param_3, uint64_t param_4);
void bb_main(uint64_t rdi, uint64_t rsi) {
    uint64_t var_0;
    uint64_t var_1;
    uint64_t var_2;
    uint32_t *var_3;
    uint64_t var_4;
    uint64_t *var_5;
    unsigned char *var_6;
    unsigned char *var_7;
    unsigned char *var_8;
    uint64_t var_9;
    uint64_t var_10;
    uint32_t *var_11;
    uint64_t local_sp_4;
    uint64_t var_20;
    uint64_t var_21;
    uint64_t var_22;
    bool var_23;
    unsigned char *var_24;
    unsigned char var_25;
    unsigned char var_28;
    uint64_t var_26;
    uint64_t local_sp_0;
    uint64_t rcx_0;
    uint64_t r9_0;
    uint64_t r8_0;
    unsigned char var_30;
    struct indirect_placeholder_54_ret_type var_27;
    struct indirect_placeholder_57_ret_type var_15;
    uint64_t var_29;
    uint64_t local_sp_1;
    uint32_t *var_31;
    uint32_t *_pre_phi106;
    uint32_t *var_32;
    bool var_33;
    uint32_t *var_34;
    uint32_t var_35;
    uint64_t local_sp_2;
    uint64_t var_36;
    uint64_t var_37;
    uint64_t var_38;
    uint64_t var_39;
    uint32_t var_40;
    uint64_t local_sp_3;
    uint64_t var_17;
    uint64_t var_18;
    uint64_t *var_19;
    uint64_t var_12;
    uint64_t var_13;
    uint64_t var_14;
    uint32_t var_16;
    unsigned int loop_state_var;
    var_0 = revng_init_local_sp(0UL);
    var_1 = init_rbp();
    var_2 = var_0 + (-8L);
    *(uint64_t *)var_2 = var_1;
    var_3 = (uint32_t *)(var_0 + (-44L));
    *var_3 = (uint32_t)rdi;
    var_4 = var_0 + (-56L);
    var_5 = (uint64_t *)var_4;
    *var_5 = rsi;
    var_6 = (unsigned char *)(var_0 + (-9L));
    *var_6 = (unsigned char)'\x00';
    var_7 = (unsigned char *)(var_0 + (-10L));
    *var_7 = (unsigned char)'\x00';
    var_8 = (unsigned char *)(var_0 + (-17L));
    *var_8 = (unsigned char)'\x01';
    var_9 = **(uint64_t **)var_4;
    *(uint64_t *)(var_0 + (-64L)) = 4202310UL;
    indirect_placeholder_14(var_9);
    *(uint64_t *)(var_0 + (-72L)) = 4202325UL;
    indirect_placeholder();
    var_10 = var_0 + (-80L);
    *(uint64_t *)var_10 = 4202335UL;
    indirect_placeholder();
    var_11 = (uint32_t *)(var_0 + (-24L));
    var_30 = (unsigned char)'\x01';
    local_sp_4 = var_10;
    while (1U)
        {
            var_12 = *var_5;
            var_13 = (uint64_t)*var_3;
            var_14 = local_sp_4 + (-8L);
            *(uint64_t *)var_14 = 4202512UL;
            var_15 = indirect_placeholder_57(4267503UL, 4266752UL, var_13, var_12, 0UL);
            var_16 = (uint32_t)var_15.field_0;
            *var_11 = var_16;
            local_sp_0 = var_14;
            local_sp_3 = var_14;
            local_sp_4 = var_14;
            if (var_16 != 4294967295U) {
                var_20 = var_15.field_1;
                var_21 = var_15.field_2;
                var_22 = var_15.field_3;
                var_23 = ((long)((uint64_t)*(uint32_t *)6375064UL << 32UL) < (long)((uint64_t)*var_3 << 32UL));
                var_24 = (unsigned char *)(var_0 + (-25L));
                var_25 = var_23;
                *var_24 = var_25;
                var_28 = var_25;
                rcx_0 = var_20;
                r9_0 = var_21;
                r8_0 = var_22;
                if (*var_6 != '\x00') {
                    loop_state_var = 0U;
                    break;
                }
                if (*var_7 != '\x00') {
                    loop_state_var = 0U;
                    break;
                }
                var_26 = local_sp_4 + (-16L);
                *(uint64_t *)var_26 = 4202577UL;
                var_27 = indirect_placeholder_54(0UL, 4267512UL, var_20, 1UL, 0UL, var_21, var_22);
                var_28 = *var_24;
                local_sp_0 = var_26;
                rcx_0 = var_27.field_0;
                r9_0 = var_27.field_1;
                r8_0 = var_27.field_2;
                loop_state_var = 0U;
                break;
            }
            if ((uint64_t)(var_16 + 130U) == 0UL) {
                *(uint64_t *)(local_sp_4 + (-16L)) = 4202400UL;
                indirect_placeholder_11(var_2, 0UL);
                abort();
            }
            if ((int)var_16 > (int)4294967166U) {
                if ((uint64_t)(var_16 + (-100)) == 0UL) {
                    *var_6 = (unsigned char)'\x01';
                    continue;
                }
                if ((uint64_t)(var_16 + (-102)) != 0UL) {
                    loop_state_var = 1U;
                    break;
                }
                *var_7 = (unsigned char)'\x01';
                continue;
            }
            if (var_16 != 4294967165U) {
                loop_state_var = 1U;
                break;
            }
            var_17 = *(uint64_t *)6374912UL;
            var_18 = local_sp_4 + (-24L);
            var_19 = (uint64_t *)var_18;
            *var_19 = 0UL;
            *(uint64_t *)(local_sp_4 + (-32L)) = 4202458UL;
            indirect_placeholder_56(0UL, 4266456UL, var_17, 4267374UL, 4267472UL, 4267490UL);
            *var_19 = 4202472UL;
            indirect_placeholder();
            local_sp_3 = var_18;
            loop_state_var = 1U;
            break;
        }
    switch (loop_state_var) {
      case 1U:
        {
            *(uint64_t *)(local_sp_3 + (-8L)) = 4202482UL;
            indirect_placeholder_11(var_2, 1UL);
            abort();
        }
        break;
      case 0U:
        {
            local_sp_1 = local_sp_0;
            var_30 = var_28;
            if (var_28 != '\x01' & *var_6 == '\x00') {
                var_29 = local_sp_0 + (-8L);
                *(uint64_t *)var_29 = 4202619UL;
                indirect_placeholder_55(0UL, 4267560UL, rcx_0, 1UL, 0UL, r9_0, r8_0);
                var_30 = *var_24;
                local_sp_1 = var_29;
            }
            local_sp_2 = local_sp_1;
            if (var_30 == '\x01') {
                var_31 = (uint32_t *)(var_0 + (-16L));
                *var_31 = 3U;
                _pre_phi106 = var_31;
            } else {
                if (*var_7 == '\x00') {
                    var_32 = (uint32_t *)(var_0 + (-16L));
                    *var_32 = 2U;
                    _pre_phi106 = var_32;
                } else {
                    var_33 = (*var_6 == '\x01');
                    var_34 = (uint32_t *)(var_0 + (-16L));
                    _pre_phi106 = var_34;
                    if (var_33) {
                        *var_34 = 1U;
                    } else {
                        *var_34 = 0U;
                    }
                }
            }
            if (*_pre_phi106 != 3U) {
                var_35 = *(uint32_t *)6375064UL;
                while ((long)((uint64_t)var_35 << 32UL) >= (long)((uint64_t)*var_3 << 32UL))
                    {
                        var_36 = *(uint64_t *)(*var_5 + ((uint64_t)var_35 << 3UL));
                        var_37 = (uint64_t)*_pre_phi106;
                        var_38 = local_sp_2 + (-8L);
                        *(uint64_t *)var_38 = 4202735UL;
                        var_39 = indirect_placeholder_1(var_37, var_36);
                        *var_8 = ((var_39 & (uint64_t)*var_8) != 0UL);
                        var_40 = *(uint32_t *)6375064UL + 1U;
                        *(uint32_t *)6375064UL = var_40;
                        var_35 = var_40;
                        local_sp_2 = var_38;
                    }
            }
            *(uint64_t *)(local_sp_1 + (-8L)) = 4202692UL;
            indirect_placeholder();
            return;
        }
        break;
    }
}

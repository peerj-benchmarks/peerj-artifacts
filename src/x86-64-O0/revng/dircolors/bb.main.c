typedef unsigned char __u_char;
typedef unsigned short __u_short;
typedef unsigned int __u_int;
typedef unsigned long __u_long;
typedef signed char __int8_t;
typedef unsigned char __uint8_t;
typedef short __int16_t;
typedef unsigned short __uint16_t;
typedef int __int32_t;
typedef unsigned int __uint32_t;
typedef long __int64_t;
typedef unsigned long __uint64_t;
typedef __int8_t __int_least8_t;
typedef __uint8_t __uint_least8_t;
typedef __int16_t __int_least16_t;
typedef __uint16_t __uint_least16_t;
typedef __int32_t __int_least32_t;
typedef __uint32_t __uint_least32_t;
typedef __int64_t __int_least64_t;
typedef __uint64_t __uint_least64_t;
typedef long __quad_t;
typedef unsigned long __u_quad_t;
typedef long __intmax_t;
typedef unsigned long __uintmax_t;
typedef unsigned long __dev_t;
typedef unsigned int __uid_t;
typedef unsigned int __gid_t;
typedef unsigned long __ino_t;
typedef unsigned long __ino64_t;
typedef unsigned int __mode_t;
typedef unsigned long __nlink_t;
typedef long __off_t;
typedef long __off64_t;
typedef int __pid_t;
typedef struct {
    int __val[2];
} __fsid_t;
typedef long __clock_t;
typedef unsigned long __rlim_t;
typedef unsigned long __rlim64_t;
typedef unsigned int __id_t;
typedef long __time_t;
typedef unsigned int __useconds_t;
typedef long __suseconds_t;
typedef int __daddr_t;
typedef int __key_t;
typedef int __clockid_t;
typedef void *__timer_t;
typedef long __blksize_t;
typedef long __blkcnt_t;
typedef long __blkcnt64_t;
typedef unsigned long __fsblkcnt_t;
typedef unsigned long __fsblkcnt64_t;
typedef unsigned long __fsfilcnt_t;
typedef unsigned long __fsfilcnt64_t;
typedef long __fsword_t;
typedef long __ssize_t;
typedef long __syscall_slong_t;
typedef unsigned long __syscall_ulong_t;
typedef __off64_t __loff_t;
typedef char *__caddr_t;
typedef long __intptr_t;
typedef unsigned int __socklen_t;
typedef int __sig_atomic_t;
typedef __int8_t int8_t;
typedef __int16_t int16_t;
typedef __int32_t int32_t;
typedef __int64_t int64_t;
typedef __uint8_t uint8_t;
typedef __uint16_t uint16_t;
typedef __uint32_t uint32_t;
typedef __uint64_t uint64_t;
typedef __int_least8_t int_least8_t;
typedef __int_least16_t int_least16_t;
typedef __int_least32_t int_least32_t;
typedef __int_least64_t int_least64_t;
typedef __uint_least8_t uint_least8_t;
typedef __uint_least16_t uint_least16_t;
typedef __uint_least32_t uint_least32_t;
typedef __uint_least64_t uint_least64_t;
typedef signed char int_fast8_t;
typedef long int_fast16_t;
typedef long int_fast32_t;
typedef long int_fast64_t;
typedef unsigned char uint_fast8_t;
typedef unsigned long uint_fast16_t;
typedef unsigned long uint_fast32_t;
typedef unsigned long uint_fast64_t;
typedef long intptr_t;
typedef unsigned long uintptr_t;
typedef __intmax_t intmax_t;
typedef __uintmax_t uintmax_t;
typedef _Bool bool;
struct indirect_placeholder_18_ret_type;
struct indirect_placeholder_20_ret_type;
struct indirect_placeholder_18_ret_type {
    uint64_t field_0;
    uint64_t field_1;
    uint64_t field_2;
};
struct indirect_placeholder_20_ret_type {
    uint64_t field_0;
    uint64_t field_1;
    uint64_t field_2;
    uint64_t field_3;
};
extern void abort(void);
extern uint64_t revng_init_local_sp(uint64_t param_0);
extern uint64_t init_rbp(void);
extern uint64_t indirect_placeholder_1(uint64_t param_0, uint64_t param_1);
extern void indirect_placeholder(void);
extern uint64_t indirect_placeholder_2(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint64_t param_3, uint64_t param_4);
extern uint64_t indirect_placeholder_16(uint64_t param_0);
extern void indirect_placeholder_10(uint64_t param_0, uint64_t param_1);
extern uint64_t indirect_placeholder_6(void);
extern void indirect_placeholder_11(uint64_t param_0);
extern void indirect_placeholder_4(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint64_t param_3, uint64_t param_4, uint64_t param_5, uint64_t param_6);
extern struct indirect_placeholder_18_ret_type indirect_placeholder_18(uint64_t param_0);
extern void indirect_placeholder_19(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint64_t param_3, uint64_t param_4, uint64_t param_5);
extern struct indirect_placeholder_20_ret_type indirect_placeholder_20(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint64_t param_3, uint64_t param_4);
void bb_main(uint64_t rdi, uint64_t rsi) {
    struct indirect_placeholder_20_ret_type var_16;
    uint64_t var_0;
    uint64_t var_1;
    uint64_t var_2;
    uint32_t *var_3;
    uint64_t var_4;
    uint64_t *var_5;
    unsigned char *var_6;
    uint32_t *var_7;
    unsigned char *var_8;
    uint64_t **var_9;
    uint64_t var_10;
    uint64_t var_11;
    uint32_t *var_12;
    uint64_t local_sp_5;
    unsigned char var_43;
    uint64_t local_sp_0;
    uint64_t *var_44;
    uint64_t var_45;
    uint64_t *var_46;
    uint64_t var_47;
    uint64_t *var_48;
    uint64_t *_pre_phi176;
    unsigned char *var_49;
    uint64_t _pre;
    uint64_t var_50;
    uint64_t var_51;
    uint64_t var_52;
    uint64_t var_53;
    uint64_t *var_54;
    uint64_t var_55;
    uint64_t var_56;
    uint64_t var_57;
    uint64_t var_58;
    uint64_t *_pre_phi180;
    uint64_t _pre173;
    uint64_t var_59;
    bool var_60;
    uint64_t *var_61;
    uint64_t var_40;
    uint64_t var_41;
    unsigned char var_42;
    uint64_t var_36;
    uint64_t var_37;
    uint64_t var_38;
    unsigned char var_39;
    uint64_t var_35;
    uint64_t local_sp_3;
    uint64_t var_68;
    uint64_t local_sp_1;
    uint64_t var_20;
    uint64_t var_21;
    uint64_t var_22;
    uint64_t var_23;
    unsigned char var_24;
    bool var_25;
    uint64_t var_26;
    uint64_t var_62;
    struct indirect_placeholder_18_ret_type var_63;
    uint64_t var_64;
    uint64_t var_65;
    uint64_t var_66;
    uint64_t var_67;
    uint64_t *var_27;
    uint64_t var_28;
    uint64_t local_sp_2;
    uint64_t var_29;
    uint64_t var_30;
    uint64_t var_31;
    uint64_t var_32;
    uint64_t var_33;
    uint32_t var_34;
    uint64_t local_sp_4;
    uint64_t var_18;
    uint64_t var_19;
    uint64_t var_13;
    uint64_t var_14;
    uint64_t var_15;
    uint32_t var_17;
    unsigned int loop_state_var;
    var_0 = revng_init_local_sp(0UL);
    var_1 = init_rbp();
    var_2 = var_0 + (-8L);
    *(uint64_t *)var_2 = var_1;
    var_3 = (uint32_t *)(var_0 + (-108L));
    *var_3 = (uint32_t)rdi;
    var_4 = var_0 + (-120L);
    var_5 = (uint64_t *)var_4;
    *var_5 = rsi;
    var_6 = (unsigned char *)(var_0 + (-9L));
    *var_6 = (unsigned char)'\x01';
    var_7 = (uint32_t *)(var_0 + (-16L));
    *var_7 = 2U;
    var_8 = (unsigned char *)(var_0 + (-17L));
    *var_8 = (unsigned char)'\x00';
    var_9 = (uint64_t **)var_4;
    var_10 = **var_9;
    *(uint64_t *)(var_0 + (-128L)) = 4206151UL;
    indirect_placeholder_11(var_10);
    *(uint64_t *)(var_0 + (-136L)) = 4206166UL;
    indirect_placeholder();
    var_11 = var_0 + (-144L);
    *(uint64_t *)var_11 = 4206176UL;
    indirect_placeholder();
    var_12 = (uint32_t *)(var_0 + (-52L));
    var_28 = 4284992UL;
    local_sp_5 = var_11;
    while (1U)
        {
            var_13 = *var_5;
            var_14 = (uint64_t)*var_3;
            var_15 = local_sp_5 + (-8L);
            *(uint64_t *)var_15 = 4206358UL;
            var_16 = indirect_placeholder_20(4291342UL, 4290240UL, 0UL, var_14, var_13);
            var_17 = (uint32_t)var_16.field_0;
            *var_12 = var_17;
            local_sp_2 = var_15;
            local_sp_3 = var_15;
            local_sp_4 = var_15;
            local_sp_5 = var_15;
            if (var_17 != 4294967295U) {
                var_20 = var_16.field_1;
                var_21 = var_16.field_2;
                var_22 = var_16.field_3;
                *var_3 = (*var_3 - *(uint32_t *)6403288UL);
                var_23 = *var_5 + ((uint64_t)*(uint32_t *)6403288UL << 3UL);
                *var_5 = var_23;
                var_24 = *var_8;
                var_25 = (var_24 == '\x00');
                if (!var_25) {
                    loop_state_var = 0U;
                    break;
                }
                if (*var_7 != 2U) {
                    loop_state_var = 0U;
                    break;
                }
                *(uint64_t *)(local_sp_5 + (-16L)) = 4206433UL;
                indirect_placeholder_4(0UL, 4291352UL, var_20, var_21, 0UL, 0UL, var_22);
                *(uint64_t *)(local_sp_5 + (-24L)) = 4206443UL;
                indirect_placeholder_10(var_2, 1UL);
                abort();
            }
            if ((uint64_t)(var_17 + (-98)) == 0UL) {
                *var_7 = 0U;
                continue;
            }
            if ((int)var_17 > (int)98U) {
                if ((uint64_t)(var_17 + (-99)) == 0UL) {
                    *var_7 = 1U;
                    continue;
                }
                if ((uint64_t)(var_17 + (-112)) != 0UL) {
                    loop_state_var = 1U;
                    break;
                }
                *var_8 = (unsigned char)'\x01';
                continue;
            }
            if ((uint64_t)(var_17 + 131U) != 0UL) {
                if ((uint64_t)(var_17 + 130U) != 0UL) {
                    loop_state_var = 1U;
                    break;
                }
                *(uint64_t *)(local_sp_5 + (-16L)) = 4206256UL;
                indirect_placeholder_10(var_2, 0UL);
                abort();
            }
            var_18 = *(uint64_t *)6403136UL;
            *(uint64_t *)(local_sp_5 + (-16L)) = 4206308UL;
            indirect_placeholder_19(0UL, 4284760UL, var_18, 4291327UL, 4291174UL, 0UL);
            var_19 = local_sp_5 + (-24L);
            *(uint64_t *)var_19 = 4206318UL;
            indirect_placeholder();
            local_sp_4 = var_19;
            loop_state_var = 1U;
            break;
        }
    switch (loop_state_var) {
      case 1U:
        {
            *(uint64_t *)(local_sp_4 + (-8L)) = 4206328UL;
            indirect_placeholder_10(var_2, 1UL);
            abort();
        }
        break;
      case 0U:
        {
            var_26 = (uint64_t)(var_24 ^ '\x01');
            if ((long)(var_26 << 32UL) < (long)((uint64_t)*var_3 << 32UL)) {
                var_62 = *(uint64_t *)(var_23 + (var_26 << 3UL));
                *(uint64_t *)(local_sp_5 + (-16L)) = 4206494UL;
                var_63 = indirect_placeholder_18(var_62);
                var_64 = var_63.field_0;
                var_65 = var_63.field_1;
                var_66 = var_63.field_2;
                var_67 = local_sp_5 + (-24L);
                *(uint64_t *)var_67 = 4206522UL;
                indirect_placeholder_4(0UL, 4291455UL, var_64, var_65, 0UL, 0UL, var_66);
                local_sp_1 = var_67;
                if (*var_8 != '\x00') {
                    var_68 = local_sp_5 + (-32L);
                    *(uint64_t *)var_68 = 4206558UL;
                    indirect_placeholder();
                    local_sp_1 = var_68;
                }
                *(uint64_t *)(local_sp_1 + (-8L)) = 4206568UL;
                indirect_placeholder_10(var_2, 1UL);
                abort();
            }
            if (!var_25) {
                var_27 = (uint64_t *)(var_0 + (-32L));
                *var_27 = 4284992UL;
                while ((var_28 + (-4284992L)) <= 4172UL)
                    {
                        *(uint64_t *)(local_sp_2 + (-8L)) = 4206596UL;
                        indirect_placeholder();
                        var_29 = *var_27;
                        var_30 = local_sp_2 + (-16L);
                        *(uint64_t *)var_30 = 4206608UL;
                        indirect_placeholder();
                        var_31 = *var_27 + (var_29 + 1UL);
                        *var_27 = var_31;
                        var_28 = var_31;
                        local_sp_2 = var_30;
                    }
            }
            var_32 = local_sp_5 + (-16L);
            *(uint64_t *)var_32 = 4206652UL;
            var_33 = indirect_placeholder_6();
            var_34 = (uint32_t)var_33;
            *var_7 = var_34;
            local_sp_3 = var_32;
            if (*var_7 != 2U & var_34 == 2U) {
                var_35 = local_sp_5 + (-24L);
                *(uint64_t *)var_35 = 4206686UL;
                indirect_placeholder_4(0UL, 4291536UL, var_20, var_21, 1UL, 0UL, var_22);
                local_sp_3 = var_35;
            }
            *(uint64_t *)(local_sp_3 + (-8L)) = 4206717UL;
            indirect_placeholder_2(0UL, 4232900UL, 4201776UL, 6403520UL, 0UL);
            if (*var_3 == 0U) {
                var_40 = local_sp_3 + (-16L);
                *(uint64_t *)var_40 = 4206738UL;
                var_41 = indirect_placeholder_1(0UL, 0UL);
                var_42 = (unsigned char)var_41;
                *var_6 = var_42;
                var_43 = var_42;
                local_sp_0 = var_40;
            } else {
                var_36 = **var_9;
                var_37 = local_sp_3 + (-16L);
                *(uint64_t *)var_37 = 4206758UL;
                var_38 = indirect_placeholder_16(var_36);
                var_39 = (unsigned char)var_38;
                *var_6 = var_39;
                var_43 = var_39;
                local_sp_0 = var_37;
            }
            if (var_43 != '\x00') {
                *(uint64_t *)(var_0 + (-64L)) = 6403520UL;
                *(uint64_t *)(var_0 + (-72L)) = (*(uint64_t *)6403544UL - *(uint64_t *)6403536UL);
                var_44 = (uint64_t *)(var_0 + (-80L));
                *var_44 = 6403520UL;
                var_45 = *(uint64_t *)6403536UL;
                var_46 = (uint64_t *)(var_0 + (-88L));
                *var_46 = var_45;
                var_47 = *var_44;
                var_48 = (uint64_t *)(var_47 + 24UL);
                _pre_phi176 = var_48;
                var_50 = var_47;
                if (*var_48 == var_45) {
                    var_49 = (unsigned char *)(var_47 + 80UL);
                    *var_49 = (*var_49 | '\x02');
                    _pre = *var_44;
                    _pre_phi176 = (uint64_t *)(_pre + 24UL);
                    var_50 = _pre;
                }
                var_51 = *_pre_phi176;
                var_52 = *(uint64_t *)(var_50 + 48UL);
                *_pre_phi176 = ((var_51 + var_52) & (var_52 ^ (-1L)));
                var_53 = *var_44;
                var_54 = (uint64_t *)(var_53 + 24UL);
                var_55 = *var_54;
                var_56 = *(uint64_t *)(var_53 + 8UL);
                var_57 = var_55 - var_56;
                var_58 = *(uint64_t *)(var_53 + 32UL);
                _pre_phi180 = var_54;
                var_59 = var_53;
                if (var_57 > (var_58 - var_56)) {
                    *var_54 = var_58;
                    _pre173 = *var_44;
                    _pre_phi180 = (uint64_t *)(_pre173 + 24UL);
                    var_59 = _pre173;
                }
                *(uint64_t *)(var_59 + 16UL) = *_pre_phi180;
                *(uint64_t *)(var_0 + (-96L)) = *var_46;
                var_60 = (*var_7 == 0U);
                var_61 = (uint64_t *)(var_0 + (-40L));
                if (var_60) {
                    *var_61 = 4291598UL;
                    *(uint64_t *)(var_0 + (-48L)) = 4291610UL;
                } else {
                    *var_61 = 4291631UL;
                    *(uint64_t *)(var_0 + (-48L)) = 4291650UL;
                }
                *(uint64_t *)(local_sp_0 + (-8L)) = 4207063UL;
                indirect_placeholder();
                *(uint64_t *)(local_sp_0 + (-16L)) = 4207091UL;
                indirect_placeholder();
                *(uint64_t *)(local_sp_0 + (-24L)) = 4207113UL;
                indirect_placeholder();
            }
            return;
        }
        break;
    }
}

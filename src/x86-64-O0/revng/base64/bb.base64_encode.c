typedef unsigned char __u_char;
typedef unsigned short __u_short;
typedef unsigned int __u_int;
typedef unsigned long __u_long;
typedef signed char __int8_t;
typedef unsigned char __uint8_t;
typedef short __int16_t;
typedef unsigned short __uint16_t;
typedef int __int32_t;
typedef unsigned int __uint32_t;
typedef long __int64_t;
typedef unsigned long __uint64_t;
typedef __int8_t __int_least8_t;
typedef __uint8_t __uint_least8_t;
typedef __int16_t __int_least16_t;
typedef __uint16_t __uint_least16_t;
typedef __int32_t __int_least32_t;
typedef __uint32_t __uint_least32_t;
typedef __int64_t __int_least64_t;
typedef __uint64_t __uint_least64_t;
typedef long __quad_t;
typedef unsigned long __u_quad_t;
typedef long __intmax_t;
typedef unsigned long __uintmax_t;
typedef unsigned long __dev_t;
typedef unsigned int __uid_t;
typedef unsigned int __gid_t;
typedef unsigned long __ino_t;
typedef unsigned long __ino64_t;
typedef unsigned int __mode_t;
typedef unsigned long __nlink_t;
typedef long __off_t;
typedef long __off64_t;
typedef int __pid_t;
typedef struct {
    int __val[2];
} __fsid_t;
typedef long __clock_t;
typedef unsigned long __rlim_t;
typedef unsigned long __rlim64_t;
typedef unsigned int __id_t;
typedef long __time_t;
typedef unsigned int __useconds_t;
typedef long __suseconds_t;
typedef int __daddr_t;
typedef int __key_t;
typedef int __clockid_t;
typedef void *__timer_t;
typedef long __blksize_t;
typedef long __blkcnt_t;
typedef long __blkcnt64_t;
typedef unsigned long __fsblkcnt_t;
typedef unsigned long __fsblkcnt64_t;
typedef unsigned long __fsfilcnt_t;
typedef unsigned long __fsfilcnt64_t;
typedef long __fsword_t;
typedef long __ssize_t;
typedef long __syscall_slong_t;
typedef unsigned long __syscall_ulong_t;
typedef __off64_t __loff_t;
typedef char *__caddr_t;
typedef long __intptr_t;
typedef unsigned int __socklen_t;
typedef int __sig_atomic_t;
typedef __int8_t int8_t;
typedef __int16_t int16_t;
typedef __int32_t int32_t;
typedef __int64_t int64_t;
typedef __uint8_t uint8_t;
typedef __uint16_t uint16_t;
typedef __uint32_t uint32_t;
typedef __uint64_t uint64_t;
typedef __int_least8_t int_least8_t;
typedef __int_least16_t int_least16_t;
typedef __int_least32_t int_least32_t;
typedef __int_least64_t int_least64_t;
typedef __uint_least8_t uint_least8_t;
typedef __uint_least16_t uint_least16_t;
typedef __uint_least32_t uint_least32_t;
typedef __uint_least64_t uint_least64_t;
typedef signed char int_fast8_t;
typedef long int_fast16_t;
typedef long int_fast32_t;
typedef long int_fast64_t;
typedef unsigned char uint_fast8_t;
typedef unsigned long uint_fast16_t;
typedef unsigned long uint_fast32_t;
typedef unsigned long uint_fast64_t;
typedef long intptr_t;
typedef unsigned long uintptr_t;
typedef __intmax_t intmax_t;
typedef __uintmax_t uintmax_t;
extern uint64_t revng_init_local_sp(uint64_t param_0);
extern uint64_t init_rbp(void);
extern uint64_t init_rbx(void);
extern uint64_t indirect_placeholder_20(uint64_t param_0);
extern void indirect_placeholder_9(uint64_t param_0, uint64_t param_1, uint64_t param_2);
extern uint64_t init_r12(void);
void bb_base64_encode(uint64_t rdx, uint64_t rcx, uint64_t rdi, uint64_t rsi) {
    uint64_t var_0;
    uint64_t var_1;
    uint64_t var_2;
    uint64_t var_3;
    uint64_t var_4;
    uint64_t var_5;
    uint64_t *var_6;
    uint64_t *var_7;
    uint64_t var_8;
    uint64_t *var_9;
    uint64_t *var_10;
    uint64_t storemerge;
    uint64_t var_33;
    uint64_t var_34;
    uint64_t var_35;
    uint64_t local_sp_0;
    uint64_t storemerge7;
    uint64_t local_sp_2;
    uint64_t var_23;
    uint64_t var_24;
    uint64_t var_25;
    uint64_t local_sp_1;
    uint64_t var_26;
    uint64_t var_27;
    uint64_t var_28;
    uint64_t var_29;
    uint64_t var_30;
    uint64_t var_31;
    uint64_t var_32;
    unsigned char storemerge8;
    uint64_t var_36;
    uint64_t var_37;
    uint64_t local_sp_3;
    uint64_t var_38;
    uint64_t var_39;
    uint64_t var_40;
    unsigned char storemerge9;
    uint64_t var_41;
    uint64_t var_42;
    uint64_t var_44;
    uint64_t var_43;
    uint64_t local_sp_4;
    uint64_t var_17;
    uint64_t var_18;
    uint64_t var_19;
    uint64_t var_20;
    uint64_t var_21;
    uint64_t var_22;
    uint64_t var_11;
    uint64_t var_46;
    uint64_t var_47;
    unsigned char **var_12;
    uint64_t var_45;
    uint64_t var_13;
    uint64_t var_14;
    uint64_t var_15;
    uint64_t var_16;
    unsigned int loop_state_var;
    var_0 = revng_init_local_sp(0UL);
    var_1 = init_rbp();
    var_2 = init_r12();
    var_3 = init_rbx();
    *(uint64_t *)(var_0 + (-8L)) = var_1;
    *(uint64_t *)(var_0 + (-16L)) = var_2;
    *(uint64_t *)(var_0 + (-24L)) = var_3;
    var_4 = var_0 + (-56L);
    var_5 = var_0 + (-32L);
    var_6 = (uint64_t *)var_5;
    *var_6 = rdi;
    var_7 = (uint64_t *)(var_0 + (-40L));
    *var_7 = rsi;
    var_8 = var_0 + (-48L);
    var_9 = (uint64_t *)var_8;
    *var_9 = rdx;
    var_10 = (uint64_t *)var_4;
    *var_10 = rcx;
    storemerge = 0UL;
    storemerge7 = 0UL;
    storemerge8 = (unsigned char)'=';
    storemerge9 = (unsigned char)'=';
    var_44 = 0UL;
    local_sp_4 = var_4;
    var_45 = 0UL;
    if ((rcx & 3UL) != 0UL) {
        var_11 = (rcx >> 2UL) * 3UL;
        if (var_11 != *var_7) {
            var_46 = *var_9;
            var_47 = *var_6;
            *(uint64_t *)(var_0 + (-64L)) = 4205051UL;
            indirect_placeholder_9(var_46, var_47, var_11);
            return;
        }
    }
    var_12 = (unsigned char **)var_5;
    while (1U)
        {
            if (*var_7 != 0UL) {
                var_45 = *var_10;
                loop_state_var = 1U;
                break;
            }
            if (*var_10 != 0UL) {
                loop_state_var = 0U;
                break;
            }
            var_13 = *var_9;
            *var_9 = (var_13 + 1UL);
            var_14 = (uint64_t)(uint32_t)(uint64_t)**var_12;
            *(uint64_t *)(local_sp_4 + (-8L)) = 4205090UL;
            var_15 = indirect_placeholder_20(var_14);
            *(unsigned char *)var_13 = *(unsigned char *)(((var_15 >> 2UL) & 63UL) | 4273920UL);
            var_16 = *var_10 + (-1L);
            *var_10 = var_16;
            if (var_16 != 0UL) {
                loop_state_var = 0U;
                break;
            }
            var_17 = *var_9;
            *var_9 = (var_17 + 1UL);
            var_18 = (uint64_t)(uint32_t)(uint64_t)**var_12;
            var_19 = local_sp_4 + (-16L);
            *(uint64_t *)var_19 = 4205153UL;
            var_20 = indirect_placeholder_20(var_18);
            var_21 = var_20 << 4UL;
            var_22 = *var_7 + (-1L);
            *var_7 = var_22;
            local_sp_1 = var_19;
            if (var_22 == 0UL) {
                var_23 = (uint64_t)(uint32_t)(uint64_t)*(unsigned char *)(*var_6 + 1UL);
                var_24 = local_sp_4 + (-24L);
                *(uint64_t *)var_24 = 4205195UL;
                var_25 = indirect_placeholder_20(var_23);
                local_sp_1 = var_24;
                storemerge = (var_25 >> 4UL) & 15UL;
            }
            *(unsigned char *)var_17 = *(unsigned char *)((storemerge | (var_21 & 48UL)) | 4273920UL);
            var_26 = *var_10 + (-1L);
            *var_10 = var_26;
            local_sp_2 = local_sp_1;
            if (var_26 != 0UL) {
                loop_state_var = 0U;
                break;
            }
            var_27 = *var_9;
            *var_9 = (var_27 + 1UL);
            if (*var_7 != 0UL) {
                var_28 = (uint64_t)(uint32_t)(uint64_t)*(unsigned char *)(*var_6 + 1UL);
                var_29 = local_sp_1 + (-8L);
                *(uint64_t *)var_29 = 4205282UL;
                var_30 = indirect_placeholder_20(var_28);
                var_31 = var_30 << 2UL;
                var_32 = *var_7 + (-1L);
                *var_7 = var_32;
                local_sp_0 = var_29;
                if (var_32 != 0UL) {
                    var_33 = (uint64_t)(uint32_t)(uint64_t)*(unsigned char *)(*var_6 + 2UL);
                    var_34 = local_sp_1 + (-16L);
                    *(uint64_t *)var_34 = 4205326UL;
                    var_35 = indirect_placeholder_20(var_33);
                    local_sp_0 = var_34;
                    storemerge7 = (var_35 >> 6UL) & 3UL;
                }
                local_sp_2 = local_sp_0;
                storemerge8 = *(unsigned char *)((storemerge7 | (var_31 & 60UL)) | 4273920UL);
            }
            *(unsigned char *)var_27 = storemerge8;
            var_36 = *var_10 + (-1L);
            *var_10 = var_36;
            local_sp_3 = local_sp_2;
            if (var_36 != 0UL) {
                loop_state_var = 0U;
                break;
            }
            var_37 = *var_9;
            *var_9 = (var_37 + 1UL);
            if (*var_7 == 0UL) {
                var_38 = (uint64_t)(uint32_t)(uint64_t)*(unsigned char *)(*var_6 + 2UL);
                var_39 = local_sp_2 + (-8L);
                *(uint64_t *)var_39 = 4205417UL;
                var_40 = indirect_placeholder_20(var_38);
                local_sp_3 = var_39;
                storemerge9 = *(unsigned char *)((var_40 & 63UL) | 4273920UL);
            }
            *(unsigned char *)var_37 = storemerge9;
            var_41 = *var_10 + (-1L);
            *var_10 = var_41;
            local_sp_4 = local_sp_3;
            if (var_41 != 0UL) {
                loop_state_var = 0U;
                break;
            }
            var_42 = *var_7;
            if (var_42 == 0UL) {
                var_43 = var_42 + (-1L);
                *var_7 = var_43;
                var_44 = var_43;
            }
            if (var_44 == 0UL) {
                *var_6 = (*var_6 + 3UL);
            }
            continue;
        }
    switch (loop_state_var) {
      case 0U:
        {
        }
        break;
      case 1U:
        {
            if (var_45 != 0UL) {
                **(unsigned char **)var_8 = (unsigned char)'\x00';
            }
            return;
        }
        break;
    }
}

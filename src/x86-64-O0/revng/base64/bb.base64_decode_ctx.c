typedef unsigned char __u_char;
typedef unsigned short __u_short;
typedef unsigned int __u_int;
typedef unsigned long __u_long;
typedef signed char __int8_t;
typedef unsigned char __uint8_t;
typedef short __int16_t;
typedef unsigned short __uint16_t;
typedef int __int32_t;
typedef unsigned int __uint32_t;
typedef long __int64_t;
typedef unsigned long __uint64_t;
typedef __int8_t __int_least8_t;
typedef __uint8_t __uint_least8_t;
typedef __int16_t __int_least16_t;
typedef __uint16_t __uint_least16_t;
typedef __int32_t __int_least32_t;
typedef __uint32_t __uint_least32_t;
typedef __int64_t __int_least64_t;
typedef __uint64_t __uint_least64_t;
typedef long __quad_t;
typedef unsigned long __u_quad_t;
typedef long __intmax_t;
typedef unsigned long __uintmax_t;
typedef unsigned long __dev_t;
typedef unsigned int __uid_t;
typedef unsigned int __gid_t;
typedef unsigned long __ino_t;
typedef unsigned long __ino64_t;
typedef unsigned int __mode_t;
typedef unsigned long __nlink_t;
typedef long __off_t;
typedef long __off64_t;
typedef int __pid_t;
typedef struct {
    int __val[2];
} __fsid_t;
typedef long __clock_t;
typedef unsigned long __rlim_t;
typedef unsigned long __rlim64_t;
typedef unsigned int __id_t;
typedef long __time_t;
typedef unsigned int __useconds_t;
typedef long __suseconds_t;
typedef int __daddr_t;
typedef int __key_t;
typedef int __clockid_t;
typedef void *__timer_t;
typedef long __blksize_t;
typedef long __blkcnt_t;
typedef long __blkcnt64_t;
typedef unsigned long __fsblkcnt_t;
typedef unsigned long __fsblkcnt64_t;
typedef unsigned long __fsfilcnt_t;
typedef unsigned long __fsfilcnt64_t;
typedef long __fsword_t;
typedef long __ssize_t;
typedef long __syscall_slong_t;
typedef unsigned long __syscall_ulong_t;
typedef __off64_t __loff_t;
typedef char *__caddr_t;
typedef long __intptr_t;
typedef unsigned int __socklen_t;
typedef int __sig_atomic_t;
typedef __int8_t int8_t;
typedef __int16_t int16_t;
typedef __int32_t int32_t;
typedef __int64_t int64_t;
typedef __uint8_t uint8_t;
typedef __uint16_t uint16_t;
typedef __uint32_t uint32_t;
typedef __uint64_t uint64_t;
typedef __int_least8_t int_least8_t;
typedef __int_least16_t int_least16_t;
typedef __int_least32_t int_least32_t;
typedef __int_least64_t int_least64_t;
typedef __uint_least8_t uint_least8_t;
typedef __uint_least16_t uint_least16_t;
typedef __uint_least32_t uint_least32_t;
typedef __uint_least64_t uint_least64_t;
typedef signed char int_fast8_t;
typedef long int_fast16_t;
typedef long int_fast32_t;
typedef long int_fast64_t;
typedef unsigned char uint_fast8_t;
typedef unsigned long uint_fast16_t;
typedef unsigned long uint_fast32_t;
typedef unsigned long uint_fast64_t;
typedef long intptr_t;
typedef unsigned long uintptr_t;
typedef __intmax_t intmax_t;
typedef __uintmax_t uintmax_t;
typedef _Bool bool;
extern uint64_t revng_init_local_sp(uint64_t param_0);
extern uint64_t init_rbp(void);
extern uint64_t indirect_placeholder_2(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint64_t param_3);
uint64_t bb_base64_decode_ctx(uint64_t rdx, uint64_t rcx, uint64_t rdi, uint64_t rsi, uint64_t r8) {
    uint64_t var_0;
    uint64_t var_1;
    uint64_t var_2;
    uint64_t var_3;
    uint64_t *var_4;
    uint64_t var_5;
    uint64_t *var_6;
    uint64_t var_7;
    uint64_t *var_8;
    uint64_t var_9;
    uint64_t *var_10;
    uint64_t var_11;
    uint64_t **var_12;
    uint64_t var_13;
    uint64_t var_14;
    uint64_t *var_15;
    bool var_16;
    unsigned char *var_17;
    unsigned char *var_18;
    uint32_t *var_19;
    uint64_t local_sp_3;
    uint64_t local_sp_1;
    uint64_t *var_20;
    uint64_t *var_21;
    uint64_t *var_22;
    uint64_t local_sp_0;
    uint64_t local_sp_2;
    uint64_t var_23;
    uint64_t var_24;
    uint64_t var_25;
    uint64_t var_26;
    bool var_27;
    uint64_t var_28;
    uint64_t var_29;
    uint64_t var_30;
    uint64_t var_31;
    uint64_t var_33;
    uint64_t var_32;
    uint64_t local_sp_4;
    uint64_t var_34;
    uint64_t var_35;
    uint64_t var_36;
    uint64_t *var_37;
    uint64_t var_38;
    unsigned int loop_state_var;
    var_0 = revng_init_local_sp(0UL);
    var_1 = init_rbp();
    *(uint64_t *)(var_0 + (-8L)) = var_1;
    var_2 = var_0 + (-104L);
    var_3 = var_0 + (-64L);
    var_4 = (uint64_t *)var_3;
    *var_4 = rdi;
    var_5 = var_0 + (-72L);
    var_6 = (uint64_t *)var_5;
    *var_6 = rsi;
    var_7 = var_0 + (-80L);
    var_8 = (uint64_t *)var_7;
    *var_8 = rdx;
    var_9 = var_0 + (-88L);
    var_10 = (uint64_t *)var_9;
    *var_10 = rcx;
    var_11 = var_0 + (-96L);
    *(uint64_t *)var_11 = r8;
    var_12 = (uint64_t **)var_11;
    var_13 = **var_12;
    var_14 = var_0 + (-56L);
    var_15 = (uint64_t *)var_14;
    *var_15 = var_13;
    var_16 = (*var_4 != 0UL);
    var_17 = (unsigned char *)(var_0 + (-33L));
    *var_17 = var_16;
    var_18 = (unsigned char *)(var_0 + (-9L));
    *var_18 = (unsigned char)'\x00';
    var_19 = (uint32_t *)(var_0 + (-16L));
    *var_19 = 0U;
    local_sp_0 = var_2;
    if (*var_17 == '\x00') {
        *var_19 = **(uint32_t **)var_3;
        *var_18 = (*var_8 == 0UL);
    }
    var_20 = (uint64_t *)(var_0 + (-24L));
    var_21 = (uint64_t *)(var_0 + (-48L));
    var_22 = (uint64_t *)(var_0 + (-32L));
    while (1U)
        {
            *var_20 = *var_15;
            local_sp_1 = local_sp_0;
            local_sp_2 = local_sp_0;
            if (*var_19 != 0U & *var_18 == '\x01') {
                *var_20 = *var_15;
                var_23 = *var_8;
                var_24 = *var_6;
                var_25 = local_sp_1 + (-8L);
                *(uint64_t *)var_25 = 4206799UL;
                var_26 = indirect_placeholder_2(var_9, var_14, var_24, var_23);
                local_sp_1 = var_25;
                local_sp_2 = var_25;
                while ((uint64_t)(unsigned char)var_26 != 1UL)
                    {
                        *var_6 = (*var_6 + 4UL);
                        *var_8 = (*var_8 + (-4L));
                        *var_20 = *var_15;
                        var_23 = *var_8;
                        var_24 = *var_6;
                        var_25 = local_sp_1 + (-8L);
                        *(uint64_t *)var_25 = 4206799UL;
                        var_26 = indirect_placeholder_2(var_9, var_14, var_24, var_23);
                        local_sp_1 = var_25;
                        local_sp_2 = var_25;
                    }
            }
            var_27 = (*var_8 == 0UL);
            local_sp_3 = local_sp_2;
            local_sp_4 = local_sp_2;
            if (!var_27) {
                if (*var_18 != '\x01') {
                    loop_state_var = 0U;
                    break;
                }
            }
            if (!var_27) {
                if (**(unsigned char **)var_5 != '\n' & *var_17 != '\x00') {
                    *var_6 = (*var_6 + 1UL);
                    *var_8 = (*var_8 + (-1L));
                    local_sp_0 = local_sp_3;
                    continue;
                }
            }
            *var_10 = ((*var_15 - *var_20) + *var_10);
            *var_15 = *var_20;
            var_28 = *var_8 + *var_6;
            *var_21 = var_28;
            if (*var_17 == '\x00') {
                var_32 = *var_6;
                *var_22 = var_32;
                var_33 = var_32;
            } else {
                var_29 = *var_4;
                var_30 = local_sp_2 + (-8L);
                *(uint64_t *)var_30 = 4206986UL;
                var_31 = indirect_placeholder_2(var_28, var_7, var_29, var_5);
                *var_22 = var_31;
                var_33 = var_31;
                local_sp_4 = var_30;
            }
            var_34 = *var_8;
            if (var_34 != 0UL) {
                loop_state_var = 1U;
                break;
            }
            if (var_34 <= 3UL) {
                if (*var_18 != '\x01' & *var_17 != '\x00') {
                    loop_state_var = 1U;
                    break;
                }
            }
            var_35 = local_sp_4 + (-8L);
            *(uint64_t *)var_35 = 4207070UL;
            var_36 = indirect_placeholder_2(var_9, var_14, var_33, var_34);
            local_sp_3 = var_35;
            if ((uint64_t)(unsigned char)var_36 != 1UL) {
                loop_state_var = 0U;
                break;
            }
            *var_8 = (*var_21 - *var_6);
            local_sp_0 = local_sp_3;
            continue;
        }
    switch (loop_state_var) {
      case 0U:
        {
            var_37 = *var_12;
            *var_37 = (*var_37 - *var_15);
            var_38 = *var_8;
            return (var_38 & (-256L)) | (var_38 == 0UL);
        }
        break;
      case 1U:
        {
            *var_8 = 0UL;
        }
        break;
    }
}

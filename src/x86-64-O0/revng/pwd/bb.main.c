typedef unsigned char __u_char;
typedef unsigned short __u_short;
typedef unsigned int __u_int;
typedef unsigned long __u_long;
typedef signed char __int8_t;
typedef unsigned char __uint8_t;
typedef short __int16_t;
typedef unsigned short __uint16_t;
typedef int __int32_t;
typedef unsigned int __uint32_t;
typedef long __int64_t;
typedef unsigned long __uint64_t;
typedef __int8_t __int_least8_t;
typedef __uint8_t __uint_least8_t;
typedef __int16_t __int_least16_t;
typedef __uint16_t __uint_least16_t;
typedef __int32_t __int_least32_t;
typedef __uint32_t __uint_least32_t;
typedef __int64_t __int_least64_t;
typedef __uint64_t __uint_least64_t;
typedef long __quad_t;
typedef unsigned long __u_quad_t;
typedef long __intmax_t;
typedef unsigned long __uintmax_t;
typedef unsigned long __dev_t;
typedef unsigned int __uid_t;
typedef unsigned int __gid_t;
typedef unsigned long __ino_t;
typedef unsigned long __ino64_t;
typedef unsigned int __mode_t;
typedef unsigned long __nlink_t;
typedef long __off_t;
typedef long __off64_t;
typedef int __pid_t;
typedef struct {
    int __val[2];
} __fsid_t;
typedef long __clock_t;
typedef unsigned long __rlim_t;
typedef unsigned long __rlim64_t;
typedef unsigned int __id_t;
typedef long __time_t;
typedef unsigned int __useconds_t;
typedef long __suseconds_t;
typedef int __daddr_t;
typedef int __key_t;
typedef int __clockid_t;
typedef void *__timer_t;
typedef long __blksize_t;
typedef long __blkcnt_t;
typedef long __blkcnt64_t;
typedef unsigned long __fsblkcnt_t;
typedef unsigned long __fsblkcnt64_t;
typedef unsigned long __fsfilcnt_t;
typedef unsigned long __fsfilcnt64_t;
typedef long __fsword_t;
typedef long __ssize_t;
typedef long __syscall_slong_t;
typedef unsigned long __syscall_ulong_t;
typedef __off64_t __loff_t;
typedef char *__caddr_t;
typedef long __intptr_t;
typedef unsigned int __socklen_t;
typedef int __sig_atomic_t;
typedef __int8_t int8_t;
typedef __int16_t int16_t;
typedef __int32_t int32_t;
typedef __int64_t int64_t;
typedef __uint8_t uint8_t;
typedef __uint16_t uint16_t;
typedef __uint32_t uint32_t;
typedef __uint64_t uint64_t;
typedef __int_least8_t int_least8_t;
typedef __int_least16_t int_least16_t;
typedef __int_least32_t int_least32_t;
typedef __int_least64_t int_least64_t;
typedef __uint_least8_t uint_least8_t;
typedef __uint_least16_t uint_least16_t;
typedef __uint_least32_t uint_least32_t;
typedef __uint_least64_t uint_least64_t;
typedef signed char int_fast8_t;
typedef long int_fast16_t;
typedef long int_fast32_t;
typedef long int_fast64_t;
typedef unsigned char uint_fast8_t;
typedef unsigned long uint_fast16_t;
typedef unsigned long uint_fast32_t;
typedef unsigned long uint_fast64_t;
typedef long intptr_t;
typedef unsigned long uintptr_t;
typedef __intmax_t intmax_t;
typedef __uintmax_t uintmax_t;
typedef _Bool bool;
struct indirect_placeholder_24_ret_type;
struct indirect_placeholder_26_ret_type;
struct indirect_placeholder_24_ret_type {
    uint64_t field_0;
    uint64_t field_1;
    uint64_t field_2;
    uint64_t field_3;
};
struct indirect_placeholder_26_ret_type {
    uint64_t field_0;
    uint64_t field_1;
    uint64_t field_2;
    uint64_t field_3;
};
extern void abort(void);
extern uint64_t revng_init_local_sp(uint64_t param_0);
extern uint64_t init_rbp(void);
extern void indirect_placeholder(void);
extern void indirect_placeholder_2(uint64_t param_0, uint64_t param_1);
extern uint64_t indirect_placeholder_3(void);
extern void indirect_placeholder_23(uint64_t param_0);
extern uint64_t init_rax(void);
extern struct indirect_placeholder_24_ret_type indirect_placeholder_24(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint64_t param_3, uint64_t param_4, uint64_t param_5, uint64_t param_6);
extern void indirect_placeholder_25(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint64_t param_3, uint64_t param_4, uint64_t param_5);
extern struct indirect_placeholder_26_ret_type indirect_placeholder_26(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint64_t param_3, uint64_t param_4);
void bb_main(uint64_t rdi, uint64_t rsi) {
    struct indirect_placeholder_26_ret_type var_15;
    uint64_t var_0;
    uint64_t var_1;
    uint64_t var_2;
    uint64_t var_3;
    uint32_t *var_4;
    uint64_t var_5;
    uint64_t *var_6;
    bool var_7;
    unsigned char *var_8;
    uint64_t var_9;
    uint64_t var_10;
    uint32_t *var_11;
    uint64_t local_sp_3;
    uint64_t var_29;
    uint64_t local_sp_0;
    uint64_t var_19;
    uint64_t var_20;
    uint64_t var_21;
    uint64_t var_22;
    uint64_t local_sp_2;
    uint64_t *_pre_phi89;
    uint64_t var_23;
    uint64_t var_24;
    uint64_t *var_25;
    uint64_t local_sp_1;
    uint64_t var_26;
    bool var_27;
    uint64_t *var_28;
    uint64_t var_17;
    uint64_t var_18;
    uint64_t var_12;
    uint64_t var_13;
    uint64_t var_14;
    uint32_t var_16;
    unsigned int loop_state_var;
    var_0 = revng_init_local_sp(0UL);
    var_1 = init_rax();
    var_2 = init_rbp();
    var_3 = var_0 + (-8L);
    *(uint64_t *)var_3 = var_2;
    var_4 = (uint32_t *)(var_0 + (-44L));
    *var_4 = (uint32_t)rdi;
    var_5 = var_0 + (-56L);
    var_6 = (uint64_t *)var_5;
    *var_6 = rsi;
    *(uint64_t *)(var_0 + (-64L)) = 4204748UL;
    indirect_placeholder();
    var_7 = (var_1 != 0UL);
    var_8 = (unsigned char *)(var_0 + (-9L));
    *var_8 = var_7;
    var_9 = **(uint64_t **)var_5;
    *(uint64_t *)(var_0 + (-72L)) = 4204772UL;
    indirect_placeholder_23(var_9);
    *(uint64_t *)(var_0 + (-80L)) = 4204787UL;
    indirect_placeholder();
    var_10 = var_0 + (-88L);
    *(uint64_t *)var_10 = 4204797UL;
    indirect_placeholder();
    var_11 = (uint32_t *)(var_0 + (-16L));
    local_sp_3 = var_10;
    while (1U)
        {
            var_12 = *var_6;
            var_13 = (uint64_t)*var_4;
            var_14 = local_sp_3 + (-8L);
            *(uint64_t *)var_14 = 4204827UL;
            var_15 = indirect_placeholder_26(4276478UL, 4275520UL, var_13, var_12, 0UL);
            var_16 = (uint32_t)var_15.field_0;
            *var_11 = var_16;
            local_sp_0 = var_14;
            local_sp_2 = var_14;
            local_sp_3 = var_14;
            if (var_16 != 4294967295U) {
                if ((long)((uint64_t)*(uint32_t *)6386872UL << 32UL) >= (long)((uint64_t)*var_4 << 32UL)) {
                    loop_state_var = 1U;
                    break;
                }
                var_19 = var_15.field_3;
                var_20 = var_15.field_2;
                var_21 = var_15.field_1;
                var_22 = local_sp_3 + (-16L);
                *(uint64_t *)var_22 = 4205019UL;
                indirect_placeholder_24(0UL, 4276494UL, var_21, 0UL, 0UL, var_20, var_19);
                local_sp_0 = var_22;
                loop_state_var = 1U;
                break;
            }
            if ((uint64_t)(var_16 + 130U) == 0UL) {
                *(uint64_t *)(local_sp_3 + (-16L)) = 4204917UL;
                indirect_placeholder_2(var_3, 0UL);
                abort();
            }
            if ((int)var_16 > (int)4294967166U) {
                if ((uint64_t)(var_16 + (-76)) == 0UL) {
                    *var_8 = (unsigned char)'\x01';
                    continue;
                }
                if ((uint64_t)(var_16 + (-80)) != 0UL) {
                    loop_state_var = 0U;
                    break;
                }
                *var_8 = (unsigned char)'\x00';
                continue;
            }
            if (var_16 != 4294967165U) {
                loop_state_var = 0U;
                break;
            }
            var_17 = *(uint64_t *)6386720UL;
            *(uint64_t *)(local_sp_3 + (-16L)) = 4204969UL;
            indirect_placeholder_25(0UL, 4275224UL, var_17, 4276068UL, 0UL, 4276481UL);
            var_18 = local_sp_3 + (-24L);
            *(uint64_t *)var_18 = 4204979UL;
            indirect_placeholder();
            local_sp_2 = var_18;
            loop_state_var = 0U;
            break;
        }
    switch (loop_state_var) {
      case 0U:
        {
            *(uint64_t *)(local_sp_2 + (-8L)) = 4204989UL;
            indirect_placeholder_2(var_3, 1UL);
            abort();
        }
        break;
      case 1U:
        {
            local_sp_1 = local_sp_0;
            if (*var_8 != '\x00') {
                var_23 = local_sp_0 + (-8L);
                *(uint64_t *)var_23 = 4205030UL;
                var_24 = indirect_placeholder_3();
                var_25 = (uint64_t *)(var_0 + (-24L));
                *var_25 = var_24;
                _pre_phi89 = var_25;
                local_sp_1 = var_23;
                if (var_24 != 0UL) {
                    *(uint64_t *)(local_sp_0 + (-16L)) = 4205053UL;
                    indirect_placeholder();
                    return;
                }
            }
            _pre_phi89 = (uint64_t *)(var_0 + (-24L));
        }
        break;
    }
}

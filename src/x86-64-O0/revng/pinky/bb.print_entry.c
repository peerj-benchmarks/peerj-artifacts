typedef unsigned char __u_char;
typedef unsigned short __u_short;
typedef unsigned int __u_int;
typedef unsigned long __u_long;
typedef signed char __int8_t;
typedef unsigned char __uint8_t;
typedef short __int16_t;
typedef unsigned short __uint16_t;
typedef int __int32_t;
typedef unsigned int __uint32_t;
typedef long __int64_t;
typedef unsigned long __uint64_t;
typedef __int8_t __int_least8_t;
typedef __uint8_t __uint_least8_t;
typedef __int16_t __int_least16_t;
typedef __uint16_t __uint_least16_t;
typedef __int32_t __int_least32_t;
typedef __uint32_t __uint_least32_t;
typedef __int64_t __int_least64_t;
typedef __uint64_t __uint_least64_t;
typedef long __quad_t;
typedef unsigned long __u_quad_t;
typedef long __intmax_t;
typedef unsigned long __uintmax_t;
typedef unsigned long __dev_t;
typedef unsigned int __uid_t;
typedef unsigned int __gid_t;
typedef unsigned long __ino_t;
typedef unsigned long __ino64_t;
typedef unsigned int __mode_t;
typedef unsigned long __nlink_t;
typedef long __off_t;
typedef long __off64_t;
typedef int __pid_t;
typedef struct {
    int __val[2];
} __fsid_t;
typedef long __clock_t;
typedef unsigned long __rlim_t;
typedef unsigned long __rlim64_t;
typedef unsigned int __id_t;
typedef long __time_t;
typedef unsigned int __useconds_t;
typedef long __suseconds_t;
typedef int __daddr_t;
typedef int __key_t;
typedef int __clockid_t;
typedef void *__timer_t;
typedef long __blksize_t;
typedef long __blkcnt_t;
typedef long __blkcnt64_t;
typedef unsigned long __fsblkcnt_t;
typedef unsigned long __fsblkcnt64_t;
typedef unsigned long __fsfilcnt_t;
typedef unsigned long __fsfilcnt64_t;
typedef long __fsword_t;
typedef long __ssize_t;
typedef long __syscall_slong_t;
typedef unsigned long __syscall_ulong_t;
typedef __off64_t __loff_t;
typedef char *__caddr_t;
typedef long __intptr_t;
typedef unsigned int __socklen_t;
typedef int __sig_atomic_t;
typedef __int8_t int8_t;
typedef __int16_t int16_t;
typedef __int32_t int32_t;
typedef __int64_t int64_t;
typedef __uint8_t uint8_t;
typedef __uint16_t uint16_t;
typedef __uint32_t uint32_t;
typedef __uint64_t uint64_t;
typedef __int_least8_t int_least8_t;
typedef __int_least16_t int_least16_t;
typedef __int_least32_t int_least32_t;
typedef __int_least64_t int_least64_t;
typedef __uint_least8_t uint_least8_t;
typedef __uint_least16_t uint_least16_t;
typedef __uint_least32_t uint_least32_t;
typedef __uint_least64_t uint_least64_t;
typedef signed char int_fast8_t;
typedef long int_fast16_t;
typedef long int_fast32_t;
typedef long int_fast64_t;
typedef unsigned char uint_fast8_t;
typedef unsigned long uint_fast16_t;
typedef unsigned long uint_fast32_t;
typedef unsigned long uint_fast64_t;
typedef long intptr_t;
typedef unsigned long uintptr_t;
typedef __intmax_t intmax_t;
typedef __uintmax_t uintmax_t;
typedef _Bool bool;
extern uint64_t revng_init_local_sp(uint64_t param_0);
extern uint64_t init_rbp(void);
extern uint64_t indirect_placeholder_1(uint64_t param_0, uint64_t param_1);
extern void indirect_placeholder(void);
extern uint64_t indirect_placeholder_9(uint64_t param_0, uint64_t param_1, uint64_t param_2);
extern uint64_t indirect_placeholder_3(uint64_t param_0);
void bb_print_entry(uint64_t rdi) {
    uint64_t var_0;
    uint64_t var_1;
    uint64_t var_2;
    uint64_t *var_3;
    uint64_t var_4;
    uint64_t *var_5;
    uint64_t var_6;
    uint64_t _pre_phi;
    uint64_t var_38;
    uint64_t var_33;
    uint64_t local_sp_2;
    uint64_t var_34;
    bool var_35;
    uint64_t var_36;
    uint64_t *var_37;
    uint64_t *var_28;
    uint64_t *var_29;
    uint64_t var_30;
    uint64_t var_31;
    uint64_t var_32;
    uint64_t var_21;
    bool var_22;
    uint64_t var_23;
    uint64_t *var_24;
    uint64_t var_25;
    uint64_t local_sp_3;
    uint64_t var_26;
    uint64_t var_27;
    uint64_t var_16;
    uint64_t var_17;
    uint64_t var_18;
    uint64_t var_19;
    uint64_t var_10;
    uint64_t var_11;
    uint64_t var_12;
    uint64_t *var_13;
    uint64_t var_14;
    uint64_t var_15;
    uint64_t local_sp_4;
    uint64_t var_20;
    uint64_t var_9;
    uint64_t var_7;
    uint64_t local_sp_5;
    uint64_t var_8;
    var_0 = revng_init_local_sp(0UL);
    var_1 = init_rbp();
    *(uint64_t *)(var_0 + (-8L)) = var_1;
    var_2 = var_0 + (-552L);
    var_3 = (uint64_t *)(var_0 + (-544L));
    *var_3 = rdi;
    var_4 = var_0 + (-264L);
    var_5 = (uint64_t *)(var_0 + (-32L));
    *var_5 = var_4;
    var_6 = *var_3 + 8UL;
    var_34 = (uint64_t)0L;
    _pre_phi = var_6;
    local_sp_5 = var_2;
    if (*(unsigned char *)var_6 == '/') {
        var_7 = var_0 + (-560L);
        *(uint64_t *)var_7 = 4203333UL;
        indirect_placeholder();
        *var_5 = var_4;
        _pre_phi = *var_3 + 8UL;
        local_sp_5 = var_7;
    }
    *(uint64_t *)(local_sp_5 + (-8L)) = 4203368UL;
    indirect_placeholder_9(32UL, var_4, _pre_phi);
    *(uint64_t *)(local_sp_5 + (-16L)) = 4203393UL;
    var_8 = indirect_placeholder_3(var_4);
    if ((uint64_t)(uint32_t)var_8 == 0UL) {
        *(unsigned char *)(var_0 + (-17L)) = (((*(uint32_t *)(var_0 + (-192L)) & 16U) == 0U) ? '*' : ' ');
        *(uint64_t *)(var_0 + (-16L)) = *(uint64_t *)(var_0 + (-144L));
    } else {
        *(unsigned char *)(var_0 + (-17L)) = (unsigned char)'?';
        *(uint64_t *)(var_0 + (-16L)) = 0UL;
    }
    var_9 = local_sp_5 + (-24L);
    *(uint64_t *)var_9 = 4203484UL;
    indirect_placeholder();
    local_sp_4 = var_9;
    if (*(unsigned char *)6382626UL != '\x00') {
        var_10 = *var_3 + 44UL;
        var_11 = var_0 + (-536L);
        *(uint64_t *)(local_sp_5 + (-32L)) = 4203533UL;
        indirect_placeholder_9(32UL, var_11, var_10);
        *(uint64_t *)(local_sp_5 + (-40L)) = 4203548UL;
        indirect_placeholder();
        var_12 = var_0 + (-56L);
        var_13 = (uint64_t *)var_12;
        *var_13 = var_11;
        var_14 = *(uint64_t *)(var_11 + 24UL);
        *(uint64_t *)(local_sp_5 + (-48L)) = 4203602UL;
        indirect_placeholder();
        var_15 = var_0 + (-64L);
        *(uint64_t *)var_15 = var_14;
        if (var_14 != 0UL) {
            **(unsigned char **)var_15 = (unsigned char)'\x00';
        }
        var_16 = **(uint64_t **)var_12;
        var_17 = *(uint64_t *)(*var_13 + 24UL);
        *(uint64_t *)(local_sp_5 + (-56L)) = 4203646UL;
        var_18 = indirect_placeholder_1(var_17, var_16);
        *(uint64_t *)(var_0 + (-72L)) = var_18;
        *(uint64_t *)(local_sp_5 + (-64L)) = 4203672UL;
        indirect_placeholder();
        var_19 = local_sp_5 + (-72L);
        *(uint64_t *)var_19 = 4203684UL;
        indirect_placeholder();
        local_sp_4 = var_19;
    }
    var_20 = local_sp_4 + (-8L);
    *(uint64_t *)var_20 = 4203724UL;
    indirect_placeholder();
    local_sp_3 = var_20;
    if (*(unsigned char *)6382624UL != '\x00') {
        var_21 = *(uint64_t *)(var_0 + (-16L));
        var_22 = (var_21 == 0UL);
        var_23 = local_sp_4 + (-16L);
        var_24 = (uint64_t *)var_23;
        local_sp_3 = var_23;
        if (var_22) {
            *var_24 = 4203794UL;
            indirect_placeholder();
        } else {
            *var_24 = 4203754UL;
            indirect_placeholder_3(var_21);
            var_25 = local_sp_4 + (-24L);
            *(uint64_t *)var_25 = 4203772UL;
            indirect_placeholder();
            local_sp_3 = var_25;
        }
    }
    var_26 = *var_3;
    *(uint64_t *)(local_sp_3 + (-8L)) = 4203809UL;
    indirect_placeholder_3(var_26);
    var_27 = local_sp_3 + (-16L);
    *(uint64_t *)var_27 = 4203827UL;
    indirect_placeholder();
    local_sp_2 = var_27;
    if (~(*(unsigned char *)6382631UL != '\x00' & *(unsigned char *)(*var_3 + 76UL) != '\x00')) {
        *(uint64_t *)(local_sp_2 + (-8L)) = 4204097UL;
        indirect_placeholder();
        return;
    }
    var_28 = (uint64_t *)(var_0 + (-40L));
    *var_28 = 0UL;
    var_29 = (uint64_t *)(var_0 + (-48L));
    *var_29 = 0UL;
    var_30 = *var_3 + 76UL;
    var_31 = var_0 + (-536L);
    *(uint64_t *)(local_sp_3 + (-24L)) = 4203911UL;
    indirect_placeholder_9(256UL, var_31, var_30);
    var_32 = local_sp_3 + (-32L);
    *(uint64_t *)var_32 = 4203931UL;
    indirect_placeholder();
    *var_29 = (var_31 + 1UL);
    *(unsigned char *)var_31 = (unsigned char)'\x00';
    if (1) {
        var_34 = *var_28;
    } else {
        *(uint64_t *)(local_sp_3 + (-40L)) = 4203983UL;
        var_33 = indirect_placeholder_3(var_31);
        *var_28 = var_33;
    }
    if (var_34 == 0UL) {
        *var_28 = var_31;
    }
    var_35 = (*var_29 == 0UL);
    var_36 = var_32 + (-8L);
    var_37 = (uint64_t *)var_36;
    local_sp_2 = var_36;
    if (var_35) {
        *var_37 = 4204062UL;
        indirect_placeholder();
    } else {
        *var_37 = 4204038UL;
        indirect_placeholder();
    }
    if (*var_28 != var_31) {
        *(uint64_t *)(local_sp_2 + (-8L)) = 4204097UL;
        indirect_placeholder();
        return;
    }
    var_38 = var_36 + (-8L);
    *(uint64_t *)var_38 = 4204087UL;
    indirect_placeholder();
    local_sp_2 = var_38;
    *(uint64_t *)(local_sp_2 + (-8L)) = 4204097UL;
    indirect_placeholder();
    return;
}

typedef unsigned char __u_char;
typedef unsigned short __u_short;
typedef unsigned int __u_int;
typedef unsigned long __u_long;
typedef signed char __int8_t;
typedef unsigned char __uint8_t;
typedef short __int16_t;
typedef unsigned short __uint16_t;
typedef int __int32_t;
typedef unsigned int __uint32_t;
typedef long __int64_t;
typedef unsigned long __uint64_t;
typedef __int8_t __int_least8_t;
typedef __uint8_t __uint_least8_t;
typedef __int16_t __int_least16_t;
typedef __uint16_t __uint_least16_t;
typedef __int32_t __int_least32_t;
typedef __uint32_t __uint_least32_t;
typedef __int64_t __int_least64_t;
typedef __uint64_t __uint_least64_t;
typedef long __quad_t;
typedef unsigned long __u_quad_t;
typedef long __intmax_t;
typedef unsigned long __uintmax_t;
typedef unsigned long __dev_t;
typedef unsigned int __uid_t;
typedef unsigned int __gid_t;
typedef unsigned long __ino_t;
typedef unsigned long __ino64_t;
typedef unsigned int __mode_t;
typedef unsigned long __nlink_t;
typedef long __off_t;
typedef long __off64_t;
typedef int __pid_t;
typedef struct {
    int __val[2];
} __fsid_t;
typedef long __clock_t;
typedef unsigned long __rlim_t;
typedef unsigned long __rlim64_t;
typedef unsigned int __id_t;
typedef long __time_t;
typedef unsigned int __useconds_t;
typedef long __suseconds_t;
typedef int __daddr_t;
typedef int __key_t;
typedef int __clockid_t;
typedef void *__timer_t;
typedef long __blksize_t;
typedef long __blkcnt_t;
typedef long __blkcnt64_t;
typedef unsigned long __fsblkcnt_t;
typedef unsigned long __fsblkcnt64_t;
typedef unsigned long __fsfilcnt_t;
typedef unsigned long __fsfilcnt64_t;
typedef long __fsword_t;
typedef long __ssize_t;
typedef long __syscall_slong_t;
typedef unsigned long __syscall_ulong_t;
typedef __off64_t __loff_t;
typedef char *__caddr_t;
typedef long __intptr_t;
typedef unsigned int __socklen_t;
typedef int __sig_atomic_t;
typedef __int8_t int8_t;
typedef __int16_t int16_t;
typedef __int32_t int32_t;
typedef __int64_t int64_t;
typedef __uint8_t uint8_t;
typedef __uint16_t uint16_t;
typedef __uint32_t uint32_t;
typedef __uint64_t uint64_t;
typedef __int_least8_t int_least8_t;
typedef __int_least16_t int_least16_t;
typedef __int_least32_t int_least32_t;
typedef __int_least64_t int_least64_t;
typedef __uint_least8_t uint_least8_t;
typedef __uint_least16_t uint_least16_t;
typedef __uint_least32_t uint_least32_t;
typedef __uint_least64_t uint_least64_t;
typedef signed char int_fast8_t;
typedef long int_fast16_t;
typedef long int_fast32_t;
typedef long int_fast64_t;
typedef unsigned char uint_fast8_t;
typedef unsigned long uint_fast16_t;
typedef unsigned long uint_fast32_t;
typedef unsigned long uint_fast64_t;
typedef long intptr_t;
typedef unsigned long uintptr_t;
typedef __intmax_t intmax_t;
typedef __uintmax_t uintmax_t;
struct indirect_placeholder_16_ret_type;
struct indirect_placeholder_16_ret_type {
    uint64_t field_0;
    uint64_t field_1;
    uint64_t field_2;
    uint64_t field_3;
};
extern void abort(void);
extern uint64_t revng_init_local_sp(uint64_t param_0);
extern uint64_t init_rbp(void);
extern void indirect_placeholder(void);
extern void indirect_placeholder_10(uint64_t param_0, uint64_t param_1);
extern void indirect_placeholder_13(uint64_t param_0);
extern void indirect_placeholder_15(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint64_t param_3, uint64_t param_4, uint64_t param_5);
extern void indirect_placeholder_14(uint64_t param_0, uint64_t param_1, uint64_t param_2);
extern void indirect_placeholder_7(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint64_t param_3, uint64_t param_4, uint64_t param_5, uint64_t param_6);
extern struct indirect_placeholder_16_ret_type indirect_placeholder_16(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint64_t param_3, uint64_t param_4);
void bb_main(uint64_t rdi, uint64_t rsi) {
    uint64_t var_0;
    uint64_t var_1;
    uint64_t var_2;
    uint32_t *var_3;
    uint64_t var_4;
    uint64_t *var_5;
    uint64_t var_6;
    uint64_t var_7;
    uint32_t *var_8;
    uint64_t local_sp_1;
    uint64_t var_17;
    uint64_t var_18;
    uint64_t var_19;
    uint32_t var_20;
    unsigned char var_21;
    uint64_t var_24;
    uint64_t var_25;
    uint64_t var_22;
    uint64_t var_23;
    uint64_t local_sp_0;
    uint64_t var_14;
    uint64_t var_15;
    uint64_t *var_16;
    struct indirect_placeholder_16_ret_type var_12;
    uint64_t var_9;
    uint64_t var_10;
    uint64_t var_11;
    uint32_t var_13;
    unsigned int loop_state_var;
    var_0 = revng_init_local_sp(0UL);
    var_1 = init_rbp();
    var_2 = var_0 + (-8L);
    *(uint64_t *)var_2 = var_1;
    var_3 = (uint32_t *)(var_0 + (-28L));
    *var_3 = (uint32_t)rdi;
    var_4 = var_0 + (-40L);
    var_5 = (uint64_t *)var_4;
    *var_5 = rsi;
    var_6 = **(uint64_t **)var_4;
    *(uint64_t *)(var_0 + (-48L)) = 4205857UL;
    indirect_placeholder_13(var_6);
    *(uint64_t *)(var_0 + (-56L)) = 4205872UL;
    indirect_placeholder();
    var_7 = var_0 + (-64L);
    *(uint64_t *)var_7 = 4205882UL;
    indirect_placeholder();
    var_8 = (uint32_t *)(var_0 + (-12L));
    local_sp_1 = var_7;
    while (1U)
        {
            var_9 = *var_5;
            var_10 = (uint64_t)*var_3;
            var_11 = local_sp_1 + (-8L);
            *(uint64_t *)var_11 = 4206244UL;
            var_12 = indirect_placeholder_16(4272592UL, 4271232UL, var_10, var_9, 0UL);
            var_13 = (uint32_t)var_12.field_0;
            *var_8 = var_13;
            local_sp_0 = var_11;
            local_sp_1 = var_11;
            if (var_13 != 4294967295U) {
                var_17 = var_12.field_1;
                var_18 = var_12.field_2;
                var_19 = var_12.field_3;
                var_20 = *var_3 - *(uint32_t *)6382776UL;
                *(uint32_t *)(var_0 + (-16L)) = var_20;
                var_21 = *(unsigned char *)6382630UL;
                if (var_21 != '\x01') {
                    loop_state_var = 1U;
                    break;
                }
                if (var_20 == 0U) {
                    *(uint64_t *)(local_sp_1 + (-16L)) = 4206318UL;
                    indirect_placeholder_7(0UL, 4272608UL, var_17, 0UL, 0UL, var_18, var_19);
                    *(uint64_t *)(local_sp_1 + (-24L)) = 4206328UL;
                    indirect_placeholder_10(var_2, 1UL);
                    abort();
                }
                if (var_21 != '\x00') {
                    loop_state_var = 1U;
                    break;
                }
                var_22 = ((uint64_t)*(uint32_t *)6382776UL << 3UL) + *var_5;
                var_23 = (uint64_t)var_20;
                *(uint64_t *)(local_sp_1 + (-16L)) = 4206415UL;
                indirect_placeholder_10(var_23, var_22);
                loop_state_var = 2U;
                break;
            }
            if ((uint64_t)(var_13 + (-105)) == 0UL) {
                *(unsigned char *)6382626UL = (unsigned char)'\x00';
                *(unsigned char *)6382631UL = (unsigned char)'\x00';
                continue;
            }
            if ((int)var_13 > (int)105U) {
                if ((uint64_t)(var_13 + (-113)) == 0UL) {
                    *(unsigned char *)6382626UL = (unsigned char)'\x00';
                    *(unsigned char *)6382631UL = (unsigned char)'\x00';
                    *(unsigned char *)6382624UL = (unsigned char)'\x00';
                    continue;
                }
                if ((int)var_13 <= (int)113U) {
                    if ((uint64_t)(var_13 + (-108)) == 0UL) {
                        *(unsigned char *)6382630UL = (unsigned char)'\x00';
                        continue;
                    }
                    if ((uint64_t)(var_13 + (-112)) != 0UL) {
                        loop_state_var = 0U;
                        break;
                    }
                    *(unsigned char *)6382628UL = (unsigned char)'\x00';
                    continue;
                }
                if ((uint64_t)(var_13 + (-115)) == 0UL) {
                    *(unsigned char *)6382630UL = (unsigned char)'\x01';
                    continue;
                }
                if ((uint64_t)(var_13 + (-119)) != 0UL) {
                    loop_state_var = 0U;
                    break;
                }
                *(unsigned char *)6382626UL = (unsigned char)'\x00';
                continue;
            }
            if ((uint64_t)(var_13 + (-98)) == 0UL) {
                *(unsigned char *)6382629UL = (unsigned char)'\x00';
                continue;
            }
            if ((int)var_13 > (int)98U) {
                if ((uint64_t)(var_13 + (-102)) == 0UL) {
                    *(unsigned char *)6382625UL = (unsigned char)'\x00';
                    continue;
                }
                if ((uint64_t)(var_13 + (-104)) != 0UL) {
                    loop_state_var = 0U;
                    break;
                }
                *(unsigned char *)6382627UL = (unsigned char)'\x00';
                continue;
            }
            if ((uint64_t)(var_13 + 131U) == 0UL) {
                if ((uint64_t)(var_13 + 130U) != 0UL) {
                    loop_state_var = 0U;
                    break;
                }
                *(uint64_t *)(local_sp_1 + (-16L)) = 4206131UL;
                indirect_placeholder_10(var_2, 0UL);
                abort();
            }
            var_14 = *(uint64_t *)6382632UL;
            *(uint64_t *)(local_sp_1 + (-16L)) = 0UL;
            var_15 = local_sp_1 + (-24L);
            var_16 = (uint64_t *)var_15;
            *var_16 = 4272580UL;
            *(uint64_t *)(local_sp_1 + (-32L)) = 4206190UL;
            indirect_placeholder_15(0UL, 4271000UL, var_14, 4272541UL, 4272547UL, 4272563UL);
            *var_16 = 4206204UL;
            indirect_placeholder();
            local_sp_0 = var_15;
            loop_state_var = 0U;
            break;
        }
    switch (loop_state_var) {
      case 0U:
        {
            *(uint64_t *)(local_sp_0 + (-8L)) = 4206214UL;
            indirect_placeholder_10(var_2, 1UL);
            abort();
        }
        break;
      case 2U:
      case 1U:
        {
            switch (loop_state_var) {
              case 1U:
                {
                    var_24 = ((uint64_t)*(uint32_t *)6382776UL << 3UL) + *var_5;
                    var_25 = (uint64_t)var_20;
                    *(uint64_t *)(local_sp_1 + (-16L)) = 4206377UL;
                    indirect_placeholder_14(var_24, 4272438UL, var_25);
                }
                break;
              case 2U:
                {
                    return;
                }
                break;
            }
        }
        break;
    }
}

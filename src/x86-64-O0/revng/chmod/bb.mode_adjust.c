typedef unsigned char __u_char;
typedef unsigned short __u_short;
typedef unsigned int __u_int;
typedef unsigned long __u_long;
typedef signed char __int8_t;
typedef unsigned char __uint8_t;
typedef short __int16_t;
typedef unsigned short __uint16_t;
typedef int __int32_t;
typedef unsigned int __uint32_t;
typedef long __int64_t;
typedef unsigned long __uint64_t;
typedef __int8_t __int_least8_t;
typedef __uint8_t __uint_least8_t;
typedef __int16_t __int_least16_t;
typedef __uint16_t __uint_least16_t;
typedef __int32_t __int_least32_t;
typedef __uint32_t __uint_least32_t;
typedef __int64_t __int_least64_t;
typedef __uint64_t __uint_least64_t;
typedef long __quad_t;
typedef unsigned long __u_quad_t;
typedef long __intmax_t;
typedef unsigned long __uintmax_t;
typedef unsigned long __dev_t;
typedef unsigned int __uid_t;
typedef unsigned int __gid_t;
typedef unsigned long __ino_t;
typedef unsigned long __ino64_t;
typedef unsigned int __mode_t;
typedef unsigned long __nlink_t;
typedef long __off_t;
typedef long __off64_t;
typedef int __pid_t;
typedef struct {
    int __val[2];
} __fsid_t;
typedef long __clock_t;
typedef unsigned long __rlim_t;
typedef unsigned long __rlim64_t;
typedef unsigned int __id_t;
typedef long __time_t;
typedef unsigned int __useconds_t;
typedef long __suseconds_t;
typedef int __daddr_t;
typedef int __key_t;
typedef int __clockid_t;
typedef void *__timer_t;
typedef long __blksize_t;
typedef long __blkcnt_t;
typedef long __blkcnt64_t;
typedef unsigned long __fsblkcnt_t;
typedef unsigned long __fsblkcnt64_t;
typedef unsigned long __fsfilcnt_t;
typedef unsigned long __fsfilcnt64_t;
typedef long __fsword_t;
typedef long __ssize_t;
typedef long __syscall_slong_t;
typedef unsigned long __syscall_ulong_t;
typedef __off64_t __loff_t;
typedef char *__caddr_t;
typedef long __intptr_t;
typedef unsigned int __socklen_t;
typedef int __sig_atomic_t;
typedef __int8_t int8_t;
typedef __int16_t int16_t;
typedef __int32_t int32_t;
typedef __int64_t int64_t;
typedef __uint8_t uint8_t;
typedef __uint16_t uint16_t;
typedef __uint32_t uint32_t;
typedef __uint64_t uint64_t;
typedef __int_least8_t int_least8_t;
typedef __int_least16_t int_least16_t;
typedef __int_least32_t int_least32_t;
typedef __int_least64_t int_least64_t;
typedef __uint_least8_t uint_least8_t;
typedef __uint_least16_t uint_least16_t;
typedef __uint_least32_t uint_least32_t;
typedef __uint_least64_t uint_least64_t;
typedef signed char int_fast8_t;
typedef long int_fast16_t;
typedef long int_fast32_t;
typedef long int_fast64_t;
typedef unsigned char uint_fast8_t;
typedef unsigned long uint_fast16_t;
typedef unsigned long uint_fast32_t;
typedef unsigned long uint_fast64_t;
typedef long intptr_t;
typedef unsigned long uintptr_t;
typedef __intmax_t intmax_t;
typedef __uintmax_t uintmax_t;
extern uint64_t revng_init_local_sp(uint64_t param_0);
extern uint64_t init_rbp(void);
uint64_t bb_mode_adjust(uint64_t rdx, uint64_t rcx, uint64_t rsi, uint64_t rdi, uint64_t r8) {
    uint64_t var_0;
    uint64_t var_1;
    uint32_t *var_2;
    uint32_t *var_3;
    uint64_t var_4;
    uint64_t *var_5;
    uint64_t var_6;
    uint64_t *var_7;
    unsigned char *var_8;
    uint32_t var_9;
    uint32_t *var_10;
    uint32_t *var_11;
    uint32_t *var_12;
    uint32_t *var_13;
    uint32_t *var_14;
    uint32_t *var_15;
    uint64_t var_16;
    uint32_t storemerge1;
    uint32_t var_17;
    uint32_t var_18;
    uint32_t var_22;
    uint32_t var_19;
    uint32_t var_20;
    uint32_t var_21;
    uint32_t var_23;
    uint32_t var_24;
    uint32_t var_25;
    uint32_t var_26;
    uint32_t var_27;
    uint64_t var_28;
    var_0 = revng_init_local_sp(0UL);
    var_1 = init_rbp();
    *(uint64_t *)(var_0 + (-8L)) = var_1;
    var_2 = (uint32_t *)(var_0 + (-44L));
    *var_2 = (uint32_t)rdi;
    var_3 = (uint32_t *)(var_0 + (-52L));
    *var_3 = (uint32_t)rdx;
    var_4 = var_0 + (-64L);
    var_5 = (uint64_t *)var_4;
    *var_5 = rcx;
    var_6 = var_0 + (-72L);
    var_7 = (uint64_t *)var_6;
    *var_7 = r8;
    var_8 = (unsigned char *)(var_0 + (-48L));
    *var_8 = (unsigned char)rsi;
    var_9 = (uint32_t)((uint16_t)*var_2 & (unsigned short)4095U);
    var_10 = (uint32_t *)(var_0 + (-12L));
    *var_10 = var_9;
    var_11 = (uint32_t *)(var_0 + (-16L));
    *var_11 = 0U;
    var_12 = (uint32_t *)(var_0 + (-24L));
    var_13 = (uint32_t *)(var_0 + (-28L));
    var_14 = (uint32_t *)(var_0 + (-20L));
    var_15 = (uint32_t *)(var_0 + (-32L));
    var_16 = *var_5;
    while (*(unsigned char *)(var_16 + 1UL) != '\x00')
        {
            *var_12 = *(uint32_t *)(var_16 + 4UL);
            *var_13 = (((*var_8 == '\x00') ? 0U : 3072U) & (*(uint32_t *)(*var_5 + 12UL) ^ (-1)));
            var_17 = *(uint32_t *)(*var_5 + 8UL);
            *var_14 = var_17;
            var_18 = (uint32_t)(uint64_t)*(unsigned char *)(*var_5 + 1UL);
            var_22 = var_17;
            if ((uint64_t)(var_18 + (-2)) == 0UL) {
                if (((uint64_t)(*var_10 & 73U) | (uint64_t)*var_8) == 0UL) {
                    var_21 = var_17 | 73U;
                    *var_14 = var_21;
                    var_22 = var_21;
                }
            } else {
                if ((uint64_t)(var_18 + (-3)) == 0UL) {
                    var_19 = *var_10 & var_17;
                    var_20 = var_19 | ((((var_19 & 73U) == 0U) ? 0U : 73U) | ((((uint32_t)((uint16_t)var_19 & (unsigned short)292U) == 0U) ? 0U : 292U) | (((uint32_t)((unsigned char)var_19 & '\x92') == 0U) ? 0U : 146U)));
                    *var_14 = var_20;
                    var_22 = var_20;
                }
            }
            var_23 = *var_12;
            storemerge1 = var_23;
            if (var_23 == 0U) {
                storemerge1 = *var_3 ^ (-1);
            }
            var_24 = var_22 & (storemerge1 & (*var_13 ^ (-1)));
            *var_14 = var_24;
            var_25 = (uint32_t)(uint64_t)**(unsigned char **)var_4;
            if ((uint64_t)(var_25 + (-45)) == 0UL) {
                *var_11 = (var_24 | *var_11);
                *var_10 = (*var_10 & (*var_14 ^ (-1)));
            } else {
                if ((uint64_t)(var_25 + (-61)) == 0UL) {
                    var_26 = *var_12;
                    var_27 = ((var_26 == 0U) ? 0U : (var_26 ^ (-1))) | *var_13;
                    *var_15 = var_27;
                    *var_11 = (((uint32_t)((uint16_t)var_27 & (unsigned short)4095U) ^ 4095U) | *var_11);
                    *var_10 = ((*var_10 & *var_15) | *var_14);
                } else {
                    if ((uint64_t)(var_25 + (-43)) == 0UL) {
                        *var_11 = (var_24 | *var_11);
                        *var_10 = (*var_14 | *var_10);
                    }
                }
            }
            var_28 = *var_5 + 16UL;
            *var_5 = var_28;
            var_16 = var_28;
        }
    if (*var_7 == 0UL) {
        return (uint64_t)*var_10;
    }
    **(uint32_t **)var_6 = *var_11;
    return (uint64_t)*var_10;
}

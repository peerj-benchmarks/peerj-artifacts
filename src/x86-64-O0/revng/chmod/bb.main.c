typedef unsigned char __u_char;
typedef unsigned short __u_short;
typedef unsigned int __u_int;
typedef unsigned long __u_long;
typedef signed char __int8_t;
typedef unsigned char __uint8_t;
typedef short __int16_t;
typedef unsigned short __uint16_t;
typedef int __int32_t;
typedef unsigned int __uint32_t;
typedef long __int64_t;
typedef unsigned long __uint64_t;
typedef __int8_t __int_least8_t;
typedef __uint8_t __uint_least8_t;
typedef __int16_t __int_least16_t;
typedef __uint16_t __uint_least16_t;
typedef __int32_t __int_least32_t;
typedef __uint32_t __uint_least32_t;
typedef __int64_t __int_least64_t;
typedef __uint64_t __uint_least64_t;
typedef long __quad_t;
typedef unsigned long __u_quad_t;
typedef long __intmax_t;
typedef unsigned long __uintmax_t;
typedef unsigned long __dev_t;
typedef unsigned int __uid_t;
typedef unsigned int __gid_t;
typedef unsigned long __ino_t;
typedef unsigned long __ino64_t;
typedef unsigned int __mode_t;
typedef unsigned long __nlink_t;
typedef long __off_t;
typedef long __off64_t;
typedef int __pid_t;
typedef struct {
    int __val[2];
} __fsid_t;
typedef long __clock_t;
typedef unsigned long __rlim_t;
typedef unsigned long __rlim64_t;
typedef unsigned int __id_t;
typedef long __time_t;
typedef unsigned int __useconds_t;
typedef long __suseconds_t;
typedef int __daddr_t;
typedef int __key_t;
typedef int __clockid_t;
typedef void *__timer_t;
typedef long __blksize_t;
typedef long __blkcnt_t;
typedef long __blkcnt64_t;
typedef unsigned long __fsblkcnt_t;
typedef unsigned long __fsblkcnt64_t;
typedef unsigned long __fsfilcnt_t;
typedef unsigned long __fsfilcnt64_t;
typedef long __fsword_t;
typedef long __ssize_t;
typedef long __syscall_slong_t;
typedef unsigned long __syscall_ulong_t;
typedef __off64_t __loff_t;
typedef char *__caddr_t;
typedef long __intptr_t;
typedef unsigned int __socklen_t;
typedef int __sig_atomic_t;
typedef __int8_t int8_t;
typedef __int16_t int16_t;
typedef __int32_t int32_t;
typedef __int64_t int64_t;
typedef __uint8_t uint8_t;
typedef __uint16_t uint16_t;
typedef __uint32_t uint32_t;
typedef __uint64_t uint64_t;
typedef __int_least8_t int_least8_t;
typedef __int_least16_t int_least16_t;
typedef __int_least32_t int_least32_t;
typedef __int_least64_t int_least64_t;
typedef __uint_least8_t uint_least8_t;
typedef __uint_least16_t uint_least16_t;
typedef __uint_least32_t uint_least32_t;
typedef __uint_least64_t uint_least64_t;
typedef signed char int_fast8_t;
typedef long int_fast16_t;
typedef long int_fast32_t;
typedef long int_fast64_t;
typedef unsigned char uint_fast8_t;
typedef unsigned long uint_fast16_t;
typedef unsigned long uint_fast32_t;
typedef unsigned long uint_fast64_t;
typedef long intptr_t;
typedef unsigned long uintptr_t;
typedef __intmax_t intmax_t;
typedef __uintmax_t uintmax_t;
typedef _Bool bool;
struct indirect_placeholder_47_ret_type;
struct indirect_placeholder_46_ret_type;
struct indirect_placeholder_49_ret_type;
struct indirect_placeholder_48_ret_type;
struct indirect_placeholder_51_ret_type;
struct indirect_placeholder_50_ret_type;
struct indirect_placeholder_52_ret_type;
struct indirect_placeholder_54_ret_type;
struct indirect_placeholder_55_ret_type;
struct indirect_placeholder_53_ret_type;
struct indirect_placeholder_56_ret_type;
struct indirect_placeholder_58_ret_type;
struct indirect_placeholder_47_ret_type {
    uint64_t field_0;
    uint64_t field_1;
    uint64_t field_2;
};
struct indirect_placeholder_46_ret_type {
    uint64_t field_0;
    uint64_t field_1;
    uint64_t field_2;
    uint64_t field_3;
};
struct indirect_placeholder_49_ret_type {
    uint64_t field_0;
    uint64_t field_1;
    uint64_t field_2;
};
struct indirect_placeholder_48_ret_type {
    uint64_t field_0;
    uint64_t field_1;
    uint64_t field_2;
    uint64_t field_3;
};
struct indirect_placeholder_51_ret_type {
    uint64_t field_0;
    uint64_t field_1;
    uint64_t field_2;
};
struct indirect_placeholder_50_ret_type {
    uint64_t field_0;
    uint64_t field_1;
    uint64_t field_2;
    uint64_t field_3;
};
struct indirect_placeholder_52_ret_type {
    uint64_t field_0;
    uint64_t field_1;
};
struct indirect_placeholder_54_ret_type {
    uint64_t field_0;
    uint64_t field_1;
    uint64_t field_2;
    uint64_t field_3;
};
struct indirect_placeholder_55_ret_type {
    uint64_t field_0;
    uint64_t field_1;
    uint64_t field_2;
};
struct indirect_placeholder_53_ret_type {
    uint64_t field_0;
    uint64_t field_1;
    uint64_t field_2;
    uint64_t field_3;
};
struct indirect_placeholder_56_ret_type {
    uint64_t field_0;
    uint64_t field_1;
    uint64_t field_2;
    uint64_t field_3;
};
struct indirect_placeholder_58_ret_type {
    uint64_t field_0;
    uint64_t field_1;
    uint64_t field_2;
    uint64_t field_3;
};
extern void abort(void);
extern uint64_t revng_init_local_sp(uint64_t param_0);
extern uint64_t init_rbp(void);
extern uint64_t indirect_placeholder_1(uint64_t param_0, uint64_t param_1);
extern void indirect_placeholder(void);
extern void indirect_placeholder_11(uint64_t param_0, uint64_t param_1);
extern uint64_t init_rbx(void);
extern uint64_t init_r10(void);
extern uint64_t indirect_placeholder_2(uint64_t param_0);
extern void indirect_placeholder_22(uint64_t param_0);
extern uint64_t indirect_placeholder_17(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint64_t param_3, uint64_t param_4, uint64_t param_5);
extern struct indirect_placeholder_47_ret_type indirect_placeholder_47(uint64_t param_0, uint64_t param_1);
extern struct indirect_placeholder_46_ret_type indirect_placeholder_46(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint64_t param_3, uint64_t param_4, uint64_t param_5, uint64_t param_6);
extern struct indirect_placeholder_49_ret_type indirect_placeholder_49(uint64_t param_0);
extern struct indirect_placeholder_48_ret_type indirect_placeholder_48(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint64_t param_3, uint64_t param_4, uint64_t param_5, uint64_t param_6);
extern struct indirect_placeholder_51_ret_type indirect_placeholder_51(uint64_t param_0, uint64_t param_1);
extern struct indirect_placeholder_50_ret_type indirect_placeholder_50(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint64_t param_3, uint64_t param_4, uint64_t param_5, uint64_t param_6);
extern struct indirect_placeholder_52_ret_type indirect_placeholder_52(uint64_t param_0);
extern struct indirect_placeholder_54_ret_type indirect_placeholder_54(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint64_t param_3, uint64_t param_4, uint64_t param_5, uint64_t param_6);
extern struct indirect_placeholder_55_ret_type indirect_placeholder_55(uint64_t param_0);
extern struct indirect_placeholder_53_ret_type indirect_placeholder_53(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint64_t param_3, uint64_t param_4, uint64_t param_5, uint64_t param_6);
extern struct indirect_placeholder_56_ret_type indirect_placeholder_56(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint64_t param_3, uint64_t param_4, uint64_t param_5, uint64_t param_6);
extern void indirect_placeholder_57(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint64_t param_3, uint64_t param_4, uint64_t param_5);
extern struct indirect_placeholder_58_ret_type indirect_placeholder_58(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint64_t param_3, uint64_t param_4);
void bb_main(uint64_t rsi, uint64_t rdi) {
    struct indirect_placeholder_58_ret_type var_24;
    uint64_t local_sp_1;
    uint64_t var_0;
    uint64_t var_1;
    uint64_t var_2;
    uint64_t var_3;
    uint64_t var_4;
    uint32_t *var_5;
    uint64_t var_6;
    uint64_t *var_7;
    uint64_t *var_8;
    uint64_t *var_9;
    uint64_t var_10;
    uint64_t *var_11;
    unsigned char *var_12;
    uint64_t *var_13;
    uint64_t var_14;
    uint64_t var_15;
    uint32_t *var_16;
    uint64_t *var_17;
    uint64_t *var_18;
    uint64_t *var_19;
    uint64_t *var_20;
    struct indirect_placeholder_47_ret_type var_82;
    uint64_t var_83;
    uint64_t var_84;
    uint64_t var_85;
    uint64_t var_86;
    uint64_t var_87;
    uint64_t local_sp_2;
    uint64_t rcx_0;
    uint64_t var_90;
    struct indirect_placeholder_49_ret_type var_91;
    uint64_t var_92;
    uint64_t var_93;
    uint64_t var_94;
    uint64_t var_78;
    uint64_t var_64;
    struct indirect_placeholder_51_ret_type var_65;
    uint64_t var_66;
    uint64_t var_67;
    uint64_t var_68;
    uint64_t var_69;
    uint64_t var_70;
    struct indirect_placeholder_50_ret_type var_71;
    uint64_t var_72;
    uint64_t var_73;
    uint64_t var_74;
    uint64_t var_75;
    uint64_t _pre229;
    uint64_t var_76;
    uint64_t var_79;
    struct indirect_placeholder_52_ret_type var_80;
    uint64_t var_81;
    uint64_t var_88;
    uint64_t var_89;
    uint64_t local_sp_3;
    uint64_t var_42;
    bool var_43;
    uint64_t var_44;
    bool var_45;
    uint64_t var_48;
    uint32_t var_46;
    uint64_t var_47;
    uint32_t var_49;
    uint64_t var_50;
    uint32_t var_51;
    uint64_t var_52;
    uint64_t var_53;
    uint64_t var_54;
    struct indirect_placeholder_55_ret_type var_55;
    uint64_t var_56;
    uint64_t var_57;
    uint64_t var_58;
    uint64_t var_59;
    uint64_t var_60;
    uint64_t var_61;
    uint64_t var_62;
    uint64_t var_63;
    uint64_t local_sp_4;
    uint64_t r8_0;
    uint64_t r10_0;
    uint64_t r9_0;
    uint64_t rbx_0;
    uint64_t var_77;
    uint64_t local_sp_7;
    uint64_t var_37;
    uint64_t var_38;
    uint64_t var_39;
    uint64_t var_40;
    uint64_t local_sp_5;
    uint64_t var_41;
    uint64_t local_sp_7_be;
    uint64_t local_sp_6;
    uint64_t var_29;
    uint64_t var_30;
    uint64_t *var_31;
    uint64_t var_32;
    uint64_t var_33;
    uint64_t var_34;
    uint64_t var_35;
    uint64_t var_36;
    uint64_t var_21;
    uint64_t var_22;
    uint64_t var_23;
    uint64_t var_25;
    uint64_t var_26;
    uint64_t var_27;
    uint32_t var_28;
    unsigned int loop_state_var;
    var_0 = revng_init_local_sp(0UL);
    var_1 = init_rbp();
    var_2 = init_r10();
    var_3 = init_rbx();
    var_4 = var_0 + (-8L);
    *(uint64_t *)var_4 = var_1;
    *(uint64_t *)(var_0 + (-16L)) = var_3;
    var_5 = (uint32_t *)(var_0 + (-124L));
    *var_5 = (uint32_t)rdi;
    var_6 = var_0 + (-136L);
    var_7 = (uint64_t *)var_6;
    *var_7 = rsi;
    var_8 = (uint64_t *)(var_0 + (-32L));
    *var_8 = 0UL;
    var_9 = (uint64_t *)(var_0 + (-40L));
    *var_9 = 0UL;
    var_10 = var_0 + (-112L);
    var_11 = (uint64_t *)var_10;
    *var_11 = 0UL;
    var_12 = (unsigned char *)(var_0 + (-41L));
    *var_12 = (unsigned char)'\x00';
    var_13 = (uint64_t *)(var_0 + (-56L));
    *var_13 = 0UL;
    var_14 = **(uint64_t **)var_6;
    *(uint64_t *)(var_0 + (-144L)) = 4205119UL;
    indirect_placeholder_22(var_14);
    *(uint64_t *)(var_0 + (-152L)) = 4205134UL;
    indirect_placeholder();
    var_15 = var_0 + (-160L);
    *(uint64_t *)var_15 = 4205144UL;
    indirect_placeholder();
    *(unsigned char *)6407582UL = (unsigned char)'\x00';
    *(unsigned char *)6407581UL = (unsigned char)'\x00';
    *(unsigned char *)6407580UL = (unsigned char)'\x00';
    var_16 = (uint32_t *)(var_0 + (-60L));
    var_17 = (uint64_t *)(var_0 + (-72L));
    var_18 = (uint64_t *)(var_0 + (-80L));
    var_19 = (uint64_t *)(var_0 + (-88L));
    var_20 = (uint64_t *)(var_0 + (-96L));
    r10_0 = var_2;
    rbx_0 = var_3;
    local_sp_7 = var_15;
    while (1U)
        {
            var_21 = *var_7;
            var_22 = (uint64_t)*var_5;
            var_23 = local_sp_7 + (-8L);
            *(uint64_t *)var_23 = 4205811UL;
            var_24 = indirect_placeholder_58(4294144UL, 4291904UL, var_21, var_22, 0UL);
            var_25 = var_24.field_0;
            var_26 = var_24.field_2;
            var_27 = var_24.field_3;
            var_28 = (uint32_t)var_25;
            *var_16 = var_28;
            local_sp_4 = var_23;
            r8_0 = var_26;
            r9_0 = var_27;
            local_sp_7_be = var_23;
            local_sp_6 = var_23;
            if (var_28 != 4294967295U) {
                var_42 = var_24.field_1;
                var_43 = (*var_13 == 0UL);
                var_44 = *var_8;
                var_45 = (var_44 == 0UL);
                var_48 = var_44;
                rcx_0 = var_42;
                if (var_43) {
                    var_48 = 0UL;
                    if (!var_45) {
                        loop_state_var = 0U;
                        break;
                    }
                    *(uint64_t *)(local_sp_7 + (-16L)) = 4205863UL;
                    indirect_placeholder_54(0UL, 4294216UL, var_42, 0UL, 0UL, var_26, var_27);
                    *(uint64_t *)(local_sp_7 + (-24L)) = 4205873UL;
                    indirect_placeholder_11(var_4, 1UL);
                    abort();
                }
                if (!var_45) {
                    loop_state_var = 0U;
                    break;
                }
                var_46 = *(uint32_t *)6407384UL;
                *(uint32_t *)6407384UL = (var_46 + 1U);
                var_47 = *(uint64_t *)(*var_7 + ((uint64_t)var_46 << 3UL));
                *var_8 = var_47;
                var_48 = var_47;
                loop_state_var = 0U;
                break;
            }
            if ((uint64_t)(var_28 + (-99)) == 0UL) {
                *(uint32_t *)6407232UL = 1U;
            } else {
                if ((int)var_28 > (int)99U) {
                    if ((uint64_t)(var_28 + (-118)) != 0UL) {
                        *(uint32_t *)6407232UL = 0U;
                        local_sp_7 = local_sp_7_be;
                        continue;
                    }
                    if ((int)var_28 <= (int)118U) {
                        if ((uint64_t)(var_28 + (-111)) != 0UL) {
                            if ((int)var_28 > (int)111U) {
                                if ((int)var_28 >= (int)114U) {
                                    loop_state_var = 1U;
                                    break;
                                }
                            }
                            if ((uint64_t)(var_28 + (-102)) == 0UL) {
                                if ((uint64_t)(var_28 + (-103)) != 0UL) {
                                    loop_state_var = 1U;
                                    break;
                                }
                            }
                            *(unsigned char *)6407581UL = (unsigned char)'\x01';
                            local_sp_7 = local_sp_7_be;
                            continue;
                        }
                    }
                    if ((uint64_t)(var_28 + (-128)) != 0UL) {
                        *var_12 = (unsigned char)'\x00';
                        local_sp_7 = local_sp_7_be;
                        continue;
                    }
                    if ((int)var_28 <= (int)128U) {
                        if ((uint64_t)(var_28 + (-129)) == 0UL) {
                            *var_12 = (unsigned char)'\x01';
                        } else {
                            if ((uint64_t)(var_28 + (-130)) != 0UL) {
                                loop_state_var = 1U;
                                break;
                            }
                            *var_13 = *(uint64_t *)6408096UL;
                        }
                        local_sp_7 = local_sp_7_be;
                        continue;
                    }
                    if ((int)var_28 <= (int)120U) {
                        loop_state_var = 1U;
                        break;
                    }
                }
                if ((int)var_28 > (int)55U) {
                    if ((uint64_t)(var_28 + (-82)) != 0UL) {
                        *(unsigned char *)6407580UL = (unsigned char)'\x01';
                        local_sp_7 = local_sp_7_be;
                        continue;
                    }
                    if ((int)var_28 <= (int)82U) {
                        if ((uint64_t)(var_28 + (-61)) != 0UL) {
                            loop_state_var = 1U;
                            break;
                        }
                    }
                    if ((uint64_t)(var_28 + (-88)) != 0UL & (uint64_t)(var_28 + (-97)) != 0UL) {
                        loop_state_var = 1U;
                        break;
                    }
                }
                if ((int)var_28 >= (int)48U) {
                    if ((uint64_t)(var_28 + 130U) == 0UL) {
                        *(uint64_t *)(local_sp_7 + (-16L)) = 4205699UL;
                        indirect_placeholder_11(var_4, 0UL);
                        abort();
                    }
                    if ((int)var_28 <= (int)4294967166U) {
                        if (var_28 != 4294967165U) {
                            loop_state_var = 1U;
                            break;
                        }
                        var_29 = *(uint64_t *)6407240UL;
                        var_30 = local_sp_7 + (-24L);
                        var_31 = (uint64_t *)var_30;
                        *var_31 = 0UL;
                        *(uint64_t *)(local_sp_7 + (-32L)) = 4205757UL;
                        indirect_placeholder_57(0UL, 4291544UL, var_29, 4294107UL, 4294126UL, 4294113UL);
                        *var_31 = 4205771UL;
                        indirect_placeholder();
                        local_sp_6 = var_30;
                        loop_state_var = 1U;
                        break;
                    }
                    if ((uint64_t)((var_28 + (-43)) & (-2)) == 0UL) {
                        loop_state_var = 1U;
                        break;
                    }
                }
                var_32 = *(uint64_t *)(*var_7 + (((uint64_t)*(uint32_t *)6407384UL << 3UL) + (-8L)));
                *var_17 = var_32;
                var_33 = local_sp_7 + (-16L);
                *(uint64_t *)var_33 = 4205459UL;
                indirect_placeholder();
                *var_18 = var_32;
                var_34 = *var_9;
                var_35 = var_34 + (var_34 != 0UL);
                *var_19 = var_35;
                var_36 = *var_18 + var_35;
                *var_20 = var_36;
                local_sp_5 = var_33;
                if (*var_11 > var_36) {
                    var_40 = *var_8;
                } else {
                    *var_11 = (var_36 + 1UL);
                    var_37 = *var_8;
                    var_38 = local_sp_7 + (-24L);
                    *(uint64_t *)var_38 = 4205541UL;
                    var_39 = indirect_placeholder_17(var_10, var_37, var_26, var_2, var_27, var_3);
                    *var_8 = var_39;
                    var_40 = var_39;
                    local_sp_5 = var_38;
                }
                *(unsigned char *)(*var_9 + var_40) = (unsigned char)',';
                var_41 = local_sp_5 + (-8L);
                *(uint64_t *)var_41 = 4205593UL;
                indirect_placeholder();
                *var_9 = *var_20;
                *(unsigned char *)6407582UL = (unsigned char)'\x01';
                local_sp_7_be = var_41;
            }
        }
    switch (loop_state_var) {
      case 1U:
        {
            *(uint64_t *)(local_sp_6 + (-8L)) = 4205781UL;
            indirect_placeholder_11(var_4, 1UL);
            abort();
        }
        break;
      case 0U:
        {
            var_49 = *(uint32_t *)6407384UL;
            var_50 = (uint64_t)var_49;
            var_51 = *var_5;
            var_76 = var_48;
            if ((long)(var_50 << 32UL) >= (long)((uint64_t)var_51 << 32UL)) {
                if (var_48 == 0UL) {
                    var_60 = local_sp_7 + (-16L);
                    *(uint64_t *)var_60 = 4205998UL;
                    indirect_placeholder_56(0UL, 4294260UL, var_42, 0UL, 0UL, var_26, var_27);
                    local_sp_3 = var_60;
                } else {
                    var_52 = ((uint64_t)var_49 << 3UL) + (-8L);
                    var_53 = *var_7;
                    if (*(uint64_t *)(var_53 + var_52) == var_48) {
                        var_54 = *(uint64_t *)(var_53 + (((uint64_t)var_51 << 3UL) + (-8L)));
                        *(uint64_t *)(local_sp_7 + (-16L)) = 4206031UL;
                        var_55 = indirect_placeholder_55(var_54);
                        var_56 = var_55.field_0;
                        var_57 = var_55.field_1;
                        var_58 = var_55.field_2;
                        var_59 = local_sp_7 + (-24L);
                        *(uint64_t *)var_59 = 4206059UL;
                        indirect_placeholder_53(0UL, 4294276UL, var_56, 0UL, 0UL, var_57, var_58);
                        local_sp_3 = var_59;
                    } else {
                        var_60 = local_sp_7 + (-16L);
                        *(uint64_t *)var_60 = 4205998UL;
                        indirect_placeholder_56(0UL, 4294260UL, var_42, 0UL, 0UL, var_26, var_27);
                        local_sp_3 = var_60;
                    }
                }
                *(uint64_t *)(local_sp_3 + (-8L)) = 4206069UL;
                indirect_placeholder_11(var_4, 1UL);
                abort();
            }
            var_61 = *var_13;
            if (var_61 == 0UL) {
                var_62 = local_sp_7 + (-16L);
                *(uint64_t *)var_62 = 4206088UL;
                var_63 = indirect_placeholder_2(var_61);
                *(uint64_t *)6407568UL = var_63;
                local_sp_1 = var_62;
                if (var_63 != 0UL) {
                    var_64 = *var_13;
                    *(uint64_t *)(local_sp_7 + (-24L)) = 4206128UL;
                    var_65 = indirect_placeholder_51(var_64, 4UL);
                    var_66 = var_65.field_0;
                    var_67 = var_65.field_1;
                    var_68 = var_65.field_2;
                    *(uint64_t *)(local_sp_7 + (-32L)) = 4206136UL;
                    indirect_placeholder();
                    var_69 = (uint64_t)*(uint32_t *)var_66;
                    var_70 = local_sp_7 + (-40L);
                    *(uint64_t *)var_70 = 4206163UL;
                    var_71 = indirect_placeholder_50(0UL, 4294304UL, var_66, var_69, 1UL, var_67, var_68);
                    var_72 = var_71.field_0;
                    var_73 = var_71.field_1;
                    var_74 = var_71.field_2;
                    var_75 = var_71.field_3;
                    _pre229 = *var_8;
                    rcx_0 = var_72;
                    var_76 = _pre229;
                    local_sp_4 = var_70;
                    r8_0 = var_73;
                    r10_0 = var_74;
                    r9_0 = var_75;
                    rbx_0 = var_66;
                    *(uint64_t *)(local_sp_4 + (-8L)) = 4206175UL;
                    var_77 = indirect_placeholder_17(rcx_0, var_76, r8_0, r10_0, r9_0, rbx_0);
                    *(uint64_t *)6407568UL = var_77;
                    if (var_77 != 0UL) {
                        var_90 = *var_8;
                        *(uint64_t *)(local_sp_4 + (-16L)) = 4206206UL;
                        var_91 = indirect_placeholder_49(var_90);
                        var_92 = var_91.field_0;
                        var_93 = var_91.field_1;
                        var_94 = var_91.field_2;
                        *(uint64_t *)(local_sp_4 + (-24L)) = 4206234UL;
                        indirect_placeholder_48(0UL, 4294335UL, var_92, 0UL, 0UL, var_93, var_94);
                        *(uint64_t *)(local_sp_4 + (-32L)) = 4206244UL;
                        indirect_placeholder_11(var_4, 1UL);
                        abort();
                    }
                    var_78 = local_sp_4 + (-16L);
                    *(uint64_t *)var_78 = 4206254UL;
                    indirect_placeholder();
                    *(uint32_t *)6407576UL = (uint32_t)var_77;
                    local_sp_1 = var_78;
                }
            } else {
                *(uint64_t *)(local_sp_4 + (-8L)) = 4206175UL;
                var_77 = indirect_placeholder_17(rcx_0, var_76, r8_0, r10_0, r9_0, rbx_0);
                *(uint64_t *)6407568UL = var_77;
                if (var_77 == 0UL) {
                    var_90 = *var_8;
                    *(uint64_t *)(local_sp_4 + (-16L)) = 4206206UL;
                    var_91 = indirect_placeholder_49(var_90);
                    var_92 = var_91.field_0;
                    var_93 = var_91.field_1;
                    var_94 = var_91.field_2;
                    *(uint64_t *)(local_sp_4 + (-24L)) = 4206234UL;
                    indirect_placeholder_48(0UL, 4294335UL, var_92, 0UL, 0UL, var_93, var_94);
                    *(uint64_t *)(local_sp_4 + (-32L)) = 4206244UL;
                    indirect_placeholder_11(var_4, 1UL);
                    abort();
                }
                var_78 = local_sp_4 + (-16L);
                *(uint64_t *)var_78 = 4206254UL;
                indirect_placeholder();
                *(uint32_t *)6407576UL = (uint32_t)var_77;
                local_sp_1 = var_78;
            }
            local_sp_2 = local_sp_1;
            if (*(unsigned char *)6407580UL == '\x00') {
                *(uint64_t *)6407584UL = 0UL;
            } else {
                if (*var_12 == '\x00') {
                    *(uint64_t *)6407584UL = 0UL;
                } else {
                    var_79 = local_sp_1 + (-8L);
                    *(uint64_t *)var_79 = 4206287UL;
                    var_80 = indirect_placeholder_52(6407600UL);
                    var_81 = var_80.field_0;
                    *(uint64_t *)6407584UL = var_81;
                    local_sp_2 = var_79;
                    if (var_81 == 0UL) {
                        *(uint64_t *)(local_sp_1 + (-16L)) = 4206321UL;
                        var_82 = indirect_placeholder_47(4292770UL, 4UL);
                        var_83 = var_82.field_0;
                        var_84 = var_82.field_1;
                        var_85 = var_82.field_2;
                        *(uint64_t *)(local_sp_1 + (-24L)) = 4206329UL;
                        indirect_placeholder();
                        var_86 = (uint64_t)*(uint32_t *)var_83;
                        var_87 = local_sp_1 + (-32L);
                        *(uint64_t *)var_87 = 4206356UL;
                        indirect_placeholder_46(0UL, 4294304UL, var_83, var_86, 1UL, var_84, var_85);
                        local_sp_2 = var_87;
                    }
                }
            }
            var_88 = *var_7 + ((uint64_t)*(uint32_t *)6407384UL << 3UL);
            *(uint64_t *)(local_sp_2 + (-8L)) = 4206405UL;
            var_89 = indirect_placeholder_1(1041UL, var_88);
            *(unsigned char *)(var_0 + (-97L)) = (unsigned char)var_89;
            return;
        }
        break;
    }
}

typedef unsigned char __u_char;
typedef unsigned short __u_short;
typedef unsigned int __u_int;
typedef unsigned long __u_long;
typedef signed char __int8_t;
typedef unsigned char __uint8_t;
typedef short __int16_t;
typedef unsigned short __uint16_t;
typedef int __int32_t;
typedef unsigned int __uint32_t;
typedef long __int64_t;
typedef unsigned long __uint64_t;
typedef __int8_t __int_least8_t;
typedef __uint8_t __uint_least8_t;
typedef __int16_t __int_least16_t;
typedef __uint16_t __uint_least16_t;
typedef __int32_t __int_least32_t;
typedef __uint32_t __uint_least32_t;
typedef __int64_t __int_least64_t;
typedef __uint64_t __uint_least64_t;
typedef long __quad_t;
typedef unsigned long __u_quad_t;
typedef long __intmax_t;
typedef unsigned long __uintmax_t;
typedef unsigned long __dev_t;
typedef unsigned int __uid_t;
typedef unsigned int __gid_t;
typedef unsigned long __ino_t;
typedef unsigned long __ino64_t;
typedef unsigned int __mode_t;
typedef unsigned long __nlink_t;
typedef long __off_t;
typedef long __off64_t;
typedef int __pid_t;
typedef struct {
    int __val[2];
} __fsid_t;
typedef long __clock_t;
typedef unsigned long __rlim_t;
typedef unsigned long __rlim64_t;
typedef unsigned int __id_t;
typedef long __time_t;
typedef unsigned int __useconds_t;
typedef long __suseconds_t;
typedef int __daddr_t;
typedef int __key_t;
typedef int __clockid_t;
typedef void *__timer_t;
typedef long __blksize_t;
typedef long __blkcnt_t;
typedef long __blkcnt64_t;
typedef unsigned long __fsblkcnt_t;
typedef unsigned long __fsblkcnt64_t;
typedef unsigned long __fsfilcnt_t;
typedef unsigned long __fsfilcnt64_t;
typedef long __fsword_t;
typedef long __ssize_t;
typedef long __syscall_slong_t;
typedef unsigned long __syscall_ulong_t;
typedef __off64_t __loff_t;
typedef char *__caddr_t;
typedef long __intptr_t;
typedef unsigned int __socklen_t;
typedef int __sig_atomic_t;
typedef __int8_t int8_t;
typedef __int16_t int16_t;
typedef __int32_t int32_t;
typedef __int64_t int64_t;
typedef __uint8_t uint8_t;
typedef __uint16_t uint16_t;
typedef __uint32_t uint32_t;
typedef __uint64_t uint64_t;
typedef __int_least8_t int_least8_t;
typedef __int_least16_t int_least16_t;
typedef __int_least32_t int_least32_t;
typedef __int_least64_t int_least64_t;
typedef __uint_least8_t uint_least8_t;
typedef __uint_least16_t uint_least16_t;
typedef __uint_least32_t uint_least32_t;
typedef __uint_least64_t uint_least64_t;
typedef signed char int_fast8_t;
typedef long int_fast16_t;
typedef long int_fast32_t;
typedef long int_fast64_t;
typedef unsigned char uint_fast8_t;
typedef unsigned long uint_fast16_t;
typedef unsigned long uint_fast32_t;
typedef unsigned long uint_fast64_t;
typedef long intptr_t;
typedef unsigned long uintptr_t;
typedef __intmax_t intmax_t;
typedef __uintmax_t uintmax_t;
typedef _Bool bool;
struct helper_divq_EAX_wrapper_ret_type;
struct type_5;
struct indirect_placeholder_73_ret_type;
struct indirect_placeholder_74_ret_type;
struct indirect_placeholder_76_ret_type;
struct indirect_placeholder_77_ret_type;
struct indirect_placeholder_78_ret_type;
struct indirect_placeholder_79_ret_type;
struct indirect_placeholder_80_ret_type;
struct indirect_placeholder_83_ret_type;
struct helper_divq_EAX_wrapper_ret_type {
    uint64_t field_0;
    uint64_t field_1;
    uint64_t field_2;
    uint32_t field_3;
    uint64_t field_4;
    uint64_t field_5;
    uint32_t field_6;
    uint64_t field_7;
    uint32_t field_8;
    uint32_t field_9;
    unsigned char field_10;
    uint32_t field_11;
    uint32_t field_12;
};
struct type_5 {
};
struct indirect_placeholder_73_ret_type {
    uint64_t field_0;
    uint64_t field_1;
    uint64_t field_2;
};
struct indirect_placeholder_74_ret_type {
    uint64_t field_0;
    uint64_t field_1;
};
struct indirect_placeholder_76_ret_type {
    uint64_t field_0;
    uint64_t field_1;
    uint64_t field_2;
};
struct indirect_placeholder_77_ret_type {
    uint64_t field_0;
    uint64_t field_1;
    uint64_t field_2;
};
struct indirect_placeholder_78_ret_type {
    uint64_t field_0;
    uint64_t field_1;
    uint64_t field_2;
};
struct indirect_placeholder_79_ret_type {
    uint64_t field_0;
    uint64_t field_1;
};
struct indirect_placeholder_80_ret_type {
    uint64_t field_0;
    uint64_t field_1;
};
struct indirect_placeholder_83_ret_type {
    uint64_t field_0;
    uint64_t field_1;
};
extern void abort(void);
extern uint64_t revng_init_local_sp(uint64_t param_0);
extern uint64_t init_rbx(void);
extern uint64_t init_rbp(void);
extern uint64_t init_cc_src2(void);
extern uint64_t indirect_placeholder_2(uint64_t param_0, uint64_t param_1);
extern uint64_t helper_cc_compute_c_wrapper(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint32_t param_3);
extern void indirect_placeholder(void);
extern void indirect_placeholder_45(uint64_t param_0, uint64_t param_1, uint64_t param_2);
extern uint64_t indirect_placeholder_5(uint64_t param_0, uint64_t param_1, uint64_t param_2);
extern uint64_t init_state_0x9018(void);
extern uint32_t init_state_0x9010(void);
extern uint64_t init_state_0x8408(void);
extern uint64_t init_state_0x8328(void);
extern uint64_t init_state_0x82d8(void);
extern uint32_t init_state_0x9080(void);
extern uint32_t init_state_0x8248(void);
extern struct helper_divq_EAX_wrapper_ret_type helper_divq_EAX_wrapper(struct type_5 *param_0, uint64_t param_1, uint64_t param_2, uint64_t param_3, uint64_t param_4, uint64_t param_5, uint64_t param_6, uint64_t param_7, uint64_t param_8, uint64_t param_9, uint64_t param_10, uint64_t param_11, uint64_t param_12, uint64_t param_13, uint32_t param_14, uint64_t param_15, uint64_t param_16, uint64_t param_17, uint32_t param_18, uint32_t param_19);
extern struct indirect_placeholder_73_ret_type indirect_placeholder_73(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint64_t param_3, uint64_t param_4, uint64_t param_5, uint64_t param_6);
extern struct indirect_placeholder_74_ret_type indirect_placeholder_74(uint64_t param_0, uint64_t param_1);
extern struct indirect_placeholder_76_ret_type indirect_placeholder_76(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint64_t param_3, uint64_t param_4, uint64_t param_5, uint64_t param_6);
extern struct indirect_placeholder_77_ret_type indirect_placeholder_77(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint64_t param_3, uint64_t param_4, uint64_t param_5, uint64_t param_6);
extern struct indirect_placeholder_78_ret_type indirect_placeholder_78(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint64_t param_3, uint64_t param_4, uint64_t param_5, uint64_t param_6);
extern struct indirect_placeholder_79_ret_type indirect_placeholder_79(uint64_t param_0, uint64_t param_1, uint64_t param_2);
extern struct indirect_placeholder_80_ret_type indirect_placeholder_80(uint64_t param_0, uint64_t param_1);
extern struct indirect_placeholder_83_ret_type indirect_placeholder_83(uint64_t param_0, uint64_t param_1);
uint64_t bb_tac_seekable(uint64_t r10, uint64_t rdx, uint64_t rdi, uint64_t rsi, uint64_t r8, uint64_t r9) {
    uint64_t var_0;
    uint64_t var_1;
    uint64_t var_2;
    uint64_t var_3;
    uint64_t var_4;
    uint32_t var_5;
    uint64_t var_6;
    uint64_t var_7;
    uint64_t var_8;
    uint32_t var_9;
    uint32_t var_10;
    uint64_t var_11;
    uint64_t var_12;
    uint32_t *var_13;
    uint64_t *var_14;
    uint64_t *var_15;
    unsigned char *var_16;
    unsigned char var_17;
    unsigned char *var_18;
    uint64_t var_19;
    uint64_t *var_20;
    uint64_t var_21;
    uint64_t *var_22;
    uint64_t var_23;
    uint64_t var_24;
    uint64_t var_26;
    struct helper_divq_EAX_wrapper_ret_type var_25;
    uint64_t local_sp_14;
    uint64_t var_99;
    uint64_t var_100;
    uint64_t var_101;
    uint64_t r96_5;
    uint64_t rcx_7;
    uint64_t rax_0;
    uint64_t var_93;
    uint64_t var_94;
    uint64_t var_95;
    struct indirect_placeholder_74_ret_type var_96;
    uint64_t var_97;
    uint64_t var_98;
    uint64_t var_68;
    struct indirect_placeholder_76_ret_type var_69;
    uint64_t r85_1;
    uint64_t r96_1;
    uint64_t rcx_0;
    uint64_t local_sp_1;
    uint64_t r85_5;
    uint64_t rcx_9;
    uint64_t r85_2_ph;
    uint64_t r96_2_ph;
    uint64_t var_44;
    uint64_t var_38;
    uint64_t var_35;
    uint64_t local_sp_15;
    uint64_t var_36;
    uint64_t *var_37;
    uint64_t rcx_1;
    uint64_t local_sp_2;
    uint64_t rcx_2_ph;
    uint64_t local_sp_3_ph;
    uint64_t var_45;
    uint64_t rcx_2;
    uint64_t local_sp_3;
    uint64_t var_115;
    uint64_t var_116;
    uint64_t var_117;
    uint64_t var_46;
    uint64_t *var_47;
    uint64_t var_48;
    uint64_t *var_49;
    uint64_t var_50;
    uint64_t rcx_3_ph;
    uint64_t local_sp_4_ph;
    uint64_t r85_2;
    uint64_t r96_2;
    uint64_t rcx_3;
    uint64_t local_sp_4;
    uint64_t var_55;
    uint64_t *var_56;
    uint64_t var_57;
    uint64_t *var_58;
    uint64_t var_61;
    uint64_t var_59;
    struct indirect_placeholder_78_ret_type var_60;
    uint64_t r85_3;
    uint64_t r96_3;
    uint64_t rcx_4;
    uint64_t local_sp_5;
    uint64_t r85_4;
    uint64_t var_62;
    uint64_t var_63;
    uint64_t var_64;
    struct indirect_placeholder_79_ret_type var_65;
    uint64_t var_66;
    uint64_t var_67;
    uint64_t r96_4;
    uint64_t rcx_5;
    uint64_t local_sp_6;
    uint64_t rcx_6_ph;
    uint64_t local_sp_7_ph;
    uint64_t var_51;
    uint64_t var_52;
    uint64_t local_sp_8;
    uint64_t var_53;
    uint64_t var_54;
    uint64_t var_70;
    uint64_t var_71;
    uint64_t var_102;
    uint64_t var_103;
    uint64_t var_72;
    uint64_t var_73;
    uint64_t var_88;
    uint64_t var_74;
    uint64_t spec_select;
    uint64_t *var_75;
    uint64_t var_76;
    uint64_t *var_77;
    uint64_t var_78;
    uint64_t var_79;
    uint64_t var_80;
    uint64_t var_81;
    uint64_t var_82;
    uint64_t var_83;
    uint64_t var_84;
    uint64_t var_85;
    uint64_t *var_86;
    uint64_t var_87;
    uint64_t local_sp_9;
    uint64_t var_89;
    bool var_90;
    uint64_t var_91;
    uint64_t var_92;
    uint64_t var_104;
    uint64_t *var_105;
    uint64_t var_107;
    uint64_t var_110;
    uint64_t _pre263;
    uint64_t var_106;
    uint64_t rcx_8;
    uint64_t local_sp_10;
    uint64_t local_sp_11;
    uint64_t var_108;
    uint64_t var_109;
    uint64_t var_111;
    uint64_t var_112;
    uint64_t var_113;
    uint64_t var_114;
    uint64_t var_39;
    uint64_t var_40;
    struct indirect_placeholder_80_ret_type var_41;
    uint64_t var_42;
    uint64_t var_43;
    uint64_t var_27;
    uint64_t *var_28;
    uint64_t *var_29;
    uint64_t var_30;
    uint64_t var_31;
    uint64_t var_32;
    struct indirect_placeholder_83_ret_type var_33;
    uint64_t var_34;
    unsigned int loop_state_var;
    var_0 = revng_init_local_sp(0UL);
    var_1 = init_rbx();
    var_2 = init_rbp();
    var_3 = init_cc_src2();
    var_4 = init_state_0x9018();
    var_5 = init_state_0x9010();
    var_6 = init_state_0x8408();
    var_7 = init_state_0x8328();
    var_8 = init_state_0x82d8();
    var_9 = init_state_0x9080();
    var_10 = init_state_0x8248();
    var_11 = var_0 + (-8L);
    *(uint64_t *)var_11 = var_2;
    *(uint64_t *)(var_0 + (-16L)) = var_1;
    var_12 = var_0 + (-200L);
    var_13 = (uint32_t *)(var_0 + (-172L));
    *var_13 = (uint32_t)rdi;
    var_14 = (uint64_t *)(var_0 + (-184L));
    *var_14 = rsi;
    var_15 = (uint64_t *)(var_0 + (-192L));
    *var_15 = rdx;
    var_16 = (unsigned char *)(var_0 + (-49L));
    *var_16 = (unsigned char)'\x01';
    var_17 = **(unsigned char **)6489664UL;
    var_18 = (unsigned char *)(var_0 + (-50L));
    *var_18 = var_17;
    var_19 = *(uint64_t *)6489664UL + 1UL;
    var_20 = (uint64_t *)(var_0 + (-64L));
    *var_20 = var_19;
    var_21 = *(uint64_t *)6489688UL + (-1L);
    var_22 = (uint64_t *)(var_0 + (-72L));
    *var_22 = var_21;
    var_23 = *var_15;
    var_24 = *(uint64_t *)6489704UL;
    var_25 = helper_divq_EAX_wrapper((struct type_5 *)(0UL), var_24, 4204674UL, r10, var_1, var_23, var_11, 0UL, var_24, rdi, rsi, r8, r9, var_4, var_5, var_6, var_7, var_8, var_9, var_10);
    var_26 = var_25.field_4;
    *(uint64_t *)(var_0 + (-80L)) = var_26;
    local_sp_14 = var_12;
    rax_0 = 0UL;
    r85_2_ph = r8;
    r96_2_ph = r9;
    var_50 = 0UL;
    r85_2 = r8;
    r96_2 = r9;
    if (var_26 == 0UL) {
        *var_15 = (*var_15 - var_26);
        var_27 = var_0 + (-208L);
        *(uint64_t *)var_27 = 4204734UL;
        indirect_placeholder();
        local_sp_14 = var_27;
    }
    var_28 = (uint64_t *)(var_0 + (-48L));
    var_29 = (uint64_t *)(var_0 + (-88L));
    local_sp_15 = local_sp_14;
    var_30 = *(uint64_t *)6489704UL;
    var_31 = (uint64_t)*var_13;
    var_32 = local_sp_15 + (-8L);
    *(uint64_t *)var_32 = 4204968UL;
    var_33 = indirect_placeholder_83(var_30, var_31);
    var_34 = var_33.field_0;
    *var_28 = var_34;
    var_38 = var_34;
    local_sp_2 = var_32;
    while (var_34 != 0UL)
        {
            *var_29 = *(uint64_t *)6489704UL;
            var_35 = local_sp_15 + (-16L);
            *(uint64_t *)var_35 = 4204846UL;
            indirect_placeholder();
            *var_15 = (*var_15 - *(uint64_t *)6489704UL);
            local_sp_15 = var_35;
            var_30 = *(uint64_t *)6489704UL;
            var_31 = (uint64_t)*var_13;
            var_32 = local_sp_15 + (-8L);
            *(uint64_t *)var_32 = 4204968UL;
            var_33 = indirect_placeholder_83(var_30, var_31);
            var_34 = var_33.field_0;
            *var_28 = var_34;
            var_38 = var_34;
            local_sp_2 = var_32;
        }
    var_36 = var_33.field_1;
    var_37 = (uint64_t *)(var_0 + (-96L));
    rcx_1 = var_36;
    while (1U)
        {
            var_44 = var_38;
            rcx_2_ph = rcx_1;
            local_sp_3_ph = local_sp_2;
            if (var_38 != *(uint64_t *)6489704UL) {
                loop_state_var = 1U;
                break;
            }
            var_39 = (uint64_t)*var_13;
            var_40 = local_sp_2 + (-8L);
            *(uint64_t *)var_40 = 4205025UL;
            var_41 = indirect_placeholder_80(var_38, var_39);
            var_42 = var_41.field_0;
            var_43 = var_41.field_1;
            *var_37 = var_42;
            var_44 = 18446744073709551615UL;
            rcx_1 = var_43;
            local_sp_2 = var_40;
            rcx_2_ph = var_43;
            local_sp_3_ph = var_40;
            rcx_2 = var_43;
            local_sp_3 = var_40;
            if (var_42 != 0UL) {
                var_45 = *var_28;
                loop_state_var = 0U;
                break;
            }
            *var_28 = var_42;
            if (var_42 != 18446744073709551615UL) {
                loop_state_var = 1U;
                break;
            }
            *var_15 = (*var_37 + *var_15);
            var_38 = *var_28;
            continue;
        }
    switch (loop_state_var) {
      case 1U:
        {
            var_45 = var_44;
            rcx_2 = rcx_2_ph;
            local_sp_3 = local_sp_3_ph;
        }
        break;
      case 0U:
        {
            rcx_3_ph = rcx_2;
            local_sp_4_ph = local_sp_3;
            rcx_3 = rcx_2;
            local_sp_4 = local_sp_3;
            if (var_45 == 18446744073709551615UL) {
                var_115 = *var_14;
                *(uint64_t *)(local_sp_3 + (-8L)) = 4205121UL;
                var_116 = indirect_placeholder_5(var_115, 0UL, 3UL);
                *(uint64_t *)(local_sp_3 + (-16L)) = 4205129UL;
                indirect_placeholder();
                var_117 = (uint64_t)*(uint32_t *)var_116;
                *(uint64_t *)(local_sp_3 + (-24L)) = 4205156UL;
                indirect_placeholder_77(0UL, 4368802UL, var_116, 0UL, var_117, r8, r9);
            } else {
                var_46 = var_45 + *(uint64_t *)6489696UL;
                var_47 = (uint64_t *)(var_0 + (-40L));
                *var_47 = var_46;
                var_48 = var_0 + (-32L);
                var_49 = (uint64_t *)var_48;
                *var_49 = var_46;
                rax_0 = 1UL;
                if (*(uint64_t *)6489680UL != 0UL) {
                    *var_49 = (var_46 - *var_22);
                    var_50 = *(uint64_t *)6489680UL;
                    r85_2 = r85_2_ph;
                    r96_2 = r96_2_ph;
                    rcx_3 = rcx_3_ph;
                    local_sp_4 = local_sp_4_ph;
                }
                while (1U)
                    {
                        r96_5 = r96_2;
                        r85_5 = r85_2;
                        r85_3 = r85_2;
                        r96_3 = r96_2;
                        rcx_4 = rcx_3;
                        local_sp_5 = local_sp_4;
                        rcx_6_ph = rcx_3;
                        local_sp_7_ph = local_sp_4;
                        if (var_50 == 0UL) {
                            var_55 = *var_49 - *(uint64_t *)6489696UL;
                            var_56 = (uint64_t *)(var_0 + (-104L));
                            *var_56 = var_55;
                            *(uint64_t *)(var_0 + (-112L)) = var_55;
                            var_57 = 1UL - var_55;
                            var_58 = (uint64_t *)(var_0 + (-120L));
                            *var_58 = var_57;
                            var_61 = var_57;
                            if ((long)var_57 > (long)1UL) {
                                var_59 = local_sp_4 + (-8L);
                                *(uint64_t *)var_59 = 4205302UL;
                                var_60 = indirect_placeholder_78(0UL, 4368817UL, rcx_3, 1UL, 0UL, r85_2, r96_2);
                                var_61 = *var_58;
                                r85_3 = var_60.field_1;
                                r96_3 = var_60.field_2;
                                rcx_4 = var_60.field_0;
                                local_sp_5 = var_59;
                            }
                            r85_1 = var_61;
                            r85_4 = r85_3;
                            r96_4 = r96_3;
                            rcx_5 = rcx_4;
                            local_sp_6 = local_sp_5;
                            if (var_61 == 1UL) {
                                *var_49 = (*(uint64_t *)6489696UL + (-1L));
                                r85_5 = r85_4;
                                r96_5 = r96_4;
                                rcx_7 = rcx_5;
                                local_sp_8 = local_sp_6;
                            } else {
                                *var_49 = (*(uint64_t *)6489696UL + (-1L));
                                r85_5 = r85_4;
                                r96_5 = r96_4;
                                rcx_7 = rcx_5;
                                local_sp_8 = local_sp_6;
                            }
                        } else {
                            while (1U)
                                {
                                    var_51 = *var_49;
                                    rcx_7 = rcx_6_ph;
                                    local_sp_8 = local_sp_7_ph;
                                    var_52 = var_51 + (-1L);
                                    *var_49 = var_52;
                                    rcx_6_ph = var_51;
                                    var_51 = var_52;
                                    do {
                                        var_52 = var_51 + (-1L);
                                        *var_49 = var_52;
                                        rcx_6_ph = var_51;
                                        var_51 = var_52;
                                    } while ((uint64_t)(**(unsigned char **)var_48 - *var_18) != 0UL);
                                    if (*var_22 == 0UL) {
                                        break;
                                    }
                                    var_53 = *var_20;
                                    var_54 = local_sp_7_ph + (-8L);
                                    *(uint64_t *)var_54 = 4205533UL;
                                    indirect_placeholder();
                                    local_sp_7_ph = var_54;
                                    rcx_7 = var_51;
                                    local_sp_8 = var_54;
                                    if ((uint64_t)(uint32_t)var_53 != 0UL) {
                                        continue;
                                    }
                                    break;
                                }
                            var_70 = *(uint64_t *)6489696UL;
                            var_71 = helper_cc_compute_c_wrapper(*var_49 - var_70, var_70, var_3, 17U);
                            r85_2_ph = r85_5;
                            r96_2_ph = r96_5;
                            local_sp_9 = local_sp_8;
                            rcx_8 = rcx_7;
                            local_sp_10 = local_sp_8;
                            if (var_71 != 0UL) {
                                if (*var_15 != 0UL) {
                                    var_102 = *(uint64_t *)6489696UL;
                                    var_103 = *var_47;
                                    *(uint64_t *)(local_sp_8 + (-8L)) = 4205586UL;
                                    indirect_placeholder_2(var_102, var_103);
                                    break;
                                }
                                var_72 = *var_47 - *(uint64_t *)6489696UL;
                                *var_28 = var_72;
                                var_73 = *(uint64_t *)6489704UL;
                                var_88 = var_73;
                                if (var_72 <= var_73) {
                                    var_74 = *(uint64_t *)6489680UL;
                                    spec_select = (var_74 == 0UL) ? 1UL : var_74;
                                    var_75 = (uint64_t *)(var_0 + (-144L));
                                    *var_75 = spec_select;
                                    var_76 = *(uint64_t *)6489712UL;
                                    var_77 = (uint64_t *)(var_0 + (-152L));
                                    *var_77 = var_76;
                                    var_78 = *(uint64_t *)6489704UL;
                                    *(uint64_t *)6489704UL = (var_78 << 1UL);
                                    var_79 = (*(uint64_t *)6489680UL + (var_78 << 2UL)) + 2UL;
                                    *(uint64_t *)6489712UL = var_79;
                                    var_80 = *var_77;
                                    var_81 = helper_cc_compute_c_wrapper(var_79 - var_80, var_80, var_3, 17U);
                                    if (var_81 != 0UL) {
                                        *(uint64_t *)(local_sp_8 + (-8L)) = 4205748UL;
                                        indirect_placeholder_45(var_11, r85_5, r96_5);
                                        abort();
                                    }
                                    var_82 = *(uint64_t *)6489712UL;
                                    var_83 = *(uint64_t *)6489696UL - *var_75;
                                    var_84 = local_sp_8 + (-8L);
                                    *(uint64_t *)var_84 = 4205786UL;
                                    var_85 = indirect_placeholder_2(var_83, var_82);
                                    var_86 = (uint64_t *)(var_0 + (-160L));
                                    *var_86 = var_85;
                                    var_87 = var_85 + *var_75;
                                    *var_86 = var_87;
                                    *(uint64_t *)6489696UL = var_87;
                                    var_88 = *(uint64_t *)6489704UL;
                                    local_sp_9 = var_84;
                                }
                                var_89 = helper_cc_compute_c_wrapper(*var_15 - var_88, var_88, var_3, 17U);
                                var_90 = (var_89 == 0UL);
                                var_91 = *var_15;
                                if (var_90) {
                                    *var_15 = (var_91 - *(uint64_t *)6489704UL);
                                } else {
                                    *(uint64_t *)6489704UL = var_91;
                                    *var_15 = 0UL;
                                }
                                *(uint64_t *)(local_sp_9 + (-8L)) = 4205922UL;
                                indirect_placeholder();
                                *(uint64_t *)(local_sp_9 + (-16L)) = 4206026UL;
                                indirect_placeholder();
                                var_92 = *(uint64_t *)6489696UL + (*var_28 + *(uint64_t *)6489704UL);
                                *var_47 = var_92;
                                if (*(uint64_t *)6489680UL == 0UL) {
                                    *var_49 = var_92;
                                } else {
                                    *var_49 = (*(uint64_t *)6489704UL + *(uint64_t *)6489696UL);
                                }
                                var_93 = *(uint64_t *)6489704UL;
                                var_94 = (uint64_t)*var_13;
                                var_95 = local_sp_9 + (-24L);
                                *(uint64_t *)var_95 = 4206127UL;
                                var_96 = indirect_placeholder_74(var_93, var_94);
                                var_97 = var_96.field_0;
                                var_98 = var_96.field_1;
                                rcx_3_ph = var_98;
                                local_sp_4_ph = var_95;
                                if (var_97 != *(uint64_t *)6489704UL) {
                                    var_99 = *var_14;
                                    *(uint64_t *)(local_sp_9 + (-32L)) = 4206171UL;
                                    var_100 = indirect_placeholder_5(var_99, 0UL, 3UL);
                                    *(uint64_t *)(local_sp_9 + (-40L)) = 4206179UL;
                                    indirect_placeholder();
                                    var_101 = (uint64_t)*(uint32_t *)var_100;
                                    *(uint64_t *)(local_sp_9 + (-48L)) = 4206206UL;
                                    indirect_placeholder_73(0UL, 4368802UL, var_100, 0UL, var_101, r85_5, r96_5);
                                    break;
                                }
                            }
                            if (*(unsigned char *)6489673UL == '\x00') {
                                var_111 = *var_47;
                                var_112 = *var_49;
                                var_113 = local_sp_8 + (-8L);
                                *(uint64_t *)var_113 = 4206318UL;
                                var_114 = indirect_placeholder_2(var_112, var_111);
                                *var_47 = *var_49;
                                rcx_9 = var_114;
                                local_sp_11 = var_113;
                            } else {
                                var_104 = *var_49 + *(uint64_t *)6489688UL;
                                var_105 = (uint64_t *)(var_0 + (-136L));
                                *var_105 = var_104;
                                var_110 = var_104;
                                if (*var_16 == '\x01') {
                                    var_106 = *var_47;
                                    var_107 = var_106;
                                    if (var_104 == var_106) {
                                        var_108 = local_sp_8 + (-8L);
                                        *(uint64_t *)var_108 = 4206285UL;
                                        var_109 = indirect_placeholder_2(var_104, var_107);
                                        var_110 = *var_105;
                                        rcx_8 = var_109;
                                        local_sp_10 = var_108;
                                    }
                                } else {
                                    _pre263 = *var_47;
                                    var_107 = _pre263;
                                    var_108 = local_sp_8 + (-8L);
                                    *(uint64_t *)var_108 = 4206285UL;
                                    var_109 = indirect_placeholder_2(var_104, var_107);
                                    var_110 = *var_105;
                                    rcx_8 = var_109;
                                    local_sp_10 = var_108;
                                }
                                *var_47 = var_110;
                                *var_16 = (unsigned char)'\x00';
                                rcx_9 = rcx_8;
                                local_sp_11 = local_sp_10;
                            }
                            rcx_3_ph = rcx_9;
                            local_sp_4_ph = local_sp_11;
                            if (*(uint64_t *)6489680UL == 0UL) {
                                *var_49 = (*var_49 + (1UL - *(uint64_t *)6489688UL));
                            }
                        }
                    }
            }
            return rax_0;
        }
        break;
    }
}

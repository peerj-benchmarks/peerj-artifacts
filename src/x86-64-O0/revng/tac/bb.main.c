typedef unsigned char __u_char;
typedef unsigned short __u_short;
typedef unsigned int __u_int;
typedef unsigned long __u_long;
typedef signed char __int8_t;
typedef unsigned char __uint8_t;
typedef short __int16_t;
typedef unsigned short __uint16_t;
typedef int __int32_t;
typedef unsigned int __uint32_t;
typedef long __int64_t;
typedef unsigned long __uint64_t;
typedef __int8_t __int_least8_t;
typedef __uint8_t __uint_least8_t;
typedef __int16_t __int_least16_t;
typedef __uint16_t __uint_least16_t;
typedef __int32_t __int_least32_t;
typedef __uint32_t __uint_least32_t;
typedef __int64_t __int_least64_t;
typedef __uint64_t __uint_least64_t;
typedef long __quad_t;
typedef unsigned long __u_quad_t;
typedef long __intmax_t;
typedef unsigned long __uintmax_t;
typedef unsigned long __dev_t;
typedef unsigned int __uid_t;
typedef unsigned int __gid_t;
typedef unsigned long __ino_t;
typedef unsigned long __ino64_t;
typedef unsigned int __mode_t;
typedef unsigned long __nlink_t;
typedef long __off_t;
typedef long __off64_t;
typedef int __pid_t;
typedef struct {
    int __val[2];
} __fsid_t;
typedef long __clock_t;
typedef unsigned long __rlim_t;
typedef unsigned long __rlim64_t;
typedef unsigned int __id_t;
typedef long __time_t;
typedef unsigned int __useconds_t;
typedef long __suseconds_t;
typedef int __daddr_t;
typedef int __key_t;
typedef int __clockid_t;
typedef void *__timer_t;
typedef long __blksize_t;
typedef long __blkcnt_t;
typedef long __blkcnt64_t;
typedef unsigned long __fsblkcnt_t;
typedef unsigned long __fsblkcnt64_t;
typedef unsigned long __fsfilcnt_t;
typedef unsigned long __fsfilcnt64_t;
typedef long __fsword_t;
typedef long __ssize_t;
typedef long __syscall_slong_t;
typedef unsigned long __syscall_ulong_t;
typedef __off64_t __loff_t;
typedef char *__caddr_t;
typedef long __intptr_t;
typedef unsigned int __socklen_t;
typedef int __sig_atomic_t;
typedef __int8_t int8_t;
typedef __int16_t int16_t;
typedef __int32_t int32_t;
typedef __int64_t int64_t;
typedef __uint8_t uint8_t;
typedef __uint16_t uint16_t;
typedef __uint32_t uint32_t;
typedef __uint64_t uint64_t;
typedef __int_least8_t int_least8_t;
typedef __int_least16_t int_least16_t;
typedef __int_least32_t int_least32_t;
typedef __int_least64_t int_least64_t;
typedef __uint_least8_t uint_least8_t;
typedef __uint_least16_t uint_least16_t;
typedef __uint_least32_t uint_least32_t;
typedef __uint_least64_t uint_least64_t;
typedef signed char int_fast8_t;
typedef long int_fast16_t;
typedef long int_fast32_t;
typedef long int_fast64_t;
typedef unsigned char uint_fast8_t;
typedef unsigned long uint_fast16_t;
typedef unsigned long uint_fast32_t;
typedef unsigned long uint_fast64_t;
typedef long intptr_t;
typedef unsigned long uintptr_t;
typedef __intmax_t intmax_t;
typedef __uintmax_t uintmax_t;
typedef _Bool bool;
struct indirect_placeholder_54_ret_type;
struct indirect_placeholder_56_ret_type;
struct indirect_placeholder_55_ret_type;
struct indirect_placeholder_58_ret_type;
struct indirect_placeholder_54_ret_type {
    uint64_t field_0;
    uint64_t field_1;
    uint64_t field_2;
};
struct indirect_placeholder_56_ret_type {
    uint64_t field_0;
    uint64_t field_1;
    uint64_t field_2;
};
struct indirect_placeholder_55_ret_type {
    uint64_t field_0;
    uint64_t field_1;
    uint64_t field_2;
};
struct indirect_placeholder_58_ret_type {
    uint64_t field_0;
    uint64_t field_1;
    uint64_t field_2;
    uint64_t field_3;
};
extern void abort(void);
extern uint64_t revng_init_local_sp(uint64_t param_0);
extern uint64_t init_rbp(void);
extern uint64_t init_cc_src2(void);
extern uint64_t indirect_placeholder_2(uint64_t param_0, uint64_t param_1);
extern uint64_t indirect_placeholder_1(uint64_t param_0);
extern uint64_t helper_cc_compute_c_wrapper(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint32_t param_3);
extern void indirect_placeholder(void);
extern void indirect_placeholder_19(uint64_t param_0);
extern void indirect_placeholder_45(uint64_t param_0, uint64_t param_1, uint64_t param_2);
extern void indirect_placeholder_12(uint64_t param_0, uint64_t param_1);
extern void indirect_placeholder_57(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint64_t param_3, uint64_t param_4, uint64_t param_5);
extern struct indirect_placeholder_54_ret_type indirect_placeholder_54(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint64_t param_3, uint64_t param_4, uint64_t param_5, uint64_t param_6);
extern struct indirect_placeholder_56_ret_type indirect_placeholder_56(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint64_t param_3, uint64_t param_4, uint64_t param_5, uint64_t param_6);
extern struct indirect_placeholder_55_ret_type indirect_placeholder_55(uint64_t param_0, uint64_t param_1, uint64_t param_2);
extern struct indirect_placeholder_58_ret_type indirect_placeholder_58(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint64_t param_3, uint64_t param_4);
void bb_main(uint64_t rdi, uint64_t rsi) {
    struct indirect_placeholder_54_ret_type var_30;
    struct indirect_placeholder_58_ret_type var_13;
    uint64_t r8_1;
    uint64_t var_0;
    uint64_t var_1;
    uint64_t var_2;
    uint64_t var_3;
    uint32_t *var_4;
    uint64_t var_5;
    uint64_t *var_6;
    uint64_t var_7;
    uint64_t var_8;
    uint32_t *var_9;
    uint64_t local_sp_7;
    uint64_t storemerge6;
    uint64_t var_53;
    uint64_t local_sp_0;
    uint64_t var_54;
    bool var_55;
    uint64_t var_56;
    uint64_t *var_57;
    uint64_t var_58;
    uint64_t var_59;
    uint64_t local_sp_2;
    uint64_t var_47;
    uint64_t local_sp_1;
    uint64_t storemerge5;
    uint32_t var_48;
    uint64_t *var_49;
    uint64_t var_50;
    unsigned char *var_51;
    uint64_t *var_52;
    uint64_t var_29;
    uint64_t var_31;
    uint64_t var_32;
    uint64_t local_sp_3;
    uint64_t var_18;
    uint64_t var_19;
    uint64_t var_20;
    uint64_t var_21;
    uint64_t var_22;
    uint64_t var_23;
    uint64_t var_24;
    struct indirect_placeholder_55_ret_type var_25;
    uint64_t var_26;
    uint64_t var_27;
    uint64_t var_28;
    uint64_t local_sp_5;
    uint64_t r8_0;
    uint64_t r9_0;
    uint64_t rax_0;
    uint64_t var_33;
    uint64_t var_34;
    uint64_t local_sp_4;
    uint64_t r9_1;
    uint64_t storemerge;
    uint64_t var_35;
    uint64_t var_36;
    uint64_t var_37;
    uint64_t *var_38;
    uint64_t var_39;
    uint64_t var_40;
    uint64_t var_41;
    uint64_t var_42;
    uint64_t var_43;
    uint64_t var_44;
    uint64_t var_45;
    uint64_t var_46;
    uint64_t local_sp_6;
    uint64_t var_15;
    uint64_t var_16;
    uint64_t *var_17;
    uint64_t var_10;
    uint64_t var_11;
    uint64_t var_12;
    uint32_t var_14;
    unsigned int loop_state_var;
    var_0 = revng_init_local_sp(0UL);
    var_1 = init_rbp();
    var_2 = init_cc_src2();
    var_3 = var_0 + (-8L);
    *(uint64_t *)var_3 = var_1;
    var_4 = (uint32_t *)(var_0 + (-60L));
    *var_4 = (uint32_t)rdi;
    var_5 = var_0 + (-72L);
    var_6 = (uint64_t *)var_5;
    *var_6 = rsi;
    var_7 = **(uint64_t **)var_5;
    *(uint64_t *)(var_0 + (-80L)) = 4207915UL;
    indirect_placeholder_19(var_7);
    *(uint64_t *)(var_0 + (-88L)) = 4207930UL;
    indirect_placeholder();
    var_8 = var_0 + (-96L);
    *(uint64_t *)var_8 = 4207940UL;
    indirect_placeholder();
    *(uint64_t *)6489664UL = 4369086UL;
    *(uint64_t *)6489680UL = 1UL;
    *(unsigned char *)6489673UL = (unsigned char)'\x01';
    var_9 = (uint32_t *)(var_0 + (-28L));
    local_sp_7 = var_8;
    storemerge6 = 4369152UL;
    var_53 = 0UL;
    rax_0 = 1UL;
    storemerge = 8192UL;
    while (1U)
        {
            var_10 = *var_6;
            var_11 = (uint64_t)*var_4;
            var_12 = local_sp_7 + (-8L);
            *(uint64_t *)var_12 = 4208178UL;
            var_13 = indirect_placeholder_58(4369116UL, 4368128UL, var_11, var_10, 0UL);
            var_14 = (uint32_t)var_13.field_0;
            *var_9 = var_14;
            local_sp_2 = var_12;
            local_sp_3 = var_12;
            local_sp_6 = var_12;
            local_sp_7 = var_12;
            if (var_14 != 4294967295U) {
                var_18 = var_13.field_1;
                var_19 = var_13.field_2;
                var_20 = var_13.field_3;
                r8_0 = var_19;
                r9_0 = var_20;
                if (*(uint64_t *)6489680UL != 0UL) {
                    loop_state_var = 1U;
                    break;
                }
                if (**(unsigned char **)6489664UL != '\x00') {
                    loop_state_var = 0U;
                    break;
                }
                var_21 = local_sp_7 + (-16L);
                *(uint64_t *)var_21 = 4208246UL;
                indirect_placeholder_56(0UL, 4369121UL, var_18, 1UL, 0UL, var_19, var_20);
                local_sp_2 = var_21;
                loop_state_var = 0U;
                break;
            }
            if ((uint64_t)(var_14 + (-98)) == 0UL) {
                *(unsigned char *)6489673UL = (unsigned char)'\x00';
                continue;
            }
            if ((int)var_14 > (int)98U) {
                if ((uint64_t)(var_14 + (-114)) == 0UL) {
                    *(uint64_t *)6489680UL = 0UL;
                    continue;
                }
                if ((uint64_t)(var_14 + (-115)) != 0UL) {
                    loop_state_var = 2U;
                    break;
                }
                *(uint64_t *)6489664UL = *(uint64_t *)6498864UL;
                continue;
            }
            if ((uint64_t)(var_14 + 131U) == 0UL) {
                if ((uint64_t)(var_14 + 130U) != 0UL) {
                    loop_state_var = 2U;
                    break;
                }
                *(uint64_t *)(local_sp_7 + (-16L)) = 4208066UL;
                indirect_placeholder_12(var_3, 0UL);
                abort();
            }
            var_15 = *(uint64_t *)6489280UL;
            var_16 = local_sp_7 + (-24L);
            var_17 = (uint64_t *)var_16;
            *var_17 = 0UL;
            *(uint64_t *)(local_sp_7 + (-32L)) = 4208124UL;
            indirect_placeholder_57(0UL, 4367832UL, var_15, 4368782UL, 4369104UL, 4369088UL);
            *var_17 = 4208138UL;
            indirect_placeholder();
            local_sp_6 = var_16;
            loop_state_var = 2U;
            break;
        }
    switch (loop_state_var) {
      case 2U:
        {
            *(uint64_t *)(local_sp_6 + (-8L)) = 4208148UL;
            indirect_placeholder_12(var_3, 1UL);
            abort();
        }
        break;
      case 1U:
      case 0U:
        {
            switch (loop_state_var) {
              case 0U:
                {
                    *(uint64_t *)6489728UL = 0UL;
                    *(uint64_t *)6489736UL = 0UL;
                    *(uint64_t *)6489760UL = 6489792UL;
                    *(uint64_t *)6489768UL = 0UL;
                    var_22 = *(uint64_t *)6489664UL;
                    *(uint64_t *)(local_sp_2 + (-8L)) = 4208305UL;
                    indirect_placeholder();
                    var_23 = *(uint64_t *)6489664UL;
                    var_24 = local_sp_2 + (-16L);
                    *(uint64_t *)var_24 = 4208331UL;
                    var_25 = indirect_placeholder_55(6489728UL, var_23, var_22);
                    var_26 = var_25.field_0;
                    var_27 = var_25.field_1;
                    var_28 = var_25.field_2;
                    *(uint64_t *)(var_0 + (-40L)) = var_26;
                    local_sp_5 = var_24;
                    r8_1 = var_27;
                    r9_1 = var_28;
                    if (var_26 != 0UL) {
                        var_29 = local_sp_2 + (-24L);
                        *(uint64_t *)var_29 = 4208374UL;
                        var_30 = indirect_placeholder_54(0UL, 4369147UL, var_26, 1UL, 0UL, var_27, var_28);
                        var_31 = var_30.field_1;
                        var_32 = var_30.field_2;
                        local_sp_3 = var_29;
                        r8_0 = var_31;
                        r9_0 = var_32;
                        local_sp_4 = local_sp_3;
                        r8_1 = r8_0;
                        r9_1 = r9_0;
                        if (**(unsigned char **)6489664UL != '\x00') {
                            var_33 = *(uint64_t *)6489664UL;
                            var_34 = local_sp_3 + (-8L);
                            *(uint64_t *)var_34 = 4208403UL;
                            indirect_placeholder();
                            rax_0 = var_33;
                            local_sp_4 = var_34;
                        }
                        *(uint64_t *)6489680UL = rax_0;
                        *(uint64_t *)6489688UL = rax_0;
                        local_sp_5 = local_sp_4;
                    }
                }
                break;
              case 1U:
                {
                    local_sp_4 = local_sp_3;
                    r8_1 = r8_0;
                    r9_1 = r9_0;
                    if (**(unsigned char **)6489664UL == '\x00') {
                        var_33 = *(uint64_t *)6489664UL;
                        var_34 = local_sp_3 + (-8L);
                        *(uint64_t *)var_34 = 4208403UL;
                        indirect_placeholder();
                        rax_0 = var_33;
                        local_sp_4 = var_34;
                    }
                    *(uint64_t *)6489680UL = rax_0;
                    *(uint64_t *)6489688UL = rax_0;
                    local_sp_5 = local_sp_4;
                }
                break;
            }
            while (1U)
                {
                    *(uint64_t *)6489704UL = storemerge;
                    var_35 = storemerge >> 1UL;
                    var_36 = *(uint64_t *)6489680UL;
                    if (var_35 > var_36) {
                        if ((long)storemerge <= (long)18446744073709551615UL) {
                            storemerge = storemerge << 1UL;
                            continue;
                        }
                        *(uint64_t *)(local_sp_5 + (-8L)) = 4208461UL;
                        indirect_placeholder_45(var_3, r8_1, r9_1);
                        abort();
                    }
                    var_37 = (var_36 + storemerge) + 1UL;
                    var_38 = (uint64_t *)(var_0 + (-48L));
                    *var_38 = var_37;
                    *(uint64_t *)6489712UL = (var_37 << 1UL);
                    var_39 = *(uint64_t *)6489704UL;
                    var_40 = *var_38;
                    var_41 = helper_cc_compute_c_wrapper(var_39 - var_40, var_40, var_2, 17U);
                    if (var_41 != 0UL) {
                        loop_state_var = 1U;
                        break;
                    }
                    var_42 = *(uint64_t *)6489712UL;
                    var_43 = helper_cc_compute_c_wrapper(*var_38 - var_42, var_42, var_2, 17U);
                    if (var_43 != 0UL) {
                        loop_state_var = 1U;
                        break;
                    }
                    var_44 = *(uint64_t *)6489712UL;
                    var_45 = local_sp_5 + (-8L);
                    *(uint64_t *)var_45 = 4208588UL;
                    var_46 = indirect_placeholder_1(var_44);
                    *(uint64_t *)6489696UL = var_46;
                    local_sp_1 = var_45;
                    if (*(uint64_t *)6489680UL != 0UL) {
                        storemerge5 = var_46 + 1UL;
                        loop_state_var = 0U;
                        break;
                    }
                    var_47 = local_sp_5 + (-16L);
                    *(uint64_t *)var_47 = 4208643UL;
                    indirect_placeholder();
                    local_sp_1 = var_47;
                    storemerge5 = *(uint64_t *)6489680UL + *(uint64_t *)6489696UL;
                    loop_state_var = 0U;
                    break;
                }
            switch (loop_state_var) {
              case 1U:
                {
                    *(uint64_t *)(local_sp_5 + (-8L)) = 4208573UL;
                    indirect_placeholder_45(var_3, r8_1, r9_1);
                    abort();
                }
                break;
              case 0U:
                {
                    *(uint64_t *)6489696UL = storemerge5;
                    var_48 = *(uint32_t *)6489432UL;
                    if ((long)((uint64_t)var_48 << 32UL) < (long)((uint64_t)*var_4 << 32UL)) {
                        storemerge6 = *var_6 + ((uint64_t)var_48 << 3UL);
                    }
                    var_49 = (uint64_t *)(var_0 + (-56L));
                    *var_49 = storemerge6;
                    var_50 = local_sp_1 + (-8L);
                    *(uint64_t *)var_50 = 4208747UL;
                    indirect_placeholder_12(1UL, 0UL);
                    var_51 = (unsigned char *)(var_0 + (-9L));
                    *var_51 = (unsigned char)'\x01';
                    var_52 = (uint64_t *)(var_0 + (-24L));
                    *var_52 = 0UL;
                    local_sp_0 = var_50;
                    var_54 = *(uint64_t *)(*var_49 + (var_53 << 3UL));
                    var_55 = (var_54 == 0UL);
                    var_56 = local_sp_0 + (-8L);
                    var_57 = (uint64_t *)var_56;
                    local_sp_0 = var_56;
                    while (!var_55)
                        {
                            *var_57 = 4208791UL;
                            var_58 = indirect_placeholder_1(var_54);
                            *var_51 = ((var_58 & (uint64_t)*var_51) != 0UL);
                            var_59 = *var_52 + 1UL;
                            *var_52 = var_59;
                            var_53 = var_59;
                            var_54 = *(uint64_t *)(*var_49 + (var_53 << 3UL));
                            var_55 = (var_54 == 0UL);
                            var_56 = local_sp_0 + (-8L);
                            var_57 = (uint64_t *)var_56;
                            local_sp_0 = var_56;
                        }
                    *var_57 = 4208855UL;
                    indirect_placeholder_2(0UL, 0UL);
                    if (*(unsigned char *)6489672UL != '\x00') {
                        *(uint64_t *)(local_sp_0 + (-16L)) = 4208876UL;
                        indirect_placeholder();
                    }
                    return;
                }
                break;
            }
        }
        break;
    }
}

typedef unsigned char __u_char;
typedef unsigned short __u_short;
typedef unsigned int __u_int;
typedef unsigned long __u_long;
typedef signed char __int8_t;
typedef unsigned char __uint8_t;
typedef short __int16_t;
typedef unsigned short __uint16_t;
typedef int __int32_t;
typedef unsigned int __uint32_t;
typedef long __int64_t;
typedef unsigned long __uint64_t;
typedef __int8_t __int_least8_t;
typedef __uint8_t __uint_least8_t;
typedef __int16_t __int_least16_t;
typedef __uint16_t __uint_least16_t;
typedef __int32_t __int_least32_t;
typedef __uint32_t __uint_least32_t;
typedef __int64_t __int_least64_t;
typedef __uint64_t __uint_least64_t;
typedef long __quad_t;
typedef unsigned long __u_quad_t;
typedef long __intmax_t;
typedef unsigned long __uintmax_t;
typedef unsigned long __dev_t;
typedef unsigned int __uid_t;
typedef unsigned int __gid_t;
typedef unsigned long __ino_t;
typedef unsigned long __ino64_t;
typedef unsigned int __mode_t;
typedef unsigned long __nlink_t;
typedef long __off_t;
typedef long __off64_t;
typedef int __pid_t;
typedef struct {
    int __val[2];
} __fsid_t;
typedef long __clock_t;
typedef unsigned long __rlim_t;
typedef unsigned long __rlim64_t;
typedef unsigned int __id_t;
typedef long __time_t;
typedef unsigned int __useconds_t;
typedef long __suseconds_t;
typedef int __daddr_t;
typedef int __key_t;
typedef int __clockid_t;
typedef void *__timer_t;
typedef long __blksize_t;
typedef long __blkcnt_t;
typedef long __blkcnt64_t;
typedef unsigned long __fsblkcnt_t;
typedef unsigned long __fsblkcnt64_t;
typedef unsigned long __fsfilcnt_t;
typedef unsigned long __fsfilcnt64_t;
typedef long __fsword_t;
typedef long __ssize_t;
typedef long __syscall_slong_t;
typedef unsigned long __syscall_ulong_t;
typedef __off64_t __loff_t;
typedef char *__caddr_t;
typedef long __intptr_t;
typedef unsigned int __socklen_t;
typedef int __sig_atomic_t;
typedef __int8_t int8_t;
typedef __int16_t int16_t;
typedef __int32_t int32_t;
typedef __int64_t int64_t;
typedef __uint8_t uint8_t;
typedef __uint16_t uint16_t;
typedef __uint32_t uint32_t;
typedef __uint64_t uint64_t;
typedef __int_least8_t int_least8_t;
typedef __int_least16_t int_least16_t;
typedef __int_least32_t int_least32_t;
typedef __int_least64_t int_least64_t;
typedef __uint_least8_t uint_least8_t;
typedef __uint_least16_t uint_least16_t;
typedef __uint_least32_t uint_least32_t;
typedef __uint_least64_t uint_least64_t;
typedef signed char int_fast8_t;
typedef long int_fast16_t;
typedef long int_fast32_t;
typedef long int_fast64_t;
typedef unsigned char uint_fast8_t;
typedef unsigned long uint_fast16_t;
typedef unsigned long uint_fast32_t;
typedef unsigned long uint_fast64_t;
typedef long intptr_t;
typedef unsigned long uintptr_t;
typedef __intmax_t intmax_t;
typedef __uintmax_t uintmax_t;
struct bb_process_file_ret_type;
struct indirect_placeholder_80_ret_type;
struct indirect_placeholder_82_ret_type;
struct indirect_placeholder_81_ret_type;
struct indirect_placeholder_83_ret_type;
struct indirect_placeholder_79_ret_type;
struct indirect_placeholder_78_ret_type;
struct indirect_placeholder_85_ret_type;
struct indirect_placeholder_84_ret_type;
struct indirect_placeholder_91_ret_type;
struct indirect_placeholder_93_ret_type;
struct indirect_placeholder_92_ret_type;
struct indirect_placeholder_94_ret_type;
struct indirect_placeholder_90_ret_type;
struct indirect_placeholder_89_ret_type;
struct indirect_placeholder_95_ret_type;
struct indirect_placeholder_86_ret_type;
struct indirect_placeholder_96_ret_type;
struct indirect_placeholder_87_ret_type;
struct indirect_placeholder_97_ret_type;
struct indirect_placeholder_88_ret_type;
struct indirect_placeholder_98_ret_type;
struct bb_process_file_ret_type {
    uint64_t field_0;
    uint64_t field_1;
    uint64_t field_2;
};
struct indirect_placeholder_80_ret_type {
    uint64_t field_0;
    uint64_t field_1;
    uint64_t field_2;
};
struct indirect_placeholder_82_ret_type {
    uint64_t field_0;
    uint64_t field_1;
    uint64_t field_2;
};
struct indirect_placeholder_81_ret_type {
    uint64_t field_0;
    uint64_t field_1;
    uint64_t field_2;
};
struct indirect_placeholder_83_ret_type {
    uint64_t field_0;
    uint64_t field_1;
};
struct indirect_placeholder_79_ret_type {
    uint64_t field_0;
    uint64_t field_1;
};
struct indirect_placeholder_78_ret_type {
    uint64_t field_0;
    uint64_t field_1;
    uint64_t field_2;
};
struct indirect_placeholder_85_ret_type {
    uint64_t field_0;
    uint64_t field_1;
    uint64_t field_2;
};
struct indirect_placeholder_84_ret_type {
    uint64_t field_0;
    uint64_t field_1;
    uint64_t field_2;
};
struct indirect_placeholder_91_ret_type {
    uint64_t field_0;
    uint64_t field_1;
    uint64_t field_2;
};
struct indirect_placeholder_93_ret_type {
    uint64_t field_0;
    uint64_t field_1;
    uint64_t field_2;
};
struct indirect_placeholder_92_ret_type {
    uint64_t field_0;
    uint64_t field_1;
    uint64_t field_2;
};
struct indirect_placeholder_94_ret_type {
    uint64_t field_0;
    uint64_t field_1;
};
struct indirect_placeholder_90_ret_type {
    uint64_t field_0;
    uint64_t field_1;
};
struct indirect_placeholder_89_ret_type {
    uint64_t field_0;
    uint64_t field_1;
    uint64_t field_2;
};
struct indirect_placeholder_95_ret_type {
    uint64_t field_0;
    uint64_t field_1;
    uint64_t field_2;
};
struct indirect_placeholder_86_ret_type {
    uint64_t field_0;
    uint64_t field_1;
    uint64_t field_2;
};
struct indirect_placeholder_96_ret_type {
    uint64_t field_0;
    uint64_t field_1;
    uint64_t field_2;
};
struct indirect_placeholder_87_ret_type {
    uint64_t field_0;
    uint64_t field_1;
    uint64_t field_2;
};
struct indirect_placeholder_97_ret_type {
    uint64_t field_0;
    uint64_t field_1;
    uint64_t field_2;
};
struct indirect_placeholder_88_ret_type {
    uint64_t field_0;
    uint64_t field_1;
    uint64_t field_2;
};
struct indirect_placeholder_98_ret_type {
    uint64_t field_0;
    uint64_t field_1;
    uint64_t field_2;
};
extern void abort(void);
extern uint64_t revng_init_local_sp(uint64_t param_0);
extern uint64_t init_rbp(void);
extern uint64_t indirect_placeholder_1(uint64_t param_0, uint64_t param_1);
extern void indirect_placeholder(void);
extern uint64_t indirect_placeholder_2(uint64_t param_0, uint64_t param_1, uint64_t param_2);
extern uint64_t init_r9(void);
extern uint64_t init_r8(void);
extern uint64_t init_rbx(void);
extern struct indirect_placeholder_80_ret_type indirect_placeholder_80(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint64_t param_3, uint64_t param_4, uint64_t param_5, uint64_t param_6);
extern struct indirect_placeholder_82_ret_type indirect_placeholder_82(uint64_t param_0, uint64_t param_1);
extern struct indirect_placeholder_81_ret_type indirect_placeholder_81(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint64_t param_3, uint64_t param_4, uint64_t param_5, uint64_t param_6);
extern struct indirect_placeholder_83_ret_type indirect_placeholder_83(uint64_t param_0, uint64_t param_1, uint64_t param_2);
extern struct indirect_placeholder_79_ret_type indirect_placeholder_79(uint64_t param_0, uint64_t param_1, uint64_t param_2);
extern struct indirect_placeholder_78_ret_type indirect_placeholder_78(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint64_t param_3, uint64_t param_4, uint64_t param_5, uint64_t param_6);
extern struct indirect_placeholder_85_ret_type indirect_placeholder_85(uint64_t param_0, uint64_t param_1, uint64_t param_2);
extern struct indirect_placeholder_84_ret_type indirect_placeholder_84(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint64_t param_3, uint64_t param_4, uint64_t param_5, uint64_t param_6);
extern struct indirect_placeholder_91_ret_type indirect_placeholder_91(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint64_t param_3, uint64_t param_4, uint64_t param_5, uint64_t param_6);
extern struct indirect_placeholder_93_ret_type indirect_placeholder_93(uint64_t param_0, uint64_t param_1);
extern struct indirect_placeholder_92_ret_type indirect_placeholder_92(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint64_t param_3, uint64_t param_4, uint64_t param_5, uint64_t param_6);
extern struct indirect_placeholder_94_ret_type indirect_placeholder_94(uint64_t param_0, uint64_t param_1, uint64_t param_2);
extern struct indirect_placeholder_90_ret_type indirect_placeholder_90(uint64_t param_0, uint64_t param_1, uint64_t param_2);
extern struct indirect_placeholder_89_ret_type indirect_placeholder_89(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint64_t param_3, uint64_t param_4, uint64_t param_5, uint64_t param_6);
extern struct indirect_placeholder_95_ret_type indirect_placeholder_95(uint64_t param_0, uint64_t param_1);
extern struct indirect_placeholder_86_ret_type indirect_placeholder_86(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint64_t param_3, uint64_t param_4, uint64_t param_5, uint64_t param_6);
extern struct indirect_placeholder_96_ret_type indirect_placeholder_96(uint64_t param_0, uint64_t param_1, uint64_t param_2);
extern struct indirect_placeholder_87_ret_type indirect_placeholder_87(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint64_t param_3, uint64_t param_4, uint64_t param_5, uint64_t param_6);
extern struct indirect_placeholder_97_ret_type indirect_placeholder_97(uint64_t param_0, uint64_t param_1);
extern struct indirect_placeholder_88_ret_type indirect_placeholder_88(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint64_t param_3, uint64_t param_4, uint64_t param_5, uint64_t param_6);
extern struct indirect_placeholder_98_ret_type indirect_placeholder_98(uint64_t param_0, uint64_t param_1);
struct bb_process_file_ret_type bb_process_file(uint64_t rsi, uint64_t rdi) {
    struct indirect_placeholder_80_ret_type var_105;
    uint64_t var_0;
    uint64_t var_1;
    uint64_t var_2;
    uint64_t var_3;
    uint64_t var_4;
    uint64_t var_5;
    uint64_t *var_6;
    uint64_t *var_7;
    uint64_t var_8;
    uint64_t *var_9;
    uint64_t var_10;
    uint64_t *var_11;
    uint64_t var_12;
    uint64_t var_13;
    uint64_t *var_14;
    unsigned char *var_15;
    uint64_t var_16;
    uint16_t var_17;
    uint64_t local_sp_2;
    uint64_t local_sp_5;
    uint64_t r9_2;
    uint64_t local_sp_0;
    uint64_t rcx_0;
    uint64_t r9_0;
    uint64_t r8_0;
    uint64_t var_104;
    uint64_t var_106;
    uint64_t var_107;
    unsigned char var_108;
    uint64_t var_97;
    struct indirect_placeholder_82_ret_type var_98;
    uint64_t var_99;
    uint64_t var_100;
    uint64_t var_101;
    uint64_t var_102;
    struct indirect_placeholder_81_ret_type var_103;
    struct indirect_placeholder_83_ret_type var_89;
    uint64_t var_90;
    uint64_t var_91;
    struct indirect_placeholder_79_ret_type var_92;
    uint64_t var_93;
    uint64_t var_94;
    uint64_t var_95;
    struct indirect_placeholder_78_ret_type var_96;
    uint64_t var_21;
    struct indirect_placeholder_85_ret_type var_22;
    uint64_t var_23;
    uint64_t var_24;
    uint64_t var_25;
    struct indirect_placeholder_84_ret_type var_26;
    uint64_t var_27;
    uint64_t var_28;
    uint64_t rax_0;
    uint64_t local_sp_1;
    uint64_t rcx_1;
    uint64_t r9_1;
    uint64_t r8_1;
    struct indirect_placeholder_91_ret_type var_79;
    uint64_t var_80;
    uint64_t var_81;
    uint64_t var_82;
    uint64_t var_83;
    uint64_t var_84;
    uint64_t var_85;
    uint64_t var_72;
    struct indirect_placeholder_93_ret_type var_73;
    uint64_t var_74;
    uint64_t var_75;
    uint64_t var_76;
    uint64_t var_77;
    struct indirect_placeholder_92_ret_type var_78;
    struct indirect_placeholder_94_ret_type var_64;
    uint64_t var_65;
    uint64_t var_66;
    struct indirect_placeholder_90_ret_type var_67;
    uint64_t var_68;
    uint64_t var_69;
    uint64_t var_70;
    struct indirect_placeholder_89_ret_type var_71;
    uint64_t r9_3;
    uint64_t r8_3;
    uint64_t var_18;
    uint64_t var_19;
    uint64_t var_20;
    uint64_t var_29;
    struct indirect_placeholder_95_ret_type var_30;
    uint64_t var_31;
    uint64_t var_32;
    uint64_t var_33;
    uint64_t var_34;
    uint64_t var_35;
    struct indirect_placeholder_86_ret_type var_36;
    uint64_t var_37;
    uint64_t var_38;
    uint64_t var_39;
    struct indirect_placeholder_96_ret_type var_40;
    uint64_t var_41;
    uint64_t var_42;
    uint64_t var_43;
    uint64_t var_44;
    uint64_t var_45;
    struct indirect_placeholder_87_ret_type var_46;
    uint64_t var_47;
    uint64_t var_48;
    uint64_t *var_49;
    uint64_t var_60;
    uint64_t var_61;
    uint64_t var_50;
    struct indirect_placeholder_97_ret_type var_51;
    uint64_t var_52;
    uint64_t var_53;
    uint64_t var_54;
    uint64_t var_55;
    uint64_t var_56;
    struct indirect_placeholder_88_ret_type var_57;
    uint64_t var_58;
    uint64_t var_59;
    uint64_t var_62;
    uint64_t var_63;
    uint64_t r8_2;
    unsigned char var_86;
    uint64_t var_87;
    uint64_t var_88;
    uint64_t local_sp_3;
    uint64_t local_sp_4;
    uint64_t var_109;
    struct indirect_placeholder_98_ret_type var_110;
    uint64_t var_111;
    uint64_t var_112;
    uint64_t var_113;
    uint64_t r9_4;
    uint64_t r8_4;
    uint64_t var_114;
    uint64_t var_115;
    uint64_t var_116;
    uint64_t var_117;
    uint64_t r9_5;
    uint64_t r8_5;
    uint64_t r9_6;
    uint64_t var_118;
    uint64_t var_119;
    uint64_t var_120;
    uint64_t r8_6;
    struct bb_process_file_ret_type mrv;
    struct bb_process_file_ret_type mrv1;
    struct bb_process_file_ret_type mrv2;
    var_0 = revng_init_local_sp(0UL);
    var_1 = init_rbp();
    var_2 = init_rbx();
    var_3 = init_r9();
    var_4 = init_r8();
    *(uint64_t *)(var_0 + (-8L)) = var_1;
    *(uint64_t *)(var_0 + (-16L)) = var_2;
    var_5 = var_0 + (-88L);
    var_6 = (uint64_t *)(var_0 + (-80L));
    *var_6 = rdi;
    var_7 = (uint64_t *)var_5;
    *var_7 = rsi;
    var_8 = *(uint64_t *)(rsi + 56UL);
    var_9 = (uint64_t *)(var_0 + (-40L));
    *var_9 = var_8;
    var_10 = *(uint64_t *)(*var_7 + 48UL);
    var_11 = (uint64_t *)(var_0 + (-48L));
    *var_11 = var_10;
    var_12 = *var_7 + 120UL;
    var_13 = var_0 + (-56L);
    var_14 = (uint64_t *)var_13;
    *var_14 = var_12;
    var_15 = (unsigned char *)(var_0 + (-25L));
    *var_15 = (unsigned char)'\x01';
    var_16 = *var_7;
    var_17 = *(uint16_t *)(var_16 + 112UL);
    local_sp_2 = var_5;
    r9_2 = var_3;
    var_108 = (unsigned char)'\x00';
    rax_0 = 1UL;
    r8_2 = var_4;
    r9_6 = var_3;
    r8_6 = var_4;
    if (var_17 > (unsigned short)10U) {
        local_sp_3 = local_sp_2;
        r9_3 = r9_2;
        r8_3 = r8_2;
        if (*(uint16_t *)(*var_7 + 112UL) == (unsigned short)6U) {
            var_108 = *var_15;
        } else {
            var_86 = *var_15;
            var_87 = *(uint64_t *)6412440UL;
            var_108 = var_86;
            if (var_86 != '\x00' & var_87 != 0UL & *(uint64_t *)(*var_14 + 8UL) != **(uint64_t **)6412440UL & **(uint64_t **)var_13 != *(uint64_t *)(var_87 + 8UL)) {
                var_88 = *var_9;
                *(uint64_t *)(local_sp_2 + (-8L)) = 4204818UL;
                indirect_placeholder();
                var_108 = (unsigned char)'\x00';
                if ((uint64_t)(uint32_t)var_88 == 0UL) {
                    var_97 = *var_9;
                    *(uint64_t *)(local_sp_2 + (-16L)) = 4204839UL;
                    var_98 = indirect_placeholder_82(var_97, 4UL);
                    var_99 = var_98.field_0;
                    var_100 = var_98.field_1;
                    var_101 = var_98.field_2;
                    var_102 = local_sp_2 + (-24L);
                    *(uint64_t *)var_102 = 4204867UL;
                    var_103 = indirect_placeholder_81(0UL, 4296424UL, var_99, 0UL, 0UL, var_100, var_101);
                    local_sp_0 = var_102;
                    rcx_0 = var_103.field_0;
                    r9_0 = var_103.field_1;
                    r8_0 = var_103.field_2;
                } else {
                    *(uint64_t *)(local_sp_2 + (-16L)) = 4204889UL;
                    var_89 = indirect_placeholder_83(4296421UL, 4UL, 1UL);
                    var_90 = var_89.field_0;
                    var_91 = *var_9;
                    *(uint64_t *)(local_sp_2 + (-24L)) = 4204914UL;
                    var_92 = indirect_placeholder_79(var_91, 4UL, 0UL);
                    var_93 = var_92.field_0;
                    var_94 = var_92.field_1;
                    var_95 = local_sp_2 + (-32L);
                    *(uint64_t *)var_95 = 4204945UL;
                    var_96 = indirect_placeholder_78(0UL, 4296472UL, var_93, 0UL, 0UL, var_94, var_90);
                    local_sp_0 = var_95;
                    rcx_0 = var_96.field_0;
                    r9_0 = var_96.field_1;
                    r8_0 = var_96.field_2;
                }
                var_104 = local_sp_0 + (-8L);
                *(uint64_t *)var_104 = 4204970UL;
                var_105 = indirect_placeholder_80(0UL, 4296536UL, rcx_0, 0UL, 0UL, r9_0, r8_0);
                var_106 = var_105.field_1;
                var_107 = var_105.field_2;
                *var_15 = (unsigned char)'\x00';
                local_sp_3 = var_104;
                r9_3 = var_106;
                r8_3 = var_107;
            }
        }
        local_sp_5 = local_sp_3;
        local_sp_4 = local_sp_3;
        r9_4 = r9_3;
        r8_4 = r8_3;
        r9_5 = r9_3;
        r8_5 = r8_3;
        if (var_108 != '\x00') {
            if (*(unsigned char *)6412434UL == '\x00') {
                var_109 = *var_9;
                *(uint64_t *)(local_sp_3 + (-8L)) = 4205008UL;
                var_110 = indirect_placeholder_98(var_109, 4UL);
                var_111 = var_110.field_1;
                var_112 = var_110.field_2;
                var_113 = local_sp_3 + (-16L);
                *(uint64_t *)var_113 = 4205026UL;
                indirect_placeholder();
                local_sp_4 = var_113;
                r9_4 = var_111;
                r8_4 = var_112;
            }
            var_114 = (uint64_t)*(uint32_t *)(*var_6 + 44UL);
            var_115 = *var_11;
            var_116 = local_sp_4 + (-8L);
            *(uint64_t *)var_116 = 4205047UL;
            var_117 = indirect_placeholder_1(var_115, var_114);
            local_sp_5 = var_116;
            r9_5 = r9_4;
            r8_5 = r8_4;
            if ((uint64_t)(uint32_t)var_117 == 0UL) {
                *var_15 = (unsigned char)'\x00';
            }
        }
        r9_6 = r9_5;
        r8_6 = r8_5;
        if (*(unsigned char *)6412433UL == '\x01') {
            var_118 = *var_7;
            var_119 = *var_6;
            *(uint64_t *)(local_sp_5 + (-8L)) = 4205093UL;
            indirect_placeholder_2(4UL, var_118, var_119);
        }
        var_120 = (uint64_t)*var_15;
        rax_0 = var_120;
        mrv.field_0 = rax_0;
        mrv1 = mrv;
        mrv1.field_1 = r9_6;
        mrv2 = mrv1;
        mrv2.field_2 = r8_6;
        return mrv2;
    }
    switch (*(uint64_t *)(((uint64_t)var_17 << 3UL) + 4296864UL)) {
      case 4204008UL:
        {
            if (*(unsigned char *)6412433UL != '\x00') {
                var_62 = *(uint64_t *)6412440UL;
                if (var_62 != 0UL & *(uint64_t *)(var_16 + 128UL) != **(uint64_t **)6412440UL & *(uint64_t *)(var_16 + 120UL) != *(uint64_t *)(var_62 + 8UL)) {
                    var_63 = *var_9;
                    *(uint64_t *)(var_0 + (-96L)) = 4204114UL;
                    indirect_placeholder();
                    rax_0 = 0UL;
                    if ((uint64_t)(uint32_t)var_63 == 0UL) {
                        var_72 = *var_9;
                        *(uint64_t *)(var_0 + (-104L)) = 4204135UL;
                        var_73 = indirect_placeholder_93(var_72, 4UL);
                        var_74 = var_73.field_0;
                        var_75 = var_73.field_1;
                        var_76 = var_73.field_2;
                        var_77 = var_0 + (-112L);
                        *(uint64_t *)var_77 = 4204163UL;
                        var_78 = indirect_placeholder_92(0UL, 4296424UL, var_74, 0UL, 0UL, var_75, var_76);
                        local_sp_1 = var_77;
                        rcx_1 = var_78.field_0;
                        r9_1 = var_78.field_1;
                        r8_1 = var_78.field_2;
                    } else {
                        *(uint64_t *)(var_0 + (-104L)) = 4204185UL;
                        var_64 = indirect_placeholder_94(4296421UL, 4UL, 1UL);
                        var_65 = var_64.field_0;
                        var_66 = *var_9;
                        *(uint64_t *)(var_0 + (-112L)) = 4204210UL;
                        var_67 = indirect_placeholder_90(var_66, 4UL, 0UL);
                        var_68 = var_67.field_0;
                        var_69 = var_67.field_1;
                        var_70 = var_0 + (-120L);
                        *(uint64_t *)var_70 = 4204241UL;
                        var_71 = indirect_placeholder_89(0UL, 4296472UL, var_68, 0UL, 0UL, var_69, var_65);
                        local_sp_1 = var_70;
                        rcx_1 = var_71.field_0;
                        r9_1 = var_71.field_1;
                        r8_1 = var_71.field_2;
                    }
                    *(uint64_t *)(local_sp_1 + (-8L)) = 4204266UL;
                    var_79 = indirect_placeholder_91(0UL, 4296536UL, rcx_1, 0UL, 0UL, r9_1, r8_1);
                    var_80 = var_79.field_1;
                    var_81 = var_79.field_2;
                    var_82 = *var_7;
                    var_83 = *var_6;
                    *(uint64_t *)(local_sp_1 + (-16L)) = 4204290UL;
                    indirect_placeholder_2(4UL, var_82, var_83);
                    var_84 = *var_6;
                    *(uint64_t *)(local_sp_1 + (-24L)) = 4204302UL;
                    var_85 = indirect_placeholder_2(var_84, var_80, var_81);
                    *(uint64_t *)(var_0 + (-64L)) = var_85;
                    r9_6 = var_80;
                    r8_6 = var_81;
                }
                mrv.field_0 = rax_0;
                mrv1 = mrv;
                mrv1.field_1 = r9_6;
                mrv2 = mrv1;
                mrv2.field_2 = r8_6;
                return mrv2;
            }
        }
        break;
      case 4204331UL:
        {
            if (*(unsigned char *)6412433UL == '\x01') {
                mrv.field_0 = rax_0;
                mrv1 = mrv;
                mrv1.field_1 = r9_6;
                mrv2 = mrv1;
                mrv2.field_2 = r8_6;
                return mrv2;
            }
        }
        break;
      case 4204360UL:
        {
            if (*(uint64_t *)(var_16 + 88UL) != 0UL) {
                var_49 = (uint64_t *)(var_16 + 32UL);
                if (*var_49 != 0UL) {
                    *var_49 = 1UL;
                    var_60 = *var_7;
                    var_61 = *var_6;
                    *(uint64_t *)(var_0 + (-96L)) = 4204422UL;
                    indirect_placeholder_2(1UL, var_60, var_61);
                    mrv.field_0 = rax_0;
                    mrv1 = mrv;
                    mrv1.field_1 = r9_6;
                    mrv2 = mrv1;
                    mrv2.field_2 = r8_6;
                    return mrv2;
                }
            }
            var_50 = *var_9;
            *(uint64_t *)(var_0 + (-96L)) = 4204449UL;
            var_51 = indirect_placeholder_97(var_50, 4UL);
            var_52 = var_51.field_0;
            var_53 = var_51.field_1;
            var_54 = var_51.field_2;
            var_55 = (uint64_t)*(uint32_t *)(*var_7 + 64UL);
            var_56 = var_0 + (-104L);
            *(uint64_t *)var_56 = 4204484UL;
            var_57 = indirect_placeholder_88(0UL, 4296585UL, var_52, var_55, 0UL, var_53, var_54);
            var_58 = var_57.field_1;
            var_59 = var_57.field_2;
            *var_15 = (unsigned char)'\x00';
            local_sp_2 = var_56;
            r9_2 = var_58;
            r8_2 = var_59;
        }
        break;
      case 4204617UL:
        {
            var_18 = *var_6;
            var_19 = var_0 + (-96L);
            *(uint64_t *)var_19 = 4204636UL;
            var_20 = indirect_placeholder_1(var_16, var_18);
            local_sp_2 = var_19;
            rax_0 = 0UL;
            if ((uint64_t)(unsigned char)var_20 != 0UL) {
                var_21 = *var_9;
                *(uint64_t *)(var_0 + (-104L)) = 4204662UL;
                var_22 = indirect_placeholder_85(var_21, 3UL, 0UL);
                var_23 = var_22.field_0;
                var_24 = var_22.field_1;
                var_25 = var_22.field_2;
                *(uint64_t *)(var_0 + (-112L)) = 4204690UL;
                var_26 = indirect_placeholder_84(0UL, 4296632UL, var_23, 0UL, 0UL, var_24, var_25);
                var_27 = var_26.field_1;
                var_28 = var_26.field_2;
                r9_6 = var_27;
                r8_6 = var_28;
                mrv.field_0 = rax_0;
                mrv1 = mrv;
                mrv1.field_1 = r9_6;
                mrv2 = mrv1;
                mrv2.field_2 = r8_6;
                return mrv2;
            }
        }
        break;
      case 4204702UL:
      case 4204559UL:
      case 4204493UL:
        {
            switch (*(uint64_t *)(((uint64_t)var_17 << 3UL) + 4296864UL)) {
              case 4204559UL:
                {
                    var_29 = *var_9;
                    *(uint64_t *)(var_0 + (-96L)) = 4204576UL;
                    var_30 = indirect_placeholder_95(var_29, 4UL);
                    var_31 = var_30.field_0;
                    var_32 = var_30.field_1;
                    var_33 = var_30.field_2;
                    var_34 = (uint64_t)*(uint32_t *)(*var_7 + 64UL);
                    var_35 = var_0 + (-104L);
                    *(uint64_t *)var_35 = 4204611UL;
                    var_36 = indirect_placeholder_86(0UL, 4296605UL, var_31, var_34, 0UL, var_32, var_33);
                    var_37 = var_36.field_1;
                    var_38 = var_36.field_2;
                    *var_15 = (unsigned char)'\x00';
                    local_sp_2 = var_35;
                    r9_2 = var_37;
                    r8_2 = var_38;
                }
                break;
              case 4204493UL:
                {
                    var_39 = *var_9;
                    *(uint64_t *)(var_0 + (-96L)) = 4204515UL;
                    var_40 = indirect_placeholder_96(var_39, 3UL, 0UL);
                    var_41 = var_40.field_0;
                    var_42 = var_40.field_1;
                    var_43 = var_40.field_2;
                    var_44 = (uint64_t)*(uint32_t *)(*var_7 + 64UL);
                    var_45 = var_0 + (-104L);
                    *(uint64_t *)var_45 = 4204550UL;
                    var_46 = indirect_placeholder_87(0UL, 4296602UL, var_41, var_44, 0UL, var_42, var_43);
                    var_47 = var_46.field_1;
                    var_48 = var_46.field_2;
                    *var_15 = (unsigned char)'\x00';
                    local_sp_2 = var_45;
                    r9_2 = var_47;
                    r8_2 = var_48;
                }
                break;
            }
        }
        break;
      default:
        {
            abort();
        }
        break;
    }
}

typedef unsigned char __u_char;
typedef unsigned short __u_short;
typedef unsigned int __u_int;
typedef unsigned long __u_long;
typedef signed char __int8_t;
typedef unsigned char __uint8_t;
typedef short __int16_t;
typedef unsigned short __uint16_t;
typedef int __int32_t;
typedef unsigned int __uint32_t;
typedef long __int64_t;
typedef unsigned long __uint64_t;
typedef __int8_t __int_least8_t;
typedef __uint8_t __uint_least8_t;
typedef __int16_t __int_least16_t;
typedef __uint16_t __uint_least16_t;
typedef __int32_t __int_least32_t;
typedef __uint32_t __uint_least32_t;
typedef __int64_t __int_least64_t;
typedef __uint64_t __uint_least64_t;
typedef long __quad_t;
typedef unsigned long __u_quad_t;
typedef long __intmax_t;
typedef unsigned long __uintmax_t;
typedef unsigned long __dev_t;
typedef unsigned int __uid_t;
typedef unsigned int __gid_t;
typedef unsigned long __ino_t;
typedef unsigned long __ino64_t;
typedef unsigned int __mode_t;
typedef unsigned long __nlink_t;
typedef long __off_t;
typedef long __off64_t;
typedef int __pid_t;
typedef struct {
    int __val[2];
} __fsid_t;
typedef long __clock_t;
typedef unsigned long __rlim_t;
typedef unsigned long __rlim64_t;
typedef unsigned int __id_t;
typedef long __time_t;
typedef unsigned int __useconds_t;
typedef long __suseconds_t;
typedef int __daddr_t;
typedef int __key_t;
typedef int __clockid_t;
typedef void *__timer_t;
typedef long __blksize_t;
typedef long __blkcnt_t;
typedef long __blkcnt64_t;
typedef unsigned long __fsblkcnt_t;
typedef unsigned long __fsblkcnt64_t;
typedef unsigned long __fsfilcnt_t;
typedef unsigned long __fsfilcnt64_t;
typedef long __fsword_t;
typedef long __ssize_t;
typedef long __syscall_slong_t;
typedef unsigned long __syscall_ulong_t;
typedef __off64_t __loff_t;
typedef char *__caddr_t;
typedef long __intptr_t;
typedef unsigned int __socklen_t;
typedef int __sig_atomic_t;
typedef __int8_t int8_t;
typedef __int16_t int16_t;
typedef __int32_t int32_t;
typedef __int64_t int64_t;
typedef __uint8_t uint8_t;
typedef __uint16_t uint16_t;
typedef __uint32_t uint32_t;
typedef __uint64_t uint64_t;
typedef __int_least8_t int_least8_t;
typedef __int_least16_t int_least16_t;
typedef __int_least32_t int_least32_t;
typedef __int_least64_t int_least64_t;
typedef __uint_least8_t uint_least8_t;
typedef __uint_least16_t uint_least16_t;
typedef __uint_least32_t uint_least32_t;
typedef __uint_least64_t uint_least64_t;
typedef signed char int_fast8_t;
typedef long int_fast16_t;
typedef long int_fast32_t;
typedef long int_fast64_t;
typedef unsigned char uint_fast8_t;
typedef unsigned long uint_fast16_t;
typedef unsigned long uint_fast32_t;
typedef unsigned long uint_fast64_t;
typedef long intptr_t;
typedef unsigned long uintptr_t;
typedef __intmax_t intmax_t;
typedef __uintmax_t uintmax_t;
typedef _Bool bool;
struct helper_pxor_xmm_wrapper_ret_type;
struct type_5;
struct type_7;
struct helper_pxor_xmm_wrapper_160_ret_type;
struct helper_cvtsq2ss_wrapper_ret_type;
struct helper_mulss_wrapper_167_ret_type;
struct helper_addss_wrapper_ret_type;
struct helper_ucomiss_wrapper_168_ret_type;
struct helper_cvttss2sq_wrapper_ret_type;
struct helper_subss_wrapper_ret_type;
struct helper_addss_wrapper_166_ret_type;
struct helper_mulss_wrapper_ret_type;
struct helper_cvttss2sq_wrapper_169_ret_type;
struct helper_ucomiss_wrapper_ret_type;
struct helper_cvtsq2ss_wrapper_165_ret_type;
struct helper_pxor_xmm_wrapper_ret_type {
    uint64_t field_0;
    uint64_t field_1;
};
struct type_5 {
};
struct type_7 {
};
struct helper_pxor_xmm_wrapper_160_ret_type {
    uint64_t field_0;
    uint64_t field_1;
};
struct helper_cvtsq2ss_wrapper_ret_type {
    uint64_t field_0;
    unsigned char field_1;
};
struct helper_mulss_wrapper_167_ret_type {
    uint64_t field_0;
    unsigned char field_1;
};
struct helper_addss_wrapper_ret_type {
    uint64_t field_0;
    unsigned char field_1;
};
struct helper_ucomiss_wrapper_168_ret_type {
    uint64_t field_0;
    unsigned char field_1;
};
struct helper_cvttss2sq_wrapper_ret_type {
    uint64_t field_0;
    unsigned char field_1;
};
struct helper_subss_wrapper_ret_type {
    uint64_t field_0;
    unsigned char field_1;
};
struct helper_addss_wrapper_166_ret_type {
    uint64_t field_0;
    unsigned char field_1;
};
struct helper_mulss_wrapper_ret_type {
    uint64_t field_0;
    unsigned char field_1;
};
struct helper_cvttss2sq_wrapper_169_ret_type {
    uint64_t field_0;
    unsigned char field_1;
};
struct helper_ucomiss_wrapper_ret_type {
    uint64_t field_0;
    unsigned char field_1;
};
struct helper_cvtsq2ss_wrapper_165_ret_type {
    uint64_t field_0;
    unsigned char field_1;
};
extern uint64_t revng_init_local_sp(uint64_t param_0);
extern uint64_t init_rbp(void);
extern uint64_t init_cc_src2(void);
extern uint64_t helper_cc_compute_c_wrapper(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint32_t param_3);
extern uint64_t init_state_0x8558(void);
extern unsigned char init_state_0x8549(void);
extern unsigned char init_state_0x854c(void);
extern unsigned char init_state_0x8548(void);
extern unsigned char init_state_0x854b(void);
extern unsigned char init_state_0x8547(void);
extern unsigned char init_state_0x854d(void);
extern void indirect_placeholder(void);
extern struct helper_pxor_xmm_wrapper_ret_type helper_pxor_xmm_wrapper(struct type_5 *param_0, struct type_7 *param_1, struct type_7 *param_2, uint64_t param_3, uint64_t param_4);
extern uint64_t init_state_0x8598(void);
extern uint64_t init_state_0x85a0(void);
extern struct helper_pxor_xmm_wrapper_160_ret_type helper_pxor_xmm_wrapper_160(struct type_5 *param_0, struct type_7 *param_1, struct type_7 *param_2, uint64_t param_3, uint64_t param_4);
extern uint64_t init_state_0x8560(void);
extern uint64_t init_r9(void);
extern uint64_t init_r8(void);
extern struct helper_cvtsq2ss_wrapper_ret_type helper_cvtsq2ss_wrapper(struct type_5 *param_0, struct type_7 *param_1, uint64_t param_2, unsigned char param_3, unsigned char param_4, unsigned char param_5, unsigned char param_6);
extern struct helper_mulss_wrapper_167_ret_type helper_mulss_wrapper_167(struct type_5 *param_0, struct type_7 *param_1, struct type_7 *param_2, uint64_t param_3, uint64_t param_4, unsigned char param_5, unsigned char param_6, unsigned char param_7, unsigned char param_8, unsigned char param_9, unsigned char param_10);
extern uint64_t init_rbx(void);
extern uint64_t init_state_0x8d58(void);
extern struct helper_addss_wrapper_ret_type helper_addss_wrapper(struct type_5 *param_0, struct type_7 *param_1, struct type_7 *param_2, uint64_t param_3, unsigned char param_4, unsigned char param_5, unsigned char param_6, unsigned char param_7, unsigned char param_8, unsigned char param_9);
extern struct helper_ucomiss_wrapper_168_ret_type helper_ucomiss_wrapper_168(struct type_5 *param_0, struct type_7 *param_1, struct type_7 *param_2, uint64_t param_3, uint64_t param_4, unsigned char param_5, unsigned char param_6);
extern struct helper_cvttss2sq_wrapper_ret_type helper_cvttss2sq_wrapper(struct type_5 *param_0, struct type_7 *param_1, uint64_t param_2, unsigned char param_3, unsigned char param_4);
extern struct helper_subss_wrapper_ret_type helper_subss_wrapper(struct type_5 *param_0, struct type_7 *param_1, struct type_7 *param_2, uint64_t param_3, uint64_t param_4, unsigned char param_5, unsigned char param_6, unsigned char param_7, unsigned char param_8, unsigned char param_9, unsigned char param_10);
extern uint64_t init_r10(void);
extern struct helper_addss_wrapper_166_ret_type helper_addss_wrapper_166(struct type_5 *param_0, struct type_7 *param_1, struct type_7 *param_2, uint64_t param_3, unsigned char param_4, unsigned char param_5, unsigned char param_6, unsigned char param_7, unsigned char param_8, unsigned char param_9);
extern struct helper_mulss_wrapper_ret_type helper_mulss_wrapper(struct type_5 *param_0, struct type_7 *param_1, struct type_7 *param_2, uint64_t param_3, uint64_t param_4, unsigned char param_5, unsigned char param_6, unsigned char param_7, unsigned char param_8, unsigned char param_9, unsigned char param_10);
extern uint64_t indirect_placeholder_24(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint64_t param_3);
extern uint64_t indirect_placeholder_10(uint64_t param_0);
extern uint64_t indirect_placeholder_31(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint64_t param_3, uint64_t param_4, uint64_t param_5, uint64_t param_6);
extern struct helper_cvttss2sq_wrapper_169_ret_type helper_cvttss2sq_wrapper_169(struct type_5 *param_0, struct type_7 *param_1, uint64_t param_2, unsigned char param_3, unsigned char param_4);
extern struct helper_ucomiss_wrapper_ret_type helper_ucomiss_wrapper(struct type_5 *param_0, struct type_7 *param_1, struct type_7 *param_2, uint64_t param_3, uint64_t param_4, unsigned char param_5, unsigned char param_6);
extern struct helper_cvtsq2ss_wrapper_165_ret_type helper_cvtsq2ss_wrapper_165(struct type_5 *param_0, struct type_7 *param_1, uint64_t param_2, unsigned char param_3, unsigned char param_4, unsigned char param_5, unsigned char param_6);
uint64_t bb_hash_insert_if_absent(uint64_t rdx, uint64_t rsi, uint64_t rdi) {
    struct helper_cvtsq2ss_wrapper_165_ret_type var_71;
    struct helper_addss_wrapper_166_ret_type var_72;
    unsigned char storemerge6;
    struct helper_mulss_wrapper_ret_type var_73;
    struct helper_ucomiss_wrapper_ret_type var_74;
    uint64_t var_75;
    unsigned char var_76;
    uint64_t var_77;
    uint64_t *var_78;
    bool var_79;
    uint64_t var_80;
    bool var_81;
    struct helper_cvtsq2ss_wrapper_ret_type var_82;
    uint64_t cc_dst_0;
    uint64_t var_83;
    struct helper_pxor_xmm_wrapper_160_ret_type var_42;
    uint64_t var_43;
    struct helper_cvtsq2ss_wrapper_ret_type var_84;
    struct helper_addss_wrapper_ret_type var_85;
    uint64_t state_0x8558_1;
    unsigned char storemerge7;
    struct helper_mulss_wrapper_167_ret_type var_86;
    uint64_t cc_dst_2;
    struct helper_cvtsq2ss_wrapper_ret_type var_87;
    uint64_t cc_dst_1;
    uint64_t var_88;
    struct helper_cvtsq2ss_wrapper_ret_type var_89;
    struct helper_addss_wrapper_ret_type var_90;
    uint64_t state_0x8558_2;
    unsigned char storemerge9;
    struct helper_mulss_wrapper_167_ret_type var_91;
    struct helper_mulss_wrapper_167_ret_type var_92;
    unsigned char storemerge10;
    uint64_t storemerge8_in;
    uint32_t *var_93;
    uint32_t var_94;
    uint64_t var_95;
    uint64_t var_96;
    uint64_t var_97;
    struct helper_ucomiss_wrapper_168_ret_type var_98;
    uint64_t var_99;
    struct helper_ucomiss_wrapper_168_ret_type var_100;
    uint64_t var_101;
    unsigned char var_102;
    uint64_t var_103;
    bool var_104;
    uint32_t var_105;
    struct helper_cvttss2sq_wrapper_169_ret_type var_106;
    uint64_t rax_0;
    struct helper_subss_wrapper_ret_type var_107;
    struct helper_cvttss2sq_wrapper_ret_type var_108;
    uint64_t var_109;
    uint64_t var_110;
    uint64_t var_30;
    struct helper_pxor_xmm_wrapper_ret_type var_31;
    uint64_t var_32;
    struct helper_cvtsq2ss_wrapper_ret_type var_33;
    uint64_t state_0x8560_1;
    uint64_t var_34;
    struct helper_pxor_xmm_wrapper_ret_type var_35;
    uint64_t var_36;
    struct helper_cvtsq2ss_wrapper_ret_type var_37;
    struct helper_addss_wrapper_ret_type var_38;
    uint64_t var_39;
    uint64_t var_40;
    uint64_t var_41;
    struct helper_cvtsq2ss_wrapper_165_ret_type var_44;
    uint64_t state_0x85a0_0;
    uint64_t var_45;
    struct helper_pxor_xmm_wrapper_160_ret_type var_46;
    uint64_t var_47;
    struct helper_cvtsq2ss_wrapper_165_ret_type var_48;
    struct helper_addss_wrapper_166_ret_type var_49;
    unsigned char storemerge4;
    struct helper_mulss_wrapper_ret_type var_50;
    uint64_t var_51;
    struct helper_ucomiss_wrapper_ret_type var_52;
    uint64_t var_53;
    unsigned char var_54;
    uint64_t var_55;
    uint64_t var_56;
    uint64_t var_57;
    uint64_t *var_120;
    uint64_t *var_123;
    uint64_t *var_124;
    uint64_t var_24;
    uint64_t local_sp_1;
    uint64_t var_26;
    uint64_t var_27;
    uint64_t var_28;
    uint64_t var_29;
    uint64_t *var_116;
    uint64_t var_117;
    uint64_t var_118;
    uint64_t var_119;
    struct helper_cvtsq2ss_wrapper_ret_type var_60;
    struct helper_pxor_xmm_wrapper_ret_type var_58;
    struct helper_cvtsq2ss_wrapper_ret_type var_64;
    struct helper_pxor_xmm_wrapper_ret_type var_62;
    uint64_t var_0;
    uint64_t var_1;
    uint64_t var_2;
    uint64_t var_3;
    uint64_t var_4;
    uint64_t var_5;
    uint64_t var_6;
    uint64_t var_7;
    uint64_t var_8;
    uint64_t var_9;
    uint64_t var_10;
    uint64_t var_11;
    unsigned char var_12;
    unsigned char var_13;
    unsigned char var_14;
    unsigned char var_15;
    unsigned char var_16;
    unsigned char var_17;
    uint64_t var_18;
    uint64_t *var_19;
    uint64_t *var_20;
    uint64_t var_21;
    uint64_t *var_22;
    uint64_t var_23;
    uint64_t var_25;
    uint64_t *var_121;
    uint64_t *var_122;
    uint64_t rax_1;
    uint64_t var_115;
    uint64_t local_sp_0;
    uint64_t var_111;
    uint64_t var_112;
    uint64_t var_113;
    uint64_t var_114;
    unsigned char storemerge;
    uint64_t state_0x8558_3;
    uint64_t var_59;
    uint64_t state_0x8560_0;
    uint64_t var_61;
    uint64_t var_63;
    struct helper_addss_wrapper_ret_type var_65;
    uint64_t state_0x8558_0;
    unsigned char storemerge5;
    uint64_t var_66;
    uint64_t var_67;
    uint64_t var_68;
    uint64_t state_0x8598_1;
    struct helper_cvtsq2ss_wrapper_165_ret_type var_69;
    uint64_t state_0x8598_0;
    uint64_t var_70;
    var_0 = revng_init_local_sp(0UL);
    var_1 = init_state_0x8558();
    var_2 = init_state_0x8598();
    var_3 = init_state_0x85a0();
    var_4 = init_state_0x8560();
    var_5 = init_rbp();
    var_6 = init_cc_src2();
    var_7 = init_rbx();
    var_8 = init_r10();
    var_9 = init_r9();
    var_10 = init_r8();
    var_11 = init_state_0x8d58();
    var_12 = init_state_0x8549();
    var_13 = init_state_0x854c();
    var_14 = init_state_0x8548();
    var_15 = init_state_0x854b();
    var_16 = init_state_0x8547();
    var_17 = init_state_0x854d();
    *(uint64_t *)(var_0 + (-8L)) = var_5;
    var_18 = var_0 + (-88L);
    var_19 = (uint64_t *)(var_0 + (-64L));
    *var_19 = rdi;
    var_20 = (uint64_t *)(var_0 + (-72L));
    *var_20 = rsi;
    var_21 = var_0 + (-80L);
    var_22 = (uint64_t *)var_21;
    *var_22 = rdx;
    var_23 = *var_20;
    rax_1 = 0UL;
    var_25 = var_23;
    local_sp_1 = var_18;
    if (var_23 == 0UL) {
        var_24 = var_0 + (-96L);
        *(uint64_t *)var_24 = 4238086UL;
        indirect_placeholder();
        var_25 = *var_20;
        local_sp_1 = var_24;
    }
    var_26 = var_0 + (-48L);
    var_27 = *var_19;
    var_28 = local_sp_1 + (-8L);
    *(uint64_t *)var_28 = 4238111UL;
    var_29 = indirect_placeholder_24(var_26, 0UL, var_25, var_27);
    *(uint64_t *)(var_0 + (-16L)) = var_29;
    local_sp_0 = var_28;
    if (var_29 == 0UL) {
        var_30 = *(uint64_t *)(*var_19 + 24UL);
        rax_1 = 4294967295UL;
        if ((long)var_30 < (long)0UL) {
            var_34 = (var_30 >> 1UL) | (var_30 & 1UL);
            var_35 = helper_pxor_xmm_wrapper((struct type_5 *)(0UL), (struct type_7 *)(776UL), (struct type_7 *)(776UL), var_1, var_4);
            var_36 = var_35.field_1;
            var_37 = helper_cvtsq2ss_wrapper((struct type_5 *)(0UL), (struct type_7 *)(776UL), var_34, var_12, var_14, var_15, var_16);
            var_38 = helper_addss_wrapper((struct type_5 *)(0UL), (struct type_7 *)(776UL), (struct type_7 *)(776UL), var_37.field_0, var_37.field_1, var_13, var_14, var_15, var_16, var_17);
            state_0x8560_1 = var_36;
            state_0x8558_3 = var_38.field_0;
            storemerge = var_38.field_1;
        } else {
            var_31 = helper_pxor_xmm_wrapper((struct type_5 *)(0UL), (struct type_7 *)(776UL), (struct type_7 *)(776UL), var_1, var_4);
            var_32 = var_31.field_1;
            var_33 = helper_cvtsq2ss_wrapper((struct type_5 *)(0UL), (struct type_7 *)(776UL), var_30, var_12, var_14, var_15, var_16);
            state_0x8560_1 = var_32;
            state_0x8558_3 = var_33.field_0;
            storemerge = var_33.field_1;
        }
        var_39 = *var_19;
        var_40 = (uint64_t)*(uint32_t *)(*(uint64_t *)(var_39 + 40UL) + 8UL);
        var_41 = *(uint64_t *)(var_39 + 16UL);
        if ((long)var_41 < (long)0UL) {
            var_45 = (var_41 >> 1UL) | (var_41 & 1UL);
            var_46 = helper_pxor_xmm_wrapper_160((struct type_5 *)(0UL), (struct type_7 *)(840UL), (struct type_7 *)(840UL), var_2, var_3);
            var_47 = var_46.field_1;
            var_48 = helper_cvtsq2ss_wrapper_165((struct type_5 *)(0UL), (struct type_7 *)(840UL), var_45, storemerge, var_14, var_15, var_16);
            var_49 = helper_addss_wrapper_166((struct type_5 *)(0UL), (struct type_7 *)(840UL), (struct type_7 *)(840UL), var_48.field_0, var_48.field_1, var_13, var_14, var_15, var_16, var_17);
            state_0x85a0_0 = var_47;
            state_0x8598_1 = var_49.field_0;
            storemerge4 = var_49.field_1;
        } else {
            var_42 = helper_pxor_xmm_wrapper_160((struct type_5 *)(0UL), (struct type_7 *)(840UL), (struct type_7 *)(840UL), var_2, var_3);
            var_43 = var_42.field_1;
            var_44 = helper_cvtsq2ss_wrapper_165((struct type_5 *)(0UL), (struct type_7 *)(840UL), var_41, storemerge, var_14, var_15, var_16);
            state_0x85a0_0 = var_43;
            state_0x8598_1 = var_44.field_0;
            storemerge4 = var_44.field_1;
        }
        var_50 = helper_mulss_wrapper((struct type_5 *)(0UL), (struct type_7 *)(840UL), (struct type_7 *)(904UL), state_0x8598_1, var_40, storemerge4, var_13, var_14, var_15, var_16, var_17);
        var_51 = var_50.field_0;
        var_52 = helper_ucomiss_wrapper((struct type_5 *)(0UL), (struct type_7 *)(776UL), (struct type_7 *)(840UL), state_0x8558_3, var_51, var_50.field_1, var_13);
        var_53 = var_52.field_0;
        var_54 = var_52.field_1;
        if ((var_53 & 65UL) != 0UL) {
            var_55 = *var_19;
            var_56 = local_sp_1 + (-16L);
            *(uint64_t *)var_56 = 4238286UL;
            indirect_placeholder_10(var_55);
            var_57 = *(uint64_t *)(*var_19 + 24UL);
            local_sp_0 = var_56;
            if ((long)var_57 < (long)0UL) {
                var_61 = (var_57 >> 1UL) | (var_57 & 1UL);
                var_62 = helper_pxor_xmm_wrapper((struct type_5 *)(0UL), (struct type_7 *)(776UL), (struct type_7 *)(776UL), state_0x8558_3, state_0x8560_1);
                var_63 = var_62.field_1;
                var_64 = helper_cvtsq2ss_wrapper((struct type_5 *)(0UL), (struct type_7 *)(776UL), var_61, var_54, var_14, var_15, var_16);
                var_65 = helper_addss_wrapper((struct type_5 *)(0UL), (struct type_7 *)(776UL), (struct type_7 *)(776UL), var_64.field_0, var_64.field_1, var_13, var_14, var_15, var_16, var_17);
                state_0x8560_0 = var_63;
                state_0x8558_0 = var_65.field_0;
                storemerge5 = var_65.field_1;
            } else {
                var_58 = helper_pxor_xmm_wrapper((struct type_5 *)(0UL), (struct type_7 *)(776UL), (struct type_7 *)(776UL), state_0x8558_3, state_0x8560_1);
                var_59 = var_58.field_1;
                var_60 = helper_cvtsq2ss_wrapper((struct type_5 *)(0UL), (struct type_7 *)(776UL), var_57, var_54, var_14, var_15, var_16);
                state_0x8560_0 = var_59;
                state_0x8558_0 = var_60.field_0;
                storemerge5 = var_60.field_1;
            }
            var_66 = *var_19;
            var_67 = (uint64_t)*(uint32_t *)(*(uint64_t *)(var_66 + 40UL) + 8UL);
            var_68 = *(uint64_t *)(var_66 + 16UL);
            if ((long)var_68 < (long)0UL) {
                var_70 = (var_68 >> 1UL) | (var_68 & 1UL);
                helper_pxor_xmm_wrapper_160((struct type_5 *)(0UL), (struct type_7 *)(840UL), (struct type_7 *)(840UL), var_51, state_0x85a0_0);
                var_71 = helper_cvtsq2ss_wrapper_165((struct type_5 *)(0UL), (struct type_7 *)(840UL), var_70, storemerge5, var_14, var_15, var_16);
                var_72 = helper_addss_wrapper_166((struct type_5 *)(0UL), (struct type_7 *)(840UL), (struct type_7 *)(840UL), var_71.field_0, var_71.field_1, var_13, var_14, var_15, var_16, var_17);
                state_0x8598_0 = var_72.field_0;
                storemerge6 = var_72.field_1;
            } else {
                helper_pxor_xmm_wrapper_160((struct type_5 *)(0UL), (struct type_7 *)(840UL), (struct type_7 *)(840UL), var_51, state_0x85a0_0);
                var_69 = helper_cvtsq2ss_wrapper_165((struct type_5 *)(0UL), (struct type_7 *)(840UL), var_68, storemerge5, var_14, var_15, var_16);
                state_0x8598_0 = var_69.field_0;
                storemerge6 = var_69.field_1;
            }
            var_73 = helper_mulss_wrapper((struct type_5 *)(0UL), (struct type_7 *)(840UL), (struct type_7 *)(904UL), state_0x8598_0, var_67, storemerge6, var_13, var_14, var_15, var_16, var_17);
            var_74 = helper_ucomiss_wrapper((struct type_5 *)(0UL), (struct type_7 *)(776UL), (struct type_7 *)(840UL), state_0x8558_0, var_73.field_0, var_73.field_1, var_13);
            var_75 = var_74.field_0;
            var_76 = var_74.field_1;
            if ((var_75 & 65UL) != 0UL) {
                var_77 = *(uint64_t *)(*var_19 + 40UL);
                var_78 = (uint64_t *)(var_0 + (-24L));
                *var_78 = var_77;
                var_79 = (*(unsigned char *)(var_77 + 16UL) == '\x00');
                var_80 = *(uint64_t *)(*var_19 + 16UL);
                var_81 = ((long)var_80 < (long)0UL);
                cc_dst_0 = var_80;
                cc_dst_1 = var_80;
                if (var_79) {
                    if (var_81) {
                        var_88 = (var_80 >> 1UL) | (var_80 & 1UL);
                        helper_pxor_xmm_wrapper((struct type_5 *)(0UL), (struct type_7 *)(776UL), (struct type_7 *)(776UL), state_0x8558_0, state_0x8560_0);
                        var_89 = helper_cvtsq2ss_wrapper((struct type_5 *)(0UL), (struct type_7 *)(776UL), var_88, var_76, var_14, var_15, var_16);
                        var_90 = helper_addss_wrapper((struct type_5 *)(0UL), (struct type_7 *)(776UL), (struct type_7 *)(776UL), var_89.field_0, var_89.field_1, var_13, var_14, var_15, var_16, var_17);
                        cc_dst_1 = var_88;
                        state_0x8558_2 = var_90.field_0;
                        storemerge9 = var_90.field_1;
                    } else {
                        helper_pxor_xmm_wrapper((struct type_5 *)(0UL), (struct type_7 *)(776UL), (struct type_7 *)(776UL), state_0x8558_0, state_0x8560_0);
                        var_87 = helper_cvtsq2ss_wrapper((struct type_5 *)(0UL), (struct type_7 *)(776UL), var_80, var_76, var_14, var_15, var_16);
                        state_0x8558_2 = var_87.field_0;
                        storemerge9 = var_87.field_1;
                    }
                    var_91 = helper_mulss_wrapper_167((struct type_5 *)(0UL), (struct type_7 *)(776UL), (struct type_7 *)(840UL), state_0x8558_2, (uint64_t)*(uint32_t *)(*var_78 + 12UL), storemerge9, var_13, var_14, var_15, var_16, var_17);
                    var_92 = helper_mulss_wrapper_167((struct type_5 *)(0UL), (struct type_7 *)(776UL), (struct type_7 *)(840UL), var_91.field_0, (uint64_t)*(uint32_t *)(*var_78 + 8UL), var_91.field_1, var_13, var_14, var_15, var_16, var_17);
                    cc_dst_2 = cc_dst_1;
                    storemerge10 = var_92.field_1;
                    storemerge8_in = var_92.field_0;
                } else {
                    if (var_81) {
                        var_83 = (var_80 >> 1UL) | (var_80 & 1UL);
                        helper_pxor_xmm_wrapper((struct type_5 *)(0UL), (struct type_7 *)(776UL), (struct type_7 *)(776UL), state_0x8558_0, state_0x8560_0);
                        var_84 = helper_cvtsq2ss_wrapper((struct type_5 *)(0UL), (struct type_7 *)(776UL), var_83, var_76, var_14, var_15, var_16);
                        var_85 = helper_addss_wrapper((struct type_5 *)(0UL), (struct type_7 *)(776UL), (struct type_7 *)(776UL), var_84.field_0, var_84.field_1, var_13, var_14, var_15, var_16, var_17);
                        cc_dst_0 = var_83;
                        state_0x8558_1 = var_85.field_0;
                        storemerge7 = var_85.field_1;
                    } else {
                        helper_pxor_xmm_wrapper((struct type_5 *)(0UL), (struct type_7 *)(776UL), (struct type_7 *)(776UL), state_0x8558_0, state_0x8560_0);
                        var_82 = helper_cvtsq2ss_wrapper((struct type_5 *)(0UL), (struct type_7 *)(776UL), var_80, var_76, var_14, var_15, var_16);
                        state_0x8558_1 = var_82.field_0;
                        storemerge7 = var_82.field_1;
                    }
                    var_86 = helper_mulss_wrapper_167((struct type_5 *)(0UL), (struct type_7 *)(776UL), (struct type_7 *)(840UL), state_0x8558_1, (uint64_t)*(uint32_t *)(*var_78 + 12UL), storemerge7, var_13, var_14, var_15, var_16, var_17);
                    cc_dst_2 = cc_dst_0;
                    storemerge10 = var_86.field_1;
                    storemerge8_in = var_86.field_0;
                }
                var_93 = (uint32_t *)(var_0 + (-28L));
                var_94 = (uint32_t)storemerge8_in;
                *var_93 = var_94;
                var_95 = (uint64_t)var_94;
                var_96 = (uint64_t)*(uint32_t *)4302868UL;
                var_97 = var_11 & (-4294967296L);
                var_98 = helper_ucomiss_wrapper_168((struct type_5 *)(0UL), (struct type_7 *)(776UL), (struct type_7 *)(2824UL), var_95, var_97 | var_96, storemerge10, var_13);
                var_99 = helper_cc_compute_c_wrapper(cc_dst_2, var_98.field_0, var_6, 1U);
                if (var_99 == 0UL) {
                    return rax_1;
                }
                var_100 = helper_ucomiss_wrapper_168((struct type_5 *)(0UL), (struct type_7 *)(776UL), (struct type_7 *)(2824UL), (uint64_t)*var_93, var_97 | (uint64_t)*(uint32_t *)4302872UL, var_98.field_1, var_13);
                var_101 = var_100.field_0;
                var_102 = var_100.field_1;
                var_103 = helper_cc_compute_c_wrapper(cc_dst_2, var_101, var_6, 1U);
                var_104 = (var_103 == 0UL);
                var_105 = *var_93;
                if (var_104) {
                    var_107 = helper_subss_wrapper((struct type_5 *)(0UL), (struct type_7 *)(776UL), (struct type_7 *)(840UL), (uint64_t)var_105, (uint64_t)*(uint32_t *)4302872UL, var_102, var_13, var_14, var_15, var_16, var_17);
                    var_108 = helper_cvttss2sq_wrapper((struct type_5 *)(0UL), (struct type_7 *)(776UL), var_107.field_0, var_107.field_1, var_13);
                    rax_0 = var_108.field_0 ^ (-9223372036854775808L);
                } else {
                    *(uint32_t *)(var_0 + (-84L)) = var_105;
                    var_106 = helper_cvttss2sq_wrapper_169((struct type_5 *)(0UL), (struct type_7 *)(2824UL), var_97 | (uint64_t)var_105, var_102, var_13);
                    rax_0 = var_106.field_0;
                }
                var_109 = *var_19;
                *(uint64_t *)(local_sp_1 + (-24L)) = 4238686UL;
                var_110 = indirect_placeholder_31(0UL, var_7, rax_0, var_109, var_8, var_9, var_10);
                if ((uint64_t)(unsigned char)var_110 == 1UL) {
                    return rax_1;
                }
                var_111 = *var_20;
                var_112 = *var_19;
                var_113 = local_sp_1 + (-32L);
                *(uint64_t *)var_113 = 4238728UL;
                var_114 = indirect_placeholder_24(var_26, 0UL, var_111, var_112);
                local_sp_0 = var_113;
                if (var_114 == 0UL) {
                    var_115 = local_sp_1 + (-40L);
                    *(uint64_t *)var_115 = 4238738UL;
                    indirect_placeholder();
                    local_sp_0 = var_115;
                }
            }
        }
        var_116 = *(uint64_t **)var_26;
        rax_1 = 1UL;
        if (*var_116 == 0UL) {
            *var_116 = *var_20;
            var_123 = (uint64_t *)(*var_19 + 32UL);
            *var_123 = (*var_123 + 1UL);
            var_124 = (uint64_t *)(*var_19 + 24UL);
            *var_124 = (*var_124 + 1UL);
        } else {
            var_117 = *var_19;
            *(uint64_t *)(local_sp_0 + (-8L)) = 4238762UL;
            var_118 = indirect_placeholder_10(var_117);
            var_119 = var_0 + (-40L);
            var_120 = (uint64_t *)var_119;
            *var_120 = var_118;
            if (var_118 == 0UL) {
                **(uint64_t **)var_119 = *var_20;
                var_121 = (uint64_t *)var_26;
                *(uint64_t *)(*var_120 + 8UL) = *(uint64_t *)(*var_121 + 8UL);
                *(uint64_t *)(*var_121 + 8UL) = *var_120;
                var_122 = (uint64_t *)(*var_19 + 32UL);
                *var_122 = (*var_122 + 1UL);
                rax_1 = 1UL;
            }
        }
    } else {
        if (*var_22 == 0UL) {
            **(uint64_t **)var_21 = var_29;
        }
    }
    return rax_1;
}

typedef unsigned char __u_char;
typedef unsigned short __u_short;
typedef unsigned int __u_int;
typedef unsigned long __u_long;
typedef signed char __int8_t;
typedef unsigned char __uint8_t;
typedef short __int16_t;
typedef unsigned short __uint16_t;
typedef int __int32_t;
typedef unsigned int __uint32_t;
typedef long __int64_t;
typedef unsigned long __uint64_t;
typedef __int8_t __int_least8_t;
typedef __uint8_t __uint_least8_t;
typedef __int16_t __int_least16_t;
typedef __uint16_t __uint_least16_t;
typedef __int32_t __int_least32_t;
typedef __uint32_t __uint_least32_t;
typedef __int64_t __int_least64_t;
typedef __uint64_t __uint_least64_t;
typedef long __quad_t;
typedef unsigned long __u_quad_t;
typedef long __intmax_t;
typedef unsigned long __uintmax_t;
typedef unsigned long __dev_t;
typedef unsigned int __uid_t;
typedef unsigned int __gid_t;
typedef unsigned long __ino_t;
typedef unsigned long __ino64_t;
typedef unsigned int __mode_t;
typedef unsigned long __nlink_t;
typedef long __off_t;
typedef long __off64_t;
typedef int __pid_t;
typedef struct {
    int __val[2];
} __fsid_t;
typedef long __clock_t;
typedef unsigned long __rlim_t;
typedef unsigned long __rlim64_t;
typedef unsigned int __id_t;
typedef long __time_t;
typedef unsigned int __useconds_t;
typedef long __suseconds_t;
typedef int __daddr_t;
typedef int __key_t;
typedef int __clockid_t;
typedef void *__timer_t;
typedef long __blksize_t;
typedef long __blkcnt_t;
typedef long __blkcnt64_t;
typedef unsigned long __fsblkcnt_t;
typedef unsigned long __fsblkcnt64_t;
typedef unsigned long __fsfilcnt_t;
typedef unsigned long __fsfilcnt64_t;
typedef long __fsword_t;
typedef long __ssize_t;
typedef long __syscall_slong_t;
typedef unsigned long __syscall_ulong_t;
typedef __off64_t __loff_t;
typedef char *__caddr_t;
typedef long __intptr_t;
typedef unsigned int __socklen_t;
typedef int __sig_atomic_t;
typedef __int8_t int8_t;
typedef __int16_t int16_t;
typedef __int32_t int32_t;
typedef __int64_t int64_t;
typedef __uint8_t uint8_t;
typedef __uint16_t uint16_t;
typedef __uint32_t uint32_t;
typedef __uint64_t uint64_t;
typedef __int_least8_t int_least8_t;
typedef __int_least16_t int_least16_t;
typedef __int_least32_t int_least32_t;
typedef __int_least64_t int_least64_t;
typedef __uint_least8_t uint_least8_t;
typedef __uint_least16_t uint_least16_t;
typedef __uint_least32_t uint_least32_t;
typedef __uint_least64_t uint_least64_t;
typedef signed char int_fast8_t;
typedef long int_fast16_t;
typedef long int_fast32_t;
typedef long int_fast64_t;
typedef unsigned char uint_fast8_t;
typedef unsigned long uint_fast16_t;
typedef unsigned long uint_fast32_t;
typedef unsigned long uint_fast64_t;
typedef long intptr_t;
typedef unsigned long uintptr_t;
typedef __intmax_t intmax_t;
typedef __uintmax_t uintmax_t;
struct indirect_placeholder_112_ret_type;
struct indirect_placeholder_111_ret_type;
struct indirect_placeholder_114_ret_type;
struct indirect_placeholder_113_ret_type;
struct indirect_placeholder_116_ret_type;
struct indirect_placeholder_115_ret_type;
struct indirect_placeholder_118_ret_type;
struct indirect_placeholder_117_ret_type;
struct indirect_placeholder_120_ret_type;
struct indirect_placeholder_119_ret_type;
struct indirect_placeholder_112_ret_type {
    uint64_t field_0;
    uint64_t field_1;
    uint64_t field_2;
};
struct indirect_placeholder_111_ret_type {
    uint64_t field_0;
    uint64_t field_1;
    uint64_t field_2;
};
struct indirect_placeholder_114_ret_type {
    uint64_t field_0;
    uint64_t field_1;
    uint64_t field_2;
};
struct indirect_placeholder_113_ret_type {
    uint64_t field_0;
    uint64_t field_1;
    uint64_t field_2;
};
struct indirect_placeholder_116_ret_type {
    uint64_t field_0;
    uint64_t field_1;
    uint64_t field_2;
};
struct indirect_placeholder_115_ret_type {
    uint64_t field_0;
    uint64_t field_1;
    uint64_t field_2;
};
struct indirect_placeholder_118_ret_type {
    uint64_t field_0;
    uint64_t field_1;
    uint64_t field_2;
};
struct indirect_placeholder_117_ret_type {
    uint64_t field_0;
    uint64_t field_1;
    uint64_t field_2;
};
struct indirect_placeholder_120_ret_type {
    uint64_t field_0;
    uint64_t field_1;
    uint64_t field_2;
};
struct indirect_placeholder_119_ret_type {
    uint64_t field_0;
    uint64_t field_1;
    uint64_t field_2;
};
extern uint64_t revng_init_local_sp(uint64_t param_0);
extern uint64_t init_rbp(void);
extern uint64_t indirect_placeholder_1(uint64_t param_0, uint64_t param_1);
extern void indirect_placeholder(void);
extern uint64_t init_rbx(void);
extern void indirect_placeholder_14(uint64_t param_0);
extern uint64_t indirect_placeholder_10(uint64_t param_0);
extern struct indirect_placeholder_112_ret_type indirect_placeholder_112(uint64_t param_0);
extern struct indirect_placeholder_111_ret_type indirect_placeholder_111(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint64_t param_3, uint64_t param_4, uint64_t param_5, uint64_t param_6);
extern struct indirect_placeholder_114_ret_type indirect_placeholder_114(uint64_t param_0);
extern struct indirect_placeholder_113_ret_type indirect_placeholder_113(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint64_t param_3, uint64_t param_4, uint64_t param_5, uint64_t param_6);
extern struct indirect_placeholder_116_ret_type indirect_placeholder_116(uint64_t param_0);
extern struct indirect_placeholder_115_ret_type indirect_placeholder_115(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint64_t param_3, uint64_t param_4, uint64_t param_5, uint64_t param_6);
extern struct indirect_placeholder_118_ret_type indirect_placeholder_118(uint64_t param_0);
extern struct indirect_placeholder_117_ret_type indirect_placeholder_117(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint64_t param_3, uint64_t param_4, uint64_t param_5, uint64_t param_6);
extern struct indirect_placeholder_120_ret_type indirect_placeholder_120(uint64_t param_0);
extern struct indirect_placeholder_119_ret_type indirect_placeholder_119(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint64_t param_3, uint64_t param_4, uint64_t param_5, uint64_t param_6);
uint64_t bb_compute_context_from_mask(uint64_t rsi, uint64_t rdi) {
    uint64_t local_sp_0;
    uint64_t var_0;
    uint64_t var_1;
    uint64_t var_2;
    uint64_t *var_3;
    uint64_t var_4;
    unsigned char *var_5;
    uint64_t var_6;
    uint64_t var_7;
    uint64_t var_8;
    uint32_t *var_9;
    uint32_t var_10;
    uint64_t var_45;
    struct indirect_placeholder_112_ret_type var_46;
    uint64_t var_47;
    uint64_t var_48;
    uint64_t var_49;
    uint64_t var_50;
    uint64_t local_sp_3;
    uint64_t var_35;
    struct indirect_placeholder_114_ret_type var_36;
    uint64_t var_37;
    uint64_t var_38;
    uint64_t var_39;
    uint64_t var_40;
    uint64_t local_sp_2;
    uint64_t var_25;
    struct indirect_placeholder_116_ret_type var_26;
    uint64_t var_27;
    uint64_t var_28;
    uint64_t var_29;
    uint64_t var_30;
    uint64_t local_sp_1;
    uint64_t var_15;
    struct indirect_placeholder_118_ret_type var_16;
    uint64_t var_17;
    uint64_t var_18;
    uint64_t var_19;
    uint64_t var_20;
    uint64_t var_56;
    struct indirect_placeholder_120_ret_type var_57;
    uint64_t var_58;
    uint64_t var_59;
    uint64_t var_60;
    uint64_t var_61;
    uint64_t rax_0;
    uint64_t var_11;
    uint64_t var_12;
    uint64_t var_13;
    uint64_t var_14;
    uint64_t var_21;
    uint64_t var_22;
    uint64_t var_23;
    uint64_t var_24;
    uint64_t var_31;
    uint64_t var_32;
    uint64_t var_33;
    uint64_t var_34;
    uint64_t var_41;
    uint64_t var_42;
    uint64_t var_43;
    uint64_t var_44;
    unsigned char var_51;
    uint64_t var_52;
    uint32_t var_53;
    uint32_t *var_54;
    uint64_t var_55;
    var_0 = revng_init_local_sp(0UL);
    var_1 = init_rbp();
    var_2 = init_rbx();
    *(uint64_t *)(var_0 + (-8L)) = var_1;
    *(uint64_t *)(var_0 + (-16L)) = var_2;
    var_3 = (uint64_t *)(var_0 + (-48L));
    *var_3 = rdi;
    var_4 = var_0 + (-56L);
    *(uint64_t *)var_4 = rsi;
    var_5 = (unsigned char *)(var_0 + (-25L));
    *var_5 = (unsigned char)'\x01';
    var_6 = *var_3;
    var_7 = var_0 + (-64L);
    *(uint64_t *)var_7 = 4202835UL;
    var_8 = indirect_placeholder_10(var_6);
    var_9 = (uint32_t *)(var_0 + (-32L));
    var_10 = (uint32_t)var_8;
    *var_9 = var_10;
    local_sp_0 = var_7;
    rax_0 = 1UL;
    if (var_10 == 0U) {
        var_56 = *var_3;
        *(uint64_t *)(var_0 + (-72L)) = 4202856UL;
        var_57 = indirect_placeholder_120(var_56);
        var_58 = var_57.field_0;
        var_59 = var_57.field_1;
        var_60 = var_57.field_2;
        *(uint64_t *)(var_0 + (-80L)) = 4202864UL;
        indirect_placeholder();
        var_61 = (uint64_t)*(uint32_t *)var_58;
        *(uint64_t *)(var_0 + (-88L)) = 4202891UL;
        indirect_placeholder_119(0UL, 4296192UL, var_58, var_61, 0UL, var_59, var_60);
    } else {
        var_11 = *(uint64_t *)6412456UL;
        rax_0 = 0UL;
        var_12 = (uint64_t)var_10;
        var_13 = var_0 + (-72L);
        *(uint64_t *)var_13 = 4202933UL;
        var_14 = indirect_placeholder_1(var_11, var_12);
        local_sp_0 = var_13;
        if (var_11 != 0UL & (uint64_t)(uint32_t)var_14 == 0UL) {
            var_15 = *(uint64_t *)6412456UL;
            *(uint64_t *)(var_0 + (-80L)) = 4202952UL;
            var_16 = indirect_placeholder_118(var_15);
            var_17 = var_16.field_0;
            var_18 = var_16.field_1;
            *(uint64_t *)(var_0 + (-88L)) = 4202960UL;
            indirect_placeholder();
            var_19 = (uint64_t)*(uint32_t *)var_17;
            var_20 = var_0 + (-96L);
            *(uint64_t *)var_20 = 4202992UL;
            indirect_placeholder_117(0UL, 4296232UL, 4295689UL, var_19, 0UL, var_18, var_17);
            *var_5 = (unsigned char)'\x00';
            local_sp_0 = var_20;
        }
        var_21 = *(uint64_t *)6412472UL;
        local_sp_1 = local_sp_0;
        var_22 = (uint64_t)*var_9;
        var_23 = local_sp_0 + (-8L);
        *(uint64_t *)var_23 = 4203028UL;
        var_24 = indirect_placeholder_1(var_21, var_22);
        local_sp_1 = var_23;
        if (var_21 != 0UL & (uint64_t)(uint32_t)var_24 == 0UL) {
            var_25 = *(uint64_t *)6412472UL;
            *(uint64_t *)(local_sp_0 + (-16L)) = 4203047UL;
            var_26 = indirect_placeholder_116(var_25);
            var_27 = var_26.field_0;
            var_28 = var_26.field_1;
            *(uint64_t *)(local_sp_0 + (-24L)) = 4203055UL;
            indirect_placeholder();
            var_29 = (uint64_t)*(uint32_t *)var_27;
            var_30 = local_sp_0 + (-32L);
            *(uint64_t *)var_30 = 4203087UL;
            indirect_placeholder_115(0UL, 4296232UL, 4295704UL, var_29, 0UL, var_28, var_27);
            *var_5 = (unsigned char)'\x00';
            local_sp_1 = var_30;
        }
        var_31 = *(uint64_t *)6412464UL;
        local_sp_2 = local_sp_1;
        var_32 = (uint64_t)*var_9;
        var_33 = local_sp_1 + (-8L);
        *(uint64_t *)var_33 = 4203123UL;
        var_34 = indirect_placeholder_1(var_31, var_32);
        local_sp_2 = var_33;
        if (var_31 != 0UL & (uint64_t)(uint32_t)var_34 == 0UL) {
            var_35 = *(uint64_t *)6412464UL;
            *(uint64_t *)(local_sp_1 + (-16L)) = 4203142UL;
            var_36 = indirect_placeholder_114(var_35);
            var_37 = var_36.field_0;
            var_38 = var_36.field_1;
            *(uint64_t *)(local_sp_1 + (-24L)) = 4203150UL;
            indirect_placeholder();
            var_39 = (uint64_t)*(uint32_t *)var_37;
            var_40 = local_sp_1 + (-32L);
            *(uint64_t *)var_40 = 4203182UL;
            indirect_placeholder_113(0UL, 4296232UL, 4295694UL, var_39, 0UL, var_38, var_37);
            *var_5 = (unsigned char)'\x00';
            local_sp_2 = var_40;
        }
        var_41 = *(uint64_t *)6412480UL;
        local_sp_3 = local_sp_2;
        var_42 = (uint64_t)*var_9;
        var_43 = local_sp_2 + (-8L);
        *(uint64_t *)var_43 = 4203218UL;
        var_44 = indirect_placeholder_1(var_41, var_42);
        local_sp_3 = var_43;
        if (var_41 != 0UL & (uint64_t)(uint32_t)var_44 == 0UL) {
            var_45 = *(uint64_t *)6412480UL;
            *(uint64_t *)(local_sp_2 + (-16L)) = 4203237UL;
            var_46 = indirect_placeholder_112(var_45);
            var_47 = var_46.field_0;
            var_48 = var_46.field_1;
            *(uint64_t *)(local_sp_2 + (-24L)) = 4203245UL;
            indirect_placeholder();
            var_49 = (uint64_t)*(uint32_t *)var_47;
            var_50 = local_sp_2 + (-32L);
            *(uint64_t *)var_50 = 4203277UL;
            indirect_placeholder_111(0UL, 4296232UL, 4295699UL, var_49, 0UL, var_48, var_47);
            *var_5 = (unsigned char)'\x00';
            local_sp_3 = var_50;
        }
        var_51 = *var_5 ^ '\x01';
        if (var_51 == '\x00') {
            **(uint32_t **)var_4 = *var_9;
        } else {
            var_52 = (uint64_t)var_51;
            *(uint64_t *)(local_sp_3 + (-8L)) = 4203297UL;
            indirect_placeholder();
            var_53 = *(uint32_t *)var_52;
            var_54 = (uint32_t *)(var_0 + (-36L));
            *var_54 = var_53;
            var_55 = (uint64_t)*var_9;
            *(uint64_t *)(local_sp_3 + (-16L)) = 4203312UL;
            indirect_placeholder_14(var_55);
            *(uint64_t *)(local_sp_3 + (-24L)) = 4203317UL;
            indirect_placeholder();
            *(uint32_t *)var_55 = *var_54;
        }
    }
    return rax_0;
}

typedef unsigned char __u_char;
typedef unsigned short __u_short;
typedef unsigned int __u_int;
typedef unsigned long __u_long;
typedef signed char __int8_t;
typedef unsigned char __uint8_t;
typedef short __int16_t;
typedef unsigned short __uint16_t;
typedef int __int32_t;
typedef unsigned int __uint32_t;
typedef long __int64_t;
typedef unsigned long __uint64_t;
typedef __int8_t __int_least8_t;
typedef __uint8_t __uint_least8_t;
typedef __int16_t __int_least16_t;
typedef __uint16_t __uint_least16_t;
typedef __int32_t __int_least32_t;
typedef __uint32_t __uint_least32_t;
typedef __int64_t __int_least64_t;
typedef __uint64_t __uint_least64_t;
typedef long __quad_t;
typedef unsigned long __u_quad_t;
typedef long __intmax_t;
typedef unsigned long __uintmax_t;
typedef unsigned long __dev_t;
typedef unsigned int __uid_t;
typedef unsigned int __gid_t;
typedef unsigned long __ino_t;
typedef unsigned long __ino64_t;
typedef unsigned int __mode_t;
typedef unsigned long __nlink_t;
typedef long __off_t;
typedef long __off64_t;
typedef int __pid_t;
typedef struct {
    int __val[2];
} __fsid_t;
typedef long __clock_t;
typedef unsigned long __rlim_t;
typedef unsigned long __rlim64_t;
typedef unsigned int __id_t;
typedef long __time_t;
typedef unsigned int __useconds_t;
typedef long __suseconds_t;
typedef int __daddr_t;
typedef int __key_t;
typedef int __clockid_t;
typedef void *__timer_t;
typedef long __blksize_t;
typedef long __blkcnt_t;
typedef long __blkcnt64_t;
typedef unsigned long __fsblkcnt_t;
typedef unsigned long __fsblkcnt64_t;
typedef unsigned long __fsfilcnt_t;
typedef unsigned long __fsfilcnt64_t;
typedef long __fsword_t;
typedef long __ssize_t;
typedef long __syscall_slong_t;
typedef unsigned long __syscall_ulong_t;
typedef __off64_t __loff_t;
typedef char *__caddr_t;
typedef long __intptr_t;
typedef unsigned int __socklen_t;
typedef int __sig_atomic_t;
typedef __int8_t int8_t;
typedef __int16_t int16_t;
typedef __int32_t int32_t;
typedef __int64_t int64_t;
typedef __uint8_t uint8_t;
typedef __uint16_t uint16_t;
typedef __uint32_t uint32_t;
typedef __uint64_t uint64_t;
typedef __int_least8_t int_least8_t;
typedef __int_least16_t int_least16_t;
typedef __int_least32_t int_least32_t;
typedef __int_least64_t int_least64_t;
typedef __uint_least8_t uint_least8_t;
typedef __uint_least16_t uint_least16_t;
typedef __uint_least32_t uint_least32_t;
typedef __uint_least64_t uint_least64_t;
typedef signed char int_fast8_t;
typedef long int_fast16_t;
typedef long int_fast32_t;
typedef long int_fast64_t;
typedef unsigned char uint_fast8_t;
typedef unsigned long uint_fast16_t;
typedef unsigned long uint_fast32_t;
typedef unsigned long uint_fast64_t;
typedef long intptr_t;
typedef unsigned long uintptr_t;
typedef __intmax_t intmax_t;
typedef __uintmax_t uintmax_t;
typedef _Bool bool;
struct bb_setup_dir_ret_type;
struct indirect_placeholder_125_ret_type;
struct bb_setup_dir_ret_type {
    uint64_t field_0;
    uint64_t field_1;
    uint64_t field_2;
    uint64_t field_3;
    uint64_t field_4;
};
struct indirect_placeholder_125_ret_type {
    uint64_t field_0;
    uint64_t field_1;
};
extern uint64_t revng_init_local_sp(uint64_t param_0);
extern uint64_t init_rbp(void);
extern uint64_t indirect_placeholder_35(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint64_t param_3, uint64_t param_4);
extern uint64_t init_rbx(void);
extern uint64_t init_rcx(void);
extern uint64_t init_r9(void);
extern uint64_t init_r8(void);
extern void indirect_placeholder_14(uint64_t param_0);
extern struct indirect_placeholder_125_ret_type indirect_placeholder_125(uint64_t param_0);
struct bb_setup_dir_ret_type bb_setup_dir(uint64_t rdi) {
    uint64_t var_9;
    uint64_t var_0;
    uint64_t var_1;
    uint64_t var_2;
    uint64_t var_3;
    uint64_t var_4;
    uint64_t var_5;
    uint64_t *var_6;
    bool var_7;
    uint64_t *var_8;
    uint64_t rcx_0;
    uint64_t r8_0;
    uint64_t rcx_1;
    uint64_t rax_0;
    uint64_t r8_1;
    struct bb_setup_dir_ret_type mrv;
    struct bb_setup_dir_ret_type mrv1;
    struct bb_setup_dir_ret_type mrv2;
    struct bb_setup_dir_ret_type mrv3;
    struct bb_setup_dir_ret_type mrv4;
    struct indirect_placeholder_125_ret_type var_10;
    uint64_t var_11;
    var_0 = revng_init_local_sp(0UL);
    var_1 = init_rbp();
    var_2 = init_rcx();
    var_3 = init_rbx();
    var_4 = init_r9();
    var_5 = init_r8();
    *(uint64_t *)(var_0 + (-8L)) = var_1;
    var_6 = (uint64_t *)(var_0 + (-16L));
    *var_6 = rdi;
    var_7 = ((uint32_t)((uint16_t)*(uint32_t *)(rdi + 72UL) & (unsigned short)258U) == 0U);
    var_8 = (uint64_t *)(var_0 + (-32L));
    rcx_0 = var_2;
    r8_0 = var_5;
    rcx_1 = var_2;
    rax_0 = 0UL;
    r8_1 = var_5;
    if (var_7) {
        *var_8 = 4216209UL;
        var_10 = indirect_placeholder_125(32UL);
        *(uint64_t *)(*var_6 + 88UL) = var_10.field_0;
        var_11 = *(uint64_t *)(*var_6 + 88UL);
        if (var_11 != 0UL) {
            *(uint64_t *)(var_0 + (-40L)) = 4216256UL;
            indirect_placeholder_14(var_11);
            rcx_1 = rcx_0;
            rax_0 = 1UL;
            r8_1 = r8_0;
        }
    } else {
        *var_8 = 4216166UL;
        var_9 = indirect_placeholder_35(4216065UL, 4215980UL, 0UL, 31UL, 4201936UL);
        *(uint64_t *)(*var_6 + 88UL) = var_9;
        rcx_0 = 4215980UL;
        r8_0 = 4201936UL;
        rcx_1 = 4215980UL;
        r8_1 = 4201936UL;
        if (*(uint64_t *)(*var_6 + 88UL) == 0UL) {
            rcx_1 = rcx_0;
            rax_0 = 1UL;
            r8_1 = r8_0;
        }
    }
    mrv.field_0 = rax_0;
    mrv1 = mrv;
    mrv1.field_1 = rcx_1;
    mrv2 = mrv1;
    mrv2.field_2 = var_3;
    mrv3 = mrv2;
    mrv3.field_3 = var_4;
    mrv4 = mrv3;
    mrv4.field_4 = r8_1;
    return mrv4;
}

typedef unsigned char __u_char;
typedef unsigned short __u_short;
typedef unsigned int __u_int;
typedef unsigned long __u_long;
typedef signed char __int8_t;
typedef unsigned char __uint8_t;
typedef short __int16_t;
typedef unsigned short __uint16_t;
typedef int __int32_t;
typedef unsigned int __uint32_t;
typedef long __int64_t;
typedef unsigned long __uint64_t;
typedef __int8_t __int_least8_t;
typedef __uint8_t __uint_least8_t;
typedef __int16_t __int_least16_t;
typedef __uint16_t __uint_least16_t;
typedef __int32_t __int_least32_t;
typedef __uint32_t __uint_least32_t;
typedef __int64_t __int_least64_t;
typedef __uint64_t __uint_least64_t;
typedef long __quad_t;
typedef unsigned long __u_quad_t;
typedef long __intmax_t;
typedef unsigned long __uintmax_t;
typedef unsigned long __dev_t;
typedef unsigned int __uid_t;
typedef unsigned int __gid_t;
typedef unsigned long __ino_t;
typedef unsigned long __ino64_t;
typedef unsigned int __mode_t;
typedef unsigned long __nlink_t;
typedef long __off_t;
typedef long __off64_t;
typedef int __pid_t;
typedef struct {
    int __val[2];
} __fsid_t;
typedef long __clock_t;
typedef unsigned long __rlim_t;
typedef unsigned long __rlim64_t;
typedef unsigned int __id_t;
typedef long __time_t;
typedef unsigned int __useconds_t;
typedef long __suseconds_t;
typedef int __daddr_t;
typedef int __key_t;
typedef int __clockid_t;
typedef void *__timer_t;
typedef long __blksize_t;
typedef long __blkcnt_t;
typedef long __blkcnt64_t;
typedef unsigned long __fsblkcnt_t;
typedef unsigned long __fsblkcnt64_t;
typedef unsigned long __fsfilcnt_t;
typedef unsigned long __fsfilcnt64_t;
typedef long __fsword_t;
typedef long __ssize_t;
typedef long __syscall_slong_t;
typedef unsigned long __syscall_ulong_t;
typedef __off64_t __loff_t;
typedef char *__caddr_t;
typedef long __intptr_t;
typedef unsigned int __socklen_t;
typedef int __sig_atomic_t;
typedef __int8_t int8_t;
typedef __int16_t int16_t;
typedef __int32_t int32_t;
typedef __int64_t int64_t;
typedef __uint8_t uint8_t;
typedef __uint16_t uint16_t;
typedef __uint32_t uint32_t;
typedef __uint64_t uint64_t;
typedef __int_least8_t int_least8_t;
typedef __int_least16_t int_least16_t;
typedef __int_least32_t int_least32_t;
typedef __int_least64_t int_least64_t;
typedef __uint_least8_t uint_least8_t;
typedef __uint_least16_t uint_least16_t;
typedef __uint_least32_t uint_least32_t;
typedef __uint_least64_t uint_least64_t;
typedef signed char int_fast8_t;
typedef long int_fast16_t;
typedef long int_fast32_t;
typedef long int_fast64_t;
typedef unsigned char uint_fast8_t;
typedef unsigned long uint_fast16_t;
typedef unsigned long uint_fast32_t;
typedef unsigned long uint_fast64_t;
typedef long intptr_t;
typedef unsigned long uintptr_t;
typedef __intmax_t intmax_t;
typedef __uintmax_t uintmax_t;
typedef _Bool bool;
struct indirect_placeholder_41_ret_type;
struct indirect_placeholder_40_ret_type;
struct indirect_placeholder_43_ret_type;
struct indirect_placeholder_42_ret_type;
struct indirect_placeholder_45_ret_type;
struct indirect_placeholder_44_ret_type;
struct indirect_placeholder_47_ret_type;
struct indirect_placeholder_48_ret_type;
struct indirect_placeholder_49_ret_type;
struct indirect_placeholder_50_ret_type;
struct indirect_placeholder_46_ret_type;
struct indirect_placeholder_51_ret_type;
struct indirect_placeholder_52_ret_type;
struct indirect_placeholder_54_ret_type;
struct indirect_placeholder_41_ret_type {
    uint64_t field_0;
    uint64_t field_1;
    uint64_t field_2;
};
struct indirect_placeholder_40_ret_type {
    uint64_t field_0;
    uint64_t field_1;
    uint64_t field_2;
};
struct indirect_placeholder_43_ret_type {
    uint64_t field_0;
    uint64_t field_1;
    uint64_t field_2;
};
struct indirect_placeholder_42_ret_type {
    uint64_t field_0;
    uint64_t field_1;
    uint64_t field_2;
};
struct indirect_placeholder_45_ret_type {
    uint64_t field_0;
    uint64_t field_1;
    uint64_t field_2;
};
struct indirect_placeholder_44_ret_type {
    uint64_t field_0;
    uint64_t field_1;
    uint64_t field_2;
};
struct indirect_placeholder_47_ret_type {
    uint64_t field_0;
    uint64_t field_1;
    uint64_t field_2;
};
struct indirect_placeholder_48_ret_type {
    uint64_t field_0;
    uint64_t field_1;
    uint64_t field_2;
};
struct indirect_placeholder_49_ret_type {
    uint64_t field_0;
    uint64_t field_1;
    uint64_t field_2;
};
struct indirect_placeholder_50_ret_type {
    uint64_t field_0;
    uint64_t field_1;
    uint64_t field_2;
};
struct indirect_placeholder_46_ret_type {
    uint64_t field_0;
    uint64_t field_1;
    uint64_t field_2;
};
struct indirect_placeholder_51_ret_type {
    uint64_t field_0;
    uint64_t field_1;
    uint64_t field_2;
};
struct indirect_placeholder_52_ret_type {
    uint64_t field_0;
    uint64_t field_1;
};
struct indirect_placeholder_54_ret_type {
    uint64_t field_0;
    uint64_t field_1;
    uint64_t field_2;
    uint64_t field_3;
};
extern void abort(void);
extern uint64_t revng_init_local_sp(uint64_t param_0);
extern uint64_t init_rbp(void);
extern uint64_t indirect_placeholder_1(uint64_t param_0, uint64_t param_1);
extern void indirect_placeholder(void);
extern void indirect_placeholder_30(uint64_t param_0, uint64_t param_1);
extern uint64_t init_rbx(void);
extern void indirect_placeholder_14(uint64_t param_0);
extern uint64_t indirect_placeholder_10(uint64_t param_0);
extern struct indirect_placeholder_41_ret_type indirect_placeholder_41(uint64_t param_0, uint64_t param_1);
extern struct indirect_placeholder_40_ret_type indirect_placeholder_40(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint64_t param_3, uint64_t param_4, uint64_t param_5, uint64_t param_6);
extern struct indirect_placeholder_43_ret_type indirect_placeholder_43(uint64_t param_0);
extern struct indirect_placeholder_42_ret_type indirect_placeholder_42(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint64_t param_3, uint64_t param_4, uint64_t param_5, uint64_t param_6);
extern struct indirect_placeholder_45_ret_type indirect_placeholder_45(uint64_t param_0, uint64_t param_1);
extern struct indirect_placeholder_44_ret_type indirect_placeholder_44(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint64_t param_3, uint64_t param_4, uint64_t param_5, uint64_t param_6);
extern struct indirect_placeholder_47_ret_type indirect_placeholder_47(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint64_t param_3, uint64_t param_4, uint64_t param_5, uint64_t param_6);
extern struct indirect_placeholder_48_ret_type indirect_placeholder_48(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint64_t param_3, uint64_t param_4, uint64_t param_5, uint64_t param_6);
extern struct indirect_placeholder_49_ret_type indirect_placeholder_49(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint64_t param_3, uint64_t param_4, uint64_t param_5, uint64_t param_6);
extern struct indirect_placeholder_50_ret_type indirect_placeholder_50(uint64_t param_0);
extern struct indirect_placeholder_46_ret_type indirect_placeholder_46(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint64_t param_3, uint64_t param_4, uint64_t param_5, uint64_t param_6);
extern struct indirect_placeholder_51_ret_type indirect_placeholder_51(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint64_t param_3, uint64_t param_4, uint64_t param_5, uint64_t param_6);
extern struct indirect_placeholder_52_ret_type indirect_placeholder_52(uint64_t param_0);
extern void indirect_placeholder_53(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint64_t param_3, uint64_t param_4, uint64_t param_5);
extern struct indirect_placeholder_54_ret_type indirect_placeholder_54(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint64_t param_3, uint64_t param_4);
void bb_main(uint64_t rsi, uint64_t rdi) {
    struct indirect_placeholder_54_ret_type var_18;
    uint64_t r8_3;
    uint64_t var_0;
    uint64_t var_1;
    uint64_t var_2;
    uint64_t var_3;
    uint32_t *var_4;
    uint64_t var_5;
    uint64_t *var_6;
    uint32_t *var_7;
    uint32_t *var_8;
    unsigned char *var_9;
    unsigned char *var_10;
    uint64_t *var_11;
    uint64_t var_12;
    uint64_t var_13;
    uint32_t *var_14;
    uint64_t local_sp_9;
    uint64_t rcx_4;
    struct indirect_placeholder_41_ret_type var_65;
    uint64_t var_66;
    uint64_t var_67;
    uint64_t var_68;
    uint64_t var_69;
    uint64_t var_70;
    uint64_t local_sp_7;
    uint64_t rcx_3;
    uint64_t var_54;
    struct indirect_placeholder_43_ret_type var_55;
    uint64_t var_56;
    uint64_t var_57;
    uint64_t var_58;
    uint64_t var_59;
    uint64_t var_60;
    struct indirect_placeholder_42_ret_type var_61;
    uint64_t local_sp_6;
    uint64_t var_42;
    struct indirect_placeholder_45_ret_type var_43;
    uint64_t var_44;
    uint64_t var_45;
    uint64_t var_46;
    uint64_t var_47;
    uint64_t var_48;
    struct indirect_placeholder_44_ret_type var_49;
    uint64_t local_sp_1;
    uint64_t r9_3;
    uint64_t rcx_0;
    uint64_t local_sp_3;
    uint64_t r9_0;
    uint64_t r8_0;
    uint64_t local_sp_2;
    uint64_t var_23;
    uint64_t var_24;
    uint64_t var_25;
    bool var_26;
    uint32_t var_27;
    uint64_t var_30;
    struct indirect_placeholder_47_ret_type var_31;
    uint64_t rcx_1;
    uint64_t r9_1;
    uint64_t r8_1;
    uint64_t local_sp_5;
    uint64_t local_sp_4;
    uint64_t var_28;
    struct indirect_placeholder_48_ret_type var_29;
    uint64_t rcx_2;
    uint64_t r9_2;
    uint64_t r8_2;
    uint32_t var_32;
    uint64_t var_33;
    uint32_t var_34;
    uint64_t var_35;
    bool var_36;
    uint64_t storemerge;
    uint64_t var_74;
    uint64_t var_75;
    struct indirect_placeholder_50_ret_type var_76;
    uint64_t var_77;
    uint64_t var_78;
    uint64_t var_79;
    uint64_t var_80;
    uint64_t var_37;
    uint64_t *var_38;
    uint64_t var_39;
    uint64_t var_40;
    uint64_t var_41;
    uint64_t r9_4;
    uint64_t r8_4;
    uint64_t var_62;
    struct indirect_placeholder_52_ret_type var_63;
    uint64_t var_64;
    uint64_t var_71;
    uint64_t var_72;
    uint64_t var_73;
    uint64_t var_50;
    uint64_t var_51;
    uint64_t var_52;
    uint64_t var_53;
    uint64_t local_sp_8;
    uint64_t var_20;
    uint64_t var_21;
    uint64_t *var_22;
    uint64_t var_15;
    uint64_t var_16;
    uint64_t var_17;
    uint32_t var_19;
    unsigned int loop_state_var;
    var_0 = revng_init_local_sp(0UL);
    var_1 = init_rbp();
    var_2 = init_rbx();
    var_3 = var_0 + (-8L);
    *(uint64_t *)var_3 = var_1;
    *(uint64_t *)(var_0 + (-16L)) = var_2;
    var_4 = (uint32_t *)(var_0 + (-76L));
    *var_4 = (uint32_t)rdi;
    var_5 = var_0 + (-88L);
    var_6 = (uint64_t *)var_5;
    *var_6 = rsi;
    var_7 = (uint32_t *)(var_0 + (-28L));
    *var_7 = 16U;
    var_8 = (uint32_t *)(var_0 + (-32L));
    *var_8 = 4294967295U;
    var_9 = (unsigned char *)(var_0 + (-33L));
    *var_9 = (unsigned char)'\x00';
    var_10 = (unsigned char *)(var_0 + (-34L));
    *var_10 = (unsigned char)'\x00';
    var_11 = (uint64_t *)(var_0 + (-48L));
    *var_11 = 0UL;
    var_12 = **(uint64_t **)var_5;
    *(uint64_t *)(var_0 + (-96L)) = 4205692UL;
    indirect_placeholder_14(var_12);
    *(uint64_t *)(var_0 + (-104L)) = 4205707UL;
    indirect_placeholder();
    var_13 = var_0 + (-112L);
    *(uint64_t *)var_13 = 4205717UL;
    indirect_placeholder();
    var_14 = (uint32_t *)(var_0 + (-52L));
    storemerge = 4294967296UL;
    local_sp_9 = var_13;
    while (1U)
        {
            var_15 = *var_6;
            var_16 = (uint64_t)*var_4;
            var_17 = local_sp_9 + (-8L);
            *(uint64_t *)var_17 = 4206257UL;
            var_18 = indirect_placeholder_54(4298855UL, 4295744UL, var_15, var_16, 0UL);
            var_19 = (uint32_t)var_18.field_0;
            *var_14 = var_19;
            local_sp_9 = var_17;
            local_sp_3 = var_17;
            local_sp_5 = var_17;
            local_sp_4 = var_17;
            local_sp_8 = var_17;
            if (var_19 != 4294967295U) {
                var_23 = var_18.field_1;
                var_24 = var_18.field_2;
                var_25 = var_18.field_3;
                r8_3 = var_25;
                rcx_3 = var_23;
                r9_3 = var_24;
                rcx_1 = var_23;
                r9_1 = var_24;
                r8_1 = var_25;
                rcx_2 = var_23;
                r9_2 = var_24;
                r8_2 = var_25;
                if (*(unsigned char *)6412433UL == '\x00') {
                    *var_7 = 16U;
                    *(unsigned char *)6412432UL = (*var_8 != 0U);
                    loop_state_var = 1U;
                    break;
                }
                var_26 = (*var_7 == 16U);
                var_27 = *var_8;
                if (!var_26) {
                    if (var_27 != 0U) {
                        loop_state_var = 2U;
                        break;
                    }
                    var_28 = local_sp_9 + (-16L);
                    *(uint64_t *)var_28 = 4206358UL;
                    var_29 = indirect_placeholder_48(0UL, 4298914UL, var_23, 0UL, 1UL, var_24, var_25);
                    local_sp_4 = var_28;
                    rcx_2 = var_29.field_0;
                    r9_2 = var_29.field_1;
                    r8_2 = var_29.field_2;
                    loop_state_var = 2U;
                    break;
                }
                if (var_27 != 1U) {
                    loop_state_var = 0U;
                    break;
                }
                var_30 = local_sp_9 + (-16L);
                *(uint64_t *)var_30 = 4206318UL;
                var_31 = indirect_placeholder_47(0UL, 4298872UL, var_23, 0UL, 1UL, var_24, var_25);
                local_sp_3 = var_30;
                rcx_1 = var_31.field_0;
                r9_1 = var_31.field_1;
                r8_1 = var_31.field_2;
                loop_state_var = 0U;
                break;
            }
            if ((uint64_t)(var_19 + (-108)) == 0UL) {
                *(uint64_t *)6412472UL = *(uint64_t *)6412968UL;
                *var_10 = (unsigned char)'\x01';
                continue;
            }
            if ((int)var_19 > (int)108U) {
                if ((uint64_t)(var_19 + (-118)) == 0UL) {
                    *(unsigned char *)6412434UL = (unsigned char)'\x01';
                    continue;
                }
                if ((int)var_19 <= (int)118U) {
                    if ((uint64_t)(var_19 + (-116)) == 0UL) {
                        *(uint64_t *)6412480UL = *(uint64_t *)6412968UL;
                        *var_10 = (unsigned char)'\x01';
                        continue;
                    }
                    if ((int)var_19 > (int)116U) {
                        *(uint64_t *)6412456UL = *(uint64_t *)6412968UL;
                        *var_10 = (unsigned char)'\x01';
                        continue;
                    }
                    if ((uint64_t)(var_19 + (-114)) != 0UL) {
                        loop_state_var = 3U;
                        break;
                    }
                    *(uint64_t *)6412464UL = *(uint64_t *)6412968UL;
                    *var_10 = (unsigned char)'\x01';
                    continue;
                }
                if ((uint64_t)(var_19 + (-129)) == 0UL) {
                    *var_9 = (unsigned char)'\x00';
                    continue;
                }
                if ((int)var_19 <= (int)129U) {
                    if ((uint64_t)(var_19 + (-128)) != 0UL) {
                        loop_state_var = 3U;
                        break;
                    }
                    *var_8 = 1U;
                    continue;
                }
                if ((uint64_t)(var_19 + (-130)) == 0UL) {
                    *var_9 = (unsigned char)'\x01';
                    continue;
                }
                if ((uint64_t)(var_19 + (-131)) != 0UL) {
                    loop_state_var = 3U;
                    break;
                }
                *var_11 = *(uint64_t *)6412968UL;
                continue;
            }
            if ((uint64_t)(var_19 + (-76)) == 0UL) {
                *var_7 = 2U;
                continue;
            }
            if ((int)var_19 > (int)76U) {
                if ((uint64_t)(var_19 + (-82)) == 0UL) {
                    *(unsigned char *)6412433UL = (unsigned char)'\x01';
                    continue;
                }
                if ((int)var_19 <= (int)82U) {
                    if ((uint64_t)(var_19 + (-80)) != 0UL) {
                        loop_state_var = 3U;
                        break;
                    }
                    *var_7 = 16U;
                    continue;
                }
                if ((uint64_t)(var_19 + (-102)) != 0UL) {
                    continue;
                }
                if ((uint64_t)(var_19 + (-104)) != 0UL) {
                    loop_state_var = 3U;
                    break;
                }
                *var_8 = 0U;
                continue;
            }
            if ((uint64_t)(var_19 + 130U) == 0UL) {
                *(uint64_t *)(local_sp_9 + (-16L)) = 4206145UL;
                indirect_placeholder_30(var_3, 0UL);
                abort();
            }
            if ((uint64_t)(var_19 + (-72)) == 0UL) {
                *var_7 = 17U;
                continue;
            }
            if (var_19 != 4294967165U) {
                loop_state_var = 3U;
                break;
            }
            var_20 = *(uint64_t *)6412096UL;
            var_21 = local_sp_9 + (-24L);
            var_22 = (uint64_t *)var_21;
            *var_22 = 0UL;
            *(uint64_t *)(local_sp_9 + (-32L)) = 4206203UL;
            indirect_placeholder_53(0UL, 4295392UL, var_20, 4298822UL, 4298828UL, 4298841UL);
            *var_22 = 4206217UL;
            indirect_placeholder();
            local_sp_8 = var_21;
            loop_state_var = 3U;
            break;
        }
    switch (loop_state_var) {
      case 3U:
        {
            *(uint64_t *)(local_sp_8 + (-8L)) = 4206227UL;
            indirect_placeholder_30(var_3, 1UL);
            abort();
        }
        break;
      case 2U:
      case 1U:
      case 0U:
        {
            switch (loop_state_var) {
              case 0U:
                {
                    *(unsigned char *)6412432UL = (unsigned char)'\x00';
                    local_sp_5 = local_sp_3;
                    rcx_3 = rcx_1;
                    r9_3 = r9_1;
                    r8_3 = r8_1;
                }
                break;
              case 2U:
                {
                    *(unsigned char *)6412432UL = (unsigned char)'\x01';
                    local_sp_5 = local_sp_4;
                    rcx_3 = rcx_2;
                    r9_3 = r9_2;
                    r8_3 = r8_2;
                }
                break;
              case 1U:
                {
                    var_32 = *(uint32_t *)6412248UL;
                    var_33 = (uint64_t)var_32;
                    var_34 = *var_4;
                    var_35 = (uint64_t)var_34 - var_33;
                    var_36 = (*var_11 == 0UL);
                    rcx_4 = rcx_3;
                    local_sp_6 = local_sp_5;
                    rcx_0 = rcx_3;
                    r9_0 = r9_3;
                    r8_0 = r8_3;
                    r9_4 = r9_3;
                    r8_4 = r8_3;
                    if (var_36) {
                    }
                    if ((long)(var_35 << 32UL) < (long)storemerge) {
                        if ((int)var_32 < (int)var_34) {
                            var_75 = *(uint64_t *)(*var_6 + (((uint64_t)var_34 << 3UL) + (-8L)));
                            *(uint64_t *)(local_sp_5 + (-8L)) = 4206498UL;
                            var_76 = indirect_placeholder_50(var_75);
                            var_77 = var_76.field_0;
                            var_78 = var_76.field_1;
                            var_79 = var_76.field_2;
                            var_80 = local_sp_5 + (-16L);
                            *(uint64_t *)var_80 = 4206526UL;
                            indirect_placeholder_46(0UL, 4298948UL, var_77, 0UL, 0UL, var_78, var_79);
                            local_sp_2 = var_80;
                        } else {
                            var_74 = local_sp_5 + (-8L);
                            *(uint64_t *)var_74 = 4206465UL;
                            indirect_placeholder_49(0UL, 4298932UL, rcx_3, 0UL, 0UL, r9_3, r8_3);
                            local_sp_2 = var_74;
                        }
                        *(uint64_t *)(local_sp_2 + (-8L)) = 4206536UL;
                        indirect_placeholder_30(var_3, 1UL);
                        abort();
                    }
                    if (var_36) {
                        if (*var_10 == '\x00') {
                            *(uint64_t *)6412448UL = 0UL;
                        } else {
                            *(uint32_t *)6412248UL = (var_32 + 1U);
                            var_50 = *(uint64_t *)(*var_6 + ((uint64_t)var_32 << 3UL));
                            *(uint64_t *)6412448UL = var_50;
                            *(uint64_t *)(local_sp_5 + (-8L)) = 4206718UL;
                            var_51 = indirect_placeholder_10(var_50);
                            var_52 = local_sp_5 + (-16L);
                            *(uint64_t *)var_52 = 4206726UL;
                            var_53 = indirect_placeholder_10(var_51);
                            local_sp_6 = var_52;
                            if ((int)(uint32_t)var_53 > (int)4294967295U) {
                                var_54 = *(uint64_t *)6412448UL;
                                *(uint64_t *)(local_sp_5 + (-24L)) = 4206745UL;
                                var_55 = indirect_placeholder_43(var_54);
                                var_56 = var_55.field_0;
                                var_57 = var_55.field_1;
                                var_58 = var_55.field_2;
                                *(uint64_t *)(local_sp_5 + (-32L)) = 4206753UL;
                                indirect_placeholder();
                                var_59 = (uint64_t)*(uint32_t *)var_56;
                                var_60 = local_sp_5 + (-40L);
                                *(uint64_t *)var_60 = 4206780UL;
                                var_61 = indirect_placeholder_42(0UL, 4298973UL, var_56, var_59, 1UL, var_57, var_58);
                                local_sp_6 = var_60;
                                rcx_4 = var_61.field_0;
                                r9_4 = var_61.field_1;
                                r8_4 = var_61.field_2;
                            }
                        }
                    } else {
                        var_37 = var_0 + (-64L);
                        var_38 = (uint64_t *)var_37;
                        *var_38 = 0UL;
                        var_39 = *var_11;
                        var_40 = local_sp_5 + (-8L);
                        *(uint64_t *)var_40 = 4206570UL;
                        var_41 = indirect_placeholder_1(var_37, var_39);
                        local_sp_1 = var_40;
                        if ((int)(uint32_t)var_41 <= (int)4294967295U) {
                            var_42 = *var_11;
                            *(uint64_t *)(local_sp_5 + (-16L)) = 4206591UL;
                            var_43 = indirect_placeholder_45(var_42, 4UL);
                            var_44 = var_43.field_0;
                            var_45 = var_43.field_1;
                            var_46 = var_43.field_2;
                            *(uint64_t *)(local_sp_5 + (-24L)) = 4206599UL;
                            indirect_placeholder();
                            var_47 = (uint64_t)*(uint32_t *)var_44;
                            var_48 = local_sp_5 + (-32L);
                            *(uint64_t *)var_48 = 4206626UL;
                            var_49 = indirect_placeholder_44(0UL, 4296288UL, var_44, var_47, 1UL, var_45, var_46);
                            local_sp_1 = var_48;
                            rcx_0 = var_49.field_0;
                            r9_0 = var_49.field_1;
                            r8_0 = var_49.field_2;
                        }
                        *(uint64_t *)6412448UL = *var_38;
                        local_sp_6 = local_sp_1;
                        rcx_4 = rcx_0;
                        r9_4 = r9_0;
                        r8_4 = r8_0;
                    }
                    local_sp_7 = local_sp_6;
                    if (*var_11 != 0UL) {
                        if (*var_10 == '\x00') {
                            *(uint64_t *)(local_sp_6 + (-8L)) = 4206818UL;
                            indirect_placeholder_51(0UL, 4299000UL, rcx_4, 0UL, 0UL, r9_4, r8_4);
                            *(uint64_t *)(local_sp_6 + (-16L)) = 4206828UL;
                            indirect_placeholder_30(var_3, 1UL);
                            abort();
                        }
                    }
                    if (*(unsigned char *)6412433UL == '\x00') {
                        *(uint64_t *)6412440UL = 0UL;
                    } else {
                        if (*var_9 == '\x00') {
                            *(uint64_t *)6412440UL = 0UL;
                        } else {
                            var_62 = local_sp_6 + (-8L);
                            *(uint64_t *)var_62 = 4206855UL;
                            var_63 = indirect_placeholder_52(6412496UL);
                            var_64 = var_63.field_0;
                            *(uint64_t *)6412440UL = var_64;
                            local_sp_7 = var_62;
                            if (var_64 == 0UL) {
                                *(uint64_t *)(local_sp_6 + (-16L)) = 4206889UL;
                                var_65 = indirect_placeholder_41(4296421UL, 4UL);
                                var_66 = var_65.field_0;
                                var_67 = var_65.field_1;
                                var_68 = var_65.field_2;
                                *(uint64_t *)(local_sp_6 + (-24L)) = 4206897UL;
                                indirect_placeholder();
                                var_69 = (uint64_t)*(uint32_t *)var_66;
                                var_70 = local_sp_6 + (-32L);
                                *(uint64_t *)var_70 = 4206924UL;
                                indirect_placeholder_40(0UL, 4299048UL, var_66, var_69, 1UL, var_67, var_68);
                                local_sp_7 = var_70;
                            }
                        }
                    }
                    var_71 = (uint64_t)(*var_7 | 8U);
                    var_72 = *var_6 + ((uint64_t)*(uint32_t *)6412248UL << 3UL);
                    *(uint64_t *)(local_sp_7 + (-8L)) = 4206978UL;
                    var_73 = indirect_placeholder_1(var_71, var_72);
                    *(unsigned char *)(var_0 + (-53L)) = (unsigned char)var_73;
                    return;
                }
                break;
            }
        }
        break;
    }
}

typedef unsigned char __u_char;
typedef unsigned short __u_short;
typedef unsigned int __u_int;
typedef unsigned long __u_long;
typedef signed char __int8_t;
typedef unsigned char __uint8_t;
typedef short __int16_t;
typedef unsigned short __uint16_t;
typedef int __int32_t;
typedef unsigned int __uint32_t;
typedef long __int64_t;
typedef unsigned long __uint64_t;
typedef __int8_t __int_least8_t;
typedef __uint8_t __uint_least8_t;
typedef __int16_t __int_least16_t;
typedef __uint16_t __uint_least16_t;
typedef __int32_t __int_least32_t;
typedef __uint32_t __uint_least32_t;
typedef __int64_t __int_least64_t;
typedef __uint64_t __uint_least64_t;
typedef long __quad_t;
typedef unsigned long __u_quad_t;
typedef long __intmax_t;
typedef unsigned long __uintmax_t;
typedef unsigned long __dev_t;
typedef unsigned int __uid_t;
typedef unsigned int __gid_t;
typedef unsigned long __ino_t;
typedef unsigned long __ino64_t;
typedef unsigned int __mode_t;
typedef unsigned long __nlink_t;
typedef long __off_t;
typedef long __off64_t;
typedef int __pid_t;
typedef struct {
    int __val[2];
} __fsid_t;
typedef long __clock_t;
typedef unsigned long __rlim_t;
typedef unsigned long __rlim64_t;
typedef unsigned int __id_t;
typedef long __time_t;
typedef unsigned int __useconds_t;
typedef long __suseconds_t;
typedef int __daddr_t;
typedef int __key_t;
typedef int __clockid_t;
typedef void *__timer_t;
typedef long __blksize_t;
typedef long __blkcnt_t;
typedef long __blkcnt64_t;
typedef unsigned long __fsblkcnt_t;
typedef unsigned long __fsblkcnt64_t;
typedef unsigned long __fsfilcnt_t;
typedef unsigned long __fsfilcnt64_t;
typedef long __fsword_t;
typedef long __ssize_t;
typedef long __syscall_slong_t;
typedef unsigned long __syscall_ulong_t;
typedef __off64_t __loff_t;
typedef char *__caddr_t;
typedef long __intptr_t;
typedef unsigned int __socklen_t;
typedef int __sig_atomic_t;
typedef __int8_t int8_t;
typedef __int16_t int16_t;
typedef __int32_t int32_t;
typedef __int64_t int64_t;
typedef __uint8_t uint8_t;
typedef __uint16_t uint16_t;
typedef __uint32_t uint32_t;
typedef __uint64_t uint64_t;
typedef __int_least8_t int_least8_t;
typedef __int_least16_t int_least16_t;
typedef __int_least32_t int_least32_t;
typedef __int_least64_t int_least64_t;
typedef __uint_least8_t uint_least8_t;
typedef __uint_least16_t uint_least16_t;
typedef __uint_least32_t uint_least32_t;
typedef __uint_least64_t uint_least64_t;
typedef signed char int_fast8_t;
typedef long int_fast16_t;
typedef long int_fast32_t;
typedef long int_fast64_t;
typedef unsigned char uint_fast8_t;
typedef unsigned long uint_fast16_t;
typedef unsigned long uint_fast32_t;
typedef unsigned long uint_fast64_t;
typedef long intptr_t;
typedef unsigned long uintptr_t;
typedef __intmax_t intmax_t;
typedef __uintmax_t uintmax_t;
typedef _Bool bool;
struct helper_pxor_xmm_wrapper_ret_type;
struct type_5;
struct type_7;
struct helper_pxor_xmm_wrapper_160_ret_type;
struct helper_cvtsq2ss_wrapper_ret_type;
struct helper_mulss_wrapper_167_ret_type;
struct helper_addss_wrapper_ret_type;
struct helper_ucomiss_wrapper_168_ret_type;
struct helper_cvttss2sq_wrapper_ret_type;
struct helper_subss_wrapper_ret_type;
struct helper_addss_wrapper_166_ret_type;
struct helper_mulss_wrapper_ret_type;
struct helper_ucomiss_wrapper_ret_type;
struct helper_cvtsq2ss_wrapper_165_ret_type;
struct helper_pxor_xmm_wrapper_ret_type {
    uint64_t field_0;
    uint64_t field_1;
};
struct type_5 {
};
struct type_7 {
};
struct helper_pxor_xmm_wrapper_160_ret_type {
    uint64_t field_0;
    uint64_t field_1;
};
struct helper_cvtsq2ss_wrapper_ret_type {
    uint64_t field_0;
    unsigned char field_1;
};
struct helper_mulss_wrapper_167_ret_type {
    uint64_t field_0;
    unsigned char field_1;
};
struct helper_addss_wrapper_ret_type {
    uint64_t field_0;
    unsigned char field_1;
};
struct helper_ucomiss_wrapper_168_ret_type {
    uint64_t field_0;
    unsigned char field_1;
};
struct helper_cvttss2sq_wrapper_ret_type {
    uint64_t field_0;
    unsigned char field_1;
};
struct helper_subss_wrapper_ret_type {
    uint64_t field_0;
    unsigned char field_1;
};
struct helper_addss_wrapper_166_ret_type {
    uint64_t field_0;
    unsigned char field_1;
};
struct helper_mulss_wrapper_ret_type {
    uint64_t field_0;
    unsigned char field_1;
};
struct helper_ucomiss_wrapper_ret_type {
    uint64_t field_0;
    unsigned char field_1;
};
struct helper_cvtsq2ss_wrapper_165_ret_type {
    uint64_t field_0;
    unsigned char field_1;
};
extern uint64_t revng_init_local_sp(uint64_t param_0);
extern uint64_t init_rbp(void);
extern uint64_t init_cc_src2(void);
extern uint64_t helper_cc_compute_c_wrapper(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint32_t param_3);
extern uint64_t init_state_0x8558(void);
extern unsigned char init_state_0x8549(void);
extern unsigned char init_state_0x854c(void);
extern unsigned char init_state_0x8548(void);
extern unsigned char init_state_0x854b(void);
extern unsigned char init_state_0x8547(void);
extern unsigned char init_state_0x854d(void);
extern void indirect_placeholder(void);
extern struct helper_pxor_xmm_wrapper_ret_type helper_pxor_xmm_wrapper(struct type_5 *param_0, struct type_7 *param_1, struct type_7 *param_2, uint64_t param_3, uint64_t param_4);
extern uint64_t init_state_0x8598(void);
extern uint64_t init_state_0x85a0(void);
extern struct helper_pxor_xmm_wrapper_160_ret_type helper_pxor_xmm_wrapper_160(struct type_5 *param_0, struct type_7 *param_1, struct type_7 *param_2, uint64_t param_3, uint64_t param_4);
extern uint64_t init_state_0x8560(void);
extern uint64_t init_r9(void);
extern uint64_t init_r8(void);
extern struct helper_cvtsq2ss_wrapper_ret_type helper_cvtsq2ss_wrapper(struct type_5 *param_0, struct type_7 *param_1, uint64_t param_2, unsigned char param_3, unsigned char param_4, unsigned char param_5, unsigned char param_6);
extern struct helper_mulss_wrapper_167_ret_type helper_mulss_wrapper_167(struct type_5 *param_0, struct type_7 *param_1, struct type_7 *param_2, uint64_t param_3, uint64_t param_4, unsigned char param_5, unsigned char param_6, unsigned char param_7, unsigned char param_8, unsigned char param_9, unsigned char param_10);
extern uint64_t init_rbx(void);
extern uint64_t init_state_0x8d58(void);
extern struct helper_addss_wrapper_ret_type helper_addss_wrapper(struct type_5 *param_0, struct type_7 *param_1, struct type_7 *param_2, uint64_t param_3, unsigned char param_4, unsigned char param_5, unsigned char param_6, unsigned char param_7, unsigned char param_8, unsigned char param_9);
extern struct helper_ucomiss_wrapper_168_ret_type helper_ucomiss_wrapper_168(struct type_5 *param_0, struct type_7 *param_1, struct type_7 *param_2, uint64_t param_3, uint64_t param_4, unsigned char param_5, unsigned char param_6);
extern struct helper_cvttss2sq_wrapper_ret_type helper_cvttss2sq_wrapper(struct type_5 *param_0, struct type_7 *param_1, uint64_t param_2, unsigned char param_3, unsigned char param_4);
extern struct helper_subss_wrapper_ret_type helper_subss_wrapper(struct type_5 *param_0, struct type_7 *param_1, struct type_7 *param_2, uint64_t param_3, uint64_t param_4, unsigned char param_5, unsigned char param_6, unsigned char param_7, unsigned char param_8, unsigned char param_9, unsigned char param_10);
extern uint64_t init_r10(void);
extern struct helper_addss_wrapper_166_ret_type helper_addss_wrapper_166(struct type_5 *param_0, struct type_7 *param_1, struct type_7 *param_2, uint64_t param_3, unsigned char param_4, unsigned char param_5, unsigned char param_6, unsigned char param_7, unsigned char param_8, unsigned char param_9);
extern struct helper_mulss_wrapper_ret_type helper_mulss_wrapper(struct type_5 *param_0, struct type_7 *param_1, struct type_7 *param_2, uint64_t param_3, uint64_t param_4, unsigned char param_5, unsigned char param_6, unsigned char param_7, unsigned char param_8, unsigned char param_9, unsigned char param_10);
extern uint64_t indirect_placeholder_24(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint64_t param_3);
extern uint64_t indirect_placeholder_10(uint64_t param_0);
extern uint64_t indirect_placeholder_31(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint64_t param_3, uint64_t param_4, uint64_t param_5, uint64_t param_6);
extern struct helper_ucomiss_wrapper_ret_type helper_ucomiss_wrapper(struct type_5 *param_0, struct type_7 *param_1, struct type_7 *param_2, uint64_t param_3, uint64_t param_4, unsigned char param_5, unsigned char param_6);
extern struct helper_cvtsq2ss_wrapper_165_ret_type helper_cvtsq2ss_wrapper_165(struct type_5 *param_0, struct type_7 *param_1, uint64_t param_2, unsigned char param_3, unsigned char param_4, unsigned char param_5, unsigned char param_6);
uint64_t bb_hash_delete(uint64_t rsi, uint64_t rdi) {
    struct helper_cvttss2sq_wrapper_ret_type var_88;
    struct helper_cvttss2sq_wrapper_ret_type var_86;
    struct helper_pxor_xmm_wrapper_ret_type var_52;
    struct helper_cvtsq2ss_wrapper_ret_type var_54;
    struct helper_pxor_xmm_wrapper_ret_type var_56;
    uint64_t var_0;
    uint64_t var_1;
    uint64_t var_2;
    uint64_t var_3;
    uint64_t var_4;
    uint64_t var_5;
    uint64_t var_6;
    uint64_t var_7;
    uint64_t var_8;
    uint64_t var_9;
    uint64_t var_10;
    uint64_t var_11;
    unsigned char var_12;
    unsigned char var_13;
    unsigned char var_14;
    unsigned char var_15;
    unsigned char var_16;
    unsigned char var_17;
    uint64_t *var_18;
    uint64_t var_19;
    uint64_t var_20;
    uint64_t var_21;
    uint64_t *var_22;
    uint64_t storemerge8;
    uint64_t var_106;
    uint64_t *var_107;
    uint64_t *var_108;
    uint64_t var_109;
    uint64_t local_sp_0;
    uint64_t var_110;
    uint64_t var_111;
    unsigned char storemerge7;
    uint64_t state_0x8558_3;
    uint64_t var_53;
    uint64_t state_0x8560_0;
    uint64_t var_55;
    uint64_t var_57;
    struct helper_cvtsq2ss_wrapper_ret_type var_58;
    struct helper_addss_wrapper_ret_type var_59;
    uint64_t state_0x8558_0;
    unsigned char storemerge5;
    uint64_t var_60;
    uint64_t var_61;
    uint64_t var_62;
    uint64_t state_0x8598_1;
    struct helper_cvtsq2ss_wrapper_165_ret_type var_63;
    uint64_t state_0x8598_0;
    uint64_t var_64;
    struct helper_cvtsq2ss_wrapper_165_ret_type var_65;
    struct helper_addss_wrapper_166_ret_type var_66;
    unsigned char storemerge4;
    struct helper_mulss_wrapper_ret_type var_67;
    struct helper_ucomiss_wrapper_ret_type var_68;
    uint64_t var_69;
    unsigned char var_70;
    uint64_t var_71;
    uint64_t *var_72;
    bool var_73;
    uint64_t var_74;
    bool var_75;
    uint64_t var_34;
    struct helper_cvtsq2ss_wrapper_ret_type var_76;
    uint64_t cc_dst_0;
    uint64_t var_77;
    struct helper_cvtsq2ss_wrapper_ret_type var_78;
    struct helper_addss_wrapper_ret_type var_79;
    uint64_t state_0x8558_1;
    unsigned char storemerge;
    struct helper_mulss_wrapper_167_ret_type var_80;
    uint64_t var_81;
    struct helper_ucomiss_wrapper_168_ret_type var_82;
    uint64_t var_83;
    unsigned char var_84;
    uint64_t var_85;
    uint64_t rax_0;
    struct helper_subss_wrapper_ret_type var_87;
    struct helper_cvtsq2ss_wrapper_ret_type var_89;
    uint64_t cc_dst_1;
    uint64_t var_90;
    struct helper_cvtsq2ss_wrapper_ret_type var_91;
    struct helper_addss_wrapper_ret_type var_92;
    uint64_t state_0x8558_2;
    unsigned char storemerge3;
    struct helper_mulss_wrapper_167_ret_type var_93;
    struct helper_mulss_wrapper_167_ret_type var_94;
    uint64_t var_95;
    struct helper_ucomiss_wrapper_168_ret_type var_96;
    uint64_t var_97;
    unsigned char var_98;
    uint64_t var_99;
    struct helper_cvttss2sq_wrapper_ret_type var_100;
    struct helper_subss_wrapper_ret_type var_101;
    struct helper_cvttss2sq_wrapper_ret_type var_102;
    uint64_t var_103;
    uint64_t var_104;
    uint64_t var_105;
    uint64_t *var_23;
    uint64_t *var_24;
    uint64_t var_25;
    struct helper_pxor_xmm_wrapper_ret_type var_26;
    uint64_t var_27;
    struct helper_cvtsq2ss_wrapper_ret_type var_28;
    uint64_t state_0x8560_1;
    uint64_t var_29;
    struct helper_pxor_xmm_wrapper_ret_type var_30;
    uint64_t var_31;
    struct helper_cvtsq2ss_wrapper_ret_type var_32;
    struct helper_addss_wrapper_ret_type var_33;
    uint64_t var_35;
    uint64_t var_36;
    struct helper_pxor_xmm_wrapper_160_ret_type var_37;
    uint64_t var_38;
    struct helper_cvtsq2ss_wrapper_165_ret_type var_39;
    uint64_t state_0x85a0_0;
    uint64_t var_40;
    struct helper_pxor_xmm_wrapper_160_ret_type var_41;
    uint64_t var_42;
    struct helper_cvtsq2ss_wrapper_165_ret_type var_43;
    struct helper_addss_wrapper_166_ret_type var_44;
    unsigned char storemerge6;
    struct helper_mulss_wrapper_ret_type var_45;
    uint64_t var_46;
    struct helper_ucomiss_wrapper_ret_type var_47;
    uint64_t var_48;
    unsigned char var_49;
    uint64_t var_50;
    uint64_t var_51;
    var_0 = revng_init_local_sp(0UL);
    var_1 = init_state_0x8558();
    var_2 = init_state_0x8598();
    var_3 = init_state_0x85a0();
    var_4 = init_state_0x8560();
    var_5 = init_rbp();
    var_6 = init_cc_src2();
    var_7 = init_rbx();
    var_8 = init_r10();
    var_9 = init_r9();
    var_10 = init_r8();
    var_11 = init_state_0x8d58();
    var_12 = init_state_0x8549();
    var_13 = init_state_0x854c();
    var_14 = init_state_0x8548();
    var_15 = init_state_0x854b();
    var_16 = init_state_0x8547();
    var_17 = init_state_0x854d();
    *(uint64_t *)(var_0 + (-8L)) = var_5;
    var_18 = (uint64_t *)(var_0 + (-64L));
    *var_18 = rdi;
    *(uint64_t *)(var_0 + (-72L)) = rsi;
    var_19 = var_0 + (-56L);
    var_20 = *var_18;
    *(uint64_t *)(var_0 + (-80L)) = 4239018UL;
    var_21 = indirect_placeholder_24(var_19, 1UL, rsi, var_20);
    var_22 = (uint64_t *)(var_0 + (-24L));
    *var_22 = var_21;
    storemerge8 = 0UL;
    if (var_21 == 0UL) {
        return storemerge8;
    }
    var_23 = (uint64_t *)(*var_18 + 32UL);
    *var_23 = (*var_23 + (-1L));
    if (**(uint64_t **)var_19 != 0UL) {
        var_24 = (uint64_t *)(*var_18 + 24UL);
        *var_24 = (*var_24 + (-1L));
        var_25 = *(uint64_t *)(*var_18 + 24UL);
        if ((long)var_25 < (long)0UL) {
            var_29 = (var_25 >> 1UL) | (var_25 & 1UL);
            var_30 = helper_pxor_xmm_wrapper((struct type_5 *)(0UL), (struct type_7 *)(776UL), (struct type_7 *)(776UL), var_1, var_4);
            var_31 = var_30.field_1;
            var_32 = helper_cvtsq2ss_wrapper((struct type_5 *)(0UL), (struct type_7 *)(776UL), var_29, var_12, var_14, var_15, var_16);
            var_33 = helper_addss_wrapper((struct type_5 *)(0UL), (struct type_7 *)(776UL), (struct type_7 *)(776UL), var_32.field_0, var_32.field_1, var_13, var_14, var_15, var_16, var_17);
            state_0x8560_1 = var_31;
            state_0x8558_3 = var_33.field_0;
            storemerge7 = var_33.field_1;
        } else {
            var_26 = helper_pxor_xmm_wrapper((struct type_5 *)(0UL), (struct type_7 *)(776UL), (struct type_7 *)(776UL), var_1, var_4);
            var_27 = var_26.field_1;
            var_28 = helper_cvtsq2ss_wrapper((struct type_5 *)(0UL), (struct type_7 *)(776UL), var_25, var_12, var_14, var_15, var_16);
            state_0x8560_1 = var_27;
            state_0x8558_3 = var_28.field_0;
            storemerge7 = var_28.field_1;
        }
        var_34 = *var_18;
        var_35 = (uint64_t)**(uint32_t **)(var_34 + 40UL);
        var_36 = *(uint64_t *)(var_34 + 16UL);
        if ((long)var_36 < (long)0UL) {
            var_40 = (var_36 >> 1UL) | (var_36 & 1UL);
            var_41 = helper_pxor_xmm_wrapper_160((struct type_5 *)(0UL), (struct type_7 *)(840UL), (struct type_7 *)(840UL), var_2, var_3);
            var_42 = var_41.field_1;
            var_43 = helper_cvtsq2ss_wrapper_165((struct type_5 *)(0UL), (struct type_7 *)(840UL), var_40, storemerge7, var_14, var_15, var_16);
            var_44 = helper_addss_wrapper_166((struct type_5 *)(0UL), (struct type_7 *)(840UL), (struct type_7 *)(840UL), var_43.field_0, var_43.field_1, var_13, var_14, var_15, var_16, var_17);
            state_0x85a0_0 = var_42;
            state_0x8598_1 = var_44.field_0;
            storemerge6 = var_44.field_1;
        } else {
            var_37 = helper_pxor_xmm_wrapper_160((struct type_5 *)(0UL), (struct type_7 *)(840UL), (struct type_7 *)(840UL), var_2, var_3);
            var_38 = var_37.field_1;
            var_39 = helper_cvtsq2ss_wrapper_165((struct type_5 *)(0UL), (struct type_7 *)(840UL), var_36, storemerge7, var_14, var_15, var_16);
            state_0x85a0_0 = var_38;
            state_0x8598_1 = var_39.field_0;
            storemerge6 = var_39.field_1;
        }
        var_45 = helper_mulss_wrapper((struct type_5 *)(0UL), (struct type_7 *)(840UL), (struct type_7 *)(904UL), state_0x8598_1, var_35, storemerge6, var_13, var_14, var_15, var_16, var_17);
        var_46 = var_45.field_0;
        var_47 = helper_ucomiss_wrapper((struct type_5 *)(0UL), (struct type_7 *)(840UL), (struct type_7 *)(776UL), state_0x8558_3, var_46, var_45.field_1, var_13);
        var_48 = var_47.field_0;
        var_49 = var_47.field_1;
        if ((var_48 & 65UL) != 0UL) {
            var_50 = *var_18;
            *(uint64_t *)(var_0 + (-88L)) = 4239230UL;
            indirect_placeholder_10(var_50);
            var_51 = *(uint64_t *)(*var_18 + 24UL);
            if ((long)var_51 < (long)0UL) {
                var_55 = (var_51 >> 1UL) | (var_51 & 1UL);
                var_56 = helper_pxor_xmm_wrapper((struct type_5 *)(0UL), (struct type_7 *)(776UL), (struct type_7 *)(776UL), state_0x8558_3, state_0x8560_1);
                var_57 = var_56.field_1;
                var_58 = helper_cvtsq2ss_wrapper((struct type_5 *)(0UL), (struct type_7 *)(776UL), var_55, var_49, var_14, var_15, var_16);
                var_59 = helper_addss_wrapper((struct type_5 *)(0UL), (struct type_7 *)(776UL), (struct type_7 *)(776UL), var_58.field_0, var_58.field_1, var_13, var_14, var_15, var_16, var_17);
                state_0x8560_0 = var_57;
                state_0x8558_0 = var_59.field_0;
                storemerge5 = var_59.field_1;
            } else {
                var_52 = helper_pxor_xmm_wrapper((struct type_5 *)(0UL), (struct type_7 *)(776UL), (struct type_7 *)(776UL), state_0x8558_3, state_0x8560_1);
                var_53 = var_52.field_1;
                var_54 = helper_cvtsq2ss_wrapper((struct type_5 *)(0UL), (struct type_7 *)(776UL), var_51, var_49, var_14, var_15, var_16);
                state_0x8560_0 = var_53;
                state_0x8558_0 = var_54.field_0;
                storemerge5 = var_54.field_1;
            }
            var_60 = *var_18;
            var_61 = (uint64_t)**(uint32_t **)(var_60 + 40UL);
            var_62 = *(uint64_t *)(var_60 + 16UL);
            if ((long)var_62 < (long)0UL) {
                var_64 = (var_62 >> 1UL) | (var_62 & 1UL);
                helper_pxor_xmm_wrapper_160((struct type_5 *)(0UL), (struct type_7 *)(840UL), (struct type_7 *)(840UL), var_46, state_0x85a0_0);
                var_65 = helper_cvtsq2ss_wrapper_165((struct type_5 *)(0UL), (struct type_7 *)(840UL), var_64, storemerge5, var_14, var_15, var_16);
                var_66 = helper_addss_wrapper_166((struct type_5 *)(0UL), (struct type_7 *)(840UL), (struct type_7 *)(840UL), var_65.field_0, var_65.field_1, var_13, var_14, var_15, var_16, var_17);
                state_0x8598_0 = var_66.field_0;
                storemerge4 = var_66.field_1;
            } else {
                helper_pxor_xmm_wrapper_160((struct type_5 *)(0UL), (struct type_7 *)(840UL), (struct type_7 *)(840UL), var_46, state_0x85a0_0);
                var_63 = helper_cvtsq2ss_wrapper_165((struct type_5 *)(0UL), (struct type_7 *)(840UL), var_62, storemerge5, var_14, var_15, var_16);
                state_0x8598_0 = var_63.field_0;
                storemerge4 = var_63.field_1;
            }
            var_67 = helper_mulss_wrapper((struct type_5 *)(0UL), (struct type_7 *)(840UL), (struct type_7 *)(904UL), state_0x8598_0, var_61, storemerge4, var_13, var_14, var_15, var_16, var_17);
            var_68 = helper_ucomiss_wrapper((struct type_5 *)(0UL), (struct type_7 *)(840UL), (struct type_7 *)(776UL), state_0x8558_0, var_67.field_0, var_67.field_1, var_13);
            var_69 = var_68.field_0;
            var_70 = var_68.field_1;
            if ((var_69 & 65UL) != 0UL) {
                var_71 = *(uint64_t *)(*var_18 + 40UL);
                var_72 = (uint64_t *)(var_0 + (-32L));
                *var_72 = var_71;
                var_73 = (*(unsigned char *)(var_71 + 16UL) == '\x00');
                var_74 = *(uint64_t *)(*var_18 + 16UL);
                var_75 = ((long)var_74 < (long)0UL);
                cc_dst_0 = var_74;
                cc_dst_1 = var_74;
                if (var_73) {
                    if (var_75) {
                        var_90 = (var_74 >> 1UL) | (var_74 & 1UL);
                        helper_pxor_xmm_wrapper((struct type_5 *)(0UL), (struct type_7 *)(776UL), (struct type_7 *)(776UL), state_0x8558_0, state_0x8560_0);
                        var_91 = helper_cvtsq2ss_wrapper((struct type_5 *)(0UL), (struct type_7 *)(776UL), var_90, var_70, var_14, var_15, var_16);
                        var_92 = helper_addss_wrapper((struct type_5 *)(0UL), (struct type_7 *)(776UL), (struct type_7 *)(776UL), var_91.field_0, var_91.field_1, var_13, var_14, var_15, var_16, var_17);
                        cc_dst_1 = var_90;
                        state_0x8558_2 = var_92.field_0;
                        storemerge3 = var_92.field_1;
                    } else {
                        helper_pxor_xmm_wrapper((struct type_5 *)(0UL), (struct type_7 *)(776UL), (struct type_7 *)(776UL), state_0x8558_0, state_0x8560_0);
                        var_89 = helper_cvtsq2ss_wrapper((struct type_5 *)(0UL), (struct type_7 *)(776UL), var_74, var_70, var_14, var_15, var_16);
                        state_0x8558_2 = var_89.field_0;
                        storemerge3 = var_89.field_1;
                    }
                    var_93 = helper_mulss_wrapper_167((struct type_5 *)(0UL), (struct type_7 *)(776UL), (struct type_7 *)(840UL), state_0x8558_2, (uint64_t)*(uint32_t *)(*var_72 + 4UL), storemerge3, var_13, var_14, var_15, var_16, var_17);
                    var_94 = helper_mulss_wrapper_167((struct type_5 *)(0UL), (struct type_7 *)(776UL), (struct type_7 *)(840UL), var_93.field_0, (uint64_t)*(uint32_t *)(*var_72 + 8UL), var_93.field_1, var_13, var_14, var_15, var_16, var_17);
                    var_95 = var_94.field_0;
                    var_96 = helper_ucomiss_wrapper_168((struct type_5 *)(0UL), (struct type_7 *)(776UL), (struct type_7 *)(2824UL), var_95, (var_11 & (-4294967296L)) | (uint64_t)*(uint32_t *)4302872UL, var_94.field_1, var_13);
                    var_97 = var_96.field_0;
                    var_98 = var_96.field_1;
                    var_99 = helper_cc_compute_c_wrapper(cc_dst_1, var_97, var_6, 1U);
                    if (var_99 == 0UL) {
                        var_101 = helper_subss_wrapper((struct type_5 *)(0UL), (struct type_7 *)(776UL), (struct type_7 *)(840UL), var_95, (uint64_t)*(uint32_t *)4302872UL, var_98, var_13, var_14, var_15, var_16, var_17);
                        var_102 = helper_cvttss2sq_wrapper((struct type_5 *)(0UL), (struct type_7 *)(776UL), var_101.field_0, var_101.field_1, var_13);
                        rax_0 = var_102.field_0 ^ (-9223372036854775808L);
                    } else {
                        var_100 = helper_cvttss2sq_wrapper((struct type_5 *)(0UL), (struct type_7 *)(776UL), var_95, var_98, var_13);
                        rax_0 = var_100.field_0;
                    }
                } else {
                    if (var_75) {
                        var_77 = (var_74 >> 1UL) | (var_74 & 1UL);
                        helper_pxor_xmm_wrapper((struct type_5 *)(0UL), (struct type_7 *)(776UL), (struct type_7 *)(776UL), state_0x8558_0, state_0x8560_0);
                        var_78 = helper_cvtsq2ss_wrapper((struct type_5 *)(0UL), (struct type_7 *)(776UL), var_77, var_70, var_14, var_15, var_16);
                        var_79 = helper_addss_wrapper((struct type_5 *)(0UL), (struct type_7 *)(776UL), (struct type_7 *)(776UL), var_78.field_0, var_78.field_1, var_13, var_14, var_15, var_16, var_17);
                        cc_dst_0 = var_77;
                        state_0x8558_1 = var_79.field_0;
                        storemerge = var_79.field_1;
                    } else {
                        helper_pxor_xmm_wrapper((struct type_5 *)(0UL), (struct type_7 *)(776UL), (struct type_7 *)(776UL), state_0x8558_0, state_0x8560_0);
                        var_76 = helper_cvtsq2ss_wrapper((struct type_5 *)(0UL), (struct type_7 *)(776UL), var_74, var_70, var_14, var_15, var_16);
                        state_0x8558_1 = var_76.field_0;
                        storemerge = var_76.field_1;
                    }
                    var_80 = helper_mulss_wrapper_167((struct type_5 *)(0UL), (struct type_7 *)(776UL), (struct type_7 *)(840UL), state_0x8558_1, (uint64_t)*(uint32_t *)(*var_72 + 4UL), storemerge, var_13, var_14, var_15, var_16, var_17);
                    var_81 = var_80.field_0;
                    var_82 = helper_ucomiss_wrapper_168((struct type_5 *)(0UL), (struct type_7 *)(776UL), (struct type_7 *)(2824UL), var_81, (var_11 & (-4294967296L)) | (uint64_t)*(uint32_t *)4302872UL, var_80.field_1, var_13);
                    var_83 = var_82.field_0;
                    var_84 = var_82.field_1;
                    var_85 = helper_cc_compute_c_wrapper(cc_dst_0, var_83, var_6, 1U);
                    if (var_85 == 0UL) {
                        var_87 = helper_subss_wrapper((struct type_5 *)(0UL), (struct type_7 *)(776UL), (struct type_7 *)(840UL), var_81, (uint64_t)*(uint32_t *)4302872UL, var_84, var_13, var_14, var_15, var_16, var_17);
                        var_88 = helper_cvttss2sq_wrapper((struct type_5 *)(0UL), (struct type_7 *)(776UL), var_87.field_0, var_87.field_1, var_13);
                        rax_0 = var_88.field_0 ^ (-9223372036854775808L);
                    } else {
                        var_86 = helper_cvttss2sq_wrapper((struct type_5 *)(0UL), (struct type_7 *)(776UL), var_81, var_84, var_13);
                        rax_0 = var_86.field_0;
                    }
                }
                *(uint64_t *)(var_0 + (-40L)) = rax_0;
                var_103 = *var_18;
                var_104 = var_0 + (-96L);
                *(uint64_t *)var_104 = 4239634UL;
                var_105 = indirect_placeholder_31(1UL, var_7, rax_0, var_103, var_8, var_9, var_10);
                local_sp_0 = var_104;
                if ((uint64_t)(unsigned char)var_105 != 1UL) {
                    var_106 = *(uint64_t *)(*var_18 + 72UL);
                    var_107 = (uint64_t *)(var_0 + (-16L));
                    *var_107 = var_106;
                    var_108 = (uint64_t *)(var_0 + (-48L));
                    var_109 = var_106;
                    while (var_109 != 0UL)
                        {
                            *var_108 = *(uint64_t *)(var_109 + 8UL);
                            var_110 = local_sp_0 + (-8L);
                            *(uint64_t *)var_110 = 4239679UL;
                            indirect_placeholder();
                            var_111 = *var_108;
                            *var_107 = var_111;
                            var_109 = var_111;
                            local_sp_0 = var_110;
                        }
                    *(uint64_t *)(*var_18 + 72UL) = 0UL;
                }
            }
        }
    }
    storemerge8 = *var_22;
    return storemerge8;
}

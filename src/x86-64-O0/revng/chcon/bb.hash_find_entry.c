typedef unsigned char __u_char;
typedef unsigned short __u_short;
typedef unsigned int __u_int;
typedef unsigned long __u_long;
typedef signed char __int8_t;
typedef unsigned char __uint8_t;
typedef short __int16_t;
typedef unsigned short __uint16_t;
typedef int __int32_t;
typedef unsigned int __uint32_t;
typedef long __int64_t;
typedef unsigned long __uint64_t;
typedef __int8_t __int_least8_t;
typedef __uint8_t __uint_least8_t;
typedef __int16_t __int_least16_t;
typedef __uint16_t __uint_least16_t;
typedef __int32_t __int_least32_t;
typedef __uint32_t __uint_least32_t;
typedef __int64_t __int_least64_t;
typedef __uint64_t __uint_least64_t;
typedef long __quad_t;
typedef unsigned long __u_quad_t;
typedef long __intmax_t;
typedef unsigned long __uintmax_t;
typedef unsigned long __dev_t;
typedef unsigned int __uid_t;
typedef unsigned int __gid_t;
typedef unsigned long __ino_t;
typedef unsigned long __ino64_t;
typedef unsigned int __mode_t;
typedef unsigned long __nlink_t;
typedef long __off_t;
typedef long __off64_t;
typedef int __pid_t;
typedef struct {
    int __val[2];
} __fsid_t;
typedef long __clock_t;
typedef unsigned long __rlim_t;
typedef unsigned long __rlim64_t;
typedef unsigned int __id_t;
typedef long __time_t;
typedef unsigned int __useconds_t;
typedef long __suseconds_t;
typedef int __daddr_t;
typedef int __key_t;
typedef int __clockid_t;
typedef void *__timer_t;
typedef long __blksize_t;
typedef long __blkcnt_t;
typedef long __blkcnt64_t;
typedef unsigned long __fsblkcnt_t;
typedef unsigned long __fsblkcnt64_t;
typedef unsigned long __fsfilcnt_t;
typedef unsigned long __fsfilcnt64_t;
typedef long __fsword_t;
typedef long __ssize_t;
typedef long __syscall_slong_t;
typedef unsigned long __syscall_ulong_t;
typedef __off64_t __loff_t;
typedef char *__caddr_t;
typedef long __intptr_t;
typedef unsigned int __socklen_t;
typedef int __sig_atomic_t;
typedef __int8_t int8_t;
typedef __int16_t int16_t;
typedef __int32_t int32_t;
typedef __int64_t int64_t;
typedef __uint8_t uint8_t;
typedef __uint16_t uint16_t;
typedef __uint32_t uint32_t;
typedef __uint64_t uint64_t;
typedef __int_least8_t int_least8_t;
typedef __int_least16_t int_least16_t;
typedef __int_least32_t int_least32_t;
typedef __int_least64_t int_least64_t;
typedef __uint_least8_t uint_least8_t;
typedef __uint_least16_t uint_least16_t;
typedef __uint_least32_t uint_least32_t;
typedef __uint_least64_t uint_least64_t;
typedef signed char int_fast8_t;
typedef long int_fast16_t;
typedef long int_fast32_t;
typedef long int_fast64_t;
typedef unsigned char uint_fast8_t;
typedef unsigned long uint_fast16_t;
typedef unsigned long uint_fast32_t;
typedef unsigned long uint_fast64_t;
typedef long intptr_t;
typedef unsigned long uintptr_t;
typedef __intmax_t intmax_t;
typedef __uintmax_t uintmax_t;
extern uint64_t revng_init_local_sp(uint64_t param_0);
extern uint64_t init_rbp(void);
extern uint64_t indirect_placeholder_1(uint64_t param_0, uint64_t param_1);
extern void indirect_placeholder(void);
extern void indirect_placeholder_30(uint64_t param_0, uint64_t param_1);
uint64_t bb_hash_find_entry(uint64_t rdx, uint64_t rcx, uint64_t rsi, uint64_t rdi) {
    uint64_t var_0;
    uint64_t var_1;
    uint64_t *var_2;
    uint64_t *var_3;
    uint64_t var_4;
    unsigned char *var_5;
    uint64_t var_6;
    uint64_t var_7;
    uint64_t var_8;
    uint64_t var_9;
    uint64_t var_10;
    uint64_t *var_11;
    uint64_t **var_12;
    uint64_t var_13;
    uint64_t rax_0;
    uint64_t var_23;
    uint64_t var_18;
    uint64_t var_16;
    uint64_t *var_17;
    uint64_t local_sp_0;
    uint64_t var_19;
    uint64_t **var_20;
    uint64_t var_31;
    uint64_t var_21;
    uint64_t var_22;
    uint64_t **_pre_phi70;
    uint64_t var_24;
    uint64_t local_sp_1;
    uint64_t *var_25;
    uint64_t var_30;
    uint64_t var_26;
    uint64_t *var_27;
    uint64_t var_28;
    uint64_t var_29;
    uint64_t var_14;
    uint64_t var_15;
    uint64_t local_sp_2;
    uint64_t *var_32;
    uint64_t var_33;
    uint64_t *var_34;
    uint64_t *var_35;
    uint64_t var_36;
    uint64_t var_37;
    uint64_t var_38;
    uint64_t var_39;
    unsigned int loop_state_var;
    var_0 = revng_init_local_sp(0UL);
    var_1 = init_rbp();
    *(uint64_t *)(var_0 + (-8L)) = var_1;
    var_2 = (uint64_t *)(var_0 + (-64L));
    *var_2 = rdi;
    var_3 = (uint64_t *)(var_0 + (-72L));
    *var_3 = rsi;
    var_4 = var_0 + (-80L);
    *(uint64_t *)var_4 = rdx;
    var_5 = (unsigned char *)(var_0 + (-84L));
    *var_5 = (unsigned char)rcx;
    var_6 = *var_3;
    var_7 = *var_2;
    var_8 = var_0 + (-96L);
    *(uint64_t *)var_8 = 4236715UL;
    var_9 = indirect_placeholder_1(var_6, var_7);
    var_10 = var_0 + (-24L);
    var_11 = (uint64_t *)var_10;
    *var_11 = var_9;
    **(uint64_t **)var_4 = var_9;
    var_12 = (uint64_t **)var_10;
    var_13 = **var_12;
    var_31 = var_13;
    local_sp_2 = var_8;
    rax_0 = 0UL;
    if (var_13 == 0UL) {
        return rax_0;
    }
    if (var_13 != *var_3) {
        var_14 = *(uint64_t *)(*var_2 + 56UL);
        var_15 = var_0 + (-104L);
        *(uint64_t *)var_15 = 4236792UL;
        indirect_placeholder();
        local_sp_0 = var_15;
        local_sp_2 = var_15;
        if ((uint64_t)(unsigned char)var_14 != 0UL) {
            var_16 = *var_11;
            var_17 = (uint64_t *)(var_0 + (-16L));
            *var_17 = var_16;
            var_18 = var_16;
            while (1U)
                {
                    var_19 = var_18 + 8UL;
                    local_sp_1 = local_sp_0;
                    if (*(uint64_t *)var_19 != 0UL) {
                        loop_state_var = 1U;
                        break;
                    }
                    var_20 = (uint64_t **)var_19;
                    _pre_phi70 = var_20;
                    if (**var_20 != *var_3) {
                        loop_state_var = 0U;
                        break;
                    }
                    var_21 = *(uint64_t *)(*var_2 + 56UL);
                    var_22 = local_sp_0 + (-8L);
                    *(uint64_t *)var_22 = 4236962UL;
                    indirect_placeholder();
                    local_sp_0 = var_22;
                    local_sp_1 = var_22;
                    if ((uint64_t)(unsigned char)var_21 != 0UL) {
                        _pre_phi70 = (uint64_t **)(*var_17 + 8UL);
                        loop_state_var = 0U;
                        break;
                    }
                    var_23 = *(uint64_t *)(*var_17 + 8UL);
                    *var_17 = var_23;
                    var_18 = var_23;
                    continue;
                }
            switch (loop_state_var) {
              case 1U:
                {
                    return rax_0;
                }
                break;
              case 0U:
                {
                    var_24 = **_pre_phi70;
                    var_25 = (uint64_t *)(var_0 + (-32L));
                    *var_25 = var_24;
                    var_30 = var_24;
                    if (*var_5 != '\x00') {
                        var_26 = *(uint64_t *)(*var_17 + 8UL);
                        var_27 = (uint64_t *)(var_0 + (-40L));
                        *var_27 = var_26;
                        *(uint64_t *)(*var_17 + 8UL) = *(uint64_t *)(var_26 + 8UL);
                        var_28 = *var_27;
                        var_29 = *var_2;
                        *(uint64_t *)(local_sp_1 + (-8L)) = 4237034UL;
                        indirect_placeholder_30(var_28, var_29);
                        var_30 = *var_25;
                    }
                    rax_0 = var_30;
                }
                break;
            }
        }
        var_31 = **var_12;
    }
    var_32 = (uint64_t *)(var_0 + (-48L));
    *var_32 = var_31;
    if (*var_5 != '\x00') {
        var_33 = *(uint64_t *)(*var_11 + 8UL);
        if (var_33 == 0UL) {
            **var_12 = 0UL;
        } else {
            var_34 = (uint64_t *)(var_0 + (-56L));
            *var_34 = var_33;
            var_35 = *var_12;
            var_36 = *(uint64_t *)(var_33 + 8UL);
            *var_35 = *(uint64_t *)var_33;
            *(uint64_t *)((uint64_t)var_35 + 8UL) = var_36;
            var_37 = *var_34;
            var_38 = *var_2;
            *(uint64_t *)(local_sp_2 + (-8L)) = 4236879UL;
            indirect_placeholder_30(var_37, var_38);
        }
    }
    var_39 = *var_32;
    rax_0 = var_39;
    return rax_0;
}

typedef unsigned char __u_char;
typedef unsigned short __u_short;
typedef unsigned int __u_int;
typedef unsigned long __u_long;
typedef signed char __int8_t;
typedef unsigned char __uint8_t;
typedef short __int16_t;
typedef unsigned short __uint16_t;
typedef int __int32_t;
typedef unsigned int __uint32_t;
typedef long __int64_t;
typedef unsigned long __uint64_t;
typedef __int8_t __int_least8_t;
typedef __uint8_t __uint_least8_t;
typedef __int16_t __int_least16_t;
typedef __uint16_t __uint_least16_t;
typedef __int32_t __int_least32_t;
typedef __uint32_t __uint_least32_t;
typedef __int64_t __int_least64_t;
typedef __uint64_t __uint_least64_t;
typedef long __quad_t;
typedef unsigned long __u_quad_t;
typedef long __intmax_t;
typedef unsigned long __uintmax_t;
typedef unsigned long __dev_t;
typedef unsigned int __uid_t;
typedef unsigned int __gid_t;
typedef unsigned long __ino_t;
typedef unsigned long __ino64_t;
typedef unsigned int __mode_t;
typedef unsigned long __nlink_t;
typedef long __off_t;
typedef long __off64_t;
typedef int __pid_t;
typedef struct {
    int __val[2];
} __fsid_t;
typedef long __clock_t;
typedef unsigned long __rlim_t;
typedef unsigned long __rlim64_t;
typedef unsigned int __id_t;
typedef long __time_t;
typedef unsigned int __useconds_t;
typedef long __suseconds_t;
typedef int __daddr_t;
typedef int __key_t;
typedef int __clockid_t;
typedef void *__timer_t;
typedef long __blksize_t;
typedef long __blkcnt_t;
typedef long __blkcnt64_t;
typedef unsigned long __fsblkcnt_t;
typedef unsigned long __fsblkcnt64_t;
typedef unsigned long __fsfilcnt_t;
typedef unsigned long __fsfilcnt64_t;
typedef long __fsword_t;
typedef long __ssize_t;
typedef long __syscall_slong_t;
typedef unsigned long __syscall_ulong_t;
typedef __off64_t __loff_t;
typedef char *__caddr_t;
typedef long __intptr_t;
typedef unsigned int __socklen_t;
typedef int __sig_atomic_t;
typedef __int8_t int8_t;
typedef __int16_t int16_t;
typedef __int32_t int32_t;
typedef __int64_t int64_t;
typedef __uint8_t uint8_t;
typedef __uint16_t uint16_t;
typedef __uint32_t uint32_t;
typedef __uint64_t uint64_t;
typedef __int_least8_t int_least8_t;
typedef __int_least16_t int_least16_t;
typedef __int_least32_t int_least32_t;
typedef __int_least64_t int_least64_t;
typedef __uint_least8_t uint_least8_t;
typedef __uint_least16_t uint_least16_t;
typedef __uint_least32_t uint_least32_t;
typedef __uint_least64_t uint_least64_t;
typedef signed char int_fast8_t;
typedef long int_fast16_t;
typedef long int_fast32_t;
typedef long int_fast64_t;
typedef unsigned char uint_fast8_t;
typedef unsigned long uint_fast16_t;
typedef unsigned long uint_fast32_t;
typedef unsigned long uint_fast64_t;
typedef long intptr_t;
typedef unsigned long uintptr_t;
typedef __intmax_t intmax_t;
typedef __uintmax_t uintmax_t;
typedef _Bool bool;
extern uint64_t revng_init_local_sp(uint64_t param_0);
extern uint64_t init_rbp(void);
extern uint64_t init_cc_src2(void);
extern uint64_t helper_cc_compute_c_wrapper(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint32_t param_3);
extern void indirect_placeholder(void);
extern uint64_t init_rbx(void);
extern void indirect_placeholder_21(uint64_t param_0);
extern uint64_t helper_cc_compute_all_wrapper(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint32_t param_3);
uint64_t bb_mbscasecmp(uint64_t rsi, uint64_t rdi) {
    uint64_t var_0;
    uint64_t var_1;
    uint64_t var_2;
    uint64_t var_3;
    uint64_t *var_4;
    uint64_t *var_5;
    uint64_t var_6;
    uint64_t rax_3;
    uint64_t local_sp_0;
    bool var_43;
    bool var_44;
    uint32_t var_58;
    uint64_t var_41;
    unsigned char var_42;
    uint32_t var_45;
    uint32_t var_46;
    uint64_t var_47;
    uint64_t local_sp_2;
    uint64_t rax_0;
    uint64_t local_sp_1;
    uint64_t var_7;
    uint64_t var_48;
    uint64_t var_49;
    uint64_t var_56;
    uint64_t var_57;
    uint64_t var_50;
    bool var_51;
    uint64_t var_52;
    uint64_t var_53;
    uint64_t *var_54;
    uint64_t var_55;
    uint64_t var_40;
    uint64_t *var_25;
    uint64_t var_26;
    unsigned char *var_27;
    uint64_t var_28;
    uint64_t *var_29;
    uint64_t var_30;
    uint64_t var_31;
    unsigned char *var_32;
    unsigned char *var_33;
    uint32_t *var_34;
    unsigned char *var_35;
    uint32_t *var_36;
    uint64_t *var_37;
    uint64_t *var_38;
    uint32_t *var_39;
    uint64_t var_10;
    uint64_t *var_11;
    uint64_t var_12;
    uint64_t var_13;
    uint64_t *var_14;
    unsigned char *var_15;
    unsigned char *var_16;
    unsigned char var_24;
    uint64_t local_sp_3;
    unsigned char var_17;
    uint64_t local_sp_4;
    uint64_t var_18;
    unsigned char var_19;
    uint32_t var_20;
    uint64_t local_sp_5;
    uint64_t var_21;
    unsigned char var_22;
    unsigned char var_23;
    uint32_t _pre_phi99;
    bool var_8;
    uint64_t var_9;
    unsigned int loop_state_var;
    var_0 = revng_init_local_sp(0UL);
    var_1 = init_rbp();
    var_2 = init_cc_src2();
    var_3 = init_rbx();
    *(uint64_t *)(var_0 + (-8L)) = var_1;
    *(uint64_t *)(var_0 + (-16L)) = var_3;
    var_4 = (uint64_t *)(var_0 + (-192L));
    *var_4 = rdi;
    var_5 = (uint64_t *)(var_0 + (-200L));
    *var_5 = rsi;
    var_6 = *var_4;
    rax_0 = 4294967295UL;
    var_24 = (unsigned char)'\x00';
    rax_3 = 0UL;
    if (var_6 == rsi) {
        return rax_3;
    }
    var_7 = var_0 + (-208L);
    *(uint64_t *)var_7 = 4238076UL;
    indirect_placeholder();
    var_8 = (var_6 > 1UL);
    var_9 = *var_4;
    local_sp_3 = var_7;
    rax_3 = 1UL;
    if (var_8) {
        var_25 = (uint64_t *)(var_0 + (-168L));
        *var_25 = var_9;
        var_26 = var_0 + (-184L);
        *(unsigned char *)var_26 = (unsigned char)'\x00';
        *(uint64_t *)(var_0 + (-216L)) = 4238136UL;
        indirect_placeholder();
        var_27 = (unsigned char *)(var_0 + (-172L));
        *var_27 = (unsigned char)'\x00';
        var_28 = *var_5;
        var_29 = (uint64_t *)(var_0 + (-104L));
        *var_29 = var_28;
        var_30 = var_0 + (-120L);
        *(unsigned char *)var_30 = (unsigned char)'\x00';
        var_31 = var_0 + (-224L);
        *(uint64_t *)var_31 = 4238184UL;
        indirect_placeholder();
        var_32 = (unsigned char *)(var_0 + (-108L));
        *var_32 = (unsigned char)'\x00';
        var_33 = (unsigned char *)(var_0 + (-152L));
        var_34 = (uint32_t *)(var_0 + (-148L));
        var_35 = (unsigned char *)(var_0 + (-88L));
        var_36 = (uint32_t *)(var_0 + (-84L));
        var_37 = (uint64_t *)(var_0 + (-160L));
        var_38 = (uint64_t *)(var_0 + (-96L));
        var_39 = (uint32_t *)(var_0 + (-44L));
        local_sp_2 = var_31;
        while (1U)
            {
                var_40 = local_sp_2 + (-8L);
                *(uint64_t *)var_40 = 4238508UL;
                indirect_placeholder_21(var_26);
                local_sp_0 = var_40;
                if (*var_33 != '\x01') {
                    if (*var_34 != 0U) {
                        loop_state_var = 1U;
                        break;
                    }
                }
                var_41 = local_sp_2 + (-16L);
                *(uint64_t *)var_41 = 4238560UL;
                indirect_placeholder_21(var_30);
                var_42 = *var_35;
                local_sp_0 = var_41;
                local_sp_1 = var_41;
                if (var_42 != '\x01') {
                    if (*var_36 != 0U) {
                        loop_state_var = 1U;
                        break;
                    }
                }
                var_43 = (*var_33 == '\x00');
                var_44 = (var_42 == '\x00');
                if (var_43) {
                    rax_0 = 1UL;
                    if (!var_44) {
                        var_48 = *var_37;
                        var_49 = *var_38;
                        if (var_48 == var_49) {
                            var_56 = *var_25;
                            var_57 = local_sp_2 + (-24L);
                            *(uint64_t *)var_57 = 4238319UL;
                            indirect_placeholder();
                            rax_0 = var_56;
                            local_sp_1 = var_57;
                        } else {
                            var_50 = helper_cc_compute_c_wrapper(var_48 - var_49, var_49, var_2, 17U);
                            var_51 = (var_50 == 0UL);
                            var_52 = *var_25;
                            var_53 = local_sp_2 + (-24L);
                            var_54 = (uint64_t *)var_53;
                            local_sp_1 = var_53;
                            if (var_51) {
                                *var_54 = 4238410UL;
                                indirect_placeholder();
                                rax_0 = ((int)(uint32_t)var_52 < (int)0U) ? 4294967295UL : 1UL;
                            } else {
                                *var_54 = 4238366UL;
                                indirect_placeholder();
                                var_55 = helper_cc_compute_all_wrapper(var_52, 0UL, 0UL, 24U);
                                rax_0 = ((uint64_t)(((unsigned char)(var_55 >> 4UL) ^ (unsigned char)var_55) & '\xc0') == 0UL) ? 1UL : 4294967295UL;
                            }
                        }
                    }
                } else {
                    if (var_44) {
                        var_45 = *var_34;
                        *(uint64_t *)(local_sp_2 + (-24L)) = 4238225UL;
                        indirect_placeholder();
                        var_46 = *var_36;
                        var_47 = local_sp_2 + (-32L);
                        *(uint64_t *)var_47 = 4238237UL;
                        indirect_placeholder();
                        rax_0 = (uint64_t)(var_45 - var_46);
                        local_sp_1 = var_47;
                    }
                }
                var_58 = (uint32_t)rax_0;
                *var_39 = var_58;
                local_sp_2 = local_sp_1;
                if (var_58 != 0U) {
                    rax_3 = (uint64_t)var_58;
                    loop_state_var = 0U;
                    break;
                }
                *var_25 = (*var_37 + *var_25);
                *var_27 = (unsigned char)'\x00';
                *var_29 = (*var_38 + *var_29);
                *var_32 = (unsigned char)'\x00';
                continue;
            }
        *(uint64_t *)(local_sp_0 + (-8L)) = 4238613UL;
        indirect_placeholder_21(var_26);
        *(uint64_t *)(local_sp_0 + (-16L)) = 4238675UL;
        indirect_placeholder_21(var_30);
        rax_3 = 4294967295UL;
        if (*var_33 != '\x01' & *var_34 != 0U & *var_35 == '\x01') {
            return (*var_36 == 0U) ? 0UL : 4294967295UL;
        }
    }
    var_10 = var_0 + (-32L);
    var_11 = (uint64_t *)var_10;
    *var_11 = var_9;
    var_12 = *var_5;
    var_13 = var_0 + (-40L);
    var_14 = (uint64_t *)var_13;
    *var_14 = var_12;
    var_15 = (unsigned char *)(var_0 + (-45L));
    var_16 = (unsigned char *)(var_0 + (-46L));
    while (1U)
        {
            var_17 = **(unsigned char **)var_10;
            local_sp_4 = local_sp_3;
            if ((uint64_t)(((uint32_t)(uint64_t)var_17 + (-65)) & (-2)) > 25UL) {
                var_18 = local_sp_3 + (-8L);
                *(uint64_t *)var_18 = 4238786UL;
                indirect_placeholder();
                local_sp_4 = var_18;
            }
            *var_15 = var_17;
            var_19 = **(unsigned char **)var_13;
            var_20 = (uint32_t)(uint64_t)var_19;
            local_sp_5 = local_sp_4;
            _pre_phi99 = var_20;
            if ((uint64_t)((var_20 + (-65)) & (-2)) > 25UL) {
                var_21 = local_sp_4 + (-8L);
                *(uint64_t *)var_21 = 4238833UL;
                indirect_placeholder();
                local_sp_5 = var_21;
            }
            *var_16 = var_19;
            local_sp_3 = local_sp_5;
            if (*var_15 != '\x00') {
                break;
            }
            *var_11 = (*var_11 + 1UL);
            *var_14 = (*var_14 + 1UL);
            var_22 = *var_15;
            var_23 = *var_16;
            var_24 = var_22;
            if ((uint64_t)(var_22 - var_23) == 0UL) {
                continue;
            }
            _pre_phi99 = (uint32_t)(uint64_t)var_23;
            break;
        }
    rax_3 = (uint64_t)((uint32_t)(uint64_t)var_24 - _pre_phi99);
}

typedef unsigned char __u_char;
typedef unsigned short __u_short;
typedef unsigned int __u_int;
typedef unsigned long __u_long;
typedef signed char __int8_t;
typedef unsigned char __uint8_t;
typedef short __int16_t;
typedef unsigned short __uint16_t;
typedef int __int32_t;
typedef unsigned int __uint32_t;
typedef long __int64_t;
typedef unsigned long __uint64_t;
typedef __int8_t __int_least8_t;
typedef __uint8_t __uint_least8_t;
typedef __int16_t __int_least16_t;
typedef __uint16_t __uint_least16_t;
typedef __int32_t __int_least32_t;
typedef __uint32_t __uint_least32_t;
typedef __int64_t __int_least64_t;
typedef __uint64_t __uint_least64_t;
typedef long __quad_t;
typedef unsigned long __u_quad_t;
typedef long __intmax_t;
typedef unsigned long __uintmax_t;
typedef unsigned long __dev_t;
typedef unsigned int __uid_t;
typedef unsigned int __gid_t;
typedef unsigned long __ino_t;
typedef unsigned long __ino64_t;
typedef unsigned int __mode_t;
typedef unsigned long __nlink_t;
typedef long __off_t;
typedef long __off64_t;
typedef int __pid_t;
typedef struct {
    int __val[2];
} __fsid_t;
typedef long __clock_t;
typedef unsigned long __rlim_t;
typedef unsigned long __rlim64_t;
typedef unsigned int __id_t;
typedef long __time_t;
typedef unsigned int __useconds_t;
typedef long __suseconds_t;
typedef int __daddr_t;
typedef int __key_t;
typedef int __clockid_t;
typedef void *__timer_t;
typedef long __blksize_t;
typedef long __blkcnt_t;
typedef long __blkcnt64_t;
typedef unsigned long __fsblkcnt_t;
typedef unsigned long __fsblkcnt64_t;
typedef unsigned long __fsfilcnt_t;
typedef unsigned long __fsfilcnt64_t;
typedef long __fsword_t;
typedef long __ssize_t;
typedef long __syscall_slong_t;
typedef unsigned long __syscall_ulong_t;
typedef __off64_t __loff_t;
typedef char *__caddr_t;
typedef long __intptr_t;
typedef unsigned int __socklen_t;
typedef int __sig_atomic_t;
typedef __int8_t int8_t;
typedef __int16_t int16_t;
typedef __int32_t int32_t;
typedef __int64_t int64_t;
typedef __uint8_t uint8_t;
typedef __uint16_t uint16_t;
typedef __uint32_t uint32_t;
typedef __uint64_t uint64_t;
typedef __int_least8_t int_least8_t;
typedef __int_least16_t int_least16_t;
typedef __int_least32_t int_least32_t;
typedef __int_least64_t int_least64_t;
typedef __uint_least8_t uint_least8_t;
typedef __uint_least16_t uint_least16_t;
typedef __uint_least32_t uint_least32_t;
typedef __uint_least64_t uint_least64_t;
typedef signed char int_fast8_t;
typedef long int_fast16_t;
typedef long int_fast32_t;
typedef long int_fast64_t;
typedef unsigned char uint_fast8_t;
typedef unsigned long uint_fast16_t;
typedef unsigned long uint_fast32_t;
typedef unsigned long uint_fast64_t;
typedef long intptr_t;
typedef unsigned long uintptr_t;
typedef __intmax_t intmax_t;
typedef __uintmax_t uintmax_t;
typedef _Bool bool;
struct bb_process_file_ret_type;
struct indirect_placeholder_93_ret_type;
struct indirect_placeholder_92_ret_type;
struct indirect_placeholder_97_ret_type;
struct indirect_placeholder_98_ret_type;
struct indirect_placeholder_96_ret_type;
struct indirect_placeholder_99_ret_type;
struct indirect_placeholder_94_ret_type;
struct indirect_placeholder_95_ret_type;
struct indirect_placeholder_101_ret_type;
struct indirect_placeholder_100_ret_type;
struct indirect_placeholder_102_ret_type;
struct indirect_placeholder_103_ret_type;
struct indirect_placeholder_104_ret_type;
struct indirect_placeholder_105_ret_type;
struct bb_process_file_ret_type {
    uint64_t field_0;
    uint64_t field_1;
    uint64_t field_2;
};
struct indirect_placeholder_93_ret_type {
    uint64_t field_0;
    uint64_t field_1;
    uint64_t field_2;
};
struct indirect_placeholder_92_ret_type {
    uint64_t field_0;
    uint64_t field_1;
    uint64_t field_2;
};
struct indirect_placeholder_97_ret_type {
    uint64_t field_0;
    uint64_t field_1;
    uint64_t field_2;
};
struct indirect_placeholder_98_ret_type {
    uint64_t field_0;
    uint64_t field_1;
    uint64_t field_2;
};
struct indirect_placeholder_96_ret_type {
    uint64_t field_0;
    uint64_t field_1;
    uint64_t field_2;
};
struct indirect_placeholder_99_ret_type {
    uint64_t field_0;
    uint64_t field_1;
    uint64_t field_2;
};
struct indirect_placeholder_94_ret_type {
    uint64_t field_0;
    uint64_t field_1;
    uint64_t field_2;
};
struct indirect_placeholder_95_ret_type {
    uint64_t field_0;
    uint64_t field_1;
    uint64_t field_2;
};
struct indirect_placeholder_101_ret_type {
    uint64_t field_0;
    uint64_t field_1;
    uint64_t field_2;
};
struct indirect_placeholder_100_ret_type {
    uint64_t field_0;
    uint64_t field_1;
    uint64_t field_2;
};
struct indirect_placeholder_102_ret_type {
    uint64_t field_0;
    uint64_t field_1;
    uint64_t field_2;
};
struct indirect_placeholder_103_ret_type {
    uint64_t field_0;
    uint64_t field_1;
};
struct indirect_placeholder_104_ret_type {
    uint64_t field_0;
    uint64_t field_1;
};
struct indirect_placeholder_105_ret_type {
    uint64_t field_0;
    uint64_t field_1;
};
extern uint64_t revng_init_local_sp(uint64_t param_0);
extern uint64_t init_rbp(void);
extern uint64_t indirect_placeholder_5(uint64_t param_0, uint64_t param_1);
extern uint64_t init_cc_src2(void);
extern uint64_t helper_cc_compute_c_wrapper(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint32_t param_3);
extern void indirect_placeholder(void);
extern uint64_t indirect_placeholder_8(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint64_t param_3, uint64_t param_4);
extern uint64_t indirect_placeholder_12(uint64_t param_0, uint64_t param_1, uint64_t param_2);
extern uint64_t init_rbx(void);
extern uint64_t init_r10(void);
extern uint64_t indirect_placeholder_24(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint64_t param_3);
extern void indirect_placeholder_21(uint64_t param_0);
extern uint64_t indirect_placeholder_3(uint64_t param_0);
extern void indirect_placeholder_15(uint64_t param_0, uint64_t param_1);
extern uint64_t helper_cc_compute_all_wrapper(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint32_t param_3);
extern uint64_t indirect_placeholder_90(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint64_t param_3, uint64_t param_4, uint64_t param_5, uint64_t param_6);
extern uint64_t indirect_placeholder_29(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint64_t param_3, uint64_t param_4, uint64_t param_5, uint64_t param_6, uint64_t param_7);
extern struct indirect_placeholder_93_ret_type indirect_placeholder_93(uint64_t param_0, uint64_t param_1, uint64_t param_2);
extern struct indirect_placeholder_92_ret_type indirect_placeholder_92(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint64_t param_3, uint64_t param_4, uint64_t param_5, uint64_t param_6);
extern struct indirect_placeholder_97_ret_type indirect_placeholder_97(uint64_t param_0, uint64_t param_1, uint64_t param_2);
extern struct indirect_placeholder_98_ret_type indirect_placeholder_98(uint64_t param_0, uint64_t param_1);
extern struct indirect_placeholder_96_ret_type indirect_placeholder_96(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint64_t param_3, uint64_t param_4, uint64_t param_5, uint64_t param_6);
extern struct indirect_placeholder_99_ret_type indirect_placeholder_99(uint64_t param_0, uint64_t param_1, uint64_t param_2);
extern struct indirect_placeholder_94_ret_type indirect_placeholder_94(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint64_t param_3, uint64_t param_4, uint64_t param_5, uint64_t param_6);
extern struct indirect_placeholder_95_ret_type indirect_placeholder_95(uint64_t param_0, uint64_t param_1, uint64_t param_2);
extern struct indirect_placeholder_101_ret_type indirect_placeholder_101(uint64_t param_0, uint64_t param_1);
extern struct indirect_placeholder_100_ret_type indirect_placeholder_100(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint64_t param_3, uint64_t param_4, uint64_t param_5, uint64_t param_6);
extern struct indirect_placeholder_102_ret_type indirect_placeholder_102(uint64_t param_0, uint64_t param_1);
extern struct indirect_placeholder_103_ret_type indirect_placeholder_103(uint64_t param_0);
extern struct indirect_placeholder_104_ret_type indirect_placeholder_104(uint64_t param_0);
extern struct indirect_placeholder_105_ret_type indirect_placeholder_105(uint64_t param_0);
struct bb_process_file_ret_type bb_process_file(uint64_t rsi, uint64_t rdi, uint64_t r8, uint64_t r9) {
    uint64_t var_133;
    uint64_t var_66;
    uint64_t var_0;
    uint64_t var_1;
    uint64_t var_2;
    uint64_t var_3;
    uint64_t var_4;
    uint64_t var_5;
    uint64_t *var_6;
    uint64_t var_7;
    uint64_t *var_8;
    unsigned char *var_9;
    uint64_t var_10;
    uint64_t *var_11;
    uint64_t var_12;
    uint64_t var_13;
    uint64_t *var_14;
    uint16_t var_15;
    uint32_t *var_16;
    uint32_t var_17;
    uint64_t r85_4;
    uint64_t var_138;
    uint64_t var_143;
    uint64_t var_144;
    uint64_t _pre255;
    uint64_t var_136;
    uint64_t var_137;
    uint64_t storemerge;
    uint64_t var_139;
    uint64_t var_140;
    uint64_t var_141;
    uint64_t storemerge3;
    uint64_t var_142;
    uint64_t r85_3;
    uint64_t var_111;
    uint64_t var_112;
    uint64_t var_108;
    uint64_t local_sp_0;
    uint64_t var_113;
    uint64_t var_114;
    uint64_t var_115;
    uint64_t var_116;
    uint64_t var_117;
    uint64_t var_118;
    uint64_t var_119;
    uint64_t local_sp_4;
    uint64_t var_131;
    uint64_t var_132;
    uint64_t r96_4;
    uint64_t local_sp_12;
    uint64_t var_105;
    uint64_t var_106;
    uint64_t var_123;
    uint64_t var_120;
    uint64_t var_121;
    uint64_t var_122;
    uint64_t local_sp_1;
    uint64_t var_124;
    uint64_t *var_125;
    uint64_t var_126;
    uint64_t local_sp_2;
    uint64_t var_127;
    uint64_t var_128;
    uint64_t var_129;
    uint64_t var_130;
    uint64_t var_107;
    uint64_t local_sp_3;
    uint64_t var_109;
    uint64_t var_110;
    uint64_t local_sp_5;
    uint64_t var_134;
    uint64_t var_135;
    uint64_t rdx_0;
    uint64_t rax_0;
    uint64_t local_sp_6;
    bool var_94;
    uint64_t var_95;
    uint64_t var_96;
    uint64_t rsi3_0;
    uint64_t var_97;
    uint64_t var_98;
    uint64_t var_99;
    uint64_t var_100;
    uint64_t *var_101;
    uint64_t var_102;
    uint64_t var_103;
    uint64_t var_104;
    uint64_t var_148;
    struct indirect_placeholder_93_ret_type var_149;
    uint64_t var_150;
    uint64_t var_151;
    uint64_t var_152;
    struct indirect_placeholder_92_ret_type var_153;
    uint64_t var_154;
    uint64_t var_155;
    uint64_t var_146;
    uint64_t var_147;
    uint64_t var_46;
    uint64_t var_47;
    uint64_t local_sp_7;
    uint32_t var_48;
    uint32_t var_49;
    uint32_t var_37;
    uint64_t var_38;
    uint64_t var_39;
    uint64_t var_40;
    uint64_t var_41;
    struct indirect_placeholder_97_ret_type var_42;
    uint64_t var_43;
    uint64_t var_44;
    uint64_t var_45;
    uint64_t r85_0;
    uint64_t r96_0;
    uint64_t local_sp_8;
    uint64_t r85_1;
    uint64_t var_59;
    uint64_t var_50;
    struct indirect_placeholder_98_ret_type var_51;
    uint64_t var_52;
    uint64_t var_53;
    uint64_t var_54;
    uint64_t var_55;
    struct indirect_placeholder_96_ret_type var_56;
    uint64_t var_57;
    uint64_t var_58;
    uint64_t r96_1;
    uint64_t local_sp_9;
    uint64_t local_sp_11;
    uint64_t local_sp_10;
    uint64_t var_61;
    uint64_t _pre252;
    uint64_t var_60;
    uint64_t var_62;
    uint64_t var_63;
    uint64_t var_64;
    uint64_t var_65;
    uint32_t var_74;
    uint64_t var_75;
    struct indirect_placeholder_99_ret_type var_76;
    uint64_t var_77;
    uint64_t var_78;
    uint64_t var_79;
    uint64_t var_80;
    uint64_t var_81;
    struct indirect_placeholder_94_ret_type var_82;
    uint64_t var_83;
    uint64_t var_84;
    uint64_t var_145;
    uint64_t var_67;
    uint64_t var_68;
    uint64_t var_69;
    struct indirect_placeholder_95_ret_type var_70;
    uint64_t var_71;
    uint64_t var_72;
    uint64_t var_73;
    uint64_t r96_3;
    uint64_t rax_1;
    struct bb_process_file_ret_type mrv;
    struct bb_process_file_ret_type mrv1;
    struct bb_process_file_ret_type mrv2;
    uint64_t var_18;
    struct indirect_placeholder_101_ret_type var_19;
    uint64_t var_20;
    uint64_t var_21;
    uint64_t var_22;
    uint64_t var_23;
    uint64_t var_24;
    struct indirect_placeholder_100_ret_type var_25;
    uint64_t var_26;
    uint64_t var_27;
    uint64_t var_28;
    uint64_t var_29;
    uint64_t var_30;
    struct indirect_placeholder_102_ret_type var_31;
    uint64_t var_32;
    uint64_t var_33;
    uint64_t var_34;
    unsigned char *var_35;
    unsigned char var_36;
    uint64_t var_88;
    uint64_t var_89;
    struct indirect_placeholder_103_ret_type var_90;
    uint64_t var_85;
    uint64_t var_86;
    struct indirect_placeholder_104_ret_type var_87;
    uint64_t var_91;
    uint64_t var_92;
    struct indirect_placeholder_105_ret_type var_93;
    switch (var_17) {
      case 4U:
        {
            var_18 = *var_11;
            *(uint64_t *)(var_0 + (-176L)) = 4207314UL;
            var_19 = indirect_placeholder_101(var_18, 4UL);
            var_20 = var_19.field_0;
            var_21 = var_19.field_1;
            var_22 = var_19.field_2;
            var_23 = (uint64_t)*(uint32_t *)(*var_8 + 64UL);
            var_24 = var_0 + (-184L);
            *(uint64_t *)var_24 = 4207352UL;
            var_25 = indirect_placeholder_100(0UL, 4436355UL, var_20, var_23, 0UL, var_21, var_22);
            var_26 = var_25.field_1;
            var_27 = var_25.field_2;
            *var_9 = (unsigned char)'\x00';
            r85_4 = var_26;
            r96_4 = var_27;
            local_sp_12 = var_24;
        }
        break;
      case 6U:
        {
            r85_3 = r85_4;
            r96_3 = r96_4;
            switch (*(uint32_t *)6574352UL) {
              case 0U:
                {
                    var_88 = *var_14;
                    var_89 = local_sp_12 + (-8L);
                    *(uint64_t *)var_89 = 4208124UL;
                    var_90 = indirect_placeholder_103(var_88);
                    rdx_0 = var_90.field_1;
                    rax_0 = var_90.field_0;
                    local_sp_6 = var_89;
                }
                break;
              case 2U:
                {
                    var_85 = *var_14;
                    var_86 = local_sp_12 + (-8L);
                    *(uint64_t *)var_86 = 4208149UL;
                    var_87 = indirect_placeholder_104(var_85);
                    rdx_0 = var_87.field_1;
                    rax_0 = var_87.field_0;
                    local_sp_6 = var_86;
                }
                break;
              default:
                {
                    var_91 = *var_14;
                    var_92 = local_sp_12 + (-8L);
                    *(uint64_t *)var_92 = 4208163UL;
                    var_93 = indirect_placeholder_105(var_91);
                    rdx_0 = var_93.field_1;
                    rax_0 = var_93.field_0;
                    local_sp_6 = var_92;
                }
                break;
            }
            var_94 = (*(unsigned char *)6574329UL == '\x00');
            var_95 = *var_14;
            if (var_94) {
                rsi3_0 = *(uint64_t *)(var_95 + 64UL) << 9UL;
            } else {
                var_96 = *(uint64_t *)(var_95 + 48UL);
                rsi3_0 = ((long)var_96 > (long)0UL) ? var_96 : 0UL;
            }
            var_97 = var_0 + (-120L);
            var_98 = local_sp_6 + (-8L);
            *(uint64_t *)var_98 = 4208229UL;
            var_99 = indirect_placeholder_24(rax_0, rdx_0, rsi3_0, var_97);
            var_100 = *(uint64_t *)(*var_8 + 88UL);
            var_101 = (uint64_t *)(var_0 + (-72L));
            *var_101 = var_100;
            var_102 = *(uint64_t *)var_97;
            var_103 = var_0 + (-152L);
            *(uint64_t *)var_103 = var_102;
            *(uint64_t *)(var_0 + (-144L)) = *(uint64_t *)(var_0 + (-112L));
            *(uint64_t *)(var_0 + (-136L)) = *(uint64_t *)(var_0 + (-104L));
            *(uint64_t *)(var_0 + (-128L)) = *(uint64_t *)(var_0 + (-96L));
            var_104 = *(uint64_t *)6574432UL;
            local_sp_1 = var_98;
            local_sp_3 = var_98;
            local_sp_4 = var_98;
            if (var_104 == 0UL) {
                var_131 = *var_101 + 10UL;
                *(uint64_t *)6574432UL = var_131;
                var_132 = local_sp_6 + (-16L);
                *(uint64_t *)var_132 = 4208329UL;
                var_133 = indirect_placeholder_90(var_99, 64UL, var_131, r85_4, r96_4, var_3, var_4);
                *(uint64_t *)6574440UL = var_133;
                local_sp_4 = var_132;
            } else {
                var_105 = *(uint64_t *)6574320UL;
                var_106 = *var_101;
                var_123 = var_105;
                var_108 = var_105;
                if (var_106 != var_105) {
                    if (var_106 > var_105) {
                        if (var_104 > var_106) {
                            var_120 = *(uint64_t *)6574440UL;
                            var_121 = local_sp_6 + (-16L);
                            *(uint64_t *)var_121 = 4208415UL;
                            var_122 = indirect_placeholder_29(128UL, var_106, var_106, var_120, r85_4, r96_4, var_3, var_4);
                            *(uint64_t *)6574440UL = var_122;
                            *(uint64_t *)6574432UL = (*var_101 << 1UL);
                            var_123 = *(uint64_t *)6574320UL;
                            local_sp_1 = var_121;
                        }
                        var_124 = var_123 + 1UL;
                        var_125 = (uint64_t *)(var_0 + (-32L));
                        *var_125 = var_124;
                        var_126 = var_124;
                        local_sp_2 = local_sp_1;
                        local_sp_4 = local_sp_2;
                        while (var_126 <= *var_101)
                            {
                                var_127 = *(uint64_t *)6574440UL + (var_126 << 6UL);
                                *(uint64_t *)(local_sp_2 + (-8L)) = 4208479UL;
                                indirect_placeholder_21(var_127);
                                var_128 = (*(uint64_t *)6574440UL + (*var_125 << 6UL)) + 32UL;
                                var_129 = local_sp_2 + (-16L);
                                *(uint64_t *)var_129 = 4208509UL;
                                indirect_placeholder_21(var_128);
                                var_130 = *var_125 + 1UL;
                                *var_125 = var_130;
                                var_126 = var_130;
                                local_sp_2 = var_129;
                                local_sp_4 = local_sp_2;
                            }
                    }
                    if ((var_105 + (-1L)) == var_106) {
                        var_107 = local_sp_6 + (-16L);
                        *(uint64_t *)var_107 = 4208571UL;
                        indirect_placeholder();
                        var_108 = *(uint64_t *)6574320UL;
                        local_sp_3 = var_107;
                    }
                    var_109 = *(uint64_t *)6574440UL + (var_108 << 6UL);
                    var_110 = local_sp_3 + (-8L);
                    *(uint64_t *)var_110 = 4208616UL;
                    indirect_placeholder_15(var_109, var_103);
                    local_sp_0 = var_110;
                    if (*(unsigned char *)6574334UL != '\x01') {
                        var_111 = (*(uint64_t *)6574440UL + (*(uint64_t *)6574320UL << 6UL)) + 32UL;
                        var_112 = local_sp_3 + (-16L);
                        *(uint64_t *)var_112 = 4208673UL;
                        indirect_placeholder_15(var_111, var_103);
                        local_sp_0 = var_112;
                    }
                    var_113 = *(uint64_t *)6574440UL;
                    var_114 = var_113 + (*(uint64_t *)6574320UL << 6UL);
                    var_115 = (var_113 + (*var_101 << 6UL)) + 32UL;
                    *(uint64_t *)(local_sp_0 + (-8L)) = 4208730UL;
                    indirect_placeholder_15(var_114, var_115);
                    var_116 = *(uint64_t *)6574440UL;
                    var_117 = (var_116 + (*(uint64_t *)6574320UL << 6UL)) + 32UL;
                    var_118 = (var_116 + (*var_101 << 6UL)) + 32UL;
                    var_119 = local_sp_0 + (-16L);
                    *(uint64_t *)var_119 = 4208788UL;
                    indirect_placeholder_15(var_117, var_118);
                    local_sp_4 = var_119;
                }
            }
            *(uint64_t *)6574320UL = *var_101;
            local_sp_5 = local_sp_4;
            if (*(unsigned char *)6574334UL != '\x01') {
                var_134 = *(uint64_t *)6574440UL + (*var_101 << 6UL);
                var_135 = local_sp_4 + (-8L);
                *(uint64_t *)var_135 = 4208861UL;
                indirect_placeholder_15(var_97, var_134);
                local_sp_5 = var_135;
                if (*var_101 <= *(uint64_t *)6573952UL) {
                    if (*(unsigned char *)6574328UL != '\x00') {
                        var_136 = *(uint64_t *)6573952UL;
                        var_137 = *var_101;
                        var_138 = var_137;
                        if (var_137 <= var_136 & var_138 == 0UL) {
                            var_142 = (uint64_t)*var_9;
                            rax_1 = var_142;
                            mrv.field_0 = rax_1;
                            mrv1 = mrv;
                            mrv1.field_1 = r85_3;
                            mrv2 = mrv1;
                            mrv2.field_2 = r96_3;
                            return mrv2;
                        }
                    }
                    _pre255 = *var_101;
                    var_138 = _pre255;
                    if (var_138 != 0UL) {
                        var_142 = (uint64_t)*var_9;
                        rax_1 = var_142;
                        mrv.field_0 = rax_1;
                        mrv1 = mrv;
                        mrv1.field_1 = r85_3;
                        mrv2 = mrv1;
                        mrv2.field_2 = r96_3;
                        return mrv2;
                    }
                }
                storemerge = *(uint64_t *)(((*(unsigned char *)6574348UL == '\x00') ? var_5 : var_0) + (-144L));
                *(uint64_t *)(var_0 + (-80L)) = storemerge;
                var_139 = *(uint64_t *)6574336UL;
                if ((long)var_139 > (long)18446744073709551615UL) {
                    var_140 = 0UL - var_139;
                    var_141 = helper_cc_compute_c_wrapper(var_140 - storemerge, storemerge, var_2, 17U);
                    storemerge3 = ((uint64_t)(unsigned char)var_141 | (var_140 & (-256L))) ^ 1UL;
                } else {
                    storemerge3 = (var_139 & (-256L)) | (var_139 <= storemerge);
                }
                if ((uint64_t)(unsigned char)storemerge3 == 0UL) {
                    *(uint64_t *)(local_sp_5 + (-16L)) = 4209036UL;
                    indirect_placeholder_21(var_103);
                }
                var_142 = (uint64_t)*var_9;
                rax_1 = var_142;
                mrv.field_0 = rax_1;
                mrv1 = mrv;
                mrv1.field_1 = r85_3;
                mrv2 = mrv1;
                mrv2.field_2 = r96_3;
                return mrv2;
            }
            break;
        }
        break;
    }
}

typedef unsigned char __u_char;
typedef unsigned short __u_short;
typedef unsigned int __u_int;
typedef unsigned long __u_long;
typedef signed char __int8_t;
typedef unsigned char __uint8_t;
typedef short __int16_t;
typedef unsigned short __uint16_t;
typedef int __int32_t;
typedef unsigned int __uint32_t;
typedef long __int64_t;
typedef unsigned long __uint64_t;
typedef __int8_t __int_least8_t;
typedef __uint8_t __uint_least8_t;
typedef __int16_t __int_least16_t;
typedef __uint16_t __uint_least16_t;
typedef __int32_t __int_least32_t;
typedef __uint32_t __uint_least32_t;
typedef __int64_t __int_least64_t;
typedef __uint64_t __uint_least64_t;
typedef long __quad_t;
typedef unsigned long __u_quad_t;
typedef long __intmax_t;
typedef unsigned long __uintmax_t;
typedef unsigned long __dev_t;
typedef unsigned int __uid_t;
typedef unsigned int __gid_t;
typedef unsigned long __ino_t;
typedef unsigned long __ino64_t;
typedef unsigned int __mode_t;
typedef unsigned long __nlink_t;
typedef long __off_t;
typedef long __off64_t;
typedef int __pid_t;
typedef struct {
    int __val[2];
} __fsid_t;
typedef long __clock_t;
typedef unsigned long __rlim_t;
typedef unsigned long __rlim64_t;
typedef unsigned int __id_t;
typedef long __time_t;
typedef unsigned int __useconds_t;
typedef long __suseconds_t;
typedef int __daddr_t;
typedef int __key_t;
typedef int __clockid_t;
typedef void *__timer_t;
typedef long __blksize_t;
typedef long __blkcnt_t;
typedef long __blkcnt64_t;
typedef unsigned long __fsblkcnt_t;
typedef unsigned long __fsblkcnt64_t;
typedef unsigned long __fsfilcnt_t;
typedef unsigned long __fsfilcnt64_t;
typedef long __fsword_t;
typedef long __ssize_t;
typedef long __syscall_slong_t;
typedef unsigned long __syscall_ulong_t;
typedef __off64_t __loff_t;
typedef char *__caddr_t;
typedef long __intptr_t;
typedef unsigned int __socklen_t;
typedef int __sig_atomic_t;
typedef __int8_t int8_t;
typedef __int16_t int16_t;
typedef __int32_t int32_t;
typedef __int64_t int64_t;
typedef __uint8_t uint8_t;
typedef __uint16_t uint16_t;
typedef __uint32_t uint32_t;
typedef __uint64_t uint64_t;
typedef __int_least8_t int_least8_t;
typedef __int_least16_t int_least16_t;
typedef __int_least32_t int_least32_t;
typedef __int_least64_t int_least64_t;
typedef __uint_least8_t uint_least8_t;
typedef __uint_least16_t uint_least16_t;
typedef __uint_least32_t uint_least32_t;
typedef __uint_least64_t uint_least64_t;
typedef signed char int_fast8_t;
typedef long int_fast16_t;
typedef long int_fast32_t;
typedef long int_fast64_t;
typedef unsigned char uint_fast8_t;
typedef unsigned long uint_fast16_t;
typedef unsigned long uint_fast32_t;
typedef unsigned long uint_fast64_t;
typedef long intptr_t;
typedef unsigned long uintptr_t;
typedef __intmax_t intmax_t;
typedef __uintmax_t uintmax_t;
typedef _Bool bool;
struct indirect_placeholder_15_ret_type;
struct indirect_placeholder_17_ret_type;
struct indirect_placeholder_18_ret_type;
struct indirect_placeholder_19_ret_type;
struct indirect_placeholder_20_ret_type;
struct indirect_placeholder_21_ret_type;
struct indirect_placeholder_22_ret_type;
struct indirect_placeholder_14_ret_type;
struct indirect_placeholder_23_ret_type;
struct indirect_placeholder_26_ret_type;
struct indirect_placeholder_27_ret_type;
struct indirect_placeholder_30_ret_type;
struct indirect_placeholder_31_ret_type;
struct indirect_placeholder_29_ret_type;
struct indirect_placeholder_32_ret_type;
struct indirect_placeholder_33_ret_type;
struct indirect_placeholder_34_ret_type;
struct indirect_placeholder_36_ret_type;
struct indirect_placeholder_35_ret_type;
struct indirect_placeholder_38_ret_type;
struct indirect_placeholder_37_ret_type;
struct indirect_placeholder_39_ret_type;
struct indirect_placeholder_41_ret_type;
struct indirect_placeholder_15_ret_type {
    uint64_t field_0;
    uint64_t field_1;
    uint64_t field_2;
    uint64_t field_3;
};
struct indirect_placeholder_17_ret_type {
    uint64_t field_0;
    uint64_t field_1;
    uint64_t field_2;
    uint64_t field_3;
};
struct indirect_placeholder_18_ret_type {
    uint64_t field_0;
    uint64_t field_1;
    uint64_t field_2;
    uint64_t field_3;
};
struct indirect_placeholder_19_ret_type {
    uint64_t field_0;
    uint64_t field_1;
    uint64_t field_2;
    uint64_t field_3;
    uint64_t field_4;
    uint64_t field_5;
};
struct indirect_placeholder_20_ret_type {
    uint64_t field_0;
    uint64_t field_1;
};
struct indirect_placeholder_21_ret_type {
    uint64_t field_0;
    uint64_t field_1;
    uint64_t field_2;
    uint64_t field_3;
    uint64_t field_4;
    uint64_t field_5;
};
struct indirect_placeholder_22_ret_type {
    uint64_t field_0;
    uint64_t field_1;
};
struct indirect_placeholder_14_ret_type {
    uint64_t field_0;
    uint64_t field_1;
    uint64_t field_2;
    uint64_t field_3;
};
struct indirect_placeholder_23_ret_type {
    uint64_t field_0;
    uint64_t field_1;
    uint64_t field_2;
    uint64_t field_3;
};
struct indirect_placeholder_26_ret_type {
    uint64_t field_0;
    uint64_t field_1;
    uint64_t field_2;
    uint64_t field_3;
};
struct indirect_placeholder_27_ret_type {
    uint64_t field_0;
    uint64_t field_1;
};
struct indirect_placeholder_30_ret_type {
    uint64_t field_0;
    uint64_t field_1;
    uint64_t field_2;
    uint64_t field_3;
};
struct indirect_placeholder_31_ret_type {
    uint64_t field_0;
    uint64_t field_1;
    uint64_t field_2;
};
struct indirect_placeholder_29_ret_type {
    uint64_t field_0;
    uint64_t field_1;
    uint64_t field_2;
    uint64_t field_3;
};
struct indirect_placeholder_32_ret_type {
    uint64_t field_0;
    uint64_t field_1;
    uint64_t field_2;
    uint64_t field_3;
    uint64_t field_4;
};
struct indirect_placeholder_33_ret_type {
    uint64_t field_0;
    uint64_t field_1;
    uint64_t field_2;
    uint64_t field_3;
};
struct indirect_placeholder_34_ret_type {
    uint64_t field_0;
    uint64_t field_1;
    uint64_t field_2;
    uint64_t field_3;
};
struct indirect_placeholder_36_ret_type {
    uint64_t field_0;
    uint64_t field_1;
    uint64_t field_2;
};
struct indirect_placeholder_35_ret_type {
    uint64_t field_0;
    uint64_t field_1;
    uint64_t field_2;
    uint64_t field_3;
};
struct indirect_placeholder_38_ret_type {
    uint64_t field_0;
    uint64_t field_1;
    uint64_t field_2;
};
struct indirect_placeholder_37_ret_type {
    uint64_t field_0;
    uint64_t field_1;
    uint64_t field_2;
    uint64_t field_3;
};
struct indirect_placeholder_39_ret_type {
    uint64_t field_0;
    uint64_t field_1;
    uint64_t field_2;
    uint64_t field_3;
};
struct indirect_placeholder_41_ret_type {
    uint64_t field_0;
    uint64_t field_1;
    uint64_t field_2;
    uint64_t field_3;
};
extern void abort(void);
extern uint64_t revng_init_local_sp(uint64_t param_0);
extern uint64_t init_rbp(void);
extern uint64_t indirect_placeholder_6(uint64_t param_0, uint64_t param_1);
extern uint64_t init_cc_src2(void);
extern void indirect_placeholder(void);
extern uint64_t indirect_placeholder_9(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint64_t param_3, uint64_t param_4);
extern uint64_t indirect_placeholder_16(uint64_t param_0, uint64_t param_1, uint64_t param_2);
extern uint64_t init_rbx(void);
extern void indirect_placeholder_28(uint64_t param_0, uint64_t param_1);
extern uint64_t helper_cc_compute_all_wrapper(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint32_t param_3);
extern uint64_t indirect_placeholder_25(void);
extern void indirect_placeholder_4(uint64_t param_0);
extern uint64_t indirect_placeholder_3(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint64_t param_3);
extern uint64_t indirect_placeholder_12(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint64_t param_3, uint64_t param_4, uint64_t param_5);
extern void indirect_placeholder_5(uint64_t param_0, uint64_t param_1, uint64_t param_2);
extern uint64_t indirect_placeholder_24(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint64_t param_3, uint64_t param_4, uint64_t param_5, uint64_t param_6, uint64_t param_7);
extern struct indirect_placeholder_15_ret_type indirect_placeholder_15(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint64_t param_3, uint64_t param_4, uint64_t param_5, uint64_t param_6);
extern struct indirect_placeholder_17_ret_type indirect_placeholder_17(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint64_t param_3, uint64_t param_4, uint64_t param_5, uint64_t param_6);
extern struct indirect_placeholder_18_ret_type indirect_placeholder_18(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint64_t param_3, uint64_t param_4, uint64_t param_5, uint64_t param_6);
extern struct indirect_placeholder_19_ret_type indirect_placeholder_19(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint64_t param_3, uint64_t param_4, uint64_t param_5, uint64_t param_6, uint64_t param_7);
extern struct indirect_placeholder_20_ret_type indirect_placeholder_20(void);
extern struct indirect_placeholder_21_ret_type indirect_placeholder_21(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint64_t param_3, uint64_t param_4, uint64_t param_5, uint64_t param_6, uint64_t param_7);
extern struct indirect_placeholder_22_ret_type indirect_placeholder_22(uint64_t param_0, uint64_t param_1, uint64_t param_2);
extern struct indirect_placeholder_14_ret_type indirect_placeholder_14(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint64_t param_3, uint64_t param_4, uint64_t param_5, uint64_t param_6);
extern struct indirect_placeholder_23_ret_type indirect_placeholder_23(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint64_t param_3, uint64_t param_4, uint64_t param_5, uint64_t param_6);
extern struct indirect_placeholder_26_ret_type indirect_placeholder_26(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint64_t param_3, uint64_t param_4, uint64_t param_5, uint64_t param_6);
extern struct indirect_placeholder_27_ret_type indirect_placeholder_27(uint64_t param_0, uint64_t param_1, uint64_t param_2);
extern struct indirect_placeholder_30_ret_type indirect_placeholder_30(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint64_t param_3, uint64_t param_4, uint64_t param_5, uint64_t param_6);
extern struct indirect_placeholder_31_ret_type indirect_placeholder_31(uint64_t param_0);
extern struct indirect_placeholder_29_ret_type indirect_placeholder_29(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint64_t param_3, uint64_t param_4, uint64_t param_5, uint64_t param_6);
extern struct indirect_placeholder_32_ret_type indirect_placeholder_32(uint64_t param_0, uint64_t param_1);
extern struct indirect_placeholder_33_ret_type indirect_placeholder_33(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint64_t param_3, uint64_t param_4, uint64_t param_5, uint64_t param_6);
extern struct indirect_placeholder_34_ret_type indirect_placeholder_34(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint64_t param_3, uint64_t param_4, uint64_t param_5, uint64_t param_6);
extern struct indirect_placeholder_36_ret_type indirect_placeholder_36(uint64_t param_0);
extern struct indirect_placeholder_35_ret_type indirect_placeholder_35(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint64_t param_3, uint64_t param_4, uint64_t param_5, uint64_t param_6);
extern struct indirect_placeholder_38_ret_type indirect_placeholder_38(uint64_t param_0);
extern struct indirect_placeholder_37_ret_type indirect_placeholder_37(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint64_t param_3, uint64_t param_4, uint64_t param_5, uint64_t param_6);
extern struct indirect_placeholder_39_ret_type indirect_placeholder_39(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint64_t param_3, uint64_t param_4, uint64_t param_5, uint64_t param_6);
extern void indirect_placeholder_40(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint64_t param_3, uint64_t param_4, uint64_t param_5);
extern struct indirect_placeholder_41_ret_type indirect_placeholder_41(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint64_t param_3, uint64_t param_4);
void bb_main(uint64_t rsi, uint64_t rdi) {
    struct indirect_placeholder_41_ret_type var_38;
    struct indirect_placeholder_15_ret_type var_141;
    uint64_t var_210;
    uint64_t r8_0;
    uint64_t var_0;
    uint64_t var_1;
    uint64_t var_2;
    uint64_t var_3;
    uint64_t var_4;
    uint32_t *var_5;
    uint64_t var_6;
    uint64_t *var_7;
    unsigned char *var_8;
    unsigned char *var_9;
    uint64_t *var_10;
    uint64_t *var_11;
    uint64_t *var_12;
    uint64_t *var_13;
    uint64_t *var_14;
    unsigned char *var_15;
    uint64_t var_16;
    uint64_t *var_17;
    unsigned char *var_18;
    unsigned char *var_19;
    uint64_t *var_20;
    uint64_t var_21;
    uint64_t *var_22;
    uint64_t *var_23;
    uint64_t var_24;
    uint64_t var_25;
    uint32_t *var_26;
    uint64_t var_27;
    uint32_t *var_28;
    uint64_t *var_29;
    uint64_t var_30;
    uint64_t *var_31;
    uint64_t *var_32;
    unsigned char *var_33;
    uint64_t *var_34;
    uint64_t rbx_7_ph;
    uint64_t rcx_3;
    uint64_t var_137;
    uint64_t var_138;
    uint64_t var_139;
    uint64_t var_140;
    uint64_t r8_3;
    uint64_t rbx_2;
    uint64_t r10_4;
    uint64_t rcx_1;
    uint64_t var_117;
    uint64_t var_118;
    uint64_t r8_1;
    uint64_t rbx_1;
    struct indirect_placeholder_17_ret_type var_119;
    uint64_t r10_2;
    uint64_t storemerge8;
    uint64_t var_96;
    uint64_t var_97;
    uint64_t var_98;
    uint64_t var_99;
    struct indirect_placeholder_18_ret_type var_100;
    uint64_t local_sp_10;
    uint64_t r10_0;
    uint64_t r9_0;
    uint64_t local_sp_9;
    uint64_t rbx_0;
    uint64_t local_sp_0;
    uint64_t rcx_0;
    uint64_t r10_1;
    uint32_t var_180;
    uint64_t var_101;
    uint64_t var_102;
    uint64_t var_103;
    uint64_t var_104;
    uint64_t var_105;
    struct indirect_placeholder_19_ret_type var_106;
    uint64_t var_107;
    uint64_t var_108;
    uint64_t var_109;
    uint64_t var_110;
    uint64_t var_111;
    uint64_t var_112;
    uint64_t r9_1;
    uint64_t local_sp_1;
    uint64_t var_78;
    uint64_t var_113;
    struct indirect_placeholder_20_ret_type var_114;
    uint64_t var_115;
    uint64_t var_116;
    uint64_t r9_2;
    uint64_t r8_2;
    uint64_t local_sp_2;
    uint64_t rcx_2;
    unsigned char var_120;
    uint64_t r10_3;
    uint64_t var_121;
    uint64_t var_122;
    uint64_t var_123;
    uint64_t var_124;
    struct indirect_placeholder_21_ret_type var_125;
    uint64_t var_126;
    uint64_t var_127;
    uint64_t var_128;
    uint64_t var_129;
    uint64_t var_130;
    uint64_t var_131;
    uint64_t r9_3;
    uint64_t rax_0;
    uint64_t local_sp_3;
    uint64_t var_132;
    uint64_t var_133;
    uint64_t var_134;
    struct indirect_placeholder_22_ret_type var_135;
    uint64_t var_136;
    uint64_t r9_4;
    uint64_t r8_4;
    uint64_t rbx_3;
    uint64_t rax_1;
    uint64_t local_sp_4;
    uint64_t rcx_4;
    uint64_t var_54;
    uint32_t *var_159;
    uint32_t *_pre_phi483;
    uint64_t r8_6;
    uint64_t r9_5;
    uint64_t local_sp_6;
    uint64_t r8_5;
    uint64_t rax_2;
    uint64_t local_sp_5;
    uint64_t rcx_5;
    uint64_t var_179;
    uint64_t r10_5;
    uint64_t var_142;
    struct indirect_placeholder_23_ret_type var_143;
    uint64_t r9_6;
    uint64_t var_144;
    uint64_t var_145;
    uint64_t var_146;
    uint64_t var_147;
    uint64_t var_148;
    uint64_t var_149;
    uint64_t var_150;
    uint32_t *var_151;
    uint64_t var_152;
    uint64_t var_153;
    uint64_t var_154;
    uint64_t var_155;
    uint64_t var_156;
    uint64_t var_157;
    uint32_t *var_158;
    uint64_t var_160;
    uint64_t var_161;
    uint64_t var_162;
    uint64_t var_163;
    uint64_t var_164;
    uint32_t *var_165;
    uint64_t var_166;
    uint64_t var_167;
    uint64_t var_168;
    uint64_t var_169;
    uint64_t var_170;
    uint64_t var_171;
    uint32_t *var_172;
    uint64_t var_173;
    uint64_t var_174;
    uint64_t var_175;
    uint64_t var_176;
    uint64_t var_177;
    uint32_t *var_178;
    uint64_t local_sp_7;
    uint64_t var_75;
    uint64_t var_76;
    uint64_t local_sp_11;
    uint64_t rbx_5;
    uint64_t local_sp_8;
    uint64_t var_77;
    uint64_t var_79;
    uint64_t var_80;
    uint64_t var_69;
    uint64_t var_70;
    uint64_t var_71;
    uint64_t var_72;
    uint64_t var_65;
    uint64_t var_66;
    uint64_t var_67;
    struct indirect_placeholder_27_ret_type var_68;
    uint32_t var_45;
    uint32_t *var_46;
    uint64_t var_47;
    uint64_t var_48;
    uint64_t *var_49;
    unsigned char var_50;
    bool var_51;
    unsigned char var_52;
    bool var_53;
    struct indirect_placeholder_31_ret_type var_55;
    uint64_t var_56;
    uint64_t var_57;
    uint64_t var_58;
    uint64_t var_59;
    uint64_t var_60;
    uint64_t var_61;
    uint64_t local_sp_19_ph;
    uint64_t rbx_4;
    uint64_t var_81;
    uint64_t var_82;
    uint64_t var_83;
    uint64_t var_84;
    uint64_t var_85;
    uint64_t var_86;
    uint64_t var_87;
    uint64_t var_88;
    struct indirect_placeholder_32_ret_type var_89;
    uint64_t var_90;
    uint64_t var_91;
    uint64_t var_92;
    uint64_t var_93;
    uint64_t var_94;
    uint64_t *var_95;
    uint64_t **var_62;
    uint64_t var_63;
    uint64_t var_64;
    uint64_t var_73;
    uint64_t var_74;
    uint64_t var_207;
    uint64_t local_sp_18;
    uint64_t var_204;
    uint64_t local_sp_16;
    uint64_t var_212;
    uint64_t var_213;
    uint64_t var_214;
    struct indirect_placeholder_36_ret_type var_215;
    uint64_t var_216;
    uint64_t var_217;
    uint64_t var_218;
    uint64_t var_219;
    uint64_t local_sp_19_be;
    uint64_t local_sp_19;
    uint64_t var_196;
    struct indirect_placeholder_38_ret_type var_197;
    uint64_t var_198;
    uint64_t var_199;
    uint64_t var_200;
    uint64_t var_201;
    uint64_t var_191;
    uint64_t rbx_6;
    uint64_t local_sp_13;
    uint64_t var_185;
    uint64_t local_sp_14;
    unsigned char **var_186;
    uint64_t var_187;
    uint64_t var_188;
    uint64_t var_189;
    uint64_t var_190;
    uint64_t local_sp_15;
    uint64_t var_192;
    uint64_t var_193;
    uint64_t var_194;
    unsigned char var_195;
    uint64_t local_sp_17;
    uint64_t var_181;
    uint64_t var_182;
    uint64_t var_183;
    uint64_t var_184;
    uint64_t var_202;
    uint64_t var_203;
    uint64_t var_205;
    uint64_t var_206;
    uint64_t var_208;
    uint64_t var_209;
    uint32_t var_211;
    uint64_t var_35;
    uint64_t var_36;
    uint64_t var_37;
    uint64_t var_39;
    uint64_t var_40;
    uint64_t var_41;
    uint64_t var_42;
    uint32_t var_43;
    uint64_t var_44;
    unsigned int loop_state_var;
    bool switch_state_var;
    var_0 = revng_init_local_sp(0UL);
    var_1 = init_rbx();
    var_2 = init_rbp();
    var_3 = init_cc_src2();
    var_4 = var_0 + (-8L);
    *(uint64_t *)var_4 = var_2;
    *(uint64_t *)(var_0 + (-16L)) = var_1;
    var_5 = (uint32_t *)(var_0 + (-204L));
    *var_5 = (uint32_t)rdi;
    var_6 = var_0 + (-216L);
    var_7 = (uint64_t *)var_6;
    *var_7 = rsi;
    var_8 = (unsigned char *)(var_0 + (-25L));
    *var_8 = (unsigned char)'\x00';
    var_9 = (unsigned char *)(var_0 + (-26L));
    *var_9 = (unsigned char)'\x00';
    var_10 = (uint64_t *)(var_0 + (-40L));
    *var_10 = 18446744073709551615UL;
    var_11 = (uint64_t *)(var_0 + (-48L));
    *var_11 = 0UL;
    var_12 = (uint64_t *)(var_0 + (-56L));
    *var_12 = 18446744073709551615UL;
    var_13 = (uint64_t *)(var_0 + (-64L));
    *var_13 = 0UL;
    var_14 = (uint64_t *)(var_0 + (-72L));
    *var_14 = 0UL;
    var_15 = (unsigned char *)(var_0 + (-73L));
    *var_15 = (unsigned char)'\n';
    var_16 = var_0 + (-176L);
    var_17 = (uint64_t *)var_16;
    *var_17 = 0UL;
    var_18 = (unsigned char *)(var_0 + (-74L));
    *var_18 = (unsigned char)'\x00';
    var_19 = (unsigned char *)(var_0 + (-75L));
    *var_19 = (unsigned char)'\x00';
    var_20 = (uint64_t *)(var_0 + (-96L));
    *var_20 = 0UL;
    var_21 = var_0 + (-184L);
    var_22 = (uint64_t *)var_21;
    *var_22 = 0UL;
    var_23 = (uint64_t *)(var_0 + (-104L));
    *var_23 = 0UL;
    var_24 = **(uint64_t **)var_6;
    *(uint64_t *)(var_0 + (-224L)) = 4205678UL;
    indirect_placeholder_4(var_24);
    *(uint64_t *)(var_0 + (-232L)) = 4205693UL;
    indirect_placeholder();
    var_25 = var_0 + (-240L);
    *(uint64_t *)var_25 = 4205703UL;
    indirect_placeholder();
    var_26 = (uint32_t *)(var_0 + (-124L));
    var_27 = var_0 + (-192L);
    var_28 = (uint32_t *)(var_0 + (-144L));
    var_29 = (uint64_t *)var_27;
    var_30 = var_0 + (-136L);
    var_31 = (uint64_t *)var_30;
    var_32 = (uint64_t *)(var_0 + (-120L));
    var_33 = (unsigned char *)(var_0 + (-137L));
    var_34 = (uint64_t *)(var_0 + (-88L));
    var_52 = (unsigned char)'\x00';
    storemerge8 = 18446744073709551615UL;
    rbx_7_ph = var_1;
    local_sp_19_ph = var_25;
    while (1U)
        {
            rbx_4 = rbx_7_ph;
            rbx_5 = rbx_7_ph;
            rbx_6 = rbx_7_ph;
            local_sp_19 = local_sp_19_ph;
            while (1U)
                {
                    var_35 = *var_7;
                    var_36 = (uint64_t)*var_5;
                    var_37 = local_sp_19 + (-8L);
                    *(uint64_t *)var_37 = 4206587UL;
                    var_38 = indirect_placeholder_41(0UL, 4290374UL, 4289856UL, var_35, var_36);
                    var_39 = var_38.field_0;
                    var_40 = var_38.field_1;
                    var_41 = var_38.field_2;
                    var_42 = var_38.field_3;
                    var_43 = (uint32_t)var_41;
                    *var_26 = var_43;
                    var_44 = helper_cc_compute_all_wrapper((uint64_t)var_43 + 1UL, 18446744073709551615UL, var_3, 16U);
                    local_sp_9 = var_37;
                    local_sp_11 = var_37;
                    local_sp_18 = var_37;
                    local_sp_16 = var_37;
                    local_sp_19_be = var_37;
                    local_sp_17 = var_37;
                    if ((var_44 & 64UL) != 0UL) {
                        var_45 = *var_5 - *(uint32_t *)6403576UL;
                        var_46 = (uint32_t *)(var_0 + (-148L));
                        *var_46 = var_45;
                        var_47 = *var_7 + ((uint64_t)*(uint32_t *)6403576UL << 3UL);
                        var_48 = var_0 + (-160L);
                        var_49 = (uint64_t *)var_48;
                        *var_49 = var_47;
                        var_50 = *var_8;
                        var_51 = (var_50 == '\x00');
                        if (!var_51) {
                            var_52 = *var_9;
                            loop_state_var = 2U;
                            break;
                        }
                        if (*var_9 != '\x00') {
                            loop_state_var = 2U;
                            break;
                        }
                        *(uint64_t *)(local_sp_19 + (-16L)) = 4206692UL;
                        indirect_placeholder_30(var_39, var_40, 0UL, 4290384UL, var_42, 0UL, 0UL);
                        *(uint64_t *)(local_sp_19 + (-24L)) = 4206702UL;
                        indirect_placeholder_28(var_4, 1UL);
                        abort();
                    }
                    var_180 = *var_26;
                    if ((uint64_t)(var_180 + (-110)) != 0UL) {
                        if ((int)var_180 > (int)110U) {
                            if ((uint64_t)(var_180 + (-114)) == 0UL) {
                                *var_19 = (unsigned char)'\x01';
                            } else {
                                if ((int)var_180 > (int)114U) {
                                    if ((uint64_t)(var_180 + (-122)) == 0UL) {
                                        *var_15 = (unsigned char)'\x00';
                                    } else {
                                        if ((uint64_t)(var_180 + (-128)) != 0UL) {
                                            loop_state_var = 1U;
                                            break;
                                        }
                                        var_205 = *var_14;
                                        var_206 = local_sp_19 + (-16L);
                                        *(uint64_t *)var_206 = 4206415UL;
                                        indirect_placeholder();
                                        local_sp_18 = var_206;
                                        if (var_205 != 0UL & (uint64_t)(uint32_t)var_205 == 0UL) {
                                            var_207 = local_sp_19 + (-24L);
                                            *(uint64_t *)var_207 = 4206444UL;
                                            indirect_placeholder_33(var_39, var_40, 0UL, 4290328UL, var_42, 0UL, 1UL);
                                            local_sp_18 = var_207;
                                        }
                                        *var_14 = *(uint64_t *)6404320UL;
                                        local_sp_19_be = local_sp_18;
                                    }
                                } else {
                                    if ((uint64_t)(var_180 + (-111)) != 0UL) {
                                        loop_state_var = 1U;
                                        break;
                                    }
                                    var_202 = *var_13;
                                    var_203 = local_sp_19 + (-16L);
                                    *(uint64_t *)var_203 = 4206341UL;
                                    indirect_placeholder();
                                    local_sp_16 = var_203;
                                    if (var_202 != 0UL & (uint64_t)(uint32_t)var_202 == 0UL) {
                                        var_204 = local_sp_19 + (-24L);
                                        *(uint64_t *)var_204 = 4206370UL;
                                        indirect_placeholder_34(var_39, var_40, 0UL, 4290296UL, var_42, 0UL, 1UL);
                                        local_sp_16 = var_204;
                                    }
                                    *var_13 = *(uint64_t *)6404320UL;
                                    local_sp_19_be = local_sp_16;
                                }
                            }
                        } else {
                            if ((uint64_t)(var_180 + 130U) == 0UL) {
                                *(uint64_t *)(local_sp_19 + (-16L)) = 4206479UL;
                                indirect_placeholder_28(var_4, 0UL);
                                abort();
                            }
                            if ((int)var_180 <= (int)4294967166U) {
                                if (var_180 != 4294967165U) {
                                    loop_state_var = 1U;
                                    break;
                                }
                                var_181 = *(uint64_t *)6403424UL;
                                *(uint64_t *)(local_sp_19 + (-16L)) = 4206531UL;
                                indirect_placeholder_40(0UL, 4290362UL, 0UL, 4288728UL, var_181, 4289710UL);
                                var_182 = local_sp_19 + (-24L);
                                *(uint64_t *)var_182 = 4206541UL;
                                indirect_placeholder();
                                local_sp_17 = var_182;
                                loop_state_var = 1U;
                                break;
                            }
                            if ((uint64_t)(var_180 + (-101)) != 0UL) {
                                loop_state_var = 0U;
                                break;
                            }
                            *var_8 = (unsigned char)'\x01';
                        }
                        local_sp_19 = local_sp_19_be;
                        continue;
                    }
                    var_208 = *(uint64_t *)6404320UL;
                    var_209 = local_sp_19 + (-16L);
                    *(uint64_t *)var_209 = 4206216UL;
                    var_210 = indirect_placeholder_9(0UL, 10UL, var_27, 0UL, var_208);
                    var_211 = (uint32_t)var_210;
                    *var_28 = var_211;
                    local_sp_19_be = var_209;
                    switch (var_211) {
                      case 0U:
                        {
                            var_212 = *var_29;
                            var_213 = *var_12;
                            *var_12 = ((var_212 > var_213) ? var_213 : var_212);
                        }
                        break;
                      case 1U:
                        {
                            break;
                        }
                        break;
                      default:
                        {
                            var_214 = *(uint64_t *)6404320UL;
                            *(uint64_t *)(local_sp_19 + (-24L)) = 4206279UL;
                            var_215 = indirect_placeholder_36(var_214);
                            var_216 = var_215.field_0;
                            var_217 = var_215.field_1;
                            var_218 = var_215.field_2;
                            var_219 = local_sp_19 + (-32L);
                            *(uint64_t *)var_219 = 4206307UL;
                            indirect_placeholder_35(var_216, var_217, 0UL, 4290271UL, var_218, 0UL, 1UL);
                            local_sp_19_be = var_219;
                        }
                        break;
                    }
                }
            switch_state_var = 0;
            switch (loop_state_var) {
              case 0U:
                {
                    if (var_180 != 105U) {
                        loop_state_var = 2U;
                        switch_state_var = 1;
                        break;
                    }
                    var_183 = *(uint64_t *)6404320UL;
                    var_184 = local_sp_19 + (-16L);
                    *(uint64_t *)var_184 = 4205856UL;
                    indirect_placeholder();
                    *var_31 = var_183;
                    *var_32 = *(uint64_t *)6404320UL;
                    *var_33 = (*var_31 == 0UL);
                    local_sp_14 = var_184;
                    if (*var_9 == '\x00') {
                        var_185 = local_sp_19 + (-24L);
                        *(uint64_t *)var_185 = 4205916UL;
                        indirect_placeholder_39(var_39, var_40, 0UL, 4290214UL, var_42, 0UL, 1UL);
                        local_sp_14 = var_185;
                    }
                    *var_9 = (unsigned char)'\x01';
                    local_sp_15 = local_sp_14;
                    if (*var_31 == 0UL) {
                        var_191 = *var_32;
                    } else {
                        var_186 = (unsigned char **)var_30;
                        **var_186 = (unsigned char)'\x00';
                        var_187 = *(uint64_t *)6404320UL;
                        var_188 = local_sp_14 + (-8L);
                        *(uint64_t *)var_188 = 4205978UL;
                        var_189 = indirect_placeholder_12(0UL, 4290244UL, 18446744073709551615UL, 4288891UL, 0UL, var_187);
                        *var_10 = var_189;
                        **var_186 = (unsigned char)'-';
                        var_190 = *var_31 + 1UL;
                        *var_32 = var_190;
                        var_191 = var_190;
                        local_sp_15 = var_188;
                    }
                    var_192 = local_sp_15 + (-8L);
                    *(uint64_t *)var_192 = 4206042UL;
                    var_193 = indirect_placeholder_12(0UL, 4290244UL, 18446744073709551615UL, 4288891UL, 0UL, var_191);
                    *var_11 = var_193;
                    var_194 = (var_193 - *var_10) + 1UL;
                    *var_34 = var_194;
                    var_195 = ((((*var_10 > *var_11) ^ (var_194 == 0UL)) | (uint64_t)*var_33) != 0UL);
                    *var_33 = var_195;
                    local_sp_13 = var_192;
                    if (var_195 == '\x00') {
                        var_196 = *(uint64_t *)6404320UL;
                        *(uint64_t *)(local_sp_15 + (-16L)) = 4206130UL;
                        var_197 = indirect_placeholder_38(var_196);
                        var_198 = var_197.field_0;
                        var_199 = var_197.field_2;
                        *(uint64_t *)(local_sp_15 + (-24L)) = 4206138UL;
                        indirect_placeholder();
                        var_200 = (uint64_t)*(uint32_t *)var_199;
                        var_201 = local_sp_15 + (-32L);
                        *(uint64_t *)var_201 = 4206170UL;
                        indirect_placeholder_37(var_198, var_199, 0UL, 4290264UL, 4290244UL, var_200, 1UL);
                        rbx_6 = var_199;
                        local_sp_13 = var_201;
                    }
                    rbx_7_ph = rbx_6;
                    local_sp_19_ph = local_sp_13;
                    continue;
                }
                break;
              case 2U:
                {
                    var_53 = (var_52 == '\x00');
                    if (var_53) {
                        if ((int)*var_46 <= (int)0U) {
                            loop_state_var = 1U;
                            switch_state_var = 1;
                            break;
                        }
                        loop_state_var = 0U;
                        switch_state_var = 1;
                        break;
                    }
                    if (var_50 != '\x01') {
                        loop_state_var = 0U;
                        switch_state_var = 1;
                        break;
                    }
                    if ((int)*var_46 <= (int)1U) {
                        loop_state_var = 1U;
                        switch_state_var = 1;
                        break;
                    }
                    loop_state_var = 0U;
                    switch_state_var = 1;
                    break;
                }
                break;
              case 1U:
                {
                    loop_state_var = 2U;
                    switch_state_var = 1;
                    break;
                }
                break;
            }
            if (switch_state_var)
                break;
        }
    switch (loop_state_var) {
      case 1U:
        {
            var_54 = *(uint64_t *)(var_47 + ((uint64_t)(var_52 ^ '\x01') << 3UL));
            *(uint64_t *)(local_sp_19 + (-16L)) = 4206798UL;
            var_55 = indirect_placeholder_31(var_54);
            var_56 = var_55.field_0;
            var_57 = var_55.field_1;
            var_58 = var_55.field_2;
            *(uint64_t *)(local_sp_19 + (-24L)) = 4206826UL;
            indirect_placeholder_29(var_56, var_57, 0UL, 4290417UL, var_58, 0UL, 0UL);
            *(uint64_t *)(local_sp_19 + (-32L)) = 4206836UL;
            indirect_placeholder_28(var_4, 1UL);
            abort();
        }
        break;
      case 2U:
        {
            *(uint64_t *)(local_sp_17 + (-8L)) = 4206551UL;
            indirect_placeholder_28(var_4, 1UL);
            abort();
        }
        break;
      case 0U:
        {
            if (!var_51) {
                var_59 = (uint64_t)(uint32_t)(uint64_t)*var_15;
                var_60 = (uint64_t)*var_46;
                var_61 = local_sp_19 + (-16L);
                *(uint64_t *)var_61 = 4206869UL;
                indirect_placeholder_5(var_59, var_60, var_47);
                *var_34 = (uint64_t)*var_46;
                *var_20 = *var_49;
                local_sp_9 = var_61;
                rbx_0 = rbx_4;
                local_sp_10 = local_sp_9;
                if (*var_19 == '\x01') {
                    var_81 = *var_12;
                    var_82 = *var_34;
                    *var_12 = ((var_82 > var_81) ? var_81 : var_82);
                }
                if (*var_18 != '\x01' & *var_19 == '\x01') {
                    var_83 = *var_34;
                    var_84 = *var_12;
                    var_85 = local_sp_9 + (-8L);
                    *(uint64_t *)var_85 = 4207261UL;
                    var_86 = indirect_placeholder_6(var_83, var_84);
                    local_sp_10 = var_85;
                    storemerge8 = var_86;
                }
                var_87 = *var_14;
                var_88 = local_sp_10 + (-8L);
                *(uint64_t *)var_88 = 4207288UL;
                var_89 = indirect_placeholder_32(storemerge8, var_87);
                var_90 = var_89.field_0;
                var_91 = var_89.field_1;
                var_92 = var_89.field_2;
                var_93 = var_89.field_3;
                var_94 = var_89.field_4;
                var_95 = (uint64_t *)(var_0 + (-168L));
                *var_95 = var_93;
                r8_0 = var_92;
                r10_0 = var_90;
                r9_0 = var_91;
                local_sp_0 = var_88;
                rcx_0 = var_94;
                if (var_93 == 0UL) {
                    var_96 = *var_14;
                    *(uint64_t *)(local_sp_10 + (-16L)) = 4207327UL;
                    var_97 = indirect_placeholder_16(var_96, 3UL, 0UL);
                    *(uint64_t *)(local_sp_10 + (-24L)) = 4207335UL;
                    indirect_placeholder();
                    var_98 = (uint64_t)*(uint32_t *)var_97;
                    var_99 = local_sp_10 + (-32L);
                    *(uint64_t *)var_99 = 4207362UL;
                    var_100 = indirect_placeholder_18(var_91, var_92, 0UL, 4290438UL, var_97, var_98, 1UL);
                    r8_0 = var_100.field_2;
                    r10_0 = var_100.field_0;
                    r9_0 = var_100.field_1;
                    rbx_0 = var_97;
                    local_sp_0 = var_99;
                    rcx_0 = var_100.field_3;
                }
                rcx_1 = rcx_0;
                r8_1 = r8_0;
                rbx_1 = rbx_0;
                r10_1 = r10_0;
                r9_1 = r9_0;
                local_sp_1 = local_sp_0;
                if (*var_18 == '\x00') {
                    var_101 = (uint64_t)(uint32_t)(uint64_t)*var_15;
                    var_102 = *(uint64_t *)6403656UL;
                    var_103 = *var_95;
                    var_104 = *var_12;
                    var_105 = local_sp_0 + (-8L);
                    *(uint64_t *)var_105 = 4207408UL;
                    var_106 = indirect_placeholder_19(r10_0, r9_0, var_21, rbx_0, var_104, var_103, var_101, var_102);
                    var_107 = var_106.field_0;
                    var_108 = var_106.field_1;
                    var_109 = var_106.field_2;
                    var_110 = var_106.field_3;
                    var_111 = var_106.field_4;
                    var_112 = var_106.field_5;
                    *var_34 = var_111;
                    *var_12 = var_111;
                    rcx_1 = var_112;
                    r8_1 = var_109;
                    rbx_1 = var_110;
                    r10_1 = var_107;
                    r9_1 = var_108;
                    local_sp_1 = var_105;
                }
                rbx_2 = rbx_1;
                r10_2 = r10_1;
                r9_2 = r9_1;
                r8_2 = r8_1;
                local_sp_2 = local_sp_1;
                rcx_2 = rcx_1;
                var_113 = local_sp_1 + (-8L);
                *(uint64_t *)var_113 = 4207457UL;
                var_114 = indirect_placeholder_20();
                var_115 = var_114.field_0;
                var_116 = var_114.field_1;
                local_sp_2 = var_113;
                rcx_2 = var_116;
                if (*var_8 != '\x01' & *var_9 != '\x01' & (uint64_t)(uint32_t)var_115 == 0UL) {
                    *(uint64_t *)(local_sp_1 + (-16L)) = 4207466UL;
                    indirect_placeholder();
                    var_117 = (uint64_t)*(uint32_t *)var_115;
                    var_118 = local_sp_1 + (-24L);
                    *(uint64_t *)var_118 = 4207490UL;
                    var_119 = indirect_placeholder_17(r9_1, r8_1, 0UL, 4290197UL, var_116, var_117, 1UL);
                    r10_2 = var_119.field_0;
                    r9_2 = var_119.field_1;
                    r8_2 = var_119.field_2;
                    local_sp_2 = var_118;
                    rcx_2 = var_119.field_3;
                }
                var_120 = *var_19 ^ '\x01';
                rcx_3 = rcx_2;
                r8_3 = r8_2;
                r10_3 = r10_2;
                r9_3 = r9_2;
                rax_0 = (uint64_t)var_120;
                local_sp_3 = local_sp_2;
                if (var_120 == '\x00') {
                    var_121 = *var_34;
                    var_122 = *var_12;
                    var_123 = *var_95;
                    var_124 = local_sp_2 + (-8L);
                    *(uint64_t *)var_124 = 4207527UL;
                    var_125 = indirect_placeholder_21(r10_2, r9_2, r8_2, rbx_1, var_121, var_122, var_122, var_123);
                    var_126 = var_125.field_0;
                    var_127 = var_125.field_1;
                    var_128 = var_125.field_2;
                    var_129 = var_125.field_3;
                    var_130 = var_125.field_4;
                    var_131 = var_125.field_5;
                    *var_23 = var_130;
                    rcx_3 = var_131;
                    r8_3 = var_128;
                    rbx_2 = var_129;
                    r10_3 = var_126;
                    r9_3 = var_127;
                    rax_0 = var_130;
                    local_sp_3 = var_124;
                }
                var_132 = *var_13;
                r10_4 = r10_3;
                r9_4 = r9_3;
                r8_4 = r8_3;
                rbx_3 = rbx_2;
                rax_1 = rax_0;
                local_sp_4 = local_sp_3;
                rcx_4 = rcx_3;
                var_133 = *(uint64_t *)6403648UL;
                var_134 = local_sp_3 + (-8L);
                *(uint64_t *)var_134 = 4207562UL;
                var_135 = indirect_placeholder_22(var_133, 4290441UL, var_132);
                var_136 = var_135.field_0;
                rax_1 = var_136;
                local_sp_4 = var_134;
                rcx_4 = var_135.field_1;
                if (var_132 != 0UL & var_136 == 0UL) {
                    var_137 = *var_13;
                    *(uint64_t *)(local_sp_3 + (-16L)) = 4207589UL;
                    var_138 = indirect_placeholder_16(var_137, 3UL, 0UL);
                    *(uint64_t *)(local_sp_3 + (-24L)) = 4207597UL;
                    indirect_placeholder();
                    var_139 = (uint64_t)*(uint32_t *)var_138;
                    var_140 = local_sp_3 + (-32L);
                    *(uint64_t *)var_140 = 4207624UL;
                    var_141 = indirect_placeholder_15(r9_3, r8_3, 0UL, 4290438UL, var_138, var_139, 1UL);
                    r10_4 = var_141.field_0;
                    r9_4 = var_141.field_1;
                    r8_4 = var_141.field_2;
                    rbx_3 = var_138;
                    rax_1 = 0UL;
                    local_sp_4 = var_140;
                    rcx_4 = var_141.field_3;
                }
                r8_6 = r8_4;
                r9_5 = r9_4;
                local_sp_6 = local_sp_4;
                r8_5 = r8_4;
                rax_2 = rax_1;
                local_sp_5 = local_sp_4;
                rcx_5 = rcx_4;
                r10_5 = r10_4;
                r9_6 = r9_4;
                if (*var_19 == '\x00') {
                    if (*var_18 == '\x00') {
                        var_160 = *var_22;
                        var_161 = *var_23;
                        var_162 = *var_34;
                        var_163 = local_sp_4 + (-8L);
                        *(uint64_t *)var_163 = 4207797UL;
                        var_164 = indirect_placeholder_16(var_161, var_160, var_162);
                        var_165 = (uint32_t *)(var_0 + (-108L));
                        *var_165 = (uint32_t)var_164;
                        _pre_phi483 = var_165;
                        rax_2 = var_164;
                        local_sp_5 = var_163;
                        rcx_5 = var_160;
                    } else {
                        if (*var_9 == '\x00') {
                            var_173 = *var_23;
                            var_174 = *var_20;
                            var_175 = *var_12;
                            var_176 = local_sp_4 + (-8L);
                            *(uint64_t *)var_176 = 4207860UL;
                            var_177 = indirect_placeholder_16(var_173, var_174, var_175);
                            var_178 = (uint32_t *)(var_0 + (-108L));
                            *var_178 = (uint32_t)var_177;
                            _pre_phi483 = var_178;
                            rax_2 = var_177;
                            local_sp_5 = var_176;
                            rcx_5 = var_174;
                        } else {
                            var_166 = (uint64_t)(uint32_t)(uint64_t)*var_15;
                            var_167 = *var_23;
                            var_168 = *var_10;
                            var_169 = *var_12;
                            var_170 = local_sp_4 + (-8L);
                            *(uint64_t *)var_170 = 4207832UL;
                            var_171 = indirect_placeholder_3(var_167, var_166, var_168, var_169);
                            var_172 = (uint32_t *)(var_0 + (-108L));
                            *var_172 = (uint32_t)var_171;
                            _pre_phi483 = var_172;
                            rax_2 = var_171;
                            local_sp_5 = var_170;
                            rcx_5 = var_166;
                        }
                    }
                } else {
                    if (*var_12 == 0UL) {
                        var_159 = (uint32_t *)(var_0 + (-108L));
                        *var_159 = 0U;
                        _pre_phi483 = var_159;
                    } else {
                        if (*var_34 == 0UL) {
                            var_142 = local_sp_4 + (-8L);
                            *(uint64_t *)var_142 = 4207685UL;
                            var_143 = indirect_placeholder_23(r9_4, r8_4, 0UL, 4290443UL, rcx_4, 0UL, 1UL);
                            r10_5 = var_143.field_0;
                            r9_6 = var_143.field_1;
                            r8_6 = var_143.field_2;
                            local_sp_6 = var_142;
                        }
                        r9_5 = r9_6;
                        r8_5 = r8_6;
                        if (*var_9 == '\x00') {
                            var_152 = *var_34;
                            var_153 = *var_20;
                            var_154 = *var_12;
                            var_155 = *var_95;
                            var_156 = local_sp_6 + (-8L);
                            *(uint64_t *)var_156 = 4207760UL;
                            var_157 = indirect_placeholder_24(r10_5, r9_6, r8_6, rbx_3, var_153, var_152, var_154, var_155);
                            var_158 = (uint32_t *)(var_0 + (-108L));
                            *var_158 = (uint32_t)var_157;
                            _pre_phi483 = var_158;
                            rax_2 = var_157;
                            local_sp_5 = var_156;
                            rcx_5 = var_152;
                        } else {
                            var_144 = (uint64_t)(uint32_t)(uint64_t)*var_15;
                            var_145 = *var_11;
                            var_146 = *var_10;
                            var_147 = *var_12;
                            var_148 = *var_95;
                            var_149 = local_sp_6 + (-8L);
                            *(uint64_t *)var_149 = 4207725UL;
                            var_150 = indirect_placeholder_24(r10_5, r9_6, var_144, rbx_3, var_146, var_145, var_147, var_148);
                            var_151 = (uint32_t *)(var_0 + (-108L));
                            *var_151 = (uint32_t)var_150;
                            _pre_phi483 = var_151;
                            r8_5 = var_144;
                            rax_2 = var_150;
                            local_sp_5 = var_149;
                            rcx_5 = var_145;
                        }
                    }
                }
                if (*_pre_phi483 != 0U) {
                    *(uint64_t *)(local_sp_5 + (-8L)) = 4207874UL;
                    indirect_placeholder();
                    var_179 = (uint64_t)*(uint32_t *)rax_2;
                    *(uint64_t *)(local_sp_5 + (-16L)) = 4207898UL;
                    indirect_placeholder_14(r9_5, r8_5, 0UL, 4290462UL, rcx_5, var_179, 1UL);
                }
                return;
            }
            if (var_53) {
                var_62 = (uint64_t **)var_48;
                var_63 = **var_62;
                var_64 = local_sp_19 + (-16L);
                *(uint64_t *)var_64 = 4206968UL;
                indirect_placeholder();
                local_sp_11 = var_64;
                var_65 = *(uint64_t *)6403656UL;
                var_66 = **var_62;
                var_67 = local_sp_19 + (-24L);
                *(uint64_t *)var_67 = 4207009UL;
                var_68 = indirect_placeholder_27(var_65, 4290436UL, var_66);
                local_sp_11 = var_67;
                if (*var_46 != 1U & (uint64_t)(uint32_t)var_63 != 0UL & *var_12 != 0UL & var_68.field_0 == 0UL) {
                    var_69 = **var_62;
                    *(uint64_t *)(local_sp_19 + (-32L)) = 4207042UL;
                    var_70 = indirect_placeholder_16(var_69, 3UL, 0UL);
                    *(uint64_t *)(local_sp_19 + (-40L)) = 4207050UL;
                    indirect_placeholder();
                    var_71 = (uint64_t)*(uint32_t *)var_70;
                    var_72 = local_sp_19 + (-48L);
                    *(uint64_t *)var_72 = 4207077UL;
                    indirect_placeholder_26(var_39, var_40, 0UL, 4290438UL, var_70, var_71, 1UL);
                    rbx_5 = var_70;
                    local_sp_11 = var_72;
                }
                var_73 = *(uint64_t *)6403656UL;
                var_74 = local_sp_11 + (-8L);
                *(uint64_t *)var_74 = 4207097UL;
                indirect_placeholder_28(2UL, var_73);
                local_sp_7 = var_74;
                local_sp_8 = var_74;
                rbx_4 = rbx_5;
                if (*var_19 == '\x01') {
                    switch (*var_12) {
                      case 18446744073709551615UL:
                        {
                            var_77 = (uint64_t)(uint32_t)(uint64_t)*var_15;
                            var_78 = *(uint64_t *)6403656UL;
                            var_79 = local_sp_8 + (-8L);
                            *(uint64_t *)var_79 = 4207177UL;
                            var_80 = indirect_placeholder_16(var_16, var_77, var_78);
                            *var_34 = var_80;
                            *var_20 = *var_17;
                            local_sp_9 = var_79;
                        }
                        break;
                      case 0UL:
                        {
                            *var_18 = (unsigned char)'\x01';
                            *var_34 = 18446744073709551615UL;
                            local_sp_9 = local_sp_7;
                        }
                        break;
                      default:
                        {
                            var_75 = local_sp_11 + (-16L);
                            *(uint64_t *)var_75 = 4207127UL;
                            var_76 = indirect_placeholder_25();
                            local_sp_7 = var_75;
                            local_sp_8 = var_75;
                            if ((long)var_76 > (long)8388608UL) {
                                var_77 = (uint64_t)(uint32_t)(uint64_t)*var_15;
                                var_78 = *(uint64_t *)6403656UL;
                                var_79 = local_sp_8 + (-8L);
                                *(uint64_t *)var_79 = 4207177UL;
                                var_80 = indirect_placeholder_16(var_16, var_77, var_78);
                                *var_34 = var_80;
                                *var_20 = *var_17;
                                local_sp_9 = var_79;
                            } else {
                                *var_18 = (unsigned char)'\x01';
                                *var_34 = 18446744073709551615UL;
                                local_sp_9 = local_sp_7;
                            }
                        }
                        break;
                    }
                } else {
                    var_77 = (uint64_t)(uint32_t)(uint64_t)*var_15;
                    var_78 = *(uint64_t *)6403656UL;
                    var_79 = local_sp_8 + (-8L);
                    *(uint64_t *)var_79 = 4207177UL;
                    var_80 = indirect_placeholder_16(var_16, var_77, var_78);
                    *var_34 = var_80;
                    *var_20 = *var_17;
                    local_sp_9 = var_79;
                }
            } else {
                *var_34 = ((*var_11 - *var_10) + 1UL);
                *var_20 = 0UL;
            }
        }
        break;
    }
}

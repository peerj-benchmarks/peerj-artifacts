typedef unsigned char __u_char;
typedef unsigned short __u_short;
typedef unsigned int __u_int;
typedef unsigned long __u_long;
typedef signed char __int8_t;
typedef unsigned char __uint8_t;
typedef short __int16_t;
typedef unsigned short __uint16_t;
typedef int __int32_t;
typedef unsigned int __uint32_t;
typedef long __int64_t;
typedef unsigned long __uint64_t;
typedef __int8_t __int_least8_t;
typedef __uint8_t __uint_least8_t;
typedef __int16_t __int_least16_t;
typedef __uint16_t __uint_least16_t;
typedef __int32_t __int_least32_t;
typedef __uint32_t __uint_least32_t;
typedef __int64_t __int_least64_t;
typedef __uint64_t __uint_least64_t;
typedef long __quad_t;
typedef unsigned long __u_quad_t;
typedef long __intmax_t;
typedef unsigned long __uintmax_t;
typedef unsigned long __dev_t;
typedef unsigned int __uid_t;
typedef unsigned int __gid_t;
typedef unsigned long __ino_t;
typedef unsigned long __ino64_t;
typedef unsigned int __mode_t;
typedef unsigned long __nlink_t;
typedef long __off_t;
typedef long __off64_t;
typedef int __pid_t;
typedef struct {
    int __val[2];
} __fsid_t;
typedef long __clock_t;
typedef unsigned long __rlim_t;
typedef unsigned long __rlim64_t;
typedef unsigned int __id_t;
typedef long __time_t;
typedef unsigned int __useconds_t;
typedef long __suseconds_t;
typedef int __daddr_t;
typedef int __key_t;
typedef int __clockid_t;
typedef void *__timer_t;
typedef long __blksize_t;
typedef long __blkcnt_t;
typedef long __blkcnt64_t;
typedef unsigned long __fsblkcnt_t;
typedef unsigned long __fsblkcnt64_t;
typedef unsigned long __fsfilcnt_t;
typedef unsigned long __fsfilcnt64_t;
typedef long __fsword_t;
typedef long __ssize_t;
typedef long __syscall_slong_t;
typedef unsigned long __syscall_ulong_t;
typedef __off64_t __loff_t;
typedef char *__caddr_t;
typedef long __intptr_t;
typedef unsigned int __socklen_t;
typedef int __sig_atomic_t;
typedef __int8_t int8_t;
typedef __int16_t int16_t;
typedef __int32_t int32_t;
typedef __int64_t int64_t;
typedef __uint8_t uint8_t;
typedef __uint16_t uint16_t;
typedef __uint32_t uint32_t;
typedef __uint64_t uint64_t;
typedef __int_least8_t int_least8_t;
typedef __int_least16_t int_least16_t;
typedef __int_least32_t int_least32_t;
typedef __int_least64_t int_least64_t;
typedef __uint_least8_t uint_least8_t;
typedef __uint_least16_t uint_least16_t;
typedef __uint_least32_t uint_least32_t;
typedef __uint_least64_t uint_least64_t;
typedef signed char int_fast8_t;
typedef long int_fast16_t;
typedef long int_fast32_t;
typedef long int_fast64_t;
typedef unsigned char uint_fast8_t;
typedef unsigned long uint_fast16_t;
typedef unsigned long uint_fast32_t;
typedef unsigned long uint_fast64_t;
typedef long intptr_t;
typedef unsigned long uintptr_t;
typedef __intmax_t intmax_t;
typedef __uintmax_t uintmax_t;
typedef _Bool bool;
extern uint64_t revng_init_local_sp(uint64_t param_0);
extern uint64_t init_rbp(void);
extern void indirect_placeholder_1(void);
extern uint64_t indirect_placeholder_2(uint64_t param_0);
uint64_t bb_search_item(uint64_t rsi, uint64_t rdi) {
    uint64_t var_40;
    uint64_t var_0;
    uint64_t var_1;
    uint64_t var_2;
    uint64_t *var_3;
    uint64_t *var_4;
    uint64_t var_5;
    uint64_t var_7;
    uint64_t var_44;
    uint64_t var_46;
    uint64_t var_36;
    uint64_t var_45;
    uint64_t var_39;
    uint64_t local_sp_0;
    uint64_t var_41;
    bool var_42;
    uint32_t *var_43;
    uint64_t var_34;
    uint64_t local_sp_1;
    uint64_t var_35;
    uint32_t *var_47;
    uint32_t var_48;
    uint32_t var_64;
    uint32_t var_49;
    uint64_t *var_50;
    uint64_t var_51;
    bool var_58;
    uint64_t var_59;
    uint64_t var_52;
    uint64_t var_53;
    uint64_t *var_13;
    uint64_t var_14;
    uint64_t var_15;
    uint32_t var_54;
    uint32_t var_55;
    bool var_56;
    uint32_t var_57;
    uint64_t var_60;
    uint64_t *var_61;
    bool var_62;
    uint64_t var_63;
    uint64_t var_37;
    uint64_t var_38;
    uint64_t var_29;
    uint64_t local_sp_2;
    uint64_t var_30;
    uint64_t var_31;
    bool var_32;
    uint64_t var_33;
    uint64_t var_27;
    uint64_t var_28;
    bool var_17;
    uint64_t var_18;
    uint64_t var_19;
    uint64_t var_21;
    uint64_t var_20;
    uint64_t var_23;
    uint64_t var_24;
    bool var_25;
    uint64_t var_26;
    uint64_t var_22;
    uint64_t local_sp_4;
    uint64_t *rax_0_in_pre_phi;
    uint64_t var_6;
    uint64_t local_sp_3;
    uint64_t var_65;
    uint64_t var_66;
    uint64_t *_pre144;
    uint64_t *var_8;
    uint64_t var_9;
    uint64_t *var_10;
    uint64_t *var_11;
    uint32_t *var_12;
    uint32_t var_16;
    unsigned int loop_state_var;
    var_0 = revng_init_local_sp(0UL);
    var_1 = init_rbp();
    *(uint64_t *)(var_0 + (-8L)) = var_1;
    var_2 = var_0 + (-72L);
    var_3 = (uint64_t *)(var_0 + (-64L));
    *var_3 = rdi;
    var_4 = (uint64_t *)var_2;
    *var_4 = rsi;
    var_5 = *var_3;
    var_7 = var_5;
    local_sp_3 = var_2;
    if (var_5 == 0UL) {
        var_6 = var_0 + (-80L);
        *(uint64_t *)var_6 = 4202084UL;
        indirect_placeholder_1();
        var_7 = *var_3;
        local_sp_3 = var_6;
    }
    local_sp_4 = local_sp_3;
    if (*(uint64_t *)(var_7 + 16UL) == 0UL) {
        var_65 = *var_4;
        *(uint64_t *)(local_sp_3 + (-8L)) = 4202112UL;
        var_66 = indirect_placeholder_2(var_65);
        *(uint64_t *)(*var_3 + 16UL) = var_66;
        _pre144 = (uint64_t *)(*var_3 + 16UL);
        rax_0_in_pre_phi = _pre144;
    } else {
        var_8 = (uint64_t *)(var_0 + (-48L));
        *var_8 = var_7;
        var_9 = *(uint64_t *)(*var_3 + 16UL);
        var_10 = (uint64_t *)(var_0 + (-16L));
        *var_10 = var_9;
        var_11 = (uint64_t *)(var_0 + (-40L));
        *var_11 = var_9;
        var_12 = (uint32_t *)(var_0 + (-52L));
        var_13 = (uint64_t *)(var_0 + (-24L));
        rax_0_in_pre_phi = var_10;
        while (1U)
            {
                var_14 = *var_4;
                var_15 = local_sp_4 + (-8L);
                *(uint64_t *)var_15 = 4202186UL;
                indirect_placeholder_1();
                var_16 = (uint32_t)var_14;
                *var_12 = var_16;
                local_sp_4 = var_15;
                if (var_16 != 0U) {
                    loop_state_var = 0U;
                    break;
                }
                var_17 = ((int)var_16 > (int)4294967295U);
                var_18 = *var_10;
                rax_0_in_pre_phi = var_13;
                if (var_17) {
                    var_20 = *(uint64_t *)(var_18 + 16UL);
                    *var_13 = var_20;
                    var_21 = var_20;
                } else {
                    var_19 = *(uint64_t *)(var_18 + 8UL);
                    *var_13 = var_19;
                    var_21 = var_19;
                }
                var_22 = var_21;
                if (var_21 == 0UL) {
                    if (*(uint32_t *)(var_21 + 24UL) == 0U) {
                        *var_8 = *var_10;
                        *var_11 = *var_13;
                        var_22 = *var_13;
                    }
                    *var_10 = var_22;
                    continue;
                }
                var_23 = *var_4;
                *(uint64_t *)(local_sp_4 + (-16L)) = 4202259UL;
                var_24 = indirect_placeholder_2(var_23);
                *var_13 = var_24;
                var_25 = ((int)*var_12 > (int)4294967295U);
                var_26 = *var_10;
                if (!var_25) {
                    *(uint64_t *)(var_26 + 8UL) = var_24;
                    loop_state_var = 1U;
                    break;
                }
                *(uint64_t *)(var_26 + 16UL) = var_24;
                loop_state_var = 1U;
                break;
            }
        var_27 = *var_4;
        var_28 = local_sp_4 + (-24L);
        *(uint64_t *)var_28 = 4202317UL;
        indirect_placeholder_1();
        local_sp_2 = var_28;
        if ((uint64_t)(uint32_t)var_27 == 0UL) {
            var_29 = local_sp_4 + (-32L);
            *(uint64_t *)var_29 = 4202346UL;
            indirect_placeholder_1();
            local_sp_2 = var_29;
        }
        var_30 = *var_4;
        var_31 = local_sp_2 + (-8L);
        *(uint64_t *)var_31 = 4202371UL;
        indirect_placeholder_1();
        var_32 = ((int)(uint32_t)var_30 > (int)4294967295U);
        var_33 = *var_11;
        local_sp_1 = var_31;
        if (var_32) {
            var_35 = *(uint64_t *)(var_33 + 16UL);
            *var_10 = var_35;
            *(uint64_t *)(var_0 + (-32L)) = var_35;
            *var_12 = 1U;
        } else {
            var_34 = *(uint64_t *)(var_33 + 8UL);
            *var_10 = var_34;
            *(uint64_t *)(var_0 + (-32L)) = var_34;
            *var_12 = 4294967295U;
        }
        var_36 = *var_10;
        while (var_36 != *var_13)
            {
                var_37 = *var_4;
                var_38 = local_sp_1 + (-8L);
                *(uint64_t *)var_38 = 4202458UL;
                indirect_placeholder_1();
                local_sp_0 = var_38;
                if ((uint64_t)(uint32_t)var_37 == 0UL) {
                    var_39 = local_sp_1 + (-16L);
                    *(uint64_t *)var_39 = 4202487UL;
                    indirect_placeholder_1();
                    local_sp_0 = var_39;
                }
                var_40 = *var_4;
                var_41 = local_sp_0 + (-8L);
                *(uint64_t *)var_41 = 4202512UL;
                indirect_placeholder_1();
                var_42 = ((int)(uint32_t)var_40 > (int)4294967295U);
                var_43 = (uint32_t *)(*var_10 + 24UL);
                local_sp_1 = var_41;
                if (var_42) {
                    *var_43 = 1U;
                    var_45 = *(uint64_t *)(*var_10 + 16UL);
                    *var_10 = var_45;
                    var_46 = var_45;
                } else {
                    *var_43 = 4294967295U;
                    var_44 = *(uint64_t *)(*var_10 + 8UL);
                    *var_10 = var_44;
                    var_46 = var_44;
                }
                var_36 = var_46;
            }
        var_47 = (uint32_t *)(*var_11 + 24UL);
        var_48 = *var_47;
        if (var_48 != 0U) {
            var_49 = *var_12;
            var_64 = var_49;
            if ((uint64_t)(var_48 - (0U - var_49)) != 0UL) {
                var_50 = (uint64_t *)(var_0 + (-32L));
                var_51 = *var_50;
                if ((uint64_t)(*(uint32_t *)(var_51 + 24UL) - var_49) == 0UL) {
                    *var_10 = var_51;
                    var_58 = ((int)*var_12 > (int)4294967295U);
                    var_59 = *var_50;
                    if (var_58) {
                        *(uint64_t *)(*var_11 + 16UL) = *(uint64_t *)(var_59 + 8UL);
                        *(uint64_t *)(*var_50 + 8UL) = *var_11;
                    } else {
                        *(uint64_t *)(*var_11 + 8UL) = *(uint64_t *)(var_59 + 16UL);
                        *(uint64_t *)(*var_50 + 16UL) = *var_11;
                    }
                    *(uint32_t *)(*var_50 + 24UL) = 0U;
                    *(uint32_t *)(*var_11 + 24UL) = *(uint32_t *)(*var_50 + 24UL);
                } else {
                    if ((int)var_49 > (int)4294967295U) {
                        var_53 = *(uint64_t *)(var_51 + 8UL);
                        *var_10 = var_53;
                        *(uint64_t *)(*var_50 + 8UL) = *(uint64_t *)(var_53 + 16UL);
                        *(uint64_t *)(*var_10 + 16UL) = *var_50;
                        *(uint64_t *)(*var_11 + 16UL) = *(uint64_t *)(*var_10 + 8UL);
                        *(uint64_t *)(*var_10 + 8UL) = *var_11;
                    } else {
                        var_52 = *(uint64_t *)(var_51 + 16UL);
                        *var_10 = var_52;
                        *(uint64_t *)(*var_50 + 16UL) = *(uint64_t *)(var_52 + 8UL);
                        *(uint64_t *)(*var_10 + 8UL) = *var_50;
                        *(uint64_t *)(*var_11 + 8UL) = *(uint64_t *)(*var_10 + 16UL);
                        *(uint64_t *)(*var_10 + 16UL) = *var_11;
                    }
                    *(uint32_t *)(*var_11 + 24UL) = 0U;
                    *(uint32_t *)(*var_50 + 24UL) = 0U;
                    var_54 = *(uint32_t *)(*var_10 + 24UL);
                    var_55 = *var_12;
                    var_56 = ((uint64_t)(var_54 - var_55) == 0UL);
                    var_57 = 0U - var_55;
                    if (var_56) {
                        *(uint32_t *)(*var_11 + 24UL) = var_57;
                    } else {
                        if ((uint64_t)(var_54 - var_57) == 0UL) {
                            *(uint32_t *)(*var_50 + 24UL) = var_55;
                        }
                    }
                    *(uint32_t *)(*var_10 + 24UL) = 0U;
                }
                var_60 = *var_8;
                var_61 = (uint64_t *)(var_60 + 16UL);
                var_62 = (*var_61 == *var_11);
                var_63 = *var_10;
                if (var_62) {
                    *var_61 = var_63;
                } else {
                    *(uint64_t *)(var_60 + 8UL) = var_63;
                }
                return *rax_0_in_pre_phi;
            }
        }
        var_64 = *var_12;
    }
}

typedef unsigned char __u_char;
typedef unsigned short __u_short;
typedef unsigned int __u_int;
typedef unsigned long __u_long;
typedef signed char __int8_t;
typedef unsigned char __uint8_t;
typedef short __int16_t;
typedef unsigned short __uint16_t;
typedef int __int32_t;
typedef unsigned int __uint32_t;
typedef long __int64_t;
typedef unsigned long __uint64_t;
typedef __int8_t __int_least8_t;
typedef __uint8_t __uint_least8_t;
typedef __int16_t __int_least16_t;
typedef __uint16_t __uint_least16_t;
typedef __int32_t __int_least32_t;
typedef __uint32_t __uint_least32_t;
typedef __int64_t __int_least64_t;
typedef __uint64_t __uint_least64_t;
typedef long __quad_t;
typedef unsigned long __u_quad_t;
typedef long __intmax_t;
typedef unsigned long __uintmax_t;
typedef unsigned long __dev_t;
typedef unsigned int __uid_t;
typedef unsigned int __gid_t;
typedef unsigned long __ino_t;
typedef unsigned long __ino64_t;
typedef unsigned int __mode_t;
typedef unsigned long __nlink_t;
typedef long __off_t;
typedef long __off64_t;
typedef int __pid_t;
typedef struct {
    int __val[2];
} __fsid_t;
typedef long __clock_t;
typedef unsigned long __rlim_t;
typedef unsigned long __rlim64_t;
typedef unsigned int __id_t;
typedef long __time_t;
typedef unsigned int __useconds_t;
typedef long __suseconds_t;
typedef int __daddr_t;
typedef int __key_t;
typedef int __clockid_t;
typedef void *__timer_t;
typedef long __blksize_t;
typedef long __blkcnt_t;
typedef long __blkcnt64_t;
typedef unsigned long __fsblkcnt_t;
typedef unsigned long __fsblkcnt64_t;
typedef unsigned long __fsfilcnt_t;
typedef unsigned long __fsfilcnt64_t;
typedef long __fsword_t;
typedef long __ssize_t;
typedef long __syscall_slong_t;
typedef unsigned long __syscall_ulong_t;
typedef __off64_t __loff_t;
typedef char *__caddr_t;
typedef long __intptr_t;
typedef unsigned int __socklen_t;
typedef int __sig_atomic_t;
typedef __int8_t int8_t;
typedef __int16_t int16_t;
typedef __int32_t int32_t;
typedef __int64_t int64_t;
typedef __uint8_t uint8_t;
typedef __uint16_t uint16_t;
typedef __uint32_t uint32_t;
typedef __uint64_t uint64_t;
typedef __int_least8_t int_least8_t;
typedef __int_least16_t int_least16_t;
typedef __int_least32_t int_least32_t;
typedef __int_least64_t int_least64_t;
typedef __uint_least8_t uint_least8_t;
typedef __uint_least16_t uint_least16_t;
typedef __uint_least32_t uint_least32_t;
typedef __uint_least64_t uint_least64_t;
typedef signed char int_fast8_t;
typedef long int_fast16_t;
typedef long int_fast32_t;
typedef long int_fast64_t;
typedef unsigned char uint_fast8_t;
typedef unsigned long uint_fast16_t;
typedef unsigned long uint_fast32_t;
typedef unsigned long uint_fast64_t;
typedef long intptr_t;
typedef unsigned long uintptr_t;
typedef __intmax_t intmax_t;
typedef __uintmax_t uintmax_t;
typedef _Bool bool;
struct indirect_placeholder_11_ret_type;
struct indirect_placeholder_12_ret_type;
struct indirect_placeholder_13_ret_type;
struct indirect_placeholder_15_ret_type;
struct indirect_placeholder_11_ret_type {
    uint64_t field_0;
    uint64_t field_1;
    uint64_t field_2;
};
struct indirect_placeholder_12_ret_type {
    uint64_t field_0;
    uint64_t field_1;
    uint64_t field_2;
};
struct indirect_placeholder_13_ret_type {
    uint64_t field_0;
    uint64_t field_1;
    uint64_t field_2;
};
struct indirect_placeholder_15_ret_type {
    uint64_t field_0;
    uint64_t field_1;
    uint64_t field_2;
};
extern uint64_t revng_init_local_sp(uint64_t param_0);
extern uint64_t init_rbp(void);
extern uint64_t indirect_placeholder(uint64_t param_0, uint64_t param_1);
extern void indirect_placeholder_1(void);
extern uint64_t indirect_placeholder_10(uint64_t param_0, uint64_t param_1, uint64_t param_2);
extern uint64_t indirect_placeholder_14(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint64_t param_3);
extern uint64_t init_rbx(void);
extern void indirect_placeholder_8(uint64_t param_0, uint64_t param_1);
extern uint64_t indirect_placeholder_4(void);
extern void indirect_placeholder_9(uint64_t param_0);
extern uint64_t init_r9(void);
extern uint64_t init_r8(void);
extern uint64_t indirect_placeholder_2(uint64_t param_0);
extern void indirect_placeholder_5(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint64_t param_3, uint64_t param_4, uint64_t param_5, uint64_t param_6);
extern struct indirect_placeholder_11_ret_type indirect_placeholder_11(uint64_t param_0, uint64_t param_1, uint64_t param_2);
extern struct indirect_placeholder_12_ret_type indirect_placeholder_12(uint64_t param_0, uint64_t param_1, uint64_t param_2);
extern struct indirect_placeholder_13_ret_type indirect_placeholder_13(uint64_t param_0, uint64_t param_1, uint64_t param_2);
extern struct indirect_placeholder_15_ret_type indirect_placeholder_15(uint64_t param_0, uint64_t param_1, uint64_t param_2);
uint64_t bb_tsort(uint64_t rdi) {
    uint64_t var_0;
    uint64_t var_1;
    uint64_t var_2;
    uint64_t var_3;
    uint64_t var_4;
    uint64_t *var_5;
    unsigned char *var_6;
    uint64_t *var_7;
    uint64_t *var_8;
    uint64_t var_9;
    bool var_10;
    unsigned char *var_11;
    uint64_t var_12;
    uint64_t var_13;
    uint64_t *var_14;
    uint64_t local_sp_8;
    uint64_t local_sp_0;
    uint64_t rax_0;
    uint64_t r9_0;
    uint64_t r8_0;
    uint64_t storemerge;
    uint64_t var_70;
    uint64_t var_48;
    uint64_t var_66;
    uint64_t var_67;
    struct indirect_placeholder_11_ret_type var_68;
    uint64_t var_69;
    uint64_t local_sp_1;
    uint64_t var_62;
    uint64_t var_63;
    uint64_t local_sp_5;
    uint64_t var_50;
    uint64_t var_55;
    uint64_t **var_51;
    uint64_t *var_52;
    uint64_t var_53;
    uint64_t var_54;
    uint64_t var_71;
    uint64_t local_sp_2;
    uint64_t var_45;
    uint64_t var_56;
    struct indirect_placeholder_12_ret_type var_57;
    uint64_t var_58;
    uint64_t var_59;
    uint64_t var_60;
    uint64_t var_61;
    uint64_t var_49;
    uint64_t local_sp_3_ph;
    uint64_t r9_1_ph;
    uint64_t r8_1_ph;
    uint64_t local_sp_3;
    uint64_t var_64;
    uint64_t var_65;
    uint64_t var_46;
    uint64_t var_47;
    uint64_t var_76;
    uint64_t local_sp_6;
    uint64_t var_77;
    uint64_t local_sp_4;
    uint64_t local_sp_7;
    uint64_t var_35;
    struct indirect_placeholder_13_ret_type var_36;
    uint64_t var_37;
    uint64_t var_38;
    uint64_t var_39;
    uint64_t var_40;
    uint64_t r8_3;
    uint64_t r9_2;
    uint64_t r8_2;
    uint64_t var_41;
    uint64_t var_42;
    uint64_t var_43;
    uint64_t *var_44;
    uint64_t var_34;
    uint64_t var_72;
    uint64_t var_73;
    uint64_t var_74;
    uint64_t var_75;
    uint64_t var_31;
    uint64_t var_32;
    uint64_t var_33;
    uint64_t var_19;
    struct indirect_placeholder_15_ret_type var_20;
    uint64_t var_21;
    uint64_t var_22;
    uint64_t var_23;
    uint64_t var_24;
    uint64_t var_25;
    uint64_t var_15;
    uint64_t var_16;
    uint64_t var_17;
    uint64_t var_18;
    uint64_t r9_3;
    uint64_t var_26;
    uint64_t var_27;
    uint64_t var_28;
    uint64_t *var_29;
    uint64_t *var_30;
    unsigned int loop_state_var;
    bool switch_state_var;
    var_0 = revng_init_local_sp(0UL);
    var_1 = init_rbp();
    var_2 = init_r9();
    var_3 = init_r8();
    var_4 = init_rbx();
    *(uint64_t *)(var_0 + (-8L)) = var_1;
    *(uint64_t *)(var_0 + (-16L)) = var_4;
    var_5 = (uint64_t *)(var_0 + (-112L));
    *var_5 = rdi;
    var_6 = (unsigned char *)(var_0 + (-25L));
    *var_6 = (unsigned char)'\x01';
    var_7 = (uint64_t *)(var_0 + (-40L));
    *var_7 = 0UL;
    var_8 = (uint64_t *)(var_0 + (-48L));
    *var_8 = 0UL;
    var_9 = *var_5;
    *(uint64_t *)(var_0 + (-128L)) = 4203996UL;
    indirect_placeholder_1();
    var_10 = ((uint64_t)(uint32_t)var_9 == 0UL);
    var_11 = (unsigned char *)(var_0 + (-57L));
    *var_11 = var_10;
    var_12 = var_0 + (-136L);
    *(uint64_t *)var_12 = 4204014UL;
    var_13 = indirect_placeholder_2(0UL);
    var_14 = (uint64_t *)(var_0 + (-72L));
    *var_14 = var_13;
    storemerge = 4270582UL;
    local_sp_8 = var_12;
    r9_3 = var_2;
    r8_3 = var_3;
    var_15 = *(uint64_t *)6379272UL;
    var_16 = *var_5;
    var_17 = var_0 + (-144L);
    *(uint64_t *)var_17 = 4204053UL;
    var_18 = indirect_placeholder_10(var_15, 4270492UL, var_16);
    local_sp_8 = var_17;
    if (*var_11 != '\x01' & var_18 == 0UL) {
        var_19 = *var_5;
        *(uint64_t *)(var_0 + (-152L)) = 4204080UL;
        var_20 = indirect_placeholder_15(var_19, 3UL, 0UL);
        var_21 = var_20.field_0;
        var_22 = var_20.field_1;
        var_23 = var_20.field_2;
        *(uint64_t *)(var_0 + (-160L)) = 4204088UL;
        indirect_placeholder_1();
        var_24 = (uint64_t)*(uint32_t *)var_21;
        var_25 = var_0 + (-168L);
        *(uint64_t *)var_25 = 4204115UL;
        indirect_placeholder_5(0UL, 4270487UL, var_21, var_24, 1UL, var_22, var_23);
        local_sp_8 = var_25;
        r9_3 = var_22;
        r8_3 = var_23;
    }
    var_26 = *(uint64_t *)6379272UL;
    *(uint64_t *)(local_sp_8 + (-8L)) = 4204135UL;
    indirect_placeholder_8(2UL, var_26);
    var_27 = var_0 + (-104L);
    var_28 = local_sp_8 + (-16L);
    *(uint64_t *)var_28 = 4204147UL;
    indirect_placeholder_9(var_27);
    var_29 = (uint64_t *)(var_0 + (-80L));
    var_30 = (uint64_t *)(var_0 + (-96L));
    r9_2 = r9_3;
    r8_2 = r8_3;
    local_sp_7 = var_28;
    while (1U)
        {
            switch_state_var = 1;
            break;
        }
    if (*var_8 == 0UL) {
        var_35 = *var_5;
        *(uint64_t *)(local_sp_7 + (-16L)) = 4204331UL;
        var_36 = indirect_placeholder_13(var_35, 3UL, 0UL);
        var_37 = var_36.field_0;
        var_38 = var_36.field_1;
        var_39 = var_36.field_2;
        var_40 = local_sp_7 + (-24L);
        *(uint64_t *)var_40 = 4204359UL;
        indirect_placeholder_5(0UL, 4270512UL, var_37, 0UL, 1UL, var_38, var_39);
        local_sp_5 = var_40;
        r9_2 = var_38;
        r8_2 = var_39;
    }
    var_41 = *var_14;
    var_42 = local_sp_5 + (-8L);
    *(uint64_t *)var_42 = 4204376UL;
    indirect_placeholder_8(4203186UL, var_41);
    var_43 = var_0 + (-56L);
    var_44 = (uint64_t *)var_43;
    local_sp_3_ph = var_42;
    r9_1_ph = r9_2;
    r8_1_ph = r8_2;
    while (1U)
        {
            r9_0 = r9_1_ph;
            r8_0 = r8_1_ph;
            var_45 = *(uint64_t *)6379432UL;
            local_sp_3 = local_sp_3_ph;
            while (1U)
                {
                    var_45 = 0UL;
                    if (var_45 != 0UL) {
                        var_64 = local_sp_3 + (-8L);
                        *(uint64_t *)var_64 = 4204716UL;
                        var_65 = indirect_placeholder_4();
                        local_sp_0 = var_64;
                        rax_0 = var_65;
                        if ((uint64_t)(uint32_t)var_65 != 0UL) {
                            loop_state_var = 2U;
                            break;
                        }
                        if (*var_11 != '\x00') {
                            loop_state_var = 1U;
                            break;
                        }
                        var_66 = *var_5;
                        var_67 = local_sp_3 + (-16L);
                        *(uint64_t *)var_67 = 4204755UL;
                        var_68 = indirect_placeholder_11(var_66, 3UL, 0UL);
                        var_69 = var_68.field_0;
                        local_sp_0 = var_67;
                        rax_0 = var_69;
                        r9_0 = var_68.field_1;
                        r8_0 = var_68.field_2;
                        storemerge = var_69;
                        loop_state_var = 1U;
                        break;
                    }
                    var_46 = *var_14;
                    var_47 = local_sp_3 + (-8L);
                    *(uint64_t *)var_47 = 4204398UL;
                    indirect_placeholder_8(4203219UL, var_46);
                    var_48 = *(uint64_t *)6379408UL;
                    local_sp_2 = var_47;
                    local_sp_3 = local_sp_2;
                    while (var_48 != 0UL)
                        {
                            *var_44 = *(uint64_t *)(var_48 + 48UL);
                            var_49 = local_sp_2 + (-8L);
                            *(uint64_t *)var_49 = 4204436UL;
                            indirect_placeholder_1();
                            **(uint64_t **)6379408UL = 0UL;
                            *(uint64_t *)6379432UL = (*(uint64_t *)6379432UL + (-1L));
                            var_50 = *var_44;
                            local_sp_2 = var_49;
                            while (var_50 != 0UL)
                                {
                                    var_51 = (uint64_t **)var_43;
                                    var_52 = (uint64_t *)(**var_51 + 32UL);
                                    *var_52 = (*var_52 + (-1L));
                                    var_53 = **var_51;
                                    if (*(uint64_t *)(var_53 + 32UL) == 0UL) {
                                        *(uint64_t *)(*(uint64_t *)6379416UL + 40UL) = var_53;
                                        *(uint64_t *)6379416UL = **var_51;
                                    }
                                    var_54 = *(uint64_t *)(*var_44 + 8UL);
                                    *var_44 = var_54;
                                    var_50 = var_54;
                                }
                            var_55 = *(uint64_t *)(*(uint64_t *)6379408UL + 40UL);
                            *(uint64_t *)6379408UL = var_55;
                            var_48 = var_55;
                            local_sp_3 = local_sp_2;
                        }
                    if (*(uint64_t *)6379432UL == 0UL) {
                        continue;
                    }
                    loop_state_var = 0U;
                    break;
                }
            switch_state_var = 0;
            switch (loop_state_var) {
              case 2U:
                {
                    switch_state_var = 1;
                    break;
                }
                break;
              case 0U:
                {
                    var_56 = *var_5;
                    *(uint64_t *)(local_sp_2 + (-8L)) = 4204624UL;
                    var_57 = indirect_placeholder_12(var_56, 3UL, 0UL);
                    var_58 = var_57.field_0;
                    var_59 = var_57.field_1;
                    var_60 = var_57.field_2;
                    var_61 = local_sp_2 + (-16L);
                    *(uint64_t *)var_61 = 4204652UL;
                    indirect_placeholder_5(0UL, 4270555UL, var_58, 0UL, 0UL, var_59, var_60);
                    *var_6 = (unsigned char)'\x00';
                    local_sp_1 = var_61;
                    r9_1_ph = var_59;
                    r8_1_ph = var_60;
                    var_62 = *var_14;
                    var_63 = local_sp_1 + (-8L);
                    *(uint64_t *)var_63 = 4204673UL;
                    indirect_placeholder_8(4203310UL, var_62);
                    local_sp_1 = var_63;
                    local_sp_3_ph = var_63;
                    do {
                        var_62 = *var_14;
                        var_63 = local_sp_1 + (-8L);
                        *(uint64_t *)var_63 = 4204673UL;
                        indirect_placeholder_8(4203310UL, var_62);
                        local_sp_1 = var_63;
                        local_sp_3_ph = var_63;
                    } while (*(uint64_t *)6379424UL != 0UL);
                    continue;
                }
                break;
              case 1U:
                {
                    *(uint64_t *)(local_sp_0 + (-8L)) = 4204763UL;
                    indirect_placeholder_1();
                    var_70 = (uint64_t)*(uint32_t *)rax_0;
                    *(uint64_t *)(local_sp_0 + (-16L)) = 4204790UL;
                    indirect_placeholder_5(0UL, 4270487UL, storemerge, var_70, 1UL, r9_0, r8_0);
                    switch_state_var = 1;
                    break;
                }
                break;
            }
            if (switch_state_var)
                break;
        }
    return (uint64_t)*var_6;
}

typedef unsigned char __u_char;
typedef unsigned short __u_short;
typedef unsigned int __u_int;
typedef unsigned long __u_long;
typedef signed char __int8_t;
typedef unsigned char __uint8_t;
typedef short __int16_t;
typedef unsigned short __uint16_t;
typedef int __int32_t;
typedef unsigned int __uint32_t;
typedef long __int64_t;
typedef unsigned long __uint64_t;
typedef __int8_t __int_least8_t;
typedef __uint8_t __uint_least8_t;
typedef __int16_t __int_least16_t;
typedef __uint16_t __uint_least16_t;
typedef __int32_t __int_least32_t;
typedef __uint32_t __uint_least32_t;
typedef __int64_t __int_least64_t;
typedef __uint64_t __uint_least64_t;
typedef long __quad_t;
typedef unsigned long __u_quad_t;
typedef long __intmax_t;
typedef unsigned long __uintmax_t;
typedef unsigned long __dev_t;
typedef unsigned int __uid_t;
typedef unsigned int __gid_t;
typedef unsigned long __ino_t;
typedef unsigned long __ino64_t;
typedef unsigned int __mode_t;
typedef unsigned long __nlink_t;
typedef long __off_t;
typedef long __off64_t;
typedef int __pid_t;
typedef struct {
    int __val[2];
} __fsid_t;
typedef long __clock_t;
typedef unsigned long __rlim_t;
typedef unsigned long __rlim64_t;
typedef unsigned int __id_t;
typedef long __time_t;
typedef unsigned int __useconds_t;
typedef long __suseconds_t;
typedef int __daddr_t;
typedef int __key_t;
typedef int __clockid_t;
typedef void *__timer_t;
typedef long __blksize_t;
typedef long __blkcnt_t;
typedef long __blkcnt64_t;
typedef unsigned long __fsblkcnt_t;
typedef unsigned long __fsblkcnt64_t;
typedef unsigned long __fsfilcnt_t;
typedef unsigned long __fsfilcnt64_t;
typedef long __fsword_t;
typedef long __ssize_t;
typedef long __syscall_slong_t;
typedef unsigned long __syscall_ulong_t;
typedef __off64_t __loff_t;
typedef char *__caddr_t;
typedef long __intptr_t;
typedef unsigned int __socklen_t;
typedef int __sig_atomic_t;
typedef __int8_t int8_t;
typedef __int16_t int16_t;
typedef __int32_t int32_t;
typedef __int64_t int64_t;
typedef __uint8_t uint8_t;
typedef __uint16_t uint16_t;
typedef __uint32_t uint32_t;
typedef __uint64_t uint64_t;
typedef __int_least8_t int_least8_t;
typedef __int_least16_t int_least16_t;
typedef __int_least32_t int_least32_t;
typedef __int_least64_t int_least64_t;
typedef __uint_least8_t uint_least8_t;
typedef __uint_least16_t uint_least16_t;
typedef __uint_least32_t uint_least32_t;
typedef __uint_least64_t uint_least64_t;
typedef signed char int_fast8_t;
typedef long int_fast16_t;
typedef long int_fast32_t;
typedef long int_fast64_t;
typedef unsigned char uint_fast8_t;
typedef unsigned long uint_fast16_t;
typedef unsigned long uint_fast32_t;
typedef unsigned long uint_fast64_t;
typedef long intptr_t;
typedef unsigned long uintptr_t;
typedef __intmax_t intmax_t;
typedef __uintmax_t uintmax_t;
typedef _Bool bool;
extern uint64_t revng_init_local_sp(uint64_t param_0);
extern uint64_t init_rbp(void);
extern uint64_t indirect_placeholder_5(uint64_t param_0, uint64_t param_1);
extern uint64_t init_cc_src2(void);
extern uint64_t helper_cc_compute_c_wrapper(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint32_t param_3);
extern void indirect_placeholder(void);
extern void indirect_placeholder_42(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint64_t param_3);
uint64_t bb_list_signals(uint64_t rdi, uint64_t rsi) {
    uint64_t var_0;
    uint64_t var_1;
    uint64_t var_2;
    uint64_t var_3;
    uint64_t *var_4;
    unsigned char *var_5;
    uint32_t *var_6;
    uint64_t var_53;
    uint64_t local_sp_0;
    uint32_t var_54;
    uint32_t var_49;
    uint64_t local_sp_3;
    uint64_t local_sp_1;
    uint64_t local_sp_8;
    bool var_44;
    uint64_t var_45;
    uint64_t *var_46;
    uint64_t var_33;
    uint64_t var_34;
    uint64_t var_35;
    uint64_t var_36;
    uint64_t local_sp_2;
    uint32_t var_37;
    uint32_t var_29;
    uint64_t local_sp_6;
    uint64_t var_25;
    uint64_t var_26;
    uint64_t var_27;
    uint64_t var_28;
    uint64_t var_18;
    uint64_t var_19;
    uint64_t local_sp_4;
    uint32_t var_20;
    uint32_t var_14;
    uint32_t *var_7;
    uint32_t *var_8;
    uint32_t *var_9;
    uint32_t var_10;
    uint64_t var_12;
    uint64_t *var_13;
    uint64_t local_sp_5;
    uint64_t var_21;
    uint64_t var_22;
    uint64_t var_23;
    uint32_t var_24;
    uint64_t local_sp_7;
    uint64_t var_30;
    uint64_t var_31;
    uint64_t var_32;
    uint64_t var_15;
    uint64_t var_16;
    uint64_t var_17;
    uint32_t var_11;
    uint64_t var_38;
    uint32_t *var_39;
    uint64_t var_40;
    uint64_t var_41;
    uint64_t var_42;
    uint32_t var_43;
    uint32_t *var_47;
    uint64_t var_48;
    uint64_t local_sp_9;
    uint64_t var_50;
    uint64_t var_51;
    uint64_t var_52;
    var_0 = revng_init_local_sp(0UL);
    var_1 = init_rbp();
    var_2 = init_cc_src2();
    *(uint64_t *)(var_0 + (-8L)) = var_1;
    var_3 = var_0 + (-72L);
    var_4 = (uint64_t *)var_3;
    *var_4 = rsi;
    var_5 = (unsigned char *)(var_0 + (-60L));
    *var_5 = (unsigned char)rdi;
    var_6 = (uint32_t *)(var_0 + (-16L));
    *var_6 = 0U;
    var_49 = 1U;
    local_sp_8 = var_3;
    var_29 = 1U;
    var_14 = 1U;
    var_10 = 1U;
    local_sp_5 = var_3;
    local_sp_9 = var_3;
    if (*var_5 == '\x00') {
        if (*var_4 == 0UL) {
            var_47 = (uint32_t *)(var_0 + (-12L));
            *var_47 = 1U;
            var_48 = var_0 + (-56L);
            while ((int)var_49 <= (int)64U)
                {
                    var_50 = (uint64_t)var_49;
                    var_51 = local_sp_9 + (-8L);
                    *(uint64_t *)var_51 = 4202632UL;
                    var_52 = indirect_placeholder_5(var_50, var_48);
                    local_sp_0 = var_51;
                    if ((uint64_t)(uint32_t)var_52 == 0UL) {
                        var_53 = local_sp_9 + (-16L);
                        *(uint64_t *)var_53 = 4202648UL;
                        indirect_placeholder();
                        local_sp_0 = var_53;
                    }
                    var_54 = *var_47 + 1U;
                    *var_47 = var_54;
                    var_49 = var_54;
                    local_sp_9 = local_sp_0;
                }
        }
        var_38 = var_0 + (-56L);
        var_39 = (uint32_t *)(var_0 + (-12L));
        var_40 = **(uint64_t **)var_3;
        while (var_40 != 0UL)
            {
                var_41 = local_sp_8 + (-8L);
                *(uint64_t *)var_41 = 4202514UL;
                var_42 = indirect_placeholder_5(var_40, var_38);
                var_43 = (uint32_t)var_42;
                *var_39 = var_43;
                local_sp_1 = var_41;
                if ((int)var_43 > (int)4294967295U) {
                    *var_6 = 1U;
                } else {
                    var_44 = ((uint64_t)(((uint32_t)(uint64_t)***(unsigned char ***)var_3 + (-48)) & (-2)) > 9UL);
                    var_45 = local_sp_8 + (-16L);
                    var_46 = (uint64_t *)var_45;
                    local_sp_1 = var_45;
                    if (var_44) {
                        *var_46 = 4202587UL;
                        indirect_placeholder();
                    } else {
                        *var_46 = 4202565UL;
                        indirect_placeholder();
                    }
                }
                *var_4 = (*var_4 + 8UL);
                local_sp_8 = local_sp_1;
                var_40 = **(uint64_t **)var_3;
            }
    }
    var_7 = (uint32_t *)(var_0 + (-20L));
    *var_7 = 0U;
    var_8 = (uint32_t *)(var_0 + (-24L));
    *var_8 = 1U;
    var_9 = (uint32_t *)(var_0 + (-12L));
    *var_9 = 1U;
    while ((int)var_10 <= (int)6U)
        {
            *var_8 = (*var_8 + 1U);
            var_11 = *var_9 * 10U;
            *var_9 = var_11;
            var_10 = var_11;
        }
    *var_9 = 1U;
    var_12 = var_0 + (-56L);
    var_13 = (uint64_t *)(var_0 + (-32L));
    local_sp_6 = local_sp_5;
    local_sp_7 = local_sp_5;
    while ((int)var_14 <= (int)64U)
        {
            var_15 = (uint64_t)var_14;
            var_16 = local_sp_5 + (-8L);
            *(uint64_t *)var_16 = 4202281UL;
            var_17 = indirect_placeholder_5(var_15, var_12);
            local_sp_4 = var_16;
            var_18 = local_sp_5 + (-16L);
            *(uint64_t *)var_18 = 4202297UL;
            indirect_placeholder();
            *var_13 = var_12;
            var_19 = helper_cc_compute_c_wrapper((uint64_t)*var_7 - var_12, var_12, var_2, 17U);
            local_sp_4 = var_18;
            if ((uint64_t)(uint32_t)var_17 != 0UL & var_19 == 0UL) {
                *var_7 = (uint32_t)*var_13;
            }
            var_20 = *var_9 + 1U;
            *var_9 = var_20;
            var_14 = var_20;
            local_sp_5 = local_sp_4;
            local_sp_6 = local_sp_5;
            local_sp_7 = local_sp_5;
        }
    if (*var_4 != 0UL) {
        *var_9 = 1U;
        while ((int)var_29 <= (int)64U)
            {
                var_30 = (uint64_t)var_29;
                var_31 = local_sp_7 + (-8L);
                *(uint64_t *)var_31 = 4202444UL;
                var_32 = indirect_placeholder_5(var_30, var_12);
                local_sp_2 = var_31;
                if ((uint64_t)(uint32_t)var_32 == 0UL) {
                    var_33 = (uint64_t)*var_7;
                    var_34 = (uint64_t)*var_8;
                    var_35 = (uint64_t)*var_9;
                    var_36 = local_sp_7 + (-16L);
                    *(uint64_t *)var_36 = 4202468UL;
                    indirect_placeholder_42(var_33, var_12, var_34, var_35);
                    local_sp_2 = var_36;
                }
                var_37 = *var_9 + 1U;
                *var_9 = var_37;
                var_29 = var_37;
                local_sp_7 = local_sp_2;
            }
    }
    var_21 = **(uint64_t **)var_3;
    while (var_21 != 0UL)
        {
            var_22 = local_sp_6 + (-8L);
            *(uint64_t *)var_22 = 4202358UL;
            var_23 = indirect_placeholder_5(var_21, var_12);
            var_24 = (uint32_t)var_23;
            *var_9 = var_24;
            local_sp_3 = var_22;
            if ((int)var_24 > (int)4294967295U) {
                var_25 = (uint64_t)*var_7;
                var_26 = (uint64_t)*var_8;
                var_27 = (uint64_t)var_24;
                var_28 = local_sp_6 + (-16L);
                *(uint64_t *)var_28 = 4202396UL;
                indirect_placeholder_42(var_25, var_12, var_26, var_27);
                local_sp_3 = var_28;
            } else {
                *var_6 = 1U;
            }
            *var_4 = (*var_4 + 8UL);
            local_sp_6 = local_sp_3;
            var_21 = **(uint64_t **)var_3;
        }
    return (uint64_t)*var_6;
}

typedef unsigned char __u_char;
typedef unsigned short __u_short;
typedef unsigned int __u_int;
typedef unsigned long __u_long;
typedef signed char __int8_t;
typedef unsigned char __uint8_t;
typedef short __int16_t;
typedef unsigned short __uint16_t;
typedef int __int32_t;
typedef unsigned int __uint32_t;
typedef long __int64_t;
typedef unsigned long __uint64_t;
typedef __int8_t __int_least8_t;
typedef __uint8_t __uint_least8_t;
typedef __int16_t __int_least16_t;
typedef __uint16_t __uint_least16_t;
typedef __int32_t __int_least32_t;
typedef __uint32_t __uint_least32_t;
typedef __int64_t __int_least64_t;
typedef __uint64_t __uint_least64_t;
typedef long __quad_t;
typedef unsigned long __u_quad_t;
typedef long __intmax_t;
typedef unsigned long __uintmax_t;
typedef unsigned long __dev_t;
typedef unsigned int __uid_t;
typedef unsigned int __gid_t;
typedef unsigned long __ino_t;
typedef unsigned long __ino64_t;
typedef unsigned int __mode_t;
typedef unsigned long __nlink_t;
typedef long __off_t;
typedef long __off64_t;
typedef int __pid_t;
typedef struct {
    int __val[2];
} __fsid_t;
typedef long __clock_t;
typedef unsigned long __rlim_t;
typedef unsigned long __rlim64_t;
typedef unsigned int __id_t;
typedef long __time_t;
typedef unsigned int __useconds_t;
typedef long __suseconds_t;
typedef int __daddr_t;
typedef int __key_t;
typedef int __clockid_t;
typedef void *__timer_t;
typedef long __blksize_t;
typedef long __blkcnt_t;
typedef long __blkcnt64_t;
typedef unsigned long __fsblkcnt_t;
typedef unsigned long __fsblkcnt64_t;
typedef unsigned long __fsfilcnt_t;
typedef unsigned long __fsfilcnt64_t;
typedef long __fsword_t;
typedef long __ssize_t;
typedef long __syscall_slong_t;
typedef unsigned long __syscall_ulong_t;
typedef __off64_t __loff_t;
typedef char *__caddr_t;
typedef long __intptr_t;
typedef unsigned int __socklen_t;
typedef int __sig_atomic_t;
typedef __int8_t int8_t;
typedef __int16_t int16_t;
typedef __int32_t int32_t;
typedef __int64_t int64_t;
typedef __uint8_t uint8_t;
typedef __uint16_t uint16_t;
typedef __uint32_t uint32_t;
typedef __uint64_t uint64_t;
typedef __int_least8_t int_least8_t;
typedef __int_least16_t int_least16_t;
typedef __int_least32_t int_least32_t;
typedef __int_least64_t int_least64_t;
typedef __uint_least8_t uint_least8_t;
typedef __uint_least16_t uint_least16_t;
typedef __uint_least32_t uint_least32_t;
typedef __uint_least64_t uint_least64_t;
typedef signed char int_fast8_t;
typedef long int_fast16_t;
typedef long int_fast32_t;
typedef long int_fast64_t;
typedef unsigned char uint_fast8_t;
typedef unsigned long uint_fast16_t;
typedef unsigned long uint_fast32_t;
typedef unsigned long uint_fast64_t;
typedef long intptr_t;
typedef unsigned long uintptr_t;
typedef __intmax_t intmax_t;
typedef __uintmax_t uintmax_t;
struct indirect_placeholder_9_ret_type;
struct indirect_placeholder_9_ret_type {
    uint64_t field_0;
    uint64_t field_1;
};
extern void abort(void);
extern uint64_t revng_init_local_sp(uint64_t param_0);
extern uint64_t init_rbp(void);
extern void indirect_placeholder(void);
extern void indirect_placeholder_7(uint64_t param_0, uint64_t param_1);
extern void indirect_placeholder_6(uint64_t param_0);
extern void indirect_placeholder_8(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint64_t param_3, uint64_t param_4, uint64_t param_5);
extern struct indirect_placeholder_9_ret_type indirect_placeholder_9(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint64_t param_3, uint64_t param_4);
void bb_main(uint64_t rdi, uint64_t rsi) {
    struct indirect_placeholder_9_ret_type var_13;
    uint64_t var_0;
    uint64_t var_1;
    uint64_t var_2;
    uint32_t *var_3;
    uint64_t var_4;
    uint64_t *var_5;
    unsigned char *var_6;
    uint64_t var_7;
    uint64_t var_8;
    uint32_t *var_9;
    uint64_t local_sp_6;
    uint64_t local_sp_2;
    uint64_t local_sp_0;
    uint64_t var_37;
    uint64_t local_sp_1;
    uint32_t var_45;
    uint32_t var_32;
    unsigned char **_phi_trans_insert;
    unsigned char var_38;
    unsigned char **var_39;
    uint64_t var_40;
    unsigned char var_41;
    uint64_t var_42;
    unsigned char var_43;
    uint64_t var_44;
    uint64_t var_18;
    uint64_t var_19;
    uint64_t *var_20;
    uint64_t local_sp_3;
    uint64_t var_21;
    uint32_t *var_22;
    uint32_t var_23;
    uint32_t *var_24;
    unsigned char *var_25;
    uint64_t var_26;
    uint64_t *var_27;
    uint64_t var_28;
    uint64_t *var_29;
    uint64_t var_30;
    uint64_t *var_31;
    uint64_t local_sp_4;
    uint64_t var_33;
    uint32_t var_34;
    uint64_t var_35;
    uint64_t var_36;
    uint64_t local_sp_5;
    uint64_t var_15;
    uint64_t var_16;
    uint64_t *var_17;
    uint64_t var_10;
    uint64_t var_11;
    uint64_t var_12;
    uint32_t var_14;
    unsigned int loop_state_var;
    var_0 = revng_init_local_sp(0UL);
    var_1 = init_rbp();
    var_2 = var_0 + (-8L);
    *(uint64_t *)var_2 = var_1;
    var_3 = (uint32_t *)(var_0 + (-60L));
    *var_3 = (uint32_t)rdi;
    var_4 = var_0 + (-72L);
    var_5 = (uint64_t *)var_4;
    *var_5 = rsi;
    var_6 = (unsigned char *)(var_0 + (-38L));
    *var_6 = (unsigned char)'\x00';
    var_7 = **(uint64_t **)var_4;
    *(uint64_t *)(var_0 + (-80L)) = 4201588UL;
    indirect_placeholder_6(var_7);
    *(uint64_t *)(var_0 + (-88L)) = 4201603UL;
    indirect_placeholder();
    *(uint64_t *)(var_0 + (-96L)) = 4201613UL;
    indirect_placeholder_6(2UL);
    var_8 = var_0 + (-104L);
    *(uint64_t *)var_8 = 4201623UL;
    indirect_placeholder();
    var_9 = (uint32_t *)(var_0 + (-52L));
    local_sp_6 = var_8;
    while (1U)
        {
            var_10 = *var_5;
            var_11 = (uint64_t)*var_3;
            var_12 = local_sp_6 + (-8L);
            *(uint64_t *)var_12 = 4201777UL;
            var_13 = indirect_placeholder_9(4266016UL, 4265280UL, var_11, var_10, 0UL);
            var_14 = (uint32_t)var_13.field_0;
            *var_9 = var_14;
            local_sp_3 = var_12;
            local_sp_4 = var_12;
            local_sp_5 = var_12;
            local_sp_6 = var_12;
            if (var_14 == 4294967295U) {
                if ((uint64_t)(var_14 + 130U) == 0UL) {
                    *(uint64_t *)(local_sp_6 + (-16L)) = 4201665UL;
                    indirect_placeholder_7(var_2, 0UL);
                    abort();
                }
                if ((uint64_t)(var_14 + (-48)) == 0UL) {
                    *var_6 = (unsigned char)'\x01';
                    continue;
                }
                if (var_14 != 4294967165U) {
                    loop_state_var = 2U;
                    break;
                }
                var_15 = *(uint64_t *)6374336UL;
                var_16 = local_sp_6 + (-24L);
                var_17 = (uint64_t *)var_16;
                *var_17 = 0UL;
                *(uint64_t *)(local_sp_6 + (-32L)) = 4201723UL;
                indirect_placeholder_8(0UL, 4264984UL, var_15, 4265782UL, 4265983UL, 4266000UL);
                *var_17 = 4201737UL;
                indirect_placeholder();
                local_sp_5 = var_16;
                loop_state_var = 2U;
                break;
            }
            if ((long)((uint64_t)*(uint32_t *)6374488UL << 32UL) >= (long)((uint64_t)*var_3 << 32UL)) {
                var_18 = *(uint64_t *)6374592UL;
                var_19 = var_0 + (-16L);
                var_20 = (uint64_t *)var_19;
                *var_20 = var_18;
                loop_state_var = 1U;
                break;
            }
            var_22 = (uint32_t *)(var_0 + (-44L));
            *var_22 = 0U;
            var_23 = *(uint32_t *)6374488UL;
            var_24 = (uint32_t *)(var_0 + (-36L));
            *var_24 = var_23;
            var_25 = (unsigned char *)(var_0 + (-45L));
            var_26 = var_0 + (-16L);
            var_27 = (uint64_t *)var_26;
            var_28 = var_0 + (-24L);
            var_29 = (uint64_t *)var_28;
            var_30 = var_0 + (-32L);
            var_31 = (uint64_t *)var_30;
            var_32 = var_23;
            var_33 = (uint64_t)var_32;
            var_34 = *var_3;
            while ((long)(var_33 << 32UL) >= (long)((uint64_t)var_34 << 32UL))
                {
                    *var_25 = (unsigned char)'\x00';
                    var_35 = *(uint64_t *)(*var_5 + ((uint64_t)*var_24 << 3UL));
                    var_36 = local_sp_4 + (-8L);
                    *(uint64_t *)var_36 = 4201944UL;
                    indirect_placeholder();
                    local_sp_0 = var_36;
                    local_sp_1 = var_36;
                    if (var_35 != 0UL) {
                        *var_27 = *(uint64_t *)6374592UL;
                        var_37 = **(uint64_t **)var_26;
                        local_sp_1 = local_sp_0;
                        local_sp_2 = local_sp_0;
                        while (var_37 != 0UL)
                            {
                                *var_29 = var_37;
                                *var_31 = *(uint64_t *)(*var_5 + ((uint64_t)*var_24 << 3UL));
                                _phi_trans_insert = (unsigned char **)var_28;
                                var_38 = **_phi_trans_insert;
                                while (1U)
                                    {
                                        if (var_38 != '\x00') {
                                            loop_state_var = 0U;
                                            break;
                                        }
                                        var_39 = (unsigned char **)var_30;
                                        if (**var_39 != '\x00') {
                                            loop_state_var = 0U;
                                            break;
                                        }
                                        var_40 = *var_29;
                                        *var_29 = (var_40 + 1UL);
                                        var_41 = *(unsigned char *)var_40;
                                        var_42 = *var_31;
                                        *var_31 = (var_42 + 1UL);
                                        if ((uint64_t)(var_41 - *(unsigned char *)var_42) != 0UL) {
                                            loop_state_var = 0U;
                                            break;
                                        }
                                        var_43 = **_phi_trans_insert;
                                        var_38 = var_43;
                                        if (var_43 != '=') {
                                            continue;
                                        }
                                        if (**var_39 == '\x00') {
                                            continue;
                                        }
                                        loop_state_var = 1U;
                                        break;
                                    }
                                switch (loop_state_var) {
                                  case 1U:
                                    {
                                        var_44 = local_sp_0 + (-8L);
                                        *(uint64_t *)var_44 = 4202076UL;
                                        indirect_placeholder();
                                        *var_25 = (unsigned char)'\x01';
                                        local_sp_2 = var_44;
                                    }
                                    break;
                                  case 0U:
                                    {
                                        *var_27 = (*var_27 + 8UL);
                                        local_sp_0 = local_sp_2;
                                        var_37 = **(uint64_t **)var_26;
                                        local_sp_1 = local_sp_0;
                                        local_sp_2 = local_sp_0;
                                        continue;
                                    }
                                    break;
                                }
                            }
                        *var_22 = (*var_22 + (uint32_t)*var_25);
                    }
                    var_45 = *var_24 + 1U;
                    *var_24 = var_45;
                    var_32 = var_45;
                    local_sp_4 = local_sp_1;
                    var_33 = (uint64_t)var_32;
                    var_34 = *var_3;
                }
            *(unsigned char *)(var_0 + (-37L)) = ((uint64_t)((var_34 - *(uint32_t *)6374488UL) - *var_22) == 0UL);
            loop_state_var = 0U;
            break;
        }
    switch (loop_state_var) {
      case 2U:
        {
            *(uint64_t *)(local_sp_5 + (-8L)) = 4201747UL;
            indirect_placeholder_7(var_2, 2UL);
            abort();
        }
        break;
      case 1U:
      case 0U:
        {
            switch (loop_state_var) {
              case 0U:
                {
                    return;
                }
                break;
              case 1U:
                {
                    while (**(uint64_t **)var_19 != 0UL)
                        {
                            var_21 = local_sp_3 + (-8L);
                            *(uint64_t *)var_21 = 4201857UL;
                            indirect_placeholder();
                            *var_20 = (*var_20 + 8UL);
                            local_sp_3 = var_21;
                        }
                    *(unsigned char *)(var_0 + (-37L)) = (unsigned char)'\x01';
                }
                break;
            }
        }
        break;
    }
}

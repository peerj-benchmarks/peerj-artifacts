typedef unsigned char __u_char;
typedef unsigned short __u_short;
typedef unsigned int __u_int;
typedef unsigned long __u_long;
typedef signed char __int8_t;
typedef unsigned char __uint8_t;
typedef short __int16_t;
typedef unsigned short __uint16_t;
typedef int __int32_t;
typedef unsigned int __uint32_t;
typedef long __int64_t;
typedef unsigned long __uint64_t;
typedef __int8_t __int_least8_t;
typedef __uint8_t __uint_least8_t;
typedef __int16_t __int_least16_t;
typedef __uint16_t __uint_least16_t;
typedef __int32_t __int_least32_t;
typedef __uint32_t __uint_least32_t;
typedef __int64_t __int_least64_t;
typedef __uint64_t __uint_least64_t;
typedef long __quad_t;
typedef unsigned long __u_quad_t;
typedef long __intmax_t;
typedef unsigned long __uintmax_t;
typedef unsigned long __dev_t;
typedef unsigned int __uid_t;
typedef unsigned int __gid_t;
typedef unsigned long __ino_t;
typedef unsigned long __ino64_t;
typedef unsigned int __mode_t;
typedef unsigned long __nlink_t;
typedef long __off_t;
typedef long __off64_t;
typedef int __pid_t;
typedef struct {
    int __val[2];
} __fsid_t;
typedef long __clock_t;
typedef unsigned long __rlim_t;
typedef unsigned long __rlim64_t;
typedef unsigned int __id_t;
typedef long __time_t;
typedef unsigned int __useconds_t;
typedef long __suseconds_t;
typedef int __daddr_t;
typedef int __key_t;
typedef int __clockid_t;
typedef void *__timer_t;
typedef long __blksize_t;
typedef long __blkcnt_t;
typedef long __blkcnt64_t;
typedef unsigned long __fsblkcnt_t;
typedef unsigned long __fsblkcnt64_t;
typedef unsigned long __fsfilcnt_t;
typedef unsigned long __fsfilcnt64_t;
typedef long __fsword_t;
typedef long __ssize_t;
typedef long __syscall_slong_t;
typedef unsigned long __syscall_ulong_t;
typedef __off64_t __loff_t;
typedef char *__caddr_t;
typedef long __intptr_t;
typedef unsigned int __socklen_t;
typedef int __sig_atomic_t;
typedef __int8_t int8_t;
typedef __int16_t int16_t;
typedef __int32_t int32_t;
typedef __int64_t int64_t;
typedef __uint8_t uint8_t;
typedef __uint16_t uint16_t;
typedef __uint32_t uint32_t;
typedef __uint64_t uint64_t;
typedef __int_least8_t int_least8_t;
typedef __int_least16_t int_least16_t;
typedef __int_least32_t int_least32_t;
typedef __int_least64_t int_least64_t;
typedef __uint_least8_t uint_least8_t;
typedef __uint_least16_t uint_least16_t;
typedef __uint_least32_t uint_least32_t;
typedef __uint_least64_t uint_least64_t;
typedef signed char int_fast8_t;
typedef long int_fast16_t;
typedef long int_fast32_t;
typedef long int_fast64_t;
typedef unsigned char uint_fast8_t;
typedef unsigned long uint_fast16_t;
typedef unsigned long uint_fast32_t;
typedef unsigned long uint_fast64_t;
typedef long intptr_t;
typedef unsigned long uintptr_t;
typedef __intmax_t intmax_t;
typedef __uintmax_t uintmax_t;
typedef _Bool bool;
struct indirect_placeholder_12_ret_type;
struct indirect_placeholder_12_ret_type {
    uint64_t field_0;
    uint64_t field_1;
    uint64_t field_2;
};
extern void abort(void);
extern uint64_t revng_init_local_sp(uint64_t param_0);
extern uint64_t init_rbp(void);
extern uint64_t indirect_placeholder_3(uint64_t param_0, uint64_t param_1);
extern void indirect_placeholder(void);
extern uint64_t indirect_placeholder_8(uint64_t param_0, uint64_t param_1, uint64_t param_2);
extern uint64_t init_rbx(void);
extern void indirect_placeholder_11(uint64_t param_0, uint64_t param_1);
extern uint64_t indirect_placeholder_9(void);
extern void indirect_placeholder_2(uint64_t param_0);
extern uint64_t indirect_placeholder_5(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint64_t param_3, uint64_t param_4, uint64_t param_5);
extern void indirect_placeholder_7(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint64_t param_3, uint64_t param_4, uint64_t param_5, uint64_t param_6);
extern void indirect_placeholder_10(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint64_t param_3, uint64_t param_4, uint64_t param_5);
extern struct indirect_placeholder_12_ret_type indirect_placeholder_12(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint64_t param_3, uint64_t param_4);
void bb_main(uint64_t rdi, uint64_t rsi) {
    struct indirect_placeholder_12_ret_type var_23;
    uint32_t var_51;
    uint64_t var_0;
    uint64_t var_1;
    uint64_t var_2;
    uint64_t var_3;
    uint32_t *var_4;
    uint64_t var_5;
    uint64_t *var_6;
    unsigned char *var_7;
    uint64_t *var_8;
    uint64_t *var_9;
    uint64_t **var_10;
    uint64_t var_11;
    uint64_t var_12;
    uint32_t var_13;
    uint32_t var_17;
    uint64_t var_57;
    uint64_t var_58;
    uint64_t var_59;
    uint64_t var_60;
    uint64_t r8_1;
    uint64_t var_55;
    uint64_t var_56;
    uint64_t var_61;
    uint64_t var_62;
    uint64_t var_63;
    uint64_t var_64;
    uint64_t local_sp_0;
    uint32_t var_67;
    uint64_t r9_1;
    uint64_t var_65;
    uint64_t var_66;
    uint64_t var_54;
    uint64_t local_sp_2;
    uint64_t var_33;
    uint64_t var_34;
    uint64_t var_35;
    uint32_t var_39;
    uint64_t var_36;
    uint64_t var_37;
    uint32_t var_38;
    uint64_t local_sp_1;
    uint64_t r9_0;
    uint64_t r8_0;
    uint64_t var_40;
    bool var_41;
    uint64_t var_42;
    uint64_t var_43;
    uint64_t var_44;
    uint32_t var_45;
    uint64_t var_46;
    uint32_t var_47;
    uint32_t var_48;
    uint64_t var_68;
    uint64_t *var_49;
    uint64_t *var_50;
    uint32_t var_52;
    uint64_t local_sp_3;
    uint64_t var_53;
    uint64_t var_25;
    uint64_t var_26;
    uint64_t var_27;
    uint64_t local_sp_5_be;
    uint32_t var_19;
    uint64_t local_sp_4;
    uint64_t var_28;
    uint64_t var_29;
    uint64_t var_30;
    uint64_t var_31;
    uint64_t var_32;
    uint64_t var_14;
    uint64_t var_15;
    uint32_t var_16;
    uint32_t *var_18;
    uint64_t local_sp_5;
    uint64_t var_20;
    uint64_t var_21;
    uint64_t var_22;
    uint32_t var_24;
    unsigned int loop_state_var;
    var_0 = revng_init_local_sp(0UL);
    var_1 = init_rbp();
    var_2 = init_rbx();
    var_3 = var_0 + (-8L);
    *(uint64_t *)var_3 = var_1;
    *(uint64_t *)(var_0 + (-16L)) = var_2;
    var_4 = (uint32_t *)(var_0 + (-76L));
    *var_4 = (uint32_t)rdi;
    var_5 = var_0 + (-88L);
    var_6 = (uint64_t *)var_5;
    *var_6 = rsi;
    var_7 = (unsigned char *)(var_0 + (-25L));
    *var_7 = (unsigned char)'\x01';
    var_8 = (uint64_t *)(var_0 + (-40L));
    *var_8 = 0UL;
    var_9 = (uint64_t *)(var_0 + (-48L));
    *var_9 = 0UL;
    var_10 = (uint64_t **)var_5;
    var_11 = **var_10;
    *(uint64_t *)(var_0 + (-96L)) = 4202312UL;
    indirect_placeholder_2(var_11);
    *(uint64_t *)(var_0 + (-104L)) = 4202327UL;
    indirect_placeholder();
    var_12 = var_0 + (-112L);
    *(uint64_t *)var_12 = 4202337UL;
    indirect_placeholder();
    *(unsigned char *)6384515UL = (unsigned char)'\x00';
    *(unsigned char *)6384514UL = (unsigned char)'\x00';
    *(unsigned char *)6384513UL = (unsigned char)'\x00';
    *(unsigned char *)6384512UL = (unsigned char)'\x00';
    *(uint32_t *)6384528UL = 75U;
    *(uint64_t *)6384520UL = 4272955UL;
    *(uint32_t *)6384532UL = 0U;
    *(uint32_t *)6384536UL = 0U;
    *(uint32_t *)6384540UL = 0U;
    var_13 = *var_4;
    var_17 = var_13;
    r8_1 = 4274497UL;
    r9_1 = 0UL;
    r9_0 = 0UL;
    r8_0 = 4274497UL;
    local_sp_5 = var_12;
    var_14 = *var_6 + 8UL;
    var_15 = *(uint64_t *)var_14 + 1UL;
    if ((int)var_13 <= (int)1U & **(unsigned char **)var_14 != '-' & (uint64_t)(((uint32_t)(uint64_t)*(unsigned char *)var_15 + (-48)) & (-2)) > 9UL) {
        *var_8 = var_15;
        *(uint64_t *)(*var_6 + 8UL) = **var_10;
        *var_6 = (*var_6 + 8UL);
        var_16 = *var_4 + (-1);
        *var_4 = var_16;
        var_17 = var_16;
    }
    var_18 = (uint32_t *)(var_0 + (-52L));
    var_19 = var_17;
    while (1U)
        {
            var_20 = *var_6;
            var_21 = (uint64_t)var_19;
            var_22 = local_sp_5 + (-8L);
            *(uint64_t *)var_22 = 4202871UL;
            var_23 = indirect_placeholder_12(4274476UL, 4274048UL, var_21, var_20, 0UL);
            var_24 = (uint32_t)var_23.field_0;
            *var_18 = var_24;
            local_sp_1 = var_22;
            local_sp_4 = var_22;
            local_sp_5_be = var_22;
            if (var_24 != 4294967295U) {
                var_33 = var_23.field_1;
                var_34 = var_23.field_2;
                var_35 = *var_8;
                if (var_35 != 0UL) {
                    var_39 = *(uint32_t *)6384528UL;
                    r9_0 = var_33;
                    r8_0 = var_34;
                    loop_state_var = 0U;
                    break;
                }
                var_36 = local_sp_5 + (-16L);
                *(uint64_t *)var_36 = 4202930UL;
                var_37 = indirect_placeholder_5(2500UL, 4272955UL, var_35, 0UL, 0UL, 4274497UL);
                var_38 = (uint32_t)var_37;
                *(uint32_t *)6384528UL = var_38;
                var_39 = var_38;
                local_sp_1 = var_36;
                loop_state_var = 0U;
                break;
            }
            var_25 = (uint64_t)var_24;
            if ((uint64_t)(var_24 + (-112)) == 0UL) {
                var_31 = *(uint64_t *)6430104UL;
                var_32 = local_sp_5 + (-16L);
                *(uint64_t *)var_32 = 4202767UL;
                indirect_placeholder_2(var_31);
                local_sp_5_be = var_32;
            } else {
                if ((int)var_24 > (int)112U) {
                    if ((uint64_t)(var_24 + (-116)) == 0UL) {
                        *(unsigned char *)6384513UL = (unsigned char)'\x01';
                    } else {
                        if ((int)var_24 > (int)116U) {
                            if ((uint64_t)(var_24 + (-117)) == 0UL) {
                                *(unsigned char *)6384515UL = (unsigned char)'\x01';
                            } else {
                                if ((uint64_t)(var_24 + (-119)) != 0UL) {
                                    loop_state_var = 1U;
                                    break;
                                }
                                *var_8 = *(uint64_t *)6430104UL;
                            }
                        } else {
                            if ((uint64_t)(var_24 + (-115)) != 0UL) {
                                loop_state_var = 1U;
                                break;
                            }
                            *(unsigned char *)6384514UL = (unsigned char)'\x01';
                        }
                    }
                } else {
                    if ((uint64_t)(var_24 + 130U) == 0UL) {
                        *(uint64_t *)(local_sp_5 + (-16L)) = 4202779UL;
                        indirect_placeholder_11(var_3, 0UL);
                        abort();
                    }
                    if ((int)var_24 > (int)4294967166U) {
                        if ((uint64_t)(var_24 + (-99)) == 0UL) {
                            *(unsigned char *)6384512UL = (unsigned char)'\x01';
                        } else {
                            if ((uint64_t)(var_24 + (-103)) != 0UL) {
                                loop_state_var = 1U;
                                break;
                            }
                            *var_9 = *(uint64_t *)6430104UL;
                        }
                    } else {
                        if ((uint64_t)(var_24 + 131U) != 0UL) {
                            loop_state_var = 1U;
                            break;
                        }
                        var_26 = *(uint64_t *)6384128UL;
                        *(uint64_t *)(local_sp_5 + (-16L)) = 4202831UL;
                        indirect_placeholder_10(0UL, 4272792UL, var_26, 4273910UL, 0UL, 4274462UL);
                        var_27 = local_sp_5 + (-24L);
                        *(uint64_t *)var_27 = 4202841UL;
                        indirect_placeholder();
                        local_sp_5_be = var_27;
                    }
                }
            }
            var_19 = *var_4;
            local_sp_5 = local_sp_5_be;
            continue;
        }
    switch (loop_state_var) {
      case 0U:
        {
            var_40 = *var_9;
            var_41 = (var_40 == 0UL);
            var_42 = (uint64_t)var_39;
            local_sp_2 = local_sp_1;
            if (var_41) {
                var_46 = var_42 * 803158884352UL;
                *(uint32_t *)6384544UL = ((uint32_t)(uint64_t)((long)((uint64_t)((long)var_46 >> (long)32UL) * 1374389535UL) >> (long)38UL) - (uint32_t)(uint64_t)((long)var_46 >> (long)63UL));
                r9_1 = r9_0;
                r8_1 = r8_0;
            } else {
                var_43 = local_sp_1 + (-8L);
                *(uint64_t *)var_43 = 4202986UL;
                var_44 = indirect_placeholder_5(var_42, 4272955UL, var_40, 0UL, 0UL, 4274497UL);
                var_45 = (uint32_t)var_44;
                *(uint32_t *)6384544UL = var_45;
                local_sp_2 = var_43;
                if (*var_8 == 0UL) {
                    *(uint32_t *)6384528UL = (var_45 + 10U);
                }
            }
            var_47 = *(uint32_t *)6384280UL;
            var_48 = *var_4;
            var_51 = var_48;
            var_52 = var_47;
            local_sp_3 = local_sp_2;
            if ((uint64_t)(var_47 - var_48) != 0UL) {
                var_49 = (uint64_t *)(var_0 + (-64L));
                var_50 = (uint64_t *)(var_0 + (-72L));
                while ((long)((uint64_t)var_52 << 32UL) >= (long)((uint64_t)var_51 << 32UL))
                    {
                        var_53 = *(uint64_t *)(*var_6 + ((uint64_t)var_52 << 3UL));
                        *var_49 = var_53;
                        *(uint64_t *)(local_sp_3 + (-8L)) = 4203138UL;
                        indirect_placeholder();
                        if ((uint64_t)(uint32_t)var_53 == 0UL) {
                            var_65 = *(uint64_t *)6384328UL;
                            var_66 = local_sp_3 + (-16L);
                            *(uint64_t *)var_66 = 4203157UL;
                            indirect_placeholder_2(var_65);
                            local_sp_0 = var_66;
                        } else {
                            var_54 = *var_49;
                            *(uint64_t *)(local_sp_3 + (-16L)) = 4203179UL;
                            indirect_placeholder();
                            *var_50 = var_54;
                            if (var_54 == 0UL) {
                                var_61 = *var_49;
                                *(uint64_t *)(local_sp_3 + (-24L)) = 4203299UL;
                                var_62 = indirect_placeholder_3(4UL, var_61);
                                *(uint64_t *)(local_sp_3 + (-32L)) = 4203307UL;
                                indirect_placeholder();
                                var_63 = (uint64_t)*(uint32_t *)var_62;
                                var_64 = local_sp_3 + (-40L);
                                *(uint64_t *)var_64 = 4203334UL;
                                indirect_placeholder_7(0UL, 4274518UL, var_62, 0UL, var_63, r9_1, r8_1);
                                *var_7 = (unsigned char)'\x00';
                                local_sp_0 = var_64;
                            } else {
                                *(uint64_t *)(local_sp_3 + (-24L)) = 4203202UL;
                                indirect_placeholder_2(var_54);
                                var_55 = local_sp_3 + (-32L);
                                *(uint64_t *)var_55 = 4203214UL;
                                var_56 = indirect_placeholder_9();
                                local_sp_0 = var_55;
                                if ((uint64_t)((uint32_t)var_56 + 1U) == 0UL) {
                                    var_57 = *var_49;
                                    *(uint64_t *)(local_sp_3 + (-40L)) = 4203241UL;
                                    var_58 = indirect_placeholder_8(var_57, 0UL, 3UL);
                                    *(uint64_t *)(local_sp_3 + (-48L)) = 4203249UL;
                                    indirect_placeholder();
                                    var_59 = (uint64_t)*(uint32_t *)var_58;
                                    var_60 = local_sp_3 + (-56L);
                                    *(uint64_t *)var_60 = 4203276UL;
                                    indirect_placeholder_7(0UL, 4274515UL, var_58, 0UL, var_59, r9_1, r8_1);
                                    *var_7 = (unsigned char)'\x00';
                                    local_sp_0 = var_60;
                                }
                            }
                        }
                        var_67 = *(uint32_t *)6384280UL + 1U;
                        *(uint32_t *)6384280UL = var_67;
                        var_51 = *var_4;
                        var_52 = var_67;
                        local_sp_3 = local_sp_0;
                    }
            }
            var_68 = *(uint64_t *)6384328UL;
            *(uint64_t *)(local_sp_2 + (-8L)) = 4203081UL;
            indirect_placeholder_2(var_68);
            return;
        }
        break;
      case 1U:
        {
            if ((var_24 + (-48)) <= 9U) {
                var_28 = var_23.field_2;
                var_29 = var_23.field_1;
                var_30 = local_sp_5 + (-16L);
                *(uint64_t *)var_30 = 4202674UL;
                indirect_placeholder_7(0UL, 4274368UL, var_25, 0UL, 0UL, var_29, var_28);
                local_sp_4 = var_30;
            }
            *(uint64_t *)(local_sp_4 + (-8L)) = 4202684UL;
            indirect_placeholder_11(var_3, 1UL);
            abort();
        }
        break;
    }
}

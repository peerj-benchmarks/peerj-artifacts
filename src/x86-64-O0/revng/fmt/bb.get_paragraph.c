typedef unsigned char __u_char;
typedef unsigned short __u_short;
typedef unsigned int __u_int;
typedef unsigned long __u_long;
typedef signed char __int8_t;
typedef unsigned char __uint8_t;
typedef short __int16_t;
typedef unsigned short __uint16_t;
typedef int __int32_t;
typedef unsigned int __uint32_t;
typedef long __int64_t;
typedef unsigned long __uint64_t;
typedef __int8_t __int_least8_t;
typedef __uint8_t __uint_least8_t;
typedef __int16_t __int_least16_t;
typedef __uint16_t __uint_least16_t;
typedef __int32_t __int_least32_t;
typedef __uint32_t __uint_least32_t;
typedef __int64_t __int_least64_t;
typedef __uint64_t __uint_least64_t;
typedef long __quad_t;
typedef unsigned long __u_quad_t;
typedef long __intmax_t;
typedef unsigned long __uintmax_t;
typedef unsigned long __dev_t;
typedef unsigned int __uid_t;
typedef unsigned int __gid_t;
typedef unsigned long __ino_t;
typedef unsigned long __ino64_t;
typedef unsigned int __mode_t;
typedef unsigned long __nlink_t;
typedef long __off_t;
typedef long __off64_t;
typedef int __pid_t;
typedef struct {
    int __val[2];
} __fsid_t;
typedef long __clock_t;
typedef unsigned long __rlim_t;
typedef unsigned long __rlim64_t;
typedef unsigned int __id_t;
typedef long __time_t;
typedef unsigned int __useconds_t;
typedef long __suseconds_t;
typedef int __daddr_t;
typedef int __key_t;
typedef int __clockid_t;
typedef void *__timer_t;
typedef long __blksize_t;
typedef long __blkcnt_t;
typedef long __blkcnt64_t;
typedef unsigned long __fsblkcnt_t;
typedef unsigned long __fsblkcnt64_t;
typedef unsigned long __fsfilcnt_t;
typedef unsigned long __fsfilcnt64_t;
typedef long __fsword_t;
typedef long __ssize_t;
typedef long __syscall_slong_t;
typedef unsigned long __syscall_ulong_t;
typedef __off64_t __loff_t;
typedef char *__caddr_t;
typedef long __intptr_t;
typedef unsigned int __socklen_t;
typedef int __sig_atomic_t;
typedef __int8_t int8_t;
typedef __int16_t int16_t;
typedef __int32_t int32_t;
typedef __int64_t int64_t;
typedef __uint8_t uint8_t;
typedef __uint16_t uint16_t;
typedef __uint32_t uint32_t;
typedef __uint64_t uint64_t;
typedef __int_least8_t int_least8_t;
typedef __int_least16_t int_least16_t;
typedef __int_least32_t int_least32_t;
typedef __int_least64_t int_least64_t;
typedef __uint_least8_t uint_least8_t;
typedef __uint_least16_t uint_least16_t;
typedef __uint_least32_t uint_least32_t;
typedef __uint_least64_t uint_least64_t;
typedef signed char int_fast8_t;
typedef long int_fast16_t;
typedef long int_fast32_t;
typedef long int_fast64_t;
typedef unsigned char uint_fast8_t;
typedef unsigned long uint_fast16_t;
typedef unsigned long uint_fast32_t;
typedef unsigned long uint_fast64_t;
typedef long intptr_t;
typedef unsigned long uintptr_t;
typedef __intmax_t intmax_t;
typedef __uintmax_t uintmax_t;
extern uint64_t revng_init_local_sp(uint64_t param_0);
extern uint64_t init_rbp(void);
extern uint64_t indirect_placeholder_3(uint64_t param_0, uint64_t param_1);
extern void indirect_placeholder(void);
extern uint64_t indirect_placeholder_9(void);
extern void indirect_placeholder_2(uint64_t param_0);
extern uint64_t indirect_placeholder_1(uint64_t param_0);
typedef _Bool bool;
uint64_t bb_get_paragraph(uint64_t rdi) {
    uint64_t var_0;
    uint64_t var_1;
    uint64_t var_2;
    uint64_t *var_3;
    uint32_t var_4;
    uint32_t *var_5;
    uint32_t var_6;
    uint64_t var_38;
    uint64_t var_39;
    uint64_t var_40;
    uint32_t var_41;
    uint32_t var_34;
    uint64_t local_sp_0;
    uint64_t var_28;
    uint64_t var_29;
    uint32_t var_30;
    uint64_t var_31;
    uint64_t var_32;
    uint64_t var_33;
    uint64_t local_sp_1;
    uint64_t var_19;
    uint64_t var_20;
    uint32_t var_21;
    uint64_t var_22;
    uint64_t var_23;
    uint64_t var_24;
    uint64_t var_16;
    uint64_t var_17;
    uint64_t var_18;
    uint64_t local_sp_2;
    uint64_t var_25;
    uint64_t var_26;
    uint64_t var_27;
    uint64_t var_35;
    uint64_t var_36;
    uint64_t var_37;
    uint64_t local_sp_3;
    uint64_t var_42;
    uint64_t var_43;
    unsigned char *var_44;
    uint32_t storemerge1;
    uint64_t storemerge;
    uint64_t var_49;
    uint64_t var_50;
    uint32_t var_51;
    uint64_t local_sp_4;
    uint32_t var_7;
    uint64_t var_8;
    uint64_t var_9;
    uint64_t var_10;
    uint32_t var_11;
    uint64_t var_12;
    uint64_t var_13;
    uint64_t var_14;
    uint64_t var_15;
    uint64_t var_45;
    uint64_t var_46;
    uint64_t var_47;
    uint32_t var_48;
    unsigned int loop_state_var;
    bool switch_state_var;
    var_0 = revng_init_local_sp(0UL);
    var_1 = init_rbp();
    *(uint64_t *)(var_0 + (-8L)) = var_1;
    var_2 = var_0 + (-40L);
    var_3 = (uint64_t *)(var_0 + (-32L));
    *var_3 = rdi;
    *(uint32_t *)6429664UL = 0U;
    var_4 = *(uint32_t *)6429656UL;
    var_5 = (uint32_t *)(var_0 + (-12L));
    *var_5 = var_4;
    storemerge1 = 4294967295U;
    storemerge = 1UL;
    var_6 = var_4;
    local_sp_4 = var_2;
    while (1U)
        {
            var_45 = (uint64_t)var_6;
            var_46 = *var_3;
            *(uint64_t *)(local_sp_4 + (-8L)) = 4203895UL;
            var_47 = indirect_placeholder_3(var_46, var_45);
            var_48 = (uint32_t)var_47;
            *var_5 = var_48;
            storemerge = 0UL;
            if (var_48 != 4294967295U) {
                loop_state_var = 4U;
                switch_state_var = 1;
                break;
            }
            *(uint64_t *)(local_sp_4 + (-16L)) = 4203934UL;
            indirect_placeholder();
            var_49 = local_sp_4 + (-24L);
            *(uint64_t *)var_49 = 4203946UL;
            var_50 = indirect_placeholder_9();
            var_51 = (uint32_t)var_50;
            *var_5 = var_51;
            var_6 = var_51;
            local_sp_4 = var_49;
            continue;
        }
    switch (loop_state_var) {
      case 4U:
        {
            *(uint32_t *)6429656UL = storemerge1;
            return storemerge;
        }
        break;
      case 3U:
      case 2U:
      case 1U:
      case 0U:
        {
            switch (loop_state_var) {
              case 2U:
                {
                    var_19 = (uint64_t)*var_5;
                    *(uint64_t *)(local_sp_1 + (-8L)) = 4204148UL;
                    var_20 = indirect_placeholder_1(var_19);
                    var_21 = (uint32_t)var_20;
                    *var_5 = var_21;
                    var_22 = (uint64_t)var_21;
                    var_23 = local_sp_1 + (-16L);
                    *(uint64_t *)var_23 = 4204161UL;
                    var_24 = indirect_placeholder_1(var_22);
                    local_sp_1 = var_23;
                    local_sp_3 = var_23;
                    do {
                        var_19 = (uint64_t)*var_5;
                        *(uint64_t *)(local_sp_1 + (-8L)) = 4204148UL;
                        var_20 = indirect_placeholder_1(var_19);
                        var_21 = (uint32_t)var_20;
                        *var_5 = var_21;
                        var_22 = (uint64_t)var_21;
                        var_23 = local_sp_1 + (-16L);
                        *(uint64_t *)var_23 = 4204161UL;
                        var_24 = indirect_placeholder_1(var_22);
                        local_sp_1 = var_23;
                        local_sp_3 = var_23;
                    } while ((uint64_t)(unsigned char)var_24 != 0UL);
                }
                break;
              case 0U:
                {
                    var_35 = (uint64_t)var_34;
                    var_36 = local_sp_2 + (-8L);
                    *(uint64_t *)var_36 = 4204315UL;
                    var_37 = indirect_placeholder_1(var_35);
                    local_sp_3 = var_36;
                    while ((uint64_t)(unsigned char)var_37 != 0UL)
                        {
                            var_38 = (uint64_t)*var_5;
                            var_39 = local_sp_2 + (-16L);
                            *(uint64_t *)var_39 = 4204302UL;
                            var_40 = indirect_placeholder_1(var_38);
                            var_41 = (uint32_t)var_40;
                            *var_5 = var_41;
                            var_34 = var_41;
                            local_sp_2 = var_39;
                            var_35 = (uint64_t)var_34;
                            var_36 = local_sp_2 + (-8L);
                            *(uint64_t *)var_36 = 4204315UL;
                            var_37 = indirect_placeholder_1(var_35);
                            local_sp_3 = var_36;
                        }
                }
                break;
              case 1U:
                {
                    var_28 = (uint64_t)*var_5;
                    *(uint64_t *)(local_sp_0 + (-8L)) = 4204248UL;
                    var_29 = indirect_placeholder_1(var_28);
                    var_30 = (uint32_t)var_29;
                    *var_5 = var_30;
                    var_31 = (uint64_t)var_30;
                    var_32 = local_sp_0 + (-16L);
                    *(uint64_t *)var_32 = 4204261UL;
                    var_33 = indirect_placeholder_1(var_31);
                    local_sp_0 = var_32;
                    local_sp_3 = var_32;
                    do {
                        var_28 = (uint64_t)*var_5;
                        *(uint64_t *)(local_sp_0 + (-8L)) = 4204248UL;
                        var_29 = indirect_placeholder_1(var_28);
                        var_30 = (uint32_t)var_29;
                        *var_5 = var_30;
                        var_31 = (uint64_t)var_30;
                        var_32 = local_sp_0 + (-16L);
                        *(uint64_t *)var_32 = 4204261UL;
                        var_33 = indirect_placeholder_1(var_31);
                        local_sp_0 = var_32;
                        local_sp_3 = var_32;
                    } while ((uint64_t)(unsigned char)var_33 != 0UL);
                }
                break;
              case 3U:
                {
                    var_42 = *(uint64_t *)6429632UL;
                    var_43 = var_42;
                    if (var_42 <= 6389632UL) {
                        *(uint64_t *)(local_sp_3 + (-8L)) = 4204375UL;
                        indirect_placeholder();
                        var_43 = *(uint64_t *)6429632UL;
                    }
                    var_44 = (unsigned char *)(var_43 + (-24L));
                    *var_44 = (unsigned char)('\x02' | ((*var_44 | '\b') & '\xfd'));
                    storemerge1 = *var_5;
                }
                break;
            }
        }
        break;
    }
}

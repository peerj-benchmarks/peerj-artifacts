typedef unsigned char __u_char;
typedef unsigned short __u_short;
typedef unsigned int __u_int;
typedef unsigned long __u_long;
typedef signed char __int8_t;
typedef unsigned char __uint8_t;
typedef short __int16_t;
typedef unsigned short __uint16_t;
typedef int __int32_t;
typedef unsigned int __uint32_t;
typedef long __int64_t;
typedef unsigned long __uint64_t;
typedef __int8_t __int_least8_t;
typedef __uint8_t __uint_least8_t;
typedef __int16_t __int_least16_t;
typedef __uint16_t __uint_least16_t;
typedef __int32_t __int_least32_t;
typedef __uint32_t __uint_least32_t;
typedef __int64_t __int_least64_t;
typedef __uint64_t __uint_least64_t;
typedef long __quad_t;
typedef unsigned long __u_quad_t;
typedef long __intmax_t;
typedef unsigned long __uintmax_t;
typedef unsigned long __dev_t;
typedef unsigned int __uid_t;
typedef unsigned int __gid_t;
typedef unsigned long __ino_t;
typedef unsigned long __ino64_t;
typedef unsigned int __mode_t;
typedef unsigned long __nlink_t;
typedef long __off_t;
typedef long __off64_t;
typedef int __pid_t;
typedef struct {
    int __val[2];
} __fsid_t;
typedef long __clock_t;
typedef unsigned long __rlim_t;
typedef unsigned long __rlim64_t;
typedef unsigned int __id_t;
typedef long __time_t;
typedef unsigned int __useconds_t;
typedef long __suseconds_t;
typedef int __daddr_t;
typedef int __key_t;
typedef int __clockid_t;
typedef void *__timer_t;
typedef long __blksize_t;
typedef long __blkcnt_t;
typedef long __blkcnt64_t;
typedef unsigned long __fsblkcnt_t;
typedef unsigned long __fsblkcnt64_t;
typedef unsigned long __fsfilcnt_t;
typedef unsigned long __fsfilcnt64_t;
typedef long __fsword_t;
typedef long __ssize_t;
typedef long __syscall_slong_t;
typedef unsigned long __syscall_ulong_t;
typedef __off64_t __loff_t;
typedef char *__caddr_t;
typedef long __intptr_t;
typedef unsigned int __socklen_t;
typedef int __sig_atomic_t;
typedef __int8_t int8_t;
typedef __int16_t int16_t;
typedef __int32_t int32_t;
typedef __int64_t int64_t;
typedef __uint8_t uint8_t;
typedef __uint16_t uint16_t;
typedef __uint32_t uint32_t;
typedef __uint64_t uint64_t;
typedef __int_least8_t int_least8_t;
typedef __int_least16_t int_least16_t;
typedef __int_least32_t int_least32_t;
typedef __int_least64_t int_least64_t;
typedef __uint_least8_t uint_least8_t;
typedef __uint_least16_t uint_least16_t;
typedef __uint_least32_t uint_least32_t;
typedef __uint_least64_t uint_least64_t;
typedef signed char int_fast8_t;
typedef long int_fast16_t;
typedef long int_fast32_t;
typedef long int_fast64_t;
typedef unsigned char uint_fast8_t;
typedef unsigned long uint_fast16_t;
typedef unsigned long uint_fast32_t;
typedef unsigned long uint_fast64_t;
typedef long intptr_t;
typedef unsigned long uintptr_t;
typedef __intmax_t intmax_t;
typedef __uintmax_t uintmax_t;
typedef _Bool bool;
struct indirect_placeholder_18_ret_type;
struct indirect_placeholder_19_ret_type;
struct indirect_placeholder_20_ret_type;
struct indirect_placeholder_21_ret_type;
struct indirect_placeholder_22_ret_type;
struct indirect_placeholder_18_ret_type {
    uint64_t field_0;
    uint64_t field_1;
    uint64_t field_2;
};
struct indirect_placeholder_19_ret_type {
    uint64_t field_0;
    uint64_t field_1;
    uint64_t field_2;
};
struct indirect_placeholder_20_ret_type {
    uint64_t field_0;
    uint64_t field_1;
    uint64_t field_2;
    uint64_t field_3;
    uint64_t field_4;
};
struct indirect_placeholder_21_ret_type {
    uint64_t field_0;
    uint64_t field_1;
    uint64_t field_2;
    uint64_t field_3;
    uint64_t field_4;
};
struct indirect_placeholder_22_ret_type {
    uint64_t field_0;
    uint64_t field_1;
    uint64_t field_2;
    uint64_t field_3;
    uint64_t field_4;
};
extern void abort(void);
extern uint64_t revng_init_local_sp(uint64_t param_0);
extern uint64_t init_rbp(void);
extern uint64_t indirect_placeholder(uint64_t param_0, uint64_t param_1);
extern uint64_t init_cc_src2(void);
extern uint64_t helper_cc_compute_c_wrapper(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint32_t param_3);
extern void indirect_placeholder_1(void);
extern uint64_t init_rbx(void);
extern uint64_t init_r10(void);
extern void indirect_placeholder_6(uint64_t param_0, uint64_t param_1);
extern uint64_t indirect_placeholder_5(void);
extern uint64_t init_r9(void);
extern uint64_t init_r8(void);
extern uint64_t indirect_placeholder_2(uint64_t param_0);
extern uint64_t init_rcx(void);
extern void indirect_placeholder_4(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint64_t param_3, uint64_t param_4, uint64_t param_5, uint64_t param_6);
extern void indirect_placeholder_15(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint64_t param_3, uint64_t param_4);
extern struct indirect_placeholder_18_ret_type indirect_placeholder_18(uint64_t param_0, uint64_t param_1);
extern struct indirect_placeholder_19_ret_type indirect_placeholder_19(uint64_t param_0);
extern struct indirect_placeholder_20_ret_type indirect_placeholder_20(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint64_t param_3, uint64_t param_4, uint64_t param_5, uint64_t param_6);
extern struct indirect_placeholder_21_ret_type indirect_placeholder_21(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint64_t param_3, uint64_t param_4, uint64_t param_5, uint64_t param_6);
extern struct indirect_placeholder_22_ret_type indirect_placeholder_22(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint64_t param_3, uint64_t param_4, uint64_t param_5, uint64_t param_6);
void bb_set_fields(uint64_t rsi, uint64_t rdi) {
    uint64_t local_sp_1;
    uint64_t var_0;
    uint64_t var_1;
    uint64_t var_2;
    uint64_t var_3;
    uint64_t var_4;
    uint64_t var_5;
    uint64_t var_6;
    uint64_t var_7;
    uint64_t var_8;
    uint64_t var_9;
    uint64_t *var_10;
    uint32_t *var_11;
    uint64_t *var_12;
    uint64_t *var_13;
    unsigned char *var_14;
    unsigned char *var_15;
    unsigned char *var_16;
    unsigned char *var_17;
    uint64_t var_82;
    uint64_t var_63;
    uint64_t rcx_1;
    uint64_t local_sp_0;
    uint64_t rcx_0;
    uint64_t var_64;
    uint64_t var_65;
    uint64_t r9_1;
    uint64_t var_84;
    uint64_t r8_1;
    uint64_t rbx_1;
    uint64_t var_85;
    uint64_t var_86;
    uint64_t var_87;
    uint64_t var_88;
    uint64_t var_89;
    uint64_t var_66;
    uint64_t var_67;
    uint64_t var_68;
    uint64_t local_sp_2;
    uint64_t var_69;
    uint64_t rcx_2;
    uint64_t var_70;
    uint64_t var_71;
    uint64_t var_72;
    uint64_t var_73;
    uint64_t var_74;
    uint64_t *var_75;
    uint64_t var_76;
    unsigned char var_27;
    uint64_t var_77;
    uint64_t var_78;
    uint64_t var_79;
    uint64_t var_80;
    uint64_t var_81;
    uint64_t var_83;
    uint64_t local_sp_3;
    uint64_t rcx_3;
    uint64_t r10_0;
    uint64_t r9_0;
    uint64_t r8_0;
    uint64_t rbx_0;
    uint64_t local_sp_4;
    uint64_t rcx_4;
    uint64_t r10_1;
    uint64_t spec_select;
    uint64_t var_60;
    uint64_t *var_61;
    uint64_t *var_62;
    uint64_t local_sp_6;
    uint64_t local_sp_7;
    uint64_t r10_3;
    uint64_t var_28;
    uint64_t var_45;
    uint64_t var_46;
    uint64_t var_29;
    uint64_t var_30;
    uint64_t var_31;
    uint64_t var_32;
    struct indirect_placeholder_18_ret_type var_33;
    uint64_t var_34;
    uint64_t var_35;
    uint64_t var_36;
    uint64_t storemerge11;
    uint64_t var_37;
    struct indirect_placeholder_19_ret_type var_38;
    uint64_t var_39;
    uint64_t var_40;
    uint64_t var_41;
    uint64_t storemerge12;
    uint64_t local_sp_5_ph;
    uint64_t var_18;
    uint64_t var_19;
    uint64_t rcx_6;
    uint64_t local_sp_5;
    uint64_t rcx_5;
    uint64_t r9_3;
    uint64_t r10_2;
    uint64_t r8_3;
    uint64_t r9_2;
    uint64_t rbx_3;
    uint64_t r8_2;
    uint64_t rbx_2;
    unsigned char **var_20;
    unsigned char var_21;
    uint64_t spec_select292;
    bool var_22;
    uint64_t spec_select293;
    uint64_t storemerge14;
    uint64_t var_23;
    uint64_t var_24;
    uint64_t var_25;
    uint64_t var_26;
    uint64_t var_42;
    uint64_t var_43;
    struct indirect_placeholder_20_ret_type var_44;
    uint64_t var_47;
    uint64_t var_48;
    uint64_t var_49;
    uint64_t var_50;
    struct indirect_placeholder_21_ret_type var_51;
    uint64_t var_52;
    uint64_t spec_select294;
    uint64_t var_53;
    struct indirect_placeholder_22_ret_type var_54;
    uint64_t var_55;
    uint64_t var_56;
    uint64_t var_57;
    uint64_t var_58;
    uint64_t var_59;
    unsigned int loop_state_var;
    bool switch_state_var;
    var_0 = revng_init_local_sp(0UL);
    var_1 = init_rbp();
    var_2 = init_cc_src2();
    var_3 = init_rcx();
    var_4 = init_r10();
    var_5 = init_r9();
    var_6 = init_r8();
    var_7 = init_rbx();
    var_8 = var_0 + (-8L);
    *(uint64_t *)var_8 = var_1;
    var_9 = var_0 + (-80L);
    var_10 = (uint64_t *)var_9;
    *var_10 = rdi;
    var_11 = (uint32_t *)(var_0 + (-84L));
    *var_11 = (uint32_t)rsi;
    var_12 = (uint64_t *)(var_0 + (-16L));
    *var_12 = 1UL;
    var_13 = (uint64_t *)(var_0 + (-64L));
    *var_13 = 0UL;
    var_14 = (unsigned char *)(var_0 + (-17L));
    *var_14 = (unsigned char)'\x00';
    var_15 = (unsigned char *)(var_0 + (-18L));
    *var_15 = (unsigned char)'\x00';
    var_16 = (unsigned char *)(var_0 + (-19L));
    *var_16 = (unsigned char)'\x00';
    var_17 = (unsigned char *)(var_0 + (-20L));
    *var_17 = (unsigned char)'\x00';
    var_63 = 0UL;
    rcx_0 = 4206399UL;
    rcx_5 = var_3;
    r10_2 = var_4;
    r9_2 = var_5;
    r8_2 = var_6;
    rbx_2 = var_7;
    storemerge14 = 1UL;
    if ((*var_11 & 1U) == 0U) {
        local_sp_5_ph = var_0 + (-88L);
    } else {
        var_18 = *var_10;
        var_19 = var_0 + (-96L);
        *(uint64_t *)var_19 = 4206866UL;
        indirect_placeholder_1();
        local_sp_5_ph = var_19;
        if ((uint64_t)(uint32_t)var_18 == 0UL) {
            *var_13 = 1UL;
            *var_14 = (unsigned char)'\x01';
            *var_16 = (unsigned char)'\x01';
            *var_10 = (*var_10 + 1UL);
        }
    }
    local_sp_5 = local_sp_5_ph;
    while (1U)
        {
            var_20 = (unsigned char **)var_9;
            var_21 = **var_20;
            local_sp_6 = local_sp_5;
            local_sp_7 = local_sp_5;
            r10_3 = r10_2;
            rcx_6 = rcx_5;
            r9_3 = r9_2;
            r8_3 = r8_2;
            rbx_3 = rbx_2;
            switch_state_var = 0;
            switch (var_21) {
              case '-':
                {
                    *var_17 = (unsigned char)'\x00';
                    if (*var_16 == '\x00') {
                        spec_select292 = ((*var_11 & 4U) == 0U) ? 4277080UL : 4277048UL;
                        *(uint64_t *)(local_sp_5 + (-8L)) = 4206961UL;
                        indirect_placeholder_4(0UL, spec_select292, rcx_5, 0UL, 0UL, r9_2, r8_2);
                        *(uint64_t *)(local_sp_5 + (-16L)) = 4206971UL;
                        indirect_placeholder_6(var_8, 1UL);
                        abort();
                    }
                    *var_16 = (unsigned char)'\x01';
                    *var_10 = (*var_10 + 1UL);
                    var_22 = (*var_14 == '\x00');
                    if (!var_22) {
                        if (*var_13 == 0UL) {
                            spec_select293 = ((*var_11 & 4U) == 0U) ? 4277149UL : 4277104UL;
                            *(uint64_t *)(local_sp_5 + (-8L)) = 4207040UL;
                            indirect_placeholder_4(0UL, spec_select293, rcx_5, 0UL, 0UL, r9_2, r8_2);
                            *(uint64_t *)(local_sp_5 + (-16L)) = 4207050UL;
                            indirect_placeholder_6(var_8, 1UL);
                            abort();
                        }
                    }
                    if (!var_22) {
                        storemerge14 = *var_13;
                    }
                    *var_12 = storemerge14;
                    *var_13 = 0UL;
                }
                break;
              case ',':
                {
                    *var_17 = (unsigned char)'\x00';
                    if (*var_16 == '\x00') {
                        var_52 = *var_13;
                        if (var_52 != 0UL) {
                            spec_select294 = ((*var_11 & 4U) == 0U) ? 4277149UL : 4277104UL;
                            *(uint64_t *)(local_sp_7 + (-8L)) = 4207396UL;
                            indirect_placeholder_4(0UL, spec_select294, rcx_5, 0UL, 0UL, r9_2, r8_2);
                            *(uint64_t *)(local_sp_7 + (-16L)) = 4207406UL;
                            indirect_placeholder_6(var_8, 1UL);
                            abort();
                        }
                        var_53 = local_sp_7 + (-8L);
                        *(uint64_t *)var_53 = 4207425UL;
                        var_54 = indirect_placeholder_22(rcx_5, var_52, var_52, r10_2, r9_2, r8_2, rbx_2);
                        var_55 = var_54.field_0;
                        var_56 = var_54.field_1;
                        var_57 = var_54.field_2;
                        var_58 = var_54.field_3;
                        var_59 = var_54.field_4;
                        *var_13 = 0UL;
                        r9_1 = var_57;
                        r8_1 = var_58;
                        rbx_1 = var_59;
                        local_sp_4 = var_53;
                        rcx_4 = var_55;
                        r10_1 = var_56;
                    } else {
                        *var_16 = (unsigned char)'\x00';
                        if (*var_14 != '\x01' & *var_15 != '\x01') {
                            if ((*var_11 & 1U) != 0U) {
                                *(uint64_t *)(local_sp_7 + (-8L)) = 4207226UL;
                                indirect_placeholder_4(0UL, 4277176UL, rcx_5, 0UL, 0UL, r9_2, r8_2);
                                *(uint64_t *)(local_sp_7 + (-16L)) = 4207236UL;
                                indirect_placeholder_6(var_8, 1UL);
                                abort();
                            }
                            *var_12 = 1UL;
                        }
                        if (*var_15 == '\x01') {
                            var_42 = *var_12;
                            var_43 = local_sp_7 + (-8L);
                            *(uint64_t *)var_43 = 4207266UL;
                            var_44 = indirect_placeholder_20(rcx_5, 18446744073709551615UL, var_42, r10_2, r9_2, r8_2, rbx_2);
                            local_sp_3 = var_43;
                            rcx_3 = var_44.field_0;
                            r10_0 = var_44.field_1;
                            r9_0 = var_44.field_2;
                            r8_0 = var_44.field_3;
                            rbx_0 = var_44.field_4;
                        } else {
                            var_45 = *var_13;
                            var_46 = *var_12;
                            var_47 = helper_cc_compute_c_wrapper(var_45 - var_46, var_46, var_2, 17U);
                            if (var_47 != 0UL) {
                                *(uint64_t *)(local_sp_7 + (-8L)) = 4207303UL;
                                indirect_placeholder_4(0UL, 4277210UL, rcx_5, 0UL, 0UL, r9_2, r8_2);
                                *(uint64_t *)(local_sp_7 + (-16L)) = 4207313UL;
                                indirect_placeholder_6(var_8, 1UL);
                                abort();
                            }
                            var_48 = *var_13;
                            var_49 = *var_12;
                            var_50 = local_sp_7 + (-8L);
                            *(uint64_t *)var_50 = 4207332UL;
                            var_51 = indirect_placeholder_21(rcx_5, var_48, var_49, r10_2, r9_2, r8_2, rbx_2);
                            local_sp_3 = var_50;
                            rcx_3 = var_51.field_0;
                            r10_0 = var_51.field_1;
                            r9_0 = var_51.field_2;
                            r8_0 = var_51.field_3;
                            rbx_0 = var_51.field_4;
                        }
                        *var_13 = 0UL;
                        r9_1 = r9_0;
                        r8_1 = r8_0;
                        rbx_1 = rbx_0;
                        local_sp_4 = local_sp_3;
                        rcx_4 = rcx_3;
                        r10_1 = r10_0;
                    }
                    local_sp_6 = local_sp_4;
                    r10_3 = r10_1;
                    rcx_6 = rcx_4;
                    r9_3 = r9_1;
                    r8_3 = r8_1;
                    rbx_3 = rbx_1;
                    if (**var_20 != '\x00') {
                        if (*(uint64_t *)6387592UL != 0UL) {
                            var_60 = local_sp_4 + (-8L);
                            *(uint64_t *)var_60 = 4207990UL;
                            indirect_placeholder_1();
                            var_61 = (uint64_t *)(var_0 + (-32L));
                            *var_61 = 0UL;
                            var_62 = (uint64_t *)(var_0 + (-40L));
                            local_sp_0 = var_60;
                            loop_state_var = 0U;
                            switch_state_var = 1;
                            break;
                        }
                        spec_select = ((*var_11 & 4U) == 0U) ? 4277425UL : 4277384UL;
                        *(uint64_t *)(local_sp_4 + (-8L)) = 4207948UL;
                        indirect_placeholder_4(0UL, spec_select, rcx_4, 0UL, 0UL, r9_1, r8_1);
                        *(uint64_t *)(local_sp_4 + (-16L)) = 4207958UL;
                        indirect_placeholder_6(var_8, 1UL);
                        abort();
                    }
                    *var_10 = (*var_10 + 1UL);
                    *var_14 = (unsigned char)'\x00';
                    *var_15 = (unsigned char)'\x00';
                }
                break;
              default:
                {
                    var_23 = (uint64_t)(uint32_t)(uint64_t)var_21;
                    *(uint64_t *)(local_sp_5 + (-8L)) = 4207112UL;
                    var_24 = indirect_placeholder_2(var_23);
                    var_25 = (uint64_t)(unsigned char)var_24;
                    var_26 = local_sp_5 + (-16L);
                    *(uint64_t *)var_26 = 4207122UL;
                    indirect_placeholder_1();
                    local_sp_6 = var_26;
                    local_sp_7 = var_26;
                    if (var_25 != 0UL) {
                        var_27 = **var_20;
                        if (var_27 == '\x00') {
                            if ((uint64_t)(((uint32_t)(uint64_t)var_27 + (-48)) & (-2)) > 9UL) {
                                var_37 = *var_10;
                                *(uint64_t *)(local_sp_5 + (-24L)) = 4207837UL;
                                var_38 = indirect_placeholder_19(var_37);
                                var_39 = var_38.field_0;
                                var_40 = var_38.field_1;
                                var_41 = var_38.field_2;
                                storemerge12 = ((*var_11 & 4U) == 0U) ? 4277355UL : 4277320UL;
                                *(uint64_t *)(local_sp_5 + (-32L)) = 4207888UL;
                                indirect_placeholder_4(0UL, storemerge12, var_39, 0UL, 0UL, var_40, var_41);
                                *(uint64_t *)(local_sp_5 + (-40L)) = 4207898UL;
                                indirect_placeholder_6(var_8, 1UL);
                                abort();
                            }
                            if (*var_17 == '\x01') {
                                *(uint64_t *)6387152UL = *var_10;
                            } else {
                                if (*(uint64_t *)6387152UL == 0UL) {
                                    *(uint64_t *)6387152UL = *var_10;
                                }
                            }
                            *var_17 = (unsigned char)'\x01';
                            if (*var_16 == '\x00') {
                                *var_14 = (unsigned char)'\x01';
                            } else {
                                *var_15 = (unsigned char)'\x01';
                            }
                            var_28 = *var_13;
                            if (var_28 <= 1844674407370955161UL) {
                                loop_state_var = 1U;
                                switch_state_var = 1;
                                break;
                            }
                            var_29 = helper_cc_compute_c_wrapper(((var_28 * 10UL) + (uint64_t)((long)(((uint64_t)**var_20 << 32UL) + (-206158430208L)) >> (long)32UL)) - var_28, var_28, var_2, 17U);
                            if (var_29 != 0UL) {
                                loop_state_var = 1U;
                                switch_state_var = 1;
                                break;
                            }
                            var_30 = (uint64_t)((long)(((uint64_t)**var_20 << 32UL) + (-206158430208L)) >> (long)32UL) + (*var_13 * 10UL);
                            *var_13 = var_30;
                            if (var_30 != 18446744073709551615UL) {
                                loop_state_var = 1U;
                                switch_state_var = 1;
                                break;
                            }
                            *var_10 = (*var_10 + 1UL);
                            local_sp_5 = local_sp_6;
                            rcx_5 = rcx_6;
                            r10_2 = r10_3;
                            r9_2 = r9_3;
                            r8_2 = r8_3;
                            rbx_2 = rbx_3;
                            continue;
                        }
                    }
                    *var_17 = (unsigned char)'\x00';
                    if (*var_16 == '\x00') {
                        *var_16 = (unsigned char)'\x00';
                        if (*var_14 != '\x01' & *var_15 != '\x01') {
                            if ((*var_11 & 1U) == 0U) {
                                *(uint64_t *)(local_sp_7 + (-8L)) = 4207226UL;
                                indirect_placeholder_4(0UL, 4277176UL, rcx_5, 0UL, 0UL, r9_2, r8_2);
                                *(uint64_t *)(local_sp_7 + (-16L)) = 4207236UL;
                                indirect_placeholder_6(var_8, 1UL);
                                abort();
                            }
                            *var_12 = 1UL;
                        }
                        if (*var_15 == '\x01') {
                            var_42 = *var_12;
                            var_43 = local_sp_7 + (-8L);
                            *(uint64_t *)var_43 = 4207266UL;
                            var_44 = indirect_placeholder_20(rcx_5, 18446744073709551615UL, var_42, r10_2, r9_2, r8_2, rbx_2);
                            local_sp_3 = var_43;
                            rcx_3 = var_44.field_0;
                            r10_0 = var_44.field_1;
                            r9_0 = var_44.field_2;
                            r8_0 = var_44.field_3;
                            rbx_0 = var_44.field_4;
                        } else {
                            var_45 = *var_13;
                            var_46 = *var_12;
                            var_47 = helper_cc_compute_c_wrapper(var_45 - var_46, var_46, var_2, 17U);
                            if (var_47 == 0UL) {
                                *(uint64_t *)(local_sp_7 + (-8L)) = 4207303UL;
                                indirect_placeholder_4(0UL, 4277210UL, rcx_5, 0UL, 0UL, r9_2, r8_2);
                                *(uint64_t *)(local_sp_7 + (-16L)) = 4207313UL;
                                indirect_placeholder_6(var_8, 1UL);
                                abort();
                            }
                            var_48 = *var_13;
                            var_49 = *var_12;
                            var_50 = local_sp_7 + (-8L);
                            *(uint64_t *)var_50 = 4207332UL;
                            var_51 = indirect_placeholder_21(rcx_5, var_48, var_49, r10_2, r9_2, r8_2, rbx_2);
                            local_sp_3 = var_50;
                            rcx_3 = var_51.field_0;
                            r10_0 = var_51.field_1;
                            r9_0 = var_51.field_2;
                            r8_0 = var_51.field_3;
                            rbx_0 = var_51.field_4;
                        }
                        *var_13 = 0UL;
                        r9_1 = r9_0;
                        r8_1 = r8_0;
                        rbx_1 = rbx_0;
                        local_sp_4 = local_sp_3;
                        rcx_4 = rcx_3;
                        r10_1 = r10_0;
                    } else {
                        var_52 = *var_13;
                        if (var_52 == 0UL) {
                            spec_select294 = ((*var_11 & 4U) == 0U) ? 4277149UL : 4277104UL;
                            *(uint64_t *)(local_sp_7 + (-8L)) = 4207396UL;
                            indirect_placeholder_4(0UL, spec_select294, rcx_5, 0UL, 0UL, r9_2, r8_2);
                            *(uint64_t *)(local_sp_7 + (-16L)) = 4207406UL;
                            indirect_placeholder_6(var_8, 1UL);
                            abort();
                        }
                        var_53 = local_sp_7 + (-8L);
                        *(uint64_t *)var_53 = 4207425UL;
                        var_54 = indirect_placeholder_22(rcx_5, var_52, var_52, r10_2, r9_2, r8_2, rbx_2);
                        var_55 = var_54.field_0;
                        var_56 = var_54.field_1;
                        var_57 = var_54.field_2;
                        var_58 = var_54.field_3;
                        var_59 = var_54.field_4;
                        *var_13 = 0UL;
                        r9_1 = var_57;
                        r8_1 = var_58;
                        rbx_1 = var_59;
                        local_sp_4 = var_53;
                        rcx_4 = var_55;
                        r10_1 = var_56;
                    }
                    local_sp_6 = local_sp_4;
                    r10_3 = r10_1;
                    rcx_6 = rcx_4;
                    r9_3 = r9_1;
                    r8_3 = r8_1;
                    rbx_3 = rbx_1;
                    if (**var_20 != '\x00') {
                        if (*(uint64_t *)6387592UL != 0UL) {
                            var_60 = local_sp_4 + (-8L);
                            *(uint64_t *)var_60 = 4207990UL;
                            indirect_placeholder_1();
                            var_61 = (uint64_t *)(var_0 + (-32L));
                            *var_61 = 0UL;
                            var_62 = (uint64_t *)(var_0 + (-40L));
                            local_sp_0 = var_60;
                            loop_state_var = 0U;
                            switch_state_var = 1;
                            break;
                        }
                        spec_select = ((*var_11 & 4U) == 0U) ? 4277425UL : 4277384UL;
                        *(uint64_t *)(local_sp_4 + (-8L)) = 4207948UL;
                        indirect_placeholder_4(0UL, spec_select, rcx_4, 0UL, 0UL, r9_1, r8_1);
                        *(uint64_t *)(local_sp_4 + (-16L)) = 4207958UL;
                        indirect_placeholder_6(var_8, 1UL);
                        abort();
                    }
                    *var_10 = (*var_10 + 1UL);
                    *var_14 = (unsigned char)'\x00';
                    *var_15 = (unsigned char)'\x00';
                }
                break;
            }
            if (switch_state_var)
                break;
        }
    switch (loop_state_var) {
      case 1U:
        {
            var_31 = *(uint64_t *)6387152UL;
            *(uint64_t *)(local_sp_5 + (-24L)) = 4207703UL;
            indirect_placeholder_1();
            *(uint64_t *)(var_0 + (-48L)) = var_31;
            *(uint64_t *)(local_sp_5 + (-32L)) = 4207729UL;
            var_32 = indirect_placeholder_5();
            *(uint64_t *)(var_0 + (-56L)) = var_32;
            *(uint64_t *)(local_sp_5 + (-40L)) = 4207745UL;
            var_33 = indirect_placeholder_18(var_31, var_32);
            var_34 = var_33.field_0;
            var_35 = var_33.field_1;
            var_36 = var_33.field_2;
            storemerge11 = ((*var_11 & 4U) == 0U) ? 4277286UL : 4277248UL;
            *(uint64_t *)(local_sp_5 + (-48L)) = 4207796UL;
            indirect_placeholder_4(0UL, storemerge11, var_34, 0UL, 0UL, var_35, var_36);
            *(uint64_t *)(local_sp_5 + (-56L)) = 4207808UL;
            indirect_placeholder_1();
            *(uint64_t *)(local_sp_5 + (-64L)) = 4207818UL;
            indirect_placeholder_6(var_8, 1UL);
            abort();
        }
        break;
      case 0U:
        {
            var_64 = *(uint64_t *)6387592UL;
            var_65 = helper_cc_compute_c_wrapper(var_63 - var_64, var_64, var_2, 17U);
            local_sp_1 = local_sp_0;
            local_sp_2 = local_sp_0;
            rcx_1 = rcx_0;
            while (var_65 != 0UL)
                {
                    var_66 = *var_61 + 1UL;
                    *var_62 = var_66;
                    var_67 = var_66;
                    var_68 = *(uint64_t *)6387592UL;
                    while (1U)
                        {
                            var_69 = helper_cc_compute_c_wrapper(var_67 - var_68, var_68, var_2, 17U);
                            local_sp_0 = local_sp_2;
                            rcx_2 = rcx_1;
                            if (var_69 != 0UL) {
                                loop_state_var = 1U;
                                break;
                            }
                            var_70 = *(uint64_t *)6387584UL;
                            var_71 = var_70 + (*var_62 << 4UL);
                            var_72 = *(uint64_t *)var_71;
                            var_73 = *var_61;
                            var_74 = var_73 << 4UL;
                            var_75 = (uint64_t *)((var_70 + var_74) + 8UL);
                            var_76 = *var_75;
                            var_82 = var_73;
                            rcx_2 = var_74;
                            if (var_72 <= var_76) {
                                loop_state_var = 0U;
                                break;
                            }
                            var_77 = *(uint64_t *)(var_71 + 8UL);
                            var_78 = helper_cc_compute_c_wrapper(var_76 - var_77, var_77, var_2, 17U);
                            *var_75 = ((var_78 == 0UL) ? var_76 : var_77);
                            var_79 = ((*var_62 << 4UL) + 16UL) + *(uint64_t *)6387584UL;
                            var_80 = local_sp_2 + (-8L);
                            *(uint64_t *)var_80 = 4208216UL;
                            indirect_placeholder_1();
                            var_81 = *(uint64_t *)6387592UL + (-1L);
                            *(uint64_t *)6387592UL = var_81;
                            var_67 = *var_62;
                            var_68 = var_81;
                            local_sp_2 = var_80;
                            rcx_1 = var_79;
                            continue;
                        }
                    switch (loop_state_var) {
                      case 0U:
                        {
                        }
                        break;
                      case 1U:
                        {
                            var_82 = *var_61;
                        }
                        break;
                    }
                    var_83 = var_82 + 1UL;
                    *var_61 = var_83;
                    var_63 = var_83;
                    rcx_0 = rcx_2;
                    var_64 = *(uint64_t *)6387592UL;
                    var_65 = helper_cc_compute_c_wrapper(var_63 - var_64, var_64, var_2, 17U);
                    local_sp_1 = local_sp_0;
                    local_sp_2 = local_sp_0;
                    rcx_1 = rcx_0;
                }
            if ((*var_11 & 2U) != 0U) {
                var_84 = local_sp_0 + (-8L);
                *(uint64_t *)var_84 = 4208302UL;
                indirect_placeholder_15(rcx_0, r10_1, r9_1, r8_1, rbx_1);
                local_sp_1 = var_84;
            }
            var_85 = *(uint64_t *)6387592UL + 1UL;
            *(uint64_t *)6387592UL = var_85;
            var_86 = var_85 << 4UL;
            var_87 = *(uint64_t *)6387584UL;
            *(uint64_t *)(local_sp_1 + (-8L)) = 4208352UL;
            var_88 = indirect_placeholder(var_86, var_87);
            *(uint64_t *)6387584UL = var_88;
            var_89 = ((*(uint64_t *)6387592UL << 4UL) + (-16L)) + var_88;
            *(uint64_t *)(var_89 + 8UL) = 18446744073709551615UL;
            *(uint64_t *)var_89 = 18446744073709551615UL;
            return;
        }
        break;
    }
}

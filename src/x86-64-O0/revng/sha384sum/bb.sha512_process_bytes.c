typedef unsigned char __u_char;
typedef unsigned short __u_short;
typedef unsigned int __u_int;
typedef unsigned long __u_long;
typedef signed char __int8_t;
typedef unsigned char __uint8_t;
typedef short __int16_t;
typedef unsigned short __uint16_t;
typedef int __int32_t;
typedef unsigned int __uint32_t;
typedef long __int64_t;
typedef unsigned long __uint64_t;
typedef __int8_t __int_least8_t;
typedef __uint8_t __uint_least8_t;
typedef __int16_t __int_least16_t;
typedef __uint16_t __uint_least16_t;
typedef __int32_t __int_least32_t;
typedef __uint32_t __uint_least32_t;
typedef __int64_t __int_least64_t;
typedef __uint64_t __uint_least64_t;
typedef long __quad_t;
typedef unsigned long __u_quad_t;
typedef long __intmax_t;
typedef unsigned long __uintmax_t;
typedef unsigned long __dev_t;
typedef unsigned int __uid_t;
typedef unsigned int __gid_t;
typedef unsigned long __ino_t;
typedef unsigned long __ino64_t;
typedef unsigned int __mode_t;
typedef unsigned long __nlink_t;
typedef long __off_t;
typedef long __off64_t;
typedef int __pid_t;
typedef struct {
    int __val[2];
} __fsid_t;
typedef long __clock_t;
typedef unsigned long __rlim_t;
typedef unsigned long __rlim64_t;
typedef unsigned int __id_t;
typedef long __time_t;
typedef unsigned int __useconds_t;
typedef long __suseconds_t;
typedef int __daddr_t;
typedef int __key_t;
typedef int __clockid_t;
typedef void *__timer_t;
typedef long __blksize_t;
typedef long __blkcnt_t;
typedef long __blkcnt64_t;
typedef unsigned long __fsblkcnt_t;
typedef unsigned long __fsblkcnt64_t;
typedef unsigned long __fsfilcnt_t;
typedef unsigned long __fsfilcnt64_t;
typedef long __fsword_t;
typedef long __ssize_t;
typedef long __syscall_slong_t;
typedef unsigned long __syscall_ulong_t;
typedef __off64_t __loff_t;
typedef char *__caddr_t;
typedef long __intptr_t;
typedef unsigned int __socklen_t;
typedef int __sig_atomic_t;
typedef __int8_t int8_t;
typedef __int16_t int16_t;
typedef __int32_t int32_t;
typedef __int64_t int64_t;
typedef __uint8_t uint8_t;
typedef __uint16_t uint16_t;
typedef __uint32_t uint32_t;
typedef __uint64_t uint64_t;
typedef __int_least8_t int_least8_t;
typedef __int_least16_t int_least16_t;
typedef __int_least32_t int_least32_t;
typedef __int_least64_t int_least64_t;
typedef __uint_least8_t uint_least8_t;
typedef __uint_least16_t uint_least16_t;
typedef __uint_least32_t uint_least32_t;
typedef __uint_least64_t uint_least64_t;
typedef signed char int_fast8_t;
typedef long int_fast16_t;
typedef long int_fast32_t;
typedef long int_fast64_t;
typedef unsigned char uint_fast8_t;
typedef unsigned long uint_fast16_t;
typedef unsigned long uint_fast32_t;
typedef unsigned long uint_fast64_t;
typedef long intptr_t;
typedef unsigned long uintptr_t;
typedef __intmax_t intmax_t;
typedef __uintmax_t uintmax_t;
extern uint64_t revng_init_local_sp(uint64_t param_0);
extern uint64_t init_rbp(void);
extern void indirect_placeholder(void);
extern void indirect_placeholder_24(uint64_t param_0, uint64_t param_1, uint64_t param_2);
void bb_sha512_process_bytes(uint64_t rdx, uint64_t rdi, uint64_t rsi) {
    uint64_t var_0;
    uint64_t var_1;
    uint64_t var_2;
    uint64_t *var_3;
    uint64_t *var_4;
    uint64_t *var_5;
    uint64_t var_6;
    uint64_t var_20;
    uint64_t var_35;
    uint64_t var_36;
    uint64_t var_37;
    uint64_t var_15;
    uint64_t var_16;
    uint64_t *var_17;
    uint64_t var_18;
    uint64_t local_sp_0;
    uint64_t var_19;
    uint64_t var_7;
    uint64_t var_8;
    uint64_t var_9;
    uint64_t *var_10;
    uint64_t var_11;
    uint64_t *var_12;
    uint64_t var_13;
    uint64_t var_14;
    uint64_t local_sp_1;
    uint64_t var_31;
    uint64_t var_21;
    uint64_t var_22;
    uint64_t local_sp_2;
    uint64_t var_23;
    uint64_t var_24;
    uint64_t var_25;
    uint64_t var_26;
    uint64_t var_27;
    uint64_t var_28;
    uint64_t var_29;
    uint64_t var_30;
    uint64_t local_sp_3;
    uint64_t var_32;
    uint64_t *var_33;
    uint64_t var_34;
    var_0 = revng_init_local_sp(0UL);
    var_1 = init_rbp();
    *(uint64_t *)(var_0 + (-8L)) = var_1;
    var_2 = var_0 + (-72L);
    var_3 = (uint64_t *)(var_0 + (-48L));
    *var_3 = rdi;
    var_4 = (uint64_t *)(var_0 + (-56L));
    *var_4 = rsi;
    var_5 = (uint64_t *)(var_0 + (-64L));
    *var_5 = rdx;
    var_6 = *(uint64_t *)(rdx + 80UL);
    local_sp_1 = var_2;
    if (var_6 == 0UL) {
        var_20 = *var_4;
    } else {
        *(uint64_t *)(var_0 + (-24L)) = var_6;
        var_7 = 256UL - var_6;
        var_8 = *var_4;
        var_9 = (var_7 > var_8) ? var_8 : var_7;
        var_10 = (uint64_t *)(var_0 + (-32L));
        *var_10 = var_9;
        var_11 = var_0 + (-80L);
        *(uint64_t *)var_11 = 4209929UL;
        indirect_placeholder();
        var_12 = (uint64_t *)(*var_5 + 80UL);
        *var_12 = (*var_12 + *var_10);
        var_13 = *var_5;
        var_14 = *(uint64_t *)(var_13 + 80UL);
        local_sp_0 = var_11;
        if (var_14 > 128UL) {
            var_15 = var_14 & (-128L);
            var_16 = var_13 + 88UL;
            *(uint64_t *)(var_0 + (-88L)) = 4210006UL;
            indirect_placeholder_24(var_13, var_16, var_15);
            var_17 = (uint64_t *)(*var_5 + 80UL);
            *var_17 = (*var_17 & 127UL);
            var_18 = var_0 + (-96L);
            *(uint64_t *)var_18 = 4210081UL;
            indirect_placeholder();
            local_sp_0 = var_18;
        }
        *var_3 = (*var_3 + *var_10);
        var_19 = *var_4 - *var_10;
        *var_4 = var_19;
        var_20 = var_19;
        local_sp_1 = local_sp_0;
    }
    var_22 = var_20;
    local_sp_2 = local_sp_1;
    var_31 = var_20;
    local_sp_3 = local_sp_1;
    if (var_20 <= 127UL) {
        var_21 = *var_3;
        if ((var_21 & 7UL) != 0UL) {
            var_31 = var_22;
            local_sp_3 = local_sp_2;
            while (var_22 <= 128UL)
                {
                    var_23 = *var_3;
                    *(uint64_t *)(local_sp_2 + (-8L)) = 4210150UL;
                    indirect_placeholder();
                    var_24 = *var_5;
                    var_25 = local_sp_2 + (-16L);
                    *(uint64_t *)var_25 = 4210173UL;
                    indirect_placeholder_24(var_24, var_23, 128UL);
                    *var_3 = (*var_3 + 128UL);
                    var_26 = *var_4 + (-128L);
                    *var_4 = var_26;
                    var_22 = var_26;
                    local_sp_2 = var_25;
                    var_31 = var_22;
                    local_sp_3 = local_sp_2;
                }
        }
        var_27 = var_20 & (-128L);
        var_28 = *var_5;
        var_29 = local_sp_1 + (-8L);
        *(uint64_t *)var_29 = 4210225UL;
        indirect_placeholder_24(var_28, var_21, var_27);
        *var_3 = (*var_3 + (*var_4 & (-128L)));
        var_30 = *var_4 & 127UL;
        *var_4 = var_30;
        var_31 = var_30;
        local_sp_3 = var_29;
    }
    if (var_31 == 0UL) {
        return;
    }
    var_32 = *(uint64_t *)(*var_5 + 80UL);
    var_33 = (uint64_t *)(var_0 + (-16L));
    *var_33 = var_32;
    *(uint64_t *)(local_sp_3 + (-8L)) = 4210300UL;
    indirect_placeholder();
    var_34 = *var_33 + *var_4;
    *var_33 = var_34;
    var_37 = var_34;
    if (var_34 > 127UL) {
        var_35 = *var_5;
        var_36 = var_35 + 88UL;
        *(uint64_t *)(local_sp_3 + (-16L)) = 4210343UL;
        indirect_placeholder_24(var_35, var_36, 128UL);
        *var_33 = (*var_33 + (-128L));
        *(uint64_t *)(local_sp_3 + (-24L)) = 4210382UL;
        indirect_placeholder();
        var_37 = *var_33;
    }
    *(uint64_t *)(*var_5 + 80UL) = var_37;
    return;
}

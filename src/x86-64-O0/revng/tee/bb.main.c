typedef unsigned char __u_char;
typedef unsigned short __u_short;
typedef unsigned int __u_int;
typedef unsigned long __u_long;
typedef signed char __int8_t;
typedef unsigned char __uint8_t;
typedef short __int16_t;
typedef unsigned short __uint16_t;
typedef int __int32_t;
typedef unsigned int __uint32_t;
typedef long __int64_t;
typedef unsigned long __uint64_t;
typedef __int8_t __int_least8_t;
typedef __uint8_t __uint_least8_t;
typedef __int16_t __int_least16_t;
typedef __uint16_t __uint_least16_t;
typedef __int32_t __int_least32_t;
typedef __uint32_t __uint_least32_t;
typedef __int64_t __int_least64_t;
typedef __uint64_t __uint_least64_t;
typedef long __quad_t;
typedef unsigned long __u_quad_t;
typedef long __intmax_t;
typedef unsigned long __uintmax_t;
typedef unsigned long __dev_t;
typedef unsigned int __uid_t;
typedef unsigned int __gid_t;
typedef unsigned long __ino_t;
typedef unsigned long __ino64_t;
typedef unsigned int __mode_t;
typedef unsigned long __nlink_t;
typedef long __off_t;
typedef long __off64_t;
typedef int __pid_t;
typedef struct {
    int __val[2];
} __fsid_t;
typedef long __clock_t;
typedef unsigned long __rlim_t;
typedef unsigned long __rlim64_t;
typedef unsigned int __id_t;
typedef long __time_t;
typedef unsigned int __useconds_t;
typedef long __suseconds_t;
typedef int __daddr_t;
typedef int __key_t;
typedef int __clockid_t;
typedef void *__timer_t;
typedef long __blksize_t;
typedef long __blkcnt_t;
typedef long __blkcnt64_t;
typedef unsigned long __fsblkcnt_t;
typedef unsigned long __fsblkcnt64_t;
typedef unsigned long __fsfilcnt_t;
typedef unsigned long __fsfilcnt64_t;
typedef long __fsword_t;
typedef long __ssize_t;
typedef long __syscall_slong_t;
typedef unsigned long __syscall_ulong_t;
typedef __off64_t __loff_t;
typedef char *__caddr_t;
typedef long __intptr_t;
typedef unsigned int __socklen_t;
typedef int __sig_atomic_t;
typedef __int8_t int8_t;
typedef __int16_t int16_t;
typedef __int32_t int32_t;
typedef __int64_t int64_t;
typedef __uint8_t uint8_t;
typedef __uint16_t uint16_t;
typedef __uint32_t uint32_t;
typedef __uint64_t uint64_t;
typedef __int_least8_t int_least8_t;
typedef __int_least16_t int_least16_t;
typedef __int_least32_t int_least32_t;
typedef __int_least64_t int_least64_t;
typedef __uint_least8_t uint_least8_t;
typedef __uint_least16_t uint_least16_t;
typedef __uint_least32_t uint_least32_t;
typedef __uint_least64_t uint_least64_t;
typedef signed char int_fast8_t;
typedef long int_fast16_t;
typedef long int_fast32_t;
typedef long int_fast64_t;
typedef unsigned char uint_fast8_t;
typedef unsigned long uint_fast16_t;
typedef unsigned long uint_fast32_t;
typedef unsigned long uint_fast64_t;
typedef long intptr_t;
typedef unsigned long uintptr_t;
typedef __intmax_t intmax_t;
typedef __uintmax_t uintmax_t;
struct indirect_placeholder_41_ret_type;
struct indirect_placeholder_41_ret_type {
    uint64_t field_0;
    uint64_t field_1;
};
extern void abort(void);
extern uint64_t revng_init_local_sp(uint64_t param_0);
extern uint64_t init_rbp(void);
extern uint64_t indirect_placeholder_1(uint64_t param_0, uint64_t param_1);
extern void indirect_placeholder(void);
extern void indirect_placeholder_29(uint64_t param_0, uint64_t param_1);
extern void indirect_placeholder_16(uint64_t param_0);
extern uint64_t indirect_placeholder_9(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint64_t param_3, uint64_t param_4, uint64_t param_5);
extern void indirect_placeholder_32(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint64_t param_3, uint64_t param_4, uint64_t param_5, uint64_t param_6);
extern void indirect_placeholder_40(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint64_t param_3, uint64_t param_4, uint64_t param_5);
extern struct indirect_placeholder_41_ret_type indirect_placeholder_41(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint64_t param_3, uint64_t param_4);
void bb_main(uint64_t rdi, uint64_t rsi) {
    struct indirect_placeholder_41_ret_type var_12;
    uint32_t var_24;
    uint64_t var_0;
    uint64_t var_1;
    uint64_t var_2;
    uint32_t *var_3;
    uint64_t var_4;
    uint64_t *var_5;
    uint64_t var_6;
    uint64_t var_7;
    uint32_t *var_8;
    uint64_t local_sp_4;
    uint64_t var_28;
    uint64_t var_21;
    uint64_t local_sp_0;
    uint64_t var_22;
    uint64_t local_sp_1;
    uint64_t var_23;
    uint64_t var_25;
    uint64_t var_26;
    uint64_t var_27;
    uint64_t local_sp_2;
    uint64_t var_14;
    uint64_t var_15;
    uint64_t *var_16;
    uint64_t var_17;
    uint64_t local_sp_3;
    uint64_t var_18;
    uint64_t var_19;
    uint64_t var_20;
    uint32_t storemerge1;
    uint64_t local_sp_4_be;
    uint64_t var_9;
    uint64_t var_10;
    uint64_t var_11;
    uint32_t var_13;
    unsigned int loop_state_var;
    var_0 = revng_init_local_sp(0UL);
    var_1 = init_rbp();
    var_2 = var_0 + (-8L);
    *(uint64_t *)var_2 = var_1;
    var_3 = (uint32_t *)(var_0 + (-28L));
    *var_3 = (uint32_t)rdi;
    var_4 = var_0 + (-40L);
    var_5 = (uint64_t *)var_4;
    *var_5 = rsi;
    var_6 = **(uint64_t **)var_4;
    *(uint64_t *)(var_0 + (-48L)) = 4202125UL;
    indirect_placeholder_16(var_6);
    *(uint64_t *)(var_0 + (-56L)) = 4202140UL;
    indirect_placeholder();
    var_7 = var_0 + (-64L);
    *(uint64_t *)var_7 = 4202150UL;
    indirect_placeholder();
    *(unsigned char *)6379600UL = (unsigned char)'\x00';
    *(unsigned char *)6379601UL = (unsigned char)'\x00';
    var_8 = (uint32_t *)(var_0 + (-12L));
    storemerge1 = 2U;
    local_sp_4 = var_7;
    while (1U)
        {
            var_9 = *var_5;
            var_10 = (uint64_t)*var_3;
            var_11 = local_sp_4 + (-8L);
            *(uint64_t *)var_11 = 4202456UL;
            var_12 = indirect_placeholder_41(4270987UL, 4269568UL, var_10, var_9, 0UL);
            var_13 = (uint32_t)var_12.field_0;
            *var_8 = var_13;
            local_sp_0 = var_11;
            local_sp_2 = var_11;
            local_sp_3 = var_11;
            local_sp_4_be = var_11;
            if (var_13 != 4294967295U) {
                var_21 = var_12.field_1;
                if (*(unsigned char *)6379601UL != '\x00') {
                    loop_state_var = 0U;
                    break;
                }
                var_22 = local_sp_4 + (-16L);
                *(uint64_t *)var_22 = 4202495UL;
                indirect_placeholder();
                local_sp_0 = var_22;
                loop_state_var = 0U;
                break;
            }
            if ((uint64_t)(var_13 + (-97)) == 0UL) {
                *(unsigned char *)6379600UL = (unsigned char)'\x01';
            } else {
                if ((int)var_13 <= (int)97U) {
                    if ((uint64_t)(var_13 + 131U) != 0UL) {
                        if ((uint64_t)(var_13 + 130U) != 0UL) {
                            loop_state_var = 1U;
                            break;
                        }
                        *(uint64_t *)(local_sp_4 + (-16L)) = 4202343UL;
                        indirect_placeholder_29(var_2, 0UL);
                        abort();
                    }
                    var_14 = *(uint64_t *)6379232UL;
                    *(uint64_t *)(local_sp_4 + (-16L)) = 0UL;
                    var_15 = local_sp_4 + (-24L);
                    var_16 = (uint64_t *)var_15;
                    *var_16 = 4270971UL;
                    *(uint64_t *)(local_sp_4 + (-32L)) = 4202402UL;
                    indirect_placeholder_40(0UL, 4269272UL, var_14, 4270939UL, 4270920UL, 4270959UL);
                    *var_16 = 4202416UL;
                    indirect_placeholder();
                    local_sp_2 = var_15;
                    loop_state_var = 1U;
                    break;
                }
                if ((uint64_t)(var_13 + (-105)) == 0UL) {
                    *(unsigned char *)6379601UL = (unsigned char)'\x01';
                } else {
                    if ((uint64_t)(var_13 + (-112)) != 0UL) {
                        loop_state_var = 1U;
                        break;
                    }
                    var_17 = *(uint64_t *)6380088UL;
                    if (var_17 != 0UL) {
                        var_18 = *(uint64_t *)6379240UL;
                        var_19 = local_sp_4 + (-16L);
                        *(uint64_t *)var_19 = 4202306UL;
                        var_20 = indirect_placeholder_9(4269824UL, 4269872UL, 4270924UL, var_18, var_17, 4UL);
                        local_sp_3 = var_19;
                        storemerge1 = *(uint32_t *)((var_20 << 2UL) + 4269872UL);
                    }
                    *(uint32_t *)6379604UL = storemerge1;
                    local_sp_4_be = local_sp_3;
                }
            }
            local_sp_4 = local_sp_4_be;
            continue;
        }
    switch (loop_state_var) {
      case 1U:
        {
            *(uint64_t *)(local_sp_2 + (-8L)) = 4202426UL;
            indirect_placeholder_29(var_2, 1UL);
            abort();
        }
        break;
      case 0U:
        {
            local_sp_1 = local_sp_0;
            if (*(uint32_t *)6379604UL == 0U) {
                var_23 = local_sp_0 + (-8L);
                *(uint64_t *)var_23 = 4202520UL;
                indirect_placeholder();
                local_sp_1 = var_23;
            }
            var_24 = *(uint32_t *)6379384UL;
            var_25 = ((uint64_t)var_24 << 3UL) + *var_5;
            var_26 = (uint64_t)(*var_3 - var_24);
            *(uint64_t *)(local_sp_1 + (-8L)) = 4202566UL;
            var_27 = indirect_placeholder_1(var_26, var_25);
            *(unsigned char *)(var_0 + (-13L)) = (unsigned char)var_27;
            *(uint64_t *)(local_sp_1 + (-16L)) = 4202579UL;
            indirect_placeholder();
            if ((uint64_t)(uint32_t)var_27 != 0UL) {
                *(uint64_t *)(local_sp_1 + (-24L)) = 4202588UL;
                indirect_placeholder();
                var_28 = (uint64_t)*(uint32_t *)var_27;
                *(uint64_t *)(local_sp_1 + (-32L)) = 4202617UL;
                indirect_placeholder_32(0UL, 4271006UL, 4270991UL, 1UL, var_21, var_28, 0UL);
            }
            return;
        }
        break;
    }
}

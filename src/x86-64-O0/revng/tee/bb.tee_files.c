typedef unsigned char __u_char;
typedef unsigned short __u_short;
typedef unsigned int __u_int;
typedef unsigned long __u_long;
typedef signed char __int8_t;
typedef unsigned char __uint8_t;
typedef short __int16_t;
typedef unsigned short __uint16_t;
typedef int __int32_t;
typedef unsigned int __uint32_t;
typedef long __int64_t;
typedef unsigned long __uint64_t;
typedef __int8_t __int_least8_t;
typedef __uint8_t __uint_least8_t;
typedef __int16_t __int_least16_t;
typedef __uint16_t __uint_least16_t;
typedef __int32_t __int_least32_t;
typedef __uint32_t __uint_least32_t;
typedef __int64_t __int_least64_t;
typedef __uint64_t __uint_least64_t;
typedef long __quad_t;
typedef unsigned long __u_quad_t;
typedef long __intmax_t;
typedef unsigned long __uintmax_t;
typedef unsigned long __dev_t;
typedef unsigned int __uid_t;
typedef unsigned int __gid_t;
typedef unsigned long __ino_t;
typedef unsigned long __ino64_t;
typedef unsigned int __mode_t;
typedef unsigned long __nlink_t;
typedef long __off_t;
typedef long __off64_t;
typedef int __pid_t;
typedef struct {
    int __val[2];
} __fsid_t;
typedef long __clock_t;
typedef unsigned long __rlim_t;
typedef unsigned long __rlim64_t;
typedef unsigned int __id_t;
typedef long __time_t;
typedef unsigned int __useconds_t;
typedef long __suseconds_t;
typedef int __daddr_t;
typedef int __key_t;
typedef int __clockid_t;
typedef void *__timer_t;
typedef long __blksize_t;
typedef long __blkcnt_t;
typedef long __blkcnt64_t;
typedef unsigned long __fsblkcnt_t;
typedef unsigned long __fsblkcnt64_t;
typedef unsigned long __fsfilcnt_t;
typedef unsigned long __fsfilcnt64_t;
typedef long __fsword_t;
typedef long __ssize_t;
typedef long __syscall_slong_t;
typedef unsigned long __syscall_ulong_t;
typedef __off64_t __loff_t;
typedef char *__caddr_t;
typedef long __intptr_t;
typedef unsigned int __socklen_t;
typedef int __sig_atomic_t;
typedef __int8_t int8_t;
typedef __int16_t int16_t;
typedef __int32_t int32_t;
typedef __int64_t int64_t;
typedef __uint8_t uint8_t;
typedef __uint16_t uint16_t;
typedef __uint32_t uint32_t;
typedef __uint64_t uint64_t;
typedef __int_least8_t int_least8_t;
typedef __int_least16_t int_least16_t;
typedef __int_least32_t int_least32_t;
typedef __int_least64_t int_least64_t;
typedef __uint_least8_t uint_least8_t;
typedef __uint_least16_t uint_least16_t;
typedef __uint_least32_t uint_least32_t;
typedef __uint_least64_t uint_least64_t;
typedef signed char int_fast8_t;
typedef long int_fast16_t;
typedef long int_fast32_t;
typedef long int_fast64_t;
typedef unsigned char uint_fast8_t;
typedef unsigned long uint_fast16_t;
typedef unsigned long uint_fast32_t;
typedef unsigned long uint_fast64_t;
typedef long intptr_t;
typedef unsigned long uintptr_t;
typedef __intmax_t intmax_t;
typedef __uintmax_t uintmax_t;
struct indirect_placeholder_38_ret_type;
struct indirect_placeholder_35_ret_type;
struct indirect_placeholder_36_ret_type;
struct indirect_placeholder_37_ret_type;
struct indirect_placeholder_38_ret_type {
    uint64_t field_0;
    uint64_t field_1;
};
struct indirect_placeholder_35_ret_type {
    uint64_t field_0;
    uint64_t field_1;
    uint64_t field_2;
};
struct indirect_placeholder_36_ret_type {
    uint64_t field_0;
    uint64_t field_1;
    uint64_t field_2;
};
struct indirect_placeholder_37_ret_type {
    uint64_t field_0;
    uint64_t field_1;
    uint64_t field_2;
};
extern uint64_t revng_init_local_sp(uint64_t param_0);
extern uint64_t init_rbp(void);
extern uint64_t indirect_placeholder_1(uint64_t param_0, uint64_t param_1);
extern void indirect_placeholder(void);
extern uint64_t init_rbx(void);
extern uint64_t init_r10(void);
extern void indirect_placeholder_29(uint64_t param_0, uint64_t param_1);
extern uint64_t indirect_placeholder_13(void);
extern uint64_t init_r9(void);
extern uint64_t init_r8(void);
extern uint64_t indirect_placeholder_12(uint64_t param_0);
extern void indirect_placeholder_32(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint64_t param_3, uint64_t param_4, uint64_t param_5, uint64_t param_6);
extern struct indirect_placeholder_38_ret_type indirect_placeholder_38(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint64_t param_3, uint64_t param_4, uint64_t param_5, uint64_t param_6);
extern struct indirect_placeholder_35_ret_type indirect_placeholder_35(uint64_t param_0, uint64_t param_1, uint64_t param_2);
extern struct indirect_placeholder_36_ret_type indirect_placeholder_36(uint64_t param_0, uint64_t param_1, uint64_t param_2);
extern struct indirect_placeholder_37_ret_type indirect_placeholder_37(uint64_t param_0, uint64_t param_1, uint64_t param_2);
typedef _Bool bool;
uint64_t bb_tee_files(uint64_t rdi, uint64_t rsi) {
    struct indirect_placeholder_38_ret_type var_16;
    uint64_t var_0;
    uint64_t var_1;
    uint64_t var_2;
    uint64_t var_3;
    uint64_t var_4;
    uint64_t var_5;
    uint32_t *var_6;
    uint64_t var_7;
    uint64_t *var_8;
    uint64_t *var_9;
    uint64_t *var_10;
    unsigned char *var_11;
    uint64_t spec_select;
    uint64_t *var_12;
    uint64_t var_13;
    uint64_t var_14;
    uint64_t var_15;
    uint64_t var_17;
    uint64_t var_18;
    uint64_t *var_19;
    uint64_t var_20;
    uint64_t var_21;
    uint32_t *var_22;
    uint32_t var_23;
    uint64_t var_71;
    struct indirect_placeholder_35_ret_type var_72;
    uint64_t var_73;
    uint64_t var_74;
    uint64_t var_75;
    uint64_t var_76;
    uint64_t var_77;
    uint64_t local_sp_11;
    unsigned char storemerge;
    unsigned char var_53;
    uint64_t local_sp_1;
    unsigned char var_55;
    uint64_t var_54;
    uint64_t local_sp_0;
    uint64_t rcx_0;
    uint64_t var_56;
    struct indirect_placeholder_36_ret_type var_57;
    uint64_t var_58;
    uint64_t var_59;
    uint64_t var_60;
    uint64_t spec_select193;
    uint64_t var_61;
    uint64_t var_62;
    uint64_t r8_2;
    uint64_t r9_0;
    uint64_t r8_0;
    uint32_t var_63;
    uint64_t var_44;
    uint32_t var_45;
    uint64_t var_46;
    uint64_t rax_1;
    uint64_t var_47;
    uint64_t rax_0;
    uint64_t local_sp_2;
    uint64_t var_65;
    uint32_t var_48;
    uint64_t r9_5_ph;
    uint64_t r9_2;
    uint64_t rcx_1;
    uint64_t local_sp_7_ph;
    uint64_t local_sp_4;
    uint64_t r9_1;
    uint64_t local_sp_3;
    uint64_t r8_1;
    uint32_t rax_1_ph_in;
    uint64_t var_49;
    uint32_t var_50;
    uint64_t var_51;
    uint64_t var_52;
    uint64_t rcx_2;
    uint32_t var_64;
    uint64_t r8_4;
    uint64_t rcx_3;
    uint64_t local_sp_6;
    uint64_t r9_3;
    uint64_t local_sp_5;
    uint64_t r8_3;
    uint32_t var_38;
    uint64_t var_31;
    struct indirect_placeholder_37_ret_type var_32;
    uint64_t var_33;
    uint64_t var_34;
    uint64_t var_35;
    uint64_t var_36;
    uint64_t spec_select196;
    uint64_t var_37;
    uint64_t var_30;
    uint64_t rcx_4;
    uint64_t r9_4;
    uint64_t var_39;
    uint32_t *var_40;
    uint32_t *var_41;
    unsigned char *var_42;
    uint64_t rcx_5_ph;
    uint64_t r8_5_ph;
    uint64_t local_sp_7;
    uint64_t rax_2;
    uint64_t local_sp_8;
    uint64_t local_sp_9;
    uint64_t var_66;
    uint64_t var_67;
    uint32_t var_68;
    uint64_t local_sp_10;
    uint64_t var_69;
    uint64_t var_70;
    uint32_t var_78;
    uint64_t var_43;
    uint64_t var_24;
    uint64_t var_25;
    uint64_t var_26;
    uint64_t var_27;
    uint64_t var_28;
    uint64_t var_29;
    unsigned int loop_state_var;
    bool switch_state_var;
    var_0 = revng_init_local_sp(0UL);
    var_1 = init_rbp();
    var_2 = init_r9();
    var_3 = init_r10();
    var_4 = init_r8();
    var_5 = init_rbx();
    *(uint64_t *)(var_0 + (-8L)) = var_1;
    *(uint64_t *)(var_0 + (-16L)) = var_5;
    var_6 = (uint32_t *)(var_0 + (-1100L));
    *var_6 = (uint32_t)rdi;
    var_7 = var_0 + (-1112L);
    var_8 = (uint64_t *)var_7;
    *var_8 = rsi;
    var_9 = (uint64_t *)(var_0 + (-32L));
    *var_9 = 0UL;
    var_10 = (uint64_t *)(var_0 + (-40L));
    *var_10 = 0UL;
    var_11 = (unsigned char *)(var_0 + (-45L));
    *var_11 = (unsigned char)'\x01';
    spec_select = (*(unsigned char *)6379600UL == '\x00') ? 4271011UL : 4271009UL;
    var_12 = (uint64_t *)(var_0 + (-56L));
    *var_12 = spec_select;
    *(uint64_t *)(var_0 + (-1120L)) = 4202724UL;
    indirect_placeholder_29(0UL, 0UL);
    *(uint64_t *)(var_0 + (-1128L)) = 4202739UL;
    indirect_placeholder_29(1UL, 0UL);
    var_13 = *(uint64_t *)6379464UL;
    *(uint64_t *)(var_0 + (-1136L)) = 4202759UL;
    var_14 = indirect_placeholder_1(var_13, 2UL);
    var_15 = (uint64_t)((long)(((uint64_t)*var_6 << 32UL) + 4294967296UL) >> (long)32UL);
    *(uint64_t *)(var_0 + (-1144L)) = 4202783UL;
    var_16 = indirect_placeholder_38(var_14, var_15, var_2, 8UL, var_3, var_4, var_5);
    var_17 = var_16.field_0;
    var_18 = var_0 + (-64L);
    var_19 = (uint64_t *)var_18;
    *var_19 = var_17;
    *var_8 = (*var_8 + (-8L));
    **(uint64_t **)var_18 = *(uint64_t *)6379456UL;
    *(uint64_t *)(var_0 + (-1152L)) = 4202819UL;
    var_20 = indirect_placeholder_12(4271013UL);
    **(uint64_t **)var_7 = var_20;
    var_21 = var_0 + (-1160L);
    *(uint64_t *)var_21 = 4202862UL;
    indirect_placeholder();
    *var_9 = (*var_9 + 1UL);
    var_22 = (uint32_t *)(var_0 + (-44L));
    *var_22 = 1U;
    var_23 = 1U;
    storemerge = (unsigned char)'\x01';
    var_48 = 0U;
    r8_4 = var_4;
    rcx_3 = 0UL;
    local_sp_6 = var_21;
    rcx_4 = 0UL;
    r9_4 = var_2;
    var_68 = 1U;
    r9_5_ph = r9_4;
    local_sp_7_ph = local_sp_6;
    rax_1_ph_in = var_23;
    r9_3 = r9_4;
    r8_3 = r8_4;
    rcx_5_ph = rcx_4;
    r8_5_ph = r8_4;
    while ((long)((uint64_t)var_23 << 32UL) <= (long)((uint64_t)*var_6 << 32UL))
        {
            var_24 = (uint64_t)var_23 << 3UL;
            var_25 = *var_19 + var_24;
            var_26 = *(uint64_t *)(*var_8 + var_24);
            var_27 = *var_12;
            *(uint64_t *)(local_sp_6 + (-8L)) = 4202941UL;
            var_28 = indirect_placeholder_1(var_26, var_27);
            *(uint64_t *)var_25 = var_28;
            var_29 = (uint64_t)*var_22 << 3UL;
            if (*(uint64_t *)(*var_19 + var_29) == 0UL) {
                var_31 = *(uint64_t *)(*var_8 + var_29);
                *(uint64_t *)(local_sp_6 + (-16L)) = 4203016UL;
                var_32 = indirect_placeholder_37(var_31, 0UL, 3UL);
                var_33 = var_32.field_0;
                var_34 = var_32.field_1;
                var_35 = var_32.field_2;
                *(uint64_t *)(local_sp_6 + (-24L)) = 4203024UL;
                indirect_placeholder();
                var_36 = (uint64_t)*(uint32_t *)var_33;
                spec_select196 = ((*(uint32_t *)6379604UL + (-3)) < 2U) ? 1UL : 0UL;
                var_37 = local_sp_6 + (-32L);
                *(uint64_t *)var_37 = 4203080UL;
                indirect_placeholder_32(0UL, 4271006UL, var_33, spec_select196, var_34, var_36, var_35);
                *var_11 = (unsigned char)'\x00';
                rcx_3 = var_33;
                r9_3 = var_34;
                local_sp_5 = var_37;
                r8_3 = var_35;
            } else {
                var_30 = local_sp_6 + (-16L);
                *(uint64_t *)var_30 = 4203132UL;
                indirect_placeholder();
                *var_9 = (*var_9 + 1UL);
                local_sp_5 = var_30;
            }
            var_38 = *var_22 + 1U;
            *var_22 = var_38;
            var_23 = var_38;
            r8_4 = r8_3;
            local_sp_6 = local_sp_5;
            rcx_4 = rcx_3;
            r9_4 = r9_3;
            r9_5_ph = r9_4;
            local_sp_7_ph = local_sp_6;
            rax_1_ph_in = var_23;
            r9_3 = r9_4;
            r8_3 = r8_4;
            rcx_5_ph = rcx_4;
            r8_5_ph = r8_4;
        }
    var_39 = var_0 + (-1096L);
    var_40 = (uint32_t *)var_39;
    var_41 = (uint32_t *)(var_0 + (-68L));
    var_42 = (unsigned char *)(var_0 + (-69L));
    var_47 = var_39;
    rax_0 = var_39;
    while (1U)
        {
            rax_1 = (uint64_t)rax_1_ph_in;
            rcx_1 = rcx_5_ph;
            r9_1 = r9_5_ph;
            r8_1 = r8_5_ph;
            local_sp_7 = local_sp_7_ph;
            while (1U)
                {
                    rax_2 = rax_1;
                    local_sp_8 = local_sp_7;
                    if (*var_9 != 0UL) {
                        var_65 = *var_10;
                        loop_state_var = 2U;
                        break;
                    }
                    var_43 = local_sp_7 + (-8L);
                    *(uint64_t *)var_43 = 4203186UL;
                    indirect_placeholder();
                    *var_10 = var_39;
                    local_sp_2 = var_43;
                    if ((long)var_39 <= (long)18446744073709551615UL) {
                        loop_state_var = 0U;
                        break;
                    }
                    var_44 = local_sp_7 + (-16L);
                    *(uint64_t *)var_44 = 4203202UL;
                    indirect_placeholder();
                    var_45 = *var_40;
                    var_46 = (uint64_t)var_45;
                    rax_0 = var_46;
                    local_sp_2 = var_44;
                    rax_1 = var_46;
                    local_sp_7 = var_44;
                    if ((uint64_t)(var_45 + (-4)) == 0UL) {
                        continue;
                    }
                    loop_state_var = 1U;
                    break;
                }
            switch_state_var = 0;
            switch (loop_state_var) {
              case 2U:
                {
                    switch_state_var = 1;
                    break;
                }
                break;
              case 1U:
              case 0U:
                {
                    switch (loop_state_var) {
                      case 1U:
                        {
                            var_47 = *var_10;
                        }
                        break;
                      case 0U:
                        {
                        }
                        break;
                    }
                    local_sp_3 = local_sp_2;
                    var_65 = var_47;
                    rax_2 = rax_0;
                    local_sp_8 = local_sp_2;
                    if ((long)var_47 > (long)0UL) {
                        switch_state_var = 1;
                        break;
                    }
                    *var_22 = 0U;
                    r8_2 = r8_1;
                    r9_0 = r9_1;
                    r8_0 = r8_1;
                    var_63 = var_48;
                    r9_5_ph = r9_1;
                    r9_2 = r9_1;
                    local_sp_7_ph = local_sp_3;
                    local_sp_4 = local_sp_3;
                    rax_1_ph_in = var_48;
                    rcx_2 = rcx_1;
                    rcx_5_ph = rcx_1;
                    r8_5_ph = r8_1;
                    while ((long)((uint64_t)var_48 << 32UL) <= (long)((uint64_t)*var_6 << 32UL))
                        {
                            var_49 = *(uint64_t *)(*var_19 + ((uint64_t)var_48 << 3UL));
                            rcx_0 = var_49;
                            if (var_49 == 0UL) {
                                var_64 = var_63 + 1U;
                                *var_22 = var_64;
                                var_48 = var_64;
                                rcx_1 = rcx_2;
                                r9_1 = r9_2;
                                local_sp_3 = local_sp_4;
                                r8_1 = r8_2;
                                r8_2 = r8_1;
                                r9_0 = r9_1;
                                r8_0 = r8_1;
                                var_63 = var_48;
                                r9_5_ph = r9_1;
                                r9_2 = r9_1;
                                local_sp_7_ph = local_sp_3;
                                local_sp_4 = local_sp_3;
                                rax_1_ph_in = var_48;
                                rcx_2 = rcx_1;
                                rcx_5_ph = rcx_1;
                                r8_5_ph = r8_1;
                                continue;
                            }
                            *(uint64_t *)(local_sp_3 + (-8L)) = 4203320UL;
                            indirect_placeholder();
                            *(uint64_t *)(local_sp_3 + (-16L)) = 4203335UL;
                            indirect_placeholder();
                            var_50 = *var_40;
                            var_51 = (uint64_t)var_50;
                            *var_41 = var_50;
                            var_52 = local_sp_3 + (-24L);
                            *(uint64_t *)var_52 = 4203345UL;
                            indirect_placeholder();
                            local_sp_0 = var_52;
                            if (*(uint32_t *)var_51 == 32U) {
                            } else {
                                var_53 = storemerge & '\x01';
                                *var_42 = var_53;
                                var_55 = var_53;
                                if (*(uint64_t *)(*var_19 + ((uint64_t)*var_22 << 3UL)) == *(uint64_t *)6379456UL) {
                                    var_54 = local_sp_3 + (-32L);
                                    *(uint64_t *)var_54 = 4203443UL;
                                    indirect_placeholder();
                                    var_55 = *var_42;
                                    local_sp_0 = var_54;
                                }
                                local_sp_1 = local_sp_0;
                                if (var_55 == '\x00') {
                                    var_56 = *(uint64_t *)(*var_8 + ((uint64_t)*var_22 << 3UL));
                                    *(uint64_t *)(local_sp_0 + (-8L)) = 4203493UL;
                                    var_57 = indirect_placeholder_36(var_56, 0UL, 3UL);
                                    var_58 = var_57.field_0;
                                    var_59 = var_57.field_1;
                                    var_60 = var_57.field_2;
                                    spec_select193 = ((*(uint32_t *)6379604UL + (-3)) < 2U) ? 1UL : 0UL;
                                    var_61 = (uint64_t)*var_41;
                                    var_62 = local_sp_0 + (-16L);
                                    *(uint64_t *)var_62 = 4203553UL;
                                    indirect_placeholder_32(0UL, 4271006UL, var_58, spec_select193, var_59, var_61, var_60);
                                    rcx_0 = var_58;
                                    r9_0 = var_59;
                                    local_sp_1 = var_62;
                                    r8_0 = var_60;
                                }
                                *(uint64_t *)(*var_19 + ((uint64_t)*var_22 << 3UL)) = 0UL;
                                rcx_2 = rcx_0;
                                r9_2 = r9_0;
                                local_sp_4 = local_sp_1;
                                r8_2 = r8_0;
                                if (*var_42 != '\x00') {
                                    *var_11 = (unsigned char)'\x00';
                                }
                                *var_9 = (*var_9 + (-1L));
                                var_63 = *var_22;
                            }
                        }
                    continue;
                }
                break;
            }
            if (switch_state_var)
                break;
        }
    local_sp_9 = local_sp_8;
    if (var_65 == 18446744073709551615UL) {
        *(uint64_t *)(local_sp_8 + (-8L)) = 4203637UL;
        indirect_placeholder();
        var_66 = (uint64_t)*(uint32_t *)rax_2;
        var_67 = local_sp_8 + (-16L);
        *(uint64_t *)var_67 = 4203661UL;
        indirect_placeholder_32(0UL, 4271029UL, rcx_5_ph, 0UL, r9_5_ph, var_66, r8_5_ph);
        *var_11 = (unsigned char)'\x00';
        local_sp_9 = var_67;
    }
    *var_22 = 1U;
    local_sp_10 = local_sp_9;
    local_sp_11 = local_sp_10;
    while ((long)((uint64_t)var_68 << 32UL) <= (long)((uint64_t)*var_6 << 32UL))
        {
            var_69 = local_sp_10 + (-8L);
            *(uint64_t *)var_69 = 4203736UL;
            var_70 = indirect_placeholder_13();
            local_sp_11 = var_69;
            if (*(uint64_t *)(*var_19 + ((uint64_t)var_68 << 3UL)) != 0UL & (uint64_t)(uint32_t)var_70 == 0UL) {
                var_71 = *(uint64_t *)(*var_8 + ((uint64_t)*var_22 << 3UL));
                *(uint64_t *)(local_sp_10 + (-16L)) = 4203784UL;
                var_72 = indirect_placeholder_35(var_71, 0UL, 3UL);
                var_73 = var_72.field_0;
                var_74 = var_72.field_1;
                var_75 = var_72.field_2;
                *(uint64_t *)(local_sp_10 + (-24L)) = 4203792UL;
                indirect_placeholder();
                var_76 = (uint64_t)*(uint32_t *)var_73;
                var_77 = local_sp_10 + (-32L);
                *(uint64_t *)var_77 = 4203819UL;
                indirect_placeholder_32(0UL, 4271006UL, var_73, 0UL, var_74, var_76, var_75);
                *var_11 = (unsigned char)'\x00';
                local_sp_11 = var_77;
            }
            var_78 = *var_22 + 1U;
            *var_22 = var_78;
            var_68 = var_78;
            local_sp_10 = local_sp_11;
            local_sp_11 = local_sp_10;
        }
    *(uint64_t *)(local_sp_10 + (-8L)) = 4203854UL;
    indirect_placeholder();
    return (uint64_t)*var_11;
}

typedef unsigned char __u_char;
typedef unsigned short __u_short;
typedef unsigned int __u_int;
typedef unsigned long __u_long;
typedef signed char __int8_t;
typedef unsigned char __uint8_t;
typedef short __int16_t;
typedef unsigned short __uint16_t;
typedef int __int32_t;
typedef unsigned int __uint32_t;
typedef long __int64_t;
typedef unsigned long __uint64_t;
typedef __int8_t __int_least8_t;
typedef __uint8_t __uint_least8_t;
typedef __int16_t __int_least16_t;
typedef __uint16_t __uint_least16_t;
typedef __int32_t __int_least32_t;
typedef __uint32_t __uint_least32_t;
typedef __int64_t __int_least64_t;
typedef __uint64_t __uint_least64_t;
typedef long __quad_t;
typedef unsigned long __u_quad_t;
typedef long __intmax_t;
typedef unsigned long __uintmax_t;
typedef unsigned long __dev_t;
typedef unsigned int __uid_t;
typedef unsigned int __gid_t;
typedef unsigned long __ino_t;
typedef unsigned long __ino64_t;
typedef unsigned int __mode_t;
typedef unsigned long __nlink_t;
typedef long __off_t;
typedef long __off64_t;
typedef int __pid_t;
typedef struct {
    int __val[2];
} __fsid_t;
typedef long __clock_t;
typedef unsigned long __rlim_t;
typedef unsigned long __rlim64_t;
typedef unsigned int __id_t;
typedef long __time_t;
typedef unsigned int __useconds_t;
typedef long __suseconds_t;
typedef int __daddr_t;
typedef int __key_t;
typedef int __clockid_t;
typedef void *__timer_t;
typedef long __blksize_t;
typedef long __blkcnt_t;
typedef long __blkcnt64_t;
typedef unsigned long __fsblkcnt_t;
typedef unsigned long __fsblkcnt64_t;
typedef unsigned long __fsfilcnt_t;
typedef unsigned long __fsfilcnt64_t;
typedef long __fsword_t;
typedef long __ssize_t;
typedef long __syscall_slong_t;
typedef unsigned long __syscall_ulong_t;
typedef __off64_t __loff_t;
typedef char *__caddr_t;
typedef long __intptr_t;
typedef unsigned int __socklen_t;
typedef int __sig_atomic_t;
typedef __int8_t int8_t;
typedef __int16_t int16_t;
typedef __int32_t int32_t;
typedef __int64_t int64_t;
typedef __uint8_t uint8_t;
typedef __uint16_t uint16_t;
typedef __uint32_t uint32_t;
typedef __uint64_t uint64_t;
typedef __int_least8_t int_least8_t;
typedef __int_least16_t int_least16_t;
typedef __int_least32_t int_least32_t;
typedef __int_least64_t int_least64_t;
typedef __uint_least8_t uint_least8_t;
typedef __uint_least16_t uint_least16_t;
typedef __uint_least32_t uint_least32_t;
typedef __uint_least64_t uint_least64_t;
typedef signed char int_fast8_t;
typedef long int_fast16_t;
typedef long int_fast32_t;
typedef long int_fast64_t;
typedef unsigned char uint_fast8_t;
typedef unsigned long uint_fast16_t;
typedef unsigned long uint_fast32_t;
typedef unsigned long uint_fast64_t;
typedef long intptr_t;
typedef unsigned long uintptr_t;
typedef __intmax_t intmax_t;
typedef __uintmax_t uintmax_t;
extern uint64_t revng_init_local_sp(uint64_t param_0);
extern uint64_t init_rbp(void);
uint64_t bb_is_local_fs_type(uint64_t rdi) {
    uint64_t var_0;
    uint64_t var_1;
    uint64_t rax_0;
    var_0 = revng_init_local_sp(0UL);
    var_1 = init_rbp();
    *(uint64_t *)(var_0 + (-8L)) = var_1;
    *(uint64_t *)(var_0 + (-16L)) = rdi;
    rax_0 = 1UL;
    if (rdi == 684539205UL) {
        return rax_0;
    }
    rax_0 = 0UL;
    if (rdi > 684539205UL) {
        if (rdi != 1702057283UL) {
            rax_0 = 1UL;
            if (rdi > 1702057283UL) {
                if (rdi != 2240043254UL) {
                    if (rdi > 2240043254UL) {
                        if (rdi != 3380511080UL) {
                            rax_0 = 0UL;
                            if (rdi > 3380511080UL) {
                                if (rdi != 4185718668UL) {
                                    rax_0 = 0UL;
                                    if (rdi <= 4185718668UL) {
                                        break;
                                    }
                                    switch (rdi) {
                                      case 4283649346UL:
                                      case 4266872130UL:
                                        {
                                            break;
                                        }
                                        break;
                                      case 4187351113UL:
                                        {
                                            rax_0 = 1UL;
                                        }
                                        break;
                                      default:
                                        {
                                            rax_0 = 4294967295UL;
                                        }
                                        break;
                                    }
                                }
                            } else {
                                if (rdi != 2866260714UL) {
                                    rax_0 = 1UL;
                                    if (rdi > 2866260714UL) {
                                        rax_0 = 0UL;
                                        switch (rdi) {
                                          case 3203391149UL:
                                          case 3133910204UL:
                                            {
                                                break;
                                            }
                                            break;
                                          case 2881100148UL:
                                            {
                                                rax_0 = 1UL;
                                            }
                                            break;
                                          default:
                                            {
                                                rax_0 = 4294967295UL;
                                            }
                                            break;
                                        }
                                    } else {
                                        switch (rdi) {
                                          case 2508478710UL:
                                          case 2435016766UL:
                                            {
                                                break;
                                            }
                                            break;
                                          case 2768370933UL:
                                            {
                                                rax_0 = 0UL;
                                            }
                                            break;
                                          default:
                                            {
                                                rax_0 = 4294967295UL;
                                            }
                                            break;
                                        }
                                    }
                                }
                            }
                        }
                    } else {
                        if (rdi != 1935894131UL) {
                            rax_0 = 0UL;
                            if (rdi > 1935894131UL) {
                                if (rdi != 1952539503UL) {
                                    rax_0 = 1UL;
                                    if (rdi > 1952539503UL) {
                                        rax_0 = 0UL;
                                        switch (rdi) {
                                          case 2088527475UL:
                                          case 2035054128UL:
                                            {
                                                break;
                                            }
                                            break;
                                          case 1953653091UL:
                                            {
                                                rax_0 = 1UL;
                                            }
                                            break;
                                          default:
                                            {
                                                rax_0 = 4294967295UL;
                                            }
                                            break;
                                        }
                                    } else {
                                        switch (rdi) {
                                          case 1936880249UL:
                                          case 1936814952UL:
                                            {
                                                break;
                                            }
                                            break;
                                          case 1937076805UL:
                                            {
                                                rax_0 = 0UL;
                                            }
                                            break;
                                          default:
                                            {
                                                rax_0 = 4294967295UL;
                                            }
                                            break;
                                        }
                                    }
                                }
                            } else {
                                rax_0 = 1UL;
                                if (rdi != 1746473250UL) {
                                    rax_0 = 0UL;
                                    if (rdi > 1746473250UL) {
                                        switch (rdi) {
                                          case 1852207972UL:
                                          case 1799439955UL:
                                            {
                                                break;
                                            }
                                            break;
                                          case 1853056627UL:
                                            {
                                                rax_0 = 1UL;
                                            }
                                            break;
                                          default:
                                            {
                                                rax_0 = 4294967295UL;
                                            }
                                            break;
                                        }
                                    } else {
                                        rax_0 = (rdi == 1702057286UL) ? 0UL : ((rdi == 1733912937UL) ? 1UL : 4294967295UL);
                                    }
                                }
                            }
                        }
                    }
                }
            } else {
                if (rdi != 1397703499UL) {
                    if (rdi > 1397703499UL) {
                        if (rdi != 1634035564UL) {
                            if (rdi > 1634035564UL) {
                                if (rdi != 1650812274UL) {
                                    if (rdi > 1650812274UL) {
                                        break;
                                    }
                                    switch (rdi) {
                                      case 1650812272UL:
                                      case 1650746742UL:
                                        {
                                            break;
                                        }
                                        break;
                                      case 1635083891UL:
                                        {
                                            rax_0 = 0UL;
                                        }
                                        break;
                                      default:
                                        {
                                            rax_0 = 4294967295UL;
                                        }
                                        break;
                                    }
                                }
                            } else {
                                if (rdi != 1479104553UL) {
                                    if (rdi <= 1479104553UL) {
                                        break;
                                    }
                                    switch (rdi) {
                                      case 1513908720UL:
                                      case 1481003842UL:
                                        {
                                            break;
                                        }
                                        break;
                                      case 1633904243UL:
                                        {
                                            rax_0 = 0UL;
                                        }
                                        break;
                                      default:
                                        {
                                            rax_0 = 4294967295UL;
                                        }
                                        break;
                                    }
                                }
                            }
                        }
                    } else {
                        if (rdi != 1161678120UL) {
                            if (rdi <= 1161678120UL) {
                                if (rdi != 827541066UL) {
                                    if (rdi <= 827541066UL) {
                                        break;
                                    }
                                    break;
                                }
                            }
                            if (rdi != 1397109069UL) {
                                rax_0 = 0UL;
                                if (rdi > 1397109069UL) {
                                    rax_0 = 1UL;
                                    switch (rdi) {
                                      case 1397118030UL:
                                      case 1397114950UL:
                                        {
                                            break;
                                        }
                                        break;
                                      case 1397113167UL:
                                        {
                                            rax_0 = 0UL;
                                        }
                                        break;
                                      default:
                                        {
                                            rax_0 = 4294967295UL;
                                        }
                                        break;
                                    }
                                } else {
                                    switch (rdi) {
                                      case 1346981957UL:
                                      case 1196443219UL:
                                        {
                                            break;
                                        }
                                        break;
                                      case 1382369651UL:
                                        {
                                            rax_0 = 1UL;
                                        }
                                        break;
                                      default:
                                        {
                                            rax_0 = 4294967295UL;
                                        }
                                        break;
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }
    } else {
        if (rdi != 44533UL) {
            if (rdi > 44533UL) {
                if (rdi != 19920821UL) {
                    rax_0 = 0UL;
                    if (rdi > 19920821UL) {
                        if (rdi != 198183888UL) {
                            rax_0 = 1UL;
                            if (rdi > 198183888UL) {
                                if (rdi != 427819522UL) {
                                    if (rdi <= 427819522UL) {
                                        break;
                                    }
                                    switch (rdi) {
                                      case 604313861UL:
                                      case 464386766UL:
                                        {
                                            break;
                                        }
                                        break;
                                      case 428016422UL:
                                        {
                                            rax_0 = 0UL;
                                        }
                                        break;
                                      default:
                                        {
                                            rax_0 = 4294967295UL;
                                        }
                                        break;
                                    }
                                }
                            } else {
                                rax_0 = 0UL;
                                if (rdi != 19993000UL) {
                                    rax_0 = 1UL;
                                    if (rdi > 19993000UL) {
                                        break;
                                    }
                                    if ((rdi + (-19920822L)) < 2UL) {
                                        rax_0 = 4294967295UL;
                                    }
                                }
                            }
                        }
                    } else {
                        if (rdi != 4278867UL) {
                            if (rdi <= 4278867UL) {
                                if (rdi != 61267UL) {
                                    if (rdi <= 61267UL) {
                                        break;
                                    }
                                    break;
                                }
                            }
                            if (rdi != 16914839UL) {
                                rax_0 = 0UL;
                                if (rdi > 16914839UL) {
                                    rax_0 = 1UL;
                                    switch (rdi) {
                                      case 19920820UL:
                                      case 19911021UL:
                                        {
                                            break;
                                        }
                                        break;
                                      case 18225520UL:
                                        {
                                            rax_0 = 0UL;
                                        }
                                        break;
                                      default:
                                        {
                                            rax_0 = 4294967295UL;
                                        }
                                        break;
                                    }
                                } else {
                                    switch (rdi) {
                                      case 12805120UL:
                                        {
                                            break;
                                        }
                                        break;
                                      case 16914836UL:
                                        {
                                            rax_0 = 1UL;
                                        }
                                        break;
                                      case 12648430UL:
                                        {
                                            rax_0 = 1UL;
                                        }
                                        break;
                                      default:
                                        {
                                            rax_0 = 4294967295UL;
                                        }
                                        break;
                                    }
                                }
                            }
                        }
                    }
                }
            } else {
                if (rdi != 16964UL) {
                    if (rdi <= 16964UL) {
                        if (rdi != 5007UL) {
                            if (rdi <= 5007UL) {
                                if (rdi != 1984UL) {
                                    if (rdi <= 1984UL) {
                                        break;
                                    }
                                    break;
                                }
                            }
                            if (rdi != 13364UL) {
                                if (rdi <= 13364UL) {
                                    break;
                                }
                                break;
                            }
                        }
                    }
                    if (rdi != 24053UL) {
                        if (rdi > 24053UL) {
                            if (rdi != 38496UL) {
                                if (rdi > 38496UL) {
                                    if ((rdi + (-40864L)) < 3UL) {
                                        rax_0 = 4294967295UL;
                                    }
                                } else {
                                    switch (rdi) {
                                      case 29366UL:
                                      case 29301UL:
                                        {
                                            break;
                                        }
                                        break;
                                      case 26985UL:
                                        {
                                            rax_0 = 0UL;
                                        }
                                        break;
                                      default:
                                        {
                                            rax_0 = 4294967295UL;
                                        }
                                        break;
                                    }
                                }
                            }
                        } else {
                            if (rdi != 19780UL) {
                                rax_0 = 0UL;
                                if (rdi <= 19780UL) {
                                    break;
                                }
                                switch (rdi) {
                                  case 22092UL:
                                  case 20859UL:
                                    {
                                        break;
                                    }
                                    break;
                                  case 19802UL:
                                    {
                                        rax_0 = 1UL;
                                    }
                                    break;
                                  default:
                                    {
                                        rax_0 = 4294967295UL;
                                    }
                                    break;
                                }
                            }
                        }
                    }
                }
            }
        }
    }
}

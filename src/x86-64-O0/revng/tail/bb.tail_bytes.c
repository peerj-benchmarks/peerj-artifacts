typedef unsigned char __u_char;
typedef unsigned short __u_short;
typedef unsigned int __u_int;
typedef unsigned long __u_long;
typedef signed char __int8_t;
typedef unsigned char __uint8_t;
typedef short __int16_t;
typedef unsigned short __uint16_t;
typedef int __int32_t;
typedef unsigned int __uint32_t;
typedef long __int64_t;
typedef unsigned long __uint64_t;
typedef __int8_t __int_least8_t;
typedef __uint8_t __uint_least8_t;
typedef __int16_t __int_least16_t;
typedef __uint16_t __uint_least16_t;
typedef __int32_t __int_least32_t;
typedef __uint32_t __uint_least32_t;
typedef __int64_t __int_least64_t;
typedef __uint64_t __uint_least64_t;
typedef long __quad_t;
typedef unsigned long __u_quad_t;
typedef long __intmax_t;
typedef unsigned long __uintmax_t;
typedef unsigned long __dev_t;
typedef unsigned int __uid_t;
typedef unsigned int __gid_t;
typedef unsigned long __ino_t;
typedef unsigned long __ino64_t;
typedef unsigned int __mode_t;
typedef unsigned long __nlink_t;
typedef long __off_t;
typedef long __off64_t;
typedef int __pid_t;
typedef struct {
    int __val[2];
} __fsid_t;
typedef long __clock_t;
typedef unsigned long __rlim_t;
typedef unsigned long __rlim64_t;
typedef unsigned int __id_t;
typedef long __time_t;
typedef unsigned int __useconds_t;
typedef long __suseconds_t;
typedef int __daddr_t;
typedef int __key_t;
typedef int __clockid_t;
typedef void *__timer_t;
typedef long __blksize_t;
typedef long __blkcnt_t;
typedef long __blkcnt64_t;
typedef unsigned long __fsblkcnt_t;
typedef unsigned long __fsblkcnt64_t;
typedef unsigned long __fsfilcnt_t;
typedef unsigned long __fsfilcnt64_t;
typedef long __fsword_t;
typedef long __ssize_t;
typedef long __syscall_slong_t;
typedef unsigned long __syscall_ulong_t;
typedef __off64_t __loff_t;
typedef char *__caddr_t;
typedef long __intptr_t;
typedef unsigned int __socklen_t;
typedef int __sig_atomic_t;
typedef __int8_t int8_t;
typedef __int16_t int16_t;
typedef __int32_t int32_t;
typedef __int64_t int64_t;
typedef __uint8_t uint8_t;
typedef __uint16_t uint16_t;
typedef __uint32_t uint32_t;
typedef __uint64_t uint64_t;
typedef __int_least8_t int_least8_t;
typedef __int_least16_t int_least16_t;
typedef __int_least32_t int_least32_t;
typedef __int_least64_t int_least64_t;
typedef __uint_least8_t uint_least8_t;
typedef __uint_least16_t uint_least16_t;
typedef __uint_least32_t uint_least32_t;
typedef __uint_least64_t uint_least64_t;
typedef signed char int_fast8_t;
typedef long int_fast16_t;
typedef long int_fast32_t;
typedef long int_fast64_t;
typedef unsigned char uint_fast8_t;
typedef unsigned long uint_fast16_t;
typedef unsigned long uint_fast32_t;
typedef unsigned long uint_fast64_t;
typedef long intptr_t;
typedef unsigned long uintptr_t;
typedef __intmax_t intmax_t;
typedef __uintmax_t uintmax_t;
struct indirect_placeholder_110_ret_type;
struct indirect_placeholder_109_ret_type;
struct indirect_placeholder_110_ret_type {
    uint64_t field_0;
    uint64_t field_1;
    uint64_t field_2;
};
struct indirect_placeholder_109_ret_type {
    uint64_t field_0;
    uint64_t field_1;
    uint64_t field_2;
    uint64_t field_3;
};
extern uint64_t revng_init_local_sp(uint64_t param_0);
extern uint64_t init_rbp(void);
extern void indirect_placeholder(void);
extern uint64_t init_rbx(void);
extern uint64_t indirect_placeholder_5(uint64_t param_0);
extern uint64_t indirect_placeholder_13(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint64_t param_3);
extern uint64_t helper_cc_compute_all_wrapper(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint32_t param_3);
extern struct indirect_placeholder_110_ret_type indirect_placeholder_110(uint64_t param_0, uint64_t param_1);
extern struct indirect_placeholder_109_ret_type indirect_placeholder_109(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint64_t param_3, uint64_t param_4, uint64_t param_5, uint64_t param_6);
uint64_t bb_tail_bytes(uint64_t rdx, uint64_t rcx, uint64_t rdi, uint64_t rsi) {
    uint64_t var_0;
    uint64_t var_1;
    uint64_t var_2;
    uint64_t *var_3;
    uint32_t *var_4;
    uint64_t *var_5;
    uint64_t var_6;
    uint64_t *var_7;
    uint32_t var_8;
    uint64_t var_9;
    uint64_t local_sp_4;
    uint64_t var_38;
    uint64_t var_39;
    uint64_t var_32;
    uint64_t local_sp_2;
    uint64_t local_sp_0;
    uint64_t **var_22;
    uint64_t *var_23;
    uint64_t **_pre117_pre_phi;
    uint64_t local_sp_1;
    uint64_t **_pre_phi;
    uint64_t rax_0;
    uint64_t var_10;
    struct indirect_placeholder_110_ret_type var_11;
    uint64_t var_12;
    uint64_t var_13;
    uint64_t var_14;
    uint64_t var_15;
    uint64_t var_24;
    uint64_t _pre;
    uint64_t var_16;
    uint64_t var_17;
    uint64_t var_18;
    uint64_t var_19;
    uint64_t var_20;
    uint64_t var_21;
    uint64_t var_25;
    uint64_t var_26;
    uint64_t var_27;
    uint64_t var_28;
    uint64_t var_29;
    uint32_t *var_30;
    uint32_t var_31;
    uint64_t *var_33;
    uint64_t *var_34;
    uint64_t var_35;
    uint64_t var_36;
    uint64_t var_37;
    uint64_t *var_40;
    uint64_t var_41;
    uint64_t storemerge;
    uint64_t var_42;
    uint64_t var_43;
    uint64_t var_44;
    uint64_t var_45;
    uint64_t var_46;
    uint64_t var_47;
    uint64_t var_48;
    uint64_t var_49;
    uint64_t var_54;
    uint64_t var_50;
    uint64_t var_51;
    uint64_t var_52;
    uint64_t var_53;
    uint64_t var_55;
    uint64_t local_sp_5;
    uint64_t local_sp_6;
    uint64_t var_56;
    uint64_t var_57;
    uint64_t var_58;
    uint64_t var_59;
    uint64_t var_60;
    uint64_t var_61;
    uint64_t var_62;
    uint64_t **var_63;
    uint64_t var_64;
    uint64_t local_sp_7;
    uint64_t var_65;
    uint64_t var_66;
    uint64_t var_67;
    uint64_t *var_68;
    var_0 = revng_init_local_sp(0UL);
    var_1 = init_rbp();
    var_2 = init_rbx();
    *(uint64_t *)(var_0 + (-8L)) = var_1;
    *(uint64_t *)(var_0 + (-16L)) = var_2;
    var_3 = (uint64_t *)(var_0 + (-208L));
    *var_3 = rdi;
    var_4 = (uint32_t *)(var_0 + (-212L));
    *var_4 = (uint32_t)rsi;
    var_5 = (uint64_t *)(var_0 + (-224L));
    *var_5 = rdx;
    var_6 = var_0 + (-232L);
    var_7 = (uint64_t *)var_6;
    *var_7 = rcx;
    var_8 = *var_4;
    var_9 = var_0 + (-240L);
    *(uint64_t *)var_9 = 4221468UL;
    indirect_placeholder();
    local_sp_4 = var_9;
    local_sp_2 = var_9;
    rax_0 = 1UL;
    storemerge = 512UL;
    var_64 = 18446744073709551615UL;
    if (var_8 == 0U) {
        var_10 = *var_3;
        *(uint64_t *)(var_0 + (-248L)) = 4221492UL;
        var_11 = indirect_placeholder_110(4UL, var_10);
        var_12 = var_11.field_0;
        var_13 = var_11.field_1;
        var_14 = var_11.field_2;
        *(uint64_t *)(var_0 + (-256L)) = 4221500UL;
        indirect_placeholder();
        var_15 = (uint64_t)*(uint32_t *)var_12;
        *(uint64_t *)(var_0 + (-264L)) = 4221527UL;
        indirect_placeholder_109(0UL, 4307918UL, var_12, 0UL, var_15, var_13, var_14);
        rax_0 = 0UL;
    } else {
        if (*(unsigned char *)6424084UL == '\x00') {
            var_33 = (uint64_t *)(var_0 + (-32L));
            *var_33 = 18446744073709551615UL;
            var_34 = (uint64_t *)(var_0 + (-40L));
            *var_34 = 18446744073709551615UL;
            if (*(unsigned char *)6424093UL != '\x01' & (long)*var_5 >= (long)0UL) {
                var_35 = var_0 + (-200L);
                var_36 = var_0 + (-248L);
                *(uint64_t *)var_36 = 4221834UL;
                var_37 = indirect_placeholder_5(var_35);
                local_sp_4 = var_36;
                if ((uint64_t)(unsigned char)var_37 == 0UL) {
                    var_38 = (uint64_t)*var_4;
                    var_39 = var_0 + (-256L);
                    *(uint64_t *)var_39 = 4221885UL;
                    indirect_placeholder();
                    *var_34 = var_38;
                    *var_33 = (*var_5 + var_38);
                    local_sp_4 = var_39;
                } else {
                    *var_33 = *(uint64_t *)(var_0 + (-152L));
                }
            }
            var_40 = (uint64_t *)(var_0 + (-144L));
            var_41 = helper_cc_compute_all_wrapper(*var_40, 0UL, 0UL, 25U);
            local_sp_5 = local_sp_4;
            if ((uint64_t)(((unsigned char)(var_41 >> 4UL) ^ (unsigned char)var_41) & '\xc0') == 0UL) {
                var_42 = *var_40;
                storemerge = (var_42 > 2305843009213693952UL) ? 512UL : var_42;
            }
            var_43 = *var_33;
            var_54 = var_43;
            if ((long)storemerge >= (long)var_43) {
                var_44 = *var_7;
                var_45 = *var_5;
                var_46 = (uint64_t)*var_4;
                var_47 = *var_3;
                *(uint64_t *)(local_sp_4 + (-8L)) = 4222006UL;
                var_48 = indirect_placeholder_13(var_45, var_44, var_47, var_46);
                rax_0 = var_48;
                return rax_0;
            }
            var_49 = *var_34;
            var_55 = var_49;
            if (var_49 == 18446744073709551615UL) {
                var_50 = *var_3;
                var_51 = (uint64_t)*var_4;
                var_52 = local_sp_4 + (-8L);
                *(uint64_t *)var_52 = 4222051UL;
                var_53 = indirect_placeholder_13(1UL, var_50, var_51, 0UL);
                *var_34 = var_53;
                var_54 = *var_33;
                var_55 = var_53;
                local_sp_5 = var_52;
            }
            local_sp_6 = local_sp_5;
            var_56 = var_54 - var_55;
            *(uint64_t *)(var_0 + (-56L)) = var_56;
            var_57 = *var_5;
            if ((long)var_55 >= (long)var_54 & var_56 > var_57) {
                var_58 = *var_33 - var_57;
                *var_34 = var_58;
                var_59 = *var_3;
                var_60 = (uint64_t)*var_4;
                var_61 = local_sp_5 + (-8L);
                *(uint64_t *)var_61 = 4222137UL;
                indirect_placeholder_13(0UL, var_59, var_60, var_58);
                local_sp_6 = var_61;
            }
            var_62 = *var_34;
            var_63 = (uint64_t **)var_6;
            **var_63 = var_62;
            _pre_phi = var_63;
            var_64 = *var_5;
            local_sp_7 = local_sp_6;
        } else {
            if (*(unsigned char *)6424093UL == '\x01') {
                _pre = *var_5;
                var_24 = _pre;
                var_25 = *var_7;
                var_26 = (uint64_t)*var_4;
                var_27 = *var_3;
                var_28 = var_9 + (-8L);
                *(uint64_t *)var_28 = 4221741UL;
                var_29 = indirect_placeholder_13(var_24, var_25, var_27, var_26);
                var_30 = (uint32_t *)(var_0 + (-44L));
                var_31 = (uint32_t)var_29;
                *var_30 = var_31;
                local_sp_1 = var_28;
                if (var_31 != 0U) {
                    var_32 = (uint64_t)(var_31 >> 31U);
                    rax_0 = var_32;
                    return rax_0;
                }
                _pre117_pre_phi = (uint64_t **)var_6;
            } else {
                var_16 = *var_5;
                var_24 = var_16;
                if ((long)var_16 < (long)0UL) {
                    var_25 = *var_7;
                    var_26 = (uint64_t)*var_4;
                    var_27 = *var_3;
                    var_28 = var_9 + (-8L);
                    *(uint64_t *)var_28 = 4221741UL;
                    var_29 = indirect_placeholder_13(var_24, var_25, var_27, var_26);
                    var_30 = (uint32_t *)(var_0 + (-44L));
                    var_31 = (uint32_t)var_29;
                    *var_30 = var_31;
                    local_sp_1 = var_28;
                    if (var_31 != 0U) {
                        var_32 = (uint64_t)(var_31 >> 31U);
                        rax_0 = var_32;
                        return rax_0;
                    }
                    _pre117_pre_phi = (uint64_t **)var_6;
                } else {
                    if ((uint32_t)((uint16_t)*(uint32_t *)(var_0 + (-176L)) & (unsigned short)61440U) == 32768U) {
                        var_21 = local_sp_2 + (-8L);
                        *(uint64_t *)var_21 = 4221668UL;
                        indirect_placeholder();
                        local_sp_0 = var_21;
                    } else {
                        var_17 = *var_3;
                        var_18 = (uint64_t)*var_4;
                        var_19 = var_0 + (-248L);
                        *(uint64_t *)var_19 = 4221635UL;
                        var_20 = indirect_placeholder_13(1UL, var_17, var_18, var_16);
                        local_sp_0 = var_19;
                        local_sp_2 = var_19;
                        if ((long)var_20 > (long)18446744073709551615UL) {
                            var_21 = local_sp_2 + (-8L);
                            *(uint64_t *)var_21 = 4221668UL;
                            indirect_placeholder();
                            local_sp_0 = var_21;
                        }
                    }
                    var_22 = (uint64_t **)var_6;
                    var_23 = *var_22;
                    *var_23 = (*var_23 + *var_5);
                    _pre117_pre_phi = var_22;
                    local_sp_1 = local_sp_0;
                }
            }
            *var_5 = 18446744073709551615UL;
            _pre_phi = _pre117_pre_phi;
            local_sp_7 = local_sp_1;
        }
        var_65 = (uint64_t)*var_4;
        var_66 = *var_3;
        *(uint64_t *)(local_sp_7 + (-8L)) = 4222184UL;
        var_67 = indirect_placeholder_13(var_65, var_64, 0UL, var_66);
        var_68 = *_pre_phi;
        *var_68 = (var_67 + *var_68);
    }
    return rax_0;
}

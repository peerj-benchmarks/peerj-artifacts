typedef unsigned char __u_char;
typedef unsigned short __u_short;
typedef unsigned int __u_int;
typedef unsigned long __u_long;
typedef signed char __int8_t;
typedef unsigned char __uint8_t;
typedef short __int16_t;
typedef unsigned short __uint16_t;
typedef int __int32_t;
typedef unsigned int __uint32_t;
typedef long __int64_t;
typedef unsigned long __uint64_t;
typedef __int8_t __int_least8_t;
typedef __uint8_t __uint_least8_t;
typedef __int16_t __int_least16_t;
typedef __uint16_t __uint_least16_t;
typedef __int32_t __int_least32_t;
typedef __uint32_t __uint_least32_t;
typedef __int64_t __int_least64_t;
typedef __uint64_t __uint_least64_t;
typedef long __quad_t;
typedef unsigned long __u_quad_t;
typedef long __intmax_t;
typedef unsigned long __uintmax_t;
typedef unsigned long __dev_t;
typedef unsigned int __uid_t;
typedef unsigned int __gid_t;
typedef unsigned long __ino_t;
typedef unsigned long __ino64_t;
typedef unsigned int __mode_t;
typedef unsigned long __nlink_t;
typedef long __off_t;
typedef long __off64_t;
typedef int __pid_t;
typedef struct {
    int __val[2];
} __fsid_t;
typedef long __clock_t;
typedef unsigned long __rlim_t;
typedef unsigned long __rlim64_t;
typedef unsigned int __id_t;
typedef long __time_t;
typedef unsigned int __useconds_t;
typedef long __suseconds_t;
typedef int __daddr_t;
typedef int __key_t;
typedef int __clockid_t;
typedef void *__timer_t;
typedef long __blksize_t;
typedef long __blkcnt_t;
typedef long __blkcnt64_t;
typedef unsigned long __fsblkcnt_t;
typedef unsigned long __fsblkcnt64_t;
typedef unsigned long __fsfilcnt_t;
typedef unsigned long __fsfilcnt64_t;
typedef long __fsword_t;
typedef long __ssize_t;
typedef long __syscall_slong_t;
typedef unsigned long __syscall_ulong_t;
typedef __off64_t __loff_t;
typedef char *__caddr_t;
typedef long __intptr_t;
typedef unsigned int __socklen_t;
typedef int __sig_atomic_t;
typedef __int8_t int8_t;
typedef __int16_t int16_t;
typedef __int32_t int32_t;
typedef __int64_t int64_t;
typedef __uint8_t uint8_t;
typedef __uint16_t uint16_t;
typedef __uint32_t uint32_t;
typedef __uint64_t uint64_t;
typedef __int_least8_t int_least8_t;
typedef __int_least16_t int_least16_t;
typedef __int_least32_t int_least32_t;
typedef __int_least64_t int_least64_t;
typedef __uint_least8_t uint_least8_t;
typedef __uint_least16_t uint_least16_t;
typedef __uint_least32_t uint_least32_t;
typedef __uint_least64_t uint_least64_t;
typedef signed char int_fast8_t;
typedef long int_fast16_t;
typedef long int_fast32_t;
typedef long int_fast64_t;
typedef unsigned char uint_fast8_t;
typedef unsigned long uint_fast16_t;
typedef unsigned long uint_fast32_t;
typedef unsigned long uint_fast64_t;
typedef long intptr_t;
typedef unsigned long uintptr_t;
typedef __intmax_t intmax_t;
typedef __uintmax_t uintmax_t;
struct indirect_placeholder_30_ret_type;
struct indirect_placeholder_29_ret_type;
struct indirect_placeholder_30_ret_type {
    uint64_t field_0;
    uint64_t field_1;
    uint64_t field_2;
};
struct indirect_placeholder_29_ret_type {
    uint64_t field_0;
    uint64_t field_1;
    uint64_t field_2;
    uint64_t field_3;
};
extern uint64_t revng_init_local_sp(uint64_t param_0);
extern uint64_t init_rbp(void);
extern uint64_t indirect_placeholder_7(uint64_t param_0, uint64_t param_1);
extern uint64_t init_cc_src2(void);
extern uint64_t helper_cc_compute_c_wrapper(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint32_t param_3);
extern void indirect_placeholder(void);
extern uint64_t indirect_placeholder_31(uint64_t param_0, uint64_t param_1, uint64_t param_2);
extern void indirect_placeholder_1(uint64_t param_0, uint64_t param_1);
extern uint64_t init_rbx(void);
extern struct indirect_placeholder_30_ret_type indirect_placeholder_30(uint64_t param_0, uint64_t param_1);
extern struct indirect_placeholder_29_ret_type indirect_placeholder_29(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint64_t param_3, uint64_t param_4, uint64_t param_5, uint64_t param_6);
typedef _Bool bool;
uint64_t bb_start_lines(uint64_t rdx, uint64_t rcx, uint64_t rdi, uint64_t rsi) {
    uint64_t var_0;
    uint64_t var_1;
    uint64_t var_2;
    uint64_t var_3;
    uint64_t *var_4;
    uint32_t *var_5;
    uint64_t *var_6;
    uint64_t var_7;
    uint64_t rax_0;
    uint64_t var_27;
    uint64_t var_22;
    uint64_t var_28;
    uint64_t var_29;
    uint64_t var_30;
    uint64_t var_31;
    uint64_t var_32;
    uint64_t var_33;
    uint64_t var_15;
    struct indirect_placeholder_30_ret_type var_16;
    uint64_t var_17;
    uint64_t var_18;
    uint64_t var_19;
    uint64_t var_20;
    uint64_t *var_21;
    uint64_t local_sp_0;
    uint64_t var_23;
    uint64_t var_24;
    uint64_t var_25;
    uint64_t var_26;
    uint64_t local_sp_1;
    uint64_t *var_8;
    uint64_t var_9;
    uint64_t *var_10;
    uint64_t *var_11;
    uint64_t var_12;
    uint64_t var_13;
    uint64_t var_14;
    unsigned int loop_state_var;
    bool switch_state_var;
    var_0 = revng_init_local_sp(0UL);
    var_1 = init_rbp();
    var_2 = init_cc_src2();
    var_3 = init_rbx();
    *(uint64_t *)(var_0 + (-8L)) = var_1;
    *(uint64_t *)(var_0 + (-16L)) = var_3;
    var_4 = (uint64_t *)(var_0 + (-1088L));
    *var_4 = rdi;
    var_5 = (uint32_t *)(var_0 + (-1092L));
    *var_5 = (uint32_t)rsi;
    var_6 = (uint64_t *)(var_0 + (-1104L));
    *var_6 = rdx;
    var_7 = var_0 + (-1112L);
    *(uint64_t *)var_7 = rcx;
    rax_0 = 0UL;
    local_sp_1 = var_7;
    if (*var_6 == 0UL) {
        return rax_0;
    }
    var_8 = (uint64_t *)(var_0 + (-40L));
    var_9 = var_0 + (-1080L);
    var_10 = (uint64_t *)(var_0 + (-48L));
    var_11 = (uint64_t *)(var_0 + (-32L));
    var_22 = var_9;
    rax_0 = 4294967295UL;
    while (1U)
        {
            var_12 = (uint64_t)*var_5;
            var_13 = local_sp_1 + (-8L);
            *(uint64_t *)var_13 = 4211293UL;
            var_14 = indirect_placeholder_7(1024UL, var_12);
            *var_8 = var_14;
            local_sp_0 = var_13;
            switch_state_var = 0;
            switch (var_14) {
              case 0UL:
                {
                    switch_state_var = 1;
                    break;
                }
                break;
              case 18446744073709551615UL:
                {
                    var_15 = *var_4;
                    *(uint64_t *)(local_sp_1 + (-16L)) = 4211341UL;
                    var_16 = indirect_placeholder_30(4UL, var_15);
                    var_17 = var_16.field_0;
                    var_18 = var_16.field_1;
                    var_19 = var_16.field_2;
                    *(uint64_t *)(local_sp_1 + (-24L)) = 4211349UL;
                    indirect_placeholder();
                    var_20 = (uint64_t)*(uint32_t *)var_17;
                    *(uint64_t *)(local_sp_1 + (-32L)) = 4211376UL;
                    indirect_placeholder_29(0UL, 4306959UL, var_17, 0UL, var_20, var_18, var_19);
                    rax_0 = 1UL;
                    switch_state_var = 1;
                    break;
                }
                break;
              default:
                {
                    *var_10 = (var_14 + var_9);
                    var_21 = *(uint64_t **)var_7;
                    *var_21 = (*var_21 + *var_8);
                    *var_11 = var_9;
                    rax_0 = 0UL;
                    while (1U)
                        {
                            var_23 = *var_10 - var_22;
                            var_24 = (uint64_t)(uint32_t)(uint64_t)*(unsigned char *)6424086UL;
                            var_25 = local_sp_0 + (-8L);
                            *(uint64_t *)var_25 = 4211557UL;
                            var_26 = indirect_placeholder_31(var_23, var_22, var_24);
                            *var_11 = var_26;
                            local_sp_0 = var_25;
                            local_sp_1 = var_25;
                            if (var_26 != 0UL) {
                                loop_state_var = 1U;
                                break;
                            }
                            *var_11 = (var_26 + 1UL);
                            var_27 = *var_6 + (-1L);
                            *var_6 = var_27;
                            if (var_27 == 0UL) {
                                var_22 = *var_11;
                                continue;
                            }
                            var_28 = *var_11;
                            var_29 = *var_10;
                            var_30 = helper_cc_compute_c_wrapper(var_28 - var_29, var_29, var_2, 17U);
                            if (var_30 != 0UL) {
                                loop_state_var = 0U;
                                break;
                            }
                            var_31 = *var_10;
                            var_32 = *var_11;
                            var_33 = var_31 - var_32;
                            *(uint64_t *)(local_sp_0 + (-16L)) = 4211509UL;
                            indirect_placeholder_1(var_32, var_33);
                            loop_state_var = 0U;
                            break;
                        }
                    switch_state_var = 0;
                    switch (loop_state_var) {
                      case 0U:
                        {
                            switch_state_var = 1;
                            break;
                        }
                        break;
                      case 1U:
                        {
                            continue;
                        }
                        break;
                    }
                    if (switch_state_var)
                        break;
                }
                break;
            }
            if (switch_state_var)
                break;
        }
    return rax_0;
}

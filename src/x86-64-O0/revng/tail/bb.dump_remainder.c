typedef unsigned char __u_char;
typedef unsigned short __u_short;
typedef unsigned int __u_int;
typedef unsigned long __u_long;
typedef signed char __int8_t;
typedef unsigned char __uint8_t;
typedef short __int16_t;
typedef unsigned short __uint16_t;
typedef int __int32_t;
typedef unsigned int __uint32_t;
typedef long __int64_t;
typedef unsigned long __uint64_t;
typedef __int8_t __int_least8_t;
typedef __uint8_t __uint_least8_t;
typedef __int16_t __int_least16_t;
typedef __uint16_t __uint_least16_t;
typedef __int32_t __int_least32_t;
typedef __uint32_t __uint_least32_t;
typedef __int64_t __int_least64_t;
typedef __uint64_t __uint_least64_t;
typedef long __quad_t;
typedef unsigned long __u_quad_t;
typedef long __intmax_t;
typedef unsigned long __uintmax_t;
typedef unsigned long __dev_t;
typedef unsigned int __uid_t;
typedef unsigned int __gid_t;
typedef unsigned long __ino_t;
typedef unsigned long __ino64_t;
typedef unsigned int __mode_t;
typedef unsigned long __nlink_t;
typedef long __off_t;
typedef long __off64_t;
typedef int __pid_t;
typedef struct {
    int __val[2];
} __fsid_t;
typedef long __clock_t;
typedef unsigned long __rlim_t;
typedef unsigned long __rlim64_t;
typedef unsigned int __id_t;
typedef long __time_t;
typedef unsigned int __useconds_t;
typedef long __suseconds_t;
typedef int __daddr_t;
typedef int __key_t;
typedef int __clockid_t;
typedef void *__timer_t;
typedef long __blksize_t;
typedef long __blkcnt_t;
typedef long __blkcnt64_t;
typedef unsigned long __fsblkcnt_t;
typedef unsigned long __fsblkcnt64_t;
typedef unsigned long __fsfilcnt_t;
typedef unsigned long __fsfilcnt64_t;
typedef long __fsword_t;
typedef long __ssize_t;
typedef long __syscall_slong_t;
typedef unsigned long __syscall_ulong_t;
typedef __off64_t __loff_t;
typedef char *__caddr_t;
typedef long __intptr_t;
typedef unsigned int __socklen_t;
typedef int __sig_atomic_t;
typedef __int8_t int8_t;
typedef __int16_t int16_t;
typedef __int32_t int32_t;
typedef __int64_t int64_t;
typedef __uint8_t uint8_t;
typedef __uint16_t uint16_t;
typedef __uint32_t uint32_t;
typedef __uint64_t uint64_t;
typedef __int_least8_t int_least8_t;
typedef __int_least16_t int_least16_t;
typedef __int_least32_t int_least32_t;
typedef __int_least64_t int_least64_t;
typedef __uint_least8_t uint_least8_t;
typedef __uint_least16_t uint_least16_t;
typedef __uint_least32_t uint_least32_t;
typedef __uint_least64_t uint_least64_t;
typedef signed char int_fast8_t;
typedef long int_fast16_t;
typedef long int_fast32_t;
typedef long int_fast64_t;
typedef unsigned char uint_fast8_t;
typedef unsigned long uint_fast16_t;
typedef unsigned long uint_fast32_t;
typedef unsigned long uint_fast64_t;
typedef long intptr_t;
typedef unsigned long uintptr_t;
typedef __intmax_t intmax_t;
typedef __uintmax_t uintmax_t;
struct indirect_placeholder_59_ret_type;
struct indirect_placeholder_58_ret_type;
struct indirect_placeholder_59_ret_type {
    uint64_t field_0;
    uint64_t field_1;
    uint64_t field_2;
};
struct indirect_placeholder_58_ret_type {
    uint64_t field_0;
    uint64_t field_1;
    uint64_t field_2;
    uint64_t field_3;
};
extern uint64_t revng_init_local_sp(uint64_t param_0);
extern uint64_t init_rbp(void);
extern uint64_t indirect_placeholder_7(uint64_t param_0, uint64_t param_1);
extern void indirect_placeholder(void);
extern void indirect_placeholder_1(uint64_t param_0, uint64_t param_1);
extern uint64_t init_rbx(void);
extern void indirect_placeholder_16(uint64_t param_0);
extern struct indirect_placeholder_59_ret_type indirect_placeholder_59(uint64_t param_0, uint64_t param_1);
extern struct indirect_placeholder_58_ret_type indirect_placeholder_58(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint64_t param_3, uint64_t param_4, uint64_t param_5, uint64_t param_6);
typedef _Bool bool;
uint64_t bb_dump_remainder(uint64_t rdx, uint64_t rcx, uint64_t rdi, uint64_t rsi) {
    uint64_t var_0;
    uint64_t var_1;
    uint64_t var_2;
    uint64_t var_3;
    uint64_t *var_4;
    uint32_t *var_5;
    uint64_t *var_6;
    unsigned char *var_7;
    uint64_t var_8;
    uint64_t *var_9;
    uint64_t *var_10;
    uint64_t *var_11;
    uint64_t *var_12;
    uint64_t var_13;
    uint64_t local_sp_1;
    uint64_t var_29;
    uint64_t var_19;
    struct indirect_placeholder_59_ret_type var_20;
    uint64_t var_21;
    uint64_t var_22;
    uint64_t var_23;
    uint64_t var_24;
    uint64_t var_27;
    uint64_t var_25;
    uint64_t var_26;
    uint64_t local_sp_0;
    uint64_t var_28;
    uint64_t var_14;
    uint64_t var_15;
    uint64_t var_16;
    uint64_t var_17;
    uint64_t var_18;
    bool switch_state_var;
    var_0 = revng_init_local_sp(0UL);
    var_1 = init_rbp();
    var_2 = init_rbx();
    *(uint64_t *)(var_0 + (-8L)) = var_1;
    *(uint64_t *)(var_0 + (-16L)) = var_2;
    var_3 = var_0 + (-1112L);
    var_4 = (uint64_t *)(var_0 + (-1096L));
    *var_4 = rsi;
    var_5 = (uint32_t *)(var_0 + (-1088L));
    *var_5 = (uint32_t)rdx;
    var_6 = (uint64_t *)(var_0 + (-1104L));
    *var_6 = rcx;
    var_7 = (unsigned char *)(var_0 + (-1084L));
    *var_7 = (unsigned char)rdi;
    var_8 = *var_6;
    var_9 = (uint64_t *)(var_0 + (-40L));
    *var_9 = var_8;
    var_10 = (uint64_t *)(var_0 + (-32L));
    *var_10 = 0UL;
    var_11 = (uint64_t *)(var_0 + (-48L));
    var_12 = (uint64_t *)(var_0 + (-56L));
    var_13 = var_0 + (-1080L);
    local_sp_1 = var_3;
    while (1U)
        {
            var_14 = *var_9;
            var_15 = (var_14 > 1024UL) ? 1024UL : var_14;
            *var_11 = var_15;
            var_16 = (uint64_t)*var_5;
            var_17 = local_sp_1 + (-8L);
            *(uint64_t *)var_17 = 4207478UL;
            var_18 = indirect_placeholder_7(var_15, var_16);
            *var_12 = var_18;
            var_27 = var_18;
            local_sp_0 = var_17;
            switch_state_var = 0;
            switch (var_18) {
              case 0UL:
                {
                    switch_state_var = 1;
                    break;
                }
                break;
              case 18446744073709551615UL:
                {
                    *(uint64_t *)(local_sp_1 + (-16L)) = 4207494UL;
                    indirect_placeholder();
                    if (*(uint32_t *)18446744073709551615UL == 11U) {
                        switch_state_var = 1;
                        break;
                    }
                    var_19 = *var_4;
                    *(uint64_t *)(local_sp_1 + (-24L)) = 4207521UL;
                    var_20 = indirect_placeholder_59(4UL, var_19);
                    var_21 = var_20.field_0;
                    var_22 = var_20.field_1;
                    var_23 = var_20.field_2;
                    *(uint64_t *)(local_sp_1 + (-32L)) = 4207529UL;
                    indirect_placeholder();
                    var_24 = (uint64_t)*(uint32_t *)var_21;
                    *(uint64_t *)(local_sp_1 + (-40L)) = 4207556UL;
                    indirect_placeholder_58(0UL, 4306959UL, var_21, 1UL, var_24, var_22, var_23);
                    switch_state_var = 1;
                    break;
                }
                break;
              default:
                {
                    if (*var_7 == '\x00') {
                        var_25 = *var_4;
                        var_26 = local_sp_1 + (-16L);
                        *(uint64_t *)var_26 = 4207591UL;
                        indirect_placeholder_16(var_25);
                        *var_7 = (unsigned char)'\x00';
                        var_27 = *var_12;
                        local_sp_0 = var_26;
                    }
                    var_28 = local_sp_0 + (-8L);
                    *(uint64_t *)var_28 = 4207620UL;
                    indirect_placeholder_1(var_13, var_27);
                    *var_10 = (*var_10 + *var_12);
                    local_sp_1 = var_28;
                    if (*var_6 != 18446744073709551615UL) {
                        continue;
                    }
                    var_29 = *var_9 - *var_12;
                    *var_9 = var_29;
                    if (var_29 == 0UL) {
                        switch_state_var = 1;
                        break;
                    }
                    if (*var_6 != 18446744073709551614UL) {
                        continue;
                    }
                    switch_state_var = 1;
                    break;
                }
                break;
            }
            if (switch_state_var)
                break;
        }
    return *var_10;
}

typedef unsigned char __u_char;
typedef unsigned short __u_short;
typedef unsigned int __u_int;
typedef unsigned long __u_long;
typedef signed char __int8_t;
typedef unsigned char __uint8_t;
typedef short __int16_t;
typedef unsigned short __uint16_t;
typedef int __int32_t;
typedef unsigned int __uint32_t;
typedef long __int64_t;
typedef unsigned long __uint64_t;
typedef __int8_t __int_least8_t;
typedef __uint8_t __uint_least8_t;
typedef __int16_t __int_least16_t;
typedef __uint16_t __uint_least16_t;
typedef __int32_t __int_least32_t;
typedef __uint32_t __uint_least32_t;
typedef __int64_t __int_least64_t;
typedef __uint64_t __uint_least64_t;
typedef long __quad_t;
typedef unsigned long __u_quad_t;
typedef long __intmax_t;
typedef unsigned long __uintmax_t;
typedef unsigned long __dev_t;
typedef unsigned int __uid_t;
typedef unsigned int __gid_t;
typedef unsigned long __ino_t;
typedef unsigned long __ino64_t;
typedef unsigned int __mode_t;
typedef unsigned long __nlink_t;
typedef long __off_t;
typedef long __off64_t;
typedef int __pid_t;
typedef struct {
    int __val[2];
} __fsid_t;
typedef long __clock_t;
typedef unsigned long __rlim_t;
typedef unsigned long __rlim64_t;
typedef unsigned int __id_t;
typedef long __time_t;
typedef unsigned int __useconds_t;
typedef long __suseconds_t;
typedef int __daddr_t;
typedef int __key_t;
typedef int __clockid_t;
typedef void *__timer_t;
typedef long __blksize_t;
typedef long __blkcnt_t;
typedef long __blkcnt64_t;
typedef unsigned long __fsblkcnt_t;
typedef unsigned long __fsblkcnt64_t;
typedef unsigned long __fsfilcnt_t;
typedef unsigned long __fsfilcnt64_t;
typedef long __fsword_t;
typedef long __ssize_t;
typedef long __syscall_slong_t;
typedef unsigned long __syscall_ulong_t;
typedef __off64_t __loff_t;
typedef char *__caddr_t;
typedef long __intptr_t;
typedef unsigned int __socklen_t;
typedef int __sig_atomic_t;
typedef __int8_t int8_t;
typedef __int16_t int16_t;
typedef __int32_t int32_t;
typedef __int64_t int64_t;
typedef __uint8_t uint8_t;
typedef __uint16_t uint16_t;
typedef __uint32_t uint32_t;
typedef __uint64_t uint64_t;
typedef __int_least8_t int_least8_t;
typedef __int_least16_t int_least16_t;
typedef __int_least32_t int_least32_t;
typedef __int_least64_t int_least64_t;
typedef __uint_least8_t uint_least8_t;
typedef __uint_least16_t uint_least16_t;
typedef __uint_least32_t uint_least32_t;
typedef __uint_least64_t uint_least64_t;
typedef signed char int_fast8_t;
typedef long int_fast16_t;
typedef long int_fast32_t;
typedef long int_fast64_t;
typedef unsigned char uint_fast8_t;
typedef unsigned long uint_fast16_t;
typedef unsigned long uint_fast32_t;
typedef unsigned long uint_fast64_t;
typedef long intptr_t;
typedef unsigned long uintptr_t;
typedef __intmax_t intmax_t;
typedef __uintmax_t uintmax_t;
struct indirect_placeholder_53_ret_type;
struct indirect_placeholder_52_ret_type;
struct indirect_placeholder_55_ret_type;
struct indirect_placeholder_54_ret_type;
struct indirect_placeholder_53_ret_type {
    uint64_t field_0;
    uint64_t field_1;
    uint64_t field_2;
};
struct indirect_placeholder_52_ret_type {
    uint64_t field_0;
    uint64_t field_1;
    uint64_t field_2;
    uint64_t field_3;
};
struct indirect_placeholder_55_ret_type {
    uint64_t field_0;
    uint64_t field_1;
    uint64_t field_2;
};
struct indirect_placeholder_54_ret_type {
    uint64_t field_0;
    uint64_t field_1;
    uint64_t field_2;
    uint64_t field_3;
};
extern uint64_t revng_init_local_sp(uint64_t param_0);
extern uint64_t init_rbp(void);
extern uint64_t indirect_placeholder_7(uint64_t param_0, uint64_t param_1);
extern void indirect_placeholder(void);
extern void indirect_placeholder_1(uint64_t param_0, uint64_t param_1);
extern uint64_t init_rbx(void);
extern uint64_t indirect_placeholder_13(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint64_t param_3);
extern struct indirect_placeholder_53_ret_type indirect_placeholder_53(uint64_t param_0, uint64_t param_1);
extern struct indirect_placeholder_52_ret_type indirect_placeholder_52(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint64_t param_3, uint64_t param_4, uint64_t param_5, uint64_t param_6);
extern struct indirect_placeholder_55_ret_type indirect_placeholder_55(uint64_t param_0, uint64_t param_1);
extern struct indirect_placeholder_54_ret_type indirect_placeholder_54(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint64_t param_3, uint64_t param_4, uint64_t param_5, uint64_t param_6);
typedef _Bool bool;
uint64_t bb_file_lines(uint64_t rdx, uint64_t rcx, uint64_t rdi, uint64_t rsi, uint64_t r9, uint64_t r8) {
    uint64_t var_33;
    uint64_t var_0;
    uint64_t var_1;
    uint64_t var_2;
    uint64_t var_3;
    uint64_t *var_4;
    uint32_t *var_5;
    uint64_t *var_6;
    uint64_t *var_7;
    uint64_t *var_8;
    uint64_t var_9;
    uint64_t var_10;
    uint64_t *var_11;
    uint64_t rax_0;
    uint64_t var_53;
    struct indirect_placeholder_53_ret_type var_54;
    uint64_t var_55;
    uint64_t var_56;
    uint64_t var_57;
    uint64_t var_58;
    uint64_t var_52;
    uint64_t var_29;
    uint64_t var_34;
    uint64_t var_35;
    uint64_t var_39;
    uint64_t var_36;
    uint64_t var_37;
    uint64_t var_38;
    uint64_t local_sp_0;
    uint64_t var_40;
    uint64_t var_41;
    uint64_t var_42;
    uint64_t var_43;
    uint64_t *var_44;
    uint64_t var_65;
    struct indirect_placeholder_55_ret_type var_66;
    uint64_t var_67;
    uint64_t var_68;
    uint64_t var_69;
    uint64_t var_70;
    uint64_t var_23;
    uint64_t **var_24;
    uint64_t var_25;
    uint64_t _pre;
    uint64_t *var_26;
    uint64_t var_27;
    uint64_t *var_28;
    uint64_t local_sp_1;
    uint64_t var_30;
    uint64_t local_sp_2;
    uint64_t var_45;
    uint64_t var_59;
    uint64_t var_60;
    uint64_t var_61;
    uint64_t var_62;
    uint64_t var_63;
    uint64_t var_64;
    uint64_t var_46;
    uint64_t var_47;
    uint64_t var_48;
    uint64_t var_49;
    uint64_t var_50;
    uint64_t var_51;
    uint64_t var_31;
    uint64_t var_32;
    uint64_t var_12;
    uint64_t var_13;
    uint64_t var_14;
    uint64_t *var_15;
    uint64_t spec_store_select;
    uint64_t var_16;
    uint64_t var_17;
    uint64_t var_18;
    uint64_t var_19;
    uint64_t var_20;
    uint64_t var_21;
    uint64_t var_22;
    unsigned int loop_state_var;
    bool switch_state_var;
    var_0 = revng_init_local_sp(0UL);
    var_1 = init_rbp();
    var_2 = init_rbx();
    var_3 = var_0 + (-8L);
    *(uint64_t *)var_3 = var_1;
    *(uint64_t *)(var_0 + (-16L)) = var_2;
    var_4 = (uint64_t *)(var_0 + (-1088L));
    *var_4 = rdi;
    var_5 = (uint32_t *)(var_0 + (-1092L));
    *var_5 = (uint32_t)rsi;
    var_6 = (uint64_t *)(var_0 + (-1104L));
    *var_6 = rdx;
    var_7 = (uint64_t *)(var_0 + (-1112L));
    *var_7 = rcx;
    var_8 = (uint64_t *)(var_0 + (-1120L));
    *var_8 = r8;
    var_9 = var_0 + (-1128L);
    *(uint64_t *)var_9 = r9;
    var_10 = *var_8;
    var_11 = (uint64_t *)(var_0 + (-40L));
    *var_11 = var_10;
    _pre = 0UL;
    rax_0 = 1UL;
    if (*var_6 == 0UL) {
        return rax_0;
    }
    var_12 = var_10 - *var_7;
    var_13 = (uint64_t)((long)var_12 >> (long)63UL) >> 54UL;
    var_14 = (uint64_t)(((uint16_t)var_12 + (uint16_t)var_13) & (unsigned short)1023U) - var_13;
    var_15 = (uint64_t *)(var_0 + (-32L));
    spec_store_select = (var_14 == 0UL) ? 1024UL : var_14;
    *var_15 = spec_store_select;
    var_16 = *var_11 - spec_store_select;
    *var_11 = var_16;
    var_17 = *var_4;
    var_18 = (uint64_t)*var_5;
    *(uint64_t *)(var_0 + (-1136L)) = 4208191UL;
    indirect_placeholder_13(0UL, var_17, var_18, var_16);
    var_19 = *var_15;
    var_20 = (uint64_t)*var_5;
    var_21 = var_0 + (-1144L);
    *(uint64_t *)var_21 = 4208218UL;
    var_22 = indirect_placeholder_7(var_19, var_20);
    *var_15 = var_22;
    local_sp_1 = var_21;
    rax_0 = 0UL;
    if (var_22 != 18446744073709551615UL) {
        var_23 = *var_11 + var_22;
        var_24 = (uint64_t **)var_9;
        **var_24 = var_23;
        var_25 = *var_15;
        rax_0 = 1UL;
        _pre = var_25;
        if (var_25 != 0UL & (uint64_t)(*(unsigned char *)(((var_25 + (-1L)) + var_3) + (-1072L)) - *(unsigned char *)6424086UL) == 0UL) {
            *var_6 = (*var_6 + (-1L));
            _pre = *var_15;
        }
        var_26 = (uint64_t *)(var_0 + (-48L));
        var_27 = var_0 + (-1080L);
        var_28 = (uint64_t *)(var_0 + (-56L));
        var_29 = _pre;
        while (1U)
            {
                *var_26 = var_29;
                var_30 = var_29;
                local_sp_2 = local_sp_1;
                while (1U)
                    {
                        if (var_30 != 0UL) {
                            loop_state_var = 0U;
                            break;
                        }
                        var_31 = local_sp_2 + (-8L);
                        *(uint64_t *)var_31 = 4208401UL;
                        indirect_placeholder();
                        *var_28 = var_27;
                        *var_26 = 0UL;
                        var_32 = *var_6;
                        *var_6 = (var_32 + (-1L));
                        local_sp_0 = var_31;
                        local_sp_2 = var_31;
                        if (var_32 == 0UL) {
                            var_30 = *var_26;
                            continue;
                        }
                        var_33 = *var_15;
                        var_34 = var_33 + (-1L);
                        var_35 = *var_26;
                        var_39 = var_33;
                        if (var_34 != var_35) {
                            loop_state_var = 1U;
                            break;
                        }
                        var_36 = var_33 + (var_35 ^ (-1L));
                        var_37 = *var_28 + 1UL;
                        var_38 = local_sp_2 + (-16L);
                        *(uint64_t *)var_38 = 4208510UL;
                        indirect_placeholder_1(var_37, var_36);
                        var_39 = *var_15;
                        local_sp_0 = var_38;
                        loop_state_var = 1U;
                        break;
                    }
                switch_state_var = 0;
                switch (loop_state_var) {
                  case 0U:
                    {
                        var_45 = *var_11;
                        rax_0 = 0UL;
                        if (var_45 != *var_7) {
                            var_59 = *var_4;
                            var_60 = (uint64_t)*var_5;
                            *(uint64_t *)(local_sp_2 + (-8L)) = 4208655UL;
                            indirect_placeholder_13(0UL, var_59, var_60, var_45);
                            var_61 = *var_8;
                            var_62 = (uint64_t)*var_5;
                            var_63 = *var_4;
                            *(uint64_t *)(local_sp_2 + (-16L)) = 4208688UL;
                            var_64 = indirect_placeholder_13(var_62, var_61, 0UL, var_63);
                            **var_24 = (var_64 + *var_7);
                            switch_state_var = 1;
                            break;
                        }
                        var_46 = var_45 + (-1024L);
                        *var_11 = var_46;
                        var_47 = *var_4;
                        var_48 = (uint64_t)*var_5;
                        *(uint64_t *)(local_sp_2 + (-8L)) = 4208761UL;
                        indirect_placeholder_13(0UL, var_47, var_48, var_46);
                        var_49 = (uint64_t)*var_5;
                        var_50 = local_sp_2 + (-16L);
                        *(uint64_t *)var_50 = 4208789UL;
                        var_51 = indirect_placeholder_7(1024UL, var_49);
                        *var_15 = var_51;
                        local_sp_1 = var_50;
                        if (var_51 == 18446744073709551615UL) {
                            **var_24 = (*var_11 + var_51);
                            var_52 = *var_15;
                            var_29 = var_52;
                            if (var_52 != 0UL) {
                                continue;
                            }
                            switch_state_var = 1;
                            break;
                        }
                        var_53 = *var_4;
                        *(uint64_t *)(local_sp_2 + (-24L)) = 4208820UL;
                        var_54 = indirect_placeholder_53(4UL, var_53);
                        var_55 = var_54.field_0;
                        var_56 = var_54.field_1;
                        var_57 = var_54.field_2;
                        *(uint64_t *)(local_sp_2 + (-32L)) = 4208828UL;
                        indirect_placeholder();
                        var_58 = (uint64_t)*(uint32_t *)var_55;
                        *(uint64_t *)(local_sp_2 + (-40L)) = 4208855UL;
                        indirect_placeholder_52(0UL, 4306959UL, var_55, 0UL, var_58, var_56, var_57);
                        switch_state_var = 1;
                        break;
                    }
                    break;
                  case 1U:
                    {
                        var_40 = *var_8 - (var_39 + *var_11);
                        var_41 = (uint64_t)*var_5;
                        var_42 = *var_4;
                        *(uint64_t *)(local_sp_0 + (-8L)) = 4208560UL;
                        var_43 = indirect_placeholder_13(var_41, var_40, 0UL, var_42);
                        var_44 = *var_24;
                        *var_44 = (var_43 + *var_44);
                        switch_state_var = 1;
                        break;
                    }
                    break;
                }
                if (switch_state_var)
                    break;
            }
    }
    var_65 = *var_4;
    *(uint64_t *)(var_0 + (-1152L)) = 4208249UL;
    var_66 = indirect_placeholder_55(4UL, var_65);
    var_67 = var_66.field_0;
    var_68 = var_66.field_1;
    var_69 = var_66.field_2;
    *(uint64_t *)(var_0 + (-1160L)) = 4208257UL;
    indirect_placeholder();
    var_70 = (uint64_t)*(uint32_t *)var_67;
    *(uint64_t *)(var_0 + (-1168L)) = 4208284UL;
    indirect_placeholder_54(0UL, 4306959UL, var_67, 0UL, var_70, var_68, var_69);
    return rax_0;
}

typedef unsigned char __u_char;
typedef unsigned short __u_short;
typedef unsigned int __u_int;
typedef unsigned long __u_long;
typedef signed char __int8_t;
typedef unsigned char __uint8_t;
typedef short __int16_t;
typedef unsigned short __uint16_t;
typedef int __int32_t;
typedef unsigned int __uint32_t;
typedef long __int64_t;
typedef unsigned long __uint64_t;
typedef __int8_t __int_least8_t;
typedef __uint8_t __uint_least8_t;
typedef __int16_t __int_least16_t;
typedef __uint16_t __uint_least16_t;
typedef __int32_t __int_least32_t;
typedef __uint32_t __uint_least32_t;
typedef __int64_t __int_least64_t;
typedef __uint64_t __uint_least64_t;
typedef long __quad_t;
typedef unsigned long __u_quad_t;
typedef long __intmax_t;
typedef unsigned long __uintmax_t;
typedef unsigned long __dev_t;
typedef unsigned int __uid_t;
typedef unsigned int __gid_t;
typedef unsigned long __ino_t;
typedef unsigned long __ino64_t;
typedef unsigned int __mode_t;
typedef unsigned long __nlink_t;
typedef long __off_t;
typedef long __off64_t;
typedef int __pid_t;
typedef struct {
    int __val[2];
} __fsid_t;
typedef long __clock_t;
typedef unsigned long __rlim_t;
typedef unsigned long __rlim64_t;
typedef unsigned int __id_t;
typedef long __time_t;
typedef unsigned int __useconds_t;
typedef long __suseconds_t;
typedef int __daddr_t;
typedef int __key_t;
typedef int __clockid_t;
typedef void *__timer_t;
typedef long __blksize_t;
typedef long __blkcnt_t;
typedef long __blkcnt64_t;
typedef unsigned long __fsblkcnt_t;
typedef unsigned long __fsblkcnt64_t;
typedef unsigned long __fsfilcnt_t;
typedef unsigned long __fsfilcnt64_t;
typedef long __fsword_t;
typedef long __ssize_t;
typedef long __syscall_slong_t;
typedef unsigned long __syscall_ulong_t;
typedef __off64_t __loff_t;
typedef char *__caddr_t;
typedef long __intptr_t;
typedef unsigned int __socklen_t;
typedef int __sig_atomic_t;
typedef __int8_t int8_t;
typedef __int16_t int16_t;
typedef __int32_t int32_t;
typedef __int64_t int64_t;
typedef __uint8_t uint8_t;
typedef __uint16_t uint16_t;
typedef __uint32_t uint32_t;
typedef __uint64_t uint64_t;
typedef __int_least8_t int_least8_t;
typedef __int_least16_t int_least16_t;
typedef __int_least32_t int_least32_t;
typedef __int_least64_t int_least64_t;
typedef __uint_least8_t uint_least8_t;
typedef __uint_least16_t uint_least16_t;
typedef __uint_least32_t uint_least32_t;
typedef __uint_least64_t uint_least64_t;
typedef signed char int_fast8_t;
typedef long int_fast16_t;
typedef long int_fast32_t;
typedef long int_fast64_t;
typedef unsigned char uint_fast8_t;
typedef unsigned long uint_fast16_t;
typedef unsigned long uint_fast32_t;
typedef unsigned long uint_fast64_t;
typedef long intptr_t;
typedef unsigned long uintptr_t;
typedef __intmax_t intmax_t;
typedef __uintmax_t uintmax_t;
struct indirect_placeholder_11_ret_type;
struct indirect_placeholder_13_ret_type;
struct indirect_placeholder_11_ret_type {
    uint64_t field_0;
    uint64_t field_1;
    uint64_t field_2;
};
struct indirect_placeholder_13_ret_type {
    uint64_t field_0;
    uint64_t field_1;
    uint64_t field_2;
    uint64_t field_3;
};
extern void abort(void);
extern uint64_t revng_init_local_sp(uint64_t param_0);
extern uint64_t init_rbp(void);
extern void indirect_placeholder(void);
extern void indirect_placeholder_10(uint64_t param_0, uint64_t param_1);
extern void indirect_placeholder_9(uint64_t param_0);
extern void indirect_placeholder_4(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint64_t param_3, uint64_t param_4, uint64_t param_5, uint64_t param_6);
extern struct indirect_placeholder_11_ret_type indirect_placeholder_11(uint64_t param_0);
extern void indirect_placeholder_12(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint64_t param_3, uint64_t param_4, uint64_t param_5);
extern struct indirect_placeholder_13_ret_type indirect_placeholder_13(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint64_t param_3, uint64_t param_4);
void bb_main(uint64_t rdi, uint64_t rsi) {
    struct indirect_placeholder_13_ret_type var_15;
    uint64_t var_0;
    uint64_t var_1;
    uint64_t var_2;
    uint32_t *var_3;
    uint64_t var_4;
    uint64_t *var_5;
    unsigned char *var_6;
    unsigned char *var_7;
    uint64_t *var_8;
    uint64_t var_9;
    uint64_t var_10;
    uint32_t *var_11;
    uint64_t local_sp_2;
    uint32_t var_19;
    uint64_t var_20;
    uint32_t var_21;
    uint64_t var_22;
    uint64_t var_23;
    uint64_t var_24;
    uint64_t var_25;
    uint64_t var_39;
    uint64_t var_40;
    uint64_t var_41;
    unsigned char var_26;
    uint64_t var_34;
    struct indirect_placeholder_11_ret_type var_35;
    uint64_t var_36;
    uint64_t var_37;
    uint64_t var_38;
    uint32_t var_27;
    uint32_t var_28;
    uint64_t local_sp_0;
    uint64_t var_29;
    uint64_t var_30;
    uint64_t var_31;
    uint32_t var_32;
    uint64_t var_33;
    uint64_t storemerge;
    uint64_t local_sp_1;
    uint64_t var_17;
    uint64_t var_18;
    uint64_t var_12;
    uint64_t var_13;
    uint64_t var_14;
    uint32_t var_16;
    unsigned int loop_state_var;
    var_0 = revng_init_local_sp(0UL);
    var_1 = init_rbp();
    var_2 = var_0 + (-8L);
    *(uint64_t *)var_2 = var_1;
    var_3 = (uint32_t *)(var_0 + (-44L));
    *var_3 = (uint32_t)rdi;
    var_4 = var_0 + (-56L);
    var_5 = (uint64_t *)var_4;
    *var_5 = rsi;
    var_6 = (unsigned char *)(var_0 + (-9L));
    *var_6 = (unsigned char)'\x00';
    var_7 = (unsigned char *)(var_0 + (-10L));
    *var_7 = (unsigned char)'\x00';
    var_8 = (uint64_t *)(var_0 + (-24L));
    *var_8 = 0UL;
    var_9 = **(uint64_t **)var_4;
    *(uint64_t *)(var_0 + (-64L)) = 4202032UL;
    indirect_placeholder_9(var_9);
    *(uint64_t *)(var_0 + (-72L)) = 4202047UL;
    indirect_placeholder();
    var_10 = var_0 + (-80L);
    *(uint64_t *)var_10 = 4202057UL;
    indirect_placeholder();
    var_11 = (uint32_t *)(var_0 + (-28L));
    storemerge = 0UL;
    local_sp_2 = var_10;
    while (1U)
        {
            var_12 = *var_5;
            var_13 = (uint64_t)*var_3;
            var_14 = local_sp_2 + (-8L);
            *(uint64_t *)var_14 = 4202087UL;
            var_15 = indirect_placeholder_13(4267202UL, 4266304UL, var_13, var_12, 0UL);
            var_16 = (uint32_t)var_15.field_0;
            *var_11 = var_16;
            local_sp_0 = var_14;
            local_sp_1 = var_14;
            local_sp_2 = var_14;
            if (var_16 == 4294967295U) {
                if ((uint64_t)(var_16 + (-97)) != 0UL) {
                    if ((int)var_16 <= (int)97U) {
                        if ((uint64_t)(var_16 + 131U) != 0UL) {
                            if ((uint64_t)(var_16 + 130U) != 0UL) {
                                loop_state_var = 0U;
                                break;
                            }
                            *(uint64_t *)(local_sp_2 + (-16L)) = 4202194UL;
                            indirect_placeholder_10(var_2, 0UL);
                            abort();
                        }
                        var_17 = *(uint64_t *)6374816UL;
                        *(uint64_t *)(local_sp_2 + (-16L)) = 4202246UL;
                        indirect_placeholder_12(0UL, 4266016UL, var_17, 4267193UL, 0UL, 4267208UL);
                        var_18 = local_sp_2 + (-24L);
                        *(uint64_t *)var_18 = 4202256UL;
                        indirect_placeholder();
                        local_sp_1 = var_18;
                        loop_state_var = 0U;
                        break;
                    }
                    if ((uint64_t)(var_16 + (-115)) != 0UL) {
                        if ((uint64_t)(var_16 + (-122)) != 0UL) {
                            loop_state_var = 0U;
                            break;
                        }
                        *var_7 = (unsigned char)'\x01';
                        continue;
                    }
                    *var_8 = *(uint64_t *)6375640UL;
                }
                *var_6 = (unsigned char)'\x01';
                continue;
            }
            var_19 = *(uint32_t *)6374968UL;
            var_20 = (uint64_t)var_19;
            var_21 = *var_3;
            var_22 = (uint64_t)var_21;
            var_23 = var_20 << 32UL;
            var_24 = var_23 + 4294967296UL;
            var_25 = var_22 << 32UL;
            var_27 = var_21;
            var_28 = var_19;
            if ((long)var_24 > (long)var_25) {
                var_39 = var_15.field_3;
                var_40 = var_15.field_2;
                var_41 = var_15.field_1;
                *(uint64_t *)(local_sp_2 + (-16L)) = 4202296UL;
                indirect_placeholder_4(0UL, 4267224UL, var_41, 0UL, 0UL, var_40, var_39);
                *(uint64_t *)(local_sp_2 + (-24L)) = 4202306UL;
                indirect_placeholder_10(var_2, 1UL);
                abort();
            }
            var_26 = *var_6;
            if (!((var_26 != '\x01') && ((long)(var_23 + 8589934592UL) < (long)var_25))) {
                if (var_26 != '\x00') {
                    loop_state_var = 1U;
                    break;
                }
                var_33 = (uint64_t)*var_7;
                if ((uint64_t)((var_19 + 2U) - var_21) != 0UL) {
                    loop_state_var = 2U;
                    break;
                }
                storemerge = *(uint64_t *)(*var_5 + (((uint64_t)var_19 << 3UL) + 8UL));
                loop_state_var = 2U;
                break;
            }
            var_34 = *(uint64_t *)(*var_5 + (((uint64_t)var_19 << 3UL) + 16UL));
            *(uint64_t *)(local_sp_2 + (-16L)) = 4202369UL;
            var_35 = indirect_placeholder_11(var_34);
            var_36 = var_35.field_0;
            var_37 = var_35.field_1;
            var_38 = var_35.field_2;
            *(uint64_t *)(local_sp_2 + (-24L)) = 4202397UL;
            indirect_placeholder_4(0UL, 4267240UL, var_36, 0UL, 0UL, var_37, var_38);
            *(uint64_t *)(local_sp_2 + (-32L)) = 4202407UL;
            indirect_placeholder_10(var_2, 1UL);
            abort();
        }
    switch (loop_state_var) {
      case 0U:
        {
            *(uint64_t *)(local_sp_1 + (-8L)) = 4202266UL;
            indirect_placeholder_10(var_2, 1UL);
            abort();
        }
        break;
      case 2U:
      case 1U:
        {
            switch (loop_state_var) {
              case 1U:
                {
                    while ((long)((uint64_t)var_28 << 32UL) >= (long)((uint64_t)var_27 << 32UL))
                        {
                            var_29 = (uint64_t)*var_7;
                            var_30 = *var_8;
                            var_31 = local_sp_0 + (-8L);
                            *(uint64_t *)var_31 = 4202460UL;
                            indirect_placeholder_10(var_29, var_30);
                            var_32 = *(uint32_t *)6374968UL + 1U;
                            *(uint32_t *)6374968UL = var_32;
                            var_27 = *var_3;
                            var_28 = var_32;
                            local_sp_0 = var_31;
                        }
                }
                break;
              case 2U:
                {
                    *(uint64_t *)(local_sp_2 + (-16L)) = 4202581UL;
                    indirect_placeholder_10(var_33, storemerge);
                }
                break;
            }
            return;
        }
        break;
    }
}

typedef unsigned char __u_char;
typedef unsigned short __u_short;
typedef unsigned int __u_int;
typedef unsigned long __u_long;
typedef signed char __int8_t;
typedef unsigned char __uint8_t;
typedef short __int16_t;
typedef unsigned short __uint16_t;
typedef int __int32_t;
typedef unsigned int __uint32_t;
typedef long __int64_t;
typedef unsigned long __uint64_t;
typedef __int8_t __int_least8_t;
typedef __uint8_t __uint_least8_t;
typedef __int16_t __int_least16_t;
typedef __uint16_t __uint_least16_t;
typedef __int32_t __int_least32_t;
typedef __uint32_t __uint_least32_t;
typedef __int64_t __int_least64_t;
typedef __uint64_t __uint_least64_t;
typedef long __quad_t;
typedef unsigned long __u_quad_t;
typedef long __intmax_t;
typedef unsigned long __uintmax_t;
typedef unsigned long __dev_t;
typedef unsigned int __uid_t;
typedef unsigned int __gid_t;
typedef unsigned long __ino_t;
typedef unsigned long __ino64_t;
typedef unsigned int __mode_t;
typedef unsigned long __nlink_t;
typedef long __off_t;
typedef long __off64_t;
typedef int __pid_t;
typedef struct {
    int __val[2];
} __fsid_t;
typedef long __clock_t;
typedef unsigned long __rlim_t;
typedef unsigned long __rlim64_t;
typedef unsigned int __id_t;
typedef long __time_t;
typedef unsigned int __useconds_t;
typedef long __suseconds_t;
typedef int __daddr_t;
typedef int __key_t;
typedef int __clockid_t;
typedef void *__timer_t;
typedef long __blksize_t;
typedef long __blkcnt_t;
typedef long __blkcnt64_t;
typedef unsigned long __fsblkcnt_t;
typedef unsigned long __fsblkcnt64_t;
typedef unsigned long __fsfilcnt_t;
typedef unsigned long __fsfilcnt64_t;
typedef long __fsword_t;
typedef long __ssize_t;
typedef long __syscall_slong_t;
typedef unsigned long __syscall_ulong_t;
typedef __off64_t __loff_t;
typedef char *__caddr_t;
typedef long __intptr_t;
typedef unsigned int __socklen_t;
typedef int __sig_atomic_t;
typedef __int8_t int8_t;
typedef __int16_t int16_t;
typedef __int32_t int32_t;
typedef __int64_t int64_t;
typedef __uint8_t uint8_t;
typedef __uint16_t uint16_t;
typedef __uint32_t uint32_t;
typedef __uint64_t uint64_t;
typedef __int_least8_t int_least8_t;
typedef __int_least16_t int_least16_t;
typedef __int_least32_t int_least32_t;
typedef __int_least64_t int_least64_t;
typedef __uint_least8_t uint_least8_t;
typedef __uint_least16_t uint_least16_t;
typedef __uint_least32_t uint_least32_t;
typedef __uint_least64_t uint_least64_t;
typedef signed char int_fast8_t;
typedef long int_fast16_t;
typedef long int_fast32_t;
typedef long int_fast64_t;
typedef unsigned char uint_fast8_t;
typedef unsigned long uint_fast16_t;
typedef unsigned long uint_fast32_t;
typedef unsigned long uint_fast64_t;
typedef long intptr_t;
typedef unsigned long uintptr_t;
typedef __intmax_t intmax_t;
typedef __uintmax_t uintmax_t;
struct bb_swallow_file_in_memory_ret_type;
struct indirect_placeholder_59_ret_type;
struct bb_swallow_file_in_memory_ret_type {
    uint64_t field_0;
    uint64_t field_1;
    uint64_t field_2;
    uint64_t field_3;
};
struct indirect_placeholder_59_ret_type {
    uint64_t field_0;
    uint64_t field_1;
    uint64_t field_2;
    uint64_t field_3;
};
extern uint64_t revng_init_local_sp(uint64_t param_0);
extern uint64_t init_rbp(void);
extern uint64_t indirect_placeholder_1(uint64_t param_0, uint64_t param_1);
extern void indirect_placeholder(void);
extern uint64_t indirect_placeholder_2(uint64_t param_0, uint64_t param_1, uint64_t param_2);
extern uint64_t init_rbx(void);
extern uint64_t init_r10(void);
extern uint64_t init_r8(void);
extern uint64_t init_r9(void);
extern uint64_t init_rcx(void);
extern struct indirect_placeholder_59_ret_type indirect_placeholder_59(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint64_t param_3, uint64_t param_4, uint64_t param_5, uint64_t param_6);
struct bb_swallow_file_in_memory_ret_type bb_swallow_file_in_memory(uint64_t rdi, uint64_t rsi) {
    struct indirect_placeholder_59_ret_type var_28;
    uint64_t var_0;
    uint64_t var_1;
    uint64_t var_2;
    uint64_t var_3;
    uint64_t var_4;
    uint64_t var_5;
    uint64_t var_6;
    uint64_t var_7;
    uint64_t var_8;
    uint64_t *var_9;
    uint64_t *var_10;
    uint64_t var_11;
    uint64_t local_sp_1;
    uint64_t **_pre_phi;
    uint64_t local_sp_0;
    uint64_t var_25;
    uint64_t var_29;
    uint64_t storemerge5;
    uint64_t var_26;
    uint64_t var_27;
    uint64_t rcx_0;
    uint64_t r8_0;
    uint64_t r9_0;
    uint64_t r10_0;
    struct bb_swallow_file_in_memory_ret_type mrv;
    struct bb_swallow_file_in_memory_ret_type mrv1;
    struct bb_swallow_file_in_memory_ret_type mrv2;
    struct bb_swallow_file_in_memory_ret_type mrv3;
    uint64_t var_12;
    uint64_t local_sp_2;
    unsigned char storemerge;
    unsigned char *var_13;
    unsigned char var_14;
    uint64_t var_15;
    uint64_t var_16;
    uint64_t var_17;
    uint64_t var_18;
    uint64_t **var_19;
    uint64_t var_20;
    uint64_t var_21;
    uint64_t var_22;
    uint64_t var_23;
    uint64_t **var_24;
    var_0 = revng_init_local_sp(0UL);
    var_1 = init_rbp();
    var_2 = init_rcx();
    var_3 = init_r8();
    var_4 = init_r9();
    var_5 = init_r10();
    var_6 = init_rbx();
    *(uint64_t *)(var_0 + (-8L)) = var_1;
    *(uint64_t *)(var_0 + (-16L)) = var_6;
    var_7 = var_0 + (-56L);
    var_8 = var_0 + (-48L);
    var_9 = (uint64_t *)var_8;
    *var_9 = rdi;
    var_10 = (uint64_t *)var_7;
    *var_10 = rsi;
    var_11 = *var_9;
    local_sp_1 = var_7;
    storemerge5 = 4380946UL;
    rcx_0 = var_2;
    r8_0 = var_3;
    r9_0 = var_4;
    r10_0 = var_5;
    storemerge = (unsigned char)'\x01';
    if (var_11 == 0UL) {
        local_sp_2 = local_sp_1;
    }
    var_13 = (unsigned char *)(var_0 + (-25L));
    var_14 = storemerge & '\x01';
    *var_13 = var_14;
    if (var_14 == '\x00') {
        var_20 = var_0 + (-40L);
        var_21 = *var_9;
        var_22 = local_sp_2 + (-8L);
        *(uint64_t *)var_22 = 4205367UL;
        var_23 = indirect_placeholder_1(var_21, var_20);
        var_24 = (uint64_t **)var_7;
        **var_24 = var_23;
        _pre_phi = var_24;
        local_sp_0 = var_22;
    } else {
        var_15 = *(uint64_t *)6505864UL;
        var_16 = var_0 + (-40L);
        var_17 = local_sp_2 + (-8L);
        *(uint64_t *)var_17 = 4205336UL;
        var_18 = indirect_placeholder_1(var_15, var_16);
        var_19 = (uint64_t **)var_7;
        **var_19 = var_18;
        _pre_phi = var_19;
        local_sp_0 = var_17;
    }
    var_25 = **_pre_phi;
    var_29 = var_25;
    if (var_25 == 0UL) {
        *(uint64_t *)(*var_10 + 8UL) = (var_29 + *(uint64_t *)(var_0 + (-40L)));
        mrv.field_0 = rcx_0;
        mrv1 = mrv;
        mrv1.field_1 = r8_0;
        mrv2 = mrv1;
        mrv2.field_2 = r9_0;
        mrv3 = mrv2;
        mrv3.field_3 = r10_0;
        return mrv3;
    }
    if (*var_13 == '\x00') {
        storemerge5 = *var_9;
    }
    *(uint64_t *)(local_sp_0 + (-8L)) = 4205424UL;
    var_26 = indirect_placeholder_2(storemerge5, 0UL, 3UL);
    *(uint64_t *)(local_sp_0 + (-16L)) = 4205432UL;
    indirect_placeholder();
    var_27 = (uint64_t)*(uint32_t *)var_26;
    *(uint64_t *)(local_sp_0 + (-24L)) = 4205459UL;
    var_28 = indirect_placeholder_59(0UL, 4380948UL, var_26, 1UL, var_27, var_3, var_4);
    var_29 = **_pre_phi;
    rcx_0 = var_28.field_0;
    r8_0 = var_28.field_1;
    r9_0 = var_28.field_2;
    r10_0 = var_28.field_3;
    *(uint64_t *)(*var_10 + 8UL) = (var_29 + *(uint64_t *)(var_0 + (-40L)));
    mrv.field_0 = rcx_0;
    mrv1 = mrv;
    mrv1.field_1 = r8_0;
    mrv2 = mrv1;
    mrv2.field_2 = r9_0;
    mrv3 = mrv2;
    mrv3.field_3 = r10_0;
    return mrv3;
}

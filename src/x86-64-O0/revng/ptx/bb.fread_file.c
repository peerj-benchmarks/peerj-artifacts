typedef unsigned char __u_char;
typedef unsigned short __u_short;
typedef unsigned int __u_int;
typedef unsigned long __u_long;
typedef signed char __int8_t;
typedef unsigned char __uint8_t;
typedef short __int16_t;
typedef unsigned short __uint16_t;
typedef int __int32_t;
typedef unsigned int __uint32_t;
typedef long __int64_t;
typedef unsigned long __uint64_t;
typedef __int8_t __int_least8_t;
typedef __uint8_t __uint_least8_t;
typedef __int16_t __int_least16_t;
typedef __uint16_t __uint_least16_t;
typedef __int32_t __int_least32_t;
typedef __uint32_t __uint_least32_t;
typedef __int64_t __int_least64_t;
typedef __uint64_t __uint_least64_t;
typedef long __quad_t;
typedef unsigned long __u_quad_t;
typedef long __intmax_t;
typedef unsigned long __uintmax_t;
typedef unsigned long __dev_t;
typedef unsigned int __uid_t;
typedef unsigned int __gid_t;
typedef unsigned long __ino_t;
typedef unsigned long __ino64_t;
typedef unsigned int __mode_t;
typedef unsigned long __nlink_t;
typedef long __off_t;
typedef long __off64_t;
typedef int __pid_t;
typedef struct {
    int __val[2];
} __fsid_t;
typedef long __clock_t;
typedef unsigned long __rlim_t;
typedef unsigned long __rlim64_t;
typedef unsigned int __id_t;
typedef long __time_t;
typedef unsigned int __useconds_t;
typedef long __suseconds_t;
typedef int __daddr_t;
typedef int __key_t;
typedef int __clockid_t;
typedef void *__timer_t;
typedef long __blksize_t;
typedef long __blkcnt_t;
typedef long __blkcnt64_t;
typedef unsigned long __fsblkcnt_t;
typedef unsigned long __fsblkcnt64_t;
typedef unsigned long __fsfilcnt_t;
typedef unsigned long __fsfilcnt64_t;
typedef long __fsword_t;
typedef long __ssize_t;
typedef long __syscall_slong_t;
typedef unsigned long __syscall_ulong_t;
typedef __off64_t __loff_t;
typedef char *__caddr_t;
typedef long __intptr_t;
typedef unsigned int __socklen_t;
typedef int __sig_atomic_t;
typedef __int8_t int8_t;
typedef __int16_t int16_t;
typedef __int32_t int32_t;
typedef __int64_t int64_t;
typedef __uint8_t uint8_t;
typedef __uint16_t uint16_t;
typedef __uint32_t uint32_t;
typedef __uint64_t uint64_t;
typedef __int_least8_t int_least8_t;
typedef __int_least16_t int_least16_t;
typedef __int_least32_t int_least32_t;
typedef __int_least64_t int_least64_t;
typedef __uint_least8_t uint_least8_t;
typedef __uint_least16_t uint_least16_t;
typedef __uint_least32_t uint_least32_t;
typedef __uint_least64_t uint_least64_t;
typedef signed char int_fast8_t;
typedef long int_fast16_t;
typedef long int_fast32_t;
typedef long int_fast64_t;
typedef unsigned char uint_fast8_t;
typedef unsigned long uint_fast16_t;
typedef unsigned long uint_fast32_t;
typedef unsigned long uint_fast64_t;
typedef long intptr_t;
typedef unsigned long uintptr_t;
typedef __intmax_t intmax_t;
typedef __uintmax_t uintmax_t;
struct indirect_placeholder_62_ret_type;
struct indirect_placeholder_62_ret_type {
    uint64_t field_0;
    uint64_t field_1;
};
extern uint64_t revng_init_local_sp(uint64_t param_0);
extern uint64_t init_rbp(void);
extern uint64_t indirect_placeholder_1(uint64_t param_0, uint64_t param_1);
extern void indirect_placeholder(void);
extern struct indirect_placeholder_62_ret_type indirect_placeholder_62(uint64_t param_0);
uint64_t bb_fread_file(uint64_t rdi, uint64_t rsi) {
    uint64_t var_0;
    uint64_t var_1;
    uint64_t *var_2;
    uint64_t var_3;
    uint64_t var_4;
    uint64_t *var_5;
    uint64_t *var_6;
    uint64_t var_7;
    uint64_t local_sp_2;
    uint64_t var_41;
    uint32_t var_42;
    uint32_t *var_43;
    uint32_t *_pre_phi84;
    uint64_t var_20;
    uint64_t var_37;
    uint64_t var_28;
    uint64_t var_29;
    uint64_t var_30;
    uint64_t var_31;
    uint64_t var_32;
    uint64_t var_33;
    uint32_t var_24;
    uint32_t *var_25;
    uint64_t var_26;
    uint64_t var_27;
    uint64_t var_34;
    uint64_t local_sp_0;
    uint32_t *var_44;
    uint32_t *var_45;
    uint64_t var_35;
    uint64_t var_36;
    uint64_t var_38;
    uint64_t var_39;
    uint64_t var_40;
    uint64_t *var_16;
    uint64_t *var_17;
    uint64_t *var_18;
    uint64_t *var_19;
    uint64_t local_sp_1;
    uint64_t var_21;
    uint64_t var_22;
    uint64_t var_23;
    uint64_t rax_0;
    uint64_t var_10;
    uint64_t var_11;
    uint64_t var_8;
    uint64_t var_9;
    uint64_t var_12;
    uint64_t var_13;
    struct indirect_placeholder_62_ret_type var_14;
    uint64_t var_15;
    unsigned int loop_state_var;
    var_0 = revng_init_local_sp(0UL);
    var_1 = init_rbp();
    *(uint64_t *)(var_0 + (-8L)) = var_1;
    var_2 = (uint64_t *)(var_0 + (-240L));
    *var_2 = rdi;
    var_3 = var_0 + (-248L);
    *(uint64_t *)var_3 = rsi;
    var_4 = var_0 + (-16L);
    var_5 = (uint64_t *)var_4;
    *var_5 = 0UL;
    var_6 = (uint64_t *)(var_0 + (-24L));
    *var_6 = 1024UL;
    *(uint64_t *)(var_0 + (-256L)) = 4226717UL;
    indirect_placeholder();
    var_7 = var_0 + (-264L);
    *(uint64_t *)var_7 = 4226736UL;
    indirect_placeholder();
    var_37 = 18446744073709551615UL;
    var_20 = 0UL;
    rax_0 = 0UL;
    local_sp_2 = var_7;
    var_8 = *var_2;
    var_9 = var_0 + (-272L);
    *(uint64_t *)var_9 = 4226773UL;
    indirect_placeholder();
    *(uint64_t *)(var_0 + (-48L)) = var_8;
    local_sp_2 = var_9;
    var_10 = *(uint64_t *)(var_0 + (-184L));
    if ((int)((uint32_t)var_0 + (-232)) >= (int)0U & (uint32_t)((uint16_t)*(uint32_t *)(var_0 + (-208L)) & (unsigned short)61440U) != 32768U & (long)var_8 >= (long)0UL & (long)var_10 <= (long)var_8) {
        var_11 = var_10 - var_8;
        *(uint64_t *)(var_0 + (-56L)) = var_11;
        if (var_11 != 18446744073709551615UL) {
            *(uint64_t *)(var_0 + (-280L)) = 4226824UL;
            indirect_placeholder();
            *(uint32_t *)18446744073709551615UL = 12U;
            return rax_0;
        }
        *var_6 = (var_11 + 1UL);
    }
    var_12 = *var_6;
    var_13 = local_sp_2 + (-8L);
    *(uint64_t *)var_13 = 4226864UL;
    var_14 = indirect_placeholder_62(var_12);
    var_15 = var_14.field_0;
    *var_5 = var_15;
    local_sp_1 = var_13;
    if (var_15 == 0UL) {
        return rax_0;
    }
    var_16 = (uint64_t *)(var_0 + (-32L));
    *var_16 = 0UL;
    var_17 = (uint64_t *)(var_0 + (-64L));
    var_18 = (uint64_t *)(var_0 + (-72L));
    var_19 = (uint64_t *)(var_0 + (-80L));
    while (1U)
        {
            var_21 = *var_6 - var_20;
            *var_17 = var_21;
            var_22 = local_sp_1 + (-8L);
            *(uint64_t *)var_22 = 4226944UL;
            indirect_placeholder();
            *var_18 = var_21;
            *var_16 = (*var_16 + var_21);
            var_23 = *var_18;
            local_sp_0 = var_22;
            if (var_23 != *var_17) {
                *(uint64_t *)(local_sp_1 + (-16L)) = 4226971UL;
                indirect_placeholder();
                var_24 = *(uint32_t *)var_23;
                var_25 = (uint32_t *)(var_0 + (-36L));
                *var_25 = var_24;
                var_26 = *var_2;
                var_27 = local_sp_1 + (-24L);
                *(uint64_t *)var_27 = 4226991UL;
                indirect_placeholder();
                _pre_phi84 = var_25;
                local_sp_0 = var_27;
                if ((uint64_t)(uint32_t)var_26 != 0UL) {
                    loop_state_var = 1U;
                    break;
                }
                var_28 = *var_6 + (-1L);
                var_29 = *var_16;
                if (var_28 <= var_29) {
                    loop_state_var = 0U;
                    break;
                }
                var_30 = var_29 + 1UL;
                var_31 = *var_5;
                *(uint64_t *)(local_sp_1 + (-32L)) = 4227037UL;
                var_32 = indirect_placeholder_1(var_31, var_30);
                *(uint64_t *)(var_0 + (-88L)) = var_32;
                if (var_32 != 0UL) {
                    loop_state_var = 0U;
                    break;
                }
                *var_5 = var_32;
                loop_state_var = 0U;
                break;
            }
            var_34 = *var_6;
            if (var_34 != 18446744073709551615UL) {
                var_44 = (uint32_t *)(var_0 + (-36L));
                *var_44 = 12U;
                _pre_phi84 = var_44;
                loop_state_var = 1U;
                break;
            }
            var_35 = var_34 >> 1UL;
            if (var_34 < (var_35 ^ (-1L))) {
                var_36 = var_34 + var_35;
                *var_6 = var_36;
                var_37 = var_36;
            } else {
                *var_6 = 18446744073709551615UL;
            }
            var_38 = *var_5;
            var_39 = local_sp_1 + (-16L);
            *(uint64_t *)var_39 = 4227165UL;
            var_40 = indirect_placeholder_1(var_38, var_37);
            *var_19 = var_40;
            local_sp_1 = var_39;
            if (var_40 == 0UL) {
                *var_5 = var_40;
                var_20 = *var_16;
                continue;
            }
            var_41 = local_sp_1 + (-24L);
            *(uint64_t *)var_41 = 4227181UL;
            indirect_placeholder();
            var_42 = *(volatile uint32_t *)(uint32_t *)0UL;
            var_43 = (uint32_t *)(var_0 + (-36L));
            *var_43 = var_42;
            _pre_phi84 = var_43;
            local_sp_0 = var_41;
            loop_state_var = 1U;
            break;
        }
    switch (loop_state_var) {
      case 0U:
        {
            *(unsigned char *)(*var_16 + *var_5) = (unsigned char)'\x00';
            **(uint64_t **)var_3 = *var_16;
            var_33 = *var_5;
            rax_0 = var_33;
        }
        break;
      case 1U:
        {
            var_45 = *(uint32_t **)var_4;
            *(uint64_t *)(local_sp_0 + (-8L)) = 4227213UL;
            indirect_placeholder();
            *(uint64_t *)(local_sp_0 + (-16L)) = 4227218UL;
            indirect_placeholder();
            *var_45 = *_pre_phi84;
        }
        break;
    }
    return rax_0;
}

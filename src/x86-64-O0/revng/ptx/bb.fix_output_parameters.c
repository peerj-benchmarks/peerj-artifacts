typedef unsigned char __u_char;
typedef unsigned short __u_short;
typedef unsigned int __u_int;
typedef unsigned long __u_long;
typedef signed char __int8_t;
typedef unsigned char __uint8_t;
typedef short __int16_t;
typedef unsigned short __uint16_t;
typedef int __int32_t;
typedef unsigned int __uint32_t;
typedef long __int64_t;
typedef unsigned long __uint64_t;
typedef __int8_t __int_least8_t;
typedef __uint8_t __uint_least8_t;
typedef __int16_t __int_least16_t;
typedef __uint16_t __uint_least16_t;
typedef __int32_t __int_least32_t;
typedef __uint32_t __uint_least32_t;
typedef __int64_t __int_least64_t;
typedef __uint64_t __uint_least64_t;
typedef long __quad_t;
typedef unsigned long __u_quad_t;
typedef long __intmax_t;
typedef unsigned long __uintmax_t;
typedef unsigned long __dev_t;
typedef unsigned int __uid_t;
typedef unsigned int __gid_t;
typedef unsigned long __ino_t;
typedef unsigned long __ino64_t;
typedef unsigned int __mode_t;
typedef unsigned long __nlink_t;
typedef long __off_t;
typedef long __off64_t;
typedef int __pid_t;
typedef struct {
    int __val[2];
} __fsid_t;
typedef long __clock_t;
typedef unsigned long __rlim_t;
typedef unsigned long __rlim64_t;
typedef unsigned int __id_t;
typedef long __time_t;
typedef unsigned int __useconds_t;
typedef long __suseconds_t;
typedef int __daddr_t;
typedef int __key_t;
typedef int __clockid_t;
typedef void *__timer_t;
typedef long __blksize_t;
typedef long __blkcnt_t;
typedef long __blkcnt64_t;
typedef unsigned long __fsblkcnt_t;
typedef unsigned long __fsblkcnt64_t;
typedef unsigned long __fsfilcnt_t;
typedef unsigned long __fsfilcnt64_t;
typedef long __fsword_t;
typedef long __ssize_t;
typedef long __syscall_slong_t;
typedef unsigned long __syscall_ulong_t;
typedef __off64_t __loff_t;
typedef char *__caddr_t;
typedef long __intptr_t;
typedef unsigned int __socklen_t;
typedef int __sig_atomic_t;
typedef __int8_t int8_t;
typedef __int16_t int16_t;
typedef __int32_t int32_t;
typedef __int64_t int64_t;
typedef __uint8_t uint8_t;
typedef __uint16_t uint16_t;
typedef __uint32_t uint32_t;
typedef __uint64_t uint64_t;
typedef __int_least8_t int_least8_t;
typedef __int_least16_t int_least16_t;
typedef __int_least32_t int_least32_t;
typedef __int_least64_t int_least64_t;
typedef __uint_least8_t uint_least8_t;
typedef __uint_least16_t uint_least16_t;
typedef __uint_least32_t uint_least32_t;
typedef __uint_least64_t uint_least64_t;
typedef signed char int_fast8_t;
typedef long int_fast16_t;
typedef long int_fast32_t;
typedef long int_fast64_t;
typedef unsigned char uint_fast8_t;
typedef unsigned long uint_fast16_t;
typedef unsigned long uint_fast32_t;
typedef unsigned long uint_fast64_t;
typedef long intptr_t;
typedef unsigned long uintptr_t;
typedef __intmax_t intmax_t;
typedef __uintmax_t uintmax_t;
typedef _Bool bool;
extern uint64_t revng_init_local_sp(uint64_t param_0);
extern uint64_t init_rbp(void);
extern uint64_t init_cc_src2(void);
extern uint64_t helper_cc_compute_c_wrapper(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint32_t param_3);
extern void indirect_placeholder(void);
extern uint64_t indirect_placeholder_10(uint64_t param_0);
void bb_fix_output_parameters(void) {
    uint64_t var_0;
    uint64_t var_1;
    uint64_t var_2;
    uint64_t var_3;
    uint64_t local_sp_258;
    uint64_t var_16;
    uint64_t var_17;
    uint64_t var_18;
    uint64_t local_sp_0;
    uint64_t var_7;
    uint64_t var_19;
    uint64_t *var_4;
    uint64_t *var_5;
    uint64_t *var_6;
    uint64_t local_sp_1;
    uint64_t var_12;
    uint64_t var_13;
    uint64_t var_14;
    uint64_t var_15;
    uint64_t var_8;
    uint64_t var_9;
    uint64_t var_10;
    uint64_t var_11;
    uint64_t local_sp_257;
    uint64_t local_sp_256_ph;
    uint64_t var_20;
    uint64_t var_21;
    uint64_t local_sp_256;
    uint64_t var_22;
    uint64_t var_23;
    uint64_t var_24;
    uint64_t var_25;
    uint64_t var_26;
    uint64_t local_sp_3;
    bool var_27;
    uint64_t var_28;
    uint64_t var_29;
    uint64_t storemerge;
    uint32_t *var_30;
    uint32_t var_31;
    uint64_t local_sp_4;
    uint32_t var_36;
    uint64_t var_37;
    uint64_t var_38;
    uint64_t *var_39;
    uint64_t local_sp_5;
    unsigned char var_40;
    uint32_t var_44;
    uint32_t var_45;
    uint64_t var_41;
    uint64_t var_42;
    uint64_t var_43;
    uint64_t var_32;
    uint64_t var_33;
    uint64_t var_34;
    uint32_t var_35;
    var_0 = revng_init_local_sp(0UL);
    var_1 = init_rbp();
    var_2 = init_cc_src2();
    *(uint64_t *)(var_0 + (-8L)) = var_1;
    var_3 = var_0 + (-72L);
    local_sp_258 = var_3;
    var_18 = 0UL;
    var_7 = 0UL;
    local_sp_1 = var_3;
    var_31 = 0U;
    var_44 = 128U;
    if (*(unsigned char *)6506048UL == '\x00') {
        *(uint64_t *)6507464UL = 0UL;
        var_4 = (uint64_t *)(var_0 + (-16L));
        *var_4 = 0UL;
        var_5 = (uint64_t *)(var_0 + (-24L));
        var_6 = (uint64_t *)(var_0 + (-32L));
        while (var_7 >= (uint64_t)*(uint32_t *)6507528UL)
            {
                var_12 = *(uint64_t *)(*(uint64_t *)6507552UL + (var_7 << 3UL)) + 1UL;
                *var_5 = var_12;
                var_13 = *var_4;
                if (var_13 == 0UL) {
                    *var_5 = (var_12 - *(uint64_t *)(*(uint64_t *)6507552UL + ((var_13 << 3UL) + (-8L))));
                }
                var_14 = local_sp_1 + (-8L);
                *(uint64_t *)var_14 = 4209383UL;
                indirect_placeholder();
                *var_6 = 0UL;
                var_15 = *(uint64_t *)(*(uint64_t *)6507544UL + (*var_4 << 3UL));
                local_sp_0 = var_14;
                if (var_15 == 0UL) {
                    var_16 = local_sp_1 + (-16L);
                    *(uint64_t *)var_16 = 4209444UL;
                    indirect_placeholder();
                    var_17 = *var_6 + var_15;
                    *var_6 = var_17;
                    var_18 = var_17;
                    local_sp_0 = var_16;
                }
                local_sp_1 = local_sp_0;
                if ((long)var_18 > (long)*(uint64_t *)6507464UL) {
                    *(uint64_t *)6507464UL = var_18;
                }
                var_19 = *var_4 + 1UL;
                *var_4 = var_19;
                var_7 = var_19;
            }
        var_8 = *(uint64_t *)6507464UL;
        *(uint64_t *)6507464UL = (var_8 + 1UL);
        var_9 = var_8 + 2UL;
        var_10 = local_sp_1 + (-8L);
        *(uint64_t *)var_10 = 4209542UL;
        var_11 = indirect_placeholder_10(var_9);
        *(uint64_t *)6508064UL = var_11;
        local_sp_258 = var_10;
        local_sp_257 = var_10;
        if (*(unsigned char *)6506048UL == '\x00') {
            local_sp_257 = local_sp_258;
            local_sp_256_ph = local_sp_258;
            if (*(unsigned char *)6506049UL == '\x00') {
                var_21 = *(uint64_t *)6505640UL;
                local_sp_256 = local_sp_256_ph;
            } else {
                local_sp_256_ph = local_sp_257;
                local_sp_256 = local_sp_257;
                if (*(unsigned char *)6506050UL == '\x01') {
                    var_21 = *(uint64_t *)6505640UL;
                    local_sp_256 = local_sp_256_ph;
                } else {
                    var_20 = *(uint64_t *)6505640UL - (*(uint64_t *)6505648UL + *(uint64_t *)6507464UL);
                    *(uint64_t *)6505640UL = var_20;
                    var_21 = var_20;
                }
            }
        } else {
            local_sp_256_ph = local_sp_257;
            local_sp_256 = local_sp_257;
            if (*(unsigned char *)6506050UL == '\x01') {
                var_20 = *(uint64_t *)6505640UL - (*(uint64_t *)6505648UL + *(uint64_t *)6507464UL);
                *(uint64_t *)6505640UL = var_20;
                var_21 = var_20;
            } else {
                var_21 = *(uint64_t *)6505640UL;
                local_sp_256 = local_sp_256_ph;
            }
        }
    } else {
        local_sp_257 = local_sp_258;
        local_sp_256_ph = local_sp_258;
        if (*(unsigned char *)6506049UL == '\x00') {
            var_21 = *(uint64_t *)6505640UL;
            local_sp_256 = local_sp_256_ph;
        } else {
            local_sp_256_ph = local_sp_257;
            local_sp_256 = local_sp_257;
            if (*(unsigned char *)6506050UL == '\x01') {
                var_20 = *(uint64_t *)6505640UL - (*(uint64_t *)6505648UL + *(uint64_t *)6507464UL);
                *(uint64_t *)6505640UL = var_20;
                var_21 = var_20;
            } else {
                var_21 = *(uint64_t *)6505640UL;
                local_sp_256 = local_sp_256_ph;
            }
        }
    }
    var_22 = var_21;
    local_sp_3 = local_sp_256;
    if ((long)var_21 > (long)18446744073709551615UL) {
        *(uint64_t *)6505640UL = 0UL;
        var_22 = 0UL;
    }
    var_23 = (uint64_t)((long)(var_22 + (var_22 >> 63UL)) >> (long)1UL);
    *(uint64_t *)6507904UL = var_23;
    *(uint64_t *)6507912UL = (var_23 - *(uint64_t *)6505648UL);
    *(uint64_t *)6507920UL = *(uint64_t *)6507904UL;
    var_24 = *(uint64_t *)6505656UL;
    var_26 = var_24;
    if (var_24 == 0UL) {
        *(uint64_t *)6505656UL = 0UL;
        var_26 = *(uint64_t *)6507928UL;
    } else {
        var_25 = local_sp_256 + (-8L);
        *(uint64_t *)var_25 = 4209751UL;
        indirect_placeholder();
        *(uint64_t *)6507928UL = var_24;
        local_sp_3 = var_25;
    }
    var_27 = (*(unsigned char *)6505632UL == '\x00');
    var_28 = var_26 << 1UL;
    local_sp_4 = local_sp_3;
    if (var_27) {
        storemerge = *(uint64_t *)6507920UL + (var_28 ^ (-1L));
    } else {
        var_29 = *(uint64_t *)6507912UL - var_28;
        *(uint64_t *)6507912UL = (((long)var_29 > (long)18446744073709551615UL) ? var_29 : 0UL);
        storemerge = *(uint64_t *)6507920UL - (*(uint64_t *)6507928UL << 1UL);
    }
    *(uint64_t *)6507920UL = storemerge;
    var_30 = (uint32_t *)(var_0 + (-36L));
    *var_30 = 0U;
    local_sp_5 = local_sp_4;
    while ((int)var_31 <= (int)255U)
        {
            var_32 = (uint64_t)var_31;
            var_33 = local_sp_4 + (-8L);
            *(uint64_t *)var_33 = 4209935UL;
            var_34 = indirect_placeholder_10(var_32);
            *(unsigned char *)((uint64_t)*var_30 + 6507648UL) = ((uint64_t)(uint32_t)var_34 != 0UL);
            var_35 = *var_30 + 1U;
            *var_30 = var_35;
            var_31 = var_35;
            local_sp_4 = var_33;
            local_sp_5 = local_sp_4;
        }
    *(unsigned char *)6507660UL = (unsigned char)'\x01';
    var_36 = *(uint32_t *)6506052UL;
    if ((uint64_t)(var_36 + (-2)) != 0UL) {
        var_37 = helper_cc_compute_c_wrapper((uint64_t)var_36 + (-2L), 2UL, var_2, 16U);
        if (var_37 != 0UL & (uint64_t)(var_36 + (-3)) != 0UL) {
            var_38 = var_0 + (-48L);
            var_39 = (uint64_t *)var_38;
            *var_39 = 4381196UL;
            var_40 = **(unsigned char **)var_38;
            while (var_40 != '\x00')
                {
                    var_41 = (uint64_t)(uint32_t)(uint64_t)var_40;
                    var_42 = local_sp_5 + (-8L);
                    *(uint64_t *)var_42 = 4210036UL;
                    var_43 = indirect_placeholder_10(var_41);
                    *(unsigned char *)((uint64_t)(unsigned char)var_43 + 6507648UL) = (unsigned char)'\x01';
                    *var_39 = (*var_39 + 1UL);
                    local_sp_5 = var_42;
                    var_40 = **(unsigned char **)var_38;
                }
            *var_30 = 128U;
            while ((int)var_44 <= (int)255U)
                {
                    *(unsigned char *)((uint64_t)var_44 + 6507648UL) = (*(unsigned char *)(uint64_t)((uint32_t)(unsigned char)var_44 | 4384256U) != '\x00');
                    var_45 = *var_30 + 1U;
                    *var_30 = var_45;
                    var_44 = var_45;
                }
        }
    }
    *(unsigned char *)6507682UL = (unsigned char)'\x01';
    return;
}

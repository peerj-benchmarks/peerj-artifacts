typedef unsigned char __u_char;
typedef unsigned short __u_short;
typedef unsigned int __u_int;
typedef unsigned long __u_long;
typedef signed char __int8_t;
typedef unsigned char __uint8_t;
typedef short __int16_t;
typedef unsigned short __uint16_t;
typedef int __int32_t;
typedef unsigned int __uint32_t;
typedef long __int64_t;
typedef unsigned long __uint64_t;
typedef __int8_t __int_least8_t;
typedef __uint8_t __uint_least8_t;
typedef __int16_t __int_least16_t;
typedef __uint16_t __uint_least16_t;
typedef __int32_t __int_least32_t;
typedef __uint32_t __uint_least32_t;
typedef __int64_t __int_least64_t;
typedef __uint64_t __uint_least64_t;
typedef long __quad_t;
typedef unsigned long __u_quad_t;
typedef long __intmax_t;
typedef unsigned long __uintmax_t;
typedef unsigned long __dev_t;
typedef unsigned int __uid_t;
typedef unsigned int __gid_t;
typedef unsigned long __ino_t;
typedef unsigned long __ino64_t;
typedef unsigned int __mode_t;
typedef unsigned long __nlink_t;
typedef long __off_t;
typedef long __off64_t;
typedef int __pid_t;
typedef struct {
    int __val[2];
} __fsid_t;
typedef long __clock_t;
typedef unsigned long __rlim_t;
typedef unsigned long __rlim64_t;
typedef unsigned int __id_t;
typedef long __time_t;
typedef unsigned int __useconds_t;
typedef long __suseconds_t;
typedef int __daddr_t;
typedef int __key_t;
typedef int __clockid_t;
typedef void *__timer_t;
typedef long __blksize_t;
typedef long __blkcnt_t;
typedef long __blkcnt64_t;
typedef unsigned long __fsblkcnt_t;
typedef unsigned long __fsblkcnt64_t;
typedef unsigned long __fsfilcnt_t;
typedef unsigned long __fsfilcnt64_t;
typedef long __fsword_t;
typedef long __ssize_t;
typedef long __syscall_slong_t;
typedef unsigned long __syscall_ulong_t;
typedef __off64_t __loff_t;
typedef char *__caddr_t;
typedef long __intptr_t;
typedef unsigned int __socklen_t;
typedef int __sig_atomic_t;
typedef __int8_t int8_t;
typedef __int16_t int16_t;
typedef __int32_t int32_t;
typedef __int64_t int64_t;
typedef __uint8_t uint8_t;
typedef __uint16_t uint16_t;
typedef __uint32_t uint32_t;
typedef __uint64_t uint64_t;
typedef __int_least8_t int_least8_t;
typedef __int_least16_t int_least16_t;
typedef __int_least32_t int_least32_t;
typedef __int_least64_t int_least64_t;
typedef __uint_least8_t uint_least8_t;
typedef __uint_least16_t uint_least16_t;
typedef __uint_least32_t uint_least32_t;
typedef __uint_least64_t uint_least64_t;
typedef signed char int_fast8_t;
typedef long int_fast16_t;
typedef long int_fast32_t;
typedef long int_fast64_t;
typedef unsigned char uint_fast8_t;
typedef unsigned long uint_fast16_t;
typedef unsigned long uint_fast32_t;
typedef unsigned long uint_fast64_t;
typedef long intptr_t;
typedef unsigned long uintptr_t;
typedef __intmax_t intmax_t;
typedef __uintmax_t uintmax_t;
extern void abort(void);
extern uint64_t revng_init_local_sp(uint64_t param_0);
extern uint64_t init_rbp(void);
extern uint64_t init_cc_src2(void);
extern uint64_t helper_cc_compute_c_wrapper(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint32_t param_3);
extern void indirect_placeholder(void);
extern uint64_t indirect_placeholder_2(uint64_t param_0, uint64_t param_1, uint64_t param_2);
extern void indirect_placeholder_14(uint64_t param_0);
extern uint64_t indirect_placeholder_10(uint64_t param_0);
extern uint64_t helper_cc_compute_all_wrapper(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint32_t param_3);
extern void indirect_placeholder_25(uint64_t param_0, uint64_t param_1, uint64_t param_2);
void bb_define_all_fields(uint64_t rdi) {
    uint64_t storemerge28;
    uint64_t local_sp_23;
    uint64_t var_163;
    uint64_t var_164;
    uint64_t var_165;
    uint64_t var_166;
    uint64_t var_167;
    uint64_t *var_168;
    uint64_t local_sp_24;
    uint64_t var_170;
    uint64_t var_171;
    uint64_t var_172;
    unsigned char storemerge30;
    uint64_t local_sp_25;
    uint64_t var_188;
    uint64_t local_sp_26;
    uint64_t var_189;
    uint64_t local_sp_24_be;
    uint64_t var_0;
    uint64_t var_1;
    uint64_t var_2;
    uint64_t var_3;
    uint64_t var_4;
    uint64_t var_5;
    uint64_t *var_6;
    uint64_t var_7;
    uint64_t var_8;
    uint64_t *var_9;
    uint64_t var_10;
    uint64_t *var_11;
    uint64_t var_12;
    uint64_t *var_13;
    uint64_t var_14;
    uint64_t *var_15;
    uint64_t var_16;
    uint64_t var_17;
    uint64_t *var_18;
    uint64_t *var_19;
    uint64_t var_20;
    uint64_t local_sp_27;
    uint64_t var_194;
    uint64_t var_186;
    uint64_t var_153;
    uint64_t var_169;
    uint64_t var_185;
    uint64_t var_180;
    uint64_t var_179;
    uint64_t local_sp_0;
    uint64_t var_181;
    uint64_t var_182;
    uint64_t var_183;
    uint64_t var_184;
    uint64_t local_sp_22;
    uint64_t var_159;
    uint64_t var_146;
    uint64_t local_sp_19_be;
    uint64_t var_126;
    uint64_t var_145;
    uint64_t var_140;
    uint64_t var_139;
    uint64_t local_sp_1;
    uint64_t var_141;
    uint64_t var_142;
    uint64_t var_143;
    uint64_t var_144;
    uint64_t storemerge37;
    uint64_t local_sp_17;
    uint64_t var_114;
    uint64_t var_106;
    uint64_t var_104;
    uint64_t var_98;
    uint64_t var_43;
    uint64_t local_sp_11_be;
    uint64_t local_sp_11;
    uint64_t var_96;
    uint64_t var_91;
    uint64_t var_90;
    uint64_t local_sp_2;
    uint64_t var_92;
    uint64_t var_93;
    uint64_t var_94;
    uint64_t var_95;
    uint64_t local_sp_9;
    uint64_t var_74;
    uint64_t storemerge23;
    uint64_t var_70;
    uint64_t local_sp_3;
    uint64_t *_pre_phi293;
    uint64_t var_71;
    uint64_t var_72;
    uint64_t var_73;
    uint64_t storemerge39;
    uint64_t var_53;
    uint64_t var_46;
    uint64_t local_sp_5_be;
    uint64_t var_39;
    uint64_t var_34;
    uint64_t var_33;
    uint64_t local_sp_4;
    uint64_t var_35;
    uint64_t var_36;
    uint64_t var_37;
    uint64_t var_38;
    uint64_t storemerge40;
    uint64_t local_sp_5;
    uint64_t var_21;
    uint64_t var_22;
    uint64_t var_23;
    uint64_t var_24;
    uint64_t var_25;
    uint64_t var_26;
    uint64_t var_27;
    uint64_t var_28;
    unsigned char **var_29;
    uint64_t var_30;
    uint64_t var_31;
    uint64_t var_32;
    uint64_t var_40;
    uint64_t var_41;
    uint64_t var_42;
    uint64_t var_162;
    uint64_t var_44;
    uint64_t var_45;
    unsigned char storemerge;
    uint64_t var_187;
    uint64_t local_sp_6;
    uint64_t var_47;
    uint64_t var_48;
    uint64_t var_49;
    uint64_t var_50;
    uint64_t var_51;
    uint64_t var_52;
    uint64_t var_54;
    uint64_t local_sp_7;
    uint64_t var_55;
    uint64_t var_56;
    uint64_t var_57;
    uint64_t local_sp_8;
    uint64_t var_60;
    uint64_t var_61;
    uint64_t *var_62;
    uint64_t var_63;
    uint64_t var_64;
    uint64_t var_65;
    unsigned char **var_66;
    uint64_t var_67;
    uint64_t var_68;
    uint64_t var_69;
    uint64_t *var_59;
    uint64_t var_58;
    uint64_t local_sp_10;
    uint64_t var_75;
    uint64_t var_76;
    uint64_t var_77;
    uint64_t var_78;
    uint64_t var_79;
    uint64_t *var_80;
    uint64_t var_81;
    uint64_t var_82;
    uint64_t var_83;
    uint64_t local_sp_14;
    uint64_t var_97;
    uint64_t local_sp_12;
    uint64_t var_99;
    uint64_t var_100;
    uint64_t var_101;
    uint64_t var_102;
    uint64_t var_103;
    uint64_t var_105;
    uint64_t local_sp_13;
    unsigned char storemerge25;
    uint64_t local_sp_15;
    uint64_t var_107;
    uint64_t var_108;
    uint64_t local_sp_16;
    uint64_t var_109;
    uint64_t var_110;
    uint64_t var_111;
    uint64_t var_112;
    uint64_t var_113;
    uint64_t var_115;
    uint64_t *var_116;
    uint64_t storemerge33;
    uint64_t var_117;
    uint64_t var_118;
    uint64_t local_sp_18;
    uint64_t var_119;
    uint64_t var_120;
    uint64_t var_121;
    uint64_t var_122;
    uint64_t var_123;
    uint64_t var_124;
    uint64_t *var_125;
    uint64_t local_sp_19;
    uint64_t var_127;
    uint64_t var_128;
    uint64_t _pre_phi295;
    uint64_t _pre284;
    uint64_t var_129;
    uint64_t var_130;
    uint64_t var_131;
    uint64_t var_132;
    uint64_t var_133;
    uint64_t var_134;
    unsigned char **var_135;
    uint64_t var_136;
    uint64_t var_137;
    uint64_t var_138;
    uint64_t var_147;
    uint64_t var_149;
    uint64_t var_148;
    unsigned char storemerge35;
    uint64_t var_150;
    uint64_t var_151;
    uint64_t var_152;
    uint64_t local_sp_20;
    uint64_t local_sp_21;
    uint64_t var_154;
    uint64_t var_155;
    uint64_t var_156;
    uint64_t var_157;
    uint64_t var_158;
    uint64_t var_160;
    uint64_t *var_161;
    uint64_t var_190;
    uint64_t var_191;
    uint64_t var_192;
    uint64_t var_193;
    uint64_t var_173;
    uint64_t var_174;
    uint64_t var_175;
    uint64_t var_176;
    uint64_t var_177;
    uint64_t var_178;
    uint64_t var_195;
    uint64_t var_196;
    uint64_t *var_197;
    uint64_t var_198;
    uint64_t var_199;
    uint64_t *var_200;
    uint64_t var_201;
    uint64_t storemerge26;
    uint64_t var_202;
    uint64_t var_203;
    uint64_t var_204;
    uint64_t var_205;
    uint64_t var_206;
    uint64_t var_207;
    uint64_t var_208;
    uint64_t var_84;
    uint64_t var_85;
    uint64_t var_86;
    uint64_t var_87;
    uint64_t var_88;
    uint64_t var_89;
    unsigned int loop_state_var;
    var_0 = revng_init_local_sp(0UL);
    var_1 = init_rbp();
    var_2 = init_cc_src2();
    var_3 = var_0 + (-8L);
    *(uint64_t *)var_3 = var_1;
    var_4 = var_0 + (-152L);
    var_5 = var_0 + (-144L);
    var_6 = (uint64_t *)var_5;
    *var_6 = rdi;
    var_7 = **(uint64_t **)var_5;
    *(uint64_t *)6508000UL = var_7;
    *(uint64_t *)6508008UL = (*(uint64_t *)(*var_6 + 8UL) + var_7);
    var_8 = *(uint64_t *)(*var_6 + 16UL) + *(uint64_t *)6508000UL;
    var_9 = (uint64_t *)(var_0 + (-48L));
    *var_9 = var_8;
    var_10 = *(uint64_t *)(*var_6 + 24UL) + *(uint64_t *)6508000UL;
    var_11 = (uint64_t *)(var_0 + (-56L));
    *var_11 = var_10;
    var_12 = *(uint64_t *)(((uint64_t)*(uint32_t *)(*var_6 + 40UL) << 4UL) + *(uint64_t *)6507560UL);
    var_13 = (uint64_t *)(var_0 + (-64L));
    *var_13 = var_12;
    var_14 = *(uint64_t *)((((uint64_t)*(uint32_t *)(*var_6 + 40UL) << 4UL) + *(uint64_t *)6507560UL) + 8UL);
    var_15 = (uint64_t *)(var_0 + (-72L));
    *var_15 = var_14;
    var_16 = *(uint64_t *)6508008UL;
    var_17 = var_0 + (-16L);
    var_18 = (uint64_t *)var_17;
    *var_18 = var_16;
    var_19 = (uint64_t *)(var_0 + (-80L));
    storemerge30 = (unsigned char)'\x00';
    var_20 = var_16;
    storemerge37 = 1UL;
    storemerge39 = 1UL;
    storemerge40 = 1UL;
    local_sp_5 = var_4;
    storemerge = (unsigned char)'\x00';
    storemerge25 = (unsigned char)'\x00';
    storemerge35 = (unsigned char)'\x00';
    var_21 = *var_11;
    var_22 = helper_cc_compute_c_wrapper(var_20 - var_21, var_21, var_2, 17U);
    local_sp_6 = local_sp_5;
    while (var_22 != 0UL)
        {
            *(uint64_t *)6508008UL = *var_18;
            if (*(uint64_t *)6506496UL != 0UL) {
                *(uint64_t *)(local_sp_5 + (-16L)) = 4210399UL;
                indirect_placeholder_14(var_3);
                abort();
            }
            var_29 = (unsigned char **)var_17;
            var_30 = (uint64_t)(uint32_t)(uint64_t)**var_29;
            var_31 = local_sp_5 + (-8L);
            *(uint64_t *)var_31 = 4210440UL;
            var_32 = indirect_placeholder_10(var_30);
            local_sp_5_be = var_31;
            local_sp_4 = var_31;
            if (*(unsigned char *)((uint64_t)(unsigned char)var_32 + 6507200UL) != '\x00') {
                var_33 = *var_18;
                var_34 = *var_11;
                var_35 = helper_cc_compute_c_wrapper(var_33 - var_34, var_34, var_2, 17U);
                local_sp_5_be = local_sp_4;
                while (var_35 != 0UL)
                    {
                        var_36 = (uint64_t)(uint32_t)(uint64_t)**var_29;
                        var_37 = local_sp_4 + (-8L);
                        *(uint64_t *)var_37 = 4210490UL;
                        var_38 = indirect_placeholder_10(var_36);
                        local_sp_5_be = var_37;
                        local_sp_4 = var_37;
                        if (*(unsigned char *)((uint64_t)(unsigned char)var_38 + 6507200UL) == '\x00') {
                            break;
                        }
                        var_39 = *var_18 + 1UL;
                        *var_18 = var_39;
                        var_33 = var_39;
                        var_34 = *var_11;
                        var_35 = helper_cc_compute_c_wrapper(var_33 - var_34, var_34, var_2, 17U);
                        local_sp_5_be = local_sp_4;
                    }
            }
            *var_18 = (*var_18 + 1UL);
        }
    var_40 = *(uint64_t *)6508000UL + *(uint64_t *)6507920UL;
    var_41 = *var_18;
    var_42 = helper_cc_compute_c_wrapper(var_40 - var_41, var_41, var_2, 17U);
    if (var_42 == 0UL) {
        *(uint64_t *)6508008UL = *var_18;
    }
    if (*(uint64_t *)6505656UL == 0UL) {
    }
    *(unsigned char *)6508016UL = storemerge;
    var_46 = *(uint64_t *)6508008UL;
    while (1U)
        {
            var_47 = *(uint64_t *)6508000UL;
            var_54 = var_47;
            local_sp_7 = local_sp_6;
            if (var_46 <= var_47) {
                break;
            }
            var_48 = (uint64_t)(uint32_t)(uint64_t)*(unsigned char *)(var_46 + (-1L));
            *(uint64_t *)(local_sp_6 + (-8L)) = 4210693UL;
            var_49 = indirect_placeholder_10(var_48);
            var_50 = (uint64_t)(unsigned char)var_49;
            var_51 = local_sp_6 + (-16L);
            *(uint64_t *)var_51 = 4210703UL;
            var_52 = indirect_placeholder_10(var_50);
            local_sp_6 = var_51;
            local_sp_7 = var_51;
            if ((uint64_t)(uint32_t)var_52 != 0UL) {
                var_54 = *(uint64_t *)6508000UL;
                break;
            }
            var_53 = *(uint64_t *)6508008UL + (-1L);
            *(uint64_t *)6508008UL = var_53;
            var_46 = var_53;
            continue;
        }
    var_55 = *(uint64_t *)(*var_6 + 16UL);
    var_56 = 0UL - var_55;
    var_57 = *(uint64_t *)6507456UL + *(uint64_t *)6507904UL;
    local_sp_8 = local_sp_7;
    if ((long)var_57 < (long)var_56) {
        var_60 = var_54 - var_57;
        var_61 = var_0 + (-24L);
        var_62 = (uint64_t *)var_61;
        *var_62 = var_60;
        _pre_phi293 = var_62;
        if (*(uint64_t *)6506496UL != 0UL) {
            *(uint64_t *)(local_sp_7 + (-16L)) = 4210857UL;
            indirect_placeholder_25(var_3, 0UL, var_60);
            abort();
        }
        var_66 = (unsigned char **)var_61;
        var_67 = (uint64_t)(uint32_t)(uint64_t)**var_66;
        var_68 = local_sp_7 + (-8L);
        *(uint64_t *)var_68 = 4210898UL;
        var_69 = indirect_placeholder_10(var_67);
        local_sp_3 = var_68;
        local_sp_8 = var_68;
        if (*(unsigned char *)((uint64_t)(unsigned char)var_69 + 6507200UL) != '\x00') {
            var_70 = *var_62;
            local_sp_8 = local_sp_3;
            while (*(uint64_t *)6508000UL <= var_70)
                {
                    var_71 = (uint64_t)(uint32_t)(uint64_t)**var_66;
                    var_72 = local_sp_3 + (-8L);
                    *(uint64_t *)var_72 = 4210951UL;
                    var_73 = indirect_placeholder_10(var_71);
                    local_sp_3 = var_72;
                    local_sp_8 = var_72;
                    if (*(unsigned char *)((uint64_t)(unsigned char)var_73 + 6507200UL) == '\x00') {
                        break;
                    }
                    var_74 = *var_62 + 1UL;
                    *var_62 = var_74;
                    var_70 = var_74;
                    local_sp_8 = local_sp_3;
                }
        }
        *var_62 = (*var_62 + 1UL);
    } else {
        var_58 = var_55 + var_54;
        var_59 = (uint64_t *)(var_0 + (-24L));
        *var_59 = var_58;
        _pre_phi293 = var_59;
        *(uint64_t *)6507968UL = *_pre_phi293;
        local_sp_9 = local_sp_8;
        storemerge23 = *(uint64_t *)6508000UL;
        *(uint64_t *)6507976UL = storemerge23;
        local_sp_10 = local_sp_9;
        while (storemerge23 <= *(uint64_t *)6507968UL)
            {
                var_75 = (uint64_t)(uint32_t)(uint64_t)*(unsigned char *)(storemerge23 + (-1L));
                *(uint64_t *)(local_sp_9 + (-8L)) = 4211089UL;
                var_76 = indirect_placeholder_10(var_75);
                var_77 = (uint64_t)(unsigned char)var_76;
                var_78 = local_sp_9 + (-16L);
                *(uint64_t *)var_78 = 4211099UL;
                var_79 = indirect_placeholder_10(var_77);
                local_sp_9 = var_78;
                local_sp_10 = var_78;
                if ((uint64_t)(uint32_t)var_79 == 0UL) {
                    break;
                }
                storemerge23 = *(uint64_t *)6507976UL + (-1L);
                *(uint64_t *)6507976UL = storemerge23;
                local_sp_10 = local_sp_9;
            }
        var_80 = (uint64_t *)(var_0 + (-96L));
        local_sp_11 = local_sp_10;
        while (1U)
            {
                var_81 = *(uint64_t *)6507912UL + *(uint64_t *)6507968UL;
                var_82 = *(uint64_t *)6507976UL;
                var_83 = helper_cc_compute_c_wrapper(var_81 - var_82, var_82, var_2, 17U);
                local_sp_12 = local_sp_11;
                local_sp_14 = local_sp_11;
                if (var_83 != 0UL) {
                    if (*(uint64_t *)6505656UL != 0UL) {
                        loop_state_var = 0U;
                        break;
                    }
                    var_97 = *(uint64_t *)6507968UL;
                    *var_18 = var_97;
                    var_98 = var_97;
                    loop_state_var = 1U;
                    break;
                }
                if (*(uint64_t *)6506496UL == 0UL) {
                    var_87 = (uint64_t)(uint32_t)(uint64_t)**(unsigned char **)6507968UL;
                    var_88 = local_sp_11 + (-8L);
                    *(uint64_t *)var_88 = 4211247UL;
                    var_89 = indirect_placeholder_10(var_87);
                    local_sp_11_be = var_88;
                    local_sp_2 = var_88;
                    if (*(unsigned char *)((uint64_t)(unsigned char)var_89 + 6507200UL) != '\x00') {
                        var_90 = *(uint64_t *)6507968UL;
                        var_91 = *(uint64_t *)6507976UL;
                        var_92 = helper_cc_compute_c_wrapper(var_90 - var_91, var_91, var_2, 17U);
                        local_sp_11_be = local_sp_2;
                        while (var_92 != 0UL)
                            {
                                var_93 = (uint64_t)(uint32_t)(uint64_t)**(unsigned char **)6507968UL;
                                var_94 = local_sp_2 + (-8L);
                                *(uint64_t *)var_94 = 4211322UL;
                                var_95 = indirect_placeholder_10(var_93);
                                local_sp_11_be = var_94;
                                local_sp_2 = var_94;
                                if (*(unsigned char *)((uint64_t)(unsigned char)var_95 + 6507200UL) == '\x00') {
                                    break;
                                }
                                var_96 = *(uint64_t *)6507968UL + 1UL;
                                *(uint64_t *)6507968UL = var_96;
                                var_90 = var_96;
                                var_91 = *(uint64_t *)6507976UL;
                                var_92 = helper_cc_compute_c_wrapper(var_90 - var_91, var_91, var_2, 17U);
                                local_sp_11_be = local_sp_2;
                            }
                    }
                    *(uint64_t *)6507968UL = (*(uint64_t *)6507968UL + 1UL);
                } else {
                    var_84 = *(uint64_t *)6507976UL - *(uint64_t *)6507968UL;
                    var_85 = local_sp_11 + (-8L);
                    *(uint64_t *)var_85 = 4211171UL;
                    var_86 = indirect_placeholder_2(var_84, 6506504UL, 0UL);
                    *var_80 = var_86;
                    local_sp_11_be = var_85;
                    if (var_86 != 18446744073709551614UL) {
                        *(uint64_t *)(local_sp_11 + (-16L)) = 4211187UL;
                        indirect_placeholder_14(var_3);
                        abort();
                    }
                    *(uint64_t *)6507968UL = (((var_86 == 18446744073709551615UL) ? 1UL : var_86) + *(uint64_t *)6507968UL);
                }
                local_sp_11 = local_sp_11_be;
                continue;
            }
        switch (loop_state_var) {
          case 0U:
            {
                *(unsigned char *)6507984UL = storemerge25;
                var_106 = *(uint64_t *)6507968UL;
                local_sp_15 = local_sp_14;
                var_107 = *var_15;
                var_108 = helper_cc_compute_c_wrapper(var_106 - var_107, var_107, var_2, 17U);
                local_sp_16 = local_sp_15;
                while (var_108 != 0UL)
                    {
                        var_109 = (uint64_t)(uint32_t)(uint64_t)**(unsigned char **)6507968UL;
                        *(uint64_t *)(local_sp_15 + (-8L)) = 4211545UL;
                        var_110 = indirect_placeholder_10(var_109);
                        var_111 = (uint64_t)(unsigned char)var_110;
                        var_112 = local_sp_15 + (-16L);
                        *(uint64_t *)var_112 = 4211555UL;
                        var_113 = indirect_placeholder_10(var_111);
                        local_sp_15 = var_112;
                        local_sp_16 = var_112;
                        if ((uint64_t)(uint32_t)var_113 == 0UL) {
                            break;
                        }
                        var_114 = *(uint64_t *)6507968UL + 1UL;
                        *(uint64_t *)6507968UL = var_114;
                        var_106 = var_114;
                        var_107 = *var_15;
                        var_108 = helper_cc_compute_c_wrapper(var_106 - var_107, var_107, var_2, 17U);
                        local_sp_16 = local_sp_15;
                    }
                var_115 = ((*(uint64_t *)6507968UL - *(uint64_t *)6507976UL) + *(uint64_t *)6507912UL) - *(uint64_t *)6505648UL;
                var_116 = (uint64_t *)(var_0 + (-104L));
                *var_116 = var_115;
                local_sp_17 = local_sp_16;
                local_sp_21 = local_sp_16;
                if ((long)var_115 <= (long)0UL) {
                    storemerge33 = *(uint64_t *)6508008UL;
                    *(uint64_t *)6507936UL = storemerge33;
                    var_117 = *var_15;
                    var_118 = helper_cc_compute_c_wrapper(storemerge33 - var_117, var_117, var_2, 17U);
                    local_sp_18 = local_sp_17;
                    while (var_118 != 0UL)
                        {
                            var_119 = (uint64_t)(uint32_t)(uint64_t)**(unsigned char **)6507936UL;
                            *(uint64_t *)(local_sp_17 + (-8L)) = 4211684UL;
                            var_120 = indirect_placeholder_10(var_119);
                            var_121 = (uint64_t)(unsigned char)var_120;
                            var_122 = local_sp_17 + (-16L);
                            *(uint64_t *)var_122 = 4211694UL;
                            var_123 = indirect_placeholder_10(var_121);
                            local_sp_17 = var_122;
                            local_sp_18 = var_122;
                            if ((uint64_t)(uint32_t)var_123 == 0UL) {
                                break;
                            }
                            storemerge33 = *(uint64_t *)6507936UL + 1UL;
                            *(uint64_t *)6507936UL = storemerge33;
                            var_117 = *var_15;
                            var_118 = helper_cc_compute_c_wrapper(storemerge33 - var_117, var_117, var_2, 17U);
                            local_sp_18 = local_sp_17;
                        }
                    var_124 = *(uint64_t *)6507936UL;
                    *(uint64_t *)6507944UL = var_124;
                    *var_18 = var_124;
                    var_125 = (uint64_t *)(var_0 + (-112L));
                    var_126 = var_124;
                    local_sp_19 = local_sp_18;
                    while (1U)
                        {
                            var_127 = *var_11;
                            var_128 = helper_cc_compute_c_wrapper(var_126 - var_127, var_127, var_2, 17U);
                            local_sp_20 = local_sp_19;
                            if (var_128 != 0UL) {
                                _pre284 = *(uint64_t *)6507936UL;
                                _pre_phi295 = *var_116 + _pre284;
                                var_146 = *var_18;
                                var_147 = _pre284;
                                break;
                            }
                            var_129 = *(uint64_t *)6507936UL;
                            var_130 = *var_116 + var_129;
                            var_131 = *var_18;
                            _pre_phi295 = var_130;
                            var_146 = var_131;
                            var_147 = var_129;
                            if (var_130 <= var_131) {
                                break;
                            }
                            *(uint64_t *)6507944UL = var_131;
                            if (*(uint64_t *)6506496UL != 0UL) {
                                *(uint64_t *)(local_sp_19 + (-16L)) = 4211806UL;
                                indirect_placeholder_14(var_3);
                                abort();
                            }
                            var_135 = (unsigned char **)var_17;
                            var_136 = (uint64_t)(uint32_t)(uint64_t)**var_135;
                            var_137 = local_sp_19 + (-8L);
                            *(uint64_t *)var_137 = 4211847UL;
                            var_138 = indirect_placeholder_10(var_136);
                            local_sp_19_be = var_137;
                            local_sp_1 = var_137;
                            if (*(unsigned char *)((uint64_t)(unsigned char)var_138 + 6507200UL) != '\x00') {
                                var_139 = *var_18;
                                var_140 = *var_11;
                                var_141 = helper_cc_compute_c_wrapper(var_139 - var_140, var_140, var_2, 17U);
                                local_sp_19_be = local_sp_1;
                                while (var_141 != 0UL)
                                    {
                                        var_142 = (uint64_t)(uint32_t)(uint64_t)**var_135;
                                        var_143 = local_sp_1 + (-8L);
                                        *(uint64_t *)var_143 = 4211897UL;
                                        var_144 = indirect_placeholder_10(var_142);
                                        local_sp_19_be = var_143;
                                        local_sp_1 = var_143;
                                        if (*(unsigned char *)((uint64_t)(unsigned char)var_144 + 6507200UL) == '\x00') {
                                            break;
                                        }
                                        var_145 = *var_18 + 1UL;
                                        *var_18 = var_145;
                                        var_139 = var_145;
                                        var_140 = *var_11;
                                        var_141 = helper_cc_compute_c_wrapper(var_139 - var_140, var_140, var_2, 17U);
                                        local_sp_19_be = local_sp_1;
                                    }
                            }
                            *var_18 = (*var_18 + 1UL);
                        }
                    var_148 = var_147;
                    var_149 = var_146;
                    if (_pre_phi295 > var_146) {
                        *(uint64_t *)6507944UL = var_146;
                        var_148 = *(uint64_t *)6507936UL;
                    } else {
                        var_149 = *(uint64_t *)6507944UL;
                    }
                    if (var_149 <= var_148) {
                        *(unsigned char *)6508016UL = (unsigned char)'\x00';
                        storemerge35 = (unsigned char)'\x01';
                        if (*(uint64_t *)6505656UL == 0UL) {
                        } else {
                            var_150 = *(uint64_t *)6507944UL;
                            var_151 = *var_11;
                            var_152 = helper_cc_compute_c_wrapper(var_150 - var_151, var_151, var_2, 17U);
                            if (var_152 == 0UL) {
                            }
                        }
                    }
                    *(unsigned char *)6507952UL = storemerge35;
                    var_153 = *(uint64_t *)6507944UL;
                    local_sp_21 = local_sp_20;
                    while (var_153 <= *(uint64_t *)6507936UL)
                        {
                            var_154 = (uint64_t)(uint32_t)(uint64_t)*(unsigned char *)(var_153 + (-1L));
                            *(uint64_t *)(local_sp_20 + (-8L)) = 4212129UL;
                            var_155 = indirect_placeholder_10(var_154);
                            var_156 = (uint64_t)(unsigned char)var_155;
                            var_157 = local_sp_20 + (-16L);
                            *(uint64_t *)var_157 = 4212139UL;
                            var_158 = indirect_placeholder_10(var_156);
                            local_sp_20 = var_157;
                            local_sp_21 = var_157;
                            if ((uint64_t)(uint32_t)var_158 == 0UL) {
                                break;
                            }
                            var_159 = *(uint64_t *)6507944UL + (-1L);
                            *(uint64_t *)6507944UL = var_159;
                            var_153 = var_159;
                            local_sp_21 = local_sp_20;
                        }
                }
                *(uint64_t *)6507936UL = 0UL;
                *(uint64_t *)6507944UL = 0UL;
                *(unsigned char *)6507952UL = (unsigned char)'\x00';
                var_160 = ((*(uint64_t *)6508000UL - *(uint64_t *)6508008UL) + *(uint64_t *)6507920UL) - *(uint64_t *)6505648UL;
                var_161 = (uint64_t *)(var_0 + (-120L));
                *var_161 = var_160;
                local_sp_22 = local_sp_21;
                local_sp_26 = local_sp_21;
                if ((long)var_160 <= (long)0UL) {
                    *(uint64_t *)6508032UL = 0UL;
                    *(uint64_t *)6508040UL = 0UL;
                    *(unsigned char *)6508048UL = (unsigned char)'\x00';
                    local_sp_27 = local_sp_26;
                    if (*(unsigned char *)6506048UL == '\x00') {
                        if (*(unsigned char *)6506049UL != '\x00') {
                            var_201 = *(uint64_t *)(*var_6 + 32UL) + *(uint64_t *)6508000UL;
                            *(uint64_t *)6508064UL = var_201;
                            storemerge26 = var_201;
                            *(uint64_t *)6508072UL = storemerge26;
                            var_202 = *var_11;
                            var_203 = helper_cc_compute_c_wrapper(storemerge26 - var_202, var_202, var_2, 17U);
                            while (var_203 != 0UL)
                                {
                                    var_204 = (uint64_t)(uint32_t)(uint64_t)**(unsigned char **)6508072UL;
                                    *(uint64_t *)(local_sp_27 + (-8L)) = 4213108UL;
                                    var_205 = indirect_placeholder_10(var_204);
                                    var_206 = (uint64_t)(unsigned char)var_205;
                                    var_207 = local_sp_27 + (-16L);
                                    *(uint64_t *)var_207 = 4213118UL;
                                    var_208 = indirect_placeholder_10(var_206);
                                    local_sp_27 = var_207;
                                    if ((uint64_t)(uint32_t)var_208 == 0UL) {
                                        break;
                                    }
                                    storemerge26 = *(uint64_t *)6508072UL + 1UL;
                                    *(uint64_t *)6508072UL = storemerge26;
                                    var_202 = *var_11;
                                    var_203 = helper_cc_compute_c_wrapper(storemerge26 - var_202, var_202, var_2, 17U);
                                }
                        }
                    }
                    var_195 = *(uint64_t *)(((uint64_t)*(uint32_t *)(*var_6 + 40UL) << 3UL) + *(uint64_t *)6507544UL);
                    *(uint64_t *)(var_0 + (-32L)) = ((var_195 == 0UL) ? 4380795UL : var_195);
                    var_196 = *(uint64_t *)(*var_6 + 32UL) + 1UL;
                    var_197 = (uint64_t *)(var_0 + (-40L));
                    *var_197 = var_196;
                    var_198 = helper_cc_compute_all_wrapper((uint64_t)*(uint32_t *)(*var_6 + 40UL), 0UL, 0UL, 24U);
                    if ((uint64_t)(((unsigned char)(var_198 >> 4UL) ^ (unsigned char)var_198) & '\xc0') == 0UL) {
                        *var_197 = (*var_197 - *(uint64_t *)((((uint64_t)*(uint32_t *)(*var_6 + 40UL) << 3UL) + (-8L)) + *(uint64_t *)6507552UL));
                    }
                    var_199 = *(uint64_t *)6508064UL;
                    *(uint64_t *)(local_sp_26 + (-8L)) = 4212953UL;
                    indirect_placeholder();
                    var_200 = (uint64_t *)(var_0 + (-136L));
                    *var_200 = var_199;
                    *(uint64_t *)(local_sp_26 + (-16L)) = 4212983UL;
                    indirect_placeholder();
                    *(uint64_t *)6508072UL = *var_200;
                    return;
                }
                storemerge28 = *(uint64_t *)6507968UL;
                *(uint64_t *)6508040UL = storemerge28;
                local_sp_23 = local_sp_22;
                while (storemerge28 <= *var_13)
                    {
                        var_162 = (uint64_t)(uint32_t)(uint64_t)*(unsigned char *)(storemerge28 + (-1L));
                        *(uint64_t *)(local_sp_22 + (-8L)) = 4212303UL;
                        var_163 = indirect_placeholder_10(var_162);
                        var_164 = (uint64_t)(unsigned char)var_163;
                        var_165 = local_sp_22 + (-16L);
                        *(uint64_t *)var_165 = 4212313UL;
                        var_166 = indirect_placeholder_10(var_164);
                        local_sp_22 = var_165;
                        local_sp_23 = var_165;
                        if ((uint64_t)(uint32_t)var_166 == 0UL) {
                            break;
                        }
                        storemerge28 = *(uint64_t *)6508040UL + (-1L);
                        *(uint64_t *)6508040UL = storemerge28;
                        local_sp_23 = local_sp_22;
                    }
                var_167 = *_pre_phi293;
                *(uint64_t *)6508032UL = var_167;
                var_168 = (uint64_t *)(var_0 + (-128L));
                var_169 = var_167;
                local_sp_24 = local_sp_23;
                while (1U)
                    {
                        var_170 = var_169 + *var_161;
                        var_171 = *(uint64_t *)6508040UL;
                        var_172 = helper_cc_compute_c_wrapper(var_170 - var_171, var_171, var_2, 17U);
                        local_sp_25 = local_sp_24;
                        if (var_172 != 0UL) {
                            if (*(uint64_t *)6508040UL <= *(uint64_t *)6508032UL) {
                                loop_state_var = 0U;
                                break;
                            }
                            *(unsigned char *)6507984UL = (unsigned char)'\x00';
                            storemerge30 = (unsigned char)'\x01';
                            if (*(uint64_t *)6505656UL != 0UL) {
                                loop_state_var = 1U;
                                break;
                            }
                            if (*(uint64_t *)6508032UL <= *var_9) {
                                loop_state_var = 1U;
                                break;
                            }
                            loop_state_var = 0U;
                            break;
                        }
                        if (*(uint64_t *)6506496UL == 0UL) {
                            var_176 = (uint64_t)(uint32_t)(uint64_t)**(unsigned char **)6508032UL;
                            var_177 = local_sp_24 + (-8L);
                            *(uint64_t *)var_177 = 4212472UL;
                            var_178 = indirect_placeholder_10(var_176);
                            local_sp_24_be = var_177;
                            local_sp_0 = var_177;
                            if (*(unsigned char *)((uint64_t)(unsigned char)var_178 + 6507200UL) != '\x00') {
                                var_179 = *(uint64_t *)6508032UL;
                                var_180 = *(uint64_t *)6508040UL;
                                var_181 = helper_cc_compute_c_wrapper(var_179 - var_180, var_180, var_2, 17U);
                                local_sp_24_be = local_sp_0;
                                while (var_181 != 0UL)
                                    {
                                        var_182 = (uint64_t)(uint32_t)(uint64_t)**(unsigned char **)6508032UL;
                                        var_183 = local_sp_0 + (-8L);
                                        *(uint64_t *)var_183 = 4212547UL;
                                        var_184 = indirect_placeholder_10(var_182);
                                        local_sp_24_be = var_183;
                                        local_sp_0 = var_183;
                                        if (*(unsigned char *)((uint64_t)(unsigned char)var_184 + 6507200UL) == '\x00') {
                                            break;
                                        }
                                        var_185 = *(uint64_t *)6508032UL + 1UL;
                                        *(uint64_t *)6508032UL = var_185;
                                        var_179 = var_185;
                                        var_180 = *(uint64_t *)6508040UL;
                                        var_181 = helper_cc_compute_c_wrapper(var_179 - var_180, var_180, var_2, 17U);
                                        local_sp_24_be = local_sp_0;
                                    }
                            }
                            *(uint64_t *)6508032UL = (*(uint64_t *)6508032UL + 1UL);
                        } else {
                            var_173 = *(uint64_t *)6508040UL - *(uint64_t *)6508032UL;
                            var_174 = local_sp_24 + (-8L);
                            *(uint64_t *)var_174 = 4212396UL;
                            var_175 = indirect_placeholder_2(var_173, 6506504UL, 0UL);
                            *var_168 = var_175;
                            local_sp_24_be = var_174;
                            if (var_175 != 18446744073709551614UL) {
                                *(uint64_t *)(local_sp_24 + (-16L)) = 4212412UL;
                                indirect_placeholder_14(var_3);
                                abort();
                            }
                            *(uint64_t *)6508032UL = (((var_175 == 18446744073709551615UL) ? 1UL : var_175) + *(uint64_t *)6508032UL);
                        }
                        var_169 = *(uint64_t *)6508032UL;
                        local_sp_24 = local_sp_24_be;
                        continue;
                    }
                switch (loop_state_var) {
                  case 1U:
                    {
                    }
                    break;
                  case 0U:
                    {
                        *(unsigned char *)6508048UL = storemerge30;
                        var_186 = *(uint64_t *)6508032UL;
                        var_187 = *(uint64_t *)6508040UL;
                        var_188 = helper_cc_compute_c_wrapper(var_186 - var_187, var_187, var_2, 17U);
                        local_sp_26 = local_sp_25;
                        while (var_188 != 0UL)
                            {
                                var_189 = (uint64_t)(uint32_t)(uint64_t)**(unsigned char **)6508032UL;
                                *(uint64_t *)(local_sp_25 + (-8L)) = 4212753UL;
                                var_190 = indirect_placeholder_10(var_189);
                                var_191 = (uint64_t)(unsigned char)var_190;
                                var_192 = local_sp_25 + (-16L);
                                *(uint64_t *)var_192 = 4212763UL;
                                var_193 = indirect_placeholder_10(var_191);
                                local_sp_25 = var_192;
                                local_sp_26 = var_192;
                                if ((uint64_t)(uint32_t)var_193 == 0UL) {
                                    break;
                                }
                                var_194 = *(uint64_t *)6508032UL + 1UL;
                                *(uint64_t *)6508032UL = var_194;
                                var_186 = var_194;
                                var_187 = *(uint64_t *)6508040UL;
                                var_188 = helper_cc_compute_c_wrapper(var_186 - var_187, var_187, var_2, 17U);
                                local_sp_26 = local_sp_25;
                            }
                    }
                    break;
                }
            }
            break;
          case 1U:
            {
                while (1U)
                    {
                        var_105 = var_98;
                        local_sp_13 = local_sp_12;
                        if (var_98 <= *var_13) {
                            break;
                        }
                        var_99 = (uint64_t)(uint32_t)(uint64_t)*(unsigned char *)(var_98 + (-1L));
                        *(uint64_t *)(local_sp_12 + (-8L)) = 4211452UL;
                        var_100 = indirect_placeholder_10(var_99);
                        var_101 = (uint64_t)(unsigned char)var_100;
                        var_102 = local_sp_12 + (-16L);
                        *(uint64_t *)var_102 = 4211462UL;
                        var_103 = indirect_placeholder_10(var_101);
                        local_sp_12 = var_102;
                        local_sp_13 = var_102;
                        if ((uint64_t)(uint32_t)var_103 != 0UL) {
                            var_105 = *var_18;
                            break;
                        }
                        var_104 = *var_18 + (-1L);
                        *var_18 = var_104;
                        var_98 = var_104;
                        continue;
                    }
                local_sp_14 = local_sp_13;
                storemerge25 = (var_105 > *var_9);
            }
            break;
        }
    }
}

typedef unsigned char __u_char;
typedef unsigned short __u_short;
typedef unsigned int __u_int;
typedef unsigned long __u_long;
typedef signed char __int8_t;
typedef unsigned char __uint8_t;
typedef short __int16_t;
typedef unsigned short __uint16_t;
typedef int __int32_t;
typedef unsigned int __uint32_t;
typedef long __int64_t;
typedef unsigned long __uint64_t;
typedef __int8_t __int_least8_t;
typedef __uint8_t __uint_least8_t;
typedef __int16_t __int_least16_t;
typedef __uint16_t __uint_least16_t;
typedef __int32_t __int_least32_t;
typedef __uint32_t __uint_least32_t;
typedef __int64_t __int_least64_t;
typedef __uint64_t __uint_least64_t;
typedef long __quad_t;
typedef unsigned long __u_quad_t;
typedef long __intmax_t;
typedef unsigned long __uintmax_t;
typedef unsigned long __dev_t;
typedef unsigned int __uid_t;
typedef unsigned int __gid_t;
typedef unsigned long __ino_t;
typedef unsigned long __ino64_t;
typedef unsigned int __mode_t;
typedef unsigned long __nlink_t;
typedef long __off_t;
typedef long __off64_t;
typedef int __pid_t;
typedef struct {
    int __val[2];
} __fsid_t;
typedef long __clock_t;
typedef unsigned long __rlim_t;
typedef unsigned long __rlim64_t;
typedef unsigned int __id_t;
typedef long __time_t;
typedef unsigned int __useconds_t;
typedef long __suseconds_t;
typedef int __daddr_t;
typedef int __key_t;
typedef int __clockid_t;
typedef void *__timer_t;
typedef long __blksize_t;
typedef long __blkcnt_t;
typedef long __blkcnt64_t;
typedef unsigned long __fsblkcnt_t;
typedef unsigned long __fsblkcnt64_t;
typedef unsigned long __fsfilcnt_t;
typedef unsigned long __fsfilcnt64_t;
typedef long __fsword_t;
typedef long __ssize_t;
typedef long __syscall_slong_t;
typedef unsigned long __syscall_ulong_t;
typedef __off64_t __loff_t;
typedef char *__caddr_t;
typedef long __intptr_t;
typedef unsigned int __socklen_t;
typedef int __sig_atomic_t;
typedef __int8_t int8_t;
typedef __int16_t int16_t;
typedef __int32_t int32_t;
typedef __int64_t int64_t;
typedef __uint8_t uint8_t;
typedef __uint16_t uint16_t;
typedef __uint32_t uint32_t;
typedef __uint64_t uint64_t;
typedef __int_least8_t int_least8_t;
typedef __int_least16_t int_least16_t;
typedef __int_least32_t int_least32_t;
typedef __int_least64_t int_least64_t;
typedef __uint_least8_t uint_least8_t;
typedef __uint_least16_t uint_least16_t;
typedef __uint_least32_t uint_least32_t;
typedef __uint_least64_t uint_least64_t;
typedef signed char int_fast8_t;
typedef long int_fast16_t;
typedef long int_fast32_t;
typedef long int_fast64_t;
typedef unsigned char uint_fast8_t;
typedef unsigned long uint_fast16_t;
typedef unsigned long uint_fast32_t;
typedef unsigned long uint_fast64_t;
typedef long intptr_t;
typedef unsigned long uintptr_t;
typedef __intmax_t intmax_t;
typedef __uintmax_t uintmax_t;
typedef _Bool bool;
extern uint64_t revng_init_local_sp(uint64_t param_0);
extern uint64_t init_rbp(void);
extern void indirect_placeholder(void);
extern uint64_t indirect_placeholder_10(uint64_t param_0);
uint64_t bb_copy_unescaped_string(uint64_t rdi) {
    uint64_t var_0;
    uint64_t var_1;
    uint64_t var_2;
    uint64_t *var_3;
    uint64_t var_4;
    uint64_t var_5;
    uint64_t var_6;
    uint64_t *var_7;
    uint64_t var_8;
    uint64_t *var_9;
    uint32_t *var_10;
    uint32_t *var_11;
    uint64_t local_sp_3;
    uint64_t local_sp_0_ph;
    uint32_t var_33;
    unsigned char var_34;
    uint32_t rax_0_in;
    uint64_t local_sp_1;
    unsigned char **var_12;
    uint64_t var_13;
    unsigned char var_14;
    uint32_t var_15;
    unsigned char var_16;
    uint64_t var_17;
    uint64_t var_18;
    unsigned char var_19;
    uint64_t var_20;
    uint64_t var_21;
    uint64_t var_22;
    uint64_t var_23;
    uint64_t var_24;
    uint32_t var_28;
    uint64_t var_29;
    uint64_t var_30;
    uint64_t var_31;
    uint64_t var_32;
    uint32_t var_35;
    uint64_t var_37;
    uint64_t local_sp_2;
    bool var_36;
    uint64_t var_38;
    uint64_t var_25;
    uint64_t var_26;
    uint64_t var_27;
    uint64_t var_39;
    uint64_t var_40;
    uint64_t var_41;
    uint64_t var_42;
    uint64_t var_43;
    unsigned int loop_state_var;
    bool switch_state_var;
    var_0 = revng_init_local_sp(0UL);
    var_1 = init_rbp();
    *(uint64_t *)(var_0 + (-8L)) = var_1;
    var_2 = var_0 + (-48L);
    var_3 = (uint64_t *)var_2;
    *var_3 = rdi;
    *(uint64_t *)(var_0 + (-64L)) = 4203884UL;
    indirect_placeholder();
    var_4 = rdi + 1UL;
    var_5 = var_0 + (-72L);
    *(uint64_t *)var_5 = 4203896UL;
    var_6 = indirect_placeholder_10(var_4);
    var_7 = (uint64_t *)(var_0 + (-32L));
    *var_7 = var_6;
    var_8 = var_0 + (-16L);
    var_9 = (uint64_t *)var_8;
    *var_9 = var_6;
    var_10 = (uint32_t *)(var_0 + (-20L));
    var_11 = (uint32_t *)(var_0 + (-24L));
    local_sp_0_ph = var_5;
    while (1U)
        {
            local_sp_1 = local_sp_0_ph;
            local_sp_3 = local_sp_0_ph;
            while (1U)
                {
                    var_12 = (unsigned char **)var_2;
                    switch_state_var = 0;
                    switch (**var_12) {
                      case '\x00':
                        {
                            **(unsigned char **)var_8 = (unsigned char)'\x00';
                            return *var_7;
                        }
                        break;
                      case '\\':
                        {
                            switch_state_var = 1;
                            break;
                        }
                        break;
                      default:
                        {
                            var_42 = *var_9;
                            *var_9 = (var_42 + 1UL);
                            var_43 = *var_3;
                            *var_3 = (var_43 + 1UL);
                            *(unsigned char *)var_42 = *(unsigned char *)var_43;
                            continue;
                        }
                        break;
                    }
                    if (switch_state_var)
                        break;
                }
            var_13 = *var_3 + 1UL;
            *var_3 = var_13;
            var_14 = **var_12;
            var_15 = (uint32_t)(uint64_t)var_14;
            var_18 = var_13;
            var_19 = var_14;
            if ((uint64_t)(var_15 + (-102)) == 0UL) {
                var_41 = *var_9;
                *var_9 = (var_41 + 1UL);
                *(unsigned char *)var_41 = (unsigned char)'\f';
                *var_3 = (*var_3 + 1UL);
            } else {
                if ((signed char)var_14 > 'f') {
                    if ((uint64_t)(var_15 + (-116)) != 0UL) {
                        var_40 = *var_9;
                        *var_9 = (var_40 + 1UL);
                        *(unsigned char *)var_40 = (unsigned char)'\t';
                        *var_3 = (*var_3 + 1UL);
                        local_sp_0_ph = local_sp_3;
                        continue;
                    }
                    if ((signed char)var_14 <= 't') {
                        if ((uint64_t)(var_15 + (-110)) != 0UL) {
                            var_24 = *var_9;
                            *var_9 = (var_24 + 1UL);
                            *(unsigned char *)var_24 = (unsigned char)'\n';
                            *var_3 = (*var_3 + 1UL);
                            local_sp_0_ph = local_sp_3;
                            continue;
                        }
                        if ((uint64_t)(var_15 + (-114)) != 0UL) {
                            var_23 = *var_9;
                            *var_9 = (var_23 + 1UL);
                            *(unsigned char *)var_23 = (unsigned char)'\r';
                            *var_3 = (*var_3 + 1UL);
                            local_sp_0_ph = local_sp_3;
                            continue;
                        }
                    }
                    if ((uint64_t)(var_15 + (-118)) != 0UL) {
                        var_39 = *var_9;
                        *var_9 = (var_39 + 1UL);
                        *(unsigned char *)var_39 = (unsigned char)'\v';
                        *var_3 = (*var_3 + 1UL);
                        local_sp_0_ph = local_sp_3;
                        continue;
                    }
                    if ((uint64_t)(var_15 + (-120)) != 0UL) {
                        *var_10 = 0U;
                        *var_11 = 0U;
                        *var_3 = (*var_3 + 1UL);
                        while (1U)
                            {
                                var_28 = *var_11;
                                var_35 = var_28;
                                local_sp_2 = local_sp_1;
                                if ((int)var_28 <= (int)2U) {
                                    loop_state_var = 1U;
                                    break;
                                }
                                var_29 = (uint64_t)(uint32_t)(uint64_t)**var_12;
                                *(uint64_t *)(local_sp_1 + (-8L)) = 4204224UL;
                                var_30 = indirect_placeholder_10(var_29);
                                var_31 = (uint64_t)(unsigned char)var_30;
                                var_32 = local_sp_1 + (-16L);
                                *(uint64_t *)var_32 = 4204234UL;
                                indirect_placeholder();
                                local_sp_1 = var_32;
                                local_sp_2 = var_32;
                                if (var_31 != 0UL) {
                                    loop_state_var = 0U;
                                    break;
                                }
                                var_33 = *var_10 << 4U;
                                var_34 = **var_12;
                                if (((signed char)var_34 <= '`') || ((signed char)var_34 > 'f')) {
                                    rax_0_in = (uint32_t)var_34 + (-87);
                                } else {
                                    if (((signed char)var_34 <= '@') || ((signed char)var_34 > 'F')) {
                                        rax_0_in = (uint32_t)var_34 + (-48);
                                    } else {
                                        rax_0_in = (uint32_t)var_34 + (-55);
                                    }
                                }
                                *var_10 = (rax_0_in + var_33);
                                *var_11 = (*var_11 + 1U);
                                *var_3 = (*var_3 + 1UL);
                                continue;
                            }
                        switch (loop_state_var) {
                          case 0U:
                            {
                                var_35 = *var_11;
                            }
                            break;
                          case 1U:
                            {
                            }
                            break;
                        }
                        var_36 = (var_35 == 0U);
                        var_37 = *var_9;
                        *var_9 = (var_37 + 1UL);
                        local_sp_3 = local_sp_2;
                        if (var_36) {
                            *(unsigned char *)var_37 = (unsigned char)'\\';
                            var_38 = *var_9;
                            *var_9 = (var_38 + 1UL);
                            *(unsigned char *)var_38 = (unsigned char)'x';
                        } else {
                            *(unsigned char *)var_37 = (unsigned char)*var_10;
                        }
                        local_sp_0_ph = local_sp_3;
                        continue;
                    }
                }
                if ((uint64_t)(var_15 + (-97)) != 0UL) {
                    var_22 = *var_9;
                    *var_9 = (var_22 + 1UL);
                    *(unsigned char *)var_22 = (unsigned char)'\a';
                    *var_3 = (*var_3 + 1UL);
                    local_sp_0_ph = local_sp_3;
                    continue;
                }
                if ((signed char)var_14 > 'a') {
                    if ((uint64_t)(var_15 + (-98)) != 0UL) {
                        var_21 = *var_9;
                        *var_9 = (var_21 + 1UL);
                        *(unsigned char *)var_21 = (unsigned char)'\b';
                        *var_3 = (*var_3 + 1UL);
                        local_sp_0_ph = local_sp_3;
                        continue;
                    }
                    if ((uint64_t)(var_15 + (-99)) == 0UL) {
                        while (var_19 != '\x00')
                            {
                                var_20 = var_18 + 1UL;
                                *var_3 = var_20;
                                var_18 = var_20;
                                var_19 = **var_12;
                            }
                        local_sp_0_ph = local_sp_3;
                        continue;
                    }
                }
                if ((uint64_t)var_15 != 0UL) {
                    local_sp_0_ph = local_sp_3;
                    continue;
                }
                if ((uint64_t)(var_15 + (-48)) != 0UL) {
                    *var_10 = 0U;
                    *var_11 = 0U;
                    *var_3 = (*var_3 + 1UL);
                    while ((int)*var_11 <= (int)2U)
                        {
                            var_16 = **var_12;
                            if (((signed char)var_16 <= '/') || ((signed char)var_16 > '7')) {
                                break;
                            }
                            *var_10 = (((uint32_t)var_16 + (-48)) + (*var_10 << 3U));
                            *var_11 = (*var_11 + 1U);
                            *var_3 = (*var_3 + 1UL);
                        }
                    var_17 = *var_9;
                    *var_9 = (var_17 + 1UL);
                    *(unsigned char *)var_17 = (unsigned char)*var_10;
                    local_sp_0_ph = local_sp_3;
                    continue;
                }
                var_25 = *var_9;
                *var_9 = (var_25 + 1UL);
                *(unsigned char *)var_25 = (unsigned char)'\\';
                var_26 = *var_9;
                *var_9 = (var_26 + 1UL);
                var_27 = *var_3;
                *var_3 = (var_27 + 1UL);
                *(unsigned char *)var_26 = *(unsigned char *)var_27;
            }
        }
}

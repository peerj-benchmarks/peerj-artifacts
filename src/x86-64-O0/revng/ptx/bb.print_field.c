typedef unsigned char __u_char;
typedef unsigned short __u_short;
typedef unsigned int __u_int;
typedef unsigned long __u_long;
typedef signed char __int8_t;
typedef unsigned char __uint8_t;
typedef short __int16_t;
typedef unsigned short __uint16_t;
typedef int __int32_t;
typedef unsigned int __uint32_t;
typedef long __int64_t;
typedef unsigned long __uint64_t;
typedef __int8_t __int_least8_t;
typedef __uint8_t __uint_least8_t;
typedef __int16_t __int_least16_t;
typedef __uint16_t __uint_least16_t;
typedef __int32_t __int_least32_t;
typedef __uint32_t __uint_least32_t;
typedef __int64_t __int_least64_t;
typedef __uint64_t __uint_least64_t;
typedef long __quad_t;
typedef unsigned long __u_quad_t;
typedef long __intmax_t;
typedef unsigned long __uintmax_t;
typedef unsigned long __dev_t;
typedef unsigned int __uid_t;
typedef unsigned int __gid_t;
typedef unsigned long __ino_t;
typedef unsigned long __ino64_t;
typedef unsigned int __mode_t;
typedef unsigned long __nlink_t;
typedef long __off_t;
typedef long __off64_t;
typedef int __pid_t;
typedef struct {
    int __val[2];
} __fsid_t;
typedef long __clock_t;
typedef unsigned long __rlim_t;
typedef unsigned long __rlim64_t;
typedef unsigned int __id_t;
typedef long __time_t;
typedef unsigned int __useconds_t;
typedef long __suseconds_t;
typedef int __daddr_t;
typedef int __key_t;
typedef int __clockid_t;
typedef void *__timer_t;
typedef long __blksize_t;
typedef long __blkcnt_t;
typedef long __blkcnt64_t;
typedef unsigned long __fsblkcnt_t;
typedef unsigned long __fsblkcnt64_t;
typedef unsigned long __fsfilcnt_t;
typedef unsigned long __fsfilcnt64_t;
typedef long __fsword_t;
typedef long __ssize_t;
typedef long __syscall_slong_t;
typedef unsigned long __syscall_ulong_t;
typedef __off64_t __loff_t;
typedef char *__caddr_t;
typedef long __intptr_t;
typedef unsigned int __socklen_t;
typedef int __sig_atomic_t;
typedef __int8_t int8_t;
typedef __int16_t int16_t;
typedef __int32_t int32_t;
typedef __int64_t int64_t;
typedef __uint8_t uint8_t;
typedef __uint16_t uint16_t;
typedef __uint32_t uint32_t;
typedef __uint64_t uint64_t;
typedef __int_least8_t int_least8_t;
typedef __int_least16_t int_least16_t;
typedef __int_least32_t int_least32_t;
typedef __int_least64_t int_least64_t;
typedef __uint_least8_t uint_least8_t;
typedef __uint_least16_t uint_least16_t;
typedef __uint_least32_t uint_least32_t;
typedef __uint_least64_t uint_least64_t;
typedef signed char int_fast8_t;
typedef long int_fast16_t;
typedef long int_fast32_t;
typedef long int_fast64_t;
typedef unsigned char uint_fast8_t;
typedef unsigned long uint_fast16_t;
typedef unsigned long uint_fast32_t;
typedef unsigned long uint_fast64_t;
typedef long intptr_t;
typedef unsigned long uintptr_t;
typedef __intmax_t intmax_t;
typedef __uintmax_t uintmax_t;
typedef _Bool bool;
extern void abort(void);
extern uint64_t revng_init_local_sp(uint64_t param_0);
extern uint64_t init_rbp(void);
extern void indirect_placeholder(void);
void bb_print_field(uint64_t rdi, uint64_t rsi) {
    uint64_t var_0;
    uint64_t var_1;
    uint64_t var_2;
    uint64_t *var_3;
    uint64_t *var_4;
    uint64_t var_5;
    uint64_t var_6;
    uint64_t *var_7;
    unsigned char *var_8;
    uint32_t *var_9;
    uint32_t *var_10;
    uint64_t var_11;
    uint64_t local_sp_3;
    unsigned char var_12;
    uint64_t var_13;
    uint32_t var_14;
    uint32_t var_15;
    uint32_t var_16;
    bool var_17;
    uint64_t var_18;
    uint64_t *var_19;
    uint64_t var_20;
    bool var_21;
    uint64_t var_22;
    uint64_t *var_23;
    uint64_t var_24;
    uint64_t var_25;
    uint64_t var_26;
    uint64_t var_27;
    uint64_t var_28;
    uint64_t var_29;
    uint64_t var_30;
    uint64_t var_31;
    uint64_t var_33;
    uint64_t var_32;
    uint64_t var_34;
    uint64_t var_35;
    uint64_t local_sp_5;
    uint64_t var_44;
    unsigned char var_36;
    uint32_t var_37;
    uint64_t var_38;
    uint64_t var_39;
    uint64_t var_40;
    uint64_t var_41;
    uint64_t var_42;
    uint64_t var_43;
    var_0 = revng_init_local_sp(0UL);
    var_1 = init_rbp();
    *(uint64_t *)(var_0 + (-8L)) = var_1;
    var_2 = var_0 + (-56L);
    var_3 = (uint64_t *)var_2;
    *var_3 = rdi;
    var_4 = (uint64_t *)(var_0 + (-48L));
    *var_4 = rsi;
    var_5 = *var_3;
    var_6 = var_0 + (-16L);
    var_7 = (uint64_t *)var_6;
    *var_7 = var_5;
    var_8 = (unsigned char *)(var_0 + (-17L));
    var_9 = (uint32_t *)(var_0 + (-24L));
    var_10 = (uint32_t *)(var_0 + (-28L));
    var_11 = var_5;
    local_sp_3 = var_2;
    while (1U)
        {
            local_sp_5 = local_sp_3;
            if (*var_4 > var_11) {
                return;
            }
            var_12 = **(unsigned char **)var_6;
            *var_8 = var_12;
            var_13 = (uint64_t)var_12;
            if (*(unsigned char *)(var_13 + 6507648UL) == '\x00') {
                var_43 = local_sp_3 + (-8L);
                *(uint64_t *)var_43 = 4209224UL;
                indirect_placeholder();
                local_sp_5 = var_43;
            } else {
                var_14 = (uint32_t)*(unsigned char *)(var_13 | 4384256UL);
                *var_9 = var_14;
                if (var_14 == 0U) {
                    var_36 = *var_8;
                    var_37 = (uint32_t)(uint64_t)var_36;
                    if ((uint64_t)(var_37 + (-92)) == 0UL) {
                        var_42 = local_sp_3 + (-8L);
                        *(uint64_t *)var_42 = 4209190UL;
                        indirect_placeholder();
                        local_sp_5 = var_42;
                    } else {
                        if (var_36 > '\\') {
                            if ((uint64_t)(var_37 + (-123)) != 0UL) {
                                var_41 = local_sp_3 + (-8L);
                                *(uint64_t *)var_41 = 4209168UL;
                                indirect_placeholder();
                                local_sp_5 = var_41;
                                var_44 = *var_7 + 1UL;
                                *var_7 = var_44;
                                var_11 = var_44;
                                local_sp_3 = local_sp_5;
                                continue;
                            }
                            if ((uint64_t)(var_37 + (-125)) != 0UL) {
                                var_41 = local_sp_3 + (-8L);
                                *(uint64_t *)var_41 = 4209168UL;
                                indirect_placeholder();
                                local_sp_5 = var_41;
                                var_44 = *var_7 + 1UL;
                                *var_7 = var_44;
                                var_11 = var_44;
                                local_sp_3 = local_sp_5;
                                continue;
                            }
                            if ((uint64_t)(var_37 + (-95)) != 0UL) {
                                var_39 = local_sp_3 + (-8L);
                                *(uint64_t *)var_39 = 4209202UL;
                                indirect_placeholder();
                                local_sp_5 = var_39;
                                var_44 = *var_7 + 1UL;
                                *var_7 = var_44;
                                var_11 = var_44;
                                local_sp_3 = local_sp_5;
                                continue;
                            }
                        }
                        if ((uint64_t)(var_37 + (-34)) != 0UL) {
                            *(uint64_t *)(local_sp_3 + (-8L)) = 4209112UL;
                            indirect_placeholder();
                            var_38 = local_sp_3 + (-16L);
                            *(uint64_t *)var_38 = 4209122UL;
                            indirect_placeholder();
                            local_sp_5 = var_38;
                            var_44 = *var_7 + 1UL;
                            *var_7 = var_44;
                            var_11 = var_44;
                            local_sp_3 = local_sp_5;
                            continue;
                        }
                        if (!((var_36 < '"') || (var_36 > '&'))) {
                            var_39 = local_sp_3 + (-8L);
                            *(uint64_t *)var_39 = 4209202UL;
                            indirect_placeholder();
                            local_sp_5 = var_39;
                            var_44 = *var_7 + 1UL;
                            *var_7 = var_44;
                            var_11 = var_44;
                            local_sp_3 = local_sp_5;
                            continue;
                        }
                        *(uint64_t *)(local_sp_3 + (-8L)) = 4209134UL;
                        indirect_placeholder();
                        var_40 = local_sp_3 + (-16L);
                        *(uint64_t *)var_40 = 4209145UL;
                        indirect_placeholder();
                        local_sp_5 = var_40;
                    }
                } else {
                    if (*(uint32_t *)6506052UL != 3U) {
                        var_15 = (uint32_t)*(unsigned char *)((uint64_t)*var_8 | 4384000UL);
                        *var_10 = var_15;
                        var_16 = *var_9;
                        if (var_16 > 9U) {
                            var_44 = *var_7 + 1UL;
                            *var_7 = var_44;
                            var_11 = var_44;
                            local_sp_3 = local_sp_5;
                            continue;
                        }
                        switch (*(uint64_t *)(((uint64_t)var_16 << 3UL) + 4381112UL)) {
                          case 4208883UL:
                            {
                                var_25 = local_sp_3 + (-8L);
                                *(uint64_t *)var_25 = 4208903UL;
                                indirect_placeholder();
                                local_sp_5 = var_25;
                            }
                            break;
                          case 4208699UL:
                            {
                                var_29 = local_sp_3 + (-8L);
                                *(uint64_t *)var_29 = 4208740UL;
                                indirect_placeholder();
                                local_sp_5 = var_29;
                            }
                            break;
                          case 4208837UL:
                            {
                                var_26 = local_sp_3 + (-8L);
                                *(uint64_t *)var_26 = 4208878UL;
                                indirect_placeholder();
                                local_sp_5 = var_26;
                            }
                            break;
                          case 4208791UL:
                            {
                                var_27 = local_sp_3 + (-8L);
                                *(uint64_t *)var_27 = 4208832UL;
                                indirect_placeholder();
                                local_sp_5 = var_27;
                            }
                            break;
                          case 4208653UL:
                            {
                                var_30 = local_sp_3 + (-8L);
                                *(uint64_t *)var_30 = 4208694UL;
                                indirect_placeholder();
                                local_sp_5 = var_30;
                            }
                            break;
                          case 4208745UL:
                            {
                                var_28 = local_sp_3 + (-8L);
                                *(uint64_t *)var_28 = 4208786UL;
                                indirect_placeholder();
                                local_sp_5 = var_28;
                            }
                            break;
                          case 4209204UL:
                            {
                                break;
                            }
                            break;
                          case 4208908UL:
                            {
                                if ((uint64_t)(var_15 + (-65)) == 0UL) {
                                    var_24 = local_sp_3 + (-8L);
                                    *(uint64_t *)var_24 = 4208963UL;
                                    indirect_placeholder();
                                    local_sp_5 = var_24;
                                } else {
                                    var_21 = ((uint64_t)(var_15 + (-97)) == 0UL);
                                    var_22 = local_sp_3 + (-8L);
                                    var_23 = (uint64_t *)var_22;
                                    local_sp_5 = var_22;
                                    if (var_21) {
                                        *var_23 = 4208941UL;
                                        indirect_placeholder();
                                    } else {
                                        *var_23 = 4208975UL;
                                        indirect_placeholder();
                                    }
                                }
                            }
                            break;
                          case 4208979UL:
                            {
                                if ((uint64_t)(var_15 + (-79)) == 0UL) {
                                    var_20 = local_sp_3 + (-8L);
                                    *(uint64_t *)var_20 = 4209034UL;
                                    indirect_placeholder();
                                    local_sp_5 = var_20;
                                } else {
                                    var_17 = ((uint64_t)(var_15 + (-111)) == 0UL);
                                    var_18 = local_sp_3 + (-8L);
                                    var_19 = (uint64_t *)var_18;
                                    local_sp_5 = var_18;
                                    if (var_17) {
                                        *var_19 = 4209012UL;
                                        indirect_placeholder();
                                    } else {
                                        *var_19 = 4209046UL;
                                        indirect_placeholder();
                                    }
                                }
                            }
                            break;
                          case 4208515UL:
                            {
                                if ((uint64_t)(var_15 + (-79)) == 0UL) {
                                    var_35 = local_sp_3 + (-8L);
                                    *(uint64_t *)var_35 = 4208587UL;
                                    indirect_placeholder();
                                    local_sp_5 = var_35;
                                } else {
                                    if ((int)var_15 > (int)79U) {
                                        if ((uint64_t)(var_15 + (-97)) == 0UL) {
                                            var_34 = local_sp_3 + (-8L);
                                            *(uint64_t *)var_34 = 4208609UL;
                                            indirect_placeholder();
                                            local_sp_5 = var_34;
                                        } else {
                                            if ((uint64_t)(var_15 + (-111)) == 0UL) {
                                                var_33 = local_sp_3 + (-8L);
                                                *(uint64_t *)var_33 = 4208565UL;
                                                indirect_placeholder();
                                                local_sp_5 = var_33;
                                            } else {
                                                var_32 = local_sp_3 + (-8L);
                                                *(uint64_t *)var_32 = 4208643UL;
                                                indirect_placeholder();
                                                local_sp_5 = var_32;
                                            }
                                        }
                                    } else {
                                        if ((uint64_t)(var_15 + (-65)) == 0UL) {
                                            var_31 = local_sp_3 + (-8L);
                                            *(uint64_t *)var_31 = 4208631UL;
                                            indirect_placeholder();
                                            local_sp_5 = var_31;
                                        } else {
                                            var_32 = local_sp_3 + (-8L);
                                            *(uint64_t *)var_32 = 4208643UL;
                                            indirect_placeholder();
                                            local_sp_5 = var_32;
                                        }
                                    }
                                }
                            }
                            break;
                          default:
                            {
                                abort();
                            }
                            break;
                        }
                    }
                }
            }
        }
}

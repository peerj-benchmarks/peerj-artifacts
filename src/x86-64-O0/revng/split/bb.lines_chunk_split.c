typedef unsigned char __u_char;
typedef unsigned short __u_short;
typedef unsigned int __u_int;
typedef unsigned long __u_long;
typedef signed char __int8_t;
typedef unsigned char __uint8_t;
typedef short __int16_t;
typedef unsigned short __uint16_t;
typedef int __int32_t;
typedef unsigned int __uint32_t;
typedef long __int64_t;
typedef unsigned long __uint64_t;
typedef __int8_t __int_least8_t;
typedef __uint8_t __uint_least8_t;
typedef __int16_t __int_least16_t;
typedef __uint16_t __uint_least16_t;
typedef __int32_t __int_least32_t;
typedef __uint32_t __uint_least32_t;
typedef __int64_t __int_least64_t;
typedef __uint64_t __uint_least64_t;
typedef long __quad_t;
typedef unsigned long __u_quad_t;
typedef long __intmax_t;
typedef unsigned long __uintmax_t;
typedef unsigned long __dev_t;
typedef unsigned int __uid_t;
typedef unsigned int __gid_t;
typedef unsigned long __ino_t;
typedef unsigned long __ino64_t;
typedef unsigned int __mode_t;
typedef unsigned long __nlink_t;
typedef long __off_t;
typedef long __off64_t;
typedef int __pid_t;
typedef struct {
    int __val[2];
} __fsid_t;
typedef long __clock_t;
typedef unsigned long __rlim_t;
typedef unsigned long __rlim64_t;
typedef unsigned int __id_t;
typedef long __time_t;
typedef unsigned int __useconds_t;
typedef long __suseconds_t;
typedef int __daddr_t;
typedef int __key_t;
typedef int __clockid_t;
typedef void *__timer_t;
typedef long __blksize_t;
typedef long __blkcnt_t;
typedef long __blkcnt64_t;
typedef unsigned long __fsblkcnt_t;
typedef unsigned long __fsblkcnt64_t;
typedef unsigned long __fsfilcnt_t;
typedef unsigned long __fsfilcnt64_t;
typedef long __fsword_t;
typedef long __ssize_t;
typedef long __syscall_slong_t;
typedef unsigned long __syscall_ulong_t;
typedef __off64_t __loff_t;
typedef char *__caddr_t;
typedef long __intptr_t;
typedef unsigned int __socklen_t;
typedef int __sig_atomic_t;
typedef __int8_t int8_t;
typedef __int16_t int16_t;
typedef __int32_t int32_t;
typedef __int64_t int64_t;
typedef __uint8_t uint8_t;
typedef __uint16_t uint16_t;
typedef __uint32_t uint32_t;
typedef __uint64_t uint64_t;
typedef __int_least8_t int_least8_t;
typedef __int_least16_t int_least16_t;
typedef __int_least32_t int_least32_t;
typedef __int_least64_t int_least64_t;
typedef __uint_least8_t uint_least8_t;
typedef __uint_least16_t uint_least16_t;
typedef __uint_least32_t uint_least32_t;
typedef __uint_least64_t uint_least64_t;
typedef signed char int_fast8_t;
typedef long int_fast16_t;
typedef long int_fast32_t;
typedef long int_fast64_t;
typedef unsigned char uint_fast8_t;
typedef unsigned long uint_fast16_t;
typedef unsigned long uint_fast32_t;
typedef unsigned long uint_fast64_t;
typedef long intptr_t;
typedef unsigned long uintptr_t;
typedef __intmax_t intmax_t;
typedef __uintmax_t uintmax_t;
struct helper_divq_EAX_wrapper_ret_type;
struct type_5;
struct indirect_placeholder_74_ret_type;
struct indirect_placeholder_75_ret_type;
struct indirect_placeholder_76_ret_type;
struct indirect_placeholder_77_ret_type;
struct indirect_placeholder_78_ret_type;
struct indirect_placeholder_79_ret_type;
struct helper_divq_EAX_wrapper_ret_type {
    uint64_t field_0;
    uint64_t field_1;
    uint64_t field_2;
    uint32_t field_3;
    uint64_t field_4;
    uint32_t field_5;
    uint64_t field_6;
    uint32_t field_7;
    uint64_t field_8;
    uint32_t field_9;
    unsigned char field_10;
    uint32_t field_11;
    uint32_t field_12;
};
struct type_5 {
};
struct indirect_placeholder_74_ret_type {
    uint64_t field_0;
    uint64_t field_1;
    uint64_t field_2;
    uint64_t field_3;
    uint64_t field_4;
};
struct indirect_placeholder_75_ret_type {
    uint64_t field_0;
    uint64_t field_1;
    uint64_t field_2;
};
struct indirect_placeholder_76_ret_type {
    uint64_t field_0;
    uint64_t field_1;
    uint64_t field_2;
};
struct indirect_placeholder_77_ret_type {
    uint64_t field_0;
    uint64_t field_1;
    uint64_t field_2;
    uint64_t field_3;
    uint64_t field_4;
};
struct indirect_placeholder_78_ret_type {
    uint64_t field_0;
    uint64_t field_1;
    uint64_t field_2;
    uint64_t field_3;
    uint64_t field_4;
};
struct indirect_placeholder_79_ret_type {
    uint64_t field_0;
    uint64_t field_1;
    uint64_t field_2;
};
extern uint64_t revng_init_local_sp(uint64_t param_0);
extern uint64_t init_rbp(void);
extern uint64_t indirect_placeholder_4(uint64_t param_0, uint64_t param_1);
extern uint64_t init_cc_src2(void);
extern uint64_t helper_cc_compute_c_wrapper(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint32_t param_3);
extern void indirect_placeholder(void);
extern uint64_t indirect_placeholder_8(uint64_t param_0, uint64_t param_1, uint64_t param_2);
extern uint64_t init_rbx(void);
extern uint32_t init_state_0x8248(void);
extern uint64_t init_state_0x9018(void);
extern uint32_t init_state_0x9010(void);
extern uint64_t init_state_0x8408(void);
extern uint64_t init_state_0x8328(void);
extern uint64_t init_state_0x82d8(void);
extern uint32_t init_state_0x9080(void);
extern struct helper_divq_EAX_wrapper_ret_type helper_divq_EAX_wrapper(struct type_5 *param_0, uint64_t param_1, uint64_t param_2, uint64_t param_3, uint64_t param_4, uint64_t param_5, uint64_t param_6, uint64_t param_7, uint64_t param_8, uint64_t param_9, uint64_t param_10, uint64_t param_11, uint64_t param_12, uint32_t param_13, uint64_t param_14, uint32_t param_15, uint64_t param_16, uint64_t param_17, uint64_t param_18, uint32_t param_19);
extern struct indirect_placeholder_74_ret_type indirect_placeholder_74(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint64_t param_3, uint64_t param_4, uint64_t param_5, uint64_t param_6);
extern struct indirect_placeholder_75_ret_type indirect_placeholder_75(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint64_t param_3, uint64_t param_4);
extern uint64_t helper_cc_compute_all_wrapper(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint32_t param_3);
extern struct indirect_placeholder_76_ret_type indirect_placeholder_76(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint64_t param_3, uint64_t param_4);
extern struct indirect_placeholder_77_ret_type indirect_placeholder_77(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint64_t param_3, uint64_t param_4, uint64_t param_5, uint64_t param_6);
extern struct indirect_placeholder_78_ret_type indirect_placeholder_78(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint64_t param_3, uint64_t param_4, uint64_t param_5, uint64_t param_6);
extern struct indirect_placeholder_79_ret_type indirect_placeholder_79(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint64_t param_3, uint64_t param_4);
typedef _Bool bool;
void bb_lines_chunk_split(uint64_t rdx, uint64_t rcx, uint64_t rdi, uint64_t rsi, uint64_t r10, uint64_t r9, uint64_t r8) {
    struct indirect_placeholder_79_ret_type var_105;
    struct indirect_placeholder_76_ret_type var_102;
    struct indirect_placeholder_75_ret_type var_91;
    struct indirect_placeholder_74_ret_type var_83;
    uint64_t var_86;
    uint64_t var_0;
    uint64_t var_1;
    uint64_t var_2;
    uint64_t var_3;
    uint32_t var_4;
    uint64_t var_5;
    uint32_t var_6;
    uint64_t var_7;
    uint64_t var_8;
    uint64_t var_9;
    uint32_t var_10;
    uint64_t var_11;
    uint64_t var_12;
    uint64_t *var_13;
    uint64_t *var_14;
    uint64_t *var_15;
    uint64_t *var_16;
    uint64_t *var_17;
    uint64_t *var_18;
    uint64_t var_19;
    uint64_t var_81;
    uint64_t var_82;
    uint64_t r87_10;
    uint64_t local_sp_11;
    uint64_t var_84;
    uint64_t var_85;
    uint64_t _pre197;
    uint64_t local_sp_1;
    uint64_t var_75;
    uint64_t var_74;
    uint64_t var_76;
    uint64_t var_77;
    uint64_t var_78;
    uint64_t var_79;
    uint64_t var_80;
    uint64_t r96_1;
    uint64_t r96_0;
    uint64_t r87_0;
    uint64_t local_sp_0;
    uint64_t var_87;
    uint64_t var_88;
    uint64_t var_89;
    uint64_t var_90;
    uint64_t r87_1;
    uint64_t r96_2_ph;
    uint64_t r87_2_ph;
    uint64_t local_sp_2_ph;
    unsigned char var_92;
    uint64_t var_64;
    uint64_t var_93;
    uint64_t var_94;
    uint64_t var_95;
    uint64_t r96_10;
    uint64_t var_96;
    uint64_t var_97;
    uint64_t var_99;
    uint64_t var_100;
    uint64_t var_98;
    uint64_t var_101;
    uint64_t var_56;
    uint64_t var_57;
    uint64_t var_58;
    uint64_t var_59;
    uint64_t r87_6;
    uint64_t local_sp_7;
    struct indirect_placeholder_77_ret_type var_60;
    uint64_t r96_7;
    uint64_t var_39;
    uint64_t var_40;
    uint64_t var_54;
    uint64_t var_41;
    uint64_t var_42;
    struct indirect_placeholder_78_ret_type var_43;
    uint64_t r96_3;
    uint64_t r87_3;
    uint64_t local_sp_3;
    uint64_t r96_4;
    uint64_t r87_4;
    uint64_t local_sp_4;
    uint64_t var_44;
    uint64_t r96_5;
    uint64_t var_20;
    uint64_t rcx2_0;
    uint64_t rdi3_0;
    uint64_t rsi4_0;
    uint64_t local_sp_5;
    uint64_t var_23;
    struct helper_divq_EAX_wrapper_ret_type var_22;
    uint64_t *var_24;
    uint64_t *var_25;
    uint64_t var_26;
    uint64_t *var_27;
    uint64_t *var_28;
    unsigned char *var_29;
    unsigned char *var_30;
    uint64_t var_31;
    uint64_t var_32;
    uint64_t *var_33;
    uint64_t var_34;
    uint64_t var_35;
    uint64_t var_36;
    uint64_t var_37;
    uint64_t var_38;
    uint64_t r87_5;
    uint64_t local_sp_6;
    uint64_t *var_45;
    uint64_t *var_46;
    uint64_t *var_47;
    unsigned char *var_48;
    uint64_t *var_49;
    uint64_t *var_50;
    uint64_t *var_51;
    uint64_t r96_6;
    uint64_t r96_8;
    uint64_t var_52;
    uint64_t r87_7;
    uint64_t local_sp_8;
    uint64_t var_61;
    uint64_t r87_8;
    uint64_t local_sp_9;
    uint64_t r96_9;
    uint64_t r87_9;
    uint64_t local_sp_10;
    uint64_t var_103;
    uint64_t var_104;
    uint64_t var_62;
    uint64_t var_63;
    uint64_t var_65;
    uint64_t var_66;
    uint64_t var_67;
    uint64_t var_68;
    uint64_t var_69;
    uint64_t var_70;
    uint64_t var_71;
    uint64_t var_72;
    uint64_t var_73;
    uint64_t var_53;
    uint64_t var_55;
    uint64_t var_21;
    unsigned int loop_state_var;
    bool switch_state_var;
    var_0 = revng_init_local_sp(0UL);
    var_1 = init_rbp();
    var_2 = init_cc_src2();
    var_3 = init_rbx();
    var_4 = init_state_0x8248();
    var_5 = init_state_0x9018();
    var_6 = init_state_0x9010();
    var_7 = init_state_0x8408();
    var_8 = init_state_0x8328();
    var_9 = init_state_0x82d8();
    var_10 = init_state_0x9080();
    var_11 = var_0 + (-8L);
    *(uint64_t *)var_11 = var_1;
    *(uint64_t *)(var_0 + (-16L)) = var_3;
    var_12 = var_0 + (-184L);
    var_13 = (uint64_t *)(var_0 + (-144L));
    *var_13 = rdi;
    var_14 = (uint64_t *)(var_0 + (-152L));
    *var_14 = rsi;
    var_15 = (uint64_t *)(var_0 + (-160L));
    *var_15 = rdx;
    var_16 = (uint64_t *)(var_0 + (-168L));
    *var_16 = rcx;
    var_17 = (uint64_t *)(var_0 + (-176L));
    *var_17 = r8;
    var_18 = (uint64_t *)var_12;
    *var_18 = r9;
    var_19 = *var_14;
    r96_3 = r9;
    r87_3 = r8;
    r96_4 = r9;
    r87_4 = r8;
    r96_5 = r9;
    rcx2_0 = 4291264UL;
    rdi3_0 = 4290331UL;
    rsi4_0 = 4289746UL;
    local_sp_5 = var_12;
    r87_5 = r8;
    if (var_19 == 0UL) {
        var_21 = var_0 + (-192L);
        *(uint64_t *)var_21 = 4209881UL;
        indirect_placeholder();
        local_sp_5 = var_21;
    }
    var_22 = helper_divq_EAX_wrapper((struct type_5 *)(0UL), *var_14, 4209896UL, *var_18, var_11, 0UL, rcx2_0, rdi3_0, var_3, rsi4_0, r10, r9, r8, var_4, var_5, var_6, var_7, var_8, var_9, var_10);
    var_23 = var_22.field_1;
    var_24 = (uint64_t *)(var_0 + (-96L));
    *var_24 = var_23;
    var_25 = (uint64_t *)(var_0 + (-32L));
    *var_25 = 1UL;
    var_26 = *var_24 + (-1L);
    var_27 = (uint64_t *)(var_0 + (-40L));
    *var_27 = var_26;
    var_28 = (uint64_t *)(var_0 + (-48L));
    *var_28 = 0UL;
    var_29 = (unsigned char *)(var_0 + (-49L));
    *var_29 = (unsigned char)'\x01';
    var_30 = (unsigned char *)(var_0 + (-50L));
    *var_30 = (unsigned char)'\x00';
    var_31 = *var_13;
    local_sp_6 = local_sp_5;
    if (var_31 <= 1UL) {
        var_32 = ((var_31 + (-1L)) * *var_24) + (-1L);
        var_33 = (uint64_t *)(var_0 + (-104L));
        *var_33 = var_32;
        var_34 = *var_17;
        var_35 = helper_cc_compute_c_wrapper(var_32 - var_34, var_34, var_2, 17U);
        if (var_35 == 0UL) {
            var_36 = local_sp_5 + (-8L);
            *(uint64_t *)var_36 = 4210043UL;
            indirect_placeholder();
            *var_17 = (*var_17 - *var_33);
            local_sp_4 = var_36;
        } else {
            var_37 = *var_33 - *var_17;
            var_38 = local_sp_5 + (-8L);
            *(uint64_t *)var_38 = 4210085UL;
            indirect_placeholder();
            local_sp_3 = var_38;
            if ((long)var_37 <= (long)18446744073709551615UL) {
                var_39 = *(uint64_t *)6404464UL;
                *(uint64_t *)(local_sp_5 + (-16L)) = 4210115UL;
                var_40 = indirect_placeholder_8(var_39, 0UL, 3UL);
                *(uint64_t *)(local_sp_5 + (-24L)) = 4210123UL;
                indirect_placeholder();
                var_41 = (uint64_t)*(uint32_t *)var_40;
                var_42 = local_sp_5 + (-32L);
                *(uint64_t *)var_42 = 4210150UL;
                var_43 = indirect_placeholder_78(0UL, 4290173UL, var_40, 1UL, var_41, r9, r8);
                r96_3 = var_43.field_3;
                r87_3 = var_43.field_4;
                local_sp_3 = var_42;
            }
            *var_17 = 18446744073709551615UL;
            r96_4 = r96_3;
            r87_4 = r87_3;
            local_sp_4 = local_sp_3;
        }
        *var_28 = *var_33;
        var_44 = *var_13 + (-1L);
        *var_25 = var_44;
        *var_27 = ((var_44 * *var_24) + (-1L));
        r96_5 = r96_4;
        r87_5 = r87_4;
        local_sp_6 = local_sp_4;
    }
    var_45 = (uint64_t *)(var_0 + (-64L));
    var_46 = (uint64_t *)(var_0 + (-72L));
    var_47 = (uint64_t *)(var_0 + (-112L));
    var_48 = (unsigned char *)(var_0 + (-73L));
    var_49 = (uint64_t *)(var_0 + (-120L));
    var_50 = (uint64_t *)(var_0 + (-88L));
    var_51 = (uint64_t *)(var_0 + (-128L));
    r96_6 = r96_5;
    r87_6 = r87_5;
    local_sp_7 = local_sp_6;
    while (1U)
        {
            r96_7 = r96_6;
            r96_8 = r96_6;
            r87_7 = r87_6;
            local_sp_8 = local_sp_7;
            r87_8 = r87_6;
            local_sp_9 = local_sp_7;
            if ((long)*var_28 >= (long)*var_18) {
                loop_state_var = 1U;
                break;
            }
            *var_45 = *var_15;
            var_52 = *var_17;
            if (var_52 == 18446744073709551615UL) {
                *var_46 = var_52;
                *var_17 = 18446744073709551615UL;
            } else {
                var_53 = *var_16;
                var_54 = local_sp_7 + (-8L);
                *(uint64_t *)var_54 = 4210278UL;
                var_55 = indirect_placeholder_4(var_53, 0UL);
                *var_46 = var_55;
                local_sp_8 = var_54;
                if (var_55 == 18446744073709551615UL) {
                    var_56 = *(uint64_t *)6404464UL;
                    *(uint64_t *)(local_sp_7 + (-16L)) = 4210314UL;
                    var_57 = indirect_placeholder_8(var_56, 0UL, 3UL);
                    *(uint64_t *)(local_sp_7 + (-24L)) = 4210322UL;
                    indirect_placeholder();
                    var_58 = (uint64_t)*(uint32_t *)var_57;
                    var_59 = local_sp_7 + (-32L);
                    *(uint64_t *)var_59 = 4210349UL;
                    var_60 = indirect_placeholder_77(0UL, 4290173UL, var_57, 1UL, var_58, r96_6, r87_6);
                    r96_7 = var_60.field_3;
                    r87_7 = var_60.field_4;
                    local_sp_8 = var_59;
                }
            }
            var_61 = *var_46;
            r87_10 = r87_7;
            local_sp_11 = local_sp_8;
            r96_10 = r96_7;
            r96_8 = r96_7;
            r87_8 = r87_7;
            local_sp_9 = local_sp_8;
            if (var_61 != 0UL) {
                loop_state_var = 1U;
                break;
            }
            var_62 = *var_18 - *var_28;
            *var_46 = ((var_62 > var_61) ? var_61 : var_62);
            *var_30 = (unsigned char)'\x00';
            var_63 = *var_46 + *var_15;
            *var_47 = var_63;
            var_64 = var_63;
            while (1U)
                {
                    r96_1 = r96_10;
                    r96_0 = r96_10;
                    r87_0 = r87_10;
                    r87_1 = r87_10;
                    r87_6 = r87_10;
                    local_sp_7 = local_sp_11;
                    r96_6 = r96_10;
                    if (*var_45 != var_64) {
                        loop_state_var = 0U;
                        break;
                    }
                    *var_48 = (unsigned char)'\x00';
                    var_65 = *var_27 - *var_28;
                    var_66 = ((long)var_65 > (long)0UL) ? var_65 : 0UL;
                    var_67 = *var_46;
                    var_68 = (var_66 > var_67) ? var_67 : var_66;
                    *var_49 = var_68;
                    var_69 = *var_46 - var_68;
                    var_70 = (uint64_t)*(uint32_t *)6403220UL;
                    var_71 = *var_45 + var_68;
                    var_72 = local_sp_11 + (-8L);
                    *(uint64_t *)var_72 = 4210497UL;
                    var_73 = indirect_placeholder_8(var_69, var_71, var_70);
                    *var_50 = (var_73 + 1UL);
                    local_sp_0 = var_72;
                    if (var_73 == 0UL) {
                        var_74 = *var_47;
                        *var_50 = var_74;
                        var_75 = var_74;
                    } else {
                        *var_48 = (unsigned char)'\x01';
                        var_75 = *var_50;
                    }
                    var_76 = var_75 - *var_45;
                    *var_51 = var_76;
                    var_77 = *var_13;
                    var_86 = var_77;
                    if (var_77 == *var_25) {
                        var_78 = *var_45;
                        var_79 = local_sp_11 + (-16L);
                        *(uint64_t *)var_79 = 4210584UL;
                        var_80 = indirect_placeholder_8(var_76, 1UL, var_78);
                        local_sp_1 = var_79;
                        if (var_80 != *var_51) {
                            *(uint64_t *)(local_sp_11 + (-24L)) = 4210595UL;
                            indirect_placeholder();
                            var_81 = (uint64_t)*(uint32_t *)var_80;
                            var_82 = local_sp_11 + (-32L);
                            *(uint64_t *)var_82 = 4210624UL;
                            var_83 = indirect_placeholder_74(0UL, 4290173UL, 4290361UL, 1UL, var_81, r96_10, r87_10);
                            var_84 = var_83.field_3;
                            var_85 = var_83.field_4;
                            _pre197 = *var_13;
                            var_86 = _pre197;
                            r96_0 = var_84;
                            r87_0 = var_85;
                            local_sp_0 = var_82;
                            r96_1 = r96_0;
                            r87_1 = r87_0;
                            local_sp_1 = local_sp_0;
                            if (var_86 == 0UL) {
                                var_87 = (uint64_t)*var_29;
                                var_88 = *var_51;
                                var_89 = *var_45;
                                var_90 = local_sp_0 + (-8L);
                                *(uint64_t *)var_90 = 4210656UL;
                                var_91 = indirect_placeholder_75(var_88, var_87, var_89, r96_0, r87_0);
                                r96_1 = var_91.field_1;
                                r87_1 = var_91.field_2;
                                local_sp_1 = var_90;
                            }
                        }
                    } else {
                        r96_1 = r96_0;
                        r87_1 = r87_0;
                        local_sp_1 = local_sp_0;
                        if (var_86 == 0UL) {
                            var_87 = (uint64_t)*var_29;
                            var_88 = *var_51;
                            var_89 = *var_45;
                            var_90 = local_sp_0 + (-8L);
                            *(uint64_t *)var_90 = 4210656UL;
                            var_91 = indirect_placeholder_75(var_88, var_87, var_89, r96_0, r87_0);
                            r96_1 = var_91.field_1;
                            r87_1 = var_91.field_2;
                            local_sp_1 = var_90;
                        }
                    }
                    *var_28 = (*var_51 + *var_28);
                    *var_45 = (*var_45 + *var_51);
                    *var_46 = (*var_46 - *var_51);
                    *var_29 = *var_48;
                    r96_2_ph = r96_1;
                    r87_2_ph = r87_1;
                    local_sp_2_ph = local_sp_1;
                    while (1U)
                        {
                            r96_10 = r96_2_ph;
                            r87_10 = r87_2_ph;
                            local_sp_11 = local_sp_2_ph;
                            while (1U)
                                {
                                    var_92 = *var_48;
                                    if (var_92 != '\x00') {
                                        if ((long)(*var_28 + (-1L)) >= (long)*var_27) {
                                            loop_state_var = 1U;
                                            break;
                                        }
                                    }
                                    if (var_92 != '\x01') {
                                        var_93 = *var_45;
                                        var_94 = *var_47;
                                        var_95 = helper_cc_compute_all_wrapper(var_93 - var_94, var_94, var_2, 17U);
                                        if ((var_95 & 64UL) != 0UL) {
                                            loop_state_var = 0U;
                                            break;
                                        }
                                    }
                                    var_96 = *var_25 + 1UL;
                                    *var_25 = var_96;
                                    var_97 = *var_13;
                                    if (!((var_97 != 0UL) && (var_96 > var_97))) {
                                        loop_state_var = 2U;
                                        break;
                                    }
                                    if (var_96 == *var_14) {
                                        var_99 = *var_18 + (-1L);
                                        *var_27 = var_99;
                                        var_100 = var_99;
                                    } else {
                                        var_98 = *var_27 + *var_24;
                                        *var_27 = var_98;
                                        var_100 = var_98;
                                    }
                                    if ((long)(*var_28 + (-1L)) < (long)var_100) {
                                        *var_48 = (unsigned char)'\x00';
                                        continue;
                                    }
                                    if (*var_13 == 0UL) {
                                        continue;
                                    }
                                    loop_state_var = 3U;
                                    break;
                                }
                            switch_state_var = 0;
                            switch (loop_state_var) {
                              case 3U:
                                {
                                    var_101 = local_sp_2_ph + (-8L);
                                    *(uint64_t *)var_101 = 4210844UL;
                                    var_102 = indirect_placeholder_76(0UL, 1UL, 0UL, r96_2_ph, r87_2_ph);
                                    r96_2_ph = var_102.field_1;
                                    r87_2_ph = var_102.field_2;
                                    local_sp_2_ph = var_101;
                                    continue;
                                }
                                break;
                              case 0U:
                                {
                                    loop_state_var = 0U;
                                    switch_state_var = 1;
                                    break;
                                }
                                break;
                              case 1U:
                                {
                                    loop_state_var = 1U;
                                    switch_state_var = 1;
                                    break;
                                }
                                break;
                              case 2U:
                                {
                                    loop_state_var = 2U;
                                    switch_state_var = 1;
                                    break;
                                }
                                break;
                            }
                            if (switch_state_var)
                                break;
                        }
                    switch_state_var = 0;
                    switch (loop_state_var) {
                      case 2U:
                        {
                            loop_state_var = 1U;
                            switch_state_var = 1;
                            break;
                        }
                        break;
                      case 1U:
                      case 0U:
                        {
                            switch (loop_state_var) {
                              case 0U:
                                {
                                    *var_30 = (unsigned char)'\x01';
                                }
                                break;
                              case 1U:
                                {
                                    var_64 = *var_47;
                                    continue;
                                }
                                break;
                            }
                        }
                        break;
                    }
                    if (switch_state_var)
                        break;
                }
            switch_state_var = 0;
            switch (loop_state_var) {
              case 0U:
                {
                    continue;
                }
                break;
              case 1U:
                {
                    loop_state_var = 0U;
                    switch_state_var = 1;
                    break;
                }
                break;
            }
            if (switch_state_var)
                break;
        }
    switch (loop_state_var) {
      case 0U:
        {
            return;
        }
        break;
      case 1U:
        {
            r96_9 = r96_8;
            r87_9 = r87_8;
            local_sp_10 = local_sp_9;
            if (*var_30 == '\x00') {
                *var_25 = (*var_25 + 1UL);
            }
            while (*var_13 != 0UL)
                {
                    var_104 = local_sp_10 + (-8L);
                    *(uint64_t *)var_104 = 4210942UL;
                    var_105 = indirect_placeholder_79(0UL, 1UL, 0UL, r96_9, r87_9);
                    r96_9 = var_105.field_1;
                    r87_9 = var_105.field_2;
                    local_sp_10 = var_104;
                }
        }
        break;
    }
}

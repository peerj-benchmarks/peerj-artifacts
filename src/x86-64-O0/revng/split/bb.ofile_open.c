typedef unsigned char __u_char;
typedef unsigned short __u_short;
typedef unsigned int __u_int;
typedef unsigned long __u_long;
typedef signed char __int8_t;
typedef unsigned char __uint8_t;
typedef short __int16_t;
typedef unsigned short __uint16_t;
typedef int __int32_t;
typedef unsigned int __uint32_t;
typedef long __int64_t;
typedef unsigned long __uint64_t;
typedef __int8_t __int_least8_t;
typedef __uint8_t __uint_least8_t;
typedef __int16_t __int_least16_t;
typedef __uint16_t __uint_least16_t;
typedef __int32_t __int_least32_t;
typedef __uint32_t __uint_least32_t;
typedef __int64_t __int_least64_t;
typedef __uint64_t __uint_least64_t;
typedef long __quad_t;
typedef unsigned long __u_quad_t;
typedef long __intmax_t;
typedef unsigned long __uintmax_t;
typedef unsigned long __dev_t;
typedef unsigned int __uid_t;
typedef unsigned int __gid_t;
typedef unsigned long __ino_t;
typedef unsigned long __ino64_t;
typedef unsigned int __mode_t;
typedef unsigned long __nlink_t;
typedef long __off_t;
typedef long __off64_t;
typedef int __pid_t;
typedef struct {
    int __val[2];
} __fsid_t;
typedef long __clock_t;
typedef unsigned long __rlim_t;
typedef unsigned long __rlim64_t;
typedef unsigned int __id_t;
typedef long __time_t;
typedef unsigned int __useconds_t;
typedef long __suseconds_t;
typedef int __daddr_t;
typedef int __key_t;
typedef int __clockid_t;
typedef void *__timer_t;
typedef long __blksize_t;
typedef long __blkcnt_t;
typedef long __blkcnt64_t;
typedef unsigned long __fsblkcnt_t;
typedef unsigned long __fsblkcnt64_t;
typedef unsigned long __fsfilcnt_t;
typedef unsigned long __fsfilcnt64_t;
typedef long __fsword_t;
typedef long __ssize_t;
typedef long __syscall_slong_t;
typedef unsigned long __syscall_ulong_t;
typedef __off64_t __loff_t;
typedef char *__caddr_t;
typedef long __intptr_t;
typedef unsigned int __socklen_t;
typedef int __sig_atomic_t;
typedef __int8_t int8_t;
typedef __int16_t int16_t;
typedef __int32_t int32_t;
typedef __int64_t int64_t;
typedef __uint8_t uint8_t;
typedef __uint16_t uint16_t;
typedef __uint32_t uint32_t;
typedef __uint64_t uint64_t;
typedef __int_least8_t int_least8_t;
typedef __int_least16_t int_least16_t;
typedef __int_least32_t int_least32_t;
typedef __int_least64_t int_least64_t;
typedef __uint_least8_t uint_least8_t;
typedef __uint_least16_t uint_least16_t;
typedef __uint_least32_t uint_least32_t;
typedef __uint_least64_t uint_least64_t;
typedef signed char int_fast8_t;
typedef long int_fast16_t;
typedef long int_fast32_t;
typedef long int_fast64_t;
typedef unsigned char uint_fast8_t;
typedef unsigned long uint_fast16_t;
typedef unsigned long uint_fast32_t;
typedef unsigned long uint_fast64_t;
typedef long intptr_t;
typedef unsigned long uintptr_t;
typedef __intmax_t intmax_t;
typedef __uintmax_t uintmax_t;
typedef _Bool bool;
struct indirect_placeholder_84_ret_type;
struct indirect_placeholder_86_ret_type;
struct indirect_placeholder_87_ret_type;
struct indirect_placeholder_85_ret_type;
struct indirect_placeholder_83_ret_type;
struct indirect_placeholder_84_ret_type {
    uint64_t field_0;
    uint64_t field_1;
    uint64_t field_2;
    uint64_t field_3;
    uint64_t field_4;
};
struct indirect_placeholder_86_ret_type {
    uint64_t field_0;
    uint64_t field_1;
    uint64_t field_2;
    uint64_t field_3;
    uint64_t field_4;
};
struct indirect_placeholder_87_ret_type {
    uint64_t field_0;
    uint64_t field_1;
};
struct indirect_placeholder_85_ret_type {
    uint64_t field_0;
    uint64_t field_1;
    uint64_t field_2;
    uint64_t field_3;
    uint64_t field_4;
};
struct indirect_placeholder_83_ret_type {
    uint64_t field_0;
    uint64_t field_1;
    uint64_t field_2;
    uint64_t field_3;
    uint64_t field_4;
};
extern uint64_t revng_init_local_sp(uint64_t param_0);
extern uint64_t init_rbp(void);
extern void indirect_placeholder(void);
extern uint64_t indirect_placeholder_8(uint64_t param_0, uint64_t param_1, uint64_t param_2);
extern uint64_t init_rbx(void);
extern uint64_t indirect_placeholder_23(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint64_t param_3);
extern uint64_t indirect_placeholder_88(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint64_t param_3, uint64_t param_4, uint64_t param_5, uint64_t param_6);
extern struct indirect_placeholder_84_ret_type indirect_placeholder_84(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint64_t param_3, uint64_t param_4, uint64_t param_5, uint64_t param_6);
extern struct indirect_placeholder_86_ret_type indirect_placeholder_86(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint64_t param_3, uint64_t param_4, uint64_t param_5, uint64_t param_6);
extern struct indirect_placeholder_87_ret_type indirect_placeholder_87(void);
extern struct indirect_placeholder_85_ret_type indirect_placeholder_85(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint64_t param_3, uint64_t param_4, uint64_t param_5, uint64_t param_6);
extern struct indirect_placeholder_83_ret_type indirect_placeholder_83(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint64_t param_3, uint64_t param_4, uint64_t param_5, uint64_t param_6);
uint64_t bb_ofile_open(uint64_t rdx, uint64_t rcx, uint64_t rdi, uint64_t rsi, uint64_t r9, uint64_t r8) {
    struct indirect_placeholder_84_ret_type var_45;
    uint64_t local_sp_0;
    uint64_t var_0;
    uint64_t var_1;
    uint64_t var_2;
    uint64_t var_3;
    uint64_t var_4;
    uint64_t *var_5;
    uint64_t *var_6;
    uint64_t var_7;
    unsigned char *var_8;
    uint64_t var_9;
    unsigned char var_51;
    uint64_t r95_2_ph;
    uint64_t var_41;
    uint64_t var_42;
    uint64_t var_43;
    uint64_t var_44;
    uint64_t r86_2_ph;
    uint64_t r86_1;
    uint64_t rcx2_0;
    uint64_t r95_0;
    uint64_t r86_0;
    uint64_t local_sp_4;
    uint64_t var_33;
    uint64_t var_34;
    uint64_t var_35;
    uint64_t var_36;
    uint64_t r86_3;
    struct indirect_placeholder_86_ret_type var_37;
    uint64_t var_32;
    uint64_t local_sp_1;
    uint64_t r95_1;
    uint64_t storemerge;
    uint64_t var_10;
    uint64_t local_sp_2_ph;
    uint64_t var_38;
    uint64_t var_39;
    struct indirect_placeholder_87_ret_type var_40;
    uint64_t storemerge6;
    uint64_t var_46;
    uint64_t var_47;
    uint64_t var_48;
    uint64_t var_49;
    struct indirect_placeholder_85_ret_type var_50;
    uint64_t var_26;
    uint64_t var_27;
    uint64_t var_28;
    uint32_t var_23;
    uint64_t rax_0;
    uint64_t var_24;
    uint64_t var_25;
    uint64_t var_29;
    uint32_t var_30;
    uint64_t var_31;
    uint64_t *var_11;
    uint32_t *var_12;
    uint64_t rcx2_1;
    uint64_t r95_3;
    uint64_t var_13;
    uint64_t var_14;
    bool var_15;
    uint64_t var_16;
    uint64_t var_17;
    uint64_t *var_18;
    uint64_t var_21;
    uint32_t var_22;
    uint64_t var_19;
    uint32_t var_20;
    unsigned int loop_state_var;
    bool switch_state_var;
    var_0 = revng_init_local_sp(0UL);
    var_1 = init_rbp();
    var_2 = init_rbx();
    var_3 = var_0 + (-8L);
    *(uint64_t *)var_3 = var_1;
    *(uint64_t *)(var_0 + (-16L)) = var_2;
    var_4 = var_0 + (-72L);
    var_5 = (uint64_t *)(var_0 + (-48L));
    *var_5 = rdi;
    var_6 = (uint64_t *)(var_0 + (-56L));
    *var_6 = rsi;
    var_7 = var_0 + (-64L);
    *(uint64_t *)var_7 = rdx;
    var_8 = (unsigned char *)(var_0 + (-25L));
    *var_8 = (unsigned char)'\x00';
    var_9 = *var_6;
    var_51 = (unsigned char)'\x00';
    local_sp_4 = var_4;
    r86_3 = r8;
    rcx2_1 = rcx;
    r95_3 = r9;
    if ((int)*(uint32_t *)((*var_5 + (var_9 << 5UL)) + 8UL) > (int)4294967295U) {
        return (uint64_t)var_51;
    }
    storemerge = *(uint64_t *)(((var_9 == 0UL) ? var_3 : var_0) + (-56L)) + (-1L);
    var_10 = var_0 + (-40L);
    var_11 = (uint64_t *)var_10;
    *var_11 = storemerge;
    var_12 = (uint32_t *)(var_0 + (-32L));
    while (1U)
        {
            var_13 = *var_6 << 5UL;
            var_14 = *var_5 + var_13;
            var_15 = (*(uint32_t *)(var_14 + 8UL) == 4294967295U);
            var_16 = *(uint64_t *)var_14;
            var_17 = local_sp_4 + (-8L);
            var_18 = (uint64_t *)var_17;
            r95_1 = r95_3;
            r86_1 = r86_3;
            if (var_15) {
                *var_18 = 4211734UL;
                var_21 = indirect_placeholder_23(rcx2_1, var_16, r95_3, r86_3);
                var_22 = (uint32_t)var_21;
                *var_12 = var_22;
                var_23 = var_22;
                rax_0 = var_21;
            } else {
                *var_18 = 4211778UL;
                var_19 = indirect_placeholder_88(0UL, var_13, rcx2_1, var_16, 3073UL, r95_3, r86_3);
                var_20 = (uint32_t)var_19;
                *var_12 = var_20;
                var_23 = var_20;
                rax_0 = var_19;
            }
            if ((int)var_23 < (int)0U) {
                break;
            }
            var_29 = var_17 + (-8L);
            *(uint64_t *)var_29 = 4211873UL;
            indirect_placeholder();
            var_30 = *(uint32_t *)rax_0;
            var_31 = (uint64_t)var_30;
            local_sp_1 = var_29;
            var_32 = var_17 + (-16L);
            *(uint64_t *)var_32 = 4211885UL;
            indirect_placeholder();
            local_sp_1 = var_32;
            if ((uint64_t)(var_30 + (-24)) != 0UL & *(uint32_t *)var_31 == 23U) {
                var_33 = *(uint64_t *)(*var_5 + (*var_6 << 5UL));
                *(uint64_t *)(var_17 + (-24L)) = 4211931UL;
                var_34 = indirect_placeholder_8(var_33, 0UL, 3UL);
                *(uint64_t *)(var_17 + (-32L)) = 4211939UL;
                indirect_placeholder();
                var_35 = (uint64_t)*(uint32_t *)var_34;
                var_36 = var_17 + (-40L);
                *(uint64_t *)var_36 = 4211966UL;
                var_37 = indirect_placeholder_86(0UL, 4290173UL, var_34, 1UL, var_35, r95_3, r86_3);
                local_sp_1 = var_36;
                r95_1 = var_37.field_3;
                r86_1 = var_37.field_4;
            }
            *var_8 = (unsigned char)'\x01';
            local_sp_2_ph = local_sp_1;
            r95_2_ph = r95_1;
            r86_2_ph = r86_1;
            while (1U)
                {
                    r95_0 = r95_2_ph;
                    r86_0 = r86_2_ph;
                    var_38 = *var_11;
                    while (1U)
                        {
                            if ((int)*(uint32_t *)((*var_5 + (var_38 << 5UL)) + 8UL) >= (int)0U) {
                                loop_state_var = 0U;
                                break;
                            }
                            storemerge6 = *(uint64_t *)((var_38 == 0UL) ? var_7 : var_10) + (-1L);
                            *var_11 = storemerge6;
                            var_38 = storemerge6;
                            if (storemerge6 == *var_6) {
                                continue;
                            }
                            loop_state_var = 1U;
                            break;
                        }
                    switch_state_var = 0;
                    switch (loop_state_var) {
                      case 0U:
                        {
                            switch_state_var = 1;
                            break;
                        }
                        break;
                      case 1U:
                        {
                            var_46 = *(uint64_t *)(*var_5 + (storemerge6 << 5UL));
                            *(uint64_t *)(local_sp_2_ph + (-8L)) = 4212050UL;
                            var_47 = indirect_placeholder_8(var_46, 0UL, 3UL);
                            *(uint64_t *)(local_sp_2_ph + (-16L)) = 4212058UL;
                            indirect_placeholder();
                            var_48 = (uint64_t)*(uint32_t *)var_47;
                            var_49 = local_sp_2_ph + (-24L);
                            *(uint64_t *)var_49 = 4212085UL;
                            var_50 = indirect_placeholder_85(0UL, 4290173UL, var_47, 1UL, var_48, r95_2_ph, r86_2_ph);
                            local_sp_2_ph = var_49;
                            r95_2_ph = var_50.field_3;
                            r86_2_ph = var_50.field_4;
                            continue;
                        }
                        break;
                    }
                    if (switch_state_var)
                        break;
                }
            var_39 = local_sp_2_ph + (-8L);
            *(uint64_t *)var_39 = 4212144UL;
            var_40 = indirect_placeholder_87();
            local_sp_0 = var_39;
            rcx2_0 = var_40.field_1;
            if ((uint64_t)(uint32_t)var_40.field_0 == 0UL) {
                var_41 = *(uint64_t *)(*var_5 + (*var_11 << 5UL));
                *(uint64_t *)(local_sp_2_ph + (-16L)) = 4212187UL;
                var_42 = indirect_placeholder_8(var_41, 0UL, 3UL);
                *(uint64_t *)(local_sp_2_ph + (-24L)) = 4212195UL;
                indirect_placeholder();
                var_43 = (uint64_t)*(uint32_t *)var_42;
                var_44 = local_sp_2_ph + (-32L);
                *(uint64_t *)var_44 = 4212222UL;
                var_45 = indirect_placeholder_84(0UL, 4290173UL, var_42, 1UL, var_43, r95_2_ph, r86_2_ph);
                local_sp_0 = var_44;
                rcx2_0 = var_45.field_0;
                r95_0 = var_45.field_3;
                r86_0 = var_45.field_4;
            }
            *(uint64_t *)((*var_5 + (*var_11 << 5UL)) + 16UL) = 0UL;
            *(uint32_t *)((*var_5 + (*var_11 << 5UL)) + 8UL) = 4294967294U;
            local_sp_4 = local_sp_0;
            rcx2_1 = rcx2_0;
            r95_3 = r95_0;
            r86_3 = r86_0;
            continue;
        }
    *(uint32_t *)(((*var_6 << 5UL) + *var_5) + 8UL) = var_23;
    var_24 = *var_5 + (*var_6 << 5UL);
    var_25 = (uint64_t)*var_12;
    *(uint64_t *)(var_17 + (-8L)) = 4211846UL;
    indirect_placeholder();
    *(uint64_t *)(var_24 + 16UL) = var_25;
    if (var_25 == 0UL) {
        var_26 = *(uint64_t *)(*var_5 + (*var_6 << 5UL));
        *(uint64_t *)(var_17 + (-16L)) = 4212317UL;
        var_27 = indirect_placeholder_8(var_26, 0UL, 3UL);
        *(uint64_t *)(var_17 + (-24L)) = 4212325UL;
        indirect_placeholder();
        var_28 = (uint64_t)*(uint32_t *)var_27;
        *(uint64_t *)(var_17 + (-32L)) = 4212352UL;
        indirect_placeholder_83(0UL, 4290173UL, var_27, 1UL, var_28, r95_3, r86_3);
    }
    *(uint32_t *)(((*var_6 << 5UL) + *var_5) + 24UL) = *(uint32_t *)6404104UL;
    *(uint32_t *)6404104UL = 0U;
    var_51 = *var_8;
    return (uint64_t)var_51;
}

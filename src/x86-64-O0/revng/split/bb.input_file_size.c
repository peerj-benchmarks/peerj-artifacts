typedef unsigned char __u_char;
typedef unsigned short __u_short;
typedef unsigned int __u_int;
typedef unsigned long __u_long;
typedef signed char __int8_t;
typedef unsigned char __uint8_t;
typedef short __int16_t;
typedef unsigned short __uint16_t;
typedef int __int32_t;
typedef unsigned int __uint32_t;
typedef long __int64_t;
typedef unsigned long __uint64_t;
typedef __int8_t __int_least8_t;
typedef __uint8_t __uint_least8_t;
typedef __int16_t __int_least16_t;
typedef __uint16_t __uint_least16_t;
typedef __int32_t __int_least32_t;
typedef __uint32_t __uint_least32_t;
typedef __int64_t __int_least64_t;
typedef __uint64_t __uint_least64_t;
typedef long __quad_t;
typedef unsigned long __u_quad_t;
typedef long __intmax_t;
typedef unsigned long __uintmax_t;
typedef unsigned long __dev_t;
typedef unsigned int __uid_t;
typedef unsigned int __gid_t;
typedef unsigned long __ino_t;
typedef unsigned long __ino64_t;
typedef unsigned int __mode_t;
typedef unsigned long __nlink_t;
typedef long __off_t;
typedef long __off64_t;
typedef int __pid_t;
typedef struct {
    int __val[2];
} __fsid_t;
typedef long __clock_t;
typedef unsigned long __rlim_t;
typedef unsigned long __rlim64_t;
typedef unsigned int __id_t;
typedef long __time_t;
typedef unsigned int __useconds_t;
typedef long __suseconds_t;
typedef int __daddr_t;
typedef int __key_t;
typedef int __clockid_t;
typedef void *__timer_t;
typedef long __blksize_t;
typedef long __blkcnt_t;
typedef long __blkcnt64_t;
typedef unsigned long __fsblkcnt_t;
typedef unsigned long __fsblkcnt64_t;
typedef unsigned long __fsfilcnt_t;
typedef unsigned long __fsfilcnt64_t;
typedef long __fsword_t;
typedef long __ssize_t;
typedef long __syscall_slong_t;
typedef unsigned long __syscall_ulong_t;
typedef __off64_t __loff_t;
typedef char *__caddr_t;
typedef long __intptr_t;
typedef unsigned int __socklen_t;
typedef int __sig_atomic_t;
typedef __int8_t int8_t;
typedef __int16_t int16_t;
typedef __int32_t int32_t;
typedef __int64_t int64_t;
typedef __uint8_t uint8_t;
typedef __uint16_t uint16_t;
typedef __uint32_t uint32_t;
typedef __uint64_t uint64_t;
typedef __int_least8_t int_least8_t;
typedef __int_least16_t int_least16_t;
typedef __int_least32_t int_least32_t;
typedef __int_least64_t int_least64_t;
typedef __uint_least8_t uint_least8_t;
typedef __uint_least16_t uint_least16_t;
typedef __uint_least32_t uint_least32_t;
typedef __uint_least64_t uint_least64_t;
typedef signed char int_fast8_t;
typedef long int_fast16_t;
typedef long int_fast32_t;
typedef long int_fast64_t;
typedef unsigned char uint_fast8_t;
typedef unsigned long uint_fast16_t;
typedef unsigned long uint_fast32_t;
typedef unsigned long uint_fast64_t;
typedef long intptr_t;
typedef unsigned long uintptr_t;
typedef __intmax_t intmax_t;
typedef __uintmax_t uintmax_t;
struct bb_input_file_size_ret_type;
struct bb_input_file_size_ret_type {
    uint64_t field_0;
    uint64_t field_1;
    uint64_t field_2;
};
extern uint64_t revng_init_local_sp(uint64_t param_0);
extern uint64_t init_rbp(void);
extern uint64_t indirect_placeholder_4(uint64_t param_0, uint64_t param_1);
extern uint64_t init_cc_src2(void);
extern uint64_t helper_cc_compute_c_wrapper(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint32_t param_3);
extern void indirect_placeholder(void);
extern uint64_t indirect_placeholder_33(uint64_t param_0);
typedef _Bool bool;
struct bb_input_file_size_ret_type bb_input_file_size(uint64_t rdx, uint64_t rcx, uint64_t rdi, uint64_t rsi) {
    uint64_t var_0;
    uint64_t var_1;
    uint64_t var_2;
    uint32_t *var_3;
    uint64_t *var_4;
    uint64_t *var_5;
    uint64_t *var_6;
    uint64_t var_7;
    uint64_t var_8;
    uint64_t *var_9;
    uint64_t *var_10;
    uint64_t *var_11;
    uint64_t var_12;
    uint64_t *_pre_phi74;
    uint64_t var_31;
    uint64_t var_32;
    uint64_t var_33;
    uint64_t var_34;
    uint64_t var_25;
    uint64_t local_sp_0;
    uint64_t *var_26;
    uint64_t rcx4_0;
    uint64_t rdi5_0;
    uint64_t var_35;
    uint64_t rax_0;
    uint64_t var_27;
    uint64_t var_28;
    uint64_t *var_29;
    uint64_t var_30;
    uint64_t var_18;
    uint64_t var_19;
    uint64_t var_20;
    uint64_t var_21;
    uint64_t var_22;
    uint64_t var_23;
    uint64_t var_24;
    uint64_t rcx4_1;
    uint64_t rdi5_1;
    struct bb_input_file_size_ret_type mrv;
    struct bb_input_file_size_ret_type mrv1;
    struct bb_input_file_size_ret_type mrv2;
    uint64_t local_sp_1;
    uint64_t var_13;
    uint64_t var_14;
    uint64_t var_15;
    uint64_t var_16;
    uint64_t var_17;
    unsigned int loop_state_var;
    bool switch_state_var;
    var_0 = revng_init_local_sp(0UL);
    var_1 = init_rbp();
    var_2 = init_cc_src2();
    *(uint64_t *)(var_0 + (-8L)) = var_1;
    var_3 = (uint32_t *)(var_0 + (-44L));
    *var_3 = (uint32_t)rdi;
    var_4 = (uint64_t *)(var_0 + (-56L));
    *var_4 = rsi;
    var_5 = (uint64_t *)(var_0 + (-64L));
    *var_5 = rdx;
    var_6 = (uint64_t *)(var_0 + (-72L));
    *var_6 = rcx;
    var_7 = (uint64_t)*var_3;
    var_8 = var_0 + (-80L);
    *(uint64_t *)var_8 = 4204263UL;
    indirect_placeholder();
    var_9 = (uint64_t *)(var_0 + (-32L));
    *var_9 = var_7;
    var_10 = (uint64_t *)(var_0 + (-16L));
    *var_10 = 0UL;
    var_11 = (uint64_t *)(var_0 + (-40L));
    rax_0 = 18446744073709551615UL;
    var_12 = 0UL;
    local_sp_1 = var_8;
    while (1U)
        {
            var_13 = *var_6 - var_12;
            var_14 = *var_5;
            var_15 = (uint64_t)*var_3;
            var_16 = local_sp_1 + (-8L);
            *(uint64_t *)var_16 = 4204352UL;
            var_17 = indirect_placeholder_4(var_13, var_15);
            *var_11 = var_17;
            rdi5_1 = var_15;
            local_sp_1 = var_16;
            switch_state_var = 0;
            switch (var_17) {
              case 0UL:
                {
                    rax_0 = *var_10;
                    rcx4_1 = var_12 + var_14;
                    loop_state_var = 0U;
                    switch_state_var = 1;
                    break;
                }
                break;
              case 18446744073709551615UL:
                {
                    rcx4_1 = var_12 + var_14;
                    loop_state_var = 0U;
                    switch_state_var = 1;
                    break;
                }
                break;
              default:
                {
                    var_18 = var_17 + *var_10;
                    *var_10 = var_18;
                    var_19 = *var_6;
                    var_20 = helper_cc_compute_c_wrapper(var_18 - var_19, var_19, var_2, 17U);
                    if (var_20 == 0UL) {
                        var_12 = *var_10;
                        continue;
                    }
                    var_21 = var_12 + var_14;
                    rcx4_0 = var_21;
                    rcx4_1 = var_21;
                    if (*(uint64_t *)(*var_4 + 48UL) != 0UL) {
                        *(uint64_t *)(local_sp_1 + (-16L)) = 4204434UL;
                        indirect_placeholder();
                        *(volatile uint32_t *)(uint32_t *)0UL = 75U;
                        loop_state_var = 0U;
                        switch_state_var = 1;
                        break;
                    }
                    *var_9 = (*var_9 + *var_10);
                    var_22 = *var_4;
                    var_23 = local_sp_1 + (-16L);
                    *(uint64_t *)var_23 = 4204472UL;
                    var_24 = indirect_placeholder_33(var_22);
                    local_sp_0 = var_23;
                    rdi5_0 = var_22;
                    if ((uint64_t)(unsigned char)var_24 != 0UL) {
                        loop_state_var = 2U;
                        switch_state_var = 1;
                        break;
                    }
                    var_25 = *(uint64_t *)(*var_4 + 48UL);
                    if ((long)var_25 >= (long)*var_9) {
                        loop_state_var = 2U;
                        switch_state_var = 1;
                        break;
                    }
                    var_26 = (uint64_t *)(var_0 + (-24L));
                    *var_26 = var_25;
                    _pre_phi74 = var_26;
                    loop_state_var = 1U;
                    switch_state_var = 1;
                    break;
                }
                break;
            }
            if (switch_state_var)
                break;
        }
    switch (loop_state_var) {
      case 0U:
        {
            mrv.field_0 = rax_0;
            mrv1 = mrv;
            mrv1.field_1 = rcx4_1;
            mrv2 = mrv1;
            mrv2.field_2 = rdi5_1;
            return mrv2;
        }
        break;
      case 2U:
      case 1U:
        {
            switch (loop_state_var) {
              case 1U:
                {
                    var_35 = *var_10 + (*_pre_phi74 - *var_9);
                    *var_10 = var_35;
                    rax_0 = var_35;
                    rcx4_1 = rcx4_0;
                    rdi5_1 = rdi5_0;
                    if (var_35 == 9223372036854775807UL) {
                        *(uint64_t *)(local_sp_0 + (-8L)) = 4204641UL;
                        indirect_placeholder();
                        *(uint32_t *)9223372036854775807UL = 75U;
                        rax_0 = 18446744073709551615UL;
                    }
                }
                break;
              case 2U:
                {
                    var_27 = (uint64_t)*var_3;
                    var_28 = local_sp_1 + (-24L);
                    *(uint64_t *)var_28 = 4204524UL;
                    indirect_placeholder();
                    var_29 = (uint64_t *)(var_0 + (-24L));
                    *var_29 = var_27;
                    var_30 = *var_9;
                    _pre_phi74 = var_29;
                    local_sp_0 = var_28;
                    rdi5_0 = var_27;
                    var_31 = (uint64_t)*var_3;
                    var_32 = local_sp_1 + (-32L);
                    *(uint64_t *)var_32 = 4204576UL;
                    indirect_placeholder();
                    var_33 = *var_29;
                    var_34 = *var_9;
                    local_sp_0 = var_32;
                    rcx4_0 = var_30;
                    rdi5_0 = var_31;
                    if (var_27 != var_30 & (long)var_33 < (long)var_34) {
                        *var_29 = var_34;
                    }
                }
                break;
            }
        }
        break;
    }
}

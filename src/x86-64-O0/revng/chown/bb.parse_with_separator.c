typedef unsigned char __u_char;
typedef unsigned short __u_short;
typedef unsigned int __u_int;
typedef unsigned long __u_long;
typedef signed char __int8_t;
typedef unsigned char __uint8_t;
typedef short __int16_t;
typedef unsigned short __uint16_t;
typedef int __int32_t;
typedef unsigned int __uint32_t;
typedef long __int64_t;
typedef unsigned long __uint64_t;
typedef __int8_t __int_least8_t;
typedef __uint8_t __uint_least8_t;
typedef __int16_t __int_least16_t;
typedef __uint16_t __uint_least16_t;
typedef __int32_t __int_least32_t;
typedef __uint32_t __uint_least32_t;
typedef __int64_t __int_least64_t;
typedef __uint64_t __uint_least64_t;
typedef long __quad_t;
typedef unsigned long __u_quad_t;
typedef long __intmax_t;
typedef unsigned long __uintmax_t;
typedef unsigned long __dev_t;
typedef unsigned int __uid_t;
typedef unsigned int __gid_t;
typedef unsigned long __ino_t;
typedef unsigned long __ino64_t;
typedef unsigned int __mode_t;
typedef unsigned long __nlink_t;
typedef long __off_t;
typedef long __off64_t;
typedef int __pid_t;
typedef struct {
    int __val[2];
} __fsid_t;
typedef long __clock_t;
typedef unsigned long __rlim_t;
typedef unsigned long __rlim64_t;
typedef unsigned int __id_t;
typedef long __time_t;
typedef unsigned int __useconds_t;
typedef long __suseconds_t;
typedef int __daddr_t;
typedef int __key_t;
typedef int __clockid_t;
typedef void *__timer_t;
typedef long __blksize_t;
typedef long __blkcnt_t;
typedef long __blkcnt64_t;
typedef unsigned long __fsblkcnt_t;
typedef unsigned long __fsblkcnt64_t;
typedef unsigned long __fsfilcnt_t;
typedef unsigned long __fsfilcnt64_t;
typedef long __fsword_t;
typedef long __ssize_t;
typedef long __syscall_slong_t;
typedef unsigned long __syscall_ulong_t;
typedef __off64_t __loff_t;
typedef char *__caddr_t;
typedef long __intptr_t;
typedef unsigned int __socklen_t;
typedef int __sig_atomic_t;
typedef __int8_t int8_t;
typedef __int16_t int16_t;
typedef __int32_t int32_t;
typedef __int64_t int64_t;
typedef __uint8_t uint8_t;
typedef __uint16_t uint16_t;
typedef __uint32_t uint32_t;
typedef __uint64_t uint64_t;
typedef __int_least8_t int_least8_t;
typedef __int_least16_t int_least16_t;
typedef __int_least32_t int_least32_t;
typedef __int_least64_t int_least64_t;
typedef __uint_least8_t uint_least8_t;
typedef __uint_least16_t uint_least16_t;
typedef __uint_least32_t uint_least32_t;
typedef __uint_least64_t uint_least64_t;
typedef signed char int_fast8_t;
typedef long int_fast16_t;
typedef long int_fast32_t;
typedef long int_fast64_t;
typedef unsigned char uint_fast8_t;
typedef unsigned long uint_fast16_t;
typedef unsigned long uint_fast32_t;
typedef unsigned long uint_fast64_t;
typedef long intptr_t;
typedef unsigned long uintptr_t;
typedef __intmax_t intmax_t;
typedef __uintmax_t uintmax_t;
extern uint64_t revng_init_local_sp(uint64_t param_0);
extern uint64_t init_rbp(void);
extern uint64_t indirect_placeholder_1(uint64_t param_0, uint64_t param_1);
extern void indirect_placeholder(void);
extern uint64_t indirect_placeholder_8(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint64_t param_3, uint64_t param_4);
extern uint64_t indirect_placeholder_5(uint64_t param_0);
uint64_t bb_parse_with_separator(uint64_t rdx, uint64_t rcx, uint64_t rdi, uint64_t rsi, uint64_t r9, uint64_t r8) {
    uint64_t var_61;
    uint64_t var_52;
    uint64_t var_0;
    uint64_t var_1;
    uint64_t var_2;
    uint64_t var_3;
    uint64_t *var_4;
    uint64_t *var_5;
    uint64_t var_6;
    uint64_t var_7;
    uint64_t *var_8;
    uint64_t var_9;
    uint64_t *var_10;
    uint64_t *var_11;
    uint64_t *var_12;
    uint32_t **var_13;
    uint32_t var_14;
    uint32_t *var_15;
    uint32_t storemerge;
    uint64_t var_62;
    uint32_t var_63;
    uint64_t local_sp_7;
    uint64_t rax_0;
    uint64_t local_sp_0;
    uint64_t var_45;
    uint64_t var_46;
    uint64_t local_sp_4;
    uint64_t var_41;
    uint64_t var_42;
    uint64_t var_43;
    uint64_t var_44;
    uint64_t var_53;
    uint32_t var_54;
    uint32_t *var_16;
    uint64_t *var_17;
    uint64_t var_18;
    uint64_t *var_19;
    uint64_t var_20;
    uint64_t local_sp_1;
    uint64_t var_27;
    uint64_t var_28;
    uint64_t var_29;
    uint64_t var_21;
    uint64_t *var_22;
    uint64_t var_23;
    uint64_t var_24;
    uint64_t var_25;
    uint64_t var_26;
    uint64_t var_30;
    uint64_t storemerge12;
    uint64_t var_31;
    uint64_t var_32;
    uint64_t *var_33;
    uint64_t var_34;
    uint64_t var_56;
    uint64_t rax_1;
    uint64_t var_35;
    uint64_t local_sp_2;
    uint64_t *var_36;
    unsigned char storemerge13;
    unsigned char *var_47;
    unsigned char var_48;
    uint64_t var_49;
    uint64_t var_50;
    uint64_t var_51;
    uint32_t var_37;
    uint64_t var_38;
    uint64_t var_39;
    uint64_t var_40;
    uint64_t var_55;
    uint64_t local_sp_5;
    uint64_t local_sp_8;
    uint64_t rax_2;
    uint64_t var_57;
    uint64_t local_sp_6;
    uint64_t var_58;
    uint64_t var_59;
    uint64_t var_60;
    uint64_t var_64;
    uint64_t var_65;
    uint64_t var_66;
    var_0 = revng_init_local_sp(0UL);
    var_1 = init_rbp();
    *(uint64_t *)(var_0 + (-8L)) = var_1;
    var_2 = var_0 + (-184L);
    var_3 = var_0 + (-144L);
    var_4 = (uint64_t *)var_3;
    *var_4 = rdi;
    var_5 = (uint64_t *)(var_0 + (-152L));
    *var_5 = rsi;
    var_6 = var_0 + (-160L);
    *(uint64_t *)var_6 = rdx;
    var_7 = var_0 + (-168L);
    var_8 = (uint64_t *)var_7;
    *var_8 = rcx;
    var_9 = var_0 + (-176L);
    var_10 = (uint64_t *)var_9;
    *var_10 = r8;
    var_11 = (uint64_t *)var_2;
    *var_11 = r9;
    var_12 = (uint64_t *)(var_0 + (-32L));
    *var_12 = 0UL;
    var_13 = (uint32_t **)var_6;
    var_14 = **var_13;
    var_15 = (uint32_t *)(var_0 + (-36L));
    *var_15 = var_14;
    storemerge = 4294967295U;
    local_sp_1 = var_2;
    storemerge12 = 0UL;
    rax_1 = 0UL;
    storemerge13 = (unsigned char)'\x00';
    rax_2 = 0UL;
    if (*var_8 == 0UL) {
        storemerge = **(uint32_t **)var_7;
    }
    var_16 = (uint32_t *)(var_0 + (-40L));
    *var_16 = storemerge;
    var_17 = (uint64_t *)(var_0 + (-16L));
    *var_17 = 0UL;
    if (*var_10 == 0UL) {
        **(uint64_t **)var_9 = 0UL;
    }
    if (*var_11 == 0UL) {
        **(uint64_t **)var_2 = 0UL;
    }
    var_18 = var_0 + (-24L);
    var_19 = (uint64_t *)var_18;
    *var_19 = 0UL;
    var_20 = *var_5;
    if (var_20 == 0UL) {
        if (**(unsigned char **)var_3 == '\x00') {
            var_27 = *var_4;
            var_28 = var_0 + (-192L);
            *(uint64_t *)var_28 = 4216143UL;
            var_29 = indirect_placeholder_5(var_27);
            *var_19 = var_29;
            local_sp_1 = var_28;
        }
    } else {
        var_21 = var_20 - *var_4;
        var_22 = (uint64_t *)(var_0 + (-48L));
        *var_22 = var_21;
        if (var_21 == 0UL) {
            var_23 = var_21 + 1UL;
            var_24 = *var_4;
            var_25 = var_0 + (-192L);
            *(uint64_t *)var_25 = 4216206UL;
            var_26 = indirect_placeholder_1(var_24, var_23);
            *var_19 = var_26;
            *(unsigned char *)(*var_22 + var_26) = (unsigned char)'\x00';
            local_sp_1 = var_25;
        }
    }
    var_30 = *var_5;
    local_sp_2 = local_sp_1;
    local_sp_5 = local_sp_1;
    if (var_30 == 0UL) {
        var_31 = var_30 + 1UL;
        storemerge12 = (*(unsigned char *)var_31 == '\x00') ? 0UL : var_31;
    }
    var_32 = var_0 + (-56L);
    var_33 = (uint64_t *)var_32;
    *var_33 = storemerge12;
    var_34 = *var_19;
    var_56 = storemerge12;
    if (var_34 != 0UL) {
        if (**(unsigned char **)var_18 == '+') {
            var_35 = local_sp_1 + (-8L);
            *(uint64_t *)var_35 = 4216308UL;
            indirect_placeholder();
            rax_1 = var_34;
            local_sp_2 = var_35;
        }
        var_36 = (uint64_t *)(var_0 + (-64L));
        *var_36 = rax_1;
        local_sp_4 = local_sp_2;
        if (rax_1 == 0UL) {
            if (*var_5 == 0UL) {
            } else {
                storemerge13 = (unsigned char)'\x01';
                if (*var_33 == 0UL) {
                }
            }
            var_47 = (unsigned char *)(var_0 + (-65L));
            var_48 = storemerge13 & '\x01';
            *var_47 = var_48;
            if (var_48 == '\x00') {
                *var_17 = *(uint64_t *)6419800UL;
            } else {
                var_49 = var_0 + (-88L);
                var_50 = *var_19;
                var_51 = local_sp_2 + (-8L);
                *(uint64_t *)var_51 = 4216420UL;
                var_52 = indirect_placeholder_8(10UL, var_49, var_50, 0UL, 4306434UL);
                local_sp_4 = var_51;
                if ((uint64_t)(uint32_t)var_52 == 0UL) {
                    *var_17 = *(uint64_t *)6419808UL;
                } else {
                    var_53 = *(uint64_t *)var_49;
                    if (var_53 > 4294967295UL) {
                        *var_17 = *(uint64_t *)6419808UL;
                    } else {
                        var_54 = (uint32_t)var_53;
                        if ((uint64_t)(var_54 + 1U) == 0UL) {
                            *var_17 = *(uint64_t *)6419808UL;
                        } else {
                            *var_15 = var_54;
                        }
                    }
                }
            }
        } else {
            *var_15 = *(uint32_t *)(rax_1 + 16UL);
            if (*var_33 != 0UL & *var_5 != 0UL) {
                var_37 = *(uint32_t *)(*var_36 + 20UL);
                *var_16 = var_37;
                var_38 = (uint64_t)var_37;
                var_39 = local_sp_2 + (-8L);
                *(uint64_t *)var_39 = 4216518UL;
                indirect_placeholder();
                var_40 = var_0 + (-80L);
                *(uint64_t *)var_40 = var_38;
                local_sp_0 = var_39;
                if (var_38 == 0UL) {
                    var_41 = (uint64_t)*var_16;
                    var_42 = var_0 + (-120L);
                    var_43 = local_sp_2 + (-16L);
                    *(uint64_t *)var_43 = 4216556UL;
                    var_44 = indirect_placeholder_1(var_41, var_42);
                    rax_0 = var_44;
                    local_sp_0 = var_43;
                } else {
                    rax_0 = **(uint64_t **)var_40;
                }
                *(uint64_t *)(local_sp_0 + (-8L)) = 4216564UL;
                var_45 = indirect_placeholder_5(rax_0);
                *var_12 = var_45;
                var_46 = local_sp_0 + (-16L);
                *(uint64_t *)var_46 = 4216573UL;
                indirect_placeholder();
                local_sp_4 = var_46;
            }
        }
        var_55 = local_sp_4 + (-8L);
        *(uint64_t *)var_55 = 4216578UL;
        indirect_placeholder();
        var_56 = *var_33;
        local_sp_5 = var_55;
    }
    local_sp_6 = local_sp_5;
    local_sp_8 = local_sp_5;
    if (var_56 != 0UL & *var_17 != 0UL) {
        if (**(unsigned char **)var_32 == '+') {
            var_57 = local_sp_5 + (-8L);
            *(uint64_t *)var_57 = 4216623UL;
            indirect_placeholder();
            rax_2 = var_56;
            local_sp_6 = var_57;
        }
        *(uint64_t *)(var_0 + (-80L)) = rax_2;
        local_sp_7 = local_sp_6;
        if (rax_2 == 0UL) {
            *var_16 = *(uint32_t *)(rax_2 + 16UL);
        } else {
            var_58 = var_0 + (-128L);
            var_59 = *var_33;
            var_60 = local_sp_6 + (-8L);
            *(uint64_t *)var_60 = 4216676UL;
            var_61 = indirect_placeholder_8(10UL, var_58, var_59, 0UL, 4306434UL);
            local_sp_7 = var_60;
            if ((uint64_t)(uint32_t)var_61 == 0UL) {
                *var_17 = *(uint64_t *)6419816UL;
            } else {
                var_62 = *(uint64_t *)var_58;
                if (var_62 > 4294967295UL) {
                    *var_17 = *(uint64_t *)6419816UL;
                } else {
                    var_63 = (uint32_t)var_62;
                    if ((uint64_t)(var_63 + 1U) == 0UL) {
                        *var_17 = *(uint64_t *)6419816UL;
                    } else {
                        *var_16 = var_63;
                    }
                }
            }
        }
        *(uint64_t *)(local_sp_7 + (-8L)) = 4216740UL;
        indirect_placeholder();
        var_64 = *var_33;
        var_65 = local_sp_7 + (-16L);
        *(uint64_t *)var_65 = 4216752UL;
        var_66 = indirect_placeholder_5(var_64);
        *var_12 = var_66;
        local_sp_8 = var_65;
    }
    if (*var_17 == 0UL) {
        *(uint64_t *)(local_sp_8 + (-8L)) = 4216873UL;
        indirect_placeholder();
        *(uint64_t *)(local_sp_8 + (-16L)) = 4216885UL;
        indirect_placeholder();
        return *var_17;
    }
    **var_13 = *var_15;
    if (*var_8 == 0UL) {
        **(uint32_t **)var_7 = *var_16;
    }
    if (*var_10 == 0UL) {
        **(uint64_t **)var_9 = *var_19;
        *var_19 = 0UL;
    }
    if (*var_11 != 0UL) {
        *(uint64_t *)(local_sp_8 + (-8L)) = 4216873UL;
        indirect_placeholder();
        *(uint64_t *)(local_sp_8 + (-16L)) = 4216885UL;
        indirect_placeholder();
        return *var_17;
    }
    **(uint64_t **)var_2 = *var_12;
    *var_12 = 0UL;
    *(uint64_t *)(local_sp_8 + (-8L)) = 4216873UL;
    indirect_placeholder();
    *(uint64_t *)(local_sp_8 + (-16L)) = 4216885UL;
    indirect_placeholder();
    return *var_17;
}

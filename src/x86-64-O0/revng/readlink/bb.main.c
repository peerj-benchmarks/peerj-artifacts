typedef unsigned char __u_char;
typedef unsigned short __u_short;
typedef unsigned int __u_int;
typedef unsigned long __u_long;
typedef signed char __int8_t;
typedef unsigned char __uint8_t;
typedef short __int16_t;
typedef unsigned short __uint16_t;
typedef int __int32_t;
typedef unsigned int __uint32_t;
typedef long __int64_t;
typedef unsigned long __uint64_t;
typedef __int8_t __int_least8_t;
typedef __uint8_t __uint_least8_t;
typedef __int16_t __int_least16_t;
typedef __uint16_t __uint_least16_t;
typedef __int32_t __int_least32_t;
typedef __uint32_t __uint_least32_t;
typedef __int64_t __int_least64_t;
typedef __uint64_t __uint_least64_t;
typedef long __quad_t;
typedef unsigned long __u_quad_t;
typedef long __intmax_t;
typedef unsigned long __uintmax_t;
typedef unsigned long __dev_t;
typedef unsigned int __uid_t;
typedef unsigned int __gid_t;
typedef unsigned long __ino_t;
typedef unsigned long __ino64_t;
typedef unsigned int __mode_t;
typedef unsigned long __nlink_t;
typedef long __off_t;
typedef long __off64_t;
typedef int __pid_t;
typedef struct {
    int __val[2];
} __fsid_t;
typedef long __clock_t;
typedef unsigned long __rlim_t;
typedef unsigned long __rlim64_t;
typedef unsigned int __id_t;
typedef long __time_t;
typedef unsigned int __useconds_t;
typedef long __suseconds_t;
typedef int __daddr_t;
typedef int __key_t;
typedef int __clockid_t;
typedef void *__timer_t;
typedef long __blksize_t;
typedef long __blkcnt_t;
typedef long __blkcnt64_t;
typedef unsigned long __fsblkcnt_t;
typedef unsigned long __fsblkcnt64_t;
typedef unsigned long __fsfilcnt_t;
typedef unsigned long __fsfilcnt64_t;
typedef long __fsword_t;
typedef long __ssize_t;
typedef long __syscall_slong_t;
typedef unsigned long __syscall_ulong_t;
typedef __off64_t __loff_t;
typedef char *__caddr_t;
typedef long __intptr_t;
typedef unsigned int __socklen_t;
typedef int __sig_atomic_t;
typedef __int8_t int8_t;
typedef __int16_t int16_t;
typedef __int32_t int32_t;
typedef __int64_t int64_t;
typedef __uint8_t uint8_t;
typedef __uint16_t uint16_t;
typedef __uint32_t uint32_t;
typedef __uint64_t uint64_t;
typedef __int_least8_t int_least8_t;
typedef __int_least16_t int_least16_t;
typedef __int_least32_t int_least32_t;
typedef __int_least64_t int_least64_t;
typedef __uint_least8_t uint_least8_t;
typedef __uint_least16_t uint_least16_t;
typedef __uint_least32_t uint_least32_t;
typedef __uint_least64_t uint_least64_t;
typedef signed char int_fast8_t;
typedef long int_fast16_t;
typedef long int_fast32_t;
typedef long int_fast64_t;
typedef unsigned char uint_fast8_t;
typedef unsigned long uint_fast16_t;
typedef unsigned long uint_fast32_t;
typedef unsigned long uint_fast64_t;
typedef long intptr_t;
typedef unsigned long uintptr_t;
typedef __intmax_t intmax_t;
typedef __uintmax_t uintmax_t;
struct indirect_placeholder_23_ret_type;
struct indirect_placeholder_23_ret_type {
    uint64_t field_0;
    uint64_t field_1;
    uint64_t field_2;
    uint64_t field_3;
};
extern void abort(void);
extern uint64_t revng_init_local_sp(uint64_t param_0);
extern uint64_t init_rbp(void);
extern uint64_t indirect_placeholder_5(uint64_t param_0, uint64_t param_1);
extern void indirect_placeholder(void);
extern uint64_t indirect_placeholder_10(uint64_t param_0, uint64_t param_1, uint64_t param_2);
extern void indirect_placeholder_13(uint64_t param_0, uint64_t param_1);
extern uint64_t init_rbx(void);
extern void indirect_placeholder_21(uint64_t param_0);
extern void indirect_placeholder_11(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint64_t param_3, uint64_t param_4, uint64_t param_5, uint64_t param_6);
extern void indirect_placeholder_22(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint64_t param_3, uint64_t param_4, uint64_t param_5);
extern struct indirect_placeholder_23_ret_type indirect_placeholder_23(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint64_t param_3, uint64_t param_4);
void bb_main(uint64_t rdi, uint64_t rsi) {
    struct indirect_placeholder_23_ret_type var_16;
    uint64_t var_0;
    uint64_t var_1;
    uint64_t var_2;
    uint64_t var_3;
    uint32_t *var_4;
    uint64_t var_5;
    uint64_t *var_6;
    uint32_t *var_7;
    uint32_t *var_8;
    unsigned char *var_9;
    uint64_t var_10;
    uint64_t var_11;
    uint32_t *var_12;
    uint64_t local_sp_7;
    uint64_t rax_0;
    uint64_t var_38;
    uint64_t local_sp_0;
    uint64_t var_39;
    uint64_t local_sp_2;
    uint64_t local_sp_1;
    uint64_t var_37;
    uint64_t var_40;
    uint64_t var_41;
    uint64_t var_42;
    uint64_t var_43;
    uint32_t var_44;
    uint32_t var_29;
    uint64_t var_20;
    uint64_t var_21;
    uint64_t var_22;
    uint32_t var_23;
    uint64_t var_24;
    uint32_t var_25;
    uint32_t _pre;
    uint64_t local_sp_3;
    uint64_t var_26;
    uint64_t local_sp_4;
    uint64_t *var_27;
    uint64_t *var_28;
    uint64_t local_sp_5;
    uint64_t var_30;
    uint32_t var_31;
    uint64_t var_32;
    uint64_t var_33;
    uint64_t var_34;
    uint64_t var_35;
    uint64_t var_36;
    uint64_t local_sp_6;
    uint64_t var_18;
    uint64_t var_19;
    uint64_t var_13;
    uint64_t var_14;
    uint64_t var_15;
    uint32_t var_17;
    unsigned int loop_state_var;
    var_0 = revng_init_local_sp(0UL);
    var_1 = init_rbp();
    var_2 = init_rbx();
    var_3 = var_0 + (-8L);
    *(uint64_t *)var_3 = var_1;
    *(uint64_t *)(var_0 + (-16L)) = var_2;
    var_4 = (uint32_t *)(var_0 + (-60L));
    *var_4 = (uint32_t)rdi;
    var_5 = var_0 + (-72L);
    var_6 = (uint64_t *)var_5;
    *var_6 = rsi;
    var_7 = (uint32_t *)(var_0 + (-28L));
    *var_7 = 4294967295U;
    var_8 = (uint32_t *)(var_0 + (-32L));
    *var_8 = 0U;
    var_9 = (unsigned char *)(var_0 + (-33L));
    *var_9 = (unsigned char)'\x00';
    var_10 = **(uint64_t **)var_5;
    *(uint64_t *)(var_0 + (-80L)) = 4202578UL;
    indirect_placeholder_21(var_10);
    *(uint64_t *)(var_0 + (-88L)) = 4202593UL;
    indirect_placeholder();
    var_11 = var_0 + (-96L);
    *(uint64_t *)var_11 = 4202603UL;
    indirect_placeholder();
    var_12 = (uint32_t *)(var_0 + (-40L));
    local_sp_7 = var_11;
    while (1U)
        {
            var_13 = *var_6;
            var_14 = (uint64_t)*var_4;
            var_15 = local_sp_7 + (-8L);
            *(uint64_t *)var_15 = 4202885UL;
            var_16 = indirect_placeholder_23(4279591UL, 4278016UL, var_14, var_13, 0UL);
            var_17 = (uint32_t)var_16.field_0;
            *var_12 = var_17;
            local_sp_3 = var_15;
            local_sp_4 = var_15;
            local_sp_6 = var_15;
            local_sp_7 = var_15;
            if (var_17 != 4294967295U) {
                var_20 = var_16.field_1;
                var_21 = var_16.field_2;
                var_22 = var_16.field_3;
                var_23 = *(uint32_t *)6390968UL;
                var_24 = (uint64_t)var_23;
                var_25 = *var_4;
                _pre = var_23;
                if ((long)(var_24 << 32UL) < (long)((uint64_t)var_25 << 32UL)) {
                    *(uint64_t *)(local_sp_7 + (-16L)) = 4202934UL;
                    indirect_placeholder_11(0UL, 4279600UL, var_20, 0UL, 0UL, var_21, var_22);
                    *(uint64_t *)(local_sp_7 + (-24L)) = 4202944UL;
                    indirect_placeholder_13(var_3, 1UL);
                    abort();
                }
                if ((int)(var_25 - var_23) <= (int)1U) {
                    loop_state_var = 1U;
                    break;
                }
                if (*(unsigned char *)6391184UL != '\x00') {
                    loop_state_var = 0U;
                    break;
                }
                var_26 = local_sp_7 + (-16L);
                *(uint64_t *)var_26 = 4202998UL;
                indirect_placeholder_11(0UL, 4279616UL, var_20, 0UL, 0UL, var_21, var_22);
                local_sp_3 = var_26;
                loop_state_var = 0U;
                break;
            }
            if ((uint64_t)(var_17 + (-109)) == 0UL) {
                *var_7 = 2U;
                continue;
            }
            if ((int)var_17 > (int)109U) {
                if ((uint64_t)(var_17 + (-115)) == 0UL) {
                    *(unsigned char *)6391185UL = (unsigned char)'\x00';
                    continue;
                }
                if ((int)var_17 > (int)115U) {
                    if ((uint64_t)(var_17 + (-110)) != 0UL) {
                        *(unsigned char *)6391184UL = (unsigned char)'\x01';
                        continue;
                    }
                    if ((uint64_t)(var_17 + (-113)) != 0UL) {
                        loop_state_var = 2U;
                        break;
                    }
                }
                if ((uint64_t)(var_17 + (-118)) == 0UL) {
                    *(unsigned char *)6391185UL = (unsigned char)'\x01';
                    continue;
                }
                if ((uint64_t)(var_17 + (-122)) != 0UL) {
                    loop_state_var = 2U;
                    break;
                }
                *var_9 = (unsigned char)'\x01';
                continue;
            }
            if ((uint64_t)(var_17 + 130U) == 0UL) {
                *(uint64_t *)(local_sp_7 + (-16L)) = 4202783UL;
                indirect_placeholder_13(var_3, 0UL);
                abort();
            }
            if ((int)var_17 > (int)4294967166U) {
                if ((uint64_t)(var_17 + (-101)) == 0UL) {
                    *var_7 = 0U;
                    continue;
                }
                if ((uint64_t)(var_17 + (-102)) != 0UL) {
                    loop_state_var = 2U;
                    break;
                }
                *var_7 = 1U;
                continue;
            }
            if (var_17 != 4294967165U) {
                loop_state_var = 2U;
                break;
            }
            var_18 = *(uint64_t *)6390816UL;
            *(uint64_t *)(local_sp_7 + (-16L)) = 4202835UL;
            indirect_placeholder_22(0UL, 4277656UL, var_18, 4279566UL, 0UL, 4279575UL);
            var_19 = local_sp_7 + (-24L);
            *(uint64_t *)var_19 = 4202845UL;
            indirect_placeholder();
            local_sp_6 = var_19;
            loop_state_var = 2U;
            break;
        }
    switch (loop_state_var) {
      case 2U:
        {
            *(uint64_t *)(local_sp_6 + (-8L)) = 4202855UL;
            indirect_placeholder_13(var_3, 1UL);
            abort();
        }
        break;
      case 1U:
      case 0U:
        {
            switch (loop_state_var) {
              case 0U:
                {
                    *(unsigned char *)6391184UL = (unsigned char)'\x00';
                    _pre = *(uint32_t *)6390968UL;
                    local_sp_4 = local_sp_3;
                }
                break;
              case 1U:
                {
                    var_27 = (uint64_t *)(var_0 + (-48L));
                    var_28 = (uint64_t *)(var_0 + (-56L));
                    var_29 = _pre;
                    local_sp_5 = local_sp_4;
                    while ((long)((uint64_t)var_29 << 32UL) >= (long)((uint64_t)*var_4 << 32UL))
                        {
                            var_30 = *(uint64_t *)(*var_6 + ((uint64_t)var_29 << 3UL));
                            *var_27 = var_30;
                            var_31 = *var_7;
                            if (var_31 == 4294967295U) {
                                var_35 = local_sp_5 + (-8L);
                                *(uint64_t *)var_35 = 4203082UL;
                                var_36 = indirect_placeholder_5(var_30, 63UL);
                                local_sp_1 = var_35;
                                rax_0 = var_36;
                            } else {
                                var_32 = (uint64_t)var_31;
                                var_33 = local_sp_5 + (-8L);
                                *(uint64_t *)var_33 = 4203063UL;
                                var_34 = indirect_placeholder_5(var_30, var_32);
                                local_sp_1 = var_33;
                                rax_0 = var_34;
                            }
                            *var_28 = rax_0;
                            local_sp_2 = local_sp_1;
                            if (rax_0 == 0UL) {
                                *var_8 = 1U;
                                if (*(unsigned char *)6391185UL == '\x00') {
                                    var_40 = *var_27;
                                    *(uint64_t *)(local_sp_1 + (-8L)) = 4203208UL;
                                    var_41 = indirect_placeholder_10(var_40, 0UL, 3UL);
                                    *(uint64_t *)(local_sp_1 + (-16L)) = 4203216UL;
                                    indirect_placeholder();
                                    var_42 = (uint64_t)*(uint32_t *)var_41;
                                    var_43 = local_sp_1 + (-24L);
                                    *(uint64_t *)var_43 = 4203243UL;
                                    indirect_placeholder_11(0UL, 4279662UL, var_41, 0UL, var_42, var_21, var_22);
                                    local_sp_2 = var_43;
                                }
                            } else {
                                var_37 = local_sp_1 + (-8L);
                                *(uint64_t *)var_37 = 4203115UL;
                                indirect_placeholder();
                                local_sp_0 = var_37;
                                if (*(unsigned char *)6391184UL != '\x01') {
                                    var_38 = local_sp_1 + (-16L);
                                    *(uint64_t *)var_38 = 4203154UL;
                                    indirect_placeholder();
                                    local_sp_0 = var_38;
                                }
                                var_39 = local_sp_0 + (-8L);
                                *(uint64_t *)var_39 = 4203166UL;
                                indirect_placeholder();
                                local_sp_2 = var_39;
                            }
                            var_44 = *(uint32_t *)6390968UL + 1U;
                            *(uint32_t *)6390968UL = var_44;
                            var_29 = var_44;
                            local_sp_5 = local_sp_2;
                        }
                    return;
                }
                break;
            }
        }
        break;
    }
}


#include "dircolors.h"

long null_ARRAY_0061b47_0_8_;
long null_ARRAY_0061b47_8_8_;
long null_ARRAY_0061b5c_16_8_;
long null_ARRAY_0061b5c_24_8_;
long null_ARRAY_0061b5c_32_8_;
long null_ARRAY_0061b5c_48_8_;
long null_ARRAY_0061b5c_8_8_;
long null_ARRAY_0061b64_0_8_;
long null_ARRAY_0061b64_16_8_;
long null_ARRAY_0061b64_24_8_;
long null_ARRAY_0061b64_32_8_;
long null_ARRAY_0061b64_40_8_;
long null_ARRAY_0061b64_48_8_;
long null_ARRAY_0061b64_8_8_;
long null_ARRAY_0061b7a_0_4_;
long null_ARRAY_0061b7a_16_8_;
long null_ARRAY_0061b7a_4_4_;
long null_ARRAY_0061b7a_8_4_;
long DAT_004160c0;
long DAT_0041617d;
long DAT_004161fb;
long DAT_00417680;
long DAT_00417a76;
long DAT_00417a7b;
long DAT_00417af8;
long DAT_00417afa;
long DAT_00417afc;
long DAT_00417b0e;
long DAT_00417bcc;
long DAT_00417c5d;
long DAT_00417cb0;
long DAT_00417dde;
long DAT_00417de2;
long DAT_00417de7;
long DAT_00417de9;
long DAT_00418453;
long DAT_00418820;
long DAT_00418838;
long DAT_0041883d;
long DAT_00418f17;
long DAT_00418fa8;
long DAT_00418fab;
long DAT_00418ff9;
long DAT_0041900e;
long DAT_00419012;
long DAT_00419088;
long DAT_00419089;
long DAT_0061b000;
long DAT_0061b010;
long DAT_0061b020;
long DAT_0061b448;
long DAT_0061b460;
long DAT_0061b4d8;
long DAT_0061b4dc;
long DAT_0061b4e0;
long DAT_0061b500;
long DAT_0061b508;
long DAT_0061b510;
long DAT_0061b520;
long DAT_0061b528;
long DAT_0061b540;
long DAT_0061b548;
long DAT_0061b618;
long DAT_0061b620;
long DAT_0061b628;
long DAT_0061b780;
long DAT_0061b7d8;
long DAT_0061b7e0;
long DAT_0061b7e8;
long DAT_0061b7f8;
long fde_00419dd8;
long FLOAT_UNKNOWN;
long null_ARRAY_004173c0;
long null_ARRAY_00417540;
long null_ARRAY_004176c0;
long null_ARRAY_004194c0;
long null_ARRAY_0061b470;
long null_ARRAY_0061b4a0;
long null_ARRAY_0061b560;
long null_ARRAY_0061b5c0;
long null_ARRAY_0061b640;
long null_ARRAY_0061b680;
long null_ARRAY_0061b7a0;
long PTR_DAT_0061b440;
long PTR_FUN_0061b4e8;
long PTR_null_ARRAY_0061b480;
undefined8
FUN_0040209f (uint uParm1)
{
  int iVar1;
  char *pcVar2;
  undefined8 uVar3;

  if (uParm1 == 0)
    {
      func_0x00401840 ("Usage: %s [OPTION]... [FILE]\n", DAT_0061b628);
      func_0x00401a40
	("Output commands to set the LS_COLORS environment variable.\n\nDetermine format of output:\n  -b, --sh, --bourne-shell    output Bourne shell code to set LS_COLORS\n  -c, --csh, --c-shell        output C shell code to set LS_COLORS\n  -p, --print-database        output defaults\n",
	 DAT_0061b500);
      func_0x00401a40 ("      --help     display this help and exit\n",
		       DAT_0061b500);
      func_0x00401a40
	("      --version  output version information and exit\n",
	 DAT_0061b500);
      func_0x00401a40
	("\nIf FILE is specified, read it to determine which colors to use for which\nfile types and extensions.  Otherwise, a precompiled database is used.\nFor details on the format of these files, run \'dircolors --print-database\'.\n",
	 DAT_0061b500);
      FUN_00401f03 ("dircolors");
    }
  else
    {
      func_0x00401a30 (DAT_0061b520,
		       "Try \'%s --help\' for more information.\n",
		       DAT_0061b628);
    }
  func_0x00401c20 ((ulong) uParm1);
  pcVar2 = (char *) func_0x004018b0 ("SHELL");
  if ((pcVar2 == (char *) 0x0) || (*pcVar2 == '\0'))
    {
      uVar3 = 2;
    }
  else
    {
      uVar3 = FUN_00403379 (pcVar2);
      iVar1 = func_0x00401b60 (uVar3, &DAT_00417680);
      if ((iVar1 != 0)
	  && (iVar1 = func_0x00401b60 (uVar3, &DAT_00417a76), iVar1 != 0))
	{
	  return 0;
	}
      uVar3 = 1;
    }
  return uVar3;
}

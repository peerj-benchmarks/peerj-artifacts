typedef unsigned char   undefined;

typedef unsigned char    byte;
typedef unsigned char    dwfenc;
typedef unsigned int    dword;
typedef unsigned long    qword;
typedef unsigned char    undefined1;
typedef unsigned short    undefined2;
typedef unsigned int    undefined4;
typedef unsigned long    undefined5;
typedef unsigned long    undefined6;
typedef unsigned long    undefined7;
typedef unsigned long    undefined8;
typedef unsigned short    word;
#define unkbyte9   unsigned long long
#define unkbyte10   unsigned long long
#define unkbyte11   unsigned long long
#define unkbyte12   unsigned long long
#define unkbyte13   unsigned long long
#define unkbyte14   unsigned long long
#define unkbyte15   unsigned long long
#define unkbyte16   unsigned long long

#define unkuint9   unsigned long long
#define unkuint10   unsigned long long
#define unkuint11   unsigned long long
#define unkuint12   unsigned long long
#define unkuint13   unsigned long long
#define unkuint14   unsigned long long
#define unkuint15   unsigned long long
#define unkuint16   unsigned long long

#define unkint9   long long
#define unkint10   long long
#define unkint11   long long
#define unkint12   long long
#define unkint13   long long
#define unkint14   long long
#define unkint15   long long
#define unkint16   long long

#define unkfloat1   float
#define unkfloat2   float
#define unkfloat3   float
#define unkfloat5   double
#define unkfloat6   double
#define unkfloat7   double
#define unkfloat9   long double
#define unkfloat11   long double
#define unkfloat12   long double
#define unkfloat13   long double
#define unkfloat14   long double
#define unkfloat15   long double
#define unkfloat16   long double

#define BADSPACEBASE   void
#define code   void

#define ulong long
#define true 1
#define bool int
#define false 0
#define float10 int
#define ushort int
#define undefined7 long
#define undefined2 long
#define uint5 long
#define uint6 long

// Add definition for uint
typedef unsigned int    uint;

#define sbyte char

extern imperfection_wrapper(void);
typedef struct eh_frame_hdr eh_frame_hdr, *Peh_frame_hdr;

struct eh_frame_hdr {
    byte eh_frame_hdr_version; // Exception Handler Frame Header Version
    dwfenc eh_frame_pointer_encoding; // Exception Handler Frame Pointer Encoding
    dwfenc eh_frame_desc_entry_count_encoding; // Encoding of # of Exception Handler FDEs
    dwfenc eh_frame_table_encoding; // Exception Handler Table Encoding
};

typedef struct fde_table_entry fde_table_entry, *Pfde_table_entry;

struct fde_table_entry {
    dword initial_loc; // Initial Location
    dword data_loc; // Data location
};

typedef enum Elf64_DynTag {
    DT_AUDIT=1879047932,
    DT_AUXILIARY=2147483645,
    DT_BIND_NOW=24,
    DT_CHECKSUM=1879047672,
    DT_CONFIG=1879047930,
    DT_DEBUG=21,
    DT_DEPAUDIT=1879047931,
    DT_ENCODING=32,
    DT_FEATURE_1=1879047676,
    DT_FILTER=2147483647,
    DT_FINI=13,
    DT_FINI_ARRAY=26,
    DT_FINI_ARRAYSZ=28,
    DT_FLAGS=30,
    DT_FLAGS_1=1879048187,
    DT_GNU_CONFLICT=1879047928,
    DT_GNU_CONFLICTSZ=1879047670,
    DT_GNU_HASH=1879047925,
    DT_GNU_LIBLIST=1879047929,
    DT_GNU_LIBLISTSZ=1879047671,
    DT_GNU_PRELINKED=1879047669,
    DT_HASH=4,
    DT_INIT=12,
    DT_INIT_ARRAY=25,
    DT_INIT_ARRAYSZ=27,
    DT_JMPREL=23,
    DT_MOVEENT=1879047674,
    DT_MOVESZ=1879047675,
    DT_MOVETAB=1879047934,
    DT_NEEDED=1,
    DT_NULL=0,
    DT_PLTGOT=3,
    DT_PLTPAD=1879047933,
    DT_PLTPADSZ=1879047673,
    DT_PLTREL=20,
    DT_PLTRELSZ=2,
    DT_POSFLAG_1=1879047677,
    DT_PREINIT_ARRAYSZ=33,
    DT_REL=17,
    DT_RELA=7,
    DT_RELACOUNT=1879048185,
    DT_RELAENT=9,
    DT_RELASZ=8,
    DT_RELCOUNT=1879048186,
    DT_RELENT=19,
    DT_RELSZ=18,
    DT_RPATH=15,
    DT_RUNPATH=29,
    DT_SONAME=14,
    DT_STRSZ=10,
    DT_STRTAB=5,
    DT_SYMBOLIC=16,
    DT_SYMENT=11,
    DT_SYMINENT=1879047679,
    DT_SYMINFO=1879047935,
    DT_SYMINSZ=1879047678,
    DT_SYMTAB=6,
    DT_TEXTREL=22,
    DT_TLSDESC_GOT=1879047927,
    DT_TLSDESC_PLT=1879047926,
    DT_VERDEF=1879048188,
    DT_VERDEFNUM=1879048189,
    DT_VERNEED=1879048190,
    DT_VERNEEDNUM=1879048191,
    DT_VERSYM=1879048176
} Elf64_DynTag;

typedef enum Elf_ProgramHeaderType {
    PT_DYNAMIC=2,
    PT_GNU_EH_FRAME=1685382480,
    PT_GNU_RELRO=1685382482,
    PT_GNU_STACK=1685382481,
    PT_INTERP=3,
    PT_LOAD=1,
    PT_NOTE=4,
    PT_NULL=0,
    PT_PHDR=6,
    PT_SHLIB=5,
    PT_TLS=7
} Elf_ProgramHeaderType;

typedef struct Elf64_Dyn Elf64_Dyn, *PElf64_Dyn;

struct Elf64_Dyn {
    enum Elf64_DynTag d_tag;
    qword d_val;
};

typedef struct Elf64_Shdr Elf64_Shdr, *PElf64_Shdr;

typedef enum Elf_SectionHeaderType {
    SHT_CHECKSUM=1879048184,
    SHT_DYNAMIC=6,
    SHT_DYNSYM=11,
    SHT_FINI_ARRAY=15,
    SHT_GNU_ATTRIBUTES=1879048181,
    SHT_GNU_HASH=1879048182,
    SHT_GNU_LIBLIST=1879048183,
    SHT_GNU_verdef=1879048189,
    SHT_GNU_verneed=1879048190,
    SHT_GNU_versym=1879048191,
    SHT_GROUP=17,
    SHT_HASH=5,
    SHT_INIT_ARRAY=14,
    SHT_NOBITS=8,
    SHT_NOTE=7,
    SHT_NULL=0,
    SHT_PREINIT_ARRAY=16,
    SHT_PROGBITS=1,
    SHT_REL=9,
    SHT_RELA=4,
    SHT_SHLIB=10,
    SHT_STRTAB=3,
    SHT_SUNW_COMDAT=1879048187,
    SHT_SUNW_move=1879048186,
    SHT_SUNW_syminfo=1879048188,
    SHT_SYMTAB=2,
    SHT_SYMTAB_SHNDX=18
} Elf_SectionHeaderType;

struct Elf64_Shdr {
    dword sh_name;
    enum Elf_SectionHeaderType sh_type;
    qword sh_flags;
    qword sh_addr;
    qword sh_offset;
    qword sh_size;
    dword sh_link;
    dword sh_info;
    qword sh_addralign;
    qword sh_entsize;
};

typedef struct Elf64_Phdr Elf64_Phdr, *PElf64_Phdr;

struct Elf64_Phdr {
    enum Elf_ProgramHeaderType p_type;
    dword p_flags;
    qword p_offset;
    qword p_vaddr;
    qword p_paddr;
    qword p_filesz;
    qword p_memsz;
    qword p_align;
};

typedef struct Elf64_Sym Elf64_Sym, *PElf64_Sym;

struct Elf64_Sym {
    dword st_name;
    byte st_info;
    byte st_other;
    word st_shndx;
    qword st_value;
    qword st_size;
};

typedef struct Elf64_Rela Elf64_Rela, *PElf64_Rela;

struct Elf64_Rela {
    qword r_offset; // location to apply the relocation action
    qword r_info; // the symbol table index and the type of relocation
    qword r_addend; // a constant addend used to compute the relocatable field value
};

typedef struct Elf64_Ehdr Elf64_Ehdr, *PElf64_Ehdr;

struct Elf64_Ehdr {
    byte e_ident_magic_num;
    char e_ident_magic_str[3];
    byte e_ident_class;
    byte e_ident_data;
    byte e_ident_version;
    byte e_ident_pad[9];
    word e_type;
    word e_machine;
    dword e_version;
    qword e_entry;
    qword e_phoff;
    qword e_shoff;
    dword e_flags;
    word e_ehsize;
    word e_phentsize;
    word e_phnum;
    word e_shentsize;
    word e_shnum;
    word e_shstrndx;
};




undefined8 _DT_INIT(void);
void FUN_004023d0(void);
void FUN_004026a0(void);
void FUN_004028f0(void);
void FUN_004029a0(void);
void FUN_00402bb0(void);
void entry(void);
void FUN_00402c20(void);
void FUN_00402ca0(void);
void FUN_00402d20(void);
void FUN_00402d5e(int iParm1);
ulong FUN_00402d76(byte bParm1);
ulong FUN_00402d85(char *pcParm1);
void FUN_00402de4(void);
void FUN_00402dfe(void);
void FUN_00402e18(undefined *puParm1);
void FUN_00402fb4(undefined8 uParm1,undefined8 uParm2);
undefined8 FUN_00402fd9(undefined8 uParm1);
undefined FUN_00402fe7(int iParm1);;
undefined8 FUN_00402ff7(void);
void FUN_00403002(undefined8 uParm1,undefined8 uParm2);
void FUN_004030ac(void);
void FUN_0040313b(undefined8 uParm1,long lParm2);
char * FUN_004032a3(char *pcParm1);
void FUN_004032f3(void);
undefined8 FUN_0040335e(long lParm1);
void FUN_00403446(void);
ulong FUN_0040360e(ulong *puParm1,ulong uParm2);
ulong FUN_00403637(long *plParm1,long *plParm2);
void FUN_0040368c(undefined8 uParm1);
ulong FUN_004036a6(undefined8 uParm1,undefined8 uParm2);
void FUN_00403727(undefined8 *puParm1);
ulong FUN_00403760(uint uParm1);
void FUN_004037ee(void);
void FUN_00403808(void);
void FUN_00403845(int iParm1);
void FUN_00403861(void);
void FUN_00403883(void);
void FUN_00403948(char cParm1);
void FUN_00403b49(void);
void FUN_00403b59(void);
ulong FUN_00403b69(uint uParm1,undefined8 *puParm2);
undefined8 FUN_004041c2(undefined8 uParm1);
ulong FUN_00404227(uint uParm1,undefined8 uParm2);
ulong FUN_00405190(byte **ppbParm1,byte **ppbParm2,char cParm3,long *plParm4);
undefined8 FUN_004054da(void);
void FUN_00405583(void);
void FUN_0040592f(void);
void FUN_004059b4(char cParm1);
void FUN_004059e5(byte bParm1,undefined8 uParm2,undefined8 uParm3);
void FUN_00405a40(long lParm1,long lParm2,undefined uParm3);
void FUN_00405ad3(long lParm1,long lParm2,byte bParm3);
void FUN_00406075(undefined8 uParm1);
undefined8 FUN_004060b6(undefined8 *puParm1,undefined8 uParm2);
ulong FUN_00406110(char *pcParm1);
undefined8 FUN_004061b4(undefined8 uParm1);
undefined8 FUN_004061c2(void);
void FUN_004061e0(undefined8 *puParm1);
void FUN_00406261(void);
ulong FUN_00406329(uint uParm1);
ulong FUN_0040635f(undefined8 uParm1,long lParm2,char cParm3);
ulong FUN_0040640c(undefined8 uParm1,long lParm2);
ulong FUN_00406493(undefined8 uParm1,long lParm2);
ulong FUN_0040650a(char *pcParm1);
undefined8 FUN_00406572(long *plParm1,int iParm2,long lParm3,byte bParm4,char *pcParm5);
ulong FUN_00407172(long lParm1);
void FUN_004071a9(undefined8 uParm1,long lParm2,byte bParm3);
undefined8 FUN_00407206(long lParm1,char *pcParm2);
void FUN_004072d8(undefined8 uParm1);
void FUN_00407302(long lParm1,byte bParm2);
ulong FUN_004074ce(undefined8 uParm1,undefined8 uParm2);
ulong FUN_0040757d(undefined8 *puParm1,undefined8 *puParm2,code *pcParm3);
ulong FUN_0040760c(undefined8 *puParm1,undefined8 *puParm2,code *pcParm3);
ulong FUN_0040769b(undefined8 *puParm1,undefined8 *puParm2,code *pcParm3);
ulong FUN_0040772a(undefined8 *puParm1,undefined8 *puParm2,code *pcParm3);
void FUN_0040779d(undefined8 *puParm1,undefined8 *puParm2,code *pcParm3);
ulong FUN_004077cd(undefined8 *puParm1,undefined8 *puParm2,code *pcParm3);
void FUN_0040786b(undefined8 uParm1,undefined8 uParm2);
void FUN_00407895(undefined8 uParm1,undefined8 uParm2);
void FUN_004078bf(undefined8 uParm1,undefined8 uParm2);
void FUN_004078e9(undefined8 uParm1,undefined8 uParm2);
undefined8 FUN_00407913(undefined8 uParm1,undefined8 uParm2);
undefined8 FUN_0040798b(undefined8 uParm1,undefined8 uParm2);
undefined8 FUN_00407a03(undefined8 uParm1,undefined8 uParm2);
undefined8 FUN_00407a7b(undefined8 uParm1,undefined8 uParm2);
void FUN_00407af3(undefined8 uParm1,undefined8 uParm2);
void FUN_00407b1d(undefined8 uParm1,undefined8 uParm2);
void FUN_00407b47(undefined8 uParm1,undefined8 uParm2);
void FUN_00407b71(undefined8 uParm1,undefined8 uParm2);
undefined8 FUN_00407b9b(undefined8 uParm1,undefined8 uParm2);
undefined8 FUN_00407c13(undefined8 uParm1,undefined8 uParm2);
undefined8 FUN_00407c8b(undefined8 uParm1,undefined8 uParm2);
undefined8 FUN_00407d03(undefined8 uParm1,undefined8 uParm2);
void FUN_00407d7b(undefined8 uParm1,undefined8 uParm2);
void FUN_00407da5(undefined8 uParm1,undefined8 uParm2);
void FUN_00407dcf(undefined8 uParm1,undefined8 uParm2);
void FUN_00407df9(undefined8 uParm1,undefined8 uParm2);
undefined8 FUN_00407e23(undefined8 uParm1,undefined8 uParm2);
undefined8 FUN_00407e9b(undefined8 uParm1,undefined8 uParm2);
undefined8 FUN_00407f13(undefined8 uParm1,undefined8 uParm2);
undefined8 FUN_00407f8b(undefined8 uParm1,undefined8 uParm2);
void FUN_00408003(undefined8 uParm1,undefined8 uParm2);
void FUN_0040802d(undefined8 uParm1,undefined8 uParm2);
void FUN_00408057(undefined8 uParm1,undefined8 uParm2);
void FUN_00408081(undefined8 uParm1,undefined8 uParm2);
undefined8 FUN_004080ab(undefined8 uParm1,undefined8 uParm2);
undefined8 FUN_00408123(undefined8 uParm1,undefined8 uParm2);
undefined8 FUN_0040819b(undefined8 uParm1,undefined8 uParm2);
undefined8 FUN_00408213(undefined8 uParm1,undefined8 uParm2);
void FUN_0040828b(undefined8 uParm1,undefined8 uParm2);
void FUN_004082b5(undefined8 uParm1,undefined8 uParm2);
void FUN_004082df(undefined8 uParm1,undefined8 uParm2);
void FUN_00408309(undefined8 uParm1,undefined8 uParm2);
undefined8 FUN_00408333(undefined8 uParm1,undefined8 uParm2);
undefined8 FUN_004083ab(undefined8 uParm1,undefined8 uParm2);
undefined8 FUN_00408423(undefined8 uParm1,undefined8 uParm2);
undefined8 FUN_0040849b(undefined8 uParm1,undefined8 uParm2);
void FUN_00408513(undefined8 uParm1,undefined8 uParm2);
void FUN_0040853d(undefined8 uParm1,undefined8 uParm2);
void FUN_00408567(undefined8 uParm1,undefined8 uParm2);
void FUN_00408591(undefined8 uParm1,undefined8 uParm2);
undefined8 FUN_004085bb(undefined8 uParm1,undefined8 uParm2);
undefined8 FUN_00408633(undefined8 uParm1,undefined8 uParm2);
undefined8 FUN_004086ab(undefined8 uParm1,undefined8 uParm2);
undefined8 FUN_00408723(undefined8 uParm1,undefined8 uParm2);
void FUN_0040879b(undefined8 *puParm1,undefined8 *puParm2);
void FUN_004087c6(undefined8 uParm1,undefined8 uParm2);
void FUN_004087eb(undefined8 uParm1,undefined8 uParm2);
undefined8 FUN_00408810(undefined8 uParm1,undefined8 uParm2);
undefined8 FUN_00408883(undefined8 uParm1,undefined8 uParm2);
void FUN_004088f6(void);
void FUN_00408955(void);
void FUN_00408a92(void);
void FUN_00408baf(undefined8 uParm1,undefined8 uParm2,byte bParm3,long lParm4,undefined8 uParm5,uint uParm6);
ulong FUN_00408c42(void);
void FUN_00408d00(long lParm1,undefined8 uParm2,uint uParm3);
void FUN_00408dcb(uint uParm1,uint uParm2,char cParm3);
void FUN_00408e25(uint uParm1,uint uParm2,char cParm3);
ulong FUN_00408e7f(long lParm1,undefined8 uParm2);
void FUN_00408ee1(uint uParm1);
void FUN_00408f1e(uint uParm1);
undefined1 * FUN_00408f5b(undefined8 uParm1,ulong uParm2,long lParm3);
void FUN_00408fce(long lParm1);
char * FUN_00409963(char **param_1,char *param_2,char *param_3,undefined8 param_4,int param_5,char **param_6,undefined *param_7);
long FUN_00409e4f(undefined *puParm1,undefined8 uParm2,uint uParm3);
char * FUN_00409f17(char *pcParm1,char cParm2);
long FUN_0040a011(char *param_1,undefined8 param_2,uint param_3,long param_4,char param_5,long param_6,long param_7);
long FUN_0040a3a3(undefined8 *puParm1,byte bParm2,undefined8 uParm3,ulong uParm4);
void FUN_0040a4e7(void);
long FUN_0040a523(long lParm1,undefined8 uParm2);
ulong FUN_0040a6c9(char cParm1,uint uParm2,int iParm3);
ulong FUN_0040a7fe(byte bParm1,uint uParm2,uint uParm3);
ulong FUN_0040a853(long lParm1);
ulong * FUN_0040a8a3(long *plParm1,char cParm2);
void FUN_0040ac71(undefined8 *puParm1);
long FUN_0040acd2(undefined8 *puParm1);
void FUN_0040ae80(void);
void FUN_0040afd6(void);
void FUN_0040b12f(char cParm1);
void FUN_0040b236(ulong uParm1,ulong uParm2);
void FUN_0040b2db(char *pcParm1,char *pcParm2,char *pcParm3);
void FUN_0040b392(void);
ulong FUN_0040b5ba(char cParm1);
long FUN_0040b855(uint uParm1);
long FUN_0040bb19(undefined8 uParm1,ulong uParm2);
void FUN_0040bc6b(void);
long FUN_0040bc7b(undefined8 uParm1,long lParm2,long lParm3,long lParm4);
void FUN_0040bdaa(undefined8 uParm1,undefined8 uParm2,long lParm3);
void FUN_0040be2b(long lParm1,long lParm2,long lParm3);
long FUN_0040bf61(undefined8 uParm1,undefined8 uParm2,undefined8 uParm3,undefined8 uParm4,undefined8 uParm5,code *pcParm6);
undefined8 FUN_0040bfe7(int iParm1);
undefined8 FUN_0040c01b(int iParm1);
ulong FUN_0040c045(int iParm1);
ulong FUN_0040c065(uint uParm1);
ulong FUN_0040c084(long *plParm1,undefined8 uParm2,undefined8 uParm3);
char * FUN_0040c12e(char *pcParm1,uint uParm2);
void FUN_0040c934(void);
ulong FUN_0040ca19(char *pcParm1);
char * FUN_0040caa2(char *pcParm1);
ulong FUN_0040cb0b(long lParm1);
undefined8 FUN_0040cb59(void);
void FUN_0040cb6c(long lParm1,undefined8 uParm2,undefined8 *puParm3);
ulong FUN_0040cc07(long lParm1,undefined8 uParm2,undefined8 *puParm3);
undefined8 FUN_0040cc63(uint uParm1);
void FUN_0040cd0e(uint uParm1,undefined *puParm2);
void FUN_0040ced9(long lParm1,undefined8 uParm2);
long FUN_0040cf00(undefined8 uParm1,undefined8 uParm2,undefined8 uParm3);
char * FUN_0040cf41(char *pcParm1);
long FUN_0040cf61(long lParm1,char *pcParm2,undefined8 *puParm3);
char * FUN_0040d0ae(char **ppcParm1);
ulong FUN_0040d189(byte bParm1);
ulong FUN_0040d1d9(long lParm1,ulong uParm2,long lParm3,ulong uParm4);
ulong FUN_0040d3f7(char *pcParm1,char *pcParm2);
void FUN_0040d630(undefined8 *puParm1);
ulong FUN_0040d684(uint uParm1);
undefined8 FUN_0040d755(long lParm1);
long FUN_0040d767(long *plParm1,undefined8 uParm2);
long FUN_0040d7be(long lParm1,long lParm2);
ulong FUN_0040d851(ulong uParm1);
ulong FUN_0040d8bd(ulong uParm1);
ulong FUN_0040d904(undefined8 uParm1,ulong uParm2);
ulong FUN_0040d93b(ulong uParm1,ulong uParm2);
undefined8 FUN_0040d954(long lParm1);
ulong FUN_0040da4d(ulong uParm1,long lParm2);
long * FUN_0040db43(undefined8 uParm1,undefined1 *puParm2,code *pcParm3,code *pcParm4,long lParm5);
void FUN_0040dcab(long **pplParm1,undefined8 uParm2);
long FUN_0040ddd5(long lParm1);
void FUN_0040de20(long lParm1,undefined8 *puParm2);
long FUN_0040de55(long lParm1,long lParm2,long **pplParm3,char cParm4);
undefined8 FUN_0040dfea(long lParm1,long **pplParm2,char cParm3);
undefined8 FUN_0040e1b8(long *plParm1,undefined8 uParm2);
undefined8 FUN_0040e3bc(long lParm1,long lParm2,long *plParm3);
undefined8 FUN_0040e70e(undefined8 uParm1,undefined8 uParm2);
long FUN_0040e757(long lParm1,undefined8 uParm2);
ulong FUN_0040ea36(undefined8 *puParm1,ulong uParm2);
ulong FUN_0040ea82(undefined8 *puParm1,undefined8 *puParm2);
void FUN_0040eafa(undefined8 *puParm1);
void FUN_0040eb2b(void);
long FUN_0040ec22(long lParm1,ulong uParm2,byte *pbParm3,undefined8 uParm4);
char * FUN_0040ed1e(ulong uParm1,long lParm2,uint uParm3,ulong uParm4,ulong uParm5);
undefined8 FUN_0040f6af(void);
ulong FUN_0040f6d0(char *pcParm1,undefined8 *puParm2,uint *puParm3);
ulong FUN_0040f83d(undefined8 uParm1,undefined8 uParm2,long *plParm3);
uint * FUN_0040f892(uint uParm1);
uint * FUN_0040f983(uint uParm1);
char * FUN_0040fa74(long lParm1,long lParm2);
char * FUN_0040fba6(ulong uParm1,long lParm2);
ulong FUN_0040fc2b(uint *puParm1);
long FUN_0040fc79(uint *puParm1,ulong uParm2);
undefined * FUN_0040fcfe(undefined *puParm1,undefined *puParm2,long lParm3);
long FUN_0040fd47(long lParm1,long lParm2,long lParm3,ulong *puParm4,int iParm5,uint uParm6);
void FUN_0041010d(undefined8 uParm1,uint uParm2);
ulong FUN_0041013f(byte *pbParm1,long lParm2,uint uParm3);
void FUN_00410378(undefined8 *puParm1,ulong uParm2,undefined8 *puParm3,code *pcParm4);
void FUN_0041051a(undefined8 *puParm1,ulong uParm2,undefined8 *puParm3,code *pcParm4);
void FUN_0041075f(long lParm1,long lParm2,undefined8 uParm3);
long FUN_004107a3(long lParm1,long lParm2,long lParm3);
long FUN_004107fc(long lParm1,long lParm2,long lParm3);
ulong FUN_00410855(int iParm1,int iParm2);
void FUN_004108a6(undefined8 uParm1,undefined8 uParm2,undefined8 uParm3,undefined8 uParm4,undefined8 uParm5,uint uParm6);
char * FUN_004108fc(char *param_1,long param_2,char *param_3,undefined8 *param_4,byte param_5,undefined8 param_6,undefined8 param_7,uint param_8);
void FUN_00412a21(long lParm1);
undefined8 FUN_00412afe(undefined1 *puParm1);
ulong FUN_00412b4a(undefined1 *puParm1);
void FUN_00412b68(undefined1 *puParm1,undefined4 uParm2);
ulong FUN_00412b8c(undefined1 *puParm1,byte bParm2,uint uParm3);
undefined8 * FUN_00412c14(undefined8 *puParm1,int iParm2);
char * FUN_00412c8b(char *pcParm1,int iParm2);
ulong FUN_00412d2b(long param_1,ulong param_2,long param_3,ulong param_4,uint param_5,uint param_6,long param_7,char *param_8,char *param_9);
undefined8 FUN_00413bf5(undefined8 uParm1,undefined8 uParm2,undefined8 uParm3,undefined8 uParm4,undefined1 *puParm5);
undefined1 * FUN_00413c97(int iParm1,undefined8 uParm2,undefined8 uParm3,uint *puParm4);
void FUN_00413f0a(uint uParm1,uint uParm2,undefined8 uParm3);
void FUN_00413f4b(uint uParm1,undefined8 uParm2);
void FUN_00413f6f(undefined8 uParm1,undefined8 uParm2,char cParm3);
void FUN_00414003(undefined8 uParm1,char cParm2);
void FUN_0041402d(undefined8 uParm1);
void FUN_0041404c(uint uParm1,uint uParm2,undefined8 uParm3);
void FUN_004140e7(uint uParm1,undefined8 uParm2,undefined8 uParm3);
void FUN_00414113(uint uParm1,undefined8 uParm2);
void FUN_0041413c(undefined8 uParm1);
void FUN_0041415b(void);
undefined8 FUN_00414165(void);
undefined8 FUN_00414187(void);
void FUN_004141a9(long lParm1);
void FUN_004141bf(long lParm1);
void FUN_004141d5(long lParm1);
ulong FUN_004141eb(uint uParm1);
ulong FUN_004141fb(long lParm1,int iParm2,long lParm3,int iParm4);
void FUN_00414282(undefined8 uParm1,long lParm2,undefined8 uParm3,undefined8 uParm4,undefined8 *puParm5,undefined8 uParm6);
void FUN_004146e6(undefined8 uParm1,undefined8 uParm2,undefined8 uParm3,undefined8 uParm4,uint *puParm5);
void FUN_004147b8(undefined4 uParm1,undefined4 uParm2,undefined4 uParm3,undefined4 uParm4,undefined4 uParm5,undefined4 uParm6,undefined4 uParm7,undefined4 uParm8,undefined8 param_1,undefined8 param_2,undefined8 param_3,undefined8 param_4,undefined8 param_5,undefined8 param_6);
void FUN_0041486e(ulong uParm1,ulong uParm2);
void FUN_004148af(undefined8 uParm1,ulong uParm2,ulong uParm3);
void FUN_004148fe(long lParm1,ulong *puParm2,ulong uParm3);
void FUN_004149c9(undefined8 uParm1);
long FUN_004149e3(long lParm1);
long FUN_00414a18(long lParm1,long lParm2);
void FUN_00414a79(undefined8 uParm1,undefined8 uParm2);
void FUN_00414aa3(undefined8 uParm1,undefined8 uParm2);
void FUN_00414ad7(undefined8 uParm1);
char * FUN_00414b04(void);
ulong FUN_00414b2e(undefined8 param_1,uint param_2,ulong param_3,ulong param_4,undefined8 param_5,undefined8 param_6,uint param_7);
long FUN_00414c40(void);
long FUN_00414c79(void);
undefined8 FUN_00414d65(int iParm1);
ulong FUN_00414d8b(ulong *puParm1,int iParm2);
ulong FUN_00414dea(undefined8 uParm1,uint uParm2,int iParm3);
ulong FUN_00414e2b(byte *pbParm1,byte **ppbParm2,uint uParm3,undefined8 *puParm4,long lParm5);
void FUN_0041520c(uint uParm1,int iParm2,undefined uParm3,long lParm4,undefined8 uParm5,uint uParm6);
undefined8 FUN_004152db(uint uParm1,uint uParm2,char cParm3,undefined8 uParm4,undefined8 uParm5);
undefined8 FUN_00415321(int iParm1);
ulong FUN_00415347(ulong *puParm1,int iParm2);
ulong FUN_004153a6(undefined8 uParm1,uint uParm2,int iParm3);
ulong FUN_004153e7(byte *pbParm1,byte **ppbParm2,uint uParm3,undefined8 *puParm4,long lParm5);
undefined8 FUN_004157c8(ulong uParm1,ulong uParm2);
ulong FUN_00415847(uint uParm1);
void FUN_00415870(void);
void FUN_004158a4(uint uParm1);
void FUN_00415918(uint uParm1,uint uParm2,undefined8 uParm3,undefined8 uParm4);
void FUN_0041599c(undefined4 uParm1,undefined4 uParm2,undefined4 uParm3,undefined4 uParm4,undefined4 uParm5,undefined4 uParm6,undefined4 uParm7,undefined4 uParm8,uint param_1,uint param_2,undefined8 param_3,undefined8 param_4,undefined8 param_5,undefined8 param_6);
ulong FUN_00415a80(byte *pbParm1,byte *pbParm2,byte *pbParm3,byte bParm4,uint uParm5);
char * FUN_00416673(char *pcParm1);
undefined8 FUN_004167b8(int iParm1,long lParm2,ulong uParm3,ulong uParm4,byte bParm5,uint uParm6);
undefined8 FUN_0041702b(int *piParm1);
ulong FUN_004170c3(uint *puParm1,uint *puParm2,uint *puParm3,byte bParm4,uint uParm5);
int * FUN_00417c51(int *piParm1);
undefined8 FUN_00417d94(int iParm1,long lParm2,ulong uParm3,ulong uParm4,byte bParm5,uint uParm6);
ulong FUN_0041866b(undefined8 uParm1,long lParm2,uint uParm3);
long FUN_00418967(long lParm1,undefined *puParm2);
void FUN_00418ef0(long lParm1,int *piParm2);
ulong FUN_004190d4(int param_1,undefined8 *param_2,char *param_3,long *param_4,int *param_5,int param_6,int *param_7,int param_8,undefined8 param_9);
char * FUN_004196be(undefined8 uParm1,undefined8 uParm2,char *pcParm3,int *piParm4,int iParm5);
ulong FUN_0041978c(uint param_1,undefined8 *param_2,char *param_3,long param_4,undefined8 param_5,uint param_6,uint *param_7,uint param_8);
ulong FUN_00419f55(uint param_1,undefined8 param_2,undefined8 param_3,undefined8 param_4,undefined8 param_5,uint param_6,uint param_7);
void FUN_00419fe5(uint uParm1,undefined8 uParm2,undefined8 uParm3,undefined8 uParm4,undefined8 uParm5);
ulong FUN_0041a02f(undefined8 uParm1,undefined8 uParm2);
void FUN_0041a0d6(undefined8 uParm1,undefined8 uParm2);
ulong FUN_0041a0fb(long lParm1,long lParm2);
undefined8 FUN_0041a1b2(long lParm1);
ulong FUN_0041a1e3(uint *puParm1,byte *pbParm2,long lParm3,undefined8 uParm4);
ulong * FUN_0041a266(ulong *puParm1,char cParm2,ulong uParm3);
void FUN_0041a378(long lParm1,undefined8 uParm2);
void FUN_0041a3c8(long lParm1,undefined8 uParm2);
undefined8 FUN_0041a418(long *plParm1,long lParm2,long lParm3);
void FUN_0041a53d(long lParm1,undefined8 uParm2,undefined8 uParm3,undefined8 uParm4,undefined8 uParm5);
void FUN_0041a598(ulong *puParm1,long lParm2);
void FUN_0041a78d(void);
void FUN_0041a7bc(uint uParm1,undefined8 uParm2,uint uParm3,uint uParm4);
ulong FUN_0041a7ec(undefined4 uParm1,undefined4 uParm2,undefined4 uParm3,undefined4 uParm4,undefined4 uParm5,undefined4 uParm6,undefined4 uParm7,undefined4 uParm8,uint param_1,undefined8 param_2,uint param_3,undefined8 param_4,undefined8 param_5,undefined8 param_6);
undefined8 FUN_0041a9b3(long lParm1,undefined8 uParm2,undefined8 uParm3);
undefined8 FUN_0041aa59(long lParm1,long lParm2);
ulong FUN_0041aac2(undefined4 uParm1,undefined4 uParm2,undefined4 uParm3,undefined4 uParm4,undefined4 uParm5,undefined4 uParm6,undefined4 uParm7,undefined4 uParm8,long param_1,ulong param_2,undefined8 param_3,undefined8 param_4,undefined8 param_5,undefined8 param_6);
void FUN_0041ac53(undefined8 uParm1,undefined8 uParm2);
ulong FUN_0041ac78(long lParm1,long lParm2);
char * FUN_0041ad08(char *pcParm1,char *pcParm2,ulong uParm3);
ulong FUN_0041ae8e(int iParm1,int iParm2);
ulong FUN_0041aec9(uint *puParm1,uint *puParm2);
void FUN_0041af71(long lParm1,undefined8 uParm2,long lParm3);
undefined8 * FUN_0041afac(long lParm1);
undefined8 FUN_0041b05f(long **pplParm1,char *pcParm2);
void FUN_0041b22f(undefined8 *puParm1);
void FUN_0041b270(void);
void FUN_0041b280(long lParm1);
ulong FUN_0041b2b7(long lParm1);
long FUN_0041b2fd(long lParm1);
ulong FUN_0041b3c9(long lParm1);
undefined8 FUN_0041b434(long lParm1,undefined8 uParm2,undefined8 uParm3);
long FUN_0041b4e0(long lParm1,undefined8 uParm2);
void FUN_0041b5b5(long lParm1);
void FUN_0041b5e4(void);
ulong FUN_0041b66f(char *pcParm1);
ulong FUN_0041b6e3(void);
long FUN_0041b730(ulong uParm1,long lParm2,ulong uParm3,long lParm4,long *plParm5);
int * FUN_0041b97c(ulong uParm1,uint *puParm2,ulong uParm3,uint *puParm4,ulong *puParm5);
char * FUN_0041c45c(ulong uParm1,long lParm2,long lParm3);
long FUN_0041c6ca(int *param_1,long *param_2);
long FUN_0041c8b5(undefined auParm1 [16],int *piParm2,long *plParm3);
undefined8 FUN_0041cc74(int iParm1,long lParm2,uint *puParm3,long lParm4,uint uParm5);
void FUN_0041d487(uint param_1);
void FUN_0041d4d1(undefined8 uParm1,uint uParm2);
ulong FUN_0041d523(void);
ulong FUN_0041d8b5(undefined auParm1 [16],undefined8 uParm2,undefined8 uParm3);
ulong FUN_0041dc8d(char *pcParm1,long lParm2);
undefined8 FUN_0041dce6(long param_1,long param_2,undefined param_3,int param_4,undefined8 param_5,ulong param_6,int param_7,ulong param_8,int param_9);
ulong FUN_00425413(undefined8 uParm1,undefined8 uParm2,undefined8 uParm3);
ulong FUN_00425534(uint *puParm1,long lParm2);
void FUN_004255aa(uint uParm1);
ulong FUN_004255f2(ulong uParm1,undefined4 uParm2);
ulong FUN_0042560e(byte *pbParm1,byte *pbParm2);
undefined8 FUN_00425685(uint uParm1,char cParm2);
undefined8 FUN_00425700(undefined8 uParm1);
undefined8 FUN_0042578b(void);
ulong FUN_00425798(char *pcParm1,ulong uParm2);
undefined1 * FUN_004257f2(void);
char * FUN_00425ba5(void);
void FUN_00425c73(undefined8 uParm1,int *piParm2,undefined8 uParm3);
void FUN_00425d14(int *param_1);
undefined8 FUN_00425dd3(undefined8 uParm1);
undefined8 FUN_00425e99(uint uParm1,undefined8 uParm2);
ulong FUN_004260b2(ulong uParm1,long lParm2);
void FUN_004260e6(undefined8 uParm1,undefined8 uParm2,undefined8 uParm3);
void FUN_00426121(undefined8 uParm1,undefined8 uParm2,undefined8 uParm3,undefined8 uParm4);
ulong FUN_00426172(ulong uParm1,ulong uParm2);
ulong FUN_0042618d(undefined8 uParm1);
ulong FUN_00426245(undefined4 uParm1,undefined4 uParm2,undefined4 uParm3,undefined4 uParm4,undefined4 uParm5,undefined4 uParm6,undefined4 uParm7,undefined4 uParm8,uint param_1,uint param_2,undefined8 param_3,undefined8 param_4,undefined8 param_5,undefined8 param_6);
undefined8 FUN_004264fc(uint uParm1);
void FUN_0042655f(undefined8 uParm1);
void FUN_00426583(void);
ulong FUN_00426591(long lParm1);
undefined8 FUN_00426661(undefined8 uParm1);
void FUN_00426680(undefined8 uParm1,undefined8 uParm2,uint uParm3);
void FUN_004266ab(uint uParm1,undefined8 uParm2,undefined8 uParm3,uint uParm4);
void FUN_004266d8(uint uParm1,undefined8 uParm2,undefined8 uParm3,uint uParm4);
ulong FUN_00426715(uint uParm1,long lParm2,long lParm3,uint uParm4);
long FUN_00426828(long lParm1,undefined4 uParm2);
ulong FUN_004268a1(ulong uParm1);
ulong FUN_0042694c(int iParm1,int iParm2);
long FUN_00426987(ulong param_1,long param_2,int param_3,int param_4,int param_5,uint param_6,int param_7,int param_8,int param_9,int param_10);
long FUN_00426bda(undefined8 uParm1,undefined8 uParm2);
long FUN_00426c29(undefined8 param_1,undefined8 param_2,uint param_3,uint param_4,uint param_5,long param_6,uint *param_7);
void FUN_00426d92(code *pcParm1,undefined8 uParm2,undefined8 uParm3);
long FUN_00426dc4(undefined8 uParm1,long *plParm2,undefined8 uParm3);
long FUN_00426f07(undefined8 *puParm1,undefined8 uParm2,long *plParm3);
void FUN_004276f3(undefined8 uParm1);
undefined8 FUN_0042771c(uint *puParm1,ulong *puParm2);
undefined8 FUN_00427ebc(char *pcParm1,ulong *puParm2,char **ppcParm3);
ulong FUN_00429172(char *pcParm1,char *pcParm2,uint uParm3);
ulong FUN_00429305(undefined auParm1 [16]);
ulong FUN_00429340(void);
undefined8 * FUN_00429372(ulong uParm1);
void FUN_00429435(ulong uParm1);
void FUN_00429510(void);
undefined8 _DT_FINI(void);
undefined FUN_00639000();
undefined FUN_00639008();
undefined FUN_00639010();
undefined FUN_00639018();
undefined FUN_00639020();
undefined FUN_00639028();
undefined FUN_00639030();
undefined FUN_00639038();
undefined FUN_00639040();
undefined FUN_00639048();
undefined FUN_00639050();
undefined FUN_00639058();
undefined FUN_00639060();
undefined FUN_00639068();
undefined FUN_00639070();
undefined FUN_00639078();
undefined FUN_00639080();
undefined FUN_00639088();
undefined FUN_00639090();
undefined FUN_00639098();
undefined FUN_006390a0();
undefined FUN_006390a8();
undefined FUN_006390b0();
undefined FUN_006390b8();
undefined FUN_006390c0();
undefined FUN_006390c8();
undefined FUN_006390d0();
undefined FUN_006390d8();
undefined FUN_006390e0();
undefined FUN_006390e8();
undefined FUN_006390f0();
undefined FUN_006390f8();
undefined FUN_00639100();
undefined FUN_00639108();
undefined FUN_00639110();
undefined FUN_00639118();
undefined FUN_00639120();
undefined FUN_00639128();
undefined FUN_00639130();
undefined FUN_00639138();
undefined FUN_00639140();
undefined FUN_00639148();
undefined FUN_00639150();
undefined FUN_00639158();
undefined FUN_00639160();
undefined FUN_00639168();
undefined FUN_00639170();
undefined FUN_00639178();
undefined FUN_00639180();
undefined FUN_00639188();
undefined FUN_00639190();
undefined FUN_00639198();
undefined FUN_006391a0();
undefined FUN_006391a8();
undefined FUN_006391b0();
undefined FUN_006391b8();
undefined FUN_006391c0();
undefined FUN_006391c8();
undefined FUN_006391d0();
undefined FUN_006391d8();
undefined FUN_006391e0();
undefined FUN_006391e8();
undefined FUN_006391f0();
undefined FUN_006391f8();
undefined FUN_00639200();
undefined FUN_00639208();
undefined FUN_00639210();
undefined FUN_00639218();
undefined FUN_00639220();
undefined FUN_00639228();
undefined FUN_00639230();
undefined FUN_00639238();
undefined FUN_00639240();
undefined FUN_00639248();
undefined FUN_00639250();
undefined FUN_00639258();
undefined FUN_00639260();
undefined FUN_00639268();
undefined FUN_00639270();
undefined FUN_00639278();
undefined FUN_00639280();
undefined FUN_00639288();
undefined FUN_00639290();
undefined FUN_00639298();
undefined FUN_006392a0();
undefined FUN_006392a8();
undefined FUN_006392b0();
undefined FUN_006392b8();
undefined FUN_006392c0();
undefined FUN_006392c8();
undefined FUN_006392d0();
undefined FUN_006392d8();
undefined FUN_006392e0();
undefined FUN_006392e8();
undefined FUN_006392f0();
undefined FUN_006392f8();
undefined FUN_00639300();
undefined FUN_00639308();
undefined FUN_00639310();
undefined FUN_00639318();
undefined FUN_00639320();
undefined FUN_00639328();
undefined FUN_00639330();
undefined FUN_00639338();
undefined FUN_00639340();
undefined FUN_00639348();
undefined FUN_00639350();
undefined FUN_00639358();
undefined FUN_00639360();
undefined FUN_00639368();
undefined FUN_00639370();
undefined FUN_00639378();
undefined FUN_00639380();
undefined FUN_00639388();
undefined FUN_00639390();
undefined FUN_00639398();
undefined FUN_006393a0();
undefined FUN_006393a8();
undefined FUN_006393b0();
undefined FUN_006393b8();
undefined FUN_006393c0();
undefined FUN_006393c8();
undefined FUN_006393d0();
undefined FUN_006393d8();


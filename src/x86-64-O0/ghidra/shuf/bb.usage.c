
#include "shuf.h"

long null_ARRAY_0061b59_0_8_;
long null_ARRAY_0061b59_8_8_;
long null_ARRAY_0061b70_0_8_;
long null_ARRAY_0061b70_16_8_;
long null_ARRAY_0061b70_24_8_;
long null_ARRAY_0061b70_32_8_;
long null_ARRAY_0061b70_40_8_;
long null_ARRAY_0061b70_48_8_;
long null_ARRAY_0061b70_8_8_;
long null_ARRAY_0061b84_0_4_;
long null_ARRAY_0061b84_16_8_;
long null_ARRAY_0061b84_4_4_;
long null_ARRAY_0061b84_8_4_;
long DAT_00417043;
long DAT_004170fd;
long DAT_0041717b;
long DAT_004174ae;
long DAT_00417782;
long DAT_00417784;
long DAT_00417786;
long DAT_00417789;
long DAT_004177c2;
long DAT_00417810;
long DAT_0041795e;
long DAT_00417962;
long DAT_00417967;
long DAT_00417969;
long DAT_00417fec;
long DAT_00418003;
long DAT_004183c0;
long DAT_00418ad0;
long DAT_00418ad5;
long DAT_00418b3f;
long DAT_00418bd0;
long DAT_00418bd3;
long DAT_00418c21;
long DAT_00418c25;
long DAT_00418d48;
long DAT_00418d49;
long DAT_0061b108;
long DAT_0061b118;
long DAT_0061b128;
long DAT_0061b568;
long DAT_0061b580;
long DAT_0061b5f8;
long DAT_0061b5fc;
long DAT_0061b600;
long DAT_0061b640;
long DAT_0061b648;
long DAT_0061b650;
long DAT_0061b660;
long DAT_0061b668;
long DAT_0061b680;
long DAT_0061b688;
long DAT_0061b6d0;
long DAT_0061b6d8;
long DAT_0061b6e0;
long DAT_0061b8b8;
long DAT_0061b8c0;
long DAT_0061b8c8;
long DAT_0061b8d0;
long DAT_0061b8e0;
long fde_00419c60;
long FLOAT_UNKNOWN;
long null_ARRAY_00417540;
long null_ARRAY_00418c30;
long null_ARRAY_00419180;
long null_ARRAY_0061b590;
long null_ARRAY_0061b5c0;
long null_ARRAY_0061b6a0;
long null_ARRAY_0061b700;
long null_ARRAY_0061b740;
long null_ARRAY_0061b840;
long PTR_DAT_0061b560;
long PTR_null_ARRAY_0061b5a0;
long PTR_null_ARRAY_0061b608;
long stack0x00000008;
void
FUN_004022a6 (uint uParm1)
{
  undefined8 uVar1;
  long lVar2;
  undefined *puVar3;
  undefined extraout_DL;
  int iVar4;
  ulong uVar5;
  int iStack52;
  long lStack48;
  undefined *puStack40;

  if (uParm1 == 0)
    {
      func_0x004019b0
	("Usage: %s [OPTION]... [FILE]\n  or:  %s -e [OPTION]... [ARG]...\n  or:  %s -i LO-HI [OPTION]...\n",
	 DAT_0061b6e0, DAT_0061b6e0, DAT_0061b6e0);
      func_0x00401bb0
	("Write a random permutation of the input lines to standard output.\n",
	 DAT_0061b640);
      FUN_0040206e ();
      FUN_00402088 ();
      func_0x00401bb0
	("  -e, --echo                treat each ARG as an input line\n  -i, --input-range=LO-HI   treat each number LO through HI as an input line\n  -n, --head-count=COUNT    output at most COUNT lines\n  -o, --output=FILE         write result to FILE instead of standard output\n      --random-source=FILE  get random bytes from FILE\n  -r, --repeat              output lines can be repeated\n",
	 DAT_0061b640);
      func_0x00401bb0
	("  -z, --zero-terminated     line delimiter is NUL, not newline\n",
	 DAT_0061b640);
      func_0x00401bb0 ("      --help     display this help and exit\n",
		       DAT_0061b640);
      iVar4 = (int) DAT_0061b640;
      func_0x00401bb0
	("      --version  output version information and exit\n");
      imperfection_wrapper ();	//    FUN_004020a2();
    }
  else
    {
      iVar4 = 0x4171b8;
      func_0x00401ba0 (DAT_0061b660,
		       "Try \'%s --help\' for more information.\n",
		       DAT_0061b6e0);
    }
  uVar5 = (ulong) uParm1;
  func_0x00401de0 ();
  lStack48 = (long) iVar4;
  iStack52 = 0;
  while (iStack52 < iVar4)
    {
      lVar2 = func_0x00401e40 (*(undefined8 *) (uVar5 + (long) iStack52 * 8));
      lStack48 = lStack48 + lVar2;
      iStack52 = iStack52 + 1;
    }
  puStack40 = (undefined *) FUN_004072a9 (lStack48);
  iStack52 = 0;
  while (iStack52 < iVar4)
    {
      uVar1 = *(undefined8 *) (uVar5 + (long) iStack52 * 8);
      puVar3 = (undefined *) func_0x00401980 (puStack40, uVar1, uVar1);
      *(undefined **) ((long) iStack52 * 8 + uVar5) = puStack40;
      puStack40 = puVar3 + 1;
      *puVar3 = extraout_DL;
      iStack52 = iStack52 + 1;
    }
  *(undefined **) ((long) iVar4 * 8 + uVar5) = puStack40;
  return;
}


#include "head.h"

long null_ARRAY_00617a5_0_8_;
long null_ARRAY_00617a5_8_8_;
long null_ARRAY_00617bc_0_8_;
long null_ARRAY_00617bc_16_8_;
long null_ARRAY_00617bc_24_8_;
long null_ARRAY_00617bc_32_8_;
long null_ARRAY_00617bc_40_8_;
long null_ARRAY_00617bc_48_8_;
long null_ARRAY_00617bc_8_8_;
long null_ARRAY_00617d0_0_4_;
long null_ARRAY_00617d0_16_8_;
long null_ARRAY_00617d0_4_4_;
long null_ARRAY_00617d0_8_4_;
long DAT_00413e83;
long DAT_00413f3d;
long DAT_00413fbb;
long DAT_004145f1;
long DAT_00414624;
long DAT_004146ae;
long DAT_00414878;
long DAT_004148c0;
long DAT_00414a1e;
long DAT_00414a22;
long DAT_00414a27;
long DAT_00414a29;
long DAT_00415093;
long DAT_00415460;
long DAT_004157f8;
long DAT_004157fd;
long DAT_00415867;
long DAT_004158f8;
long DAT_004158fb;
long DAT_00415949;
long DAT_0041594d;
long DAT_004159c0;
long DAT_004159c1;
long DAT_00617638;
long DAT_00617648;
long DAT_00617658;
long DAT_00617a20;
long DAT_00617a30;
long DAT_00617a40;
long DAT_00617ab8;
long DAT_00617abc;
long DAT_00617ac0;
long DAT_00617b00;
long DAT_00617b10;
long DAT_00617b20;
long DAT_00617b28;
long DAT_00617b40;
long DAT_00617b48;
long DAT_00617b90;
long DAT_00617b91;
long DAT_00617b92;
long DAT_00617b93;
long DAT_00617b98;
long DAT_00617ba0;
long DAT_00617ba8;
long DAT_00617d38;
long DAT_00617d40;
long DAT_00617d48;
long DAT_00617d58;
long fde_00416760;
long FLOAT_UNKNOWN;
long null_ARRAY_00414080;
long null_ARRAY_00414850;
long null_ARRAY_00415e00;
long null_ARRAY_00617a50;
long null_ARRAY_00617a80;
long null_ARRAY_00617b60;
long null_ARRAY_00617bc0;
long null_ARRAY_00617c00;
long null_ARRAY_00617d00;
long PTR_DAT_00617a28;
long PTR_null_ARRAY_00617a60;
void
FUN_00401dcd (int iParm1)
{
  uint *puVar1;
  undefined8 uVar2;
  char *pcVar3;

  if (iParm1 == 0)
    {
      func_0x004015d0 ("Usage: %s [OPTION]... [FILE]...\n", DAT_00617ba8);
      func_0x004015d0
	("Print the first %d lines of each FILE to standard output.\nWith more than one FILE, precede each with a header giving the file name.\n",
	 10);
      FUN_00401bbe ();
      FUN_00401bd8 ();
      func_0x004015d0
	("  -c, --bytes=[-]NUM       print the first NUM bytes of each file;\n                             with the leading \'-\', print all but the last\n                             NUM bytes of each file\n  -n, --lines=[-]NUM       print the first NUM lines instead of the first %d;\n                             with the leading \'-\', print all but the last\n                             NUM lines of each file\n",
	 10);
      func_0x00401790
	("  -q, --quiet, --silent    never print headers giving file names\n  -v, --verbose            always print headers giving file names\n",
	 DAT_00617b00);
      func_0x00401790
	("  -z, --zero-terminated    line delimiter is NUL, not newline\n",
	 DAT_00617b00);
      func_0x00401790 ("      --help     display this help and exit\n",
		       DAT_00617b00);
      func_0x00401790
	("      --version  output version information and exit\n",
	 DAT_00617b00);
      pcVar3 = DAT_00617b00;
      func_0x00401790
	("\nNUM may have a multiplier suffix:\nb 512, kB 1000, K 1024, MB 1000*1000, M 1024*1024,\nGB 1000*1000*1000, G 1024*1024*1024, and so on for T, P, E, Z, Y.\n");
      imperfection_wrapper ();	//    FUN_00401bf2();
    }
  else
    {
      pcVar3 = "Try \'%s --help\' for more information.\n";
      func_0x00401780 (DAT_00617b20,
		       "Try \'%s --help\' for more information.\n",
		       DAT_00617ba8);
    }
  func_0x00401930 ();
  if (iParm1 == 1)
    {
      imperfection_wrapper ();	//    uVar2 = FUN_004053bb(4, pcVar3);
      puVar1 = (uint *) func_0x00401920 ();
      imperfection_wrapper ();	//    FUN_00406523(0, (ulong)*puVar1, "error reading %s", uVar2);
    }
  else
    {
      if (iParm1 == 2)
	{
	  imperfection_wrapper ();	//      uVar2 = FUN_004054bc(0, 3, pcVar3);
	  puVar1 = (uint *) func_0x00401920 ();
	  imperfection_wrapper ();	//      FUN_00406523(0, (ulong)*puVar1, "%s: file has shrunk too much", uVar2);
	}
      else
	{
	  func_0x00401710 ();
	}
    }
  return;
}

typedef unsigned char   undefined;

typedef unsigned char    byte;
typedef unsigned char    dwfenc;
typedef unsigned int    dword;
typedef unsigned long    qword;
typedef unsigned char    undefined1;
typedef unsigned short    undefined2;
typedef unsigned int    undefined4;
typedef unsigned long    undefined5;
typedef unsigned long    undefined6;
typedef unsigned long    undefined8;
typedef unsigned short    word;
#define unkbyte9   unsigned long long
#define unkbyte10   unsigned long long
#define unkbyte11   unsigned long long
#define unkbyte12   unsigned long long
#define unkbyte13   unsigned long long
#define unkbyte14   unsigned long long
#define unkbyte15   unsigned long long
#define unkbyte16   unsigned long long

#define unkuint9   unsigned long long
#define unkuint10   unsigned long long
#define unkuint11   unsigned long long
#define unkuint12   unsigned long long
#define unkuint13   unsigned long long
#define unkuint14   unsigned long long
#define unkuint15   unsigned long long
#define unkuint16   unsigned long long

#define unkint9   long long
#define unkint10   long long
#define unkint11   long long
#define unkint12   long long
#define unkint13   long long
#define unkint14   long long
#define unkint15   long long
#define unkint16   long long

#define unkfloat1   float
#define unkfloat2   float
#define unkfloat3   float
#define unkfloat5   double
#define unkfloat6   double
#define unkfloat7   double
#define unkfloat9   long double
#define unkfloat11   long double
#define unkfloat12   long double
#define unkfloat13   long double
#define unkfloat14   long double
#define unkfloat15   long double
#define unkfloat16   long double

#define BADSPACEBASE   void
#define code   void

#define ulong long
#define true 1
#define bool int
#define false 0
#define float10 int
#define ushort int
#define undefined7 long
#define undefined2 long
#define uint5 long
#define uint6 long

// Add definition for uint
typedef unsigned int    uint;

#define sbyte char

extern imperfection_wrapper(void);
typedef struct eh_frame_hdr eh_frame_hdr, *Peh_frame_hdr;

struct eh_frame_hdr {
    byte eh_frame_hdr_version; // Exception Handler Frame Header Version
    dwfenc eh_frame_pointer_encoding; // Exception Handler Frame Pointer Encoding
    dwfenc eh_frame_desc_entry_count_encoding; // Encoding of # of Exception Handler FDEs
    dwfenc eh_frame_table_encoding; // Exception Handler Table Encoding
};

typedef struct fde_table_entry fde_table_entry, *Pfde_table_entry;

struct fde_table_entry {
    dword initial_loc; // Initial Location
    dword data_loc; // Data location
};

typedef enum Elf64_DynTag {
    DT_AUDIT=1879047932,
    DT_AUXILIARY=2147483645,
    DT_BIND_NOW=24,
    DT_CHECKSUM=1879047672,
    DT_CONFIG=1879047930,
    DT_DEBUG=21,
    DT_DEPAUDIT=1879047931,
    DT_ENCODING=32,
    DT_FEATURE_1=1879047676,
    DT_FILTER=2147483647,
    DT_FINI=13,
    DT_FINI_ARRAY=26,
    DT_FINI_ARRAYSZ=28,
    DT_FLAGS=30,
    DT_FLAGS_1=1879048187,
    DT_GNU_CONFLICT=1879047928,
    DT_GNU_CONFLICTSZ=1879047670,
    DT_GNU_HASH=1879047925,
    DT_GNU_LIBLIST=1879047929,
    DT_GNU_LIBLISTSZ=1879047671,
    DT_GNU_PRELINKED=1879047669,
    DT_HASH=4,
    DT_INIT=12,
    DT_INIT_ARRAY=25,
    DT_INIT_ARRAYSZ=27,
    DT_JMPREL=23,
    DT_MOVEENT=1879047674,
    DT_MOVESZ=1879047675,
    DT_MOVETAB=1879047934,
    DT_NEEDED=1,
    DT_NULL=0,
    DT_PLTGOT=3,
    DT_PLTPAD=1879047933,
    DT_PLTPADSZ=1879047673,
    DT_PLTREL=20,
    DT_PLTRELSZ=2,
    DT_POSFLAG_1=1879047677,
    DT_PREINIT_ARRAYSZ=33,
    DT_REL=17,
    DT_RELA=7,
    DT_RELACOUNT=1879048185,
    DT_RELAENT=9,
    DT_RELASZ=8,
    DT_RELCOUNT=1879048186,
    DT_RELENT=19,
    DT_RELSZ=18,
    DT_RPATH=15,
    DT_RUNPATH=29,
    DT_SONAME=14,
    DT_STRSZ=10,
    DT_STRTAB=5,
    DT_SYMBOLIC=16,
    DT_SYMENT=11,
    DT_SYMINENT=1879047679,
    DT_SYMINFO=1879047935,
    DT_SYMINSZ=1879047678,
    DT_SYMTAB=6,
    DT_TEXTREL=22,
    DT_TLSDESC_GOT=1879047927,
    DT_TLSDESC_PLT=1879047926,
    DT_VERDEF=1879048188,
    DT_VERDEFNUM=1879048189,
    DT_VERNEED=1879048190,
    DT_VERNEEDNUM=1879048191,
    DT_VERSYM=1879048176
} Elf64_DynTag;

typedef enum Elf_ProgramHeaderType {
    PT_DYNAMIC=2,
    PT_GNU_EH_FRAME=1685382480,
    PT_GNU_RELRO=1685382482,
    PT_GNU_STACK=1685382481,
    PT_INTERP=3,
    PT_LOAD=1,
    PT_NOTE=4,
    PT_NULL=0,
    PT_PHDR=6,
    PT_SHLIB=5,
    PT_TLS=7
} Elf_ProgramHeaderType;

typedef struct Elf64_Dyn Elf64_Dyn, *PElf64_Dyn;

struct Elf64_Dyn {
    enum Elf64_DynTag d_tag;
    qword d_val;
};

typedef struct Elf64_Shdr Elf64_Shdr, *PElf64_Shdr;

typedef enum Elf_SectionHeaderType {
    SHT_CHECKSUM=1879048184,
    SHT_DYNAMIC=6,
    SHT_DYNSYM=11,
    SHT_FINI_ARRAY=15,
    SHT_GNU_ATTRIBUTES=1879048181,
    SHT_GNU_HASH=1879048182,
    SHT_GNU_LIBLIST=1879048183,
    SHT_GNU_verdef=1879048189,
    SHT_GNU_verneed=1879048190,
    SHT_GNU_versym=1879048191,
    SHT_GROUP=17,
    SHT_HASH=5,
    SHT_INIT_ARRAY=14,
    SHT_NOBITS=8,
    SHT_NOTE=7,
    SHT_NULL=0,
    SHT_PREINIT_ARRAY=16,
    SHT_PROGBITS=1,
    SHT_REL=9,
    SHT_RELA=4,
    SHT_SHLIB=10,
    SHT_STRTAB=3,
    SHT_SUNW_COMDAT=1879048187,
    SHT_SUNW_move=1879048186,
    SHT_SUNW_syminfo=1879048188,
    SHT_SYMTAB=2,
    SHT_SYMTAB_SHNDX=18
} Elf_SectionHeaderType;

struct Elf64_Shdr {
    dword sh_name;
    enum Elf_SectionHeaderType sh_type;
    qword sh_flags;
    qword sh_addr;
    qword sh_offset;
    qword sh_size;
    dword sh_link;
    dword sh_info;
    qword sh_addralign;
    qword sh_entsize;
};

typedef struct Elf64_Phdr Elf64_Phdr, *PElf64_Phdr;

struct Elf64_Phdr {
    enum Elf_ProgramHeaderType p_type;
    dword p_flags;
    qword p_offset;
    qword p_vaddr;
    qword p_paddr;
    qword p_filesz;
    qword p_memsz;
    qword p_align;
};

typedef struct Elf64_Sym Elf64_Sym, *PElf64_Sym;

struct Elf64_Sym {
    dword st_name;
    byte st_info;
    byte st_other;
    word st_shndx;
    qword st_value;
    qword st_size;
};

typedef struct Elf64_Rela Elf64_Rela, *PElf64_Rela;

struct Elf64_Rela {
    qword r_offset; // location to apply the relocation action
    qword r_info; // the symbol table index and the type of relocation
    qword r_addend; // a constant addend used to compute the relocatable field value
};

typedef struct Elf64_Ehdr Elf64_Ehdr, *PElf64_Ehdr;

struct Elf64_Ehdr {
    byte e_ident_magic_num;
    char e_ident_magic_str[3];
    byte e_ident_class;
    byte e_ident_data;
    byte e_ident_version;
    byte e_ident_pad[9];
    word e_type;
    word e_machine;
    dword e_version;
    qword e_entry;
    qword e_phoff;
    qword e_shoff;
    dword e_flags;
    word e_ehsize;
    word e_phentsize;
    word e_phnum;
    word e_shentsize;
    word e_shnum;
    word e_shstrndx;
};




undefined8 _DT_INIT(void);
void FUN_00401790(void);
void entry(void);
void FUN_00401cd0(void);
void FUN_00401d50(void);
void FUN_00401dd0(void);
undefined8 FUN_00401e0e(int iParm1);
ulong FUN_00401e34(byte bParm1);
void FUN_00401e43(undefined *puParm1);
ulong FUN_00401fdf(long *plParm1,long lParm2,char cParm3);
ulong FUN_00402030(int iParm1);
ulong FUN_0040211f(char cParm1,char cParm2);
ulong FUN_00402139(undefined4 uParm1,byte bParm2);
void FUN_00402261(undefined8 *puParm1);
undefined8 FUN_0040228e(long lParm1,long *plParm2);
ulong FUN_004025f9(undefined8 uParm1,long lParm2);
byte * FUN_00402663(byte bParm1);
undefined8 FUN_004026c1(long lParm1,ulong uParm2);
void FUN_0040280a(long lParm1,undefined uParm2);
undefined8 FUN_00402891(long lParm1,byte bParm2,byte bParm3);
undefined8 FUN_0040299d(long lParm1,undefined8 uParm2,undefined8 uParm3);
void FUN_00402a51(long lParm1,undefined uParm2,undefined8 uParm3);
undefined8 FUN_00402ae8(long lParm1,undefined *puParm2,long lParm3);
undefined8 FUN_00402b8a(long *plParm1,ulong uParm2,char cParm3,ulong *puParm4);
undefined8 FUN_00402c42(long *plParm1,long lParm2,undefined *puParm3,long *plParm4,ulong *puParm5);
undefined8 FUN_00402e32(long *plParm1,long lParm2);
undefined8 FUN_00402ee6(long *plParm1,undefined8 uParm2);
void FUN_004032cb(long lParm1);
ulong FUN_004032f5(long *plParm1,undefined4 *puParm2);
ulong FUN_00403675(long lParm1);
void FUN_004036fd(long lParm1,long lParm2);
void FUN_00403930(long *plParm1);
void FUN_00403b4d(long lParm1);
void FUN_00403b89(long lParm1,ulong uParm2);
void FUN_00403bed(long *plParm1);
ulong FUN_00403c33(undefined8 uParm1,undefined8 uParm2);
void FUN_00403c96(long lParm1,long lParm2);
undefined8 FUN_00403dcb(long lParm1);
void FUN_00403e36(long lParm1,long lParm2);
void FUN_00404009(long lParm1,undefined8 uParm2,code *pcParm3);
long FUN_004041f9(undefined8 uParm1,undefined8 uParm2);
ulong FUN_0040424c(long lParm1,undefined8 uParm2);
ulong FUN_00404345(long lParm1,undefined8 uParm2);
void FUN_004043c3(long lParm1,char cParm2,long lParm3);
undefined8 FUN_0040445d(uint uParm1,undefined8 *puParm2);
void FUN_00404cb1(void);
void FUN_00404d96(uint uParm1,undefined8 uParm2,undefined8 uParm3,uint uParm4);
void FUN_00404dc6(long lParm1,uint uParm2);
void FUN_00404e00(long lParm1);
ulong FUN_00404edd(undefined1 *puParm1,byte bParm2,uint uParm3);
char * FUN_00404f65(char *pcParm1,int iParm2);
ulong FUN_00405005(long param_1,ulong param_2,long param_3,ulong param_4,uint param_5,uint param_6,long param_7,char *param_8,char *param_9);
undefined1 * FUN_00405ecf(int iParm1,undefined8 uParm2,undefined8 uParm3,uint *puParm4);
void FUN_00406142(undefined8 uParm1,undefined8 uParm2,char cParm3);
void FUN_004061d6(undefined8 uParm1,char cParm2);
void FUN_00406200(undefined8 uParm1);
void FUN_0040621f(uint uParm1,undefined8 uParm2,undefined8 uParm3);
void FUN_0040624b(uint uParm1,undefined8 uParm2);
void FUN_00406274(undefined8 uParm1);
long FUN_00406293(uint uParm1,undefined8 uParm2,undefined8 uParm3);
void FUN_00406304(undefined8 uParm1,long lParm2,undefined8 uParm3,undefined8 uParm4,undefined8 *puParm5,undefined8 uParm6);
void FUN_00406768(undefined8 uParm1,undefined8 uParm2,undefined8 uParm3,undefined8 uParm4,uint *puParm5);
void FUN_0040683a(undefined4 uParm1,undefined4 uParm2,undefined4 uParm3,undefined4 uParm4,undefined4 uParm5,undefined4 uParm6,undefined4 uParm7,undefined4 uParm8,undefined8 param_1,undefined8 param_2,undefined8 param_3,undefined8 param_4,undefined8 param_5,undefined8 param_6);
void FUN_004068f0(ulong uParm1,ulong uParm2);
void FUN_00406931(undefined8 uParm1);
long FUN_0040694b(long lParm1);
long FUN_00406980(long lParm1,long lParm2);
long FUN_004069e1(ulong uParm1,ulong uParm2);
void FUN_00406a33(void);
void FUN_00406a5d(void);
void FUN_00406a63(uint uParm1,uint uParm2);
undefined8 FUN_00406a8b(int iParm1);
ulong FUN_00406ab1(ulong *puParm1,int iParm2);
ulong FUN_00406b10(undefined8 uParm1,uint uParm2,int iParm3);
ulong FUN_00406b51(byte *pbParm1,byte **ppbParm2,uint uParm3,undefined8 *puParm4,long lParm5);
undefined8 FUN_00406f32(ulong uParm1,ulong uParm2);
ulong FUN_00406fb1(uint uParm1);
void FUN_00406fda(void);
void FUN_0040700e(uint uParm1);
void FUN_00407082(uint uParm1,uint uParm2,undefined8 uParm3,undefined8 uParm4);
void FUN_00407106(undefined4 uParm1,undefined4 uParm2,undefined4 uParm3,undefined4 uParm4,undefined4 uParm5,undefined4 uParm6,undefined4 uParm7,undefined4 uParm8,uint param_1,uint param_2,undefined8 param_3,undefined8 param_4,undefined8 param_5,undefined8 param_6);
void FUN_004071ea(long lParm1,int *piParm2);
ulong FUN_004073ce(int param_1,undefined8 *param_2,char *param_3,long *param_4,int *param_5,int param_6,int *param_7,int param_8,undefined8 param_9);
char * FUN_004079b8(undefined8 uParm1,undefined8 uParm2,char *pcParm3,int *piParm4,int iParm5);
ulong FUN_00407a86(uint param_1,undefined8 *param_2,char *param_3,long param_4,undefined8 param_5,uint param_6,uint *param_7,uint param_8);
ulong FUN_0040824f(uint param_1,undefined8 param_2,undefined8 param_3,undefined8 param_4,undefined8 param_5,uint param_6,uint param_7);
void FUN_004082df(uint uParm1,undefined8 uParm2,undefined8 uParm3,undefined8 uParm4,undefined8 uParm5);
undefined8 FUN_00408329(long lParm1);
ulong FUN_0040835a(uint *puParm1,byte *pbParm2,long lParm3,undefined8 uParm4);
undefined8 FUN_004083dd(long lParm1,long lParm2);
ulong FUN_00408446(undefined8 uParm1,undefined8 uParm2,undefined8 uParm3);
undefined8 FUN_00408567(void);
undefined8 FUN_00408578(void);
ulong FUN_00408586(uint uParm1,uint uParm2);
ulong FUN_004085bd(byte *pbParm1,byte *pbParm2);
undefined8 FUN_00408634(undefined8 uParm1);
undefined8 FUN_004086bf(void);
ulong FUN_004086cc(uint uParm1);
undefined1 * FUN_0040879d(void);
char * FUN_00408b50(void);
ulong FUN_00408c1e(undefined8 uParm1);
void FUN_00408cd6(undefined8 uParm1);
void FUN_00408cfa(void);
ulong FUN_00408d08(long lParm1);
undefined8 FUN_00408dd8(undefined8 uParm1);
void FUN_00408df7(undefined8 uParm1,undefined8 uParm2,uint uParm3);
ulong FUN_00408e22(void);
long FUN_00408e6f(ulong uParm1,long lParm2,ulong uParm3,long lParm4,long *plParm5);
int * FUN_004090bb(ulong uParm1,uint *puParm2,ulong uParm3,uint *puParm4,ulong *puParm5);
char * FUN_00409b9b(ulong uParm1,long lParm2,long lParm3);
long FUN_00409e09(int *param_1,long *param_2);
long FUN_00409ff4(undefined auParm1 [16],int *piParm2,long *plParm3);
undefined8 FUN_0040a3b3(int iParm1,long lParm2,uint *puParm3,long lParm4,uint uParm5);
void FUN_0040abc6(uint param_1);
void FUN_0040ac10(undefined8 uParm1,uint uParm2);
ulong FUN_0040ac62(void);
ulong FUN_0040aff4(undefined auParm1 [16],undefined8 uParm2,undefined8 uParm3);
ulong FUN_0040b3cc(char *pcParm1,long lParm2);
undefined8 FUN_0040b425(long param_1,long param_2,undefined param_3,int param_4,undefined8 param_5,ulong param_6,int param_7,ulong param_8,int param_9);
long FUN_0040b6e8(long lParm1,char **ppcParm2,long lParm3,undefined8 uParm4);
ulong FUN_00412b52(uint uParm1);
void FUN_00412b71(undefined8 uParm1,int *piParm2,undefined8 uParm3);
void FUN_00412c12(int *param_1);
ulong FUN_00412cd1(ulong uParm1,long lParm2);
void FUN_00412d05(undefined8 uParm1,undefined8 uParm2,undefined8 uParm3);
void FUN_00412d40(undefined8 uParm1,undefined8 uParm2,undefined8 uParm3,undefined8 uParm4);
ulong FUN_00412d91(ulong uParm1,ulong uParm2);
undefined8 FUN_00412dac(uint *puParm1,ulong *puParm2);
undefined8 FUN_0041354c(char *pcParm1,ulong *puParm2,char **ppcParm3);
ulong FUN_00414802(undefined auParm1 [16]);
ulong FUN_0041483d(void);
void FUN_00414870(void);
undefined8 _DT_FINI(void);
undefined FUN_0061a000();
undefined FUN_0061a008();
undefined FUN_0061a010();
undefined FUN_0061a018();
undefined FUN_0061a020();
undefined FUN_0061a028();
undefined FUN_0061a030();
undefined FUN_0061a038();
undefined FUN_0061a040();
undefined FUN_0061a048();
undefined FUN_0061a050();
undefined FUN_0061a058();
undefined FUN_0061a060();
undefined FUN_0061a068();
undefined FUN_0061a070();
undefined FUN_0061a078();
undefined FUN_0061a080();
undefined FUN_0061a088();
undefined FUN_0061a090();
undefined FUN_0061a098();
undefined FUN_0061a0a0();
undefined FUN_0061a0a8();
undefined FUN_0061a0b0();
undefined FUN_0061a0b8();
undefined FUN_0061a0c0();
undefined FUN_0061a0c8();
undefined FUN_0061a0d0();
undefined FUN_0061a0d8();
undefined FUN_0061a0e0();
undefined FUN_0061a0e8();
undefined FUN_0061a0f0();
undefined FUN_0061a0f8();
undefined FUN_0061a100();
undefined FUN_0061a108();
undefined FUN_0061a110();
undefined FUN_0061a118();
undefined FUN_0061a120();
undefined FUN_0061a128();
undefined FUN_0061a130();
undefined FUN_0061a138();
undefined FUN_0061a140();
undefined FUN_0061a148();
undefined FUN_0061a150();
undefined FUN_0061a158();
undefined FUN_0061a160();
undefined FUN_0061a168();
undefined FUN_0061a170();
undefined FUN_0061a178();
undefined FUN_0061a180();
undefined FUN_0061a188();
undefined FUN_0061a190();
undefined FUN_0061a198();
undefined FUN_0061a1a0();
undefined FUN_0061a1a8();
undefined FUN_0061a1b0();
undefined FUN_0061a1b8();
undefined FUN_0061a1c0();
undefined FUN_0061a1c8();
undefined FUN_0061a1d0();
undefined FUN_0061a1d8();
undefined FUN_0061a1e0();
undefined FUN_0061a1e8();
undefined FUN_0061a1f0();
undefined FUN_0061a1f8();
undefined FUN_0061a200();
undefined FUN_0061a208();
undefined FUN_0061a210();
undefined FUN_0061a218();
undefined FUN_0061a220();
undefined FUN_0061a228();
undefined FUN_0061a230();
undefined FUN_0061a238();
undefined FUN_0061a240();
undefined FUN_0061a248();
undefined FUN_0061a250();
undefined FUN_0061a258();
undefined FUN_0061a260();
undefined FUN_0061a268();
undefined FUN_0061a270();


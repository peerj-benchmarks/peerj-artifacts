
#include "tr.h"

long null_ARRAY_0061945_0_8_;
long null_ARRAY_0061945_8_8_;
long null_ARRAY_00619d4_0_8_;
long null_ARRAY_00619d4_16_8_;
long null_ARRAY_00619d4_24_8_;
long null_ARRAY_00619d4_32_8_;
long null_ARRAY_00619d4_40_8_;
long null_ARRAY_00619d4_48_8_;
long null_ARRAY_00619d4_8_8_;
long null_ARRAY_00619e8_0_4_;
long null_ARRAY_00619e8_16_8_;
long null_ARRAY_00619e8_4_4_;
long null_ARRAY_00619e8_8_4_;
long DAT_004148c0;
long DAT_0041497d;
long DAT_004149fb;
long DAT_00415568;
long DAT_0041567f;
long DAT_00415681;
long DAT_00415684;
long DAT_00415687;
long DAT_0041568a;
long DAT_0041568d;
long DAT_00415690;
long DAT_00415693;
long DAT_00415ccc;
long DAT_00415e54;
long DAT_00415e71;
long DAT_00415eb8;
long DAT_00415fde;
long DAT_00415fe2;
long DAT_00415fe7;
long DAT_00415fe9;
long DAT_00416653;
long DAT_00416a20;
long DAT_00416db0;
long DAT_00416db5;
long DAT_00416e1f;
long DAT_00416eb0;
long DAT_00416eb3;
long DAT_00416f01;
long DAT_00416f05;
long DAT_00416f78;
long DAT_00416f79;
long DAT_00417168;
long DAT_00619000;
long DAT_00619010;
long DAT_00619020;
long DAT_00619428;
long DAT_00619440;
long DAT_006194b8;
long DAT_006194bc;
long DAT_006194c0;
long DAT_00619500;
long DAT_00619508;
long DAT_00619510;
long DAT_00619520;
long DAT_00619528;
long DAT_00619540;
long DAT_00619548;
long DAT_006195c0;
long DAT_006195c1;
long DAT_006195c2;
long DAT_006195c3;
long DAT_006195c4;
long DAT_00619d00;
long DAT_00619d08;
long DAT_00619d10;
long DAT_00619eb8;
long DAT_00619ec0;
long DAT_00619ec8;
long DAT_00619ed8;
long fde_00417d78;
long FLOAT_UNKNOWN;
long null_ARRAY_00414a80;
long null_ARRAY_00414b40;
long null_ARRAY_004173c0;
long null_ARRAY_00619450;
long null_ARRAY_00619480;
long null_ARRAY_00619560;
long null_ARRAY_00619600;
long null_ARRAY_00619a00;
long null_ARRAY_00619b00;
long null_ARRAY_00619c00;
long null_ARRAY_00619d40;
long null_ARRAY_00619d80;
long null_ARRAY_00619e80;
long PTR_DAT_00619420;
long PTR_null_ARRAY_00619460;
undefined8
FUN_00402c42 (long *plParm1, long lParm2, undefined * puParm3, long *plParm4,
	      ulong * puParm5)
{
  char cVar1;
  int iVar2;
  undefined8 uVar3;
  char *local_30;
  undefined8 local_28;
  char *local_20;
  long local_18;
  ulong local_10;

  if ((ulong) plParm1[2] <= lParm2 + 1U)
    {
      func_0x00401a90 ("start_idx + 1 < es->len", "src/tr.c", 0x30e,
		       "find_bracketed_repeat");
    }
  imperfection_wrapper ();	//  cVar1 = FUN_00401fdf(plParm1, lParm2 + 1, 0x2a, lParm2 + 1);
  if (cVar1 == '\x01')
    {
      local_10 = lParm2 + 2;
      while ((local_10 < (ulong) plParm1[2]
	      && (*(char *) (local_10 + plParm1[1]) != '\x01')))
	{
	  if (*(char *) (local_10 + *plParm1) == ']')
	    {
	      local_18 = (local_10 - lParm2) + -2;
	      *puParm3 = *(undefined *) (lParm2 + *plParm1);
	      if (local_18 == 0)
		{
		  *plParm4 = 0;
		}
	      else
		{
		  local_20 = (char *) (*plParm1 + lParm2 + 2);
		  if (*local_20 == '0')
		    {
		      uVar3 = 8;
		    }
		  else
		    {
		      uVar3 = 10;
		    }
		  iVar2 =
		    FUN_00406b51 (local_20, &local_30, uVar3, plParm4, 0);
		  if (((iVar2 != 0) || (*plParm4 == -1))
		      || (local_20 + local_18 != local_30))
		    {
		      imperfection_wrapper ();	//            local_28 = FUN_004026c1(local_20, local_18, local_18);
		      imperfection_wrapper ();	//            uVar3 = FUN_00406274(local_28);
		      imperfection_wrapper ();	//            FUN_00407106(0, 0, "invalid repeat count %s in [c*n] construct", uVar3);
		      func_0x00401c70 (local_28);
		      return 0xfffffffe;
		    }
		}
	      *puParm5 = local_10;
	      return 0;
	    }
	  local_10 = local_10 + 1;
	}
    }
  return 0xffffffff;
}


#include "tr.h"

long null_ARRAY_0061945_0_8_;
long null_ARRAY_0061945_8_8_;
long null_ARRAY_00619d4_0_8_;
long null_ARRAY_00619d4_16_8_;
long null_ARRAY_00619d4_24_8_;
long null_ARRAY_00619d4_32_8_;
long null_ARRAY_00619d4_40_8_;
long null_ARRAY_00619d4_48_8_;
long null_ARRAY_00619d4_8_8_;
long null_ARRAY_00619e8_0_4_;
long null_ARRAY_00619e8_16_8_;
long null_ARRAY_00619e8_4_4_;
long null_ARRAY_00619e8_8_4_;
long DAT_004148c0;
long DAT_0041497d;
long DAT_004149fb;
long DAT_00415568;
long DAT_0041567f;
long DAT_00415681;
long DAT_00415684;
long DAT_00415687;
long DAT_0041568a;
long DAT_0041568d;
long DAT_00415690;
long DAT_00415693;
long DAT_00415ccc;
long DAT_00415e54;
long DAT_00415e71;
long DAT_00415eb8;
long DAT_00415fde;
long DAT_00415fe2;
long DAT_00415fe7;
long DAT_00415fe9;
long DAT_00416653;
long DAT_00416a20;
long DAT_00416db0;
long DAT_00416db5;
long DAT_00416e1f;
long DAT_00416eb0;
long DAT_00416eb3;
long DAT_00416f01;
long DAT_00416f05;
long DAT_00416f78;
long DAT_00416f79;
long DAT_00417168;
long DAT_00619000;
long DAT_00619010;
long DAT_00619020;
long DAT_00619428;
long DAT_00619440;
long DAT_006194b8;
long DAT_006194bc;
long DAT_006194c0;
long DAT_00619500;
long DAT_00619508;
long DAT_00619510;
long DAT_00619520;
long DAT_00619528;
long DAT_00619540;
long DAT_00619548;
long DAT_006195c0;
long DAT_006195c1;
long DAT_006195c2;
long DAT_006195c3;
long DAT_006195c4;
long DAT_00619d00;
long DAT_00619d08;
long DAT_00619d10;
long DAT_00619eb8;
long DAT_00619ec0;
long DAT_00619ec8;
long DAT_00619ed8;
long fde_00417d78;
long FLOAT_UNKNOWN;
long null_ARRAY_00414a80;
long null_ARRAY_00414b40;
long null_ARRAY_004173c0;
long null_ARRAY_00619450;
long null_ARRAY_00619480;
long null_ARRAY_00619560;
long null_ARRAY_00619600;
long null_ARRAY_00619a00;
long null_ARRAY_00619b00;
long null_ARRAY_00619c00;
long null_ARRAY_00619d40;
long null_ARRAY_00619d80;
long null_ARRAY_00619e80;
long PTR_DAT_00619420;
long PTR_null_ARRAY_00619460;
void
FUN_00403e36 (long lParm1, long lParm2)
{
  char cVar1;

  FUN_00403b4d (lParm1);
  if (*(long *) (lParm1 + 0x20) != 0)
    {
      imperfection_wrapper ();	//    FUN_00407106(1, 0, "the [c*] repeat construct may not appear in string1");
    }
  if (lParm2 != 0)
    {
      imperfection_wrapper ();	//    FUN_00403b89(lParm2, *(undefined8 *)(lParm1 + 0x18), *(undefined8 *)(lParm1 + 0x18));
      if (1 < *(ulong *) (lParm2 + 0x20))
	{
	  imperfection_wrapper ();	//      FUN_00407106(1, 0, "only one [c*] repeat construct may appear in string2");
	}
      if (DAT_006195c4 != '\0')
	{
	  if (*(char *) (lParm2 + 0x30) != '\0')
	    {
	      imperfection_wrapper ();	//        FUN_00407106(1, 0, "[=c=] expressions may not appear in string2 when translating");
	    }
	  if (*(char *) (lParm2 + 0x32) != '\0')
	    {
	      imperfection_wrapper ();	//        FUN_00407106(1, 0, "when translating, the only character classes that may appear in\nstring2 are \'upper\' and \'lower\'");
	    }
	  imperfection_wrapper ();	//      FUN_004036fd(lParm1, lParm2, lParm2);
	  if ((*(ulong *) (lParm2 + 0x18) < *(ulong *) (lParm1 + 0x18))
	      && (DAT_006195c3 != '\x01'))
	    {
	      if (*(long *) (lParm2 + 0x18) == 0)
		{
		  imperfection_wrapper ();	//          FUN_00407106(1, 0, "when not truncating set1, string2 must be non-empty");
		}
	      imperfection_wrapper ();	//        FUN_00403c96(lParm1, lParm2, lParm2);
	    }
	  if (DAT_006195c2 == '\0')
	    {
	      return;
	    }
	  if (*(char *) (lParm1 + 0x31) == '\0')
	    {
	      return;
	    }
	  if ((*(long *) (lParm2 + 0x18) == *(long *) (lParm1 + 0x18))
	      && (cVar1 = FUN_00403dcb (lParm2), cVar1 == '\x01'))
	    {
	      return;
	    }
	  imperfection_wrapper ();	//      FUN_00407106(1, 0, "when translating with complemented character classes,\nstring2 must map all characters in the domain to one");
	}
      if (*(long *) (lParm2 + 0x20) != 0)
	{
	  imperfection_wrapper ();	//      FUN_00407106(1, 0, "the [c*] construct may appear in string2 only when translating");
	}
    }
  return;
}

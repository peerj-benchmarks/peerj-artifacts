
#include "tr.h"

long null_ARRAY_0061945_0_8_;
long null_ARRAY_0061945_8_8_;
long null_ARRAY_00619d4_0_8_;
long null_ARRAY_00619d4_16_8_;
long null_ARRAY_00619d4_24_8_;
long null_ARRAY_00619d4_32_8_;
long null_ARRAY_00619d4_40_8_;
long null_ARRAY_00619d4_48_8_;
long null_ARRAY_00619d4_8_8_;
long null_ARRAY_00619e8_0_4_;
long null_ARRAY_00619e8_16_8_;
long null_ARRAY_00619e8_4_4_;
long null_ARRAY_00619e8_8_4_;
long DAT_004148c0;
long DAT_0041497d;
long DAT_004149fb;
long DAT_00415568;
long DAT_0041567f;
long DAT_00415681;
long DAT_00415684;
long DAT_00415687;
long DAT_0041568a;
long DAT_0041568d;
long DAT_00415690;
long DAT_00415693;
long DAT_00415ccc;
long DAT_00415e54;
long DAT_00415e71;
long DAT_00415eb8;
long DAT_00415fde;
long DAT_00415fe2;
long DAT_00415fe7;
long DAT_00415fe9;
long DAT_00416653;
long DAT_00416a20;
long DAT_00416db0;
long DAT_00416db5;
long DAT_00416e1f;
long DAT_00416eb0;
long DAT_00416eb3;
long DAT_00416f01;
long DAT_00416f05;
long DAT_00416f78;
long DAT_00416f79;
long DAT_00417168;
long DAT_00619000;
long DAT_00619010;
long DAT_00619020;
long DAT_00619428;
long DAT_00619440;
long DAT_006194b8;
long DAT_006194bc;
long DAT_006194c0;
long DAT_00619500;
long DAT_00619508;
long DAT_00619510;
long DAT_00619520;
long DAT_00619528;
long DAT_00619540;
long DAT_00619548;
long DAT_006195c0;
long DAT_006195c1;
long DAT_006195c2;
long DAT_006195c3;
long DAT_006195c4;
long DAT_00619d00;
long DAT_00619d08;
long DAT_00619d10;
long DAT_00619eb8;
long DAT_00619ec0;
long DAT_00619ec8;
long DAT_00619ed8;
long fde_00417d78;
long FLOAT_UNKNOWN;
long null_ARRAY_00414a80;
long null_ARRAY_00414b40;
long null_ARRAY_004173c0;
long null_ARRAY_00619450;
long null_ARRAY_00619480;
long null_ARRAY_00619560;
long null_ARRAY_00619600;
long null_ARRAY_00619a00;
long null_ARRAY_00619b00;
long null_ARRAY_00619c00;
long null_ARRAY_00619d40;
long null_ARRAY_00619d80;
long null_ARRAY_00619e80;
long PTR_DAT_00619420;
long PTR_null_ARRAY_00619460;
ulong
FUN_00402030 (int iParm1)
{
  char cVar1;
  char cVar2;

  if (iParm1 == 0)
    {
      func_0x004017c0 ("Usage: %s [OPTION]... SET1 [SET2]\n", DAT_00619d10);
      func_0x004019a0
	("Translate, squeeze, and/or delete characters from standard input,\nwriting to standard output.\n\n  -c, -C, --complement    use the complement of SET1\n  -d, --delete            delete characters in SET1, do not translate\n  -s, --squeeze-repeats   replace each sequence of a repeated character\n                            that is listed in the last specified SET,\n                            with a single occurrence of that character\n  -t, --truncate-set1     first truncate SET1 to length of SET2\n",
	 DAT_00619500);
      func_0x004019a0 ("      --help     display this help and exit\n",
		       DAT_00619500);
      func_0x004019a0
	("      --version  output version information and exit\n",
	 DAT_00619500);
      func_0x004019a0
	("\nSETs are specified as strings of characters.  Most represent themselves.\nInterpreted sequences are:\n\n  \\NNN            character with octal value NNN (1 to 3 octal digits)\n  \\\\              backslash\n  \\a              audible BEL\n  \\b              backspace\n  \\f              form feed\n  \\n              new line\n  \\r              return\n  \\t              horizontal tab\n",
	 DAT_00619500);
      func_0x004019a0
	("  \\v              vertical tab\n  CHAR1-CHAR2     all characters from CHAR1 to CHAR2 in ascending order\n  [CHAR*]         in SET2, copies of CHAR until length of SET1\n  [CHAR*REPEAT]   REPEAT copies of CHAR, REPEAT octal if starting with 0\n  [:alnum:]       all letters and digits\n  [:alpha:]       all letters\n  [:blank:]       all horizontal whitespace\n  [:cntrl:]       all control characters\n  [:digit:]       all digits\n",
	 DAT_00619500);
      func_0x004019a0
	("  [:graph:]       all printable characters, not including space\n  [:lower:]       all lower case letters\n  [:print:]       all printable characters, including space\n  [:punct:]       all punctuation characters\n  [:space:]       all horizontal or vertical whitespace\n  [:upper:]       all upper case letters\n  [:xdigit:]      all hexadecimal digits\n  [=CHAR=]        all characters which are equivalent to CHAR\n",
	 DAT_00619500);
      cVar1 = (char) DAT_00619500;
      func_0x004019a0
	("\nTranslation occurs if -d is not given and both SET1 and SET2 appear.\n-t may be used only when translating.  SET2 is extended to length of\nSET1 by repeating its last character as necessary.  Excess characters\nof SET2 are ignored.  Only [:lower:] and [:upper:] are guaranteed to\nexpand in ascending order; used in SET2 while translating, they may\nonly be used in pairs to specify case conversion.  -s uses the last\nspecified SET, and occurs after translation or deletion.\n");
      imperfection_wrapper ();	//    FUN_00401e43();
    }
  else
    {
      cVar1 = ' ';
      func_0x00401990 (DAT_00619520,
		       "Try \'%s --help\' for more information.\n",
		       DAT_00619d10);
    }
  cVar2 = (char) iParm1;
  func_0x00401b60 ();
  return (ulong) (cVar2 == cVar1);
}

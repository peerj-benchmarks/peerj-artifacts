
#include "uniq.h"

long null_ARRAY_0061757_0_8_;
long null_ARRAY_0061757_8_8_;
long null_ARRAY_0061770_0_8_;
long null_ARRAY_0061770_16_8_;
long null_ARRAY_0061770_24_8_;
long null_ARRAY_0061770_32_8_;
long null_ARRAY_0061770_40_8_;
long null_ARRAY_0061770_48_8_;
long null_ARRAY_0061770_8_8_;
long null_ARRAY_0061784_0_4_;
long null_ARRAY_0061784_16_8_;
long null_ARRAY_0061784_4_4_;
long null_ARRAY_0061784_8_4_;
long DAT_0041374b;
long DAT_00413805;
long DAT_00413883;
long DAT_004141ec;
long DAT_004141fe;
long DAT_00414200;
long DAT_00414202;
long DAT_00414205;
long DAT_004143f9;
long DAT_00414411;
long DAT_0041441e;
long DAT_00414478;
long DAT_0041459e;
long DAT_004145a2;
long DAT_004145a7;
long DAT_004145a9;
long DAT_00414c13;
long DAT_00414fe0;
long DAT_004153e6;
long DAT_004153eb;
long DAT_00415457;
long DAT_004154e8;
long DAT_004154eb;
long DAT_00415539;
long DAT_004155a8;
long DAT_004155a9;
long DAT_00415788;
long DAT_00617138;
long DAT_00617148;
long DAT_00617158;
long DAT_00617550;
long DAT_00617560;
long DAT_006175d8;
long DAT_006175dc;
long DAT_006175e0;
long DAT_00617600;
long DAT_00617608;
long DAT_00617610;
long DAT_00617620;
long DAT_00617628;
long DAT_00617640;
long DAT_00617648;
long DAT_00617690;
long DAT_00617698;
long DAT_006176a0;
long DAT_006176a8;
long DAT_006176b0;
long DAT_006176b4;
long DAT_006176b5;
long DAT_006176b6;
long DAT_006176b7;
long DAT_006176b8;
long DAT_006176bc;
long DAT_006176c0;
long DAT_006176c8;
long DAT_006176d0;
long DAT_00617878;
long DAT_00617880;
long DAT_00617888;
long DAT_00617898;
long fde_00416320;
long FLOAT_UNKNOWN;
long null_ARRAY_004138e0;
long null_ARRAY_00413900;
long null_ARRAY_00413920;
long null_ARRAY_00413950;
long null_ARRAY_00413a00;
long null_ARRAY_004159e0;
long null_ARRAY_00617570;
long null_ARRAY_006175a0;
long null_ARRAY_00617660;
long null_ARRAY_00617700;
long null_ARRAY_00617740;
long null_ARRAY_00617840;
long PTR_DAT_00617540;
long PTR_FUN_00617548;
long PTR_null_ARRAY_00617580;
ulong
FUN_00401f67 (uint uParm1)
{
  int iVar1;
  uint uVar2;

  if (uParm1 == 0)
    {
      func_0x00401740 ("Usage: %s [OPTION]... [INPUT [OUTPUT]]\n",
		       DAT_006176d0);
      func_0x00401900
	("Filter adjacent matching lines from INPUT (or standard input),\nwriting to OUTPUT (or standard output).\n\nWith no options, matching lines are merged to the first occurrence.\n",
	 DAT_00617600);
      FUN_00401db1 ();
      func_0x00401900
	("  -c, --count           prefix lines by the number of occurrences\n  -d, --repeated        only print duplicate lines, one for each group\n",
	 DAT_00617600);
      func_0x00401900
	("  -D                    print all duplicate lines\n      --all-repeated[=METHOD]  like -D, but allow separating groups\n                                 with an empty line;\n                                 METHOD={none(default),prepend,separate}\n",
	 DAT_00617600);
      func_0x00401900
	("  -f, --skip-fields=N   avoid comparing the first N fields\n",
	 DAT_00617600);
      func_0x00401900
	("      --group[=METHOD]  show all items, separating groups with an empty line;\n                          METHOD={separate(default),prepend,append,both}\n",
	 DAT_00617600);
      func_0x00401900
	("  -i, --ignore-case     ignore differences in case when comparing\n  -s, --skip-chars=N    avoid comparing the first N characters\n  -u, --unique          only print unique lines\n",
	 DAT_00617600);
      func_0x00401900
	("  -z, --zero-terminated     line delimiter is NUL, not newline\n",
	 DAT_00617600);
      func_0x00401900
	("  -w, --check-chars=N   compare no more than N characters in lines\n",
	 DAT_00617600);
      func_0x00401900 ("      --help     display this help and exit\n",
		       DAT_00617600);
      func_0x00401900
	("      --version  output version information and exit\n",
	 DAT_00617600);
      func_0x00401900
	("\nA field is a run of blanks (usually spaces and/or TABs), then non-blank\ncharacters.  Fields are skipped before chars.\n",
	 DAT_00617600);
      func_0x00401900
	("\nNote: \'uniq\' does not detect repeated lines unless they are adjacent.\nYou may want to sort the input first, or use \'sort -u\' without \'uniq\'.\nAlso, comparisons honor the rules specified by \'LC_COLLATE\'.\n",
	 DAT_00617600);
      FUN_00401dcb (&DAT_004141ec);
    }
  else
    {
      func_0x004018f0 (DAT_00617620,
		       "Try \'%s --help\' for more information.\n",
		       DAT_006176d0);
    }
  func_0x00401ae0 ((ulong) uParm1);
  iVar1 = FUN_004038fb ();
  if ((iVar1 < 0x30db0) || (0x31068 < iVar1))
    {
      uVar2 = 0;
    }
  else
    {
      uVar2 = 1;
    }
  return (ulong) uVar2;
}


#include "groups.h"

long null_ARRAY_0061543_0_8_;
long null_ARRAY_0061543_8_8_;
long null_ARRAY_0061558_0_8_;
long null_ARRAY_0061558_16_8_;
long null_ARRAY_0061558_24_8_;
long null_ARRAY_0061558_32_8_;
long null_ARRAY_0061558_40_8_;
long null_ARRAY_0061558_48_8_;
long null_ARRAY_0061558_8_8_;
long null_ARRAY_006156c_0_4_;
long null_ARRAY_006156c_16_8_;
long null_ARRAY_006156c_4_4_;
long null_ARRAY_006156c_8_4_;
long DAT_00411f80;
long DAT_0041203d;
long DAT_004120bb;
long DAT_004123ca;
long DAT_00412410;
long DAT_0041255e;
long DAT_00412562;
long DAT_00412567;
long DAT_00412569;
long DAT_00412bd3;
long DAT_00412fa0;
long DAT_00412fb8;
long DAT_00412fbd;
long DAT_00413027;
long DAT_004130b8;
long DAT_004130bb;
long DAT_00413109;
long DAT_0041310d;
long DAT_00413180;
long DAT_00413181;
long DAT_00615000;
long DAT_00615010;
long DAT_00615020;
long DAT_00615408;
long DAT_00615420;
long DAT_00615498;
long DAT_0061549c;
long DAT_006154a0;
long DAT_006154c0;
long DAT_006154d0;
long DAT_006154e0;
long DAT_006154e8;
long DAT_00615500;
long DAT_00615508;
long DAT_00615568;
long DAT_00615570;
long DAT_00615578;
long DAT_006156f8;
long DAT_00615700;
long DAT_00615708;
long DAT_00615718;
long fde_00413e28;
long FLOAT_UNKNOWN;
long null_ARRAY_00412100;
long null_ARRAY_004135c0;
long null_ARRAY_00615430;
long null_ARRAY_00615460;
long null_ARRAY_00615520;
long null_ARRAY_00615550;
long null_ARRAY_00615580;
long null_ARRAY_006155c0;
long null_ARRAY_006156c0;
long PTR_DAT_00615400;
long PTR_null_ARRAY_00615440;
ulong
FUN_00401e8a (uint uParm1)
{
  char cVar1;
  int iVar2;
  uint uVar3;
  uint uVar4;
  uint uVar5;
  undefined4 *puVar6;
  int *piVar7;
  uint *puVar8;
  long lVar9;
  char *pcVar10;
  bool bVar11;
  undefined8 uVar12;

  if (uParm1 == 0)
    {
      func_0x004016e0 ("Usage: %s [OPTION]... [USERNAME]...\n", DAT_00615578);
      func_0x00401890
	("Print group memberships for each USERNAME or, if no USERNAME is specified, for\nthe current process (which may differ if the groups database has changed).\n",
	 DAT_006154c0);
      func_0x00401890 ("      --help     display this help and exit\n",
		       DAT_006154c0);
      pcVar10 = (char *) DAT_006154c0;
      func_0x00401890
	("      --version  output version information and exit\n");
      imperfection_wrapper ();	//    FUN_00401cee();
    }
  else
    {
      pcVar10 = "Try \'%s --help\' for more information.\n";
      func_0x00401880 (DAT_006154e0,
		       "Try \'%s --help\' for more information.\n",
		       DAT_00615578);
    }
  func_0x00401a60 ();
  bVar11 = true;
  FUN_0040262e (*(undefined8 *) pcVar10);
  func_0x00401a10 (6, &DAT_004120bb);
  func_0x004019f0 (FUN_004024c4);
  uVar12 = 0x401f7f;
  imperfection_wrapper ();	//  iVar2 = FUN_004054fc((ulong)uParm1, pcVar10, &DAT_004120bb, null_ARRAY_00412100, 0);
  if (iVar2 == -1)
    goto LAB_00401ff5;
  if (iVar2 == -0x83)
    {
    LAB_00401fa3:
      ;
      imperfection_wrapper ();	//    FUN_00403ff7(DAT_006154c0, "groups", "GNU coreutils", PTR_DAT_00615400, "David MacKenzie", "James Youngman", 0, uVar12);
      func_0x00401a60 (0);
    }
  else
    {
      if (iVar2 == -0x82)
	{
	  uVar12 = 0x401fa3;
	  FUN_00401e8a (0);
	  goto LAB_00401fa3;
	}
    }
  FUN_00401e8a (1);
LAB_00401ff5:
  ;
  if (DAT_00615498 == uParm1)
    {
      puVar6 = (undefined4 *) func_0x00401a50 ();
      *puVar6 = 0;
      uVar3 = func_0x004017d0 ();
      if ((uVar3 == 0xffffffff)
	  && (piVar7 = (int *) func_0x00401a50 (), *piVar7 != 0))
	{
	  puVar8 = (uint *) func_0x00401a50 ();
	  imperfection_wrapper ();	//      FUN_00404323(1, (ulong)*puVar8, "cannot get real UID");
	}
      puVar6 = (undefined4 *) func_0x00401a50 ();
      *puVar6 = 0;
      uVar4 = func_0x00401770 ();
      if ((uVar4 == 0xffffffff)
	  && (piVar7 = (int *) func_0x00401a50 (), *piVar7 != 0))
	{
	  puVar8 = (uint *) func_0x00401a50 ();
	  imperfection_wrapper ();	//      FUN_00404323(1, (ulong)*puVar8, "cannot get effective GID");
	}
      puVar6 = (undefined4 *) func_0x00401a50 ();
      *puVar6 = 0;
      uVar5 = func_0x004016d0 ();
      if ((uVar5 == 0xffffffff)
	  && (piVar7 = (int *) func_0x00401a50 (), *piVar7 != 0))
	{
	  puVar8 = (uint *) func_0x00401a50 ();
	  imperfection_wrapper ();	//      FUN_00404323(1, (ulong)*puVar8, "cannot get real GID");
	}
      cVar1 =
	FUN_00402253 (0, (ulong) uVar3, (ulong) uVar5, (ulong) uVar4, 1,
		      0x20);
      bVar11 = cVar1 == '\x01';
      func_0x00401a20 (10);
    }
  else
    {
      while ((int) DAT_00615498 < (int) uParm1)
	{
	  lVar9 =
	    func_0x004019c0 (((undefined8 *) pcVar10)[(long) (int)
						      DAT_00615498]);
	  if (lVar9 == 0)
	    {
	      FUN_00403aa2 (((undefined8 *) pcVar10)[(long) (int)
						     DAT_00615498]);
	      imperfection_wrapper ();	//        FUN_00404323(0, 0, "%s: no such user");
	      bVar11 = false;
	    }
	  else
	    {
	      uVar3 = *(uint *) (lVar9 + 0x10);
	      uVar4 = *(uint *) (lVar9 + 0x14);
	      func_0x004016e0 ("%s : ",
			       ((undefined8 *) pcVar10)[(long) (int)
							DAT_00615498]);
	      cVar1 =
		FUN_00402253 (((undefined8 *) pcVar10)[(long) (int)
						       DAT_00615498],
			      (ulong) uVar3, (ulong) uVar4, (ulong) uVar4, 1,
			      0x20);
	      if (cVar1 != '\x01')
		{
		  bVar11 = false;
		}
	      func_0x00401a20 (10);
	    }
	  DAT_00615498 = DAT_00615498 + 1;
	}
    }
  return (ulong) ! bVar11;
}


#include "hostid.h"

long null_ARRAY_006143f_0_8_;
long null_ARRAY_006143f_8_8_;
long null_ARRAY_0061454_0_8_;
long null_ARRAY_0061454_16_8_;
long null_ARRAY_0061454_24_8_;
long null_ARRAY_0061454_32_8_;
long null_ARRAY_0061454_40_8_;
long null_ARRAY_0061454_48_8_;
long null_ARRAY_0061454_8_8_;
long null_ARRAY_0061468_0_4_;
long null_ARRAY_0061468_16_8_;
long null_ARRAY_0061468_4_4_;
long null_ARRAY_0061468_8_4_;
long DAT_004112c0;
long DAT_0041137d;
long DAT_004113fb;
long DAT_00411591;
long DAT_00411660;
long DAT_004116a8;
long DAT_004117de;
long DAT_004117e2;
long DAT_004117e7;
long DAT_004117e9;
long DAT_00411e53;
long DAT_00412220;
long DAT_00412238;
long DAT_0041223d;
long DAT_004122a7;
long DAT_00412338;
long DAT_0041233b;
long DAT_00412389;
long DAT_0041238d;
long DAT_00412400;
long DAT_00412401;
long DAT_00614000;
long DAT_00614010;
long DAT_00614020;
long DAT_006143c8;
long DAT_006143e0;
long DAT_00614458;
long DAT_0061445c;
long DAT_00614460;
long DAT_00614480;
long DAT_00614490;
long DAT_006144a0;
long DAT_006144a8;
long DAT_006144c0;
long DAT_006144c8;
long DAT_00614510;
long DAT_00614518;
long DAT_00614520;
long DAT_006146b8;
long DAT_006146c0;
long DAT_006146c8;
long DAT_006146d8;
long fde_00413060;
long FLOAT_UNKNOWN;
long null_ARRAY_00411440;
long null_ARRAY_00411600;
long null_ARRAY_00412840;
long null_ARRAY_006143f0;
long null_ARRAY_00614420;
long null_ARRAY_006144e0;
long null_ARRAY_00614540;
long null_ARRAY_00614580;
long null_ARRAY_00614680;
long PTR_DAT_006143c0;
long PTR_null_ARRAY_00614400;
undefined8
FUN_00401b7a (uint uParm1)
{
  int iVar1;
  uint uVar2;
  undefined8 uVar3;
  char *pcVar4;

  if (uParm1 == 0)
    {
      func_0x00401450
	("Usage: %s [OPTION]\nPrint the numeric identifier (in hexadecimal) for the current host.\n\n",
	 DAT_00614520);
      func_0x004015d0 ("      --help     display this help and exit\n",
		       DAT_00614480);
      pcVar4 = (char *) DAT_00614480;
      func_0x004015d0
	("      --version  output version information and exit\n");
      imperfection_wrapper ();	//    FUN_004019de();
    }
  else
    {
      pcVar4 = "Try \'%s --help\' for more information.\n";
      func_0x004015c0 (DAT_006144a0,
		       "Try \'%s --help\' for more information.\n",
		       DAT_00614520);
    }
  func_0x00401740 ();
  FUN_00401f4a (*(undefined8 *) pcVar4);
  func_0x00401700 (6, &DAT_004113fb);
  func_0x004016e0 (FUN_00401d14);
  imperfection_wrapper ();	//  FUN_00401df9((ulong)uParm1, pcVar4, "hostid", "GNU coreutils", PTR_DAT_006143c0, FUN_00401b7a, "Jim Meyering", 0);
  imperfection_wrapper ();	//  iVar1 = FUN_00404d1b((ulong)uParm1, pcVar4, &DAT_004113fb, null_ARRAY_00411440, 0);
  if (iVar1 != -1)
    {
      FUN_00401b7a (1);
    }
  if (DAT_00614458 < (int) uParm1)
    {
      imperfection_wrapper ();	//    uVar3 = FUN_004033be(((undefined8 *)pcVar4)[(long)DAT_00614458]);
      imperfection_wrapper ();	//    FUN_00403b42(0, 0, "extra operand %s", uVar3);
      FUN_00401b7a (1);
    }
  uVar2 = func_0x00401800 ();
  func_0x00401450 ("%08x\n", (ulong) uVar2);
  return 0;
}

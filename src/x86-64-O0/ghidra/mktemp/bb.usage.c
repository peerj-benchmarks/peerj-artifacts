
#include "mktemp.h"

long null_ARRAY_0061847_0_8_;
long null_ARRAY_0061847_8_8_;
long null_ARRAY_006185c_0_8_;
long null_ARRAY_006185c_16_8_;
long null_ARRAY_006185c_24_8_;
long null_ARRAY_006185c_32_8_;
long null_ARRAY_006185c_40_8_;
long null_ARRAY_006185c_48_8_;
long null_ARRAY_006185c_8_8_;
long null_ARRAY_0061870_0_4_;
long null_ARRAY_0061870_16_8_;
long null_ARRAY_0061870_4_4_;
long null_ARRAY_0061870_8_4_;
long DAT_00414440;
long DAT_004144fd;
long DAT_0041457b;
long DAT_00414872;
long DAT_00414e5a;
long DAT_00414ea0;
long DAT_00414fde;
long DAT_00414fe2;
long DAT_00414fe7;
long DAT_00414fe9;
long DAT_00415640;
long DAT_0041570b;
long DAT_00415ac0;
long DAT_00415ad8;
long DAT_00415add;
long DAT_00415b47;
long DAT_00415bd8;
long DAT_00415bdb;
long DAT_00415c29;
long DAT_00415c2d;
long DAT_00415ca0;
long DAT_00415ca1;
long DAT_00415cdf;
long DAT_00618000;
long DAT_00618010;
long DAT_00618020;
long DAT_00618450;
long DAT_00618460;
long DAT_006184d8;
long DAT_006184dc;
long DAT_006184e0;
long DAT_00618500;
long DAT_00618510;
long DAT_00618520;
long DAT_00618528;
long DAT_00618540;
long DAT_00618548;
long DAT_00618590;
long DAT_00618598;
long DAT_006185a0;
long DAT_006185a8;
long DAT_00618738;
long DAT_00618778;
long DAT_00618780;
long DAT_00618788;
long DAT_00618798;
long fde_00416ad0;
long FLOAT_UNKNOWN;
long null_ARRAY_00414600;
long null_ARRAY_00416120;
long null_ARRAY_00618470;
long null_ARRAY_006184a0;
long null_ARRAY_00618560;
long null_ARRAY_006185c0;
long null_ARRAY_00618600;
long null_ARRAY_00618700;
long PTR_DAT_00618448;
long PTR_null_ARRAY_00618480;
long PTR_null_ARRAY_006184e8;
long PTR_s_tmp_XXXXXXXXXX_00618440;
long
FUN_0040207a (uint uParm1)
{
  ulong uVar1;
  char *pcStack64;
  long lStack40;

  if (uParm1 == 0)
    {
      func_0x00401850 ("Usage: %s [OPTION]... [TEMPLATE]\n", DAT_006185a8);
      func_0x00401a30
	("Create a temporary file or directory, safely, and print its name.\nTEMPLATE must contain at least 3 consecutive \'X\'s in last component.\nIf TEMPLATE is not specified, use tmp.XXXXXXXXXX, and --tmpdir is implied.\n",
	 DAT_00618500);
      func_0x00401a30
	("Files are created u+rw, and directories u+rwx, minus umask restrictions.\n",
	 DAT_00618500);
      func_0x00401a30 (&DAT_00414872, DAT_00618500);
      func_0x00401a30
	("  -d, --directory     create a directory, not a file\n  -u, --dry-run       do not create anything; merely print a name (unsafe)\n  -q, --quiet         suppress diagnostics about file/dir-creation failure\n",
	 DAT_00618500);
      func_0x00401a30
	("      --suffix=SUFF   append SUFF to TEMPLATE; SUFF must not contain a slash.\n                        This option is implied if TEMPLATE does not end in X\n",
	 DAT_00618500);
      func_0x00401a30
	("  -p DIR, --tmpdir[=DIR]  interpret TEMPLATE relative to DIR; if DIR is not\n                        specified, use $TMPDIR if set, else /tmp.  With\n                        this option, TEMPLATE must not be an absolute name;\n                        unlike with -t, TEMPLATE may contain slashes, but\n                        mktemp creates only the final component\n",
	 DAT_00618500);
      func_0x00401a30
	("  -t                  interpret TEMPLATE as a single file name component,\n                        relative to a directory: $TMPDIR, if set; else the\n                        directory specified via -p; else /tmp [deprecated]\n",
	 DAT_00618500);
      func_0x00401a30 ("      --help     display this help and exit\n",
		       DAT_00618500);
      pcStack64 = DAT_00618500;
      func_0x00401a30
	("      --version  output version information and exit\n");
      imperfection_wrapper ();	//    FUN_00401ede();
    }
  else
    {
      pcStack64 = "Try \'%s --help\' for more information.\n";
      func_0x00401a20 (DAT_00618520,
		       "Try \'%s --help\' for more information.\n",
		       DAT_006185a8);
    }
  uVar1 = (ulong) uParm1;
  func_0x00401c30 ();
  lStack40 = 0;
  while ((pcStack64 != (char *) 0x0 && (pcStack64[uVar1 - 1] == 'X')))
    {
      lStack40 = lStack40 + 1;
      pcStack64 = pcStack64 + -1;
    }
  return lStack40;
}


#include "nohup.h"

long null_ARRAY_0061541_0_8_;
long null_ARRAY_0061541_8_8_;
long null_ARRAY_0061558_0_8_;
long null_ARRAY_0061558_16_8_;
long null_ARRAY_0061558_24_8_;
long null_ARRAY_0061558_32_8_;
long null_ARRAY_0061558_40_8_;
long null_ARRAY_0061558_48_8_;
long null_ARRAY_0061558_8_8_;
long null_ARRAY_006156e_0_4_;
long null_ARRAY_006156e_16_8_;
long null_ARRAY_006156e_4_4_;
long null_ARRAY_006156e_8_4_;
long DAT_00411e80;
long DAT_00411f3d;
long DAT_00411fbb;
long DAT_00412314;
long DAT_00412372;
long DAT_0041247b;
long DAT_00412520;
long DAT_00412568;
long DAT_0041269e;
long DAT_004126a2;
long DAT_004126a7;
long DAT_004126a9;
long DAT_00412d13;
long DAT_004130e0;
long DAT_004130f8;
long DAT_004130fd;
long DAT_00413167;
long DAT_004131f8;
long DAT_004131fb;
long DAT_00413249;
long DAT_0041324d;
long DAT_004132c0;
long DAT_004132c1;
long DAT_00615000;
long DAT_00615010;
long DAT_00615020;
long DAT_006153e8;
long DAT_00615400;
long DAT_00615478;
long DAT_0061547c;
long DAT_00615480;
long DAT_006154c0;
long DAT_006154d0;
long DAT_006154e0;
long DAT_006154e8;
long DAT_00615500;
long DAT_00615508;
long DAT_00615550;
long DAT_00615558;
long DAT_00615560;
long DAT_006156c0;
long DAT_00615718;
long DAT_00615720;
long DAT_00615728;
long DAT_00615738;
long fde_00413f60;
long FLOAT_UNKNOWN;
long null_ARRAY_00412000;
long null_ARRAY_004124c0;
long null_ARRAY_00413700;
long null_ARRAY_00615410;
long null_ARRAY_00615520;
long null_ARRAY_00615580;
long null_ARRAY_006155c0;
long null_ARRAY_006156e0;
long PTR_DAT_006153e0;
long PTR_null_ARRAY_00615420;
ulong
FUN_00401d12 (uint uParm1)
{
  uint uVar1;
  bool bVar2;
  uint uVar3;
  int iVar4;
  uint uVar5;
  long lVar6;
  int *piVar7;
  uint *puVar8;
  undefined8 uVar9;
  char *pcVar10;
  undefined8 *puVar11;
  char *pcVar12;
  bool bVar13;
  bool bVar14;
  bool bVar15;
  char *pcStack56;
  char *pcStack48;
  uint uStack40;
  uint uStack36;

  if (uParm1 == 0)
    {
      func_0x00401580 ("Usage: %s COMMAND [ARG]...\n  or:  %s OPTION\n",
		       DAT_00615560, DAT_00615560);
      func_0x00401730 ("Run COMMAND, ignoring hangup signals.\n\n",
		       DAT_006154c0);
      func_0x00401730 ("      --help     display this help and exit\n",
		       DAT_006154c0);
      func_0x00401730
	("      --version  output version information and exit\n",
	 DAT_006154c0);
      func_0x00401580
	("\nIf standard input is a terminal, redirect it from an unreadable file.\nIf standard output is a terminal, append output to \'nohup.out\' if possible,\n\'$HOME/nohup.out\' otherwise.\nIf standard error is a terminal, redirect it to standard output.\nTo save output to FILE, use \'%s COMMAND > FILE\'.\n",
	 DAT_00615560);
      pcVar12 = "nohup";
      func_0x00401580
	("\nNOTE: your shell may have its own version of %s, which usually supersedes\nthe version described here.  Please refer to your shell\'s documentation\nfor details about the options it supports.\n");
      imperfection_wrapper ();	//    FUN_00401b76();
    }
  else
    {
      pcVar12 = "Try \'%s --help\' for more information.\n";
      func_0x00401720 (DAT_006154e0,
		       "Try \'%s --help\' for more information.\n",
		       DAT_00615560);
    }
  func_0x004018c0 ();
  uStack36 = 1;
  uStack40 = 2;
  FUN_00402759 (*(undefined8 *) pcVar12);
  func_0x00401880 (6, &DAT_00411fbb);
  lVar6 = func_0x004015e0 ("POSIXLY_CORRECT");
  if (lVar6 == 0)
    {
      uVar3 = 0x7d;
    }
  else
    {
      uVar3 = 0x7f;
    }
  FUN_00401b5e ((ulong) uVar3);
  func_0x00401860 (FUN_004022f9);
  imperfection_wrapper ();	//  FUN_00402608((ulong)uParm1, pcVar12, "nohup", "GNU coreutils", PTR_DAT_006153e0, FUN_00401d12, "Jim Meyering", 0);
  imperfection_wrapper ();	//  iVar4 = FUN_00405849((ulong)uParm1, pcVar12, &DAT_00412314, null_ARRAY_00412000, 0);
  if (iVar4 != -1)
    {
      FUN_00401d12 ((ulong) uVar3);
    }
  if ((int) uParm1 <= DAT_00615478)
    {
      imperfection_wrapper ();	//    FUN_004043b9(0, 0, "missing operand");
      FUN_00401d12 ((ulong) uVar3);
    }
  iVar4 = func_0x00401670 (0);
  bVar13 = iVar4 != 0;
  iVar4 = func_0x00401670 (1);
  bVar14 = iVar4 != 0;
  if ((bVar14) || (piVar7 = (int *) func_0x004018b0 (), *piVar7 != 9))
    {
      bVar2 = false;
    }
  else
    {
      bVar2 = true;
    }
  iVar4 = func_0x00401670 (2);
  bVar15 = iVar4 != 0;
  if (bVar13)
    {
      iVar4 = FUN_004023de (0, "/dev/null", 1, 0);
      if (iVar4 < 0)
	{
	  puVar8 = (uint *) func_0x004018b0 ();
	  imperfection_wrapper ();	//      FUN_004043b9((ulong)uVar3, (ulong)*puVar8, "failed to render standard input unusable", (ulong)*puVar8);
	}
      if ((!bVar14) && (!bVar15))
	{
	  imperfection_wrapper ();	//      FUN_004043b9(0, 0, "ignoring input");
	}
    }
  if ((bVar14) || ((bVar15 && (bVar2))))
    {
      pcStack48 = (char *) 0x0;
      pcStack56 = "nohup.out";
      uVar5 = func_0x00401760 (0xfffffe7f);
      if (bVar14)
	{
	  uStack36 = FUN_004023de (1, "nohup.out", 0x441, 0x180);
	}
      else
	{
	  uStack36 = func_0x00401930 ("nohup.out", 0x441, 0x180, 0x441);
	}
      if ((int) uStack36 < 0)
	{
	  puVar8 = (uint *) func_0x004018b0 ();
	  uVar1 = *puVar8;
	  lVar6 = func_0x004015e0 (&DAT_00412372);
	  if (lVar6 != 0)
	    {
	      imperfection_wrapper ();	//        pcStack48 = (char *)FUN_0040245a(lVar6, "nohup.out", 0, "nohup.out");
	      if (bVar14)
		{
		  uStack36 = FUN_004023de (1, pcStack48, 0x441, 0x180);
		}
	      else
		{
		  uStack36 = func_0x00401930 (pcStack48, 0x441, 0x180, 0x441);
		}
	    }
	  if ((int) uStack36 < 0)
	    {
	      puVar8 = (uint *) func_0x004018b0 ();
	      uVar5 = *puVar8;
	      imperfection_wrapper ();	//        uVar9 = FUN_00403b53(4, "nohup.out");
	      imperfection_wrapper ();	//        FUN_004043b9(0, (ulong)uVar1, "failed to open %s", uVar9);
	      if (pcStack48 != (char *) 0x0)
		{
		  imperfection_wrapper ();	//          uVar9 = FUN_00403b53(4, pcStack48);
		  imperfection_wrapper ();	//          FUN_004043b9(0, (ulong)uVar5, "failed to open %s", uVar9);
		}
	      return (ulong) uVar3;
	    }
	  pcStack56 = pcStack48;
	}
      func_0x00401760 ((ulong) uVar5);
      imperfection_wrapper ();	//    uVar9 = FUN_00403b53(4, pcStack56);
      if (bVar13)
	{
	  pcVar10 = "ignoring input and appending output to %s";
	}
      else
	{
	  pcVar10 = "appending output to %s";
	}
      imperfection_wrapper ();	//    FUN_004043b9(0, 0, pcVar10, uVar9);
      func_0x004019c0 (pcStack48);
    }
  if (bVar15)
    {
      imperfection_wrapper ();	//    uStack40 = FUN_0040449d(2, 0x406, 3);
      if (!bVar14)
	{
	  if (bVar13)
	    {
	      pcVar10 = "ignoring input and redirecting stderr to stdout";
	    }
	  else
	    {
	      pcVar10 = "redirecting stderr to stdout";
	    }
	  imperfection_wrapper ();	//      FUN_004043b9(0, 0, pcVar10);
	}
      iVar4 = func_0x00401630 ((ulong) uStack36, 2);
      if (iVar4 < 0)
	{
	  puVar8 = (uint *) func_0x004018b0 ();
	  imperfection_wrapper ();	//      FUN_004043b9((ulong)uVar3, (ulong)*puVar8, "failed to redirect standard error", (ulong)*puVar8);
	}
      if (bVar2)
	{
	  func_0x004019b0 ((ulong) uStack36);
	}
    }
  iVar4 = func_0x004018e0 (DAT_006154e0);
  if (iVar4 == 0)
    {
      func_0x00401790 (1, 1);
      puVar11 = (undefined8 *) pcVar12 + (long) DAT_00615478;
      func_0x00401960 (*puVar11, puVar11, puVar11);
      piVar7 = (int *) func_0x004018b0 ();
      if (*piVar7 == 2)
	{
	  uVar3 = 0x7f;
	}
      else
	{
	  uVar3 = 0x7e;
	}
      puVar8 = (uint *) func_0x004018b0 ();
      uVar5 = *puVar8;
      iVar4 = func_0x00401630 ((ulong) uStack40, 2);
      if (iVar4 == 2)
	{
	  imperfection_wrapper ();	//      uVar9 = FUN_00403b53(4, *puVar11);
	  imperfection_wrapper ();	//      FUN_004043b9(0, (ulong)uVar5, "failed to run command %s", uVar9);
	}
    }
  return (ulong) uVar3;
}

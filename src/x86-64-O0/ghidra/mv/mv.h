typedef unsigned char   undefined;

typedef unsigned char    byte;
typedef unsigned char    dwfenc;
typedef unsigned int    dword;
typedef unsigned long    qword;
typedef unsigned char    undefined1;
typedef unsigned short    undefined2;
typedef unsigned int    undefined3;
typedef unsigned int    undefined4;
typedef unsigned long    undefined5;
typedef unsigned long    undefined6;
typedef unsigned long    undefined8;
typedef unsigned short    word;
#define unkbyte9   unsigned long long
#define unkbyte10   unsigned long long
#define unkbyte11   unsigned long long
#define unkbyte12   unsigned long long
#define unkbyte13   unsigned long long
#define unkbyte14   unsigned long long
#define unkbyte15   unsigned long long
#define unkbyte16   unsigned long long

#define unkuint9   unsigned long long
#define unkuint10   unsigned long long
#define unkuint11   unsigned long long
#define unkuint12   unsigned long long
#define unkuint13   unsigned long long
#define unkuint14   unsigned long long
#define unkuint15   unsigned long long
#define unkuint16   unsigned long long

#define unkint9   long long
#define unkint10   long long
#define unkint11   long long
#define unkint12   long long
#define unkint13   long long
#define unkint14   long long
#define unkint15   long long
#define unkint16   long long

#define unkfloat1   float
#define unkfloat2   float
#define unkfloat3   float
#define unkfloat5   double
#define unkfloat6   double
#define unkfloat7   double
#define unkfloat9   long double
#define unkfloat11   long double
#define unkfloat12   long double
#define unkfloat13   long double
#define unkfloat14   long double
#define unkfloat15   long double
#define unkfloat16   long double

#define BADSPACEBASE   void
#define code   void

#define ulong long
#define true 1
#define bool int
#define false 0
#define float10 int
#define ushort int
#define undefined7 long
#define undefined2 long
#define uint5 long
#define uint6 long

// Add definition for uint
typedef unsigned int    uint;

#define sbyte char

extern imperfection_wrapper(void);
typedef struct eh_frame_hdr eh_frame_hdr, *Peh_frame_hdr;

struct eh_frame_hdr {
    byte eh_frame_hdr_version; // Exception Handler Frame Header Version
    dwfenc eh_frame_pointer_encoding; // Exception Handler Frame Pointer Encoding
    dwfenc eh_frame_desc_entry_count_encoding; // Encoding of # of Exception Handler FDEs
    dwfenc eh_frame_table_encoding; // Exception Handler Table Encoding
};

typedef struct fde_table_entry fde_table_entry, *Pfde_table_entry;

struct fde_table_entry {
    dword initial_loc; // Initial Location
    dword data_loc; // Data location
};

typedef enum Elf64_DynTag {
    DT_AUDIT=1879047932,
    DT_AUXILIARY=2147483645,
    DT_BIND_NOW=24,
    DT_CHECKSUM=1879047672,
    DT_CONFIG=1879047930,
    DT_DEBUG=21,
    DT_DEPAUDIT=1879047931,
    DT_ENCODING=32,
    DT_FEATURE_1=1879047676,
    DT_FILTER=2147483647,
    DT_FINI=13,
    DT_FINI_ARRAY=26,
    DT_FINI_ARRAYSZ=28,
    DT_FLAGS=30,
    DT_FLAGS_1=1879048187,
    DT_GNU_CONFLICT=1879047928,
    DT_GNU_CONFLICTSZ=1879047670,
    DT_GNU_HASH=1879047925,
    DT_GNU_LIBLIST=1879047929,
    DT_GNU_LIBLISTSZ=1879047671,
    DT_GNU_PRELINKED=1879047669,
    DT_HASH=4,
    DT_INIT=12,
    DT_INIT_ARRAY=25,
    DT_INIT_ARRAYSZ=27,
    DT_JMPREL=23,
    DT_MOVEENT=1879047674,
    DT_MOVESZ=1879047675,
    DT_MOVETAB=1879047934,
    DT_NEEDED=1,
    DT_NULL=0,
    DT_PLTGOT=3,
    DT_PLTPAD=1879047933,
    DT_PLTPADSZ=1879047673,
    DT_PLTREL=20,
    DT_PLTRELSZ=2,
    DT_POSFLAG_1=1879047677,
    DT_PREINIT_ARRAYSZ=33,
    DT_REL=17,
    DT_RELA=7,
    DT_RELACOUNT=1879048185,
    DT_RELAENT=9,
    DT_RELASZ=8,
    DT_RELCOUNT=1879048186,
    DT_RELENT=19,
    DT_RELSZ=18,
    DT_RPATH=15,
    DT_RUNPATH=29,
    DT_SONAME=14,
    DT_STRSZ=10,
    DT_STRTAB=5,
    DT_SYMBOLIC=16,
    DT_SYMENT=11,
    DT_SYMINENT=1879047679,
    DT_SYMINFO=1879047935,
    DT_SYMINSZ=1879047678,
    DT_SYMTAB=6,
    DT_TEXTREL=22,
    DT_TLSDESC_GOT=1879047927,
    DT_TLSDESC_PLT=1879047926,
    DT_VERDEF=1879048188,
    DT_VERDEFNUM=1879048189,
    DT_VERNEED=1879048190,
    DT_VERNEEDNUM=1879048191,
    DT_VERSYM=1879048176
} Elf64_DynTag;

typedef enum Elf_ProgramHeaderType {
    PT_DYNAMIC=2,
    PT_GNU_EH_FRAME=1685382480,
    PT_GNU_RELRO=1685382482,
    PT_GNU_STACK=1685382481,
    PT_INTERP=3,
    PT_LOAD=1,
    PT_NOTE=4,
    PT_NULL=0,
    PT_PHDR=6,
    PT_SHLIB=5,
    PT_TLS=7
} Elf_ProgramHeaderType;

typedef struct Elf64_Dyn Elf64_Dyn, *PElf64_Dyn;

struct Elf64_Dyn {
    enum Elf64_DynTag d_tag;
    qword d_val;
};

typedef struct Elf64_Shdr Elf64_Shdr, *PElf64_Shdr;

typedef enum Elf_SectionHeaderType {
    SHT_CHECKSUM=1879048184,
    SHT_DYNAMIC=6,
    SHT_DYNSYM=11,
    SHT_FINI_ARRAY=15,
    SHT_GNU_ATTRIBUTES=1879048181,
    SHT_GNU_HASH=1879048182,
    SHT_GNU_LIBLIST=1879048183,
    SHT_GNU_verdef=1879048189,
    SHT_GNU_verneed=1879048190,
    SHT_GNU_versym=1879048191,
    SHT_GROUP=17,
    SHT_HASH=5,
    SHT_INIT_ARRAY=14,
    SHT_NOBITS=8,
    SHT_NOTE=7,
    SHT_NULL=0,
    SHT_PREINIT_ARRAY=16,
    SHT_PROGBITS=1,
    SHT_REL=9,
    SHT_RELA=4,
    SHT_SHLIB=10,
    SHT_STRTAB=3,
    SHT_SUNW_COMDAT=1879048187,
    SHT_SUNW_move=1879048186,
    SHT_SUNW_syminfo=1879048188,
    SHT_SYMTAB=2,
    SHT_SYMTAB_SHNDX=18
} Elf_SectionHeaderType;

struct Elf64_Shdr {
    dword sh_name;
    enum Elf_SectionHeaderType sh_type;
    qword sh_flags;
    qword sh_addr;
    qword sh_offset;
    qword sh_size;
    dword sh_link;
    dword sh_info;
    qword sh_addralign;
    qword sh_entsize;
};

typedef struct Elf64_Phdr Elf64_Phdr, *PElf64_Phdr;

struct Elf64_Phdr {
    enum Elf_ProgramHeaderType p_type;
    dword p_flags;
    qword p_offset;
    qword p_vaddr;
    qword p_paddr;
    qword p_filesz;
    qword p_memsz;
    qword p_align;
};

typedef struct Elf64_Rela Elf64_Rela, *PElf64_Rela;

struct Elf64_Rela {
    qword r_offset; // location to apply the relocation action
    qword r_info; // the symbol table index and the type of relocation
    qword r_addend; // a constant addend used to compute the relocatable field value
};

typedef struct Elf64_Sym Elf64_Sym, *PElf64_Sym;

struct Elf64_Sym {
    dword st_name;
    byte st_info;
    byte st_other;
    word st_shndx;
    qword st_value;
    qword st_size;
};

typedef struct Elf64_Ehdr Elf64_Ehdr, *PElf64_Ehdr;

struct Elf64_Ehdr {
    byte e_ident_magic_num;
    char e_ident_magic_str[3];
    byte e_ident_class;
    byte e_ident_data;
    byte e_ident_version;
    byte e_ident_pad[9];
    word e_type;
    word e_machine;
    dword e_version;
    qword e_entry;
    qword e_phoff;
    qword e_shoff;
    dword e_flags;
    word e_ehsize;
    word e_phentsize;
    word e_phnum;
    word e_shentsize;
    word e_shnum;
    word e_shstrndx;
};




undefined8 _DT_INIT(void);
void FUN_00402510(void);
void FUN_00402790(void);
void FUN_00402a30(void);
void FUN_00402d50(void);
void entry(void);
void FUN_00402db0(void);
void FUN_00402e30(void);
void FUN_00402eb0(void);
void FUN_00402eee(void);
void FUN_00402f08(void);
void FUN_00402f36(undefined *puParm1);
void FUN_004030d2(undefined *puParm1);
void FUN_0040317c(long lParm1);
ulong FUN_004032d2(undefined8 uParm1);
ulong FUN_00403382(long lParm1,undefined8 uParm2,long lParm3);
ulong FUN_0040347f(undefined8 uParm1,undefined8 uParm2,char cParm3,undefined8 uParm4);
ulong FUN_00403535(uint uParm1);
ulong FUN_00403628(uint uParm1,undefined8 *puParm2);
ulong FUN_00403c08(char *pcParm1);
long FUN_00403c67(undefined8 uParm1);
ulong FUN_00403cab(uint uParm1,undefined8 uParm2);
undefined8 FUN_00403d53(uint uParm1,undefined8 uParm2,long lParm3,uint uParm4);
long FUN_00403ddb(long lParm1);
undefined8 FUN_00403df5(uint uParm1,undefined8 uParm2,long lParm3);
undefined8 FUN_00403e92(long lParm1,long lParm2,char cParm3,char *pcParm4,int iParm5,undefined4 *puParm6);
undefined8 FUN_00404240(int iParm1);
ulong FUN_00404273(char *pcParm1,uint uParm2);
void FUN_004042ac(undefined8 uParm1,undefined8 uParm2);
void FUN_004042e6(long lParm1);
undefined8 FUN_00404332(long lParm1,long lParm2,long lParm3,char cParm4);
ulong FUN_00404531(long lParm1,long lParm2,long lParm3);
ulong FUN_00404a97(long *plParm1,long lParm2);
ulong FUN_00404bd7(char *pcParm1);
long FUN_00404c36(long lParm1,ulong uParm2);
ulong FUN_00404c7c(long lParm1,ulong uParm2);
undefined FUN_00404d11(int iParm1);;
void FUN_00404d21(long lParm1);
ulong FUN_00404d57(void);
ulong FUN_00404db9(int iParm1);
undefined8 FUN_00404ddd(void);
undefined8 FUN_00404e05(void);
ulong FUN_00404e26(undefined8 uParm1,undefined8 uParm2);
ulong FUN_00404e6a(uint uParm1,undefined8 uParm2,undefined8 uParm3);
undefined8 FUN_00404ece(uint uParm1,undefined8 uParm2,char cParm3,long lParm4);
undefined8 FUN_00404fb0(uint param_1,uint param_2,long param_3,ulong param_4,ulong param_5,byte param_6,undefined8 param_7,undefined8 param_8,ulong param_9,long *param_10,char *param_11);
void FUN_0040530e(uint uParm1,uint uParm2);
undefined8 FUN_00405335(uint uParm1,ulong uParm2);
undefined8 FUN_004053e4(uint param_1,uint param_2,undefined8 param_3,undefined8 param_4,undefined8 param_5,long param_6,int param_7,undefined8 param_8,undefined8 param_9,undefined *param_10);
undefined8 FUN_00405924(long *plParm1,undefined8 *puParm2);
ulong FUN_0040597b(int iParm1);
undefined8 FUN_0040599f(void);
ulong FUN_004059bc(undefined8 param_1,undefined8 param_2,byte param_3,undefined8 param_4,undefined8 param_5,undefined8 *param_6,byte *param_7,byte *param_8);
ulong FUN_00405c08(long lParm1,undefined8 uParm2,uint uParm3,long lParm4,char cParm5,long lParm6);
void FUN_00405e7b(void);
undefined8 FUN_00405e8c(undefined8 uParm1,undefined8 uParm2,uint uParm3,char cParm4,long lParm5);
undefined8 FUN_004060a5(undefined8 uParm1,byte bParm2,byte bParm3,long lParm4);
void FUN_004061a4(uint uParm1,undefined8 uParm2,uint uParm3);
ulong FUN_004061e0(long lParm1);
ulong FUN_00406233(undefined8 *param_1,char *param_2,long param_3,uint param_4,uint param_5,byte *param_6,long *param_7);
ulong FUN_00407017(undefined8 uParm1,ulong *puParm2,undefined8 uParm3,ulong *puParm4,int *piParm5,undefined *puParm6);
ulong FUN_0040764d(undefined8 uParm1,uint uParm2);
void FUN_0040769a(long lParm1,undefined8 uParm2,long lParm3);
void FUN_004077ab(long lParm1);
ulong FUN_004077e3(long lParm1,undefined8 uParm2,long lParm3);
void FUN_0040789a(undefined8 uParm1,undefined8 uParm2,long lParm3);
void FUN_0040792e(void);
undefined8 FUN_0040795f(undefined8 uParm1,undefined8 uParm2,byte bParm3,char cParm4,char cParm5);
ulong FUN_00407a5b(long lParm1,char cParm2);
ulong FUN_00407a97(long lParm1,long *plParm2,undefined8 uParm3);
ulong FUN_00407c29(char *param_1,undefined8 param_2,byte param_3,long *param_4,undefined8 param_5,uint *param_6,byte param_7,char *param_8,undefined *param_9,undefined *param_10);
undefined8 FUN_0040a025(uint *puParm1);
void FUN_0040a16f(undefined8 uParm1,undefined8 uParm2,byte bParm3,undefined8 uParm4,undefined8 uParm5,undefined8 uParm6);
void FUN_0040a20f(long lParm1);
ulong FUN_0040a253(long lParm1);
ulong FUN_0040a297(long lParm1);
ulong FUN_0040a2db(void);
ulong FUN_0040a30f(ulong *puParm1,ulong uParm2);
ulong FUN_0040a338(long *plParm1,long *plParm2);
void FUN_0040a38d(long lParm1);
void FUN_0040a3bf(undefined8 uParm1,undefined8 uParm2);
undefined8 FUN_0040a416(undefined8 uParm1,undefined8 uParm2);
undefined8 FUN_0040a468(undefined8 uParm1,undefined8 uParm2,undefined8 uParm3);
void FUN_0040a505(void);
undefined8 FUN_0040a542(void);
void FUN_0040a54d(undefined4 uParm1,undefined4 *puParm2);
ulong FUN_0040a5aa(uint *puParm1);
long FUN_0040abbc(long lParm1,long lParm2);
void FUN_0040ac50(undefined8 uParm1,uint *puParm2);
ulong FUN_0040ac94(uint uParm1,undefined8 uParm2,uint uParm3,undefined8 uParm4,uint uParm5,char cParm6);
void FUN_0040ae37(undefined8 uParm1,undefined8 *puParm2);
ulong FUN_0040ae6d(undefined8 uParm1,uint uParm2,undefined8 uParm3,char cParm4);
ulong FUN_0040afe0(undefined8 uParm1,uint uParm2,undefined8 uParm3,uint uParm4,uint uParm5);
ulong FUN_0040b094(undefined8 uParm1,uint uParm2,uint uParm3);
long FUN_0040b0fd(undefined8 uParm1,ulong uParm2);
void FUN_0040b24f(char *pcParm1);
void FUN_0040b2a8(long lParm1,long lParm2,undefined uParm3);
ulong FUN_0040b3e9(long *plParm1,undefined8 uParm2,long lParm3,long lParm4,long *plParm5);
long FUN_0040b7b0(long lParm1,int iParm2,char cParm3);
void FUN_0040ba6c(undefined8 uParm1,uint uParm2);
ulong FUN_0040ba93(undefined8 uParm1,char *pcParm2);
void FUN_0040baef(undefined8 uParm1,char *pcParm2);
ulong FUN_0040bb3f(ulong uParm1,ulong uParm2,ulong uParm3);
void FUN_0040bc0a(undefined8 uParm1);
ulong FUN_0040bc29(long *plParm1,undefined8 uParm2,undefined8 uParm3);
char * FUN_0040bcd3(char *pcParm1,uint uParm2);
void FUN_0040c4d9(void);
void FUN_0040c5dd(void);
long FUN_0040c6c2(undefined8 uParm1);
long FUN_0040c792(undefined8 uParm1);
ulong FUN_0040c7c0(char *pcParm1);
undefined * FUN_0040c849(undefined8 uParm1);
char * FUN_0040c8e0(char *pcParm1);
ulong FUN_0040c949(long lParm1);
ulong FUN_0040c997(char *pcParm1);
void FUN_0040c9fc(uint uParm1,undefined8 uParm2,undefined8 uParm3,uint uParm4);
void FUN_0040ca2c(undefined4 uParm1,undefined4 uParm2,undefined4 uParm3,undefined4 uParm4,undefined4 uParm5,undefined4 uParm6,undefined4 uParm7,undefined4 uParm8,undefined8 param_1,uint param_2,undefined8 param_3,undefined8 param_4,undefined8 param_5,undefined8 param_6);
void FUN_0040cb3b(long lParm1,undefined8 uParm2,undefined8 *puParm3);
ulong FUN_0040cbd6(long lParm1,undefined8 uParm2,undefined8 *puParm3);
char * FUN_0040cc32(long lParm1);
undefined8 FUN_0040cd14(uint uParm1);
void FUN_0040cdbf(uint uParm1,undefined *puParm2);
long FUN_0040cf8a(undefined8 uParm1,undefined8 uParm2,undefined8 uParm3);
char * FUN_0040cfcb(char *pcParm1);
long FUN_0040cfeb(long lParm1,char *pcParm2,undefined8 *puParm3);
long FUN_0040d138(uint uParm1,long lParm2,long lParm3);
long FUN_0040d1b8(long *plParm1,undefined8 uParm2);
long FUN_0040d20f(long lParm1,long lParm2);
ulong FUN_0040d2a2(ulong uParm1);
ulong FUN_0040d30e(ulong uParm1);
ulong FUN_0040d355(undefined8 uParm1,ulong uParm2);
ulong FUN_0040d38c(ulong uParm1,ulong uParm2);
undefined8 FUN_0040d3a5(long lParm1);
ulong FUN_0040d49e(ulong uParm1,long lParm2);
long * FUN_0040d594(undefined8 uParm1,undefined1 *puParm2,code *pcParm3,code *pcParm4,long lParm5);
void FUN_0040d6fc(long **pplParm1,undefined8 uParm2);
long FUN_0040d826(long lParm1);
void FUN_0040d871(long lParm1,undefined8 *puParm2);
long FUN_0040d8a6(long lParm1,long lParm2,long **pplParm3,char cParm4);
undefined8 FUN_0040da3b(long lParm1,long **pplParm2,char cParm3);
undefined8 FUN_0040dc09(long *plParm1,undefined8 uParm2);
undefined8 FUN_0040de0d(long lParm1,long lParm2,long *plParm3);
undefined8 FUN_0040e15f(undefined8 uParm1,undefined8 uParm2);
long FUN_0040e1a8(long lParm1,undefined8 uParm2);
ulong FUN_0040e487(undefined8 *puParm1,ulong uParm2);
ulong FUN_0040e4d3(undefined8 *puParm1,undefined8 *puParm2);
ulong FUN_0040e54b(undefined8 *puParm1,undefined8 *puParm2);
void FUN_0040e5c3(undefined8 *puParm1);
undefined8 FUN_0040e5f4(void);
void FUN_0040e5ff(long lParm1);
ulong FUN_0040e6dc(undefined8 uParm1,uint uParm2,undefined8 uParm3,uint uParm4,uint uParm5);
ulong FUN_0040e73a(undefined8 uParm1,uint uParm2,undefined4 uParm3,undefined8 uParm4,undefined8 uParm5,undefined8 uParm6);
ulong FUN_0040e786(undefined1 *puParm1,byte bParm2,uint uParm3);
undefined8 * FUN_0040e80e(undefined8 *puParm1,int iParm2);
char * FUN_0040e885(char *pcParm1,int iParm2);
ulong FUN_0040e925(long param_1,ulong param_2,long param_3,ulong param_4,uint param_5,uint param_6,long param_7,char *param_8,char *param_9);
undefined1 * FUN_0040f7ef(int iParm1,undefined8 uParm2,undefined8 uParm3,uint *puParm4);
void FUN_0040fa62(uint uParm1,uint uParm2,undefined8 uParm3);
void FUN_0040faa3(uint uParm1,undefined8 uParm2);
void FUN_0040fac7(undefined8 uParm1,undefined8 uParm2,char cParm3);
void FUN_0040fb5b(undefined8 uParm1,char cParm2);
void FUN_0040fb85(undefined8 uParm1);
void FUN_0040fba4(uint uParm1,uint uParm2,undefined8 uParm3);
void FUN_0040fc3f(uint uParm1,undefined8 uParm2,undefined8 uParm3);
void FUN_0040fc6b(uint uParm1,undefined8 uParm2);
void FUN_0040fc94(undefined8 uParm1);
undefined8 FUN_0040fcb3(undefined4 uParm1);
ulong FUN_0040fcd2(uint uParm1,long lParm2,uint uParm3,long lParm4,uint uParm5);
undefined8 * FUN_00410134(undefined8 *puParm1);
long FUN_00410191(uint uParm1,undefined8 uParm2,undefined8 uParm3);
ulong FUN_00410202(undefined8 uParm1,undefined8 uParm2);
void FUN_004103a7(undefined8 *puParm1,undefined8 *puParm2);
long FUN_004103e2(long lParm1,uint uParm2);
undefined8 FUN_0041072a(undefined8 uParm1,uint uParm2);
void FUN_004107ac(void);
undefined8 FUN_004107b6(void);
undefined8 FUN_004107d4(void);
undefined8 FUN_004107f6(long lParm1);
undefined8 FUN_00410808(long lParm1);
undefined8 FUN_0041081a(long lParm1);
void FUN_0041082c(long lParm1);
void FUN_00410842(long lParm1);
ulong FUN_00410858(uint uParm1);
void FUN_00410868(uint uParm1,undefined8 uParm2,undefined8 uParm3);
ulong FUN_00410894(undefined8 uParm1,ulong uParm2);
ulong FUN_004108be(long lParm1,int iParm2,undefined8 uParm3,code *pcParm4,ulong uParm5);
ulong FUN_00410a85(uint uParm1);
ulong FUN_00410ad5(ulong *puParm1,ulong uParm2);
ulong FUN_00410afe(ulong *puParm1,ulong *puParm2);
ulong FUN_00410b30(undefined8 uParm1,undefined8 *puParm2,long lParm3,uint uParm4);
ulong FUN_004112b0(undefined8 *puParm1);
undefined8 FUN_00411416(undefined8 uParm1,long *plParm2);
ulong FUN_0041153f(uint uParm1,long lParm2,undefined8 *puParm3);
void FUN_00411985(undefined8 uParm1,undefined8 uParm2);
ulong FUN_004119ac(undefined8 uParm1,undefined8 *puParm2);
void FUN_00411bdf(undefined8 uParm1,long lParm2,undefined8 uParm3,undefined8 uParm4,undefined8 *puParm5,undefined8 uParm6);
void FUN_00412043(undefined8 uParm1,undefined8 uParm2,undefined8 uParm3,undefined8 uParm4,uint *puParm5);
void FUN_00412115(undefined4 uParm1,undefined4 uParm2,undefined4 uParm3,undefined4 uParm4,undefined4 uParm5,undefined4 uParm6,undefined4 uParm7,undefined4 uParm8,undefined8 param_1,undefined8 param_2,undefined8 param_3,undefined8 param_4,undefined8 param_5,undefined8 param_6);
ulong FUN_004121cb(void);
void FUN_0041220c(undefined8 uParm1,ulong uParm2,ulong uParm3);
void FUN_0041225b(long lParm1,ulong *puParm2,ulong uParm3);
void FUN_00412326(undefined8 uParm1);
long FUN_00412340(long lParm1);
long FUN_00412375(long lParm1,long lParm2);
void FUN_004123d6(undefined8 uParm1,undefined8 uParm2);
void FUN_0041240a(undefined8 uParm1);
long FUN_00412437(void);
long FUN_00412461(undefined8 uParm1,uint uParm2,undefined8 uParm3);
long FUN_004124cc(void);
ulong FUN_00412505(void);
undefined8 FUN_00412550(ulong uParm1,ulong uParm2);
ulong FUN_004125cf(uint uParm1);
void FUN_004125f8(void);
void FUN_0041262c(uint uParm1);
void FUN_004126a0(uint uParm1,uint uParm2,undefined8 uParm3,undefined8 uParm4);
void FUN_00412724(undefined4 uParm1,undefined4 uParm2,undefined4 uParm3,undefined4 uParm4,undefined4 uParm5,undefined4 uParm6,undefined4 uParm7,undefined4 uParm8,uint param_1,uint param_2,undefined8 param_3,undefined8 param_4,undefined8 param_5,undefined8 param_6);
void FUN_00412808(uint uParm1,undefined8 uParm2,uint uParm3,uint uParm4);
ulong FUN_00412833(uint uParm1,long lParm2,uint uParm3,uint uParm4);
ulong FUN_004128f6(undefined4 uParm1,undefined4 uParm2,undefined4 uParm3,undefined4 uParm4,undefined4 uParm5,undefined4 uParm6,undefined4 uParm7,undefined4 uParm8,uint param_1,uint param_2,undefined8 param_3,undefined8 param_4,undefined8 param_5,undefined8 param_6);
undefined8 FUN_00412bad(uint uParm1);
void FUN_00412c10(undefined8 uParm1);
void FUN_00412c34(void);
ulong FUN_00412c42(long lParm1);
undefined8 FUN_00412d12(undefined8 uParm1);
void FUN_00412d31(undefined8 uParm1,undefined8 uParm2,uint uParm3);
void FUN_00412d5c(uint uParm1,undefined8 uParm2,undefined8 uParm3,uint uParm4);
void FUN_00412d89(uint uParm1,undefined8 uParm2,undefined8 uParm3,uint uParm4);
ulong FUN_00412dc6(uint uParm1,long lParm2,long lParm3,uint uParm4);
ulong FUN_00412ed9(long *plParm1,long *plParm2);
ulong FUN_00412f2e(long lParm1,ulong uParm2);
undefined8 FUN_00412f58(long lParm1);
undefined8 FUN_00412ff4(long lParm1,undefined8 *puParm2);
void FUN_00413104(long lParm1,long lParm2);
void FUN_00413211(long lParm1);
void FUN_0041325e(undefined8 uParm1);
void FUN_004132a0(long lParm1,char cParm2);
long FUN_004132e3(uint uParm1,undefined8 uParm2,uint uParm3,uint *puParm4);
void FUN_00413374(long lParm1,uint uParm2,char cParm3);
ulong FUN_00413401(long lParm1);
ulong FUN_004134ab(long lParm1,undefined8 uParm2);
long * FUN_00413543(long *plParm1,uint uParm2,long lParm3);
void FUN_00413905(long lParm1,long lParm2);
undefined8 FUN_004139bf(long *plParm1);
ulong FUN_00413b4b(ulong *puParm1,ulong uParm2);
ulong FUN_00413b7c(ulong *puParm1,ulong *puParm2);
undefined8 FUN_00413bae(long lParm1);
undefined8 FUN_00413d28(undefined8 uParm1);
undefined8 FUN_00413d5e(undefined8 uParm1);
long FUN_00413dc2(long *plParm1);
undefined8 FUN_004144a6(undefined8 uParm1,long lParm2,int iParm3);
ulong FUN_004144fd(long *plParm1,long *plParm2);
void FUN_00414558(long lParm1,undefined4 uParm2);
long FUN_004145c8(long *plParm1,int iParm2);
undefined8 FUN_00414f12(long lParm1,long lParm2,char cParm3);
long FUN_004150f1(long lParm1,long lParm2,ulong uParm3);
long FUN_00415238(long lParm1,undefined8 uParm2,long lParm3);
void FUN_004152ea(long lParm1);
undefined8 FUN_00415325(long lParm1,long lParm2);
void FUN_004153f2(long lParm1,long lParm2);
long FUN_00415505(long *plParm1);
ulong FUN_0041555b(long lParm1,long lParm2,uint uParm3,long lParm4);
long FUN_00415791(long lParm1,undefined *puParm2);
void FUN_00415d1a(long lParm1,int *piParm2);
ulong FUN_00415efe(int param_1,undefined8 *param_2,char *param_3,long *param_4,int *param_5,int param_6,int *param_7,int param_8,undefined8 param_9);
char * FUN_004164e8(undefined8 uParm1,undefined8 uParm2,char *pcParm3,int *piParm4,int iParm5);
ulong FUN_004165b6(uint param_1,undefined8 *param_2,char *param_3,long param_4,undefined8 param_5,uint param_6,uint *param_7,uint param_8);
ulong FUN_00416d7f(uint param_1,undefined8 param_2,undefined8 param_3,undefined8 param_4,undefined8 param_5,uint param_6,uint param_7);
void FUN_00416e0f(uint uParm1,undefined8 uParm2,undefined8 uParm3,undefined8 uParm4,undefined8 uParm5);
ulong FUN_00416e59(long lParm1,uint uParm2,uint uParm3);
ulong FUN_00416fc6(uint uParm1,char *pcParm2,uint uParm3,undefined8 uParm4);
ulong FUN_00417199(uint uParm1,long lParm2,uint uParm3,long lParm4,uint uParm5);
void FUN_00417375(undefined8 uParm1,undefined8 uParm2);
ulong FUN_0041739a(long lParm1,long lParm2);
undefined8 FUN_00417451(long lParm1);
ulong FUN_00417482(uint *puParm1,byte *pbParm2,long lParm3,undefined8 uParm4);
ulong FUN_00417505(long lParm1,uint uParm2);
undefined8 FUN_00417641(long lParm1,uint uParm2);
undefined8 FUN_004176d4(long lParm1,uint uParm2,long lParm3);
void FUN_004177c1(uint uParm1,undefined8 uParm2,uint uParm3,uint uParm4);
ulong FUN_004177f1(undefined4 uParm1,undefined4 uParm2,undefined4 uParm3,undefined4 uParm4,undefined4 uParm5,undefined4 uParm6,undefined4 uParm7,undefined4 uParm8,uint param_1,undefined8 param_2,uint param_3,undefined8 param_4,undefined8 param_5,undefined8 param_6);
undefined8 FUN_004179b8(long lParm1,undefined8 uParm2,undefined8 uParm3);
undefined8 FUN_00417a5e(long lParm1,long lParm2);
ulong FUN_00417ac7(long lParm1,long lParm2);
void FUN_00417fd3(uint uParm1,undefined8 uParm2,uint uParm3,undefined8 uParm4);
ulong FUN_00418006(long lParm1);
void FUN_004180c3(undefined8 uParm1,undefined8 uParm2);
ulong FUN_004180e8(long lParm1,long lParm2);
undefined8 FUN_00418178(undefined8 uParm1,uint uParm2,long lParm3);
ulong FUN_00418220(long lParm1);
ulong FUN_0041838c(uint uParm1,long lParm2,uint uParm3);
ulong FUN_0041854d(undefined8 uParm1,undefined8 uParm2,undefined8 uParm3);
undefined8 FUN_0041866e(undefined8 uParm1,undefined8 uParm2,undefined4 uParm3,undefined4 *puParm4);
void FUN_004186aa(undefined8 uParm1,uint uParm2,uint uParm3);
ulong FUN_004186e6(uint *puParm1,undefined8 uParm2,uint uParm3);
void FUN_0041879a(uint uParm1,undefined8 uParm2);
void FUN_004187cf(void);
long FUN_004187df(undefined8 uParm1,long lParm2,long lParm3,long lParm4);
void FUN_0041890e(undefined8 uParm1,undefined8 uParm2,long lParm3);
void FUN_0041898f(long lParm1,long lParm2,long lParm3);
long FUN_00418ac5(undefined8 uParm1,undefined8 uParm2,undefined8 uParm3,undefined8 uParm4,undefined8 uParm5,code *pcParm6);
ulong FUN_00418b4b(ulong uParm1,undefined4 uParm2);
ulong FUN_00418b67(byte *pbParm1,byte *pbParm2);
undefined *FUN_00418bde(uint uParm1,undefined8 uParm2,undefined *puParm3,ulong uParm4,code **ppcParm5,code *pcParm6);
undefined8 FUN_00418e7d(uint uParm1,char cParm2);
undefined8 FUN_00418ef8(undefined8 uParm1);
ulong FUN_00418f83(ulong uParm1);
void FUN_00418f9f(long lParm1);
undefined8 FUN_00418fc0(long *plParm1,long *plParm2);
undefined8 FUN_00419097(void);
void FUN_004190a4(undefined8 *puParm1);
ulong FUN_004190f8(uint uParm1);
ulong FUN_004191c9(char *pcParm1,ulong uParm2);
void FUN_00419223(long lParm1,undefined4 uParm2);
ulong FUN_0041927b(long lParm1);
ulong FUN_0041928d(long lParm1,undefined4 uParm2);
ulong FUN_00419315(long lParm1);
undefined1 * FUN_00419397(void);
char * FUN_0041974a(void);
void FUN_00419818(undefined4 uParm1,undefined4 uParm2,undefined4 uParm3,undefined4 uParm4,undefined4 uParm5,undefined4 uParm6,undefined4 uParm7,undefined4 uParm8,uint param_1,undefined8 param_2,uint param_3,undefined8 param_4,undefined8 param_5,undefined8 param_6);
undefined8 * FUN_00419929(undefined8 uParm1);
undefined8 FUN_00419970(undefined8 uParm1,undefined8 uParm2);
long FUN_004199b3(long lParm1);
ulong FUN_004199c5(undefined8 *puParm1,ulong uParm2);
void FUN_00419b76(undefined8 uParm1);
ulong FUN_00419ba1(undefined8 *puParm1);
ulong * FUN_00419be7(ulong uParm1,ulong uParm2);
undefined8 * FUN_00419c47(undefined8 uParm1,undefined8 uParm2);
void FUN_00419c8e(long lParm1,ulong uParm2,ulong uParm3);
long FUN_00419ef3(long lParm1,ulong uParm2);
void FUN_00419fe2(undefined8 *puParm1,long lParm2,long lParm3);
void FUN_0041a082(ulong *puParm1,ulong uParm2,ulong uParm3);
void FUN_0041a1c7(long *plParm1,undefined8 uParm2,undefined8 uParm3);
undefined8 FUN_0041a21d(long *plParm1);
undefined8 FUN_0041a26d(undefined8 uParm1);
undefined8 FUN_0041a287(long lParm1,undefined8 uParm2);
void FUN_0041a2cb(ulong *puParm1,long *plParm2);
void FUN_0041a95b(long lParm1);
void FUN_0041aee2(uint uParm1);
ulong FUN_0041af08(long lParm1,uint uParm2,uint uParm3);
void FUN_0041b0b1(undefined8 uParm1,undefined8 uParm2);
ulong FUN_0041b0d8(undefined8 uParm1);
ulong FUN_0041b190(undefined8 uParm1,undefined8 uParm2);
undefined8 FUN_0041b237(uint uParm1,long lParm2,undefined8 uParm3,undefined8 uParm4);
ulong FUN_0041b2e5(void);
long FUN_0041b332(ulong uParm1,long lParm2,ulong uParm3,long lParm4,long *plParm5);
int * FUN_0041b57e(ulong uParm1,uint *puParm2,ulong uParm3,uint *puParm4,ulong *puParm5);
char * FUN_0041c05e(ulong uParm1,long lParm2,long lParm3);
long FUN_0041c2cc(int *param_1,long *param_2);
long FUN_0041c4b7(undefined auParm1 [16],int *piParm2,long *plParm3);
undefined8 FUN_0041c876(int iParm1,long lParm2,uint *puParm3,long lParm4,uint uParm5);
void FUN_0041d089(uint param_1);
void FUN_0041d0d3(undefined8 uParm1,uint uParm2);
ulong FUN_0041d125(void);
ulong FUN_0041d4b7(undefined auParm1 [16],undefined8 uParm2,undefined8 uParm3);
ulong FUN_0041d88f(char *pcParm1,long lParm2);
undefined8 FUN_0041d8e8(long param_1,long param_2,undefined param_3,int param_4,undefined8 param_5,ulong param_6,int param_7,ulong param_8,int param_9);
ulong FUN_00425015(uint uParm1);
long FUN_00425034(undefined8 uParm1,undefined8 uParm2);
void FUN_00425120(undefined8 uParm1,int *piParm2,undefined8 uParm3);
void FUN_004251c1(int *param_1);
ulong FUN_00425280(ulong uParm1,long lParm2);
void FUN_004252b4(undefined8 uParm1,undefined8 uParm2,undefined8 uParm3);
void FUN_004252ef(undefined8 uParm1,undefined8 uParm2,undefined8 uParm3,undefined8 uParm4);
ulong FUN_00425340(ulong uParm1,ulong uParm2);
undefined8 FUN_0042535b(uint *puParm1,ulong *puParm2);
undefined8 FUN_00425afb(char *pcParm1,ulong *puParm2,char **ppcParm3);
ulong FUN_00426db1(undefined auParm1 [16]);
ulong FUN_00426dec(void);
void FUN_00426e20(void);
undefined8 _DT_FINI(void);
undefined FUN_0062f000();
undefined FUN_0062f008();
undefined FUN_0062f010();
undefined FUN_0062f018();
undefined FUN_0062f020();
undefined FUN_0062f028();
undefined FUN_0062f030();
undefined FUN_0062f038();
undefined FUN_0062f040();
undefined FUN_0062f048();
undefined FUN_0062f050();
undefined FUN_0062f058();
undefined FUN_0062f060();
undefined FUN_0062f068();
undefined FUN_0062f070();
undefined FUN_0062f078();
undefined FUN_0062f080();
undefined FUN_0062f088();
undefined FUN_0062f090();
undefined FUN_0062f098();
undefined FUN_0062f0a0();
undefined FUN_0062f0a8();
undefined FUN_0062f0b0();
undefined FUN_0062f0b8();
undefined FUN_0062f0c0();
undefined FUN_0062f0c8();
undefined FUN_0062f0d0();
undefined FUN_0062f0d8();
undefined FUN_0062f0e0();
undefined FUN_0062f0e8();
undefined FUN_0062f0f0();
undefined FUN_0062f0f8();
undefined FUN_0062f100();
undefined FUN_0062f108();
undefined FUN_0062f110();
undefined FUN_0062f118();
undefined FUN_0062f120();
undefined FUN_0062f128();
undefined FUN_0062f130();
undefined FUN_0062f138();
undefined FUN_0062f140();
undefined FUN_0062f148();
undefined FUN_0062f150();
undefined FUN_0062f158();
undefined FUN_0062f160();
undefined FUN_0062f168();
undefined FUN_0062f170();
undefined FUN_0062f178();
undefined FUN_0062f180();
undefined FUN_0062f188();
undefined FUN_0062f190();
undefined FUN_0062f198();
undefined FUN_0062f1a0();
undefined FUN_0062f1a8();
undefined FUN_0062f1b0();
undefined FUN_0062f1b8();
undefined FUN_0062f1c0();
undefined FUN_0062f1c8();
undefined FUN_0062f1d0();
undefined FUN_0062f1d8();
undefined FUN_0062f1e0();
undefined FUN_0062f1e8();
undefined FUN_0062f1f0();
undefined FUN_0062f1f8();
undefined FUN_0062f200();
undefined FUN_0062f208();
undefined FUN_0062f210();
undefined FUN_0062f218();
undefined FUN_0062f220();
undefined FUN_0062f228();
undefined FUN_0062f230();
undefined FUN_0062f238();
undefined FUN_0062f240();
undefined FUN_0062f248();
undefined FUN_0062f250();
undefined FUN_0062f258();
undefined FUN_0062f260();
undefined FUN_0062f268();
undefined FUN_0062f270();
undefined FUN_0062f278();
undefined FUN_0062f280();
undefined FUN_0062f288();
undefined FUN_0062f290();
undefined FUN_0062f298();
undefined FUN_0062f2a0();
undefined FUN_0062f2a8();
undefined FUN_0062f2b0();
undefined FUN_0062f2b8();
undefined FUN_0062f2c0();
undefined FUN_0062f2c8();
undefined FUN_0062f2d0();
undefined FUN_0062f2d8();
undefined FUN_0062f2e0();
undefined FUN_0062f2e8();
undefined FUN_0062f2f0();
undefined FUN_0062f2f8();
undefined FUN_0062f300();
undefined FUN_0062f308();
undefined FUN_0062f310();
undefined FUN_0062f318();
undefined FUN_0062f320();
undefined FUN_0062f328();
undefined FUN_0062f330();
undefined FUN_0062f338();
undefined FUN_0062f340();
undefined FUN_0062f348();
undefined FUN_0062f350();
undefined FUN_0062f358();
undefined FUN_0062f360();
undefined FUN_0062f368();
undefined FUN_0062f370();
undefined FUN_0062f378();
undefined FUN_0062f380();
undefined FUN_0062f388();
undefined FUN_0062f390();
undefined FUN_0062f398();
undefined FUN_0062f3a0();
undefined FUN_0062f3a8();
undefined FUN_0062f3b0();
undefined FUN_0062f3b8();
undefined FUN_0062f3c0();
undefined FUN_0062f3c8();
undefined FUN_0062f3d0();
undefined FUN_0062f3d8();
undefined FUN_0062f3e0();
undefined FUN_0062f3e8();
undefined FUN_0062f3f0();
undefined FUN_0062f3f8();
undefined FUN_0062f400();
undefined FUN_0062f408();



#include "mv.h"

long null_ARRAY_0062e73_0_8_;
long null_ARRAY_0062e73_8_8_;
long null_ARRAY_0062ed0_0_8_;
long null_ARRAY_0062ed0_16_8_;
long null_ARRAY_0062ed0_24_8_;
long null_ARRAY_0062ed0_32_8_;
long null_ARRAY_0062ed0_40_8_;
long null_ARRAY_0062ed0_48_8_;
long null_ARRAY_0062ed0_8_8_;
long null_ARRAY_0062ee6_0_4_;
long null_ARRAY_0062ee6_16_8_;
long null_ARRAY_0062ee6_4_4_;
long null_ARRAY_0062ee6_8_4_;
long DAT_0042708e;
long DAT_00427145;
long DAT_004271c3;
long DAT_00427480;
long DAT_00427a96;
long DAT_00427c15;
long DAT_00427ce0;
long DAT_00427ce3;
long DAT_00427d1b;
long DAT_00427fc7;
long DAT_00428798;
long DAT_004289e4;
long DAT_00428a9f;
long DAT_00428af2;
long DAT_00428af4;
long DAT_00428be1;
long DAT_00428bfd;
long DAT_00428c13;
long DAT_00428d80;
long DAT_00428ede;
long DAT_00428ee2;
long DAT_00428ee7;
long DAT_00428ee9;
long DAT_00429540;
long DAT_00429542;
long DAT_00429560;
long DAT_0042962b;
long DAT_004299e0;
long DAT_00429a1d;
long DAT_00429a22;
long DAT_00429a38;
long DAT_00429a39;
long DAT_00429a3b;
long DAT_00429aa8;
long DAT_00429aaa;
long DAT_00429aac;
long DAT_00429b07;
long DAT_00429b98;
long DAT_00429b9b;
long DAT_00429be9;
long DAT_00429c42;
long DAT_00429c7d;
long DAT_00429cf0;
long DAT_00429cf1;
long DAT_00429d2f;
long DAT_0062e120;
long DAT_0062e130;
long DAT_0062e140;
long DAT_0062e700;
long DAT_0062e708;
long DAT_0062e718;
long DAT_0062e720;
long DAT_0062e798;
long DAT_0062e79c;
long DAT_0062e7a0;
long DAT_0062e7c0;
long DAT_0062e7c8;
long DAT_0062e7d0;
long DAT_0062e7e0;
long DAT_0062e7e8;
long DAT_0062e800;
long DAT_0062e808;
long DAT_0062e850;
long DAT_0062e880;
long DAT_0062e888;
long DAT_0062e890;
long DAT_0062ecc0;
long DAT_0062ecc8;
long DAT_0062ecd0;
long DAT_0062ecd8;
long DAT_0062ece0;
long DAT_0062ece8;
long DAT_0062ee40;
long DAT_0062ee48;
long DAT_0062ee50;
long DAT_0062ee54;
long DAT_0062ee58;
long DAT_0062ee59;
long DAT_0062ee5c;
long DAT_0062ee98;
long DAT_0062ee9c;
long DAT_0062eea0;
long DAT_0062eef8;
long DAT_0062ef00;
long DAT_0062ef10;
long fde_0042b2a0;
long FLOAT_UNKNOWN;
long null_ARRAY_004272c0;
long null_ARRAY_00428a91;
long null_ARRAY_00428b40;
long null_ARRAY_00428ba0;
long null_ARRAY_00428c90;
long null_ARRAY_00429550;
long null_ARRAY_0042a160;
long null_ARRAY_0042a360;
long null_ARRAY_0062e730;
long null_ARRAY_0062e760;
long null_ARRAY_0062e820;
long null_ARRAY_0062e860;
long null_ARRAY_0062e8c0;
long null_ARRAY_0062ed00;
long null_ARRAY_0062ed40;
long null_ARRAY_0062ee60;
long PTR_DAT_0062e710;
long PTR_FUN_0062e7a8;
long PTR_null_ARRAY_0062e740;
long PTR_null_ARRAY_0062e7b0;
long stack0x00000008;
ulong
FUN_00403535 (uint uParm1)
{
  char cVar1;
  byte bVar2;
  int iVar3;
  uint *puVar4;
  undefined8 uVar5;
  char *pcVar6;
  uint uVar7;
  undefined auStack336[24];
  uint uStack312;
  undefined4 auStack192[2];
  int iStack184;
  undefined uStack159;
  undefined uStack155;
  undefined uStack147;
  undefined uStack146;
  undefined8 *puStack112;
  int iStack100;
  char cStack93;
  int iStack92;
  int iStack88;
  char cStack81;
  long lStack80;
  long lStack72;
  long lStack64;
  char cStack50;
  bool bStack49;
  code *pcStack32;
  uint local_c;

  if (uParm1 == 0)
    {
      pcStack32 = (code *) 0x403592;
      local_c = uParm1;
      func_0x00402580
	("Usage: %s [OPTION]... [-T] SOURCE DEST\n  or:  %s [OPTION]... SOURCE... DIRECTORY\n  or:  %s [OPTION]... -t DIRECTORY SOURCE...\n",
	 DAT_0062ece8, DAT_0062ece8, DAT_0062ece8);
      pcStack32 = (code *) 0x4035a6;
      func_0x00402920
	("Rename SOURCE to DEST, or move SOURCE(s) to DIRECTORY.\n",
	 DAT_0062e7c0);
      pcStack32 = (code *) 0x4035ab;
      FUN_00402eee ();
      pcStack32 = (code *) 0x4035bf;
      func_0x00402920
	("      --backup[=CONTROL]       make a backup of each existing destination file\n  -b                           like --backup but does not accept an argument\n  -f, --force                  do not prompt before overwriting\n  -i, --interactive            prompt before overwrite\n  -n, --no-clobber             do not overwrite an existing file\nIf you specify more than one of -i, -f, -n, only the final one takes effect.\n",
	 DAT_0062e7c0);
      pcStack32 = (code *) 0x4035d3;
      func_0x00402920
	("      --strip-trailing-slashes  remove any trailing slashes from each SOURCE\n                                 argument\n  -S, --suffix=SUFFIX          override the usual backup suffix\n",
	 DAT_0062e7c0);
      pcStack32 = (code *) 0x4035e7;
      func_0x00402920
	("  -t, --target-directory=DIRECTORY  move all SOURCE arguments into DIRECTORY\n  -T, --no-target-directory    treat DEST as a normal file\n  -u, --update                 move only when the SOURCE file is newer\n                                 than the destination file or when the\n                                 destination file is missing\n  -v, --verbose                explain what is being done\n  -Z, --context                set SELinux security context of destination\n                                 file to default type\n",
	 DAT_0062e7c0);
      pcStack32 = (code *) 0x4035fb;
      func_0x00402920 ("      --help     display this help and exit\n",
		       DAT_0062e7c0);
      pcStack32 = (code *) 0x40360f;
      pcVar6 = (char *) DAT_0062e7c0;
      func_0x00402920
	("      --version  output version information and exit\n");
      pcStack32 = (code *) 0x403614;
      FUN_00402f08 ();
      pcStack32 = (code *) 0x40361e;
      imperfection_wrapper ();	//    FUN_00402f36();
    }
  else
    {
      pcVar6 = "Try \'%s --help\' for more information.\n";
      pcStack32 = (code *) 0x403566;
      local_c = uParm1;
      func_0x00402910 (DAT_0062e7e0,
		       "Try \'%s --help\' for more information.\n",
		       DAT_0062ece8);
    }
  pcStack32 = FUN_00403628;
  uVar7 = local_c;
  func_0x00402c10 ();
  cStack50 = '\0';
  lStack64 = 0;
  lStack72 = 0;
  lStack80 = 0;
  cStack81 = '\0';
  cStack93 = '\0';
  pcStack32 = (code *) & stack0xfffffffffffffff8;
  FUN_0040e5ff (*(undefined8 *) pcVar6);
  func_0x00402b90 (6, &DAT_004271c3);
  func_0x00402b70 (FUN_0040c4d9);
  FUN_0040317c (auStack192);
  FUN_0040e5f4 ();
LAB_004038ef:
  ;
  do
    {
      imperfection_wrapper ();	//    iStack100 = FUN_00416e0f((ulong)uVar7, pcVar6, "bfint:uvS:TZ", null_ARRAY_004272c0, 0);
      if (iStack100 == -1)
	break;
      if (iStack100 != 0x66)
	{
	  if (iStack100 < 0x67)
	    {
	      if (iStack100 == 0x53)
		{
		  cStack50 = '\x01';
		  lStack64 = DAT_0062ef10;
		  goto LAB_004038ef;
		}
	      if (iStack100 < 0x54)
		{
		  if (iStack100 != -0x83)
		    {
		      if (iStack100 != -0x82)
			goto LAB_004038e5;
		      FUN_00403535 (0);
		    }
		  imperfection_wrapper ();	//          FUN_00412115(DAT_0062e7c0, &DAT_00427a96, "GNU coreutils", PTR_DAT_0062e710, "Mike Parker", "David MacKenzie", "Jim Meyering", 0);
		  func_0x00402c10 (0);
		}
	      else
		{
		  if (iStack100 == 0x5a)
		    {
		      if (cStack93 != '\0')
			{
			  uStack155 = 0;
			  uStack159 = 1;
			}
		      goto LAB_004038ef;
		    }
		  if (iStack100 == 0x62)
		    {
		      cStack50 = '\x01';
		      if (DAT_0062ef10 != 0)
			{
			  lStack72 = DAT_0062ef10;
			}
		      goto LAB_004038ef;
		    }
		  if (iStack100 == 0x54)
		    {
		      cStack81 = '\x01';
		      goto LAB_004038ef;
		    }
		}
	    }
	  else
	    {
	      if (iStack100 == 0x74)
		{
		  if (lStack80 != 0)
		    {
		      imperfection_wrapper ();	//            FUN_00412724(1, 0, "multiple target directories specified");
		    }
		  imperfection_wrapper ();	//          iVar3 = FUN_004180e8(DAT_0062ef10, auStack336, auStack336);
		  if (iVar3 != 0)
		    {
		      imperfection_wrapper ();	//            uVar5 = FUN_0040faa3(4, DAT_0062ef10);
		      puVar4 = (uint *) func_0x00402c00 ();
		      imperfection_wrapper ();	//            FUN_00412724(1, (ulong)*puVar4, "failed to access %s", uVar5);
		    }
		  if ((uStack312 & 0xf000) != 0x4000)
		    {
		      imperfection_wrapper ();	//            uVar5 = FUN_0040faa3(4, DAT_0062ef10);
		      imperfection_wrapper ();	//            FUN_00412724(1, 0, "target %s is not a directory", uVar5);
		    }
		  lStack80 = DAT_0062ef10;
		  goto LAB_004038ef;
		}
	      if (iStack100 < 0x75)
		{
		  if (iStack100 == 0x69)
		    {
		      iStack184 = 3;
		      goto LAB_004038ef;
		    }
		  if (iStack100 == 0x6e)
		    {
		      iStack184 = 2;
		      goto LAB_004038ef;
		    }
		}
	      else
		{
		  if (iStack100 == 0x76)
		    {
		      uStack146 = 1;
		      goto LAB_004038ef;
		    }
		  if (iStack100 < 0x76)
		    {
		      uStack147 = 1;
		      goto LAB_004038ef;
		    }
		  if (iStack100 == 0x80)
		    {
		      DAT_0062e850 = 1;
		      goto LAB_004038ef;
		    }
		}
	    }
	LAB_004038e5:
	  ;
	  imperfection_wrapper ();	//      FUN_00403535();
	  goto LAB_004038ef;
	}
      iStack184 = 1;
    }
  while (true);
  iStack88 = uVar7 - DAT_0062e798;
  puStack112 = (undefined8 *) pcVar6 + (long) DAT_0062e798;
  if (iStack88 <= (int) (uint) (lStack80 == 0))
    {
      if (iStack88 < 1)
	{
	  imperfection_wrapper ();	//      FUN_00412724(0, 0, "missing file operand");
	}
      else
	{
	  imperfection_wrapper ();	//      uVar5 = FUN_0040faa3(4, *puStack112);
	  imperfection_wrapper ();	//      FUN_00412724(0, 0, "missing destination file operand after %s", uVar5);
	}
      FUN_00403535 (1);
    }
  if (cStack81 != '\0')
    {
      if (lStack80 != 0)
	{
	  imperfection_wrapper ();	//      FUN_00412724(1, 0, "cannot combine --target-directory (-t) and --no-target-directory (-T)");
	}
      if (iStack88 < 3)
	goto LAB_00403ae1;
      imperfection_wrapper ();	//    uVar5 = FUN_0040faa3(4, puStack112[2]);
      imperfection_wrapper ();	//    FUN_00412724(0, 0, "extra operand %s", uVar5);
      FUN_00403535 (1);
    }
  if (lStack80 == 0)
    {
      if (iStack88 < 2)
	{
	  func_0x00402af0 ("2 <= n_files", "src/mv.c", 0x1c6, &DAT_00427c15);
	}
      cVar1 = FUN_004032d2 (puStack112[(long) iStack88 + -1]);
      if (cVar1 == '\0')
	{
	  if (2 < iStack88)
	    {
	      imperfection_wrapper ();	//        uVar5 = FUN_0040faa3(4, puStack112[(long)iStack88 + -1]);
	      imperfection_wrapper ();	//        FUN_00412724(1, 0, "target %s is not a directory", uVar5);
	    }
	}
      else
	{
	  iStack88 = iStack88 + -1;
	  lStack80 = puStack112[(long) iStack88];
	}
    }
LAB_00403ae1:
  ;
  if ((cStack50 != '\0') && (iStack184 == 2))
    {
      imperfection_wrapper ();	//    FUN_00412724(0, 0, "options --backup and --no-clobber are mutually exclusive");
      FUN_00403535 (1);
    }
  if (cStack50 == '\0')
    {
      auStack192[0] = 0;
    }
  else
    {
      imperfection_wrapper ();	//    auStack192[0] = FUN_0040baef("backup type", lStack72);
    }
  FUN_0040b24f (lStack64);
  FUN_0040a505 ();
  if (lStack80 == 0)
    {
      bStack49 =
	(bool) FUN_0040347f (*puStack112, puStack112[1], 0, auStack192);
    }
  else
    {
      if (1 < iStack88)
	{
	  FUN_004077ab (auStack192);
	}
      bStack49 = true;
      iStack92 = 0;
      while (iStack92 < iStack88)
	{
	  bVar2 =
	    FUN_0040347f (puStack112[(long) iStack92], lStack80, 1,
			  auStack192);
	  bStack49 = (bVar2 & bStack49) != 0;
	  iStack92 = iStack92 + 1;
	}
    }
  return (ulong) (bStack49 == false);
}

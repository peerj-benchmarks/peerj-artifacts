
#include "chown.h"

long null_ARRAY_0061f4f_0_8_;
long null_ARRAY_0061f4f_8_8_;
long null_ARRAY_0061f68_0_8_;
long null_ARRAY_0061f68_16_8_;
long null_ARRAY_0061f68_24_8_;
long null_ARRAY_0061f68_32_8_;
long null_ARRAY_0061f68_40_8_;
long null_ARRAY_0061f68_48_8_;
long null_ARRAY_0061f68_8_8_;
long null_ARRAY_0061f7c_0_4_;
long null_ARRAY_0061f7c_16_8_;
long null_ARRAY_0061f7c_4_4_;
long null_ARRAY_0061f7c_8_4_;
long DAT_00419b40;
long DAT_00419bfd;
long DAT_00419c7b;
long DAT_0041a96f;
long DAT_0041a978;
long DAT_0041ab58;
long DAT_0041ac12;
long DAT_0041adf9;
long DAT_0041ae40;
long DAT_0041af9e;
long DAT_0041afa2;
long DAT_0041afa7;
long DAT_0041afa9;
long DAT_0041b600;
long DAT_0041b602;
long DAT_0041b643;
long DAT_0041ba00;
long DAT_0041bdae;
long DAT_0041bdb3;
long DAT_0041bdc8;
long DAT_0041bdc9;
long DAT_0041bdcb;
long DAT_0041be8f;
long DAT_0041bf20;
long DAT_0041bf23;
long DAT_0041bf71;
long DAT_0041bfe2;
long DAT_0041c108;
long DAT_0041c109;
long DAT_0041c17e;
long DAT_0041c19e;
long DAT_0041c1f6;
long DAT_0041c1f8;
long DAT_0041c1fa;
long DAT_0061f000;
long DAT_0061f010;
long DAT_0061f020;
long DAT_0061f4c8;
long DAT_0061f4e0;
long DAT_0061f570;
long DAT_0061f574;
long DAT_0061f578;
long DAT_0061f580;
long DAT_0061f590;
long DAT_0061f5a0;
long DAT_0061f5a8;
long DAT_0061f5c0;
long DAT_0061f5c8;
long DAT_0061f610;
long DAT_0061f630;
long DAT_0061f638;
long DAT_0061f640;
long DAT_0061f7f8;
long DAT_0061f7fc;
long DAT_0061f800;
long DAT_0061f808;
long DAT_0061f810;
long DAT_0061f818;
long DAT_0061f828;
long fde_0041d208;
long FLOAT_UNKNOWN;
long null_ARRAY_00419d40;
long null_ARRAY_0041bff0;
long null_ARRAY_0041c620;
long null_ARRAY_0061f4f0;
long null_ARRAY_0061f520;
long null_ARRAY_0061f5e0;
long null_ARRAY_0061f620;
long null_ARRAY_0061f680;
long null_ARRAY_0061f6c0;
long null_ARRAY_0061f7c0;
long PTR_DAT_0061f4c0;
long PTR_null_ARRAY_0061f500;
long PTR_s_invalid_group_0061f568;
long PTR_s_invalid_spec_0061f558;
long PTR_s_invalid_user_0061f560;
long stack0x00000008;
ulong
FUN_00402568 (uint uParm1)
{
  char cVar1;
  int iVar2;
  uint *puVar3;
  undefined8 uVar4;
  char *pcVar5;
  uint uVar6;
  undefined8 uVar7;
  undefined auStack288[28];
  uint uStack260;
  uint uStack256;
  undefined4 uStack144;
  char cStack140;
  long lStack136;
  bool bStack128;
  undefined uStack127;
  long lStack120;
  long lStack112;
  uint uStack100;
  uint uStack96;
  uint uStack92;
  uint auStack88[2];
  long lStack80;
  long lStack72;
  int iStack64;
  int iStack60;
  uint uStack56;
  char cStack49;
  code *pcStack32;
  uint local_c;

  if (uParm1 == 0)
    {
      pcStack32 = (code *) 0x4025be;
      local_c = uParm1;
      func_0x00401c50
	("Usage: %s [OPTION]... [OWNER][:[GROUP]] FILE...\n  or:  %s [OPTION]... --reference=RFILE FILE...\n",
	 DAT_0061f640, DAT_0061f640);
      pcStack32 = (code *) 0x4025d2;
      func_0x00401ee0
	("Change the owner and/or group of each FILE to OWNER and/or GROUP.\nWith --reference, change the owner and group of each FILE to those of RFILE.\n\n",
	 DAT_0061f580);
      pcStack32 = (code *) 0x4025e6;
      func_0x00401ee0
	("  -c, --changes          like verbose but report only when a change is made\n  -f, --silent, --quiet  suppress most error messages\n  -v, --verbose          output a diagnostic for every file processed\n",
	 DAT_0061f580);
      pcStack32 = (code *) 0x4025fa;
      func_0x00401ee0
	("      --dereference      affect the referent of each symbolic link (this is\n                         the default), rather than the symbolic link itself\n  -h, --no-dereference   affect symbolic links instead of any referenced file\n",
	 DAT_0061f580);
      pcStack32 = (code *) 0x40260e;
      func_0x00401ee0
	("                         (useful only on systems that can change the\n                         ownership of a symlink)\n",
	 DAT_0061f580);
      pcStack32 = (code *) 0x402622;
      func_0x00401ee0
	("      --from=CURRENT_OWNER:CURRENT_GROUP\n                         change the owner and/or group of each file only if\n                         its current owner and/or group match those specified\n                         here.  Either may be omitted, in which case a match\n                         is not required for the omitted attribute\n",
	 DAT_0061f580);
      pcStack32 = (code *) 0x402636;
      func_0x00401ee0
	("      --no-preserve-root  do not treat \'/\' specially (the default)\n      --preserve-root    fail to operate recursively on \'/\'\n",
	 DAT_0061f580);
      pcStack32 = (code *) 0x40264a;
      func_0x00401ee0
	("      --reference=RFILE  use RFILE\'s owner and group rather than\n                         specifying OWNER:GROUP values\n",
	 DAT_0061f580);
      pcStack32 = (code *) 0x40265e;
      func_0x00401ee0
	("  -R, --recursive        operate on files and directories recursively\n",
	 DAT_0061f580);
      pcStack32 = (code *) 0x402672;
      func_0x00401ee0
	("\nThe following options modify how a hierarchy is traversed when the -R\noption is also specified.  If more than one is specified, only the final\none takes effect.\n\n  -H                     if a command line argument is a symbolic link\n                         to a directory, traverse it\n  -L                     traverse every symbolic link to a directory\n                         encountered\n  -P                     do not traverse any symbolic links (default)\n\n",
	 DAT_0061f580);
      pcStack32 = (code *) 0x402686;
      func_0x00401ee0 ("      --help     display this help and exit\n",
		       DAT_0061f580);
      pcStack32 = (code *) 0x40269a;
      func_0x00401ee0
	("      --version  output version information and exit\n",
	 DAT_0061f580);
      pcStack32 = (code *) 0x4026ae;
      func_0x00401ee0
	("\nOwner is unchanged if missing.  Group is unchanged if missing, but changed\nto login group if implied by a \':\' following a symbolic OWNER.\nOWNER and GROUP may be numeric as well as symbolic.\n",
	 DAT_0061f580);
      pcStack32 = (code *) 0x4026d5;
      pcVar5 = (char *) DAT_0061f640;
      func_0x00401c50
	("\nExamples:\n  %s root /u        Change the owner of /u to \"root\".\n  %s root:staff /u  Likewise, but also change its group to \"staff\".\n  %s -hR root /u    Change the owner of /u and subfiles to \"root\".\n",
	 DAT_0061f640, DAT_0061f640, DAT_0061f640);
      pcStack32 = (code *) 0x4026df;
      imperfection_wrapper ();	//    FUN_004023be();
    }
  else
    {
      pcVar5 = "Try \'%s --help\' for more information.\n";
      pcStack32 = (code *) 0x402599;
      local_c = uParm1;
      func_0x00401ed0 (DAT_0061f5a0,
		       "Try \'%s --help\' for more information.\n",
		       DAT_0061f640);
    }
  pcStack32 = FUN_004026e9;
  uVar6 = local_c;
  func_0x004020f0 ();
  cStack49 = '\0';
  auStack88[0] = 0xffffffff;
  uStack92 = 0xffffffff;
  uStack96 = 0xffffffff;
  uStack100 = 0xffffffff;
  uStack56 = 0x10;
  iStack60 = -1;
  pcStack32 = (code *) & stack0xfffffffffffffff8;
  FUN_00403e1b (*(undefined8 *) pcVar5);
  func_0x00402090 (6, &DAT_00419c7b);
  func_0x00402070 (FUN_00403c4f);
  FUN_00402c7d (&uStack144);
LAB_0040294c:
  ;
  while (true)
    {
      uVar7 = 0x402970;
      imperfection_wrapper ();	//    iStack64 = FUN_0040a650((ulong)uVar6, pcVar5, "HLPRcfhv", null_ARRAY_00419d40, 0);
      if (iStack64 == -1)
	{
	  if (cStack140 == '\0')
	    {
	      uStack56 = 0x10;
	    }
	  else
	    {
	      if (uStack56 == 0x10)
		{
		  if (iStack60 == 1)
		    {
		      imperfection_wrapper ();	//            FUN_0040677d(1, 0, "-R --dereference requires either -H or -L");
		    }
		  iStack60 = 0;
		}
	    }
	  bStack128 = iStack60 != 0;
	  if (DAT_0061f610 == 0)
	    {
	      iVar2 = 2;
	    }
	  else
	    {
	      iVar2 = 1;
	    }
	  if ((int) (uVar6 - DAT_0061f570) < iVar2)
	    {
	      if (DAT_0061f570 < (int) uVar6)
		{
		  imperfection_wrapper ();	//          uVar7 = FUN_00405406(((undefined8 *)pcVar5)[(long)(int)uVar6 + -1]);
		  imperfection_wrapper ();	//          FUN_0040677d(0, 0, "missing operand after %s", uVar7);
		}
	      else
		{
		  imperfection_wrapper ();	//          FUN_0040677d(0, 0, "missing operand");
		}
	      FUN_00402568 (1);
	    }
	  if (DAT_0061f610 == 0)
	    {
	      uVar7 = 0x402b3f;
	      lStack80 =
		FUN_0040583b (((undefined8 *) pcVar5)[(long) DAT_0061f570],
			      auStack88, &uStack92, &lStack120, &lStack112);
	      if (lStack80 != 0)
		{
		  imperfection_wrapper ();	//          uVar4 = FUN_00405406(((undefined8 *)pcVar5)[(long)DAT_0061f570]);
		  uVar7 = 0x402b95;
		  imperfection_wrapper ();	//          FUN_0040677d(1, 0, "%s: %s", lStack80, uVar4);
		}
	      if ((lStack120 == 0) && (lStack112 != 0))
		{
		  uVar7 = 0x402bb1;
		  lStack120 = FUN_0040255a (&DAT_00419c7b);
		}
	      DAT_0061f570 = DAT_0061f570 + 1;
	    }
	  else
	    {
	      imperfection_wrapper ();	//        iVar2 = FUN_0040ad5a(DAT_0061f610, auStack288, auStack288);
	      if (iVar2 != 0)
		{
		  imperfection_wrapper ();	//          uVar7 = FUN_00405215(4, DAT_0061f610);
		  puVar3 = (uint *) func_0x004020e0 ();
		  imperfection_wrapper ();	//          FUN_0040677d(1, (ulong)*puVar3, "failed to get attributes of %s", uVar7);
		}
	      auStack88[0] = uStack260;
	      uStack92 = uStack256;
	      imperfection_wrapper ();	//        lStack120 = FUN_00402d12((ulong)uStack260);
	      uVar7 = 0x402af6;
	      imperfection_wrapper ();	//        lStack112 = FUN_00402ccd((ulong)uStack256);
	    }
	  if ((cStack140 != '\0') && (cStack49 != '\0'))
	    {
	      uVar7 = 0x402bdc;
	      lStack136 = FUN_00405425 (null_ARRAY_0061f620);
	      if (lStack136 == 0)
		{
		  imperfection_wrapper ();	//          uVar4 = FUN_00405215(4, &DAT_0041a96f);
		  puVar3 = (uint *) func_0x004020e0 ();
		  uVar7 = 0x402c1b;
		  imperfection_wrapper ();	//          FUN_0040677d(1, (ulong)*puVar3, "failed to get attributes of %s", uVar4);
		}
	    }
	  uStack56 = uStack56 | 0x400;
	  imperfection_wrapper ();	//      cVar1 = FUN_00403b0c((undefined8 *)pcVar5 + (long)DAT_0061f570, (ulong)uStack56, (ulong)auStack88[0], (ulong)uStack92, (ulong)uStack96, (ulong)uStack100, &uStack144, uVar7);
	  return (ulong) (cVar1 == '\0');
	}
      if (iStack64 != 0x66)
	break;
      uStack127 = 1;
    }
  if (iStack64 < 0x67)
    {
      if (iStack64 == 0x4c)
	{
	  uStack56 = 2;
	  goto LAB_0040294c;
	}
      if (iStack64 < 0x4d)
	{
	  if (iStack64 == -0x82)
	    {
	      uVar7 = 0x4028fa;
	      FUN_00402568 (0);
	    }
	  else
	    {
	      if (iStack64 == 0x48)
		{
		  uStack56 = 0x11;
		  goto LAB_0040294c;
		}
	      if (iStack64 != -0x83)
		goto LAB_00402942;
	    }
	  imperfection_wrapper ();	//      FUN_00405e44(DAT_0061f580, "chown", "GNU coreutils", PTR_DAT_0061f4c0, "David MacKenzie", "Jim Meyering", 0, uVar7);
	  func_0x004020f0 (0);
	}
      else
	{
	  if (iStack64 == 0x52)
	    {
	      cStack140 = '\x01';
	      goto LAB_0040294c;
	    }
	  if (iStack64 == 99)
	    {
	      uStack144 = 1;
	      goto LAB_0040294c;
	    }
	  if (iStack64 == 0x50)
	    {
	      uStack56 = 0x10;
	      goto LAB_0040294c;
	    }
	}
    }
  else
    {
      if (iStack64 == 0x81)
	{
	  lStack72 = FUN_0040583b (DAT_0061f828, &uStack96, &uStack100, 0, 0);
	  if (lStack72 != 0)
	    {
	      imperfection_wrapper ();	//        uVar7 = FUN_00405406(DAT_0061f828);
	      imperfection_wrapper ();	//        FUN_0040677d(1, 0, "%s: %s", lStack72, uVar7);
	    }
	  goto LAB_0040294c;
	}
      if (iStack64 < 0x82)
	{
	  if (iStack64 == 0x76)
	    {
	      uStack144 = 0;
	    }
	  else
	    {
	      if (iStack64 == 0x80)
		{
		  iStack60 = 1;
		}
	      else
		{
		  if (iStack64 != 0x68)
		    goto LAB_00402942;
		  iStack60 = 0;
		}
	    }
	  goto LAB_0040294c;
	}
      if (iStack64 == 0x83)
	{
	  cStack49 = '\x01';
	  goto LAB_0040294c;
	}
      if (iStack64 < 0x83)
	{
	  cStack49 = '\0';
	  goto LAB_0040294c;
	}
      if (iStack64 == 0x84)
	{
	  DAT_0061f610 = DAT_0061f828;
	  goto LAB_0040294c;
	}
    }
LAB_00402942:
  ;
  imperfection_wrapper ();	//  FUN_00402568();
  goto LAB_0040294c;
}

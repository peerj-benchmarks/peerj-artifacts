typedef unsigned char   undefined;

typedef unsigned char    byte;
typedef unsigned char    dwfenc;
typedef unsigned int    dword;
typedef unsigned long    qword;
typedef unsigned char    undefined1;
typedef unsigned short    undefined2;
typedef unsigned int    undefined4;
typedef unsigned long    undefined6;
typedef unsigned long    undefined8;
typedef unsigned short    word;
#define unkbyte9   unsigned long long
#define unkbyte10   unsigned long long
#define unkbyte11   unsigned long long
#define unkbyte12   unsigned long long
#define unkbyte13   unsigned long long
#define unkbyte14   unsigned long long
#define unkbyte15   unsigned long long
#define unkbyte16   unsigned long long

#define unkuint9   unsigned long long
#define unkuint10   unsigned long long
#define unkuint11   unsigned long long
#define unkuint12   unsigned long long
#define unkuint13   unsigned long long
#define unkuint14   unsigned long long
#define unkuint15   unsigned long long
#define unkuint16   unsigned long long

#define unkint9   long long
#define unkint10   long long
#define unkint11   long long
#define unkint12   long long
#define unkint13   long long
#define unkint14   long long
#define unkint15   long long
#define unkint16   long long

#define unkfloat1   float
#define unkfloat2   float
#define unkfloat3   float
#define unkfloat5   double
#define unkfloat6   double
#define unkfloat7   double
#define unkfloat9   long double
#define unkfloat11   long double
#define unkfloat12   long double
#define unkfloat13   long double
#define unkfloat14   long double
#define unkfloat15   long double
#define unkfloat16   long double

#define BADSPACEBASE   void
#define code   void

#define ulong long
#define true 1
#define bool int
#define false 0
#define float10 int
#define ushort int
#define undefined7 long
#define undefined2 long
#define uint5 long
#define uint6 long

// Add definition for uint
typedef unsigned int    uint;

#define sbyte char

extern imperfection_wrapper(void);
typedef struct eh_frame_hdr eh_frame_hdr, *Peh_frame_hdr;

struct eh_frame_hdr {
    byte eh_frame_hdr_version; // Exception Handler Frame Header Version
    dwfenc eh_frame_pointer_encoding; // Exception Handler Frame Pointer Encoding
    dwfenc eh_frame_desc_entry_count_encoding; // Encoding of # of Exception Handler FDEs
    dwfenc eh_frame_table_encoding; // Exception Handler Table Encoding
};

typedef struct fde_table_entry fde_table_entry, *Pfde_table_entry;

struct fde_table_entry {
    dword initial_loc; // Initial Location
    dword data_loc; // Data location
};

typedef enum Elf64_DynTag {
    DT_AUDIT=1879047932,
    DT_AUXILIARY=2147483645,
    DT_BIND_NOW=24,
    DT_CHECKSUM=1879047672,
    DT_CONFIG=1879047930,
    DT_DEBUG=21,
    DT_DEPAUDIT=1879047931,
    DT_ENCODING=32,
    DT_FEATURE_1=1879047676,
    DT_FILTER=2147483647,
    DT_FINI=13,
    DT_FINI_ARRAY=26,
    DT_FINI_ARRAYSZ=28,
    DT_FLAGS=30,
    DT_FLAGS_1=1879048187,
    DT_GNU_CONFLICT=1879047928,
    DT_GNU_CONFLICTSZ=1879047670,
    DT_GNU_HASH=1879047925,
    DT_GNU_LIBLIST=1879047929,
    DT_GNU_LIBLISTSZ=1879047671,
    DT_GNU_PRELINKED=1879047669,
    DT_HASH=4,
    DT_INIT=12,
    DT_INIT_ARRAY=25,
    DT_INIT_ARRAYSZ=27,
    DT_JMPREL=23,
    DT_MOVEENT=1879047674,
    DT_MOVESZ=1879047675,
    DT_MOVETAB=1879047934,
    DT_NEEDED=1,
    DT_NULL=0,
    DT_PLTGOT=3,
    DT_PLTPAD=1879047933,
    DT_PLTPADSZ=1879047673,
    DT_PLTREL=20,
    DT_PLTRELSZ=2,
    DT_POSFLAG_1=1879047677,
    DT_PREINIT_ARRAYSZ=33,
    DT_REL=17,
    DT_RELA=7,
    DT_RELACOUNT=1879048185,
    DT_RELAENT=9,
    DT_RELASZ=8,
    DT_RELCOUNT=1879048186,
    DT_RELENT=19,
    DT_RELSZ=18,
    DT_RPATH=15,
    DT_RUNPATH=29,
    DT_SONAME=14,
    DT_STRSZ=10,
    DT_STRTAB=5,
    DT_SYMBOLIC=16,
    DT_SYMENT=11,
    DT_SYMINENT=1879047679,
    DT_SYMINFO=1879047935,
    DT_SYMINSZ=1879047678,
    DT_SYMTAB=6,
    DT_TEXTREL=22,
    DT_TLSDESC_GOT=1879047927,
    DT_TLSDESC_PLT=1879047926,
    DT_VERDEF=1879048188,
    DT_VERDEFNUM=1879048189,
    DT_VERNEED=1879048190,
    DT_VERNEEDNUM=1879048191,
    DT_VERSYM=1879048176
} Elf64_DynTag;

typedef enum Elf_ProgramHeaderType {
    PT_DYNAMIC=2,
    PT_GNU_EH_FRAME=1685382480,
    PT_GNU_RELRO=1685382482,
    PT_GNU_STACK=1685382481,
    PT_INTERP=3,
    PT_LOAD=1,
    PT_NOTE=4,
    PT_NULL=0,
    PT_PHDR=6,
    PT_SHLIB=5,
    PT_TLS=7
} Elf_ProgramHeaderType;

typedef struct Elf64_Dyn Elf64_Dyn, *PElf64_Dyn;

struct Elf64_Dyn {
    enum Elf64_DynTag d_tag;
    qword d_val;
};

typedef struct Elf64_Shdr Elf64_Shdr, *PElf64_Shdr;

typedef enum Elf_SectionHeaderType {
    SHT_CHECKSUM=1879048184,
    SHT_DYNAMIC=6,
    SHT_DYNSYM=11,
    SHT_FINI_ARRAY=15,
    SHT_GNU_ATTRIBUTES=1879048181,
    SHT_GNU_HASH=1879048182,
    SHT_GNU_LIBLIST=1879048183,
    SHT_GNU_verdef=1879048189,
    SHT_GNU_verneed=1879048190,
    SHT_GNU_versym=1879048191,
    SHT_GROUP=17,
    SHT_HASH=5,
    SHT_INIT_ARRAY=14,
    SHT_NOBITS=8,
    SHT_NOTE=7,
    SHT_NULL=0,
    SHT_PREINIT_ARRAY=16,
    SHT_PROGBITS=1,
    SHT_REL=9,
    SHT_RELA=4,
    SHT_SHLIB=10,
    SHT_STRTAB=3,
    SHT_SUNW_COMDAT=1879048187,
    SHT_SUNW_move=1879048186,
    SHT_SUNW_syminfo=1879048188,
    SHT_SYMTAB=2,
    SHT_SYMTAB_SHNDX=18
} Elf_SectionHeaderType;

struct Elf64_Shdr {
    dword sh_name;
    enum Elf_SectionHeaderType sh_type;
    qword sh_flags;
    qword sh_addr;
    qword sh_offset;
    qword sh_size;
    dword sh_link;
    dword sh_info;
    qword sh_addralign;
    qword sh_entsize;
};

typedef struct Elf64_Phdr Elf64_Phdr, *PElf64_Phdr;

struct Elf64_Phdr {
    enum Elf_ProgramHeaderType p_type;
    dword p_flags;
    qword p_offset;
    qword p_vaddr;
    qword p_paddr;
    qword p_filesz;
    qword p_memsz;
    qword p_align;
};

typedef struct Elf64_Rela Elf64_Rela, *PElf64_Rela;

struct Elf64_Rela {
    qword r_offset; // location to apply the relocation action
    qword r_info; // the symbol table index and the type of relocation
    qword r_addend; // a constant addend used to compute the relocatable field value
};

typedef struct Elf64_Sym Elf64_Sym, *PElf64_Sym;

struct Elf64_Sym {
    dword st_name;
    byte st_info;
    byte st_other;
    word st_shndx;
    qword st_value;
    qword st_size;
};

typedef struct Elf64_Ehdr Elf64_Ehdr, *PElf64_Ehdr;

struct Elf64_Ehdr {
    byte e_ident_magic_num;
    char e_ident_magic_str[3];
    byte e_ident_class;
    byte e_ident_data;
    byte e_ident_version;
    byte e_ident_pad[9];
    word e_type;
    word e_machine;
    dword e_version;
    qword e_entry;
    qword e_phoff;
    qword e_shoff;
    dword e_flags;
    word e_ehsize;
    word e_phentsize;
    word e_phnum;
    word e_shentsize;
    word e_shnum;
    word e_shstrndx;
};




undefined8 _DT_INIT(void);
void FUN_00401c10(void);
void thunk_FUN_00620130(void);
void thunk_FUN_006201c8(void);
void FUN_00402220(void);
void thunk_FUN_00620300(void);
void entry(void);
void FUN_00402280(void);
void FUN_00402300(void);
void FUN_00402380(void);
void FUN_004023be(undefined *puParm1);
undefined8 FUN_0040255a(undefined8 uParm1);
ulong FUN_00402568(uint uParm1);
ulong FUN_004026e9(uint uParm1,undefined8 *puParm2);
void FUN_00402c7d(undefined4 *puParm1);
void FUN_00402ccd(uint uParm1);
void FUN_00402d12(uint uParm1);
undefined8 FUN_00402d57(long lParm1,long lParm2);
void FUN_00402e14(undefined8 uParm1,int iParm2,undefined8 uParm3,undefined8 uParm4,long lParm5,long lParm6);
ulong FUN_00402fd3(uint param_1,undefined8 param_2,long *param_3,uint param_4,uint param_5,int param_6,int param_7);
ulong FUN_004031ed(long param_1,long param_2,uint param_3,uint param_4,uint param_5,uint param_6,int *param_7);
ulong FUN_00403b0c(undefined8 param_1,uint param_2,uint param_3,uint param_4,uint param_5,uint param_6,int *param_7);
void FUN_00403c4f(void);
void FUN_00403d34(uint uParm1,undefined8 uParm2,uint uParm3,uint uParm4);
void FUN_00403d65(uint uParm1,undefined8 uParm2,uint uParm3,uint uParm4);
char * FUN_00403d96(ulong uParm1,long lParm2);
void FUN_00403e1b(long lParm1);
ulong FUN_00403ef8(undefined1 *puParm1,byte bParm2,uint uParm3);
undefined8 * FUN_00403f80(undefined8 *puParm1,int iParm2);
char * FUN_00403ff7(char *pcParm1,int iParm2);
ulong FUN_00404097(long param_1,ulong param_2,long param_3,ulong param_4,uint param_5,uint param_6,long param_7,char *param_8,char *param_9);
undefined1 * FUN_00404f61(int iParm1,undefined8 uParm2,undefined8 uParm3,uint *puParm4);
void FUN_004051d4(uint uParm1,uint uParm2,undefined8 uParm3);
void FUN_00405215(uint uParm1,undefined8 uParm2);
void FUN_00405239(undefined8 uParm1,undefined8 uParm2,char cParm3);
void FUN_004052cd(undefined8 uParm1,char cParm2);
void FUN_004052f7(undefined8 uParm1);
void FUN_00405316(uint uParm1,uint uParm2,undefined8 uParm3);
void FUN_004053b1(uint uParm1,undefined8 uParm2,undefined8 uParm3);
void FUN_004053dd(uint uParm1,undefined8 uParm2);
void FUN_00405406(undefined8 uParm1);
undefined8 * FUN_00405425(undefined8 *puParm1);
undefined *FUN_00405482(char *pcParm1,long lParm2,int *piParm3,uint *puParm4,char **ppcParm5,undefined8 *puParm6);
long FUN_0040583b(undefined8 uParm1,undefined8 uParm2,long lParm3,undefined8 uParm4,undefined8 uParm5);
void FUN_0040590e(undefined8 uParm1,long lParm2,undefined8 uParm3,undefined8 uParm4,undefined8 *puParm5,undefined8 uParm6);
void FUN_00405d72(undefined8 uParm1,undefined8 uParm2,undefined8 uParm3,undefined8 uParm4,uint *puParm5);
void FUN_00405e44(undefined4 uParm1,undefined4 uParm2,undefined4 uParm3,undefined4 uParm4,undefined4 uParm5,undefined4 uParm6,undefined4 uParm7,undefined4 uParm8,undefined8 param_1,undefined8 param_2,undefined8 param_3,undefined8 param_4,undefined8 param_5,undefined8 param_6);
void FUN_00405efa(undefined8 uParm1);
long FUN_00405f14(long lParm1);
long FUN_00405f49(long lParm1,long lParm2);
void FUN_00405faa(undefined8 uParm1,undefined8 uParm2);
void FUN_00405fde(undefined8 uParm1);
void FUN_0040600b(void);
long FUN_00406035(undefined8 uParm1,uint uParm2,undefined8 uParm3);
ulong FUN_004060a0(long lParm1,long lParm2);
undefined8 FUN_00406102(int iParm1);
ulong FUN_00406128(ulong *puParm1,int iParm2);
ulong FUN_00406187(undefined8 uParm1,uint uParm2,int iParm3);
ulong FUN_004061c8(byte *pbParm1,byte **ppbParm2,uint uParm3,undefined8 *puParm4,long lParm5);
undefined8 FUN_004065a9(ulong uParm1,ulong uParm2);
ulong FUN_00406628(uint uParm1);
void FUN_00406651(void);
void FUN_00406685(uint uParm1);
void FUN_004066f9(uint uParm1,uint uParm2,undefined8 uParm3,undefined8 uParm4);
void FUN_0040677d(undefined4 uParm1,undefined4 uParm2,undefined4 uParm3,undefined4 uParm4,undefined4 uParm5,undefined4 uParm6,undefined4 uParm7,undefined4 uParm8,uint param_1,uint param_2,undefined8 param_3,undefined8 param_4,undefined8 param_5,undefined8 param_6);
ulong FUN_00406861(uint uParm1,char *pcParm2,uint uParm3,uint uParm4,uint uParm5);
void FUN_00406b26(uint uParm1,undefined8 uParm2,undefined8 uParm3,uint uParm4);
void FUN_00406b53(uint uParm1,undefined8 uParm2,undefined8 uParm3,uint uParm4);
ulong FUN_00406b90(uint uParm1,long lParm2,long lParm3,uint uParm4);
ulong FUN_00406ca3(long *plParm1,long *plParm2);
ulong FUN_00406cf8(long lParm1,ulong uParm2);
undefined8 FUN_00406d22(long lParm1);
undefined8 FUN_00406dbe(long lParm1,undefined8 *puParm2);
void FUN_00406ece(long lParm1,long lParm2);
void FUN_00406fdb(long lParm1);
void FUN_00407028(undefined8 uParm1);
void FUN_0040706a(long lParm1,char cParm2);
long FUN_004070ad(uint uParm1,undefined8 uParm2,uint uParm3,uint *puParm4);
void FUN_0040713e(long lParm1,uint uParm2,char cParm3);
ulong FUN_004071cb(long lParm1);
ulong FUN_00407275(long lParm1,undefined8 uParm2);
long * FUN_0040730d(long *plParm1,uint uParm2,long lParm3);
void FUN_004076cf(long lParm1,long lParm2);
undefined8 FUN_00407789(long *plParm1);
ulong FUN_00407915(ulong *puParm1,ulong uParm2);
ulong FUN_00407946(ulong *puParm1,ulong *puParm2);
undefined8 FUN_00407978(long lParm1);
undefined8 FUN_00407af2(undefined8 uParm1);
undefined8 FUN_00407b28(undefined8 uParm1);
long FUN_00407b8c(long *plParm1);
undefined8 FUN_00408270(undefined8 uParm1,long lParm2,int iParm3);
ulong FUN_004082c7(long *plParm1,long *plParm2);
void FUN_00408322(long lParm1,undefined4 uParm2);
long FUN_00408392(long *plParm1,int iParm2);
undefined8 FUN_00408cdc(long lParm1,long lParm2,char cParm3);
long FUN_00408ebb(long lParm1,long lParm2,ulong uParm3);
long FUN_00409002(long lParm1,undefined8 uParm2,long lParm3);
void FUN_004090b4(long lParm1);
undefined8 FUN_004090ef(long lParm1,long lParm2);
void FUN_004091bc(long lParm1,long lParm2);
long FUN_004092cf(long *plParm1);
ulong FUN_00409325(long lParm1,long lParm2,uint uParm3,long lParm4);
void FUN_0040955b(long lParm1,int *piParm2);
ulong FUN_0040973f(int param_1,undefined8 *param_2,char *param_3,long *param_4,int *param_5,int param_6,int *param_7,int param_8,undefined8 param_9);
char * FUN_00409d29(undefined8 uParm1,undefined8 uParm2,char *pcParm3,int *piParm4,int iParm5);
ulong FUN_00409df7(uint param_1,undefined8 *param_2,char *param_3,long param_4,undefined8 param_5,uint param_6,uint *param_7,uint param_8);
ulong FUN_0040a5c0(uint param_1,undefined8 param_2,undefined8 param_3,undefined8 param_4,undefined8 param_5,uint param_6,uint param_7);
void FUN_0040a650(uint uParm1,undefined8 uParm2,undefined8 uParm3,undefined8 uParm4,undefined8 uParm5);
ulong FUN_0040a69a(long lParm1,uint uParm2,uint uParm3);
void FUN_0040a807(undefined8 uParm1,undefined8 uParm2);
ulong FUN_0040a82c(long lParm1,long lParm2);
undefined8 FUN_0040a8e3(long lParm1);
ulong FUN_0040a914(uint *puParm1,byte *pbParm2,long lParm3,undefined8 uParm4);
void FUN_0040a997(uint uParm1,undefined8 uParm2,uint uParm3,uint uParm4);
ulong FUN_0040a9c7(undefined4 uParm1,undefined4 uParm2,undefined4 uParm3,undefined4 uParm4,undefined4 uParm5,undefined4 uParm6,undefined4 uParm7,undefined4 uParm8,uint param_1,undefined8 param_2,uint param_3,undefined8 param_4,undefined8 param_5,undefined8 param_6);
undefined * FUN_0040ab8e(undefined *puParm1,uint uParm2,char *pcParm3);
undefined8 FUN_0040accc(long lParm1,long lParm2);
void FUN_0040ad35(undefined8 uParm1,undefined8 uParm2);
ulong FUN_0040ad5a(long lParm1,long lParm2);
ulong FUN_0040adea(undefined8 uParm1,undefined8 uParm2,undefined8 uParm3);
ulong FUN_0040af0b(byte *pbParm1,byte *pbParm2);
undefined8 FUN_0040af82(uint uParm1,char cParm2);
undefined8 FUN_0040affd(undefined8 uParm1);
ulong FUN_0040b088(ulong uParm1);
void FUN_0040b0a4(long lParm1);
undefined8 FUN_0040b0c5(long *plParm1,long *plParm2);
void FUN_0040b19c(undefined4 uParm1,undefined4 uParm2,undefined4 uParm3,undefined4 uParm4,undefined4 uParm5,undefined4 uParm6,undefined4 uParm7,undefined4 uParm8,undefined8 param_1,uint param_2,undefined8 param_3,undefined8 param_4,undefined8 param_5,undefined8 param_6);
undefined8 FUN_0040b2ab(void);
ulong FUN_0040b2b8(uint uParm1);
long FUN_0040b389(long *plParm1,undefined8 uParm2);
long FUN_0040b3e0(long lParm1,long lParm2);
ulong FUN_0040b473(ulong uParm1);
ulong FUN_0040b4df(ulong uParm1);
ulong FUN_0040b526(undefined8 uParm1,ulong uParm2);
ulong FUN_0040b55d(ulong uParm1,ulong uParm2);
undefined8 FUN_0040b576(long lParm1);
ulong FUN_0040b66f(ulong uParm1,long lParm2);
long * FUN_0040b765(undefined8 uParm1,undefined1 *puParm2,code *pcParm3,code *pcParm4,long lParm5);
void FUN_0040b8cd(long **pplParm1,undefined8 uParm2);
long FUN_0040b9f7(long lParm1);
void FUN_0040ba42(long lParm1,undefined8 *puParm2);
long FUN_0040ba77(long lParm1,long lParm2,long **pplParm3,char cParm4);
undefined8 FUN_0040bc0c(long lParm1,long **pplParm2,char cParm3);
undefined8 FUN_0040bdda(long *plParm1,undefined8 uParm2);
undefined8 FUN_0040bfde(long lParm1,long lParm2,long *plParm3);
undefined8 FUN_0040c330(undefined8 uParm1,undefined8 uParm2);
long FUN_0040c379(long lParm1,undefined8 uParm2);
void FUN_0040c658(long lParm1,undefined4 uParm2);
ulong FUN_0040c6b0(long lParm1);
ulong FUN_0040c6c2(long lParm1,undefined4 uParm2);
ulong FUN_0040c74a(long lParm1);
undefined1 * FUN_0040c7cc(void);
char * FUN_0040cb7f(void);
void FUN_0040cc4d(uint uParm1);
void FUN_0040cc79(uint uParm1);
void FUN_0040cca5(undefined4 uParm1,undefined4 uParm2,undefined4 uParm3,undefined4 uParm4,undefined4 uParm5,undefined4 uParm6,undefined4 uParm7,undefined4 uParm8,uint param_1,undefined8 param_2,uint param_3,undefined8 param_4,undefined8 param_5,undefined8 param_6);
undefined8 FUN_0040cdb6(int *piParm1);
void FUN_0040ce30(uint *puParm1);
void FUN_0040ce67(uint *puParm1);
ulong FUN_0040ce9c(uint uParm1);
ulong FUN_0040ceac(uint uParm1);
void FUN_0040cefc(undefined4 *puParm1);
void FUN_0040cf10(uint *puParm1);
void FUN_0040cf2b(uint *puParm1);
undefined8 FUN_0040cf7f(uint *puParm1,undefined8 uParm2);
long FUN_0040cfd9(long lParm1);
ulong FUN_0040d007(char *pcParm1);
ulong FUN_0040d30e(long lParm1,uint uParm2,uint uParm3);
ulong FUN_0040d4b7(undefined8 uParm1);
ulong FUN_0040d56f(undefined4 uParm1,undefined4 uParm2,undefined4 uParm3,undefined4 uParm4,undefined4 uParm5,undefined4 uParm6,undefined4 uParm7,undefined4 uParm8,uint param_1,uint param_2,undefined8 param_3,undefined8 param_4,undefined8 param_5,undefined8 param_6);
undefined8 FUN_0040d826(uint uParm1);
void FUN_0040d889(undefined8 uParm1);
void FUN_0040d8ad(void);
ulong FUN_0040d8bb(long lParm1);
undefined8 FUN_0040d98b(undefined8 uParm1);
void FUN_0040d9aa(undefined8 uParm1,undefined8 uParm2,uint uParm3);
long FUN_0040d9d5(long lParm1,undefined *puParm2);
ulong * FUN_0040df5e(ulong *puParm1,char cParm2,ulong uParm3);
ulong FUN_0040e070(void);
long FUN_0040e0bd(ulong uParm1,long lParm2,ulong uParm3,long lParm4,long *plParm5);
int * FUN_0040e309(ulong uParm1,uint *puParm2,ulong uParm3,uint *puParm4,ulong *puParm5);
char * FUN_0040ede9(ulong uParm1,long lParm2,long lParm3);
long FUN_0040f057(int *param_1,long *param_2);
long FUN_0040f242(undefined auParm1 [16],int *piParm2,long *plParm3);
undefined8 FUN_0040f601(int iParm1,long lParm2,uint *puParm3,long lParm4,uint uParm5);
void FUN_0040fe14(uint param_1);
void FUN_0040fe5e(undefined8 uParm1,uint uParm2);
ulong FUN_0040feb0(void);
ulong FUN_00410242(undefined auParm1 [16],undefined8 uParm2,undefined8 uParm3);
ulong FUN_0041061a(char *pcParm1,long lParm2);
undefined8 FUN_00410673(long param_1,long param_2,undefined param_3,int param_4,undefined8 param_5,ulong param_6,int param_7,ulong param_8,int param_9);
ulong FUN_00417da0(ulong uParm1,undefined4 uParm2);
ulong FUN_00417dbc(uint uParm1);
void FUN_00417ddb(undefined8 uParm1,int *piParm2,undefined8 uParm3);
void FUN_00417e7c(int *param_1);
void FUN_00417f3b(uint uParm1);
ulong FUN_00417f61(ulong uParm1,long lParm2);
void FUN_00417f95(undefined8 uParm1,undefined8 uParm2,undefined8 uParm3);
void FUN_00417fd0(undefined8 uParm1,undefined8 uParm2,undefined8 uParm3,undefined8 uParm4);
ulong FUN_00418021(ulong uParm1,ulong uParm2);
undefined8 FUN_0041803c(uint *puParm1,ulong *puParm2);
undefined8 FUN_004187dc(char *pcParm1,ulong *puParm2,char **ppcParm3);
ulong FUN_00419a92(undefined auParm1 [16]);
ulong FUN_00419acd(void);
void FUN_00419b00(void);
undefined8 _DT_FINI(void);
undefined FUN_00620000();
undefined FUN_00620008();
undefined FUN_00620010();
undefined FUN_00620018();
undefined FUN_00620020();
undefined FUN_00620028();
undefined FUN_00620030();
undefined FUN_00620038();
undefined FUN_00620040();
undefined FUN_00620048();
undefined FUN_00620050();
undefined FUN_00620058();
undefined FUN_00620060();
undefined FUN_00620068();
undefined FUN_00620070();
undefined FUN_00620078();
undefined FUN_00620080();
undefined FUN_00620088();
undefined FUN_00620090();
undefined FUN_00620098();
undefined FUN_006200a0();
undefined FUN_006200a8();
undefined FUN_006200b0();
undefined FUN_006200b8();
undefined FUN_006200c0();
undefined FUN_006200c8();
undefined FUN_006200d0();
undefined FUN_006200d8();
undefined FUN_006200e0();
undefined FUN_006200e8();
undefined FUN_006200f0();
undefined FUN_006200f8();
undefined FUN_00620100();
undefined FUN_00620108();
undefined FUN_00620110();
undefined FUN_00620118();
undefined FUN_00620120();
undefined FUN_00620128();
undefined FUN_00620130();
undefined FUN_00620138();
undefined FUN_00620140();
undefined FUN_00620148();
undefined FUN_00620150();
undefined FUN_00620158();
undefined FUN_00620160();
undefined FUN_00620168();
undefined FUN_00620170();
undefined FUN_00620178();
undefined FUN_00620180();
undefined FUN_00620188();
undefined FUN_00620190();
undefined FUN_00620198();
undefined FUN_006201a0();
undefined FUN_006201a8();
undefined FUN_006201b0();
undefined FUN_006201b8();
undefined FUN_006201c0();
undefined FUN_006201c8();
undefined FUN_006201d0();
undefined FUN_006201d8();
undefined FUN_006201e0();
undefined FUN_006201e8();
undefined FUN_006201f0();
undefined FUN_006201f8();
undefined FUN_00620200();
undefined FUN_00620208();
undefined FUN_00620210();
undefined FUN_00620218();
undefined FUN_00620220();
undefined FUN_00620228();
undefined FUN_00620230();
undefined FUN_00620238();
undefined FUN_00620240();
undefined FUN_00620248();
undefined FUN_00620250();
undefined FUN_00620258();
undefined FUN_00620260();
undefined FUN_00620268();
undefined FUN_00620270();
undefined FUN_00620278();
undefined FUN_00620280();
undefined FUN_00620288();
undefined FUN_00620290();
undefined FUN_00620298();
undefined FUN_006202a0();
undefined FUN_006202a8();
undefined FUN_006202b0();
undefined FUN_006202b8();
undefined FUN_006202c0();
undefined FUN_006202c8();
undefined FUN_006202d0();
undefined FUN_006202d8();
undefined FUN_006202e0();
undefined FUN_006202e8();
undefined FUN_006202f0();
undefined FUN_006202f8();
undefined FUN_00620300();


typedef unsigned char   undefined;

typedef unsigned char    byte;
typedef unsigned char    dwfenc;
typedef unsigned int    dword;
typedef unsigned long    qword;
typedef unsigned char    undefined1;
typedef unsigned short    undefined2;
typedef unsigned int    undefined3;
typedef unsigned int    undefined4;
typedef unsigned long    undefined6;
typedef unsigned long    undefined7;
typedef unsigned long    undefined8;
typedef unsigned short    word;
#define unkbyte9   unsigned long long
#define unkbyte10   unsigned long long
#define unkbyte11   unsigned long long
#define unkbyte12   unsigned long long
#define unkbyte13   unsigned long long
#define unkbyte14   unsigned long long
#define unkbyte15   unsigned long long
#define unkbyte16   unsigned long long

#define unkuint9   unsigned long long
#define unkuint10   unsigned long long
#define unkuint11   unsigned long long
#define unkuint12   unsigned long long
#define unkuint13   unsigned long long
#define unkuint14   unsigned long long
#define unkuint15   unsigned long long
#define unkuint16   unsigned long long

#define unkint9   long long
#define unkint10   long long
#define unkint11   long long
#define unkint12   long long
#define unkint13   long long
#define unkint14   long long
#define unkint15   long long
#define unkint16   long long

#define unkfloat1   float
#define unkfloat2   float
#define unkfloat3   float
#define unkfloat5   double
#define unkfloat6   double
#define unkfloat7   double
#define unkfloat9   long double
#define unkfloat11   long double
#define unkfloat12   long double
#define unkfloat13   long double
#define unkfloat14   long double
#define unkfloat15   long double
#define unkfloat16   long double

#define BADSPACEBASE   void
#define code   void

#define ulong long
#define true 1
#define bool int
#define false 0
#define float10 int
#define ushort int
#define undefined7 long
#define undefined2 long
#define uint5 long
#define uint6 long

// Add definition for uint
typedef unsigned int    uint;

#define sbyte char

extern imperfection_wrapper(void);
typedef struct eh_frame_hdr eh_frame_hdr, *Peh_frame_hdr;

struct eh_frame_hdr {
    byte eh_frame_hdr_version; // Exception Handler Frame Header Version
    dwfenc eh_frame_pointer_encoding; // Exception Handler Frame Pointer Encoding
    dwfenc eh_frame_desc_entry_count_encoding; // Encoding of # of Exception Handler FDEs
    dwfenc eh_frame_table_encoding; // Exception Handler Table Encoding
};

typedef struct fde_table_entry fde_table_entry, *Pfde_table_entry;

struct fde_table_entry {
    dword initial_loc; // Initial Location
    dword data_loc; // Data location
};

typedef enum Elf64_DynTag {
    DT_AUDIT=1879047932,
    DT_AUXILIARY=2147483645,
    DT_BIND_NOW=24,
    DT_CHECKSUM=1879047672,
    DT_CONFIG=1879047930,
    DT_DEBUG=21,
    DT_DEPAUDIT=1879047931,
    DT_ENCODING=32,
    DT_FEATURE_1=1879047676,
    DT_FILTER=2147483647,
    DT_FINI=13,
    DT_FINI_ARRAY=26,
    DT_FINI_ARRAYSZ=28,
    DT_FLAGS=30,
    DT_FLAGS_1=1879048187,
    DT_GNU_CONFLICT=1879047928,
    DT_GNU_CONFLICTSZ=1879047670,
    DT_GNU_HASH=1879047925,
    DT_GNU_LIBLIST=1879047929,
    DT_GNU_LIBLISTSZ=1879047671,
    DT_GNU_PRELINKED=1879047669,
    DT_HASH=4,
    DT_INIT=12,
    DT_INIT_ARRAY=25,
    DT_INIT_ARRAYSZ=27,
    DT_JMPREL=23,
    DT_MOVEENT=1879047674,
    DT_MOVESZ=1879047675,
    DT_MOVETAB=1879047934,
    DT_NEEDED=1,
    DT_NULL=0,
    DT_PLTGOT=3,
    DT_PLTPAD=1879047933,
    DT_PLTPADSZ=1879047673,
    DT_PLTREL=20,
    DT_PLTRELSZ=2,
    DT_POSFLAG_1=1879047677,
    DT_PREINIT_ARRAYSZ=33,
    DT_REL=17,
    DT_RELA=7,
    DT_RELACOUNT=1879048185,
    DT_RELAENT=9,
    DT_RELASZ=8,
    DT_RELCOUNT=1879048186,
    DT_RELENT=19,
    DT_RELSZ=18,
    DT_RPATH=15,
    DT_RUNPATH=29,
    DT_SONAME=14,
    DT_STRSZ=10,
    DT_STRTAB=5,
    DT_SYMBOLIC=16,
    DT_SYMENT=11,
    DT_SYMINENT=1879047679,
    DT_SYMINFO=1879047935,
    DT_SYMINSZ=1879047678,
    DT_SYMTAB=6,
    DT_TEXTREL=22,
    DT_TLSDESC_GOT=1879047927,
    DT_TLSDESC_PLT=1879047926,
    DT_VERDEF=1879048188,
    DT_VERDEFNUM=1879048189,
    DT_VERNEED=1879048190,
    DT_VERNEEDNUM=1879048191,
    DT_VERSYM=1879048176
} Elf64_DynTag;

typedef enum Elf_ProgramHeaderType {
    PT_DYNAMIC=2,
    PT_GNU_EH_FRAME=1685382480,
    PT_GNU_RELRO=1685382482,
    PT_GNU_STACK=1685382481,
    PT_INTERP=3,
    PT_LOAD=1,
    PT_NOTE=4,
    PT_NULL=0,
    PT_PHDR=6,
    PT_SHLIB=5,
    PT_TLS=7
} Elf_ProgramHeaderType;

typedef struct Elf64_Dyn Elf64_Dyn, *PElf64_Dyn;

struct Elf64_Dyn {
    enum Elf64_DynTag d_tag;
    qword d_val;
};

typedef struct Elf64_Shdr Elf64_Shdr, *PElf64_Shdr;

typedef enum Elf_SectionHeaderType {
    SHT_CHECKSUM=1879048184,
    SHT_DYNAMIC=6,
    SHT_DYNSYM=11,
    SHT_FINI_ARRAY=15,
    SHT_GNU_ATTRIBUTES=1879048181,
    SHT_GNU_HASH=1879048182,
    SHT_GNU_LIBLIST=1879048183,
    SHT_GNU_verdef=1879048189,
    SHT_GNU_verneed=1879048190,
    SHT_GNU_versym=1879048191,
    SHT_GROUP=17,
    SHT_HASH=5,
    SHT_INIT_ARRAY=14,
    SHT_NOBITS=8,
    SHT_NOTE=7,
    SHT_NULL=0,
    SHT_PREINIT_ARRAY=16,
    SHT_PROGBITS=1,
    SHT_REL=9,
    SHT_RELA=4,
    SHT_SHLIB=10,
    SHT_STRTAB=3,
    SHT_SUNW_COMDAT=1879048187,
    SHT_SUNW_move=1879048186,
    SHT_SUNW_syminfo=1879048188,
    SHT_SYMTAB=2,
    SHT_SYMTAB_SHNDX=18
} Elf_SectionHeaderType;

struct Elf64_Shdr {
    dword sh_name;
    enum Elf_SectionHeaderType sh_type;
    qword sh_flags;
    qword sh_addr;
    qword sh_offset;
    qword sh_size;
    dword sh_link;
    dword sh_info;
    qword sh_addralign;
    qword sh_entsize;
};

typedef struct Elf64_Phdr Elf64_Phdr, *PElf64_Phdr;

struct Elf64_Phdr {
    enum Elf_ProgramHeaderType p_type;
    dword p_flags;
    qword p_offset;
    qword p_vaddr;
    qword p_paddr;
    qword p_filesz;
    qword p_memsz;
    qword p_align;
};

typedef struct Elf64_Rela Elf64_Rela, *PElf64_Rela;

struct Elf64_Rela {
    qword r_offset; // location to apply the relocation action
    qword r_info; // the symbol table index and the type of relocation
    qword r_addend; // a constant addend used to compute the relocatable field value
};

typedef struct Elf64_Sym Elf64_Sym, *PElf64_Sym;

struct Elf64_Sym {
    dword st_name;
    byte st_info;
    byte st_other;
    word st_shndx;
    qword st_value;
    qword st_size;
};

typedef struct Elf64_Ehdr Elf64_Ehdr, *PElf64_Ehdr;

struct Elf64_Ehdr {
    byte e_ident_magic_num;
    char e_ident_magic_str[3];
    byte e_ident_class;
    byte e_ident_data;
    byte e_ident_version;
    byte e_ident_pad[9];
    word e_type;
    word e_machine;
    dword e_version;
    qword e_entry;
    qword e_phoff;
    qword e_shoff;
    dword e_flags;
    word e_ehsize;
    word e_phentsize;
    word e_phnum;
    word e_shentsize;
    word e_shnum;
    word e_shstrndx;
};




undefined8 _DT_INIT(void);
void FUN_004019f0(void);
void entry(void);
void FUN_00401fd0(void);
void FUN_00402050(void);
void FUN_004020d0(void);
void FUN_0040210e(void);
void FUN_00402128(void);
void FUN_00402142(undefined *puParm1);
ulong FUN_004022de(uint uParm1);
ulong FUN_004023eb(char **ppcParm1,undefined8 *puParm2,undefined8 uParm3);
void FUN_004024f8(void);
void FUN_00402570(void);
void FUN_004025b2(void);
void FUN_004025f4(void);
void FUN_00402636(void);
undefined8 FUN_004027f4(void);
void FUN_004028ed(undefined8 uParm1);
undefined8 FUN_00402949(undefined8 uParm1);
ulong FUN_00402acb(uint uParm1,undefined8 *puParm2);
void FUN_00403165(void);
void FUN_0040324a(uint uParm1,undefined8 uParm2,undefined8 uParm3,uint uParm4);
void FUN_0040327a(long lParm1,uint uParm2);
void FUN_004032b4(undefined8 uParm1);
void FUN_004032d8(undefined8 uParm1,undefined8 uParm2);
long * FUN_00403302(long *plParm1,undefined8 uParm2,char cParm3);
void FUN_00403443(long lParm1);
ulong FUN_00403520(undefined1 *puParm1,byte bParm2,uint uParm3);
undefined8 * FUN_004035a8(undefined8 *puParm1,int iParm2);
char * FUN_0040361f(char *pcParm1,int iParm2);
ulong FUN_004036bf(long param_1,ulong param_2,long param_3,ulong param_4,uint param_5,uint param_6,long param_7,char *param_8,char *param_9);
undefined1 * FUN_00404589(int iParm1,undefined8 uParm2,undefined8 uParm3,uint *puParm4);
void FUN_004047fc(undefined8 uParm1,undefined8 uParm2,char cParm3);
void FUN_00404890(undefined8 uParm1,char cParm2);
void FUN_004048ba(undefined8 uParm1);
void FUN_004048d9(uint uParm1,uint uParm2,undefined8 uParm3);
void FUN_00404974(uint uParm1,undefined8 uParm2,undefined8 uParm3);
void FUN_004049a0(uint uParm1,undefined8 uParm2);
void FUN_004049c9(undefined8 uParm1);
void FUN_004049e8(undefined8 uParm1,long lParm2,undefined8 uParm3,undefined8 uParm4,undefined8 *puParm5,undefined8 uParm6);
void FUN_00404e4c(undefined8 uParm1,undefined8 uParm2,undefined8 uParm3,undefined8 uParm4,uint *puParm5);
void FUN_00404f1e(undefined4 uParm1,undefined4 uParm2,undefined4 uParm3,undefined4 uParm4,undefined4 uParm5,undefined4 uParm6,undefined4 uParm7,undefined4 uParm8,undefined8 param_1,undefined8 param_2,undefined8 param_3,undefined8 param_4,undefined8 param_5,undefined8 param_6);
void FUN_00404fd4(long lParm1,ulong *puParm2,ulong uParm3);
void FUN_0040509f(undefined8 uParm1);
long FUN_004050b9(long lParm1);
long FUN_004050ee(long lParm1,long lParm2);
void FUN_0040514f(undefined8 uParm1,undefined8 uParm2);
long FUN_00405179(void);
long FUN_004051a3(undefined8 param_1,uint param_2,long param_3,long param_4,undefined8 param_5,undefined8 param_6,uint param_7);
void FUN_004052ce(undefined8 uParm1,undefined8 uParm2,undefined8 uParm3,undefined8 uParm4,undefined8 uParm5,uint uParm6);
undefined8 FUN_00405323(long *plParm1,int iParm2);
ulong FUN_004053c1(undefined8 uParm1,uint uParm2,int iParm3);
ulong FUN_00405402(char *pcParm1,char **ppcParm2,uint uParm3,undefined8 *puParm4,long lParm5);
undefined8 FUN_004057a1(ulong uParm1,ulong uParm2);
ulong FUN_00405820(uint uParm1);
void FUN_00405849(void);
void FUN_0040587d(uint uParm1);
void FUN_004058f1(uint uParm1,uint uParm2,undefined8 uParm3,undefined8 uParm4);
void FUN_00405975(undefined4 uParm1,undefined4 uParm2,undefined4 uParm3,undefined4 uParm4,undefined4 uParm5,undefined4 uParm6,undefined4 uParm7,undefined4 uParm8,uint param_1,uint param_2,undefined8 param_3,undefined8 param_4,undefined8 param_5,undefined8 param_6);
ulong FUN_00405a59(undefined8 uParm1);
void FUN_00405b11(undefined8 uParm1);
void FUN_00405b35(void);
ulong FUN_00405b43(long lParm1);
undefined8 FUN_00405c13(undefined8 uParm1);
void FUN_00405c32(undefined8 uParm1,undefined8 uParm2,uint uParm3);
void FUN_00405c5d(long lParm1,int *piParm2);
ulong FUN_00405e41(int param_1,undefined8 *param_2,char *param_3,long *param_4,int *param_5,int param_6,int *param_7,int param_8,undefined8 param_9);
char * FUN_0040642b(undefined8 uParm1,undefined8 uParm2,char *pcParm3,int *piParm4,int iParm5);
ulong FUN_004064f9(uint param_1,undefined8 *param_2,char *param_3,long param_4,undefined8 param_5,uint param_6,uint *param_7,uint param_8);
ulong FUN_00406cc2(uint param_1,undefined8 param_2,undefined8 param_3,undefined8 param_4,undefined8 param_5,uint param_6,uint param_7);
void FUN_00406d52(uint uParm1,undefined8 uParm2,undefined8 uParm3,undefined8 uParm4,undefined8 uParm5);
undefined8 FUN_00406d9c(long lParm1);
ulong FUN_00406dcd(uint *puParm1,byte *pbParm2,long lParm3,undefined8 uParm4);
undefined8 FUN_00406e50(long lParm1,long lParm2);
undefined8 FUN_00406eb9(int iParm1);
void FUN_00406edf(long lParm1,long lParm2);
void FUN_00406f4b(long lParm1,long lParm2);
ulong FUN_00406fba(long lParm1,long lParm2);
void FUN_00407011(undefined8 uParm1);
void FUN_00407035(undefined8 uParm1);
void FUN_00407059(undefined8 uParm1,undefined8 uParm2);
void FUN_00407083(long lParm1);
void FUN_004070d2(long lParm1,long lParm2);
void FUN_0040713d(long lParm1,long lParm2);
ulong FUN_004071a8(long lParm1,long lParm2);
ulong FUN_0040721b(long lParm1,long lParm2);
undefined8 FUN_00407264(void);
ulong FUN_00407277(long param_1,undefined8 param_2,long param_3,long param_4,undefined8 param_5,byte param_6,long param_7);
ulong FUN_004073c2(long lParm1,undefined8 uParm2,long lParm3,long lParm4,byte bParm5,long lParm6);
undefined8 FUN_00407596(long lParm1,ulong uParm2);
void FUN_0040770a(undefined8 uParm1,undefined8 uParm2,undefined8 *puParm3,long lParm4,char cParm5,long lParm6);
void FUN_004077fa(long *plParm1);
undefined8 FUN_00407b25(long *plParm1);
long FUN_00408601(long *plParm1,long lParm2,uint *puParm3);
void FUN_00408734(long *plParm1);
void FUN_00408805(long *plParm1);
ulong FUN_004088a5(byte **ppbParm1,byte *pbParm2,uint uParm3);
ulong FUN_0040958b(long *plParm1,long lParm2);
ulong FUN_0040970d(long *plParm1);
void FUN_00409897(long lParm1);
ulong FUN_004098e4(long lParm1,ulong uParm2,uint uParm3);
undefined8 FUN_00409a6a(long *plParm1,long lParm2);
undefined8 FUN_00409ad7(undefined8 *puParm1,undefined8 uParm2);
undefined8 FUN_00409b61(undefined8 *puParm1,long lParm2,long lParm3);
undefined8 FUN_00409c3f(long *plParm1,long lParm2);
undefined8 FUN_00409d1f(long *plParm1,long lParm2,long lParm3);
undefined8 FUN_0040a100(long *plParm1,long lParm2,long lParm3);
undefined8 FUN_0040a412(long *plParm1,long lParm2);
ulong FUN_0040a7d8(long *plParm1,long lParm2);
undefined8 FUN_0040a9c3(long *plParm1,undefined8 uParm2);
undefined8 FUN_0040aa78(long lParm1,long lParm2);
long FUN_0040ab07(long lParm1,long lParm2);
void FUN_0040abbf(long lParm1,long lParm2);
long FUN_0040ac3d(long *plParm1,undefined8 uParm2,undefined8 uParm3);
long FUN_0040afd5(long lParm1,uint uParm2);
ulong * FUN_0040b02f(undefined4 *puParm1,long lParm2,long lParm3);
ulong * FUN_0040b151(undefined4 *puParm1,long lParm2,long lParm3,uint uParm4);
undefined8 FUN_0040b27e(long *plParm1,ulong *puParm2,ulong uParm3);
void FUN_0040b432(long lParm1);
long FUN_0040b4d5(long *plParm1,long lParm2,undefined8 uParm3);
long FUN_0040b6aa(long *plParm1,long lParm2,uint uParm3,undefined8 uParm4);
char * FUN_0040b996(undefined8 uParm1,undefined8 uParm2,long lParm3);
undefined8 FUN_0040ba22(long *plParm1);
void FUN_0040bb1b(long **pplParm1,long lParm2,long lParm3);
void FUN_0040c1f4(long *plParm1,undefined8 uParm2);
ulong FUN_0040c456(long *plParm1,undefined8 uParm2,undefined8 uParm3,ulong uParm4);
undefined8 FUN_0040c7d0(long *plParm1,ulong uParm2);
void FUN_0040cb5e(long lParm1);
void FUN_0040cce0(long *plParm1);
ulong FUN_0040cd6f(long *plParm1);
void FUN_0040d0be(long *plParm1);
ulong FUN_0040d315(long *plParm1);
ulong FUN_0040d6ab(long **pplParm1,code *pcParm2,undefined8 uParm3);
ulong FUN_0040d789(long *plParm1,code *pcParm2,undefined8 uParm3);
undefined8 FUN_0040d844(long lParm1,long lParm2);
ulong FUN_0040d9bb(undefined8 uParm1,long lParm2);
long FUN_0040da9d(undefined4 *puParm1,long *plParm2,long lParm3);
undefined8 FUN_0040dc8e(long *plParm1,long lParm2);
undefined8 FUN_0040dd80(undefined8 uParm1,long lParm2);
ulong FUN_0040de2f(long lParm1,long lParm2);
ulong FUN_0040e0a6(long *plParm1,long lParm2,long lParm3,long lParm4,uint uParm5);
long FUN_0040e5d6(long *plParm1,long lParm2,uint uParm3);
long FUN_0040e66f(long *plParm1,long lParm2,undefined4 uParm3);
undefined8 FUN_0040e7a5(long lParm1);
ulong FUN_0040e8e3(long lParm1);
ulong FUN_0040e9c3(undefined8 *puParm1,long *plParm2,long lParm3,char cParm4);
void FUN_0040ed95(undefined8 uParm1,long lParm2,undefined8 uParm3);
undefined8 FUN_0040edda(long *plParm1,long lParm2,ulong uParm3);
ulong FUN_0040f5d2(char *pcParm1,long lParm2,ulong uParm3);
long FUN_0040f7e7(undefined8 uParm1,long *plParm2,ulong uParm3,int *piParm4);
long FUN_0040f913(undefined8 uParm1,long *plParm2,long lParm3,ulong uParm4,long lParm5,int *piParm6);
long FUN_0040fb12(undefined8 uParm1,undefined8 *puParm2,long lParm3,undefined8 uParm4,long lParm5,int *piParm6);
long FUN_0040fcce(long lParm1,long *plParm2,undefined8 *puParm3,ulong uParm4,long lParm5,int *piParm6);
long FUN_00410554(undefined8 uParm1,long *plParm2,long lParm3,ulong uParm4,undefined8 uParm5,int *piParm6);
long FUN_004106e8(long lParm1,long lParm2,undefined8 uParm3,undefined8 *puParm4,ulong uParm5,undefined4 *puParm6);
ulong FUN_00410c0c(byte bParm1,long lParm2);
undefined8 FUN_00410c43(ulong uParm1,undefined8 uParm2,long lParm3,long *plParm4,int *piParm5,int *piParm6);
undefined8 FUN_00411004(undefined8 uParm1,undefined8 uParm2,undefined8 uParm3,byte *pbParm4);
long FUN_00411063(long lParm1,long lParm2,long lParm3,ulong uParm4,int *piParm5);
undefined8 FUN_00411999(undefined4 *param_1,long param_2,undefined *param_3,int param_4,undefined8 param_5,undefined8 param_6,char param_7);
undefined8 FUN_00411ae4(undefined4 *puParm1,long lParm2,char *pcParm3);
undefined8 FUN_00411c4b(undefined8 uParm1,undefined8 uParm2,undefined8 uParm3,byte *pbParm4);
undefined8 FUN_00411ca5(long lParm1,undefined8 uParm2,long lParm3,long *plParm4,char *pcParm5,ulong uParm6);
long FUN_0041260c(long lParm1,undefined8 uParm2,undefined8 uParm3,char *pcParm4,byte bParm5,int *piParm6);
long FUN_004128a7(undefined8 uParm1,byte *pbParm2,undefined8 uParm3);
void FUN_0041298e(undefined8 *puParm1);
void FUN_004129c7(undefined8 uParm1,undefined8 uParm2,undefined8 uParm3,undefined uParm4);
undefined8 * FUN_004129fe(long lParm1,undefined8 *puParm2,undefined8 *puParm3,undefined8 *puParm4);
undefined8 FUN_00412b4a(long lParm1,long lParm2);
void FUN_00412b8d(undefined8 *puParm1);
undefined8 FUN_00412bf1(undefined8 uParm1,long lParm2);
long * FUN_00412c18(long **pplParm1,undefined8 uParm2);
void FUN_00412d44(undefined8 uParm1,undefined8 uParm2,undefined8 uParm3,undefined8 uParm4,undefined8 uParm5,undefined8 uParm6);
ulong FUN_00412d95(long param_1,undefined8 param_2,ulong param_3,ulong param_4,ulong param_5,undefined8 param_6,ulong *param_7,char param_8);
ulong FUN_0041311d(ulong *puParm1,long lParm2,ulong uParm3,int iParm4);
ulong FUN_004133cd(long *param_1,long param_2,long param_3,long param_4,long param_5,undefined8 param_6,ulong param_7,undefined8 *param_8,uint param_9);
ulong FUN_00414148(long lParm1);
long FUN_0041446a(long lParm1,char cParm2,long lParm3);
undefined8 FUN_00414983(long *plParm1,long lParm2,uint uParm3);
undefined8 FUN_00414a4a(long lParm1,long lParm2,undefined8 uParm3);
long FUN_00414aed(long param_1,undefined8 param_2,long param_3,long *param_4,long param_5,long param_6,long param_7);
ulong FUN_0041502f(long *plParm1,undefined8 uParm2,undefined8 uParm3,long lParm4,undefined8 uParm5,undefined8 uParm6);
undefined8 FUN_004151fc(long *plParm1,undefined8 *puParm2,long lParm3,undefined8 uParm4,undefined8 *puParm5);
undefined8 FUN_0041534d(long *plParm1,long lParm2,ulong uParm3,long *plParm4,char cParm5);
undefined8 FUN_0041575f(long *plParm1);
void FUN_004157f5(long *plParm1,long lParm2,long lParm3,long lParm4,long lParm5,long lParm6);
ulong FUN_00415999(long lParm1,long *plParm2);
undefined8 FUN_00415b36(long lParm1,long *plParm2,long lParm3,undefined8 uParm4);
ulong FUN_00415d40(long lParm1,long lParm2);
ulong FUN_00415e2a(undefined8 uParm1,long lParm2,long lParm3,long lParm4);
ulong FUN_00415f84(long lParm1,long *plParm2,long lParm3,long lParm4);
ulong FUN_00416163(long lParm1,long lParm2,undefined8 uParm3);
ulong FUN_00416292(long *plParm1,long lParm2,undefined8 uParm3,undefined8 uParm4);
undefined8 FUN_00416520(long lParm1,long lParm2,undefined8 uParm3,undefined8 uParm4,undefined8 uParm5,undefined8 uParm6);
ulong FUN_0041668c(long lParm1,uint uParm2,long lParm3,long lParm4,long lParm5);
undefined8 FUN_004168fd(long lParm1,long lParm2,undefined8 uParm3,undefined8 uParm4,long lParm5,undefined8 uParm6);
ulong FUN_004169ca(long *plParm1,long lParm2,undefined8 uParm3,long lParm4,long lParm5,long lParm6);
ulong FUN_00416d3e(long lParm1,long *plParm2,long lParm3,long lParm4);
ulong FUN_004171ee(long lParm1,long *plParm2,long lParm3,long lParm4,long lParm5);
undefined8 FUN_004172c8(int *piParm1,long lParm2,long lParm3);
long FUN_00417443(int *piParm1,long lParm2,long lParm3);
long FUN_004176cf(int *piParm1,long lParm2);
ulong FUN_00417779(long lParm1,long lParm2,undefined8 uParm3);
ulong FUN_00417874(long lParm1,long lParm2);
ulong FUN_00417bea(long lParm1,long lParm2);
ulong FUN_0041813f(long lParm1,long lParm2,long lParm3);
ulong FUN_004186a4(undefined8 uParm1,long *plParm2,undefined8 *puParm3,undefined8 uParm4,long lParm5);
long FUN_0041877b(long *plParm1,long lParm2,long lParm3,uint uParm4);
ulong FUN_00418807(long param_1,long *param_2,long param_3,long param_4,undefined8 param_5,long param_6,uint param_7);
ulong FUN_00418f95(long lParm1,long lParm2,long lParm3,undefined8 uParm4);
ulong FUN_00419258(long lParm1,undefined8 *puParm2,undefined8 uParm3,uint uParm4);
ulong FUN_004193cc(long *plParm1,undefined8 uParm2,long lParm3,long lParm4,uint uParm5);
ulong FUN_00419589(long lParm1,undefined8 uParm2,long lParm3,undefined8 uParm4,uint uParm5);
undefined8 FUN_00419964(long lParm1,long lParm2);
long FUN_0041a37d(long *plParm1,long lParm2,long lParm3,long lParm4);
ulong FUN_0041ac17(long *plParm1,long lParm2,long lParm3,long lParm4);
undefined8 FUN_0041b052(long lParm1,undefined8 *puParm2,long lParm3);
ulong FUN_0041b223(long lParm1,int iParm2);
undefined8 FUN_0041b3ab(long lParm1,undefined4 uParm2,ulong uParm3);
void FUN_0041b4ec(long lParm1);
void FUN_0041b5fc(long lParm1);
undefined8 FUN_0041b63c(long lParm1,undefined8 uParm2,long lParm3,long lParm4,long lParm5);
long FUN_0041b959(long lParm1,long lParm2);
undefined8 FUN_0041ba2f(long lParm1,undefined8 uParm2,undefined8 uParm3);
undefined8 * FUN_0041bba7(long lParm1,undefined8 uParm2,undefined8 uParm3);
void FUN_0041bcb3(undefined8 *puParm1,undefined8 uParm2,undefined8 uParm3,undefined8 uParm4,undefined8 uParm5);
ulong FUN_0041bd1a(undefined8 uParm1,undefined8 uParm2,undefined8 uParm3);
ulong FUN_0041be3b(byte *pbParm1,byte *pbParm2);
undefined8 FUN_0041beb2(undefined8 uParm1);
undefined8 FUN_0041bf3d(void);
ulong FUN_0041bf4a(uint uParm1);
undefined1 * FUN_0041c01b(void);
char * FUN_0041c3ce(void);
ulong FUN_0041c49c(void);
long FUN_0041c4e9(ulong uParm1,long lParm2,ulong uParm3,long lParm4,long *plParm5);
int * FUN_0041c735(ulong uParm1,uint *puParm2,ulong uParm3,uint *puParm4,ulong *puParm5);
char * FUN_0041d215(ulong uParm1,long lParm2,long lParm3);
long FUN_0041d483(int *param_1,long *param_2);
long FUN_0041d66e(undefined auParm1 [16],int *piParm2,long *plParm3);
undefined8 FUN_0041da2d(int iParm1,long lParm2,uint *puParm3,long lParm4,uint uParm5);
void FUN_0041e240(uint param_1);
void FUN_0041e28a(undefined8 uParm1,uint uParm2);
ulong FUN_0041e2dc(void);
ulong FUN_0041e66e(undefined auParm1 [16],undefined8 uParm2,undefined8 uParm3);
ulong FUN_0041ea46(char *pcParm1,long lParm2);
undefined8 FUN_0041ea9f(long param_1,long param_2,undefined param_3,int param_4,undefined8 param_5,ulong param_6,int param_7,ulong param_8,int param_9);
ulong FUN_004261cc(uint uParm1);
void FUN_004261eb(undefined8 uParm1,int *piParm2,undefined8 uParm3);
void FUN_0042628c(int *param_1);
ulong FUN_0042634b(ulong uParm1,long lParm2);
void FUN_0042637f(undefined8 uParm1,undefined8 uParm2,undefined8 uParm3);
void FUN_004263ba(undefined8 uParm1,undefined8 uParm2,undefined8 uParm3,undefined8 uParm4);
ulong FUN_0042640b(ulong uParm1,ulong uParm2);
undefined8 FUN_00426426(uint *puParm1,ulong *puParm2);
undefined8 FUN_00426bc6(char *pcParm1,ulong *puParm2,char **ppcParm3);
ulong FUN_00427e7c(undefined auParm1 [16]);
ulong FUN_00427eb7(void);
void FUN_00427ef0(void);
undefined8 _DT_FINI(void);
undefined FUN_0062f000();
undefined FUN_0062f008();
undefined FUN_0062f010();
undefined FUN_0062f018();
undefined FUN_0062f020();
undefined FUN_0062f028();
undefined FUN_0062f030();
undefined FUN_0062f038();
undefined FUN_0062f040();
undefined FUN_0062f048();
undefined FUN_0062f050();
undefined FUN_0062f058();
undefined FUN_0062f060();
undefined FUN_0062f068();
undefined FUN_0062f070();
undefined FUN_0062f078();
undefined FUN_0062f080();
undefined FUN_0062f088();
undefined FUN_0062f090();
undefined FUN_0062f098();
undefined FUN_0062f0a0();
undefined FUN_0062f0a8();
undefined FUN_0062f0b0();
undefined FUN_0062f0b8();
undefined FUN_0062f0c0();
undefined FUN_0062f0c8();
undefined FUN_0062f0d0();
undefined FUN_0062f0d8();
undefined FUN_0062f0e0();
undefined FUN_0062f0e8();
undefined FUN_0062f0f0();
undefined FUN_0062f0f8();
undefined FUN_0062f100();
undefined FUN_0062f108();
undefined FUN_0062f110();
undefined FUN_0062f118();
undefined FUN_0062f120();
undefined FUN_0062f128();
undefined FUN_0062f130();
undefined FUN_0062f138();
undefined FUN_0062f140();
undefined FUN_0062f148();
undefined FUN_0062f150();
undefined FUN_0062f158();
undefined FUN_0062f160();
undefined FUN_0062f168();
undefined FUN_0062f170();
undefined FUN_0062f178();
undefined FUN_0062f180();
undefined FUN_0062f188();
undefined FUN_0062f190();
undefined FUN_0062f198();
undefined FUN_0062f1a0();
undefined FUN_0062f1a8();
undefined FUN_0062f1b0();
undefined FUN_0062f1b8();
undefined FUN_0062f1c0();
undefined FUN_0062f1c8();
undefined FUN_0062f1d0();
undefined FUN_0062f1d8();
undefined FUN_0062f1e0();
undefined FUN_0062f1e8();
undefined FUN_0062f1f0();
undefined FUN_0062f1f8();
undefined FUN_0062f200();
undefined FUN_0062f208();
undefined FUN_0062f210();
undefined FUN_0062f218();
undefined FUN_0062f220();
undefined FUN_0062f228();
undefined FUN_0062f230();
undefined FUN_0062f238();
undefined FUN_0062f240();
undefined FUN_0062f248();
undefined FUN_0062f250();
undefined FUN_0062f258();
undefined FUN_0062f260();
undefined FUN_0062f268();
undefined FUN_0062f270();
undefined FUN_0062f278();
undefined FUN_0062f280();
undefined FUN_0062f288();
undefined FUN_0062f290();
undefined FUN_0062f298();
undefined FUN_0062f2a0();
undefined FUN_0062f2a8();
undefined FUN_0062f2b0();
undefined FUN_0062f2b8();
undefined FUN_0062f2c0();



#include "nl.h"

long null_ARRAY_0062e51_0_8_;
long null_ARRAY_0062e51_8_8_;
long null_ARRAY_0062eac_16_8_;
long null_ARRAY_0062eac_8_8_;
long null_ARRAY_0062eb4_0_8_;
long null_ARRAY_0062eb4_16_8_;
long null_ARRAY_0062eb4_24_8_;
long null_ARRAY_0062eb4_32_8_;
long null_ARRAY_0062eb4_40_8_;
long null_ARRAY_0062eb4_48_8_;
long null_ARRAY_0062eb4_8_8_;
long null_ARRAY_0062ec8_0_4_;
long null_ARRAY_0062ec8_16_8_;
long null_ARRAY_0062ec8_4_4_;
long null_ARRAY_0062ec8_8_4_;
long local_a_0_4_;
long DAT_00427fc3;
long DAT_0042807d;
long DAT_004280fb;
long DAT_00428a4b;
long DAT_00428a4e;
long DAT_00428a8b;
long DAT_00428a8d;
long DAT_00428b88;
long DAT_00428b8b;
long DAT_00428b8e;
long DAT_00428c06;
long DAT_00428c50;
long DAT_00428d9e;
long DAT_00428da2;
long DAT_00428da7;
long DAT_00428da9;
long DAT_00429413;
long DAT_004297e0;
long DAT_00429b78;
long DAT_00429b7d;
long DAT_00429be7;
long DAT_00429c78;
long DAT_00429c7b;
long DAT_00429cc9;
long DAT_00429f40;
long DAT_0042a6f0;
long DAT_0042a6f8;
long DAT_0042a828;
long DAT_0042a99c;
long DAT_0042aa10;
long DAT_0042aa11;
long DAT_0062e000;
long DAT_0062e010;
long DAT_0062e020;
long DAT_0062e4a8;
long DAT_0062e4b0;
long DAT_0062e4b8;
long DAT_0062e4c0;
long DAT_0062e4c8;
long DAT_0062e4e0;
long DAT_0062e500;
long DAT_0062e578;
long DAT_0062e57c;
long DAT_0062e580;
long DAT_0062e5c0;
long DAT_0062e5c8;
long DAT_0062e5d0;
long DAT_0062e5e0;
long DAT_0062e5e8;
long DAT_0062e600;
long DAT_0062e608;
long DAT_0062e680;
long DAT_0062ea80;
long DAT_0062ea88;
long DAT_0062ea90;
long DAT_0062ea98;
long DAT_0062eaa0;
long DAT_0062eaa8;
long DAT_0062eab0;
long DAT_0062ead8;
long DAT_0062eae0;
long DAT_0062eae8;
long DAT_0062eaf0;
long DAT_0062eaf8;
long DAT_0062eb00;
long DAT_0062eb08;
long DAT_0062ecb8;
long DAT_0062ecc0;
long DAT_0062ecc8;
long DAT_0062ecd8;
long DAT_0062ece0;
long fde_0042bc10;
long FLOAT_UNKNOWN;
long null_ARRAY_00428240;
long null_ARRAY_00429e80;
long null_ARRAY_00429f20;
long null_ARRAY_0042ae60;
long null_ARRAY_0062e510;
long null_ARRAY_0062e540;
long null_ARRAY_0062e620;
long null_ARRAY_0062e6c0;
long null_ARRAY_0062e700;
long null_ARRAY_0062e740;
long null_ARRAY_0062e780;
long null_ARRAY_0062e880;
long null_ARRAY_0062e980;
long null_ARRAY_0062eac0;
long null_ARRAY_0062eb40;
long null_ARRAY_0062eb80;
long null_ARRAY_0062ec80;
long PTR_DAT_0062e480;
long PTR_DAT_0062e488;
long PTR_DAT_0062e490;
long PTR_DAT_0062e498;
long PTR_DAT_0062e4a0;
long PTR_DAT_0062e4d8;
long PTR_null_ARRAY_0062e520;
long PTR_s___ld_s_0062e4d0;
long stack0x00000008;
long stack0xfffffffffffffff8;
long s_Try___s___help__for_more_informa_00428400;
ulong
FUN_004022de (uint uParm1)
{
  char cVar1;
  char *pcVar2;
  undefined8 uVar3;
  long lVar4;
  undefined8 extraout_RDX;
  char *pcVar5;
  char **ppcVar6;
  byte bStack33;

  if (uParm1 == 0)
    {
      func_0x00401a20 (s_Usage___s__OPTION______FILE_____00428428,
		       DAT_0062eb08);
      func_0x00401c30
	("Write each FILE to standard output, with line numbers added.\n",
	 DAT_0062e5c0);
      FUN_0040210e ();
      FUN_00402128 ();
      func_0x00401c30
	("  -b, --body-numbering=STYLE      use STYLE for numbering body lines\n  -d, --section-delimiter=CC      use CC for logical page delimiters\n  -f, --footer-numbering=STYLE    use STYLE for numbering footer lines\n",
	 DAT_0062e5c0);
      func_0x00401c30
	("  -h, --header-numbering=STYLE    use STYLE for numbering header lines\n  -i, --line-increment=NUMBER     line number increment at each line\n  -l, --join-blank-lines=NUMBER   group of NUMBER empty lines counted as one\n  -n, --number-format=FORMAT      insert line numbers according to FORMAT\n  -p, --no-renumber               do not reset line numbers for each section\n  -s, --number-separator=STRING   add STRING after (possible) line number\n",
	 DAT_0062e5c0);
      func_0x00401c30
	("  -v, --starting-line-number=NUMBER  first line number for each section\n  -w, --number-width=NUMBER       use NUMBER columns for line numbers\n",
	 DAT_0062e5c0);
      func_0x00401c30 ("      --help     display this help and exit\n",
		       DAT_0062e5c0);
      func_0x00401c30
	("      --version  output version information and exit\n",
	 DAT_0062e5c0);
      func_0x00401c30
	("\nBy default, selects -v1 -i1 -l1 -sTAB -w6 -nrn -hn -bt -fn.\nCC are two delimiter characters used to construct logical page delimiters,\na missing second character implies :.  Type \\\\ for \\.  STYLE is one of:\n",
	 DAT_0062e5c0);
      pcVar5 = (char *) DAT_0062e5c0;
      func_0x00401c30
	("\n  a         number all lines\n  t         number only nonempty lines\n  n         number no lines\n  pBRE      number only lines that contain a match for the basic regular\n            expression, BRE\n\nFORMAT is one of:\n\n  ln   left justified, no leading zeros\n  rn   right justified, no leading zeros\n  rz   right justified, leading zeros\n\n");
      imperfection_wrapper ();	//    FUN_00402142();
    }
  else
    {
      pcVar5 = s_Try___s___help__for_more_informa_00428400;
      func_0x00401c20 (DAT_0062e5e0,
		       s_Try___s___help__for_more_informa_00428400,
		       DAT_0062eb08);
    }
  ppcVar6 = (char **) (ulong) uParm1;
  func_0x00401e40 ();
  bStack33 = 1;
  imperfection_wrapper ();	//  cVar1 = *DAT_0062ecd8;
  if (cVar1 == 'n')
    {
    LAB_00402432:
      ;
      *ppcVar6 = DAT_0062ecd8;
    }
  else
    {
      if (cVar1 < 'o')
	{
	  if (cVar1 == 'a')
	    goto LAB_00402432;
	}
      else
	{
	  if (cVar1 == 'p')
	    {
	      pcVar2 = DAT_0062ecd8 + 1;
	      *ppcVar6 = DAT_0062ecd8;
	      DAT_0062ecd8 = pcVar2;
	      *(undefined8 *) pcVar5 = 0;
	      ((undefined8 *) pcVar5)[1] = 0;
	      ((undefined8 *) pcVar5)[4] = extraout_RDX;
	      ((undefined8 *) pcVar5)[5] = 0;
	      DAT_0062ece0 = 0x2c6;
	      uVar3 = func_0x00401ed0 (DAT_0062ecd8);
	      imperfection_wrapper ();	//        lVar4 = FUN_0040b996(DAT_0062ecd8, uVar3, pcVar5, uVar3);
	      if (lVar4 != 0)
		{
		  imperfection_wrapper ();	//          FUN_00405975(1, 0, &DAT_00428a4e, lVar4);
		}
	      goto LAB_004024f2;
	    }
	  if (cVar1 == 't')
	    goto LAB_00402432;
	}
      bStack33 = 0;
    }
LAB_004024f2:
  ;
  return (ulong) bStack33;
}

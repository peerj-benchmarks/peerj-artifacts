typedef unsigned char   undefined;

typedef unsigned char    byte;
typedef unsigned char    dwfenc;
typedef unsigned int    dword;
typedef unsigned long    qword;
typedef unsigned char    undefined1;
typedef unsigned short    undefined2;
typedef unsigned int    undefined3;
typedef unsigned int    undefined4;
typedef unsigned long    undefined5;
typedef unsigned long    undefined6;
typedef unsigned long    undefined8;
typedef unsigned short    word;
#define unkbyte9   unsigned long long
#define unkbyte10   unsigned long long
#define unkbyte11   unsigned long long
#define unkbyte12   unsigned long long
#define unkbyte13   unsigned long long
#define unkbyte14   unsigned long long
#define unkbyte15   unsigned long long
#define unkbyte16   unsigned long long

#define unkuint9   unsigned long long
#define unkuint10   unsigned long long
#define unkuint11   unsigned long long
#define unkuint12   unsigned long long
#define unkuint13   unsigned long long
#define unkuint14   unsigned long long
#define unkuint15   unsigned long long
#define unkuint16   unsigned long long

#define unkint9   long long
#define unkint10   long long
#define unkint11   long long
#define unkint12   long long
#define unkint13   long long
#define unkint14   long long
#define unkint15   long long
#define unkint16   long long

#define unkfloat1   float
#define unkfloat2   float
#define unkfloat3   float
#define unkfloat5   double
#define unkfloat6   double
#define unkfloat7   double
#define unkfloat9   long double
#define unkfloat11   long double
#define unkfloat12   long double
#define unkfloat13   long double
#define unkfloat14   long double
#define unkfloat15   long double
#define unkfloat16   long double

#define BADSPACEBASE   void
#define code   void

#define ulong long
#define true 1
#define bool int
#define false 0
#define float10 int
#define ushort int
#define undefined7 long
#define undefined2 long
#define uint5 long
#define uint6 long

// Add definition for uint
typedef unsigned int    uint;

#define sbyte char

extern imperfection_wrapper(void);
typedef struct eh_frame_hdr eh_frame_hdr, *Peh_frame_hdr;

struct eh_frame_hdr {
    byte eh_frame_hdr_version; // Exception Handler Frame Header Version
    dwfenc eh_frame_pointer_encoding; // Exception Handler Frame Pointer Encoding
    dwfenc eh_frame_desc_entry_count_encoding; // Encoding of # of Exception Handler FDEs
    dwfenc eh_frame_table_encoding; // Exception Handler Table Encoding
};

typedef struct fde_table_entry fde_table_entry, *Pfde_table_entry;

struct fde_table_entry {
    dword initial_loc; // Initial Location
    dword data_loc; // Data location
};

typedef enum Elf64_DynTag {
    DT_AUDIT=1879047932,
    DT_AUXILIARY=2147483645,
    DT_BIND_NOW=24,
    DT_CHECKSUM=1879047672,
    DT_CONFIG=1879047930,
    DT_DEBUG=21,
    DT_DEPAUDIT=1879047931,
    DT_ENCODING=32,
    DT_FEATURE_1=1879047676,
    DT_FILTER=2147483647,
    DT_FINI=13,
    DT_FINI_ARRAY=26,
    DT_FINI_ARRAYSZ=28,
    DT_FLAGS=30,
    DT_FLAGS_1=1879048187,
    DT_GNU_CONFLICT=1879047928,
    DT_GNU_CONFLICTSZ=1879047670,
    DT_GNU_HASH=1879047925,
    DT_GNU_LIBLIST=1879047929,
    DT_GNU_LIBLISTSZ=1879047671,
    DT_GNU_PRELINKED=1879047669,
    DT_HASH=4,
    DT_INIT=12,
    DT_INIT_ARRAY=25,
    DT_INIT_ARRAYSZ=27,
    DT_JMPREL=23,
    DT_MOVEENT=1879047674,
    DT_MOVESZ=1879047675,
    DT_MOVETAB=1879047934,
    DT_NEEDED=1,
    DT_NULL=0,
    DT_PLTGOT=3,
    DT_PLTPAD=1879047933,
    DT_PLTPADSZ=1879047673,
    DT_PLTREL=20,
    DT_PLTRELSZ=2,
    DT_POSFLAG_1=1879047677,
    DT_PREINIT_ARRAYSZ=33,
    DT_REL=17,
    DT_RELA=7,
    DT_RELACOUNT=1879048185,
    DT_RELAENT=9,
    DT_RELASZ=8,
    DT_RELCOUNT=1879048186,
    DT_RELENT=19,
    DT_RELSZ=18,
    DT_RPATH=15,
    DT_RUNPATH=29,
    DT_SONAME=14,
    DT_STRSZ=10,
    DT_STRTAB=5,
    DT_SYMBOLIC=16,
    DT_SYMENT=11,
    DT_SYMINENT=1879047679,
    DT_SYMINFO=1879047935,
    DT_SYMINSZ=1879047678,
    DT_SYMTAB=6,
    DT_TEXTREL=22,
    DT_TLSDESC_GOT=1879047927,
    DT_TLSDESC_PLT=1879047926,
    DT_VERDEF=1879048188,
    DT_VERDEFNUM=1879048189,
    DT_VERNEED=1879048190,
    DT_VERNEEDNUM=1879048191,
    DT_VERSYM=1879048176
} Elf64_DynTag;

typedef enum Elf_ProgramHeaderType {
    PT_DYNAMIC=2,
    PT_GNU_EH_FRAME=1685382480,
    PT_GNU_RELRO=1685382482,
    PT_GNU_STACK=1685382481,
    PT_INTERP=3,
    PT_LOAD=1,
    PT_NOTE=4,
    PT_NULL=0,
    PT_PHDR=6,
    PT_SHLIB=5,
    PT_TLS=7
} Elf_ProgramHeaderType;

typedef struct Elf64_Dyn Elf64_Dyn, *PElf64_Dyn;

struct Elf64_Dyn {
    enum Elf64_DynTag d_tag;
    qword d_val;
};

typedef struct Elf64_Shdr Elf64_Shdr, *PElf64_Shdr;

typedef enum Elf_SectionHeaderType {
    SHT_CHECKSUM=1879048184,
    SHT_DYNAMIC=6,
    SHT_DYNSYM=11,
    SHT_FINI_ARRAY=15,
    SHT_GNU_ATTRIBUTES=1879048181,
    SHT_GNU_HASH=1879048182,
    SHT_GNU_LIBLIST=1879048183,
    SHT_GNU_verdef=1879048189,
    SHT_GNU_verneed=1879048190,
    SHT_GNU_versym=1879048191,
    SHT_GROUP=17,
    SHT_HASH=5,
    SHT_INIT_ARRAY=14,
    SHT_NOBITS=8,
    SHT_NOTE=7,
    SHT_NULL=0,
    SHT_PREINIT_ARRAY=16,
    SHT_PROGBITS=1,
    SHT_REL=9,
    SHT_RELA=4,
    SHT_SHLIB=10,
    SHT_STRTAB=3,
    SHT_SUNW_COMDAT=1879048187,
    SHT_SUNW_move=1879048186,
    SHT_SUNW_syminfo=1879048188,
    SHT_SYMTAB=2,
    SHT_SYMTAB_SHNDX=18
} Elf_SectionHeaderType;

struct Elf64_Shdr {
    dword sh_name;
    enum Elf_SectionHeaderType sh_type;
    qword sh_flags;
    qword sh_addr;
    qword sh_offset;
    qword sh_size;
    dword sh_link;
    dword sh_info;
    qword sh_addralign;
    qword sh_entsize;
};

typedef struct Elf64_Phdr Elf64_Phdr, *PElf64_Phdr;

struct Elf64_Phdr {
    enum Elf_ProgramHeaderType p_type;
    dword p_flags;
    qword p_offset;
    qword p_vaddr;
    qword p_paddr;
    qword p_filesz;
    qword p_memsz;
    qword p_align;
};

typedef struct Elf64_Rela Elf64_Rela, *PElf64_Rela;

struct Elf64_Rela {
    qword r_offset; // location to apply the relocation action
    qword r_info; // the symbol table index and the type of relocation
    qword r_addend; // a constant addend used to compute the relocatable field value
};

typedef struct Elf64_Sym Elf64_Sym, *PElf64_Sym;

struct Elf64_Sym {
    dword st_name;
    byte st_info;
    byte st_other;
    word st_shndx;
    qword st_value;
    qword st_size;
};

typedef struct Elf64_Ehdr Elf64_Ehdr, *PElf64_Ehdr;

struct Elf64_Ehdr {
    byte e_ident_magic_num;
    char e_ident_magic_str[3];
    byte e_ident_class;
    byte e_ident_data;
    byte e_ident_version;
    byte e_ident_pad[9];
    word e_type;
    word e_machine;
    dword e_version;
    qword e_entry;
    qword e_phoff;
    qword e_shoff;
    dword e_flags;
    word e_ehsize;
    word e_phentsize;
    word e_phnum;
    word e_shentsize;
    word e_shnum;
    word e_shstrndx;
};




undefined8 _DT_INIT(void);
void FUN_00401920(void);
void FUN_00401e70(void);
void entry(void);
void FUN_00401ed0(void);
void FUN_00401f50(void);
void FUN_00401fd0(void);
undefined8 FUN_0040200e(undefined8 uParm1);
void FUN_0040201c(undefined *puParm1);
void FUN_004021b8(int iParm1,undefined8 *puParm2);
void FUN_004022a1(uint uParm1);
void FUN_004023c4(undefined *puParm1);
ulong FUN_0040242e(uint uParm1,undefined8 *puParm2);
ulong FUN_004028c4(char *pcParm1);
long FUN_00402923(undefined8 uParm1);
ulong FUN_00402967(uint uParm1,undefined8 uParm2);
undefined8 FUN_00402a0f(uint uParm1,undefined8 uParm2,long lParm3,uint uParm4);
long FUN_00402a97(long lParm1);
undefined8 FUN_00402ab1(uint uParm1,undefined8 uParm2,long lParm3);
undefined8 FUN_00402b4e(long lParm1,long lParm2,char cParm3,char *pcParm4,int iParm5,undefined4 *puParm6);
undefined8 FUN_00402efc(int iParm1);
ulong FUN_00402f2f(char *pcParm1,uint uParm2);
void FUN_00402f68(undefined8 uParm1,undefined8 uParm2);
void FUN_00402fa2(long lParm1);
undefined8 FUN_00402fee(long lParm1,long lParm2,long lParm3,char cParm4);
ulong FUN_004031ed(long lParm1,long lParm2,long lParm3);
ulong FUN_00403753(long *plParm1,long lParm2);
void FUN_00403893(void);
long FUN_004038a3(undefined8 uParm1,long lParm2,long lParm3,long lParm4);
void FUN_004039d2(undefined8 uParm1,undefined8 uParm2,long lParm3);
void FUN_00403a53(long lParm1,long lParm2,long lParm3);
long FUN_00403b89(undefined8 uParm1,undefined8 uParm2,undefined8 uParm3,undefined8 uParm4,undefined8 uParm5,code *pcParm6);
void FUN_00403c0f(void);
void FUN_00403d13(void);
char * FUN_00403df8(char *pcParm1);
char * FUN_00403e61(long lParm1);
undefined8 FUN_00403f43(void);
void FUN_00403f4e(long lParm1);
ulong FUN_0040402b(undefined1 *puParm1,byte bParm2,uint uParm3);
undefined8 * FUN_004040b3(undefined8 *puParm1,int iParm2);
char * FUN_0040412a(char *pcParm1,int iParm2);
ulong FUN_004041ca(long param_1,ulong param_2,long param_3,ulong param_4,uint param_5,uint param_6,long param_7,char *param_8,char *param_9);
undefined1 * FUN_00405094(int iParm1,undefined8 uParm2,undefined8 uParm3,uint *puParm4);
void FUN_00405307(uint uParm1,uint uParm2,undefined8 uParm3);
void FUN_00405348(uint uParm1,undefined8 uParm2);
void FUN_0040536c(undefined8 uParm1,undefined8 uParm2,char cParm3);
void FUN_00405400(undefined8 uParm1,char cParm2);
void FUN_0040542a(undefined8 uParm1);
void FUN_00405449(uint uParm1,uint uParm2,undefined8 uParm3);
void FUN_004054e4(uint uParm1,undefined8 uParm2,undefined8 uParm3);
void FUN_00405510(uint uParm1,undefined8 uParm2);
void FUN_00405539(undefined8 uParm1);
undefined8 * FUN_00405558(undefined8 *puParm1);
void FUN_004055b5(uint uParm1,undefined8 uParm2,undefined8 uParm3);
void FUN_004055e1(undefined8 uParm1,long lParm2,undefined8 uParm3,undefined8 uParm4,undefined8 *puParm5,undefined8 uParm6);
void FUN_00405a45(undefined8 uParm1,undefined8 uParm2,undefined8 uParm3,undefined8 uParm4,uint *puParm5);
void FUN_00405b17(undefined4 uParm1,undefined4 uParm2,undefined4 uParm3,undefined4 uParm4,undefined4 uParm5,undefined4 uParm6,undefined4 uParm7,undefined4 uParm8,undefined8 param_1,undefined8 param_2,undefined8 param_3,undefined8 param_4,undefined8 param_5,undefined8 param_6);
ulong FUN_00405bcd(void);
void FUN_00405c0e(undefined8 uParm1);
long FUN_00405c28(long lParm1);
long FUN_00405c5d(long lParm1,long lParm2);
long FUN_00405cbe(void);
long FUN_00405ce8(undefined8 uParm1,uint uParm2,undefined8 uParm3);
ulong FUN_00405d53(void);
undefined8 FUN_00405d9e(ulong uParm1,ulong uParm2);
ulong FUN_00405e1d(uint uParm1);
void FUN_00405e46(void);
void FUN_00405e7a(uint uParm1);
void FUN_00405eee(uint uParm1,uint uParm2,undefined8 uParm3,undefined8 uParm4);
void FUN_00405f72(undefined4 uParm1,undefined4 uParm2,undefined4 uParm3,undefined4 uParm4,undefined4 uParm5,undefined4 uParm6,undefined4 uParm7,undefined4 uParm8,uint param_1,uint param_2,undefined8 param_3,undefined8 param_4,undefined8 param_5,undefined8 param_6);
void FUN_00406056(uint uParm1,undefined8 uParm2,uint uParm3,uint uParm4);
ulong FUN_00406081(uint uParm1,long lParm2,uint uParm3,uint uParm4);
undefined8 FUN_00406144(uint uParm1);
void FUN_004061a7(undefined8 uParm1);
void FUN_004061cb(void);
ulong FUN_004061d9(long lParm1);
undefined8 FUN_004062a9(undefined8 uParm1);
void FUN_004062c8(undefined8 uParm1,undefined8 uParm2,uint uParm3);
void FUN_004062f3(uint uParm1,undefined8 uParm2,undefined8 uParm3,uint uParm4);
void FUN_00406320(uint uParm1,undefined8 uParm2,undefined8 uParm3,uint uParm4);
ulong FUN_0040635d(uint uParm1,long lParm2,long lParm3,uint uParm4);
ulong FUN_00406470(long *plParm1,long *plParm2);
ulong FUN_004064c5(long lParm1,ulong uParm2);
undefined8 FUN_004064ef(long lParm1);
undefined8 FUN_0040658b(long lParm1,undefined8 *puParm2);
void FUN_0040669b(long lParm1,long lParm2);
void FUN_004067a8(long lParm1);
void FUN_004067f5(undefined8 uParm1);
void FUN_00406837(long lParm1,char cParm2);
long FUN_0040687a(uint uParm1,undefined8 uParm2,uint uParm3,uint *puParm4);
void FUN_0040690b(long lParm1,uint uParm2,char cParm3);
ulong FUN_00406998(long lParm1);
ulong FUN_00406a42(long lParm1,undefined8 uParm2);
long * FUN_00406ada(long *plParm1,uint uParm2,long lParm3);
void FUN_00406e9c(long lParm1,long lParm2);
undefined8 FUN_00406f56(long *plParm1);
ulong FUN_004070e2(ulong *puParm1,ulong uParm2);
ulong FUN_00407113(ulong *puParm1,ulong *puParm2);
undefined8 FUN_00407145(long lParm1);
undefined8 FUN_004072bf(undefined8 uParm1);
undefined8 FUN_004072f5(undefined8 uParm1);
long FUN_00407359(long *plParm1);
undefined8 FUN_00407a3d(undefined8 uParm1,long lParm2,int iParm3);
ulong FUN_00407a94(long *plParm1,long *plParm2);
void FUN_00407aef(long lParm1,undefined4 uParm2);
long FUN_00407b5f(long *plParm1,int iParm2);
undefined8 FUN_004084a9(long lParm1,long lParm2,char cParm3);
long FUN_00408688(long lParm1,long lParm2,ulong uParm3);
long FUN_004087cf(long lParm1,undefined8 uParm2,long lParm3);
void FUN_00408881(long lParm1);
undefined8 FUN_004088bc(long lParm1,long lParm2);
void FUN_00408989(long lParm1,long lParm2);
long FUN_00408a9c(long *plParm1);
ulong FUN_00408af2(long lParm1,long lParm2,uint uParm3,long lParm4);
void FUN_00408d28(long lParm1,int *piParm2);
ulong FUN_00408f0c(int param_1,undefined8 *param_2,char *param_3,long *param_4,int *param_5,int param_6,int *param_7,int param_8,undefined8 param_9);
char * FUN_004094f6(undefined8 uParm1,undefined8 uParm2,char *pcParm3,int *piParm4,int iParm5);
ulong FUN_004095c4(uint param_1,undefined8 *param_2,char *param_3,long param_4,undefined8 param_5,uint param_6,uint *param_7,uint param_8);
ulong FUN_00409d8d(uint param_1,undefined8 param_2,undefined8 param_3,undefined8 param_4,undefined8 param_5,uint param_6,uint param_7);
void FUN_00409e1d(uint uParm1,undefined8 uParm2,undefined8 uParm3,undefined8 uParm4,undefined8 uParm5);
void FUN_00409e67(undefined8 uParm1,undefined8 uParm2);
ulong FUN_00409e8c(long lParm1,long lParm2);
undefined8 FUN_00409f43(long lParm1);
ulong FUN_00409f74(uint *puParm1,byte *pbParm2,long lParm3,undefined8 uParm4);
void FUN_00409ff7(uint uParm1,undefined8 uParm2,uint uParm3,uint uParm4);
ulong FUN_0040a027(undefined4 uParm1,undefined4 uParm2,undefined4 uParm3,undefined4 uParm4,undefined4 uParm5,undefined4 uParm6,undefined4 uParm7,undefined4 uParm8,uint param_1,undefined8 param_2,uint param_3,undefined8 param_4,undefined8 param_5,undefined8 param_6);
undefined8 FUN_0040a1ee(long lParm1,long lParm2);
void FUN_0040a257(undefined8 uParm1,undefined8 uParm2);
ulong FUN_0040a27c(long lParm1,long lParm2);
ulong FUN_0040a30c(uint uParm1,long lParm2,uint uParm3);
ulong FUN_0040a4cd(undefined8 uParm1,undefined8 uParm2,undefined8 uParm3);
ulong FUN_0040a5ee(byte *pbParm1,byte *pbParm2);
undefined8 FUN_0040a665(uint uParm1,char cParm2);
undefined8 FUN_0040a6e0(undefined8 uParm1);
ulong FUN_0040a76b(ulong uParm1);
void FUN_0040a787(long lParm1);
undefined8 FUN_0040a7a8(long *plParm1,long *plParm2);
void FUN_0040a87f(undefined4 uParm1,undefined4 uParm2,undefined4 uParm3,undefined4 uParm4,undefined4 uParm5,undefined4 uParm6,undefined4 uParm7,undefined4 uParm8,undefined8 param_1,uint param_2,undefined8 param_3,undefined8 param_4,undefined8 param_5,undefined8 param_6);
undefined8 FUN_0040a98e(void);
ulong FUN_0040a99b(uint uParm1);
long FUN_0040aa6c(long *plParm1,undefined8 uParm2);
long FUN_0040aac3(long lParm1,long lParm2);
ulong FUN_0040ab56(ulong uParm1);
ulong FUN_0040abc2(ulong uParm1);
ulong FUN_0040ac09(undefined8 uParm1,ulong uParm2);
ulong FUN_0040ac40(ulong uParm1,ulong uParm2);
undefined8 FUN_0040ac59(long lParm1);
ulong FUN_0040ad52(ulong uParm1,long lParm2);
long * FUN_0040ae48(undefined8 uParm1,undefined1 *puParm2,code *pcParm3,code *pcParm4,long lParm5);
void FUN_0040afb0(long **pplParm1,undefined8 uParm2);
long FUN_0040b0da(long lParm1);
void FUN_0040b125(long lParm1,undefined8 *puParm2);
long FUN_0040b15a(long lParm1,long lParm2,long **pplParm3,char cParm4);
undefined8 FUN_0040b2ef(long lParm1,long **pplParm2,char cParm3);
undefined8 FUN_0040b4bd(long *plParm1,undefined8 uParm2);
undefined8 FUN_0040b6c1(long lParm1,long lParm2,long *plParm3);
undefined8 FUN_0040ba13(undefined8 uParm1,undefined8 uParm2);
long FUN_0040ba5c(long lParm1,undefined8 uParm2);
void FUN_0040bd3b(long lParm1,undefined4 uParm2);
ulong FUN_0040bd93(long lParm1);
ulong FUN_0040bda5(long lParm1,undefined4 uParm2);
ulong FUN_0040be2d(long lParm1);
undefined1 * FUN_0040beaf(void);
char * FUN_0040c262(void);
void FUN_0040c330(undefined4 uParm1,undefined4 uParm2,undefined4 uParm3,undefined4 uParm4,undefined4 uParm5,undefined4 uParm6,undefined4 uParm7,undefined4 uParm8,uint param_1,undefined8 param_2,uint param_3,undefined8 param_4,undefined8 param_5,undefined8 param_6);
ulong FUN_0040c441(uint uParm1);
ulong FUN_0040c451(uint uParm1);
ulong FUN_0040c4a1(undefined8 uParm1);
ulong FUN_0040c559(undefined4 uParm1,undefined4 uParm2,undefined4 uParm3,undefined4 uParm4,undefined4 uParm5,undefined4 uParm6,undefined4 uParm7,undefined4 uParm8,uint param_1,uint param_2,undefined8 param_3,undefined8 param_4,undefined8 param_5,undefined8 param_6);
ulong FUN_0040c810(void);
long FUN_0040c85d(ulong uParm1,long lParm2,ulong uParm3,long lParm4,long *plParm5);
int * FUN_0040caa9(ulong uParm1,uint *puParm2,ulong uParm3,uint *puParm4,ulong *puParm5);
char * FUN_0040d589(ulong uParm1,long lParm2,long lParm3);
long FUN_0040d7f7(int *param_1,long *param_2);
long FUN_0040d9e2(undefined auParm1 [16],int *piParm2,long *plParm3);
undefined8 FUN_0040dda1(int iParm1,long lParm2,uint *puParm3,long lParm4,uint uParm5);
void FUN_0040e5b4(uint param_1);
void FUN_0040e5fe(undefined8 uParm1,uint uParm2);
ulong FUN_0040e650(void);
ulong FUN_0040e9e2(undefined auParm1 [16],undefined8 uParm2,undefined8 uParm3);
ulong FUN_0040edba(char *pcParm1,long lParm2);
undefined8 FUN_0040ee13(long param_1,long param_2,undefined param_3,int param_4,undefined8 param_5,ulong param_6,int param_7,ulong param_8,int param_9);
ulong FUN_00416540(ulong uParm1,undefined4 uParm2);
ulong FUN_0041655c(uint uParm1);
void FUN_0041657b(undefined8 uParm1,int *piParm2,undefined8 uParm3);
void FUN_0041661c(int *param_1);
void FUN_004166db(uint uParm1);
ulong FUN_00416701(ulong uParm1,long lParm2);
void FUN_00416735(undefined8 uParm1,undefined8 uParm2,undefined8 uParm3);
void FUN_00416770(undefined8 uParm1,undefined8 uParm2,undefined8 uParm3,undefined8 uParm4);
ulong FUN_004167c1(ulong uParm1,ulong uParm2);
undefined8 FUN_004167dc(uint *puParm1,ulong *puParm2);
undefined8 FUN_00416f7c(char *pcParm1,ulong *puParm2,char **ppcParm3);
ulong FUN_00418232(undefined auParm1 [16]);
ulong FUN_0041826d(void);
void FUN_004182a0(void);
undefined8 _DT_FINI(void);
undefined FUN_0061e000();
undefined FUN_0061e008();
undefined FUN_0061e010();
undefined FUN_0061e018();
undefined FUN_0061e020();
undefined FUN_0061e028();
undefined FUN_0061e030();
undefined FUN_0061e038();
undefined FUN_0061e040();
undefined FUN_0061e048();
undefined FUN_0061e050();
undefined FUN_0061e058();
undefined FUN_0061e060();
undefined FUN_0061e068();
undefined FUN_0061e070();
undefined FUN_0061e078();
undefined FUN_0061e080();
undefined FUN_0061e088();
undefined FUN_0061e090();
undefined FUN_0061e098();
undefined FUN_0061e0a0();
undefined FUN_0061e0a8();
undefined FUN_0061e0b0();
undefined FUN_0061e0b8();
undefined FUN_0061e0c0();
undefined FUN_0061e0c8();
undefined FUN_0061e0d0();
undefined FUN_0061e0d8();
undefined FUN_0061e0e0();
undefined FUN_0061e0e8();
undefined FUN_0061e0f0();
undefined FUN_0061e0f8();
undefined FUN_0061e100();
undefined FUN_0061e108();
undefined FUN_0061e110();
undefined FUN_0061e118();
undefined FUN_0061e120();
undefined FUN_0061e128();
undefined FUN_0061e130();
undefined FUN_0061e138();
undefined FUN_0061e140();
undefined FUN_0061e148();
undefined FUN_0061e150();
undefined FUN_0061e158();
undefined FUN_0061e160();
undefined FUN_0061e168();
undefined FUN_0061e170();
undefined FUN_0061e178();
undefined FUN_0061e180();
undefined FUN_0061e188();
undefined FUN_0061e190();
undefined FUN_0061e198();
undefined FUN_0061e1a0();
undefined FUN_0061e1a8();
undefined FUN_0061e1b0();
undefined FUN_0061e1b8();
undefined FUN_0061e1c0();
undefined FUN_0061e1c8();
undefined FUN_0061e1d0();
undefined FUN_0061e1d8();
undefined FUN_0061e1e0();
undefined FUN_0061e1e8();
undefined FUN_0061e1f0();
undefined FUN_0061e1f8();
undefined FUN_0061e200();
undefined FUN_0061e208();
undefined FUN_0061e210();
undefined FUN_0061e218();
undefined FUN_0061e220();
undefined FUN_0061e228();
undefined FUN_0061e230();
undefined FUN_0061e238();
undefined FUN_0061e240();
undefined FUN_0061e248();
undefined FUN_0061e250();
undefined FUN_0061e258();
undefined FUN_0061e260();
undefined FUN_0061e268();
undefined FUN_0061e270();
undefined FUN_0061e278();
undefined FUN_0061e280();
undefined FUN_0061e288();
undefined FUN_0061e290();
undefined FUN_0061e298();
undefined FUN_0061e2a0();


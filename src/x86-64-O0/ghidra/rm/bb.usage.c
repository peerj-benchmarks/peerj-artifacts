
#include "rm.h"

long null_ARRAY_0061d49_0_8_;
long null_ARRAY_0061d49_8_8_;
long null_ARRAY_0061d60_0_8_;
long null_ARRAY_0061d60_16_8_;
long null_ARRAY_0061d60_24_8_;
long null_ARRAY_0061d60_32_8_;
long null_ARRAY_0061d60_40_8_;
long null_ARRAY_0061d60_48_8_;
long null_ARRAY_0061d60_8_8_;
long null_ARRAY_0061d76_0_4_;
long null_ARRAY_0061d76_16_8_;
long null_ARRAY_0061d76_4_4_;
long null_ARRAY_0061d76_8_4_;
long DAT_00418300;
long DAT_004183bd;
long DAT_0041843b;
long DAT_00418dea;
long DAT_00418e99;
long DAT_00418f63;
long DAT_00419028;
long DAT_0041902b;
long DAT_00419063;
long DAT_0041930f;
long DAT_0041936c;
long DAT_0041938b;
long DAT_004193a1;
long DAT_00419460;
long DAT_0041959e;
long DAT_004195a2;
long DAT_004195a7;
long DAT_004195a9;
long DAT_00419c00;
long DAT_00419c1b;
long DAT_00419fe0;
long DAT_0041a01d;
long DAT_0041a022;
long DAT_0041a038;
long DAT_0041a039;
long DAT_0041a03b;
long DAT_0041a0ff;
long DAT_0041a190;
long DAT_0041a193;
long DAT_0041a1e1;
long DAT_0041a21b;
long DAT_0041a348;
long DAT_0041a349;
long DAT_0061d000;
long DAT_0061d010;
long DAT_0061d020;
long DAT_0061d470;
long DAT_0061d480;
long DAT_0061d4f8;
long DAT_0061d4fc;
long DAT_0061d500;
long DAT_0061d540;
long DAT_0061d548;
long DAT_0061d550;
long DAT_0061d560;
long DAT_0061d568;
long DAT_0061d580;
long DAT_0061d588;
long DAT_0061d5e0;
long DAT_0061d5e8;
long DAT_0061d5f0;
long DAT_0061d5f8;
long DAT_0061d740;
long DAT_0061d741;
long DAT_0061d798;
long DAT_0061d7a0;
long DAT_0061d7a8;
long DAT_0061d7b0;
long DAT_0061d7b8;
long DAT_0061d7c8;
long fde_0041b338;
long FLOAT_UNKNOWN;
long null_ARRAY_00418500;
long null_ARRAY_004186a0;
long null_ARRAY_004186e0;
long null_ARRAY_0041a230;
long null_ARRAY_0041a780;
long null_ARRAY_0061d490;
long null_ARRAY_0061d4c0;
long null_ARRAY_0061d5a0;
long null_ARRAY_0061d5d0;
long null_ARRAY_0061d600;
long null_ARRAY_0061d640;
long null_ARRAY_0061d760;
long PTR_DAT_0061d460;
long PTR_FUN_0061d468;
long PTR_null_ARRAY_0061d4a0;
void
FUN_004022a1 (uint uParm1)
{
  int iVar1;
  undefined *puVar2;

  if (uParm1 == 0)
    {
      func_0x00401950 ("Usage: %s [OPTION]... [FILE]...\n", DAT_0061d5f8);
      func_0x00401b80
	("Remove (unlink) the FILE(s).\n\n  -f, --force           ignore nonexistent files and arguments, never prompt\n  -i                    prompt before every removal\n",
	 DAT_0061d540);
      func_0x00401b80
	("  -I                    prompt once before removing more than three files, or\n                          when removing recursively; less intrusive than -i,\n                          while still giving protection against most mistakes\n      --interactive[=WHEN]  prompt according to WHEN: never, once (-I), or\n                          always (-i); without WHEN, prompt always\n",
	 DAT_0061d540);
      func_0x00401b80
	("      --one-file-system  when removing a hierarchy recursively, skip any\n                          directory that is on a file system different from\n                          that of the corresponding command line argument\n",
	 DAT_0061d540);
      func_0x00401b80
	("      --no-preserve-root  do not treat \'/\' specially\n      --preserve-root   do not remove \'/\' (default)\n  -r, -R, --recursive   remove directories and their contents recursively\n  -d, --dir             remove empty directories\n  -v, --verbose         explain what is being done\n",
	 DAT_0061d540);
      func_0x00401b80 ("      --help     display this help and exit\n",
		       DAT_0061d540);
      func_0x00401b80
	("      --version  output version information and exit\n",
	 DAT_0061d540);
      func_0x00401b80
	("\nBy default, rm does not remove directories.  Use the --recursive (-r or -R)\noption to remove each listed directory, too, along with all of its contents.\n",
	 DAT_0061d540);
      func_0x00401950
	("\nTo remove a file whose name starts with a \'-\', for example \'-foo\',\nuse one of these commands:\n  %s -- -foo\n\n  %s ./-foo\n",
	 DAT_0061d5f8, DAT_0061d5f8);
      func_0x00401b80
	("\nNote that if you use rm to remove a file, it might be possible to recover\nsome of its contents, given sufficient expertise and/or time.  For greater\nassurance that the contents are truly unrecoverable, consider using shred.\n",
	 DAT_0061d540);
      imperfection_wrapper ();	//    FUN_0040201c();
    }
  else
    {
      func_0x00401b70 (DAT_0061d560,
		       "Try \'%s --help\' for more information.\n",
		       DAT_0061d5f8);
    }
  puVar2 = (undefined *) (ulong) uParm1;
  func_0x00401d50 ();
  *puVar2 = 0;
  *(undefined4 *) (puVar2 + 4) = 4;
  puVar2[8] = 0;
  puVar2[10] = 0;
  puVar2[9] = 0;
  *(undefined8 *) (puVar2 + 0x10) = 0;
  iVar1 = func_0x00401a90 (0);
  *(bool *) (puVar2 + 0x18) = iVar1 != 0;
  puVar2[0x19] = 0;
  puVar2[0x1a] = 0;
  return;
}


#include "tty.h"

long null_ARRAY_006143f_0_8_;
long null_ARRAY_006143f_8_8_;
long null_ARRAY_0061454_0_8_;
long null_ARRAY_0061454_16_8_;
long null_ARRAY_0061454_24_8_;
long null_ARRAY_0061454_32_8_;
long null_ARRAY_0061454_40_8_;
long null_ARRAY_0061454_48_8_;
long null_ARRAY_0061454_8_8_;
long null_ARRAY_0061468_0_4_;
long null_ARRAY_0061468_16_8_;
long null_ARRAY_0061468_4_4_;
long null_ARRAY_0061468_8_4_;
long DAT_00411380;
long DAT_0041143d;
long DAT_004114bb;
long DAT_0041170e;
long DAT_00411722;
long DAT_00411757;
long DAT_004117a0;
long DAT_004118de;
long DAT_004118e2;
long DAT_004118e7;
long DAT_004118e9;
long DAT_00411f53;
long DAT_00412320;
long DAT_00412338;
long DAT_0041233d;
long DAT_004123a7;
long DAT_00412438;
long DAT_0041243b;
long DAT_00412489;
long DAT_0041248d;
long DAT_00412500;
long DAT_00412501;
long DAT_004126e8;
long DAT_00614000;
long DAT_00614010;
long DAT_00614020;
long DAT_006143c8;
long DAT_006143e0;
long DAT_00614458;
long DAT_0061445c;
long DAT_00614460;
long DAT_00614480;
long DAT_00614490;
long DAT_006144a0;
long DAT_006144a8;
long DAT_006144c0;
long DAT_006144c8;
long DAT_00614510;
long DAT_00614518;
long DAT_00614520;
long DAT_00614528;
long DAT_006146b8;
long DAT_006146c0;
long DAT_006146c8;
long DAT_006146d8;
long fde_00413168;
long FLOAT_UNKNOWN;
long null_ARRAY_00411540;
long null_ARRAY_00412940;
long null_ARRAY_006143f0;
long null_ARRAY_00614420;
long null_ARRAY_006144e0;
long null_ARRAY_00614540;
long null_ARRAY_00614580;
long null_ARRAY_00614680;
long PTR_DAT_006143c0;
long PTR_null_ARRAY_00614400;
ulong
FUN_00401c22 (uint uParm1)
{
  int iVar1;
  undefined8 uVar2;
  undefined4 *puVar3;
  char *pcVar4;
  bool bVar5;
  char *pcStack48;

  if (uParm1 == 0)
    {
      func_0x004014c0 ("Usage: %s [OPTION]...\n", DAT_00614528);
      func_0x00401660
	("Print the file name of the terminal connected to standard input.\n\n  -s, --silent, --quiet   print nothing, only return an exit status\n",
	 DAT_00614480);
      func_0x00401660 ("      --help     display this help and exit\n",
		       DAT_00614480);
      pcVar4 = (char *) DAT_00614480;
      func_0x00401660
	("      --version  output version information and exit\n");
      imperfection_wrapper ();	//    FUN_00401a86();
    }
  else
    {
      pcVar4 = "Try \'%s --help\' for more information.\n";
      func_0x00401650 (DAT_006144a0,
		       "Try \'%s --help\' for more information.\n",
		       DAT_00614528);
    }
  func_0x004017e0 ();
  FUN_00401f44 (*(undefined8 *) pcVar4);
  func_0x004017a0 (6, &DAT_004114bb);
  FUN_00401a6e (3);
  func_0x00401780 (FUN_00401e5f);
  DAT_00614510 = '\0';
  do
    {
      while (true)
	{
	  imperfection_wrapper ();	//      iVar1 = FUN_00404dcb((ulong)uParm1, pcVar4, &DAT_00411722, null_ARRAY_00411540, 0);
	  if (iVar1 == -1)
	    {
	      if (DAT_00614458 < (int) uParm1)
		{
		  imperfection_wrapper ();	//          uVar2 = FUN_004033b8(((undefined8 *)pcVar4)[(long)DAT_00614458]);
		  imperfection_wrapper ();	//          FUN_00403bf2(0, 0, "extra operand %s", uVar2);
		  FUN_00401c22 (2);
		}
	      puVar3 = (undefined4 *) func_0x004017d0 ();
	      *puVar3 = 2;
	      if (DAT_00614510 == '\0')
		{
		  pcStack48 = (char *) func_0x00401740 (0);
		  bVar5 = pcStack48 == (char *) 0x0;
		  if (bVar5)
		    {
		      pcStack48 = "not a tty";
		    }
		  func_0x00401570 (pcStack48);
		}
	      else
		{
		  iVar1 = func_0x004015b0 (0);
		  bVar5 = iVar1 == 0;
		}
	      return (ulong) bVar5;
	    }
	  if (iVar1 != -0x82)
	    break;
	  FUN_00401c22 (0);
	LAB_00401d33:
	  ;
	  imperfection_wrapper ();	//      FUN_0040390d(DAT_00614480, &DAT_0041170e, "GNU coreutils", PTR_DAT_006143c0, "David MacKenzie", 0);
	  func_0x004017e0 (0);
	LAB_00401d71:
	  ;
	  imperfection_wrapper ();	//      FUN_00401c22();
	}
      if (iVar1 != 0x73)
	{
	  if (iVar1 == -0x83)
	    goto LAB_00401d33;
	  goto LAB_00401d71;
	}
      DAT_00614510 = '\x01';
    }
  while (true);
}

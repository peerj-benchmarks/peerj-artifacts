
#include "od.h"

long null_ARRAY_0061c7f_0_8_;
long null_ARRAY_0061c7f_8_8_;
long null_ARRAY_0061c9e_16_4_;
long null_ARRAY_0061c9e_32_4_;
long null_ARRAY_0061c9e_4_4_;
long null_ARRAY_0061c9e_8_4_;
long null_ARRAY_0061ca4_16_4_;
long null_ARRAY_0061ca4_32_4_;
long null_ARRAY_0061ca4_64_4_;
long null_ARRAY_0061cac_0_8_;
long null_ARRAY_0061cac_16_8_;
long null_ARRAY_0061cac_24_8_;
long null_ARRAY_0061cac_32_8_;
long null_ARRAY_0061cac_40_8_;
long null_ARRAY_0061cac_48_8_;
long null_ARRAY_0061cac_8_8_;
long null_ARRAY_0061cc0_0_4_;
long null_ARRAY_0061cc0_16_8_;
long null_ARRAY_0061cc0_4_4_;
long null_ARRAY_0061cc0_8_4_;
long DAT_00416c83;
long DAT_00416d3d;
long DAT_00416dbb;
long DAT_004170c4;
long DAT_00417128;
long DAT_00418005;
long DAT_00418008;
long DAT_0041800c;
long DAT_00418010;
long DAT_00418014;
long DAT_00418017;
long DAT_0041801a;
long DAT_0041801d;
long DAT_00418020;
long DAT_00418023;
long DAT_00418026;
long DAT_00418029;
long DAT_0041802c;
long DAT_0041802f;
long DAT_00418125;
long DAT_00418128;
long DAT_00418130;
long DAT_00418133;
long DAT_0041813e;
long DAT_00418141;
long DAT_00418143;
long DAT_00418146;
long DAT_00418307;
long DAT_00418309;
long DAT_00418378;
long DAT_00418399;
long DAT_00418515;
long DAT_00418db0;
long DAT_00418e1c;
long DAT_00418e34;
long DAT_00418ed0;
long DAT_0041901e;
long DAT_00419022;
long DAT_00419027;
long DAT_00419029;
long DAT_00419693;
long DAT_00419a60;
long DAT_00419a88;
long DAT_00419e78;
long DAT_00419e7d;
long DAT_00419ee7;
long DAT_00419f78;
long DAT_00419f7b;
long DAT_00419fc9;
long DAT_0041a7c8;
long DAT_0041a838;
long DAT_0041a839;
long DAT_0061c390;
long DAT_0061c3a0;
long DAT_0061c3b0;
long DAT_0061c7c0;
long DAT_0061c7c1;
long DAT_0061c7d8;
long DAT_0061c7e0;
long DAT_0061c858;
long DAT_0061c85c;
long DAT_0061c860;
long DAT_0061c868;
long DAT_0061c880;
long DAT_0061c888;
long DAT_0061c890;
long DAT_0061c8a0;
long DAT_0061c8a8;
long DAT_0061c8c0;
long DAT_0061c8c8;
long DAT_0061c940;
long DAT_0061c944;
long DAT_0061c948;
long DAT_0061c950;
long DAT_0061c951;
long DAT_0061c952;
long DAT_0061c958;
long DAT_0061c960;
long DAT_0061c968;
long DAT_0061c970;
long DAT_0061c978;
long DAT_0061c980;
long DAT_0061c988;
long DAT_0061c990;
long DAT_0061c998;
long DAT_0061c9a0;
long DAT_0061c9a8;
long DAT_0061c9b0;
long DAT_0061c9b8;
long DAT_0061c9c0;
long DAT_0061ca84;
long DAT_0061ca85;
long DAT_0061ca88;
long DAT_0061ca90;
long DAT_0061ca98;
long DAT_0061cc38;
long DAT_0061cc40;
long DAT_0061cc48;
long DAT_0061cc58;
long fde_0041b0c8;
long FLOAT_UNKNOWN;
long INFINITY;
long NAN;
long null_ARRAY_00416e00;
long null_ARRAY_00416e80;
long null_ARRAY_00416f00;
long null_ARRAY_00416f80;
long null_ARRAY_00416fe0;
long null_ARRAY_00417040;
long null_ARRAY_004170d0;
long null_ARRAY_00417110;
long null_ARRAY_004171c0;
long null_ARRAY_00418db5;
long null_ARRAY_0041a420;
long null_ARRAY_0061c7f0;
long null_ARRAY_0061c820;
long null_ARRAY_0061c8e0;
long null_ARRAY_0061c9e0;
long null_ARRAY_0061ca40;
long null_ARRAY_0061cac0;
long null_ARRAY_0061cb00;
long null_ARRAY_0061cc00;
long PTR_DAT_0061c7c8;
long PTR_FUN_0061c7d0;
long PTR_null_ARRAY_0061c800;
void
FUN_004020e0 (uint uParm1, undefined8 uParm2, undefined8 uParm3,
	      undefined8 uParm4, int iParm5, int iParm6)
{
  char cVar1;
  int iVar2;
  uint uVar3;
  char *extraout_RDX;
  char *pcVar4;
  char *pcVar5;
  int iStack52;
  char *pcStack48;
  char *pcStack40;

  if (uParm1 == 0)
    {
      uParm4 = DAT_0061ca98;
      func_0x004017f0
	("Usage: %s [OPTION]... [FILE]...\n  or:  %s [-abcdfilosx]... [FILE] [[+]OFFSET[.][b]]\n  or:  %s --traditional [OPTION]... [FILE] [[+]OFFSET[.][b] [+][LABEL][.][b]]\n",
	 DAT_0061ca98, DAT_0061ca98);
      func_0x004019d0
	("\nWrite an unambiguous representation, octal bytes by default,\nof FILE to standard output.  With more than one FILE argument,\nconcatenate them in the listed order to form the input.\n",
	 DAT_0061c880);
      FUN_00401ed1 ();
      func_0x004019d0
	("\nIf first and second call formats both apply, the second format is assumed\nif the last operand begins with + or (if there are 2 operands) a digit.\nAn OFFSET operand means -j OFFSET.  LABEL is the pseudo-address\nat first byte printed, incremented when dump is progressing.\nFor OFFSET and LABEL, a 0x or 0X prefix indicates hexadecimal;\nsuffixes may be . for octal and b for multiply by 512.\n",
	 DAT_0061c880);
      FUN_00401eeb ();
      func_0x004019d0
	("  -A, --address-radix=RADIX   output format for file offsets; RADIX is one\n                                of [doxn], for Decimal, Octal, Hex or None\n      --endian={big|little}   swap input bytes according the specified order\n  -j, --skip-bytes=BYTES      skip BYTES input bytes first\n",
	 DAT_0061c880);
      func_0x004019d0
	("  -N, --read-bytes=BYTES      limit dump to BYTES input bytes\n  -S BYTES, --strings[=BYTES]  output strings of at least BYTES graphic chars;\n                                3 is implied when BYTES is not specified\n  -t, --format=TYPE           select output format or formats\n  -v, --output-duplicates     do not use * to mark line suppression\n  -w[BYTES], --width[=BYTES]  output BYTES bytes per output line;\n                                32 is implied when BYTES is not specified\n      --traditional           accept arguments in third form above\n",
	 DAT_0061c880);
      func_0x004019d0 ("      --help     display this help and exit\n",
		       DAT_0061c880);
      func_0x004019d0
	("      --version  output version information and exit\n",
	 DAT_0061c880);
      func_0x004019d0
	("\n\nTraditional format specifications may be intermixed; they accumulate:\n  -a   same as -t a,  select named characters, ignoring high-order bit\n  -b   same as -t o1, select octal bytes\n  -c   same as -t c,  select printable characters or backslash escapes\n  -d   same as -t u2, select unsigned decimal 2-byte units\n",
	 DAT_0061c880);
      func_0x004019d0
	("  -f   same as -t fF, select floats\n  -i   same as -t dI, select decimal ints\n  -l   same as -t dL, select decimal longs\n  -o   same as -t o2, select octal 2-byte units\n  -s   same as -t d2, select decimal 2-byte units\n  -x   same as -t x2, select hexadecimal 2-byte units\n",
	 DAT_0061c880);
      func_0x004019d0
	("\n\nTYPE is made up of one or more of these specifications:\n  a          named character, ignoring high-order bit\n  c          printable character or backslash escape\n",
	 DAT_0061c880);
      func_0x004019d0
	("  d[SIZE]    signed decimal, SIZE bytes per integer\n  f[SIZE]    floating point, SIZE bytes per float\n  o[SIZE]    octal, SIZE bytes per integer\n  u[SIZE]    unsigned decimal, SIZE bytes per integer\n  x[SIZE]    hexadecimal, SIZE bytes per integer\n",
	 DAT_0061c880);
      func_0x004019d0
	("\nSIZE is a number.  For TYPE in [doux], SIZE may also be C for\nsizeof(char), S for sizeof(short), I for sizeof(int) or L for\nsizeof(long).  If TYPE is f, SIZE may also be F for sizeof(float), D\nfor sizeof(double) or L for sizeof(long double).\n",
	 DAT_0061c880);
      func_0x004019d0
	("\nAdding a z suffix to any type displays printable characters at the end of\neach output line.\n",
	 DAT_0061c880);
      pcVar4 = DAT_0061c880;
      func_0x004019d0
	("\n\nBYTES is hex with 0x or 0X prefix, and may have a multiplier suffix:\n  b    512\n  KB   1000\n  K    1024\n  MB   1000*1000\n  M    1024*1024\nand so on for G, T, P, E, Z, Y.\n");
      imperfection_wrapper ();	//    FUN_00401f05();
    }
  else
    {
      pcVar4 = "Try \'%s --help\' for more information.\n";
      func_0x004019c0 (DAT_0061c8a0,
		       "Try \'%s --help\' for more information.\n",
		       DAT_0061ca98);
    }
  pcVar5 = (char *) (ulong) uParm1;
  func_0x00401bd0 ();
  iStack52 = iParm6;
  pcStack48 = pcVar5;
  pcStack40 = extraout_RDX;
  while (pcVar4 < pcStack48)
    {
      iVar2 =
	(int) ((ulong) ((long) iParm6 * (long) (pcStack48 + -1)) /
	       (ulong) pcVar5);
      uVar3 = iParm5 + (iStack52 - iVar2);
      cVar1 = *pcStack40;
      pcStack40 = pcStack40 + 1;
      imperfection_wrapper ();	//    FUN_00407f8d(uParm4, (ulong)uVar3, (ulong)(uint)(int)cVar1, (ulong)uVar3);
      pcStack48 = pcStack48 + -1;
      iStack52 = iVar2;
    }
  return;
}


#include "kill.h"

long null_ARRAY_0061543_0_8_;
long null_ARRAY_0061543_8_8_;
long null_ARRAY_0061574_0_8_;
long null_ARRAY_0061574_16_8_;
long null_ARRAY_0061574_24_8_;
long null_ARRAY_0061574_32_8_;
long null_ARRAY_0061574_40_8_;
long null_ARRAY_0061574_48_8_;
long null_ARRAY_0061574_8_8_;
long null_ARRAY_0061588_0_4_;
long null_ARRAY_0061588_16_8_;
long null_ARRAY_0061588_4_4_;
long null_ARRAY_0061588_8_4_;
long DAT_00411fcb;
long DAT_00412085;
long DAT_00412103;
long DAT_0041258d;
long DAT_00412657;
long DAT_00412666;
long DAT_00412681;
long DAT_00412772;
long DAT_004127b8;
long DAT_004128de;
long DAT_004128e2;
long DAT_004128e7;
long DAT_004128e9;
long DAT_00412f53;
long DAT_00413320;
long DAT_00413338;
long DAT_0041333d;
long DAT_004133a7;
long DAT_00413438;
long DAT_0041343b;
long DAT_00413489;
long DAT_00413499;
long DAT_0041349d;
long DAT_00413510;
long DAT_00413511;
long DAT_00615000;
long DAT_00615010;
long DAT_00615020;
long DAT_00615408;
long DAT_00615420;
long DAT_00615498;
long DAT_0061549c;
long DAT_006154a0;
long DAT_006154c0;
long DAT_00615680;
long DAT_00615690;
long DAT_006156a0;
long DAT_006156a8;
long DAT_006156c0;
long DAT_006156c8;
long DAT_00615710;
long DAT_00615718;
long DAT_00615720;
long DAT_006158b8;
long DAT_006158c0;
long DAT_006158c8;
long DAT_006158d8;
long fde_004141d0;
long FLOAT_UNKNOWN;
long null_ARRAY_00412200;
long null_ARRAY_00413960;
long null_ARRAY_00615430;
long null_ARRAY_00615460;
long null_ARRAY_006156e0;
long null_ARRAY_00615740;
long null_ARRAY_00615780;
long null_ARRAY_00615880;
long PTR_DAT_00615400;
long PTR_null_ARRAY_00615440;
void
FUN_00401d74 (uint uParm1, undefined8 uParm2, undefined8 uParm3,
	      undefined8 uParm4)
{
  uint extraout_EDX;
  uint uVar1;
  undefined *puVar2;

  if (uParm1 == 0)
    {
      uParm4 = DAT_00615720;
      func_0x004015d0
	("Usage: %s [-s SIGNAL | -SIGNAL] PID...\n  or:  %s -l [SIGNAL]...\n  or:  %s -t [SIGNAL]...\n",
	 DAT_00615720, DAT_00615720);
      func_0x004017b0 ("Send signals to processes, or list signals.\n",
		       DAT_00615680);
      FUN_00401bbe ();
      func_0x004017b0
	("  -s, --signal=SIGNAL, -SIGNAL\n                   specify the name or number of the signal to be sent\n  -l, --list       list signal names, or convert signal names to/from numbers\n  -t, --table      print a table of signal information\n",
	 DAT_00615680);
      func_0x004017b0 ("      --help     display this help and exit\n",
		       DAT_00615680);
      func_0x004017b0
	("      --version  output version information and exit\n",
	 DAT_00615680);
      func_0x004017b0
	("\nSIGNAL may be a signal name like \'HUP\', or a signal number like \'1\',\nor the exit status of a process terminated by a signal.\nPID is an integer; if negative it identifies a process group.\n",
	 DAT_00615680);
      uVar1 = 0x41258d;
      func_0x004015d0
	("\nNOTE: your shell may have its own version of %s, which usually supersedes\nthe version described here.  Please refer to your shell\'s documentation\nfor details about the options it supports.\n");
      imperfection_wrapper ();	//    FUN_00401bd8();
    }
  else
    {
      uVar1 = 0x4122c0;
      func_0x00401790 (DAT_006156a0,
		       "Try \'%s --help\' for more information.\n",
		       DAT_00615720);
    }
  func_0x00401930 ();
  puVar2 = (undefined *) func_0x004018a0 ((ulong) uVar1);
  if (puVar2 == (undefined *) 0x0)
    {
      puVar2 = &DAT_00412657;
    }
  func_0x004015d0 ("%*d %-*s %s\n", (ulong) uParm1, (ulong) uVar1,
		   (ulong) extraout_EDX, uParm4, puVar2);
  return;
}

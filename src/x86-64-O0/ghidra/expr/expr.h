typedef unsigned char   undefined;

typedef unsigned char    byte;
typedef unsigned char    dwfenc;
typedef unsigned int    dword;
typedef unsigned long    qword;
typedef unsigned char    undefined1;
typedef unsigned short    undefined2;
typedef unsigned int    undefined4;
typedef unsigned long    undefined6;
typedef unsigned long    undefined8;
typedef unsigned short    word;
#define unkbyte9   unsigned long long
#define unkbyte10   unsigned long long
#define unkbyte11   unsigned long long
#define unkbyte12   unsigned long long
#define unkbyte13   unsigned long long
#define unkbyte14   unsigned long long
#define unkbyte15   unsigned long long
#define unkbyte16   unsigned long long

#define unkuint9   unsigned long long
#define unkuint10   unsigned long long
#define unkuint11   unsigned long long
#define unkuint12   unsigned long long
#define unkuint13   unsigned long long
#define unkuint14   unsigned long long
#define unkuint15   unsigned long long
#define unkuint16   unsigned long long

#define unkint9   long long
#define unkint10   long long
#define unkint11   long long
#define unkint12   long long
#define unkint13   long long
#define unkint14   long long
#define unkint15   long long
#define unkint16   long long

#define unkfloat1   float
#define unkfloat2   float
#define unkfloat3   float
#define unkfloat5   double
#define unkfloat6   double
#define unkfloat7   double
#define unkfloat9   long double
#define unkfloat11   long double
#define unkfloat12   long double
#define unkfloat13   long double
#define unkfloat14   long double
#define unkfloat15   long double
#define unkfloat16   long double

#define BADSPACEBASE   void
#define code   void

#define ulong long
#define true 1
#define bool int
#define false 0
#define float10 int
#define ushort int
#define undefined7 long
#define undefined2 long
#define uint5 long
#define uint6 long

// Add definition for uint
typedef unsigned int    uint;

#define sbyte char

extern imperfection_wrapper(void);
typedef struct eh_frame_hdr eh_frame_hdr, *Peh_frame_hdr;

struct eh_frame_hdr {
    byte eh_frame_hdr_version; // Exception Handler Frame Header Version
    dwfenc eh_frame_pointer_encoding; // Exception Handler Frame Pointer Encoding
    dwfenc eh_frame_desc_entry_count_encoding; // Encoding of # of Exception Handler FDEs
    dwfenc eh_frame_table_encoding; // Exception Handler Table Encoding
};

typedef struct fde_table_entry fde_table_entry, *Pfde_table_entry;

struct fde_table_entry {
    dword initial_loc; // Initial Location
    dword data_loc; // Data location
};

typedef enum Elf64_DynTag {
    DT_AUDIT=1879047932,
    DT_AUXILIARY=2147483645,
    DT_BIND_NOW=24,
    DT_CHECKSUM=1879047672,
    DT_CONFIG=1879047930,
    DT_DEBUG=21,
    DT_DEPAUDIT=1879047931,
    DT_ENCODING=32,
    DT_FEATURE_1=1879047676,
    DT_FILTER=2147483647,
    DT_FINI=13,
    DT_FINI_ARRAY=26,
    DT_FINI_ARRAYSZ=28,
    DT_FLAGS=30,
    DT_FLAGS_1=1879048187,
    DT_GNU_CONFLICT=1879047928,
    DT_GNU_CONFLICTSZ=1879047670,
    DT_GNU_HASH=1879047925,
    DT_GNU_LIBLIST=1879047929,
    DT_GNU_LIBLISTSZ=1879047671,
    DT_GNU_PRELINKED=1879047669,
    DT_HASH=4,
    DT_INIT=12,
    DT_INIT_ARRAY=25,
    DT_INIT_ARRAYSZ=27,
    DT_JMPREL=23,
    DT_MOVEENT=1879047674,
    DT_MOVESZ=1879047675,
    DT_MOVETAB=1879047934,
    DT_NEEDED=1,
    DT_NULL=0,
    DT_PLTGOT=3,
    DT_PLTPAD=1879047933,
    DT_PLTPADSZ=1879047673,
    DT_PLTREL=20,
    DT_PLTRELSZ=2,
    DT_POSFLAG_1=1879047677,
    DT_PREINIT_ARRAYSZ=33,
    DT_REL=17,
    DT_RELA=7,
    DT_RELACOUNT=1879048185,
    DT_RELAENT=9,
    DT_RELASZ=8,
    DT_RELCOUNT=1879048186,
    DT_RELENT=19,
    DT_RELSZ=18,
    DT_RPATH=15,
    DT_RUNPATH=29,
    DT_SONAME=14,
    DT_STRSZ=10,
    DT_STRTAB=5,
    DT_SYMBOLIC=16,
    DT_SYMENT=11,
    DT_SYMINENT=1879047679,
    DT_SYMINFO=1879047935,
    DT_SYMINSZ=1879047678,
    DT_SYMTAB=6,
    DT_TEXTREL=22,
    DT_TLSDESC_GOT=1879047927,
    DT_TLSDESC_PLT=1879047926,
    DT_VERDEF=1879048188,
    DT_VERDEFNUM=1879048189,
    DT_VERNEED=1879048190,
    DT_VERNEEDNUM=1879048191,
    DT_VERSYM=1879048176
} Elf64_DynTag;

typedef enum Elf_ProgramHeaderType {
    PT_DYNAMIC=2,
    PT_GNU_EH_FRAME=1685382480,
    PT_GNU_RELRO=1685382482,
    PT_GNU_STACK=1685382481,
    PT_INTERP=3,
    PT_LOAD=1,
    PT_NOTE=4,
    PT_NULL=0,
    PT_PHDR=6,
    PT_SHLIB=5,
    PT_TLS=7
} Elf_ProgramHeaderType;

typedef struct Elf64_Dyn Elf64_Dyn, *PElf64_Dyn;

struct Elf64_Dyn {
    enum Elf64_DynTag d_tag;
    qword d_val;
};

typedef struct Elf64_Shdr Elf64_Shdr, *PElf64_Shdr;

typedef enum Elf_SectionHeaderType {
    SHT_CHECKSUM=1879048184,
    SHT_DYNAMIC=6,
    SHT_DYNSYM=11,
    SHT_FINI_ARRAY=15,
    SHT_GNU_ATTRIBUTES=1879048181,
    SHT_GNU_HASH=1879048182,
    SHT_GNU_LIBLIST=1879048183,
    SHT_GNU_verdef=1879048189,
    SHT_GNU_verneed=1879048190,
    SHT_GNU_versym=1879048191,
    SHT_GROUP=17,
    SHT_HASH=5,
    SHT_INIT_ARRAY=14,
    SHT_NOBITS=8,
    SHT_NOTE=7,
    SHT_NULL=0,
    SHT_PREINIT_ARRAY=16,
    SHT_PROGBITS=1,
    SHT_REL=9,
    SHT_RELA=4,
    SHT_SHLIB=10,
    SHT_STRTAB=3,
    SHT_SUNW_COMDAT=1879048187,
    SHT_SUNW_move=1879048186,
    SHT_SUNW_syminfo=1879048188,
    SHT_SYMTAB=2,
    SHT_SYMTAB_SHNDX=18
} Elf_SectionHeaderType;

struct Elf64_Shdr {
    dword sh_name;
    enum Elf_SectionHeaderType sh_type;
    qword sh_flags;
    qword sh_addr;
    qword sh_offset;
    qword sh_size;
    dword sh_link;
    dword sh_info;
    qword sh_addralign;
    qword sh_entsize;
};

typedef struct Elf64_Phdr Elf64_Phdr, *PElf64_Phdr;

struct Elf64_Phdr {
    enum Elf_ProgramHeaderType p_type;
    dword p_flags;
    qword p_offset;
    qword p_vaddr;
    qword p_paddr;
    qword p_filesz;
    qword p_memsz;
    qword p_align;
};

typedef struct Elf64_Rela Elf64_Rela, *PElf64_Rela;

struct Elf64_Rela {
    qword r_offset; // location to apply the relocation action
    qword r_info; // the symbol table index and the type of relocation
    qword r_addend; // a constant addend used to compute the relocatable field value
};

typedef struct Elf64_Sym Elf64_Sym, *PElf64_Sym;

struct Elf64_Sym {
    dword st_name;
    byte st_info;
    byte st_other;
    word st_shndx;
    qword st_value;
    qword st_size;
};

typedef struct Elf64_Ehdr Elf64_Ehdr, *PElf64_Ehdr;

struct Elf64_Ehdr {
    byte e_ident_magic_num;
    char e_ident_magic_str[3];
    byte e_ident_class;
    byte e_ident_data;
    byte e_ident_version;
    byte e_ident_pad[9];
    word e_type;
    word e_machine;
    dword e_version;
    qword e_entry;
    qword e_phoff;
    qword e_shoff;
    dword e_flags;
    word e_ehsize;
    word e_phentsize;
    word e_phnum;
    word e_shentsize;
    word e_shnum;
    word e_shstrndx;
};




undefined8 _DT_INIT(void);
void FUN_00401920(void);
void entry(void);
void FUN_00401ee0(void);
void FUN_00401f60(void);
void FUN_00401fe0(void);
void FUN_0040201e(int iParm1);
void FUN_00402036(undefined *puParm1);
void FUN_004021d2(void);
void FUN_004021dc(undefined8 *puParm1,undefined8 uParm2);
undefined8 FUN_004021f5(undefined8 uParm1,undefined8 uParm2,uint uParm3);
void FUN_00402238(long *plParm1,long *plParm2,long *plParm3);
void FUN_004022a1(long *plParm1,long *plParm2,long *plParm3);
void FUN_00402307(long *plParm1,long *plParm2,long *plParm3);
void FUN_0040239a(long *plParm1,long *plParm2,long *plParm3);
void FUN_004023fb(long *plParm1,long *plParm2,long *plParm3);
void FUN_00402452(undefined8 uParm1,undefined8 uParm2,undefined8 *puParm3);
ulong FUN_00402485(long *plParm1);
ulong FUN_004024b2(ulong *puParm1);
undefined8 FUN_004024cd(undefined8 *puParm1);
ulong FUN_004024de(undefined8 uParm1,undefined8 uParm2,undefined8 *puParm3);
long FUN_00402524(char *pcParm1,char *pcParm2);
undefined * FUN_0040274e(long lParm1,ulong uParm2,ulong uParm3);
long FUN_0040290c(long lParm1,ulong uParm2);
ulong FUN_004029bd(int iParm1);
ulong FUN_00402af9(char cParm1);
ulong FUN_00402b25(uint uParm1,undefined8 *puParm2);
undefined4 * FUN_00402c7c(undefined8 uParm1);
undefined4 * FUN_00402cbd(undefined8 uParm1);
void FUN_00402cfe(int *piParm1);
void FUN_00402d45(int *piParm1);
ulong FUN_00402da3(int *piParm1);
undefined8 FUN_00402e38(char *pcParm1);
void FUN_00402e83(int *piParm1);
undefined8 FUN_00402eee(int *piParm1);
long FUN_00402fa2(undefined8 uParm1);
ulong FUN_00402ffd(undefined8 uParm1);
ulong FUN_0040305f(void);
void FUN_00403075(void);
undefined8 FUN_004030c0(long lParm1,long lParm2);
undefined8 FUN_0040332e(byte bParm1);
undefined8 FUN_0040343b(byte bParm1);
undefined8 FUN_004036fe(byte bParm1);
long FUN_00403780(byte bParm1);
long FUN_004038c4(byte bParm1);
long FUN_004039b3(byte bParm1);
undefined8 FUN_00403c4c(byte bParm1);
undefined8 FUN_00403d13(byte bParm1);
void FUN_00403de2(void);
char * FUN_00403ec7(long lParm1,long lParm2);
void FUN_00403ff9(undefined4 uParm1,undefined4 uParm2,undefined4 uParm3,undefined4 uParm4,undefined4 uParm5,undefined4 uParm6,undefined4 uParm7,undefined4 uParm8,int param_1,undefined8 param_2,undefined8 param_3,undefined8 param_4,undefined8 param_5,code *param_6);
byte * FUN_0040414a(byte *pbParm1,uint uParm2);
long FUN_0040421c(long lParm1);
void FUN_004042c5(char *pcParm1);
void FUN_004044b7(long lParm1);
ulong FUN_00404594(undefined1 *puParm1,byte bParm2,uint uParm3);
undefined8 * FUN_0040461c(undefined8 *puParm1,int iParm2);
char * FUN_00404693(char *pcParm1,int iParm2);
ulong FUN_00404733(long param_1,ulong param_2,long param_3,ulong param_4,uint param_5,uint param_6,long param_7,char *param_8,char *param_9);
undefined1 * FUN_004055fd(int iParm1,undefined8 uParm2,undefined8 uParm3,uint *puParm4);
void FUN_00405870(uint uParm1,uint uParm2,undefined8 uParm3);
void FUN_004058b1(undefined8 uParm1,undefined8 uParm2,char cParm3);
void FUN_00405945(undefined8 uParm1,char cParm2);
void FUN_0040596f(undefined8 uParm1);
long FUN_0040598e(long lParm1,long lParm2);
ulong FUN_004059d8(char *pcParm1,char *pcParm2,char cParm3);
ulong FUN_00405b3b(byte *pbParm1,byte *pbParm2,uint uParm3,uint uParm4);
void FUN_00405fab(undefined8 uParm1,undefined8 uParm2);
void FUN_00405fd7(undefined8 uParm1,long lParm2,undefined8 uParm3,undefined8 uParm4,undefined8 *puParm5,undefined8 uParm6);
void FUN_0040643b(undefined8 uParm1,undefined8 uParm2,undefined8 uParm3,undefined8 uParm4,uint *puParm5);
void FUN_0040650d(undefined8 uParm1);
long FUN_00406527(long lParm1);
long FUN_0040655c(long lParm1,long lParm2);
void FUN_004065bd(undefined8 uParm1,undefined8 uParm2);
void FUN_004065f1(undefined8 uParm1);
undefined8 FUN_0040661e(void);
undefined8 FUN_00406648(long *plParm1,int iParm2);
ulong FUN_004066e6(undefined8 uParm1,uint uParm2,int iParm3);
ulong FUN_00406727(char *pcParm1,char **ppcParm2,uint uParm3,undefined8 *puParm4,long lParm5);
undefined8 FUN_00406ac6(ulong uParm1,ulong uParm2);
ulong FUN_00406b45(uint uParm1);
void FUN_00406b6e(void);
void FUN_00406ba2(uint uParm1);
void FUN_00406c16(uint uParm1,uint uParm2,undefined8 uParm3,undefined8 uParm4);
void FUN_00406c9a(undefined4 uParm1,undefined4 uParm2,undefined4 uParm3,undefined4 uParm4,undefined4 uParm5,undefined4 uParm6,undefined4 uParm7,undefined4 uParm8,uint param_1,uint param_2,undefined8 param_3,undefined8 param_4,undefined8 param_5,undefined8 param_6);
void FUN_00406d7e(long lParm1,int *piParm2);
ulong FUN_00406f62(int param_1,undefined8 *param_2,char *param_3,long *param_4,int *param_5,int param_6,int *param_7,int param_8,undefined8 param_9);
char * FUN_0040754c(undefined8 uParm1,undefined8 uParm2,char *pcParm3,int *piParm4,int iParm5);
ulong FUN_0040761a(uint param_1,undefined8 *param_2,char *param_3,long param_4,undefined8 param_5,uint param_6,uint *param_7,uint param_8);
ulong FUN_00407de3(uint param_1,undefined8 param_2,undefined8 param_3,undefined8 param_4,undefined8 param_5,uint param_6,uint param_7);
void FUN_00407e73(uint uParm1,undefined8 uParm2,undefined8 uParm3,undefined8 uParm4,undefined8 uParm5);
undefined8 FUN_00407ebd(long lParm1);
ulong FUN_00407eee(uint *puParm1,byte *pbParm2,long lParm3,undefined8 uParm4);
ulong * FUN_00407f71(ulong *puParm1,char cParm2,ulong uParm3);
undefined8 FUN_00408083(long lParm1,long lParm2);
undefined8 FUN_004080ec(int iParm1);
void FUN_00408112(long lParm1,long lParm2);
void FUN_0040817e(long lParm1,long lParm2);
ulong FUN_004081ed(long lParm1,long lParm2);
void FUN_00408244(undefined8 uParm1);
void FUN_00408268(undefined8 uParm1);
void FUN_0040828c(undefined8 uParm1,undefined8 uParm2);
void FUN_004082b6(long lParm1);
void FUN_00408305(long lParm1,long lParm2);
void FUN_00408370(long lParm1,long lParm2);
ulong FUN_004083db(long lParm1,long lParm2);
ulong FUN_0040844e(long lParm1,long lParm2);
undefined8 FUN_00408497(void);
ulong FUN_004084aa(long param_1,undefined8 param_2,long param_3,long param_4,undefined8 param_5,byte param_6,long param_7);
ulong FUN_004085f5(long lParm1,undefined8 uParm2,long lParm3,long lParm4,byte bParm5,long lParm6);
undefined8 FUN_004087c9(long lParm1,ulong uParm2);
void FUN_0040893d(undefined8 uParm1,undefined8 uParm2,undefined8 *puParm3,long lParm4,char cParm5,long lParm6);
void FUN_00408a2d(long *plParm1);
undefined8 FUN_00408d58(long *plParm1);
long FUN_00409834(long *plParm1,long lParm2,uint *puParm3);
void FUN_00409967(long *plParm1);
void FUN_00409a38(long *plParm1);
ulong FUN_00409ad8(byte **ppbParm1,byte *pbParm2,uint uParm3);
ulong FUN_0040a7be(long *plParm1,long lParm2);
ulong FUN_0040a940(long *plParm1);
void FUN_0040aaca(long lParm1);
ulong FUN_0040ab17(long lParm1,ulong uParm2,uint uParm3);
undefined8 FUN_0040ac9d(long *plParm1,long lParm2);
undefined8 FUN_0040ad0a(undefined8 *puParm1,undefined8 uParm2);
undefined8 FUN_0040ad94(undefined8 *puParm1,long lParm2,long lParm3);
undefined8 FUN_0040ae72(long *plParm1,long lParm2);
undefined8 FUN_0040af52(long *plParm1,long lParm2,long lParm3);
undefined8 FUN_0040b333(long *plParm1,long lParm2,long lParm3);
undefined8 FUN_0040b645(long *plParm1,long lParm2);
ulong FUN_0040ba0b(long *plParm1,long lParm2);
undefined8 FUN_0040bbf6(long *plParm1,undefined8 uParm2);
undefined8 FUN_0040bcab(long lParm1,long lParm2);
long FUN_0040bd3a(long lParm1,long lParm2);
void FUN_0040bdf2(long lParm1,long lParm2);
long FUN_0040be70(long *plParm1,undefined8 uParm2,undefined8 uParm3);
long FUN_0040c208(long lParm1,uint uParm2);
ulong * FUN_0040c262(undefined4 *puParm1,long lParm2,long lParm3);
ulong * FUN_0040c384(undefined4 *puParm1,long lParm2,long lParm3,uint uParm4);
undefined8 FUN_0040c4b1(long *plParm1,ulong *puParm2,ulong uParm3);
void FUN_0040c665(long lParm1);
long FUN_0040c708(long *plParm1,long lParm2,undefined8 uParm3);
long FUN_0040c8dd(long *plParm1,long lParm2,uint uParm3,undefined8 uParm4);
char * FUN_0040cbc9(undefined8 uParm1,undefined8 uParm2,long lParm3);
undefined8 FUN_0040cc55(long *plParm1);
void FUN_0040cd4e(long **pplParm1,long lParm2,long lParm3);
void FUN_0040d427(long *plParm1,undefined8 uParm2);
void FUN_0040d689(long *plParm1);
ulong FUN_0040d70d(long *plParm1,undefined8 uParm2,undefined8 uParm3,ulong uParm4);
undefined8 FUN_0040da87(long *plParm1,ulong uParm2);
void FUN_0040de15(long lParm1);
void FUN_0040df97(long *plParm1);
ulong FUN_0040e026(long *plParm1);
void FUN_0040e375(long *plParm1);
ulong FUN_0040e5cc(long *plParm1);
ulong FUN_0040e962(long **pplParm1,code *pcParm2,undefined8 uParm3);
ulong FUN_0040ea40(long *plParm1,code *pcParm2,undefined8 uParm3);
undefined8 FUN_0040eafb(long lParm1,long lParm2);
ulong FUN_0040ec72(undefined8 uParm1,long lParm2);
long FUN_0040ed54(undefined4 *puParm1,long *plParm2,long lParm3);
undefined8 FUN_0040ef45(long *plParm1,long lParm2);
undefined8 FUN_0040f037(undefined8 uParm1,long lParm2);
ulong FUN_0040f0e6(long lParm1,long lParm2);
ulong FUN_0040f35d(long *plParm1,long lParm2,long lParm3,long lParm4,uint uParm5);
long FUN_0040f88d(long *plParm1,long lParm2,uint uParm3);
long FUN_0040f926(long *plParm1,long lParm2,undefined4 uParm3);
undefined8 FUN_0040fa5c(long lParm1);
ulong FUN_0040fb9a(long lParm1);
ulong FUN_0040fc7a(undefined8 *puParm1,long *plParm2,long lParm3,char cParm4);
void FUN_0041004c(undefined8 uParm1,long lParm2,undefined8 uParm3);
undefined8 FUN_00410091(long *plParm1,long lParm2,ulong uParm3);
ulong FUN_00410889(char *pcParm1,long lParm2,ulong uParm3);
long FUN_00410a9e(undefined8 uParm1,long *plParm2,ulong uParm3,int *piParm4);
long FUN_00410bca(undefined8 uParm1,long *plParm2,long lParm3,ulong uParm4,long lParm5,int *piParm6);
long FUN_00410dc9(undefined8 uParm1,undefined8 *puParm2,long lParm3,undefined8 uParm4,long lParm5,int *piParm6);
long FUN_00410f85(long lParm1,long *plParm2,undefined8 *puParm3,ulong uParm4,long lParm5,int *piParm6);
long FUN_0041180b(undefined8 uParm1,long *plParm2,long lParm3,ulong uParm4,undefined8 uParm5,int *piParm6);
long FUN_0041199f(long lParm1,long lParm2,undefined8 uParm3,undefined8 *puParm4,ulong uParm5,undefined4 *puParm6);
ulong FUN_00411ec3(byte bParm1,long lParm2);
undefined8 FUN_00411efa(ulong uParm1,undefined8 uParm2,long lParm3,long *plParm4,int *piParm5,int *piParm6);
undefined8 FUN_004122bb(undefined8 uParm1,undefined8 uParm2,undefined8 uParm3,byte *pbParm4);
long FUN_0041231a(long lParm1,long lParm2,long lParm3,ulong uParm4,int *piParm5);
undefined8 FUN_00412c50(undefined4 *param_1,long param_2,undefined *param_3,int param_4,undefined8 param_5,undefined8 param_6,char param_7);
undefined8 FUN_00412d9b(undefined4 *puParm1,long lParm2,char *pcParm3);
undefined8 FUN_00412f02(undefined8 uParm1,undefined8 uParm2,undefined8 uParm3,byte *pbParm4);
undefined8 FUN_00412f5c(long lParm1,undefined8 uParm2,long lParm3,long *plParm4,char *pcParm5,ulong uParm6);
long FUN_004138c3(long lParm1,undefined8 uParm2,undefined8 uParm3,char *pcParm4,byte bParm5,int *piParm6);
long FUN_00413b5e(undefined8 uParm1,byte *pbParm2,undefined8 uParm3);
void FUN_00413c45(undefined8 *puParm1);
void FUN_00413c7e(undefined8 uParm1,undefined8 uParm2,undefined8 uParm3,undefined uParm4);
undefined8 * FUN_00413cb5(long lParm1,undefined8 *puParm2,undefined8 *puParm3,undefined8 *puParm4);
undefined8 FUN_00413e01(long lParm1,long lParm2);
void FUN_00413e44(undefined8 *puParm1);
undefined8 FUN_00413ea8(undefined8 uParm1,long lParm2);
long * FUN_00413ecf(long **pplParm1,undefined8 uParm2);
void FUN_00413ffb(undefined8 uParm1,undefined8 uParm2,undefined8 uParm3,undefined8 uParm4,undefined8 uParm5);
ulong FUN_00414047(long param_1,undefined8 param_2,ulong param_3,ulong param_4,ulong param_5,undefined8 param_6,ulong *param_7,char param_8);
ulong FUN_004143cf(ulong *puParm1,long lParm2,ulong uParm3,int iParm4);
ulong FUN_0041467f(long *param_1,long param_2,long param_3,long param_4,long param_5,undefined8 param_6,ulong param_7,undefined8 *param_8,uint param_9);
ulong FUN_004153fa(long lParm1);
long FUN_0041571c(long lParm1,char cParm2,long lParm3);
undefined8 FUN_00415c35(long *plParm1,long lParm2,uint uParm3);
undefined8 FUN_00415cfc(long lParm1,long lParm2,undefined8 uParm3);
long FUN_00415d9f(long param_1,undefined8 param_2,long param_3,long *param_4,long param_5,long param_6,long param_7);
ulong FUN_004162e1(long *plParm1,undefined8 uParm2,undefined8 uParm3,long lParm4,undefined8 uParm5,undefined8 uParm6);
undefined8 FUN_004164ae(long *plParm1,undefined8 *puParm2,long lParm3,undefined8 uParm4,undefined8 *puParm5);
undefined8 FUN_004165ff(long *plParm1,long lParm2,ulong uParm3,long *plParm4,char cParm5);
undefined8 FUN_00416a11(long *plParm1);
void FUN_00416aa7(long *plParm1,long lParm2,long lParm3,long lParm4,long lParm5,long lParm6);
ulong FUN_00416c4b(long lParm1,long *plParm2);
undefined8 FUN_00416de8(long lParm1,long *plParm2,long lParm3,undefined8 uParm4);
ulong FUN_00416ff2(long lParm1,long lParm2);
ulong FUN_004170dc(undefined8 uParm1,long lParm2,long lParm3,long lParm4);
ulong FUN_00417236(long lParm1,long *plParm2,long lParm3,long lParm4);
ulong FUN_00417415(long lParm1,long lParm2,undefined8 uParm3);
ulong FUN_00417544(long *plParm1,long lParm2,undefined8 uParm3,undefined8 uParm4);
undefined8 FUN_004177d2(long lParm1,long lParm2,undefined8 uParm3,undefined8 uParm4,undefined8 uParm5,undefined8 uParm6);
ulong FUN_0041793e(long lParm1,uint uParm2,long lParm3,long lParm4,long lParm5);
undefined8 FUN_00417baf(long lParm1,long lParm2,undefined8 uParm3,undefined8 uParm4,long lParm5,undefined8 uParm6);
ulong FUN_00417c7c(long *plParm1,long lParm2,undefined8 uParm3,long lParm4,long lParm5,long lParm6);
ulong FUN_00417ff0(long lParm1,long *plParm2,long lParm3,long lParm4);
ulong FUN_004184a0(long lParm1,long *plParm2,long lParm3,long lParm4,long lParm5);
undefined8 FUN_0041857a(int *piParm1,long lParm2,long lParm3);
long FUN_004186f5(int *piParm1,long lParm2,long lParm3);
long FUN_00418981(int *piParm1,long lParm2);
ulong FUN_00418a2b(long lParm1,long lParm2,undefined8 uParm3);
ulong FUN_00418b26(long lParm1,long lParm2);
ulong FUN_00418e9c(long lParm1,long lParm2);
ulong FUN_004193f1(long lParm1,long lParm2,long lParm3);
ulong FUN_00419956(undefined8 uParm1,long *plParm2,undefined8 *puParm3,undefined8 uParm4,long lParm5);
long FUN_00419a2d(long *plParm1,long lParm2,long lParm3,uint uParm4);
ulong FUN_00419ab9(long param_1,long *param_2,long param_3,long param_4,undefined8 param_5,long param_6,uint param_7);
ulong FUN_0041a247(long lParm1,long lParm2,long lParm3,undefined8 uParm4);
ulong FUN_0041a50a(long lParm1,undefined8 *puParm2,undefined8 uParm3,uint uParm4);
ulong FUN_0041a67e(long *plParm1,undefined8 uParm2,long lParm3,long lParm4,uint uParm5);
ulong FUN_0041a83b(long lParm1,undefined8 uParm2,long lParm3,undefined8 uParm4,uint uParm5);
undefined8 FUN_0041ac16(long lParm1,long lParm2);
long FUN_0041b62f(long *plParm1,long lParm2,long lParm3,long lParm4);
ulong FUN_0041bec9(long *plParm1,long lParm2,long lParm3,long lParm4);
undefined8 FUN_0041c304(long lParm1,undefined8 *puParm2,long lParm3);
ulong FUN_0041c4d5(long lParm1,int iParm2);
undefined8 FUN_0041c65d(long lParm1,undefined4 uParm2,ulong uParm3);
void FUN_0041c79e(long lParm1);
void FUN_0041c8ae(long lParm1);
undefined8 FUN_0041c8ee(long lParm1,undefined8 uParm2,long lParm3,long lParm4,long lParm5);
long FUN_0041cc0b(long lParm1,long lParm2);
undefined8 FUN_0041cce1(long lParm1,undefined8 uParm2,undefined8 uParm3);
undefined8 * FUN_0041ce59(long lParm1,undefined8 uParm2,undefined8 uParm3);
void FUN_0041cf65(undefined8 *puParm1,undefined8 uParm2,undefined8 uParm3,undefined8 uParm4,undefined8 uParm5);
ulong FUN_0041cfcc(undefined8 uParm1,undefined8 uParm2,undefined8 uParm3);
ulong FUN_0041d0ed(byte *pbParm1,byte *pbParm2);
undefined8 FUN_0041d164(undefined8 uParm1);
undefined8 FUN_0041d1ef(void);
ulong FUN_0041d1fc(uint uParm1);
undefined1 * FUN_0041d2cd(void);
char * FUN_0041d680(void);
ulong FUN_0041d74e(byte bParm1);
ulong FUN_0041d784(undefined8 uParm1);
void FUN_0041d83c(undefined8 uParm1);
void FUN_0041d860(void);
ulong FUN_0041d86e(long lParm1);
undefined8 FUN_0041d93e(undefined8 uParm1);
void FUN_0041d95d(undefined8 uParm1,undefined8 uParm2,uint uParm3);
ulong FUN_0041d988(void);
long FUN_0041d9d5(ulong uParm1,long lParm2,ulong uParm3,long lParm4,long *plParm5);
int * FUN_0041dc21(ulong uParm1,uint *puParm2,ulong uParm3,uint *puParm4,ulong *puParm5);
char * FUN_0041e701(ulong uParm1,long lParm2,long lParm3);
long FUN_0041e96f(int *param_1,long *param_2);
long FUN_0041eb5a(undefined auParm1 [16],int *piParm2,long *plParm3);
undefined8 FUN_0041ef19(int iParm1,long lParm2,uint *puParm3,long lParm4,uint uParm5);
void FUN_0041f72c(uint param_1);
void FUN_0041f776(undefined8 uParm1,uint uParm2);
ulong FUN_0041f7c8(void);
ulong FUN_0041fb5a(undefined auParm1 [16],undefined8 uParm2,undefined8 uParm3);
ulong FUN_0041ff32(char *pcParm1,long lParm2);
undefined8 FUN_0041ff8b(long param_1,long param_2,undefined param_3,int param_4,undefined8 param_5,ulong param_6,int param_7,ulong param_8,int param_9);
ulong FUN_004276b8(uint uParm1);
void FUN_004276d7(undefined8 uParm1,int *piParm2,undefined8 uParm3);
void FUN_00427778(int *param_1);
ulong FUN_00427837(ulong uParm1,long lParm2);
void FUN_0042786b(undefined8 uParm1,undefined8 uParm2,undefined8 uParm3);
void FUN_004278a6(undefined8 uParm1,undefined8 uParm2,undefined8 uParm3,undefined8 uParm4);
ulong FUN_004278f7(ulong uParm1,ulong uParm2);
undefined8 FUN_00427912(uint *puParm1,ulong *puParm2);
undefined8 FUN_004280b2(char *pcParm1,ulong *puParm2,char **ppcParm3);
ulong FUN_00429368(undefined auParm1 [16]);
ulong FUN_004293a3(void);
void FUN_004293e0(void);
undefined8 _DT_FINI(void);
undefined FUN_00630000();
undefined FUN_00630008();
undefined FUN_00630010();
undefined FUN_00630018();
undefined FUN_00630020();
undefined FUN_00630028();
undefined FUN_00630030();
undefined FUN_00630038();
undefined FUN_00630040();
undefined FUN_00630048();
undefined FUN_00630050();
undefined FUN_00630058();
undefined FUN_00630060();
undefined FUN_00630068();
undefined FUN_00630070();
undefined FUN_00630078();
undefined FUN_00630080();
undefined FUN_00630088();
undefined FUN_00630090();
undefined FUN_00630098();
undefined FUN_006300a0();
undefined FUN_006300a8();
undefined FUN_006300b0();
undefined FUN_006300b8();
undefined FUN_006300c0();
undefined FUN_006300c8();
undefined FUN_006300d0();
undefined FUN_006300d8();
undefined FUN_006300e0();
undefined FUN_006300e8();
undefined FUN_006300f0();
undefined FUN_006300f8();
undefined FUN_00630100();
undefined FUN_00630108();
undefined FUN_00630110();
undefined FUN_00630118();
undefined FUN_00630120();
undefined FUN_00630128();
undefined FUN_00630130();
undefined FUN_00630138();
undefined FUN_00630140();
undefined FUN_00630148();
undefined FUN_00630150();
undefined FUN_00630158();
undefined FUN_00630160();
undefined FUN_00630168();
undefined FUN_00630170();
undefined FUN_00630178();
undefined FUN_00630180();
undefined FUN_00630188();
undefined FUN_00630190();
undefined FUN_00630198();
undefined FUN_006301a0();
undefined FUN_006301a8();
undefined FUN_006301b0();
undefined FUN_006301b8();
undefined FUN_006301c0();
undefined FUN_006301c8();
undefined FUN_006301d0();
undefined FUN_006301d8();
undefined FUN_006301e0();
undefined FUN_006301e8();
undefined FUN_006301f0();
undefined FUN_006301f8();
undefined FUN_00630200();
undefined FUN_00630208();
undefined FUN_00630210();
undefined FUN_00630218();
undefined FUN_00630220();
undefined FUN_00630228();
undefined FUN_00630230();
undefined FUN_00630238();
undefined FUN_00630240();
undefined FUN_00630248();
undefined FUN_00630250();
undefined FUN_00630258();
undefined FUN_00630260();
undefined FUN_00630268();
undefined FUN_00630270();
undefined FUN_00630278();
undefined FUN_00630280();
undefined FUN_00630288();
undefined FUN_00630290();
undefined FUN_00630298();
undefined FUN_006302a0();
undefined FUN_006302a8();
undefined FUN_006302b0();



#include "expr.h"

long null_ARRAY_0062fad_0_8_;
long null_ARRAY_0062fad_8_8_;
long null_ARRAY_0062fc4_0_8_;
long null_ARRAY_0062fc4_16_8_;
long null_ARRAY_0062fc4_24_8_;
long null_ARRAY_0062fc4_32_8_;
long null_ARRAY_0062fc4_40_8_;
long null_ARRAY_0062fc4_48_8_;
long null_ARRAY_0062fc4_8_8_;
long null_ARRAY_0062fd8_0_4_;
long null_ARRAY_0062fd8_16_8_;
long null_ARRAY_0062fd8_4_4_;
long null_ARRAY_0062fd8_8_4_;
long local_a_0_4_;
long DAT_00000022;
long DAT_00429440;
long DAT_004294fd;
long DAT_0042957b;
long DAT_00429db4;
long DAT_00429db9;
long DAT_00429dbc;
long DAT_00429de8;
long DAT_00429e25;
long DAT_00429e74;
long DAT_00429e9d;
long DAT_00429ee7;
long DAT_00429f03;
long DAT_00429f05;
long DAT_00429f07;
long DAT_00429f09;
long DAT_00429f31;
long DAT_00429f33;
long DAT_00429f35;
long DAT_00429f38;
long DAT_00429f3a;
long DAT_00429f3d;
long DAT_00429f40;
long DAT_00429f43;
long DAT_00429fe8;
long DAT_00429fea;
long DAT_00429fff;
long DAT_0042a0e0;
long DAT_0042a198;
long DAT_0042a2de;
long DAT_0042a2e2;
long DAT_0042a2e7;
long DAT_0042a2e9;
long DAT_0042a953;
long DAT_0042ad20;
long DAT_0042b0b0;
long DAT_0042b0b5;
long DAT_0042b11f;
long DAT_0042b1b0;
long DAT_0042b1b3;
long DAT_0042b201;
long DAT_0042b480;
long DAT_0042bc30;
long DAT_0042bc38;
long DAT_0042bd68;
long DAT_0042bedc;
long DAT_0042bf50;
long DAT_0042bf51;
long DAT_0062f638;
long DAT_0062f648;
long DAT_0062f658;
long DAT_0062faa0;
long DAT_0062fac0;
long DAT_0062fb38;
long DAT_0062fb3c;
long DAT_0062fb40;
long DAT_0062fb80;
long DAT_0062fb90;
long DAT_0062fba0;
long DAT_0062fba8;
long DAT_0062fbc0;
long DAT_0062fbc8;
long DAT_0062fc10;
long DAT_0062fc18;
long DAT_0062fc20;
long DAT_0062fc28;
long DAT_0062fdb8;
long DAT_0062fdc0;
long DAT_0062fdc8;
long DAT_0062fdd8;
long DAT_0062fde0;
long fde_0042d258;
long FLOAT_UNKNOWN;
long null_ARRAY_0042a080;
long null_ARRAY_0042b3c0;
long null_ARRAY_0042b460;
long null_ARRAY_0042bf80;
long null_ARRAY_0042c3c0;
long null_ARRAY_0062fad0;
long null_ARRAY_0062fbe0;
long null_ARRAY_0062fc40;
long null_ARRAY_0062fc80;
long null_ARRAY_0062fd80;
long PTR_null_ARRAY_0062fae0;
ulong
FUN_004029bd (int iParm1)
{
  char cVar1;
  byte bVar2;
  int iVar3;
  undefined8 uVar4;
  undefined8 uVar5;
  undefined8 *puStack88;
  uint uStack60;

  if (iParm1 == 0)
    {
      func_0x00401940 ("Usage: %s EXPRESSION\n  or:  %s OPTION\n",
		       DAT_0062fc28, DAT_0062fc28);
      func_0x00401d10 (10);
      func_0x00401b60 ("      --help     display this help and exit\n",
		       DAT_0062fb80);
      func_0x00401b60
	("      --version  output version information and exit\n",
	 DAT_0062fb80);
      func_0x00401b60
	("\nPrint the value of EXPRESSION to standard output.  A blank line below\nseparates increasing precedence groups.  EXPRESSION may be:\n\n  ARG1 | ARG2       ARG1 if it is neither null nor 0, otherwise ARG2\n\n  ARG1 & ARG2       ARG1 if neither argument is null or 0, otherwise 0\n",
	 DAT_0062fb80);
      func_0x00401b60
	("\n  ARG1 < ARG2       ARG1 is less than ARG2\n  ARG1 <= ARG2      ARG1 is less than or equal to ARG2\n  ARG1 = ARG2       ARG1 is equal to ARG2\n  ARG1 != ARG2      ARG1 is unequal to ARG2\n  ARG1 >= ARG2      ARG1 is greater than or equal to ARG2\n  ARG1 > ARG2       ARG1 is greater than ARG2\n",
	 DAT_0062fb80);
      func_0x00401b60
	("\n  ARG1 + ARG2       arithmetic sum of ARG1 and ARG2\n  ARG1 - ARG2       arithmetic difference of ARG1 and ARG2\n",
	 DAT_0062fb80);
      func_0x00401b60
	("\n  ARG1 * ARG2       arithmetic product of ARG1 and ARG2\n  ARG1 / ARG2       arithmetic quotient of ARG1 divided by ARG2\n  ARG1 % ARG2       arithmetic remainder of ARG1 divided by ARG2\n",
	 DAT_0062fb80);
      func_0x00401b60
	("\n  STRING : REGEXP   anchored pattern match of REGEXP in STRING\n\n  match STRING REGEXP        same as STRING : REGEXP\n  substr STRING POS LENGTH   substring of STRING, POS counted from 1\n  index STRING CHARS         index in STRING where any CHARS is found, or 0\n  length STRING              length of STRING\n",
	 DAT_0062fb80);
      func_0x00401b60
	("  + TOKEN                    interpret TOKEN as a string, even if it is a\n                               keyword like \'match\' or an operator like \'/\'\n\n  ( EXPRESSION )             value of EXPRESSION\n",
	 DAT_0062fb80);
      func_0x00401b60
	("\nBeware that many operators need to be escaped or quoted for shells.\nComparisons are arithmetic if both ARGs are numbers, else lexicographical.\nPattern matches return the string matched between \\( and \\) or null; if\n\\( and \\) are not used, they return the number of characters matched or 0.\n",
	 DAT_0062fb80);
      func_0x00401b60
	("\nExit status is 0 if EXPRESSION is neither null nor 0, 1 if EXPRESSION is null\nor 0, 2 if EXPRESSION is syntactically invalid, and 3 if an error occurred.\n",
	 DAT_0062fb80);
      imperfection_wrapper ();	//    FUN_00402036();
    }
  else
    {
      func_0x00401b50 (DAT_0062fba0,
		       "Try \'%s --help\' for more information.\n",
		       DAT_0062fc28);
    }
  cVar1 = (char) iParm1;
  func_0x00401d50 ();
  puStack88 = (undefined8 *) & DAT_00000022;
  uStack60 = 3;
  imperfection_wrapper ();	//  FUN_00406c9a(3, 0x22, &DAT_00429db9, (ulong)(uint)(int)cVar1);
  FUN_004044b7 (*puStack88);
  func_0x00401d00 (6, &DAT_0042957b);
  FUN_0040201e (3);
  func_0x00401cd0 (FUN_00403de2);
  imperfection_wrapper ();	//  FUN_00403ff9((ulong)uStack60, puStack88, &DAT_00429db4, "GNU coreutils", &DAT_00429dbc, FUN_004029bd, "Mike Parker", "James Youngman", "Paul Eggert", 0);
  if (1 < uStack60)
    {
      iVar3 = func_0x00401c90 (puStack88[1], &DAT_00429de8);
      if (iVar3 == 0)
	{
	  uStack60 = uStack60 - 1;
	  puStack88 = puStack88 + 1;
	}
    }
  if (uStack60 < 2)
    {
      imperfection_wrapper ();	//    FUN_00406c9a(0, 0, "missing operand");
      FUN_004029bd (2);
    }
  DAT_0062fc10 = puStack88 + 1;
  uVar4 = FUN_00403d13 (1);
  cVar1 = FUN_0040305f ();
  if (cVar1 != '\x01')
    {
      imperfection_wrapper ();	//    uVar5 = FUN_00405870(0, 8, *DAT_0062fc10);
      imperfection_wrapper ();	//    FUN_00406c9a(2, 0, "syntax error: unexpected argument %s", uVar5);
    }
  FUN_00402d45 (uVar4);
  bVar2 = FUN_00402da3 (uVar4);
  return (ulong) bVar2;
}

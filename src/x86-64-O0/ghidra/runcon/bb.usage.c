
#include "runcon.h"

long null_ARRAY_006143f_0_8_;
long null_ARRAY_006143f_8_8_;
long null_ARRAY_0061454_0_8_;
long null_ARRAY_0061454_16_8_;
long null_ARRAY_0061454_24_8_;
long null_ARRAY_0061454_32_8_;
long null_ARRAY_0061454_40_8_;
long null_ARRAY_0061454_48_8_;
long null_ARRAY_0061454_8_8_;
long null_ARRAY_0061468_0_4_;
long null_ARRAY_0061468_16_8_;
long null_ARRAY_0061468_4_4_;
long null_ARRAY_0061468_8_4_;
long DAT_0041148b;
long DAT_00411545;
long DAT_004115c3;
long DAT_00411ad8;
long DAT_00411b20;
long DAT_00411c5e;
long DAT_00411c62;
long DAT_00411c67;
long DAT_00411c69;
long DAT_004122d3;
long DAT_004126a0;
long DAT_004126b8;
long DAT_004126bd;
long DAT_00412727;
long DAT_004127b8;
long DAT_004127bb;
long DAT_00412809;
long DAT_0041280d;
long DAT_00412880;
long DAT_00412881;
long DAT_00614000;
long DAT_00614010;
long DAT_00614020;
long DAT_006143c8;
long DAT_006143e0;
long DAT_00614458;
long DAT_0061445c;
long DAT_00614460;
long DAT_00614480;
long DAT_00614490;
long DAT_006144a0;
long DAT_006144a8;
long DAT_006144c0;
long DAT_006144c8;
long DAT_00614510;
long DAT_00614518;
long DAT_00614520;
long DAT_006146b8;
long DAT_006146c0;
long DAT_006146c8;
long DAT_006146d8;
long fde_004134d8;
long FLOAT_UNKNOWN;
long null_ARRAY_00411640;
long null_ARRAY_00412cc0;
long null_ARRAY_006143f0;
long null_ARRAY_006144e0;
long null_ARRAY_00614540;
long null_ARRAY_00614580;
long null_ARRAY_00614680;
long PTR_DAT_006143c0;
long PTR_null_ARRAY_00614400;
long stack0x00000008;
undefined8
FUN_00401b94 (uint uParm1)
{
  int iVar1;
  uint *puVar2;
  undefined8 uVar3;
  char *pcVar4;
  uint uVar5;
  undefined4 uStack116;
  undefined8 uStack112;
  undefined8 uStack104;
  undefined8 uStack96;
  int iStack84;
  undefined8 uStack80;
  char cStack65;
  long lStack64;
  long lStack56;
  long lStack48;
  long lStack40;
  code *pcStack32;
  uint local_c;

  if (uParm1 == 0)
    {
      pcStack32 = (code *) 0x401be7;
      local_c = uParm1;
      func_0x00401450
	("Usage: %s CONTEXT COMMAND [args]\n  or:  %s [ -c ] [-u USER] [-r ROLE] [-t TYPE] [-l RANGE] COMMAND [args]\n",
	 DAT_00614520, DAT_00614520);
      pcStack32 = (code *) 0x401bfb;
      func_0x004015d0
	("Run a program in a different SELinux security context.\nWith neither CONTEXT nor COMMAND, print the current security context.\n",
	 DAT_00614480);
      pcStack32 = (code *) 0x401c00;
      FUN_004019de ();
      pcStack32 = (code *) 0x401c14;
      func_0x004015d0
	("  CONTEXT            Complete security context\n  -c, --compute      compute process transition context before modifying\n  -t, --type=TYPE    type (for same role as parent)\n  -u, --user=USER    user identity\n  -r, --role=ROLE    role\n  -l, --range=RANGE  levelrange\n\n",
	 DAT_00614480);
      pcStack32 = (code *) 0x401c28;
      func_0x004015d0 ("      --help     display this help and exit\n",
		       DAT_00614480);
      pcStack32 = (code *) 0x401c3c;
      pcVar4 = (char *) DAT_00614480;
      func_0x004015d0
	("      --version  output version information and exit\n");
      pcStack32 = (code *) 0x401c46;
      imperfection_wrapper ();	//    FUN_004019f8();
    }
  else
    {
      pcVar4 = "Try \'%s --help\' for more information.\n";
      pcStack32 = (code *) 0x401bc5;
      local_c = uParm1;
      func_0x004015c0 (DAT_006144a0,
		       "Try \'%s --help\' for more information.\n",
		       DAT_00614520);
    }
  pcStack32 = FUN_00401c50;
  uVar5 = local_c;
  func_0x00401740 ();
  lStack40 = 0;
  lStack48 = 0;
  lStack56 = 0;
  lStack64 = 0;
  uStack80 = 0;
  uStack96 = 0;
  uStack104 = 0;
  uStack112 = 0;
  cStack65 = '\0';
  pcStack32 = (code *) & stack0xfffffffffffffff8;
  FUN_00402085 (*(undefined8 *) pcVar4);
  func_0x00401700 (6, &DAT_004115c3);
  func_0x004016e0 (FUN_00401fa0);
LAB_00401ccb:
  ;
  while (true)
    {
      while (true)
	{
	  uStack116 = 0;
	  imperfection_wrapper ();	//      iStack84 = FUN_00404eb6((ulong)uVar5, pcVar4, "+r:t:u:l:c", null_ARRAY_00411640, &uStack116);
	  if (iStack84 == -1)
	    {
	      if (uVar5 == DAT_00614458)
		{
		  imperfection_wrapper ();	//          iVar1 = FUN_004034a4(&uStack96);
		  if (iVar1 < 0)
		    {
		      puVar2 = (uint *) func_0x00401730 ();
		      imperfection_wrapper ();	//            FUN_00403cdd(1, (ulong)*puVar2, "failed to get current context");
		    }
		  func_0x004015d0 (uStack96, DAT_00614480, DAT_00614480);
		  func_0x004017e0 (10, DAT_00614480);
		  uVar3 = 0;
		}
	      else
		{
		  if ((((lStack56 == 0) && (lStack40 == 0))
		       && (lStack64 == 0)) && ((lStack48 == 0
						&& (cStack65 != '\x01'))))
		    {
		      if ((int) uVar5 <= (int) DAT_00614458)
			{
			  imperfection_wrapper ();	//              FUN_00403cdd(0, 0, "you must specify -c, -t, -u, -l, -r, or context");
			  FUN_00401b94 (1);
			}
		      uStack80 =
			((undefined8 *) pcVar4)[(long) (int) DAT_00614458];
		      DAT_00614458 = DAT_00614458 + 1;
		    }
		  if ((int) uVar5 <= (int) DAT_00614458)
		    {
		      imperfection_wrapper ();	//            FUN_00403cdd(0, 0, "no command specified");
		      FUN_00401b94 (1);
		    }
		  imperfection_wrapper ();	//          uVar3 = FUN_00403cdd(1, 0, "%s may be used only on a SELinux kernel", DAT_00614520);
		}
	      return uVar3;
	    }
	  if (iStack84 != 0x6c)
	    break;
	  if (lStack48 != 0)
	    {
	      imperfection_wrapper ();	//        FUN_00403cdd(1, 0, "multiple levelranges");
	    }
	  lStack48 = DAT_006146d8;
	}
      if (iStack84 < 0x6d)
	break;
      if (iStack84 == 0x74)
	{
	  if (lStack64 != 0)
	    {
	      imperfection_wrapper ();	//        FUN_00403cdd(1, 0, "multiple types");
	    }
	  lStack64 = DAT_006146d8;
	}
      else
	{
	  if (iStack84 == 0x75)
	    {
	      if (lStack56 != 0)
		{
		  imperfection_wrapper ();	//          FUN_00403cdd(1, 0, "multiple users");
		}
	      lStack56 = DAT_006146d8;
	    }
	  else
	    {
	      if (iStack84 != 0x72)
		goto LAB_00401e61;
	      if (lStack40 != 0)
		{
		  imperfection_wrapper ();	//          FUN_00403cdd(1, 0, "multiple roles");
		}
	      lStack40 = DAT_006146d8;
	    }
	}
    }
  if (iStack84 == -0x82)
    {
      FUN_00401b94 (0);
    }
  else
    {
      if (iStack84 == 99)
	{
	  cStack65 = '\x01';
	  goto LAB_00401ccb;
	}
      if (iStack84 != -0x83)
	goto LAB_00401e61;
    }
  imperfection_wrapper ();	//  FUN_004039f8(DAT_00614480, "runcon", "GNU coreutils", PTR_DAT_006143c0, "Russell Coker", 0);
  func_0x00401740 (0);
LAB_00401e61:
  ;
  imperfection_wrapper ();	//  FUN_00401b94();
  goto LAB_00401ccb;
}

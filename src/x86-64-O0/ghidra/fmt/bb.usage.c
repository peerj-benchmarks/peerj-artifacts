
#include "fmt.h"

long null_ARRAY_00616a3_0_8_;
long null_ARRAY_00616a3_8_8_;
long null_ARRAY_00617f8_32_8_;
long null_ARRAY_00621c0_0_8_;
long null_ARRAY_00621c0_16_8_;
long null_ARRAY_00621c0_24_8_;
long null_ARRAY_00621c0_32_8_;
long null_ARRAY_00621c0_40_8_;
long null_ARRAY_00621c0_48_8_;
long null_ARRAY_00621c0_8_8_;
long null_ARRAY_00621d4_0_4_;
long null_ARRAY_00621d4_16_8_;
long null_ARRAY_00621d4_4_4_;
long null_ARRAY_00621d4_8_4_;
long DAT_00413203;
long DAT_004132bd;
long DAT_0041333b;
long DAT_004136f6;
long DAT_0041394f;
long DAT_00413951;
long DAT_00413953;
long DAT_00413993;
long DAT_00413998;
long DAT_004139c2;
long DAT_00413a08;
long DAT_00413b5e;
long DAT_00413b62;
long DAT_00413b67;
long DAT_00413b69;
long DAT_004141d3;
long DAT_004145a0;
long DAT_00414938;
long DAT_0041493d;
long DAT_004149a7;
long DAT_00414a38;
long DAT_00414a3b;
long DAT_00414a89;
long DAT_00414a8d;
long DAT_00414b00;
long DAT_00414b01;
long DAT_00616618;
long DAT_00616628;
long DAT_00616638;
long DAT_00616a08;
long DAT_00616a20;
long DAT_00616a98;
long DAT_00616a9c;
long DAT_00616aa0;
long DAT_00616ac0;
long DAT_00616ac8;
long DAT_00616ad0;
long DAT_00616ae0;
long DAT_00616ae8;
long DAT_00616b00;
long DAT_00616b08;
long DAT_00616b80;
long DAT_00616b81;
long DAT_00616b82;
long DAT_00616b83;
long DAT_00616b88;
long DAT_00616b90;
long DAT_00616b94;
long DAT_00616b98;
long DAT_00616b9c;
long DAT_00616ba0;
long DAT_00616ba4;
long DAT_00616ba8;
long DAT_00617f48;
long DAT_00621bc0;
long DAT_00621bc8;
long DAT_00621bcc;
long DAT_00621bd0;
long DAT_00621bd4;
long DAT_00621bd8;
long DAT_00621bdc;
long DAT_00621be0;
long DAT_00621be8;
long DAT_00621bf0;
long DAT_00621bf8;
long DAT_00621d78;
long DAT_00621d80;
long DAT_00621d88;
long DAT_00621d98;
long fde_00415868;
long FLOAT_UNKNOWN;
long null_ARRAY_00413780;
long null_ARRAY_00414f40;
long null_ARRAY_00616a30;
long null_ARRAY_00616a60;
long null_ARRAY_00616b20;
long null_ARRAY_00616bc0;
long null_ARRAY_00617f80;
long null_ARRAY_00621c00;
long null_ARRAY_00621c40;
long null_ARRAY_00621d40;
long PTR_DAT_00616a00;
long PTR_null_ARRAY_00616a40;
long stack0x00000008;
ulong
FUN_00401e44 (uint uParm1)
{
  bool bVar1;
  undefined uVar2;
  undefined uVar3;
  undefined uVar4;
  uint uVar5;
  int iVar6;
  long lVar7;
  undefined8 uVar8;
  uint *puVar9;
  char *pcStack112;
  uint uStack100;
  long lStack72;
  long lStack64;

  if (uParm1 == 0)
    {
      func_0x00401650 ("Usage: %s [-WIDTH] [OPTION]... [FILE]...\n",
		       DAT_00621bf8);
      func_0x00401800
	("Reformat each paragraph in the FILE(s), writing to standard output.\nThe option -WIDTH is an abbreviated form of --width=DIGITS.\n",
	 DAT_00616ac0);
      FUN_00401c74 ();
      FUN_00401c8e ();
      func_0x00401800
	("  -c, --crown-margin        preserve indentation of first two lines\n  -p, --prefix=STRING       reformat only lines beginning with STRING,\n                              reattaching the prefix to reformatted lines\n  -s, --split-only          split long lines, but do not refill\n",
	 DAT_00616ac0);
      func_0x00401800
	("  -t, --tagged-paragraph    indentation of first line different from second\n  -u, --uniform-spacing     one space between words, two after sentences\n  -w, --width=WIDTH         maximum line width (default of 75 columns)\n  -g, --goal=WIDTH          goal width (default of 93% of width)\n",
	 DAT_00616ac0);
      func_0x00401800 ("      --help     display this help and exit\n",
		       DAT_00616ac0);
      pcStack112 = (char *) DAT_00616ac0;
      func_0x00401800
	("      --version  output version information and exit\n");
      imperfection_wrapper ();	//    FUN_00401ca8();
    }
  else
    {
      pcStack112 = "Try \'%s --help\' for more information.\n";
      func_0x004017f0 (DAT_00616ae0,
		       "Try \'%s --help\' for more information.\n",
		       DAT_00621bf8);
    }
  func_0x004019c0 ();
  bVar1 = true;
  lStack64 = 0;
  lStack72 = 0;
  FUN_004035cc (*(undefined8 *) pcStack112);
  func_0x00401960 (6, &DAT_0041333b);
  func_0x00401940 (FUN_0040347d);
  DAT_00616b83 = 0;
  DAT_00616b82 = 0;
  DAT_00616b81 = 0;
  DAT_00616b80 = 0;
  DAT_00616b90 = 0x4b;
  DAT_00616b88 = &DAT_0041333b;
  DAT_00616b94 = 0;
  DAT_00616b98 = 0;
  DAT_00616b9c = 0;
  uVar2 = 0;
  uVar3 = 0;
  uVar4 = 0;
  uStack100 = uParm1;
  if (((1 < (int) uParm1)
       && (uVar2 = DAT_00616b80, uVar3 = DAT_00616b81, uVar4 =
	   DAT_00616b82, uStack100 =
	   uParm1, *(char *) ((undefined8 *) pcStack112)[1] == '-'))
      && (uVar2 = DAT_00616b80, uVar3 = DAT_00616b81, uVar4 =
	  DAT_00616b82, uStack100 =
	  uParm1,
	  (int) *(char *) (((undefined8 *) pcStack112)[1] + 1) - 0x30U < 10))
    {
      lStack64 = ((undefined8 *) pcStack112)[1] + 1;
      DAT_00616b80 = 0;
      DAT_00616b81 = 0;
      DAT_00616b82 = 0;
      ((undefined8 *) pcStack112)[1] = *(undefined8 *) pcStack112;
      pcStack112 = (char *) ((undefined8 *) pcStack112 + 1);
      uStack100 = uParm1 - 1;
      uVar2 = DAT_00616b80;
      uVar3 = DAT_00616b81;
      uVar4 = DAT_00616b82;
    }
LAB_00402159:
  ;
  while (true)
    {
      DAT_00616b82 = uVar4;
      DAT_00616b81 = uVar3;
      DAT_00616b80 = uVar2;
      imperfection_wrapper ();	//    uVar5 = FUN_00406ddc((ulong)uStack100, pcStack112, "0123456789cstuw:p:g:", null_ARRAY_00413780, 0);
      if (uVar5 == 0xffffffff)
	{
	  if (lStack64 != 0)
	    {
	      imperfection_wrapper ();	//        DAT_00616b90 = FUN_004053ae(lStack64, 0, 0x9c4, &DAT_0041333b, "invalid width", 0);
	    }
	  if (lStack72 == 0)
	    {
	      DAT_00616ba0 = (DAT_00616b90 * 0xbb) / 200;
	    }
	  else
	    {
	      imperfection_wrapper ();	//        DAT_00616ba0 = FUN_004053ae(lStack72, 0, (long)DAT_00616b90, &DAT_0041333b, "invalid width", 0);
	      if (lStack64 == 0)
		{
		  DAT_00616b90 = DAT_00616ba0 + 10;
		}
	    }
	  if (DAT_00616a98 == uStack100)
	    {
	      FUN_00402426 (DAT_00616ac8);
	    }
	  else
	    {
	      while ((int) DAT_00616a98 < (int) uStack100)
		{
		  uVar8 =
		    ((undefined8 *) pcStack112)[(long) (int) DAT_00616a98];
		  iVar6 = func_0x00401900 (uVar8, &DAT_0041394f);
		  if (iVar6 == 0)
		    {
		      FUN_00402426 (DAT_00616ac8);
		    }
		  else
		    {
		      lVar7 = func_0x004018c0 (uVar8, &DAT_00413951);
		      if (lVar7 == 0)
			{
			  imperfection_wrapper ();	//              uVar8 = FUN_004049c6(4, uVar8);
			  puVar9 = (uint *) func_0x004019b0 ();
			  imperfection_wrapper ();	//              FUN_004059ff(0, (ulong)*puVar9, "cannot open %s for reading", uVar8);
			  bVar1 = false;
			}
		      else
			{
			  FUN_00402426 (lVar7);
			  iVar6 = FUN_00405ae3 (lVar7);
			  if (iVar6 == -1)
			    {
			      imperfection_wrapper ();	//                uVar8 = FUN_00404ac7(0, 3, uVar8);
			      puVar9 = (uint *) func_0x004019b0 ();
			      imperfection_wrapper ();	//                FUN_004059ff(0, (ulong)*puVar9, &DAT_00413953, uVar8);
			      bVar1 = false;
			    }
			}
		    }
		  DAT_00616a98 = DAT_00616a98 + 1;
		}
	    }
	  return (ulong) ! bVar1;
	}
      if (uVar5 != 0x70)
	break;
      imperfection_wrapper ();	//    FUN_00402381();
      uVar2 = DAT_00616b80;
      uVar3 = DAT_00616b81;
      uVar4 = DAT_00616b82;
    }
  if ((int) uVar5 < 0x71)
    {
      if (uVar5 == 0xffffff7e)
	{
	  FUN_00401e44 (0);
	LAB_0040211b:
	  ;
	  imperfection_wrapper ();	//      FUN_0040510c(DAT_00616ac0, &DAT_004136f6, "GNU coreutils", PTR_DAT_00616a00, "Ross Paterson", 0);
	  func_0x004019c0 ();
	  uVar2 = DAT_00616b80;
	  uVar3 = DAT_00616b81;
	  uVar4 = DAT_00616b82;
	  goto LAB_00402159;
	}
      if (-0x82 < (int) uVar5)
	{
	  if (uVar5 != 99)
	    {
	      if (uVar5 != 0x67)
		goto LAB_00402089;
	      lStack72 = DAT_00621d98;
	      uVar2 = DAT_00616b80;
	      uVar3 = DAT_00616b81;
	      uVar4 = DAT_00616b82;
	      goto LAB_00402159;
	    }
	  goto LAB_004020bc;
	}
      if (uVar5 == 0xffffff7d)
	goto LAB_0040211b;
    }
  else
    {
      if (uVar5 == 0x74)
	{
	  DAT_00616b81 = 1;
	  uVar2 = DAT_00616b80;
	  uVar3 = DAT_00616b81;
	  uVar4 = DAT_00616b82;
	  goto LAB_00402159;
	}
      if ((int) uVar5 < 0x75)
	{
	  if (uVar5 != 0x73)
	    goto LAB_00402089;
	  DAT_00616b82 = 1;
	  uVar2 = DAT_00616b80;
	  uVar3 = DAT_00616b81;
	  uVar4 = DAT_00616b82;
	  goto LAB_00402159;
	}
      if (uVar5 == 0x75)
	{
	  DAT_00616b83 = 1;
	  uVar2 = DAT_00616b80;
	  uVar3 = DAT_00616b81;
	  uVar4 = DAT_00616b82;
	  goto LAB_00402159;
	}
      if (uVar5 == 0x77)
	{
	  lStack64 = DAT_00621d98;
	  uVar2 = DAT_00616b80;
	  uVar3 = DAT_00616b81;
	  uVar4 = DAT_00616b82;
	  goto LAB_00402159;
	}
    }
LAB_00402089:
  ;
  if (uVar5 - 0x30 < 10)
    {
      imperfection_wrapper ();	//    FUN_004059ff(0, 0, "invalid option -- %c; -WIDTH is recognized only when it is the first\noption; use -w N instead", (ulong)uVar5);
    }
  imperfection_wrapper ();	//  FUN_00401e44();
LAB_004020bc:
  ;
  DAT_00616b80 = 1;
  uVar2 = DAT_00616b80;
  uVar3 = DAT_00616b81;
  uVar4 = DAT_00616b82;
  goto LAB_00402159;
}

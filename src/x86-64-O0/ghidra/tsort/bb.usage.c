
#include "tsort.h"

long null_ARRAY_0061565_0_8_;
long null_ARRAY_0061565_8_8_;
long null_ARRAY_0061580_0_8_;
long null_ARRAY_0061580_16_8_;
long null_ARRAY_0061580_24_8_;
long null_ARRAY_0061580_32_8_;
long null_ARRAY_0061580_40_8_;
long null_ARRAY_0061580_48_8_;
long null_ARRAY_0061580_8_8_;
long null_ARRAY_0061594_0_4_;
long null_ARRAY_0061594_16_8_;
long null_ARRAY_0061594_4_4_;
long null_ARRAY_0061594_8_4_;
long DAT_004126b8;
long DAT_00412775;
long DAT_004127f3;
long DAT_004128ec;
long DAT_00412968;
long DAT_00412997;
long DAT_0041299a;
long DAT_0041299c;
long DAT_0041299e;
long DAT_00412a4e;
long DAT_00412b20;
long DAT_00412b68;
long DAT_00412c9e;
long DAT_00412ca2;
long DAT_00412ca7;
long DAT_00412ca9;
long DAT_00413313;
long DAT_004136e0;
long DAT_004136f8;
long DAT_004136fd;
long DAT_00413767;
long DAT_004137f8;
long DAT_004137fb;
long DAT_00413849;
long DAT_0041384d;
long DAT_004138c0;
long DAT_004138c1;
long DAT_00413aa8;
long DAT_00615240;
long DAT_00615250;
long DAT_00615260;
long DAT_00615628;
long DAT_00615640;
long DAT_006156b8;
long DAT_006156bc;
long DAT_006156c0;
long DAT_00615700;
long DAT_00615708;
long DAT_00615710;
long DAT_00615720;
long DAT_00615728;
long DAT_00615740;
long DAT_00615748;
long DAT_00615790;
long DAT_00615798;
long DAT_006157a0;
long DAT_006157a8;
long DAT_006157b0;
long DAT_006157b8;
long DAT_006157c0;
long DAT_00615978;
long DAT_00615980;
long DAT_00615988;
long DAT_00615998;
long fde_004145d8;
long FLOAT_UNKNOWN;
long null_ARRAY_00412840;
long null_ARRAY_00412ac0;
long null_ARRAY_00413d00;
long null_ARRAY_00615650;
long null_ARRAY_00615680;
long null_ARRAY_00615760;
long null_ARRAY_00615800;
long null_ARRAY_00615840;
long null_ARRAY_00615940;
long PTR_DAT_00615620;
long PTR_null_ARRAY_00615660;
undefined8 *
FUN_00401d04 (uint uParm1)
{
  undefined8 *puVar1;
  undefined8 uVar2;
  ulong uVar3;

  if (uParm1 == 0)
    {
      func_0x00401580
	("Usage: %s [OPTION] [FILE]\nWrite totally ordered list consistent with the partial ordering in FILE.\n",
	 DAT_006157c0);
      FUN_00401b4e ();
      func_0x00401720 (&DAT_004128ec, DAT_00615700);
      func_0x00401720 ("      --help     display this help and exit\n",
		       DAT_00615700);
      func_0x00401720
	("      --version  output version information and exit\n",
	 DAT_00615700);
      imperfection_wrapper ();	//    FUN_00401b68();
    }
  else
    {
      func_0x00401710 (DAT_00615720,
		       "Try \'%s --help\' for more information.\n",
		       DAT_006157c0);
    }
  uVar3 = (ulong) uParm1;
  func_0x004018c0 ();
  puVar1 = (undefined8 *) FUN_00404c94 (0x38);
  if (uVar3 == 0)
    {
      uVar2 = 0;
    }
  else
    {
      imperfection_wrapper ();	//    uVar2 = FUN_00404d5e(uVar3);
    }
  *puVar1 = uVar2;
  puVar1[2] = 0;
  puVar1[1] = puVar1[2];
  *(undefined4 *) (puVar1 + 3) = 0;
  puVar1[4] = 0;
  puVar1[5] = 0;
  puVar1[6] = 0;
  return puVar1;
}


#include "chmod.h"

long null_ARRAY_0061c47_0_8_;
long null_ARRAY_0061c47_8_8_;
long null_ARRAY_0061c60_0_8_;
long null_ARRAY_0061c60_16_8_;
long null_ARRAY_0061c60_24_8_;
long null_ARRAY_0061c60_32_8_;
long null_ARRAY_0061c60_40_8_;
long null_ARRAY_0061c60_48_8_;
long null_ARRAY_0061c60_8_8_;
long null_ARRAY_0061c74_0_4_;
long null_ARRAY_0061c74_16_8_;
long null_ARRAY_0061c74_4_4_;
long null_ARRAY_0061c74_8_4_;
long DAT_00417b40;
long DAT_00417bfd;
long DAT_00417c7b;
long DAT_00417fa4;
long DAT_004180a2;
long DAT_004186e8;
long DAT_00418838;
long DAT_0041895e;
long DAT_00418962;
long DAT_00418967;
long DAT_00418969;
long DAT_00418fc0;
long DAT_00418fdb;
long DAT_004193a0;
long DAT_004193dd;
long DAT_004193e2;
long DAT_004193f8;
long DAT_004193f9;
long DAT_004193fb;
long DAT_004194bf;
long DAT_00419550;
long DAT_00419553;
long DAT_004195a1;
long DAT_004195db;
long DAT_00419708;
long DAT_00419709;
long DAT_004198e8;
long DAT_0061c000;
long DAT_0061c010;
long DAT_0061c020;
long DAT_0061c440;
long DAT_0061c450;
long DAT_0061c460;
long DAT_0061c4d8;
long DAT_0061c4dc;
long DAT_0061c4e0;
long DAT_0061c500;
long DAT_0061c510;
long DAT_0061c520;
long DAT_0061c528;
long DAT_0061c540;
long DAT_0061c548;
long DAT_0061c590;
long DAT_0061c598;
long DAT_0061c59c;
long DAT_0061c59d;
long DAT_0061c59e;
long DAT_0061c5a0;
long DAT_0061c5c0;
long DAT_0061c5c8;
long DAT_0061c5d0;
long DAT_0061c778;
long DAT_0061c780;
long DAT_0061c784;
long DAT_0061c788;
long DAT_0061c790;
long DAT_0061c7a0;
long fde_0041a678;
long FLOAT_UNKNOWN;
long null_ARRAY_00417d40;
long null_ARRAY_004195f0;
long null_ARRAY_00419b40;
long null_ARRAY_0061c470;
long null_ARRAY_0061c4a0;
long null_ARRAY_0061c560;
long null_ARRAY_0061c5b0;
long null_ARRAY_0061c600;
long null_ARRAY_0061c640;
long null_ARRAY_0061c740;
long PTR_DAT_0061c448;
long PTR_null_ARRAY_0061c480;
long stack0x00000008;
ulong
FUN_004028eb (uint uParm1)
{
  char cVar1;
  uint *puVar2;
  char *pcVar3;
  uint uVar4;
  undefined8 uVar5;
  ulong auStack136[2];
  ulong uStack120;
  long lStack112;
  long lStack104;
  undefined8 uStack96;
  int iStack84;
  long lStack80;
  char cStack65;
  ulong uStack64;
  long lStack56;
  code *pcStack32;
  uint local_c;

  if (uParm1 == 0)
    {
      pcStack32 = (code *) 0x402948;
      local_c = uParm1;
      func_0x00401810
	("Usage: %s [OPTION]... MODE[,MODE]... FILE...\n  or:  %s [OPTION]... OCTAL-MODE FILE...\n  or:  %s [OPTION]... --reference=RFILE FILE...\n",
	 DAT_0061c5d0, DAT_0061c5d0, DAT_0061c5d0);
      pcStack32 = (code *) 0x40295c;
      func_0x00401a10
	("Change the mode of each FILE to MODE.\nWith --reference, change the mode of each FILE to that of RFILE.\n\n",
	 DAT_0061c500);
      pcStack32 = (code *) 0x402970;
      func_0x00401a10
	("  -c, --changes          like verbose but report only when a change is made\n  -f, --silent, --quiet  suppress most error messages\n  -v, --verbose          output a diagnostic for every file processed\n",
	 DAT_0061c500);
      pcStack32 = (code *) 0x402984;
      func_0x00401a10
	("      --no-preserve-root  do not treat \'/\' specially (the default)\n      --preserve-root    fail to operate recursively on \'/\'\n",
	 DAT_0061c500);
      pcStack32 = (code *) 0x402998;
      func_0x00401a10
	("      --reference=RFILE  use RFILE\'s mode instead of MODE values\n",
	 DAT_0061c500);
      pcStack32 = (code *) 0x4029ac;
      func_0x00401a10
	("  -R, --recursive        change files and directories recursively\n",
	 DAT_0061c500);
      pcStack32 = (code *) 0x4029c0;
      func_0x00401a10 ("      --help     display this help and exit\n",
		       DAT_0061c500);
      pcStack32 = (code *) 0x4029d4;
      func_0x00401a10
	("      --version  output version information and exit\n",
	 DAT_0061c500);
      pcStack32 = (code *) 0x4029e8;
      pcVar3 = (char *) DAT_0061c500;
      func_0x00401a10
	("\nEach MODE is of the form \'[ugoa]*([-+=]([rwxXst]*|[ugo]))+|[-+=][0-7]+\'.\n");
      pcStack32 = (code *) 0x4029f2;
      imperfection_wrapper ();	//    FUN_00401e8e();
    }
  else
    {
      pcVar3 = "Try \'%s --help\' for more information.\n";
      pcStack32 = (code *) 0x40291c;
      local_c = uParm1;
      func_0x00401a00 (DAT_0061c520,
		       "Try \'%s --help\' for more information.\n",
		       DAT_0061c5d0);
    }
  pcStack32 = FUN_004029fc;
  uVar4 = local_c;
  func_0x00401be0 ();
  lStack56 = 0;
  uStack64 = 0;
  auStack136[0] = 0;
  cStack65 = '\0';
  lStack80 = 0;
  pcStack32 = (code *) & stack0xfffffffffffffff8;
  FUN_0040395d (*(undefined8 *) pcVar3);
  func_0x00401b90 (6, &DAT_00417c7b);
  func_0x00401b70 (FUN_00402f60);
  DAT_0061c59e = 0;
  DAT_0061c59d = 0;
  DAT_0061c59c = '\0';
LAB_00402cd5:
  ;
  do
    {
      uVar5 = 0x402cf3;
      imperfection_wrapper ();	//    iStack84 = FUN_0040966f((ulong)uVar4, pcVar3, "Rcfvr::w::x::X::s::t::u::g::o::a::,::+::=::0::1::2::3::4::5::6::7::", null_ARRAY_00417d40, 0);
      if (iStack84 == -1)
	break;
      if (iStack84 != 99)
	{
	  if (iStack84 < 100)
	    {
	      if (iStack84 < 0x38)
		{
		  if (0x2f < iStack84)
		    goto LAB_00402b69;
		  if (iStack84 == -0x82)
		    {
		      uVar5 = 0x402c83;
		      FUN_004028eb (0);
		    LAB_00402c83:
		      ;
		      imperfection_wrapper ();	//            FUN_004054fa(DAT_0061c500, "chmod", "GNU coreutils", PTR_DAT_0061c448, "David MacKenzie", "Jim Meyering", 0, uVar5);
		      func_0x00401be0 (0);
		    }
		  else
		    {
		      if (iStack84 < -0x81)
			{
			  if (iStack84 == -0x83)
			    goto LAB_00402c83;
			}
		      else
			{
			  if (iStack84 - 0x2bU < 2)
			    goto LAB_00402b69;
			}
		    }
		  goto LAB_00402ccb;
		}
	      if (iStack84 == 0x52)
		{
		  DAT_0061c59c = '\x01';
		  goto LAB_00402cd5;
		}
	      if (iStack84 < 0x53)
		{
		  if (iStack84 == 0x3d)
		    {
		    LAB_00402b69:
		      ;
		      uStack96 =
			((undefined8 *) pcVar3)[(long) DAT_0061c4d8 + -1];
		      lStack104 = func_0x00401c50 (uStack96);
		      lStack112 = uStack64 + (ulong) (uStack64 != 0);
		      uStack120 = lStack104 + lStack112;
		      if (auStack136[0] <= uStack120)
			{
			  auStack136[0] = uStack120 + 1;
			  imperfection_wrapper ();	//              lStack56 = FUN_0040576c(lStack56, auStack136, auStack136);
			}
		      *(undefined *) (uStack64 + lStack56) = 0x2c;
		      func_0x004018f0 (lStack56 + lStack112, uStack96,
				       lStack104 + 1);
		      uStack64 = uStack120;
		      DAT_0061c59e = 1;
		      goto LAB_00402cd5;
		    }
		}
	      else
		{
		  if ((iStack84 == 0x58) || (iStack84 == 0x61))
		    goto LAB_00402b69;
		}
	    }
	  else
	    {
	      if (iStack84 == 0x76)
		{
		  DAT_0061c440 = 0;
		  goto LAB_00402cd5;
		}
	      if (iStack84 < 0x77)
		{
		  if (iStack84 == 0x6f)
		    goto LAB_00402b69;
		  if (iStack84 < 0x70)
		    {
		      if (iStack84 != 0x66)
			{
			  if (iStack84 == 0x67)
			    goto LAB_00402b69;
			  goto LAB_00402ccb;
			}
		      DAT_0061c59d = 1;
		      goto LAB_00402cd5;
		    }
		  if (0x71 < iStack84)
		    goto LAB_00402b69;
		}
	      else
		{
		  if (iStack84 == 0x80)
		    {
		      cStack65 = '\0';
		      goto LAB_00402cd5;
		    }
		  if (iStack84 < 0x81)
		    {
		      if (iStack84 < 0x79)
			goto LAB_00402b69;
		    }
		  else
		    {
		      if (iStack84 == 0x81)
			{
			  cStack65 = '\x01';
			  goto LAB_00402cd5;
			}
		      if (iStack84 == 0x82)
			{
			  lStack80 = DAT_0061c7a0;
			  goto LAB_00402cd5;
			}
		    }
		}
	    }
	LAB_00402ccb:
	  ;
	  imperfection_wrapper ();	//      FUN_004028eb();
	  goto LAB_00402cd5;
	}
      DAT_0061c440 = 1;
    }
  while (true);
  if (lStack80 == 0)
    {
    LAB_00402d31:
      ;
      if (lStack56 == 0)
	{
	  lStack56 = ((undefined8 *) pcVar3)[(long) DAT_0061c4d8];
	  DAT_0061c4d8 = DAT_0061c4d8 + 1;
	}
    }
  else
    {
      if (lStack56 != 0)
	{
	  imperfection_wrapper ();	//      FUN_00405a61(0, 0, "cannot combine mode and --reference options");
	  FUN_004028eb (1);
	  goto LAB_00402d31;
	}
    }
  if ((int) uVar4 <= DAT_0061c4d8)
    {
      if ((lStack56 == 0)
	  || (((undefined8 *) pcVar3)[(long) DAT_0061c4d8 + -1] != lStack56))
	{
	  imperfection_wrapper ();	//      FUN_00405a61(0, 0, "missing operand");
	}
      else
	{
	  imperfection_wrapper ();	//      uVar5 = FUN_00404f48(((undefined8 *)pcVar3)[(long)(int)uVar4 + -1]);
	  imperfection_wrapper ();	//      FUN_00405a61(0, 0, "missing operand after %s", uVar5);
	}
      FUN_004028eb (1);
    }
  if (lStack80 != 0)
    {
      DAT_0061c590 = FUN_0040377d (lStack80);
      if (DAT_0061c590 != 0)
	goto LAB_00402eb4;
      imperfection_wrapper ();	//    uVar5 = FUN_00404d57(4, lStack80);
      puVar2 = (uint *) func_0x00401bd0 ();
      imperfection_wrapper ();	//    FUN_00405a61(1, (ulong)*puVar2, "failed to get attributes of %s", uVar5);
    }
  DAT_0061c590 = FUN_0040334d (lStack56);
  if (DAT_0061c590 == 0)
    {
      imperfection_wrapper ();	//    uVar5 = FUN_00404f48(lStack56);
      imperfection_wrapper ();	//    FUN_00405a61(0, 0, "invalid mode: %s", uVar5);
      FUN_004028eb (1);
    }
  DAT_0061c598 = func_0x00401a40 (0);
LAB_00402eb4:
  ;
  if ((DAT_0061c59c == '\0') || (cStack65 == '\0'))
    {
      DAT_0061c5a0 = 0;
    }
  else
    {
      DAT_0061c5a0 = FUN_00404f67 (null_ARRAY_0061c5b0);
      if (DAT_0061c5a0 == 0)
	{
	  imperfection_wrapper ();	//      uVar5 = FUN_00404d57(4, &DAT_004180a2);
	  puVar2 = (uint *) func_0x00401bd0 ();
	  imperfection_wrapper ();	//      FUN_00405a61(1, (ulong)*puVar2, "failed to get attributes of %s", uVar5);
	}
    }
  imperfection_wrapper ();	//  cVar1 = FUN_0040280c((undefined8 *)pcVar3 + (long)DAT_0061c4d8, 0x411, (long)DAT_0061c4d8 * 8);
  return (ulong) (cVar1 == '\0');
}

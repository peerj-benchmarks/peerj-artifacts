
#include "pathchk.h"

long null_ARRAY_0061541_0_8_;
long null_ARRAY_0061541_8_8_;
long null_ARRAY_0061558_0_8_;
long null_ARRAY_0061558_16_8_;
long null_ARRAY_0061558_24_8_;
long null_ARRAY_0061558_32_8_;
long null_ARRAY_0061558_40_8_;
long null_ARRAY_0061558_48_8_;
long null_ARRAY_0061558_8_8_;
long null_ARRAY_006156c_0_4_;
long null_ARRAY_006156c_16_8_;
long null_ARRAY_006156c_4_4_;
long null_ARRAY_006156c_8_4_;
long DAT_00411e00;
long DAT_00411ebd;
long DAT_00411f3b;
long DAT_00412207;
long DAT_004122d1;
long DAT_004122d4;
long DAT_004122d6;
long DAT_0041239b;
long DAT_004123e0;
long DAT_0041251e;
long DAT_00412522;
long DAT_00412527;
long DAT_00412529;
long DAT_00412b93;
long DAT_00412f60;
long DAT_00412f78;
long DAT_00412f7d;
long DAT_00412fe7;
long DAT_00413078;
long DAT_0041307b;
long DAT_004130c9;
long DAT_004130cd;
long DAT_00413140;
long DAT_00413141;
long DAT_00615000;
long DAT_00615010;
long DAT_00615020;
long DAT_006153e8;
long DAT_00615400;
long DAT_00615478;
long DAT_0061547c;
long DAT_00615480;
long DAT_006154c0;
long DAT_006154d0;
long DAT_006154e0;
long DAT_006154e8;
long DAT_00615500;
long DAT_00615508;
long DAT_00615550;
long DAT_00615558;
long DAT_00615560;
long DAT_006156f8;
long DAT_00615700;
long DAT_00615708;
long DAT_00615710;
long DAT_00615720;
long fde_00413e20;
long FLOAT_UNKNOWN;
long null_ARRAY_00411fc0;
long null_ARRAY_00413580;
long null_ARRAY_00615410;
long null_ARRAY_00615440;
long null_ARRAY_00615520;
long null_ARRAY_00615580;
long null_ARRAY_006155c0;
long null_ARRAY_006156c0;
long PTR_DAT_006153e0;
long PTR_null_ARRAY_00615420;
ulong
FUN_00401c5a (uint uParm1)
{
  byte bVar1;
  int iVar2;
  char *pcVar3;
  byte bStack35;
  byte bStack34;
  bool bStack33;

  if (uParm1 == 0)
    {
      func_0x00401500 ("Usage: %s [OPTION]... NAME...\n", DAT_00615560);
      func_0x00401690
	("Diagnose invalid or unportable file names.\n\n  -p                  check for most POSIX systems\n  -P                  check for empty names and leading \"-\"\n      --portability   check for all POSIX systems (equivalent to -p -P)\n",
	 DAT_006154c0);
      func_0x00401690 ("      --help     display this help and exit\n",
		       DAT_006154c0);
      pcVar3 = (char *) DAT_006154c0;
      func_0x00401690
	("      --version  output version information and exit\n");
      imperfection_wrapper ();	//    FUN_00401abe();
    }
  else
    {
      pcVar3 = "Try \'%s --help\' for more information.\n";
      func_0x00401680 (DAT_006154e0,
		       "Try \'%s --help\' for more information.\n",
		       DAT_00615560);
    }
  func_0x00401820 ();
  bStack33 = true;
  bStack34 = 0;
  bStack35 = 0;
  FUN_00402668 (*(undefined8 *) pcVar3);
  func_0x004017d0 (6, &DAT_00411f3b);
  func_0x004017b0 (FUN_00402583);
LAB_00401dda:
  ;
  do
    {
      while (true)
	{
	  while (true)
	    {
	      imperfection_wrapper ();	//        iVar2 = FUN_004056a5((ulong)uParm1, pcVar3, &DAT_00412207, null_ARRAY_00411fc0, 0);
	      if (iVar2 == -1)
		{
		  if (DAT_00615478 == uParm1)
		    {
		      imperfection_wrapper ();	//            FUN_004044cc(0, 0, "missing operand");
		      FUN_00401c5a (1);
		    }
		  while ((int) DAT_00615478 < (int) uParm1)
		    {
		      imperfection_wrapper ();	//            bVar1 = FUN_0040206d(((undefined8 *)pcVar3)[(long)(int)DAT_00615478], (ulong)bStack34, (ulong)bStack35, (ulong)bStack34);
		      bStack33 = (bVar1 & bStack33) != 0;
		      DAT_00615478 = DAT_00615478 + 1;
		    }
		  return (ulong) (bStack33 == false);
		}
	      if (iVar2 != 0x50)
		break;
	      bStack35 = 1;
	    }
	  if (0x50 < iVar2)
	    break;
	  if (iVar2 != -0x83)
	    {
	      if (iVar2 != -0x82)
		goto LAB_00401dd0;
	      FUN_00401c5a (0);
	    }
	  imperfection_wrapper ();	//      FUN_004041e7(DAT_006154c0, "pathchk", "GNU coreutils", PTR_DAT_006153e0, "Paul Eggert", "David MacKenzie", "Jim Meyering", 0);
	  func_0x00401820 (0);
	LAB_00401dd0:
	  ;
	  imperfection_wrapper ();	//      FUN_00401c5a();
	}
      if (iVar2 != 0x70)
	{
	  if (iVar2 != 0x80)
	    goto LAB_00401dd0;
	  bStack34 = 1;
	  bStack35 = 1;
	  goto LAB_00401dda;
	}
      bStack34 = 1;
    }
  while (true);
}


#include "echo.h"

long null_ARRAY_006133d_0_8_;
long null_ARRAY_006133d_8_8_;
long null_ARRAY_0061350_0_8_;
long null_ARRAY_0061350_16_8_;
long null_ARRAY_0061350_24_8_;
long null_ARRAY_0061350_32_8_;
long null_ARRAY_0061350_40_8_;
long null_ARRAY_0061350_48_8_;
long null_ARRAY_0061350_8_8_;
long DAT_00410400;
long DAT_004104bd;
long DAT_0041053b;
long DAT_004108c6;
long DAT_00410ad0;
long DAT_00410d60;
long DAT_00410da8;
long DAT_00410ede;
long DAT_00410ee2;
long DAT_00410ee7;
long DAT_00410ee9;
long DAT_00411553;
long DAT_00411920;
long DAT_00411938;
long DAT_0041193d;
long DAT_00411950;
long DAT_004119c0;
long DAT_004119c1;
long DAT_00613000;
long DAT_00613010;
long DAT_00613020;
long DAT_006133a8;
long DAT_006133c0;
long DAT_00613440;
long DAT_00613450;
long DAT_00613460;
long DAT_00613468;
long DAT_00613480;
long DAT_00613488;
long DAT_006134d0;
long DAT_006134d8;
long DAT_006134e0;
long DAT_00613640;
long DAT_00613648;
long DAT_00613650;
long fde_004125e0;
long FLOAT_UNKNOWN;
long null_ARRAY_00411e00;
long null_ARRAY_006133d0;
long null_ARRAY_006134a0;
long null_ARRAY_00613500;
long null_ARRAY_00613540;
long PTR_DAT_006133a0;
long PTR_null_ARRAY_006133e0;
ulong
FUN_00401aea (int iParm1)
{
  ulong uVar1;
  byte bVar2;

  if (iParm1 == 0)
    {
      func_0x004013e0
	("Usage: %s [SHORT-OPTION]... [STRING]...\n  or:  %s LONG-OPTION\n",
	 DAT_006134e0, DAT_006134e0);
      func_0x00401570
	("Echo the STRING(s) to standard output.\n\n  -n             do not output the trailing newline\n",
	 DAT_00613440);
      func_0x00401570
	("  -e             enable interpretation of backslash escapes\n  -E             disable interpretation of backslash escapes (default)\n",
	 DAT_00613440);
      func_0x00401570 ("      --help     display this help and exit\n",
		       DAT_00613440);
      func_0x00401570
	("      --version  output version information and exit\n",
	 DAT_00613440);
      func_0x00401570
	("\nIf -e is in effect, the following sequences are recognized:\n\n",
	 DAT_00613440);
      func_0x00401570
	("  \\\\      backslash\n  \\a      alert (BEL)\n  \\b      backspace\n  \\c      produce no further output\n  \\e      escape\n  \\f      form feed\n  \\n      new line\n  \\r      carriage return\n  \\t      horizontal tab\n  \\v      vertical tab\n",
	 DAT_00613440);
      func_0x00401570
	("  \\0NNN   byte with octal value NNN (1 to 3 digits)\n  \\xHH    byte with hexadecimal value HH (1 to 2 digits)\n",
	 DAT_00613440);
      func_0x004013e0
	("\nNOTE: your shell may have its own version of %s, which usually supersedes\nthe version described here.  Please refer to your shell\'s documentation\nfor details about the options it supports.\n",
	 &DAT_004108c6);
      imperfection_wrapper ();	//    FUN_0040194e();
    }
  else
    {
      func_0x00401560 (DAT_00613460,
		       "Try \'%s --help\' for more information.\n",
		       DAT_006134e0);
    }
  bVar2 = (byte) iParm1;
  func_0x004016d0 ();
  if (false)
    {
    switchD_00401c13_caseD_47:
      uVar1 = (ulong) ((uint) bVar2 - 0x30);
    }
  else
    {
      switch (bVar2)
	{
	case 0x41:
	case 0x61:
	  uVar1 = 10;
	  break;
	case 0x42:
	case 0x62:
	  uVar1 = 0xb;
	  break;
	case 0x43:
	case 99:
	  uVar1 = 0xc;
	  break;
	case 0x44:
	case 100:
	  uVar1 = 0xd;
	  break;
	case 0x45:
	case 0x65:
	  uVar1 = 0xe;
	  break;
	case 0x46:
	case 0x66:
	  uVar1 = 0xf;
	  break;
	default:
	  goto switchD_00401c13_caseD_47;
	}
    }
  return uVar1;
}

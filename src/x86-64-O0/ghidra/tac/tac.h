typedef unsigned char   undefined;

typedef unsigned char    byte;
typedef unsigned char    dwfenc;
typedef unsigned int    dword;
typedef unsigned long    qword;
typedef unsigned char    undefined1;
typedef unsigned short    undefined2;
typedef unsigned int    undefined4;
typedef unsigned long    undefined6;
typedef unsigned long    undefined8;
typedef unsigned short    word;
#define unkbyte9   unsigned long long
#define unkbyte10   unsigned long long
#define unkbyte11   unsigned long long
#define unkbyte12   unsigned long long
#define unkbyte13   unsigned long long
#define unkbyte14   unsigned long long
#define unkbyte15   unsigned long long
#define unkbyte16   unsigned long long

#define unkuint9   unsigned long long
#define unkuint10   unsigned long long
#define unkuint11   unsigned long long
#define unkuint12   unsigned long long
#define unkuint13   unsigned long long
#define unkuint14   unsigned long long
#define unkuint15   unsigned long long
#define unkuint16   unsigned long long

#define unkint9   long long
#define unkint10   long long
#define unkint11   long long
#define unkint12   long long
#define unkint13   long long
#define unkint14   long long
#define unkint15   long long
#define unkint16   long long

#define unkfloat1   float
#define unkfloat2   float
#define unkfloat3   float
#define unkfloat5   double
#define unkfloat6   double
#define unkfloat7   double
#define unkfloat9   long double
#define unkfloat11   long double
#define unkfloat12   long double
#define unkfloat13   long double
#define unkfloat14   long double
#define unkfloat15   long double
#define unkfloat16   long double

#define BADSPACEBASE   void
#define code   void

#define ulong long
#define true 1
#define bool int
#define false 0
#define float10 int
#define ushort int
#define undefined7 long
#define undefined2 long
#define uint5 long
#define uint6 long

// Add definition for uint
typedef unsigned int    uint;

#define sbyte char

extern imperfection_wrapper(void);
typedef struct eh_frame_hdr eh_frame_hdr, *Peh_frame_hdr;

struct eh_frame_hdr {
    byte eh_frame_hdr_version; // Exception Handler Frame Header Version
    dwfenc eh_frame_pointer_encoding; // Exception Handler Frame Pointer Encoding
    dwfenc eh_frame_desc_entry_count_encoding; // Encoding of # of Exception Handler FDEs
    dwfenc eh_frame_table_encoding; // Exception Handler Table Encoding
};

typedef struct fde_table_entry fde_table_entry, *Pfde_table_entry;

struct fde_table_entry {
    dword initial_loc; // Initial Location
    dword data_loc; // Data location
};

typedef enum Elf64_DynTag {
    DT_AUDIT=1879047932,
    DT_AUXILIARY=2147483645,
    DT_BIND_NOW=24,
    DT_CHECKSUM=1879047672,
    DT_CONFIG=1879047930,
    DT_DEBUG=21,
    DT_DEPAUDIT=1879047931,
    DT_ENCODING=32,
    DT_FEATURE_1=1879047676,
    DT_FILTER=2147483647,
    DT_FINI=13,
    DT_FINI_ARRAY=26,
    DT_FINI_ARRAYSZ=28,
    DT_FLAGS=30,
    DT_FLAGS_1=1879048187,
    DT_GNU_CONFLICT=1879047928,
    DT_GNU_CONFLICTSZ=1879047670,
    DT_GNU_HASH=1879047925,
    DT_GNU_LIBLIST=1879047929,
    DT_GNU_LIBLISTSZ=1879047671,
    DT_GNU_PRELINKED=1879047669,
    DT_HASH=4,
    DT_INIT=12,
    DT_INIT_ARRAY=25,
    DT_INIT_ARRAYSZ=27,
    DT_JMPREL=23,
    DT_MOVEENT=1879047674,
    DT_MOVESZ=1879047675,
    DT_MOVETAB=1879047934,
    DT_NEEDED=1,
    DT_NULL=0,
    DT_PLTGOT=3,
    DT_PLTPAD=1879047933,
    DT_PLTPADSZ=1879047673,
    DT_PLTREL=20,
    DT_PLTRELSZ=2,
    DT_POSFLAG_1=1879047677,
    DT_PREINIT_ARRAYSZ=33,
    DT_REL=17,
    DT_RELA=7,
    DT_RELACOUNT=1879048185,
    DT_RELAENT=9,
    DT_RELASZ=8,
    DT_RELCOUNT=1879048186,
    DT_RELENT=19,
    DT_RELSZ=18,
    DT_RPATH=15,
    DT_RUNPATH=29,
    DT_SONAME=14,
    DT_STRSZ=10,
    DT_STRTAB=5,
    DT_SYMBOLIC=16,
    DT_SYMENT=11,
    DT_SYMINENT=1879047679,
    DT_SYMINFO=1879047935,
    DT_SYMINSZ=1879047678,
    DT_SYMTAB=6,
    DT_TEXTREL=22,
    DT_TLSDESC_GOT=1879047927,
    DT_TLSDESC_PLT=1879047926,
    DT_VERDEF=1879048188,
    DT_VERDEFNUM=1879048189,
    DT_VERNEED=1879048190,
    DT_VERNEEDNUM=1879048191,
    DT_VERSYM=1879048176
} Elf64_DynTag;

typedef enum Elf_ProgramHeaderType {
    PT_DYNAMIC=2,
    PT_GNU_EH_FRAME=1685382480,
    PT_GNU_RELRO=1685382482,
    PT_GNU_STACK=1685382481,
    PT_INTERP=3,
    PT_LOAD=1,
    PT_NOTE=4,
    PT_NULL=0,
    PT_PHDR=6,
    PT_SHLIB=5,
    PT_TLS=7
} Elf_ProgramHeaderType;

typedef struct Elf64_Dyn Elf64_Dyn, *PElf64_Dyn;

struct Elf64_Dyn {
    enum Elf64_DynTag d_tag;
    qword d_val;
};

typedef struct Elf64_Shdr Elf64_Shdr, *PElf64_Shdr;

typedef enum Elf_SectionHeaderType {
    SHT_CHECKSUM=1879048184,
    SHT_DYNAMIC=6,
    SHT_DYNSYM=11,
    SHT_FINI_ARRAY=15,
    SHT_GNU_ATTRIBUTES=1879048181,
    SHT_GNU_HASH=1879048182,
    SHT_GNU_LIBLIST=1879048183,
    SHT_GNU_verdef=1879048189,
    SHT_GNU_verneed=1879048190,
    SHT_GNU_versym=1879048191,
    SHT_GROUP=17,
    SHT_HASH=5,
    SHT_INIT_ARRAY=14,
    SHT_NOBITS=8,
    SHT_NOTE=7,
    SHT_NULL=0,
    SHT_PREINIT_ARRAY=16,
    SHT_PROGBITS=1,
    SHT_REL=9,
    SHT_RELA=4,
    SHT_SHLIB=10,
    SHT_STRTAB=3,
    SHT_SUNW_COMDAT=1879048187,
    SHT_SUNW_move=1879048186,
    SHT_SUNW_syminfo=1879048188,
    SHT_SYMTAB=2,
    SHT_SYMTAB_SHNDX=18
} Elf_SectionHeaderType;

struct Elf64_Shdr {
    dword sh_name;
    enum Elf_SectionHeaderType sh_type;
    qword sh_flags;
    qword sh_addr;
    qword sh_offset;
    qword sh_size;
    dword sh_link;
    dword sh_info;
    qword sh_addralign;
    qword sh_entsize;
};

typedef struct Elf64_Phdr Elf64_Phdr, *PElf64_Phdr;

struct Elf64_Phdr {
    enum Elf_ProgramHeaderType p_type;
    dword p_flags;
    qword p_offset;
    qword p_vaddr;
    qword p_paddr;
    qword p_filesz;
    qword p_memsz;
    qword p_align;
};

typedef struct Elf64_Sym Elf64_Sym, *PElf64_Sym;

struct Elf64_Sym {
    dword st_name;
    byte st_info;
    byte st_other;
    word st_shndx;
    qword st_value;
    qword st_size;
};

typedef struct Elf64_Rela Elf64_Rela, *PElf64_Rela;

struct Elf64_Rela {
    qword r_offset; // location to apply the relocation action
    qword r_info; // the symbol table index and the type of relocation
    qword r_addend; // a constant addend used to compute the relocatable field value
};

typedef struct Elf64_Ehdr Elf64_Ehdr, *PElf64_Ehdr;

struct Elf64_Ehdr {
    byte e_ident_magic_num;
    char e_ident_magic_str[3];
    byte e_ident_class;
    byte e_ident_data;
    byte e_ident_version;
    byte e_ident_pad[9];
    word e_type;
    word e_machine;
    dword e_version;
    qword e_entry;
    qword e_phoff;
    qword e_shoff;
    dword e_flags;
    word e_ehsize;
    word e_phentsize;
    word e_phnum;
    word e_shentsize;
    word e_shnum;
    word e_shstrndx;
};




undefined8 _DT_INIT(void);
void FUN_00401cb0(void);
void entry(void);
void FUN_00402340(void);
void FUN_004023c0(void);
void FUN_00402440(void);
void FUN_0040247e(void);
void FUN_00402498(void);
void FUN_004024b2(undefined *puParm1);
void FUN_0040264e(uint uParm1);
void FUN_00402708(long lParm1,long lParm2);
undefined8 FUN_00402820(uint uParm1,undefined8 uParm2,char *pcParm3);
void FUN_00402f27(undefined8 uParm1);
undefined8 FUN_00402f45(long *plParm1,long *plParm2);
long FUN_00403184(undefined8 *puParm1,undefined8 *puParm2,uint uParm3,undefined8 uParm4);
ulong FUN_0040332a(uint uParm1,undefined8 uParm2);
ulong FUN_00403394(char *pcParm1);
ulong FUN_0040350d(uint uParm1,undefined8 *puParm2);
void FUN_00403925(void);
char * FUN_00403a0a(char *pcParm1);
long FUN_00403a2a(long lParm1,char *pcParm2,undefined8 *puParm3);
void FUN_00403b77(long lParm1);
ulong FUN_00403c54(undefined1 *puParm1,byte bParm2,uint uParm3);
undefined8 * FUN_00403cdc(undefined8 *puParm1,int iParm2);
char * FUN_00403d53(char *pcParm1,int iParm2);
ulong FUN_00403df3(long param_1,ulong param_2,long param_3,ulong param_4,uint param_5,uint param_6,long param_7,char *param_8,char *param_9);
undefined1 * FUN_00404cbd(int iParm1,undefined8 uParm2,undefined8 uParm3,uint *puParm4);
void FUN_00404f30(uint uParm1,uint uParm2,undefined8 uParm3);
void FUN_00404f71(uint uParm1,undefined8 uParm2);
void FUN_00404f95(undefined8 uParm1,undefined8 uParm2,char cParm3);
void FUN_00405029(undefined8 uParm1,char cParm2);
void FUN_00405053(undefined8 uParm1);
void FUN_00405072(uint uParm1,uint uParm2,undefined8 uParm3);
void FUN_0040510d(uint uParm1,undefined8 uParm2,undefined8 uParm3);
void FUN_00405139(uint uParm1,undefined8 uParm2);
void FUN_00405162(undefined8 uParm1);
long FUN_00405181(uint uParm1,undefined8 uParm2,undefined8 uParm3);
void FUN_004051f2(undefined8 uParm1);
ulong FUN_00405213(uint uParm1);
void FUN_00405263(undefined8 uParm1,long lParm2,undefined8 uParm3,undefined8 uParm4,undefined8 *puParm5,undefined8 uParm6);
void FUN_004056c7(undefined8 uParm1,undefined8 uParm2,undefined8 uParm3,undefined8 uParm4,uint *puParm5);
void FUN_00405799(undefined4 uParm1,undefined4 uParm2,undefined4 uParm3,undefined4 uParm4,undefined4 uParm5,undefined4 uParm6,undefined4 uParm7,undefined4 uParm8,undefined8 param_1,undefined8 param_2,undefined8 param_3,undefined8 param_4,undefined8 param_5,undefined8 param_6);
void FUN_0040584f(undefined8 uParm1);
long FUN_00405869(long lParm1);
long FUN_0040589e(long lParm1,long lParm2);
void FUN_004058ff(void);
void FUN_00405929(void);
void FUN_0040592f(uint uParm1,uint uParm2);
undefined8 FUN_00405957(ulong uParm1,ulong uParm2);
ulong FUN_004059d6(uint uParm1);
void FUN_004059ff(void);
void FUN_00405a33(uint uParm1);
void FUN_00405aa7(uint uParm1,uint uParm2,undefined8 uParm3,undefined8 uParm4);
void FUN_00405b2b(undefined4 uParm1,undefined4 uParm2,undefined4 uParm3,undefined4 uParm4,undefined4 uParm5,undefined4 uParm6,undefined4 uParm7,undefined4 uParm8,uint param_1,uint param_2,undefined8 param_3,undefined8 param_4,undefined8 param_5,undefined8 param_6);
void FUN_00405c0f(undefined8 uParm1,undefined8 uParm2,uint uParm3);
void FUN_00405c3a(long lParm1,int *piParm2);
ulong FUN_00405e1e(int param_1,undefined8 *param_2,char *param_3,long *param_4,int *param_5,int param_6,int *param_7,int param_8,undefined8 param_9);
char * FUN_00406408(undefined8 uParm1,undefined8 uParm2,char *pcParm3,int *piParm4,int iParm5);
ulong FUN_004064d6(uint param_1,undefined8 *param_2,char *param_3,long param_4,undefined8 param_5,uint param_6,uint *param_7,uint param_8);
ulong FUN_00406c9f(uint param_1,undefined8 param_2,undefined8 param_3,undefined8 param_4,undefined8 param_5,uint param_6,uint param_7);
void FUN_00406d2f(uint uParm1,undefined8 uParm2,undefined8 uParm3,undefined8 uParm4,undefined8 uParm5);
undefined8 FUN_00406d79(long lParm1);
ulong FUN_00406daa(uint *puParm1,byte *pbParm2,long lParm3,undefined8 uParm4);
void FUN_00406e2d(undefined8 uParm1);
undefined8 FUN_00406e56(long lParm1,long lParm2);
undefined8 FUN_00406ebf(int iParm1);
void FUN_00406ee5(long lParm1,long lParm2);
void FUN_00406f51(long lParm1,long lParm2);
ulong FUN_00406fc0(long lParm1,long lParm2);
void FUN_00407017(undefined8 uParm1);
void FUN_0040703b(undefined8 uParm1);
void FUN_0040705f(undefined8 uParm1,undefined8 uParm2);
void FUN_00407089(long lParm1);
void FUN_004070d8(long lParm1,long lParm2);
void FUN_00407143(long lParm1,long lParm2);
ulong FUN_004071ae(long lParm1,long lParm2);
ulong FUN_00407221(long lParm1,long lParm2);
undefined8 FUN_0040726a(void);
ulong FUN_0040727d(long param_1,undefined8 param_2,long param_3,long param_4,undefined8 param_5,byte param_6,long param_7);
ulong FUN_004073c8(long lParm1,undefined8 uParm2,long lParm3,long lParm4,byte bParm5,long lParm6);
undefined8 FUN_0040759c(long lParm1,ulong uParm2);
void FUN_00407710(undefined8 uParm1,undefined8 uParm2,undefined8 *puParm3,long lParm4,char cParm5,long lParm6);
void FUN_00407800(long *plParm1);
undefined8 FUN_00407b2b(long *plParm1);
long FUN_00408607(long *plParm1,long lParm2,uint *puParm3);
void FUN_0040873a(long *plParm1);
void FUN_0040880b(long *plParm1);
ulong FUN_004088ab(byte **ppbParm1,byte *pbParm2,uint uParm3);
ulong FUN_00409591(long *plParm1,long lParm2);
ulong FUN_00409713(long *plParm1);
void FUN_0040989d(long lParm1);
ulong FUN_004098ea(long lParm1,ulong uParm2,uint uParm3);
undefined8 FUN_00409a70(long *plParm1,long lParm2);
undefined8 FUN_00409add(undefined8 *puParm1,undefined8 uParm2);
undefined8 FUN_00409b67(undefined8 *puParm1,long lParm2,long lParm3);
undefined8 FUN_00409c45(long *plParm1,long lParm2);
undefined8 FUN_00409d25(long *plParm1,long lParm2,long lParm3);
undefined8 FUN_0040a106(long *plParm1,long lParm2,long lParm3);
undefined8 FUN_0040a418(long *plParm1,long lParm2);
ulong FUN_0040a7de(long *plParm1,long lParm2);
undefined8 FUN_0040a9c9(long *plParm1,undefined8 uParm2);
undefined8 FUN_0040aa7e(long lParm1,long lParm2);
long FUN_0040ab0d(long lParm1,long lParm2);
void FUN_0040abc5(long lParm1,long lParm2);
long FUN_0040ac43(long *plParm1,undefined8 uParm2,undefined8 uParm3);
long FUN_0040afdb(long lParm1,uint uParm2);
ulong * FUN_0040b035(undefined4 *puParm1,long lParm2,long lParm3);
ulong * FUN_0040b157(undefined4 *puParm1,long lParm2,long lParm3,uint uParm4);
undefined8 FUN_0040b284(long *plParm1,ulong *puParm2,ulong uParm3);
void FUN_0040b438(long lParm1);
long FUN_0040b4db(long *plParm1,long lParm2,undefined8 uParm3);
long FUN_0040b6b0(long *plParm1,long lParm2,uint uParm3,undefined8 uParm4);
char * FUN_0040b99c(undefined8 uParm1,undefined8 uParm2,long lParm3);
undefined8 FUN_0040ba28(long *plParm1);
void FUN_0040bb21(long **pplParm1,long lParm2,long lParm3);
void FUN_0040c1fa(long *plParm1,undefined8 uParm2);
ulong FUN_0040c45c(long *plParm1,undefined8 uParm2,undefined8 uParm3,ulong uParm4);
undefined8 FUN_0040c7d6(long *plParm1,ulong uParm2);
void FUN_0040cb64(long lParm1);
void FUN_0040cce6(long *plParm1);
ulong FUN_0040cd75(long *plParm1);
void FUN_0040d0c4(long *plParm1);
ulong FUN_0040d31b(long *plParm1);
ulong FUN_0040d6b1(long **pplParm1,code *pcParm2,undefined8 uParm3);
ulong FUN_0040d78f(long *plParm1,code *pcParm2,undefined8 uParm3);
undefined8 FUN_0040d84a(long lParm1,long lParm2);
ulong FUN_0040d9c1(undefined8 uParm1,long lParm2);
long FUN_0040daa3(undefined4 *puParm1,long *plParm2,long lParm3);
undefined8 FUN_0040dc94(long *plParm1,long lParm2);
undefined8 FUN_0040dd86(undefined8 uParm1,long lParm2);
ulong FUN_0040de35(long lParm1,long lParm2);
ulong FUN_0040e0ac(long *plParm1,long lParm2,long lParm3,long lParm4,uint uParm5);
long FUN_0040e5dc(long *plParm1,long lParm2,uint uParm3);
long FUN_0040e675(long *plParm1,long lParm2,undefined4 uParm3);
undefined8 FUN_0040e7ab(long lParm1);
ulong FUN_0040e8e9(long lParm1);
ulong FUN_0040e9c9(undefined8 *puParm1,long *plParm2,long lParm3,char cParm4);
void FUN_0040ed9b(undefined8 uParm1,long lParm2,undefined8 uParm3);
undefined8 FUN_0040ede0(long *plParm1,long lParm2,ulong uParm3);
ulong FUN_0040f5d8(char *pcParm1,long lParm2,ulong uParm3);
long FUN_0040f7ed(undefined8 uParm1,long *plParm2,ulong uParm3,int *piParm4);
long FUN_0040f919(undefined8 uParm1,long *plParm2,long lParm3,ulong uParm4,long lParm5,int *piParm6);
long FUN_0040fb18(undefined8 uParm1,undefined8 *puParm2,long lParm3,undefined8 uParm4,long lParm5,int *piParm6);
long FUN_0040fcd4(long lParm1,long *plParm2,undefined8 *puParm3,ulong uParm4,long lParm5,int *piParm6);
long FUN_0041055a(undefined8 uParm1,long *plParm2,long lParm3,ulong uParm4,undefined8 uParm5,int *piParm6);
long FUN_004106ee(long lParm1,long lParm2,undefined8 uParm3,undefined8 *puParm4,ulong uParm5,undefined4 *puParm6);
ulong FUN_00410c12(byte bParm1,long lParm2);
undefined8 FUN_00410c49(ulong uParm1,undefined8 uParm2,long lParm3,long *plParm4,int *piParm5,int *piParm6);
undefined8 FUN_0041100a(undefined8 uParm1,undefined8 uParm2,undefined8 uParm3,byte *pbParm4);
long FUN_00411069(long lParm1,long lParm2,long lParm3,ulong uParm4,int *piParm5);
undefined8 FUN_0041199f(undefined4 *param_1,long param_2,undefined *param_3,int param_4,undefined8 param_5,undefined8 param_6,char param_7);
undefined8 FUN_00411aea(undefined4 *puParm1,long lParm2,char *pcParm3);
undefined8 FUN_00411c51(undefined8 uParm1,undefined8 uParm2,undefined8 uParm3,byte *pbParm4);
undefined8 FUN_00411cab(long lParm1,undefined8 uParm2,long lParm3,long *plParm4,char *pcParm5,ulong uParm6);
long FUN_00412612(long lParm1,undefined8 uParm2,undefined8 uParm3,char *pcParm4,byte bParm5,int *piParm6);
long FUN_004128ad(undefined8 uParm1,byte *pbParm2,undefined8 uParm3);
void FUN_00412994(undefined8 *puParm1);
void FUN_004129cd(undefined8 uParm1,undefined8 uParm2,undefined8 uParm3,undefined uParm4);
undefined8 * FUN_00412a04(long lParm1,undefined8 *puParm2,undefined8 *puParm3,undefined8 *puParm4);
undefined8 FUN_00412b50(long lParm1,long lParm2);
void FUN_00412b93(undefined8 *puParm1);
undefined8 FUN_00412bf7(undefined8 uParm1,long lParm2);
long * FUN_00412c1e(long **pplParm1,undefined8 uParm2);
void FUN_00412d4a(undefined8 uParm1,undefined8 uParm2,undefined8 uParm3,undefined8 uParm4,undefined8 uParm5,undefined8 uParm6);
ulong FUN_00412d9b(long param_1,undefined8 param_2,ulong param_3,ulong param_4,ulong param_5,undefined8 param_6,ulong *param_7,char param_8);
ulong FUN_00413123(ulong *puParm1,long lParm2,ulong uParm3,int iParm4);
ulong FUN_004133d3(long *param_1,long param_2,long param_3,long param_4,long param_5,undefined8 param_6,ulong param_7,undefined8 *param_8,uint param_9);
ulong FUN_0041414e(long lParm1);
long FUN_00414470(long lParm1,char cParm2,long lParm3);
undefined8 FUN_00414989(long *plParm1,long lParm2,uint uParm3);
undefined8 FUN_00414a50(long lParm1,long lParm2,undefined8 uParm3);
long FUN_00414af3(long param_1,undefined8 param_2,long param_3,long *param_4,long param_5,long param_6,long param_7);
ulong FUN_00415035(long *plParm1,undefined8 uParm2,undefined8 uParm3,long lParm4,undefined8 uParm5,undefined8 uParm6);
undefined8 FUN_00415202(long *plParm1,undefined8 *puParm2,long lParm3,undefined8 uParm4,undefined8 *puParm5);
undefined8 FUN_00415353(long *plParm1,long lParm2,ulong uParm3,long *plParm4,char cParm5);
undefined8 FUN_00415765(long *plParm1);
void FUN_004157fb(long *plParm1,long lParm2,long lParm3,long lParm4,long lParm5,long lParm6);
ulong FUN_0041599f(long lParm1,long *plParm2);
undefined8 FUN_00415b3c(long lParm1,long *plParm2,long lParm3,undefined8 uParm4);
ulong FUN_00415d46(long lParm1,long lParm2);
ulong FUN_00415e30(undefined8 uParm1,long lParm2,long lParm3,long lParm4);
ulong FUN_00415f8a(long lParm1,long *plParm2,long lParm3,long lParm4);
ulong FUN_00416169(long lParm1,long lParm2,undefined8 uParm3);
ulong FUN_00416298(long *plParm1,long lParm2,undefined8 uParm3,undefined8 uParm4);
undefined8 FUN_00416526(long lParm1,long lParm2,undefined8 uParm3,undefined8 uParm4,undefined8 uParm5,undefined8 uParm6);
ulong FUN_00416692(long lParm1,uint uParm2,long lParm3,long lParm4,long lParm5);
undefined8 FUN_00416903(long lParm1,long lParm2,undefined8 uParm3,undefined8 uParm4,long lParm5,undefined8 uParm6);
ulong FUN_004169d0(long *plParm1,long lParm2,undefined8 uParm3,long lParm4,long lParm5,long lParm6);
ulong FUN_00416d44(long lParm1,long *plParm2,long lParm3,long lParm4);
ulong FUN_004171f4(long lParm1,long *plParm2,long lParm3,long lParm4,long lParm5);
undefined8 FUN_004172ce(int *piParm1,long lParm2,long lParm3);
long FUN_00417449(int *piParm1,long lParm2,long lParm3);
long FUN_004176d5(int *piParm1,long lParm2);
ulong FUN_0041777f(long lParm1,long lParm2,undefined8 uParm3);
ulong FUN_0041787a(long lParm1,long lParm2);
ulong FUN_00417bf0(long lParm1,long lParm2);
ulong FUN_00418145(long lParm1,long lParm2,long lParm3);
ulong FUN_004186aa(undefined8 uParm1,long *plParm2,undefined8 *puParm3,undefined8 uParm4,long lParm5);
long FUN_00418781(long *plParm1,long lParm2,long lParm3,uint uParm4);
ulong FUN_0041880d(long param_1,long *param_2,long param_3,long param_4,undefined8 param_5,long param_6,uint param_7);
ulong FUN_00418f9b(long lParm1,long lParm2,long lParm3,undefined8 uParm4);
ulong FUN_0041925e(long lParm1,undefined8 *puParm2,undefined8 uParm3,uint uParm4);
ulong FUN_004193d2(long *plParm1,undefined8 uParm2,long lParm3,long lParm4,uint uParm5);
ulong FUN_0041958f(long lParm1,undefined8 uParm2,long lParm3,undefined8 uParm4,uint uParm5);
undefined8 FUN_0041996a(long lParm1,long lParm2);
long FUN_0041a383(long *plParm1,long lParm2,long lParm3,long lParm4);
ulong FUN_0041ac1d(long *plParm1,long lParm2,long lParm3,long lParm4);
undefined8 FUN_0041b058(long lParm1,undefined8 *puParm2,long lParm3);
ulong FUN_0041b229(long lParm1,int iParm2);
undefined8 FUN_0041b3b1(long lParm1,undefined4 uParm2,ulong uParm3);
void FUN_0041b4f2(long lParm1);
void FUN_0041b602(long lParm1);
undefined8 FUN_0041b642(long lParm1,undefined8 uParm2,long lParm3,long lParm4,long lParm5);
long FUN_0041b95f(long lParm1,long lParm2);
undefined8 FUN_0041ba35(long lParm1,undefined8 uParm2,undefined8 uParm3);
undefined8 * FUN_0041bbad(long lParm1,undefined8 uParm2,undefined8 uParm3);
void FUN_0041bcb9(undefined8 *puParm1,undefined8 uParm2,undefined8 uParm3,undefined8 uParm4,undefined8 uParm5);
ulong FUN_0041bd20(long lParm1);
ulong FUN_0041be8c(undefined8 uParm1,undefined8 uParm2,undefined8 uParm3);
undefined8 FUN_0041bfad(void);
undefined8 FUN_0041bfbe(void);
ulong FUN_0041bfcc(uint uParm1,uint uParm2);
ulong FUN_0041c003(byte *pbParm1,byte *pbParm2);
undefined8 FUN_0041c07a(undefined8 uParm1);
char * FUN_0041c105(char *pcParm1);
ulong FUN_0041c16e(long lParm1);
undefined8 FUN_0041c1bc(void);
ulong FUN_0041c1c9(uint uParm1);
undefined1 * FUN_0041c29a(void);
char * FUN_0041c64d(void);
ulong FUN_0041c71b(undefined8 uParm1,ulong uParm2);
ulong FUN_0041c745(long lParm1,int iParm2,undefined8 uParm3,code *pcParm4,ulong uParm5);
void FUN_0041c90c(undefined8 uParm1,uint *puParm2);
void FUN_0041c94d(undefined8 uParm1);
undefined8 FUN_0041c970(undefined8 uParm1);
void FUN_0041c9d7(undefined8 uParm1,uint uParm2,undefined4 uParm3,int iParm4,undefined8 uParm5);
void FUN_0041ca5d(undefined8 uParm1,uint uParm2,uint uParm3,uint uParm4);
void FUN_0041ca8f(uint uParm1);
ulong FUN_0041cab5(undefined8 uParm1);
ulong FUN_0041cb6d(undefined4 uParm1,undefined4 uParm2,undefined4 uParm3,undefined4 uParm4,undefined4 uParm5,undefined4 uParm6,undefined4 uParm7,undefined4 uParm8,uint param_1,uint param_2,undefined8 param_3,undefined8 param_4,undefined8 param_5,undefined8 param_6);
void FUN_0041ce24(undefined8 uParm1);
void FUN_0041ce48(void);
ulong FUN_0041ce56(long lParm1);
undefined8 FUN_0041cf26(undefined8 uParm1);
void FUN_0041cf45(undefined8 uParm1,undefined8 uParm2);
ulong FUN_0041cf6a(long lParm1,long lParm2);
ulong FUN_0041d021(long lParm1,uint uParm2);
void FUN_0041d15d(undefined8 uParm1,undefined8 uParm2);
ulong FUN_0041d182(long lParm1,long lParm2);
ulong FUN_0041d212(void);
long FUN_0041d25f(ulong uParm1,long lParm2,ulong uParm3,long lParm4,long *plParm5);
int * FUN_0041d4ab(ulong uParm1,uint *puParm2,ulong uParm3,uint *puParm4,ulong *puParm5);
char * FUN_0041df8b(ulong uParm1,long lParm2,long lParm3);
long FUN_0041e1f9(int *param_1,long *param_2);
long FUN_0041e3e4(undefined auParm1 [16],int *piParm2,long *plParm3);
undefined8 FUN_0041e7a3(int iParm1,long lParm2,uint *puParm3,long lParm4,uint uParm5);
void FUN_0041efb6(uint param_1);
void FUN_0041f000(undefined8 uParm1,uint uParm2);
ulong FUN_0041f052(void);
ulong FUN_0041f3e4(undefined auParm1 [16],undefined8 uParm2,undefined8 uParm3);
ulong FUN_0041f7bc(char *pcParm1,long lParm2);
undefined8 FUN_0041f815(long param_1,long param_2,undefined param_3,int param_4,undefined8 param_5,ulong param_6,int param_7,ulong param_8,int param_9);
long FUN_0041fad8(long lParm1,char **ppcParm2,long lParm3,undefined8 uParm4);
ulong FUN_00426f42(uint uParm1);
ulong FUN_00426f61(char *pcParm1);
void FUN_00426fc6(undefined8 uParm1,int *piParm2,undefined8 uParm3);
void FUN_00427067(int *param_1);
undefined8 * FUN_00427126(undefined8 uParm1);
undefined8 FUN_0042716d(undefined8 uParm1,undefined8 uParm2);
long FUN_004271b0(long lParm1);
ulong FUN_004271c2(undefined8 *puParm1,ulong uParm2);
void FUN_00427373(undefined8 uParm1);
ulong FUN_0042739e(undefined8 *puParm1);
ulong * FUN_004273e4(ulong uParm1,ulong uParm2);
undefined8 * FUN_00427444(undefined8 uParm1,undefined8 uParm2);
void FUN_0042748b(long lParm1,ulong uParm2,ulong uParm3);
long FUN_004276f0(long lParm1,ulong uParm2);
void FUN_004277df(undefined8 *puParm1,long lParm2,long lParm3);
void FUN_0042787f(ulong *puParm1,ulong uParm2,ulong uParm3);
void FUN_004279c4(long *plParm1,undefined8 uParm2,undefined8 uParm3);
undefined8 FUN_00427a1a(long *plParm1);
undefined8 FUN_00427a6a(undefined8 uParm1);
undefined8 FUN_00427a84(long lParm1,undefined8 uParm2);
void FUN_00427ac8(ulong *puParm1,long *plParm2);
void FUN_00428158(long lParm1);
ulong FUN_004286df(uint uParm1);
ulong FUN_004286ef(ulong uParm1,long lParm2);
void FUN_00428723(undefined8 uParm1,undefined8 uParm2,undefined8 uParm3);
void FUN_0042875e(undefined8 uParm1,undefined8 uParm2,undefined8 uParm3,undefined8 uParm4);
ulong FUN_004287af(ulong uParm1,ulong uParm2);
void FUN_004287ca(undefined8 uParm1,undefined8 uParm2);
ulong FUN_004287f1(undefined8 uParm1,undefined8 uParm2);
undefined8 FUN_00428898(uint *puParm1,ulong *puParm2);
undefined8 FUN_00429038(char *pcParm1,ulong *puParm2,char **ppcParm3);
ulong FUN_0042a2ee(undefined auParm1 [16]);
ulong FUN_0042a329(void);
long FUN_0042a35b(undefined8 uParm1,undefined8 uParm2);
void FUN_0042a450(void);
undefined8 _DT_FINI(void);
undefined FUN_00633000();
undefined FUN_00633008();
undefined FUN_00633010();
undefined FUN_00633018();
undefined FUN_00633020();
undefined FUN_00633028();
undefined FUN_00633030();
undefined FUN_00633038();
undefined FUN_00633040();
undefined FUN_00633048();
undefined FUN_00633050();
undefined FUN_00633058();
undefined FUN_00633060();
undefined FUN_00633068();
undefined FUN_00633070();
undefined FUN_00633078();
undefined FUN_00633080();
undefined FUN_00633088();
undefined FUN_00633090();
undefined FUN_00633098();
undefined FUN_006330a0();
undefined FUN_006330a8();
undefined FUN_006330b0();
undefined FUN_006330b8();
undefined FUN_006330c0();
undefined FUN_006330c8();
undefined FUN_006330d0();
undefined FUN_006330d8();
undefined FUN_006330e0();
undefined FUN_006330e8();
undefined FUN_006330f0();
undefined FUN_006330f8();
undefined FUN_00633100();
undefined FUN_00633108();
undefined FUN_00633110();
undefined FUN_00633118();
undefined FUN_00633120();
undefined FUN_00633128();
undefined FUN_00633130();
undefined FUN_00633138();
undefined FUN_00633140();
undefined FUN_00633148();
undefined FUN_00633150();
undefined FUN_00633158();
undefined FUN_00633160();
undefined FUN_00633168();
undefined FUN_00633170();
undefined FUN_00633178();
undefined FUN_00633180();
undefined FUN_00633188();
undefined FUN_00633190();
undefined FUN_00633198();
undefined FUN_006331a0();
undefined FUN_006331a8();
undefined FUN_006331b0();
undefined FUN_006331b8();
undefined FUN_006331c0();
undefined FUN_006331c8();
undefined FUN_006331d0();
undefined FUN_006331d8();
undefined FUN_006331e0();
undefined FUN_006331e8();
undefined FUN_006331f0();
undefined FUN_006331f8();
undefined FUN_00633200();
undefined FUN_00633208();
undefined FUN_00633210();
undefined FUN_00633218();
undefined FUN_00633220();
undefined FUN_00633228();
undefined FUN_00633230();
undefined FUN_00633238();
undefined FUN_00633240();
undefined FUN_00633248();
undefined FUN_00633250();
undefined FUN_00633258();
undefined FUN_00633260();
undefined FUN_00633268();
undefined FUN_00633270();
undefined FUN_00633278();
undefined FUN_00633280();
undefined FUN_00633288();
undefined FUN_00633290();
undefined FUN_00633298();
undefined FUN_006332a0();
undefined FUN_006332a8();
undefined FUN_006332b0();
undefined FUN_006332b8();
undefined FUN_006332c0();
undefined FUN_006332c8();
undefined FUN_006332d0();
undefined FUN_006332d8();
undefined FUN_006332e0();
undefined FUN_006332e8();
undefined FUN_006332f0();
undefined FUN_006332f8();
undefined FUN_00633300();
undefined FUN_00633308();
undefined FUN_00633310();
undefined FUN_00633318();


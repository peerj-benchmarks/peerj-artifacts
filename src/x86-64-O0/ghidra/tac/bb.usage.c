
#include "tac.h"

long null_ARRAY_006304f_0_8_;
long null_ARRAY_006304f_8_8_;
long null_ARRAY_0063068_0_8_;
long null_ARRAY_0063068_32_8_;
long null_ARRAY_0063068_40_8_;
long null_ARRAY_0063068_8_8_;
long null_ARRAY_006307c_16_8_;
long null_ARRAY_006307c_8_8_;
long null_ARRAY_0063284_0_8_;
long null_ARRAY_0063284_16_8_;
long null_ARRAY_0063284_24_8_;
long null_ARRAY_0063284_32_8_;
long null_ARRAY_0063284_40_8_;
long null_ARRAY_0063284_48_8_;
long null_ARRAY_0063284_8_8_;
long null_ARRAY_0063298_0_4_;
long null_ARRAY_0063298_16_8_;
long null_ARRAY_0063298_4_4_;
long null_ARRAY_0063298_8_4_;
long local_a_0_4_;
long DAT_0042a543;
long DAT_0042a5fd;
long DAT_0042a67b;
long DAT_0042a98e;
long DAT_0042a9f2;
long DAT_0042aa3e;
long DAT_0042aa8f;
long DAT_0042aadc;
long DAT_0042aafb;
long DAT_0042ab28;
long DAT_0042ab70;
long DAT_0042ac9e;
long DAT_0042aca2;
long DAT_0042aca7;
long DAT_0042aca9;
long DAT_0042b313;
long DAT_0042b6e0;
long DAT_0042b6f8;
long DAT_0042b6fd;
long DAT_0042b767;
long DAT_0042b7f8;
long DAT_0042b7fb;
long DAT_0042b849;
long DAT_0042bac0;
long DAT_0042c270;
long DAT_0042c278;
long DAT_0042c3a8;
long DAT_0042c51c;
long DAT_0042c590;
long DAT_0042c591;
long DAT_0042c5c0;
long DAT_0042c848;
long DAT_0042ccbc;
long DAT_00630000;
long DAT_00630010;
long DAT_00630020;
long DAT_006304c8;
long DAT_006304e0;
long DAT_00630558;
long DAT_0063055c;
long DAT_00630560;
long DAT_00630580;
long DAT_00630590;
long DAT_006305a0;
long DAT_006305a8;
long DAT_006305c0;
long DAT_006305c8;
long DAT_00630640;
long DAT_00630648;
long DAT_00630649;
long DAT_00630650;
long DAT_00630658;
long DAT_00630660;
long DAT_00630668;
long DAT_00630670;
long DAT_006307d8;
long DAT_00632800;
long DAT_00632808;
long DAT_00632810;
long DAT_00632818;
long DAT_00632820;
long DAT_006329b8;
long DAT_006329c0;
long DAT_00632a18;
long DAT_00632a20;
long DAT_00632a30;
long DAT_00632a38;
long fde_0042d9b8;
long FLOAT_UNKNOWN;
long null_ARRAY_0042a700;
long null_ARRAY_0042ab00;
long null_ARRAY_0042ba00;
long null_ARRAY_0042baa0;
long null_ARRAY_0042caa0;
long null_ARRAY_006304f0;
long null_ARRAY_00630520;
long null_ARRAY_006305e0;
long null_ARRAY_00630680;
long null_ARRAY_006306c0;
long null_ARRAY_006307c0;
long null_ARRAY_00630800;
long null_ARRAY_00632840;
long null_ARRAY_00632880;
long null_ARRAY_00632980;
long PTR_DAT_006304c0;
long PTR_null_ARRAY_00630500;
long PTR_null_ARRAY_00630568;
void
FUN_0040264e (uint uParm1)
{
  char *pcStack56;
  char *pcStack48;
  char *pcStack40;

  if (uParm1 == 0)
    {
      func_0x00401ce0 ("Usage: %s [OPTION]... [FILE]...\n", DAT_00632820);
      func_0x00401f40
	("Write each FILE to standard output, last line first.\n",
	 DAT_00630580);
      FUN_0040247e ();
      FUN_00402498 ();
      func_0x00401f40
	("  -b, --before             attach the separator before instead of after\n  -r, --regex              interpret the separator as a regular expression\n  -s, --separator=STRING   use STRING as the separator instead of newline\n",
	 DAT_00630580);
      func_0x00401f40 ("      --help     display this help and exit\n",
		       DAT_00630580);
      pcStack40 = DAT_00630580;
      func_0x00401f40
	("      --version  output version information and exit\n");
      imperfection_wrapper ();	//    FUN_004024b2();
    }
  else
    {
      pcStack40 = "Try \'%s --help\' for more information.\n";
      func_0x00401f30 (DAT_006305a0,
		       "Try \'%s --help\' for more information.\n",
		       DAT_00632820);
    }
  pcStack56 = (char *) (ulong) uParm1;
  func_0x00402190 ();
  pcStack40 = pcStack40 + -(long) pcStack56;
  pcStack48 = (char *) (0x2000 - (long) DAT_006307d8);
  if (pcStack56 == (char *) 0x0)
    {
      func_0x00402150 (null_ARRAY_00630800, 1, DAT_006307d8, DAT_00630580);
      DAT_006307d8 = (char *) 0x0;
    }
  else
    {
      while (pcStack48 <= pcStack40)
	{
	  func_0x00401db0 (null_ARRAY_00630800 + (long) DAT_006307d8,
			   pcStack56, pcStack48,
			   null_ARRAY_00630800 + (long) DAT_006307d8);
	  pcStack40 = pcStack40 + -(long) pcStack48;
	  pcStack56 = pcStack48 + (long) pcStack56;
	  func_0x00402150 (null_ARRAY_00630800, 1, 0x2000, DAT_00630580);
	  DAT_006307d8 = (char *) 0x0;
	  pcStack48 = (char *) 0x2000;
	}
      func_0x00401db0 (null_ARRAY_00630800 + (long) DAT_006307d8, pcStack56,
		       pcStack40, null_ARRAY_00630800 + (long) DAT_006307d8);
      DAT_006307d8 = pcStack40 + (long) DAT_006307d8;
    }
  return;
}

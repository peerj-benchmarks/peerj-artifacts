
#include "touch.h"

long null_ARRAY_0062455_0_8_;
long null_ARRAY_0062455_8_8_;
long null_ARRAY_006246c_0_8_;
long null_ARRAY_006246c_16_8_;
long null_ARRAY_006246c_24_8_;
long null_ARRAY_006246c_8_8_;
long null_ARRAY_0062470_0_8_;
long null_ARRAY_0062470_16_8_;
long null_ARRAY_0062470_24_8_;
long null_ARRAY_0062470_32_8_;
long null_ARRAY_0062470_40_8_;
long null_ARRAY_0062470_48_8_;
long null_ARRAY_0062470_8_8_;
long null_ARRAY_0062484_0_4_;
long null_ARRAY_0062484_16_8_;
long null_ARRAY_0062484_4_4_;
long null_ARRAY_0062484_8_4_;
long DAT_0041dd4b;
long DAT_0041de05;
long DAT_0041de83;
long DAT_0041e07b;
long DAT_0041e7b8;
long DAT_0041e7d0;
long DAT_0041e807;
long DAT_0041e80a;
long DAT_0041e80e;
long DAT_0041e869;
long DAT_0041e88f;
long DAT_0041e894;
long DAT_0041ed40;
long DAT_0041f290;
long DAT_0041ff75;
long DAT_0041ff78;
long DAT_0041ffb9;
long DAT_00420088;
long DAT_0042008b;
long DAT_00420090;
long DAT_00420159;
long DAT_004202ad;
long DAT_004208c0;
long DAT_0042097c;
long DAT_004209d0;
long DAT_00420b1e;
long DAT_00420b22;
long DAT_00420b27;
long DAT_00420b29;
long DAT_00421193;
long DAT_00421560;
long DAT_00421578;
long DAT_0042157d;
long DAT_004215e7;
long DAT_00421678;
long DAT_0042167b;
long DAT_004216c9;
long DAT_00421741;
long DAT_00421928;
long DAT_00421d70;
long DAT_00421de0;
long DAT_00421de1;
long DAT_006240c8;
long DAT_006240d8;
long DAT_006240e8;
long DAT_00624530;
long DAT_00624540;
long DAT_006245b8;
long DAT_006245bc;
long DAT_006245c0;
long DAT_00624600;
long DAT_00624610;
long DAT_00624620;
long DAT_00624628;
long DAT_00624640;
long DAT_00624648;
long DAT_006246a0;
long DAT_006246a4;
long DAT_006246a5;
long DAT_006246a6;
long DAT_006246a7;
long DAT_006246e0;
long DAT_006246e8;
long DAT_006246f0;
long DAT_006246f8;
long DAT_006248b8;
long DAT_006248c0;
long DAT_006248c8;
long DAT_00625108;
long DAT_00625110;
long DAT_00625120;
long fde_00422b20;
long FLOAT_UNKNOWN;
long null_ARRAY_0041df00;
long null_ARRAY_0041e020;
long null_ARRAY_0041e050;
long null_ARRAY_0041e940;
long null_ARRAY_0041ea80;
long null_ARRAY_0041eb00;
long null_ARRAY_0041eb80;
long null_ARRAY_0041eba0;
long null_ARRAY_0041ebc0;
long null_ARRAY_0041ec40;
long null_ARRAY_0041ecc0;
long null_ARRAY_0041edc0;
long null_ARRAY_0041f240;
long null_ARRAY_0041f2a0;
long null_ARRAY_0041f380;
long null_ARRAY_0041f580;
long null_ARRAY_0041f6c0;
long null_ARRAY_0041f840;
long null_ARRAY_0041f980;
long null_ARRAY_0041fcc0;
long null_ARRAY_00420960;
long null_ARRAY_00421700;
long null_ARRAY_00421b80;
long null_ARRAY_00624550;
long null_ARRAY_00624580;
long null_ARRAY_00624660;
long null_ARRAY_006246c0;
long null_ARRAY_00624700;
long null_ARRAY_00624740;
long null_ARRAY_00624840;
long null_ARRAY_00624880;
long null_ARRAY_00624900;
long PTR_DAT_00624520;
long PTR_FUN_00624528;
long PTR_null_ARRAY_00624560;
long PTR_null_ARRAY_006245c8;
long register0x00000020;
long stack0x00000008;
ulong
FUN_00402467 (uint uParm1)
{
  char cVar1;
  byte bVar2;
  int iVar3;
  undefined8 uVar4;
  uint *puVar5;
  long lVar6;
  char *pcVar7;
  uint uVar8;
  undefined auVar9[16];
  undefined auStack272[144];
  ulong uStack128;
  long lStack120;
  ulong uStack112;
  long lStack104;
  ulong uStack96;
  long lStack88;
  uint *puStack80;
  int iStack68;
  long lStack64;
  bool bStack50;
  char cStack49;
  code *pcStack32;
  uint local_c;

  if (uParm1 == 0)
    {
      pcStack32 = (code *) 0x4024b6;
      local_c = uParm1;
      func_0x00401920 ("Usage: %s [OPTION]... FILE...\n", DAT_006246f8);
      pcStack32 = (code *) 0x4024ca;
      func_0x00401af0
	("Update the access and modification times of each FILE to the current time.\n\nA FILE argument that does not exist is created empty, unless -c or -h\nis supplied.\n\nA FILE argument string of - is handled specially and causes touch to\nchange the times of the file associated with standard output.\n",
	 DAT_00624600);
      pcStack32 = (code *) 0x4024cf;
      FUN_00401fbe ();
      pcStack32 = (code *) 0x4024e3;
      func_0x00401af0
	("  -a                     change only the access time\n  -c, --no-create        do not create any files\n  -d, --date=STRING      parse STRING and use it instead of current time\n  -f                     (ignored)\n",
	 DAT_00624600);
      pcStack32 = (code *) 0x4024f7;
      func_0x00401af0
	("  -h, --no-dereference   affect each symbolic link instead of any referenced\n                         file (useful only on systems that can change the\n                         timestamps of a symlink)\n  -m                     change only the modification time\n",
	 DAT_00624600);
      pcStack32 = (code *) 0x40250b;
      func_0x00401af0
	("  -r, --reference=FILE   use this file\'s times instead of current time\n  -t STAMP               use [[CC]YY]MMDDhhmm[.ss] instead of current time\n      --time=WORD        change the specified time:\n                           WORD is access, atime, or use: equivalent to -a\n                           WORD is modify or mtime: equivalent to -m\n",
	 DAT_00624600);
      pcStack32 = (code *) 0x40251f;
      func_0x00401af0 ("      --help     display this help and exit\n",
		       DAT_00624600);
      pcStack32 = (code *) 0x402533;
      func_0x00401af0
	("      --version  output version information and exit\n",
	 DAT_00624600);
      pcStack32 = (code *) 0x402547;
      pcVar7 = (char *) DAT_00624600;
      func_0x00401af0
	("\nNote that the -d and -t options accept different time-date formats.\n");
      pcStack32 = (code *) 0x402551;
      imperfection_wrapper ();	//    FUN_00401fd8();
    }
  else
    {
      pcVar7 = "Try \'%s --help\' for more information.\n";
      pcStack32 = (code *) 0x402498;
      local_c = uParm1;
      func_0x00401ae0 (DAT_00624620,
		       "Try \'%s --help\' for more information.\n",
		       DAT_006246f8);
    }
  pcStack32 = FUN_0040255b;
  uVar8 = local_c;
  func_0x00401d00 ();
  cStack49 = '\0';
  bStack50 = true;
  lStack64 = 0;
  pcStack32 = (code *) & stack0xfffffffffffffff8;
  FUN_0040a638 (*(undefined8 *) pcVar7);
  func_0x00401c80 (6, &DAT_0041de83);
  func_0x00401c60 (FUN_00402fd8);
  DAT_006246a0 = 0;
  DAT_006246a5 = '\0';
  DAT_006246a4 = 0;
LAB_004027d6:
  ;
  do
    {
      imperfection_wrapper ();	//    iStack68 = FUN_0040d5d7((ulong)uVar8, pcVar7, "acd:fhmr:t:", null_ARRAY_0041df00, 0);
      if (iStack68 == -1)
	{
	  if (DAT_006246a0 == 0)
	    {
	      DAT_006246a0 = 3;
	    }
	  if ((cStack49 != '\0')
	      && ((DAT_006246a5 != '\0' || (lStack64 != 0))))
	    {
	      imperfection_wrapper ();	//        FUN_0040c3fe(0, 0, "cannot specify times from more than one source");
	      FUN_00402467 (1);
	    }
	  if (DAT_006246a5 == '\0')
	    {
	      if (lStack64 != 0)
		{
		  FUN_004031c8 (&uStack96);
		  FUN_00402174 (null_ARRAY_006246c0, lStack64, &uStack96);
		  null_ARRAY_006246c_16_8_ = null_ARRAY_006246c_0_8_;
		  null_ARRAY_006246c_24_8_ = null_ARRAY_006246c_8_8_;
		  cStack49 = '\x01';
		  if (((DAT_006246a0 == 3)
		       && (null_ARRAY_006246c_0_8_ == uStack96))
		      && (null_ARRAY_006246c_8_8_ == lStack88))
		    {
		      uStack112 = uStack96 ^ 1;
		      lStack104 = lStack88;
		      FUN_00402174 (&uStack128, lStack64, &uStack112);
		      if ((uStack128 == uStack112)
			  && (lStack120 == lStack104))
			{
			  cStack49 = '\0';
			}
		    }
		}
	    }
	  else
	    {
	      if (DAT_006246a6 == '\0')
		{
		  imperfection_wrapper ();	//          iVar3 = FUN_0040e9a7(DAT_006246e0, auStack272, auStack272);
		}
	      else
		{
		  imperfection_wrapper ();	//          iVar3 = FUN_0040d729(DAT_006246e0, auStack272, auStack272);
		}
	      if (iVar3 != 0)
		{
		  FUN_0040ba32 (4, DAT_006246e0);
		  puVar5 = (uint *) func_0x00401cf0 ();
		  imperfection_wrapper ();	//          FUN_0040c3fe(1, (ulong)*puVar5, "failed to get attributes of %s");
		}
	      imperfection_wrapper ();	//        auVar9 = FUN_0040bba7(auStack272);
	      imperfection_wrapper ();	//        null_ARRAY_006246c0._8_8_ = SUB168(auVar9 >> 0x40, 0);
	      null_ARRAY_006246c_0_8_ = SUB168 (auVar9, 0);
	      imperfection_wrapper ();	//        auVar9 = FUN_0040bbbd(auStack272);
	      imperfection_wrapper ();	//        null_ARRAY_006246c0._24_8_ = SUB168(auVar9 >> 0x40, 0);
	      null_ARRAY_006246c_16_8_ = SUB168 (auVar9, 0);
	      cStack49 = '\x01';
	      if (lStack64 != 0)
		{
		  if ((DAT_006246a0 & 1) != 0)
		    {
		      FUN_00402174 (null_ARRAY_006246c0, lStack64,
				    null_ARRAY_006246c0);
		    }
		  if ((DAT_006246a0 & 2) != 0)
		    {
		      FUN_00402174 (0x6246d0, lStack64, 0x6246d0);
		    }
		}
	    }
	  if (((cStack49 != '\x01') && (1 < (int) (uVar8 - DAT_006245b8)))
	      &&
	      ((iVar3 = FUN_0040a5b4 (), iVar3 < 0x30db0
		&& (cVar1 =
		    FUN_0040a400 (null_ARRAY_006246c0,
				  ((undefined8 *) pcVar7)[(long) (int)
							  DAT_006245b8], 9),
		    cVar1 != '\0'))))
	    {
	      null_ARRAY_006246c_8_8_ = 0;
	      null_ARRAY_006246c_16_8_ = null_ARRAY_006246c_0_8_;
	      null_ARRAY_006246c_24_8_ = 0;
	      cStack49 = '\x01';
	      lVar6 = func_0x00401980 ("POSIXLY_CORRECT");
	      if (1)
		{
		  imperfection_wrapper ();	//          FUN_0040c3fe(0, 0, "warning: \'touch %s\' is obsolete; use \'touch -t %04ld%02d%02d%02d%02d.%02d\'", ((undefined8 *)pcVar7)[(long)(int)DAT_006245b8], (long)(int)puStack80[5] + 0x76c, (ulong)(puStack80[4] + 1), (ulong)puStack80[3], (ulong)puStack80[2], (ulong)puStack80[1], (ulong)*puStack80);
		}
	      DAT_006245b8 = DAT_006245b8 + 1;
	    }
	  if (cStack49 != '\x01')
	    {
	      if (DAT_006246a0 == 3)
		{
		  DAT_006246a7 = 1;
		}
	      else
		{
		  null_ARRAY_006246c_8_8_ = 0x3fffffff;
		  null_ARRAY_006246c_24_8_ = 0x3fffffff;
		}
	    }
	  if (DAT_006245b8 == uVar8)
	    {
	      imperfection_wrapper ();	//        FUN_0040c3fe(0, 0, "missing file operand");
	      FUN_00402467 (1);
	    }
	  while ((int) DAT_006245b8 < (int) uVar8)
	    {
	      bVar2 =
		FUN_004021d0 (((undefined8 *) pcVar7)[(long) (int)
						      DAT_006245b8]);
	      bStack50 = (bVar2 & bStack50) != 0;
	      DAT_006245b8 = DAT_006245b8 + 1;
	    }
	  return (ulong) (bStack50 == false);
	}
    }
  while (iStack68 == 0x66);
  if (iStack68 < 0x67)
    {
      if (iStack68 == 0x61)
	{
	  DAT_006246a0 = DAT_006246a0 | 1;
	  goto LAB_004027d6;
	}
      if (iStack68 < 0x62)
	{
	  if (iStack68 != -0x83)
	    {
	      if (iStack68 != -0x82)
		goto LAB_004027cc;
	      FUN_00402467 (0);
	    }
	  imperfection_wrapper ();	//      FUN_0040c119(DAT_00624600, "touch", "GNU coreutils", PTR_DAT_00624520, "Paul Rubin", "Arnold Robbins", "Jim Kingdon", "David MacKenzie", "Randy Smith", 0);
	  func_0x00401d00 (0);
	}
      else
	{
	  if (iStack68 == 99)
	    {
	      DAT_006246a4 = 1;
	      goto LAB_004027d6;
	    }
	  if (iStack68 == 100)
	    {
	      lStack64 = DAT_00625120;
	      goto LAB_004027d6;
	    }
	}
    }
  else
    {
      if (iStack68 == 0x72)
	{
	  DAT_006246a5 = '\x01';
	  DAT_006246e0 = DAT_00625120;
	  goto LAB_004027d6;
	}
      if (0x72 < iStack68)
	{
	  if (iStack68 == 0x74)
	    {
	      cVar1 = FUN_0040a400 (null_ARRAY_006246c0, DAT_00625120, 6);
	      if (cVar1 != '\x01')
		{
		  imperfection_wrapper ();	//          uVar4 = FUN_0040bb88(DAT_00625120);
		  imperfection_wrapper ();	//          FUN_0040c3fe(1, 0, "invalid date format %s", uVar4);
		}
	      null_ARRAY_006246c_8_8_ = 0;
	      null_ARRAY_006246c_16_8_ = null_ARRAY_006246c_0_8_;
	      null_ARRAY_006246c_24_8_ = 0;
	      cStack49 = '\x01';
	    }
	  else
	    {
	      if (iStack68 != 0x80)
		goto LAB_004027cc;
	      lVar6 =
		FUN_00402f52 ("--time", DAT_00625120, null_ARRAY_0041e020,
			      null_ARRAY_0041e050, 4, PTR_FUN_00624528);
	      DAT_006246a0 =
		DAT_006246a0 | *(uint *) (null_ARRAY_0041e050 + lVar6 * 4);
	    }
	  goto LAB_004027d6;
	}
      if (iStack68 == 0x68)
	{
	  DAT_006246a6 = '\x01';
	  goto LAB_004027d6;
	}
      if (iStack68 == 0x6d)
	{
	  DAT_006246a0 = DAT_006246a0 | 2;
	  goto LAB_004027d6;
	}
    }
LAB_004027cc:
  ;
  imperfection_wrapper ();	//  FUN_00402467();
  goto LAB_004027d6;
}

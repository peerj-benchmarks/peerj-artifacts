typedef unsigned char   undefined;

typedef unsigned char    byte;
typedef unsigned char    dwfenc;
typedef unsigned int    dword;
typedef unsigned long    qword;
typedef unsigned char    undefined1;
typedef unsigned short    undefined2;
typedef unsigned int    undefined4;
typedef unsigned long    undefined6;
typedef unsigned long    undefined8;
typedef unsigned short    word;
#define unkbyte9   unsigned long long
#define unkbyte10   unsigned long long
#define unkbyte11   unsigned long long
#define unkbyte12   unsigned long long
#define unkbyte13   unsigned long long
#define unkbyte14   unsigned long long
#define unkbyte15   unsigned long long
#define unkbyte16   unsigned long long

#define unkuint9   unsigned long long
#define unkuint10   unsigned long long
#define unkuint11   unsigned long long
#define unkuint12   unsigned long long
#define unkuint13   unsigned long long
#define unkuint14   unsigned long long
#define unkuint15   unsigned long long
#define unkuint16   unsigned long long

#define unkint9   long long
#define unkint10   long long
#define unkint11   long long
#define unkint12   long long
#define unkint13   long long
#define unkint14   long long
#define unkint15   long long
#define unkint16   long long

#define unkfloat1   float
#define unkfloat2   float
#define unkfloat3   float
#define unkfloat5   double
#define unkfloat6   double
#define unkfloat7   double
#define unkfloat9   long double
#define unkfloat11   long double
#define unkfloat12   long double
#define unkfloat13   long double
#define unkfloat14   long double
#define unkfloat15   long double
#define unkfloat16   long double

#define BADSPACEBASE   void
#define code   void

#define ulong long
#define true 1
#define bool int
#define false 0
#define float10 int
#define ushort int
#define undefined7 long
#define undefined2 long
#define uint5 long
#define uint6 long

// Add definition for uint
typedef unsigned int    uint;

#define sbyte char

extern imperfection_wrapper(void);
typedef struct eh_frame_hdr eh_frame_hdr, *Peh_frame_hdr;

struct eh_frame_hdr {
    byte eh_frame_hdr_version; // Exception Handler Frame Header Version
    dwfenc eh_frame_pointer_encoding; // Exception Handler Frame Pointer Encoding
    dwfenc eh_frame_desc_entry_count_encoding; // Encoding of # of Exception Handler FDEs
    dwfenc eh_frame_table_encoding; // Exception Handler Table Encoding
};

typedef struct fde_table_entry fde_table_entry, *Pfde_table_entry;

struct fde_table_entry {
    dword initial_loc; // Initial Location
    dword data_loc; // Data location
};

typedef enum Elf64_DynTag {
    DT_AUDIT=1879047932,
    DT_AUXILIARY=2147483645,
    DT_BIND_NOW=24,
    DT_CHECKSUM=1879047672,
    DT_CONFIG=1879047930,
    DT_DEBUG=21,
    DT_DEPAUDIT=1879047931,
    DT_ENCODING=32,
    DT_FEATURE_1=1879047676,
    DT_FILTER=2147483647,
    DT_FINI=13,
    DT_FINI_ARRAY=26,
    DT_FINI_ARRAYSZ=28,
    DT_FLAGS=30,
    DT_FLAGS_1=1879048187,
    DT_GNU_CONFLICT=1879047928,
    DT_GNU_CONFLICTSZ=1879047670,
    DT_GNU_HASH=1879047925,
    DT_GNU_LIBLIST=1879047929,
    DT_GNU_LIBLISTSZ=1879047671,
    DT_GNU_PRELINKED=1879047669,
    DT_HASH=4,
    DT_INIT=12,
    DT_INIT_ARRAY=25,
    DT_INIT_ARRAYSZ=27,
    DT_JMPREL=23,
    DT_MOVEENT=1879047674,
    DT_MOVESZ=1879047675,
    DT_MOVETAB=1879047934,
    DT_NEEDED=1,
    DT_NULL=0,
    DT_PLTGOT=3,
    DT_PLTPAD=1879047933,
    DT_PLTPADSZ=1879047673,
    DT_PLTREL=20,
    DT_PLTRELSZ=2,
    DT_POSFLAG_1=1879047677,
    DT_PREINIT_ARRAYSZ=33,
    DT_REL=17,
    DT_RELA=7,
    DT_RELACOUNT=1879048185,
    DT_RELAENT=9,
    DT_RELASZ=8,
    DT_RELCOUNT=1879048186,
    DT_RELENT=19,
    DT_RELSZ=18,
    DT_RPATH=15,
    DT_RUNPATH=29,
    DT_SONAME=14,
    DT_STRSZ=10,
    DT_STRTAB=5,
    DT_SYMBOLIC=16,
    DT_SYMENT=11,
    DT_SYMINENT=1879047679,
    DT_SYMINFO=1879047935,
    DT_SYMINSZ=1879047678,
    DT_SYMTAB=6,
    DT_TEXTREL=22,
    DT_TLSDESC_GOT=1879047927,
    DT_TLSDESC_PLT=1879047926,
    DT_VERDEF=1879048188,
    DT_VERDEFNUM=1879048189,
    DT_VERNEED=1879048190,
    DT_VERNEEDNUM=1879048191,
    DT_VERSYM=1879048176
} Elf64_DynTag;

typedef enum Elf_ProgramHeaderType {
    PT_DYNAMIC=2,
    PT_GNU_EH_FRAME=1685382480,
    PT_GNU_RELRO=1685382482,
    PT_GNU_STACK=1685382481,
    PT_INTERP=3,
    PT_LOAD=1,
    PT_NOTE=4,
    PT_NULL=0,
    PT_PHDR=6,
    PT_SHLIB=5,
    PT_TLS=7
} Elf_ProgramHeaderType;

typedef struct Elf64_Dyn Elf64_Dyn, *PElf64_Dyn;

struct Elf64_Dyn {
    enum Elf64_DynTag d_tag;
    qword d_val;
};

typedef struct Elf64_Shdr Elf64_Shdr, *PElf64_Shdr;

typedef enum Elf_SectionHeaderType {
    SHT_CHECKSUM=1879048184,
    SHT_DYNAMIC=6,
    SHT_DYNSYM=11,
    SHT_FINI_ARRAY=15,
    SHT_GNU_ATTRIBUTES=1879048181,
    SHT_GNU_HASH=1879048182,
    SHT_GNU_LIBLIST=1879048183,
    SHT_GNU_verdef=1879048189,
    SHT_GNU_verneed=1879048190,
    SHT_GNU_versym=1879048191,
    SHT_GROUP=17,
    SHT_HASH=5,
    SHT_INIT_ARRAY=14,
    SHT_NOBITS=8,
    SHT_NOTE=7,
    SHT_NULL=0,
    SHT_PREINIT_ARRAY=16,
    SHT_PROGBITS=1,
    SHT_REL=9,
    SHT_RELA=4,
    SHT_SHLIB=10,
    SHT_STRTAB=3,
    SHT_SUNW_COMDAT=1879048187,
    SHT_SUNW_move=1879048186,
    SHT_SUNW_syminfo=1879048188,
    SHT_SYMTAB=2,
    SHT_SYMTAB_SHNDX=18
} Elf_SectionHeaderType;

struct Elf64_Shdr {
    dword sh_name;
    enum Elf_SectionHeaderType sh_type;
    qword sh_flags;
    qword sh_addr;
    qword sh_offset;
    qword sh_size;
    dword sh_link;
    dword sh_info;
    qword sh_addralign;
    qword sh_entsize;
};

typedef struct Elf64_Phdr Elf64_Phdr, *PElf64_Phdr;

struct Elf64_Phdr {
    enum Elf_ProgramHeaderType p_type;
    dword p_flags;
    qword p_offset;
    qword p_vaddr;
    qword p_paddr;
    qword p_filesz;
    qword p_memsz;
    qword p_align;
};

typedef struct Elf64_Rela Elf64_Rela, *PElf64_Rela;

struct Elf64_Rela {
    qword r_offset; // location to apply the relocation action
    qword r_info; // the symbol table index and the type of relocation
    qword r_addend; // a constant addend used to compute the relocatable field value
};

typedef struct Elf64_Sym Elf64_Sym, *PElf64_Sym;

struct Elf64_Sym {
    dword st_name;
    byte st_info;
    byte st_other;
    word st_shndx;
    qword st_value;
    qword st_size;
};

typedef struct Elf64_Ehdr Elf64_Ehdr, *PElf64_Ehdr;

struct Elf64_Ehdr {
    byte e_ident_magic_num;
    char e_ident_magic_str[3];
    byte e_ident_class;
    byte e_ident_data;
    byte e_ident_version;
    byte e_ident_pad[9];
    word e_type;
    word e_machine;
    dword e_version;
    qword e_entry;
    qword e_phoff;
    qword e_shoff;
    dword e_flags;
    word e_ehsize;
    word e_phentsize;
    word e_phnum;
    word e_shentsize;
    word e_shnum;
    word e_shstrndx;
};




undefined8 _DT_INIT(void);
void FUN_004018d0(void);
void FUN_00401a70(void);
void FUN_00401ca0(void);
void entry(void);
void FUN_00401e80(void);
void FUN_00401f00(void);
void FUN_00401f80(void);
void FUN_00401fbe(void);
void FUN_00401fd8(undefined *puParm1);
void FUN_00402174(undefined8 uParm1,undefined8 uParm2,undefined8 uParm3);
undefined8 FUN_004021d0(undefined8 uParm1);
ulong FUN_00402467(uint uParm1);
ulong FUN_0040255b(uint uParm1,undefined8 *puParm2);
void FUN_00402c5c(void);
long FUN_00402c6c(undefined8 uParm1,long lParm2,long lParm3,long lParm4);
void FUN_00402d9b(undefined8 uParm1,undefined8 uParm2,long lParm3);
void FUN_00402e1c(long lParm1,long lParm2,long lParm3);
long FUN_00402f52(undefined8 uParm1,undefined8 uParm2,undefined8 uParm3,undefined8 uParm4,undefined8 uParm5,code *pcParm6);
void FUN_00402fd8(void);
ulong FUN_004030bd(uint uParm1,undefined8 uParm2,uint uParm3,uint uParm4);
ulong FUN_00403139(uint uParm1,uint uParm2,long lParm3,undefined8 uParm4,uint uParm5);
void FUN_004031c8(undefined8 *puParm1);
undefined8 FUN_0040321c(void);
ulong FUN_0040322b(byte bParm1);
void FUN_0040323a(undefined4 uParm1,undefined4 uParm2,undefined4 uParm3,undefined4 uParm4,undefined4 uParm5,undefined4 uParm6,undefined4 uParm7,undefined4 uParm8,undefined8 param_1,undefined8 param_2,undefined8 param_3,undefined8 param_4,undefined8 param_5,undefined8 param_6);
void FUN_004032f6(long param_1);
ulong FUN_00403589(long param_1,int param_2);
void FUN_00403e76(long lParm1,undefined8 uParm2,undefined8 uParm3,undefined8 uParm4,int iParm5);
undefined * FUN_00403ec2(long lParm1,undefined *puParm2,int iParm3);
long FUN_00403ff6(int iParm1,long lParm2);
void FUN_004041ec(undefined8 uParm1,long lParm2);
ulong FUN_00404658(byte bParm1,long lParm2,undefined8 uParm3);
void FUN_004046b0(undefined8 uParm1,long lParm2);
void FUN_00404862(void);
ulong FUN_00404886(long lParm1);
undefined8 FUN_00406961(long param_1,long param_2);
ulong FUN_00406c0f(ulong uParm1,int iParm2);
undefined8 FUN_00406c96(int iParm1,undefined8 uParm2);
undefined8 FUN_00406d26(char param_1,int *param_2);
long * FUN_00406ebb(long lParm1,undefined8 uParm2);
char ** FUN_00406f8c(undefined8 uParm1,char *pcParm2);
ulong FUN_004072b9(long *plParm1,byte **ppbParm2);
undefined8 FUN_0040789c(void);
ulong FUN_004078af(undefined8 uParm1,uint *puParm2,uint *puParm3,long lParm4);
long FUN_0040796b(undefined8 uParm1,long lParm2,long lParm3,int iParm4);
undefined8 FUN_00407a57(long lParm1,undefined8 uParm2,int iParm3);
undefined8 FUN_00407ac9(uint *puParm1,undefined8 uParm2,int iParm3);
void FUN_00407b1a(int *piParm1,int *piParm2,long lParm3,char cParm4);
ulong FUN_00407ded(undefined8 uParm1,undefined8 uParm2,undefined8 uParm3);
ulong FUN_00407e69(long *plParm1,byte *pbParm2,long *plParm3,uint uParm4,long lParm5,byte *pbParm6);
undefined8 FUN_0040a052(long lParm1,int *piParm2,long lParm3,uint uParm4);
undefined8 FUN_0040a144(int *piParm1,long lParm2,uint uParm3);
undefined8 FUN_0040a400(long *plParm1,undefined8 uParm2,uint uParm3);
long FUN_0040a5b4(void);
void FUN_0040a638(long lParm1);
ulong FUN_0040a715(undefined1 *puParm1,byte bParm2,uint uParm3);
undefined8 * FUN_0040a79d(undefined8 *puParm1,int iParm2);
char * FUN_0040a814(char *pcParm1,int iParm2);
ulong FUN_0040a8b4(long param_1,ulong param_2,long param_3,ulong param_4,uint param_5,uint param_6,long param_7,char *param_8,char *param_9);
undefined1 * FUN_0040b77e(int iParm1,undefined8 uParm2,undefined8 uParm3,uint *puParm4);
void FUN_0040b9f1(uint uParm1,uint uParm2,undefined8 uParm3);
void FUN_0040ba32(uint uParm1,undefined8 uParm2);
void FUN_0040ba56(undefined8 uParm1,undefined8 uParm2,char cParm3);
void FUN_0040baea(undefined8 uParm1,char cParm2);
void FUN_0040bb14(undefined8 uParm1);
void FUN_0040bb33(uint uParm1,undefined8 uParm2,undefined8 uParm3);
void FUN_0040bb5f(uint uParm1,undefined8 uParm2);
void FUN_0040bb88(undefined8 uParm1);
void FUN_0040bba7(long lParm1);
void FUN_0040bbbd(long lParm1);
ulong FUN_0040bbd3(uint uParm1);
void FUN_0040bbe3(undefined8 uParm1,long lParm2,undefined8 uParm3,undefined8 uParm4,undefined8 *puParm5,undefined8 uParm6);
void FUN_0040c047(undefined8 uParm1,undefined8 uParm2,undefined8 uParm3,undefined8 uParm4,uint *puParm5);
void FUN_0040c119(undefined4 uParm1,undefined4 uParm2,undefined4 uParm3,undefined4 uParm4,undefined4 uParm5,undefined4 uParm6,undefined4 uParm7,undefined4 uParm8,undefined8 param_1,undefined8 param_2,undefined8 param_3,undefined8 param_4,undefined8 param_5,undefined8 param_6);
void FUN_0040c1cf(undefined8 uParm1);
long FUN_0040c1e9(long lParm1);
long FUN_0040c21e(long lParm1,long lParm2);
ulong FUN_0040c27f(void);
ulong FUN_0040c2a9(uint uParm1);
void FUN_0040c2d2(void);
void FUN_0040c306(uint uParm1);
void FUN_0040c37a(uint uParm1,uint uParm2,undefined8 uParm3,undefined8 uParm4);
void FUN_0040c3fe(undefined4 uParm1,undefined4 uParm2,undefined4 uParm3,undefined4 uParm4,undefined4 uParm5,undefined4 uParm6,undefined4 uParm7,undefined4 uParm8,uint param_1,uint param_2,undefined8 param_3,undefined8 param_4,undefined8 param_5,undefined8 param_6);
void FUN_0040c4e2(long lParm1,int *piParm2);
ulong FUN_0040c6c6(int param_1,undefined8 *param_2,char *param_3,long *param_4,int *param_5,int param_6,int *param_7,int param_8,undefined8 param_9);
char * FUN_0040ccb0(undefined8 uParm1,undefined8 uParm2,char *pcParm3,int *piParm4,int iParm5);
ulong FUN_0040cd7e(uint param_1,undefined8 *param_2,char *param_3,long param_4,undefined8 param_5,uint param_6,uint *param_7,uint param_8);
ulong FUN_0040d547(uint param_1,undefined8 param_2,undefined8 param_3,undefined8 param_4,undefined8 param_5,uint param_6,uint param_7);
void FUN_0040d5d7(uint uParm1,undefined8 uParm2,undefined8 uParm3,undefined8 uParm4,undefined8 uParm5);
ulong FUN_0040d621(undefined8 uParm1,undefined8 uParm2);
undefined * FUN_0040d6c8(undefined8 uParm1);
void FUN_0040d704(undefined8 uParm1,undefined8 uParm2);
ulong FUN_0040d729(long lParm1,long lParm2);
undefined8 FUN_0040d7e0(long lParm1);
ulong FUN_0040d811(uint *puParm1,byte *pbParm2,long lParm3,undefined8 uParm4);
long FUN_0040d894(long lParm1,undefined4 uParm2);
ulong FUN_0040d90d(ulong uParm1);
ulong FUN_0040d9b8(int iParm1,int iParm2);
long FUN_0040d9f3(ulong param_1,long param_2,int param_3,int param_4,int param_5,uint param_6,int param_7,int param_8,int param_9,int param_10);
long FUN_0040dc46(undefined8 uParm1,undefined8 uParm2);
long FUN_0040dc95(undefined8 param_1,undefined8 param_2,uint param_3,uint param_4,uint param_5,long param_6,uint *param_7);
void FUN_0040ddfe(code *pcParm1,undefined8 uParm2,undefined8 uParm3);
long FUN_0040de30(undefined8 uParm1,long *plParm2,undefined8 uParm3);
long FUN_0040df73(undefined8 *puParm1,undefined8 uParm2,long *plParm3);
void FUN_0040e75f(undefined8 uParm1);
undefined8 FUN_0040e788(long lParm1,long lParm2);
ulong FUN_0040e7f1(undefined4 uParm1,undefined4 uParm2,undefined4 uParm3,undefined4 uParm4,undefined4 uParm5,undefined4 uParm6,undefined4 uParm7,undefined4 uParm8,long param_1,ulong param_2,undefined8 param_3,undefined8 param_4,undefined8 param_5,undefined8 param_6);
void FUN_0040e982(undefined8 uParm1,undefined8 uParm2);
ulong FUN_0040e9a7(long lParm1,long lParm2);
ulong FUN_0040ea37(int iParm1,int iParm2);
ulong FUN_0040ea72(uint *puParm1,uint *puParm2);
void FUN_0040eb1a(long lParm1,undefined8 uParm2,long lParm3);
undefined8 * FUN_0040eb55(long lParm1);
undefined8 FUN_0040ec08(long **pplParm1,char *pcParm2);
void FUN_0040edd8(undefined8 *puParm1);
void FUN_0040ee19(void);
void FUN_0040ee29(long lParm1);
ulong FUN_0040ee60(long lParm1);
long FUN_0040eea6(long lParm1);
ulong FUN_0040ef72(long lParm1);
undefined8 FUN_0040efdd(long lParm1,undefined8 uParm2,undefined8 uParm3);
long FUN_0040f089(long lParm1,undefined8 uParm2);
void FUN_0040f15e(long lParm1);
void FUN_0040f18d(void);
ulong FUN_0040f218(char *pcParm1);
ulong FUN_0040f28c(void);
long FUN_0040f2d9(ulong uParm1,long lParm2,ulong uParm3,long lParm4,long *plParm5);
int * FUN_0040f525(ulong uParm1,uint *puParm2,ulong uParm3,uint *puParm4,ulong *puParm5);
char * FUN_00410005(ulong uParm1,long lParm2,long lParm3);
long FUN_00410273(int *param_1,long *param_2);
long FUN_0041045e(undefined auParm1 [16],int *piParm2,long *plParm3);
undefined8 FUN_0041081d(int iParm1,long lParm2,uint *puParm3,long lParm4,uint uParm5);
void FUN_00411030(uint param_1);
void FUN_0041107a(undefined8 uParm1,uint uParm2);
ulong FUN_004110cc(void);
ulong FUN_0041145e(undefined auParm1 [16],undefined8 uParm2,undefined8 uParm3);
ulong FUN_00411836(char *pcParm1,long lParm2);
undefined8 FUN_0041188f(long param_1,long param_2,undefined param_3,int param_4,undefined8 param_5,ulong param_6,int param_7,ulong param_8,int param_9);
long FUN_00411b52(long lParm1,char **ppcParm2,long lParm3,undefined8 uParm4);
ulong FUN_00418fbc(undefined8 uParm1,undefined8 uParm2,undefined8 uParm3);
undefined8 FUN_004190dd(int iParm1);
ulong FUN_00419107(int iParm1);
undefined8 FUN_00419127(int iParm1);
ulong FUN_0041914e(uint uParm1);
ulong FUN_0041916d(uint uParm1);
ulong FUN_0041918c(byte *pbParm1,byte *pbParm2);
undefined8 FUN_00419203(undefined8 uParm1);
undefined8 FUN_0041928e(void);
ulong FUN_0041929b(uint uParm1);
undefined1 * FUN_0041936c(void);
char * FUN_0041971f(void);
long FUN_004197ed(long lParm1,long lParm2,long lParm3);
long FUN_00419846(long lParm1,long lParm2,long lParm3);
ulong FUN_0041989f(int iParm1,int iParm2);
void FUN_004198f0(undefined8 uParm1,undefined8 uParm2,undefined8 uParm3,undefined8 uParm4,undefined8 uParm5,uint uParm6);
char * FUN_00419946(char *param_1,long param_2,char *param_3,undefined8 *param_4,byte param_5,undefined8 param_6,undefined8 param_7,uint param_8);
void FUN_0041ba6b(undefined8 uParm1,int *piParm2,undefined8 uParm3);
void FUN_0041bb0c(int *param_1);
ulong FUN_0041bbcb(ulong uParm1,long lParm2);
void FUN_0041bbff(undefined8 uParm1,undefined8 uParm2,undefined8 uParm3);
void FUN_0041bc3a(undefined8 uParm1,undefined8 uParm2,undefined8 uParm3,undefined8 uParm4);
ulong FUN_0041bc8b(ulong uParm1,ulong uParm2);
ulong FUN_0041bca6(undefined8 uParm1);
void FUN_0041bd5e(undefined8 uParm1);
void FUN_0041bd82(void);
ulong FUN_0041bd90(long lParm1);
undefined8 FUN_0041be60(undefined8 uParm1);
void FUN_0041be7f(undefined8 uParm1,undefined8 uParm2,uint uParm3);
undefined8 FUN_0041beaa(uint *puParm1,ulong *puParm2);
undefined8 FUN_0041c64a(char *pcParm1,ulong *puParm2,char **ppcParm3);
ulong FUN_0041d900(char *pcParm1,char *pcParm2,uint uParm3);
ulong FUN_0041da93(undefined auParm1 [16]);
ulong FUN_0041dace(void);
undefined8 * FUN_0041db00(ulong uParm1);
void FUN_0041dbc3(ulong uParm1);
void FUN_0041dca0(void);
undefined8 _DT_FINI(void);
undefined FUN_00626000();
undefined FUN_00626008();
undefined FUN_00626010();
undefined FUN_00626018();
undefined FUN_00626020();
undefined FUN_00626028();
undefined FUN_00626030();
undefined FUN_00626038();
undefined FUN_00626040();
undefined FUN_00626048();
undefined FUN_00626050();
undefined FUN_00626058();
undefined FUN_00626060();
undefined FUN_00626068();
undefined FUN_00626070();
undefined FUN_00626078();
undefined FUN_00626080();
undefined FUN_00626088();
undefined FUN_00626090();
undefined FUN_00626098();
undefined FUN_006260a0();
undefined FUN_006260a8();
undefined FUN_006260b0();
undefined FUN_006260b8();
undefined FUN_006260c0();
undefined FUN_006260c8();
undefined FUN_006260d0();
undefined FUN_006260d8();
undefined FUN_006260e0();
undefined FUN_006260e8();
undefined FUN_006260f0();
undefined FUN_006260f8();
undefined FUN_00626100();
undefined FUN_00626108();
undefined FUN_00626110();
undefined FUN_00626118();
undefined FUN_00626120();
undefined FUN_00626128();
undefined FUN_00626130();
undefined FUN_00626138();
undefined FUN_00626140();
undefined FUN_00626148();
undefined FUN_00626150();
undefined FUN_00626158();
undefined FUN_00626160();
undefined FUN_00626168();
undefined FUN_00626170();
undefined FUN_00626178();
undefined FUN_00626180();
undefined FUN_00626188();
undefined FUN_00626190();
undefined FUN_00626198();
undefined FUN_006261a0();
undefined FUN_006261a8();
undefined FUN_006261b0();
undefined FUN_006261b8();
undefined FUN_006261c0();
undefined FUN_006261c8();
undefined FUN_006261d0();
undefined FUN_006261d8();
undefined FUN_006261e0();
undefined FUN_006261e8();
undefined FUN_006261f0();
undefined FUN_006261f8();
undefined FUN_00626200();
undefined FUN_00626208();
undefined FUN_00626210();
undefined FUN_00626218();
undefined FUN_00626220();
undefined FUN_00626228();
undefined FUN_00626230();
undefined FUN_00626238();
undefined FUN_00626240();
undefined FUN_00626248();
undefined FUN_00626250();
undefined FUN_00626258();
undefined FUN_00626260();
undefined FUN_00626268();
undefined FUN_00626270();
undefined FUN_00626278();
undefined FUN_00626280();
undefined FUN_00626288();
undefined FUN_00626290();
undefined FUN_00626298();



#include "ln.h"

long null_ARRAY_0061f93_0_8_;
long null_ARRAY_0061f93_8_8_;
long null_ARRAY_0061fac_0_8_;
long null_ARRAY_0061fac_16_8_;
long null_ARRAY_0061fac_24_8_;
long null_ARRAY_0061fac_32_8_;
long null_ARRAY_0061fac_40_8_;
long null_ARRAY_0061fac_48_8_;
long null_ARRAY_0061fac_8_8_;
long null_ARRAY_0061fc2_0_4_;
long null_ARRAY_0061fc2_16_8_;
long null_ARRAY_0061fc2_4_4_;
long null_ARRAY_0061fc2_8_4_;
long DAT_0041a84e;
long DAT_0041a905;
long DAT_0041a983;
long DAT_0041b6ce;
long DAT_0041b762;
long DAT_0041b84b;
long DAT_0041b891;
long DAT_0041b894;
long DAT_0041b898;
long DAT_0041b8b5;
long DAT_0041b8d4;
long DAT_0041b8d6;
long DAT_0041b9e1;
long DAT_0041b9fd;
long DAT_0041ba13;
long DAT_0041bb10;
long DAT_0041bc5e;
long DAT_0041bc62;
long DAT_0041bc67;
long DAT_0041bc69;
long DAT_0041c2c0;
long DAT_0041c2e0;
long DAT_0041c3ab;
long DAT_0041c760;
long DAT_0041c778;
long DAT_0041c77d;
long DAT_0041c790;
long DAT_0041c792;
long DAT_0041c794;
long DAT_0041c7ef;
long DAT_0041c880;
long DAT_0041c883;
long DAT_0041c8d1;
long DAT_0041c92a;
long DAT_0041c92f;
long DAT_0041c9a0;
long DAT_0041c9a1;
long DAT_0041c9df;
long DAT_0061f430;
long DAT_0061f440;
long DAT_0061f450;
long DAT_0061f900;
long DAT_0061f901;
long DAT_0061f910;
long DAT_0061f920;
long DAT_0061f998;
long DAT_0061f99c;
long DAT_0061f9a0;
long DAT_0061f9c0;
long DAT_0061f9c8;
long DAT_0061f9d0;
long DAT_0061f9e0;
long DAT_0061f9e8;
long DAT_0061fa00;
long DAT_0061fa08;
long DAT_0061fa50;
long DAT_0061fa54;
long DAT_0061fa55;
long DAT_0061fa56;
long DAT_0061fa57;
long DAT_0061fa58;
long DAT_0061fa59;
long DAT_0061fa60;
long DAT_0061fa68;
long DAT_0061fa70;
long DAT_0061fa78;
long DAT_0061fa80;
long DAT_0061fa88;
long DAT_0061fc00;
long DAT_0061fc58;
long DAT_0061fc5c;
long DAT_0061fc60;
long DAT_0061fcb8;
long DAT_0061fcc0;
long DAT_0061fcd0;
long fde_0041da70;
long FLOAT_UNKNOWN;
long null_ARRAY_0041aa80;
long null_ARRAY_0041b888;
long null_ARRAY_0041b940;
long null_ARRAY_0041b9a0;
long null_ARRAY_0041ba20;
long null_ARRAY_0041ce20;
long null_ARRAY_0041d020;
long null_ARRAY_0061f930;
long null_ARRAY_0061f960;
long null_ARRAY_0061fa20;
long null_ARRAY_0061fac0;
long null_ARRAY_0061fb00;
long null_ARRAY_0061fc20;
long PTR_DAT_0061f908;
long PTR_FUN_0061f9a8;
long PTR_null_ARRAY_0061f940;
long PTR_null_ARRAY_0061f9b0;
long stack0x00000008;
ulong
FUN_00403172 (uint uParm1)
{
  char cVar1;
  byte bVar2;
  int iVar3;
  uint *puVar4;
  char *pcVar5;
  uint uVar6;
  undefined8 uVar7;
  undefined auStack272[24];
  uint uStack248;
  undefined8 uStack120;
  undefined8 uStack112;
  undefined8 *puStack104;
  int iStack96;
  int iStack92;
  int iStack88;
  char cStack81;
  undefined *puStack80;
  undefined *puStack72;
  undefined *puStack64;
  char cStack50;
  bool bStack49;
  code *pcStack32;
  uint local_c;

  if (uParm1 == 0)
    {
      pcStack32 = (code *) 0x4031d9;
      local_c = uParm1;
      func_0x00401d50
	("Usage: %s [OPTION]... [-T] TARGET LINK_NAME   (1st form)\n  or:  %s [OPTION]... TARGET                  (2nd form)\n  or:  %s [OPTION]... TARGET... DIRECTORY     (3rd form)\n  or:  %s [OPTION]... -t DIRECTORY TARGET...  (4th form)\n",
	 DAT_0061fa88, DAT_0061fa88, DAT_0061fa88, DAT_0061fa88);
      pcStack32 = (code *) 0x4031ed;
      func_0x00401fd0
	("In the 1st form, create a link to TARGET with the name LINK_NAME.\nIn the 2nd form, create a link to TARGET in the current directory.\nIn the 3rd and 4th forms, create links to each TARGET in DIRECTORY.\nCreate hard links by default, symbolic links with --symbolic.\nBy default, each destination (name of new link) should not already exist.\nWhen creating hard links, each TARGET must exist.  Symbolic links\ncan hold arbitrary text; if later resolved, a relative link is\ninterpreted in relation to its parent directory.\n",
	 DAT_0061f9c0);
      pcStack32 = (code *) 0x4031f2;
      FUN_004024ee ();
      pcStack32 = (code *) 0x403206;
      func_0x00401fd0
	("      --backup[=CONTROL]      make a backup of each existing destination file\n  -b                          like --backup but does not accept an argument\n  -d, -F, --directory         allow the superuser to attempt to hard link\n                                directories (note: will probably fail due to\n                                system restrictions, even for the superuser)\n  -f, --force                 remove existing destination files\n",
	 DAT_0061f9c0);
      pcStack32 = (code *) 0x40321a;
      func_0x00401fd0
	("  -i, --interactive           prompt whether to remove destinations\n  -L, --logical               dereference TARGETs that are symbolic links\n  -n, --no-dereference        treat LINK_NAME as a normal file if\n                                it is a symbolic link to a directory\n  -P, --physical              make hard links directly to symbolic links\n  -r, --relative              create symbolic links relative to link location\n  -s, --symbolic              make symbolic links instead of hard links\n",
	 DAT_0061f9c0);
      pcStack32 = (code *) 0x40322e;
      func_0x00401fd0
	("  -S, --suffix=SUFFIX         override the usual backup suffix\n  -t, --target-directory=DIRECTORY  specify the DIRECTORY in which to create\n                                the links\n  -T, --no-target-directory   treat LINK_NAME as a normal file always\n  -v, --verbose               print name of each linked file\n",
	 DAT_0061f9c0);
      pcStack32 = (code *) 0x403242;
      func_0x00401fd0 ("      --help     display this help and exit\n",
		       DAT_0061f9c0);
      pcStack32 = (code *) 0x403256;
      func_0x00401fd0
	("      --version  output version information and exit\n",
	 DAT_0061f9c0);
      pcStack32 = (code *) 0x40325b;
      FUN_00402508 ();
      pcVar5 = &DAT_0041b6ce;
      pcStack32 = (code *) 0x40326f;
      func_0x00401d50
	("\nUsing -s ignores -L and -P.  Otherwise, the last option specified controls\nbehavior when a TARGET is a symbolic link, defaulting to %s.\n");
      pcStack32 = (code *) 0x403279;
      imperfection_wrapper ();	//    FUN_00402536();
    }
  else
    {
      pcVar5 = "Try \'%s --help\' for more information.\n";
      pcStack32 = (code *) 0x4031a3;
      local_c = uParm1;
      func_0x00401fc0 (DAT_0061f9e0,
		       "Try \'%s --help\' for more information.\n",
		       DAT_0061fa88);
    }
  pcStack32 = FUN_00403283;
  uVar6 = local_c;
  func_0x00402220 ();
  cStack50 = '\0';
  puStack64 = (undefined *) 0x0;
  puStack72 = (undefined *) 0x0;
  puStack80 = (undefined *) 0x0;
  cStack81 = '\0';
  pcStack32 = (code *) & stack0xfffffffffffffff8;
  FUN_00406bb8 (*(undefined8 *) pcVar5);
  func_0x004021b0 (6, &DAT_0041a983);
  func_0x00402190 (FUN_00405293);
  DAT_0061fa59 = 0;
  DAT_0061fa58 = 0;
  DAT_0061fa56 = 0;
  DAT_0061fa57 = '\0';
  DAT_0061fa54 = '\0';
LAB_004035ac:
  ;
  uVar7 = 0x4035d0;
  imperfection_wrapper ();	//  iStack96 = FUN_0040b0c9((ulong)uVar6, pcVar5, "bdfinrst:vFLPS:T", null_ARRAY_0041aa80, 0);
  if (iStack96 != -1)
    {
      if (iStack96 != 0x62)
	{
	  if (iStack96 < 99)
	    {
	      if (iStack96 == 0x4c)
		{
		  DAT_0061f900 = 1;
		  goto LAB_004035ac;
		}
	      if (iStack96 < 0x4d)
		{
		  if (iStack96 == -0x82)
		    {
		      uVar7 = 0x40355a;
		      FUN_00403172 (0);
		    }
		  else
		    {
		      if (iStack96 == 0x46)
			{
			LAB_004033ff:
			  ;
			  DAT_0061fa59 = 1;
			  goto LAB_004035ac;
			}
		      if (iStack96 != -0x83)
			goto LAB_004035a2;
		    }
		  imperfection_wrapper ();	//          FUN_00408f3b(DAT_0061f9c0, &DAT_0041b762, "GNU coreutils", PTR_DAT_0061f908, "Mike Parker", "David MacKenzie", 0, uVar7);
		  func_0x00402220 (0);
		}
	      else
		{
		  if (iStack96 == 0x53)
		    {
		      cStack50 = '\x01';
		      puStack64 = DAT_0061fcd0;
		      goto LAB_004035ac;
		    }
		  if (iStack96 == 0x54)
		    {
		      cStack81 = '\x01';
		      goto LAB_004035ac;
		    }
		  if (iStack96 == 0x50)
		    {
		      DAT_0061f900 = 0;
		      goto LAB_004035ac;
		    }
		}
	    }
	  else
	    {
	      if (iStack96 == 0x6e)
		{
		  DAT_0061f901 = 0;
		  goto LAB_004035ac;
		}
	      if (iStack96 < 0x6f)
		{
		  if (iStack96 == 0x66)
		    {
		      DAT_0061fa57 = '\x01';
		      DAT_0061fa56 = 0;
		      goto LAB_004035ac;
		    }
		  if (iStack96 == 0x69)
		    {
		      DAT_0061fa57 = '\0';
		      DAT_0061fa56 = 1;
		      goto LAB_004035ac;
		    }
		  if (iStack96 == 100)
		    goto LAB_004033ff;
		}
	      else
		{
		  if (iStack96 == 0x73)
		    {
		      DAT_0061fa54 = '\x01';
		      goto LAB_004035ac;
		    }
		  if (iStack96 < 0x74)
		    {
		      if (iStack96 == 0x72)
			{
			  DAT_0061fa55 = '\x01';
			  goto LAB_004035ac;
			}
		    }
		  else
		    {
		      if (iStack96 == 0x74)
			{
			  if (puStack80 != (undefined *) 0x0)
			    {
			      imperfection_wrapper ();	//                FUN_00409384(1, 0, "multiple target directories specified");
			    }
			  imperfection_wrapper ();	//              iVar3 = FUN_0040bed3(DAT_0061fcd0, auStack272, auStack272);
			  if (iVar3 != 0)
			    {
			      imperfection_wrapper ();	//                uVar7 = FUN_00407fb2(4, DAT_0061fcd0);
			      puVar4 = (uint *) func_0x00402210 ();
			      imperfection_wrapper ();	//                FUN_00409384(1, (ulong)*puVar4, "failed to access %s", uVar7);
			    }
			  if ((uStack248 & 0xf000) != 0x4000)
			    {
			      imperfection_wrapper ();	//                uVar7 = FUN_00407fb2(4, DAT_0061fcd0);
			      imperfection_wrapper ();	//                FUN_00409384(1, 0, "target %s is not a directory", uVar7);
			    }
			  puStack80 = DAT_0061fcd0;
			  goto LAB_004035ac;
			}
		      if (iStack96 == 0x76)
			{
			  DAT_0061fa58 = 1;
			  goto LAB_004035ac;
			}
		    }
		}
	    }
	LAB_004035a2:
	  ;
	  imperfection_wrapper ();	//      FUN_00403172();
	  goto LAB_004035ac;
	}
      cStack50 = '\x01';
      if (DAT_0061fcd0 != (undefined *) 0x0)
	{
	  puStack72 = DAT_0061fcd0;
	}
      goto LAB_004035ac;
    }
  iStack88 = uVar6 - DAT_0061f998;
  puStack104 = (undefined8 *) pcVar5 + (long) DAT_0061f998;
  if (iStack88 < 1)
    {
      imperfection_wrapper ();	//    FUN_00409384(0, 0, "missing file operand");
      FUN_00403172 (1);
    }
  if (cStack81 != '\0')
    {
      if (puStack80 != (undefined *) 0x0)
	{
	  imperfection_wrapper ();	//      FUN_00409384(1, 0, "cannot combine --target-directory and --no-target-directory");
	}
      if (iStack88 == 2)
	goto LAB_0040378f;
      if (iStack88 < 2)
	{
	  imperfection_wrapper ();	//      uVar7 = FUN_00407fb2(4, *puStack104);
	  imperfection_wrapper ();	//      FUN_00409384(0, 0, "missing destination file operand after %s", uVar7);
	}
      else
	{
	  imperfection_wrapper ();	//      uVar7 = FUN_00407fb2(4, puStack104[2]);
	  imperfection_wrapper ();	//      FUN_00409384(0, 0, "extra operand %s", uVar7);
	}
      FUN_00403172 (1);
    }
  if (puStack80 == (undefined *) 0x0)
    {
      if (iStack88 < 2)
	{
	  puStack80 = &DAT_0041b84b;
	}
      else
	{
	  if ((iStack88 < 2)
	      || (cVar1 =
		  FUN_00402702 (puStack104[(long) iStack88 + -1]),
		  cVar1 == '\0'))
	    {
	      if (2 < iStack88)
		{
		  imperfection_wrapper ();	//          uVar7 = FUN_00407fb2(4, puStack104[(long)iStack88 + -1]);
		  imperfection_wrapper ();	//          FUN_00409384(1, 0, "target %s is not a directory", uVar7);
		}
	    }
	  else
	    {
	      iStack88 = iStack88 + -1;
	      puStack80 = (undefined *) puStack104[(long) iStack88];
	    }
	}
    }
LAB_0040378f:
  ;
  if (cStack50 == '\0')
    {
      DAT_0061fa50 = 0;
    }
  else
    {
      imperfection_wrapper ();	//    DAT_0061fa50 = FUN_00404974("backup type", puStack72);
    }
  FUN_004040c0 (puStack64);
  if ((DAT_0061fa55 != '\0') && (DAT_0061fa54 != '\x01'))
    {
      imperfection_wrapper ();	//    FUN_00409384(1, 0, "cannot do --relative without --symbolic");
    }
  if (puStack80 == (undefined *) 0x0)
    {
      imperfection_wrapper ();	//    bStack49 = (bool)FUN_0040295f(*puStack104, puStack104[1], puStack104[1]);
    }
  else
    {
      if ((((1 < iStack88) && (DAT_0061fa57 != '\0'))
	   && (DAT_0061fa54 != '\x01')) && ((DAT_0061fa50 != 3
					     && (DAT_0061fa60 =
						 FUN_00405e37 (0x3d, 0,
							       FUN_00406a4b,
							       FUN_00406a97,
							       FUN_00406b87),
						 DAT_0061fa60 == 0))))
	{
	  FUN_00409102 ();
	}
      bStack49 = true;
      iStack92 = 0;
      while (iStack92 < iStack88)
	{
	  uVar7 = FUN_0040569a (puStack104[(long) iStack92]);
	  imperfection_wrapper ();	//      uStack112 = FUN_004058ad(puStack80, uVar7, &uStack120, uVar7);
	  FUN_00405751 (uStack120);
	  imperfection_wrapper ();	//      bVar2 = FUN_0040295f(puStack104[(long)iStack92], uStack112, uStack112);
	  bStack49 = (bVar2 & bStack49) != 0;
	  imperfection_wrapper ();	//      FUN_00402350(uStack112);
	  iStack92 = iStack92 + 1;
	}
    }
  return (ulong) (bStack49 == false);
}

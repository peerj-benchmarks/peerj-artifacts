typedef unsigned char   undefined;

typedef unsigned char    byte;
typedef unsigned char    dwfenc;
typedef unsigned int    dword;
typedef unsigned long    qword;
typedef unsigned char    undefined1;
typedef unsigned short    undefined2;
typedef unsigned int    undefined4;
typedef unsigned long    undefined6;
typedef unsigned long    undefined8;
typedef unsigned short    word;
#define unkbyte9   unsigned long long
#define unkbyte10   unsigned long long
#define unkbyte11   unsigned long long
#define unkbyte12   unsigned long long
#define unkbyte13   unsigned long long
#define unkbyte14   unsigned long long
#define unkbyte15   unsigned long long
#define unkbyte16   unsigned long long

#define unkuint9   unsigned long long
#define unkuint10   unsigned long long
#define unkuint11   unsigned long long
#define unkuint12   unsigned long long
#define unkuint13   unsigned long long
#define unkuint14   unsigned long long
#define unkuint15   unsigned long long
#define unkuint16   unsigned long long

#define unkint9   long long
#define unkint10   long long
#define unkint11   long long
#define unkint12   long long
#define unkint13   long long
#define unkint14   long long
#define unkint15   long long
#define unkint16   long long

#define unkfloat1   float
#define unkfloat2   float
#define unkfloat3   float
#define unkfloat5   double
#define unkfloat6   double
#define unkfloat7   double
#define unkfloat9   long double
#define unkfloat11   long double
#define unkfloat12   long double
#define unkfloat13   long double
#define unkfloat14   long double
#define unkfloat15   long double
#define unkfloat16   long double

#define BADSPACEBASE   void
#define code   void

#define ulong long
#define true 1
#define bool int
#define false 0
#define float10 int
#define ushort int
#define undefined7 long
#define undefined2 long
#define uint5 long
#define uint6 long

// Add definition for uint
typedef unsigned int    uint;

#define sbyte char

extern imperfection_wrapper(void);
typedef struct eh_frame_hdr eh_frame_hdr, *Peh_frame_hdr;

struct eh_frame_hdr {
    byte eh_frame_hdr_version; // Exception Handler Frame Header Version
    dwfenc eh_frame_pointer_encoding; // Exception Handler Frame Pointer Encoding
    dwfenc eh_frame_desc_entry_count_encoding; // Encoding of # of Exception Handler FDEs
    dwfenc eh_frame_table_encoding; // Exception Handler Table Encoding
};

typedef struct fde_table_entry fde_table_entry, *Pfde_table_entry;

struct fde_table_entry {
    dword initial_loc; // Initial Location
    dword data_loc; // Data location
};

typedef enum Elf64_DynTag {
    DT_AUDIT=1879047932,
    DT_AUXILIARY=2147483645,
    DT_BIND_NOW=24,
    DT_CHECKSUM=1879047672,
    DT_CONFIG=1879047930,
    DT_DEBUG=21,
    DT_DEPAUDIT=1879047931,
    DT_ENCODING=32,
    DT_FEATURE_1=1879047676,
    DT_FILTER=2147483647,
    DT_FINI=13,
    DT_FINI_ARRAY=26,
    DT_FINI_ARRAYSZ=28,
    DT_FLAGS=30,
    DT_FLAGS_1=1879048187,
    DT_GNU_CONFLICT=1879047928,
    DT_GNU_CONFLICTSZ=1879047670,
    DT_GNU_HASH=1879047925,
    DT_GNU_LIBLIST=1879047929,
    DT_GNU_LIBLISTSZ=1879047671,
    DT_GNU_PRELINKED=1879047669,
    DT_HASH=4,
    DT_INIT=12,
    DT_INIT_ARRAY=25,
    DT_INIT_ARRAYSZ=27,
    DT_JMPREL=23,
    DT_MOVEENT=1879047674,
    DT_MOVESZ=1879047675,
    DT_MOVETAB=1879047934,
    DT_NEEDED=1,
    DT_NULL=0,
    DT_PLTGOT=3,
    DT_PLTPAD=1879047933,
    DT_PLTPADSZ=1879047673,
    DT_PLTREL=20,
    DT_PLTRELSZ=2,
    DT_POSFLAG_1=1879047677,
    DT_PREINIT_ARRAYSZ=33,
    DT_REL=17,
    DT_RELA=7,
    DT_RELACOUNT=1879048185,
    DT_RELAENT=9,
    DT_RELASZ=8,
    DT_RELCOUNT=1879048186,
    DT_RELENT=19,
    DT_RELSZ=18,
    DT_RPATH=15,
    DT_RUNPATH=29,
    DT_SONAME=14,
    DT_STRSZ=10,
    DT_STRTAB=5,
    DT_SYMBOLIC=16,
    DT_SYMENT=11,
    DT_SYMINENT=1879047679,
    DT_SYMINFO=1879047935,
    DT_SYMINSZ=1879047678,
    DT_SYMTAB=6,
    DT_TEXTREL=22,
    DT_TLSDESC_GOT=1879047927,
    DT_TLSDESC_PLT=1879047926,
    DT_VERDEF=1879048188,
    DT_VERDEFNUM=1879048189,
    DT_VERNEED=1879048190,
    DT_VERNEEDNUM=1879048191,
    DT_VERSYM=1879048176
} Elf64_DynTag;

typedef enum Elf_ProgramHeaderType {
    PT_DYNAMIC=2,
    PT_GNU_EH_FRAME=1685382480,
    PT_GNU_RELRO=1685382482,
    PT_GNU_STACK=1685382481,
    PT_INTERP=3,
    PT_LOAD=1,
    PT_NOTE=4,
    PT_NULL=0,
    PT_PHDR=6,
    PT_SHLIB=5,
    PT_TLS=7
} Elf_ProgramHeaderType;

typedef struct Elf64_Dyn Elf64_Dyn, *PElf64_Dyn;

struct Elf64_Dyn {
    enum Elf64_DynTag d_tag;
    qword d_val;
};

typedef struct Elf64_Shdr Elf64_Shdr, *PElf64_Shdr;

typedef enum Elf_SectionHeaderType {
    SHT_CHECKSUM=1879048184,
    SHT_DYNAMIC=6,
    SHT_DYNSYM=11,
    SHT_FINI_ARRAY=15,
    SHT_GNU_ATTRIBUTES=1879048181,
    SHT_GNU_HASH=1879048182,
    SHT_GNU_LIBLIST=1879048183,
    SHT_GNU_verdef=1879048189,
    SHT_GNU_verneed=1879048190,
    SHT_GNU_versym=1879048191,
    SHT_GROUP=17,
    SHT_HASH=5,
    SHT_INIT_ARRAY=14,
    SHT_NOBITS=8,
    SHT_NOTE=7,
    SHT_NULL=0,
    SHT_PREINIT_ARRAY=16,
    SHT_PROGBITS=1,
    SHT_REL=9,
    SHT_RELA=4,
    SHT_SHLIB=10,
    SHT_STRTAB=3,
    SHT_SUNW_COMDAT=1879048187,
    SHT_SUNW_move=1879048186,
    SHT_SUNW_syminfo=1879048188,
    SHT_SYMTAB=2,
    SHT_SYMTAB_SHNDX=18
} Elf_SectionHeaderType;

struct Elf64_Shdr {
    dword sh_name;
    enum Elf_SectionHeaderType sh_type;
    qword sh_flags;
    qword sh_addr;
    qword sh_offset;
    qword sh_size;
    dword sh_link;
    dword sh_info;
    qword sh_addralign;
    qword sh_entsize;
};

typedef struct Elf64_Rela Elf64_Rela, *PElf64_Rela;

struct Elf64_Rela {
    qword r_offset; // location to apply the relocation action
    qword r_info; // the symbol table index and the type of relocation
    qword r_addend; // a constant addend used to compute the relocatable field value
};

typedef struct Elf64_Phdr Elf64_Phdr, *PElf64_Phdr;

struct Elf64_Phdr {
    enum Elf_ProgramHeaderType p_type;
    dword p_flags;
    qword p_offset;
    qword p_vaddr;
    qword p_paddr;
    qword p_filesz;
    qword p_memsz;
    qword p_align;
};

typedef struct Elf64_Sym Elf64_Sym, *PElf64_Sym;

struct Elf64_Sym {
    dword st_name;
    byte st_info;
    byte st_other;
    word st_shndx;
    qword st_value;
    qword st_size;
};

typedef struct Elf64_Ehdr Elf64_Ehdr, *PElf64_Ehdr;

struct Elf64_Ehdr {
    byte e_ident_magic_num;
    char e_ident_magic_str[3];
    byte e_ident_class;
    byte e_ident_data;
    byte e_ident_version;
    byte e_ident_pad[9];
    word e_type;
    word e_machine;
    dword e_version;
    qword e_entry;
    qword e_phoff;
    qword e_shoff;
    dword e_flags;
    word e_ehsize;
    word e_phentsize;
    word e_phnum;
    word e_shentsize;
    word e_shnum;
    word e_shstrndx;
};




undefined8 _DT_INIT(void);
void FUN_00401d10(void);
void FUN_00401ec0(void);
void FUN_00402080(void);
void FUN_00402350(void);
void entry(void);
void FUN_004023b0(void);
void FUN_00402430(void);
void FUN_004024b0(void);
void FUN_004024ee(void);
void FUN_00402508(void);
void FUN_00402536(undefined *puParm1);
ulong FUN_004026d2(int iParm1);
ulong FUN_00402702(undefined8 uParm1);
long FUN_0040287e(undefined8 uParm1,undefined8 uParm2);
ulong FUN_0040295f(char *pcParm1,undefined8 uParm2);
ulong FUN_00403172(uint uParm1);
ulong FUN_00403283(uint uParm1,undefined8 *puParm2);
long FUN_0040393f(long lParm1,long lParm2);
void FUN_004039d3(undefined8 uParm1,uint *puParm2);
ulong FUN_00403a17(uint uParm1,undefined8 uParm2,uint uParm3,undefined8 uParm4,uint uParm5,char cParm6);
void FUN_00403bba(undefined8 uParm1,undefined8 *puParm2);
ulong FUN_00403bf0(undefined8 uParm1,uint uParm2,undefined8 uParm3,char cParm4);
ulong FUN_00403d63(char *pcParm1,char *pcParm2);
undefined8 FUN_00403e48(undefined8 uParm1,long *plParm2,ulong *puParm3);
ulong FUN_00403ef3(long lParm1,long lParm2,undefined8 uParm3,undefined8 uParm4);
void FUN_004040c0(char *pcParm1);
void FUN_00404119(long lParm1,long lParm2,undefined uParm3);
ulong FUN_0040425a(long *plParm1,undefined8 uParm2,long lParm3,long lParm4,long *plParm5);
long FUN_00404621(long lParm1,int iParm2,char cParm3);
long FUN_004048dd(undefined8 uParm1,uint uParm2);
ulong FUN_00404918(undefined8 uParm1,char *pcParm2);
void FUN_00404974(undefined8 uParm1,char *pcParm2);
void FUN_004049c4(undefined8 uParm1);
ulong FUN_004049e3(long *plParm1,undefined8 uParm2,undefined8 uParm3);
char * FUN_00404a8d(char *pcParm1,uint uParm2);
void FUN_00405293(void);
void FUN_00405397(void);
long FUN_0040547c(undefined8 uParm1);
long FUN_0040554c(undefined8 uParm1);
ulong FUN_0040557a(char *pcParm1);
undefined * FUN_00405603(undefined8 uParm1);
char * FUN_0040569a(char *pcParm1);
ulong FUN_00405703(long lParm1);
ulong FUN_00405751(char *pcParm1);
void FUN_004057b6(long lParm1,undefined8 uParm2,undefined8 *puParm3);
ulong FUN_00405851(long lParm1,undefined8 uParm2,undefined8 *puParm3);
long FUN_004058ad(undefined8 uParm1,undefined8 uParm2,undefined8 uParm3);
char * FUN_004058ee(char *pcParm1);
long FUN_0040590e(long lParm1,char *pcParm2,undefined8 *puParm3);
long FUN_00405a5b(long *plParm1,undefined8 uParm2);
long FUN_00405ab2(long lParm1,long lParm2);
ulong FUN_00405b45(ulong uParm1);
ulong FUN_00405bb1(ulong uParm1);
ulong FUN_00405bf8(undefined8 uParm1,ulong uParm2);
ulong FUN_00405c2f(ulong uParm1,ulong uParm2);
undefined8 FUN_00405c48(long lParm1);
ulong FUN_00405d41(ulong uParm1,long lParm2);
long * FUN_00405e37(undefined8 uParm1,undefined1 *puParm2,code *pcParm3,code *pcParm4,long lParm5);
void FUN_00405f9f(long **pplParm1,undefined8 uParm2);
long FUN_004060c9(long lParm1);
void FUN_00406114(long lParm1,undefined8 *puParm2);
long FUN_00406149(long lParm1,long lParm2,long **pplParm3,char cParm4);
undefined8 FUN_004062de(long lParm1,long **pplParm2,char cParm3);
undefined8 FUN_004064ac(long *plParm1,undefined8 uParm2);
undefined8 FUN_004066b0(long lParm1,long lParm2,long *plParm3);
undefined8 FUN_00406a02(undefined8 uParm1,undefined8 uParm2);
ulong FUN_00406a4b(undefined8 *puParm1,ulong uParm2);
ulong FUN_00406a97(undefined8 *puParm1,undefined8 *puParm2);
ulong FUN_00406b0f(undefined8 *puParm1,undefined8 *puParm2);
void FUN_00406b87(undefined8 *puParm1);
void FUN_00406bb8(long lParm1);
ulong FUN_00406c95(undefined1 *puParm1,byte bParm2,uint uParm3);
undefined8 * FUN_00406d1d(undefined8 *puParm1,int iParm2);
char * FUN_00406d94(char *pcParm1,int iParm2);
ulong FUN_00406e34(long param_1,ulong param_2,long param_3,ulong param_4,uint param_5,uint param_6,long param_7,char *param_8,char *param_9);
undefined1 * FUN_00407cfe(int iParm1,undefined8 uParm2,undefined8 uParm3,uint *puParm4);
void FUN_00407f71(uint uParm1,uint uParm2,undefined8 uParm3);
void FUN_00407fb2(uint uParm1,undefined8 uParm2);
void FUN_00407fd6(undefined8 uParm1,undefined8 uParm2,char cParm3);
void FUN_0040806a(undefined8 uParm1,char cParm2);
void FUN_00408094(undefined8 uParm1);
void FUN_004080b3(uint uParm1,uint uParm2,undefined8 uParm3);
void FUN_0040814e(uint uParm1,undefined8 uParm2,undefined8 uParm3);
void FUN_0040817a(uint uParm1,undefined8 uParm2);
void FUN_004081a3(undefined8 uParm1);
undefined8 FUN_004081c2(undefined4 uParm1);
ulong FUN_004081e1(uint uParm1,long lParm2,uint uParm3,long lParm4,uint uParm5);
ulong FUN_00408643(undefined8 uParm1,undefined8 uParm2);
void FUN_004087e8(uint uParm1,undefined8 uParm2,undefined8 uParm3);
ulong FUN_00408814(undefined8 uParm1,ulong uParm2);
ulong FUN_0040883e(long lParm1,int iParm2,undefined8 uParm3,code *pcParm4,ulong uParm5);
void FUN_00408a05(undefined8 uParm1,long lParm2,undefined8 uParm3,undefined8 uParm4,undefined8 *puParm5,undefined8 uParm6);
void FUN_00408e69(undefined8 uParm1,undefined8 uParm2,undefined8 uParm3,undefined8 uParm4,uint *puParm5);
void FUN_00408f3b(undefined4 uParm1,undefined4 uParm2,undefined4 uParm3,undefined4 uParm4,undefined4 uParm5,undefined4 uParm6,undefined4 uParm7,undefined4 uParm8,undefined8 param_1,undefined8 param_2,undefined8 param_3,undefined8 param_4,undefined8 param_5,undefined8 param_6);
void FUN_00408ff1(undefined8 uParm1);
long FUN_0040900b(long lParm1);
long FUN_00409040(long lParm1,long lParm2);
void FUN_004090a1(undefined8 uParm1,undefined8 uParm2);
void FUN_004090d5(undefined8 uParm1);
long FUN_00409102(void);
long FUN_0040912c(void);
ulong FUN_00409165(void);
undefined8 FUN_004091b0(ulong uParm1,ulong uParm2);
ulong FUN_0040922f(uint uParm1);
void FUN_00409258(void);
void FUN_0040928c(uint uParm1);
void FUN_00409300(uint uParm1,uint uParm2,undefined8 uParm3,undefined8 uParm4);
void FUN_00409384(undefined4 uParm1,undefined4 uParm2,undefined4 uParm3,undefined4 uParm4,undefined4 uParm5,undefined4 uParm6,undefined4 uParm7,undefined4 uParm8,uint param_1,uint param_2,undefined8 param_3,undefined8 param_4,undefined8 param_5,undefined8 param_6);
ulong FUN_00409468(undefined4 uParm1,undefined4 uParm2,undefined4 uParm3,undefined4 uParm4,undefined4 uParm5,undefined4 uParm6,undefined4 uParm7,undefined4 uParm8,uint param_1,uint param_2,undefined8 param_3,undefined8 param_4,undefined8 param_5,undefined8 param_6);
undefined8 FUN_0040971f(uint uParm1);
void FUN_00409782(undefined8 uParm1);
void FUN_004097a6(void);
ulong FUN_004097b4(long lParm1);
undefined8 FUN_00409884(undefined8 uParm1);
void FUN_004098a3(undefined8 uParm1,undefined8 uParm2,uint uParm3);
void FUN_004098ce(uint uParm1,undefined8 uParm2,undefined8 uParm3,uint uParm4);
void FUN_004098fb(uint uParm1,undefined8 uParm2,undefined8 uParm3,uint uParm4);
ulong FUN_00409938(uint uParm1,long lParm2,long lParm3,uint uParm4);
long FUN_00409a4b(long lParm1,undefined *puParm2);
void FUN_00409fd4(long lParm1,int *piParm2);
ulong FUN_0040a1b8(int param_1,undefined8 *param_2,char *param_3,long *param_4,int *param_5,int param_6,int *param_7,int param_8,undefined8 param_9);
char * FUN_0040a7a2(undefined8 uParm1,undefined8 uParm2,char *pcParm3,int *piParm4,int iParm5);
ulong FUN_0040a870(uint param_1,undefined8 *param_2,char *param_3,long param_4,undefined8 param_5,uint param_6,uint *param_7,uint param_8);
ulong FUN_0040b039(uint param_1,undefined8 param_2,undefined8 param_3,undefined8 param_4,undefined8 param_5,uint param_6,uint param_7);
void FUN_0040b0c9(uint uParm1,undefined8 uParm2,undefined8 uParm3,undefined8 uParm4,undefined8 uParm5);
ulong FUN_0040b113(uint uParm1,char *pcParm2,uint uParm3,undefined8 uParm4);
ulong FUN_0040b2e6(uint uParm1,long lParm2,uint uParm3,long lParm4,uint uParm5);
void FUN_0040b4c2(undefined8 uParm1,undefined8 uParm2);
ulong FUN_0040b4e7(long lParm1,long lParm2);
undefined8 FUN_0040b59e(long lParm1);
ulong FUN_0040b5cf(uint *puParm1,byte *pbParm2,long lParm3,undefined8 uParm4);
void FUN_0040b652(uint uParm1,undefined8 uParm2,uint uParm3,uint uParm4);
ulong FUN_0040b682(undefined4 uParm1,undefined4 uParm2,undefined4 uParm3,undefined4 uParm4,undefined4 uParm5,undefined4 uParm6,undefined4 uParm7,undefined4 uParm8,uint param_1,undefined8 param_2,uint param_3,undefined8 param_4,undefined8 param_5,undefined8 param_6);
undefined8 FUN_0040b849(long lParm1,long lParm2);
ulong FUN_0040b8b2(long lParm1,long lParm2);
void FUN_0040bdbe(uint uParm1,undefined8 uParm2,uint uParm3,undefined8 uParm4);
ulong FUN_0040bdf1(long lParm1);
void FUN_0040beae(undefined8 uParm1,undefined8 uParm2);
ulong FUN_0040bed3(long lParm1,long lParm2);
undefined8 FUN_0040bf63(undefined8 uParm1,uint uParm2,long lParm3);
ulong FUN_0040c00b(uint uParm1,long lParm2,uint uParm3);
ulong FUN_0040c1cc(undefined8 uParm1,undefined8 uParm2,undefined8 uParm3);
long FUN_0040c2ed(undefined8 uParm1,ulong uParm2);
void FUN_0040c43f(uint uParm1,undefined8 uParm2);
void FUN_0040c474(void);
long FUN_0040c484(undefined8 uParm1,long lParm2,long lParm3,long lParm4);
void FUN_0040c5b3(undefined8 uParm1,undefined8 uParm2,long lParm3);
void FUN_0040c634(long lParm1,long lParm2,long lParm3);
long FUN_0040c76a(undefined8 uParm1,undefined8 uParm2,undefined8 uParm3,undefined8 uParm4,undefined8 uParm5,code *pcParm6);
ulong FUN_0040c7f0(ulong uParm1,undefined4 uParm2);
ulong FUN_0040c80c(byte *pbParm1,byte *pbParm2);
undefined *FUN_0040c883(uint uParm1,undefined8 uParm2,undefined *puParm3,ulong uParm4,code **ppcParm5,code *pcParm6);
undefined8 FUN_0040cb22(uint uParm1,char cParm2);
undefined8 FUN_0040cb9d(undefined8 uParm1);
undefined8 FUN_0040cc28(void);
ulong FUN_0040cc35(uint uParm1);
ulong FUN_0040cd06(char *pcParm1,ulong uParm2);
undefined1 * FUN_0040cd60(void);
char * FUN_0040d113(void);
undefined8 * FUN_0040d1e1(undefined8 uParm1);
undefined8 FUN_0040d228(undefined8 uParm1,undefined8 uParm2);
long FUN_0040d26b(long lParm1);
ulong FUN_0040d27d(undefined8 *puParm1,ulong uParm2);
void FUN_0040d42e(undefined8 uParm1);
ulong FUN_0040d459(undefined8 *puParm1);
ulong * FUN_0040d49f(ulong uParm1,ulong uParm2);
undefined8 * FUN_0040d4ff(undefined8 uParm1,undefined8 uParm2);
void FUN_0040d546(long lParm1,ulong uParm2,ulong uParm3);
long FUN_0040d7ab(long lParm1,ulong uParm2);
void FUN_0040d89a(undefined8 *puParm1,long lParm2,long lParm3);
void FUN_0040d93a(ulong *puParm1,ulong uParm2,ulong uParm3);
void FUN_0040da7f(long *plParm1,undefined8 uParm2,undefined8 uParm3);
undefined8 FUN_0040dad5(long *plParm1);
undefined8 FUN_0040db25(undefined8 uParm1);
undefined8 FUN_0040db3f(long lParm1,undefined8 uParm2);
void FUN_0040db83(ulong *puParm1,long *plParm2);
void FUN_0040e213(long lParm1);
ulong FUN_0040e79a(uint uParm1);
void FUN_0040e7aa(undefined8 uParm1,undefined8 uParm2);
ulong FUN_0040e7d1(undefined8 uParm1);
ulong FUN_0040e889(undefined8 uParm1,undefined8 uParm2);
undefined8 FUN_0040e930(long lParm1,undefined8 uParm2,undefined8 uParm3);
undefined8 FUN_0040e9d6(uint uParm1,long lParm2,undefined8 uParm3,undefined8 uParm4);
ulong FUN_0040ea84(void);
long FUN_0040ead1(ulong uParm1,long lParm2,ulong uParm3,long lParm4,long *plParm5);
int * FUN_0040ed1d(ulong uParm1,uint *puParm2,ulong uParm3,uint *puParm4,ulong *puParm5);
char * FUN_0040f7fd(ulong uParm1,long lParm2,long lParm3);
long FUN_0040fa6b(int *param_1,long *param_2);
long FUN_0040fc56(undefined auParm1 [16],int *piParm2,long *plParm3);
undefined8 FUN_00410015(int iParm1,long lParm2,uint *puParm3,long lParm4,uint uParm5);
void FUN_00410828(uint param_1);
void FUN_00410872(undefined8 uParm1,uint uParm2);
ulong FUN_004108c4(void);
ulong FUN_00410c56(undefined auParm1 [16],undefined8 uParm2,undefined8 uParm3);
ulong FUN_0041102e(char *pcParm1,long lParm2);
undefined8 FUN_00411087(long param_1,long param_2,undefined param_3,int param_4,undefined8 param_5,ulong param_6,int param_7,ulong param_8,int param_9);
ulong FUN_004187b4(uint uParm1);
long FUN_004187d3(undefined8 uParm1,undefined8 uParm2);
void FUN_004188bf(undefined8 uParm1,int *piParm2,undefined8 uParm3);
void FUN_00418960(int *param_1);
void FUN_00418a1f(uint uParm1);
ulong FUN_00418a45(ulong uParm1,long lParm2);
void FUN_00418a79(undefined8 uParm1,undefined8 uParm2,undefined8 uParm3);
void FUN_00418ab4(undefined8 uParm1,undefined8 uParm2,undefined8 uParm3,undefined8 uParm4);
ulong FUN_00418b05(ulong uParm1,ulong uParm2);
undefined8 FUN_00418b20(uint *puParm1,ulong *puParm2);
undefined8 FUN_004192c0(char *pcParm1,ulong *puParm2,char **ppcParm3);
ulong FUN_0041a576(undefined auParm1 [16]);
ulong FUN_0041a5b1(void);
void FUN_0041a5f0(void);
undefined8 _DT_FINI(void);
undefined FUN_00620000();
undefined FUN_00620008();
undefined FUN_00620010();
undefined FUN_00620018();
undefined FUN_00620020();
undefined FUN_00620028();
undefined FUN_00620030();
undefined FUN_00620038();
undefined FUN_00620040();
undefined FUN_00620048();
undefined FUN_00620050();
undefined FUN_00620058();
undefined FUN_00620060();
undefined FUN_00620068();
undefined FUN_00620070();
undefined FUN_00620078();
undefined FUN_00620080();
undefined FUN_00620088();
undefined FUN_00620090();
undefined FUN_00620098();
undefined FUN_006200a0();
undefined FUN_006200a8();
undefined FUN_006200b0();
undefined FUN_006200b8();
undefined FUN_006200c0();
undefined FUN_006200c8();
undefined FUN_006200d0();
undefined FUN_006200d8();
undefined FUN_006200e0();
undefined FUN_006200e8();
undefined FUN_006200f0();
undefined FUN_006200f8();
undefined FUN_00620100();
undefined FUN_00620108();
undefined FUN_00620110();
undefined FUN_00620118();
undefined FUN_00620120();
undefined FUN_00620128();
undefined FUN_00620130();
undefined FUN_00620138();
undefined FUN_00620140();
undefined FUN_00620148();
undefined FUN_00620150();
undefined FUN_00620158();
undefined FUN_00620160();
undefined FUN_00620168();
undefined FUN_00620170();
undefined FUN_00620178();
undefined FUN_00620180();
undefined FUN_00620188();
undefined FUN_00620190();
undefined FUN_00620198();
undefined FUN_006201a0();
undefined FUN_006201a8();
undefined FUN_006201b0();
undefined FUN_006201b8();
undefined FUN_006201c0();
undefined FUN_006201c8();
undefined FUN_006201d0();
undefined FUN_006201d8();
undefined FUN_006201e0();
undefined FUN_006201e8();
undefined FUN_006201f0();
undefined FUN_006201f8();
undefined FUN_00620200();
undefined FUN_00620208();
undefined FUN_00620210();
undefined FUN_00620218();
undefined FUN_00620220();
undefined FUN_00620228();
undefined FUN_00620230();
undefined FUN_00620238();
undefined FUN_00620240();
undefined FUN_00620248();
undefined FUN_00620250();
undefined FUN_00620258();
undefined FUN_00620260();
undefined FUN_00620268();
undefined FUN_00620270();
undefined FUN_00620278();
undefined FUN_00620280();
undefined FUN_00620288();
undefined FUN_00620290();
undefined FUN_00620298();
undefined FUN_006202a0();
undefined FUN_006202a8();
undefined FUN_006202b0();
undefined FUN_006202b8();
undefined FUN_006202c0();
undefined FUN_006202c8();
undefined FUN_006202d0();
undefined FUN_006202d8();
undefined FUN_006202e0();
undefined FUN_006202e8();
undefined FUN_006202f0();
undefined FUN_006202f8();
undefined FUN_00620300();
undefined FUN_00620308();


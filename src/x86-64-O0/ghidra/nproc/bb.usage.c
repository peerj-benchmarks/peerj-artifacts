
#include "nproc.h"

long null_ARRAY_0061541_0_8_;
long null_ARRAY_0061541_8_8_;
long null_ARRAY_0061558_0_8_;
long null_ARRAY_0061558_16_8_;
long null_ARRAY_0061558_24_8_;
long null_ARRAY_0061558_32_8_;
long null_ARRAY_0061558_40_8_;
long null_ARRAY_0061558_48_8_;
long null_ARRAY_0061558_8_8_;
long null_ARRAY_006156c_0_4_;
long null_ARRAY_006156c_16_8_;
long null_ARRAY_006156c_4_4_;
long null_ARRAY_006156c_8_4_;
long DAT_00411d40;
long DAT_00411dfd;
long DAT_00411e7b;
long DAT_00412176;
long DAT_00412193;
long DAT_004121f8;
long DAT_0041231e;
long DAT_00412322;
long DAT_00412327;
long DAT_00412329;
long DAT_00412993;
long DAT_00412d60;
long DAT_004130f8;
long DAT_004130fd;
long DAT_00413167;
long DAT_004131f8;
long DAT_004131fb;
long DAT_00413249;
long DAT_0041324d;
long DAT_004132c0;
long DAT_004132c1;
long DAT_00615000;
long DAT_00615010;
long DAT_00615020;
long DAT_006153e8;
long DAT_00615400;
long DAT_00615478;
long DAT_0061547c;
long DAT_00615480;
long DAT_006154c0;
long DAT_006154d0;
long DAT_006154e0;
long DAT_006154e8;
long DAT_00615500;
long DAT_00615508;
long DAT_00615550;
long DAT_00615558;
long DAT_00615560;
long DAT_006156f8;
long DAT_00615700;
long DAT_00615708;
long DAT_00615718;
long fde_00413f80;
long FLOAT_UNKNOWN;
long null_ARRAY_00411f00;
long null_ARRAY_00413700;
long null_ARRAY_00615410;
long null_ARRAY_00615440;
long null_ARRAY_00615520;
long null_ARRAY_00615580;
long null_ARRAY_006155c0;
long null_ARRAY_006156c0;
long PTR_DAT_006153e0;
long PTR_null_ARRAY_00615420;
long stack0x00000008;
undefined8
FUN_00401d1a (uint uParm1)
{
  int iVar1;
  undefined8 uVar2;
  ulong uVar3;
  char *pcVar4;
  uint uStack52;
  ulong uStack48;
  long lStack40;

  if (uParm1 == 0)
    {
      func_0x004015b0 ("Usage: %s [OPTION]...\n", DAT_00615560);
      func_0x00401750
	("Print the number of processing units available to the current process,\nwhich may be less than the number of online processors\n\n",
	 DAT_006154c0);
      func_0x00401750
	("      --all      print the number of installed processors\n      --ignore=N  if possible, exclude N processing units\n",
	 DAT_006154c0);
      func_0x00401750 ("      --help     display this help and exit\n",
		       DAT_006154c0);
      pcVar4 = (char *) DAT_006154c0;
      func_0x00401750
	("      --version  output version information and exit\n");
      imperfection_wrapper ();	//    FUN_00401b7e();
    }
  else
    {
      pcVar4 = "Try \'%s --help\' for more information.\n";
      func_0x00401740 (DAT_006154e0,
		       "Try \'%s --help\' for more information.\n",
		       DAT_00615560);
    }
  func_0x004018f0 ();
  uStack48 = 0;
  FUN_004022af (*(undefined8 *) pcVar4);
  func_0x004018a0 (6, &DAT_00411e7b);
  func_0x00401880 (FUN_00401f9d);
  uStack52 = 2;
  do
    {
      while (true)
	{
	  imperfection_wrapper ();	//      iVar1 = FUN_00405744((ulong)uParm1, pcVar4, &DAT_00411e7b, null_ARRAY_00411f00, 0);
	  if (iVar1 == -1)
	    {
	      if (uParm1 != DAT_00615478)
		{
		  imperfection_wrapper ();	//          uVar2 = FUN_00403723(((undefined8 *)pcVar4)[(long)(int)DAT_00615478]);
		  imperfection_wrapper ();	//          FUN_0040456b(0, 0, "extra operand %s", uVar2);
		  FUN_00401d1a (1);
		}
	      uVar3 = FUN_00402221 ((ulong) uStack52);
	      if (uStack48 < uVar3)
		{
		  lStack40 = uVar3 - uStack48;
		}
	      else
		{
		  lStack40 = 1;
		}
	      func_0x004015b0 (&DAT_00412176, lStack40);
	      return 0;
	    }
	  if (iVar1 != -0x82)
	    break;
	  FUN_00401d1a (0);
	LAB_00401e86:
	  ;
	  imperfection_wrapper ();	//      FUN_00403c78(DAT_006154c0, "nproc", "GNU coreutils", PTR_DAT_006153e0, "Giuseppe Scrivano", 0);
	  func_0x004018f0 ();
	LAB_00401ec4:
	  ;
	  uStack52 = 0;
	}
      if (iVar1 < -0x81)
	{
	  if (iVar1 == -0x83)
	    goto LAB_00401e86;
	LAB_00401eff:
	  ;
	  imperfection_wrapper ();	//      FUN_00401d1a();
	}
      else
	{
	  if (iVar1 == 0x80)
	    goto LAB_00401ec4;
	  if (iVar1 != 0x81)
	    goto LAB_00401eff;
	  imperfection_wrapper ();	//      uStack48 = FUN_00403f1a(DAT_00615718, 0, 0xffffffffffffffff, &DAT_00411e7b, "invalid number", 0);
	}
    }
  while (true);
}

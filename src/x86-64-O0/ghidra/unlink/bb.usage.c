
#include "unlink.h"

long null_ARRAY_0061445_0_8_;
long null_ARRAY_0061445_8_8_;
long null_ARRAY_006145c_0_8_;
long null_ARRAY_006145c_16_8_;
long null_ARRAY_006145c_24_8_;
long null_ARRAY_006145c_32_8_;
long null_ARRAY_006145c_40_8_;
long null_ARRAY_006145c_48_8_;
long null_ARRAY_006145c_8_8_;
long null_ARRAY_0061470_0_4_;
long null_ARRAY_0061470_16_8_;
long null_ARRAY_0061470_4_4_;
long null_ARRAY_0061470_8_4_;
long DAT_004117c0;
long DAT_0041187d;
long DAT_004118fb;
long DAT_00411ab5;
long DAT_00411b60;
long DAT_00411ba8;
long DAT_00411cde;
long DAT_00411ce2;
long DAT_00411ce7;
long DAT_00411ce9;
long DAT_00412353;
long DAT_00412720;
long DAT_00412738;
long DAT_0041273d;
long DAT_004127a7;
long DAT_00412838;
long DAT_0041283b;
long DAT_00412889;
long DAT_0041288d;
long DAT_00412900;
long DAT_00412901;
long DAT_00412ae8;
long DAT_00614050;
long DAT_00614060;
long DAT_00614070;
long DAT_00614428;
long DAT_00614440;
long DAT_006144b8;
long DAT_006144bc;
long DAT_006144c0;
long DAT_00614500;
long DAT_00614510;
long DAT_00614520;
long DAT_00614528;
long DAT_00614540;
long DAT_00614548;
long DAT_00614590;
long DAT_00614598;
long DAT_006145a0;
long DAT_00614738;
long DAT_00614740;
long DAT_00614748;
long DAT_00614758;
long fde_004135a8;
long FLOAT_UNKNOWN;
long null_ARRAY_00411940;
long null_ARRAY_00411b00;
long null_ARRAY_00412d40;
long null_ARRAY_00614450;
long null_ARRAY_00614480;
long null_ARRAY_00614560;
long null_ARRAY_006145c0;
long null_ARRAY_00614600;
long null_ARRAY_00614700;
long PTR_DAT_00614420;
long PTR_null_ARRAY_00614460;
undefined8
FUN_00401c0a (uint uParm1)
{
  int iVar1;
  undefined8 uVar2;
  uint *puVar3;
  char *pcVar4;

  if (uParm1 == 0)
    {
      func_0x004014c0 ("Usage: %s FILE\n  or:  %s OPTION\n", DAT_006145a0,
		       DAT_006145a0);
      func_0x00401640
	("Call the unlink function to remove the specified FILE.\n\n",
	 DAT_00614500);
      func_0x00401640 ("      --help     display this help and exit\n",
		       DAT_00614500);
      pcVar4 = (char *) DAT_00614500;
      func_0x00401640
	("      --version  output version information and exit\n");
      imperfection_wrapper ();	//    FUN_00401a6e();
    }
  else
    {
      pcVar4 = "Try \'%s --help\' for more information.\n";
      func_0x00401630 (DAT_00614520,
		       "Try \'%s --help\' for more information.\n",
		       DAT_006145a0);
    }
  func_0x004017e0 ();
  FUN_00402086 (*(undefined8 *) pcVar4);
  func_0x00401790 (6, &DAT_004118fb);
  func_0x00401770 (FUN_00401e50);
  imperfection_wrapper ();	//  FUN_00401f35((ulong)uParm1, pcVar4, "unlink", "GNU coreutils", PTR_DAT_00614420, FUN_00401c0a, "Michael Stone", 0);
  imperfection_wrapper ();	//  iVar1 = FUN_00404f33((ulong)uParm1, pcVar4, &DAT_004118fb, null_ARRAY_00411940, 0);
  if (iVar1 != -1)
    {
      FUN_00401c0a (1);
    }
  if ((int) uParm1 < DAT_006144b8 + 1)
    {
      imperfection_wrapper ();	//    FUN_00403d5a(0, 0, "missing operand");
      FUN_00401c0a (1);
    }
  if (DAT_006144b8 + 1 < (int) uParm1)
    {
      imperfection_wrapper ();	//    uVar2 = FUN_004035d6(((undefined8 *)pcVar4)[(long)DAT_006144b8 + 1]);
      imperfection_wrapper ();	//    FUN_00403d5a(0, 0, "extra operand %s", uVar2);
      FUN_00401c0a (1);
    }
  iVar1 = FUN_0040509a (((undefined8 *) pcVar4)[(long) DAT_006144b8]);
  if (iVar1 != 0)
    {
      imperfection_wrapper ();	//    uVar2 = FUN_00403480(4, ((undefined8 *)pcVar4)[(long)DAT_006144b8]);
      puVar3 = (uint *) func_0x004017d0 ();
      imperfection_wrapper ();	//    FUN_00403d5a(1, (ulong)*puVar3, "cannot unlink %s", uVar2);
    }
  return 0;
}


#include "cp.h"

long null_ARRAY_0062c61_0_8_;
long null_ARRAY_0062c61_8_8_;
long null_ARRAY_0062c7c_0_8_;
long null_ARRAY_0062c7c_16_8_;
long null_ARRAY_0062c7c_24_8_;
long null_ARRAY_0062c7c_32_8_;
long null_ARRAY_0062c7c_40_8_;
long null_ARRAY_0062c7c_48_8_;
long null_ARRAY_0062c7c_56_8_;
long null_ARRAY_0062c7c_64_8_;
long null_ARRAY_0062c7c_8_8_;
long null_ARRAY_0062ccc_0_8_;
long null_ARRAY_0062ccc_16_8_;
long null_ARRAY_0062ccc_24_8_;
long null_ARRAY_0062ccc_32_8_;
long null_ARRAY_0062ccc_40_8_;
long null_ARRAY_0062ccc_48_8_;
long null_ARRAY_0062ccc_8_8_;
long null_ARRAY_0062ce2_0_4_;
long null_ARRAY_0062ce2_16_8_;
long null_ARRAY_0062ce2_4_4_;
long null_ARRAY_0062ce2_8_4_;
long DAT_0042430e;
long DAT_004243c5;
long DAT_00424443;
long DAT_004244e8;
long DAT_00425a4a;
long DAT_00425c16;
long DAT_00426fe8;
long DAT_00427234;
long DAT_004272ef;
long DAT_00427380;
long DAT_0042739c;
long DAT_0042739e;
long DAT_004274a1;
long DAT_004274bd;
long DAT_004274d3;
long DAT_004275d0;
long DAT_0042771e;
long DAT_00427722;
long DAT_00427727;
long DAT_00427729;
long DAT_00427d80;
long DAT_00427da0;
long DAT_00427e6b;
long DAT_00428220;
long DAT_00428238;
long DAT_0042823d;
long DAT_00428250;
long DAT_00428252;
long DAT_00428254;
long DAT_004282af;
long DAT_00428340;
long DAT_00428343;
long DAT_00428391;
long DAT_00428395;
long DAT_00428408;
long DAT_00428409;
long DAT_00428447;
long DAT_0062c000;
long DAT_0062c010;
long DAT_0062c020;
long DAT_0062c5c0;
long DAT_0062c5c8;
long DAT_0062c5e0;
long DAT_0062c600;
long DAT_0062c678;
long DAT_0062c67c;
long DAT_0062c680;
long DAT_0062c6c0;
long DAT_0062c6c8;
long DAT_0062c6d0;
long DAT_0062c6e0;
long DAT_0062c6e8;
long DAT_0062c700;
long DAT_0062c708;
long DAT_0062c780;
long DAT_0062c781;
long DAT_0062c782;
long DAT_0062c840;
long DAT_0062c848;
long DAT_0062c850;
long DAT_0062cc80;
long DAT_0062cc88;
long DAT_0062cc90;
long DAT_0062cc98;
long DAT_0062cca0;
long DAT_0062cca8;
long DAT_0062ce00;
long DAT_0062ce08;
long DAT_0062ce10;
long DAT_0062ce14;
long DAT_0062ce18;
long DAT_0062ce19;
long DAT_0062ce1c;
long DAT_0062ce58;
long DAT_0062ce5c;
long DAT_0062ce60;
long DAT_0062ceb8;
long DAT_0062cec0;
long DAT_0062ced0;
long fde_00429800;
long FLOAT_UNKNOWN;
long null_ARRAY_004244a0;
long null_ARRAY_004244c0;
long null_ARRAY_004244d0;
long null_ARRAY_00424640;
long null_ARRAY_004267c0;
long null_ARRAY_00426800;
long null_ARRAY_004272e1;
long null_ARRAY_00427400;
long null_ARRAY_00427460;
long null_ARRAY_004274e0;
long null_ARRAY_00427d90;
long null_ARRAY_00428880;
long null_ARRAY_00428a80;
long null_ARRAY_0062c610;
long null_ARRAY_0062c640;
long null_ARRAY_0062c720;
long null_ARRAY_0062c7c0;
long null_ARRAY_0062c880;
long null_ARRAY_0062ccc0;
long null_ARRAY_0062cd00;
long null_ARRAY_0062ce20;
long PTR_DAT_0062c5d0;
long PTR_FUN_0062c5d8;
long PTR_null_ARRAY_0062c620;
long PTR_null_ARRAY_0062c688;
long stack0x00000008;
undefined8
FUN_00402fb0 (uint uParm1, undefined8 uParm2, undefined8 uParm3, long lParm4)
{
  uint uVar1;
  uint uVar2;
  char cVar3;
  int iVar4;
  long lVar5;
  ulong uVar6;
  undefined8 uVar7;
  uint *puVar8;
  long extraout_RDX;
  undefined auVar9[16];
  long alStack152[3];
  char *pcStack128;
  ulong uStack120;
  undefined8 uStack112;
  undefined8 uStack104;
  undefined8 uStack96;
  undefined8 uStack88;
  undefined4 uStack76;
  char *pcStack72;
  long lStack64;
  long lStack56;
  code *pcStack32;
  uint local_c;

  if (uParm1 == 0)
    {
      pcStack32 = (code *) 0x40300d;
      lParm4 = DAT_0062cca8;
      local_c = uParm1;
      func_0x00402490
	("Usage: %s [OPTION]... [-T] SOURCE DEST\n  or:  %s [OPTION]... SOURCE... DIRECTORY\n  or:  %s [OPTION]... -t DIRECTORY SOURCE...\n",
	 DAT_0062cca8, DAT_0062cca8);
      pcStack32 = (code *) 0x403021;
      func_0x004027f0
	("Copy SOURCE to DEST, or multiple SOURCE(s) to DIRECTORY.\n",
	 DAT_0062c6c0);
      pcStack32 = (code *) 0x403026;
      FUN_00402dbe ();
      pcStack32 = (code *) 0x40303a;
      func_0x004027f0
	("  -a, --archive                same as -dR --preserve=all\n      --attributes-only        don\'t copy the file data, just the attributes\n      --backup[=CONTROL]       make a backup of each existing destination file\n  -b                           like --backup but does not accept an argument\n      --copy-contents          copy contents of special files when recursive\n  -d                           same as --no-dereference --preserve=links\n",
	 DAT_0062c6c0);
      pcStack32 = (code *) 0x40304e;
      func_0x004027f0
	("  -f, --force                  if an existing destination file cannot be\n                                 opened, remove it and try again (this option\n                                 is ignored when the -n option is also used)\n  -i, --interactive            prompt before overwrite (overrides a previous -n\n                                  option)\n  -H                           follow command-line symbolic links in SOURCE\n",
	 DAT_0062c6c0);
      pcStack32 = (code *) 0x403062;
      func_0x004027f0
	("  -l, --link                   hard link files instead of copying\n  -L, --dereference            always follow symbolic links in SOURCE\n",
	 DAT_0062c6c0);
      pcStack32 = (code *) 0x403076;
      func_0x004027f0
	("  -n, --no-clobber             do not overwrite an existing file (overrides\n                                 a previous -i option)\n  -P, --no-dereference         never follow symbolic links in SOURCE\n",
	 DAT_0062c6c0);
      pcStack32 = (code *) 0x40308a;
      func_0x004027f0
	("  -p                           same as --preserve=mode,ownership,timestamps\n      --preserve[=ATTR_LIST]   preserve the specified attributes (default:\n                                 mode,ownership,timestamps), if possible\n                                 additional attributes: context, links, xattr,\n                                 all\n",
	 DAT_0062c6c0);
      pcStack32 = (code *) 0x40309e;
      func_0x004027f0
	("      --no-preserve=ATTR_LIST  don\'t preserve the specified attributes\n      --parents                use full source file name under DIRECTORY\n",
	 DAT_0062c6c0);
      pcStack32 = (code *) 0x4030b2;
      func_0x004027f0
	("  -R, -r, --recursive          copy directories recursively\n      --reflink[=WHEN]         control clone/CoW copies. See below\n      --remove-destination     remove each existing destination file before\n                                 attempting to open it (contrast with --force)\n",
	 DAT_0062c6c0);
      pcStack32 = (code *) 0x4030c6;
      func_0x004027f0
	("      --sparse=WHEN            control creation of sparse files. See below\n      --strip-trailing-slashes  remove any trailing slashes from each SOURCE\n                                 argument\n",
	 DAT_0062c6c0);
      pcStack32 = (code *) 0x4030da;
      func_0x004027f0
	("  -s, --symbolic-link          make symbolic links instead of copying\n  -S, --suffix=SUFFIX          override the usual backup suffix\n  -t, --target-directory=DIRECTORY  copy all SOURCE arguments into DIRECTORY\n  -T, --no-target-directory    treat DEST as a normal file\n",
	 DAT_0062c6c0);
      pcStack32 = (code *) 0x4030ee;
      func_0x004027f0
	("  -u, --update                 copy only when the SOURCE file is newer\n                                 than the destination file or when the\n                                 destination file is missing\n  -v, --verbose                explain what is being done\n  -x, --one-file-system        stay on this file system\n",
	 DAT_0062c6c0);
      pcStack32 = (code *) 0x403102;
      func_0x004027f0
	("  -Z                           set SELinux security context of destination\n                                 file to default type\n      --context[=CTX]          like -Z, or if CTX is specified then set the\n                                 SELinux or SMACK security context to CTX\n",
	 DAT_0062c6c0);
      pcStack32 = (code *) 0x403116;
      func_0x004027f0 ("      --help     display this help and exit\n",
		       DAT_0062c6c0);
      pcStack32 = (code *) 0x40312a;
      func_0x004027f0
	("      --version  output version information and exit\n",
	 DAT_0062c6c0);
      pcStack32 = (code *) 0x40313e;
      func_0x004027f0
	("\nBy default, sparse SOURCE files are detected by a crude heuristic and the\ncorresponding DEST file is made sparse as well.  That is the behavior\nselected by --sparse=auto.  Specify --sparse=always to create a sparse DEST\nfile whenever the SOURCE file contains a long enough sequence of zero bytes.\nUse --sparse=never to inhibit creation of sparse files.\n\nWhen --reflink[=always] is specified, perform a lightweight copy, where the\ndata blocks are copied only when modified.  If this is not possible the copy\nfails, or if --reflink=auto is specified, fall back to a standard copy.\n",
	 DAT_0062c6c0);
      pcStack32 = (code *) 0x403143;
      FUN_00402dd8 ();
      pcStack32 = (code *) 0x403157;
      pcStack128 = DAT_0062c6c0;
      func_0x004027f0
	("\nAs a special case, cp makes a backup of SOURCE when the force and backup\noptions are given and SOURCE and DEST are the same name for an existing,\nregular file.\n");
      pcStack32 = (code *) 0x403161;
      imperfection_wrapper ();	//    FUN_00402e06();
    }
  else
    {
      pcStack128 = "Try \'%s --help\' for more information.\n";
      pcStack32 = (code *) 0x402fe1;
      local_c = uParm1;
      func_0x004027e0 (DAT_0062c6e0,
		       "Try \'%s --help\' for more information.\n",
		       DAT_0062cca8);
    }
  uStack120 = (ulong) local_c;
  pcStack32 = FUN_0040316b;
  func_0x00402ae0 ();
  alStack152[0] = 0x403190;
  alStack152[1] = lParm4;
  alStack152[2] = extraout_RDX;
  pcStack32 = (code *) & stack0xfffffffffffffff8;
  lVar5 = func_0x00402b70 (uStack120);
  uVar6 = (lVar5 + 0x1fU) / 0x10;
  alStack152[uVar6 * 0x1ffffffffffffffe] = 0x4031d9;
  lStack64 =
    func_0x00402450 (alStack152 + uVar6 * 0x1ffffffffffffffe + 1, uStack120,
		     alStack152 + uVar6 * 0x1ffffffffffffffe + 1);
  pcStack72 = pcStack128 + lStack64;
  lStack56 = alStack152[2];
  do
    {
      if (lStack56 == 0)
	{
	  return 1;
	}
      *(undefined *) (lStack64 + *(long *) (lStack56 + 0x98)) = 0;
      if (*(char *) (alStack152[1] + 0x1f) != '\0')
	{
	  alStack152[uVar6 * 0x1ffffffffffffffe] = 0x403226;
	  imperfection_wrapper ();	//      auVar9 = FUN_00410bc6(lStack56);
	  imperfection_wrapper ();	//      uStack104 = SUB168(auVar9 >> 0x40, 0);
	  uStack112 = SUB168 (auVar9, 0);
	  alStack152[uVar6 * 0x1ffffffffffffffe] = 0x40323a;
	  imperfection_wrapper ();	//      auVar9 = FUN_00410bdc(lStack56);
	  imperfection_wrapper ();	//      uStack88 = SUB168(auVar9 >> 0x40, 0);
	  uStack96 = SUB168 (auVar9, 0);
	  alStack152[uVar6 * 0x1ffffffffffffffe] = 0x403255;
	  imperfection_wrapper ();	//      iVar4 = FUN_00411d1f(lStack64, &uStack112, &uStack112);
	  if (iVar4 != 0)
	    {
	      alStack152[uVar6 * 0x1ffffffffffffffe] = 0x40326a;
	      imperfection_wrapper ();	//        uVar7 = FUN_0040fe9a(4, lStack64);
	      alStack152[uVar6 * 0x1ffffffffffffffe] = 0x403272;
	      puVar8 = (uint *) func_0x00402ad0 ();
	      uVar1 = *puVar8;
	      alStack152[uVar6 * 0x1ffffffffffffffe] = 0x40328d;
	      imperfection_wrapper ();	//        FUN_00412a53(0, (ulong)uVar1, "failed to preserve times for %s", uVar7);
	      return 0;
	    }
	}
      if (*(char *) (alStack152[1] + 0x1d) != '\0')
	{
	  uVar1 = *(uint *) (lStack56 + 0x20);
	  uVar2 = *(uint *) (lStack56 + 0x1c);
	  alStack152[uVar6 * 0x1ffffffffffffffe] = 0x4032c3;
	  imperfection_wrapper ();	//      iVar4 = FUN_004147e2(lStack64, (ulong)uVar2, (ulong)uVar1, (ulong)uVar2);
	  if (iVar4 != 0)
	    {
	      alStack152[uVar6 * 0x1ffffffffffffffe] = 0x4032d3;
	      cVar3 = FUN_0040a356 (alStack152[1]);
	      if (cVar3 != '\x01')
		{
		  alStack152[uVar6 * 0x1ffffffffffffffe] = 0x4032eb;
		  imperfection_wrapper ();	//          uVar7 = FUN_0040fe9a(4, lStack64);
		  alStack152[uVar6 * 0x1ffffffffffffffe] = 0x4032f3;
		  puVar8 = (uint *) func_0x00402ad0 ();
		  uVar1 = *puVar8;
		  alStack152[uVar6 * 0x1ffffffffffffffe] = 0x40330e;
		  imperfection_wrapper ();	//          FUN_00412a53(0, (ulong)uVar1, "failed to preserve ownership for %s", uVar7);
		  return 0;
		}
	      uVar1 = *(uint *) (lStack56 + 0x20);
	      alStack152[uVar6 * 0x1ffffffffffffffe] = 0x403330;
	      uStack76 = FUN_004147e2 (lStack64, 0xffffffff, (ulong) uVar1);
	    }
	}
      if (*(char *) (alStack152[1] + 0x1e) == '\0')
	{
	  if (*(char *) (lStack56 + 0x90) != '\0')
	    {
	      alStack152[uVar6 * 0x1ffffffffffffffe] = 0x403395;
	      iVar4 = func_0x004028e0 (lStack64);
	      if (iVar4 != 0)
		{
		  alStack152[uVar6 * 0x1ffffffffffffffe] = 0x4033aa;
		  imperfection_wrapper ();	//          uVar7 = FUN_0040fe9a(4, lStack64);
		  alStack152[uVar6 * 0x1ffffffffffffffe] = 0x4033b2;
		  puVar8 = (uint *) func_0x00402ad0 ();
		  uVar1 = *puVar8;
		  alStack152[uVar6 * 0x1ffffffffffffffe] = 0x4033cd;
		  imperfection_wrapper ();	//          FUN_00412a53(0, (ulong)uVar1, "failed to preserve permissions for %s", uVar7);
		  return 0;
		}
	    }
	}
      else
	{
	  uVar1 = *(uint *) (lStack56 + 0x18);
	  alStack152[uVar6 * 0x1ffffffffffffffe] = 0x403363;
	  iVar4 =
	    FUN_0040b0e3 (pcStack72, 0xffffffff, lStack64, 0xffffffff,
			  (ulong) uVar1);
	  if (iVar4 != 0)
	    {
	      return 0;
	    }
	}
      *(undefined *) (lStack64 + *(long *) (lStack56 + 0x98)) = 0x2f;
      lStack56 = *(long *) (lStack56 + 0xa0);
    }
  while (true);
}

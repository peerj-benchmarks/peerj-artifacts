typedef unsigned char   undefined;

typedef unsigned char    byte;
typedef unsigned char    dwfenc;
typedef unsigned int    dword;
typedef unsigned long    qword;
typedef unsigned char    undefined1;
typedef unsigned short    undefined2;
typedef unsigned int    undefined4;
typedef unsigned long    undefined5;
typedef unsigned long    undefined6;
typedef unsigned long    undefined8;
typedef unsigned short    word;
#define unkbyte9   unsigned long long
#define unkbyte10   unsigned long long
#define unkbyte11   unsigned long long
#define unkbyte12   unsigned long long
#define unkbyte13   unsigned long long
#define unkbyte14   unsigned long long
#define unkbyte15   unsigned long long
#define unkbyte16   unsigned long long

#define unkuint9   unsigned long long
#define unkuint10   unsigned long long
#define unkuint11   unsigned long long
#define unkuint12   unsigned long long
#define unkuint13   unsigned long long
#define unkuint14   unsigned long long
#define unkuint15   unsigned long long
#define unkuint16   unsigned long long

#define unkint9   long long
#define unkint10   long long
#define unkint11   long long
#define unkint12   long long
#define unkint13   long long
#define unkint14   long long
#define unkint15   long long
#define unkint16   long long

#define unkfloat1   float
#define unkfloat2   float
#define unkfloat3   float
#define unkfloat5   double
#define unkfloat6   double
#define unkfloat7   double
#define unkfloat9   long double
#define unkfloat11   long double
#define unkfloat12   long double
#define unkfloat13   long double
#define unkfloat14   long double
#define unkfloat15   long double
#define unkfloat16   long double

#define BADSPACEBASE   void
#define code   void

#define ulong long
#define true 1
#define bool int
#define false 0
#define float10 int
#define ushort int
#define undefined7 long
#define undefined2 long
#define uint5 long
#define uint6 long

// Add definition for uint
typedef unsigned int    uint;

#define sbyte char

extern imperfection_wrapper(void);
typedef struct eh_frame_hdr eh_frame_hdr, *Peh_frame_hdr;

struct eh_frame_hdr {
    byte eh_frame_hdr_version; // Exception Handler Frame Header Version
    dwfenc eh_frame_pointer_encoding; // Exception Handler Frame Pointer Encoding
    dwfenc eh_frame_desc_entry_count_encoding; // Encoding of # of Exception Handler FDEs
    dwfenc eh_frame_table_encoding; // Exception Handler Table Encoding
};

typedef struct fde_table_entry fde_table_entry, *Pfde_table_entry;

struct fde_table_entry {
    dword initial_loc; // Initial Location
    dword data_loc; // Data location
};

typedef enum Elf64_DynTag {
    DT_AUDIT=1879047932,
    DT_AUXILIARY=2147483645,
    DT_BIND_NOW=24,
    DT_CHECKSUM=1879047672,
    DT_CONFIG=1879047930,
    DT_DEBUG=21,
    DT_DEPAUDIT=1879047931,
    DT_ENCODING=32,
    DT_FEATURE_1=1879047676,
    DT_FILTER=2147483647,
    DT_FINI=13,
    DT_FINI_ARRAY=26,
    DT_FINI_ARRAYSZ=28,
    DT_FLAGS=30,
    DT_FLAGS_1=1879048187,
    DT_GNU_CONFLICT=1879047928,
    DT_GNU_CONFLICTSZ=1879047670,
    DT_GNU_HASH=1879047925,
    DT_GNU_LIBLIST=1879047929,
    DT_GNU_LIBLISTSZ=1879047671,
    DT_GNU_PRELINKED=1879047669,
    DT_HASH=4,
    DT_INIT=12,
    DT_INIT_ARRAY=25,
    DT_INIT_ARRAYSZ=27,
    DT_JMPREL=23,
    DT_MOVEENT=1879047674,
    DT_MOVESZ=1879047675,
    DT_MOVETAB=1879047934,
    DT_NEEDED=1,
    DT_NULL=0,
    DT_PLTGOT=3,
    DT_PLTPAD=1879047933,
    DT_PLTPADSZ=1879047673,
    DT_PLTREL=20,
    DT_PLTRELSZ=2,
    DT_POSFLAG_1=1879047677,
    DT_PREINIT_ARRAYSZ=33,
    DT_REL=17,
    DT_RELA=7,
    DT_RELACOUNT=1879048185,
    DT_RELAENT=9,
    DT_RELASZ=8,
    DT_RELCOUNT=1879048186,
    DT_RELENT=19,
    DT_RELSZ=18,
    DT_RPATH=15,
    DT_RUNPATH=29,
    DT_SONAME=14,
    DT_STRSZ=10,
    DT_STRTAB=5,
    DT_SYMBOLIC=16,
    DT_SYMENT=11,
    DT_SYMINENT=1879047679,
    DT_SYMINFO=1879047935,
    DT_SYMINSZ=1879047678,
    DT_SYMTAB=6,
    DT_TEXTREL=22,
    DT_TLSDESC_GOT=1879047927,
    DT_TLSDESC_PLT=1879047926,
    DT_VERDEF=1879048188,
    DT_VERDEFNUM=1879048189,
    DT_VERNEED=1879048190,
    DT_VERNEEDNUM=1879048191,
    DT_VERSYM=1879048176
} Elf64_DynTag;

typedef enum Elf_ProgramHeaderType {
    PT_DYNAMIC=2,
    PT_GNU_EH_FRAME=1685382480,
    PT_GNU_RELRO=1685382482,
    PT_GNU_STACK=1685382481,
    PT_INTERP=3,
    PT_LOAD=1,
    PT_NOTE=4,
    PT_NULL=0,
    PT_PHDR=6,
    PT_SHLIB=5,
    PT_TLS=7
} Elf_ProgramHeaderType;

typedef struct Elf64_Dyn Elf64_Dyn, *PElf64_Dyn;

struct Elf64_Dyn {
    enum Elf64_DynTag d_tag;
    qword d_val;
};

typedef struct Elf64_Shdr Elf64_Shdr, *PElf64_Shdr;

typedef enum Elf_SectionHeaderType {
    SHT_CHECKSUM=1879048184,
    SHT_DYNAMIC=6,
    SHT_DYNSYM=11,
    SHT_FINI_ARRAY=15,
    SHT_GNU_ATTRIBUTES=1879048181,
    SHT_GNU_HASH=1879048182,
    SHT_GNU_LIBLIST=1879048183,
    SHT_GNU_verdef=1879048189,
    SHT_GNU_verneed=1879048190,
    SHT_GNU_versym=1879048191,
    SHT_GROUP=17,
    SHT_HASH=5,
    SHT_INIT_ARRAY=14,
    SHT_NOBITS=8,
    SHT_NOTE=7,
    SHT_NULL=0,
    SHT_PREINIT_ARRAY=16,
    SHT_PROGBITS=1,
    SHT_REL=9,
    SHT_RELA=4,
    SHT_SHLIB=10,
    SHT_STRTAB=3,
    SHT_SUNW_COMDAT=1879048187,
    SHT_SUNW_move=1879048186,
    SHT_SUNW_syminfo=1879048188,
    SHT_SYMTAB=2,
    SHT_SYMTAB_SHNDX=18
} Elf_SectionHeaderType;

struct Elf64_Shdr {
    dword sh_name;
    enum Elf_SectionHeaderType sh_type;
    qword sh_flags;
    qword sh_addr;
    qword sh_offset;
    qword sh_size;
    dword sh_link;
    dword sh_info;
    qword sh_addralign;
    qword sh_entsize;
};

typedef struct Elf64_Phdr Elf64_Phdr, *PElf64_Phdr;

struct Elf64_Phdr {
    enum Elf_ProgramHeaderType p_type;
    dword p_flags;
    qword p_offset;
    qword p_vaddr;
    qword p_paddr;
    qword p_filesz;
    qword p_memsz;
    qword p_align;
};

typedef struct Elf64_Rela Elf64_Rela, *PElf64_Rela;

struct Elf64_Rela {
    qword r_offset; // location to apply the relocation action
    qword r_info; // the symbol table index and the type of relocation
    qword r_addend; // a constant addend used to compute the relocatable field value
};

typedef struct Elf64_Sym Elf64_Sym, *PElf64_Sym;

struct Elf64_Sym {
    dword st_name;
    byte st_info;
    byte st_other;
    word st_shndx;
    qword st_value;
    qword st_size;
};

typedef struct Elf64_Ehdr Elf64_Ehdr, *PElf64_Ehdr;

struct Elf64_Ehdr {
    byte e_ident_magic_num;
    char e_ident_magic_str[3];
    byte e_ident_class;
    byte e_ident_data;
    byte e_ident_version;
    byte e_ident_pad[9];
    word e_type;
    word e_machine;
    dword e_version;
    qword e_entry;
    qword e_phoff;
    qword e_shoff;
    dword e_flags;
    word e_ehsize;
    word e_phentsize;
    word e_phnum;
    word e_shentsize;
    word e_shnum;
    word e_shstrndx;
};




undefined8 _DT_INIT(void);
void FUN_00402420(void);
void FUN_00402680(void);
void FUN_00402900(void);
void FUN_00402c20(void);
void entry(void);
void FUN_00402c80(void);
void FUN_00402d00(void);
void FUN_00402d80(void);
void FUN_00402dbe(void);
void FUN_00402dd8(void);
void FUN_00402e06(undefined *puParm1);
undefined8 FUN_00402fa2(undefined8 uParm1);
undefined8 FUN_00402fb0(uint uParm1,undefined8 uParm2,undefined8 uParm3,long lParm4);
undefined8 FUN_0040316b(undefined8 uParm1,long lParm2,long lParm3,long lParm4);
undefined8 FUN_0040340e(undefined8 uParm1,long lParm2,long lParm3,long *plParm4,char *pcParm5,long lParm6);
ulong FUN_00403aa9(undefined8 uParm1,long lParm2,undefined *puParm3);
ulong FUN_00403b5a(int iParm1,undefined8 *puParm2,long lParm3,char cParm4,ulong *puParm5);
void FUN_00404266(long lParm1);
void FUN_004043b6(undefined8 uParm1,long lParm2,char cParm3);
ulong FUN_00404581(uint uParm1,undefined8 *puParm2);
ulong FUN_00404ca2(char *pcParm1);
long FUN_00404d01(long lParm1,ulong uParm2);
ulong FUN_00404d47(long lParm1,ulong uParm2);
undefined FUN_00404ddc(int iParm1);;
void FUN_00404dec(long lParm1);
ulong FUN_00404e22(void);
ulong FUN_00404e84(int iParm1);
undefined8 FUN_00404ea8(void);
undefined8 FUN_00404ed0(void);
ulong FUN_00404ef1(undefined8 uParm1,undefined8 uParm2);
ulong FUN_00404f35(uint uParm1,undefined8 uParm2,undefined8 uParm3);
undefined8 FUN_00404f99(uint uParm1,undefined8 uParm2,char cParm3,long lParm4);
undefined8 FUN_0040507b(uint param_1,uint param_2,long param_3,ulong param_4,ulong param_5,byte param_6,undefined8 param_7,undefined8 param_8,ulong param_9,long *param_10,char *param_11);
void FUN_004053d9(uint uParm1,uint uParm2);
undefined8 FUN_00405400(uint uParm1,ulong uParm2);
undefined8 FUN_004054af(uint param_1,uint param_2,undefined8 param_3,undefined8 param_4,undefined8 param_5,long param_6,int param_7,undefined8 param_8,undefined8 param_9,undefined *param_10);
undefined8 FUN_004059ef(long *plParm1,undefined8 *puParm2);
ulong FUN_00405a46(int iParm1);
undefined8 FUN_00405a6a(void);
ulong FUN_00405a87(undefined8 param_1,undefined8 param_2,byte param_3,undefined8 param_4,undefined8 param_5,undefined8 *param_6,byte *param_7,byte *param_8);
ulong FUN_00405cd3(long lParm1,undefined8 uParm2,uint uParm3,long lParm4,char cParm5,long lParm6);
void FUN_00405f46(void);
undefined8 FUN_00405f57(undefined8 uParm1,undefined8 uParm2,uint uParm3,char cParm4,long lParm5);
undefined8 FUN_00406170(undefined8 uParm1,byte bParm2,byte bParm3,long lParm4);
void FUN_0040626f(uint uParm1,undefined8 uParm2,uint uParm3);
ulong FUN_004062ab(long lParm1);
ulong FUN_004062fe(undefined8 *param_1,char *param_2,long param_3,uint param_4,uint param_5,byte *param_6,long *param_7);
ulong FUN_004070e2(undefined8 uParm1,ulong *puParm2,undefined8 uParm3,ulong *puParm4,int *piParm5,undefined *puParm6);
ulong FUN_00407718(undefined8 uParm1,uint uParm2);
void FUN_00407765(long lParm1,undefined8 uParm2,long lParm3);
void FUN_00407876(long lParm1);
void FUN_004078ae(long lParm1);
ulong FUN_004078e6(long lParm1,undefined8 uParm2,long lParm3);
void FUN_0040799d(undefined8 uParm1,undefined8 uParm2,long lParm3);
void FUN_00407a31(void);
undefined8 FUN_00407a62(undefined8 uParm1,undefined8 uParm2,byte bParm3,char cParm4,char cParm5);
ulong FUN_00407b5e(long lParm1,char cParm2);
ulong FUN_00407b9a(long lParm1,long *plParm2,undefined8 uParm3);
ulong FUN_00407d2c(char *param_1,undefined8 param_2,byte param_3,long *param_4,undefined8 param_5,uint *param_6,byte param_7,char *param_8,undefined *param_9,undefined *param_10);
undefined8 FUN_0040a128(uint *puParm1);
void FUN_0040a272(undefined8 uParm1,undefined8 uParm2,byte bParm3,undefined8 uParm4,undefined8 uParm5,undefined8 uParm6);
void FUN_0040a312(long lParm1);
ulong FUN_0040a356(long lParm1);
ulong FUN_0040a39a(long lParm1);
ulong FUN_0040a3de(void);
ulong FUN_0040a412(ulong *puParm1,ulong uParm2);
ulong FUN_0040a43b(long *plParm1,long *plParm2);
void FUN_0040a490(long lParm1);
void FUN_0040a4c2(undefined8 uParm1,undefined8 uParm2);
undefined8 FUN_0040a519(undefined8 uParm1,undefined8 uParm2);
undefined8 FUN_0040a56b(undefined8 uParm1,undefined8 uParm2,undefined8 uParm3);
void FUN_0040a608(void);
undefined8 FUN_0040a645(void);
void FUN_0040a650(undefined4 uParm1,undefined4 *puParm2);
ulong FUN_0040a6ad(uint *puParm1);
long FUN_0040acbf(long lParm1,long lParm2);
void FUN_0040ad53(undefined8 uParm1,uint *puParm2);
ulong FUN_0040ad97(uint uParm1,undefined8 uParm2,uint uParm3,undefined8 uParm4,uint uParm5,char cParm6);
void FUN_0040af3a(undefined8 uParm1,undefined8 *puParm2);
ulong FUN_0040af70(undefined8 uParm1,uint uParm2,undefined8 uParm3,char cParm4);
ulong FUN_0040b0e3(undefined8 uParm1,uint uParm2,undefined8 uParm3,uint uParm4,uint uParm5);
ulong FUN_0040b197(undefined8 uParm1,uint uParm2,uint uParm3);
long FUN_0040b200(undefined8 uParm1,ulong uParm2);
void FUN_0040b352(void);
long FUN_0040b362(undefined8 uParm1,long lParm2,long lParm3,long lParm4);
void FUN_0040b491(undefined8 uParm1,undefined8 uParm2,long lParm3);
void FUN_0040b512(long lParm1,long lParm2,long lParm3);
long FUN_0040b648(undefined8 uParm1,undefined8 uParm2,undefined8 uParm3,undefined8 uParm4,undefined8 uParm5,code *pcParm6);
void FUN_0040b6ce(char *pcParm1);
void FUN_0040b727(long lParm1,long lParm2,undefined uParm3);
ulong FUN_0040b868(long *plParm1,undefined8 uParm2,long lParm3,long lParm4,long *plParm5);
long FUN_0040bc2f(long lParm1,int iParm2,char cParm3);
void FUN_0040beeb(undefined8 uParm1,uint uParm2);
long FUN_0040bf12(undefined8 uParm1,uint uParm2);
ulong FUN_0040bf4d(undefined8 uParm1,char *pcParm2);
void FUN_0040bfa9(undefined8 uParm1,char *pcParm2);
ulong FUN_0040bff9(ulong uParm1,ulong uParm2,ulong uParm3);
void FUN_0040c0c4(undefined8 uParm1);
ulong FUN_0040c0e3(long *plParm1,undefined8 uParm2,undefined8 uParm3);
char * FUN_0040c18d(char *pcParm1,uint uParm2);
void FUN_0040c993(void);
void FUN_0040ca97(void);
long FUN_0040cb7c(undefined8 uParm1);
long FUN_0040cc4c(undefined8 uParm1);
ulong FUN_0040cc7a(char *pcParm1);
undefined * FUN_0040cd03(undefined8 uParm1);
char * FUN_0040cd9a(char *pcParm1);
ulong FUN_0040ce03(long lParm1);
ulong FUN_0040ce51(char *pcParm1);
void FUN_0040ceb6(uint uParm1,undefined8 uParm2,undefined8 uParm3,uint uParm4);
void FUN_0040cee6(undefined4 uParm1,undefined4 uParm2,undefined4 uParm3,undefined4 uParm4,undefined4 uParm5,undefined4 uParm6,undefined4 uParm7,undefined4 uParm8,undefined8 param_1,uint param_2,undefined8 param_3,undefined8 param_4,undefined8 param_5,undefined8 param_6);
void FUN_0040cff5(long lParm1,undefined8 uParm2,undefined8 *puParm3);
ulong FUN_0040d090(long lParm1,undefined8 uParm2,undefined8 *puParm3);
undefined8 FUN_0040d0ec(uint uParm1);
void FUN_0040d197(uint uParm1,undefined *puParm2);
long FUN_0040d362(undefined8 uParm1,undefined8 uParm2,undefined8 uParm3);
char * FUN_0040d3a3(char *pcParm1);
long FUN_0040d3c3(long lParm1,char *pcParm2,undefined8 *puParm3);
long FUN_0040d510(uint uParm1,long lParm2,long lParm3);
long FUN_0040d590(long *plParm1,undefined8 uParm2);
long FUN_0040d5e7(long lParm1,long lParm2);
ulong FUN_0040d67a(ulong uParm1);
ulong FUN_0040d6e6(ulong uParm1);
ulong FUN_0040d72d(undefined8 uParm1,ulong uParm2);
ulong FUN_0040d764(ulong uParm1,ulong uParm2);
undefined8 FUN_0040d77d(long lParm1);
ulong FUN_0040d876(ulong uParm1,long lParm2);
long * FUN_0040d96c(undefined8 uParm1,undefined1 *puParm2,code *pcParm3,code *pcParm4,long lParm5);
void FUN_0040dad4(long **pplParm1,undefined8 uParm2);
long FUN_0040dbfe(long lParm1);
void FUN_0040dc49(long lParm1,undefined8 *puParm2);
long FUN_0040dc7e(long lParm1,long lParm2,long **pplParm3,char cParm4);
undefined8 FUN_0040de13(long lParm1,long **pplParm2,char cParm3);
undefined8 FUN_0040dfe1(long *plParm1,undefined8 uParm2);
undefined8 FUN_0040e1e5(long lParm1,long lParm2,long *plParm3);
undefined8 FUN_0040e537(undefined8 uParm1,undefined8 uParm2);
long FUN_0040e580(long lParm1,undefined8 uParm2);
ulong FUN_0040e85f(undefined8 *puParm1,ulong uParm2);
ulong FUN_0040e8ab(long lParm1,ulong uParm2);
ulong FUN_0040e8d5(undefined8 *puParm1,undefined8 *puParm2);
ulong FUN_0040e94d(undefined8 *puParm1,undefined8 *puParm2);
void FUN_0040e9c5(undefined8 *puParm1);
void FUN_0040e9f6(long lParm1);
ulong FUN_0040ead3(undefined8 uParm1,uint uParm2,undefined8 uParm3,uint uParm4,uint uParm5);
ulong FUN_0040eb31(undefined8 uParm1,uint uParm2,undefined4 uParm3,undefined8 uParm4,undefined8 uParm5,undefined8 uParm6);
ulong FUN_0040eb7d(undefined1 *puParm1,byte bParm2,uint uParm3);
undefined8 * FUN_0040ec05(undefined8 *puParm1,int iParm2);
char * FUN_0040ec7c(char *pcParm1,int iParm2);
ulong FUN_0040ed1c(long param_1,ulong param_2,long param_3,ulong param_4,uint param_5,uint param_6,long param_7,char *param_8,char *param_9);
undefined1 * FUN_0040fbe6(int iParm1,undefined8 uParm2,undefined8 uParm3,uint *puParm4);
void FUN_0040fe59(uint uParm1,uint uParm2,undefined8 uParm3);
void FUN_0040fe9a(uint uParm1,undefined8 uParm2);
void FUN_0040febe(undefined8 uParm1,undefined8 uParm2,char cParm3);
void FUN_0040ff52(undefined8 uParm1,char cParm2);
void FUN_0040ff7c(undefined8 uParm1);
void FUN_0040ff9b(uint uParm1,uint uParm2,undefined8 uParm3);
void FUN_00410036(uint uParm1,undefined8 uParm2,undefined8 uParm3);
void FUN_00410062(uint uParm1,undefined8 uParm2);
void FUN_0041008b(undefined8 uParm1);
undefined8 FUN_004100aa(undefined4 uParm1);
ulong FUN_004100c9(uint uParm1,long lParm2,uint uParm3,long lParm4,uint uParm5);
long FUN_0041052b(uint uParm1,undefined8 uParm2,undefined8 uParm3);
ulong FUN_0041059c(undefined8 uParm1,undefined8 uParm2);
void FUN_00410741(undefined8 *puParm1,undefined8 *puParm2);
long FUN_0041077c(long lParm1,uint uParm2);
undefined8 FUN_00410ac4(undefined8 uParm1,uint uParm2);
void FUN_00410b46(void);
undefined8 FUN_00410b50(void);
undefined8 FUN_00410b6e(void);
undefined8 FUN_00410b90(long lParm1);
undefined8 FUN_00410ba2(long lParm1);
undefined8 FUN_00410bb4(long lParm1);
void FUN_00410bc6(long lParm1);
void FUN_00410bdc(long lParm1);
ulong FUN_00410bf2(uint uParm1);
void FUN_00410c02(uint uParm1,undefined8 uParm2,undefined8 uParm3);
ulong FUN_00410c2e(undefined8 uParm1,ulong uParm2);
ulong FUN_00410c58(long lParm1,int iParm2,undefined8 uParm3,code *pcParm4,ulong uParm5);
ulong FUN_00410e1f(uint uParm1);
ulong FUN_00410e6f(ulong *puParm1,ulong uParm2);
ulong FUN_00410e98(ulong *puParm1,ulong *puParm2);
ulong FUN_00410eca(undefined8 uParm1,undefined8 *puParm2,long lParm3,uint uParm4);
ulong FUN_0041164a(undefined8 *puParm1);
undefined8 FUN_004117b0(undefined8 uParm1,long *plParm2);
ulong FUN_004118d9(uint uParm1,long lParm2,undefined8 *puParm3);
void FUN_00411d1f(undefined8 uParm1,undefined8 uParm2);
ulong FUN_00411d46(undefined8 uParm1,undefined8 *puParm2);
void FUN_00411f79(undefined8 uParm1,long lParm2,undefined8 uParm3,undefined8 uParm4,undefined8 *puParm5,undefined8 uParm6);
void FUN_004123dd(undefined8 uParm1,undefined8 uParm2,undefined8 uParm3,undefined8 uParm4,uint *puParm5);
void FUN_004124af(undefined4 uParm1,undefined4 uParm2,undefined4 uParm3,undefined4 uParm4,undefined4 uParm5,undefined4 uParm6,undefined4 uParm7,undefined4 uParm8,undefined8 param_1,undefined8 param_2,undefined8 param_3,undefined8 param_4,undefined8 param_5,undefined8 param_6);
ulong FUN_00412565(void);
void FUN_004125a6(undefined8 uParm1,ulong uParm2,ulong uParm3);
void FUN_004125f5(long lParm1,ulong *puParm2,ulong uParm3);
void FUN_004126c0(undefined8 uParm1);
long FUN_004126da(long lParm1);
long FUN_0041270f(long lParm1,long lParm2);
void FUN_00412770(undefined8 uParm1,undefined8 uParm2);
void FUN_004127a4(undefined8 uParm1);
long FUN_004127d1(void);
long FUN_004127fb(void);
ulong FUN_00412834(void);
undefined8 FUN_0041287f(ulong uParm1,ulong uParm2);
ulong FUN_004128fe(uint uParm1);
void FUN_00412927(void);
void FUN_0041295b(uint uParm1);
void FUN_004129cf(uint uParm1,uint uParm2,undefined8 uParm3,undefined8 uParm4);
void FUN_00412a53(undefined4 uParm1,undefined4 uParm2,undefined4 uParm3,undefined4 uParm4,undefined4 uParm5,undefined4 uParm6,undefined4 uParm7,undefined4 uParm8,uint param_1,uint param_2,undefined8 param_3,undefined8 param_4,undefined8 param_5,undefined8 param_6);
ulong FUN_00412b37(undefined4 uParm1,undefined4 uParm2,undefined4 uParm3,undefined4 uParm4,undefined4 uParm5,undefined4 uParm6,undefined4 uParm7,undefined4 uParm8,uint param_1,uint param_2,undefined8 param_3,undefined8 param_4,undefined8 param_5,undefined8 param_6);
undefined8 FUN_00412dee(uint uParm1);
void FUN_00412e51(undefined8 uParm1);
void FUN_00412e75(void);
ulong FUN_00412e83(long lParm1);
undefined8 FUN_00412f53(undefined8 uParm1);
void FUN_00412f72(undefined8 uParm1,undefined8 uParm2,uint uParm3);
void FUN_00412f9d(uint uParm1,undefined8 uParm2,undefined8 uParm3,uint uParm4);
void FUN_00412fca(uint uParm1,undefined8 uParm2,undefined8 uParm3,uint uParm4);
ulong FUN_00413007(uint uParm1,long lParm2,long lParm3,uint uParm4);
long FUN_0041311a(long lParm1,undefined *puParm2);
void FUN_004136a3(long lParm1,int *piParm2);
ulong FUN_00413887(int param_1,undefined8 *param_2,char *param_3,long *param_4,int *param_5,int param_6,int *param_7,int param_8,undefined8 param_9);
char * FUN_00413e71(undefined8 uParm1,undefined8 uParm2,char *pcParm3,int *piParm4,int iParm5);
ulong FUN_00413f3f(uint param_1,undefined8 *param_2,char *param_3,long param_4,undefined8 param_5,uint param_6,uint *param_7,uint param_8);
ulong FUN_00414708(uint param_1,undefined8 param_2,undefined8 param_3,undefined8 param_4,undefined8 param_5,uint param_6,uint param_7);
void FUN_00414798(uint uParm1,undefined8 uParm2,undefined8 uParm3,undefined8 uParm4,undefined8 uParm5);
ulong FUN_004147e2(long lParm1,uint uParm2,uint uParm3);
ulong FUN_0041494f(uint uParm1,char *pcParm2,uint uParm3,undefined8 uParm4);
ulong FUN_00414b22(uint uParm1,long lParm2,uint uParm3,long lParm4,uint uParm5);
void FUN_00414cfe(undefined8 uParm1,undefined8 uParm2);
ulong FUN_00414d23(long lParm1,long lParm2);
undefined8 FUN_00414dda(long lParm1);
ulong FUN_00414e0b(uint *puParm1,byte *pbParm2,long lParm3,undefined8 uParm4);
ulong FUN_00414e8e(long lParm1,uint uParm2);
undefined8 FUN_00414fca(long lParm1,uint uParm2);
undefined8 FUN_0041505d(long lParm1,uint uParm2,long lParm3);
void FUN_0041514a(uint uParm1,undefined8 uParm2,uint uParm3,uint uParm4);
ulong FUN_0041517a(undefined4 uParm1,undefined4 uParm2,undefined4 uParm3,undefined4 uParm4,undefined4 uParm5,undefined4 uParm6,undefined4 uParm7,undefined4 uParm8,uint param_1,undefined8 param_2,uint param_3,undefined8 param_4,undefined8 param_5,undefined8 param_6);
undefined8 FUN_00415341(long lParm1,undefined8 uParm2,undefined8 uParm3);
undefined8 FUN_004153e7(long lParm1,long lParm2);
ulong FUN_00415450(long lParm1,long lParm2);
void FUN_0041595c(uint uParm1,undefined8 uParm2,uint uParm3,undefined8 uParm4);
ulong FUN_0041598f(long lParm1);
void FUN_00415a4c(undefined8 uParm1,undefined8 uParm2);
ulong FUN_00415a71(long lParm1,long lParm2);
undefined8 FUN_00415b01(undefined8 uParm1,uint uParm2,long lParm3);
ulong FUN_00415ba9(long lParm1);
ulong FUN_00415d15(uint uParm1,long lParm2,uint uParm3);
ulong FUN_00415ed6(undefined8 uParm1,undefined8 uParm2,undefined8 uParm3);
undefined8 FUN_00415ff7(undefined8 uParm1,undefined8 uParm2,undefined4 uParm3,undefined4 *puParm4);
void FUN_00416033(undefined8 uParm1,uint uParm2,uint uParm3);
ulong FUN_0041606f(uint *puParm1,undefined8 uParm2,uint uParm3);
void FUN_00416123(uint uParm1,undefined8 uParm2);
ulong FUN_00416158(ulong uParm1,undefined4 uParm2);
ulong FUN_00416174(byte *pbParm1,byte *pbParm2);
undefined *FUN_004161eb(uint uParm1,undefined8 uParm2,undefined *puParm3,ulong uParm4,code **ppcParm5,code *pcParm6);
undefined8 FUN_0041648a(uint uParm1,char cParm2);
undefined8 FUN_00416505(undefined8 uParm1);
undefined8 FUN_00416590(void);
void FUN_0041659d(undefined8 *puParm1);
ulong FUN_004165f1(uint uParm1);
ulong FUN_004166c2(char *pcParm1,ulong uParm2);
undefined1 * FUN_0041671c(void);
char * FUN_00416acf(void);
undefined8 * FUN_00416b9d(undefined8 uParm1);
undefined8 FUN_00416be4(undefined8 uParm1,undefined8 uParm2);
long FUN_00416c27(long lParm1);
ulong FUN_00416c39(undefined8 *puParm1,ulong uParm2);
void FUN_00416dea(undefined8 uParm1);
ulong FUN_00416e15(undefined8 *puParm1);
ulong * FUN_00416e5b(ulong uParm1,ulong uParm2);
undefined8 * FUN_00416ebb(undefined8 uParm1,undefined8 uParm2);
void FUN_00416f02(long lParm1,ulong uParm2,ulong uParm3);
long FUN_00417167(long lParm1,ulong uParm2);
void FUN_00417256(undefined8 *puParm1,long lParm2,long lParm3);
void FUN_004172f6(ulong *puParm1,ulong uParm2,ulong uParm3);
void FUN_0041743b(long *plParm1,undefined8 uParm2,undefined8 uParm3);
undefined8 FUN_00417491(long *plParm1);
undefined8 FUN_004174e1(undefined8 uParm1);
undefined8 FUN_004174fb(long lParm1,undefined8 uParm2);
void FUN_0041753f(ulong *puParm1,long *plParm2);
void FUN_00417bcf(long lParm1);
void FUN_00418156(uint uParm1);
ulong FUN_0041817c(long lParm1,uint uParm2,uint uParm3);
void FUN_00418325(undefined8 uParm1,undefined8 uParm2);
ulong FUN_0041834c(undefined8 uParm1);
ulong FUN_00418404(undefined8 uParm1,undefined8 uParm2);
undefined8 FUN_004184ab(uint uParm1,long lParm2,undefined8 uParm3,undefined8 uParm4);
ulong FUN_00418559(void);
long FUN_004185a6(ulong uParm1,long lParm2,ulong uParm3,long lParm4,long *plParm5);
int * FUN_004187f2(ulong uParm1,uint *puParm2,ulong uParm3,uint *puParm4,ulong *puParm5);
char * FUN_004192d2(ulong uParm1,long lParm2,long lParm3);
long FUN_00419540(int *param_1,long *param_2);
long FUN_0041972b(undefined auParm1 [16],int *piParm2,long *plParm3);
undefined8 FUN_00419aea(int iParm1,long lParm2,uint *puParm3,long lParm4,uint uParm5);
void FUN_0041a2fd(uint param_1);
void FUN_0041a347(undefined8 uParm1,uint uParm2);
ulong FUN_0041a399(void);
ulong FUN_0041a72b(undefined auParm1 [16],undefined8 uParm2,undefined8 uParm3);
ulong FUN_0041ab03(char *pcParm1,long lParm2);
undefined8 FUN_0041ab5c(long param_1,long param_2,undefined param_3,int param_4,undefined8 param_5,ulong param_6,int param_7,ulong param_8,int param_9);
ulong FUN_00422289(uint uParm1);
long FUN_004222a8(undefined8 uParm1,undefined8 uParm2);
void FUN_00422394(undefined8 uParm1,int *piParm2,undefined8 uParm3);
void FUN_00422435(int *param_1);
ulong FUN_004224f4(ulong uParm1,long lParm2);
void FUN_00422528(undefined8 uParm1,undefined8 uParm2,undefined8 uParm3);
void FUN_00422563(undefined8 uParm1,undefined8 uParm2,undefined8 uParm3,undefined8 uParm4);
ulong FUN_004225b4(ulong uParm1,ulong uParm2);
undefined8 FUN_004225cf(uint *puParm1,ulong *puParm2);
undefined8 FUN_00422d6f(char *pcParm1,ulong *puParm2,char **ppcParm3);
ulong FUN_00424025(undefined auParm1 [16]);
ulong FUN_00424060(void);
void FUN_004240a0(void);
undefined8 _DT_FINI(void);
undefined FUN_0062d000();
undefined FUN_0062d008();
undefined FUN_0062d010();
undefined FUN_0062d018();
undefined FUN_0062d020();
undefined FUN_0062d028();
undefined FUN_0062d030();
undefined FUN_0062d038();
undefined FUN_0062d040();
undefined FUN_0062d048();
undefined FUN_0062d050();
undefined FUN_0062d058();
undefined FUN_0062d060();
undefined FUN_0062d068();
undefined FUN_0062d070();
undefined FUN_0062d078();
undefined FUN_0062d080();
undefined FUN_0062d088();
undefined FUN_0062d090();
undefined FUN_0062d098();
undefined FUN_0062d0a0();
undefined FUN_0062d0a8();
undefined FUN_0062d0b0();
undefined FUN_0062d0b8();
undefined FUN_0062d0c0();
undefined FUN_0062d0c8();
undefined FUN_0062d0d0();
undefined FUN_0062d0d8();
undefined FUN_0062d0e0();
undefined FUN_0062d0e8();
undefined FUN_0062d0f0();
undefined FUN_0062d0f8();
undefined FUN_0062d100();
undefined FUN_0062d108();
undefined FUN_0062d110();
undefined FUN_0062d118();
undefined FUN_0062d120();
undefined FUN_0062d128();
undefined FUN_0062d130();
undefined FUN_0062d138();
undefined FUN_0062d140();
undefined FUN_0062d148();
undefined FUN_0062d150();
undefined FUN_0062d158();
undefined FUN_0062d160();
undefined FUN_0062d168();
undefined FUN_0062d170();
undefined FUN_0062d178();
undefined FUN_0062d180();
undefined FUN_0062d188();
undefined FUN_0062d190();
undefined FUN_0062d198();
undefined FUN_0062d1a0();
undefined FUN_0062d1a8();
undefined FUN_0062d1b0();
undefined FUN_0062d1b8();
undefined FUN_0062d1c0();
undefined FUN_0062d1c8();
undefined FUN_0062d1d0();
undefined FUN_0062d1d8();
undefined FUN_0062d1e0();
undefined FUN_0062d1e8();
undefined FUN_0062d1f0();
undefined FUN_0062d1f8();
undefined FUN_0062d200();
undefined FUN_0062d208();
undefined FUN_0062d210();
undefined FUN_0062d218();
undefined FUN_0062d220();
undefined FUN_0062d228();
undefined FUN_0062d230();
undefined FUN_0062d238();
undefined FUN_0062d240();
undefined FUN_0062d248();
undefined FUN_0062d250();
undefined FUN_0062d258();
undefined FUN_0062d260();
undefined FUN_0062d268();
undefined FUN_0062d270();
undefined FUN_0062d278();
undefined FUN_0062d280();
undefined FUN_0062d288();
undefined FUN_0062d290();
undefined FUN_0062d298();
undefined FUN_0062d2a0();
undefined FUN_0062d2a8();
undefined FUN_0062d2b0();
undefined FUN_0062d2b8();
undefined FUN_0062d2c0();
undefined FUN_0062d2c8();
undefined FUN_0062d2d0();
undefined FUN_0062d2d8();
undefined FUN_0062d2e0();
undefined FUN_0062d2e8();
undefined FUN_0062d2f0();
undefined FUN_0062d2f8();
undefined FUN_0062d300();
undefined FUN_0062d308();
undefined FUN_0062d310();
undefined FUN_0062d318();
undefined FUN_0062d320();
undefined FUN_0062d328();
undefined FUN_0062d330();
undefined FUN_0062d338();
undefined FUN_0062d340();
undefined FUN_0062d348();
undefined FUN_0062d350();
undefined FUN_0062d358();
undefined FUN_0062d360();
undefined FUN_0062d368();
undefined FUN_0062d370();
undefined FUN_0062d378();
undefined FUN_0062d380();
undefined FUN_0062d388();
undefined FUN_0062d390();
undefined FUN_0062d398();
undefined FUN_0062d3a0();
undefined FUN_0062d3a8();
undefined FUN_0062d3b0();
undefined FUN_0062d3b8();
undefined FUN_0062d3c0();
undefined FUN_0062d3c8();
undefined FUN_0062d3d0();
undefined FUN_0062d3d8();
undefined FUN_0062d3e0();
undefined FUN_0062d3e8();


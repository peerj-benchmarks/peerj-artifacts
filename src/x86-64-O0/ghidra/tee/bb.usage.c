
#include "tee.h"

long null_ARRAY_0061571_0_8_;
long null_ARRAY_0061571_8_8_;
long null_ARRAY_0061588_0_8_;
long null_ARRAY_0061588_16_8_;
long null_ARRAY_0061588_24_8_;
long null_ARRAY_0061588_32_8_;
long null_ARRAY_0061588_40_8_;
long null_ARRAY_0061588_48_8_;
long null_ARRAY_0061588_8_8_;
long null_ARRAY_006159e_0_4_;
long null_ARRAY_006159e_16_8_;
long null_ARRAY_006159e_4_4_;
long null_ARRAY_006159e_8_4_;
long DAT_00412440;
long DAT_004124fd;
long DAT_0041257b;
long DAT_00412b48;
long DAT_00412b8b;
long DAT_00412b9e;
long DAT_00412ba1;
long DAT_00412ba3;
long DAT_00412c1a;
long DAT_00412c32;
long DAT_00412c78;
long DAT_00412d9e;
long DAT_00412da2;
long DAT_00412da7;
long DAT_00412da9;
long DAT_00413413;
long DAT_004137e0;
long DAT_004137f8;
long DAT_004137fd;
long DAT_00413867;
long DAT_004138f8;
long DAT_004138fb;
long DAT_00413949;
long DAT_0041394d;
long DAT_004139c0;
long DAT_004139c1;
long DAT_00413ba8;
long DAT_006152f8;
long DAT_00615308;
long DAT_00615318;
long DAT_006156f0;
long DAT_00615700;
long DAT_00615778;
long DAT_0061577c;
long DAT_00615780;
long DAT_006157c0;
long DAT_006157c8;
long DAT_006157d0;
long DAT_006157e0;
long DAT_006157e8;
long DAT_00615800;
long DAT_00615808;
long DAT_00615850;
long DAT_00615851;
long DAT_00615854;
long DAT_00615858;
long DAT_00615860;
long DAT_00615868;
long DAT_006159c0;
long DAT_00615a18;
long DAT_00615a20;
long DAT_00615a28;
long DAT_00615a38;
long fde_004146c8;
long FLOAT_UNKNOWN;
long null_ARRAY_00412600;
long null_ARRAY_00412700;
long null_ARRAY_00412730;
long null_ARRAY_00413e00;
long null_ARRAY_00615710;
long null_ARRAY_00615740;
long null_ARRAY_00615820;
long null_ARRAY_00615880;
long null_ARRAY_006158c0;
long null_ARRAY_006159e0;
long PTR_DAT_006156e0;
long PTR_FUN_006156e8;
long PTR_null_ARRAY_00615720;
ulong
FUN_00401da8 (uint uParm1)
{
  char cVar1;
  int iVar2;
  long lVar3;
  uint *puVar4;
  char *pcVar5;

  if (uParm1 == 0)
    {
      func_0x00401610 ("Usage: %s [OPTION]... [FILE]...\n", DAT_00615868);
      func_0x004017b0
	("Copy standard input to each FILE, and also to standard output.\n\n  -a, --append              append to the given FILEs, do not overwrite\n  -i, --ignore-interrupts   ignore interrupt signals\n",
	 DAT_006157c0);
      func_0x004017b0
	("  -p                        diagnose errors writing to non pipes\n      --output-error[=MODE]   set behavior on write error.  See MODE below\n",
	 DAT_006157c0);
      func_0x004017b0 ("      --help     display this help and exit\n",
		       DAT_006157c0);
      func_0x004017b0
	("      --version  output version information and exit\n",
	 DAT_006157c0);
      pcVar5 = (char *) DAT_006157c0;
      func_0x004017b0
	("\nMODE determines behavior with write errors on the outputs:\n  \'warn\'         diagnose errors writing to any output\n  \'warn-nopipe\'  diagnose errors writing to any output not a pipe\n  \'exit\'         exit on error writing to any output\n  \'exit-nopipe\'  exit on error writing to any output not a pipe\nThe default MODE for the -p option is \'warn-nopipe\'.\nThe default operation when --output-error is not specified, is to\nexit immediately on error writing to a pipe, and diagnose errors\nwriting to non pipe outputs.\n");
      imperfection_wrapper ();	//    FUN_00401bfe();
    }
  else
    {
      pcVar5 = "Try \'%s --help\' for more information.\n";
      func_0x004017a0 (DAT_006157e0,
		       "Try \'%s --help\' for more information.\n",
		       DAT_00615868);
    }
  func_0x00401970 ();
  FUN_00402b13 (*(undefined8 *) pcVar5);
  func_0x00401920 (6, &DAT_0041257b);
  func_0x00401900 (FUN_004028d8);
  DAT_00615850 = 0;
  DAT_00615851 = '\0';
LAB_00401fba:
  ;
  do
    {
      while (true)
	{
	  while (true)
	    {
	      imperfection_wrapper ();	//        iVar2 = FUN_0040603d((ulong)uParm1, pcVar5, &DAT_00412b8b, null_ARRAY_00412600, 0);
	      if (iVar2 == -1)
		{
		  if (DAT_00615851 != '\0')
		    {
		      func_0x00401810 (2, 1);
		    }
		  if (DAT_00615854 != 0)
		    {
		      func_0x00401810 (0xd, 1);
		    }
		  imperfection_wrapper ();	//          cVar1 = FUN_0040208d((ulong)(uParm1 - DAT_00615778), (undefined8 *)pcVar5 + (long)DAT_00615778, (undefined8 *)pcVar5 + (long)DAT_00615778, (ulong)(uParm1 - DAT_00615778));
		  iVar2 = func_0x00401a50 (0);
		  if (iVar2 != 0)
		    {
		      puVar4 = (uint *) func_0x00401960 ();
		      imperfection_wrapper ();	//            FUN_004049a9(1, (ulong)*puVar4, &DAT_00412b9e, "standard input");
		    }
		  return (ulong) (cVar1 == '\0');
		}
	      if (iVar2 != 0x61)
		break;
	      DAT_00615850 = 1;
	    }
	  if (0x61 < iVar2)
	    break;
	  if (iVar2 != -0x83)
	    {
	      if (iVar2 != -0x82)
		goto LAB_00401fb0;
	      FUN_00401da8 (0);
	    }
	  imperfection_wrapper ();	//      FUN_00404655(DAT_006157c0, &DAT_00412b48, "GNU coreutils", PTR_DAT_006156e0, "Mike Parker", "Richard M. Stallman", "David MacKenzie", 0);
	  func_0x00401970 (0);
	LAB_00401fb0:
	  ;
	  imperfection_wrapper ();	//      FUN_00401da8();
	}
      if (iVar2 != 0x69)
	{
	  if (iVar2 != 0x70)
	    goto LAB_00401fb0;
	  if (DAT_00615a38 == 0)
	    {
	      DAT_00615854 = 2;
	    }
	  else
	    {
	      lVar3 =
		FUN_00402852 ("--output-error", DAT_00615a38,
			      null_ARRAY_00412700, null_ARRAY_00412730, 4,
			      PTR_FUN_006156e8);
	      DAT_00615854 = *(int *) (null_ARRAY_00412730 + lVar3 * 4);
	    }
	  goto LAB_00401fba;
	}
      DAT_00615851 = '\x01';
    }
  while (true);
}

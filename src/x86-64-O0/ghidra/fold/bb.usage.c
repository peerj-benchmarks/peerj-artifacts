
#include "fold.h"

long null_ARRAY_0061563_0_8_;
long null_ARRAY_0061563_8_8_;
long null_ARRAY_0061578_0_8_;
long null_ARRAY_0061578_16_8_;
long null_ARRAY_0061578_24_8_;
long null_ARRAY_0061578_32_8_;
long null_ARRAY_0061578_40_8_;
long null_ARRAY_0061578_48_8_;
long null_ARRAY_0061578_8_8_;
long null_ARRAY_006158c_0_4_;
long null_ARRAY_006158c_16_8_;
long null_ARRAY_006158c_4_4_;
long null_ARRAY_006158c_8_4_;
long DAT_004123c3;
long DAT_0041247d;
long DAT_004124fb;
long DAT_0041280e;
long DAT_00412813;
long DAT_00412815;
long DAT_00412817;
long DAT_0041285c;
long DAT_004128a0;
long DAT_004129de;
long DAT_004129e2;
long DAT_004129e7;
long DAT_004129e9;
long DAT_00413053;
long DAT_00413420;
long DAT_004137b8;
long DAT_004137bd;
long DAT_00413827;
long DAT_004138b8;
long DAT_004138bb;
long DAT_00413909;
long DAT_0041390d;
long DAT_00413980;
long DAT_00413981;
long DAT_00615218;
long DAT_00615228;
long DAT_00615238;
long DAT_00615608;
long DAT_00615620;
long DAT_00615698;
long DAT_0061569c;
long DAT_006156a0;
long DAT_006156c0;
long DAT_006156c8;
long DAT_006156d0;
long DAT_006156e0;
long DAT_006156e8;
long DAT_00615700;
long DAT_00615708;
long DAT_00615750;
long DAT_00615751;
long DAT_00615752;
long DAT_00615758;
long DAT_00615760;
long DAT_00615768;
long DAT_00615770;
long DAT_00615778;
long DAT_006158f8;
long DAT_00615900;
long DAT_00615908;
long DAT_00615918;
long fde_00414668;
long FLOAT_UNKNOWN;
long null_ARRAY_00412540;
long null_ARRAY_004125c0;
long null_ARRAY_00413dc0;
long null_ARRAY_00615630;
long null_ARRAY_00615660;
long null_ARRAY_00615720;
long null_ARRAY_00615780;
long null_ARRAY_006157c0;
long null_ARRAY_006158c0;
long PTR_DAT_00615600;
long PTR_null_ARRAY_00615640;
long stack0x00000008;
ulong
FUN_00401e2d (uint uParm1)
{
  char cVar1;
  ulong uStack40;

  if (uParm1 == 0)
    {
      func_0x00401650 ("Usage: %s [OPTION]... [FILE]...\n", DAT_00615778);
      func_0x00401800
	("Wrap input lines in each FILE, writing to standard output.\n",
	 DAT_006156c0);
      FUN_00401c5d ();
      FUN_00401c77 ();
      func_0x00401800
	("  -b, --bytes         count bytes rather than columns\n  -s, --spaces        break at spaces\n  -w, --width=WIDTH   use WIDTH columns instead of 80\n",
	 DAT_006156c0);
      func_0x00401800 ("      --help     display this help and exit\n",
		       DAT_006156c0);
      cVar1 = (char) DAT_006156c0;
      func_0x00401800
	("      --version  output version information and exit\n");
      imperfection_wrapper ();	//    FUN_00401c91();
    }
  else
    {
      cVar1 = -0x80;
      func_0x004017f0 (DAT_006156e0,
		       "Try \'%s --help\' for more information.\n",
		       DAT_00615778);
    }
  uStack40 = (ulong) uParm1;
  func_0x004019c0 ();
  if (DAT_00615751 == '\x01')
    {
      uStack40 = uStack40 + 1;
    }
  else
    {
      if (cVar1 == '\b')
	{
	  if (uStack40 != 0)
	    {
	      uStack40 = uStack40 - 1;
	    }
	}
      else
	{
	  if (cVar1 == '\r')
	    {
	      uStack40 = 0;
	    }
	  else
	    {
	      if (cVar1 == '\t')
		{
		  uStack40 = (uStack40 & 0xfffffffffffffff8) + 8;
		}
	      else
		{
		  uStack40 = uStack40 + 1;
		}
	    }
	}
    }
  return uStack40;
}

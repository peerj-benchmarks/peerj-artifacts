typedef unsigned char   undefined;

typedef unsigned char    byte;
typedef unsigned char    dwfenc;
typedef unsigned int    dword;
typedef unsigned long    qword;
typedef unsigned char    undefined1;
typedef unsigned short    undefined2;
typedef unsigned int    undefined4;
typedef unsigned long    undefined6;
typedef unsigned long    undefined8;
typedef unsigned short    word;
#define unkbyte9   unsigned long long
#define unkbyte10   unsigned long long
#define unkbyte11   unsigned long long
#define unkbyte12   unsigned long long
#define unkbyte13   unsigned long long
#define unkbyte14   unsigned long long
#define unkbyte15   unsigned long long
#define unkbyte16   unsigned long long

#define unkuint9   unsigned long long
#define unkuint10   unsigned long long
#define unkuint11   unsigned long long
#define unkuint12   unsigned long long
#define unkuint13   unsigned long long
#define unkuint14   unsigned long long
#define unkuint15   unsigned long long
#define unkuint16   unsigned long long

#define unkint9   long long
#define unkint10   long long
#define unkint11   long long
#define unkint12   long long
#define unkint13   long long
#define unkint14   long long
#define unkint15   long long
#define unkint16   long long

#define unkfloat1   float
#define unkfloat2   float
#define unkfloat3   float
#define unkfloat5   double
#define unkfloat6   double
#define unkfloat7   double
#define unkfloat9   long double
#define unkfloat11   long double
#define unkfloat12   long double
#define unkfloat13   long double
#define unkfloat14   long double
#define unkfloat15   long double
#define unkfloat16   long double

#define BADSPACEBASE   void
#define code   void

#define ulong long
#define true 1
#define bool int
#define false 0
#define float10 int
#define ushort int
#define undefined7 long
#define undefined2 long
#define uint5 long
#define uint6 long

// Add definition for uint
typedef unsigned int    uint;

#define sbyte char

extern imperfection_wrapper(void);
typedef struct eh_frame_hdr eh_frame_hdr, *Peh_frame_hdr;

struct eh_frame_hdr {
    byte eh_frame_hdr_version; // Exception Handler Frame Header Version
    dwfenc eh_frame_pointer_encoding; // Exception Handler Frame Pointer Encoding
    dwfenc eh_frame_desc_entry_count_encoding; // Encoding of # of Exception Handler FDEs
    dwfenc eh_frame_table_encoding; // Exception Handler Table Encoding
};

typedef struct fde_table_entry fde_table_entry, *Pfde_table_entry;

struct fde_table_entry {
    dword initial_loc; // Initial Location
    dword data_loc; // Data location
};

typedef enum Elf64_DynTag {
    DT_AUDIT=1879047932,
    DT_AUXILIARY=2147483645,
    DT_BIND_NOW=24,
    DT_CHECKSUM=1879047672,
    DT_CONFIG=1879047930,
    DT_DEBUG=21,
    DT_DEPAUDIT=1879047931,
    DT_ENCODING=32,
    DT_FEATURE_1=1879047676,
    DT_FILTER=2147483647,
    DT_FINI=13,
    DT_FINI_ARRAY=26,
    DT_FINI_ARRAYSZ=28,
    DT_FLAGS=30,
    DT_FLAGS_1=1879048187,
    DT_GNU_CONFLICT=1879047928,
    DT_GNU_CONFLICTSZ=1879047670,
    DT_GNU_HASH=1879047925,
    DT_GNU_LIBLIST=1879047929,
    DT_GNU_LIBLISTSZ=1879047671,
    DT_GNU_PRELINKED=1879047669,
    DT_HASH=4,
    DT_INIT=12,
    DT_INIT_ARRAY=25,
    DT_INIT_ARRAYSZ=27,
    DT_JMPREL=23,
    DT_MOVEENT=1879047674,
    DT_MOVESZ=1879047675,
    DT_MOVETAB=1879047934,
    DT_NEEDED=1,
    DT_NULL=0,
    DT_PLTGOT=3,
    DT_PLTPAD=1879047933,
    DT_PLTPADSZ=1879047673,
    DT_PLTREL=20,
    DT_PLTRELSZ=2,
    DT_POSFLAG_1=1879047677,
    DT_PREINIT_ARRAYSZ=33,
    DT_REL=17,
    DT_RELA=7,
    DT_RELACOUNT=1879048185,
    DT_RELAENT=9,
    DT_RELASZ=8,
    DT_RELCOUNT=1879048186,
    DT_RELENT=19,
    DT_RELSZ=18,
    DT_RPATH=15,
    DT_RUNPATH=29,
    DT_SONAME=14,
    DT_STRSZ=10,
    DT_STRTAB=5,
    DT_SYMBOLIC=16,
    DT_SYMENT=11,
    DT_SYMINENT=1879047679,
    DT_SYMINFO=1879047935,
    DT_SYMINSZ=1879047678,
    DT_SYMTAB=6,
    DT_TEXTREL=22,
    DT_TLSDESC_GOT=1879047927,
    DT_TLSDESC_PLT=1879047926,
    DT_VERDEF=1879048188,
    DT_VERDEFNUM=1879048189,
    DT_VERNEED=1879048190,
    DT_VERNEEDNUM=1879048191,
    DT_VERSYM=1879048176
} Elf64_DynTag;

typedef enum Elf_ProgramHeaderType {
    PT_DYNAMIC=2,
    PT_GNU_EH_FRAME=1685382480,
    PT_GNU_RELRO=1685382482,
    PT_GNU_STACK=1685382481,
    PT_INTERP=3,
    PT_LOAD=1,
    PT_NOTE=4,
    PT_NULL=0,
    PT_PHDR=6,
    PT_SHLIB=5,
    PT_TLS=7
} Elf_ProgramHeaderType;

typedef struct Elf64_Dyn Elf64_Dyn, *PElf64_Dyn;

struct Elf64_Dyn {
    enum Elf64_DynTag d_tag;
    qword d_val;
};

typedef struct Elf64_Shdr Elf64_Shdr, *PElf64_Shdr;

typedef enum Elf_SectionHeaderType {
    SHT_CHECKSUM=1879048184,
    SHT_DYNAMIC=6,
    SHT_DYNSYM=11,
    SHT_FINI_ARRAY=15,
    SHT_GNU_ATTRIBUTES=1879048181,
    SHT_GNU_HASH=1879048182,
    SHT_GNU_LIBLIST=1879048183,
    SHT_GNU_verdef=1879048189,
    SHT_GNU_verneed=1879048190,
    SHT_GNU_versym=1879048191,
    SHT_GROUP=17,
    SHT_HASH=5,
    SHT_INIT_ARRAY=14,
    SHT_NOBITS=8,
    SHT_NOTE=7,
    SHT_NULL=0,
    SHT_PREINIT_ARRAY=16,
    SHT_PROGBITS=1,
    SHT_REL=9,
    SHT_RELA=4,
    SHT_SHLIB=10,
    SHT_STRTAB=3,
    SHT_SUNW_COMDAT=1879048187,
    SHT_SUNW_move=1879048186,
    SHT_SUNW_syminfo=1879048188,
    SHT_SYMTAB=2,
    SHT_SYMTAB_SHNDX=18
} Elf_SectionHeaderType;

struct Elf64_Shdr {
    dword sh_name;
    enum Elf_SectionHeaderType sh_type;
    qword sh_flags;
    qword sh_addr;
    qword sh_offset;
    qword sh_size;
    dword sh_link;
    dword sh_info;
    qword sh_addralign;
    qword sh_entsize;
};

typedef struct Elf64_Phdr Elf64_Phdr, *PElf64_Phdr;

struct Elf64_Phdr {
    enum Elf_ProgramHeaderType p_type;
    dword p_flags;
    qword p_offset;
    qword p_vaddr;
    qword p_paddr;
    qword p_filesz;
    qword p_memsz;
    qword p_align;
};

typedef struct Elf64_Sym Elf64_Sym, *PElf64_Sym;

struct Elf64_Sym {
    dword st_name;
    byte st_info;
    byte st_other;
    word st_shndx;
    qword st_value;
    qword st_size;
};

typedef struct Elf64_Rela Elf64_Rela, *PElf64_Rela;

struct Elf64_Rela {
    qword r_offset; // location to apply the relocation action
    qword r_info; // the symbol table index and the type of relocation
    qword r_addend; // a constant addend used to compute the relocatable field value
};

typedef struct Elf64_Ehdr Elf64_Ehdr, *PElf64_Ehdr;

struct Elf64_Ehdr {
    byte e_ident_magic_num;
    char e_ident_magic_str[3];
    byte e_ident_class;
    byte e_ident_data;
    byte e_ident_version;
    byte e_ident_pad[9];
    word e_type;
    word e_machine;
    dword e_version;
    qword e_entry;
    qword e_phoff;
    qword e_shoff;
    dword e_flags;
    word e_ehsize;
    word e_phentsize;
    word e_phnum;
    word e_shentsize;
    word e_shnum;
    word e_shstrndx;
};




undefined8 _DT_INIT(void);
void FUN_00401bb0(void);
void entry(void);
void FUN_004021f0(void);
void FUN_00402270(void);
void FUN_004022f0(void);
undefined8 FUN_0040232e(int iParm1);
ulong FUN_00402354(byte bParm1);
void FUN_00402363(void);
void FUN_0040237d(void);
void FUN_00402397(undefined *puParm1);
char * FUN_00402533(void);
char * FUN_00402554(char *pcParm1);
void FUN_0040289f(undefined8 *puParm1);
void FUN_00402975(void);
void FUN_00402ab1(char *pcParm1,long *plParm2);
ulong FUN_00402bb0(long *plParm1,long lParm2);
ulong FUN_00402d13(ulong *puParm1,ulong *puParm2);
undefined8 FUN_00402d77(undefined8 uParm1,long *plParm2);
void FUN_00402e21(void);
void FUN_00402e56(undefined8 uParm1);
void FUN_00402ef0(undefined8 uParm1,long *plParm2);
void FUN_00403052(int iParm1);
void FUN_004036ba(long lParm1);
void FUN_004036e8(byte *pbParm1,byte *pbParm2);
void FUN_00403a5d(void);
void FUN_00403dcd(char **ppcParm1);
void FUN_00404984(void);
void FUN_00404b52(void);
void FUN_00404d86(void);
void FUN_00405194(void);
undefined8 FUN_0040523b(uint uParm1);
undefined8 FUN_0040534f(uint uParm1,undefined8 *puParm2);
void FUN_00405c1d(void);
long FUN_00405c2d(undefined8 uParm1,long lParm2,long lParm3,long lParm4);
void FUN_00405d5c(undefined8 uParm1,undefined8 uParm2,long lParm3);
void FUN_00405ddd(long lParm1,long lParm2,long lParm3);
long FUN_00405f13(undefined8 uParm1,undefined8 uParm2,undefined8 uParm3,undefined8 uParm4,undefined8 uParm5,code *pcParm6);
void FUN_00405f99(void);
undefined8 FUN_0040607e(uint uParm1);
long FUN_004060d1(undefined8 uParm1,undefined8 uParm2,undefined8 uParm3);
void FUN_00406227(long lParm1);
ulong FUN_00406304(char *pcParm1,undefined8 uParm2);
long FUN_00406756(long lParm1,long lParm2);
ulong FUN_0040695c(undefined1 *puParm1,byte bParm2,uint uParm3);
undefined8 * FUN_004069e4(undefined8 *puParm1,int iParm2);
char * FUN_00406a5b(char *pcParm1,int iParm2);
ulong FUN_00406afb(long param_1,ulong param_2,long param_3,ulong param_4,uint param_5,uint param_6,long param_7,char *param_8,char *param_9);
undefined1 * FUN_004079c5(int iParm1,undefined8 uParm2,undefined8 uParm3,uint *puParm4);
void FUN_00407c38(uint uParm1,uint uParm2,undefined8 uParm3);
void FUN_00407c79(undefined8 uParm1,undefined8 uParm2,char cParm3);
void FUN_00407d0d(undefined8 uParm1,char cParm2);
void FUN_00407d37(undefined8 uParm1);
void FUN_00407d56(uint uParm1,uint uParm2,undefined8 uParm3);
void FUN_00407df1(uint uParm1,undefined8 uParm2,undefined8 uParm3);
void FUN_00407e1d(uint uParm1,undefined8 uParm2);
void FUN_00407e46(undefined8 uParm1);
long FUN_00407e65(undefined8 uParm1,ulong *puParm2);
long FUN_004080a1(undefined8 uParm1,undefined8 uParm2,undefined8 uParm3);
void FUN_00408142(undefined8 uParm1,undefined8 uParm2);
undefined8 FUN_0040816c(int iParm1);
byte * FUN_00408192(undefined8 uParm1,int iParm2);
void FUN_004084aa(undefined8 uParm1,long lParm2,undefined8 uParm3,undefined8 uParm4,undefined8 *puParm5,undefined8 uParm6);
void FUN_0040890e(undefined8 uParm1,undefined8 uParm2,undefined8 uParm3,undefined8 uParm4,uint *puParm5);
void FUN_004089e0(undefined4 uParm1,undefined4 uParm2,undefined4 uParm3,undefined4 uParm4,undefined4 uParm5,undefined4 uParm6,undefined4 uParm7,undefined4 uParm8,undefined8 param_1,undefined8 param_2,undefined8 param_3,undefined8 param_4,undefined8 param_5,undefined8 param_6);
void FUN_00408a96(ulong uParm1,ulong uParm2);
void FUN_00408ad7(long lParm1,ulong *puParm2,ulong uParm3);
void FUN_00408ba2(undefined8 uParm1);
long FUN_00408bbc(long lParm1);
long FUN_00408bf1(long lParm1,long lParm2);
long FUN_00408c52(void);
long FUN_00408c7c(undefined8 uParm1,undefined8 uParm2,undefined8 uParm3);
undefined8 FUN_00408cc9(long *plParm1,int iParm2);
ulong FUN_00408d67(undefined8 uParm1,uint uParm2,int iParm3);
ulong FUN_00408da8(char *pcParm1,char **ppcParm2,uint uParm3,undefined8 *puParm4,long lParm5);
undefined8 FUN_00409147(ulong uParm1,ulong uParm2);
ulong FUN_004091c6(uint uParm1);
void FUN_004091ef(void);
void FUN_00409223(uint uParm1);
void FUN_00409297(uint uParm1,uint uParm2,undefined8 uParm3,undefined8 uParm4);
void FUN_0040931b(undefined4 uParm1,undefined4 uParm2,undefined4 uParm3,undefined4 uParm4,undefined4 uParm5,undefined4 uParm6,undefined4 uParm7,undefined4 uParm8,uint param_1,uint param_2,undefined8 param_3,undefined8 param_4,undefined8 param_5,undefined8 param_6);
ulong FUN_004093ff(undefined8 uParm1);
void FUN_004094b7(undefined8 uParm1);
void FUN_004094db(void);
ulong FUN_004094e9(long lParm1);
undefined8 FUN_004095b9(undefined8 uParm1);
void FUN_004095d8(undefined8 uParm1,undefined8 uParm2,uint uParm3);
void FUN_00409603(long lParm1,int *piParm2);
ulong FUN_004097e7(int param_1,undefined8 *param_2,char *param_3,long *param_4,int *param_5,int param_6,int *param_7,int param_8,undefined8 param_9);
char * FUN_00409dd1(undefined8 uParm1,undefined8 uParm2,char *pcParm3,int *piParm4,int iParm5);
ulong FUN_00409e9f(uint param_1,undefined8 *param_2,char *param_3,long param_4,undefined8 param_5,uint param_6,uint *param_7,uint param_8);
ulong FUN_0040a668(uint param_1,undefined8 param_2,undefined8 param_3,undefined8 param_4,undefined8 param_5,uint param_6,uint param_7);
void FUN_0040a6f8(uint uParm1,undefined8 uParm2,undefined8 uParm3,undefined8 uParm4,undefined8 uParm5);
undefined8 FUN_0040a742(long lParm1);
ulong FUN_0040a773(uint *puParm1,byte *pbParm2,long lParm3,undefined8 uParm4);
undefined8 FUN_0040a7f6(long lParm1,long lParm2);
undefined8 FUN_0040a85f(int iParm1);
void FUN_0040a885(long lParm1,long lParm2);
void FUN_0040a8f1(long lParm1,long lParm2);
ulong FUN_0040a960(long lParm1,long lParm2);
void FUN_0040a9b7(undefined8 uParm1);
void FUN_0040a9db(undefined8 uParm1);
void FUN_0040a9ff(undefined8 uParm1,undefined8 uParm2);
void FUN_0040aa29(long lParm1);
void FUN_0040aa78(long lParm1,long lParm2);
void FUN_0040aae3(long lParm1,long lParm2);
ulong FUN_0040ab4e(long lParm1,long lParm2);
ulong FUN_0040abc1(long lParm1,long lParm2);
undefined8 FUN_0040ac0a(void);
ulong FUN_0040ac1d(long param_1,undefined8 param_2,long param_3,long param_4,undefined8 param_5,byte param_6,long param_7);
ulong FUN_0040ad68(long lParm1,undefined8 uParm2,long lParm3,long lParm4,byte bParm5,long lParm6);
undefined8 FUN_0040af3c(long lParm1,ulong uParm2);
void FUN_0040b0b0(undefined8 uParm1,undefined8 uParm2,undefined8 *puParm3,long lParm4,char cParm5,long lParm6);
void FUN_0040b1a0(long *plParm1);
undefined8 FUN_0040b4cb(long *plParm1);
long FUN_0040bfa7(long *plParm1,long lParm2,uint *puParm3);
void FUN_0040c0da(long *plParm1);
void FUN_0040c1ab(long *plParm1);
ulong FUN_0040c24b(byte **ppbParm1,byte *pbParm2,uint uParm3);
ulong FUN_0040cf31(long *plParm1,long lParm2);
ulong FUN_0040d0b3(long *plParm1);
void FUN_0040d23d(long lParm1);
ulong FUN_0040d28a(long lParm1,ulong uParm2,uint uParm3);
undefined8 FUN_0040d410(long *plParm1,long lParm2);
undefined8 FUN_0040d47d(undefined8 *puParm1,undefined8 uParm2);
undefined8 FUN_0040d507(undefined8 *puParm1,long lParm2,long lParm3);
undefined8 FUN_0040d5e5(long *plParm1,long lParm2);
undefined8 FUN_0040d6c5(long *plParm1,long lParm2,long lParm3);
undefined8 FUN_0040daa6(long *plParm1,long lParm2,long lParm3);
undefined8 FUN_0040ddb8(long *plParm1,long lParm2);
ulong FUN_0040e17e(long *plParm1,long lParm2);
undefined8 FUN_0040e369(long *plParm1,undefined8 uParm2);
undefined8 FUN_0040e41e(long lParm1,long lParm2);
long FUN_0040e4ad(long lParm1,long lParm2);
void FUN_0040e565(long lParm1,long lParm2);
long FUN_0040e5e3(long *plParm1,undefined8 uParm2,undefined8 uParm3);
long FUN_0040e97b(long lParm1,uint uParm2);
ulong * FUN_0040e9d5(undefined4 *puParm1,long lParm2,long lParm3);
ulong * FUN_0040eaf7(undefined4 *puParm1,long lParm2,long lParm3,uint uParm4);
undefined8 FUN_0040ec24(long *plParm1,ulong *puParm2,ulong uParm3);
void FUN_0040edd8(long lParm1);
long FUN_0040ee7b(long *plParm1,long lParm2,undefined8 uParm3);
long FUN_0040f050(long *plParm1,long lParm2,uint uParm3,undefined8 uParm4);
char * FUN_0040f33c(undefined8 uParm1,undefined8 uParm2,long lParm3);
undefined8 FUN_0040f3c8(long *plParm1);
void FUN_0040f4c1(long **pplParm1,long lParm2,long lParm3);
void FUN_0040fb9a(long *plParm1,undefined8 uParm2);
ulong FUN_0040fdfc(long *plParm1,undefined8 uParm2,undefined8 uParm3,ulong uParm4);
undefined8 FUN_00410176(long *plParm1,ulong uParm2);
void FUN_00410504(long lParm1);
void FUN_00410686(long *plParm1);
ulong FUN_00410715(long *plParm1);
void FUN_00410a64(long *plParm1);
ulong FUN_00410cbb(long *plParm1);
ulong FUN_00411051(long **pplParm1,code *pcParm2,undefined8 uParm3);
ulong FUN_0041112f(long *plParm1,code *pcParm2,undefined8 uParm3);
undefined8 FUN_004111ea(long lParm1,long lParm2);
ulong FUN_00411361(undefined8 uParm1,long lParm2);
long FUN_00411443(undefined4 *puParm1,long *plParm2,long lParm3);
undefined8 FUN_00411634(long *plParm1,long lParm2);
undefined8 FUN_00411726(undefined8 uParm1,long lParm2);
ulong FUN_004117d5(long lParm1,long lParm2);
ulong FUN_00411a4c(long *plParm1,long lParm2,long lParm3,long lParm4,uint uParm5);
long FUN_00411f7c(long *plParm1,long lParm2,uint uParm3);
long FUN_00412015(long *plParm1,long lParm2,undefined4 uParm3);
undefined8 FUN_0041214b(long lParm1);
ulong FUN_00412289(long lParm1);
ulong FUN_00412369(undefined8 *puParm1,long *plParm2,long lParm3,char cParm4);
void FUN_0041273b(undefined8 uParm1,long lParm2,undefined8 uParm3);
undefined8 FUN_00412780(long *plParm1,long lParm2,ulong uParm3);
ulong FUN_00412f78(char *pcParm1,long lParm2,ulong uParm3);
long FUN_0041318d(undefined8 uParm1,long *plParm2,ulong uParm3,int *piParm4);
long FUN_004132b9(undefined8 uParm1,long *plParm2,long lParm3,ulong uParm4,long lParm5,int *piParm6);
long FUN_004134b8(undefined8 uParm1,undefined8 *puParm2,long lParm3,undefined8 uParm4,long lParm5,int *piParm6);
long FUN_00413674(long lParm1,long *plParm2,undefined8 *puParm3,ulong uParm4,long lParm5,int *piParm6);
long FUN_00413efa(undefined8 uParm1,long *plParm2,long lParm3,ulong uParm4,undefined8 uParm5,int *piParm6);
long FUN_0041408e(long lParm1,long lParm2,undefined8 uParm3,undefined8 *puParm4,ulong uParm5,undefined4 *puParm6);
ulong FUN_004145b2(byte bParm1,long lParm2);
undefined8 FUN_004145e9(ulong uParm1,undefined8 uParm2,long lParm3,long *plParm4,int *piParm5,int *piParm6);
undefined8 FUN_004149aa(undefined8 uParm1,undefined8 uParm2,undefined8 uParm3,byte *pbParm4);
long FUN_00414a09(long lParm1,long lParm2,long lParm3,ulong uParm4,int *piParm5);
undefined8 FUN_0041533f(undefined4 *param_1,long param_2,undefined *param_3,int param_4,undefined8 param_5,undefined8 param_6,char param_7);
undefined8 FUN_0041548a(undefined4 *puParm1,long lParm2,char *pcParm3);
undefined8 FUN_004155f1(undefined8 uParm1,undefined8 uParm2,undefined8 uParm3,byte *pbParm4);
undefined8 FUN_0041564b(long lParm1,undefined8 uParm2,long lParm3,long *plParm4,char *pcParm5,ulong uParm6);
long FUN_00415fb2(long lParm1,undefined8 uParm2,undefined8 uParm3,char *pcParm4,byte bParm5,int *piParm6);
long FUN_0041624d(undefined8 uParm1,byte *pbParm2,undefined8 uParm3);
void FUN_00416334(undefined8 *puParm1);
void FUN_0041636d(undefined8 uParm1,undefined8 uParm2,undefined8 uParm3,undefined uParm4);
undefined8 * FUN_004163a4(long lParm1,undefined8 *puParm2,undefined8 *puParm3,undefined8 *puParm4);
undefined8 FUN_004164f0(long lParm1,long lParm2);
void FUN_00416533(undefined8 *puParm1);
undefined8 FUN_00416597(undefined8 uParm1,long lParm2);
long * FUN_004165be(long **pplParm1,undefined8 uParm2);
void FUN_004166ea(undefined8 uParm1,undefined8 uParm2,undefined8 uParm3,undefined8 uParm4,undefined8 uParm5);
void FUN_00416736(undefined8 uParm1,undefined8 uParm2,undefined8 uParm3,undefined8 uParm4,undefined8 uParm5,undefined8 uParm6);
ulong FUN_00416787(long param_1,undefined8 param_2,ulong param_3,ulong param_4,ulong param_5,undefined8 param_6,ulong *param_7,char param_8);
ulong FUN_00416b0f(ulong *puParm1,long lParm2,ulong uParm3,int iParm4);
ulong FUN_00416dbf(long *param_1,long param_2,long param_3,long param_4,long param_5,undefined8 param_6,ulong param_7,undefined8 *param_8,uint param_9);
ulong FUN_00417b3a(long lParm1);
long FUN_00417e5c(long lParm1,char cParm2,long lParm3);
undefined8 FUN_00418375(long *plParm1,long lParm2,uint uParm3);
undefined8 FUN_0041843c(long lParm1,long lParm2,undefined8 uParm3);
long FUN_004184df(long param_1,undefined8 param_2,long param_3,long *param_4,long param_5,long param_6,long param_7);
ulong FUN_00418a21(long *plParm1,undefined8 uParm2,undefined8 uParm3,long lParm4,undefined8 uParm5,undefined8 uParm6);
undefined8 FUN_00418bee(long *plParm1,undefined8 *puParm2,long lParm3,undefined8 uParm4,undefined8 *puParm5);
undefined8 FUN_00418d3f(long *plParm1,long lParm2,ulong uParm3,long *plParm4,char cParm5);
undefined8 FUN_00419151(long *plParm1);
void FUN_004191e7(long *plParm1,long lParm2,long lParm3,long lParm4,long lParm5,long lParm6);
ulong FUN_0041938b(long lParm1,long *plParm2);
undefined8 FUN_00419528(long lParm1,long *plParm2,long lParm3,undefined8 uParm4);
ulong FUN_00419732(long lParm1,long lParm2);
ulong FUN_0041981c(undefined8 uParm1,long lParm2,long lParm3,long lParm4);
ulong FUN_00419976(long lParm1,long *plParm2,long lParm3,long lParm4);
ulong FUN_00419b55(long lParm1,long lParm2,undefined8 uParm3);
ulong FUN_00419c84(long *plParm1,long lParm2,undefined8 uParm3,undefined8 uParm4);
undefined8 FUN_00419f12(long lParm1,long lParm2,undefined8 uParm3,undefined8 uParm4,undefined8 uParm5,undefined8 uParm6);
ulong FUN_0041a07e(long lParm1,uint uParm2,long lParm3,long lParm4,long lParm5);
undefined8 FUN_0041a2ef(long lParm1,long lParm2,undefined8 uParm3,undefined8 uParm4,long lParm5,undefined8 uParm6);
ulong FUN_0041a3bc(long *plParm1,long lParm2,undefined8 uParm3,long lParm4,long lParm5,long lParm6);
ulong FUN_0041a730(long lParm1,long *plParm2,long lParm3,long lParm4);
ulong FUN_0041abe0(long lParm1,long *plParm2,long lParm3,long lParm4,long lParm5);
undefined8 FUN_0041acba(int *piParm1,long lParm2,long lParm3);
long FUN_0041ae35(int *piParm1,long lParm2,long lParm3);
long FUN_0041b0c1(int *piParm1,long lParm2);
ulong FUN_0041b16b(long lParm1,long lParm2,undefined8 uParm3);
ulong FUN_0041b266(long lParm1,long lParm2);
ulong FUN_0041b5dc(long lParm1,long lParm2);
ulong FUN_0041bb31(long lParm1,long lParm2,long lParm3);
ulong FUN_0041c096(undefined8 uParm1,long *plParm2,undefined8 *puParm3,undefined8 uParm4,long lParm5);
long FUN_0041c16d(long *plParm1,long lParm2,long lParm3,uint uParm4);
ulong FUN_0041c1f9(long param_1,long *param_2,long param_3,long param_4,undefined8 param_5,long param_6,uint param_7);
ulong FUN_0041c987(long lParm1,long lParm2,long lParm3,undefined8 uParm4);
ulong FUN_0041cc4a(long lParm1,undefined8 *puParm2,undefined8 uParm3,uint uParm4);
ulong FUN_0041cdbe(long *plParm1,undefined8 uParm2,long lParm3,long lParm4,uint uParm5);
ulong FUN_0041cf7b(long lParm1,undefined8 uParm2,long lParm3,undefined8 uParm4,uint uParm5);
undefined8 FUN_0041d356(long lParm1,long lParm2);
long FUN_0041dd6f(long *plParm1,long lParm2,long lParm3,long lParm4);
ulong FUN_0041e609(long *plParm1,long lParm2,long lParm3,long lParm4);
undefined8 FUN_0041ea44(long lParm1,undefined8 *puParm2,long lParm3);
ulong FUN_0041ec15(long lParm1,int iParm2);
undefined8 FUN_0041ed9d(long lParm1,undefined4 uParm2,ulong uParm3);
void FUN_0041eede(long lParm1);
void FUN_0041efee(long lParm1);
undefined8 FUN_0041f02e(long lParm1,undefined8 uParm2,long lParm3,long lParm4,long lParm5);
long FUN_0041f34b(long lParm1,long lParm2);
undefined8 FUN_0041f421(long lParm1,undefined8 uParm2,undefined8 uParm3);
undefined8 * FUN_0041f599(long lParm1,undefined8 uParm2,undefined8 uParm3);
void FUN_0041f6a5(undefined8 *puParm1,undefined8 uParm2,undefined8 uParm3,undefined8 uParm4,undefined8 uParm5);
ulong FUN_0041f70c(undefined8 uParm1,undefined8 uParm2,undefined8 uParm3);
ulong FUN_0041f82d(byte *pbParm1,byte *pbParm2);
undefined8 FUN_0041f8a4(undefined8 uParm1);
undefined8 FUN_0041f92f(void);
ulong FUN_0041f93c(uint uParm1);
undefined1 * FUN_0041fa0d(void);
char * FUN_0041fdc0(void);
void FUN_0041fe8e(long *plParm1);
undefined8 FUN_00420082(char *pcParm1,long lParm2,char *pcParm3,char **ppcParm4);
undefined8 FUN_004202c1(long lParm1,long lParm2,long *plParm3);
char * FUN_0042095f(char *pcParm1,char *pcParm2);
void FUN_00421063(char *pcParm1);
undefined8 FUN_00421255(undefined8 uParm1,long lParm2,undefined8 uParm3,long *plParm4,ulong *puParm5);
long FUN_004215b3(undefined8 uParm1,undefined8 uParm2);
long FUN_0042168d(char *pcParm1,undefined8 uParm2,undefined8 uParm3);
long FUN_00421799(long lParm1,long lParm2);
ulong * FUN_004217e3(ulong *puParm1,char cParm2,ulong uParm3);
ulong FUN_004218f5(void);
long FUN_00421942(ulong uParm1,long lParm2,ulong uParm3,long lParm4,long *plParm5);
int * FUN_00421b8e(ulong uParm1,uint *puParm2,ulong uParm3,uint *puParm4,ulong *puParm5);
char * FUN_0042266e(ulong uParm1,long lParm2,long lParm3);
long FUN_004228dc(int *param_1,long *param_2);
long FUN_00422ac7(undefined auParm1 [16],int *piParm2,long *plParm3);
undefined8 FUN_00422e86(int iParm1,long lParm2,uint *puParm3,long lParm4,uint uParm5);
void FUN_00423699(uint param_1);
void FUN_004236e3(undefined8 uParm1,uint uParm2);
ulong FUN_00423735(void);
ulong FUN_00423ac7(undefined auParm1 [16],undefined8 uParm2,undefined8 uParm3);
ulong FUN_00423e9f(char *pcParm1,long lParm2);
undefined8 FUN_00423ef8(long param_1,long param_2,undefined param_3,int param_4,undefined8 param_5,ulong param_6,int param_7,ulong param_8,int param_9);
ulong FUN_0042b625(uint uParm1);
undefined8 * FUN_0042b644(ulong uParm1);
void FUN_0042b707(ulong uParm1);
void FUN_0042b7e0(long **pplParm1,long **pplParm2);
ulong FUN_0042b881(byte bParm1);
long FUN_0042b8b7(long lParm1);
void FUN_0042b960(undefined8 uParm1,int *piParm2,undefined8 uParm3);
void FUN_0042ba01(int *param_1);
ulong FUN_0042bac0(ulong uParm1,long lParm2);
void FUN_0042baf4(undefined8 uParm1,undefined8 uParm2,undefined8 uParm3);
void FUN_0042bb2f(undefined8 uParm1,undefined8 uParm2,undefined8 uParm3,undefined8 uParm4);
ulong FUN_0042bb80(ulong uParm1,ulong uParm2);
undefined8 FUN_0042bb9b(uint *puParm1,ulong *puParm2);
undefined8 FUN_0042c33b(char *pcParm1,ulong *puParm2,char **ppcParm3);
ulong FUN_0042d5f1(undefined auParm1 [16]);
ulong FUN_0042d62c(void);
void FUN_0042d660(void);
undefined8 _DT_FINI(void);
undefined FUN_00636000();
undefined FUN_00636008();
undefined FUN_00636010();
undefined FUN_00636018();
undefined FUN_00636020();
undefined FUN_00636028();
undefined FUN_00636030();
undefined FUN_00636038();
undefined FUN_00636040();
undefined FUN_00636048();
undefined FUN_00636050();
undefined FUN_00636058();
undefined FUN_00636060();
undefined FUN_00636068();
undefined FUN_00636070();
undefined FUN_00636078();
undefined FUN_00636080();
undefined FUN_00636088();
undefined FUN_00636090();
undefined FUN_00636098();
undefined FUN_006360a0();
undefined FUN_006360a8();
undefined FUN_006360b0();
undefined FUN_006360b8();
undefined FUN_006360c0();
undefined FUN_006360c8();
undefined FUN_006360d0();
undefined FUN_006360d8();
undefined FUN_006360e0();
undefined FUN_006360e8();
undefined FUN_006360f0();
undefined FUN_006360f8();
undefined FUN_00636100();
undefined FUN_00636108();
undefined FUN_00636110();
undefined FUN_00636118();
undefined FUN_00636120();
undefined FUN_00636128();
undefined FUN_00636130();
undefined FUN_00636138();
undefined FUN_00636140();
undefined FUN_00636148();
undefined FUN_00636150();
undefined FUN_00636158();
undefined FUN_00636160();
undefined FUN_00636168();
undefined FUN_00636170();
undefined FUN_00636178();
undefined FUN_00636180();
undefined FUN_00636188();
undefined FUN_00636190();
undefined FUN_00636198();
undefined FUN_006361a0();
undefined FUN_006361a8();
undefined FUN_006361b0();
undefined FUN_006361b8();
undefined FUN_006361c0();
undefined FUN_006361c8();
undefined FUN_006361d0();
undefined FUN_006361d8();
undefined FUN_006361e0();
undefined FUN_006361e8();
undefined FUN_006361f0();
undefined FUN_006361f8();
undefined FUN_00636200();
undefined FUN_00636208();
undefined FUN_00636210();
undefined FUN_00636218();
undefined FUN_00636220();
undefined FUN_00636228();
undefined FUN_00636230();
undefined FUN_00636238();
undefined FUN_00636240();
undefined FUN_00636248();
undefined FUN_00636250();
undefined FUN_00636258();
undefined FUN_00636260();
undefined FUN_00636268();
undefined FUN_00636270();
undefined FUN_00636278();
undefined FUN_00636280();
undefined FUN_00636288();
undefined FUN_00636290();
undefined FUN_00636298();
undefined FUN_006362a0();
undefined FUN_006362a8();
undefined FUN_006362b0();
undefined FUN_006362b8();
undefined FUN_006362c0();
undefined FUN_006362c8();
undefined FUN_006362d0();
undefined FUN_006362d8();
undefined FUN_006362e0();
undefined FUN_006362e8();
undefined FUN_006362f0();


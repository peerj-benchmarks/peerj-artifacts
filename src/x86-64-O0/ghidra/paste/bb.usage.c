
#include "paste.h"

long null_ARRAY_0061541_0_8_;
long null_ARRAY_0061541_8_8_;
long null_ARRAY_0061558_0_8_;
long null_ARRAY_0061558_16_8_;
long null_ARRAY_0061558_24_8_;
long null_ARRAY_0061558_32_8_;
long null_ARRAY_0061558_40_8_;
long null_ARRAY_0061558_48_8_;
long null_ARRAY_0061558_8_8_;
long null_ARRAY_006156c_0_4_;
long null_ARRAY_006156c_16_8_;
long null_ARRAY_006156c_4_4_;
long null_ARRAY_006156c_8_4_;
long DAT_004121c3;
long DAT_0041227d;
long DAT_004122fb;
long DAT_0041244c;
long DAT_0041244e;
long DAT_00412450;
long DAT_00412698;
long DAT_004126ec;
long DAT_00412730;
long DAT_0041285e;
long DAT_00412862;
long DAT_00412867;
long DAT_00412869;
long DAT_00412ed3;
long DAT_004132a0;
long DAT_004132b8;
long DAT_004132bd;
long DAT_00413327;
long DAT_004133b8;
long DAT_004133bb;
long DAT_00413409;
long DAT_0041340d;
long DAT_00413480;
long DAT_00413481;
long DAT_00615000;
long DAT_00615010;
long DAT_00615020;
long DAT_006153e0;
long DAT_006153f0;
long DAT_00615400;
long DAT_00615478;
long DAT_0061547c;
long DAT_00615480;
long DAT_006154c0;
long DAT_006154c8;
long DAT_006154d0;
long DAT_006154e0;
long DAT_006154e8;
long DAT_00615500;
long DAT_00615508;
long DAT_00615550;
long DAT_00615551;
long DAT_00615558;
long DAT_00615560;
long DAT_00615568;
long DAT_00615570;
long DAT_00615578;
long DAT_006156f8;
long DAT_00615700;
long DAT_00615708;
long DAT_00615718;
long fde_00414140;
long FLOAT_UNKNOWN;
long null_ARRAY_00412380;
long null_ARRAY_004138c0;
long null_ARRAY_00615410;
long null_ARRAY_00615520;
long null_ARRAY_00615580;
long null_ARRAY_006155c0;
long null_ARRAY_006156c0;
long PTR_DAT_006153e8;
long PTR_null_ARRAY_00615420;
ulong
FUN_00402759 (uint uParm1)
{
  char cVar1;
  int iVar2;
  long lVar3;
  uint *puVar4;
  code *pcVar5;
  char *pcVar6;
  undefined8 uVar7;
  int iStack60;
  char *pcStack56;

  if (uParm1 == 0)
    {
      func_0x004015a0 ("Usage: %s [OPTION]... [FILE]...\n", DAT_00615578);
      func_0x00401730
	("Write lines consisting of the sequentially corresponding lines from\neach FILE, separated by TABs, to standard output.\n",
	 DAT_006154c0);
      FUN_00401b6e ();
      FUN_00401b88 ();
      func_0x00401730
	("  -d, --delimiters=LIST   reuse characters from LIST instead of TABs\n  -s, --serial            paste one file at a time instead of in parallel\n",
	 DAT_006154c0);
      func_0x00401730
	("  -z, --zero-terminated    line delimiter is NUL, not newline\n",
	 DAT_006154c0);
      func_0x00401730 ("      --help     display this help and exit\n",
		       DAT_006154c0);
      pcVar6 = (char *) DAT_006154c0;
      func_0x00401730
	("      --version  output version information and exit\n");
      imperfection_wrapper ();	//    FUN_00401ba2();
    }
  else
    {
      pcVar6 = "Try \'%s --help\' for more information.\n";
      func_0x00401720 (DAT_006154e0,
		       "Try \'%s --help\' for more information.\n",
		       DAT_00615578);
    }
  func_0x004018e0 ();
  pcStack56 = "\t";
  FUN_00402bd8 (*(undefined8 *) pcVar6);
  func_0x00401880 (6, &DAT_004122fb);
  func_0x00401860 (FUN_00402a89);
  DAT_00615550 = '\0';
  DAT_00615551 = '\0';
LAB_0040293c:
  ;
  do
    {
      while (true)
	{
	  while (true)
	    {
	      uVar7 = 0x40295a;
	      imperfection_wrapper ();	//        iVar2 = FUN_00405da3((ulong)uParm1, pcVar6, &DAT_00412698, null_ARRAY_00412380, 0);
	      if (iVar2 == -1)
		{
		  iStack60 = uParm1 - DAT_00615478;
		  if (iStack60 == 0)
		    {
		      lVar3 = (long) DAT_00615478;
		      uVar7 = FUN_00401d3e (&DAT_0041244c);
		      ((undefined8 *) pcVar6)[lVar3] = uVar7;
		      iStack60 = 1;
		    }
		  iVar2 = FUN_00401d4c (pcStack56);
		  if (iVar2 != 0)
		    {
		      imperfection_wrapper ();	//            uVar7 = FUN_0040406e(0, 6, pcStack56);
		      imperfection_wrapper ();	//            FUN_004049c6(1, 0, "delimiter list ends with an unescaped backslash: %s", uVar7);
		    }
		  if (DAT_00615551 == '\0')
		    {
		      pcVar5 = FUN_00401f22;
		    }
		  else
		    {
		      pcVar5 = FUN_004024ea;
		    }
		  imperfection_wrapper ();	//          cVar1 = (*pcVar5)((long)iStack60, (undefined8 *)pcVar6 + (long)DAT_00615478, (undefined8 *)pcVar6 + (long)DAT_00615478, pcVar5);
		  func_0x004019d0 (DAT_00615558);
		  if ((DAT_00615550 != '\0')
		      && (iVar2 = FUN_00404aaa (DAT_006154c8), iVar2 == -1))
		    {
		      puVar4 = (uint *) func_0x004018d0 ();
		      imperfection_wrapper ();	//            FUN_004049c6(1, (ulong)*puVar4, &DAT_0041244c);
		    }
		  return (ulong) (cVar1 == '\0');
		}
	      if (iVar2 != 100)
		break;
	      pcStack56 = DAT_00615718;
	      if (1)
		{
		  pcStack56 = "\\0";
		}
	    }
	  if (100 < iVar2)
	    break;
	  if (iVar2 != -0x83)
	    {
	      if (iVar2 != -0x82)
		goto LAB_00402932;
	      uVar7 = 0x4028ea;
	      FUN_00402759 (0);
	    }
	  imperfection_wrapper ();	//      FUN_0040463f(DAT_006154c0, "paste", "GNU coreutils", PTR_DAT_006153e8, "David M. Ihnat", "David MacKenzie", 0, uVar7);
	  func_0x004018e0 (0);
	LAB_00402932:
	  ;
	  imperfection_wrapper ();	//      FUN_00402759();
	}
      if (iVar2 != 0x73)
	{
	  if (iVar2 != 0x7a)
	    goto LAB_00402932;
	  DAT_006153e0 = 0;
	  goto LAB_0040293c;
	}
      DAT_00615551 = '\x01';
    }
  while (true);
}

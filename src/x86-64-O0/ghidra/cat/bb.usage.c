
#include "cat.h"

long null_ARRAY_0061587_0_8_;
long null_ARRAY_0061587_8_8_;
long null_ARRAY_006159c_0_8_;
long null_ARRAY_006159c_16_8_;
long null_ARRAY_006159c_24_8_;
long null_ARRAY_006159c_32_8_;
long null_ARRAY_006159c_40_8_;
long null_ARRAY_006159c_48_8_;
long null_ARRAY_006159c_8_8_;
long null_ARRAY_00615b0_0_4_;
long null_ARRAY_00615b0_16_8_;
long null_ARRAY_00615b0_4_4_;
long null_ARRAY_00615b0_8_4_;
long DAT_00412638;
long DAT_004126f5;
long DAT_00412773;
long DAT_00412b49;
long DAT_00412b4d;
long DAT_00412bb3;
long DAT_00412dd8;
long DAT_00412e20;
long DAT_00412f5e;
long DAT_00412f62;
long DAT_00412f67;
long DAT_00412f69;
long DAT_004135d3;
long DAT_004139a0;
long DAT_004139b8;
long DAT_004139bd;
long DAT_00413a27;
long DAT_00413ab8;
long DAT_00413abb;
long DAT_00413b09;
long DAT_00413b0d;
long DAT_00413b80;
long DAT_00413b81;
long DAT_00413d68;
long DAT_00615428;
long DAT_00615438;
long DAT_00615448;
long DAT_00615838;
long DAT_00615840;
long DAT_00615848;
long DAT_00615858;
long DAT_00615860;
long DAT_006158d8;
long DAT_006158dc;
long DAT_006158e0;
long DAT_00615900;
long DAT_00615910;
long DAT_00615920;
long DAT_00615928;
long DAT_00615940;
long DAT_00615948;
long DAT_00615990;
long DAT_00615998;
long DAT_0061599c;
long DAT_006159a0;
long DAT_006159a8;
long DAT_006159b0;
long DAT_00615b38;
long DAT_00615b40;
long DAT_00615b48;
long DAT_00615b58;
long fde_00414868;
long FLOAT_UNKNOWN;
long null_ARRAY_00412c80;
long null_ARRAY_00413fc0;
long null_ARRAY_00615820;
long null_ARRAY_00615870;
long null_ARRAY_00615960;
long null_ARRAY_006159c0;
long null_ARRAY_00615a00;
long null_ARRAY_00615b00;
long PTR_DAT_00615850;
long PTR_null_ARRAY_00615880;
void
FUN_00401e4c (uint uParm1)
{
  char *pcVar1;
  char cVar2;
  char *pcStack40;

  if (uParm1 == 0)
    {
      func_0x00401610 ("Usage: %s [OPTION]... [FILE]...\n", DAT_006159b0);
      func_0x004017d0 ("Concatenate FILE(s) to standard output.\n",
		       DAT_00615900);
      FUN_00401c34 ();
      func_0x004017d0
	("\n  -A, --show-all           equivalent to -vET\n  -b, --number-nonblank    number nonempty output lines, overrides -n\n  -e                       equivalent to -vE\n  -E, --show-ends          display $ at end of each line\n  -n, --number             number all output lines\n  -s, --squeeze-blank      suppress repeated empty output lines\n",
	 DAT_00615900);
      func_0x004017d0
	("  -t                       equivalent to -vT\n  -T, --show-tabs          display TAB characters as ^I\n  -u                       (ignored)\n  -v, --show-nonprinting   use ^ and M- notation, except for LFD and TAB\n",
	 DAT_00615900);
      func_0x004017d0 ("      --help     display this help and exit\n",
		       DAT_00615900);
      func_0x004017d0
	("      --version  output version information and exit\n",
	 DAT_00615900);
      func_0x00401610
	("\nExamples:\n  %s f - g  Output f\'s contents, then standard input, then g\'s contents.\n  %s        Copy standard input to standard output.\n",
	 DAT_006159b0, DAT_006159b0);
      FUN_00401c4e (&DAT_00412b49);
    }
  else
    {
      func_0x004017c0 (DAT_00615920,
		       "Try \'%s --help\' for more information.\n",
		       DAT_006159b0);
    }
  func_0x00401960 ((ulong) uParm1);
  pcStack40 = DAT_00615848;
  do
    {
      cVar2 = *pcStack40;
      *pcStack40 = cVar2 + '\x01';
      if (cVar2 < '9')
	{
	  return;
	}
      pcVar1 = pcStack40 + -1;
      *pcStack40 = '0';
      pcStack40 = pcVar1;
    }
  while (DAT_00615840 <= pcVar1);
  if (DAT_00615840 < null_ARRAY_00615820 + 1)
    {
      imperfection_wrapper ();	//    null_ARRAY_00615820[0] = 0x3e;
    }
  else
    {
      DAT_00615840 = DAT_00615840 + -1;
      imperfection_wrapper ();	//    *DAT_00615840 = '1';
    }
  if (DAT_00615840 < DAT_00615838)
    {
      DAT_00615838 = DAT_00615838 + -1;
    }
  return;
}


#include "base32.h"

long null_ARRAY_00616a5_0_8_;
long null_ARRAY_00616a5_8_8_;
long null_ARRAY_00616bc_0_8_;
long null_ARRAY_00616bc_16_8_;
long null_ARRAY_00616bc_24_8_;
long null_ARRAY_00616bc_32_8_;
long null_ARRAY_00616bc_40_8_;
long null_ARRAY_00616bc_48_8_;
long null_ARRAY_00616bc_8_8_;
long null_ARRAY_00616d0_0_4_;
long null_ARRAY_00616d0_16_8_;
long null_ARRAY_00616d0_4_4_;
long null_ARRAY_00616d0_8_4_;
long DAT_00413343;
long DAT_004133fd;
long DAT_0041347b;
long DAT_0041390b;
long DAT_00413921;
long DAT_00413923;
long DAT_00413926;
long DAT_00413ab3;
long DAT_00413af8;
long DAT_00413c1e;
long DAT_00413c22;
long DAT_00413c27;
long DAT_00413c29;
long DAT_00414293;
long DAT_00414660;
long DAT_004149f8;
long DAT_004149fd;
long DAT_00414a67;
long DAT_00414af8;
long DAT_00414afb;
long DAT_00414b49;
long DAT_00414b4d;
long DAT_00414bc0;
long DAT_00414bc1;
long DAT_00414da8;
long DAT_00616620;
long DAT_00616630;
long DAT_00616640;
long DAT_00616a28;
long DAT_00616a40;
long DAT_00616ab8;
long DAT_00616abc;
long DAT_00616ac0;
long DAT_00616b00;
long DAT_00616b08;
long DAT_00616b10;
long DAT_00616b20;
long DAT_00616b28;
long DAT_00616b40;
long DAT_00616b48;
long DAT_00616b90;
long DAT_00616b98;
long DAT_00616ba0;
long DAT_00616d38;
long DAT_00616d40;
long DAT_00616d48;
long DAT_00616d58;
long fde_00415900;
long FLOAT_UNKNOWN;
long null_ARRAY_00413500;
long null_ARRAY_00413980;
long null_ARRAY_00415000;
long null_ARRAY_00616a50;
long null_ARRAY_00616a80;
long null_ARRAY_00616b60;
long null_ARRAY_00616bc0;
long null_ARRAY_00616c00;
long null_ARRAY_00616d00;
long PTR_DAT_00616a20;
long PTR_null_ARRAY_00616a60;
void
FUN_00401e6e (uint uParm1, undefined8 uParm2, undefined8 uParm3,
	      char **ppcParm4, undefined8 uParm5)
{
  int iVar1;
  uint *puVar2;
  char *pcVar3;
  char *pcVar4;
  long extraout_RDX;
  char *pcVar5;
  ulong uVar6;
  char *pcStack40;

  if (uParm1 == 0)
    {
      func_0x00401690
	("Usage: %s [OPTION]... [FILE]\nBase%d encode or decode FILE, or standard input, to standard output.\n",
	 DAT_00616ba0, 0x20);
      FUN_00401c9e ();
      FUN_00401cb8 ();
      func_0x00401830
	("  -d, --decode          decode data\n  -i, --ignore-garbage  when decoding, ignore non-alphabet characters\n  -w, --wrap=COLS       wrap encoded lines after COLS character (default 76).\n                          Use 0 to disable line wrapping\n\n",
	 DAT_00616b00);
      func_0x00401830 ("      --help     display this help and exit\n",
		       DAT_00616b00);
      func_0x00401830
	("      --version  output version information and exit\n",
	 DAT_00616b00);
      pcVar5 = "base32";
      func_0x00401690
	("\nThe data are encoded as described for the %s alphabet in RFC 4648.\nWhen decoding, the input may contain newlines in addition to the bytes of\nthe formal %s alphabet.  Use --ignore-garbage to attempt to recover\nfrom any other non-alphabet bytes in the encoded stream.\n",
	 "base32", "base32");
      imperfection_wrapper ();	//    FUN_00401cd2();
    }
  else
    {
      pcVar5 = "Try \'%s --help\' for more information.\n";
      func_0x00401820 (DAT_00616b20,
		       "Try \'%s --help\' for more information.\n",
		       DAT_00616ba0);
    }
  uVar6 = (ulong) uParm1;
  func_0x00401a00 ();
  if (extraout_RDX == 0)
    {
      pcVar3 = (char *) func_0x004019d0 (uVar6, 1, pcVar5, DAT_00616b00);
      if (pcVar5 <= pcVar3)
	{
	  return;
	}
      puVar2 = (uint *) func_0x004019f0 ();
      imperfection_wrapper ();	//    FUN_004059f2(1, (ulong)*puVar2, "write error");
    }
  pcStack40 = (char *) 0x0;
  while (pcStack40 < pcVar5)
    {
      pcVar3 = (char *) (extraout_RDX - (long) *ppcParm4);
      if (pcVar5 + -(long) pcStack40 <=
	  (char *) (extraout_RDX - (long) *ppcParm4))
	{
	  pcVar3 = pcVar5 + -(long) pcStack40;
	}
      if (pcVar3 == (char *) 0x0)
	{
	  iVar1 = func_0x00401aa0 (10, uParm5);
	  if (iVar1 == -1)
	    {
	      puVar2 = (uint *) func_0x004019f0 ();
	      imperfection_wrapper ();	//        FUN_004059f2(1, (ulong)*puVar2, "write error");
	    }
	  *ppcParm4 = (char *) 0x0;
	}
      else
	{
	  pcVar4 =
	    (char *) func_0x004019d0 (pcStack40 + uVar6, 1, pcVar3,
				      DAT_00616b00);
	  if (pcVar4 < pcVar3)
	    {
	      puVar2 = (uint *) func_0x004019f0 ();
	      imperfection_wrapper ();	//        FUN_004059f2(1, (ulong)*puVar2, "write error");
	    }
	  *ppcParm4 = pcVar3 + (long) *ppcParm4;
	  pcStack40 = pcStack40 + (long) pcVar3;
	}
    }
  return;
}

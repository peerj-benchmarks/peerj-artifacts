
#include "printenv.h"

long null_ARRAY_006143f_0_8_;
long null_ARRAY_006143f_8_8_;
long null_ARRAY_0061458_0_8_;
long null_ARRAY_0061458_16_8_;
long null_ARRAY_0061458_24_8_;
long null_ARRAY_0061458_32_8_;
long null_ARRAY_0061458_40_8_;
long null_ARRAY_0061458_48_8_;
long null_ARRAY_0061458_8_8_;
long null_ARRAY_006146c_0_4_;
long null_ARRAY_006146c_16_8_;
long null_ARRAY_006146c_4_4_;
long null_ARRAY_006146c_8_4_;
long DAT_00411380;
long DAT_0041143d;
long DAT_004114bb;
long DAT_00411826;
long DAT_00411843;
long DAT_00411888;
long DAT_004119de;
long DAT_004119e2;
long DAT_004119e7;
long DAT_004119e9;
long DAT_00412053;
long DAT_00412420;
long DAT_00412438;
long DAT_0041243d;
long DAT_004124a7;
long DAT_00412538;
long DAT_0041253b;
long DAT_00412589;
long DAT_0041258d;
long DAT_00412600;
long DAT_00412601;
long DAT_00614000;
long DAT_00614010;
long DAT_00614020;
long DAT_006143c8;
long DAT_006143e0;
long DAT_00614458;
long DAT_0061445c;
long DAT_00614460;
long DAT_00614480;
long DAT_006144c0;
long DAT_006144d0;
long DAT_006144e0;
long DAT_006144e8;
long DAT_00614500;
long DAT_00614508;
long DAT_00614550;
long DAT_00614558;
long DAT_00614560;
long DAT_006146f8;
long DAT_00614700;
long DAT_00614708;
long DAT_00614718;
long fde_00413250;
long FLOAT_UNKNOWN;
long null_ARRAY_00411540;
long null_ARRAY_00412a40;
long null_ARRAY_006143f0;
long null_ARRAY_00614520;
long null_ARRAY_00614580;
long null_ARRAY_006145c0;
long null_ARRAY_006146c0;
long PTR_DAT_006143c0;
long PTR_null_ARRAY_00614400;
ulong
FUN_00401ba2 (uint uParm1)
{
  char *pcVar1;
  char *pcVar2;
  int iVar3;
  long lVar4;
  char *pcVar5;
  bool bVar6;
  undefined8 uVar7;
  byte bStack69;
  int iStack68;
  int iStack60;
  char *pcStack56;
  char *pcStack48;
  char **ppcStack40;

  if (uParm1 == 0)
    {
      func_0x00401470
	("Usage: %s [OPTION]... [VARIABLE]...\nPrint the values of the specified environment VARIABLE(s).\nIf no VARIABLE is specified, print name and value pairs for them all.\n\n",
	 DAT_00614560);
      func_0x004015f0
	("  -0, --null     end each output line with NUL, not newline\n",
	 DAT_00614480);
      func_0x004015f0 ("      --help     display this help and exit\n",
		       DAT_00614480);
      func_0x004015f0
	("      --version  output version information and exit\n",
	 DAT_00614480);
      pcVar5 = "printenv";
      func_0x00401470
	("\nNOTE: your shell may have its own version of %s, which usually supersedes\nthe version described here.  Please refer to your shell\'s documentation\nfor details about the options it supports.\n");
      imperfection_wrapper ();	//    FUN_00401a06();
    }
  else
    {
      pcVar5 = "Try \'%s --help\' for more information.\n";
      func_0x004015e0 (DAT_006144e0,
		       "Try \'%s --help\' for more information.\n",
		       DAT_00614560);
    }
  func_0x00401760 ();
  bVar6 = false;
  FUN_00401fd5 (*(undefined8 *) pcVar5);
  func_0x00401720 (6, &DAT_004114bb);
  FUN_004019ee (2);
  func_0x00401700 (FUN_00401ef0);
LAB_00401d13:
  ;
  do
    {
      uVar7 = 0x401d31;
      imperfection_wrapper ();	//    iVar3 = FUN_00404de8((ulong)uParm1, pcVar5, "+iu:0", null_ARRAY_00411540, 0);
      if (iVar3 == -1)
	{
	  if (DAT_00614458 < (int) uParm1)
	    {
	      iStack68 = 0;
	      iStack60 = DAT_00614458;
	      while (iStack60 < (int) uParm1)
		{
		  bStack69 = 0;
		  lVar4 =
		    func_0x004017e0 (((undefined8 *) pcVar5)[(long) iStack60],
				     0x3d);
		  if (lVar4 == 0)
		    {
		      ppcStack40 = DAT_006144c0;
		      while (*ppcStack40 != (char *) 0x0)
			{
			  pcStack56 =
			    (char *) ((undefined8 *) pcVar5)[(long) iStack60];
			  pcVar1 = *ppcStack40;
			  do
			    {
			      pcStack48 = pcVar1;
			      if ((*pcStack48 == '\0')
				  || (*pcStack56 == '\0'))
				goto LAB_00401e9a;
			      pcVar1 = pcStack48 + 1;
			      pcVar2 = pcStack56 + 1;
			      if (*pcStack48 != *pcStack56)
				goto LAB_00401e9a;
			      pcStack56 = pcVar2;
			    }
			  while ((*pcVar1 != '=')
				 || (pcStack56 = pcVar2, *pcVar2 != '\0'));
			  if (bVar6)
			    {
			      uVar7 = 0;
			    }
			  else
			    {
			      uVar7 = 10;
			    }
			  func_0x00401470 (&DAT_00411826, pcStack48 + 2,
					   uVar7);
			  bStack69 = 1;
			LAB_00401e9a:
			  ;
			  ppcStack40 = ppcStack40 + 1;
			}
		      iStack68 = iStack68 + (uint) bStack69;
		    }
		  iStack60 = iStack60 + 1;
		}
	      bVar6 = uParm1 - DAT_00614458 == iStack68;
	    }
	  else
	    {
	      ppcStack40 = DAT_006144c0;
	      while (*ppcStack40 != (char *) 0x0)
		{
		  if (bVar6)
		    {
		      uVar7 = 0;
		    }
		  else
		    {
		      uVar7 = 10;
		    }
		  func_0x00401470 (&DAT_00411826, *ppcStack40, uVar7);
		  ppcStack40 = ppcStack40 + 1;
		}
	      bVar6 = true;
	    }
	  return (ulong) ! bVar6;
	}
      if (iVar3 == -0x82)
	{
	  uVar7 = 0x401cc1;
	  FUN_00401ba2 (0);
	LAB_00401cc1:
	  ;
	  imperfection_wrapper ();	//      FUN_0040392a(DAT_00614480, "printenv", "GNU coreutils", PTR_DAT_006143c0, "David MacKenzie", "Richard Mlynarik", 0, uVar7);
	  func_0x00401760 (0);
	LAB_00401d09:
	  ;
	  imperfection_wrapper ();	//      FUN_00401ba2();
	  goto LAB_00401d13;
	}
      if (iVar3 != 0x30)
	{
	  if (iVar3 == -0x83)
	    goto LAB_00401cc1;
	  goto LAB_00401d09;
	}
      bVar6 = true;
    }
  while (true);
}

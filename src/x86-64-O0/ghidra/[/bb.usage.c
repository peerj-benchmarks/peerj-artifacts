
#include "[.h"

long null_ARRAY_0061741_0_8_;
long null_ARRAY_0061741_8_8_;
long null_ARRAY_0061754_0_8_;
long null_ARRAY_0061754_16_8_;
long null_ARRAY_0061754_24_8_;
long null_ARRAY_0061754_32_8_;
long null_ARRAY_0061754_40_8_;
long null_ARRAY_0061754_48_8_;
long null_ARRAY_0061754_8_8_;
long DAT_00412fc0;
long DAT_0041307d;
long DAT_004130fb;
long DAT_00413160;
long DAT_00413162;
long DAT_00413165;
long DAT_00413168;
long DAT_0041316c;
long DAT_00413170;
long DAT_00413174;
long DAT_00413178;
long DAT_0041317c;
long DAT_00413180;
long DAT_00413184;
long DAT_00413188;
long DAT_0041318c;
long DAT_004131b0;
long DAT_004133d0;
long DAT_004133d3;
long DAT_00413578;
long DAT_0041357a;
long DAT_00414272;
long DAT_004142a9;
long DAT_004142f0;
long DAT_0041441e;
long DAT_00414422;
long DAT_00414427;
long DAT_00414429;
long DAT_00414a80;
long DAT_00414abb;
long DAT_00414e80;
long DAT_00414e98;
long DAT_00414e9d;
long DAT_00414ea2;
long DAT_00414eb0;
long DAT_00414f20;
long DAT_00414f21;
long DAT_00415108;
long DAT_00617000;
long DAT_00617010;
long DAT_00617020;
long DAT_006173e8;
long DAT_00617400;
long DAT_00617480;
long DAT_00617490;
long DAT_006174a0;
long DAT_006174a8;
long DAT_006174c0;
long DAT_006174c8;
long DAT_00617510;
long DAT_00617514;
long DAT_00617518;
long DAT_00617520;
long DAT_00617528;
long DAT_00617530;
long DAT_00617680;
long DAT_00617688;
long DAT_00617690;
long DAT_00617698;
long DAT_006176a0;
long DAT_006176a8;
long fde_00415c78;
long FLOAT_UNKNOWN;
long null_ARRAY_00415360;
long null_ARRAY_00617410;
long null_ARRAY_00617440;
long null_ARRAY_006174e0;
long null_ARRAY_00617540;
long null_ARRAY_00617580;
long PTR_DAT_006173e0;
long PTR_null_ARRAY_00617420;
undefined8
FUN_00403aa7 (int iParm1)
{
  char cVar1;
  int iVar2;
  char *pcVar3;
  undefined8 uVar4;

  if (iParm1 == 0)
    {
      func_0x00401700
	("Usage: test EXPRESSION\n  or:  test\n  or:  [ EXPRESSION ]\n  or:  [ ]\n  or:  [ OPTION\n",
	 DAT_00617480);
      func_0x00401700 ("Exit with the status determined by EXPRESSION.\n\n",
		       DAT_00617480);
      func_0x00401700 ("      --help     display this help and exit\n",
		       DAT_00617480);
      func_0x00401700
	("      --version  output version information and exit\n",
	 DAT_00617480);
      func_0x00401700
	("\nAn omitted EXPRESSION defaults to false.  Otherwise,\nEXPRESSION is true or false and sets exit status.  It is one of:\n",
	 DAT_00617480);
      func_0x00401700
	("\n  ( EXPRESSION )               EXPRESSION is true\n  ! EXPRESSION                 EXPRESSION is false\n  EXPRESSION1 -a EXPRESSION2   both EXPRESSION1 and EXPRESSION2 are true\n  EXPRESSION1 -o EXPRESSION2   either EXPRESSION1 or EXPRESSION2 is true\n",
	 DAT_00617480);
      func_0x00401700
	("\n  -n STRING            the length of STRING is nonzero\n  STRING               equivalent to -n STRING\n  -z STRING            the length of STRING is zero\n  STRING1 = STRING2    the strings are equal\n  STRING1 != STRING2   the strings are not equal\n",
	 DAT_00617480);
      func_0x00401700
	("\n  INTEGER1 -eq INTEGER2   INTEGER1 is equal to INTEGER2\n  INTEGER1 -ge INTEGER2   INTEGER1 is greater than or equal to INTEGER2\n  INTEGER1 -gt INTEGER2   INTEGER1 is greater than INTEGER2\n  INTEGER1 -le INTEGER2   INTEGER1 is less than or equal to INTEGER2\n  INTEGER1 -lt INTEGER2   INTEGER1 is less than INTEGER2\n  INTEGER1 -ne INTEGER2   INTEGER1 is not equal to INTEGER2\n",
	 DAT_00617480);
      func_0x00401700
	("\n  FILE1 -ef FILE2   FILE1 and FILE2 have the same device and inode numbers\n  FILE1 -nt FILE2   FILE1 is newer (modification date) than FILE2\n  FILE1 -ot FILE2   FILE1 is older than FILE2\n",
	 DAT_00617480);
      func_0x00401700
	("\n  -b FILE     FILE exists and is block special\n  -c FILE     FILE exists and is character special\n  -d FILE     FILE exists and is a directory\n  -e FILE     FILE exists\n",
	 DAT_00617480);
      func_0x00401700
	("  -f FILE     FILE exists and is a regular file\n  -g FILE     FILE exists and is set-group-ID\n  -G FILE     FILE exists and is owned by the effective group ID\n  -h FILE     FILE exists and is a symbolic link (same as -L)\n  -k FILE     FILE exists and has its sticky bit set\n",
	 DAT_00617480);
      func_0x00401700
	("  -L FILE     FILE exists and is a symbolic link (same as -h)\n  -O FILE     FILE exists and is owned by the effective user ID\n  -p FILE     FILE exists and is a named pipe\n  -r FILE     FILE exists and read permission is granted\n  -s FILE     FILE exists and has a size greater than zero\n",
	 DAT_00617480);
      func_0x00401700
	("  -S FILE     FILE exists and is a socket\n  -t FD       file descriptor FD is opened on a terminal\n  -u FILE     FILE exists and its set-user-ID bit is set\n  -w FILE     FILE exists and write permission is granted\n  -x FILE     FILE exists and execute (or search) permission is granted\n",
	 DAT_00617480);
      func_0x00401700
	("\nExcept for -h and -L, all FILE-related tests dereference symbolic links.\nBeware that parentheses need to be escaped (e.g., by backslashes) for shells.\nINTEGER may also be -l STRING, which evaluates to the length of STRING.\n",
	 DAT_00617480);
      func_0x00401700
	("\nNOTE: Binary -a and -o are inherently ambiguous.  Use \'test EXPR1 && test\nEXPR2\' or \'test EXPR1 || test EXPR2\' instead.\n",
	 DAT_00617480);
      func_0x00401700
	("\nNOTE: [ honors the --help and --version options, but test does not.\ntest treats each of those as it treats any other nonempty STRING.\n",
	 DAT_00617480);
      pcVar3 = "test and/or [";
      func_0x00401530
	("\nNOTE: your shell may have its own version of %s, which usually supersedes\nthe version described here.  Please refer to your shell\'s documentation\nfor details about the options it supports.\n");
      imperfection_wrapper ();	//    FUN_00401b25();
    }
  else
    {
      pcVar3 = "Try \'%s --help\' for more information.\n";
      func_0x004016f0 (DAT_006174a0,
		       "Try \'%s --help\' for more information.\n",
		       DAT_00617530);
    }
  func_0x00401880 ();
  FUN_00403f73 (*(undefined8 *) pcVar3);
  func_0x00401830 (6, &DAT_004130fb);
  FUN_00401afe (2);
  func_0x00401820 (FUN_00403e09);
  DAT_00617518 = (undefined8 *) pcVar3;
  if (iParm1 == 2)
    {
      DAT_00617518 = (undefined8 *) pcVar3;
      iVar2 = func_0x004017d0 (((undefined8 *) pcVar3)[1], "--help");
      if (iVar2 == 0)
	{
	  FUN_00403aa7 (0);
	}
      uVar4 = 0x403cd9;
      iVar2 = func_0x004017d0 (((undefined8 *) pcVar3)[1], "--version");
      if (iVar2 == 0)
	{
	  imperfection_wrapper ();	//      FUN_004060df(DAT_00617480, &DAT_00412fc0, "GNU coreutils", PTR_DAT_006173e0, "Kevin Braunsdorf", "Matthew Bradburn", 0, uVar4);
	  return 0;
	}
    }
  if ((iParm1 < 2)
      || (iVar2 =
	  func_0x004017d0 (((undefined8 *) pcVar3)[(long) iParm1 + -1],
			   &DAT_00414272), iVar2 != 0))
    {
      imperfection_wrapper ();	//    uVar4 = FUN_004053e7(&DAT_00414272);
      imperfection_wrapper ();	//    FUN_00401cc1("missing %s", uVar4);
    }
  DAT_00617514 = iParm1 + -1;
  DAT_00617510 = 1;
  if (DAT_00617514 < 2)
    {
      uVar4 = 1;
    }
  else
    {
      cVar1 = FUN_0040396a ((ulong) (iParm1 - 2));
      if (DAT_00617510 != DAT_00617514)
	{
	  imperfection_wrapper ();	//      uVar4 = FUN_004053e7(DAT_00617518[(long)DAT_00617510]);
	  imperfection_wrapper ();	//      FUN_00401cc1("extra argument %s", uVar4);
	}
      if (cVar1 == '\0')
	{
	  uVar4 = 1;
	}
      else
	{
	  uVar4 = 0;
	}
    }
  return uVar4;
}

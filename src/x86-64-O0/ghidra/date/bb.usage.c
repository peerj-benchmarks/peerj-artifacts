
#include "date.h"

long null_ARRAY_0062841_0_8_;
long null_ARRAY_0062841_8_8_;
long null_ARRAY_006285c_0_8_;
long null_ARRAY_006285c_16_8_;
long null_ARRAY_006285c_24_8_;
long null_ARRAY_006285c_32_8_;
long null_ARRAY_006285c_40_8_;
long null_ARRAY_006285c_48_8_;
long null_ARRAY_006285c_8_8_;
long null_ARRAY_0062870_0_4_;
long null_ARRAY_0062870_16_8_;
long null_ARRAY_0062870_4_4_;
long null_ARRAY_0062870_8_4_;
long DAT_004207cb;
long DAT_00420885;
long DAT_00420903;
long DAT_00420949;
long DAT_00421dd6;
long DAT_00421de7;
long DAT_00421de9;
long DAT_00421f99;
long DAT_00421fac;
long DAT_0042217a;
long DAT_00422192;
long DAT_004225c7;
long DAT_004225ca;
long DAT_004225ce;
long DAT_00422629;
long DAT_0042264f;
long DAT_00422654;
long DAT_00422b00;
long DAT_00423050;
long DAT_00423d35;
long DAT_00423d38;
long DAT_00423d79;
long DAT_00423e4b;
long DAT_00423e50;
long DAT_00423f19;
long DAT_0042406d;
long DAT_00424680;
long DAT_0042473c;
long DAT_00424780;
long DAT_004248de;
long DAT_004248e2;
long DAT_004248e7;
long DAT_004248e9;
long DAT_00424f53;
long DAT_00425320;
long DAT_00425338;
long DAT_0042533d;
long DAT_004253a7;
long DAT_00425438;
long DAT_0042543b;
long DAT_00425489;
long DAT_00425501;
long DAT_00425b30;
long DAT_00425ba0;
long DAT_00425ba1;
long DAT_00627fa0;
long DAT_00627fb0;
long DAT_00627fc0;
long DAT_006283f0;
long DAT_00628400;
long DAT_00628478;
long DAT_0062847c;
long DAT_00628480;
long DAT_006284c0;
long DAT_00628500;
long DAT_00628508;
long DAT_00628510;
long DAT_00628520;
long DAT_00628528;
long DAT_00628540;
long DAT_00628548;
long DAT_00628590;
long DAT_00628598;
long DAT_006285a0;
long DAT_006285a8;
long DAT_00628778;
long DAT_00628780;
long DAT_00628788;
long DAT_00628790;
long DAT_00628fc8;
long DAT_00628fd0;
long DAT_00628fe0;
long fde_00426918;
long FLOAT_UNKNOWN;
long null_ARRAY_00420960;
long null_ARRAY_00420990;
long null_ARRAY_004209c9;
long null_ARRAY_00420a40;
long null_ARRAY_00422000;
long null_ARRAY_00422080;
long null_ARRAY_00422700;
long null_ARRAY_00422840;
long null_ARRAY_004228c0;
long null_ARRAY_00422940;
long null_ARRAY_00422960;
long null_ARRAY_00422980;
long null_ARRAY_00422a00;
long null_ARRAY_00422a80;
long null_ARRAY_00422b80;
long null_ARRAY_00423000;
long null_ARRAY_00423060;
long null_ARRAY_00423140;
long null_ARRAY_00423340;
long null_ARRAY_00423480;
long null_ARRAY_00423600;
long null_ARRAY_00423740;
long null_ARRAY_00423a80;
long null_ARRAY_00424720;
long null_ARRAY_004254c0;
long null_ARRAY_00425940;
long null_ARRAY_00628410;
long null_ARRAY_00628440;
long null_ARRAY_00628560;
long null_ARRAY_006285c0;
long null_ARRAY_00628600;
long null_ARRAY_00628700;
long null_ARRAY_00628740;
long null_ARRAY_006287c0;
long PTR_DAT_006283e0;
long PTR_FUN_006283e8;
long PTR_null_ARRAY_00628420;
long PTR_null_ARRAY_00628488;
long register0x00000020;
long stack0x00000008;
ulong
FUN_004021b7 (uint uParm1, undefined8 uParm2, undefined8 uParm3,
	      undefined8 uParm4)
{
  char cVar1;
  byte bVar2;
  int iVar3;
  uint *puVar4;
  undefined8 uVar5;
  undefined8 extraout_RDX;
  char *pcVar6;
  char *pcStack120;
  undefined8 uStack112;
  undefined8 uStack104;
  undefined8 uStack88;
  long lStack80;
  long lStack72;
  long lStack64;
  bool bStack49;
  code *pcStack32;
  uint local_c;

  if (uParm1 == 0)
    {
      pcStack32 = (code *) 0x40220d;
      local_c = uParm1;
      func_0x00401950
	("Usage: %s [OPTION]... [+FORMAT]\n  or:  %s [-u|--utc|--universal] [MMDDhhmm[[CC]YY][.ss]]\n",
	 DAT_006285a8, DAT_006285a8);
      pcStack32 = (code *) 0x402221;
      func_0x00401b10
	("Display the current time in the given FORMAT, or set the system date.\n",
	 DAT_006284c0);
      pcStack32 = (code *) 0x402226;
      FUN_00401fce ();
      pcStack32 = (code *) 0x40223a;
      func_0x00401b10
	("  -d, --date=STRING          display time described by STRING, not \'now\'\n",
	 DAT_006284c0);
      pcStack32 = (code *) 0x40224e;
      func_0x00401b10
	("      --debug                annotate the parsed date,\n                              and warn about questionable usage to stderr\n",
	 DAT_006284c0);
      pcStack32 = (code *) 0x402262;
      func_0x00401b10
	("  -f, --file=DATEFILE        like --date; once for each line of DATEFILE\n",
	 DAT_006284c0);
      pcStack32 = (code *) 0x402276;
      func_0x00401b10
	("  -I[FMT], --iso-8601[=FMT]  output date/time in ISO 8601 format.\n                               FMT=\'date\' for date only (the default),\n                               \'hours\', \'minutes\', \'seconds\', or \'ns\'\n                               for date and time to the indicated precision.\n                               Example: 2006-08-14T02:34:56-06:00\n",
	 DAT_006284c0);
      pcStack32 = (code *) 0x40228a;
      func_0x00401b10
	("  -R, --rfc-email            output date and time in RFC 5322 format.\n                               Example: Mon, 14 Aug 2006 02:34:56 -0600\n",
	 DAT_006284c0);
      pcStack32 = (code *) 0x40229e;
      func_0x00401b10
	("      --rfc-3339=FMT         output date/time in RFC 3339 format.\n                               FMT=\'date\', \'seconds\', or \'ns\'\n                               for date and time to the indicated precision.\n                               Example: 2006-08-14 02:34:56-06:00\n",
	 DAT_006284c0);
      pcStack32 = (code *) 0x4022b2;
      func_0x00401b10
	("  -r, --reference=FILE       display the last modification time of FILE\n",
	 DAT_006284c0);
      pcStack32 = (code *) 0x4022c6;
      func_0x00401b10
	("  -s, --set=STRING           set time described by STRING\n  -u, --utc, --universal     print or set Coordinated Universal Time (UTC)\n",
	 DAT_006284c0);
      pcStack32 = (code *) 0x4022da;
      func_0x00401b10 ("      --help     display this help and exit\n",
		       DAT_006284c0);
      pcStack32 = (code *) 0x4022ee;
      func_0x00401b10
	("      --version  output version information and exit\n",
	 DAT_006284c0);
      pcStack32 = (code *) 0x402302;
      func_0x00401b10
	("\nFORMAT controls the output.  Interpreted sequences are:\n\n  %%   a literal %\n  %a   locale\'s abbreviated weekday name (e.g., Sun)\n",
	 DAT_006284c0);
      pcStack32 = (code *) 0x402316;
      func_0x00401b10
	("  %A   locale\'s full weekday name (e.g., Sunday)\n  %b   locale\'s abbreviated month name (e.g., Jan)\n  %B   locale\'s full month name (e.g., January)\n  %c   locale\'s date and time (e.g., Thu Mar  3 23:05:25 2005)\n",
	 DAT_006284c0);
      pcStack32 = (code *) 0x40232a;
      func_0x00401b10
	("  %C   century; like %Y, except omit last two digits (e.g., 20)\n  %d   day of month (e.g., 01)\n  %D   date; same as %m/%d/%y\n  %e   day of month, space padded; same as %_d\n",
	 DAT_006284c0);
      pcStack32 = (code *) 0x40233e;
      func_0x00401b10
	("  %F   full date; same as %Y-%m-%d\n  %g   last two digits of year of ISO week number (see %G)\n  %G   year of ISO week number (see %V); normally useful only with %V\n",
	 DAT_006284c0);
      pcStack32 = (code *) 0x402352;
      func_0x00401b10
	("  %h   same as %b\n  %H   hour (00..23)\n  %I   hour (01..12)\n  %j   day of year (001..366)\n",
	 DAT_006284c0);
      pcStack32 = (code *) 0x402366;
      func_0x00401b10
	("  %k   hour, space padded ( 0..23); same as %_H\n  %l   hour, space padded ( 1..12); same as %_I\n  %m   month (01..12)\n  %M   minute (00..59)\n",
	 DAT_006284c0);
      pcStack32 = (code *) 0x40237a;
      func_0x00401b10
	("  %n   a newline\n  %N   nanoseconds (000000000..999999999)\n  %p   locale\'s equivalent of either AM or PM; blank if not known\n  %P   like %p, but lower case\n  %q   quarter of year (1..4)\n  %r   locale\'s 12-hour clock time (e.g., 11:11:04 PM)\n  %R   24-hour hour and minute; same as %H:%M\n  %s   seconds since 1970-01-01 00:00:00 UTC\n",
	 DAT_006284c0);
      pcStack32 = (code *) 0x40238e;
      func_0x00401b10
	("  %S   second (00..60)\n  %t   a tab\n  %T   time; same as %H:%M:%S\n  %u   day of week (1..7); 1 is Monday\n",
	 DAT_006284c0);
      pcStack32 = (code *) 0x4023a2;
      func_0x00401b10
	("  %U   week number of year, with Sunday as first day of week (00..53)\n  %V   ISO week number, with Monday as first day of week (01..53)\n  %w   day of week (0..6); 0 is Sunday\n  %W   week number of year, with Monday as first day of week (00..53)\n",
	 DAT_006284c0);
      pcStack32 = (code *) 0x4023b6;
      func_0x00401b10
	("  %x   locale\'s date representation (e.g., 12/31/99)\n  %X   locale\'s time representation (e.g., 23:13:48)\n  %y   last two digits of year (00..99)\n  %Y   year\n",
	 DAT_006284c0);
      pcStack32 = (code *) 0x4023ca;
      func_0x00401b10
	("  %z   +hhmm numeric time zone (e.g., -0400)\n  %:z  +hh:mm numeric time zone (e.g., -04:00)\n  %::z  +hh:mm:ss numeric time zone (e.g., -04:00:00)\n  %:::z  numeric time zone with : to necessary precision (e.g., -04, +05:30)\n  %Z   alphabetic time zone abbreviation (e.g., EDT)\n\nBy default, date pads numeric fields with zeroes.\n",
	 DAT_006284c0);
      pcStack32 = (code *) 0x4023de;
      func_0x00401b10
	("The following optional flags may follow \'%\':\n\n  -  (hyphen) do not pad the field\n  _  (underscore) pad with spaces\n  0  (zero) pad with zeros\n  ^  use upper case if possible\n  #  use opposite case if possible\n",
	 DAT_006284c0);
      pcStack32 = (code *) 0x4023f2;
      func_0x00401b10
	("\nAfter any flags comes an optional field width, as a decimal number;\nthen an optional modifier, which is either\nE to use the locale\'s alternate representations if available, or\nO to use the locale\'s alternate numeric symbols if available.\n",
	 DAT_006284c0);
      pcStack32 = (code *) 0x402406;
      pcVar6 = DAT_006284c0;
      func_0x00401b10
	("\nExamples:\nConvert seconds since the epoch (1970-01-01 UTC) to a date\n  $ date --date=\'@2147483647\'\n\nShow the time on the west coast of the US (use tzselect(1) to find TZ)\n  $ TZ=\'America/Los_Angeles\' date\n\nShow the local time for 9AM next Friday on the west coast of the US\n  $ date --date=\'TZ=\"America/Los_Angeles\" 09:00 next Fri\'\n");
      pcStack32 = (code *) 0x402410;
      imperfection_wrapper ();	//    FUN_00401fe8();
    }
  else
    {
      pcVar6 = "Try \'%s --help\' for more information.\n";
      pcStack32 = (code *) 0x4021e8;
      local_c = uParm1;
      func_0x00401b00 (DAT_00628520,
		       "Try \'%s --help\' for more information.\n",
		       DAT_006285a8);
    }
  pcStack120 = (char *) (ulong) local_c;
  pcStack32 = FUN_0040241a;
  func_0x00401d00 ();
  pcStack32 = (code *) & stack0xfffffffffffffff8;
  iVar3 = func_0x00401c20 (pcStack120, &DAT_00421dd6);
  if (iVar3 == 0)
    {
      pcStack120 = "standard input";
      lStack64 = DAT_00628508;
    }
  else
    {
      lStack64 = func_0x00401be0 (pcStack120, &DAT_00421de7);
      if (lStack64 == 0)
	{
	  imperfection_wrapper ();	//      uVar5 = FUN_0040e150(0, 3, pcStack120);
	  puVar4 = (uint *) func_0x00401cf0 ();
	  imperfection_wrapper ();	//      FUN_0040eb22(1, (ulong)*puVar4, &DAT_00421de9, uVar5);
	}
    }
  lStack80 = 0;
  uStack88 = 0;
  bStack49 = true;
  while (1)
    {
      cVar1 =
	FUN_0040a52e (&uStack112, lStack80, 0, (ulong) DAT_00628590,
		      extraout_RDX, uParm4);
      if (cVar1 == '\x01')
	{
	  bVar2 = FUN_00402d20 (pcVar6, uStack112, uStack104, extraout_RDX);
	  bStack49 = (bVar2 & bStack49) != 0;
	}
      else
	{
	  if (*(char *) (lStack80 + lStack72 + -1) == '\n')
	    {
	      *(undefined *) (lStack80 + lStack72 + -1) = 0;
	    }
	  imperfection_wrapper ();	//      uVar5 = FUN_0040e240(lStack80);
	  imperfection_wrapper ();	//      FUN_0040eb22(0, 0, "invalid date %s", uVar5);
	  bStack49 = false;
	}
    }
  iVar3 = FUN_0040ec06 (lStack64);
  if (iVar3 == -1)
    {
      imperfection_wrapper ();	//    uVar5 = FUN_0040e150(0, 3, pcStack120);
      puVar4 = (uint *) func_0x00401cf0 ();
      imperfection_wrapper ();	//    FUN_0040eb22(1, (ulong)*puVar4, &DAT_00421de9, uVar5);
    }
  func_0x00401e30 (lStack80);
  return (ulong) bStack49;
}

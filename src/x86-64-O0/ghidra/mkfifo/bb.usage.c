
#include "mkfifo.h"

long null_ARRAY_0061541_0_8_;
long null_ARRAY_0061541_8_8_;
long null_ARRAY_0061558_0_8_;
long null_ARRAY_0061558_16_8_;
long null_ARRAY_0061558_24_8_;
long null_ARRAY_0061558_32_8_;
long null_ARRAY_0061558_40_8_;
long null_ARRAY_0061558_48_8_;
long null_ARRAY_0061558_8_8_;
long null_ARRAY_006156c_0_4_;
long null_ARRAY_006156c_16_8_;
long null_ARRAY_006156c_4_4_;
long null_ARRAY_006156c_8_4_;
long DAT_00411f8b;
long DAT_00412045;
long DAT_004120c3;
long DAT_00412449;
long DAT_00412517;
long DAT_00412668;
long DAT_0041279e;
long DAT_004127a2;
long DAT_004127a7;
long DAT_004127a9;
long DAT_00412e13;
long DAT_004131e0;
long DAT_004131f8;
long DAT_004131fd;
long DAT_00413267;
long DAT_004132f8;
long DAT_004132fb;
long DAT_00413349;
long DAT_0041334d;
long DAT_004133c0;
long DAT_004133c1;
long DAT_00615000;
long DAT_00615010;
long DAT_00615020;
long DAT_006153e8;
long DAT_00615400;
long DAT_00615478;
long DAT_0061547c;
long DAT_00615480;
long DAT_006154c0;
long DAT_006154d0;
long DAT_006154e0;
long DAT_006154e8;
long DAT_00615500;
long DAT_00615508;
long DAT_00615550;
long DAT_00615558;
long DAT_00615560;
long DAT_006156f8;
long DAT_00615700;
long DAT_00615708;
long DAT_00615718;
long fde_004140b0;
long FLOAT_UNKNOWN;
long null_ARRAY_00412140;
long null_ARRAY_00413800;
long null_ARRAY_00615410;
long null_ARRAY_00615440;
long null_ARRAY_00615520;
long null_ARRAY_00615580;
long null_ARRAY_006155c0;
long null_ARRAY_006156c0;
long PTR_DAT_006153e0;
long PTR_null_ARRAY_00615420;
ulong
FUN_00401cbd (uint uParm1)
{
  char cVar1;
  int iVar2;
  uint uVar3;
  undefined8 uVar4;
  uint *puVar5;
  long lVar6;
  char *pcVar7;
  int iStack84;
  long lStack80;
  uint uStack68;
  long lStack64;
  uint uStack52;

  if (uParm1 == 0)
    {
      func_0x00401500 ("Usage: %s [OPTION]... NAME...\n", DAT_00615560);
      func_0x00401680 ("Create named pipes (FIFOs) with the given NAMEs.\n",
		       DAT_006154c0);
      FUN_00401abe ();
      func_0x00401680
	("  -m, --mode=MODE    set file permission bits to MODE, not a=rw - umask\n",
	 DAT_006154c0);
      func_0x00401680
	("  -Z                   set the SELinux security context to default type\n      --context[=CTX]  like -Z, or if CTX is specified then set the SELinux\n                         or SMACK security context to CTX\n",
	 DAT_006154c0);
      func_0x00401680 ("      --help     display this help and exit\n",
		       DAT_006154c0);
      pcVar7 = (char *) DAT_006154c0;
      func_0x00401680
	("      --version  output version information and exit\n");
      imperfection_wrapper ();	//    FUN_00401ad8();
    }
  else
    {
      pcVar7 = "Try \'%s --help\' for more information.\n";
      func_0x00401670 (DAT_006154e0,
		       "Try \'%s --help\' for more information.\n",
		       DAT_00615560);
    }
  func_0x00401830 ();
  lStack64 = 0;
  uStack68 = 0;
  lStack80 = 0;
  FUN_00402895 (*(undefined8 *) pcVar7);
  func_0x004017e0 (6, &DAT_004120c3);
  func_0x004017c0 (FUN_0040217f);
  do
    {
      while (true)
	{
	  imperfection_wrapper ();	//      iVar2 = FUN_00405857((ulong)uParm1, pcVar7, &DAT_00412449, null_ARRAY_00412140, 0);
	  if (iVar2 == -1)
	    {
	      if (DAT_00615478 == uParm1)
		{
		  imperfection_wrapper ();	//          FUN_0040467e(0, 0, "missing operand");
		  FUN_00401cbd (1);
		}
	      if (lStack80 != 0)
		{
		  cVar1 = FUN_00401cb2 ();
		  if (cVar1 == '\0')
		    {
		      uVar4 = FUN_00401c74 (lStack80);
		      imperfection_wrapper ();	//            iStack84 = FUN_00403e04(uVar4);
		    }
		  else
		    {
		      imperfection_wrapper ();	//            iStack84 = FUN_00401ca3(lStack80);
		    }
		  if (iStack84 < 0)
		    {
		      imperfection_wrapper ();	//            uVar4 = FUN_00403de5(lStack80);
		      puVar5 = (uint *) func_0x00401820 ();
		      imperfection_wrapper ();	//            FUN_0040467e(1, (ulong)*puVar5, "failed to set default file creation context to %s", uVar4);
		    }
		}
	      uStack52 = 0x1b6;
	      if (lStack64 != 0)
		{
		  lVar6 = FUN_004022cc (lStack64);
		  if (lVar6 == 0)
		    {
		      imperfection_wrapper ();	//            FUN_0040467e(1, 0, "invalid mode");
		    }
		  uVar3 = func_0x004016c0 (0);
		  func_0x004016c0 ((ulong) uVar3);
		  uStack52 = FUN_004026fc (0x1b6, 0, (ulong) uVar3, lVar6, 0);
		  func_0x00401920 (lVar6);
		  if ((uStack52 & 0xfffffe00) != 0)
		    {
		      imperfection_wrapper ();	//            FUN_0040467e(1, 0, "mode must specify only file permission bits");
		    }
		}
	      while ((int) DAT_00615478 < (int) uParm1)
		{
		  if (false)
		    {
		      imperfection_wrapper ();	//            FUN_00401c82(((undefined8 *)pcVar7)[(long)(int)DAT_00615478], 0x1000);
		    }
		  imperfection_wrapper ();	//          iVar2 = FUN_00405955(((undefined8 *)pcVar7)[(long)(int)DAT_00615478], (ulong)uStack52, (ulong)uStack52);
		  if (iVar2 == 0)
		    {
		      if ((lStack64 != 0)
			  && (iVar2 =
			      func_0x00401700 (((undefined8 *)
						pcVar7)[(long) (int)
							DAT_00615478]),
			      iVar2 != 0))
			{
			  imperfection_wrapper ();	//              uVar4 = FUN_00403c8f(4);
			  puVar5 = (uint *) func_0x00401820 ();
			  imperfection_wrapper ();	//              FUN_0040467e(0, (ulong)*puVar5, "cannot set permissions of %s", uVar4);
			  uStack68 = 1;
			}
		    }
		  else
		    {
		      imperfection_wrapper ();	//            uVar4 = FUN_00403c8f(4);
		      puVar5 = (uint *) func_0x00401820 ();
		      imperfection_wrapper ();	//            FUN_0040467e(0, (ulong)*puVar5, "cannot create fifo %s", uVar4);
		      uStack68 = 1;
		    }
		  DAT_00615478 = DAT_00615478 + 1;
		}
	      return (ulong) uStack68;
	    }
	  if (iVar2 != -0x82)
	    break;
	  FUN_00401cbd (0);
	LAB_00401e63:
	  ;
	  imperfection_wrapper ();	//      FUN_00404358(DAT_006154c0, "mkfifo", "GNU coreutils", PTR_DAT_006153e0, "David MacKenzie", 0);
	  func_0x00401830 (0);
	LAB_00401ea1:
	  ;
	  imperfection_wrapper ();	//      FUN_00401cbd();
	}
      if (iVar2 < -0x81)
	{
	  if (iVar2 == -0x83)
	    goto LAB_00401e63;
	  goto LAB_00401ea1;
	}
      if (iVar2 == 0x5a)
	{
	  cVar1 = FUN_00401cb2 ();
	  if (cVar1 == '\0')
	    {
	      if (DAT_00615718 != 0)
		{
		  imperfection_wrapper ();	//          FUN_0040467e(0, 0, "warning: ignoring --context; it requires an SELinux/SMACK-enabled kernel");
		}
	    }
	  else
	    {
	      lStack80 = DAT_00615718;
	    }
	}
      else
	{
	  if (iVar2 != 0x6d)
	    goto LAB_00401ea1;
	  lStack64 = DAT_00615718;
	}
    }
  while (true);
}

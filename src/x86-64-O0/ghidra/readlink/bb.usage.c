
#include "readlink.h"

long null_ARRAY_0061845_0_8_;
long null_ARRAY_0061845_8_8_;
long null_ARRAY_006185c_0_8_;
long null_ARRAY_006185c_16_8_;
long null_ARRAY_006185c_24_8_;
long null_ARRAY_006185c_32_8_;
long null_ARRAY_006185c_40_8_;
long null_ARRAY_006185c_48_8_;
long null_ARRAY_006185c_8_8_;
long null_ARRAY_0061870_0_4_;
long null_ARRAY_0061870_16_8_;
long null_ARRAY_0061870_4_4_;
long null_ARRAY_0061870_8_4_;
long DAT_00414500;
long DAT_004145bd;
long DAT_0041463b;
long DAT_00414d6e;
long DAT_00414d76;
long DAT_00414d8b;
long DAT_00414e80;
long DAT_00414fde;
long DAT_00414fe2;
long DAT_00414fe7;
long DAT_00414fe9;
long DAT_00415653;
long DAT_00415a20;
long DAT_00415a38;
long DAT_00415a3d;
long DAT_00415a50;
long DAT_00415a52;
long DAT_00415a54;
long DAT_00415aaf;
long DAT_00415b40;
long DAT_00415b43;
long DAT_00415b91;
long DAT_00415b95;
long DAT_00415c08;
long DAT_00415c09;
long DAT_00618000;
long DAT_00618010;
long DAT_00618020;
long DAT_00618428;
long DAT_00618440;
long DAT_006184b8;
long DAT_006184bc;
long DAT_006184c0;
long DAT_00618500;
long DAT_00618510;
long DAT_00618520;
long DAT_00618528;
long DAT_00618540;
long DAT_00618548;
long DAT_00618590;
long DAT_00618591;
long DAT_00618598;
long DAT_006185a0;
long DAT_006185a8;
long DAT_00618738;
long DAT_00618740;
long DAT_00618748;
long DAT_00618750;
long DAT_00618758;
long DAT_00618768;
long fde_004169c8;
long FLOAT_UNKNOWN;
long null_ARRAY_00414700;
long null_ARRAY_00414d90;
long null_ARRAY_00416040;
long null_ARRAY_00618450;
long null_ARRAY_00618560;
long null_ARRAY_006185c0;
long null_ARRAY_00618600;
long null_ARRAY_00618700;
long PTR_DAT_00618420;
long PTR_null_ARRAY_00618460;
ulong
FUN_00401f5a (uint uParm1)
{
  bool bVar1;
  int iVar2;
  uint uVar3;
  long lVar4;
  undefined8 uVar5;
  uint *puVar6;
  char *pcVar7;
  uint uStack56;
  uint uStack52;

  if (uParm1 == 0)
    {
      func_0x00401770 ("Usage: %s [OPTION]... FILE...\n", DAT_006185a8);
      func_0x00401960
	("Print value of a symbolic link or canonical file name\n\n",
	 DAT_00618500);
      func_0x00401960
	("  -f, --canonicalize            canonicalize by following every symlink in\n                                every component of the given name recursively;\n                                all but the last component must exist\n  -e, --canonicalize-existing   canonicalize by following every symlink in\n                                every component of the given name recursively,\n                                all components must exist\n",
	 DAT_00618500);
      func_0x00401960
	("  -m, --canonicalize-missing    canonicalize by following every symlink in\n                                every component of the given name recursively,\n                                without requirements on components existence\n  -n, --no-newline              do not output the trailing delimiter\n  -q, --quiet\n  -s, --silent                  suppress most error messages (on by default)\n  -v, --verbose                 report error messages\n  -z, --zero                    end each output line with NUL, not newline\n",
	 DAT_00618500);
      func_0x00401960 ("      --help     display this help and exit\n",
		       DAT_00618500);
      pcVar7 = (char *) DAT_00618500;
      func_0x00401960
	("      --version  output version information and exit\n");
      imperfection_wrapper ();	//    FUN_00401dbe();
    }
  else
    {
      pcVar7 = "Try \'%s --help\' for more information.\n";
      func_0x00401950 (DAT_00618520,
		       "Try \'%s --help\' for more information.\n",
		       DAT_006185a8);
    }
  func_0x00401b00 ();
  uStack52 = 0xffffffff;
  uStack56 = 0;
  bVar1 = false;
  FUN_00403fd6 (*(undefined8 *) pcVar7);
  func_0x00401aa0 (6, &DAT_0041463b);
  func_0x00401a80 (FUN_00402d15);
LAB_00402167:
  ;
  while (true)
    {
      imperfection_wrapper ();	//    iVar2 = FUN_0040759d((ulong)uParm1, pcVar7, "efmnqsvz", null_ARRAY_00414700, 0);
      if (iVar2 == -1)
	{
	  if ((int) uParm1 <= DAT_006184b8)
	    {
	      imperfection_wrapper ();	//        FUN_00405e3b(0, 0, "missing operand");
	      FUN_00401f5a (1);
	    }
	  if (1 < (int) (uParm1 - DAT_006184b8))
	    {
	      if (DAT_00618590 != '\0')
		{
		  imperfection_wrapper ();	//          FUN_00405e3b(0, 0, "ignoring --no-newline with multiple arguments");
		}
	      DAT_00618590 = '\0';
	    }
	  while (DAT_006184b8 < (int) uParm1)
	    {
	      uVar5 = ((undefined8 *) pcVar7)[(long) DAT_006184b8];
	      if (uStack52 == 0xffffffff)
		{
		  lVar4 = FUN_00402313 (uVar5, 0x3f);
		}
	      else
		{
		  imperfection_wrapper ();	//          lVar4 = FUN_0040250f(uVar5, (ulong)uStack52, (ulong)uStack52);
		}
	      if (lVar4 == 0)
		{
		  uStack56 = 1;
		  if (DAT_00618591 != '\0')
		    {
		      imperfection_wrapper ();	//            uVar5 = FUN_0040546c(0, 3, uVar5);
		      puVar6 = (uint *) func_0x00401af0 ();
		      imperfection_wrapper ();	//            FUN_00405e3b(0, (ulong)*puVar6, &DAT_00414d6e, uVar5);
		    }
		}
	      else
		{
		  func_0x00401960 (lVar4);
		  if (DAT_00618590 != '\x01')
		    {
		      if (bVar1)
			{
			  uVar3 = 0;
			}
		      else
			{
			  uVar3 = 10;
			}
		      func_0x00401ab0 ((ulong) uVar3);
		    }
		  func_0x00401c20 (lVar4);
		}
	      DAT_006184b8 = DAT_006184b8 + 1;
	    }
	  return (ulong) uStack56;
	}
      if (iVar2 != 0x6d)
	break;
      uStack52 = 2;
    }
  if (iVar2 < 0x6e)
    {
      if (iVar2 == -0x82)
	{
	  FUN_00401f5a (0);
	LAB_0040211f:
	  ;
	  imperfection_wrapper ();	//      FUN_00405a3d(DAT_00618500, "readlink", "GNU coreutils", PTR_DAT_00618420, "Dmitry V. Levin", 0);
	  func_0x00401b00 (0);
	}
      else
	{
	  if (-0x82 < iVar2)
	    {
	      if (iVar2 == 0x65)
		{
		  uStack52 = 0;
		}
	      else
		{
		  if (iVar2 != 0x66)
		    goto LAB_0040215d;
		  uStack52 = 1;
		}
	      goto LAB_00402167;
	    }
	  if (iVar2 == -0x83)
	    goto LAB_0040211f;
	}
    }
  else
    {
      if (iVar2 == 0x73)
	{
	LAB_004020fd:
	  ;
	  DAT_00618591 = '\0';
	  goto LAB_00402167;
	}
      if (0x73 < iVar2)
	{
	  if (iVar2 == 0x76)
	    {
	      DAT_00618591 = '\x01';
	    }
	  else
	    {
	      if (iVar2 != 0x7a)
		goto LAB_0040215d;
	      bVar1 = true;
	    }
	  goto LAB_00402167;
	}
      if (iVar2 == 0x6e)
	{
	  DAT_00618590 = '\x01';
	  goto LAB_00402167;
	}
      if (iVar2 == 0x71)
	goto LAB_004020fd;
    }
LAB_0040215d:
  ;
  imperfection_wrapper ();	//  FUN_00401f5a();
  goto LAB_00402167;
}

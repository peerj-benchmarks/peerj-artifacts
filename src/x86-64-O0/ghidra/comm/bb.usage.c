
#include "comm.h"

long DAT_0061655_0_1_;
long DAT_0061655_1_1_;
long null_ARRAY_0061641_0_8_;
long null_ARRAY_0061641_8_8_;
long null_ARRAY_0061658_0_8_;
long null_ARRAY_0061658_16_8_;
long null_ARRAY_0061658_24_8_;
long null_ARRAY_0061658_32_8_;
long null_ARRAY_0061658_40_8_;
long null_ARRAY_0061658_48_8_;
long null_ARRAY_0061658_8_8_;
long null_ARRAY_006166e_0_4_;
long null_ARRAY_006166e_16_8_;
long null_ARRAY_006166e_4_4_;
long null_ARRAY_006166e_8_4_;
long DAT_00412e40;
long DAT_00412efd;
long DAT_00412f7b;
long DAT_0041362c;
long DAT_00413657;
long DAT_00413659;
long DAT_0041365b;
long DAT_004136b9;
long DAT_00413710;
long DAT_00413713;
long DAT_00413760;
long DAT_0041389e;
long DAT_004138a2;
long DAT_004138a7;
long DAT_004138a9;
long DAT_00413f13;
long DAT_004142e0;
long DAT_00414372;
long DAT_00414377;
long DAT_004143e7;
long DAT_00414478;
long DAT_0041447b;
long DAT_004144c9;
long DAT_00414538;
long DAT_00414539;
long DAT_00616000;
long DAT_00616010;
long DAT_00616020;
long DAT_006163e0;
long DAT_006163f8;
long DAT_00616400;
long DAT_00616478;
long DAT_0061647c;
long DAT_00616480;
long DAT_006164c0;
long DAT_006164c8;
long DAT_006164d0;
long DAT_006164e0;
long DAT_006164e8;
long DAT_00616500;
long DAT_00616508;
long DAT_00616550;
long DAT_00616551;
long DAT_00616552;
long DAT_00616553;
long DAT_00616554;
long DAT_00616555;
long DAT_00616557;
long DAT_00616558;
long DAT_00616560;
long DAT_00616568;
long DAT_00616570;
long DAT_00616578;
long DAT_006166c0;
long DAT_00616718;
long DAT_00616720;
long DAT_00616728;
long DAT_00616738;
long fde_00415248;
long FLOAT_UNKNOWN;
long null_ARRAY_00413040;
long null_ARRAY_00414980;
long null_ARRAY_00616410;
long null_ARRAY_00616440;
long null_ARRAY_00616520;
long null_ARRAY_00616580;
long null_ARRAY_006165c0;
long null_ARRAY_006166e0;
long PTR_DAT_006163e8;
long PTR_DAT_006163f0;
long PTR_null_ARRAY_00616420;
void
FUN_00401cfa (uint uParm1)
{
  int extraout_EDX;
  char *pcVar1;
  ulong uVar2;

  if (uParm1 == 0)
    {
      func_0x00401590 ("Usage: %s [OPTION]... FILE1 FILE2\n", DAT_00616578);
      func_0x00401710 ("Compare sorted files FILE1 and FILE2 line by line.\n",
		       DAT_006164c0);
      func_0x00401710
	("\nWhen FILE1 or FILE2 (not both) is -, read standard input.\n",
	 DAT_006164c0);
      func_0x00401710
	("\nWith no options, produce three-column output.  Column one contains\nlines unique to FILE1, column two contains lines unique to FILE2,\nand column three contains lines common to both files.\n",
	 DAT_006164c0);
      func_0x00401710
	("\n  -1              suppress column 1 (lines unique to FILE1)\n  -2              suppress column 2 (lines unique to FILE2)\n  -3              suppress column 3 (lines that appear in both files)\n",
	 DAT_006164c0);
      func_0x00401710
	("\n  --check-order     check that the input is correctly sorted, even\n                      if all input lines are pairable\n  --nocheck-order   do not check that the input is correctly sorted\n",
	 DAT_006164c0);
      func_0x00401710
	("  --output-delimiter=STR  separate columns with STR\n",
	 DAT_006164c0);
      func_0x00401710 ("  --total           output a summary\n",
		       DAT_006164c0);
      func_0x00401710
	("  -z, --zero-terminated    line delimiter is NUL, not newline\n",
	 DAT_006164c0);
      func_0x00401710 ("      --help     display this help and exit\n",
		       DAT_006164c0);
      func_0x00401710
	("      --version  output version information and exit\n",
	 DAT_006164c0);
      func_0x00401710
	("\nNote, comparisons honor the rules specified by \'LC_COLLATE\'.\n",
	 DAT_006164c0);
      pcVar1 = DAT_00616578;
      func_0x00401590
	("\nExamples:\n  %s -12 file1 file2  Print only lines present in both file1 and file2.\n  %s -3 file1 file2  Print lines in file1 not in file2, and vice versa.\n",
	 DAT_00616578, DAT_00616578);
      imperfection_wrapper ();	//    FUN_00401b5e();
    }
  else
    {
      pcVar1 = "Try \'%s --help\' for more information.\n";
      func_0x00401700 (DAT_006164e0,
		       "Try \'%s --help\' for more information.\n",
		       DAT_00616578);
    }
  uVar2 = (ulong) uParm1;
  func_0x004018d0 ();
  if (extraout_EDX == 2)
    {
      if (DAT_00616552 != '\x01')
	{
	  return;
	}
      if (DAT_00616551 != '\0')
	{
	  func_0x004018a0 (PTR_DAT_006163e8, 1, DAT_00616560, pcVar1);
	}
    }
  else
    {
      if (extraout_EDX == 3)
	{
	  if (DAT_00616553 != '\x01')
	    {
	      return;
	    }
	  if (DAT_00616551 != '\0')
	    {
	      func_0x004018a0 (PTR_DAT_006163e8, 1, DAT_00616560, pcVar1);
	    }
	  if (DAT_00616552 != '\0')
	    {
	      func_0x004018a0 (PTR_DAT_006163e8, 1, DAT_00616560, pcVar1);
	    }
	}
      else
	{
	  if ((extraout_EDX == 1) && (DAT_00616551 != '\x01'))
	    {
	      return;
	    }
	}
    }
  func_0x004018a0 (*(undefined8 *) (uVar2 + 0x10), 1,
		   *(undefined8 *) (uVar2 + 8), pcVar1);
  return;
}


#include "realpath.h"

long null_ARRAY_0061859_0_8_;
long null_ARRAY_0061859_8_8_;
long null_ARRAY_0061870_0_8_;
long null_ARRAY_0061870_16_8_;
long null_ARRAY_0061870_24_8_;
long null_ARRAY_0061870_32_8_;
long null_ARRAY_0061870_40_8_;
long null_ARRAY_0061870_48_8_;
long null_ARRAY_0061870_8_8_;
long null_ARRAY_0061884_0_4_;
long null_ARRAY_0061884_16_8_;
long null_ARRAY_0061884_4_4_;
long null_ARRAY_0061884_8_4_;
long DAT_00414cc0;
long DAT_00414d7d;
long DAT_00414dfb;
long DAT_004153ee;
long DAT_00415417;
long DAT_0041541a;
long DAT_0041541e;
long DAT_0041543b;
long DAT_00415443;
long DAT_00415458;
long DAT_00415550;
long DAT_0041569e;
long DAT_004156a2;
long DAT_004156a7;
long DAT_004156a9;
long DAT_00415d13;
long DAT_004160e0;
long DAT_004160f8;
long DAT_004160fd;
long DAT_00416110;
long DAT_00416112;
long DAT_00416114;
long DAT_0041616f;
long DAT_00416200;
long DAT_00416203;
long DAT_00416251;
long DAT_00416255;
long DAT_004162c8;
long DAT_004162c9;
long DAT_00618130;
long DAT_00618140;
long DAT_00618150;
long DAT_00618560;
long DAT_00618570;
long DAT_00618580;
long DAT_006185f8;
long DAT_006185fc;
long DAT_00618600;
long DAT_00618640;
long DAT_00618650;
long DAT_00618660;
long DAT_00618668;
long DAT_00618680;
long DAT_00618688;
long DAT_006186d0;
long DAT_006186d1;
long DAT_006186d8;
long DAT_006186e0;
long DAT_006186e8;
long DAT_006186f0;
long DAT_006186f8;
long DAT_00618878;
long DAT_00618880;
long DAT_00618888;
long DAT_00618890;
long DAT_00618898;
long DAT_006188a8;
long fde_004170d0;
long FLOAT_UNKNOWN;
long null_ARRAY_00414ec0;
long null_ARRAY_00415460;
long null_ARRAY_00416700;
long null_ARRAY_00618590;
long null_ARRAY_006186a0;
long null_ARRAY_00618700;
long null_ARRAY_00618740;
long null_ARRAY_00618840;
long PTR_DAT_00618568;
long PTR_null_ARRAY_006185a0;
long
FUN_00401f5a (uint uParm1)
{
  long lVar1;
  long lVar2;
  uint uVar3;
  ulong uVar4;

  if (uParm1 == 0)
    {
      func_0x00401770 ("Usage: %s [OPTION]... FILE...\n", DAT_006186f8);
      func_0x00401960
	("Print the resolved absolute file name;\nall but the last component must exist\n\n",
	 DAT_00618640);
      func_0x00401960
	("  -e, --canonicalize-existing  all components of the path must exist\n  -m, --canonicalize-missing   no path components need exist or be a directory\n  -L, --logical                resolve \'..\' components before symlinks\n  -P, --physical               resolve symlinks as encountered (default)\n  -q, --quiet                  suppress most error messages\n      --relative-to=DIR        print the resolved path relative to DIR\n      --relative-base=DIR      print absolute paths unless paths below DIR\n  -s, --strip, --no-symlinks   don\'t expand symlinks\n  -z, --zero                   end each output line with NUL, not newline\n\n",
	 DAT_00618640);
      func_0x00401960 ("      --help     display this help and exit\n",
		       DAT_00618640);
      uVar3 = (uint) DAT_00618640;
      func_0x00401960
	("      --version  output version information and exit\n");
      imperfection_wrapper ();	//    FUN_00401dbe();
    }
  else
    {
      uVar3 = 0x415060;
      func_0x00401950 (DAT_00618660,
		       "Try \'%s --help\' for more information.\n",
		       DAT_006186f8);
    }
  uVar4 = (ulong) uParm1;
  func_0x00401b00 ();
  imperfection_wrapper ();	//  lVar1 = FUN_00402b29(uVar4, (ulong)uVar3, (ulong)uVar3);
  if ((DAT_006186d0 != '\0') && (lVar1 != 0))
    {
      imperfection_wrapper ();	//    lVar2 = FUN_00402b29(lVar1, (ulong)(uVar3 & 0xfffffffb), (ulong)(uVar3 & 0xfffffffb));
      func_0x00401c20 (lVar1);
      lVar1 = lVar2;
    }
  return lVar1;
}

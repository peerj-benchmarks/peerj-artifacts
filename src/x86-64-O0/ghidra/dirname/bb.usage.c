
#include "dirname.h"

long null_ARRAY_006143f_0_8_;
long null_ARRAY_006143f_8_8_;
long null_ARRAY_0061454_0_8_;
long null_ARRAY_0061454_16_8_;
long null_ARRAY_0061454_24_8_;
long null_ARRAY_0061454_32_8_;
long null_ARRAY_0061454_40_8_;
long null_ARRAY_0061454_48_8_;
long null_ARRAY_0061454_8_8_;
long null_ARRAY_0061468_0_4_;
long null_ARRAY_0061468_16_8_;
long null_ARRAY_0061468_4_4_;
long null_ARRAY_0061468_8_4_;
long DAT_00411400;
long DAT_004114bd;
long DAT_0041153b;
long DAT_00411852;
long DAT_00411881;
long DAT_0041189a;
long DAT_004118e0;
long DAT_00411a1e;
long DAT_00411a22;
long DAT_00411a27;
long DAT_00411a29;
long DAT_00412093;
long DAT_00412460;
long DAT_00412478;
long DAT_0041247d;
long DAT_004124e7;
long DAT_00412578;
long DAT_0041257b;
long DAT_004125c9;
long DAT_004125cd;
long DAT_00412640;
long DAT_00412641;
long DAT_00614000;
long DAT_00614010;
long DAT_00614020;
long DAT_006143c8;
long DAT_006143e0;
long DAT_00614458;
long DAT_0061445c;
long DAT_00614460;
long DAT_00614480;
long DAT_00614490;
long DAT_006144a0;
long DAT_006144a8;
long DAT_006144c0;
long DAT_006144c8;
long DAT_00614510;
long DAT_00614518;
long DAT_00614520;
long DAT_006146b8;
long DAT_006146c0;
long DAT_006146c8;
long DAT_006146d8;
long fde_00413298;
long FLOAT_UNKNOWN;
long null_ARRAY_004115c0;
long null_ARRAY_00412a80;
long null_ARRAY_006143f0;
long null_ARRAY_006144e0;
long null_ARRAY_00614540;
long null_ARRAY_00614580;
long null_ARRAY_00614680;
long PTR_DAT_006143c0;
long PTR_null_ARRAY_00614400;
undefined8
FUN_00401bda (uint uParm1)
{
  bool bVar1;
  int iVar2;
  uint uVar3;
  char *pcVar4;
  undefined8 uVar5;
  long lStack56;
  undefined1 *puStack48;

  if (uParm1 == 0)
    {
      func_0x004014a0 ("Usage: %s [OPTION] NAME...\n", DAT_00614520);
      func_0x00401620
	("Output each NAME with its last non-slash component and trailing slashes\nremoved; if NAME contains no /\'s, output \'.\' (meaning the current directory).\n\n",
	 DAT_00614480);
      func_0x00401620
	("  -z, --zero     end each output line with NUL, not newline\n",
	 DAT_00614480);
      func_0x00401620 ("      --help     display this help and exit\n",
		       DAT_00614480);
      func_0x00401620
	("      --version  output version information and exit\n",
	 DAT_00614480);
      pcVar4 = (char *) DAT_00614520;
      func_0x004014a0
	("\nExamples:\n  %s /usr/bin/          -> \"/usr\"\n  %s dir1/str dir2/str  -> \"dir1\" followed by \"dir2\"\n  %s stdio.h            -> \".\"\n",
	 DAT_00614520, DAT_00614520, DAT_00614520);
      imperfection_wrapper ();	//    FUN_00401a3e();
    }
  else
    {
      pcVar4 = "Try \'%s --help\' for more information.\n";
      func_0x00401610 (DAT_006144a0,
		       "Try \'%s --help\' for more information.\n",
		       DAT_00614520);
    }
  func_0x004017b0 ();
  bVar1 = false;
  FUN_00402048 (*(undefined8 *) pcVar4);
  func_0x00401750 (6, &DAT_0041153b);
  func_0x00401730 (FUN_00401e71);
LAB_00401cef:
  ;
  do
    {
      uVar5 = 0x401d0d;
      imperfection_wrapper ();	//    iVar2 = FUN_00404e5b((ulong)uParm1, pcVar4, &DAT_00411852, null_ARRAY_004115c0, 0);
      if (iVar2 == -1)
	{
	  if ((int) uParm1 < DAT_00614458 + 1)
	    {
	      imperfection_wrapper ();	//        FUN_00403c82(0, 0, "missing operand");
	      FUN_00401bda (1);
	    }
	  while (DAT_00614458 < (int) uParm1)
	    {
	      puStack48 =
		(undefined1 *) ((undefined8 *) pcVar4)[(long) DAT_00614458];
	      lStack56 = FUN_00401f56 (puStack48);
	      if (lStack56 == 0)
		{
		  puStack48 = &DAT_00411881;
		  lStack56 = 1;
		}
	      func_0x00401780 (puStack48, 1, lStack56, DAT_00614480);
	      if (bVar1)
		{
		  uVar3 = 0;
		}
	      else
		{
		  uVar3 = 10;
		}
	      func_0x00401760 ((ulong) uVar3);
	      DAT_00614458 = DAT_00614458 + 1;
	    }
	  return 0;
	}
      if (iVar2 == -0x82)
	{
	  uVar5 = 0x401d53;
	  FUN_00401bda (0);
	LAB_00401d53:
	  ;
	  imperfection_wrapper ();	//      FUN_0040399d(DAT_00614480, "dirname", "GNU coreutils", PTR_DAT_006143c0, "David MacKenzie", "Jim Meyering", 0, uVar5);
	  func_0x004017b0 (0);
	}
      else
	{
	  if (iVar2 == 0x7a)
	    {
	      bVar1 = true;
	      goto LAB_00401cef;
	    }
	  if (iVar2 == -0x83)
	    goto LAB_00401d53;
	}
      imperfection_wrapper ();	//    FUN_00401bda();
    }
  while (true);
}

typedef unsigned char   undefined;

typedef unsigned char    byte;
typedef unsigned char    dwfenc;
typedef unsigned int    dword;
typedef unsigned long    qword;
typedef unsigned char    undefined1;
typedef unsigned short    undefined2;
typedef unsigned int    undefined4;
typedef unsigned long    undefined6;
typedef unsigned long    undefined8;
typedef unsigned short    word;
#define unkbyte9   unsigned long long
#define unkbyte10   unsigned long long
#define unkbyte11   unsigned long long
#define unkbyte12   unsigned long long
#define unkbyte13   unsigned long long
#define unkbyte14   unsigned long long
#define unkbyte15   unsigned long long
#define unkbyte16   unsigned long long

#define unkuint9   unsigned long long
#define unkuint10   unsigned long long
#define unkuint11   unsigned long long
#define unkuint12   unsigned long long
#define unkuint13   unsigned long long
#define unkuint14   unsigned long long
#define unkuint15   unsigned long long
#define unkuint16   unsigned long long

#define unkint9   long long
#define unkint10   long long
#define unkint11   long long
#define unkint12   long long
#define unkint13   long long
#define unkint14   long long
#define unkint15   long long
#define unkint16   long long

#define unkfloat1   float
#define unkfloat2   float
#define unkfloat3   float
#define unkfloat5   double
#define unkfloat6   double
#define unkfloat7   double
#define unkfloat9   long double
#define unkfloat11   long double
#define unkfloat12   long double
#define unkfloat13   long double
#define unkfloat14   long double
#define unkfloat15   long double
#define unkfloat16   long double

#define BADSPACEBASE   void
#define code   void

#define ulong long
#define true 1
#define bool int
#define false 0
#define float10 int
#define ushort int
#define undefined7 long
#define undefined2 long
#define uint5 long
#define uint6 long

// Add definition for uint
typedef unsigned int    uint;

#define sbyte char

extern imperfection_wrapper(void);
typedef struct eh_frame_hdr eh_frame_hdr, *Peh_frame_hdr;

struct eh_frame_hdr {
    byte eh_frame_hdr_version; // Exception Handler Frame Header Version
    dwfenc eh_frame_pointer_encoding; // Exception Handler Frame Pointer Encoding
    dwfenc eh_frame_desc_entry_count_encoding; // Encoding of # of Exception Handler FDEs
    dwfenc eh_frame_table_encoding; // Exception Handler Table Encoding
};

typedef struct fde_table_entry fde_table_entry, *Pfde_table_entry;

struct fde_table_entry {
    dword initial_loc; // Initial Location
    dword data_loc; // Data location
};

typedef enum Elf64_DynTag {
    DT_AUDIT=1879047932,
    DT_AUXILIARY=2147483645,
    DT_BIND_NOW=24,
    DT_CHECKSUM=1879047672,
    DT_CONFIG=1879047930,
    DT_DEBUG=21,
    DT_DEPAUDIT=1879047931,
    DT_ENCODING=32,
    DT_FEATURE_1=1879047676,
    DT_FILTER=2147483647,
    DT_FINI=13,
    DT_FINI_ARRAY=26,
    DT_FINI_ARRAYSZ=28,
    DT_FLAGS=30,
    DT_FLAGS_1=1879048187,
    DT_GNU_CONFLICT=1879047928,
    DT_GNU_CONFLICTSZ=1879047670,
    DT_GNU_HASH=1879047925,
    DT_GNU_LIBLIST=1879047929,
    DT_GNU_LIBLISTSZ=1879047671,
    DT_GNU_PRELINKED=1879047669,
    DT_HASH=4,
    DT_INIT=12,
    DT_INIT_ARRAY=25,
    DT_INIT_ARRAYSZ=27,
    DT_JMPREL=23,
    DT_MOVEENT=1879047674,
    DT_MOVESZ=1879047675,
    DT_MOVETAB=1879047934,
    DT_NEEDED=1,
    DT_NULL=0,
    DT_PLTGOT=3,
    DT_PLTPAD=1879047933,
    DT_PLTPADSZ=1879047673,
    DT_PLTREL=20,
    DT_PLTRELSZ=2,
    DT_POSFLAG_1=1879047677,
    DT_PREINIT_ARRAYSZ=33,
    DT_REL=17,
    DT_RELA=7,
    DT_RELACOUNT=1879048185,
    DT_RELAENT=9,
    DT_RELASZ=8,
    DT_RELCOUNT=1879048186,
    DT_RELENT=19,
    DT_RELSZ=18,
    DT_RPATH=15,
    DT_RUNPATH=29,
    DT_SONAME=14,
    DT_STRSZ=10,
    DT_STRTAB=5,
    DT_SYMBOLIC=16,
    DT_SYMENT=11,
    DT_SYMINENT=1879047679,
    DT_SYMINFO=1879047935,
    DT_SYMINSZ=1879047678,
    DT_SYMTAB=6,
    DT_TEXTREL=22,
    DT_TLSDESC_GOT=1879047927,
    DT_TLSDESC_PLT=1879047926,
    DT_VERDEF=1879048188,
    DT_VERDEFNUM=1879048189,
    DT_VERNEED=1879048190,
    DT_VERNEEDNUM=1879048191,
    DT_VERSYM=1879048176
} Elf64_DynTag;

typedef enum Elf_ProgramHeaderType {
    PT_DYNAMIC=2,
    PT_GNU_EH_FRAME=1685382480,
    PT_GNU_RELRO=1685382482,
    PT_GNU_STACK=1685382481,
    PT_INTERP=3,
    PT_LOAD=1,
    PT_NOTE=4,
    PT_NULL=0,
    PT_PHDR=6,
    PT_SHLIB=5,
    PT_TLS=7
} Elf_ProgramHeaderType;

typedef struct Elf64_Dyn Elf64_Dyn, *PElf64_Dyn;

struct Elf64_Dyn {
    enum Elf64_DynTag d_tag;
    qword d_val;
};

typedef struct Elf64_Shdr Elf64_Shdr, *PElf64_Shdr;

typedef enum Elf_SectionHeaderType {
    SHT_CHECKSUM=1879048184,
    SHT_DYNAMIC=6,
    SHT_DYNSYM=11,
    SHT_FINI_ARRAY=15,
    SHT_GNU_ATTRIBUTES=1879048181,
    SHT_GNU_HASH=1879048182,
    SHT_GNU_LIBLIST=1879048183,
    SHT_GNU_verdef=1879048189,
    SHT_GNU_verneed=1879048190,
    SHT_GNU_versym=1879048191,
    SHT_GROUP=17,
    SHT_HASH=5,
    SHT_INIT_ARRAY=14,
    SHT_NOBITS=8,
    SHT_NOTE=7,
    SHT_NULL=0,
    SHT_PREINIT_ARRAY=16,
    SHT_PROGBITS=1,
    SHT_REL=9,
    SHT_RELA=4,
    SHT_SHLIB=10,
    SHT_STRTAB=3,
    SHT_SUNW_COMDAT=1879048187,
    SHT_SUNW_move=1879048186,
    SHT_SUNW_syminfo=1879048188,
    SHT_SYMTAB=2,
    SHT_SYMTAB_SHNDX=18
} Elf_SectionHeaderType;

struct Elf64_Shdr {
    dword sh_name;
    enum Elf_SectionHeaderType sh_type;
    qword sh_flags;
    qword sh_addr;
    qword sh_offset;
    qword sh_size;
    dword sh_link;
    dword sh_info;
    qword sh_addralign;
    qword sh_entsize;
};

typedef struct Elf64_Phdr Elf64_Phdr, *PElf64_Phdr;

struct Elf64_Phdr {
    enum Elf_ProgramHeaderType p_type;
    dword p_flags;
    qword p_offset;
    qword p_vaddr;
    qword p_paddr;
    qword p_filesz;
    qword p_memsz;
    qword p_align;
};

typedef struct Elf64_Sym Elf64_Sym, *PElf64_Sym;

struct Elf64_Sym {
    dword st_name;
    byte st_info;
    byte st_other;
    word st_shndx;
    qword st_value;
    qword st_size;
};

typedef struct Elf64_Rela Elf64_Rela, *PElf64_Rela;

struct Elf64_Rela {
    qword r_offset; // location to apply the relocation action
    qword r_info; // the symbol table index and the type of relocation
    qword r_addend; // a constant addend used to compute the relocatable field value
};

typedef struct Elf64_Ehdr Elf64_Ehdr, *PElf64_Ehdr;

struct Elf64_Ehdr {
    byte e_ident_magic_num;
    char e_ident_magic_str[3];
    byte e_ident_class;
    byte e_ident_data;
    byte e_ident_version;
    byte e_ident_pad[9];
    word e_type;
    word e_machine;
    dword e_version;
    qword e_entry;
    qword e_phoff;
    qword e_shoff;
    dword e_flags;
    word e_ehsize;
    word e_phentsize;
    word e_phnum;
    word e_shentsize;
    word e_shnum;
    word e_shstrndx;
};




undefined8 _DT_INIT(void);
void FUN_004017f0(void);
void entry(void);
void FUN_00401d50(void);
void FUN_00401dd0(void);
void FUN_00401e50(void);
ulong FUN_00401e8e(byte bParm1);
ulong FUN_00401e9d(byte bParm1);
void FUN_00401ed0(void);
void FUN_00401eea(undefined *puParm1);
undefined8 FUN_00402086(int iParm1);
ulong FUN_004020a6(char cParm1);
undefined8 FUN_004020d3(undefined uParm1);
char * FUN_00402133(undefined4 uParm1);
void FUN_00402193(int param_1);
void FUN_004021e9(void);
void FUN_0040221e(uint param_1,int *param_2);
long FUN_004022b2(void);
long FUN_004022e7(void);
void FUN_00402306(void);
long FUN_00402339(void);
long FUN_0040235d(void);
void FUN_004023be(undefined4 param_1);
ulong FUN_004024ac(char *pcParm1,char **ppcParm2,undefined8 *puParm3,char *pcParm4);
ulong FUN_00402623(undefined8 uParm1,long *plParm2,float10 *pfParm3,ulong *puParm4);
ulong FUN_004027c4(undefined8 uParm1,char **ppcParm2,float10 *pfParm3,ulong *puParm4,uint uParm5);
void FUN_00402aa4(undefined4 uParm1,undefined8 uParm2);
ulong FUN_00402b3b(uint param_1,undefined8 param_2,ulong param_3,uint param_4,int param_5,uint param_6);
long FUN_0040307c(long lParm1);
void FUN_0040321b(ulong uParm1);
void FUN_00403267(uint uParm1);
void FUN_00403551(long lParm1);
ulong FUN_00403abc(undefined8 uParm1,undefined8 uParm2,undefined8 uParm3);
undefined8 FUN_00403b99(ulong param_1);
void FUN_00403e64(void);
undefined8 FUN_00403ecd(char *pcParm1,undefined8 *puParm2,undefined8 uParm3,long lParm4);
byte * FUN_00404172(byte **ppbParm1);
ulong FUN_0040423c(ulong uParm1);
ulong FUN_0040429d(undefined8 uParm1,undefined8 uParm2);
ulong FUN_00404362(char *pcParm1,char cParm2);
ulong FUN_00404441(uint uParm1,undefined8 *puParm2);
ulong FUN_00404d0a(byte bParm1);
void FUN_00404d19(undefined8 uParm1,undefined8 uParm2);
ulong FUN_00404da9(undefined8 *puParm1,undefined8 *puParm2);
void FUN_00404de6(void);
void FUN_00404f32(char *pcParm1,uint uParm2);
void FUN_00405594(void);
long FUN_004055a4(undefined8 uParm1,long lParm2,long lParm3,long lParm4);
void FUN_004056d3(undefined8 uParm1,undefined8 uParm2,long lParm3);
void FUN_00405754(long lParm1,long lParm2,long lParm3);
long FUN_0040588a(undefined8 uParm1,undefined8 uParm2,undefined8 uParm3,undefined8 uParm4,undefined8 uParm5,code *pcParm6);
ulong FUN_00405910(int iParm1);
ulong FUN_00405930(uint uParm1);
void FUN_0040594f(void);
ulong FUN_00405a34(uint *puParm1);
long FUN_00405a82(uint *puParm1,ulong uParm2);
undefined * FUN_00405b07(undefined *puParm1,undefined *puParm2,long lParm3);
long FUN_00405b50(long lParm1,long lParm2,long lParm3,ulong *puParm4,int iParm5,uint uParm6);
void FUN_00405f16(long lParm1);
ulong FUN_00405ff3(undefined1 *puParm1,byte bParm2,uint uParm3);
undefined8 * FUN_0040607b(undefined8 *puParm1,int iParm2);
char * FUN_004060f2(char *pcParm1,int iParm2);
ulong FUN_00406192(long param_1,ulong param_2,long param_3,ulong param_4,uint param_5,uint param_6,long param_7,char *param_8,char *param_9);
undefined1 * FUN_0040705c(int iParm1,undefined8 uParm2,undefined8 uParm3,uint *puParm4);
void FUN_004072cf(uint uParm1,uint uParm2,undefined8 uParm3);
void FUN_00407310(undefined8 uParm1,undefined8 uParm2,char cParm3);
void FUN_004073a4(undefined8 uParm1,char cParm2);
void FUN_004073ce(undefined8 uParm1);
void FUN_004073ed(uint uParm1,undefined8 uParm2,undefined8 uParm3);
void FUN_00407419(uint uParm1,undefined8 uParm2);
void FUN_00407442(undefined8 uParm1);
void FUN_00407461(undefined8 uParm1,long lParm2,undefined8 uParm3,undefined8 uParm4,undefined8 *puParm5,undefined8 uParm6);
void FUN_004078c5(undefined8 uParm1,undefined8 uParm2,undefined8 uParm3,undefined8 uParm4,uint *puParm5);
void FUN_00407997(undefined4 uParm1,undefined4 uParm2,undefined4 uParm3,undefined4 uParm4,undefined4 uParm5,undefined4 uParm6,undefined4 uParm7,undefined4 uParm8,undefined8 param_1,undefined8 param_2,undefined8 param_3,undefined8 param_4,undefined8 param_5,undefined8 param_6);
void FUN_00407a4d(long lParm1,ulong *puParm2,ulong uParm3);
void FUN_00407b18(undefined8 uParm1);
long FUN_00407b32(long lParm1);
long FUN_00407b67(long lParm1,long lParm2);
void FUN_00407bc8(undefined8 uParm1,undefined8 uParm2);
void FUN_00407bfc(undefined8 uParm1);
long FUN_00407c29(void);
long FUN_00407c53(undefined8 uParm1,undefined8 uParm2);
undefined8 FUN_00407c8c(long *plParm1,int iParm2);
ulong FUN_00407d2a(undefined8 uParm1,uint uParm2,int iParm3);
ulong FUN_00407d6b(char *pcParm1,char **ppcParm2,uint uParm3,undefined8 *puParm4,long lParm5);
undefined8 FUN_0040810a(int iParm1);
ulong FUN_00408130(ulong *puParm1,int iParm2);
ulong FUN_0040818f(undefined8 uParm1,uint uParm2,int iParm3);
ulong FUN_004081d0(byte *pbParm1,byte **ppbParm2,uint uParm3,undefined8 *puParm4,long lParm5);
ulong FUN_004085b1(uint uParm1);
void FUN_004085da(void);
void FUN_0040860e(uint uParm1);
void FUN_00408682(uint uParm1,uint uParm2,undefined8 uParm3,undefined8 uParm4);
void FUN_00408706(undefined4 uParm1,undefined4 uParm2,undefined4 uParm3,undefined4 uParm4,undefined4 uParm5,undefined4 uParm6,undefined4 uParm7,undefined4 uParm8,uint param_1,uint param_2,undefined8 param_3,undefined8 param_4,undefined8 param_5,undefined8 param_6);
void FUN_004087ea(void);
long FUN_004087f0(long *plParm1,ulong *puParm2,int iParm3,long lParm4);
void FUN_004089ca(long lParm1,int *piParm2);
ulong FUN_00408bae(int param_1,undefined8 *param_2,char *param_3,long *param_4,int *param_5,int param_6,int *param_7,int param_8,undefined8 param_9);
char * FUN_00409198(undefined8 uParm1,undefined8 uParm2,char *pcParm3,int *piParm4,int iParm5);
ulong FUN_00409266(uint param_1,undefined8 *param_2,char *param_3,long param_4,undefined8 param_5,uint param_6,uint *param_7,uint param_8);
ulong FUN_00409a2f(uint param_1,undefined8 param_2,undefined8 param_3,undefined8 param_4,undefined8 param_5,uint param_6,uint param_7);
void FUN_00409abf(uint uParm1,undefined8 uParm2,undefined8 uParm3,undefined8 uParm4,undefined8 uParm5);
undefined8 FUN_00409b09(long lParm1);
ulong FUN_00409b3a(uint *puParm1,byte *pbParm2,long lParm3,undefined8 uParm4);
undefined8 FUN_00409bbd(long lParm1,long lParm2);
ulong FUN_00409c26(undefined4 uParm1,undefined4 uParm2,undefined4 uParm3,undefined4 uParm4,undefined4 uParm5,undefined4 uParm6,undefined4 uParm7,undefined4 uParm8,long param_1,ulong param_2,undefined8 param_3,undefined8 param_4,undefined8 param_5,undefined8 param_6);
ulong FUN_00409db7(void);
long FUN_00409e04(ulong uParm1,long lParm2,ulong uParm3,long lParm4,long *plParm5);
int * FUN_0040a050(ulong uParm1,uint *puParm2,ulong uParm3,uint *puParm4,ulong *puParm5);
char * FUN_0040ab30(ulong uParm1,long lParm2,long lParm3);
long FUN_0040ad9e(int *param_1,long *param_2);
long FUN_0040af89(undefined auParm1 [16],int *piParm2,long *plParm3);
undefined8 FUN_0040b348(int iParm1,long lParm2,uint *puParm3,long lParm4,uint uParm5);
void FUN_0040bb5b(uint param_1);
void FUN_0040bba5(undefined8 uParm1,uint uParm2);
ulong FUN_0040bbf7(void);
ulong FUN_0040bf89(undefined auParm1 [16],undefined8 uParm2,undefined8 uParm3);
ulong FUN_0040c361(char *pcParm1,long lParm2);
undefined8 FUN_0040c3ba(long param_1,long param_2,undefined param_3,int param_4,undefined8 param_5,ulong param_6,int param_7,ulong param_8,int param_9);
ulong FUN_00413ae7(undefined8 uParm1,undefined8 uParm2,undefined8 uParm3);
ulong FUN_00413c08(uint *puParm1,long lParm2);
void FUN_00413c7e(uint uParm1);
ulong FUN_00413cc6(byte *pbParm1,byte *pbParm2);
undefined8 FUN_00413d3d(undefined8 uParm1);
undefined8 FUN_00413dc8(void);
ulong FUN_00413dd5(uint uParm1);
undefined1 * FUN_00413ea6(void);
char * FUN_00414259(void);
void FUN_00414327(undefined8 uParm1,int *piParm2,undefined8 uParm3);
void FUN_004143c8(int *param_1);
undefined8 FUN_00414487(undefined8 uParm1);
undefined8 FUN_0041454d(uint uParm1,undefined8 uParm2);
ulong FUN_00414766(ulong uParm1,long lParm2);
void FUN_0041479a(undefined8 uParm1,undefined8 uParm2,undefined8 uParm3);
void FUN_004147d5(undefined8 uParm1,undefined8 uParm2,undefined8 uParm3,undefined8 uParm4);
ulong FUN_00414826(ulong uParm1,ulong uParm2);
ulong FUN_00414841(undefined8 uParm1);
void FUN_004148f9(undefined8 uParm1);
void FUN_0041491d(void);
ulong FUN_0041492b(long lParm1);
undefined8 FUN_004149fb(undefined8 uParm1);
void FUN_00414a1a(undefined8 uParm1,undefined8 uParm2,uint uParm3);
undefined8 FUN_00414a45(uint *puParm1,ulong *puParm2);
undefined8 FUN_004151e5(char *pcParm1,ulong *puParm2,char **ppcParm3);
ulong FUN_0041649b(undefined auParm1 [16]);
ulong FUN_004164d6(void);
void FUN_00416510(void);
undefined8 _DT_FINI(void);
undefined FUN_0061e000();
undefined FUN_0061e008();
undefined FUN_0061e010();
undefined FUN_0061e018();
undefined FUN_0061e020();
undefined FUN_0061e028();
undefined FUN_0061e030();
undefined FUN_0061e038();
undefined FUN_0061e040();
undefined FUN_0061e048();
undefined FUN_0061e050();
undefined FUN_0061e058();
undefined FUN_0061e060();
undefined FUN_0061e068();
undefined FUN_0061e070();
undefined FUN_0061e078();
undefined FUN_0061e080();
undefined FUN_0061e088();
undefined FUN_0061e090();
undefined FUN_0061e098();
undefined FUN_0061e0a0();
undefined FUN_0061e0a8();
undefined FUN_0061e0b0();
undefined FUN_0061e0b8();
undefined FUN_0061e0c0();
undefined FUN_0061e0c8();
undefined FUN_0061e0d0();
undefined FUN_0061e0d8();
undefined FUN_0061e0e0();
undefined FUN_0061e0e8();
undefined FUN_0061e0f0();
undefined FUN_0061e0f8();
undefined FUN_0061e100();
undefined FUN_0061e108();
undefined FUN_0061e110();
undefined FUN_0061e118();
undefined FUN_0061e120();
undefined FUN_0061e128();
undefined FUN_0061e130();
undefined FUN_0061e138();
undefined FUN_0061e140();
undefined FUN_0061e148();
undefined FUN_0061e150();
undefined FUN_0061e158();
undefined FUN_0061e160();
undefined FUN_0061e168();
undefined FUN_0061e170();
undefined FUN_0061e178();
undefined FUN_0061e180();
undefined FUN_0061e188();
undefined FUN_0061e190();
undefined FUN_0061e198();
undefined FUN_0061e1a0();
undefined FUN_0061e1a8();
undefined FUN_0061e1b0();
undefined FUN_0061e1b8();
undefined FUN_0061e1c0();
undefined FUN_0061e1c8();
undefined FUN_0061e1d0();
undefined FUN_0061e1d8();
undefined FUN_0061e1e0();
undefined FUN_0061e1e8();
undefined FUN_0061e1f0();
undefined FUN_0061e1f8();
undefined FUN_0061e200();
undefined FUN_0061e208();
undefined FUN_0061e210();
undefined FUN_0061e218();
undefined FUN_0061e220();
undefined FUN_0061e228();
undefined FUN_0061e230();
undefined FUN_0061e238();
undefined FUN_0061e240();
undefined FUN_0061e248();
undefined FUN_0061e250();
undefined FUN_0061e258();
undefined FUN_0061e260();
undefined FUN_0061e268();
undefined FUN_0061e270();
undefined FUN_0061e278();
undefined FUN_0061e280();


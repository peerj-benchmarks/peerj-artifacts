
#include "numfmt.h"

long null_ARRAY_0061d4b_0_8_;
long null_ARRAY_0061d4b_8_8_;
long null_ARRAY_0061d68_0_8_;
long null_ARRAY_0061d68_16_8_;
long null_ARRAY_0061d68_24_8_;
long null_ARRAY_0061d68_32_8_;
long null_ARRAY_0061d68_40_8_;
long null_ARRAY_0061d68_48_8_;
long null_ARRAY_0061d68_8_8_;
long null_ARRAY_0061d7c_0_4_;
long null_ARRAY_0061d7c_16_8_;
long null_ARRAY_0061d7c_4_4_;
long null_ARRAY_0061d7c_8_4_;
long DAT_004165cb;
long DAT_00416685;
long DAT_00416703;
long DAT_00416f18;
long DAT_00416f8e;
long DAT_00417020;
long DAT_00418134;
long DAT_00418230;
long DAT_00418234;
long DAT_0041846e;
long DAT_00418470;
long DAT_0041847b;
long DAT_004186a0;
long DAT_00418892;
long DAT_004188aa;
long DAT_004188f0;
long DAT_00418a1e;
long DAT_00418a22;
long DAT_00418a27;
long DAT_00418a29;
long DAT_00419093;
long DAT_00419460;
long DAT_00419b60;
long DAT_00419b65;
long DAT_00419bcf;
long DAT_00419c60;
long DAT_00419c63;
long DAT_00419cb1;
long DAT_0041a2d6;
long DAT_0041a348;
long DAT_0041a349;
long DAT_0041a38e;
long DAT_0041a399;
long DAT_0061d000;
long DAT_0061d010;
long DAT_0061d020;
long DAT_0061d440;
long DAT_0061d448;
long DAT_0061d450;
long DAT_0061d458;
long DAT_0061d460;
long DAT_0061d464;
long DAT_0061d468;
long DAT_0061d46c;
long DAT_0061d488;
long DAT_0061d4a0;
long DAT_0061d518;
long DAT_0061d51c;
long DAT_0061d520;
long DAT_0061d540;
long DAT_0061d548;
long DAT_0061d550;
long DAT_0061d560;
long DAT_0061d568;
long DAT_0061d580;
long DAT_0061d588;
long DAT_0061d5d0;
long DAT_0061d5d4;
long DAT_0061d5d8;
long DAT_0061d5e0;
long DAT_0061d5e8;
long DAT_0061d5f0;
long DAT_0061d5f8;
long DAT_0061d600;
long DAT_0061d608;
long DAT_0061d610;
long DAT_0061d618;
long DAT_0061d620;
long DAT_0061d628;
long DAT_0061d630;
long DAT_0061d638;
long DAT_0061d640;
long DAT_0061d648;
long DAT_0061d64c;
long DAT_0061d650;
long DAT_0061d658;
long DAT_0061d660;
long DAT_0061d668;
long DAT_0061d670;
long DAT_0061d7f8;
long DAT_0061d800;
long DAT_0061d808;
long DAT_0061d810;
long DAT_0061d818;
long DAT_0061d828;
long fde_0041b688;
long FLOAT_UNKNOWN;
long null_ARRAY_00416760;
long null_ARRAY_00416790;
long null_ARRAY_004167c0;
long null_ARRAY_004167f0;
long null_ARRAY_00416840;
long null_ARRAY_00416870;
long null_ARRAY_004168a0;
long null_ARRAY_004168d0;
long null_ARRAY_00416980;
long null_ARRAY_0041a0e0;
long null_ARRAY_0041a3c0;
long null_ARRAY_0041ad40;
long null_ARRAY_0061d4b0;
long null_ARRAY_0061d4e0;
long null_ARRAY_0061d5a0;
long null_ARRAY_0061d680;
long null_ARRAY_0061d6c0;
long null_ARRAY_0061d7c0;
long PTR_DAT_0061d478;
long PTR_FUN_0061d480;
long PTR_null_ARRAY_0061d4c0;
long PTR_s_KMGTPEZY_0061d470;
long stack0x00000008;
void
FUN_00403267 (uint uParm1)
{
  int iVar1;
  undefined4 *puVar2;
  int *piVar3;
  undefined8 uVar4;
  long lVar5;
  undefined *puVar6;
  undefined8 uVar7;
  undefined8 uVar8;
  ulong uVar9;
  char *pcVar10;
  long lStack136;
  long lStack128;
  long lStack120;
  long lStack112;
  char cStack97;
  long lStack96;
  long lStack88;
  code *pcStack32;
  uint local_c;

  if (uParm1 == 0)
    {
      pcStack32 = (code *) 0x4032b6;
      local_c = uParm1;
      func_0x00401820 ("Usage: %s [OPTION]... [NUMBER]...\n", DAT_0061d670);
      pcStack32 = (code *) 0x4032ca;
      func_0x00401a30
	("Reformat NUMBER(s), or the numbers from standard input if none are specified.\n",
	 DAT_0061d540);
      pcStack32 = (code *) 0x4032cf;
      FUN_00401ed0 ();
      pcStack32 = (code *) 0x4032e3;
      func_0x00401a30
	("      --debug          print warnings about invalid input\n",
	 DAT_0061d540);
      pcStack32 = (code *) 0x4032f7;
      func_0x00401a30
	("  -d, --delimiter=X    use X instead of whitespace for field delimiter\n",
	 DAT_0061d540);
      pcStack32 = (code *) 0x40330b;
      func_0x00401a30
	("      --field=FIELDS   replace the numbers in these input fields (default=1)\n                         see FIELDS below\n",
	 DAT_0061d540);
      pcStack32 = (code *) 0x40331f;
      func_0x00401a30
	("      --format=FORMAT  use printf style floating-point FORMAT;\n                         see FORMAT below for details\n",
	 DAT_0061d540);
      pcStack32 = (code *) 0x403333;
      func_0x00401a30
	("      --from=UNIT      auto-scale input numbers to UNITs; default is \'none\';\n                         see UNIT below\n",
	 DAT_0061d540);
      pcStack32 = (code *) 0x403347;
      func_0x00401a30
	("      --from-unit=N    specify the input unit size (instead of the default 1)\n",
	 DAT_0061d540);
      pcStack32 = (code *) 0x40335b;
      func_0x00401a30
	("      --grouping       use locale-defined grouping of digits, e.g. 1,000,000\n                         (which means it has no effect in the C/POSIX locale)\n",
	 DAT_0061d540);
      pcStack32 = (code *) 0x40336f;
      func_0x00401a30
	("      --header[=N]     print (without converting) the first N header lines;\n                         N defaults to 1 if not specified\n",
	 DAT_0061d540);
      pcStack32 = (code *) 0x403383;
      func_0x00401a30
	("      --invalid=MODE   failure mode for invalid numbers: MODE can be:\n                         abort (default), fail, warn, ignore\n",
	 DAT_0061d540);
      pcStack32 = (code *) 0x403397;
      func_0x00401a30
	("      --padding=N      pad the output to N characters; positive N will\n                         right-align; negative N will left-align;\n                         padding is ignored if the output is wider than N;\n                         the default is to automatically pad if a whitespace\n                         is found\n",
	 DAT_0061d540);
      pcStack32 = (code *) 0x4033ab;
      func_0x00401a30
	("      --round=METHOD   use METHOD for rounding when scaling; METHOD can be:\n                         up, down, from-zero (default), towards-zero, nearest\n",
	 DAT_0061d540);
      pcStack32 = (code *) 0x4033bf;
      func_0x00401a30
	("      --suffix=SUFFIX  add SUFFIX to output numbers, and accept optional\n                         SUFFIX in input numbers\n",
	 DAT_0061d540);
      pcStack32 = (code *) 0x4033d3;
      func_0x00401a30
	("      --to=UNIT        auto-scale output numbers to UNITs; see UNIT below\n",
	 DAT_0061d540);
      pcStack32 = (code *) 0x4033e7;
      func_0x00401a30
	("      --to-unit=N      the output unit size (instead of the default 1)\n",
	 DAT_0061d540);
      pcStack32 = (code *) 0x4033fb;
      func_0x00401a30
	("  -z, --zero-terminated    line delimiter is NUL, not newline\n",
	 DAT_0061d540);
      pcStack32 = (code *) 0x40340f;
      func_0x00401a30 ("      --help     display this help and exit\n",
		       DAT_0061d540);
      pcStack32 = (code *) 0x403423;
      func_0x00401a30
	("      --version  output version information and exit\n",
	 DAT_0061d540);
      pcStack32 = (code *) 0x403437;
      func_0x00401a30 ("\nUNIT options:\n", DAT_0061d540);
      pcStack32 = (code *) 0x40344b;
      func_0x00401a30
	("  none       no auto-scaling is done; suffixes will trigger an error\n",
	 DAT_0061d540);
      pcStack32 = (code *) 0x40345f;
      func_0x00401a30
	("  auto       accept optional single/two letter suffix:\n               1K = 1000,\n               1Ki = 1024,\n               1M = 1000000,\n               1Mi = 1048576,\n",
	 DAT_0061d540);
      pcStack32 = (code *) 0x403473;
      func_0x00401a30
	("  si         accept optional single letter suffix:\n               1K = 1000,\n               1M = 1000000,\n               ...\n",
	 DAT_0061d540);
      pcStack32 = (code *) 0x403487;
      func_0x00401a30
	("  iec        accept optional single letter suffix:\n               1K = 1024,\n               1M = 1048576,\n               ...\n",
	 DAT_0061d540);
      pcStack32 = (code *) 0x40349b;
      func_0x00401a30
	("  iec-i      accept optional two-letter suffix:\n               1Ki = 1024,\n               1Mi = 1048576,\n               ...\n",
	 DAT_0061d540);
      pcStack32 = (code *) 0x4034af;
      func_0x00401a30
	("\nFIELDS supports cut(1) style field ranges:\n  N    N\'th field, counted from 1\n  N-   from N\'th field, to end of line\n  N-M  from N\'th to M\'th field (inclusive)\n  -M   from first to M\'th field (inclusive)\n  -    all fields\nMultiple fields/ranges can be separated with commas\n",
	 DAT_0061d540);
      pcStack32 = (code *) 0x4034c3;
      func_0x00401a30
	("\nFORMAT must be suitable for printing one floating-point argument \'%f\'.\nOptional quote (%\'f) will enable --grouping (if supported by current locale).\nOptional width value (%10f) will pad output. Optional zero (%010f) width\nwill zero pad the number. Optional negative values (%-10f) will left align.\nOptional precision (%.1f) will override the input determined precision.\n",
	 DAT_0061d540);
      pcStack32 = (code *) 0x4034dc;
      func_0x00401820
	("\nExit status is 0 if all input numbers were successfully converted.\nBy default, %s will stop at the first conversion error with exit status 2.\nWith --invalid=\'fail\' a warning is printed for each conversion error\nand the exit status is 2.  With --invalid=\'warn\' each conversion error is\ndiagnosed, but the exit status is 0.  With --invalid=\'ignore\' conversion\nerrors are not diagnosed and the exit status is 0.\n",
	 DAT_0061d670);
      pcStack32 = (code *) DAT_0061d670;
      func_0x00401820
	("\nExamples:\n  $ %s --to=si 1000\n            -> \"1.0K\"\n  $ %s --to=iec 2048\n           -> \"2.0K\"\n  $ %s --to=iec-i 4096\n           -> \"4.0Ki\"\n  $ echo 1K | %s --from=si\n           -> \"1000\"\n  $ echo 1K | %s --from=iec\n           -> \"1024\"\n  $ df -B1 | %s --header --field 2-4 --to=si\n  $ ls -l  | %s --header --field 5 --to=iec\n  $ ls -lh | %s --header --field 5 --from=iec --padding=10\n  $ ls -lh | %s --header --field 5 --from=iec --format %%10f\n",
	 DAT_0061d670, DAT_0061d670, DAT_0061d670, DAT_0061d670,
	 DAT_0061d670);
      pcStack32 = (code *) 0x403547;
      imperfection_wrapper ();	//    FUN_00401eea();
    }
  else
    {
      pcStack32 = (code *) 0x403298;
      local_c = uParm1;
      func_0x00401a20 (DAT_0061d560,
		       "Try \'%s --help\' for more information.\n",
		       DAT_0061d670);
    }
  uVar9 = (ulong) local_c;
  pcStack32 = FUN_00403551;
  func_0x00401be0 ();
  lStack96 = 0;
  lStack112 = 0;
  lStack136 = 0;
  cStack97 = '\0';
  lStack88 = 0;
  pcStack32 = (code *) & stack0xfffffffffffffff8;
  while ((*(char *) (lStack88 + uVar9) != '%'
	  || (*(char *) (uVar9 + lStack88 + 1) == '%')))
    {
      if (*(char *) (lStack88 + uVar9) == '\0')
	{
	  imperfection_wrapper ();	//      uVar4 = FUN_00407442(uVar9);
	  imperfection_wrapper ();	//      FUN_00408706(1, 0, "format %s has no %% directive", uVar4);
	}
      lStack96 = lStack96 + 1;
      if (*(char *) (lStack88 + uVar9) == '%')
	{
	  lVar5 = 2;
	}
      else
	{
	  lVar5 = 1;
	}
      lStack88 = lStack88 + lVar5;
    }
  lStack88 = lStack88 + 1;
  do
    {
      while (true)
	{
	  while (true)
	    {
	      lStack120 =
		func_0x00401c30 (lStack88 + uVar9, &DAT_00418134, uVar9);
	      lStack88 = lStack88 + lStack120;
	      if (*(char *) (lStack88 + uVar9) != '\'')
		break;
	      DAT_0061d5e8 = 1;
	      lStack88 = lStack88 + 1;
	    }
	  if (*(char *) (lStack88 + uVar9) != '0')
	    break;
	  cStack97 = '\x01';
	  lStack88 = lStack88 + 1;
	}
    }
  while (lStack120 != 0);
  puVar2 = (undefined4 *) func_0x00401bd0 ();
  *puVar2 = 0;
  lStack112 =
    func_0x004019d0 (uVar9 + lStack88, &lStack136, 10, uVar9 + lStack88);
  piVar3 = (int *) func_0x00401bd0 ();
  if (*piVar3 == 0x22)
    {
      imperfection_wrapper ();	//    uVar4 = FUN_00407442(uVar9);
      imperfection_wrapper ();	//    FUN_00408706(1, 0, "invalid format %s (width overflow)", uVar4);
    }
  if ((uVar9 + lStack88 != lStack136) && (lStack112 != 0))
    {
      if (((DAT_0061d638 != '\0') && (DAT_0061d600 != 0))
	  && ((cStack97 != '\x01' || (lStack112 < 1))))
	{
	  imperfection_wrapper ();	//      FUN_00408706(0, 0, "--format padding overriding --padding");
	}
      if (lStack112 < 0)
	{
	  DAT_0061d464 = 0;
	  DAT_0061d600 = -lStack112;
	}
      else
	{
	  if (cStack97 == '\0')
	    {
	      DAT_0061d600 = lStack112;
	    }
	  else
	    {
	      DAT_0061d608 = lStack112;
	    }
	}
    }
  lStack88 = lStack136 - uVar9;
  if (*(char *) (lStack88 + uVar9) == '\0')
    {
      imperfection_wrapper ();	//    uVar4 = FUN_00407442(uVar9);
      imperfection_wrapper ();	//    FUN_00408706(1, 0, "format %s ends in %%", uVar4);
    }
  if (*(char *) (lStack88 + uVar9) == '.')
    {
      lStack88 = lStack88 + 1;
      puVar2 = (undefined4 *) func_0x00401bd0 ();
      *puVar2 = 0;
      DAT_0061d458 =
	func_0x004019d0 (uVar9 + lStack88, &lStack136, 10, uVar9 + lStack88);
      piVar3 = (int *) func_0x00401bd0 ();
      if ((((*piVar3 == 0x22) || (DAT_0061d458 < 0))
	   || (iVar1 =
	       func_0x004018c0 ((ulong) (uint) (int)
				*(char *) (lStack88 + uVar9)), iVar1 != 0))
	  || (*(char *) (lStack88 + uVar9) == '+'))
	{
	  imperfection_wrapper ();	//      uVar4 = FUN_00407442(uVar9);
	  imperfection_wrapper ();	//      FUN_00408706(1, 0, "invalid precision in format %s", uVar4);
	}
      lStack88 = lStack136 - uVar9;
    }
  if (*(char *) (lStack88 + uVar9) != 'f')
    {
      imperfection_wrapper ();	//    uVar4 = FUN_00407442(uVar9);
      imperfection_wrapper ();	//    FUN_00408706(1, 0, "invalid format %s, directive must be %%[0][\'][-][N][.][N]f", uVar4);
    }
  lStack128 = lStack88 + 1;
  lStack88 = lStack128;
  while (*(char *) (lStack88 + uVar9) != '\0')
    {
      if ((*(char *) (lStack88 + uVar9) == '%')
	  && (*(char *) (uVar9 + lStack88 + 1) != '%'))
	{
	  imperfection_wrapper ();	//      uVar4 = FUN_00407442(uVar9);
	  imperfection_wrapper ();	//      FUN_00408706(1, 0, "format %s has too many %% directives", uVar4);
	}
      if (*(char *) (lStack88 + uVar9) == '%')
	{
	  lVar5 = 2;
	}
      else
	{
	  lVar5 = 1;
	}
      lStack88 = lStack88 + lVar5;
    }
  if (lStack96 != 0)
    {
      imperfection_wrapper ();	//    DAT_0061d618 = (undefined *)FUN_00407c53(uVar9, lStack96, lStack96);
    }
  if (*(char *) (lStack128 + uVar9) != '\0')
    {
      imperfection_wrapper ();	//    DAT_0061d620 = (undefined *)FUN_00407bfc(lStack128 + uVar9);
    }
  if (DAT_0061d64c != '\0')
    {
      puVar6 = DAT_0061d620;
      if (DAT_0061d620 == (undefined *) 0x0)
	{
	  puVar6 = &DAT_00416703;
	}
      imperfection_wrapper ();	//    uVar4 = FUN_00407419(2, puVar6);
      puVar6 = DAT_0061d618;
      if (DAT_0061d618 == (undefined *) 0x0)
	{
	  puVar6 = &DAT_00416703;
	}
      imperfection_wrapper ();	//    uVar7 = FUN_00407419(1, puVar6);
      lVar5 = DAT_0061d600;
      if (DAT_0061d464 == 0)
	{
	  pcVar10 = "Left";
	}
      else
	{
	  pcVar10 = "Right";
	}
      if (DAT_0061d5e8 == 0)
	{
	  puVar6 = &DAT_00418234;
	}
      else
	{
	  puVar6 = &DAT_00418230;
	}
      imperfection_wrapper ();	//    uVar8 = FUN_00407419(0, uVar9);
      func_0x00401a20 (DAT_0061d560,
		       "format String:\n  input: %s\n  grouping: %s\n  padding width: %ld\n  alignment: %s\n  prefix: %s\n  suffix: %s\n",
		       uVar8, puVar6, lVar5, pcVar10, uVar7, uVar4);
    }
  return;
}


#include "pwd.h"

long null_ARRAY_0061745_0_8_;
long null_ARRAY_0061745_8_8_;
long null_ARRAY_006175c_0_8_;
long null_ARRAY_006175c_16_8_;
long null_ARRAY_006175c_24_8_;
long null_ARRAY_006175c_32_8_;
long null_ARRAY_006175c_40_8_;
long null_ARRAY_006175c_48_8_;
long null_ARRAY_006175c_8_8_;
long null_ARRAY_0061770_0_4_;
long null_ARRAY_0061770_16_8_;
long null_ARRAY_0061770_4_4_;
long null_ARRAY_0061770_8_4_;
long DAT_00413b80;
long DAT_00413c3d;
long DAT_00413cbb;
long DAT_00413f64;
long DAT_00414027;
long DAT_0041402b;
long DAT_0041405d;
long DAT_004140c1;
long DAT_004140e7;
long DAT_004140eb;
long DAT_004140fe;
long DAT_00414144;
long DAT_00414188;
long DAT_004142de;
long DAT_004142e2;
long DAT_004142e7;
long DAT_004142e9;
long DAT_00414940;
long DAT_0041495b;
long DAT_00414d20;
long DAT_00414d38;
long DAT_00414d3d;
long DAT_00414d50;
long DAT_00414d52;
long DAT_00414d54;
long DAT_00414daf;
long DAT_00414e40;
long DAT_00414e43;
long DAT_00414e91;
long DAT_00414e95;
long DAT_00414f08;
long DAT_00414f09;
long DAT_00617000;
long DAT_00617010;
long DAT_00617020;
long DAT_00617428;
long DAT_00617440;
long DAT_006174b8;
long DAT_006174bc;
long DAT_006174c0;
long DAT_00617500;
long DAT_00617510;
long DAT_00617520;
long DAT_00617528;
long DAT_00617540;
long DAT_00617548;
long DAT_00617590;
long DAT_00617598;
long DAT_006175a0;
long DAT_00617738;
long DAT_00617740;
long DAT_00617748;
long DAT_00617750;
long DAT_00617758;
long DAT_00617768;
long fde_00415c70;
long FLOAT_UNKNOWN;
long null_ARRAY_00413d40;
long null_ARRAY_00415340;
long null_ARRAY_00617450;
long null_ARRAY_00617480;
long null_ARRAY_00617560;
long null_ARRAY_006175c0;
long null_ARRAY_00617600;
long null_ARRAY_00617700;
long PTR_DAT_00617420;
long PTR_null_ARRAY_00617460;
void
FUN_0040201d (uint uParm1)
{
  undefined8 *puVar1;

  if (uParm1 == 0)
    {
      func_0x00401780 ("Usage: %s [OPTION]...\n", DAT_006175a0);
      func_0x00401970
	("Print the full filename of the current working directory.\n\n",
	 DAT_00617500);
      func_0x00401970
	("  -L, --logical   use PWD from environment, even if it contains symlinks\n  -P, --physical  avoid all symlinks\n",
	 DAT_00617500);
      func_0x00401970 ("      --help     display this help and exit\n",
		       DAT_00617500);
      func_0x00401970
	("      --version  output version information and exit\n",
	 DAT_00617500);
      func_0x00401970 ("\nIf no option is specified, -P is assumed.\n",
		       DAT_00617500);
      func_0x00401780
	("\nNOTE: your shell may have its own version of %s, which usually supersedes\nthe version described here.  Please refer to your shell\'s documentation\nfor details about the options it supports.\n",
	 &DAT_00413f64);
      imperfection_wrapper ();	//    FUN_00401e81();
    }
  else
    {
      func_0x00401960 (DAT_00617520,
		       "Try \'%s --help\' for more information.\n",
		       DAT_006175a0);
    }
  puVar1 = (undefined8 *) (ulong) uParm1;
  func_0x00401b30 ();
  func_0x00401c40 (*puVar1);
  func_0x00401c40 (puVar1);
  return;
}

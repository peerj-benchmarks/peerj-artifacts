typedef unsigned char   undefined;

typedef unsigned char    byte;
typedef unsigned char    dwfenc;
typedef unsigned int    dword;
typedef unsigned long    qword;
typedef unsigned char    undefined1;
typedef unsigned short    undefined2;
typedef unsigned int    undefined4;
typedef unsigned long    undefined6;
typedef unsigned long    undefined8;
typedef unsigned short    word;
#define unkbyte9   unsigned long long
#define unkbyte10   unsigned long long
#define unkbyte11   unsigned long long
#define unkbyte12   unsigned long long
#define unkbyte13   unsigned long long
#define unkbyte14   unsigned long long
#define unkbyte15   unsigned long long
#define unkbyte16   unsigned long long

#define unkuint9   unsigned long long
#define unkuint10   unsigned long long
#define unkuint11   unsigned long long
#define unkuint12   unsigned long long
#define unkuint13   unsigned long long
#define unkuint14   unsigned long long
#define unkuint15   unsigned long long
#define unkuint16   unsigned long long

#define unkint9   long long
#define unkint10   long long
#define unkint11   long long
#define unkint12   long long
#define unkint13   long long
#define unkint14   long long
#define unkint15   long long
#define unkint16   long long

#define unkfloat1   float
#define unkfloat2   float
#define unkfloat3   float
#define unkfloat5   double
#define unkfloat6   double
#define unkfloat7   double
#define unkfloat9   long double
#define unkfloat11   long double
#define unkfloat12   long double
#define unkfloat13   long double
#define unkfloat14   long double
#define unkfloat15   long double
#define unkfloat16   long double

#define BADSPACEBASE   void
#define code   void

#define ulong long
#define true 1
#define bool int
#define false 0
#define float10 int
#define ushort int
#define undefined7 long
#define undefined2 long
#define uint5 long
#define uint6 long

// Add definition for uint
typedef unsigned int    uint;

#define sbyte char

extern imperfection_wrapper(void);
typedef struct eh_frame_hdr eh_frame_hdr, *Peh_frame_hdr;

struct eh_frame_hdr {
    byte eh_frame_hdr_version; // Exception Handler Frame Header Version
    dwfenc eh_frame_pointer_encoding; // Exception Handler Frame Pointer Encoding
    dwfenc eh_frame_desc_entry_count_encoding; // Encoding of # of Exception Handler FDEs
    dwfenc eh_frame_table_encoding; // Exception Handler Table Encoding
};

typedef struct fde_table_entry fde_table_entry, *Pfde_table_entry;

struct fde_table_entry {
    dword initial_loc; // Initial Location
    dword data_loc; // Data location
};

typedef enum Elf64_DynTag {
    DT_AUDIT=1879047932,
    DT_AUXILIARY=2147483645,
    DT_BIND_NOW=24,
    DT_CHECKSUM=1879047672,
    DT_CONFIG=1879047930,
    DT_DEBUG=21,
    DT_DEPAUDIT=1879047931,
    DT_ENCODING=32,
    DT_FEATURE_1=1879047676,
    DT_FILTER=2147483647,
    DT_FINI=13,
    DT_FINI_ARRAY=26,
    DT_FINI_ARRAYSZ=28,
    DT_FLAGS=30,
    DT_FLAGS_1=1879048187,
    DT_GNU_CONFLICT=1879047928,
    DT_GNU_CONFLICTSZ=1879047670,
    DT_GNU_HASH=1879047925,
    DT_GNU_LIBLIST=1879047929,
    DT_GNU_LIBLISTSZ=1879047671,
    DT_GNU_PRELINKED=1879047669,
    DT_HASH=4,
    DT_INIT=12,
    DT_INIT_ARRAY=25,
    DT_INIT_ARRAYSZ=27,
    DT_JMPREL=23,
    DT_MOVEENT=1879047674,
    DT_MOVESZ=1879047675,
    DT_MOVETAB=1879047934,
    DT_NEEDED=1,
    DT_NULL=0,
    DT_PLTGOT=3,
    DT_PLTPAD=1879047933,
    DT_PLTPADSZ=1879047673,
    DT_PLTREL=20,
    DT_PLTRELSZ=2,
    DT_POSFLAG_1=1879047677,
    DT_PREINIT_ARRAYSZ=33,
    DT_REL=17,
    DT_RELA=7,
    DT_RELACOUNT=1879048185,
    DT_RELAENT=9,
    DT_RELASZ=8,
    DT_RELCOUNT=1879048186,
    DT_RELENT=19,
    DT_RELSZ=18,
    DT_RPATH=15,
    DT_RUNPATH=29,
    DT_SONAME=14,
    DT_STRSZ=10,
    DT_STRTAB=5,
    DT_SYMBOLIC=16,
    DT_SYMENT=11,
    DT_SYMINENT=1879047679,
    DT_SYMINFO=1879047935,
    DT_SYMINSZ=1879047678,
    DT_SYMTAB=6,
    DT_TEXTREL=22,
    DT_TLSDESC_GOT=1879047927,
    DT_TLSDESC_PLT=1879047926,
    DT_VERDEF=1879048188,
    DT_VERDEFNUM=1879048189,
    DT_VERNEED=1879048190,
    DT_VERNEEDNUM=1879048191,
    DT_VERSYM=1879048176
} Elf64_DynTag;

typedef enum Elf_ProgramHeaderType {
    PT_DYNAMIC=2,
    PT_GNU_EH_FRAME=1685382480,
    PT_GNU_RELRO=1685382482,
    PT_GNU_STACK=1685382481,
    PT_INTERP=3,
    PT_LOAD=1,
    PT_NOTE=4,
    PT_NULL=0,
    PT_PHDR=6,
    PT_SHLIB=5,
    PT_TLS=7
} Elf_ProgramHeaderType;

typedef struct Elf64_Dyn Elf64_Dyn, *PElf64_Dyn;

struct Elf64_Dyn {
    enum Elf64_DynTag d_tag;
    qword d_val;
};

typedef struct Elf64_Shdr Elf64_Shdr, *PElf64_Shdr;

typedef enum Elf_SectionHeaderType {
    SHT_CHECKSUM=1879048184,
    SHT_DYNAMIC=6,
    SHT_DYNSYM=11,
    SHT_FINI_ARRAY=15,
    SHT_GNU_ATTRIBUTES=1879048181,
    SHT_GNU_HASH=1879048182,
    SHT_GNU_LIBLIST=1879048183,
    SHT_GNU_verdef=1879048189,
    SHT_GNU_verneed=1879048190,
    SHT_GNU_versym=1879048191,
    SHT_GROUP=17,
    SHT_HASH=5,
    SHT_INIT_ARRAY=14,
    SHT_NOBITS=8,
    SHT_NOTE=7,
    SHT_NULL=0,
    SHT_PREINIT_ARRAY=16,
    SHT_PROGBITS=1,
    SHT_REL=9,
    SHT_RELA=4,
    SHT_SHLIB=10,
    SHT_STRTAB=3,
    SHT_SUNW_COMDAT=1879048187,
    SHT_SUNW_move=1879048186,
    SHT_SUNW_syminfo=1879048188,
    SHT_SYMTAB=2,
    SHT_SYMTAB_SHNDX=18
} Elf_SectionHeaderType;

struct Elf64_Shdr {
    dword sh_name;
    enum Elf_SectionHeaderType sh_type;
    qword sh_flags;
    qword sh_addr;
    qword sh_offset;
    qword sh_size;
    dword sh_link;
    dword sh_info;
    qword sh_addralign;
    qword sh_entsize;
};

typedef struct Elf64_Phdr Elf64_Phdr, *PElf64_Phdr;

struct Elf64_Phdr {
    enum Elf_ProgramHeaderType p_type;
    dword p_flags;
    qword p_offset;
    qword p_vaddr;
    qword p_paddr;
    qword p_filesz;
    qword p_memsz;
    qword p_align;
};

typedef struct Elf64_Rela Elf64_Rela, *PElf64_Rela;

struct Elf64_Rela {
    qword r_offset; // location to apply the relocation action
    qword r_info; // the symbol table index and the type of relocation
    qword r_addend; // a constant addend used to compute the relocatable field value
};

typedef struct Elf64_Sym Elf64_Sym, *PElf64_Sym;

struct Elf64_Sym {
    dword st_name;
    byte st_info;
    byte st_other;
    word st_shndx;
    qword st_value;
    qword st_size;
};

typedef struct Elf64_Ehdr Elf64_Ehdr, *PElf64_Ehdr;

struct Elf64_Ehdr {
    byte e_ident_magic_num;
    char e_ident_magic_str[3];
    byte e_ident_class;
    byte e_ident_data;
    byte e_ident_version;
    byte e_ident_pad[9];
    word e_type;
    word e_machine;
    dword e_version;
    qword e_entry;
    qword e_phoff;
    qword e_shoff;
    dword e_flags;
    word e_ehsize;
    word e_phentsize;
    word e_phnum;
    word e_shentsize;
    word e_shnum;
    word e_shstrndx;
};




undefined8 _DT_INIT(void);
void FUN_00401750(void);
void FUN_00401a40(void);
void entry(void);
void FUN_00401c80(void);
void FUN_00401d00(void);
void FUN_00401d80(void);
undefined8 FUN_00401dbe(undefined8 uParm1);
long FUN_00401dcc(long lParm1,ulong uParm2);
void FUN_00401e12(void);
void FUN_00401e2c(undefined *puParm1);
undefined8 FUN_00401fc8(undefined8 uParm1);
undefined * FUN_00401fd6(uint uParm1);
undefined * FUN_004020fc(undefined *puParm1,ulong uParm2);
ulong FUN_004021ef(byte *pbParm1);
undefined8 FUN_00402246(long lParm1,long lParm2,long *plParm3,long *plParm4,char cParm5);
ulong FUN_0040239d(long lParm1,long lParm2,byte **ppbParm3,uint *puParm4,long *plParm5);
void FUN_004028bf(char *pcParm1,char cParm2);
undefined8 FUN_0040295b(undefined8 uParm1,undefined8 uParm2,undefined8 uParm3,undefined *puParm4);
ulong FUN_00402b3a(char *pcParm1);
ulong FUN_00403280(uint uParm1,undefined8 *puParm2);
undefined8 FUN_00403ae7(undefined8 *puParm1);
void FUN_00403b96(undefined *puParm1,undefined4 uParm2);
void FUN_00403be6(undefined *puParm1,undefined8 uParm2);
ulong FUN_00403c86(ulong uParm1,undefined4 uParm2);
void FUN_00403ca2(undefined8 uParm1,undefined8 uParm2);
void FUN_00403ccd(long lParm1);
ulong FUN_00403ce3(long lParm1);
void FUN_00403cfe(long lParm1);
void FUN_00403d33(long lParm1,ulong uParm2);
void FUN_00403d7d(long lParm1);
undefined8 FUN_00403dcf(long lParm1,byte *pbParm2);
undefined8 FUN_00403e5e(undefined8 uParm1,ulong uParm2);
void FUN_00403f43(long lParm1,long lParm2);
undefined8 FUN_0040ac2b(long lParm1,long lParm2,ulong uParm3);
undefined8 FUN_0040ad6e(long lParm1,long lParm2,ulong uParm3);
ulong FUN_0040aebd(undefined8 uParm1,undefined8 uParm2,undefined8 uParm3);
long FUN_0040b01d(undefined8 uParm1,long lParm2,long lParm3,long lParm4);
void FUN_0040b14c(void);
void FUN_0040b231(uint uParm1,undefined8 uParm2,undefined8 uParm3,uint uParm4);
void FUN_0040b261(long lParm1,uint uParm2);
long FUN_0040b29b(undefined8 uParm1,undefined8 uParm2);
void FUN_0040b387(long lParm1);
ulong FUN_0040b464(undefined1 *puParm1,byte bParm2,uint uParm3);
undefined8 * FUN_0040b4ec(undefined8 *puParm1,int iParm2);
char * FUN_0040b563(char *pcParm1,int iParm2);
ulong FUN_0040b603(long param_1,ulong param_2,long param_3,ulong param_4,uint param_5,uint param_6,long param_7,char *param_8,char *param_9);
undefined1 * FUN_0040c4cd(int iParm1,undefined8 uParm2,undefined8 uParm3,uint *puParm4);
void FUN_0040c740(undefined8 uParm1,undefined8 uParm2,char cParm3);
void FUN_0040c7d4(undefined8 uParm1,char cParm2);
void FUN_0040c7fe(undefined8 uParm1);
void FUN_0040c81d(uint uParm1,uint uParm2,undefined8 uParm3);
void FUN_0040c8b8(uint uParm1,undefined8 uParm2,undefined8 uParm3);
void FUN_0040c8e4(uint uParm1,undefined8 uParm2);
void FUN_0040c90d(undefined8 uParm1);
void FUN_0040c92c(uint uParm1);
void FUN_0040c952(undefined8 uParm1,long lParm2,undefined8 uParm3,undefined8 uParm4,undefined8 *puParm5,undefined8 uParm6);
void FUN_0040cdb6(undefined8 uParm1,undefined8 uParm2,undefined8 uParm3,undefined8 uParm4,uint *puParm5);
void FUN_0040ce88(undefined4 uParm1,undefined4 uParm2,undefined4 uParm3,undefined4 uParm4,undefined4 uParm5,undefined4 uParm6,undefined4 uParm7,undefined4 uParm8,undefined8 param_1,undefined8 param_2,undefined8 param_3,undefined8 param_4,undefined8 param_5,undefined8 param_6);
void FUN_0040cf3e(undefined8 uParm1);
long FUN_0040cf58(long lParm1);
long FUN_0040cf8d(long lParm1,long lParm2);
char * FUN_0040cfee(void);
ulong FUN_0040d018(undefined8 param_1,uint param_2,ulong param_3,ulong param_4,undefined8 param_5,undefined8 param_6,uint param_7);
void FUN_0040d12a(undefined8 uParm1,undefined8 uParm2,undefined8 uParm3,undefined8 uParm4,undefined8 uParm5,uint uParm6);
undefined8 FUN_0040d17f(int iParm1);
ulong FUN_0040d1a5(ulong *puParm1,int iParm2);
ulong FUN_0040d204(undefined8 uParm1,uint uParm2,int iParm3);
ulong FUN_0040d245(byte *pbParm1,byte **ppbParm2,uint uParm3,undefined8 *puParm4,long lParm5);
undefined8 FUN_0040d626(int iParm1);
ulong FUN_0040d64c(ulong *puParm1,int iParm2);
ulong FUN_0040d6ab(undefined8 uParm1,uint uParm2,int iParm3);
ulong FUN_0040d6ec(byte *pbParm1,byte **ppbParm2,uint uParm3,undefined8 *puParm4,long lParm5);
ulong FUN_0040dacd(uint uParm1);
void FUN_0040daf6(void);
void FUN_0040db2a(uint uParm1);
void FUN_0040db9e(uint uParm1,uint uParm2,undefined8 uParm3,undefined8 uParm4);
void FUN_0040dc22(undefined4 uParm1,undefined4 uParm2,undefined4 uParm3,undefined4 uParm4,undefined4 uParm5,undefined4 uParm6,undefined4 uParm7,undefined4 uParm8,uint param_1,uint param_2,undefined8 param_3,undefined8 param_4,undefined8 param_5,undefined8 param_6);
ulong FUN_0040dd06(undefined8 uParm1);
ulong FUN_0040ddbe(undefined4 uParm1,undefined4 uParm2,undefined4 uParm3,undefined4 uParm4,undefined4 uParm5,undefined4 uParm6,undefined4 uParm7,undefined4 uParm8,uint param_1,uint param_2,undefined8 param_3,undefined8 param_4,undefined8 param_5,undefined8 param_6);
void FUN_0040e075(undefined8 uParm1);
void FUN_0040e099(void);
ulong FUN_0040e0a7(long lParm1);
undefined8 FUN_0040e177(undefined8 uParm1);
void FUN_0040e196(undefined8 uParm1,undefined8 uParm2,uint uParm3);
void FUN_0040e1c1(undefined8 uParm1,undefined8 uParm2,undefined8 uParm3);
void FUN_0040e1f3(long lParm1,int *piParm2);
ulong FUN_0040e3d7(int param_1,undefined8 *param_2,char *param_3,long *param_4,int *param_5,int param_6,int *param_7,int param_8,undefined8 param_9);
char * FUN_0040e9c1(undefined8 uParm1,undefined8 uParm2,char *pcParm3,int *piParm4,int iParm5);
ulong FUN_0040ea8f(uint param_1,undefined8 *param_2,char *param_3,long param_4,undefined8 param_5,uint param_6,uint *param_7,uint param_8);
ulong FUN_0040f258(uint param_1,undefined8 param_2,undefined8 param_3,undefined8 param_4,undefined8 param_5,uint param_6,uint param_7);
void FUN_0040f2e8(uint uParm1,undefined8 uParm2,undefined8 uParm3,undefined8 uParm4,undefined8 uParm5);
undefined8 FUN_0040f332(long lParm1);
ulong FUN_0040f363(uint *puParm1,byte *pbParm2,long lParm3,undefined8 uParm4);
undefined8 FUN_0040f3e6(long lParm1,long lParm2);
ulong FUN_0040f44f(undefined8 uParm1,undefined8 uParm2,undefined8 uParm3);
ulong FUN_0040f570(byte *pbParm1,byte *pbParm2);
undefined8 FUN_0040f5e7(undefined8 uParm1);
undefined8 FUN_0040f672(void);
ulong FUN_0040f67f(uint uParm1);
undefined1 * FUN_0040f750(void);
char * FUN_0040fb03(void);
void FUN_0040fbd1(void);
long FUN_0040fbd7(long *plParm1,ulong *puParm2,int iParm3,long lParm4);
ulong FUN_0040fdb1(void);
long FUN_0040fdfe(ulong uParm1,long lParm2,ulong uParm3,long lParm4,long *plParm5);
int * FUN_0041004a(ulong uParm1,uint *puParm2,ulong uParm3,uint *puParm4,ulong *puParm5);
char * FUN_00410b2a(ulong uParm1,long lParm2,long lParm3);
long FUN_00410d98(int *param_1,long *param_2);
long FUN_00410f83(undefined auParm1 [16],int *piParm2,long *plParm3);
undefined8 FUN_00411342(int iParm1,long lParm2,uint *puParm3,long lParm4,uint uParm5);
void FUN_00411b55(uint param_1);
void FUN_00411b9f(undefined8 uParm1,uint uParm2);
ulong FUN_00411bf1(void);
ulong FUN_00411f83(undefined auParm1 [16],undefined8 uParm2,undefined8 uParm3);
ulong FUN_0041235b(char *pcParm1,long lParm2);
undefined8 FUN_004123b4(long param_1,long param_2,undefined param_3,int param_4,undefined8 param_5,ulong param_6,int param_7,ulong param_8,int param_9);
long FUN_00412677(long lParm1,char **ppcParm2,long lParm3,undefined8 uParm4);
ulong FUN_00419ae1(uint uParm1);
void FUN_00419b00(undefined8 uParm1,int *piParm2,undefined8 uParm3);
void FUN_00419ba1(int *param_1);
ulong FUN_00419c60(ulong uParm1,long lParm2);
void FUN_00419c94(undefined8 uParm1,undefined8 uParm2,undefined8 uParm3);
void FUN_00419ccf(undefined8 uParm1,undefined8 uParm2,undefined8 uParm3,undefined8 uParm4);
ulong FUN_00419d20(ulong uParm1,ulong uParm2);
undefined8 FUN_00419d3b(uint *puParm1,ulong *puParm2);
undefined8 FUN_0041a4db(char *pcParm1,ulong *puParm2,char **ppcParm3);
ulong FUN_0041b791(undefined auParm1 [16]);
ulong FUN_0041b7cc(void);
void FUN_0041b800(void);
undefined8 _DT_FINI(void);
undefined FUN_00621000();
undefined FUN_00621008();
undefined FUN_00621010();
undefined FUN_00621018();
undefined FUN_00621020();
undefined FUN_00621028();
undefined FUN_00621030();
undefined FUN_00621038();
undefined FUN_00621040();
undefined FUN_00621048();
undefined FUN_00621050();
undefined FUN_00621058();
undefined FUN_00621060();
undefined FUN_00621068();
undefined FUN_00621070();
undefined FUN_00621078();
undefined FUN_00621080();
undefined FUN_00621088();
undefined FUN_00621090();
undefined FUN_00621098();
undefined FUN_006210a0();
undefined FUN_006210a8();
undefined FUN_006210b0();
undefined FUN_006210b8();
undefined FUN_006210c0();
undefined FUN_006210c8();
undefined FUN_006210d0();
undefined FUN_006210d8();
undefined FUN_006210e0();
undefined FUN_006210e8();
undefined FUN_006210f0();
undefined FUN_006210f8();
undefined FUN_00621100();
undefined FUN_00621108();
undefined FUN_00621110();
undefined FUN_00621118();
undefined FUN_00621120();
undefined FUN_00621128();
undefined FUN_00621130();
undefined FUN_00621138();
undefined FUN_00621140();
undefined FUN_00621148();
undefined FUN_00621150();
undefined FUN_00621158();
undefined FUN_00621160();
undefined FUN_00621168();
undefined FUN_00621170();
undefined FUN_00621178();
undefined FUN_00621180();
undefined FUN_00621188();
undefined FUN_00621190();
undefined FUN_00621198();
undefined FUN_006211a0();
undefined FUN_006211a8();
undefined FUN_006211b0();
undefined FUN_006211b8();
undefined FUN_006211c0();
undefined FUN_006211c8();
undefined FUN_006211d0();
undefined FUN_006211d8();
undefined FUN_006211e0();
undefined FUN_006211e8();
undefined FUN_006211f0();
undefined FUN_006211f8();
undefined FUN_00621200();
undefined FUN_00621208();
undefined FUN_00621210();
undefined FUN_00621218();
undefined FUN_00621220();
undefined FUN_00621228();
undefined FUN_00621230();
undefined FUN_00621238();
undefined FUN_00621240();
undefined FUN_00621248();
undefined FUN_00621250();
undefined FUN_00621258();
undefined FUN_00621260();


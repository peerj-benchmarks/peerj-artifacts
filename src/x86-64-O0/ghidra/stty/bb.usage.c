
#include "stty.h"

long null_ARRAY_0061c45_0_8_;
long null_ARRAY_0061c45_8_8_;
long null_ARRAY_0061c6c_0_8_;
long null_ARRAY_0061c6c_16_8_;
long null_ARRAY_0061c6c_24_8_;
long null_ARRAY_0061c6c_32_8_;
long null_ARRAY_0061c6c_40_8_;
long null_ARRAY_0061c6c_48_8_;
long null_ARRAY_0061c6c_8_8_;
long null_ARRAY_0061c82_0_4_;
long null_ARRAY_0061c82_16_8_;
long null_ARRAY_0061c82_4_4_;
long null_ARRAY_0061c82_8_4_;
long DAT_0041534b;
long DAT_00415405;
long DAT_00415483;
long DAT_00415669;
long DAT_0041566e;
long DAT_00415671;
long DAT_00415674;
long DAT_00415680;
long DAT_004156a0;
long DAT_004156a5;
long DAT_004156a9;
long DAT_004161d9;
long DAT_004161dd;
long DAT_00416b9a;
long DAT_00416b9d;
long DAT_004183b5;
long DAT_004183f9;
long DAT_004183fe;
long DAT_0041840b;
long DAT_00418410;
long DAT_0041850d;
long DAT_00418567;
long DAT_00418625;
long DAT_00418693;
long DAT_00418ae9;
long DAT_00418b2e;
long DAT_00418b78;
long DAT_00418c9e;
long DAT_00418ca2;
long DAT_00418ca7;
long DAT_00418ca9;
long DAT_00419313;
long DAT_004196e0;
long DAT_00419de8;
long DAT_00419ded;
long DAT_00419e57;
long DAT_00419ee8;
long DAT_00419eeb;
long DAT_00419f39;
long DAT_00419f3d;
long DAT_00419fb0;
long DAT_00419fb1;
long DAT_0041a1a8;
long DAT_0061c000;
long DAT_0061c010;
long DAT_0061c020;
long DAT_0061c420;
long DAT_0061c430;
long DAT_0061c440;
long DAT_0061c4b8;
long DAT_0061c4bc;
long DAT_0061c4c0;
long DAT_0061c500;
long DAT_0061c510;
long DAT_0061c520;
long DAT_0061c528;
long DAT_0061c540;
long DAT_0061c548;
long DAT_0061c5a0;
long DAT_0061c5a4;
long DAT_0061c688;
long DAT_0061c690;
long DAT_0061c698;
long DAT_0061c800;
long DAT_0061c858;
long DAT_0061c860;
long DAT_0061c868;
long DAT_0061c878;
long fde_0041ad50;
long FLOAT_UNKNOWN;
long null_ARRAY_004156c0;
long null_ARRAY_00416200;
long null_ARRAY_00416400;
long null_ARRAY_00418780;
long null_ARRAY_0041a400;
long null_ARRAY_0061c450;
long null_ARRAY_0061c480;
long null_ARRAY_0061c560;
long null_ARRAY_0061c5c0;
long null_ARRAY_0061c600;
long null_ARRAY_0061c640;
long null_ARRAY_0061c67c;
long null_ARRAY_0061c6c0;
long null_ARRAY_0061c700;
long null_ARRAY_0061c820;
long PTR_DAT_0061c428;
long PTR_null_ARRAY_0061c460;
long stack0x00000008;
void
FUN_0040209c (int iParm1, undefined8 uParm2, undefined8 uParm3, int iParm4,
	      long lParm5, undefined * puParm6)
{
  bool bVar1;
  char cVar2;
  int iVar3;
  uint uVar4;
  undefined *puVar5;
  undefined8 uVar6;
  ulong uVar7;
  long extraout_RDX;
  char *pcVar8;
  char cVar9;
  bool bVar10;
  int iStack56;
  char cStack49;
  char *pcStack48;
  int iStack36;
  undefined4 uStack16;

  if (iParm1 == 0)
    {
      func_0x00401750
	("Usage: %s [-F DEVICE | --file=DEVICE] [SETTING]...\n  or:  %s [-F DEVICE | --file=DEVICE] [-a|--all]\n  or:  %s [-F DEVICE | --file=DEVICE] [-g|--save]\n",
	 DAT_0061c698, DAT_0061c698, DAT_0061c698);
      func_0x00401940 ("Print or change terminal characteristics.\n",
		       DAT_0061c500);
      FUN_00401d9d ();
      func_0x00401940
	("  -a, --all          print all current settings in human-readable form\n  -g, --save         print all current settings in a stty-readable form\n  -F, --file=DEVICE  open and use the specified DEVICE instead of stdin\n",
	 DAT_0061c500);
      func_0x00401940 ("      --help     display this help and exit\n",
		       DAT_0061c500);
      func_0x00401940
	("      --version  output version information and exit\n",
	 DAT_0061c500);
      func_0x00401940
	("\nOptional - before SETTING indicates negation.  An * marks non-POSIX\nsettings.  The underlying system defines which settings are available.\n",
	 DAT_0061c500);
      func_0x00401940 ("\nSpecial characters:\n", DAT_0061c500);
      func_0x00401940
	(" * discard CHAR  CHAR will toggle discarding of output\n",
	 DAT_0061c500);
      func_0x00401940
	("   eof CHAR      CHAR will send an end of file (terminate the input)\n   eol CHAR      CHAR will end the line\n",
	 DAT_0061c500);
      func_0x00401940
	(" * eol2 CHAR     alternate CHAR for ending the line\n",
	 DAT_0061c500);
      func_0x00401940
	("   erase CHAR    CHAR will erase the last character typed\n   intr CHAR     CHAR will send an interrupt signal\n   kill CHAR     CHAR will erase the current line\n",
	 DAT_0061c500);
      func_0x00401940
	(" * lnext CHAR    CHAR will enter the next character quoted\n",
	 DAT_0061c500);
      func_0x00401940 ("   quit CHAR     CHAR will send a quit signal\n",
		       DAT_0061c500);
      func_0x00401940 (" * rprnt CHAR    CHAR will redraw the current line\n",
		       DAT_0061c500);
      func_0x00401940
	("   start CHAR    CHAR will restart the output after stopping it\n   stop CHAR     CHAR will stop the output\n   susp CHAR     CHAR will send a terminal stop signal\n",
	 DAT_0061c500);
      func_0x00401940
	(" * swtch CHAR    CHAR will switch to a different shell layer\n",
	 DAT_0061c500);
      func_0x00401940
	(" * werase CHAR   CHAR will erase the last word typed\n",
	 DAT_0061c500);
      func_0x00401940
	("\nSpecial settings:\n   N             set the input and output speeds to N bauds\n",
	 DAT_0061c500);
      func_0x00401940
	(" * cols N        tell the kernel that the terminal has N columns\n * columns N     same as cols N\n",
	 DAT_0061c500);
      if (DAT_0061c420 == 1)
	{
	  puVar5 = &DAT_00416b9a;
	}
      else
	{
	  puVar5 = &DAT_00416b9d;
	}
      func_0x00401750
	(" * [-]drain      wait for transmission before applying settings (%s by default)\n",
	 puVar5);
      func_0x00401940 ("   ispeed N      set the input speed to N\n",
		       DAT_0061c500);
      func_0x00401940 (" * line N        use line discipline N\n",
		       DAT_0061c500);
      func_0x00401940
	("   min N         with -icanon, set N characters minimum for a completed read\n   ospeed N      set the output speed to N\n",
	 DAT_0061c500);
      func_0x00401940
	(" * rows N        tell the kernel that the terminal has N rows\n * size          print the number of rows and columns according to the kernel\n",
	 DAT_0061c500);
      func_0x00401940
	("   speed         print the terminal speed\n   time N        with -icanon, set read timeout of N tenths of a second\n",
	 DAT_0061c500);
      func_0x00401940
	("\nControl settings:\n   [-]clocal     disable modem control signals\n   [-]cread      allow input to be received\n",
	 DAT_0061c500);
      func_0x00401940 (" * [-]crtscts    enable RTS/CTS handshaking\n",
		       DAT_0061c500);
      func_0x00401940
	("   csN           set character size to N bits, N in [5..8]\n",
	 DAT_0061c500);
      func_0x00401940
	("   [-]cstopb     use two stop bits per character (one with \'-\')\n   [-]hup        send a hangup signal when the last process closes the tty\n   [-]hupcl      same as [-]hup\n   [-]parenb     generate parity bit in output and expect parity bit in input\n   [-]parodd     set odd parity (or even parity with \'-\')\n",
	 DAT_0061c500);
      func_0x00401940
	("\nInput settings:\n   [-]brkint     breaks cause an interrupt signal\n   [-]icrnl      translate carriage return to newline\n   [-]ignbrk     ignore break characters\n   [-]igncr      ignore carriage return\n   [-]ignpar     ignore characters with parity errors\n",
	 DAT_0061c500);
      func_0x00401940
	(" * [-]imaxbel    beep and do not flush a full input buffer on a character\n",
	 DAT_0061c500);
      func_0x00401940
	("   [-]inlcr      translate newline to carriage return\n   [-]inpck      enable input parity checking\n   [-]istrip     clear high (8th) bit of input characters\n",
	 DAT_0061c500);
      func_0x00401940
	(" * [-]iutf8      assume input characters are UTF-8 encoded\n",
	 DAT_0061c500);
      func_0x00401940
	(" * [-]iuclc      translate uppercase characters to lowercase\n",
	 DAT_0061c500);
      func_0x00401940
	(" * [-]ixany      let any character restart output, not only start character\n",
	 DAT_0061c500);
      func_0x00401940
	("   [-]ixoff      enable sending of start/stop characters\n   [-]ixon       enable XON/XOFF flow control\n   [-]parmrk     mark parity errors (with a 255-0-character sequence)\n   [-]tandem     same as [-]ixoff\n",
	 DAT_0061c500);
      func_0x00401940 ("\nOutput settings:\n", DAT_0061c500);
      func_0x00401940
	(" * bsN           backspace delay style, N in [0..1]\n",
	 DAT_0061c500);
      func_0x00401940
	(" * crN           carriage return delay style, N in [0..3]\n",
	 DAT_0061c500);
      func_0x00401940
	(" * ffN           form feed delay style, N in [0..1]\n",
	 DAT_0061c500);
      func_0x00401940 (" * nlN           newline delay style, N in [0..1]\n",
		       DAT_0061c500);
      func_0x00401940
	(" * [-]ocrnl      translate carriage return to newline\n",
	 DAT_0061c500);
      func_0x00401940
	(" * [-]ofdel      use delete characters for fill instead of NUL characters\n",
	 DAT_0061c500);
      func_0x00401940
	(" * [-]ofill      use fill (padding) characters instead of timing for delays\n",
	 DAT_0061c500);
      func_0x00401940
	(" * [-]olcuc      translate lowercase characters to uppercase\n",
	 DAT_0061c500);
      func_0x00401940
	(" * [-]onlcr      translate newline to carriage return-newline\n",
	 DAT_0061c500);
      func_0x00401940
	(" * [-]onlret     newline performs a carriage return\n",
	 DAT_0061c500);
      func_0x00401940
	(" * [-]onocr      do not print carriage returns in the first column\n",
	 DAT_0061c500);
      func_0x00401940 ("   [-]opost      postprocess output\n", DAT_0061c500);
      func_0x00401940
	(" * tabN          horizontal tab delay style, N in [0..3]\n * tabs          same as tab0\n * -tabs         same as tab3\n",
	 DAT_0061c500);
      func_0x00401940
	(" * vtN           vertical tab delay style, N in [0..1]\n",
	 DAT_0061c500);
      func_0x00401940
	("\nLocal settings:\n   [-]crterase   echo erase characters as backspace-space-backspace\n",
	 DAT_0061c500);
      func_0x00401940
	(" * crtkill       kill all line by obeying the echoprt and echoe settings\n * -crtkill      kill all line by obeying the echoctl and echok settings\n",
	 DAT_0061c500);
      func_0x00401940
	(" * [-]ctlecho    echo control characters in hat notation (\'^c\')\n",
	 DAT_0061c500);
      func_0x00401940 ("   [-]echo       echo input characters\n",
		       DAT_0061c500);
      func_0x00401940 (" * [-]echoctl    same as [-]ctlecho\n", DAT_0061c500);
      func_0x00401940
	("   [-]echoe      same as [-]crterase\n   [-]echok      echo a newline after a kill character\n",
	 DAT_0061c500);
      func_0x00401940 (" * [-]echoke     same as [-]crtkill\n", DAT_0061c500);
      func_0x00401940
	("   [-]echonl     echo newline even if not echoing other characters\n",
	 DAT_0061c500);
      func_0x00401940
	(" * [-]echoprt    echo erased characters backward, between \'\\\' and \'/\'\n",
	 DAT_0061c500);
      func_0x00401940
	(" * [-]extproc    enable \"LINEMODE\"; useful with high latency links\n",
	 DAT_0061c500);
      func_0x00401940 (" * [-]flusho     discard output\n", DAT_0061c500);
      func_0x00401750
	("   [-]icanon     enable special characters: %s\n   [-]iexten     enable non-POSIX special characters\n",
	 "erase, kill, werase, rprnt");
      func_0x00401940
	("   [-]isig       enable interrupt, quit, and suspend special characters\n   [-]noflsh     disable flushing after interrupt and quit special characters\n",
	 DAT_0061c500);
      func_0x00401940 (" * [-]prterase   same as [-]echoprt\n", DAT_0061c500);
      func_0x00401940
	(" * [-]tostop     stop background jobs that try to write to the terminal\n",
	 DAT_0061c500);
      func_0x00401940 ("\nCombination settings:\n", DAT_0061c500);
      func_0x00401940
	("   cbreak        same as -icanon\n   -cbreak       same as icanon\n",
	 DAT_0061c500);
      func_0x00401940
	("   cooked        same as brkint ignpar istrip icrnl ixon opost isig\n                 icanon, eof and eol characters to their default values\n   -cooked       same as raw\n",
	 DAT_0061c500);
      func_0x00401750 ("   crt           same as %s\n",
		       "echoe echoctl echoke");
      func_0x00401750
	("   dec           same as %s intr ^c erase 0177\n                 kill ^u\n",
	 "echoe echoctl echoke -ixany");
      func_0x00401940 (" * [-]decctlq    same as [-]ixany\n", DAT_0061c500);
      func_0x00401940
	("   ek            erase and kill characters to their default values\n   evenp         same as parenb -parodd cs7\n   -evenp        same as -parenb cs8\n",
	 DAT_0061c500);
      func_0x00401940
	("   litout        same as -parenb -istrip -opost cs8\n   -litout       same as parenb istrip opost cs7\n",
	 DAT_0061c500);
      func_0x00401750
	("   nl            same as %s\n   -nl           same as %s\n",
	 "-icrnl -onlcr", "icrnl -inlcr -igncr onlcr -ocrnl -onlret");
      func_0x00401940
	("   oddp          same as parenb parodd cs7\n   -oddp         same as -parenb cs8\n   [-]parity     same as [-]evenp\n   pass8         same as -parenb -istrip cs8\n   -pass8        same as parenb istrip cs7\n",
	 DAT_0061c500);
      func_0x00401750
	("   raw           same as -ignbrk -brkint -ignpar -parmrk -inpck -istrip\n                 -inlcr -igncr -icrnl -ixon -ixoff -icanon -opost\n                 -isig%s min 1 time 0\n   -raw          same as cooked\n",
	 " -iuclc -ixany -imaxbel");
      iParm4 = 0x4180e8;
      func_0x00401750
	("   sane          same as cread -ignbrk brkint -inlcr -igncr icrnl\n                 icanon iexten echo echoe echok -echonl -noflsh\n                 %s\n                 %s\n                 %s,\n                 all special characters to their default values\n",
	 "-ixoff -iutf8 -iuclc -ixany imaxbel -olcuc -ocrnl",
	 "opost -ofill onlcr -onocr -onlret nl0 cr0 tab0 bs0 vt0 ff0");
      pcVar8 = DAT_0061c500;
      func_0x00401940
	("\nHandle the tty line connected to standard input.  Without arguments,\nprints baud rate, line discipline, and deviations from stty sane.  In\nsettings, CHAR is taken literally, or coded as in ^c, 0x37, 0177 or\n127; special values ^- or undef used to disable special characters.\n");
      imperfection_wrapper ();	//    FUN_00401db7();
    }
  else
    {
      pcVar8 = "Try \'%s --help\' for more information.\n";
      func_0x00401930 (DAT_0061c520,
		       "Try \'%s --help\' for more information.\n",
		       DAT_0061c698);
    }
  cVar9 = (char) iParm1;
  func_0x00401af0 ();
  iStack36 = 1;
  do
    {
      if (iParm4 <= iStack36)
	{
	  return;
	}
      pcStack48 = *(char **) (extraout_RDX + (long) iStack36 * 8);
      cStack49 = '\0';
      bVar1 = false;
      if (pcStack48 != (char *) 0x0)
	{
	  bVar10 = *pcStack48 == '-';
	  if (bVar10)
	    {
	      pcStack48 = pcStack48 + 1;
	    }
	  iVar3 = func_0x00401a40 (pcStack48, "drain");
	  if (iVar3 == 0)
	    {
	      DAT_0061c420 = (uint) ! bVar10;
	    }
	  else
	    {
	      iStack56 = 0;
	      while (*(long *) (null_ARRAY_004156c0 + (long) iStack56 * 0x20)
		     != 0)
		{
		  iVar3 =
		    func_0x00401a40 (pcStack48,
				     *(undefined8 *) (null_ARRAY_004156c0 +
						      (long) iStack56 * 0x20),
				     *(undefined8 *) (null_ARRAY_004156c0 +
						      (long) iStack56 *
						      0x20));
		  if (iVar3 == 0)
		    {
		      if (1)
			{
			  imperfection_wrapper ();	//              cStack49 = FUN_004034f1(null_ARRAY_004156c0 + (long)iStack56 * 0x20, (ulong)bVar10, lParm5, null_ARRAY_004156c0 + (long)iStack56 * 0x20);
			  *(undefined *) CONCAT44 (iParm1, uStack16) = 1;
			}
		      else
			{
			  bVar1 = true;
			  cStack49 = '\x01';
			}
		      break;
		    }
		  iStack56 = iStack56 + 1;
		}
	      if ((cStack49 != '\x01') && (bVar10))
		{
		  imperfection_wrapper ();	//          uVar6 = FUN_004065e7(pcStack48 + -1);
		  imperfection_wrapper ();	//          FUN_00407858(0, 0, "invalid argument %s", uVar6);
		  imperfection_wrapper ();	//          FUN_0040209c(1);
		}
	      if (cStack49 != '\x01')
		{
		  iStack56 = 0;
		  while (*(long *)
			 (null_ARRAY_00416200 + (long) iStack56 * 0x18) != 0)
		    {
		      iVar3 =
			func_0x00401a40 (pcStack48,
					 *(undefined8 *) (null_ARRAY_00416200
							  +
							  (long) iStack56 *
							  0x18),
					 *(undefined8 *) (null_ARRAY_00416200
							  +
							  (long) iStack56 *
							  0x18));
		      if (iVar3 == 0)
			{
			  if ((iParm4 + -1 == iStack36)
			      ||
			      (*(long *)
			       (extraout_RDX + ((long) iStack36 + 1) * 8) ==
			       0))
			    {
			      imperfection_wrapper ();	//                uVar6 = FUN_004065e7(pcStack48);
			      imperfection_wrapper ();	//                FUN_00407858(0, 0, "missing argument to %s", uVar6);
			      imperfection_wrapper ();	//                FUN_0040209c(1);
			    }
			  cStack49 = '\x01';
			  iStack36 = iStack36 + 1;
			  uVar6 =
			    *(undefined8 *) (extraout_RDX +
					     (long) iStack36 * 8);
			  imperfection_wrapper ();	//              FUN_00403ab7(null_ARRAY_00416200 + (long)iStack56 * 0x18, uVar6, lParm5, uVar6);
			  *(undefined *) CONCAT44 (iParm1, uStack16) = 1;
			  break;
			}
		      iStack56 = iStack56 + 1;
		    }
		}
	      if ((cStack49 != '\x01') || (bVar1))
		{
		  iVar3 = func_0x00401a40 (pcStack48, "ispeed");
		  if (iVar3 == 0)
		    {
		      if ((iParm4 + -1 == iStack36)
			  ||
			  (*(long *)
			   (extraout_RDX + ((long) iStack36 + 1) * 8) == 0))
			{
			  FUN_004065e7 (pcStack48);
			  imperfection_wrapper ();	//              FUN_00407858(0, 0, "missing argument to %s");
			  imperfection_wrapper ();	//              FUN_0040209c(1);
			}
		      iStack36 = iStack36 + 1;
		      if (cVar9 == '\0')
			{
			  FUN_00403c03 (0,
					*(undefined8 *) (extraout_RDX +
							 (long) iStack36 * 8),
					lParm5);
			  *puParm6 = 1;
			  *(undefined *) CONCAT44 (iParm1, uStack16) = 1;
			}
		    }
		  else
		    {
		      iVar3 = func_0x00401a40 (pcStack48, "ospeed");
		      if (iVar3 == 0)
			{
			  if ((iParm4 + -1 == iStack36)
			      ||
			      (*(long *)
			       (extraout_RDX + ((long) iStack36 + 1) * 8) ==
			       0))
			    {
			      FUN_004065e7 (pcStack48);
			      imperfection_wrapper ();	//                FUN_00407858(0, 0, "missing argument to %s");
			      imperfection_wrapper ();	//                FUN_0040209c(1);
			    }
			  iStack36 = iStack36 + 1;
			  if (cVar9 == '\0')
			    {
			      FUN_00403c03 (1,
					    *(undefined8 *) (extraout_RDX +
							     (long) iStack36 *
							     8), lParm5);
			      *puParm6 = 1;
			      *(undefined *) CONCAT44 (iParm1, uStack16) = 1;
			    }
			}
		      else
			{
			  iVar3 = func_0x00401a40 (pcStack48, &DAT_004183f9);
			  if (iVar3 == 0)
			    {
			      if ((iParm4 + -1 == iStack36)
				  ||
				  (*(long *)
				   (extraout_RDX +
				    ((long) iStack36 + 1) * 8) == 0))
				{
				  FUN_004065e7 (pcStack48);
				  imperfection_wrapper ();	//                  FUN_00407858(0, 0, "missing argument to %s");
				  imperfection_wrapper ();	//                  FUN_0040209c(1);
				}
			      iStack36 = iStack36 + 1;
			      if (cVar9 == '\0')
				{
				  imperfection_wrapper ();	//                  uVar4 = FUN_00404ebb(*(undefined8 *)(extraout_RDX + (long)iStack36 * 8), 0x7fffffff);
				  FUN_00403c90 ((ulong) uVar4, 0xffffffff,
						pcVar8);
				}
			    }
			  else
			    {
			      iVar3 =
				func_0x00401a40 (pcStack48, &DAT_004183fe);
			      if ((iVar3 == 0)
				  || (iVar3 =
				      func_0x00401a40 (pcStack48, "columns"),
				      iVar3 == 0))
				{
				  if ((iParm4 + -1 == iStack36)
				      ||
				      (*(long *)
				       (extraout_RDX +
					((long) iStack36 + 1) * 8) == 0))
				    {
				      imperfection_wrapper ();	//                    uVar6 = FUN_004065e7(pcStack48);
				      imperfection_wrapper ();	//                    FUN_00407858(0, 0, "missing argument to %s", uVar6);
				      imperfection_wrapper ();	//                    FUN_0040209c(1);
				    }
				  iStack36 = iStack36 + 1;
				  if (cVar9 == '\0')
				    {
				      imperfection_wrapper ();	//                    uVar4 = FUN_00404ebb(*(undefined8 *)(extraout_RDX + (long)iStack36 * 8), 0x7fffffff);
				      imperfection_wrapper ();	//                    FUN_00403c90(0xffffffff, (ulong)uVar4, pcVar8, (ulong)uVar4);
				    }
				}
			      else
				{
				  iVar3 =
				    func_0x00401a40 (pcStack48,
						     &DAT_0041840b);
				  if (iVar3 == 0)
				    {
				      if (cVar9 == '\0')
					{
					  DAT_0061c5a0 = FUN_00403e84 ();
					  DAT_0061c5a4 = 0;
					  FUN_00403d8c (0, pcVar8);
					}
				    }
				  else
				    {
				      iVar3 =
					func_0x00401a40 (pcStack48,
							 &DAT_00418410);
				      if (iVar3 == 0)
					{
					  if ((iParm4 + -1 == iStack36)
					      ||
					      (*(long *)
					       (extraout_RDX +
						((long) iStack36 + 1) * 8) ==
					       0))
					    {
					      imperfection_wrapper ();	//                        uVar6 = FUN_004065e7(pcStack48);
					      imperfection_wrapper ();	//                        FUN_00407858(0, 0, "missing argument to %s", uVar6);
					      imperfection_wrapper ();	//                        FUN_0040209c(1);
					    }
					  iStack36 = iStack36 + 1;
					  imperfection_wrapper ();	//                      uVar7 = FUN_00404ebb(*(undefined8 *)(extraout_RDX + (long)iStack36 * 8), 0xffffffffffffffff);
					  *(undefined *) (lParm5 + 0x10) =
					    (char) uVar7;
					  if ((ulong) *
					      (byte *) (lParm5 + 0x10) !=
					      uVar7)
					    {
					      imperfection_wrapper ();	//                        uVar6 = FUN_004065e7(*(undefined8 *)(extraout_RDX + (long)iStack36 * 8));
					      imperfection_wrapper ();	//                        FUN_00407858(0, 0, "invalid line discipline %s", uVar6);
					    }
					  *(undefined *) CONCAT44 (iParm1,
								   uStack16) =
					    1;
					}
				      else
					{
					  iVar3 =
					    func_0x00401a40 (pcStack48,
							     "speed");
					  if (iVar3 == 0)
					    {
					      if (cVar9 == '\0')
						{
						  DAT_0061c5a0 =
						    FUN_00403e84 ();
						  FUN_004046ca (lParm5, 0);
						}
					    }
					  else
					    {
					      iVar3 =
						FUN_00404a75 (pcStack48);
					      if (iVar3 == -1)
						{
						  imperfection_wrapper ();	//                          cVar2 = FUN_0040495e(pcStack48, lParm5, lParm5);
						  if (cVar2 != '\x01')
						    {
						      imperfection_wrapper ();	//                            uVar6 = FUN_004065e7(pcStack48);
						      imperfection_wrapper ();	//                            FUN_00407858(0, 0, "invalid argument %s", uVar6);
						      imperfection_wrapper ();	//                            FUN_0040209c(1);
						    }
						  *(undefined *)
						    CONCAT44 (iParm1,
							      uStack16) = 1;
						}
					      else
						{
						  if (cVar9 == '\0')
						    {
						      FUN_00403c03 (2,
								    pcStack48,
								    lParm5);
						      *puParm6 = 1;
						      *(undefined *)
							CONCAT44 (iParm1,
								  uStack16) =
							1;
						    }
						}
					    }
					}
				    }
				}
			    }
			}
		    }
		}
	    }
	}
      iStack36 = iStack36 + 1;
    }
  while (true);
}

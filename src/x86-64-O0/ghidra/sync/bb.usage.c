
#include "sync.h"

long null_ARRAY_0061463_0_8_;
long null_ARRAY_0061463_8_8_;
long null_ARRAY_0061478_0_8_;
long null_ARRAY_0061478_16_8_;
long null_ARRAY_0061478_24_8_;
long null_ARRAY_0061478_32_8_;
long null_ARRAY_0061478_40_8_;
long null_ARRAY_0061478_48_8_;
long null_ARRAY_0061478_8_8_;
long null_ARRAY_006148e_0_4_;
long null_ARRAY_006148e_16_8_;
long null_ARRAY_006148e_4_4_;
long null_ARRAY_006148e_8_4_;
long DAT_00411940;
long DAT_004119fd;
long DAT_00411a7b;
long DAT_00411d6e;
long DAT_00411def;
long DAT_00411e63;
long DAT_00411ea8;
long DAT_00411fde;
long DAT_00411fe2;
long DAT_00411fe7;
long DAT_00411fe9;
long DAT_00412653;
long DAT_00412a20;
long DAT_00412a38;
long DAT_00412a3d;
long DAT_00412aa7;
long DAT_00412b38;
long DAT_00412b3b;
long DAT_00412b89;
long DAT_00412b8d;
long DAT_00412c00;
long DAT_00412c01;
long DAT_00412de8;
long DAT_00614238;
long DAT_00614248;
long DAT_00614258;
long DAT_00614608;
long DAT_00614620;
long DAT_00614698;
long DAT_0061469c;
long DAT_006146a0;
long DAT_006146c0;
long DAT_006146d0;
long DAT_006146e0;
long DAT_006146e8;
long DAT_00614700;
long DAT_00614708;
long DAT_00614750;
long DAT_00614758;
long DAT_00614760;
long DAT_006148c0;
long DAT_00614918;
long DAT_00614920;
long DAT_00614928;
long DAT_00614938;
long fde_00413870;
long FLOAT_UNKNOWN;
long null_ARRAY_00411b00;
long null_ARRAY_00413040;
long null_ARRAY_00614630;
long null_ARRAY_00614720;
long null_ARRAY_00614780;
long null_ARRAY_006147c0;
long null_ARRAY_006148e0;
long PTR_DAT_00614600;
long PTR_null_ARRAY_00614640;
ulong
FUN_00401c5a (int iParm1)
{
  uint uVar1;
  int iVar2;
  uint *puVar3;
  undefined8 uVar4;
  ulong uVar5;
  char *pcVar6;
  int iStack60;
  uint uStack56;
  byte bStack49;

  if (iParm1 == 0)
    {
      func_0x00401500 ("Usage: %s [OPTION] [FILE]...\n", DAT_00614760);
      func_0x00401690
	("Synchronize cached writes to persistent storage\n\nIf one or more files are specified, sync only them,\nor their containing file systems.\n\n",
	 DAT_006146c0);
      func_0x00401690
	("  -d, --data             sync only file data, no unneeded metadata\n",
	 DAT_006146c0);
      func_0x00401690
	("  -f, --file-system      sync the file systems that contain the files\n",
	 DAT_006146c0);
      func_0x00401690 ("      --help     display this help and exit\n",
		       DAT_006146c0);
      pcVar6 = DAT_006146c0;
      func_0x00401690
	("      --version  output version information and exit\n");
      imperfection_wrapper ();	//    FUN_00401abe();
    }
  else
    {
      pcVar6 = "Try \'%s --help\' for more information.\n";
      func_0x00401680 (DAT_006146e0,
		       "Try \'%s --help\' for more information.\n",
		       DAT_00614760);
    }
  func_0x00401830 ();
  bStack49 = 1;
  uStack56 = func_0x004018a0 (pcVar6, 0x800, 0x800);
  if ((int) uStack56 < 0)
    {
      puVar3 = (uint *) func_0x00401820 ();
      uVar1 = *puVar3;
      if (true)
	{
	  uStack56 = func_0x004018a0 (pcVar6, 0x801);
	}
      if ((int) uStack56 < 0)
	{
	  imperfection_wrapper ();	//      uVar4 = FUN_0040360d(4, pcVar6);
	  imperfection_wrapper ();	//      FUN_00403f29(0, (ulong)uVar1, "error opening %s", uVar4);
	}
      uVar5 = 0;
    }
  else
    {
      imperfection_wrapper ();	//    uVar1 = FUN_0040400d((ulong)uStack56, 3);
      if (1)
	{
	  imperfection_wrapper ();	//      uVar4 = FUN_0040360d(4, pcVar6);
	  puVar3 = (uint *) func_0x00401820 ();
	  imperfection_wrapper ();	//      FUN_00403f29(0, (ulong)*puVar3, "couldn\'t reset non-blocking mode %s", uVar4);
	  bStack49 = 0;
	}
      if (bStack49 != 0)
	{
	  iStack60 = -1;
	  if (iParm1 == 1)
	    {
	      iStack60 = func_0x00401670 ((ulong) uStack56);
	    }
	  else
	    {
	      if (iParm1 == 0)
		{
		  iStack60 = func_0x004017e0 ((ulong) uStack56);
		}
	      else
		{
		  if (iParm1 == 2)
		    {
		      iStack60 = func_0x00401800 ((ulong) uStack56);
		    }
		}
	    }
	  if (iStack60 < 0)
	    {
	      imperfection_wrapper ();	//        uVar4 = FUN_0040360d(4, pcVar6);
	      puVar3 = (uint *) func_0x00401820 ();
	      imperfection_wrapper ();	//        FUN_00403f29(0, (ulong)*puVar3, "error syncing %s", uVar4);
	      bStack49 = 0;
	    }
	}
      iVar2 = func_0x00401910 ((ulong) uStack56);
      if (iVar2 < 0)
	{
	  imperfection_wrapper ();	//      uVar4 = FUN_0040360d(4, pcVar6);
	  puVar3 = (uint *) func_0x00401820 ();
	  imperfection_wrapper ();	//      FUN_00403f29(0, (ulong)*puVar3, "failed to close %s", uVar4);
	  bStack49 = 0;
	}
      uVar5 = (ulong) bStack49;
    }
  return uVar5;
}

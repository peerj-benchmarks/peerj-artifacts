
#include "sum.h"

long null_ARRAY_006156b_0_8_;
long null_ARRAY_006156b_8_8_;
long null_ARRAY_0061580_0_8_;
long null_ARRAY_0061580_16_8_;
long null_ARRAY_0061580_24_8_;
long null_ARRAY_0061580_32_8_;
long null_ARRAY_0061580_40_8_;
long null_ARRAY_0061580_48_8_;
long null_ARRAY_0061580_8_8_;
long null_ARRAY_0061594_0_4_;
long null_ARRAY_0061594_16_8_;
long null_ARRAY_0061594_4_4_;
long null_ARRAY_0061594_8_4_;
long DAT_00412838;
long DAT_004128f5;
long DAT_00412973;
long DAT_00412ba6;
long DAT_00412baa;
long DAT_00412bac;
long DAT_00412bae;
long DAT_00412bba;
long DAT_00412be6;
long DAT_00412c01;
long DAT_00412c19;
long DAT_00412c1b;
long DAT_00412d20;
long DAT_00412e5e;
long DAT_00412e62;
long DAT_00412e67;
long DAT_00412e69;
long DAT_004134d3;
long DAT_004138a0;
long DAT_004138b8;
long DAT_004138bd;
long DAT_00413927;
long DAT_004139b8;
long DAT_004139bb;
long DAT_00413a09;
long DAT_00413a0d;
long DAT_00413a80;
long DAT_00413a81;
long DAT_00413c68;
long DAT_00615288;
long DAT_00615298;
long DAT_006152a8;
long DAT_00615688;
long DAT_006156a0;
long DAT_00615718;
long DAT_0061571c;
long DAT_00615720;
long DAT_00615740;
long DAT_00615748;
long DAT_00615750;
long DAT_00615760;
long DAT_00615768;
long DAT_00615780;
long DAT_00615788;
long DAT_006157d0;
long DAT_006157d8;
long DAT_006157e0;
long DAT_006157e8;
long DAT_00615978;
long DAT_00615980;
long DAT_00615988;
long DAT_00615998;
long fde_00414748;
long FLOAT_UNKNOWN;
long null_ARRAY_004129c0;
long null_ARRAY_00412c10;
long null_ARRAY_00413ec0;
long null_ARRAY_006156b0;
long null_ARRAY_006157a0;
long null_ARRAY_00615800;
long null_ARRAY_00615840;
long null_ARRAY_00615940;
long PTR_DAT_00615680;
long PTR_null_ARRAY_006156c0;
long stack0x00000008;
undefined8
FUN_00401da4 (uint uParm1)
{
  int iVar1;
  uint *puVar2;
  undefined8 uVar3;
  int iVar4;
  ulong uVar5;
  undefined auStack288[208];
  int iStack80;
  bool bStack73;
  long lStack72;
  uint uStack60;
  long lStack56;
  code *pcStack32;
  uint local_c;

  if (uParm1 == 0)
    {
      pcStack32 = (code *) 0x401df0;
      local_c = uParm1;
      func_0x00401600 ("Usage: %s [OPTION]... [FILE]...\n", DAT_006157e8);
      pcStack32 = (code *) 0x401e04;
      func_0x004017a0 ("Print checksum and block counts for each FILE.\n",
		       DAT_00615740);
      pcStack32 = (code *) 0x401e09;
      FUN_00401bee ();
      pcStack32 = (code *) 0x401e1d;
      func_0x004017a0
	("\n  -r              use BSD sum algorithm, use 1K blocks\n  -s, --sysv      use System V sum algorithm, use 512 bytes blocks\n",
	 DAT_00615740);
      pcStack32 = (code *) 0x401e31;
      func_0x004017a0 ("      --help     display this help and exit\n",
		       DAT_00615740);
      iVar4 = (int) DAT_00615740;
      pcStack32 = (code *) 0x401e45;
      func_0x004017a0
	("      --version  output version information and exit\n");
      pcStack32 = (code *) 0x401e4f;
      imperfection_wrapper ();	//    FUN_00401c08();
    }
  else
    {
      iVar4 = 0x412a40;
      pcStack32 = (code *) 0x401dd5;
      local_c = uParm1;
      func_0x00401790 (DAT_00615760,
		       "Try \'%s --help\' for more information.\n",
		       DAT_006157e8);
    }
  uVar5 = (ulong) local_c;
  pcStack32 = FUN_00401e59;
  func_0x00401960 ();
  uStack60 = 0;
  lStack72 = 0;
  pcStack32 = (code *) & stack0xfffffffffffffff8;
  iVar1 = func_0x004018a0 (uVar5, &DAT_00412baa);
  bStack73 = iVar1 == 0;
  if (bStack73)
    {
      lStack56 = DAT_00615748;
      DAT_006157d0 = 1;
      FUN_00404eac (0, 0);
    }
  else
    {
      lStack56 = func_0x00401870 (uVar5, &DAT_00412bac);
      if (lStack56 == 0)
	{
	  imperfection_wrapper ();	//      uVar3 = FUN_004046d4(0, 3, uVar5);
	  puVar2 = (uint *) func_0x00401950 ();
	  imperfection_wrapper ();	//      FUN_00405029(0, (ulong)*puVar2, &DAT_00412bae, uVar3);
	  return 0;
	}
    }
  FUN_00402680 (lStack56, 2);
  while (iStack80 = func_0x00401660 (lStack56), iStack80 != -1)
    {
      lStack72 = lStack72 + 1;
      uStack60 =
	(uStack60 & 1) * 0x8000 + ((int) uStack60 >> 1) + iStack80 & 0xffff;
    }
  iVar1 = func_0x00401980 (lStack56);
  if (iVar1 == 0)
    {
      if ((bStack73 != true) && (iVar1 = FUN_0040510d (lStack56), iVar1 != 0))
	{
	  imperfection_wrapper ();	//      uVar3 = FUN_004046d4(0, 3, uVar5);
	  puVar2 = (uint *) func_0x00401950 ();
	  imperfection_wrapper ();	//      FUN_00405029(0, (ulong)*puVar2, &DAT_00412bae, uVar3);
	  return 0;
	}
      uVar3 = FUN_004028ad (lStack72, auStack288, 0, 1, 0x400);
      func_0x00401600 ("%05d %5s", (ulong) uStack60, uVar3);
      if (1 < iVar4)
	{
	  func_0x00401600 (&DAT_00412bba, uVar5);
	}
      func_0x00401910 (10);
      uVar3 = 1;
    }
  else
    {
      imperfection_wrapper ();	//    uVar3 = FUN_004046d4(0, 3, uVar5);
      puVar2 = (uint *) func_0x00401950 ();
      imperfection_wrapper ();	//    FUN_00405029(0, (ulong)*puVar2, &DAT_00412bae, uVar3);
      if (bStack73 != true)
	{
	  FUN_0040510d (lStack56);
	}
      uVar3 = 0;
    }
  return uVar3;
}

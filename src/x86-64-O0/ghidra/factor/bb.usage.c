
#include "factor.h"

long null_ARRAY_0061e43_0_8_;
long null_ARRAY_0061e43_8_8_;
long null_ARRAY_0061e56_0_8_;
long null_ARRAY_0061e56_8_8_;
long null_ARRAY_0061e5c_0_8_;
long null_ARRAY_0061e5c_16_8_;
long null_ARRAY_0061e5c_24_8_;
long null_ARRAY_0061e5c_32_8_;
long null_ARRAY_0061e5c_40_8_;
long null_ARRAY_0061e5c_48_8_;
long null_ARRAY_0061e5c_8_8_;
long null_ARRAY_0061e70_0_4_;
long null_ARRAY_0061e70_16_8_;
long null_ARRAY_0061e70_4_4_;
long null_ARRAY_0061e70_8_4_;
long local_4_4_4_;
long local_5_0_1_;
long DAT_00417ac0;
long DAT_00417b7d;
long DAT_00417bfb;
long DAT_0041ae3c;
long DAT_0041affd;
long DAT_0041b001;
long DAT_0041b01c;
long DAT_0041b0d8;
long DAT_0041b120;
long DAT_0041b25e;
long DAT_0041b262;
long DAT_0041b267;
long DAT_0041b269;
long DAT_0041b8d3;
long DAT_0041bca0;
long DAT_0041bcb8;
long DAT_0041bcbd;
long DAT_0041bd27;
long DAT_0041bdb8;
long DAT_0041bdbb;
long DAT_0041be09;
long DAT_0041be0d;
long DAT_0041be80;
long DAT_0041be81;
long DAT_0061e000;
long DAT_0061e010;
long DAT_0061e020;
long DAT_0061e400;
long DAT_0061e404;
long DAT_0061e410;
long DAT_0061e420;
long DAT_0061e498;
long DAT_0061e49c;
long DAT_0061e4a0;
long DAT_0061e4c0;
long DAT_0061e4c8;
long DAT_0061e4d0;
long DAT_0061e4e0;
long DAT_0061e4e8;
long DAT_0061e500;
long DAT_0061e508;
long DAT_0061e550;
long DAT_0061e570;
long DAT_0061e578;
long DAT_0061e580;
long DAT_0061e738;
long DAT_0061ef48;
long DAT_0061ef50;
long DAT_0061ef60;
long fde_0041cda8;
long FLOAT_UNKNOWN;
long null_ARRAY_00417c80;
long null_ARRAY_00417d40;
long null_ARRAY_00418000;
long null_ARRAY_004182c0;
long null_ARRAY_0041ad00;
long null_ARRAY_0041b051;
long null_ARRAY_0041b074;
long null_ARRAY_0041c3a0;
long null_ARRAY_0041c5a0;
long null_ARRAY_0061e430;
long null_ARRAY_0061e460;
long null_ARRAY_0061e520;
long null_ARRAY_0061e560;
long null_ARRAY_0061e5c0;
long null_ARRAY_0061e600;
long null_ARRAY_0061e700;
long null_ARRAY_0061e740;
long PTR_DAT_0061e408;
long PTR_null_ARRAY_0061e440;
long stack0x00000008;
long stack0xfffffffffffffff8;
ulong
FUN_00405717 (uint uParm1)
{
  byte bVar1;
  undefined auStack64[8];
  undefined8 uStack56;
  long lStack48;
  bool bStack33;
  code *pcStack32;
  uint local_c;

  if (uParm1 == 0)
    {
      pcStack32 = (code *) 0x40576a;
      local_c = uParm1;
      func_0x004016b0 ("Usage: %s [NUMBER]...\n  or:  %s OPTION\n",
		       DAT_0061e580, DAT_0061e580);
      pcStack32 = (code *) 0x40577e;
      func_0x00401880
	("Print the prime factors of each specified integer NUMBER.  If none\nare specified on the command line, read them from standard input.\n\n",
	 DAT_0061e4c0);
      pcStack32 = (code *) 0x405792;
      func_0x00401880 ("      --help     display this help and exit\n",
		       DAT_0061e4c0);
      pcStack32 = (code *) 0x4057a6;
      func_0x00401880
	("      --version  output version information and exit\n",
	 DAT_0061e4c0);
      pcStack32 = (code *) 0x4057b0;
      FUN_00401cce ("factor");
    }
  else
    {
      pcStack32 = (code *) 0x405748;
      local_c = uParm1;
      func_0x00401870 (DAT_0061e4e0,
		       "Try \'%s --help\' for more information.\n",
		       DAT_0061e580);
    }
  pcStack32 = FUN_004057ba;
  func_0x00401a30 ((ulong) local_c);
  bStack33 = true;
  pcStack32 = (code *) & stack0xfffffffffffffff8;
  FUN_0040768b (auStack64);
  while (true)
    {
      lStack48 = FUN_00407742 (DAT_0061e4c8, &DAT_0041affd, 3, auStack64);
      if (lStack48 == -1)
	break;
      bVar1 = FUN_00405626 (uStack56);
      bStack33 = (bVar1 & bStack33) != 0;
    }
  func_0x00401b30 (uStack56);
  return (ulong) bStack33;
}

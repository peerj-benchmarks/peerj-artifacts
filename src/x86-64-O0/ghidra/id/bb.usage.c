
#include "id.h"

long null_ARRAY_00616bf_0_8_;
long null_ARRAY_00616bf_8_8_;
long null_ARRAY_00616dc_0_8_;
long null_ARRAY_00616dc_16_8_;
long null_ARRAY_00616dc_24_8_;
long null_ARRAY_00616dc_32_8_;
long null_ARRAY_00616dc_40_8_;
long null_ARRAY_00616dc_48_8_;
long null_ARRAY_00616dc_8_8_;
long null_ARRAY_00616f0_0_4_;
long null_ARRAY_00616f0_16_8_;
long null_ARRAY_00616f0_4_4_;
long null_ARRAY_00616f0_8_4_;
long DAT_00413300;
long DAT_004133bd;
long DAT_0041343b;
long DAT_0041396f;
long DAT_00413b77;
long DAT_00413c9a;
long DAT_00413ce0;
long DAT_00413e1e;
long DAT_00413e22;
long DAT_00413e27;
long DAT_00413e29;
long DAT_00414480;
long DAT_004144c3;
long DAT_00414880;
long DAT_00414c0e;
long DAT_00414c13;
long DAT_00414c7f;
long DAT_00414d10;
long DAT_00414d13;
long DAT_00414d61;
long DAT_00414d65;
long DAT_00414dd8;
long DAT_00414dd9;
long DAT_00616790;
long DAT_006167a0;
long DAT_006167b0;
long DAT_00616bc0;
long DAT_00616bd0;
long DAT_00616be0;
long DAT_00616c70;
long DAT_00616c74;
long DAT_00616c78;
long DAT_00616c80;
long DAT_00616c90;
long DAT_00616ca0;
long DAT_00616ca8;
long DAT_00616cc0;
long DAT_00616cc8;
long DAT_00616d10;
long DAT_00616d11;
long DAT_00616d14;
long DAT_00616d18;
long DAT_00616d1c;
long DAT_00616d20;
long DAT_00616d28;
long DAT_00616d88;
long DAT_00616d90;
long DAT_00616d98;
long DAT_00616f38;
long DAT_00616f40;
long DAT_00616f48;
long DAT_00616f58;
long fde_00415b00;
long FLOAT_UNKNOWN;
long null_ARRAY_004134c0;
long null_ARRAY_00415220;
long null_ARRAY_00616bf0;
long null_ARRAY_00616c20;
long null_ARRAY_00616ce0;
long null_ARRAY_00616d30;
long null_ARRAY_00616d50;
long null_ARRAY_00616d70;
long null_ARRAY_00616dc0;
long null_ARRAY_00616e00;
long null_ARRAY_00616f00;
long PTR_DAT_00616bc8;
long PTR_null_ARRAY_00616c00;
long PTR_s_invalid_group_00616c68;
long PTR_s_invalid_spec_00616c58;
long PTR_s_invalid_user_00616c60;
ulong
FUN_00402026 (uint uParm1)
{
  bool bVar1;
  bool bVar2;
  char cVar3;
  bool bVar4;
  int iVar5;
  uint uVar6;
  long lVar7;
  undefined4 *puVar8;
  int *piVar9;
  uint *puVar10;
  uint uVar11;
  char *pcVar12;
  undefined8 uVar13;
  undefined8 *puStack64;
  byte bStack52;
  byte bStack50;
  byte bStack49;
  undefined8 uStack48;

  if (uParm1 == 0)
    {
      func_0x00401810 ("Usage: %s [OPTION]... [USER]\n", DAT_00616d98);
      func_0x004019f0
	("Print user and group information for the specified USER,\nor (when USER omitted) for the current user.\n\n",
	 DAT_00616c80);
      func_0x004019f0
	("  -a             ignore, for compatibility with other versions\n  -Z, --context  print only the security context of the process\n  -g, --group    print only the effective group ID\n  -G, --groups   print all group IDs\n  -n, --name     print a name instead of a number, for -ugG\n  -r, --real     print the real ID instead of the effective ID, with -ugG\n  -u, --user     print only the effective user ID\n  -z, --zero     delimit entries with NUL characters, not whitespace;\n                   not permitted in default format\n",
	 DAT_00616c80);
      func_0x004019f0 ("      --help     display this help and exit\n",
		       DAT_00616c80);
      func_0x004019f0
	("      --version  output version information and exit\n",
	 DAT_00616c80);
      pcVar12 = (char *) DAT_00616c80;
      func_0x004019f0
	("\nWithout any OPTION, print some useful set of identified information.\n");
      imperfection_wrapper ();	//    FUN_00401e6e();
    }
  else
    {
      pcVar12 = "Try \'%s --help\' for more information.\n";
      func_0x004019e0 (DAT_00616ca0,
		       "Try \'%s --help\' for more information.\n",
		       DAT_00616d98);
    }
  func_0x00401bd0 ();
  cVar3 = FUN_0040201b ();
  bVar2 = false;
  uStack48 = 0;
  bStack49 = 0;
  bStack50 = 0;
  bVar1 = false;
  bStack52 = 0;
  FUN_00403016 (*(undefined8 *) pcVar12);
  func_0x00401b80 (6, &DAT_0041343b);
  func_0x00401b60 (FUN_00402eac);
LAB_0040226c:
  ;
  do
    {
      uVar13 = 0x40228a;
      imperfection_wrapper ();	//    iVar5 = FUN_00406896((ulong)uParm1, pcVar12, "agnruzGZ", null_ARRAY_004134c0, 0);
      uVar6 = DAT_00616c70;
      if (iVar5 == -1)
	{
	  uVar11 = uParm1 - DAT_00616c70;
	  if (1 < uVar11)
	    {
	      imperfection_wrapper ();	//        uVar13 = FUN_0040448a(((undefined8 *)pcVar12)[(long)(int)DAT_00616c70 + 1]);
	      imperfection_wrapper ();	//        FUN_004056bd(0, 0, "extra operand %s", uVar13);
	      FUN_00402026 (1);
	    }
	  if ((uParm1 != uVar6) && (DAT_00616d10 != 0))
	    {
	      imperfection_wrapper ();	//        FUN_004056bd(1, 0, "cannot print security context when user specified");
	    }
	  if (1 <
	      (uint) DAT_00616d10 + (uint) bStack52 + (uint) bStack50 +
	      (uint) bStack49)
	    {
	      imperfection_wrapper ();	//        FUN_004056bd(1, 0, "cannot print \"only\" of more than one choice");
	    }
	  if ((((bStack52 == 1) || (bStack50 == 1)) || (bStack49 == 1))
	      || (DAT_00616d10 == 1))
	    {
	      bVar4 = false;
	    }
	  else
	    {
	      bVar4 = true;
	    }
	  if ((bVar4) && ((bVar1 || (DAT_00616d11 != 0))))
	    {
	      imperfection_wrapper ();	//        FUN_004056bd(1, 0, "cannot print only names or real IDs in default format");
	    }
	  if ((bVar4) && (bVar2))
	    {
	      imperfection_wrapper ();	//        FUN_004056bd(1, 0, "option --zero not permitted in default format");
	    }
	  if (1)
	    {
	      imperfection_wrapper ();	//        FUN_004056bd(1, 0, "can\'t get process context");
	    }
	  if (uVar11 == 1)
	    {
	      puStack64 = (undefined8 *) 0x0;
	      pcVar12 =
		(char *) ((undefined8 *) pcVar12)[(long) (int) DAT_00616c70];
	      if ((*pcVar12 != '\0')
		  && (lVar7 =
		      FUN_00404880 (pcVar12, &DAT_00616d18, 0, 0, 0),
		      lVar7 == 0))
		{
		  puStack64 =
		    (undefined8 *) func_0x00401b10 ((ulong) DAT_00616d18);
		}
	      if (puStack64 == (undefined8 *) 0x0)
		{
		  imperfection_wrapper ();	//          uVar13 = FUN_0040448a(pcVar12);
		  imperfection_wrapper ();	//          FUN_004056bd(1, 0, "%s: no such user", uVar13);
		}
	      imperfection_wrapper ();	//        uStack48 = FUN_00405023(*puStack64);
	      DAT_00616d14 = *(uint *) (puStack64 + 2);
	      DAT_00616d1c = *(uint *) ((long) puStack64 + 0x14);
	      DAT_00616d18 = DAT_00616d14;
	      DAT_00616d20 = DAT_00616d1c;
	    }
	  else
	    {
	      if (bStack52 == 0)
		{
		  if (((bStack50 == 1) || (bStack49 == 1))
		      || (DAT_00616d10 == 1))
		    {
		      bVar4 = false;
		    }
		  else
		    {
		      bVar4 = true;
		    }
		}
	      else
		{
		  bVar4 = (bool) (bVar1 ^ 1);
		}
	      if (bVar4)
		{
		  puVar8 = (undefined4 *) func_0x00401bc0 ();
		  *puVar8 = 0;
		  DAT_00616d18 = func_0x00401840 ();
		  if ((DAT_00616d18 == 0xffffffff)
		      && (piVar9 = (int *) func_0x00401bc0 (), *piVar9 != 0))
		    {
		      puVar10 = (uint *) func_0x00401bc0 ();
		      imperfection_wrapper ();	//            FUN_004056bd(1, (ulong)*puVar10, "cannot get effective UID");
		    }
		}
	      bVar4 = bVar1;
	      if (bStack52 == 0)
		{
		  if ((bStack50 == 1)
		      || ((bStack49 == 0 && (DAT_00616d10 == 1))))
		    {
		      bVar4 = false;
		    }
		  else
		    {
		      bVar4 = true;
		    }
		}
	      if (bVar4)
		{
		  puVar8 = (undefined4 *) func_0x00401bc0 ();
		  *puVar8 = 0;
		  DAT_00616d14 = func_0x00401910 ();
		  if ((DAT_00616d14 == 0xffffffff)
		      && (piVar9 = (int *) func_0x00401bc0 (), *piVar9 != 0))
		    {
		      puVar10 = (uint *) func_0x00401bc0 ();
		      imperfection_wrapper ();	//            FUN_004056bd(1, (ulong)*puVar10, "cannot get real UID");
		    }
		}
	      if ((bStack52 != 1)
		  &&
		  (((bStack50 != 0 || (bStack49 != 0))
		    || (DAT_00616d10 != 1))))
		{
		  puVar8 = (undefined4 *) func_0x00401bc0 ();
		  *puVar8 = 0;
		  DAT_00616d20 = func_0x004018b0 ();
		  if ((DAT_00616d20 == 0xffffffff)
		      && (piVar9 = (int *) func_0x00401bc0 (), *piVar9 != 0))
		    {
		      puVar10 = (uint *) func_0x00401bc0 ();
		      imperfection_wrapper ();	//            FUN_004056bd(1, (ulong)*puVar10, "cannot get effective GID");
		    }
		  puVar8 = (undefined4 *) func_0x00401bc0 ();
		  *puVar8 = 0;
		  DAT_00616d1c = func_0x00401800 ();
		  if ((DAT_00616d1c == 0xffffffff)
		      && (piVar9 = (int *) func_0x00401bc0 (), *piVar9 != 0))
		    {
		      puVar10 = (uint *) func_0x00401bc0 ();
		      imperfection_wrapper ();	//            FUN_004056bd(1, (ulong)*puVar10, "cannot get real GID");
		    }
		}
	    }
	  if (bStack52 == 0)
	    {
	      if (bStack50 == 0)
		{
		  if (bStack49 == 0)
		    {
		      if (DAT_00616d10 == 0)
			{
			  FUN_0040294d (uStack48);
			}
		      else
			{
			  func_0x004019f0 (DAT_00616d28, DAT_00616c80,
					   DAT_00616c80);
			}
		    }
		  else
		    {
		      if (bVar2)
			{
			  uVar6 = 0;
			}
		      else
			{
			  uVar6 = 0x20;
			}
		      cVar3 =
			FUN_00402c3b (uStack48, (ulong) DAT_00616d14,
				      (ulong) DAT_00616d1c,
				      (ulong) DAT_00616d20,
				      (ulong) DAT_00616d11, (ulong) uVar6);
		      if (cVar3 != '\x01')
			{
			  DAT_00616bc0 = '\0';
			}
		    }
		}
	      else
		{
		  uVar6 = DAT_00616d20;
		  if (bVar1)
		    {
		      uVar6 = DAT_00616d1c;
		    }
		  imperfection_wrapper ();	//          cVar3 = FUN_00402e14((ulong)uVar6, (ulong)DAT_00616d11, (ulong)DAT_00616d11);
		  if (cVar3 != '\x01')
		    {
		      DAT_00616bc0 = '\0';
		    }
		}
	    }
	  else
	    {
	      uVar6 = DAT_00616d18;
	      if (bVar1)
		{
		  uVar6 = DAT_00616d14;
		}
	      FUN_004028b3 ((ulong) uVar6);
	    }
	  if (bVar2)
	    {
	      uVar6 = 0;
	    }
	  else
	    {
	      uVar6 = 10;
	    }
	  func_0x00401b90 ((ulong) uVar6);
	  return (ulong) (DAT_00616bc0 == '\0');
	}
    }
  while (iVar5 == 0x61);
  if (iVar5 < 0x62)
    {
      if (iVar5 == -0x82)
	{
	  uVar13 = 0x40221a;
	  FUN_00402026 (0);
	LAB_0040221a:
	  ;
	  imperfection_wrapper ();	//      FUN_00404e89(DAT_00616c80, &DAT_0041396f, "GNU coreutils", PTR_DAT_00616bc8, "Arnold Robbins", "David MacKenzie", 0, uVar13);
	  func_0x00401bd0 (0);
	}
      else
	{
	  if (iVar5 < -0x81)
	    {
	      if (iVar5 == -0x83)
		goto LAB_0040221a;
	    }
	  else
	    {
	      if (iVar5 == 0x47)
		{
		  bStack49 = 1;
		  goto LAB_0040226c;
		}
	      if (iVar5 == 0x5a)
		{
		  if (true)
		    {
		      imperfection_wrapper ();	//            FUN_004056bd(1, 0, "--context (-Z) works only on an SELinux-enabled kernel");
		    }
		  DAT_00616d10 = 1;
		  goto LAB_0040226c;
		}
	    }
	}
    }
  else
    {
      if (iVar5 == 0x72)
	{
	  bVar1 = true;
	  goto LAB_0040226c;
	}
      if (0x72 < iVar5)
	{
	  if (iVar5 == 0x75)
	    {
	      bStack52 = 1;
	    }
	  else
	    {
	      if (iVar5 != 0x7a)
		goto LAB_00402262;
	      bVar2 = true;
	    }
	  goto LAB_0040226c;
	}
      if (iVar5 == 0x67)
	{
	  bStack50 = 1;
	  goto LAB_0040226c;
	}
      if (iVar5 == 0x6e)
	{
	  DAT_00616d11 = 1;
	  goto LAB_0040226c;
	}
    }
LAB_00402262:
  ;
  imperfection_wrapper ();	//  FUN_00402026();
  goto LAB_0040226c;
}


#include "chcon.h"

long null_ARRAY_0061d77_0_8_;
long null_ARRAY_0061d77_8_8_;
long null_ARRAY_0061d90_0_8_;
long null_ARRAY_0061d90_16_8_;
long null_ARRAY_0061d90_24_8_;
long null_ARRAY_0061d90_32_8_;
long null_ARRAY_0061d90_40_8_;
long null_ARRAY_0061d90_48_8_;
long null_ARRAY_0061d90_8_8_;
long null_ARRAY_0061da4_0_4_;
long null_ARRAY_0061da4_16_8_;
long null_ARRAY_0061da4_4_4_;
long null_ARRAY_0061da4_8_4_;
long DAT_00418a4b;
long DAT_00418b05;
long DAT_00418b83;
long DAT_00418c09;
long DAT_00418c0e;
long DAT_00418c13;
long DAT_00418ee5;
long DAT_00418f9a;
long DAT_0041995f;
long DAT_004199a8;
long DAT_00419ade;
long DAT_00419ae2;
long DAT_00419ae7;
long DAT_00419ae9;
long DAT_0041a140;
long DAT_0041a15b;
long DAT_0041a520;
long DAT_0041a55d;
long DAT_0041a562;
long DAT_0041a578;
long DAT_0041a579;
long DAT_0041a57b;
long DAT_0041a63f;
long DAT_0041a6d0;
long DAT_0041a6d3;
long DAT_0041a721;
long DAT_0041a75b;
long DAT_0041a888;
long DAT_0041a889;
long DAT_0041a8fe;
long DAT_0041a91e;
long DAT_0041a976;
long DAT_0041a978;
long DAT_0041a97a;
long DAT_0041ab88;
long DAT_0061d2e0;
long DAT_0061d2f0;
long DAT_0061d300;
long DAT_0061d748;
long DAT_0061d760;
long DAT_0061d7d8;
long DAT_0061d7dc;
long DAT_0061d7e0;
long DAT_0061d800;
long DAT_0061d810;
long DAT_0061d820;
long DAT_0061d828;
long DAT_0061d840;
long DAT_0061d848;
long DAT_0061d890;
long DAT_0061d891;
long DAT_0061d892;
long DAT_0061d898;
long DAT_0061d8a0;
long DAT_0061d8a8;
long DAT_0061d8b0;
long DAT_0061d8b8;
long DAT_0061d8c0;
long DAT_0061d8e0;
long DAT_0061d8e8;
long DAT_0061d8f0;
long DAT_0061da78;
long DAT_0061da80;
long DAT_0061da84;
long DAT_0061da88;
long DAT_0061da90;
long DAT_0061da98;
long DAT_0061daa8;
long fde_0041b9c8;
long FLOAT_UNKNOWN;
long null_ARRAY_00418c40;
long null_ARRAY_0041a770;
long null_ARRAY_0041ade0;
long null_ARRAY_0061d770;
long null_ARRAY_0061d7a0;
long null_ARRAY_0061d860;
long null_ARRAY_0061d8d0;
long null_ARRAY_0061d900;
long null_ARRAY_0061d940;
long null_ARRAY_0061da40;
long PTR_DAT_0061d740;
long PTR_null_ARRAY_0061d780;
long stack0x00000008;
ulong
FUN_00402b01 (uint uParm1)
{
  char cVar1;
  int iVar2;
  uint *puVar3;
  char *pcVar4;
  uint uVar5;
  undefined8 uVar6;
  undefined8 uStack88;
  int iStack76;
  long lStack72;
  char cStack58;
  char cStack57;
  int iStack56;
  uint uStack52;
  code *pcStack32;
  uint local_c;

  if (uParm1 == 0)
    {
      pcStack32 = (code *) 0x402b5e;
      local_c = uParm1;
      func_0x004018c0
	("Usage: %s [OPTION]... CONTEXT FILE...\n  or:  %s [OPTION]... [-u USER] [-r ROLE] [-l RANGE] [-t TYPE] FILE...\n  or:  %s [OPTION]... --reference=RFILE FILE...\n",
	 DAT_0061d8f0, DAT_0061d8f0, DAT_0061d8f0);
      pcStack32 = (code *) 0x402b72;
      func_0x00401ae0
	("Change the SELinux security context of each FILE to CONTEXT.\nWith --reference, change the security context of each FILE to that of RFILE.\n",
	 DAT_0061d800);
      pcStack32 = (code *) 0x402b77;
      FUN_00401f6e ();
      pcStack32 = (code *) 0x402b8b;
      func_0x00401ae0
	("      --dereference      affect the referent of each symbolic link (this is\n                         the default), rather than the symbolic link itself\n  -h, --no-dereference   affect symbolic links instead of any referenced file\n",
	 DAT_0061d800);
      pcStack32 = (code *) 0x402b9f;
      func_0x00401ae0
	("  -u, --user=USER        set user USER in the target security context\n  -r, --role=ROLE        set role ROLE in the target security context\n  -t, --type=TYPE        set type TYPE in the target security context\n  -l, --range=RANGE      set range RANGE in the target security context\n",
	 DAT_0061d800);
      pcStack32 = (code *) 0x402bb3;
      func_0x00401ae0
	("      --no-preserve-root  do not treat \'/\' specially (the default)\n      --preserve-root    fail to operate recursively on \'/\'\n",
	 DAT_0061d800);
      pcStack32 = (code *) 0x402bc7;
      func_0x00401ae0
	("      --reference=RFILE  use RFILE\'s security context rather than specifying\n                         a CONTEXT value\n",
	 DAT_0061d800);
      pcStack32 = (code *) 0x402bdb;
      func_0x00401ae0
	("  -R, --recursive        operate on files and directories recursively\n",
	 DAT_0061d800);
      pcStack32 = (code *) 0x402bef;
      func_0x00401ae0
	("  -v, --verbose          output a diagnostic for every file processed\n",
	 DAT_0061d800);
      pcStack32 = (code *) 0x402c03;
      func_0x00401ae0
	("\nThe following options modify how a hierarchy is traversed when the -R\noption is also specified.  If more than one is specified, only the final\none takes effect.\n\n  -H                     if a command line argument is a symbolic link\n                         to a directory, traverse it\n  -L                     traverse every symbolic link to a directory\n                         encountered\n  -P                     do not traverse any symbolic links (default)\n\n",
	 DAT_0061d800);
      pcStack32 = (code *) 0x402c17;
      func_0x00401ae0 ("      --help     display this help and exit\n",
		       DAT_0061d800);
      pcStack32 = (code *) 0x402c2b;
      pcVar4 = (char *) DAT_0061d800;
      func_0x00401ae0
	("      --version  output version information and exit\n");
      pcStack32 = (code *) 0x402c35;
      imperfection_wrapper ();	//    FUN_00401f88();
    }
  else
    {
      pcVar4 = "Try \'%s --help\' for more information.\n";
      pcStack32 = (code *) 0x402b32;
      local_c = uParm1;
      func_0x00401ad0 (DAT_0061d820,
		       "Try \'%s --help\' for more information.\n",
		       DAT_0061d8f0);
    }
  pcStack32 = FUN_00402c3f;
  uVar5 = local_c;
  func_0x00401cb0 ();
  uStack52 = 0x10;
  iStack56 = -1;
  cStack57 = '\0';
  cStack58 = '\0';
  lStack72 = 0;
  pcStack32 = (code *) & stack0xfffffffffffffff8;
  FUN_00403282 (*(undefined8 *) pcVar4);
  func_0x00401c50 (6, &DAT_00418b83);
  func_0x00401c30 (FUN_0040319d);
LAB_00402e93:
  ;
  while (true)
    {
      uVar6 = 0x402eb1;
      imperfection_wrapper ();	//    iStack76 = FUN_00408e59((ulong)uVar5, pcVar4, "HLPRhvu:r:t:l:", null_ARRAY_00418c40, 0);
      if (iStack76 == -1)
	{
	  if (DAT_0061d891 == '\0')
	    {
	      uStack52 = 0x10;
	      DAT_0061d890 = iStack56 != 0;
	    }
	  else
	    {
	      if (uStack52 == 0x10)
		{
		  if (iStack56 == 1)
		    {
		      imperfection_wrapper ();	//            FUN_004053c8(1, 0, "-R --dereference requires either -H or -L");
		    }
		  DAT_0061d890 = false;
		}
	      else
		{
		  if (iStack56 == 0)
		    {
		      imperfection_wrapper ();	//            FUN_004053c8(1, 0, "-R -h requires -P");
		    }
		  DAT_0061d890 = true;
		}
	    }
	  if ((lStack72 == 0) && (cStack58 == '\0'))
	    {
	      iVar2 = 2;
	    }
	  else
	    {
	      iVar2 = 1;
	    }
	  if ((int) (uVar5 - DAT_0061d7d8) < iVar2)
	    {
	      if (DAT_0061d7d8 < (int) uVar5)
		{
		  imperfection_wrapper ();	//          uVar6 = FUN_0040486d(((undefined8 *)pcVar4)[(long)(int)uVar5 + -1]);
		  imperfection_wrapper ();	//          FUN_004053c8(0, 0, "missing operand after %s", uVar6);
		}
	      else
		{
		  imperfection_wrapper ();	//          FUN_004053c8(0, 0, "missing operand");
		}
	      FUN_00402b01 (1);
	    }
	  if (lStack72 == 0)
	    {
	      if (cStack58 == '\0')
		{
		  DAT_0061d8a0 = ((undefined8 *) pcVar4)[(long) DAT_0061d7d8];
		  DAT_0061d7d8 = DAT_0061d7d8 + 1;
		  uVar6 = FUN_00402124 (DAT_0061d8a0);
		  imperfection_wrapper ();	//          iVar2 = FUN_00404a43(uVar6);
		  if (iVar2 < 0)
		    {
		      imperfection_wrapper ();	//            uVar6 = FUN_0040486d(DAT_0061d8a0);
		      puVar3 = (uint *) func_0x00401ca0 ();
		      imperfection_wrapper ();	//            FUN_004053c8(1, (ulong)*puVar3, "invalid context: %s", uVar6);
		    }
		}
	      else
		{
		  DAT_0061d8a0 = 0;
		}
	    }
	  else
	    {
	      uStack88 = 0;
	      imperfection_wrapper ();	//        iVar2 = FUN_004049bb(lStack72, &uStack88, &uStack88);
	      if (iVar2 < 0)
		{
		  imperfection_wrapper ();	//          uVar6 = FUN_0040467c(4, lStack72);
		  puVar3 = (uint *) func_0x00401ca0 ();
		  imperfection_wrapper ();	//          FUN_004053c8(1, (ulong)*puVar3, "failed to get security context of %s", uVar6);
		}
	      DAT_0061d8a0 = uStack88;
	    }
	  if ((lStack72 != 0) && (cStack58 != '\0'))
	    {
	      imperfection_wrapper ();	//        FUN_004053c8(0, 0, "conflicting security context specifiers given");
	      FUN_00402b01 (1);
	    }
	  if ((DAT_0061d891 == '\0') || (cStack57 == '\0'))
	    {
	      DAT_0061d898 = 0;
	    }
	  else
	    {
	      DAT_0061d898 = FUN_0040488c (null_ARRAY_0061d8d0);
	      if (DAT_0061d898 == 0)
		{
		  imperfection_wrapper ();	//          uVar6 = FUN_0040467c(4, &DAT_00418ee5);
		  puVar3 = (uint *) func_0x00401ca0 ();
		  imperfection_wrapper ();	//          FUN_004053c8(1, (ulong)*puVar3, "failed to get attributes of %s", uVar6);
		}
	    }
	  imperfection_wrapper ();	//      cVar1 = FUN_00402a30((undefined8 *)pcVar4 + (long)DAT_0061d7d8, (ulong)(uStack52 | 8), (ulong)(uStack52 | 8), (long)DAT_0061d7d8 * 8);
	  return (ulong) (cVar1 == '\0');
	}
      if (iStack76 != 0x6c)
	break;
      DAT_0061d8b8 = DAT_0061daa8;
      cStack58 = '\x01';
    }
  if (iStack76 < 0x6d)
    {
      if (iStack76 == 0x4c)
	{
	  uStack52 = 2;
	  goto LAB_00402e93;
	}
      if (iStack76 < 0x4d)
	{
	  if (iStack76 == -0x82)
	    {
	      uVar6 = 0x402e41;
	      FUN_00402b01 (0);
	    }
	  else
	    {
	      if (iStack76 == 0x48)
		{
		  uStack52 = 0x11;
		  goto LAB_00402e93;
		}
	      if (iStack76 != -0x83)
		goto LAB_00402e89;
	    }
	  imperfection_wrapper ();	//      FUN_00404f97(DAT_0061d800, "chcon", "GNU coreutils", PTR_DAT_0061d740, "Russell Coker", "Jim Meyering", 0, uVar6);
	  func_0x00401cb0 (0);
	}
      else
	{
	  if (iStack76 == 0x52)
	    {
	      DAT_0061d891 = '\x01';
	      goto LAB_00402e93;
	    }
	  if (iStack76 < 0x53)
	    {
	      if (iStack76 == 0x50)
		{
		  uStack52 = 0x10;
		  goto LAB_00402e93;
		}
	    }
	  else
	    {
	      if (iStack76 == 0x66)
		goto LAB_00402e93;
	      if (iStack76 == 0x68)
		{
		  iStack56 = 0;
		  goto LAB_00402e93;
		}
	    }
	}
    }
  else
    {
      if (iStack76 == 0x76)
	{
	  DAT_0061d892 = 1;
	  goto LAB_00402e93;
	}
      if (iStack76 < 0x77)
	{
	  if (iStack76 == 0x74)
	    {
	      DAT_0061d8c0 = DAT_0061daa8;
	      cStack58 = '\x01';
	      goto LAB_00402e93;
	    }
	  if (0x74 < iStack76)
	    {
	      DAT_0061d8a8 = DAT_0061daa8;
	      cStack58 = '\x01';
	      goto LAB_00402e93;
	    }
	  if (iStack76 == 0x72)
	    {
	      DAT_0061d8b0 = DAT_0061daa8;
	      cStack58 = '\x01';
	      goto LAB_00402e93;
	    }
	}
      else
	{
	  if (iStack76 == 0x81)
	    {
	      cStack57 = '\0';
	      goto LAB_00402e93;
	    }
	  if (0x81 < iStack76)
	    {
	      if (iStack76 == 0x82)
		{
		  cStack57 = '\x01';
		}
	      else
		{
		  if (iStack76 != 0x83)
		    goto LAB_00402e89;
		  lStack72 = DAT_0061daa8;
		}
	      goto LAB_00402e93;
	    }
	  if (iStack76 == 0x80)
	    {
	      iStack56 = 1;
	      goto LAB_00402e93;
	    }
	}
    }
LAB_00402e89:
  ;
  imperfection_wrapper ();	//  FUN_00402b01();
  goto LAB_00402e93;
}

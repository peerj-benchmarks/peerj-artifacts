
#include "stdbuf.h"

long null_ARRAY_0061687_0_8_;
long null_ARRAY_0061687_8_8_;
long null_ARRAY_00616ac_0_8_;
long null_ARRAY_00616ac_16_8_;
long null_ARRAY_00616ac_24_8_;
long null_ARRAY_00616ac_32_8_;
long null_ARRAY_00616ac_40_8_;
long null_ARRAY_00616ac_48_8_;
long null_ARRAY_00616ac_8_8_;
long null_ARRAY_00616c0_0_4_;
long null_ARRAY_00616c0_16_8_;
long null_ARRAY_00616c0_4_4_;
long null_ARRAY_00616c0_8_4_;
long DAT_00412e4b;
long DAT_00412f05;
long DAT_00412f83;
long DAT_00413559;
long DAT_0041355e;
long DAT_004136b4;
long DAT_0041378b;
long DAT_004137a8;
long DAT_004137f0;
long DAT_0041391e;
long DAT_00413922;
long DAT_00413927;
long DAT_00413929;
long DAT_00413f93;
long DAT_00414360;
long DAT_004146f0;
long DAT_004146f5;
long DAT_0041475f;
long DAT_004147f0;
long DAT_004147f3;
long DAT_00414841;
long DAT_00414845;
long DAT_004148b8;
long DAT_004148b9;
long DAT_00414aa8;
long DAT_00616440;
long DAT_00616450;
long DAT_00616460;
long DAT_00616848;
long DAT_00616860;
long DAT_006168d8;
long DAT_006168dc;
long DAT_006168e0;
long DAT_00616900;
long DAT_00616940;
long DAT_00616950;
long DAT_00616960;
long DAT_00616968;
long DAT_00616980;
long DAT_00616988;
long DAT_00616a00;
long DAT_00616a88;
long DAT_00616a90;
long DAT_00616a98;
long DAT_00616c38;
long DAT_00616c40;
long DAT_00616c48;
long DAT_00616c50;
long DAT_00616c60;
long fde_00415660;
long FLOAT_UNKNOWN;
long null_ARRAY_00413000;
long null_ARRAY_00414d00;
long null_ARRAY_00414f00;
long null_ARRAY_00616870;
long null_ARRAY_006168a0;
long null_ARRAY_006169a0;
long null_ARRAY_00616a40;
long null_ARRAY_00616ac0;
long null_ARRAY_00616b00;
long null_ARRAY_00616c00;
long PTR_DAT_00616840;
long PTR_null_ARRAY_00616880;
void
FUN_00401ef0 (uint uParm1)
{
  int iVar1;
  long lVar2;
  undefined8 uVar3;
  ulong uVar4;
  long lStack48;
  long lStack40;

  if (uParm1 == 0)
    {
      func_0x00401690 ("Usage: %s OPTION... COMMAND\n", DAT_00616a98);
      func_0x00401840
	("Run COMMAND, with modified buffering operations for its standard streams.\n",
	 DAT_00616900);
      FUN_00401cb6 ();
      func_0x00401840
	("  -i, --input=MODE   adjust standard input stream buffering\n  -o, --output=MODE  adjust standard output stream buffering\n  -e, --error=MODE   adjust standard error stream buffering\n",
	 DAT_00616900);
      func_0x00401840 ("      --help     display this help and exit\n",
		       DAT_00616900);
      func_0x00401840
	("      --version  output version information and exit\n",
	 DAT_00616900);
      func_0x00401840
	("\nIf MODE is \'L\' the corresponding stream will be line buffered.\nThis option is invalid with standard input.\n",
	 DAT_00616900);
      func_0x00401840
	("\nIf MODE is \'0\' the corresponding stream will be unbuffered.\n",
	 DAT_00616900);
      func_0x00401840
	("\nOtherwise MODE is a number which may be followed by one of the following:\nKB 1000, K 1024, MB 1000*1000, M 1024*1024, and so on for G, T, P, E, Z, Y.\nIn this case the corresponding stream will be fully buffered with the buffer\nsize set to MODE bytes.\n",
	 DAT_00616900);
      func_0x00401840
	("\nNOTE: If COMMAND adjusts the buffering of its standard streams (\'tee\' does\nfor example) then that will override corresponding changes by \'stdbuf\'.\nAlso some filters (like \'dd\' and \'cat\' etc.) don\'t use streams for I/O,\nand are thus unaffected by \'stdbuf\' settings.\n",
	 DAT_00616900);
      imperfection_wrapper ();	//    FUN_00401cd0();
    }
  else
    {
      func_0x00401830 (DAT_00616960,
		       "Try \'%s --help\' for more information.\n",
		       DAT_00616a98);
    }
  uVar4 = (ulong) uParm1;
  func_0x004019f0 ();
  lVar2 = func_0x00401a80 (uVar4, 0x2f);
  if (lVar2 == 0)
    {
      lStack40 = FUN_0040488d ("/proc/self/exe");
      if (lStack40 == 0)
	{
	  lStack40 = func_0x004016f0 (&DAT_00413559);
	  if (lStack40 != 0)
	    {
	      imperfection_wrapper ();	//        lStack40 = FUN_00404836(lStack40);
	      lStack48 = func_0x004018c0 (lStack40, &DAT_0041355e);
	      while (lStack48 != 0)
		{
		  imperfection_wrapper ();	//          uVar3 = FUN_00402b25(lStack48, uVar4, 0, uVar4);
		  iVar1 = func_0x004019d0 (uVar3, 1);
		  if (iVar1 == 0)
		    {
		      DAT_00616a00 = FUN_00402920 (uVar3);
		      imperfection_wrapper ();	//            FUN_00401b00(uVar3);
		      break;
		    }
		  imperfection_wrapper ();	//          FUN_00401b00(uVar3);
		  lStack48 = func_0x004018c0 (0, &DAT_0041355e);
		}
	    }
	}
      else
	{
	  DAT_00616a00 = FUN_00402920 (lStack40);
	}
      imperfection_wrapper ();	//    FUN_00401b00(lStack40);
    }
  else
    {
      DAT_00616a00 = FUN_00402920 (uVar4);
    }
  return;
}


#include "join.h"

long DAT_00618aa_0_1_;
long DAT_00618aa_1_1_;
long null_ARRAY_0061891_0_8_;
long null_ARRAY_0061891_8_8_;
long null_ARRAY_00618a6_0_8_;
long null_ARRAY_00618a6_8_8_;
long null_ARRAY_00618a8_0_8_;
long null_ARRAY_00618a8_8_8_;
long null_ARRAY_00618ad_16_8_;
long null_ARRAY_00618b8_0_8_;
long null_ARRAY_00618b8_16_8_;
long null_ARRAY_00618b8_24_8_;
long null_ARRAY_00618b8_32_8_;
long null_ARRAY_00618b8_40_8_;
long null_ARRAY_00618b8_48_8_;
long null_ARRAY_00618b8_8_8_;
long null_ARRAY_00618ce_0_4_;
long null_ARRAY_00618ce_16_8_;
long null_ARRAY_00618ce_4_4_;
long null_ARRAY_00618ce_8_4_;
long DAT_00414800;
long DAT_004148bd;
long DAT_0041493b;
long DAT_00415382;
long DAT_0041545e;
long DAT_004154cc;
long DAT_004154d1;
long DAT_00415547;
long DAT_00415549;
long DAT_0041554b;
long DAT_00415596;
long DAT_00415599;
long DAT_004155e8;
long DAT_0041571e;
long DAT_00415722;
long DAT_00415727;
long DAT_00415729;
long DAT_00415d93;
long DAT_00416160;
long DAT_00416566;
long DAT_0041656b;
long DAT_004165d7;
long DAT_00416668;
long DAT_0041666b;
long DAT_004166b9;
long DAT_00416728;
long DAT_00416729;
long DAT_006184b8;
long DAT_006184c8;
long DAT_006184d8;
long DAT_006188c0;
long DAT_006188c8;
long DAT_006188d8;
long DAT_006188dc;
long DAT_006188e8;
long DAT_00618900;
long DAT_00618978;
long DAT_0061897c;
long DAT_00618980;
long DAT_006189c0;
long DAT_006189c8;
long DAT_006189d0;
long DAT_006189e0;
long DAT_006189e8;
long DAT_00618a00;
long DAT_00618a08;
long DAT_00618aa0;
long DAT_00618aa1;
long DAT_00618aa2;
long DAT_00618aa3;
long DAT_00618aa4;
long DAT_00618aa5;
long DAT_00618aa8;
long DAT_00618ab0;
long DAT_00618ab8;
long DAT_00618ac0;
long DAT_00618ae8;
long DAT_00618b30;
long DAT_00618b31;
long DAT_00618b38;
long DAT_00618b40;
long DAT_00618b48;
long DAT_00618cc0;
long DAT_00618d18;
long DAT_00618d20;
long DAT_00618d28;
long DAT_00618d38;
long fde_00417508;
long FLOAT_UNKNOWN;
long null_ARRAY_004149c0;
long null_ARRAY_00416b60;
long null_ARRAY_00618910;
long null_ARRAY_00618940;
long null_ARRAY_00618a20;
long null_ARRAY_00618a60;
long null_ARRAY_00618a70;
long null_ARRAY_00618a80;
long null_ARRAY_00618a90;
long null_ARRAY_00618ad0;
long null_ARRAY_00618b00;
long null_ARRAY_00618b80;
long null_ARRAY_00618bc0;
long null_ARRAY_00618ce0;
long PTR_DAT_006188e0;
long PTR_null_ARRAY_006188d0;
long PTR_null_ARRAY_00618920;
void
FUN_00401f4d (uint uParm1)
{
  undefined8 uVar1;
  undefined8 extraout_RDX;
  char *pcVar2;
  ulong uVar3;

  if (uParm1 == 0)
    {
      func_0x00401740 ("Usage: %s [OPTION]... FILE1 FILE2\n", DAT_00618b48);
      func_0x004018f0
	("For each pair of input lines with identical join fields, write a line to\nstandard output.  The default join field is the first, delimited by blanks.\n",
	 DAT_006189c0);
      func_0x004018f0
	("\nWhen FILE1 or FILE2 (not both) is -, read standard input.\n",
	 DAT_006189c0);
      func_0x004018f0
	("\n  -a FILENUM        also print unpairable lines from file FILENUM, where\n                      FILENUM is 1 or 2, corresponding to FILE1 or FILE2\n  -e EMPTY          replace missing input fields with EMPTY\n",
	 DAT_006189c0);
      func_0x004018f0
	("  -i, --ignore-case  ignore differences in case when comparing fields\n  -j FIELD          equivalent to \'-1 FIELD -2 FIELD\'\n  -o FORMAT         obey FORMAT while constructing output line\n  -t CHAR           use CHAR as input and output field separator\n",
	 DAT_006189c0);
      func_0x004018f0
	("  -v FILENUM        like -a FILENUM, but suppress joined output lines\n  -1 FIELD          join on this FIELD of file 1\n  -2 FIELD          join on this FIELD of file 2\n  --check-order     check that the input is correctly sorted, even\n                      if all input lines are pairable\n  --nocheck-order   do not check that the input is correctly sorted\n  --header          treat the first line in each file as field headers,\n                      print them without trying to pair them\n",
	 DAT_006189c0);
      func_0x004018f0
	("  -z, --zero-terminated     line delimiter is NUL, not newline\n",
	 DAT_006189c0);
      func_0x004018f0 ("      --help     display this help and exit\n",
		       DAT_006189c0);
      func_0x004018f0
	("      --version  output version information and exit\n",
	 DAT_006189c0);
      pcVar2 = DAT_006189c0;
      func_0x004018f0
	("\nUnless -t CHAR is given, leading blanks separate fields and are ignored,\nelse fields are separated by CHAR.  Any FIELD is a field number counted\nfrom 1.  FORMAT is one or more comma or blank separated specifications,\neach being \'FILENUM.FIELD\' or \'0\'.  Default FORMAT outputs the join field,\nthe remaining fields from FILE1, the remaining fields from FILE2, all\nseparated by CHAR.  If FORMAT is the keyword \'auto\', then the first\nline of each file determines the number of fields output for each line.\n\nImportant: FILE1 and FILE2 must be sorted on the join fields.\nE.g., use \"sort -k 1b,1\" if \'join\' has no options,\nor use \"join -t \'\'\" if \'sort\' has no options.\nNote, comparisons honor the rules specified by \'LC_COLLATE\'.\nIf the input is not sorted and some lines cannot be joined, a\nwarning message will be given.\n");
      imperfection_wrapper ();	//    FUN_00401db1();
    }
  else
    {
      pcVar2 = "Try \'%s --help\' for more information.\n";
      func_0x004018e0 (DAT_006189e0,
		       "Try \'%s --help\' for more information.\n",
		       DAT_00618b48);
    }
  uVar3 = (ulong) uParm1;
  func_0x00401ad0 ();
  if (*(ulong *) (uVar3 + 0x20) <= *(ulong *) (uVar3 + 0x18))
    {
      imperfection_wrapper ();	//    uVar1 = FUN_00406214(*(undefined8 *)(uVar3 + 0x28), uVar3 + 0x20, 0x10, uVar3 + 0x20);
      *(undefined8 *) (uVar3 + 0x28) = uVar1;
    }
  *(char **) (*(long *) (uVar3 + 0x28) + *(long *) (uVar3 + 0x18) * 0x10) =
    pcVar2;
  *(undefined8 *) (*(long *) (uVar3 + 0x28) +
		   *(long *) (uVar3 + 0x18) * 0x10 + 8) = extraout_RDX;
  *(long *) (uVar3 + 0x18) = *(long *) (uVar3 + 0x18) + 1;
  return;
}


#include "tail.h"

long DAT_0041a983;
long DAT_0041aa3d;
long DAT_0041aabb;
long DAT_0041ab28;
long DAT_0041b7b5;
long DAT_0041b7ba;
long DAT_0041b7de;
long DAT_0041b89d;
long DAT_0041b979;
long DAT_0041baf0;
long DAT_0041bc2d;
long DAT_0041bc30;
long DAT_00620010;
long DAT_00620020;
long DAT_00620480;
long DAT_00620488;
long DAT_00620490;
long DAT_00620580;
long DAT_006205a0;
long DAT_006205c0;
long DAT_006205c8;
long DAT_00620610;
long DAT_00620611;
long DAT_00620612;
long DAT_00620613;
long DAT_00620614;
long DAT_00620615;
long DAT_00620616;
long DAT_00620618;
long DAT_0062061c;
long DAT_0062061d;
long DAT_0062061e;
long DAT_00620638;
long DAT_006207f8;
long fde_0041e150;
long null_ARRAY_0041ab10;
long null_ARRAY_0041ac00;
long null_ARRAY_006205e0;
long PTR_DAT_006204a0;
long PTR_FUN_006204a8;
long stack0x00000008;
void
FUN_00402ee4 (uint uParm1)
{
  int iVar1;
  ulong auStack192[16];
  undefined8 uStack64;
  undefined8 uStack56;
  ulong *puStack48;
  int iStack36;
  code *pcStack32;
  uint local_c;

  if (uParm1 == 0)
    {
      pcStack32 = (code *) 0x402f33;
      local_c = uParm1;
      func_0x004019d0 ("Usage: %s [OPTION]... [FILE]...\n", DAT_00620638);
      pcStack32 = (code *) 0x402f47;
      func_0x004019d0
	("Print the last %d lines of each FILE to standard output.\nWith more than one FILE, precede each with a header giving the file name.\n",
	 10);
      pcStack32 = (code *) 0x402f4c;
      FUN_004020ce ();
      pcStack32 = (code *) 0x402f51;
      FUN_004020e8 ();
      pcStack32 = (code *) 0x402f65;
      func_0x00401c40
	("  -c, --bytes=[+]NUM       output the last NUM bytes; or use -c +NUM to\n                             output starting with byte NUM of each file\n",
	 DAT_00620580);
      pcStack32 = (code *) 0x402f79;
      func_0x00401c40
	("  -f, --follow[={name|descriptor}]\n                           output appended data as the file grows;\n                             an absent option argument means \'descriptor\'\n  -F                       same as --follow=name --retry\n",
	 DAT_00620580);
      pcStack32 = (code *) 0x402f92;
      func_0x004019d0
	("  -n, --lines=[+]NUM       output the last NUM lines, instead of the last %d;\n                             or use -n +NUM to output starting with line NUM\n      --max-unchanged-stats=N\n                           with --follow=name, reopen a FILE which has not\n                             changed size after N (default %d) iterations\n                             to see if it has been unlinked or renamed\n                             (this is the usual case of rotated log files);\n                             with inotify, this option is rarely useful\n",
	 10, 5);
      pcStack32 = (code *) 0x402fa6;
      func_0x00401c40
	("      --pid=PID            with -f, terminate after process ID, PID dies\n  -q, --quiet, --silent    never output headers giving file names\n      --retry              keep trying to open a file if it is inaccessible\n",
	 DAT_00620580);
      pcStack32 = (code *) 0x402fba;
      func_0x00401c40
	("  -s, --sleep-interval=N   with -f, sleep for approximately N seconds\n                             (default 1.0) between iterations;\n                             with inotify and --pid=P, check process P at\n                             least once every N seconds\n  -v, --verbose            always output headers giving file names\n",
	 DAT_00620580);
      pcStack32 = (code *) 0x402fce;
      func_0x00401c40
	("  -z, --zero-terminated    line delimiter is NUL, not newline\n",
	 DAT_00620580);
      pcStack32 = (code *) 0x402fe2;
      func_0x00401c40 ("      --help     display this help and exit\n",
		       DAT_00620580);
      pcStack32 = (code *) 0x402ff6;
      func_0x00401c40
	("      --version  output version information and exit\n",
	 DAT_00620580);
      pcStack32 = (code *) 0x40300a;
      func_0x00401c40
	("\nNUM may have a multiplier suffix:\nb 512, kB 1000, K 1024, MB 1000*1000, M 1024*1024,\nGB 1000*1000*1000, G 1024*1024*1024, and so on for T, P, E, Z, Y.\n\n",
	 DAT_00620580);
      pcStack32 = (code *) 0x40301e;
      func_0x00401c40
	("With --follow (-f), tail defaults to following the file descriptor, which\nmeans that even if a tail\'ed file is renamed, tail will continue to track\nits end.  This default behavior is not desirable when you really want to\ntrack the actual name of the file, not the file descriptor (e.g., log\nrotation).  Use --follow=name in that case.  That causes tail to track the\nnamed file in a way that accommodates renaming, removal and creation.\n",
	 DAT_00620580);
      pcStack32 = (code *) 0x403028;
      FUN_00402102 (&DAT_0041b7b5);
    }
  else
    {
      pcStack32 = (code *) 0x402f15;
      local_c = uParm1;
      func_0x00401c20 (DAT_006205a0,
		       "Try \'%s --help\' for more information.\n",
		       DAT_00620638);
    }
  pcStack32 = FUN_00403032;
  func_0x00401e30 ((ulong) local_c);
  if (DAT_00620613 == '\x01')
    {
      uStack56 = 0;
      uStack64 = 0;
      iStack36 = 0x10;
      puStack48 = auStack192;
      while (iStack36 != 0)
	{
	  *puStack48 = 0;
	  iStack36 = iStack36 + -1;
	  puStack48 = puStack48 + 1;
	}
      auStack192[0] = auStack192[0] | 2;
      pcStack32 = (code *) & stack0xfffffffffffffff8;
      iVar1 = FUN_0040e0e1 (2, auStack192, 0, 0, &uStack64);
      if (iVar1 == 1)
	{
	  func_0x00401f20 (0xd);
	}
    }
  return;
}

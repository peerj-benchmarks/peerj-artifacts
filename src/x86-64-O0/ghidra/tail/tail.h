typedef unsigned char   undefined;

typedef unsigned char    byte;
typedef unsigned char    dwfenc;
typedef unsigned int    dword;
typedef unsigned long    qword;
typedef unsigned char    undefined1;
typedef unsigned short    undefined2;
typedef unsigned int    undefined4;
typedef unsigned long    undefined6;
typedef unsigned long    undefined8;
typedef unsigned short    word;
#define unkbyte9   unsigned long long
#define unkbyte10   unsigned long long
#define unkbyte11   unsigned long long
#define unkbyte12   unsigned long long
#define unkbyte13   unsigned long long
#define unkbyte14   unsigned long long
#define unkbyte15   unsigned long long
#define unkbyte16   unsigned long long

#define unkuint9   unsigned long long
#define unkuint10   unsigned long long
#define unkuint11   unsigned long long
#define unkuint12   unsigned long long
#define unkuint13   unsigned long long
#define unkuint14   unsigned long long
#define unkuint15   unsigned long long
#define unkuint16   unsigned long long

#define unkint9   long long
#define unkint10   long long
#define unkint11   long long
#define unkint12   long long
#define unkint13   long long
#define unkint14   long long
#define unkint15   long long
#define unkint16   long long

#define unkfloat1   float
#define unkfloat2   float
#define unkfloat3   float
#define unkfloat5   double
#define unkfloat6   double
#define unkfloat7   double
#define unkfloat9   long double
#define unkfloat11   long double
#define unkfloat12   long double
#define unkfloat13   long double
#define unkfloat14   long double
#define unkfloat15   long double
#define unkfloat16   long double

#define BADSPACEBASE   void
#define code   void

#define ulong long
#define true 1
#define bool int
#define false 0
#define float10 int
#define ushort int
#define undefined7 long
#define undefined2 long
#define uint5 long
#define uint6 long

// Add definition for uint
typedef unsigned int    uint;

#define sbyte char

extern imperfection_wrapper(void);
typedef struct eh_frame_hdr eh_frame_hdr, *Peh_frame_hdr;

struct eh_frame_hdr {
    byte eh_frame_hdr_version; // Exception Handler Frame Header Version
    dwfenc eh_frame_pointer_encoding; // Exception Handler Frame Pointer Encoding
    dwfenc eh_frame_desc_entry_count_encoding; // Encoding of # of Exception Handler FDEs
    dwfenc eh_frame_table_encoding; // Exception Handler Table Encoding
};

typedef struct fde_table_entry fde_table_entry, *Pfde_table_entry;

struct fde_table_entry {
    dword initial_loc; // Initial Location
    dword data_loc; // Data location
};

typedef enum Elf64_DynTag {
    DT_AUDIT=1879047932,
    DT_AUXILIARY=2147483645,
    DT_BIND_NOW=24,
    DT_CHECKSUM=1879047672,
    DT_CONFIG=1879047930,
    DT_DEBUG=21,
    DT_DEPAUDIT=1879047931,
    DT_ENCODING=32,
    DT_FEATURE_1=1879047676,
    DT_FILTER=2147483647,
    DT_FINI=13,
    DT_FINI_ARRAY=26,
    DT_FINI_ARRAYSZ=28,
    DT_FLAGS=30,
    DT_FLAGS_1=1879048187,
    DT_GNU_CONFLICT=1879047928,
    DT_GNU_CONFLICTSZ=1879047670,
    DT_GNU_HASH=1879047925,
    DT_GNU_LIBLIST=1879047929,
    DT_GNU_LIBLISTSZ=1879047671,
    DT_GNU_PRELINKED=1879047669,
    DT_HASH=4,
    DT_INIT=12,
    DT_INIT_ARRAY=25,
    DT_INIT_ARRAYSZ=27,
    DT_JMPREL=23,
    DT_MOVEENT=1879047674,
    DT_MOVESZ=1879047675,
    DT_MOVETAB=1879047934,
    DT_NEEDED=1,
    DT_NULL=0,
    DT_PLTGOT=3,
    DT_PLTPAD=1879047933,
    DT_PLTPADSZ=1879047673,
    DT_PLTREL=20,
    DT_PLTRELSZ=2,
    DT_POSFLAG_1=1879047677,
    DT_PREINIT_ARRAYSZ=33,
    DT_REL=17,
    DT_RELA=7,
    DT_RELACOUNT=1879048185,
    DT_RELAENT=9,
    DT_RELASZ=8,
    DT_RELCOUNT=1879048186,
    DT_RELENT=19,
    DT_RELSZ=18,
    DT_RPATH=15,
    DT_RUNPATH=29,
    DT_SONAME=14,
    DT_STRSZ=10,
    DT_STRTAB=5,
    DT_SYMBOLIC=16,
    DT_SYMENT=11,
    DT_SYMINENT=1879047679,
    DT_SYMINFO=1879047935,
    DT_SYMINSZ=1879047678,
    DT_SYMTAB=6,
    DT_TEXTREL=22,
    DT_TLSDESC_GOT=1879047927,
    DT_TLSDESC_PLT=1879047926,
    DT_VERDEF=1879048188,
    DT_VERDEFNUM=1879048189,
    DT_VERNEED=1879048190,
    DT_VERNEEDNUM=1879048191,
    DT_VERSYM=1879048176
} Elf64_DynTag;

typedef enum Elf_ProgramHeaderType {
    PT_DYNAMIC=2,
    PT_GNU_EH_FRAME=1685382480,
    PT_GNU_RELRO=1685382482,
    PT_GNU_STACK=1685382481,
    PT_INTERP=3,
    PT_LOAD=1,
    PT_NOTE=4,
    PT_NULL=0,
    PT_PHDR=6,
    PT_SHLIB=5,
    PT_TLS=7
} Elf_ProgramHeaderType;

typedef struct Elf64_Dyn Elf64_Dyn, *PElf64_Dyn;

struct Elf64_Dyn {
    enum Elf64_DynTag d_tag;
    qword d_val;
};

typedef struct Elf64_Shdr Elf64_Shdr, *PElf64_Shdr;

typedef enum Elf_SectionHeaderType {
    SHT_CHECKSUM=1879048184,
    SHT_DYNAMIC=6,
    SHT_DYNSYM=11,
    SHT_FINI_ARRAY=15,
    SHT_GNU_ATTRIBUTES=1879048181,
    SHT_GNU_HASH=1879048182,
    SHT_GNU_LIBLIST=1879048183,
    SHT_GNU_verdef=1879048189,
    SHT_GNU_verneed=1879048190,
    SHT_GNU_versym=1879048191,
    SHT_GROUP=17,
    SHT_HASH=5,
    SHT_INIT_ARRAY=14,
    SHT_NOBITS=8,
    SHT_NOTE=7,
    SHT_NULL=0,
    SHT_PREINIT_ARRAY=16,
    SHT_PROGBITS=1,
    SHT_REL=9,
    SHT_RELA=4,
    SHT_SHLIB=10,
    SHT_STRTAB=3,
    SHT_SUNW_COMDAT=1879048187,
    SHT_SUNW_move=1879048186,
    SHT_SUNW_syminfo=1879048188,
    SHT_SYMTAB=2,
    SHT_SYMTAB_SHNDX=18
} Elf_SectionHeaderType;

struct Elf64_Shdr {
    dword sh_name;
    enum Elf_SectionHeaderType sh_type;
    qword sh_flags;
    qword sh_addr;
    qword sh_offset;
    qword sh_size;
    dword sh_link;
    dword sh_info;
    qword sh_addralign;
    qword sh_entsize;
};

typedef struct Elf64_Phdr Elf64_Phdr, *PElf64_Phdr;

struct Elf64_Phdr {
    enum Elf_ProgramHeaderType p_type;
    dword p_flags;
    qword p_offset;
    qword p_vaddr;
    qword p_paddr;
    qword p_filesz;
    qword p_memsz;
    qword p_align;
};

typedef struct Elf64_Rela Elf64_Rela, *PElf64_Rela;

struct Elf64_Rela {
    qword r_offset; // location to apply the relocation action
    qword r_info; // the symbol table index and the type of relocation
    qword r_addend; // a constant addend used to compute the relocatable field value
};

typedef struct Elf64_Sym Elf64_Sym, *PElf64_Sym;

struct Elf64_Sym {
    dword st_name;
    byte st_info;
    byte st_other;
    word st_shndx;
    qword st_value;
    qword st_size;
};

typedef struct Elf64_Ehdr Elf64_Ehdr, *PElf64_Ehdr;

struct Elf64_Ehdr {
    byte e_ident_magic_num;
    char e_ident_magic_str[3];
    byte e_ident_class;
    byte e_ident_data;
    byte e_ident_version;
    byte e_ident_pad[9];
    word e_type;
    word e_machine;
    dword e_version;
    qword e_entry;
    qword e_phoff;
    qword e_shoff;
    dword e_flags;
    word e_ehsize;
    word e_phentsize;
    word e_phnum;
    word e_shentsize;
    word e_shnum;
    word e_shstrndx;
};




undefined8 _DT_INIT(void);
void FUN_004019b0(void);
void entry(void);
void FUN_00401f90(void);
void FUN_00402010(void);
void FUN_00402090(void);
void FUN_004020ce(void);
void FUN_004020e8(void);
void FUN_00402102(undefined *puParm1);
ulong FUN_0040229e(long lParm1);
undefined8 FUN_004022dd(ulong uParm1);
void FUN_00402ee4(uint uParm1);
void FUN_00403032(void);
ulong FUN_004030d8(long lParm1);
char * FUN_00403108(char **ppcParm1);
void FUN_0040313c(long lParm1,undefined4 uParm2,undefined8 uParm3,undefined8 *puParm4,undefined4 uParm5);
void FUN_004031d8(uint uParm1,undefined8 uParm2);
void FUN_00403243(undefined8 uParm1);
void FUN_00403288(undefined8 uParm1,ulong uParm2);
long FUN_0040330a(char cParm1,undefined8 uParm2,uint uParm3,ulong uParm4);
long FUN_00403442(uint uParm1,undefined8 uParm2,uint uParm3,undefined8 uParm4);
undefined8 FUN_00403590(undefined8 uParm1,uint uParm2,long lParm3,long lParm4,long lParm5,long *plParm6);
ulong FUN_0040390d(undefined8 uParm1,uint uParm2,ulong uParm3,long *plParm4);
ulong FUN_00403dc6(undefined8 uParm1,uint uParm2,ulong uParm3,long *plParm4);
undefined8 FUN_004040d5(undefined8 uParm1,uint uParm2,ulong uParm3,long *plParm4);
undefined8 FUN_00404206(undefined8 uParm1,uint uParm2,long lParm3,long *plParm4);
ulong FUN_0040437f(uint uParm1,undefined8 uParm2);
void FUN_00404453(undefined8 *puParm1,byte bParm2);
undefined8 FUN_00404afa(long lParm1,ulong uParm2);
void FUN_00404ba4(undefined8 uParm1,long lParm2,ulong uParm3);
undefined8 FUN_00405488(long lParm1,ulong uParm2);
undefined8 FUN_00405500(long lParm1,ulong uParm2);
undefined8 FUN_0040557b(long lParm1,ulong uParm2);
undefined8 FUN_00405607(long lParm1,ulong uParm2);
undefined8 FUN_004056b7(long lParm1,ulong uParm2);
ulong FUN_00405743(long lParm1,ulong uParm2);
ulong FUN_0040576e(long lParm1,long lParm2);
void FUN_0040579f(long lParm1,long *plParm2);
undefined8 FUN_004059e7(undefined auParm1 [16],uint uParm2,long lParm3,ulong uParm4);
ulong FUN_004069de(undefined8 uParm1,uint uParm2,ulong uParm3,long *plParm4);
ulong FUN_00406d11(undefined8 uParm1,uint uParm2,undefined8 uParm3,long *plParm4);
void FUN_00406f3e(undefined8 uParm1,uint uParm2,undefined8 uParm3,undefined8 *puParm4);
ulong FUN_00406f9d(undefined8 *puParm1,undefined8 uParm2);
undefined8 FUN_00407403(int iParm1,long lParm2,long *plParm3);
void FUN_00407668(uint uParm1,undefined8 uParm2,undefined8 *puParm3,undefined4 *puParm4,double *pdParm5);
long FUN_00407b3f(long lParm1,ulong uParm2);
ulong FUN_00407cac(uint uParm1,undefined8 *puParm2);
void FUN_0040835f(void);
long FUN_0040836f(undefined8 uParm1,long lParm2,long lParm3,long lParm4);
void FUN_0040849e(undefined8 uParm1,undefined8 uParm2,long lParm3);
void FUN_0040851f(long lParm1,long lParm2,long lParm3);
long FUN_00408655(undefined8 uParm1,undefined8 uParm2,undefined8 uParm3,undefined8 uParm4,undefined8 uParm5,code *pcParm6);
long FUN_004086db(void);
void FUN_0040870f(undefined8 uParm1,undefined8 *puParm2);
void FUN_00408777(void);
ulong FUN_0040885c(char *pcParm1);
char * FUN_004088e5(char *pcParm1);
void FUN_0040894e(undefined4 uParm1,undefined4 uParm2,undefined4 uParm3,undefined4 uParm4,undefined4 uParm5,undefined4 uParm6,undefined4 uParm7,undefined4 uParm8,undefined8 param_1,uint param_2,undefined8 param_3,undefined8 param_4,undefined8 param_5,undefined8 param_6);
undefined8 FUN_00408a5d(long lParm1);
long FUN_00408a6f(long *plParm1,undefined8 uParm2);
long FUN_00408ac6(long lParm1,long lParm2);
ulong FUN_00408b59(ulong uParm1);
ulong FUN_00408bc5(ulong uParm1);
ulong FUN_00408c0c(undefined8 uParm1,ulong uParm2);
ulong FUN_00408c43(ulong uParm1,ulong uParm2);
undefined8 FUN_00408c5c(long lParm1);
ulong FUN_00408d55(ulong uParm1,long lParm2);
long * FUN_00408e4b(undefined8 uParm1,undefined1 *puParm2,code *pcParm3,code *pcParm4,long lParm5);
void FUN_00408fb3(long **pplParm1,undefined8 uParm2);
long FUN_004090dd(long lParm1);
void FUN_00409128(long lParm1,undefined8 *puParm2);
long FUN_0040915d(long lParm1,long lParm2,long **pplParm3,char cParm4);
undefined8 FUN_004092f2(long lParm1,long **pplParm2,char cParm3);
undefined8 FUN_004094c0(long *plParm1,undefined8 uParm2);
undefined8 FUN_004096c4(long lParm1,long lParm2,long *plParm3);
undefined8 FUN_00409a16(undefined8 uParm1,undefined8 uParm2);
long FUN_00409a5f(long lParm1,undefined8 uParm2);
char * FUN_00409d3e(long lParm1,long lParm2);
long FUN_00409e70(void);
void FUN_00409ef4(long lParm1);
ulong FUN_00409fd1(undefined1 *puParm1,byte bParm2,uint uParm3);
undefined8 * FUN_0040a059(undefined8 *puParm1,int iParm2);
char * FUN_0040a0d0(char *pcParm1,int iParm2);
ulong FUN_0040a170(long param_1,ulong param_2,long param_3,ulong param_4,uint param_5,uint param_6,long param_7,char *param_8,char *param_9);
undefined1 * FUN_0040b03a(int iParm1,undefined8 uParm2,undefined8 uParm3,uint *puParm4);
void FUN_0040b2ad(uint uParm1,uint uParm2,undefined8 uParm3);
void FUN_0040b2ee(uint uParm1,undefined8 uParm2);
void FUN_0040b312(undefined8 uParm1,undefined8 uParm2,char cParm3);
void FUN_0040b3a6(undefined8 uParm1,char cParm2);
void FUN_0040b3d0(undefined8 uParm1);
void FUN_0040b3ef(uint uParm1,uint uParm2,undefined8 uParm3);
void FUN_0040b48a(uint uParm1,undefined8 uParm2,undefined8 uParm3);
void FUN_0040b4b6(uint uParm1,undefined8 uParm2);
void FUN_0040b4df(undefined8 uParm1);
long FUN_0040b4fe(uint uParm1,undefined8 uParm2,undefined8 uParm3);
void FUN_0040b56f(long lParm1);
ulong FUN_0040b585(uint uParm1);
void FUN_0040b595(undefined8 uParm1,undefined8 uParm2);
ulong FUN_0040b5bb(long lParm1,int iParm2,long lParm3,int iParm4);
ulong FUN_0040b642(uint uParm1);
void FUN_0040b692(undefined8 uParm1,long lParm2,undefined8 uParm3,undefined8 uParm4,undefined8 *puParm5,undefined8 uParm6);
void FUN_0040baf6(undefined8 uParm1,undefined8 uParm2,undefined8 uParm3,undefined8 uParm4,uint *puParm5);
void FUN_0040bbc8(undefined4 uParm1,undefined4 uParm2,undefined4 uParm3,undefined4 uParm4,undefined4 uParm5,undefined4 uParm6,undefined4 uParm7,undefined4 uParm8,undefined8 param_1,undefined8 param_2,undefined8 param_3,undefined8 param_4,undefined8 param_5,undefined8 param_6);
void FUN_0040bc7e(ulong uParm1,ulong uParm2);
void FUN_0040bcbf(undefined8 uParm1);
long FUN_0040bcd9(long lParm1);
long FUN_0040bd0e(long lParm1,long lParm2);
void FUN_0040bd6f(void);
void FUN_0040bd99(void);
void FUN_0040bd9f(uint uParm1,uint uParm2);
ulong FUN_0040bdc7(undefined8 param_1,uint param_2,ulong param_3,ulong param_4,undefined8 param_5,undefined8 param_6,uint param_7);
void FUN_0040bed9(undefined8 uParm1,undefined8 uParm2,undefined8 uParm3,undefined8 uParm4,undefined8 uParm5,uint uParm6);
undefined8 FUN_0040bf2e(undefined8 uParm1);
ulong FUN_0040bf9f(char *pcParm1,char **ppcParm2,double *pdParm3,code *pcParm4);
undefined8 FUN_0040c04e(int iParm1);
ulong FUN_0040c074(ulong *puParm1,int iParm2);
ulong FUN_0040c0d3(undefined8 uParm1,uint uParm2,int iParm3);
ulong FUN_0040c114(byte *pbParm1,byte **ppbParm2,uint uParm3,undefined8 *puParm4,long lParm5);
undefined8 FUN_0040c4f5(ulong uParm1,ulong uParm2);
ulong FUN_0040c574(uint uParm1);
void FUN_0040c59d(void);
void FUN_0040c5d1(uint uParm1);
void FUN_0040c645(uint uParm1,uint uParm2,undefined8 uParm3,undefined8 uParm4);
void FUN_0040c6c9(undefined4 uParm1,undefined4 uParm2,undefined4 uParm3,undefined4 uParm4,undefined4 uParm5,undefined4 uParm6,undefined4 uParm7,undefined4 uParm8,uint param_1,uint param_2,undefined8 param_3,undefined8 param_4,undefined8 param_5,undefined8 param_6);
ulong FUN_0040c7ad(undefined4 uParm1,undefined4 uParm2,undefined4 uParm3,undefined4 uParm4,undefined4 uParm5,undefined4 uParm6,undefined4 uParm7,undefined4 uParm8,uint param_1,uint param_2,undefined8 param_3,undefined8 param_4,undefined8 param_5,undefined8 param_6);
void FUN_0040ca64(long lParm1,int *piParm2);
ulong FUN_0040cc48(int param_1,undefined8 *param_2,char *param_3,long *param_4,int *param_5,int param_6,int *param_7,int param_8,undefined8 param_9);
char * FUN_0040d232(undefined8 uParm1,undefined8 uParm2,char *pcParm3,int *piParm4,int iParm5);
ulong FUN_0040d300(uint param_1,undefined8 *param_2,char *param_3,long param_4,undefined8 param_5,uint param_6,uint *param_7,uint param_8);
ulong FUN_0040dac9(uint param_1,undefined8 param_2,undefined8 param_3,undefined8 param_4,undefined8 param_5,uint param_6,uint param_7);
void FUN_0040db59(uint uParm1,undefined8 uParm2,undefined8 uParm3,undefined8 uParm4,undefined8 uParm5);
ulong FUN_0040dba3(uint uParm1);
void FUN_0040dcf7(undefined8 uParm1,undefined8 uParm2);
ulong FUN_0040dd1c(long lParm1,long lParm2);
undefined8 FUN_0040ddd3(long lParm1);
ulong FUN_0040de04(uint *puParm1,byte *pbParm2,long lParm3,undefined8 uParm4);
ulong * FUN_0040de87(ulong *puParm1,char cParm2,ulong uParm3);
ulong FUN_0040df99(long *plParm1,long *plParm2);
undefined8 FUN_0040e078(long lParm1,long lParm2);
undefined8 FUN_0040e0e1(uint uParm1,long lParm2,long lParm3,long lParm4,undefined8 uParm5);
void FUN_0040e22e(undefined8 uParm1,undefined8 uParm2);
ulong FUN_0040e253(long lParm1,long lParm2);
ulong FUN_0040e2e3(undefined8 uParm1,undefined8 uParm2,undefined8 uParm3);
undefined8 FUN_0040e404(void);
undefined8 FUN_0040e415(void);
ulong FUN_0040e423(uint uParm1,uint uParm2);
ulong FUN_0040e45a(ulong uParm1,undefined4 uParm2);
ulong FUN_0040e476(byte *pbParm1,byte *pbParm2);
undefined8 FUN_0040e4ed(undefined8 uParm1);
void FUN_0040e578(undefined auParm1 [16]);
undefined8 FUN_0040e6da(void);
ulong FUN_0040e6e7(uint uParm1);
undefined1 * FUN_0040e7b8(void);
char * FUN_0040eb6b(void);
void FUN_0040ec39(uint uParm1);
ulong FUN_0040ec5f(undefined8 uParm1);
void FUN_0040ed17(undefined8 uParm1);
void FUN_0040ed3b(void);
ulong FUN_0040ed49(long lParm1);
undefined8 FUN_0040ee19(undefined8 uParm1);
void FUN_0040ee38(undefined8 uParm1,undefined8 uParm2,uint uParm3);
ulong FUN_0040ee63(void);
long FUN_0040eeb0(ulong uParm1,long lParm2,ulong uParm3,long lParm4,long *plParm5);
int * FUN_0040f0fc(ulong uParm1,uint *puParm2,ulong uParm3,uint *puParm4,ulong *puParm5);
char * FUN_0040fbdc(ulong uParm1,long lParm2,long lParm3);
long FUN_0040fe4a(int *param_1,long *param_2);
long FUN_00410035(undefined auParm1 [16],int *piParm2,long *plParm3);
undefined8 FUN_004103f4(int iParm1,long lParm2,uint *puParm3,long lParm4,uint uParm5);
void FUN_00410c07(uint param_1);
void FUN_00410c51(undefined8 uParm1,uint uParm2);
ulong FUN_00410ca3(void);
ulong FUN_00411035(undefined auParm1 [16],undefined8 uParm2,undefined8 uParm3);
ulong FUN_0041140d(char *pcParm1,long lParm2);
undefined8 FUN_00411466(long param_1,long param_2,undefined param_3,int param_4,undefined8 param_5,ulong param_6,int param_7,ulong param_8,int param_9);
long FUN_00411729(long lParm1,char **ppcParm2,long lParm3,undefined8 uParm4);
ulong FUN_00418b93(uint uParm1);
void FUN_00418bb2(undefined8 uParm1,int *piParm2,undefined8 uParm3);
void FUN_00418c53(int *param_1);
ulong FUN_00418d12(ulong uParm1,long lParm2);
void FUN_00418d46(undefined8 uParm1,undefined8 uParm2,undefined8 uParm3);
void FUN_00418d81(undefined8 uParm1,undefined8 uParm2,undefined8 uParm3,undefined8 uParm4);
ulong FUN_00418dd2(ulong uParm1,ulong uParm2);
undefined8 FUN_00418ded(uint *puParm1,ulong *puParm2);
undefined8 FUN_0041958d(char *pcParm1,ulong *puParm2,char **ppcParm3);
ulong FUN_0041a843(undefined auParm1 [16]);
ulong FUN_0041a87e(void);
void FUN_0041a8b0(void);
undefined8 _DT_FINI(void);
undefined FUN_00621000();
undefined FUN_00621008();
undefined FUN_00621010();
undefined FUN_00621018();
undefined FUN_00621020();
undefined FUN_00621028();
undefined FUN_00621030();
undefined FUN_00621038();
undefined FUN_00621040();
undefined FUN_00621048();
undefined FUN_00621050();
undefined FUN_00621058();
undefined FUN_00621060();
undefined FUN_00621068();
undefined FUN_00621070();
undefined FUN_00621078();
undefined FUN_00621080();
undefined FUN_00621088();
undefined FUN_00621090();
undefined FUN_00621098();
undefined FUN_006210a0();
undefined FUN_006210a8();
undefined FUN_006210b0();
undefined FUN_006210b8();
undefined FUN_006210c0();
undefined FUN_006210c8();
undefined FUN_006210d0();
undefined FUN_006210d8();
undefined FUN_006210e0();
undefined FUN_006210e8();
undefined FUN_006210f0();
undefined FUN_006210f8();
undefined FUN_00621100();
undefined FUN_00621108();
undefined FUN_00621110();
undefined FUN_00621118();
undefined FUN_00621120();
undefined FUN_00621128();
undefined FUN_00621130();
undefined FUN_00621138();
undefined FUN_00621140();
undefined FUN_00621148();
undefined FUN_00621150();
undefined FUN_00621158();
undefined FUN_00621160();
undefined FUN_00621168();
undefined FUN_00621170();
undefined FUN_00621178();
undefined FUN_00621180();
undefined FUN_00621188();
undefined FUN_00621190();
undefined FUN_00621198();
undefined FUN_006211a0();
undefined FUN_006211a8();
undefined FUN_006211b0();
undefined FUN_006211b8();
undefined FUN_006211c0();
undefined FUN_006211c8();
undefined FUN_006211d0();
undefined FUN_006211d8();
undefined FUN_006211e0();
undefined FUN_006211e8();
undefined FUN_006211f0();
undefined FUN_006211f8();
undefined FUN_00621200();
undefined FUN_00621208();
undefined FUN_00621210();
undefined FUN_00621218();
undefined FUN_00621220();
undefined FUN_00621228();
undefined FUN_00621230();
undefined FUN_00621238();
undefined FUN_00621240();
undefined FUN_00621248();
undefined FUN_00621250();
undefined FUN_00621258();
undefined FUN_00621260();
undefined FUN_00621268();
undefined FUN_00621270();
undefined FUN_00621278();
undefined FUN_00621280();
undefined FUN_00621288();
undefined FUN_00621290();
undefined FUN_00621298();
undefined FUN_006212a0();
undefined FUN_006212a8();
undefined FUN_006212b0();
undefined FUN_006212b8();
undefined FUN_006212c0();


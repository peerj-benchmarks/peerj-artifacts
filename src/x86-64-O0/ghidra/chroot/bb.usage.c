
#include "chroot.h"

long null_ARRAY_0061a4d_0_8_;
long null_ARRAY_0061a4d_8_8_;
long null_ARRAY_0061a64_0_8_;
long null_ARRAY_0061a64_16_8_;
long null_ARRAY_0061a64_24_8_;
long null_ARRAY_0061a64_32_8_;
long null_ARRAY_0061a64_40_8_;
long null_ARRAY_0061a64_48_8_;
long null_ARRAY_0061a64_8_8_;
long null_ARRAY_0061a78_0_4_;
long null_ARRAY_0061a78_16_8_;
long null_ARRAY_0061a78_4_4_;
long null_ARRAY_0061a78_8_4_;
long DAT_00416340;
long DAT_004163fd;
long DAT_0041647b;
long DAT_004165c0;
long DAT_004165e9;
long DAT_00416823;
long DAT_004168c5;
long DAT_004168c8;
long DAT_0041698d;
long DAT_004169a2;
long DAT_00416aa0;
long DAT_00416bde;
long DAT_00416be2;
long DAT_00416be7;
long DAT_00416be9;
long DAT_00417240;
long DAT_00417283;
long DAT_00417640;
long DAT_004179ce;
long DAT_004179d3;
long DAT_004179e6;
long DAT_004179e8;
long DAT_004179ea;
long DAT_00417a47;
long DAT_00417ad8;
long DAT_00417adb;
long DAT_00417b29;
long DAT_00417b2d;
long DAT_00417ba0;
long DAT_00417ba1;
long DAT_0061a000;
long DAT_0061a010;
long DAT_0061a020;
long DAT_0061a4a8;
long DAT_0061a4c0;
long DAT_0061a550;
long DAT_0061a554;
long DAT_0061a558;
long DAT_0061a580;
long DAT_0061a590;
long DAT_0061a5a0;
long DAT_0061a5a8;
long DAT_0061a5c0;
long DAT_0061a5c8;
long DAT_0061a610;
long DAT_0061a618;
long DAT_0061a620;
long DAT_0061a7b8;
long DAT_0061a7c0;
long DAT_0061a7c8;
long DAT_0061a7d0;
long DAT_0061a7d8;
long DAT_0061a7e8;
long fde_00418a38;
long FLOAT_UNKNOWN;
long null_ARRAY_00416500;
long null_ARRAY_004169b0;
long null_ARRAY_00417fe0;
long null_ARRAY_0061a4d0;
long null_ARRAY_0061a500;
long null_ARRAY_0061a5e0;
long null_ARRAY_0061a640;
long null_ARRAY_0061a680;
long null_ARRAY_0061a780;
long PTR_DAT_0061a4a0;
long PTR_null_ARRAY_0061a4e0;
long PTR_s_invalid_group_0061a548;
long PTR_s_invalid_spec_0061a538;
long PTR_s_invalid_user_0061a540;
long stack0x00000008;
long stack0xfffffffffffffff8;
ulong
FUN_004027ef (uint uParm1)
{
  char cVar1;
  int iVar2;
  undefined8 uVar3;
  uint *puVar4;
  long lVar5;
  int *piVar6;
  uint uVar7;
  char *pcStack224;
  undefined8 uStack208;
  long lStack200;
  undefined8 uStack192;
  uint uStack184;
  uint uStack180;
  uint uStack176;
  int iStack172;
  long *plStack168;
  long lStack160;
  int iStack152;
  undefined4 uStack148;
  long *plStack144;
  undefined8 uStack136;
  char cStack121;
  long lStack120;
  long lStack112;
  int iStack100;
  undefined8 uStack96;
  long lStack88;
  char cStack73;
  char *pcStack72;
  long lStack64;
  char *pcStack56;
  code *pcStack32;
  uint local_c;

  if (uParm1 == 0)
    {
      pcStack32 = (code *) 0x402845;
      local_c = uParm1;
      func_0x00401c00
	("Usage: %s [OPTION] NEWROOT [COMMAND [ARG]...]\n  or:  %s OPTION\n",
	 DAT_0061a620, DAT_0061a620);
      pcStack32 = (code *) 0x402859;
      func_0x00401e30 ("Run COMMAND with root directory set to NEWROOT.\n\n",
		       DAT_0061a580);
      pcStack32 = (code *) 0x40286d;
      func_0x00401e30
	("  --groups=G_LIST        specify supplementary groups as g1,g2,..,gN\n",
	 DAT_0061a580);
      pcStack32 = (code *) 0x402881;
      func_0x00401e30
	("  --userspec=USER:GROUP  specify user and group (ID or name) to use\n",
	 DAT_0061a580);
      pcStack32 = (code *) 0x402890;
      imperfection_wrapper ();	//    uVar3 = FUN_004060cb(4, &DAT_004165e9);
      pcStack32 = (code *) 0x4028a2;
      func_0x00401c00
	("  --skip-chdir           do not change working directory to %s\n",
	 uVar3);
      pcStack32 = (code *) 0x4028b6;
      func_0x00401e30 ("      --help     display this help and exit\n",
		       DAT_0061a580);
      pcStack32 = (code *) 0x4028ca;
      func_0x00401e30
	("      --version  output version information and exit\n",
	 DAT_0061a580);
      pcStack32 = (code *) 0x4028de;
      pcStack224 = (char *) DAT_0061a580;
      func_0x00401e30
	("\nIf no command is given, run \'\"$SHELL\" -i\' (default: \'/bin/sh -i\').\n");
      pcStack32 = (code *) 0x4028e8;
      imperfection_wrapper ();	//    FUN_0040238b();
    }
  else
    {
      pcStack224 = s_Try___s___help__for_more_informa_004165f0;
      pcStack32 = (code *) 0x402820;
      local_c = uParm1;
      func_0x00401e20 (DAT_0061a5a0,
		       s_Try___s___help__for_more_informa_004165f0,
		       DAT_0061a620);
    }
  pcStack32 = FUN_004028f2;
  uVar7 = local_c;
  func_0x00402050 ();
  pcStack56 = (char *) 0x0;
  lStack64 = 0;
  pcStack72 = (char *) 0x0;
  cStack73 = '\0';
  uStack180 = 0xffffffff;
  uStack184 = 0xffffffff;
  uStack192 = 0;
  lStack200 = 0;
  pcStack32 = (code *) & stack0xfffffffffffffff8;
  FUN_00404cd1 (*(long *) pcStack224);
  func_0x00402000 (6, &DAT_0041647b);
  FUN_0040233e (0x7d);
  func_0x00401fe0 (FUN_00403a10);
  while (1)
    {
      if (iStack100 == 0x100)
	{
	  pcStack72 = DAT_0061a7e8;
	}
      else
	{
	  if (iStack100 < 0x101)
	    {
	      if (iStack100 != -0x83)
		{
		  if (iStack100 != -0x82)
		    goto LAB_00402a6d;
		  FUN_004027ef (0);
		}
	      imperfection_wrapper ();	//        FUN_00406c02(DAT_0061a580, "chroot", "GNU coreutils", PTR_DAT_0061a4a0, "Roland McGrath", 0);
	      func_0x00402050 (0);
	    LAB_00402a6d:
	      ;
	      imperfection_wrapper ();	//        FUN_004027ef();
	    }
	  else
	    {
	      if (iStack100 == 0x101)
		{
		  pcStack56 = DAT_0061a7e8;
		  lStack112 = func_0x004020e0 ();
		  if ((lStack112 != 0) && (pcStack56[lStack112 + -1] == ':'))
		    {
		      pcStack56[lStack112 + -1] = '\0';
		    }
		}
	      else
		{
		  if (iStack100 != 0x102)
		    goto LAB_00402a6d;
		  cStack73 = '\x01';
		}
	    }
	}
    }
  if ((int) uVar7 <= DAT_0061a550)
    {
      imperfection_wrapper ();	//    FUN_004075b9(0, 0, "missing operand");
      FUN_004027ef (0x7d);
    }
  lStack120 = ((long *) pcStack224)[(long) DAT_0061a550];
  cStack121 = FUN_00402792 (lStack120);
  if ((cStack121 != '\x01') && (cStack73 != '\0'))
    {
      imperfection_wrapper ();	//    uVar3 = FUN_004060cb(4, &DAT_004165e9);
      imperfection_wrapper ();	//    FUN_004075b9(0, 0, "option --skip-chdir only permitted if NEWROOT is old %s", uVar3);
      FUN_004027ef (0x7d);
    }
  if (cStack121 != '\x01')
    {
      if (pcStack56 != (char *) 0x0)
	{
	  uStack136 = FUN_004065f9 (pcStack56, &uStack180, &uStack184, 0, 0);
	}
      cVar1 = FUN_00402535 ((ulong) uStack180);
      if ((cVar1 != '\x01')
	  &&
	  (((pcStack72 == (char *) 0x0
	     || (cVar1 = FUN_00402545 ((ulong) uStack184), cVar1 != '\0'))
	    && (plStack144 =
		(long *) func_0x00401f90 ((ulong) uStack180),
		plStack144 != (long *) 0x0))))
	{
	  cVar1 = FUN_00402545 ((ulong) uStack184);
	  if (cVar1 != '\0')
	    {
	      uStack184 = *(uint *) ((long) plStack144 + 0x14);
	    }
	  lStack64 = *plStack144;
	}
      if ((pcStack72 == (char *) 0x0) || (*pcStack72 == '\0'))
	{
	  if (1)
	    {
	      lStack200 = (long) iStack152;
	    }
	}
      else
	{
	  uStack148 = FUN_00402555 (pcStack72, &uStack192, &lStack200, 0);
	}
    }
  iVar2 = func_0x00401be0 (lStack120);
  if (iVar2 != 0)
    {
      imperfection_wrapper ();	//    uVar3 = FUN_004060cb(4, lStack120);
      puVar4 = (uint *) func_0x00402040 ();
      imperfection_wrapper ();	//    FUN_004075b9(0x7d, (ulong)*puVar4, "cannot change root directory to %s", uVar3);
    }
  if ((cStack73 != '\x01')
      && (iVar2 = func_0x00401e40 (&DAT_004165e9), iVar2 != 0))
    {
      puVar4 = (uint *) func_0x00402040 ();
      imperfection_wrapper ();	//    FUN_004075b9(0x7d, (ulong)*puVar4, "cannot chdir to root directory");
    }
  if (DAT_0061a550 + 1U == uVar7)
    {
      lStack88 = func_0x00401c90 ("SHELL");
      if (lStack88 == 0)
	{
	  lStack88 = FUN_00402527 ("/bin/sh");
	}
      *(long *) pcStack224 = lStack88;
      lVar5 = FUN_00402527 (&DAT_004168c5);
      ((long *) pcStack224)[1] = lVar5;
      ((long *) pcStack224)[2] = 0;
    }
  else
    {
      pcStack224 = (char *) ((long *) pcStack224 + (long) DAT_0061a550 + 1);
    }
  if ((((pcStack56 != (char *) 0x0)
	&& (lStack160 =
	    FUN_004065f9 (pcStack56, &uStack180, &uStack184, 0, 0),
	    lStack160 != 0))
       && (cVar1 = FUN_00402535 ((ulong) uStack180), cVar1 != '\0'))
      && (cVar1 = FUN_00402545 ((ulong) uStack184), cVar1 != '\0'))
    {
      puVar4 = (uint *) func_0x00402040 ();
      imperfection_wrapper ();	//    FUN_004075b9(0x7d, (ulong)*puVar4, &DAT_004168c8, lStack160);
    }
  cVar1 = FUN_00402535 ((ulong) uStack180);
  if ((cVar1 != '\x01')
      &&
      ((pcStack72 == (char *) 0x0
	|| (cVar1 = FUN_00402545 ((ulong) uStack184), cVar1 != '\0'))))
    {
      plStack168 = (long *) func_0x00401f90 ((ulong) uStack180);
      if (plStack168 == (long *) 0x0)
	{
	  cVar1 = FUN_00402545 ((ulong) uStack184);
	  uVar7 = uStack180;
	  if (cVar1 != '\0')
	    {
	      puVar4 = (uint *) func_0x00402040 ();
	      imperfection_wrapper ();	//        FUN_004075b9(0x7d, (ulong)*puVar4, "no group specified for unknown uid: %d", (ulong)uVar7);
	    }
	}
      else
	{
	  cVar1 = FUN_00402545 ((ulong) uStack184);
	  if (cVar1 != '\0')
	    {
	      uStack184 = *(uint *) ((long) plStack168 + 0x14);
	    }
	  lStack64 = *plStack168;
	}
    }
  uStack96 = uStack192;
  uStack208 = 0;
  if ((pcStack72 == (char *) 0x0) || (*pcStack72 == '\0'))
    {
      if ((pcStack72 == (char *) 0x0)
	  &&
	  ((cVar1 = FUN_00402545 ((ulong) uStack184), cVar1 != '\x01'
	    && (lStack64 != 0))))
	{
	  imperfection_wrapper ();	//      iStack172 = FUN_00406ef7(lStack64, (ulong)uStack184, &uStack208, (ulong)uStack184);
	  if (iStack172 < 1)
	    {
	      if (lStack200 != 0)
		goto LAB_00402fce;
	      puVar4 = (uint *) func_0x00402040 ();
	      imperfection_wrapper ();	//        FUN_004075b9(0x7d, (ulong)*puVar4, "failed to get supplemental groups");
	    }
	  lStack200 = (long) iStack172;
	  uStack96 = uStack208;
	}
    }
  else
    {
      iVar2 =
	FUN_00402555 (pcStack72, &uStack208, &lStack200,
		      (ulong) (lStack200 == 0));
      if (iVar2 == 0)
	{
	  uStack96 = uStack208;
	}
      else
	{
	  if (lStack200 == 0)
	    {
	      return 0x7d;
	    }
	}
    }
LAB_00402fce:
  ;
  cVar1 = FUN_00402535 ((ulong) uStack180);
  if (((cVar1 != '\x01') || (pcStack72 != (char *) 0x0))
      && (iVar2 =
	  func_0x00401d70 (lStack200, uStack96, uStack96), iVar2 != 0))
    {
      puVar4 = (uint *) func_0x00402040 ();
      imperfection_wrapper ();	//    FUN_004075b9(0x7d, (ulong)*puVar4, "failed to set supplemental groups");
    }
  func_0x004021a0 (uStack208);
  func_0x004021a0 (uStack192);
  cVar1 = FUN_00402545 ((ulong) uStack184);
  if ((cVar1 != '\x01')
      && (iVar2 = func_0x00401eb0 ((ulong) uStack184), iVar2 != 0))
    {
      puVar4 = (uint *) func_0x00402040 ();
      imperfection_wrapper ();	//    FUN_004075b9(0x7d, (ulong)*puVar4, "failed to set group-ID");
    }
  cVar1 = FUN_00402535 ((ulong) uStack180);
  if ((cVar1 != '\x01')
      && (iVar2 = func_0x00402160 ((ulong) uStack180), iVar2 != 0))
    {
      puVar4 = (uint *) func_0x00402040 ();
      imperfection_wrapper ();	//    FUN_004075b9(0x7d, (ulong)*puVar4, "failed to set user-ID");
    }
  func_0x00402120 (*(long *) pcStack224, pcStack224, pcStack224);
  piVar6 = (int *) func_0x00402040 ();
  if (*piVar6 == 2)
    {
      uStack176 = 0x7f;
    }
  else
    {
      uStack176 = 0x7e;
    }
  imperfection_wrapper ();	//  uVar3 = FUN_00406221(*(long *)pcStack224);
  puVar4 = (uint *) func_0x00402040 ();
  imperfection_wrapper ();	//  FUN_004075b9(0, (ulong)*puVar4, "failed to run command %s", uVar3);
  return (ulong) uStack176;
}

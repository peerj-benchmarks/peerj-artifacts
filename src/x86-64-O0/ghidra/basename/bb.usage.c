
#include "basename.h"

long null_ARRAY_006145d_0_8_;
long null_ARRAY_006145d_8_8_;
long null_ARRAY_0061474_0_8_;
long null_ARRAY_0061474_16_8_;
long null_ARRAY_0061474_24_8_;
long null_ARRAY_0061474_32_8_;
long null_ARRAY_0061474_40_8_;
long null_ARRAY_0061474_48_8_;
long null_ARRAY_0061474_8_8_;
long null_ARRAY_0061488_0_4_;
long null_ARRAY_0061488_16_8_;
long null_ARRAY_0061488_4_4_;
long null_ARRAY_0061488_8_4_;
long DAT_0041178b;
long DAT_00411845;
long DAT_004118c3;
long DAT_00411d11;
long DAT_00411d58;
long DAT_00411e9e;
long DAT_00411ea2;
long DAT_00411ea7;
long DAT_00411ea9;
long DAT_00412513;
long DAT_004128e0;
long DAT_004128f8;
long DAT_004128fd;
long DAT_00412967;
long DAT_004129f8;
long DAT_004129fb;
long DAT_00412a49;
long DAT_00412a4d;
long DAT_00412ac0;
long DAT_00412ac1;
long DAT_00412ca8;
long DAT_006141e0;
long DAT_006141f0;
long DAT_00614200;
long DAT_006145a8;
long DAT_006145c0;
long DAT_00614638;
long DAT_0061463c;
long DAT_00614640;
long DAT_00614680;
long DAT_00614690;
long DAT_006146a0;
long DAT_006146a8;
long DAT_006146c0;
long DAT_006146c8;
long DAT_00614710;
long DAT_00614718;
long DAT_00614720;
long DAT_006148b8;
long DAT_006148c0;
long DAT_006148c8;
long DAT_006148d8;
long fde_00413760;
long FLOAT_UNKNOWN;
long null_ARRAY_00411940;
long null_ARRAY_00412f00;
long null_ARRAY_006145d0;
long null_ARRAY_00614600;
long null_ARRAY_006146e0;
long null_ARRAY_00614740;
long null_ARRAY_00614780;
long null_ARRAY_00614880;
long PTR_DAT_006145a0;
long PTR_null_ARRAY_006145e0;
void
FUN_00401bf4 (uint uParm1)
{
  long lVar1;
  char *pcVar2;
  char *pcVar3;
  char *pcStack48;
  char *pcStack40;

  if (uParm1 == 0)
    {
      func_0x004014a0
	("Usage: %s NAME [SUFFIX]\n  or:  %s OPTION... NAME...\n",
	 DAT_00614720, DAT_00614720);
      func_0x00401630
	("Print NAME with any leading directory components removed.\nIf specified, also remove a trailing SUFFIX.\n",
	 DAT_00614680);
      FUN_00401a3e ();
      func_0x00401630
	("  -a, --multiple       support multiple arguments and treat each as a NAME\n  -s, --suffix=SUFFIX  remove a trailing SUFFIX; implies -a\n  -z, --zero           end each output line with NUL, not newline\n",
	 DAT_00614680);
      func_0x00401630 ("      --help     display this help and exit\n",
		       DAT_00614680);
      func_0x00401630
	("      --version  output version information and exit\n",
	 DAT_00614680);
      pcVar2 = DAT_00614720;
      func_0x004014a0
	("\nExamples:\n  %s /usr/bin/sort          -> \"sort\"\n  %s include/stdio.h .h     -> \"stdio\"\n  %s -s .h include/stdio.h  -> \"stdio\"\n  %s -a any/str1 any/str2   -> \"str1\" followed by \"str2\"\n",
	 DAT_00614720, DAT_00614720, DAT_00614720, DAT_00614720);
      imperfection_wrapper ();	//    FUN_00401a58();
    }
  else
    {
      pcVar2 = "Try \'%s --help\' for more information.\n";
      func_0x00401620 (DAT_006146a0,
		       "Try \'%s --help\' for more information.\n",
		       DAT_00614720);
    }
  pcVar3 = (char *) (ulong) uParm1;
  func_0x004017b0 ();
  lVar1 = func_0x00401810 (pcVar3);
  pcStack40 = pcVar3 + lVar1;
  lVar1 = func_0x00401810 (pcVar2);
  pcStack48 = pcVar2 + lVar1;
  while ((pcVar3 < pcStack40 && (pcVar2 < pcStack48)))
    {
      pcStack40 = pcStack40 + -1;
      pcStack48 = pcStack48 + -1;
      if (*pcStack40 != *pcStack48)
	{
	  return;
	}
    }
  if (pcStack40 <= pcVar3)
    {
      return;
    }
  *pcStack40 = '\0';
  return;
}

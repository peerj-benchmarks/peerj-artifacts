
#include "cut.h"

long DAT_006175c_0_1_;
long DAT_006175c_1_1_;
long null_ARRAY_0061747_0_8_;
long null_ARRAY_0061747_8_8_;
long null_ARRAY_0061760_0_8_;
long null_ARRAY_0061760_16_8_;
long null_ARRAY_0061760_24_8_;
long null_ARRAY_0061760_32_8_;
long null_ARRAY_0061760_40_8_;
long null_ARRAY_0061760_48_8_;
long null_ARRAY_0061760_8_8_;
long null_ARRAY_0061774_0_4_;
long null_ARRAY_0061774_16_8_;
long null_ARRAY_0061774_4_4_;
long null_ARRAY_0061774_8_4_;
long DAT_00413843;
long DAT_004138fd;
long DAT_0041397b;
long DAT_004141a6;
long DAT_004141c1;
long DAT_004141c3;
long DAT_004141c5;
long DAT_00414330;
long DAT_004144e0;
long DAT_00414528;
long DAT_0041465e;
long DAT_00414662;
long DAT_00414667;
long DAT_00414669;
long DAT_00414cd3;
long DAT_004150a0;
long DAT_004150b8;
long DAT_004150bd;
long DAT_00415127;
long DAT_004151b8;
long DAT_004151bb;
long DAT_00415209;
long DAT_0041520d;
long DAT_00415280;
long DAT_00415281;
long DAT_00617000;
long DAT_00617010;
long DAT_00617020;
long DAT_00617440;
long DAT_00617450;
long DAT_00617460;
long DAT_006174d8;
long DAT_006174dc;
long DAT_006174e0;
long DAT_00617500;
long DAT_00617508;
long DAT_00617510;
long DAT_00617520;
long DAT_00617528;
long DAT_00617540;
long DAT_00617548;
long DAT_00617590;
long DAT_00617598;
long DAT_006175a0;
long DAT_006175a8;
long DAT_006175ac;
long DAT_006175ad;
long DAT_006175ae;
long DAT_006175af;
long DAT_006175b0;
long DAT_006175b8;
long DAT_006175c0;
long DAT_006175c1;
long DAT_006175c8;
long DAT_006175d0;
long DAT_006175d8;
long DAT_006175e0;
long DAT_006175e8;
long DAT_00617778;
long DAT_00617780;
long DAT_00617788;
long DAT_00617790;
long DAT_00617798;
long DAT_006177a8;
long fde_00415fc0;
long FLOAT_UNKNOWN;
long null_ARRAY_00413a40;
long null_ARRAY_004156c0;
long null_ARRAY_00617470;
long null_ARRAY_006174a0;
long null_ARRAY_00617560;
long null_ARRAY_00617600;
long null_ARRAY_00617640;
long null_ARRAY_00617740;
long PTR_DAT_00617448;
long PTR_null_ARRAY_00617480;
void
FUN_0040213d (uint uParm1)
{
  ulong *puVar1;

  if (uParm1 == 0)
    {
      func_0x004018c0 ("Usage: %s OPTION... [FILE]...\n", DAT_006175e8);
      func_0x00401a90
	("Print selected parts of lines from each FILE to standard output.\n",
	 DAT_00617500);
      FUN_00401f6d ();
      FUN_00401f87 ();
      func_0x00401a90
	("  -b, --bytes=LIST        select only these bytes\n  -c, --characters=LIST   select only these characters\n  -d, --delimiter=DELIM   use DELIM instead of TAB for field delimiter\n",
	 DAT_00617500);
      func_0x00401a90
	("  -f, --fields=LIST       select only these fields;  also print any line\n                            that contains no delimiter character, unless\n                            the -s option is specified\n  -n                      (ignored)\n",
	 DAT_00617500);
      func_0x00401a90
	("      --complement        complement the set of selected bytes, characters\n                            or fields\n",
	 DAT_00617500);
      func_0x00401a90
	("  -s, --only-delimited    do not print lines not containing delimiters\n      --output-delimiter=STRING  use STRING as the output delimiter\n                            the default is to use the input delimiter\n",
	 DAT_00617500);
      func_0x00401a90
	("  -z, --zero-terminated    line delimiter is NUL, not newline\n",
	 DAT_00617500);
      func_0x00401a90 ("      --help     display this help and exit\n",
		       DAT_00617500);
      func_0x00401a90
	("      --version  output version information and exit\n",
	 DAT_00617500);
      func_0x00401a90
	("\nUse one, and only one of -b, -c or -f.  Each LIST is made up of one\nrange, or many ranges separated by commas.  Selected input is written\nin the same order that it is read, and is written exactly once.\n",
	 DAT_00617500);
      func_0x00401a90
	("Each range is one of:\n\n  N     N\'th byte, character or field, counted from 1\n  N-    from N\'th byte, character or field, to end of line\n  N-M   from N\'th to M\'th (included) byte, character or field\n  -M    from first to M\'th (included) byte, character or field\n",
	 DAT_00617500);
      imperfection_wrapper ();	//    FUN_00401fa1();
    }
  else
    {
      func_0x00401a80 (DAT_00617520,
		       "Try \'%s --help\' for more information.\n",
		       DAT_006175e8);
    }
  puVar1 = (ulong *) (ulong) uParm1;
  func_0x00401ca0 ();
  *puVar1 = *puVar1 + 1;
  if (*(ulong *) (DAT_00617590 + 8) < *puVar1)
    {
      DAT_00617590 = DAT_00617590 + 0x10;
    }
  return;
}

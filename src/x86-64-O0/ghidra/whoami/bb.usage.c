
#include "whoami.h"

long null_ARRAY_006143f_0_8_;
long null_ARRAY_006143f_8_8_;
long null_ARRAY_0061454_0_8_;
long null_ARRAY_0061454_16_8_;
long null_ARRAY_0061454_24_8_;
long null_ARRAY_0061454_32_8_;
long null_ARRAY_0061454_40_8_;
long null_ARRAY_0061454_48_8_;
long null_ARRAY_0061454_8_8_;
long null_ARRAY_0061468_0_4_;
long null_ARRAY_0061468_16_8_;
long null_ARRAY_0061468_4_4_;
long null_ARRAY_0061468_8_4_;
long DAT_004113c0;
long DAT_0041147d;
long DAT_004114fb;
long DAT_004116c1;
long DAT_004117a0;
long DAT_004117e8;
long DAT_0041191e;
long DAT_00411922;
long DAT_00411927;
long DAT_00411929;
long DAT_00411f93;
long DAT_00412360;
long DAT_00412378;
long DAT_0041237d;
long DAT_004123e7;
long DAT_00412478;
long DAT_0041247b;
long DAT_004124c9;
long DAT_004124cd;
long DAT_00412540;
long DAT_00412541;
long DAT_00412728;
long DAT_00614000;
long DAT_00614010;
long DAT_00614020;
long DAT_006143c8;
long DAT_006143e0;
long DAT_00614458;
long DAT_0061445c;
long DAT_00614460;
long DAT_00614480;
long DAT_00614490;
long DAT_006144a0;
long DAT_006144a8;
long DAT_006144c0;
long DAT_006144c8;
long DAT_00614510;
long DAT_00614518;
long DAT_00614520;
long DAT_006146b8;
long DAT_006146c0;
long DAT_006146c8;
long DAT_006146d8;
long fde_004131a0;
long FLOAT_UNKNOWN;
long null_ARRAY_00411540;
long null_ARRAY_00411740;
long null_ARRAY_00412980;
long null_ARRAY_006143f0;
long null_ARRAY_00614420;
long null_ARRAY_006144e0;
long null_ARRAY_00614540;
long null_ARRAY_00614580;
long null_ARRAY_00614680;
long PTR_DAT_006143c0;
long PTR_null_ARRAY_00614400;
undefined8
FUN_00401c0a (uint uParm1)
{
  int iVar1;
  uint uVar2;
  undefined8 uVar3;
  undefined4 *puVar4;
  int *piVar5;
  undefined8 *puVar6;
  uint *puVar7;
  char *pcVar8;

  if (uParm1 == 0)
    {
      func_0x004014c0 ("Usage: %s [OPTION]...\n", DAT_00614520);
      func_0x00401660
	("Print the user name associated with the current effective user ID.\nSame as id -un.\n\n",
	 DAT_00614480);
      func_0x00401660 ("      --help     display this help and exit\n",
		       DAT_00614480);
      pcVar8 = (char *) DAT_00614480;
      func_0x00401660
	("      --version  output version information and exit\n");
      imperfection_wrapper ();	//    FUN_00401a6e();
    }
  else
    {
      pcVar8 = "Try \'%s --help\' for more information.\n";
      func_0x00401650 (DAT_006144a0,
		       "Try \'%s --help\' for more information.\n",
		       DAT_00614520);
    }
  func_0x004017e0 ();
  FUN_00402052 (*(undefined8 *) pcVar8);
  func_0x004017a0 (6, &DAT_004114fb);
  func_0x00401780 (FUN_00401e1c);
  imperfection_wrapper ();	//  FUN_00401f01((ulong)uParm1, pcVar8, "whoami", "GNU coreutils", PTR_DAT_006143c0, FUN_00401c0a, "Richard Mlynarik", 0);
  imperfection_wrapper ();	//  iVar1 = FUN_00404e23((ulong)uParm1, pcVar8, &DAT_004114fb, null_ARRAY_00411540, 0);
  if (iVar1 != -1)
    {
      FUN_00401c0a (1);
    }
  if (DAT_00614458 != uParm1)
    {
      imperfection_wrapper ();	//    uVar3 = FUN_004034c6(((undefined8 *)pcVar8)[(long)(int)DAT_00614458]);
      imperfection_wrapper ();	//    FUN_00403c4a(0, 0, "extra operand %s", uVar3);
      FUN_00401c0a (1);
    }
  puVar4 = (undefined4 *) func_0x004017d0 ();
  *puVar4 = 0;
  uVar2 = func_0x004014f0 ();
  if (uVar2 == 0xffffffff)
    {
      piVar5 = (int *) func_0x004017d0 ();
      if (*piVar5 != 0)
	{
	  puVar6 = (undefined8 *) 0x0;
	  goto LAB_00401dd4;
	}
    }
  puVar6 = (undefined8 *) func_0x00401740 ((ulong) uVar2);
LAB_00401dd4:
  ;
  if (puVar6 == (undefined8 *) 0x0)
    {
      puVar7 = (uint *) func_0x004017d0 ();
      imperfection_wrapper ();	//    FUN_00403c4a(1, (ulong)*puVar7, "cannot find name for user ID %lu", (ulong)uVar2);
    }
  func_0x00401580 (*puVar6);
  return 0;
}

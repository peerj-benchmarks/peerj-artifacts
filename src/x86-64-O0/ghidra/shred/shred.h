typedef unsigned char   undefined;

typedef unsigned char    byte;
typedef unsigned char    dwfenc;
typedef unsigned int    dword;
typedef unsigned long    qword;
typedef unsigned char    undefined1;
typedef unsigned short    undefined2;
typedef unsigned int    undefined4;
typedef unsigned long    undefined6;
typedef unsigned long    undefined8;
typedef unsigned short    word;
#define unkbyte9   unsigned long long
#define unkbyte10   unsigned long long
#define unkbyte11   unsigned long long
#define unkbyte12   unsigned long long
#define unkbyte13   unsigned long long
#define unkbyte14   unsigned long long
#define unkbyte15   unsigned long long
#define unkbyte16   unsigned long long

#define unkuint9   unsigned long long
#define unkuint10   unsigned long long
#define unkuint11   unsigned long long
#define unkuint12   unsigned long long
#define unkuint13   unsigned long long
#define unkuint14   unsigned long long
#define unkuint15   unsigned long long
#define unkuint16   unsigned long long

#define unkint9   long long
#define unkint10   long long
#define unkint11   long long
#define unkint12   long long
#define unkint13   long long
#define unkint14   long long
#define unkint15   long long
#define unkint16   long long

#define unkfloat1   float
#define unkfloat2   float
#define unkfloat3   float
#define unkfloat5   double
#define unkfloat6   double
#define unkfloat7   double
#define unkfloat9   long double
#define unkfloat11   long double
#define unkfloat12   long double
#define unkfloat13   long double
#define unkfloat14   long double
#define unkfloat15   long double
#define unkfloat16   long double

#define BADSPACEBASE   void
#define code   void

#define ulong long
#define true 1
#define bool int
#define false 0
#define float10 int
#define ushort int
#define undefined7 long
#define undefined2 long
#define uint5 long
#define uint6 long

// Add definition for uint
typedef unsigned int    uint;

#define sbyte char

extern imperfection_wrapper(void);
typedef struct eh_frame_hdr eh_frame_hdr, *Peh_frame_hdr;

struct eh_frame_hdr {
    byte eh_frame_hdr_version; // Exception Handler Frame Header Version
    dwfenc eh_frame_pointer_encoding; // Exception Handler Frame Pointer Encoding
    dwfenc eh_frame_desc_entry_count_encoding; // Encoding of # of Exception Handler FDEs
    dwfenc eh_frame_table_encoding; // Exception Handler Table Encoding
};

typedef struct fde_table_entry fde_table_entry, *Pfde_table_entry;

struct fde_table_entry {
    dword initial_loc; // Initial Location
    dword data_loc; // Data location
};

typedef enum Elf64_DynTag {
    DT_AUDIT=1879047932,
    DT_AUXILIARY=2147483645,
    DT_BIND_NOW=24,
    DT_CHECKSUM=1879047672,
    DT_CONFIG=1879047930,
    DT_DEBUG=21,
    DT_DEPAUDIT=1879047931,
    DT_ENCODING=32,
    DT_FEATURE_1=1879047676,
    DT_FILTER=2147483647,
    DT_FINI=13,
    DT_FINI_ARRAY=26,
    DT_FINI_ARRAYSZ=28,
    DT_FLAGS=30,
    DT_FLAGS_1=1879048187,
    DT_GNU_CONFLICT=1879047928,
    DT_GNU_CONFLICTSZ=1879047670,
    DT_GNU_HASH=1879047925,
    DT_GNU_LIBLIST=1879047929,
    DT_GNU_LIBLISTSZ=1879047671,
    DT_GNU_PRELINKED=1879047669,
    DT_HASH=4,
    DT_INIT=12,
    DT_INIT_ARRAY=25,
    DT_INIT_ARRAYSZ=27,
    DT_JMPREL=23,
    DT_MOVEENT=1879047674,
    DT_MOVESZ=1879047675,
    DT_MOVETAB=1879047934,
    DT_NEEDED=1,
    DT_NULL=0,
    DT_PLTGOT=3,
    DT_PLTPAD=1879047933,
    DT_PLTPADSZ=1879047673,
    DT_PLTREL=20,
    DT_PLTRELSZ=2,
    DT_POSFLAG_1=1879047677,
    DT_PREINIT_ARRAYSZ=33,
    DT_REL=17,
    DT_RELA=7,
    DT_RELACOUNT=1879048185,
    DT_RELAENT=9,
    DT_RELASZ=8,
    DT_RELCOUNT=1879048186,
    DT_RELENT=19,
    DT_RELSZ=18,
    DT_RPATH=15,
    DT_RUNPATH=29,
    DT_SONAME=14,
    DT_STRSZ=10,
    DT_STRTAB=5,
    DT_SYMBOLIC=16,
    DT_SYMENT=11,
    DT_SYMINENT=1879047679,
    DT_SYMINFO=1879047935,
    DT_SYMINSZ=1879047678,
    DT_SYMTAB=6,
    DT_TEXTREL=22,
    DT_TLSDESC_GOT=1879047927,
    DT_TLSDESC_PLT=1879047926,
    DT_VERDEF=1879048188,
    DT_VERDEFNUM=1879048189,
    DT_VERNEED=1879048190,
    DT_VERNEEDNUM=1879048191,
    DT_VERSYM=1879048176
} Elf64_DynTag;

typedef enum Elf_ProgramHeaderType {
    PT_DYNAMIC=2,
    PT_GNU_EH_FRAME=1685382480,
    PT_GNU_RELRO=1685382482,
    PT_GNU_STACK=1685382481,
    PT_INTERP=3,
    PT_LOAD=1,
    PT_NOTE=4,
    PT_NULL=0,
    PT_PHDR=6,
    PT_SHLIB=5,
    PT_TLS=7
} Elf_ProgramHeaderType;

typedef struct Elf64_Dyn Elf64_Dyn, *PElf64_Dyn;

struct Elf64_Dyn {
    enum Elf64_DynTag d_tag;
    qword d_val;
};

typedef struct Elf64_Shdr Elf64_Shdr, *PElf64_Shdr;

typedef enum Elf_SectionHeaderType {
    SHT_CHECKSUM=1879048184,
    SHT_DYNAMIC=6,
    SHT_DYNSYM=11,
    SHT_FINI_ARRAY=15,
    SHT_GNU_ATTRIBUTES=1879048181,
    SHT_GNU_HASH=1879048182,
    SHT_GNU_LIBLIST=1879048183,
    SHT_GNU_verdef=1879048189,
    SHT_GNU_verneed=1879048190,
    SHT_GNU_versym=1879048191,
    SHT_GROUP=17,
    SHT_HASH=5,
    SHT_INIT_ARRAY=14,
    SHT_NOBITS=8,
    SHT_NOTE=7,
    SHT_NULL=0,
    SHT_PREINIT_ARRAY=16,
    SHT_PROGBITS=1,
    SHT_REL=9,
    SHT_RELA=4,
    SHT_SHLIB=10,
    SHT_STRTAB=3,
    SHT_SUNW_COMDAT=1879048187,
    SHT_SUNW_move=1879048186,
    SHT_SUNW_syminfo=1879048188,
    SHT_SYMTAB=2,
    SHT_SYMTAB_SHNDX=18
} Elf_SectionHeaderType;

struct Elf64_Shdr {
    dword sh_name;
    enum Elf_SectionHeaderType sh_type;
    qword sh_flags;
    qword sh_addr;
    qword sh_offset;
    qword sh_size;
    dword sh_link;
    dword sh_info;
    qword sh_addralign;
    qword sh_entsize;
};

typedef struct Elf64_Phdr Elf64_Phdr, *PElf64_Phdr;

struct Elf64_Phdr {
    enum Elf_ProgramHeaderType p_type;
    dword p_flags;
    qword p_offset;
    qword p_vaddr;
    qword p_paddr;
    qword p_filesz;
    qword p_memsz;
    qword p_align;
};

typedef struct Elf64_Sym Elf64_Sym, *PElf64_Sym;

struct Elf64_Sym {
    dword st_name;
    byte st_info;
    byte st_other;
    word st_shndx;
    qword st_value;
    qword st_size;
};

typedef struct Elf64_Rela Elf64_Rela, *PElf64_Rela;

struct Elf64_Rela {
    qword r_offset; // location to apply the relocation action
    qword r_info; // the symbol table index and the type of relocation
    qword r_addend; // a constant addend used to compute the relocatable field value
};

typedef struct Elf64_Ehdr Elf64_Ehdr, *PElf64_Ehdr;

struct Elf64_Ehdr {
    byte e_ident_magic_num;
    char e_ident_magic_str[3];
    byte e_ident_class;
    byte e_ident_data;
    byte e_ident_version;
    byte e_ident_pad[9];
    word e_type;
    word e_machine;
    dword e_version;
    qword e_entry;
    qword e_phoff;
    qword e_shoff;
    dword e_flags;
    word e_ehsize;
    word e_phentsize;
    word e_phnum;
    word e_shentsize;
    word e_shnum;
    word e_shstrndx;
};




undefined8 _DT_INIT(void);
void FUN_00401b70(void);
void entry(void);
void FUN_004021b0(void);
void FUN_00402230(void);
void FUN_004022b0(void);
long FUN_004022ee(long lParm1,ulong uParm2);
void FUN_00402334(void);
void FUN_0040234e(undefined *puParm1);
void FUN_004024ea(undefined8 uParm1,long lParm2);
ulong FUN_00402513(uint uParm1);
ulong FUN_0040266b(uint uParm1);
void FUN_004026d4(uint uParm1,undefined *puParm2,ulong uParm3);
void FUN_004027d7(byte *pbParm1,undefined8 uParm2);
ulong FUN_00402847(int iParm1);
undefined8 FUN_00402871(uint uParm1,undefined8 uParm2);
void FUN_00402957(uint uParm1,char cParm2);
ulong FUN_004029bc(uint uParm1,long lParm2);
ulong FUN_00402a42(ulong uParm1);
ulong FUN_00402a57(uint param_1,undefined8 param_2,undefined8 param_3,ulong *param_4,uint param_5,undefined8 param_6,undefined8 param_7,long param_8);
void FUN_00403264(int *piParm1,ulong uParm2,undefined8 uParm3);
ulong FUN_00403500(uint uParm1,undefined8 uParm2,undefined8 uParm3,long lParm4);
undefined8 FUN_00403a31(uint uParm1,undefined8 uParm2,undefined8 uParm3,undefined8 uParm4);
undefined8 FUN_00403adb(long lParm1,long lParm2);
ulong FUN_00403b8d(long lParm1,long lParm2,long lParm3);
ulong FUN_00403e94(undefined8 uParm1,undefined8 uParm2,undefined8 uParm3,char *pcParm4);
void FUN_00403fc1(void);
ulong FUN_00403fd6(uint uParm1,undefined8 *puParm2);
void FUN_0040443e(void);
long FUN_0040444e(undefined8 uParm1,long lParm2,long lParm3,long lParm4);
void FUN_0040457d(undefined8 uParm1,undefined8 uParm2,long lParm3);
void FUN_004045fe(long lParm1,long lParm2,long lParm3);
long FUN_00404734(undefined8 uParm1,undefined8 uParm2,undefined8 uParm3,undefined8 uParm4,undefined8 uParm5,code *pcParm6);
void FUN_004047ba(void);
long FUN_0040489f(undefined8 uParm1);
ulong FUN_004048cd(char *pcParm1);
undefined * FUN_00404956(undefined8 uParm1);
char * FUN_004049ed(char *pcParm1);
ulong FUN_00404a56(long lParm1);
void FUN_00404aa4(undefined4 uParm1,undefined4 uParm2,undefined4 uParm3,undefined4 uParm4,undefined4 uParm5,undefined4 uParm6,undefined4 uParm7,undefined4 uParm8,undefined8 param_1,uint param_2,undefined8 param_3,undefined8 param_4,undefined8 param_5,undefined8 param_6);
void FUN_00404bb3(void);
long FUN_00404caa(long lParm1,ulong uParm2,byte *pbParm3,undefined8 uParm4);
char * FUN_00404da6(ulong uParm1,long lParm2,uint uParm3,ulong uParm4,ulong uParm5);
char * FUN_00405737(ulong uParm1,long lParm2);
void FUN_004057bc(long lParm1);
ulong FUN_00405899(undefined1 *puParm1,byte bParm2,uint uParm3);
undefined8 * FUN_00405921(undefined8 *puParm1,int iParm2);
char * FUN_00405998(char *pcParm1,int iParm2);
ulong FUN_00405a38(long param_1,ulong param_2,long param_3,ulong param_4,uint param_5,uint param_6,long param_7,char *param_8,char *param_9);
undefined1 * FUN_00406902(int iParm1,undefined8 uParm2,undefined8 uParm3,uint *puParm4);
void FUN_00406b75(uint uParm1,uint uParm2,undefined8 uParm3);
void FUN_00406bb6(undefined8 uParm1,undefined8 uParm2,char cParm3);
void FUN_00406c4a(undefined8 uParm1,char cParm2);
void FUN_00406c74(undefined8 uParm1);
void FUN_00406c93(uint uParm1,uint uParm2,undefined8 uParm3);
void FUN_00406d2e(uint uParm1,undefined8 uParm2,undefined8 uParm3);
void FUN_00406d5a(uint uParm1,undefined8 uParm2);
void FUN_00406d83(undefined8 uParm1);
undefined8 * FUN_00406da2(undefined8 uParm1);
undefined8 FUN_00406de9(undefined8 uParm1,undefined8 uParm2);
undefined8 FUN_00406e2c(undefined8 *puParm1);
long FUN_00406e3d(long lParm1);
ulong FUN_00406e4f(undefined8 *puParm1,ulong uParm2);
void FUN_00407000(undefined8 uParm1);
ulong FUN_0040702b(undefined8 *puParm1);
ulong * FUN_00407071(ulong uParm1,ulong uParm2);
undefined8 * FUN_004070d1(undefined8 uParm1,undefined8 uParm2);
void FUN_00407118(long lParm1,ulong uParm2,ulong uParm3);
long FUN_0040737d(long lParm1,ulong uParm2);
void FUN_0040746c(undefined8 *puParm1,long lParm2,long lParm3);
void FUN_0040750c(ulong *puParm1,ulong uParm2,ulong uParm3);
void FUN_00407651(long *plParm1,undefined8 uParm2,undefined8 uParm3);
undefined8 FUN_004076a7(long *plParm1);
undefined8 FUN_004076f7(undefined8 uParm1);
undefined8 FUN_00407711(long lParm1,undefined8 uParm2);
void FUN_00407755(ulong *puParm1,long *plParm2);
void FUN_00407de5(long lParm1);
undefined8 FUN_0040836c(undefined4 uParm1);
ulong FUN_0040838b(uint uParm1,long lParm2,uint uParm3,long lParm4,uint uParm5);
void FUN_004087ed(uint uParm1,undefined8 uParm2,undefined8 uParm3);
ulong FUN_00408819(uint uParm1);
void FUN_00408869(undefined8 uParm1,long lParm2,undefined8 uParm3,undefined8 uParm4,undefined8 *puParm5,undefined8 uParm6);
void FUN_00408ccd(undefined8 uParm1,undefined8 uParm2,undefined8 uParm3,undefined8 uParm4,uint *puParm5);
void FUN_00408d9f(undefined4 uParm1,undefined4 uParm2,undefined4 uParm3,undefined4 uParm4,undefined4 uParm5,undefined4 uParm6,undefined4 uParm7,undefined4 uParm8,undefined8 param_1,undefined8 param_2,undefined8 param_3,undefined8 param_4,undefined8 param_5,undefined8 param_6);
void FUN_00408e55(ulong uParm1,ulong uParm2);
void FUN_00408e96(undefined8 uParm1);
long FUN_00408eb0(long lParm1);
long FUN_00408ee5(long lParm1,long lParm2);
void FUN_00408f46(undefined8 uParm1,undefined8 uParm2);
void FUN_00408f7a(undefined8 uParm1);
char * FUN_00408fa7(void);
ulong FUN_00408fd1(undefined8 param_1,uint param_2,ulong param_3,ulong param_4,undefined8 param_5,undefined8 param_6,uint param_7);
void FUN_004090e3(undefined8 uParm1,undefined8 uParm2,undefined8 uParm3,undefined8 uParm4,undefined8 uParm5,uint uParm6);
undefined8 FUN_00409138(int iParm1);
ulong FUN_0040915e(ulong *puParm1,int iParm2);
ulong FUN_004091bd(undefined8 uParm1,uint uParm2,int iParm3);
ulong FUN_004091fe(byte *pbParm1,byte **ppbParm2,uint uParm3,undefined8 *puParm4,long lParm5);
ulong FUN_004095df(uint uParm1);
void FUN_00409608(void);
void FUN_0040963c(uint uParm1);
void FUN_004096b0(uint uParm1,uint uParm2,undefined8 uParm3,undefined8 uParm4);
void FUN_00409734(undefined4 uParm1,undefined4 uParm2,undefined4 uParm3,undefined4 uParm4,undefined4 uParm5,undefined4 uParm6,undefined4 uParm7,undefined4 uParm8,uint param_1,uint param_2,undefined8 param_3,undefined8 param_4,undefined8 param_5,undefined8 param_6);
void FUN_00409818(undefined8 uParm1,undefined8 uParm2);
ulong FUN_0040983f(undefined8 uParm1);
ulong FUN_004098f7(undefined4 uParm1,undefined4 uParm2,undefined4 uParm3,undefined4 uParm4,undefined4 uParm5,undefined4 uParm6,undefined4 uParm7,undefined4 uParm8,uint param_1,uint param_2,undefined8 param_3,undefined8 param_4,undefined8 param_5,undefined8 param_6);
void FUN_00409bae(undefined8 uParm1);
void FUN_00409bd2(void);
ulong FUN_00409be0(long lParm1);
undefined8 FUN_00409cb0(undefined8 uParm1);
void FUN_00409ccf(undefined8 uParm1,undefined8 uParm2,uint uParm3);
void FUN_00409cfa(uint uParm1,undefined8 uParm2,undefined8 uParm3,uint uParm4);
void FUN_00409d27(uint uParm1,undefined8 uParm2,undefined8 uParm3,uint uParm4);
ulong FUN_00409d64(uint uParm1,long lParm2,long lParm3,uint uParm4);
void FUN_00409e77(long lParm1,int *piParm2);
ulong FUN_0040a05b(int param_1,undefined8 *param_2,char *param_3,long *param_4,int *param_5,int param_6,int *param_7,int param_8,undefined8 param_9);
char * FUN_0040a645(undefined8 uParm1,undefined8 uParm2,char *pcParm3,int *piParm4,int iParm5);
ulong FUN_0040a713(uint param_1,undefined8 *param_2,char *param_3,long param_4,undefined8 param_5,uint param_6,uint *param_7,uint param_8);
ulong FUN_0040aedc(uint param_1,undefined8 param_2,undefined8 param_3,undefined8 param_4,undefined8 param_5,uint param_6,uint param_7);
void FUN_0040af6c(uint uParm1,undefined8 uParm2,undefined8 uParm3,undefined8 uParm4,undefined8 uParm5);
ulong FUN_0040afb6(undefined8 uParm1,undefined8 uParm2);
undefined8 FUN_0040b05d(long lParm1);
ulong FUN_0040b08e(uint *puParm1,byte *pbParm2,long lParm3,undefined8 uParm4);
undefined8 FUN_0040b111(long lParm1,long lParm2);
ulong FUN_0040b17a(long lParm1);
ulong FUN_0040b2e6(undefined8 uParm1,undefined8 uParm2,undefined8 uParm3);
ulong FUN_0040b407(byte *pbParm1,byte *pbParm2);
undefined8 FUN_0040b47e(undefined8 uParm1);
ulong FUN_0040b509(char *pcParm1);
long FUN_0040b56e(undefined8 uParm1,undefined8 uParm2);
undefined8 FUN_0040b65a(void);
ulong FUN_0040b667(uint uParm1);
undefined1 * FUN_0040b738(void);
char * FUN_0040baeb(void);
ulong FUN_0040bbb9(uint uParm1);
void FUN_0040bbc9(uint uParm1);
void FUN_0040bbef(undefined8 uParm1,undefined8 uParm2);
ulong FUN_0040bc14(long lParm1,long lParm2);
void FUN_0040bccb(undefined8 uParm1,undefined8 uParm2);
ulong FUN_0040bcf0(long lParm1,long lParm2);
ulong FUN_0040bd80(void);
long FUN_0040bdcd(ulong uParm1,long lParm2,ulong uParm3,long lParm4,long *plParm5);
int * FUN_0040c019(ulong uParm1,uint *puParm2,ulong uParm3,uint *puParm4,ulong *puParm5);
char * FUN_0040caf9(ulong uParm1,long lParm2,long lParm3);
long FUN_0040cd67(int *param_1,long *param_2);
long FUN_0040cf52(undefined auParm1 [16],int *piParm2,long *plParm3);
undefined8 FUN_0040d311(int iParm1,long lParm2,uint *puParm3,long lParm4,uint uParm5);
void FUN_0040db24(uint param_1);
void FUN_0040db6e(undefined8 uParm1,uint uParm2);
ulong FUN_0040dbc0(void);
ulong FUN_0040df52(undefined auParm1 [16],undefined8 uParm2,undefined8 uParm3);
ulong FUN_0040e32a(char *pcParm1,long lParm2);
undefined8 FUN_0040e383(long param_1,long param_2,undefined param_3,int param_4,undefined8 param_5,ulong param_6,int param_7,ulong param_8,int param_9);
ulong FUN_00415ab0(uint uParm1);
void FUN_00415acf(undefined8 uParm1,int *piParm2,undefined8 uParm3);
void FUN_00415b70(int *param_1);
ulong FUN_00415c2f(ulong uParm1,long lParm2);
void FUN_00415c63(undefined8 uParm1,undefined8 uParm2,undefined8 uParm3);
void FUN_00415c9e(undefined8 uParm1,undefined8 uParm2,undefined8 uParm3,undefined8 uParm4);
ulong FUN_00415cef(ulong uParm1,ulong uParm2);
undefined8 FUN_00415d0a(uint *puParm1,ulong *puParm2);
undefined8 FUN_004164aa(char *pcParm1,ulong *puParm2,char **ppcParm3);
ulong FUN_00417760(undefined auParm1 [16]);
ulong FUN_0041779b(void);
void FUN_004177d0(void);
undefined8 _DT_FINI(void);
undefined FUN_0061d000();
undefined FUN_0061d008();
undefined FUN_0061d010();
undefined FUN_0061d018();
undefined FUN_0061d020();
undefined FUN_0061d028();
undefined FUN_0061d030();
undefined FUN_0061d038();
undefined FUN_0061d040();
undefined FUN_0061d048();
undefined FUN_0061d050();
undefined FUN_0061d058();
undefined FUN_0061d060();
undefined FUN_0061d068();
undefined FUN_0061d070();
undefined FUN_0061d078();
undefined FUN_0061d080();
undefined FUN_0061d088();
undefined FUN_0061d090();
undefined FUN_0061d098();
undefined FUN_0061d0a0();
undefined FUN_0061d0a8();
undefined FUN_0061d0b0();
undefined FUN_0061d0b8();
undefined FUN_0061d0c0();
undefined FUN_0061d0c8();
undefined FUN_0061d0d0();
undefined FUN_0061d0d8();
undefined FUN_0061d0e0();
undefined FUN_0061d0e8();
undefined FUN_0061d0f0();
undefined FUN_0061d0f8();
undefined FUN_0061d100();
undefined FUN_0061d108();
undefined FUN_0061d110();
undefined FUN_0061d118();
undefined FUN_0061d120();
undefined FUN_0061d128();
undefined FUN_0061d130();
undefined FUN_0061d138();
undefined FUN_0061d140();
undefined FUN_0061d148();
undefined FUN_0061d150();
undefined FUN_0061d158();
undefined FUN_0061d160();
undefined FUN_0061d168();
undefined FUN_0061d170();
undefined FUN_0061d178();
undefined FUN_0061d180();
undefined FUN_0061d188();
undefined FUN_0061d190();
undefined FUN_0061d198();
undefined FUN_0061d1a0();
undefined FUN_0061d1a8();
undefined FUN_0061d1b0();
undefined FUN_0061d1b8();
undefined FUN_0061d1c0();
undefined FUN_0061d1c8();
undefined FUN_0061d1d0();
undefined FUN_0061d1d8();
undefined FUN_0061d1e0();
undefined FUN_0061d1e8();
undefined FUN_0061d1f0();
undefined FUN_0061d1f8();
undefined FUN_0061d200();
undefined FUN_0061d208();
undefined FUN_0061d210();
undefined FUN_0061d218();
undefined FUN_0061d220();
undefined FUN_0061d228();
undefined FUN_0061d230();
undefined FUN_0061d238();
undefined FUN_0061d240();
undefined FUN_0061d248();
undefined FUN_0061d250();
undefined FUN_0061d258();
undefined FUN_0061d260();
undefined FUN_0061d268();
undefined FUN_0061d270();
undefined FUN_0061d278();
undefined FUN_0061d280();
undefined FUN_0061d288();
undefined FUN_0061d290();
undefined FUN_0061d298();
undefined FUN_0061d2a0();
undefined FUN_0061d2a8();
undefined FUN_0061d2b0();
undefined FUN_0061d2b8();
undefined FUN_0061d2c0();
undefined FUN_0061d2c8();
undefined FUN_0061d2d0();
undefined FUN_0061d2d8();
undefined FUN_0061d2e0();
undefined FUN_0061d2e8();
undefined FUN_0061d2f0();


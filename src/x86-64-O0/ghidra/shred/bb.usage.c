
#include "shred.h"

long null_ARRAY_0061c5f_0_8_;
long null_ARRAY_0061c5f_8_8_;
long null_ARRAY_0061c74_0_8_;
long null_ARRAY_0061c74_16_8_;
long null_ARRAY_0061c74_24_8_;
long null_ARRAY_0061c74_32_8_;
long null_ARRAY_0061c74_40_8_;
long null_ARRAY_0061c74_48_8_;
long null_ARRAY_0061c74_8_8_;
long null_ARRAY_0061c8a_0_4_;
long null_ARRAY_0061c8a_16_8_;
long null_ARRAY_0061c8a_4_4_;
long null_ARRAY_0061c8a_8_4_;
long local_5_0_5_;
long local_5_0_6_;
long local_5_0_7_;
long local_5_4_4_;
long DAT_0041788b;
long DAT_00417945;
long DAT_004179c3;
long DAT_00418a4d;
long DAT_00418b56;
long DAT_00418b59;
long DAT_00418bbd;
long DAT_00418bd5;
long DAT_00418be9;
long DAT_00418beb;
long DAT_00418cf0;
long DAT_00418e1e;
long DAT_00418e22;
long DAT_00418e27;
long DAT_00418e29;
long DAT_004194ac;
long DAT_004194c3;
long DAT_00419880;
long DAT_00419c18;
long DAT_00419c1d;
long DAT_00419c87;
long DAT_00419d18;
long DAT_00419d1b;
long DAT_00419d69;
long DAT_00419d6d;
long DAT_00419de0;
long DAT_00419de1;
long DAT_0061c110;
long DAT_0061c120;
long DAT_0061c130;
long DAT_0061c5d0;
long DAT_0061c5e0;
long DAT_0061c658;
long DAT_0061c65c;
long DAT_0061c660;
long DAT_0061c680;
long DAT_0061c690;
long DAT_0061c6a0;
long DAT_0061c6a8;
long DAT_0061c6c0;
long DAT_0061c6c8;
long DAT_0061c710;
long DAT_0061c718;
long DAT_0061c720;
long DAT_0061c728;
long DAT_0061c880;
long DAT_0061c918;
long DAT_0061c920;
long DAT_0061c928;
long DAT_0061c938;
long fde_0041ace0;
long FLOAT_UNKNOWN;
long null_ARRAY_00417a20;
long null_ARRAY_00417a40;
long null_ARRAY_00417ac0;
long null_ARRAY_00418880;
long null_ARRAY_00418be0;
long null_ARRAY_0041a220;
long null_ARRAY_0061c5f0;
long null_ARRAY_0061c620;
long null_ARRAY_0061c6e0;
long null_ARRAY_0061c740;
long null_ARRAY_0061c780;
long null_ARRAY_0061c8a0;
long PTR_DAT_0061c5c0;
long PTR_FUN_0061c5c8;
long PTR_null_ARRAY_0061c600;
long PTR_null_ARRAY_0061c668;
long stack0x00000008;
ulong
FUN_00402513 (uint uParm1)
{
  char cVar1;
  uint uVar2;
  ulong uVar3;

  if (uParm1 == 0)
    {
      func_0x00401bc0 ("Usage: %s [OPTION]... FILE...\n", DAT_0061c728);
      func_0x00401e20
	("Overwrite the specified FILE(s) repeatedly, in order to make it harder\nfor even very expensive hardware probing to recover the data.\n",
	 DAT_0061c680);
      func_0x00401e20 ("\nIf FILE is -, shred standard output.\n",
		       DAT_0061c680);
      FUN_00402334 ();
      func_0x00401bc0
	("  -f, --force    change permissions to allow writing if necessary\n  -n, --iterations=N  overwrite N times instead of the default (%d)\n      --random-source=FILE  get random bytes from FILE\n  -s, --size=N   shred this many bytes (suffixes like K, M, G accepted)\n",
	 3);
      func_0x00401e20
	("  -u             deallocate and remove file after overwriting\n      --remove[=HOW]  like -u but give control on HOW to delete;  See below\n  -v, --verbose  show progress\n  -x, --exact    do not round file sizes up to the next full block;\n                   this is the default for non-regular files\n  -z, --zero     add a final overwrite with zeros to hide shredding\n",
	 DAT_0061c680);
      func_0x00401e20 ("      --help     display this help and exit\n",
		       DAT_0061c680);
      func_0x00401e20
	("      --version  output version information and exit\n",
	 DAT_0061c680);
      func_0x00401e20
	("\nDelete FILE(s) if --remove (-u) is specified.  The default is not to remove\nthe files because it is common to operate on device files like /dev/hda,\nand those files usually should not be removed.\nThe optional HOW parameter indicates how to remove a directory entry:\n\'unlink\' => use a standard unlink call.\n\'wipe\' => also first obfuscate bytes in the name.\n\'wipesync\' => also sync each obfuscated byte to disk.\nThe default mode is \'wipesync\', but note it can be expensive.\n\n",
	 DAT_0061c680);
      func_0x00401e20
	("CAUTION: Note that shred relies on a very important assumption:\nthat the file system overwrites data in place.  This is the traditional\nway to do things, but many modern file system designs do not satisfy this\nassumption.  The following are examples of file systems on which shred is\nnot effective, or is not guaranteed to be effective in all file system modes:\n\n",
	 DAT_0061c680);
      func_0x00401e20
	("* log-structured or journaled file systems, such as those supplied with\nAIX and Solaris (and JFS, ReiserFS, XFS, Ext3, etc.)\n\n* file systems that write redundant data and carry on even if some writes\nfail, such as RAID-based file systems\n\n* file systems that make snapshots, such as Network Appliance\'s NFS server\n\n",
	 DAT_0061c680);
      func_0x00401e20
	("* file systems that cache in temporary locations, such as NFS\nversion 3 clients\n\n* compressed file systems\n\n",
	 DAT_0061c680);
      func_0x00401e20
	("In the case of ext3 file systems, the above disclaimer applies\n(and shred is thus of limited effectiveness) only in data=journal mode,\nwhich journals file data in addition to just metadata.  In both the\ndata=ordered (default) and data=writeback modes, shred works as usual.\nExt3 journaling modes can be changed by adding the data=something option\nto the mount options for a particular file system in the /etc/fstab file,\nas documented in the mount man page (man mount).\n\n",
	 DAT_0061c680);
      func_0x00401e20
	("In addition, file system backups and remote mirrors may contain copies\nof the file that cannot be removed, and that will allow a shredded file\nto be recovered later.\n",
	 DAT_0061c680);
      imperfection_wrapper ();	//    FUN_0040234e();
    }
  else
    {
      func_0x00401e10 (DAT_0061c6a0,
		       "Try \'%s --help\' for more information.\n",
		       DAT_0061c728);
    }
  func_0x00402060 ();
  if ((int) uParm1 < 1)
    {
      uVar3 = 0;
    }
  else
    {
      uParm1 = uParm1 & 0xfff;
      uVar2 = uParm1 | uParm1 << 0xc;
      cVar1 = (char) (uVar2 >> 4);
      if ((cVar1 == (char) (uVar2 >> 8)) && (cVar1 == (char) uParm1))
	{
	  uVar2 = 0;
	}
      else
	{
	  uVar2 = 1;
	}
      uVar3 = (ulong) uVar2;
    }
  return uVar3;
}

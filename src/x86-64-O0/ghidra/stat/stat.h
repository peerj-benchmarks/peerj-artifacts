typedef unsigned char   undefined;

typedef unsigned char    byte;
typedef unsigned char    dwfenc;
typedef unsigned int    dword;
typedef unsigned long    qword;
typedef unsigned char    undefined1;
typedef unsigned short    undefined2;
typedef unsigned int    undefined4;
typedef unsigned long    undefined6;
typedef unsigned long    undefined8;
typedef unsigned short    word;
#define unkbyte9   unsigned long long
#define unkbyte10   unsigned long long
#define unkbyte11   unsigned long long
#define unkbyte12   unsigned long long
#define unkbyte13   unsigned long long
#define unkbyte14   unsigned long long
#define unkbyte15   unsigned long long
#define unkbyte16   unsigned long long

#define unkuint9   unsigned long long
#define unkuint10   unsigned long long
#define unkuint11   unsigned long long
#define unkuint12   unsigned long long
#define unkuint13   unsigned long long
#define unkuint14   unsigned long long
#define unkuint15   unsigned long long
#define unkuint16   unsigned long long

#define unkint9   long long
#define unkint10   long long
#define unkint11   long long
#define unkint12   long long
#define unkint13   long long
#define unkint14   long long
#define unkint15   long long
#define unkint16   long long

#define unkfloat1   float
#define unkfloat2   float
#define unkfloat3   float
#define unkfloat5   double
#define unkfloat6   double
#define unkfloat7   double
#define unkfloat9   long double
#define unkfloat11   long double
#define unkfloat12   long double
#define unkfloat13   long double
#define unkfloat14   long double
#define unkfloat15   long double
#define unkfloat16   long double

#define BADSPACEBASE   void
#define code   void

#define ulong long
#define true 1
#define bool int
#define false 0
#define float10 int
#define ushort int
#define undefined7 long
#define undefined2 long
#define uint5 long
#define uint6 long

// Add definition for uint
typedef unsigned int    uint;

#define sbyte char

extern imperfection_wrapper(void);
typedef struct eh_frame_hdr eh_frame_hdr, *Peh_frame_hdr;

struct eh_frame_hdr {
    byte eh_frame_hdr_version; // Exception Handler Frame Header Version
    dwfenc eh_frame_pointer_encoding; // Exception Handler Frame Pointer Encoding
    dwfenc eh_frame_desc_entry_count_encoding; // Encoding of # of Exception Handler FDEs
    dwfenc eh_frame_table_encoding; // Exception Handler Table Encoding
};

typedef struct fde_table_entry fde_table_entry, *Pfde_table_entry;

struct fde_table_entry {
    dword initial_loc; // Initial Location
    dword data_loc; // Data location
};

typedef enum Elf64_DynTag {
    DT_AUDIT=1879047932,
    DT_AUXILIARY=2147483645,
    DT_BIND_NOW=24,
    DT_CHECKSUM=1879047672,
    DT_CONFIG=1879047930,
    DT_DEBUG=21,
    DT_DEPAUDIT=1879047931,
    DT_ENCODING=32,
    DT_FEATURE_1=1879047676,
    DT_FILTER=2147483647,
    DT_FINI=13,
    DT_FINI_ARRAY=26,
    DT_FINI_ARRAYSZ=28,
    DT_FLAGS=30,
    DT_FLAGS_1=1879048187,
    DT_GNU_CONFLICT=1879047928,
    DT_GNU_CONFLICTSZ=1879047670,
    DT_GNU_HASH=1879047925,
    DT_GNU_LIBLIST=1879047929,
    DT_GNU_LIBLISTSZ=1879047671,
    DT_GNU_PRELINKED=1879047669,
    DT_HASH=4,
    DT_INIT=12,
    DT_INIT_ARRAY=25,
    DT_INIT_ARRAYSZ=27,
    DT_JMPREL=23,
    DT_MOVEENT=1879047674,
    DT_MOVESZ=1879047675,
    DT_MOVETAB=1879047934,
    DT_NEEDED=1,
    DT_NULL=0,
    DT_PLTGOT=3,
    DT_PLTPAD=1879047933,
    DT_PLTPADSZ=1879047673,
    DT_PLTREL=20,
    DT_PLTRELSZ=2,
    DT_POSFLAG_1=1879047677,
    DT_PREINIT_ARRAYSZ=33,
    DT_REL=17,
    DT_RELA=7,
    DT_RELACOUNT=1879048185,
    DT_RELAENT=9,
    DT_RELASZ=8,
    DT_RELCOUNT=1879048186,
    DT_RELENT=19,
    DT_RELSZ=18,
    DT_RPATH=15,
    DT_RUNPATH=29,
    DT_SONAME=14,
    DT_STRSZ=10,
    DT_STRTAB=5,
    DT_SYMBOLIC=16,
    DT_SYMENT=11,
    DT_SYMINENT=1879047679,
    DT_SYMINFO=1879047935,
    DT_SYMINSZ=1879047678,
    DT_SYMTAB=6,
    DT_TEXTREL=22,
    DT_TLSDESC_GOT=1879047927,
    DT_TLSDESC_PLT=1879047926,
    DT_VERDEF=1879048188,
    DT_VERDEFNUM=1879048189,
    DT_VERNEED=1879048190,
    DT_VERNEEDNUM=1879048191,
    DT_VERSYM=1879048176
} Elf64_DynTag;

typedef enum Elf_ProgramHeaderType {
    PT_DYNAMIC=2,
    PT_GNU_EH_FRAME=1685382480,
    PT_GNU_RELRO=1685382482,
    PT_GNU_STACK=1685382481,
    PT_INTERP=3,
    PT_LOAD=1,
    PT_NOTE=4,
    PT_NULL=0,
    PT_PHDR=6,
    PT_SHLIB=5,
    PT_TLS=7
} Elf_ProgramHeaderType;

typedef struct Elf64_Dyn Elf64_Dyn, *PElf64_Dyn;

struct Elf64_Dyn {
    enum Elf64_DynTag d_tag;
    qword d_val;
};

typedef struct Elf64_Rela Elf64_Rela, *PElf64_Rela;

struct Elf64_Rela {
    qword r_offset; // location to apply the relocation action
    qword r_info; // the symbol table index and the type of relocation
    qword r_addend; // a constant addend used to compute the relocatable field value
};

typedef struct Elf64_Shdr Elf64_Shdr, *PElf64_Shdr;

typedef enum Elf_SectionHeaderType {
    SHT_CHECKSUM=1879048184,
    SHT_DYNAMIC=6,
    SHT_DYNSYM=11,
    SHT_FINI_ARRAY=15,
    SHT_GNU_ATTRIBUTES=1879048181,
    SHT_GNU_HASH=1879048182,
    SHT_GNU_LIBLIST=1879048183,
    SHT_GNU_verdef=1879048189,
    SHT_GNU_verneed=1879048190,
    SHT_GNU_versym=1879048191,
    SHT_GROUP=17,
    SHT_HASH=5,
    SHT_INIT_ARRAY=14,
    SHT_NOBITS=8,
    SHT_NOTE=7,
    SHT_NULL=0,
    SHT_PREINIT_ARRAY=16,
    SHT_PROGBITS=1,
    SHT_REL=9,
    SHT_RELA=4,
    SHT_SHLIB=10,
    SHT_STRTAB=3,
    SHT_SUNW_COMDAT=1879048187,
    SHT_SUNW_move=1879048186,
    SHT_SUNW_syminfo=1879048188,
    SHT_SYMTAB=2,
    SHT_SYMTAB_SHNDX=18
} Elf_SectionHeaderType;

struct Elf64_Shdr {
    dword sh_name;
    enum Elf_SectionHeaderType sh_type;
    qword sh_flags;
    qword sh_addr;
    qword sh_offset;
    qword sh_size;
    dword sh_link;
    dword sh_info;
    qword sh_addralign;
    qword sh_entsize;
};

typedef struct Elf64_Phdr Elf64_Phdr, *PElf64_Phdr;

struct Elf64_Phdr {
    enum Elf_ProgramHeaderType p_type;
    dword p_flags;
    qword p_offset;
    qword p_vaddr;
    qword p_paddr;
    qword p_filesz;
    qword p_memsz;
    qword p_align;
};

typedef struct Elf64_Sym Elf64_Sym, *PElf64_Sym;

struct Elf64_Sym {
    dword st_name;
    byte st_info;
    byte st_other;
    word st_shndx;
    qword st_value;
    qword st_size;
};

typedef struct Elf64_Ehdr Elf64_Ehdr, *PElf64_Ehdr;

struct Elf64_Ehdr {
    byte e_ident_magic_num;
    char e_ident_magic_str[3];
    byte e_ident_class;
    byte e_ident_data;
    byte e_ident_version;
    byte e_ident_pad[9];
    word e_type;
    word e_machine;
    dword e_version;
    qword e_entry;
    qword e_phoff;
    qword e_shoff;
    dword e_flags;
    word e_ehsize;
    word e_phentsize;
    word e_phnum;
    word e_shentsize;
    word e_shnum;
    word e_shstrndx;
};




undefined8 _DT_INIT(void);
void FUN_00401d90(void);
void FUN_00401fa0(void);
void FUN_00402230(void);
void entry(void);
void FUN_00402460(void);
void FUN_004024e0(void);
void FUN_00402560(void);
ulong FUN_0040259e(byte bParm1);
void FUN_004025ad(void);
void FUN_004025c7(undefined *puParm1);
void FUN_00402763(undefined8 uParm1,undefined8 uParm2);
char * FUN_00402788(ulong *puParm1);
undefined1 * FUN_004033cb(undefined8 uParm1);
undefined1 * FUN_004033f6(undefined8 uParm1,undefined8 uParm2);
void FUN_004034bd(long lParm1,long lParm2,undefined8 uParm3,undefined8 uParm4);
void FUN_00403594(undefined8 uParm1,undefined8 uParm2,undefined8 uParm3);
void FUN_004035dc(undefined8 uParm1,undefined8 uParm2,undefined8 uParm3);
void FUN_00403624(undefined8 uParm1,undefined8 uParm2,undefined8 uParm3);
void FUN_0040366c(undefined8 uParm1,undefined8 uParm2,undefined8 uParm3);
void FUN_004036b4(undefined8 uParm1,undefined8 uParm2,undefined8 uParm3);
void FUN_004036fc(undefined8 uParm1,undefined8 uParm2);
void FUN_0040374c(char *pcParm1,char *pcParm2,undefined8 uParm3,long lParm4,long lParm5);
ulong FUN_00403b83(long lParm1,long lParm2,undefined8 uParm3);
undefined8 FUN_00403c79(undefined8 uParm1,undefined8 uParm2,undefined4 uParm3,undefined8 uParm4,undefined8 uParm5,undefined8 *puParm6);
char * FUN_00403eec(undefined8 uParm1);
ulong FUN_00404039(undefined8 uParm1,undefined8 uParm2,undefined8 uParm3,long lParm4);
void FUN_0040417c(undefined8 uParm1,undefined8 uParm2,undefined8 uParm3);
void FUN_004041ad(undefined8 uParm1,long lParm2);
void FUN_004041ef(void);
ulong FUN_00404294(undefined8 uParm1,undefined8 uParm2,undefined4 uParm3,uint uParm4,undefined8 uParm5,undefined8 *puParm6);
void FUN_00404968(char cParm1);
ulong FUN_004049ec(char *pcParm1,uint uParm2,undefined8 uParm3,code *pcParm4,undefined8 uParm5);
ulong FUN_00404e43(undefined8 uParm1,undefined8 uParm2);
ulong FUN_00404f53(undefined8 uParm1,undefined8 uParm2,undefined8 uParm3);
undefined8 FUN_004050e8(char cParm1,char cParm2,char cParm3);
ulong FUN_0040521d(uint uParm1);
ulong FUN_0040539d(uint uParm1,undefined8 *puParm2);
undefined8 FUN_00405662(undefined8 uParm1,long lParm2);
long FUN_0040599f(undefined8 uParm1,ulong uParm2);
long FUN_00405af1(undefined8 uParm1,long lParm2,long lParm3,long lParm4);
void FUN_00405c20(undefined8 uParm1);
ulong FUN_00405c3f(long *plParm1,undefined8 uParm2,undefined8 uParm3);
char * FUN_00405ce9(char *pcParm1,uint uParm2);
void FUN_004064ef(void);
long FUN_004065d4(undefined8 uParm1);
ulong FUN_00406602(char *pcParm1);
undefined * FUN_0040668b(undefined8 uParm1);
char * FUN_00406722(char *pcParm1);
void FUN_0040678b(long lParm1,undefined8 uParm2,undefined8 *puParm3);
ulong FUN_00406826(long lParm1,undefined8 uParm2,undefined8 *puParm3);
char * FUN_00406882(long lParm1);
undefined8 FUN_00406964(uint uParm1);
void FUN_00406a0f(uint uParm1,undefined *puParm2);
void FUN_00406bda(long lParm1,undefined8 uParm2);
long FUN_00406c01(long *plParm1,undefined8 uParm2);
long FUN_00406c58(long lParm1,long lParm2);
ulong FUN_00406ceb(ulong uParm1);
ulong FUN_00406d57(ulong uParm1);
ulong FUN_00406d9e(undefined8 uParm1,ulong uParm2);
ulong FUN_00406dd5(ulong uParm1,ulong uParm2);
undefined8 FUN_00406dee(long lParm1);
ulong FUN_00406ee7(ulong uParm1,long lParm2);
long * FUN_00406fdd(undefined8 uParm1,undefined1 *puParm2,code *pcParm3,code *pcParm4,long lParm5);
void FUN_00407145(long **pplParm1,undefined8 uParm2);
long FUN_0040726f(long lParm1);
void FUN_004072ba(long lParm1,undefined8 *puParm2);
long FUN_004072ef(long lParm1,long lParm2,long **pplParm3,char cParm4);
undefined8 FUN_00407484(long lParm1,long **pplParm2,char cParm3);
undefined8 FUN_00407652(long *plParm1,undefined8 uParm2);
undefined8 FUN_00407856(long lParm1,long lParm2,long *plParm3);
undefined8 FUN_00407ba8(undefined8 uParm1,undefined8 uParm2);
ulong FUN_00407bf1(undefined8 *puParm1,ulong uParm2);
ulong FUN_00407c3d(undefined8 *puParm1,undefined8 *puParm2);
void FUN_00407cb5(undefined8 *puParm1);
char * FUN_00407ce6(long lParm1,long lParm2);
long FUN_00407e18(long lParm1,long lParm2,long lParm3);
long FUN_00407e71(long lParm1,long lParm2,long lParm3);
ulong FUN_00407eca(int iParm1,int iParm2);
void FUN_00407f1b(undefined8 uParm1,undefined8 uParm2,undefined8 uParm3,undefined8 uParm4,undefined8 uParm5,uint uParm6);
char * FUN_00407f71(char *param_1,long param_2,char *param_3,undefined8 *param_4,byte param_5,undefined8 param_6,undefined8 param_7,uint param_8);
void FUN_0040a096(long lParm1);
ulong FUN_0040a173(undefined1 *puParm1);
void FUN_0040a191(undefined1 *puParm1,undefined4 uParm2);
ulong FUN_0040a1b5(undefined1 *puParm1,byte bParm2,uint uParm3);
undefined8 * FUN_0040a23d(undefined8 *puParm1,int iParm2);
char * FUN_0040a2b4(char *pcParm1,int iParm2);
ulong FUN_0040a354(long param_1,ulong param_2,long param_3,ulong param_4,uint param_5,uint param_6,long param_7,char *param_8,char *param_9);
undefined1 * FUN_0040b21e(int iParm1,undefined8 uParm2,undefined8 uParm3,uint *puParm4);
void FUN_0040b491(uint uParm1,uint uParm2,undefined8 uParm3);
void FUN_0040b4d2(uint uParm1,undefined8 uParm2);
void FUN_0040b4f6(undefined8 uParm1,undefined8 uParm2,char cParm3);
void FUN_0040b58a(undefined8 uParm1,char cParm2);
void FUN_0040b5b4(undefined8 uParm1);
void FUN_0040b5d3(uint uParm1,undefined8 uParm2,undefined8 uParm3);
void FUN_0040b5ff(uint uParm1,undefined8 uParm2);
void FUN_0040b628(undefined8 uParm1);
undefined8 FUN_0040b647(int *piParm1);
void FUN_0040b6c1(uint *puParm1);
void FUN_0040b6f8(uint *puParm1);
void FUN_0040b72d(void);
undefined8 FUN_0040b737(void);
undefined8 FUN_0040b759(void);
void FUN_0040b77b(long lParm1);
void FUN_0040b791(long lParm1);
void FUN_0040b7a7(long lParm1);
void FUN_0040b7bd(void);
ulong FUN_0040b7df(uint uParm1);
void FUN_0040b7ef(undefined8 uParm1,long lParm2,undefined8 uParm3,undefined8 uParm4,undefined8 *puParm5,undefined8 uParm6);
void FUN_0040bc53(undefined8 uParm1,undefined8 uParm2,undefined8 uParm3,undefined8 uParm4,uint *puParm5);
void FUN_0040bd25(undefined4 uParm1,undefined4 uParm2,undefined4 uParm3,undefined4 uParm4,undefined4 uParm5,undefined4 uParm6,undefined4 uParm7,undefined4 uParm8,undefined8 param_1,undefined8 param_2,undefined8 param_3,undefined8 param_4,undefined8 param_5,undefined8 param_6);
void FUN_0040bddb(undefined8 uParm1);
long FUN_0040bdf5(long lParm1);
long FUN_0040be2a(long lParm1,long lParm2);
void FUN_0040be8b(undefined8 uParm1,undefined8 uParm2);
void FUN_0040bebf(undefined8 uParm1);
long FUN_0040beec(void);
long FUN_0040bf16(void);
undefined8 FUN_0040bf4f(undefined4 uParm1,undefined4 uParm2,undefined4 uParm3,undefined4 uParm4,undefined4 uParm5,undefined4 uParm6,undefined4 uParm7,undefined4 uParm8,undefined8 param_1,undefined8 param_2,undefined8 param_3,undefined8 param_4,undefined8 param_5,undefined8 param_6);
undefined8 FUN_0040bffe(ulong uParm1,ulong uParm2);
void FUN_0040c07d(undefined4 *puParm1);
void FUN_0040c091(uint *puParm1);
void FUN_0040c0ac(uint *puParm1);
undefined8 FUN_0040c100(uint *puParm1,undefined8 uParm2);
long FUN_0040c15a(long lParm1);
ulong FUN_0040c188(char *pcParm1);
ulong FUN_0040c48f(uint uParm1);
void FUN_0040c4b8(void);
void FUN_0040c4ec(uint uParm1);
void FUN_0040c560(uint uParm1,uint uParm2,undefined8 uParm3,undefined8 uParm4);
void FUN_0040c5e4(undefined4 uParm1,undefined4 uParm2,undefined4 uParm3,undefined4 uParm4,undefined4 uParm5,undefined4 uParm6,undefined4 uParm7,undefined4 uParm8,uint param_1,uint param_2,undefined8 param_3,undefined8 param_4,undefined8 param_5,undefined8 param_6);
long FUN_0040c6c8(long lParm1,undefined *puParm2);
void FUN_0040cc51(long lParm1,int *piParm2);
ulong FUN_0040ce35(int param_1,undefined8 *param_2,char *param_3,long *param_4,int *param_5,int param_6,int *param_7,int param_8,undefined8 param_9);
char * FUN_0040d41f(undefined8 uParm1,undefined8 uParm2,char *pcParm3,int *piParm4,int iParm5);
ulong FUN_0040d4ed(uint param_1,undefined8 *param_2,char *param_3,long param_4,undefined8 param_5,uint param_6,uint *param_7,uint param_8);
ulong FUN_0040dcb6(uint param_1,undefined8 param_2,undefined8 param_3,undefined8 param_4,undefined8 param_5,uint param_6,uint param_7);
void FUN_0040dd46(uint uParm1,undefined8 uParm2,undefined8 uParm3,undefined8 uParm4,undefined8 uParm5);
void FUN_0040dd90(undefined8 uParm1,undefined8 uParm2);
ulong FUN_0040ddb5(long lParm1,long lParm2);
undefined8 FUN_0040de6c(long lParm1);
ulong FUN_0040de9d(uint *puParm1,byte *pbParm2,long lParm3,undefined8 uParm4);
ulong * FUN_0040df20(ulong *puParm1,char cParm2,ulong uParm3);
undefined8 FUN_0040e032(void);
void FUN_0040e043(long lParm1);
char ** FUN_0040e1e3(void);
void FUN_0040eb17(undefined8 *puParm1);
void FUN_0040eb7f(uint uParm1,undefined8 uParm2,uint uParm3,uint uParm4);
ulong FUN_0040ebaf(undefined4 uParm1,undefined4 uParm2,undefined4 uParm3,undefined4 uParm4,undefined4 uParm5,undefined4 uParm6,undefined4 uParm7,undefined4 uParm8,uint param_1,undefined8 param_2,uint param_3,undefined8 param_4,undefined8 param_5,undefined8 param_6);
undefined8 FUN_0040ed76(long lParm1,undefined8 uParm2,undefined8 uParm3);
undefined8 FUN_0040ee1c(long lParm1,long lParm2);
void FUN_0040ee85(undefined8 uParm1,undefined8 uParm2);
ulong FUN_0040eeaa(long lParm1,long lParm2);
long FUN_0040ef3a(long lParm1,ulong uParm2,long *plParm3);
long FUN_0040f162(long lParm1,long lParm2,long lParm3,ulong uParm4);
long FUN_0040f45f(long lParm1,long lParm2,long lParm3,ulong uParm4);
char * FUN_0040f910(char *pcParm1,char *pcParm2);
ulong FUN_0040fa6d(int iParm1,int iParm2);
ulong FUN_0040faa8(uint *puParm1,uint *puParm2);
void FUN_0040fb50(long lParm1,undefined8 uParm2,long lParm3);
undefined8 * FUN_0040fb8b(long lParm1);
undefined8 FUN_0040fc3e(long **pplParm1,char *pcParm2);
void FUN_0040fe0e(undefined8 *puParm1);
void FUN_0040fe4f(void);
void FUN_0040fe5f(long lParm1);
ulong FUN_0040fe96(long lParm1);
long FUN_0040fedc(long lParm1);
ulong FUN_0040ffa8(long lParm1);
undefined8 FUN_00410013(long lParm1,undefined8 uParm2,undefined8 uParm3);
long FUN_004100bf(long lParm1,undefined8 uParm2);
void FUN_00410194(long lParm1);
void FUN_004101c3(void);
ulong FUN_0041024e(char *pcParm1);
ulong FUN_004102c2(undefined8 uParm1,undefined8 uParm2,undefined8 uParm3);
ulong FUN_004103e3(ulong uParm1,undefined4 uParm2);
ulong FUN_004103ff(byte *pbParm1,byte *pbParm2);
undefined8 FUN_00410476(uint uParm1,char cParm2);
undefined8 FUN_004104f1(undefined8 uParm1);
void FUN_0041057c(undefined4 uParm1,undefined4 uParm2,undefined4 uParm3,undefined4 uParm4,undefined4 uParm5,undefined4 uParm6,undefined4 uParm7,undefined4 uParm8,undefined8 param_1,uint param_2,undefined8 param_3,undefined8 param_4,undefined8 param_5,undefined8 param_6);
undefined8 FUN_0041068b(void);
ulong FUN_00410698(uint uParm1);
ulong FUN_00410769(char *pcParm1,ulong uParm2);
undefined1 * FUN_004107c3(void);
char * FUN_00410b76(void);
ulong FUN_00410c44(uint uParm1);
undefined * FUN_00410c94(long lParm1,ulong *puParm2);
undefined8 FUN_00410e16(char *pcParm1,undefined8 uParm2);
ulong FUN_00410eba(undefined8 uParm1);
ulong FUN_00410f72(undefined4 uParm1,undefined4 uParm2,undefined4 uParm3,undefined4 uParm4,undefined4 uParm5,undefined4 uParm6,undefined4 uParm7,undefined4 uParm8,uint param_1,uint param_2,undefined8 param_3,undefined8 param_4,undefined8 param_5,undefined8 param_6);
undefined8 FUN_00411229(uint uParm1);
void FUN_0041128c(undefined8 uParm1);
void FUN_004112b0(void);
ulong FUN_004112be(long lParm1);
undefined8 FUN_0041138e(undefined8 uParm1);
void FUN_004113ad(undefined8 uParm1,undefined8 uParm2,uint uParm3);
void FUN_004113d8(uint uParm1,undefined8 uParm2,undefined8 uParm3,uint uParm4);
void FUN_00411405(uint uParm1,undefined8 uParm2,undefined8 uParm3,uint uParm4);
ulong FUN_00411442(uint uParm1,long lParm2,long lParm3,uint uParm4);
void FUN_00411555(undefined8 uParm1,undefined8 uParm2,undefined8 uParm3);
long FUN_00411587(long lParm1,undefined4 uParm2);
ulong FUN_00411600(ulong uParm1);
ulong FUN_004116ab(int iParm1,int iParm2);
long FUN_004116e6(ulong param_1,long param_2,int param_3,int param_4,int param_5,uint param_6,int param_7,int param_8,int param_9,int param_10);
long FUN_00411939(undefined8 uParm1,undefined8 uParm2);
long FUN_00411988(undefined8 param_1,undefined8 param_2,uint param_3,uint param_4,uint param_5,long param_6,uint *param_7);
void FUN_00411af1(code *pcParm1,undefined8 uParm2,undefined8 uParm3);
long FUN_00411b23(undefined8 uParm1,long *plParm2,undefined8 uParm3);
long FUN_00411c66(undefined8 *puParm1,undefined8 uParm2,long *plParm3);
void FUN_00412452(undefined8 uParm1);
ulong FUN_0041247b(char *pcParm1,char *pcParm2,uint uParm3);
ulong FUN_0041260e(void);
long FUN_0041265b(ulong uParm1,long lParm2,ulong uParm3,long lParm4,long *plParm5);
int * FUN_004128a7(ulong uParm1,uint *puParm2,ulong uParm3,uint *puParm4,ulong *puParm5);
char * FUN_00413387(ulong uParm1,long lParm2,long lParm3);
long FUN_004135f5(int *param_1,long *param_2);
long FUN_004137e0(undefined auParm1 [16],int *piParm2,long *plParm3);
undefined8 FUN_00413b9f(int iParm1,long lParm2,uint *puParm3,long lParm4,uint uParm5);
void FUN_004143b2(uint param_1);
void FUN_004143fc(undefined8 uParm1,uint uParm2);
ulong FUN_0041444e(void);
ulong FUN_004147e0(undefined auParm1 [16],undefined8 uParm2,undefined8 uParm3);
ulong FUN_00414bb8(char *pcParm1,long lParm2);
undefined8 FUN_00414c11(long param_1,long param_2,undefined param_3,int param_4,undefined8 param_5,ulong param_6,int param_7,ulong param_8,int param_9);
long FUN_00414ed4(long lParm1,char **ppcParm2,long lParm3,undefined8 uParm4);
ulong FUN_0041c33e(long *plParm1,undefined8 uParm2,undefined8 uParm3);
ulong FUN_0041c3b8(uint uParm1);
undefined8 * FUN_0041c3d7(ulong uParm1);
void FUN_0041c49a(ulong uParm1);
void FUN_0041c573(undefined8 uParm1,int *piParm2,undefined8 uParm3);
void FUN_0041c614(int *param_1);
void FUN_0041c6d3(uint uParm1);
ulong FUN_0041c6f9(ulong uParm1,long lParm2);
void FUN_0041c72d(undefined8 uParm1,undefined8 uParm2,undefined8 uParm3);
void FUN_0041c768(undefined8 uParm1,undefined8 uParm2,undefined8 uParm3,undefined8 uParm4);
ulong FUN_0041c7b9(ulong uParm1,ulong uParm2);
void FUN_0041c7d4(void);
long FUN_0041c7da(long *plParm1,ulong *puParm2,int iParm3,long lParm4);
undefined8 FUN_0041c9b4(uint *puParm1,ulong *puParm2);
undefined8 FUN_0041d154(char *pcParm1,ulong *puParm2,char **ppcParm3);
ulong FUN_0041e40a(undefined auParm1 [16]);
ulong FUN_0041e445(void);
void FUN_0041e480(void);
undefined8 _DT_FINI(void);
undefined FUN_00626000();
undefined FUN_00626008();
undefined FUN_00626010();
undefined FUN_00626018();
undefined FUN_00626020();
undefined FUN_00626028();
undefined FUN_00626030();
undefined FUN_00626038();
undefined FUN_00626040();
undefined FUN_00626048();
undefined FUN_00626050();
undefined FUN_00626058();
undefined FUN_00626060();
undefined FUN_00626068();
undefined FUN_00626070();
undefined FUN_00626078();
undefined FUN_00626080();
undefined FUN_00626088();
undefined FUN_00626090();
undefined FUN_00626098();
undefined FUN_006260a0();
undefined FUN_006260a8();
undefined FUN_006260b0();
undefined FUN_006260b8();
undefined FUN_006260c0();
undefined FUN_006260c8();
undefined FUN_006260d0();
undefined FUN_006260d8();
undefined FUN_006260e0();
undefined FUN_006260e8();
undefined FUN_006260f0();
undefined FUN_006260f8();
undefined FUN_00626100();
undefined FUN_00626108();
undefined FUN_00626110();
undefined FUN_00626118();
undefined FUN_00626120();
undefined FUN_00626128();
undefined FUN_00626130();
undefined FUN_00626138();
undefined FUN_00626140();
undefined FUN_00626148();
undefined FUN_00626150();
undefined FUN_00626158();
undefined FUN_00626160();
undefined FUN_00626168();
undefined FUN_00626170();
undefined FUN_00626178();
undefined FUN_00626180();
undefined FUN_00626188();
undefined FUN_00626190();
undefined FUN_00626198();
undefined FUN_006261a0();
undefined FUN_006261a8();
undefined FUN_006261b0();
undefined FUN_006261b8();
undefined FUN_006261c0();
undefined FUN_006261c8();
undefined FUN_006261d0();
undefined FUN_006261d8();
undefined FUN_006261e0();
undefined FUN_006261e8();
undefined FUN_006261f0();
undefined FUN_006261f8();
undefined FUN_00626200();
undefined FUN_00626208();
undefined FUN_00626210();
undefined FUN_00626218();
undefined FUN_00626220();
undefined FUN_00626228();
undefined FUN_00626230();
undefined FUN_00626238();
undefined FUN_00626240();
undefined FUN_00626248();
undefined FUN_00626250();
undefined FUN_00626258();
undefined FUN_00626260();
undefined FUN_00626268();
undefined FUN_00626270();
undefined FUN_00626278();
undefined FUN_00626280();
undefined FUN_00626288();
undefined FUN_00626290();
undefined FUN_00626298();
undefined FUN_006262a0();
undefined FUN_006262a8();
undefined FUN_006262b0();
undefined FUN_006262b8();
undefined FUN_006262c0();
undefined FUN_006262c8();
undefined FUN_006262d0();
undefined FUN_006262d8();
undefined FUN_006262e0();
undefined FUN_006262e8();
undefined FUN_006262f0();
undefined FUN_006262f8();
undefined FUN_00626300();
undefined FUN_00626308();
undefined FUN_00626310();
undefined FUN_00626318();
undefined FUN_00626320();
undefined FUN_00626328();


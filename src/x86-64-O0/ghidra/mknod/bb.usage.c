
#include "mknod.h"

long null_ARRAY_0061641_0_8_;
long null_ARRAY_0061641_8_8_;
long null_ARRAY_0061658_0_8_;
long null_ARRAY_0061658_16_8_;
long null_ARRAY_0061658_24_8_;
long null_ARRAY_0061658_32_8_;
long null_ARRAY_0061658_40_8_;
long null_ARRAY_0061658_48_8_;
long null_ARRAY_0061658_8_8_;
long null_ARRAY_006166c_0_4_;
long null_ARRAY_006166c_16_8_;
long null_ARRAY_006166c_4_4_;
long null_ARRAY_006166c_8_4_;
long DAT_00412b0b;
long DAT_00412bc5;
long DAT_00412c43;
long DAT_00413221;
long DAT_004132c6;
long DAT_0041338f;
long DAT_004133de;
long DAT_00413530;
long DAT_0041365e;
long DAT_00413662;
long DAT_00413667;
long DAT_00413669;
long DAT_00413cd3;
long DAT_004140a0;
long DAT_00414430;
long DAT_00414435;
long DAT_0041449f;
long DAT_00414530;
long DAT_00414533;
long DAT_00414581;
long DAT_00414585;
long DAT_004145f8;
long DAT_004145f9;
long DAT_00616000;
long DAT_00616010;
long DAT_00616020;
long DAT_006163e8;
long DAT_00616400;
long DAT_00616478;
long DAT_0061647c;
long DAT_00616480;
long DAT_006164c0;
long DAT_006164d0;
long DAT_006164e0;
long DAT_006164e8;
long DAT_00616500;
long DAT_00616508;
long DAT_00616550;
long DAT_00616558;
long DAT_00616560;
long DAT_006166f8;
long DAT_00616700;
long DAT_00616708;
long DAT_00616718;
long fde_00415320;
long FLOAT_UNKNOWN;
long null_ARRAY_00412cc0;
long null_ARRAY_00414a40;
long null_ARRAY_00616410;
long null_ARRAY_00616440;
long null_ARRAY_00616520;
long null_ARRAY_00616580;
long null_ARRAY_006165c0;
long null_ARRAY_006166c0;
long PTR_DAT_006163e0;
long PTR_null_ARRAY_00616420;
long stack0x00000008;
undefined8
FUN_00401dad (uint uParm1)
{
  char cVar1;
  int iVar2;
  undefined8 uVar3;
  uint *puVar4;
  char *pcVar5;
  uint uVar6;
  ulong uStack160;
  ulong uStack152;
  ulong uStack144;
  undefined8 uStack136;
  undefined8 uStack128;
  ulong uStack120;
  uint uStack108;
  long lStack104;
  int iStack92;
  char cStack85;
  int iStack84;
  long lStack80;
  uint uStack68;
  long lStack64;
  uint uStack52;
  code *pcStack32;
  uint local_c;

  if (uParm1 == 0)
    {
      pcStack32 = (code *) 0x401dfc;
      local_c = uParm1;
      func_0x004015c0 ("Usage: %s [OPTION]... NAME TYPE [MAJOR MINOR]\n",
		       DAT_00616560);
      pcStack32 = (code *) 0x401e10;
      func_0x00401760 ("Create the special file NAME of the given TYPE.\n",
		       DAT_006164c0);
      pcStack32 = (code *) 0x401e15;
      FUN_00401bae ();
      pcStack32 = (code *) 0x401e29;
      func_0x00401760
	("  -m, --mode=MODE    set file permission bits to MODE, not a=rw - umask\n",
	 DAT_006164c0);
      pcStack32 = (code *) 0x401e3d;
      func_0x00401760
	("  -Z                   set the SELinux security context to default type\n      --context[=CTX]  like -Z, or if CTX is specified then set the SELinux\n                         or SMACK security context to CTX\n",
	 DAT_006164c0);
      pcStack32 = (code *) 0x401e51;
      func_0x00401760 ("      --help     display this help and exit\n",
		       DAT_006164c0);
      pcStack32 = (code *) 0x401e65;
      func_0x00401760
	("      --version  output version information and exit\n",
	 DAT_006164c0);
      pcStack32 = (code *) 0x401e79;
      func_0x00401760
	("\nBoth MAJOR and MINOR must be specified when TYPE is b, c, or u, and they\nmust be omitted when TYPE is p.  If MAJOR or MINOR begins with 0x or 0X,\nit is interpreted as hexadecimal; otherwise, if it begins with 0, as octal;\notherwise, as decimal.  TYPE may be:\n",
	 DAT_006164c0);
      pcStack32 = (code *) 0x401e8d;
      func_0x00401760
	("\n  b      create a block (buffered) special file\n  c, u   create a character (unbuffered) special file\n  p      create a FIFO\n",
	 DAT_006164c0);
      pcVar5 = "mknod";
      pcStack32 = (code *) 0x401ea1;
      func_0x004015c0
	("\nNOTE: your shell may have its own version of %s, which usually supersedes\nthe version described here.  Please refer to your shell\'s documentation\nfor details about the options it supports.\n");
      pcStack32 = (code *) 0x401eab;
      imperfection_wrapper ();	//    FUN_00401bc8();
    }
  else
    {
      pcVar5 = "Try \'%s --help\' for more information.\n";
      pcStack32 = (code *) 0x401dde;
      local_c = uParm1;
      func_0x00401750 (DAT_006164e0,
		       "Try \'%s --help\' for more information.\n",
		       DAT_00616560);
    }
  pcStack32 = FUN_00401eb5;
  uVar6 = local_c;
  func_0x00401920 ();
  lStack64 = 0;
  lStack80 = 0;
  cStack85 = '\0';
  pcStack32 = (code *) & stack0xfffffffffffffff8;
  FUN_00402dfd (*(undefined8 *) pcVar5);
  func_0x004018d0 (6, &DAT_00412c43);
  func_0x004018b0 (FUN_004026e7);
  while (1)
    {
      if (iStack92 == -0x82)
	{
	  FUN_00401dad (0);
	LAB_00401f94:
	  ;
	  imperfection_wrapper ();	//      FUN_0040495b(DAT_006164c0, "mknod", "GNU coreutils", PTR_DAT_006163e0, "David MacKenzie", 0);
	  func_0x00401920 (0);
	LAB_00401fd2:
	  ;
	  imperfection_wrapper ();	//      FUN_00401dad();
	}
      else
	{
	  if (iStack92 < -0x81)
	    {
	      if (iStack92 == -0x83)
		goto LAB_00401f94;
	      goto LAB_00401fd2;
	    }
	  if (iStack92 == 0x5a)
	    {
	      cVar1 = FUN_00401da2 ();
	      if (cVar1 == '\0')
		{
		  if (DAT_00616718 != 0)
		    {
		      imperfection_wrapper ();	//            FUN_00405128(0, 0, "warning: ignoring --context; it requires an SELinux/SMACK-enabled kernel");
		    }
		}
	      else
		{
		  lStack80 = DAT_00616718;
		}
	    }
	  else
	    {
	      if (iStack92 != 0x6d)
		goto LAB_00401fd2;
	      lStack64 = DAT_00616718;
	    }
	}
    }
  uStack52 = 0x1b6;
  if (lStack64 != 0)
    {
      lStack104 = FUN_00402834 (lStack64);
      if (lStack104 == 0)
	{
	  imperfection_wrapper ();	//      FUN_00405128(1, 0, "invalid mode");
	}
      uStack108 = func_0x004017a0 (0);
      func_0x004017a0 ((ulong) uStack108);
      uStack52 =
	FUN_00402c64 ((ulong) uStack52, 0, (ulong) uStack108, lStack104, 0);
      func_0x00401a10 (lStack104);
      if ((uStack52 & 0xfffffe00) != 0)
	{
	  imperfection_wrapper ();	//      FUN_00405128(1, 0, "mode must specify only file permission bits");
	}
    }
  if ((DAT_00616478 < (int) uVar6)
      &&
      (((int) uVar6 <= DAT_00616478 + 1
	|| (*(char *) ((undefined8 *) pcVar5)[(long) DAT_00616478 + 1] !=
	    'p'))))
    {
      uStack120 = 4;
    }
  else
    {
      uStack120 = 2;
    }
  if ((ulong) (long) (int) (uVar6 - DAT_00616478) < uStack120)
    {
      if (DAT_00616478 < (int) uVar6)
	{
	  imperfection_wrapper ();	//      uVar3 = FUN_004043e8(((undefined8 *)pcVar5)[(long)(int)uVar6 + -1]);
	  imperfection_wrapper ();	//      FUN_00405128(0, 0, "missing operand after %s", uVar3);
	}
      else
	{
	  imperfection_wrapper ();	//      FUN_00405128(0, 0, "missing operand");
	}
      if ((uStack120 == 4) && (uVar6 - DAT_00616478 == 2))
	{
	  func_0x00401750 (DAT_006164e0, &DAT_004132c6,
			   "Special files require major and minor device numbers.");
	}
      FUN_00401dad (1);
    }
  if (uStack120 < (ulong) (long) (int) (uVar6 - DAT_00616478))
    {
      imperfection_wrapper ();	//    uVar3 = FUN_004043e8(((undefined8 *)pcVar5)[uStack120 + (long)DAT_00616478]);
      imperfection_wrapper ();	//    FUN_00405128(0, 0, "extra operand %s", uVar3);
      if ((uStack120 == 2) && (uVar6 - DAT_00616478 == 4))
	{
	  func_0x00401750 (DAT_006164e0, &DAT_004132c6,
			   "Fifos do not have major and minor device numbers.");
	}
      FUN_00401dad (1);
    }
  if (lStack80 != 0)
    {
      iStack84 = 0;
      cVar1 = FUN_00401da2 ();
      if (cVar1 == '\0')
	{
	  uVar3 = FUN_00401d64 (lStack80);
	  imperfection_wrapper ();	//      iStack84 = FUN_00404407(uVar3);
	}
      else
	{
	  imperfection_wrapper ();	//      iStack84 = FUN_00401d93(lStack80);
	}
      if (iStack84 < 0)
	{
	  imperfection_wrapper ();	//      uVar3 = FUN_004043e8(lStack80);
	  puVar4 = (uint *) func_0x00401910 ();
	  imperfection_wrapper ();	//      FUN_00405128(1, (ulong)*puVar4, "failed to set default file creation context to %s", uVar3);
	}
    }
  cVar1 = *(char *) ((undefined8 *) pcVar5)[(long) DAT_00616478 + 1];
  if (cVar1 == 'c')
    {
    LAB_0040234b:
      ;
      uStack68 = 0x2000;
    LAB_00402353:
      ;
      uStack128 = ((undefined8 *) pcVar5)[(long) DAT_00616478 + 2];
      uStack136 = ((undefined8 *) pcVar5)[(long) DAT_00616478 + 3];
      iVar2 = FUN_00404bf2 (uStack128, 0, 0, &uStack152, 0);
      if ((iVar2 != 0) || ((uStack152 & 0xffffffff) != uStack152))
	{
	  imperfection_wrapper ();	//      uVar3 = FUN_004043e8(uStack128);
	  imperfection_wrapper ();	//      FUN_00405128(1, 0, "invalid major device number %s", uVar3);
	}
      iVar2 = FUN_00404bf2 (uStack136, 0, 0, &uStack160, 0);
      if ((iVar2 != 0) || ((uStack160 & 0xffffffff) != uStack160))
	{
	  imperfection_wrapper ();	//      uVar3 = FUN_004043e8(uStack136);
	  imperfection_wrapper ();	//      FUN_00405128(1, 0, "invalid minor device number %s", uVar3);
	}
      uStack144 =
	uStack160 & 0xff | (ulong) ((uint) uStack152 & 0xfffff000) << 0x20 |
	(ulong) ((uint) uStack152 & 0xfff) << 8 | (ulong) ((uint) uStack160 &
							   0xffffff00) << 0xc;
      if (cStack85 != '\0')
	{
	  imperfection_wrapper ();	//      FUN_00401d72(((undefined8 *)pcVar5)[(long)DAT_00616478], (ulong)uStack68, (ulong)uStack68);
	}
      imperfection_wrapper ();	//    iVar2 = FUN_00406492(((undefined8 *)pcVar5)[(long)DAT_00616478], (ulong)(uStack52 | uStack68), uStack144, (ulong)(uStack52 | uStack68));
      if (iVar2 != 0)
	{
	  imperfection_wrapper ();	//      uVar3 = FUN_004042f8(0, 3, ((undefined8 *)pcVar5)[(long)DAT_00616478]);
	  puVar4 = (uint *) func_0x00401910 ();
	  imperfection_wrapper ();	//      FUN_00405128(1, (ulong)*puVar4, &DAT_0041338f, uVar3);
	}
    }
  else
    {
      if (cVar1 < 'd')
	{
	  if (cVar1 == 'b')
	    {
	      uStack68 = 0x6000;
	      goto LAB_00402353;
	    }
	}
      else
	{
	  if (cVar1 == 'p')
	    {
	      if (cStack85 != '\0')
		{
		  imperfection_wrapper ();	//          FUN_00401d72(((undefined8 *)pcVar5)[(long)DAT_00616478], 0x1000);
		}
	      imperfection_wrapper ();	//        iVar2 = FUN_004063ff(((undefined8 *)pcVar5)[(long)DAT_00616478], (ulong)uStack52, (ulong)uStack52);
	      if (iVar2 != 0)
		{
		  imperfection_wrapper ();	//          uVar3 = FUN_004042f8(0, 3, ((undefined8 *)pcVar5)[(long)DAT_00616478]);
		  puVar4 = (uint *) func_0x00401910 ();
		  imperfection_wrapper ();	//          FUN_00405128(1, (ulong)*puVar4, &DAT_0041338f, uVar3);
		}
	      goto LAB_00402656;
	    }
	  if (cVar1 == 'u')
	    goto LAB_0040234b;
	}
      imperfection_wrapper ();	//    uVar3 = FUN_004043e8(((undefined8 *)pcVar5)[(long)DAT_00616478 + 1]);
      imperfection_wrapper ();	//    FUN_00405128(0, 0, "invalid device type %s", uVar3);
      FUN_00401dad (1);
    }
LAB_00402656:
  ;
  if ((lStack64 != 0)
      && (iVar2 =
	  func_0x004017e0 (((undefined8 *) pcVar5)[(long) DAT_00616478],
			   (ulong) uStack52, (ulong) uStack52), iVar2 != 0))
    {
      imperfection_wrapper ();	//    uVar3 = FUN_004041f7(4, ((undefined8 *)pcVar5)[(long)DAT_00616478]);
      puVar4 = (uint *) func_0x00401910 ();
      imperfection_wrapper ();	//    FUN_00405128(1, (ulong)*puVar4, "cannot set permissions of %s", uVar3);
    }
  return 0;
}

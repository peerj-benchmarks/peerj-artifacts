
#include "uname.h"

long null_ARRAY_0061463_0_8_;
long null_ARRAY_0061463_8_8_;
long null_ARRAY_0061478_0_8_;
long null_ARRAY_0061478_16_8_;
long null_ARRAY_0061478_24_8_;
long null_ARRAY_0061478_32_8_;
long null_ARRAY_0061478_40_8_;
long null_ARRAY_0061478_48_8_;
long null_ARRAY_0061478_8_8_;
long null_ARRAY_006148c_0_4_;
long null_ARRAY_006148c_16_8_;
long null_ARRAY_006148c_4_4_;
long null_ARRAY_006148c_8_4_;
long DAT_00411640;
long DAT_004116fd;
long DAT_0041177b;
long DAT_00411e47;
long DAT_00411e90;
long DAT_00411fde;
long DAT_00411fe2;
long DAT_00411fe7;
long DAT_00411fe9;
long DAT_00412653;
long DAT_00412a20;
long DAT_00412a38;
long DAT_00412a3d;
long DAT_00412aa7;
long DAT_00412b38;
long DAT_00412b3b;
long DAT_00412b89;
long DAT_00412b8d;
long DAT_00412c00;
long DAT_00412c01;
long DAT_00412de8;
long DAT_00614230;
long DAT_00614240;
long DAT_00614250;
long DAT_00614600;
long DAT_00614610;
long DAT_00614620;
long DAT_00614698;
long DAT_0061469c;
long DAT_006146a0;
long DAT_006146c0;
long DAT_006146d0;
long DAT_006146e0;
long DAT_006146e8;
long DAT_00614700;
long DAT_00614708;
long DAT_00614750;
long DAT_00614758;
long DAT_00614760;
long DAT_00614768;
long DAT_006148f8;
long DAT_00614900;
long DAT_00614908;
long DAT_00614918;
long fde_00413870;
long FLOAT_UNKNOWN;
long null_ARRAY_00411840;
long null_ARRAY_00411a00;
long null_ARRAY_00413040;
long null_ARRAY_00614630;
long null_ARRAY_00614660;
long null_ARRAY_00614720;
long null_ARRAY_00614780;
long null_ARRAY_006147c0;
long null_ARRAY_006148c0;
long PTR_DAT_00614608;
long PTR_null_ARRAY_00614640;
void
FUN_00401bda (uint uParm1)
{
  ulong uVar1;

  if (uParm1 == 0)
    {
      func_0x004014a0 ("Usage: %s [OPTION]...\n", DAT_00614768);
      if (DAT_00614600 == 1)
	{
	  func_0x00401630
	    ("Print certain system information.  With no OPTION, same as -s.\n\n  -a, --all                print all information, in the following order,\n                             except omit -p and -i if unknown:\n  -s, --kernel-name        print the kernel name\n  -n, --nodename           print the network node hostname\n  -r, --kernel-release     print the kernel release\n",
	     DAT_006146c0);
	  func_0x00401630
	    ("  -v, --kernel-version     print the kernel version\n  -m, --machine            print the machine hardware name\n  -p, --processor          print the processor type (non-portable)\n  -i, --hardware-platform  print the hardware platform (non-portable)\n  -o, --operating-system   print the operating system\n",
	     DAT_006146c0);
	}
      else
	{
	  func_0x00401630 ("Print machine architecture.\n\n", DAT_006146c0);
	}
      func_0x00401630 ("      --help     display this help and exit\n",
		       DAT_006146c0);
      func_0x00401630
	("      --version  output version information and exit\n",
	 DAT_006146c0);
      imperfection_wrapper ();	//    FUN_00401a3e();
    }
  else
    {
      func_0x00401620 (DAT_006146e0,
		       "Try \'%s --help\' for more information.\n",
		       DAT_00614768);
    }
  uVar1 = (ulong) uParm1;
  func_0x004017b0 ();
  if (DAT_00614750 != '\0')
    {
      func_0x00401770 (0x20);
    }
  DAT_00614750 = 1;
  func_0x00401630 (uVar1, DAT_006146c0, DAT_006146c0);
  return;
}


#include "rmdir.h"

long null_ARRAY_0061541_0_8_;
long null_ARRAY_0061541_8_8_;
long null_ARRAY_0061558_0_8_;
long null_ARRAY_0061558_16_8_;
long null_ARRAY_0061558_24_8_;
long null_ARRAY_0061558_32_8_;
long null_ARRAY_0061558_40_8_;
long null_ARRAY_0061558_48_8_;
long null_ARRAY_0061558_8_8_;
long null_ARRAY_006156c_0_4_;
long null_ARRAY_006156c_16_8_;
long null_ARRAY_006156c_4_4_;
long null_ARRAY_006156c_8_4_;
long DAT_00412100;
long DAT_004121bd;
long DAT_0041223b;
long DAT_00412724;
long DAT_0041274b;
long DAT_00412766;
long DAT_004127b0;
long DAT_004128de;
long DAT_004128e2;
long DAT_004128e7;
long DAT_004128e9;
long DAT_00412f53;
long DAT_00413320;
long DAT_00413338;
long DAT_0041333d;
long DAT_004133a7;
long DAT_00413438;
long DAT_0041343b;
long DAT_00413489;
long DAT_0041348d;
long DAT_00413500;
long DAT_00413501;
long DAT_00615000;
long DAT_00615010;
long DAT_00615020;
long DAT_006153e8;
long DAT_00615400;
long DAT_00615478;
long DAT_0061547c;
long DAT_00615480;
long DAT_006154c0;
long DAT_006154d0;
long DAT_006154e0;
long DAT_006154e8;
long DAT_00615500;
long DAT_00615508;
long DAT_00615550;
long DAT_00615551;
long DAT_00615552;
long DAT_00615558;
long DAT_00615560;
long DAT_00615568;
long DAT_006156f8;
long DAT_00615700;
long DAT_00615708;
long DAT_00615710;
long DAT_00615718;
long DAT_00615728;
long fde_004141e8;
long FLOAT_UNKNOWN;
long null_ARRAY_004122c0;
long null_ARRAY_00413940;
long null_ARRAY_00615410;
long null_ARRAY_00615520;
long null_ARRAY_00615580;
long null_ARRAY_006155c0;
long null_ARRAY_006156c0;
long PTR_DAT_006153e0;
long PTR_null_ARRAY_00615420;
ulong
FUN_0040204e (uint uParm1)
{
  char cVar1;
  byte bVar2;
  int iVar3;
  undefined8 uVar4;
  uint *puVar5;
  undefined8 uVar6;
  char *pcVar7;
  bool bStack49;

  if (uParm1 == 0)
    {
      func_0x004015b0 ("Usage: %s [OPTION]... DIRECTORY...\n", DAT_00615568);
      func_0x00401770
	("Remove the DIRECTORY(ies), if they are empty.\n\n      --ignore-fail-on-non-empty\n                  ignore each failure that is solely because a directory\n                    is non-empty\n",
	 DAT_006154c0);
      func_0x00401770
	("  -p, --parents   remove DIRECTORY and its ancestors; e.g., \'rmdir -p a/b/c\' is\n                    similar to \'rmdir a/b/c a/b a\'\n  -v, --verbose   output a diagnostic for every directory processed\n",
	 DAT_006154c0);
      func_0x00401770 ("      --help     display this help and exit\n",
		       DAT_006154c0);
      pcVar7 = (char *) DAT_006154c0;
      func_0x00401770
	("      --version  output version information and exit\n");
      imperfection_wrapper ();	//    FUN_00401ce9();
    }
  else
    {
      pcVar7 = "Try \'%s --help\' for more information.\n";
      func_0x00401760 (DAT_006154e0,
		       "Try \'%s --help\' for more information.\n",
		       DAT_00615568);
    }
  func_0x004018e0 ();
  bStack49 = true;
  FUN_00402589 (*(undefined8 *) pcVar7);
  func_0x004018a0 (6, &DAT_0041223b);
  func_0x00401880 (FUN_0040243f);
  DAT_00615550 = '\0';
LAB_004021de:
  ;
  do
    {
      while (true)
	{
	  while (true)
	    {
	      imperfection_wrapper ();	//        iVar3 = FUN_004054db((ulong)uParm1, pcVar7, &DAT_00412724, null_ARRAY_004122c0, 0);
	      if (iVar3 == -1)
		{
		  if (DAT_00615478 == uParm1)
		    {
		      imperfection_wrapper ();	//            FUN_0040429f(0, 0, "missing operand");
		      FUN_0040204e (1);
		    }
		  while ((int) DAT_00615478 < (int) uParm1)
		    {
		      uVar6 =
			((undefined8 *) pcVar7)[(long) (int) DAT_00615478];
		      if (DAT_00615552 != '\0')
			{
			  imperfection_wrapper ();	//              uVar4 = FUN_00403983(4, uVar6);
			  imperfection_wrapper ();	//              FUN_00402356(DAT_006154c0, "removing directory, %s", uVar4);
			}
		      iVar3 = FUN_00405839 (uVar6);
		      if (iVar3 == 0)
			{
			  if (DAT_00615550 != '\0')
			    {
			      bVar2 = FUN_00401f2d (uVar6);
			      bStack49 = (bVar2 & bStack49) != 0;
			    }
			}
		      else
			{
			  puVar5 = (uint *) func_0x004018d0 ();
			  imperfection_wrapper ();	//              cVar1 = FUN_00401ed1((ulong)*puVar5, uVar6, uVar6);
			  if (cVar1 == '\0')
			    {
			      imperfection_wrapper ();	//                uVar6 = FUN_00403983(4, uVar6);
			      puVar5 = (uint *) func_0x004018d0 ();
			      imperfection_wrapper ();	//                FUN_0040429f(0, (ulong)*puVar5, "failed to remove %s", uVar6);
			      bStack49 = false;
			    }
			}
		      DAT_00615478 = DAT_00615478 + 1;
		    }
		  return (ulong) (bStack49 == false);
		}
	      if (iVar3 != 0x70)
		break;
	      DAT_00615550 = '\x01';
	    }
	  if (0x70 < iVar3)
	    break;
	  if (iVar3 != -0x83)
	    {
	      if (iVar3 != -0x82)
		goto LAB_004021d4;
	      FUN_0040204e (0);
	    }
	  imperfection_wrapper ();	//      FUN_00403fba(DAT_006154c0, "rmdir", "GNU coreutils", PTR_DAT_006153e0, "David MacKenzie", 0);
	  func_0x004018e0 (0);
	LAB_004021d4:
	  ;
	  imperfection_wrapper ();	//      FUN_0040204e();
	}
      if (iVar3 != 0x76)
	{
	  if (iVar3 != 0x80)
	    goto LAB_004021d4;
	  DAT_00615551 = 1;
	  goto LAB_004021de;
	}
      DAT_00615552 = '\x01';
    }
  while (true);
}

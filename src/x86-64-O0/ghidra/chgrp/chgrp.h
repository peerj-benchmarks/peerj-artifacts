typedef unsigned char   undefined;

typedef unsigned char    byte;
typedef unsigned char    dwfenc;
typedef unsigned int    dword;
typedef unsigned long    qword;
typedef unsigned char    undefined1;
typedef unsigned short    undefined2;
typedef unsigned int    undefined4;
typedef unsigned long    undefined6;
typedef unsigned long    undefined8;
typedef unsigned short    word;
#define unkbyte9   unsigned long long
#define unkbyte10   unsigned long long
#define unkbyte11   unsigned long long
#define unkbyte12   unsigned long long
#define unkbyte13   unsigned long long
#define unkbyte14   unsigned long long
#define unkbyte15   unsigned long long
#define unkbyte16   unsigned long long

#define unkuint9   unsigned long long
#define unkuint10   unsigned long long
#define unkuint11   unsigned long long
#define unkuint12   unsigned long long
#define unkuint13   unsigned long long
#define unkuint14   unsigned long long
#define unkuint15   unsigned long long
#define unkuint16   unsigned long long

#define unkint9   long long
#define unkint10   long long
#define unkint11   long long
#define unkint12   long long
#define unkint13   long long
#define unkint14   long long
#define unkint15   long long
#define unkint16   long long

#define unkfloat1   float
#define unkfloat2   float
#define unkfloat3   float
#define unkfloat5   double
#define unkfloat6   double
#define unkfloat7   double
#define unkfloat9   long double
#define unkfloat11   long double
#define unkfloat12   long double
#define unkfloat13   long double
#define unkfloat14   long double
#define unkfloat15   long double
#define unkfloat16   long double

#define BADSPACEBASE   void
#define code   void

#define ulong long
#define true 1
#define bool int
#define false 0
#define float10 int
#define ushort int
#define undefined7 long
#define undefined2 long
#define uint5 long
#define uint6 long

// Add definition for uint
typedef unsigned int    uint;

#define sbyte char

extern imperfection_wrapper(void);
typedef struct eh_frame_hdr eh_frame_hdr, *Peh_frame_hdr;

struct eh_frame_hdr {
    byte eh_frame_hdr_version; // Exception Handler Frame Header Version
    dwfenc eh_frame_pointer_encoding; // Exception Handler Frame Pointer Encoding
    dwfenc eh_frame_desc_entry_count_encoding; // Encoding of # of Exception Handler FDEs
    dwfenc eh_frame_table_encoding; // Exception Handler Table Encoding
};

typedef struct fde_table_entry fde_table_entry, *Pfde_table_entry;

struct fde_table_entry {
    dword initial_loc; // Initial Location
    dword data_loc; // Data location
};

typedef enum Elf64_DynTag {
    DT_AUDIT=1879047932,
    DT_AUXILIARY=2147483645,
    DT_BIND_NOW=24,
    DT_CHECKSUM=1879047672,
    DT_CONFIG=1879047930,
    DT_DEBUG=21,
    DT_DEPAUDIT=1879047931,
    DT_ENCODING=32,
    DT_FEATURE_1=1879047676,
    DT_FILTER=2147483647,
    DT_FINI=13,
    DT_FINI_ARRAY=26,
    DT_FINI_ARRAYSZ=28,
    DT_FLAGS=30,
    DT_FLAGS_1=1879048187,
    DT_GNU_CONFLICT=1879047928,
    DT_GNU_CONFLICTSZ=1879047670,
    DT_GNU_HASH=1879047925,
    DT_GNU_LIBLIST=1879047929,
    DT_GNU_LIBLISTSZ=1879047671,
    DT_GNU_PRELINKED=1879047669,
    DT_HASH=4,
    DT_INIT=12,
    DT_INIT_ARRAY=25,
    DT_INIT_ARRAYSZ=27,
    DT_JMPREL=23,
    DT_MOVEENT=1879047674,
    DT_MOVESZ=1879047675,
    DT_MOVETAB=1879047934,
    DT_NEEDED=1,
    DT_NULL=0,
    DT_PLTGOT=3,
    DT_PLTPAD=1879047933,
    DT_PLTPADSZ=1879047673,
    DT_PLTREL=20,
    DT_PLTRELSZ=2,
    DT_POSFLAG_1=1879047677,
    DT_PREINIT_ARRAYSZ=33,
    DT_REL=17,
    DT_RELA=7,
    DT_RELACOUNT=1879048185,
    DT_RELAENT=9,
    DT_RELASZ=8,
    DT_RELCOUNT=1879048186,
    DT_RELENT=19,
    DT_RELSZ=18,
    DT_RPATH=15,
    DT_RUNPATH=29,
    DT_SONAME=14,
    DT_STRSZ=10,
    DT_STRTAB=5,
    DT_SYMBOLIC=16,
    DT_SYMENT=11,
    DT_SYMINENT=1879047679,
    DT_SYMINFO=1879047935,
    DT_SYMINSZ=1879047678,
    DT_SYMTAB=6,
    DT_TEXTREL=22,
    DT_TLSDESC_GOT=1879047927,
    DT_TLSDESC_PLT=1879047926,
    DT_VERDEF=1879048188,
    DT_VERDEFNUM=1879048189,
    DT_VERNEED=1879048190,
    DT_VERNEEDNUM=1879048191,
    DT_VERSYM=1879048176
} Elf64_DynTag;

typedef enum Elf_ProgramHeaderType {
    PT_DYNAMIC=2,
    PT_GNU_EH_FRAME=1685382480,
    PT_GNU_RELRO=1685382482,
    PT_GNU_STACK=1685382481,
    PT_INTERP=3,
    PT_LOAD=1,
    PT_NOTE=4,
    PT_NULL=0,
    PT_PHDR=6,
    PT_SHLIB=5,
    PT_TLS=7
} Elf_ProgramHeaderType;

typedef struct Elf64_Dyn Elf64_Dyn, *PElf64_Dyn;

struct Elf64_Dyn {
    enum Elf64_DynTag d_tag;
    qword d_val;
};

typedef struct Elf64_Shdr Elf64_Shdr, *PElf64_Shdr;

typedef enum Elf_SectionHeaderType {
    SHT_CHECKSUM=1879048184,
    SHT_DYNAMIC=6,
    SHT_DYNSYM=11,
    SHT_FINI_ARRAY=15,
    SHT_GNU_ATTRIBUTES=1879048181,
    SHT_GNU_HASH=1879048182,
    SHT_GNU_LIBLIST=1879048183,
    SHT_GNU_verdef=1879048189,
    SHT_GNU_verneed=1879048190,
    SHT_GNU_versym=1879048191,
    SHT_GROUP=17,
    SHT_HASH=5,
    SHT_INIT_ARRAY=14,
    SHT_NOBITS=8,
    SHT_NOTE=7,
    SHT_NULL=0,
    SHT_PREINIT_ARRAY=16,
    SHT_PROGBITS=1,
    SHT_REL=9,
    SHT_RELA=4,
    SHT_SHLIB=10,
    SHT_STRTAB=3,
    SHT_SUNW_COMDAT=1879048187,
    SHT_SUNW_move=1879048186,
    SHT_SUNW_syminfo=1879048188,
    SHT_SYMTAB=2,
    SHT_SYMTAB_SHNDX=18
} Elf_SectionHeaderType;

struct Elf64_Shdr {
    dword sh_name;
    enum Elf_SectionHeaderType sh_type;
    qword sh_flags;
    qword sh_addr;
    qword sh_offset;
    qword sh_size;
    dword sh_link;
    dword sh_info;
    qword sh_addralign;
    qword sh_entsize;
};

typedef struct Elf64_Phdr Elf64_Phdr, *PElf64_Phdr;

struct Elf64_Phdr {
    enum Elf_ProgramHeaderType p_type;
    dword p_flags;
    qword p_offset;
    qword p_vaddr;
    qword p_paddr;
    qword p_filesz;
    qword p_memsz;
    qword p_align;
};

typedef struct Elf64_Rela Elf64_Rela, *PElf64_Rela;

struct Elf64_Rela {
    qword r_offset; // location to apply the relocation action
    qword r_info; // the symbol table index and the type of relocation
    qword r_addend; // a constant addend used to compute the relocatable field value
};

typedef struct Elf64_Sym Elf64_Sym, *PElf64_Sym;

struct Elf64_Sym {
    dword st_name;
    byte st_info;
    byte st_other;
    word st_shndx;
    qword st_value;
    qword st_size;
};

typedef struct Elf64_Ehdr Elf64_Ehdr, *PElf64_Ehdr;

struct Elf64_Ehdr {
    byte e_ident_magic_num;
    char e_ident_magic_str[3];
    byte e_ident_class;
    byte e_ident_data;
    byte e_ident_version;
    byte e_ident_pad[9];
    word e_type;
    word e_machine;
    dword e_version;
    qword e_entry;
    qword e_phoff;
    qword e_shoff;
    dword e_flags;
    word e_ehsize;
    word e_phentsize;
    word e_phnum;
    word e_shentsize;
    word e_shnum;
    word e_shstrndx;
};




undefined8 _DT_INIT(void);
void FUN_00401b90(void);
void thunk_FUN_0061f128(void);
void thunk_FUN_0061f1c0(void);
void FUN_00402180(void);
void thunk_FUN_0061f2f0(void);
void entry(void);
void FUN_004021e0(void);
void FUN_00402260(void);
void FUN_004022e0(void);
void FUN_0040231e(undefined *puParm1);
ulong FUN_004024ba(char *pcParm1);
ulong FUN_0040256d(uint uParm1);
ulong FUN_004026bf(uint uParm1,undefined8 *puParm2);
void FUN_00402b36(undefined4 *puParm1);
void FUN_00402b86(uint uParm1);
void FUN_00402bcb(uint uParm1);
undefined8 FUN_00402c10(long lParm1,long lParm2);
void FUN_00402ccd(undefined8 uParm1,int iParm2,undefined8 uParm3,undefined8 uParm4,long lParm5,long lParm6);
ulong FUN_00402e8c(uint param_1,undefined8 param_2,long *param_3,uint param_4,uint param_5,int param_6,int param_7);
ulong FUN_004030a6(long param_1,long param_2,uint param_3,uint param_4,uint param_5,uint param_6,int *param_7);
ulong FUN_004039c5(undefined8 param_1,uint param_2,uint param_3,uint param_4,uint param_5,uint param_6,int *param_7);
void FUN_00403b08(void);
void FUN_00403bed(uint uParm1,undefined8 uParm2,uint uParm3,uint uParm4);
void FUN_00403c1e(uint uParm1,undefined8 uParm2,uint uParm3,uint uParm4);
char * FUN_00403c4f(ulong uParm1,long lParm2);
void FUN_00403cd4(long lParm1);
ulong FUN_00403db1(undefined1 *puParm1,byte bParm2,uint uParm3);
undefined8 * FUN_00403e39(undefined8 *puParm1,int iParm2);
char * FUN_00403eb0(char *pcParm1,int iParm2);
ulong FUN_00403f50(long param_1,ulong param_2,long param_3,ulong param_4,uint param_5,uint param_6,long param_7,char *param_8,char *param_9);
undefined1 * FUN_00404e1a(int iParm1,undefined8 uParm2,undefined8 uParm3,uint *puParm4);
void FUN_0040508d(uint uParm1,uint uParm2,undefined8 uParm3);
void FUN_004050ce(uint uParm1,undefined8 uParm2);
void FUN_004050f2(undefined8 uParm1,undefined8 uParm2,char cParm3);
void FUN_00405186(undefined8 uParm1,char cParm2);
void FUN_004051b0(undefined8 uParm1);
void FUN_004051cf(uint uParm1,uint uParm2,undefined8 uParm3);
void FUN_0040526a(uint uParm1,undefined8 uParm2,undefined8 uParm3);
void FUN_00405296(uint uParm1,undefined8 uParm2);
void FUN_004052bf(undefined8 uParm1);
undefined8 * FUN_004052de(undefined8 *puParm1);
void FUN_0040533b(undefined8 uParm1,long lParm2,undefined8 uParm3,undefined8 uParm4,undefined8 *puParm5,undefined8 uParm6);
void FUN_0040579f(undefined8 uParm1,undefined8 uParm2,undefined8 uParm3,undefined8 uParm4,uint *puParm5);
void FUN_00405871(undefined4 uParm1,undefined4 uParm2,undefined4 uParm3,undefined4 uParm4,undefined4 uParm5,undefined4 uParm6,undefined4 uParm7,undefined4 uParm8,undefined8 param_1,undefined8 param_2,undefined8 param_3,undefined8 param_4,undefined8 param_5,undefined8 param_6);
void FUN_00405927(undefined8 uParm1);
long FUN_00405941(long lParm1);
long FUN_00405976(long lParm1,long lParm2);
void FUN_004059d7(undefined8 uParm1,undefined8 uParm2);
void FUN_00405a0b(undefined8 uParm1);
void FUN_00405a38(void);
long FUN_00405a62(undefined8 uParm1,uint uParm2,undefined8 uParm3);
ulong FUN_00405acd(long lParm1,long lParm2);
undefined8 FUN_00405b2f(int iParm1);
ulong FUN_00405b55(ulong *puParm1,int iParm2);
ulong FUN_00405bb4(undefined8 uParm1,uint uParm2,int iParm3);
ulong FUN_00405bf5(byte *pbParm1,byte **ppbParm2,uint uParm3,undefined8 *puParm4,long lParm5);
undefined8 FUN_00405fd6(ulong uParm1,ulong uParm2);
ulong FUN_00406055(uint uParm1);
void FUN_0040607e(void);
void FUN_004060b2(uint uParm1);
void FUN_00406126(uint uParm1,uint uParm2,undefined8 uParm3,undefined8 uParm4);
void FUN_004061aa(undefined4 uParm1,undefined4 uParm2,undefined4 uParm3,undefined4 uParm4,undefined4 uParm5,undefined4 uParm6,undefined4 uParm7,undefined4 uParm8,uint param_1,uint param_2,undefined8 param_3,undefined8 param_4,undefined8 param_5,undefined8 param_6);
ulong FUN_0040628e(uint uParm1,char *pcParm2,uint uParm3,uint uParm4,uint uParm5);
void FUN_00406553(uint uParm1,undefined8 uParm2,undefined8 uParm3,uint uParm4);
void FUN_00406580(uint uParm1,undefined8 uParm2,undefined8 uParm3,uint uParm4);
ulong FUN_004065bd(uint uParm1,long lParm2,long lParm3,uint uParm4);
ulong FUN_004066d0(long *plParm1,long *plParm2);
ulong FUN_00406725(long lParm1,ulong uParm2);
undefined8 FUN_0040674f(long lParm1);
undefined8 FUN_004067eb(long lParm1,undefined8 *puParm2);
void FUN_004068fb(long lParm1,long lParm2);
void FUN_00406a08(long lParm1);
void FUN_00406a55(undefined8 uParm1);
void FUN_00406a97(long lParm1,char cParm2);
long FUN_00406ada(uint uParm1,undefined8 uParm2,uint uParm3,uint *puParm4);
void FUN_00406b6b(long lParm1,uint uParm2,char cParm3);
ulong FUN_00406bf8(long lParm1);
ulong FUN_00406ca2(long lParm1,undefined8 uParm2);
long * FUN_00406d3a(long *plParm1,uint uParm2,long lParm3);
void FUN_004070fc(long lParm1,long lParm2);
undefined8 FUN_004071b6(long *plParm1);
ulong FUN_00407342(ulong *puParm1,ulong uParm2);
ulong FUN_00407373(ulong *puParm1,ulong *puParm2);
undefined8 FUN_004073a5(long lParm1);
undefined8 FUN_0040751f(undefined8 uParm1);
undefined8 FUN_00407555(undefined8 uParm1);
long FUN_004075b9(long *plParm1);
undefined8 FUN_00407c9d(undefined8 uParm1,long lParm2,int iParm3);
ulong FUN_00407cf4(long *plParm1,long *plParm2);
void FUN_00407d4f(long lParm1,undefined4 uParm2);
long FUN_00407dbf(long *plParm1,int iParm2);
undefined8 FUN_00408709(long lParm1,long lParm2,char cParm3);
long FUN_004088e8(long lParm1,long lParm2,ulong uParm3);
long FUN_00408a2f(long lParm1,undefined8 uParm2,long lParm3);
void FUN_00408ae1(long lParm1);
undefined8 FUN_00408b1c(long lParm1,long lParm2);
void FUN_00408be9(long lParm1,long lParm2);
long FUN_00408cfc(long *plParm1);
ulong FUN_00408d52(long lParm1,long lParm2,uint uParm3,long lParm4);
void FUN_00408f88(long lParm1,int *piParm2);
ulong FUN_0040916c(int param_1,undefined8 *param_2,char *param_3,long *param_4,int *param_5,int param_6,int *param_7,int param_8,undefined8 param_9);
char * FUN_00409756(undefined8 uParm1,undefined8 uParm2,char *pcParm3,int *piParm4,int iParm5);
ulong FUN_00409824(uint param_1,undefined8 *param_2,char *param_3,long param_4,undefined8 param_5,uint param_6,uint *param_7,uint param_8);
ulong FUN_00409fed(uint param_1,undefined8 param_2,undefined8 param_3,undefined8 param_4,undefined8 param_5,uint param_6,uint param_7);
void FUN_0040a07d(uint uParm1,undefined8 uParm2,undefined8 uParm3,undefined8 uParm4,undefined8 uParm5);
ulong FUN_0040a0c7(long lParm1,uint uParm2,uint uParm3);
void FUN_0040a234(undefined8 uParm1,undefined8 uParm2);
ulong FUN_0040a259(long lParm1,long lParm2);
undefined8 FUN_0040a310(long lParm1);
ulong FUN_0040a341(uint *puParm1,byte *pbParm2,long lParm3,undefined8 uParm4);
void FUN_0040a3c4(uint uParm1,undefined8 uParm2,uint uParm3,uint uParm4);
ulong FUN_0040a3f4(undefined4 uParm1,undefined4 uParm2,undefined4 uParm3,undefined4 uParm4,undefined4 uParm5,undefined4 uParm6,undefined4 uParm7,undefined4 uParm8,uint param_1,undefined8 param_2,uint param_3,undefined8 param_4,undefined8 param_5,undefined8 param_6);
undefined * FUN_0040a5bb(undefined *puParm1,uint uParm2,char *pcParm3);
undefined8 FUN_0040a6f9(long lParm1,long lParm2);
void FUN_0040a762(undefined8 uParm1,undefined8 uParm2);
ulong FUN_0040a787(long lParm1,long lParm2);
ulong FUN_0040a817(undefined8 uParm1,undefined8 uParm2,undefined8 uParm3);
ulong FUN_0040a938(byte *pbParm1,byte *pbParm2);
undefined8 FUN_0040a9af(uint uParm1,char cParm2);
undefined8 FUN_0040aa2a(undefined8 uParm1);
ulong FUN_0040aab5(ulong uParm1);
void FUN_0040aad1(long lParm1);
undefined8 FUN_0040aaf2(long *plParm1,long *plParm2);
void FUN_0040abc9(undefined4 uParm1,undefined4 uParm2,undefined4 uParm3,undefined4 uParm4,undefined4 uParm5,undefined4 uParm6,undefined4 uParm7,undefined4 uParm8,undefined8 param_1,uint param_2,undefined8 param_3,undefined8 param_4,undefined8 param_5,undefined8 param_6);
undefined8 FUN_0040acd8(void);
ulong FUN_0040ace5(uint uParm1);
long FUN_0040adb6(long *plParm1,undefined8 uParm2);
long FUN_0040ae0d(long lParm1,long lParm2);
ulong FUN_0040aea0(ulong uParm1);
ulong FUN_0040af0c(ulong uParm1);
ulong FUN_0040af53(undefined8 uParm1,ulong uParm2);
ulong FUN_0040af8a(ulong uParm1,ulong uParm2);
undefined8 FUN_0040afa3(long lParm1);
ulong FUN_0040b09c(ulong uParm1,long lParm2);
long * FUN_0040b192(undefined8 uParm1,undefined1 *puParm2,code *pcParm3,code *pcParm4,long lParm5);
void FUN_0040b2fa(long **pplParm1,undefined8 uParm2);
long FUN_0040b424(long lParm1);
void FUN_0040b46f(long lParm1,undefined8 *puParm2);
long FUN_0040b4a4(long lParm1,long lParm2,long **pplParm3,char cParm4);
undefined8 FUN_0040b639(long lParm1,long **pplParm2,char cParm3);
undefined8 FUN_0040b807(long *plParm1,undefined8 uParm2);
undefined8 FUN_0040ba0b(long lParm1,long lParm2,long *plParm3);
undefined8 FUN_0040bd5d(undefined8 uParm1,undefined8 uParm2);
long FUN_0040bda6(long lParm1,undefined8 uParm2);
void FUN_0040c085(long lParm1,undefined4 uParm2);
ulong FUN_0040c0dd(long lParm1);
ulong FUN_0040c0ef(long lParm1,undefined4 uParm2);
ulong FUN_0040c177(long lParm1);
undefined1 * FUN_0040c1f9(void);
char * FUN_0040c5ac(void);
void FUN_0040c67a(uint uParm1);
void FUN_0040c6a6(uint uParm1);
void FUN_0040c6d2(undefined4 uParm1,undefined4 uParm2,undefined4 uParm3,undefined4 uParm4,undefined4 uParm5,undefined4 uParm6,undefined4 uParm7,undefined4 uParm8,uint param_1,undefined8 param_2,uint param_3,undefined8 param_4,undefined8 param_5,undefined8 param_6);
undefined8 FUN_0040c7e3(int *piParm1);
void FUN_0040c85d(uint *puParm1);
void FUN_0040c894(uint *puParm1);
ulong FUN_0040c8c9(uint uParm1);
ulong FUN_0040c8d9(uint uParm1);
void FUN_0040c929(undefined4 *puParm1);
void FUN_0040c93d(uint *puParm1);
void FUN_0040c958(uint *puParm1);
undefined8 FUN_0040c9ac(uint *puParm1,undefined8 uParm2);
long FUN_0040ca06(long lParm1);
ulong FUN_0040ca34(char *pcParm1);
ulong FUN_0040cd3b(long lParm1,uint uParm2,uint uParm3);
ulong FUN_0040cee4(undefined8 uParm1);
ulong FUN_0040cf9c(undefined4 uParm1,undefined4 uParm2,undefined4 uParm3,undefined4 uParm4,undefined4 uParm5,undefined4 uParm6,undefined4 uParm7,undefined4 uParm8,uint param_1,uint param_2,undefined8 param_3,undefined8 param_4,undefined8 param_5,undefined8 param_6);
undefined8 FUN_0040d253(uint uParm1);
void FUN_0040d2b6(undefined8 uParm1);
void FUN_0040d2da(void);
ulong FUN_0040d2e8(long lParm1);
undefined8 FUN_0040d3b8(undefined8 uParm1);
void FUN_0040d3d7(undefined8 uParm1,undefined8 uParm2,uint uParm3);
long FUN_0040d402(long lParm1,undefined *puParm2);
ulong * FUN_0040d98b(ulong *puParm1,char cParm2,ulong uParm3);
ulong FUN_0040da9d(void);
long FUN_0040daea(ulong uParm1,long lParm2,ulong uParm3,long lParm4,long *plParm5);
int * FUN_0040dd36(ulong uParm1,uint *puParm2,ulong uParm3,uint *puParm4,ulong *puParm5);
char * FUN_0040e816(ulong uParm1,long lParm2,long lParm3);
long FUN_0040ea84(int *param_1,long *param_2);
long FUN_0040ec6f(undefined auParm1 [16],int *piParm2,long *plParm3);
undefined8 FUN_0040f02e(int iParm1,long lParm2,uint *puParm3,long lParm4,uint uParm5);
void FUN_0040f841(uint param_1);
void FUN_0040f88b(undefined8 uParm1,uint uParm2);
ulong FUN_0040f8dd(void);
ulong FUN_0040fc6f(undefined auParm1 [16],undefined8 uParm2,undefined8 uParm3);
ulong FUN_00410047(char *pcParm1,long lParm2);
undefined8 FUN_004100a0(long param_1,long param_2,undefined param_3,int param_4,undefined8 param_5,ulong param_6,int param_7,ulong param_8,int param_9);
ulong FUN_004177cd(ulong uParm1,undefined4 uParm2);
ulong FUN_004177e9(uint uParm1);
void FUN_00417808(undefined8 uParm1,int *piParm2,undefined8 uParm3);
void FUN_004178a9(int *param_1);
void FUN_00417968(uint uParm1);
ulong FUN_0041798e(ulong uParm1,long lParm2);
void FUN_004179c2(undefined8 uParm1,undefined8 uParm2,undefined8 uParm3);
void FUN_004179fd(undefined8 uParm1,undefined8 uParm2,undefined8 uParm3,undefined8 uParm4);
ulong FUN_00417a4e(ulong uParm1,ulong uParm2);
undefined8 FUN_00417a69(uint *puParm1,ulong *puParm2);
undefined8 FUN_00418209(char *pcParm1,ulong *puParm2,char **ppcParm3);
ulong FUN_004194bf(undefined auParm1 [16]);
ulong FUN_004194fa(void);
void FUN_00419530(void);
undefined8 _DT_FINI(void);
undefined FUN_0061f000();
undefined FUN_0061f008();
undefined FUN_0061f010();
undefined FUN_0061f018();
undefined FUN_0061f020();
undefined FUN_0061f028();
undefined FUN_0061f030();
undefined FUN_0061f038();
undefined FUN_0061f040();
undefined FUN_0061f048();
undefined FUN_0061f050();
undefined FUN_0061f058();
undefined FUN_0061f060();
undefined FUN_0061f068();
undefined FUN_0061f070();
undefined FUN_0061f078();
undefined FUN_0061f080();
undefined FUN_0061f088();
undefined FUN_0061f090();
undefined FUN_0061f098();
undefined FUN_0061f0a0();
undefined FUN_0061f0a8();
undefined FUN_0061f0b0();
undefined FUN_0061f0b8();
undefined FUN_0061f0c0();
undefined FUN_0061f0c8();
undefined FUN_0061f0d0();
undefined FUN_0061f0d8();
undefined FUN_0061f0e0();
undefined FUN_0061f0e8();
undefined FUN_0061f0f0();
undefined FUN_0061f0f8();
undefined FUN_0061f100();
undefined FUN_0061f108();
undefined FUN_0061f110();
undefined FUN_0061f118();
undefined FUN_0061f120();
undefined FUN_0061f128();
undefined FUN_0061f130();
undefined FUN_0061f138();
undefined FUN_0061f140();
undefined FUN_0061f148();
undefined FUN_0061f150();
undefined FUN_0061f158();
undefined FUN_0061f160();
undefined FUN_0061f168();
undefined FUN_0061f170();
undefined FUN_0061f178();
undefined FUN_0061f180();
undefined FUN_0061f188();
undefined FUN_0061f190();
undefined FUN_0061f198();
undefined FUN_0061f1a0();
undefined FUN_0061f1a8();
undefined FUN_0061f1b0();
undefined FUN_0061f1b8();
undefined FUN_0061f1c0();
undefined FUN_0061f1c8();
undefined FUN_0061f1d0();
undefined FUN_0061f1d8();
undefined FUN_0061f1e0();
undefined FUN_0061f1e8();
undefined FUN_0061f1f0();
undefined FUN_0061f1f8();
undefined FUN_0061f200();
undefined FUN_0061f208();
undefined FUN_0061f210();
undefined FUN_0061f218();
undefined FUN_0061f220();
undefined FUN_0061f228();
undefined FUN_0061f230();
undefined FUN_0061f238();
undefined FUN_0061f240();
undefined FUN_0061f248();
undefined FUN_0061f250();
undefined FUN_0061f258();
undefined FUN_0061f260();
undefined FUN_0061f268();
undefined FUN_0061f270();
undefined FUN_0061f278();
undefined FUN_0061f280();
undefined FUN_0061f288();
undefined FUN_0061f290();
undefined FUN_0061f298();
undefined FUN_0061f2a0();
undefined FUN_0061f2a8();
undefined FUN_0061f2b0();
undefined FUN_0061f2b8();
undefined FUN_0061f2c0();
undefined FUN_0061f2c8();
undefined FUN_0061f2d0();
undefined FUN_0061f2d8();
undefined FUN_0061f2e0();
undefined FUN_0061f2e8();
undefined FUN_0061f2f0();


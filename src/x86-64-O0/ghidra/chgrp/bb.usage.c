
#include "chgrp.h"

long null_ARRAY_0061e71_0_8_;
long null_ARRAY_0061e71_8_8_;
long null_ARRAY_0061e8c_0_8_;
long null_ARRAY_0061e8c_16_8_;
long null_ARRAY_0061e8c_24_8_;
long null_ARRAY_0061e8c_32_8_;
long null_ARRAY_0061e8c_40_8_;
long null_ARRAY_0061e8c_48_8_;
long null_ARRAY_0061e8c_8_8_;
long null_ARRAY_0061ea0_0_4_;
long null_ARRAY_0061ea0_16_8_;
long null_ARRAY_0061ea0_4_4_;
long null_ARRAY_0061ea0_8_4_;
long DAT_00419580;
long DAT_0041963d;
long DAT_004196bb;
long DAT_0041a0ff;
long DAT_0041a108;
long DAT_0041a2e8;
long DAT_0041a3a2;
long DAT_0041a589;
long DAT_0041a5d0;
long DAT_0041a71e;
long DAT_0041a722;
long DAT_0041a727;
long DAT_0041a729;
long DAT_0041ad80;
long DAT_0041ad9b;
long DAT_0041b160;
long DAT_0041b50e;
long DAT_0041b513;
long DAT_0041b528;
long DAT_0041b529;
long DAT_0041b52b;
long DAT_0041b5ef;
long DAT_0041b680;
long DAT_0041b683;
long DAT_0041b6d1;
long DAT_0041b742;
long DAT_0041b868;
long DAT_0041b869;
long DAT_0041b8de;
long DAT_0041b8fe;
long DAT_0041b956;
long DAT_0041b958;
long DAT_0041b95a;
long DAT_0061e228;
long DAT_0061e238;
long DAT_0061e248;
long DAT_0061e6e8;
long DAT_0061e700;
long DAT_0061e778;
long DAT_0061e77c;
long DAT_0061e780;
long DAT_0061e7c0;
long DAT_0061e7d0;
long DAT_0061e7e0;
long DAT_0061e7e8;
long DAT_0061e800;
long DAT_0061e808;
long DAT_0061e850;
long DAT_0061e870;
long DAT_0061e878;
long DAT_0061e880;
long DAT_0061ea38;
long DAT_0061ea3c;
long DAT_0061ea40;
long DAT_0061ea48;
long DAT_0061ea50;
long DAT_0061ea58;
long DAT_0061ea68;
long fde_0041c958;
long FLOAT_UNKNOWN;
long null_ARRAY_00419780;
long null_ARRAY_0041b750;
long null_ARRAY_0041bd80;
long null_ARRAY_0061e710;
long null_ARRAY_0061e740;
long null_ARRAY_0061e820;
long null_ARRAY_0061e860;
long null_ARRAY_0061e8c0;
long null_ARRAY_0061e900;
long null_ARRAY_0061ea00;
long PTR_DAT_0061e6e0;
long PTR_null_ARRAY_0061e720;
long stack0x00000008;
ulong
FUN_0040256d (uint uParm1)
{
  char cVar1;
  int iVar2;
  uint *puVar3;
  undefined8 uVar4;
  char *pcVar5;
  uint uVar6;
  undefined8 uVar7;
  undefined auStack272[32];
  uint uStack240;
  undefined4 uStack128;
  char cStack124;
  long lStack120;
  bool bStack112;
  undefined uStack111;
  char *pcStack96;
  char *pcStack80;
  int iStack68;
  int iStack64;
  uint uStack60;
  uint uStack56;
  char cStack49;
  code *pcStack32;
  uint local_c;

  if (uParm1 == 0)
    {
      pcStack32 = (code *) 0x4025c3;
      local_c = uParm1;
      func_0x00401bd0
	("Usage: %s [OPTION]... GROUP FILE...\n  or:  %s [OPTION]... --reference=RFILE FILE...\n",
	 DAT_0061e880, DAT_0061e880);
      pcStack32 = (code *) 0x4025d7;
      func_0x00401e50
	("Change the group of each FILE to GROUP.\nWith --reference, change the group of each FILE to that of RFILE.\n\n",
	 DAT_0061e7c0);
      pcStack32 = (code *) 0x4025eb;
      func_0x00401e50
	("  -c, --changes          like verbose but report only when a change is made\n  -f, --silent, --quiet  suppress most error messages\n  -v, --verbose          output a diagnostic for every file processed\n",
	 DAT_0061e7c0);
      pcStack32 = (code *) 0x4025ff;
      func_0x00401e50
	("      --dereference      affect the referent of each symbolic link (this is\n                         the default), rather than the symbolic link itself\n  -h, --no-dereference   affect symbolic links instead of any referenced file\n",
	 DAT_0061e7c0);
      pcStack32 = (code *) 0x402613;
      func_0x00401e50
	("                         (useful only on systems that can change the\n                         ownership of a symlink)\n",
	 DAT_0061e7c0);
      pcStack32 = (code *) 0x402627;
      func_0x00401e50
	("      --no-preserve-root  do not treat \'/\' specially (the default)\n      --preserve-root    fail to operate recursively on \'/\'\n",
	 DAT_0061e7c0);
      pcStack32 = (code *) 0x40263b;
      func_0x00401e50
	("      --reference=RFILE  use RFILE\'s group rather than specifying a\n                         GROUP value\n",
	 DAT_0061e7c0);
      pcStack32 = (code *) 0x40264f;
      func_0x00401e50
	("  -R, --recursive        operate on files and directories recursively\n",
	 DAT_0061e7c0);
      pcStack32 = (code *) 0x402663;
      func_0x00401e50
	("\nThe following options modify how a hierarchy is traversed when the -R\noption is also specified.  If more than one is specified, only the final\none takes effect.\n\n  -H                     if a command line argument is a symbolic link\n                         to a directory, traverse it\n  -L                     traverse every symbolic link to a directory\n                         encountered\n  -P                     do not traverse any symbolic links (default)\n\n",
	 DAT_0061e7c0);
      pcStack32 = (code *) 0x402677;
      func_0x00401e50 ("      --help     display this help and exit\n",
		       DAT_0061e7c0);
      pcStack32 = (code *) 0x40268b;
      func_0x00401e50
	("      --version  output version information and exit\n",
	 DAT_0061e7c0);
      pcStack32 = (code *) 0x4026ab;
      pcVar5 = (char *) DAT_0061e880;
      func_0x00401bd0
	("\nExamples:\n  %s staff /u      Change the group of /u to \"staff\".\n  %s -hR staff /u  Change the group of /u and subfiles to \"staff\".\n",
	 DAT_0061e880, DAT_0061e880);
      pcStack32 = (code *) 0x4026b5;
      imperfection_wrapper ();	//    FUN_0040231e();
    }
  else
    {
      pcVar5 = "Try \'%s --help\' for more information.\n";
      pcStack32 = (code *) 0x40259e;
      local_c = uParm1;
      func_0x00401e40 (DAT_0061e7e0,
		       "Try \'%s --help\' for more information.\n",
		       DAT_0061e880);
    }
  pcStack32 = FUN_004026bf;
  uVar6 = local_c;
  func_0x00402050 ();
  cStack49 = '\0';
  uStack60 = 0x10;
  iStack64 = -1;
  pcStack32 = (code *) & stack0xfffffffffffffff8;
  FUN_00403cd4 (*(undefined8 *) pcVar5);
  func_0x00401ff0 (6, &DAT_004196bb);
  func_0x00401fd0 (FUN_00403b08);
  FUN_00402b36 (&uStack128);
LAB_00402890:
  ;
  while (true)
    {
      uVar7 = 0x4028b4;
      imperfection_wrapper ();	//    iStack68 = FUN_0040a07d((ulong)uVar6, pcVar5, "HLPRcfhv", null_ARRAY_00419780, 0);
      if (iStack68 == -1)
	{
	  if (cStack124 == '\0')
	    {
	      uStack60 = 0x10;
	    }
	  else
	    {
	      if (uStack60 == 0x10)
		{
		  if (iStack64 == 1)
		    {
		      imperfection_wrapper ();	//            FUN_004061aa(1, 0, "-R --dereference requires either -H or -L");
		    }
		  iStack64 = 0;
		}
	    }
	  bStack112 = iStack64 != 0;
	  if (DAT_0061e850 == 0)
	    {
	      iVar2 = 2;
	    }
	  else
	    {
	      iVar2 = 1;
	    }
	  if ((int) (uVar6 - DAT_0061e778) < iVar2)
	    {
	      if (DAT_0061e778 < (int) uVar6)
		{
		  imperfection_wrapper ();	//          uVar7 = FUN_004052bf(((undefined8 *)pcVar5)[(long)(int)uVar6 + -1]);
		  imperfection_wrapper ();	//          FUN_004061aa(0, 0, "missing operand after %s", uVar7);
		}
	      else
		{
		  imperfection_wrapper ();	//          FUN_004061aa(0, 0, "missing operand");
		}
	      FUN_0040256d (1);
	    }
	  if (DAT_0061e850 == 0)
	    {
	      pcStack80 =
		(char *) ((undefined8 *) pcVar5)[(long) DAT_0061e778];
	      pcStack96 = pcStack80;
	      if (*pcStack80 == '\0')
		{
		  pcStack96 = (char *) 0x0;
		}
	      uVar7 = 0x402a72;
	      DAT_0061e778 = DAT_0061e778 + 1;
	      uStack56 = FUN_004024ba (pcStack80);
	    }
	  else
	    {
	      imperfection_wrapper ();	//        iVar2 = FUN_0040a787(DAT_0061e850, auStack272, auStack272);
	      if (iVar2 != 0)
		{
		  imperfection_wrapper ();	//          uVar7 = FUN_004050ce(4, DAT_0061e850);
		  puVar3 = (uint *) func_0x00402040 ();
		  imperfection_wrapper ();	//          FUN_004061aa(1, (ulong)*puVar3, "failed to get attributes of %s", uVar7);
		}
	      uStack56 = uStack240;
	      uVar7 = 0x402a1c;
	      imperfection_wrapper ();	//        pcStack96 = (char *)FUN_00402b86((ulong)uStack240);
	    }
	  if ((cStack124 != '\0') && (cStack49 != '\0'))
	    {
	      uVar7 = 0x402a8d;
	      lStack120 = FUN_004052de (null_ARRAY_0061e860);
	      if (lStack120 == 0)
		{
		  imperfection_wrapper ();	//          uVar4 = FUN_004050ce(4, &DAT_0041a0ff);
		  puVar3 = (uint *) func_0x00402040 ();
		  uVar7 = 0x402acc;
		  imperfection_wrapper ();	//          FUN_004061aa(1, (ulong)*puVar3, "failed to get attributes of %s", uVar4);
		}
	    }
	  uStack60 = uStack60 | 0x400;
	  imperfection_wrapper ();	//      cVar1 = FUN_004039c5((undefined8 *)pcVar5 + (long)DAT_0061e778, (ulong)uStack60, 0xffffffff, (ulong)uStack56, 0xffffffff, 0xffffffff, &uStack128, uVar7);
	  return (ulong) (cVar1 == '\0');
	}
      if (iStack68 != 99)
	break;
      uStack128 = 1;
    }
  if (iStack68 < 100)
    {
      if (iStack68 == 0x48)
	{
	  uStack60 = 0x11;
	  goto LAB_00402890;
	}
      if (iStack68 < 0x49)
	{
	  if (iStack68 != -0x83)
	    {
	      if (iStack68 != -0x82)
		goto LAB_00402886;
	      uVar7 = 0x40283e;
	      FUN_0040256d (0);
	    }
	  imperfection_wrapper ();	//      FUN_00405871(DAT_0061e7c0, "chgrp", "GNU coreutils", PTR_DAT_0061e6e0, "David MacKenzie", "Jim Meyering", 0, uVar7);
	  func_0x00402050 (0);
	}
      else
	{
	  if (iStack68 == 0x50)
	    {
	      uStack60 = 0x10;
	      goto LAB_00402890;
	    }
	  if (iStack68 == 0x52)
	    {
	      cStack124 = '\x01';
	      goto LAB_00402890;
	    }
	  if (iStack68 == 0x4c)
	    {
	      uStack60 = 2;
	      goto LAB_00402890;
	    }
	}
    }
  else
    {
      if (iStack68 == 0x80)
	{
	  iStack64 = 1;
	  goto LAB_00402890;
	}
      if (iStack68 < 0x81)
	{
	  if (iStack68 == 0x68)
	    {
	      iStack64 = 0;
	    }
	  else
	    {
	      if (iStack68 == 0x76)
		{
		  uStack128 = 0;
		}
	      else
		{
		  if (iStack68 != 0x66)
		    goto LAB_00402886;
		  uStack111 = 1;
		}
	    }
	  goto LAB_00402890;
	}
      if (iStack68 == 0x82)
	{
	  cStack49 = '\x01';
	  goto LAB_00402890;
	}
      if (iStack68 < 0x82)
	{
	  cStack49 = '\0';
	  goto LAB_00402890;
	}
      if (iStack68 == 0x83)
	{
	  DAT_0061e850 = DAT_0061ea68;
	  goto LAB_00402890;
	}
    }
LAB_00402886:
  ;
  imperfection_wrapper ();	//  FUN_0040256d();
  goto LAB_00402890;
}

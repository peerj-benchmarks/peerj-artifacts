
#include "timeout.h"

long null_ARRAY_006164f_0_8_;
long null_ARRAY_006164f_8_8_;
long null_ARRAY_0061684_0_8_;
long null_ARRAY_0061684_16_8_;
long null_ARRAY_0061684_24_8_;
long null_ARRAY_0061684_32_8_;
long null_ARRAY_0061684_40_8_;
long null_ARRAY_0061684_48_8_;
long null_ARRAY_0061684_8_8_;
long null_ARRAY_0061698_0_4_;
long null_ARRAY_0061698_16_8_;
long null_ARRAY_0061698_4_4_;
long null_ARRAY_0061698_8_4_;
long _cVar12;
long DAT_00412dcb;
long DAT_00412e85;
long DAT_00412f03;
long DAT_004130ad;
long DAT_0041388b;
long DAT_004138a0;
long DAT_00413900;
long DAT_00413a5e;
long DAT_00413a62;
long DAT_00413a67;
long DAT_00413a69;
long DAT_004140d3;
long DAT_004144f0;
long DAT_00414580;
long DAT_004145b8;
long DAT_004145bb;
long DAT_00414609;
long DAT_00414613;
long DAT_00414619;
long DAT_004147e8;
long DAT_00414c30;
long DAT_00414ca0;
long DAT_00414ca1;
long DAT_00616060;
long DAT_00616070;
long DAT_00616080;
long DAT_006164c0;
long DAT_006164d0;
long DAT_006164e0;
long DAT_00616558;
long DAT_0061655c;
long DAT_00616560;
long DAT_00616580;
long DAT_00616740;
long DAT_00616750;
long DAT_00616760;
long DAT_00616768;
long DAT_00616780;
long DAT_00616788;
long DAT_006167d0;
long DAT_006167d4;
long DAT_006167d8;
long DAT_006167e0;
long DAT_006167e1;
long DAT_006167e2;
long DAT_006167e8;
long DAT_006167f0;
long DAT_006167f8;
long DAT_00616800;
long DAT_00616808;
long DAT_006169b8;
long DAT_006169c0;
long DAT_006169c8;
long DAT_006169d8;
long DAT_25686602;
long DAT_6f618058;
long fde_004153b8;
long FLOAT_UNKNOWN;
long null_ARRAY_00412f80;
long null_ARRAY_00414a40;
long null_ARRAY_006164f0;
long null_ARRAY_00616520;
long null_ARRAY_006167a0;
long null_ARRAY_00616840;
long null_ARRAY_00616880;
long null_ARRAY_00616980;
long PTR_DAT_006164c8;
long PTR_null_ARRAY_00616500;
long register0x00000020;
long stack0x00000008;
undefined8
FUN_00402530 (uint uParm1)
{
  char cVar1;
  double *pdVar2;
  int iStack36;

  if (uParm1 == 0)
    {
      func_0x00401980
	("Usage: %s [OPTION] DURATION COMMAND [ARG]...\n  or:  %s [OPTION]\n",
	 DAT_00616808, DAT_00616808);
      func_0x00401bc0
	("Start COMMAND, and kill it if still running after DURATION.\n",
	 DAT_00616740);
      FUN_00402066 ();
      func_0x00401bc0
	("      --preserve-status\n                 exit with the same status as COMMAND, even when the\n                   command times out\n      --foreground\n                 when not running timeout directly from a shell prompt,\n                   allow COMMAND to read from the TTY and get TTY signals;\n                   in this mode, children of COMMAND will not be timed out\n  -k, --kill-after=DURATION\n                 also send a KILL signal if COMMAND is still running\n                   this long after the initial signal was sent\n  -s, --signal=SIGNAL\n                 specify the signal to be sent on timeout;\n                   SIGNAL may be a name like \'HUP\' or a number;\n                   see \'kill -l\' for a list of signals\n",
	 DAT_00616740);
      func_0x00401bc0
	("  -v, --verbose  diagnose to stderr any signal sent upon timeout\n",
	 DAT_00616740);
      func_0x00401bc0 ("      --help     display this help and exit\n",
		       DAT_00616740);
      func_0x00401bc0
	("      --version  output version information and exit\n",
	 DAT_00616740);
      func_0x00401bc0
	("\nDURATION is a floating point number with an optional suffix:\n\'s\' for seconds (the default), \'m\' for minutes, \'h\' for hours or \'d\' for days.\n",
	 DAT_00616740);
      cVar1 = (char) DAT_00616740;
      func_0x00401bc0
	("\nIf the command times out, and --preserve-status is not set, then exit with\nstatus 124.  Otherwise, exit with the status of COMMAND.  If no signal\nis specified, send the TERM signal upon timeout.  The TERM signal kills\nany process that does not block or catch that signal.  It may be necessary\nto use the KILL (9) signal, since this signal cannot be caught, in which\ncase the exit status is 128+9 rather than 124.\n");
      imperfection_wrapper ();	//    FUN_00402080();
    }
  else
    {
      cVar1 = -0x30;
      func_0x00401ba0 (DAT_00616760,
		       "Try \'%s --help\' for more information.\n",
		       DAT_00616808);
    }
  pdVar2 = (double *) (ulong) uParm1;
  func_0x00401d70 ();
  if (cVar1 == 'h')
    {
      iStack36 = 0xe10;
    }
  else
    {
      if (cVar1 < 'i')
	{
	  if (cVar1 != '\0')
	    {
	      if (cVar1 != 'd')
		{
		  return 0;
		}
	      iStack36 = 0x15180;
	      goto LAB_00402686;
	    }
	}
      else
	{
	  if (cVar1 == 'm')
	    {
	      iStack36 = 0x3c;
	      goto LAB_00402686;
	    }
	  if (cVar1 != 's')
	    {
	      return 0;
	    }
	}
      iStack36 = 1;
    }
LAB_00402686:
  ;
  *pdVar2 = *pdVar2 * (double) iStack36;
  return 1;
}

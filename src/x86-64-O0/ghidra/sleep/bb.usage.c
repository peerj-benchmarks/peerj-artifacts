
#include "sleep.h"

long null_ARRAY_006146b_0_8_;
long null_ARRAY_006146b_8_8_;
long null_ARRAY_0061480_0_8_;
long null_ARRAY_0061480_16_8_;
long null_ARRAY_0061480_24_8_;
long null_ARRAY_0061480_32_8_;
long null_ARRAY_0061480_40_8_;
long null_ARRAY_0061480_48_8_;
long null_ARRAY_0061480_8_8_;
long null_ARRAY_0061494_0_4_;
long null_ARRAY_0061494_16_8_;
long null_ARRAY_0061494_4_4_;
long null_ARRAY_0061494_8_4_;
long DAT_00411900;
long DAT_004119bd;
long DAT_00411a3b;
long DAT_00411d16;
long DAT_00411d2b;
long DAT_00411de0;
long DAT_00411e28;
long DAT_00411f5e;
long DAT_00411f62;
long DAT_00411f67;
long DAT_00411f69;
long DAT_004125d3;
long DAT_004129a0;
long DAT_004129b8;
long DAT_004129bd;
long DAT_00412a27;
long DAT_00412ab8;
long DAT_00412abb;
long DAT_00412b09;
long DAT_00412b28;
long DAT_00412b98;
long DAT_00412b99;
long DAT_006142c0;
long DAT_006142d0;
long DAT_006142e0;
long DAT_00614688;
long DAT_006146a0;
long DAT_00614718;
long DAT_0061471c;
long DAT_00614720;
long DAT_00614740;
long DAT_00614750;
long DAT_00614760;
long DAT_00614768;
long DAT_00614780;
long DAT_00614788;
long DAT_006147d0;
long DAT_006147d8;
long DAT_006147e0;
long DAT_006147e8;
long DAT_00614978;
long DAT_00614980;
long DAT_00614988;
long DAT_00614998;
long fde_00413840;
long FLOAT_UNKNOWN;
long null_ARRAY_00411a80;
long null_ARRAY_00411d80;
long null_ARRAY_00412fe0;
long null_ARRAY_006146b0;
long null_ARRAY_006146e0;
long null_ARRAY_006147a0;
long null_ARRAY_00614800;
long null_ARRAY_00614840;
long null_ARRAY_00614940;
long PTR_DAT_00614680;
long PTR_null_ARRAY_006146c0;
long stack0x00000008;
undefined8
FUN_00401c1a (uint uParm1)
{
  char cVar1;
  double *pdVar2;
  int iStack36;

  if (uParm1 == 0)
    {
      func_0x004014d0
	("Usage: %s NUMBER[SUFFIX]...\n  or:  %s OPTION\nPause for NUMBER seconds.  SUFFIX may be \'s\' for seconds (the default),\n\'m\' for minutes, \'h\' for hours or \'d\' for days.  Unlike most implementations\nthat require NUMBER be an integer, here NUMBER may be an arbitrary floating\npoint number.  Given two or more arguments, pause for the amount of time\nspecified by the sum of their values.\n\n",
	 DAT_006147e8, DAT_006147e8);
      func_0x00401670 ("      --help     display this help and exit\n",
		       DAT_00614740);
      cVar1 = (char) DAT_00614740;
      func_0x00401670
	("      --version  output version information and exit\n");
      imperfection_wrapper ();	//    FUN_00401a7e();
    }
  else
    {
      cVar1 = -0x60;
      func_0x00401660 (DAT_00614760,
		       "Try \'%s --help\' for more information.\n",
		       DAT_006147e8);
    }
  pdVar2 = (double *) (ulong) uParm1;
  func_0x004017f0 ();
  if (cVar1 == 'h')
    {
      iStack36 = 0xe10;
    }
  else
    {
      if (cVar1 < 'i')
	{
	  if (cVar1 != '\0')
	    {
	      if (cVar1 != 'd')
		{
		  return 0;
		}
	      iStack36 = 0x15180;
	      goto LAB_00401d04;
	    }
	}
      else
	{
	  if (cVar1 == 'm')
	    {
	      iStack36 = 0x3c;
	      goto LAB_00401d04;
	    }
	  if (cVar1 != 's')
	    {
	      return 0;
	    }
	}
      iStack36 = 1;
    }
LAB_00401d04:
  ;
  *pdVar2 = *pdVar2 * (double) iStack36;
  return 1;
}

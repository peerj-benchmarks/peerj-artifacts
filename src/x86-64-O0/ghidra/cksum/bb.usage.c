
#include "cksum.h"

long null_ARRAY_00614af_0_8_;
long null_ARRAY_00614af_8_8_;
long null_ARRAY_00614c4_0_8_;
long null_ARRAY_00614c4_16_8_;
long null_ARRAY_00614c4_24_8_;
long null_ARRAY_00614c4_32_8_;
long null_ARRAY_00614c4_40_8_;
long null_ARRAY_00614c4_48_8_;
long null_ARRAY_00614c4_8_8_;
long null_ARRAY_00614d8_0_4_;
long null_ARRAY_00614d8_16_8_;
long null_ARRAY_00614d8_4_4_;
long null_ARRAY_00614d8_8_4_;
long DAT_00411a40;
long DAT_00411afd;
long DAT_00411b7b;
long DAT_00412000;
long DAT_00412002;
long DAT_00412004;
long DAT_00412159;
long DAT_00412220;
long DAT_00412268;
long DAT_0041239e;
long DAT_004123a2;
long DAT_004123a7;
long DAT_004123a9;
long DAT_00412a13;
long DAT_00412de0;
long DAT_00412df8;
long DAT_00412dfd;
long DAT_00412e67;
long DAT_00412ef8;
long DAT_00412efb;
long DAT_00412f49;
long DAT_00412f4d;
long DAT_00412fc0;
long DAT_00412fc1;
long DAT_006146e8;
long DAT_006146f8;
long DAT_00614708;
long DAT_00614ac8;
long DAT_00614ae0;
long DAT_00614b58;
long DAT_00614b5c;
long DAT_00614b60;
long DAT_00614b80;
long DAT_00614b88;
long DAT_00614b90;
long DAT_00614ba0;
long DAT_00614ba8;
long DAT_00614bc0;
long DAT_00614bc8;
long DAT_00614c10;
long DAT_00614c18;
long DAT_00614c20;
long DAT_00614c28;
long DAT_00614db8;
long DAT_00614dc0;
long DAT_00614dc8;
long DAT_00614dd8;
long fde_00413c60;
long FLOAT_UNKNOWN;
long null_ARRAY_00411bc0;
long null_ARRAY_00411c00;
long null_ARRAY_004121c0;
long null_ARRAY_00413400;
long null_ARRAY_00614af0;
long null_ARRAY_00614be0;
long null_ARRAY_00614c40;
long null_ARRAY_00614c80;
long null_ARRAY_00614d80;
long PTR_DAT_00614ac0;
long PTR_null_ARRAY_00614b00;
ulong
FUN_00402079 (uint uParm1)
{
  byte bVar1;
  int iVar2;
  uint *puVar3;
  char *pcVar4;
  bool bStack37;
  uint uStack36;

  if (uParm1 == 0)
    {
      func_0x00401590 ("Usage: %s [FILE]...\n  or:  %s [OPTION]\n",
		       DAT_00614c28, DAT_00614c28);
      func_0x00401720 ("Print CRC checksum and byte counts of each FILE.\n\n",
		       DAT_00614b80);
      func_0x00401720 ("      --help     display this help and exit\n",
		       DAT_00614b80);
      pcVar4 = (char *) DAT_00614b80;
      func_0x00401720
	("      --version  output version information and exit\n");
      imperfection_wrapper ();	//    FUN_00401b5e();
    }
  else
    {
      pcVar4 = "Try \'%s --help\' for more information.\n";
      func_0x00401710 (DAT_00614ba0,
		       "Try \'%s --help\' for more information.\n",
		       DAT_00614c28);
    }
  func_0x004018d0 ();
  FUN_004025c1 (*(undefined8 *) pcVar4);
  func_0x00401880 (6, &DAT_00411b7b);
  func_0x00401860 (FUN_0040229c);
  func_0x00401640 (DAT_00614b80, 0, 1, 0);
  imperfection_wrapper ();	//  FUN_00402470((ulong)uParm1, pcVar4, "cksum", "coreutils", PTR_DAT_00614ac0, FUN_00402079, "Q. Frank Xia", 0);
  imperfection_wrapper ();	//  iVar2 = FUN_00405662((ulong)uParm1, pcVar4, &DAT_00411b7b, null_ARRAY_00411bc0, 0);
  if (iVar2 != -1)
    {
      FUN_00402079 (1);
    }
  DAT_00614c10 = '\0';
  if (DAT_00614b58 == uParm1)
    {
      bStack37 = (bool) FUN_00401cfa (&DAT_00412000, 0);
    }
  else
    {
      bStack37 = true;
      uStack36 = DAT_00614b58;
      while ((int) uStack36 < (int) uParm1)
	{
	  bVar1 =
	    FUN_00401cfa (((undefined8 *) pcVar4)[(long) (int) uStack36], 1);
	  bStack37 = (bVar1 & bStack37) != 0;
	  uStack36 = uStack36 + 1;
	}
    }
  if ((DAT_00614c10 != '\0')
      && (iVar2 = FUN_00404369 (DAT_00614b88), iVar2 == -1))
    {
      puVar3 = (uint *) func_0x004018c0 ();
      imperfection_wrapper ();	//    FUN_00404285(1, (ulong)*puVar3, &DAT_00412000);
    }
  return (ulong) (bStack37 == false);
}


#include "wc.h"

long null_ARRAY_0061947_0_8_;
long null_ARRAY_0061947_8_8_;
long null_ARRAY_0061960_0_8_;
long null_ARRAY_0061960_16_8_;
long null_ARRAY_0061960_24_8_;
long null_ARRAY_0061960_32_8_;
long null_ARRAY_0061960_40_8_;
long null_ARRAY_0061960_48_8_;
long null_ARRAY_0061960_8_8_;
long null_ARRAY_0061974_0_4_;
long null_ARRAY_0061974_16_8_;
long null_ARRAY_0061974_4_4_;
long null_ARRAY_0061974_8_4_;
long DAT_00414af8;
long DAT_00414bb5;
long DAT_00414c33;
long DAT_004151ce;
long DAT_004151d1;
long DAT_004151e4;
long DAT_00415368;
long DAT_004153d4;
long DAT_004153d8;
long DAT_004154c6;
long DAT_004154cb;
long DAT_004154f8;
long DAT_00415568;
long DAT_0041569e;
long DAT_004156a2;
long DAT_004156a7;
long DAT_004156a9;
long DAT_00415d13;
long DAT_004160e0;
long DAT_004160f8;
long DAT_004160fd;
long DAT_00416167;
long DAT_004161f8;
long DAT_004161fb;
long DAT_00416249;
long DAT_0041625e;
long DAT_00416268;
long DAT_004162d8;
long DAT_004162d9;
long DAT_0041630e;
long DAT_00416319;
long DAT_00416f88;
long DAT_00619000;
long DAT_00619010;
long DAT_00619020;
long DAT_00619448;
long DAT_00619460;
long DAT_006194d8;
long DAT_006194dc;
long DAT_006194e0;
long DAT_00619500;
long DAT_00619508;
long DAT_00619510;
long DAT_00619520;
long DAT_00619528;
long DAT_00619540;
long DAT_00619548;
long DAT_00619590;
long DAT_00619598;
long DAT_006195a0;
long DAT_006195a8;
long DAT_006195b0;
long DAT_006195b8;
long DAT_006195b9;
long DAT_006195ba;
long DAT_006195bb;
long DAT_006195bc;
long DAT_006195c0;
long DAT_006195c4;
long DAT_006195c8;
long DAT_006195d0;
long DAT_006195d8;
long DAT_006195e0;
long DAT_006195e8;
long DAT_00619778;
long DAT_00619780;
long DAT_00619788;
long DAT_00619798;
long fde_00417b78;
long FLOAT_UNKNOWN;
long null_ARRAY_00414cc0;
long null_ARRAY_00415500;
long null_ARRAY_00416340;
long null_ARRAY_00416cc0;
long null_ARRAY_004171e0;
long null_ARRAY_00619470;
long null_ARRAY_00619560;
long null_ARRAY_00619600;
long null_ARRAY_00619640;
long null_ARRAY_00619740;
long PTR_DAT_00619440;
long PTR_FUN_006194e8;
long PTR_null_ARRAY_00619480;
long stack0x00000008;
void
FUN_00402108 (uint uParm1, undefined8 uParm2, undefined8 uParm3,
	      undefined8 uParm4, undefined8 uParm5, long lParm6)
{
  undefined8 uVar1;
  long lVar2;
  undefined8 extraout_RDX;
  char *pcVar3;
  ulong uVar4;
  undefined auStack64[24];
  undefined5 *puStack40;
  code *pcStack32;
  uint local_c;

  if (uParm1 == 0)
    {
      pcStack32 = (code *) 0x40215e;
      local_c = uParm1;
      func_0x00401860
	("Usage: %s [OPTION]... [FILE]...\n  or:  %s [OPTION]... --files0-from=F\n",
	 DAT_006195e8, DAT_006195e8);
      pcStack32 = (code *) 0x402172;
      func_0x00401a50
	("Print newline, word, and byte counts for each FILE, and a total line if\nmore than one FILE is specified.  A word is a non-zero-length sequence of\ncharacters delimited by white space.\n",
	 DAT_00619500);
      pcStack32 = (code *) 0x402177;
      FUN_00401f13 ();
      pcStack32 = (code *) 0x40218b;
      func_0x00401a50
	("\nThe options below may be used to select which counts are printed, always in\nthe following order: newline, word, character, byte, maximum line length.\n  -c, --bytes            print the byte counts\n  -m, --chars            print the character counts\n  -l, --lines            print the newline counts\n",
	 DAT_00619500);
      pcStack32 = (code *) 0x40219f;
      func_0x00401a50
	("      --files0-from=F    read input from the files specified by\n                           NUL-terminated names in file F;\n                           If F is - then read names from standard input\n  -L, --max-line-length  print the maximum display width\n  -w, --words            print the word counts\n",
	 DAT_00619500);
      pcStack32 = (code *) 0x4021b3;
      func_0x00401a50 ("      --help     display this help and exit\n",
		       DAT_00619500);
      pcStack32 = (code *) 0x4021c7;
      pcVar3 = DAT_00619500;
      func_0x00401a50
	("      --version  output version information and exit\n");
      pcStack32 = (code *) 0x4021d1;
      imperfection_wrapper ();	//    FUN_00401f2d();
    }
  else
    {
      pcVar3 = "Try \'%s --help\' for more information.\n";
      pcStack32 = (code *) 0x402139;
      local_c = uParm1;
      func_0x00401a40 (DAT_00619520,
		       "Try \'%s --help\' for more information.\n",
		       DAT_006195e8);
    }
  uVar4 = (ulong) local_c;
  pcStack32 = FUN_004021db;
  func_0x00401c50 ();
  puStack40 = (undefined5 *) 0x4154c7;
  pcStack32 = (code *) & stack0xfffffffffffffff8;
  if (DAT_006195b8 != '\0')
    {
      pcStack32 = (code *) & stack0xfffffffffffffff8;
      imperfection_wrapper ();	//    uVar1 = FUN_00403e09(uVar4, auStack64, auStack64);
      func_0x00401860 (puStack40, (ulong) DAT_006195c0, uVar1,
		       (ulong) DAT_006195c0);
      puStack40 = &DAT_004154c6;
    }
  if (DAT_006195b9 != '\0')
    {
      imperfection_wrapper ();	//    uVar1 = FUN_00403e09(pcVar3, auStack64, auStack64);
      func_0x00401860 (puStack40, (ulong) DAT_006195c0, uVar1,
		       (ulong) DAT_006195c0);
      puStack40 = &DAT_004154c6;
    }
  if (DAT_006195ba != '\0')
    {
      imperfection_wrapper ();	//    uVar1 = FUN_00403e09(extraout_RDX, auStack64, auStack64);
      func_0x00401860 (puStack40, (ulong) DAT_006195c0, uVar1,
		       (ulong) DAT_006195c0);
      puStack40 = &DAT_004154c6;
    }
  if (DAT_006195bb != '\0')
    {
      imperfection_wrapper ();	//    uVar1 = FUN_00403e09(uParm4, auStack64, auStack64);
      func_0x00401860 (puStack40, (ulong) DAT_006195c0, uVar1,
		       (ulong) DAT_006195c0);
      puStack40 = &DAT_004154c6;
    }
  if (DAT_006195bc != '\0')
    {
      imperfection_wrapper ();	//    uVar1 = FUN_00403e09(uParm5, auStack64, auStack64);
      func_0x00401860 (puStack40, (ulong) DAT_006195c0, uVar1,
		       (ulong) DAT_006195c0);
    }
  if (lParm6 != 0)
    {
      lVar2 = func_0x00401cd0 (lParm6, 10);
      if (lVar2 != 0)
	{
	  imperfection_wrapper ();	//      lParm6 = FUN_00405613(0, 3, lParm6);
	}
      func_0x00401860 (&DAT_004151d1, lParm6);
    }
  func_0x00401c00 (10);
  return;
}

typedef unsigned char   undefined;

typedef unsigned char    byte;
typedef unsigned char    dwfenc;
typedef unsigned int    dword;
typedef unsigned long    qword;
typedef unsigned char    undefined1;
typedef unsigned short    undefined2;
typedef unsigned int    undefined4;
typedef unsigned long    undefined5;
typedef unsigned long    undefined6;
typedef unsigned long    undefined8;
typedef unsigned short    word;
#define unkbyte9   unsigned long long
#define unkbyte10   unsigned long long
#define unkbyte11   unsigned long long
#define unkbyte12   unsigned long long
#define unkbyte13   unsigned long long
#define unkbyte14   unsigned long long
#define unkbyte15   unsigned long long
#define unkbyte16   unsigned long long

#define unkuint9   unsigned long long
#define unkuint10   unsigned long long
#define unkuint11   unsigned long long
#define unkuint12   unsigned long long
#define unkuint13   unsigned long long
#define unkuint14   unsigned long long
#define unkuint15   unsigned long long
#define unkuint16   unsigned long long

#define unkint9   long long
#define unkint10   long long
#define unkint11   long long
#define unkint12   long long
#define unkint13   long long
#define unkint14   long long
#define unkint15   long long
#define unkint16   long long

#define unkfloat1   float
#define unkfloat2   float
#define unkfloat3   float
#define unkfloat5   double
#define unkfloat6   double
#define unkfloat7   double
#define unkfloat9   long double
#define unkfloat11   long double
#define unkfloat12   long double
#define unkfloat13   long double
#define unkfloat14   long double
#define unkfloat15   long double
#define unkfloat16   long double

#define BADSPACEBASE   void
#define code   void

#define ulong long
#define true 1
#define bool int
#define false 0
#define float10 int
#define ushort int
#define undefined7 long
#define undefined2 long
#define uint5 long
#define uint6 long

// Add definition for uint
typedef unsigned int    uint;

#define sbyte char

extern imperfection_wrapper(void);
typedef struct eh_frame_hdr eh_frame_hdr, *Peh_frame_hdr;

struct eh_frame_hdr {
    byte eh_frame_hdr_version; // Exception Handler Frame Header Version
    dwfenc eh_frame_pointer_encoding; // Exception Handler Frame Pointer Encoding
    dwfenc eh_frame_desc_entry_count_encoding; // Encoding of # of Exception Handler FDEs
    dwfenc eh_frame_table_encoding; // Exception Handler Table Encoding
};

typedef struct fde_table_entry fde_table_entry, *Pfde_table_entry;

struct fde_table_entry {
    dword initial_loc; // Initial Location
    dword data_loc; // Data location
};

typedef enum Elf64_DynTag {
    DT_AUDIT=1879047932,
    DT_AUXILIARY=2147483645,
    DT_BIND_NOW=24,
    DT_CHECKSUM=1879047672,
    DT_CONFIG=1879047930,
    DT_DEBUG=21,
    DT_DEPAUDIT=1879047931,
    DT_ENCODING=32,
    DT_FEATURE_1=1879047676,
    DT_FILTER=2147483647,
    DT_FINI=13,
    DT_FINI_ARRAY=26,
    DT_FINI_ARRAYSZ=28,
    DT_FLAGS=30,
    DT_FLAGS_1=1879048187,
    DT_GNU_CONFLICT=1879047928,
    DT_GNU_CONFLICTSZ=1879047670,
    DT_GNU_HASH=1879047925,
    DT_GNU_LIBLIST=1879047929,
    DT_GNU_LIBLISTSZ=1879047671,
    DT_GNU_PRELINKED=1879047669,
    DT_HASH=4,
    DT_INIT=12,
    DT_INIT_ARRAY=25,
    DT_INIT_ARRAYSZ=27,
    DT_JMPREL=23,
    DT_MOVEENT=1879047674,
    DT_MOVESZ=1879047675,
    DT_MOVETAB=1879047934,
    DT_NEEDED=1,
    DT_NULL=0,
    DT_PLTGOT=3,
    DT_PLTPAD=1879047933,
    DT_PLTPADSZ=1879047673,
    DT_PLTREL=20,
    DT_PLTRELSZ=2,
    DT_POSFLAG_1=1879047677,
    DT_PREINIT_ARRAYSZ=33,
    DT_REL=17,
    DT_RELA=7,
    DT_RELACOUNT=1879048185,
    DT_RELAENT=9,
    DT_RELASZ=8,
    DT_RELCOUNT=1879048186,
    DT_RELENT=19,
    DT_RELSZ=18,
    DT_RPATH=15,
    DT_RUNPATH=29,
    DT_SONAME=14,
    DT_STRSZ=10,
    DT_STRTAB=5,
    DT_SYMBOLIC=16,
    DT_SYMENT=11,
    DT_SYMINENT=1879047679,
    DT_SYMINFO=1879047935,
    DT_SYMINSZ=1879047678,
    DT_SYMTAB=6,
    DT_TEXTREL=22,
    DT_TLSDESC_GOT=1879047927,
    DT_TLSDESC_PLT=1879047926,
    DT_VERDEF=1879048188,
    DT_VERDEFNUM=1879048189,
    DT_VERNEED=1879048190,
    DT_VERNEEDNUM=1879048191,
    DT_VERSYM=1879048176
} Elf64_DynTag;

typedef enum Elf_ProgramHeaderType {
    PT_DYNAMIC=2,
    PT_GNU_EH_FRAME=1685382480,
    PT_GNU_RELRO=1685382482,
    PT_GNU_STACK=1685382481,
    PT_INTERP=3,
    PT_LOAD=1,
    PT_NOTE=4,
    PT_NULL=0,
    PT_PHDR=6,
    PT_SHLIB=5,
    PT_TLS=7
} Elf_ProgramHeaderType;

typedef struct Elf64_Dyn Elf64_Dyn, *PElf64_Dyn;

struct Elf64_Dyn {
    enum Elf64_DynTag d_tag;
    qword d_val;
};

typedef struct Elf64_Shdr Elf64_Shdr, *PElf64_Shdr;

typedef enum Elf_SectionHeaderType {
    SHT_CHECKSUM=1879048184,
    SHT_DYNAMIC=6,
    SHT_DYNSYM=11,
    SHT_FINI_ARRAY=15,
    SHT_GNU_ATTRIBUTES=1879048181,
    SHT_GNU_HASH=1879048182,
    SHT_GNU_LIBLIST=1879048183,
    SHT_GNU_verdef=1879048189,
    SHT_GNU_verneed=1879048190,
    SHT_GNU_versym=1879048191,
    SHT_GROUP=17,
    SHT_HASH=5,
    SHT_INIT_ARRAY=14,
    SHT_NOBITS=8,
    SHT_NOTE=7,
    SHT_NULL=0,
    SHT_PREINIT_ARRAY=16,
    SHT_PROGBITS=1,
    SHT_REL=9,
    SHT_RELA=4,
    SHT_SHLIB=10,
    SHT_STRTAB=3,
    SHT_SUNW_COMDAT=1879048187,
    SHT_SUNW_move=1879048186,
    SHT_SUNW_syminfo=1879048188,
    SHT_SYMTAB=2,
    SHT_SYMTAB_SHNDX=18
} Elf_SectionHeaderType;

struct Elf64_Shdr {
    dword sh_name;
    enum Elf_SectionHeaderType sh_type;
    qword sh_flags;
    qword sh_addr;
    qword sh_offset;
    qword sh_size;
    dword sh_link;
    dword sh_info;
    qword sh_addralign;
    qword sh_entsize;
};

typedef struct Elf64_Phdr Elf64_Phdr, *PElf64_Phdr;

struct Elf64_Phdr {
    enum Elf_ProgramHeaderType p_type;
    dword p_flags;
    qword p_offset;
    qword p_vaddr;
    qword p_paddr;
    qword p_filesz;
    qword p_memsz;
    qword p_align;
};

typedef struct Elf64_Sym Elf64_Sym, *PElf64_Sym;

struct Elf64_Sym {
    dword st_name;
    byte st_info;
    byte st_other;
    word st_shndx;
    qword st_value;
    qword st_size;
};

typedef struct Elf64_Rela Elf64_Rela, *PElf64_Rela;

struct Elf64_Rela {
    qword r_offset; // location to apply the relocation action
    qword r_info; // the symbol table index and the type of relocation
    qword r_addend; // a constant addend used to compute the relocatable field value
};

typedef struct Elf64_Ehdr Elf64_Ehdr, *PElf64_Ehdr;

struct Elf64_Ehdr {
    byte e_ident_magic_num;
    char e_ident_magic_str[3];
    byte e_ident_class;
    byte e_ident_data;
    byte e_ident_version;
    byte e_ident_pad[9];
    word e_type;
    word e_machine;
    dword e_version;
    qword e_entry;
    qword e_phoff;
    qword e_shoff;
    dword e_flags;
    word e_ehsize;
    word e_phentsize;
    word e_phnum;
    word e_shentsize;
    word e_shnum;
    word e_shstrndx;
};




undefined8 _DT_INIT(void);
void FUN_00401830(void);
void FUN_00401d40(void);
void entry(void);
void FUN_00401da0(void);
void FUN_00401e20(void);
void FUN_00401ea0(void);
undefined8 FUN_00401ede(int iParm1);
ulong FUN_00401f04(byte bParm1);
void FUN_00401f13(void);
void FUN_00401f2d(undefined *puParm1);
ulong FUN_004020c9(long lParm1);
void FUN_00402108(uint uParm1,undefined8 uParm2,undefined8 uParm3,undefined8 uParm4,undefined8 uParm5,long lParm6);
void FUN_004021db(undefined8 uParm1,undefined8 uParm2,undefined8 uParm3,undefined8 uParm4,undefined8 uParm5,long lParm6);
ulong FUN_0040239c(uint uParm1,char *pcParm2,int *piParm3,ulong uParm4);
ulong FUN_00402e5d(long lParm1,undefined8 uParm2);
undefined4 * FUN_00402f93(ulong uParm1,long lParm2);
ulong FUN_00403131(ulong uParm1,int *piParm2);
ulong FUN_00403254(uint uParm1,undefined8 *puParm2);
undefined8 * FUN_00403ae0(undefined8 uParm1);
undefined8 * FUN_00403b31(undefined8 uParm1);
long FUN_00403b9a(long *plParm1,undefined4 *puParm2);
long FUN_00403c81(long *plParm1);
void FUN_00403cbe(long *plParm1);
void FUN_00403cf4(void);
void FUN_00403dd9(uint uParm1,undefined8 uParm2,undefined8 uParm3,uint uParm4);
char * FUN_00403e09(ulong uParm1,long lParm2);
ulong FUN_00403e8e(byte bParm1);
void FUN_00403ec4(void);
void FUN_00403fd7(void);
void FUN_00404118(long lParm1);
ulong FUN_004041f5(undefined1 *puParm1,byte bParm2,uint uParm3);
undefined8 * FUN_0040427d(undefined8 *puParm1,int iParm2);
char * FUN_004042f4(char *pcParm1,int iParm2);
ulong FUN_00404394(long param_1,ulong param_2,long param_3,ulong param_4,uint param_5,uint param_6,long param_7,char *param_8,char *param_9);
undefined1 * FUN_0040525e(int iParm1,undefined8 uParm2,undefined8 uParm3,uint *puParm4);
void FUN_004054d1(uint uParm1,uint uParm2,undefined8 uParm3);
void FUN_00405512(uint uParm1,undefined8 uParm2);
void FUN_00405536(undefined8 uParm1,undefined8 uParm2,char cParm3);
void FUN_004055ca(undefined8 uParm1,char cParm2);
void FUN_004055f4(undefined8 uParm1);
void FUN_00405613(uint uParm1,uint uParm2,undefined8 uParm3);
void FUN_004056ae(undefined8 *puParm1);
void FUN_00405750(long lParm1);
void FUN_00405883(long *plParm1);
ulong FUN_00405a8e(undefined8 uParm1,long lParm2);
long FUN_00405e0f(uint uParm1,undefined8 uParm2,undefined8 uParm3);
void FUN_00405e80(undefined8 uParm1,long lParm2,undefined8 uParm3,undefined8 uParm4,undefined8 *puParm5,undefined8 uParm6);
void FUN_004062e4(undefined8 uParm1,undefined8 uParm2,undefined8 uParm3,undefined8 uParm4,uint *puParm5);
void FUN_004063b6(undefined4 uParm1,undefined4 uParm2,undefined4 uParm3,undefined4 uParm4,undefined4 uParm5,undefined4 uParm6,undefined4 uParm7,undefined4 uParm8,undefined8 param_1,undefined8 param_2,undefined8 param_3,undefined8 param_4,undefined8 param_5,undefined8 param_6);
void FUN_0040646c(ulong uParm1,ulong uParm2);
void FUN_004064ad(undefined8 uParm1);
long FUN_004064c7(long lParm1);
long FUN_004064fc(long lParm1,long lParm2);
void FUN_0040655d(void);
void FUN_00406587(void);
void FUN_0040658d(uint uParm1,uint uParm2);
ulong FUN_004065b5(uint uParm1);
void FUN_004065de(void);
void FUN_00406612(uint uParm1);
void FUN_00406686(uint uParm1,uint uParm2,undefined8 uParm3,undefined8 uParm4);
void FUN_0040670a(undefined4 uParm1,undefined4 uParm2,undefined4 uParm3,undefined4 uParm4,undefined4 uParm5,undefined4 uParm6,undefined4 uParm7,undefined4 uParm8,uint param_1,uint param_2,undefined8 param_3,undefined8 param_4,undefined8 param_5,undefined8 param_6);
ulong FUN_004067ee(undefined8 uParm1);
void FUN_004068a6(undefined8 uParm1);
void FUN_004068ca(void);
ulong FUN_004068d8(long lParm1);
undefined8 FUN_004069a8(undefined8 uParm1);
void FUN_004069c7(undefined8 uParm1,undefined8 uParm2,uint uParm3);
void FUN_004069f2(void);
long FUN_004069f8(long *plParm1,ulong *puParm2,int iParm3,long lParm4);
void FUN_00406bd2(long lParm1,int *piParm2);
ulong FUN_00406db6(int param_1,undefined8 *param_2,char *param_3,long *param_4,int *param_5,int param_6,int *param_7,int param_8,undefined8 param_9);
char * FUN_004073a0(undefined8 uParm1,undefined8 uParm2,char *pcParm3,int *piParm4,int iParm5);
ulong FUN_0040746e(uint param_1,undefined8 *param_2,char *param_3,long param_4,undefined8 param_5,uint param_6,uint *param_7,uint param_8);
ulong FUN_00407c37(uint param_1,undefined8 param_2,undefined8 param_3,undefined8 param_4,undefined8 param_5,uint param_6,uint param_7);
void FUN_00407cc7(uint uParm1,undefined8 uParm2,undefined8 uParm3,undefined8 uParm4,undefined8 uParm5);
undefined8 FUN_00407d11(long lParm1);
ulong FUN_00407d42(uint *puParm1,byte *pbParm2,long lParm3,undefined8 uParm4);
ulong * FUN_00407dc5(ulong *puParm1,char cParm2,ulong uParm3);
void FUN_00407ed7(long lParm1,undefined8 uParm2);
void FUN_00407f27(long lParm1,undefined8 uParm2);
undefined8 FUN_00407f77(long *plParm1,long lParm2,long lParm3);
void FUN_0040809c(long lParm1,undefined8 uParm2,undefined8 uParm3,undefined8 uParm4,undefined8 uParm5);
void FUN_004080f7(ulong *puParm1,long lParm2);
void FUN_004082ec(long lParm1,ulong *puParm2);
undefined8 FUN_004083ab(void);
undefined8 FUN_004083da(long lParm1,long lParm2);
void FUN_00408443(undefined8 uParm1,undefined8 uParm2);
ulong FUN_00408468(long lParm1,long lParm2);
ulong FUN_004084f8(undefined8 uParm1,undefined8 uParm2,undefined8 uParm3);
void FUN_00408619(uint uParm1);
undefined8 FUN_00408661(void);
undefined8 FUN_00408672(void);
ulong FUN_00408680(uint uParm1,uint uParm2);
ulong FUN_004086b7(byte *pbParm1,byte *pbParm2);
undefined8 FUN_0040872e(undefined8 uParm1);
undefined8 FUN_004087b9(void);
ulong FUN_004087c6(uint uParm1);
undefined1 * FUN_00408897(void);
char * FUN_00408c4a(void);
ulong FUN_00408d18(uint uParm1);
undefined8 FUN_00408d28(undefined8 uParm1);
undefined8 FUN_00408dee(uint uParm1,undefined8 uParm2);
ulong FUN_00409007(void);
long FUN_00409054(ulong uParm1,long lParm2,ulong uParm3,long lParm4,long *plParm5);
int * FUN_004092a0(ulong uParm1,uint *puParm2,ulong uParm3,uint *puParm4,ulong *puParm5);
char * FUN_00409d80(ulong uParm1,long lParm2,long lParm3);
long FUN_00409fee(int *param_1,long *param_2);
long FUN_0040a1d9(undefined auParm1 [16],int *piParm2,long *plParm3);
undefined8 FUN_0040a598(int iParm1,long lParm2,uint *puParm3,long lParm4,uint uParm5);
void FUN_0040adab(uint param_1);
void FUN_0040adf5(undefined8 uParm1,uint uParm2);
ulong FUN_0040ae47(void);
ulong FUN_0040b1d9(undefined auParm1 [16],undefined8 uParm2,undefined8 uParm3);
ulong FUN_0040b5b1(char *pcParm1,long lParm2);
undefined8 FUN_0040b60a(long param_1,long param_2,undefined param_3,int param_4,undefined8 param_5,ulong param_6,int param_7,ulong param_8,int param_9);
long FUN_0040b8cd(long lParm1,char **ppcParm2,long lParm3,undefined8 uParm4);
ulong FUN_00412d37(uint uParm1);
void FUN_00412d56(undefined8 uParm1,int *piParm2,undefined8 uParm3);
void FUN_00412df7(int *param_1);
ulong FUN_00412eb6(ulong uParm1,long lParm2);
void FUN_00412eea(undefined8 uParm1,undefined8 uParm2,undefined8 uParm3);
void FUN_00412f25(undefined8 uParm1,undefined8 uParm2,undefined8 uParm3,undefined8 uParm4);
ulong FUN_00412f76(ulong uParm1,ulong uParm2);
undefined8 FUN_00412f91(uint *puParm1,ulong *puParm2);
undefined8 FUN_00413731(char *pcParm1,ulong *puParm2,char **ppcParm3);
ulong FUN_004149e7(undefined auParm1 [16]);
ulong FUN_00414a22(void);
void FUN_00414a60(void);
undefined8 _DT_FINI(void);
undefined FUN_0061a000();
undefined FUN_0061a008();
undefined FUN_0061a010();
undefined FUN_0061a018();
undefined FUN_0061a020();
undefined FUN_0061a028();
undefined FUN_0061a030();
undefined FUN_0061a038();
undefined FUN_0061a040();
undefined FUN_0061a048();
undefined FUN_0061a050();
undefined FUN_0061a058();
undefined FUN_0061a060();
undefined FUN_0061a068();
undefined FUN_0061a070();
undefined FUN_0061a078();
undefined FUN_0061a080();
undefined FUN_0061a088();
undefined FUN_0061a090();
undefined FUN_0061a098();
undefined FUN_0061a0a0();
undefined FUN_0061a0a8();
undefined FUN_0061a0b0();
undefined FUN_0061a0b8();
undefined FUN_0061a0c0();
undefined FUN_0061a0c8();
undefined FUN_0061a0d0();
undefined FUN_0061a0d8();
undefined FUN_0061a0e0();
undefined FUN_0061a0e8();
undefined FUN_0061a0f0();
undefined FUN_0061a0f8();
undefined FUN_0061a100();
undefined FUN_0061a108();
undefined FUN_0061a110();
undefined FUN_0061a118();
undefined FUN_0061a120();
undefined FUN_0061a128();
undefined FUN_0061a130();
undefined FUN_0061a138();
undefined FUN_0061a140();
undefined FUN_0061a148();
undefined FUN_0061a150();
undefined FUN_0061a158();
undefined FUN_0061a160();
undefined FUN_0061a168();
undefined FUN_0061a170();
undefined FUN_0061a178();
undefined FUN_0061a180();
undefined FUN_0061a188();
undefined FUN_0061a190();
undefined FUN_0061a198();
undefined FUN_0061a1a0();
undefined FUN_0061a1a8();
undefined FUN_0061a1b0();
undefined FUN_0061a1b8();
undefined FUN_0061a1c0();
undefined FUN_0061a1c8();
undefined FUN_0061a1d0();
undefined FUN_0061a1d8();
undefined FUN_0061a1e0();
undefined FUN_0061a1e8();
undefined FUN_0061a1f0();
undefined FUN_0061a1f8();
undefined FUN_0061a200();
undefined FUN_0061a208();
undefined FUN_0061a210();
undefined FUN_0061a218();
undefined FUN_0061a220();
undefined FUN_0061a228();
undefined FUN_0061a230();
undefined FUN_0061a238();
undefined FUN_0061a240();
undefined FUN_0061a248();
undefined FUN_0061a250();
undefined FUN_0061a258();
undefined FUN_0061a260();
undefined FUN_0061a268();
undefined FUN_0061a270();
undefined FUN_0061a278();
undefined FUN_0061a280();


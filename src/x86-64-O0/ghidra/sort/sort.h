typedef unsigned char   undefined;

typedef unsigned char    byte;
typedef unsigned char    dwfenc;
typedef unsigned int    dword;
typedef unsigned long    qword;
typedef unsigned char    undefined1;
typedef unsigned short    undefined2;
typedef unsigned int    undefined3;
typedef unsigned int    undefined4;
typedef unsigned long    undefined5;
typedef unsigned long    undefined6;
typedef unsigned long    undefined8;
typedef unsigned short    word;
#define unkbyte9   unsigned long long
#define unkbyte10   unsigned long long
#define unkbyte11   unsigned long long
#define unkbyte12   unsigned long long
#define unkbyte13   unsigned long long
#define unkbyte14   unsigned long long
#define unkbyte15   unsigned long long
#define unkbyte16   unsigned long long

#define unkuint9   unsigned long long
#define unkuint10   unsigned long long
#define unkuint11   unsigned long long
#define unkuint12   unsigned long long
#define unkuint13   unsigned long long
#define unkuint14   unsigned long long
#define unkuint15   unsigned long long
#define unkuint16   unsigned long long

#define unkint9   long long
#define unkint10   long long
#define unkint11   long long
#define unkint12   long long
#define unkint13   long long
#define unkint14   long long
#define unkint15   long long
#define unkint16   long long

#define unkfloat1   float
#define unkfloat2   float
#define unkfloat3   float
#define unkfloat5   double
#define unkfloat6   double
#define unkfloat7   double
#define unkfloat9   long double
#define unkfloat11   long double
#define unkfloat12   long double
#define unkfloat13   long double
#define unkfloat14   long double
#define unkfloat15   long double
#define unkfloat16   long double

#define BADSPACEBASE   void
#define code   void

#define ulong long
#define true 1
#define bool int
#define false 0
#define float10 int
#define ushort int
#define undefined7 long
#define undefined2 long
#define uint5 long
#define uint6 long

// Add definition for uint
typedef unsigned int    uint;

#define sbyte char

extern imperfection_wrapper(void);
typedef struct eh_frame_hdr eh_frame_hdr, *Peh_frame_hdr;

struct eh_frame_hdr {
    byte eh_frame_hdr_version; // Exception Handler Frame Header Version
    dwfenc eh_frame_pointer_encoding; // Exception Handler Frame Pointer Encoding
    dwfenc eh_frame_desc_entry_count_encoding; // Encoding of # of Exception Handler FDEs
    dwfenc eh_frame_table_encoding; // Exception Handler Table Encoding
};

typedef struct fde_table_entry fde_table_entry, *Pfde_table_entry;

struct fde_table_entry {
    dword initial_loc; // Initial Location
    dword data_loc; // Data location
};

typedef enum Elf64_DynTag {
    DT_AUDIT=1879047932,
    DT_AUXILIARY=2147483645,
    DT_BIND_NOW=24,
    DT_CHECKSUM=1879047672,
    DT_CONFIG=1879047930,
    DT_DEBUG=21,
    DT_DEPAUDIT=1879047931,
    DT_ENCODING=32,
    DT_FEATURE_1=1879047676,
    DT_FILTER=2147483647,
    DT_FINI=13,
    DT_FINI_ARRAY=26,
    DT_FINI_ARRAYSZ=28,
    DT_FLAGS=30,
    DT_FLAGS_1=1879048187,
    DT_GNU_CONFLICT=1879047928,
    DT_GNU_CONFLICTSZ=1879047670,
    DT_GNU_HASH=1879047925,
    DT_GNU_LIBLIST=1879047929,
    DT_GNU_LIBLISTSZ=1879047671,
    DT_GNU_PRELINKED=1879047669,
    DT_HASH=4,
    DT_INIT=12,
    DT_INIT_ARRAY=25,
    DT_INIT_ARRAYSZ=27,
    DT_JMPREL=23,
    DT_MOVEENT=1879047674,
    DT_MOVESZ=1879047675,
    DT_MOVETAB=1879047934,
    DT_NEEDED=1,
    DT_NULL=0,
    DT_PLTGOT=3,
    DT_PLTPAD=1879047933,
    DT_PLTPADSZ=1879047673,
    DT_PLTREL=20,
    DT_PLTRELSZ=2,
    DT_POSFLAG_1=1879047677,
    DT_PREINIT_ARRAYSZ=33,
    DT_REL=17,
    DT_RELA=7,
    DT_RELACOUNT=1879048185,
    DT_RELAENT=9,
    DT_RELASZ=8,
    DT_RELCOUNT=1879048186,
    DT_RELENT=19,
    DT_RELSZ=18,
    DT_RPATH=15,
    DT_RUNPATH=29,
    DT_SONAME=14,
    DT_STRSZ=10,
    DT_STRTAB=5,
    DT_SYMBOLIC=16,
    DT_SYMENT=11,
    DT_SYMINENT=1879047679,
    DT_SYMINFO=1879047935,
    DT_SYMINSZ=1879047678,
    DT_SYMTAB=6,
    DT_TEXTREL=22,
    DT_TLSDESC_GOT=1879047927,
    DT_TLSDESC_PLT=1879047926,
    DT_VERDEF=1879048188,
    DT_VERDEFNUM=1879048189,
    DT_VERNEED=1879048190,
    DT_VERNEEDNUM=1879048191,
    DT_VERSYM=1879048176
} Elf64_DynTag;

typedef struct Elf64_Rela Elf64_Rela, *PElf64_Rela;

struct Elf64_Rela {
    qword r_offset; // location to apply the relocation action
    qword r_info; // the symbol table index and the type of relocation
    qword r_addend; // a constant addend used to compute the relocatable field value
};

typedef enum Elf_ProgramHeaderType {
    PT_DYNAMIC=2,
    PT_GNU_EH_FRAME=1685382480,
    PT_GNU_RELRO=1685382482,
    PT_GNU_STACK=1685382481,
    PT_INTERP=3,
    PT_LOAD=1,
    PT_NOTE=4,
    PT_NULL=0,
    PT_PHDR=6,
    PT_SHLIB=5,
    PT_TLS=7
} Elf_ProgramHeaderType;

typedef struct Elf64_Dyn Elf64_Dyn, *PElf64_Dyn;

struct Elf64_Dyn {
    enum Elf64_DynTag d_tag;
    qword d_val;
};

typedef struct Elf64_Shdr Elf64_Shdr, *PElf64_Shdr;

typedef enum Elf_SectionHeaderType {
    SHT_CHECKSUM=1879048184,
    SHT_DYNAMIC=6,
    SHT_DYNSYM=11,
    SHT_FINI_ARRAY=15,
    SHT_GNU_ATTRIBUTES=1879048181,
    SHT_GNU_HASH=1879048182,
    SHT_GNU_LIBLIST=1879048183,
    SHT_GNU_verdef=1879048189,
    SHT_GNU_verneed=1879048190,
    SHT_GNU_versym=1879048191,
    SHT_GROUP=17,
    SHT_HASH=5,
    SHT_INIT_ARRAY=14,
    SHT_NOBITS=8,
    SHT_NOTE=7,
    SHT_NULL=0,
    SHT_PREINIT_ARRAY=16,
    SHT_PROGBITS=1,
    SHT_REL=9,
    SHT_RELA=4,
    SHT_SHLIB=10,
    SHT_STRTAB=3,
    SHT_SUNW_COMDAT=1879048187,
    SHT_SUNW_move=1879048186,
    SHT_SUNW_syminfo=1879048188,
    SHT_SYMTAB=2,
    SHT_SYMTAB_SHNDX=18
} Elf_SectionHeaderType;

struct Elf64_Shdr {
    dword sh_name;
    enum Elf_SectionHeaderType sh_type;
    qword sh_flags;
    qword sh_addr;
    qword sh_offset;
    qword sh_size;
    dword sh_link;
    dword sh_info;
    qword sh_addralign;
    qword sh_entsize;
};

typedef struct Elf64_Phdr Elf64_Phdr, *PElf64_Phdr;

struct Elf64_Phdr {
    enum Elf_ProgramHeaderType p_type;
    dword p_flags;
    qword p_offset;
    qword p_vaddr;
    qword p_paddr;
    qword p_filesz;
    qword p_memsz;
    qword p_align;
};

typedef struct Elf64_Sym Elf64_Sym, *PElf64_Sym;

struct Elf64_Sym {
    dword st_name;
    byte st_info;
    byte st_other;
    word st_shndx;
    qword st_value;
    qword st_size;
};

typedef struct Elf64_Ehdr Elf64_Ehdr, *PElf64_Ehdr;

struct Elf64_Ehdr {
    byte e_ident_magic_num;
    char e_ident_magic_str[3];
    byte e_ident_class;
    byte e_ident_data;
    byte e_ident_version;
    byte e_ident_pad[9];
    word e_type;
    word e_machine;
    dword e_version;
    qword e_entry;
    qword e_phoff;
    qword e_shoff;
    dword e_flags;
    word e_ehsize;
    word e_phentsize;
    word e_phnum;
    word e_shentsize;
    word e_shnum;
    word e_shstrndx;
};




undefined8 _DT_INIT(void);
void FUN_00402610(void);
void thunk_FUN_0062e170(void);
void FUN_00402e70(void);
void entry(void);
void FUN_00402ed0(void);
void FUN_00402f50(void);
void FUN_00402fd0(void);
void FUN_0040300e(int iParm1);
ulong FUN_00403026(byte bParm1);
ulong FUN_00403035(byte bParm1);
undefined8 FUN_00403068(undefined8 uParm1);
void FUN_00403076(void);
void FUN_00403090(void);
void FUN_004030aa(undefined *puParm1);
void FUN_00403246(uint uParm1,undefined8 uParm2);
void FUN_004032f2(undefined8 uParm1,char *pcParm2);
void FUN_00403351(uint uParm1);
void FUN_00403541(long lParm1);
void FUN_00403579(char *pcParm1);
ulong FUN_004035ac(long lParm1,ulong uParm2);
ulong FUN_004035d7(long lParm1,long lParm2);
ulong FUN_00403608(uint uParm1);
void FUN_004036f8(long lParm1);
ulong FUN_00403771(undefined4 uParm1);
void FUN_004037b9(uint uParm1);
void FUN_004037de(void);
void FUN_004037ff(void);
void FUN_00403814(void);
void FUN_00403830(void);
void FUN_00403874(void);
undefined8 * FUN_004038b5(int *piParm1,char cParm2);
undefined8 FUN_00403a79(long lParm1,char *pcParm2);
long FUN_00403b9e(undefined8 uParm1,undefined8 uParm2);
void FUN_00403be3(undefined8 uParm1,undefined8 uParm2);
void FUN_00403c71(uint uParm1,uint uParm2);
ulong FUN_00403ca2(uint *puParm1,long lParm2);
long FUN_00403e3f(long *plParm1,byte bParm2);
void FUN_00403f85(undefined8 uParm1);
long FUN_00403fa4(long lParm1);
void FUN_00404155(undefined8 uParm1);
void FUN_004041be(long lParm1);
void FUN_004042cd(undefined8 *puParm1,undefined8 *puParm2);
void FUN_00404308(void);
void FUN_00404502(uint uParm1,char cParm2,undefined8 uParm3);
void FUN_004046e3(uint uParm1,char cParm2,undefined8 uParm3);
long FUN_004048f5(uint uParm1,char cParm2,undefined8 uParm3);
ulong FUN_00404989(void);
long FUN_00404bd1(long lParm1,ulong uParm2,long lParm3,ulong uParm4,long lParm5);
void FUN_00404de4(long *plParm1,long lParm2,ulong uParm3);
long FUN_00404e9a(long *plParm1);
char * FUN_00404ebe(char **ppcParm1,long *plParm2);
char * FUN_00405054(char **ppcParm1,long lParm2);
undefined8 FUN_0040520c(char **ppcParm1,undefined8 uParm2,undefined8 uParm3);
ulong FUN_00405687(byte **ppbParm1);
ulong FUN_00405763(char *pcParm1);
ulong FUN_004057d8(char *pcParm1,char *pcParm2);
void FUN_00405888(char *pcParm1,char *pcParm2);
void FUN_00405906(undefined8 uParm1,undefined8 uParm2,undefined8 uParm3,undefined8 uParm4,undefined8 uParm5,undefined8 uParm6);
undefined8 FUN_004059a0(long lParm1,long lParm2);
ulong FUN_00405ad4(char *pcParm1,char **ppcParm2);
void FUN_00405c3c(undefined8 uParm1);
undefined8 FUN_00405cd0(undefined8 uParm1,undefined8 uParm2,undefined8 uParm3);
ulong FUN_00405d83(undefined *puParm1,ulong uParm2,undefined *puParm3,ulong uParm4);
long FUN_004062b0(char *pcParm1,char *pcParm2);
void FUN_0040631a(long lParm1,long lParm2);
ulong FUN_00406381(long lParm1);
void FUN_004063be(byte **ppbParm1,long *plParm2);
void FUN_00406637(undefined8 uParm1);
ulong FUN_00406699(long lParm1);
void FUN_0040672e(long lParm1,undefined *puParm2);
void FUN_00406881(undefined8 *puParm1,char cParm2);
ulong FUN_00406f47(char **ppcParm1,char **ppcParm2);
ulong FUN_00407969(undefined8 *puParm1,undefined8 *puParm2);
void FUN_00407ab0(char **ppcParm1,undefined8 uParm2,long lParm3);
ulong FUN_00407bc4(undefined8 uParm1,char cParm2);
long FUN_00407f00(long lParm1,ulong uParm2,long *plParm3);
void FUN_0040801b(long lParm1,ulong uParm2,ulong uParm3,undefined8 uParm4,undefined8 uParm5,long lParm6);
ulong FUN_00408c1d(long lParm1,undefined8 uParm2,ulong uParm3,undefined8 uParm4,undefined8 uParm5);
void FUN_00408cb3(long lParm1,ulong uParm2,undefined8 *puParm3);
void FUN_00408dd5(long lParm1,ulong uParm2,long lParm3,byte bParm4);
undefined8 * FUN_00409055(long lParm1,undefined8 uParm2,undefined8 uParm3);
undefined8 *FUN_0040914e(long lParm1,undefined8 *puParm2,long lParm3,ulong uParm4,long lParm5,char cParm6);
ulong FUN_00409345(long lParm1,long lParm2);
void FUN_004093bd(long lParm1);
void FUN_004093db(long lParm1);
void FUN_004093f9(undefined8 *puParm1,long lParm2);
void FUN_00409453(undefined8 *puParm1,long lParm2);
long FUN_004094b3(undefined8 *puParm1);
void FUN_00409530(long *plParm1,undefined8 uParm2,undefined8 uParm3);
void FUN_004095ba(long *plParm1,ulong uParm2,undefined8 uParm3,undefined8 uParm4);
void FUN_00409a74(undefined8 uParm1,long *plParm2);
void FUN_00409b49(undefined8 uParm1,long lParm2);
void FUN_00409bcf(undefined8 uParm1,undefined8 uParm2,undefined8 uParm3,undefined8 uParm4);
undefined8 FUN_00409c74(undefined8 *puParm1);
void FUN_00409cd7(long param_1,ulong param_2,long param_3,long *param_4,undefined8 param_5,undefined8 param_6,undefined8 param_7);
void FUN_00409f81(long lParm1,ulong uParm2,ulong uParm3,long lParm4);
void FUN_0040a1a2(long lParm1,ulong uParm2);
void FUN_0040a244(long lParm1);
void FUN_0040a2a4(long *plParm1,ulong uParm2,ulong uParm3,undefined8 uParm4);
void FUN_0040a779(undefined8 *puParm1,long lParm2,long lParm3,ulong uParm4);
void FUN_0040ab5c(undefined8 uParm1);
void FUN_0040abbb(undefined8 uParm1,undefined8 uParm2);
void FUN_0040abfd(undefined8 uParm1);
void FUN_0040ac29(void);
undefined8 FUN_0040ad04(undefined8 uParm1,long *plParm2,long lParm3);
void FUN_0040adbb(uint uParm1);
char * FUN_0040ade6(char *pcParm1,long lParm2,int iParm3);
long FUN_0040aeed(long lParm1);
undefined8 FUN_0040af21(uint uParm1,undefined8 *puParm2);
void FUN_0040c4c4(void);
long FUN_0040c4d4(undefined8 uParm1,long lParm2,long lParm3,long lParm4);
void FUN_0040c603(undefined8 uParm1,undefined8 uParm2,long lParm3);
void FUN_0040c684(long lParm1,long lParm2,long lParm3);
long FUN_0040c7ba(undefined8 uParm1,undefined8 uParm2,undefined8 uParm3,undefined8 uParm4,undefined8 uParm5,code *pcParm6);
void FUN_0040c840(void);
void FUN_0040c925(undefined4 *puParm1);
void FUN_0040c97e(undefined8 uParm1,undefined4 uParm2);
long FUN_0040c9a7(uint *puParm1,long lParm2);
void FUN_0040ca24(long lParm1,undefined8 uParm2);
void FUN_0040cb40(ulong uParm1,ulong uParm2,long lParm3);
void FUN_0040cd74(int *piParm1,ulong uParm2,int *piParm3);
void FUN_0040da67(uint uParm1,undefined8 uParm2,undefined8 uParm3,uint uParm4);
void FUN_0040da97(long lParm1,uint uParm2);
char * FUN_0040dad1(char **ppcParm1);
ulong FUN_0040dbac(byte bParm1);
ulong FUN_0040dbfc(long lParm1,ulong uParm2,long lParm3,ulong uParm4);
ulong FUN_0040de1a(char *pcParm1,char *pcParm2);
ulong FUN_0040e053(uint uParm1);
long FUN_0040e124(long *plParm1,undefined8 uParm2);
ulong FUN_0040e17b(ulong uParm1);
ulong FUN_0040e1e7(ulong uParm1);
ulong FUN_0040e22e(undefined8 uParm1,ulong uParm2);
ulong FUN_0040e265(ulong uParm1,ulong uParm2);
undefined8 FUN_0040e27e(long lParm1);
ulong FUN_0040e377(ulong uParm1,long lParm2);
long * FUN_0040e46d(undefined8 uParm1,undefined1 *puParm2,code *pcParm3,code *pcParm4,long lParm5);
long FUN_0040e5d5(long lParm1);
void FUN_0040e620(long lParm1,undefined8 *puParm2);
long FUN_0040e655(long lParm1,long lParm2,long **pplParm3,char cParm4);
undefined8 FUN_0040e7ea(long lParm1,long **pplParm2,char cParm3);
undefined8 FUN_0040e9b8(long *plParm1,undefined8 uParm2);
undefined8 FUN_0040ebbc(long lParm1,long lParm2,long *plParm3);
undefined8 FUN_0040ef0e(undefined8 uParm1,undefined8 uParm2);
long FUN_0040ef57(long lParm1,undefined8 uParm2);
undefined8 * FUN_0040f236(code *pcParm1,long lParm2);
undefined8 FUN_0040f2c4(void);
undefined8 FUN_0040f2d7(long *plParm1,undefined8 uParm2);
undefined8 FUN_0040f383(long *plParm1);
ulong FUN_0040f412(long lParm1,ulong uParm2,ulong uParm3,code *pcParm4);
void FUN_0040f53c(long lParm1,ulong uParm2,code *pcParm3);
char * FUN_0040f5fe(int iParm1,long lParm2);
char * FUN_0040f6e8(uint uParm1,long lParm2);
char * FUN_0040f752(ulong uParm1,long lParm2);
ulong FUN_0040f7d7(byte *pbParm1,long lParm2,uint uParm3);
long FUN_0040fa10(void);
long FUN_0040fa66(int iParm1);
undefined8 FUN_0040facc(char *pcParm1);
ulong FUN_0040fbaf(uint uParm1);
void FUN_0040fc3d(void);
void FUN_0040fd50(void);
ulong FUN_0040fe91(uint *puParm1,uint uParm2);
long FUN_004100f7(void);
void FUN_0041017b(long lParm1);
ulong FUN_00410258(undefined1 *puParm1,byte bParm2,uint uParm3);
undefined8 * FUN_004102e0(undefined8 *puParm1,int iParm2);
char * FUN_00410357(char *pcParm1,int iParm2);
ulong FUN_004103f7(long param_1,ulong param_2,long param_3,ulong param_4,uint param_5,uint param_6,long param_7,char *param_8,char *param_9);
undefined1 * FUN_004112c1(int iParm1,undefined8 uParm2,undefined8 uParm3,uint *puParm4);
void FUN_00411534(uint uParm1,uint uParm2,undefined8 uParm3);
void FUN_00411575(uint uParm1,uint uParm2,undefined8 uParm3,undefined8 uParm4);
void FUN_004115b4(uint uParm1,undefined8 uParm2);
void FUN_004115d8(undefined8 uParm1,undefined8 uParm2,char cParm3);
void FUN_0041166c(undefined8 uParm1,char cParm2);
void FUN_00411696(undefined8 uParm1);
void FUN_004116b5(uint uParm1,uint uParm2,undefined8 uParm3);
void FUN_00411750(uint uParm1,undefined8 uParm2,undefined8 uParm3);
void FUN_0041177c(uint uParm1,undefined8 uParm2);
void FUN_004117a5(undefined8 uParm1);
void FUN_004117c4(long lParm1);
undefined8 * FUN_00411824(undefined8 uParm1,undefined8 uParm2);
void FUN_0041186b(long lParm1,ulong uParm2,ulong uParm3);
long FUN_00411ad0(long lParm1,ulong uParm2);
void FUN_00411bbf(undefined8 *puParm1,long lParm2,long lParm3);
void FUN_00411c5f(ulong *puParm1,ulong uParm2,ulong uParm3);
void FUN_00411da4(long *plParm1,undefined8 uParm2,undefined8 uParm3);
undefined8 FUN_00411dfa(long *plParm1);
undefined8 FUN_00411e4a(undefined8 uParm1);
undefined8 FUN_00411e64(long lParm1,undefined8 uParm2);
void FUN_00411ea8(ulong *puParm1,long *plParm2);
void FUN_00412538(long lParm1);
void FUN_00412abf(undefined8 *puParm1);
void FUN_00412b61(long *plParm1);
ulong FUN_00412d6c(undefined8 uParm1,long lParm2);
void FUN_004130ed(undefined8 uParm1,uint uParm2);
ulong FUN_0041311d(char *pcParm1,char *pcParm2,char cParm3);
ulong FUN_00413280(byte *pbParm1,byte *pbParm2,uint uParm3,uint uParm4);
void FUN_004136f0(undefined8 uParm1,undefined8 uParm2,uint uParm3,uint uParm4);
void FUN_0041371e(undefined8 uParm1,long lParm2,undefined8 uParm3,undefined8 uParm4,undefined8 *puParm5,undefined8 uParm6);
void FUN_00413b82(undefined8 uParm1,undefined8 uParm2,undefined8 uParm3,undefined8 uParm4,uint *puParm5);
void FUN_00413c54(undefined4 uParm1,undefined4 uParm2,undefined4 uParm3,undefined4 uParm4,undefined4 uParm5,undefined4 uParm6,undefined4 uParm7,undefined4 uParm8,undefined8 param_1,undefined8 param_2,undefined8 param_3,undefined8 param_4,undefined8 param_5,undefined8 param_6);
void FUN_00413d0a(ulong uParm1,ulong uParm2);
void FUN_00413d4b(long lParm1,ulong *puParm2,ulong uParm3);
void FUN_00413e16(undefined8 uParm1);
long FUN_00413e30(long lParm1);
long FUN_00413e65(long lParm1,long lParm2);
long FUN_00413ec6(ulong uParm1,ulong uParm2);
void FUN_00413f18(undefined8 uParm1,undefined8 uParm2);
void FUN_00413f4c(void);
void FUN_00413f76(uint uParm1);
ulong FUN_0041402d(undefined8 uParm1,long lParm2,undefined8 uParm3,long lParm4);
undefined8 FUN_004140a0(undefined8 uParm1);
undefined8 FUN_00414111(int iParm1);
ulong FUN_00414137(ulong *puParm1,int iParm2);
ulong FUN_00414196(undefined8 uParm1,uint uParm2,int iParm3);
ulong FUN_004141d7(byte *pbParm1,byte **ppbParm2,uint uParm3,undefined8 *puParm4,long lParm5);
void FUN_004145b8(uint uParm1,int iParm2,undefined uParm3,long lParm4,undefined8 uParm5,uint uParm6);
void FUN_00414687(uint uParm1,uint uParm2,char cParm3,undefined8 uParm4,undefined8 uParm5);
undefined8 FUN_004146cd(int iParm1);
ulong FUN_004146f3(ulong *puParm1,int iParm2);
ulong FUN_00414752(undefined8 uParm1,uint uParm2,int iParm3);
ulong FUN_00414793(byte *pbParm1,byte **ppbParm2,uint uParm3,undefined8 *puParm4,long lParm5);
undefined8 FUN_00414b74(ulong uParm1,ulong uParm2);
ulong FUN_00414bf3(uint uParm1);
void FUN_00414c1c(void);
void FUN_00414c50(uint uParm1);
void FUN_00414cc4(uint uParm1,uint uParm2,undefined8 uParm3,undefined8 uParm4);
void FUN_00414d48(undefined4 uParm1,undefined4 uParm2,undefined4 uParm3,undefined4 uParm4,undefined4 uParm5,undefined4 uParm6,undefined4 uParm7,undefined4 uParm8,uint param_1,uint param_2,undefined8 param_3,undefined8 param_4,undefined8 param_5,undefined8 param_6);
void FUN_00414e2c(undefined8 uParm1,undefined8 uParm2);
ulong FUN_00414e53(undefined8 uParm1);
ulong FUN_00414f0b(undefined4 uParm1,undefined4 uParm2,undefined4 uParm3,undefined4 uParm4,undefined4 uParm5,undefined4 uParm6,undefined4 uParm7,undefined4 uParm8,uint param_1,uint param_2,undefined8 param_3,undefined8 param_4,undefined8 param_5,undefined8 param_6);
void FUN_004151c2(undefined8 uParm1);
void FUN_004151e6(void);
ulong FUN_004151f4(long lParm1);
undefined8 FUN_004152c4(undefined8 uParm1);
void FUN_004152e3(undefined8 uParm1,undefined8 uParm2,uint uParm3);
void FUN_0041530e(long lParm1,int *piParm2);
ulong FUN_004154f2(int param_1,undefined8 *param_2,char *param_3,long *param_4,int *param_5,int param_6,int *param_7,int param_8,undefined8 param_9);
char * FUN_00415adc(undefined8 uParm1,undefined8 uParm2,char *pcParm3,int *piParm4,int iParm5);
ulong FUN_00415baa(uint param_1,undefined8 *param_2,char *param_3,long param_4,undefined8 param_5,uint param_6,uint *param_7,uint param_8);
ulong FUN_00416373(uint param_1,undefined8 param_2,undefined8 param_3,undefined8 param_4,undefined8 param_5,uint param_6,uint param_7);
void FUN_00416403(uint uParm1,undefined8 uParm2,undefined8 uParm3,undefined8 uParm4,undefined8 uParm5);
ulong FUN_0041644d(undefined8 uParm1,undefined8 uParm2);
undefined8 FUN_004164f4(long lParm1);
ulong FUN_00416525(uint *puParm1,byte *pbParm2,long lParm3,undefined8 uParm4);
ulong * FUN_004165a8(ulong *puParm1,char cParm2,ulong uParm3);
ulong FUN_004166ba(long *plParm1,long *plParm2);
void FUN_00416799(long lParm1,undefined8 uParm2);
void FUN_004167e9(long lParm1,undefined8 uParm2);
undefined8 FUN_00416839(long *plParm1,long lParm2,long lParm3);
void FUN_0041695e(long lParm1,undefined8 uParm2,undefined8 uParm3,undefined8 uParm4,undefined8 uParm5);
void FUN_004169b9(ulong *puParm1,long lParm2);
undefined8 FUN_00416bae(void);
undefined8 FUN_00416bdd(long lParm1,long lParm2);
void FUN_00416c46(undefined8 uParm1,undefined8 uParm2);
ulong FUN_00416c6b(long lParm1,long lParm2);
ulong FUN_00416cfb(long lParm1);
ulong FUN_00416e67(undefined8 uParm1,undefined8 uParm2,undefined8 uParm3);
void FUN_00416f88(uint uParm1);
ulong FUN_00416fd0(ulong uParm1,undefined4 uParm2);
undefined8 FUN_00416fec(int iParm1);
undefined8 FUN_00417020(int iParm1);
ulong FUN_0041704a(int iParm1);
undefined8 FUN_0041706a(int iParm1);
ulong FUN_00417091(uint uParm1);
ulong FUN_004170b0(byte *pbParm1,byte *pbParm2);
undefined8 FUN_00417127(undefined8 uParm1);
void FUN_004171b2(undefined auParm1 [16]);
ulong FUN_00417314(uint uParm1,uint uParm2);
void FUN_0041736c(uint uParm1,uint uParm2);
long FUN_004173aa(undefined8 uParm1,undefined8 uParm2);
undefined8 FUN_00417496(void);
undefined1 * FUN_004174a3(void);
char * FUN_00417856(void);
ulong FUN_00417924(long lParm1,long lParm2,long lParm3,long lParm4);
undefined8 FUN_004179f5(undefined8 uParm1,long lParm2,undefined8 uParm3,long lParm4);
ulong FUN_00417a5e(uint uParm1);
void FUN_00417a6e(undefined8 uParm1,undefined8 uParm2);
void FUN_00417a94(uint uParm1);
undefined8 FUN_00417aba(undefined8 uParm1);
undefined8 FUN_00417b80(uint uParm1,undefined8 uParm2);
void FUN_00417d99(undefined8 uParm1,undefined8 uParm2);
ulong FUN_00417dbe(long lParm1,long lParm2);
ulong FUN_00417e75(void);
long FUN_00417ec2(ulong uParm1,long lParm2,ulong uParm3,long lParm4,long *plParm5);
int * FUN_0041810e(ulong uParm1,uint *puParm2,ulong uParm3,uint *puParm4,ulong *puParm5);
char * FUN_00418bee(ulong uParm1,long lParm2,long lParm3);
long FUN_00418e5c(int *param_1,long *param_2);
long FUN_00419047(undefined auParm1 [16],int *piParm2,long *plParm3);
undefined8 FUN_00419406(int iParm1,long lParm2,uint *puParm3,long lParm4,uint uParm5);
void FUN_00419c19(uint param_1);
void FUN_00419c63(undefined8 uParm1,uint uParm2);
ulong FUN_00419cb5(void);
ulong FUN_0041a047(undefined auParm1 [16],undefined8 uParm2,undefined8 uParm3);
ulong FUN_0041a41f(char *pcParm1,long lParm2);
undefined8 FUN_0041a478(long param_1,long param_2,undefined param_3,int param_4,undefined8 param_5,ulong param_6,int param_7,ulong param_8,int param_9);
void FUN_00421ba5(undefined8 uParm1,int *piParm2,undefined8 uParm3);
void FUN_00421c46(int *param_1);
ulong FUN_00421d05(ulong uParm1,long lParm2);
void FUN_00421d39(undefined8 uParm1,undefined8 uParm2,undefined8 uParm3);
void FUN_00421d74(undefined8 uParm1,undefined8 uParm2,undefined8 uParm3,undefined8 uParm4);
ulong FUN_00421dc5(ulong uParm1,ulong uParm2);
undefined8 FUN_00421de0(uint *puParm1,ulong *puParm2);
undefined8 FUN_00422580(char *pcParm1,ulong *puParm2,char **ppcParm3);
ulong FUN_00423836(undefined auParm1 [16]);
ulong FUN_00423871(void);
void FUN_004238b0(void);
undefined8 _DT_FINI(void);
undefined FUN_0062e000();
undefined FUN_0062e008();
undefined FUN_0062e010();
undefined FUN_0062e018();
undefined FUN_0062e020();
undefined FUN_0062e028();
undefined FUN_0062e030();
undefined FUN_0062e038();
undefined FUN_0062e040();
undefined FUN_0062e048();
undefined FUN_0062e050();
undefined FUN_0062e058();
undefined FUN_0062e060();
undefined FUN_0062e068();
undefined FUN_0062e070();
undefined FUN_0062e078();
undefined FUN_0062e080();
undefined FUN_0062e088();
undefined FUN_0062e090();
undefined FUN_0062e098();
undefined FUN_0062e0a0();
undefined FUN_0062e0a8();
undefined FUN_0062e0b0();
undefined FUN_0062e0b8();
undefined FUN_0062e0c0();
undefined FUN_0062e0c8();
undefined FUN_0062e0d0();
undefined FUN_0062e0d8();
undefined FUN_0062e0e0();
undefined FUN_0062e0e8();
undefined FUN_0062e0f0();
undefined FUN_0062e0f8();
undefined FUN_0062e100();
undefined FUN_0062e108();
undefined FUN_0062e110();
undefined FUN_0062e118();
undefined FUN_0062e120();
undefined FUN_0062e128();
undefined FUN_0062e130();
undefined FUN_0062e138();
undefined FUN_0062e140();
undefined FUN_0062e148();
undefined FUN_0062e150();
undefined FUN_0062e158();
undefined FUN_0062e160();
undefined FUN_0062e168();
undefined FUN_0062e170();
undefined FUN_0062e178();
undefined FUN_0062e180();
undefined FUN_0062e188();
undefined FUN_0062e190();
undefined FUN_0062e198();
undefined FUN_0062e1a0();
undefined FUN_0062e1a8();
undefined FUN_0062e1b0();
undefined FUN_0062e1b8();
undefined FUN_0062e1c0();
undefined FUN_0062e1c8();
undefined FUN_0062e1d0();
undefined FUN_0062e1d8();
undefined FUN_0062e1e0();
undefined FUN_0062e1e8();
undefined FUN_0062e1f0();
undefined FUN_0062e1f8();
undefined FUN_0062e200();
undefined FUN_0062e208();
undefined FUN_0062e210();
undefined FUN_0062e218();
undefined FUN_0062e220();
undefined FUN_0062e228();
undefined FUN_0062e230();
undefined FUN_0062e238();
undefined FUN_0062e240();
undefined FUN_0062e248();
undefined FUN_0062e250();
undefined FUN_0062e258();
undefined FUN_0062e260();
undefined FUN_0062e268();
undefined FUN_0062e270();
undefined FUN_0062e278();
undefined FUN_0062e280();
undefined FUN_0062e288();
undefined FUN_0062e290();
undefined FUN_0062e298();
undefined FUN_0062e2a0();
undefined FUN_0062e2a8();
undefined FUN_0062e2b0();
undefined FUN_0062e2b8();
undefined FUN_0062e2c0();
undefined FUN_0062e2c8();
undefined FUN_0062e2d0();
undefined FUN_0062e2d8();
undefined FUN_0062e2e0();
undefined FUN_0062e2e8();
undefined FUN_0062e2f0();
undefined FUN_0062e2f8();
undefined FUN_0062e300();
undefined FUN_0062e308();
undefined FUN_0062e310();
undefined FUN_0062e318();
undefined FUN_0062e320();
undefined FUN_0062e328();
undefined FUN_0062e330();
undefined FUN_0062e338();
undefined FUN_0062e340();
undefined FUN_0062e348();
undefined FUN_0062e350();
undefined FUN_0062e358();
undefined FUN_0062e360();
undefined FUN_0062e368();
undefined FUN_0062e370();
undefined FUN_0062e378();
undefined FUN_0062e380();
undefined FUN_0062e388();
undefined FUN_0062e390();
undefined FUN_0062e398();
undefined FUN_0062e3a0();
undefined FUN_0062e3a8();
undefined FUN_0062e3b0();
undefined FUN_0062e3b8();
undefined FUN_0062e3c0();
undefined FUN_0062e3c8();
undefined FUN_0062e3d0();
undefined FUN_0062e3d8();
undefined FUN_0062e3e0();
undefined FUN_0062e3e8();
undefined FUN_0062e3f0();
undefined FUN_0062e3f8();
undefined FUN_0062e400();
undefined FUN_0062e408();
undefined FUN_0062e410();
undefined FUN_0062e418();
undefined FUN_0062e420();
undefined FUN_0062e428();


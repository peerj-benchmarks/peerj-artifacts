
#include "expand.h"

long null_ARRAY_006156d_0_8_;
long null_ARRAY_006156d_8_8_;
long null_ARRAY_0061588_0_8_;
long null_ARRAY_0061588_16_8_;
long null_ARRAY_0061588_24_8_;
long null_ARRAY_0061588_32_8_;
long null_ARRAY_0061588_40_8_;
long null_ARRAY_0061588_48_8_;
long null_ARRAY_0061588_8_8_;
long null_ARRAY_006159c_0_4_;
long null_ARRAY_006159c_16_8_;
long null_ARRAY_006159c_4_4_;
long null_ARRAY_006159c_8_4_;
long DAT_00412483;
long DAT_0041253d;
long DAT_004125bb;
long DAT_00412888;
long DAT_00412a1b;
long DAT_00412a1e;
long DAT_00412bef;
long DAT_00412c38;
long DAT_00412d5e;
long DAT_00412d62;
long DAT_00412d67;
long DAT_00412d69;
long DAT_004133d3;
long DAT_004137a0;
long DAT_004137b8;
long DAT_004137bd;
long DAT_00413827;
long DAT_004138b8;
long DAT_004138bb;
long DAT_00413909;
long DAT_0041390d;
long DAT_00413980;
long DAT_00413981;
long DAT_006152b0;
long DAT_006152c0;
long DAT_006152d0;
long DAT_006156b8;
long DAT_006156c0;
long DAT_00615738;
long DAT_0061573c;
long DAT_00615740;
long DAT_00615780;
long DAT_00615788;
long DAT_00615790;
long DAT_006157a0;
long DAT_006157a8;
long DAT_006157c0;
long DAT_006157c8;
long DAT_00615810;
long DAT_00615814;
long DAT_00615818;
long DAT_00615820;
long DAT_00615828;
long DAT_00615830;
long DAT_00615838;
long DAT_00615840;
long DAT_00615848;
long DAT_00615850;
long DAT_00615858;
long DAT_00615860;
long DAT_00615868;
long DAT_00615870;
long DAT_006159f8;
long DAT_00615a00;
long DAT_00615a08;
long DAT_00615a10;
long DAT_00615a20;
long fde_00414688;
long FLOAT_UNKNOWN;
long null_ARRAY_00412600;
long null_ARRAY_00412640;
long null_ARRAY_00413dc0;
long null_ARRAY_006156a0;
long null_ARRAY_006156d0;
long null_ARRAY_00615700;
long null_ARRAY_006157e0;
long null_ARRAY_00615880;
long null_ARRAY_006158c0;
long null_ARRAY_006159c0;
long PTR_DAT_006156b0;
long PTR_null_ARRAY_006156e0;
long stack0x00000008;
void
FUN_00401e1e (uint uParm1)
{
  int iVar1;
  uint *puVar2;
  byte bVar3;
  char cStack73;
  long lStack72;
  ulong uStack64;
  ulong uStack56;
  bool bStack45;
  uint uStack44;
  long lStack40;
  code *pcStack32;
  uint local_c;

  if (uParm1 == 0)
    {
      pcStack32 = (code *) 0x401e6d;
      local_c = uParm1;
      func_0x00401650 ("Usage: %s [OPTION]... [FILE]...\n", DAT_00615870);
      pcStack32 = (code *) 0x401e81;
      func_0x00401800
	("Convert tabs in each FILE to spaces, writing to standard output.\n",
	 DAT_00615780);
      pcStack32 = (code *) 0x401e86;
      FUN_00401c4e ();
      pcStack32 = (code *) 0x401e8b;
      FUN_00401c68 ();
      pcStack32 = (code *) 0x401e9f;
      func_0x00401800
	("  -i, --initial    do not convert tabs after non blanks\n  -t, --tabs=N     have tabs N characters apart, not 8\n",
	 DAT_00615780);
      pcStack32 = (code *) 0x401ea4;
      FUN_00402c43 ();
      pcStack32 = (code *) 0x401eb8;
      func_0x00401800 ("      --help     display this help and exit\n",
		       DAT_00615780);
      pcStack32 = (code *) 0x401ecc;
      func_0x00401800
	("      --version  output version information and exit\n",
	 DAT_00615780);
      pcStack32 = (code *) 0x401ed6;
      FUN_00401c82 ("expand");
    }
  else
    {
      pcStack32 = (code *) 0x401e4f;
      local_c = uParm1;
      func_0x004017f0 (DAT_006157a0,
		       "Try \'%s --help\' for more information.\n",
		       DAT_00615870);
    }
  pcStack32 = FUN_00401ee0;
  func_0x004019b0 ((ulong) local_c);
  pcStack32 = (code *) & stack0xfffffffffffffff8;
  lStack40 = FUN_00402a11 (0);
  if (lStack40 == 0)
    {
      return;
    }
  do
    {
      bStack45 = true;
      uStack56 = 0;
      lStack72 = 0;
    LAB_00401f19:
      ;
      do
	{
	  uStack44 = func_0x004016b0 (lStack40);
	  if ((int) uStack44 < 0)
	    {
	      lStack40 = FUN_00402a11 (lStack40);
	      if (lStack40 != 0)
		goto LAB_00401f19;
	    }
	  if (bStack45 != false)
	    {
	      if (uStack44 == 9)
		{
		  imperfection_wrapper ();	//          uStack64 = FUN_00402897(uStack56, &lStack72, &cStack73, &lStack72);
		  if (cStack73 != '\0')
		    {
		      uStack64 = uStack56 + 1;
		    }
		  if (uStack64 < uStack56)
		    {
		      imperfection_wrapper ();	//            FUN_00404c84(1, 0, "input line is too long");
		    }
		  while (uStack56 = uStack56 + 1, uStack56 < uStack64)
		    {
		      iVar1 = func_0x00401970 (0x20);
		      if (iVar1 < 0)
			{
			  puVar2 = (uint *) func_0x004019a0 ();
			  imperfection_wrapper ();	//              FUN_00404c84(1, (ulong)*puVar2, "write error");
			}
		    }
		  uStack44 = 0x20;
		}
	      else
		{
		  if (uStack44 == 8)
		    {
		      uStack56 = uStack56 - (ulong) (uStack56 != 0);
		      lStack72 = lStack72 - (ulong) (lStack72 != 0);
		    }
		  else
		    {
		      uStack56 = uStack56 + 1;
		      if (uStack56 == 0)
			{
			  imperfection_wrapper ();	//              FUN_00404c84(1, 0, "input line is too long");
			}
		    }
		}
	      if ((DAT_00615810 == '\0')
		  && (iVar1 = func_0x004016e0 ((ulong) uStack44), iVar1 == 0))
		{
		  bVar3 = 0;
		}
	      else
		{
		  bVar3 = 1;
		}
	      bStack45 = (bStack45 & bVar3) != 0;
	    }
	  if ((int) uStack44 < 0)
	    {
	      return;
	    }
	  iVar1 = func_0x00401970 ((ulong) uStack44);
	  if (iVar1 < 0)
	    {
	      puVar2 = (uint *) func_0x004019a0 ();
	      imperfection_wrapper ();	//        FUN_00404c84(1, (ulong)*puVar2, "write error");
	    }
	}
      while (uStack44 != 10);
    }
  while (true);
}

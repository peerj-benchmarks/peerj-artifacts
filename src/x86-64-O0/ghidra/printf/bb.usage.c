
#include "printf.h"

long null_ARRAY_0061557_0_8_;
long null_ARRAY_0061557_8_8_;
long null_ARRAY_006156c_0_8_;
long null_ARRAY_006156c_16_8_;
long null_ARRAY_006156c_24_8_;
long null_ARRAY_006156c_32_8_;
long null_ARRAY_006156c_40_8_;
long null_ARRAY_006156c_48_8_;
long null_ARRAY_006156c_8_8_;
long local_1_0_4_;
long DAT_00411d40;
long DAT_00411dfd;
long DAT_00411e7b;
long DAT_004124ff;
long DAT_00412659;
long DAT_0041265c;
long DAT_00412af1;
long DAT_00412b42;
long DAT_00412b57;
long DAT_00412ba0;
long DAT_00412cde;
long DAT_00412ce2;
long DAT_00412ce7;
long DAT_00412ce9;
long DAT_00413403;
long DAT_004137c0;
long DAT_004137fd;
long DAT_00413802;
long DAT_00413815;
long DAT_00413888;
long DAT_00413889;
long DAT_00615140;
long DAT_00615150;
long DAT_00615160;
long DAT_00615548;
long DAT_00615560;
long DAT_00615600;
long DAT_00615610;
long DAT_00615620;
long DAT_00615628;
long DAT_00615640;
long DAT_00615648;
long DAT_00615690;
long DAT_00615694;
long DAT_00615698;
long DAT_006156a0;
long DAT_006156a8;
long DAT_006156b0;
long DAT_00615800;
long DAT_00615804;
long DAT_00615808;
long DAT_00615810;
long DAT_00615818;
long DAT_00615820;
long fde_00414570;
long FLOAT_UNKNOWN;
long null_ARRAY_00413cc0;
long null_ARRAY_00615570;
long null_ARRAY_006155a0;
long null_ARRAY_00615660;
long null_ARRAY_006156c0;
long null_ARRAY_00615700;
long PTR_DAT_00615540;
long PTR_null_ARRAY_00615580;
long stack0x00000008;
void
FUN_00401da9 (uint uParm1)
{
  int *piVar1;
  undefined8 uVar2;
  uint *puVar3;
  char *pcVar4;
  char *pcVar5;

  if (uParm1 == 0)
    {
      func_0x00401600 ("Usage: %s FORMAT [ARGUMENT]...\n  or:  %s OPTION\n",
		       DAT_006156b0, DAT_006156b0);
      func_0x004017f0
	("Print ARGUMENT(s) according to FORMAT, or execute according to OPTION:\n\n",
	 DAT_00615600);
      func_0x004017f0 ("      --help     display this help and exit\n",
		       DAT_00615600);
      func_0x004017f0
	("      --version  output version information and exit\n",
	 DAT_00615600);
      func_0x004017f0
	("\nFORMAT controls the output as in C printf.  Interpreted sequences are:\n\n  \\\"      double quote\n",
	 DAT_00615600);
      func_0x004017f0
	("  \\\\      backslash\n  \\a      alert (BEL)\n  \\b      backspace\n  \\c      produce no further output\n  \\e      escape\n  \\f      form feed\n  \\n      new line\n  \\r      carriage return\n  \\t      horizontal tab\n  \\v      vertical tab\n",
	 DAT_00615600);
      func_0x004017f0
	("  \\NNN    byte with octal value NNN (1 to 3 digits)\n  \\xHH    byte with hexadecimal value HH (1 to 2 digits)\n  \\uHHHH  Unicode (ISO/IEC 10646) character with hex value HHHH (4 digits)\n  \\UHHHHHHHH  Unicode character with hex value HHHHHHHH (8 digits)\n",
	 DAT_00615600);
      func_0x004017f0
	("  %%      a single %\n  %b      ARGUMENT as a string with \'\\\' escapes interpreted,\n          except that octal escapes are of the form \\0 or \\0NNN\n  %q      ARGUMENT is printed in a format that can be reused as shell input,\n          escaping non-printable characters with the proposed POSIX $\'\' syntax.\n\nand all C format specifications ending with one of diouxXfeEgGcs, with\nARGUMENTs converted to proper type first.  Variable widths are handled.\n",
	 DAT_00615600);
      pcVar4 = "printf";
      func_0x00401600
	("\nNOTE: your shell may have its own version of %s, which usually supersedes\nthe version described here.  Please refer to your shell\'s documentation\nfor details about the options it supports.\n");
      imperfection_wrapper ();	//    FUN_00401c0d();
    }
  else
    {
      pcVar4 = "Try \'%s --help\' for more information.\n";
      func_0x004017e0 (DAT_00615620,
		       "Try \'%s --help\' for more information.\n",
		       DAT_006156b0);
    }
  pcVar5 = (char *) (ulong) uParm1;
  func_0x00401970 ();
  piVar1 = (int *) func_0x00401960 ();
  if (*piVar1 == 0)
    {
      if (*pcVar4 != '\0')
	{
	  if (pcVar5 == pcVar4)
	    {
	      imperfection_wrapper ();	//        uVar2 = FUN_00404923(pcVar5);
	      imperfection_wrapper ();	//        FUN_004056d4(0, 0, "%s: expected a numeric value", uVar2);
	    }
	  else
	    {
	      imperfection_wrapper ();	//        uVar2 = FUN_00404923(pcVar5);
	      imperfection_wrapper ();	//        FUN_004056d4(0, 0, "%s: value not completely converted", uVar2);
	    }
	  DAT_00615690 = 1;
	}
    }
  else
    {
      imperfection_wrapper ();	//    uVar2 = FUN_00404923(pcVar5);
      puVar3 = (uint *) func_0x00401960 ();
      imperfection_wrapper ();	//    FUN_004056d4(0, (ulong)*puVar3, &DAT_004124ff, uVar2);
      DAT_00615690 = 1;
    }
  return;
}

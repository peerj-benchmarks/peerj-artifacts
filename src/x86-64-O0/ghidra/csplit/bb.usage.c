
#include "csplit.h"

long null_ARRAY_006314f_0_8_;
long null_ARRAY_006314f_8_8_;
long null_ARRAY_0063170_0_8_;
long null_ARRAY_0063170_104_8_;
long null_ARRAY_0063170_112_8_;
long null_ARRAY_0063170_120_8_;
long null_ARRAY_0063170_16_8_;
long null_ARRAY_0063170_24_8_;
long null_ARRAY_0063170_32_8_;
long null_ARRAY_0063170_40_8_;
long null_ARRAY_0063170_48_8_;
long null_ARRAY_0063170_56_8_;
long null_ARRAY_0063170_64_8_;
long null_ARRAY_0063170_72_8_;
long null_ARRAY_0063170_80_8_;
long null_ARRAY_0063170_8_8_;
long null_ARRAY_0063170_88_8_;
long null_ARRAY_0063170_96_8_;
long null_ARRAY_006317c_0_8_;
long null_ARRAY_006317c_16_8_;
long null_ARRAY_006317c_24_8_;
long null_ARRAY_006317c_32_8_;
long null_ARRAY_006317c_40_8_;
long null_ARRAY_006317c_48_8_;
long null_ARRAY_006317c_8_8_;
long null_ARRAY_0063192_0_4_;
long null_ARRAY_0063192_16_8_;
long null_ARRAY_0063192_4_4_;
long null_ARRAY_0063192_8_4_;
long local_a_0_4_;
long DAT_0042a7cb;
long DAT_0042a885;
long DAT_0042a903;
long DAT_0042ab31;
long DAT_0042ab5e;
long DAT_0042ab60;
long DAT_0042abd4;
long DAT_0042ac13;
long DAT_0042ac18;
long DAT_0042ac2d;
long DAT_0042aee6;
long DAT_0042b4a4;
long DAT_0042b4e8;
long DAT_0042b61e;
long DAT_0042b622;
long DAT_0042b627;
long DAT_0042b629;
long DAT_0042bc93;
long DAT_0042c758;
long DAT_0042c75d;
long DAT_0042c7c7;
long DAT_0042c858;
long DAT_0042c85b;
long DAT_0042c8a9;
long DAT_0042cb00;
long DAT_0042d2b0;
long DAT_0042d2b8;
long DAT_0042d3e8;
long DAT_0042db70;
long DAT_0042dbe0;
long DAT_0042dbe1;
long DAT_00631000;
long DAT_00631010;
long DAT_00631020;
long DAT_006314c0;
long DAT_006314d0;
long DAT_006314e0;
long DAT_00631558;
long DAT_0063155c;
long DAT_00631560;
long DAT_00631580;
long DAT_00631590;
long DAT_006315a0;
long DAT_006315a8;
long DAT_006315c0;
long DAT_006315c8;
long DAT_00631640;
long DAT_00631648;
long DAT_00631650;
long DAT_00631658;
long DAT_00631660;
long DAT_00631668;
long DAT_00631670;
long DAT_00631678;
long DAT_00631680;
long DAT_00631688;
long DAT_00631690;
long DAT_00631698;
long DAT_006316a0;
long DAT_006316a8;
long DAT_006316b0;
long DAT_006316b1;
long DAT_006316b2;
long DAT_006316b3;
long DAT_006316b8;
long DAT_006316c0;
long DAT_00631780;
long DAT_00631788;
long DAT_00631790;
long DAT_00631798;
long DAT_006317a0;
long DAT_006317a8;
long DAT_00631900;
long DAT_00631958;
long DAT_00631960;
long DAT_00631968;
long DAT_00631978;
long DAT_00631980;
long fde_0042e920;
long FLOAT_UNKNOWN;
long null_ARRAY_0042a9c0;
long null_ARRAY_0042b460;
long null_ARRAY_0042ca40;
long null_ARRAY_0042cae0;
long null_ARRAY_0042d980;
long null_ARRAY_006314f0;
long null_ARRAY_00631520;
long null_ARRAY_006315e0;
long null_ARRAY_00631700;
long null_ARRAY_006317c0;
long null_ARRAY_00631800;
long null_ARRAY_00631920;
long PTR_DAT_006314c8;
long PTR_null_ARRAY_00631500;
long stack0x00000008;
void
FUN_00404ab7 (uint uParm1)
{
  int iVar1;
  int *piVar2;
  undefined8 uVar3;
  uint *puVar4;

  if (uParm1 == 0)
    {
      func_0x00401c10 ("Usage: %s [OPTION]... FILE PATTERN...\n",
		       DAT_006317a8);
      func_0x00401e50
	("Output pieces of FILE separated by PATTERN(s) to files \'xx00\', \'xx01\', ...,\nand output byte counts of each piece to standard output.\n",
	 DAT_00631580);
      func_0x00401e50 ("\nRead standard input if FILE is -\n", DAT_00631580);
      FUN_0040238e ();
      func_0x00401e50
	("  -b, --suffix-format=FORMAT  use sprintf FORMAT instead of %02d\n  -f, --prefix=PREFIX        use PREFIX instead of \'xx\'\n  -k, --keep-files           do not remove output files on errors\n",
	 DAT_00631580);
      func_0x00401e50
	("      --suppress-matched     suppress the lines matching PATTERN\n",
	 DAT_00631580);
      func_0x00401e50
	("  -n, --digits=DIGITS        use specified number of digits instead of 2\n  -s, --quiet, --silent      do not print counts of output file sizes\n  -z, --elide-empty-files    remove empty output files\n",
	 DAT_00631580);
      func_0x00401e50 ("      --help     display this help and exit\n",
		       DAT_00631580);
      func_0x00401e50
	("      --version  output version information and exit\n",
	 DAT_00631580);
      func_0x00401e50
	("\nEach PATTERN may be:\n  INTEGER            copy up to but not including specified line number\n  /REGEXP/[OFFSET]   copy up to but not including a matching line\n  %REGEXP%[OFFSET]   skip to, but not including a matching line\n  {INTEGER}          repeat the previous pattern specified number of times\n  {*}                repeat the previous pattern as many times as possible\n\nA line OFFSET is a required \'+\' or \'-\' followed by a positive integer.\n",
	 DAT_00631580);
      FUN_004023a8 ("csplit");
    }
  else
    {
      func_0x00401e40 (DAT_006315a0,
		       "Try \'%s --help\' for more information.\n",
		       DAT_006317a8);
    }
  func_0x004020a0 ((ulong) uParm1);
  iVar1 = FUN_00428279 (DAT_00631580);
  if (iVar1 != 0)
    {
      if (DAT_006317a0 == '\x01')
	{
	  piVar2 = (int *) func_0x00402090 ();
	  if (*piVar2 == 0x20)
	    goto LAB_00404c7d;
	}
      if (DAT_00631798 == 0)
	{
	  puVar4 = (uint *) func_0x00402090 ();
	  imperfection_wrapper ();	//      FUN_00407946(0, (ulong)*puVar4, &DAT_0042b4a4, "write error");
	}
      else
	{
	  imperfection_wrapper ();	//      uVar3 = FUN_0040636d(DAT_00631798);
	  puVar4 = (uint *) func_0x00402090 ();
	  imperfection_wrapper ();	//      FUN_00407946(0, (ulong)*puVar4, "%s: %s", uVar3, "write error");
	}
      func_0x00402110 ((ulong) DAT_006314d0);
    }
LAB_00404c7d:
  ;
  iVar1 = FUN_00428279 (DAT_006315a0);
  if (iVar1 != 0)
    {
      func_0x00402110 ((ulong) DAT_006314d0);
    }
  return;
}

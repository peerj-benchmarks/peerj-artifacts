typedef unsigned char   undefined;

typedef unsigned char    byte;
typedef unsigned char    dwfenc;
typedef unsigned int    dword;
typedef unsigned long    qword;
typedef unsigned char    undefined1;
typedef unsigned short    undefined2;
typedef unsigned int    undefined4;
typedef unsigned long    undefined6;
typedef unsigned long    undefined8;
typedef unsigned short    word;
#define unkbyte9   unsigned long long
#define unkbyte10   unsigned long long
#define unkbyte11   unsigned long long
#define unkbyte12   unsigned long long
#define unkbyte13   unsigned long long
#define unkbyte14   unsigned long long
#define unkbyte15   unsigned long long
#define unkbyte16   unsigned long long

#define unkuint9   unsigned long long
#define unkuint10   unsigned long long
#define unkuint11   unsigned long long
#define unkuint12   unsigned long long
#define unkuint13   unsigned long long
#define unkuint14   unsigned long long
#define unkuint15   unsigned long long
#define unkuint16   unsigned long long

#define unkint9   long long
#define unkint10   long long
#define unkint11   long long
#define unkint12   long long
#define unkint13   long long
#define unkint14   long long
#define unkint15   long long
#define unkint16   long long

#define unkfloat1   float
#define unkfloat2   float
#define unkfloat3   float
#define unkfloat5   double
#define unkfloat6   double
#define unkfloat7   double
#define unkfloat9   long double
#define unkfloat11   long double
#define unkfloat12   long double
#define unkfloat13   long double
#define unkfloat14   long double
#define unkfloat15   long double
#define unkfloat16   long double

#define BADSPACEBASE   void
#define code   void

#define ulong long
#define true 1
#define bool int
#define false 0
#define float10 int
#define ushort int
#define undefined7 long
#define undefined2 long
#define uint5 long
#define uint6 long

// Add definition for uint
typedef unsigned int    uint;

#define sbyte char

extern imperfection_wrapper(void);
typedef struct eh_frame_hdr eh_frame_hdr, *Peh_frame_hdr;

struct eh_frame_hdr {
    byte eh_frame_hdr_version; // Exception Handler Frame Header Version
    dwfenc eh_frame_pointer_encoding; // Exception Handler Frame Pointer Encoding
    dwfenc eh_frame_desc_entry_count_encoding; // Encoding of # of Exception Handler FDEs
    dwfenc eh_frame_table_encoding; // Exception Handler Table Encoding
};

typedef struct fde_table_entry fde_table_entry, *Pfde_table_entry;

struct fde_table_entry {
    dword initial_loc; // Initial Location
    dword data_loc; // Data location
};

typedef enum Elf64_DynTag {
    DT_AUDIT=1879047932,
    DT_AUXILIARY=2147483645,
    DT_BIND_NOW=24,
    DT_CHECKSUM=1879047672,
    DT_CONFIG=1879047930,
    DT_DEBUG=21,
    DT_DEPAUDIT=1879047931,
    DT_ENCODING=32,
    DT_FEATURE_1=1879047676,
    DT_FILTER=2147483647,
    DT_FINI=13,
    DT_FINI_ARRAY=26,
    DT_FINI_ARRAYSZ=28,
    DT_FLAGS=30,
    DT_FLAGS_1=1879048187,
    DT_GNU_CONFLICT=1879047928,
    DT_GNU_CONFLICTSZ=1879047670,
    DT_GNU_HASH=1879047925,
    DT_GNU_LIBLIST=1879047929,
    DT_GNU_LIBLISTSZ=1879047671,
    DT_GNU_PRELINKED=1879047669,
    DT_HASH=4,
    DT_INIT=12,
    DT_INIT_ARRAY=25,
    DT_INIT_ARRAYSZ=27,
    DT_JMPREL=23,
    DT_MOVEENT=1879047674,
    DT_MOVESZ=1879047675,
    DT_MOVETAB=1879047934,
    DT_NEEDED=1,
    DT_NULL=0,
    DT_PLTGOT=3,
    DT_PLTPAD=1879047933,
    DT_PLTPADSZ=1879047673,
    DT_PLTREL=20,
    DT_PLTRELSZ=2,
    DT_POSFLAG_1=1879047677,
    DT_PREINIT_ARRAYSZ=33,
    DT_REL=17,
    DT_RELA=7,
    DT_RELACOUNT=1879048185,
    DT_RELAENT=9,
    DT_RELASZ=8,
    DT_RELCOUNT=1879048186,
    DT_RELENT=19,
    DT_RELSZ=18,
    DT_RPATH=15,
    DT_RUNPATH=29,
    DT_SONAME=14,
    DT_STRSZ=10,
    DT_STRTAB=5,
    DT_SYMBOLIC=16,
    DT_SYMENT=11,
    DT_SYMINENT=1879047679,
    DT_SYMINFO=1879047935,
    DT_SYMINSZ=1879047678,
    DT_SYMTAB=6,
    DT_TEXTREL=22,
    DT_TLSDESC_GOT=1879047927,
    DT_TLSDESC_PLT=1879047926,
    DT_VERDEF=1879048188,
    DT_VERDEFNUM=1879048189,
    DT_VERNEED=1879048190,
    DT_VERNEEDNUM=1879048191,
    DT_VERSYM=1879048176
} Elf64_DynTag;

typedef enum Elf_ProgramHeaderType {
    PT_DYNAMIC=2,
    PT_GNU_EH_FRAME=1685382480,
    PT_GNU_RELRO=1685382482,
    PT_GNU_STACK=1685382481,
    PT_INTERP=3,
    PT_LOAD=1,
    PT_NOTE=4,
    PT_NULL=0,
    PT_PHDR=6,
    PT_SHLIB=5,
    PT_TLS=7
} Elf_ProgramHeaderType;

typedef struct Elf64_Dyn Elf64_Dyn, *PElf64_Dyn;

struct Elf64_Dyn {
    enum Elf64_DynTag d_tag;
    qword d_val;
};

typedef struct Elf64_Shdr Elf64_Shdr, *PElf64_Shdr;

typedef enum Elf_SectionHeaderType {
    SHT_CHECKSUM=1879048184,
    SHT_DYNAMIC=6,
    SHT_DYNSYM=11,
    SHT_FINI_ARRAY=15,
    SHT_GNU_ATTRIBUTES=1879048181,
    SHT_GNU_HASH=1879048182,
    SHT_GNU_LIBLIST=1879048183,
    SHT_GNU_verdef=1879048189,
    SHT_GNU_verneed=1879048190,
    SHT_GNU_versym=1879048191,
    SHT_GROUP=17,
    SHT_HASH=5,
    SHT_INIT_ARRAY=14,
    SHT_NOBITS=8,
    SHT_NOTE=7,
    SHT_NULL=0,
    SHT_PREINIT_ARRAY=16,
    SHT_PROGBITS=1,
    SHT_REL=9,
    SHT_RELA=4,
    SHT_SHLIB=10,
    SHT_STRTAB=3,
    SHT_SUNW_COMDAT=1879048187,
    SHT_SUNW_move=1879048186,
    SHT_SUNW_syminfo=1879048188,
    SHT_SYMTAB=2,
    SHT_SYMTAB_SHNDX=18
} Elf_SectionHeaderType;

struct Elf64_Shdr {
    dword sh_name;
    enum Elf_SectionHeaderType sh_type;
    qword sh_flags;
    qword sh_addr;
    qword sh_offset;
    qword sh_size;
    dword sh_link;
    dword sh_info;
    qword sh_addralign;
    qword sh_entsize;
};

typedef struct Elf64_Phdr Elf64_Phdr, *PElf64_Phdr;

struct Elf64_Phdr {
    enum Elf_ProgramHeaderType p_type;
    dword p_flags;
    qword p_offset;
    qword p_vaddr;
    qword p_paddr;
    qword p_filesz;
    qword p_memsz;
    qword p_align;
};

typedef struct Elf64_Rela Elf64_Rela, *PElf64_Rela;

struct Elf64_Rela {
    qword r_offset; // location to apply the relocation action
    qword r_info; // the symbol table index and the type of relocation
    qword r_addend; // a constant addend used to compute the relocatable field value
};

typedef struct Elf64_Sym Elf64_Sym, *PElf64_Sym;

struct Elf64_Sym {
    dword st_name;
    byte st_info;
    byte st_other;
    word st_shndx;
    qword st_value;
    qword st_size;
};

typedef struct Elf64_Ehdr Elf64_Ehdr, *PElf64_Ehdr;

struct Elf64_Ehdr {
    byte e_ident_magic_num;
    char e_ident_magic_str[3];
    byte e_ident_class;
    byte e_ident_data;
    byte e_ident_version;
    byte e_ident_pad[9];
    word e_type;
    word e_machine;
    dword e_version;
    qword e_entry;
    qword e_phoff;
    qword e_shoff;
    dword e_flags;
    word e_ehsize;
    word e_phentsize;
    word e_phnum;
    word e_shentsize;
    word e_shnum;
    word e_shstrndx;
};




undefined8 _DT_INIT(void);
void FUN_00401bf0(void);
void entry(void);
void FUN_00402250(void);
void FUN_004022d0(void);
void FUN_00402350(void);
void FUN_0040238e(void);
void FUN_004023a8(undefined *puParm1);
void FUN_00402544(void);
void FUN_00402589(void);
void FUN_0040259c(void);
void FUN_004025c3(uint uParm1);
void FUN_004025f3(undefined8 uParm1,undefined8 uParm2);
long FUN_0040262a(undefined8 uParm1,long lParm2);
void FUN_0040269e(undefined8 *puParm1);
long FUN_004026cb(void);
void FUN_00402702(long lParm1,long lParm2,long lParm3);
long FUN_004027f7(long lParm1);
long * FUN_00402948(long lParm1);
long FUN_004029aa(ulong uParm1);
void FUN_00402a45(long lParm1);
void FUN_00402ab3(long lParm1);
ulong FUN_00402b24(void);
undefined8 FUN_00402cbd(void);
long * FUN_00402d03(void);
long FUN_00402e56(ulong uParm1);
ulong FUN_00402f74(void);
void FUN_00402f93(undefined8 uParm1);
void FUN_0040300f(ulong uParm1,char cParm2,int iParm3);
void FUN_00403104(void);
void FUN_0040312c(long lParm1,long lParm2);
void FUN_004031d4(long lParm1,long lParm2);
void FUN_004032b5(long lParm1,long lParm2,char cParm3);
void FUN_0040337a(long *plParm1,undefined8 uParm2);
void FUN_0040364e(void);
long FUN_004037ba(uint uParm1);
void FUN_0040386b(void);
void FUN_00403970(char cParm1);
void FUN_00403a1c(void);
void FUN_00403c0c(long *plParm1);
undefined8 * FUN_00403cb3(void);
void FUN_00403d54(undefined8 uParm1,undefined8 uParm2,undefined8 uParm3);
void FUN_00403db9(int iParm1,long lParm2,long lParm3);
long FUN_00403ec7(undefined4 uParm1,undefined uParm2,char *pcParm3);
void FUN_0040403e(int iParm1,uint uParm2,long lParm3);
long FUN_004042e3(long lParm1,uint *puParm2);
void FUN_00404351(byte *pbParm1,uint uParm2);
long FUN_00404465(char *pcParm1);
undefined8 FUN_0040459b(uint uParm1,undefined8 *puParm2);
void FUN_00404ab7(uint uParm1);
void FUN_00404bbf(void);
ulong FUN_00404ca4(uint uParm1,undefined8 uParm2,uint uParm3,uint uParm4);
long FUN_00404d20(undefined8 uParm1,undefined8 uParm2);
char * FUN_00404e0c(ulong uParm1,long lParm2);
void FUN_00404e91(long lParm1);
ulong FUN_00404f6e(undefined1 *puParm1,byte bParm2,uint uParm3);
undefined8 * FUN_00404ff6(undefined8 *puParm1,int iParm2);
char * FUN_0040506d(char *pcParm1,int iParm2);
ulong FUN_0040510d(long param_1,ulong param_2,long param_3,ulong param_4,uint param_5,uint param_6,long param_7,char *param_8,char *param_9);
undefined1 * FUN_00405fd7(int iParm1,undefined8 uParm2,undefined8 uParm3,uint *puParm4);
void FUN_0040624a(uint uParm1,uint uParm2,undefined8 uParm3);
void FUN_0040628b(uint uParm1,undefined8 uParm2);
void FUN_004062af(undefined8 uParm1,undefined8 uParm2,char cParm3);
void FUN_00406343(undefined8 uParm1,char cParm2);
void FUN_0040636d(undefined8 uParm1);
void FUN_0040638c(uint uParm1,uint uParm2,undefined8 uParm3);
void FUN_00406427(uint uParm1,undefined8 uParm2,undefined8 uParm3);
void FUN_00406453(uint uParm1,undefined8 uParm2);
void FUN_0040647c(undefined8 uParm1);
long FUN_0040649b(uint uParm1,undefined8 uParm2,undefined8 uParm3);
void FUN_0040650c(uint uParm1);
void FUN_00406532(undefined8 uParm1,long lParm2,undefined8 uParm3,undefined8 uParm4,undefined8 *puParm5,undefined8 uParm6);
void FUN_00406996(undefined8 uParm1,undefined8 uParm2,undefined8 uParm3,undefined8 uParm4,uint *puParm5);
void FUN_00406a68(undefined4 uParm1,undefined4 uParm2,undefined4 uParm3,undefined4 uParm4,undefined4 uParm5,undefined4 uParm6,undefined4 uParm7,undefined4 uParm8,undefined8 param_1,undefined8 param_2,undefined8 param_3,undefined8 param_4,undefined8 param_5,undefined8 param_6);
void FUN_00406b1e(long lParm1,ulong *puParm2,ulong uParm3);
void FUN_00406be9(undefined8 uParm1);
long FUN_00406c03(long lParm1);
long FUN_00406c38(long lParm1,long lParm2);
void FUN_00406c99(undefined8 uParm1,undefined8 uParm2);
long FUN_00406ccd(undefined8 param_1,uint param_2,long param_3,long param_4,undefined8 param_5,undefined8 param_6,uint param_7);
void FUN_00406df8(undefined8 uParm1,undefined8 uParm2,undefined8 uParm3,undefined8 uParm4,undefined8 uParm5,uint uParm6);
undefined8 FUN_00406e4d(long *plParm1,int iParm2);
ulong FUN_00406eeb(undefined8 uParm1,uint uParm2,int iParm3);
ulong FUN_00406f2c(char *pcParm1,char **ppcParm2,uint uParm3,undefined8 *puParm4,long lParm5);
undefined8 FUN_004072cb(int iParm1);
ulong FUN_004072f1(ulong *puParm1,int iParm2);
ulong FUN_00407350(undefined8 uParm1,uint uParm2,int iParm3);
ulong FUN_00407391(byte *pbParm1,byte **ppbParm2,uint uParm3,undefined8 *puParm4,long lParm5);
undefined8 FUN_00407772(ulong uParm1,ulong uParm2);
ulong FUN_004077f1(uint uParm1);
void FUN_0040781a(void);
void FUN_0040784e(uint uParm1);
void FUN_004078c2(uint uParm1,uint uParm2,undefined8 uParm3,undefined8 uParm4);
void FUN_00407946(undefined4 uParm1,undefined4 uParm2,undefined4 uParm3,undefined4 uParm4,undefined4 uParm5,undefined4 uParm6,undefined4 uParm7,undefined4 uParm8,uint param_1,uint param_2,undefined8 param_3,undefined8 param_4,undefined8 param_5,undefined8 param_6);
ulong FUN_00407a2a(undefined8 uParm1);
ulong FUN_00407ae2(undefined4 uParm1,undefined4 uParm2,undefined4 uParm3,undefined4 uParm4,undefined4 uParm5,undefined4 uParm6,undefined4 uParm7,undefined4 uParm8,uint param_1,uint param_2,undefined8 param_3,undefined8 param_4,undefined8 param_5,undefined8 param_6);
void FUN_00407d99(undefined8 uParm1);
void FUN_00407dbd(void);
ulong FUN_00407dcb(long lParm1);
undefined8 FUN_00407e9b(undefined8 uParm1);
void FUN_00407eba(undefined8 uParm1,undefined8 uParm2,uint uParm3);
void FUN_00407ee5(long lParm1,int *piParm2);
ulong FUN_004080c9(int param_1,undefined8 *param_2,char *param_3,long *param_4,int *param_5,int param_6,int *param_7,int param_8,undefined8 param_9);
char * FUN_004086b3(undefined8 uParm1,undefined8 uParm2,char *pcParm3,int *piParm4,int iParm5);
ulong FUN_00408781(uint param_1,undefined8 *param_2,char *param_3,long param_4,undefined8 param_5,uint param_6,uint *param_7,uint param_8);
ulong FUN_00408f4a(uint param_1,undefined8 param_2,undefined8 param_3,undefined8 param_4,undefined8 param_5,uint param_6,uint param_7);
void FUN_00408fda(uint uParm1,undefined8 uParm2,undefined8 uParm3,undefined8 uParm4,undefined8 uParm5);
undefined8 FUN_00409024(long lParm1);
ulong FUN_00409055(uint *puParm1,byte *pbParm2,long lParm3,undefined8 uParm4);
ulong * FUN_004090d8(ulong *puParm1,char cParm2,ulong uParm3);
undefined8 FUN_004091ea(long lParm1,long lParm2);
undefined8 FUN_00409253(int iParm1);
void FUN_00409279(long lParm1,long lParm2);
void FUN_004092e5(long lParm1,long lParm2);
ulong FUN_00409354(long lParm1,long lParm2);
void FUN_004093ab(undefined8 uParm1);
void FUN_004093cf(undefined8 uParm1);
void FUN_004093f3(undefined8 uParm1,undefined8 uParm2);
void FUN_0040941d(long lParm1);
void FUN_0040946c(long lParm1,long lParm2);
void FUN_004094d7(long lParm1,long lParm2);
ulong FUN_00409542(long lParm1,long lParm2);
ulong FUN_004095b5(long lParm1,long lParm2);
undefined8 FUN_004095fe(void);
ulong FUN_00409611(long param_1,undefined8 param_2,long param_3,long param_4,undefined8 param_5,byte param_6,long param_7);
ulong FUN_0040975c(long lParm1,undefined8 uParm2,long lParm3,long lParm4,byte bParm5,long lParm6);
undefined8 FUN_00409930(long lParm1,ulong uParm2);
void FUN_00409aa4(undefined8 uParm1,undefined8 uParm2,undefined8 *puParm3,long lParm4,char cParm5,long lParm6);
void FUN_00409b94(long *plParm1);
undefined8 FUN_00409ebf(long *plParm1);
long FUN_0040a99b(long *plParm1,long lParm2,uint *puParm3);
void FUN_0040aace(long *plParm1);
void FUN_0040ab9f(long *plParm1);
ulong FUN_0040ac3f(byte **ppbParm1,byte *pbParm2,uint uParm3);
ulong FUN_0040b925(long *plParm1,long lParm2);
ulong FUN_0040baa7(long *plParm1);
void FUN_0040bc31(long lParm1);
ulong FUN_0040bc7e(long lParm1,ulong uParm2,uint uParm3);
undefined8 FUN_0040be04(long *plParm1,long lParm2);
undefined8 FUN_0040be71(undefined8 *puParm1,undefined8 uParm2);
undefined8 FUN_0040befb(undefined8 *puParm1,long lParm2,long lParm3);
undefined8 FUN_0040bfd9(long *plParm1,long lParm2);
undefined8 FUN_0040c0b9(long *plParm1,long lParm2,long lParm3);
undefined8 FUN_0040c49a(long *plParm1,long lParm2,long lParm3);
undefined8 FUN_0040c7ac(long *plParm1,long lParm2);
ulong FUN_0040cb72(long *plParm1,long lParm2);
undefined8 FUN_0040cd5d(long *plParm1,undefined8 uParm2);
undefined8 FUN_0040ce12(long lParm1,long lParm2);
long FUN_0040cea1(long lParm1,long lParm2);
void FUN_0040cf59(long lParm1,long lParm2);
long FUN_0040cfd7(long *plParm1,undefined8 uParm2,undefined8 uParm3);
long FUN_0040d36f(long lParm1,uint uParm2);
ulong * FUN_0040d3c9(undefined4 *puParm1,long lParm2,long lParm3);
ulong * FUN_0040d4eb(undefined4 *puParm1,long lParm2,long lParm3,uint uParm4);
undefined8 FUN_0040d618(long *plParm1,ulong *puParm2,ulong uParm3);
void FUN_0040d7cc(long lParm1);
long FUN_0040d86f(long *plParm1,long lParm2,undefined8 uParm3);
long FUN_0040da44(long *plParm1,long lParm2,uint uParm3,undefined8 uParm4);
char * FUN_0040dd30(undefined8 uParm1,undefined8 uParm2,long lParm3);
undefined8 FUN_0040ddbc(long *plParm1);
void FUN_0040deb5(long **pplParm1,long lParm2,long lParm3);
void FUN_0040e58e(long *plParm1,undefined8 uParm2);
ulong FUN_0040e7f0(long *plParm1,undefined8 uParm2,undefined8 uParm3,ulong uParm4);
undefined8 FUN_0040eb6a(long *plParm1,ulong uParm2);
void FUN_0040eef8(long lParm1);
void FUN_0040f07a(long *plParm1);
ulong FUN_0040f109(long *plParm1);
void FUN_0040f458(long *plParm1);
ulong FUN_0040f6af(long *plParm1);
ulong FUN_0040fa45(long **pplParm1,code *pcParm2,undefined8 uParm3);
ulong FUN_0040fb23(long *plParm1,code *pcParm2,undefined8 uParm3);
undefined8 FUN_0040fbde(long lParm1,long lParm2);
ulong FUN_0040fd55(undefined8 uParm1,long lParm2);
long FUN_0040fe37(undefined4 *puParm1,long *plParm2,long lParm3);
undefined8 FUN_00410028(long *plParm1,long lParm2);
undefined8 FUN_0041011a(undefined8 uParm1,long lParm2);
ulong FUN_004101c9(long lParm1,long lParm2);
ulong FUN_00410440(long *plParm1,long lParm2,long lParm3,long lParm4,uint uParm5);
long FUN_00410970(long *plParm1,long lParm2,uint uParm3);
long FUN_00410a09(long *plParm1,long lParm2,undefined4 uParm3);
undefined8 FUN_00410b3f(long lParm1);
ulong FUN_00410c7d(long lParm1);
ulong FUN_00410d5d(undefined8 *puParm1,long *plParm2,long lParm3,char cParm4);
void FUN_0041112f(undefined8 uParm1,long lParm2,undefined8 uParm3);
undefined8 FUN_00411174(long *plParm1,long lParm2,ulong uParm3);
ulong FUN_0041196c(char *pcParm1,long lParm2,ulong uParm3);
long FUN_00411b81(undefined8 uParm1,long *plParm2,ulong uParm3,int *piParm4);
long FUN_00411cad(undefined8 uParm1,long *plParm2,long lParm3,ulong uParm4,long lParm5,int *piParm6);
long FUN_00411eac(undefined8 uParm1,undefined8 *puParm2,long lParm3,undefined8 uParm4,long lParm5,int *piParm6);
long FUN_00412068(long lParm1,long *plParm2,undefined8 *puParm3,ulong uParm4,long lParm5,int *piParm6);
long FUN_004128ee(undefined8 uParm1,long *plParm2,long lParm3,ulong uParm4,undefined8 uParm5,int *piParm6);
long FUN_00412a82(long lParm1,long lParm2,undefined8 uParm3,undefined8 *puParm4,ulong uParm5,undefined4 *puParm6);
ulong FUN_00412fa6(byte bParm1,long lParm2);
undefined8 FUN_00412fdd(ulong uParm1,undefined8 uParm2,long lParm3,long *plParm4,int *piParm5,int *piParm6);
undefined8 FUN_0041339e(undefined8 uParm1,undefined8 uParm2,undefined8 uParm3,byte *pbParm4);
long FUN_004133fd(long lParm1,long lParm2,long lParm3,ulong uParm4,int *piParm5);
undefined8 FUN_00413d33(undefined4 *param_1,long param_2,undefined *param_3,int param_4,undefined8 param_5,undefined8 param_6,char param_7);
undefined8 FUN_00413e7e(undefined4 *puParm1,long lParm2,char *pcParm3);
undefined8 FUN_00413fe5(undefined8 uParm1,undefined8 uParm2,undefined8 uParm3,byte *pbParm4);
undefined8 FUN_0041403f(long lParm1,undefined8 uParm2,long lParm3,long *plParm4,char *pcParm5,ulong uParm6);
long FUN_004149a6(long lParm1,undefined8 uParm2,undefined8 uParm3,char *pcParm4,byte bParm5,int *piParm6);
long FUN_00414c41(undefined8 uParm1,byte *pbParm2,undefined8 uParm3);
void FUN_00414d28(undefined8 *puParm1);
void FUN_00414d61(undefined8 uParm1,undefined8 uParm2,undefined8 uParm3,undefined uParm4);
undefined8 * FUN_00414d98(long lParm1,undefined8 *puParm2,undefined8 *puParm3,undefined8 *puParm4);
undefined8 FUN_00414ee4(long lParm1,long lParm2);
void FUN_00414f27(undefined8 *puParm1);
undefined8 FUN_00414f8b(undefined8 uParm1,long lParm2);
long * FUN_00414fb2(long **pplParm1,undefined8 uParm2);
void FUN_004150de(undefined8 uParm1,undefined8 uParm2,undefined8 uParm3,undefined8 uParm4,undefined8 uParm5,undefined8 uParm6);
ulong FUN_0041512f(long param_1,undefined8 param_2,ulong param_3,ulong param_4,ulong param_5,undefined8 param_6,ulong *param_7,char param_8);
ulong FUN_004154b7(ulong *puParm1,long lParm2,ulong uParm3,int iParm4);
ulong FUN_00415767(long *param_1,long param_2,long param_3,long param_4,long param_5,undefined8 param_6,ulong param_7,undefined8 *param_8,uint param_9);
ulong FUN_004164e2(long lParm1);
long FUN_00416804(long lParm1,char cParm2,long lParm3);
undefined8 FUN_00416d1d(long *plParm1,long lParm2,uint uParm3);
undefined8 FUN_00416de4(long lParm1,long lParm2,undefined8 uParm3);
long FUN_00416e87(long param_1,undefined8 param_2,long param_3,long *param_4,long param_5,long param_6,long param_7);
ulong FUN_004173c9(long *plParm1,undefined8 uParm2,undefined8 uParm3,long lParm4,undefined8 uParm5,undefined8 uParm6);
undefined8 FUN_00417596(long *plParm1,undefined8 *puParm2,long lParm3,undefined8 uParm4,undefined8 *puParm5);
undefined8 FUN_004176e7(long *plParm1,long lParm2,ulong uParm3,long *plParm4,char cParm5);
undefined8 FUN_00417af9(long *plParm1);
void FUN_00417b8f(long *plParm1,long lParm2,long lParm3,long lParm4,long lParm5,long lParm6);
ulong FUN_00417d33(long lParm1,long *plParm2);
undefined8 FUN_00417ed0(long lParm1,long *plParm2,long lParm3,undefined8 uParm4);
ulong FUN_004180da(long lParm1,long lParm2);
ulong FUN_004181c4(undefined8 uParm1,long lParm2,long lParm3,long lParm4);
ulong FUN_0041831e(long lParm1,long *plParm2,long lParm3,long lParm4);
ulong FUN_004184fd(long lParm1,long lParm2,undefined8 uParm3);
ulong FUN_0041862c(long *plParm1,long lParm2,undefined8 uParm3,undefined8 uParm4);
undefined8 FUN_004188ba(long lParm1,long lParm2,undefined8 uParm3,undefined8 uParm4,undefined8 uParm5,undefined8 uParm6);
ulong FUN_00418a26(long lParm1,uint uParm2,long lParm3,long lParm4,long lParm5);
undefined8 FUN_00418c97(long lParm1,long lParm2,undefined8 uParm3,undefined8 uParm4,long lParm5,undefined8 uParm6);
ulong FUN_00418d64(long *plParm1,long lParm2,undefined8 uParm3,long lParm4,long lParm5,long lParm6);
ulong FUN_004190d8(long lParm1,long *plParm2,long lParm3,long lParm4);
ulong FUN_00419588(long lParm1,long *plParm2,long lParm3,long lParm4,long lParm5);
undefined8 FUN_00419662(int *piParm1,long lParm2,long lParm3);
long FUN_004197dd(int *piParm1,long lParm2,long lParm3);
long FUN_00419a69(int *piParm1,long lParm2);
ulong FUN_00419b13(long lParm1,long lParm2,undefined8 uParm3);
ulong FUN_00419c0e(long lParm1,long lParm2);
ulong FUN_00419f84(long lParm1,long lParm2);
ulong FUN_0041a4d9(long lParm1,long lParm2,long lParm3);
ulong FUN_0041aa3e(undefined8 uParm1,long *plParm2,undefined8 *puParm3,undefined8 uParm4,long lParm5);
long FUN_0041ab15(long *plParm1,long lParm2,long lParm3,uint uParm4);
ulong FUN_0041aba1(long param_1,long *param_2,long param_3,long param_4,undefined8 param_5,long param_6,uint param_7);
ulong FUN_0041b32f(long lParm1,long lParm2,long lParm3,undefined8 uParm4);
ulong FUN_0041b5f2(long lParm1,undefined8 *puParm2,undefined8 uParm3,uint uParm4);
ulong FUN_0041b766(long *plParm1,undefined8 uParm2,long lParm3,long lParm4,uint uParm5);
ulong FUN_0041b923(long lParm1,undefined8 uParm2,long lParm3,undefined8 uParm4,uint uParm5);
undefined8 FUN_0041bcfe(long lParm1,long lParm2);
long FUN_0041c717(long *plParm1,long lParm2,long lParm3,long lParm4);
ulong FUN_0041cfb1(long *plParm1,long lParm2,long lParm3,long lParm4);
undefined8 FUN_0041d3ec(long lParm1,undefined8 *puParm2,long lParm3);
ulong FUN_0041d5bd(long lParm1,int iParm2);
undefined8 FUN_0041d745(long lParm1,undefined4 uParm2,ulong uParm3);
void FUN_0041d886(long lParm1);
void FUN_0041d996(long lParm1);
undefined8 FUN_0041d9d6(long lParm1,undefined8 uParm2,long lParm3,long lParm4,long lParm5);
long FUN_0041dcf3(long lParm1,long lParm2);
undefined8 FUN_0041ddc9(long lParm1,undefined8 uParm2,undefined8 uParm3);
undefined8 * FUN_0041df41(long lParm1,undefined8 uParm2,undefined8 uParm3);
void FUN_0041e04d(undefined8 *puParm1,undefined8 uParm2,undefined8 uParm3,undefined8 uParm4,undefined8 uParm5);
ulong FUN_0041e0b4(undefined4 uParm1,undefined4 uParm2,undefined4 uParm3,undefined4 uParm4,undefined4 uParm5,undefined4 uParm6,undefined4 uParm7,undefined4 uParm8,long param_1,ulong param_2,undefined8 param_3,undefined8 param_4,undefined8 param_5,undefined8 param_6);
ulong FUN_0041e245(long lParm1);
ulong FUN_0041e3b1(void);
long FUN_0041e3fe(ulong uParm1,long lParm2,ulong uParm3,long lParm4,long *plParm5);
int * FUN_0041e64a(ulong uParm1,uint *puParm2,ulong uParm3,uint *puParm4,ulong *puParm5);
char * FUN_0041f12a(ulong uParm1,long lParm2,long lParm3);
long FUN_0041f398(int *param_1,long *param_2);
long FUN_0041f583(undefined auParm1 [16],int *piParm2,long *plParm3);
undefined8 FUN_0041f942(int iParm1,long lParm2,uint *puParm3,long lParm4,uint uParm5);
void FUN_00420155(uint param_1);
void FUN_0042019f(undefined8 uParm1,uint uParm2);
ulong FUN_004201f1(void);
ulong FUN_00420583(undefined auParm1 [16],undefined8 uParm2,undefined8 uParm3);
ulong FUN_0042095b(char *pcParm1,long lParm2);
undefined8 FUN_004209b4(long param_1,long param_2,undefined param_3,int param_4,undefined8 param_5,ulong param_6,int param_7,ulong param_8,int param_9);
ulong FUN_004280e1(undefined8 uParm1,undefined8 uParm2,undefined8 uParm3);
ulong FUN_00428202(byte *pbParm1,byte *pbParm2);
undefined8 FUN_00428279(undefined8 uParm1);
undefined8 FUN_00428304(void);
ulong FUN_00428311(uint uParm1);
undefined1 * FUN_004283e2(void);
char * FUN_00428795(void);
void FUN_00428863(undefined8 uParm1,int *piParm2,undefined8 uParm3);
void FUN_00428904(int *param_1);
ulong FUN_004289c3(ulong uParm1,long lParm2);
void FUN_004289f7(undefined8 uParm1,undefined8 uParm2,undefined8 uParm3);
void FUN_00428a32(undefined8 uParm1,undefined8 uParm2,undefined8 uParm3,undefined8 uParm4);
ulong FUN_00428a83(ulong uParm1,ulong uParm2);
void FUN_00428a9e(undefined8 uParm1,undefined8 uParm2);
ulong FUN_00428ac3(long lParm1,long lParm2);
undefined8 FUN_00428b7a(uint *puParm1,ulong *puParm2);
undefined8 FUN_0042931a(char *pcParm1,ulong *puParm2,char **ppcParm3);
ulong FUN_0042a5d0(undefined auParm1 [16]);
ulong FUN_0042a60b(void);
void FUN_0042a63d(undefined8 uParm1,undefined8 uParm2);
ulong FUN_0042a662(long lParm1,long lParm2);
ulong FUN_0042a6f2(uint uParm1);
ulong FUN_0042a711(uint uParm1);
void FUN_0042a730(void);
undefined8 _DT_FINI(void);
undefined FUN_00632000();
undefined FUN_00632008();
undefined FUN_00632010();
undefined FUN_00632018();
undefined FUN_00632020();
undefined FUN_00632028();
undefined FUN_00632030();
undefined FUN_00632038();
undefined FUN_00632040();
undefined FUN_00632048();
undefined FUN_00632050();
undefined FUN_00632058();
undefined FUN_00632060();
undefined FUN_00632068();
undefined FUN_00632070();
undefined FUN_00632078();
undefined FUN_00632080();
undefined FUN_00632088();
undefined FUN_00632090();
undefined FUN_00632098();
undefined FUN_006320a0();
undefined FUN_006320a8();
undefined FUN_006320b0();
undefined FUN_006320b8();
undefined FUN_006320c0();
undefined FUN_006320c8();
undefined FUN_006320d0();
undefined FUN_006320d8();
undefined FUN_006320e0();
undefined FUN_006320e8();
undefined FUN_006320f0();
undefined FUN_006320f8();
undefined FUN_00632100();
undefined FUN_00632108();
undefined FUN_00632110();
undefined FUN_00632118();
undefined FUN_00632120();
undefined FUN_00632128();
undefined FUN_00632130();
undefined FUN_00632138();
undefined FUN_00632140();
undefined FUN_00632148();
undefined FUN_00632150();
undefined FUN_00632158();
undefined FUN_00632160();
undefined FUN_00632168();
undefined FUN_00632170();
undefined FUN_00632178();
undefined FUN_00632180();
undefined FUN_00632188();
undefined FUN_00632190();
undefined FUN_00632198();
undefined FUN_006321a0();
undefined FUN_006321a8();
undefined FUN_006321b0();
undefined FUN_006321b8();
undefined FUN_006321c0();
undefined FUN_006321c8();
undefined FUN_006321d0();
undefined FUN_006321d8();
undefined FUN_006321e0();
undefined FUN_006321e8();
undefined FUN_006321f0();
undefined FUN_006321f8();
undefined FUN_00632200();
undefined FUN_00632208();
undefined FUN_00632210();
undefined FUN_00632218();
undefined FUN_00632220();
undefined FUN_00632228();
undefined FUN_00632230();
undefined FUN_00632238();
undefined FUN_00632240();
undefined FUN_00632248();
undefined FUN_00632250();
undefined FUN_00632258();
undefined FUN_00632260();
undefined FUN_00632268();
undefined FUN_00632270();
undefined FUN_00632278();
undefined FUN_00632280();
undefined FUN_00632288();
undefined FUN_00632290();
undefined FUN_00632298();
undefined FUN_006322a0();
undefined FUN_006322a8();
undefined FUN_006322b0();
undefined FUN_006322b8();
undefined FUN_006322c0();
undefined FUN_006322c8();
undefined FUN_006322d0();
undefined FUN_006322d8();
undefined FUN_006322e0();
undefined FUN_006322e8();
undefined FUN_006322f0();
undefined FUN_006322f8();
undefined FUN_00632300();


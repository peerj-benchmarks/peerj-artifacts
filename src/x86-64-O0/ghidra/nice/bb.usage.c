
#include "nice.h"

long null_ARRAY_0061541_0_8_;
long null_ARRAY_0061541_8_8_;
long null_ARRAY_0061558_0_8_;
long null_ARRAY_0061558_16_8_;
long null_ARRAY_0061558_24_8_;
long null_ARRAY_0061558_32_8_;
long null_ARRAY_0061558_40_8_;
long null_ARRAY_0061558_48_8_;
long null_ARRAY_0061558_8_8_;
long null_ARRAY_006156c_0_4_;
long null_ARRAY_006156c_16_8_;
long null_ARRAY_006156c_4_4_;
long null_ARRAY_006156c_8_4_;
long DAT_00411c0b;
long DAT_00411cc5;
long DAT_00411d43;
long DAT_0041201e;
long DAT_004120e7;
long DAT_00412157;
long DAT_0041216f;
long DAT_0041218a;
long DAT_004121d0;
long DAT_0041231e;
long DAT_00412322;
long DAT_00412327;
long DAT_00412329;
long DAT_00412993;
long DAT_00412d60;
long DAT_004130ed;
long DAT_004130f2;
long DAT_0041315f;
long DAT_004131f0;
long DAT_004131f3;
long DAT_00413241;
long DAT_00413245;
long DAT_004132b8;
long DAT_004132b9;
long DAT_00615000;
long DAT_00615010;
long DAT_00615020;
long DAT_006153e8;
long DAT_00615400;
long DAT_00615478;
long DAT_0061547c;
long DAT_00615480;
long DAT_006154c0;
long DAT_006154d0;
long DAT_006154e0;
long DAT_006154e8;
long DAT_00615500;
long DAT_00615508;
long DAT_00615550;
long DAT_00615558;
long DAT_00615560;
long DAT_006156f8;
long DAT_00615700;
long DAT_00615708;
long DAT_00615718;
long fde_00413f50;
long FLOAT_UNKNOWN;
long null_ARRAY_00411dc0;
long null_ARRAY_00413700;
long null_ARRAY_00615410;
long null_ARRAY_00615440;
long null_ARRAY_00615520;
long null_ARRAY_00615580;
long null_ARRAY_006155c0;
long null_ARRAY_006156c0;
long PTR_DAT_006153e0;
long PTR_null_ARRAY_00615420;
ulong
FUN_00401cec (int iParm1)
{
  uint uVar1;

  if (iParm1 == 0)
    {
      func_0x00401550 ("Usage: %s [OPTION] [COMMAND [ARG]...]\n",
		       DAT_00615560);
      func_0x00401550
	("Run COMMAND with an adjusted niceness, which affects process scheduling.\nWith no COMMAND, print the current niceness.  Niceness values range from\n%d (most favorable to the process) to %d (least favorable to the process).\n",
	 0xffffffec, 0x13);
      FUN_00401b36 ();
      func_0x00401700
	("  -n, --adjustment=N   add integer N to the niceness (default 10)\n",
	 DAT_006154c0);
      func_0x00401700 ("      --help     display this help and exit\n",
		       DAT_006154c0);
      func_0x00401700
	("      --version  output version information and exit\n",
	 DAT_006154c0);
      func_0x00401550
	("\nNOTE: your shell may have its own version of %s, which usually supersedes\nthe version described here.  Please refer to your shell\'s documentation\nfor details about the options it supports.\n",
	 &DAT_0041201e);
      imperfection_wrapper ();	//    FUN_00401b50();
    }
  else
    {
      func_0x004016f0 (DAT_006154e0,
		       "Try \'%s --help\' for more information.\n",
		       DAT_00615560);
    }
  func_0x00401880 ();
  if ((iParm1 == 0xd) || (iParm1 == 1))
    {
      uVar1 = 1;
    }
  else
    {
      uVar1 = 0;
    }
  return (ulong) uVar1;
}

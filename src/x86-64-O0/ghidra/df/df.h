typedef unsigned char   undefined;

typedef unsigned char    byte;
typedef unsigned char    dwfenc;
typedef unsigned int    dword;
typedef unsigned long    qword;
typedef unsigned char    undefined1;
typedef unsigned short    undefined2;
typedef unsigned int    undefined4;
typedef unsigned long    undefined6;
typedef unsigned long    undefined8;
typedef unsigned short    word;
#define unkbyte9   unsigned long long
#define unkbyte10   unsigned long long
#define unkbyte11   unsigned long long
#define unkbyte12   unsigned long long
#define unkbyte13   unsigned long long
#define unkbyte14   unsigned long long
#define unkbyte15   unsigned long long
#define unkbyte16   unsigned long long

#define unkuint9   unsigned long long
#define unkuint10   unsigned long long
#define unkuint11   unsigned long long
#define unkuint12   unsigned long long
#define unkuint13   unsigned long long
#define unkuint14   unsigned long long
#define unkuint15   unsigned long long
#define unkuint16   unsigned long long

#define unkint9   long long
#define unkint10   long long
#define unkint11   long long
#define unkint12   long long
#define unkint13   long long
#define unkint14   long long
#define unkint15   long long
#define unkint16   long long

#define unkfloat1   float
#define unkfloat2   float
#define unkfloat3   float
#define unkfloat5   double
#define unkfloat6   double
#define unkfloat7   double
#define unkfloat9   long double
#define unkfloat11   long double
#define unkfloat12   long double
#define unkfloat13   long double
#define unkfloat14   long double
#define unkfloat15   long double
#define unkfloat16   long double

#define BADSPACEBASE   void
#define code   void

#define ulong long
#define true 1
#define bool int
#define false 0
#define float10 int
#define ushort int
#define undefined7 long
#define undefined2 long
#define uint5 long
#define uint6 long

// Add definition for uint
typedef unsigned int    uint;

#define sbyte char

extern imperfection_wrapper(void);
typedef struct eh_frame_hdr eh_frame_hdr, *Peh_frame_hdr;

struct eh_frame_hdr {
    byte eh_frame_hdr_version; // Exception Handler Frame Header Version
    dwfenc eh_frame_pointer_encoding; // Exception Handler Frame Pointer Encoding
    dwfenc eh_frame_desc_entry_count_encoding; // Encoding of # of Exception Handler FDEs
    dwfenc eh_frame_table_encoding; // Exception Handler Table Encoding
};

typedef struct fde_table_entry fde_table_entry, *Pfde_table_entry;

struct fde_table_entry {
    dword initial_loc; // Initial Location
    dword data_loc; // Data location
};

typedef enum Elf64_DynTag {
    DT_AUDIT=1879047932,
    DT_AUXILIARY=2147483645,
    DT_BIND_NOW=24,
    DT_CHECKSUM=1879047672,
    DT_CONFIG=1879047930,
    DT_DEBUG=21,
    DT_DEPAUDIT=1879047931,
    DT_ENCODING=32,
    DT_FEATURE_1=1879047676,
    DT_FILTER=2147483647,
    DT_FINI=13,
    DT_FINI_ARRAY=26,
    DT_FINI_ARRAYSZ=28,
    DT_FLAGS=30,
    DT_FLAGS_1=1879048187,
    DT_GNU_CONFLICT=1879047928,
    DT_GNU_CONFLICTSZ=1879047670,
    DT_GNU_HASH=1879047925,
    DT_GNU_LIBLIST=1879047929,
    DT_GNU_LIBLISTSZ=1879047671,
    DT_GNU_PRELINKED=1879047669,
    DT_HASH=4,
    DT_INIT=12,
    DT_INIT_ARRAY=25,
    DT_INIT_ARRAYSZ=27,
    DT_JMPREL=23,
    DT_MOVEENT=1879047674,
    DT_MOVESZ=1879047675,
    DT_MOVETAB=1879047934,
    DT_NEEDED=1,
    DT_NULL=0,
    DT_PLTGOT=3,
    DT_PLTPAD=1879047933,
    DT_PLTPADSZ=1879047673,
    DT_PLTREL=20,
    DT_PLTRELSZ=2,
    DT_POSFLAG_1=1879047677,
    DT_PREINIT_ARRAYSZ=33,
    DT_REL=17,
    DT_RELA=7,
    DT_RELACOUNT=1879048185,
    DT_RELAENT=9,
    DT_RELASZ=8,
    DT_RELCOUNT=1879048186,
    DT_RELENT=19,
    DT_RELSZ=18,
    DT_RPATH=15,
    DT_RUNPATH=29,
    DT_SONAME=14,
    DT_STRSZ=10,
    DT_STRTAB=5,
    DT_SYMBOLIC=16,
    DT_SYMENT=11,
    DT_SYMINENT=1879047679,
    DT_SYMINFO=1879047935,
    DT_SYMINSZ=1879047678,
    DT_SYMTAB=6,
    DT_TEXTREL=22,
    DT_TLSDESC_GOT=1879047927,
    DT_TLSDESC_PLT=1879047926,
    DT_VERDEF=1879048188,
    DT_VERDEFNUM=1879048189,
    DT_VERNEED=1879048190,
    DT_VERNEEDNUM=1879048191,
    DT_VERSYM=1879048176
} Elf64_DynTag;

typedef enum Elf_ProgramHeaderType {
    PT_DYNAMIC=2,
    PT_GNU_EH_FRAME=1685382480,
    PT_GNU_RELRO=1685382482,
    PT_GNU_STACK=1685382481,
    PT_INTERP=3,
    PT_LOAD=1,
    PT_NOTE=4,
    PT_NULL=0,
    PT_PHDR=6,
    PT_SHLIB=5,
    PT_TLS=7
} Elf_ProgramHeaderType;

typedef struct Elf64_Dyn Elf64_Dyn, *PElf64_Dyn;

struct Elf64_Dyn {
    enum Elf64_DynTag d_tag;
    qword d_val;
};

typedef struct Elf64_Shdr Elf64_Shdr, *PElf64_Shdr;

typedef enum Elf_SectionHeaderType {
    SHT_CHECKSUM=1879048184,
    SHT_DYNAMIC=6,
    SHT_DYNSYM=11,
    SHT_FINI_ARRAY=15,
    SHT_GNU_ATTRIBUTES=1879048181,
    SHT_GNU_HASH=1879048182,
    SHT_GNU_LIBLIST=1879048183,
    SHT_GNU_verdef=1879048189,
    SHT_GNU_verneed=1879048190,
    SHT_GNU_versym=1879048191,
    SHT_GROUP=17,
    SHT_HASH=5,
    SHT_INIT_ARRAY=14,
    SHT_NOBITS=8,
    SHT_NOTE=7,
    SHT_NULL=0,
    SHT_PREINIT_ARRAY=16,
    SHT_PROGBITS=1,
    SHT_REL=9,
    SHT_RELA=4,
    SHT_SHLIB=10,
    SHT_STRTAB=3,
    SHT_SUNW_COMDAT=1879048187,
    SHT_SUNW_move=1879048186,
    SHT_SUNW_syminfo=1879048188,
    SHT_SYMTAB=2,
    SHT_SYMTAB_SHNDX=18
} Elf_SectionHeaderType;

struct Elf64_Shdr {
    dword sh_name;
    enum Elf_SectionHeaderType sh_type;
    qword sh_flags;
    qword sh_addr;
    qword sh_offset;
    qword sh_size;
    dword sh_link;
    dword sh_info;
    qword sh_addralign;
    qword sh_entsize;
};

typedef struct Elf64_Phdr Elf64_Phdr, *PElf64_Phdr;

struct Elf64_Phdr {
    enum Elf_ProgramHeaderType p_type;
    dword p_flags;
    qword p_offset;
    qword p_vaddr;
    qword p_paddr;
    qword p_filesz;
    qword p_memsz;
    qword p_align;
};

typedef struct Elf64_Rela Elf64_Rela, *PElf64_Rela;

struct Elf64_Rela {
    qword r_offset; // location to apply the relocation action
    qword r_info; // the symbol table index and the type of relocation
    qword r_addend; // a constant addend used to compute the relocatable field value
};

typedef struct Elf64_Sym Elf64_Sym, *PElf64_Sym;

struct Elf64_Sym {
    dword st_name;
    byte st_info;
    byte st_other;
    word st_shndx;
    qword st_value;
    qword st_size;
};

typedef struct Elf64_Ehdr Elf64_Ehdr, *PElf64_Ehdr;

struct Elf64_Ehdr {
    byte e_ident_magic_num;
    char e_ident_magic_str[3];
    byte e_ident_class;
    byte e_ident_data;
    byte e_ident_version;
    byte e_ident_pad[9];
    word e_type;
    word e_machine;
    dword e_version;
    qword e_entry;
    qword e_phoff;
    qword e_shoff;
    dword e_flags;
    word e_ehsize;
    word e_phentsize;
    word e_phnum;
    word e_shentsize;
    word e_shnum;
    word e_shstrndx;
};




undefined8 _DT_INIT(void);
void FUN_00401c70(void);
void entry(void);
void FUN_004022f0(void);
void FUN_00402370(void);
void FUN_004023f0(void);
ulong FUN_0040242e(byte bParm1);
void FUN_0040243d(void);
void FUN_00402457(void);
void FUN_00402471(undefined8 uParm1);
void FUN_00402495(undefined *puParm1);
char * FUN_00402631(char *pcParm1);
void FUN_00402683(void);
void FUN_004026fb(void);
void FUN_00402846(int iParm1,long lParm2);
void FUN_00402946(undefined8 uParm1);
void FUN_00402b02(void);
void FUN_00402d3c(void);
undefined8 FUN_0040306e(long lParm1);
undefined8 FUN_004030dc(long lParm1);
ulong FUN_0040314a(ulong *puParm1,ulong uParm2);
ulong FUN_00403173(ulong *puParm1,ulong *puParm2);
undefined8 FUN_004031a5(undefined8 uParm1);
void FUN_004031e4(undefined8 uParm1);
void FUN_004031fe(char cParm1);
undefined8 FUN_00403602(undefined8 uParm1);
undefined FUN_00403636(ulong uParm1);;
undefined * FUN_00403648(byte bParm1,long lParm2,long lParm3,undefined8 uParm4,undefined8 uParm5);
void FUN_004036de(ulong *puParm1,byte *pbParm2,ulong uParm3,byte bParm4);
ulong FUN_004037ab(long lParm1);
void FUN_00403801(undefined8 *puParm1,undefined8 *puParm2,undefined8 *puParm3);
void FUN_004039d5(long *plParm1,long lParm2);
void FUN_00403af7(char *param_1,char *param_2,undefined *param_3,char *param_4,undefined *param_5,char param_6,char param_7,undefined8 *param_8,char param_9);
char * FUN_00404674(undefined8 uParm1);
undefined8 FUN_00404729(char *pcParm1);
void FUN_004049e0(undefined8 uParm1,long *plParm2);
void FUN_00404dc3(undefined8 uParm1,long lParm2);
void FUN_00404e20(void);
void FUN_00404ebd(undefined8 uParm1);
void FUN_00404efe(undefined8 uParm1);
ulong FUN_00404f3f(uint uParm1);
ulong FUN_0040506a(uint uParm1,undefined8 *puParm2);
undefined8 FUN_00405a7b(undefined8 uParm1,long lParm2);
void FUN_00405db8(undefined8 uParm1);
ulong FUN_00405dd7(long *plParm1,undefined8 uParm2,undefined8 uParm3);
char * FUN_00405e81(char *pcParm1,uint uParm2);
void FUN_00406687(void);
long FUN_0040676c(undefined8 uParm1);
ulong FUN_0040679a(char *pcParm1);
undefined * FUN_00406823(undefined8 uParm1);
char * FUN_004068ba(char *pcParm1);
void FUN_00406923(long lParm1,undefined8 uParm2,undefined8 *puParm3);
ulong FUN_004069be(long lParm1,undefined8 uParm2,undefined8 *puParm3);
long FUN_00406a1a(long *plParm1,undefined8 uParm2);
long FUN_00406a71(long lParm1,long lParm2);
ulong FUN_00406b04(ulong uParm1);
ulong FUN_00406b70(ulong uParm1);
ulong FUN_00406bb7(undefined8 uParm1,ulong uParm2);
ulong FUN_00406bee(ulong uParm1,ulong uParm2);
undefined8 FUN_00406c07(long lParm1);
ulong FUN_00406d00(ulong uParm1,long lParm2);
long * FUN_00406df6(undefined8 uParm1,undefined1 *puParm2,code *pcParm3,code *pcParm4,long lParm5);
void FUN_00406f5e(long **pplParm1,undefined8 uParm2);
long FUN_00407088(long lParm1);
void FUN_004070d3(long lParm1,undefined8 *puParm2);
long FUN_00407108(long lParm1,long lParm2,long **pplParm3,char cParm4);
undefined8 FUN_0040729d(long lParm1,long **pplParm2,char cParm3);
undefined8 FUN_0040746b(long *plParm1,undefined8 uParm2);
undefined8 FUN_0040766f(long lParm1,long lParm2,long *plParm3);
undefined8 FUN_004079c1(undefined8 uParm1,undefined8 uParm2);
ulong FUN_00407a0a(undefined8 *puParm1,ulong uParm2);
ulong FUN_00407a56(undefined8 *puParm1,undefined8 *puParm2);
void FUN_00407ace(undefined8 *puParm1);
void FUN_00407aff(void);
long FUN_00407bf6(long lParm1,ulong uParm2,byte *pbParm3,undefined8 uParm4);
char * FUN_00407cf2(ulong uParm1,long lParm2,uint uParm3,ulong uParm4,ulong uParm5);
undefined8 FUN_00408683(void);
ulong FUN_004086a4(char *pcParm1,undefined8 *puParm2,uint *puParm3);
ulong FUN_00408811(undefined8 uParm1,undefined8 uParm2,long *plParm3);
char * FUN_00408866(ulong uParm1,long lParm2);
ulong FUN_004088eb(uint *puParm1);
long FUN_00408939(uint *puParm1,ulong uParm2);
undefined * FUN_004089be(undefined *puParm1,undefined *puParm2,long lParm3);
long FUN_00408a07(long lParm1,long lParm2,long lParm3,ulong *puParm4,int iParm5,uint uParm6);
long FUN_00408dcd(undefined8 uParm1,ulong *puParm2,uint uParm3,uint uParm4);
void FUN_00408ebb(undefined8 uParm1,uint uParm2);
ulong FUN_00408eed(byte *pbParm1,long lParm2,uint uParm3);
void FUN_00409126(long lParm1);
ulong FUN_00409203(undefined1 *puParm1,byte bParm2,uint uParm3);
undefined8 * FUN_0040928b(undefined8 *puParm1,int iParm2);
char * FUN_00409302(char *pcParm1,int iParm2);
ulong FUN_004093a2(long param_1,ulong param_2,long param_3,ulong param_4,uint param_5,uint param_6,long param_7,char *param_8,char *param_9);
undefined1 * FUN_0040a26c(int iParm1,undefined8 uParm2,undefined8 uParm3,uint *puParm4);
void FUN_0040a4df(uint uParm1,uint uParm2,undefined8 uParm3);
void FUN_0040a520(uint uParm1,undefined8 uParm2);
void FUN_0040a544(undefined8 uParm1,undefined8 uParm2,char cParm3);
void FUN_0040a5d8(undefined8 uParm1,char cParm2);
void FUN_0040a602(undefined8 uParm1);
void FUN_0040a621(uint uParm1,uint uParm2,undefined8 uParm3);
void FUN_0040a6bc(uint uParm1,undefined8 uParm2,undefined8 uParm3);
void FUN_0040a6e8(uint uParm1,undefined8 uParm2);
void FUN_0040a711(undefined8 uParm1);
undefined8 FUN_0040a730(int *piParm1);
void FUN_0040a7aa(uint *puParm1);
void FUN_0040a7e1(uint *puParm1);
void FUN_0040a816(undefined8 uParm1,long lParm2,undefined8 uParm3,undefined8 uParm4,undefined8 *puParm5,undefined8 uParm6);
void FUN_0040ac7a(undefined8 uParm1,undefined8 uParm2,undefined8 uParm3,undefined8 uParm4,uint *puParm5);
void FUN_0040ad4c(undefined4 uParm1,undefined4 uParm2,undefined4 uParm3,undefined4 uParm4,undefined4 uParm5,undefined4 uParm6,undefined4 uParm7,undefined4 uParm8,undefined8 param_1,undefined8 param_2,undefined8 param_3,undefined8 param_4,undefined8 param_5,undefined8 param_6);
void FUN_0040ae02(ulong uParm1,ulong uParm2);
void FUN_0040ae43(undefined8 uParm1,ulong uParm2,ulong uParm3);
void FUN_0040ae92(undefined8 uParm1);
long FUN_0040aeac(long lParm1);
long FUN_0040aee1(long lParm1,long lParm2);
void FUN_0040af42(undefined8 uParm1,undefined8 uParm2);
void FUN_0040af76(undefined8 uParm1);
long FUN_0040afa3(void);
long FUN_0040afcd(void);
void FUN_0040b006(uint uParm1,int iParm2,undefined uParm3,long lParm4,undefined8 uParm5,uint uParm6);
undefined8 FUN_0040b0d5(uint uParm1,uint uParm2,char cParm3,undefined8 uParm4,undefined8 uParm5);
undefined8 FUN_0040b11b(int iParm1);
ulong FUN_0040b141(ulong *puParm1,int iParm2);
ulong FUN_0040b1a0(undefined8 uParm1,uint uParm2,int iParm3);
ulong FUN_0040b1e1(byte *pbParm1,byte **ppbParm2,uint uParm3,undefined8 *puParm4,long lParm5);
ulong FUN_0040b5c2(undefined4 uParm1,undefined4 uParm2,undefined4 uParm3,undefined4 uParm4,undefined4 uParm5,undefined4 uParm6,undefined4 uParm7,undefined4 uParm8,undefined8 param_1,undefined8 param_2,undefined8 param_3,undefined8 param_4,undefined8 param_5,undefined8 param_6);
undefined8 FUN_0040b676(ulong uParm1,ulong uParm2);
void FUN_0040b6f5(undefined4 *puParm1);
void FUN_0040b709(uint *puParm1);
void FUN_0040b724(uint *puParm1);
undefined8 FUN_0040b778(uint *puParm1,undefined8 uParm2);
long FUN_0040b7d2(long lParm1);
ulong FUN_0040b800(char *pcParm1);
ulong FUN_0040bb07(uint uParm1);
void FUN_0040bb30(void);
void FUN_0040bb64(uint uParm1);
void FUN_0040bbd8(uint uParm1,uint uParm2,undefined8 uParm3,undefined8 uParm4);
void FUN_0040bc5c(undefined4 uParm1,undefined4 uParm2,undefined4 uParm3,undefined4 uParm4,undefined4 uParm5,undefined4 uParm6,undefined4 uParm7,undefined4 uParm8,uint param_1,uint param_2,undefined8 param_3,undefined8 param_4,undefined8 param_5,undefined8 param_6);
undefined8 FUN_0040bd40(void);
undefined8 FUN_0040bd4b(undefined8 uParm1,undefined8 uParm2,long *plParm3);
long FUN_0040be34(long lParm1,undefined *puParm2);
void FUN_0040c3bd(long lParm1,int *piParm2);
ulong FUN_0040c5a1(int param_1,undefined8 *param_2,char *param_3,long *param_4,int *param_5,int param_6,int *param_7,int param_8,undefined8 param_9);
char * FUN_0040cb8b(undefined8 uParm1,undefined8 uParm2,char *pcParm3,int *piParm4,int iParm5);
ulong FUN_0040cc59(uint param_1,undefined8 *param_2,char *param_3,long param_4,undefined8 param_5,uint param_6,uint *param_7,uint param_8);
ulong FUN_0040d422(uint param_1,undefined8 param_2,undefined8 param_3,undefined8 param_4,undefined8 param_5,uint param_6,uint param_7);
void FUN_0040d4b2(uint uParm1,undefined8 uParm2,undefined8 uParm3,undefined8 uParm4,undefined8 uParm5);
void FUN_0040d4fc(undefined8 uParm1,undefined8 uParm2);
ulong FUN_0040d521(long lParm1,long lParm2);
undefined8 FUN_0040d5d8(long lParm1);
ulong FUN_0040d609(uint *puParm1,byte *pbParm2,long lParm3,undefined8 uParm4);
ulong * FUN_0040d68c(ulong *puParm1,char cParm2,ulong uParm3);
undefined8 FUN_0040d79e(void);
void FUN_0040d7af(long lParm1);
char ** FUN_0040d94f(void);
void FUN_0040e283(undefined8 *puParm1);
void FUN_0040e2eb(uint uParm1,undefined8 uParm2,uint uParm3,uint uParm4);
ulong FUN_0040e31b(undefined4 uParm1,undefined4 uParm2,undefined4 uParm3,undefined4 uParm4,undefined4 uParm5,undefined4 uParm6,undefined4 uParm7,undefined4 uParm8,uint param_1,undefined8 param_2,uint param_3,undefined8 param_4,undefined8 param_5,undefined8 param_6);
undefined8 FUN_0040e4e2(long lParm1,long lParm2);
void FUN_0040e54b(undefined8 uParm1,undefined8 uParm2);
ulong FUN_0040e570(long lParm1,long lParm2);
long FUN_0040e600(long lParm1,ulong uParm2,long *plParm3);
long FUN_0040e828(long lParm1,long lParm2,long lParm3,ulong uParm4);
long FUN_0040eb25(long lParm1,long lParm2,long lParm3,ulong uParm4);
char * FUN_0040efd6(char *pcParm1,char *pcParm2);
ulong FUN_0040f133(long *plParm1,undefined8 uParm2,undefined8 uParm3);
ulong FUN_0040f1ad(undefined8 uParm1,undefined8 uParm2,undefined8 uParm3);
ulong FUN_0040f2ce(uint *puParm1,long lParm2);
void FUN_0040f344(uint uParm1);
long FUN_0040f38c(undefined8 uParm1,ulong uParm2);
long FUN_0040f4de(undefined8 uParm1,long lParm2,long lParm3,long lParm4);
ulong FUN_0040f60d(ulong uParm1,undefined4 uParm2);
ulong FUN_0040f629(byte *pbParm1,byte *pbParm2);
undefined8 FUN_0040f6a0(uint uParm1,char cParm2);
undefined8 FUN_0040f71b(undefined8 uParm1);
void FUN_0040f7a6(undefined4 uParm1,undefined4 uParm2,undefined4 uParm3,undefined4 uParm4,undefined4 uParm5,undefined4 uParm6,undefined4 uParm7,undefined4 uParm8,undefined8 param_1,uint param_2,undefined8 param_3,undefined8 param_4,undefined8 param_5,undefined8 param_6);
undefined8 FUN_0040f8b5(void);
ulong FUN_0040f8c2(uint uParm1);
ulong FUN_0040f993(char *pcParm1,ulong uParm2);
undefined1 * FUN_0040f9ed(void);
char * FUN_0040fda0(void);
ulong FUN_0040fe6e(uint uParm1);
ulong FUN_0040fe7e(uint uParm1);
undefined8 FUN_0040fece(undefined8 uParm1);
undefined8 FUN_0040ff94(uint uParm1,undefined8 uParm2);
ulong FUN_004101ad(undefined8 uParm1);
ulong FUN_00410265(undefined4 uParm1,undefined4 uParm2,undefined4 uParm3,undefined4 uParm4,undefined4 uParm5,undefined4 uParm6,undefined4 uParm7,undefined4 uParm8,uint param_1,uint param_2,undefined8 param_3,undefined8 param_4,undefined8 param_5,undefined8 param_6);
undefined8 FUN_0041051c(uint uParm1);
void FUN_0041057f(undefined8 uParm1);
void FUN_004105a3(void);
ulong FUN_004105b1(long lParm1);
undefined8 FUN_00410681(undefined8 uParm1);
void FUN_004106a0(undefined8 uParm1,undefined8 uParm2,uint uParm3);
void FUN_004106cb(uint uParm1,undefined8 uParm2,undefined8 uParm3,uint uParm4);
void FUN_004106f8(uint uParm1,undefined8 uParm2,undefined8 uParm3,uint uParm4);
ulong FUN_00410735(uint uParm1,long lParm2,long lParm3,uint uParm4);
void FUN_00410848(undefined8 uParm1,undefined8 uParm2,undefined8 uParm3);
undefined8 FUN_0041087a(long lParm1,undefined8 uParm2,undefined8 uParm3);
ulong FUN_00410920(void);
long FUN_0041096d(ulong uParm1,long lParm2,ulong uParm3,long lParm4,long *plParm5);
int * FUN_00410bb9(ulong uParm1,uint *puParm2,ulong uParm3,uint *puParm4,ulong *puParm5);
char * FUN_00411699(ulong uParm1,long lParm2,long lParm3);
long FUN_00411907(int *param_1,long *param_2);
long FUN_00411af2(undefined auParm1 [16],int *piParm2,long *plParm3);
undefined8 FUN_00411eb1(int iParm1,long lParm2,uint *puParm3,long lParm4,uint uParm5);
void FUN_004126c4(uint param_1);
void FUN_0041270e(undefined8 uParm1,uint uParm2);
ulong FUN_00412760(void);
ulong FUN_00412af2(undefined auParm1 [16],undefined8 uParm2,undefined8 uParm3);
ulong FUN_00412eca(char *pcParm1,long lParm2);
undefined8 FUN_00412f23(long param_1,long param_2,undefined param_3,int param_4,undefined8 param_5,ulong param_6,int param_7,ulong param_8,int param_9);
ulong FUN_0041a650(uint uParm1);
void FUN_0041a66f(undefined8 uParm1,int *piParm2,undefined8 uParm3);
void FUN_0041a710(int *param_1);
void FUN_0041a7cf(uint uParm1);
ulong FUN_0041a7f5(ulong uParm1,long lParm2);
void FUN_0041a829(undefined8 uParm1,undefined8 uParm2,undefined8 uParm3);
void FUN_0041a864(undefined8 uParm1,undefined8 uParm2,undefined8 uParm3,undefined8 uParm4);
ulong FUN_0041a8b5(ulong uParm1,ulong uParm2);
void FUN_0041a8d0(void);
long FUN_0041a8d6(long *plParm1,ulong *puParm2,int iParm3,long lParm4);
undefined8 FUN_0041aab0(uint *puParm1,ulong *puParm2);
undefined8 FUN_0041b250(char *pcParm1,ulong *puParm2,char **ppcParm3);
ulong FUN_0041c506(undefined auParm1 [16]);
ulong FUN_0041c541(void);
void FUN_0041c580(void);
undefined8 _DT_FINI(void);
undefined FUN_00623000();
undefined FUN_00623008();
undefined FUN_00623010();
undefined FUN_00623018();
undefined FUN_00623020();
undefined FUN_00623028();
undefined FUN_00623030();
undefined FUN_00623038();
undefined FUN_00623040();
undefined FUN_00623048();
undefined FUN_00623050();
undefined FUN_00623058();
undefined FUN_00623060();
undefined FUN_00623068();
undefined FUN_00623070();
undefined FUN_00623078();
undefined FUN_00623080();
undefined FUN_00623088();
undefined FUN_00623090();
undefined FUN_00623098();
undefined FUN_006230a0();
undefined FUN_006230a8();
undefined FUN_006230b0();
undefined FUN_006230b8();
undefined FUN_006230c0();
undefined FUN_006230c8();
undefined FUN_006230d0();
undefined FUN_006230d8();
undefined FUN_006230e0();
undefined FUN_006230e8();
undefined FUN_006230f0();
undefined FUN_006230f8();
undefined FUN_00623100();
undefined FUN_00623108();
undefined FUN_00623110();
undefined FUN_00623118();
undefined FUN_00623120();
undefined FUN_00623128();
undefined FUN_00623130();
undefined FUN_00623138();
undefined FUN_00623140();
undefined FUN_00623148();
undefined FUN_00623150();
undefined FUN_00623158();
undefined FUN_00623160();
undefined FUN_00623168();
undefined FUN_00623170();
undefined FUN_00623178();
undefined FUN_00623180();
undefined FUN_00623188();
undefined FUN_00623190();
undefined FUN_00623198();
undefined FUN_006231a0();
undefined FUN_006231a8();
undefined FUN_006231b0();
undefined FUN_006231b8();
undefined FUN_006231c0();
undefined FUN_006231c8();
undefined FUN_006231d0();
undefined FUN_006231d8();
undefined FUN_006231e0();
undefined FUN_006231e8();
undefined FUN_006231f0();
undefined FUN_006231f8();
undefined FUN_00623200();
undefined FUN_00623208();
undefined FUN_00623210();
undefined FUN_00623218();
undefined FUN_00623220();
undefined FUN_00623228();
undefined FUN_00623230();
undefined FUN_00623238();
undefined FUN_00623240();
undefined FUN_00623248();
undefined FUN_00623250();
undefined FUN_00623258();
undefined FUN_00623260();
undefined FUN_00623268();
undefined FUN_00623270();
undefined FUN_00623278();
undefined FUN_00623280();
undefined FUN_00623288();
undefined FUN_00623290();
undefined FUN_00623298();
undefined FUN_006232a0();
undefined FUN_006232a8();
undefined FUN_006232b0();
undefined FUN_006232b8();
undefined FUN_006232c0();
undefined FUN_006232c8();
undefined FUN_006232d0();
undefined FUN_006232d8();
undefined FUN_006232e0();
undefined FUN_006232e8();
undefined FUN_006232f0();
undefined FUN_006232f8();
undefined FUN_00623300();
undefined FUN_00623308();
undefined FUN_00623310();


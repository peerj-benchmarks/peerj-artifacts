
#include "df.h"

long null_ARRAY_00622b7_0_8_;
long null_ARRAY_00622b7_8_8_;
long null_ARRAY_00622ce_0_8_;
long null_ARRAY_00622ce_16_8_;
long null_ARRAY_00622ce_40_8_;
long null_ARRAY_00622ce_48_8_;
long null_ARRAY_00622ce_8_8_;
long null_ARRAY_00622d8_0_8_;
long null_ARRAY_00622d8_16_8_;
long null_ARRAY_00622d8_24_8_;
long null_ARRAY_00622d8_32_8_;
long null_ARRAY_00622d8_40_8_;
long null_ARRAY_00622d8_48_8_;
long null_ARRAY_00622d8_8_8_;
long null_ARRAY_00622ec_0_4_;
long null_ARRAY_00622ec_16_8_;
long null_ARRAY_00622ec_4_4_;
long null_ARRAY_00622ec_8_4_;
long DAT_0041c78f;
long DAT_0041c845;
long DAT_0041c8c3;
long DAT_0041cd0e;
long DAT_0041cdde;
long DAT_0041cdf8;
long DAT_0041cec9;
long DAT_0041d506;
long DAT_0041d5ec;
long DAT_0041d635;
long DAT_0041d638;
long DAT_0041d63b;
long DAT_0041d6fa;
long DAT_0041d7bb;
long DAT_0041d7e7;
long DAT_0041d833;
long DAT_0041d848;
long DAT_0041d909;
long DAT_0041d90b;
long DAT_0041d948;
long DAT_0041da10;
long DAT_0041db5e;
long DAT_0041db62;
long DAT_0041db67;
long DAT_0041db69;
long DAT_0041e1c0;
long DAT_0041e1db;
long DAT_0041e5a0;
long DAT_0041e5a8;
long DAT_0041e9a1;
long DAT_0041ea0e;
long DAT_0041ea13;
long DAT_0041ea26;
long DAT_0041ea28;
long DAT_0041ea2a;
long DAT_0041ea87;
long DAT_0041eb18;
long DAT_0041eb1b;
long DAT_0041eb69;
long DAT_0041eb85;
long DAT_0041eba8;
long DAT_0041ebc9;
long DAT_0041ec17;
long DAT_0041ec22;
long DAT_0041ec38;
long DAT_0041ec43;
long DAT_0041ecb8;
long DAT_0041ecb9;
long DAT_0041ed0e;
long DAT_0041ed19;
long DAT_00622448;
long DAT_00622458;
long DAT_00622468;
long DAT_00622b50;
long DAT_00622b60;
long DAT_00622bd8;
long DAT_00622bdc;
long DAT_00622be0;
long DAT_00622c00;
long DAT_00622c10;
long DAT_00622c20;
long DAT_00622c28;
long DAT_00622c40;
long DAT_00622c48;
long DAT_00622ca0;
long DAT_00622ca8;
long DAT_00622ca9;
long DAT_00622caa;
long DAT_00622cac;
long DAT_00622cb0;
long DAT_00622cb8;
long DAT_00622cb9;
long DAT_00622cbc;
long DAT_00622cc0;
long DAT_00622cc8;
long DAT_00622cd0;
long DAT_00622cd8;
long DAT_00622cd9;
long DAT_00622d18;
long DAT_00622d20;
long DAT_00622d28;
long DAT_00622d30;
long DAT_00622d38;
long DAT_00622d40;
long DAT_00622d48;
long DAT_00622d50;
long DAT_00622ef8;
long DAT_00622f00;
long DAT_00622f08;
long DAT_00622f10;
long DAT_00622f18;
long DAT_00622f28;
long fde_00420880;
long FLOAT_UNKNOWN;
long null_ARRAY_0041ca80;
long null_ARRAY_0041d850;
long null_ARRAY_0041d900;
long null_ARRAY_0041d930;
long null_ARRAY_0041ed40;
long null_ARRAY_0041f6c0;
long null_ARRAY_0041fbe0;
long null_ARRAY_00622900;
long null_ARRAY_00622b70;
long null_ARRAY_00622ba0;
long null_ARRAY_00622c60;
long null_ARRAY_00622ce0;
long null_ARRAY_00622d80;
long null_ARRAY_00622dc0;
long null_ARRAY_00622ec0;
long PTR_DAT_00622b48;
long PTR_null_ARRAY_00622b80;
long PTR_s_source_fstype_itotal_iused_iavai_00622b40;
long stack0x00000008;
ulong
FUN_00404f3f (uint uParm1)
{
  int iVar1;
  uint uVar2;
  ulong uVar3;
  uint *puVar4;
  char *pcVar5;
  long lVar6;
  uint uVar7;
  undefined8 uVar8;
  uint uStack132;
  char *pcStack128;
  uint uStack116;
  uint uStack112;
  int iStack108;
  char *pcStack104;
  int iStack92;
  uint uStack88;
  int iStack84;
  undefined8 *puStack80;
  undefined8 *puStack72;
  char cStack58;
  char cStack57;
  long lStack56;
  code *pcStack32;
  uint local_c;

  if (uParm1 == 0)
    {
      pcStack32 = (code *) 0x404f8e;
      local_c = uParm1;
      func_0x00401ca0 ("Usage: %s [OPTION]... [FILE]...\n", DAT_00622d50);
      pcStack32 = (code *) 0x404fa2;
      func_0x00401f10
	("Show information about the file system on which each FILE resides,\nor all file systems by default.\n",
	 DAT_00622c00);
      pcStack32 = (code *) 0x404fa7;
      FUN_0040243d ();
      pcStack32 = (code *) 0x404fbb;
      func_0x00401f10
	("  -a, --all             include pseudo, duplicate, inaccessible file systems\n  -B, --block-size=SIZE  scale sizes by SIZE before printing them; e.g.,\n                           \'-BM\' prints sizes in units of 1,048,576 bytes;\n                           see SIZE format below\n  -h, --human-readable  print sizes in powers of 1024 (e.g., 1023M)\n  -H, --si              print sizes in powers of 1000 (e.g., 1.1G)\n",
	 DAT_00622c00);
      pcStack32 = (code *) 0x404fcf;
      func_0x00401f10
	("  -i, --inodes          list inode information instead of block usage\n  -k                    like --block-size=1K\n  -l, --local           limit listing to local file systems\n      --no-sync         do not invoke sync before getting usage info (default)\n",
	 DAT_00622c00);
      pcStack32 = (code *) 0x404fe3;
      func_0x00401f10
	("      --output[=FIELD_LIST]  use the output format defined by FIELD_LIST,\n                               or print all fields if FIELD_LIST is omitted.\n  -P, --portability     use the POSIX output format\n      --sync            invoke sync before getting usage info\n",
	 DAT_00622c00);
      pcStack32 = (code *) 0x404ff7;
      func_0x00401f10
	("      --total           elide all entries insignificant to available space,\n                          and produce a grand total\n",
	 DAT_00622c00);
      pcStack32 = (code *) 0x40500b;
      func_0x00401f10
	("  -t, --type=TYPE       limit listing to file systems of type TYPE\n  -T, --print-type      print file system type\n  -x, --exclude-type=TYPE   limit listing to file systems not of type TYPE\n  -v                    (ignored)\n",
	 DAT_00622c00);
      pcStack32 = (code *) 0x40501f;
      func_0x00401f10 ("      --help     display this help and exit\n",
		       DAT_00622c00);
      pcStack32 = (code *) 0x405033;
      func_0x00401f10
	("      --version  output version information and exit\n",
	 DAT_00622c00);
      pcStack32 = (code *) 0x40503d;
      FUN_00402471 (&DAT_0041d506);
      pcStack32 = (code *) 0x405042;
      FUN_00402457 ();
      pcStack32 = (code *) 0x405056;
      pcVar5 = (char *) DAT_00622c00;
      func_0x00401f10
	("\nFIELD_LIST is a comma-separated list of columns to be included.  Valid\nfield names are: \'source\', \'fstype\', \'itotal\', \'iused\', \'iavail\', \'ipcent\',\n\'size\', \'used\', \'avail\', \'pcent\', \'file\' and \'target\' (see info page).\n");
      pcStack32 = (code *) 0x405060;
      imperfection_wrapper ();	//    FUN_00402495();
    }
  else
    {
      pcVar5 = "Try \'%s --help\' for more information.\n";
      pcStack32 = (code *) 0x404f70;
      local_c = uParm1;
      func_0x00401f00 (DAT_00622c20,
		       "Try \'%s --help\' for more information.\n",
		       DAT_00622d50);
    }
  pcStack32 = FUN_0040506a;
  uVar7 = local_c;
  func_0x00402160 ();
  pcStack32 = (code *) & stack0xfffffffffffffff8;
  FUN_00409126 (*(undefined8 *) pcVar5);
  func_0x004020e0 (6, &DAT_0041c8c3);
  func_0x004020c0 (FUN_00406687);
  DAT_00622cc0 = (undefined8 *) 0x0;
  DAT_00622cc8 = (undefined8 *) 0x0;
  DAT_00622ca8 = '\0';
  DAT_00622caa = 0;
  DAT_00622cac = 0xffffffff;
  DAT_00622cd8 = '\0';
  DAT_00622cb8 = '\0';
  DAT_00622cbc = 0;
  DAT_00622cd9 = '\0';
  null_ARRAY_00622ce_0_8_ = 1;
  cStack57 = '\0';
  pcStack104 = "options %s and %s are mutually exclusive";
LAB_00405106:
  ;
  do
    {
      uStack132 = 0xffffffff;
      imperfection_wrapper ();	//    iStack108 = FUN_0040d4b2((ulong)uVar7, pcVar5, "aB:iF:hHklmPTt:vx:", null_ARRAY_0041ca80, &uStack132);
      if (iStack108 == -1)
	{
	  if (DAT_00622cac == 0xffffffff)
	    {
	      if (cStack57 == '\0')
		{
		  uVar8 = func_0x00401d30 ("DF_BLOCK_SIZE");
		  FUN_00408811 (uVar8, &DAT_00622cac, &DAT_00622cb0);
		}
	      else
		{
		  DAT_00622cac = 0;
		  lVar6 = func_0x00401d30 ("POSIXLY_CORRECT");
		  if (lVar6 == 0)
		    {
		      DAT_00622cb0 = 0x400;
		    }
		  else
		    {
		      DAT_00622cb0 = 0x200;
		    }
		}
	    }
	  if ((DAT_00622d18 != 1) && (DAT_00622d18 != 4))
	    {
	      if ((DAT_00622cac & 0x10) == 0)
		{
		  if (cStack57 != '\0')
		    {
		      DAT_00622d18 = 3;
		    }
		}
	      else
		{
		  DAT_00622d18 = 2;
		}
	    }
	  cStack58 = '\0';
	  puStack72 = DAT_00622cc0;
	  do
	    {
	      if (puStack72 == (undefined8 *) 0x0)
		{
		  if (cStack58 == '\0')
		    {
		      if (DAT_00622bd8 < (int) uVar7)
			{
			  imperfection_wrapper ();	//              lStack56 = FUN_0040ae02((long)(int)(uVar7 - DAT_00622bd8), 0x90, (ulong)(uVar7 - DAT_00622bd8));
			  iStack84 = DAT_00622bd8;
			  while (iStack84 < (int) uVar7)
			    {
			      lVar6 =
				(long) (iStack84 - DAT_00622bd8) * 0x90 +
				lStack56;
			      imperfection_wrapper ();	//                iVar1 = FUN_0040e570(((undefined8 *)pcVar5)[(long)iStack84], lVar6, lVar6);
			      if (iVar1 == 0)
				{
				  if (((*(uint *)
					(lStack56 +
					 (long) (iStack84 -
						 DAT_00622bd8) * 0x90 +
					 0x18) & 0xf000) != 0x1000)
				      && (uStack116 =
					  func_0x00402200 (((undefined8 *)
							    pcVar5)[(long)
								    iStack84],
							   0x100),
					  -1 < (int) uStack116))
				    {
				      func_0x00402280 ((ulong) uStack116);
				    }
				}
			      else
				{
				  imperfection_wrapper ();	//                  uVar8 = FUN_0040a621(0, 3, ((undefined8 *)pcVar5)[(long)iStack84]);
				  puVar4 = (uint *) func_0x00402150 ();
				  imperfection_wrapper ();	//                  FUN_0040bc5c(0, (ulong)*puVar4, &DAT_0041cdf8, uVar8);
				  DAT_00622cbc = 1;
				  ((undefined8 *) pcVar5)[(long) iStack84] =
				    0;
				}
			      iStack84 = iStack84 + 1;
			    }
			}
		      if (1)
			{
			  uVar2 = 0;
			}
		      else
			{
			  uVar2 = 1;
			}
		      imperfection_wrapper ();	//            DAT_00622cd0 = FUN_0040d94f((ulong)uVar2);
		      if (DAT_00622cd0 == 0)
			{
			  uStack88 = 0;
			  if ((((int) uVar7 <= DAT_00622bd8)
			       || (DAT_00622ca8 != '\0'))
			      ||
			      ((DAT_00622ca9 != '\0'
				||
				((DAT_00622cc0 != (undefined8 *) 0x0
				  || (DAT_00622cc8 != (undefined8 *) 0x0))))))
			    {
			      uStack88 = 1;
			    }
			  if (uStack88 == 0)
			    {
			      pcStack128 = "Warning: ";
			    }
			  else
			    {
			      pcStack128 = "";
			    }
			  puVar4 = (uint *) func_0x00402150 ();
			  imperfection_wrapper ();	//              FUN_0040bc5c((ulong)uStack88, (ulong)*puVar4, &DAT_0041d6fa, pcStack128, "cannot read table of mounted file systems");
			}
		      if (DAT_00622cb9 != '\0')
			{
			  func_0x00401f90 ();
			}
		      FUN_00402b02 ();
		      uVar8 = 0x40594a;
		      FUN_00402d3c ();
		      if (DAT_00622bd8 < (int) uVar7)
			{
			  DAT_00622caa = 1;
			  iStack92 = DAT_00622bd8;
			  while (iStack92 < (int) uVar7)
			    {
			      if (((undefined8 *) pcVar5)[(long) iStack92] !=
				  0)
				{
				  lVar6 =
				    (long) (iStack92 - DAT_00622bd8) * 0x90 +
				    lStack56;
				  uVar8 = 0x4059d1;
				  imperfection_wrapper ();	//                  FUN_00404dc3(((undefined8 *)pcVar5)[(long)iStack92], lVar6, lVar6);
				}
			      iStack92 = iStack92 + 1;
			    }
			}
		      else
			{
			  uVar8 = 0x4059e4;
			  FUN_00404e20 ();
			}
		      if (DAT_00622cb8 == '\0')
			{
			  if (DAT_00622cbc == 0)
			    {
			      imperfection_wrapper ();	//                FUN_0040bc5c(1, 0, "no file systems processed");
			    }
			}
		      else
			{
			  if (DAT_00622cd9 != '\0')
			    {
			      if (1)
				{
				  pcVar5 = "total";
				}
			      else
				{
				  pcVar5 = "-";
				}
			      imperfection_wrapper ();	//                FUN_00403af7("total", pcVar5, 0, 0, 0, 0, 0, null_ARRAY_00622ce0, 0, uVar8);
			    }
			  FUN_004026fb ();
			}
		      uVar3 = (ulong) DAT_00622cbc;
		    }
		  else
		    {
		      uVar3 = 1;
		    }
		  return uVar3;
		}
	      puStack80 = DAT_00622cc8;
	      while (puStack80 != (undefined8 *) 0x0)
		{
		  iVar1 =
		    func_0x00402080 (*puStack72, *puStack80, *puStack80);
		  if (iVar1 == 0)
		    {
		      imperfection_wrapper ();	//            uVar8 = FUN_0040a711(*puStack72);
		      imperfection_wrapper ();	//            FUN_0040bc5c(0, 0, "file system type %s both selected and excluded", uVar8);
		      cStack58 = '\x01';
		      break;
		    }
		  puStack80 = (undefined8 *) puStack80[1];
		}
	      puStack72 = (undefined8 *) puStack72[1];
	    }
	  while (true);
	}
      if (iStack108 != 0x69)
	{
	  if (iStack108 < 0x6a)
	    {
	      if (iStack108 == 0x48)
		{
		  DAT_00622cac = 0x90;
		  DAT_00622cb0 = 1;
		  goto LAB_00405106;
		}
	      if (iStack108 < 0x49)
		{
		  if (iStack108 == -0x82)
		    {
		      FUN_00404f3f (0);
		    LAB_0040552b:
		      ;
		      imperfection_wrapper ();	//            FUN_0040ad4c(DAT_00622c00, &DAT_0041d5ec, "GNU coreutils", PTR_DAT_00622b48, "Torbjorn Granlund", "David MacKenzie", "Paul Eggert", 0);
		      func_0x00402160 (0);
		    }
		  else
		    {
		      if (iStack108 < -0x81)
			{
			  if (iStack108 == -0x83)
			    goto LAB_0040552b;
			}
		      else
			{
			  if (iStack108 == 0x42)
			    {
			      uStack112 =
				FUN_00408811 (DAT_00622f28, &DAT_00622cac,
					      &DAT_00622cb0);
			      if (uStack112 != 0)
				{
				  FUN_0040b0d5 ((ulong) uStack112,
						(ulong) uStack132,
						(ulong) (uint) (int) (char)
						iStack108,
						null_ARRAY_0041ca80,
						DAT_00622f28);
				}
			      goto LAB_00405106;
			    }
			  if (iStack108 == 0x46)
			    goto LAB_00405411;
			}
		    }
		}
	      else
		{
		  if (iStack108 == 0x54)
		    {
		      if (DAT_00622d18 == 4)
			{
			  imperfection_wrapper ();	//              FUN_0040bc5c(0, 0, pcStack104, &DAT_0041d638, "--output");
			  imperfection_wrapper ();	//              FUN_00404f3f();
			}
		      DAT_00622cd8 = '\x01';
		      goto LAB_00405106;
		    }
		  if (iStack108 < 0x55)
		    {
		      if (iStack108 == 0x50)
			{
			  if (DAT_00622d18 == 4)
			    {
			      imperfection_wrapper ();	//                FUN_0040bc5c(0, 0, pcStack104, &DAT_0041d63b, "--output");
			      imperfection_wrapper ();	//                FUN_00404f3f();
			    }
			  cStack57 = '\x01';
			  goto LAB_00405106;
			}
		    }
		  else
		    {
		      if (iStack108 == 0x61)
			{
			  DAT_00622ca8 = '\x01';
			  goto LAB_00405106;
			}
		      if (iStack108 == 0x68)
			{
			  DAT_00622cac = 0xb0;
			  DAT_00622cb0 = 1;
			  goto LAB_00405106;
			}
		    }
		}
	    }
	  else
	    {
	      if (iStack108 == 0x76)
		goto LAB_00405106;
	      if (iStack108 < 0x77)
		{
		  if (iStack108 == 0x6c)
		    {
		      DAT_00622ca9 = '\x01';
		      goto LAB_00405106;
		    }
		  if (iStack108 < 0x6d)
		    {
		      if (iStack108 == 0x6b)
			{
			  DAT_00622cac = 0;
			  DAT_00622cb0 = 0x400;
			  goto LAB_00405106;
			}
		    }
		  else
		    {
		      if (iStack108 == 0x6d)
			{
			  DAT_00622cac = 0;
			  DAT_00622cb0 = 0x100000;
			  goto LAB_00405106;
			}
		      if (iStack108 == 0x74)
			{
			LAB_00405411:
			  ;
			  imperfection_wrapper ();	//              FUN_00404ebd();
			  goto LAB_00405106;
			}
		    }
		}
	      else
		{
		  if (iStack108 == 0x81)
		    {
		      DAT_00622cb9 = '\x01';
		      goto LAB_00405106;
		    }
		  if (iStack108 < 0x82)
		    {
		      if (iStack108 == 0x78)
			{
			  imperfection_wrapper ();	//              FUN_00404efe();
			  goto LAB_00405106;
			}
		      if (iStack108 == 0x80)
			{
			  DAT_00622cb9 = '\0';
			  goto LAB_00405106;
			}
		    }
		  else
		    {
		      if (iStack108 == 0x82)
			{
			  DAT_00622cd9 = '\x01';
			  goto LAB_00405106;
			}
		      if (iStack108 == 0x83)
			{
			  if (DAT_00622d18 == 1)
			    {
			      imperfection_wrapper ();	//                FUN_0040bc5c(0, 0, pcStack104, &DAT_0041d635, "--output");
			      imperfection_wrapper ();	//                FUN_00404f3f();
			    }
			  if ((cStack57 != '\0') && (DAT_00622d18 == 0))
			    {
			      imperfection_wrapper ();	//                FUN_0040bc5c(0, 0, pcStack104, &DAT_0041d63b, "--output");
			      imperfection_wrapper ();	//                FUN_00404f3f();
			    }
			  if (DAT_00622cd8 != '\0')
			    {
			      imperfection_wrapper ();	//                FUN_0040bc5c(0, 0, pcStack104, &DAT_0041d638, "--output");
			      imperfection_wrapper ();	//                FUN_00404f3f();
			    }
			  DAT_00622d18 = 4;
			  if (DAT_00622f28 != 0)
			    {
			      imperfection_wrapper ();	//                FUN_00402946();
			    }
			  goto LAB_00405106;
			}
		    }
		}
	    }
	  imperfection_wrapper ();	//      FUN_00404f3f();
	  goto LAB_00405106;
	}
      if (DAT_00622d18 == 4)
	{
	  imperfection_wrapper ();	//      FUN_0040bc5c(0, 0, pcStack104, &DAT_0041d635, "--output");
	  imperfection_wrapper ();	//      FUN_00404f3f();
	}
      DAT_00622d18 = 1;
    }
  while (true);
}

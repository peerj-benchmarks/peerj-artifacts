
#include "mkdir.h"

long null_ARRAY_0061773_0_8_;
long null_ARRAY_0061773_8_8_;
long null_ARRAY_0061788_0_8_;
long null_ARRAY_0061788_16_8_;
long null_ARRAY_0061788_24_8_;
long null_ARRAY_0061788_32_8_;
long null_ARRAY_0061788_40_8_;
long null_ARRAY_0061788_48_8_;
long null_ARRAY_0061788_8_8_;
long null_ARRAY_006179c_0_4_;
long null_ARRAY_006179c_16_8_;
long null_ARRAY_006179c_4_4_;
long null_ARRAY_006179c_8_4_;
long DAT_00413e8b;
long DAT_00413f45;
long DAT_00413fc3;
long DAT_00414512;
long DAT_0041452d;
long DAT_004146e8;
long DAT_0041481e;
long DAT_00414822;
long DAT_00414827;
long DAT_00414829;
long DAT_00414e80;
long DAT_00414e8f;
long DAT_00414fe3;
long DAT_004153a0;
long DAT_004153b8;
long DAT_004153bd;
long DAT_00415427;
long DAT_004154b8;
long DAT_004154bb;
long DAT_00415509;
long DAT_0041550d;
long DAT_00415580;
long DAT_00415581;
long DAT_006172b8;
long DAT_006172c8;
long DAT_006172d8;
long DAT_00617708;
long DAT_00617720;
long DAT_00617798;
long DAT_0061779c;
long DAT_006177a0;
long DAT_006177c0;
long DAT_006177d0;
long DAT_006177e0;
long DAT_006177e8;
long DAT_00617800;
long DAT_00617808;
long DAT_00617850;
long DAT_00617858;
long DAT_00617860;
long DAT_006179f8;
long DAT_00617a00;
long DAT_00617a08;
long DAT_00617a10;
long DAT_00617a20;
long fde_00416350;
long FLOAT_UNKNOWN;
long null_ARRAY_00414040;
long null_ARRAY_004159c0;
long null_ARRAY_00617730;
long null_ARRAY_00617760;
long null_ARRAY_00617820;
long null_ARRAY_00617880;
long null_ARRAY_006178c0;
long null_ARRAY_006179c0;
long PTR_DAT_00617700;
long PTR_null_ARRAY_00617740;
void
FUN_00402109 (uint uParm1)
{
  undefined8 uVar1;
  char *pcVar2;
  ulong uVar3;

  if (uParm1 == 0)
    {
      func_0x00401830 ("Usage: %s [OPTION]... DIRECTORY...\n", DAT_00617860);
      func_0x00401a20
	("Create the DIRECTORY(ies), if they do not already exist.\n",
	 DAT_006177c0);
      FUN_00401ebe ();
      func_0x00401a20
	("  -m, --mode=MODE   set file mode (as in chmod), not a=rwx - umask\n  -p, --parents     no error if existing, make parent directories as needed\n  -v, --verbose     print a message for each created directory\n",
	 DAT_006177c0);
      func_0x00401a20
	("  -Z                   set SELinux security context of each created directory\n                         to the default type\n      --context[=CTX]  like -Z, or if CTX is specified then set the SELinux\n                         or SMACK security context to CTX\n",
	 DAT_006177c0);
      func_0x00401a20 ("      --help     display this help and exit\n",
		       DAT_006177c0);
      pcVar2 = DAT_006177c0;
      func_0x00401a20
	("      --version  output version information and exit\n");
      imperfection_wrapper ();	//    FUN_00401ed8();
    }
  else
    {
      pcVar2 = "Try \'%s --help\' for more information.\n";
      func_0x00401a10 (DAT_006177e0,
		       "Try \'%s --help\' for more information.\n",
		       DAT_00617860);
    }
  uVar3 = (ulong) uParm1;
  func_0x00401c00 ();
  if (*(long *) (pcVar2 + 0x18) != 0)
    {
      imperfection_wrapper ();	//    uVar1 = FUN_00404820(4, uVar3);
      imperfection_wrapper ();	//    FUN_00402800(DAT_006177c0, *(undefined8 *)(pcVar2 + 0x18), uVar1, *(undefined8 *)(pcVar2 + 0x18));
    }
  return;
}

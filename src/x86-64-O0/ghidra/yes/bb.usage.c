
#include "yes.h"

long null_ARRAY_006143f_0_8_;
long null_ARRAY_006143f_8_8_;
long null_ARRAY_0061454_0_8_;
long null_ARRAY_0061454_16_8_;
long null_ARRAY_0061454_24_8_;
long null_ARRAY_0061454_32_8_;
long null_ARRAY_0061454_40_8_;
long null_ARRAY_0061454_48_8_;
long null_ARRAY_0061454_8_8_;
long null_ARRAY_0061468_0_4_;
long null_ARRAY_0061468_16_8_;
long null_ARRAY_0061468_4_4_;
long null_ARRAY_0061468_8_4_;
long DAT_00411500;
long DAT_004115bd;
long DAT_0041163b;
long DAT_0041179e;
long DAT_004117b2;
long DAT_004117b4;
long DAT_004117de;
long DAT_004118a0;
long DAT_004118e8;
long DAT_00411a1e;
long DAT_00411a22;
long DAT_00411a27;
long DAT_00411a29;
long DAT_00412093;
long DAT_00412460;
long DAT_00412478;
long DAT_0041247d;
long DAT_004124e7;
long DAT_00412578;
long DAT_0041257b;
long DAT_004125c9;
long DAT_004125cd;
long DAT_00412640;
long DAT_00412641;
long DAT_00412828;
long DAT_00614000;
long DAT_00614010;
long DAT_00614020;
long DAT_006143c8;
long DAT_006143e0;
long DAT_00614458;
long DAT_0061445c;
long DAT_00614460;
long DAT_00614480;
long DAT_00614490;
long DAT_006144a0;
long DAT_006144a8;
long DAT_006144c0;
long DAT_006144c8;
long DAT_00614510;
long DAT_00614518;
long DAT_00614520;
long DAT_006146b8;
long DAT_006146c0;
long DAT_006146c8;
long DAT_006146d8;
long fde_004132a0;
long FLOAT_UNKNOWN;
long null_ARRAY_00411680;
long null_ARRAY_00411840;
long null_ARRAY_00412a80;
long null_ARRAY_006143f0;
long null_ARRAY_006144e0;
long null_ARRAY_00614540;
long null_ARRAY_00614580;
long null_ARRAY_00614680;
long PTR_DAT_006143c0;
long PTR_null_ARRAY_00614400;
undefined8
FUN_00401b88 (uint uParm1)
{
  bool bVar1;
  int iVar2;
  long *plVar3;
  long lVar4;
  long lVar5;
  ulong uVar6;
  uint *puVar7;
  char *pcVar8;
  ulong uStack104;
  long *plStack96;
  ulong uStack88;
  long *plStack80;
  ulong uStack64;
  long *plStack56;

  if (uParm1 == 0)
    {
      func_0x00401450 ("Usage: %s [STRING]...\n  or:  %s OPTION\n",
		       DAT_00614520, DAT_00614520);
      func_0x004015e0
	("Repeatedly output a line with all specified STRING(s), or \'y\'.\n\n",
	 DAT_00614480);
      func_0x004015e0 ("      --help     display this help and exit\n",
		       DAT_00614480);
      pcVar8 = (char *) DAT_00614480;
      func_0x004015e0
	("      --version  output version information and exit\n");
      imperfection_wrapper ();	//    FUN_004019de();
    }
  else
    {
      pcVar8 = "Try \'%s --help\' for more information.\n";
      func_0x004015d0 (DAT_006144a0,
		       "Try \'%s --help\' for more information.\n",
		       DAT_00614520);
    }
  func_0x00401750 ();
  FUN_004021ad (*(undefined8 *) pcVar8);
  func_0x00401710 (6, &DAT_0041163b);
  func_0x004016f0 (FUN_00401ef7);
  imperfection_wrapper ();	//  FUN_0040205c((ulong)uParm1, pcVar8, &DAT_0041179e, "GNU coreutils", PTR_DAT_006143c0, FUN_00401b88, "David MacKenzie", 0);
  imperfection_wrapper ();	//  iVar2 = FUN_00404f7b((ulong)uParm1, pcVar8, &DAT_004117b2, null_ARRAY_00411680, 0);
  if (iVar2 != -1)
    {
      FUN_00401b88 (1);
    }
  plStack96 = (undefined8 *) pcVar8 + (long) (int) DAT_00614458;
  plVar3 = (undefined8 *) pcVar8 + (long) (int) uParm1;
  plStack56 = plVar3;
  if (DAT_00614458 == uParm1)
    {
      plStack56 = plVar3 + 1;
      lVar4 = FUN_00401b7a (&DAT_004117b4);
      *plVar3 = lVar4;
    }
  uStack64 = 0;
  bVar1 = true;
  plStack80 = plStack96;
  while (plStack80 < plStack56)
    {
      lVar4 = func_0x004017b0 (*plStack80);
      uStack64 = uStack64 + lVar4 + 1;
      if ((plStack80 + 1 < plStack56)
	  && (lVar4 + 1 + *plStack80 != plStack80[1]))
	{
	  bVar1 = false;
	}
      plStack80 = plStack80 + 1;
    }
  if (uStack64 < 0x201)
    {
      uStack64 = 0x400;
      bVar1 = false;
    }
  if (bVar1)
    {
      lVar4 = *plStack96;
    }
  else
    {
      lVar4 = FUN_00403b8d (uStack64);
    }
  uStack88 = 0;
  while (uVar6 = uStack88, plStack96 < plStack56)
    {
      lVar5 = func_0x004017b0 (*plStack96);
      if (!bVar1)
	{
	  func_0x004014f0 (lVar4 + uStack88, *plStack96, lVar5,
			   lVar4 + uStack88);
	}
      lVar5 = uStack88 + lVar5;
      uStack88 = lVar5 + 1;
      *(undefined *) (lVar5 + lVar4) = 0x20;
      plStack96 = plStack96 + 1;
    }
  *(undefined *) (lVar4 + (uStack88 - 1)) = 10;
  uStack104 = uStack64 / uStack88;
  while (uStack104 = uStack104 - 1, uStack104 != 0)
    {
      func_0x004014f0 (lVar4 + uStack88, lVar4, uVar6, lVar4 + uStack88);
      uStack88 = uStack88 + uVar6;
    }
  do
    {
      uVar6 = FUN_00401fdc (1, lVar4, uStack88);
    }
  while (uVar6 == uStack88);
  puVar7 = (uint *) func_0x00401740 ();
  imperfection_wrapper ();	//  FUN_00403da2(0, (ulong)*puVar7, "standard output");
  return 1;
}

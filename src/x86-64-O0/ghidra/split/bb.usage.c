
#include "split.h"

long null_ARRAY_0061b4d_0_8_;
long null_ARRAY_0061b4d_8_8_;
long null_ARRAY_0061b98_0_8_;
long null_ARRAY_0061b98_8_8_;
long null_ARRAY_0061ba8_0_8_;
long null_ARRAY_0061ba8_16_8_;
long null_ARRAY_0061ba8_24_8_;
long null_ARRAY_0061ba8_32_8_;
long null_ARRAY_0061ba8_40_8_;
long null_ARRAY_0061ba8_48_8_;
long null_ARRAY_0061ba8_8_8_;
long null_ARRAY_0061bbc_0_4_;
long null_ARRAY_0061bbc_16_8_;
long null_ARRAY_0061bbc_4_4_;
long null_ARRAY_0061bbc_8_4_;
long local_2_0_4_;
long DAT_004168e1;
long DAT_0041699d;
long DAT_00416a1b;
long DAT_00417576;
long DAT_00417622;
long DAT_0041767d;
long DAT_0041769a;
long DAT_0041776b;
long DAT_0041776d;
long DAT_0041779d;
long DAT_00417852;
long DAT_00417855;
long DAT_0041786f;
long DAT_004179d6;
long DAT_00417b19;
long DAT_00417b60;
long DAT_00417c9e;
long DAT_00417ca2;
long DAT_00417ca7;
long DAT_00417ca9;
long DAT_00418313;
long DAT_004186e0;
long DAT_00418a78;
long DAT_00418a7d;
long DAT_00418ae7;
long DAT_00418b78;
long DAT_00418b7b;
long DAT_00418bc9;
long DAT_00418bd9;
long DAT_00418bdd;
long DAT_00418c50;
long DAT_00418c51;
long DAT_0061b000;
long DAT_0061b010;
long DAT_0061b020;
long DAT_0061b480;
long DAT_0061b490;
long DAT_0061b494;
long DAT_0061b4a0;
long DAT_0061b4c0;
long DAT_0061b538;
long DAT_0061b53c;
long DAT_0061b540;
long DAT_0061b580;
long DAT_0061b740;
long DAT_0061b750;
long DAT_0061b760;
long DAT_0061b768;
long DAT_0061b780;
long DAT_0061b788;
long DAT_0061b800;
long DAT_0061b808;
long DAT_0061b810;
long DAT_0061b818;
long DAT_0061b820;
long DAT_0061b940;
long DAT_0061b948;
long DAT_0061b950;
long DAT_0061b958;
long DAT_0061b960;
long DAT_0061b968;
long DAT_0061b970;
long DAT_0061ba10;
long DAT_0061ba11;
long DAT_0061ba12;
long DAT_0061ba18;
long DAT_0061ba20;
long DAT_0061ba28;
long DAT_0061ba30;
long DAT_0061ba38;
long DAT_0061ba40;
long DAT_0061ba48;
long DAT_0061bbf8;
long DAT_0061c408;
long DAT_0061c410;
long DAT_0061c418;
long DAT_0061c428;
long fde_00419aa0;
long FLOAT_UNKNOWN;
long null_ARRAY_00416b40;
long null_ARRAY_00417af4;
long null_ARRAY_004190a0;
long null_ARRAY_0061b4d0;
long null_ARRAY_0061b500;
long null_ARRAY_0061b7a0;
long null_ARRAY_0061b840;
long null_ARRAY_0061b8c0;
long null_ARRAY_0061b980;
long null_ARRAY_0061ba80;
long null_ARRAY_0061bac0;
long null_ARRAY_0061bbc0;
long null_ARRAY_0061bc00;
long PTR_DAT_0061b498;
long PTR_null_ARRAY_0061b4e0;
long PTR_s_abcdefghijklmnopqrstuvwxyz_0061b488;
ulong
FUN_004025c8 (uint uParm1, undefined8 uParm2, undefined8 uParm3, ulong uParm4)
{
  char cVar1;
  long lVar2;
  int *piVar3;
  long lVar4;
  undefined4 *puVar5;
  long extraout_RDX;
  char *pcVar6;
  long lStack48;
  ulong uStack40;

  if (uParm1 == 0)
    {
      func_0x00401a30 ("Usage: %s [OPTION]... [FILE [PREFIX]]\n",
		       DAT_0061ba48);
      func_0x00401cb0
	("Output pieces of FILE to PREFIXaa, PREFIXab, ...;\ndefault size is 1000 lines, and default PREFIX is \'x\'.\n",
	 DAT_0061b740);
      FUN_004021a9 ();
      FUN_004021c3 ();
      func_0x00401ca0 (DAT_0061b740,
		       "  -a, --suffix-length=N   generate suffixes of length N (default %d)\n      --additional-suffix=SUFFIX  append an additional SUFFIX to file names\n  -b, --bytes=SIZE        put SIZE bytes per output file\n  -C, --line-bytes=SIZE   put at most SIZE bytes of records per output file\n  -d                      use numeric suffixes starting at 0, not alphabetic\n      --numeric-suffixes[=FROM]  same as -d, but allow setting the start value\n  -x                      use hex suffixes starting at 0, not alphabetic\n      --hex-suffixes[=FROM]  same as -x, but allow setting the start value\n  -e, --elide-empty-files  do not generate empty output files with \'-n\'\n      --filter=COMMAND    write to shell COMMAND; file name is $FILE\n  -l, --lines=NUMBER      put NUMBER lines/records per output file\n  -n, --number=CHUNKS     generate CHUNKS output files; see explanation below\n  -t, --separator=SEP     use SEP instead of newline as the record separator;\n                            \'\\0\' (zero) specifies the NUL character\n  -u, --unbuffered        immediately copy input to output with \'-n r/...\'\n",
		       2);
      func_0x00401cb0
	("      --verbose           print a diagnostic just before each\n                            output file is opened\n",
	 DAT_0061b740);
      func_0x00401cb0 ("      --help     display this help and exit\n",
		       DAT_0061b740);
      func_0x00401cb0
	("      --version  output version information and exit\n",
	 DAT_0061b740);
      FUN_004021dd ();
      pcVar6 = DAT_0061b740;
      func_0x00401cb0
	("\nCHUNKS may be:\n  N       split into N files based on size of input\n  K/N     output Kth of N to stdout\n  l/N     split into N files without splitting lines/records\n  l/K/N   output Kth of N to stdout without splitting lines/records\n  r/N     like \'l\' but use round robin distribution\n  r/K/N   likewise but only output Kth of N to stdout\n");
      imperfection_wrapper ();	//    FUN_004021f7();
    }
  else
    {
      pcVar6 = "Try \'%s --help\' for more information.\n";
      func_0x00401ca0 (DAT_0061b760,
		       "Try \'%s --help\' for more information.\n",
		       DAT_0061ba48);
    }
  func_0x00401e80 ();
  lVar2 = func_0x00401be0 ((ulong) uParm1, 0, 1);
  if (lVar2 < 0)
    {
      piVar3 = (int *) func_0x00401e70 ();
      if (*piVar3 == 0x1d)
	{
	  puVar5 = (undefined4 *) func_0x00401e70 ();
	  *puVar5 = 0;
	}
      lVar2 = -1;
    }
  else
    {
      uStack40 = 0;
      do
	{
	  imperfection_wrapper ();	//      lVar4 = FUN_004075ec((ulong)uParm1, uStack40 + extraout_RDX, uParm4 - uStack40, uStack40 + extraout_RDX);
	  if (lVar4 == 0)
	    {
	      return uStack40;
	    }
	  if (lVar4 == -1)
	    {
	      return 0xffffffffffffffff;
	    }
	  uStack40 = lVar4 + uStack40;
	}
      while (uStack40 < uParm4);
      if (*(long *) (pcVar6 + 0x30) == 0)
	{
	  puVar5 = (undefined4 *) func_0x00401e70 ();
	  *puVar5 = 0x4b;
	  lVar2 = -1;
	}
      else
	{
	  lVar2 = lVar2 + uStack40;
	  cVar1 = FUN_004023a1 (pcVar6);
	  if ((cVar1 == '\0') || (*(long *) (pcVar6 + 0x30) < lVar2))
	    {
	      lStack48 = func_0x00401be0 ((ulong) uParm1, 0, 2);
	      if (lStack48 < 0)
		{
		  return 0xffffffffffffffff;
		}
	      if (lStack48 != lVar2)
		{
		  lVar4 = func_0x00401be0 ((ulong) uParm1, lVar2, 0, lVar2);
		  if (lVar4 < 0)
		    {
		      return 0xffffffffffffffff;
		    }
		  if (lStack48 < lVar2)
		    {
		      lStack48 = lVar2;
		    }
		}
	    }
	  else
	    {
	      lStack48 = *(long *) (pcVar6 + 0x30);
	    }
	  lVar2 = uStack40 + (lStack48 - lVar2);
	  if (lVar2 == 0x7fffffffffffffff)
	    {
	      puVar5 = (undefined4 *) func_0x00401e70 ();
	      *puVar5 = 0x4b;
	      lVar2 = -1;
	    }
	}
    }
  return lVar2;
}

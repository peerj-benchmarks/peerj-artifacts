
#include "du.h"

long null_ARRAY_00644fb_0_8_;
long null_ARRAY_00644fb_8_8_;
long null_ARRAY_0064517_0_8_;
long null_ARRAY_006451c_0_8_;
long null_ARRAY_006451c_16_8_;
long null_ARRAY_006451c_24_8_;
long null_ARRAY_006451c_32_8_;
long null_ARRAY_006451c_40_8_;
long null_ARRAY_006451c_48_8_;
long null_ARRAY_006451c_8_8_;
long null_ARRAY_0064532_0_4_;
long null_ARRAY_0064532_16_8_;
long null_ARRAY_0064532_4_4_;
long null_ARRAY_0064532_8_4_;
long local_a_0_4_;
long DAT_00439e8f;
long DAT_00439f45;
long DAT_00439fc3;
long DAT_0043b156;
long DAT_0043b159;
long DAT_0043b1bf;
long DAT_0043b2bf;
long DAT_0043b339;
long DAT_0043b4f4;
long DAT_0043b4f8;
long DAT_0043b4fa;
long DAT_0043be3c;
long DAT_0043beaa;
long DAT_0043bec2;
long DAT_0043c2bf;
long DAT_0043c799;
long DAT_0043c79b;
long DAT_0043c7d8;
long DAT_0043c908;
long DAT_0043ca5e;
long DAT_0043ca62;
long DAT_0043ca67;
long DAT_0043ca69;
long DAT_0043d0d3;
long DAT_0043d4a0;
long DAT_0043dbb8;
long DAT_0043dfa8;
long DAT_0043dfad;
long DAT_0043e630;
long DAT_0043e631;
long DAT_0043e633;
long DAT_0043e6f7;
long DAT_0043e788;
long DAT_0043e78b;
long DAT_0043e7d9;
long DAT_0043e7f5;
long DAT_0043e818;
long DAT_0043e839;
long DAT_0043e887;
long DAT_0043e892;
long DAT_0043e8a8;
long DAT_0043eb00;
long DAT_0043f2b0;
long DAT_0043f2b8;
long DAT_0043f3e8;
long DAT_0043f569;
long DAT_0043f5a2;
long DAT_0043f618;
long DAT_0043f619;
long DAT_00644a10;
long DAT_00644a20;
long DAT_00644a30;
long DAT_00644f80;
long DAT_00644f98;
long DAT_00644fa0;
long DAT_00645018;
long DAT_0064501c;
long DAT_00645020;
long DAT_00645040;
long DAT_00645048;
long DAT_00645050;
long DAT_00645060;
long DAT_00645068;
long DAT_00645080;
long DAT_00645088;
long DAT_006450e0;
long DAT_006450e8;
long DAT_006450f0;
long DAT_006450f8;
long DAT_006450f9;
long DAT_006450fa;
long DAT_006450fb;
long DAT_006450fc;
long DAT_006450fd;
long DAT_006450fe;
long DAT_00645100;
long DAT_00645108;
long DAT_0064510c;
long DAT_0064510d;
long DAT_00645110;
long DAT_00645118;
long DAT_00645120;
long DAT_00645128;
long DAT_00645130;
long DAT_00645138;
long DAT_00645160;
long DAT_00645168;
long DAT_00645180;
long DAT_00645188;
long DAT_00645190;
long DAT_00645300;
long DAT_00645358;
long DAT_00645360;
long DAT_00645368;
long DAT_006453b8;
long DAT_006453c0;
long DAT_00645c08;
long DAT_00645c10;
long DAT_00645c20;
long fde_00440f10;
long FLOAT_UNKNOWN;
long null_ARRAY_0043a100;
long null_ARRAY_0043a460;
long null_ARRAY_0043a490;
long null_ARRAY_0043a4c0;
long null_ARRAY_0043a4e0;
long null_ARRAY_0043c6e0;
long null_ARRAY_0043c790;
long null_ARRAY_0043c7c0;
long null_ARRAY_0043eae0;
long null_ARRAY_0043f640;
long null_ARRAY_0043f680;
long null_ARRAY_0043fae0;
long null_ARRAY_00644fb0;
long null_ARRAY_00644fe0;
long null_ARRAY_006450a0;
long null_ARRAY_00645140;
long null_ARRAY_00645170;
long null_ARRAY_006451c0;
long null_ARRAY_00645200;
long null_ARRAY_00645320;
long null_ARRAY_00645400;
long PTR_DAT_00644f88;
long PTR_FUN_00644f90;
long PTR_null_ARRAY_00644fc0;
long PTR_null_ARRAY_00645028;
long register0x00000020;
ulong
FUN_00402dd7 (uint uParm1)
{
  int iVar1;
  ulong uVar2;
  undefined8 extraout_RDX;
  char *pcVar3;

  if (uParm1 == 0)
    {
      func_0x004021c0
	("Usage: %s [OPTION]... [FILE]...\n  or:  %s [OPTION]... --files0-from=F\n",
	 DAT_00645190, DAT_00645190);
      func_0x004024b0
	("Summarize disk usage of the set of FILEs, recursively for directories.\n",
	 DAT_00645040);
      FUN_00402a8e ();
      func_0x004024b0
	("  -0, --null            end each output line with NUL, not newline\n  -a, --all             write counts for all files, not just directories\n      --apparent-size   print apparent sizes, rather than disk usage; although\n                          the apparent size is usually smaller, it may be\n                          larger due to holes in (\'sparse\') files, internal\n                          fragmentation, indirect blocks, and the like\n",
	 DAT_00645040);
      func_0x004024b0
	("  -B, --block-size=SIZE  scale sizes by SIZE before printing them; e.g.,\n                           \'-BM\' prints sizes in units of 1,048,576 bytes;\n                           see SIZE format below\n  -b, --bytes           equivalent to \'--apparent-size --block-size=1\'\n  -c, --total           produce a grand total\n  -D, --dereference-args  dereference only symlinks that are listed on the\n                          command line\n  -d, --max-depth=N     print the total for a directory (or file, with --all)\n                          only if it is N or fewer levels below the command\n                          line argument;  --max-depth=0 is the same as\n                          --summarize\n",
	 DAT_00645040);
      func_0x004024b0
	("      --files0-from=F   summarize disk usage of the\n                          NUL-terminated file names specified in file F;\n                          if F is -, then read names from standard input\n  -H                    equivalent to --dereference-args (-D)\n  -h, --human-readable  print sizes in human readable format (e.g., 1K 234M 2G)\n      --inodes          list inode usage information instead of block usage\n",
	 DAT_00645040);
      func_0x004024b0
	("  -k                    like --block-size=1K\n  -L, --dereference     dereference all symbolic links\n  -l, --count-links     count sizes many times if hard linked\n  -m                    like --block-size=1M\n",
	 DAT_00645040);
      func_0x004024b0
	("  -P, --no-dereference  don\'t follow any symbolic links (this is the default)\n  -S, --separate-dirs   for directories do not include size of subdirectories\n      --si              like -h, but use powers of 1000 not 1024\n  -s, --summarize       display only a total for each argument\n",
	 DAT_00645040);
      func_0x004024b0
	("  -t, --threshold=SIZE  exclude entries smaller than SIZE if positive,\n                          or entries greater than SIZE if negative\n      --time            show time of the last modification of any file in the\n                          directory, or any of its subdirectories\n      --time=WORD       show time as WORD instead of modification time:\n                          atime, access, use, ctime or status\n      --time-style=STYLE  show times using STYLE, which can be:\n                            full-iso, long-iso, iso, or +FORMAT;\n                            FORMAT is interpreted like in \'date\'\n",
	 DAT_00645040);
      func_0x004024b0
	("  -X, --exclude-from=FILE  exclude files that match any pattern in FILE\n      --exclude=PATTERN    exclude files that match PATTERN\n  -x, --one-file-system    skip directories on different file systems\n",
	 DAT_00645040);
      func_0x004024b0 ("      --help     display this help and exit\n",
		       DAT_00645040);
      pcVar3 = DAT_00645040;
      func_0x004024b0
	("      --version  output version information and exit\n");
      FUN_00402ac2 (&DAT_0043b156);
      FUN_00402aa8 ();
      imperfection_wrapper ();	//    FUN_00402ae6();
    }
  else
    {
      pcVar3 = "Try \'%s --help\' for more information.\n";
      func_0x004024a0 (DAT_00645060,
		       "Try \'%s --help\' for more information.\n",
		       DAT_00645190);
    }
  uVar2 = (ulong) uParm1;
  func_0x00402780 ();
  imperfection_wrapper ();	//  uVar2 = FUN_00404fff(uVar2, extraout_RDX, pcVar3, extraout_RDX);
  iVar1 = (int) uVar2;
  if (iVar1 < 0)
    {
      uVar2 = FUN_0040d011 ();
    }
  return uVar2 & 0xffffffffffffff00 | (ulong) (iVar1 != 0);
}

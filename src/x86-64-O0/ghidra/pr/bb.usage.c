
#include "pr.h"

long null_ARRAY_00621eb_0_8_;
long null_ARRAY_00621eb_8_8_;
long null_ARRAY_006220c_0_8_;
long null_ARRAY_006220c_8_8_;
long null_ARRAY_0062210_0_8_;
long null_ARRAY_0062210_16_8_;
long null_ARRAY_0062210_24_8_;
long null_ARRAY_0062210_32_8_;
long null_ARRAY_0062210_40_8_;
long null_ARRAY_0062210_48_8_;
long null_ARRAY_0062210_8_8_;
long null_ARRAY_0062226_0_4_;
long null_ARRAY_0062226_16_8_;
long null_ARRAY_0062226_4_4_;
long null_ARRAY_0062226_8_4_;
long DAT_0041a9c3;
long DAT_0041aa7d;
long DAT_0041aafb;
long DAT_0041ab33;
long DAT_0041b191;
long DAT_0041b1c2;
long DAT_0041bad3;
long DAT_0041bad5;
long DAT_0041bad7;
long DAT_0041bae2;
long DAT_0041bb4b;
long DAT_0041cace;
long DAT_0041cad1;
long DAT_0041cf20;
long DAT_0041d05e;
long DAT_0041d062;
long DAT_0041d067;
long DAT_0041d069;
long DAT_0041d6d3;
long DAT_0041daa0;
long DAT_0041e198;
long DAT_0041e588;
long DAT_0041e58d;
long DAT_0041e5f7;
long DAT_0041e688;
long DAT_0041e68b;
long DAT_0041e6d9;
long DAT_0041e6e9;
long DAT_0041e760;
long DAT_0041e761;
long DAT_0041e78e;
long DAT_0041e799;
long DAT_006219c8;
long DAT_006219d8;
long DAT_006219e8;
long DAT_00621e20;
long DAT_00621e21;
long DAT_00621e24;
long DAT_00621e28;
long DAT_00621e2c;
long DAT_00621e30;
long DAT_00621e34;
long DAT_00621e38;
long DAT_00621e3c;
long DAT_00621e40;
long DAT_00621e48;
long DAT_00621e4c;
long DAT_00621e50;
long DAT_00621e54;
long DAT_00621e58;
long DAT_00621e80;
long DAT_00621ea0;
long DAT_00621f18;
long DAT_00621f1c;
long DAT_00621f20;
long DAT_00621f40;
long DAT_00621f48;
long DAT_00621f50;
long DAT_00621f60;
long DAT_00621f68;
long DAT_00621f80;
long DAT_00621f88;
long DAT_00621fd0;
long DAT_00621fd8;
long DAT_00621fe0;
long DAT_00621fe8;
long DAT_00621ff0;
long DAT_00621ff8;
long DAT_00622000;
long DAT_00622001;
long DAT_00622002;
long DAT_00622003;
long DAT_00622004;
long DAT_00622005;
long DAT_00622006;
long DAT_00622007;
long DAT_00622008;
long DAT_00622009;
long DAT_0062200a;
long DAT_0062200b;
long DAT_0062200c;
long DAT_00622010;
long DAT_00622011;
long DAT_00622014;
long DAT_00622018;
long DAT_00622019;
long DAT_0062201c;
long DAT_00622020;
long DAT_00622024;
long DAT_00622028;
long DAT_0062202c;
long DAT_00622030;
long DAT_00622038;
long DAT_00622040;
long DAT_00622048;
long DAT_0062204c;
long DAT_00622050;
long DAT_00622058;
long DAT_00622060;
long DAT_00622061;
long DAT_00622062;
long DAT_00622064;
long DAT_00622068;
long DAT_00622069;
long DAT_0062206c;
long DAT_00622070;
long DAT_00622074;
long DAT_00622078;
long DAT_00622080;
long DAT_00622088;
long DAT_00622090;
long DAT_00622098;
long DAT_006220a0;
long DAT_006220a8;
long DAT_006220b0;
long DAT_006220b8;
long DAT_006220d0;
long DAT_006220d8;
long DAT_006220e0;
long DAT_00622240;
long DAT_006222d8;
long DAT_006222e0;
long DAT_006222e8;
long DAT_00622b08;
long DAT_00622b10;
long DAT_00622b20;
long fde_00420248;
long FLOAT_UNKNOWN;
long null_ARRAY_0041acc0;
long null_ARRAY_0041e7c0;
long null_ARRAY_0041f140;
long null_ARRAY_0041f260;
long null_ARRAY_0041f6c0;
long null_ARRAY_00621eb0;
long null_ARRAY_00621ee0;
long null_ARRAY_00621fa0;
long null_ARRAY_006220c0;
long null_ARRAY_00622100;
long null_ARRAY_00622140;
long null_ARRAY_00622260;
long null_ARRAY_00622300;
long PTR_DAT_00621e60;
long PTR_DAT_00621e68;
long PTR_DAT_00621e70;
long PTR_DAT_00621e78;
long PTR_null_ARRAY_00621ec0;
long PTR_null_ARRAY_00621f28;
long register0x00000020;
long stack0x00000008;
void
FUN_004056f0 (uint uParm1)
{
  int iVar1;
  int *piVar2;
  undefined8 uVar3;
  uint *puVar4;

  if (uParm1 == 0)
    {
      func_0x00401980 ("Usage: %s [OPTION]... [FILE]...\n", DAT_006220e0);
      func_0x00401b70 ("Paginate or columnate FILE(s) for printing.\n",
		       DAT_00621f40);
      FUN_0040203d ();
      FUN_00402057 ();
      func_0x00401b70
	("  +FIRST_PAGE[:LAST_PAGE], --pages=FIRST_PAGE[:LAST_PAGE]\n                    begin [stop] printing with page FIRST_[LAST_]PAGE\n  -COLUMN, --columns=COLUMN\n                    output COLUMN columns and print columns down,\n                    unless -a is used. Balance number of lines in the\n                    columns on each page\n",
	 DAT_00621f40);
      func_0x00401b70
	("  -a, --across      print columns across rather than down, used together\n                    with -COLUMN\n  -c, --show-control-chars\n                    use hat notation (^G) and octal backslash notation\n  -d, --double-space\n                    double space the output\n",
	 DAT_00621f40);
      func_0x00401b70
	("  -D, --date-format=FORMAT\n                    use FORMAT for the header date\n  -e[CHAR[WIDTH]], --expand-tabs[=CHAR[WIDTH]]\n                    expand input CHARs (TABs) to tab WIDTH (8)\n  -F, -f, --form-feed\n                    use form feeds instead of newlines to separate pages\n                    (by a 3-line page header with -F or a 5-line header\n                    and trailer without -F)\n",
	 DAT_00621f40);
      func_0x00401b70
	("  -h, --header=HEADER\n                    use a centered HEADER instead of filename in page header,\n                    -h \"\" prints a blank line, don\'t use -h\"\"\n  -i[CHAR[WIDTH]], --output-tabs[=CHAR[WIDTH]]\n                    replace spaces with CHARs (TABs) to tab WIDTH (8)\n  -J, --join-lines  merge full lines, turns off -W line truncation, no column\n                    alignment, --sep-string[=STRING] sets separators\n",
	 DAT_00621f40);
      func_0x00401b70
	("  -l, --length=PAGE_LENGTH\n                    set the page length to PAGE_LENGTH (66) lines\n                    (default number of lines of text 56, and with -F 63).\n                    implies -t if PAGE_LENGTH <= 10\n",
	 DAT_00621f40);
      func_0x00401b70
	("  -m, --merge       print all files in parallel, one in each column,\n                    truncate lines, but join lines of full length with -J\n",
	 DAT_00621f40);
      func_0x00401b70
	("  -n[SEP[DIGITS]], --number-lines[=SEP[DIGITS]]\n                    number lines, use DIGITS (5) digits, then SEP (TAB),\n                    default counting starts with 1st line of input file\n  -N, --first-line-number=NUMBER\n                    start counting with NUMBER at 1st line of first\n                    page printed (see +FIRST_PAGE)\n",
	 DAT_00621f40);
      func_0x00401b70
	("  -o, --indent=MARGIN\n                    offset each line with MARGIN (zero) spaces, do not\n                    affect -w or -W, MARGIN will be added to PAGE_WIDTH\n  -r, --no-file-warnings\n                    omit warning when a file cannot be opened\n",
	 DAT_00621f40);
      func_0x00401b70
	("  -s[CHAR], --separator[=CHAR]\n                    separate columns by a single character, default for CHAR\n                    is the <TAB> character without -w and \'no char\' with -w.\n                    -s[CHAR] turns off line truncation of all 3 column\n                    options (-COLUMN|-a -COLUMN|-m) except -w is set\n",
	 DAT_00621f40);
      func_0x00401b70
	("  -S[STRING], --sep-string[=STRING]\n                    separate columns by STRING,\n                    without -S: Default separator <TAB> with -J and <space>\n                    otherwise (same as -S\" \"), no effect on column options\n",
	 DAT_00621f40);
      func_0x00401b70
	("  -t, --omit-header  omit page headers and trailers;\n                     implied if PAGE_LENGTH <= 10\n",
	 DAT_00621f40);
      func_0x00401b70
	("  -T, --omit-pagination\n                    omit page headers and trailers, eliminate any pagination\n                    by form feeds set in input files\n  -v, --show-nonprinting\n                    use octal backslash notation\n  -w, --width=PAGE_WIDTH\n                    set page width to PAGE_WIDTH (72) characters for\n                    multiple text-column output only, -s[char] turns off (72)\n",
	 DAT_00621f40);
      func_0x00401b70
	("  -W, --page-width=PAGE_WIDTH\n                    set page width to PAGE_WIDTH (72) characters always,\n                    truncate lines, except -J option is set, no interference\n                    with -S or -s\n",
	 DAT_00621f40);
      func_0x00401b70 ("      --help     display this help and exit\n",
		       DAT_00621f40);
      func_0x00401b70
	("      --version  output version information and exit\n",
	 DAT_00621f40);
      FUN_00402071 (&DAT_0041b191);
    }
  else
    {
      func_0x00401b60 (DAT_00621f60,
		       "Try \'%s --help\' for more information.\n",
		       DAT_006220e0);
    }
  func_0x00401d70 ((ulong) uParm1);
  iVar1 = FUN_0040d459 (DAT_00621f40);
  if (iVar1 != 0)
    {
      if (DAT_006220d8 == '\x01')
	{
	  piVar2 = (int *) func_0x00401d60 ();
	  if (*piVar2 == 0x20)
	    goto LAB_0040595b;
	}
      if (DAT_006220d0 == 0)
	{
	  puVar4 = (uint *) func_0x00401d60 ();
	  imperfection_wrapper ();	//      FUN_0040b182(0, (ulong)*puVar4, &DAT_0041cace, "write error");
	}
      else
	{
	  imperfection_wrapper ();	//      uVar3 = FUN_0040968f(DAT_006220d0);
	  puVar4 = (uint *) func_0x00401d60 ();
	  imperfection_wrapper ();	//      FUN_0040b182(0, (ulong)*puVar4, "%s: %s", uVar3, "write error");
	}
      func_0x00401dc0 ((ulong) DAT_00621e80);
    }
LAB_0040595b:
  ;
  iVar1 = FUN_0040d459 (DAT_00621f60);
  if (iVar1 != 0)
    {
      func_0x00401dc0 ((ulong) DAT_00621e80);
    }
  return;
}

typedef unsigned char   undefined;

typedef unsigned char    byte;
typedef unsigned char    dwfenc;
typedef unsigned int    dword;
typedef unsigned long    qword;
typedef unsigned char    undefined1;
typedef unsigned short    undefined2;
typedef unsigned int    undefined4;
typedef unsigned long    undefined6;
typedef unsigned long    undefined8;
typedef unsigned short    word;
#define unkbyte9   unsigned long long
#define unkbyte10   unsigned long long
#define unkbyte11   unsigned long long
#define unkbyte12   unsigned long long
#define unkbyte13   unsigned long long
#define unkbyte14   unsigned long long
#define unkbyte15   unsigned long long
#define unkbyte16   unsigned long long

#define unkuint9   unsigned long long
#define unkuint10   unsigned long long
#define unkuint11   unsigned long long
#define unkuint12   unsigned long long
#define unkuint13   unsigned long long
#define unkuint14   unsigned long long
#define unkuint15   unsigned long long
#define unkuint16   unsigned long long

#define unkint9   long long
#define unkint10   long long
#define unkint11   long long
#define unkint12   long long
#define unkint13   long long
#define unkint14   long long
#define unkint15   long long
#define unkint16   long long

#define unkfloat1   float
#define unkfloat2   float
#define unkfloat3   float
#define unkfloat5   double
#define unkfloat6   double
#define unkfloat7   double
#define unkfloat9   long double
#define unkfloat11   long double
#define unkfloat12   long double
#define unkfloat13   long double
#define unkfloat14   long double
#define unkfloat15   long double
#define unkfloat16   long double

#define BADSPACEBASE   void
#define code   void

#define ulong long
#define true 1
#define bool int
#define false 0
#define float10 int
#define ushort int
#define undefined7 long
#define undefined2 long
#define uint5 long
#define uint6 long

// Add definition for uint
typedef unsigned int    uint;

#define sbyte char

extern imperfection_wrapper(void);
typedef struct eh_frame_hdr eh_frame_hdr, *Peh_frame_hdr;

struct eh_frame_hdr {
    byte eh_frame_hdr_version; // Exception Handler Frame Header Version
    dwfenc eh_frame_pointer_encoding; // Exception Handler Frame Pointer Encoding
    dwfenc eh_frame_desc_entry_count_encoding; // Encoding of # of Exception Handler FDEs
    dwfenc eh_frame_table_encoding; // Exception Handler Table Encoding
};

typedef struct fde_table_entry fde_table_entry, *Pfde_table_entry;

struct fde_table_entry {
    dword initial_loc; // Initial Location
    dword data_loc; // Data location
};

typedef enum Elf64_DynTag {
    DT_AUDIT=1879047932,
    DT_AUXILIARY=2147483645,
    DT_BIND_NOW=24,
    DT_CHECKSUM=1879047672,
    DT_CONFIG=1879047930,
    DT_DEBUG=21,
    DT_DEPAUDIT=1879047931,
    DT_ENCODING=32,
    DT_FEATURE_1=1879047676,
    DT_FILTER=2147483647,
    DT_FINI=13,
    DT_FINI_ARRAY=26,
    DT_FINI_ARRAYSZ=28,
    DT_FLAGS=30,
    DT_FLAGS_1=1879048187,
    DT_GNU_CONFLICT=1879047928,
    DT_GNU_CONFLICTSZ=1879047670,
    DT_GNU_HASH=1879047925,
    DT_GNU_LIBLIST=1879047929,
    DT_GNU_LIBLISTSZ=1879047671,
    DT_GNU_PRELINKED=1879047669,
    DT_HASH=4,
    DT_INIT=12,
    DT_INIT_ARRAY=25,
    DT_INIT_ARRAYSZ=27,
    DT_JMPREL=23,
    DT_MOVEENT=1879047674,
    DT_MOVESZ=1879047675,
    DT_MOVETAB=1879047934,
    DT_NEEDED=1,
    DT_NULL=0,
    DT_PLTGOT=3,
    DT_PLTPAD=1879047933,
    DT_PLTPADSZ=1879047673,
    DT_PLTREL=20,
    DT_PLTRELSZ=2,
    DT_POSFLAG_1=1879047677,
    DT_PREINIT_ARRAYSZ=33,
    DT_REL=17,
    DT_RELA=7,
    DT_RELACOUNT=1879048185,
    DT_RELAENT=9,
    DT_RELASZ=8,
    DT_RELCOUNT=1879048186,
    DT_RELENT=19,
    DT_RELSZ=18,
    DT_RPATH=15,
    DT_RUNPATH=29,
    DT_SONAME=14,
    DT_STRSZ=10,
    DT_STRTAB=5,
    DT_SYMBOLIC=16,
    DT_SYMENT=11,
    DT_SYMINENT=1879047679,
    DT_SYMINFO=1879047935,
    DT_SYMINSZ=1879047678,
    DT_SYMTAB=6,
    DT_TEXTREL=22,
    DT_TLSDESC_GOT=1879047927,
    DT_TLSDESC_PLT=1879047926,
    DT_VERDEF=1879048188,
    DT_VERDEFNUM=1879048189,
    DT_VERNEED=1879048190,
    DT_VERNEEDNUM=1879048191,
    DT_VERSYM=1879048176
} Elf64_DynTag;

typedef enum Elf_ProgramHeaderType {
    PT_DYNAMIC=2,
    PT_GNU_EH_FRAME=1685382480,
    PT_GNU_RELRO=1685382482,
    PT_GNU_STACK=1685382481,
    PT_INTERP=3,
    PT_LOAD=1,
    PT_NOTE=4,
    PT_NULL=0,
    PT_PHDR=6,
    PT_SHLIB=5,
    PT_TLS=7
} Elf_ProgramHeaderType;

typedef struct Elf64_Dyn Elf64_Dyn, *PElf64_Dyn;

struct Elf64_Dyn {
    enum Elf64_DynTag d_tag;
    qword d_val;
};

typedef struct Elf64_Shdr Elf64_Shdr, *PElf64_Shdr;

typedef enum Elf_SectionHeaderType {
    SHT_CHECKSUM=1879048184,
    SHT_DYNAMIC=6,
    SHT_DYNSYM=11,
    SHT_FINI_ARRAY=15,
    SHT_GNU_ATTRIBUTES=1879048181,
    SHT_GNU_HASH=1879048182,
    SHT_GNU_LIBLIST=1879048183,
    SHT_GNU_verdef=1879048189,
    SHT_GNU_verneed=1879048190,
    SHT_GNU_versym=1879048191,
    SHT_GROUP=17,
    SHT_HASH=5,
    SHT_INIT_ARRAY=14,
    SHT_NOBITS=8,
    SHT_NOTE=7,
    SHT_NULL=0,
    SHT_PREINIT_ARRAY=16,
    SHT_PROGBITS=1,
    SHT_REL=9,
    SHT_RELA=4,
    SHT_SHLIB=10,
    SHT_STRTAB=3,
    SHT_SUNW_COMDAT=1879048187,
    SHT_SUNW_move=1879048186,
    SHT_SUNW_syminfo=1879048188,
    SHT_SYMTAB=2,
    SHT_SYMTAB_SHNDX=18
} Elf_SectionHeaderType;

struct Elf64_Shdr {
    dword sh_name;
    enum Elf_SectionHeaderType sh_type;
    qword sh_flags;
    qword sh_addr;
    qword sh_offset;
    qword sh_size;
    dword sh_link;
    dword sh_info;
    qword sh_addralign;
    qword sh_entsize;
};

typedef struct Elf64_Phdr Elf64_Phdr, *PElf64_Phdr;

struct Elf64_Phdr {
    enum Elf_ProgramHeaderType p_type;
    dword p_flags;
    qword p_offset;
    qword p_vaddr;
    qword p_paddr;
    qword p_filesz;
    qword p_memsz;
    qword p_align;
};

typedef struct Elf64_Rela Elf64_Rela, *PElf64_Rela;

struct Elf64_Rela {
    qword r_offset; // location to apply the relocation action
    qword r_info; // the symbol table index and the type of relocation
    qword r_addend; // a constant addend used to compute the relocatable field value
};

typedef struct Elf64_Sym Elf64_Sym, *PElf64_Sym;

struct Elf64_Sym {
    dword st_name;
    byte st_info;
    byte st_other;
    word st_shndx;
    qword st_value;
    qword st_size;
};

typedef struct Elf64_Ehdr Elf64_Ehdr, *PElf64_Ehdr;

struct Elf64_Ehdr {
    byte e_ident_magic_num;
    char e_ident_magic_str[3];
    byte e_ident_class;
    byte e_ident_data;
    byte e_ident_version;
    byte e_ident_pad[9];
    word e_type;
    word e_machine;
    dword e_version;
    qword e_entry;
    qword e_phoff;
    qword e_shoff;
    dword e_flags;
    word e_ehsize;
    word e_phentsize;
    word e_phnum;
    word e_shentsize;
    word e_shnum;
    word e_shstrndx;
};




undefined8 _DT_INIT(void);
void FUN_004014a0(void);
void entry(void);
void FUN_00401930(void);
void FUN_004019b0(void);
void FUN_00401a30(void);
void FUN_00401a6e(int iParm1);
ulong FUN_00401a86(byte bParm1);
void FUN_00401a95(undefined4 uParm1,undefined4 uParm2,undefined4 uParm3,undefined4 uParm4,undefined4 uParm5,undefined4 uParm6,undefined4 uParm7,undefined4 uParm8,undefined8 param_1,undefined8 param_2,undefined8 param_3,undefined8 param_4,undefined8 param_5,undefined8 param_6);
void FUN_00401b48(char cParm1);
void FUN_00401b81(void);
char * FUN_00401ba0(void);
char * FUN_00401bdc(char *pcParm1);
ulong FUN_00401cd5(undefined8 uParm1,undefined8 *puParm2);
ulong FUN_00401d38(undefined8 uParm1);
ulong FUN_00401e65(void);
ulong FUN_00402298(char cParm1);
ulong FUN_00402b85(void);
ulong FUN_004032cb(void);
ulong FUN_0040333c(void);
void FUN_004033ad(void);
undefined8 FUN_004033cd(char *pcParm1);
ulong FUN_00403417(void);
ulong FUN_00403447(void);
ulong FUN_00403585(void);
ulong FUN_0040373e(int iParm1);
undefined8 FUN_0040387b(int iParm1,undefined8 *puParm2);
void FUN_0040395d(void);
char * FUN_00403a42(ulong uParm1,long lParm2);
void FUN_00403ac7(long lParm1);
ulong FUN_00403ba4(undefined1 *puParm1,byte bParm2,uint uParm3);
char * FUN_00403c2c(char *pcParm1,int iParm2);
ulong FUN_00403ccc(long param_1,ulong param_2,long param_3,ulong param_4,uint param_5,uint param_6,long param_7,char *param_8,char *param_9);
undefined1 * FUN_00404b96(int iParm1,undefined8 uParm2,undefined8 uParm3,uint *puParm4);
void FUN_00404e09(undefined8 uParm1,undefined8 uParm2,char cParm3);
void FUN_00404e9d(undefined8 uParm1,char cParm2);
void FUN_00404ec7(undefined8 uParm1);
void FUN_00404ee6(uint uParm1,undefined8 uParm2,undefined8 uParm3);
void FUN_00404f12(uint uParm1,undefined8 uParm2);
void FUN_00404f3b(undefined8 uParm1);
void FUN_00404f5a(long lParm1);
ulong FUN_00404f70(uint uParm1);
ulong FUN_00404f80(char *pcParm1,char *pcParm2,char cParm3);
ulong FUN_004050e3(byte *pbParm1,byte *pbParm2,uint uParm3,uint uParm4);
void FUN_00405553(undefined8 uParm1,undefined8 uParm2);
ulong FUN_0040557f(long lParm1,int iParm2,long lParm3,int iParm4);
void FUN_00405606(uint uParm1,uint uParm2,undefined8 uParm3,undefined8 uParm4);
void FUN_00405643(uint uParm1,uint uParm2,long lParm3,uint uParm4,undefined8 uParm5,undefined8 uParm6);
void FUN_004056fd(undefined8 uParm1);
long FUN_00405717(long lParm1);
long FUN_0040574c(long lParm1,long lParm2);
undefined * FUN_004057ad(void);
undefined * FUN_004057d7(long lParm1,ulong *puParm2);
undefined8 FUN_00405959(char *pcParm1,undefined8 uParm2);
ulong FUN_004059fd(uint uParm1);
void FUN_00405a26(void);
void FUN_00405a5a(uint uParm1);
void FUN_00405ace(uint uParm1,uint uParm2,undefined8 uParm3,undefined8 uParm4);
void FUN_00405b52(undefined4 uParm1,undefined4 uParm2,undefined4 uParm3,undefined4 uParm4,undefined4 uParm5,undefined4 uParm6,undefined4 uParm7,undefined4 uParm8,uint param_1,uint param_2,undefined8 param_3,undefined8 param_4,undefined8 param_5,undefined8 param_6);
void FUN_00405c36(undefined4 uParm1,undefined4 uParm2,undefined4 uParm3,undefined4 uParm4,undefined4 uParm5,undefined4 uParm6,undefined4 uParm7,undefined4 uParm8,uint param_1,uint param_2,long param_3,uint param_4,undefined8 param_5,undefined8 param_6);
void FUN_00405dca(undefined8 uParm1,undefined8 uParm2);
ulong FUN_00405def(long lParm1,long lParm2);
undefined8 FUN_00405ea6(long lParm1);
ulong FUN_00405ed7(uint *puParm1,byte *pbParm2,long lParm3,undefined8 uParm4);
undefined8 FUN_00405f5a(long lParm1,long lParm2);
void FUN_00405fc3(undefined8 uParm1,undefined8 uParm2);
ulong FUN_00405fe8(long lParm1,long lParm2);
ulong FUN_00406078(long *plParm1,undefined8 uParm2,undefined8 uParm3);
ulong FUN_004060f2(undefined8 uParm1,undefined8 uParm2,undefined8 uParm3);
ulong FUN_00406213(byte *pbParm1,byte *pbParm2);
undefined8 FUN_0040628a(undefined8 uParm1);
undefined8 FUN_00406315(void);
ulong FUN_00406322(uint uParm1);
undefined1 * FUN_004063f3(void);
char * FUN_004067a6(void);
ulong FUN_00406874(ulong uParm1,long lParm2);
void FUN_004068a8(undefined8 uParm1,undefined8 uParm2,undefined8 uParm3);
void FUN_004068e3(undefined8 uParm1,undefined8 uParm2,undefined8 uParm3,undefined8 uParm4);
ulong FUN_00406934(ulong uParm1,ulong uParm2);
ulong FUN_0040694f(undefined8 uParm1);
void FUN_00406a07(undefined8 uParm1);
void FUN_00406a2b(void);
ulong FUN_00406a39(long lParm1);
undefined8 FUN_00406b09(undefined8 uParm1);
void FUN_00406b28(undefined8 uParm1,undefined8 uParm2,uint uParm3);
ulong FUN_00406b53(void);
long FUN_00406ba0(ulong uParm1,long lParm2,ulong uParm3,long lParm4,long *plParm5);
int * FUN_00406dec(ulong uParm1,uint *puParm2,ulong uParm3,uint *puParm4,ulong *puParm5);
char * FUN_004078cc(ulong uParm1,long lParm2,long lParm3);
long FUN_00407b3a(int *param_1,long *param_2);
long FUN_00407d25(undefined auParm1 [16],int *piParm2,long *plParm3);
undefined8 FUN_004080e4(int iParm1,long lParm2,uint *puParm3,long lParm4,uint uParm5);
void FUN_004088f7(uint param_1);
void FUN_00408941(undefined8 uParm1,uint uParm2);
ulong FUN_00408993(void);
ulong FUN_00408d25(undefined auParm1 [16],undefined8 uParm2,undefined8 uParm3);
ulong FUN_004090fd(char *pcParm1,long lParm2);
undefined8 FUN_00409156(long param_1,long param_2,undefined param_3,int param_4,undefined8 param_5,ulong param_6,int param_7,ulong param_8,int param_9);
long FUN_00409419(long lParm1,char **ppcParm2,long lParm3,undefined8 uParm4);
ulong FUN_00410883(uint uParm1);
void FUN_004108a2(undefined8 uParm1,int *piParm2,undefined8 uParm3);
void FUN_00410943(int *param_1);
undefined8 FUN_00410a02(uint *puParm1,ulong *puParm2);
undefined8 FUN_004111a2(char *pcParm1,ulong *puParm2,char **ppcParm3);
ulong FUN_00412458(undefined auParm1 [16]);
ulong FUN_00412493(void);
void FUN_004124d0(void);
undefined8 _DT_FINI(void);
undefined FUN_00617000();
undefined FUN_00617008();
undefined FUN_00617010();
undefined FUN_00617018();
undefined FUN_00617020();
undefined FUN_00617028();
undefined FUN_00617030();
undefined FUN_00617038();
undefined FUN_00617040();
undefined FUN_00617048();
undefined FUN_00617050();
undefined FUN_00617058();
undefined FUN_00617060();
undefined FUN_00617068();
undefined FUN_00617070();
undefined FUN_00617078();
undefined FUN_00617080();
undefined FUN_00617088();
undefined FUN_00617090();
undefined FUN_00617098();
undefined FUN_006170a0();
undefined FUN_006170a8();
undefined FUN_006170b0();
undefined FUN_006170b8();
undefined FUN_006170c0();
undefined FUN_006170c8();
undefined FUN_006170d0();
undefined FUN_006170d8();
undefined FUN_006170e0();
undefined FUN_006170e8();
undefined FUN_006170f0();
undefined FUN_006170f8();
undefined FUN_00617100();
undefined FUN_00617108();
undefined FUN_00617110();
undefined FUN_00617118();
undefined FUN_00617120();
undefined FUN_00617128();
undefined FUN_00617130();
undefined FUN_00617138();
undefined FUN_00617140();
undefined FUN_00617148();
undefined FUN_00617150();
undefined FUN_00617158();
undefined FUN_00617160();
undefined FUN_00617168();
undefined FUN_00617170();
undefined FUN_00617178();
undefined FUN_00617180();
undefined FUN_00617188();
undefined FUN_00617190();
undefined FUN_00617198();
undefined FUN_006171a0();
undefined FUN_006171a8();
undefined FUN_006171b0();
undefined FUN_006171b8();
undefined FUN_006171c0();
undefined FUN_006171c8();
undefined FUN_006171d0();
undefined FUN_006171d8();
undefined FUN_006171e0();
undefined FUN_006171e8();
undefined FUN_006171f0();
undefined FUN_006171f8();
undefined FUN_00617200();
undefined FUN_00617208();
undefined FUN_00617210();
undefined FUN_00617218();


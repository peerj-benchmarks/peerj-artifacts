
#include "unexpand.h"

long null_ARRAY_0061599_0_8_;
long null_ARRAY_0061599_8_8_;
long null_ARRAY_00615b4_0_8_;
long null_ARRAY_00615b4_16_8_;
long null_ARRAY_00615b4_24_8_;
long null_ARRAY_00615b4_32_8_;
long null_ARRAY_00615b4_40_8_;
long null_ARRAY_00615b4_48_8_;
long null_ARRAY_00615b4_8_8_;
long null_ARRAY_00615c8_0_4_;
long null_ARRAY_00615c8_16_8_;
long null_ARRAY_00615c8_4_4_;
long null_ARRAY_00615c8_8_4_;
long DAT_00412683;
long DAT_0041273d;
long DAT_004127bb;
long DAT_00412b48;
long DAT_00412cdb;
long DAT_00412cde;
long DAT_00412eaf;
long DAT_00412ef8;
long DAT_0041301e;
long DAT_00413022;
long DAT_00413027;
long DAT_00413029;
long DAT_00413693;
long DAT_00413a60;
long DAT_00413a78;
long DAT_00413a7d;
long DAT_00413ae7;
long DAT_00413b78;
long DAT_00413b7b;
long DAT_00413bc9;
long DAT_00413bcd;
long DAT_00413c40;
long DAT_00413c41;
long DAT_00413e28;
long DAT_00615570;
long DAT_00615580;
long DAT_00615590;
long DAT_00615978;
long DAT_00615980;
long DAT_006159f8;
long DAT_006159fc;
long DAT_00615a00;
long DAT_00615a40;
long DAT_00615a48;
long DAT_00615a50;
long DAT_00615a60;
long DAT_00615a68;
long DAT_00615a80;
long DAT_00615a88;
long DAT_00615ad0;
long DAT_00615ad4;
long DAT_00615ad8;
long DAT_00615ae0;
long DAT_00615ae8;
long DAT_00615af0;
long DAT_00615af8;
long DAT_00615b00;
long DAT_00615b08;
long DAT_00615b10;
long DAT_00615b18;
long DAT_00615b20;
long DAT_00615b28;
long DAT_00615b30;
long DAT_00615cb8;
long DAT_00615cc0;
long DAT_00615cc8;
long DAT_00615cd0;
long DAT_00615ce0;
long fde_00414948;
long FLOAT_UNKNOWN;
long null_ARRAY_00412840;
long null_ARRAY_00414080;
long null_ARRAY_00615960;
long null_ARRAY_00615990;
long null_ARRAY_006159c0;
long null_ARRAY_00615aa0;
long null_ARRAY_00615b40;
long null_ARRAY_00615b80;
long null_ARRAY_00615c80;
long PTR_DAT_00615970;
long PTR_null_ARRAY_006159a0;
long stack0x00000008;
void
FUN_00401e6e (uint uParm1)
{
  byte bVar1;
  int iVar2;
  ulong uVar3;
  uint *puVar4;
  char cStack105;
  long lStack104;
  bool bStack89;
  ulong uStack88;
  undefined *puStack80;
  ulong uStack72;
  char cStack58;
  byte bStack57;
  ulong uStack56;
  bool bStack45;
  uint uStack44;
  long lStack40;
  code *pcStack32;
  uint local_c;

  if (uParm1 == 0)
    {
      pcStack32 = (code *) 0x401ebd;
      local_c = uParm1;
      func_0x00401690 ("Usage: %s [OPTION]... [FILE]...\n", DAT_00615b30);
      pcStack32 = (code *) 0x401ed1;
      func_0x00401840
	("Convert blanks in each FILE to tabs, writing to standard output.\n",
	 DAT_00615a40);
      pcStack32 = (code *) 0x401ed6;
      FUN_00401c9e ();
      pcStack32 = (code *) 0x401edb;
      FUN_00401cb8 ();
      pcStack32 = (code *) 0x401eef;
      func_0x00401840
	("  -a, --all        convert all blanks, instead of just initial blanks\n      --first-only  convert only leading sequences of blanks (overrides -a)\n  -t, --tabs=N     have tabs N characters apart instead of 8 (enables -a)\n",
	 DAT_00615a40);
      pcStack32 = (code *) 0x401ef4;
      FUN_00402e46 ();
      pcStack32 = (code *) 0x401f08;
      func_0x00401840 ("      --help     display this help and exit\n",
		       DAT_00615a40);
      pcStack32 = (code *) 0x401f1c;
      func_0x00401840
	("      --version  output version information and exit\n",
	 DAT_00615a40);
      pcStack32 = (code *) 0x401f26;
      FUN_00401cd2 ("unexpand");
    }
  else
    {
      pcStack32 = (code *) 0x401e9f;
      local_c = uParm1;
      func_0x00401830 (DAT_00615a60,
		       "Try \'%s --help\' for more information.\n",
		       DAT_00615b30);
    }
  pcStack32 = FUN_00401f30;
  func_0x00401a00 ((ulong) local_c);
  pcStack32 = (code *) & stack0xfffffffffffffff8;
  lStack40 = FUN_00402c14 (0);
  if (lStack40 == 0)
    {
      return;
    }
  puStack80 = (undefined *) FUN_00404c39 (DAT_00615cc0);
  do
    {
      bStack45 = true;
      uStack56 = 0;
      uStack88 = 0;
      lStack104 = 0;
      bStack57 = 0;
      cStack58 = '\x01';
      uStack72 = 0;
    LAB_00401f94:
      ;
      do
	{
	  uStack44 = func_0x004016f0 (lStack40);
	  if ((int) uStack44 < 0)
	    {
	      lStack40 = FUN_00402c14 (lStack40);
	      if (lStack40 != 0)
		goto LAB_00401f94;
	    }
	  if (bStack45 == false)
	    {
	    LAB_004021b2:
	      ;
	      if ((int) uStack44 < 0)
		{
		  func_0x00401b00 (puStack80);
		  return;
		}
	      iVar2 = func_0x004019b0 ((ulong) uStack44);
	      if (iVar2 < 0)
		{
		  puVar4 = (uint *) func_0x004019f0 ();
		  imperfection_wrapper ();	//          FUN_00404e87(1, (ulong)*puVar4, "write error");
		}
	    }
	  else
	    {
	      iVar2 = func_0x00401720 ((ulong) uStack44);
	      bStack89 = iVar2 != 0;
	      if (!bStack89)
		{
		  if (uStack44 == 8)
		    {
		      uStack88 = uStack56 - (ulong) (uStack56 != 0);
		      lStack104 = lStack104 - (ulong) (lStack104 != 0);
		      uStack56 = uStack88;
		    }
		  else
		    {
		      uStack56 = uStack56 + 1;
		      if (uStack56 == 0)
			{
			  imperfection_wrapper ();	//              FUN_00404e87(1, 0, "input line is too long");
			}
		    }
		LAB_0040211a:
		  ;
		  if (uStack72 != 0)
		    {
		      if ((1 < uStack72) && (bStack57 != 0))
			{
			  *puStack80 = 9;
			}
		      uVar3 =
			func_0x004019d0 (puStack80, 1, uStack72,
					 DAT_00615a40);
		      if (uVar3 != uStack72)
			{
			  puVar4 = (uint *) func_0x004019f0 ();
			  imperfection_wrapper ();	//              FUN_00404e87(1, (ulong)*puVar4, "write error");
			}
		      uStack72 = 0;
		      bStack57 = 0;
		    }
		  cStack58 = bStack89;
		  if ((DAT_00615ad0 == '\0') && (bStack89 == false))
		    {
		      bVar1 = 0;
		    }
		  else
		    {
		      bVar1 = 1;
		    }
		  bStack45 = (bVar1 & bStack45) != 0;
		  goto LAB_004021b2;
		}
	      imperfection_wrapper ();	//        uStack88 = FUN_00402a9a(uStack56, &lStack104, &cStack105, &lStack104);
	      if (cStack105 != '\0')
		{
		  bStack45 = false;
		}
	      if (bStack45 == false)
		goto LAB_0040211a;
	      if (uStack88 < uStack56)
		{
		  imperfection_wrapper ();	//          FUN_00404e87(1, 0, "input line is too long");
		}
	      if (uStack44 == 9)
		{
		  uStack56 = uStack88;
		  if (uStack72 != 0)
		    {
		      *puStack80 = 9;
		    }
		LAB_004020af:
		  ;
		  uStack72 = (ulong) bStack57;
		  goto LAB_0040211a;
		}
	      uStack56 = uStack56 + 1;
	      if ((cStack58 == '\x01') && (uStack56 == uStack88))
		{
		  uStack44 = 9;
		  *puStack80 = 9;
		  goto LAB_004020af;
		}
	      if (uStack56 == uStack88)
		{
		  bStack57 = 1;
		}
	      puStack80[uStack72] = (char) uStack44;
	      cStack58 = '\x01';
	      uStack72 = uStack72 + 1;
	    }
	}
      while (uStack44 != 10);
    }
  while (true);
}

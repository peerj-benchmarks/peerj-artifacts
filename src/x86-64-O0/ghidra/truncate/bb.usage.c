
#include "truncate.h"

long null_ARRAY_0061641_0_8_;
long null_ARRAY_0061641_8_8_;
long null_ARRAY_0061658_0_8_;
long null_ARRAY_0061658_16_8_;
long null_ARRAY_0061658_24_8_;
long null_ARRAY_0061658_32_8_;
long null_ARRAY_0061658_40_8_;
long null_ARRAY_0061658_48_8_;
long null_ARRAY_0061658_8_8_;
long null_ARRAY_006166c_0_4_;
long null_ARRAY_006166c_16_8_;
long null_ARRAY_006166c_4_4_;
long null_ARRAY_006166c_8_4_;
long DAT_00412769;
long DAT_00412825;
long DAT_004128a3;
long DAT_0041300a;
long DAT_00413050;
long DAT_0041319e;
long DAT_004131a2;
long DAT_004131a7;
long DAT_004131a9;
long DAT_00413813;
long DAT_00413be0;
long DAT_00413f78;
long DAT_00413f7d;
long DAT_00413fe7;
long DAT_00414078;
long DAT_0041407b;
long DAT_004140c9;
long DAT_004140cd;
long DAT_00414140;
long DAT_00414141;
long DAT_00414328;
long DAT_00616000;
long DAT_00616010;
long DAT_00616020;
long DAT_006163e8;
long DAT_00616400;
long DAT_00616478;
long DAT_0061647c;
long DAT_00616480;
long DAT_006164c0;
long DAT_006164d0;
long DAT_006164e0;
long DAT_006164e8;
long DAT_00616500;
long DAT_00616508;
long DAT_00616550;
long DAT_00616551;
long DAT_00616558;
long DAT_00616560;
long DAT_00616568;
long DAT_00616570;
long DAT_006166f8;
long DAT_00616700;
long DAT_00616708;
long DAT_00616718;
long fde_00414e28;
long FLOAT_UNKNOWN;
long null_ARRAY_00412940;
long null_ARRAY_00414580;
long null_ARRAY_00616410;
long null_ARRAY_00616440;
long null_ARRAY_00616520;
long null_ARRAY_00616580;
long null_ARRAY_006165c0;
long null_ARRAY_006166c0;
long PTR_DAT_006163e0;
long PTR_null_ARRAY_00616420;
long stack0x00000008;
ulong
FUN_00401d52 (uint uParm1, undefined8 uParm2, undefined8 uParm3, ulong uParm4,
	      int iParm5)
{
  undefined auVar1[16];
  char cVar2;
  int iVar3;
  undefined8 uVar4;
  uint *puVar5;
  ulong extraout_RDX;
  char *pcVar6;
  uint uVar7;
  ulong uStack264;
  undefined auStack240[48];
  ulong uStack192;
  ulong uStack184;
  ulong uStack88;
  ulong uStack80;
  ulong uStack72;
  ulong uStack64;
  ulong uStack56;
  code *pcStack32;
  uint local_c;

  if (uParm1 == 0)
    {
      pcStack32 = (code *) 0x401da1;
      local_c = uParm1;
      func_0x00401540 ("Usage: %s OPTION... FILE...\n", DAT_00616570);
      pcStack32 = (code *) 0x401db5;
      func_0x004016f0
	("Shrink or extend the size of each FILE to the specified size\n\nA FILE argument that does not exist is created.\n\nIf a FILE is larger than the specified size, the extra data is lost.\nIf a FILE is shorter, it is extended and the extended part (hole)\nreads as zero bytes.\n",
	 DAT_006164c0);
      pcStack32 = (code *) 0x401dba;
      FUN_00401b43 ();
      pcStack32 = (code *) 0x401dce;
      func_0x004016f0 ("  -c, --no-create        do not create any files\n",
		       DAT_006164c0);
      pcStack32 = (code *) 0x401de2;
      func_0x004016f0
	("  -o, --io-blocks        treat SIZE as number of IO blocks instead of bytes\n",
	 DAT_006164c0);
      pcStack32 = (code *) 0x401df6;
      func_0x004016f0
	("  -r, --reference=RFILE  base size on RFILE\n  -s, --size=SIZE        set or adjust the file size by SIZE bytes\n",
	 DAT_006164c0);
      pcStack32 = (code *) 0x401e0a;
      func_0x004016f0 ("      --help     display this help and exit\n",
		       DAT_006164c0);
      pcStack32 = (code *) 0x401e1e;
      func_0x004016f0
	("      --version  output version information and exit\n",
	 DAT_006164c0);
      pcStack32 = (code *) 0x401e23;
      FUN_00401b5d ();
      pcStack32 = (code *) 0x401e37;
      pcVar6 = DAT_006164c0;
      func_0x004016f0
	("\nSIZE may also be prefixed by one of the following modifying characters:\n\'+\' extend by, \'-\' reduce by, \'<\' at most, \'>\' at least,\n\'/\' round down to multiple of, \'%\' round up to multiple of.\n");
      pcStack32 = (code *) 0x401e41;
      imperfection_wrapper ();	//    FUN_00401b77();
    }
  else
    {
      pcVar6 = "Try \'%s --help\' for more information.\n";
      pcStack32 = (code *) 0x401d83;
      local_c = uParm1;
      func_0x004016e0 (DAT_006164e0,
		       "Try \'%s --help\' for more information.\n",
		       DAT_00616570);
    }
  pcStack32 = FUN_00401e4b;
  uVar7 = local_c;
  func_0x00401880 ();
  if (((DAT_00616551 != '\0')
       ||
       ((pcStack32 = (code *) & stack0xfffffffffffffff8, iParm5 != 0
	 && (pcStack32 =
	     (code *) & stack0xfffffffffffffff8, (long) uParm4 < 0))))
      && (pcStack32 = (code *) & stack0xfffffffffffffff8, iVar3 =
	  func_0x004016d0 ((ulong) uVar7, auStack240, auStack240),
	  iVar3 != 0))
    {
      imperfection_wrapper ();	//    uVar4 = FUN_00403ea8(4, pcVar6);
      puVar5 = (uint *) func_0x00401870 ();
      imperfection_wrapper ();	//    FUN_00404e36(0, (ulong)*puVar5, "cannot fstat %s", uVar4);
      return 0;
    }
  uStack264 = extraout_RDX;
  if (DAT_00616551 != '\0')
    {
      if (((long) uStack184 < 1) || (0x2000000000000000 < uStack184))
	{
	  uStack184 = 0x200;
	}
      imperfection_wrapper ();	//    auVar1 = (undefined[16])0x0 << 0x40 | ZEXT816(0x8000000000000000);
      imperfection_wrapper ();	//    uVar4 = SUB168(auVar1 % SEXT816((long)uStack184), 0);
      if (1)
	{
	  uStack80 = uStack184;
	  imperfection_wrapper ();	//      uVar4 = FUN_00403ea8(4, pcVar6, uVar4);
	  imperfection_wrapper ();	//      FUN_00404e36(0, 0, "overflow in %ld * %ld byte blocks for file %s", extraout_RDX, uStack80, uVar4);
	  return 0;
	}
      uStack264 = extraout_RDX * uStack184;
      uStack80 = uStack184;
    }
  if (iParm5 == 0)
    {
      uStack56 = uStack264;
    }
  else
    {
      uStack64 = uParm4;
      if ((long) uParm4 < 0)
	{
	  cVar2 = FUN_00401d13 (auStack240);
	  if (cVar2 == '\0')
	    {
	      uStack72 = func_0x00401660 ((ulong) uVar7, 0, 2);
	      if ((long) uStack72 < 0)
		{
		  imperfection_wrapper ();	//          uVar4 = FUN_00403ea8(4, pcVar6);
		  puVar5 = (uint *) func_0x00401870 ();
		  imperfection_wrapper ();	//          FUN_00404e36(0, (ulong)*puVar5, "cannot get the size of %s", uVar4);
		  return 0;
		}
	    }
	  else
	    {
	      uStack72 = uStack192;
	      if ((long) uStack192 < 0)
		{
		  imperfection_wrapper ();	//          uVar4 = FUN_00403ea8(4, pcVar6);
		  imperfection_wrapper ();	//          FUN_00404e36(0, 0, "%s has unusable, apparently negative size", uVar4);
		  return 0;
		}
	    }
	  uStack64 = uStack72;
	}
      if (iParm5 == 2)
	{
	  uStack56 = uStack64;
	  if (uStack64 <= uStack264)
	    {
	      uStack56 = uStack264;
	    }
	}
      else
	{
	  if (iParm5 == 3)
	    {
	      uStack56 = uStack64;
	      if (uStack264 <= uStack64)
		{
		  uStack56 = uStack264;
		}
	    }
	  else
	    {
	      if (iParm5 == 4)
		{
		  uStack56 = uStack264 * (uStack64 / uStack264);
		}
	      else
		{
		  if (iParm5 == 5)
		    {
		      uStack88 =
			uStack264 * (((uStack64 + uStack264) - 1) /
				     uStack264);
		      uStack56 = uStack88;
		      if ((long) uStack88 < 0)
			{
			  imperfection_wrapper ();	//              uVar4 = FUN_00403ea8(4, pcVar6);
			  imperfection_wrapper ();	//              FUN_00404e36(0, 0, "overflow rounding up size of file %s", uVar4);
			  return 0;
			}
		    }
		  else
		    {
		      if ((long) (0x7fffffffffffffff - uStack64) <
			  (long) uStack264)
			{
			  imperfection_wrapper ();	//              uVar4 = FUN_00403ea8(4, pcVar6);
			  imperfection_wrapper ();	//              FUN_00404e36(0, 0, "overflow extending size of file %s", uVar4);
			  return 0;
			}
		      uStack56 = uStack64 + uStack264;
		    }
		}
	    }
	}
    }
  if ((long) uStack56 < 0)
    {
      uStack56 = 0;
    }
  iVar3 = func_0x00401650 ((ulong) uVar7, uStack56, uStack56);
  if (iVar3 == -1)
    {
      imperfection_wrapper ();	//    uVar4 = FUN_00403ea8(4, pcVar6);
      puVar5 = (uint *) func_0x00401870 ();
      imperfection_wrapper ();	//    FUN_00404e36(0, (ulong)*puVar5, "failed to truncate %s at %ld bytes", uVar4, uStack56);
    }
  return (ulong) (iVar3 != -1);
}


#include "seq.h"

long null_ARRAY_0061643_0_8_;
long null_ARRAY_0061643_8_8_;
long null_ARRAY_006165c_0_8_;
long null_ARRAY_006165c_16_8_;
long null_ARRAY_006165c_24_8_;
long null_ARRAY_006165c_32_8_;
long null_ARRAY_006165c_40_8_;
long null_ARRAY_006165c_48_8_;
long null_ARRAY_006165c_8_8_;
long null_ARRAY_0061670_0_4_;
long null_ARRAY_0061670_16_8_;
long null_ARRAY_0061670_4_4_;
long null_ARRAY_0061670_8_4_;
long DAT_00412f8b;
long DAT_00413045;
long DAT_004130c3;
long DAT_004130fb;
long DAT_004136e8;
long DAT_00413739;
long DAT_0041373c;
long DAT_004137e9;
long DAT_004137fe;
long DAT_00413802;
long DAT_00413806;
long DAT_00413885;
long DAT_004138b4;
long DAT_004138c9;
long DAT_00413910;
long DAT_00413a5e;
long DAT_00413a62;
long DAT_00413a67;
long DAT_00413a69;
long DAT_004140d3;
long DAT_004144f0;
long DAT_00414580;
long DAT_004145b8;
long DAT_004145bb;
long DAT_00414609;
long DAT_00414680;
long DAT_00414681;
long DAT_00616000;
long DAT_00616010;
long DAT_00616020;
long DAT_00616408;
long DAT_00616420;
long DAT_00616498;
long DAT_0061649c;
long DAT_006164a0;
long DAT_006164c0;
long DAT_006164d0;
long DAT_006164e0;
long DAT_006164e8;
long DAT_00616500;
long DAT_00616508;
long DAT_00616550;
long DAT_00616558;
long DAT_00616580;
long DAT_00616588;
long DAT_00616590;
long DAT_00616598;
long DAT_00616738;
long DAT_00616740;
long DAT_00616748;
long DAT_00616758;
long DAT_25686602;
long DAT_6f618058;
long fde_00415380;
long FLOAT_UNKNOWN;
long null_ARRAY_00413140;
long null_ARRAY_00414ac0;
long null_ARRAY_00616430;
long null_ARRAY_00616460;
long null_ARRAY_00616520;
long null_ARRAY_00616560;
long null_ARRAY_006165c0;
long null_ARRAY_00616600;
long null_ARRAY_00616700;
long PTR_DAT_00616400;
long PTR_null_ARRAY_00616440;
long register0x00000020;
long stack0x00000008;
undefined8 *
FUN_00401e39 (uint uParm1)
{
  char cVar1;
  int iVar2;
  undefined8 uVar3;
  undefined8 uVar4;
  long lVar5;
  undefined8 *puVar6;
  char *pcStack128;
  undefined8 uStack112;
  undefined2 uStack104;
  undefined6 uStack102;
  long lStack96;
  int iStack88;
  undefined4 uStack84;
  char *pcStack80;
  ulong uStack72;
  char *pcStack64;
  ulong uStack56;
  code *pcStack32;
  uint local_c;

  if (uParm1 == 0)
    {
      pcStack32 = (code *) 0x401e96;
      local_c = uParm1;
      func_0x00401640
	("Usage: %s [OPTION]... LAST\n  or:  %s [OPTION]... FIRST LAST\n  or:  %s [OPTION]... FIRST INCREMENT LAST\n",
	 DAT_00616598, DAT_00616598, DAT_00616598);
      pcStack32 = (code *) 0x401eaa;
      func_0x00401820
	("Print numbers from FIRST to LAST, in steps of INCREMENT.\n",
	 DAT_006164c0);
      pcStack32 = (code *) 0x401eaf;
      FUN_00401c83 ();
      pcStack32 = (code *) 0x401ec3;
      func_0x00401820
	("  -f, --format=FORMAT      use printf style floating-point FORMAT\n  -s, --separator=STRING   use STRING to separate numbers (default: \\n)\n  -w, --equal-width        equalize width by padding with leading zeroes\n",
	 DAT_006164c0);
      pcStack32 = (code *) 0x401ed7;
      func_0x00401820 ("      --help     display this help and exit\n",
		       DAT_006164c0);
      pcStack32 = (code *) 0x401eeb;
      func_0x00401820
	("      --version  output version information and exit\n",
	 DAT_006164c0);
      pcStack32 = (code *) 0x401eff;
      func_0x00401820
	("\nIf FIRST or INCREMENT is omitted, it defaults to 1.  That is, an\nomitted INCREMENT defaults to 1 even when LAST is smaller than FIRST.\nThe sequence of numbers ends when the sum of the current number and\nINCREMENT would become greater than LAST.\nFIRST, INCREMENT, and LAST are interpreted as floating point values.\nINCREMENT is usually positive if FIRST is smaller than LAST, and\nINCREMENT is usually negative if FIRST is greater than LAST.\nINCREMENT must not be 0; none of FIRST, INCREMENT and LAST may be NaN.\n",
	 DAT_006164c0);
      pcStack32 = (code *) 0x401f13;
      pcStack128 = DAT_006164c0;
      func_0x00401820
	("FORMAT must be suitable for printing one argument of type \'double\';\nit defaults to %.PRECf if FIRST, INCREMENT, and LAST are all fixed point\ndecimal numbers with maximum precision PREC, and to %g otherwise.\n");
      pcStack32 = (code *) 0x401f1d;
      imperfection_wrapper ();	//    FUN_00401c9d();
    }
  else
    {
      pcStack128 = "Try \'%s --help\' for more information.\n";
      pcStack32 = (code *) 0x401e6a;
      local_c = uParm1;
      func_0x00401810 (DAT_006164e0,
		       "Try \'%s --help\' for more information.\n",
		       DAT_00616598);
    }
  puVar6 = (undefined8 *) (ulong) local_c;
  pcStack32 = FUN_00401f27;
  func_0x004019b0 ();
  pcStack32 = (code *) & stack0xfffffffffffffff8;
  imperfection_wrapper ();	//  cVar1 = FUN_004054a2(pcStack128, 0, &uStack112);
  if (cVar1 != '\x01')
    {
      FUN_00404d5c (pcStack128);
      imperfection_wrapper ();	//    FUN_00405768(0, 0, "invalid floating point argument: %s");
      FUN_00401e39 (1);
    }
  if ((false) || (false))
    {
      imperfection_wrapper ();	//    uVar3 = FUN_00404d33(1, pcStack128);
      imperfection_wrapper ();	//    uVar4 = FUN_00404d33(0, "not-a-number");
      imperfection_wrapper ();	//    FUN_00405768(0, 0, "invalid %s argument: %s", uVar4, uVar3);
      FUN_00401e39 (1);
    }
  while (true)
    {
      FUN_00401c74 ((ulong) (uint) (int) *pcStack128);
      imperfection_wrapper ();	//    iVar2 = FUN_00401c4e();
      if ((iVar2 == 0) && (*pcStack128 != '+'))
	break;
      pcStack128 = pcStack128 + 1;
    }
  lStack96 = 0;
  iStack88 = 0x7fffffff;
  pcStack80 = (char *) func_0x00401a40 (pcStack128, 0x2e);
  if ((pcStack80 == (char *) 0x0)
      && (lVar5 = func_0x00401a40 (pcStack128, 0x70), lVar5 == 0))
    {
      iStack88 = 0;
    }
  lVar5 = func_0x00401940 (pcStack128, &DAT_00413739);
  if (((pcStack128[lVar5] == '\0') && (true))
      && ((float10) 0 ==
	  (float10) 0 * (float10) CONCAT28 (uStack104, uStack112)))
    {
      uStack56 = 0;
      lStack96 = func_0x00401a20 (pcStack128);
      if (pcStack80 != (char *) 0x0)
	{
	  uStack56 = func_0x00401940 (pcStack80 + 1, &DAT_0041373c);
	  if (uStack56 < 0x80000000)
	    {
	      iStack88 = (int) uStack56;
	    }
	  if (uStack56 == 0)
	    {
	      lVar5 = -1;
	    }
	  else
	    {
	      if ((pcStack80 == pcStack128)
		  || (9 < (int) pcStack80[-1] - 0x30U))
		{
		  iVar2 = 1;
		}
	      else
		{
		  iVar2 = 0;
		}
	      lVar5 = (long) iVar2;
	    }
	  lStack96 = lVar5 + lStack96;
	}
      pcStack64 = (char *) func_0x00401a40 (pcStack128, 0x65);
      if (pcStack64 == (char *) 0x0)
	{
	  pcStack64 = (char *) func_0x00401a40 (pcStack128, 0x45);
	}
      if (pcStack64 != (char *) 0x0)
	{
	  uStack72 = func_0x004017b0 (pcStack64 + 1, 0, 10);
	  iVar2 = (int) uStack72;
	  if ((-1 < (long) uStack72) && ((long) iStack88 <= (long) uStack72))
	    {
	      iVar2 = iStack88;
	    }
	  iStack88 = iStack88 - iVar2;
	  lVar5 = func_0x00401a20 (pcStack128);
	  lStack96 =
	    lStack96 - (long) (pcStack128 + (lVar5 - (long) pcStack64));
	  if ((long) uStack72 < 0)
	    {
	      if (pcStack80 == (char *) 0x0)
		{
		  lStack96 = lStack96 + 1;
		}
	      else
		{
		  if (pcStack80 + 1 == pcStack64)
		    {
		      lStack96 = lStack96 + 1;
		    }
		}
	      uStack72 = -uStack72;
	    }
	  else
	    {
	      if (((pcStack80 != (char *) 0x0) && (iStack88 == 0))
		  && (uStack56 != 0))
		{
		  lStack96 = lStack96 + -1;
		}
	      if (uStack72 <= uStack56)
		{
		  uStack56 = uStack72;
		}
	      uStack72 = uStack72 - uStack56;
	    }
	  lStack96 = uStack72 + lStack96;
	}
    }
  *puVar6 = uStack112;
  puVar6[1] = CONCAT62 (uStack102, uStack104);
  puVar6[2] = lStack96;
  puVar6[3] = CONCAT44 (uStack84, iStack88);
  return puVar6;
}

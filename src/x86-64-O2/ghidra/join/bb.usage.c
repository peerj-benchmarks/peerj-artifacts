
#include "join.h"

long DAT_00612b7_0_1_;
long DAT_00612b7_1_1_;
long null_ARRAY_00612a2_0_8_;
long null_ARRAY_00612a2_8_8_;
long null_ARRAY_00612b4_16_8_;
long null_ARRAY_00612b8_0_8_;
long null_ARRAY_00612b8_8_8_;
long null_ARRAY_00612b9_0_8_;
long null_ARRAY_00612b9_8_8_;
long null_ARRAY_00612bb_0_8_;
long null_ARRAY_00612bb_8_8_;
long null_ARRAY_00612d0_0_8_;
long null_ARRAY_00612d0_16_8_;
long null_ARRAY_00612d0_24_8_;
long null_ARRAY_00612d0_32_8_;
long null_ARRAY_00612d0_40_8_;
long null_ARRAY_00612d0_48_8_;
long null_ARRAY_00612d0_8_8_;
long null_ARRAY_00612d4_0_4_;
long null_ARRAY_00612d4_16_8_;
long null_ARRAY_00612d4_4_4_;
long null_ARRAY_00612d4_8_4_;
long local_2f_1_1_;
long local_4_4_4_;
long local_5b_1_1_;
long local_9d_0_4_;
long local_a0_0_8_;
long local_a0_4_6_;
long local_a0_8_2_;
long local_a1_4_4_;
long local_a2_4_4_;
long local_a3_0_4_;
long local_a3_4_4_;
long local_a4_0_2_;
long local_a4_0_4_;
long local_a4_0_8_;
long local_a4_1_9_;
long local_a4_4_6_;
long local_a4_8_2_;
long local_a5_0_4_;
long local_a5_4_4_;
long DAT_00000010;
long DAT_0040f80d;
long DAT_0040f810;
long DAT_0040f854;
long DAT_0040f856;
long DAT_0040f8dd;
long DAT_0040f8f2;
long DAT_0040f8f7;
long DAT_0040fad8;
long DAT_0040fb20;
long DAT_0040fb24;
long DAT_0040fb28;
long DAT_0040fb2b;
long DAT_0040fb2d;
long DAT_0040fb31;
long DAT_0040fb35;
long DAT_004100eb;
long DAT_004106fe;
long DAT_00410801;
long DAT_00410807;
long DAT_00410819;
long DAT_0041081a;
long DAT_00410838;
long DAT_004108b6;
long DAT_00612580;
long DAT_00612590;
long DAT_006125a0;
long DAT_006129a0;
long DAT_006129a4;
long DAT_006129b0;
long DAT_006129b8;
long DAT_006129c8;
long DAT_00612a30;
long DAT_00612a34;
long DAT_00612a38;
long DAT_00612a3c;
long DAT_00612a40;
long DAT_00612a48;
long DAT_00612a50;
long DAT_00612a60;
long DAT_00612a68;
long DAT_00612a80;
long DAT_00612a88;
long DAT_00612ae0;
long DAT_00612ae1;
long DAT_00612b30;
long DAT_00612b58;
long DAT_00612b60;
long DAT_00612b68;
long DAT_00612b70;
long DAT_00612b78;
long DAT_00612b7a;
long DAT_00612b7b;
long DAT_00612b7c;
long DAT_00612b7d;
long DAT_00612b7e;
long DAT_00612bc0;
long DAT_00612bc8;
long DAT_00612bd0;
long DAT_00612d38;
long DAT_00612d78;
long DAT_00612d80;
long DAT_00612d88;
long DAT_00612d98;
long fde_00411330;
long null_ARRAY_0040f9c0;
long null_ARRAY_00410b40;
long null_ARRAY_00410d70;
long null_ARRAY_006129e0;
long null_ARRAY_00612a20;
long null_ARRAY_00612aa0;
long null_ARRAY_00612b00;
long null_ARRAY_00612b40;
long null_ARRAY_00612b80;
long null_ARRAY_00612b90;
long null_ARRAY_00612ba0;
long null_ARRAY_00612bb0;
long null_ARRAY_00612c00;
long null_ARRAY_00612d00;
long null_ARRAY_00612d40;
long PTR_DAT_006129c0;
long PTR_null_ARRAY_006129a8;
long PTR_null_ARRAY_00612a18;
long register0x00000020;
void
FUN_00403590 (uint uParm1)
{
  undefined8 uVar1;
  int iVar2;
  long lVar3;
  undefined *puVar4;
  undefined **ppuVar5;
  undefined *puVar6;
  undefined *local_88;
  char *local_80;
  undefined8 local_78;
  char *local_70;
  char *local_68;
  char *local_60;
  char *local_58;
  char *local_50;
  char *local_48;
  char *local_40;
  char *local_38;
  char *local_30;
  undefined8 local_28;
  undefined8 local_20;

  if (uParm1 == 0)
    goto LAB_004035bd;
  func_0x004018e0 (DAT_00612a60, "Try \'%s --help\' for more information.\n",
		   DAT_00612bd0);
  do
    {
      func_0x00401ad0 ((ulong) uParm1);
    LAB_004035bd:
      ;
      func_0x00401740 ("Usage: %s [OPTION]... FILE1 FILE2\n", DAT_00612bd0);
      uVar1 = DAT_00612a40;
      func_0x004018f0
	("For each pair of input lines with identical join fields, write a line to\nstandard output.  The default join field is the first, delimited by blanks.\n",
	 DAT_00612a40);
      func_0x004018f0
	("\nWhen FILE1 or FILE2 (not both) is -, read standard input.\n",
	 uVar1);
      func_0x004018f0
	("\n  -a FILENUM        also print unpairable lines from file FILENUM, where\n                      FILENUM is 1 or 2, corresponding to FILE1 or FILE2\n  -e EMPTY          replace missing input fields with EMPTY\n",
	 uVar1);
      func_0x004018f0
	("  -i, --ignore-case  ignore differences in case when comparing fields\n  -j FIELD          equivalent to \'-1 FIELD -2 FIELD\'\n  -o FORMAT         obey FORMAT while constructing output line\n  -t CHAR           use CHAR as input and output field separator\n",
	 uVar1);
      func_0x004018f0
	("  -v FILENUM        like -a FILENUM, but suppress joined output lines\n  -1 FIELD          join on this FIELD of file 1\n  -2 FIELD          join on this FIELD of file 2\n  --check-order     check that the input is correctly sorted, even\n                      if all input lines are pairable\n  --nocheck-order   do not check that the input is correctly sorted\n  --header          treat the first line in each file as field headers,\n                      print them without trying to pair them\n",
	 uVar1);
      func_0x004018f0
	("  -z, --zero-terminated     line delimiter is NUL, not newline\n",
	 uVar1);
      func_0x004018f0 ("      --help     display this help and exit\n",
		       uVar1);
      func_0x004018f0
	("      --version  output version information and exit\n", uVar1);
      func_0x004018f0
	("\nUnless -t CHAR is given, leading blanks separate fields and are ignored,\nelse fields are separated by CHAR.  Any FIELD is a field number counted\nfrom 1.  FORMAT is one or more comma or blank separated specifications,\neach being \'FILENUM.FIELD\' or \'0\'.  Default FORMAT outputs the join field,\nthe remaining fields from FILE1, the remaining fields from FILE2, all\nseparated by CHAR.  If FORMAT is the keyword \'auto\', then the first\nline of each file determines the number of fields output for each line.\n\nImportant: FILE1 and FILE2 must be sorted on the join fields.\nE.g., use \"sort -k 1b,1\" if \'join\' has no options,\nor use \"join -t \'\'\" if \'sort\' has no options.\nNote, comparisons honor the rules specified by \'LC_COLLATE\'.\nIf the input is not sorted and some lines cannot be joined, a\nwarning message will be given.\n",
	 uVar1);
      local_88 = &DAT_0040f854;
      local_80 = "test invocation";
      puVar6 = &DAT_0040f854;
      local_78 = 0x40f8bc;
      local_70 = "Multi-call invocation";
      local_68 = "sha224sum";
      local_60 = "sha2 utilities";
      local_58 = "sha256sum";
      local_50 = "sha2 utilities";
      local_48 = "sha384sum";
      local_40 = "sha2 utilities";
      local_38 = "sha512sum";
      local_30 = "sha2 utilities";
      local_28 = 0;
      local_20 = 0;
      ppuVar5 = &local_88;
      do
	{
	  iVar2 = func_0x00401a00 (&DAT_0040f856, puVar6);
	  if (iVar2 == 0)
	    break;
	  ppuVar5 = ppuVar5 + 2;
	  puVar6 = *ppuVar5;
	}
      while (puVar6 != (undefined *) 0x0);
      puVar6 = ppuVar5[1];
      if (puVar6 == (undefined *) 0x0)
	{
	  func_0x00401740 ("\n%s online help: <%s>\n", "GNU coreutils",
			   "https://www.gnu.org/software/coreutils/");
	  lVar3 = func_0x00401a60 (5, 0);
	  if (lVar3 != 0)
	    {
	      iVar2 = func_0x00401960 (lVar3, &DAT_0040f8dd, 3);
	      if (iVar2 != 0)
		{
		  puVar6 = &DAT_0040f856;
		  goto LAB_004037a1;
		}
	    }
	  func_0x00401740 ("Full documentation at: <%s%s>\n",
			   "https://www.gnu.org/software/coreutils/",
			   &DAT_0040f856);
	LAB_004037ca:
	  ;
	  puVar6 = &DAT_0040f856;
	  puVar4 = (undefined *) 0x40f875;
	}
      else
	{
	  func_0x00401740 ("\n%s online help: <%s>\n", "GNU coreutils",
			   "https://www.gnu.org/software/coreutils/");
	  lVar3 = func_0x00401a60 (5, 0);
	  if (lVar3 != 0)
	    {
	      iVar2 = func_0x00401960 (lVar3, &DAT_0040f8dd, 3);
	      if (iVar2 != 0)
		{
		LAB_004037a1:
		  ;
		  func_0x00401740
		    ("Report %s translation bugs to <https://translationproject.org/team/>\n",
		     &DAT_0040f856);
		}
	    }
	  func_0x00401740 ("Full documentation at: <%s%s>\n",
			   "https://www.gnu.org/software/coreutils/",
			   &DAT_0040f856);
	  puVar4 = &DAT_0040f810;
	  if (puVar6 == &DAT_0040f856)
	    goto LAB_004037ca;
	}
      func_0x00401740
	("or available locally via: info \'(coreutils) %s%s\'\n", puVar6,
	 puVar4);
    }
  while (true);
}

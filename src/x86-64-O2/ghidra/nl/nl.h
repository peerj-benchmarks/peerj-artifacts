typedef unsigned char   undefined;

typedef unsigned char    byte;
typedef unsigned char    dwfenc;
typedef unsigned int    dword;
typedef unsigned long    qword;
typedef unsigned char    undefined1;
typedef unsigned short    undefined2;
typedef unsigned int    undefined3;
typedef unsigned int    undefined4;
typedef unsigned long    undefined6;
typedef unsigned long    undefined7;
typedef unsigned long    undefined8;
typedef unsigned short    word;
#define unkbyte9   unsigned long long
#define unkbyte10   unsigned long long
#define unkbyte11   unsigned long long
#define unkbyte12   unsigned long long
#define unkbyte13   unsigned long long
#define unkbyte14   unsigned long long
#define unkbyte15   unsigned long long
#define unkbyte16   unsigned long long

#define unkuint9   unsigned long long
#define unkuint10   unsigned long long
#define unkuint11   unsigned long long
#define unkuint12   unsigned long long
#define unkuint13   unsigned long long
#define unkuint14   unsigned long long
#define unkuint15   unsigned long long
#define unkuint16   unsigned long long

#define unkint9   long long
#define unkint10   long long
#define unkint11   long long
#define unkint12   long long
#define unkint13   long long
#define unkint14   long long
#define unkint15   long long
#define unkint16   long long

#define unkfloat1   float
#define unkfloat2   float
#define unkfloat3   float
#define unkfloat5   double
#define unkfloat6   double
#define unkfloat7   double
#define unkfloat9   long double
#define unkfloat11   long double
#define unkfloat12   long double
#define unkfloat13   long double
#define unkfloat14   long double
#define unkfloat15   long double
#define unkfloat16   long double

#define BADSPACEBASE   void
#define code   void

#define ulong long
#define true 1
#define bool int
#define false 0
#define float10 int
#define ushort int
#define undefined7 long
#define undefined2 long
#define uint5 long
#define uint6 long

// Add definition for uint
typedef unsigned int    uint;

#define sbyte char

extern imperfection_wrapper(void);
typedef struct eh_frame_hdr eh_frame_hdr, *Peh_frame_hdr;

struct eh_frame_hdr {
    byte eh_frame_hdr_version; // Exception Handler Frame Header Version
    dwfenc eh_frame_pointer_encoding; // Exception Handler Frame Pointer Encoding
    dwfenc eh_frame_desc_entry_count_encoding; // Encoding of # of Exception Handler FDEs
    dwfenc eh_frame_table_encoding; // Exception Handler Table Encoding
};

typedef struct fde_table_entry fde_table_entry, *Pfde_table_entry;

struct fde_table_entry {
    dword initial_loc; // Initial Location
    dword data_loc; // Data location
};

typedef enum Elf64_DynTag {
    DT_AUDIT=1879047932,
    DT_AUXILIARY=2147483645,
    DT_BIND_NOW=24,
    DT_CHECKSUM=1879047672,
    DT_CONFIG=1879047930,
    DT_DEBUG=21,
    DT_DEPAUDIT=1879047931,
    DT_ENCODING=32,
    DT_FEATURE_1=1879047676,
    DT_FILTER=2147483647,
    DT_FINI=13,
    DT_FINI_ARRAY=26,
    DT_FINI_ARRAYSZ=28,
    DT_FLAGS=30,
    DT_FLAGS_1=1879048187,
    DT_GNU_CONFLICT=1879047928,
    DT_GNU_CONFLICTSZ=1879047670,
    DT_GNU_HASH=1879047925,
    DT_GNU_LIBLIST=1879047929,
    DT_GNU_LIBLISTSZ=1879047671,
    DT_GNU_PRELINKED=1879047669,
    DT_HASH=4,
    DT_INIT=12,
    DT_INIT_ARRAY=25,
    DT_INIT_ARRAYSZ=27,
    DT_JMPREL=23,
    DT_MOVEENT=1879047674,
    DT_MOVESZ=1879047675,
    DT_MOVETAB=1879047934,
    DT_NEEDED=1,
    DT_NULL=0,
    DT_PLTGOT=3,
    DT_PLTPAD=1879047933,
    DT_PLTPADSZ=1879047673,
    DT_PLTREL=20,
    DT_PLTRELSZ=2,
    DT_POSFLAG_1=1879047677,
    DT_PREINIT_ARRAYSZ=33,
    DT_REL=17,
    DT_RELA=7,
    DT_RELACOUNT=1879048185,
    DT_RELAENT=9,
    DT_RELASZ=8,
    DT_RELCOUNT=1879048186,
    DT_RELENT=19,
    DT_RELSZ=18,
    DT_RPATH=15,
    DT_RUNPATH=29,
    DT_SONAME=14,
    DT_STRSZ=10,
    DT_STRTAB=5,
    DT_SYMBOLIC=16,
    DT_SYMENT=11,
    DT_SYMINENT=1879047679,
    DT_SYMINFO=1879047935,
    DT_SYMINSZ=1879047678,
    DT_SYMTAB=6,
    DT_TEXTREL=22,
    DT_TLSDESC_GOT=1879047927,
    DT_TLSDESC_PLT=1879047926,
    DT_VERDEF=1879048188,
    DT_VERDEFNUM=1879048189,
    DT_VERNEED=1879048190,
    DT_VERNEEDNUM=1879048191,
    DT_VERSYM=1879048176
} Elf64_DynTag;

typedef enum Elf_ProgramHeaderType {
    PT_DYNAMIC=2,
    PT_GNU_EH_FRAME=1685382480,
    PT_GNU_RELRO=1685382482,
    PT_GNU_STACK=1685382481,
    PT_INTERP=3,
    PT_LOAD=1,
    PT_NOTE=4,
    PT_NULL=0,
    PT_PHDR=6,
    PT_SHLIB=5,
    PT_TLS=7
} Elf_ProgramHeaderType;

typedef struct Elf64_Dyn Elf64_Dyn, *PElf64_Dyn;

struct Elf64_Dyn {
    enum Elf64_DynTag d_tag;
    qword d_val;
};

typedef struct Elf64_Shdr Elf64_Shdr, *PElf64_Shdr;

typedef enum Elf_SectionHeaderType {
    SHT_CHECKSUM=1879048184,
    SHT_DYNAMIC=6,
    SHT_DYNSYM=11,
    SHT_FINI_ARRAY=15,
    SHT_GNU_ATTRIBUTES=1879048181,
    SHT_GNU_HASH=1879048182,
    SHT_GNU_LIBLIST=1879048183,
    SHT_GNU_verdef=1879048189,
    SHT_GNU_verneed=1879048190,
    SHT_GNU_versym=1879048191,
    SHT_GROUP=17,
    SHT_HASH=5,
    SHT_INIT_ARRAY=14,
    SHT_NOBITS=8,
    SHT_NOTE=7,
    SHT_NULL=0,
    SHT_PREINIT_ARRAY=16,
    SHT_PROGBITS=1,
    SHT_REL=9,
    SHT_RELA=4,
    SHT_SHLIB=10,
    SHT_STRTAB=3,
    SHT_SUNW_COMDAT=1879048187,
    SHT_SUNW_move=1879048186,
    SHT_SUNW_syminfo=1879048188,
    SHT_SYMTAB=2,
    SHT_SYMTAB_SHNDX=18
} Elf_SectionHeaderType;

struct Elf64_Shdr {
    dword sh_name;
    enum Elf_SectionHeaderType sh_type;
    qword sh_flags;
    qword sh_addr;
    qword sh_offset;
    qword sh_size;
    dword sh_link;
    dword sh_info;
    qword sh_addralign;
    qword sh_entsize;
};

typedef struct Elf64_Phdr Elf64_Phdr, *PElf64_Phdr;

struct Elf64_Phdr {
    enum Elf_ProgramHeaderType p_type;
    dword p_flags;
    qword p_offset;
    qword p_vaddr;
    qword p_paddr;
    qword p_filesz;
    qword p_memsz;
    qword p_align;
};

typedef struct Elf64_Rela Elf64_Rela, *PElf64_Rela;

struct Elf64_Rela {
    qword r_offset; // location to apply the relocation action
    qword r_info; // the symbol table index and the type of relocation
    qword r_addend; // a constant addend used to compute the relocatable field value
};

typedef struct Elf64_Sym Elf64_Sym, *PElf64_Sym;

struct Elf64_Sym {
    dword st_name;
    byte st_info;
    byte st_other;
    word st_shndx;
    qword st_value;
    qword st_size;
};

typedef struct Elf64_Ehdr Elf64_Ehdr, *PElf64_Ehdr;

struct Elf64_Ehdr {
    byte e_ident_magic_num;
    char e_ident_magic_str[3];
    byte e_ident_class;
    byte e_ident_data;
    byte e_ident_version;
    byte e_ident_pad[9];
    word e_type;
    word e_machine;
    dword e_version;
    qword e_entry;
    qword e_phoff;
    qword e_shoff;
    dword e_flags;
    word e_ehsize;
    word e_phentsize;
    word e_phnum;
    word e_shentsize;
    word e_shnum;
    word e_shstrndx;
};




undefined8 _DT_INIT(void);
void FUN_004019f0(void);
void thunk_FUN_00622038(void);
void thunk_FUN_006220f0(void);
ulong FUN_00401f90(ulong uParm1,undefined8 *puParm2);
void entry(void);
void FUN_004024e0(undefined8 *puParm1);
void FUN_00402510(void);
void FUN_00402590(void);
void FUN_00402610(void);
long FUN_00402650(char **ppcParm1,undefined8 *puParm2,undefined8 uParm3);
long FUN_00402710(void);
undefined8 FUN_00402770(undefined8 uParm1);
void FUN_00402b70(uint uParm1);
void FUN_00402dc0(void);
void FUN_00402e50(long lParm1,ulong uParm2);
void FUN_00402e80(undefined8 uParm1);
long * FUN_00402e90(long *plParm1,undefined8 uParm2,char cParm3);
void FUN_00402f90(undefined8 uParm1,undefined8 uParm2);
void FUN_00402fa0(long lParm1);
undefined8 * FUN_00403040(undefined8 *puParm1,int iParm2);
undefined * FUN_004030b0(char *pcParm1,int iParm2);
ulong FUN_00403180(undefined *param_1,ulong param_2,long param_3,ulong param_4,uint param_5,ulong param_6,long param_7,char *param_8,char *param_9);
undefined1 * FUN_00404080(uint uParm1,undefined8 uParm2,undefined8 uParm3,uint *puParm4);
void FUN_00404230(undefined8 uParm1,undefined8 uParm2,byte bParm3);
void FUN_004042d0(undefined8 uParm1);
void FUN_004042f0(uint uParm1,undefined8 uParm2,undefined8 uParm3);
void FUN_00404380(undefined8 uParm1);
long FUN_004043a0(undefined8 uParm1,long lParm2,undefined8 uParm3,undefined8 uParm4,long *plParm5,undefined8 uParm6);
void FUN_00404600(void);
void FUN_00404670(void);
void FUN_00404700(long lParm1);
long FUN_00404720(long lParm1,long lParm2);
void FUN_00404760(long lParm1,ulong *puParm2);
void FUN_004047c0(void);
long FUN_004047f0(undefined8 param_1,ulong param_2,long param_3,long param_4,undefined8 param_5,undefined8 param_6,uint param_7);
void FUN_004048e0(undefined8 uParm1,undefined8 uParm2,undefined8 uParm3,undefined8 uParm4,undefined8 uParm5,undefined8 uParm6);
ulong FUN_00404910(byte *pbParm1,byte **ppbParm2,uint uParm3,long *plParm4,long lParm5);
undefined8 FUN_00404fb0(ulong uParm1,ulong uParm2);
void FUN_00405000(undefined8 uParm1);
void FUN_00405040(undefined8 uParm1,uint uParm2,undefined8 uParm3,undefined8 uParm4);
void FUN_004050b0(void);
void FUN_004050f0(undefined4 uParm1,undefined4 uParm2,undefined4 uParm3,undefined4 uParm4,undefined4 uParm5,undefined4 uParm6,undefined4 uParm7,undefined4 uParm8,ulong param_1,ulong param_2,undefined8 param_3,undefined8 param_4,undefined8 param_5,undefined8 param_6);
void FUN_004051d0(undefined8 uParm1);
ulong FUN_00405260(long lParm1);
undefined8 FUN_004052f0(void);
void FUN_00405300(void);
void FUN_00405310(long lParm1,int *piParm2);
ulong FUN_004053f0(int param_1,undefined8 *param_2,char *param_3,long *param_4,int *param_5,int param_6,int *param_7,int param_8,undefined8 param_9);
ulong FUN_00405970(uint param_1,undefined8 *param_2,char *param_3,long param_4,undefined8 param_5,ulong param_6,uint *param_7,int param_8);
void FUN_00405ec0(void);
void FUN_00405f20(void);
void FUN_00405f40(long lParm1);
ulong FUN_00405f60(uint *puParm1,byte *pbParm2,long lParm3);
void FUN_00405fd0(long lParm1,long lParm2);
void FUN_00406010(long *plParm1);
undefined8 FUN_00406060(char *pcParm1,long lParm2,ulong uParm3);
undefined8 FUN_00406190(long lParm1,long lParm2);
ulong FUN_004061b0(long lParm1,uint uParm2,long lParm3,long lParm4,long lParm5);
undefined8 FUN_004063e0(long lParm1,long lParm2,undefined8 uParm3,undefined8 uParm4,long lParm5);
undefined8 FUN_00406450(long *plParm1,code *pcParm2,undefined8 uParm3);
undefined8 FUN_004064c0(long *plParm1,code *pcParm2,undefined8 uParm3);
undefined8 FUN_00406520(long lParm1,ulong uParm2);
undefined8 FUN_004065c0(long *plParm1,undefined8 uParm2);
long * FUN_00406630(long *plParm1,long lParm2);
undefined8 FUN_00406770(long *plParm1,long lParm2);
undefined8 FUN_004067c0(long *plParm1,ulong *puParm2,ulong uParm3);
void FUN_004068a0(long lParm1);
void FUN_00406920(long *plParm1);
undefined8 FUN_00406ad0(long *plParm1);
undefined8 FUN_004070d0(long lParm1,int iParm2);
undefined8 FUN_004071d0(long lParm1,long lParm2);
void FUN_00407260(undefined8 *puParm1);
void FUN_00407280(undefined8 *puParm1);
undefined8 FUN_004072b0(undefined8 uParm1,long lParm2);
long FUN_004072d0(long *plParm1,undefined8 uParm2,undefined8 uParm3);
void FUN_004074c0(long *plParm1,long lParm2,undefined8 uParm3);
undefined8 FUN_00407570(long *plParm1,long lParm2,long lParm3,long lParm4,uint uParm5);
void FUN_004077f0(long lParm1);
void FUN_00407850(long lParm1);
void FUN_00407890(long *plParm1);
void FUN_00407a00(long lParm1);
undefined8 FUN_00407ac0(long *plParm1);
undefined8 FUN_00407b30(long lParm1,long lParm2);
undefined8 FUN_00407da0(long lParm1,long lParm2);
long FUN_00407e00(long lParm1,long *plParm2,long lParm3);
undefined8 FUN_00407e60(long *plParm1,long lParm2,long lParm3,long lParm4,uint uParm5);
void FUN_00407f60(long *plParm1,long *plParm2,long lParm3);
undefined8 FUN_00407fa0(long lParm1,long lParm2);
undefined8 FUN_00408030(undefined8 uParm1,long lParm2);
long FUN_004080a0(long lParm1,long *plParm2,long lParm3);
undefined8 FUN_00408120(long param_1,long *param_2,long *param_3,undefined8 param_4,undefined8 param_5,undefined8 param_6,undefined8 param_7);
undefined8 *FUN_00408230(undefined8 *puParm1,int *piParm2,long *plParm3,long *plParm4,undefined *puParm5);
long ** FUN_00408300(long **pplParm1,long lParm2);
long FUN_004083c0(undefined4 *puParm1,long *plParm2,long lParm3);
ulong FUN_004085d0(undefined8 uParm1,long lParm2);
undefined8 FUN_00408660(long lParm1,long *plParm2,long *plParm3,undefined8 uParm4);
undefined8 FUN_004087b0(long *plParm1,long lParm2);
undefined8 FUN_004089a0(undefined8 *puParm1,long *plParm2,long lParm3,byte bParm4);
ulong FUN_00408c20(long *plParm1,undefined8 *puParm2,long lParm3,ulong uParm4);
undefined8 FUN_00408d40(long *plParm1,long *plParm2,undefined8 *puParm3);
undefined8 FUN_00408dd0(long *plParm1,long lParm2,long lParm3);
ulong * FUN_00408f90(undefined4 *puParm1,long *plParm2,long lParm3,uint uParm4);
ulong * FUN_00409340(undefined4 *puParm1,long *plParm2,long lParm3);
ulong FUN_00409570(undefined8 uParm1,long lParm2,long lParm3,long lParm4);
ulong FUN_00409620(long lParm1,long lParm2,long lParm3,undefined8 uParm4,uint uParm5);
ulong FUN_00409930(long *plParm1,long lParm2);
undefined8 FUN_0040a420(long *plParm1,long *plParm2,long *plParm3,long *plParm4,long *plParm5);
ulong FUN_0040a5e0(long *plParm1,long lParm2,long lParm3,long lParm4);
ulong FUN_0040a850(long lParm1,long lParm2,ulong uParm3);
ulong FUN_0040a920(long lParm1,long *plParm2,long lParm3,undefined8 uParm4,undefined8 uParm5);
undefined8 FUN_0040aa40(byte **ppbParm1,byte *pbParm2,uint uParm3);
long FUN_0040b1e0(long lParm1,long lParm2,long *plParm3,undefined8 uParm4);
undefined8 FUN_0040b2c0(long *plParm1,long lParm2);
undefined8 FUN_0040b360(long *plParm1,long *plParm2,undefined8 *puParm3,long lParm4,undefined8 uParm5,undefined8 *puParm6);
undefined8 FUN_0040b430(long param_1,ulong *param_2,long *param_3,long *param_4,long *param_5,char *param_6,ulong param_7);
long FUN_0040bc50(long lParm1,undefined8 uParm2,undefined8 uParm3,char *pcParm4,byte bParm5,int *piParm6);
void FUN_0040beb0(long **pplParm1,long *plParm2,long *plParm3,long lParm4);
ulong FUN_0040c310(long lParm1,ulong *puParm2,long lParm3,long lParm4,long lParm5);
ulong FUN_0040c5c0(long **pplParm1,long lParm2,long lParm3,long *plParm4,char cParm5);
ulong FUN_0040cdf0(long param_1,long *param_2,long param_3,long param_4,undefined8 param_5,long param_6,uint param_7);
undefined8 FUN_0040d570(long lParm1,long *plParm2,undefined8 *puParm3,undefined8 uParm4,long lParm5);
ulong FUN_0040d720(long lParm1,long *plParm2,long *plParm3);
long FUN_0040df50(int *piParm1,long lParm2,long lParm3);
ulong FUN_0040e120(long lParm1,ulong *puParm2,ulong uParm3,long lParm4);
ulong FUN_0040e990(long lParm1,long *plParm2);
ulong FUN_0040ecb0(long **param_1,long param_2,ulong param_3,long param_4,long param_5,ulong param_6,long *param_7,undefined8 *param_8,uint param_9);
undefined8 FUN_00410490(undefined4 *puParm1,long *plParm2,char *pcParm3,int iParm4,undefined8 uParm5,char cParm6);
undefined8 FUN_004106e0(long *plParm1,long *plParm2,ulong uParm3);
long FUN_00410d80(long lParm1,byte *pbParm2,undefined8 uParm3);
long FUN_00410e40(long lParm1,long *plParm2,undefined8 *puParm3,ulong uParm4,long lParm5,int *piParm6);
long FUN_00412290(undefined8 uParm1,long *plParm2,long lParm3,undefined8 uParm4,long lParm5,int *piParm6);
long FUN_00412410(long lParm1,long *plParm2,long lParm3,ulong uParm4,long lParm5,int *piParm6);
ulong FUN_004125b0(long **pplParm1,undefined8 uParm2,ulong uParm3,long *plParm4);
char * FUN_004132a0(undefined8 uParm1,undefined8 uParm2,long lParm3);
undefined8 FUN_00413300(long *plParm1);
long FUN_004133a0(long param_1,undefined8 param_2,long param_3,long param_4,long param_5,undefined8 param_6,ulong *param_7,char param_8);
void FUN_00413840(void);
ulong FUN_00413860(undefined8 uParm1,undefined8 uParm2,undefined8 uParm3);
ulong FUN_00413930(undefined8 uParm1);
undefined8 FUN_004139a0(void);
ulong FUN_004139b0(ulong uParm1);
char * FUN_00413a50(void);
undefined4 * FUN_00413da0(ulong uParm1,uint *puParm2,ulong uParm3,uint *puParm4,long *plParm5);
ulong FUN_00413f00(void);
uint * FUN_00414170(ulong uParm1,long lParm2,ulong uParm3,uint *puParm4,ulong *puParm5);
char * FUN_00414730(int iParm1,long lParm2,long lParm3,long lParm4,uint uParm5);
void FUN_00414cd0(uint param_1);
ulong FUN_00414e50(void);
void FUN_00415070(undefined8 uParm1,uint uParm2);
undefined8 *FUN_004151e0(undefined8 *puParm1,undefined8 *puParm2,undefined8 *puParm3,undefined8 uParm4);
double FUN_0041a7b0(int *piParm1);
void FUN_0041a7f0(uint *param_1);
undefined8 FUN_0041a870(uint *puParm1,ulong *puParm2);
undefined8 FUN_0041aa90(byte *pbParm1,ulong *puParm2,byte **ppbParm3);
ulong FUN_0041b6e0(void);
ulong FUN_0041b710(void);
void FUN_0041b750(void);
undefined8 _DT_FINI(void);
undefined FUN_00622000();
undefined FUN_00622008();
undefined FUN_00622010();
undefined FUN_00622018();
undefined FUN_00622020();
undefined FUN_00622028();
undefined FUN_00622030();
undefined FUN_00622038();
undefined FUN_00622040();
undefined FUN_00622048();
undefined FUN_00622050();
undefined FUN_00622058();
undefined FUN_00622060();
undefined FUN_00622068();
undefined FUN_00622070();
undefined FUN_00622078();
undefined FUN_00622080();
undefined FUN_00622088();
undefined FUN_00622090();
undefined FUN_00622098();
undefined FUN_006220a0();
undefined FUN_006220a8();
undefined FUN_006220b0();
undefined FUN_006220b8();
undefined FUN_006220c0();
undefined FUN_006220c8();
undefined FUN_006220d0();
undefined FUN_006220d8();
undefined FUN_006220e0();
undefined FUN_006220e8();
undefined FUN_006220f0();
undefined FUN_006220f8();
undefined FUN_00622100();
undefined FUN_00622108();
undefined FUN_00622110();
undefined FUN_00622118();
undefined FUN_00622120();
undefined FUN_00622128();
undefined FUN_00622130();
undefined FUN_00622138();
undefined FUN_00622140();
undefined FUN_00622148();
undefined FUN_00622150();
undefined FUN_00622158();
undefined FUN_00622160();
undefined FUN_00622168();
undefined FUN_00622170();
undefined FUN_00622178();
undefined FUN_00622180();
undefined FUN_00622188();
undefined FUN_00622190();
undefined FUN_00622198();
undefined FUN_006221a0();
undefined FUN_006221a8();
undefined FUN_006221b0();
undefined FUN_006221b8();
undefined FUN_006221c0();
undefined FUN_006221c8();
undefined FUN_006221d0();
undefined FUN_006221d8();
undefined FUN_006221e0();
undefined FUN_006221e8();
undefined FUN_006221f0();
undefined FUN_006221f8();
undefined FUN_00622200();
undefined FUN_00622208();
undefined FUN_00622210();
undefined FUN_00622218();
undefined FUN_00622220();
undefined FUN_00622228();
undefined FUN_00622230();
undefined FUN_00622238();
undefined FUN_00622240();
undefined FUN_00622248();
undefined FUN_00622250();
undefined FUN_00622258();
undefined FUN_00622260();
undefined FUN_00622268();
undefined FUN_00622270();
undefined FUN_00622278();
undefined FUN_00622280();
undefined FUN_00622288();
undefined FUN_00622290();
undefined FUN_00622298();
undefined FUN_006222a0();
undefined FUN_006222a8();
undefined FUN_006222b0();
undefined FUN_006222b8();
undefined FUN_006222c0();



#include "nl.h"

long null_ARRAY_0062158_0_8_;
long null_ARRAY_0062158_8_8_;
long null_ARRAY_006216a_16_8_;
long null_ARRAY_006216a_8_8_;
long null_ARRAY_00621c0_0_8_;
long null_ARRAY_00621c0_16_8_;
long null_ARRAY_00621c0_24_8_;
long null_ARRAY_00621c0_32_8_;
long null_ARRAY_00621c0_40_8_;
long null_ARRAY_00621c0_48_8_;
long null_ARRAY_00621c0_8_8_;
long null_ARRAY_00621c4_0_4_;
long null_ARRAY_00621c4_16_8_;
long null_ARRAY_00621c4_4_4_;
long null_ARRAY_00621c4_8_4_;
long local_2f_1_1_;
long local_4_4_4_;
long local_5_1_1_;
long local_5b_1_1_;
long local_9_0_4_;
long local_9_4_4_;
long local_9d_0_4_;
long local_a0_0_8_;
long local_a_0_4_;
long local_a0_4_6_;
long local_a0_8_2_;
long local_a1_4_4_;
long local_a2_4_4_;
long local_a3_0_4_;
long local_a3_4_4_;
long local_a4_0_2_;
long local_a4_0_4_;
long local_a4_0_8_;
long local_a4_1_9_;
long local_a4_4_6_;
long local_a4_8_2_;
long local_a5_0_4_;
long local_a5_4_4_;
long DAT_00000010;
long DAT_0041b7d5;
long DAT_0041b7d7;
long DAT_0041b85c;
long DAT_0041b85e;
long DAT_0041b89b;
long DAT_0041b89e;
long DAT_0041b8a1;
long DAT_0041b996;
long DAT_0041c4b8;
long DAT_0041c4bc;
long DAT_0041c4c0;
long DAT_0041c4c3;
long DAT_0041c4c5;
long DAT_0041c4c9;
long DAT_0041c4cd;
long DAT_0041ca6b;
long DAT_0041d008;
long DAT_0041d111;
long DAT_0041d117;
long DAT_0041d129;
long DAT_0041d12a;
long DAT_0041d148;
long DAT_0041d190;
long DAT_0041d197;
long DAT_0041d1d5;
long DAT_0041d1ee;
long DAT_0041de3e;
long DAT_0041debe;
long DAT_00621050;
long DAT_00621060;
long DAT_00621070;
long DAT_006214c8;
long DAT_006214d0;
long DAT_006214d8;
long DAT_006214e0;
long DAT_006214e8;
long DAT_00621520;
long DAT_00621590;
long DAT_00621594;
long DAT_00621598;
long DAT_0062159c;
long DAT_006215c0;
long DAT_006215c8;
long DAT_006215d0;
long DAT_006215e0;
long DAT_006215e8;
long DAT_00621600;
long DAT_00621608;
long DAT_00621680;
long DAT_00621688;
long DAT_00621690;
long DAT_00621698;
long DAT_006216b8;
long DAT_006216c0;
long DAT_006216c8;
long DAT_006216d0;
long DAT_006216d8;
long DAT_006216e0;
long DAT_006216e8;
long DAT_00621ac0;
long DAT_00621ac8;
long DAT_00621ad0;
long DAT_00621ad8;
long DAT_00621c78;
long DAT_00621c80;
long DAT_00621c88;
long DAT_00621c98;
long DAT_00621ca0;
long _DYNAMIC;
long fde_0041eb48;
long null_ARRAY_0041c280;
long null_ARRAY_0041dbc0;
long null_ARRAY_0041dc00;
long null_ARRAY_0041e160;
long null_ARRAY_0041e390;
long null_ARRAY_00621540;
long null_ARRAY_00621580;
long null_ARRAY_00621620;
long null_ARRAY_006216a0;
long null_ARRAY_00621700;
long null_ARRAY_00621800;
long null_ARRAY_00621900;
long null_ARRAY_00621a00;
long null_ARRAY_00621a40;
long null_ARRAY_00621a80;
long null_ARRAY_00621b00;
long null_ARRAY_00621c00;
long null_ARRAY_00621c40;
long PTR_DAT_006214f0;
long PTR_DAT_006214f8;
long PTR_DAT_00621500;
long PTR_DAT_00621508;
long PTR_DAT_00621518;
long PTR_null_ARRAY_00621578;
long PTR_s___ld_s_006214c0;
long PTR_s_t_00621510;
long register0x00000020;
long stack0x00000008;
long stack0xffffffffffffffd8;
void
FUN_00402b70 (uint uParm1)
{
  undefined8 uVar1;
  int iVar2;
  long lVar3;
  undefined *puVar4;
  undefined **ppuVar5;
  undefined *puVar6;
  undefined *local_88;
  char *local_80;
  undefined8 local_78;
  char *local_70;
  char *local_68;
  char *local_60;
  char *local_58;
  char *local_50;
  char *local_48;
  char *local_40;
  char *local_38;
  char *local_30;
  undefined8 local_28;
  undefined8 local_20;

  if (uParm1 == 0)
    goto LAB_00402b9d;
  func_0x00401c20 (DAT_006215e0, "Try \'%s --help\' for more information.\n",
		   DAT_00621ad8);
  do
    {
      func_0x00401e40 ((ulong) uParm1);
    LAB_00402b9d:
      ;
      func_0x00401a20 ("Usage: %s [OPTION]... [FILE]...\n", DAT_00621ad8);
      uVar1 = DAT_006215c0;
      func_0x00401c30
	("Write each FILE to standard output, with line numbers added.\n",
	 DAT_006215c0);
      func_0x00401c30
	("\nWith no FILE, or when FILE is -, read standard input.\n", uVar1);
      func_0x00401c30
	("\nMandatory arguments to long options are mandatory for short options too.\n",
	 uVar1);
      func_0x00401c30
	("  -b, --body-numbering=STYLE      use STYLE for numbering body lines\n  -d, --section-delimiter=CC      use CC for logical page delimiters\n  -f, --footer-numbering=STYLE    use STYLE for numbering footer lines\n",
	 uVar1);
      func_0x00401c30
	("  -h, --header-numbering=STYLE    use STYLE for numbering header lines\n  -i, --line-increment=NUMBER     line number increment at each line\n  -l, --join-blank-lines=NUMBER   group of NUMBER empty lines counted as one\n  -n, --number-format=FORMAT      insert line numbers according to FORMAT\n  -p, --no-renumber               do not reset line numbers for each section\n  -s, --number-separator=STRING   add STRING after (possible) line number\n",
	 uVar1);
      func_0x00401c30
	("  -v, --starting-line-number=NUMBER  first line number for each section\n  -w, --number-width=NUMBER       use NUMBER columns for line numbers\n",
	 uVar1);
      func_0x00401c30 ("      --help     display this help and exit\n",
		       uVar1);
      func_0x00401c30
	("      --version  output version information and exit\n", uVar1);
      func_0x00401c30
	("\nBy default, selects -v1 -i1 -l1 -sTAB -w6 -nrn -hn -bt -fn.\nCC are two delimiter characters used to construct logical page delimiters,\na missing second character implies :.  Type \\\\ for \\.  STYLE is one of:\n",
	 uVar1);
      func_0x00401c30
	("\n  a         number all lines\n  t         number only nonempty lines\n  n         number no lines\n  pBRE      number only lines that contain a match for the basic regular\n            expression, BRE\n\nFORMAT is one of:\n\n  ln   left justified, no leading zeros\n  rn   right justified, no leading zeros\n  rz   right justified, leading zeros\n\n",
	 uVar1);
      local_88 = &DAT_0041b7d5;
      local_80 = "test invocation";
      puVar6 = &DAT_0041b7d5;
      local_78 = 0x41b83b;
      local_70 = "Multi-call invocation";
      local_68 = "sha224sum";
      local_60 = "sha2 utilities";
      local_58 = "sha256sum";
      local_50 = "sha2 utilities";
      local_48 = "sha384sum";
      local_40 = "sha2 utilities";
      local_38 = "sha512sum";
      local_30 = "sha2 utilities";
      local_28 = 0;
      local_20 = 0;
      ppuVar5 = &local_88;
      do
	{
	  iVar2 = func_0x00401d70 (&DAT_0041b7d7, puVar6);
	  if (iVar2 == 0)
	    break;
	  ppuVar5 = ppuVar5 + 2;
	  puVar6 = *ppuVar5;
	}
      while (puVar6 != (undefined *) 0x0);
      puVar6 = ppuVar5[1];
      if (puVar6 == (undefined *) 0x0)
	{
	  func_0x00401a20 ("\n%s online help: <%s>\n", "GNU coreutils",
			   "https://www.gnu.org/software/coreutils/");
	  lVar3 = func_0x00401dd0 (5, 0);
	  if (lVar3 != 0)
	    {
	      iVar2 = func_0x00401ca0 (lVar3, &DAT_0041b85c, 3);
	      if (iVar2 != 0)
		{
		  puVar6 = &DAT_0041b7d7;
		  goto LAB_00402d89;
		}
	    }
	  func_0x00401a20 ("Full documentation at: <%s%s>\n",
			   "https://www.gnu.org/software/coreutils/",
			   &DAT_0041b7d7);
	LAB_00402db2:
	  ;
	  puVar6 = &DAT_0041b7d7;
	  puVar4 = (undefined *) 0x41b7f4;
	}
      else
	{
	  func_0x00401a20 ("\n%s online help: <%s>\n", "GNU coreutils",
			   "https://www.gnu.org/software/coreutils/");
	  lVar3 = func_0x00401dd0 (5, 0);
	  if (lVar3 != 0)
	    {
	      iVar2 = func_0x00401ca0 (lVar3, &DAT_0041b85c, 3);
	      if (iVar2 != 0)
		{
		LAB_00402d89:
		  ;
		  func_0x00401a20
		    ("Report %s translation bugs to <https://translationproject.org/team/>\n",
		     &DAT_0041b7d7);
		}
	    }
	  func_0x00401a20 ("Full documentation at: <%s%s>\n",
			   "https://www.gnu.org/software/coreutils/",
			   &DAT_0041b7d7);
	  puVar4 = &DAT_0041b996;
	  if (puVar6 == &DAT_0041b7d7)
	    goto LAB_00402db2;
	}
      func_0x00401a20
	("or available locally via: info \'(coreutils) %s%s\'\n", puVar6,
	 puVar4);
    }
  while (true);
}

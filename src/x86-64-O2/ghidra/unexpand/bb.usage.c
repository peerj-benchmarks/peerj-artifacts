
#include "unexpand.h"

long null_ARRAY_0061046_0_8_;
long null_ARRAY_0061046_8_8_;
long null_ARRAY_0061068_0_8_;
long null_ARRAY_0061068_16_8_;
long null_ARRAY_0061068_24_8_;
long null_ARRAY_0061068_32_8_;
long null_ARRAY_0061068_40_8_;
long null_ARRAY_0061068_48_8_;
long null_ARRAY_0061068_8_8_;
long null_ARRAY_006106c_0_4_;
long null_ARRAY_006106c_16_8_;
long null_ARRAY_006106c_4_4_;
long null_ARRAY_006106c_8_4_;
long local_2f_1_1_;
long local_4_4_4_;
long local_5b_1_1_;
long local_9d_0_4_;
long local_a0_0_8_;
long local_a0_4_6_;
long local_a0_8_2_;
long local_a1_4_4_;
long local_a2_4_4_;
long local_a3_0_4_;
long local_a3_4_4_;
long local_a4_0_2_;
long local_a4_0_4_;
long local_a4_0_8_;
long local_a4_1_9_;
long local_a4_4_6_;
long local_a4_8_2_;
long local_a5_0_4_;
long local_a5_4_4_;
long DAT_00000010;
long DAT_0040d400;
long DAT_0040d48d;
long DAT_0040dc98;
long DAT_0040dc9c;
long DAT_0040dca0;
long DAT_0040dca3;
long DAT_0040dca5;
long DAT_0040dca9;
long DAT_0040dcad;
long DAT_0040e22b;
long DAT_0040e5d5;
long DAT_0040e6d9;
long DAT_0040e6df;
long DAT_0040e6f1;
long DAT_0040e6f2;
long DAT_0040e710;
long DAT_0040e714;
long DAT_0040e796;
long DAT_00610000;
long DAT_00610010;
long DAT_00610020;
long DAT_00610418;
long DAT_00610470;
long DAT_00610474;
long DAT_00610478;
long DAT_0061047c;
long DAT_00610480;
long DAT_00610488;
long DAT_00610490;
long DAT_006104a0;
long DAT_006104a8;
long DAT_006104c0;
long DAT_006104c8;
long DAT_00610510;
long DAT_00610514;
long DAT_00610518;
long DAT_00610520;
long DAT_00610528;
long DAT_00610530;
long DAT_00610538;
long DAT_00610540;
long DAT_00610548;
long DAT_00610550;
long DAT_00610558;
long DAT_00610560;
long DAT_00610568;
long DAT_00610570;
long DAT_006106f8;
long DAT_00610700;
long DAT_00610708;
long DAT_00610710;
long DAT_00610720;
long fde_0040f160;
long null_ARRAY_0040d840;
long null_ARRAY_0040ea20;
long null_ARRAY_0040ec50;
long null_ARRAY_00610400;
long null_ARRAY_00610420;
long null_ARRAY_00610460;
long null_ARRAY_006104e0;
long null_ARRAY_00610580;
long null_ARRAY_00610680;
long null_ARRAY_006106c0;
long PTR_DAT_00610410;
long PTR_null_ARRAY_00610458;
void
FUN_004020b0 (uint uParm1)
{
  int iVar1;
  long lVar2;
  undefined8 uVar3;
  undefined **ppuVar4;
  char *pcVar5;
  undefined *puVar6;
  undefined *local_88;
  char *local_80;
  undefined8 local_78;
  char *local_70;
  char *local_68;
  char *local_60;
  char *local_58;
  char *local_50;
  char *local_48;
  char *local_40;
  char *local_38;
  char *local_30;
  undefined8 local_28;
  undefined8 local_20;

  if (uParm1 == 0)
    goto LAB_004020dd;
  func_0x00401830 (DAT_006104a0, "Try \'%s --help\' for more information.\n",
		   DAT_00610570);
  do
    {
      func_0x00401a00 ((ulong) uParm1);
    LAB_004020dd:
      ;
      func_0x00401690 ("Usage: %s [OPTION]... [FILE]...\n", DAT_00610570);
      uVar3 = DAT_00610480;
      func_0x00401840
	("Convert blanks in each FILE to tabs, writing to standard output.\n",
	 DAT_00610480);
      func_0x00401840
	("\nWith no FILE, or when FILE is -, read standard input.\n", uVar3);
      func_0x00401840
	("\nMandatory arguments to long options are mandatory for short options too.\n",
	 uVar3);
      func_0x00401840
	("  -a, --all        convert all blanks, instead of just initial blanks\n      --first-only  convert only leading sequences of blanks (overrides -a)\n  -t, --tabs=N     have tabs N characters apart instead of 8 (enables -a)\n",
	 uVar3);
      FUN_00402b90 ();
      func_0x00401840 ("      --help     display this help and exit\n",
		       uVar3);
      func_0x00401840
	("      --version  output version information and exit\n", uVar3);
      local_88 = &DAT_0040d400;
      local_80 = "test invocation";
      puVar6 = &DAT_0040d400;
      local_78 = 0x40d46c;
      local_70 = "Multi-call invocation";
      local_68 = "sha224sum";
      local_60 = "sha2 utilities";
      local_58 = "sha256sum";
      local_50 = "sha2 utilities";
      local_48 = "sha384sum";
      local_40 = "sha2 utilities";
      local_38 = "sha512sum";
      local_30 = "sha2 utilities";
      local_28 = 0;
      local_20 = 0;
      ppuVar4 = &local_88;
      do
	{
	  iVar1 = func_0x00401940 ("unexpand", puVar6);
	  if (iVar1 == 0)
	    break;
	  ppuVar4 = ppuVar4 + 2;
	  puVar6 = *ppuVar4;
	}
      while (puVar6 != (undefined *) 0x0);
      pcVar5 = ppuVar4[1];
      if (pcVar5 == (char *) 0x0)
	{
	  func_0x00401690 ("\n%s online help: <%s>\n", "GNU coreutils",
			   "https://www.gnu.org/software/coreutils/");
	  lVar2 = func_0x004019a0 (5, 0);
	  if (lVar2 != 0)
	    {
	      iVar1 = func_0x004018a0 (lVar2, &DAT_0040d48d, 3);
	      if (iVar1 != 0)
		{
		  pcVar5 = "unexpand";
		  goto LAB_004022a1;
		}
	    }
	  func_0x00401690 ("Full documentation at: <%s%s>\n",
			   "https://www.gnu.org/software/coreutils/",
			   "unexpand");
	LAB_004022ca:
	  ;
	  pcVar5 = "unexpand";
	  uVar3 = 0x40d425;
	}
      else
	{
	  func_0x00401690 ("\n%s online help: <%s>\n", "GNU coreutils",
			   "https://www.gnu.org/software/coreutils/");
	  lVar2 = func_0x004019a0 (5, 0);
	  if (lVar2 != 0)
	    {
	      iVar1 = func_0x004018a0 (lVar2, &DAT_0040d48d, 3);
	      if (iVar1 != 0)
		{
		LAB_004022a1:
		  ;
		  func_0x00401690
		    ("Report %s translation bugs to <https://translationproject.org/team/>\n",
		     "unexpand");
		}
	    }
	  func_0x00401690 ("Full documentation at: <%s%s>\n",
			   "https://www.gnu.org/software/coreutils/",
			   "unexpand");
	  uVar3 = 0x40e70f;
	  if (pcVar5 == "unexpand")
	    goto LAB_004022ca;
	}
      func_0x00401690
	("or available locally via: info \'(coreutils) %s%s\'\n", pcVar5,
	 uVar3);
    }
  while (true);
}

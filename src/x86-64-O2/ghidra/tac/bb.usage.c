
#include "tac.h"

long null_ARRAY_0062252_0_8_;
long null_ARRAY_0062252_8_8_;
long null_ARRAY_0062466_16_8_;
long null_ARRAY_0062466_8_8_;
long null_ARRAY_0062478_0_8_;
long null_ARRAY_0062478_32_8_;
long null_ARRAY_0062478_40_8_;
long null_ARRAY_0062478_8_8_;
long null_ARRAY_0062494_0_8_;
long null_ARRAY_0062494_16_8_;
long null_ARRAY_0062494_24_8_;
long null_ARRAY_0062494_32_8_;
long null_ARRAY_0062494_40_8_;
long null_ARRAY_0062494_48_8_;
long null_ARRAY_0062494_8_8_;
long null_ARRAY_0062498_0_4_;
long null_ARRAY_0062498_16_8_;
long null_ARRAY_0062498_4_4_;
long null_ARRAY_0062498_8_4_;
long local_2f_1_1_;
long local_4_4_4_;
long local_5_1_1_;
long local_5b_1_1_;
long local_9_0_4_;
long local_9_4_4_;
long local_9d_0_4_;
long local_a0_0_8_;
long local_a_0_4_;
long local_a0_4_6_;
long local_a0_8_2_;
long local_a1_4_4_;
long local_a2_4_4_;
long local_a3_0_4_;
long local_a3_4_4_;
long local_a4_0_2_;
long local_a4_0_4_;
long local_a4_0_8_;
long local_a4_1_9_;
long local_a4_4_6_;
long local_a4_8_2_;
long local_a5_0_4_;
long local_a5_4_4_;
long DAT_00000010;
long DAT_0041c8f0;
long DAT_0041c8f2;
long DAT_0041c978;
long DAT_0041c97a;
long DAT_0041c98b;
long DAT_0041c9ac;
long DAT_0041c9fa;
long DAT_0041cf50;
long DAT_0041cf54;
long DAT_0041cf58;
long DAT_0041cf5b;
long DAT_0041cf5d;
long DAT_0041cf61;
long DAT_0041cf65;
long DAT_0041d4eb;
long DAT_0041d895;
long DAT_0041d999;
long DAT_0041d99f;
long DAT_0041d9b1;
long DAT_0041d9b2;
long DAT_0041d9d0;
long DAT_0041da18;
long DAT_0041da1f;
long DAT_0041da5d;
long DAT_0041da76;
long DAT_0041e6be;
long DAT_0041e7e5;
long DAT_0041ebf0;
long DAT_00622000;
long DAT_00622010;
long DAT_00622020;
long DAT_006224c8;
long DAT_00622530;
long DAT_00622534;
long DAT_00622538;
long DAT_0062253c;
long DAT_00622580;
long DAT_00622590;
long DAT_006225a0;
long DAT_006225a8;
long DAT_006225c0;
long DAT_006225c8;
long DAT_00624640;
long DAT_00624648;
long DAT_00624650;
long DAT_006247c0;
long DAT_006247c8;
long DAT_006247d0;
long DAT_006247d8;
long DAT_006247e0;
long DAT_006247e8;
long DAT_006247e9;
long DAT_006247f0;
long DAT_006247f8;
long DAT_00624800;
long DAT_00624808;
long DAT_006249b8;
long DAT_006249c0;
long DAT_00624a18;
long DAT_00624a20;
long DAT_00624a30;
long DAT_00624a38;
long _DYNAMIC;
long fde_0041f550;
long null_ARRAY_0041ce00;
long null_ARRAY_0041ce40;
long null_ARRAY_0041e440;
long null_ARRAY_0041e480;
long null_ARRAY_0041ea80;
long null_ARRAY_0041ecd0;
long null_ARRAY_006224e0;
long null_ARRAY_00622520;
long null_ARRAY_006225e0;
long null_ARRAY_00622640;
long null_ARRAY_00624660;
long null_ARRAY_00624680;
long null_ARRAY_00624780;
long null_ARRAY_00624840;
long null_ARRAY_00624940;
long null_ARRAY_00624980;
long PTR_DAT_006224c0;
long PTR_null_ARRAY_00622518;
long PTR_null_ARRAY_00622540;
long register0x00000020;
long stack0x00000008;
void
FUN_004030a0 (uint uParm1)
{
  int iVar1;
  long lVar2;
  undefined8 uVar3;
  undefined **ppuVar4;
  undefined *puVar5;
  undefined *local_88;
  char *local_80;
  undefined8 local_78;
  char *local_70;
  char *local_68;
  char *local_60;
  char *local_58;
  char *local_50;
  char *local_48;
  char *local_40;
  char *local_38;
  char *local_30;
  undefined8 local_28;
  undefined8 local_20;

  if (uParm1 == 0)
    goto LAB_004030cd;
  func_0x00401f30 (DAT_006225a0, "Try \'%s --help\' for more information.\n",
		   DAT_00624808);
  do
    {
      func_0x00402190 ((ulong) uParm1);
    LAB_004030cd:
      ;
      func_0x00401ce0 ("Usage: %s [OPTION]... [FILE]...\n", DAT_00624808);
      uVar3 = DAT_00622580;
      func_0x00401f40
	("Write each FILE to standard output, last line first.\n",
	 DAT_00622580);
      func_0x00401f40
	("\nWith no FILE, or when FILE is -, read standard input.\n", uVar3);
      func_0x00401f40
	("\nMandatory arguments to long options are mandatory for short options too.\n",
	 uVar3);
      func_0x00401f40
	("  -b, --before             attach the separator before instead of after\n  -r, --regex              interpret the separator as a regular expression\n  -s, --separator=STRING   use STRING as the separator instead of newline\n",
	 uVar3);
      func_0x00401f40 ("      --help     display this help and exit\n",
		       uVar3);
      func_0x00401f40
	("      --version  output version information and exit\n", uVar3);
      local_88 = &DAT_0041c8f0;
      local_80 = "test invocation";
      puVar5 = &DAT_0041c8f0;
      local_78 = 0x41c957;
      local_70 = "Multi-call invocation";
      local_68 = "sha224sum";
      local_60 = "sha2 utilities";
      local_58 = "sha256sum";
      local_50 = "sha2 utilities";
      local_48 = "sha384sum";
      local_40 = "sha2 utilities";
      local_38 = "sha512sum";
      local_30 = "sha2 utilities";
      local_28 = 0;
      local_20 = 0;
      ppuVar4 = &local_88;
      do
	{
	  iVar1 = func_0x004020d0 (&DAT_0041c8f2, puVar5);
	  if (iVar1 == 0)
	    break;
	  ppuVar4 = ppuVar4 + 2;
	  puVar5 = *ppuVar4;
	}
      while (puVar5 != (undefined *) 0x0);
      puVar5 = ppuVar4[1];
      if (puVar5 == (undefined *) 0x0)
	{
	  func_0x00401ce0 ("\n%s online help: <%s>\n", "GNU coreutils",
			   "https://www.gnu.org/software/coreutils/");
	  lVar2 = func_0x00402130 (5, 0);
	  if (lVar2 != 0)
	    {
	      iVar1 = func_0x00401fc0 (lVar2, &DAT_0041c978, 3);
	      if (iVar1 != 0)
		{
		  puVar5 = &DAT_0041c8f2;
		  goto LAB_00403289;
		}
	    }
	  func_0x00401ce0 ("Full documentation at: <%s%s>\n",
			   "https://www.gnu.org/software/coreutils/",
			   &DAT_0041c8f2);
	LAB_004032b2:
	  ;
	  puVar5 = &DAT_0041c8f2;
	  uVar3 = 0x41c910;
	}
      else
	{
	  func_0x00401ce0 ("\n%s online help: <%s>\n", "GNU coreutils",
			   "https://www.gnu.org/software/coreutils/");
	  lVar2 = func_0x00402130 (5, 0);
	  if (lVar2 != 0)
	    {
	      iVar1 = func_0x00401fc0 (lVar2, &DAT_0041c978, 3);
	      if (iVar1 != 0)
		{
		LAB_00403289:
		  ;
		  func_0x00401ce0
		    ("Report %s translation bugs to <https://translationproject.org/team/>\n",
		     &DAT_0041c8f2);
		}
	    }
	  func_0x00401ce0 ("Full documentation at: <%s%s>\n",
			   "https://www.gnu.org/software/coreutils/",
			   &DAT_0041c8f2);
	  uVar3 = 0x41d9cf;
	  if (puVar5 == &DAT_0041c8f2)
	    goto LAB_004032b2;
	}
      func_0x00401ce0
	("or available locally via: info \'(coreutils) %s%s\'\n", puVar5,
	 uVar3);
    }
  while (true);
}

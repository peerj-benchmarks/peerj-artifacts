
#include "yes.h"

long null_ARRAY_0060f42_0_8_;
long null_ARRAY_0060f42_8_8_;
long null_ARRAY_0060f60_0_8_;
long null_ARRAY_0060f60_16_8_;
long null_ARRAY_0060f60_24_8_;
long null_ARRAY_0060f60_32_8_;
long null_ARRAY_0060f60_40_8_;
long null_ARRAY_0060f60_48_8_;
long null_ARRAY_0060f60_8_8_;
long null_ARRAY_0060f64_0_4_;
long null_ARRAY_0060f64_16_8_;
long null_ARRAY_0060f64_4_4_;
long null_ARRAY_0060f64_8_4_;
long local_2f_1_1_;
long local_5b_1_1_;
long local_9d_0_4_;
long local_a0_0_8_;
long local_a0_4_6_;
long local_a0_8_2_;
long local_a1_4_4_;
long local_a2_4_4_;
long local_a3_0_4_;
long local_a3_4_4_;
long local_a4_0_2_;
long local_a4_0_4_;
long local_a4_0_8_;
long local_a4_1_9_;
long local_a4_4_6_;
long local_a4_8_2_;
long local_a5_0_4_;
long local_a5_4_4_;
long DAT_00000010;
long DAT_0040c640;
long DAT_0040c642;
long DAT_0040c6c8;
long DAT_0040c6dc;
long DAT_0040c9e0;
long DAT_0040c9e4;
long DAT_0040c9e8;
long DAT_0040c9eb;
long DAT_0040c9ed;
long DAT_0040c9f1;
long DAT_0040c9f5;
long DAT_0040cfab;
long DAT_0040d355;
long DAT_0040d459;
long DAT_0040d45f;
long DAT_0040d471;
long DAT_0040d472;
long DAT_0040d490;
long DAT_0040d494;
long DAT_0040d516;
long DAT_0060f000;
long DAT_0060f010;
long DAT_0060f020;
long DAT_0060f3c8;
long DAT_0060f430;
long DAT_0060f434;
long DAT_0060f438;
long DAT_0060f43c;
long DAT_0060f440;
long DAT_0060f450;
long DAT_0060f460;
long DAT_0060f468;
long DAT_0060f480;
long DAT_0060f488;
long DAT_0060f4d0;
long DAT_0060f4d8;
long DAT_0060f4e0;
long DAT_0060f678;
long DAT_0060f680;
long DAT_0060f688;
long DAT_0060f698;
long _DYNAMIC;
long fde_0040de88;
long null_ARRAY_0040c8c0;
long null_ARRAY_0040c940;
long null_ARRAY_0040d7a0;
long null_ARRAY_0040d9d0;
long null_ARRAY_0060f420;
long null_ARRAY_0060f4a0;
long null_ARRAY_0060f500;
long null_ARRAY_0060f600;
long null_ARRAY_0060f640;
long PTR_DAT_0060f3c0;
long PTR_null_ARRAY_0060f418;
long register0x00000020;
void
FUN_00401c00 (uint uParm1)
{
  int iVar1;
  long lVar2;
  undefined8 uVar3;
  undefined **ppuVar4;
  undefined *puVar5;
  undefined *local_88;
  char *local_80;
  undefined8 local_78;
  char *local_70;
  char *local_68;
  char *local_60;
  char *local_58;
  char *local_50;
  char *local_48;
  char *local_40;
  char *local_38;
  char *local_30;
  undefined8 local_28;
  undefined8 local_20;

  if (uParm1 == 0)
    goto LAB_00401c2d;
  func_0x004015d0 (DAT_0060f460, "Try \'%s --help\' for more information.\n",
		   DAT_0060f4e0);
  do
    {
      func_0x00401750 ((ulong) uParm1);
    LAB_00401c2d:
      ;
      func_0x00401450 ("Usage: %s [STRING]...\n  or:  %s OPTION\n",
		       DAT_0060f4e0, DAT_0060f4e0);
      uVar3 = DAT_0060f440;
      func_0x004015e0
	("Repeatedly output a line with all specified STRING(s), or \'y\'.\n\n",
	 DAT_0060f440);
      func_0x004015e0 ("      --help     display this help and exit\n",
		       uVar3);
      func_0x004015e0
	("      --version  output version information and exit\n", uVar3);
      local_88 = &DAT_0040c640;
      local_80 = "test invocation";
      puVar5 = &DAT_0040c640;
      local_78 = 0x40c6a7;
      local_70 = "Multi-call invocation";
      local_68 = "sha224sum";
      local_60 = "sha2 utilities";
      local_58 = "sha256sum";
      local_50 = "sha2 utilities";
      local_48 = "sha384sum";
      local_40 = "sha2 utilities";
      local_38 = "sha512sum";
      local_30 = "sha2 utilities";
      local_28 = 0;
      local_20 = 0;
      ppuVar4 = &local_88;
      do
	{
	  iVar1 = func_0x004016b0 (&DAT_0040c642, puVar5);
	  if (iVar1 == 0)
	    break;
	  ppuVar4 = ppuVar4 + 2;
	  puVar5 = *ppuVar4;
	}
      while (puVar5 != (undefined *) 0x0);
      puVar5 = ppuVar4[1];
      if (puVar5 == (undefined *) 0x0)
	{
	  func_0x00401450 ("\n%s online help: <%s>\n", "GNU coreutils",
			   "https://www.gnu.org/software/coreutils/");
	  lVar2 = func_0x00401710 (5, 0);
	  if (lVar2 != 0)
	    {
	      iVar1 = func_0x00401630 (lVar2, &DAT_0040c6c8, 3);
	      if (iVar1 != 0)
		{
		  puVar5 = &DAT_0040c642;
		  goto LAB_00401dc9;
		}
	    }
	  func_0x00401450 ("Full documentation at: <%s%s>\n",
			   "https://www.gnu.org/software/coreutils/",
			   &DAT_0040c642);
	LAB_00401df2:
	  ;
	  puVar5 = &DAT_0040c642;
	  uVar3 = 0x40c660;
	}
      else
	{
	  func_0x00401450 ("\n%s online help: <%s>\n", "GNU coreutils",
			   "https://www.gnu.org/software/coreutils/");
	  lVar2 = func_0x00401710 (5, 0);
	  if (lVar2 != 0)
	    {
	      iVar1 = func_0x00401630 (lVar2, &DAT_0040c6c8, 3);
	      if (iVar1 != 0)
		{
		LAB_00401dc9:
		  ;
		  func_0x00401450
		    ("Report %s translation bugs to <https://translationproject.org/team/>\n",
		     &DAT_0040c642);
		}
	    }
	  func_0x00401450 ("Full documentation at: <%s%s>\n",
			   "https://www.gnu.org/software/coreutils/",
			   &DAT_0040c642);
	  uVar3 = 0x40d48f;
	  if (puVar5 == &DAT_0040c642)
	    goto LAB_00401df2;
	}
      func_0x00401450
	("or available locally via: info \'(coreutils) %s%s\'\n", puVar5,
	 uVar3);
    }
  while (true);
}


#include "chgrp.h"

long null_ARRAY_0061770_0_8_;
long null_ARRAY_0061770_8_8_;
long null_ARRAY_0061790_0_8_;
long null_ARRAY_0061790_16_8_;
long null_ARRAY_0061790_24_8_;
long null_ARRAY_0061790_32_8_;
long null_ARRAY_0061790_40_8_;
long null_ARRAY_0061790_48_8_;
long null_ARRAY_0061790_8_8_;
long null_ARRAY_0061794_0_4_;
long null_ARRAY_0061794_16_8_;
long null_ARRAY_0061794_4_4_;
long null_ARRAY_0061794_8_4_;
long local_2f_1_1_;
long local_4_4_4_;
long local_5b_1_1_;
long local_9d_0_4_;
long local_a0_0_8_;
long local_a0_4_6_;
long local_a0_8_2_;
long local_a1_4_4_;
long local_a2_4_4_;
long local_a3_0_4_;
long local_a3_4_4_;
long local_a4_0_2_;
long local_a4_0_4_;
long local_a4_0_8_;
long local_a4_1_9_;
long local_a4_4_6_;
long local_a4_8_2_;
long local_a5_0_4_;
long local_a5_4_4_;
long DAT_00000010;
long DAT_00412a40;
long DAT_00412aca;
long DAT_004139e0;
long DAT_004139e4;
long DAT_004139e8;
long DAT_004139eb;
long DAT_004139ed;
long DAT_004139f1;
long DAT_004139f5;
long DAT_00413fab;
long DAT_0041456e;
long DAT_0041457d;
long DAT_0041457f;
long DAT_00414580;
long DAT_004146d9;
long DAT_004146da;
long DAT_004146f8;
long DAT_00414769;
long DAT_004148b2;
long DAT_004148bc;
long DAT_004148fa;
long DAT_006171f8;
long DAT_00617208;
long DAT_00617218;
long DAT_006176a8;
long DAT_00617710;
long DAT_00617714;
long DAT_00617718;
long DAT_0061771c;
long DAT_00617740;
long DAT_00617750;
long DAT_00617760;
long DAT_00617768;
long DAT_00617780;
long DAT_00617788;
long DAT_006177e0;
long DAT_006177e8;
long DAT_006177f0;
long DAT_006177f8;
long DAT_00617978;
long DAT_0061797c;
long DAT_00617980;
long DAT_00617988;
long DAT_00617990;
long DAT_00617998;
long DAT_006179a8;
long fde_00415538;
long null_ARRAY_004133c0;
long null_ARRAY_004145a0;
long null_ARRAY_00414780;
long null_ARRAY_00414ba0;
long null_ARRAY_00414dc0;
long null_ARRAY_006176c0;
long null_ARRAY_00617700;
long null_ARRAY_006177a0;
long null_ARRAY_006177d0;
long null_ARRAY_00617800;
long null_ARRAY_00617900;
long null_ARRAY_00617940;
long PTR_DAT_006176a0;
long PTR_null_ARRAY_006176f8;
long register0x00000020;
void
FUN_00402710 (uint uParm1)
{
  int iVar1;
  long lVar2;
  undefined8 uVar3;
  undefined **ppuVar4;
  char *pcVar5;
  undefined *puVar6;
  undefined *local_88;
  char *local_80;
  undefined8 local_78;
  char *local_70;
  char *local_68;
  char *local_60;
  char *local_58;
  char *local_50;
  char *local_48;
  char *local_40;
  char *local_38;
  char *local_30;
  undefined8 local_28;
  undefined8 local_20;

  if (uParm1 == 0)
    goto LAB_0040273d;
  func_0x00401e40 (DAT_00617760, "Try \'%s --help\' for more information.\n",
		   DAT_006177f8);
  do
    {
      func_0x00402050 ((ulong) uParm1);
    LAB_0040273d:
      ;
      func_0x00401bd0
	("Usage: %s [OPTION]... GROUP FILE...\n  or:  %s [OPTION]... --reference=RFILE FILE...\n",
	 DAT_006177f8, DAT_006177f8);
      uVar3 = DAT_00617740;
      func_0x00401e50
	("Change the group of each FILE to GROUP.\nWith --reference, change the group of each FILE to that of RFILE.\n\n",
	 DAT_00617740);
      func_0x00401e50
	("  -c, --changes          like verbose but report only when a change is made\n  -f, --silent, --quiet  suppress most error messages\n  -v, --verbose          output a diagnostic for every file processed\n",
	 uVar3);
      func_0x00401e50
	("      --dereference      affect the referent of each symbolic link (this is\n                         the default), rather than the symbolic link itself\n  -h, --no-dereference   affect symbolic links instead of any referenced file\n",
	 uVar3);
      func_0x00401e50
	("                         (useful only on systems that can change the\n                         ownership of a symlink)\n",
	 uVar3);
      func_0x00401e50
	("      --no-preserve-root  do not treat \'/\' specially (the default)\n      --preserve-root    fail to operate recursively on \'/\'\n",
	 uVar3);
      func_0x00401e50
	("      --reference=RFILE  use RFILE\'s group rather than specifying a\n                         GROUP value\n",
	 uVar3);
      func_0x00401e50
	("  -R, --recursive        operate on files and directories recursively\n",
	 uVar3);
      func_0x00401e50
	("\nThe following options modify how a hierarchy is traversed when the -R\noption is also specified.  If more than one is specified, only the final\none takes effect.\n\n  -H                     if a command line argument is a symbolic link\n                         to a directory, traverse it\n  -L                     traverse every symbolic link to a directory\n                         encountered\n  -P                     do not traverse any symbolic links (default)\n\n",
	 uVar3);
      func_0x00401e50 ("      --help     display this help and exit\n",
		       uVar3);
      func_0x00401e50
	("      --version  output version information and exit\n", uVar3);
      func_0x00401bd0
	("\nExamples:\n  %s staff /u      Change the group of /u to \"staff\".\n  %s -hR staff /u  Change the group of /u and subfiles to \"staff\".\n",
	 DAT_006177f8, DAT_006177f8);
      local_88 = &DAT_00412a40;
      local_80 = "test invocation";
      puVar6 = &DAT_00412a40;
      local_78 = 0x412aa9;
      local_70 = "Multi-call invocation";
      local_68 = "sha224sum";
      local_60 = "sha2 utilities";
      local_58 = "sha256sum";
      local_50 = "sha2 utilities";
      local_48 = "sha384sum";
      local_40 = "sha2 utilities";
      local_38 = "sha512sum";
      local_30 = "sha2 utilities";
      local_28 = 0;
      local_20 = 0;
      ppuVar4 = &local_88;
      do
	{
	  iVar1 = func_0x00401f80 ("chgrp", puVar6);
	  if (iVar1 == 0)
	    break;
	  ppuVar4 = ppuVar4 + 2;
	  puVar6 = *ppuVar4;
	}
      while (puVar6 != (undefined *) 0x0);
      pcVar5 = ppuVar4[1];
      if (pcVar5 == (char *) 0x0)
	{
	  func_0x00401bd0 ("\n%s online help: <%s>\n", "GNU coreutils",
			   "https://www.gnu.org/software/coreutils/");
	  lVar2 = func_0x00401ff0 (5, 0);
	  if (lVar2 != 0)
	    {
	      iVar1 = func_0x00401ee0 (lVar2, &DAT_00412aca, 3);
	      if (iVar1 != 0)
		{
		  pcVar5 = "chgrp";
		  goto LAB_00402949;
		}
	    }
	  func_0x00401bd0 ("Full documentation at: <%s%s>\n",
			   "https://www.gnu.org/software/coreutils/",
			   "chgrp");
	LAB_00402972:
	  ;
	  pcVar5 = "chgrp";
	  uVar3 = 0x412a62;
	}
      else
	{
	  func_0x00401bd0 ("\n%s online help: <%s>\n", "GNU coreutils",
			   "https://www.gnu.org/software/coreutils/");
	  lVar2 = func_0x00401ff0 (5, 0);
	  if (lVar2 != 0)
	    {
	      iVar1 = func_0x00401ee0 (lVar2, &DAT_00412aca, 3);
	      if (iVar1 != 0)
		{
		LAB_00402949:
		  ;
		  func_0x00401bd0
		    ("Report %s translation bugs to <https://translationproject.org/team/>\n",
		     "chgrp");
		}
	    }
	  func_0x00401bd0 ("Full documentation at: <%s%s>\n",
			   "https://www.gnu.org/software/coreutils/",
			   "chgrp");
	  uVar3 = 0x4146f7;
	  if (pcVar5 == "chgrp")
	    goto LAB_00402972;
	}
      func_0x00401bd0
	("or available locally via: info \'(coreutils) %s%s\'\n", pcVar5,
	 uVar3);
    }
  while (true);
}

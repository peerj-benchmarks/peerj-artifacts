
#include "[.h"

long null_ARRAY_00610d6_0_8_;
long null_ARRAY_00610d6_8_8_;
long null_ARRAY_00610f4_0_8_;
long null_ARRAY_00610f4_16_8_;
long null_ARRAY_00610f4_24_8_;
long null_ARRAY_00610f4_32_8_;
long null_ARRAY_00610f4_40_8_;
long null_ARRAY_00610f4_48_8_;
long null_ARRAY_00610f4_8_8_;
long local_2f_1_1_;
long local_5b_1_1_;
long local_9d_0_4_;
long local_a0_0_8_;
long local_a0_4_6_;
long local_a0_8_2_;
long local_a1_4_4_;
long local_a2_4_4_;
long local_a3_0_4_;
long local_a3_4_4_;
long local_a4_0_2_;
long local_a4_0_4_;
long local_a4_0_8_;
long local_a4_1_9_;
long local_a4_4_6_;
long local_a4_8_2_;
long local_a5_0_4_;
long local_a5_4_4_;
long DAT_0040d640;
long DAT_0040d641;
long DAT_0040d643;
long DAT_0040d646;
long DAT_0040d64a;
long DAT_0040d64e;
long DAT_0040d652;
long DAT_0040d656;
long DAT_0040d65a;
long DAT_0040d65e;
long DAT_0040d662;
long DAT_0040d666;
long DAT_0040d6f8;
long DAT_0040d716;
long DAT_0040d718;
long DAT_0040d71b;
long DAT_0040d7ed;
long DAT_0040d824;
long DAT_0040e798;
long DAT_0040e79c;
long DAT_0040e79e;
long DAT_0040e7a2;
long DAT_0040e7a5;
long DAT_0040e7a7;
long DAT_0040e7ab;
long DAT_0040e7af;
long DAT_0040ed4b;
long DAT_0040ed4d;
long DAT_0040f0f5;
long DAT_0040f102;
long DAT_0040f106;
long DAT_0040f186;
long DAT_00610930;
long DAT_00610940;
long DAT_00610950;
long DAT_00610d08;
long DAT_00610d70;
long DAT_00610d80;
long DAT_00610d90;
long DAT_00610da0;
long DAT_00610da8;
long DAT_00610dc0;
long DAT_00610dc8;
long DAT_00610e10;
long DAT_00610e18;
long DAT_00610e1c;
long DAT_00610e20;
long DAT_00610e28;
long DAT_00610e30;
long DAT_00610f78;
long DAT_00610f80;
long DAT_00610f88;
long DAT_00610f90;
long DAT_00610f98;
long DAT_00610fa0;
long _DYNAMIC;
long fde_0040fb98;
long null_ARRAY_0040f420;
long null_ARRAY_0040f650;
long null_ARRAY_00610d20;
long null_ARRAY_00610d60;
long null_ARRAY_00610de0;
long null_ARRAY_00610e40;
long null_ARRAY_00610f40;
long PTR_DAT_00610d00;
long PTR_null_ARRAY_00610d58;
long register0x00000020;
void
FUN_00403130 (uint uParm1)
{
  int iVar1;
  long lVar2;
  undefined8 uVar3;
  long *plVar4;
  long lVar5;
  long local_88;
  char *local_80;
  undefined8 local_78;
  char *local_70;
  char *local_68;
  char *local_60;
  char *local_58;
  char *local_50;
  char *local_48;
  char *local_40;
  char *local_38;
  char *local_30;
  undefined8 local_28;
  undefined8 local_20;

  if (uParm1 == 0)
    goto LAB_0040315d;
  func_0x004016f0 (DAT_00610da0, "Try \'%s --help\' for more information.\n",
		   DAT_00610e30);
  do
    {
      func_0x00401880 ((ulong) uParm1);
    LAB_0040315d:
      ;
      uVar3 = DAT_00610d80;
      func_0x00401700
	("Usage: test EXPRESSION\n  or:  test\n  or:  [ EXPRESSION ]\n  or:  [ ]\n  or:  [ OPTION\n",
	 DAT_00610d80);
      func_0x00401700 ("Exit with the status determined by EXPRESSION.\n\n",
		       uVar3);
      func_0x00401700 ("      --help     display this help and exit\n",
		       uVar3);
      func_0x00401700
	("      --version  output version information and exit\n", uVar3);
      func_0x00401700
	("\nAn omitted EXPRESSION defaults to false.  Otherwise,\nEXPRESSION is true or false and sets exit status.  It is one of:\n",
	 uVar3);
      func_0x00401700
	("\n  ( EXPRESSION )               EXPRESSION is true\n  ! EXPRESSION                 EXPRESSION is false\n  EXPRESSION1 -a EXPRESSION2   both EXPRESSION1 and EXPRESSION2 are true\n  EXPRESSION1 -o EXPRESSION2   either EXPRESSION1 or EXPRESSION2 is true\n",
	 uVar3);
      func_0x00401700
	("\n  -n STRING            the length of STRING is nonzero\n  STRING               equivalent to -n STRING\n  -z STRING            the length of STRING is zero\n  STRING1 = STRING2    the strings are equal\n  STRING1 != STRING2   the strings are not equal\n",
	 uVar3);
      func_0x00401700
	("\n  INTEGER1 -eq INTEGER2   INTEGER1 is equal to INTEGER2\n  INTEGER1 -ge INTEGER2   INTEGER1 is greater than or equal to INTEGER2\n  INTEGER1 -gt INTEGER2   INTEGER1 is greater than INTEGER2\n  INTEGER1 -le INTEGER2   INTEGER1 is less than or equal to INTEGER2\n  INTEGER1 -lt INTEGER2   INTEGER1 is less than INTEGER2\n  INTEGER1 -ne INTEGER2   INTEGER1 is not equal to INTEGER2\n",
	 uVar3);
      func_0x00401700
	("\n  FILE1 -ef FILE2   FILE1 and FILE2 have the same device and inode numbers\n  FILE1 -nt FILE2   FILE1 is newer (modification date) than FILE2\n  FILE1 -ot FILE2   FILE1 is older than FILE2\n",
	 uVar3);
      func_0x00401700
	("\n  -b FILE     FILE exists and is block special\n  -c FILE     FILE exists and is character special\n  -d FILE     FILE exists and is a directory\n  -e FILE     FILE exists\n",
	 uVar3);
      func_0x00401700
	("  -f FILE     FILE exists and is a regular file\n  -g FILE     FILE exists and is set-group-ID\n  -G FILE     FILE exists and is owned by the effective group ID\n  -h FILE     FILE exists and is a symbolic link (same as -L)\n  -k FILE     FILE exists and has its sticky bit set\n",
	 uVar3);
      func_0x00401700
	("  -L FILE     FILE exists and is a symbolic link (same as -h)\n  -O FILE     FILE exists and is owned by the effective user ID\n  -p FILE     FILE exists and is a named pipe\n  -r FILE     FILE exists and read permission is granted\n  -s FILE     FILE exists and has a size greater than zero\n",
	 uVar3);
      func_0x00401700
	("  -S FILE     FILE exists and is a socket\n  -t FD       file descriptor FD is opened on a terminal\n  -u FILE     FILE exists and its set-user-ID bit is set\n  -w FILE     FILE exists and write permission is granted\n  -x FILE     FILE exists and execute (or search) permission is granted\n",
	 uVar3);
      func_0x00401700
	("\nExcept for -h and -L, all FILE-related tests dereference symbolic links.\nBeware that parentheses need to be escaped (e.g., by backslashes) for shells.\nINTEGER may also be -l STRING, which evaluates to the length of STRING.\n",
	 uVar3);
      func_0x00401700
	("\nNOTE: Binary -a and -o are inherently ambiguous.  Use \'test EXPR1 && test\nEXPR2\' or \'test EXPR1 || test EXPR2\' instead.\n",
	 uVar3);
      func_0x00401700
	("\nNOTE: [ honors the --help and --version options, but test does not.\ntest treats each of those as it treats any other nonempty STRING.\n",
	 uVar3);
      func_0x00401530
	("\nNOTE: your shell may have its own version of %s, which usually supersedes\nthe version described here.  Please refer to your shell\'s documentation\nfor details about the options it supports.\n",
	 "test and/or [");
      local_88 = 0x40d769;
      local_80 = "test invocation";
      lVar5 = 0x40d769;
      local_78 = 0x40d7cc;
      local_70 = "Multi-call invocation";
      local_68 = "sha224sum";
      local_60 = "sha2 utilities";
      local_58 = "sha256sum";
      local_50 = "sha2 utilities";
      local_48 = "sha384sum";
      local_40 = "sha2 utilities";
      local_38 = "sha512sum";
      local_30 = "sha2 utilities";
      local_28 = 0;
      local_20 = 0;
      plVar4 = &local_88;
      do
	{
	  iVar1 = func_0x004017d0 (0x40d769, lVar5);
	  if (iVar1 == 0)
	    break;
	  plVar4 = plVar4 + 2;
	  lVar5 = *plVar4;
	}
      while (lVar5 != 0);
      lVar5 = plVar4[1];
      if (lVar5 == 0)
	{
	  func_0x00401530 ("\n%s online help: <%s>\n", "GNU coreutils",
			   "https://www.gnu.org/software/coreutils/");
	  lVar5 = func_0x00401830 (5, 0);
	  if (lVar5 != 0)
	    {
	      iVar1 = func_0x00401760 (lVar5, &DAT_0040d7ed, 3);
	      if (iVar1 != 0)
		{
		  lVar5 = 0x40d769;
		  goto LAB_00403399;
		}
	    }
	  func_0x00401530 ("Full documentation at: <%s%s>\n",
			   "https://www.gnu.org/software/coreutils/",
			   0x40d769);
	LAB_004033c2:
	  ;
	  lVar5 = 0x40d769;
	  uVar3 = 0x40d785;
	}
      else
	{
	  func_0x00401530 ("\n%s online help: <%s>\n", "GNU coreutils",
			   "https://www.gnu.org/software/coreutils/");
	  lVar2 = func_0x00401830 (5, 0);
	  if (lVar2 != 0)
	    {
	      iVar1 = func_0x00401760 (lVar2, &DAT_0040d7ed, 3);
	      if (iVar1 != 0)
		{
		LAB_00403399:
		  ;
		  func_0x00401530
		    ("Report %s translation bugs to <https://translationproject.org/team/>\n",
		     0x40d769);
		}
	    }
	  func_0x00401530 ("Full documentation at: <%s%s>\n",
			   "https://www.gnu.org/software/coreutils/",
			   0x40d769);
	  uVar3 = 0x40ed91;
	  if (lVar5 == 0x40d769)
	    goto LAB_004033c2;
	}
      func_0x00401530
	("or available locally via: info \'(coreutils) %s%s\'\n", lVar5,
	 uVar3);
    }
  while (true);
}

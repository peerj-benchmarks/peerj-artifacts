
#include "comm.h"

long DAT_0061085_0_1_;
long DAT_0061085_1_1_;
long null_ARRAY_0061078_0_8_;
long null_ARRAY_0061078_8_8_;
long null_ARRAY_0061098_0_8_;
long null_ARRAY_0061098_16_8_;
long null_ARRAY_0061098_24_8_;
long null_ARRAY_0061098_32_8_;
long null_ARRAY_0061098_40_8_;
long null_ARRAY_0061098_48_8_;
long null_ARRAY_0061098_8_8_;
long null_ARRAY_006109c_0_4_;
long null_ARRAY_006109c_16_8_;
long null_ARRAY_006109c_4_4_;
long null_ARRAY_006109c_8_4_;
long local_2f_1_1_;
long local_4_4_4_;
long local_5b_1_1_;
long local_9d_0_4_;
long local_a0_0_8_;
long local_a0_4_6_;
long local_a0_8_2_;
long local_a1_4_4_;
long local_a2_4_4_;
long local_a3_0_4_;
long local_a3_4_4_;
long local_a4_0_2_;
long local_a4_0_4_;
long local_a4_0_8_;
long local_a4_1_9_;
long local_a4_4_6_;
long local_a4_8_2_;
long local_a5_0_4_;
long local_a5_4_4_;
long DAT_00000010;
long DAT_0040dbd4;
long DAT_0040dbd6;
long DAT_0040dc5d;
long DAT_0040dc85;
long DAT_0040dd01;
long DAT_0040de58;
long DAT_0040dea0;
long DAT_0040dea4;
long DAT_0040dea8;
long DAT_0040deab;
long DAT_0040dead;
long DAT_0040deb1;
long DAT_0040deb5;
long DAT_0040e46b;
long DAT_0040e88a;
long DAT_0040e991;
long DAT_0040e997;
long DAT_0040e9a9;
long DAT_0040e9aa;
long DAT_0040e9c8;
long DAT_0040ea46;
long DAT_00610338;
long DAT_00610348;
long DAT_00610358;
long DAT_00610728;
long DAT_00610738;
long DAT_00610790;
long DAT_00610794;
long DAT_00610798;
long DAT_0061079c;
long DAT_006107c0;
long DAT_006107c8;
long DAT_006107d0;
long DAT_006107e0;
long DAT_006107e8;
long DAT_00610800;
long DAT_00610808;
long DAT_00610850;
long DAT_00610858;
long DAT_0061085c;
long DAT_0061085d;
long DAT_0061085f;
long DAT_00610860;
long DAT_00610861;
long DAT_00610862;
long DAT_00610863;
long DAT_00610868;
long DAT_00610870;
long DAT_00610878;
long DAT_006109b8;
long DAT_006109f8;
long DAT_00610a00;
long DAT_00610a08;
long DAT_00610a18;
long fde_0040f450;
long null_ARRAY_0040dd40;
long null_ARRAY_0040ece0;
long null_ARRAY_0040ef10;
long null_ARRAY_00610740;
long null_ARRAY_00610780;
long null_ARRAY_00610820;
long null_ARRAY_00610880;
long null_ARRAY_00610980;
long null_ARRAY_006109c0;
long PTR_DAT_00610720;
long PTR_DAT_00610730;
long PTR_null_ARRAY_00610778;
long register0x00000020;
void
FUN_004024d0 (uint uParm1)
{
  undefined8 uVar1;
  int iVar2;
  long lVar3;
  undefined *puVar4;
  undefined **ppuVar5;
  undefined *puVar6;
  undefined *local_88;
  char *local_80;
  undefined8 local_78;
  char *local_70;
  char *local_68;
  char *local_60;
  char *local_58;
  char *local_50;
  char *local_48;
  char *local_40;
  char *local_38;
  char *local_30;
  undefined8 local_28;
  undefined8 local_20;

  if (uParm1 == 0)
    goto LAB_004024fd;
  func_0x00401700 (DAT_006107e0, "Try \'%s --help\' for more information.\n",
		   DAT_00610878);
  do
    {
      func_0x004018d0 ((ulong) uParm1);
    LAB_004024fd:
      ;
      func_0x00401590 ("Usage: %s [OPTION]... FILE1 FILE2\n", DAT_00610878);
      uVar1 = DAT_006107c0;
      func_0x00401710 ("Compare sorted files FILE1 and FILE2 line by line.\n",
		       DAT_006107c0);
      func_0x00401710
	("\nWhen FILE1 or FILE2 (not both) is -, read standard input.\n",
	 uVar1);
      func_0x00401710
	("\nWith no options, produce three-column output.  Column one contains\nlines unique to FILE1, column two contains lines unique to FILE2,\nand column three contains lines common to both files.\n",
	 uVar1);
      func_0x00401710
	("\n  -1              suppress column 1 (lines unique to FILE1)\n  -2              suppress column 2 (lines unique to FILE2)\n  -3              suppress column 3 (lines that appear in both files)\n",
	 uVar1);
      func_0x00401710
	("\n  --check-order     check that the input is correctly sorted, even\n                      if all input lines are pairable\n  --nocheck-order   do not check that the input is correctly sorted\n",
	 uVar1);
      func_0x00401710
	("  --output-delimiter=STR  separate columns with STR\n", uVar1);
      func_0x00401710 ("  --total           output a summary\n", uVar1);
      func_0x00401710
	("  -z, --zero-terminated    line delimiter is NUL, not newline\n",
	 uVar1);
      func_0x00401710 ("      --help     display this help and exit\n",
		       uVar1);
      func_0x00401710
	("      --version  output version information and exit\n", uVar1);
      func_0x00401710
	("\nNote, comparisons honor the rules specified by \'LC_COLLATE\'.\n",
	 uVar1);
      func_0x00401590
	("\nExamples:\n  %s -12 file1 file2  Print only lines present in both file1 and file2.\n  %s -3 file1 file2  Print lines in file1 not in file2, and vice versa.\n",
	 DAT_00610878, DAT_00610878);
      local_88 = &DAT_0040dbd4;
      local_80 = "test invocation";
      puVar6 = &DAT_0040dbd4;
      local_78 = 0x40dc3c;
      local_70 = "Multi-call invocation";
      local_68 = "sha224sum";
      local_60 = "sha2 utilities";
      local_58 = "sha256sum";
      local_50 = "sha2 utilities";
      local_48 = "sha384sum";
      local_40 = "sha2 utilities";
      local_38 = "sha512sum";
      local_30 = "sha2 utilities";
      local_28 = 0;
      local_20 = 0;
      ppuVar5 = &local_88;
      do
	{
	  iVar2 = func_0x00401810 (&DAT_0040dbd6, puVar6);
	  if (iVar2 == 0)
	    break;
	  ppuVar5 = ppuVar5 + 2;
	  puVar6 = *ppuVar5;
	}
      while (puVar6 != (undefined *) 0x0);
      puVar6 = ppuVar5[1];
      if (puVar6 == (undefined *) 0x0)
	{
	  func_0x00401590 ("\n%s online help: <%s>\n", "GNU coreutils",
			   "https://www.gnu.org/software/coreutils/");
	  lVar3 = func_0x00401870 (5, 0);
	  if (lVar3 != 0)
	    {
	      iVar2 = func_0x00401780 (lVar3, &DAT_0040dc5d, 3);
	      if (iVar2 != 0)
		{
		  puVar6 = &DAT_0040dbd6;
		  goto LAB_00402711;
		}
	    }
	  func_0x00401590 ("Full documentation at: <%s%s>\n",
			   "https://www.gnu.org/software/coreutils/",
			   &DAT_0040dbd6);
	LAB_0040273a:
	  ;
	  puVar6 = &DAT_0040dbd6;
	  puVar4 = (undefined *) 0x40dbf5;
	}
      else
	{
	  func_0x00401590 ("\n%s online help: <%s>\n", "GNU coreutils",
			   "https://www.gnu.org/software/coreutils/");
	  lVar3 = func_0x00401870 (5, 0);
	  if (lVar3 != 0)
	    {
	      iVar2 = func_0x00401780 (lVar3, &DAT_0040dc5d, 3);
	      if (iVar2 != 0)
		{
		LAB_00402711:
		  ;
		  func_0x00401590
		    ("Report %s translation bugs to <https://translationproject.org/team/>\n",
		     &DAT_0040dbd6);
		}
	    }
	  func_0x00401590 ("Full documentation at: <%s%s>\n",
			   "https://www.gnu.org/software/coreutils/",
			   &DAT_0040dbd6);
	  puVar4 = &DAT_0040dd01;
	  if (puVar6 == &DAT_0040dbd6)
	    goto LAB_0040273a;
	}
      func_0x00401590
	("or available locally via: info \'(coreutils) %s%s\'\n", puVar6,
	 puVar4);
    }
  while (true);
}

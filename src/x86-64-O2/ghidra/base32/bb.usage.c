
#include "base32.h"

long null_ARRAY_00610c4_0_8_;
long null_ARRAY_00610c4_8_8_;
long null_ARRAY_00610e4_0_8_;
long null_ARRAY_00610e4_16_8_;
long null_ARRAY_00610e4_24_8_;
long null_ARRAY_00610e4_32_8_;
long null_ARRAY_00610e4_40_8_;
long null_ARRAY_00610e4_48_8_;
long null_ARRAY_00610e4_8_8_;
long null_ARRAY_00610e8_0_4_;
long null_ARRAY_00610e8_16_8_;
long null_ARRAY_00610e8_4_4_;
long null_ARRAY_00610e8_8_4_;
long local_2f_1_1_;
long local_4_4_4_;
long local_5b_1_1_;
long local_9d_0_4_;
long local_a0_0_8_;
long local_a0_4_6_;
long local_a0_8_2_;
long local_a1_4_4_;
long local_a2_4_4_;
long local_a3_0_4_;
long local_a3_4_4_;
long local_a4_0_2_;
long local_a4_0_4_;
long local_a4_0_8_;
long local_a4_1_9_;
long local_a4_4_6_;
long local_a4_8_2_;
long local_a5_0_4_;
long local_a5_4_4_;
long DAT_00000008;
long DAT_00000010;
long DAT_0040db25;
long DAT_0040dbb0;
long DAT_0040dbd6;
long DAT_0040dbec;
long DAT_0040e308;
long DAT_0040e30c;
long DAT_0040e310;
long DAT_0040e313;
long DAT_0040e315;
long DAT_0040e319;
long DAT_0040e31d;
long DAT_0040e8ab;
long DAT_0040ee48;
long DAT_0040ef51;
long DAT_0040ef57;
long DAT_0040ef69;
long DAT_0040ef6a;
long DAT_0040ef88;
long DAT_0040ef8c;
long DAT_0040f00e;
long DAT_006107e0;
long DAT_006107f0;
long DAT_00610800;
long DAT_00610be8;
long DAT_00610c50;
long DAT_00610c54;
long DAT_00610c58;
long DAT_00610c5c;
long DAT_00610c80;
long DAT_00610c88;
long DAT_00610c90;
long DAT_00610ca0;
long DAT_00610ca8;
long DAT_00610cc0;
long DAT_00610cc8;
long DAT_00610d10;
long DAT_00610d18;
long DAT_00610d20;
long DAT_00610eb8;
long DAT_00610ec0;
long DAT_00610ec8;
long DAT_00610ed8;
long fde_0040f9f0;
long null_ARRAY_0040e080;
long null_ARRAY_0040e1c0;
long null_ARRAY_0040f2a0;
long null_ARRAY_0040f4d0;
long null_ARRAY_00610c00;
long null_ARRAY_00610c40;
long null_ARRAY_00610ce0;
long null_ARRAY_00610d40;
long null_ARRAY_00610e40;
long null_ARRAY_00610e80;
long PTR_DAT_00610be0;
long PTR_null_ARRAY_00610c38;
long register0x00000020;
void
FUN_00402360 (uint uParm1)
{
  int iVar1;
  long lVar2;
  undefined8 uVar3;
  undefined **ppuVar4;
  char *pcVar5;
  undefined *puVar6;
  undefined *local_88;
  char *local_80;
  undefined8 local_78;
  char *local_70;
  char *local_68;
  char *local_60;
  char *local_58;
  char *local_50;
  char *local_48;
  char *local_40;
  char *local_38;
  char *local_30;
  undefined8 local_28;
  undefined8 local_20;

  if (uParm1 == 0)
    goto LAB_0040238d;
  func_0x00401820 (DAT_00610ca0, "Try \'%s --help\' for more information.\n",
		   DAT_00610d20);
  do
    {
      func_0x00401a00 ((ulong) uParm1);
    LAB_0040238d:
      ;
      func_0x00401690
	("Usage: %s [OPTION]... [FILE]\nBase%d encode or decode FILE, or standard input, to standard output.\n",
	 DAT_00610d20, 0x20);
      uVar3 = DAT_00610c80;
      func_0x00401830
	("\nWith no FILE, or when FILE is -, read standard input.\n",
	 DAT_00610c80);
      func_0x00401830
	("\nMandatory arguments to long options are mandatory for short options too.\n",
	 uVar3);
      func_0x00401830
	("  -d, --decode          decode data\n  -i, --ignore-garbage  when decoding, ignore non-alphabet characters\n  -w, --wrap=COLS       wrap encoded lines after COLS character (default 76).\n                          Use 0 to disable line wrapping\n\n",
	 uVar3);
      func_0x00401830 ("      --help     display this help and exit\n",
		       uVar3);
      func_0x00401830
	("      --version  output version information and exit\n", uVar3);
      func_0x00401690
	("\nThe data are encoded as described for the %s alphabet in RFC 4648.\nWhen decoding, the input may contain newlines in addition to the bytes of\nthe formal %s alphabet.  Use --ignore-garbage to attempt to recover\nfrom any other non-alphabet bytes in the encoded stream.\n",
	 "base32", "base32");
      local_88 = &DAT_0040db25;
      local_80 = "test invocation";
      puVar6 = &DAT_0040db25;
      local_78 = 0x40db8f;
      local_70 = "Multi-call invocation";
      local_68 = "sha224sum";
      local_60 = "sha2 utilities";
      local_58 = "sha256sum";
      local_50 = "sha2 utilities";
      local_48 = "sha384sum";
      local_40 = "sha2 utilities";
      local_38 = "sha512sum";
      local_30 = "sha2 utilities";
      local_28 = 0;
      local_20 = 0;
      ppuVar4 = &local_88;
      do
	{
	  iVar1 = func_0x00401940 ("base32", puVar6);
	  if (iVar1 == 0)
	    break;
	  ppuVar4 = ppuVar4 + 2;
	  puVar6 = *ppuVar4;
	}
      while (puVar6 != (undefined *) 0x0);
      pcVar5 = ppuVar4[1];
      if (pcVar5 == (char *) 0x0)
	{
	  func_0x00401690 ("\n%s online help: <%s>\n", "GNU coreutils",
			   "https://www.gnu.org/software/coreutils/");
	  lVar2 = func_0x004019a0 (5, 0);
	  if (lVar2 != 0)
	    {
	      iVar1 = func_0x00401890 (lVar2, &DAT_0040dbb0, 3);
	      if (iVar1 != 0)
		{
		  pcVar5 = "base32";
		  goto LAB_00402559;
		}
	    }
	  func_0x00401690 ("Full documentation at: <%s%s>\n",
			   "https://www.gnu.org/software/coreutils/",
			   "base32");
	LAB_00402582:
	  ;
	  pcVar5 = "base32";
	  uVar3 = 0x40db48;
	}
      else
	{
	  func_0x00401690 ("\n%s online help: <%s>\n", "GNU coreutils",
			   "https://www.gnu.org/software/coreutils/");
	  lVar2 = func_0x004019a0 (5, 0);
	  if (lVar2 != 0)
	    {
	      iVar1 = func_0x00401890 (lVar2, &DAT_0040dbb0, 3);
	      if (iVar1 != 0)
		{
		LAB_00402559:
		  ;
		  func_0x00401690
		    ("Report %s translation bugs to <https://translationproject.org/team/>\n",
		     "base32");
		}
	    }
	  func_0x00401690 ("Full documentation at: <%s%s>\n",
			   "https://www.gnu.org/software/coreutils/",
			   "base32");
	  uVar3 = 0x40ef87;
	  if (pcVar5 == "base32")
	    goto LAB_00402582;
	}
      func_0x00401690
	("or available locally via: info \'(coreutils) %s%s\'\n", pcVar5,
	 uVar3);
    }
  while (true);
}

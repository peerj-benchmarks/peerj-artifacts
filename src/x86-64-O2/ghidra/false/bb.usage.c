
#include "false.h"

long null_ARRAY_0060d9c_0_8_;
long null_ARRAY_0060d9c_8_8_;
long null_ARRAY_0060dbc_0_8_;
long null_ARRAY_0060dbc_16_8_;
long null_ARRAY_0060dbc_24_8_;
long null_ARRAY_0060dbc_32_8_;
long null_ARRAY_0060dbc_40_8_;
long null_ARRAY_0060dbc_48_8_;
long null_ARRAY_0060dbc_8_8_;
long local_2f_1_1_;
long local_5b_1_1_;
long local_9d_0_4_;
long local_a0_0_8_;
long local_a0_4_6_;
long local_a0_8_2_;
long local_a1_4_4_;
long local_a2_4_4_;
long local_a3_0_4_;
long local_a3_4_4_;
long local_a4_0_2_;
long local_a4_0_4_;
long local_a4_0_8_;
long local_a4_1_9_;
long local_a4_4_6_;
long local_a4_8_2_;
long local_a5_0_4_;
long local_a5_4_4_;
long DAT_0040b5c0;
long DAT_0040b5c8;
long DAT_0040b5cc;
long DAT_0040b64f;
long DAT_0040b930;
long DAT_0040b934;
long DAT_0040b936;
long DAT_0040b93a;
long DAT_0040b93d;
long DAT_0040b93f;
long DAT_0040b943;
long DAT_0040b947;
long DAT_0040beeb;
long DAT_0040c295;
long DAT_0040c2a6;
long DAT_0040c326;
long DAT_0060d5b8;
long DAT_0060d5c8;
long DAT_0060d5d8;
long DAT_0060d968;
long DAT_0060d9d0;
long DAT_0060da00;
long DAT_0060da10;
long DAT_0060da20;
long DAT_0060da28;
long DAT_0060da40;
long DAT_0060da48;
long DAT_0060da90;
long DAT_0060da98;
long DAT_0060daa0;
long DAT_0060dbf8;
long DAT_0060dc00;
long DAT_0060dc08;
long _DYNAMIC;
long fde_0040cc70;
long null_ARRAY_0040c5c0;
long null_ARRAY_0040c7f0;
long null_ARRAY_0060d9c0;
long null_ARRAY_0060da60;
long null_ARRAY_0060dac0;
long null_ARRAY_0060dbc0;
long PTR_DAT_0060d960;
long PTR_null_ARRAY_0060d9b8;
long register0x00000020;
long stack0x00000008;
void
FUN_00401950 (uint uParm1)
{
  undefined8 uVar1;
  int iVar2;
  long lVar3;
  undefined *puVar4;
  undefined **ppuVar5;
  char *pcVar6;
  undefined *local_88;
  char *local_80;
  undefined8 local_78;
  char *local_70;
  char *local_68;
  char *local_60;
  char *local_58;
  char *local_50;
  char *local_48;
  char *local_40;
  char *local_38;
  char *local_30;
  undefined8 local_28;
  undefined8 local_20;

  ppuVar5 = &local_88;
  func_0x00401360
    ("Usage: %s [ignored command line arguments]\n  or:  %s OPTION\n",
     DAT_0060daa0, DAT_0060daa0);
  func_0x00401360 (&DAT_0040b5c8,
		   "Exit with a status code indicating failure.");
  uVar1 = DAT_0060da00;
  func_0x004014e0 ("      --help     display this help and exit\n",
		   DAT_0060da00);
  func_0x004014e0 ("      --version  output version information and exit\n",
		   uVar1);
  func_0x00401360
    ("\nNOTE: your shell may have its own version of %s, which usually supersedes\nthe version described here.  Please refer to your shell\'s documentation\nfor details about the options it supports.\n",
     "false");
  local_88 = &DAT_0040b5c0;
  local_80 = "test invocation";
  puVar4 = &DAT_0040b5c0;
  local_78 = 0x40b62e;
  local_70 = "Multi-call invocation";
  local_68 = "sha224sum";
  local_60 = "sha2 utilities";
  local_58 = "sha256sum";
  local_50 = "sha2 utilities";
  local_48 = "sha384sum";
  local_40 = "sha2 utilities";
  local_38 = "sha512sum";
  local_30 = "sha2 utilities";
  local_28 = 0;
  local_20 = 0;
  do
    {
      iVar2 = func_0x004015a0 ("false", puVar4);
      if (iVar2 == 0)
	break;
      ppuVar5 = ppuVar5 + 2;
      puVar4 = *ppuVar5;
    }
  while (puVar4 != (undefined *) 0x0);
  pcVar6 = ppuVar5[1];
  if (pcVar6 != (char *) 0x0)
    {
      func_0x00401360 ("\n%s online help: <%s>\n", "GNU coreutils",
		       "https://www.gnu.org/software/coreutils/");
      lVar3 = func_0x004015f0 (5, 0);
      if (lVar3 != 0)
	{
	  iVar2 = func_0x00401530 (lVar3, &DAT_0040b64f, 3);
	  if (iVar2 != 0)
	    goto LAB_00401adf;
	}
      do
	{
	  func_0x00401360 ("Full documentation at: <%s%s>\n",
			   "https://www.gnu.org/software/coreutils/",
			   "false");
	  puVar4 = &DAT_0040b5cc;
	  if (pcVar6 == "false")
	    {
	    LAB_00401b34:
	      ;
	      pcVar6 = "false";
	      puVar4 = (undefined *) 0x40b5e7;
	    }
	  func_0x00401360
	    ("or available locally via: info \'(coreutils) %s%s\'\n", pcVar6,
	     puVar4);
	  func_0x00401630 ((ulong) uParm1);
	LAB_00401ada:
	  ;
	  pcVar6 = "false";
	LAB_00401adf:
	  ;
	  func_0x00401360
	    ("Report %s translation bugs to <https://translationproject.org/team/>\n",
	     "false");
	}
      while (true);
    }
  func_0x00401360 ("\n%s online help: <%s>\n", "GNU coreutils",
		   "https://www.gnu.org/software/coreutils/");
  lVar3 = func_0x004015f0 (5, 0);
  if (lVar3 != 0)
    {
      iVar2 = func_0x00401530 (lVar3, &DAT_0040b64f, 3);
      if (iVar2 != 0)
	goto LAB_00401ada;
    }
  func_0x00401360 ("Full documentation at: <%s%s>\n",
		   "https://www.gnu.org/software/coreutils/", "false");
  goto LAB_00401b34;
}

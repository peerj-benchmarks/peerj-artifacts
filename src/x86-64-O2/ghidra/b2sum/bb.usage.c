
#include "b2sum.h"

long null_ARRAY_0061448_0_8_;
long null_ARRAY_0061448_8_8_;
long null_ARRAY_006146c_0_8_;
long null_ARRAY_006146c_16_8_;
long null_ARRAY_006146c_24_8_;
long null_ARRAY_006146c_32_8_;
long null_ARRAY_006146c_40_8_;
long null_ARRAY_006146c_48_8_;
long null_ARRAY_006146c_8_8_;
long null_ARRAY_0061470_0_4_;
long null_ARRAY_0061470_16_8_;
long null_ARRAY_0061470_4_4_;
long null_ARRAY_0061470_8_4_;
long local_2f_1_1_;
long local_4_4_4_;
long local_5b_1_1_;
long local_9d_0_4_;
long local_a0_0_8_;
long local_a0_4_6_;
long local_a0_8_2_;
long local_a1_4_4_;
long local_a2_4_4_;
long local_a3_0_4_;
long local_a3_4_4_;
long local_a4_0_2_;
long local_a4_0_4_;
long local_a4_0_8_;
long local_a4_1_9_;
long local_a4_4_6_;
long local_a4_8_2_;
long local_a5_0_4_;
long local_a5_4_4_;
long DAT_00000010;
long DAT_00410440;
long DAT_00410446;
long DAT_004104e0;
long DAT_004105b7;
long DAT_004105bc;
long DAT_004105bf;
long DAT_004105c4;
long DAT_00411298;
long DAT_0041129c;
long DAT_004112a0;
long DAT_004112a3;
long DAT_004112a5;
long DAT_004112a9;
long DAT_004112ad;
long DAT_0041182b;
long DAT_00411f88;
long DAT_00412091;
long DAT_00412097;
long DAT_004120a9;
long DAT_004120aa;
long DAT_004120c8;
long DAT_004120cc;
long DAT_0041214e;
long DAT_00614000;
long DAT_00614010;
long DAT_00614020;
long DAT_00614420;
long DAT_00614438;
long DAT_00614490;
long DAT_00614494;
long DAT_00614498;
long DAT_0061449c;
long DAT_006144c0;
long DAT_006144c8;
long DAT_006144d0;
long DAT_006144e0;
long DAT_006144e8;
long DAT_00614500;
long DAT_00614508;
long DAT_00614550;
long DAT_00614558;
long DAT_0061455c;
long DAT_0061455d;
long DAT_0061455e;
long DAT_0061455f;
long DAT_00614560;
long DAT_00614568;
long DAT_00614570;
long DAT_00614578;
long DAT_00614580;
long DAT_00614588;
long DAT_00614590;
long DAT_006146f8;
long DAT_00614738;
long DAT_00614740;
long DAT_00614748;
long DAT_00614758;
long _DYNAMIC;
long fde_00412b80;
long null_ARRAY_00411080;
long null_ARRAY_00411220;
long null_ARRAY_00411230;
long null_ARRAY_004123e0;
long null_ARRAY_00412610;
long null_ARRAY_00614440;
long null_ARRAY_00614480;
long null_ARRAY_00614520;
long null_ARRAY_006145c0;
long null_ARRAY_006146c0;
long null_ARRAY_00614700;
long PTR_DAT_00614430;
long PTR_FUN_00614428;
long PTR_null_ARRAY_00614478;
long register0x00000020;
long stack0x00000008;
void
FUN_00403020 (uint uParm1)
{
  int iVar1;
  long lVar2;
  undefined8 uVar3;
  undefined **ppuVar4;
  char *pcVar5;
  undefined *puVar6;
  undefined *local_88;
  char *local_80;
  undefined8 local_78;
  char *local_70;
  char *local_68;
  char *local_60;
  char *local_58;
  char *local_50;
  char *local_48;
  char *local_40;
  char *local_38;
  char *local_30;
  undefined8 local_28;
  undefined8 local_20;

  if (uParm1 == 0)
    goto LAB_0040304d;
  func_0x00401940 (DAT_006144e0, "Try \'%s --help\' for more information.\n",
		   DAT_00614590);
  do
    {
      func_0x00401b30 ((ulong) uParm1);
    LAB_0040304d:
      ;
      func_0x00401770
	("Usage: %s [OPTION]... [FILE]...\nPrint or check %s (%d-bit) checksums.\n",
	 DAT_00614590, "BLAKE2", 0x200);
      uVar3 = DAT_006144c0;
      func_0x00401950
	("\nWith no FILE, or when FILE is -, read standard input.\n",
	 DAT_006144c0);
      func_0x00401950 ("\n  -b, --binary         read in binary mode\n",
		       uVar3);
      func_0x00401770
	("  -c, --check          read %s sums from the FILEs and check them\n",
	 "BLAKE2");
      func_0x00401950
	("  -l, --length         digest length in bits; must not exceed the maximum for\n                       the blake2 algorithm and must be a multiple of 8\n",
	 uVar3);
      func_0x00401950 ("      --tag            create a BSD-style checksum\n",
		       uVar3);
      func_0x00401950 ("  -t, --text           read in text mode (default)\n",
		       uVar3);
      func_0x00401950
	("\nThe following five options are useful only when verifying checksums:\n      --ignore-missing  don\'t fail or report status for missing files\n      --quiet          don\'t print OK for each successfully verified file\n      --status         don\'t output anything, status code shows success\n      --strict         exit non-zero for improperly formatted checksum lines\n  -w, --warn           warn about improperly formatted checksum lines\n\n",
	 uVar3);
      func_0x00401950 ("      --help     display this help and exit\n",
		       uVar3);
      func_0x00401950
	("      --version  output version information and exit\n", uVar3);
      func_0x00401770
	("\nThe sums are computed as described in %s.  When checking, the input\nshould be a former output of this program.  The default mode is to print a\nline with checksum, a space, a character indicating input mode (\'*\' for binary,\n\' \' for text or where binary is insignificant), and name for each FILE.\n",
	 "RFC 7693");
      local_88 = &DAT_00410446;
      local_80 = "test invocation";
      puVar6 = &DAT_00410446;
      local_78 = 0x4104bf;
      local_70 = "Multi-call invocation";
      local_68 = "sha224sum";
      local_60 = "sha2 utilities";
      local_58 = "sha256sum";
      local_50 = "sha2 utilities";
      local_48 = "sha384sum";
      local_40 = "sha2 utilities";
      local_38 = "sha512sum";
      local_30 = "sha2 utilities";
      local_28 = 0;
      local_20 = 0;
      ppuVar4 = &local_88;
      do
	{
	  iVar1 = func_0x00401a70 ("b2sum", puVar6);
	  if (iVar1 == 0)
	    break;
	  ppuVar4 = ppuVar4 + 2;
	  puVar6 = *ppuVar4;
	}
      while (puVar6 != (undefined *) 0x0);
      pcVar5 = ppuVar4[1];
      if (pcVar5 == (char *) 0x0)
	{
	  func_0x00401770 ("\n%s online help: <%s>\n", "GNU coreutils",
			   "https://www.gnu.org/software/coreutils/");
	  lVar2 = func_0x00401ad0 (5, 0);
	  if (lVar2 != 0)
	    {
	      iVar1 = func_0x004019c0 (lVar2, &DAT_004104e0, 3);
	      if (iVar1 != 0)
		{
		  pcVar5 = "b2sum";
		  goto LAB_00403251;
		}
	    }
	  func_0x00401770 ("Full documentation at: <%s%s>\n",
			   "https://www.gnu.org/software/coreutils/",
			   "b2sum");
	LAB_0040327a:
	  ;
	  pcVar5 = "b2sum";
	  uVar3 = 0x410478;
	}
      else
	{
	  func_0x00401770 ("\n%s online help: <%s>\n", "GNU coreutils",
			   "https://www.gnu.org/software/coreutils/");
	  lVar2 = func_0x00401ad0 (5, 0);
	  if (lVar2 != 0)
	    {
	      iVar1 = func_0x004019c0 (lVar2, &DAT_004104e0, 3);
	      if (iVar1 != 0)
		{
		LAB_00403251:
		  ;
		  func_0x00401770
		    ("Report %s translation bugs to <https://translationproject.org/team/>\n",
		     "b2sum");
		}
	    }
	  func_0x00401770 ("Full documentation at: <%s%s>\n",
			   "https://www.gnu.org/software/coreutils/",
			   "b2sum");
	  uVar3 = 0x4120c7;
	  if (pcVar5 == "b2sum")
	    goto LAB_0040327a;
	}
      func_0x00401770
	("or available locally via: info \'(coreutils) %s%s\'\n", pcVar5,
	 uVar3);
    }
  while (true);
}

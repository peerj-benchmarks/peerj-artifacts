
#include "seq.h"

long null_ARRAY_00610a0_0_8_;
long null_ARRAY_00610a0_8_8_;
long null_ARRAY_00610c4_0_8_;
long null_ARRAY_00610c4_16_8_;
long null_ARRAY_00610c4_24_8_;
long null_ARRAY_00610c4_32_8_;
long null_ARRAY_00610c4_40_8_;
long null_ARRAY_00610c4_48_8_;
long null_ARRAY_00610c4_8_8_;
long null_ARRAY_00610c8_0_4_;
long null_ARRAY_00610c8_16_8_;
long null_ARRAY_00610c8_4_4_;
long null_ARRAY_00610c8_8_4_;
long local_2f_1_1_;
long local_5b_1_1_;
long local_9d_0_4_;
long local_a0_0_8_;
long local_a0_4_6_;
long local_a0_8_2_;
long local_a1_4_4_;
long local_a2_4_4_;
long local_a3_0_4_;
long local_a3_4_4_;
long local_a4_0_2_;
long local_a4_0_4_;
long local_a4_0_8_;
long local_a4_1_9_;
long local_a4_4_6_;
long local_a4_8_2_;
long local_a5_0_4_;
long local_a5_4_4_;
long local_f_0_8_;
long local_f_4_6_;
long local_f_8_2_;
long DAT_00000010;
long DAT_0040d9d7;
long DAT_0040d9db;
long DAT_0040d9dd;
long DAT_0040da63;
long DAT_0040da8c;
long DAT_0040da8f;
long DAT_0040da92;
long DAT_0040da94;
long DAT_0040daed;
long DAT_0040daf2;
long DAT_0040db2b;
long DAT_0040e300;
long DAT_0040e358;
long DAT_0040e35c;
long DAT_0040e360;
long DAT_0040e363;
long DAT_0040e365;
long DAT_0040e369;
long DAT_0040e36d;
long DAT_0040e8eb;
long DAT_0040ec95;
long DAT_0040edb1;
long DAT_0040edb2;
long DAT_0040edd0;
long DAT_0040ee4e;
long DAT_006105b0;
long DAT_006105c0;
long DAT_006105d0;
long DAT_006109a8;
long DAT_00610a10;
long DAT_00610a14;
long DAT_00610a18;
long DAT_00610a1c;
long DAT_00610a40;
long DAT_00610a50;
long DAT_00610a60;
long DAT_00610a68;
long DAT_00610a80;
long DAT_00610a88;
long DAT_00610af0;
long DAT_00610af8;
long DAT_00610b00;
long DAT_00610b08;
long DAT_00610b10;
long DAT_00610b18;
long DAT_00610cb8;
long DAT_00610cc0;
long DAT_00610cc8;
long DAT_00610cd8;
long _DYNAMIC;
long fde_0040f818;
long null_ARRAY_0040f0e0;
long null_ARRAY_0040f310;
long null_ARRAY_006109c0;
long null_ARRAY_00610a00;
long null_ARRAY_00610aa0;
long null_ARRAY_00610ad0;
long null_ARRAY_00610b40;
long null_ARRAY_00610c40;
long null_ARRAY_00610c80;
long PTR_DAT_006109a0;
long PTR_null_ARRAY_006109f8;
long register0x00000020;
void
FUN_00402b20 (uint uParm1)
{
  int iVar1;
  long lVar2;
  undefined8 uVar3;
  undefined **ppuVar4;
  undefined *puVar5;
  undefined *local_88;
  char *local_80;
  undefined8 local_78;
  char *local_70;
  char *local_68;
  char *local_60;
  char *local_58;
  char *local_50;
  char *local_48;
  char *local_40;
  char *local_38;
  char *local_30;
  undefined8 local_28;
  undefined8 local_20;

  if (uParm1 == 0)
    goto LAB_00402b4d;
  func_0x00401810 (DAT_00610a60, "Try \'%s --help\' for more information.\n",
		   DAT_00610b18);
  do
    {
      func_0x004019b0 ((ulong) uParm1);
    LAB_00402b4d:
      ;
      func_0x00401640
	("Usage: %s [OPTION]... LAST\n  or:  %s [OPTION]... FIRST LAST\n  or:  %s [OPTION]... FIRST INCREMENT LAST\n",
	 DAT_00610b18, DAT_00610b18, DAT_00610b18);
      uVar3 = DAT_00610a40;
      func_0x00401820
	("Print numbers from FIRST to LAST, in steps of INCREMENT.\n",
	 DAT_00610a40);
      func_0x00401820
	("\nMandatory arguments to long options are mandatory for short options too.\n",
	 uVar3);
      func_0x00401820
	("  -f, --format=FORMAT      use printf style floating-point FORMAT\n  -s, --separator=STRING   use STRING to separate numbers (default: \\n)\n  -w, --equal-width        equalize width by padding with leading zeroes\n",
	 uVar3);
      func_0x00401820 ("      --help     display this help and exit\n",
		       uVar3);
      func_0x00401820
	("      --version  output version information and exit\n", uVar3);
      func_0x00401820
	("\nIf FIRST or INCREMENT is omitted, it defaults to 1.  That is, an\nomitted INCREMENT defaults to 1 even when LAST is smaller than FIRST.\nThe sequence of numbers ends when the sum of the current number and\nINCREMENT would become greater than LAST.\nFIRST, INCREMENT, and LAST are interpreted as floating point values.\nINCREMENT is usually positive if FIRST is smaller than LAST, and\nINCREMENT is usually negative if FIRST is greater than LAST.\nINCREMENT must not be 0; none of FIRST, INCREMENT and LAST may be NaN.\n",
	 uVar3);
      func_0x00401820
	("FORMAT must be suitable for printing one argument of type \'double\';\nit defaults to %.PRECf if FIRST, INCREMENT, and LAST are all fixed point\ndecimal numbers with maximum precision PREC, and to %g otherwise.\n",
	 uVar3);
      local_88 = &DAT_0040d9db;
      local_80 = "test invocation";
      puVar5 = &DAT_0040d9db;
      local_78 = 0x40da42;
      local_70 = "Multi-call invocation";
      local_68 = "sha224sum";
      local_60 = "sha2 utilities";
      local_58 = "sha256sum";
      local_50 = "sha2 utilities";
      local_48 = "sha384sum";
      local_40 = "sha2 utilities";
      local_38 = "sha512sum";
      local_30 = "sha2 utilities";
      local_28 = 0;
      local_20 = 0;
      ppuVar4 = &local_88;
      do
	{
	  iVar1 = func_0x004018f0 (&DAT_0040d9dd, puVar5);
	  if (iVar1 == 0)
	    break;
	  ppuVar4 = ppuVar4 + 2;
	  puVar5 = *ppuVar4;
	}
      while (puVar5 != (undefined *) 0x0);
      puVar5 = ppuVar4[1];
      if (puVar5 == (undefined *) 0x0)
	{
	  func_0x00401640 ("\n%s online help: <%s>\n", "GNU coreutils",
			   "https://www.gnu.org/software/coreutils/");
	  lVar2 = func_0x00401960 (5, 0);
	  if (lVar2 != 0)
	    {
	      iVar1 = func_0x00401870 (lVar2, &DAT_0040da63, 3);
	      if (iVar1 != 0)
		{
		  puVar5 = &DAT_0040d9dd;
		  goto LAB_00402d19;
		}
	    }
	  func_0x00401640 ("Full documentation at: <%s%s>\n",
			   "https://www.gnu.org/software/coreutils/",
			   &DAT_0040d9dd);
	LAB_00402d42:
	  ;
	  puVar5 = &DAT_0040d9dd;
	  uVar3 = 0x40d9fb;
	}
      else
	{
	  func_0x00401640 ("\n%s online help: <%s>\n", "GNU coreutils",
			   "https://www.gnu.org/software/coreutils/");
	  lVar2 = func_0x00401960 (5, 0);
	  if (lVar2 != 0)
	    {
	      iVar1 = func_0x00401870 (lVar2, &DAT_0040da63, 3);
	      if (iVar1 != 0)
		{
		LAB_00402d19:
		  ;
		  func_0x00401640
		    ("Report %s translation bugs to <https://translationproject.org/team/>\n",
		     &DAT_0040d9dd);
		}
	    }
	  func_0x00401640 ("Full documentation at: <%s%s>\n",
			   "https://www.gnu.org/software/coreutils/",
			   &DAT_0040d9dd);
	  uVar3 = 0x40edcf;
	  if (puVar5 == &DAT_0040d9dd)
	    goto LAB_00402d42;
	}
      func_0x00401640
	("or available locally via: info \'(coreutils) %s%s\'\n", puVar5,
	 uVar3);
    }
  while (true);
}

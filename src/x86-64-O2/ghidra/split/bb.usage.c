
#include "split.h"

long null_ARRAY_0061450_0_8_;
long null_ARRAY_0061450_8_8_;
long null_ARRAY_0061480_0_8_;
long null_ARRAY_0061480_24_4_;
long null_ARRAY_0061480_48_8_;
long null_ARRAY_0061480_56_8_;
long null_ARRAY_0061480_8_8_;
long null_ARRAY_00614b4_0_8_;
long null_ARRAY_00614b4_16_8_;
long null_ARRAY_00614b4_24_8_;
long null_ARRAY_00614b4_32_8_;
long null_ARRAY_00614b4_40_8_;
long null_ARRAY_00614b4_48_8_;
long null_ARRAY_00614b4_8_8_;
long null_ARRAY_00614b8_0_4_;
long null_ARRAY_00614b8_16_8_;
long null_ARRAY_00614b8_4_4_;
long null_ARRAY_00614b8_8_4_;
long local_2f_1_1_;
long local_4_4_4_;
long local_5b_1_1_;
long local_9d_0_4_;
long local_a0_0_8_;
long local_a0_4_6_;
long local_a0_8_2_;
long local_a1_4_4_;
long local_a2_4_4_;
long local_a3_0_4_;
long local_a3_4_4_;
long local_a4_0_2_;
long local_a4_0_4_;
long local_a4_0_8_;
long local_a4_1_9_;
long local_a4_4_6_;
long local_a4_8_2_;
long local_a5_0_4_;
long local_a5_4_4_;
long DAT_00000010;
long DAT_004103db;
long DAT_0041045a;
long DAT_00410490;
long DAT_004104ac;
long DAT_004104af;
long DAT_00410539;
long DAT_0041059e;
long DAT_004105a1;
long DAT_004105e9;
long DAT_004105ea;
long DAT_004116e8;
long DAT_004116ec;
long DAT_004116f0;
long DAT_004116f3;
long DAT_004116f5;
long DAT_004116f9;
long DAT_004116fd;
long DAT_00411cab;
long DAT_00412248;
long DAT_00412351;
long DAT_00412357;
long DAT_00412369;
long DAT_0041236a;
long DAT_00412388;
long DAT_00412398;
long DAT_0041239c;
long DAT_00614008;
long DAT_00614018;
long DAT_00614028;
long DAT_00614480;
long DAT_00614484;
long DAT_00614490;
long DAT_006144a0;
long DAT_00614510;
long DAT_00614514;
long DAT_00614518;
long DAT_0061451c;
long DAT_00614540;
long DAT_00614544;
long DAT_00614700;
long DAT_00614710;
long DAT_00614720;
long DAT_00614728;
long DAT_00614740;
long DAT_00614748;
long DAT_006147c0;
long DAT_006147c8;
long DAT_006147d0;
long DAT_006147d8;
long DAT_006147e0;
long DAT_006147e1;
long DAT_006147e2;
long DAT_00614890;
long DAT_00614898;
long DAT_006148a0;
long DAT_006148a8;
long DAT_006148b0;
long DAT_006148b8;
long DAT_006148c0;
long DAT_00614a00;
long DAT_00614a08;
long DAT_00614a10;
long DAT_00614a18;
long DAT_00614a20;
long DAT_00614a28;
long DAT_00614a30;
long DAT_00614a38;
long DAT_00614bb8;
long DAT_006153c8;
long DAT_006153d0;
long DAT_006153d8;
long DAT_006153e8;
long fde_00412ea0;
long null_ARRAY_00411443;
long null_ARRAY_004126c0;
long null_ARRAY_004128f0;
long null_ARRAY_006144c0;
long null_ARRAY_00614500;
long null_ARRAY_00614760;
long null_ARRAY_00614800;
long null_ARRAY_00614900;
long null_ARRAY_00614980;
long null_ARRAY_00614a40;
long null_ARRAY_00614b40;
long null_ARRAY_00614b80;
long null_ARRAY_00614bc0;
long PTR_DAT_00614498;
long PTR_null_ARRAY_006144f8;
long PTR_s_abcdefghijklmnopqrstuvwxyz_00614488;
long register0x00000020;
long stack0x00000008;
void
FUN_00404a70 (uint uParm1)
{
  int iVar1;
  long lVar2;
  undefined8 uVar3;
  undefined **ppuVar4;
  char *pcVar5;
  undefined *puVar6;
  undefined *local_88;
  char *local_80;
  undefined8 local_78;
  char *local_70;
  char *local_68;
  char *local_60;
  char *local_58;
  char *local_50;
  char *local_48;
  char *local_40;
  char *local_38;
  char *local_30;
  undefined8 local_28;
  undefined8 local_20;

  if (uParm1 == 0)
    goto LAB_00404a9d;
  func_0x00401ca0 (DAT_00614720, "Try \'%s --help\' for more information.\n",
		   DAT_00614a38);
  do
    {
      func_0x00401e80 ((ulong) uParm1);
    LAB_00404a9d:
      ;
      func_0x00401a30 ("Usage: %s [OPTION]... [FILE [PREFIX]]\n",
		       DAT_00614a38);
      uVar3 = DAT_00614700;
      func_0x00401cb0
	("Output pieces of FILE to PREFIXaa, PREFIXab, ...;\ndefault size is 1000 lines, and default PREFIX is \'x\'.\n",
	 DAT_00614700);
      func_0x00401cb0
	("\nWith no FILE, or when FILE is -, read standard input.\n", uVar3);
      func_0x00401cb0
	("\nMandatory arguments to long options are mandatory for short options too.\n",
	 uVar3);
      func_0x00401ca0 (uVar3,
		       "  -a, --suffix-length=N   generate suffixes of length N (default %d)\n      --additional-suffix=SUFFIX  append an additional SUFFIX to file names\n  -b, --bytes=SIZE        put SIZE bytes per output file\n  -C, --line-bytes=SIZE   put at most SIZE bytes of records per output file\n  -d                      use numeric suffixes starting at 0, not alphabetic\n      --numeric-suffixes[=FROM]  same as -d, but allow setting the start value\n  -x                      use hex suffixes starting at 0, not alphabetic\n      --hex-suffixes[=FROM]  same as -x, but allow setting the start value\n  -e, --elide-empty-files  do not generate empty output files with \'-n\'\n      --filter=COMMAND    write to shell COMMAND; file name is $FILE\n  -l, --lines=NUMBER      put NUMBER lines/records per output file\n  -n, --number=CHUNKS     generate CHUNKS output files; see explanation below\n  -t, --separator=SEP     use SEP instead of newline as the record separator;\n                            \'\\0\' (zero) specifies the NUL character\n  -u, --unbuffered        immediately copy input to output with \'-n r/...\'\n",
		       2);
      func_0x00401cb0
	("      --verbose           print a diagnostic just before each\n                            output file is opened\n",
	 uVar3);
      func_0x00401cb0 ("      --help     display this help and exit\n",
		       uVar3);
      func_0x00401cb0
	("      --version  output version information and exit\n", uVar3);
      func_0x00401cb0
	("\nThe SIZE argument is an integer and optional unit (example: 10K is 10*1024).\nUnits are K,M,G,T,P,E,Z,Y (powers of 1024) or KB,MB,... (powers of 1000).\n",
	 uVar3);
      func_0x00401cb0
	("\nCHUNKS may be:\n  N       split into N files based on size of input\n  K/N     output Kth of N to stdout\n  l/N     split into N files without splitting lines/records\n  l/K/N   output Kth of N to stdout without splitting lines/records\n  r/N     like \'l\' but use round robin distribution\n  r/K/N   likewise but only output Kth of N to stdout\n",
	 uVar3);
      local_88 = &DAT_004104af;
      local_80 = "test invocation";
      puVar6 = &DAT_004104af;
      local_78 = 0x410518;
      local_70 = "Multi-call invocation";
      local_68 = "sha224sum";
      local_60 = "sha2 utilities";
      local_58 = "sha256sum";
      local_50 = "sha2 utilities";
      local_48 = "sha384sum";
      local_40 = "sha2 utilities";
      local_38 = "sha512sum";
      local_30 = "sha2 utilities";
      local_28 = 0;
      local_20 = 0;
      ppuVar4 = &local_88;
      do
	{
	  iVar1 = func_0x00401dd0 ("split", puVar6);
	  if (iVar1 == 0)
	    break;
	  ppuVar4 = ppuVar4 + 2;
	  puVar6 = *ppuVar4;
	}
      while (puVar6 != (undefined *) 0x0);
      pcVar5 = ppuVar4[1];
      if (pcVar5 == (char *) 0x0)
	{
	  func_0x00401a30 ("\n%s online help: <%s>\n", "GNU coreutils",
			   "https://www.gnu.org/software/coreutils/");
	  lVar2 = func_0x00401e30 (5, 0);
	  if (lVar2 != 0)
	    {
	      iVar1 = func_0x00401d10 (lVar2, &DAT_00410539, 3);
	      if (iVar1 != 0)
		{
		  pcVar5 = "split";
		  goto LAB_00404c89;
		}
	    }
	  func_0x00401a30 ("Full documentation at: <%s%s>\n",
			   "https://www.gnu.org/software/coreutils/",
			   "split");
	LAB_00404cb2:
	  ;
	  pcVar5 = "split";
	  uVar3 = 0x4104d1;
	}
      else
	{
	  func_0x00401a30 ("\n%s online help: <%s>\n", "GNU coreutils",
			   "https://www.gnu.org/software/coreutils/");
	  lVar2 = func_0x00401e30 (5, 0);
	  if (lVar2 != 0)
	    {
	      iVar1 = func_0x00401d10 (lVar2, &DAT_00410539, 3);
	      if (iVar1 != 0)
		{
		LAB_00404c89:
		  ;
		  func_0x00401a30
		    ("Report %s translation bugs to <https://translationproject.org/team/>\n",
		     "split");
		}
	    }
	  func_0x00401a30 ("Full documentation at: <%s%s>\n",
			   "https://www.gnu.org/software/coreutils/",
			   "split");
	  uVar3 = 0x412387;
	  if (pcVar5 == "split")
	    goto LAB_00404cb2;
	}
      func_0x00401a30
	("or available locally via: info \'(coreutils) %s%s\'\n", pcVar5,
	 uVar3);
    }
  while (true);
}

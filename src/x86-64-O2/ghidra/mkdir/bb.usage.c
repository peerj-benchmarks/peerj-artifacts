
#include "mkdir.h"

long null_ARRAY_006119a_0_8_;
long null_ARRAY_006119a_8_8_;
long null_ARRAY_00611b8_0_8_;
long null_ARRAY_00611b8_16_8_;
long null_ARRAY_00611b8_24_8_;
long null_ARRAY_00611b8_32_8_;
long null_ARRAY_00611b8_40_8_;
long null_ARRAY_00611b8_48_8_;
long null_ARRAY_00611b8_8_8_;
long null_ARRAY_00611bc_0_4_;
long null_ARRAY_00611bc_16_8_;
long null_ARRAY_00611bc_4_4_;
long null_ARRAY_00611bc_8_4_;
long local_2f_1_1_;
long local_4_4_4_;
long local_5b_1_1_;
long local_9d_0_4_;
long local_a0_0_8_;
long local_a0_4_6_;
long local_a0_8_2_;
long local_a1_4_4_;
long local_a2_4_4_;
long local_a3_0_4_;
long local_a3_4_4_;
long local_a4_0_2_;
long local_a4_0_4_;
long local_a4_0_8_;
long local_a4_1_9_;
long local_a4_4_6_;
long local_a4_8_2_;
long local_a5_0_4_;
long local_a5_4_4_;
long DAT_00000010;
long DAT_0040e949;
long DAT_0040e94b;
long DAT_0040e94f;
long DAT_0040e9d3;
long DAT_0040eb78;
long DAT_0040ed30;
long DAT_0040ed34;
long DAT_0040ed38;
long DAT_0040ed3b;
long DAT_0040ed3d;
long DAT_0040ed41;
long DAT_0040ed45;
long DAT_0040f3f1;
long DAT_0040f795;
long DAT_0040f797;
long DAT_0040f899;
long DAT_0040f89f;
long DAT_0040f8b1;
long DAT_0040f8b2;
long DAT_0040f8d0;
long DAT_0040f8d4;
long DAT_0040f956;
long DAT_00611508;
long DAT_00611518;
long DAT_00611528;
long DAT_00611948;
long DAT_006119b0;
long DAT_006119b4;
long DAT_006119b8;
long DAT_006119bc;
long DAT_006119c0;
long DAT_006119d0;
long DAT_006119e0;
long DAT_006119e8;
long DAT_00611a00;
long DAT_00611a08;
long DAT_00611a50;
long DAT_00611a58;
long DAT_00611a60;
long DAT_00611bf8;
long DAT_00611c00;
long DAT_00611c08;
long DAT_00611c10;
long DAT_00611c20;
long fde_004103a0;
long null_ARRAY_0040ea80;
long null_ARRAY_0040fbe0;
long null_ARRAY_0040fe10;
long null_ARRAY_00611960;
long null_ARRAY_006119a0;
long null_ARRAY_00611a20;
long null_ARRAY_00611a80;
long null_ARRAY_00611b80;
long null_ARRAY_00611bc0;
long PTR_DAT_00611940;
long PTR_null_ARRAY_00611998;
void
FUN_00402330 (uint uParm1)
{
  int iVar1;
  long lVar2;
  undefined8 uVar3;
  undefined **ppuVar4;
  undefined *puVar5;
  undefined *local_88;
  char *local_80;
  undefined8 local_78;
  char *local_70;
  char *local_68;
  char *local_60;
  char *local_58;
  char *local_50;
  char *local_48;
  char *local_40;
  char *local_38;
  char *local_30;
  undefined8 local_28;
  undefined8 local_20;

  if (uParm1 == 0)
    goto LAB_0040235d;
  func_0x00401a10 (DAT_006119e0, "Try \'%s --help\' for more information.\n",
		   DAT_00611a60);
  do
    {
      func_0x00401c00 ((ulong) uParm1);
    LAB_0040235d:
      ;
      func_0x00401830 ("Usage: %s [OPTION]... DIRECTORY...\n", DAT_00611a60);
      uVar3 = DAT_006119c0;
      func_0x00401a20
	("Create the DIRECTORY(ies), if they do not already exist.\n",
	 DAT_006119c0);
      func_0x00401a20
	("\nMandatory arguments to long options are mandatory for short options too.\n",
	 uVar3);
      func_0x00401a20
	("  -m, --mode=MODE   set file mode (as in chmod), not a=rwx - umask\n  -p, --parents     no error if existing, make parent directories as needed\n  -v, --verbose     print a message for each created directory\n",
	 uVar3);
      func_0x00401a20
	("  -Z                   set SELinux security context of each created directory\n                         to the default type\n      --context[=CTX]  like -Z, or if CTX is specified then set the SELinux\n                         or SMACK security context to CTX\n",
	 uVar3);
      func_0x00401a20 ("      --help     display this help and exit\n",
		       uVar3);
      func_0x00401a20
	("      --version  output version information and exit\n", uVar3);
      local_88 = &DAT_0040e949;
      local_80 = "test invocation";
      puVar5 = &DAT_0040e949;
      local_78 = 0x40e9b2;
      local_70 = "Multi-call invocation";
      local_68 = "sha224sum";
      local_60 = "sha2 utilities";
      local_58 = "sha256sum";
      local_50 = "sha2 utilities";
      local_48 = "sha384sum";
      local_40 = "sha2 utilities";
      local_38 = "sha512sum";
      local_30 = "sha2 utilities";
      local_28 = 0;
      local_20 = 0;
      ppuVar4 = &local_88;
      do
	{
	  iVar1 = func_0x00401b50 (&DAT_0040e94b, puVar5);
	  if (iVar1 == 0)
	    break;
	  ppuVar4 = ppuVar4 + 2;
	  puVar5 = *ppuVar4;
	}
      while (puVar5 != (undefined *) 0x0);
      puVar5 = ppuVar4[1];
      if (puVar5 == (undefined *) 0x0)
	{
	  func_0x00401830 ("\n%s online help: <%s>\n", "GNU coreutils",
			   "https://www.gnu.org/software/coreutils/");
	  lVar2 = func_0x00401bb0 (5, 0);
	  if (lVar2 != 0)
	    {
	      iVar1 = func_0x00401aa0 (lVar2, &DAT_0040e9d3, 3);
	      if (iVar1 != 0)
		{
		  puVar5 = &DAT_0040e94b;
		  goto LAB_00402519;
		}
	    }
	  func_0x00401830 ("Full documentation at: <%s%s>\n",
			   "https://www.gnu.org/software/coreutils/",
			   &DAT_0040e94b);
	LAB_00402542:
	  ;
	  puVar5 = &DAT_0040e94b;
	  uVar3 = 0x40e96b;
	}
      else
	{
	  func_0x00401830 ("\n%s online help: <%s>\n", "GNU coreutils",
			   "https://www.gnu.org/software/coreutils/");
	  lVar2 = func_0x00401bb0 (5, 0);
	  if (lVar2 != 0)
	    {
	      iVar1 = func_0x00401aa0 (lVar2, &DAT_0040e9d3, 3);
	      if (iVar1 != 0)
		{
		LAB_00402519:
		  ;
		  func_0x00401830
		    ("Report %s translation bugs to <https://translationproject.org/team/>\n",
		     &DAT_0040e94b);
		}
	    }
	  func_0x00401830 ("Full documentation at: <%s%s>\n",
			   "https://www.gnu.org/software/coreutils/",
			   &DAT_0040e94b);
	  uVar3 = 0x40f8cf;
	  if (puVar5 == &DAT_0040e94b)
	    goto LAB_00402542;
	}
      func_0x00401830
	("or available locally via: info \'(coreutils) %s%s\'\n", puVar5,
	 uVar3);
    }
  while (true);
}

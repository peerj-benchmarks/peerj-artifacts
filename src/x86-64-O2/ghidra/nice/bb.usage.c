
#include "nice.h"

long null_ARRAY_0060fa2_0_8_;
long null_ARRAY_0060fa2_8_8_;
long null_ARRAY_0060fc0_0_8_;
long null_ARRAY_0060fc0_16_8_;
long null_ARRAY_0060fc0_24_8_;
long null_ARRAY_0060fc0_32_8_;
long null_ARRAY_0060fc0_40_8_;
long null_ARRAY_0060fc0_48_8_;
long null_ARRAY_0060fc0_8_8_;
long null_ARRAY_0060fc4_0_4_;
long null_ARRAY_0060fc4_16_8_;
long null_ARRAY_0060fc4_4_4_;
long null_ARRAY_0060fc4_8_4_;
long local_2f_1_1_;
long local_5b_1_1_;
long local_9d_0_4_;
long local_a0_0_8_;
long local_a0_4_6_;
long local_a0_8_2_;
long local_a1_4_4_;
long local_a2_4_4_;
long local_a3_0_4_;
long local_a3_4_4_;
long local_a4_0_2_;
long local_a4_0_4_;
long local_a4_0_8_;
long local_a4_1_9_;
long local_a4_4_6_;
long local_a4_8_2_;
long local_a5_0_4_;
long local_a5_4_4_;
long DAT_00000010;
long DAT_0040ce00;
long DAT_0040ce02;
long DAT_0040ce89;
long DAT_0040ce8d;
long DAT_0040cecb;
long DAT_0040d3d8;
long DAT_0040d3dc;
long DAT_0040d3e0;
long DAT_0040d3e3;
long DAT_0040d3e5;
long DAT_0040d3e9;
long DAT_0040d3ed;
long DAT_0040d96b;
long DAT_0040df05;
long DAT_0040e009;
long DAT_0040e00f;
long DAT_0040e021;
long DAT_0040e022;
long DAT_0040e040;
long DAT_0040e044;
long DAT_0040e0c6;
long DAT_0060f5e8;
long DAT_0060f5f8;
long DAT_0060f608;
long DAT_0060f9c8;
long DAT_0060fa30;
long DAT_0060fa34;
long DAT_0060fa38;
long DAT_0060fa3c;
long DAT_0060fa40;
long DAT_0060fa50;
long DAT_0060fa60;
long DAT_0060fa68;
long DAT_0060fa80;
long DAT_0060fa88;
long DAT_0060fad0;
long DAT_0060fad8;
long DAT_0060fae0;
long DAT_0060fc78;
long DAT_0060fc80;
long DAT_0060fc88;
long DAT_0060fc98;
long _DYNAMIC;
long fde_0040ea48;
long null_ARRAY_0040d300;
long null_ARRAY_0040e360;
long null_ARRAY_0040e590;
long null_ARRAY_0060f9e0;
long null_ARRAY_0060fa20;
long null_ARRAY_0060faa0;
long null_ARRAY_0060fb00;
long null_ARRAY_0060fc00;
long null_ARRAY_0060fc40;
long PTR_DAT_0060f9c0;
long PTR_null_ARRAY_0060fa18;
long register0x00000020;
void
FUN_00401e70 (uint uParm1)
{
  int iVar1;
  long lVar2;
  undefined8 uVar3;
  undefined **ppuVar4;
  undefined *puVar5;
  undefined *local_88;
  char *local_80;
  undefined8 local_78;
  char *local_70;
  char *local_68;
  char *local_60;
  char *local_58;
  char *local_50;
  char *local_48;
  char *local_40;
  char *local_38;
  char *local_30;
  undefined8 local_28;
  undefined8 local_20;

  if (uParm1 == 0)
    goto LAB_00401e9d;
  func_0x004016f0 (DAT_0060fa60, "Try \'%s --help\' for more information.\n",
		   DAT_0060fae0);
  do
    {
      func_0x00401880 ((ulong) uParm1);
    LAB_00401e9d:
      ;
      func_0x00401550 ("Usage: %s [OPTION] [COMMAND [ARG]...]\n",
		       DAT_0060fae0);
      func_0x00401550
	("Run COMMAND with an adjusted niceness, which affects process scheduling.\nWith no COMMAND, print the current niceness.  Niceness values range from\n%d (most favorable to the process) to %d (least favorable to the process).\n",
	 0xffffffec, 0x13);
      uVar3 = DAT_0060fa40;
      func_0x00401700
	("\nMandatory arguments to long options are mandatory for short options too.\n",
	 DAT_0060fa40);
      func_0x00401700
	("  -n, --adjustment=N   add integer N to the niceness (default 10)\n",
	 uVar3);
      func_0x00401700 ("      --help     display this help and exit\n",
		       uVar3);
      func_0x00401700
	("      --version  output version information and exit\n", uVar3);
      func_0x00401550
	("\nNOTE: your shell may have its own version of %s, which usually supersedes\nthe version described here.  Please refer to your shell\'s documentation\nfor details about the options it supports.\n",
	 &DAT_0040ce02);
      local_88 = &DAT_0040ce00;
      local_80 = "test invocation";
      puVar5 = &DAT_0040ce00;
      local_78 = 0x40ce68;
      local_70 = "Multi-call invocation";
      local_68 = "sha224sum";
      local_60 = "sha2 utilities";
      local_58 = "sha256sum";
      local_50 = "sha2 utilities";
      local_48 = "sha384sum";
      local_40 = "sha2 utilities";
      local_38 = "sha512sum";
      local_30 = "sha2 utilities";
      local_28 = 0;
      local_20 = 0;
      ppuVar4 = &local_88;
      do
	{
	  iVar1 = func_0x004017e0 (&DAT_0040ce02, puVar5);
	  if (iVar1 == 0)
	    break;
	  ppuVar4 = ppuVar4 + 2;
	  puVar5 = *ppuVar4;
	}
      while (puVar5 != (undefined *) 0x0);
      puVar5 = ppuVar4[1];
      if (puVar5 == (undefined *) 0x0)
	{
	  func_0x00401550 ("\n%s online help: <%s>\n", "GNU coreutils",
			   "https://www.gnu.org/software/coreutils/");
	  lVar2 = func_0x00401840 (5, 0);
	  if (lVar2 != 0)
	    {
	      iVar1 = func_0x00401750 (lVar2, &DAT_0040ce89, 3);
	      if (iVar1 != 0)
		{
		  puVar5 = &DAT_0040ce02;
		  goto LAB_00402069;
		}
	    }
	  func_0x00401550 ("Full documentation at: <%s%s>\n",
			   "https://www.gnu.org/software/coreutils/",
			   &DAT_0040ce02);
	LAB_00402092:
	  ;
	  puVar5 = &DAT_0040ce02;
	  uVar3 = 0x40ce21;
	}
      else
	{
	  func_0x00401550 ("\n%s online help: <%s>\n", "GNU coreutils",
			   "https://www.gnu.org/software/coreutils/");
	  lVar2 = func_0x00401840 (5, 0);
	  if (lVar2 != 0)
	    {
	      iVar1 = func_0x00401750 (lVar2, &DAT_0040ce89, 3);
	      if (iVar1 != 0)
		{
		LAB_00402069:
		  ;
		  func_0x00401550
		    ("Report %s translation bugs to <https://translationproject.org/team/>\n",
		     &DAT_0040ce02);
		}
	    }
	  func_0x00401550 ("Full documentation at: <%s%s>\n",
			   "https://www.gnu.org/software/coreutils/",
			   &DAT_0040ce02);
	  uVar3 = 0x40e03f;
	  if (puVar5 == &DAT_0040ce02)
	    goto LAB_00402092;
	}
      func_0x00401550
	("or available locally via: info \'(coreutils) %s%s\'\n", puVar5,
	 uVar3);
    }
  while (true);
}

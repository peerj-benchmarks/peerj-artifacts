
#include "stdbuf.h"

long null_ARRAY_00610c4_0_8_;
long null_ARRAY_00610c4_8_8_;
long null_ARRAY_00610f0_0_8_;
long null_ARRAY_00610f0_16_8_;
long null_ARRAY_00610f0_24_8_;
long null_ARRAY_00610f0_32_8_;
long null_ARRAY_00610f0_40_8_;
long null_ARRAY_00610f0_48_8_;
long null_ARRAY_00610f0_8_8_;
long null_ARRAY_00610f4_0_4_;
long null_ARRAY_00610f4_16_8_;
long null_ARRAY_00610f4_4_4_;
long null_ARRAY_00610f4_8_4_;
long local_2f_1_1_;
long local_5b_1_1_;
long local_9d_0_4_;
long local_a0_0_8_;
long local_a0_4_6_;
long local_a0_8_2_;
long local_a1_4_4_;
long local_a2_4_4_;
long local_a3_0_4_;
long local_a3_4_4_;
long local_a4_0_2_;
long local_a4_0_4_;
long local_a4_0_8_;
long local_a4_1_9_;
long local_a4_4_6_;
long local_a4_8_2_;
long local_a5_0_4_;
long local_a5_4_4_;
long DAT_00000010;
long DAT_0040d840;
long DAT_0040d8e8;
long DAT_0040d922;
long DAT_0040d928;
long DAT_0040d943;
long DAT_0040d948;
long DAT_0040d962;
long DAT_0040e218;
long DAT_0040e21c;
long DAT_0040e220;
long DAT_0040e223;
long DAT_0040e225;
long DAT_0040e229;
long DAT_0040e22d;
long DAT_0040e7ab;
long DAT_0040ed48;
long DAT_0040ee51;
long DAT_0040ee57;
long DAT_0040ee69;
long DAT_0040ee6a;
long DAT_0040ee88;
long DAT_0040ee8c;
long DAT_0040ef0e;
long DAT_006107e0;
long DAT_006107f0;
long DAT_00610800;
long DAT_00610be8;
long DAT_00610c50;
long DAT_00610c54;
long DAT_00610c58;
long DAT_00610c5c;
long DAT_00610c80;
long DAT_00610cc0;
long DAT_00610cd0;
long DAT_00610ce0;
long DAT_00610ce8;
long DAT_00610d00;
long DAT_00610d08;
long DAT_00610dc8;
long DAT_00610dd0;
long DAT_00610dd8;
long DAT_00610de0;
long DAT_00610f78;
long DAT_00610f80;
long DAT_00610f88;
long DAT_00610f90;
long DAT_00610fa0;
long _DYNAMIC;
long fde_0040f938;
long null_ARRAY_0040e0c0;
long null_ARRAY_0040e100;
long null_ARRAY_0040f1a0;
long null_ARRAY_0040f300;
long null_ARRAY_0040f3f0;
long null_ARRAY_00610c00;
long null_ARRAY_00610c40;
long null_ARRAY_00610d20;
long null_ARRAY_00610d80;
long null_ARRAY_00610e00;
long null_ARRAY_00610f00;
long null_ARRAY_00610f40;
long PTR_null_ARRAY_00610c38;
long register0x00000020;
void
FUN_00402270 (uint uParm1)
{
  int iVar1;
  long lVar2;
  undefined8 uVar3;
  undefined **ppuVar4;
  char *pcVar5;
  undefined *puVar6;
  undefined *local_88;
  char *local_80;
  undefined8 local_78;
  char *local_70;
  char *local_68;
  char *local_60;
  char *local_58;
  char *local_50;
  char *local_48;
  char *local_40;
  char *local_38;
  char *local_30;
  undefined8 local_28;
  undefined8 local_20;

  if (uParm1 == 0)
    goto LAB_0040229d;
  func_0x00401830 (DAT_00610ce0, "Try \'%s --help\' for more information.\n",
		   DAT_00610de0);
  do
    {
      func_0x004019f0 ((ulong) uParm1);
    LAB_0040229d:
      ;
      func_0x00401690 ("Usage: %s OPTION... COMMAND\n", DAT_00610de0);
      uVar3 = DAT_00610c80;
      func_0x00401840
	("Run COMMAND, with modified buffering operations for its standard streams.\n",
	 DAT_00610c80);
      func_0x00401840
	("\nMandatory arguments to long options are mandatory for short options too.\n",
	 uVar3);
      func_0x00401840
	("  -i, --input=MODE   adjust standard input stream buffering\n  -o, --output=MODE  adjust standard output stream buffering\n  -e, --error=MODE   adjust standard error stream buffering\n",
	 uVar3);
      func_0x00401840 ("      --help     display this help and exit\n",
		       uVar3);
      func_0x00401840
	("      --version  output version information and exit\n", uVar3);
      func_0x00401840
	("\nIf MODE is \'L\' the corresponding stream will be line buffered.\nThis option is invalid with standard input.\n",
	 uVar3);
      func_0x00401840
	("\nIf MODE is \'0\' the corresponding stream will be unbuffered.\n",
	 uVar3);
      func_0x00401840
	("\nOtherwise MODE is a number which may be followed by one of the following:\nKB 1000, K 1024, MB 1000*1000, M 1024*1024, and so on for G, T, P, E, Z, Y.\nIn this case the corresponding stream will be fully buffered with the buffer\nsize set to MODE bytes.\n",
	 uVar3);
      func_0x00401840
	("\nNOTE: If COMMAND adjusts the buffering of its standard streams (\'tee\' does\nfor example) then that will override corresponding changes by \'stdbuf\'.\nAlso some filters (like \'dd\' and \'cat\' etc.) don\'t use streams for I/O,\nand are thus unaffected by \'stdbuf\' settings.\n",
	 uVar3);
      local_88 = &DAT_0040d840;
      local_80 = "test invocation";
      puVar6 = &DAT_0040d840;
      local_78 = 0x40d8c7;
      local_70 = "Multi-call invocation";
      local_68 = "sha224sum";
      local_60 = "sha2 utilities";
      local_58 = "sha256sum";
      local_50 = "sha2 utilities";
      local_48 = "sha384sum";
      local_40 = "sha2 utilities";
      local_38 = "sha512sum";
      local_30 = "sha2 utilities";
      local_28 = 0;
      local_20 = 0;
      ppuVar4 = &local_88;
      do
	{
	  iVar1 = func_0x00401930 ("stdbuf", puVar6);
	  if (iVar1 == 0)
	    break;
	  ppuVar4 = ppuVar4 + 2;
	  puVar6 = *ppuVar4;
	}
      while (puVar6 != (undefined *) 0x0);
      pcVar5 = ppuVar4[1];
      if (pcVar5 == (char *) 0x0)
	{
	  func_0x00401690 ("\n%s online help: <%s>\n", "GNU coreutils",
			   "https://www.gnu.org/software/coreutils/");
	  lVar2 = func_0x00401990 (5, 0);
	  if (lVar2 != 0)
	    {
	      iVar1 = func_0x00401890 (lVar2, &DAT_0040d8e8, 3);
	      if (iVar1 != 0)
		{
		  pcVar5 = "stdbuf";
		  goto LAB_00402481;
		}
	    }
	  func_0x00401690 ("Full documentation at: <%s%s>\n",
			   "https://www.gnu.org/software/coreutils/",
			   "stdbuf");
	LAB_004024aa:
	  ;
	  pcVar5 = "stdbuf";
	  uVar3 = 0x40d880;
	}
      else
	{
	  func_0x00401690 ("\n%s online help: <%s>\n", "GNU coreutils",
			   "https://www.gnu.org/software/coreutils/");
	  lVar2 = func_0x00401990 (5, 0);
	  if (lVar2 != 0)
	    {
	      iVar1 = func_0x00401890 (lVar2, &DAT_0040d8e8, 3);
	      if (iVar1 != 0)
		{
		LAB_00402481:
		  ;
		  func_0x00401690
		    ("Report %s translation bugs to <https://translationproject.org/team/>\n",
		     "stdbuf");
		}
	    }
	  func_0x00401690 ("Full documentation at: <%s%s>\n",
			   "https://www.gnu.org/software/coreutils/",
			   "stdbuf");
	  uVar3 = 0x40ee87;
	  if (pcVar5 == "stdbuf")
	    goto LAB_004024aa;
	}
      func_0x00401690
	("or available locally via: info \'(coreutils) %s%s\'\n", pcVar5,
	 uVar3);
    }
  while (true);
}

typedef unsigned char   undefined;

typedef unsigned char    byte;
typedef unsigned char    dwfenc;
typedef unsigned int    dword;
typedef unsigned long    qword;
typedef unsigned char    undefined1;
typedef unsigned short    undefined2;
typedef unsigned int    undefined4;
typedef unsigned long    undefined6;
typedef unsigned long    undefined8;
typedef unsigned short    word;
#define unkbyte9   unsigned long long
#define unkbyte10   unsigned long long
#define unkbyte11   unsigned long long
#define unkbyte12   unsigned long long
#define unkbyte13   unsigned long long
#define unkbyte14   unsigned long long
#define unkbyte15   unsigned long long
#define unkbyte16   unsigned long long

#define unkuint9   unsigned long long
#define unkuint10   unsigned long long
#define unkuint11   unsigned long long
#define unkuint12   unsigned long long
#define unkuint13   unsigned long long
#define unkuint14   unsigned long long
#define unkuint15   unsigned long long
#define unkuint16   unsigned long long

#define unkint9   long long
#define unkint10   long long
#define unkint11   long long
#define unkint12   long long
#define unkint13   long long
#define unkint14   long long
#define unkint15   long long
#define unkint16   long long

#define unkfloat1   float
#define unkfloat2   float
#define unkfloat3   float
#define unkfloat5   double
#define unkfloat6   double
#define unkfloat7   double
#define unkfloat9   long double
#define unkfloat11   long double
#define unkfloat12   long double
#define unkfloat13   long double
#define unkfloat14   long double
#define unkfloat15   long double
#define unkfloat16   long double

#define BADSPACEBASE   void
#define code   void

#define ulong long
#define true 1
#define bool int
#define false 0
#define float10 int
#define ushort int
#define undefined7 long
#define undefined2 long
#define uint5 long
#define uint6 long

// Add definition for uint
typedef unsigned int    uint;

#define sbyte char

extern imperfection_wrapper(void);
typedef struct eh_frame_hdr eh_frame_hdr, *Peh_frame_hdr;

struct eh_frame_hdr {
    byte eh_frame_hdr_version; // Exception Handler Frame Header Version
    dwfenc eh_frame_pointer_encoding; // Exception Handler Frame Pointer Encoding
    dwfenc eh_frame_desc_entry_count_encoding; // Encoding of # of Exception Handler FDEs
    dwfenc eh_frame_table_encoding; // Exception Handler Table Encoding
};

typedef struct fde_table_entry fde_table_entry, *Pfde_table_entry;

struct fde_table_entry {
    dword initial_loc; // Initial Location
    dword data_loc; // Data location
};

typedef enum Elf64_DynTag {
    DT_AUDIT=1879047932,
    DT_AUXILIARY=2147483645,
    DT_BIND_NOW=24,
    DT_CHECKSUM=1879047672,
    DT_CONFIG=1879047930,
    DT_DEBUG=21,
    DT_DEPAUDIT=1879047931,
    DT_ENCODING=32,
    DT_FEATURE_1=1879047676,
    DT_FILTER=2147483647,
    DT_FINI=13,
    DT_FINI_ARRAY=26,
    DT_FINI_ARRAYSZ=28,
    DT_FLAGS=30,
    DT_FLAGS_1=1879048187,
    DT_GNU_CONFLICT=1879047928,
    DT_GNU_CONFLICTSZ=1879047670,
    DT_GNU_HASH=1879047925,
    DT_GNU_LIBLIST=1879047929,
    DT_GNU_LIBLISTSZ=1879047671,
    DT_GNU_PRELINKED=1879047669,
    DT_HASH=4,
    DT_INIT=12,
    DT_INIT_ARRAY=25,
    DT_INIT_ARRAYSZ=27,
    DT_JMPREL=23,
    DT_MOVEENT=1879047674,
    DT_MOVESZ=1879047675,
    DT_MOVETAB=1879047934,
    DT_NEEDED=1,
    DT_NULL=0,
    DT_PLTGOT=3,
    DT_PLTPAD=1879047933,
    DT_PLTPADSZ=1879047673,
    DT_PLTREL=20,
    DT_PLTRELSZ=2,
    DT_POSFLAG_1=1879047677,
    DT_PREINIT_ARRAYSZ=33,
    DT_REL=17,
    DT_RELA=7,
    DT_RELACOUNT=1879048185,
    DT_RELAENT=9,
    DT_RELASZ=8,
    DT_RELCOUNT=1879048186,
    DT_RELENT=19,
    DT_RELSZ=18,
    DT_RPATH=15,
    DT_RUNPATH=29,
    DT_SONAME=14,
    DT_STRSZ=10,
    DT_STRTAB=5,
    DT_SYMBOLIC=16,
    DT_SYMENT=11,
    DT_SYMINENT=1879047679,
    DT_SYMINFO=1879047935,
    DT_SYMINSZ=1879047678,
    DT_SYMTAB=6,
    DT_TEXTREL=22,
    DT_TLSDESC_GOT=1879047927,
    DT_TLSDESC_PLT=1879047926,
    DT_VERDEF=1879048188,
    DT_VERDEFNUM=1879048189,
    DT_VERNEED=1879048190,
    DT_VERNEEDNUM=1879048191,
    DT_VERSYM=1879048176
} Elf64_DynTag;

typedef enum Elf_ProgramHeaderType {
    PT_DYNAMIC=2,
    PT_GNU_EH_FRAME=1685382480,
    PT_GNU_RELRO=1685382482,
    PT_GNU_STACK=1685382481,
    PT_INTERP=3,
    PT_LOAD=1,
    PT_NOTE=4,
    PT_NULL=0,
    PT_PHDR=6,
    PT_SHLIB=5,
    PT_TLS=7
} Elf_ProgramHeaderType;

typedef struct Elf64_Dyn Elf64_Dyn, *PElf64_Dyn;

struct Elf64_Dyn {
    enum Elf64_DynTag d_tag;
    qword d_val;
};

typedef struct Elf64_Shdr Elf64_Shdr, *PElf64_Shdr;

typedef enum Elf_SectionHeaderType {
    SHT_CHECKSUM=1879048184,
    SHT_DYNAMIC=6,
    SHT_DYNSYM=11,
    SHT_FINI_ARRAY=15,
    SHT_GNU_ATTRIBUTES=1879048181,
    SHT_GNU_HASH=1879048182,
    SHT_GNU_LIBLIST=1879048183,
    SHT_GNU_verdef=1879048189,
    SHT_GNU_verneed=1879048190,
    SHT_GNU_versym=1879048191,
    SHT_GROUP=17,
    SHT_HASH=5,
    SHT_INIT_ARRAY=14,
    SHT_NOBITS=8,
    SHT_NOTE=7,
    SHT_NULL=0,
    SHT_PREINIT_ARRAY=16,
    SHT_PROGBITS=1,
    SHT_REL=9,
    SHT_RELA=4,
    SHT_SHLIB=10,
    SHT_STRTAB=3,
    SHT_SUNW_COMDAT=1879048187,
    SHT_SUNW_move=1879048186,
    SHT_SUNW_syminfo=1879048188,
    SHT_SYMTAB=2,
    SHT_SYMTAB_SHNDX=18
} Elf_SectionHeaderType;

struct Elf64_Shdr {
    dword sh_name;
    enum Elf_SectionHeaderType sh_type;
    qword sh_flags;
    qword sh_addr;
    qword sh_offset;
    qword sh_size;
    dword sh_link;
    dword sh_info;
    qword sh_addralign;
    qword sh_entsize;
};

typedef struct Elf64_Phdr Elf64_Phdr, *PElf64_Phdr;

struct Elf64_Phdr {
    enum Elf_ProgramHeaderType p_type;
    dword p_flags;
    qword p_offset;
    qword p_vaddr;
    qword p_paddr;
    qword p_filesz;
    qword p_memsz;
    qword p_align;
};

typedef struct Elf64_Sym Elf64_Sym, *PElf64_Sym;

struct Elf64_Sym {
    dword st_name;
    byte st_info;
    byte st_other;
    word st_shndx;
    qword st_value;
    qword st_size;
};

typedef struct Elf64_Rela Elf64_Rela, *PElf64_Rela;

struct Elf64_Rela {
    qword r_offset; // location to apply the relocation action
    qword r_info; // the symbol table index and the type of relocation
    qword r_addend; // a constant addend used to compute the relocatable field value
};

typedef struct Elf64_Ehdr Elf64_Ehdr, *PElf64_Ehdr;

struct Elf64_Ehdr {
    byte e_ident_magic_num;
    char e_ident_magic_str[3];
    byte e_ident_class;
    byte e_ident_data;
    byte e_ident_version;
    byte e_ident_pad[9];
    word e_type;
    word e_machine;
    dword e_version;
    qword e_entry;
    qword e_phoff;
    qword e_shoff;
    dword e_flags;
    word e_ehsize;
    word e_phentsize;
    word e_phnum;
    word e_shentsize;
    word e_shnum;
    word e_shstrndx;
};




undefined8 _DT_INIT(void);
void FUN_00401900(void);
void thunk_FUN_00620040(void);
void FUN_00401aa0(void);
void thunk_FUN_006200d8(void);
void FUN_00401ca0(void);
void thunk_FUN_00620288(void);
void FUN_00401e50(int iParm1,undefined8 uParm2,ulong uParm3);
ulong FUN_00401eb0(ulong uParm1,undefined8 *puParm2);
void entry(void);
void FUN_00402530(undefined8 *puParm1);
void FUN_00402560(void);
void FUN_004025e0(void);
void FUN_00402660(void);
undefined8 FUN_004026a0(char *pcParm1,undefined8 uParm2,ulong uParm3,undefined8 uParm4);
void FUN_00402790(uint uParm1);
void FUN_00402ac0(void);
long FUN_00402ad0(undefined8 uParm1,long *plParm2,long lParm3,long lParm4);
void FUN_00402c00(undefined8 uParm1,undefined8 uParm2,long lParm3);
void FUN_00402c50(long *plParm1,long lParm2,long lParm3);
long FUN_00402d20(undefined8 uParm1,undefined8 uParm2,undefined8 uParm3,undefined8 uParm4,undefined8 uParm5,code *pcParm6);
void FUN_00402d90(void);
void FUN_00402e20(undefined8 uParm1,byte *pbParm2,long lParm3);
char * FUN_00402e60(long param_1,char *param_2,undefined8 *param_3,undefined param_4,ulong param_5,undefined8 param_6,uint param_7);
void FUN_004048e0(void);
void FUN_00404900(undefined8 *puParm1);
char * FUN_00404940(long lParm1,long lParm2);
void FUN_004049e0(long param_1);
long FUN_00404b70(int iParm1,long lParm2,ulong uParm3);
void FUN_00404c70(undefined4 uParm1,undefined4 uParm2,undefined4 uParm3,undefined4 uParm4,undefined4 uParm5,undefined4 uParm6,undefined4 uParm7,undefined4 uParm8,undefined8 param_1,undefined8 param_2,undefined8 param_3,undefined8 param_4,undefined8 param_5,undefined8 param_6);
ulong FUN_00404d30(undefined8 uParm1,uint *puParm2,uint *puParm3,long lParm4);
ulong FUN_00404da0(long param_1,long param_2,long param_3,long param_4,long param_5,long param_6,long param_7,int param_8,int param_9);
undefined8 FUN_00405330(int *piParm1,char cParm2,long lParm3,long lParm4,long lParm5);
long * FUN_00405480(long lParm1,undefined8 uParm2);
undefined8 FUN_00405530(byte bParm1,undefined8 uParm2,undefined8 uParm3);
void FUN_00405570(undefined8 uParm1,long lParm2);
long FUN_00405720(undefined8 uParm1,long lParm2,long lParm3);
undefined * FUN_004057a0(long lParm1,undefined *puParm2);
void FUN_00405870(undefined8 uParm1,long lParm2);
undefined8 FUN_00405b60(byte **ppbParm1);
undefined8 FUN_00407490(long *plParm1,byte *pbParm2,long *plParm3,undefined4 uParm4,long lParm5,byte *pbParm6);
undefined8 FUN_004090a0(int *piParm1,int *piParm2,long lParm3,ulong uParm4);
undefined8 FUN_00409140(long *plParm1,char *pcParm2,ulong uParm3);
void FUN_004093f0(long lParm1);
undefined8 * FUN_00409490(undefined8 *puParm1,int iParm2);
undefined * FUN_00409500(char *pcParm1,int iParm2);
ulong FUN_004095d0(undefined *param_1,ulong param_2,long param_3,ulong param_4,uint param_5,ulong param_6,long param_7,char *param_8,char *param_9);
undefined1 * FUN_0040a4d0(uint uParm1,undefined8 uParm2,undefined8 uParm3,uint *puParm4);
void FUN_0040a680(uint uParm1,undefined8 uParm2,undefined8 uParm3);
void FUN_0040a6b0(undefined8 uParm1,undefined8 uParm2,byte bParm3);
void FUN_0040a750(undefined8 uParm1);
void FUN_0040a770(uint uParm1,undefined8 uParm2,undefined8 uParm3);
void FUN_0040a800(undefined8 uParm1,undefined8 uParm2);
void FUN_0040a820(undefined8 uParm1);
ulong FUN_0040a840(undefined8 *puParm1);
long FUN_0040a8c0(undefined8 uParm1,long lParm2,undefined8 uParm3,undefined8 uParm4,long *plParm5,undefined8 uParm6);
void FUN_0040ab20(void);
void FUN_0040ab90(void);
void FUN_0040ac20(long lParm1);
long FUN_0040ac40(long lParm1,long lParm2);
void FUN_0040ac80(void);
void FUN_0040acb0(undefined8 uParm1);
void FUN_0040acf0(undefined8 uParm1,uint uParm2,undefined8 uParm3,undefined8 uParm4);
void FUN_0040ad60(void);
void FUN_0040ada0(undefined4 uParm1,undefined4 uParm2,undefined4 uParm3,undefined4 uParm4,undefined4 uParm5,undefined4 uParm6,undefined4 uParm7,undefined4 uParm8,ulong param_1,ulong param_2,undefined8 param_3,undefined8 param_4,undefined8 param_5,undefined8 param_6);
void FUN_0040ae80(undefined8 uParm1);
ulong FUN_0040af10(long lParm1);
undefined8 FUN_0040afa0(void);
void FUN_0040afb0(void);
void FUN_0040afc0(undefined8 uParm1,undefined8 uParm2,undefined8 uParm3);
void FUN_0040afd0(long lParm1,int *piParm2);
ulong FUN_0040b0b0(int param_1,undefined8 *param_2,char *param_3,long *param_4,int *param_5,int param_6,int *param_7,int param_8,undefined8 param_9);
ulong FUN_0040b630(uint param_1,undefined8 *param_2,char *param_3,long param_4,undefined8 param_5,ulong param_6,uint *param_7,int param_8);
void FUN_0040bb80(void);
void FUN_0040bbe0(void);
void FUN_0040bc00(void);
void FUN_0040bcc0(void);
void FUN_0040bcf0(long lParm1);
ulong FUN_0040bd10(uint *puParm1,byte *pbParm2,long lParm3);
long FUN_0040bd80(ulong param_1,long param_2,int param_3,int param_4,int param_5,uint param_6,int param_7,int param_8,int param_9,int param_10);
void FUN_0040beb0(code *pcParm1,long *plParm2,undefined8 uParm3);
long FUN_0040bf60(void);
long FUN_0040c010(undefined8 *puParm1,code *pcParm2,long *plParm3);
void FUN_0040c550(undefined8 uParm1);
undefined8 FUN_0040c570(char *pcParm1);
void FUN_0040c6e0(long lParm1,long lParm2);
ulong FUN_0040c720(undefined4 uParm1,undefined4 uParm2,undefined4 uParm3,undefined4 uParm4,undefined4 uParm5,undefined4 uParm6,undefined4 uParm7,undefined4 uParm8,long param_1,ulong param_2,undefined8 param_3,undefined8 param_4,undefined8 param_5,undefined8 param_6);
ulong FUN_0040c840(long lParm1,long lParm2);
ulong FUN_0040c890(long lParm1);
ulong FUN_0040c8e0(undefined8 *puParm1);
undefined8 * FUN_0040c960(long lParm1);
undefined8 FUN_0040ca00(long *plParm1,char *pcParm2);
long * FUN_0040cb80(long lParm1);
void FUN_0040cc40(long *plParm1);
undefined8 FUN_0040cc70(long lParm1,undefined8 uParm2,undefined8 uParm3);
long FUN_0040cd00(long lParm1,uint *puParm2);
void FUN_0040ce30(long lParm1);
void FUN_0040ce50(void);
ulong FUN_0040cf00(char *pcParm1);
undefined4 * FUN_0040cf70(ulong uParm1,uint *puParm2,ulong uParm3,uint *puParm4,long *plParm5);
ulong FUN_0040d0d0(void);
uint * FUN_0040d340(ulong uParm1,long lParm2,ulong uParm3,uint *puParm4,ulong *puParm5);
char * FUN_0040d900(int iParm1,long lParm2,long lParm3,long lParm4,uint uParm5);
void FUN_0040dea0(uint param_1);
ulong FUN_0040e020(void);
void FUN_0040e240(undefined8 uParm1,uint uParm2);
undefined8 *FUN_0040e3b0(undefined8 *puParm1,undefined8 *puParm2,undefined8 *puParm3,undefined8 uParm4);
ulong FUN_00413980(undefined8 uParm1,undefined8 uParm2,undefined8 uParm3);
ulong FUN_00413a50(undefined8 uParm1);
undefined8 FUN_00413ac0(void);
ulong FUN_00413ad0(ulong uParm1);
char * FUN_00413b70(void);
long FUN_00413ec0(long lParm1,long lParm2,long lParm3);
char * FUN_00413f00(char *param_1,long param_2,char *param_3,undefined8 *param_4,byte param_5,undefined8 param_6,undefined8 param_7,uint param_8);
void FUN_004158a0(void);
double FUN_004158c0(int *piParm1);
void FUN_00415900(uint *param_1);
ulong FUN_00415980(long *plParm1,ulong *puParm2,int iParm3,long lParm4);
undefined8 FUN_00415ae0(uint *puParm1,ulong *puParm2);
undefined8 FUN_00415d00(byte *pbParm1,ulong *puParm2,byte **ppbParm3);
ulong FUN_00416950(char *pcParm1,char *pcParm2,ulong uParm3);
ulong FUN_00416a80(void);
ulong FUN_00416ab0(void);
undefined8 * FUN_00416af0(ulong uParm1);
void FUN_00416b70(ulong uParm1);
void FUN_00416c00(void);
undefined8 _DT_FINI(void);
undefined FUN_00620000();
undefined FUN_00620008();
undefined FUN_00620010();
undefined FUN_00620018();
undefined FUN_00620020();
undefined FUN_00620028();
undefined FUN_00620030();
undefined FUN_00620038();
undefined FUN_00620040();
undefined FUN_00620048();
undefined FUN_00620050();
undefined FUN_00620058();
undefined FUN_00620060();
undefined FUN_00620068();
undefined FUN_00620070();
undefined FUN_00620078();
undefined FUN_00620080();
undefined FUN_00620088();
undefined FUN_00620090();
undefined FUN_00620098();
undefined FUN_006200a0();
undefined FUN_006200a8();
undefined FUN_006200b0();
undefined FUN_006200b8();
undefined FUN_006200c0();
undefined FUN_006200c8();
undefined FUN_006200d0();
undefined FUN_006200d8();
undefined FUN_006200e0();
undefined FUN_006200e8();
undefined FUN_006200f0();
undefined FUN_006200f8();
undefined FUN_00620100();
undefined FUN_00620108();
undefined FUN_00620110();
undefined FUN_00620118();
undefined FUN_00620120();
undefined FUN_00620128();
undefined FUN_00620130();
undefined FUN_00620138();
undefined FUN_00620140();
undefined FUN_00620148();
undefined FUN_00620150();
undefined FUN_00620158();
undefined FUN_00620160();
undefined FUN_00620168();
undefined FUN_00620170();
undefined FUN_00620178();
undefined FUN_00620180();
undefined FUN_00620188();
undefined FUN_00620190();
undefined FUN_00620198();
undefined FUN_006201a0();
undefined FUN_006201a8();
undefined FUN_006201b0();
undefined FUN_006201b8();
undefined FUN_006201c0();
undefined FUN_006201c8();
undefined FUN_006201d0();
undefined FUN_006201d8();
undefined FUN_006201e0();
undefined FUN_006201e8();
undefined FUN_006201f0();
undefined FUN_006201f8();
undefined FUN_00620200();
undefined FUN_00620208();
undefined FUN_00620210();
undefined FUN_00620218();
undefined FUN_00620220();
undefined FUN_00620228();
undefined FUN_00620230();
undefined FUN_00620238();
undefined FUN_00620240();
undefined FUN_00620248();
undefined FUN_00620250();
undefined FUN_00620258();
undefined FUN_00620260();
undefined FUN_00620268();
undefined FUN_00620270();
undefined FUN_00620278();
undefined FUN_00620280();
undefined FUN_00620288();


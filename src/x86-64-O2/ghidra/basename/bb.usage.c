
#include "basename.h"

long null_ARRAY_0060f42_0_8_;
long null_ARRAY_0060f42_8_8_;
long null_ARRAY_0060f60_0_8_;
long null_ARRAY_0060f60_16_8_;
long null_ARRAY_0060f60_24_8_;
long null_ARRAY_0060f60_32_8_;
long null_ARRAY_0060f60_40_8_;
long null_ARRAY_0060f60_48_8_;
long null_ARRAY_0060f60_8_8_;
long null_ARRAY_0060f64_0_4_;
long null_ARRAY_0060f64_16_8_;
long null_ARRAY_0060f64_4_4_;
long null_ARRAY_0060f64_8_4_;
long local_2f_1_1_;
long local_5b_1_1_;
long local_9d_0_4_;
long local_a0_0_8_;
long local_a0_4_6_;
long local_a0_8_2_;
long local_a1_4_4_;
long local_a2_4_4_;
long local_a3_0_4_;
long local_a3_4_4_;
long local_a4_0_2_;
long local_a4_0_4_;
long local_a4_0_8_;
long local_a4_1_9_;
long local_a4_4_6_;
long local_a4_8_2_;
long local_a5_0_4_;
long local_a5_4_4_;
long DAT_00000010;
long DAT_0040c680;
long DAT_0040c70d;
long DAT_0040cc98;
long DAT_0040cc9c;
long DAT_0040cca0;
long DAT_0040cca3;
long DAT_0040cca5;
long DAT_0040cca9;
long DAT_0040ccad;
long DAT_0040d22b;
long DAT_0040d5d5;
long DAT_0040d6d9;
long DAT_0040d6df;
long DAT_0040d6f1;
long DAT_0040d6f2;
long DAT_0040d710;
long DAT_0040d714;
long DAT_0040d796;
long DAT_0060f000;
long DAT_0060f010;
long DAT_0060f020;
long DAT_0060f3c8;
long DAT_0060f430;
long DAT_0060f434;
long DAT_0060f438;
long DAT_0060f43c;
long DAT_0060f440;
long DAT_0060f450;
long DAT_0060f460;
long DAT_0060f468;
long DAT_0060f480;
long DAT_0060f488;
long DAT_0060f4d0;
long DAT_0060f4d8;
long DAT_0060f4e0;
long DAT_0060f678;
long DAT_0060f680;
long DAT_0060f688;
long DAT_0060f698;
long _DYNAMIC;
long fde_0040e130;
long null_ARRAY_0040cb80;
long null_ARRAY_0040da20;
long null_ARRAY_0040dc50;
long null_ARRAY_0060f3e0;
long null_ARRAY_0060f420;
long null_ARRAY_0060f4a0;
long null_ARRAY_0060f500;
long null_ARRAY_0060f600;
long null_ARRAY_0060f640;
long PTR_DAT_0060f3c0;
long PTR_null_ARRAY_0060f418;
long register0x00000020;
long stack0x00000008;
void
FUN_00401c50 (uint uParm1)
{
  int iVar1;
  long lVar2;
  undefined8 uVar3;
  undefined **ppuVar4;
  char *pcVar5;
  undefined *puVar6;
  undefined *local_88;
  char *local_80;
  undefined8 local_78;
  char *local_70;
  char *local_68;
  char *local_60;
  char *local_58;
  char *local_50;
  char *local_48;
  char *local_40;
  char *local_38;
  char *local_30;
  undefined8 local_28;
  undefined8 local_20;

  if (uParm1 == 0)
    goto LAB_00401c7d;
  func_0x00401620 (DAT_0060f460, "Try \'%s --help\' for more information.\n",
		   DAT_0060f4e0);
  do
    {
      func_0x004017b0 ((ulong) uParm1);
    LAB_00401c7d:
      ;
      func_0x004014a0
	("Usage: %s NAME [SUFFIX]\n  or:  %s OPTION... NAME...\n",
	 DAT_0060f4e0, DAT_0060f4e0);
      uVar3 = DAT_0060f440;
      func_0x00401630
	("Print NAME with any leading directory components removed.\nIf specified, also remove a trailing SUFFIX.\n",
	 DAT_0060f440);
      func_0x00401630
	("\nMandatory arguments to long options are mandatory for short options too.\n",
	 uVar3);
      func_0x00401630
	("  -a, --multiple       support multiple arguments and treat each as a NAME\n  -s, --suffix=SUFFIX  remove a trailing SUFFIX; implies -a\n  -z, --zero           end each output line with NUL, not newline\n",
	 uVar3);
      func_0x00401630 ("      --help     display this help and exit\n",
		       uVar3);
      func_0x00401630
	("      --version  output version information and exit\n", uVar3);
      func_0x004014a0
	("\nExamples:\n  %s /usr/bin/sort          -> \"sort\"\n  %s include/stdio.h .h     -> \"stdio\"\n  %s -s .h include/stdio.h  -> \"stdio\"\n  %s -a any/str1 any/str2   -> \"str1\" followed by \"str2\"\n",
	 DAT_0060f4e0, DAT_0060f4e0, DAT_0060f4e0, DAT_0060f4e0);
      local_88 = &DAT_0040c680;
      local_80 = "test invocation";
      puVar6 = &DAT_0040c680;
      local_78 = 0x40c6ec;
      local_70 = "Multi-call invocation";
      local_68 = "sha224sum";
      local_60 = "sha2 utilities";
      local_58 = "sha256sum";
      local_50 = "sha2 utilities";
      local_48 = "sha384sum";
      local_40 = "sha2 utilities";
      local_38 = "sha512sum";
      local_30 = "sha2 utilities";
      local_28 = 0;
      local_20 = 0;
      ppuVar4 = &local_88;
      do
	{
	  iVar1 = func_0x00401700 ("basename", puVar6);
	  if (iVar1 == 0)
	    break;
	  ppuVar4 = ppuVar4 + 2;
	  puVar6 = *ppuVar4;
	}
      while (puVar6 != (undefined *) 0x0);
      pcVar5 = ppuVar4[1];
      if (pcVar5 == (char *) 0x0)
	{
	  func_0x004014a0 ("\n%s online help: <%s>\n", "GNU coreutils",
			   "https://www.gnu.org/software/coreutils/");
	  lVar2 = func_0x00401760 (5, 0);
	  if (lVar2 != 0)
	    {
	      iVar1 = func_0x00401680 (lVar2, &DAT_0040c70d, 3);
	      if (iVar1 != 0)
		{
		  pcVar5 = "basename";
		  goto LAB_00401e49;
		}
	    }
	  func_0x004014a0 ("Full documentation at: <%s%s>\n",
			   "https://www.gnu.org/software/coreutils/",
			   "basename");
	LAB_00401e72:
	  ;
	  pcVar5 = "basename";
	  uVar3 = 0x40c6a5;
	}
      else
	{
	  func_0x004014a0 ("\n%s online help: <%s>\n", "GNU coreutils",
			   "https://www.gnu.org/software/coreutils/");
	  lVar2 = func_0x00401760 (5, 0);
	  if (lVar2 != 0)
	    {
	      iVar1 = func_0x00401680 (lVar2, &DAT_0040c70d, 3);
	      if (iVar1 != 0)
		{
		LAB_00401e49:
		  ;
		  func_0x004014a0
		    ("Report %s translation bugs to <https://translationproject.org/team/>\n",
		     "basename");
		}
	    }
	  func_0x004014a0 ("Full documentation at: <%s%s>\n",
			   "https://www.gnu.org/software/coreutils/",
			   "basename");
	  uVar3 = 0x40d70f;
	  if (pcVar5 == "basename")
	    goto LAB_00401e72;
	}
      func_0x004014a0
	("or available locally via: info \'(coreutils) %s%s\'\n", pcVar5,
	 uVar3);
    }
  while (true);
}

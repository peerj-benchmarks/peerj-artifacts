
#include "head.h"

long null_ARRAY_0061180_0_8_;
long null_ARRAY_0061180_8_8_;
long null_ARRAY_00611a0_0_8_;
long null_ARRAY_00611a0_16_8_;
long null_ARRAY_00611a0_24_8_;
long null_ARRAY_00611a0_32_8_;
long null_ARRAY_00611a0_40_8_;
long null_ARRAY_00611a0_48_8_;
long null_ARRAY_00611a0_8_8_;
long null_ARRAY_00611a4_0_4_;
long null_ARRAY_00611a4_16_8_;
long null_ARRAY_00611a4_4_4_;
long null_ARRAY_00611a4_8_4_;
long local_2f_1_1_;
long local_4_4_4_;
long local_5b_1_1_;
long local_9d_0_4_;
long local_a0_0_8_;
long local_a0_4_6_;
long local_a0_8_2_;
long local_a1_4_4_;
long local_a2_4_4_;
long local_a3_0_4_;
long local_a3_4_4_;
long local_a4_0_2_;
long local_a4_0_4_;
long local_a4_0_8_;
long local_a4_1_9_;
long local_a4_4_6_;
long local_a4_8_2_;
long local_a5_0_4_;
long local_a5_4_4_;
long DAT_00000010;
long DAT_0040e444;
long DAT_0040e446;
long DAT_0040e4cd;
long DAT_0040edd8;
long DAT_0040eddc;
long DAT_0040ede0;
long DAT_0040ede3;
long DAT_0040ede5;
long DAT_0040ede9;
long DAT_0040eded;
long DAT_0040f36b;
long DAT_0040f908;
long DAT_0040fa11;
long DAT_0040fa17;
long DAT_0040fa29;
long DAT_0040fa2a;
long DAT_0040fa48;
long DAT_0040fa4c;
long DAT_0040face;
long DAT_006113a8;
long DAT_006113b8;
long DAT_006113c8;
long DAT_006117a0;
long DAT_006117b0;
long DAT_00611810;
long DAT_00611814;
long DAT_00611818;
long DAT_0061181c;
long DAT_00611840;
long DAT_00611850;
long DAT_00611860;
long DAT_00611868;
long DAT_00611880;
long DAT_00611888;
long DAT_006118d0;
long DAT_006118d1;
long DAT_006118d2;
long DAT_006118d3;
long DAT_006118d8;
long DAT_006118e0;
long DAT_006118e8;
long DAT_00611a78;
long DAT_00611a80;
long DAT_00611a88;
long DAT_00611a98;
long _DYNAMIC;
long fde_004104d0;
long null_ARRAY_0040ec10;
long null_ARRAY_0040ec40;
long null_ARRAY_0040fd60;
long null_ARRAY_0040ff90;
long null_ARRAY_006117c0;
long null_ARRAY_00611800;
long null_ARRAY_006118a0;
long null_ARRAY_00611900;
long null_ARRAY_00611a00;
long null_ARRAY_00611a40;
long PTR_DAT_006117a8;
long PTR_null_ARRAY_006117f8;
long register0x00000020;
void
FUN_004030b0 (uint uParm1)
{
  int iVar1;
  long lVar2;
  undefined8 uVar3;
  undefined **ppuVar4;
  undefined *puVar5;
  undefined *local_88;
  char *local_80;
  undefined8 local_78;
  char *local_70;
  char *local_68;
  char *local_60;
  char *local_58;
  char *local_50;
  char *local_48;
  char *local_40;
  char *local_38;
  char *local_30;
  undefined8 local_28;
  undefined8 local_20;

  if (uParm1 == 0)
    goto LAB_004030dd;
  func_0x00401780 (DAT_00611860, "Try \'%s --help\' for more information.\n",
		   DAT_006118e8);
  do
    {
      func_0x00401930 ((ulong) uParm1);
    LAB_004030dd:
      ;
      func_0x004015d0 ("Usage: %s [OPTION]... [FILE]...\n", DAT_006118e8);
      func_0x004015d0
	("Print the first %d lines of each FILE to standard output.\nWith more than one FILE, precede each with a header giving the file name.\n",
	 10);
      uVar3 = DAT_00611840;
      func_0x00401790
	("\nWith no FILE, or when FILE is -, read standard input.\n",
	 DAT_00611840);
      func_0x00401790
	("\nMandatory arguments to long options are mandatory for short options too.\n",
	 uVar3);
      func_0x004015d0
	("  -c, --bytes=[-]NUM       print the first NUM bytes of each file;\n                             with the leading \'-\', print all but the last\n                             NUM bytes of each file\n  -n, --lines=[-]NUM       print the first NUM lines instead of the first %d;\n                             with the leading \'-\', print all but the last\n                             NUM lines of each file\n",
	 10);
      func_0x00401790
	("  -q, --quiet, --silent    never print headers giving file names\n  -v, --verbose            always print headers giving file names\n",
	 uVar3);
      func_0x00401790
	("  -z, --zero-terminated    line delimiter is NUL, not newline\n",
	 uVar3);
      func_0x00401790 ("      --help     display this help and exit\n",
		       uVar3);
      func_0x00401790
	("      --version  output version information and exit\n", uVar3);
      func_0x00401790
	("\nNUM may have a multiplier suffix:\nb 512, kB 1000, K 1024, MB 1000*1000, M 1024*1024,\nGB 1000*1000*1000, G 1024*1024*1024, and so on for T, P, E, Z, Y.\n",
	 uVar3);
      local_88 = &DAT_0040e444;
      local_80 = "test invocation";
      puVar5 = &DAT_0040e444;
      local_78 = 0x40e4ac;
      local_70 = "Multi-call invocation";
      local_68 = "sha224sum";
      local_60 = "sha2 utilities";
      local_58 = "sha256sum";
      local_50 = "sha2 utilities";
      local_48 = "sha384sum";
      local_40 = "sha2 utilities";
      local_38 = "sha512sum";
      local_30 = "sha2 utilities";
      local_28 = 0;
      local_20 = 0;
      ppuVar4 = &local_88;
      do
	{
	  iVar1 = func_0x00401880 (&DAT_0040e446, puVar5);
	  if (iVar1 == 0)
	    break;
	  ppuVar4 = ppuVar4 + 2;
	  puVar5 = *ppuVar4;
	}
      while (puVar5 != (undefined *) 0x0);
      puVar5 = ppuVar4[1];
      if (puVar5 == (undefined *) 0x0)
	{
	  func_0x004015d0 ("\n%s online help: <%s>\n", "GNU coreutils",
			   "https://www.gnu.org/software/coreutils/");
	  lVar2 = func_0x004018e0 (5, 0);
	  if (lVar2 != 0)
	    {
	      iVar1 = func_0x004017f0 (lVar2, &DAT_0040e4cd, 3);
	      if (iVar1 != 0)
		{
		  puVar5 = &DAT_0040e446;
		  goto LAB_004032c9;
		}
	    }
	  func_0x004015d0 ("Full documentation at: <%s%s>\n",
			   "https://www.gnu.org/software/coreutils/",
			   &DAT_0040e446);
	LAB_004032f2:
	  ;
	  puVar5 = &DAT_0040e446;
	  uVar3 = 0x40e465;
	}
      else
	{
	  func_0x004015d0 ("\n%s online help: <%s>\n", "GNU coreutils",
			   "https://www.gnu.org/software/coreutils/");
	  lVar2 = func_0x004018e0 (5, 0);
	  if (lVar2 != 0)
	    {
	      iVar1 = func_0x004017f0 (lVar2, &DAT_0040e4cd, 3);
	      if (iVar1 != 0)
		{
		LAB_004032c9:
		  ;
		  func_0x004015d0
		    ("Report %s translation bugs to <https://translationproject.org/team/>\n",
		     &DAT_0040e446);
		}
	    }
	  func_0x004015d0 ("Full documentation at: <%s%s>\n",
			   "https://www.gnu.org/software/coreutils/",
			   &DAT_0040e446);
	  uVar3 = 0x40fa47;
	  if (puVar5 == &DAT_0040e446)
	    goto LAB_004032f2;
	}
      func_0x004015d0
	("or available locally via: info \'(coreutils) %s%s\'\n", puVar5,
	 uVar3);
    }
  while (true);
}


#include "rmdir.h"

long null_ARRAY_0060fa0_0_8_;
long null_ARRAY_0060fa0_8_8_;
long null_ARRAY_0060fc0_0_8_;
long null_ARRAY_0060fc0_16_8_;
long null_ARRAY_0060fc0_24_8_;
long null_ARRAY_0060fc0_32_8_;
long null_ARRAY_0060fc0_40_8_;
long null_ARRAY_0060fc0_48_8_;
long null_ARRAY_0060fc0_8_8_;
long null_ARRAY_0060fc4_0_4_;
long null_ARRAY_0060fc4_16_8_;
long null_ARRAY_0060fc4_4_4_;
long null_ARRAY_0060fc4_8_4_;
long local_2f_1_1_;
long local_4_4_4_;
long local_5b_1_1_;
long local_9d_0_4_;
long local_a0_0_8_;
long local_a0_4_6_;
long local_a0_8_2_;
long local_a1_4_4_;
long local_a2_4_4_;
long local_a3_0_4_;
long local_a3_4_4_;
long local_a4_0_2_;
long local_a4_0_4_;
long local_a4_0_8_;
long local_a4_1_9_;
long local_a4_4_6_;
long local_a4_8_2_;
long local_a5_0_4_;
long local_a5_4_4_;
long DAT_00000010;
long DAT_0040ce00;
long DAT_0040ce02;
long DAT_0040ce06;
long DAT_0040ce8a;
long DAT_0040ce9e;
long DAT_0040d378;
long DAT_0040d37c;
long DAT_0040d380;
long DAT_0040d383;
long DAT_0040d385;
long DAT_0040d389;
long DAT_0040d38d;
long DAT_0040d92b;
long DAT_0040dcd5;
long DAT_0040dcd7;
long DAT_0040ddd9;
long DAT_0040dddf;
long DAT_0040ddf1;
long DAT_0040ddf2;
long DAT_0040de10;
long DAT_0040de14;
long DAT_0040de96;
long DAT_0060f5b8;
long DAT_0060f5c8;
long DAT_0060f5d8;
long DAT_0060f9a8;
long DAT_0060fa10;
long DAT_0060fa14;
long DAT_0060fa18;
long DAT_0060fa1c;
long DAT_0060fa40;
long DAT_0060fa50;
long DAT_0060fa60;
long DAT_0060fa68;
long DAT_0060fa80;
long DAT_0060fa88;
long DAT_0060fad0;
long DAT_0060fad1;
long DAT_0060fad2;
long DAT_0060fad8;
long DAT_0060fae0;
long DAT_0060fae8;
long DAT_0060fc78;
long DAT_0060fc80;
long DAT_0060fc88;
long DAT_0060fc90;
long DAT_0060fc98;
long DAT_0060fca8;
long fde_0040e860;
long null_ARRAY_0040d240;
long null_ARRAY_0040e120;
long null_ARRAY_0040e350;
long null_ARRAY_0060fa00;
long null_ARRAY_0060faa0;
long null_ARRAY_0060fb00;
long null_ARRAY_0060fc00;
long null_ARRAY_0060fc40;
long PTR_DAT_0060f9a0;
long PTR_null_ARRAY_0060f9f8;
long register0x00000020;
void
FUN_00401f40 (uint uParm1)
{
  int iVar1;
  long lVar2;
  undefined8 uVar3;
  undefined **ppuVar4;
  undefined *puVar5;
  undefined *local_88;
  char *local_80;
  undefined8 local_78;
  char *local_70;
  char *local_68;
  char *local_60;
  char *local_58;
  char *local_50;
  char *local_48;
  char *local_40;
  char *local_38;
  char *local_30;
  undefined8 local_28;
  undefined8 local_20;

  if (uParm1 == 0)
    goto LAB_00401f6d;
  func_0x00401760 (DAT_0060fa60, "Try \'%s --help\' for more information.\n",
		   DAT_0060fae8);
  do
    {
      func_0x004018e0 ((ulong) uParm1);
    LAB_00401f6d:
      ;
      func_0x004015b0 ("Usage: %s [OPTION]... DIRECTORY...\n", DAT_0060fae8);
      uVar3 = DAT_0060fa40;
      func_0x00401770
	("Remove the DIRECTORY(ies), if they are empty.\n\n      --ignore-fail-on-non-empty\n                  ignore each failure that is solely because a directory\n                    is non-empty\n",
	 DAT_0060fa40);
      func_0x00401770
	("  -p, --parents   remove DIRECTORY and its ancestors; e.g., \'rmdir -p a/b/c\' is\n                    similar to \'rmdir a/b/c a/b a\'\n  -v, --verbose   output a diagnostic for every directory processed\n",
	 uVar3);
      func_0x00401770 ("      --help     display this help and exit\n",
		       uVar3);
      func_0x00401770
	("      --version  output version information and exit\n", uVar3);
      local_88 = &DAT_0040ce00;
      local_80 = "test invocation";
      puVar5 = &DAT_0040ce00;
      local_78 = 0x40ce69;
      local_70 = "Multi-call invocation";
      local_68 = "sha224sum";
      local_60 = "sha2 utilities";
      local_58 = "sha256sum";
      local_50 = "sha2 utilities";
      local_48 = "sha384sum";
      local_40 = "sha2 utilities";
      local_38 = "sha512sum";
      local_30 = "sha2 utilities";
      local_28 = 0;
      local_20 = 0;
      ppuVar4 = &local_88;
      do
	{
	  iVar1 = func_0x00401840 (&DAT_0040ce02, puVar5);
	  if (iVar1 == 0)
	    break;
	  ppuVar4 = ppuVar4 + 2;
	  puVar5 = *ppuVar4;
	}
      while (puVar5 != (undefined *) 0x0);
      puVar5 = ppuVar4[1];
      if (puVar5 == (undefined *) 0x0)
	{
	  func_0x004015b0 ("\n%s online help: <%s>\n", "GNU coreutils",
			   "https://www.gnu.org/software/coreutils/");
	  lVar2 = func_0x004018a0 (5, 0);
	  if (lVar2 != 0)
	    {
	      iVar1 = func_0x004017c0 (lVar2, &DAT_0040ce8a, 3);
	      if (iVar1 != 0)
		{
		  puVar5 = &DAT_0040ce02;
		  goto LAB_00402111;
		}
	    }
	  func_0x004015b0 ("Full documentation at: <%s%s>\n",
			   "https://www.gnu.org/software/coreutils/",
			   &DAT_0040ce02);
	LAB_0040213a:
	  ;
	  puVar5 = &DAT_0040ce02;
	  uVar3 = 0x40ce22;
	}
      else
	{
	  func_0x004015b0 ("\n%s online help: <%s>\n", "GNU coreutils",
			   "https://www.gnu.org/software/coreutils/");
	  lVar2 = func_0x004018a0 (5, 0);
	  if (lVar2 != 0)
	    {
	      iVar1 = func_0x004017c0 (lVar2, &DAT_0040ce8a, 3);
	      if (iVar1 != 0)
		{
		LAB_00402111:
		  ;
		  func_0x004015b0
		    ("Report %s translation bugs to <https://translationproject.org/team/>\n",
		     &DAT_0040ce02);
		}
	    }
	  func_0x004015b0 ("Full documentation at: <%s%s>\n",
			   "https://www.gnu.org/software/coreutils/",
			   &DAT_0040ce02);
	  uVar3 = 0x40de0f;
	  if (puVar5 == &DAT_0040ce02)
	    goto LAB_0040213a;
	}
      func_0x004015b0
	("or available locally via: info \'(coreutils) %s%s\'\n", puVar5,
	 uVar3);
    }
  while (true);
}

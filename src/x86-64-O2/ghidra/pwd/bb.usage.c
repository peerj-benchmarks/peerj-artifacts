
#include "pwd.h"

long null_ARRAY_0061148_0_8_;
long null_ARRAY_0061148_8_8_;
long null_ARRAY_0061168_0_8_;
long null_ARRAY_0061168_16_8_;
long null_ARRAY_0061168_24_8_;
long null_ARRAY_0061168_32_8_;
long null_ARRAY_0061168_40_8_;
long null_ARRAY_0061168_48_8_;
long null_ARRAY_0061168_8_8_;
long null_ARRAY_006116c_0_4_;
long null_ARRAY_006116c_16_8_;
long null_ARRAY_006116c_4_4_;
long null_ARRAY_006116c_8_4_;
long local_2f_1_1_;
long local_4_4_4_;
long local_5b_1_1_;
long local_9d_0_4_;
long local_a0_0_8_;
long local_a0_4_6_;
long local_a0_8_2_;
long local_a1_4_4_;
long local_a2_4_4_;
long local_a3_0_4_;
long local_a3_4_4_;
long local_a4_0_2_;
long local_a4_0_4_;
long local_a4_0_8_;
long local_a4_1_9_;
long local_a4_4_6_;
long local_a4_8_2_;
long local_a5_0_4_;
long local_a5_4_4_;
long DAT_00000010;
long DAT_0040e100;
long DAT_0040e104;
long DAT_0040e107;
long DAT_0040e109;
long DAT_0040e11d;
long DAT_0040e11e;
long DAT_0040e164;
long DAT_0040e166;
long DAT_0040e203;
long DAT_0040e217;
long DAT_0040e6b8;
long DAT_0040e6bc;
long DAT_0040e6c0;
long DAT_0040e6c3;
long DAT_0040e6c5;
long DAT_0040e6c9;
long DAT_0040e6cd;
long DAT_0040ec6b;
long DAT_0040f015;
long DAT_0040f119;
long DAT_0040f11f;
long DAT_0040f121;
long DAT_0040f122;
long DAT_0040f140;
long DAT_0040f144;
long DAT_0040f1c6;
long DAT_00611000;
long DAT_00611010;
long DAT_00611020;
long DAT_00611428;
long DAT_00611490;
long DAT_00611494;
long DAT_00611498;
long DAT_0061149c;
long DAT_006114c0;
long DAT_006114d0;
long DAT_006114e0;
long DAT_006114e8;
long DAT_00611500;
long DAT_00611508;
long DAT_00611550;
long DAT_00611558;
long DAT_00611560;
long DAT_006116f8;
long DAT_00611700;
long DAT_00611708;
long DAT_00611710;
long DAT_00611718;
long DAT_00611728;
long fde_0040fbe8;
long null_ARRAY_0040e5c0;
long null_ARRAY_0040f460;
long null_ARRAY_0040f690;
long null_ARRAY_00611440;
long null_ARRAY_00611480;
long null_ARRAY_00611520;
long null_ARRAY_00611580;
long null_ARRAY_00611680;
long null_ARRAY_006116c0;
long PTR_DAT_00611420;
long PTR_null_ARRAY_00611478;
long register0x00000020;
void
FUN_004024b0 (uint uParm1)
{
  int iVar1;
  long lVar2;
  undefined8 uVar3;
  undefined **ppuVar4;
  undefined *puVar5;
  undefined *local_88;
  char *local_80;
  undefined8 local_78;
  char *local_70;
  char *local_68;
  char *local_60;
  char *local_58;
  char *local_50;
  char *local_48;
  char *local_40;
  char *local_38;
  char *local_30;
  undefined8 local_28;
  undefined8 local_20;

  if (uParm1 == 0)
    goto LAB_004024dd;
  func_0x00401960 (DAT_006114e0, "Try \'%s --help\' for more information.\n",
		   DAT_00611560);
  do
    {
      func_0x00401b30 ((ulong) uParm1);
    LAB_004024dd:
      ;
      func_0x00401780 ("Usage: %s [OPTION]...\n", DAT_00611560);
      uVar3 = DAT_006114c0;
      func_0x00401970
	("Print the full filename of the current working directory.\n\n",
	 DAT_006114c0);
      func_0x00401970
	("  -L, --logical   use PWD from environment, even if it contains symlinks\n  -P, --physical  avoid all symlinks\n",
	 uVar3);
      func_0x00401970 ("      --help     display this help and exit\n",
		       uVar3);
      func_0x00401970
	("      --version  output version information and exit\n", uVar3);
      func_0x00401970 ("\nIf no option is specified, -P is assumed.\n",
		       uVar3);
      func_0x00401780
	("\nNOTE: your shell may have its own version of %s, which usually supersedes\nthe version described here.  Please refer to your shell\'s documentation\nfor details about the options it supports.\n",
	 &DAT_0040e166);
      local_88 = &DAT_0040e164;
      local_80 = "test invocation";
      puVar5 = &DAT_0040e164;
      local_78 = 0x40e1e2;
      local_70 = "Multi-call invocation";
      local_68 = "sha224sum";
      local_60 = "sha2 utilities";
      local_58 = "sha256sum";
      local_50 = "sha2 utilities";
      local_48 = "sha384sum";
      local_40 = "sha2 utilities";
      local_38 = "sha512sum";
      local_30 = "sha2 utilities";
      local_28 = 0;
      local_20 = 0;
      ppuVar4 = &local_88;
      do
	{
	  iVar1 = func_0x00401a80 (&DAT_0040e166, puVar5);
	  if (iVar1 == 0)
	    break;
	  ppuVar4 = ppuVar4 + 2;
	  puVar5 = *ppuVar4;
	}
      while (puVar5 != (undefined *) 0x0);
      puVar5 = ppuVar4[1];
      if (puVar5 == (undefined *) 0x0)
	{
	  func_0x00401780 ("\n%s online help: <%s>\n", "GNU coreutils",
			   "https://www.gnu.org/software/coreutils/");
	  lVar2 = func_0x00401ae0 (5, 0);
	  if (lVar2 != 0)
	    {
	      iVar1 = func_0x004019f0 (lVar2, &DAT_0040e203, 3);
	      if (iVar1 != 0)
		{
		  puVar5 = &DAT_0040e166;
		  goto LAB_00402699;
		}
	    }
	  func_0x00401780 ("Full documentation at: <%s%s>\n",
			   "https://www.gnu.org/software/coreutils/",
			   &DAT_0040e166);
	LAB_004026c2:
	  ;
	  puVar5 = &DAT_0040e166;
	  uVar3 = 0x40e19b;
	}
      else
	{
	  func_0x00401780 ("\n%s online help: <%s>\n", "GNU coreutils",
			   "https://www.gnu.org/software/coreutils/");
	  lVar2 = func_0x00401ae0 (5, 0);
	  if (lVar2 != 0)
	    {
	      iVar1 = func_0x004019f0 (lVar2, &DAT_0040e203, 3);
	      if (iVar1 != 0)
		{
		LAB_00402699:
		  ;
		  func_0x00401780
		    ("Report %s translation bugs to <https://translationproject.org/team/>\n",
		     &DAT_0040e166);
		}
	    }
	  func_0x00401780 ("Full documentation at: <%s%s>\n",
			   "https://www.gnu.org/software/coreutils/",
			   &DAT_0040e166);
	  uVar3 = 0x40f13f;
	  if (puVar5 == &DAT_0040e166)
	    goto LAB_004026c2;
	}
      func_0x00401780
	("or available locally via: info \'(coreutils) %s%s\'\n", puVar5,
	 uVar3);
    }
  while (true);
}

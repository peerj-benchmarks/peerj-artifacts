
#include "fmt.h"

long null_ARRAY_0061146_0_8_;
long null_ARRAY_0061146_8_8_;
long null_ARRAY_0061158_32_8_;
long null_ARRAY_0061c70_0_8_;
long null_ARRAY_0061c70_16_8_;
long null_ARRAY_0061c70_24_8_;
long null_ARRAY_0061c70_32_8_;
long null_ARRAY_0061c70_40_8_;
long null_ARRAY_0061c70_48_8_;
long null_ARRAY_0061c70_8_8_;
long null_ARRAY_0061c74_0_4_;
long null_ARRAY_0061c74_16_8_;
long null_ARRAY_0061c74_4_4_;
long null_ARRAY_0061c74_8_4_;
long local_2f_1_1_;
long local_4_4_4_;
long local_5b_1_1_;
long local_9d_0_4_;
long local_a0_0_8_;
long local_a0_4_6_;
long local_a0_8_2_;
long local_a1_4_4_;
long local_a2_4_4_;
long local_a3_0_4_;
long local_a3_4_4_;
long local_a4_0_2_;
long local_a4_0_4_;
long local_a4_0_8_;
long local_a4_1_9_;
long local_a4_4_6_;
long local_a4_8_2_;
long local_a5_0_4_;
long local_a5_4_4_;
long DAT_00000010;
long DAT_0040de06;
long DAT_0040de09;
long DAT_0040de0b;
long DAT_0040de2b;
long DAT_0040de2d;
long DAT_0040deb3;
long DAT_0040e698;
long DAT_0040e69c;
long DAT_0040e6a0;
long DAT_0040e6a3;
long DAT_0040e6a7;
long DAT_0040e6ab;
long DAT_0040ec2b;
long DAT_0040f1c8;
long DAT_0040f2d1;
long DAT_0040f2d7;
long DAT_0040f2e9;
long DAT_0040f2ea;
long DAT_0040f308;
long DAT_0040f30c;
long DAT_0040f38e;
long DAT_00611000;
long DAT_00611010;
long DAT_00611020;
long DAT_00611408;
long DAT_00611470;
long DAT_00611474;
long DAT_00611478;
long DAT_0061147c;
long DAT_00611480;
long DAT_00611488;
long DAT_00611490;
long DAT_006114a0;
long DAT_006114a8;
long DAT_006114c0;
long DAT_006114c8;
long DAT_00611540;
long DAT_00611544;
long DAT_00611548;
long DAT_0061154c;
long DAT_00611550;
long DAT_00611554;
long DAT_00611558;
long DAT_00611560;
long DAT_0061b1c0;
long DAT_0061c588;
long DAT_0061c58c;
long DAT_0061c590;
long DAT_0061c594;
long DAT_0061c598;
long DAT_0061c59c;
long DAT_0061c5a0;
long DAT_0061c5a8;
long DAT_0061c5b0;
long DAT_0061c5b1;
long DAT_0061c5b2;
long DAT_0061c5b3;
long DAT_0061c5b8;
long DAT_0061c5c0;
long DAT_0061c5c8;
long DAT_0061c778;
long DAT_0061c780;
long DAT_0061c788;
long DAT_0061c798;
long fde_0040fd98;
long null_ARRAY_0040e500;
long null_ARRAY_0040f620;
long null_ARRAY_0040f850;
long null_ARRAY_00611420;
long null_ARRAY_00611460;
long null_ARRAY_006114e0;
long null_ARRAY_00611580;
long null_ARRAY_0061b200;
long null_ARRAY_0061c600;
long null_ARRAY_0061c700;
long null_ARRAY_0061c740;
long PTR_DAT_00611400;
long PTR_null_ARRAY_00611458;
long register0x00000020;
void
FUN_00402e20 (uint uParm1)
{
  int iVar1;
  long lVar2;
  undefined8 uVar3;
  undefined **ppuVar4;
  undefined *puVar5;
  undefined *local_88;
  char *local_80;
  undefined8 local_78;
  char *local_70;
  char *local_68;
  char *local_60;
  char *local_58;
  char *local_50;
  char *local_48;
  char *local_40;
  char *local_38;
  char *local_30;
  undefined8 local_28;
  undefined8 local_20;

  if (uParm1 == 0)
    goto LAB_00402e4d;
  func_0x004017f0 (DAT_006114a0, "Try \'%s --help\' for more information.\n",
		   DAT_0061c5c8);
  do
    {
      func_0x004019c0 ((ulong) uParm1);
    LAB_00402e4d:
      ;
      func_0x00401650 ("Usage: %s [-WIDTH] [OPTION]... [FILE]...\n",
		       DAT_0061c5c8);
      uVar3 = DAT_00611480;
      func_0x00401800
	("Reformat each paragraph in the FILE(s), writing to standard output.\nThe option -WIDTH is an abbreviated form of --width=DIGITS.\n",
	 DAT_00611480);
      func_0x00401800
	("\nWith no FILE, or when FILE is -, read standard input.\n", uVar3);
      func_0x00401800
	("\nMandatory arguments to long options are mandatory for short options too.\n",
	 uVar3);
      func_0x00401800
	("  -c, --crown-margin        preserve indentation of first two lines\n  -p, --prefix=STRING       reformat only lines beginning with STRING,\n                              reattaching the prefix to reformatted lines\n  -s, --split-only          split long lines, but do not refill\n",
	 uVar3);
      func_0x00401800
	("  -t, --tagged-paragraph    indentation of first line different from second\n  -u, --uniform-spacing     one space between words, two after sentences\n  -w, --width=WIDTH         maximum line width (default of 75 columns)\n  -g, --goal=WIDTH          goal width (default of 93% of width)\n",
	 uVar3);
      func_0x00401800 ("      --help     display this help and exit\n",
		       uVar3);
      func_0x00401800
	("      --version  output version information and exit\n", uVar3);
      local_88 = &DAT_0040de2b;
      local_80 = "test invocation";
      puVar5 = &DAT_0040de2b;
      local_78 = 0x40de92;
      local_70 = "Multi-call invocation";
      local_68 = "sha224sum";
      local_60 = "sha2 utilities";
      local_58 = "sha256sum";
      local_50 = "sha2 utilities";
      local_48 = "sha384sum";
      local_40 = "sha2 utilities";
      local_38 = "sha512sum";
      local_30 = "sha2 utilities";
      local_28 = 0;
      local_20 = 0;
      ppuVar4 = &local_88;
      do
	{
	  iVar1 = func_0x00401900 (&DAT_0040de2d, puVar5);
	  if (iVar1 == 0)
	    break;
	  ppuVar4 = ppuVar4 + 2;
	  puVar5 = *ppuVar4;
	}
      while (puVar5 != (undefined *) 0x0);
      puVar5 = ppuVar4[1];
      if (puVar5 == (undefined *) 0x0)
	{
	  func_0x00401650 ("\n%s online help: <%s>\n", "GNU coreutils",
			   "https://www.gnu.org/software/coreutils/");
	  lVar2 = func_0x00401960 (5, 0);
	  if (lVar2 != 0)
	    {
	      iVar1 = func_0x00401860 (lVar2, &DAT_0040deb3, 3);
	      if (iVar1 != 0)
		{
		  puVar5 = &DAT_0040de2d;
		  goto LAB_00403019;
		}
	    }
	  func_0x00401650 ("Full documentation at: <%s%s>\n",
			   "https://www.gnu.org/software/coreutils/",
			   &DAT_0040de2d);
	LAB_00403042:
	  ;
	  puVar5 = &DAT_0040de2d;
	  uVar3 = 0x40de4b;
	}
      else
	{
	  func_0x00401650 ("\n%s online help: <%s>\n", "GNU coreutils",
			   "https://www.gnu.org/software/coreutils/");
	  lVar2 = func_0x00401960 (5, 0);
	  if (lVar2 != 0)
	    {
	      iVar1 = func_0x00401860 (lVar2, &DAT_0040deb3, 3);
	      if (iVar1 != 0)
		{
		LAB_00403019:
		  ;
		  func_0x00401650
		    ("Report %s translation bugs to <https://translationproject.org/team/>\n",
		     &DAT_0040de2d);
		}
	    }
	  func_0x00401650 ("Full documentation at: <%s%s>\n",
			   "https://www.gnu.org/software/coreutils/",
			   &DAT_0040de2d);
	  uVar3 = 0x40f307;
	  if (puVar5 == &DAT_0040de2d)
	    goto LAB_00403042;
	}
      func_0x00401650
	("or available locally via: info \'(coreutils) %s%s\'\n", puVar5,
	 uVar3);
    }
  while (true);
}

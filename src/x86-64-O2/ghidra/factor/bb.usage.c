
#include "factor.h"

long null_ARRAY_0061946_0_8_;
long null_ARRAY_0061946_8_8_;
long null_ARRAY_0061951_0_8_;
long null_ARRAY_0061951_8_8_;
long null_ARRAY_0061964_0_8_;
long null_ARRAY_0061964_16_8_;
long null_ARRAY_0061964_24_8_;
long null_ARRAY_0061964_32_8_;
long null_ARRAY_0061964_40_8_;
long null_ARRAY_0061964_48_8_;
long null_ARRAY_0061964_8_8_;
long null_ARRAY_0061968_0_4_;
long null_ARRAY_0061968_16_8_;
long null_ARRAY_0061968_4_4_;
long null_ARRAY_0061968_8_4_;
long local_2f_1_1_;
long local_5b_1_1_;
long local_9d_0_4_;
long local_a0_0_8_;
long local_a0_4_6_;
long local_a0_8_2_;
long local_a1_4_4_;
long local_a2_4_4_;
long local_a3_0_4_;
long local_a3_4_4_;
long local_a4_0_2_;
long local_a4_0_4_;
long local_a4_0_8_;
long local_a4_1_9_;
long local_a4_4_6_;
long local_a4_8_2_;
long local_a5_0_4_;
long local_a5_4_4_;
long local_c_0_1_;
long DAT_00000010;
long DAT_00412d2b;
long DAT_00412db6;
long DAT_00412dba;
long DAT_00412dd5;
long DAT_00412e05;
long DAT_00413140;
long DAT_004162d0;
long DAT_00416343;
long DAT_00416347;
long DAT_0041634a;
long DAT_0041634c;
long DAT_00416350;
long DAT_00416354;
long DAT_00416915;
long DAT_00416cb5;
long DAT_00416dd1;
long DAT_00416dd2;
long DAT_00416df0;
long DAT_00416df4;
long DAT_00416eb9;
long DAT_00619000;
long DAT_00619010;
long DAT_00619020;
long DAT_00619400;
long DAT_00619410;
long DAT_00619470;
long DAT_00619474;
long DAT_00619478;
long DAT_0061947c;
long DAT_00619480;
long DAT_00619488;
long DAT_00619490;
long DAT_006194a0;
long DAT_006194a8;
long DAT_006194c0;
long DAT_006194c8;
long DAT_00619520;
long DAT_00619528;
long DAT_00619530;
long DAT_00619538;
long DAT_006196b8;
long DAT_00619ec8;
long DAT_00619ed0;
long DAT_00619ee0;
long fde_004179a8;
long null_ARRAY_00413145;
long null_ARRAY_00413184;
long null_ARRAY_004131c0;
long null_ARRAY_00413240;
long null_ARRAY_00415c80;
long null_ARRAY_00415f40;
long null_ARRAY_00416200;
long null_ARRAY_00416e80;
long null_ARRAY_00417160;
long null_ARRAY_004173a0;
long null_ARRAY_00619420;
long null_ARRAY_00619460;
long null_ARRAY_006194e0;
long null_ARRAY_00619510;
long null_ARRAY_00619540;
long null_ARRAY_00619640;
long null_ARRAY_00619680;
long null_ARRAY_006196c0;
long PTR_DAT_00619408;
long PTR_null_ARRAY_00619458;
long register0x00000020;
long stack0x00000008;
void
FUN_00404600 (uint uParm1)
{
  int iVar1;
  long lVar2;
  undefined8 uVar3;
  undefined **ppuVar4;
  char *pcVar5;
  undefined *puVar6;
  undefined *local_88;
  char *local_80;
  undefined8 local_78;
  char *local_70;
  char *local_68;
  char *local_60;
  char *local_58;
  char *local_50;
  char *local_48;
  char *local_40;
  char *local_38;
  char *local_30;
  undefined8 local_28;
  undefined8 local_20;

  if (uParm1 == 0)
    goto LAB_0040462d;
  func_0x00401870 (DAT_006194a0, "Try \'%s --help\' for more information.\n",
		   DAT_00619538);
  do
    {
      func_0x00401a30 ((ulong) uParm1);
    LAB_0040462d:
      ;
      func_0x004016b0 ("Usage: %s [NUMBER]...\n  or:  %s OPTION\n",
		       DAT_00619538, DAT_00619538);
      uVar3 = DAT_00619480;
      func_0x00401880
	("Print the prime factors of each specified integer NUMBER.  If none\nare specified on the command line, read them from standard input.\n\n",
	 DAT_00619480);
      func_0x00401880 ("      --help     display this help and exit\n",
		       uVar3);
      func_0x00401880
	("      --version  output version information and exit\n", uVar3);
      local_88 = &DAT_00412d2b;
      local_80 = "test invocation";
      puVar6 = &DAT_00412d2b;
      local_78 = 0x412d95;
      local_70 = "Multi-call invocation";
      local_68 = "sha224sum";
      local_60 = "sha2 utilities";
      local_58 = "sha256sum";
      local_50 = "sha2 utilities";
      local_48 = "sha384sum";
      local_40 = "sha2 utilities";
      local_38 = "sha512sum";
      local_30 = "sha2 utilities";
      local_28 = 0;
      local_20 = 0;
      ppuVar4 = &local_88;
      do
	{
	  iVar1 = func_0x00401980 ("factor", puVar6);
	  if (iVar1 == 0)
	    break;
	  ppuVar4 = ppuVar4 + 2;
	  puVar6 = *ppuVar4;
	}
      while (puVar6 != (undefined *) 0x0);
      pcVar5 = ppuVar4[1];
      if (pcVar5 == (char *) 0x0)
	{
	  func_0x004016b0 ("\n%s online help: <%s>\n", "GNU coreutils",
			   "https://www.gnu.org/software/coreutils/");
	  lVar2 = func_0x004019e0 (5, 0);
	  if (lVar2 != 0)
	    {
	      iVar1 = func_0x004018d0 (lVar2, &DAT_00412db6, 3);
	      if (iVar1 != 0)
		{
		  pcVar5 = "factor";
		  goto LAB_004047c9;
		}
	    }
	  func_0x004016b0 ("Full documentation at: <%s%s>\n",
			   "https://www.gnu.org/software/coreutils/",
			   "factor");
	LAB_004047f2:
	  ;
	  pcVar5 = "factor";
	  uVar3 = 0x412d4e;
	}
      else
	{
	  func_0x004016b0 ("\n%s online help: <%s>\n", "GNU coreutils",
			   "https://www.gnu.org/software/coreutils/");
	  lVar2 = func_0x004019e0 (5, 0);
	  if (lVar2 != 0)
	    {
	      iVar1 = func_0x004018d0 (lVar2, &DAT_00412db6, 3);
	      if (iVar1 != 0)
		{
		LAB_004047c9:
		  ;
		  func_0x004016b0
		    ("Report %s translation bugs to <https://translationproject.org/team/>\n",
		     "factor");
		}
	    }
	  func_0x004016b0 ("Full documentation at: <%s%s>\n",
			   "https://www.gnu.org/software/coreutils/",
			   "factor");
	  uVar3 = 0x416def;
	  if (pcVar5 == "factor")
	    goto LAB_004047f2;
	}
      func_0x004016b0
	("or available locally via: info \'(coreutils) %s%s\'\n", pcVar5,
	 uVar3);
    }
  while (true);
}

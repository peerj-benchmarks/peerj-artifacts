
#include "id.h"

long null_ARRAY_0061148_0_8_;
long null_ARRAY_0061148_8_8_;
long null_ARRAY_0061170_0_8_;
long null_ARRAY_0061170_16_8_;
long null_ARRAY_0061170_24_8_;
long null_ARRAY_0061170_32_8_;
long null_ARRAY_0061170_40_8_;
long null_ARRAY_0061170_48_8_;
long null_ARRAY_0061170_8_8_;
long null_ARRAY_0061174_0_4_;
long null_ARRAY_0061174_16_8_;
long null_ARRAY_0061174_4_4_;
long null_ARRAY_0061174_8_4_;
long local_2f_1_1_;
long local_5b_1_1_;
long local_9d_0_4_;
long local_a0_0_8_;
long local_a0_4_6_;
long local_a0_8_2_;
long local_a1_4_4_;
long local_a2_4_4_;
long local_a3_0_4_;
long local_a3_4_4_;
long local_a4_0_2_;
long local_a4_0_4_;
long local_a4_0_8_;
long local_a4_1_9_;
long local_a4_4_6_;
long local_a4_8_2_;
long local_a5_0_4_;
long local_a5_4_4_;
long DAT_00000010;
long DAT_0040dc00;
long DAT_0040dc02;
long DAT_0040dca5;
long DAT_0040dd5d;
long DAT_0040e500;
long DAT_0040e504;
long DAT_0040e508;
long DAT_0040e50b;
long DAT_0040e50d;
long DAT_0040e511;
long DAT_0040e515;
long DAT_0040ead3;
long DAT_0040f066;
long DAT_0040f169;
long DAT_0040f16f;
long DAT_0040f171;
long DAT_0040f172;
long DAT_0040f190;
long DAT_0040f194;
long DAT_0040f216;
long DAT_00611000;
long DAT_00611010;
long DAT_00611020;
long DAT_00611420;
long DAT_00611430;
long DAT_00611490;
long DAT_00611494;
long DAT_00611498;
long DAT_0061149c;
long DAT_006114c0;
long DAT_006114d0;
long DAT_006114e0;
long DAT_006114e8;
long DAT_00611500;
long DAT_00611508;
long DAT_00611588;
long DAT_0061158c;
long DAT_00611590;
long DAT_00611594;
long DAT_00611598;
long DAT_006115b8;
long DAT_006115c0;
long DAT_006115c8;
long DAT_00611778;
long DAT_00611780;
long DAT_00611788;
long DAT_00611798;
long _DYNAMIC;
long fde_0040fbd8;
long null_ARRAY_0040e340;
long null_ARRAY_0040f4a0;
long null_ARRAY_0040f6d0;
long null_ARRAY_00611440;
long null_ARRAY_00611480;
long null_ARRAY_00611520;
long null_ARRAY_00611550;
long null_ARRAY_00611570;
long null_ARRAY_006115a0;
long null_ARRAY_00611600;
long null_ARRAY_00611700;
long null_ARRAY_00611740;
long PTR_DAT_00611428;
long PTR_null_ARRAY_00611478;
long register0x00000020;
void
FUN_004025d0 (uint uParm1)
{
  int iVar1;
  long lVar2;
  undefined8 uVar3;
  undefined **ppuVar4;
  undefined *puVar5;
  undefined *local_88;
  char *local_80;
  undefined8 local_78;
  char *local_70;
  char *local_68;
  char *local_60;
  char *local_58;
  char *local_50;
  char *local_48;
  char *local_40;
  char *local_38;
  char *local_30;
  undefined8 local_28;
  undefined8 local_20;

  if (uParm1 == 0)
    goto LAB_004025fd;
  func_0x00401960 (DAT_006114e0, "Try \'%s --help\' for more information.\n",
		   DAT_006115c8);
  do
    {
      func_0x00401b40 ((ulong) uParm1);
    LAB_004025fd:
      ;
      func_0x00401790 ("Usage: %s [OPTION]... [USER]\n", DAT_006115c8);
      uVar3 = DAT_006114c0;
      func_0x00401970
	("Print user and group information for the specified USER,\nor (when USER omitted) for the current user.\n\n",
	 DAT_006114c0);
      func_0x00401970
	("  -a             ignore, for compatibility with other versions\n  -Z, --context  print only the security context of the process\n  -g, --group    print only the effective group ID\n  -G, --groups   print all group IDs\n  -n, --name     print a name instead of a number, for -ugG\n  -r, --real     print the real ID instead of the effective ID, with -ugG\n  -u, --user     print only the effective user ID\n  -z, --zero     delimit entries with NUL characters, not whitespace;\n                   not permitted in default format\n",
	 uVar3);
      func_0x00401970 ("      --help     display this help and exit\n",
		       uVar3);
      func_0x00401970
	("      --version  output version information and exit\n", uVar3);
      func_0x00401970
	("\nWithout any OPTION, print some useful set of identified information.\n",
	 uVar3);
      local_88 = &DAT_0040dc00;
      local_80 = "test invocation";
      puVar5 = &DAT_0040dc00;
      local_78 = 0x40dc84;
      local_70 = "Multi-call invocation";
      local_68 = "sha224sum";
      local_60 = "sha2 utilities";
      local_58 = "sha256sum";
      local_50 = "sha2 utilities";
      local_48 = "sha384sum";
      local_40 = "sha2 utilities";
      local_38 = "sha512sum";
      local_30 = "sha2 utilities";
      local_28 = 0;
      local_20 = 0;
      ppuVar4 = &local_88;
      do
	{
	  iVar1 = func_0x00401a70 (&DAT_0040dc02, puVar5);
	  if (iVar1 == 0)
	    break;
	  ppuVar4 = ppuVar4 + 2;
	  puVar5 = *ppuVar4;
	}
      while (puVar5 != (undefined *) 0x0);
      puVar5 = ppuVar4[1];
      if (puVar5 == (undefined *) 0x0)
	{
	  func_0x00401790 ("\n%s online help: <%s>\n", "GNU coreutils",
			   "https://www.gnu.org/software/coreutils/");
	  lVar2 = func_0x00401af0 (5, 0);
	  if (lVar2 != 0)
	    {
	      iVar1 = func_0x004019d0 (lVar2, &DAT_0040dca5, 3);
	      if (iVar1 != 0)
		{
		  puVar5 = &DAT_0040dc02;
		  goto LAB_004027a9;
		}
	    }
	  func_0x00401790 ("Full documentation at: <%s%s>\n",
			   "https://www.gnu.org/software/coreutils/",
			   &DAT_0040dc02);
	LAB_004027d2:
	  ;
	  puVar5 = &DAT_0040dc02;
	  uVar3 = 0x40dc3d;
	}
      else
	{
	  func_0x00401790 ("\n%s online help: <%s>\n", "GNU coreutils",
			   "https://www.gnu.org/software/coreutils/");
	  lVar2 = func_0x00401af0 (5, 0);
	  if (lVar2 != 0)
	    {
	      iVar1 = func_0x004019d0 (lVar2, &DAT_0040dca5, 3);
	      if (iVar1 != 0)
		{
		LAB_004027a9:
		  ;
		  func_0x00401790
		    ("Report %s translation bugs to <https://translationproject.org/team/>\n",
		     &DAT_0040dc02);
		}
	    }
	  func_0x00401790 ("Full documentation at: <%s%s>\n",
			   "https://www.gnu.org/software/coreutils/",
			   &DAT_0040dc02);
	  uVar3 = 0x40f18f;
	  if (puVar5 == &DAT_0040dc02)
	    goto LAB_004027d2;
	}
      func_0x00401790
	("or available locally via: info \'(coreutils) %s%s\'\n", puVar5,
	 uVar3);
    }
  while (true);
}

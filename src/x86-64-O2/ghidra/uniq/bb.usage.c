
#include "uniq.h"

long null_ARRAY_00611ca_0_8_;
long null_ARRAY_00611ca_8_8_;
long null_ARRAY_00611ec_0_8_;
long null_ARRAY_00611ec_16_8_;
long null_ARRAY_00611ec_24_8_;
long null_ARRAY_00611ec_32_8_;
long null_ARRAY_00611ec_40_8_;
long null_ARRAY_00611ec_48_8_;
long null_ARRAY_00611ec_8_8_;
long null_ARRAY_00611f0_0_4_;
long null_ARRAY_00611f0_16_8_;
long null_ARRAY_00611f0_4_4_;
long null_ARRAY_00611f0_8_4_;
long local_2f_1_1_;
long local_4_4_4_;
long local_5b_1_1_;
long local_9d_0_4_;
long local_a0_0_8_;
long local_a0_4_6_;
long local_a0_8_2_;
long local_a1_4_4_;
long local_a2_4_4_;
long local_a3_0_4_;
long local_a3_4_4_;
long local_a4_0_2_;
long local_a4_0_4_;
long local_a4_0_8_;
long local_a4_1_9_;
long local_a4_4_6_;
long local_a4_8_2_;
long local_a5_0_4_;
long local_a5_4_4_;
long DAT_00000010;
long DAT_0040e1cd;
long DAT_0040e1cf;
long DAT_0040e256;
long DAT_0040e2ce;
long DAT_0040ee7a;
long DAT_0040ee7c;
long DAT_0040ee95;
long DAT_0040eef0;
long DAT_0040eef4;
long DAT_0040eef8;
long DAT_0040eefb;
long DAT_0040eefd;
long DAT_0040ef01;
long DAT_0040ef05;
long DAT_0040f4ab;
long DAT_0040fabe;
long DAT_0040fbc1;
long DAT_0040fbc7;
long DAT_0040fbc9;
long DAT_0040fbca;
long DAT_0040fbe8;
long DAT_0040fc66;
long DAT_00611830;
long DAT_00611840;
long DAT_00611850;
long DAT_00611c50;
long DAT_00611cb0;
long DAT_00611cb4;
long DAT_00611cb8;
long DAT_00611cbc;
long DAT_00611cc0;
long DAT_00611cc8;
long DAT_00611cd0;
long DAT_00611ce0;
long DAT_00611ce8;
long DAT_00611d00;
long DAT_00611d08;
long DAT_00611d50;
long DAT_00611d54;
long DAT_00611d58;
long DAT_00611d59;
long DAT_00611d5a;
long DAT_00611d5b;
long DAT_00611d5c;
long DAT_00611d60;
long DAT_00611d68;
long DAT_00611d70;
long DAT_00611d78;
long DAT_00611d80;
long DAT_00611d88;
long DAT_00611d90;
long DAT_00611f38;
long DAT_00611f40;
long DAT_00611f48;
long DAT_00611f58;
long fde_004106c0;
long null_ARRAY_0040ec00;
long null_ARRAY_0040eda0;
long null_ARRAY_0040edc0;
long null_ARRAY_0040ede8;
long null_ARRAY_0040ee00;
long null_ARRAY_0040ff00;
long null_ARRAY_00410130;
long null_ARRAY_00611c60;
long null_ARRAY_00611ca0;
long null_ARRAY_00611d20;
long null_ARRAY_00611dc0;
long null_ARRAY_00611ec0;
long null_ARRAY_00611f00;
long PTR_DAT_00611c40;
long PTR_FUN_00611c48;
long PTR_null_ARRAY_00611c98;
long register0x00000020;
void
FUN_00402930 (uint uParm1)
{
  int iVar1;
  long lVar2;
  undefined8 uVar3;
  undefined **ppuVar4;
  undefined *puVar5;
  undefined *local_88;
  char *local_80;
  undefined8 local_78;
  char *local_70;
  char *local_68;
  char *local_60;
  char *local_58;
  char *local_50;
  char *local_48;
  char *local_40;
  char *local_38;
  char *local_30;
  undefined8 local_28;
  undefined8 local_20;

  if (uParm1 == 0)
    goto LAB_0040295d;
  func_0x004018f0 (DAT_00611ce0, "Try \'%s --help\' for more information.\n",
		   DAT_00611d90);
  do
    {
      func_0x00401ae0 ((ulong) uParm1);
    LAB_0040295d:
      ;
      func_0x00401740 ("Usage: %s [OPTION]... [INPUT [OUTPUT]]\n",
		       DAT_00611d90);
      uVar3 = DAT_00611cc0;
      func_0x00401900
	("Filter adjacent matching lines from INPUT (or standard input),\nwriting to OUTPUT (or standard output).\n\nWith no options, matching lines are merged to the first occurrence.\n",
	 DAT_00611cc0);
      func_0x00401900
	("\nMandatory arguments to long options are mandatory for short options too.\n",
	 uVar3);
      func_0x00401900
	("  -c, --count           prefix lines by the number of occurrences\n  -d, --repeated        only print duplicate lines, one for each group\n",
	 uVar3);
      func_0x00401900
	("  -D                    print all duplicate lines\n      --all-repeated[=METHOD]  like -D, but allow separating groups\n                                 with an empty line;\n                                 METHOD={none(default),prepend,separate}\n",
	 uVar3);
      func_0x00401900
	("  -f, --skip-fields=N   avoid comparing the first N fields\n",
	 uVar3);
      func_0x00401900
	("      --group[=METHOD]  show all items, separating groups with an empty line;\n                          METHOD={separate(default),prepend,append,both}\n",
	 uVar3);
      func_0x00401900
	("  -i, --ignore-case     ignore differences in case when comparing\n  -s, --skip-chars=N    avoid comparing the first N characters\n  -u, --unique          only print unique lines\n",
	 uVar3);
      func_0x00401900
	("  -z, --zero-terminated     line delimiter is NUL, not newline\n",
	 uVar3);
      func_0x00401900
	("  -w, --check-chars=N   compare no more than N characters in lines\n",
	 uVar3);
      func_0x00401900 ("      --help     display this help and exit\n",
		       uVar3);
      func_0x00401900
	("      --version  output version information and exit\n", uVar3);
      func_0x00401900
	("\nA field is a run of blanks (usually spaces and/or TABs), then non-blank\ncharacters.  Fields are skipped before chars.\n",
	 uVar3);
      func_0x00401900
	("\nNote: \'uniq\' does not detect repeated lines unless they are adjacent.\nYou may want to sort the input first, or use \'sort -u\' without \'uniq\'.\nAlso, comparisons honor the rules specified by \'LC_COLLATE\'.\n",
	 uVar3);
      local_88 = &DAT_0040e1cd;
      local_80 = "test invocation";
      puVar5 = &DAT_0040e1cd;
      local_78 = 0x40e235;
      local_70 = "Multi-call invocation";
      local_68 = "sha224sum";
      local_60 = "sha2 utilities";
      local_58 = "sha256sum";
      local_50 = "sha2 utilities";
      local_48 = "sha384sum";
      local_40 = "sha2 utilities";
      local_38 = "sha512sum";
      local_30 = "sha2 utilities";
      local_28 = 0;
      local_20 = 0;
      ppuVar4 = &local_88;
      do
	{
	  iVar1 = func_0x00401a00 (&DAT_0040e1cf, puVar5);
	  if (iVar1 == 0)
	    break;
	  ppuVar4 = ppuVar4 + 2;
	  puVar5 = *ppuVar4;
	}
      while (puVar5 != (undefined *) 0x0);
      puVar5 = ppuVar4[1];
      if (puVar5 == (undefined *) 0x0)
	{
	  func_0x00401740 ("\n%s online help: <%s>\n", "GNU coreutils",
			   "https://www.gnu.org/software/coreutils/");
	  lVar2 = func_0x00401a60 (5, 0);
	  if (lVar2 != 0)
	    {
	      iVar1 = func_0x00401970 (lVar2, &DAT_0040e256, 3);
	      if (iVar1 != 0)
		{
		  puVar5 = &DAT_0040e1cf;
		  goto LAB_00402b79;
		}
	    }
	  func_0x00401740 ("Full documentation at: <%s%s>\n",
			   "https://www.gnu.org/software/coreutils/",
			   &DAT_0040e1cf);
	LAB_00402ba2:
	  ;
	  puVar5 = &DAT_0040e1cf;
	  uVar3 = 0x40e1ee;
	}
      else
	{
	  func_0x00401740 ("\n%s online help: <%s>\n", "GNU coreutils",
			   "https://www.gnu.org/software/coreutils/");
	  lVar2 = func_0x00401a60 (5, 0);
	  if (lVar2 != 0)
	    {
	      iVar1 = func_0x00401970 (lVar2, &DAT_0040e256, 3);
	      if (iVar1 != 0)
		{
		LAB_00402b79:
		  ;
		  func_0x00401740
		    ("Report %s translation bugs to <https://translationproject.org/team/>\n",
		     &DAT_0040e1cf);
		}
	    }
	  func_0x00401740 ("Full documentation at: <%s%s>\n",
			   "https://www.gnu.org/software/coreutils/",
			   &DAT_0040e1cf);
	  uVar3 = 0x40fbe7;
	  if (puVar5 == &DAT_0040e1cf)
	    goto LAB_00402ba2;
	}
      func_0x00401740
	("or available locally via: info \'(coreutils) %s%s\'\n", puVar5,
	 uVar3);
    }
  while (true);
}

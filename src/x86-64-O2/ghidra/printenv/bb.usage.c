
#include "printenv.h"

long null_ARRAY_0060ed0_0_8_;
long null_ARRAY_0060ed0_8_8_;
long null_ARRAY_0060ef4_0_8_;
long null_ARRAY_0060ef4_16_8_;
long null_ARRAY_0060ef4_24_8_;
long null_ARRAY_0060ef4_32_8_;
long null_ARRAY_0060ef4_40_8_;
long null_ARRAY_0060ef4_48_8_;
long null_ARRAY_0060ef4_8_8_;
long null_ARRAY_0060ef8_0_4_;
long null_ARRAY_0060ef8_16_8_;
long null_ARRAY_0060ef8_4_4_;
long null_ARRAY_0060ef8_8_4_;
long bVar1;
long local_2f_1_1_;
long local_5b_1_1_;
long local_9d_0_4_;
long local_a0_0_8_;
long local_a0_4_6_;
long local_a0_8_2_;
long local_a1_4_4_;
long local_a2_4_4_;
long local_a3_0_4_;
long local_a3_4_4_;
long local_a4_0_2_;
long local_a4_0_4_;
long local_a4_0_8_;
long local_a4_1_9_;
long local_a4_4_6_;
long local_a4_8_2_;
long local_a5_0_4_;
long local_a5_4_4_;
long DAT_00000010;
long DAT_0040c480;
long DAT_0040c50d;
long DAT_0040c538;
long DAT_0040c958;
long DAT_0040c95c;
long DAT_0040c960;
long DAT_0040c963;
long DAT_0040c965;
long DAT_0040c969;
long DAT_0040c96d;
long DAT_0040ceeb;
long DAT_0040d295;
long DAT_0040d399;
long DAT_0040d39f;
long DAT_0040d3b1;
long DAT_0040d3b2;
long DAT_0040d3d0;
long DAT_0040d3d4;
long DAT_0040d456;
long DAT_0060e8e8;
long DAT_0060e8f8;
long DAT_0060e908;
long DAT_0060eca8;
long DAT_0060ed10;
long DAT_0060ed14;
long DAT_0060ed18;
long DAT_0060ed1c;
long DAT_0060ed40;
long DAT_0060ed80;
long DAT_0060ed90;
long DAT_0060eda0;
long DAT_0060eda8;
long DAT_0060edc0;
long DAT_0060edc8;
long DAT_0060ee10;
long DAT_0060ee18;
long DAT_0060ee20;
long DAT_0060efb8;
long DAT_0060efc0;
long DAT_0060efc8;
long DAT_0060efd8;
long fde_0040ddb8;
long null_ARRAY_0040c880;
long null_ARRAY_0040d6e0;
long null_ARRAY_0040d910;
long null_ARRAY_0060ed00;
long null_ARRAY_0060ede0;
long null_ARRAY_0060ee40;
long null_ARRAY_0060ef40;
long null_ARRAY_0060ef80;
long PTR_DAT_0060eca0;
long PTR_null_ARRAY_0060ecf8;
long register0x00000020;
void
FUN_00401bd0 (uint uParm1)
{
  int iVar1;
  long lVar2;
  undefined8 uVar3;
  undefined **ppuVar4;
  char *pcVar5;
  undefined *puVar6;
  undefined *local_88;
  char *local_80;
  undefined8 local_78;
  char *local_70;
  char *local_68;
  char *local_60;
  char *local_58;
  char *local_50;
  char *local_48;
  char *local_40;
  char *local_38;
  char *local_30;
  undefined8 local_28;
  undefined8 local_20;

  if (uParm1 == 0)
    goto LAB_00401bfd;
  func_0x004015e0 (DAT_0060eda0, "Try \'%s --help\' for more information.\n",
		   DAT_0060ee20);
  do
    {
      func_0x00401760 ((ulong) uParm1);
    LAB_00401bfd:
      ;
      func_0x00401470
	("Usage: %s [OPTION]... [VARIABLE]...\nPrint the values of the specified environment VARIABLE(s).\nIf no VARIABLE is specified, print name and value pairs for them all.\n\n",
	 DAT_0060ee20);
      uVar3 = DAT_0060ed40;
      func_0x004015f0
	("  -0, --null     end each output line with NUL, not newline\n",
	 DAT_0060ed40);
      func_0x004015f0 ("      --help     display this help and exit\n",
		       uVar3);
      func_0x004015f0
	("      --version  output version information and exit\n", uVar3);
      func_0x00401470
	("\nNOTE: your shell may have its own version of %s, which usually supersedes\nthe version described here.  Please refer to your shell\'s documentation\nfor details about the options it supports.\n",
	 "printenv");
      local_88 = &DAT_0040c480;
      local_80 = "test invocation";
      puVar6 = &DAT_0040c480;
      local_78 = 0x40c4ec;
      local_70 = "Multi-call invocation";
      local_68 = "sha224sum";
      local_60 = "sha2 utilities";
      local_58 = "sha256sum";
      local_50 = "sha2 utilities";
      local_48 = "sha384sum";
      local_40 = "sha2 utilities";
      local_38 = "sha512sum";
      local_30 = "sha2 utilities";
      local_28 = 0;
      local_20 = 0;
      ppuVar4 = &local_88;
      do
	{
	  iVar1 = func_0x004016c0 ("printenv", puVar6);
	  if (iVar1 == 0)
	    break;
	  ppuVar4 = ppuVar4 + 2;
	  puVar6 = *ppuVar4;
	}
      while (puVar6 != (undefined *) 0x0);
      pcVar5 = ppuVar4[1];
      if (pcVar5 == (char *) 0x0)
	{
	  func_0x00401470 ("\n%s online help: <%s>\n", "GNU coreutils",
			   "https://www.gnu.org/software/coreutils/");
	  lVar2 = func_0x00401720 (5, 0);
	  if (lVar2 != 0)
	    {
	      iVar1 = func_0x00401640 (lVar2, &DAT_0040c50d, 3);
	      if (iVar1 != 0)
		{
		  pcVar5 = "printenv";
		  goto LAB_00401da9;
		}
	    }
	  func_0x00401470 ("Full documentation at: <%s%s>\n",
			   "https://www.gnu.org/software/coreutils/",
			   "printenv");
	LAB_00401dd2:
	  ;
	  pcVar5 = "printenv";
	  uVar3 = 0x40c4a5;
	}
      else
	{
	  func_0x00401470 ("\n%s online help: <%s>\n", "GNU coreutils",
			   "https://www.gnu.org/software/coreutils/");
	  lVar2 = func_0x00401720 (5, 0);
	  if (lVar2 != 0)
	    {
	      iVar1 = func_0x00401640 (lVar2, &DAT_0040c50d, 3);
	      if (iVar1 != 0)
		{
		LAB_00401da9:
		  ;
		  func_0x00401470
		    ("Report %s translation bugs to <https://translationproject.org/team/>\n",
		     "printenv");
		}
	    }
	  func_0x00401470 ("Full documentation at: <%s%s>\n",
			   "https://www.gnu.org/software/coreutils/",
			   "printenv");
	  uVar3 = 0x40d3cf;
	  if (pcVar5 == "printenv")
	    goto LAB_00401dd2;
	}
      func_0x00401470
	("or available locally via: info \'(coreutils) %s%s\'\n", pcVar5,
	 uVar3);
    }
  while (true);
}

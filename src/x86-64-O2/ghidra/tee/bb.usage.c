
#include "tee.h"

long null_ARRAY_0061044_0_8_;
long null_ARRAY_0061044_8_8_;
long null_ARRAY_0061064_0_8_;
long null_ARRAY_0061064_16_8_;
long null_ARRAY_0061064_24_8_;
long null_ARRAY_0061064_32_8_;
long null_ARRAY_0061064_40_8_;
long null_ARRAY_0061064_48_8_;
long null_ARRAY_0061064_8_8_;
long null_ARRAY_0061068_0_4_;
long null_ARRAY_0061068_16_8_;
long null_ARRAY_0061068_4_4_;
long null_ARRAY_0061068_8_4_;
long local_2f_1_1_;
long local_4_4_4_;
long local_5b_1_1_;
long local_9d_0_4_;
long local_a0_0_8_;
long local_a0_4_6_;
long local_a0_8_2_;
long local_a1_4_4_;
long local_a2_4_4_;
long local_a3_0_4_;
long local_a3_4_4_;
long local_a4_0_2_;
long local_a4_0_4_;
long local_a4_0_8_;
long local_a4_1_9_;
long local_a4_4_6_;
long local_a4_8_2_;
long local_a5_0_4_;
long local_a5_4_4_;
long DAT_00000010;
long DAT_0040d080;
long DAT_0040d082;
long DAT_0040d09f;
long DAT_0040d0a1;
long DAT_0040d127;
long DAT_0040d16a;
long DAT_0040d85a;
long DAT_0040d85c;
long DAT_0040d8b8;
long DAT_0040d8bc;
long DAT_0040d8c0;
long DAT_0040d8c3;
long DAT_0040d8c5;
long DAT_0040d8c9;
long DAT_0040d8cd;
long DAT_0040de6b;
long DAT_0040e215;
long DAT_0040e319;
long DAT_0040e31f;
long DAT_0040e331;
long DAT_0040e332;
long DAT_0040e350;
long DAT_0040e354;
long DAT_0040e3d6;
long DAT_00610000;
long DAT_00610010;
long DAT_00610020;
long DAT_006103f0;
long DAT_00610450;
long DAT_00610454;
long DAT_00610458;
long DAT_0061045c;
long DAT_00610480;
long DAT_00610488;
long DAT_00610490;
long DAT_006104a0;
long DAT_006104a8;
long DAT_006104c0;
long DAT_006104c8;
long DAT_00610510;
long DAT_00610514;
long DAT_00610515;
long DAT_00610518;
long DAT_00610520;
long DAT_00610528;
long DAT_00610678;
long DAT_006106b8;
long DAT_006106c0;
long DAT_006106c8;
long DAT_006106d8;
long _DYNAMIC;
long fde_0040edb0;
long null_ARRAY_0040d6c0;
long null_ARRAY_0040d6e0;
long null_ARRAY_0040d740;
long null_ARRAY_0040e660;
long null_ARRAY_0040e890;
long null_ARRAY_00610400;
long null_ARRAY_00610440;
long null_ARRAY_006104e0;
long null_ARRAY_00610540;
long null_ARRAY_00610640;
long null_ARRAY_00610680;
long PTR_DAT_006103e0;
long PTR_null_ARRAY_00610438;
long register0x00000020;
void
FUN_00402160 (uint uParm1)
{
  int iVar1;
  long lVar2;
  undefined8 uVar3;
  undefined **ppuVar4;
  undefined *puVar5;
  undefined *local_88;
  char *local_80;
  undefined8 local_78;
  char *local_70;
  char *local_68;
  char *local_60;
  char *local_58;
  char *local_50;
  char *local_48;
  char *local_40;
  char *local_38;
  char *local_30;
  undefined8 local_28;
  undefined8 local_20;

  if (uParm1 == 0)
    goto LAB_0040218d;
  func_0x004017a0 (DAT_006104a0, "Try \'%s --help\' for more information.\n",
		   DAT_00610528);
  do
    {
      func_0x00401970 ((ulong) uParm1);
    LAB_0040218d:
      ;
      func_0x00401610 ("Usage: %s [OPTION]... [FILE]...\n", DAT_00610528);
      uVar3 = DAT_00610480;
      func_0x004017b0
	("Copy standard input to each FILE, and also to standard output.\n\n  -a, --append              append to the given FILEs, do not overwrite\n  -i, --ignore-interrupts   ignore interrupt signals\n",
	 DAT_00610480);
      func_0x004017b0
	("  -p                        diagnose errors writing to non pipes\n      --output-error[=MODE]   set behavior on write error.  See MODE below\n",
	 uVar3);
      func_0x004017b0 ("      --help     display this help and exit\n",
		       uVar3);
      func_0x004017b0
	("      --version  output version information and exit\n", uVar3);
      func_0x004017b0
	("\nMODE determines behavior with write errors on the outputs:\n  \'warn\'         diagnose errors writing to any output\n  \'warn-nopipe\'  diagnose errors writing to any output not a pipe\n  \'exit\'         exit on error writing to any output\n  \'exit-nopipe\'  exit on error writing to any output not a pipe\nThe default MODE for the -p option is \'warn-nopipe\'.\nThe default operation when --output-error is not specified, is to\nexit immediately on error writing to a pipe, and diagnose errors\nwriting to non pipe outputs.\n",
	 uVar3);
      local_88 = &DAT_0040d09f;
      local_80 = "test invocation";
      puVar5 = &DAT_0040d09f;
      local_78 = 0x40d106;
      local_70 = "Multi-call invocation";
      local_68 = "sha224sum";
      local_60 = "sha2 utilities";
      local_58 = "sha256sum";
      local_50 = "sha2 utilities";
      local_48 = "sha384sum";
      local_40 = "sha2 utilities";
      local_38 = "sha512sum";
      local_30 = "sha2 utilities";
      local_28 = 0;
      local_20 = 0;
      ppuVar4 = &local_88;
      do
	{
	  iVar1 = func_0x004018c0 (&DAT_0040d0a1, puVar5);
	  if (iVar1 == 0)
	    break;
	  ppuVar4 = ppuVar4 + 2;
	  puVar5 = *ppuVar4;
	}
      while (puVar5 != (undefined *) 0x0);
      puVar5 = ppuVar4[1];
      if (puVar5 == (undefined *) 0x0)
	{
	  func_0x00401610 ("\n%s online help: <%s>\n", "GNU coreutils",
			   "https://www.gnu.org/software/coreutils/");
	  lVar2 = func_0x00401920 (5, 0);
	  if (lVar2 != 0)
	    {
	      iVar1 = func_0x00401830 (lVar2, &DAT_0040d127, 3);
	      if (iVar1 != 0)
		{
		  puVar5 = &DAT_0040d0a1;
		  goto LAB_00402339;
		}
	    }
	  func_0x00401610 ("Full documentation at: <%s%s>\n",
			   "https://www.gnu.org/software/coreutils/",
			   &DAT_0040d0a1);
	LAB_00402362:
	  ;
	  puVar5 = &DAT_0040d0a1;
	  uVar3 = 0x40d0bf;
	}
      else
	{
	  func_0x00401610 ("\n%s online help: <%s>\n", "GNU coreutils",
			   "https://www.gnu.org/software/coreutils/");
	  lVar2 = func_0x00401920 (5, 0);
	  if (lVar2 != 0)
	    {
	      iVar1 = func_0x00401830 (lVar2, &DAT_0040d127, 3);
	      if (iVar1 != 0)
		{
		LAB_00402339:
		  ;
		  func_0x00401610
		    ("Report %s translation bugs to <https://translationproject.org/team/>\n",
		     &DAT_0040d0a1);
		}
	    }
	  func_0x00401610 ("Full documentation at: <%s%s>\n",
			   "https://www.gnu.org/software/coreutils/",
			   &DAT_0040d0a1);
	  uVar3 = 0x40e34f;
	  if (puVar5 == &DAT_0040d0a1)
	    goto LAB_00402362;
	}
      func_0x00401610
	("or available locally via: info \'(coreutils) %s%s\'\n", puVar5,
	 uVar3);
    }
  while (true);
}


#include "whoami.h"

long null_ARRAY_0060ec8_0_8_;
long null_ARRAY_0060ec8_8_8_;
long null_ARRAY_0060ee8_0_8_;
long null_ARRAY_0060ee8_16_8_;
long null_ARRAY_0060ee8_24_8_;
long null_ARRAY_0060ee8_32_8_;
long null_ARRAY_0060ee8_40_8_;
long null_ARRAY_0060ee8_48_8_;
long null_ARRAY_0060ee8_8_8_;
long null_ARRAY_0060eec_0_4_;
long null_ARRAY_0060eec_16_8_;
long null_ARRAY_0060eec_4_4_;
long null_ARRAY_0060eec_8_4_;
long local_2f_1_1_;
long local_5b_1_1_;
long local_9d_0_4_;
long local_a0_0_8_;
long local_a0_4_6_;
long local_a0_8_2_;
long local_a1_4_4_;
long local_a2_4_4_;
long local_a3_0_4_;
long local_a3_4_4_;
long local_a4_0_2_;
long local_a4_0_4_;
long local_a4_0_8_;
long local_a4_1_9_;
long local_a4_4_6_;
long local_a4_8_2_;
long local_a5_0_4_;
long local_a5_4_4_;
long DAT_00000010;
long DAT_0040c4c0;
long DAT_0040c562;
long DAT_0040c798;
long DAT_0040c860;
long DAT_0040c864;
long DAT_0040c868;
long DAT_0040c86b;
long DAT_0040c86d;
long DAT_0040c871;
long DAT_0040c875;
long DAT_0040ce2b;
long DAT_0040d1d5;
long DAT_0040d2d9;
long DAT_0040d2df;
long DAT_0040d2f1;
long DAT_0040d2f2;
long DAT_0040d310;
long DAT_0040d314;
long DAT_0040d396;
long DAT_0060e848;
long DAT_0060e858;
long DAT_0060e868;
long DAT_0060ec28;
long DAT_0060ec90;
long DAT_0060ec94;
long DAT_0060ec98;
long DAT_0060ec9c;
long DAT_0060ecc0;
long DAT_0060ecd0;
long DAT_0060ece0;
long DAT_0060ece8;
long DAT_0060ed00;
long DAT_0060ed08;
long DAT_0060ed50;
long DAT_0060ed58;
long DAT_0060ed60;
long DAT_0060eef8;
long DAT_0060ef00;
long DAT_0060ef08;
long DAT_0060ef18;
long _DYNAMIC;
long fde_0040dd00;
long null_ARRAY_0040c760;
long null_ARRAY_0040c7c0;
long null_ARRAY_0040d620;
long null_ARRAY_0040d850;
long null_ARRAY_0060ec40;
long null_ARRAY_0060ec80;
long null_ARRAY_0060ed20;
long null_ARRAY_0060ed80;
long null_ARRAY_0060ee80;
long null_ARRAY_0060eec0;
long PTR_DAT_0060ec20;
long PTR_null_ARRAY_0060ec78;
long register0x00000020;
void
FUN_00401b80 (uint uParm1)
{
  int iVar1;
  long lVar2;
  undefined8 uVar3;
  undefined **ppuVar4;
  char *pcVar5;
  undefined *puVar6;
  undefined *local_88;
  char *local_80;
  undefined8 local_78;
  char *local_70;
  char *local_68;
  char *local_60;
  char *local_58;
  char *local_50;
  char *local_48;
  char *local_40;
  char *local_38;
  char *local_30;
  undefined8 local_28;
  undefined8 local_20;

  if (uParm1 == 0)
    goto LAB_00401bad;
  func_0x00401650 (DAT_0060ece0, "Try \'%s --help\' for more information.\n",
		   DAT_0060ed60);
  do
    {
      func_0x004017e0 ((ulong) uParm1);
    LAB_00401bad:
      ;
      func_0x004014c0 ("Usage: %s [OPTION]...\n", DAT_0060ed60);
      uVar3 = DAT_0060ecc0;
      func_0x00401660
	("Print the user name associated with the current effective user ID.\nSame as id -un.\n\n",
	 DAT_0060ecc0);
      func_0x00401660 ("      --help     display this help and exit\n",
		       uVar3);
      func_0x00401660
	("      --version  output version information and exit\n", uVar3);
      local_88 = &DAT_0040c4c0;
      local_80 = "test invocation";
      puVar6 = &DAT_0040c4c0;
      local_78 = 0x40c541;
      local_70 = "Multi-call invocation";
      local_68 = "sha224sum";
      local_60 = "sha2 utilities";
      local_58 = "sha256sum";
      local_50 = "sha2 utilities";
      local_48 = "sha384sum";
      local_40 = "sha2 utilities";
      local_38 = "sha512sum";
      local_30 = "sha2 utilities";
      local_28 = 0;
      local_20 = 0;
      ppuVar4 = &local_88;
      do
	{
	  iVar1 = func_0x00401730 ("whoami", puVar6);
	  if (iVar1 == 0)
	    break;
	  ppuVar4 = ppuVar4 + 2;
	  puVar6 = *ppuVar4;
	}
      while (puVar6 != (undefined *) 0x0);
      pcVar5 = ppuVar4[1];
      if (pcVar5 == (char *) 0x0)
	{
	  func_0x004014c0 ("\n%s online help: <%s>\n", "GNU coreutils",
			   "https://www.gnu.org/software/coreutils/");
	  lVar2 = func_0x004017a0 (5, 0);
	  if (lVar2 != 0)
	    {
	      iVar1 = func_0x004016b0 (lVar2, &DAT_0040c562, 3);
	      if (iVar1 != 0)
		{
		  pcVar5 = "whoami";
		  goto LAB_00401d41;
		}
	    }
	  func_0x004014c0 ("Full documentation at: <%s%s>\n",
			   "https://www.gnu.org/software/coreutils/",
			   "whoami");
	LAB_00401d6a:
	  ;
	  pcVar5 = "whoami";
	  uVar3 = 0x40c4fa;
	}
      else
	{
	  func_0x004014c0 ("\n%s online help: <%s>\n", "GNU coreutils",
			   "https://www.gnu.org/software/coreutils/");
	  lVar2 = func_0x004017a0 (5, 0);
	  if (lVar2 != 0)
	    {
	      iVar1 = func_0x004016b0 (lVar2, &DAT_0040c562, 3);
	      if (iVar1 != 0)
		{
		LAB_00401d41:
		  ;
		  func_0x004014c0
		    ("Report %s translation bugs to <https://translationproject.org/team/>\n",
		     "whoami");
		}
	    }
	  func_0x004014c0 ("Full documentation at: <%s%s>\n",
			   "https://www.gnu.org/software/coreutils/",
			   "whoami");
	  uVar3 = 0x40d30f;
	  if (pcVar5 == "whoami")
	    goto LAB_00401d6a;
	}
      func_0x004014c0
	("or available locally via: info \'(coreutils) %s%s\'\n", pcVar5,
	 uVar3);
    }
  while (true);
}

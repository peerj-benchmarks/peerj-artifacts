
#include "uptime.h"

long null_ARRAY_0061348_0_8_;
long null_ARRAY_0061348_8_8_;
long null_ARRAY_0061368_0_8_;
long null_ARRAY_0061368_16_8_;
long null_ARRAY_0061368_24_8_;
long null_ARRAY_0061368_32_8_;
long null_ARRAY_0061368_40_8_;
long null_ARRAY_0061368_48_8_;
long null_ARRAY_0061368_8_8_;
long null_ARRAY_006136c_0_4_;
long null_ARRAY_006136c_16_8_;
long null_ARRAY_006136c_4_4_;
long null_ARRAY_006136c_8_4_;
long local_2f_1_1_;
long local_4_4_4_;
long local_5b_1_1_;
long local_9d_0_4_;
long local_a0_0_8_;
long local_a0_4_6_;
long local_a0_8_2_;
long local_a1_4_4_;
long local_a2_4_4_;
long local_a3_0_4_;
long local_a3_4_4_;
long local_a4_0_2_;
long local_a4_0_4_;
long local_a4_0_8_;
long local_a4_1_9_;
long local_a4_4_6_;
long local_a4_8_2_;
long local_a5_0_4_;
long local_a5_4_4_;
long DAT_00000010;
long DAT_0040fa33;
long DAT_0040fafa;
long DAT_00410610;
long DAT_004106e0;
long DAT_004106e4;
long DAT_004106e8;
long DAT_004106eb;
long DAT_004106ed;
long DAT_004106f1;
long DAT_004106f5;
long DAT_00410cab;
long DAT_00411055;
long DAT_00411159;
long DAT_0041115f;
long DAT_00411171;
long DAT_00411172;
long DAT_00411190;
long DAT_00411194;
long DAT_00411197;
long DAT_0041125a;
long DAT_00613000;
long DAT_00613010;
long DAT_00613020;
long DAT_00613428;
long DAT_00613490;
long DAT_00613494;
long DAT_00613498;
long DAT_0061349c;
long DAT_006134c0;
long DAT_006134d0;
long DAT_006134e0;
long DAT_006134e8;
long DAT_00613500;
long DAT_00613508;
long DAT_00613550;
long DAT_00613558;
long DAT_00613560;
long DAT_00613738;
long DAT_00613740;
long DAT_00613748;
long DAT_00613f88;
long DAT_00613f90;
long DAT_00613fa0;
long fde_00411ca8;
long LAB_004018f0;
long null_ARRAY_0040fe00;
long null_ARRAY_00410640;
long null_ARRAY_00411220;
long null_ARRAY_00411500;
long null_ARRAY_00411730;
long null_ARRAY_00613440;
long null_ARRAY_00613480;
long null_ARRAY_00613520;
long null_ARRAY_00613580;
long null_ARRAY_00613680;
long null_ARRAY_006136c0;
long null_ARRAY_00613700;
long null_ARRAY_00613780;
long PTR_DAT_00613420;
long PTR_null_ARRAY_00613478;
long PTR_null_ARRAY_006134a0;
long register0x00000020;
long stack0x00000008;
void
FUN_004021b0 (uint uParm1)
{
  int iVar1;
  long lVar2;
  undefined8 uVar3;
  undefined **ppuVar4;
  char *pcVar5;
  undefined *puVar6;
  undefined *local_88;
  char *local_80;
  undefined8 local_78;
  char *local_70;
  char *local_68;
  char *local_60;
  char *local_58;
  char *local_50;
  char *local_48;
  char *local_40;
  char *local_38;
  char *local_30;
  undefined8 local_28;
  undefined8 local_20;

  if (uParm1 == 0)
    goto LAB_004021dd;
  func_0x00401950 (DAT_006134e0, "Try \'%s --help\' for more information.\n",
		   DAT_00613560);
  do
    {
      func_0x00401b60 ((ulong) uParm1);
    LAB_004021dd:
      ;
      func_0x004017c0 ("Usage: %s [OPTION]... [FILE]\n", DAT_00613560);
      func_0x004017c0
	("Print the current time, the length of time the system has been up,\nthe number of users on the system, and the average number of jobs\nin the run queue over the last 1, 5 and 15 minutes.");
      func_0x004017c0
	("  Processes in\nan uninterruptible sleep state also contribute to the load average.\n");
      func_0x004017c0
	("If FILE is not specified, use %s.  %s as FILE is common.\n\n",
	 "/dev/null/utmp", "/dev/null/wtmp");
      uVar3 = DAT_006134c0;
      func_0x00401970 ("      --help     display this help and exit\n",
		       DAT_006134c0);
      func_0x00401970
	("      --version  output version information and exit\n", uVar3);
      local_88 = &DAT_0040fa33;
      local_80 = "test invocation";
      puVar6 = &DAT_0040fa33;
      local_78 = 0x40fad9;
      local_70 = "Multi-call invocation";
      local_68 = "sha224sum";
      local_60 = "sha2 utilities";
      local_58 = "sha256sum";
      local_50 = "sha2 utilities";
      local_48 = "sha384sum";
      local_40 = "sha2 utilities";
      local_38 = "sha512sum";
      local_30 = "sha2 utilities";
      local_28 = 0;
      local_20 = 0;
      ppuVar4 = &local_88;
      do
	{
	  iVar1 = func_0x00401a80 ("uptime", puVar6);
	  if (iVar1 == 0)
	    break;
	  ppuVar4 = ppuVar4 + 2;
	  puVar6 = *ppuVar4;
	}
      while (puVar6 != (undefined *) 0x0);
      pcVar5 = ppuVar4[1];
      if (pcVar5 == (char *) 0x0)
	{
	  func_0x004017c0 ("\n%s online help: <%s>\n", "GNU coreutils",
			   "https://www.gnu.org/software/coreutils/");
	  lVar2 = func_0x00401ae0 (5, 0);
	  if (lVar2 != 0)
	    {
	      iVar1 = func_0x004019c0 (lVar2, &DAT_0040fafa, 3);
	      if (iVar1 != 0)
		{
		  pcVar5 = "uptime";
		  goto LAB_00402399;
		}
	    }
	  func_0x004017c0 ("Full documentation at: <%s%s>\n",
			   "https://www.gnu.org/software/coreutils/",
			   "uptime");
	LAB_004023c2:
	  ;
	  pcVar5 = "uptime";
	  uVar3 = 0x40fa92;
	}
      else
	{
	  func_0x004017c0 ("\n%s online help: <%s>\n", "GNU coreutils",
			   "https://www.gnu.org/software/coreutils/");
	  lVar2 = func_0x00401ae0 (5, 0);
	  if (lVar2 != 0)
	    {
	      iVar1 = func_0x004019c0 (lVar2, &DAT_0040fafa, 3);
	      if (iVar1 != 0)
		{
		LAB_00402399:
		  ;
		  func_0x004017c0
		    ("Report %s translation bugs to <https://translationproject.org/team/>\n",
		     "uptime");
		}
	    }
	  func_0x004017c0 ("Full documentation at: <%s%s>\n",
			   "https://www.gnu.org/software/coreutils/",
			   "uptime");
	  uVar3 = 0x41118f;
	  if (pcVar5 == "uptime")
	    goto LAB_004023c2;
	}
      func_0x004017c0
	("or available locally via: info \'(coreutils) %s%s\'\n", pcVar5,
	 uVar3);
    }
  while (true);
}

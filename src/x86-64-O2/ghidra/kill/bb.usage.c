
#include "kill.h"

long null_ARRAY_0060fb6_0_8_;
long null_ARRAY_0060fb6_8_8_;
long null_ARRAY_0060ff0_0_8_;
long null_ARRAY_0060ff0_16_8_;
long null_ARRAY_0060ff0_24_8_;
long null_ARRAY_0060ff0_32_8_;
long null_ARRAY_0060ff0_40_8_;
long null_ARRAY_0060ff0_48_8_;
long null_ARRAY_0060ff0_8_8_;
long null_ARRAY_0060ff4_0_4_;
long null_ARRAY_0060ff4_16_8_;
long null_ARRAY_0060ff4_4_4_;
long null_ARRAY_0060ff4_8_4_;
long local_2f_1_1_;
long local_5b_1_1_;
long local_9d_0_4_;
long local_a0_0_8_;
long local_a0_4_6_;
long local_a0_8_2_;
long local_a1_4_4_;
long local_a2_4_4_;
long local_a3_0_4_;
long local_a3_4_4_;
long local_a4_0_2_;
long local_a4_0_4_;
long local_a4_0_8_;
long local_a4_1_9_;
long local_a4_4_6_;
long local_a4_8_2_;
long local_a5_0_4_;
long local_a5_4_4_;
long DAT_00000010;
long DAT_0040ce00;
long DAT_0040ce02;
long DAT_0040ce89;
long DAT_0040ce8d;
long DAT_0040ced5;
long DAT_0040d5f8;
long DAT_0040d5fc;
long DAT_0040d600;
long DAT_0040d603;
long DAT_0040d605;
long DAT_0040d609;
long DAT_0040d60d;
long DAT_0040dbab;
long DAT_0040df55;
long DAT_0040e059;
long DAT_0040e05f;
long DAT_0040e071;
long DAT_0040e072;
long DAT_0040e090;
long DAT_0040e0a0;
long DAT_0040e0a4;
long DAT_0060f720;
long DAT_0060f730;
long DAT_0060f740;
long DAT_0060fb08;
long DAT_0060fb70;
long DAT_0060fb74;
long DAT_0060fb78;
long DAT_0060fb7c;
long DAT_0060fb80;
long DAT_0060fb84;
long DAT_0060fd40;
long DAT_0060fd50;
long DAT_0060fd60;
long DAT_0060fd68;
long DAT_0060fd80;
long DAT_0060fd88;
long DAT_0060fdd0;
long DAT_0060fdd8;
long DAT_0060fde0;
long DAT_0060ff78;
long DAT_0060ff80;
long DAT_0060ff88;
long DAT_0060ff98;
long fde_0040eac8;
long null_ARRAY_0040d440;
long null_ARRAY_0040e3c0;
long null_ARRAY_0040e5f0;
long null_ARRAY_0060fb20;
long null_ARRAY_0060fb60;
long null_ARRAY_0060fda0;
long null_ARRAY_0060fe00;
long null_ARRAY_0060ff00;
long null_ARRAY_0060ff40;
long PTR_DAT_0060fb00;
long PTR_null_ARRAY_0060fb58;
long register0x00000020;
void
FUN_004020c0 (uint uParm1)
{
  int iVar1;
  long lVar2;
  undefined8 uVar3;
  undefined **ppuVar4;
  undefined *puVar5;
  undefined *local_88;
  char *local_80;
  undefined8 local_78;
  char *local_70;
  char *local_68;
  char *local_60;
  char *local_58;
  char *local_50;
  char *local_48;
  char *local_40;
  char *local_38;
  char *local_30;
  undefined8 local_28;
  undefined8 local_20;

  if (uParm1 == 0)
    goto LAB_004020ed;
  func_0x00401790 (DAT_0060fd60, "Try \'%s --help\' for more information.\n",
		   DAT_0060fde0);
  do
    {
      func_0x00401930 ((ulong) uParm1);
    LAB_004020ed:
      ;
      func_0x004015d0
	("Usage: %s [-s SIGNAL | -SIGNAL] PID...\n  or:  %s -l [SIGNAL]...\n  or:  %s -t [SIGNAL]...\n",
	 DAT_0060fde0, DAT_0060fde0, DAT_0060fde0);
      uVar3 = DAT_0060fd40;
      func_0x004017b0 ("Send signals to processes, or list signals.\n",
		       DAT_0060fd40);
      func_0x004017b0
	("\nMandatory arguments to long options are mandatory for short options too.\n",
	 uVar3);
      func_0x004017b0
	("  -s, --signal=SIGNAL, -SIGNAL\n                   specify the name or number of the signal to be sent\n  -l, --list       list signal names, or convert signal names to/from numbers\n  -t, --table      print a table of signal information\n",
	 uVar3);
      func_0x004017b0 ("      --help     display this help and exit\n",
		       uVar3);
      func_0x004017b0
	("      --version  output version information and exit\n", uVar3);
      func_0x004017b0
	("\nSIGNAL may be a signal name like \'HUP\', or a signal number like \'1\',\nor the exit status of a process terminated by a signal.\nPID is an integer; if negative it identifies a process group.\n",
	 uVar3);
      func_0x004015d0
	("\nNOTE: your shell may have its own version of %s, which usually supersedes\nthe version described here.  Please refer to your shell\'s documentation\nfor details about the options it supports.\n",
	 &DAT_0040ce02);
      local_88 = &DAT_0040ce00;
      local_80 = "test invocation";
      puVar5 = &DAT_0040ce00;
      local_78 = 0x40ce68;
      local_70 = "Multi-call invocation";
      local_68 = "sha224sum";
      local_60 = "sha2 utilities";
      local_58 = "sha256sum";
      local_50 = "sha2 utilities";
      local_48 = "sha384sum";
      local_40 = "sha2 utilities";
      local_38 = "sha512sum";
      local_30 = "sha2 utilities";
      local_28 = 0;
      local_20 = 0;
      ppuVar4 = &local_88;
      do
	{
	  iVar1 = func_0x00401880 (&DAT_0040ce02, puVar5);
	  if (iVar1 == 0)
	    break;
	  ppuVar4 = ppuVar4 + 2;
	  puVar5 = *ppuVar4;
	}
      while (puVar5 != (undefined *) 0x0);
      puVar5 = ppuVar4[1];
      if (puVar5 == (undefined *) 0x0)
	{
	  func_0x004015d0 ("\n%s online help: <%s>\n", "GNU coreutils",
			   "https://www.gnu.org/software/coreutils/");
	  lVar2 = func_0x004018f0 (5, 0);
	  if (lVar2 != 0)
	    {
	      iVar1 = func_0x00401800 (lVar2, &DAT_0040ce89, 3);
	      if (iVar1 != 0)
		{
		  puVar5 = &DAT_0040ce02;
		  goto LAB_004022c1;
		}
	    }
	  func_0x004015d0 ("Full documentation at: <%s%s>\n",
			   "https://www.gnu.org/software/coreutils/",
			   &DAT_0040ce02);
	LAB_004022ea:
	  ;
	  puVar5 = &DAT_0040ce02;
	  uVar3 = 0x40ce21;
	}
      else
	{
	  func_0x004015d0 ("\n%s online help: <%s>\n", "GNU coreutils",
			   "https://www.gnu.org/software/coreutils/");
	  lVar2 = func_0x004018f0 (5, 0);
	  if (lVar2 != 0)
	    {
	      iVar1 = func_0x00401800 (lVar2, &DAT_0040ce89, 3);
	      if (iVar1 != 0)
		{
		LAB_004022c1:
		  ;
		  func_0x004015d0
		    ("Report %s translation bugs to <https://translationproject.org/team/>\n",
		     &DAT_0040ce02);
		}
	    }
	  func_0x004015d0 ("Full documentation at: <%s%s>\n",
			   "https://www.gnu.org/software/coreutils/",
			   &DAT_0040ce02);
	  uVar3 = 0x40e08f;
	  if (puVar5 == &DAT_0040ce02)
	    goto LAB_004022ea;
	}
      func_0x004015d0
	("or available locally via: info \'(coreutils) %s%s\'\n", puVar5,
	 uVar3);
    }
  while (true);
}


#include "pathchk.h"

long null_ARRAY_0060f70_0_8_;
long null_ARRAY_0060f70_8_8_;
long null_ARRAY_0060f90_0_8_;
long null_ARRAY_0060f90_16_8_;
long null_ARRAY_0060f90_24_8_;
long null_ARRAY_0060f90_32_8_;
long null_ARRAY_0060f90_40_8_;
long null_ARRAY_0060f90_48_8_;
long null_ARRAY_0060f90_8_8_;
long null_ARRAY_0060f94_0_4_;
long null_ARRAY_0060f94_16_8_;
long null_ARRAY_0060f94_4_4_;
long null_ARRAY_0060f94_8_4_;
long local_2f_1_1_;
long local_4_4_4_;
long local_5b_1_1_;
long local_9d_0_4_;
long local_a0_0_8_;
long local_a0_4_6_;
long local_a0_8_2_;
long local_a1_4_4_;
long local_a2_4_4_;
long local_a3_0_4_;
long local_a3_4_4_;
long local_a4_0_2_;
long local_a4_0_4_;
long local_a4_0_8_;
long local_a4_1_9_;
long local_a4_4_6_;
long local_a4_8_2_;
long local_a5_0_4_;
long local_a5_4_4_;
long DAT_00000010;
long DAT_0040cc00;
long DAT_0040cc8c;
long DAT_0040cc90;
long DAT_0040ccbb;
long DAT_0040d190;
long DAT_0040d196;
long DAT_0040d198;
long DAT_0040d19c;
long DAT_0040d1a0;
long DAT_0040d1a3;
long DAT_0040d1a5;
long DAT_0040d1a9;
long DAT_0040d1ad;
long DAT_0040d72b;
long DAT_0040dad5;
long DAT_0040dbd9;
long DAT_0040dbdf;
long DAT_0040dbf1;
long DAT_0040dbf2;
long DAT_0040dc10;
long DAT_0040dc14;
long DAT_0040dc96;
long DAT_0060f2d0;
long DAT_0060f2e0;
long DAT_0060f2f0;
long DAT_0060f6a8;
long DAT_0060f710;
long DAT_0060f714;
long DAT_0060f718;
long DAT_0060f71c;
long DAT_0060f740;
long DAT_0060f750;
long DAT_0060f760;
long DAT_0060f768;
long DAT_0060f780;
long DAT_0060f788;
long DAT_0060f7d0;
long DAT_0060f7d8;
long DAT_0060f7e0;
long DAT_0060f978;
long DAT_0060f980;
long DAT_0060f988;
long DAT_0060f990;
long DAT_0060f9a0;
long fde_0040e640;
long null_ARRAY_0040d0c0;
long null_ARRAY_0040df20;
long null_ARRAY_0040e150;
long null_ARRAY_0060f6c0;
long null_ARRAY_0060f700;
long null_ARRAY_0060f7a0;
long null_ARRAY_0060f800;
long null_ARRAY_0060f900;
long null_ARRAY_0060f940;
long PTR_DAT_0060f6a0;
long PTR_null_ARRAY_0060f6f8;
long register0x00000020;
void
FUN_004020a0 (uint uParm1)
{
  int iVar1;
  long lVar2;
  undefined8 uVar3;
  undefined **ppuVar4;
  char *pcVar5;
  undefined *puVar6;
  undefined *local_88;
  char *local_80;
  undefined8 local_78;
  char *local_70;
  char *local_68;
  char *local_60;
  char *local_58;
  char *local_50;
  char *local_48;
  char *local_40;
  char *local_38;
  char *local_30;
  undefined8 local_28;
  undefined8 local_20;

  if (uParm1 == 0)
    goto LAB_004020cd;
  func_0x00401680 (DAT_0060f760, "Try \'%s --help\' for more information.\n",
		   DAT_0060f7e0);
  do
    {
      func_0x00401820 ((ulong) uParm1);
    LAB_004020cd:
      ;
      func_0x00401500 ("Usage: %s [OPTION]... NAME...\n", DAT_0060f7e0);
      uVar3 = DAT_0060f740;
      func_0x00401690
	("Diagnose invalid or unportable file names.\n\n  -p                  check for most POSIX systems\n  -P                  check for empty names and leading \"-\"\n      --portability   check for all POSIX systems (equivalent to -p -P)\n",
	 DAT_0060f740);
      func_0x00401690 ("      --help     display this help and exit\n",
		       uVar3);
      func_0x00401690
	("      --version  output version information and exit\n", uVar3);
      local_88 = &DAT_0040cc00;
      local_80 = "test invocation";
      puVar6 = &DAT_0040cc00;
      local_78 = 0x40cc6b;
      local_70 = "Multi-call invocation";
      local_68 = "sha224sum";
      local_60 = "sha2 utilities";
      local_58 = "sha256sum";
      local_50 = "sha2 utilities";
      local_48 = "sha384sum";
      local_40 = "sha2 utilities";
      local_38 = "sha512sum";
      local_30 = "sha2 utilities";
      local_28 = 0;
      local_20 = 0;
      ppuVar4 = &local_88;
      do
	{
	  iVar1 = func_0x00401770 ("pathchk", puVar6);
	  if (iVar1 == 0)
	    break;
	  ppuVar4 = ppuVar4 + 2;
	  puVar6 = *ppuVar4;
	}
      while (puVar6 != (undefined *) 0x0);
      pcVar5 = ppuVar4[1];
      if (pcVar5 == (char *) 0x0)
	{
	  func_0x00401500 ("\n%s online help: <%s>\n", "GNU coreutils",
			   "https://www.gnu.org/software/coreutils/");
	  lVar2 = func_0x004017d0 (5, 0);
	  if (lVar2 != 0)
	    {
	      iVar1 = func_0x004016f0 (lVar2, &DAT_0040cc8c, 3);
	      if (iVar1 != 0)
		{
		  pcVar5 = "pathchk";
		  goto LAB_00402261;
		}
	    }
	  func_0x00401500 ("Full documentation at: <%s%s>\n",
			   "https://www.gnu.org/software/coreutils/",
			   "pathchk");
	LAB_0040228a:
	  ;
	  pcVar5 = "pathchk";
	  uVar3 = 0x40cc24;
	}
      else
	{
	  func_0x00401500 ("\n%s online help: <%s>\n", "GNU coreutils",
			   "https://www.gnu.org/software/coreutils/");
	  lVar2 = func_0x004017d0 (5, 0);
	  if (lVar2 != 0)
	    {
	      iVar1 = func_0x004016f0 (lVar2, &DAT_0040cc8c, 3);
	      if (iVar1 != 0)
		{
		LAB_00402261:
		  ;
		  func_0x00401500
		    ("Report %s translation bugs to <https://translationproject.org/team/>\n",
		     "pathchk");
		}
	    }
	  func_0x00401500 ("Full documentation at: <%s%s>\n",
			   "https://www.gnu.org/software/coreutils/",
			   "pathchk");
	  uVar3 = 0x40dc0f;
	  if (pcVar5 == "pathchk")
	    goto LAB_0040228a;
	}
      func_0x00401500
	("or available locally via: info \'(coreutils) %s%s\'\n", pcVar5,
	 uVar3);
    }
  while (true);
}


#include "uname.h"

long null_ARRAY_0060f42_0_8_;
long null_ARRAY_0060f42_8_8_;
long null_ARRAY_0060f60_0_8_;
long null_ARRAY_0060f60_16_8_;
long null_ARRAY_0060f60_24_8_;
long null_ARRAY_0060f60_32_8_;
long null_ARRAY_0060f60_40_8_;
long null_ARRAY_0060f60_48_8_;
long null_ARRAY_0060f60_8_8_;
long null_ARRAY_0060f64_0_4_;
long null_ARRAY_0060f64_16_8_;
long null_ARRAY_0060f64_4_4_;
long null_ARRAY_0060f64_8_4_;
long local_2f_1_1_;
long local_5b_1_1_;
long local_9d_0_4_;
long local_a0_0_8_;
long local_a0_4_6_;
long local_a0_8_2_;
long local_a1_4_4_;
long local_a2_4_4_;
long local_a3_0_4_;
long local_a3_4_4_;
long local_a4_0_2_;
long local_a4_0_4_;
long local_a4_0_8_;
long local_a4_1_9_;
long local_a4_4_6_;
long local_a4_8_2_;
long local_a5_0_4_;
long local_a5_4_4_;
long DAT_00000010;
long DAT_0040c6cb;
long DAT_0040c784;
long DAT_0040cf58;
long DAT_0040cf5c;
long DAT_0040cf60;
long DAT_0040cf63;
long DAT_0040cf65;
long DAT_0040cf69;
long DAT_0040cf6d;
long DAT_0040d4eb;
long DAT_0040d895;
long DAT_0040d999;
long DAT_0040d99f;
long DAT_0040d9b1;
long DAT_0040d9b2;
long DAT_0040d9d0;
long DAT_0040d9d4;
long DAT_0040da56;
long DAT_0060f000;
long DAT_0060f010;
long DAT_0060f020;
long DAT_0060f3c0;
long DAT_0060f3d0;
long DAT_0060f430;
long DAT_0060f434;
long DAT_0060f438;
long DAT_0060f43c;
long DAT_0060f440;
long DAT_0060f450;
long DAT_0060f460;
long DAT_0060f468;
long DAT_0060f480;
long DAT_0060f488;
long DAT_0060f4d0;
long DAT_0060f4d8;
long DAT_0060f4e0;
long DAT_0060f4e8;
long DAT_0060f678;
long DAT_0060f680;
long DAT_0060f688;
long DAT_0060f698;
long _DYNAMIC;
long fde_0040e3c8;
long null_ARRAY_0040ccc0;
long null_ARRAY_0040cd40;
long null_ARRAY_0040dce0;
long null_ARRAY_0040df10;
long null_ARRAY_0060f3e0;
long null_ARRAY_0060f420;
long null_ARRAY_0060f4a0;
long null_ARRAY_0060f500;
long null_ARRAY_0060f600;
long null_ARRAY_0060f640;
long PTR_DAT_0060f3c8;
long PTR_null_ARRAY_0060f418;
long register0x00000020;
void
FUN_00401da0 (uint uParm1)
{
  int iVar1;
  long lVar2;
  undefined8 uVar3;
  undefined **ppuVar4;
  char *pcVar5;
  char *pcVar6;
  undefined *puVar7;
  undefined *local_88;
  char *local_80;
  undefined8 local_78;
  char *local_70;
  char *local_68;
  char *local_60;
  char *local_58;
  char *local_50;
  char *local_48;
  char *local_40;
  char *local_38;
  char *local_30;
  undefined8 local_28;
  undefined8 local_20;

  if (uParm1 == 0)
    goto LAB_00401dd1;
  func_0x00401620 (DAT_0060f460, "Try \'%s --help\' for more information.\n",
		   DAT_0060f4e8);
  do
    {
      func_0x004017b0 ((ulong) uParm1);
    LAB_00401dd1:
      ;
      func_0x004014a0 ("Usage: %s [OPTION]...\n", DAT_0060f4e8);
      uVar3 = DAT_0060f440;
      if (DAT_0060f3c0 == 1)
	{
	  func_0x00401630
	    ("Print certain system information.  With no OPTION, same as -s.\n\n  -a, --all                print all information, in the following order,\n                             except omit -p and -i if unknown:\n  -s, --kernel-name        print the kernel name\n  -n, --nodename           print the network node hostname\n  -r, --kernel-release     print the kernel release\n",
	     DAT_0060f440);
	  func_0x00401630
	    ("  -v, --kernel-version     print the kernel version\n  -m, --machine            print the machine hardware name\n  -p, --processor          print the processor type (non-portable)\n  -i, --hardware-platform  print the hardware platform (non-portable)\n  -o, --operating-system   print the operating system\n",
	     uVar3);
	}
      else
	{
	  func_0x00401630 ("Print machine architecture.\n\n", DAT_0060f440);
	  uVar3 = DAT_0060f440;
	}
      pcVar6 = "arch";
      func_0x00401630 ("      --help     display this help and exit\n",
		       uVar3);
      func_0x00401630
	("      --version  output version information and exit\n", uVar3);
      local_88 = &DAT_0040c6cb;
      local_80 = "test invocation";
      local_78 = 0x40c763;
      puVar7 = &DAT_0040c6cb;
      local_70 = "Multi-call invocation";
      local_68 = "sha224sum";
      local_60 = "sha2 utilities";
      local_58 = "sha256sum";
      if (DAT_0060f3c0 == 1)
	{
	  pcVar6 = "uname";
	}
      local_50 = "sha2 utilities";
      local_48 = "sha384sum";
      local_40 = "sha2 utilities";
      local_38 = "sha512sum";
      local_30 = "sha2 utilities";
      local_28 = 0;
      local_20 = 0;
      ppuVar4 = &local_88;
      do
	{
	  iVar1 = func_0x00401700 (pcVar6, puVar7);
	  if (iVar1 == 0)
	    break;
	  ppuVar4 = ppuVar4 + 2;
	  puVar7 = *ppuVar4;
	}
      while (puVar7 != (undefined *) 0x0);
      pcVar5 = ppuVar4[1];
      if (pcVar5 == (char *) 0x0)
	{
	  func_0x004014a0 ("\n%s online help: <%s>\n", "GNU coreutils",
			   "https://www.gnu.org/software/coreutils/");
	  lVar2 = func_0x00401760 (5, 0);
	  if ((lVar2 != 0)
	      && (iVar1 = func_0x00401680 (lVar2, &DAT_0040c784, 3), pcVar5 =
		  pcVar6, iVar1 != 0))
	    goto LAB_00401fb1;
	  func_0x004014a0 ("Full documentation at: <%s%s>\n",
			   "https://www.gnu.org/software/coreutils/", pcVar6);
	LAB_00401fd9:
	  ;
	  uVar3 = 0x40c71c;
	  pcVar5 = pcVar6;
	}
      else
	{
	  func_0x004014a0 ("\n%s online help: <%s>\n", "GNU coreutils",
			   "https://www.gnu.org/software/coreutils/");
	  lVar2 = func_0x00401760 (5, 0);
	  if ((lVar2 != 0)
	      && (iVar1 =
		  func_0x00401680 (lVar2, &DAT_0040c784, 3), iVar1 != 0))
	    {
	    LAB_00401fb1:
	      ;
	      func_0x004014a0
		("Report %s translation bugs to <https://translationproject.org/team/>\n",
		 pcVar6);
	    }
	  func_0x004014a0 ("Full documentation at: <%s%s>\n",
			   "https://www.gnu.org/software/coreutils/", pcVar6);
	  uVar3 = 0x40c701;
	  if (pcVar6 == pcVar5)
	    goto LAB_00401fd9;
	}
      func_0x004014a0
	("or available locally via: info \'(coreutils) %s%s\'\n", pcVar5,
	 uVar3);
    }
  while (true);
}

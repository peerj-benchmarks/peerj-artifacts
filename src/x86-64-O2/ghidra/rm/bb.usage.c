
#include "rm.h"

long null_ARRAY_006164c_0_8_;
long null_ARRAY_006164c_8_8_;
long null_ARRAY_006166c_0_8_;
long null_ARRAY_006166c_16_8_;
long null_ARRAY_006166c_24_8_;
long null_ARRAY_006166c_32_8_;
long null_ARRAY_006166c_40_8_;
long null_ARRAY_006166c_48_8_;
long null_ARRAY_006166c_8_8_;
long null_ARRAY_0061670_0_4_;
long null_ARRAY_0061670_16_8_;
long null_ARRAY_0061670_4_4_;
long null_ARRAY_0061670_8_4_;
long local_2f_1_1_;
long local_4_4_4_;
long local_5b_1_1_;
long local_9d_0_4_;
long local_a0_0_8_;
long local_a0_4_6_;
long local_a0_8_2_;
long local_a1_4_4_;
long local_a2_4_4_;
long local_a3_0_4_;
long local_a3_4_4_;
long local_a4_0_2_;
long local_a4_0_4_;
long local_a4_0_8_;
long local_a4_1_9_;
long local_a4_4_6_;
long local_a4_8_2_;
long local_a5_0_4_;
long local_a5_4_4_;
long DAT_00000010;
long DAT_00411880;
long DAT_00411882;
long DAT_00411907;
long DAT_004122c0;
long DAT_00412516;
long DAT_00412517;
long DAT_00412868;
long DAT_004128c5;
long DAT_004128c7;
long DAT_0041293e;
long DAT_004129a8;
long DAT_004129ae;
long DAT_004129b0;
long DAT_004129b4;
long DAT_004129b8;
long DAT_004129bb;
long DAT_004129bd;
long DAT_004129c1;
long DAT_004129c5;
long DAT_00412f6b;
long DAT_0041331a;
long DAT_00413461;
long DAT_00413467;
long DAT_00413479;
long DAT_0041347a;
long DAT_00413498;
long DAT_004134d2;
long DAT_00413586;
long DAT_00616000;
long DAT_00616010;
long DAT_00616020;
long DAT_00616470;
long DAT_006164d0;
long DAT_006164d4;
long DAT_006164d8;
long DAT_006164dc;
long DAT_00616500;
long DAT_00616508;
long DAT_00616510;
long DAT_00616520;
long DAT_00616528;
long DAT_00616540;
long DAT_00616548;
long DAT_006165a0;
long DAT_006165a8;
long DAT_006165b0;
long DAT_006165b8;
long DAT_006166f8;
long DAT_006166f9;
long DAT_00616738;
long DAT_00616740;
long DAT_00616748;
long DAT_00616750;
long DAT_00616758;
long DAT_00616768;
long fde_00414178;
long null_ARRAY_004122d0;
long null_ARRAY_00412300;
long null_ARRAY_00412340;
long null_ARRAY_00413340;
long null_ARRAY_004134e0;
long null_ARRAY_00413820;
long null_ARRAY_00413a40;
long null_ARRAY_00616480;
long null_ARRAY_006164c0;
long null_ARRAY_00616560;
long null_ARRAY_00616590;
long null_ARRAY_006165c0;
long null_ARRAY_006166c0;
long null_ARRAY_00616700;
long PTR_DAT_00616460;
long PTR_FUN_00616468;
long PTR_null_ARRAY_006164b8;
long register0x00000020;
void
FUN_00402460 (uint uParm1)
{
  int iVar1;
  long lVar2;
  undefined8 uVar3;
  undefined **ppuVar4;
  undefined *puVar5;
  undefined *local_88;
  char *local_80;
  undefined8 local_78;
  char *local_70;
  char *local_68;
  char *local_60;
  char *local_58;
  char *local_50;
  char *local_48;
  char *local_40;
  char *local_38;
  char *local_30;
  undefined8 local_28;
  undefined8 local_20;

  if (uParm1 == 0)
    goto LAB_0040248d;
  func_0x00401b70 (DAT_00616520, "Try \'%s --help\' for more information.\n",
		   DAT_006165b8);
  do
    {
      func_0x00401d50 ((ulong) uParm1);
    LAB_0040248d:
      ;
      func_0x00401950 ("Usage: %s [OPTION]... [FILE]...\n", DAT_006165b8);
      uVar3 = DAT_00616500;
      func_0x00401b80
	("Remove (unlink) the FILE(s).\n\n  -f, --force           ignore nonexistent files and arguments, never prompt\n  -i                    prompt before every removal\n",
	 DAT_00616500);
      func_0x00401b80
	("  -I                    prompt once before removing more than three files, or\n                          when removing recursively; less intrusive than -i,\n                          while still giving protection against most mistakes\n      --interactive[=WHEN]  prompt according to WHEN: never, once (-I), or\n                          always (-i); without WHEN, prompt always\n",
	 uVar3);
      func_0x00401b80
	("      --one-file-system  when removing a hierarchy recursively, skip any\n                          directory that is on a file system different from\n                          that of the corresponding command line argument\n",
	 uVar3);
      func_0x00401b80
	("      --no-preserve-root  do not treat \'/\' specially\n      --preserve-root   do not remove \'/\' (default)\n  -r, -R, --recursive   remove directories and their contents recursively\n  -d, --dir             remove empty directories\n  -v, --verbose         explain what is being done\n",
	 uVar3);
      func_0x00401b80 ("      --help     display this help and exit\n",
		       uVar3);
      func_0x00401b80
	("      --version  output version information and exit\n", uVar3);
      func_0x00401b80
	("\nBy default, rm does not remove directories.  Use the --recursive (-r or -R)\noption to remove each listed directory, too, along with all of its contents.\n",
	 uVar3);
      func_0x00401950
	("\nTo remove a file whose name starts with a \'-\', for example \'-foo\',\nuse one of these commands:\n  %s -- -foo\n\n  %s ./-foo\n",
	 DAT_006165b8, DAT_006165b8);
      func_0x00401b80
	("\nNote that if you use rm to remove a file, it might be possible to recover\nsome of its contents, given sufficient expertise and/or time.  For greater\nassurance that the contents are truly unrecoverable, consider using shred.\n",
	 uVar3);
      local_88 = &DAT_00411880;
      local_80 = "test invocation";
      puVar5 = &DAT_00411880;
      local_78 = 0x4118e6;
      local_70 = "Multi-call invocation";
      local_68 = "sha224sum";
      local_60 = "sha2 utilities";
      local_58 = "sha256sum";
      local_50 = "sha2 utilities";
      local_48 = "sha384sum";
      local_40 = "sha2 utilities";
      local_38 = "sha512sum";
      local_30 = "sha2 utilities";
      local_28 = 0;
      local_20 = 0;
      ppuVar4 = &local_88;
      do
	{
	  iVar1 = func_0x00401ca0 (&DAT_00411882, puVar5);
	  if (iVar1 == 0)
	    break;
	  ppuVar4 = ppuVar4 + 2;
	  puVar5 = *ppuVar4;
	}
      while (puVar5 != (undefined *) 0x0);
      puVar5 = ppuVar4[1];
      if (puVar5 == (undefined *) 0x0)
	{
	  func_0x00401950 ("\n%s online help: <%s>\n", "GNU coreutils",
			   "https://www.gnu.org/software/coreutils/");
	  lVar2 = func_0x00401d00 (5, 0);
	  if (lVar2 != 0)
	    {
	      iVar1 = func_0x00401c00 (lVar2, &DAT_00411907, 3);
	      if (iVar1 != 0)
		{
		  puVar5 = &DAT_00411882;
		  goto LAB_00402679;
		}
	    }
	  func_0x00401950 ("Full documentation at: <%s%s>\n",
			   "https://www.gnu.org/software/coreutils/",
			   &DAT_00411882);
	LAB_004026a2:
	  ;
	  puVar5 = &DAT_00411882;
	  uVar3 = 0x41189f;
	}
      else
	{
	  func_0x00401950 ("\n%s online help: <%s>\n", "GNU coreutils",
			   "https://www.gnu.org/software/coreutils/");
	  lVar2 = func_0x00401d00 (5, 0);
	  if (lVar2 != 0)
	    {
	      iVar1 = func_0x00401c00 (lVar2, &DAT_00411907, 3);
	      if (iVar1 != 0)
		{
		LAB_00402679:
		  ;
		  func_0x00401950
		    ("Report %s translation bugs to <https://translationproject.org/team/>\n",
		     &DAT_00411882);
		}
	    }
	  func_0x00401950 ("Full documentation at: <%s%s>\n",
			   "https://www.gnu.org/software/coreutils/",
			   &DAT_00411882);
	  uVar3 = 0x413497;
	  if (puVar5 == &DAT_00411882)
	    goto LAB_004026a2;
	}
      func_0x00401950
	("or available locally via: info \'(coreutils) %s%s\'\n", puVar5,
	 uVar3);
    }
  while (true);
}


#include "expand.h"

long null_ARRAY_0061046_0_8_;
long null_ARRAY_0061046_8_8_;
long null_ARRAY_0061068_0_8_;
long null_ARRAY_0061068_16_8_;
long null_ARRAY_0061068_24_8_;
long null_ARRAY_0061068_32_8_;
long null_ARRAY_0061068_40_8_;
long null_ARRAY_0061068_48_8_;
long null_ARRAY_0061068_8_8_;
long null_ARRAY_006106c_0_4_;
long null_ARRAY_006106c_16_8_;
long null_ARRAY_006106c_4_4_;
long null_ARRAY_006106c_8_4_;
long local_2f_1_1_;
long local_4_4_4_;
long local_5b_1_1_;
long local_9d_0_4_;
long local_a0_0_8_;
long local_a0_4_6_;
long local_a0_8_2_;
long local_a1_4_4_;
long local_a2_4_4_;
long local_a3_0_4_;
long local_a3_4_4_;
long local_a4_0_2_;
long local_a4_0_4_;
long local_a4_0_8_;
long local_a4_1_9_;
long local_a4_4_6_;
long local_a4_8_2_;
long local_a5_0_4_;
long local_a5_4_4_;
long DAT_00000010;
long DAT_0040d240;
long DAT_0040d2cb;
long DAT_0040da60;
long DAT_0040da64;
long DAT_0040da68;
long DAT_0040da6b;
long DAT_0040da6d;
long DAT_0040da71;
long DAT_0040da75;
long DAT_0040e02b;
long DAT_0040e3d5;
long DAT_0040e4d9;
long DAT_0040e4df;
long DAT_0040e4f1;
long DAT_0040e4f2;
long DAT_0040e510;
long DAT_0040e514;
long DAT_0040e596;
long DAT_00610000;
long DAT_00610010;
long DAT_00610020;
long DAT_00610418;
long DAT_00610470;
long DAT_00610474;
long DAT_00610478;
long DAT_0061047c;
long DAT_00610480;
long DAT_00610488;
long DAT_00610490;
long DAT_006104a0;
long DAT_006104a8;
long DAT_006104c0;
long DAT_006104c8;
long DAT_00610510;
long DAT_00610514;
long DAT_00610518;
long DAT_00610520;
long DAT_00610528;
long DAT_00610530;
long DAT_00610538;
long DAT_00610540;
long DAT_00610548;
long DAT_00610550;
long DAT_00610558;
long DAT_00610560;
long DAT_00610568;
long DAT_00610570;
long DAT_006106f8;
long DAT_00610700;
long DAT_00610708;
long DAT_00610710;
long DAT_00610720;
long fde_0040ef60;
long null_ARRAY_0040d600;
long null_ARRAY_0040d6a0;
long null_ARRAY_0040e820;
long null_ARRAY_0040ea50;
long null_ARRAY_00610400;
long null_ARRAY_00610420;
long null_ARRAY_00610460;
long null_ARRAY_006104e0;
long null_ARRAY_00610580;
long null_ARRAY_00610680;
long null_ARRAY_006106c0;
long PTR_DAT_00610410;
long PTR_null_ARRAY_00610458;
long register0x00000020;
void
FUN_00401f00 (uint uParm1)
{
  int iVar1;
  long lVar2;
  undefined8 uVar3;
  undefined **ppuVar4;
  char *pcVar5;
  undefined *puVar6;
  undefined *local_88;
  char *local_80;
  undefined8 local_78;
  char *local_70;
  char *local_68;
  char *local_60;
  char *local_58;
  char *local_50;
  char *local_48;
  char *local_40;
  char *local_38;
  char *local_30;
  undefined8 local_28;
  undefined8 local_20;

  if (uParm1 == 0)
    goto LAB_00401f2d;
  func_0x004017f0 (DAT_006104a0, "Try \'%s --help\' for more information.\n",
		   DAT_00610570);
  do
    {
      func_0x004019b0 ((ulong) uParm1);
    LAB_00401f2d:
      ;
      func_0x00401650 ("Usage: %s [OPTION]... [FILE]...\n", DAT_00610570);
      uVar3 = DAT_00610480;
      func_0x00401800
	("Convert tabs in each FILE to spaces, writing to standard output.\n",
	 DAT_00610480);
      func_0x00401800
	("\nWith no FILE, or when FILE is -, read standard input.\n", uVar3);
      func_0x00401800
	("\nMandatory arguments to long options are mandatory for short options too.\n",
	 uVar3);
      func_0x00401800
	("  -i, --initial    do not convert tabs after non blanks\n  -t, --tabs=N     have tabs N characters apart, not 8\n",
	 uVar3);
      FUN_004029e0 ();
      func_0x00401800 ("      --help     display this help and exit\n",
		       uVar3);
      func_0x00401800
	("      --version  output version information and exit\n", uVar3);
      local_88 = &DAT_0040d240;
      local_80 = "test invocation";
      puVar6 = &DAT_0040d240;
      local_78 = 0x40d2aa;
      local_70 = "Multi-call invocation";
      local_68 = "sha224sum";
      local_60 = "sha2 utilities";
      local_58 = "sha256sum";
      local_50 = "sha2 utilities";
      local_48 = "sha384sum";
      local_40 = "sha2 utilities";
      local_38 = "sha512sum";
      local_30 = "sha2 utilities";
      local_28 = 0;
      local_20 = 0;
      ppuVar4 = &local_88;
      do
	{
	  iVar1 = func_0x00401900 ("expand", puVar6);
	  if (iVar1 == 0)
	    break;
	  ppuVar4 = ppuVar4 + 2;
	  puVar6 = *ppuVar4;
	}
      while (puVar6 != (undefined *) 0x0);
      pcVar5 = ppuVar4[1];
      if (pcVar5 == (char *) 0x0)
	{
	  func_0x00401650 ("\n%s online help: <%s>\n", "GNU coreutils",
			   "https://www.gnu.org/software/coreutils/");
	  lVar2 = func_0x00401960 (5, 0);
	  if (lVar2 != 0)
	    {
	      iVar1 = func_0x00401860 (lVar2, &DAT_0040d2cb, 3);
	      if (iVar1 != 0)
		{
		  pcVar5 = "expand";
		  goto LAB_004020f1;
		}
	    }
	  func_0x00401650 ("Full documentation at: <%s%s>\n",
			   "https://www.gnu.org/software/coreutils/",
			   "expand");
	LAB_0040211a:
	  ;
	  pcVar5 = "expand";
	  uVar3 = 0x40d263;
	}
      else
	{
	  func_0x00401650 ("\n%s online help: <%s>\n", "GNU coreutils",
			   "https://www.gnu.org/software/coreutils/");
	  lVar2 = func_0x00401960 (5, 0);
	  if (lVar2 != 0)
	    {
	      iVar1 = func_0x00401860 (lVar2, &DAT_0040d2cb, 3);
	      if (iVar1 != 0)
		{
		LAB_004020f1:
		  ;
		  func_0x00401650
		    ("Report %s translation bugs to <https://translationproject.org/team/>\n",
		     "expand");
		}
	    }
	  func_0x00401650 ("Full documentation at: <%s%s>\n",
			   "https://www.gnu.org/software/coreutils/",
			   "expand");
	  uVar3 = 0x40e50f;
	  if (pcVar5 == "expand")
	    goto LAB_0040211a;
	}
      func_0x00401650
	("or available locally via: info \'(coreutils) %s%s\'\n", pcVar5,
	 uVar3);
    }
  while (true);
}

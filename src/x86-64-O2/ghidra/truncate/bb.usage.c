
#include "truncate.h"

long null_ARRAY_006106c_0_8_;
long null_ARRAY_006106c_8_8_;
long null_ARRAY_006108c_0_8_;
long null_ARRAY_006108c_16_8_;
long null_ARRAY_006108c_24_8_;
long null_ARRAY_006108c_32_8_;
long null_ARRAY_006108c_40_8_;
long null_ARRAY_006108c_48_8_;
long null_ARRAY_006108c_8_8_;
long null_ARRAY_0061090_0_4_;
long null_ARRAY_0061090_16_8_;
long null_ARRAY_0061090_4_4_;
long null_ARRAY_0061090_8_4_;
long local_2f_1_1_;
long local_4_4_4_;
long local_5b_1_1_;
long local_9d_0_4_;
long local_a0_0_8_;
long local_a0_4_6_;
long local_a0_8_2_;
long local_a1_4_4_;
long local_a2_4_4_;
long local_a3_0_4_;
long local_a3_4_4_;
long local_a4_0_2_;
long local_a4_0_4_;
long local_a4_0_8_;
long local_a4_1_9_;
long local_a4_4_6_;
long local_a4_8_2_;
long local_a5_0_4_;
long local_a5_4_4_;
long DAT_00000010;
long DAT_0040d580;
long DAT_0040d62a;
long DAT_0040def8;
long DAT_0040defc;
long DAT_0040df00;
long DAT_0040df03;
long DAT_0040df05;
long DAT_0040df09;
long DAT_0040df0d;
long DAT_0040e4ab;
long DAT_0040ea48;
long DAT_0040eb51;
long DAT_0040eb57;
long DAT_0040eb69;
long DAT_0040eb6a;
long DAT_0040eb88;
long DAT_0040eb8c;
long DAT_0040ec0e;
long DAT_00610280;
long DAT_00610290;
long DAT_006102a0;
long DAT_00610668;
long DAT_006106d0;
long DAT_006106d4;
long DAT_006106d8;
long DAT_006106dc;
long DAT_00610700;
long DAT_00610710;
long DAT_00610720;
long DAT_00610728;
long DAT_00610740;
long DAT_00610748;
long DAT_00610790;
long DAT_00610798;
long DAT_00610799;
long DAT_006107a0;
long DAT_006107a8;
long DAT_006107b0;
long DAT_00610938;
long DAT_00610940;
long DAT_00610948;
long DAT_00610958;
long fde_0040f5c0;
long null_ARRAY_0040eea0;
long null_ARRAY_0040f0d0;
long null_ARRAY_00610680;
long null_ARRAY_006106c0;
long null_ARRAY_00610760;
long null_ARRAY_006107c0;
long null_ARRAY_006108c0;
long null_ARRAY_00610900;
long PTR_DAT_00610660;
long PTR_null_ARRAY_006106b8;
long register0x00000020;
void
FUN_00402380 (uint uParm1)
{
  int iVar1;
  long lVar2;
  undefined8 uVar3;
  undefined **ppuVar4;
  char *pcVar5;
  undefined *puVar6;
  undefined *local_88;
  char *local_80;
  undefined8 local_78;
  char *local_70;
  char *local_68;
  char *local_60;
  char *local_58;
  char *local_50;
  char *local_48;
  char *local_40;
  char *local_38;
  char *local_30;
  undefined8 local_28;
  undefined8 local_20;

  if (uParm1 == 0)
    goto LAB_004023ad;
  func_0x004016e0 (DAT_00610720, "Try \'%s --help\' for more information.\n",
		   DAT_006107b0);
  do
    {
      func_0x00401880 ((ulong) uParm1);
    LAB_004023ad:
      ;
      func_0x00401540 ("Usage: %s OPTION... FILE...\n", DAT_006107b0);
      uVar3 = DAT_00610700;
      func_0x004016f0
	("Shrink or extend the size of each FILE to the specified size\n\nA FILE argument that does not exist is created.\n\nIf a FILE is larger than the specified size, the extra data is lost.\nIf a FILE is shorter, it is extended and the extended part (hole)\nreads as zero bytes.\n",
	 DAT_00610700);
      func_0x004016f0
	("\nMandatory arguments to long options are mandatory for short options too.\n",
	 uVar3);
      func_0x004016f0 ("  -c, --no-create        do not create any files\n",
		       uVar3);
      func_0x004016f0
	("  -o, --io-blocks        treat SIZE as number of IO blocks instead of bytes\n",
	 uVar3);
      func_0x004016f0
	("  -r, --reference=RFILE  base size on RFILE\n  -s, --size=SIZE        set or adjust the file size by SIZE bytes\n",
	 uVar3);
      func_0x004016f0 ("      --help     display this help and exit\n",
		       uVar3);
      func_0x004016f0
	("      --version  output version information and exit\n", uVar3);
      func_0x004016f0
	("\nThe SIZE argument is an integer and optional unit (example: 10K is 10*1024).\nUnits are K,M,G,T,P,E,Z,Y (powers of 1024) or KB,MB,... (powers of 1000).\n",
	 uVar3);
      func_0x004016f0
	("\nSIZE may also be prefixed by one of the following modifying characters:\n\'+\' extend by, \'-\' reduce by, \'<\' at most, \'>\' at least,\n\'/\' round down to multiple of, \'%\' round up to multiple of.\n",
	 uVar3);
      local_88 = &DAT_0040d580;
      local_80 = "test invocation";
      puVar6 = &DAT_0040d580;
      local_78 = 0x40d609;
      local_70 = "Multi-call invocation";
      local_68 = "sha224sum";
      local_60 = "sha2 utilities";
      local_58 = "sha256sum";
      local_50 = "sha2 utilities";
      local_48 = "sha384sum";
      local_40 = "sha2 utilities";
      local_38 = "sha512sum";
      local_30 = "sha2 utilities";
      local_28 = 0;
      local_20 = 0;
      ppuVar4 = &local_88;
      do
	{
	  iVar1 = func_0x004017d0 ("truncate", puVar6);
	  if (iVar1 == 0)
	    break;
	  ppuVar4 = ppuVar4 + 2;
	  puVar6 = *ppuVar4;
	}
      while (puVar6 != (undefined *) 0x0);
      pcVar5 = ppuVar4[1];
      if (pcVar5 == (char *) 0x0)
	{
	  func_0x00401540 ("\n%s online help: <%s>\n", "GNU coreutils",
			   "https://www.gnu.org/software/coreutils/");
	  lVar2 = func_0x00401830 (5, 0);
	  if (lVar2 != 0)
	    {
	      iVar1 = func_0x00401740 (lVar2, &DAT_0040d62a, 3);
	      if (iVar1 != 0)
		{
		  pcVar5 = "truncate";
		  goto LAB_00402591;
		}
	    }
	  func_0x00401540 ("Full documentation at: <%s%s>\n",
			   "https://www.gnu.org/software/coreutils/",
			   "truncate");
	LAB_004025ba:
	  ;
	  pcVar5 = "truncate";
	  uVar3 = 0x40d5c2;
	}
      else
	{
	  func_0x00401540 ("\n%s online help: <%s>\n", "GNU coreutils",
			   "https://www.gnu.org/software/coreutils/");
	  lVar2 = func_0x00401830 (5, 0);
	  if (lVar2 != 0)
	    {
	      iVar1 = func_0x00401740 (lVar2, &DAT_0040d62a, 3);
	      if (iVar1 != 0)
		{
		LAB_00402591:
		  ;
		  func_0x00401540
		    ("Report %s translation bugs to <https://translationproject.org/team/>\n",
		     "truncate");
		}
	    }
	  func_0x00401540 ("Full documentation at: <%s%s>\n",
			   "https://www.gnu.org/software/coreutils/",
			   "truncate");
	  uVar3 = 0x40eb87;
	  if (pcVar5 == "truncate")
	    goto LAB_004025ba;
	}
      func_0x00401540
	("or available locally via: info \'(coreutils) %s%s\'\n", pcVar5,
	 uVar3);
    }
  while (true);
}

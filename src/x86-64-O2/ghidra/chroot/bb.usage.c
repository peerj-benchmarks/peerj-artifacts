
#include "chroot.h"

long null_ARRAY_006137a_0_8_;
long null_ARRAY_006137a_8_8_;
long null_ARRAY_0061398_0_8_;
long null_ARRAY_0061398_16_8_;
long null_ARRAY_0061398_24_8_;
long null_ARRAY_0061398_32_8_;
long null_ARRAY_0061398_40_8_;
long null_ARRAY_0061398_48_8_;
long null_ARRAY_0061398_8_8_;
long null_ARRAY_006139c_0_4_;
long null_ARRAY_006139c_16_8_;
long null_ARRAY_006139c_4_4_;
long null_ARRAY_006139c_8_4_;
long local_2f_1_1_;
long local_4_4_4_;
long local_5b_1_1_;
long local_9d_0_4_;
long local_a0_0_8_;
long local_a0_4_6_;
long local_a0_8_2_;
long local_a1_4_4_;
long local_a2_4_4_;
long local_a3_0_4_;
long local_a3_4_4_;
long local_a4_0_2_;
long local_a4_0_4_;
long local_a4_0_8_;
long local_a4_1_9_;
long local_a4_4_6_;
long local_a4_8_2_;
long local_a5_0_4_;
long local_a5_4_4_;
long DAT_00000010;
long DAT_0040fec0;
long DAT_0040fee9;
long DAT_0040ff74;
long DAT_0040ff8f;
long DAT_00410548;
long DAT_0041054e;
long DAT_00410550;
long DAT_00410554;
long DAT_00410558;
long DAT_0041055b;
long DAT_0041055d;
long DAT_00410561;
long DAT_00410565;
long DAT_00410b13;
long DAT_004110a6;
long DAT_004110b7;
long DAT_004110b8;
long DAT_004111b1;
long DAT_004111b7;
long DAT_004111c9;
long DAT_004111ca;
long DAT_004111e8;
long DAT_004111ec;
long DAT_0041126e;
long DAT_00613298;
long DAT_006132a8;
long DAT_006132b8;
long DAT_00613748;
long DAT_006137b0;
long DAT_006137b4;
long DAT_006137b8;
long DAT_006137bc;
long DAT_006137c0;
long DAT_006137d0;
long DAT_006137e0;
long DAT_006137e8;
long DAT_00613800;
long DAT_00613808;
long DAT_00613850;
long DAT_00613858;
long DAT_00613860;
long DAT_006139f8;
long DAT_00613a00;
long DAT_00613a08;
long DAT_00613a10;
long DAT_00613a18;
long DAT_00613a28;
long _DYNAMIC;
long fde_00411d40;
long null_ARRAY_00410400;
long null_ARRAY_004104e0;
long null_ARRAY_00411500;
long null_ARRAY_00411720;
long null_ARRAY_00613760;
long null_ARRAY_006137a0;
long null_ARRAY_00613820;
long null_ARRAY_00613880;
long null_ARRAY_00613980;
long null_ARRAY_006139c0;
long PTR_DAT_00613740;
long PTR_null_ARRAY_00613798;
long register0x00000020;
void
FUN_00402b50 (uint uParm1)
{
  int iVar1;
  undefined8 uVar2;
  long lVar3;
  undefined8 uVar4;
  undefined **ppuVar5;
  char *pcVar6;
  undefined *puVar7;
  undefined *local_88;
  char *local_80;
  undefined8 local_78;
  char *local_70;
  char *local_68;
  char *local_60;
  char *local_58;
  char *local_50;
  char *local_48;
  char *local_40;
  char *local_38;
  char *local_30;
  undefined8 local_28;
  undefined8 local_20;

  if (uParm1 == 0)
    goto LAB_00402b7d;
  func_0x00401da0 (DAT_006137e0, "Try \'%s --help\' for more information.\n",
		   DAT_00613860);
  do
    {
      func_0x00401fc0 ((ulong) uParm1);
    LAB_00402b7d:
      ;
      func_0x00401b80
	("Usage: %s [OPTION] NEWROOT [COMMAND [ARG]...]\n  or:  %s OPTION\n",
	 DAT_00613860, DAT_00613860);
      uVar4 = DAT_006137c0;
      func_0x00401db0 ("Run COMMAND with root directory set to NEWROOT.\n\n",
		       DAT_006137c0);
      func_0x00401db0
	("  --groups=G_LIST        specify supplementary groups as g1,g2,..,gN\n",
	 uVar4);
      func_0x00401db0
	("  --userspec=USER:GROUP  specify user and group (ID or name) to use\n",
	 uVar4);
      imperfection_wrapper ();	//    uVar2 = FUN_004053a0(4, &DAT_0041054e);
      func_0x00401b80
	("  --skip-chdir           do not change working directory to %s\n",
	 uVar2);
      func_0x00401db0 ("      --help     display this help and exit\n",
		       uVar4);
      func_0x00401db0
	("      --version  output version information and exit\n", uVar4);
      func_0x00401db0
	("\nIf no command is given, run \'\"$SHELL\" -i\' (default: \'/bin/sh -i\').\n",
	 uVar4);
      local_88 = &DAT_0040fee9;
      local_80 = "test invocation";
      puVar7 = &DAT_0040fee9;
      local_78 = 0x40ff53;
      local_70 = "Multi-call invocation";
      local_68 = "sha224sum";
      local_60 = "sha2 utilities";
      local_58 = "sha256sum";
      local_50 = "sha2 utilities";
      local_48 = "sha384sum";
      local_40 = "sha2 utilities";
      local_38 = "sha512sum";
      local_30 = "sha2 utilities";
      local_28 = 0;
      local_20 = 0;
      ppuVar5 = &local_88;
      do
	{
	  iVar1 = func_0x00401ef0 ("chroot", puVar7);
	  if (iVar1 == 0)
	    break;
	  ppuVar5 = ppuVar5 + 2;
	  puVar7 = *ppuVar5;
	}
      while (puVar7 != (undefined *) 0x0);
      pcVar6 = ppuVar5[1];
      if (pcVar6 == (char *) 0x0)
	{
	  func_0x00401b80 ("\n%s online help: <%s>\n", "GNU coreutils",
			   "https://www.gnu.org/software/coreutils/");
	  lVar3 = func_0x00401f70 (5, 0);
	  if (lVar3 != 0)
	    {
	      iVar1 = func_0x00401e40 (lVar3, &DAT_0040ff74, 3);
	      if (iVar1 != 0)
		{
		  pcVar6 = "chroot";
		  goto LAB_00402d59;
		}
	    }
	  func_0x00401b80 ("Full documentation at: <%s%s>\n",
			   "https://www.gnu.org/software/coreutils/",
			   "chroot");
	LAB_00402d82:
	  ;
	  pcVar6 = "chroot";
	  uVar4 = 0x40ff0c;
	}
      else
	{
	  func_0x00401b80 ("\n%s online help: <%s>\n", "GNU coreutils",
			   "https://www.gnu.org/software/coreutils/");
	  lVar3 = func_0x00401f70 (5, 0);
	  if (lVar3 != 0)
	    {
	      iVar1 = func_0x00401e40 (lVar3, &DAT_0040ff74, 3);
	      if (iVar1 != 0)
		{
		LAB_00402d59:
		  ;
		  func_0x00401b80
		    ("Report %s translation bugs to <https://translationproject.org/team/>\n",
		     "chroot");
		}
	    }
	  func_0x00401b80 ("Full documentation at: <%s%s>\n",
			   "https://www.gnu.org/software/coreutils/",
			   "chroot");
	  uVar4 = 0x4111e7;
	  if (pcVar6 == "chroot")
	    goto LAB_00402d82;
	}
      func_0x00401b80
	("or available locally via: info \'(coreutils) %s%s\'\n", pcVar6,
	 uVar4);
    }
  while (true);
}

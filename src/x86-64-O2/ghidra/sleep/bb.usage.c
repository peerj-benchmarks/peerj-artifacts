
#include "sleep.h"

long null_ARRAY_0060f42_0_8_;
long null_ARRAY_0060f42_8_8_;
long null_ARRAY_0060f60_0_8_;
long null_ARRAY_0060f60_16_8_;
long null_ARRAY_0060f60_24_8_;
long null_ARRAY_0060f60_32_8_;
long null_ARRAY_0060f60_40_8_;
long null_ARRAY_0060f60_48_8_;
long null_ARRAY_0060f60_8_8_;
long null_ARRAY_0060f64_0_4_;
long null_ARRAY_0060f64_16_8_;
long null_ARRAY_0060f64_4_4_;
long null_ARRAY_0060f64_8_4_;
long local_2f_1_1_;
long local_5b_1_1_;
long local_9d_0_4_;
long local_a0_0_8_;
long local_a0_4_6_;
long local_a0_8_2_;
long local_a1_4_4_;
long local_a2_4_4_;
long local_a3_0_4_;
long local_a3_4_4_;
long local_a4_0_2_;
long local_a4_0_4_;
long local_a4_0_8_;
long local_a4_1_9_;
long local_a4_4_6_;
long local_a4_8_2_;
long local_a5_0_4_;
long local_a5_4_4_;
long DAT_00000010;
long DAT_0040c900;
long DAT_0040c98a;
long DAT_0040cd25;
long DAT_0040cd3a;
long DAT_0040ce20;
long DAT_0040ce24;
long DAT_0040ce28;
long DAT_0040ce2b;
long DAT_0040ce2d;
long DAT_0040ce31;
long DAT_0040ce35;
long DAT_0040d3eb;
long DAT_0040d79d;
long DAT_0040d8a1;
long DAT_0040d8a7;
long DAT_0040d8b9;
long DAT_0040d8ba;
long DAT_0040d8d8;
long DAT_0040d976;
long DAT_0060f000;
long DAT_0060f010;
long DAT_0060f020;
long DAT_0060f3c8;
long DAT_0060f430;
long DAT_0060f434;
long DAT_0060f438;
long DAT_0060f43c;
long DAT_0060f440;
long DAT_0060f450;
long DAT_0060f460;
long DAT_0060f468;
long DAT_0060f480;
long DAT_0060f488;
long DAT_0060f4d0;
long DAT_0060f4d8;
long DAT_0060f4e0;
long DAT_0060f4e8;
long DAT_0060f678;
long DAT_0060f680;
long DAT_0060f688;
long DAT_0060f698;
long _DYNAMIC;
long fde_0040e2f8;
long null_ARRAY_0040cce0;
long null_ARRAY_0040cd80;
long null_ARRAY_0040dc00;
long null_ARRAY_0040de20;
long null_ARRAY_0060f3e0;
long null_ARRAY_0060f420;
long null_ARRAY_0060f4a0;
long null_ARRAY_0060f500;
long null_ARRAY_0060f600;
long null_ARRAY_0060f640;
long PTR_DAT_0060f3c0;
long PTR_null_ARRAY_0060f418;
long register0x00000020;
void
FUN_00401cb0 (uint uParm1)
{
  int iVar1;
  long lVar2;
  undefined8 uVar3;
  undefined **ppuVar4;
  char *pcVar5;
  undefined *puVar6;
  undefined *local_88;
  char *local_80;
  undefined8 local_78;
  char *local_70;
  char *local_68;
  char *local_60;
  char *local_58;
  char *local_50;
  char *local_48;
  char *local_40;
  char *local_38;
  char *local_30;
  undefined8 local_28;
  undefined8 local_20;

  if (uParm1 == 0)
    goto LAB_00401cdd;
  func_0x00401660 (DAT_0060f460, "Try \'%s --help\' for more information.\n",
		   DAT_0060f4e8);
  do
    {
      func_0x004017f0 ((ulong) uParm1);
    LAB_00401cdd:
      ;
      func_0x004014d0
	("Usage: %s NUMBER[SUFFIX]...\n  or:  %s OPTION\nPause for NUMBER seconds.  SUFFIX may be \'s\' for seconds (the default),\n\'m\' for minutes, \'h\' for hours or \'d\' for days.  Unlike most implementations\nthat require NUMBER be an integer, here NUMBER may be an arbitrary floating\npoint number.  Given two or more arguments, pause for the amount of time\nspecified by the sum of their values.\n\n",
	 DAT_0060f4e8, DAT_0060f4e8);
      uVar3 = DAT_0060f440;
      func_0x00401670 ("      --help     display this help and exit\n",
		       DAT_0060f440);
      func_0x00401670
	("      --version  output version information and exit\n", uVar3);
      local_88 = &DAT_0040c900;
      local_80 = "test invocation";
      puVar6 = &DAT_0040c900;
      local_78 = 0x40c969;
      local_70 = "Multi-call invocation";
      local_68 = "sha224sum";
      local_60 = "sha2 utilities";
      local_58 = "sha256sum";
      local_50 = "sha2 utilities";
      local_48 = "sha384sum";
      local_40 = "sha2 utilities";
      local_38 = "sha512sum";
      local_30 = "sha2 utilities";
      local_28 = 0;
      local_20 = 0;
      ppuVar4 = &local_88;
      do
	{
	  iVar1 = func_0x00401740 ("sleep", puVar6);
	  if (iVar1 == 0)
	    break;
	  ppuVar4 = ppuVar4 + 2;
	  puVar6 = *ppuVar4;
	}
      while (puVar6 != (undefined *) 0x0);
      pcVar5 = ppuVar4[1];
      if (pcVar5 == (char *) 0x0)
	{
	  func_0x004014d0 ("\n%s online help: <%s>\n", "GNU coreutils",
			   "https://www.gnu.org/software/coreutils/");
	  lVar2 = func_0x004017b0 (5, 0);
	  if (lVar2 != 0)
	    {
	      iVar1 = func_0x004016c0 (lVar2, &DAT_0040c98a, 3);
	      if (iVar1 != 0)
		{
		  pcVar5 = "sleep";
		  goto LAB_00401e69;
		}
	    }
	  func_0x004014d0 ("Full documentation at: <%s%s>\n",
			   "https://www.gnu.org/software/coreutils/",
			   "sleep");
	LAB_00401e92:
	  ;
	  pcVar5 = "sleep";
	  uVar3 = 0x40c922;
	}
      else
	{
	  func_0x004014d0 ("\n%s online help: <%s>\n", "GNU coreutils",
			   "https://www.gnu.org/software/coreutils/");
	  lVar2 = func_0x004017b0 (5, 0);
	  if (lVar2 != 0)
	    {
	      iVar1 = func_0x004016c0 (lVar2, &DAT_0040c98a, 3);
	      if (iVar1 != 0)
		{
		LAB_00401e69:
		  ;
		  func_0x004014d0
		    ("Report %s translation bugs to <https://translationproject.org/team/>\n",
		     "sleep");
		}
	    }
	  func_0x004014d0 ("Full documentation at: <%s%s>\n",
			   "https://www.gnu.org/software/coreutils/",
			   "sleep");
	  uVar3 = 0x40d8d7;
	  if (pcVar5 == "sleep")
	    goto LAB_00401e92;
	}
      func_0x004014d0
	("or available locally via: info \'(coreutils) %s%s\'\n", pcVar5,
	 uVar3);
    }
  while (true);
}

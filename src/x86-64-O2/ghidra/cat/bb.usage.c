
#include "cat.h"

long null_ARRAY_0061048_0_8_;
long null_ARRAY_0061048_8_8_;
long null_ARRAY_0061068_0_8_;
long null_ARRAY_0061068_16_8_;
long null_ARRAY_0061068_24_8_;
long null_ARRAY_0061068_32_8_;
long null_ARRAY_0061068_40_8_;
long null_ARRAY_0061068_48_8_;
long null_ARRAY_0061068_8_8_;
long null_ARRAY_006106c_0_4_;
long null_ARRAY_006106c_16_8_;
long null_ARRAY_006106c_4_4_;
long null_ARRAY_006106c_8_4_;
long local_2f_1_1_;
long local_4_4_4_;
long local_5b_1_1_;
long local_9d_0_4_;
long local_a0_0_8_;
long local_a0_4_6_;
long local_a0_8_2_;
long local_a1_4_4_;
long local_a2_4_4_;
long local_a3_0_4_;
long local_a3_4_4_;
long local_a4_0_2_;
long local_a4_0_4_;
long local_a4_0_8_;
long local_a4_1_9_;
long local_a4_4_6_;
long local_a4_8_2_;
long local_a5_0_4_;
long local_a5_4_4_;
long DAT_00000010;
long DAT_0040d30c;
long DAT_0040d30e;
long DAT_0040d394;
long DAT_0040dad0;
long DAT_0040dad4;
long DAT_0040dad8;
long DAT_0040dadb;
long DAT_0040dadd;
long DAT_0040dae1;
long DAT_0040dae5;
long DAT_0040e06b;
long DAT_0040e415;
long DAT_0040e519;
long DAT_0040e51f;
long DAT_0040e531;
long DAT_0040e532;
long DAT_0040e550;
long DAT_0040e554;
long DAT_0040e5d6;
long DAT_00610000;
long DAT_00610010;
long DAT_00610020;
long DAT_00610400;
long DAT_00610408;
long DAT_00610430;
long DAT_00610490;
long DAT_00610494;
long DAT_00610498;
long DAT_0061049c;
long DAT_006104c0;
long DAT_006104d0;
long DAT_006104e0;
long DAT_006104e8;
long DAT_00610500;
long DAT_00610508;
long DAT_00610550;
long DAT_00610554;
long DAT_00610558;
long DAT_00610560;
long DAT_00610568;
long DAT_00610570;
long DAT_006106f8;
long DAT_00610700;
long DAT_00610708;
long DAT_00610718;
long _DYNAMIC;
long fde_0040ef88;
long null_ARRAY_0040d940;
long null_ARRAY_0040e860;
long null_ARRAY_0040ea90;
long null_ARRAY_00610410;
long null_ARRAY_00610480;
long null_ARRAY_00610520;
long null_ARRAY_00610580;
long null_ARRAY_00610680;
long null_ARRAY_006106c0;
long PTR_DAT_00610428;
long PTR_null_ARRAY_00610478;
long register0x00000020;
void
FUN_00402750 (uint uParm1)
{
  int iVar1;
  long lVar2;
  undefined8 uVar3;
  undefined **ppuVar4;
  undefined *puVar5;
  undefined *local_88;
  char *local_80;
  undefined8 local_78;
  char *local_70;
  char *local_68;
  char *local_60;
  char *local_58;
  char *local_50;
  char *local_48;
  char *local_40;
  char *local_38;
  char *local_30;
  undefined8 local_28;
  undefined8 local_20;

  if (uParm1 == 0)
    goto LAB_0040277d;
  func_0x004017c0 (DAT_006104e0, "Try \'%s --help\' for more information.\n",
		   DAT_00610570);
  do
    {
      func_0x00401960 ((ulong) uParm1);
    LAB_0040277d:
      ;
      func_0x00401610 ("Usage: %s [OPTION]... [FILE]...\n", DAT_00610570);
      uVar3 = DAT_006104c0;
      func_0x004017d0 ("Concatenate FILE(s) to standard output.\n",
		       DAT_006104c0);
      func_0x004017d0
	("\nWith no FILE, or when FILE is -, read standard input.\n", uVar3);
      func_0x004017d0
	("\n  -A, --show-all           equivalent to -vET\n  -b, --number-nonblank    number nonempty output lines, overrides -n\n  -e                       equivalent to -vE\n  -E, --show-ends          display $ at end of each line\n  -n, --number             number all output lines\n  -s, --squeeze-blank      suppress repeated empty output lines\n",
	 uVar3);
      func_0x004017d0
	("  -t                       equivalent to -vT\n  -T, --show-tabs          display TAB characters as ^I\n  -u                       (ignored)\n  -v, --show-nonprinting   use ^ and M- notation, except for LFD and TAB\n",
	 uVar3);
      func_0x004017d0 ("      --help     display this help and exit\n",
		       uVar3);
      func_0x004017d0
	("      --version  output version information and exit\n", uVar3);
      func_0x00401610
	("\nExamples:\n  %s f - g  Output f\'s contents, then standard input, then g\'s contents.\n  %s        Copy standard input to standard output.\n",
	 DAT_00610570, DAT_00610570);
      local_88 = &DAT_0040d30c;
      local_80 = "test invocation";
      puVar5 = &DAT_0040d30c;
      local_78 = 0x40d373;
      local_70 = "Multi-call invocation";
      local_68 = "sha224sum";
      local_60 = "sha2 utilities";
      local_58 = "sha256sum";
      local_50 = "sha2 utilities";
      local_48 = "sha384sum";
      local_40 = "sha2 utilities";
      local_38 = "sha512sum";
      local_30 = "sha2 utilities";
      local_28 = 0;
      local_20 = 0;
      ppuVar4 = &local_88;
      do
	{
	  iVar1 = func_0x004018c0 (&DAT_0040d30e, puVar5);
	  if (iVar1 == 0)
	    break;
	  ppuVar4 = ppuVar4 + 2;
	  puVar5 = *ppuVar4;
	}
      while (puVar5 != (undefined *) 0x0);
      puVar5 = ppuVar4[1];
      if (puVar5 == (undefined *) 0x0)
	{
	  func_0x00401610 ("\n%s online help: <%s>\n", "GNU coreutils",
			   "https://www.gnu.org/software/coreutils/");
	  lVar2 = func_0x00401920 (5, 0);
	  if (lVar2 != 0)
	    {
	      iVar1 = func_0x00401840 (lVar2, &DAT_0040d394, 3);
	      if (iVar1 != 0)
		{
		  puVar5 = &DAT_0040d30e;
		  goto LAB_00402951;
		}
	    }
	  func_0x00401610 ("Full documentation at: <%s%s>\n",
			   "https://www.gnu.org/software/coreutils/",
			   &DAT_0040d30e);
	LAB_0040297a:
	  ;
	  puVar5 = &DAT_0040d30e;
	  uVar3 = 0x40d32c;
	}
      else
	{
	  func_0x00401610 ("\n%s online help: <%s>\n", "GNU coreutils",
			   "https://www.gnu.org/software/coreutils/");
	  lVar2 = func_0x00401920 (5, 0);
	  if (lVar2 != 0)
	    {
	      iVar1 = func_0x00401840 (lVar2, &DAT_0040d394, 3);
	      if (iVar1 != 0)
		{
		LAB_00402951:
		  ;
		  func_0x00401610
		    ("Report %s translation bugs to <https://translationproject.org/team/>\n",
		     &DAT_0040d30e);
		}
	    }
	  func_0x00401610 ("Full documentation at: <%s%s>\n",
			   "https://www.gnu.org/software/coreutils/",
			   &DAT_0040d30e);
	  uVar3 = 0x40e54f;
	  if (puVar5 == &DAT_0040d30e)
	    goto LAB_0040297a;
	}
      func_0x00401610
	("or available locally via: info \'(coreutils) %s%s\'\n", puVar5,
	 uVar3);
    }
  while (true);
}

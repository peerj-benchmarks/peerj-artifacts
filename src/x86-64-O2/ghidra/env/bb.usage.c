
#include "env.h"

long null_ARRAY_0060f52_0_8_;
long null_ARRAY_0060f52_8_8_;
long null_ARRAY_0060f74_0_8_;
long null_ARRAY_0060f74_16_8_;
long null_ARRAY_0060f74_24_8_;
long null_ARRAY_0060f74_32_8_;
long null_ARRAY_0060f74_40_8_;
long null_ARRAY_0060f74_48_8_;
long null_ARRAY_0060f74_8_8_;
long null_ARRAY_0060f78_0_4_;
long null_ARRAY_0060f78_16_8_;
long null_ARRAY_0060f78_4_4_;
long null_ARRAY_0060f78_8_4_;
long local_2f_1_1_;
long local_4_4_4_;
long local_5b_1_1_;
long local_9d_0_4_;
long local_a0_0_8_;
long local_a0_4_6_;
long local_a0_8_2_;
long local_a1_4_4_;
long local_a2_4_4_;
long local_a3_0_4_;
long local_a3_4_4_;
long local_a4_0_2_;
long local_a4_0_4_;
long local_a4_0_8_;
long local_a4_1_9_;
long local_a4_4_6_;
long local_a4_8_2_;
long local_a5_0_4_;
long local_a5_4_4_;
long DAT_00000010;
long DAT_0040ca00;
long DAT_0040ca02;
long DAT_0040ca88;
long DAT_0040cad3;
long DAT_0040d038;
long DAT_0040d03c;
long DAT_0040d040;
long DAT_0040d043;
long DAT_0040d045;
long DAT_0040d049;
long DAT_0040d04d;
long DAT_0040d5eb;
long DAT_0040d995;
long DAT_0040da99;
long DAT_0040da9f;
long DAT_0040dab1;
long DAT_0040dab2;
long DAT_0040dad0;
long DAT_0040dad4;
long DAT_0040db56;
long DAT_0060f0f8;
long DAT_0060f108;
long DAT_0060f118;
long DAT_0060f4c8;
long DAT_0060f530;
long DAT_0060f534;
long DAT_0060f538;
long DAT_0060f53c;
long DAT_0060f540;
long DAT_0060f580;
long DAT_0060f590;
long DAT_0060f5a0;
long DAT_0060f5a8;
long DAT_0060f5c0;
long DAT_0060f5c8;
long DAT_0060f610;
long DAT_0060f618;
long DAT_0060f620;
long DAT_0060f628;
long DAT_0060f7b8;
long DAT_0060f7c0;
long DAT_0060f7c8;
long DAT_0060f7d0;
long DAT_0060f7e0;
long _DYNAMIC;
long fde_0040e4e8;
long null_ARRAY_0040cf00;
long null_ARRAY_0040dde0;
long null_ARRAY_0040e010;
long null_ARRAY_0060f4e0;
long null_ARRAY_0060f520;
long null_ARRAY_0060f5e0;
long null_ARRAY_0060f640;
long null_ARRAY_0060f740;
long null_ARRAY_0060f780;
long PTR_DAT_0060f4c0;
long PTR_null_ARRAY_0060f518;
long register0x00000020;
long stack0x00000008;
void
FUN_00401e50 (uint uParm1)
{
  int iVar1;
  long lVar2;
  undefined8 uVar3;
  undefined **ppuVar4;
  undefined *puVar5;
  undefined *local_88;
  char *local_80;
  undefined8 local_78;
  char *local_70;
  char *local_68;
  char *local_60;
  char *local_58;
  char *local_50;
  char *local_48;
  char *local_40;
  char *local_38;
  char *local_30;
  undefined8 local_28;
  undefined8 local_20;

  if (uParm1 == 0)
    goto LAB_00401e7d;
  func_0x004016a0 (DAT_0060f5a0, "Try \'%s --help\' for more information.\n",
		   DAT_0060f628);
  do
    {
      func_0x00401830 ((ulong) uParm1);
    LAB_00401e7d:
      ;
      func_0x00401530
	("Usage: %s [OPTION]... [-] [NAME=VALUE]... [COMMAND [ARG]...]\n",
	 DAT_0060f628);
      uVar3 = DAT_0060f540;
      func_0x004016b0
	("Set each NAME to VALUE in the environment and run COMMAND.\n",
	 DAT_0060f540);
      func_0x004016b0
	("\nMandatory arguments to long options are mandatory for short options too.\n",
	 uVar3);
      func_0x004016b0
	("  -i, --ignore-environment  start with an empty environment\n  -0, --null           end each output line with NUL, not newline\n  -u, --unset=NAME     remove variable from the environment\n",
	 uVar3);
      func_0x004016b0
	("  -C, --chdir=DIR      change working directory to DIR\n", uVar3);
      func_0x004016b0 ("      --help     display this help and exit\n",
		       uVar3);
      func_0x004016b0
	("      --version  output version information and exit\n", uVar3);
      func_0x004016b0
	("\nA mere - implies -i.  If no COMMAND, print the resulting environment.\n",
	 uVar3);
      local_88 = &DAT_0040ca00;
      local_80 = "test invocation";
      puVar5 = &DAT_0040ca00;
      local_78 = 0x40ca67;
      local_70 = "Multi-call invocation";
      local_68 = "sha224sum";
      local_60 = "sha2 utilities";
      local_58 = "sha256sum";
      local_50 = "sha2 utilities";
      local_48 = "sha384sum";
      local_40 = "sha2 utilities";
      local_38 = "sha512sum";
      local_30 = "sha2 utilities";
      local_28 = 0;
      local_20 = 0;
      ppuVar4 = &local_88;
      do
	{
	  iVar1 = func_0x00401790 (&DAT_0040ca02, puVar5);
	  if (iVar1 == 0)
	    break;
	  ppuVar4 = ppuVar4 + 2;
	  puVar5 = *ppuVar4;
	}
      while (puVar5 != (undefined *) 0x0);
      puVar5 = ppuVar4[1];
      if (puVar5 == (undefined *) 0x0)
	{
	  func_0x00401530 ("\n%s online help: <%s>\n", "GNU coreutils",
			   "https://www.gnu.org/software/coreutils/");
	  lVar2 = func_0x004017f0 (5, 0);
	  if (lVar2 != 0)
	    {
	      iVar1 = func_0x00401710 (lVar2, &DAT_0040ca88, 3);
	      if (iVar1 != 0)
		{
		  puVar5 = &DAT_0040ca02;
		  goto LAB_00402049;
		}
	    }
	  func_0x00401530 ("Full documentation at: <%s%s>\n",
			   "https://www.gnu.org/software/coreutils/",
			   &DAT_0040ca02);
	LAB_00402072:
	  ;
	  puVar5 = &DAT_0040ca02;
	  uVar3 = 0x40ca20;
	}
      else
	{
	  func_0x00401530 ("\n%s online help: <%s>\n", "GNU coreutils",
			   "https://www.gnu.org/software/coreutils/");
	  lVar2 = func_0x004017f0 (5, 0);
	  if (lVar2 != 0)
	    {
	      iVar1 = func_0x00401710 (lVar2, &DAT_0040ca88, 3);
	      if (iVar1 != 0)
		{
		LAB_00402049:
		  ;
		  func_0x00401530
		    ("Report %s translation bugs to <https://translationproject.org/team/>\n",
		     &DAT_0040ca02);
		}
	    }
	  func_0x00401530 ("Full documentation at: <%s%s>\n",
			   "https://www.gnu.org/software/coreutils/",
			   &DAT_0040ca02);
	  uVar3 = 0x40dacf;
	  if (puVar5 == &DAT_0040ca02)
	    goto LAB_00402072;
	}
      func_0x00401530
	("or available locally via: info \'(coreutils) %s%s\'\n", puVar5,
	 uVar3);
    }
  while (true);
}

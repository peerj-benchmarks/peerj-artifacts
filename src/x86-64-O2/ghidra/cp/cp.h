typedef unsigned char   undefined;

typedef unsigned char    byte;
typedef unsigned char    dwfenc;
typedef unsigned int    dword;
typedef unsigned long    qword;
typedef unsigned char    undefined1;
typedef unsigned short    undefined2;
typedef unsigned int    undefined4;
typedef unsigned long    undefined6;
typedef unsigned long    undefined8;
typedef unsigned short    word;
#define unkbyte9   unsigned long long
#define unkbyte10   unsigned long long
#define unkbyte11   unsigned long long
#define unkbyte12   unsigned long long
#define unkbyte13   unsigned long long
#define unkbyte14   unsigned long long
#define unkbyte15   unsigned long long
#define unkbyte16   unsigned long long

#define unkuint9   unsigned long long
#define unkuint10   unsigned long long
#define unkuint11   unsigned long long
#define unkuint12   unsigned long long
#define unkuint13   unsigned long long
#define unkuint14   unsigned long long
#define unkuint15   unsigned long long
#define unkuint16   unsigned long long

#define unkint9   long long
#define unkint10   long long
#define unkint11   long long
#define unkint12   long long
#define unkint13   long long
#define unkint14   long long
#define unkint15   long long
#define unkint16   long long

#define unkfloat1   float
#define unkfloat2   float
#define unkfloat3   float
#define unkfloat5   double
#define unkfloat6   double
#define unkfloat7   double
#define unkfloat9   long double
#define unkfloat11   long double
#define unkfloat12   long double
#define unkfloat13   long double
#define unkfloat14   long double
#define unkfloat15   long double
#define unkfloat16   long double

#define BADSPACEBASE   void
#define code   void

#define ulong long
#define true 1
#define bool int
#define false 0
#define float10 int
#define ushort int
#define undefined7 long
#define undefined2 long
#define uint5 long
#define uint6 long

// Add definition for uint
typedef unsigned int    uint;

#define sbyte char

extern imperfection_wrapper(void);
typedef struct eh_frame_hdr eh_frame_hdr, *Peh_frame_hdr;

struct eh_frame_hdr {
    byte eh_frame_hdr_version; // Exception Handler Frame Header Version
    dwfenc eh_frame_pointer_encoding; // Exception Handler Frame Pointer Encoding
    dwfenc eh_frame_desc_entry_count_encoding; // Encoding of # of Exception Handler FDEs
    dwfenc eh_frame_table_encoding; // Exception Handler Table Encoding
};

typedef struct fde_table_entry fde_table_entry, *Pfde_table_entry;

struct fde_table_entry {
    dword initial_loc; // Initial Location
    dword data_loc; // Data location
};

typedef enum Elf64_DynTag {
    DT_AUDIT=1879047932,
    DT_AUXILIARY=2147483645,
    DT_BIND_NOW=24,
    DT_CHECKSUM=1879047672,
    DT_CONFIG=1879047930,
    DT_DEBUG=21,
    DT_DEPAUDIT=1879047931,
    DT_ENCODING=32,
    DT_FEATURE_1=1879047676,
    DT_FILTER=2147483647,
    DT_FINI=13,
    DT_FINI_ARRAY=26,
    DT_FINI_ARRAYSZ=28,
    DT_FLAGS=30,
    DT_FLAGS_1=1879048187,
    DT_GNU_CONFLICT=1879047928,
    DT_GNU_CONFLICTSZ=1879047670,
    DT_GNU_HASH=1879047925,
    DT_GNU_LIBLIST=1879047929,
    DT_GNU_LIBLISTSZ=1879047671,
    DT_GNU_PRELINKED=1879047669,
    DT_HASH=4,
    DT_INIT=12,
    DT_INIT_ARRAY=25,
    DT_INIT_ARRAYSZ=27,
    DT_JMPREL=23,
    DT_MOVEENT=1879047674,
    DT_MOVESZ=1879047675,
    DT_MOVETAB=1879047934,
    DT_NEEDED=1,
    DT_NULL=0,
    DT_PLTGOT=3,
    DT_PLTPAD=1879047933,
    DT_PLTPADSZ=1879047673,
    DT_PLTREL=20,
    DT_PLTRELSZ=2,
    DT_POSFLAG_1=1879047677,
    DT_PREINIT_ARRAYSZ=33,
    DT_REL=17,
    DT_RELA=7,
    DT_RELACOUNT=1879048185,
    DT_RELAENT=9,
    DT_RELASZ=8,
    DT_RELCOUNT=1879048186,
    DT_RELENT=19,
    DT_RELSZ=18,
    DT_RPATH=15,
    DT_RUNPATH=29,
    DT_SONAME=14,
    DT_STRSZ=10,
    DT_STRTAB=5,
    DT_SYMBOLIC=16,
    DT_SYMENT=11,
    DT_SYMINENT=1879047679,
    DT_SYMINFO=1879047935,
    DT_SYMINSZ=1879047678,
    DT_SYMTAB=6,
    DT_TEXTREL=22,
    DT_TLSDESC_GOT=1879047927,
    DT_TLSDESC_PLT=1879047926,
    DT_VERDEF=1879048188,
    DT_VERDEFNUM=1879048189,
    DT_VERNEED=1879048190,
    DT_VERNEEDNUM=1879048191,
    DT_VERSYM=1879048176
} Elf64_DynTag;

typedef enum Elf_ProgramHeaderType {
    PT_DYNAMIC=2,
    PT_GNU_EH_FRAME=1685382480,
    PT_GNU_RELRO=1685382482,
    PT_GNU_STACK=1685382481,
    PT_INTERP=3,
    PT_LOAD=1,
    PT_NOTE=4,
    PT_NULL=0,
    PT_PHDR=6,
    PT_SHLIB=5,
    PT_TLS=7
} Elf_ProgramHeaderType;

typedef struct Elf64_Dyn Elf64_Dyn, *PElf64_Dyn;

struct Elf64_Dyn {
    enum Elf64_DynTag d_tag;
    qword d_val;
};

typedef struct Elf64_Shdr Elf64_Shdr, *PElf64_Shdr;

typedef enum Elf_SectionHeaderType {
    SHT_CHECKSUM=1879048184,
    SHT_DYNAMIC=6,
    SHT_DYNSYM=11,
    SHT_FINI_ARRAY=15,
    SHT_GNU_ATTRIBUTES=1879048181,
    SHT_GNU_HASH=1879048182,
    SHT_GNU_LIBLIST=1879048183,
    SHT_GNU_verdef=1879048189,
    SHT_GNU_verneed=1879048190,
    SHT_GNU_versym=1879048191,
    SHT_GROUP=17,
    SHT_HASH=5,
    SHT_INIT_ARRAY=14,
    SHT_NOBITS=8,
    SHT_NOTE=7,
    SHT_NULL=0,
    SHT_PREINIT_ARRAY=16,
    SHT_PROGBITS=1,
    SHT_REL=9,
    SHT_RELA=4,
    SHT_SHLIB=10,
    SHT_STRTAB=3,
    SHT_SUNW_COMDAT=1879048187,
    SHT_SUNW_move=1879048186,
    SHT_SUNW_syminfo=1879048188,
    SHT_SYMTAB=2,
    SHT_SYMTAB_SHNDX=18
} Elf_SectionHeaderType;

struct Elf64_Shdr {
    dword sh_name;
    enum Elf_SectionHeaderType sh_type;
    qword sh_flags;
    qword sh_addr;
    qword sh_offset;
    qword sh_size;
    dword sh_link;
    dword sh_info;
    qword sh_addralign;
    qword sh_entsize;
};

typedef struct Elf64_Phdr Elf64_Phdr, *PElf64_Phdr;

struct Elf64_Phdr {
    enum Elf_ProgramHeaderType p_type;
    dword p_flags;
    qword p_offset;
    qword p_vaddr;
    qword p_paddr;
    qword p_filesz;
    qword p_memsz;
    qword p_align;
};

typedef struct Elf64_Rela Elf64_Rela, *PElf64_Rela;

struct Elf64_Rela {
    qword r_offset; // location to apply the relocation action
    qword r_info; // the symbol table index and the type of relocation
    qword r_addend; // a constant addend used to compute the relocatable field value
};

typedef struct Elf64_Sym Elf64_Sym, *PElf64_Sym;

struct Elf64_Sym {
    dword st_name;
    byte st_info;
    byte st_other;
    word st_shndx;
    qword st_value;
    qword st_size;
};

typedef struct Elf64_Ehdr Elf64_Ehdr, *PElf64_Ehdr;

struct Elf64_Ehdr {
    byte e_ident_magic_num;
    char e_ident_magic_str[3];
    byte e_ident_class;
    byte e_ident_data;
    byte e_ident_version;
    byte e_ident_pad[9];
    word e_type;
    word e_machine;
    dword e_version;
    qword e_entry;
    qword e_phoff;
    qword e_shoff;
    dword e_flags;
    word e_ehsize;
    word e_phentsize;
    word e_phnum;
    word e_shentsize;
    word e_shnum;
    word e_shstrndx;
};




undefined8 _DT_INIT(void);
void FUN_00402420(void);
void thunk_FUN_00624070(void);
void thunk_FUN_00624198(void);
void FUN_00402c20(void);
void thunk_FUN_006243e8(void);
void FUN_00402c40(void);
ulong FUN_00402c70(uint uParm1,undefined8 *puParm2);
void entry(void);
void FUN_00403340(undefined8 *puParm1);
void FUN_00403370(void);
void FUN_004033f0(void);
void FUN_00403470(void);
ulong FUN_004034b0(undefined8 uParm1,long lParm2,undefined *puParm3);
void FUN_00403530(undefined8 uParm1,undefined8 uParm2,char cParm3);
undefined8 FUN_00403680(undefined8 uParm1,long lParm2,long lParm3,undefined8 *puParm4,char *pcParm5,long lParm6);
undefined8 FUN_00403ba0(undefined8 uParm1,long lParm2,long lParm3,long lParm4);
void FUN_00403db0(uint uParm1);
ulong FUN_00404080(int iParm1,undefined8 *puParm2,long lParm3,char cParm4,uint *puParm5);
ulong FUN_004044e0(undefined8 uParm1,undefined8 uParm2);
void FUN_00404530(undefined8 uParm1,undefined8 uParm2,long lParm3);
undefined8 FUN_004045a0(uint uParm1,ulong uParm2);
ulong FUN_00404640(undefined8 uParm1,undefined8 uParm2,undefined8 uParm3);
void FUN_00404690(int iParm1,undefined8 uParm2,uint uParm3);
void FUN_004046b0(long lParm1,undefined8 uParm2,uint *puParm3);
ulong FUN_00404790(undefined8 uParm1,undefined8 uParm2,byte bParm3,uint uParm4,char cParm5);
undefined8 FUN_00404850(ulong uParm1,undefined8 uParm2,char cParm3,long lParm4);
undefined8 FUN_00404900(uint param_1,uint param_2,long param_3,ulong param_4,ulong param_5,byte param_6,undefined8 param_7,undefined8 param_8,ulong param_9,long *param_10,byte *param_11);
ulong FUN_00404d50(undefined8 uParm1,undefined8 uParm2,undefined8 uParm3,byte bParm4,long lParm5);
undefined8 FUN_00404e70(undefined8 uParm1,undefined8 uParm2,undefined8 uParm3,long lParm4);
void FUN_00404f00(long lParm1);
void FUN_00404f30(long lParm1);
void FUN_00404f60(long lParm1);
ulong FUN_00404f90(long lParm1);
ulong FUN_00404fc0(long lParm1,undefined8 uParm2,uint uParm3,long lParm4,char cParm5,long lParm6);
ulong FUN_004051b0(void);
ulong FUN_004051f0(char *param_1,char *param_2,byte param_3,long **param_4,long **param_5,long **param_6,uint param_7,byte *param_8,byte *param_9,undefined *param_10);
void FUN_00409240(undefined8 uParm1,undefined8 uParm2,ulong uParm3,uint *puParm4,undefined8 uParm5,undefined8 uParm6);
ulong FUN_004093b0(ulong *puParm1,ulong uParm2);
ulong FUN_004093c0(long *plParm1,long *plParm2);
void FUN_004093e0(long lParm1);
void FUN_00409400(undefined8 uParm1,undefined8 uParm2);
undefined8 FUN_00409450(undefined8 uParm1,undefined8 uParm2);
undefined8 FUN_00409490(undefined8 uParm1,undefined8 uParm2,undefined8 uParm3);
void FUN_00409510(void);
void FUN_00409550(undefined4 uParm1,undefined4 *puParm2);
ulong FUN_00409580(uint *puParm1);
void FUN_00409850(undefined8 uParm1,uint *puParm2);
long FUN_00409870(long lParm1,long lParm2);
void FUN_004098d0(undefined8 uParm1,undefined8 *puParm2);
ulong FUN_004098f0(undefined4 uParm1,undefined8 uParm2,ulong uParm3,undefined8 uParm4,undefined4 uParm5,byte bParm6);
ulong FUN_00409a20(undefined8 uParm1,ulong uParm2,undefined8 uParm3,byte bParm4);
ulong FUN_00409b30(undefined8 uParm1,undefined8 uParm2,undefined8 uParm3);
ulong FUN_00409bb0(undefined8 uParm1);
long FUN_00409bf0(undefined8 uParm1,ulong uParm2);
void FUN_00409d10(void);
long FUN_00409d20(undefined8 uParm1,long *plParm2,long lParm3,long lParm4);
void FUN_00409e50(undefined8 uParm1,undefined8 uParm2,long lParm3);
void FUN_00409ea0(long *plParm1,long lParm2,long lParm3);
long FUN_00409f70(undefined8 uParm1,undefined8 uParm2,undefined8 uParm3,undefined8 uParm4,undefined8 uParm5,code *pcParm6);
void FUN_00409fe0(long lParm1,long lParm2);
void FUN_0040a0d0(char *pcParm1);
long FUN_0040a130(long lParm1,int iParm2,char cParm3);
void FUN_0040a630(undefined8 uParm1,undefined8 uParm2);
ulong FUN_0040a640(undefined8 uParm1,undefined8 uParm2);
void FUN_0040a670(undefined8 uParm1,undefined8 uParm2);
undefined8 FUN_0040a690(undefined8 uParm1,char *pcParm2);
ulong FUN_0040a6f0(ulong uParm1,ulong uParm2,ulong uParm3);
char * FUN_0040a770(char *pcParm1,uint uParm2);
void FUN_0040ad70(undefined8 uParm1);
void FUN_0040ad80(void);
void FUN_0040ae40(void);
long FUN_0040aed0(void);
void FUN_0040af80(void);
ulong FUN_0040afa0(char *pcParm1);
undefined * FUN_0040b000(undefined8 uParm1);
char * thunk_FUN_0040b08c(char *pcParm1);
char * FUN_0040b08c(char *pcParm1);
void FUN_0040b0d0(long lParm1);
undefined FUN_0040b100(char *pcParm1);;
void FUN_0040b140(void);
void FUN_0040b150(undefined8 param_1,byte param_2,ulong param_3);
void FUN_0040b1a0(long lParm1,undefined8 uParm2,undefined8 *puParm3);
ulong FUN_0040b230(long lParm1,undefined8 uParm2,undefined8 *puParm3);
void FUN_0040b270(ulong uParm1,undefined *puParm2);
void FUN_0040b440(void);
long FUN_0040b460(long lParm1,char *pcParm2,undefined8 *puParm3);
long FUN_0040b560(uint uParm1,long lParm2,long lParm3);
ulong FUN_0040b5e0(ulong uParm1);
ulong FUN_0040b680(ulong uParm1,ulong uParm2);
undefined FUN_0040b690(long lParm1,long lParm2);;
long FUN_0040b6a0(long *plParm1,undefined8 uParm2);
long FUN_0040b6d0(long lParm1,long lParm2,long **pplParm3,char cParm4);
ulong FUN_0040b7d0(float **ppfParm1,undefined8 uParm2,uint uParm3);
undefined8 FUN_0040b860(long lParm1,long **pplParm2,char cParm3);
long FUN_0040b9b0(long lParm1,long lParm2);
long * FUN_0040ba10(ulong uParm1,undefined1 *puParm2,code *pcParm3,code *pcParm4,long lParm5);
void FUN_0040bbc0(long **pplParm1);
ulong FUN_0040bc80(long *plParm1,ulong uParm2);
undefined8 FUN_0040be80(long lParm1,long lParm2,long *plParm3);
undefined8 FUN_0040c0f0(undefined8 uParm1,undefined8 uParm2);
long FUN_0040c130(long lParm1,undefined8 uParm2);
ulong FUN_0040c330(undefined8 *puParm1,ulong uParm2);
ulong FUN_0040c360(long lParm1,ulong uParm2);
undefined8 FUN_0040c370(undefined8 *puParm1,undefined8 *puParm2);
ulong FUN_0040c3a0(undefined8 *puParm1,undefined8 *puParm2);
void FUN_0040c3e0(undefined8 *puParm1);
void FUN_0040c400(long lParm1);
undefined8 FUN_0040c4a0(undefined8 uParm1,undefined8 uParm2,undefined8 uParm3,uint uParm4,uint uParm5);
void FUN_0040c4e0(undefined8 uParm1,uint uParm2,undefined4 uParm3);
undefined8 * FUN_0040c520(undefined8 *puParm1,int iParm2);
undefined * FUN_0040c590(char *pcParm1,int iParm2);
ulong FUN_0040c660(undefined *param_1,ulong param_2,long param_3,ulong param_4,uint param_5,ulong param_6,long param_7,char *param_8,char *param_9);
undefined1 * FUN_0040d560(uint uParm1,undefined8 uParm2,undefined8 uParm3,uint *puParm4);
void FUN_0040d710(uint uParm1,undefined8 uParm2,undefined8 uParm3);
void FUN_0040d740(ulong uParm1,undefined8 uParm2);
void FUN_0040d750(undefined8 uParm1,undefined8 uParm2,byte bParm3);
void FUN_0040d7f0(undefined8 uParm1);
void FUN_0040d810(uint uParm1,undefined8 uParm2,undefined8 uParm3);
void FUN_0040d8a0(undefined8 uParm1,undefined8 uParm2);
void FUN_0040d8c0(undefined8 uParm1);
ulong FUN_0040d8e0(uint uParm1,long lParm2,uint uParm3,long lParm4,uint uParm5);
long FUN_0040dcb0(uint uParm1,undefined8 uParm2,ulong uParm3);
ulong FUN_0040dd20(undefined8 uParm1,undefined8 uParm2);
void FUN_0040de50(undefined8 *puParm1,undefined8 *puParm2);
long FUN_0040de60(long lParm1,uint uParm2);
undefined8 FUN_0040e190(undefined8 uParm1,ulong uParm2);
ulong FUN_0040e200(long lParm1,int iParm2,undefined8 uParm3,code *pcParm4,ulong uParm5);
ulong FUN_0040e380(uint uParm1);
ulong FUN_0040e3c0(ulong *puParm1,ulong uParm2);
ulong FUN_0040e3d0(ulong *puParm1,ulong *puParm2);
ulong FUN_0040e3e0(undefined8 uParm1,undefined8 *puParm2,long lParm3,ulong uParm4);
undefined8 FUN_0040eb80(long lParm1,long *plParm2);
ulong FUN_0040ec60(undefined8 *puParm1);
ulong FUN_0040ed20(uint uParm1,long lParm2,undefined8 *puParm3);
void FUN_0040f0f0(undefined8 uParm1,undefined8 uParm2);
ulong FUN_0040f100(undefined8 uParm1,undefined8 *puParm2);
long FUN_0040f2f0(undefined8 uParm1,long lParm2,undefined8 uParm3,undefined8 uParm4,long *plParm5,undefined8 uParm6);
void FUN_0040f550(void);
void FUN_0040f5c0(void);
ulong FUN_0040f650(void);
void FUN_0040f690(long lParm1);
long FUN_0040f6b0(long lParm1,long lParm2);
void FUN_0040f6f0(undefined8 uParm1,undefined8 uParm2);
void FUN_0040f720(undefined8 uParm1);
void FUN_0040f740(void);
long FUN_0040f770(void);
ulong FUN_0040f7a0(void);
undefined8 FUN_0040f7e0(ulong uParm1,ulong uParm2);
void FUN_0040f830(undefined8 uParm1);
void FUN_0040f870(undefined8 uParm1,uint uParm2,undefined8 uParm3,undefined8 uParm4);
void FUN_0040f8e0(void);
void FUN_0040f920(undefined4 uParm1,undefined4 uParm2,undefined4 uParm3,undefined4 uParm4,undefined4 uParm5,undefined4 uParm6,undefined4 uParm7,undefined4 uParm8,ulong param_1,ulong param_2,undefined8 param_3,undefined8 param_4,undefined8 param_5,undefined8 param_6);
ulong FUN_0040fa00(ulong param_1,undefined8 param_2,ulong param_3);
undefined8 FUN_0040fb40(ulong uParm1);
ulong FUN_0040fbb0(long lParm1);
undefined8 FUN_0040fc40(void);
void FUN_0040fc50(void);
undefined8 FUN_0040fc60(ulong uParm1,long lParm2,long lParm3,ulong uParm4);
long FUN_0040fd20(long lParm1,ulong uParm2);
void FUN_004101a0(long lParm1,int *piParm2);
ulong FUN_00410280(int param_1,undefined8 *param_2,char *param_3,long *param_4,int *param_5,int param_6,int *param_7,int param_8,undefined8 param_9);
ulong FUN_00410800(uint param_1,undefined8 *param_2,char *param_3,long param_4,undefined8 param_5,ulong param_6,uint *param_7,int param_8);
void FUN_00410d50(void);
void FUN_00410db0(void);
ulong FUN_00410dd0(long lParm1,uint uParm2,uint uParm3);
ulong FUN_00410ec0(ulong uParm1,char *pcParm2,uint uParm3,long lParm4,uint uParm5);
undefined8 FUN_00411150(long lParm1,long lParm2);
void FUN_004111d0(void);
ulong FUN_004111f0(uint *puParm1,byte *pbParm2,long lParm3);
ulong FUN_00411260(long lParm1,ulong uParm2);
undefined8 FUN_00411350(long lParm1,ulong uParm2);
undefined8 FUN_004113b0(long lParm1,uint uParm2,long lParm3);
ulong FUN_00411460(uint param_1,undefined8 param_2,uint param_3,ulong param_4);
undefined8 FUN_00411580(long lParm1,undefined8 uParm2,undefined8 uParm3);
void FUN_004115f0(long lParm1,long lParm2);
ulong FUN_00411630(long lParm1,long lParm2);
void FUN_00411a80(void);
ulong FUN_00411a90(long lParm1);
ulong FUN_00411b30(long lParm1,long lParm2);
undefined8 FUN_00411b80(undefined8 uParm1,ulong uParm2,long lParm3);
undefined8 FUN_00411bf0(long lParm1);
undefined8 FUN_00411cc0(uint uParm1,long lParm2,ulong uParm3);
ulong FUN_00411dc0(undefined8 uParm1,undefined8 uParm2,undefined8 uParm3);
undefined8 FUN_00411e90(undefined8 uParm1,undefined8 uParm2,undefined4 uParm3,undefined4 *puParm4);
void FUN_00411ec0(undefined8 uParm1,uint uParm2,uint uParm3);
ulong FUN_00411ee0(uint *puParm1,undefined8 uParm2,uint uParm3);
void FUN_00411f10(undefined8 uParm1,undefined8 uParm2);
undefined *FUN_00411f30(uint uParm1,undefined8 uParm2,undefined *puParm3,ulong uParm4,undefined1 *puParm5,code *pcParm6);
ulong FUN_004120f0(ulong uParm1,char cParm2);
ulong FUN_00412150(undefined8 uParm1);
undefined8 FUN_004121c0(void);
void FUN_004121d0(undefined8 *puParm1);
ulong FUN_00412210(ulong uParm1);
ulong FUN_004122b0(char *pcParm1,ulong uParm2);
char * FUN_004122e0(void);
long * FUN_00412630(void);
ulong FUN_00412670(undefined8 *puParm1,ulong uParm2);
ulong FUN_00412750(undefined8 *puParm1);
void FUN_00412790(long lParm1);
long * FUN_004127e0(ulong uParm1,ulong uParm2);
void FUN_00412a70(long *plParm1,ulong uParm2,ulong uParm3);
undefined8 FUN_00412be0(long *plParm1);
void FUN_00412c30(long *plParm1,long *plParm2);
void FUN_00412ea0(ulong *puParm1);
void FUN_004130e0(undefined8 uParm1);
undefined8 FUN_004130f0(long lParm1,uint uParm2,uint uParm3);
void FUN_00413210(undefined8 uParm1,undefined8 uParm2);
void FUN_00413230(undefined8 uParm1);
void FUN_004132c0(void);
undefined8 FUN_00413380(uint uParm1,long lParm2,undefined8 uParm3,undefined8 uParm4);
undefined4 * FUN_004133f0(ulong uParm1,uint *puParm2,ulong uParm3,uint *puParm4,long *plParm5);
ulong FUN_00413550(void);
uint * FUN_004137c0(ulong uParm1,long lParm2,ulong uParm3,uint *puParm4,ulong *puParm5);
char * FUN_00413d80(int iParm1,long lParm2,long lParm3,long lParm4,uint uParm5);
void FUN_00414320(uint param_1);
ulong FUN_004144a0(void);
void FUN_004146c0(undefined8 uParm1,uint uParm2);
undefined8 *FUN_00414830(undefined8 *puParm1,undefined8 *puParm2,undefined8 *puParm3,undefined8 uParm4);
long FUN_00419e00(undefined8 uParm1,undefined8 uParm2);
double FUN_00419ea0(int *piParm1);
void FUN_00419ee0(uint *param_1);
undefined8 FUN_00419f60(uint *puParm1,ulong *puParm2);
undefined8 FUN_0041a180(byte *pbParm1,ulong *puParm2,byte **ppbParm3);
ulong FUN_0041add0(void);
ulong FUN_0041ae00(void);
void FUN_0041ae40(void);
undefined8 _DT_FINI(void);
undefined FUN_00624000();
undefined FUN_00624008();
undefined FUN_00624010();
undefined FUN_00624018();
undefined FUN_00624020();
undefined FUN_00624028();
undefined FUN_00624030();
undefined FUN_00624038();
undefined FUN_00624040();
undefined FUN_00624048();
undefined FUN_00624050();
undefined FUN_00624058();
undefined FUN_00624060();
undefined FUN_00624068();
undefined FUN_00624070();
undefined FUN_00624078();
undefined FUN_00624080();
undefined FUN_00624088();
undefined FUN_00624090();
undefined FUN_00624098();
undefined FUN_006240a0();
undefined FUN_006240a8();
undefined FUN_006240b0();
undefined FUN_006240b8();
undefined FUN_006240c0();
undefined FUN_006240c8();
undefined FUN_006240d0();
undefined FUN_006240d8();
undefined FUN_006240e0();
undefined FUN_006240e8();
undefined FUN_006240f0();
undefined FUN_006240f8();
undefined FUN_00624100();
undefined FUN_00624108();
undefined FUN_00624110();
undefined FUN_00624118();
undefined FUN_00624120();
undefined FUN_00624128();
undefined FUN_00624130();
undefined FUN_00624138();
undefined FUN_00624140();
undefined FUN_00624148();
undefined FUN_00624150();
undefined FUN_00624158();
undefined FUN_00624160();
undefined FUN_00624168();
undefined FUN_00624170();
undefined FUN_00624178();
undefined FUN_00624180();
undefined FUN_00624188();
undefined FUN_00624190();
undefined FUN_00624198();
undefined FUN_006241a0();
undefined FUN_006241a8();
undefined FUN_006241b0();
undefined FUN_006241b8();
undefined FUN_006241c0();
undefined FUN_006241c8();
undefined FUN_006241d0();
undefined FUN_006241d8();
undefined FUN_006241e0();
undefined FUN_006241e8();
undefined FUN_006241f0();
undefined FUN_006241f8();
undefined FUN_00624200();
undefined FUN_00624208();
undefined FUN_00624210();
undefined FUN_00624218();
undefined FUN_00624220();
undefined FUN_00624228();
undefined FUN_00624230();
undefined FUN_00624238();
undefined FUN_00624240();
undefined FUN_00624248();
undefined FUN_00624250();
undefined FUN_00624258();
undefined FUN_00624260();
undefined FUN_00624268();
undefined FUN_00624270();
undefined FUN_00624278();
undefined FUN_00624280();
undefined FUN_00624288();
undefined FUN_00624290();
undefined FUN_00624298();
undefined FUN_006242a0();
undefined FUN_006242a8();
undefined FUN_006242b0();
undefined FUN_006242b8();
undefined FUN_006242c0();
undefined FUN_006242c8();
undefined FUN_006242d0();
undefined FUN_006242d8();
undefined FUN_006242e0();
undefined FUN_006242e8();
undefined FUN_006242f0();
undefined FUN_006242f8();
undefined FUN_00624300();
undefined FUN_00624308();
undefined FUN_00624310();
undefined FUN_00624318();
undefined FUN_00624320();
undefined FUN_00624328();
undefined FUN_00624330();
undefined FUN_00624338();
undefined FUN_00624340();
undefined FUN_00624348();
undefined FUN_00624350();
undefined FUN_00624358();
undefined FUN_00624360();
undefined FUN_00624368();
undefined FUN_00624370();
undefined FUN_00624378();
undefined FUN_00624380();
undefined FUN_00624388();
undefined FUN_00624390();
undefined FUN_00624398();
undefined FUN_006243a0();
undefined FUN_006243a8();
undefined FUN_006243b0();
undefined FUN_006243b8();
undefined FUN_006243c0();
undefined FUN_006243c8();
undefined FUN_006243d0();
undefined FUN_006243d8();
undefined FUN_006243e0();
undefined FUN_006243e8();


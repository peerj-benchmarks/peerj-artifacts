
#include "who.h"

long DAT_0061142_1_1_;
long null_ARRAY_0061148_0_8_;
long null_ARRAY_0061148_8_8_;
long null_ARRAY_006115e_0_4_;
long null_ARRAY_0061174_0_8_;
long null_ARRAY_0061174_16_8_;
long null_ARRAY_0061174_24_8_;
long null_ARRAY_0061174_32_8_;
long null_ARRAY_0061174_40_8_;
long null_ARRAY_0061174_48_8_;
long null_ARRAY_0061174_8_8_;
long null_ARRAY_0061178_0_4_;
long null_ARRAY_0061178_16_8_;
long null_ARRAY_0061178_4_4_;
long null_ARRAY_0061178_8_4_;
long local_2f_1_1_;
long local_4_4_4_;
long local_5b_1_1_;
long local_9d_0_4_;
long local_a0_0_8_;
long local_a0_4_6_;
long local_a0_8_2_;
long local_a1_4_4_;
long local_a2_4_4_;
long local_a3_0_4_;
long local_a3_4_4_;
long local_a4_0_2_;
long local_a4_0_4_;
long local_a4_0_8_;
long local_a4_1_9_;
long local_a4_4_6_;
long local_a4_8_2_;
long local_a5_0_4_;
long local_a5_4_4_;
long DAT_00000010;
long DAT_0040dd40;
long DAT_0040dd58;
long DAT_0040dd62;
long DAT_0040dd66;
long DAT_0040dd68;
long DAT_0040dda4;
long DAT_0040dda9;
long DAT_0040ddb5;
long DAT_0040ddba;
long DAT_0040ddcd;
long DAT_0040ddd2;
long DAT_0040ddd7;
long DAT_0040dded;
long DAT_0040de08;
long DAT_0040de42;
long DAT_0040de44;
long DAT_0040dee8;
long DAT_0040e818;
long DAT_0040e860;
long DAT_0040e864;
long DAT_0040e868;
long DAT_0040e86b;
long DAT_0040e86d;
long DAT_0040e871;
long DAT_0040e875;
long DAT_0040ee2b;
long DAT_0040f1d5;
long DAT_0040f2d9;
long DAT_0040f2df;
long DAT_0040f2f1;
long DAT_0040f2f2;
long DAT_0040f310;
long DAT_0040f38e;
long DAT_00611000;
long DAT_00611010;
long DAT_00611020;
long DAT_00611420;
long DAT_00611428;
long DAT_00611438;
long DAT_00611490;
long DAT_00611494;
long DAT_00611498;
long DAT_0061149c;
long DAT_006114c0;
long DAT_006114d0;
long DAT_006114e0;
long DAT_006114e8;
long DAT_00611500;
long DAT_00611508;
long DAT_00611560;
long DAT_00611568;
long DAT_00611570;
long DAT_006115a1;
long DAT_006115a8;
long DAT_006115b0;
long DAT_006115b8;
long DAT_006115c0;
long DAT_006115c8;
long DAT_006115c9;
long DAT_006115ca;
long DAT_006115cb;
long DAT_006115cc;
long DAT_006115cd;
long DAT_006115ce;
long DAT_006115cf;
long DAT_006115d0;
long DAT_006115d1;
long DAT_006115d2;
long DAT_006115d3;
long DAT_006115d4;
long DAT_006115d5;
long DAT_006115d6;
long DAT_00611610;
long DAT_00611618;
long DAT_00611620;
long DAT_00611628;
long DAT_006117f8;
long DAT_00611800;
long DAT_00611808;
long DAT_00611818;
long fde_0040fd80;
long null_ARRAY_0040e5c0;
long null_ARRAY_0040f620;
long null_ARRAY_0040f850;
long null_ARRAY_00611440;
long null_ARRAY_00611480;
long null_ARRAY_00611520;
long null_ARRAY_00611580;
long null_ARRAY_006115e0;
long null_ARRAY_00611640;
long null_ARRAY_00611740;
long null_ARRAY_00611780;
long null_ARRAY_006117c0;
long PTR_DAT_00611430;
long PTR_null_ARRAY_00611478;
long PTR_null_ARRAY_006114a0;
long stack0x00000008;
void
FUN_00402e10 (uint uParm1)
{
  int iVar1;
  long lVar2;
  undefined8 uVar3;
  undefined **ppuVar4;
  undefined *puVar5;
  undefined *local_88;
  char *local_80;
  undefined8 local_78;
  char *local_70;
  char *local_68;
  char *local_60;
  char *local_58;
  char *local_50;
  char *local_48;
  char *local_40;
  char *local_38;
  char *local_30;
  undefined8 local_28;
  undefined8 local_20;

  if (uParm1 == 0)
    goto LAB_00402e3d;
  func_0x00401900 (DAT_006114e0, "Try \'%s --help\' for more information.\n",
		   DAT_00611628);
  do
    {
      func_0x00401b30 ((ulong) uParm1);
    LAB_00402e3d:
      ;
      func_0x00401770 ("Usage: %s [OPTION]... [ FILE | ARG1 ARG2 ]\n",
		       DAT_00611628);
      uVar3 = DAT_006114c0;
      func_0x00401920
	("Print information about users who are currently logged in.\n",
	 DAT_006114c0);
      func_0x00401920
	("\n  -a, --all         same as -b -d --login -p -r -t -T -u\n  -b, --boot        time of last system boot\n  -d, --dead        print dead processes\n  -H, --heading     print line of column headings\n",
	 uVar3);
      func_0x00401920 ("  -l, --login       print system login processes\n",
		       uVar3);
      func_0x00401920
	("      --lookup      attempt to canonicalize hostnames via DNS\n  -m                only hostname and user associated with stdin\n  -p, --process     print active processes spawned by init\n",
	 uVar3);
      func_0x00401920
	("  -q, --count       all login names and number of users logged on\n  -r, --runlevel    print current runlevel\n  -s, --short       print only name, line, and time (default)\n  -t, --time        print last system clock change\n",
	 uVar3);
      func_0x00401920
	("  -T, -w, --mesg    add user\'s message status as +, - or ?\n  -u, --users       list users logged in\n      --message     same as -T\n      --writable    same as -T\n",
	 uVar3);
      func_0x00401920 ("      --help     display this help and exit\n",
		       uVar3);
      func_0x00401920
	("      --version  output version information and exit\n", uVar3);
      func_0x00401770
	("\nIf FILE is not specified, use %s.  %s as FILE is common.\nIf ARG1 ARG2 given, -m presumed: \'am i\' or \'mom likes\' are usual.\n",
	 "/dev/null/utmp", "/dev/null/wtmp");
      local_88 = &DAT_0040de42;
      local_80 = "test invocation";
      puVar5 = &DAT_0040de42;
      local_78 = 0x40dec7;
      local_70 = "Multi-call invocation";
      local_68 = "sha224sum";
      local_60 = "sha2 utilities";
      local_58 = "sha256sum";
      local_50 = "sha2 utilities";
      local_48 = "sha384sum";
      local_40 = "sha2 utilities";
      local_38 = "sha512sum";
      local_30 = "sha2 utilities";
      local_28 = 0;
      local_20 = 0;
      ppuVar4 = &local_88;
      do
	{
	  iVar1 = func_0x00401a50 (&DAT_0040de44, puVar5);
	  if (iVar1 == 0)
	    break;
	  ppuVar4 = ppuVar4 + 2;
	  puVar5 = *ppuVar4;
	}
      while (puVar5 != (undefined *) 0x0);
      puVar5 = ppuVar4[1];
      if (puVar5 == (undefined *) 0x0)
	{
	  func_0x00401770 ("\n%s online help: <%s>\n", "GNU coreutils",
			   "https://www.gnu.org/software/coreutils/");
	  lVar2 = func_0x00401ac0 (5, 0);
	  if (lVar2 != 0)
	    {
	      iVar1 = func_0x00401970 (lVar2, &DAT_0040dee8, 3);
	      if (iVar1 != 0)
		{
		  puVar5 = &DAT_0040de44;
		  goto LAB_00403029;
		}
	    }
	  func_0x00401770 ("Full documentation at: <%s%s>\n",
			   "https://www.gnu.org/software/coreutils/",
			   &DAT_0040de44);
	LAB_00403052:
	  ;
	  puVar5 = &DAT_0040de44;
	  uVar3 = 0x40de80;
	}
      else
	{
	  func_0x00401770 ("\n%s online help: <%s>\n", "GNU coreutils",
			   "https://www.gnu.org/software/coreutils/");
	  lVar2 = func_0x00401ac0 (5, 0);
	  if (lVar2 != 0)
	    {
	      iVar1 = func_0x00401970 (lVar2, &DAT_0040dee8, 3);
	      if (iVar1 != 0)
		{
		LAB_00403029:
		  ;
		  func_0x00401770
		    ("Report %s translation bugs to <https://translationproject.org/team/>\n",
		     &DAT_0040de44);
		}
	    }
	  func_0x00401770 ("Full documentation at: <%s%s>\n",
			   "https://www.gnu.org/software/coreutils/",
			   &DAT_0040de44);
	  uVar3 = 0x40f30f;
	  if (puVar5 == &DAT_0040de44)
	    goto LAB_00403052;
	}
      func_0x00401770
	("or available locally via: info \'(coreutils) %s%s\'\n", puVar5,
	 uVar3);
    }
  while (true);
}


#include "nproc.h"

long null_ARRAY_0060f94_0_8_;
long null_ARRAY_0060f94_8_8_;
long null_ARRAY_0060fb4_0_8_;
long null_ARRAY_0060fb4_16_8_;
long null_ARRAY_0060fb4_24_8_;
long null_ARRAY_0060fb4_32_8_;
long null_ARRAY_0060fb4_40_8_;
long null_ARRAY_0060fb4_48_8_;
long null_ARRAY_0060fb4_8_8_;
long null_ARRAY_0060fb8_0_4_;
long null_ARRAY_0060fb8_16_8_;
long null_ARRAY_0060fb8_4_4_;
long null_ARRAY_0060fb8_8_4_;
long local_2f_1_1_;
long local_5b_1_1_;
long local_9d_0_4_;
long local_a0_0_8_;
long local_a0_4_6_;
long local_a0_8_2_;
long local_a1_4_4_;
long local_a2_4_4_;
long local_a3_0_4_;
long local_a3_4_4_;
long local_a4_0_2_;
long local_a4_0_4_;
long local_a4_0_8_;
long local_a4_1_9_;
long local_a4_4_6_;
long local_a4_8_2_;
long local_a5_0_4_;
long local_a5_4_4_;
long DAT_00000010;
long DAT_0040cd40;
long DAT_0040cde1;
long DAT_0040ce17;
long DAT_0040d1e0;
long DAT_0040d1e4;
long DAT_0040d1e8;
long DAT_0040d1eb;
long DAT_0040d1ed;
long DAT_0040d1f1;
long DAT_0040d1f5;
long DAT_0040d7ab;
long DAT_0040dd48;
long DAT_0040de51;
long DAT_0040de57;
long DAT_0040de69;
long DAT_0040de6a;
long DAT_0040de88;
long DAT_0040de8c;
long DAT_0040df0e;
long DAT_0060f508;
long DAT_0060f518;
long DAT_0060f528;
long DAT_0060f8e8;
long DAT_0060f950;
long DAT_0060f954;
long DAT_0060f958;
long DAT_0060f95c;
long DAT_0060f980;
long DAT_0060f990;
long DAT_0060f9a0;
long DAT_0060f9a8;
long DAT_0060f9c0;
long DAT_0060f9c8;
long DAT_0060fa10;
long DAT_0060fa18;
long DAT_0060fa20;
long DAT_0060fbb8;
long DAT_0060fbc0;
long DAT_0060fbc8;
long DAT_0060fbd8;
long fde_0040e8a8;
long null_ARRAY_0040d0c0;
long null_ARRAY_0040e1a0;
long null_ARRAY_0040e3d0;
long null_ARRAY_0060f900;
long null_ARRAY_0060f940;
long null_ARRAY_0060f9e0;
long null_ARRAY_0060fa40;
long null_ARRAY_0060fb40;
long null_ARRAY_0060fb80;
long PTR_DAT_0060f8e0;
long PTR_null_ARRAY_0060f938;
long register0x00000020;
void
FUN_00401cd0 (uint uParm1)
{
  int iVar1;
  long lVar2;
  undefined8 uVar3;
  undefined **ppuVar4;
  char *pcVar5;
  undefined *puVar6;
  undefined *local_88;
  char *local_80;
  undefined8 local_78;
  char *local_70;
  char *local_68;
  char *local_60;
  char *local_58;
  char *local_50;
  char *local_48;
  char *local_40;
  char *local_38;
  char *local_30;
  undefined8 local_28;
  undefined8 local_20;

  if (uParm1 == 0)
    goto LAB_00401cfd;
  func_0x00401740 (DAT_0060f9a0, "Try \'%s --help\' for more information.\n",
		   DAT_0060fa20);
  do
    {
      func_0x004018f0 ((ulong) uParm1);
    LAB_00401cfd:
      ;
      func_0x004015b0 ("Usage: %s [OPTION]...\n", DAT_0060fa20);
      uVar3 = DAT_0060f980;
      func_0x00401750
	("Print the number of processing units available to the current process,\nwhich may be less than the number of online processors\n\n",
	 DAT_0060f980);
      func_0x00401750
	("      --all      print the number of installed processors\n      --ignore=N  if possible, exclude N processing units\n",
	 uVar3);
      func_0x00401750 ("      --help     display this help and exit\n",
		       uVar3);
      func_0x00401750
	("      --version  output version information and exit\n", uVar3);
      local_88 = &DAT_0040cd40;
      local_80 = "test invocation";
      puVar6 = &DAT_0040cd40;
      local_78 = 0x40cdc0;
      local_70 = "Multi-call invocation";
      local_68 = "sha224sum";
      local_60 = "sha2 utilities";
      local_58 = "sha256sum";
      local_50 = "sha2 utilities";
      local_48 = "sha384sum";
      local_40 = "sha2 utilities";
      local_38 = "sha512sum";
      local_30 = "sha2 utilities";
      local_28 = 0;
      local_20 = 0;
      ppuVar4 = &local_88;
      do
	{
	  iVar1 = func_0x00401840 ("nproc", puVar6);
	  if (iVar1 == 0)
	    break;
	  ppuVar4 = ppuVar4 + 2;
	  puVar6 = *ppuVar4;
	}
      while (puVar6 != (undefined *) 0x0);
      pcVar5 = ppuVar4[1];
      if (pcVar5 == (char *) 0x0)
	{
	  func_0x004015b0 ("\n%s online help: <%s>\n", "GNU coreutils",
			   "https://www.gnu.org/software/coreutils/");
	  lVar2 = func_0x004018a0 (5, 0);
	  if (lVar2 != 0)
	    {
	      iVar1 = func_0x004017a0 (lVar2, &DAT_0040cde1, 3);
	      if (iVar1 != 0)
		{
		  pcVar5 = "nproc";
		  goto LAB_00401ea1;
		}
	    }
	  func_0x004015b0 ("Full documentation at: <%s%s>\n",
			   "https://www.gnu.org/software/coreutils/",
			   "nproc");
	LAB_00401eca:
	  ;
	  pcVar5 = "nproc";
	  uVar3 = 0x40cd79;
	}
      else
	{
	  func_0x004015b0 ("\n%s online help: <%s>\n", "GNU coreutils",
			   "https://www.gnu.org/software/coreutils/");
	  lVar2 = func_0x004018a0 (5, 0);
	  if (lVar2 != 0)
	    {
	      iVar1 = func_0x004017a0 (lVar2, &DAT_0040cde1, 3);
	      if (iVar1 != 0)
		{
		LAB_00401ea1:
		  ;
		  func_0x004015b0
		    ("Report %s translation bugs to <https://translationproject.org/team/>\n",
		     "nproc");
		}
	    }
	  func_0x004015b0 ("Full documentation at: <%s%s>\n",
			   "https://www.gnu.org/software/coreutils/",
			   "nproc");
	  uVar3 = 0x40de87;
	  if (pcVar5 == "nproc")
	    goto LAB_00401eca;
	}
      func_0x004015b0
	("or available locally via: info \'(coreutils) %s%s\'\n", pcVar5,
	 uVar3);
    }
  while (true);
}

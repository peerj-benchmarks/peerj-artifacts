
#include "readlink.h"

long null_ARRAY_0061248_0_8_;
long null_ARRAY_0061248_8_8_;
long null_ARRAY_0061268_0_8_;
long null_ARRAY_0061268_16_8_;
long null_ARRAY_0061268_24_8_;
long null_ARRAY_0061268_32_8_;
long null_ARRAY_0061268_40_8_;
long null_ARRAY_0061268_48_8_;
long null_ARRAY_0061268_8_8_;
long null_ARRAY_006126c_0_4_;
long null_ARRAY_006126c_16_8_;
long null_ARRAY_006126c_4_4_;
long null_ARRAY_006126c_8_4_;
long local_2f_1_1_;
long local_4_4_4_;
long local_5b_1_1_;
long local_9d_0_4_;
long local_a0_0_8_;
long local_a0_4_6_;
long local_a0_8_2_;
long local_a1_4_4_;
long local_a2_4_4_;
long local_a3_0_4_;
long local_a3_4_4_;
long local_a4_0_2_;
long local_a4_0_4_;
long local_a4_0_8_;
long local_a4_1_9_;
long local_a4_4_6_;
long local_a4_8_2_;
long local_a5_0_4_;
long local_a5_4_4_;
long DAT_00000010;
long DAT_0040e880;
long DAT_0040e90d;
long DAT_0040f168;
long DAT_0040f16e;
long DAT_0040f170;
long DAT_0040f174;
long DAT_0040f178;
long DAT_0040f17b;
long DAT_0040f17d;
long DAT_0040f181;
long DAT_0040f185;
long DAT_0040f72b;
long DAT_0040fad5;
long DAT_0040fae6;
long DAT_0040fae7;
long DAT_0040fbe1;
long DAT_0040fbe7;
long DAT_0040fbf9;
long DAT_0040fbfa;
long DAT_0040fc18;
long DAT_0040fc1c;
long DAT_0040fc9e;
long DAT_00612000;
long DAT_00612010;
long DAT_00612020;
long DAT_00612428;
long DAT_00612490;
long DAT_00612494;
long DAT_00612498;
long DAT_0061249c;
long DAT_006124c0;
long DAT_006124d0;
long DAT_006124e0;
long DAT_006124e8;
long DAT_00612500;
long DAT_00612508;
long DAT_00612550;
long DAT_00612551;
long DAT_00612558;
long DAT_00612560;
long DAT_00612568;
long DAT_006126f8;
long DAT_00612700;
long DAT_00612708;
long DAT_00612710;
long DAT_00612718;
long DAT_00612728;
long fde_00410728;
long null_ARRAY_0040ef80;
long null_ARRAY_0040f100;
long null_ARRAY_0040ff40;
long null_ARRAY_00410160;
long null_ARRAY_00612480;
long null_ARRAY_00612520;
long null_ARRAY_00612580;
long null_ARRAY_00612680;
long null_ARRAY_006126c0;
long PTR_DAT_00612420;
long PTR_null_ARRAY_00612478;
long register0x00000020;
void
FUN_00402030 (uint uParm1)
{
  int iVar1;
  long lVar2;
  undefined8 uVar3;
  undefined **ppuVar4;
  char *pcVar5;
  undefined *puVar6;
  undefined *local_88;
  char *local_80;
  undefined8 local_78;
  char *local_70;
  char *local_68;
  char *local_60;
  char *local_58;
  char *local_50;
  char *local_48;
  char *local_40;
  char *local_38;
  char *local_30;
  undefined8 local_28;
  undefined8 local_20;

  if (uParm1 == 0)
    goto LAB_0040205d;
  func_0x00401950 (DAT_006124e0, "Try \'%s --help\' for more information.\n",
		   DAT_00612568);
  do
    {
      func_0x00401b00 ((ulong) uParm1);
    LAB_0040205d:
      ;
      func_0x00401770 ("Usage: %s [OPTION]... FILE...\n", DAT_00612568);
      uVar3 = DAT_006124c0;
      func_0x00401960
	("Print value of a symbolic link or canonical file name\n\n",
	 DAT_006124c0);
      func_0x00401960
	("  -f, --canonicalize            canonicalize by following every symlink in\n                                every component of the given name recursively;\n                                all but the last component must exist\n  -e, --canonicalize-existing   canonicalize by following every symlink in\n                                every component of the given name recursively,\n                                all components must exist\n",
	 uVar3);
      func_0x00401960
	("  -m, --canonicalize-missing    canonicalize by following every symlink in\n                                every component of the given name recursively,\n                                without requirements on components existence\n  -n, --no-newline              do not output the trailing delimiter\n  -q, --quiet\n  -s, --silent                  suppress most error messages (on by default)\n  -v, --verbose                 report error messages\n  -z, --zero                    end each output line with NUL, not newline\n",
	 uVar3);
      func_0x00401960 ("      --help     display this help and exit\n",
		       uVar3);
      func_0x00401960
	("      --version  output version information and exit\n", uVar3);
      local_88 = &DAT_0040e880;
      local_80 = "test invocation";
      puVar6 = &DAT_0040e880;
      local_78 = 0x40e8ec;
      local_70 = "Multi-call invocation";
      local_68 = "sha224sum";
      local_60 = "sha2 utilities";
      local_58 = "sha256sum";
      local_50 = "sha2 utilities";
      local_48 = "sha384sum";
      local_40 = "sha2 utilities";
      local_38 = "sha512sum";
      local_30 = "sha2 utilities";
      local_28 = 0;
      local_20 = 0;
      ppuVar4 = &local_88;
      do
	{
	  iVar1 = func_0x00401a40 ("readlink", puVar6);
	  if (iVar1 == 0)
	    break;
	  ppuVar4 = ppuVar4 + 2;
	  puVar6 = *ppuVar4;
	}
      while (puVar6 != (undefined *) 0x0);
      pcVar5 = ppuVar4[1];
      if (pcVar5 == (char *) 0x0)
	{
	  func_0x00401770 ("\n%s online help: <%s>\n", "GNU coreutils",
			   "https://www.gnu.org/software/coreutils/");
	  lVar2 = func_0x00401aa0 (5, 0);
	  if (lVar2 != 0)
	    {
	      iVar1 = func_0x004019c0 (lVar2, &DAT_0040e90d, 3);
	      if (iVar1 != 0)
		{
		  pcVar5 = "readlink";
		  goto LAB_00402209;
		}
	    }
	  func_0x00401770 ("Full documentation at: <%s%s>\n",
			   "https://www.gnu.org/software/coreutils/",
			   "readlink");
	LAB_00402232:
	  ;
	  pcVar5 = "readlink";
	  uVar3 = 0x40e8a5;
	}
      else
	{
	  func_0x00401770 ("\n%s online help: <%s>\n", "GNU coreutils",
			   "https://www.gnu.org/software/coreutils/");
	  lVar2 = func_0x00401aa0 (5, 0);
	  if (lVar2 != 0)
	    {
	      iVar1 = func_0x004019c0 (lVar2, &DAT_0040e90d, 3);
	      if (iVar1 != 0)
		{
		LAB_00402209:
		  ;
		  func_0x00401770
		    ("Report %s translation bugs to <https://translationproject.org/team/>\n",
		     "readlink");
		}
	    }
	  func_0x00401770 ("Full documentation at: <%s%s>\n",
			   "https://www.gnu.org/software/coreutils/",
			   "readlink");
	  uVar3 = 0x40fc17;
	  if (pcVar5 == "readlink")
	    goto LAB_00402232;
	}
      func_0x00401770
	("or available locally via: info \'(coreutils) %s%s\'\n", pcVar5,
	 uVar3);
    }
  while (true);
}

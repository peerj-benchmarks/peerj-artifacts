
#include "wc.h"

long null_ARRAY_006134a_0_8_;
long null_ARRAY_006134a_8_8_;
long null_ARRAY_0061370_0_8_;
long null_ARRAY_0061370_16_8_;
long null_ARRAY_0061370_24_8_;
long null_ARRAY_0061370_32_8_;
long null_ARRAY_0061370_40_8_;
long null_ARRAY_0061370_48_8_;
long null_ARRAY_0061370_8_8_;
long null_ARRAY_0061374_0_4_;
long null_ARRAY_0061374_16_8_;
long null_ARRAY_0061374_4_4_;
long null_ARRAY_0061374_8_4_;
long local_2f_1_1_;
long local_4_4_4_;
long local_5b_1_1_;
long local_9d_0_4_;
long local_a0_0_8_;
long local_a0_4_6_;
long local_a0_8_2_;
long local_a1_4_4_;
long local_a2_4_4_;
long local_a3_0_4_;
long local_a3_4_4_;
long local_a4_0_2_;
long local_a4_0_4_;
long local_a4_0_8_;
long local_a4_1_9_;
long local_a4_4_6_;
long local_a4_8_2_;
long local_a5_0_4_;
long local_a5_4_4_;
long DAT_00000010;
long DAT_0040ee4f;
long DAT_0040ee51;
long DAT_0040eed6;
long DAT_0040f140;
long DAT_0040f8f0;
long DAT_0040f8f4;
long DAT_0040f8f8;
long DAT_0040f8fb;
long DAT_0040f8fd;
long DAT_0040f901;
long DAT_0040f905;
long DAT_0040feab;
long DAT_00410255;
long DAT_00410359;
long DAT_0041035f;
long DAT_00410371;
long DAT_00410372;
long DAT_00410390;
long DAT_00410394;
long DAT_00410425;
long DAT_00410ec6;
long DAT_00613000;
long DAT_00613010;
long DAT_00613020;
long DAT_00613448;
long DAT_006134b0;
long DAT_006134b4;
long DAT_006134b8;
long DAT_006134bc;
long DAT_00613500;
long DAT_00613508;
long DAT_00613510;
long DAT_00613520;
long DAT_00613528;
long DAT_00613540;
long DAT_00613548;
long DAT_00613590;
long DAT_00613598;
long DAT_006135a0;
long DAT_006135a4;
long DAT_006135a8;
long DAT_006135a9;
long DAT_006135aa;
long DAT_006135ab;
long DAT_006135ac;
long DAT_006135b0;
long DAT_006135b8;
long DAT_006135c0;
long DAT_006135c8;
long DAT_006135d0;
long DAT_006135d8;
long DAT_006135e0;
long DAT_006135e8;
long DAT_00613778;
long DAT_00613780;
long DAT_00613788;
long DAT_00613798;
long _DYNAMIC;
long fde_00411940;
long null_ARRAY_0040f180;
long null_ARRAY_0040f880;
long null_ARRAY_00410440;
long null_ARRAY_00410540;
long null_ARRAY_00411160;
long null_ARRAY_00411380;
long null_ARRAY_006134a0;
long null_ARRAY_00613560;
long null_ARRAY_00613600;
long null_ARRAY_00613700;
long null_ARRAY_00613740;
long PTR_DAT_0040f080;
long PTR_DAT_00613440;
long PTR_FUN_006134c0;
long PTR_null_ARRAY_00613498;
long register0x00000020;
void
FUN_004031f0 (uint uParm1)
{
  int iVar1;
  long lVar2;
  undefined8 uVar3;
  undefined **ppuVar4;
  undefined *puVar5;
  undefined *local_88;
  char *local_80;
  undefined8 local_78;
  char *local_70;
  char *local_68;
  char *local_60;
  char *local_58;
  char *local_50;
  char *local_48;
  char *local_40;
  char *local_38;
  char *local_30;
  undefined8 local_28;
  undefined8 local_20;

  if (uParm1 == 0)
    goto LAB_0040321d;
  func_0x00401a40 (DAT_00613520, "Try \'%s --help\' for more information.\n",
		   DAT_006135e8);
  do
    {
      func_0x00401c50 ((ulong) uParm1);
    LAB_0040321d:
      ;
      func_0x00401860
	("Usage: %s [OPTION]... [FILE]...\n  or:  %s [OPTION]... --files0-from=F\n",
	 DAT_006135e8, DAT_006135e8);
      uVar3 = DAT_00613500;
      func_0x00401a50
	("Print newline, word, and byte counts for each FILE, and a total line if\nmore than one FILE is specified.  A word is a non-zero-length sequence of\ncharacters delimited by white space.\n",
	 DAT_00613500);
      func_0x00401a50
	("\nWith no FILE, or when FILE is -, read standard input.\n", uVar3);
      func_0x00401a50
	("\nThe options below may be used to select which counts are printed, always in\nthe following order: newline, word, character, byte, maximum line length.\n  -c, --bytes            print the byte counts\n  -m, --chars            print the character counts\n  -l, --lines            print the newline counts\n",
	 uVar3);
      func_0x00401a50
	("      --files0-from=F    read input from the files specified by\n                           NUL-terminated names in file F;\n                           If F is - then read names from standard input\n  -L, --max-line-length  print the maximum display width\n  -w, --words            print the word counts\n",
	 uVar3);
      func_0x00401a50 ("      --help     display this help and exit\n",
		       uVar3);
      func_0x00401a50
	("      --version  output version information and exit\n", uVar3);
      local_88 = &DAT_0040ee4f;
      local_80 = "test invocation";
      puVar5 = &DAT_0040ee4f;
      local_78 = 0x40eeb5;
      local_70 = "Multi-call invocation";
      local_68 = "sha224sum";
      local_60 = "sha2 utilities";
      local_58 = "sha256sum";
      local_50 = "sha2 utilities";
      local_48 = "sha384sum";
      local_40 = "sha2 utilities";
      local_38 = "sha512sum";
      local_30 = "sha2 utilities";
      local_28 = 0;
      local_20 = 0;
      ppuVar4 = &local_88;
      do
	{
	  iVar1 = func_0x00401b80 (&DAT_0040ee51, puVar5);
	  if (iVar1 == 0)
	    break;
	  ppuVar4 = ppuVar4 + 2;
	  puVar5 = *ppuVar4;
	}
      while (puVar5 != (undefined *) 0x0);
      puVar5 = ppuVar4[1];
      if (puVar5 == (undefined *) 0x0)
	{
	  func_0x00401860 ("\n%s online help: <%s>\n", "GNU coreutils",
			   "https://www.gnu.org/software/coreutils/");
	  lVar2 = func_0x00401bf0 (5, 0);
	  if (lVar2 != 0)
	    {
	      iVar1 = func_0x00401ad0 (lVar2, &DAT_0040eed6, 3);
	      if (iVar1 != 0)
		{
		  puVar5 = &DAT_0040ee51;
		  goto LAB_004033d9;
		}
	    }
	  func_0x00401860 ("Full documentation at: <%s%s>\n",
			   "https://www.gnu.org/software/coreutils/",
			   &DAT_0040ee51);
	LAB_00403402:
	  ;
	  puVar5 = &DAT_0040ee51;
	  uVar3 = 0x40ee6e;
	}
      else
	{
	  func_0x00401860 ("\n%s online help: <%s>\n", "GNU coreutils",
			   "https://www.gnu.org/software/coreutils/");
	  lVar2 = func_0x00401bf0 (5, 0);
	  if (lVar2 != 0)
	    {
	      iVar1 = func_0x00401ad0 (lVar2, &DAT_0040eed6, 3);
	      if (iVar1 != 0)
		{
		LAB_004033d9:
		  ;
		  func_0x00401860
		    ("Report %s translation bugs to <https://translationproject.org/team/>\n",
		     &DAT_0040ee51);
		}
	    }
	  func_0x00401860 ("Full documentation at: <%s%s>\n",
			   "https://www.gnu.org/software/coreutils/",
			   &DAT_0040ee51);
	  uVar3 = 0x41038f;
	  if (puVar5 == &DAT_0040ee51)
	    goto LAB_00403402;
	}
      func_0x00401860
	("or available locally via: info \'(coreutils) %s%s\'\n", puVar5,
	 uVar3);
    }
  while (true);
}

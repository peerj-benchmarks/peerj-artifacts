typedef unsigned char   undefined;

typedef unsigned char    byte;
typedef unsigned char    dwfenc;
typedef unsigned int    dword;
typedef unsigned long    qword;
typedef unsigned char    undefined1;
typedef unsigned short    undefined2;
typedef unsigned int    undefined4;
typedef unsigned long    undefined6;
typedef unsigned long    undefined8;
typedef unsigned short    word;
#define unkbyte9   unsigned long long
#define unkbyte10   unsigned long long
#define unkbyte11   unsigned long long
#define unkbyte12   unsigned long long
#define unkbyte13   unsigned long long
#define unkbyte14   unsigned long long
#define unkbyte15   unsigned long long
#define unkbyte16   unsigned long long

#define unkuint9   unsigned long long
#define unkuint10   unsigned long long
#define unkuint11   unsigned long long
#define unkuint12   unsigned long long
#define unkuint13   unsigned long long
#define unkuint14   unsigned long long
#define unkuint15   unsigned long long
#define unkuint16   unsigned long long

#define unkint9   long long
#define unkint10   long long
#define unkint11   long long
#define unkint12   long long
#define unkint13   long long
#define unkint14   long long
#define unkint15   long long
#define unkint16   long long

#define unkfloat1   float
#define unkfloat2   float
#define unkfloat3   float
#define unkfloat5   double
#define unkfloat6   double
#define unkfloat7   double
#define unkfloat9   long double
#define unkfloat11   long double
#define unkfloat12   long double
#define unkfloat13   long double
#define unkfloat14   long double
#define unkfloat15   long double
#define unkfloat16   long double

#define BADSPACEBASE   void
#define code   void

#define ulong long
#define true 1
#define bool int
#define false 0
#define float10 int
#define ushort int
#define undefined7 long
#define undefined2 long
#define uint5 long
#define uint6 long

// Add definition for uint
typedef unsigned int    uint;

#define sbyte char

extern imperfection_wrapper(void);
typedef struct eh_frame_hdr eh_frame_hdr, *Peh_frame_hdr;

struct eh_frame_hdr {
    byte eh_frame_hdr_version; // Exception Handler Frame Header Version
    dwfenc eh_frame_pointer_encoding; // Exception Handler Frame Pointer Encoding
    dwfenc eh_frame_desc_entry_count_encoding; // Encoding of # of Exception Handler FDEs
    dwfenc eh_frame_table_encoding; // Exception Handler Table Encoding
};

typedef struct fde_table_entry fde_table_entry, *Pfde_table_entry;

struct fde_table_entry {
    dword initial_loc; // Initial Location
    dword data_loc; // Data location
};

typedef enum Elf64_DynTag {
    DT_AUDIT=1879047932,
    DT_AUXILIARY=2147483645,
    DT_BIND_NOW=24,
    DT_CHECKSUM=1879047672,
    DT_CONFIG=1879047930,
    DT_DEBUG=21,
    DT_DEPAUDIT=1879047931,
    DT_ENCODING=32,
    DT_FEATURE_1=1879047676,
    DT_FILTER=2147483647,
    DT_FINI=13,
    DT_FINI_ARRAY=26,
    DT_FINI_ARRAYSZ=28,
    DT_FLAGS=30,
    DT_FLAGS_1=1879048187,
    DT_GNU_CONFLICT=1879047928,
    DT_GNU_CONFLICTSZ=1879047670,
    DT_GNU_HASH=1879047925,
    DT_GNU_LIBLIST=1879047929,
    DT_GNU_LIBLISTSZ=1879047671,
    DT_GNU_PRELINKED=1879047669,
    DT_HASH=4,
    DT_INIT=12,
    DT_INIT_ARRAY=25,
    DT_INIT_ARRAYSZ=27,
    DT_JMPREL=23,
    DT_MOVEENT=1879047674,
    DT_MOVESZ=1879047675,
    DT_MOVETAB=1879047934,
    DT_NEEDED=1,
    DT_NULL=0,
    DT_PLTGOT=3,
    DT_PLTPAD=1879047933,
    DT_PLTPADSZ=1879047673,
    DT_PLTREL=20,
    DT_PLTRELSZ=2,
    DT_POSFLAG_1=1879047677,
    DT_PREINIT_ARRAYSZ=33,
    DT_REL=17,
    DT_RELA=7,
    DT_RELACOUNT=1879048185,
    DT_RELAENT=9,
    DT_RELASZ=8,
    DT_RELCOUNT=1879048186,
    DT_RELENT=19,
    DT_RELSZ=18,
    DT_RPATH=15,
    DT_RUNPATH=29,
    DT_SONAME=14,
    DT_STRSZ=10,
    DT_STRTAB=5,
    DT_SYMBOLIC=16,
    DT_SYMENT=11,
    DT_SYMINENT=1879047679,
    DT_SYMINFO=1879047935,
    DT_SYMINSZ=1879047678,
    DT_SYMTAB=6,
    DT_TEXTREL=22,
    DT_TLSDESC_GOT=1879047927,
    DT_TLSDESC_PLT=1879047926,
    DT_VERDEF=1879048188,
    DT_VERDEFNUM=1879048189,
    DT_VERNEED=1879048190,
    DT_VERNEEDNUM=1879048191,
    DT_VERSYM=1879048176
} Elf64_DynTag;

typedef enum Elf_ProgramHeaderType {
    PT_DYNAMIC=2,
    PT_GNU_EH_FRAME=1685382480,
    PT_GNU_RELRO=1685382482,
    PT_GNU_STACK=1685382481,
    PT_INTERP=3,
    PT_LOAD=1,
    PT_NOTE=4,
    PT_NULL=0,
    PT_PHDR=6,
    PT_SHLIB=5,
    PT_TLS=7
} Elf_ProgramHeaderType;

typedef struct Elf64_Dyn Elf64_Dyn, *PElf64_Dyn;

struct Elf64_Dyn {
    enum Elf64_DynTag d_tag;
    qword d_val;
};

typedef struct Elf64_Shdr Elf64_Shdr, *PElf64_Shdr;

typedef enum Elf_SectionHeaderType {
    SHT_CHECKSUM=1879048184,
    SHT_DYNAMIC=6,
    SHT_DYNSYM=11,
    SHT_FINI_ARRAY=15,
    SHT_GNU_ATTRIBUTES=1879048181,
    SHT_GNU_HASH=1879048182,
    SHT_GNU_LIBLIST=1879048183,
    SHT_GNU_verdef=1879048189,
    SHT_GNU_verneed=1879048190,
    SHT_GNU_versym=1879048191,
    SHT_GROUP=17,
    SHT_HASH=5,
    SHT_INIT_ARRAY=14,
    SHT_NOBITS=8,
    SHT_NOTE=7,
    SHT_NULL=0,
    SHT_PREINIT_ARRAY=16,
    SHT_PROGBITS=1,
    SHT_REL=9,
    SHT_RELA=4,
    SHT_SHLIB=10,
    SHT_STRTAB=3,
    SHT_SUNW_COMDAT=1879048187,
    SHT_SUNW_move=1879048186,
    SHT_SUNW_syminfo=1879048188,
    SHT_SYMTAB=2,
    SHT_SYMTAB_SHNDX=18
} Elf_SectionHeaderType;

struct Elf64_Shdr {
    dword sh_name;
    enum Elf_SectionHeaderType sh_type;
    qword sh_flags;
    qword sh_addr;
    qword sh_offset;
    qword sh_size;
    dword sh_link;
    dword sh_info;
    qword sh_addralign;
    qword sh_entsize;
};

typedef struct Elf64_Phdr Elf64_Phdr, *PElf64_Phdr;

struct Elf64_Phdr {
    enum Elf_ProgramHeaderType p_type;
    dword p_flags;
    qword p_offset;
    qword p_vaddr;
    qword p_paddr;
    qword p_filesz;
    qword p_memsz;
    qword p_align;
};

typedef struct Elf64_Sym Elf64_Sym, *PElf64_Sym;

struct Elf64_Sym {
    dword st_name;
    byte st_info;
    byte st_other;
    word st_shndx;
    qword st_value;
    qword st_size;
};

typedef struct Elf64_Rela Elf64_Rela, *PElf64_Rela;

struct Elf64_Rela {
    qword r_offset; // location to apply the relocation action
    qword r_info; // the symbol table index and the type of relocation
    qword r_addend; // a constant addend used to compute the relocatable field value
};

typedef struct Elf64_Ehdr Elf64_Ehdr, *PElf64_Ehdr;

struct Elf64_Ehdr {
    byte e_ident_magic_num;
    char e_ident_magic_str[3];
    byte e_ident_class;
    byte e_ident_data;
    byte e_ident_version;
    byte e_ident_pad[9];
    word e_type;
    word e_machine;
    dword e_version;
    qword e_entry;
    qword e_phoff;
    qword e_shoff;
    dword e_flags;
    word e_ehsize;
    word e_phentsize;
    word e_phnum;
    word e_shentsize;
    word e_shnum;
    word e_shstrndx;
};




undefined8 _DT_INIT(void);
void FUN_00401c10(void);
void thunk_FUN_00619050(void);
void thunk_FUN_00619130(void);
void thunk_FUN_006191c8(void);
void FUN_00402220(void);
void thunk_FUN_00619300(void);
ulong FUN_00402240(int iParm1,undefined8 *puParm2);
void entry(void);
void FUN_004026b0(undefined8 *puParm1);
void FUN_004026e0(void);
void FUN_00402760(void);
void FUN_004027e0(void);
void FUN_00402820(uint uParm1);
undefined8 FUN_00402ab0(long lParm1,long lParm2);
void FUN_00402b30(undefined4 *puParm1);
void FUN_00402b60(ulong uParm1);
void FUN_00402ba0(ulong uParm1);
ulong FUN_00402be0(long param_1,long param_2,uint param_3,ulong param_4,uint param_5,uint param_6,int *param_7);
ulong FUN_00403750(undefined8 param_1,uint param_2,uint param_3,ulong param_4,uint param_5,uint param_6,int *param_7);
void FUN_00403850(void);
char * FUN_004038e0(ulong uParm1,long lParm2);
void FUN_00403930(long lParm1);
undefined8 * FUN_004039d0(undefined8 *puParm1,int iParm2);
undefined * FUN_00403a40(char *pcParm1,int iParm2);
ulong FUN_00403b10(undefined *param_1,ulong param_2,long param_3,ulong param_4,uint param_5,ulong param_6,long param_7,char *param_8,char *param_9);
undefined1 * FUN_00404a10(uint uParm1,undefined8 uParm2,undefined8 uParm3,uint *puParm4);
void FUN_00404bc0(uint uParm1,undefined8 uParm2,undefined8 uParm3);
void FUN_00404bf0(ulong uParm1,undefined8 uParm2);
void FUN_00404c00(undefined8 uParm1,undefined8 uParm2,byte bParm3);
void FUN_00404ca0(undefined8 uParm1);
void FUN_00404cc0(uint uParm1,undefined8 uParm2,undefined8 uParm3);
void FUN_00404d50(undefined8 uParm1);
undefined8 * FUN_00404d70(undefined8 *puParm1);
char * FUN_00404dc0(char *pcParm1,long lParm2,undefined4 *puParm3,uint *puParm4,char **ppcParm5,undefined8 *puParm6);
long FUN_004050d0(undefined8 uParm1,undefined8 uParm2,long lParm3,undefined8 uParm4,undefined8 uParm5);
long FUN_00405190(undefined8 uParm1,long lParm2,undefined8 uParm3,undefined8 uParm4,long *plParm5,undefined8 uParm6);
void FUN_004053f0(void);
void FUN_00405460(void);
void FUN_004054f0(long lParm1);
long FUN_00405510(long lParm1,long lParm2);
void FUN_00405550(undefined8 uParm1,undefined8 uParm2);
void FUN_00405580(undefined8 uParm1);
void FUN_004055a0(void);
void FUN_004055d0(undefined8 uParm1,uint uParm2);
ulong FUN_00405620(long lParm1,long lParm2);
ulong FUN_00405650(byte *pbParm1,byte **ppbParm2,uint uParm3,ulong *puParm4,long lParm5);
undefined8 FUN_00405ab0(ulong uParm1,ulong uParm2);
void FUN_00405b00(undefined8 uParm1);
void FUN_00405b40(undefined8 uParm1,uint uParm2,undefined8 uParm3,undefined8 uParm4);
void FUN_00405bb0(void);
void FUN_00405bf0(undefined4 uParm1,undefined4 uParm2,undefined4 uParm3,undefined4 uParm4,undefined4 uParm5,undefined4 uParm6,undefined4 uParm7,undefined4 uParm8,ulong param_1,ulong param_2,undefined8 param_3,undefined8 param_4,undefined8 param_5,undefined8 param_6);
ulong FUN_00405cd0(uint uParm1,char *pcParm2,ulong uParm3,uint uParm4,uint uParm5);
undefined8 FUN_00405f50(ulong uParm1,long lParm2,long lParm3,ulong uParm4);
ulong FUN_00406010(ulong *puParm1,ulong *puParm2);
ulong FUN_00406030(long lParm1,ulong uParm2);
ulong FUN_00406040(ulong *puParm1,ulong uParm2);
ulong FUN_00406050(ulong *puParm1,ulong *puParm2);
ulong FUN_00406060(long *plParm1,long *plParm2);
ulong FUN_00406090(long lParm1,long lParm2,char cParm3);
long FUN_00406200(long lParm1,long lParm2,ulong uParm3);
long FUN_00406320(long lParm1,undefined8 uParm2,long lParm3);
void FUN_004063c0(long lParm1);
void FUN_00406400(long lParm1);
void FUN_00406430(undefined8 uParm1);
undefined8 FUN_00406470(long lParm1);
undefined8 FUN_00406570(void);
void FUN_004065d0(long lParm1,uint uParm2,char cParm3);
ulong FUN_00406640(long lParm1);
undefined8 FUN_004066a0(long *plParm1,ulong *puParm2,long lParm3);
ulong FUN_00406720(long lParm1);
void FUN_00406780(undefined8 uParm1,ulong uParm2,undefined8 uParm3);
ulong FUN_004067d0(long lParm1,long lParm2,ulong uParm3,long lParm4);
undefined8 FUN_004069a0(long lParm1,undefined8 *puParm2);
void FUN_00406a50(long lParm1,long lParm2);
long FUN_00406af0(long *plParm1,int iParm2);
long * FUN_00407490(long *plParm1,uint uParm2,long lParm3);
undefined8 FUN_00407880(long *plParm1);
long FUN_00407a00(long *plParm1);
undefined8 FUN_00408140(undefined8 uParm1,long lParm2,uint uParm3);
void FUN_00408170(long lParm1,int *piParm2);
ulong FUN_00408250(int param_1,undefined8 *param_2,char *param_3,long *param_4,int *param_5,int param_6,int *param_7,int param_8,undefined8 param_9);
ulong FUN_004087d0(uint param_1,undefined8 *param_2,char *param_3,long param_4,undefined8 param_5,ulong param_6,uint *param_7,int param_8);
void FUN_00408d20(void);
void FUN_00408d80(void);
ulong FUN_00408da0(long lParm1,uint uParm2,uint uParm3);
undefined8 FUN_00408e90(long lParm1,long lParm2);
void FUN_00408f10(long lParm1);
ulong FUN_00408f30(uint *puParm1,byte *pbParm2,long lParm3);
ulong FUN_00408fa0(uint param_1,undefined8 param_2,uint param_3,ulong param_4);
undefined * FUN_004090c0(undefined *puParm1,uint uParm2,char *pcParm3);
void FUN_004091d0(long lParm1,long lParm2);
ulong FUN_00409210(long lParm1,long lParm2);
ulong FUN_00409260(undefined8 uParm1,undefined8 uParm2,undefined8 uParm3);
ulong FUN_00409330(ulong uParm1,char cParm2);
ulong FUN_00409390(undefined8 uParm1);
void FUN_00409400(long lParm1);
undefined8 FUN_00409410(long *plParm1,long *plParm2);
void FUN_004094a0(undefined8 param_1,byte param_2,ulong param_3);
undefined8 FUN_004094f0(void);
ulong FUN_00409500(ulong uParm1);
ulong FUN_004095a0(ulong uParm1);
ulong FUN_00409640(ulong uParm1,ulong uParm2);
undefined FUN_00409650(long lParm1,long lParm2);;
long FUN_00409660(long *plParm1,undefined8 uParm2);
long FUN_00409690(long lParm1,long lParm2,long **pplParm3,char cParm4);
ulong FUN_00409790(float **ppfParm1,undefined8 uParm2,uint uParm3);
undefined8 FUN_00409820(long lParm1,long **pplParm2,char cParm3);
long FUN_00409970(long lParm1,long lParm2);
long * FUN_004099d0(ulong uParm1,undefined1 *puParm2,code *pcParm3,code *pcParm4,long lParm5);
void FUN_00409b80(long **pplParm1);
ulong FUN_00409c40(long *plParm1,ulong uParm2);
undefined8 FUN_00409e40(long lParm1,long lParm2,long *plParm3);
undefined8 FUN_0040a0b0(undefined8 uParm1,undefined8 uParm2);
long FUN_0040a0f0(long lParm1,undefined8 uParm2);
void FUN_0040a2f0(undefined4 *puParm1,undefined4 uParm2);
ulong FUN_0040a320(long lParm1);
ulong FUN_0040a330(long lParm1,uint uParm2);
ulong FUN_0040a370(long lParm1);
char * FUN_0040a3b0(void);
void FUN_0040a700(ulong uParm1);
void FUN_0040a720(ulong uParm1);
void FUN_0040a740(void);
ulong FUN_0040a790(int *piParm1);
void FUN_0040a7e0(uint *puParm1);
void FUN_0040a800(int *piParm1);
ulong FUN_0040a820(uint uParm1);
void FUN_0040a860(int *piParm1);
undefined8 FUN_0040a8a0(uint *puParm1,undefined8 uParm2);
ulong FUN_0040a8e0(char *pcParm1);
undefined8 FUN_0040ab90(long lParm1,uint uParm2,uint uParm3);
void FUN_0040acb0(undefined8 uParm1);
ulong FUN_0040ad40(ulong param_1,undefined8 param_2,ulong param_3);
undefined8 FUN_0040ae80(ulong uParm1);
ulong FUN_0040aef0(long lParm1);
undefined8 FUN_0040af80(void);
void FUN_0040af90(void);
long FUN_0040afa0(long lParm1,ulong uParm2);
ulong * FUN_0040b420(ulong *puParm1,char cParm2,ulong uParm3);
undefined4 * FUN_0040b510(ulong uParm1,uint *puParm2,ulong uParm3,uint *puParm4,long *plParm5);
ulong FUN_0040b670(void);
uint * FUN_0040b8e0(ulong uParm1,long lParm2,ulong uParm3,uint *puParm4,ulong *puParm5);
char * FUN_0040bea0(int iParm1,long lParm2,long lParm3,long lParm4,uint uParm5);
void FUN_0040c440(uint param_1);
ulong FUN_0040c5c0(void);
void FUN_0040c7e0(undefined8 uParm1,uint uParm2);
undefined8 *FUN_0040c950(undefined8 *puParm1,undefined8 *puParm2,undefined8 *puParm3,undefined8 uParm4);
double FUN_00411f20(int *piParm1);
void FUN_00411f60(uint *param_1);
void FUN_00411fe0(undefined8 uParm1);
undefined8 FUN_00411ff0(uint *puParm1,ulong *puParm2);
undefined8 FUN_00412210(byte *pbParm1,ulong *puParm2,byte **ppbParm3);
ulong FUN_00412e60(void);
ulong FUN_00412e90(void);
void FUN_00412ed0(void);
undefined8 _DT_FINI(void);
undefined FUN_00619000();
undefined FUN_00619008();
undefined FUN_00619010();
undefined FUN_00619018();
undefined FUN_00619020();
undefined FUN_00619028();
undefined FUN_00619030();
undefined FUN_00619038();
undefined FUN_00619040();
undefined FUN_00619048();
undefined FUN_00619050();
undefined FUN_00619058();
undefined FUN_00619060();
undefined FUN_00619068();
undefined FUN_00619070();
undefined FUN_00619078();
undefined FUN_00619080();
undefined FUN_00619088();
undefined FUN_00619090();
undefined FUN_00619098();
undefined FUN_006190a0();
undefined FUN_006190a8();
undefined FUN_006190b0();
undefined FUN_006190b8();
undefined FUN_006190c0();
undefined FUN_006190c8();
undefined FUN_006190d0();
undefined FUN_006190d8();
undefined FUN_006190e0();
undefined FUN_006190e8();
undefined FUN_006190f0();
undefined FUN_006190f8();
undefined FUN_00619100();
undefined FUN_00619108();
undefined FUN_00619110();
undefined FUN_00619118();
undefined FUN_00619120();
undefined FUN_00619128();
undefined FUN_00619130();
undefined FUN_00619138();
undefined FUN_00619140();
undefined FUN_00619148();
undefined FUN_00619150();
undefined FUN_00619158();
undefined FUN_00619160();
undefined FUN_00619168();
undefined FUN_00619170();
undefined FUN_00619178();
undefined FUN_00619180();
undefined FUN_00619188();
undefined FUN_00619190();
undefined FUN_00619198();
undefined FUN_006191a0();
undefined FUN_006191a8();
undefined FUN_006191b0();
undefined FUN_006191b8();
undefined FUN_006191c0();
undefined FUN_006191c8();
undefined FUN_006191d0();
undefined FUN_006191d8();
undefined FUN_006191e0();
undefined FUN_006191e8();
undefined FUN_006191f0();
undefined FUN_006191f8();
undefined FUN_00619200();
undefined FUN_00619208();
undefined FUN_00619210();
undefined FUN_00619218();
undefined FUN_00619220();
undefined FUN_00619228();
undefined FUN_00619230();
undefined FUN_00619238();
undefined FUN_00619240();
undefined FUN_00619248();
undefined FUN_00619250();
undefined FUN_00619258();
undefined FUN_00619260();
undefined FUN_00619268();
undefined FUN_00619270();
undefined FUN_00619278();
undefined FUN_00619280();
undefined FUN_00619288();
undefined FUN_00619290();
undefined FUN_00619298();
undefined FUN_006192a0();
undefined FUN_006192a8();
undefined FUN_006192b0();
undefined FUN_006192b8();
undefined FUN_006192c0();
undefined FUN_006192c8();
undefined FUN_006192d0();
undefined FUN_006192d8();
undefined FUN_006192e0();
undefined FUN_006192e8();
undefined FUN_006192f0();
undefined FUN_006192f8();
undefined FUN_00619300();


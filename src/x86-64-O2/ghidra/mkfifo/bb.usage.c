
#include "mkfifo.h"

long null_ARRAY_0060f80_0_8_;
long null_ARRAY_0060f80_8_8_;
long null_ARRAY_0060fa0_0_8_;
long null_ARRAY_0060fa0_16_8_;
long null_ARRAY_0060fa0_24_8_;
long null_ARRAY_0060fa0_32_8_;
long null_ARRAY_0060fa0_40_8_;
long null_ARRAY_0060fa0_48_8_;
long null_ARRAY_0060fa0_8_8_;
long null_ARRAY_0060fa4_0_4_;
long null_ARRAY_0060fa4_16_8_;
long null_ARRAY_0060fa4_4_4_;
long null_ARRAY_0060fa4_8_4_;
long local_2f_1_1_;
long local_4_4_4_;
long local_5b_1_1_;
long local_9d_0_4_;
long local_a0_0_8_;
long local_a0_4_6_;
long local_a0_8_2_;
long local_a1_4_4_;
long local_a2_4_4_;
long local_a3_0_4_;
long local_a3_4_4_;
long local_a4_0_2_;
long local_a4_0_4_;
long local_a4_0_8_;
long local_a4_1_9_;
long local_a4_4_6_;
long local_a4_8_2_;
long local_a5_0_4_;
long local_a5_4_4_;
long DAT_00000010;
long DAT_0040cc00;
long DAT_0040cc8b;
long DAT_0040cc9f;
long DAT_0040d2c0;
long DAT_0040d2c4;
long DAT_0040d2c8;
long DAT_0040d2cb;
long DAT_0040d2cd;
long DAT_0040d2d1;
long DAT_0040d2d5;
long DAT_0040d86b;
long DAT_0040dc15;
long DAT_0040dd19;
long DAT_0040dd1f;
long DAT_0040dd31;
long DAT_0040dd32;
long DAT_0040dd50;
long DAT_0040dd54;
long DAT_0040ddd6;
long DAT_0060f3c8;
long DAT_0060f3d8;
long DAT_0060f3e8;
long DAT_0060f7a8;
long DAT_0060f810;
long DAT_0060f814;
long DAT_0060f818;
long DAT_0060f81c;
long DAT_0060f840;
long DAT_0060f850;
long DAT_0060f860;
long DAT_0060f868;
long DAT_0060f880;
long DAT_0060f888;
long DAT_0060f8d0;
long DAT_0060f8d8;
long DAT_0060f8e0;
long DAT_0060fa78;
long DAT_0060fa80;
long DAT_0060fa88;
long DAT_0060fa98;
long _DYNAMIC;
long fde_0040e770;
long null_ARRAY_0040d0c0;
long null_ARRAY_0040e060;
long null_ARRAY_0040e290;
long null_ARRAY_0060f800;
long null_ARRAY_0060f8a0;
long null_ARRAY_0060f900;
long null_ARRAY_0060fa00;
long null_ARRAY_0060fa40;
long PTR_DAT_0060f7a0;
long PTR_null_ARRAY_0060f7f8;
long register0x00000020;
long stack0x00000008;
void
FUN_00401cf0 (uint uParm1)
{
  int iVar1;
  long lVar2;
  undefined8 uVar3;
  undefined **ppuVar4;
  char *pcVar5;
  undefined *puVar6;
  undefined *local_88;
  char *local_80;
  undefined8 local_78;
  char *local_70;
  char *local_68;
  char *local_60;
  char *local_58;
  char *local_50;
  char *local_48;
  char *local_40;
  char *local_38;
  char *local_30;
  undefined8 local_28;
  undefined8 local_20;

  if (uParm1 == 0)
    goto LAB_00401d1d;
  func_0x00401670 (DAT_0060f860, "Try \'%s --help\' for more information.\n",
		   DAT_0060f8e0);
  do
    {
      func_0x00401830 ((ulong) uParm1);
    LAB_00401d1d:
      ;
      func_0x00401500 ("Usage: %s [OPTION]... NAME...\n", DAT_0060f8e0);
      uVar3 = DAT_0060f840;
      func_0x00401680 ("Create named pipes (FIFOs) with the given NAMEs.\n",
		       DAT_0060f840);
      func_0x00401680
	("\nMandatory arguments to long options are mandatory for short options too.\n",
	 uVar3);
      func_0x00401680
	("  -m, --mode=MODE    set file permission bits to MODE, not a=rw - umask\n",
	 uVar3);
      func_0x00401680
	("  -Z                   set the SELinux security context to default type\n      --context[=CTX]  like -Z, or if CTX is specified then set the SELinux\n                         or SMACK security context to CTX\n",
	 uVar3);
      func_0x00401680 ("      --help     display this help and exit\n",
		       uVar3);
      func_0x00401680
	("      --version  output version information and exit\n", uVar3);
      local_88 = &DAT_0040cc00;
      local_80 = "test invocation";
      puVar6 = &DAT_0040cc00;
      local_78 = 0x40cc6a;
      local_70 = "Multi-call invocation";
      local_68 = "sha224sum";
      local_60 = "sha2 utilities";
      local_58 = "sha256sum";
      local_50 = "sha2 utilities";
      local_48 = "sha384sum";
      local_40 = "sha2 utilities";
      local_38 = "sha512sum";
      local_30 = "sha2 utilities";
      local_28 = 0;
      local_20 = 0;
      ppuVar4 = &local_88;
      do
	{
	  iVar1 = func_0x00401780 ("mkfifo", puVar6);
	  if (iVar1 == 0)
	    break;
	  ppuVar4 = ppuVar4 + 2;
	  puVar6 = *ppuVar4;
	}
      while (puVar6 != (undefined *) 0x0);
      pcVar5 = ppuVar4[1];
      if (pcVar5 == (char *) 0x0)
	{
	  func_0x00401500 ("\n%s online help: <%s>\n", "GNU coreutils",
			   "https://www.gnu.org/software/coreutils/");
	  lVar2 = func_0x004017e0 (5, 0);
	  if (lVar2 != 0)
	    {
	      iVar1 = func_0x004016f0 (lVar2, &DAT_0040cc8b, 3);
	      if (iVar1 != 0)
		{
		  pcVar5 = "mkfifo";
		  goto LAB_00401ed9;
		}
	    }
	  func_0x00401500 ("Full documentation at: <%s%s>\n",
			   "https://www.gnu.org/software/coreutils/",
			   "mkfifo");
	LAB_00401f02:
	  ;
	  pcVar5 = "mkfifo";
	  uVar3 = 0x40cc23;
	}
      else
	{
	  func_0x00401500 ("\n%s online help: <%s>\n", "GNU coreutils",
			   "https://www.gnu.org/software/coreutils/");
	  lVar2 = func_0x004017e0 (5, 0);
	  if (lVar2 != 0)
	    {
	      iVar1 = func_0x004016f0 (lVar2, &DAT_0040cc8b, 3);
	      if (iVar1 != 0)
		{
		LAB_00401ed9:
		  ;
		  func_0x00401500
		    ("Report %s translation bugs to <https://translationproject.org/team/>\n",
		     "mkfifo");
		}
	    }
	  func_0x00401500 ("Full documentation at: <%s%s>\n",
			   "https://www.gnu.org/software/coreutils/",
			   "mkfifo");
	  uVar3 = 0x40dd4f;
	  if (pcVar5 == "mkfifo")
	    goto LAB_00401f02;
	}
      func_0x00401500
	("or available locally via: info \'(coreutils) %s%s\'\n", pcVar5,
	 uVar3);
    }
  while (true);
}

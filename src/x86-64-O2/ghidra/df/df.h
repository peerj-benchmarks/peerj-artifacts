typedef unsigned char   undefined;

typedef unsigned char    byte;
typedef unsigned char    dwfenc;
typedef unsigned int    dword;
typedef unsigned long    qword;
typedef unsigned char    undefined1;
typedef unsigned short    undefined2;
typedef unsigned int    undefined4;
typedef unsigned long    undefined6;
typedef unsigned long    undefined8;
typedef unsigned short    word;
#define unkbyte9   unsigned long long
#define unkbyte10   unsigned long long
#define unkbyte11   unsigned long long
#define unkbyte12   unsigned long long
#define unkbyte13   unsigned long long
#define unkbyte14   unsigned long long
#define unkbyte15   unsigned long long
#define unkbyte16   unsigned long long

#define unkuint9   unsigned long long
#define unkuint10   unsigned long long
#define unkuint11   unsigned long long
#define unkuint12   unsigned long long
#define unkuint13   unsigned long long
#define unkuint14   unsigned long long
#define unkuint15   unsigned long long
#define unkuint16   unsigned long long

#define unkint9   long long
#define unkint10   long long
#define unkint11   long long
#define unkint12   long long
#define unkint13   long long
#define unkint14   long long
#define unkint15   long long
#define unkint16   long long

#define unkfloat1   float
#define unkfloat2   float
#define unkfloat3   float
#define unkfloat5   double
#define unkfloat6   double
#define unkfloat7   double
#define unkfloat9   long double
#define unkfloat11   long double
#define unkfloat12   long double
#define unkfloat13   long double
#define unkfloat14   long double
#define unkfloat15   long double
#define unkfloat16   long double

#define BADSPACEBASE   void
#define code   void

#define ulong long
#define true 1
#define bool int
#define false 0
#define float10 int
#define ushort int
#define undefined7 long
#define undefined2 long
#define uint5 long
#define uint6 long

// Add definition for uint
typedef unsigned int    uint;

#define sbyte char

extern imperfection_wrapper(void);
typedef struct eh_frame_hdr eh_frame_hdr, *Peh_frame_hdr;

struct eh_frame_hdr {
    byte eh_frame_hdr_version; // Exception Handler Frame Header Version
    dwfenc eh_frame_pointer_encoding; // Exception Handler Frame Pointer Encoding
    dwfenc eh_frame_desc_entry_count_encoding; // Encoding of # of Exception Handler FDEs
    dwfenc eh_frame_table_encoding; // Exception Handler Table Encoding
};

typedef struct fde_table_entry fde_table_entry, *Pfde_table_entry;

struct fde_table_entry {
    dword initial_loc; // Initial Location
    dword data_loc; // Data location
};

typedef enum Elf64_DynTag {
    DT_AUDIT=1879047932,
    DT_AUXILIARY=2147483645,
    DT_BIND_NOW=24,
    DT_CHECKSUM=1879047672,
    DT_CONFIG=1879047930,
    DT_DEBUG=21,
    DT_DEPAUDIT=1879047931,
    DT_ENCODING=32,
    DT_FEATURE_1=1879047676,
    DT_FILTER=2147483647,
    DT_FINI=13,
    DT_FINI_ARRAY=26,
    DT_FINI_ARRAYSZ=28,
    DT_FLAGS=30,
    DT_FLAGS_1=1879048187,
    DT_GNU_CONFLICT=1879047928,
    DT_GNU_CONFLICTSZ=1879047670,
    DT_GNU_HASH=1879047925,
    DT_GNU_LIBLIST=1879047929,
    DT_GNU_LIBLISTSZ=1879047671,
    DT_GNU_PRELINKED=1879047669,
    DT_HASH=4,
    DT_INIT=12,
    DT_INIT_ARRAY=25,
    DT_INIT_ARRAYSZ=27,
    DT_JMPREL=23,
    DT_MOVEENT=1879047674,
    DT_MOVESZ=1879047675,
    DT_MOVETAB=1879047934,
    DT_NEEDED=1,
    DT_NULL=0,
    DT_PLTGOT=3,
    DT_PLTPAD=1879047933,
    DT_PLTPADSZ=1879047673,
    DT_PLTREL=20,
    DT_PLTRELSZ=2,
    DT_POSFLAG_1=1879047677,
    DT_PREINIT_ARRAYSZ=33,
    DT_REL=17,
    DT_RELA=7,
    DT_RELACOUNT=1879048185,
    DT_RELAENT=9,
    DT_RELASZ=8,
    DT_RELCOUNT=1879048186,
    DT_RELENT=19,
    DT_RELSZ=18,
    DT_RPATH=15,
    DT_RUNPATH=29,
    DT_SONAME=14,
    DT_STRSZ=10,
    DT_STRTAB=5,
    DT_SYMBOLIC=16,
    DT_SYMENT=11,
    DT_SYMINENT=1879047679,
    DT_SYMINFO=1879047935,
    DT_SYMINSZ=1879047678,
    DT_SYMTAB=6,
    DT_TEXTREL=22,
    DT_TLSDESC_GOT=1879047927,
    DT_TLSDESC_PLT=1879047926,
    DT_VERDEF=1879048188,
    DT_VERDEFNUM=1879048189,
    DT_VERNEED=1879048190,
    DT_VERNEEDNUM=1879048191,
    DT_VERSYM=1879048176
} Elf64_DynTag;

typedef enum Elf_ProgramHeaderType {
    PT_DYNAMIC=2,
    PT_GNU_EH_FRAME=1685382480,
    PT_GNU_RELRO=1685382482,
    PT_GNU_STACK=1685382481,
    PT_INTERP=3,
    PT_LOAD=1,
    PT_NOTE=4,
    PT_NULL=0,
    PT_PHDR=6,
    PT_SHLIB=5,
    PT_TLS=7
} Elf_ProgramHeaderType;

typedef struct Elf64_Dyn Elf64_Dyn, *PElf64_Dyn;

struct Elf64_Dyn {
    enum Elf64_DynTag d_tag;
    qword d_val;
};

typedef struct Elf64_Shdr Elf64_Shdr, *PElf64_Shdr;

typedef enum Elf_SectionHeaderType {
    SHT_CHECKSUM=1879048184,
    SHT_DYNAMIC=6,
    SHT_DYNSYM=11,
    SHT_FINI_ARRAY=15,
    SHT_GNU_ATTRIBUTES=1879048181,
    SHT_GNU_HASH=1879048182,
    SHT_GNU_LIBLIST=1879048183,
    SHT_GNU_verdef=1879048189,
    SHT_GNU_verneed=1879048190,
    SHT_GNU_versym=1879048191,
    SHT_GROUP=17,
    SHT_HASH=5,
    SHT_INIT_ARRAY=14,
    SHT_NOBITS=8,
    SHT_NOTE=7,
    SHT_NULL=0,
    SHT_PREINIT_ARRAY=16,
    SHT_PROGBITS=1,
    SHT_REL=9,
    SHT_RELA=4,
    SHT_SHLIB=10,
    SHT_STRTAB=3,
    SHT_SUNW_COMDAT=1879048187,
    SHT_SUNW_move=1879048186,
    SHT_SUNW_syminfo=1879048188,
    SHT_SYMTAB=2,
    SHT_SYMTAB_SHNDX=18
} Elf_SectionHeaderType;

struct Elf64_Shdr {
    dword sh_name;
    enum Elf_SectionHeaderType sh_type;
    qword sh_flags;
    qword sh_addr;
    qword sh_offset;
    qword sh_size;
    dword sh_link;
    dword sh_info;
    qword sh_addralign;
    qword sh_entsize;
};

typedef struct Elf64_Phdr Elf64_Phdr, *PElf64_Phdr;

struct Elf64_Phdr {
    enum Elf_ProgramHeaderType p_type;
    dword p_flags;
    qword p_offset;
    qword p_vaddr;
    qword p_paddr;
    qword p_filesz;
    qword p_memsz;
    qword p_align;
};

typedef struct Elf64_Rela Elf64_Rela, *PElf64_Rela;

struct Elf64_Rela {
    qword r_offset; // location to apply the relocation action
    qword r_info; // the symbol table index and the type of relocation
    qword r_addend; // a constant addend used to compute the relocatable field value
};

typedef struct Elf64_Sym Elf64_Sym, *PElf64_Sym;

struct Elf64_Sym {
    dword st_name;
    byte st_info;
    byte st_other;
    word st_shndx;
    qword st_value;
    qword st_size;
};

typedef struct Elf64_Ehdr Elf64_Ehdr, *PElf64_Ehdr;

struct Elf64_Ehdr {
    byte e_ident_magic_num;
    char e_ident_magic_str[3];
    byte e_ident_class;
    byte e_ident_data;
    byte e_ident_version;
    byte e_ident_pad[9];
    word e_type;
    word e_machine;
    dword e_version;
    qword e_entry;
    qword e_phoff;
    qword e_shoff;
    dword e_flags;
    word e_ehsize;
    word e_phentsize;
    word e_phnum;
    word e_shentsize;
    word e_shnum;
    word e_shstrndx;
};




undefined8 _DT_INIT(void);
void FUN_00401c70(void);
void thunk_FUN_0061b048(void);
ulong FUN_004022b0(undefined4 uParm1,char **ppcParm2);
void entry(void);
void FUN_004037e0(undefined8 *puParm1);
void FUN_00403810(void);
void FUN_00403890(void);
void FUN_00403910(void);
ulong FUN_00403950(ulong *puParm1,ulong uParm2);
ulong FUN_00403960(ulong *puParm1,ulong *puParm2);
ulong FUN_00403970(long lParm1);
undefined8 FUN_004039c0(long lParm1);
void FUN_00403a20(void);
void FUN_00403a30(void);
void FUN_00403aa0(int iParm1,long lParm2);
undefined * FUN_00403b60(byte bParm1,ulong uParm2,long lParm3);
byte * FUN_00403bc0(byte *pbParm1);
void FUN_00403c00(char *param_1,char *param_2,undefined8 param_3,char *param_4,undefined8 param_5,char param_6,char param_7,long *param_8,char param_9);
void FUN_00404680(uint uParm1);
void FUN_004048f0(void);
char * FUN_00404a40(undefined8 uParm1,long *plParm2);
char * FUN_00404cc0(char *pcParm1,uint uParm2);
void FUN_004052c0(undefined8 uParm1);
void FUN_004052d0(void);
void FUN_00405360(void);
ulong FUN_00405380(char *pcParm1);
undefined * FUN_004053e0(undefined8 uParm1);
char * thunk_FUN_0040546c(char *pcParm1);
char * FUN_0040546c(char *pcParm1);
void FUN_004054b0(long lParm1,undefined8 uParm2,undefined8 *puParm3);
ulong FUN_00405540(long lParm1,undefined8 uParm2,undefined8 *puParm3);
ulong FUN_00405580(ulong uParm1);
ulong FUN_00405620(ulong uParm1,ulong uParm2);
undefined FUN_00405630(long lParm1,long lParm2);;
long FUN_00405640(long *plParm1,undefined8 uParm2);
long FUN_00405670(long lParm1,long lParm2,long **pplParm3,char cParm4);
ulong FUN_00405770(float **ppfParm1,undefined8 uParm2,uint uParm3);
undefined8 FUN_00405800(long lParm1,long **pplParm2,char cParm3);
long FUN_00405950(long lParm1,long lParm2);
long * FUN_004059b0(ulong uParm1,undefined1 *puParm2,code *pcParm3,code *pcParm4,long lParm5);
void FUN_00405b60(long **pplParm1);
ulong FUN_00405c20(long *plParm1,ulong uParm2);
undefined8 FUN_00405e20(long lParm1,long lParm2,long *plParm3);
undefined8 FUN_00406090(undefined8 uParm1,undefined8 uParm2);
ulong FUN_004060d0(undefined8 *puParm1,ulong uParm2);
ulong FUN_00406100(undefined8 *puParm1,undefined8 *puParm2);
void FUN_00406140(undefined8 *puParm1);
char * FUN_00406160(ulong uParm1,long lParm2,ulong uParm3,ulong uParm4,ulong uParm5);
undefined8 FUN_00406b90(char *pcParm1,uint *puParm2,long *plParm3);
char * FUN_00406d30(ulong uParm1,long lParm2);
undefined *FUN_00406d80(long lParm1,undefined *puParm2,long lParm3,undefined8 *puParm4,int iParm5,ulong uParm6);
long FUN_00407180(undefined8 uParm1,ulong *puParm2,uint uParm3,uint uParm4);
ulong FUN_00407220(byte *pbParm1,long lParm2,ulong uParm3);
void FUN_004073f0(undefined8 uParm1,ulong uParm2);
void FUN_00407420(long lParm1);
undefined8 * FUN_004074c0(undefined8 *puParm1,int iParm2);
undefined * FUN_00407530(char *pcParm1,int iParm2);
ulong FUN_00407600(undefined *param_1,ulong param_2,long param_3,ulong param_4,uint param_5,ulong param_6,long param_7,char *param_8,char *param_9);
undefined1 * FUN_00408500(uint uParm1,undefined8 uParm2,undefined8 uParm3,uint *puParm4);
void FUN_004086b0(uint uParm1,undefined8 uParm2,undefined8 uParm3);
void FUN_004086e0(ulong uParm1,undefined8 uParm2);
void FUN_004086f0(undefined8 uParm1,undefined8 uParm2,byte bParm3);
void FUN_00408790(undefined8 uParm1);
void FUN_004087b0(uint uParm1,undefined8 uParm2,undefined8 uParm3);
void FUN_00408840(undefined8 uParm1);
ulong FUN_00408860(int *piParm1);
void FUN_004088b0(uint *puParm1);
void FUN_004088d0(int *piParm1);
long FUN_004088f0(undefined8 uParm1,long lParm2,undefined8 uParm3,undefined8 uParm4,long *plParm5,undefined8 uParm6);
void FUN_00408b50(void);
void FUN_00408bc0(void);
void FUN_00408c50(long lParm1);
long FUN_00408c70(long lParm1,long lParm2);
void FUN_00408cb0(undefined8 uParm1,undefined8 uParm2);
void FUN_00408ce0(undefined8 uParm1);
void FUN_00408d00(void);
long FUN_00408d30(void);
void FUN_00408d60(uint uParm1,int iParm2,undefined uParm3,long lParm4,undefined8 uParm5);
ulong FUN_00408de0(byte *pbParm1,byte **ppbParm2,uint uParm3,ulong *puParm4,long lParm5);
void FUN_00409240(undefined4 uParm1,undefined4 uParm2,undefined4 uParm3,undefined4 uParm4,undefined4 uParm5,undefined4 uParm6,undefined4 uParm7,undefined4 uParm8,undefined8 param_1,undefined8 param_2,undefined8 param_3,undefined8 param_4,undefined8 param_5,undefined8 param_6);
undefined8 FUN_004092d0(ulong uParm1,ulong uParm2);
void FUN_00409320(int *piParm1);
undefined8 FUN_00409360(uint *puParm1,undefined8 uParm2);
ulong FUN_004093a0(char *pcParm1);
void FUN_00409650(undefined8 uParm1);
void FUN_00409690(undefined8 uParm1,uint uParm2,undefined8 uParm3,undefined8 uParm4);
void FUN_00409700(void);
void FUN_00409740(undefined4 uParm1,undefined4 uParm2,undefined4 uParm3,undefined4 uParm4,undefined4 uParm5,undefined4 uParm6,undefined4 uParm7,undefined4 uParm8,ulong param_1,ulong param_2,undefined8 param_3,undefined8 param_4,undefined8 param_5,undefined8 param_6);
undefined8 FUN_00409820(undefined8 uParm1,undefined8 uParm2,long *plParm3);
long FUN_00409890(long lParm1,ulong uParm2);
void FUN_00409d10(long lParm1,int *piParm2);
ulong FUN_00409df0(int param_1,undefined8 *param_2,char *param_3,long *param_4,int *param_5,int param_6,int *param_7,int param_8,undefined8 param_9);
ulong FUN_0040a370(uint param_1,undefined8 *param_2,char *param_3,long param_4,undefined8 param_5,ulong param_6,uint *param_7,int param_8);
void FUN_0040a8c0(void);
void FUN_0040a920(void);
undefined8 FUN_0040a940(long lParm1,long lParm2);
void FUN_0040a9c0(long lParm1);
ulong FUN_0040a9e0(uint *puParm1,byte *pbParm2,long lParm3);
ulong * FUN_0040aa50(ulong *puParm1,char cParm2,ulong uParm3);
void FUN_0040ab40(long lParm1);
void FUN_0040abe0(undefined8 *puParm1);
char * FUN_0040ac20(void);
ulong FUN_0040b2e0(uint param_1,undefined8 param_2,uint param_3,ulong param_4);
void FUN_0040b400(long lParm1,long lParm2);
ulong FUN_0040b440(long lParm1,long lParm2);
ulong FUN_0040b490(long lParm1,ulong uParm2,long *plParm3);
long FUN_0040b5c0(long lParm1,long lParm2,long lParm3,ulong uParm4);
char * FUN_0040b900(char *pcParm1,char *pcParm2);
ulong FUN_0040bca0(long *plParm1,undefined8 uParm2,undefined8 uParm3);
ulong FUN_0040bd10(undefined8 uParm1,undefined8 uParm2,undefined8 uParm3);
ulong FUN_0040bde0(int *piParm1,long lParm2);
void FUN_0040be60(ulong uParm1);
long FUN_0040beb0(undefined8 uParm1,ulong uParm2);
long FUN_0040bfd0(undefined8 uParm1,long *plParm2,long lParm3,long lParm4);
ulong FUN_0040c100(ulong uParm1,char cParm2);
ulong FUN_0040c160(undefined8 uParm1);
void FUN_0040c1d0(undefined8 param_1,byte param_2,ulong param_3);
undefined8 FUN_0040c220(void);
ulong FUN_0040c230(ulong uParm1);
ulong FUN_0040c2d0(char *pcParm1,ulong uParm2);
char * FUN_0040c300(void);
ulong FUN_0040c650(uint uParm1);
ulong FUN_0040c690(long param_1,long param_2,char param_3,char param_4,char param_5,char param_6,char param_7);
undefined8 FUN_0040c710(long param_1,undefined8 param_2,char param_3,char param_4,char param_5,char param_6,char param_7);
ulong FUN_0040c770(uint uParm1,byte *pbParm2,undefined8 uParm3,undefined8 uParm4,undefined8 uParm5);
void FUN_0040cb80(undefined8 uParm1);
ulong FUN_0040cc10(ulong param_1,undefined8 param_2,ulong param_3);
undefined8 FUN_0040cd50(ulong uParm1);
ulong FUN_0040cdc0(long lParm1);
undefined8 FUN_0040ce50(void);
void FUN_0040ce60(void);
undefined8 FUN_0040ce70(ulong uParm1,long lParm2,long lParm3,ulong uParm4);
void FUN_0040cf30(undefined8 uParm1,undefined8 uParm2,undefined8 uParm3);
undefined8 FUN_0040cf40(long lParm1,undefined8 uParm2,undefined8 uParm3);
undefined4 * FUN_0040cfb0(ulong uParm1,uint *puParm2,ulong uParm3,uint *puParm4,long *plParm5);
ulong FUN_0040d110(void);
uint * FUN_0040d380(ulong uParm1,long lParm2,ulong uParm3,uint *puParm4,ulong *puParm5);
char * FUN_0040d940(int iParm1,long lParm2,long lParm3,long lParm4,uint uParm5);
void FUN_0040dee0(uint param_1);
ulong FUN_0040e060(void);
void FUN_0040e280(undefined8 uParm1,uint uParm2);
undefined8 *FUN_0040e3f0(undefined8 *puParm1,undefined8 *puParm2,undefined8 *puParm3,undefined8 uParm4);
double FUN_004139c0(int *piParm1);
void FUN_00413a00(uint *param_1);
void FUN_00413a80(undefined8 uParm1);
ulong FUN_00413a90(long *plParm1,ulong *puParm2,int iParm3,long lParm4);
undefined8 FUN_00413bf0(uint *puParm1,ulong *puParm2);
undefined8 FUN_00413e10(byte *pbParm1,ulong *puParm2,byte **ppbParm3);
ulong FUN_00414a60(void);
ulong FUN_00414a90(void);
void FUN_00414ad0(void);
undefined8 _DT_FINI(void);
undefined FUN_0061b000();
undefined FUN_0061b008();
undefined FUN_0061b010();
undefined FUN_0061b018();
undefined FUN_0061b020();
undefined FUN_0061b028();
undefined FUN_0061b030();
undefined FUN_0061b038();
undefined FUN_0061b040();
undefined FUN_0061b048();
undefined FUN_0061b050();
undefined FUN_0061b058();
undefined FUN_0061b060();
undefined FUN_0061b068();
undefined FUN_0061b070();
undefined FUN_0061b078();
undefined FUN_0061b080();
undefined FUN_0061b088();
undefined FUN_0061b090();
undefined FUN_0061b098();
undefined FUN_0061b0a0();
undefined FUN_0061b0a8();
undefined FUN_0061b0b0();
undefined FUN_0061b0b8();
undefined FUN_0061b0c0();
undefined FUN_0061b0c8();
undefined FUN_0061b0d0();
undefined FUN_0061b0d8();
undefined FUN_0061b0e0();
undefined FUN_0061b0e8();
undefined FUN_0061b0f0();
undefined FUN_0061b0f8();
undefined FUN_0061b100();
undefined FUN_0061b108();
undefined FUN_0061b110();
undefined FUN_0061b118();
undefined FUN_0061b120();
undefined FUN_0061b128();
undefined FUN_0061b130();
undefined FUN_0061b138();
undefined FUN_0061b140();
undefined FUN_0061b148();
undefined FUN_0061b150();
undefined FUN_0061b158();
undefined FUN_0061b160();
undefined FUN_0061b168();
undefined FUN_0061b170();
undefined FUN_0061b178();
undefined FUN_0061b180();
undefined FUN_0061b188();
undefined FUN_0061b190();
undefined FUN_0061b198();
undefined FUN_0061b1a0();
undefined FUN_0061b1a8();
undefined FUN_0061b1b0();
undefined FUN_0061b1b8();
undefined FUN_0061b1c0();
undefined FUN_0061b1c8();
undefined FUN_0061b1d0();
undefined FUN_0061b1d8();
undefined FUN_0061b1e0();
undefined FUN_0061b1e8();
undefined FUN_0061b1f0();
undefined FUN_0061b1f8();
undefined FUN_0061b200();
undefined FUN_0061b208();
undefined FUN_0061b210();
undefined FUN_0061b218();
undefined FUN_0061b220();
undefined FUN_0061b228();
undefined FUN_0061b230();
undefined FUN_0061b238();
undefined FUN_0061b240();
undefined FUN_0061b248();
undefined FUN_0061b250();
undefined FUN_0061b258();
undefined FUN_0061b260();
undefined FUN_0061b268();
undefined FUN_0061b270();
undefined FUN_0061b278();
undefined FUN_0061b280();
undefined FUN_0061b288();
undefined FUN_0061b290();
undefined FUN_0061b298();
undefined FUN_0061b2a0();
undefined FUN_0061b2a8();
undefined FUN_0061b2b0();
undefined FUN_0061b2b8();
undefined FUN_0061b2c0();
undefined FUN_0061b2c8();
undefined FUN_0061b2d0();
undefined FUN_0061b2d8();
undefined FUN_0061b2e0();
undefined FUN_0061b2e8();
undefined FUN_0061b2f0();
undefined FUN_0061b2f8();
undefined FUN_0061b300();
undefined FUN_0061b308();
undefined FUN_0061b310();



#include "df.h"

long null_ARRAY_0061aa6_0_8_;
long null_ARRAY_0061aa6_8_8_;
long null_ARRAY_0061ab6_0_8_;
long null_ARRAY_0061ab6_16_8_;
long null_ARRAY_0061ab6_24_8_;
long null_ARRAY_0061ab6_40_8_;
long null_ARRAY_0061ab6_48_8_;
long null_ARRAY_0061ab6_8_8_;
long null_ARRAY_0061ad0_0_8_;
long null_ARRAY_0061ad0_16_8_;
long null_ARRAY_0061ad0_24_8_;
long null_ARRAY_0061ad0_32_8_;
long null_ARRAY_0061ad0_40_8_;
long null_ARRAY_0061ad0_48_8_;
long null_ARRAY_0061ad0_8_8_;
long null_ARRAY_0061ad4_0_4_;
long null_ARRAY_0061ad4_16_8_;
long null_ARRAY_0061ad4_4_4_;
long null_ARRAY_0061ad4_8_4_;
long local_2f_1_1_;
long local_4_4_4_;
long local_5b_1_1_;
long local_9d_0_4_;
long local_a0_0_8_;
long local_a0_4_6_;
long local_a0_8_2_;
long local_a1_4_4_;
long local_a2_4_4_;
long local_a3_0_4_;
long local_a3_4_4_;
long local_a4_0_2_;
long local_a4_0_4_;
long local_a4_0_8_;
long local_a4_1_9_;
long local_a4_4_6_;
long local_a4_8_2_;
long local_a5_0_4_;
long local_a5_4_4_;
long DAT_00000010;
long DAT_00414ba9;
long DAT_00414bab;
long DAT_00414bae;
long DAT_00414c33;
long DAT_00414c37;
long DAT_00414c79;
long DAT_00414c7c;
long DAT_00414c7f;
long DAT_00414cce;
long DAT_00414d00;
long DAT_00415c9b;
long DAT_00415c9c;
long DAT_00415d80;
long DAT_00415e08;
long DAT_00415e0e;
long DAT_00415e10;
long DAT_00415e14;
long DAT_00415e18;
long DAT_00415e1b;
long DAT_00415e1f;
long DAT_00415e23;
long DAT_004163ab;
long DAT_00416778;
long DAT_00416779;
long DAT_004169cd;
long DAT_004169d7;
long DAT_00416a24;
long DAT_00416b4d;
long DAT_00416b66;
long DAT_00416b87;
long DAT_00416bd5;
long DAT_00416be0;
long DAT_00416bf6;
long DAT_00416c20;
long DAT_00416cad;
long DAT_00417786;
long DAT_0061a2e0;
long DAT_0061a2f0;
long DAT_0061a300;
long DAT_0061aa08;
long DAT_0061aa70;
long DAT_0061aa74;
long DAT_0061aa78;
long DAT_0061aa7c;
long DAT_0061aa80;
long DAT_0061aa90;
long DAT_0061aaa0;
long DAT_0061aaa8;
long DAT_0061aac0;
long DAT_0061aac8;
long DAT_0061ab20;
long DAT_0061ab28;
long DAT_0061ab30;
long DAT_0061ab38;
long DAT_0061ab40;
long DAT_0061ab98;
long DAT_0061ab99;
long DAT_0061aba0;
long DAT_0061aba8;
long DAT_0061abb0;
long DAT_0061abb8;
long DAT_0061abbc;
long DAT_0061abbd;
long DAT_0061abc0;
long DAT_0061abc8;
long DAT_0061abcc;
long DAT_0061abcd;
long DAT_0061abce;
long DAT_0061abd0;
long DAT_0061abd8;
long DAT_0061abe0;
long DAT_0061abe8;
long DAT_0061ad78;
long DAT_0061ad80;
long DAT_0061ad88;
long DAT_0061ad90;
long DAT_0061ad98;
long DAT_0061ada8;
long fde_004183a8;
long null_ARRAY_00414f00;
long null_ARRAY_00415d20;
long null_ARRAY_00415d90;
long null_ARRAY_00415da8;
long null_ARRAY_00416d00;
long null_ARRAY_00416e00;
long null_ARRAY_00417a20;
long null_ARRAY_00417c20;
long null_ARRAY_0061a7c0;
long null_ARRAY_0061aa20;
long null_ARRAY_0061aa60;
long null_ARRAY_0061aae0;
long null_ARRAY_0061ab60;
long null_ARRAY_0061ac00;
long null_ARRAY_0061ad00;
long null_ARRAY_0061ad40;
long PTR_DAT_00414e00;
long PTR_DAT_0061aa00;
long PTR_null_ARRAY_0061aa58;
long register0x00000020;
long stack0x00000008;
void
FUN_00404680 (uint uParm1)
{
  int iVar1;
  long lVar2;
  undefined8 uVar3;
  undefined **ppuVar4;
  undefined *puVar5;
  undefined *local_88;
  char *local_80;
  undefined8 local_78;
  char *local_70;
  char *local_68;
  char *local_60;
  char *local_58;
  char *local_50;
  char *local_48;
  char *local_40;
  char *local_38;
  char *local_30;
  undefined8 local_28;
  undefined8 local_20;

  if (uParm1 == 0)
    goto LAB_004046ad;
  func_0x00401f00 (DAT_0061aaa0, "Try \'%s --help\' for more information.\n",
		   DAT_0061abe8);
  do
    {
      func_0x00402160 ((ulong) uParm1);
    LAB_004046ad:
      ;
      func_0x00401ca0 ("Usage: %s [OPTION]... [FILE]...\n", DAT_0061abe8);
      uVar3 = DAT_0061aa80;
      func_0x00401f10
	("Show information about the file system on which each FILE resides,\nor all file systems by default.\n",
	 DAT_0061aa80);
      func_0x00401f10
	("\nMandatory arguments to long options are mandatory for short options too.\n",
	 uVar3);
      func_0x00401f10
	("  -a, --all             include pseudo, duplicate, inaccessible file systems\n  -B, --block-size=SIZE  scale sizes by SIZE before printing them; e.g.,\n                           \'-BM\' prints sizes in units of 1,048,576 bytes;\n                           see SIZE format below\n  -h, --human-readable  print sizes in powers of 1024 (e.g., 1023M)\n  -H, --si              print sizes in powers of 1000 (e.g., 1.1G)\n",
	 uVar3);
      func_0x00401f10
	("  -i, --inodes          list inode information instead of block usage\n  -k                    like --block-size=1K\n  -l, --local           limit listing to local file systems\n      --no-sync         do not invoke sync before getting usage info (default)\n",
	 uVar3);
      func_0x00401f10
	("      --output[=FIELD_LIST]  use the output format defined by FIELD_LIST,\n                               or print all fields if FIELD_LIST is omitted.\n  -P, --portability     use the POSIX output format\n      --sync            invoke sync before getting usage info\n",
	 uVar3);
      func_0x00401f10
	("      --total           elide all entries insignificant to available space,\n                          and produce a grand total\n",
	 uVar3);
      func_0x00401f10
	("  -t, --type=TYPE       limit listing to file systems of type TYPE\n  -T, --print-type      print file system type\n  -x, --exclude-type=TYPE   limit listing to file systems not of type TYPE\n  -v                    (ignored)\n",
	 uVar3);
      func_0x00401f10 ("      --help     display this help and exit\n",
		       uVar3);
      func_0x00401f10
	("      --version  output version information and exit\n", uVar3);
      func_0x00401ca0
	("\nDisplay values are in units of the first available SIZE from --block-size,\nand the %s_BLOCK_SIZE, BLOCK_SIZE and BLOCKSIZE environment variables.\nOtherwise, units default to 1024 bytes (or 512 if POSIXLY_CORRECT is set).\n",
	 &DAT_00414bae);
      func_0x00401f10
	("\nThe SIZE argument is an integer and optional unit (example: 10K is 10*1024).\nUnits are K,M,G,T,P,E,Z,Y (powers of 1024) or KB,MB,... (powers of 1000).\n",
	 uVar3);
      func_0x00401f10
	("\nFIELD_LIST is a comma-separated list of columns to be included.  Valid\nfield names are: \'source\', \'fstype\', \'itotal\', \'iused\', \'iavail\', \'ipcent\',\n\'size\', \'used\', \'avail\', \'pcent\', \'file\' and \'target\' (see info page).\n",
	 uVar3);
      local_88 = &DAT_00414ba9;
      local_80 = "test invocation";
      puVar5 = &DAT_00414ba9;
      local_78 = 0x414c12;
      local_70 = "Multi-call invocation";
      local_68 = "sha224sum";
      local_60 = "sha2 utilities";
      local_58 = "sha256sum";
      local_50 = "sha2 utilities";
      local_48 = "sha384sum";
      local_40 = "sha2 utilities";
      local_38 = "sha512sum";
      local_30 = "sha2 utilities";
      local_28 = 0;
      local_20 = 0;
      ppuVar4 = &local_88;
      do
	{
	  iVar1 = func_0x00402080 (&DAT_00414bab, puVar5);
	  if (iVar1 == 0)
	    break;
	  ppuVar4 = ppuVar4 + 2;
	  puVar5 = *ppuVar4;
	}
      while (puVar5 != (undefined *) 0x0);
      puVar5 = ppuVar4[1];
      if (puVar5 == (undefined *) 0x0)
	{
	  func_0x00401ca0 ("\n%s online help: <%s>\n", "GNU coreutils",
			   "https://www.gnu.org/software/coreutils/");
	  lVar2 = func_0x004020e0 (5, 0);
	  if (lVar2 != 0)
	    {
	      iVar1 = func_0x00401f80 (lVar2, &DAT_00414c33, 3);
	      if (iVar1 != 0)
		{
		  puVar5 = &DAT_00414bab;
		  goto LAB_004048b9;
		}
	    }
	  func_0x00401ca0 ("Full documentation at: <%s%s>\n",
			   "https://www.gnu.org/software/coreutils/",
			   &DAT_00414bab);
	LAB_004048e2:
	  ;
	  puVar5 = &DAT_00414bab;
	  uVar3 = 0x414bcb;
	}
      else
	{
	  func_0x00401ca0 ("\n%s online help: <%s>\n", "GNU coreutils",
			   "https://www.gnu.org/software/coreutils/");
	  lVar2 = func_0x004020e0 (5, 0);
	  if (lVar2 != 0)
	    {
	      iVar1 = func_0x00401f80 (lVar2, &DAT_00414c33, 3);
	      if (iVar1 != 0)
		{
		LAB_004048b9:
		  ;
		  func_0x00401ca0
		    ("Report %s translation bugs to <https://translationproject.org/team/>\n",
		     &DAT_00414bab);
		}
	    }
	  func_0x00401ca0 ("Full documentation at: <%s%s>\n",
			   "https://www.gnu.org/software/coreutils/",
			   &DAT_00414bab);
	  uVar3 = 0x416b4c;
	  if (puVar5 == &DAT_00414bab)
	    goto LAB_004048e2;
	}
      func_0x00401ca0
	("or available locally via: info \'(coreutils) %s%s\'\n", puVar5,
	 uVar3);
    }
  while (true);
}


#include "sum.h"

long null_ARRAY_0061044_0_8_;
long null_ARRAY_0061044_8_8_;
long null_ARRAY_0061064_0_8_;
long null_ARRAY_0061064_16_8_;
long null_ARRAY_0061064_24_8_;
long null_ARRAY_0061064_32_8_;
long null_ARRAY_0061064_40_8_;
long null_ARRAY_0061064_48_8_;
long null_ARRAY_0061064_8_8_;
long null_ARRAY_0061068_0_4_;
long null_ARRAY_0061068_16_8_;
long null_ARRAY_0061068_4_4_;
long null_ARRAY_0061068_8_4_;
long local_2f_1_1_;
long local_4_4_4_;
long local_5b_1_1_;
long local_9d_0_4_;
long local_a0_0_8_;
long local_a0_4_6_;
long local_a0_8_2_;
long local_a1_4_4_;
long local_a2_4_4_;
long local_a3_0_4_;
long local_a3_4_4_;
long local_a4_0_2_;
long local_a4_0_4_;
long local_a4_0_8_;
long local_a4_1_9_;
long local_a4_4_6_;
long local_a4_8_2_;
long local_a5_0_4_;
long local_a5_4_4_;
long DAT_00000010;
long DAT_0040d54f;
long DAT_0040d5a4;
long DAT_0040d5aa;
long DAT_0040d5d3;
long DAT_0040d5f9;
long DAT_0040d918;
long DAT_0040da00;
long DAT_0040da04;
long DAT_0040da08;
long DAT_0040da0b;
long DAT_0040da0d;
long DAT_0040da11;
long DAT_0040da15;
long DAT_0040dfab;
long DAT_0040e355;
long DAT_0040e459;
long DAT_0040e45f;
long DAT_0040e461;
long DAT_0040e462;
long DAT_0040e480;
long DAT_0040e484;
long DAT_0040e506;
long DAT_00610000;
long DAT_00610010;
long DAT_00610020;
long DAT_006103e8;
long DAT_00610450;
long DAT_00610454;
long DAT_00610458;
long DAT_0061045c;
long DAT_00610480;
long DAT_00610488;
long DAT_00610490;
long DAT_006104a0;
long DAT_006104a8;
long DAT_006104c0;
long DAT_006104c8;
long DAT_00610510;
long DAT_00610518;
long DAT_00610520;
long DAT_00610528;
long DAT_006106b8;
long DAT_006106c0;
long DAT_006106c8;
long DAT_006106d8;
long fde_0040eeb0;
long null_ARRAY_0040d880;
long null_ARRAY_0040d998;
long null_ARRAY_0040e7a0;
long null_ARRAY_0040e9d0;
long null_ARRAY_00610440;
long null_ARRAY_006104e0;
long null_ARRAY_00610540;
long null_ARRAY_00610640;
long null_ARRAY_00610680;
long PTR_DAT_006103e0;
long PTR_null_ARRAY_00610438;
long register0x00000020;
long stack0x00000008;
void
FUN_004020c0 (uint uParm1)
{
  int iVar1;
  long lVar2;
  undefined8 uVar3;
  undefined **ppuVar4;
  undefined *puVar5;
  undefined *local_88;
  char *local_80;
  undefined8 local_78;
  char *local_70;
  char *local_68;
  char *local_60;
  char *local_58;
  char *local_50;
  char *local_48;
  char *local_40;
  undefined *local_38;
  char *local_30;
  undefined8 local_28;
  undefined8 local_20;

  if (uParm1 == 0)
    goto LAB_004020ed;
  func_0x00401790 (DAT_006104a0, "Try \'%s --help\' for more information.\n",
		   DAT_00610528);
  do
    {
      func_0x00401960 ((ulong) uParm1);
    LAB_004020ed:
      ;
      func_0x00401600 ("Usage: %s [OPTION]... [FILE]...\n", DAT_00610528);
      uVar3 = DAT_00610480;
      func_0x004017a0 ("Print checksum and block counts for each FILE.\n",
		       DAT_00610480);
      func_0x004017a0
	("\nWith no FILE, or when FILE is -, read standard input.\n", uVar3);
      func_0x004017a0
	("\n  -r              use BSD sum algorithm, use 1K blocks\n  -s, --sysv      use System V sum algorithm, use 512 bytes blocks\n",
	 uVar3);
      func_0x004017a0 ("      --help     display this help and exit\n",
		       uVar3);
      func_0x004017a0
	("      --version  output version information and exit\n", uVar3);
      local_88 = &DAT_0040d54f;
      local_80 = "test invocation";
      puVar5 = &DAT_0040d54f;
      local_78 = 0x40d5b2;
      local_70 = "Multi-call invocation";
      local_68 = "sha224sum";
      local_60 = "sha2 utilities";
      local_58 = "sha256sum";
      local_50 = "sha2 utilities";
      local_48 = "sha384sum";
      local_40 = "sha2 utilities";
      local_38 = &DAT_0040d5a4;
      local_30 = "sha2 utilities";
      local_28 = 0;
      local_20 = 0;
      ppuVar4 = &local_88;
      do
	{
	  iVar1 = func_0x004018a0 (&DAT_0040d5aa, puVar5);
	  if (iVar1 == 0)
	    break;
	  ppuVar4 = ppuVar4 + 2;
	  puVar5 = *ppuVar4;
	}
      while (puVar5 != (undefined *) 0x0);
      puVar5 = ppuVar4[1];
      if (puVar5 == (undefined *) 0x0)
	{
	  func_0x00401600 ("\n%s online help: <%s>\n", "GNU coreutils",
			   "https://www.gnu.org/software/coreutils/");
	  lVar2 = func_0x00401900 (5, 0);
	  if (lVar2 != 0)
	    {
	      iVar1 = func_0x00401810 (lVar2, &DAT_0040d5d3, 3);
	      if (iVar1 != 0)
		{
		  puVar5 = &DAT_0040d5aa;
		  goto LAB_00402299;
		}
	    }
	  func_0x00401600 ("Full documentation at: <%s%s>\n",
			   "https://www.gnu.org/software/coreutils/",
			   &DAT_0040d5aa);
	LAB_004022c2:
	  ;
	  puVar5 = &DAT_0040d5aa;
	  uVar3 = 0x40d56b;
	}
      else
	{
	  func_0x00401600 ("\n%s online help: <%s>\n", "GNU coreutils",
			   "https://www.gnu.org/software/coreutils/");
	  lVar2 = func_0x00401900 (5, 0);
	  if (lVar2 != 0)
	    {
	      iVar1 = func_0x00401810 (lVar2, &DAT_0040d5d3, 3);
	      if (iVar1 != 0)
		{
		LAB_00402299:
		  ;
		  func_0x00401600
		    ("Report %s translation bugs to <https://translationproject.org/team/>\n",
		     &DAT_0040d5aa);
		}
	    }
	  func_0x00401600 ("Full documentation at: <%s%s>\n",
			   "https://www.gnu.org/software/coreutils/",
			   &DAT_0040d5aa);
	  uVar3 = 0x40e47f;
	  if (puVar5 == &DAT_0040d5aa)
	    goto LAB_004022c2;
	}
      func_0x00401600
	("or available locally via: info \'(coreutils) %s%s\'\n", puVar5,
	 uVar3);
    }
  while (true);
}

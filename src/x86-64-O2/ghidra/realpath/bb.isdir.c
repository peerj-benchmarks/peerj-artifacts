
#include "realpath.h"

long null_ARRAY_0061254_0_8_;
long null_ARRAY_0061254_8_8_;
long null_ARRAY_0061274_0_8_;
long null_ARRAY_0061274_16_8_;
long null_ARRAY_0061274_24_8_;
long null_ARRAY_0061274_32_8_;
long null_ARRAY_0061274_40_8_;
long null_ARRAY_0061274_48_8_;
long null_ARRAY_0061274_8_8_;
long null_ARRAY_0061278_0_4_;
long null_ARRAY_0061278_16_8_;
long null_ARRAY_0061278_4_4_;
long null_ARRAY_0061278_8_4_;
long local_2f_1_1_;
long local_4_4_4_;
long local_5b_1_1_;
long local_9d_0_4_;
long local_a0_0_8_;
long local_a0_4_6_;
long local_a0_8_2_;
long local_a1_4_4_;
long local_a2_4_4_;
long local_a3_0_4_;
long local_a3_4_4_;
long local_a4_0_2_;
long local_a4_0_4_;
long local_a4_0_8_;
long local_a4_1_9_;
long local_a4_4_6_;
long local_a4_8_2_;
long local_a5_0_4_;
long local_a5_4_4_;
long _bVar2;
long DAT_00000010;
long DAT_0040eecf;
long DAT_0040ef00;
long DAT_0040ef5c;
long DAT_0040f620;
long DAT_0040f621;
long DAT_0040f622;
long DAT_0040f6c8;
long DAT_0040f6ce;
long DAT_0040f6d0;
long DAT_0040f6d4;
long DAT_0040f6d8;
long DAT_0040f6db;
long DAT_0040f6dd;
long DAT_0040f6e1;
long DAT_0040f6e5;
long DAT_0040fc6b;
long DAT_00410015;
long DAT_00410119;
long DAT_0041011f;
long DAT_00410131;
long DAT_00410132;
long DAT_00410150;
long DAT_00410154;
long DAT_004101d6;
long DAT_006120c0;
long DAT_006120d0;
long DAT_006120e0;
long DAT_006124e0;
long DAT_006124f0;
long DAT_00612550;
long DAT_00612554;
long DAT_00612558;
long DAT_0061255c;
long DAT_00612580;
long DAT_00612590;
long DAT_006125a0;
long DAT_006125a8;
long DAT_006125c0;
long DAT_006125c8;
long DAT_00612610;
long DAT_00612618;
long DAT_00612620;
long DAT_00612621;
long DAT_00612628;
long DAT_00612630;
long DAT_00612638;
long DAT_006127b8;
long DAT_006127c0;
long DAT_006127c8;
long DAT_006127d0;
long DAT_006127d8;
long DAT_006127e8;
long _DYNAMIC;
long fde_00410c80;
long null_ARRAY_0040f480;
long null_ARRAY_0040f660;
long null_ARRAY_00410460;
long null_ARRAY_00410680;
long null_ARRAY_00612540;
long null_ARRAY_006125e0;
long null_ARRAY_00612640;
long null_ARRAY_00612740;
long null_ARRAY_00612780;
long PTR_DAT_006124e8;
long PTR_null_ARRAY_00612538;
long register0x00000020;
ulong
FUN_00402300 (undefined8 uParm1)
{
  int iVar1;
  uint *puVar2;
  long lVar3;
  undefined8 uVar4;
  undefined **ppuVar5;
  char *pcVar6;
  undefined *puVar7;
  ulong uVar8;
  undefined *puStack288;
  char *pcStack280;
  undefined8 uStack272;
  char *pcStack264;
  undefined *puStack256;
  char *pcStack248;
  char *pcStack240;
  char *pcStack232;
  char *pcStack224;
  char *pcStack216;
  char *pcStack208;
  char *pcStack200;
  undefined8 uStack192;
  undefined8 uStack184;
  undefined8 uStack168;
  undefined auStack152[24];
  uint local_80;

  iVar1 = FUN_00406940 (uParm1, auStack152);
  if (iVar1 == 0)
    {
      return (ulong) (local_80 & 0xf000 |
		      (uint) ((local_80 & 0xf000) == 0x4000));
    }
  imperfection_wrapper ();	//  uStack168 = FUN_00404e30(4, uParm1);
  puVar2 = (uint *) func_0x00401af0 ();
  uVar8 = 1;
  imperfection_wrapper ();	//  FUN_00405540(1, (ulong)*puVar2, "cannot stat %s", uStack168);
  if ((int) uVar8 == 0)
    goto LAB_0040238d;
  func_0x00401950 (DAT_006125a0, "Try \'%s --help\' for more information.\n",
		   DAT_00612638);
  do
    {
      func_0x00401b00 (uVar8 & 0xffffffff);
    LAB_0040238d:
      ;
      func_0x00401770 ("Usage: %s [OPTION]... FILE...\n", DAT_00612638);
      uVar4 = DAT_00612580;
      func_0x00401960
	("Print the resolved absolute file name;\nall but the last component must exist\n\n",
	 DAT_00612580);
      func_0x00401960
	("  -e, --canonicalize-existing  all components of the path must exist\n  -m, --canonicalize-missing   no path components need exist or be a directory\n  -L, --logical                resolve \'..\' components before symlinks\n  -P, --physical               resolve symlinks as encountered (default)\n  -q, --quiet                  suppress most error messages\n      --relative-to=DIR        print the resolved path relative to DIR\n      --relative-base=DIR      print absolute paths unless paths below DIR\n  -s, --strip, --no-symlinks   don\'t expand symlinks\n  -z, --zero                   end each output line with NUL, not newline\n\n",
	 uVar4);
      func_0x00401960 ("      --help     display this help and exit\n",
		       uVar4);
      func_0x00401960
	("      --version  output version information and exit\n", uVar4);
      puStack288 = &DAT_0040eecf;
      pcStack280 = "test invocation";
      puVar7 = &DAT_0040eecf;
      uStack272 = 0x40ef3b;
      pcStack264 = "Multi-call invocation";
      puStack256 = &DAT_0040ef00;
      pcStack248 = "sha2 utilities";
      pcStack240 = "sha256sum";
      pcStack232 = "sha2 utilities";
      pcStack224 = "sha384sum";
      pcStack216 = "sha2 utilities";
      pcStack208 = "sha512sum";
      pcStack200 = "sha2 utilities";
      uStack192 = 0;
      uStack184 = 0;
      ppuVar5 = &puStack288;
      do
	{
	  iVar1 = func_0x00401a40 ("realpath", puVar7);
	  if (iVar1 == 0)
	    break;
	  ppuVar5 = ppuVar5 + 2;
	  puVar7 = *ppuVar5;
	}
      while (puVar7 != (undefined *) 0x0);
      pcVar6 = ppuVar5[1];
      if (pcVar6 == (char *) 0x0)
	{
	  func_0x00401770 ("\n%s online help: <%s>\n", "GNU coreutils",
			   "https://www.gnu.org/software/coreutils/");
	  lVar3 = func_0x00401aa0 (5, 0);
	  if (lVar3 != 0)
	    {
	      iVar1 = func_0x004019c0 (lVar3, &DAT_0040ef5c, 3);
	      if (iVar1 != 0)
		{
		  pcVar6 = "realpath";
		  goto LAB_00402531;
		}
	    }
	  func_0x00401770 ("Full documentation at: <%s%s>\n",
			   "https://www.gnu.org/software/coreutils/",
			   "realpath");
	LAB_0040255a:
	  ;
	  pcVar6 = "realpath";
	  uVar4 = 0x40eef4;
	}
      else
	{
	  func_0x00401770 ("\n%s online help: <%s>\n", "GNU coreutils",
			   "https://www.gnu.org/software/coreutils/");
	  lVar3 = func_0x00401aa0 (5, 0);
	  if (lVar3 != 0)
	    {
	      iVar1 = func_0x004019c0 (lVar3, &DAT_0040ef5c, 3);
	      if (iVar1 != 0)
		{
		LAB_00402531:
		  ;
		  func_0x00401770
		    ("Report %s translation bugs to <https://translationproject.org/team/>\n",
		     "realpath");
		}
	    }
	  func_0x00401770 ("Full documentation at: <%s%s>\n",
			   "https://www.gnu.org/software/coreutils/",
			   "realpath");
	  uVar4 = 0x41014f;
	  if (pcVar6 == "realpath")
	    goto LAB_0040255a;
	}
      func_0x00401770
	("or available locally via: info \'(coreutils) %s%s\'\n", pcVar6,
	 uVar4);
    }
  while (true);
}

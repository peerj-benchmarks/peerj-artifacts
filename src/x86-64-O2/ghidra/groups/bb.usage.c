
#include "groups.h"

long null_ARRAY_0060f66_0_8_;
long null_ARRAY_0060f66_8_8_;
long null_ARRAY_0060f84_0_8_;
long null_ARRAY_0060f84_16_8_;
long null_ARRAY_0060f84_24_8_;
long null_ARRAY_0060f84_32_8_;
long null_ARRAY_0060f84_40_8_;
long null_ARRAY_0060f84_48_8_;
long null_ARRAY_0060f84_8_8_;
long null_ARRAY_0060f88_0_4_;
long null_ARRAY_0060f88_16_8_;
long null_ARRAY_0060f88_4_4_;
long null_ARRAY_0060f88_8_4_;
long local_2f_1_1_;
long local_5b_1_1_;
long local_9d_0_4_;
long local_a0_0_8_;
long local_a0_4_6_;
long local_a0_8_2_;
long local_a1_4_4_;
long local_a2_4_4_;
long local_a3_0_4_;
long local_a3_4_4_;
long local_a4_0_2_;
long local_a4_0_4_;
long local_a4_0_8_;
long local_a4_1_9_;
long local_a4_4_6_;
long local_a4_8_2_;
long local_a5_0_4_;
long local_a5_4_4_;
long DAT_00000010;
long DAT_0040cc40;
long DAT_0040cccb;
long DAT_0040d0b8;
long DAT_0040d0bc;
long DAT_0040d0c0;
long DAT_0040d0c3;
long DAT_0040d0c5;
long DAT_0040d0c9;
long DAT_0040d0cd;
long DAT_0040d66b;
long DAT_0040da15;
long DAT_0040db19;
long DAT_0040db1f;
long DAT_0040db31;
long DAT_0040db32;
long DAT_0040db50;
long DAT_0040db54;
long DAT_0040dbd6;
long DAT_0060f210;
long DAT_0060f220;
long DAT_0060f230;
long DAT_0060f608;
long DAT_0060f670;
long DAT_0060f674;
long DAT_0060f678;
long DAT_0060f67c;
long DAT_0060f680;
long DAT_0060f690;
long DAT_0060f6a0;
long DAT_0060f6a8;
long DAT_0060f6c0;
long DAT_0060f6c8;
long DAT_0060f728;
long DAT_0060f730;
long DAT_0060f738;
long DAT_0060f8b8;
long DAT_0060f8c0;
long DAT_0060f8c8;
long DAT_0060f8d8;
long _DYNAMIC;
long fde_0040e570;
long null_ARRAY_0040cf80;
long null_ARRAY_0040de60;
long null_ARRAY_0040e090;
long null_ARRAY_0060f620;
long null_ARRAY_0060f660;
long null_ARRAY_0060f6e0;
long null_ARRAY_0060f710;
long null_ARRAY_0060f740;
long null_ARRAY_0060f840;
long null_ARRAY_0060f880;
long PTR_DAT_0060f600;
long PTR_null_ARRAY_0060f658;
long register0x00000020;
long stack0x00000008;
void
FUN_00401e80 (uint uParm1)
{
  int iVar1;
  long lVar2;
  undefined8 uVar3;
  undefined **ppuVar4;
  char *pcVar5;
  undefined *puVar6;
  undefined *local_88;
  char *local_80;
  undefined8 local_78;
  char *local_70;
  char *local_68;
  char *local_60;
  char *local_58;
  char *local_50;
  char *local_48;
  char *local_40;
  char *local_38;
  char *local_30;
  undefined8 local_28;
  undefined8 local_20;

  if (uParm1 == 0)
    goto LAB_00401ead;
  func_0x004017c0 (DAT_0060f6a0, "Try \'%s --help\' for more information.\n",
		   DAT_0060f738);
  do
    {
      func_0x00401980 ((ulong) uParm1);
    LAB_00401ead:
      ;
      func_0x00401620 ("Usage: %s [OPTION]... [USERNAME]...\n", DAT_0060f738);
      uVar3 = DAT_0060f680;
      func_0x004017d0
	("Print group memberships for each USERNAME or, if no USERNAME is specified, for\nthe current process (which may differ if the groups database has changed).\n",
	 DAT_0060f680);
      func_0x004017d0 ("      --help     display this help and exit\n",
		       uVar3);
      func_0x004017d0
	("      --version  output version information and exit\n", uVar3);
      local_88 = &DAT_0040cc40;
      local_80 = "test invocation";
      puVar6 = &DAT_0040cc40;
      local_78 = 0x40ccaa;
      local_70 = "Multi-call invocation";
      local_68 = "sha224sum";
      local_60 = "sha2 utilities";
      local_58 = "sha256sum";
      local_50 = "sha2 utilities";
      local_48 = "sha384sum";
      local_40 = "sha2 utilities";
      local_38 = "sha512sum";
      local_30 = "sha2 utilities";
      local_28 = 0;
      local_20 = 0;
      ppuVar4 = &local_88;
      do
	{
	  iVar1 = func_0x004018b0 ("groups", puVar6);
	  if (iVar1 == 0)
	    break;
	  ppuVar4 = ppuVar4 + 2;
	  puVar6 = *ppuVar4;
	}
      while (puVar6 != (undefined *) 0x0);
      pcVar5 = ppuVar4[1];
      if (pcVar5 == (char *) 0x0)
	{
	  func_0x00401620 ("\n%s online help: <%s>\n", "GNU coreutils",
			   "https://www.gnu.org/software/coreutils/");
	  lVar2 = func_0x00401930 (5, 0);
	  if (lVar2 != 0)
	    {
	      iVar1 = func_0x00401820 (lVar2, &DAT_0040cccb, 3);
	      if (iVar1 != 0)
		{
		  pcVar5 = "groups";
		  goto LAB_00402041;
		}
	    }
	  func_0x00401620 ("Full documentation at: <%s%s>\n",
			   "https://www.gnu.org/software/coreutils/",
			   "groups");
	LAB_0040206a:
	  ;
	  pcVar5 = "groups";
	  uVar3 = 0x40cc63;
	}
      else
	{
	  func_0x00401620 ("\n%s online help: <%s>\n", "GNU coreutils",
			   "https://www.gnu.org/software/coreutils/");
	  lVar2 = func_0x00401930 (5, 0);
	  if (lVar2 != 0)
	    {
	      iVar1 = func_0x00401820 (lVar2, &DAT_0040cccb, 3);
	      if (iVar1 != 0)
		{
		LAB_00402041:
		  ;
		  func_0x00401620
		    ("Report %s translation bugs to <https://translationproject.org/team/>\n",
		     "groups");
		}
	    }
	  func_0x00401620 ("Full documentation at: <%s%s>\n",
			   "https://www.gnu.org/software/coreutils/",
			   "groups");
	  uVar3 = 0x40db4f;
	  if (pcVar5 == "groups")
	    goto LAB_0040206a;
	}
      func_0x00401620
	("or available locally via: info \'(coreutils) %s%s\'\n", pcVar5,
	 uVar3);
    }
  while (true);
}

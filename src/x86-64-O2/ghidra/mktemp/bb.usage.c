
#include "mktemp.h"

long null_ARRAY_00611a6_0_8_;
long null_ARRAY_00611a6_8_8_;
long null_ARRAY_00611c8_0_8_;
long null_ARRAY_00611c8_16_8_;
long null_ARRAY_00611c8_24_8_;
long null_ARRAY_00611c8_32_8_;
long null_ARRAY_00611c8_40_8_;
long null_ARRAY_00611c8_48_8_;
long null_ARRAY_00611c8_8_8_;
long null_ARRAY_00611cc_0_4_;
long null_ARRAY_00611cc_16_8_;
long null_ARRAY_00611cc_4_4_;
long null_ARRAY_00611cc_8_4_;
long local_2f_1_1_;
long local_5b_1_1_;
long local_9d_0_4_;
long local_a0_0_8_;
long local_a0_4_6_;
long local_a0_8_2_;
long local_a1_4_4_;
long local_a2_4_4_;
long local_a3_0_4_;
long local_a3_4_4_;
long local_a4_0_2_;
long local_a4_0_4_;
long local_a4_0_8_;
long local_a4_1_9_;
long local_a4_4_6_;
long local_a4_8_2_;
long local_a5_0_4_;
long local_a5_4_4_;
long DAT_00000010;
long DAT_0040e340;
long DAT_0040e3cb;
long DAT_0040e468;
long DAT_0040ed90;
long DAT_0040ed94;
long DAT_0040ed98;
long DAT_0040ed9b;
long DAT_0040ed9d;
long DAT_0040eda1;
long DAT_0040eda5;
long DAT_0040f3d2;
long DAT_0040f775;
long DAT_0040f879;
long DAT_0040f87f;
long DAT_0040f891;
long DAT_0040f892;
long DAT_0040f8b0;
long DAT_0040f8b4;
long DAT_0040f94f;
long DAT_0040f965;
long DAT_006115b8;
long DAT_006115c8;
long DAT_006115d8;
long DAT_00611a08;
long DAT_00611a70;
long DAT_00611a74;
long DAT_00611a78;
long DAT_00611a7c;
long DAT_00611ac0;
long DAT_00611ad0;
long DAT_00611ae0;
long DAT_00611ae8;
long DAT_00611b00;
long DAT_00611b08;
long DAT_00611b50;
long DAT_00611b58;
long DAT_00611b60;
long DAT_00611b68;
long DAT_00611cf8;
long DAT_00611d38;
long DAT_00611d40;
long DAT_00611d48;
long DAT_00611d58;
long _DYNAMIC;
long fde_004103e8;
long null_ARRAY_0040ec40;
long null_ARRAY_0040fc00;
long null_ARRAY_0040fe30;
long null_ARRAY_00611a20;
long null_ARRAY_00611a60;
long null_ARRAY_00611b20;
long null_ARRAY_00611b80;
long null_ARRAY_00611c80;
long null_ARRAY_00611cc0;
long PTR_DAT_00611a00;
long PTR_null_ARRAY_00611a58;
long PTR_null_ARRAY_00611a80;
long register0x00000020;
void
FUN_00402480 (uint uParm1)
{
  int iVar1;
  long lVar2;
  undefined8 uVar3;
  undefined **ppuVar4;
  char *pcVar5;
  undefined *puVar6;
  undefined *local_88;
  char *local_80;
  undefined8 local_78;
  char *local_70;
  char *local_68;
  char *local_60;
  char *local_58;
  char *local_50;
  char *local_48;
  char *local_40;
  char *local_38;
  char *local_30;
  undefined8 local_28;
  undefined8 local_20;

  if (uParm1 == 0)
    goto LAB_004024ad;
  func_0x00401a20 (DAT_00611ae0, "Try \'%s --help\' for more information.\n",
		   DAT_00611b68);
  do
    {
      func_0x00401c30 ((ulong) uParm1);
    LAB_004024ad:
      ;
      func_0x00401850 ("Usage: %s [OPTION]... [TEMPLATE]\n", DAT_00611b68);
      uVar3 = DAT_00611ac0;
      func_0x00401a30
	("Create a temporary file or directory, safely, and print its name.\nTEMPLATE must contain at least 3 consecutive \'X\'s in last component.\nIf TEMPLATE is not specified, use tmp.XXXXXXXXXX, and --tmpdir is implied.\n",
	 DAT_00611ac0);
      func_0x00401a30
	("Files are created u+rw, and directories u+rwx, minus umask restrictions.\n",
	 uVar3);
      func_0x00401a30 (0x40f8ae, uVar3);
      func_0x00401a30
	("  -d, --directory     create a directory, not a file\n  -u, --dry-run       do not create anything; merely print a name (unsafe)\n  -q, --quiet         suppress diagnostics about file/dir-creation failure\n",
	 uVar3);
      func_0x00401a30
	("      --suffix=SUFF   append SUFF to TEMPLATE; SUFF must not contain a slash.\n                        This option is implied if TEMPLATE does not end in X\n",
	 uVar3);
      func_0x00401a30
	("  -p DIR, --tmpdir[=DIR]  interpret TEMPLATE relative to DIR; if DIR is not\n                        specified, use $TMPDIR if set, else /tmp.  With\n                        this option, TEMPLATE must not be an absolute name;\n                        unlike with -t, TEMPLATE may contain slashes, but\n                        mktemp creates only the final component\n",
	 uVar3);
      func_0x00401a30
	("  -t                  interpret TEMPLATE as a single file name component,\n                        relative to a directory: $TMPDIR, if set; else the\n                        directory specified via -p; else /tmp [deprecated]\n",
	 uVar3);
      func_0x00401a30 ("      --help     display this help and exit\n",
		       uVar3);
      func_0x00401a30
	("      --version  output version information and exit\n", uVar3);
      local_88 = &DAT_0040e340;
      local_80 = "test invocation";
      puVar6 = &DAT_0040e340;
      local_78 = 0x40e3aa;
      local_70 = "Multi-call invocation";
      local_68 = "sha224sum";
      local_60 = "sha2 utilities";
      local_58 = "sha256sum";
      local_50 = "sha2 utilities";
      local_48 = "sha384sum";
      local_40 = "sha2 utilities";
      local_38 = "sha512sum";
      local_30 = "sha2 utilities";
      local_28 = 0;
      local_20 = 0;
      ppuVar4 = &local_88;
      do
	{
	  iVar1 = func_0x00401b80 ("mktemp", puVar6);
	  if (iVar1 == 0)
	    break;
	  ppuVar4 = ppuVar4 + 2;
	  puVar6 = *ppuVar4;
	}
      while (puVar6 != (undefined *) 0x0);
      pcVar5 = ppuVar4[1];
      if (pcVar5 == (char *) 0x0)
	{
	  func_0x00401850 ("\n%s online help: <%s>\n", "GNU coreutils",
			   "https://www.gnu.org/software/coreutils/");
	  lVar2 = func_0x00401be0 (5, 0);
	  if (lVar2 != 0)
	    {
	      iVar1 = func_0x00401aa0 (lVar2, &DAT_0040e3cb, 3);
	      if (iVar1 != 0)
		{
		  pcVar5 = "mktemp";
		  goto LAB_00402691;
		}
	    }
	  func_0x00401850 ("Full documentation at: <%s%s>\n",
			   "https://www.gnu.org/software/coreutils/",
			   "mktemp");
	LAB_004026ba:
	  ;
	  pcVar5 = "mktemp";
	  uVar3 = 0x40e363;
	}
      else
	{
	  func_0x00401850 ("\n%s online help: <%s>\n", "GNU coreutils",
			   "https://www.gnu.org/software/coreutils/");
	  lVar2 = func_0x00401be0 (5, 0);
	  if (lVar2 != 0)
	    {
	      iVar1 = func_0x00401aa0 (lVar2, &DAT_0040e3cb, 3);
	      if (iVar1 != 0)
		{
		LAB_00402691:
		  ;
		  func_0x00401850
		    ("Report %s translation bugs to <https://translationproject.org/team/>\n",
		     "mktemp");
		}
	    }
	  func_0x00401850 ("Full documentation at: <%s%s>\n",
			   "https://www.gnu.org/software/coreutils/",
			   "mktemp");
	  uVar3 = 0x40f8af;
	  if (pcVar5 == "mktemp")
	    goto LAB_004026ba;
	}
      func_0x00401850
	("or available locally via: info \'(coreutils) %s%s\'\n", pcVar5,
	 uVar3);
    }
  while (true);
}

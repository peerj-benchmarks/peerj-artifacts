
#include "cksum.h"

long null_ARRAY_0060f64_0_8_;
long null_ARRAY_0060f64_8_8_;
long null_ARRAY_0060f84_0_8_;
long null_ARRAY_0060f84_16_8_;
long null_ARRAY_0060f84_24_8_;
long null_ARRAY_0060f84_32_8_;
long null_ARRAY_0060f84_40_8_;
long null_ARRAY_0060f84_48_8_;
long null_ARRAY_0060f84_8_8_;
long null_ARRAY_0060f88_0_4_;
long null_ARRAY_0060f88_16_8_;
long null_ARRAY_0060f88_4_4_;
long null_ARRAY_0060f88_8_4_;
long local_2f_1_1_;
long local_4_4_4_;
long local_5b_1_1_;
long local_9d_0_4_;
long local_a0_0_8_;
long local_a0_4_6_;
long local_a0_8_2_;
long local_a1_4_4_;
long local_a2_4_4_;
long local_a3_0_4_;
long local_a3_4_4_;
long local_a4_0_2_;
long local_a4_0_4_;
long local_a4_0_8_;
long local_a4_1_9_;
long local_a4_4_6_;
long local_a4_8_2_;
long local_a5_0_4_;
long local_a5_4_4_;
long DAT_00000010;
long DAT_0040c9ef;
long DAT_0040c9f0;
long DAT_0040c9f2;
long DAT_0040c9f5;
long DAT_0040ca7f;
long DAT_0040d0ac;
long DAT_0040d160;
long DAT_0040d164;
long DAT_0040d168;
long DAT_0040d16b;
long DAT_0040d16d;
long DAT_0040d171;
long DAT_0040d175;
long DAT_0040d724;
long DAT_0040dab5;
long DAT_0040dbb9;
long DAT_0040dbbf;
long DAT_0040dbd1;
long DAT_0040dbd2;
long DAT_0040dbf0;
long DAT_0040dbf4;
long DAT_0040dc76;
long DAT_0060f208;
long DAT_0060f218;
long DAT_0060f228;
long DAT_0060f5e8;
long DAT_0060f650;
long DAT_0060f654;
long DAT_0060f658;
long DAT_0060f65c;
long DAT_0060f680;
long DAT_0060f688;
long DAT_0060f690;
long DAT_0060f6a0;
long DAT_0060f6a8;
long DAT_0060f6c0;
long DAT_0060f6c8;
long DAT_0060f710;
long DAT_0060f718;
long DAT_0060f720;
long DAT_0060f728;
long DAT_0060f8b8;
long DAT_0060f8c0;
long DAT_0060f8c8;
long DAT_0060f8d8;
long _DYNAMIC;
long fde_0040e600;
long null_ARRAY_0040cc80;
long null_ARRAY_0040d080;
long null_ARRAY_0040d0c0;
long null_ARRAY_0040df00;
long null_ARRAY_0040e130;
long null_ARRAY_0060f640;
long null_ARRAY_0060f6e0;
long null_ARRAY_0060f740;
long null_ARRAY_0060f840;
long null_ARRAY_0060f880;
long PTR_DAT_0060f5e0;
long PTR_null_ARRAY_0060f638;
long register0x00000020;
long stack0x00000008;
void
FUN_00401ef0 (uint uParm1)
{
  int iVar1;
  long lVar2;
  undefined8 uVar3;
  undefined **ppuVar4;
  char *pcVar5;
  undefined *puVar6;
  undefined *local_88;
  char *local_80;
  undefined8 local_78;
  char *local_70;
  char *local_68;
  char *local_60;
  char *local_58;
  char *local_50;
  char *local_48;
  char *local_40;
  char *local_38;
  char *local_30;
  undefined8 local_28;
  undefined8 local_20;

  if (uParm1 == 0)
    goto LAB_00401f1d;
  func_0x00401710 (DAT_0060f6a0, "Try \'%s --help\' for more information.\n",
		   DAT_0060f728);
  do
    {
      func_0x004018d0 ((ulong) uParm1);
    LAB_00401f1d:
      ;
      func_0x00401590 ("Usage: %s [FILE]...\n  or:  %s [OPTION]\n",
		       DAT_0060f728, DAT_0060f728);
      uVar3 = DAT_0060f680;
      func_0x00401720 ("Print CRC checksum and byte counts of each FILE.\n\n",
		       DAT_0060f680);
      func_0x00401720 ("      --help     display this help and exit\n",
		       uVar3);
      func_0x00401720
	("      --version  output version information and exit\n", uVar3);
      local_88 = &DAT_0040c9f5;
      local_80 = "test invocation";
      puVar6 = &DAT_0040c9f5;
      local_78 = 0x40ca5e;
      local_70 = "Multi-call invocation";
      local_68 = "sha224sum";
      local_60 = "sha2 utilities";
      local_58 = "sha256sum";
      local_50 = "sha2 utilities";
      local_48 = "sha384sum";
      local_40 = "sha2 utilities";
      local_38 = "sha512sum";
      local_30 = "sha2 utilities";
      local_28 = 0;
      local_20 = 0;
      ppuVar4 = &local_88;
      do
	{
	  iVar1 = func_0x00401820 ("cksum", puVar6);
	  if (iVar1 == 0)
	    break;
	  ppuVar4 = ppuVar4 + 2;
	  puVar6 = *ppuVar4;
	}
      while (puVar6 != (undefined *) 0x0);
      pcVar5 = ppuVar4[1];
      if (pcVar5 == (char *) 0x0)
	{
	  func_0x00401590 ("\n%s online help: <%s>\n", "GNU coreutils",
			   "https://www.gnu.org/software/coreutils/");
	  lVar2 = func_0x00401880 (5, 0);
	  if (lVar2 != 0)
	    {
	      iVar1 = func_0x00401780 (lVar2, &DAT_0040ca7f, 3);
	      if (iVar1 != 0)
		{
		  pcVar5 = "cksum";
		  goto LAB_004020b9;
		}
	    }
	  func_0x00401590 ("Full documentation at: <%s%s>\n",
			   "https://www.gnu.org/software/coreutils/",
			   "cksum");
	LAB_004020e2:
	  ;
	  pcVar5 = "cksum";
	  uVar3 = 0x40ca17;
	}
      else
	{
	  func_0x00401590 ("\n%s online help: <%s>\n", "GNU coreutils",
			   "https://www.gnu.org/software/coreutils/");
	  lVar2 = func_0x00401880 (5, 0);
	  if (lVar2 != 0)
	    {
	      iVar1 = func_0x00401780 (lVar2, &DAT_0040ca7f, 3);
	      if (iVar1 != 0)
		{
		LAB_004020b9:
		  ;
		  func_0x00401590
		    ("Report %s translation bugs to <https://translationproject.org/team/>\n",
		     "cksum");
		}
	    }
	  func_0x00401590 ("Full documentation at: <%s%s>\n",
			   "https://www.gnu.org/software/coreutils/",
			   "cksum");
	  uVar3 = 0x40dbef;
	  if (pcVar5 == "cksum")
	    goto LAB_004020e2;
	}
      func_0x00401590
	("or available locally via: info \'(coreutils) %s%s\'\n", pcVar5,
	 uVar3);
    }
  while (true);
}

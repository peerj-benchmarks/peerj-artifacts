
#include "runcon.h"

long null_ARRAY_0060f42_0_8_;
long null_ARRAY_0060f42_8_8_;
long null_ARRAY_0060f60_0_8_;
long null_ARRAY_0060f60_16_8_;
long null_ARRAY_0060f60_24_8_;
long null_ARRAY_0060f60_32_8_;
long null_ARRAY_0060f60_40_8_;
long null_ARRAY_0060f60_48_8_;
long null_ARRAY_0060f60_8_8_;
long null_ARRAY_0060f64_0_4_;
long null_ARRAY_0060f64_16_8_;
long null_ARRAY_0060f64_4_4_;
long null_ARRAY_0060f64_8_4_;
long local_2f_1_1_;
long local_5b_1_1_;
long local_9d_0_4_;
long local_a0_0_8_;
long local_a0_4_6_;
long local_a0_8_2_;
long local_a1_4_4_;
long local_a2_4_4_;
long local_a3_0_4_;
long local_a3_4_4_;
long local_a4_0_2_;
long local_a4_0_4_;
long local_a4_0_8_;
long local_a4_1_9_;
long local_a4_4_6_;
long local_a4_8_2_;
long local_a5_0_4_;
long local_a5_4_4_;
long DAT_00000010;
long DAT_0040c4c0;
long DAT_0040c54b;
long DAT_0040cb98;
long DAT_0040cb9c;
long DAT_0040cba0;
long DAT_0040cba3;
long DAT_0040cba5;
long DAT_0040cba9;
long DAT_0040cbad;
long DAT_0040d12b;
long DAT_0040d4d5;
long DAT_0040d5d9;
long DAT_0040d5df;
long DAT_0040d5f1;
long DAT_0040d5f2;
long DAT_0040d610;
long DAT_0040d614;
long DAT_0040d696;
long DAT_0060f000;
long DAT_0060f010;
long DAT_0060f020;
long DAT_0060f3c8;
long DAT_0060f430;
long DAT_0060f434;
long DAT_0060f438;
long DAT_0060f43c;
long DAT_0060f440;
long DAT_0060f450;
long DAT_0060f460;
long DAT_0060f468;
long DAT_0060f480;
long DAT_0060f488;
long DAT_0060f4d0;
long DAT_0060f4d8;
long DAT_0060f4e0;
long DAT_0060f678;
long DAT_0060f680;
long DAT_0060f688;
long DAT_0060f698;
long _DYNAMIC;
long fde_0040dff8;
long null_ARRAY_0040ca40;
long null_ARRAY_0040d920;
long null_ARRAY_0040db50;
long null_ARRAY_0060f420;
long null_ARRAY_0060f4a0;
long null_ARRAY_0060f500;
long null_ARRAY_0060f600;
long null_ARRAY_0060f640;
long PTR_DAT_0060f3c0;
long PTR_null_ARRAY_0060f418;
long register0x00000020;
void
FUN_00401bd0 (uint uParm1)
{
  int iVar1;
  long lVar2;
  undefined8 uVar3;
  undefined **ppuVar4;
  char *pcVar5;
  undefined *puVar6;
  undefined *local_88;
  char *local_80;
  undefined8 local_78;
  char *local_70;
  char *local_68;
  char *local_60;
  char *local_58;
  char *local_50;
  char *local_48;
  char *local_40;
  char *local_38;
  char *local_30;
  undefined8 local_28;
  undefined8 local_20;

  if (uParm1 == 0)
    goto LAB_00401bfd;
  func_0x00401590 (DAT_0060f460, "Try \'%s --help\' for more information.\n",
		   DAT_0060f4e0);
  do
    {
      func_0x00401710 ((ulong) uParm1);
    LAB_00401bfd:
      ;
      func_0x00401420
	("Usage: %s CONTEXT COMMAND [args]\n  or:  %s [ -c ] [-u USER] [-r ROLE] [-t TYPE] [-l RANGE] COMMAND [args]\n",
	 DAT_0060f4e0, DAT_0060f4e0);
      uVar3 = DAT_0060f440;
      func_0x004015a0
	("Run a program in a different SELinux security context.\nWith neither CONTEXT nor COMMAND, print the current security context.\n",
	 DAT_0060f440);
      func_0x004015a0
	("\nMandatory arguments to long options are mandatory for short options too.\n",
	 uVar3);
      func_0x004015a0
	("  CONTEXT            Complete security context\n  -c, --compute      compute process transition context before modifying\n  -t, --type=TYPE    type (for same role as parent)\n  -u, --user=USER    user identity\n  -r, --role=ROLE    role\n  -l, --range=RANGE  levelrange\n\n",
	 uVar3);
      func_0x004015a0 ("      --help     display this help and exit\n",
		       uVar3);
      func_0x004015a0
	("      --version  output version information and exit\n", uVar3);
      local_88 = &DAT_0040c4c0;
      local_80 = "test invocation";
      puVar6 = &DAT_0040c4c0;
      local_78 = 0x40c52a;
      local_70 = "Multi-call invocation";
      local_68 = "sha224sum";
      local_60 = "sha2 utilities";
      local_58 = "sha256sum";
      local_50 = "sha2 utilities";
      local_48 = "sha384sum";
      local_40 = "sha2 utilities";
      local_38 = "sha512sum";
      local_30 = "sha2 utilities";
      local_28 = 0;
      local_20 = 0;
      ppuVar4 = &local_88;
      do
	{
	  iVar1 = func_0x00401670 ("runcon", puVar6);
	  if (iVar1 == 0)
	    break;
	  ppuVar4 = ppuVar4 + 2;
	  puVar6 = *ppuVar4;
	}
      while (puVar6 != (undefined *) 0x0);
      pcVar5 = ppuVar4[1];
      if (pcVar5 == (char *) 0x0)
	{
	  func_0x00401420 ("\n%s online help: <%s>\n", "GNU coreutils",
			   "https://www.gnu.org/software/coreutils/");
	  lVar2 = func_0x004016d0 (5, 0);
	  if (lVar2 != 0)
	    {
	      iVar1 = func_0x004015f0 (lVar2, &DAT_0040c54b, 3);
	      if (iVar1 != 0)
		{
		  pcVar5 = "runcon";
		  goto LAB_00401db1;
		}
	    }
	  func_0x00401420 ("Full documentation at: <%s%s>\n",
			   "https://www.gnu.org/software/coreutils/",
			   "runcon");
	LAB_00401dda:
	  ;
	  pcVar5 = "runcon";
	  uVar3 = 0x40c4e3;
	}
      else
	{
	  func_0x00401420 ("\n%s online help: <%s>\n", "GNU coreutils",
			   "https://www.gnu.org/software/coreutils/");
	  lVar2 = func_0x004016d0 (5, 0);
	  if (lVar2 != 0)
	    {
	      iVar1 = func_0x004015f0 (lVar2, &DAT_0040c54b, 3);
	      if (iVar1 != 0)
		{
		LAB_00401db1:
		  ;
		  func_0x00401420
		    ("Report %s translation bugs to <https://translationproject.org/team/>\n",
		     "runcon");
		}
	    }
	  func_0x00401420 ("Full documentation at: <%s%s>\n",
			   "https://www.gnu.org/software/coreutils/",
			   "runcon");
	  uVar3 = 0x40d60f;
	  if (pcVar5 == "runcon")
	    goto LAB_00401dda;
	}
      func_0x00401420
	("or available locally via: info \'(coreutils) %s%s\'\n", pcVar5,
	 uVar3);
    }
  while (true);
}

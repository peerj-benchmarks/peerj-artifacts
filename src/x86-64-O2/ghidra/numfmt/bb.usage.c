
#include "numfmt.h"

long null_ARRAY_00616c4_0_8_;
long null_ARRAY_00616c4_8_8_;
long null_ARRAY_00616ec_0_8_;
long null_ARRAY_00616ec_16_8_;
long null_ARRAY_00616ec_24_8_;
long null_ARRAY_00616ec_32_8_;
long null_ARRAY_00616ec_40_8_;
long null_ARRAY_00616ec_48_8_;
long null_ARRAY_00616ec_8_8_;
long null_ARRAY_00616f0_0_4_;
long null_ARRAY_00616f0_16_8_;
long null_ARRAY_00616f0_4_4_;
long null_ARRAY_00616f0_8_4_;
long local_14_4_6_;
long local_14_8_2_;
long local_15_4_6_;
long local_15_8_2_;
long local_2f_1_1_;
long local_4_4_4_;
long local_5b_1_1_;
long local_9d_0_4_;
long local_a0_0_8_;
long local_a0_4_6_;
long local_a0_8_2_;
long local_a1_4_4_;
long local_a2_4_4_;
long local_a3_0_4_;
long local_a3_4_4_;
long local_a4_0_2_;
long local_a4_0_4_;
long local_a4_0_8_;
long local_a4_1_9_;
long local_a4_4_6_;
long local_a4_8_2_;
long local_a5_0_4_;
long local_a5_4_4_;
long DAT_00000010;
long DAT_00410dc5;
long DAT_00410ddc;
long DAT_00410e14;
long DAT_00410eaf;
long DAT_00410ebe;
long DAT_00410ec2;
long DAT_00410ec5;
long DAT_00410ec7;
long DAT_00410ed2;
long DAT_00411065;
long DAT_0041302a;
long DAT_0041302c;
long DAT_00413088;
long DAT_0041308c;
long DAT_00413090;
long DAT_00413093;
long DAT_00413095;
long DAT_00413099;
long DAT_0041309d;
long DAT_0041362b;
long DAT_00413d80;
long DAT_00413d83;
long DAT_00413e89;
long DAT_00413e8f;
long DAT_00413ea1;
long DAT_00413ea2;
long DAT_00413ec0;
long DAT_00413eca;
long DAT_004142c0;
long DAT_0041434d;
long DAT_00616760;
long DAT_00616770;
long DAT_00616780;
long DAT_00616ba0;
long DAT_00616ba4;
long DAT_00616ba8;
long DAT_00616bac;
long DAT_00616bb0;
long DAT_00616bb8;
long DAT_00616bc0;
long DAT_00616bc8;
long DAT_00616be0;
long DAT_00616c50;
long DAT_00616c54;
long DAT_00616c58;
long DAT_00616c5c;
long DAT_00616c80;
long DAT_00616c88;
long DAT_00616c90;
long DAT_00616ca0;
long DAT_00616ca8;
long DAT_00616cc0;
long DAT_00616cc8;
long DAT_00616d10;
long DAT_00616d14;
long DAT_00616d18;
long DAT_00616d20;
long DAT_00616d28;
long DAT_00616d30;
long DAT_00616d38;
long DAT_00616d40;
long DAT_00616d48;
long DAT_00616d50;
long DAT_00616d58;
long DAT_00616d60;
long DAT_00616d68;
long DAT_00616d70;
long DAT_00616d78;
long DAT_00616d80;
long DAT_00616d84;
long DAT_00616d88;
long DAT_00616d90;
long DAT_00616d98;
long DAT_00616da0;
long DAT_00616da8;
long DAT_00616db0;
long DAT_00616f38;
long DAT_00616f40;
long DAT_00616f48;
long DAT_00616f50;
long DAT_00616f58;
long DAT_00616f68;
long _DYNAMIC;
long fde_00415468;
long FLOAT_UNKNOWN;
long null_ARRAY_00412940;
long null_ARRAY_004129c0;
long null_ARRAY_00412a40;
long null_ARRAY_00412ca0;
long null_ARRAY_00412cc0;
long null_ARRAY_00412cf0;
long null_ARRAY_00412d20;
long null_ARRAY_00412d50;
long null_ARRAY_00412d60;
long null_ARRAY_00412d90;
long null_ARRAY_00412dc0;
long null_ARRAY_00414160;
long null_ARRAY_00414380;
long null_ARRAY_00414480;
long null_ARRAY_00414ed0;
long null_ARRAY_00616c00;
long null_ARRAY_00616c40;
long null_ARRAY_00616ce0;
long null_ARRAY_00616dc0;
long null_ARRAY_00616ec0;
long null_ARRAY_00616f00;
long PTR_DAT_00616bd0;
long PTR_FUN_00616bd8;
long PTR_LAB_004128b8;
long PTR_LAB_004128e0;
long PTR_null_ARRAY_00616c38;
long register0x00000020;
void
FUN_004041b0 (uint uParm1)
{
  int iVar1;
  long lVar2;
  undefined8 uVar3;
  undefined **ppuVar4;
  char *pcVar5;
  undefined *puVar6;
  undefined *local_88;
  char *local_80;
  undefined8 local_78;
  char *local_70;
  char *local_68;
  char *local_60;
  char *local_58;
  char *local_50;
  char *local_48;
  char *local_40;
  char *local_38;
  char *local_30;
  undefined8 local_28;
  undefined8 local_20;

  if (uParm1 == 0)
    goto LAB_004041dd;
  func_0x00401a20 (DAT_00616ca0, "Try \'%s --help\' for more information.\n",
		   DAT_00616db0);
  do
    {
      func_0x00401be0 ((ulong) uParm1);
    LAB_004041dd:
      ;
      func_0x00401820 ("Usage: %s [OPTION]... [NUMBER]...\n", DAT_00616db0);
      uVar3 = DAT_00616c80;
      func_0x00401a30
	("Reformat NUMBER(s), or the numbers from standard input if none are specified.\n",
	 DAT_00616c80);
      func_0x00401a30
	("\nMandatory arguments to long options are mandatory for short options too.\n",
	 uVar3);
      func_0x00401a30
	("      --debug          print warnings about invalid input\n",
	 uVar3);
      func_0x00401a30
	("  -d, --delimiter=X    use X instead of whitespace for field delimiter\n",
	 uVar3);
      func_0x00401a30
	("      --field=FIELDS   replace the numbers in these input fields (default=1)\n                         see FIELDS below\n",
	 uVar3);
      func_0x00401a30
	("      --format=FORMAT  use printf style floating-point FORMAT;\n                         see FORMAT below for details\n",
	 uVar3);
      func_0x00401a30
	("      --from=UNIT      auto-scale input numbers to UNITs; default is \'none\';\n                         see UNIT below\n",
	 uVar3);
      func_0x00401a30
	("      --from-unit=N    specify the input unit size (instead of the default 1)\n",
	 uVar3);
      func_0x00401a30
	("      --grouping       use locale-defined grouping of digits, e.g. 1,000,000\n                         (which means it has no effect in the C/POSIX locale)\n",
	 uVar3);
      func_0x00401a30
	("      --header[=N]     print (without converting) the first N header lines;\n                         N defaults to 1 if not specified\n",
	 uVar3);
      func_0x00401a30
	("      --invalid=MODE   failure mode for invalid numbers: MODE can be:\n                         abort (default), fail, warn, ignore\n",
	 uVar3);
      func_0x00401a30
	("      --padding=N      pad the output to N characters; positive N will\n                         right-align; negative N will left-align;\n                         padding is ignored if the output is wider than N;\n                         the default is to automatically pad if a whitespace\n                         is found\n",
	 uVar3);
      func_0x00401a30
	("      --round=METHOD   use METHOD for rounding when scaling; METHOD can be:\n                         up, down, from-zero (default), towards-zero, nearest\n",
	 uVar3);
      func_0x00401a30
	("      --suffix=SUFFIX  add SUFFIX to output numbers, and accept optional\n                         SUFFIX in input numbers\n",
	 uVar3);
      func_0x00401a30
	("      --to=UNIT        auto-scale output numbers to UNITs; see UNIT below\n",
	 uVar3);
      func_0x00401a30
	("      --to-unit=N      the output unit size (instead of the default 1)\n",
	 uVar3);
      func_0x00401a30
	("  -z, --zero-terminated    line delimiter is NUL, not newline\n",
	 uVar3);
      func_0x00401a30 ("      --help     display this help and exit\n",
		       uVar3);
      func_0x00401a30
	("      --version  output version information and exit\n", uVar3);
      func_0x00401a30 ("\nUNIT options:\n", uVar3);
      func_0x00401a30
	("  none       no auto-scaling is done; suffixes will trigger an error\n",
	 uVar3);
      func_0x00401a30
	("  auto       accept optional single/two letter suffix:\n               1K = 1000,\n               1Ki = 1024,\n               1M = 1000000,\n               1Mi = 1048576,\n",
	 uVar3);
      func_0x00401a30
	("  si         accept optional single letter suffix:\n               1K = 1000,\n               1M = 1000000,\n               ...\n",
	 uVar3);
      func_0x00401a30
	("  iec        accept optional single letter suffix:\n               1K = 1024,\n               1M = 1048576,\n               ...\n",
	 uVar3);
      func_0x00401a30
	("  iec-i      accept optional two-letter suffix:\n               1Ki = 1024,\n               1Mi = 1048576,\n               ...\n",
	 uVar3);
      func_0x00401a30
	("\nFIELDS supports cut(1) style field ranges:\n  N    N\'th field, counted from 1\n  N-   from N\'th field, to end of line\n  N-M  from N\'th to M\'th field (inclusive)\n  -M   from first to M\'th field (inclusive)\n  -    all fields\nMultiple fields/ranges can be separated with commas\n",
	 uVar3);
      func_0x00401a30
	("\nFORMAT must be suitable for printing one floating-point argument \'%f\'.\nOptional quote (%\'f) will enable --grouping (if supported by current locale).\nOptional width value (%10f) will pad output. Optional zero (%010f) width\nwill zero pad the number. Optional negative values (%-10f) will left align.\nOptional precision (%.1f) will override the input determined precision.\n",
	 uVar3);
      func_0x00401820
	("\nExit status is 0 if all input numbers were successfully converted.\nBy default, %s will stop at the first conversion error with exit status 2.\nWith --invalid=\'fail\' a warning is printed for each conversion error\nand the exit status is 2.  With --invalid=\'warn\' each conversion error is\ndiagnosed, but the exit status is 0.  With --invalid=\'ignore\' conversion\nerrors are not diagnosed and the exit status is 0.\n",
	 DAT_00616db0);
      func_0x00401820
	("\nExamples:\n  $ %s --to=si 1000\n            -> \"1.0K\"\n  $ %s --to=iec 2048\n           -> \"2.0K\"\n  $ %s --to=iec-i 4096\n           -> \"4.0Ki\"\n  $ echo 1K | %s --from=si\n           -> \"1000\"\n  $ echo 1K | %s --from=iec\n           -> \"1024\"\n  $ df -B1 | %s --header --field 2-4 --to=si\n  $ ls -l  | %s --header --field 5 --to=iec\n  $ ls -lh | %s --header --field 5 --from=iec --padding=10\n  $ ls -lh | %s --header --field 5 --from=iec --format %%10f\n",
	 DAT_00616db0, DAT_00616db0, DAT_00616db0, DAT_00616db0, DAT_00616db0,
	 DAT_00616db0, DAT_00616db0, DAT_00616db0, DAT_00616db0);
      local_88 = &DAT_00410e14;
      local_80 = "test invocation";
      puVar6 = &DAT_00410e14;
      local_78 = 0x410e8e;
      local_70 = "Multi-call invocation";
      local_68 = "sha224sum";
      local_60 = "sha2 utilities";
      local_58 = "sha256sum";
      local_50 = "sha2 utilities";
      local_48 = "sha384sum";
      local_40 = "sha2 utilities";
      local_38 = "sha512sum";
      local_30 = "sha2 utilities";
      local_28 = 0;
      local_20 = 0;
      ppuVar4 = &local_88;
      do
	{
	  iVar1 = func_0x00401b30 ("numfmt", puVar6);
	  if (iVar1 == 0)
	    break;
	  ppuVar4 = ppuVar4 + 2;
	  puVar6 = *ppuVar4;
	}
      while (puVar6 != (undefined *) 0x0);
      pcVar5 = ppuVar4[1];
      if (pcVar5 == (char *) 0x0)
	{
	  func_0x00401820 ("\n%s online help: <%s>\n", "GNU coreutils",
			   "https://www.gnu.org/software/coreutils/");
	  lVar2 = func_0x00401b90 (5, 0);
	  if (lVar2 != 0)
	    {
	      iVar1 = func_0x00401a80 (lVar2, &DAT_00410eaf, 3);
	      if (iVar1 != 0)
		{
		  pcVar5 = "numfmt";
		  goto LAB_004044e9;
		}
	    }
	  func_0x00401820 ("Full documentation at: <%s%s>\n",
			   "https://www.gnu.org/software/coreutils/",
			   "numfmt");
	LAB_00404512:
	  ;
	  pcVar5 = "numfmt";
	  uVar3 = 0x410e47;
	}
      else
	{
	  func_0x00401820 ("\n%s online help: <%s>\n", "GNU coreutils",
			   "https://www.gnu.org/software/coreutils/");
	  lVar2 = func_0x00401b90 (5, 0);
	  if (lVar2 != 0)
	    {
	      iVar1 = func_0x00401a80 (lVar2, &DAT_00410eaf, 3);
	      if (iVar1 != 0)
		{
		LAB_004044e9:
		  ;
		  func_0x00401820
		    ("Report %s translation bugs to <https://translationproject.org/team/>\n",
		     "numfmt");
		}
	    }
	  func_0x00401820 ("Full documentation at: <%s%s>\n",
			   "https://www.gnu.org/software/coreutils/",
			   "numfmt");
	  uVar3 = 0x413ebf;
	  if (pcVar5 == "numfmt")
	    goto LAB_00404512;
	}
      func_0x00401820
	("or available locally via: info \'(coreutils) %s%s\'\n", pcVar5,
	 uVar3);
    }
  while (true);
}


#include "pinky.h"

long null_ARRAY_0061050_0_8_;
long null_ARRAY_0061050_8_8_;
long null_ARRAY_0061064_0_4_;
long null_ARRAY_006107c_0_8_;
long null_ARRAY_006107c_16_8_;
long null_ARRAY_006107c_24_8_;
long null_ARRAY_006107c_32_8_;
long null_ARRAY_006107c_40_8_;
long null_ARRAY_006107c_48_8_;
long null_ARRAY_006107c_8_8_;
long null_ARRAY_0061080_0_4_;
long null_ARRAY_0061080_16_8_;
long null_ARRAY_0061080_4_4_;
long null_ARRAY_0061080_8_4_;
long local_2f_1_1_;
long local_4_4_4_;
long local_5b_1_1_;
long local_9d_0_4_;
long local_a0_0_8_;
long local_a0_4_6_;
long local_a0_8_2_;
long local_a1_4_4_;
long local_a2_4_4_;
long local_a3_0_4_;
long local_a3_4_4_;
long local_a4_0_2_;
long local_a4_0_4_;
long local_a4_0_8_;
long local_a4_1_9_;
long local_a4_4_6_;
long local_a4_8_2_;
long local_a5_0_4_;
long local_a5_4_4_;
long DAT_00000010;
long DAT_0040d6e2;
long DAT_0040d74b;
long DAT_0040d75f;
long DAT_0040d777;
long DAT_0040d810;
long DAT_0040d86c;
long DAT_0040d871;
long DAT_0040d87d;
long DAT_0040d888;
long DAT_0040d88d;
long DAT_0040ddf8;
long DAT_0040de40;
long DAT_0040de44;
long DAT_0040de48;
long DAT_0040de4b;
long DAT_0040de4d;
long DAT_0040de51;
long DAT_0040de55;
long DAT_0040e3eb;
long DAT_0040e795;
long DAT_0040e899;
long DAT_0040e89f;
long DAT_0040e8b1;
long DAT_0040e8b2;
long DAT_0040e8d0;
long DAT_0040e94e;
long DAT_00610090;
long DAT_006100a0;
long DAT_006100b0;
long DAT_006104a0;
long DAT_006104a1;
long DAT_006104a2;
long DAT_006104a3;
long DAT_006104a4;
long DAT_006104a5;
long DAT_006104a6;
long DAT_006104a7;
long DAT_006104b0;
long DAT_00610510;
long DAT_00610514;
long DAT_00610518;
long DAT_0061051c;
long DAT_00610540;
long DAT_00610550;
long DAT_00610560;
long DAT_00610568;
long DAT_00610580;
long DAT_00610588;
long DAT_00610628;
long DAT_00610630;
long DAT_00610638;
long DAT_00610670;
long DAT_00610678;
long DAT_00610680;
long DAT_00610688;
long DAT_00610878;
long DAT_00610880;
long DAT_00610888;
long DAT_00610898;
long _DYNAMIC;
long fde_0040f310;
long null_ARRAY_0040dd80;
long null_ARRAY_0040ebe0;
long null_ARRAY_0040ee10;
long null_ARRAY_00610500;
long null_ARRAY_006105a0;
long null_ARRAY_006105e0;
long null_ARRAY_00610610;
long null_ARRAY_00610640;
long null_ARRAY_006106c0;
long null_ARRAY_006107c0;
long null_ARRAY_00610800;
long null_ARRAY_00610840;
long PTR_DAT_006104a8;
long PTR_null_ARRAY_006104f8;
long PTR_null_ARRAY_00610520;
long register0x00000020;
void
FUN_00402930 (uint uParm1)
{
  int iVar1;
  long lVar2;
  undefined8 uVar3;
  undefined **ppuVar4;
  char *pcVar5;
  undefined *puVar6;
  undefined *local_88;
  char *local_80;
  undefined8 local_78;
  char *local_70;
  char *local_68;
  char *local_60;
  char *local_58;
  char *local_50;
  char *local_48;
  char *local_40;
  char *local_38;
  char *local_30;
  undefined8 local_28;
  undefined8 local_20;

  if (uParm1 == 0)
    goto LAB_0040295d;
  func_0x004018c0 (DAT_00610560, "Try \'%s --help\' for more information.\n",
		   DAT_00610688);
  do
    {
      func_0x00401ae0 ((ulong) uParm1);
    LAB_0040295d:
      ;
      func_0x00401740 ("Usage: %s [OPTION]... [USER]...\n", DAT_00610688);
      uVar3 = DAT_00610540;
      func_0x004018e0
	("\n  -l              produce long format output for the specified USERs\n  -b              omit the user\'s home directory and shell in long format\n  -h              omit the user\'s project file in long format\n  -p              omit the user\'s plan file in long format\n  -s              do short format output, this is the default\n",
	 DAT_00610540);
      func_0x004018e0
	("  -f              omit the line of column headings in short format\n  -w              omit the user\'s full name in short format\n  -i              omit the user\'s full name and remote host in short format\n  -q              omit the user\'s full name, remote host and idle time\n                  in short format\n",
	 uVar3);
      func_0x004018e0 ("      --help     display this help and exit\n",
		       uVar3);
      func_0x004018e0
	("      --version  output version information and exit\n", uVar3);
      func_0x00401740
	("\nA lightweight \'finger\' program;  print user information.\nThe utmp file will be %s.\n",
	 "/dev/null/utmp");
      local_88 = &DAT_0040d777;
      local_80 = "test invocation";
      puVar6 = &DAT_0040d777;
      local_78 = 0x40d7ef;
      local_70 = "Multi-call invocation";
      local_68 = "sha224sum";
      local_60 = "sha2 utilities";
      local_58 = "sha256sum";
      local_50 = "sha2 utilities";
      local_48 = "sha384sum";
      local_40 = "sha2 utilities";
      local_38 = "sha512sum";
      local_30 = "sha2 utilities";
      local_28 = 0;
      local_20 = 0;
      ppuVar4 = &local_88;
      do
	{
	  iVar1 = func_0x004019e0 ("pinky", puVar6);
	  if (iVar1 == 0)
	    break;
	  ppuVar4 = ppuVar4 + 2;
	  puVar6 = *ppuVar4;
	}
      while (puVar6 != (undefined *) 0x0);
      pcVar5 = ppuVar4[1];
      if (pcVar5 == (char *) 0x0)
	{
	  func_0x00401740 ("\n%s online help: <%s>\n", "GNU coreutils",
			   "https://www.gnu.org/software/coreutils/");
	  lVar2 = func_0x00401a50 (5, 0);
	  if (lVar2 != 0)
	    {
	      iVar1 = func_0x00401930 (lVar2, &DAT_0040d810, 3);
	      if (iVar1 != 0)
		{
		  pcVar5 = "pinky";
		  goto LAB_00402b11;
		}
	    }
	  func_0x00401740 ("Full documentation at: <%s%s>\n",
			   "https://www.gnu.org/software/coreutils/",
			   "pinky");
	LAB_00402b3a:
	  ;
	  pcVar5 = "pinky";
	  uVar3 = 0x40d7a8;
	}
      else
	{
	  func_0x00401740 ("\n%s online help: <%s>\n", "GNU coreutils",
			   "https://www.gnu.org/software/coreutils/");
	  lVar2 = func_0x00401a50 (5, 0);
	  if (lVar2 != 0)
	    {
	      iVar1 = func_0x00401930 (lVar2, &DAT_0040d810, 3);
	      if (iVar1 != 0)
		{
		LAB_00402b11:
		  ;
		  func_0x00401740
		    ("Report %s translation bugs to <https://translationproject.org/team/>\n",
		     "pinky");
		}
	    }
	  func_0x00401740 ("Full documentation at: <%s%s>\n",
			   "https://www.gnu.org/software/coreutils/",
			   "pinky");
	  uVar3 = 0x40e8cf;
	  if (pcVar5 == "pinky")
	    goto LAB_00402b3a;
	}
      func_0x00401740
	("or available locally via: info \'(coreutils) %s%s\'\n", pcVar5,
	 uVar3);
    }
  while (true);
}

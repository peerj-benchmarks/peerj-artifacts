
#include "chmod.h"

long null_ARRAY_006154a_0_8_;
long null_ARRAY_006154a_8_8_;
long null_ARRAY_006156c_0_8_;
long null_ARRAY_006156c_16_8_;
long null_ARRAY_006156c_24_8_;
long null_ARRAY_006156c_32_8_;
long null_ARRAY_006156c_40_8_;
long null_ARRAY_006156c_48_8_;
long null_ARRAY_006156c_8_8_;
long null_ARRAY_0061570_0_4_;
long null_ARRAY_0061570_16_8_;
long null_ARRAY_0061570_4_4_;
long null_ARRAY_0061570_8_4_;
long local_2f_1_1_;
long local_4_4_4_;
long local_5b_1_1_;
long local_9d_0_4_;
long local_a0_0_8_;
long local_a0_4_6_;
long local_a0_8_2_;
long local_a1_4_4_;
long local_a2_4_4_;
long local_a3_0_4_;
long local_a3_4_4_;
long local_a4_0_2_;
long local_a4_0_4_;
long local_a4_0_8_;
long local_a4_1_9_;
long local_a4_4_6_;
long local_a4_8_2_;
long local_a5_0_4_;
long local_a5_4_4_;
long DAT_00000010;
long DAT_00411240;
long DAT_004112ca;
long DAT_00411ef8;
long DAT_00411efe;
long DAT_00411f00;
long DAT_00411f04;
long DAT_00411f08;
long DAT_00411f0b;
long DAT_00411f0d;
long DAT_00411f11;
long DAT_00411f15;
long DAT_004124ab;
long DAT_0041287a;
long DAT_0041288b;
long DAT_0041288c;
long DAT_004129c1;
long DAT_004129c7;
long DAT_004129d9;
long DAT_004129da;
long DAT_004129f8;
long DAT_00412a32;
long DAT_00412ae6;
long DAT_00615000;
long DAT_00615010;
long DAT_00615020;
long DAT_00615440;
long DAT_00615450;
long DAT_006154b0;
long DAT_006154b4;
long DAT_006154b8;
long DAT_006154bc;
long DAT_006154c0;
long DAT_006154d0;
long DAT_006154e0;
long DAT_006154e8;
long DAT_00615500;
long DAT_00615508;
long DAT_00615560;
long DAT_00615568;
long DAT_00615569;
long DAT_0061556a;
long DAT_0061556c;
long DAT_00615570;
long DAT_00615578;
long DAT_00615580;
long DAT_00615588;
long DAT_00615738;
long DAT_00615740;
long DAT_00615744;
long DAT_00615748;
long DAT_00615750;
long DAT_00615760;
long fde_00413680;
long int7;
long null_ARRAY_00411c40;
long null_ARRAY_004128a0;
long null_ARRAY_00412a40;
long null_ARRAY_00412d80;
long null_ARRAY_00412fa0;
long null_ARRAY_00615460;
long null_ARRAY_006154a0;
long null_ARRAY_00615520;
long null_ARRAY_00615550;
long null_ARRAY_006155c0;
long null_ARRAY_006156c0;
long null_ARRAY_00615700;
long PTR_DAT_00615448;
long PTR_null_ARRAY_00615498;
long register0x00000020;
void
FUN_004029a0 (uint uParm1)
{
  int iVar1;
  long lVar2;
  undefined8 uVar3;
  undefined **ppuVar4;
  char *pcVar5;
  undefined *puVar6;
  undefined *local_88;
  char *local_80;
  undefined8 local_78;
  char *local_70;
  char *local_68;
  char *local_60;
  char *local_58;
  char *local_50;
  char *local_48;
  char *local_40;
  char *local_38;
  char *local_30;
  undefined8 local_28;
  undefined8 local_20;

  if (uParm1 == 0)
    goto LAB_004029cd;
  func_0x00401a00 (DAT_006154e0, "Try \'%s --help\' for more information.\n",
		   DAT_00615588);
  do
    {
      func_0x00401be0 ((ulong) uParm1);
    LAB_004029cd:
      ;
      func_0x00401810
	("Usage: %s [OPTION]... MODE[,MODE]... FILE...\n  or:  %s [OPTION]... OCTAL-MODE FILE...\n  or:  %s [OPTION]... --reference=RFILE FILE...\n",
	 DAT_00615588, DAT_00615588, DAT_00615588);
      uVar3 = DAT_006154c0;
      func_0x00401a10
	("Change the mode of each FILE to MODE.\nWith --reference, change the mode of each FILE to that of RFILE.\n\n",
	 DAT_006154c0);
      func_0x00401a10
	("  -c, --changes          like verbose but report only when a change is made\n  -f, --silent, --quiet  suppress most error messages\n  -v, --verbose          output a diagnostic for every file processed\n",
	 uVar3);
      func_0x00401a10
	("      --no-preserve-root  do not treat \'/\' specially (the default)\n      --preserve-root    fail to operate recursively on \'/\'\n",
	 uVar3);
      func_0x00401a10
	("      --reference=RFILE  use RFILE\'s mode instead of MODE values\n",
	 uVar3);
      func_0x00401a10
	("  -R, --recursive        change files and directories recursively\n",
	 uVar3);
      func_0x00401a10 ("      --help     display this help and exit\n",
		       uVar3);
      func_0x00401a10
	("      --version  output version information and exit\n", uVar3);
      func_0x00401a10
	("\nEach MODE is of the form \'[ugoa]*([-+=]([rwxXst]*|[ugo]))+|[-+=][0-7]+\'.\n",
	 uVar3);
      local_88 = &DAT_00411240;
      local_80 = "test invocation";
      puVar6 = &DAT_00411240;
      local_78 = 0x4112a9;
      local_70 = "Multi-call invocation";
      local_68 = "sha224sum";
      local_60 = "sha2 utilities";
      local_58 = "sha256sum";
      local_50 = "sha2 utilities";
      local_48 = "sha384sum";
      local_40 = "sha2 utilities";
      local_38 = "sha512sum";
      local_30 = "sha2 utilities";
      local_28 = 0;
      local_20 = 0;
      ppuVar4 = &local_88;
      do
	{
	  iVar1 = func_0x00401b20 ("chmod", puVar6);
	  if (iVar1 == 0)
	    break;
	  ppuVar4 = ppuVar4 + 2;
	  puVar6 = *ppuVar4;
	}
      while (puVar6 != (undefined *) 0x0);
      pcVar5 = ppuVar4[1];
      if (pcVar5 == (char *) 0x0)
	{
	  func_0x00401810 ("\n%s online help: <%s>\n", "GNU coreutils",
			   "https://www.gnu.org/software/coreutils/");
	  lVar2 = func_0x00401b90 (5, 0);
	  if (lVar2 != 0)
	    {
	      iVar1 = func_0x00401a90 (lVar2, &DAT_004112ca, 3);
	      if (iVar1 != 0)
		{
		  pcVar5 = "chmod";
		  goto LAB_00402ba9;
		}
	    }
	  func_0x00401810 ("Full documentation at: <%s%s>\n",
			   "https://www.gnu.org/software/coreutils/",
			   "chmod");
	LAB_00402bd2:
	  ;
	  pcVar5 = "chmod";
	  uVar3 = 0x411262;
	}
      else
	{
	  func_0x00401810 ("\n%s online help: <%s>\n", "GNU coreutils",
			   "https://www.gnu.org/software/coreutils/");
	  lVar2 = func_0x00401b90 (5, 0);
	  if (lVar2 != 0)
	    {
	      iVar1 = func_0x00401a90 (lVar2, &DAT_004112ca, 3);
	      if (iVar1 != 0)
		{
		LAB_00402ba9:
		  ;
		  func_0x00401810
		    ("Report %s translation bugs to <https://translationproject.org/team/>\n",
		     "chmod");
		}
	    }
	  func_0x00401810 ("Full documentation at: <%s%s>\n",
			   "https://www.gnu.org/software/coreutils/",
			   "chmod");
	  uVar3 = 0x4129f7;
	  if (pcVar5 == "chmod")
	    goto LAB_00402bd2;
	}
      func_0x00401810
	("or available locally via: info \'(coreutils) %s%s\'\n", pcVar5,
	 uVar3);
    }
  while (true);
}

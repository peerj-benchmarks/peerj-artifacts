
#include "echo.h"

long null_ARRAY_0060e52_0_8_;
long null_ARRAY_0060e52_8_8_;
long null_ARRAY_0060e70_0_8_;
long null_ARRAY_0060e70_16_8_;
long null_ARRAY_0060e70_24_8_;
long null_ARRAY_0060e70_32_8_;
long null_ARRAY_0060e70_40_8_;
long null_ARRAY_0060e70_48_8_;
long null_ARRAY_0060e70_8_8_;
long local_2f_1_1_;
long local_5b_1_1_;
long local_9d_0_4_;
long local_a0_0_8_;
long local_a0_4_6_;
long local_a0_8_2_;
long local_a1_4_4_;
long local_a2_4_4_;
long local_a3_0_4_;
long local_a3_4_4_;
long local_a4_0_2_;
long local_a4_0_4_;
long local_a4_0_8_;
long local_a4_1_9_;
long local_a4_4_6_;
long local_a4_8_2_;
long local_a5_0_4_;
long local_a5_4_4_;
long DAT_0040be38;
long DAT_0040be3a;
long DAT_0040bec1;
long DAT_0040bed5;
long DAT_0040c430;
long DAT_0040c434;
long DAT_0040c436;
long DAT_0040c43a;
long DAT_0040c43d;
long DAT_0040c43f;
long DAT_0040c443;
long DAT_0040c447;
long DAT_0040c9eb;
long DAT_0040cd95;
long DAT_0040cda6;
long DAT_0040ce26;
long DAT_0060e118;
long DAT_0060e128;
long DAT_0060e138;
long DAT_0060e4c8;
long DAT_0060e530;
long DAT_0060e540;
long DAT_0060e550;
long DAT_0060e560;
long DAT_0060e568;
long DAT_0060e580;
long DAT_0060e588;
long DAT_0060e5d0;
long DAT_0060e5d8;
long DAT_0060e5e0;
long DAT_0060e738;
long DAT_0060e740;
long DAT_0060e748;
long _DYNAMIC;
long fde_0040d778;
long null_ARRAY_0040d0c0;
long null_ARRAY_0040d2f0;
long null_ARRAY_0060e520;
long null_ARRAY_0060e5a0;
long null_ARRAY_0060e600;
long null_ARRAY_0060e700;
long PTR_DAT_0060e4c0;
long PTR_null_ARRAY_0060e518;
long register0x00000020;
void
FUN_00401e10 (uint uParm1)
{
  int iVar1;
  long lVar2;
  undefined8 uVar3;
  undefined **ppuVar4;
  undefined *puVar5;
  undefined *local_88;
  char *local_80;
  undefined8 local_78;
  char *local_70;
  char *local_68;
  char *local_60;
  char *local_58;
  char *local_50;
  char *local_48;
  char *local_40;
  char *local_38;
  char *local_30;
  undefined8 local_28;
  undefined8 local_20;

  if (uParm1 == 0)
    goto LAB_00401e3d;
  func_0x00401560 (DAT_0060e560, "Try \'%s --help\' for more information.\n",
		   DAT_0060e5e0);
  do
    {
      func_0x004016d0 ((ulong) uParm1);
    LAB_00401e3d:
      ;
      func_0x004013e0
	("Usage: %s [SHORT-OPTION]... [STRING]...\n  or:  %s LONG-OPTION\n",
	 DAT_0060e5e0, DAT_0060e5e0);
      uVar3 = DAT_0060e540;
      func_0x00401570
	("Echo the STRING(s) to standard output.\n\n  -n             do not output the trailing newline\n",
	 DAT_0060e540);
      func_0x00401570
	("  -e             enable interpretation of backslash escapes\n  -E             disable interpretation of backslash escapes (default)\n",
	 uVar3);
      func_0x00401570 ("      --help     display this help and exit\n",
		       uVar3);
      func_0x00401570
	("      --version  output version information and exit\n", uVar3);
      func_0x00401570
	("\nIf -e is in effect, the following sequences are recognized:\n\n",
	 uVar3);
      func_0x00401570
	("  \\\\      backslash\n  \\a      alert (BEL)\n  \\b      backspace\n  \\c      produce no further output\n  \\e      escape\n  \\f      form feed\n  \\n      new line\n  \\r      carriage return\n  \\t      horizontal tab\n  \\v      vertical tab\n",
	 uVar3);
      func_0x00401570
	("  \\0NNN   byte with octal value NNN (1 to 3 digits)\n  \\xHH    byte with hexadecimal value HH (1 to 2 digits)\n",
	 uVar3);
      func_0x004013e0
	("\nNOTE: your shell may have its own version of %s, which usually supersedes\nthe version described here.  Please refer to your shell\'s documentation\nfor details about the options it supports.\n",
	 &DAT_0040be3a);
      local_88 = &DAT_0040be38;
      local_80 = "test invocation";
      puVar5 = &DAT_0040be38;
      local_78 = 0x40bea0;
      local_70 = "Multi-call invocation";
      local_68 = "sha224sum";
      local_60 = "sha2 utilities";
      local_58 = "sha256sum";
      local_50 = "sha2 utilities";
      local_48 = "sha384sum";
      local_40 = "sha2 utilities";
      local_38 = "sha512sum";
      local_30 = "sha2 utilities";
      local_28 = 0;
      local_20 = 0;
      ppuVar4 = &local_88;
      do
	{
	  iVar1 = func_0x00401630 (&DAT_0040be3a, puVar5);
	  if (iVar1 == 0)
	    break;
	  ppuVar4 = ppuVar4 + 2;
	  puVar5 = *ppuVar4;
	}
      while (puVar5 != (undefined *) 0x0);
      puVar5 = ppuVar4[1];
      if (puVar5 == (undefined *) 0x0)
	{
	  func_0x004013e0 ("\n%s online help: <%s>\n", "GNU coreutils",
			   "https://www.gnu.org/software/coreutils/");
	  lVar2 = func_0x00401680 (5, 0);
	  if (lVar2 != 0)
	    {
	      iVar1 = func_0x004015c0 (lVar2, &DAT_0040bec1, 3);
	      if (iVar1 != 0)
		{
		  puVar5 = &DAT_0040be3a;
		  goto LAB_00402019;
		}
	    }
	  func_0x004013e0 ("Full documentation at: <%s%s>\n",
			   "https://www.gnu.org/software/coreutils/",
			   &DAT_0040be3a);
	LAB_00402042:
	  ;
	  puVar5 = &DAT_0040be3a;
	  uVar3 = 0x40be59;
	}
      else
	{
	  func_0x004013e0 ("\n%s online help: <%s>\n", "GNU coreutils",
			   "https://www.gnu.org/software/coreutils/");
	  lVar2 = func_0x00401680 (5, 0);
	  if (lVar2 != 0)
	    {
	      iVar1 = func_0x004015c0 (lVar2, &DAT_0040bec1, 3);
	      if (iVar1 != 0)
		{
		LAB_00402019:
		  ;
		  func_0x004013e0
		    ("Report %s translation bugs to <https://translationproject.org/team/>\n",
		     &DAT_0040be3a);
		}
	    }
	  func_0x004013e0 ("Full documentation at: <%s%s>\n",
			   "https://www.gnu.org/software/coreutils/",
			   &DAT_0040be3a);
	  uVar3 = 0x40ca31;
	  if (puVar5 == &DAT_0040be3a)
	    goto LAB_00402042;
	}
      func_0x004013e0
	("or available locally via: info \'(coreutils) %s%s\'\n", puVar5,
	 uVar3);
    }
  while (true);
}

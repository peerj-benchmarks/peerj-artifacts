
#include "logname.h"

long null_ARRAY_0060ebe_0_8_;
long null_ARRAY_0060ebe_8_8_;
long null_ARRAY_0060edc_0_8_;
long null_ARRAY_0060edc_16_8_;
long null_ARRAY_0060edc_24_8_;
long null_ARRAY_0060edc_32_8_;
long null_ARRAY_0060edc_40_8_;
long null_ARRAY_0060edc_48_8_;
long null_ARRAY_0060edc_8_8_;
long null_ARRAY_0060ee0_0_4_;
long null_ARRAY_0060ee0_16_8_;
long null_ARRAY_0060ee0_4_4_;
long null_ARRAY_0060ee0_8_4_;
long local_2f_1_1_;
long local_5b_1_1_;
long local_9d_0_4_;
long local_a0_0_8_;
long local_a0_4_6_;
long local_a0_8_2_;
long local_a1_4_4_;
long local_a2_4_4_;
long local_a3_0_4_;
long local_a3_4_4_;
long local_a4_0_2_;
long local_a4_0_4_;
long local_a4_0_8_;
long local_a4_1_9_;
long local_a4_4_6_;
long local_a4_8_2_;
long local_a5_0_4_;
long local_a5_4_4_;
long lVar8;
long var8;
long DAT_00000010;
long DAT_0040c480;
long DAT_0040c520;
long DAT_0040c718;
long DAT_0040c7e0;
long DAT_0040c7e4;
long DAT_0040c7e8;
long DAT_0040c7eb;
long DAT_0040c7ed;
long DAT_0040c7f1;
long DAT_0040c7f5;
long DAT_0040cdab;
long DAT_0040d155;
long DAT_0040d259;
long DAT_0040d25f;
long DAT_0040d271;
long DAT_0040d272;
long DAT_0040d290;
long DAT_0040d294;
long DAT_0040d316;
long DAT_0060e7c8;
long DAT_0060e7d8;
long DAT_0060e7e8;
long DAT_0060eb88;
long DAT_0060ebf0;
long DAT_0060ebf4;
long DAT_0060ebf8;
long DAT_0060ebfc;
long DAT_0060ec00;
long DAT_0060ec10;
long DAT_0060ec20;
long DAT_0060ec28;
long DAT_0060ec40;
long DAT_0060ec48;
long DAT_0060ec90;
long DAT_0060ec98;
long DAT_0060eca0;
long DAT_0060ee38;
long DAT_0060ee40;
long DAT_0060ee48;
long DAT_0060ee58;
long _DYNAMIC;
long fde_0040dc80;
long null_ARRAY_0040c6e0;
long null_ARRAY_0040c740;
long null_ARRAY_0040d5a0;
long null_ARRAY_0040d7d0;
long null_ARRAY_0060eba0;
long null_ARRAY_0060ebe0;
long null_ARRAY_0060ec60;
long null_ARRAY_0060ecc0;
long null_ARRAY_0060edc0;
long null_ARRAY_0060ee00;
long PTR_DAT_0060eb80;
long PTR_null_ARRAY_0060ebd8;
long register0x00000020;
void
FUN_00401b10 (uint uParm1)
{
  int iVar1;
  long lVar2;
  undefined8 uVar3;
  undefined **ppuVar4;
  char *pcVar5;
  undefined *puVar6;
  undefined *local_88;
  char *local_80;
  undefined8 local_78;
  char *local_70;
  char *local_68;
  char *local_60;
  char *local_58;
  char *local_50;
  char *local_48;
  char *local_40;
  char *local_38;
  char *local_30;
  undefined8 local_28;
  undefined8 local_20;

  if (uParm1 == 0)
    goto LAB_00401b3d;
  func_0x00401620 (DAT_0060ec20, "Try \'%s --help\' for more information.\n",
		   DAT_0060eca0);
  do
    {
      func_0x004017a0 ((ulong) uParm1);
    LAB_00401b3d:
      ;
      func_0x00401490 ("Usage: %s [OPTION]\n", DAT_0060eca0);
      uVar3 = DAT_0060ec00;
      func_0x00401630 ("Print the name of the current user.\n\n",
		       DAT_0060ec00);
      func_0x00401630 ("      --help     display this help and exit\n",
		       uVar3);
      func_0x00401630
	("      --version  output version information and exit\n", uVar3);
      local_88 = &DAT_0040c480;
      local_80 = "test invocation";
      puVar6 = &DAT_0040c480;
      local_78 = 0x40c4ff;
      local_70 = "Multi-call invocation";
      local_68 = "sha224sum";
      local_60 = "sha2 utilities";
      local_58 = "sha256sum";
      local_50 = "sha2 utilities";
      local_48 = "sha384sum";
      local_40 = "sha2 utilities";
      local_38 = "sha512sum";
      local_30 = "sha2 utilities";
      local_28 = 0;
      local_20 = 0;
      ppuVar4 = &local_88;
      do
	{
	  iVar1 = func_0x00401700 ("logname", puVar6);
	  if (iVar1 == 0)
	    break;
	  ppuVar4 = ppuVar4 + 2;
	  puVar6 = *ppuVar4;
	}
      while (puVar6 != (undefined *) 0x0);
      pcVar5 = ppuVar4[1];
      if (pcVar5 == (char *) 0x0)
	{
	  func_0x00401490 ("\n%s online help: <%s>\n", "GNU coreutils",
			   "https://www.gnu.org/software/coreutils/");
	  lVar2 = func_0x00401760 (5, 0);
	  if (lVar2 != 0)
	    {
	      iVar1 = func_0x00401680 (lVar2, &DAT_0040c520, 3);
	      if (iVar1 != 0)
		{
		  pcVar5 = "logname";
		  goto LAB_00401cd1;
		}
	    }
	  func_0x00401490 ("Full documentation at: <%s%s>\n",
			   "https://www.gnu.org/software/coreutils/",
			   "logname");
	LAB_00401cfa:
	  ;
	  pcVar5 = "logname";
	  uVar3 = 0x40c4b8;
	}
      else
	{
	  func_0x00401490 ("\n%s online help: <%s>\n", "GNU coreutils",
			   "https://www.gnu.org/software/coreutils/");
	  lVar2 = func_0x00401760 (5, 0);
	  if (lVar2 != 0)
	    {
	      iVar1 = func_0x00401680 (lVar2, &DAT_0040c520, 3);
	      if (iVar1 != 0)
		{
		LAB_00401cd1:
		  ;
		  func_0x00401490
		    ("Report %s translation bugs to <https://translationproject.org/team/>\n",
		     "logname");
		}
	    }
	  func_0x00401490 ("Full documentation at: <%s%s>\n",
			   "https://www.gnu.org/software/coreutils/",
			   "logname");
	  uVar3 = 0x40d28f;
	  if (pcVar5 == "logname")
	    goto LAB_00401cfa;
	}
      func_0x00401490
	("or available locally via: info \'(coreutils) %s%s\'\n", pcVar5,
	 uVar3);
    }
  while (true);
}

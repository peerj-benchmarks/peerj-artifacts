
#include "stat.h"

long null_ARRAY_0061bf2_0_8_;
long null_ARRAY_0061bf2_8_8_;
long null_ARRAY_0061c20_0_8_;
long null_ARRAY_0061c20_16_8_;
long null_ARRAY_0061c20_24_8_;
long null_ARRAY_0061c20_32_8_;
long null_ARRAY_0061c20_40_8_;
long null_ARRAY_0061c20_48_8_;
long null_ARRAY_0061c20_8_8_;
long null_ARRAY_0061c24_0_4_;
long null_ARRAY_0061c24_16_8_;
long null_ARRAY_0061c24_4_4_;
long null_ARRAY_0061c24_8_4_;
long local_2f_1_1_;
long local_4_4_4_;
long local_5b_1_1_;
long local_9d_0_4_;
long local_a0_0_8_;
long local_a0_4_6_;
long local_a0_8_2_;
long local_a1_4_4_;
long local_a2_4_4_;
long local_a3_0_4_;
long local_a3_4_4_;
long local_a4_0_2_;
long local_a4_0_4_;
long local_a4_0_8_;
long local_a4_1_9_;
long local_a4_4_6_;
long local_a4_8_2_;
long local_a5_0_4_;
long local_a5_4_4_;
long DAT_00000010;
long DAT_00415a40;
long DAT_00415a43;
long DAT_00415a47;
long DAT_00415a4a;
long DAT_00415a4e;
long DAT_00415ad5;
long DAT_00415c63;
long DAT_00415d62;
long DAT_00415d85;
long DAT_00415d9f;
long DAT_00415dbe;
long DAT_00415dc3;
long DAT_00415de0;
long DAT_00415de2;
long DAT_00415e69;
long DAT_00415e92;
long DAT_0041764b;
long DAT_0041764c;
long DAT_00417702;
long DAT_0041776f;
long DAT_00417f70;
long DAT_00417f76;
long DAT_00417f78;
long DAT_00417f7c;
long DAT_00417f80;
long DAT_00417f83;
long DAT_00417f85;
long DAT_00417f89;
long DAT_00417f8d;
long DAT_0041852b;
long DAT_004188f2;
long DAT_004188fc;
long DAT_00418949;
long DAT_00418a69;
long DAT_00418a6a;
long DAT_00418a88;
long DAT_00418aa1;
long DAT_00418acf;
long DAT_00418aeb;
long DAT_00418b10;
long DAT_00418bd4;
long DAT_00418bd6;
long DAT_00418bda;
long DAT_0061b9d8;
long DAT_0061b9e8;
long DAT_0061b9f8;
long DAT_0061bed0;
long DAT_0061bf30;
long DAT_0061bf34;
long DAT_0061bf38;
long DAT_0061bf3c;
long DAT_0061bf80;
long DAT_0061bf90;
long DAT_0061bfa0;
long DAT_0061bfa8;
long DAT_0061bfc0;
long DAT_0061bfc8;
long DAT_0061c060;
long DAT_0061c068;
long DAT_0061c070;
long DAT_0061c0a0;
long DAT_0061c0a8;
long DAT_0061c0b0;
long DAT_0061c0b1;
long DAT_0061c0b8;
long DAT_0061c0c0;
long DAT_0061c0c8;
long DAT_0061c278;
long DAT_0061c280;
long DAT_0061c288;
long DAT_0061c290;
long DAT_0061c2d8;
long DAT_0061cb08;
long DAT_0061cb10;
long DAT_0061cb20;
long fde_00419898;
long _iVar7;
long null_ARRAY_00416500;
long null_ARRAY_0041666a;
long null_ARRAY_00417730;
long null_ARRAY_00418460;
long null_ARRAY_004184c0;
long null_ARRAY_00418ba0;
long null_ARRAY_00418e80;
long null_ARRAY_004190a0;
long null_ARRAY_0061bee0;
long null_ARRAY_0061bf20;
long null_ARRAY_0061bfe0;
long null_ARRAY_0061c020;
long null_ARRAY_0061c071;
long null_ARRAY_0061c080;
long null_ARRAY_0061c100;
long null_ARRAY_0061c200;
long null_ARRAY_0061c240;
long null_ARRAY_0061c300;
long PTR_DAT_0061bec8;
long PTR_null_ARRAY_0061bf18;
long PTR_null_ARRAY_0061bf40;
long PTR_s__0061bec0;
long register0x00000020;
long stack0x00000008;
long stack0xffffffffffffffe0;
void
FUN_00404530 (uint uParm1)
{
  int iVar1;
  long lVar2;
  undefined8 uVar3;
  undefined **ppuVar4;
  undefined *puVar5;
  undefined *local_88;
  char *local_80;
  undefined8 local_78;
  char *local_70;
  char *local_68;
  char *local_60;
  char *local_58;
  char *local_50;
  char *local_48;
  char *local_40;
  char *local_38;
  char *local_30;
  undefined8 local_28;
  undefined8 local_20;

  if (uParm1 == 0)
    goto LAB_0040455d;
  func_0x00402040 (DAT_0061bfa0, "Try \'%s --help\' for more information.\n",
		   DAT_0061c0c8);
  do
    {
      func_0x004022b0 ((ulong) uParm1);
    LAB_0040455d:
      ;
      func_0x00401dd0 ("Usage: %s [OPTION]... FILE...\n", DAT_0061c0c8);
      uVar3 = DAT_0061bf80;
      func_0x00402050 ("Display file or file system status.\n", DAT_0061bf80);
      func_0x00402050
	("\nMandatory arguments to long options are mandatory for short options too.\n",
	 uVar3);
      func_0x00402050
	("  -L, --dereference     follow links\n  -f, --file-system     display file system status instead of file status\n",
	 uVar3);
      func_0x00402050
	("  -c  --format=FORMAT   use the specified FORMAT instead of the default;\n                          output a newline after each use of FORMAT\n      --printf=FORMAT   like --format, but interpret backslash escapes,\n                          and do not output a mandatory trailing newline;\n                          if you want a newline, include \\n in FORMAT\n  -t, --terse           print the information in terse form\n",
	 uVar3);
      func_0x00402050 ("      --help     display this help and exit\n",
		       uVar3);
      func_0x00402050
	("      --version  output version information and exit\n", uVar3);
      func_0x00402050
	("\nThe valid format sequences for files (without --file-system):\n\n  %a   access rights in octal (note \'#\' and \'0\' printf flags)\n  %A   access rights in human readable form\n  %b   number of blocks allocated (see %B)\n  %B   the size in bytes of each block reported by %b\n  %C   SELinux security context string\n",
	 uVar3);
      func_0x00402050
	("  %d   device number in decimal\n  %D   device number in hex\n  %f   raw mode in hex\n  %F   file type\n  %g   group ID of owner\n  %G   group name of owner\n",
	 uVar3);
      func_0x00402050
	("  %h   number of hard links\n  %i   inode number\n  %m   mount point\n  %n   file name\n  %N   quoted file name with dereference if symbolic link\n  %o   optimal I/O transfer size hint\n  %s   total size, in bytes\n  %t   major device type in hex, for character/block device special files\n  %T   minor device type in hex, for character/block device special files\n",
	 uVar3);
      func_0x00402050
	("  %u   user ID of owner\n  %U   user name of owner\n  %w   time of file birth, human-readable; - if unknown\n  %W   time of file birth, seconds since Epoch; 0 if unknown\n  %x   time of last access, human-readable\n  %X   time of last access, seconds since Epoch\n  %y   time of last data modification, human-readable\n  %Y   time of last data modification, seconds since Epoch\n  %z   time of last status change, human-readable\n  %Z   time of last status change, seconds since Epoch\n\n",
	 uVar3);
      func_0x00402050
	("Valid format sequences for file systems:\n\n  %a   free blocks available to non-superuser\n  %b   total data blocks in file system\n  %c   total file nodes in file system\n  %d   free file nodes in file system\n  %f   free blocks in file system\n",
	 uVar3);
      func_0x00402050
	("  %i   file system ID in hex\n  %l   maximum length of filenames\n  %n   file name\n  %s   block size (for faster transfers)\n  %S   fundamental block size (for block counts)\n  %t   file system type in hex\n  %T   file system type in human readable form\n",
	 uVar3);
      func_0x00401dd0
	("\n--terse is equivalent to the following FORMAT:\n    %s",
	 "%n %s %b %f %u %g %D %i %h %t %T %X %Y %Z %W %o\n");
      func_0x00401dd0
	("--terse --file-system is equivalent to the following FORMAT:\n    %s",
	 "%n %i %l %t %s %S %b %f %a %c %d\n");
      func_0x00401dd0
	("\nNOTE: your shell may have its own version of %s, which usually supersedes\nthe version described here.  Please refer to your shell\'s documentation\nfor details about the options it supports.\n",
	 &DAT_00415de2);
      local_88 = &DAT_00415de0;
      local_80 = "test invocation";
      puVar5 = &DAT_00415de0;
      local_78 = 0x415e48;
      local_70 = "Multi-call invocation";
      local_68 = "sha224sum";
      local_60 = "sha2 utilities";
      local_58 = "sha256sum";
      local_50 = "sha2 utilities";
      local_48 = "sha384sum";
      local_40 = "sha2 utilities";
      local_38 = "sha512sum";
      local_30 = "sha2 utilities";
      local_28 = 0;
      local_20 = 0;
      ppuVar4 = &local_88;
      do
	{
	  iVar1 = func_0x004021a0 (&DAT_00415de2, puVar5);
	  if (iVar1 == 0)
	    break;
	  ppuVar4 = ppuVar4 + 2;
	  puVar5 = *ppuVar4;
	}
      while (puVar5 != (undefined *) 0x0);
      puVar5 = ppuVar4[1];
      if (puVar5 == (undefined *) 0x0)
	{
	  func_0x00401dd0 ("\n%s online help: <%s>\n", "GNU coreutils",
			   "https://www.gnu.org/software/coreutils/");
	  lVar2 = func_0x00402210 (5, 0);
	  if (lVar2 != 0)
	    {
	      iVar1 = func_0x004020c0 (lVar2, &DAT_00415e69, 3);
	      if (iVar1 != 0)
		{
		  puVar5 = &DAT_00415de2;
		  goto LAB_00404799;
		}
	    }
	  func_0x00401dd0 ("Full documentation at: <%s%s>\n",
			   "https://www.gnu.org/software/coreutils/",
			   &DAT_00415de2);
	LAB_004047c2:
	  ;
	  puVar5 = &DAT_00415de2;
	  uVar3 = 0x415e01;
	}
      else
	{
	  func_0x00401dd0 ("\n%s online help: <%s>\n", "GNU coreutils",
			   "https://www.gnu.org/software/coreutils/");
	  lVar2 = func_0x00402210 (5, 0);
	  if (lVar2 != 0)
	    {
	      iVar1 = func_0x004020c0 (lVar2, &DAT_00415e69, 3);
	      if (iVar1 != 0)
		{
		LAB_00404799:
		  ;
		  func_0x00401dd0
		    ("Report %s translation bugs to <https://translationproject.org/team/>\n",
		     &DAT_00415de2);
		}
	    }
	  func_0x00401dd0 ("Full documentation at: <%s%s>\n",
			   "https://www.gnu.org/software/coreutils/",
			   &DAT_00415de2);
	  uVar3 = 0x418a87;
	  if (puVar5 == &DAT_00415de2)
	    goto LAB_004047c2;
	}
      func_0x00401dd0
	("or available locally via: info \'(coreutils) %s%s\'\n", puVar5,
	 uVar3);
    }
  while (true);
}

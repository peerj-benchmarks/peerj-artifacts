
#include "mknod.h"

long null_ARRAY_0061082_0_8_;
long null_ARRAY_0061082_8_8_;
long null_ARRAY_00610a0_0_8_;
long null_ARRAY_00610a0_16_8_;
long null_ARRAY_00610a0_24_8_;
long null_ARRAY_00610a0_32_8_;
long null_ARRAY_00610a0_40_8_;
long null_ARRAY_00610a0_48_8_;
long null_ARRAY_00610a0_8_8_;
long null_ARRAY_00610a4_0_4_;
long null_ARRAY_00610a4_16_8_;
long null_ARRAY_00610a4_4_4_;
long null_ARRAY_00610a4_8_4_;
long local_2f_1_1_;
long local_4_4_4_;
long local_5b_1_1_;
long local_9d_0_4_;
long local_a0_0_8_;
long local_a0_4_6_;
long local_a0_8_2_;
long local_a1_4_4_;
long local_a2_4_4_;
long local_a3_0_4_;
long local_a3_4_4_;
long local_a4_0_2_;
long local_a4_0_4_;
long local_a4_0_8_;
long local_a4_1_9_;
long local_a4_4_6_;
long local_a4_8_2_;
long local_a5_0_4_;
long local_a5_4_4_;
long DAT_00000010;
long DAT_0040d5c0;
long DAT_0040d64a;
long DAT_0040d65e;
long DAT_0040dfc0;
long DAT_0040dfc4;
long DAT_0040dfc8;
long DAT_0040dfcb;
long DAT_0040dfcd;
long DAT_0040dfd1;
long DAT_0040dfd5;
long DAT_0040e56b;
long DAT_0040eb08;
long DAT_0040ec11;
long DAT_0040ec17;
long DAT_0040ec29;
long DAT_0040ec2a;
long DAT_0040ec48;
long DAT_0040ec4c;
long DAT_0040ecce;
long DAT_006103e0;
long DAT_006103f0;
long DAT_00610400;
long DAT_006107c8;
long DAT_00610830;
long DAT_00610834;
long DAT_00610838;
long DAT_0061083c;
long DAT_00610840;
long DAT_00610850;
long DAT_00610860;
long DAT_00610868;
long DAT_00610880;
long DAT_00610888;
long DAT_006108d0;
long DAT_006108d8;
long DAT_006108e0;
long DAT_00610a78;
long DAT_00610a80;
long DAT_00610a88;
long DAT_00610a98;
long fde_0040f690;
long null_ARRAY_0040ddc0;
long null_ARRAY_0040ef60;
long null_ARRAY_0040f190;
long null_ARRAY_006107e0;
long null_ARRAY_00610820;
long null_ARRAY_006108a0;
long null_ARRAY_00610900;
long null_ARRAY_00610a00;
long null_ARRAY_00610a40;
long PTR_DAT_006107c0;
long PTR_null_ARRAY_00610818;
long register0x00000020;
void
FUN_004020c0 (uint uParm1)
{
  int iVar1;
  long lVar2;
  undefined8 uVar3;
  undefined **ppuVar4;
  char *pcVar5;
  undefined *puVar6;
  undefined *local_88;
  char *local_80;
  undefined8 local_78;
  char *local_70;
  char *local_68;
  char *local_60;
  char *local_58;
  char *local_50;
  char *local_48;
  char *local_40;
  char *local_38;
  char *local_30;
  undefined8 local_28;
  undefined8 local_20;

  if (uParm1 == 0)
    goto LAB_004020ed;
  func_0x00401750 (DAT_00610860, "Try \'%s --help\' for more information.\n",
		   DAT_006108e0);
  do
    {
      func_0x00401920 ((ulong) uParm1);
    LAB_004020ed:
      ;
      func_0x004015c0 ("Usage: %s [OPTION]... NAME TYPE [MAJOR MINOR]\n",
		       DAT_006108e0);
      uVar3 = DAT_00610840;
      func_0x00401760 ("Create the special file NAME of the given TYPE.\n",
		       DAT_00610840);
      func_0x00401760
	("\nMandatory arguments to long options are mandatory for short options too.\n",
	 uVar3);
      func_0x00401760
	("  -m, --mode=MODE    set file permission bits to MODE, not a=rw - umask\n",
	 uVar3);
      func_0x00401760
	("  -Z                   set the SELinux security context to default type\n      --context[=CTX]  like -Z, or if CTX is specified then set the SELinux\n                         or SMACK security context to CTX\n",
	 uVar3);
      func_0x00401760 ("      --help     display this help and exit\n",
		       uVar3);
      func_0x00401760
	("      --version  output version information and exit\n", uVar3);
      func_0x00401760
	("\nBoth MAJOR and MINOR must be specified when TYPE is b, c, or u, and they\nmust be omitted when TYPE is p.  If MAJOR or MINOR begins with 0x or 0X,\nit is interpreted as hexadecimal; otherwise, if it begins with 0, as octal;\notherwise, as decimal.  TYPE may be:\n",
	 uVar3);
      func_0x00401760
	("\n  b      create a block (buffered) special file\n  c, u   create a character (unbuffered) special file\n  p      create a FIFO\n",
	 uVar3);
      func_0x004015c0
	("\nNOTE: your shell may have its own version of %s, which usually supersedes\nthe version described here.  Please refer to your shell\'s documentation\nfor details about the options it supports.\n",
	 "mknod");
      local_88 = &DAT_0040d5c0;
      local_80 = "test invocation";
      puVar6 = &DAT_0040d5c0;
      local_78 = 0x40d629;
      local_70 = "Multi-call invocation";
      local_68 = "sha224sum";
      local_60 = "sha2 utilities";
      local_58 = "sha256sum";
      local_50 = "sha2 utilities";
      local_48 = "sha384sum";
      local_40 = "sha2 utilities";
      local_38 = "sha512sum";
      local_30 = "sha2 utilities";
      local_28 = 0;
      local_20 = 0;
      ppuVar4 = &local_88;
      do
	{
	  iVar1 = func_0x00401870 ("mknod", puVar6);
	  if (iVar1 == 0)
	    break;
	  ppuVar4 = ppuVar4 + 2;
	  puVar6 = *ppuVar4;
	}
      while (puVar6 != (undefined *) 0x0);
      pcVar5 = ppuVar4[1];
      if (pcVar5 == (char *) 0x0)
	{
	  func_0x004015c0 ("\n%s online help: <%s>\n", "GNU coreutils",
			   "https://www.gnu.org/software/coreutils/");
	  lVar2 = func_0x004018d0 (5, 0);
	  if (lVar2 != 0)
	    {
	      iVar1 = func_0x004017d0 (lVar2, &DAT_0040d64a, 3);
	      if (iVar1 != 0)
		{
		  pcVar5 = "mknod";
		  goto LAB_004022d9;
		}
	    }
	  func_0x004015c0 ("Full documentation at: <%s%s>\n",
			   "https://www.gnu.org/software/coreutils/",
			   "mknod");
	LAB_00402302:
	  ;
	  pcVar5 = "mknod";
	  uVar3 = 0x40d5e2;
	}
      else
	{
	  func_0x004015c0 ("\n%s online help: <%s>\n", "GNU coreutils",
			   "https://www.gnu.org/software/coreutils/");
	  lVar2 = func_0x004018d0 (5, 0);
	  if (lVar2 != 0)
	    {
	      iVar1 = func_0x004017d0 (lVar2, &DAT_0040d64a, 3);
	      if (iVar1 != 0)
		{
		LAB_004022d9:
		  ;
		  func_0x004015c0
		    ("Report %s translation bugs to <https://translationproject.org/team/>\n",
		     "mknod");
		}
	    }
	  func_0x004015c0 ("Full documentation at: <%s%s>\n",
			   "https://www.gnu.org/software/coreutils/",
			   "mknod");
	  uVar3 = 0x40ec47;
	  if (pcVar5 == "mknod")
	    goto LAB_00402302;
	}
      func_0x004015c0
	("or available locally via: info \'(coreutils) %s%s\'\n", pcVar5,
	 uVar3);
    }
  while (true);
}


#include "unlink.h"

long null_ARRAY_0060f42_0_8_;
long null_ARRAY_0060f42_8_8_;
long null_ARRAY_0060f60_0_8_;
long null_ARRAY_0060f60_16_8_;
long null_ARRAY_0060f60_24_8_;
long null_ARRAY_0060f60_32_8_;
long null_ARRAY_0060f60_40_8_;
long null_ARRAY_0060f60_48_8_;
long null_ARRAY_0060f60_8_8_;
long null_ARRAY_0060f64_0_4_;
long null_ARRAY_0060f64_16_8_;
long null_ARRAY_0060f64_4_4_;
long null_ARRAY_0060f64_8_4_;
long local_2f_1_1_;
long local_4_4_4_;
long local_5b_1_1_;
long local_9d_0_4_;
long local_a0_0_8_;
long local_a0_4_6_;
long local_a0_8_2_;
long local_a1_4_4_;
long local_a2_4_4_;
long local_a3_0_4_;
long local_a3_4_4_;
long local_a4_0_2_;
long local_a4_0_4_;
long local_a4_0_8_;
long local_a4_1_9_;
long local_a4_4_6_;
long local_a4_8_2_;
long local_a5_0_4_;
long local_a5_4_4_;
long DAT_00000010;
long DAT_0040c740;
long DAT_0040c7cb;
long DAT_0040ca18;
long DAT_0040cae0;
long DAT_0040cae4;
long DAT_0040cae8;
long DAT_0040caeb;
long DAT_0040caed;
long DAT_0040caf1;
long DAT_0040caf5;
long DAT_0040d0ab;
long DAT_0040d455;
long DAT_0040d559;
long DAT_0040d55f;
long DAT_0040d571;
long DAT_0040d572;
long DAT_0040d590;
long DAT_0040d594;
long DAT_0040d616;
long DAT_0060f000;
long DAT_0060f010;
long DAT_0060f020;
long DAT_0060f3c8;
long DAT_0060f430;
long DAT_0060f434;
long DAT_0060f438;
long DAT_0060f43c;
long DAT_0060f440;
long DAT_0060f450;
long DAT_0060f460;
long DAT_0060f468;
long DAT_0060f480;
long DAT_0060f488;
long DAT_0060f4d0;
long DAT_0060f4d8;
long DAT_0060f4e0;
long DAT_0060f678;
long DAT_0060f680;
long DAT_0060f688;
long DAT_0060f698;
long _DYNAMIC;
long fde_0040dfb0;
long null_ARRAY_0040c9e0;
long null_ARRAY_0040ca40;
long null_ARRAY_0040d8a0;
long null_ARRAY_0040dad0;
long null_ARRAY_0060f3e0;
long null_ARRAY_0060f420;
long null_ARRAY_0060f4a0;
long null_ARRAY_0060f500;
long null_ARRAY_0060f600;
long null_ARRAY_0060f640;
long PTR_DAT_0060f3c0;
long PTR_null_ARRAY_0060f418;
long register0x00000020;
void
FUN_00401b90 (uint uParm1)
{
  int iVar1;
  long lVar2;
  undefined8 uVar3;
  undefined **ppuVar4;
  char *pcVar5;
  undefined *puVar6;
  undefined *local_88;
  char *local_80;
  undefined8 local_78;
  char *local_70;
  char *local_68;
  char *local_60;
  char *local_58;
  char *local_50;
  char *local_48;
  char *local_40;
  char *local_38;
  char *local_30;
  undefined8 local_28;
  undefined8 local_20;

  if (uParm1 == 0)
    goto LAB_00401bbd;
  func_0x00401630 (DAT_0060f460, "Try \'%s --help\' for more information.\n",
		   DAT_0060f4e0);
  do
    {
      func_0x004017e0 ((ulong) uParm1);
    LAB_00401bbd:
      ;
      func_0x004014c0 ("Usage: %s FILE\n  or:  %s OPTION\n", DAT_0060f4e0,
		       DAT_0060f4e0);
      uVar3 = DAT_0060f440;
      func_0x00401640
	("Call the unlink function to remove the specified FILE.\n\n",
	 DAT_0060f440);
      func_0x00401640 ("      --help     display this help and exit\n",
		       uVar3);
      func_0x00401640
	("      --version  output version information and exit\n", uVar3);
      local_88 = &DAT_0040c740;
      local_80 = "test invocation";
      puVar6 = &DAT_0040c740;
      local_78 = 0x40c7aa;
      local_70 = "Multi-call invocation";
      local_68 = "sha224sum";
      local_60 = "sha2 utilities";
      local_58 = "sha256sum";
      local_50 = "sha2 utilities";
      local_48 = "sha384sum";
      local_40 = "sha2 utilities";
      local_38 = "sha512sum";
      local_30 = "sha2 utilities";
      local_28 = 0;
      local_20 = 0;
      ppuVar4 = &local_88;
      do
	{
	  iVar1 = func_0x00401730 ("unlink", puVar6);
	  if (iVar1 == 0)
	    break;
	  ppuVar4 = ppuVar4 + 2;
	  puVar6 = *ppuVar4;
	}
      while (puVar6 != (undefined *) 0x0);
      pcVar5 = ppuVar4[1];
      if (pcVar5 == (char *) 0x0)
	{
	  func_0x004014c0 ("\n%s online help: <%s>\n", "GNU coreutils",
			   "https://www.gnu.org/software/coreutils/");
	  lVar2 = func_0x00401790 (5, 0);
	  if (lVar2 != 0)
	    {
	      iVar1 = func_0x004016a0 (lVar2, &DAT_0040c7cb, 3);
	      if (iVar1 != 0)
		{
		  pcVar5 = "unlink";
		  goto LAB_00401d59;
		}
	    }
	  func_0x004014c0 ("Full documentation at: <%s%s>\n",
			   "https://www.gnu.org/software/coreutils/",
			   "unlink");
	LAB_00401d82:
	  ;
	  pcVar5 = "unlink";
	  uVar3 = 0x40c763;
	}
      else
	{
	  func_0x004014c0 ("\n%s online help: <%s>\n", "GNU coreutils",
			   "https://www.gnu.org/software/coreutils/");
	  lVar2 = func_0x00401790 (5, 0);
	  if (lVar2 != 0)
	    {
	      iVar1 = func_0x004016a0 (lVar2, &DAT_0040c7cb, 3);
	      if (iVar1 != 0)
		{
		LAB_00401d59:
		  ;
		  func_0x004014c0
		    ("Report %s translation bugs to <https://translationproject.org/team/>\n",
		     "unlink");
		}
	    }
	  func_0x004014c0 ("Full documentation at: <%s%s>\n",
			   "https://www.gnu.org/software/coreutils/",
			   "unlink");
	  uVar3 = 0x40d58f;
	  if (pcVar5 == "unlink")
	    goto LAB_00401d82;
	}
      func_0x004014c0
	("or available locally via: info \'(coreutils) %s%s\'\n", pcVar5,
	 uVar3);
    }
  while (true);
}

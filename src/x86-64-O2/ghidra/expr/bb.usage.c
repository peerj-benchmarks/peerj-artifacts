
#include "expr.h"

long null_ARRAY_006234c_0_8_;
long null_ARRAY_006234c_8_8_;
long null_ARRAY_006236c_0_8_;
long null_ARRAY_006236c_16_8_;
long null_ARRAY_006236c_24_8_;
long null_ARRAY_006236c_32_8_;
long null_ARRAY_006236c_40_8_;
long null_ARRAY_006236c_48_8_;
long null_ARRAY_006236c_8_8_;
long null_ARRAY_0062370_0_4_;
long null_ARRAY_0062370_16_8_;
long null_ARRAY_0062370_4_4_;
long null_ARRAY_0062370_8_4_;
long local_2f_1_1_;
long local_4_4_4_;
long local_5_1_1_;
long local_5b_1_1_;
long local_9_0_4_;
long local_9_4_4_;
long local_9d_0_4_;
long local_a0_0_8_;
long local_a_0_4_;
long local_a0_4_6_;
long local_a0_8_2_;
long local_a1_4_4_;
long local_a2_4_4_;
long local_a3_0_4_;
long local_a3_4_4_;
long local_a4_0_2_;
long local_a4_0_4_;
long local_a4_0_8_;
long local_a4_1_9_;
long local_a4_4_6_;
long local_a4_8_2_;
long local_a5_0_4_;
long local_a5_4_4_;
long DAT_00000010;
long DAT_0041cdc0;
long DAT_0041ce13;
long DAT_0041ce2f;
long DAT_0041ce4e;
long DAT_0041ce50;
long DAT_0041ce78;
long DAT_0041ce7a;
long DAT_0041ce7d;
long DAT_0041ce80;
long DAT_0041ce81;
long DAT_0041ce83;
long DAT_0041ce86;
long DAT_0041cea1;
long DAT_0041cea3;
long DAT_0041cea5;
long DAT_0041cea7;
long DAT_0041cf2e;
long DAT_0041cf30;
long DAT_0041cf32;
long DAT_0041cf6e;
long DAT_0041cf6f;
long DAT_0041daf0;
long DAT_0041daf6;
long DAT_0041daf8;
long DAT_0041dafc;
long DAT_0041db00;
long DAT_0041db03;
long DAT_0041db05;
long DAT_0041db09;
long DAT_0041db0d;
long DAT_0041e0ab;
long DAT_0041e0ad;
long DAT_0041e648;
long DAT_0041e657;
long DAT_0041e785;
long DAT_0041e812;
long DAT_0041e82b;
long DAT_0041f47e;
long DAT_0041f4fe;
long DAT_00623000;
long DAT_00623010;
long DAT_00623020;
long DAT_00623460;
long DAT_006234d0;
long DAT_006234d4;
long DAT_006234d8;
long DAT_006234dc;
long DAT_00623500;
long DAT_00623510;
long DAT_00623520;
long DAT_00623528;
long DAT_00623540;
long DAT_00623548;
long DAT_00623590;
long DAT_00623598;
long DAT_006235a0;
long DAT_006235a8;
long DAT_00623738;
long DAT_00623740;
long DAT_00623748;
long DAT_00623758;
long DAT_00623760;
long fde_00420220;
long null_ARRAY_0041da00;
long null_ARRAY_0041da60;
long null_ARRAY_0041f200;
long null_ARRAY_0041f240;
long null_ARRAY_0041f7a0;
long null_ARRAY_0041f9d0;
long null_ARRAY_006234c0;
long null_ARRAY_00623560;
long null_ARRAY_006235c0;
long null_ARRAY_006236c0;
long null_ARRAY_00623700;
long PTR_null_ARRAY_006234b8;
long register0x00000020;
long stack0x00000008;
void
FUN_00403b30 (uint uParm1)
{
  int iVar1;
  long lVar2;
  undefined8 uVar3;
  undefined **ppuVar4;
  undefined *puVar5;
  undefined *local_88;
  char *local_80;
  undefined8 local_78;
  char *local_70;
  char *local_68;
  char *local_60;
  char *local_58;
  char *local_50;
  char *local_48;
  char *local_40;
  char *local_38;
  char *local_30;
  undefined8 local_28;
  undefined8 local_20;

  if (uParm1 == 0)
    goto LAB_00403b5d;
  func_0x00401b50 (DAT_00623520, "Try \'%s --help\' for more information.\n",
		   DAT_006235a8);
  do
    {
      func_0x00401d50 ((ulong) uParm1);
    LAB_00403b5d:
      ;
      func_0x00401940 ("Usage: %s EXPRESSION\n  or:  %s OPTION\n",
		       DAT_006235a8, DAT_006235a8);
      func_0x00401d10 (10);
      uVar3 = DAT_00623500;
      func_0x00401b60 ("      --help     display this help and exit\n",
		       DAT_00623500);
      func_0x00401b60
	("      --version  output version information and exit\n", uVar3);
      func_0x00401b60
	("\nPrint the value of EXPRESSION to standard output.  A blank line below\nseparates increasing precedence groups.  EXPRESSION may be:\n\n  ARG1 | ARG2       ARG1 if it is neither null nor 0, otherwise ARG2\n\n  ARG1 & ARG2       ARG1 if neither argument is null or 0, otherwise 0\n",
	 uVar3);
      func_0x00401b60
	("\n  ARG1 < ARG2       ARG1 is less than ARG2\n  ARG1 <= ARG2      ARG1 is less than or equal to ARG2\n  ARG1 = ARG2       ARG1 is equal to ARG2\n  ARG1 != ARG2      ARG1 is unequal to ARG2\n  ARG1 >= ARG2      ARG1 is greater than or equal to ARG2\n  ARG1 > ARG2       ARG1 is greater than ARG2\n",
	 uVar3);
      func_0x00401b60
	("\n  ARG1 + ARG2       arithmetic sum of ARG1 and ARG2\n  ARG1 - ARG2       arithmetic difference of ARG1 and ARG2\n",
	 uVar3);
      func_0x00401b60
	("\n  ARG1 * ARG2       arithmetic product of ARG1 and ARG2\n  ARG1 / ARG2       arithmetic quotient of ARG1 divided by ARG2\n  ARG1 % ARG2       arithmetic remainder of ARG1 divided by ARG2\n",
	 uVar3);
      func_0x00401b60
	("\n  STRING : REGEXP   anchored pattern match of REGEXP in STRING\n\n  match STRING REGEXP        same as STRING : REGEXP\n  substr STRING POS LENGTH   substring of STRING, POS counted from 1\n  index STRING CHARS         index in STRING where any CHARS is found, or 0\n  length STRING              length of STRING\n",
	 uVar3);
      func_0x00401b60
	("  + TOKEN                    interpret TOKEN as a string, even if it is a\n                               keyword like \'match\' or an operator like \'/\'\n\n  ( EXPRESSION )             value of EXPRESSION\n",
	 uVar3);
      func_0x00401b60
	("\nBeware that many operators need to be escaped or quoted for shells.\nComparisons are arithmetic if both ARGs are numbers, else lexicographical.\nPattern matches return the string matched between \\( and \\) or null; if\n\\( and \\) are not used, they return the number of characters matched or 0.\n",
	 uVar3);
      func_0x00401b60
	("\nExit status is 0 if EXPRESSION is neither null nor 0, 1 if EXPRESSION is null\nor 0, 2 if EXPRESSION is syntactically invalid, and 3 if an error occurred.\n",
	 uVar3);
      local_88 = &DAT_0041cea5;
      local_80 = "test invocation";
      puVar5 = &DAT_0041cea5;
      local_78 = 0x41cf0d;
      local_70 = "Multi-call invocation";
      local_68 = "sha224sum";
      local_60 = "sha2 utilities";
      local_58 = "sha256sum";
      local_50 = "sha2 utilities";
      local_48 = "sha384sum";
      local_40 = "sha2 utilities";
      local_38 = "sha512sum";
      local_30 = "sha2 utilities";
      local_28 = 0;
      local_20 = 0;
      ppuVar4 = &local_88;
      do
	{
	  iVar1 = func_0x00401c90 (&DAT_0041cea7, puVar5);
	  if (iVar1 == 0)
	    break;
	  ppuVar4 = ppuVar4 + 2;
	  puVar5 = *ppuVar4;
	}
      while (puVar5 != (undefined *) 0x0);
      puVar5 = ppuVar4[1];
      if (puVar5 == (undefined *) 0x0)
	{
	  func_0x00401940 ("\n%s online help: <%s>\n", "GNU coreutils",
			   "https://www.gnu.org/software/coreutils/");
	  lVar2 = func_0x00401d00 (5, 0);
	  if (lVar2 != 0)
	    {
	      iVar1 = func_0x00401bd0 (lVar2, &DAT_0041cf2e, 3);
	      if (iVar1 != 0)
		{
		  puVar5 = &DAT_0041cea7;
		  goto LAB_00403d59;
		}
	    }
	  func_0x00401940 ("Full documentation at: <%s%s>\n",
			   "https://www.gnu.org/software/coreutils/",
			   &DAT_0041cea7);
	LAB_00403d82:
	  ;
	  puVar5 = &DAT_0041cea7;
	  uVar3 = 0x41cec6;
	}
      else
	{
	  func_0x00401940 ("\n%s online help: <%s>\n", "GNU coreutils",
			   "https://www.gnu.org/software/coreutils/");
	  lVar2 = func_0x00401d00 (5, 0);
	  if (lVar2 != 0)
	    {
	      iVar1 = func_0x00401bd0 (lVar2, &DAT_0041cf2e, 3);
	      if (iVar1 != 0)
		{
		LAB_00403d59:
		  ;
		  func_0x00401940
		    ("Report %s translation bugs to <https://translationproject.org/team/>\n",
		     &DAT_0041cea7);
		}
	    }
	  func_0x00401940 ("Full documentation at: <%s%s>\n",
			   "https://www.gnu.org/software/coreutils/",
			   &DAT_0041cea7);
	  uVar3 = 0x41e784;
	  if (puVar5 == &DAT_0041cea7)
	    goto LAB_00403d82;
	}
      func_0x00401940
	("or available locally via: info \'(coreutils) %s%s\'\n", puVar5,
	 uVar3);
    }
  while (true);
}

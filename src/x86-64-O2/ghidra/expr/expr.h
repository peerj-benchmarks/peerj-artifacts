typedef unsigned char   undefined;

typedef unsigned char    byte;
typedef unsigned char    dwfenc;
typedef unsigned int    dword;
typedef unsigned long    qword;
typedef unsigned char    undefined1;
typedef unsigned short    undefined2;
typedef unsigned int    undefined4;
typedef unsigned long    undefined6;
typedef unsigned long    undefined8;
typedef unsigned short    word;
#define unkbyte9   unsigned long long
#define unkbyte10   unsigned long long
#define unkbyte11   unsigned long long
#define unkbyte12   unsigned long long
#define unkbyte13   unsigned long long
#define unkbyte14   unsigned long long
#define unkbyte15   unsigned long long
#define unkbyte16   unsigned long long

#define unkuint9   unsigned long long
#define unkuint10   unsigned long long
#define unkuint11   unsigned long long
#define unkuint12   unsigned long long
#define unkuint13   unsigned long long
#define unkuint14   unsigned long long
#define unkuint15   unsigned long long
#define unkuint16   unsigned long long

#define unkint9   long long
#define unkint10   long long
#define unkint11   long long
#define unkint12   long long
#define unkint13   long long
#define unkint14   long long
#define unkint15   long long
#define unkint16   long long

#define unkfloat1   float
#define unkfloat2   float
#define unkfloat3   float
#define unkfloat5   double
#define unkfloat6   double
#define unkfloat7   double
#define unkfloat9   long double
#define unkfloat11   long double
#define unkfloat12   long double
#define unkfloat13   long double
#define unkfloat14   long double
#define unkfloat15   long double
#define unkfloat16   long double

#define BADSPACEBASE   void
#define code   void

#define ulong long
#define true 1
#define bool int
#define false 0
#define float10 int
#define ushort int
#define undefined7 long
#define undefined2 long
#define uint5 long
#define uint6 long

// Add definition for uint
typedef unsigned int    uint;

#define sbyte char

extern imperfection_wrapper(void);
typedef struct eh_frame_hdr eh_frame_hdr, *Peh_frame_hdr;

struct eh_frame_hdr {
    byte eh_frame_hdr_version; // Exception Handler Frame Header Version
    dwfenc eh_frame_pointer_encoding; // Exception Handler Frame Pointer Encoding
    dwfenc eh_frame_desc_entry_count_encoding; // Encoding of # of Exception Handler FDEs
    dwfenc eh_frame_table_encoding; // Exception Handler Table Encoding
};

typedef struct fde_table_entry fde_table_entry, *Pfde_table_entry;

struct fde_table_entry {
    dword initial_loc; // Initial Location
    dword data_loc; // Data location
};

typedef enum Elf64_DynTag {
    DT_AUDIT=1879047932,
    DT_AUXILIARY=2147483645,
    DT_BIND_NOW=24,
    DT_CHECKSUM=1879047672,
    DT_CONFIG=1879047930,
    DT_DEBUG=21,
    DT_DEPAUDIT=1879047931,
    DT_ENCODING=32,
    DT_FEATURE_1=1879047676,
    DT_FILTER=2147483647,
    DT_FINI=13,
    DT_FINI_ARRAY=26,
    DT_FINI_ARRAYSZ=28,
    DT_FLAGS=30,
    DT_FLAGS_1=1879048187,
    DT_GNU_CONFLICT=1879047928,
    DT_GNU_CONFLICTSZ=1879047670,
    DT_GNU_HASH=1879047925,
    DT_GNU_LIBLIST=1879047929,
    DT_GNU_LIBLISTSZ=1879047671,
    DT_GNU_PRELINKED=1879047669,
    DT_HASH=4,
    DT_INIT=12,
    DT_INIT_ARRAY=25,
    DT_INIT_ARRAYSZ=27,
    DT_JMPREL=23,
    DT_MOVEENT=1879047674,
    DT_MOVESZ=1879047675,
    DT_MOVETAB=1879047934,
    DT_NEEDED=1,
    DT_NULL=0,
    DT_PLTGOT=3,
    DT_PLTPAD=1879047933,
    DT_PLTPADSZ=1879047673,
    DT_PLTREL=20,
    DT_PLTRELSZ=2,
    DT_POSFLAG_1=1879047677,
    DT_PREINIT_ARRAYSZ=33,
    DT_REL=17,
    DT_RELA=7,
    DT_RELACOUNT=1879048185,
    DT_RELAENT=9,
    DT_RELASZ=8,
    DT_RELCOUNT=1879048186,
    DT_RELENT=19,
    DT_RELSZ=18,
    DT_RPATH=15,
    DT_RUNPATH=29,
    DT_SONAME=14,
    DT_STRSZ=10,
    DT_STRTAB=5,
    DT_SYMBOLIC=16,
    DT_SYMENT=11,
    DT_SYMINENT=1879047679,
    DT_SYMINFO=1879047935,
    DT_SYMINSZ=1879047678,
    DT_SYMTAB=6,
    DT_TEXTREL=22,
    DT_TLSDESC_GOT=1879047927,
    DT_TLSDESC_PLT=1879047926,
    DT_VERDEF=1879048188,
    DT_VERDEFNUM=1879048189,
    DT_VERNEED=1879048190,
    DT_VERNEEDNUM=1879048191,
    DT_VERSYM=1879048176
} Elf64_DynTag;

typedef enum Elf_ProgramHeaderType {
    PT_DYNAMIC=2,
    PT_GNU_EH_FRAME=1685382480,
    PT_GNU_RELRO=1685382482,
    PT_GNU_STACK=1685382481,
    PT_INTERP=3,
    PT_LOAD=1,
    PT_NOTE=4,
    PT_NULL=0,
    PT_PHDR=6,
    PT_SHLIB=5,
    PT_TLS=7
} Elf_ProgramHeaderType;

typedef struct Elf64_Dyn Elf64_Dyn, *PElf64_Dyn;

struct Elf64_Dyn {
    enum Elf64_DynTag d_tag;
    qword d_val;
};

typedef struct Elf64_Shdr Elf64_Shdr, *PElf64_Shdr;

typedef enum Elf_SectionHeaderType {
    SHT_CHECKSUM=1879048184,
    SHT_DYNAMIC=6,
    SHT_DYNSYM=11,
    SHT_FINI_ARRAY=15,
    SHT_GNU_ATTRIBUTES=1879048181,
    SHT_GNU_HASH=1879048182,
    SHT_GNU_LIBLIST=1879048183,
    SHT_GNU_verdef=1879048189,
    SHT_GNU_verneed=1879048190,
    SHT_GNU_versym=1879048191,
    SHT_GROUP=17,
    SHT_HASH=5,
    SHT_INIT_ARRAY=14,
    SHT_NOBITS=8,
    SHT_NOTE=7,
    SHT_NULL=0,
    SHT_PREINIT_ARRAY=16,
    SHT_PROGBITS=1,
    SHT_REL=9,
    SHT_RELA=4,
    SHT_SHLIB=10,
    SHT_STRTAB=3,
    SHT_SUNW_COMDAT=1879048187,
    SHT_SUNW_move=1879048186,
    SHT_SUNW_syminfo=1879048188,
    SHT_SYMTAB=2,
    SHT_SYMTAB_SHNDX=18
} Elf_SectionHeaderType;

struct Elf64_Shdr {
    dword sh_name;
    enum Elf_SectionHeaderType sh_type;
    qword sh_flags;
    qword sh_addr;
    qword sh_offset;
    qword sh_size;
    dword sh_link;
    dword sh_info;
    qword sh_addralign;
    qword sh_entsize;
};

typedef struct Elf64_Phdr Elf64_Phdr, *PElf64_Phdr;

struct Elf64_Phdr {
    enum Elf_ProgramHeaderType p_type;
    dword p_flags;
    qword p_offset;
    qword p_vaddr;
    qword p_paddr;
    qword p_filesz;
    qword p_memsz;
    qword p_align;
};

typedef struct Elf64_Rela Elf64_Rela, *PElf64_Rela;

struct Elf64_Rela {
    qword r_offset; // location to apply the relocation action
    qword r_info; // the symbol table index and the type of relocation
    qword r_addend; // a constant addend used to compute the relocatable field value
};

typedef struct Elf64_Sym Elf64_Sym, *PElf64_Sym;

struct Elf64_Sym {
    dword st_name;
    byte st_info;
    byte st_other;
    word st_shndx;
    qword st_value;
    qword st_size;
};

typedef struct Elf64_Ehdr Elf64_Ehdr, *PElf64_Ehdr;

struct Elf64_Ehdr {
    byte e_ident_magic_num;
    char e_ident_magic_str[3];
    byte e_ident_class;
    byte e_ident_data;
    byte e_ident_version;
    byte e_ident_pad[9];
    word e_type;
    word e_machine;
    dword e_version;
    qword e_entry;
    qword e_phoff;
    qword e_shoff;
    dword e_flags;
    word e_ehsize;
    word e_phentsize;
    word e_phnum;
    word e_shentsize;
    word e_shnum;
    word e_shstrndx;
};




undefined8 _DT_INIT(void);
void FUN_00401920(void);
void thunk_FUN_00624030(void);
void thunk_FUN_006240f0(void);
void FUN_00401ea0(char cParm1);
ulong FUN_00401ec0(uint uParm1,undefined8 *puParm2);
void entry(void);
void FUN_00402020(undefined8 *puParm1);
void FUN_00402050(void);
void FUN_004020d0(void);
void FUN_00402150(void);
long FUN_00402190(long *plParm1,long *plParm2,long *plParm3);
ulong FUN_004021d0(undefined8 uParm1);
void FUN_00402210(long *plParm1,long *plParm2,long *plParm3);
void FUN_00402240(long *plParm1,long *plParm2,long *plParm3);
void FUN_00402270(long *plParm1,long *plParm2,long *plParm3);
void FUN_004022d0(long *plParm1,long *plParm2,long *plParm3);
void FUN_00402300(undefined8 uParm1,undefined8 uParm2,undefined8 uParm3,undefined8 uParm4,undefined8 uParm5,undefined8 uParm6);
void FUN_00402340(int *piParm1);
undefined4 * FUN_00402370(undefined8 uParm1);
ulong FUN_004023b0(int *piParm1);
undefined8 FUN_00402430(int *piParm1);
void FUN_004024e0(int *piParm1);
undefined4 * FUN_00402530(long lParm1,long lParm2);
undefined4 * FUN_00402940(byte bParm1);
undefined8 FUN_00403490(byte bParm1);
long FUN_00403500(byte bParm1);
long FUN_00403630(byte bParm1);
undefined4 * FUN_00403700(byte bParm1);
undefined4 * FUN_004039d0(byte bParm1);
undefined4 * FUN_00403a80(byte bParm1);
void FUN_00403b30(uint uParm1);
void FUN_00403d90(void);
char * FUN_00403e20(long lParm1,long lParm2);
void FUN_00403ec0(undefined4 uParm1,undefined4 uParm2,undefined4 uParm3,undefined4 uParm4,undefined4 uParm5,undefined4 uParm6,undefined4 uParm7,undefined4 uParm8,undefined8 param_1,undefined8 param_2,undefined8 param_3,undefined8 param_4,undefined8 param_5,code *param_6);
byte * FUN_00403fd0(byte *pbParm1,ulong uParm2);
long FUN_00404230(byte *pbParm1);
void FUN_00404430(long lParm1);
undefined8 * FUN_004044d0(undefined8 *puParm1,int iParm2);
undefined * FUN_00404540(char *pcParm1,int iParm2);
ulong FUN_00404610(undefined *param_1,ulong param_2,long param_3,ulong param_4,uint param_5,ulong param_6,long param_7,char *param_8,char *param_9);
undefined1 * FUN_00405510(uint uParm1,undefined8 uParm2,undefined8 uParm3,uint *puParm4);
void FUN_004056c0(uint uParm1,undefined8 uParm2,undefined8 uParm3);
void FUN_004056f0(undefined8 uParm1,undefined8 uParm2,byte bParm3);
void FUN_00405790(undefined8 uParm1);
long FUN_004057b0(long lParm1,undefined8 uParm2);
ulong FUN_004057f0(byte *pbParm1,byte *pbParm2);
long FUN_00405a40(undefined8 uParm1,long lParm2,undefined8 uParm3,undefined8 uParm4,long *plParm5,undefined8 uParm6);
void FUN_00405ca0(void);
void FUN_00405d10(long lParm1);
long FUN_00405d30(long lParm1,long lParm2);
void FUN_00405d70(undefined8 uParm1,undefined8 uParm2);
void FUN_00405da0(undefined8 uParm1);
void FUN_00405dc0(void);
ulong FUN_00405df0(byte *pbParm1,byte **ppbParm2,uint uParm3,long *plParm4,long lParm5);
undefined8 FUN_00406490(ulong uParm1,ulong uParm2);
void FUN_004064e0(undefined8 uParm1);
void FUN_00406520(undefined8 uParm1,uint uParm2,undefined8 uParm3,undefined8 uParm4);
void FUN_00406590(void);
void FUN_004065d0(undefined4 uParm1,undefined4 uParm2,undefined4 uParm3,undefined4 uParm4,undefined4 uParm5,undefined4 uParm6,undefined4 uParm7,undefined4 uParm8,ulong param_1,ulong param_2,undefined8 param_3,undefined8 param_4,undefined8 param_5,undefined8 param_6);
void FUN_004066b0(long lParm1,int *piParm2);
ulong FUN_00406790(int param_1,undefined8 *param_2,char *param_3,long *param_4,int *param_5,int param_6,int *param_7,int param_8,undefined8 param_9);
ulong FUN_00406d10(uint param_1,undefined8 *param_2,char *param_3,long param_4,undefined8 param_5,ulong param_6,uint *param_7,int param_8);
void FUN_00407260(void);
void FUN_004072c0(void);
void FUN_004072e0(long lParm1);
ulong FUN_00407300(uint *puParm1,byte *pbParm2,long lParm3);
ulong * FUN_00407370(ulong *puParm1,char cParm2,ulong uParm3);
void FUN_00407460(long lParm1,long lParm2);
void FUN_004074a0(long *plParm1);
undefined8 FUN_004074f0(char *pcParm1,long lParm2,ulong uParm3);
undefined8 FUN_00407620(long lParm1,long lParm2);
ulong FUN_00407640(long lParm1,uint uParm2,long lParm3,long lParm4,long lParm5);
undefined8 FUN_00407870(long lParm1,long lParm2,undefined8 uParm3,undefined8 uParm4,long lParm5);
undefined8 FUN_004078e0(long *plParm1,code *pcParm2,undefined8 uParm3);
undefined8 FUN_00407950(long *plParm1,code *pcParm2,undefined8 uParm3);
undefined8 FUN_004079b0(long lParm1,ulong uParm2);
undefined8 FUN_00407a50(long *plParm1,undefined8 uParm2);
long * FUN_00407ac0(long *plParm1,long lParm2);
undefined8 FUN_00407c00(long *plParm1,long lParm2);
undefined8 FUN_00407c50(long *plParm1,ulong *puParm2,ulong uParm3);
void FUN_00407d30(long lParm1);
void FUN_00407db0(long *plParm1);
undefined8 FUN_00407f60(long *plParm1);
undefined8 FUN_00408560(long lParm1,int iParm2);
undefined8 FUN_00408660(long lParm1,long lParm2);
void FUN_004086f0(undefined8 *puParm1);
void FUN_00408710(undefined8 *puParm1);
undefined8 FUN_00408740(undefined8 uParm1,long lParm2);
long FUN_00408760(long *plParm1,undefined8 uParm2,undefined8 uParm3);
void FUN_00408950(long *plParm1,long lParm2,undefined8 uParm3);
undefined8 FUN_00408a00(long *plParm1,long lParm2,long lParm3,long lParm4,uint uParm5);
void FUN_00408c80(long lParm1);
void FUN_00408ce0(long lParm1);
void FUN_00408d20(long *plParm1);
void FUN_00408e90(long lParm1);
undefined8 FUN_00408f50(long *plParm1);
undefined8 FUN_00408fc0(long lParm1,long lParm2);
undefined8 FUN_00409230(long lParm1,long lParm2);
long FUN_00409290(long lParm1,long *plParm2,long lParm3);
undefined8 FUN_004092f0(long *plParm1,long lParm2,long lParm3,long lParm4,uint uParm5);
void FUN_004093f0(long *plParm1,long *plParm2,long lParm3);
undefined8 FUN_00409430(long lParm1,long lParm2);
undefined8 FUN_004094c0(undefined8 uParm1,long lParm2);
long FUN_00409530(long lParm1,long *plParm2,long lParm3);
undefined8 FUN_004095b0(long param_1,long *param_2,long *param_3,undefined8 param_4,undefined8 param_5,undefined8 param_6,undefined8 param_7);
undefined8 *FUN_004096c0(undefined8 *puParm1,int *piParm2,long *plParm3,long *plParm4,undefined *puParm5);
long ** FUN_00409790(long **pplParm1,long lParm2);
long FUN_00409850(undefined4 *puParm1,long *plParm2,long lParm3);
ulong FUN_00409a60(undefined8 uParm1,long lParm2);
undefined8 FUN_00409af0(long lParm1,long *plParm2,long *plParm3,undefined8 uParm4);
undefined8 FUN_00409c40(long *plParm1,long lParm2);
undefined8 FUN_00409e30(undefined8 *puParm1,long *plParm2,long lParm3,byte bParm4);
ulong FUN_0040a0b0(long *plParm1,undefined8 *puParm2,long lParm3,ulong uParm4);
undefined8 FUN_0040a1d0(long *plParm1,long *plParm2,undefined8 *puParm3);
undefined8 FUN_0040a260(long *plParm1,long lParm2,long lParm3);
ulong * FUN_0040a420(undefined4 *puParm1,long *plParm2,long lParm3,uint uParm4);
ulong * FUN_0040a7d0(undefined4 *puParm1,long *plParm2,long lParm3);
ulong FUN_0040aa00(undefined8 uParm1,long lParm2,long lParm3,long lParm4);
ulong FUN_0040aab0(long lParm1,long lParm2,long lParm3,undefined8 uParm4,uint uParm5);
ulong FUN_0040adc0(long *plParm1,long lParm2);
undefined8 FUN_0040b8b0(long *plParm1,long *plParm2,long *plParm3,long *plParm4,long *plParm5);
ulong FUN_0040ba70(long *plParm1,long lParm2,long lParm3,long lParm4);
ulong FUN_0040bce0(long lParm1,long lParm2,ulong uParm3);
ulong FUN_0040bdb0(long lParm1,long *plParm2,long lParm3,undefined8 uParm4,undefined8 uParm5);
undefined8 FUN_0040bed0(byte **ppbParm1,byte *pbParm2,uint uParm3);
long FUN_0040c670(long lParm1,long lParm2,long *plParm3,undefined8 uParm4);
undefined8 FUN_0040c750(long *plParm1,long lParm2);
undefined8 FUN_0040c7f0(long *plParm1,long *plParm2,undefined8 *puParm3,long lParm4,undefined8 uParm5,undefined8 *puParm6);
undefined8 FUN_0040c8c0(long param_1,ulong *param_2,long *param_3,long *param_4,long *param_5,char *param_6,ulong param_7);
long FUN_0040d0e0(long lParm1,undefined8 uParm2,undefined8 uParm3,char *pcParm4,byte bParm5,int *piParm6);
void FUN_0040d340(long **pplParm1,long *plParm2,long *plParm3,long lParm4);
ulong FUN_0040d7a0(long lParm1,ulong *puParm2,long lParm3,long lParm4,long lParm5);
ulong FUN_0040da50(long **pplParm1,long lParm2,long lParm3,long *plParm4,char cParm5);
ulong FUN_0040e280(long param_1,long *param_2,long param_3,long param_4,undefined8 param_5,long param_6,uint param_7);
undefined8 FUN_0040ea00(long lParm1,long *plParm2,undefined8 *puParm3,undefined8 uParm4,long lParm5);
ulong FUN_0040ebb0(long lParm1,long *plParm2,long *plParm3);
long FUN_0040f3e0(int *piParm1,long lParm2,long lParm3);
ulong FUN_0040f5b0(long lParm1,ulong *puParm2,ulong uParm3,long lParm4);
ulong FUN_0040fe20(long lParm1,long *plParm2);
ulong FUN_00410140(long **param_1,long param_2,ulong param_3,long param_4,long param_5,ulong param_6,long *param_7,undefined8 *param_8,uint param_9);
undefined8 FUN_00411920(undefined4 *puParm1,long *plParm2,char *pcParm3,int iParm4,undefined8 uParm5,char cParm6);
undefined8 FUN_00411b70(long *plParm1,long *plParm2,ulong uParm3);
long FUN_00412210(long lParm1,byte *pbParm2,undefined8 uParm3);
long FUN_004122d0(long lParm1,long *plParm2,undefined8 *puParm3,ulong uParm4,long lParm5,int *piParm6);
long FUN_00413720(undefined8 uParm1,long *plParm2,long lParm3,undefined8 uParm4,long lParm5,int *piParm6);
long FUN_004138a0(long lParm1,long *plParm2,long lParm3,ulong uParm4,long lParm5,int *piParm6);
ulong FUN_00413a40(long **pplParm1,undefined8 uParm2,ulong uParm3,long *plParm4);
char * FUN_00414730(undefined8 uParm1,undefined8 uParm2,long lParm3);
undefined8 FUN_00414790(long *plParm1);
long FUN_00414830(long param_1,undefined8 param_2,long param_3,long param_4,long param_5,undefined8 param_6,ulong *param_7,char param_8);
void FUN_00414cd0(long *plParm1);
void FUN_00414d20(void);
ulong FUN_00414d40(undefined8 uParm1,undefined8 uParm2,undefined8 uParm3);
ulong FUN_00414e10(undefined8 uParm1);
undefined8 FUN_00414e80(void);
ulong FUN_00414e90(ulong uParm1);
char * FUN_00414f30(void);
void FUN_00415280(undefined8 uParm1);
ulong FUN_00415310(long lParm1);
undefined8 FUN_004153a0(void);
void FUN_004153b0(void);
undefined4 * FUN_004153c0(ulong uParm1,uint *puParm2,ulong uParm3,uint *puParm4,long *plParm5);
ulong FUN_00415520(void);
uint * FUN_00415790(ulong uParm1,long lParm2,ulong uParm3,uint *puParm4,ulong *puParm5);
char * FUN_00415d50(int iParm1,long lParm2,long lParm3,long lParm4,uint uParm5);
void FUN_004162f0(uint param_1);
ulong FUN_00416470(void);
void FUN_00416690(undefined8 uParm1,uint uParm2);
undefined8 *FUN_00416800(undefined8 *puParm1,undefined8 *puParm2,undefined8 *puParm3,undefined8 uParm4);
double FUN_0041bdd0(int *piParm1);
void FUN_0041be10(uint *param_1);
undefined8 FUN_0041be90(uint *puParm1,ulong *puParm2);
undefined8 FUN_0041c0b0(byte *pbParm1,ulong *puParm2,byte **ppbParm3);
ulong FUN_0041cd00(void);
ulong FUN_0041cd30(void);
void FUN_0041cd70(void);
undefined8 _DT_FINI(void);
undefined FUN_00624000();
undefined FUN_00624008();
undefined FUN_00624010();
undefined FUN_00624018();
undefined FUN_00624020();
undefined FUN_00624028();
undefined FUN_00624030();
undefined FUN_00624038();
undefined FUN_00624040();
undefined FUN_00624048();
undefined FUN_00624050();
undefined FUN_00624058();
undefined FUN_00624060();
undefined FUN_00624068();
undefined FUN_00624070();
undefined FUN_00624078();
undefined FUN_00624080();
undefined FUN_00624088();
undefined FUN_00624090();
undefined FUN_00624098();
undefined FUN_006240a0();
undefined FUN_006240a8();
undefined FUN_006240b0();
undefined FUN_006240b8();
undefined FUN_006240c0();
undefined FUN_006240c8();
undefined FUN_006240d0();
undefined FUN_006240d8();
undefined FUN_006240e0();
undefined FUN_006240e8();
undefined FUN_006240f0();
undefined FUN_006240f8();
undefined FUN_00624100();
undefined FUN_00624108();
undefined FUN_00624110();
undefined FUN_00624118();
undefined FUN_00624120();
undefined FUN_00624128();
undefined FUN_00624130();
undefined FUN_00624138();
undefined FUN_00624140();
undefined FUN_00624148();
undefined FUN_00624150();
undefined FUN_00624158();
undefined FUN_00624160();
undefined FUN_00624168();
undefined FUN_00624170();
undefined FUN_00624178();
undefined FUN_00624180();
undefined FUN_00624188();
undefined FUN_00624190();
undefined FUN_00624198();
undefined FUN_006241a0();
undefined FUN_006241a8();
undefined FUN_006241b0();
undefined FUN_006241b8();
undefined FUN_006241c0();
undefined FUN_006241c8();
undefined FUN_006241d0();
undefined FUN_006241d8();
undefined FUN_006241e0();
undefined FUN_006241e8();
undefined FUN_006241f0();
undefined FUN_006241f8();
undefined FUN_00624200();
undefined FUN_00624208();
undefined FUN_00624210();
undefined FUN_00624218();
undefined FUN_00624220();
undefined FUN_00624228();
undefined FUN_00624230();
undefined FUN_00624238();
undefined FUN_00624240();
undefined FUN_00624248();
undefined FUN_00624250();
undefined FUN_00624258();
undefined FUN_00624260();
undefined FUN_00624268();
undefined FUN_00624270();
undefined FUN_00624278();
undefined FUN_00624280();
undefined FUN_00624288();
undefined FUN_00624290();
undefined FUN_00624298();
undefined FUN_006242a0();
undefined FUN_006242a8();
undefined FUN_006242b0();


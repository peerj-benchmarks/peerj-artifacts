
#include "timeout.h"

long null_ARRAY_006114e_0_8_;
long null_ARRAY_006114e_8_8_;
long null_ARRAY_006118c_0_8_;
long null_ARRAY_006118c_16_8_;
long null_ARRAY_006118c_24_8_;
long null_ARRAY_006118c_32_8_;
long null_ARRAY_006118c_40_8_;
long null_ARRAY_006118c_48_8_;
long null_ARRAY_006118c_8_8_;
long null_ARRAY_0061190_0_4_;
long null_ARRAY_0061190_16_8_;
long null_ARRAY_0061190_4_4_;
long null_ARRAY_0061190_8_4_;
long local_2f_1_1_;
long local_5b_1_1_;
long local_9d_0_4_;
long local_a0_0_8_;
long local_a0_4_6_;
long local_a0_8_2_;
long local_a1_4_4_;
long local_a2_4_4_;
long local_a3_0_4_;
long local_a3_4_4_;
long local_a4_0_2_;
long local_a4_0_4_;
long local_a4_0_8_;
long local_a4_1_9_;
long local_a4_4_6_;
long local_a4_8_2_;
long local_a5_0_4_;
long local_a5_4_4_;
long DAT_00000010;
long DAT_0040d9ad;
long DAT_0040d9c5;
long DAT_0040da51;
long DAT_0040e4a3;
long DAT_0040e510;
long DAT_0040e514;
long DAT_0040e518;
long DAT_0040e51b;
long DAT_0040e51d;
long DAT_0040e521;
long DAT_0040e525;
long DAT_0040eaab;
long DAT_0040ee55;
long DAT_0040ef59;
long DAT_0040ef5f;
long DAT_0040ef71;
long DAT_0040ef72;
long DAT_0040ef90;
long DAT_0040efa0;
long DAT_00611000;
long DAT_00611010;
long DAT_00611020;
long DAT_00611480;
long DAT_00611490;
long DAT_006114f0;
long DAT_006114f4;
long DAT_006114f8;
long DAT_006114fc;
long DAT_00611500;
long DAT_00611504;
long DAT_006116c0;
long DAT_006116d0;
long DAT_006116e0;
long DAT_006116e8;
long DAT_00611700;
long DAT_00611708;
long DAT_00611750;
long DAT_00611758;
long DAT_00611759;
long DAT_0061175a;
long DAT_00611760;
long DAT_00611768;
long DAT_0061176c;
long DAT_00611770;
long DAT_00611778;
long DAT_00611780;
long DAT_00611788;
long DAT_00611938;
long DAT_00611940;
long DAT_00611948;
long DAT_00611958;
long fde_0040f9f8;
long null_ARRAY_0040e340;
long null_ARRAY_0040f240;
long null_ARRAY_0040f4d0;
long null_ARRAY_006114a0;
long null_ARRAY_006114e0;
long null_ARRAY_00611720;
long null_ARRAY_006117c0;
long null_ARRAY_006118c0;
long null_ARRAY_00611900;
long PTR_DAT_00611488;
long PTR_null_ARRAY_006114d8;
long register0x00000020;
void
FUN_004028a0 (uint uParm1)
{
  int iVar1;
  long lVar2;
  undefined8 uVar3;
  undefined **ppuVar4;
  char *pcVar5;
  undefined *puVar6;
  undefined *local_88;
  char *local_80;
  undefined8 local_78;
  char *local_70;
  char *local_68;
  char *local_60;
  char *local_58;
  char *local_50;
  char *local_48;
  char *local_40;
  char *local_38;
  char *local_30;
  undefined8 local_28;
  undefined8 local_20;

  if (uParm1 == 0)
    goto LAB_004028cd;
  func_0x00401ba0 (DAT_006116e0, "Try \'%s --help\' for more information.\n",
		   DAT_00611788);
  do
    {
      func_0x00401d70 ((ulong) uParm1);
    LAB_004028cd:
      ;
      func_0x00401980
	("Usage: %s [OPTION] DURATION COMMAND [ARG]...\n  or:  %s [OPTION]\n",
	 DAT_00611788, DAT_00611788);
      uVar3 = DAT_006116c0;
      func_0x00401bc0
	("Start COMMAND, and kill it if still running after DURATION.\n",
	 DAT_006116c0);
      func_0x00401bc0
	("\nMandatory arguments to long options are mandatory for short options too.\n",
	 uVar3);
      func_0x00401bc0
	("      --preserve-status\n                 exit with the same status as COMMAND, even when the\n                   command times out\n      --foreground\n                 when not running timeout directly from a shell prompt,\n                   allow COMMAND to read from the TTY and get TTY signals;\n                   in this mode, children of COMMAND will not be timed out\n  -k, --kill-after=DURATION\n                 also send a KILL signal if COMMAND is still running\n                   this long after the initial signal was sent\n  -s, --signal=SIGNAL\n                 specify the signal to be sent on timeout;\n                   SIGNAL may be a name like \'HUP\' or a number;\n                   see \'kill -l\' for a list of signals\n",
	 uVar3);
      func_0x00401bc0
	("  -v, --verbose  diagnose to stderr any signal sent upon timeout\n",
	 uVar3);
      func_0x00401bc0 ("      --help     display this help and exit\n",
		       uVar3);
      func_0x00401bc0
	("      --version  output version information and exit\n", uVar3);
      func_0x00401bc0
	("\nDURATION is a floating point number with an optional suffix:\n\'s\' for seconds (the default), \'m\' for minutes, \'h\' for hours or \'d\' for days.\n",
	 uVar3);
      func_0x00401bc0
	("\nIf the command times out, and --preserve-status is not set, then exit with\nstatus 124.  Otherwise, exit with the status of COMMAND.  If no signal\nis specified, send the TERM signal upon timeout.  The TERM signal kills\nany process that does not block or catch that signal.  It may be necessary\nto use the KILL (9) signal, since this signal cannot be caught, in which\ncase the exit status is 128+9 rather than 124.\n",
	 uVar3);
      local_88 = &DAT_0040d9c5;
      local_80 = "test invocation";
      puVar6 = &DAT_0040d9c5;
      local_78 = 0x40da30;
      local_70 = "Multi-call invocation";
      local_68 = "sha224sum";
      local_60 = "sha2 utilities";
      local_58 = "sha256sum";
      local_50 = "sha2 utilities";
      local_48 = "sha384sum";
      local_40 = "sha2 utilities";
      local_38 = "sha512sum";
      local_30 = "sha2 utilities";
      local_28 = 0;
      local_20 = 0;
      ppuVar4 = &local_88;
      do
	{
	  iVar1 = func_0x00401cc0 ("timeout", puVar6);
	  if (iVar1 == 0)
	    break;
	  ppuVar4 = ppuVar4 + 2;
	  puVar6 = *ppuVar4;
	}
      while (puVar6 != (undefined *) 0x0);
      pcVar5 = ppuVar4[1];
      if (pcVar5 == (char *) 0x0)
	{
	  func_0x00401980 ("\n%s online help: <%s>\n", "GNU coreutils",
			   "https://www.gnu.org/software/coreutils/");
	  lVar2 = func_0x00401d30 (5, 0);
	  if (lVar2 != 0)
	    {
	      iVar1 = func_0x00401c20 (lVar2, &DAT_0040da51, 3);
	      if (iVar1 != 0)
		{
		  pcVar5 = "timeout";
		  goto LAB_00402aa9;
		}
	    }
	  func_0x00401980 ("Full documentation at: <%s%s>\n",
			   "https://www.gnu.org/software/coreutils/",
			   "timeout");
	LAB_00402ad2:
	  ;
	  pcVar5 = "timeout";
	  uVar3 = 0x40d9e9;
	}
      else
	{
	  func_0x00401980 ("\n%s online help: <%s>\n", "GNU coreutils",
			   "https://www.gnu.org/software/coreutils/");
	  lVar2 = func_0x00401d30 (5, 0);
	  if (lVar2 != 0)
	    {
	      iVar1 = func_0x00401c20 (lVar2, &DAT_0040da51, 3);
	      if (iVar1 != 0)
		{
		LAB_00402aa9:
		  ;
		  func_0x00401980
		    ("Report %s translation bugs to <https://translationproject.org/team/>\n",
		     "timeout");
		}
	    }
	  func_0x00401980 ("Full documentation at: <%s%s>\n",
			   "https://www.gnu.org/software/coreutils/",
			   "timeout");
	  uVar3 = 0x40ef8f;
	  if (pcVar5 == "timeout")
	    goto LAB_00402ad2;
	}
      func_0x00401980
	("or available locally via: info \'(coreutils) %s%s\'\n", pcVar5,
	 uVar3);
    }
  while (true);
}

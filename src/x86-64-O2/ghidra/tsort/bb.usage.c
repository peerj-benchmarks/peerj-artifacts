
#include "tsort.h"

long null_ARRAY_0061044_0_8_;
long null_ARRAY_0061044_8_8_;
long null_ARRAY_0061068_0_8_;
long null_ARRAY_0061068_16_8_;
long null_ARRAY_0061068_24_8_;
long null_ARRAY_0061068_32_8_;
long null_ARRAY_0061068_40_8_;
long null_ARRAY_0061068_48_8_;
long null_ARRAY_0061068_8_8_;
long null_ARRAY_006106c_0_4_;
long null_ARRAY_006106c_16_8_;
long null_ARRAY_006106c_4_4_;
long null_ARRAY_006106c_8_4_;
long local_2f_1_1_;
long local_4_4_4_;
long local_5b_1_1_;
long local_9d_0_4_;
long local_a0_0_8_;
long local_a0_4_6_;
long local_a0_8_2_;
long local_a1_4_4_;
long local_a2_4_4_;
long local_a3_0_4_;
long local_a3_4_4_;
long local_a4_0_2_;
long local_a4_0_4_;
long local_a4_0_8_;
long local_a4_1_9_;
long local_a4_4_6_;
long local_a4_8_2_;
long local_a5_0_4_;
long local_a5_4_4_;
long DAT_00000010;
long DAT_0040d2c0;
long DAT_0040d34a;
long DAT_0040d37c;
long DAT_0040d37e;
long DAT_0040d37f;
long DAT_0040d395;
long DAT_0040d682;
long DAT_0040d760;
long DAT_0040d764;
long DAT_0040d768;
long DAT_0040d76b;
long DAT_0040d76d;
long DAT_0040d771;
long DAT_0040d775;
long DAT_0040dd2b;
long DAT_0040e0d5;
long DAT_0040e1d9;
long DAT_0040e1df;
long DAT_0040e1f1;
long DAT_0040e1f2;
long DAT_0040e210;
long DAT_0040e214;
long DAT_0040e296;
long DAT_00610000;
long DAT_00610010;
long DAT_00610020;
long DAT_006103e8;
long DAT_00610450;
long DAT_00610454;
long DAT_00610458;
long DAT_0061045c;
long DAT_00610480;
long DAT_00610488;
long DAT_00610490;
long DAT_006104a0;
long DAT_006104a8;
long DAT_006104c0;
long DAT_006104c8;
long DAT_00610510;
long DAT_00610518;
long DAT_00610520;
long DAT_00610528;
long DAT_00610530;
long DAT_00610538;
long DAT_00610540;
long DAT_006106f8;
long DAT_00610700;
long DAT_00610708;
long DAT_00610718;
long _DYNAMIC;
long fde_0040ec70;
long null_ARRAY_0040d640;
long null_ARRAY_0040d6c0;
long null_ARRAY_0040e520;
long null_ARRAY_0040e750;
long null_ARRAY_00610400;
long null_ARRAY_00610440;
long null_ARRAY_006104e0;
long null_ARRAY_00610580;
long null_ARRAY_00610680;
long null_ARRAY_006106c0;
long PTR_DAT_006103e0;
long PTR_null_ARRAY_00610438;
long register0x00000020;
void
FUN_00402460 (uint uParm1)
{
  undefined8 uVar1;
  int iVar2;
  long lVar3;
  undefined *puVar4;
  undefined **ppuVar5;
  char *pcVar6;
  undefined *local_88;
  char *local_80;
  undefined8 local_78;
  char *local_70;
  char *local_68;
  char *local_60;
  char *local_58;
  char *local_50;
  char *local_48;
  char *local_40;
  char *local_38;
  char *local_30;
  undefined8 local_28;
  undefined8 local_20;

  if (uParm1 == 0)
    goto LAB_0040248d;
  func_0x00401710 (DAT_006104a0, "Try \'%s --help\' for more information.\n",
		   DAT_00610540);
  do
    {
      func_0x004018c0 ((ulong) uParm1);
    LAB_0040248d:
      ;
      func_0x00401580
	("Usage: %s [OPTION] [FILE]\nWrite totally ordered list consistent with the partial ordering in FILE.\n",
	 DAT_00610540);
      uVar1 = DAT_00610480;
      func_0x00401720
	("\nWith no FILE, or when FILE is -, read standard input.\n",
	 DAT_00610480);
      func_0x00401720 (&DAT_0040d37e, uVar1);
      func_0x00401720 ("      --help     display this help and exit\n",
		       uVar1);
      func_0x00401720
	("      --version  output version information and exit\n", uVar1);
      local_88 = &DAT_0040d2c0;
      local_80 = "test invocation";
      puVar4 = &DAT_0040d2c0;
      local_78 = 0x40d329;
      local_70 = "Multi-call invocation";
      local_68 = "sha224sum";
      local_60 = "sha2 utilities";
      local_58 = "sha256sum";
      local_50 = "sha2 utilities";
      local_48 = "sha384sum";
      local_40 = "sha2 utilities";
      local_38 = "sha512sum";
      local_30 = "sha2 utilities";
      local_28 = 0;
      local_20 = 0;
      ppuVar5 = &local_88;
      do
	{
	  iVar2 = func_0x00401810 ("tsort", puVar4);
	  if (iVar2 == 0)
	    break;
	  ppuVar5 = ppuVar5 + 2;
	  puVar4 = *ppuVar5;
	}
      while (puVar4 != (undefined *) 0x0);
      pcVar6 = ppuVar5[1];
      if (pcVar6 == (char *) 0x0)
	{
	  func_0x00401580 ("\n%s online help: <%s>\n", "GNU coreutils",
			   "https://www.gnu.org/software/coreutils/");
	  lVar3 = func_0x00401870 (5, 0);
	  if (lVar3 != 0)
	    {
	      iVar2 = func_0x00401780 (lVar3, &DAT_0040d34a, 3);
	      if (iVar2 != 0)
		{
		  pcVar6 = "tsort";
		  goto LAB_00402631;
		}
	    }
	  func_0x00401580 ("Full documentation at: <%s%s>\n",
			   "https://www.gnu.org/software/coreutils/",
			   "tsort");
	LAB_0040265a:
	  ;
	  pcVar6 = "tsort";
	  puVar4 = (undefined *) 0x40d2e2;
	}
      else
	{
	  func_0x00401580 ("\n%s online help: <%s>\n", "GNU coreutils",
			   "https://www.gnu.org/software/coreutils/");
	  lVar3 = func_0x00401870 (5, 0);
	  if (lVar3 != 0)
	    {
	      iVar2 = func_0x00401780 (lVar3, &DAT_0040d34a, 3);
	      if (iVar2 != 0)
		{
		LAB_00402631:
		  ;
		  func_0x00401580
		    ("Report %s translation bugs to <https://translationproject.org/team/>\n",
		     "tsort");
		}
	    }
	  func_0x00401580 ("Full documentation at: <%s%s>\n",
			   "https://www.gnu.org/software/coreutils/",
			   "tsort");
	  puVar4 = &DAT_0040d37f;
	  if (pcVar6 == "tsort")
	    goto LAB_0040265a;
	}
      func_0x00401580
	("or available locally via: info \'(coreutils) %s%s\'\n", pcVar6,
	 puVar4);
    }
  while (true);
}


#include "csplit.h"

long null_ARRAY_0062388_0_8_;
long null_ARRAY_0062388_8_8_;
long null_ARRAY_00623c0_0_8_;
long null_ARRAY_00623c0_16_8_;
long null_ARRAY_00623c0_24_8_;
long null_ARRAY_00623c0_32_8_;
long null_ARRAY_00623c0_40_8_;
long null_ARRAY_00623c0_48_8_;
long null_ARRAY_00623c0_8_8_;
long null_ARRAY_00623c4_0_4_;
long null_ARRAY_00623c4_16_8_;
long null_ARRAY_00623c4_4_4_;
long null_ARRAY_00623c4_8_4_;
long local_2f_1_1_;
long local_4_4_4_;
long local_5_1_1_;
long local_5b_1_1_;
long local_9_0_4_;
long local_9_4_4_;
long local_9d_0_4_;
long local_a0_0_8_;
long local_a_0_4_;
long local_a0_4_6_;
long local_a0_8_2_;
long local_a1_4_4_;
long local_a2_4_4_;
long local_a3_0_4_;
long local_a3_4_4_;
long local_a4_0_2_;
long local_a4_0_4_;
long local_a4_0_8_;
long local_a4_1_9_;
long local_a4_4_6_;
long local_a4_8_2_;
long local_a5_0_4_;
long local_a5_4_4_;
long DAT_00000010;
long DAT_0041d380;
long DAT_0041d398;
long DAT_0041d3ee;
long DAT_0041d402;
long DAT_0041d48d;
long DAT_0041d48f;
long DAT_0041d491;
long DAT_0041e0f8;
long DAT_0041e0fc;
long DAT_0041e100;
long DAT_0041e103;
long DAT_0041e105;
long DAT_0041e109;
long DAT_0041e10d;
long DAT_0041e6ab;
long DAT_0041edf8;
long DAT_0041ef01;
long DAT_0041ef07;
long DAT_0041ef19;
long DAT_0041ef1a;
long DAT_0041ef38;
long DAT_0041ef80;
long DAT_0041ef87;
long DAT_0041efc5;
long DAT_0041efde;
long DAT_0041fc44;
long DAT_00420040;
long DAT_00623378;
long DAT_00623388;
long DAT_00623398;
long DAT_00623820;
long DAT_00623830;
long DAT_00623890;
long DAT_00623894;
long DAT_00623898;
long DAT_0062389c;
long DAT_006238c0;
long DAT_006238d0;
long DAT_006238e0;
long DAT_006238e8;
long DAT_00623900;
long DAT_00623908;
long DAT_00623980;
long DAT_00623988;
long DAT_00623990;
long DAT_00623a40;
long DAT_00623a48;
long DAT_00623a50;
long DAT_00623a51;
long DAT_00623a52;
long DAT_00623a53;
long DAT_00623a58;
long DAT_00623a60;
long DAT_00623a68;
long DAT_00623a70;
long DAT_00623a78;
long DAT_00623a80;
long DAT_00623a88;
long DAT_00623a90;
long DAT_00623a98;
long DAT_00623aa0;
long DAT_00623aa8;
long DAT_00623ab0;
long DAT_00623ab8;
long DAT_00623ac0;
long DAT_00623ac8;
long DAT_00623ad0;
long DAT_00623ad8;
long DAT_00623c38;
long DAT_00623c78;
long DAT_00623c80;
long DAT_00623c88;
long DAT_00623c98;
long DAT_00623ca0;
long _DYNAMIC;
long fde_004209f8;
long null_ARRAY_0041dee0;
long null_ARRAY_0041df40;
long null_ARRAY_0041f9c0;
long null_ARRAY_0041fa00;
long null_ARRAY_0041fee0;
long null_ARRAY_00420180;
long null_ARRAY_00623840;
long null_ARRAY_00623880;
long null_ARRAY_00623920;
long null_ARRAY_006239c0;
long null_ARRAY_00623b00;
long null_ARRAY_00623c00;
long null_ARRAY_00623c40;
long PTR_DAT_00623828;
long PTR_null_ARRAY_00623878;
long register0x00000020;
long stack0x00000008;
void
FUN_00403d80 (uint uParm1)
{
  int iVar1;
  long lVar2;
  undefined8 uVar3;
  undefined **ppuVar4;
  char *pcVar5;
  undefined *puVar6;
  undefined *local_88;
  char *local_80;
  undefined8 local_78;
  char *local_70;
  char *local_68;
  char *local_60;
  char *local_58;
  char *local_50;
  char *local_48;
  char *local_40;
  char *local_38;
  char *local_30;
  undefined8 local_28;
  undefined8 local_20;

  if (uParm1 == 0)
    goto LAB_00403dad;
  func_0x00401e40 (DAT_006238e0, "Try \'%s --help\' for more information.\n",
		   DAT_00623ad8);
  do
    {
      func_0x004020a0 ((ulong) uParm1);
    LAB_00403dad:
      ;
      func_0x00401c10 ("Usage: %s [OPTION]... FILE PATTERN...\n",
		       DAT_00623ad8);
      uVar3 = DAT_006238c0;
      func_0x00401e50
	("Output pieces of FILE separated by PATTERN(s) to files \'xx00\', \'xx01\', ...,\nand output byte counts of each piece to standard output.\n",
	 DAT_006238c0);
      func_0x00401e50 ("\nRead standard input if FILE is -\n", uVar3);
      func_0x00401e50
	("\nMandatory arguments to long options are mandatory for short options too.\n",
	 uVar3);
      func_0x00401e50
	("  -b, --suffix-format=FORMAT  use sprintf FORMAT instead of %02d\n  -f, --prefix=PREFIX        use PREFIX instead of \'xx\'\n  -k, --keep-files           do not remove output files on errors\n",
	 uVar3);
      func_0x00401e50
	("      --suppress-matched     suppress the lines matching PATTERN\n",
	 uVar3);
      func_0x00401e50
	("  -n, --digits=DIGITS        use specified number of digits instead of 2\n  -s, --quiet, --silent      do not print counts of output file sizes\n  -z, --elide-empty-files    remove empty output files\n",
	 uVar3);
      func_0x00401e50 ("      --help     display this help and exit\n",
		       uVar3);
      func_0x00401e50
	("      --version  output version information and exit\n", uVar3);
      func_0x00401e50
	("\nEach PATTERN may be:\n  INTEGER            copy up to but not including specified line number\n  /REGEXP/[OFFSET]   copy up to but not including a matching line\n  %REGEXP%[OFFSET]   skip to, but not including a matching line\n  {INTEGER}          repeat the previous pattern specified number of times\n  {*}                repeat the previous pattern as many times as possible\n\nA line OFFSET is a required \'+\' or \'-\' followed by a positive integer.\n",
	 uVar3);
      local_88 = &DAT_0041d402;
      local_80 = "test invocation";
      puVar6 = &DAT_0041d402;
      local_78 = 0x41d46c;
      local_70 = "Multi-call invocation";
      local_68 = "sha224sum";
      local_60 = "sha2 utilities";
      local_58 = "sha256sum";
      local_50 = "sha2 utilities";
      local_48 = "sha384sum";
      local_40 = "sha2 utilities";
      local_38 = "sha512sum";
      local_30 = "sha2 utilities";
      local_28 = 0;
      local_20 = 0;
      ppuVar4 = &local_88;
      do
	{
	  iVar1 = func_0x00401fd0 ("csplit", puVar6);
	  if (iVar1 == 0)
	    break;
	  ppuVar4 = ppuVar4 + 2;
	  puVar6 = *ppuVar4;
	}
      while (puVar6 != (undefined *) 0x0);
      pcVar5 = ppuVar4[1];
      if (pcVar5 == (char *) 0x0)
	{
	  func_0x00401c10 ("\n%s online help: <%s>\n", "GNU coreutils",
			   "https://www.gnu.org/software/coreutils/");
	  lVar2 = func_0x00402030 (5, 0);
	  if (lVar2 != 0)
	    {
	      iVar1 = func_0x00401ee0 (lVar2, &DAT_0041d48d, 3);
	      if (iVar1 != 0)
		{
		  pcVar5 = "csplit";
		  goto LAB_00403f91;
		}
	    }
	  func_0x00401c10 ("Full documentation at: <%s%s>\n",
			   "https://www.gnu.org/software/coreutils/",
			   "csplit");
	LAB_00403fba:
	  ;
	  pcVar5 = "csplit";
	  uVar3 = 0x41d425;
	}
      else
	{
	  func_0x00401c10 ("\n%s online help: <%s>\n", "GNU coreutils",
			   "https://www.gnu.org/software/coreutils/");
	  lVar2 = func_0x00402030 (5, 0);
	  if (lVar2 != 0)
	    {
	      iVar1 = func_0x00401ee0 (lVar2, &DAT_0041d48d, 3);
	      if (iVar1 != 0)
		{
		LAB_00403f91:
		  ;
		  func_0x00401c10
		    ("Report %s translation bugs to <https://translationproject.org/team/>\n",
		     "csplit");
		}
	    }
	  func_0x00401c10 ("Full documentation at: <%s%s>\n",
			   "https://www.gnu.org/software/coreutils/",
			   "csplit");
	  uVar3 = 0x41ef37;
	  if (pcVar5 == "csplit")
	    goto LAB_00403fba;
	}
      func_0x00401c10
	("or available locally via: info \'(coreutils) %s%s\'\n", pcVar5,
	 uVar3);
    }
  while (true);
}


#include "users.h"

long null_ARRAY_0060f44_0_8_;
long null_ARRAY_0060f44_8_8_;
long null_ARRAY_0060f64_0_8_;
long null_ARRAY_0060f64_16_8_;
long null_ARRAY_0060f64_24_8_;
long null_ARRAY_0060f64_32_8_;
long null_ARRAY_0060f64_40_8_;
long null_ARRAY_0060f64_48_8_;
long null_ARRAY_0060f64_8_8_;
long null_ARRAY_0060f68_0_4_;
long null_ARRAY_0060f68_16_8_;
long null_ARRAY_0060f68_4_4_;
long null_ARRAY_0060f68_8_4_;
long local_2f_1_1_;
long local_4_4_4_;
long local_5b_1_1_;
long local_9d_0_4_;
long local_a0_0_8_;
long local_a0_4_6_;
long local_a0_8_2_;
long local_a1_4_4_;
long local_a2_4_4_;
long local_a3_0_4_;
long local_a3_4_4_;
long local_a4_0_2_;
long local_a4_0_4_;
long local_a4_0_8_;
long local_a4_1_9_;
long local_a4_4_6_;
long local_a4_8_2_;
long local_a5_0_4_;
long local_a5_4_4_;
long DAT_00000010;
long DAT_0040ca80;
long DAT_0040cb46;
long DAT_0040cd98;
long DAT_0040ce60;
long DAT_0040ce64;
long DAT_0040ce68;
long DAT_0040ce6b;
long DAT_0040ce6d;
long DAT_0040ce71;
long DAT_0040ce75;
long DAT_0040d42b;
long DAT_0040d7d5;
long DAT_0040d8d9;
long DAT_0040d8df;
long DAT_0040d8f1;
long DAT_0040d8f2;
long DAT_0040d910;
long DAT_0040d914;
long DAT_0040d996;
long DAT_0060f000;
long DAT_0060f010;
long DAT_0060f020;
long DAT_0060f3e8;
long DAT_0060f450;
long DAT_0060f454;
long DAT_0060f458;
long DAT_0060f45c;
long DAT_0060f480;
long DAT_0060f490;
long DAT_0060f4a0;
long DAT_0060f4a8;
long DAT_0060f4c0;
long DAT_0060f4c8;
long DAT_0060f510;
long DAT_0060f518;
long DAT_0060f520;
long DAT_0060f6b8;
long DAT_0060f6c0;
long DAT_0060f6c8;
long DAT_0060f6d8;
long fde_0040e330;
long null_ARRAY_0040cd60;
long null_ARRAY_0040cdc0;
long null_ARRAY_0040dc20;
long null_ARRAY_0040de50;
long null_ARRAY_0060f400;
long null_ARRAY_0060f440;
long null_ARRAY_0060f4e0;
long null_ARRAY_0060f540;
long null_ARRAY_0060f640;
long null_ARRAY_0060f680;
long PTR_DAT_0060f3e0;
long PTR_null_ARRAY_0060f438;
long register0x00000020;
void
FUN_00401de0 (uint uParm1)
{
  int iVar1;
  long lVar2;
  undefined8 uVar3;
  undefined **ppuVar4;
  char *pcVar5;
  undefined *puVar6;
  undefined *local_88;
  char *local_80;
  undefined8 local_78;
  char *local_70;
  char *local_68;
  char *local_60;
  char *local_58;
  char *local_50;
  char *local_48;
  char *local_40;
  char *local_38;
  char *local_30;
  undefined8 local_28;
  undefined8 local_20;

  if (uParm1 == 0)
    goto LAB_00401e0d;
  func_0x00401710 (DAT_0060f4a0, "Try \'%s --help\' for more information.\n",
		   DAT_0060f520);
  do
    {
      func_0x004018e0 ((ulong) uParm1);
    LAB_00401e0d:
      ;
      func_0x00401590 ("Usage: %s [OPTION]... [FILE]\n", DAT_0060f520);
      func_0x00401590
	("Output who is currently logged in according to FILE.\nIf FILE is not specified, use %s.  %s as FILE is common.\n\n",
	 "/dev/null/utmp", "/dev/null/wtmp");
      uVar3 = DAT_0060f480;
      func_0x00401730 ("      --help     display this help and exit\n",
		       DAT_0060f480);
      func_0x00401730
	("      --version  output version information and exit\n", uVar3);
      local_88 = &DAT_0040ca80;
      local_80 = "test invocation";
      puVar6 = &DAT_0040ca80;
      local_78 = 0x40cb25;
      local_70 = "Multi-call invocation";
      local_68 = "sha224sum";
      local_60 = "sha2 utilities";
      local_58 = "sha256sum";
      local_50 = "sha2 utilities";
      local_48 = "sha384sum";
      local_40 = "sha2 utilities";
      local_38 = "sha512sum";
      local_30 = "sha2 utilities";
      local_28 = 0;
      local_20 = 0;
      ppuVar4 = &local_88;
      do
	{
	  iVar1 = func_0x00401830 ("users", puVar6);
	  if (iVar1 == 0)
	    break;
	  ppuVar4 = ppuVar4 + 2;
	  puVar6 = *ppuVar4;
	}
      while (puVar6 != (undefined *) 0x0);
      pcVar5 = ppuVar4[1];
      if (pcVar5 == (char *) 0x0)
	{
	  func_0x00401590 ("\n%s online help: <%s>\n", "GNU coreutils",
			   "https://www.gnu.org/software/coreutils/");
	  lVar2 = func_0x00401890 (5, 0);
	  if (lVar2 != 0)
	    {
	      iVar1 = func_0x00401780 (lVar2, &DAT_0040cb46, 3);
	      if (iVar1 != 0)
		{
		  pcVar5 = "users";
		  goto LAB_00401fa9;
		}
	    }
	  func_0x00401590 ("Full documentation at: <%s%s>\n",
			   "https://www.gnu.org/software/coreutils/",
			   "users");
	LAB_00401fd2:
	  ;
	  pcVar5 = "users";
	  uVar3 = 0x40cade;
	}
      else
	{
	  func_0x00401590 ("\n%s online help: <%s>\n", "GNU coreutils",
			   "https://www.gnu.org/software/coreutils/");
	  lVar2 = func_0x00401890 (5, 0);
	  if (lVar2 != 0)
	    {
	      iVar1 = func_0x00401780 (lVar2, &DAT_0040cb46, 3);
	      if (iVar1 != 0)
		{
		LAB_00401fa9:
		  ;
		  func_0x00401590
		    ("Report %s translation bugs to <https://translationproject.org/team/>\n",
		     "users");
		}
	    }
	  func_0x00401590 ("Full documentation at: <%s%s>\n",
			   "https://www.gnu.org/software/coreutils/",
			   "users");
	  uVar3 = 0x40d90f;
	  if (pcVar5 == "users")
	    goto LAB_00401fd2;
	}
      func_0x00401590
	("or available locally via: info \'(coreutils) %s%s\'\n", pcVar5,
	 uVar3);
    }
  while (true);
}


#include "fold.h"

long null_ARRAY_0061046_0_8_;
long null_ARRAY_0061046_8_8_;
long null_ARRAY_0061064_0_8_;
long null_ARRAY_0061064_16_8_;
long null_ARRAY_0061064_24_8_;
long null_ARRAY_0061064_32_8_;
long null_ARRAY_0061064_40_8_;
long null_ARRAY_0061064_48_8_;
long null_ARRAY_0061064_8_8_;
long null_ARRAY_0061068_0_4_;
long null_ARRAY_0061068_16_8_;
long null_ARRAY_0061068_4_4_;
long null_ARRAY_0061068_8_4_;
long local_2f_1_1_;
long local_4_4_4_;
long local_5b_1_1_;
long local_9d_0_4_;
long local_a0_0_8_;
long local_a0_4_6_;
long local_a0_8_2_;
long local_a1_4_4_;
long local_a2_4_4_;
long local_a3_0_4_;
long local_a3_4_4_;
long local_a4_0_2_;
long local_a4_0_4_;
long local_a4_0_8_;
long local_a4_1_9_;
long local_a4_4_6_;
long local_a4_8_2_;
long local_a5_0_4_;
long local_a5_4_4_;
long DAT_00000010;
long DAT_0040d1c0;
long DAT_0040d1c2;
long DAT_0040d249;
long DAT_0040d6c0;
long DAT_0040d6c4;
long DAT_0040d6c8;
long DAT_0040d6cb;
long DAT_0040d6cd;
long DAT_0040d6d1;
long DAT_0040d6d5;
long DAT_0040dc6b;
long DAT_0040e208;
long DAT_0040e311;
long DAT_0040e317;
long DAT_0040e329;
long DAT_0040e32a;
long DAT_0040e348;
long DAT_0040e34c;
long DAT_0040e3ce;
long DAT_00610000;
long DAT_00610010;
long DAT_00610020;
long DAT_00610408;
long DAT_00610470;
long DAT_00610474;
long DAT_00610478;
long DAT_0061047c;
long DAT_00610480;
long DAT_00610488;
long DAT_00610490;
long DAT_006104a0;
long DAT_006104a8;
long DAT_006104c0;
long DAT_006104c8;
long DAT_00610510;
long DAT_00610518;
long DAT_00610520;
long DAT_00610521;
long DAT_00610522;
long DAT_00610528;
long DAT_00610530;
long DAT_00610538;
long DAT_006106b8;
long DAT_006106c0;
long DAT_006106c8;
long DAT_006106d8;
long _DYNAMIC;
long fde_0040ed88;
long null_ARRAY_0040d580;
long null_ARRAY_0040d640;
long null_ARRAY_0040e660;
long null_ARRAY_0040e890;
long null_ARRAY_00610420;
long null_ARRAY_00610460;
long null_ARRAY_006104e0;
long null_ARRAY_00610540;
long null_ARRAY_00610640;
long null_ARRAY_00610680;
long PTR_DAT_00610400;
long PTR_null_ARRAY_00610458;
long register0x00000020;
void
FUN_004021e0 (uint uParm1)
{
  int iVar1;
  long lVar2;
  undefined8 uVar3;
  undefined **ppuVar4;
  undefined *puVar5;
  undefined *local_88;
  char *local_80;
  undefined8 local_78;
  char *local_70;
  char *local_68;
  char *local_60;
  char *local_58;
  char *local_50;
  char *local_48;
  char *local_40;
  char *local_38;
  char *local_30;
  undefined8 local_28;
  undefined8 local_20;

  if (uParm1 == 0)
    goto LAB_0040220d;
  func_0x004017f0 (DAT_006104a0, "Try \'%s --help\' for more information.\n",
		   DAT_00610538);
  do
    {
      func_0x004019c0 ((ulong) uParm1);
    LAB_0040220d:
      ;
      func_0x00401650 ("Usage: %s [OPTION]... [FILE]...\n", DAT_00610538);
      uVar3 = DAT_00610480;
      func_0x00401800
	("Wrap input lines in each FILE, writing to standard output.\n",
	 DAT_00610480);
      func_0x00401800
	("\nWith no FILE, or when FILE is -, read standard input.\n", uVar3);
      func_0x00401800
	("\nMandatory arguments to long options are mandatory for short options too.\n",
	 uVar3);
      func_0x00401800
	("  -b, --bytes         count bytes rather than columns\n  -s, --spaces        break at spaces\n  -w, --width=WIDTH   use WIDTH columns instead of 80\n",
	 uVar3);
      func_0x00401800 ("      --help     display this help and exit\n",
		       uVar3);
      func_0x00401800
	("      --version  output version information and exit\n", uVar3);
      local_88 = &DAT_0040d1c0;
      local_80 = "test invocation";
      puVar5 = &DAT_0040d1c0;
      local_78 = 0x40d228;
      local_70 = "Multi-call invocation";
      local_68 = "sha224sum";
      local_60 = "sha2 utilities";
      local_58 = "sha256sum";
      local_50 = "sha2 utilities";
      local_48 = "sha384sum";
      local_40 = "sha2 utilities";
      local_38 = "sha512sum";
      local_30 = "sha2 utilities";
      local_28 = 0;
      local_20 = 0;
      ppuVar4 = &local_88;
      do
	{
	  iVar1 = func_0x00401900 (&DAT_0040d1c2, puVar5);
	  if (iVar1 == 0)
	    break;
	  ppuVar4 = ppuVar4 + 2;
	  puVar5 = *ppuVar4;
	}
      while (puVar5 != (undefined *) 0x0);
      puVar5 = ppuVar4[1];
      if (puVar5 == (undefined *) 0x0)
	{
	  func_0x00401650 ("\n%s online help: <%s>\n", "GNU coreutils",
			   "https://www.gnu.org/software/coreutils/");
	  lVar2 = func_0x00401960 (5, 0);
	  if (lVar2 != 0)
	    {
	      iVar1 = func_0x00401860 (lVar2, &DAT_0040d249, 3);
	      if (iVar1 != 0)
		{
		  puVar5 = &DAT_0040d1c2;
		  goto LAB_004023c9;
		}
	    }
	  func_0x00401650 ("Full documentation at: <%s%s>\n",
			   "https://www.gnu.org/software/coreutils/",
			   &DAT_0040d1c2);
	LAB_004023f2:
	  ;
	  puVar5 = &DAT_0040d1c2;
	  uVar3 = 0x40d1e1;
	}
      else
	{
	  func_0x00401650 ("\n%s online help: <%s>\n", "GNU coreutils",
			   "https://www.gnu.org/software/coreutils/");
	  lVar2 = func_0x00401960 (5, 0);
	  if (lVar2 != 0)
	    {
	      iVar1 = func_0x00401860 (lVar2, &DAT_0040d249, 3);
	      if (iVar1 != 0)
		{
		LAB_004023c9:
		  ;
		  func_0x00401650
		    ("Report %s translation bugs to <https://translationproject.org/team/>\n",
		     &DAT_0040d1c2);
		}
	    }
	  func_0x00401650 ("Full documentation at: <%s%s>\n",
			   "https://www.gnu.org/software/coreutils/",
			   &DAT_0040d1c2);
	  uVar3 = 0x40e347;
	  if (puVar5 == &DAT_0040d1c2)
	    goto LAB_004023f2;
	}
      func_0x00401650
	("or available locally via: info \'(coreutils) %s%s\'\n", puVar5,
	 uVar3);
    }
  while (true);
}

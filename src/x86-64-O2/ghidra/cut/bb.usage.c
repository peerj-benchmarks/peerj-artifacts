
#include "cut.h"

long DAT_0061171_0_1_;
long DAT_0061171_1_1_;
long null_ARRAY_0061166_0_8_;
long null_ARRAY_0061166_8_8_;
long null_ARRAY_0061188_0_8_;
long null_ARRAY_0061188_16_8_;
long null_ARRAY_0061188_24_8_;
long null_ARRAY_0061188_32_8_;
long null_ARRAY_0061188_40_8_;
long null_ARRAY_0061188_48_8_;
long null_ARRAY_0061188_8_8_;
long null_ARRAY_006118c_0_4_;
long null_ARRAY_006118c_16_8_;
long null_ARRAY_006118c_4_4_;
long null_ARRAY_006118c_8_4_;
long local_2f_1_1_;
long local_4_4_4_;
long local_5b_1_1_;
long local_9d_0_4_;
long local_a0_0_8_;
long local_a0_4_6_;
long local_a0_8_2_;
long local_a1_4_4_;
long local_a2_4_4_;
long local_a3_0_4_;
long local_a3_4_4_;
long local_a4_0_2_;
long local_a4_0_4_;
long local_a4_0_8_;
long local_a4_1_9_;
long local_a4_4_6_;
long local_a4_8_2_;
long local_a5_0_4_;
long local_a5_4_4_;
long DAT_00000010;
long DAT_0040e117;
long DAT_0040e119;
long DAT_0040e19f;
long DAT_0040ee48;
long DAT_0040ee4c;
long DAT_0040ee50;
long DAT_0040ee53;
long DAT_0040ee55;
long DAT_0040ee59;
long DAT_0040ee5d;
long DAT_0040f3eb;
long DAT_0040f795;
long DAT_0040f899;
long DAT_0040f89f;
long DAT_0040f8b1;
long DAT_0040f8b2;
long DAT_0040f8d0;
long DAT_0040f8d4;
long DAT_0040f956;
long DAT_006111b8;
long DAT_006111c8;
long DAT_006111d8;
long DAT_00611600;
long DAT_00611610;
long DAT_00611670;
long DAT_00611674;
long DAT_00611678;
long DAT_0061167c;
long DAT_00611680;
long DAT_00611688;
long DAT_00611690;
long DAT_006116a0;
long DAT_006116a8;
long DAT_006116c0;
long DAT_006116c8;
long DAT_00611710;
long DAT_00611712;
long DAT_00611718;
long DAT_00611720;
long DAT_00611728;
long DAT_00611729;
long DAT_0061172a;
long DAT_0061172b;
long DAT_0061172c;
long DAT_00611730;
long DAT_00611738;
long DAT_00611740;
long DAT_00611748;
long DAT_00611750;
long DAT_00611758;
long DAT_00611760;
long DAT_00611768;
long DAT_006118f8;
long DAT_00611900;
long DAT_00611908;
long DAT_00611910;
long DAT_00611918;
long DAT_00611928;
long fde_00410330;
long null_ARRAY_0040eb00;
long null_ARRAY_0040fbe0;
long null_ARRAY_0040fe10;
long null_ARRAY_00611620;
long null_ARRAY_00611660;
long null_ARRAY_006116e0;
long null_ARRAY_00611780;
long null_ARRAY_00611880;
long null_ARRAY_006118c0;
long PTR_DAT_00611608;
long PTR_null_ARRAY_00611658;
long register0x00000020;
void
FUN_00402910 (uint uParm1)
{
  int iVar1;
  long lVar2;
  undefined8 uVar3;
  undefined **ppuVar4;
  undefined *puVar5;
  undefined *local_88;
  char *local_80;
  undefined8 local_78;
  char *local_70;
  char *local_68;
  char *local_60;
  char *local_58;
  char *local_50;
  char *local_48;
  char *local_40;
  char *local_38;
  char *local_30;
  undefined8 local_28;
  undefined8 local_20;

  if (uParm1 == 0)
    goto LAB_0040293d;
  func_0x00401a80 (DAT_006116a0, "Try \'%s --help\' for more information.\n",
		   DAT_00611768);
  do
    {
      func_0x00401ca0 ((ulong) uParm1);
    LAB_0040293d:
      ;
      func_0x004018c0 ("Usage: %s OPTION... [FILE]...\n", DAT_00611768);
      uVar3 = DAT_00611680;
      func_0x00401a90
	("Print selected parts of lines from each FILE to standard output.\n",
	 DAT_00611680);
      func_0x00401a90
	("\nWith no FILE, or when FILE is -, read standard input.\n", uVar3);
      func_0x00401a90
	("\nMandatory arguments to long options are mandatory for short options too.\n",
	 uVar3);
      func_0x00401a90
	("  -b, --bytes=LIST        select only these bytes\n  -c, --characters=LIST   select only these characters\n  -d, --delimiter=DELIM   use DELIM instead of TAB for field delimiter\n",
	 uVar3);
      func_0x00401a90
	("  -f, --fields=LIST       select only these fields;  also print any line\n                            that contains no delimiter character, unless\n                            the -s option is specified\n  -n                      (ignored)\n",
	 uVar3);
      func_0x00401a90
	("      --complement        complement the set of selected bytes, characters\n                            or fields\n",
	 uVar3);
      func_0x00401a90
	("  -s, --only-delimited    do not print lines not containing delimiters\n      --output-delimiter=STRING  use STRING as the output delimiter\n                            the default is to use the input delimiter\n",
	 uVar3);
      func_0x00401a90
	("  -z, --zero-terminated    line delimiter is NUL, not newline\n",
	 uVar3);
      func_0x00401a90 ("      --help     display this help and exit\n",
		       uVar3);
      func_0x00401a90
	("      --version  output version information and exit\n", uVar3);
      func_0x00401a90
	("\nUse one, and only one of -b, -c or -f.  Each LIST is made up of one\nrange, or many ranges separated by commas.  Selected input is written\nin the same order that it is read, and is written exactly once.\n",
	 uVar3);
      func_0x00401a90
	("Each range is one of:\n\n  N     N\'th byte, character or field, counted from 1\n  N-    from N\'th byte, character or field, to end of line\n  N-M   from N\'th to M\'th (included) byte, character or field\n  -M    from first to M\'th (included) byte, character or field\n",
	 uVar3);
      local_88 = &DAT_0040e117;
      local_80 = "test invocation";
      puVar5 = &DAT_0040e117;
      local_78 = 0x40e17e;
      local_70 = "Multi-call invocation";
      local_68 = "sha224sum";
      local_60 = "sha2 utilities";
      local_58 = "sha256sum";
      local_50 = "sha2 utilities";
      local_48 = "sha384sum";
      local_40 = "sha2 utilities";
      local_38 = "sha512sum";
      local_30 = "sha2 utilities";
      local_28 = 0;
      local_20 = 0;
      ppuVar4 = &local_88;
      do
	{
	  iVar1 = func_0x00401bb0 (&DAT_0040e119, puVar5);
	  if (iVar1 == 0)
	    break;
	  ppuVar4 = ppuVar4 + 2;
	  puVar5 = *ppuVar4;
	}
      while (puVar5 != (undefined *) 0x0);
      puVar5 = ppuVar4[1];
      if (puVar5 == (undefined *) 0x0)
	{
	  func_0x004018c0 ("\n%s online help: <%s>\n", "GNU coreutils",
			   "https://www.gnu.org/software/coreutils/");
	  lVar2 = func_0x00401c20 (5, 0);
	  if (lVar2 != 0)
	    {
	      iVar1 = func_0x00401b00 (lVar2, &DAT_0040e19f, 3);
	      if (iVar1 != 0)
		{
		  puVar5 = &DAT_0040e119;
		  goto LAB_00402b49;
		}
	    }
	  func_0x004018c0 ("Full documentation at: <%s%s>\n",
			   "https://www.gnu.org/software/coreutils/",
			   &DAT_0040e119);
	LAB_00402b72:
	  ;
	  puVar5 = &DAT_0040e119;
	  uVar3 = 0x40e137;
	}
      else
	{
	  func_0x004018c0 ("\n%s online help: <%s>\n", "GNU coreutils",
			   "https://www.gnu.org/software/coreutils/");
	  lVar2 = func_0x00401c20 (5, 0);
	  if (lVar2 != 0)
	    {
	      iVar1 = func_0x00401b00 (lVar2, &DAT_0040e19f, 3);
	      if (iVar1 != 0)
		{
		LAB_00402b49:
		  ;
		  func_0x004018c0
		    ("Report %s translation bugs to <https://translationproject.org/team/>\n",
		     &DAT_0040e119);
		}
	    }
	  func_0x004018c0 ("Full documentation at: <%s%s>\n",
			   "https://www.gnu.org/software/coreutils/",
			   &DAT_0040e119);
	  uVar3 = 0x40f8cf;
	  if (puVar5 == &DAT_0040e119)
	    goto LAB_00402b72;
	}
      func_0x004018c0
	("or available locally via: info \'(coreutils) %s%s\'\n", puVar5,
	 uVar3);
    }
  while (true);
}

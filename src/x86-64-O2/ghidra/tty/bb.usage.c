
#include "tty.h"

long null_ARRAY_0060ec4_0_8_;
long null_ARRAY_0060ec4_8_8_;
long null_ARRAY_0060ee4_0_8_;
long null_ARRAY_0060ee4_16_8_;
long null_ARRAY_0060ee4_24_8_;
long null_ARRAY_0060ee4_32_8_;
long null_ARRAY_0060ee4_40_8_;
long null_ARRAY_0060ee4_48_8_;
long null_ARRAY_0060ee4_8_8_;
long null_ARRAY_0060ee8_0_4_;
long null_ARRAY_0060ee8_16_8_;
long null_ARRAY_0060ee8_4_4_;
long null_ARRAY_0060ee8_8_4_;
long local_2f_1_1_;
long local_5b_1_1_;
long local_9d_0_4_;
long local_a0_0_8_;
long local_a0_4_6_;
long local_a0_8_2_;
long local_a1_4_4_;
long local_a2_4_4_;
long local_a3_0_4_;
long local_a3_4_4_;
long local_a4_0_2_;
long local_a4_0_4_;
long local_a4_0_8_;
long local_a4_1_9_;
long local_a4_4_6_;
long local_a4_8_2_;
long local_a5_0_4_;
long local_a5_4_4_;
long DAT_00000010;
long DAT_0040c480;
long DAT_0040c51b;
long DAT_0040c878;
long DAT_0040c87c;
long DAT_0040c880;
long DAT_0040c883;
long DAT_0040c885;
long DAT_0040c889;
long DAT_0040c88d;
long DAT_0040ce2b;
long DAT_0040d1d5;
long DAT_0040d2d9;
long DAT_0040d2df;
long DAT_0040d2f1;
long DAT_0040d2f2;
long DAT_0040d310;
long DAT_0040d314;
long DAT_0040d396;
long DAT_0060e820;
long DAT_0060e830;
long DAT_0060e840;
long DAT_0060ebe8;
long DAT_0060ec50;
long DAT_0060ec54;
long DAT_0060ec58;
long DAT_0060ec5c;
long DAT_0060ec80;
long DAT_0060ec90;
long DAT_0060eca0;
long DAT_0060eca8;
long DAT_0060ecc0;
long DAT_0060ecc8;
long DAT_0060ed10;
long DAT_0060ed18;
long DAT_0060ed20;
long DAT_0060ed28;
long DAT_0060eeb8;
long DAT_0060eec0;
long DAT_0060eec8;
long DAT_0060eed8;
long fde_0040dd00;
long null_ARRAY_0040c780;
long null_ARRAY_0040d620;
long null_ARRAY_0040d850;
long null_ARRAY_0060ec00;
long null_ARRAY_0060ec40;
long null_ARRAY_0060ece0;
long null_ARRAY_0060ed40;
long null_ARRAY_0060ee40;
long null_ARRAY_0060ee80;
long PTR_DAT_0060ebe0;
long PTR_null_ARRAY_0060ec38;
long register0x00000020;
void
FUN_00401ba0 (uint uParm1)
{
  int iVar1;
  long lVar2;
  undefined8 uVar3;
  undefined **ppuVar4;
  long lVar5;
  undefined *puVar6;
  undefined *local_88;
  char *local_80;
  undefined8 local_78;
  char *local_70;
  char *local_68;
  char *local_60;
  char *local_58;
  char *local_50;
  char *local_48;
  char *local_40;
  char *local_38;
  char *local_30;
  undefined8 local_28;
  undefined8 local_20;

  if (uParm1 == 0)
    goto LAB_00401bcd;
  func_0x00401650 (DAT_0060eca0, "Try \'%s --help\' for more information.\n",
		   DAT_0060ed28);
  do
    {
      func_0x004017e0 ((ulong) uParm1);
    LAB_00401bcd:
      ;
      func_0x004014c0 ("Usage: %s [OPTION]...\n", DAT_0060ed28);
      uVar3 = DAT_0060ec80;
      func_0x00401660
	("Print the file name of the terminal connected to standard input.\n\n  -s, --silent, --quiet   print nothing, only return an exit status\n",
	 DAT_0060ec80);
      func_0x00401660 ("      --help     display this help and exit\n",
		       uVar3);
      func_0x00401660
	("      --version  output version information and exit\n", uVar3);
      local_88 = &DAT_0040c480;
      local_80 = "test invocation";
      puVar6 = &DAT_0040c480;
      local_78 = 0x40c4fa;
      local_70 = "Multi-call invocation";
      local_68 = "sha224sum";
      local_60 = "sha2 utilities";
      local_58 = "sha256sum";
      local_50 = "sha2 utilities";
      local_48 = "sha384sum";
      local_40 = "sha2 utilities";
      local_38 = "sha512sum";
      local_30 = "sha2 utilities";
      local_28 = 0;
      local_20 = 0;
      ppuVar4 = &local_88;
      do
	{
	  iVar1 = func_0x00401730 (0x40c525, puVar6);
	  if (iVar1 == 0)
	    break;
	  ppuVar4 = ppuVar4 + 2;
	  puVar6 = *ppuVar4;
	}
      while (puVar6 != (undefined *) 0x0);
      lVar5 = (long) ppuVar4[1];
      if (lVar5 == 0)
	{
	  func_0x004014c0 ("\n%s online help: <%s>\n", "GNU coreutils",
			   "https://www.gnu.org/software/coreutils/");
	  lVar5 = func_0x004017a0 (5, 0);
	  if (lVar5 != 0)
	    {
	      iVar1 = func_0x004016b0 (lVar5, &DAT_0040c51b, 3);
	      if (iVar1 != 0)
		{
		  lVar5 = 0x40c525;
		  goto LAB_00401d61;
		}
	    }
	  func_0x004014c0 ("Full documentation at: <%s%s>\n",
			   "https://www.gnu.org/software/coreutils/",
			   0x40c525);
	LAB_00401d8a:
	  ;
	  lVar5 = 0x40c525;
	  uVar3 = 0x40c4b3;
	}
      else
	{
	  func_0x004014c0 ("\n%s online help: <%s>\n", "GNU coreutils",
			   "https://www.gnu.org/software/coreutils/");
	  lVar2 = func_0x004017a0 (5, 0);
	  if (lVar2 != 0)
	    {
	      iVar1 = func_0x004016b0 (lVar2, &DAT_0040c51b, 3);
	      if (iVar1 != 0)
		{
		LAB_00401d61:
		  ;
		  func_0x004014c0
		    ("Report %s translation bugs to <https://translationproject.org/team/>\n",
		     0x40c525);
		}
	    }
	  func_0x004014c0 ("Full documentation at: <%s%s>\n",
			   "https://www.gnu.org/software/coreutils/",
			   0x40c525);
	  uVar3 = 0x40d30f;
	  if (lVar5 == 0x40c525)
	    goto LAB_00401d8a;
	}
      func_0x004014c0
	("or available locally via: info \'(coreutils) %s%s\'\n", lVar5,
	 uVar3);
    }
  while (true);
}

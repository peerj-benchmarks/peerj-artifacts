
#include "nohup.h"

long null_ARRAY_0060fa2_0_8_;
long null_ARRAY_0060fa2_8_8_;
long null_ARRAY_0060fc0_0_8_;
long null_ARRAY_0060fc0_16_8_;
long null_ARRAY_0060fc0_24_8_;
long null_ARRAY_0060fc0_32_8_;
long null_ARRAY_0060fc0_40_8_;
long null_ARRAY_0060fc0_48_8_;
long null_ARRAY_0060fc0_8_8_;
long null_ARRAY_0060fc4_0_4_;
long null_ARRAY_0060fc4_16_8_;
long null_ARRAY_0060fc4_4_4_;
long null_ARRAY_0060fc4_8_4_;
long local_2f_1_1_;
long local_4_4_4_;
long local_5_4_4_;
long local_5b_1_1_;
long local_9d_0_4_;
long local_a0_0_8_;
long local_a0_4_6_;
long local_a0_8_2_;
long local_a1_4_4_;
long local_a2_4_4_;
long local_a3_0_4_;
long local_a3_4_4_;
long local_a4_0_2_;
long local_a4_0_4_;
long local_a4_0_8_;
long local_a4_1_9_;
long local_a4_4_6_;
long local_a4_8_2_;
long local_a5_0_4_;
long local_a5_4_4_;
long DAT_00000010;
long DAT_0040cd80;
long DAT_0040ce0a;
long DAT_0040ce69;
long DAT_0040ce94;
long DAT_0040d420;
long DAT_0040d424;
long DAT_0040d428;
long DAT_0040d42b;
long DAT_0040d42d;
long DAT_0040d431;
long DAT_0040d435;
long DAT_0040d9eb;
long DAT_0040dd95;
long DAT_0040de99;
long DAT_0040de9f;
long DAT_0040dea1;
long DAT_0040dea2;
long DAT_0040dec0;
long DAT_0040dec4;
long DAT_0040df46;
long DAT_0060f5e0;
long DAT_0060f5f0;
long DAT_0060f600;
long DAT_0060f9c8;
long DAT_0060fa30;
long DAT_0060fa34;
long DAT_0060fa38;
long DAT_0060fa3c;
long DAT_0060fa40;
long DAT_0060fa50;
long DAT_0060fa60;
long DAT_0060fa68;
long DAT_0060fa80;
long DAT_0060fa88;
long DAT_0060fad0;
long DAT_0060fad8;
long DAT_0060fae0;
long DAT_0060fc38;
long DAT_0060fc78;
long DAT_0060fc80;
long DAT_0060fc88;
long DAT_0060fc98;
long _DYNAMIC;
long fde_0040e900;
long null_ARRAY_0040d320;
long null_ARRAY_0040d380;
long null_ARRAY_0040e1e0;
long null_ARRAY_0040e410;
long null_ARRAY_0060fa20;
long null_ARRAY_0060faa0;
long null_ARRAY_0060fb00;
long null_ARRAY_0060fc00;
long null_ARRAY_0060fc40;
long PTR_DAT_0060f9c0;
long PTR_null_ARRAY_0060fa18;
long register0x00000020;
void
FUN_00402030 (uint uParm1)
{
  int iVar1;
  long lVar2;
  undefined8 uVar3;
  undefined **ppuVar4;
  char *pcVar5;
  undefined *puVar6;
  undefined *local_88;
  char *local_80;
  undefined8 local_78;
  char *local_70;
  char *local_68;
  char *local_60;
  char *local_58;
  char *local_50;
  char *local_48;
  char *local_40;
  char *local_38;
  char *local_30;
  undefined8 local_28;
  undefined8 local_20;

  if (uParm1 == 0)
    goto LAB_0040205d;
  func_0x00401720 (DAT_0060fa60, "Try \'%s --help\' for more information.\n",
		   DAT_0060fae0);
  do
    {
      func_0x004018c0 ((ulong) uParm1);
    LAB_0040205d:
      ;
      func_0x00401580 ("Usage: %s COMMAND [ARG]...\n  or:  %s OPTION\n",
		       DAT_0060fae0, DAT_0060fae0);
      uVar3 = DAT_0060fa40;
      func_0x00401730 ("Run COMMAND, ignoring hangup signals.\n\n",
		       DAT_0060fa40);
      func_0x00401730 ("      --help     display this help and exit\n",
		       uVar3);
      func_0x00401730
	("      --version  output version information and exit\n", uVar3);
      func_0x00401580
	("\nIf standard input is a terminal, redirect it from an unreadable file.\nIf standard output is a terminal, append output to \'nohup.out\' if possible,\n\'$HOME/nohup.out\' otherwise.\nIf standard error is a terminal, redirect it to standard output.\nTo save output to FILE, use \'%s COMMAND > FILE\'.\n",
	 DAT_0060fae0);
      func_0x00401580
	("\nNOTE: your shell may have its own version of %s, which usually supersedes\nthe version described here.  Please refer to your shell\'s documentation\nfor details about the options it supports.\n",
	 "nohup");
      local_88 = &DAT_0040cd80;
      local_80 = "test invocation";
      puVar6 = &DAT_0040cd80;
      local_78 = 0x40cde9;
      local_70 = "Multi-call invocation";
      local_68 = "sha224sum";
      local_60 = "sha2 utilities";
      local_58 = "sha256sum";
      local_50 = "sha2 utilities";
      local_48 = "sha384sum";
      local_40 = "sha2 utilities";
      local_38 = "sha512sum";
      local_30 = "sha2 utilities";
      local_28 = 0;
      local_20 = 0;
      ppuVar4 = &local_88;
      do
	{
	  iVar1 = func_0x00401820 ("nohup", puVar6);
	  if (iVar1 == 0)
	    break;
	  ppuVar4 = ppuVar4 + 2;
	  puVar6 = *ppuVar4;
	}
      while (puVar6 != (undefined *) 0x0);
      pcVar5 = ppuVar4[1];
      if (pcVar5 == (char *) 0x0)
	{
	  func_0x00401580 ("\n%s online help: <%s>\n", "GNU coreutils",
			   "https://www.gnu.org/software/coreutils/");
	  lVar2 = func_0x00401880 (5, 0);
	  if (lVar2 != 0)
	    {
	      iVar1 = func_0x004017a0 (lVar2, &DAT_0040ce0a, 3);
	      if (iVar1 != 0)
		{
		  pcVar5 = "nohup";
		  goto LAB_00402219;
		}
	    }
	  func_0x00401580 ("Full documentation at: <%s%s>\n",
			   "https://www.gnu.org/software/coreutils/",
			   "nohup");
	LAB_00402242:
	  ;
	  pcVar5 = "nohup";
	  uVar3 = 0x40cda2;
	}
      else
	{
	  func_0x00401580 ("\n%s online help: <%s>\n", "GNU coreutils",
			   "https://www.gnu.org/software/coreutils/");
	  lVar2 = func_0x00401880 (5, 0);
	  if (lVar2 != 0)
	    {
	      iVar1 = func_0x004017a0 (lVar2, &DAT_0040ce0a, 3);
	      if (iVar1 != 0)
		{
		LAB_00402219:
		  ;
		  func_0x00401580
		    ("Report %s translation bugs to <https://translationproject.org/team/>\n",
		     "nohup");
		}
	    }
	  func_0x00401580 ("Full documentation at: <%s%s>\n",
			   "https://www.gnu.org/software/coreutils/",
			   "nohup");
	  uVar3 = 0x40debf;
	  if (pcVar5 == "nohup")
	    goto LAB_00402242;
	}
      func_0x00401580
	("or available locally via: info \'(coreutils) %s%s\'\n", pcVar5,
	 uVar3);
    }
  while (true);
}


#include "paste.h"

long null_ARRAY_0060fa8_0_8_;
long null_ARRAY_0060fa8_8_8_;
long null_ARRAY_0060fc8_0_8_;
long null_ARRAY_0060fc8_16_8_;
long null_ARRAY_0060fc8_24_8_;
long null_ARRAY_0060fc8_32_8_;
long null_ARRAY_0060fc8_40_8_;
long null_ARRAY_0060fc8_48_8_;
long null_ARRAY_0060fc8_8_8_;
long null_ARRAY_0060fcc_0_4_;
long null_ARRAY_0060fcc_16_8_;
long null_ARRAY_0060fcc_4_4_;
long null_ARRAY_0060fcc_8_4_;
long local_2f_1_1_;
long local_4_4_4_;
long local_5b_1_1_;
long local_9d_0_4_;
long local_a0_0_8_;
long local_a0_4_6_;
long local_a0_8_2_;
long local_a1_4_4_;
long local_a2_4_4_;
long local_a3_0_4_;
long local_a3_4_4_;
long local_a4_0_2_;
long local_a4_0_4_;
long local_a4_0_8_;
long local_a4_1_9_;
long local_a4_4_6_;
long local_a4_8_2_;
long local_a5_0_4_;
long local_a5_4_4_;
long DAT_00000010;
long DAT_0040cf65;
long DAT_0040cfef;
long DAT_0040cff7;
long DAT_0040d017;
long DAT_0040d510;
long DAT_0040d514;
long DAT_0040d518;
long DAT_0040d51b;
long DAT_0040d51d;
long DAT_0040d521;
long DAT_0040d525;
long DAT_0040daab;
long DAT_0040de55;
long DAT_0040df59;
long DAT_0040df5f;
long DAT_0040df71;
long DAT_0040df72;
long DAT_0040df90;
long DAT_0040df94;
long DAT_0040e016;
long DAT_0060f648;
long DAT_0060f658;
long DAT_0060f668;
long DAT_0060fa20;
long DAT_0060fa30;
long DAT_0060fa90;
long DAT_0060fa94;
long DAT_0060fa98;
long DAT_0060fa9c;
long DAT_0060fac0;
long DAT_0060fac8;
long DAT_0060fad0;
long DAT_0060fae0;
long DAT_0060fae8;
long DAT_0060fb00;
long DAT_0060fb08;
long DAT_0060fb50;
long DAT_0060fb58;
long DAT_0060fb60;
long DAT_0060fb61;
long DAT_0060fb68;
long DAT_0060fb70;
long DAT_0060fb78;
long DAT_0060fcf8;
long DAT_0060fd00;
long DAT_0060fd08;
long DAT_0060fd18;
long fde_0040e9b8;
long null_ARRAY_0040d400;
long null_ARRAY_0040e2a0;
long null_ARRAY_0040e4d0;
long null_ARRAY_0060fa80;
long null_ARRAY_0060fb20;
long null_ARRAY_0060fb80;
long null_ARRAY_0060fc80;
long null_ARRAY_0060fcc0;
long PTR_DAT_0060fa28;
long PTR_null_ARRAY_0060fa78;
long register0x00000020;
void
FUN_004024f0 (uint uParm1)
{
  undefined8 uVar1;
  int iVar2;
  long lVar3;
  undefined1 *puVar4;
  undefined **ppuVar5;
  char *pcVar6;
  undefined *puVar7;
  undefined *local_88;
  char *local_80;
  undefined8 local_78;
  char *local_70;
  char *local_68;
  char *local_60;
  char *local_58;
  char *local_50;
  char *local_48;
  char *local_40;
  char *local_38;
  char *local_30;
  undefined8 local_28;
  undefined8 local_20;

  if (uParm1 == 0)
    goto LAB_0040251d;
  func_0x00401720 (DAT_0060fae0, "Try \'%s --help\' for more information.\n",
		   DAT_0060fb78);
  do
    {
      func_0x004018e0 ((ulong) uParm1);
    LAB_0040251d:
      ;
      func_0x004015a0 ("Usage: %s [OPTION]... [FILE]...\n", DAT_0060fb78);
      uVar1 = DAT_0060fac0;
      func_0x00401730
	("Write lines consisting of the sequentially corresponding lines from\neach FILE, separated by TABs, to standard output.\n",
	 DAT_0060fac0);
      func_0x00401730
	("\nWith no FILE, or when FILE is -, read standard input.\n", uVar1);
      func_0x00401730
	("\nMandatory arguments to long options are mandatory for short options too.\n",
	 uVar1);
      func_0x00401730
	("  -d, --delimiters=LIST   reuse characters from LIST instead of TABs\n  -s, --serial            paste one file at a time instead of in parallel\n",
	 uVar1);
      func_0x00401730
	("  -z, --zero-terminated    line delimiter is NUL, not newline\n",
	 uVar1);
      func_0x00401730 ("      --help     display this help and exit\n",
		       uVar1);
      func_0x00401730
	("      --version  output version information and exit\n", uVar1);
      local_88 = &DAT_0040cf65;
      local_80 = "test invocation";
      puVar7 = &DAT_0040cf65;
      local_78 = 0x40cfce;
      local_70 = "Multi-call invocation";
      local_68 = "sha224sum";
      local_60 = "sha2 utilities";
      local_58 = "sha256sum";
      local_50 = "sha2 utilities";
      local_48 = "sha384sum";
      local_40 = "sha2 utilities";
      local_38 = "sha512sum";
      local_30 = "sha2 utilities";
      local_28 = 0;
      local_20 = 0;
      ppuVar5 = &local_88;
      do
	{
	  iVar2 = func_0x00401820 ("paste", puVar7);
	  if (iVar2 == 0)
	    break;
	  ppuVar5 = ppuVar5 + 2;
	  puVar7 = *ppuVar5;
	}
      while (puVar7 != (undefined *) 0x0);
      pcVar6 = ppuVar5[1];
      if (pcVar6 == (char *) 0x0)
	{
	  func_0x004015a0 ("\n%s online help: <%s>\n", "GNU coreutils",
			   "https://www.gnu.org/software/coreutils/");
	  lVar3 = func_0x00401880 (5, 0);
	  if (lVar3 != 0)
	    {
	      iVar2 = func_0x00401790 (lVar3, &DAT_0040cfef, 3);
	      if (iVar2 != 0)
		{
		  pcVar6 = "paste";
		  goto LAB_004026e9;
		}
	    }
	  func_0x004015a0 ("Full documentation at: <%s%s>\n",
			   "https://www.gnu.org/software/coreutils/",
			   "paste");
	LAB_00402712:
	  ;
	  pcVar6 = "paste";
	  puVar4 = (undefined1 *) 0x40cf87;
	}
      else
	{
	  func_0x004015a0 ("\n%s online help: <%s>\n", "GNU coreutils",
			   "https://www.gnu.org/software/coreutils/");
	  lVar3 = func_0x00401880 (5, 0);
	  if (lVar3 != 0)
	    {
	      iVar2 = func_0x00401790 (lVar3, &DAT_0040cfef, 3);
	      if (iVar2 != 0)
		{
		LAB_004026e9:
		  ;
		  func_0x004015a0
		    ("Report %s translation bugs to <https://translationproject.org/team/>\n",
		     "paste");
		}
	    }
	  func_0x004015a0 ("Full documentation at: <%s%s>\n",
			   "https://www.gnu.org/software/coreutils/",
			   "paste");
	  puVar4 = &DAT_0040cff7;
	  if (pcVar6 == "paste")
	    goto LAB_00402712;
	}
      func_0x004015a0
	("or available locally via: info \'(coreutils) %s%s\'\n", pcVar6,
	 puVar4);
    }
  while (true);
}

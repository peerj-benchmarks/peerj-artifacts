typedef unsigned char   undefined;

typedef unsigned char    byte;
typedef unsigned char    dwfenc;
typedef unsigned int    dword;
typedef unsigned long    qword;
typedef unsigned char    undefined1;
typedef unsigned short    undefined2;
typedef unsigned int    undefined4;
typedef unsigned long    undefined5;
typedef unsigned long    undefined6;
typedef unsigned long    undefined7;
typedef unsigned long    undefined8;
typedef unsigned short    word;
#define unkbyte9   unsigned long long
#define unkbyte10   unsigned long long
#define unkbyte11   unsigned long long
#define unkbyte12   unsigned long long
#define unkbyte13   unsigned long long
#define unkbyte14   unsigned long long
#define unkbyte15   unsigned long long
#define unkbyte16   unsigned long long

#define unkuint9   unsigned long long
#define unkuint10   unsigned long long
#define unkuint11   unsigned long long
#define unkuint12   unsigned long long
#define unkuint13   unsigned long long
#define unkuint14   unsigned long long
#define unkuint15   unsigned long long
#define unkuint16   unsigned long long

#define unkint9   long long
#define unkint10   long long
#define unkint11   long long
#define unkint12   long long
#define unkint13   long long
#define unkint14   long long
#define unkint15   long long
#define unkint16   long long

#define unkfloat1   float
#define unkfloat2   float
#define unkfloat3   float
#define unkfloat5   double
#define unkfloat6   double
#define unkfloat7   double
#define unkfloat9   long double
#define unkfloat11   long double
#define unkfloat12   long double
#define unkfloat13   long double
#define unkfloat14   long double
#define unkfloat15   long double
#define unkfloat16   long double

#define BADSPACEBASE   void
#define code   void

#define ulong long
#define true 1
#define bool int
#define false 0
#define float10 int
#define ushort int
#define undefined7 long
#define undefined2 long
#define uint5 long
#define uint6 long

// Add definition for uint
typedef unsigned int    uint;

#define sbyte char

extern imperfection_wrapper(void);
typedef struct eh_frame_hdr eh_frame_hdr, *Peh_frame_hdr;

struct eh_frame_hdr {
    byte eh_frame_hdr_version; // Exception Handler Frame Header Version
    dwfenc eh_frame_pointer_encoding; // Exception Handler Frame Pointer Encoding
    dwfenc eh_frame_desc_entry_count_encoding; // Encoding of # of Exception Handler FDEs
    dwfenc eh_frame_table_encoding; // Exception Handler Table Encoding
};

typedef struct fde_table_entry fde_table_entry, *Pfde_table_entry;

struct fde_table_entry {
    dword initial_loc; // Initial Location
    dword data_loc; // Data location
};

typedef enum Elf64_DynTag {
    DT_AUDIT=1879047932,
    DT_AUXILIARY=2147483645,
    DT_BIND_NOW=24,
    DT_CHECKSUM=1879047672,
    DT_CONFIG=1879047930,
    DT_DEBUG=21,
    DT_DEPAUDIT=1879047931,
    DT_ENCODING=32,
    DT_FEATURE_1=1879047676,
    DT_FILTER=2147483647,
    DT_FINI=13,
    DT_FINI_ARRAY=26,
    DT_FINI_ARRAYSZ=28,
    DT_FLAGS=30,
    DT_FLAGS_1=1879048187,
    DT_GNU_CONFLICT=1879047928,
    DT_GNU_CONFLICTSZ=1879047670,
    DT_GNU_HASH=1879047925,
    DT_GNU_LIBLIST=1879047929,
    DT_GNU_LIBLISTSZ=1879047671,
    DT_GNU_PRELINKED=1879047669,
    DT_HASH=4,
    DT_INIT=12,
    DT_INIT_ARRAY=25,
    DT_INIT_ARRAYSZ=27,
    DT_JMPREL=23,
    DT_MOVEENT=1879047674,
    DT_MOVESZ=1879047675,
    DT_MOVETAB=1879047934,
    DT_NEEDED=1,
    DT_NULL=0,
    DT_PLTGOT=3,
    DT_PLTPAD=1879047933,
    DT_PLTPADSZ=1879047673,
    DT_PLTREL=20,
    DT_PLTRELSZ=2,
    DT_POSFLAG_1=1879047677,
    DT_PREINIT_ARRAYSZ=33,
    DT_REL=17,
    DT_RELA=7,
    DT_RELACOUNT=1879048185,
    DT_RELAENT=9,
    DT_RELASZ=8,
    DT_RELCOUNT=1879048186,
    DT_RELENT=19,
    DT_RELSZ=18,
    DT_RPATH=15,
    DT_RUNPATH=29,
    DT_SONAME=14,
    DT_STRSZ=10,
    DT_STRTAB=5,
    DT_SYMBOLIC=16,
    DT_SYMENT=11,
    DT_SYMINENT=1879047679,
    DT_SYMINFO=1879047935,
    DT_SYMINSZ=1879047678,
    DT_SYMTAB=6,
    DT_TEXTREL=22,
    DT_TLSDESC_GOT=1879047927,
    DT_TLSDESC_PLT=1879047926,
    DT_VERDEF=1879048188,
    DT_VERDEFNUM=1879048189,
    DT_VERNEED=1879048190,
    DT_VERNEEDNUM=1879048191,
    DT_VERSYM=1879048176
} Elf64_DynTag;

typedef enum Elf_ProgramHeaderType {
    PT_DYNAMIC=2,
    PT_GNU_EH_FRAME=1685382480,
    PT_GNU_RELRO=1685382482,
    PT_GNU_STACK=1685382481,
    PT_INTERP=3,
    PT_LOAD=1,
    PT_NOTE=4,
    PT_NULL=0,
    PT_PHDR=6,
    PT_SHLIB=5,
    PT_TLS=7
} Elf_ProgramHeaderType;

typedef struct Elf64_Dyn Elf64_Dyn, *PElf64_Dyn;

struct Elf64_Dyn {
    enum Elf64_DynTag d_tag;
    qword d_val;
};

typedef struct Elf64_Rela Elf64_Rela, *PElf64_Rela;

struct Elf64_Rela {
    qword r_offset; // location to apply the relocation action
    qword r_info; // the symbol table index and the type of relocation
    qword r_addend; // a constant addend used to compute the relocatable field value
};

typedef struct Elf64_Shdr Elf64_Shdr, *PElf64_Shdr;

typedef enum Elf_SectionHeaderType {
    SHT_CHECKSUM=1879048184,
    SHT_DYNAMIC=6,
    SHT_DYNSYM=11,
    SHT_FINI_ARRAY=15,
    SHT_GNU_ATTRIBUTES=1879048181,
    SHT_GNU_HASH=1879048182,
    SHT_GNU_LIBLIST=1879048183,
    SHT_GNU_verdef=1879048189,
    SHT_GNU_verneed=1879048190,
    SHT_GNU_versym=1879048191,
    SHT_GROUP=17,
    SHT_HASH=5,
    SHT_INIT_ARRAY=14,
    SHT_NOBITS=8,
    SHT_NOTE=7,
    SHT_NULL=0,
    SHT_PREINIT_ARRAY=16,
    SHT_PROGBITS=1,
    SHT_REL=9,
    SHT_RELA=4,
    SHT_SHLIB=10,
    SHT_STRTAB=3,
    SHT_SUNW_COMDAT=1879048187,
    SHT_SUNW_move=1879048186,
    SHT_SUNW_syminfo=1879048188,
    SHT_SYMTAB=2,
    SHT_SYMTAB_SHNDX=18
} Elf_SectionHeaderType;

struct Elf64_Shdr {
    dword sh_name;
    enum Elf_SectionHeaderType sh_type;
    qword sh_flags;
    qword sh_addr;
    qword sh_offset;
    qword sh_size;
    dword sh_link;
    dword sh_info;
    qword sh_addralign;
    qword sh_entsize;
};

typedef struct Elf64_Phdr Elf64_Phdr, *PElf64_Phdr;

struct Elf64_Phdr {
    enum Elf_ProgramHeaderType p_type;
    dword p_flags;
    qword p_offset;
    qword p_vaddr;
    qword p_paddr;
    qword p_filesz;
    qword p_memsz;
    qword p_align;
};

typedef struct Elf64_Sym Elf64_Sym, *PElf64_Sym;

struct Elf64_Sym {
    dword st_name;
    byte st_info;
    byte st_other;
    word st_shndx;
    qword st_value;
    qword st_size;
};

typedef struct Elf64_Ehdr Elf64_Ehdr, *PElf64_Ehdr;

struct Elf64_Ehdr {
    byte e_ident_magic_num;
    char e_ident_magic_str[3];
    byte e_ident_class;
    byte e_ident_data;
    byte e_ident_version;
    byte e_ident_pad[9];
    word e_type;
    word e_machine;
    dword e_version;
    qword e_entry;
    qword e_phoff;
    qword e_shoff;
    dword e_flags;
    word e_ehsize;
    word e_phentsize;
    word e_phnum;
    word e_shentsize;
    word e_shnum;
    word e_shstrndx;
};




undefined8 _DT_INIT(void);
void FUN_00402180(void);
void thunk_FUN_00636048(void);
void thunk_FUN_00636150(void);
void FUN_004026f0(void);
void FUN_004028f0(void);
ulong FUN_00402910(ulong uParm1,undefined8 *puParm2);
void entry(void);
void FUN_00403bd0(undefined8 *puParm1);
void FUN_00403c00(void);
void FUN_00403c80(void);
void FUN_00403d00(void);
void FUN_00403d40(long *plParm1,undefined8 uParm2);
void FUN_00403e80(uint uParm1);
void FUN_00404100(void);
long FUN_00404110(undefined8 uParm1,long *plParm2,long lParm3,long lParm4);
void FUN_00404240(undefined8 uParm1,undefined8 uParm2,long lParm3);
void FUN_00404290(long *plParm1,long lParm2,long lParm3);
long FUN_00404360(undefined8 uParm1,undefined8 uParm2,undefined8 uParm3,undefined8 uParm4,undefined8 uParm5,code *pcParm6);
void FUN_004043d0(undefined8 uParm1);
void FUN_00404400(undefined8 uParm1);
long FUN_00404440(long *plParm1,int *piParm2);
long FUN_004044d0(long *plParm1);
void FUN_004044f0(long *plParm1);
void FUN_00404510(void);
ulong FUN_004045a0(ulong *puParm1,ulong uParm2);
ulong FUN_004045b0(ulong *puParm1,ulong *puParm2);
ulong FUN_004045c0(ulong uParm1,ulong uParm2);
void FUN_004045d0(long lParm1);
long FUN_004045f0(undefined8 *puParm1,long **pplParm2,long lParm3);
long FUN_004046a0(long *plParm1,long lParm2);
long * FUN_00404710(void);
void FUN_00404780(undefined8 *puParm1);
undefined8 FUN_004047b0(long lParm1,undefined8 uParm2,undefined8 uParm3);
ulong FUN_00404810(long lParm1,undefined8 uParm2,undefined8 uParm3);
void FUN_00404870(void);
void FUN_00404880(void);
uint FUN_00404890(void);
uint FUN_004048b0(void);
ulong thunk_FUN_00407590(byte *pbParm1,ulong uParm2);
ulong FUN_004048e0(byte *pbParm1,ulong uParm2);
ulong FUN_00404b20(undefined8 uParm1,long lParm2,ulong uParm3);
undefined8 FUN_00404c00(byte *pbParm1,ulong uParm2);
void FUN_00404c90(void);
ulong FUN_00404ca0(undefined8 uParm1,char *pcParm2,ulong uParm3);
ulong FUN_00404d40(long **pplParm1,long lParm2);
void FUN_00404ef0(long **pplParm1,long *plParm2,uint uParm3);
ulong FUN_00405250(code *pcParm1,long lParm2,undefined8 uParm3,uint uParm4,byte bParm5);
ulong FUN_00405410(undefined8 uParm1,undefined8 uParm2,char *pcParm3,ulong uParm4,char cParm5);
void FUN_004054c0(undefined8 uParm1,byte *pbParm2,long lParm3);
char * FUN_00405500(long param_1,char *param_2,undefined8 *param_3,undefined param_4,ulong param_5,undefined8 param_6,uint param_7);
void FUN_00406f80(void);
undefined8 FUN_00406fa0(uint uParm1);
long FUN_00406ff0(undefined8 uParm1,undefined8 uParm2,undefined8 uParm3);
ulong FUN_00407160(ulong uParm1);
ulong FUN_00407200(ulong uParm1,ulong uParm2);
undefined FUN_00407210(long lParm1,long lParm2);;
long FUN_00407220(long *plParm1,undefined8 uParm2);
long FUN_00407250(long lParm1,long lParm2,long **pplParm3,char cParm4);
ulong FUN_00407350(float **ppfParm1,undefined8 uParm2,uint uParm3);
undefined8 FUN_004073e0(long lParm1,long **pplParm2,char cParm3);
long FUN_00407530(long lParm1,long lParm2);
ulong FUN_00407590(byte *pbParm1,ulong uParm2);
long * FUN_004075d0(ulong uParm1,undefined1 *puParm2,code *pcParm3,code *pcParm4,long lParm5);
void FUN_00407780(long **pplParm1);
ulong FUN_00407840(long *plParm1,ulong uParm2);
undefined8 FUN_00407a40(long lParm1,long lParm2,long *plParm3);
undefined8 FUN_00407cb0(undefined8 uParm1,undefined8 uParm2);
long FUN_00407cf0(long lParm1,undefined8 uParm2);
char * FUN_00407ef0(ulong uParm1,long lParm2,ulong uParm3,ulong uParm4,ulong uParm5);
undefined8 FUN_00408920(char *pcParm1,uint *puParm2,long *plParm3);
ulong FUN_00408ac0(ulong *puParm1,ulong uParm2);
ulong FUN_00408ad0(ulong *puParm1,ulong *puParm2);
long * FUN_00408ae0(long lParm1);
long FUN_00408b40(undefined8 *puParm1,long lParm2);
char * FUN_00408be0(long lParm1,long lParm2);
ulong FUN_00408c80(byte *pbParm1,byte *pbParm2);
void FUN_00409520(long lParm1);
undefined8 * FUN_004095c0(undefined8 *puParm1,int iParm2);
undefined * FUN_00409630(char *pcParm1,int iParm2);
ulong FUN_00409700(undefined *param_1,ulong param_2,long param_3,ulong param_4,uint param_5,ulong param_6,long param_7,char *param_8,char *param_9);
undefined1 * FUN_0040a600(uint uParm1,undefined8 uParm2,undefined8 uParm3,uint *puParm4);
void FUN_0040a7b0(uint uParm1,undefined8 uParm2,undefined8 uParm3);
void FUN_0040a7e0(ulong uParm1,undefined8 uParm2);
void FUN_0040a7f0(undefined8 uParm1,undefined8 uParm2,byte bParm3);
void FUN_0040a890(undefined8 uParm1);
void FUN_0040a8b0(uint uParm1,undefined8 uParm2,undefined8 uParm3);
void FUN_0040a940(undefined8 uParm1,undefined8 uParm2);
void FUN_0040a960(undefined8 uParm1);
long FUN_0040a980(long lParm1,undefined8 uParm2);
long FUN_0040a9c0(undefined8 uParm1,long lParm2,undefined8 uParm3,undefined8 uParm4,long *plParm5,undefined8 uParm6);
void FUN_0040ac20(void);
void FUN_0040ac90(void);
void FUN_0040ad20(long lParm1);
long FUN_0040ad40(long lParm1,long lParm2);
void FUN_0040ad80(long lParm1,ulong *puParm2);
void FUN_0040ade0(undefined8 uParm1);
void FUN_0040ae00(ulong uParm1,ulong uParm2);
void FUN_0040ae30(undefined8 uParm1,undefined8 uParm2);
void FUN_0040ae60(undefined8 uParm1);
void FUN_0040ae80(void);
void FUN_0040aeb0(undefined8 uParm1,uint uParm2);
ulong FUN_0040af00(long lParm1,long lParm2);
ulong FUN_0040af30(byte *pbParm1,byte **ppbParm2,uint uParm3,long *plParm4,long lParm5);
ulong FUN_0040b5d0(byte *pbParm1,byte **ppbParm2,uint uParm3,ulong *puParm4,long lParm5);
void FUN_0040ba30(uint uParm1,int iParm2,undefined uParm3,long lParm4,undefined8 uParm5);
ulong FUN_0040bab0(byte *pbParm1,byte **ppbParm2,uint uParm3,ulong *puParm4,long lParm5);
undefined8 FUN_0040bf10(ulong uParm1,ulong uParm2);
void FUN_0040bf60(undefined8 uParm1);
void FUN_0040bfa0(undefined8 uParm1,uint uParm2,undefined8 uParm3,undefined8 uParm4);
void FUN_0040c010(void);
void FUN_0040c050(undefined4 uParm1,undefined4 uParm2,undefined4 uParm3,undefined4 uParm4,undefined4 uParm5,undefined4 uParm6,undefined4 uParm7,undefined4 uParm8,ulong param_1,ulong param_2,undefined8 param_3,undefined8 param_4,undefined8 param_5,undefined8 param_6);
void FUN_0040c130(undefined8 uParm1);
ulong FUN_0040c1c0(long lParm1);
int * FUN_0040c250(int *piParm1);
char * FUN_0040c350(char *pcParm1);
undefined8 FUN_0040c460(int iParm1,long lParm2,ulong uParm3,ulong uParm4,byte bParm5,uint uParm6);
ulong FUN_0040cae0(uint *puParm1,uint *puParm2,uint *puParm3,byte bParm4,uint uParm5);
undefined8 FUN_0040d600(int iParm1,long lParm2,ulong uParm3,ulong uParm4,byte bParm5,uint uParm6);
ulong FUN_0040dc00(byte *pbParm1,byte *pbParm2,byte *pbParm3,byte bParm4,uint uParm5);
ulong FUN_0040e690(undefined8 uParm1,long lParm2,ulong uParm3);
undefined8 FUN_0040e910(void);
void FUN_0040e920(void);
ulong FUN_0040e930(ulong *puParm1,ulong *puParm2);
ulong FUN_0040e950(long lParm1,ulong uParm2);
ulong FUN_0040e960(ulong *puParm1,ulong uParm2);
ulong FUN_0040e970(ulong *puParm1,ulong *puParm2);
ulong FUN_0040e980(long *plParm1,long *plParm2);
ulong FUN_0040e9b0(long lParm1,long lParm2,char cParm3);
long FUN_0040eb20(long lParm1,long lParm2,ulong uParm3);
long FUN_0040ec40(long lParm1,undefined8 uParm2,long lParm3);
void FUN_0040ece0(long lParm1);
void FUN_0040ed20(long lParm1);
void FUN_0040ed50(undefined8 uParm1);
undefined8 FUN_0040ed90(long lParm1);
undefined8 FUN_0040ee90(void);
void FUN_0040eef0(long lParm1,uint uParm2,char cParm3);
ulong FUN_0040ef60(long lParm1);
undefined8 FUN_0040efc0(long *plParm1,ulong *puParm2,long lParm3);
ulong FUN_0040f040(long lParm1);
void FUN_0040f0a0(undefined8 uParm1,ulong uParm2,undefined8 uParm3);
ulong FUN_0040f0f0(long lParm1,long lParm2,ulong uParm3,long lParm4);
undefined8 FUN_0040f2c0(long lParm1,undefined8 *puParm2);
void FUN_0040f370(long lParm1,long lParm2);
long FUN_0040f410(long *plParm1,int iParm2);
long * FUN_0040fdb0(long *plParm1,uint uParm2,long lParm3);
undefined8 FUN_004101a0(long *plParm1);
long FUN_00410320(long *plParm1);
undefined8 FUN_00410a60(undefined8 uParm1,long lParm2,uint uParm3);
ulong FUN_00410a90(long *plParm1,ulong *puParm2,int iParm3,long lParm4);
void FUN_00410bf0(long lParm1,int *piParm2);
ulong FUN_00410cd0(int param_1,undefined8 *param_2,char *param_3,long *param_4,int *param_5,int param_6,int *param_7,int param_8,undefined8 param_9);
ulong FUN_00411250(uint param_1,undefined8 *param_2,char *param_3,long param_4,undefined8 param_5,ulong param_6,uint *param_7,int param_8);
void FUN_004117a0(void);
void FUN_00411800(void);
undefined8 FUN_00411820(long lParm1,long lParm2);
void FUN_004118a0(long lParm1);
ulong FUN_004118c0(uint *puParm1,byte *pbParm2,long lParm3);
ulong * FUN_00411930(ulong *puParm1,char cParm2,ulong uParm3);
void FUN_00411a20(long lParm1);
void FUN_00411ac0(undefined8 *puParm1);
char * FUN_00411b00(void);
void FUN_004121c0(long lParm1,long lParm2);
void FUN_00412200(long *plParm1);
undefined8 FUN_00412250(char *pcParm1,long lParm2,ulong uParm3);
undefined8 FUN_00412380(long lParm1,long lParm2);
ulong FUN_004123a0(long lParm1,uint uParm2,long lParm3,long lParm4,long lParm5);
undefined8 FUN_004125d0(long lParm1,long lParm2,undefined8 uParm3,undefined8 uParm4,long lParm5);
undefined8 FUN_00412640(long *plParm1,code *pcParm2,undefined8 uParm3);
undefined8 FUN_004126b0(long *plParm1,code *pcParm2,undefined8 uParm3);
undefined8 FUN_00412710(long lParm1,ulong uParm2);
undefined8 FUN_004127b0(long *plParm1,undefined8 uParm2);
long * FUN_00412820(long *plParm1,long lParm2);
undefined8 FUN_00412960(long *plParm1,long lParm2);
undefined8 FUN_004129b0(long *plParm1,ulong *puParm2,ulong uParm3);
void FUN_00412a90(long lParm1);
void FUN_00412b10(long *plParm1);
undefined8 FUN_00412cc0(long *plParm1);
undefined8 FUN_004132c0(long lParm1,int iParm2);
undefined8 FUN_004133c0(long lParm1,long lParm2);
void FUN_00413450(undefined8 *puParm1);
void FUN_00413470(undefined8 *puParm1);
undefined8 FUN_004134a0(undefined8 uParm1,long lParm2);
long FUN_004134c0(long *plParm1,undefined8 uParm2,undefined8 uParm3);
void FUN_004136b0(long *plParm1,long lParm2,undefined8 uParm3);
undefined8 FUN_00413760(long *plParm1,long lParm2,long lParm3,long lParm4,uint uParm5);
void FUN_004139e0(long lParm1);
void FUN_00413a40(long lParm1);
void FUN_00413a80(long *plParm1);
void FUN_00413bf0(long lParm1);
undefined8 FUN_00413cb0(long *plParm1);
undefined8 FUN_00413d20(long lParm1,long lParm2);
undefined8 FUN_00413f90(long lParm1,long lParm2);
long FUN_00413ff0(long lParm1,long *plParm2,long lParm3);
undefined8 FUN_00414050(long *plParm1,long lParm2,long lParm3,long lParm4,uint uParm5);
void FUN_00414150(long *plParm1,long *plParm2,long lParm3);
undefined8 FUN_00414190(long lParm1,long lParm2);
undefined8 FUN_00414220(undefined8 uParm1,long lParm2);
long FUN_00414290(long lParm1,long *plParm2,long lParm3);
undefined8 FUN_00414310(long param_1,long *param_2,long *param_3,undefined8 param_4,undefined8 param_5,undefined8 param_6,undefined8 param_7);
undefined8 *FUN_00414420(undefined8 *puParm1,int *piParm2,long *plParm3,long *plParm4,undefined *puParm5);
long ** FUN_004144f0(long **pplParm1,long lParm2);
long FUN_004145b0(undefined4 *puParm1,long *plParm2,long lParm3);
ulong FUN_004147c0(undefined8 uParm1,long lParm2);
undefined8 FUN_00414850(long lParm1,long *plParm2,long *plParm3,undefined8 uParm4);
undefined8 FUN_004149a0(long *plParm1,long lParm2);
undefined8 FUN_00414b90(undefined8 *puParm1,long *plParm2,long lParm3,byte bParm4);
ulong FUN_00414e10(long *plParm1,undefined8 *puParm2,long lParm3,ulong uParm4);
undefined8 FUN_00414f30(long *plParm1,long *plParm2,undefined8 *puParm3);
undefined8 FUN_00414fc0(long *plParm1,long lParm2,long lParm3);
ulong * FUN_00415180(undefined4 *puParm1,long *plParm2,long lParm3,uint uParm4);
ulong * FUN_00415530(undefined4 *puParm1,long *plParm2,long lParm3);
ulong FUN_00415760(undefined8 uParm1,long lParm2,long lParm3,long lParm4);
ulong FUN_00415810(long lParm1,long lParm2,long lParm3,undefined8 uParm4,uint uParm5);
ulong FUN_00415b20(long *plParm1,long lParm2);
undefined8 FUN_00416610(long *plParm1,long *plParm2,long *plParm3,long *plParm4,long *plParm5);
ulong FUN_004167d0(long *plParm1,long lParm2,long lParm3,long lParm4);
ulong FUN_00416a40(long lParm1,long lParm2,ulong uParm3);
ulong FUN_00416b10(long lParm1,long *plParm2,long lParm3,undefined8 uParm4,undefined8 uParm5);
undefined8 FUN_00416c30(byte **ppbParm1,byte *pbParm2,uint uParm3);
long FUN_004173d0(long lParm1,long lParm2,long *plParm3,undefined8 uParm4);
undefined8 FUN_004174b0(long *plParm1,long lParm2);
undefined8 FUN_00417550(long *plParm1,long *plParm2,undefined8 *puParm3,long lParm4,undefined8 uParm5,undefined8 *puParm6);
undefined8 FUN_00417620(long param_1,ulong *param_2,long *param_3,long *param_4,long *param_5,char *param_6,ulong param_7);
long FUN_00417e40(long lParm1,undefined8 uParm2,undefined8 uParm3,char *pcParm4,byte bParm5,int *piParm6);
void FUN_004180a0(long **pplParm1,long *plParm2,long *plParm3,long lParm4);
ulong FUN_00418500(long lParm1,ulong *puParm2,long lParm3,long lParm4,long lParm5);
ulong FUN_004187b0(long **pplParm1,long lParm2,long lParm3,long *plParm4,char cParm5);
ulong FUN_00418fe0(long param_1,long *param_2,long param_3,long param_4,undefined8 param_5,long param_6,uint param_7);
undefined8 FUN_00419760(long lParm1,long *plParm2,undefined8 *puParm3,undefined8 uParm4,long lParm5);
ulong FUN_00419910(long lParm1,long *plParm2,long *plParm3);
long FUN_0041a140(int *piParm1,long lParm2,long lParm3);
ulong FUN_0041a310(long lParm1,ulong *puParm2,ulong uParm3,long lParm4);
ulong FUN_0041ab80(long lParm1,long *plParm2);
ulong FUN_0041aea0(long **param_1,long param_2,ulong param_3,long param_4,long param_5,ulong param_6,long *param_7,undefined8 *param_8,uint param_9);
undefined8 FUN_0041c680(undefined4 *puParm1,long *plParm2,char *pcParm3,int iParm4,undefined8 uParm5,char cParm6);
undefined8 FUN_0041c8d0(long *plParm1,long *plParm2,ulong uParm3);
long FUN_0041cf70(long lParm1,byte *pbParm2,undefined8 uParm3);
long FUN_0041d030(long lParm1,long *plParm2,undefined8 *puParm3,ulong uParm4,long lParm5,int *piParm6);
long FUN_0041e480(undefined8 uParm1,long *plParm2,long lParm3,undefined8 uParm4,long lParm5,int *piParm6);
long FUN_0041e600(long lParm1,long *plParm2,long lParm3,ulong uParm4,long lParm5,int *piParm6);
ulong FUN_0041e7a0(long **pplParm1,undefined8 uParm2,ulong uParm3,long *plParm4);
undefined8 FUN_0041f490(long *plParm1);
ulong FUN_0041f530(undefined8 *puParm1,undefined8 uParm2,ulong uParm3);
ulong FUN_0041f650(long lParm1,undefined8 uParm2,undefined8 uParm3,undefined8 *puParm4,ulong uParm5);
ulong FUN_0041f6f0(long lParm1,long lParm2);
ulong FUN_0041f740(long lParm1,ulong uParm2,long *plParm3);
long FUN_0041f870(long lParm1,long lParm2,long lParm3,ulong uParm4);
char * FUN_0041fbb0(char *pcParm1,char *pcParm2);
ulong FUN_0041ff50(long lParm1);
ulong FUN_0041ffa0(undefined8 *puParm1);
undefined8 * FUN_00420020(long lParm1);
undefined8 FUN_004200c0(long *plParm1,char *pcParm2);
long * FUN_00420240(long lParm1);
undefined8 FUN_00420300(long lParm1,undefined8 uParm2,undefined8 uParm3);
long FUN_00420390(long lParm1,uint *puParm2);
void FUN_004204c0(long lParm1);
void FUN_004204e0(void);
ulong FUN_00420590(char *pcParm1);
ulong FUN_00420600(undefined8 uParm1,undefined8 uParm2,undefined8 uParm3);
ulong FUN_004206d0(undefined8 uParm1);
void FUN_00420740(long lParm1);
undefined8 FUN_00420750(long *plParm1,long *plParm2);
void FUN_004207e0(undefined8 param_1,byte param_2,ulong param_3);
undefined8 FUN_00420830(void);
ulong FUN_00420840(ulong uParm1);
void FUN_004208e0(undefined4 *puParm1,undefined4 uParm2);
ulong FUN_00420910(long lParm1);
ulong FUN_00420920(long lParm1,uint uParm2);
ulong FUN_00420960(long lParm1);
char * FUN_004209a0(void);
void FUN_00420cf0(void);
ulong FUN_00420d40(uint uParm1);
ulong FUN_00420d80(ulong param_1,undefined8 param_2,ulong param_3);
undefined8 FUN_00420ec0(ulong uParm1);
undefined8 FUN_00420f30(ulong uParm1,long lParm2,long lParm3,ulong uParm4);
void FUN_00420ff0(undefined8 uParm1,undefined8 uParm2,undefined8 uParm3);
long FUN_00421000(ulong param_1,long param_2,int param_3,int param_4,int param_5,uint param_6,int param_7,int param_8,int param_9,int param_10);
void FUN_00421130(code *pcParm1,long *plParm2,undefined8 uParm3);
long FUN_004211e0(void);
long FUN_00421290(undefined8 *puParm1,code *pcParm2,long *plParm3);
void FUN_004217d0(undefined8 uParm1);
ulong FUN_004217f0(uint param_1,undefined8 param_2,uint param_3,ulong param_4);
ulong FUN_00421910(char *pcParm1,char *pcParm2,ulong uParm3);
undefined4 * FUN_00421a40(ulong uParm1,uint *puParm2,ulong uParm3,uint *puParm4,long *plParm5);
ulong FUN_00421ba0(void);
uint * FUN_00421e10(ulong uParm1,long lParm2,ulong uParm3,uint *puParm4,ulong *puParm5);
char * FUN_004223d0(int iParm1,long lParm2,long lParm3,long lParm4,uint uParm5);
void FUN_00422970(uint param_1);
ulong FUN_00422af0(void);
void FUN_00422d10(undefined8 uParm1,uint uParm2);
undefined8 *FUN_00422e80(undefined8 *puParm1,undefined8 *puParm2,undefined8 *puParm3,undefined8 uParm4);
ulong FUN_00428450(ulong uParm1,char cParm2);
undefined8 * FUN_004284b0(ulong uParm1);
void FUN_00428530(ulong uParm1);
double FUN_004285c0(int *piParm1);
void FUN_00428600(uint *param_1);
void FUN_00428680(undefined8 uParm1);
undefined8 FUN_00428690(uint *puParm1,ulong *puParm2);
undefined8 FUN_004288b0(byte *pbParm1,ulong *puParm2,byte **ppbParm3);
ulong FUN_00429500(void);
ulong FUN_00429530(void);
void FUN_00429570(void);
undefined8 _DT_FINI(void);
undefined FUN_00636000();
undefined FUN_00636008();
undefined FUN_00636010();
undefined FUN_00636018();
undefined FUN_00636020();
undefined FUN_00636028();
undefined FUN_00636030();
undefined FUN_00636038();
undefined FUN_00636040();
undefined FUN_00636048();
undefined FUN_00636050();
undefined FUN_00636058();
undefined FUN_00636060();
undefined FUN_00636068();
undefined FUN_00636070();
undefined FUN_00636078();
undefined FUN_00636080();
undefined FUN_00636088();
undefined FUN_00636090();
undefined FUN_00636098();
undefined FUN_006360a0();
undefined FUN_006360a8();
undefined FUN_006360b0();
undefined FUN_006360b8();
undefined FUN_006360c0();
undefined FUN_006360c8();
undefined FUN_006360d0();
undefined FUN_006360d8();
undefined FUN_006360e0();
undefined FUN_006360e8();
undefined FUN_006360f0();
undefined FUN_006360f8();
undefined FUN_00636100();
undefined FUN_00636108();
undefined FUN_00636110();
undefined FUN_00636118();
undefined FUN_00636120();
undefined FUN_00636128();
undefined FUN_00636130();
undefined FUN_00636138();
undefined FUN_00636140();
undefined FUN_00636148();
undefined FUN_00636150();
undefined FUN_00636158();
undefined FUN_00636160();
undefined FUN_00636168();
undefined FUN_00636170();
undefined FUN_00636178();
undefined FUN_00636180();
undefined FUN_00636188();
undefined FUN_00636190();
undefined FUN_00636198();
undefined FUN_006361a0();
undefined FUN_006361a8();
undefined FUN_006361b0();
undefined FUN_006361b8();
undefined FUN_006361c0();
undefined FUN_006361c8();
undefined FUN_006361d0();
undefined FUN_006361d8();
undefined FUN_006361e0();
undefined FUN_006361e8();
undefined FUN_006361f0();
undefined FUN_006361f8();
undefined FUN_00636200();
undefined FUN_00636208();
undefined FUN_00636210();
undefined FUN_00636218();
undefined FUN_00636220();
undefined FUN_00636228();
undefined FUN_00636230();
undefined FUN_00636238();
undefined FUN_00636240();
undefined FUN_00636248();
undefined FUN_00636250();
undefined FUN_00636258();
undefined FUN_00636260();
undefined FUN_00636268();
undefined FUN_00636270();
undefined FUN_00636278();
undefined FUN_00636280();
undefined FUN_00636288();
undefined FUN_00636290();
undefined FUN_00636298();
undefined FUN_006362a0();
undefined FUN_006362a8();
undefined FUN_006362b0();
undefined FUN_006362b8();
undefined FUN_006362c0();
undefined FUN_006362c8();
undefined FUN_006362d0();
undefined FUN_006362d8();
undefined FUN_006362e0();
undefined FUN_006362e8();
undefined FUN_006362f0();
undefined FUN_006362f8();
undefined FUN_00636300();
undefined FUN_00636308();
undefined FUN_00636310();
undefined FUN_00636318();
undefined FUN_00636320();
undefined FUN_00636328();
undefined FUN_00636330();
undefined FUN_00636338();
undefined FUN_00636340();
undefined FUN_00636348();
undefined FUN_00636350();
undefined FUN_00636358();
undefined FUN_00636360();
undefined FUN_00636368();
undefined FUN_00636370();
undefined FUN_00636378();
undefined FUN_00636380();
undefined FUN_00636388();
undefined FUN_00636390();
undefined FUN_00636398();
undefined FUN_006363a0();



#include "tr.h"

long null_ARRAY_0061348_0_8_;
long null_ARRAY_0061348_8_8_;
long null_ARRAY_00613dc_0_8_;
long null_ARRAY_00613dc_16_8_;
long null_ARRAY_00613dc_24_8_;
long null_ARRAY_00613dc_32_8_;
long null_ARRAY_00613dc_40_8_;
long null_ARRAY_00613dc_48_8_;
long null_ARRAY_00613dc_8_8_;
long null_ARRAY_00613e0_0_4_;
long null_ARRAY_00613e0_16_8_;
long null_ARRAY_00613e0_4_4_;
long null_ARRAY_00613e0_8_4_;
long local_2f_1_1_;
long local_5b_1_1_;
long local_9d_0_4_;
long local_a0_0_8_;
long local_a0_4_6_;
long local_a0_8_2_;
long local_a1_4_4_;
long local_a2_4_4_;
long local_a3_0_4_;
long local_a3_4_4_;
long local_a4_0_2_;
long local_a4_0_4_;
long local_a4_0_8_;
long local_a4_1_9_;
long local_a4_4_6_;
long local_a4_8_2_;
long local_a5_0_4_;
long local_a5_4_4_;
long lVar8;
long var8;
long DAT_00000010;
long DAT_0040ebda;
long DAT_0040ebdc;
long DAT_0040ebdf;
long DAT_0040ebe2;
long DAT_0040ebe5;
long DAT_0040ebe8;
long DAT_0040ebe9;
long DAT_0040ebeb;
long DAT_0040ebee;
long DAT_0040ec4a;
long DAT_0040ec4c;
long DAT_0040ecd1;
long DAT_0040efd2;
long DAT_00410178;
long DAT_0041017c;
long DAT_00410180;
long DAT_00410183;
long DAT_00410185;
long DAT_00410189;
long DAT_0041018d;
long DAT_0041072b;
long DAT_00410cc8;
long DAT_00410dd1;
long DAT_00410dd7;
long DAT_00410de9;
long DAT_00410dea;
long DAT_00410e08;
long DAT_00410e0c;
long DAT_00410e8e;
long DAT_00613000;
long DAT_00613010;
long DAT_00613020;
long DAT_00613428;
long DAT_00613490;
long DAT_00613494;
long DAT_00613498;
long DAT_0061349c;
long DAT_006134c0;
long DAT_006134c8;
long DAT_006134d0;
long DAT_006134e0;
long DAT_006134e8;
long DAT_00613500;
long DAT_00613508;
long DAT_00613c80;
long DAT_00613c81;
long DAT_00613c82;
long DAT_00613c83;
long DAT_00613c84;
long DAT_00613c88;
long DAT_00613c90;
long DAT_00613c98;
long DAT_00613e38;
long DAT_00613e40;
long DAT_00613e48;
long DAT_00613e58;
long fde_00411898;
long null_ARRAY_0040f000;
long null_ARRAY_0040f100;
long null_ARRAY_00411120;
long null_ARRAY_00411350;
long null_ARRAY_00613440;
long null_ARRAY_00613480;
long null_ARRAY_00613520;
long null_ARRAY_00613580;
long null_ARRAY_00613680;
long null_ARRAY_00613780;
long null_ARRAY_00613880;
long null_ARRAY_00613cc0;
long null_ARRAY_00613dc0;
long null_ARRAY_00613e00;
long PTR_DAT_00613420;
long PTR_null_ARRAY_00613478;
long register0x00000020;
void
FUN_00403d50 (uint uParm1)
{
  int iVar1;
  long lVar2;
  undefined8 uVar3;
  undefined **ppuVar4;
  undefined *puVar5;
  undefined *local_88;
  char *local_80;
  undefined8 local_78;
  char *local_70;
  char *local_68;
  char *local_60;
  char *local_58;
  char *local_50;
  char *local_48;
  char *local_40;
  char *local_38;
  char *local_30;
  undefined8 local_28;
  undefined8 local_20;

  if (uParm1 == 0)
    goto LAB_00403d7d;
  func_0x00401990 (DAT_006134e0, "Try \'%s --help\' for more information.\n",
		   DAT_00613c98);
  do
    {
      func_0x00401b60 ((ulong) uParm1);
    LAB_00403d7d:
      ;
      func_0x004017c0 ("Usage: %s [OPTION]... SET1 [SET2]\n", DAT_00613c98);
      uVar3 = DAT_006134c0;
      func_0x004019a0
	("Translate, squeeze, and/or delete characters from standard input,\nwriting to standard output.\n\n  -c, -C, --complement    use the complement of SET1\n  -d, --delete            delete characters in SET1, do not translate\n  -s, --squeeze-repeats   replace each sequence of a repeated character\n                            that is listed in the last specified SET,\n                            with a single occurrence of that character\n  -t, --truncate-set1     first truncate SET1 to length of SET2\n",
	 DAT_006134c0);
      func_0x004019a0 ("      --help     display this help and exit\n",
		       uVar3);
      func_0x004019a0
	("      --version  output version information and exit\n", uVar3);
      func_0x004019a0
	("\nSETs are specified as strings of characters.  Most represent themselves.\nInterpreted sequences are:\n\n  \\NNN            character with octal value NNN (1 to 3 octal digits)\n  \\\\              backslash\n  \\a              audible BEL\n  \\b              backspace\n  \\f              form feed\n  \\n              new line\n  \\r              return\n  \\t              horizontal tab\n",
	 uVar3);
      func_0x004019a0
	("  \\v              vertical tab\n  CHAR1-CHAR2     all characters from CHAR1 to CHAR2 in ascending order\n  [CHAR*]         in SET2, copies of CHAR until length of SET1\n  [CHAR*REPEAT]   REPEAT copies of CHAR, REPEAT octal if starting with 0\n  [:alnum:]       all letters and digits\n  [:alpha:]       all letters\n  [:blank:]       all horizontal whitespace\n  [:cntrl:]       all control characters\n  [:digit:]       all digits\n",
	 uVar3);
      func_0x004019a0
	("  [:graph:]       all printable characters, not including space\n  [:lower:]       all lower case letters\n  [:print:]       all printable characters, including space\n  [:punct:]       all punctuation characters\n  [:space:]       all horizontal or vertical whitespace\n  [:upper:]       all upper case letters\n  [:xdigit:]      all hexadecimal digits\n  [=CHAR=]        all characters which are equivalent to CHAR\n",
	 uVar3);
      func_0x004019a0
	("\nTranslation occurs if -d is not given and both SET1 and SET2 appear.\n-t may be used only when translating.  SET2 is extended to length of\nSET1 by repeating its last character as necessary.  Excess characters\nof SET2 are ignored.  Only [:lower:] and [:upper:] are guaranteed to\nexpand in ascending order; used in SET2 while translating, they may\nonly be used in pairs to specify case conversion.  -s uses the last\nspecified SET, and occurs after translation or deletion.\n",
	 uVar3);
      local_88 = &DAT_0040ec4a;
      local_80 = "test invocation";
      puVar5 = &DAT_0040ec4a;
      local_78 = 0x40ecb0;
      local_70 = "Multi-call invocation";
      local_68 = "sha224sum";
      local_60 = "sha2 utilities";
      local_58 = "sha256sum";
      local_50 = "sha2 utilities";
      local_48 = "sha384sum";
      local_40 = "sha2 utilities";
      local_38 = "sha512sum";
      local_30 = "sha2 utilities";
      local_28 = 0;
      local_20 = 0;
      ppuVar4 = &local_88;
      do
	{
	  iVar1 = func_0x00401ab0 (&DAT_0040ec4c, puVar5);
	  if (iVar1 == 0)
	    break;
	  ppuVar4 = ppuVar4 + 2;
	  puVar5 = *ppuVar4;
	}
      while (puVar5 != (undefined *) 0x0);
      puVar5 = ppuVar4[1];
      if (puVar5 == (undefined *) 0x0)
	{
	  func_0x004017c0 ("\n%s online help: <%s>\n", "GNU coreutils",
			   "https://www.gnu.org/software/coreutils/");
	  lVar2 = func_0x00401b10 (5, 0);
	  if (lVar2 != 0)
	    {
	      iVar1 = func_0x00401a10 (lVar2, &DAT_0040ecd1, 3);
	      if (iVar1 != 0)
		{
		  puVar5 = &DAT_0040ec4c;
		  goto LAB_00403f49;
		}
	    }
	  func_0x004017c0 ("Full documentation at: <%s%s>\n",
			   "https://www.gnu.org/software/coreutils/",
			   &DAT_0040ec4c);
	LAB_00403f72:
	  ;
	  puVar5 = &DAT_0040ec4c;
	  uVar3 = 0x40ec69;
	}
      else
	{
	  func_0x004017c0 ("\n%s online help: <%s>\n", "GNU coreutils",
			   "https://www.gnu.org/software/coreutils/");
	  lVar2 = func_0x00401b10 (5, 0);
	  if (lVar2 != 0)
	    {
	      iVar1 = func_0x00401a10 (lVar2, &DAT_0040ecd1, 3);
	      if (iVar1 != 0)
		{
		LAB_00403f49:
		  ;
		  func_0x004017c0
		    ("Report %s translation bugs to <https://translationproject.org/team/>\n",
		     &DAT_0040ec4c);
		}
	    }
	  func_0x004017c0 ("Full documentation at: <%s%s>\n",
			   "https://www.gnu.org/software/coreutils/",
			   &DAT_0040ec4c);
	  uVar3 = 0x410e07;
	  if (puVar5 == &DAT_0040ec4c)
	    goto LAB_00403f72;
	}
      func_0x004017c0
	("or available locally via: info \'(coreutils) %s%s\'\n", puVar5,
	 uVar3);
    }
  while (true);
}

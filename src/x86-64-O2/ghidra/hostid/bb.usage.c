
#include "hostid.h"

long null_ARRAY_0060eba_0_8_;
long null_ARRAY_0060eba_8_8_;
long null_ARRAY_0060ed8_0_8_;
long null_ARRAY_0060ed8_16_8_;
long null_ARRAY_0060ed8_24_8_;
long null_ARRAY_0060ed8_32_8_;
long null_ARRAY_0060ed8_40_8_;
long null_ARRAY_0060ed8_48_8_;
long null_ARRAY_0060ed8_8_8_;
long null_ARRAY_0060edc_0_4_;
long null_ARRAY_0060edc_16_8_;
long null_ARRAY_0060edc_4_4_;
long null_ARRAY_0060edc_8_4_;
long local_2f_1_1_;
long local_5b_1_1_;
long local_9d_0_4_;
long local_a0_0_8_;
long local_a0_4_6_;
long local_a0_8_2_;
long local_a1_4_4_;
long local_a2_4_4_;
long local_a3_0_4_;
long local_a3_4_4_;
long local_a4_0_2_;
long local_a4_0_4_;
long local_a4_0_8_;
long local_a4_1_9_;
long local_a4_4_6_;
long local_a4_8_2_;
long local_a5_0_4_;
long local_a5_4_4_;
long DAT_00000010;
long DAT_0040c400;
long DAT_0040c48b;
long DAT_0040c6b8;
long DAT_0040c7a0;
long DAT_0040c7a4;
long DAT_0040c7a8;
long DAT_0040c7ab;
long DAT_0040c7ad;
long DAT_0040c7b1;
long DAT_0040c7b5;
long DAT_0040cd6b;
long DAT_0040d115;
long DAT_0040d219;
long DAT_0040d21f;
long DAT_0040d231;
long DAT_0040d232;
long DAT_0040d250;
long DAT_0040d254;
long DAT_0040d2d6;
long DAT_0060e788;
long DAT_0060e798;
long DAT_0060e7a8;
long DAT_0060eb48;
long DAT_0060ebb0;
long DAT_0060ebb4;
long DAT_0060ebb8;
long DAT_0060ebbc;
long DAT_0060ebc0;
long DAT_0060ebd0;
long DAT_0060ebe0;
long DAT_0060ebe8;
long DAT_0060ec00;
long DAT_0060ec08;
long DAT_0060ec50;
long DAT_0060ec58;
long DAT_0060ec60;
long DAT_0060edf8;
long DAT_0060ee00;
long DAT_0060ee08;
long DAT_0060ee18;
long _DYNAMIC;
long fde_0040dc40;
long null_ARRAY_0040c680;
long null_ARRAY_0040c700;
long null_ARRAY_0040d560;
long null_ARRAY_0040d790;
long null_ARRAY_0060eb60;
long null_ARRAY_0060eba0;
long null_ARRAY_0060ec20;
long null_ARRAY_0060ec80;
long null_ARRAY_0060ed80;
long null_ARRAY_0060edc0;
long PTR_DAT_0060eb40;
long PTR_null_ARRAY_0060eb98;
long register0x00000020;
void
FUN_00401ab0 (uint uParm1)
{
  int iVar1;
  long lVar2;
  undefined8 uVar3;
  undefined **ppuVar4;
  char *pcVar5;
  undefined *puVar6;
  undefined *local_88;
  char *local_80;
  undefined8 local_78;
  char *local_70;
  char *local_68;
  char *local_60;
  char *local_58;
  char *local_50;
  char *local_48;
  char *local_40;
  char *local_38;
  char *local_30;
  undefined8 local_28;
  undefined8 local_20;

  if (uParm1 == 0)
    goto LAB_00401add;
  func_0x004015c0 (DAT_0060ebe0, "Try \'%s --help\' for more information.\n",
		   DAT_0060ec60);
  do
    {
      func_0x00401740 ((ulong) uParm1);
    LAB_00401add:
      ;
      func_0x00401450
	("Usage: %s [OPTION]\nPrint the numeric identifier (in hexadecimal) for the current host.\n\n",
	 DAT_0060ec60);
      uVar3 = DAT_0060ebc0;
      func_0x004015d0 ("      --help     display this help and exit\n",
		       DAT_0060ebc0);
      func_0x004015d0
	("      --version  output version information and exit\n", uVar3);
      local_88 = &DAT_0040c400;
      local_80 = "test invocation";
      puVar6 = &DAT_0040c400;
      local_78 = 0x40c46a;
      local_70 = "Multi-call invocation";
      local_68 = "sha224sum";
      local_60 = "sha2 utilities";
      local_58 = "sha256sum";
      local_50 = "sha2 utilities";
      local_48 = "sha384sum";
      local_40 = "sha2 utilities";
      local_38 = "sha512sum";
      local_30 = "sha2 utilities";
      local_28 = 0;
      local_20 = 0;
      ppuVar4 = &local_88;
      do
	{
	  iVar1 = func_0x004016a0 ("hostid", puVar6);
	  if (iVar1 == 0)
	    break;
	  ppuVar4 = ppuVar4 + 2;
	  puVar6 = *ppuVar4;
	}
      while (puVar6 != (undefined *) 0x0);
      pcVar5 = ppuVar4[1];
      if (pcVar5 == (char *) 0x0)
	{
	  func_0x00401450 ("\n%s online help: <%s>\n", "GNU coreutils",
			   "https://www.gnu.org/software/coreutils/");
	  lVar2 = func_0x00401700 (5, 0);
	  if (lVar2 != 0)
	    {
	      iVar1 = func_0x00401620 (lVar2, &DAT_0040c48b, 3);
	      if (iVar1 != 0)
		{
		  pcVar5 = "hostid";
		  goto LAB_00401c69;
		}
	    }
	  func_0x00401450 ("Full documentation at: <%s%s>\n",
			   "https://www.gnu.org/software/coreutils/",
			   "hostid");
	LAB_00401c92:
	  ;
	  pcVar5 = "hostid";
	  uVar3 = 0x40c423;
	}
      else
	{
	  func_0x00401450 ("\n%s online help: <%s>\n", "GNU coreutils",
			   "https://www.gnu.org/software/coreutils/");
	  lVar2 = func_0x00401700 (5, 0);
	  if (lVar2 != 0)
	    {
	      iVar1 = func_0x00401620 (lVar2, &DAT_0040c48b, 3);
	      if (iVar1 != 0)
		{
		LAB_00401c69:
		  ;
		  func_0x00401450
		    ("Report %s translation bugs to <https://translationproject.org/team/>\n",
		     "hostid");
		}
	    }
	  func_0x00401450 ("Full documentation at: <%s%s>\n",
			   "https://www.gnu.org/software/coreutils/",
			   "hostid");
	  uVar3 = 0x40d24f;
	  if (pcVar5 == "hostid")
	    goto LAB_00401c92;
	}
      func_0x00401450
	("or available locally via: info \'(coreutils) %s%s\'\n", pcVar5,
	 uVar3);
    }
  while (true);
}

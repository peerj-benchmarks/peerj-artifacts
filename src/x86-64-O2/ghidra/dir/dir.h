typedef unsigned char   undefined;

typedef unsigned char    byte;
typedef unsigned char    dwfenc;
typedef unsigned int    dword;
typedef unsigned long    qword;
typedef unsigned char    undefined1;
typedef unsigned short    undefined2;
typedef unsigned int    undefined4;
typedef unsigned long    undefined5;
typedef unsigned long    undefined6;
typedef unsigned long    undefined7;
typedef unsigned long    undefined8;
typedef unsigned short    word;
#define unkbyte9   unsigned long long
#define unkbyte10   unsigned long long
#define unkbyte11   unsigned long long
#define unkbyte12   unsigned long long
#define unkbyte13   unsigned long long
#define unkbyte14   unsigned long long
#define unkbyte15   unsigned long long
#define unkbyte16   unsigned long long

#define unkuint9   unsigned long long
#define unkuint10   unsigned long long
#define unkuint11   unsigned long long
#define unkuint12   unsigned long long
#define unkuint13   unsigned long long
#define unkuint14   unsigned long long
#define unkuint15   unsigned long long
#define unkuint16   unsigned long long

#define unkint9   long long
#define unkint10   long long
#define unkint11   long long
#define unkint12   long long
#define unkint13   long long
#define unkint14   long long
#define unkint15   long long
#define unkint16   long long

#define unkfloat1   float
#define unkfloat2   float
#define unkfloat3   float
#define unkfloat5   double
#define unkfloat6   double
#define unkfloat7   double
#define unkfloat9   long double
#define unkfloat11   long double
#define unkfloat12   long double
#define unkfloat13   long double
#define unkfloat14   long double
#define unkfloat15   long double
#define unkfloat16   long double

#define BADSPACEBASE   void
#define code   void

#define ulong long
#define true 1
#define bool int
#define false 0
#define float10 int
#define ushort int
#define undefined7 long
#define undefined2 long
#define uint5 long
#define uint6 long

// Add definition for uint
typedef unsigned int    uint;

#define sbyte char

extern imperfection_wrapper(void);
typedef struct eh_frame_hdr eh_frame_hdr, *Peh_frame_hdr;

struct eh_frame_hdr {
    byte eh_frame_hdr_version; // Exception Handler Frame Header Version
    dwfenc eh_frame_pointer_encoding; // Exception Handler Frame Pointer Encoding
    dwfenc eh_frame_desc_entry_count_encoding; // Encoding of # of Exception Handler FDEs
    dwfenc eh_frame_table_encoding; // Exception Handler Table Encoding
};

typedef struct fde_table_entry fde_table_entry, *Pfde_table_entry;

struct fde_table_entry {
    dword initial_loc; // Initial Location
    dword data_loc; // Data location
};

typedef enum Elf64_DynTag {
    DT_AUDIT=1879047932,
    DT_AUXILIARY=2147483645,
    DT_BIND_NOW=24,
    DT_CHECKSUM=1879047672,
    DT_CONFIG=1879047930,
    DT_DEBUG=21,
    DT_DEPAUDIT=1879047931,
    DT_ENCODING=32,
    DT_FEATURE_1=1879047676,
    DT_FILTER=2147483647,
    DT_FINI=13,
    DT_FINI_ARRAY=26,
    DT_FINI_ARRAYSZ=28,
    DT_FLAGS=30,
    DT_FLAGS_1=1879048187,
    DT_GNU_CONFLICT=1879047928,
    DT_GNU_CONFLICTSZ=1879047670,
    DT_GNU_HASH=1879047925,
    DT_GNU_LIBLIST=1879047929,
    DT_GNU_LIBLISTSZ=1879047671,
    DT_GNU_PRELINKED=1879047669,
    DT_HASH=4,
    DT_INIT=12,
    DT_INIT_ARRAY=25,
    DT_INIT_ARRAYSZ=27,
    DT_JMPREL=23,
    DT_MOVEENT=1879047674,
    DT_MOVESZ=1879047675,
    DT_MOVETAB=1879047934,
    DT_NEEDED=1,
    DT_NULL=0,
    DT_PLTGOT=3,
    DT_PLTPAD=1879047933,
    DT_PLTPADSZ=1879047673,
    DT_PLTREL=20,
    DT_PLTRELSZ=2,
    DT_POSFLAG_1=1879047677,
    DT_PREINIT_ARRAYSZ=33,
    DT_REL=17,
    DT_RELA=7,
    DT_RELACOUNT=1879048185,
    DT_RELAENT=9,
    DT_RELASZ=8,
    DT_RELCOUNT=1879048186,
    DT_RELENT=19,
    DT_RELSZ=18,
    DT_RPATH=15,
    DT_RUNPATH=29,
    DT_SONAME=14,
    DT_STRSZ=10,
    DT_STRTAB=5,
    DT_SYMBOLIC=16,
    DT_SYMENT=11,
    DT_SYMINENT=1879047679,
    DT_SYMINFO=1879047935,
    DT_SYMINSZ=1879047678,
    DT_SYMTAB=6,
    DT_TEXTREL=22,
    DT_TLSDESC_GOT=1879047927,
    DT_TLSDESC_PLT=1879047926,
    DT_VERDEF=1879048188,
    DT_VERDEFNUM=1879048189,
    DT_VERNEED=1879048190,
    DT_VERNEEDNUM=1879048191,
    DT_VERSYM=1879048176
} Elf64_DynTag;

typedef enum Elf_ProgramHeaderType {
    PT_DYNAMIC=2,
    PT_GNU_EH_FRAME=1685382480,
    PT_GNU_RELRO=1685382482,
    PT_GNU_STACK=1685382481,
    PT_INTERP=3,
    PT_LOAD=1,
    PT_NOTE=4,
    PT_NULL=0,
    PT_PHDR=6,
    PT_SHLIB=5,
    PT_TLS=7
} Elf_ProgramHeaderType;

typedef struct Elf64_Dyn Elf64_Dyn, *PElf64_Dyn;

struct Elf64_Dyn {
    enum Elf64_DynTag d_tag;
    qword d_val;
};

typedef struct Elf64_Shdr Elf64_Shdr, *PElf64_Shdr;

typedef enum Elf_SectionHeaderType {
    SHT_CHECKSUM=1879048184,
    SHT_DYNAMIC=6,
    SHT_DYNSYM=11,
    SHT_FINI_ARRAY=15,
    SHT_GNU_ATTRIBUTES=1879048181,
    SHT_GNU_HASH=1879048182,
    SHT_GNU_LIBLIST=1879048183,
    SHT_GNU_verdef=1879048189,
    SHT_GNU_verneed=1879048190,
    SHT_GNU_versym=1879048191,
    SHT_GROUP=17,
    SHT_HASH=5,
    SHT_INIT_ARRAY=14,
    SHT_NOBITS=8,
    SHT_NOTE=7,
    SHT_NULL=0,
    SHT_PREINIT_ARRAY=16,
    SHT_PROGBITS=1,
    SHT_REL=9,
    SHT_RELA=4,
    SHT_SHLIB=10,
    SHT_STRTAB=3,
    SHT_SUNW_COMDAT=1879048187,
    SHT_SUNW_move=1879048186,
    SHT_SUNW_syminfo=1879048188,
    SHT_SYMTAB=2,
    SHT_SYMTAB_SHNDX=18
} Elf_SectionHeaderType;

struct Elf64_Shdr {
    dword sh_name;
    enum Elf_SectionHeaderType sh_type;
    qword sh_flags;
    qword sh_addr;
    qword sh_offset;
    qword sh_size;
    dword sh_link;
    dword sh_info;
    qword sh_addralign;
    qword sh_entsize;
};

typedef struct Elf64_Phdr Elf64_Phdr, *PElf64_Phdr;

struct Elf64_Phdr {
    enum Elf_ProgramHeaderType p_type;
    dword p_flags;
    qword p_offset;
    qword p_vaddr;
    qword p_paddr;
    qword p_filesz;
    qword p_memsz;
    qword p_align;
};

typedef struct Elf64_Sym Elf64_Sym, *PElf64_Sym;

struct Elf64_Sym {
    dword st_name;
    byte st_info;
    byte st_other;
    word st_shndx;
    qword st_value;
    qword st_size;
};

typedef struct Elf64_Rela Elf64_Rela, *PElf64_Rela;

struct Elf64_Rela {
    qword r_offset; // location to apply the relocation action
    qword r_info; // the symbol table index and the type of relocation
    qword r_addend; // a constant addend used to compute the relocatable field value
};

typedef struct Elf64_Ehdr Elf64_Ehdr, *PElf64_Ehdr;

struct Elf64_Ehdr {
    byte e_ident_magic_num;
    char e_ident_magic_str[3];
    byte e_ident_class;
    byte e_ident_data;
    byte e_ident_version;
    byte e_ident_pad[9];
    word e_type;
    word e_machine;
    dword e_version;
    qword e_entry;
    qword e_phoff;
    qword e_shoff;
    dword e_flags;
    word e_ehsize;
    word e_phentsize;
    word e_phnum;
    word e_shentsize;
    word e_shnum;
    word e_shstrndx;
};




undefined8 _DT_INIT(void);
void FUN_004023d0(void);
void thunk_FUN_0062e078(void);
void FUN_004029a0(void);
void FUN_00402bb0(void);
ulong FUN_00402be0(ulong uParm1,undefined1 *puParm2);
void entry(void);
void FUN_00404380(undefined8 *puParm1);
void FUN_004043b0(void);
void FUN_00404430(void);
void FUN_004044b0(void);
ulong FUN_004044f0(ulong *puParm1,ulong uParm2);
ulong FUN_00404500(long *plParm1,long *plParm2);
void FUN_00404520(int iParm1);
void FUN_00404540(void);
void thunk_FUN_00402bb0(void);
void FUN_00404620(ulong uParm1,ulong uParm2);
ulong FUN_004046b0(byte **ppbParm1,byte **ppbParm2,char cParm3,long *plParm4);
ulong FUN_004049c0(uint uParm1);
undefined8 FUN_00404a30(undefined8 uParm1,long lParm2);
undefined8 FUN_00404ad0(undefined8 uParm1);
void FUN_00404b30(void);
void FUN_00404d40(long lParm1,long lParm2,undefined uParm3);
void FUN_00404db0(undefined8 *puParm1,undefined8 *puParm2);
void FUN_00404dc0(undefined8 *puParm1,undefined8 *puParm2);
void FUN_00404dd0(long lParm1,undefined8 uParm2,ulong uParm3);
ulong FUN_00404e60(char *pcParm1);
byte * FUN_00404ec0(long lParm1,char cParm2);
void FUN_00404f80(ulong uParm1,uint uParm2,char cParm3);
void FUN_00404fc0(void);
void FUN_00405160(undefined8 *puParm1,undefined8 *puParm2);
void FUN_00405170(undefined8 *puParm1,undefined8 *puParm2);
void FUN_00405180(void);
void FUN_004051a0(char cParm1,undefined8 uParm2,undefined8 uParm3);
ulong FUN_00405210(undefined8 *puParm1,undefined8 *puParm2);
ulong FUN_00405290(undefined8 *puParm1,undefined8 *puParm2);
ulong FUN_00405300(undefined8 *puParm1,undefined8 *puParm2);
undefined8 FUN_00405370(undefined8 *puParm1,undefined8 *puParm2);
undefined8 FUN_004053d0(undefined8 *puParm1,undefined8 *puParm2);
ulong FUN_00405430(char cParm1,ulong uParm2,int iParm3);
uint FUN_00405510(byte bParm1);
undefined8 FUN_00405540(undefined8 *puParm1,undefined8 *puParm2);
undefined8 FUN_004055a0(undefined8 *puParm1,undefined8 *puParm2);
undefined8 FUN_00405600(undefined8 *puParm1,undefined8 *puParm2);
void FUN_004056b0(char cParm1);
void FUN_00405890(void);
void FUN_004058d0(undefined8 *puParm1);
void FUN_00405930(void);
void FUN_004059e0(void);
byte * FUN_00405a40(byte **ppbParm1,byte *pbParm2,undefined8 uParm3,int iParm4,byte **ppbParm5,char *pcParm6);
long FUN_00405ea0(char *param_1,undefined8 param_2,ulong param_3,long param_4,char param_5,long param_6,long param_7);
long FUN_004061c0(long *plParm1,byte bParm2,undefined8 uParm3,ulong uParm4);
void FUN_00406640(long lParm1);
long FUN_00406f50(long *plParm1);
ulong FUN_00407110(char cParm1,ulong uParm2);
undefined8 FUN_004073d0(undefined8 *puParm1,undefined8 *puParm2);
undefined8 FUN_00407440(undefined8 *puParm1,undefined8 *puParm2);
undefined8 FUN_00407480(undefined8 *puParm1,undefined8 *puParm2);
undefined8 FUN_004074c0(undefined8 *puParm1,undefined8 *puParm2);
ulong FUN_00407530(undefined8 uParm1,undefined8 uParm2);
void FUN_004075c0(undefined8 *puParm1,undefined8 *puParm2);
void FUN_004075d0(undefined8 *puParm1,undefined8 *puParm2);
undefined8 FUN_004075e0(undefined8 *puParm1,undefined8 *puParm2);
undefined8 FUN_00407640(undefined8 *puParm1,undefined8 *puParm2);
undefined8 FUN_004076a0(undefined8 *puParm1,undefined8 *puParm2);
undefined8 FUN_004076e0(undefined8 *puParm1,undefined8 *puParm2);
undefined8 FUN_00407720(undefined8 *puParm1,undefined8 *puParm2);
undefined8 FUN_00407790(undefined8 *puParm1,undefined8 *puParm2);
long FUN_00407800(long lParm1,undefined8 uParm2);
void FUN_00407950(char cParm1);
void FUN_00407a70(void);
undefined8 FUN_00407d80(ulong uParm1);
undefined8 FUN_00407de0(undefined8 ******ppppppuParm1,int iParm2,byte bParm3,char *pcParm4);
ulong FUN_00408ae0(undefined8 *puParm1,undefined8 *puParm2);
ulong FUN_00408b30(undefined8 *puParm1,undefined8 *puParm2);
ulong FUN_00408b80(undefined8 *puParm1,undefined8 *puParm2);
ulong FUN_00408bc0(undefined8 *puParm1,undefined8 *puParm2);
ulong FUN_00408c10(undefined8 *puParm1,undefined8 *puParm2);
ulong FUN_00408c50(undefined8 *puParm1,undefined8 *puParm2);
ulong FUN_00408ca0(undefined8 *puParm1,undefined8 *puParm2);
ulong FUN_00408d20(undefined8 *puParm1,undefined8 *puParm2);
ulong FUN_00408d90(undefined8 *puParm1,undefined8 *puParm2);
ulong FUN_00408e00(undefined8 *puParm1,undefined8 *puParm2);
ulong FUN_00408e70(undefined8 *puParm1,undefined8 *puParm2);
ulong FUN_00408ee0(undefined8 *puParm1,undefined8 *puParm2);
void FUN_00408f60(undefined8 *puParm1,undefined8 *puParm2);
void FUN_00408fd0(undefined8 *puParm1,undefined8 *puParm2);
undefined8 FUN_00409040(undefined8 *puParm1,undefined8 *puParm2);
undefined8 FUN_004090f0(undefined8 *puParm1,undefined8 *puParm2);
ulong FUN_004091a0(undefined8 *puParm1,undefined8 *puParm2);
ulong FUN_004091f0(undefined8 *puParm1,undefined8 *puParm2);
ulong FUN_00409230(undefined8 *puParm1,undefined8 *puParm2);
ulong FUN_00409280(undefined8 *puParm1,undefined8 *puParm2);
ulong FUN_004092d0(undefined8 *puParm1,undefined8 *puParm2);
ulong FUN_00409310(undefined8 *puParm1,undefined8 *puParm2);
ulong FUN_00409360(undefined8 *puParm1,undefined8 *puParm2);
ulong FUN_004093d0(undefined8 *puParm1,undefined8 *puParm2);
ulong FUN_00409440(undefined8 *puParm1,undefined8 *puParm2);
void FUN_004094c0(undefined8 *puParm1,undefined8 *puParm2);
void FUN_00409530(undefined8 *puParm1,undefined8 *puParm2);
undefined8 FUN_004095a0(undefined8 *puParm1,undefined8 *puParm2);
void FUN_00409650(long lParm1,byte bParm2);
void FUN_004097c0(long lParm1,long lParm2,byte bParm3);
void FUN_00409d30(uint uParm1);
long FUN_0040a0b0(undefined8 uParm1,ulong uParm2);
void FUN_0040a1d0(void);
long FUN_0040a1e0(undefined8 uParm1,long *plParm2,long lParm3,long lParm4);
void FUN_0040a310(undefined8 uParm1,undefined8 uParm2,long lParm3);
void FUN_0040a360(long *plParm1,long lParm2,long lParm3);
long FUN_0040a430(undefined8 uParm1,undefined8 uParm2,undefined8 uParm3,undefined8 uParm4,undefined8 uParm5,code *pcParm6);
char * FUN_0040a4a0(char *pcParm1,uint uParm2);
void FUN_0040aaa0(void);
ulong FUN_0040ab30(char *pcParm1);
char * thunk_FUN_0040ab9c(char *pcParm1);
char * FUN_0040ab9c(char *pcParm1);
void FUN_0040abe0(long lParm1);
undefined8 FUN_0040ac10(void);
void FUN_0040ac20(long lParm1,undefined8 uParm2,undefined8 *puParm3);
ulong FUN_0040acb0(long lParm1,undefined8 uParm2,undefined8 *puParm3);
void FUN_0040acf0(ulong uParm1,undefined *puParm2);
void FUN_0040aec0(long lParm1);
void FUN_0040aed0(void);
long FUN_0040aef0(long lParm1,char *pcParm2,undefined8 *puParm3);
char * FUN_0040aff0(char **ppcParm1);
ulong FUN_0040b0c0(byte bParm1);
ulong FUN_0040b110(char *pcParm1,char *pcParm2);
void FUN_0040b3d0(undefined8 *puParm1);
ulong FUN_0040b410(ulong uParm1);
ulong FUN_0040b4b0(ulong uParm1);
ulong FUN_0040b550(ulong uParm1,ulong uParm2);
undefined FUN_0040b560(long lParm1,long lParm2);;
long FUN_0040b570(long *plParm1,undefined8 uParm2);
long FUN_0040b5a0(long lParm1,long lParm2,long **pplParm3,char cParm4);
ulong FUN_0040b6a0(float **ppfParm1,undefined8 uParm2,uint uParm3);
undefined8 FUN_0040b730(long lParm1,long **pplParm2,char cParm3);
undefined8 FUN_0040b880(long lParm1);
long FUN_0040b890(long lParm1,long lParm2);
long * FUN_0040b8f0(ulong uParm1,undefined1 *puParm2,code *pcParm3,code *pcParm4,long lParm5);
void FUN_0040baa0(long **pplParm1);
ulong FUN_0040bb60(long *plParm1,ulong uParm2);
undefined8 FUN_0040bd60(long lParm1,long lParm2,long *plParm3);
undefined8 FUN_0040bfd0(undefined8 uParm1,undefined8 uParm2);
long FUN_0040c010(long lParm1,undefined8 uParm2);
ulong FUN_0040c210(undefined8 *puParm1,ulong uParm2);
ulong FUN_0040c240(undefined8 *puParm1,undefined8 *puParm2);
void FUN_0040c280(undefined8 *puParm1);
char * FUN_0040c2a0(ulong uParm1,long lParm2,ulong uParm3,ulong uParm4,ulong uParm5);
undefined8 FUN_0040ccd0(char *pcParm1,uint *puParm2,long *plParm3);
int * FUN_0040ce70(int iParm1);
int * FUN_0040cf10(int iParm1);
char * FUN_0040cfb0(long lParm1,long lParm2);
char * FUN_0040d050(ulong uParm1,long lParm2);
undefined *FUN_0040d0a0(long lParm1,undefined *puParm2,long lParm3,undefined8 *puParm4,int iParm5,ulong uParm6);
ulong FUN_0040d4a0(byte *pbParm1,long lParm2,ulong uParm3);
void FUN_0040d670(undefined8 uParm1,ulong uParm2);
void FUN_0040d6a0(undefined8 *puParm1,ulong uParm2,undefined8 *puParm3,code *pcParm4);
void FUN_0040d8e0(long lParm1,long lParm2,undefined8 uParm3);
long FUN_0040d8f0(long lParm1,long lParm2,long lParm3);
char * FUN_0040d930(char *param_1,long param_2,char *param_3,undefined8 *param_4,byte param_5,undefined8 param_6,undefined8 param_7,uint param_8);
void FUN_0040f2d0(void);
void FUN_0040f2f0(long lParm1);
undefined8 * FUN_0040f390(undefined8 *puParm1,int iParm2);
undefined * FUN_0040f400(char *pcParm1,int iParm2);
ulong FUN_0040f4d0(undefined *param_1,ulong param_2,long param_3,ulong param_4,uint param_5,ulong param_6,long param_7,char *param_8,char *param_9);
undefined1 * FUN_004103d0(uint uParm1,undefined8 uParm2,undefined8 uParm3,uint *puParm4);
undefined8 FUN_00410580(undefined1 *puParm1);
ulong FUN_004105c0(undefined1 *puParm1);
void FUN_004105d0(undefined1 *puParm1,undefined4 uParm2);
ulong FUN_004105e0(undefined1 *puParm1,byte bParm2,undefined8 uParm3);
undefined8 FUN_00410620(undefined8 uParm1,undefined8 uParm2,undefined8 uParm3,undefined8 uParm4,undefined1 *puParm5);
void FUN_004106a0(uint uParm1,undefined8 uParm2,undefined8 uParm3);
void FUN_004106d0(ulong uParm1,undefined8 uParm2);
void FUN_004106e0(undefined8 uParm1,undefined8 uParm2,byte bParm3);
void FUN_00410780(undefined8 uParm1);
void FUN_004107a0(uint uParm1,undefined8 uParm2,undefined8 uParm3);
void FUN_00410830(undefined8 uParm1,undefined8 uParm2);
void FUN_00410850(undefined8 uParm1);
long FUN_00410870(undefined8 uParm1,long lParm2,undefined8 uParm3,undefined8 uParm4,long *plParm5,undefined8 uParm6);
void FUN_00410ad0(void);
void FUN_00410b40(void);
void FUN_00410bd0(long lParm1);
long FUN_00410bf0(long lParm1,long lParm2);
void FUN_00410c30(long lParm1,ulong *puParm2);
void FUN_00410c90(undefined8 uParm1,undefined8 uParm2);
void FUN_00410cc0(undefined8 uParm1);
void FUN_00410ce0(void);
ulong FUN_00410d10(undefined8 param_1,ulong param_2,ulong param_3,ulong param_4,undefined8 param_5,undefined8 param_6,uint param_7);
long FUN_00410df0(void);
long FUN_00410e20(void);
ulong FUN_00410ed0(byte *pbParm1,byte **ppbParm2,uint uParm3,ulong *puParm4,long lParm5);
void FUN_00411330(uint uParm1,int iParm2,undefined uParm3,long lParm4,undefined8 uParm5);
ulong FUN_004113b0(byte *pbParm1,byte **ppbParm2,uint uParm3,ulong *puParm4,long lParm5);
undefined8 FUN_00411810(ulong uParm1,ulong uParm2);
void FUN_00411860(undefined8 uParm1);
void FUN_004118a0(undefined8 uParm1,uint uParm2,undefined8 uParm3,undefined8 uParm4);
void FUN_00411910(void);
void FUN_00411950(undefined4 uParm1,undefined4 uParm2,undefined4 uParm3,undefined4 uParm4,undefined4 uParm5,undefined4 uParm6,undefined4 uParm7,undefined4 uParm8,ulong param_1,ulong param_2,undefined8 param_3,undefined8 param_4,undefined8 param_5,undefined8 param_6);
int * FUN_00411a30(int *piParm1);
char * FUN_00411b30(char *pcParm1);
undefined8 FUN_00411c40(int iParm1,long lParm2,ulong uParm3,ulong uParm4,byte bParm5,uint uParm6);
ulong FUN_004122c0(uint *puParm1,uint *puParm2,uint *puParm3,byte bParm4,uint uParm5);
undefined8 FUN_00412de0(int iParm1,long lParm2,ulong uParm3,ulong uParm4,byte bParm5,uint uParm6);
ulong FUN_004133e0(byte *pbParm1,byte *pbParm2,byte *pbParm3,byte bParm4,uint uParm5);
ulong FUN_00413e70(undefined8 uParm1,long lParm2,ulong uParm3);
long FUN_004140f0(long lParm1,ulong uParm2);
void FUN_00414570(long lParm1,int *piParm2);
ulong FUN_00414650(int param_1,undefined8 *param_2,char *param_3,long *param_4,int *param_5,int param_6,int *param_7,int param_8,undefined8 param_9);
ulong FUN_00414bd0(uint param_1,undefined8 *param_2,char *param_3,long param_4,undefined8 param_5,ulong param_6,uint *param_7,int param_8);
void FUN_00415120(void);
void FUN_00415180(void);
void FUN_004151a0(void);
undefined8 FUN_00415260(long lParm1,long lParm2);
void FUN_004152e0(long lParm1);
ulong FUN_00415300(uint *puParm1,byte *pbParm2,long lParm3);
ulong * FUN_00415370(ulong *puParm1,char cParm2,ulong uParm3);
void FUN_00415460(void);
void FUN_00415490(long lParm1,undefined8 uParm2);
void FUN_004154c0(long lParm1,undefined8 uParm2);
undefined8 FUN_004154f0(long *plParm1,long lParm2,long lParm3,long lParm4,long lParm5);
void FUN_00415580(long lParm1,undefined8 uParm2,undefined8 uParm3,undefined8 uParm4,undefined8 uParm5);
void FUN_004155a0(ulong *puParm1,long lParm2);
ulong FUN_00415690(uint param_1,undefined8 param_2,uint param_3,ulong param_4);
undefined8 FUN_004157b0(long lParm1,undefined8 uParm2,undefined8 uParm3);
void FUN_00415820(long lParm1,long lParm2);
ulong FUN_00415860(undefined4 uParm1,undefined4 uParm2,undefined4 uParm3,undefined4 uParm4,undefined4 uParm5,undefined4 uParm6,undefined4 uParm7,undefined4 uParm8,long param_1,ulong param_2,undefined8 param_3,undefined8 param_4,undefined8 param_5,undefined8 param_6);
ulong FUN_00415980(long lParm1,long lParm2);
char * FUN_004159d0(char *pcParm1,char *pcParm2,ulong uParm3);
ulong FUN_00415ad0(long lParm1);
ulong FUN_00415b20(undefined8 *puParm1);
undefined8 * FUN_00415ba0(long lParm1);
undefined8 FUN_00415c40(long *plParm1,char *pcParm2);
long * FUN_00415dc0(long lParm1);
undefined8 FUN_00415e80(long lParm1,undefined8 uParm2,undefined8 uParm3);
long FUN_00415f10(long lParm1,uint *puParm2);
void FUN_00416040(long lParm1);
void FUN_00416060(void);
ulong FUN_00416110(char *pcParm1);
undefined4 * FUN_00416180(ulong uParm1,uint *puParm2,ulong uParm3,uint *puParm4,long *plParm5);
ulong FUN_004162e0(void);
uint * FUN_00416550(ulong uParm1,long lParm2,ulong uParm3,uint *puParm4,ulong *puParm5);
char * FUN_00416b10(int iParm1,long lParm2,long lParm3,long lParm4,uint uParm5);
void FUN_004170b0(uint param_1);
ulong FUN_00417230(void);
void FUN_00417450(undefined8 uParm1,uint uParm2);
undefined8 *FUN_004175c0(undefined8 *puParm1,undefined8 *puParm2,undefined8 *puParm3,undefined8 uParm4);
ulong FUN_0041cb90(undefined8 uParm1,undefined8 uParm2,undefined8 uParm3);
ulong FUN_0041cc60(int *piParm1,long lParm2);
void FUN_0041cce0(ulong uParm1);
ulong FUN_0041cd30(ulong uParm1,char cParm2);
ulong FUN_0041cd90(undefined8 uParm1);
undefined8 FUN_0041ce00(void);
ulong FUN_0041ce10(char *pcParm1,ulong uParm2);
char * FUN_0041ce40(void);
double FUN_0041d190(int *piParm1);
void FUN_0041d1d0(uint *param_1);
ulong FUN_0041d250(long param_1,long param_2,char param_3,char param_4,char param_5,char param_6,char param_7);
undefined8 FUN_0041d2d0(long param_1,undefined8 param_2,char param_3,char param_4,char param_5,char param_6,char param_7);
ulong FUN_0041d330(uint uParm1,byte *pbParm2,undefined8 uParm3,undefined8 uParm4,undefined8 uParm5);
void FUN_0041d740(undefined8 uParm1);
ulong FUN_0041d7d0(ulong param_1,undefined8 param_2,ulong param_3);
undefined8 FUN_0041d910(ulong uParm1);
ulong FUN_0041d980(long lParm1);
undefined8 FUN_0041da10(void);
void FUN_0041da20(void);
undefined8 FUN_0041da30(ulong uParm1,long lParm2,long lParm3,ulong uParm4);
long FUN_0041daf0(ulong param_1,long param_2,int param_3,int param_4,int param_5,uint param_6,int param_7,int param_8,int param_9,int param_10);
void FUN_0041dc20(code *pcParm1,long *plParm2,undefined8 uParm3);
long FUN_0041dcd0(void);
long FUN_0041dd80(undefined8 *puParm1,code *pcParm2,long *plParm3);
void FUN_0041e2c0(undefined8 uParm1);
undefined8 FUN_0041e2e0(uint *puParm1,ulong *puParm2);
undefined8 FUN_0041e500(byte *pbParm1,ulong *puParm2,byte **ppbParm3);
ulong FUN_0041f150(char *pcParm1,char *pcParm2,ulong uParm3);
ulong FUN_0041f280(void);
ulong FUN_0041f2b0(void);
undefined8 * FUN_0041f2f0(ulong uParm1);
void FUN_0041f370(ulong uParm1);
void FUN_0041f400(void);
undefined8 _DT_FINI(void);
undefined FUN_0062e000();
undefined FUN_0062e008();
undefined FUN_0062e010();
undefined FUN_0062e018();
undefined FUN_0062e020();
undefined FUN_0062e028();
undefined FUN_0062e030();
undefined FUN_0062e038();
undefined FUN_0062e040();
undefined FUN_0062e048();
undefined FUN_0062e050();
undefined FUN_0062e058();
undefined FUN_0062e060();
undefined FUN_0062e068();
undefined FUN_0062e070();
undefined FUN_0062e078();
undefined FUN_0062e080();
undefined FUN_0062e088();
undefined FUN_0062e090();
undefined FUN_0062e098();
undefined FUN_0062e0a0();
undefined FUN_0062e0a8();
undefined FUN_0062e0b0();
undefined FUN_0062e0b8();
undefined FUN_0062e0c0();
undefined FUN_0062e0c8();
undefined FUN_0062e0d0();
undefined FUN_0062e0d8();
undefined FUN_0062e0e0();
undefined FUN_0062e0e8();
undefined FUN_0062e0f0();
undefined FUN_0062e0f8();
undefined FUN_0062e100();
undefined FUN_0062e108();
undefined FUN_0062e110();
undefined FUN_0062e118();
undefined FUN_0062e120();
undefined FUN_0062e128();
undefined FUN_0062e130();
undefined FUN_0062e138();
undefined FUN_0062e140();
undefined FUN_0062e148();
undefined FUN_0062e150();
undefined FUN_0062e158();
undefined FUN_0062e160();
undefined FUN_0062e168();
undefined FUN_0062e170();
undefined FUN_0062e178();
undefined FUN_0062e180();
undefined FUN_0062e188();
undefined FUN_0062e190();
undefined FUN_0062e198();
undefined FUN_0062e1a0();
undefined FUN_0062e1a8();
undefined FUN_0062e1b0();
undefined FUN_0062e1b8();
undefined FUN_0062e1c0();
undefined FUN_0062e1c8();
undefined FUN_0062e1d0();
undefined FUN_0062e1d8();
undefined FUN_0062e1e0();
undefined FUN_0062e1e8();
undefined FUN_0062e1f0();
undefined FUN_0062e1f8();
undefined FUN_0062e200();
undefined FUN_0062e208();
undefined FUN_0062e210();
undefined FUN_0062e218();
undefined FUN_0062e220();
undefined FUN_0062e228();
undefined FUN_0062e230();
undefined FUN_0062e238();
undefined FUN_0062e240();
undefined FUN_0062e248();
undefined FUN_0062e250();
undefined FUN_0062e258();
undefined FUN_0062e260();
undefined FUN_0062e268();
undefined FUN_0062e270();
undefined FUN_0062e278();
undefined FUN_0062e280();
undefined FUN_0062e288();
undefined FUN_0062e290();
undefined FUN_0062e298();
undefined FUN_0062e2a0();
undefined FUN_0062e2a8();
undefined FUN_0062e2b0();
undefined FUN_0062e2b8();
undefined FUN_0062e2c0();
undefined FUN_0062e2c8();
undefined FUN_0062e2d0();
undefined FUN_0062e2d8();
undefined FUN_0062e2e0();
undefined FUN_0062e2e8();
undefined FUN_0062e2f0();
undefined FUN_0062e2f8();
undefined FUN_0062e300();
undefined FUN_0062e308();
undefined FUN_0062e310();
undefined FUN_0062e318();
undefined FUN_0062e320();
undefined FUN_0062e328();
undefined FUN_0062e330();
undefined FUN_0062e338();
undefined FUN_0062e340();
undefined FUN_0062e348();
undefined FUN_0062e350();
undefined FUN_0062e358();
undefined FUN_0062e360();
undefined FUN_0062e368();
undefined FUN_0062e370();
undefined FUN_0062e378();
undefined FUN_0062e380();
undefined FUN_0062e388();
undefined FUN_0062e390();
undefined FUN_0062e398();
undefined FUN_0062e3a0();
undefined FUN_0062e3a8();
undefined FUN_0062e3b0();
undefined FUN_0062e3b8();
undefined FUN_0062e3c0();
undefined FUN_0062e3c8();
undefined FUN_0062e3d0();
undefined FUN_0062e3d8();
undefined FUN_0062e3e0();


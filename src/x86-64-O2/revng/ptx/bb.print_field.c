typedef unsigned char __u_char;
typedef unsigned short __u_short;
typedef unsigned int __u_int;
typedef unsigned long __u_long;
typedef signed char __int8_t;
typedef unsigned char __uint8_t;
typedef short __int16_t;
typedef unsigned short __uint16_t;
typedef int __int32_t;
typedef unsigned int __uint32_t;
typedef long __int64_t;
typedef unsigned long __uint64_t;
typedef __int8_t __int_least8_t;
typedef __uint8_t __uint_least8_t;
typedef __int16_t __int_least16_t;
typedef __uint16_t __uint_least16_t;
typedef __int32_t __int_least32_t;
typedef __uint32_t __uint_least32_t;
typedef __int64_t __int_least64_t;
typedef __uint64_t __uint_least64_t;
typedef long __quad_t;
typedef unsigned long __u_quad_t;
typedef long __intmax_t;
typedef unsigned long __uintmax_t;
typedef unsigned long __dev_t;
typedef unsigned int __uid_t;
typedef unsigned int __gid_t;
typedef unsigned long __ino_t;
typedef unsigned long __ino64_t;
typedef unsigned int __mode_t;
typedef unsigned long __nlink_t;
typedef long __off_t;
typedef long __off64_t;
typedef int __pid_t;
typedef struct {
    int __val[2];
} __fsid_t;
typedef long __clock_t;
typedef unsigned long __rlim_t;
typedef unsigned long __rlim64_t;
typedef unsigned int __id_t;
typedef long __time_t;
typedef unsigned int __useconds_t;
typedef long __suseconds_t;
typedef int __daddr_t;
typedef int __key_t;
typedef int __clockid_t;
typedef void *__timer_t;
typedef long __blksize_t;
typedef long __blkcnt_t;
typedef long __blkcnt64_t;
typedef unsigned long __fsblkcnt_t;
typedef unsigned long __fsblkcnt64_t;
typedef unsigned long __fsfilcnt_t;
typedef unsigned long __fsfilcnt64_t;
typedef long __fsword_t;
typedef long __ssize_t;
typedef long __syscall_slong_t;
typedef unsigned long __syscall_ulong_t;
typedef __off64_t __loff_t;
typedef char *__caddr_t;
typedef long __intptr_t;
typedef unsigned int __socklen_t;
typedef int __sig_atomic_t;
typedef __int8_t int8_t;
typedef __int16_t int16_t;
typedef __int32_t int32_t;
typedef __int64_t int64_t;
typedef __uint8_t uint8_t;
typedef __uint16_t uint16_t;
typedef __uint32_t uint32_t;
typedef __uint64_t uint64_t;
typedef __int_least8_t int_least8_t;
typedef __int_least16_t int_least16_t;
typedef __int_least32_t int_least32_t;
typedef __int_least64_t int_least64_t;
typedef __uint_least8_t uint_least8_t;
typedef __uint_least16_t uint_least16_t;
typedef __uint_least32_t uint_least32_t;
typedef __uint_least64_t uint_least64_t;
typedef signed char int_fast8_t;
typedef long int_fast16_t;
typedef long int_fast32_t;
typedef long int_fast64_t;
typedef unsigned char uint_fast8_t;
typedef unsigned long uint_fast16_t;
typedef unsigned long uint_fast32_t;
typedef unsigned long uint_fast64_t;
typedef long intptr_t;
typedef unsigned long uintptr_t;
typedef __intmax_t intmax_t;
typedef __uintmax_t uintmax_t;
extern void abort(void);
extern uint64_t revng_init_local_sp(uint64_t param_0);
extern uint64_t init_rbx(void);
extern uint64_t init_rbp(void);
extern void indirect_placeholder_1(void);
extern uint64_t init_cc_src2(void);
extern uint64_t helper_cc_compute_c_wrapper(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint32_t param_3);
extern uint64_t init_r12(void);
extern uint64_t helper_cc_compute_all_wrapper(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint32_t param_3);
void bb_print_field(uint64_t rdi, uint64_t rsi) {
    uint64_t var_0;
    uint64_t var_1;
    uint64_t var_2;
    uint64_t var_3;
    uint64_t var_4;
    uint64_t var_5;
    uint64_t rbx_0;
    uint64_t rbx_0_be;
    uint64_t local_sp_0_be;
    uint64_t local_sp_0;
    unsigned char var_6;
    uint64_t var_7;
    uint64_t var_36;
    uint64_t var_37;
    unsigned char var_8;
    uint64_t var_9;
    unsigned char var_10;
    uint64_t var_28;
    uint64_t var_11;
    uint64_t var_12;
    uint64_t var_14;
    uint64_t var_15;
    uint64_t var_13;
    uint64_t var_16;
    uint64_t var_17;
    uint64_t var_18;
    uint64_t var_19;
    uint64_t var_20;
    uint64_t var_21;
    uint64_t var_22;
    uint64_t var_23;
    uint64_t var_24;
    uint64_t var_25;
    uint64_t local_sp_1;
    uint64_t var_35;
    uint64_t var_26;
    uint64_t var_27;
    uint64_t var_29;
    uint64_t var_30;
    uint64_t var_31;
    uint64_t var_32;
    uint64_t var_33;
    uint64_t var_34;
    var_0 = revng_init_local_sp(0UL);
    var_1 = init_rbx();
    var_2 = init_rbp();
    var_3 = init_cc_src2();
    var_4 = init_r12();
    *(uint64_t *)(var_0 + (-8L)) = var_4;
    *(uint64_t *)(var_0 + (-16L)) = var_2;
    var_5 = var_0 + (-24L);
    *(uint64_t *)var_5 = var_1;
    rbx_0 = rdi;
    local_sp_0 = var_5;
    if (rdi < rsi) {
        return;
    }
    while (1U)
        {
            var_6 = *(unsigned char *)rbx_0;
            var_7 = (uint64_t)var_6;
            local_sp_1 = local_sp_0;
            if (*(unsigned char *)(var_7 + 6457024UL) == '\x00') {
                var_36 = rbx_0 + 1UL;
                var_37 = local_sp_0 + (-8L);
                *(uint64_t *)var_37 = 4213759UL;
                indirect_placeholder_1();
                rbx_0_be = var_36;
                local_sp_0_be = var_37;
                if (var_36 == rsi) {
                    break;
                }
            }
            var_8 = *(unsigned char *)(var_7 + 4335744UL);
            var_9 = (uint64_t)var_8;
            if (var_8 == '\x00') {
                var_26 = var_7 + (-92L);
                if ((uint64_t)(unsigned char)var_26 == 0UL) {
                    var_34 = local_sp_0 + (-8L);
                    *(uint64_t *)var_34 = 4213825UL;
                    indirect_placeholder_1();
                    local_sp_1 = var_34;
                } else {
                    var_27 = helper_cc_compute_all_wrapper(var_26, 92UL, var_3, 14U);
                    if ((var_27 & 65UL) == 0UL) {
                        if ((uint64_t)(var_6 + '\x85') != 0UL) {
                            var_33 = local_sp_0 + (-8L);
                            *(uint64_t *)var_33 = 4213878UL;
                            indirect_placeholder_1();
                            local_sp_1 = var_33;
                            var_35 = rbx_0 + 1UL;
                            rbx_0_be = var_35;
                            local_sp_0_be = local_sp_1;
                            if (var_35 == rsi) {
                                break;
                            }
                            rbx_0 = rbx_0_be;
                            local_sp_0 = local_sp_0_be;
                            continue;
                        }
                        if ((uint64_t)(var_6 + '\x83') != 0UL) {
                            var_33 = local_sp_0 + (-8L);
                            *(uint64_t *)var_33 = 4213878UL;
                            indirect_placeholder_1();
                            local_sp_1 = var_33;
                            var_35 = rbx_0 + 1UL;
                            rbx_0_be = var_35;
                            local_sp_0_be = local_sp_1;
                            if (var_35 == rsi) {
                                break;
                            }
                            rbx_0 = rbx_0_be;
                            local_sp_0 = local_sp_0_be;
                            continue;
                        }
                        if ((uint64_t)(var_6 + '\xa1') != 0UL) {
                            var_31 = local_sp_0 + (-8L);
                            *(uint64_t *)var_31 = 4213798UL;
                            indirect_placeholder_1();
                            local_sp_1 = var_31;
                            var_35 = rbx_0 + 1UL;
                            rbx_0_be = var_35;
                            local_sp_0_be = local_sp_1;
                            if (var_35 == rsi) {
                                break;
                            }
                            rbx_0 = rbx_0_be;
                            local_sp_0 = local_sp_0_be;
                            continue;
                        }
                    }
                    var_28 = var_7 + (-34L);
                    if ((uint64_t)(unsigned char)var_28 != 0UL) {
                        *(uint64_t *)(local_sp_0 + (-8L)) = 4213842UL;
                        indirect_placeholder_1();
                        var_30 = local_sp_0 + (-16L);
                        *(uint64_t *)var_30 = 4213852UL;
                        indirect_placeholder_1();
                        local_sp_1 = var_30;
                        var_35 = rbx_0 + 1UL;
                        rbx_0_be = var_35;
                        local_sp_0_be = local_sp_1;
                        if (var_35 == rsi) {
                            break;
                        }
                        rbx_0 = rbx_0_be;
                        local_sp_0 = local_sp_0_be;
                        continue;
                    }
                    var_29 = helper_cc_compute_c_wrapper(var_28, 34UL, var_3, 14U);
                    if (!((var_29 != 0UL) || (var_6 > '&'))) {
                        var_31 = local_sp_0 + (-8L);
                        *(uint64_t *)var_31 = 4213798UL;
                        indirect_placeholder_1();
                        local_sp_1 = var_31;
                        var_35 = rbx_0 + 1UL;
                        rbx_0_be = var_35;
                        local_sp_0_be = local_sp_1;
                        if (var_35 == rsi) {
                            break;
                        }
                        rbx_0 = rbx_0_be;
                        local_sp_0 = local_sp_0_be;
                        continue;
                    }
                    *(uint64_t *)(local_sp_0 + (-8L)) = 4213710UL;
                    indirect_placeholder_1();
                    var_32 = local_sp_0 + (-16L);
                    *(uint64_t *)var_32 = 4213717UL;
                    indirect_placeholder_1();
                    local_sp_1 = var_32;
                }
            } else {
                if (*(uint32_t *)6458788UL != 3U) {
                    var_10 = *(unsigned char *)(var_7 + 4336000UL);
                    if (var_8 > '\t') {
                        var_35 = rbx_0 + 1UL;
                        rbx_0_be = var_35;
                        local_sp_0_be = local_sp_1;
                        if (var_35 == rsi) {
                            break;
                        }
                        rbx_0 = rbx_0_be;
                        local_sp_0 = local_sp_0_be;
                        continue;
                    }
                    switch (*(uint64_t *)((var_9 << 3UL) + 4334848UL)) {
                      case 4214062UL:
                        {
                            var_20 = local_sp_0 + (-8L);
                            *(uint64_t *)var_20 = 4214091UL;
                            indirect_placeholder_1();
                            local_sp_1 = var_20;
                        }
                        break;
                      case 4213994UL:
                        {
                            var_22 = local_sp_0 + (-8L);
                            *(uint64_t *)var_22 = 4214023UL;
                            indirect_placeholder_1();
                            local_sp_1 = var_22;
                        }
                        break;
                      case 4214028UL:
                        {
                            var_21 = local_sp_0 + (-8L);
                            *(uint64_t *)var_21 = 4214057UL;
                            indirect_placeholder_1();
                            local_sp_1 = var_21;
                        }
                        break;
                      case 4213920UL:
                        {
                            var_25 = local_sp_0 + (-8L);
                            *(uint64_t *)var_25 = 4213949UL;
                            indirect_placeholder_1();
                            local_sp_1 = var_25;
                        }
                        break;
                      case 4214096UL:
                        {
                            var_19 = local_sp_0 + (-8L);
                            *(uint64_t *)var_19 = 4214110UL;
                            indirect_placeholder_1();
                            local_sp_1 = var_19;
                        }
                        break;
                      case 4213954UL:
                        {
                            if ((uint64_t)(var_10 + '\xb1') == 0UL) {
                                var_24 = local_sp_0 + (-8L);
                                *(uint64_t *)var_24 = 4214259UL;
                                indirect_placeholder_1();
                                local_sp_1 = var_24;
                            } else {
                                if ((uint64_t)(var_10 + '\x91') == 0UL) {
                                    var_23 = local_sp_0 + (-8L);
                                    *(uint64_t *)var_23 = 4213989UL;
                                    indirect_placeholder_1();
                                    local_sp_1 = var_23;
                                } else {
                                    var_31 = local_sp_0 + (-8L);
                                    *(uint64_t *)var_31 = 4213798UL;
                                    indirect_placeholder_1();
                                    local_sp_1 = var_31;
                                }
                            }
                        }
                        break;
                      case 4214151UL:
                        {
                            var_11 = (uint64_t)(uint32_t)(uint64_t)var_10 + (-79L);
                            if ((uint64_t)(unsigned char)var_11 == 0UL) {
                                var_16 = local_sp_0 + (-8L);
                                *(uint64_t *)var_16 = 4214312UL;
                                indirect_placeholder_1();
                                local_sp_1 = var_16;
                            } else {
                                var_12 = helper_cc_compute_all_wrapper(var_11, 79UL, var_3, 14U);
                                if ((uint64_t)(((unsigned char)(var_12 >> 4UL) ^ (unsigned char)var_12) & '\xc0') == 0UL) {
                                    if ((uint64_t)(var_10 + '\x9f') == 0UL) {
                                        var_15 = local_sp_0 + (-8L);
                                        *(uint64_t *)var_15 = 4214215UL;
                                        indirect_placeholder_1();
                                        local_sp_1 = var_15;
                                    } else {
                                        if ((uint64_t)(var_10 + '\x91') == 0UL) {
                                            var_14 = local_sp_0 + (-8L);
                                            *(uint64_t *)var_14 = 4214193UL;
                                            indirect_placeholder_1();
                                            local_sp_1 = var_14;
                                        } else {
                                            var_31 = local_sp_0 + (-8L);
                                            *(uint64_t *)var_31 = 4213798UL;
                                            indirect_placeholder_1();
                                            local_sp_1 = var_31;
                                        }
                                    }
                                } else {
                                    if ((uint64_t)(var_10 + '\xbf') == 0UL) {
                                        var_13 = local_sp_0 + (-8L);
                                        *(uint64_t *)var_13 = 4214290UL;
                                        indirect_placeholder_1();
                                        local_sp_1 = var_13;
                                    } else {
                                        var_31 = local_sp_0 + (-8L);
                                        *(uint64_t *)var_31 = 4213798UL;
                                        indirect_placeholder_1();
                                        local_sp_1 = var_31;
                                    }
                                }
                            }
                        }
                        break;
                      case 4214115UL:
                        {
                            if ((uint64_t)(var_10 + '\xbf') == 0UL) {
                                var_18 = local_sp_0 + (-8L);
                                *(uint64_t *)var_18 = 4214237UL;
                                indirect_placeholder_1();
                                local_sp_1 = var_18;
                            } else {
                                if ((uint64_t)(var_10 + '\x9f') == 0UL) {
                                    var_17 = local_sp_0 + (-8L);
                                    *(uint64_t *)var_17 = 4214146UL;
                                    indirect_placeholder_1();
                                    local_sp_1 = var_17;
                                } else {
                                    var_31 = local_sp_0 + (-8L);
                                    *(uint64_t *)var_31 = 4213798UL;
                                    indirect_placeholder_1();
                                    local_sp_1 = var_31;
                                }
                            }
                        }
                        break;
                      default:
                        {
                            abort();
                        }
                        break;
                    }
                }
            }
        }
    return;
}

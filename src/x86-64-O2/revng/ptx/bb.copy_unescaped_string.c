typedef unsigned char __u_char;
typedef unsigned short __u_short;
typedef unsigned int __u_int;
typedef unsigned long __u_long;
typedef signed char __int8_t;
typedef unsigned char __uint8_t;
typedef short __int16_t;
typedef unsigned short __uint16_t;
typedef int __int32_t;
typedef unsigned int __uint32_t;
typedef long __int64_t;
typedef unsigned long __uint64_t;
typedef __int8_t __int_least8_t;
typedef __uint8_t __uint_least8_t;
typedef __int16_t __int_least16_t;
typedef __uint16_t __uint_least16_t;
typedef __int32_t __int_least32_t;
typedef __uint32_t __uint_least32_t;
typedef __int64_t __int_least64_t;
typedef __uint64_t __uint_least64_t;
typedef long __quad_t;
typedef unsigned long __u_quad_t;
typedef long __intmax_t;
typedef unsigned long __uintmax_t;
typedef unsigned long __dev_t;
typedef unsigned int __uid_t;
typedef unsigned int __gid_t;
typedef unsigned long __ino_t;
typedef unsigned long __ino64_t;
typedef unsigned int __mode_t;
typedef unsigned long __nlink_t;
typedef long __off_t;
typedef long __off64_t;
typedef int __pid_t;
typedef struct {
    int __val[2];
} __fsid_t;
typedef long __clock_t;
typedef unsigned long __rlim_t;
typedef unsigned long __rlim64_t;
typedef unsigned int __id_t;
typedef long __time_t;
typedef unsigned int __useconds_t;
typedef long __suseconds_t;
typedef int __daddr_t;
typedef int __key_t;
typedef int __clockid_t;
typedef void *__timer_t;
typedef long __blksize_t;
typedef long __blkcnt_t;
typedef long __blkcnt64_t;
typedef unsigned long __fsblkcnt_t;
typedef unsigned long __fsblkcnt64_t;
typedef unsigned long __fsfilcnt_t;
typedef unsigned long __fsfilcnt64_t;
typedef long __fsword_t;
typedef long __ssize_t;
typedef long __syscall_slong_t;
typedef unsigned long __syscall_ulong_t;
typedef __off64_t __loff_t;
typedef char *__caddr_t;
typedef long __intptr_t;
typedef unsigned int __socklen_t;
typedef int __sig_atomic_t;
typedef __int8_t int8_t;
typedef __int16_t int16_t;
typedef __int32_t int32_t;
typedef __int64_t int64_t;
typedef __uint8_t uint8_t;
typedef __uint16_t uint16_t;
typedef __uint32_t uint32_t;
typedef __uint64_t uint64_t;
typedef __int_least8_t int_least8_t;
typedef __int_least16_t int_least16_t;
typedef __int_least32_t int_least32_t;
typedef __int_least64_t int_least64_t;
typedef __uint_least8_t uint_least8_t;
typedef __uint_least16_t uint_least16_t;
typedef __uint_least32_t uint_least32_t;
typedef __uint_least64_t uint_least64_t;
typedef signed char int_fast8_t;
typedef long int_fast16_t;
typedef long int_fast32_t;
typedef long int_fast64_t;
typedef unsigned char uint_fast8_t;
typedef unsigned long uint_fast16_t;
typedef unsigned long uint_fast32_t;
typedef unsigned long uint_fast64_t;
typedef long intptr_t;
typedef unsigned long uintptr_t;
typedef __intmax_t intmax_t;
typedef __uintmax_t uintmax_t;
typedef _Bool bool;
struct indirect_placeholder_79_ret_type;
struct indirect_placeholder_79_ret_type {
    uint64_t field_0;
    uint64_t field_1;
    uint64_t field_2;
    uint64_t field_3;
    uint64_t field_4;
};
extern uint64_t revng_init_local_sp(uint64_t param_0);
extern uint64_t init_rbx(void);
extern uint64_t init_rbp(void);
extern void indirect_placeholder_1(void);
extern uint64_t init_cc_src2(void);
extern uint64_t init_r13(void);
extern uint64_t init_r12(void);
extern uint64_t init_r14(void);
extern uint64_t init_rax(void);
extern uint64_t helper_cc_compute_all_wrapper(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint32_t param_3);
extern uint64_t init_r15(void);
extern struct indirect_placeholder_79_ret_type indirect_placeholder_79(uint64_t param_0);
uint64_t bb_copy_unescaped_string(uint64_t rdi) {
    uint64_t rdx_2;
    uint64_t var_0;
    uint64_t var_1;
    uint64_t var_2;
    uint64_t var_3;
    uint64_t var_4;
    uint64_t var_5;
    uint64_t var_6;
    uint64_t var_7;
    uint64_t var_8;
    uint64_t var_9;
    uint64_t var_10;
    struct indirect_placeholder_79_ret_type var_11;
    uint64_t var_12;
    uint64_t rbx_1;
    uint64_t r14_1;
    unsigned char var_57;
    uint64_t var_58;
    uint64_t rdx_0_in;
    unsigned char var_59;
    uint64_t var_60;
    uint64_t var_61;
    uint64_t var_62;
    uint64_t rbx_0;
    uint64_t local_sp_1;
    uint64_t r15_0;
    uint64_t rdx_1;
    uint64_t var_65;
    uint64_t var_66;
    uint64_t r14_0;
    uint64_t rdx_5;
    uint64_t rcx_2;
    uint64_t rbx_1_be;
    uint64_t rbp_0_be;
    uint64_t local_sp_0_be;
    uint64_t rdx_1_be;
    uint64_t var_63;
    uint64_t var_64;
    uint64_t rbp_0;
    uint64_t local_sp_0;
    uint64_t rbp_2;
    uint64_t rbx_2;
    uint64_t rbp_1;
    unsigned char var_13;
    uint64_t var_14;
    unsigned char var_15;
    uint64_t var_16;
    uint64_t var_17;
    unsigned char var_18;
    uint64_t var_19;
    uint64_t var_20;
    uint64_t var_21;
    uint64_t var_22;
    uint64_t var_23;
    uint64_t var_24;
    unsigned char var_25;
    uint64_t var_26;
    uint64_t var_27;
    uint64_t rbx_4;
    uint64_t rax_0;
    uint64_t rcx_0;
    uint64_t rdx_3;
    uint64_t var_28;
    uint64_t var_29;
    bool var_30;
    unsigned char var_31;
    uint64_t rbx_3;
    uint64_t var_32;
    uint64_t var_33;
    uint64_t var_34;
    uint64_t var_35;
    uint64_t var_36;
    uint64_t var_37;
    uint64_t var_38;
    uint64_t var_39;
    uint64_t var_40;
    uint64_t var_41;
    uint64_t var_42;
    uint64_t var_43;
    uint64_t var_44;
    uint64_t var_45;
    uint64_t var_46;
    uint64_t var_47;
    uint64_t var_48;
    uint64_t var_52;
    uint64_t var_53;
    uint32_t var_54;
    uint64_t var_55;
    uint64_t var_56;
    uint64_t var_49;
    uint64_t var_50;
    uint64_t var_51;
    uint64_t var_67;
    uint64_t var_68;
    uint64_t var_69;
    uint64_t var_70;
    uint64_t var_71;
    uint64_t var_72;
    uint64_t var_73;
    uint64_t var_74;
    uint64_t var_75;
    unsigned int loop_state_var;
    bool switch_state_var;
    var_0 = revng_init_local_sp(0UL);
    var_1 = init_rax();
    var_2 = init_rbx();
    var_3 = init_rbp();
    var_4 = init_r13();
    var_5 = init_cc_src2();
    var_6 = init_r12();
    var_7 = init_r15();
    var_8 = init_r14();
    *(uint64_t *)(var_0 + (-8L)) = var_7;
    *(uint64_t *)(var_0 + (-16L)) = var_8;
    *(uint64_t *)(var_0 + (-24L)) = var_4;
    *(uint64_t *)(var_0 + (-32L)) = var_6;
    *(uint64_t *)(var_0 + (-40L)) = var_3;
    *(uint64_t *)(var_0 + (-48L)) = var_2;
    *(uint64_t *)(var_0 + (-80L)) = 4212358UL;
    indirect_placeholder_1();
    var_9 = var_1 + 1UL;
    var_10 = var_0 + (-88L);
    *(uint64_t *)var_10 = 4212367UL;
    var_11 = indirect_placeholder_79(var_9);
    var_12 = var_11.field_0;
    rbx_1 = rdi;
    r14_1 = 0UL;
    rdx_1 = (uint64_t)*(unsigned char *)rdi;
    rcx_2 = 0UL;
    rbp_0 = var_12;
    local_sp_0 = var_10;
    rcx_0 = 0UL;
    while (1U)
        {
            rdx_2 = rdx_1;
            local_sp_1 = local_sp_0;
            local_sp_0_be = local_sp_0;
            rbp_2 = rbp_0;
            rbx_2 = rbx_1;
            rbp_1 = rbp_0;
            if ((uint64_t)(unsigned char)rdx_1 != 0UL) {
                loop_state_var = 1U;
                break;
            }
            while (1U)
                {
                    var_13 = (unsigned char)rdx_2;
                    rbp_2 = rbp_1;
                    if ((uint64_t)(var_13 + '\xa4') != 0UL) {
                        loop_state_var = 0U;
                        break;
                    }
                    *(unsigned char *)rbp_1 = var_13;
                    var_14 = rbx_2 + 1UL;
                    var_15 = *(unsigned char *)var_14;
                    var_16 = rbp_1 + 1UL;
                    rbx_2 = var_14;
                    rbp_1 = var_16;
                    rbp_2 = var_16;
                    if (var_15 != '\x00') {
                        loop_state_var = 1U;
                        break;
                    }
                    rdx_2 = (uint64_t)var_15;
                    continue;
                }
            switch_state_var = 0;
            switch (loop_state_var) {
              case 0U:
                {
                    var_17 = rbx_2 + 1UL;
                    var_18 = *(unsigned char *)var_17;
                    var_19 = (uint64_t)var_18;
                    var_20 = var_19 + (-102L);
                    rbx_4 = var_17;
                    if ((uint64_t)(unsigned char)var_20 == 0UL) {
                        *(unsigned char *)rbp_1 = (unsigned char)'\f';
                        var_73 = rbx_2 + 2UL;
                        var_74 = (uint64_t)*(unsigned char *)var_73;
                        var_75 = rbp_1 + 1UL;
                        rbx_1_be = var_73;
                        rbp_0_be = var_75;
                        rdx_1_be = var_74;
                    } else {
                        var_21 = helper_cc_compute_all_wrapper(var_20, 102UL, var_5, 14U);
                        if ((uint64_t)(((unsigned char)(var_21 >> 4UL) ^ (unsigned char)var_21) & '\xc0') == 0UL) {
                            var_41 = var_19 + (-116L);
                            if ((uint64_t)(unsigned char)var_41 != 0UL) {
                                *(unsigned char *)rbp_1 = (unsigned char)'\t';
                                var_70 = rbx_2 + 2UL;
                                var_71 = (uint64_t)*(unsigned char *)var_70;
                                var_72 = rbp_1 + 1UL;
                                rbx_1_be = var_70;
                                rbp_0_be = var_72;
                                rdx_1_be = var_71;
                                rbx_1 = rbx_1_be;
                                rbp_0 = rbp_0_be;
                                local_sp_0 = local_sp_0_be;
                                rdx_1 = rdx_1_be;
                                continue;
                            }
                            var_42 = helper_cc_compute_all_wrapper(var_41, 116UL, var_5, 14U);
                            if ((uint64_t)(((unsigned char)(var_42 >> 4UL) ^ (unsigned char)var_42) & '\xc0') != 0UL) {
                                if ((uint64_t)(var_18 + '\x92') != 0UL) {
                                    *(unsigned char *)rbp_1 = (unsigned char)'\n';
                                    var_46 = rbx_2 + 2UL;
                                    var_47 = (uint64_t)*(unsigned char *)var_46;
                                    var_48 = rbp_1 + 1UL;
                                    rbx_1_be = var_46;
                                    rbp_0_be = var_48;
                                    rdx_1_be = var_47;
                                    rbx_1 = rbx_1_be;
                                    rbp_0 = rbp_0_be;
                                    local_sp_0 = local_sp_0_be;
                                    rdx_1 = rdx_1_be;
                                    continue;
                                }
                                if ((uint64_t)(var_18 + '\x8e') != 0UL) {
                                    *(unsigned char *)rbp_1 = (unsigned char)'\r';
                                    var_43 = rbx_2 + 2UL;
                                    var_44 = (uint64_t)*(unsigned char *)var_43;
                                    var_45 = rbp_1 + 1UL;
                                    rbx_1_be = var_43;
                                    rbp_0_be = var_45;
                                    rdx_1_be = var_44;
                                    rbx_1 = rbx_1_be;
                                    rbp_0 = rbp_0_be;
                                    local_sp_0 = local_sp_0_be;
                                    rdx_1 = rdx_1_be;
                                    continue;
                                }
                            }
                            if ((uint64_t)(var_18 + '\x8a') != 0UL) {
                                *(unsigned char *)rbp_1 = (unsigned char)'\v';
                                var_67 = rbx_2 + 2UL;
                                var_68 = (uint64_t)*(unsigned char *)var_67;
                                var_69 = rbp_1 + 1UL;
                                rbx_1_be = var_67;
                                rbp_0_be = var_69;
                                rdx_1_be = var_68;
                                rbx_1 = rbx_1_be;
                                rbp_0 = rbp_0_be;
                                local_sp_0 = local_sp_0_be;
                                rdx_1 = rdx_1_be;
                                continue;
                            }
                            if ((uint64_t)(var_18 + '\x88') != 0UL) {
                                var_52 = rbx_2 + 2UL;
                                var_53 = rbx_2 + 5UL;
                                r15_0 = var_52;
                                while (1U)
                                    {
                                        var_54 = (uint32_t)r15_0 - (uint32_t)var_52;
                                        var_55 = (uint64_t)var_54;
                                        *(uint32_t *)(local_sp_1 + 12UL) = var_54;
                                        var_56 = local_sp_1 + (-8L);
                                        *(uint64_t *)var_56 = 4212687UL;
                                        indirect_placeholder_1();
                                        rbx_0 = r15_0;
                                        local_sp_1 = var_56;
                                        r14_0 = r14_1;
                                        rbx_1_be = r15_0;
                                        local_sp_0_be = var_56;
                                        if (var_55 != 0UL) {
                                            loop_state_var = 1U;
                                            break;
                                        }
                                        var_57 = *(unsigned char *)r15_0;
                                        var_58 = r14_1 << 4UL;
                                        rbx_0 = var_53;
                                        if ((uint64_t)((var_57 + '\x9f') & '\xfe') > 5UL) {
                                            var_59 = var_57 + '\xbf';
                                            var_60 = (uint64_t)(uint32_t)(uint64_t)var_57;
                                            rdx_0_in = ((uint64_t)(var_59 & '\xfe') > 5UL) ? (var_60 + 4294967248UL) : (var_60 + 4294967241UL);
                                        } else {
                                            rdx_0_in = (uint64_t)var_57 + 4294967209UL;
                                        }
                                        var_61 = r15_0 + 1UL;
                                        var_62 = (uint64_t)((uint32_t)var_58 + (uint32_t)rdx_0_in);
                                        r14_0 = var_62;
                                        r15_0 = var_61;
                                        r14_1 = var_62;
                                        if (var_61 == var_53) {
                                            continue;
                                        }
                                        loop_state_var = 0U;
                                        break;
                                    }
                                switch (loop_state_var) {
                                  case 0U:
                                    {
                                        *(unsigned char *)rbp_1 = (unsigned char)r14_0;
                                        var_63 = (uint64_t)*(unsigned char *)rbx_0;
                                        var_64 = rbp_1 + 1UL;
                                        rbx_1_be = rbx_0;
                                        rbp_0_be = var_64;
                                        rdx_1_be = var_63;
                                        rbx_1 = rbx_1_be;
                                        rbp_0 = rbp_0_be;
                                        local_sp_0 = local_sp_0_be;
                                        rdx_1 = rdx_1_be;
                                        continue;
                                    }
                                    break;
                                  case 1U:
                                    {
                                        if (*(uint32_t *)(local_sp_1 + 4UL) != 0U) {
                                            *(unsigned char *)rbp_1 = (unsigned char)'\\';
                                            *(unsigned char *)(rbp_1 + 1UL) = (unsigned char)'x';
                                            var_65 = rbp_1 + 2UL;
                                            var_66 = (uint64_t)*(unsigned char *)r15_0;
                                            rbp_0_be = var_65;
                                            rdx_1_be = var_66;
                                            rbx_1 = rbx_1_be;
                                            rbp_0 = rbp_0_be;
                                            local_sp_0 = local_sp_0_be;
                                            rdx_1 = rdx_1_be;
                                            continue;
                                        }
                                    }
                                    break;
                                }
                            }
                        }
                        var_22 = var_19 + (-97L);
                        if ((uint64_t)(unsigned char)var_22 != 0UL) {
                            *(unsigned char *)rbp_1 = (unsigned char)'\a';
                            var_38 = rbx_2 + 2UL;
                            var_39 = (uint64_t)*(unsigned char *)var_38;
                            var_40 = rbp_1 + 1UL;
                            rbx_1_be = var_38;
                            rbp_0_be = var_40;
                            rdx_1_be = var_39;
                            rbx_1 = rbx_1_be;
                            rbp_0 = rbp_0_be;
                            local_sp_0 = local_sp_0_be;
                            rdx_1 = rdx_1_be;
                            continue;
                        }
                        var_23 = helper_cc_compute_all_wrapper(var_22, 97UL, var_5, 14U);
                        if ((uint64_t)(((unsigned char)(var_23 >> 4UL) ^ (unsigned char)var_23) & '\xc0') == 0UL) {
                            if ((uint64_t)(var_18 + '\x9e') != 0UL) {
                                *(unsigned char *)rbp_1 = (unsigned char)'\b';
                                var_35 = rbx_2 + 2UL;
                                var_36 = (uint64_t)*(unsigned char *)var_35;
                                var_37 = rbp_1 + 1UL;
                                rbx_1_be = var_35;
                                rbp_0_be = var_37;
                                rdx_1_be = var_36;
                                rbx_1 = rbx_1_be;
                                rbp_0 = rbp_0_be;
                                local_sp_0 = local_sp_0_be;
                                rdx_1 = rdx_1_be;
                                continue;
                            }
                            if ((uint64_t)(var_18 + '\x9d') == 0UL) {
                                loop_state_var = 0U;
                                switch_state_var = 1;
                                break;
                            }
                        }
                        if (var_18 != '\x00') {
                            loop_state_var = 1U;
                            switch_state_var = 1;
                            break;
                        }
                        if ((uint64_t)(var_18 + '\xd0') == 0UL) {
                            *(unsigned char *)(rbp_1 + 1UL) = var_18;
                            *(unsigned char *)rbp_1 = (unsigned char)'\\';
                            var_49 = rbx_2 + 2UL;
                            var_50 = (uint64_t)*(unsigned char *)var_49;
                            var_51 = rbp_1 + 2UL;
                            rbx_1_be = var_49;
                            rbp_0_be = var_51;
                            rdx_1_be = var_50;
                        } else {
                            var_24 = rbx_2 + 2UL;
                            var_25 = *(unsigned char *)var_24;
                            var_26 = (uint64_t)(uint32_t)(uint64_t)var_25;
                            var_27 = rbx_2 + 5UL;
                            rax_0 = var_24;
                            rdx_3 = var_26;
                            rbx_3 = var_24;
                            rdx_5 = var_26;
                            if ((uint64_t)((var_25 + '\xd0') & '\xf8') == 0UL) {
                                *(unsigned char *)rbp_1 = (unsigned char)rcx_2;
                                var_33 = rbp_1 + 1UL;
                                rbx_1_be = rbx_3;
                                rbp_0_be = var_33;
                                rdx_1_be = rdx_5;
                                rbx_1 = rbx_1_be;
                                rbp_0 = rbp_0_be;
                                local_sp_0 = local_sp_0_be;
                                rdx_1 = rdx_1_be;
                                continue;
                            }
                            while (1U)
                                {
                                    var_28 = (uint64_t)(((uint32_t)(rcx_0 << 3UL) + (uint32_t)rdx_3) + (-48));
                                    var_29 = rax_0 + 1UL;
                                    var_30 = (var_29 == var_27);
                                    var_31 = *(unsigned char *)var_29;
                                    rax_0 = var_29;
                                    rcx_0 = var_28;
                                    rcx_2 = var_28;
                                    rbx_3 = var_27;
                                    if (!var_30) {
                                        loop_state_var = 1U;
                                        break;
                                    }
                                    var_32 = (uint64_t)(uint32_t)(uint64_t)var_31;
                                    rdx_3 = var_32;
                                    rbx_3 = var_29;
                                    rdx_5 = var_32;
                                    if ((uint64_t)((var_31 + '\xd0') & '\xf8') == 0UL) {
                                        continue;
                                    }
                                    loop_state_var = 0U;
                                    break;
                                }
                            rdx_5 = (uint64_t)var_31;
                        }
                    }
                }
                break;
              case 1U:
                {
                    loop_state_var = 1U;
                    switch_state_var = 1;
                    break;
                }
                break;
            }
            if (switch_state_var)
                break;
        }
    switch (loop_state_var) {
      case 1U:
        {
            *(unsigned char *)rbp_2 = (unsigned char)'\x00';
            return var_12;
        }
        break;
      case 0U:
        {
            var_34 = rbx_4 + 1UL;
            rbx_4 = var_34;
            do {
                var_34 = rbx_4 + 1UL;
                rbx_4 = var_34;
            } while (*(unsigned char *)var_34 != '\x00');
        }
        break;
    }
}

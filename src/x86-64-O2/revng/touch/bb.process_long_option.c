typedef unsigned char __u_char;
typedef unsigned short __u_short;
typedef unsigned int __u_int;
typedef unsigned long __u_long;
typedef signed char __int8_t;
typedef unsigned char __uint8_t;
typedef short __int16_t;
typedef unsigned short __uint16_t;
typedef int __int32_t;
typedef unsigned int __uint32_t;
typedef long __int64_t;
typedef unsigned long __uint64_t;
typedef __int8_t __int_least8_t;
typedef __uint8_t __uint_least8_t;
typedef __int16_t __int_least16_t;
typedef __uint16_t __uint_least16_t;
typedef __int32_t __int_least32_t;
typedef __uint32_t __uint_least32_t;
typedef __int64_t __int_least64_t;
typedef __uint64_t __uint_least64_t;
typedef long __quad_t;
typedef unsigned long __u_quad_t;
typedef long __intmax_t;
typedef unsigned long __uintmax_t;
typedef unsigned long __dev_t;
typedef unsigned int __uid_t;
typedef unsigned int __gid_t;
typedef unsigned long __ino_t;
typedef unsigned long __ino64_t;
typedef unsigned int __mode_t;
typedef unsigned long __nlink_t;
typedef long __off_t;
typedef long __off64_t;
typedef int __pid_t;
typedef struct {
    int __val[2];
} __fsid_t;
typedef long __clock_t;
typedef unsigned long __rlim_t;
typedef unsigned long __rlim64_t;
typedef unsigned int __id_t;
typedef long __time_t;
typedef unsigned int __useconds_t;
typedef long __suseconds_t;
typedef int __daddr_t;
typedef int __key_t;
typedef int __clockid_t;
typedef void *__timer_t;
typedef long __blksize_t;
typedef long __blkcnt_t;
typedef long __blkcnt64_t;
typedef unsigned long __fsblkcnt_t;
typedef unsigned long __fsblkcnt64_t;
typedef unsigned long __fsfilcnt_t;
typedef unsigned long __fsfilcnt64_t;
typedef long __fsword_t;
typedef long __ssize_t;
typedef long __syscall_slong_t;
typedef unsigned long __syscall_ulong_t;
typedef __off64_t __loff_t;
typedef char *__caddr_t;
typedef long __intptr_t;
typedef unsigned int __socklen_t;
typedef int __sig_atomic_t;
typedef __int8_t int8_t;
typedef __int16_t int16_t;
typedef __int32_t int32_t;
typedef __int64_t int64_t;
typedef __uint8_t uint8_t;
typedef __uint16_t uint16_t;
typedef __uint32_t uint32_t;
typedef __uint64_t uint64_t;
typedef __int_least8_t int_least8_t;
typedef __int_least16_t int_least16_t;
typedef __int_least32_t int_least32_t;
typedef __int_least64_t int_least64_t;
typedef __uint_least8_t uint_least8_t;
typedef __uint_least16_t uint_least16_t;
typedef __uint_least32_t uint_least32_t;
typedef __uint_least64_t uint_least64_t;
typedef signed char int_fast8_t;
typedef long int_fast16_t;
typedef long int_fast32_t;
typedef long int_fast64_t;
typedef unsigned char uint_fast8_t;
typedef unsigned long uint_fast16_t;
typedef unsigned long uint_fast32_t;
typedef unsigned long uint_fast64_t;
typedef long intptr_t;
typedef unsigned long uintptr_t;
typedef __intmax_t intmax_t;
typedef __uintmax_t uintmax_t;
typedef _Bool bool;
extern uint64_t revng_init_local_sp(uint64_t param_0);
extern void indirect_placeholder(void);
extern uint64_t init_rbx(void);
extern uint64_t init_rbp(void);
extern uint64_t indirect_placeholder_2(uint64_t param_0);
extern uint64_t init_r13(void);
extern uint64_t init_r12(void);
extern uint64_t init_r14(void);
extern uint64_t init_r15(void);
uint64_t bb_process_long_option(uint64_t rcx, uint64_t r9, uint64_t rdi, uint64_t r8, uint64_t rdx, uint64_t rsi) {
    uint64_t var_0;
    uint64_t var_1;
    uint64_t var_2;
    uint64_t var_3;
    uint64_t var_4;
    uint64_t var_5;
    uint64_t var_6;
    uint64_t var_7;
    uint64_t var_8;
    uint64_t *var_9;
    uint64_t var_10;
    unsigned char var_11;
    uint64_t rax_5;
    uint64_t rbp_5;
    uint64_t rax_4;
    uint64_t var_38;
    uint64_t var_39;
    uint64_t *var_40;
    uint64_t var_41;
    uint64_t _pre;
    uint64_t var_42;
    uint64_t local_sp_1;
    uint64_t local_sp_0;
    uint64_t r12_0;
    uint64_t var_54;
    uint64_t var_55;
    uint64_t var_56;
    uint64_t local_sp_5;
    uint64_t rax_4_ph;
    uint64_t r15_0;
    uint64_t rbp_2;
    uint64_t rbp_4;
    uint32_t var_28;
    uint64_t var_29;
    uint32_t var_30;
    uint64_t var_31;
    uint64_t rax_0;
    uint64_t r12_1;
    bool var_32;
    uint64_t var_33;
    bool var_34;
    uint64_t local_sp_2;
    uint64_t var_35;
    uint64_t var_36;
    uint64_t var_37;
    uint64_t rbp_0;
    uint64_t r14_1;
    uint64_t var_44;
    uint64_t var_45;
    uint64_t local_sp_4;
    uint64_t local_sp_10_ph;
    uint64_t local_sp_10;
    uint64_t local_sp_3;
    uint64_t rax_1;
    uint64_t rbp_1;
    uint64_t var_43;
    uint64_t r15_3;
    uint64_t rbp_4_ph;
    uint64_t r12_1_ph;
    uint64_t r14_1_ph;
    bool var_23;
    uint32_t *var_24;
    uint64_t *var_25;
    uint32_t *var_26;
    uint64_t var_46;
    uint64_t var_47;
    uint64_t var_48;
    bool var_49;
    uint64_t local_sp_12;
    uint64_t var_50;
    uint64_t *var_51;
    uint64_t var_52;
    uint64_t var_53;
    uint64_t rax_2;
    uint64_t local_sp_6;
    uint64_t var_57;
    uint32_t *var_58;
    uint64_t local_sp_7;
    uint64_t rbx_1;
    uint64_t r15_1;
    uint64_t rbx_0;
    uint64_t r12_2;
    uint64_t rbp_3;
    uint32_t *var_59;
    uint32_t var_60;
    uint64_t var_61;
    uint64_t var_62;
    bool var_63;
    uint32_t var_64;
    uint64_t var_65;
    uint64_t var_66;
    uint64_t local_sp_8;
    uint64_t var_69;
    uint64_t var_68;
    uint64_t var_67;
    uint64_t merge;
    uint64_t var_70;
    uint64_t var_71;
    bool var_72;
    uint32_t var_73;
    uint64_t var_18;
    uint64_t local_sp_11;
    uint64_t local_sp_9;
    uint64_t var_19;
    uint64_t var_20;
    uint64_t var_21;
    uint64_t var_22;
    uint64_t r15_2_ph;
    uint64_t var_27;
    uint64_t var_12;
    unsigned char var_13;
    uint64_t rbp_6;
    uint64_t *var_14;
    bool var_15;
    bool var_16;
    uint64_t var_17;
    uint64_t local_sp_13;
    uint64_t var_74;
    uint64_t var_75;
    uint32_t *var_76;
    unsigned int loop_state_var;
    bool switch_state_var;
    var_0 = revng_init_local_sp(0UL);
    var_1 = init_r15();
    var_2 = init_rbx();
    var_3 = init_rbp();
    var_4 = init_r13();
    var_5 = init_r12();
    var_6 = init_r14();
    *(uint64_t *)(var_0 + (-8L)) = var_1;
    *(uint64_t *)(var_0 + (-16L)) = var_6;
    *(uint64_t *)(var_0 + (-24L)) = var_4;
    *(uint64_t *)(var_0 + (-32L)) = var_5;
    *(uint64_t *)(var_0 + (-40L)) = var_3;
    *(uint64_t *)(var_0 + (-48L)) = var_2;
    var_7 = var_0 + (-136L);
    var_8 = *(uint64_t *)(var_0 | 8UL);
    *(uint64_t *)(var_0 + (-120L)) = rsi;
    *(uint32_t *)(var_0 + (-96L)) = (uint32_t)rdi;
    *(uint64_t *)(var_0 + (-112L)) = rdx;
    *(uint64_t *)(var_0 + (-128L)) = r8;
    *(uint32_t *)(var_0 + (-132L)) = (uint32_t)r9;
    var_9 = (uint64_t *)(var_8 + 32UL);
    var_10 = *var_9;
    var_11 = *(unsigned char *)var_10;
    rax_5 = (uint64_t)var_11;
    rbp_5 = var_10;
    r12_0 = 0UL;
    r15_3 = rcx;
    rbp_4_ph = 0UL;
    r12_1_ph = 0UL;
    r14_1_ph = rcx;
    local_sp_12 = var_7;
    rbx_1 = 0UL;
    r12_2 = 0UL;
    merge = 4294967295UL;
    local_sp_11 = var_7;
    r15_2_ph = 0UL;
    rbp_6 = var_10;
    if (!(((uint64_t)(var_11 + '\xc3') == 0UL) || (var_11 == '\x00'))) {
        while (1U)
            {
                switch_state_var = 1;
                break;
            }
        rax_5 = (uint64_t)var_13;
        r12_2 = var_12 - var_10;
    }
    var_14 = (uint64_t *)rcx;
    rbp_3 = rbp_6;
    if (*var_14 != 0UL) {
        local_sp_13 = local_sp_12;
        if (*(uint32_t *)(local_sp_12 + 4UL) != 0U) {
            var_74 = *var_9;
            var_75 = local_sp_12 + (-8L);
            *(uint64_t *)var_75 = 4233603UL;
            indirect_placeholder();
            local_sp_13 = var_75;
            if (*(unsigned char *)(*(uint64_t *)(((uint64_t)*(uint32_t *)var_8 << 3UL) + *(uint64_t *)(local_sp_12 + 16UL)) + 1UL) != '-' & var_74 == 0UL) {
                return merge;
            }
        }
        if (*(uint32_t *)(local_sp_13 + 152UL) != 0U) {
            *(uint64_t *)(local_sp_13 + (-8L)) = 4233225UL;
            indirect_placeholder();
        }
        *var_9 = 0UL;
        var_76 = (uint32_t *)var_8;
        *var_76 = (*var_76 + 1U);
        *(uint32_t *)(var_8 + 8UL) = 0U;
        return 63UL;
    }
    var_15 = (rax_5 == 0UL);
    var_16 = (r12_2 == rax_5);
    merge = 63UL;
    while (1U)
        {
            var_17 = local_sp_11 + (-8L);
            *(uint64_t *)var_17 = 4232491UL;
            indirect_placeholder();
            r15_1 = r15_3;
            rbx_0 = rbx_1;
            local_sp_9 = var_17;
            if (!var_15) {
                var_18 = local_sp_11 + (-16L);
                *(uint64_t *)var_18 = 4232503UL;
                indirect_placeholder();
                local_sp_7 = var_18;
                local_sp_9 = var_18;
                if (!var_16) {
                    loop_state_var = 0U;
                    break;
                }
            }
            var_19 = r15_3 + 32UL;
            var_20 = *(uint64_t *)var_19;
            var_21 = rbx_1 + 1UL;
            local_sp_10_ph = local_sp_9;
            local_sp_11 = local_sp_9;
            r15_3 = var_19;
            local_sp_12 = local_sp_9;
            if (var_20 == 0UL) {
                rbx_1 = (uint64_t)(uint32_t)var_21;
                continue;
            }
            if (*var_14 != 0UL) {
                loop_state_var = 2U;
                break;
            }
            var_22 = (uint64_t)((long)(var_21 << 32UL) >> (long)32UL);
            *(uint64_t *)(local_sp_9 + 48UL) = rbp_6;
            *(uint32_t *)(local_sp_9 + 64UL) = (uint32_t)var_21;
            *(uint32_t *)(local_sp_9 + 44UL) = 4294967295U;
            *(uint32_t *)(local_sp_9 + 68UL) = 0U;
            *(uint64_t *)(local_sp_9 + 32UL) = 0UL;
            *(uint64_t *)(local_sp_9 + 72UL) = var_22;
            *(uint64_t *)(local_sp_9 + 56UL) = rcx;
            rax_4_ph = var_22;
            while (1U)
                {
                    var_23 = (r15_2_ph == 0UL);
                    var_24 = (uint32_t *)(r15_2_ph + 8UL);
                    var_25 = (uint64_t *)(r15_2_ph + 16UL);
                    var_26 = (uint32_t *)(r15_2_ph + 24UL);
                    rax_4 = rax_4_ph;
                    r15_0 = r15_2_ph;
                    rbp_4 = rbp_4_ph;
                    r12_1 = r12_1_ph;
                    r14_1 = r14_1_ph;
                    local_sp_10 = local_sp_10_ph;
                    while (1U)
                        {
                            var_27 = local_sp_10 + (-8L);
                            *(uint64_t *)var_27 = 4232692UL;
                            indirect_placeholder();
                            rax_4_ph = rax_4;
                            rbp_2 = rbp_4;
                            rax_0 = rax_4;
                            local_sp_2 = var_27;
                            rbp_0 = rbp_4;
                            local_sp_4 = var_27;
                            local_sp_10_ph = var_27;
                            local_sp_3 = var_27;
                            rax_1 = rax_4;
                            rbp_1 = rbp_4;
                            rbp_4_ph = rbp_4;
                            r15_2_ph = r14_1;
                            if ((uint64_t)(uint32_t)rax_4 == 0UL) {
                                var_43 = r14_1 + 32UL;
                                rax_4 = rax_1;
                                rbp_2 = rbp_1;
                                rbp_4 = rbp_1;
                                r14_1 = var_43;
                                local_sp_4 = local_sp_3;
                                local_sp_10 = local_sp_3;
                                if (*(uint64_t *)var_43 != 0UL) {
                                    loop_state_var = 0U;
                                    break;
                                }
                                r12_1 = r12_1 + 1UL;
                                continue;
                            }
                            r15_0 = r14_1;
                            if (!var_23) {
                                loop_state_var = 1U;
                                break;
                            }
                            if (*(uint32_t *)(local_sp_10 + (-4L)) != 0U) {
                                var_28 = *(uint32_t *)(r14_1 + 8UL);
                                rax_0 = (uint64_t)var_28;
                                var_29 = *(uint64_t *)(r14_1 + 16UL);
                                rax_0 = var_29;
                                var_30 = *(uint32_t *)(r14_1 + 24UL);
                                var_31 = (uint64_t)var_30;
                                rax_0 = var_31;
                                rax_1 = var_31;
                                if ((uint64_t)(*var_24 - var_28) != 0UL & *var_25 != var_29 & (uint64_t)(*var_26 - var_30) != 0UL) {
                                    var_43 = r14_1 + 32UL;
                                    rax_4 = rax_1;
                                    rbp_2 = rbp_1;
                                    rbp_4 = rbp_1;
                                    r14_1 = var_43;
                                    local_sp_4 = local_sp_3;
                                    local_sp_10 = local_sp_3;
                                    if (*(uint64_t *)var_43 != 0UL) {
                                        loop_state_var = 0U;
                                        break;
                                    }
                                    r12_1 = r12_1 + 1UL;
                                    continue;
                                }
                            }
                            rax_1 = rax_0;
                            if ((uint64_t)(uint32_t)rbp_4 != 0UL) {
                                var_32 = (*(uint32_t *)(local_sp_10 + 144UL) == 0U);
                                var_33 = *(uint64_t *)(local_sp_10 + 24UL);
                                var_34 = (var_33 == 0UL);
                                var_42 = var_33;
                                rax_1 = 0UL;
                                rbp_1 = 1UL;
                                if (var_32) {
                                    rbp_0 = 1UL;
                                    rax_1 = rax_0;
                                    if (var_34) {
                                        *(unsigned char *)(r12_1 + var_42) = (unsigned char)'\x01';
                                        local_sp_3 = local_sp_2;
                                        rax_1 = var_42;
                                        rbp_1 = rbp_0;
                                    }
                                } else {
                                    if (var_34) {
                                        *(unsigned char *)(r12_1 + var_42) = (unsigned char)'\x01';
                                        local_sp_3 = local_sp_2;
                                        rax_1 = var_42;
                                        rbp_1 = rbp_0;
                                    } else {
                                        var_35 = *(uint64_t *)(local_sp_10 + 64UL);
                                        var_36 = local_sp_10 + (-16L);
                                        *(uint64_t *)var_36 = 4233670UL;
                                        var_37 = indirect_placeholder_2(var_35);
                                        *(uint64_t *)(local_sp_10 + 16UL) = var_37;
                                        local_sp_3 = var_36;
                                        if (var_37 != 0UL) {
                                            var_38 = local_sp_10 + (-24L);
                                            *(uint64_t *)var_38 = 4233697UL;
                                            indirect_placeholder();
                                            var_39 = (uint64_t)*(uint32_t *)(local_sp_10 + 20UL);
                                            var_40 = (uint64_t *)(local_sp_10 + 8UL);
                                            var_41 = *var_40;
                                            *(uint32_t *)(local_sp_10 + 44UL) = 1U;
                                            *(unsigned char *)(var_41 + var_39) = (unsigned char)'\x01';
                                            _pre = *var_40;
                                            var_42 = _pre;
                                            local_sp_2 = var_38;
                                            *(unsigned char *)(r12_1 + var_42) = (unsigned char)'\x01';
                                            local_sp_3 = local_sp_2;
                                            rax_1 = var_42;
                                            rbp_1 = rbp_0;
                                        }
                                    }
                                }
                            }
                        }
                    switch_state_var = 0;
                    switch (loop_state_var) {
                      case 0U:
                        {
                            switch_state_var = 1;
                            break;
                        }
                        break;
                      case 1U:
                        {
                            var_44 = r14_1 + 32UL;
                            var_45 = *(uint64_t *)var_44;
                            *(uint32_t *)(local_sp_10 + 36UL) = (uint32_t)r12_1;
                            r14_1_ph = var_44;
                            if (var_45 == 0UL) {
                                switch_state_var = 1;
                                break;
                            }
                            r12_1_ph = r12_1 + 1UL;
                            continue;
                        }
                        break;
                    }
                    if (switch_state_var)
                        break;
                }
            var_46 = (uint64_t)(uint32_t)rbp_2;
            var_47 = (uint64_t)*(uint32_t *)(local_sp_4 + 64UL);
            var_48 = *(uint64_t *)(local_sp_4 + 48UL);
            var_49 = ((uint64_t)(unsigned char)rbp_2 == 0UL);
            local_sp_5 = local_sp_4;
            local_sp_12 = local_sp_4;
            rax_2 = var_46;
            local_sp_7 = local_sp_4;
            r15_1 = r15_0;
            rbp_3 = var_48;
            if (!var_49) {
                loop_state_var = 1U;
                break;
            }
            if (*(uint64_t *)(local_sp_4 + 32UL) != 0UL) {
                loop_state_var = 1U;
                break;
            }
            rbx_0 = (uint64_t)*(uint32_t *)(local_sp_4 + 44UL);
            if (r15_0 != 0UL) {
                loop_state_var = 2U;
                break;
            }
            loop_state_var = 0U;
            break;
        }
    switch (loop_state_var) {
      case 0U:
        {
            var_59 = (uint32_t *)var_8;
            var_60 = *var_59;
            var_61 = (uint64_t)var_60;
            *var_9 = 0UL;
            var_62 = var_61 + 1UL;
            *var_59 = (uint32_t)var_62;
            var_63 = (*(unsigned char *)rbp_3 == '\x00');
            var_64 = *(uint32_t *)(r15_1 + 8UL);
            local_sp_8 = local_sp_7;
            if (var_63) {
                if (var_64 != 1U) {
                    var_65 = (uint64_t)*(uint32_t *)(local_sp_7 + 40UL);
                    var_66 = var_62 << 32UL;
                    if ((long)var_66 >= (long)(var_65 << 32UL)) {
                        if (*(uint32_t *)(local_sp_7 + 152UL) == 0U) {
                            var_67 = local_sp_7 + (-8L);
                            *(uint64_t *)var_67 = 4233561UL;
                            indirect_placeholder();
                            local_sp_8 = var_67;
                        }
                        *(uint32_t *)(var_8 + 8UL) = *(uint32_t *)(r15_1 + 24UL);
                        var_68 = (**(unsigned char **)(local_sp_8 + 24UL) == ':') ? 58UL : 63UL;
                        merge = var_68;
                        return merge;
                    }
                    var_69 = *(uint64_t *)(local_sp_7 + 16UL);
                    *var_59 = (var_60 + 2U);
                    *(uint64_t *)(var_8 + 16UL) = *(uint64_t *)((uint64_t)((long)var_66 >> (long)29UL) + var_69);
                }
            } else {
                if (var_64 != 0U) {
                    if (*(uint32_t *)(local_sp_7 + 152UL) == 0U) {
                        *(uint32_t *)(var_8 + 8UL) = *(uint32_t *)(r15_1 + 24UL);
                    } else {
                        *(uint64_t *)(local_sp_7 + (-8L)) = 4233132UL;
                        indirect_placeholder();
                        *(uint32_t *)(var_8 + 8UL) = *(uint32_t *)(r15_1 + 24UL);
                    }
                    return merge;
                }
                *(uint64_t *)(var_8 + 16UL) = (rbp_3 + 1UL);
            }
            var_70 = *(uint64_t *)(local_sp_7 + 8UL);
            merge = 0UL;
            if (var_70 == 0UL) {
                *(uint32_t *)var_70 = (uint32_t)rbx_0;
            }
            var_71 = *(uint64_t *)(r15_1 + 16UL);
            var_72 = (var_71 == 0UL);
            var_73 = *(uint32_t *)(r15_1 + 24UL);
            if (var_72) {
                return (uint64_t)var_73;
            }
            *(uint32_t *)var_71 = var_73;
            return merge;
        }
        break;
      case 1U:
        {
            if (*(uint32_t *)(local_sp_4 + 152UL) != 0U) {
                var_50 = local_sp_4 + (-8L);
                var_51 = (uint64_t *)var_50;
                local_sp_5 = var_50;
                rax_2 = 0UL;
                if (var_49) {
                    *var_51 = 4233388UL;
                    indirect_placeholder();
                } else {
                    *var_51 = 4232859UL;
                    indirect_placeholder();
                    var_52 = local_sp_4 + (-16L);
                    *(uint64_t *)var_52 = 4232894UL;
                    indirect_placeholder();
                    var_53 = var_47 << 32UL;
                    local_sp_0 = var_52;
                    var_55 = r12_0 + 1UL;
                    local_sp_0 = local_sp_1;
                    r12_0 = var_55;
                    do {
                        local_sp_1 = local_sp_0;
                        if (*(unsigned char *)(r12_0 + *(uint64_t *)(local_sp_4 + 16UL)) == '\x00') {
                            var_54 = local_sp_0 + (-8L);
                            *(uint64_t *)var_54 = 4232954UL;
                            indirect_placeholder();
                            local_sp_1 = var_54;
                        }
                        var_55 = r12_0 + 1UL;
                        local_sp_0 = local_sp_1;
                        r12_0 = var_55;
                    } while ((long)var_53 <= (long)(var_55 << 32UL));
                    *(uint64_t *)(local_sp_1 + (-8L)) = 4233647UL;
                    indirect_placeholder();
                    var_56 = local_sp_1 + (-16L);
                    *(uint64_t *)var_56 = 4233655UL;
                    indirect_placeholder();
                    local_sp_5 = var_56;
                }
            }
            local_sp_6 = local_sp_5;
            if (*(uint32_t *)(local_sp_5 + 68UL) != 0U) {
                var_57 = local_sp_5 + (-8L);
                *(uint64_t *)var_57 = 4233408UL;
                indirect_placeholder();
                local_sp_6 = var_57;
            }
            *(uint64_t *)(local_sp_6 + (-8L)) = 4233417UL;
            indirect_placeholder();
            var_58 = (uint32_t *)var_8;
            *var_58 = (*var_58 + 1U);
            *var_9 = (*var_9 + rax_2);
            *(uint32_t *)(var_8 + 8UL) = 0U;
            return 63UL;
        }
        break;
      case 2U:
        {
            local_sp_13 = local_sp_12;
            if (*(uint32_t *)(local_sp_12 + 4UL) != 0U) {
                var_74 = *var_9;
                var_75 = local_sp_12 + (-8L);
                *(uint64_t *)var_75 = 4233603UL;
                indirect_placeholder();
                local_sp_13 = var_75;
                if (*(unsigned char *)(*(uint64_t *)(((uint64_t)*(uint32_t *)var_8 << 3UL) + *(uint64_t *)(local_sp_12 + 16UL)) + 1UL) != '-' & var_74 == 0UL) {
                    return merge;
                }
            }
            if (*(uint32_t *)(local_sp_13 + 152UL) != 0U) {
                *(uint64_t *)(local_sp_13 + (-8L)) = 4233225UL;
                indirect_placeholder();
            }
            *var_9 = 0UL;
            var_76 = (uint32_t *)var_8;
            *var_76 = (*var_76 + 1U);
            *(uint32_t *)(var_8 + 8UL) = 0U;
            return 63UL;
        }
        break;
    }
}

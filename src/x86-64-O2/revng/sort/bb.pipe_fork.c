typedef unsigned char __u_char;
typedef unsigned short __u_short;
typedef unsigned int __u_int;
typedef unsigned long __u_long;
typedef signed char __int8_t;
typedef unsigned char __uint8_t;
typedef short __int16_t;
typedef unsigned short __uint16_t;
typedef int __int32_t;
typedef unsigned int __uint32_t;
typedef long __int64_t;
typedef unsigned long __uint64_t;
typedef __int8_t __int_least8_t;
typedef __uint8_t __uint_least8_t;
typedef __int16_t __int_least16_t;
typedef __uint16_t __uint_least16_t;
typedef __int32_t __int_least32_t;
typedef __uint32_t __uint_least32_t;
typedef __int64_t __int_least64_t;
typedef __uint64_t __uint_least64_t;
typedef long __quad_t;
typedef unsigned long __u_quad_t;
typedef long __intmax_t;
typedef unsigned long __uintmax_t;
typedef unsigned long __dev_t;
typedef unsigned int __uid_t;
typedef unsigned int __gid_t;
typedef unsigned long __ino_t;
typedef unsigned long __ino64_t;
typedef unsigned int __mode_t;
typedef unsigned long __nlink_t;
typedef long __off_t;
typedef long __off64_t;
typedef int __pid_t;
typedef struct {
    int __val[2];
} __fsid_t;
typedef long __clock_t;
typedef unsigned long __rlim_t;
typedef unsigned long __rlim64_t;
typedef unsigned int __id_t;
typedef long __time_t;
typedef unsigned int __useconds_t;
typedef long __suseconds_t;
typedef int __daddr_t;
typedef int __key_t;
typedef int __clockid_t;
typedef void *__timer_t;
typedef long __blksize_t;
typedef long __blkcnt_t;
typedef long __blkcnt64_t;
typedef unsigned long __fsblkcnt_t;
typedef unsigned long __fsblkcnt64_t;
typedef unsigned long __fsfilcnt_t;
typedef unsigned long __fsfilcnt64_t;
typedef long __fsword_t;
typedef long __ssize_t;
typedef long __syscall_slong_t;
typedef unsigned long __syscall_ulong_t;
typedef __off64_t __loff_t;
typedef char *__caddr_t;
typedef long __intptr_t;
typedef unsigned int __socklen_t;
typedef int __sig_atomic_t;
typedef __int8_t int8_t;
typedef __int16_t int16_t;
typedef __int32_t int32_t;
typedef __int64_t int64_t;
typedef __uint8_t uint8_t;
typedef __uint16_t uint16_t;
typedef __uint32_t uint32_t;
typedef __uint64_t uint64_t;
typedef __int_least8_t int_least8_t;
typedef __int_least16_t int_least16_t;
typedef __int_least32_t int_least32_t;
typedef __int_least64_t int_least64_t;
typedef __uint_least8_t uint_least8_t;
typedef __uint_least16_t uint_least16_t;
typedef __uint_least32_t uint_least32_t;
typedef __uint_least64_t uint_least64_t;
typedef signed char int_fast8_t;
typedef long int_fast16_t;
typedef long int_fast32_t;
typedef long int_fast64_t;
typedef unsigned char uint_fast8_t;
typedef unsigned long uint_fast16_t;
typedef unsigned long uint_fast32_t;
typedef unsigned long uint_fast64_t;
typedef long intptr_t;
typedef unsigned long uintptr_t;
typedef __intmax_t intmax_t;
typedef __uintmax_t uintmax_t;
typedef _Bool bool;
struct helper_addsd_wrapper_338_ret_type;
struct type_5;
struct type_7;
struct helper_addsd_wrapper_338_ret_type {
    uint64_t field_0;
    unsigned char field_1;
};
struct type_5 {
};
struct type_7 {
};
extern uint64_t revng_init_local_sp(uint64_t param_0);
extern uint64_t init_rbx(void);
extern uint64_t init_rbp(void);
extern void indirect_placeholder_4(void);
extern unsigned char init_state_0x8549(void);
extern unsigned char init_state_0x854c(void);
extern unsigned char init_state_0x8548(void);
extern unsigned char init_state_0x854b(void);
extern unsigned char init_state_0x8547(void);
extern unsigned char init_state_0x854d(void);
extern uint64_t init_cc_src2(void);
extern uint64_t helper_cc_compute_c_wrapper(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint32_t param_3);
extern uint64_t init_r13(void);
extern uint64_t init_r12(void);
extern uint64_t init_r14(void);
extern uint64_t helper_cc_compute_all_wrapper(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint32_t param_3);
extern uint64_t init_r15(void);
extern void indirect_placeholder_15(uint64_t param_0);
extern uint64_t indirect_placeholder_9(uint64_t param_0);
extern uint64_t indirect_placeholder_5(uint64_t param_0, uint64_t param_1);
extern struct helper_addsd_wrapper_338_ret_type helper_addsd_wrapper_338(struct type_5 *param_0, struct type_7 *param_1, struct type_7 *param_2, uint64_t param_3, uint64_t param_4, unsigned char param_5, unsigned char param_6, unsigned char param_7, unsigned char param_8, unsigned char param_9, unsigned char param_10);
uint64_t bb_pipe_fork(uint64_t rdi, uint64_t rsi) {
    uint64_t var_0;
    uint64_t var_1;
    uint64_t var_2;
    uint64_t var_3;
    uint64_t var_4;
    uint64_t var_5;
    uint64_t var_6;
    uint64_t var_7;
    unsigned char var_8;
    unsigned char var_9;
    unsigned char var_10;
    unsigned char var_11;
    unsigned char var_12;
    unsigned char var_13;
    uint64_t var_14;
    uint64_t var_15;
    uint64_t rax_1;
    uint64_t var_41;
    uint64_t local_sp_2;
    uint64_t rbp_0;
    uint64_t var_37;
    uint64_t var_29;
    uint64_t local_sp_4;
    struct helper_addsd_wrapper_338_ret_type var_38;
    uint64_t var_39;
    unsigned char state_0x8549_0;
    uint64_t var_40;
    uint32_t *_pre_phi;
    uint64_t local_sp_0;
    uint64_t var_35;
    uint64_t *var_36;
    uint64_t local_sp_1;
    uint64_t var_34;
    uint64_t var_32;
    uint64_t var_33;
    uint64_t var_16;
    uint64_t var_17;
    uint64_t var_18;
    uint64_t rax_0;
    uint64_t local_sp_3;
    uint32_t *_pre;
    uint32_t var_21;
    uint64_t var_22;
    bool var_23;
    unsigned char var_24;
    uint32_t *var_25;
    bool var_26;
    bool var_27;
    uint64_t var_28;
    uint32_t var_30;
    unsigned char var_31;
    uint64_t rbx_0;
    uint64_t local_sp_5;
    uint32_t var_42;
    uint64_t var_43;
    uint64_t var_19;
    uint64_t var_20;
    unsigned int loop_state_var;
    var_0 = revng_init_local_sp(0UL);
    var_1 = init_rbx();
    var_2 = init_rbp();
    var_3 = init_r13();
    var_4 = init_cc_src2();
    var_5 = init_r12();
    var_6 = init_r15();
    var_7 = init_r14();
    var_8 = init_state_0x8549();
    var_9 = init_state_0x854c();
    var_10 = init_state_0x8548();
    var_11 = init_state_0x854b();
    var_12 = init_state_0x8547();
    var_13 = init_state_0x854d();
    *(uint64_t *)(var_0 + (-8L)) = var_6;
    *(uint64_t *)(var_0 + (-16L)) = var_7;
    *(uint64_t *)(var_0 + (-24L)) = var_3;
    *(uint64_t *)(var_0 + (-32L)) = var_5;
    *(uint64_t *)(var_0 + (-40L)) = var_2;
    *(uint64_t *)(var_0 + (-48L)) = var_1;
    var_14 = var_0 + (-224L);
    *(uint64_t *)var_14 = 4220465UL;
    var_15 = indirect_placeholder_5(rdi, 524288UL);
    rax_1 = 4294967295UL;
    rbp_0 = rsi;
    state_0x8549_0 = var_8;
    local_sp_3 = var_14;
    rbx_0 = var_1;
    if ((int)(uint32_t)var_15 < (int)0U) {
        return rax_1;
    }
    var_16 = (uint64_t)(*(uint32_t *)6430744UL + 1U);
    var_17 = (uint64_t)*(uint32_t *)6431580UL;
    var_18 = helper_cc_compute_c_wrapper(var_16 - var_17, var_17, var_4, 16U);
    rax_0 = var_16;
    rax_1 = 0UL;
    if (var_18 == 0UL) {
        *(uint64_t *)(var_0 + (-232L)) = 4220834UL;
        var_19 = indirect_placeholder_9(4294967295UL);
        var_20 = var_0 + (-240L);
        *(uint64_t *)var_20 = 4220839UL;
        indirect_placeholder_4();
        rax_0 = var_19;
        local_sp_3 = var_20;
    }
    local_sp_4 = local_sp_3;
    local_sp_5 = local_sp_3;
    if (rsi != 0UL) {
        _pre = (uint32_t *)rax_0;
        _pre_phi = _pre;
        *(uint64_t *)(local_sp_5 + (-8L)) = 4220773UL;
        indirect_placeholder_4();
        var_42 = *_pre_phi;
        *(uint64_t *)(local_sp_5 + (-16L)) = 4220783UL;
        indirect_placeholder_4();
        *(uint64_t *)(local_sp_5 + (-24L)) = 4220792UL;
        indirect_placeholder_4();
        *(uint64_t *)(local_sp_5 + (-32L)) = 4220797UL;
        indirect_placeholder_4();
        *_pre_phi = var_42;
        var_43 = (uint64_t)(uint32_t)rbx_0;
        rax_1 = var_43;
        return rax_1;
    }
    *(uint64_t *)(local_sp_3 + 8UL) = *(uint64_t *)4311048UL;
    var_21 = (uint32_t)rax_0;
    var_22 = (uint64_t)var_21;
    var_23 = (var_22 == 0UL);
    var_24 = var_23;
    var_25 = (uint32_t *)rax_0;
    var_26 = ((int)var_21 > (int)4294967295U);
    var_27 = ((int)var_21 < (int)0U);
    _pre_phi = var_25;
    rbx_0 = var_22;
    while (1U)
        {
            var_28 = local_sp_4 + (-8L);
            *(uint64_t *)var_28 = 4220637UL;
            indirect_placeholder_4();
            var_29 = *(uint64_t *)6431592UL;
            *(uint64_t *)6431592UL = 0UL;
            *(unsigned char *)(local_sp_4 + 8UL) = var_24;
            *(uint64_t *)(local_sp_4 + (-16L)) = 4220667UL;
            indirect_placeholder_4();
            *(uint64_t *)(local_sp_4 + (-24L)) = 4220674UL;
            indirect_placeholder_4();
            var_30 = *var_25;
            var_31 = *(unsigned char *)var_28;
            if (var_23) {
                if (var_31 != '\x00') {
                    *(uint64_t *)(local_sp_4 + (-32L)) = 4220738UL;
                    indirect_placeholder_15(var_28);
                    var_33 = local_sp_4 + (-40L);
                    *(uint64_t *)var_33 = 4220743UL;
                    indirect_placeholder_4();
                    *var_25 = var_30;
                    local_sp_0 = var_33;
                    local_sp_1 = var_33;
                    if (!var_27) {
                        loop_state_var = 2U;
                        break;
                    }
                }
                var_41 = local_sp_4 + (-32L);
                *(uint64_t *)var_41 = 4220697UL;
                indirect_placeholder_4();
                *var_25 = var_30;
                local_sp_2 = var_41;
                loop_state_var = 0U;
                break;
            }
            *(uint64_t *)6431592UL = var_29;
            if (var_31 == '\x00') {
                var_32 = local_sp_4 + (-32L);
                *(uint64_t *)var_32 = 4220551UL;
                indirect_placeholder_4();
                *var_25 = var_30;
                local_sp_0 = var_32;
                local_sp_1 = var_32;
                if (var_26) {
                    loop_state_var = 2U;
                    break;
                }
            }
            *(uint64_t *)(local_sp_4 + (-32L)) = 4220738UL;
            indirect_placeholder_15(var_28);
            var_33 = local_sp_4 + (-40L);
            *(uint64_t *)var_33 = 4220743UL;
            indirect_placeholder_4();
            *var_25 = var_30;
            local_sp_0 = var_33;
            local_sp_1 = var_33;
            if (!var_27) {
                loop_state_var = 2U;
                break;
            }
            var_35 = local_sp_0 + (-8L);
            var_36 = (uint64_t *)var_35;
            *var_36 = 4220567UL;
            indirect_placeholder_4();
            local_sp_5 = var_35;
            if (*var_25 != 11U) {
                loop_state_var = 1U;
                break;
            }
            *(uint64_t *)(local_sp_0 + (-16L)) = 4220587UL;
            indirect_placeholder_4();
            var_37 = *var_36;
            var_38 = helper_addsd_wrapper_338((struct type_5 *)(0UL), (struct type_7 *)(840UL), (struct type_7 *)(904UL), var_37, var_37, state_0x8549_0, var_9, var_10, var_11, var_12, var_13);
            *var_36 = var_38.field_0;
            var_39 = local_sp_0 + (-24L);
            *(uint64_t *)var_39 = 4220612UL;
            indirect_placeholder_4();
            var_40 = rbp_0 + (-1L);
            rbp_0 = var_40;
            local_sp_4 = var_39;
            local_sp_5 = var_39;
            if (var_40 != 0UL) {
                loop_state_var = 1U;
                break;
            }
            state_0x8549_0 = var_38.field_1;
            continue;
        }
    switch (loop_state_var) {
      case 0U:
        {
            *(uint64_t *)(local_sp_2 + (-8L)) = 4220707UL;
            indirect_placeholder_4();
            *(uint64_t *)(local_sp_2 + (-16L)) = 4220717UL;
            indirect_placeholder_4();
        }
        break;
      case 1U:
        {
            *(uint64_t *)(local_sp_5 + (-8L)) = 4220773UL;
            indirect_placeholder_4();
            var_42 = *_pre_phi;
            *(uint64_t *)(local_sp_5 + (-16L)) = 4220783UL;
            indirect_placeholder_4();
            *(uint64_t *)(local_sp_5 + (-24L)) = 4220792UL;
            indirect_placeholder_4();
            *(uint64_t *)(local_sp_5 + (-32L)) = 4220797UL;
            indirect_placeholder_4();
            *_pre_phi = var_42;
            var_43 = (uint64_t)(uint32_t)rbx_0;
            rax_1 = var_43;
        }
        break;
      case 2U:
        {
            var_34 = helper_cc_compute_all_wrapper(var_22, 0UL, var_4, 24U);
            local_sp_2 = local_sp_1;
            rax_1 = var_22;
            if ((var_34 & 64UL) != 0UL) {
                *(uint32_t *)6431580UL = (*(uint32_t *)6431580UL + 1U);
                return rax_1;
            }
            *(uint64_t *)(local_sp_2 + (-8L)) = 4220707UL;
            indirect_placeholder_4();
            *(uint64_t *)(local_sp_2 + (-16L)) = 4220717UL;
            indirect_placeholder_4();
        }
        break;
    }
}

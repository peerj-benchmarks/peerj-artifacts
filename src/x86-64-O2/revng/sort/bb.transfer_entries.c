typedef unsigned char __u_char;
typedef unsigned short __u_short;
typedef unsigned int __u_int;
typedef unsigned long __u_long;
typedef signed char __int8_t;
typedef unsigned char __uint8_t;
typedef short __int16_t;
typedef unsigned short __uint16_t;
typedef int __int32_t;
typedef unsigned int __uint32_t;
typedef long __int64_t;
typedef unsigned long __uint64_t;
typedef __int8_t __int_least8_t;
typedef __uint8_t __uint_least8_t;
typedef __int16_t __int_least16_t;
typedef __uint16_t __uint_least16_t;
typedef __int32_t __int_least32_t;
typedef __uint32_t __uint_least32_t;
typedef __int64_t __int_least64_t;
typedef __uint64_t __uint_least64_t;
typedef long __quad_t;
typedef unsigned long __u_quad_t;
typedef long __intmax_t;
typedef unsigned long __uintmax_t;
typedef unsigned long __dev_t;
typedef unsigned int __uid_t;
typedef unsigned int __gid_t;
typedef unsigned long __ino_t;
typedef unsigned long __ino64_t;
typedef unsigned int __mode_t;
typedef unsigned long __nlink_t;
typedef long __off_t;
typedef long __off64_t;
typedef int __pid_t;
typedef struct {
    int __val[2];
} __fsid_t;
typedef long __clock_t;
typedef unsigned long __rlim_t;
typedef unsigned long __rlim64_t;
typedef unsigned int __id_t;
typedef long __time_t;
typedef unsigned int __useconds_t;
typedef long __suseconds_t;
typedef int __daddr_t;
typedef int __key_t;
typedef int __clockid_t;
typedef void *__timer_t;
typedef long __blksize_t;
typedef long __blkcnt_t;
typedef long __blkcnt64_t;
typedef unsigned long __fsblkcnt_t;
typedef unsigned long __fsblkcnt64_t;
typedef unsigned long __fsfilcnt_t;
typedef unsigned long __fsfilcnt64_t;
typedef long __fsword_t;
typedef long __ssize_t;
typedef long __syscall_slong_t;
typedef unsigned long __syscall_ulong_t;
typedef __off64_t __loff_t;
typedef char *__caddr_t;
typedef long __intptr_t;
typedef unsigned int __socklen_t;
typedef int __sig_atomic_t;
typedef __int8_t int8_t;
typedef __int16_t int16_t;
typedef __int32_t int32_t;
typedef __int64_t int64_t;
typedef __uint8_t uint8_t;
typedef __uint16_t uint16_t;
typedef __uint32_t uint32_t;
typedef __uint64_t uint64_t;
typedef __int_least8_t int_least8_t;
typedef __int_least16_t int_least16_t;
typedef __int_least32_t int_least32_t;
typedef __int_least64_t int_least64_t;
typedef __uint_least8_t uint_least8_t;
typedef __uint_least16_t uint_least16_t;
typedef __uint_least32_t uint_least32_t;
typedef __uint_least64_t uint_least64_t;
typedef signed char int_fast8_t;
typedef long int_fast16_t;
typedef long int_fast32_t;
typedef long int_fast64_t;
typedef unsigned char uint_fast8_t;
typedef unsigned long uint_fast16_t;
typedef unsigned long uint_fast32_t;
typedef unsigned long uint_fast64_t;
typedef long intptr_t;
typedef unsigned long uintptr_t;
typedef __intmax_t intmax_t;
typedef __uintmax_t uintmax_t;
typedef _Bool bool;
struct indirect_placeholder_260_ret_type;
struct indirect_placeholder_260_ret_type {
    uint64_t field_0;
    uint64_t field_1;
};
extern uint64_t revng_init_local_sp(uint64_t param_0);
extern uint64_t init_rbx(void);
extern uint64_t init_rbp(void);
extern uint64_t init_cc_src2(void);
extern uint64_t helper_cc_compute_c_wrapper(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint32_t param_3);
extern uint64_t init_r13(void);
extern uint64_t init_r12(void);
extern uint64_t init_r14(void);
extern uint64_t init_r15(void);
extern uint64_t indirect_placeholder_5(uint64_t param_0, uint64_t param_1);
extern struct indirect_placeholder_260_ret_type indirect_placeholder_260(uint64_t param_0);
uint64_t bb_transfer_entries(uint64_t rdi, uint64_t rsi, uint64_t rdx) {
    uint64_t var_0;
    uint64_t var_1;
    uint64_t var_2;
    uint64_t var_3;
    uint64_t var_4;
    uint64_t var_5;
    uint64_t var_6;
    uint64_t var_7;
    uint64_t var_8;
    uint64_t *var_9;
    uint64_t var_10;
    uint64_t var_11;
    uint64_t local_sp_5;
    uint64_t local_sp_1;
    uint64_t merge;
    uint64_t var_31;
    uint64_t *var_32;
    uint64_t r13_0;
    uint64_t *_pre_phi56;
    uint64_t local_sp_2;
    uint64_t rax_0;
    uint64_t local_sp_0;
    uint64_t *var_36;
    uint64_t var_37;
    uint64_t local_sp_3;
    uint64_t var_33;
    struct indirect_placeholder_260_ret_type var_34;
    uint64_t var_35;
    uint64_t var_38;
    uint64_t r13_0_be;
    uint64_t local_sp_2_be;
    uint64_t local_sp_4;
    uint64_t rbx_0;
    uint64_t rbp_0;
    uint64_t *var_28;
    uint64_t var_12;
    uint64_t *var_13;
    uint64_t *var_14;
    bool var_15;
    uint64_t *var_16;
    uint64_t *var_17;
    uint64_t var_18;
    uint64_t *var_19;
    uint64_t var_20;
    uint64_t var_29;
    uint64_t var_30;
    uint64_t *_cast2;
    uint64_t *var_21;
    uint64_t var_22;
    uint64_t var_23;
    uint64_t var_24;
    uint64_t *_cast1;
    uint64_t var_25;
    uint64_t *var_26;
    uint64_t var_27;
    uint64_t var_39;
    var_0 = revng_init_local_sp(0UL);
    var_1 = init_rbx();
    var_2 = init_rbp();
    var_3 = init_r13();
    var_4 = init_cc_src2();
    var_5 = init_r12();
    var_6 = init_r15();
    var_7 = init_r14();
    *(uint64_t *)(var_0 + (-8L)) = var_6;
    *(uint64_t *)(var_0 + (-16L)) = var_7;
    *(uint64_t *)(var_0 + (-24L)) = var_3;
    *(uint64_t *)(var_0 + (-32L)) = var_5;
    *(uint64_t *)(var_0 + (-40L)) = var_2;
    *(uint64_t *)(var_0 + (-48L)) = var_1;
    var_8 = *(uint64_t *)rsi;
    var_9 = (uint64_t *)(rsi + 8UL);
    var_10 = *var_9;
    var_11 = helper_cc_compute_c_wrapper(var_8 - var_10, var_10, var_4, 17U);
    merge = 1UL;
    r13_0 = var_8;
    if (var_11 == 0UL) {
        return merge;
    }
    var_12 = var_0 + (-56L);
    var_13 = (uint64_t *)(rdi + 24UL);
    var_14 = (uint64_t *)(rdi + 72UL);
    var_15 = ((uint64_t)(unsigned char)rdx == 0UL);
    var_16 = (uint64_t *)(rsi + 24UL);
    merge = 0UL;
    local_sp_2 = var_12;
    while (1U)
        {
            var_17 = (uint64_t *)r13_0;
            var_18 = *var_17;
            rbp_0 = var_18;
            local_sp_3 = local_sp_2;
            local_sp_4 = local_sp_2;
            local_sp_5 = local_sp_2;
            if (var_18 == 0UL) {
                var_39 = r13_0 + 16UL;
                r13_0_be = var_39;
                local_sp_2_be = local_sp_5;
                if (*var_9 > var_39) {
                    break;
                }
                r13_0 = r13_0_be;
                local_sp_2 = local_sp_2_be;
                continue;
            }
            var_19 = (uint64_t *)(r13_0 + 8UL);
            var_20 = *var_19;
            rbx_0 = var_20;
            if (var_20 != 0UL) {
                while (1U)
                    {
                        var_21 = (uint64_t *)rbx_0;
                        var_22 = *var_21;
                        var_23 = local_sp_4 + (-8L);
                        *(uint64_t *)var_23 = 4240322UL;
                        var_24 = indirect_placeholder_5(rdi, var_22);
                        _cast1 = (uint64_t *)var_24;
                        var_25 = *_cast1;
                        var_26 = (uint64_t *)(rbx_0 + 8UL);
                        var_27 = *var_26;
                        local_sp_3 = var_23;
                        rbx_0 = var_27;
                        local_sp_4 = var_23;
                        if (var_25 != 0UL) {
                            var_28 = (uint64_t *)(var_24 + 8UL);
                            *var_26 = *var_28;
                            *var_28 = rbx_0;
                            if (var_27 != 0UL) {
                                continue;
                            }
                            break;
                        }
                        *_cast1 = var_22;
                        *var_13 = (*var_13 + 1UL);
                        *var_21 = 0UL;
                        *var_26 = *var_14;
                        *var_14 = rbx_0;
                        if (var_27 != 0UL) {
                            continue;
                        }
                        break;
                    }
                rbp_0 = *var_17;
            }
            *var_19 = 0UL;
            local_sp_5 = local_sp_3;
            if (!var_15) {
                var_29 = local_sp_3 + (-8L);
                *(uint64_t *)var_29 = 4240443UL;
                var_30 = indirect_placeholder_5(rdi, rbp_0);
                _cast2 = (uint64_t *)var_30;
                local_sp_0 = var_29;
                local_sp_1 = var_29;
                if (*_cast2 == 0UL) {
                    *_cast2 = rbp_0;
                    *var_13 = (*var_13 + 1UL);
                } else {
                    var_31 = *var_14;
                    rax_0 = var_31;
                    if (var_31 == 0UL) {
                        var_32 = (uint64_t *)(var_31 + 8UL);
                        *var_14 = *var_32;
                        _pre_phi56 = var_32;
                    } else {
                        var_33 = local_sp_3 + (-16L);
                        *(uint64_t *)var_33 = 4240536UL;
                        var_34 = indirect_placeholder_260(16UL);
                        var_35 = var_34.field_0;
                        rax_0 = var_35;
                        local_sp_0 = var_33;
                        if (var_35 != 0UL) {
                            break;
                        }
                        _pre_phi56 = (uint64_t *)(var_35 + 8UL);
                    }
                    var_36 = (uint64_t *)(var_30 + 8UL);
                    var_37 = *var_36;
                    *(uint64_t *)rax_0 = rbp_0;
                    *_pre_phi56 = var_37;
                    *var_36 = rax_0;
                    local_sp_1 = local_sp_0;
                }
                *var_17 = 0UL;
                *var_16 = (*var_16 + (-1L));
                var_38 = r13_0 + 16UL;
                r13_0_be = var_38;
                local_sp_2_be = local_sp_1;
                if (*var_9 > var_38) {
                    break;
                }
                r13_0 = r13_0_be;
                local_sp_2 = local_sp_2_be;
                continue;
            }
        }
    return merge;
}

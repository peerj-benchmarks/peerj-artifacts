typedef unsigned char __u_char;
typedef unsigned short __u_short;
typedef unsigned int __u_int;
typedef unsigned long __u_long;
typedef signed char __int8_t;
typedef unsigned char __uint8_t;
typedef short __int16_t;
typedef unsigned short __uint16_t;
typedef int __int32_t;
typedef unsigned int __uint32_t;
typedef long __int64_t;
typedef unsigned long __uint64_t;
typedef __int8_t __int_least8_t;
typedef __uint8_t __uint_least8_t;
typedef __int16_t __int_least16_t;
typedef __uint16_t __uint_least16_t;
typedef __int32_t __int_least32_t;
typedef __uint32_t __uint_least32_t;
typedef __int64_t __int_least64_t;
typedef __uint64_t __uint_least64_t;
typedef long __quad_t;
typedef unsigned long __u_quad_t;
typedef long __intmax_t;
typedef unsigned long __uintmax_t;
typedef unsigned long __dev_t;
typedef unsigned int __uid_t;
typedef unsigned int __gid_t;
typedef unsigned long __ino_t;
typedef unsigned long __ino64_t;
typedef unsigned int __mode_t;
typedef unsigned long __nlink_t;
typedef long __off_t;
typedef long __off64_t;
typedef int __pid_t;
typedef struct {
    int __val[2];
} __fsid_t;
typedef long __clock_t;
typedef unsigned long __rlim_t;
typedef unsigned long __rlim64_t;
typedef unsigned int __id_t;
typedef long __time_t;
typedef unsigned int __useconds_t;
typedef long __suseconds_t;
typedef int __daddr_t;
typedef int __key_t;
typedef int __clockid_t;
typedef void *__timer_t;
typedef long __blksize_t;
typedef long __blkcnt_t;
typedef long __blkcnt64_t;
typedef unsigned long __fsblkcnt_t;
typedef unsigned long __fsblkcnt64_t;
typedef unsigned long __fsfilcnt_t;
typedef unsigned long __fsfilcnt64_t;
typedef long __fsword_t;
typedef long __ssize_t;
typedef long __syscall_slong_t;
typedef unsigned long __syscall_ulong_t;
typedef __off64_t __loff_t;
typedef char *__caddr_t;
typedef long __intptr_t;
typedef unsigned int __socklen_t;
typedef int __sig_atomic_t;
typedef __int8_t int8_t;
typedef __int16_t int16_t;
typedef __int32_t int32_t;
typedef __int64_t int64_t;
typedef __uint8_t uint8_t;
typedef __uint16_t uint16_t;
typedef __uint32_t uint32_t;
typedef __uint64_t uint64_t;
typedef __int_least8_t int_least8_t;
typedef __int_least16_t int_least16_t;
typedef __int_least32_t int_least32_t;
typedef __int_least64_t int_least64_t;
typedef __uint_least8_t uint_least8_t;
typedef __uint_least16_t uint_least16_t;
typedef __uint_least32_t uint_least32_t;
typedef __uint_least64_t uint_least64_t;
typedef signed char int_fast8_t;
typedef long int_fast16_t;
typedef long int_fast32_t;
typedef long int_fast64_t;
typedef unsigned char uint_fast8_t;
typedef unsigned long uint_fast16_t;
typedef unsigned long uint_fast32_t;
typedef unsigned long uint_fast64_t;
typedef long intptr_t;
typedef unsigned long uintptr_t;
typedef __intmax_t intmax_t;
typedef __uintmax_t uintmax_t;
struct indirect_placeholder_230_ret_type;
struct indirect_placeholder_230_ret_type {
    uint64_t field_0;
    uint64_t field_1;
};
extern uint64_t revng_init_local_sp(uint64_t param_0);
extern uint64_t init_rbx(void);
extern uint64_t init_rbp(void);
extern void indirect_placeholder_4(void);
extern uint64_t init_r12(void);
extern uint64_t init_rax(void);
extern uint64_t indirect_placeholder_5(uint64_t param_0, uint64_t param_1);
extern struct indirect_placeholder_230_ret_type indirect_placeholder_230(uint64_t param_0);
uint64_t bb_rpl_unlink(uint64_t rdi) {
    uint64_t var_0;
    uint64_t var_1;
    uint64_t var_2;
    uint64_t var_3;
    uint64_t var_4;
    uint64_t var_5;
    uint64_t local_sp_2;
    uint64_t rax_4;
    uint32_t var_16;
    uint64_t var_17;
    uint64_t local_sp_0;
    uint64_t rax_0;
    uint64_t var_18;
    uint64_t rbx_0;
    uint64_t var_12;
    unsigned char *var_13;
    uint64_t rax_1;
    uint64_t var_19;
    uint64_t var_14;
    uint64_t var_15;
    uint64_t var_11;
    uint64_t local_sp_1;
    uint64_t rax_2;
    uint64_t var_8;
    struct indirect_placeholder_230_ret_type var_9;
    uint64_t var_10;
    uint64_t rax_3;
    uint64_t var_6;
    uint64_t var_7;
    unsigned int loop_state_var;
    var_0 = revng_init_local_sp(0UL);
    var_1 = init_rax();
    var_2 = init_rbx();
    var_3 = init_rbp();
    var_4 = init_r12();
    *(uint64_t *)(var_0 + (-8L)) = var_4;
    *(uint64_t *)(var_0 + (-16L)) = var_3;
    *(uint64_t *)(var_0 + (-24L)) = var_2;
    var_5 = var_0 + (-176L);
    *(uint64_t *)var_5 = 4265395UL;
    indirect_placeholder_4();
    rbx_0 = var_1;
    rax_2 = 0UL;
    local_sp_2 = var_5;
    rax_4 = 0UL;
    if (var_1 == 0UL) {
        *(uint64_t *)(local_sp_2 + (-8L)) = 4265411UL;
        indirect_placeholder_4();
        rax_3 = rax_4;
        return rax_3;
    }
    rax_4 = var_1;
    if (*(unsigned char *)((var_1 + rdi) + (-1L)) == '/') {
        return;
    }
    var_6 = var_0 + (-184L);
    *(uint64_t *)var_6 = 4265442UL;
    var_7 = indirect_placeholder_5(rdi, var_5);
    rax_3 = var_7;
    if ((uint64_t)(uint32_t)var_7 == 0UL) {
        return rax_3;
    }
    var_8 = var_0 + (-192L);
    *(uint64_t *)var_8 = 4265454UL;
    var_9 = indirect_placeholder_230(var_1);
    var_10 = var_9.field_0;
    rax_1 = var_10;
    local_sp_1 = var_8;
    rax_3 = 4294967295UL;
    if (var_10 == 0UL) {
        *(uint64_t *)(local_sp_1 + (-8L)) = 4265555UL;
        indirect_placeholder_4();
        *(uint32_t *)rax_2 = 1U;
    } else {
        var_11 = var_0 + (-200L);
        *(uint64_t *)var_11 = 4265476UL;
        indirect_placeholder_4();
        local_sp_0 = var_11;
        while (1U)
            {
                var_12 = rbx_0 + (-1L);
                var_13 = (unsigned char *)(var_12 + var_10);
                rbx_0 = var_12;
                if (*var_13 == '/') {
                    *var_13 = (unsigned char)'\x00';
                    if (var_12 == 0UL) {
                        continue;
                    }
                    loop_state_var = 0U;
                    break;
                }
                var_14 = var_0 + (-208L);
                *(uint64_t *)var_14 = 4265522UL;
                var_15 = indirect_placeholder_5(var_10, var_11);
                rax_0 = var_15;
                local_sp_0 = var_14;
                if ((uint64_t)(uint32_t)var_15 != 0UL) {
                    loop_state_var = 1U;
                    break;
                }
                var_16 = (uint32_t)((uint16_t)*(uint32_t *)var_6 & (unsigned short)61440U);
                var_17 = (uint64_t)var_16;
                rax_0 = var_17;
                rax_1 = var_17;
                if ((uint64_t)((var_16 + (-40960)) & (-4096)) != 0UL) {
                    loop_state_var = 1U;
                    break;
                }
                loop_state_var = 0U;
                break;
            }
        switch (loop_state_var) {
          case 1U:
            {
                var_18 = var_0 + (-216L);
                *(uint64_t *)var_18 = 4265550UL;
                indirect_placeholder_4();
                local_sp_1 = var_18;
                rax_2 = rax_0;
            }
            break;
          case 0U:
            {
                var_19 = local_sp_0 + (-8L);
                *(uint64_t *)var_19 = 4265509UL;
                indirect_placeholder_4();
                local_sp_2 = var_19;
                rax_4 = rax_1;
                *(uint64_t *)(local_sp_2 + (-8L)) = 4265411UL;
                indirect_placeholder_4();
                rax_3 = rax_4;
                return rax_3;
            }
            break;
        }
    }
}

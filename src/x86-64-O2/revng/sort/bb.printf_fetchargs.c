typedef unsigned char __u_char;
typedef unsigned short __u_short;
typedef unsigned int __u_int;
typedef unsigned long __u_long;
typedef signed char __int8_t;
typedef unsigned char __uint8_t;
typedef short __int16_t;
typedef unsigned short __uint16_t;
typedef int __int32_t;
typedef unsigned int __uint32_t;
typedef long __int64_t;
typedef unsigned long __uint64_t;
typedef __int8_t __int_least8_t;
typedef __uint8_t __uint_least8_t;
typedef __int16_t __int_least16_t;
typedef __uint16_t __uint_least16_t;
typedef __int32_t __int_least32_t;
typedef __uint32_t __uint_least32_t;
typedef __int64_t __int_least64_t;
typedef __uint64_t __uint_least64_t;
typedef long __quad_t;
typedef unsigned long __u_quad_t;
typedef long __intmax_t;
typedef unsigned long __uintmax_t;
typedef unsigned long __dev_t;
typedef unsigned int __uid_t;
typedef unsigned int __gid_t;
typedef unsigned long __ino_t;
typedef unsigned long __ino64_t;
typedef unsigned int __mode_t;
typedef unsigned long __nlink_t;
typedef long __off_t;
typedef long __off64_t;
typedef int __pid_t;
typedef struct {
    int __val[2];
} __fsid_t;
typedef long __clock_t;
typedef unsigned long __rlim_t;
typedef unsigned long __rlim64_t;
typedef unsigned int __id_t;
typedef long __time_t;
typedef unsigned int __useconds_t;
typedef long __suseconds_t;
typedef int __daddr_t;
typedef int __key_t;
typedef int __clockid_t;
typedef void *__timer_t;
typedef long __blksize_t;
typedef long __blkcnt_t;
typedef long __blkcnt64_t;
typedef unsigned long __fsblkcnt_t;
typedef unsigned long __fsblkcnt64_t;
typedef unsigned long __fsfilcnt_t;
typedef unsigned long __fsfilcnt64_t;
typedef long __fsword_t;
typedef long __ssize_t;
typedef long __syscall_slong_t;
typedef unsigned long __syscall_ulong_t;
typedef __off64_t __loff_t;
typedef char *__caddr_t;
typedef long __intptr_t;
typedef unsigned int __socklen_t;
typedef int __sig_atomic_t;
typedef __int8_t int8_t;
typedef __int16_t int16_t;
typedef __int32_t int32_t;
typedef __int64_t int64_t;
typedef __uint8_t uint8_t;
typedef __uint16_t uint16_t;
typedef __uint32_t uint32_t;
typedef __uint64_t uint64_t;
typedef __int_least8_t int_least8_t;
typedef __int_least16_t int_least16_t;
typedef __int_least32_t int_least32_t;
typedef __int_least64_t int_least64_t;
typedef __uint_least8_t uint_least8_t;
typedef __uint_least16_t uint_least16_t;
typedef __uint_least32_t uint_least32_t;
typedef __uint_least64_t uint_least64_t;
typedef signed char int_fast8_t;
typedef long int_fast16_t;
typedef long int_fast32_t;
typedef long int_fast64_t;
typedef unsigned char uint_fast8_t;
typedef unsigned long uint_fast16_t;
typedef unsigned long uint_fast32_t;
typedef unsigned long uint_fast64_t;
typedef long intptr_t;
typedef unsigned long uintptr_t;
typedef __intmax_t intmax_t;
typedef __uintmax_t uintmax_t;
struct type_3;
struct helper_fpop_wrapper_ret_type;
struct helper_fldt_ST0_wrapper_ret_type;
struct type_3 {
};
struct helper_fpop_wrapper_ret_type {
    uint32_t field_0;
    unsigned char field_1;
    unsigned char field_2;
    unsigned char field_3;
    unsigned char field_4;
    unsigned char field_5;
    unsigned char field_6;
    unsigned char field_7;
    unsigned char field_8;
};
struct helper_fldt_ST0_wrapper_ret_type {
    uint32_t field_0;
    uint64_t field_1;
    uint64_t field_2;
    uint64_t field_3;
    uint64_t field_4;
    uint64_t field_5;
    uint64_t field_6;
    uint64_t field_7;
    uint64_t field_8;
    uint16_t field_9;
    uint16_t field_10;
    uint16_t field_11;
    uint16_t field_12;
    uint16_t field_13;
    uint16_t field_14;
    uint16_t field_15;
    uint16_t field_16;
    unsigned char field_17;
    unsigned char field_18;
    unsigned char field_19;
    unsigned char field_20;
    unsigned char field_21;
    unsigned char field_22;
    unsigned char field_23;
    unsigned char field_24;
};
extern void abort(void);
extern uint64_t revng_init_local_sp(uint64_t param_0);
extern uint32_t init_state_0x8480(void);
extern void helper_fstt_ST0_wrapper(struct type_3 *param_0, uint64_t param_1, uint32_t param_2, uint64_t param_3, uint64_t param_4, uint64_t param_5, uint64_t param_6, uint64_t param_7, uint64_t param_8, uint64_t param_9, uint64_t param_10, uint16_t param_11, uint16_t param_12, uint16_t param_13, uint16_t param_14, uint16_t param_15, uint16_t param_16, uint16_t param_17, uint16_t param_18);
extern struct helper_fpop_wrapper_ret_type helper_fpop_wrapper(struct type_3 *param_0, uint32_t param_1);
extern uint64_t init_cc_src2(void);
extern uint64_t helper_cc_compute_c_wrapper(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint32_t param_3);
extern struct helper_fldt_ST0_wrapper_ret_type helper_fldt_ST0_wrapper(struct type_3 *param_0, uint64_t param_1, uint32_t param_2);
typedef _Bool bool;
uint64_t bb_printf_fetchargs(uint64_t rdi, uint64_t rsi) {
    uint64_t var_13;
    uint64_t var_0;
    uint64_t *_cast;
    uint64_t merge;
    uint64_t var_1;
    uint32_t var_2;
    uint32_t *var_3;
    uint64_t *var_4;
    uint64_t *var_5;
    uint32_t *var_6;
    uint64_t rax_0;
    uint64_t rdx_0;
    uint32_t state_0x8480_0;
    uint32_t var_7;
    uint32_t var_8;
    uint64_t var_9;
    uint64_t var_10;
    uint64_t var_11;
    uint64_t r10_0;
    uint64_t var_12;
    uint32_t var_44;
    uint64_t var_45;
    uint64_t var_46;
    uint32_t state_0x8480_1;
    uint32_t var_14;
    uint64_t var_15;
    uint64_t var_16;
    uint64_t var_17;
    uint64_t r10_1;
    uint64_t var_18;
    uint64_t var_19;
    uint64_t var_20;
    struct helper_fldt_ST0_wrapper_ret_type var_21;
    uint32_t var_22;
    struct helper_fpop_wrapper_ret_type var_23;
    uint32_t var_24;
    uint64_t var_25;
    uint64_t var_26;
    uint64_t var_27;
    uint64_t r10_2;
    uint64_t var_28;
    uint32_t var_29;
    uint64_t var_30;
    uint64_t var_31;
    uint64_t var_32;
    uint64_t r10_3;
    uint64_t var_33;
    uint32_t var_34;
    uint64_t var_35;
    uint64_t var_36;
    uint64_t var_37;
    uint64_t r10_4;
    uint64_t var_38;
    uint32_t var_39;
    uint64_t var_40;
    uint64_t var_41;
    uint64_t var_42;
    uint64_t r10_5;
    uint64_t var_43;
    uint64_t var_47;
    uint64_t r10_6;
    uint64_t var_48;
    uint64_t var_49;
    bool switch_state_var;
    revng_init_local_sp(0UL);
    var_0 = init_cc_src2();
    _cast = (uint64_t *)rsi;
    rdx_0 = 0UL;
    merge = 0UL;
    if (*_cast == 0UL) {
        return merge;
    }
    var_1 = *(uint64_t *)(rsi + 8UL);
    var_2 = init_state_0x8480();
    var_3 = (uint32_t *)rdi;
    var_4 = (uint64_t *)(rdi + 16UL);
    var_5 = (uint64_t *)(rdi + 8UL);
    var_6 = (uint32_t *)(rdi + 4UL);
    rax_0 = var_1;
    state_0x8480_0 = var_2;
    merge = 4294967295UL;
    var_7 = *(uint32_t *)rax_0;
    state_0x8480_1 = state_0x8480_0;
    while (var_7 <= 22U)
        {
            switch_state_var = 0;
            switch (*(uint64_t *)(((uint64_t)var_7 << 3UL) + 4319280UL)) {
              case 4296768UL:
                {
                    switch_state_var = 1;
                    break;
                }
                break;
              case 4296720UL:
              case 4296672UL:
              case 4296640UL:
              case 4296592UL:
              case 4296480UL:
              case 4296448UL:
              case 4296416UL:
              case 4296360UL:
                {
                    switch (*(uint64_t *)(((uint64_t)var_7 << 3UL) + 4319280UL)) {
                      case 4296640UL:
                        {
                            var_20 = (*var_5 + 15UL) & (-16L);
                            *var_5 = (var_20 + 16UL);
                            var_21 = helper_fldt_ST0_wrapper((struct type_3 *)(0UL), var_20, state_0x8480_0);
                            var_22 = var_21.field_0;
                            helper_fstt_ST0_wrapper((struct type_3 *)(0UL), rax_0 + 16UL, var_22, var_21.field_1, var_21.field_2, var_21.field_3, var_21.field_4, var_21.field_5, var_21.field_6, var_21.field_7, var_21.field_8, var_21.field_9, var_21.field_10, var_21.field_11, var_21.field_12, var_21.field_13, var_21.field_14, var_21.field_15, var_21.field_16);
                            var_23 = helper_fpop_wrapper((struct type_3 *)(0UL), var_22);
                            state_0x8480_1 = var_23.field_0;
                        }
                        break;
                      case 4296416UL:
                        {
                            var_39 = *var_3;
                            var_40 = (uint64_t)var_39;
                            var_41 = helper_cc_compute_c_wrapper(var_40 + (-48L), 48UL, var_0, 16U);
                            if (var_41 == 0UL) {
                                var_43 = *var_5;
                                *var_5 = (var_43 + 8UL);
                                r10_5 = var_43;
                            } else {
                                var_42 = *var_4 + var_40;
                                *var_3 = (var_39 + 8U);
                                r10_5 = var_42;
                            }
                            *(uint32_t *)(rax_0 + 16UL) = *(uint32_t *)r10_5;
                        }
                        break;
                      case 4296720UL:
                        {
                            var_8 = *var_3;
                            var_9 = (uint64_t)var_8;
                            var_10 = helper_cc_compute_c_wrapper(var_9 + (-48L), 48UL, var_0, 16U);
                            if (var_10 == 0UL) {
                                var_12 = *var_5;
                                *var_5 = (var_12 + 8UL);
                                r10_0 = var_12;
                            } else {
                                var_11 = *var_4 + var_9;
                                *var_3 = (var_8 + 8U);
                                r10_0 = var_11;
                            }
                            var_13 = *(uint64_t *)r10_0;
                            *(uint64_t *)(rax_0 + 16UL) = ((var_13 == 0UL) ? 4319264UL : var_13);
                        }
                        break;
                      case 4296360UL:
                        {
                            var_44 = *var_3;
                            var_45 = (uint64_t)var_44;
                            var_46 = helper_cc_compute_c_wrapper(var_45 + (-48L), 48UL, var_0, 16U);
                            if (var_46 == 0UL) {
                                var_48 = *var_5;
                                *var_5 = (var_48 + 8UL);
                                r10_6 = var_48;
                            } else {
                                var_47 = *var_4 + var_45;
                                *var_3 = (var_44 + 8U);
                                r10_6 = var_47;
                            }
                            *(uint64_t *)(rax_0 + 16UL) = *(uint64_t *)r10_6;
                        }
                        break;
                      case 4296672UL:
                        {
                            var_14 = *var_3;
                            var_15 = (uint64_t)var_14;
                            var_16 = helper_cc_compute_c_wrapper(var_15 + (-48L), 48UL, var_0, 16U);
                            if (var_16 == 0UL) {
                                var_18 = *var_5;
                                *var_5 = (var_18 + 8UL);
                                r10_1 = var_18;
                            } else {
                                var_17 = *var_4 + var_15;
                                *var_3 = (var_14 + 8U);
                                r10_1 = var_17;
                            }
                            var_19 = *(uint64_t *)r10_1;
                            *(uint64_t *)(rax_0 + 16UL) = ((var_19 == 0UL) ? 4319472UL : var_19);
                        }
                        break;
                      case 4296448UL:
                        {
                            var_34 = *var_3;
                            var_35 = (uint64_t)var_34;
                            var_36 = helper_cc_compute_c_wrapper(var_35 + (-48L), 48UL, var_0, 16U);
                            if (var_36 == 0UL) {
                                var_38 = *var_5;
                                *var_5 = (var_38 + 8UL);
                                r10_4 = var_38;
                            } else {
                                var_37 = *var_4 + var_35;
                                *var_3 = (var_34 + 8U);
                                r10_4 = var_37;
                            }
                            *(uint16_t *)(rax_0 + 16UL) = (uint16_t)*(uint32_t *)r10_4;
                        }
                        break;
                      case 4296480UL:
                        {
                            var_29 = *var_3;
                            var_30 = (uint64_t)var_29;
                            var_31 = helper_cc_compute_c_wrapper(var_30 + (-48L), 48UL, var_0, 16U);
                            if (var_31 == 0UL) {
                                var_33 = *var_5;
                                *var_5 = (var_33 + 8UL);
                                r10_3 = var_33;
                            } else {
                                var_32 = *var_4 + var_30;
                                *var_3 = (var_29 + 8U);
                                r10_3 = var_32;
                            }
                            *(unsigned char *)(rax_0 + 16UL) = (unsigned char)*(uint32_t *)r10_3;
                        }
                        break;
                      case 4296592UL:
                        {
                            var_24 = *var_6;
                            var_25 = (uint64_t)var_24;
                            var_26 = helper_cc_compute_c_wrapper(var_25 + (-176L), 176UL, var_0, 16U);
                            if (var_26 == 0UL) {
                                var_28 = *var_5;
                                *var_5 = (var_28 + 8UL);
                                r10_2 = var_28;
                            } else {
                                var_27 = *var_4 + var_25;
                                *var_6 = (var_24 + 16U);
                                r10_2 = var_27;
                            }
                            *(uint64_t *)(rax_0 + 16UL) = *(uint64_t *)r10_2;
                        }
                        break;
                    }
                    var_49 = rdx_0 + 1UL;
                    rdx_0 = var_49;
                    state_0x8480_0 = state_0x8480_1;
                    merge = 0UL;
                    if (*_cast > var_49) {
                        switch_state_var = 1;
                        break;
                    }
                    rax_0 = rax_0 + 32UL;
                    var_7 = *(uint32_t *)rax_0;
                    state_0x8480_1 = state_0x8480_0;
                    continue;
                }
                break;
              default:
                {
                    abort();
                }
                break;
            }
            if (switch_state_var)
                break;
        }
    return merge;
}

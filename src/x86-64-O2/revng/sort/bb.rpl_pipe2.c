typedef unsigned char __u_char;
typedef unsigned short __u_short;
typedef unsigned int __u_int;
typedef unsigned long __u_long;
typedef signed char __int8_t;
typedef unsigned char __uint8_t;
typedef short __int16_t;
typedef unsigned short __uint16_t;
typedef int __int32_t;
typedef unsigned int __uint32_t;
typedef long __int64_t;
typedef unsigned long __uint64_t;
typedef __int8_t __int_least8_t;
typedef __uint8_t __uint_least8_t;
typedef __int16_t __int_least16_t;
typedef __uint16_t __uint_least16_t;
typedef __int32_t __int_least32_t;
typedef __uint32_t __uint_least32_t;
typedef __int64_t __int_least64_t;
typedef __uint64_t __uint_least64_t;
typedef long __quad_t;
typedef unsigned long __u_quad_t;
typedef long __intmax_t;
typedef unsigned long __uintmax_t;
typedef unsigned long __dev_t;
typedef unsigned int __uid_t;
typedef unsigned int __gid_t;
typedef unsigned long __ino_t;
typedef unsigned long __ino64_t;
typedef unsigned int __mode_t;
typedef unsigned long __nlink_t;
typedef long __off_t;
typedef long __off64_t;
typedef int __pid_t;
typedef struct {
    int __val[2];
} __fsid_t;
typedef long __clock_t;
typedef unsigned long __rlim_t;
typedef unsigned long __rlim64_t;
typedef unsigned int __id_t;
typedef long __time_t;
typedef unsigned int __useconds_t;
typedef long __suseconds_t;
typedef int __daddr_t;
typedef int __key_t;
typedef int __clockid_t;
typedef void *__timer_t;
typedef long __blksize_t;
typedef long __blkcnt_t;
typedef long __blkcnt64_t;
typedef unsigned long __fsblkcnt_t;
typedef unsigned long __fsblkcnt64_t;
typedef unsigned long __fsfilcnt_t;
typedef unsigned long __fsfilcnt64_t;
typedef long __fsword_t;
typedef long __ssize_t;
typedef long __syscall_slong_t;
typedef unsigned long __syscall_ulong_t;
typedef __off64_t __loff_t;
typedef char *__caddr_t;
typedef long __intptr_t;
typedef unsigned int __socklen_t;
typedef int __sig_atomic_t;
typedef __int8_t int8_t;
typedef __int16_t int16_t;
typedef __int32_t int32_t;
typedef __int64_t int64_t;
typedef __uint8_t uint8_t;
typedef __uint16_t uint16_t;
typedef __uint32_t uint32_t;
typedef __uint64_t uint64_t;
typedef __int_least8_t int_least8_t;
typedef __int_least16_t int_least16_t;
typedef __int_least32_t int_least32_t;
typedef __int_least64_t int_least64_t;
typedef __uint_least8_t uint_least8_t;
typedef __uint_least16_t uint_least16_t;
typedef __uint_least32_t uint_least32_t;
typedef __uint_least64_t uint_least64_t;
typedef signed char int_fast8_t;
typedef long int_fast16_t;
typedef long int_fast32_t;
typedef long int_fast64_t;
typedef unsigned char uint_fast8_t;
typedef unsigned long uint_fast16_t;
typedef unsigned long uint_fast32_t;
typedef unsigned long uint_fast64_t;
typedef long intptr_t;
typedef unsigned long uintptr_t;
typedef __intmax_t intmax_t;
typedef __uintmax_t uintmax_t;
typedef _Bool bool;
struct indirect_placeholder_238_ret_type;
struct indirect_placeholder_239_ret_type;
struct indirect_placeholder_240_ret_type;
struct indirect_placeholder_241_ret_type;
struct indirect_placeholder_242_ret_type;
struct indirect_placeholder_243_ret_type;
struct indirect_placeholder_244_ret_type;
struct indirect_placeholder_245_ret_type;
struct indirect_placeholder_238_ret_type {
    uint64_t field_0;
    uint64_t field_1;
};
struct indirect_placeholder_239_ret_type {
    uint64_t field_0;
    uint64_t field_1;
};
struct indirect_placeholder_240_ret_type {
    uint64_t field_0;
    uint64_t field_1;
};
struct indirect_placeholder_241_ret_type {
    uint64_t field_0;
    uint64_t field_1;
};
struct indirect_placeholder_242_ret_type {
    uint64_t field_0;
    uint64_t field_1;
};
struct indirect_placeholder_243_ret_type {
    uint64_t field_0;
    uint64_t field_1;
};
struct indirect_placeholder_244_ret_type {
    uint64_t field_0;
    uint64_t field_1;
};
struct indirect_placeholder_245_ret_type {
    uint64_t field_0;
    uint64_t field_1;
};
extern uint64_t revng_init_local_sp(uint64_t param_0);
extern uint64_t init_rbx(void);
extern uint64_t init_rbp(void);
extern void indirect_placeholder_4(void);
extern uint64_t init_r13(void);
extern uint64_t init_r12(void);
extern uint64_t init_r14(void);
extern uint64_t init_rcx(void);
extern struct indirect_placeholder_238_ret_type indirect_placeholder_238(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint64_t param_3, uint64_t param_4);
extern struct indirect_placeholder_239_ret_type indirect_placeholder_239(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint64_t param_3, uint64_t param_4);
extern struct indirect_placeholder_240_ret_type indirect_placeholder_240(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint64_t param_3, uint64_t param_4);
extern struct indirect_placeholder_241_ret_type indirect_placeholder_241(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint64_t param_3, uint64_t param_4);
extern struct indirect_placeholder_242_ret_type indirect_placeholder_242(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint64_t param_3, uint64_t param_4);
extern struct indirect_placeholder_243_ret_type indirect_placeholder_243(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint64_t param_3, uint64_t param_4);
extern struct indirect_placeholder_244_ret_type indirect_placeholder_244(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint64_t param_3, uint64_t param_4);
extern struct indirect_placeholder_245_ret_type indirect_placeholder_245(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint64_t param_3, uint64_t param_4);
uint64_t bb_rpl_pipe2(uint64_t rdi, uint64_t rsi) {
    struct indirect_placeholder_241_ret_type var_60;
    struct indirect_placeholder_240_ret_type var_25;
    struct indirect_placeholder_239_ret_type var_30;
    struct indirect_placeholder_238_ret_type var_37;
    struct indirect_placeholder_242_ret_type var_53;
    uint64_t var_0;
    uint64_t var_1;
    uint64_t var_2;
    uint64_t var_3;
    uint64_t var_4;
    uint64_t var_5;
    uint64_t var_6;
    uint32_t var_7;
    uint64_t var_8;
    uint32_t *_cast;
    uint32_t var_9;
    uint32_t *var_10;
    uint32_t var_11;
    uint64_t var_33;
    uint64_t var_34;
    uint64_t var_35;
    uint64_t var_36;
    uint64_t var_38;
    uint64_t local_sp_1;
    uint64_t var_27;
    uint64_t var_28;
    uint64_t var_29;
    uint64_t var_31;
    uint32_t var_32;
    uint64_t var_21;
    uint64_t var_22;
    uint64_t var_23;
    uint64_t var_24;
    uint64_t var_26;
    uint64_t rcx_0;
    uint64_t var_56;
    uint64_t var_57;
    uint64_t var_58;
    uint64_t var_59;
    uint64_t var_61;
    uint64_t var_50;
    uint64_t var_51;
    uint64_t var_52;
    uint64_t var_54;
    uint32_t var_55;
    uint64_t var_44;
    uint64_t var_45;
    uint64_t var_46;
    uint64_t var_47;
    struct indirect_placeholder_243_ret_type var_48;
    uint64_t var_49;
    uint64_t local_sp_0;
    uint64_t rax_0;
    uint32_t *var_62;
    uint32_t var_63;
    uint64_t var_39;
    uint64_t var_40;
    struct indirect_placeholder_244_ret_type var_41;
    uint64_t var_42;
    uint32_t var_43;
    uint64_t var_16;
    uint64_t var_17;
    struct indirect_placeholder_245_ret_type var_18;
    uint64_t var_19;
    uint32_t var_20;
    uint32_t var_12;
    bool var_13;
    uint64_t var_14;
    uint64_t *var_15;
    var_0 = revng_init_local_sp(0UL);
    var_1 = init_rcx();
    var_2 = init_rbx();
    var_3 = init_rbp();
    var_4 = init_r13();
    var_5 = init_r12();
    var_6 = init_r14();
    var_7 = *(uint32_t *)6432896UL;
    var_8 = (uint64_t)var_7;
    *(uint64_t *)(var_0 + (-8L)) = var_6;
    *(uint64_t *)(var_0 + (-16L)) = var_4;
    *(uint64_t *)(var_0 + (-24L)) = var_5;
    *(uint64_t *)(var_0 + (-32L)) = var_3;
    *(uint64_t *)(var_0 + (-40L)) = var_2;
    _cast = (uint32_t *)rdi;
    var_9 = *_cast;
    var_10 = (uint32_t *)(rdi + 4UL);
    var_11 = *var_10;
    rcx_0 = var_1;
    if ((int)var_7 < (int)0U) {
        *(uint64_t *)(var_0 + (-48L)) = 4245220UL;
        indirect_placeholder_4();
        *(uint32_t *)6432896UL = 1U;
        return var_8;
    }
    var_12 = (uint32_t)rsi;
    var_13 = ((uint64_t)(var_12 & (-526337)) == 0UL);
    var_14 = var_0 + (-48L);
    var_15 = (uint64_t *)var_14;
    local_sp_1 = var_14;
    if (!var_13) {
        *var_15 = 4245597UL;
        indirect_placeholder_4();
        *(uint32_t *)var_8 = 22U;
        return 4294967295UL;
    }
    *var_15 = 4245295UL;
    indirect_placeholder_4();
    if (!!1) {
        return;
    }
    if ((uint64_t)((uint16_t)rsi & (unsigned short)2048U) != 0UL) {
        var_16 = (uint64_t)*var_10;
        var_17 = var_0 + (-56L);
        *(uint64_t *)var_17 = 4245505UL;
        var_18 = indirect_placeholder_245(0UL, var_1, var_16, 3UL, 0UL);
        var_19 = var_18.field_0;
        var_20 = (uint32_t)var_19;
        local_sp_0 = var_17;
        rax_0 = var_19;
        if ((int)var_20 >= (int)0U) {
            *(uint64_t *)(local_sp_0 + (-8L)) = 4245445UL;
            indirect_placeholder_4();
            var_62 = (uint32_t *)rax_0;
            var_63 = *var_62;
            *(uint64_t *)(local_sp_0 + (-16L)) = 4245454UL;
            indirect_placeholder_4();
            *(uint64_t *)(local_sp_0 + (-24L)) = 4245462UL;
            indirect_placeholder_4();
            *_cast = var_9;
            *var_10 = var_11;
            *(uint64_t *)(local_sp_0 + (-32L)) = 4245474UL;
            indirect_placeholder_4();
            *var_62 = var_63;
            return 4294967295UL;
        }
        var_21 = var_18.field_1;
        var_22 = (uint64_t)*var_10;
        var_23 = (uint64_t)(var_20 & (-65281)) | ((uint64_t)((uint16_t)var_19 & (unsigned short)63232U) | 2048UL);
        var_24 = var_0 + (-64L);
        *(uint64_t *)var_24 = 4245529UL;
        var_25 = indirect_placeholder_240(0UL, var_21, var_22, 4UL, var_23);
        var_26 = var_25.field_0;
        local_sp_0 = var_24;
        rax_0 = var_26;
        if ((uint64_t)((uint32_t)var_26 + 1U) != 0UL) {
            *(uint64_t *)(local_sp_0 + (-8L)) = 4245445UL;
            indirect_placeholder_4();
            var_62 = (uint32_t *)rax_0;
            var_63 = *var_62;
            *(uint64_t *)(local_sp_0 + (-16L)) = 4245454UL;
            indirect_placeholder_4();
            *(uint64_t *)(local_sp_0 + (-24L)) = 4245462UL;
            indirect_placeholder_4();
            *_cast = var_9;
            *var_10 = var_11;
            *(uint64_t *)(local_sp_0 + (-32L)) = 4245474UL;
            indirect_placeholder_4();
            *var_62 = var_63;
            return 4294967295UL;
        }
        var_27 = var_25.field_1;
        var_28 = (uint64_t)*_cast;
        var_29 = var_0 + (-72L);
        *(uint64_t *)var_29 = 4245550UL;
        var_30 = indirect_placeholder_239(0UL, var_27, var_28, 3UL, 0UL);
        var_31 = var_30.field_0;
        var_32 = (uint32_t)var_31;
        local_sp_0 = var_29;
        rax_0 = var_31;
        if ((int)var_32 >= (int)0U) {
            *(uint64_t *)(local_sp_0 + (-8L)) = 4245445UL;
            indirect_placeholder_4();
            var_62 = (uint32_t *)rax_0;
            var_63 = *var_62;
            *(uint64_t *)(local_sp_0 + (-16L)) = 4245454UL;
            indirect_placeholder_4();
            *(uint64_t *)(local_sp_0 + (-24L)) = 4245462UL;
            indirect_placeholder_4();
            *_cast = var_9;
            *var_10 = var_11;
            *(uint64_t *)(local_sp_0 + (-32L)) = 4245474UL;
            indirect_placeholder_4();
            *var_62 = var_63;
            return 4294967295UL;
        }
        var_33 = var_30.field_1;
        var_34 = (uint64_t)*_cast;
        var_35 = (uint64_t)(var_32 & (-65281)) | ((uint64_t)((uint16_t)var_31 & (unsigned short)63232U) | 2048UL);
        var_36 = var_0 + (-80L);
        *(uint64_t *)var_36 = 4245573UL;
        var_37 = indirect_placeholder_238(0UL, var_33, var_34, 4UL, var_35);
        var_38 = var_37.field_0;
        local_sp_0 = var_36;
        rax_0 = var_38;
        local_sp_1 = var_36;
        rcx_0 = var_37.field_1;
        if ((uint64_t)((uint32_t)var_38 + 1U) != 0UL) {
            *(uint64_t *)(local_sp_0 + (-8L)) = 4245445UL;
            indirect_placeholder_4();
            var_62 = (uint32_t *)rax_0;
            var_63 = *var_62;
            *(uint64_t *)(local_sp_0 + (-16L)) = 4245454UL;
            indirect_placeholder_4();
            *(uint64_t *)(local_sp_0 + (-24L)) = 4245462UL;
            indirect_placeholder_4();
            *_cast = var_9;
            *var_10 = var_11;
            *(uint64_t *)(local_sp_0 + (-32L)) = 4245474UL;
            indirect_placeholder_4();
            *var_62 = var_63;
            return 4294967295UL;
        }
    }
    if ((uint64_t)(var_12 & 524288U) != 0UL) {
        return 4294967295UL;
    }
    var_39 = (uint64_t)*var_10;
    var_40 = local_sp_1 + (-8L);
    *(uint64_t *)var_40 = 4245361UL;
    var_41 = indirect_placeholder_244(0UL, rcx_0, var_39, 1UL, 0UL);
    var_42 = var_41.field_0;
    var_43 = (uint32_t)var_42;
    local_sp_0 = var_40;
    rax_0 = var_42;
    if ((int)var_43 >= (int)0U) {
        var_44 = var_41.field_1;
        var_45 = (uint64_t)*var_10;
        var_46 = (uint64_t)(var_43 & (-2)) | 1UL;
        var_47 = local_sp_1 + (-16L);
        *(uint64_t *)var_47 = 4245385UL;
        var_48 = indirect_placeholder_243(0UL, var_44, var_45, 2UL, var_46);
        var_49 = var_48.field_0;
        local_sp_0 = var_47;
        rax_0 = var_49;
        var_50 = var_48.field_1;
        var_51 = (uint64_t)*_cast;
        var_52 = local_sp_1 + (-24L);
        *(uint64_t *)var_52 = 4245406UL;
        var_53 = indirect_placeholder_242(0UL, var_50, var_51, 1UL, 0UL);
        var_54 = var_53.field_0;
        var_55 = (uint32_t)var_54;
        local_sp_0 = var_52;
        rax_0 = var_54;
        var_56 = var_53.field_1;
        var_57 = (uint64_t)*_cast;
        var_58 = (uint64_t)(var_55 & (-2)) | 1UL;
        var_59 = local_sp_1 + (-32L);
        *(uint64_t *)var_59 = 4245429UL;
        var_60 = indirect_placeholder_241(0UL, var_56, var_57, 2UL, var_58);
        var_61 = var_60.field_0;
        local_sp_0 = var_59;
        rax_0 = var_61;
        if ((uint64_t)((uint32_t)var_49 + 1U) != 0UL & (int)var_55 >= (int)0U & (uint64_t)((uint32_t)var_61 + 1U) != 0UL) {
            return 4294967295UL;
        }
    }
    *(uint64_t *)(local_sp_0 + (-8L)) = 4245445UL;
    indirect_placeholder_4();
    var_62 = (uint32_t *)rax_0;
    var_63 = *var_62;
    *(uint64_t *)(local_sp_0 + (-16L)) = 4245454UL;
    indirect_placeholder_4();
    *(uint64_t *)(local_sp_0 + (-24L)) = 4245462UL;
    indirect_placeholder_4();
    *_cast = var_9;
    *var_10 = var_11;
    *(uint64_t *)(local_sp_0 + (-32L)) = 4245474UL;
    indirect_placeholder_4();
    *var_62 = var_63;
}

typedef unsigned char __u_char;
typedef unsigned short __u_short;
typedef unsigned int __u_int;
typedef unsigned long __u_long;
typedef signed char __int8_t;
typedef unsigned char __uint8_t;
typedef short __int16_t;
typedef unsigned short __uint16_t;
typedef int __int32_t;
typedef unsigned int __uint32_t;
typedef long __int64_t;
typedef unsigned long __uint64_t;
typedef __int8_t __int_least8_t;
typedef __uint8_t __uint_least8_t;
typedef __int16_t __int_least16_t;
typedef __uint16_t __uint_least16_t;
typedef __int32_t __int_least32_t;
typedef __uint32_t __uint_least32_t;
typedef __int64_t __int_least64_t;
typedef __uint64_t __uint_least64_t;
typedef long __quad_t;
typedef unsigned long __u_quad_t;
typedef long __intmax_t;
typedef unsigned long __uintmax_t;
typedef unsigned long __dev_t;
typedef unsigned int __uid_t;
typedef unsigned int __gid_t;
typedef unsigned long __ino_t;
typedef unsigned long __ino64_t;
typedef unsigned int __mode_t;
typedef unsigned long __nlink_t;
typedef long __off_t;
typedef long __off64_t;
typedef int __pid_t;
typedef struct {
    int __val[2];
} __fsid_t;
typedef long __clock_t;
typedef unsigned long __rlim_t;
typedef unsigned long __rlim64_t;
typedef unsigned int __id_t;
typedef long __time_t;
typedef unsigned int __useconds_t;
typedef long __suseconds_t;
typedef int __daddr_t;
typedef int __key_t;
typedef int __clockid_t;
typedef void *__timer_t;
typedef long __blksize_t;
typedef long __blkcnt_t;
typedef long __blkcnt64_t;
typedef unsigned long __fsblkcnt_t;
typedef unsigned long __fsblkcnt64_t;
typedef unsigned long __fsfilcnt_t;
typedef unsigned long __fsfilcnt64_t;
typedef long __fsword_t;
typedef long __ssize_t;
typedef long __syscall_slong_t;
typedef unsigned long __syscall_ulong_t;
typedef __off64_t __loff_t;
typedef char *__caddr_t;
typedef long __intptr_t;
typedef unsigned int __socklen_t;
typedef int __sig_atomic_t;
typedef __int8_t int8_t;
typedef __int16_t int16_t;
typedef __int32_t int32_t;
typedef __int64_t int64_t;
typedef __uint8_t uint8_t;
typedef __uint16_t uint16_t;
typedef __uint32_t uint32_t;
typedef __uint64_t uint64_t;
typedef __int_least8_t int_least8_t;
typedef __int_least16_t int_least16_t;
typedef __int_least32_t int_least32_t;
typedef __int_least64_t int_least64_t;
typedef __uint_least8_t uint_least8_t;
typedef __uint_least16_t uint_least16_t;
typedef __uint_least32_t uint_least32_t;
typedef __uint_least64_t uint_least64_t;
typedef signed char int_fast8_t;
typedef long int_fast16_t;
typedef long int_fast32_t;
typedef long int_fast64_t;
typedef unsigned char uint_fast8_t;
typedef unsigned long uint_fast16_t;
typedef unsigned long uint_fast32_t;
typedef unsigned long uint_fast64_t;
typedef long intptr_t;
typedef unsigned long uintptr_t;
typedef __intmax_t intmax_t;
typedef __uintmax_t uintmax_t;
typedef _Bool bool;
struct helper_pxor_xmm_wrapper_110_ret_type;
struct type_4;
struct type_6;
struct helper_ucomisd_wrapper_109_ret_type;
struct helper_ucomisd_wrapper_ret_type;
struct helper_pxor_xmm_wrapper_111_ret_type;
struct helper_mulsd_wrapper_112_ret_type;
struct helper_pxor_xmm_wrapper_ret_type;
struct helper_cvtsi2sd_wrapper_121_ret_type;
struct helper_pxor_xmm_wrapper_118_ret_type;
struct helper_cvtsi2sd_wrapper_126_ret_type;
struct helper_divsd_wrapper_ret_type;
struct helper_divsd_wrapper_166_ret_type;
struct helper_mulsd_wrapper_167_ret_type;
struct helper_ucomisd_wrapper_168_ret_type;
struct helper_ucomisd_wrapper_116_ret_type;
struct helper_cvtsi2sd_wrapper_ret_type;
struct helper_divsd_wrapper_169_ret_type;
struct helper_divsd_wrapper_170_ret_type;
struct helper_addsd_wrapper_125_ret_type;
struct helper_pxor_xmm_wrapper_110_ret_type {
    uint64_t field_0;
    uint64_t field_1;
};
struct type_4 {
};
struct type_6 {
};
struct helper_ucomisd_wrapper_109_ret_type {
    uint64_t field_0;
    unsigned char field_1;
};
struct helper_ucomisd_wrapper_ret_type {
    uint64_t field_0;
    unsigned char field_1;
};
struct helper_pxor_xmm_wrapper_111_ret_type {
    uint64_t field_0;
    uint64_t field_1;
};
struct helper_mulsd_wrapper_112_ret_type {
    uint64_t field_0;
    unsigned char field_1;
};
struct helper_pxor_xmm_wrapper_ret_type {
    uint64_t field_0;
    uint64_t field_1;
};
struct helper_cvtsi2sd_wrapper_121_ret_type {
    uint64_t field_0;
};
struct helper_pxor_xmm_wrapper_118_ret_type {
    uint64_t field_0;
    uint64_t field_1;
};
struct helper_cvtsi2sd_wrapper_126_ret_type {
    uint64_t field_0;
};
struct helper_divsd_wrapper_ret_type {
    uint64_t field_0;
    unsigned char field_1;
};
struct helper_divsd_wrapper_166_ret_type {
    uint64_t field_0;
    unsigned char field_1;
};
struct helper_mulsd_wrapper_167_ret_type {
    uint64_t field_0;
    unsigned char field_1;
};
struct helper_ucomisd_wrapper_168_ret_type {
    uint64_t field_0;
    unsigned char field_1;
};
struct helper_ucomisd_wrapper_116_ret_type {
    uint64_t field_0;
    unsigned char field_1;
};
struct helper_cvtsi2sd_wrapper_ret_type {
    uint64_t field_0;
};
struct helper_divsd_wrapper_169_ret_type {
    uint64_t field_0;
    unsigned char field_1;
};
struct helper_divsd_wrapper_170_ret_type {
    uint64_t field_0;
    unsigned char field_1;
};
struct helper_addsd_wrapper_125_ret_type {
    uint64_t field_0;
    unsigned char field_1;
};
extern uint64_t revng_init_local_sp(uint64_t param_0);
extern uint64_t init_rbx(void);
extern uint64_t init_rbp(void);
extern void indirect_placeholder_1(void);
extern unsigned char init_state_0x8549(void);
extern unsigned char init_state_0x854c(void);
extern unsigned char init_state_0x8548(void);
extern unsigned char init_state_0x854b(void);
extern unsigned char init_state_0x8547(void);
extern unsigned char init_state_0x854d(void);
extern struct helper_pxor_xmm_wrapper_110_ret_type helper_pxor_xmm_wrapper_110(struct type_4 *param_0, struct type_6 *param_1, struct type_6 *param_2, uint64_t param_3, uint64_t param_4);
extern uint64_t init_cc_src2(void);
extern uint64_t helper_cc_compute_c_wrapper(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint32_t param_3);
extern uint64_t init_state_0x8598(void);
extern uint64_t init_state_0x85a0(void);
extern uint64_t init_state_0x85d8(void);
extern uint64_t init_state_0x85e0(void);
extern uint64_t init_r13(void);
extern uint64_t init_r12(void);
extern uint64_t init_r14(void);
extern struct helper_ucomisd_wrapper_109_ret_type helper_ucomisd_wrapper_109(struct type_4 *param_0, struct type_6 *param_1, struct type_6 *param_2, uint64_t param_3, uint64_t param_4, unsigned char param_5, unsigned char param_6);
extern struct helper_ucomisd_wrapper_ret_type helper_ucomisd_wrapper(struct type_4 *param_0, struct type_6 *param_1, struct type_6 *param_2, uint64_t param_3, uint64_t param_4, unsigned char param_5, unsigned char param_6);
extern struct helper_pxor_xmm_wrapper_111_ret_type helper_pxor_xmm_wrapper_111(struct type_4 *param_0, struct type_6 *param_1, struct type_6 *param_2, uint64_t param_3, uint64_t param_4);
extern struct helper_mulsd_wrapper_112_ret_type helper_mulsd_wrapper_112(struct type_4 *param_0, struct type_6 *param_1, struct type_6 *param_2, uint64_t param_3, uint64_t param_4, unsigned char param_5, unsigned char param_6, unsigned char param_7, unsigned char param_8, unsigned char param_9, unsigned char param_10);
extern uint64_t helper_cc_compute_all_wrapper(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint32_t param_3);
extern struct helper_pxor_xmm_wrapper_ret_type helper_pxor_xmm_wrapper(struct type_4 *param_0, struct type_6 *param_1, struct type_6 *param_2, uint64_t param_3, uint64_t param_4);
extern struct helper_cvtsi2sd_wrapper_121_ret_type helper_cvtsi2sd_wrapper_121(struct type_4 *param_0, struct type_6 *param_1, uint32_t param_2);
extern uint64_t init_r15(void);
extern struct helper_pxor_xmm_wrapper_118_ret_type helper_pxor_xmm_wrapper_118(struct type_4 *param_0, struct type_6 *param_1, struct type_6 *param_2, uint64_t param_3, uint64_t param_4);
extern struct helper_cvtsi2sd_wrapper_126_ret_type helper_cvtsi2sd_wrapper_126(struct type_4 *param_0, struct type_6 *param_1, uint32_t param_2);
extern struct helper_divsd_wrapper_ret_type helper_divsd_wrapper(struct type_4 *param_0, struct type_6 *param_1, struct type_6 *param_2, uint64_t param_3, uint64_t param_4, unsigned char param_5, unsigned char param_6, unsigned char param_7, unsigned char param_8, unsigned char param_9, unsigned char param_10);
extern struct helper_divsd_wrapper_166_ret_type helper_divsd_wrapper_166(struct type_4 *param_0, struct type_6 *param_1, struct type_6 *param_2, uint64_t param_3, uint64_t param_4, unsigned char param_5, unsigned char param_6, unsigned char param_7, unsigned char param_8, unsigned char param_9, unsigned char param_10);
extern struct helper_mulsd_wrapper_167_ret_type helper_mulsd_wrapper_167(struct type_4 *param_0, struct type_6 *param_1, struct type_6 *param_2, uint64_t param_3, uint64_t param_4, unsigned char param_5, unsigned char param_6, unsigned char param_7, unsigned char param_8, unsigned char param_9, unsigned char param_10);
extern struct helper_ucomisd_wrapper_168_ret_type helper_ucomisd_wrapper_168(struct type_4 *param_0, struct type_6 *param_1, struct type_6 *param_2, uint64_t param_3, uint64_t param_4, unsigned char param_5, unsigned char param_6);
extern struct helper_ucomisd_wrapper_116_ret_type helper_ucomisd_wrapper_116(struct type_4 *param_0, struct type_6 *param_1, struct type_6 *param_2, uint64_t param_3, uint64_t param_4, unsigned char param_5, unsigned char param_6);
extern struct helper_cvtsi2sd_wrapper_ret_type helper_cvtsi2sd_wrapper(struct type_4 *param_0, struct type_6 *param_1, uint32_t param_2);
extern struct helper_divsd_wrapper_169_ret_type helper_divsd_wrapper_169(struct type_4 *param_0, struct type_6 *param_1, struct type_6 *param_2, uint64_t param_3, uint64_t param_4, unsigned char param_5, unsigned char param_6, unsigned char param_7, unsigned char param_8, unsigned char param_9, unsigned char param_10);
extern struct helper_divsd_wrapper_170_ret_type helper_divsd_wrapper_170(struct type_4 *param_0, struct type_6 *param_1, struct type_6 *param_2, uint64_t param_3, uint64_t param_4, unsigned char param_5, unsigned char param_6, unsigned char param_7, unsigned char param_8, unsigned char param_9, unsigned char param_10);
extern struct helper_addsd_wrapper_125_ret_type helper_addsd_wrapper_125(struct type_4 *param_0, struct type_6 *param_1, struct type_6 *param_2, uint64_t param_3, uint64_t param_4, unsigned char param_5, unsigned char param_6, unsigned char param_7, unsigned char param_8, unsigned char param_9, unsigned char param_10);
uint64_t bb_parse_number(uint64_t rcx, uint64_t r9, uint64_t rdi, uint64_t r8, uint64_t rdx, uint64_t rsi) {
    uint64_t state_0x8558_2;
    unsigned char state_0x8549_4;
    uint64_t state_0x85a0_2;
    uint64_t state_0x8598_5;
    uint64_t var_0;
    uint64_t var_1;
    uint64_t var_2;
    uint64_t var_3;
    uint64_t var_4;
    uint64_t var_5;
    uint64_t var_6;
    uint64_t var_7;
    uint64_t var_8;
    uint64_t var_9;
    uint64_t var_10;
    uint64_t var_11;
    unsigned char var_12;
    unsigned char var_13;
    unsigned char var_14;
    unsigned char var_15;
    unsigned char var_16;
    unsigned char var_17;
    uint64_t var_19;
    uint64_t var_20;
    uint64_t var_77;
    uint64_t rax_6;
    struct helper_pxor_xmm_wrapper_ret_type var_18;
    struct helper_ucomisd_wrapper_ret_type var_74;
    uint32_t var_21;
    uint64_t var_22;
    uint64_t var_23;
    uint64_t var_24;
    uint64_t var_25;
    uint32_t var_26;
    bool var_27;
    uint64_t rdx5_3;
    uint64_t state_0x8558_3;
    bool var_70;
    uint64_t var_71;
    uint64_t state_0x8598_0;
    uint64_t rax_0;
    uint64_t rbx_0;
    uint64_t state_0x85a0_3;
    uint64_t rdx5_0;
    uint64_t var_72;
    uint64_t rax_7;
    uint64_t rax_2;
    uint64_t rbx_5;
    uint64_t rbx_1;
    uint64_t rdx5_1;
    unsigned char state_0x8549_5;
    uint64_t state_0x8558_0;
    uint64_t r12_0;
    unsigned char state_0x8549_0;
    uint64_t state_0x8598_6;
    uint64_t state_0x85a0_0;
    uint64_t cc_src_0;
    unsigned char var_28;
    uint64_t var_29;
    uint32_t var_30;
    uint32_t var_31;
    uint64_t var_32;
    uint64_t storemerge2_in;
    uint64_t var_51;
    uint64_t var_52;
    uint64_t var_53;
    uint64_t rax_1;
    uint64_t var_54;
    unsigned char var_55;
    uint64_t var_56;
    uint64_t *var_57;
    uint64_t *var_58;
    uint64_t var_59;
    uint32_t *var_60;
    uint32_t var_61;
    uint64_t *var_62;
    uint64_t var_63;
    uint64_t *var_64;
    uint64_t var_65;
    uint64_t var_66;
    uint64_t *var_67;
    uint64_t var_68;
    uint64_t var_69;
    uint64_t var_73;
    uint64_t cc_dst_0;
    uint64_t rbx_2;
    uint64_t state_0x8558_1;
    uint64_t state_0x8560_0;
    uint64_t state_0x85d8_0;
    uint64_t state_0x8598_1;
    uint64_t local_sp_0;
    uint64_t var_75;
    unsigned char var_76;
    uint64_t var_78;
    uint64_t var_79;
    struct helper_cvtsi2sd_wrapper_126_ret_type var_80;
    uint64_t var_81;
    struct helper_divsd_wrapper_ret_type var_82;
    uint64_t var_83;
    struct helper_ucomisd_wrapper_ret_type var_84;
    uint64_t rax_3;
    unsigned char var_85;
    uint64_t var_86;
    uint64_t var_87;
    struct helper_divsd_wrapper_166_ret_type var_88;
    uint64_t var_89;
    struct helper_ucomisd_wrapper_109_ret_type var_90;
    uint64_t rdx5_2;
    unsigned char state_0x8549_1;
    uint64_t state_0x8598_2;
    struct helper_mulsd_wrapper_167_ret_type var_91;
    uint64_t var_92;
    struct helper_ucomisd_wrapper_168_ret_type var_93;
    struct helper_ucomisd_wrapper_116_ret_type var_94;
    uint64_t var_95;
    struct helper_cvtsi2sd_wrapper_ret_type var_96;
    unsigned char state_0x8549_3;
    uint64_t rax_4;
    unsigned char state_0x8549_2;
    uint64_t state_0x8598_3;
    struct helper_divsd_wrapper_169_ret_type var_97;
    uint64_t var_98;
    struct helper_ucomisd_wrapper_168_ret_type var_99;
    uint64_t var_100;
    unsigned char var_101;
    struct helper_ucomisd_wrapper_116_ret_type var_102;
    uint64_t var_103;
    uint64_t local_sp_1;
    struct helper_pxor_xmm_wrapper_110_ret_type var_33;
    uint64_t var_34;
    struct helper_cvtsi2sd_wrapper_121_ret_type var_35;
    uint64_t var_36;
    struct helper_divsd_wrapper_170_ret_type var_37;
    struct helper_ucomisd_wrapper_109_ret_type var_38;
    uint64_t var_39;
    unsigned char var_40;
    uint64_t var_41;
    struct helper_mulsd_wrapper_112_ret_type var_42;
    uint64_t var_43;
    unsigned char var_44;
    struct helper_pxor_xmm_wrapper_110_ret_type var_45;
    uint64_t var_46;
    struct helper_cvtsi2sd_wrapper_121_ret_type var_47;
    uint64_t var_48;
    struct helper_addsd_wrapper_125_ret_type var_49;
    uint64_t rbx_4;
    uint64_t var_50;
    unsigned int loop_state_var;
    var_0 = revng_init_local_sp(0UL);
    var_1 = init_state_0x8598();
    var_2 = init_state_0x85a0();
    var_3 = init_state_0x85d8();
    var_4 = init_state_0x85e0();
    var_5 = init_rbx();
    var_6 = init_rbp();
    var_7 = init_r13();
    var_8 = init_cc_src2();
    var_9 = init_r12();
    var_10 = init_r15();
    var_11 = init_r14();
    var_12 = init_state_0x8549();
    var_13 = init_state_0x854c();
    var_14 = init_state_0x8548();
    var_15 = init_state_0x854b();
    var_16 = init_state_0x8547();
    var_17 = init_state_0x854d();
    *(uint64_t *)(var_0 + (-8L)) = var_10;
    var_18 = helper_pxor_xmm_wrapper((struct type_4 *)(0UL), (struct type_6 *)(904UL), (struct type_6 *)(904UL), var_3, var_4);
    var_19 = var_18.field_0;
    var_20 = var_18.field_1;
    *(uint64_t *)(var_0 + (-16L)) = var_11;
    *(uint64_t *)(var_0 + (-24L)) = var_7;
    *(uint64_t *)(var_0 + (-32L)) = var_9;
    *(uint64_t *)(var_0 + (-40L)) = var_6;
    *(uint64_t *)(var_0 + (-48L)) = var_5;
    var_21 = (uint32_t)rdx;
    var_22 = (uint64_t)var_21;
    var_23 = var_0 + (-104L);
    var_24 = (uint64_t)((long)(rcx << 32UL) >> (long)32UL);
    var_25 = *(uint64_t *)4274344UL;
    var_26 = (uint32_t)rsi;
    var_27 = ((uint64_t)(var_26 + (-16)) != 0UL);
    state_0x8598_0 = var_1;
    rbx_1 = 0UL;
    rdx5_1 = 0UL;
    state_0x8558_0 = var_19;
    r12_0 = rdi;
    state_0x8549_0 = var_12;
    state_0x85a0_0 = var_2;
    state_0x8560_0 = 0UL;
    state_0x85d8_0 = var_19;
    local_sp_0 = var_23;
    rdx5_2 = 0UL;
    local_sp_1 = var_23;
    while (1U)
        {
            var_28 = *(unsigned char *)r12_0;
            var_29 = (uint64_t)var_28;
            var_30 = (uint32_t)var_29;
            var_31 = var_30 + (-48);
            var_32 = (uint64_t)var_31;
            state_0x8558_2 = state_0x8558_0;
            rdx5_3 = rdx5_1;
            state_0x8558_3 = state_0x8558_0;
            rbx_0 = rbx_1;
            state_0x85a0_3 = state_0x85a0_0;
            rdx5_0 = r12_0;
            rbx_5 = rbx_1;
            state_0x8549_5 = state_0x8549_0;
            state_0x8598_6 = state_0x8598_0;
            rbx_2 = rbx_1;
            state_0x8558_1 = state_0x8558_0;
            state_0x8598_1 = state_0x8558_0;
            rbx_4 = rbx_1;
            if ((uint64_t)((unsigned char)var_32 & '\xfe') > 9UL) {
                storemerge2_in = (uint64_t)var_30 + (-48L);
            } else {
                rdx5_3 = 1UL;
                if (!(var_27 || (var_32 > 54UL))) {
                    var_51 = (uint64_t)(var_31 & (-256)) | ((uint64_t)(var_28 + '\xd2') == 0UL);
                    var_52 = helper_cc_compute_c_wrapper(rdx5_1 - var_51, var_51, var_8, 14U);
                    if (var_52 == 0UL) {
                        break;
                    }
                    state_0x8598_0 = state_0x8598_6;
                    rbx_1 = rbx_5;
                    rdx5_1 = rdx5_3;
                    state_0x8558_0 = state_0x8558_3;
                    r12_0 = r12_0 + 1UL;
                    state_0x8549_0 = state_0x8549_5;
                    state_0x85a0_0 = state_0x85a0_3;
                    continue;
                }
                if (((1UL << (var_32 & 63UL)) & 35465847073801215UL) != 0UL) {
                    var_51 = (uint64_t)(var_31 & (-256)) | ((uint64_t)(var_28 + '\xd2') == 0UL);
                    var_52 = helper_cc_compute_c_wrapper(rdx5_1 - var_51, var_51, var_8, 14U);
                    if (var_52 == 0UL) {
                        break;
                    }
                    state_0x8598_0 = state_0x8598_6;
                    rbx_1 = rbx_5;
                    rdx5_1 = rdx5_3;
                    state_0x8558_0 = state_0x8558_3;
                    r12_0 = r12_0 + 1UL;
                    state_0x8549_0 = state_0x8549_5;
                    state_0x85a0_0 = state_0x85a0_3;
                    continue;
                }
                storemerge2_in = (uint64_t)(uint32_t)(((uint64_t)((var_28 + '\xbf') & '\xfe') > 25UL) ? var_29 : (var_29 + 32UL)) + (-87L);
            }
            var_33 = helper_pxor_xmm_wrapper_110((struct type_4 *)(0UL), (struct type_6 *)(840UL), (struct type_6 *)(840UL), state_0x8598_0, state_0x85a0_0);
            var_34 = var_33.field_1;
            var_35 = helper_cvtsi2sd_wrapper_121((struct type_4 *)(0UL), (struct type_6 *)(840UL), var_26);
            var_36 = var_35.field_0;
            var_37 = helper_divsd_wrapper_170((struct type_4 *)(0UL), (struct type_6 *)(968UL), (struct type_6 *)(840UL), var_36, var_25, state_0x8549_0, var_13, var_14, var_15, var_16, var_17);
            var_38 = helper_ucomisd_wrapper_109((struct type_4 *)(0UL), (struct type_6 *)(968UL), (struct type_6 *)(776UL), state_0x8558_0, var_37.field_0, var_37.field_1, var_13);
            var_39 = var_38.field_0;
            var_40 = var_38.field_1;
            var_41 = helper_cc_compute_c_wrapper(storemerge2_in, var_39, var_8, 1U);
            state_0x8549_4 = var_40;
            state_0x85a0_2 = var_34;
            state_0x8598_5 = var_36;
            if (var_41 == 0UL) {
                var_42 = helper_mulsd_wrapper_112((struct type_4 *)(0UL), (struct type_6 *)(776UL), (struct type_6 *)(840UL), var_36, state_0x8558_0, var_40, var_13, var_14, var_15, var_16, var_17);
                var_43 = var_42.field_0;
                var_44 = var_42.field_1;
                var_45 = helper_pxor_xmm_wrapper_110((struct type_4 *)(0UL), (struct type_6 *)(840UL), (struct type_6 *)(840UL), var_36, var_34);
                var_46 = var_45.field_1;
                var_47 = helper_cvtsi2sd_wrapper_121((struct type_4 *)(0UL), (struct type_6 *)(840UL), (uint32_t)storemerge2_in);
                var_48 = var_47.field_0;
                var_49 = helper_addsd_wrapper_125((struct type_4 *)(0UL), (struct type_6 *)(776UL), (struct type_6 *)(840UL), var_48, var_43, var_44, var_13, var_14, var_15, var_16, var_17);
                state_0x8558_2 = var_49.field_0;
                state_0x8549_4 = var_49.field_1;
                state_0x85a0_2 = var_46;
                state_0x8598_5 = var_48;
            } else {
                rbx_4 = rbx_1 + var_24;
            }
            var_50 = rbx_4 - (((uint64_t)(unsigned char)rdx5_1 == 0UL) ? 0UL : var_24);
            state_0x8558_3 = state_0x8558_2;
            state_0x85a0_3 = state_0x85a0_2;
            rbx_5 = var_50;
            state_0x8549_5 = state_0x8549_4;
            state_0x8598_6 = state_0x8598_5;
            state_0x8598_0 = state_0x8598_6;
            rbx_1 = rbx_5;
            rdx5_1 = rdx5_3;
            state_0x8558_0 = state_0x8558_3;
            r12_0 = r12_0 + 1UL;
            state_0x8549_0 = state_0x8549_5;
            state_0x85a0_0 = state_0x85a0_3;
            continue;
        }
    var_53 = (uint64_t)(var_30 + (-65));
    rax_1 = var_53;
    if ((uint64_t)((uint32_t)((int)((uint32_t)r8 << 24U) >> (int)24U) - (uint32_t)(((uint64_t)((unsigned char)var_53 & '\xfe') > 25UL) ? var_29 : (var_29 + 32UL))) == 0UL) {
        var_54 = r12_0 + 1UL;
        var_55 = *(unsigned char *)var_54;
        var_56 = (uint64_t)((uint32_t)(uint64_t)var_55 + (-9));
        rax_0 = var_56;
        rax_1 = var_56;
        if (var_56 <= 4UL) {
            var_73 = var_22 + (-2L);
            *(uint64_t *)r9 = r12_0;
            rax_2 = rax_1;
            cc_dst_0 = var_73;
            state_0x8560_0 = var_20;
            rax_7 = rax_1;
            if ((uint64_t)(uint32_t)var_73 == 0UL) {
                *(uint64_t *)(local_sp_1 + (-8L)) = 4227907UL;
                indirect_placeholder_1();
                return rax_7;
            }
        }
        if ((uint64_t)(var_55 + '\xe0') == 0UL) {
            var_73 = var_22 + (-2L);
            *(uint64_t *)r9 = r12_0;
            rax_2 = rax_1;
            cc_dst_0 = var_73;
            state_0x8560_0 = var_20;
            rax_7 = rax_1;
            if ((uint64_t)(uint32_t)var_73 == 0UL) {
                *(uint64_t *)(local_sp_1 + (-8L)) = 4227907UL;
                indirect_placeholder_1();
                return rax_7;
            }
        }
        *(uint64_t *)(var_0 + (-80L)) = var_19;
        var_57 = (uint64_t *)(var_0 + (-88L));
        *var_57 = r9;
        *(uint64_t *)(var_0 + (-96L)) = state_0x8558_0;
        var_58 = (uint64_t *)var_23;
        *var_58 = state_0x8558_0;
        var_59 = var_0 + (-112L);
        *(uint64_t *)var_59 = 4227741UL;
        indirect_placeholder_1();
        var_60 = (uint32_t *)var_56;
        var_61 = *var_60;
        var_62 = (uint64_t *)(var_0 + (-120L));
        *var_62 = 4227762UL;
        indirect_placeholder_1();
        var_63 = var_0 + (-128L);
        var_64 = (uint64_t *)var_63;
        *var_64 = 4227770UL;
        indirect_placeholder_1();
        var_65 = *var_57;
        *var_60 = var_61;
        var_66 = *var_64;
        var_67 = *(uint64_t **)var_59;
        var_68 = *var_62;
        var_69 = *var_58;
        state_0x8558_1 = var_66;
        state_0x85d8_0 = var_69;
        state_0x8598_1 = var_68;
        local_sp_0 = var_63;
        local_sp_1 = var_63;
        if (var_54 != var_65) {
            var_70 = ((long)rbx_1 < (long)0UL);
            var_71 = rbx_1 + var_56;
            rax_0 = var_71;
            rdx5_0 = var_65;
            if (var_70) {
                rbx_0 = ((long)var_56 < (long)(9223372036854775808UL - (-9223372036854775808L))) ? 9223372036854775808UL : var_71;
            } else {
                rbx_0 = ((long)var_56 > (long)(9223372036854775807UL - rbx_1)) ? 9223372036854775807UL : var_71;
            }
        }
        var_72 = var_22 + (-2L);
        *var_67 = rdx5_0;
        rax_2 = rax_0;
        cc_dst_0 = var_72;
        rbx_2 = rbx_0;
        rax_7 = rax_0;
        if ((uint64_t)(uint32_t)var_72 == 0UL) {
            *(uint64_t *)(local_sp_1 + (-8L)) = 4227907UL;
            indirect_placeholder_1();
            return rax_7;
        }
    }
    var_73 = var_22 + (-2L);
    *(uint64_t *)r9 = r12_0;
    rax_2 = rax_1;
    cc_dst_0 = var_73;
    state_0x8560_0 = var_20;
    rax_7 = rax_1;
    if ((uint64_t)(uint32_t)var_73 == 0UL) {
        *(uint64_t *)(local_sp_1 + (-8L)) = 4227907UL;
        indirect_placeholder_1();
        return rax_7;
    }
    var_74 = helper_ucomisd_wrapper((struct type_4 *)(0UL), (struct type_6 *)(776UL), (struct type_6 *)(904UL), state_0x85d8_0, state_0x8558_1, state_0x8549_0, var_13);
    var_75 = var_74.field_0;
    var_76 = var_74.field_1;
    rax_6 = rax_2;
    cc_src_0 = var_75;
    rax_3 = rax_2;
    state_0x8598_2 = state_0x8598_1;
    state_0x8549_2 = var_76;
    state_0x8598_3 = state_0x8598_1;
    if ((var_75 & 4UL) != 0UL) {
        var_77 = helper_cc_compute_all_wrapper(cc_dst_0, var_75, var_8, 1U);
        cc_src_0 = var_77;
        if ((var_77 & 64UL) == 0UL) {
            return rax_6;
        }
    }
    if ((long)rbx_2 < (long)0UL) {
        helper_pxor_xmm_wrapper_118((struct type_4 *)(0UL), (struct type_6 *)(776UL), (struct type_6 *)(776UL), state_0x8558_1, state_0x8560_0);
        var_95 = rbx_2 + 1UL;
        var_96 = helper_cvtsi2sd_wrapper((struct type_4 *)(0UL), (struct type_6 *)(776UL), var_21);
        rax_4 = var_95;
        while (1U)
            {
                var_97 = helper_divsd_wrapper_169((struct type_4 *)(0UL), (struct type_6 *)(840UL), (struct type_6 *)(776UL), state_0x8598_3, var_96.field_0, state_0x8549_2, var_13, var_14, var_15, var_16, var_17);
                var_98 = var_97.field_0;
                var_99 = helper_ucomisd_wrapper_168((struct type_4 *)(0UL), (struct type_6 *)(840UL), (struct type_6 *)(904UL), var_98, state_0x85d8_0, var_97.field_1, var_13);
                var_100 = var_99.field_0;
                var_101 = var_99.field_1;
                state_0x8598_3 = var_98;
                state_0x8549_3 = var_101;
                rax_6 = rax_4;
                if ((var_100 & 4UL) == 0UL) {
                    var_103 = rax_4 + 1UL;
                    rax_4 = var_103;
                    state_0x8549_2 = state_0x8549_3;
                    rax_6 = var_103;
                    if (rax_4 != 0UL) {
                        continue;
                    }
                    break;
                }
                var_102 = helper_ucomisd_wrapper_116((struct type_4 *)(0UL), (struct type_6 *)(840UL), (struct type_6 *)(968UL), var_98, state_0x85d8_0, var_101, var_13);
                state_0x8549_3 = var_102.field_1;
                if ((var_102.field_0 & 64UL) != 0UL) {
                    *(uint64_t *)local_sp_0 = var_98;
                    *(uint64_t *)(local_sp_0 + (-8L)) = 4228074UL;
                    indirect_placeholder_1();
                    *(uint32_t *)rax_4 = 34U;
                    break;
                }
            }
    }
    var_78 = helper_cc_compute_all_wrapper(rbx_2, cc_src_0, var_8, 25U);
    if ((var_78 & 64UL) == 0UL) {
        return;
    }
    helper_pxor_xmm_wrapper_111((struct type_4 *)(0UL), (struct type_6 *)(1032UL), (struct type_6 *)(1032UL), var_25, 0UL);
    var_79 = *(uint64_t *)4274352UL;
    var_80 = helper_cvtsi2sd_wrapper_126((struct type_4 *)(0UL), (struct type_6 *)(1032UL), var_21);
    var_81 = var_80.field_0;
    var_82 = helper_divsd_wrapper((struct type_4 *)(0UL), (struct type_6 *)(904UL), (struct type_6 *)(1032UL), var_79, var_81, var_76, var_13, var_14, var_15, var_16, var_17);
    var_83 = var_82.field_0;
    var_84 = helper_ucomisd_wrapper((struct type_4 *)(0UL), (struct type_6 *)(904UL), (struct type_6 *)(776UL), var_83, state_0x8558_1, var_82.field_1, var_13);
    if ((var_84.field_0 & 65UL) != 0UL) {
        *(uint64_t *)(local_sp_0 + (-8L)) = 4228005UL;
        indirect_placeholder_1();
        *(uint32_t *)rax_3 = 34U;
        rax_6 = rax_3;
        return rax_6;
    }
    var_85 = var_84.field_1;
    var_86 = *(uint64_t *)4274344UL;
    var_87 = rbx_2 + (-1L);
    var_88 = helper_divsd_wrapper_166((struct type_4 *)(0UL), (struct type_6 *)(968UL), (struct type_6 *)(1032UL), var_81, var_86, var_85, var_13, var_14, var_15, var_16, var_17);
    var_89 = var_88.field_0;
    var_90 = helper_ucomisd_wrapper_109((struct type_4 *)(0UL), (struct type_6 *)(776UL), (struct type_6 *)(968UL), state_0x8558_1, var_89, var_88.field_1, var_13);
    rax_3 = var_87;
    rax_6 = var_87;
    if ((var_90.field_0 & 65UL) != 0UL) {
        *(uint64_t *)(local_sp_0 + (-8L)) = 4228037UL;
        indirect_placeholder_1();
        *(uint32_t *)var_87 = 34U;
        return rax_6;
    }
    state_0x8549_1 = var_90.field_1;
    while (1U)
        {
            var_91 = helper_mulsd_wrapper_167((struct type_4 *)(0UL), (struct type_6 *)(840UL), (struct type_6 *)(1032UL), state_0x8598_2, var_81, state_0x8549_1, var_13, var_14, var_15, var_16, var_17);
            var_92 = var_91.field_0;
            state_0x8598_2 = var_92;
            if (rdx5_2 != var_87) {
                loop_state_var = 2U;
                break;
            }
            var_93 = helper_ucomisd_wrapper_168((struct type_4 *)(0UL), (struct type_6 *)(904UL), (struct type_6 *)(840UL), var_92, var_83, var_91.field_1, var_13);
            if ((var_93.field_0 & 65UL) != 0UL) {
                loop_state_var = 0U;
                break;
            }
            var_94 = helper_ucomisd_wrapper_116((struct type_4 *)(0UL), (struct type_6 *)(840UL), (struct type_6 *)(968UL), var_92, var_89, var_93.field_1, var_13);
            if ((var_94.field_0 & 65UL) != 0UL) {
                loop_state_var = 1U;
                break;
            }
            rdx5_2 = rdx5_2 + 1UL;
            state_0x8549_1 = var_94.field_1;
            continue;
        }
    switch (loop_state_var) {
      case 0U:
        {
            *(uint64_t *)(local_sp_0 + (-8L)) = 4228005UL;
            indirect_placeholder_1();
            *(uint32_t *)rax_3 = 34U;
            rax_6 = rax_3;
        }
        break;
      case 1U:
        {
            *(uint64_t *)(local_sp_0 + (-8L)) = 4228037UL;
            indirect_placeholder_1();
            *(uint32_t *)var_87 = 34U;
        }
        break;
      case 2U:
        {
            break;
        }
        break;
    }
}

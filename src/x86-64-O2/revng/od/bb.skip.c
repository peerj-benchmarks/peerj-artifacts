typedef unsigned char __u_char;
typedef unsigned short __u_short;
typedef unsigned int __u_int;
typedef unsigned long __u_long;
typedef signed char __int8_t;
typedef unsigned char __uint8_t;
typedef short __int16_t;
typedef unsigned short __uint16_t;
typedef int __int32_t;
typedef unsigned int __uint32_t;
typedef long __int64_t;
typedef unsigned long __uint64_t;
typedef __int8_t __int_least8_t;
typedef __uint8_t __uint_least8_t;
typedef __int16_t __int_least16_t;
typedef __uint16_t __uint_least16_t;
typedef __int32_t __int_least32_t;
typedef __uint32_t __uint_least32_t;
typedef __int64_t __int_least64_t;
typedef __uint64_t __uint_least64_t;
typedef long __quad_t;
typedef unsigned long __u_quad_t;
typedef long __intmax_t;
typedef unsigned long __uintmax_t;
typedef unsigned long __dev_t;
typedef unsigned int __uid_t;
typedef unsigned int __gid_t;
typedef unsigned long __ino_t;
typedef unsigned long __ino64_t;
typedef unsigned int __mode_t;
typedef unsigned long __nlink_t;
typedef long __off_t;
typedef long __off64_t;
typedef int __pid_t;
typedef struct {
    int __val[2];
} __fsid_t;
typedef long __clock_t;
typedef unsigned long __rlim_t;
typedef unsigned long __rlim64_t;
typedef unsigned int __id_t;
typedef long __time_t;
typedef unsigned int __useconds_t;
typedef long __suseconds_t;
typedef int __daddr_t;
typedef int __key_t;
typedef int __clockid_t;
typedef void *__timer_t;
typedef long __blksize_t;
typedef long __blkcnt_t;
typedef long __blkcnt64_t;
typedef unsigned long __fsblkcnt_t;
typedef unsigned long __fsblkcnt64_t;
typedef unsigned long __fsfilcnt_t;
typedef unsigned long __fsfilcnt64_t;
typedef long __fsword_t;
typedef long __ssize_t;
typedef long __syscall_slong_t;
typedef unsigned long __syscall_ulong_t;
typedef __off64_t __loff_t;
typedef char *__caddr_t;
typedef long __intptr_t;
typedef unsigned int __socklen_t;
typedef int __sig_atomic_t;
typedef __int8_t int8_t;
typedef __int16_t int16_t;
typedef __int32_t int32_t;
typedef __int64_t int64_t;
typedef __uint8_t uint8_t;
typedef __uint16_t uint16_t;
typedef __uint32_t uint32_t;
typedef __uint64_t uint64_t;
typedef __int_least8_t int_least8_t;
typedef __int_least16_t int_least16_t;
typedef __int_least32_t int_least32_t;
typedef __int_least64_t int_least64_t;
typedef __uint_least8_t uint_least8_t;
typedef __uint_least16_t uint_least16_t;
typedef __uint_least32_t uint_least32_t;
typedef __uint_least64_t uint_least64_t;
typedef signed char int_fast8_t;
typedef long int_fast16_t;
typedef long int_fast32_t;
typedef long int_fast64_t;
typedef unsigned char uint_fast8_t;
typedef unsigned long uint_fast16_t;
typedef unsigned long uint_fast32_t;
typedef unsigned long uint_fast64_t;
typedef long intptr_t;
typedef unsigned long uintptr_t;
typedef __intmax_t intmax_t;
typedef __uintmax_t uintmax_t;
typedef _Bool bool;
struct bb_skip_ret_type;
struct indirect_placeholder_23_ret_type;
struct indirect_placeholder_22_ret_type;
struct indirect_placeholder_25_ret_type;
struct indirect_placeholder_24_ret_type;
struct indirect_placeholder_26_ret_type;
struct bb_skip_ret_type {
    uint64_t field_0;
    uint64_t field_1;
    uint64_t field_2;
    uint64_t field_3;
    uint64_t field_4;
    uint64_t field_5;
};
struct indirect_placeholder_23_ret_type {
    uint64_t field_0;
    uint64_t field_1;
    uint64_t field_2;
};
struct indirect_placeholder_22_ret_type {
    uint64_t field_0;
    uint64_t field_1;
    uint64_t field_2;
    uint64_t field_3;
    uint64_t field_4;
    uint64_t field_5;
    uint64_t field_6;
};
struct indirect_placeholder_25_ret_type {
    uint64_t field_0;
    uint64_t field_1;
    uint64_t field_2;
    uint64_t field_3;
};
struct indirect_placeholder_24_ret_type {
    uint64_t field_0;
    uint64_t field_1;
    uint64_t field_2;
    uint64_t field_3;
    uint64_t field_4;
    uint64_t field_5;
};
struct indirect_placeholder_26_ret_type {
    uint64_t field_0;
    uint64_t field_1;
    uint64_t field_2;
    uint64_t field_3;
    uint64_t field_4;
    uint64_t field_5;
    uint64_t field_6;
};
extern void abort(void);
extern uint64_t revng_init_local_sp(uint64_t param_0);
extern uint64_t init_rbx(void);
extern uint64_t init_rbp(void);
extern void indirect_placeholder_1(void);
extern uint64_t init_cc_src2(void);
extern uint64_t helper_cc_compute_c_wrapper(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint32_t param_3);
extern uint64_t init_r13(void);
extern uint64_t init_r12(void);
extern uint64_t init_r14(void);
extern uint64_t init_rax(void);
extern uint64_t init_r10(void);
extern uint64_t init_rsi(void);
extern struct indirect_placeholder_23_ret_type indirect_placeholder_23(uint64_t param_0, uint64_t param_1, uint64_t param_2);
extern struct indirect_placeholder_22_ret_type indirect_placeholder_22(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint64_t param_3, uint64_t param_4, uint64_t param_5, uint64_t param_6);
extern struct indirect_placeholder_25_ret_type indirect_placeholder_25(uint64_t param_0);
extern struct indirect_placeholder_24_ret_type indirect_placeholder_24(void);
extern struct indirect_placeholder_26_ret_type indirect_placeholder_26(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint64_t param_3, uint64_t param_4, uint64_t param_5, uint64_t param_6);
struct bb_skip_ret_type bb_skip(uint64_t rcx, uint64_t r9, uint64_t rdi, uint64_t r8) {
    uint64_t var_0;
    uint64_t var_1;
    uint64_t var_2;
    uint64_t var_3;
    uint64_t var_4;
    uint64_t var_5;
    uint64_t var_6;
    uint64_t var_7;
    uint64_t var_8;
    uint64_t var_9;
    uint64_t var_10;
    uint64_t rcx6_5;
    uint64_t var_34;
    uint64_t var_35;
    uint64_t r10_0;
    uint64_t rcx6_0;
    uint64_t var_14;
    uint64_t rdi8_0;
    uint64_t local_sp_0;
    uint64_t r97_0;
    uint64_t rsi_0;
    uint64_t rcx6_2;
    uint64_t var_32;
    uint64_t var_33;
    uint64_t rbx_2;
    uint64_t rdi8_1;
    uint64_t local_sp_1;
    uint64_t r12_2;
    uint32_t var_21;
    uint64_t rax_0;
    uint64_t var_22;
    uint64_t var_23;
    uint64_t var_24;
    uint64_t var_25;
    uint64_t var_11;
    struct indirect_placeholder_23_ret_type var_15;
    uint64_t var_16;
    uint64_t var_17;
    uint64_t var_18;
    uint64_t var_19;
    uint64_t var_20;
    uint64_t rbx_1;
    uint64_t rax_1;
    uint64_t var_36;
    uint64_t var_37;
    uint64_t local_sp_3;
    uint64_t r12_0;
    struct indirect_placeholder_25_ret_type var_38;
    uint64_t var_39;
    struct indirect_placeholder_24_ret_type var_40;
    uint64_t var_41;
    uint64_t rdi8_3;
    uint64_t rsi_2;
    uint64_t r12_1;
    uint64_t var_45;
    uint64_t var_46;
    bool var_26;
    uint64_t rbp_0;
    uint64_t local_sp_4;
    uint64_t var_27;
    uint64_t var_28;
    uint64_t var_29;
    uint64_t var_30;
    uint64_t var_31;
    uint64_t rcx6_4;
    uint64_t rcx6_3;
    uint64_t rbx_3;
    uint64_t local_sp_5;
    uint64_t var_12;
    uint64_t var_13;
    uint64_t var_42;
    uint64_t var_43;
    uint64_t var_44;
    uint64_t r97_1;
    uint64_t r89_0;
    uint64_t local_sp_6;
    uint64_t r10_1;
    uint64_t r97_2;
    uint64_t rdi8_4;
    uint64_t rsi_3;
    uint64_t storemerge;
    struct bb_skip_ret_type mrv;
    struct bb_skip_ret_type mrv1;
    struct bb_skip_ret_type mrv2;
    struct bb_skip_ret_type mrv3;
    struct bb_skip_ret_type mrv4;
    struct bb_skip_ret_type mrv5;
    unsigned int loop_state_var;
    var_0 = revng_init_local_sp(0UL);
    var_1 = init_rax();
    var_2 = init_r10();
    var_3 = init_rbx();
    var_4 = init_rbp();
    var_5 = init_r13();
    var_6 = init_cc_src2();
    var_7 = init_rsi();
    var_8 = init_r12();
    var_9 = init_r14();
    *(uint64_t *)(var_0 + (-8L)) = var_9;
    *(uint64_t *)(var_0 + (-16L)) = var_5;
    *(uint64_t *)(var_0 + (-24L)) = var_8;
    *(uint64_t *)(var_0 + (-32L)) = var_4;
    *(uint64_t *)(var_0 + (-40L)) = var_3;
    var_10 = var_0 + (-1208L);
    rcx6_5 = rcx;
    r10_0 = var_2;
    r97_0 = r9;
    r12_2 = 1UL;
    rax_1 = var_1;
    r12_0 = 0UL;
    rdi8_3 = 0UL;
    rsi_2 = 1UL;
    rbp_0 = 1024UL;
    rcx6_4 = rcx;
    rcx6_3 = rcx;
    rbx_3 = rdi;
    local_sp_5 = var_10;
    r97_1 = r9;
    r89_0 = r8;
    local_sp_6 = var_10;
    r10_1 = var_2;
    r97_2 = r9;
    rdi8_4 = 0UL;
    rsi_3 = var_7;
    storemerge = 1UL;
    if (rdi == 0UL) {
        mrv.field_0 = storemerge;
        mrv1 = mrv;
        mrv1.field_1 = rcx6_5;
        mrv2 = mrv1;
        mrv2.field_2 = r10_1;
        mrv3 = mrv2;
        mrv3.field_3 = r97_2;
        mrv4 = mrv3;
        mrv4.field_4 = rdi8_4;
        mrv5 = mrv4;
        mrv5.field_5 = rsi_3;
        return mrv5;
    }
    if (*(uint64_t *)6383176UL == 0UL) {
        *(uint64_t *)(local_sp_6 + (-8L)) = 4212053UL;
        indirect_placeholder_26(0UL, rcx6_4, r97_1, 1UL, r89_0, 4262912UL, 0UL);
        abort();
    }
    while (1U)
        {
            var_11 = local_sp_5 + (-8L);
            *(uint64_t *)var_11 = 4211790UL;
            indirect_placeholder_1();
            var_12 = (uint64_t)(uint32_t)rax_1;
            var_13 = local_sp_5 + (-16L);
            *(uint64_t *)var_13 = 4211800UL;
            indirect_placeholder_1();
            rcx6_0 = rcx6_3;
            rsi_0 = rbx_3;
            rcx6_2 = rcx6_3;
            rbx_2 = rbx_3;
            rbx_1 = rbx_3;
            local_sp_3 = var_13;
            r12_1 = r12_2;
            local_sp_4 = var_13;
            r10_1 = r10_0;
            r97_2 = r97_0;
            if (var_12 == 0UL) {
                var_21 = (uint32_t)((uint16_t)*(uint32_t *)(local_sp_5 + 8UL) & (unsigned short)53248U);
                r12_0 = r12_2;
                rax_0 = (uint64_t)var_21;
                if ((uint64_t)((var_21 + (-32768)) & (-12288)) != 0UL) {
                    loop_state_var = 0U;
                    break;
                }
                var_22 = *(uint64_t *)(local_sp_5 + 40UL);
                var_23 = var_22 + (-1L);
                var_24 = *(uint64_t *)(local_sp_5 + 32UL);
                var_25 = (var_23 > 2305843009213693951UL) ? 512UL : var_22;
                rsi_2 = rbx_3;
                rax_0 = var_25;
                if ((long)var_25 >= (long)var_24) {
                    loop_state_var = 0U;
                    break;
                }
                var_36 = helper_cc_compute_c_wrapper(var_24 - rbx_3, rbx_3, var_6, 17U);
                if (var_36 == 0UL) {
                    var_37 = rbx_3 - var_24;
                    rbx_1 = var_37;
                    rsi_2 = var_11;
                    if (var_37 != 0UL) {
                        loop_state_var = 3U;
                        break;
                    }
                }
                var_45 = *(uint64_t *)6383176UL;
                var_46 = local_sp_5 + (-24L);
                *(uint64_t *)var_46 = 4211994UL;
                indirect_placeholder_1();
                rdi8_0 = var_45;
                local_sp_0 = var_46;
                rdi8_3 = var_45;
                if ((uint64_t)(uint32_t)var_25 != 0UL) {
                    loop_state_var = 3U;
                    break;
                }
                loop_state_var = 2U;
                break;
            }
            var_14 = *(uint64_t *)6383192UL;
            *(uint64_t *)(local_sp_5 + (-24L)) = 4211942UL;
            var_15 = indirect_placeholder_23(0UL, var_14, 3UL);
            var_16 = var_15.field_0;
            var_17 = var_15.field_1;
            var_18 = var_15.field_2;
            *(uint64_t *)(local_sp_5 + (-32L)) = 4211950UL;
            indirect_placeholder_1();
            var_19 = (uint64_t)*(uint32_t *)var_16;
            var_20 = local_sp_5 + (-40L);
            *(uint64_t *)var_20 = 4211969UL;
            indirect_placeholder_22(0UL, var_16, var_17, 0UL, var_18, 4270816UL, var_19);
            local_sp_3 = var_20;
            *(uint64_t *)(local_sp_3 + (-8L)) = 4211758UL;
            var_38 = indirect_placeholder_25(0UL);
            var_39 = local_sp_3 + (-16L);
            *(uint64_t *)var_39 = 4211766UL;
            var_40 = indirect_placeholder_24();
            var_41 = var_40.field_0;
            rax_1 = var_41;
            rbx_3 = rbx_1;
            local_sp_5 = var_39;
            local_sp_6 = var_39;
            if (*(uint64_t *)6383176UL != 0UL) {
                var_42 = var_40.field_1;
                var_43 = var_40.field_3;
                var_44 = var_40.field_4;
                rcx6_4 = var_42;
                r97_1 = var_43;
                r89_0 = var_44;
                loop_state_var = 1U;
                break;
            }
            rcx6_3 = var_40.field_1;
            r10_0 = var_40.field_2;
            r97_0 = var_40.field_3;
            r12_2 = (uint64_t)((uint32_t)r12_0 & (uint32_t)var_38.field_0) & var_41;
            continue;
        }
    switch (loop_state_var) {
      case 1U:
        {
            *(uint64_t *)(local_sp_6 + (-8L)) = 4212053UL;
            indirect_placeholder_26(0UL, rcx6_4, r97_1, 1UL, r89_0, 4262912UL, 0UL);
            abort();
        }
        break;
      case 3U:
      case 2U:
      case 0U:
        {
            switch (loop_state_var) {
              case 2U:
                {
                    *(uint64_t *)(local_sp_0 + (-8L)) = 4212008UL;
                    indirect_placeholder_1();
                    rcx6_2 = rcx6_0;
                    rdi8_3 = rdi8_0;
                    rsi_2 = rsi_0;
                    r12_1 = 0UL;
                }
                break;
              case 3U:
                {
                    rcx6_5 = rcx6_2;
                    rdi8_4 = rdi8_3;
                    rsi_3 = rsi_2;
                    storemerge = (uint64_t)(uint32_t)r12_1;
                }
                break;
              case 0U:
                {
                    var_26 = ((uint64_t)(uint32_t)rax_0 == 0UL);
                    rsi_0 = 1UL;
                    while (1U)
                        {
                            var_27 = *(uint64_t *)6383176UL;
                            var_28 = local_sp_4 + 144UL;
                            var_29 = (rbp_0 > rbx_2) ? rbx_2 : rbp_0;
                            var_30 = local_sp_4 + (-8L);
                            *(uint64_t *)var_30 = 4211867UL;
                            indirect_placeholder_1();
                            var_31 = rbx_2 - rax_0;
                            rcx6_0 = var_27;
                            rcx6_2 = var_27;
                            rbx_2 = var_31;
                            rdi8_1 = var_28;
                            local_sp_1 = var_30;
                            rbp_0 = var_29;
                            if (rax_0 != var_29) {
                                var_32 = *(uint64_t *)6383176UL;
                                var_33 = local_sp_4 + (-16L);
                                *(uint64_t *)var_33 = 4211887UL;
                                indirect_placeholder_1();
                                rdi8_0 = var_32;
                                local_sp_0 = var_33;
                                if (!var_26) {
                                    loop_state_var = 0U;
                                    break;
                                }
                                var_34 = *(uint64_t *)6383176UL;
                                var_35 = local_sp_4 + (-24L);
                                *(uint64_t *)var_35 = 4211903UL;
                                indirect_placeholder_1();
                                rdi8_1 = var_34;
                                local_sp_1 = var_35;
                            }
                            rdi8_3 = rdi8_1;
                            local_sp_4 = local_sp_1;
                            if (var_31 == 0UL) {
                                continue;
                            }
                            loop_state_var = 1U;
                            break;
                        }
                    *(uint64_t *)(local_sp_0 + (-8L)) = 4212008UL;
                    indirect_placeholder_1();
                    rcx6_2 = rcx6_0;
                    rdi8_3 = rdi8_0;
                    rsi_2 = rsi_0;
                    r12_1 = 0UL;
                }
                break;
            }
        }
        break;
    }
}

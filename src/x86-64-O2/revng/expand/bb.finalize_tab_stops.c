typedef unsigned char __u_char;
typedef unsigned short __u_short;
typedef unsigned int __u_int;
typedef unsigned long __u_long;
typedef signed char __int8_t;
typedef unsigned char __uint8_t;
typedef short __int16_t;
typedef unsigned short __uint16_t;
typedef int __int32_t;
typedef unsigned int __uint32_t;
typedef long __int64_t;
typedef unsigned long __uint64_t;
typedef __int8_t __int_least8_t;
typedef __uint8_t __uint_least8_t;
typedef __int16_t __int_least16_t;
typedef __uint16_t __uint_least16_t;
typedef __int32_t __int_least32_t;
typedef __uint32_t __uint_least32_t;
typedef __int64_t __int_least64_t;
typedef __uint64_t __uint_least64_t;
typedef long __quad_t;
typedef unsigned long __u_quad_t;
typedef long __intmax_t;
typedef unsigned long __uintmax_t;
typedef unsigned long __dev_t;
typedef unsigned int __uid_t;
typedef unsigned int __gid_t;
typedef unsigned long __ino_t;
typedef unsigned long __ino64_t;
typedef unsigned int __mode_t;
typedef unsigned long __nlink_t;
typedef long __off_t;
typedef long __off64_t;
typedef int __pid_t;
typedef struct {
    int __val[2];
} __fsid_t;
typedef long __clock_t;
typedef unsigned long __rlim_t;
typedef unsigned long __rlim64_t;
typedef unsigned int __id_t;
typedef long __time_t;
typedef unsigned int __useconds_t;
typedef long __suseconds_t;
typedef int __daddr_t;
typedef int __key_t;
typedef int __clockid_t;
typedef void *__timer_t;
typedef long __blksize_t;
typedef long __blkcnt_t;
typedef long __blkcnt64_t;
typedef unsigned long __fsblkcnt_t;
typedef unsigned long __fsblkcnt64_t;
typedef unsigned long __fsfilcnt_t;
typedef unsigned long __fsfilcnt64_t;
typedef long __fsword_t;
typedef long __ssize_t;
typedef long __syscall_slong_t;
typedef unsigned long __syscall_ulong_t;
typedef __off64_t __loff_t;
typedef char *__caddr_t;
typedef long __intptr_t;
typedef unsigned int __socklen_t;
typedef int __sig_atomic_t;
typedef __int8_t int8_t;
typedef __int16_t int16_t;
typedef __int32_t int32_t;
typedef __int64_t int64_t;
typedef __uint8_t uint8_t;
typedef __uint16_t uint16_t;
typedef __uint32_t uint32_t;
typedef __uint64_t uint64_t;
typedef __int_least8_t int_least8_t;
typedef __int_least16_t int_least16_t;
typedef __int_least32_t int_least32_t;
typedef __int_least64_t int_least64_t;
typedef __uint_least8_t uint_least8_t;
typedef __uint_least16_t uint_least16_t;
typedef __uint_least32_t uint_least32_t;
typedef __uint_least64_t uint_least64_t;
typedef signed char int_fast8_t;
typedef long int_fast16_t;
typedef long int_fast32_t;
typedef long int_fast64_t;
typedef unsigned char uint_fast8_t;
typedef unsigned long uint_fast16_t;
typedef unsigned long uint_fast32_t;
typedef unsigned long uint_fast64_t;
typedef long intptr_t;
typedef unsigned long uintptr_t;
typedef __intmax_t intmax_t;
typedef __uintmax_t uintmax_t;
typedef _Bool bool;
struct indirect_placeholder_28_ret_type;
struct indirect_placeholder_29_ret_type;
struct indirect_placeholder_30_ret_type;
struct indirect_placeholder_28_ret_type {
    uint64_t field_0;
    uint64_t field_1;
    uint64_t field_2;
};
struct indirect_placeholder_29_ret_type {
    uint64_t field_0;
    uint64_t field_1;
    uint64_t field_2;
};
struct indirect_placeholder_30_ret_type {
    uint64_t field_0;
    uint64_t field_1;
    uint64_t field_2;
};
extern void abort(void);
extern uint64_t revng_init_local_sp(uint64_t param_0);
extern struct indirect_placeholder_28_ret_type indirect_placeholder_28(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint64_t param_3, uint64_t param_4, uint64_t param_5, uint64_t param_6);
extern struct indirect_placeholder_29_ret_type indirect_placeholder_29(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint64_t param_3, uint64_t param_4, uint64_t param_5, uint64_t param_6);
extern struct indirect_placeholder_30_ret_type indirect_placeholder_30(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint64_t param_3, uint64_t param_4, uint64_t param_5, uint64_t param_6);
void bb_finalize_tab_stops(uint64_t rcx, uint64_t r9, uint64_t r8) {
    uint64_t local_sp_0;
    uint64_t var_0;
    uint64_t var_1;
    uint64_t var_2;
    uint64_t var_3;
    uint64_t var_4;
    uint64_t rcx1_2;
    uint64_t rax_0;
    uint64_t rcx1_0;
    uint64_t var_5;
    uint64_t var_10;
    uint64_t var_7;
    uint64_t local_sp_1;
    uint64_t storemerge;
    uint64_t var_6;
    uint64_t rcx1_1;
    uint64_t r92_0;
    uint64_t r83_0;
    struct indirect_placeholder_28_ret_type var_11;
    uint64_t var_12;
    uint64_t var_13;
    uint64_t var_14;
    uint64_t var_8;
    struct indirect_placeholder_29_ret_type var_9;
    uint64_t var_15;
    bool var_16;
    uint64_t var_17;
    bool var_18;
    uint64_t rax_1;
    uint64_t rcx1_3;
    uint64_t r92_1;
    uint64_t r83_1;
    uint64_t var_19;
    unsigned int loop_state_var;
    var_0 = revng_init_local_sp(0UL);
    var_1 = var_0 + (-8L);
    var_2 = *(uint64_t *)6358320UL;
    var_3 = *(uint64_t *)6358336UL;
    local_sp_0 = var_1;
    rcx1_2 = 0UL;
    rax_0 = 0UL;
    local_sp_1 = var_1;
    storemerge = 0UL;
    r92_0 = r9;
    rcx1_3 = rcx;
    r92_1 = r9;
    r83_1 = r8;
    if (var_2 == 0UL) {
        var_15 = *(uint64_t *)6358344UL;
        var_16 = (var_15 == 0UL);
        var_17 = *(uint64_t *)6358352UL;
        var_18 = (var_17 == 0UL);
        rax_1 = var_15;
        if (var_16) {
            if (!var_18) {
                *(uint64_t *)6358784UL = rax_1;
                *(uint64_t *)6358360UL = rax_1;
                return;
            }
        }
        var_19 = var_18 ? 8UL : var_17;
        rax_1 = var_19;
        *(uint64_t *)6358784UL = rax_1;
        *(uint64_t *)6358360UL = rax_1;
        return;
    }
    var_4 = *(uint64_t *)var_3;
    rcx1_0 = var_4;
    r83_0 = var_4;
    r83_1 = var_4;
    if (var_4 != 0UL) {
        var_8 = var_0 + (-16L);
        *(uint64_t *)var_8 = 4204203UL;
        var_9 = indirect_placeholder_29(0UL, rcx1_2, r9, 1UL, var_4, 0UL, 4250041UL);
        local_sp_0 = var_8;
        rcx1_1 = var_9.field_0;
        r92_0 = var_9.field_1;
        r83_0 = var_9.field_2;
        var_10 = local_sp_0 + (-8L);
        *(uint64_t *)var_10 = 4204222UL;
        var_11 = indirect_placeholder_28(0UL, rcx1_1, r92_0, 1UL, r83_0, 0UL, 4250062UL);
        var_12 = var_11.field_0;
        var_13 = var_11.field_1;
        var_14 = var_11.field_2;
        local_sp_1 = var_10;
        rcx1_3 = var_12;
        r92_1 = var_13;
        r83_1 = var_14;
        *(uint64_t *)(local_sp_1 + (-8L)) = 4204241UL;
        indirect_placeholder_30(0UL, rcx1_3, r92_1, 1UL, r83_1, 0UL, 4249528UL);
        abort();
    }
    while (1U)
        {
            var_5 = rax_0 + 1UL;
            rax_0 = var_5;
            rcx1_1 = rcx1_0;
            rcx1_2 = rcx1_0;
            rcx1_3 = rcx1_0;
            if (var_5 == var_2) {
                var_6 = *(uint64_t *)((var_5 << 3UL) + var_3);
                rcx1_0 = var_6;
                if (var_6 != 0UL) {
                    loop_state_var = 1U;
                    break;
                }
                if (var_6 > rcx1_0) {
                    continue;
                }
                loop_state_var = 3U;
                break;
            }
            var_7 = *(uint64_t *)6358344UL;
            if (var_7 != 0UL) {
                loop_state_var = 2U;
                break;
            }
            if (*(uint64_t *)6358352UL != 0UL) {
                loop_state_var = 2U;
                break;
            }
            loop_state_var = 0U;
            break;
        }
    switch (loop_state_var) {
      case 3U:
      case 1U:
      case 0U:
        {
            switch (loop_state_var) {
              case 0U:
                {
                    *(uint64_t *)(local_sp_1 + (-8L)) = 4204241UL;
                    indirect_placeholder_30(0UL, rcx1_3, r92_1, 1UL, r83_1, 0UL, 4249528UL);
                    abort();
                }
                break;
              case 3U:
              case 1U:
                {
                    var_8 = var_0 + (-16L);
                    *(uint64_t *)var_8 = 4204203UL;
                    var_9 = indirect_placeholder_29(0UL, rcx1_2, r9, 1UL, var_4, 0UL, 4250041UL);
                    local_sp_0 = var_8;
                    rcx1_1 = var_9.field_0;
                    r92_0 = var_9.field_1;
                    r83_0 = var_9.field_2;
                }
                break;
            }
        }
        break;
      case 2U:
        {
            if (var_2 == 1UL) {
            } else {
                storemerge = var_4;
                if ((var_7 | *(uint64_t *)6358352UL) == 0UL) {
                }
            }
            *(uint64_t *)6358360UL = storemerge;
            return;
        }
        break;
    }
}

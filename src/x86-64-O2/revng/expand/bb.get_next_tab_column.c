typedef unsigned char __u_char;
typedef unsigned short __u_short;
typedef unsigned int __u_int;
typedef unsigned long __u_long;
typedef signed char __int8_t;
typedef unsigned char __uint8_t;
typedef short __int16_t;
typedef unsigned short __uint16_t;
typedef int __int32_t;
typedef unsigned int __uint32_t;
typedef long __int64_t;
typedef unsigned long __uint64_t;
typedef __int8_t __int_least8_t;
typedef __uint8_t __uint_least8_t;
typedef __int16_t __int_least16_t;
typedef __uint16_t __uint_least16_t;
typedef __int32_t __int_least32_t;
typedef __uint32_t __uint_least32_t;
typedef __int64_t __int_least64_t;
typedef __uint64_t __uint_least64_t;
typedef long __quad_t;
typedef unsigned long __u_quad_t;
typedef long __intmax_t;
typedef unsigned long __uintmax_t;
typedef unsigned long __dev_t;
typedef unsigned int __uid_t;
typedef unsigned int __gid_t;
typedef unsigned long __ino_t;
typedef unsigned long __ino64_t;
typedef unsigned int __mode_t;
typedef unsigned long __nlink_t;
typedef long __off_t;
typedef long __off64_t;
typedef int __pid_t;
typedef struct {
    int __val[2];
} __fsid_t;
typedef long __clock_t;
typedef unsigned long __rlim_t;
typedef unsigned long __rlim64_t;
typedef unsigned int __id_t;
typedef long __time_t;
typedef unsigned int __useconds_t;
typedef long __suseconds_t;
typedef int __daddr_t;
typedef int __key_t;
typedef int __clockid_t;
typedef void *__timer_t;
typedef long __blksize_t;
typedef long __blkcnt_t;
typedef long __blkcnt64_t;
typedef unsigned long __fsblkcnt_t;
typedef unsigned long __fsblkcnt64_t;
typedef unsigned long __fsfilcnt_t;
typedef unsigned long __fsfilcnt64_t;
typedef long __fsword_t;
typedef long __ssize_t;
typedef long __syscall_slong_t;
typedef unsigned long __syscall_ulong_t;
typedef __off64_t __loff_t;
typedef char *__caddr_t;
typedef long __intptr_t;
typedef unsigned int __socklen_t;
typedef int __sig_atomic_t;
typedef __int8_t int8_t;
typedef __int16_t int16_t;
typedef __int32_t int32_t;
typedef __int64_t int64_t;
typedef __uint8_t uint8_t;
typedef __uint16_t uint16_t;
typedef __uint32_t uint32_t;
typedef __uint64_t uint64_t;
typedef __int_least8_t int_least8_t;
typedef __int_least16_t int_least16_t;
typedef __int_least32_t int_least32_t;
typedef __int_least64_t int_least64_t;
typedef __uint_least8_t uint_least8_t;
typedef __uint_least16_t uint_least16_t;
typedef __uint_least32_t uint_least32_t;
typedef __uint_least64_t uint_least64_t;
typedef signed char int_fast8_t;
typedef long int_fast16_t;
typedef long int_fast32_t;
typedef long int_fast64_t;
typedef unsigned char uint_fast8_t;
typedef unsigned long uint_fast16_t;
typedef unsigned long uint_fast32_t;
typedef unsigned long uint_fast64_t;
typedef long intptr_t;
typedef unsigned long uintptr_t;
typedef __intmax_t intmax_t;
typedef __uintmax_t uintmax_t;
struct bb_get_next_tab_column_ret_type;
struct helper_divq_EAX_wrapper_ret_type;
struct type_5;
struct bb_get_next_tab_column_ret_type {
    uint64_t field_0;
    uint64_t field_1;
    uint64_t field_2;
    uint64_t field_3;
    uint64_t field_4;
};
struct helper_divq_EAX_wrapper_ret_type {
    uint64_t field_0;
    uint64_t field_1;
    uint32_t field_2;
    uint32_t field_3;
    uint64_t field_4;
    uint64_t field_5;
    uint32_t field_6;
    uint64_t field_7;
    uint32_t field_8;
    uint64_t field_9;
    uint32_t field_10;
    unsigned char field_11;
    uint32_t field_12;
};
struct type_5 {
};
extern uint64_t revng_init_local_sp(uint64_t param_0);
extern uint64_t init_cc_src2(void);
extern uint64_t helper_cc_compute_c_wrapper(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint32_t param_3);
extern uint32_t init_state_0x8248(void);
extern uint64_t init_state_0x9018(void);
extern uint32_t init_state_0x9010(void);
extern uint64_t init_state_0x8408(void);
extern uint64_t init_state_0x8328(void);
extern uint64_t init_state_0x82d8(void);
extern uint32_t init_state_0x9080(void);
extern struct helper_divq_EAX_wrapper_ret_type helper_divq_EAX_wrapper(struct type_5 *param_0, uint64_t param_1, uint64_t param_2, uint64_t param_3, uint64_t param_4, uint64_t param_5, uint64_t param_6, uint64_t param_7, uint64_t param_8, uint64_t param_9, uint64_t param_10, uint64_t param_11, uint64_t param_12, uint32_t param_13, uint64_t param_14, uint32_t param_15, uint64_t param_16, uint64_t param_17, uint64_t param_18, uint32_t param_19);
struct bb_get_next_tab_column_ret_type bb_get_next_tab_column(uint64_t r10, uint64_t rbx, uint64_t r9, uint64_t rdi, uint64_t rbp, uint64_t rsi, uint64_t rdx) {
    uint64_t var_0;
    uint32_t var_1;
    uint64_t var_2;
    uint32_t var_3;
    uint64_t var_4;
    uint64_t var_5;
    uint64_t var_6;
    uint32_t var_7;
    uint64_t var_8;
    unsigned char *_cast;
    uint64_t r912_2;
    uint64_t *var_9;
    uint64_t var_10;
    uint64_t var_11;
    struct bb_get_next_tab_column_ret_type mrv1;
    struct bb_get_next_tab_column_ret_type mrv2;
    uint64_t r912_0;
    uint64_t var_12;
    uint64_t var_13;
    uint64_t var_14;
    uint64_t var_15;
    uint64_t r8_0;
    uint64_t rcx_0;
    uint64_t var_16;
    uint64_t var_17;
    uint64_t var_18;
    uint64_t var_19;
    uint64_t var_23;
    uint64_t r912_1;
    uint64_t var_24;
    uint64_t var_25;
    uint64_t var_26;
    struct helper_divq_EAX_wrapper_ret_type var_27;
    uint64_t var_28;
    uint64_t rcx_1;
    struct bb_get_next_tab_column_ret_type mrv;
    struct bb_get_next_tab_column_ret_type mrv3;
    struct bb_get_next_tab_column_ret_type mrv4;
    uint64_t rcx_2;
    uint64_t var_20;
    uint64_t var_22;
    struct helper_divq_EAX_wrapper_ret_type var_21;
    struct bb_get_next_tab_column_ret_type mrv5;
    struct bb_get_next_tab_column_ret_type mrv6;
    struct bb_get_next_tab_column_ret_type mrv7;
    struct bb_get_next_tab_column_ret_type mrv8;
    struct bb_get_next_tab_column_ret_type mrv9;
    unsigned int loop_state_var;
    revng_init_local_sp(0UL);
    var_0 = init_cc_src2();
    var_1 = init_state_0x8248();
    var_2 = init_state_0x9018();
    var_3 = init_state_0x9010();
    var_4 = init_state_0x8408();
    var_5 = init_state_0x8328();
    var_6 = init_state_0x82d8();
    var_7 = init_state_0x9080();
    var_8 = *(uint64_t *)6358360UL;
    _cast = (unsigned char *)rdx;
    *_cast = (unsigned char)'\x00';
    r912_2 = r9;
    r912_0 = r9;
    r8_0 = 0UL;
    rcx_1 = 0UL;
    rcx_2 = var_8;
    if (var_8 == 0UL) {
        var_20 = rcx_2 + rdi;
        var_21 = helper_divq_EAX_wrapper((struct type_5 *)(0UL), rcx_2, rdi, 4204377UL, rcx_2, r10, rbx, r912_2, rdi, rbp, var_20, rsi, 0UL, var_1, var_2, var_3, var_4, var_5, var_6, var_7);
        var_22 = var_20 - var_21.field_5;
        mrv5.field_0 = var_22;
        mrv6 = mrv5;
        mrv6.field_1 = rcx_2;
        mrv7 = mrv6;
        mrv7.field_2 = rbx;
        mrv8 = mrv7;
        mrv8.field_3 = r912_2;
        mrv9 = mrv8;
        mrv9.field_4 = var_22;
        return mrv9;
    }
    var_9 = (uint64_t *)rsi;
    var_10 = *var_9;
    var_11 = *(uint64_t *)6358320UL;
    if (var_11 > var_10) {
        var_19 = *(uint64_t *)6358352UL;
        r912_1 = r912_0;
        r912_2 = r912_0;
        rcx_2 = var_19;
        if (var_19 != 0UL) {
            var_23 = *(uint64_t *)6358344UL;
            if (var_23 == 0UL) {
                *_cast = (unsigned char)'\x01';
            } else {
                var_24 = *(uint64_t *)6358336UL;
                var_25 = var_23 + rdi;
                var_26 = rdi - *(uint64_t *)(((var_11 << 3UL) + var_24) + (-8L));
                var_27 = helper_divq_EAX_wrapper((struct type_5 *)(0UL), var_23, var_26, 4204413UL, var_25, r10, rbx, r912_0, var_26, rbp, var_23, rsi, 0UL, var_1, var_2, var_3, var_4, var_5, var_6, var_7);
                var_28 = var_25 - var_27.field_5;
                r8_0 = var_28;
                rcx_1 = var_28;
            }
            mrv.field_0 = r8_0;
            mrv1 = mrv;
            mrv1.field_1 = rcx_1;
            mrv2 = mrv1;
            mrv2.field_2 = rbx;
            mrv3 = mrv2;
            mrv3.field_3 = r912_1;
            mrv4 = mrv3;
            mrv4.field_4 = r8_0;
            return mrv4;
        }
    }
    var_12 = *(uint64_t *)6358336UL;
    var_13 = *(uint64_t *)((var_10 << 3UL) + var_12);
    var_14 = var_10 + 1UL;
    var_15 = helper_cc_compute_c_wrapper(rdi - var_13, var_13, var_0, 17U);
    r912_0 = var_12;
    r8_0 = var_13;
    rcx_0 = var_14;
    r912_1 = var_12;
    rcx_1 = var_14;
    if (var_15 == 0UL) {
        mrv.field_0 = r8_0;
        mrv1 = mrv;
        mrv1.field_1 = rcx_1;
        mrv2 = mrv1;
        mrv2.field_2 = rbx;
        mrv3 = mrv2;
        mrv3.field_3 = r912_1;
        mrv4 = mrv3;
        mrv4.field_4 = r8_0;
        return mrv4;
    }
    while (1U)
        {
            *var_9 = rcx_0;
            if (rcx_0 != var_11) {
                loop_state_var = 1U;
                break;
            }
            var_16 = rcx_0 + 1UL;
            var_17 = *(uint64_t *)(((var_16 << 3UL) + var_12) + (-8L));
            var_18 = helper_cc_compute_c_wrapper(rdi - var_17, var_17, var_0, 17U);
            rcx_0 = var_16;
            r8_0 = var_17;
            rcx_1 = var_16;
            if (var_18 == 0UL) {
                continue;
            }
            loop_state_var = 0U;
            break;
        }
    switch (loop_state_var) {
      case 0U:
        {
            mrv.field_0 = r8_0;
            mrv1 = mrv;
            mrv1.field_1 = rcx_1;
            mrv2 = mrv1;
            mrv2.field_2 = rbx;
            mrv3 = mrv2;
            mrv3.field_3 = r912_1;
            mrv4 = mrv3;
            mrv4.field_4 = r8_0;
            return mrv4;
        }
        break;
      case 1U:
        {
            break;
        }
        break;
    }
}

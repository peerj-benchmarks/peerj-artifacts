typedef unsigned char __u_char;
typedef unsigned short __u_short;
typedef unsigned int __u_int;
typedef unsigned long __u_long;
typedef signed char __int8_t;
typedef unsigned char __uint8_t;
typedef short __int16_t;
typedef unsigned short __uint16_t;
typedef int __int32_t;
typedef unsigned int __uint32_t;
typedef long __int64_t;
typedef unsigned long __uint64_t;
typedef __int8_t __int_least8_t;
typedef __uint8_t __uint_least8_t;
typedef __int16_t __int_least16_t;
typedef __uint16_t __uint_least16_t;
typedef __int32_t __int_least32_t;
typedef __uint32_t __uint_least32_t;
typedef __int64_t __int_least64_t;
typedef __uint64_t __uint_least64_t;
typedef long __quad_t;
typedef unsigned long __u_quad_t;
typedef long __intmax_t;
typedef unsigned long __uintmax_t;
typedef unsigned long __dev_t;
typedef unsigned int __uid_t;
typedef unsigned int __gid_t;
typedef unsigned long __ino_t;
typedef unsigned long __ino64_t;
typedef unsigned int __mode_t;
typedef unsigned long __nlink_t;
typedef long __off_t;
typedef long __off64_t;
typedef int __pid_t;
typedef struct {
    int __val[2];
} __fsid_t;
typedef long __clock_t;
typedef unsigned long __rlim_t;
typedef unsigned long __rlim64_t;
typedef unsigned int __id_t;
typedef long __time_t;
typedef unsigned int __useconds_t;
typedef long __suseconds_t;
typedef int __daddr_t;
typedef int __key_t;
typedef int __clockid_t;
typedef void *__timer_t;
typedef long __blksize_t;
typedef long __blkcnt_t;
typedef long __blkcnt64_t;
typedef unsigned long __fsblkcnt_t;
typedef unsigned long __fsblkcnt64_t;
typedef unsigned long __fsfilcnt_t;
typedef unsigned long __fsfilcnt64_t;
typedef long __fsword_t;
typedef long __ssize_t;
typedef long __syscall_slong_t;
typedef unsigned long __syscall_ulong_t;
typedef __off64_t __loff_t;
typedef char *__caddr_t;
typedef long __intptr_t;
typedef unsigned int __socklen_t;
typedef int __sig_atomic_t;
typedef __int8_t int8_t;
typedef __int16_t int16_t;
typedef __int32_t int32_t;
typedef __int64_t int64_t;
typedef __uint8_t uint8_t;
typedef __uint16_t uint16_t;
typedef __uint32_t uint32_t;
typedef __uint64_t uint64_t;
typedef __int_least8_t int_least8_t;
typedef __int_least16_t int_least16_t;
typedef __int_least32_t int_least32_t;
typedef __int_least64_t int_least64_t;
typedef __uint_least8_t uint_least8_t;
typedef __uint_least16_t uint_least16_t;
typedef __uint_least32_t uint_least32_t;
typedef __uint_least64_t uint_least64_t;
typedef signed char int_fast8_t;
typedef long int_fast16_t;
typedef long int_fast32_t;
typedef long int_fast64_t;
typedef unsigned char uint_fast8_t;
typedef unsigned long uint_fast16_t;
typedef unsigned long uint_fast32_t;
typedef unsigned long uint_fast64_t;
typedef long intptr_t;
typedef unsigned long uintptr_t;
typedef __intmax_t intmax_t;
typedef __uintmax_t uintmax_t;
typedef _Bool bool;
extern uint64_t revng_init_local_sp(uint64_t param_0);
extern void indirect_placeholder(void);
extern uint64_t init_rbx(void);
extern uint64_t init_rbp(void);
extern uint64_t init_r12(void);
extern uint64_t init_rax(void);
uint64_t bb_lookup_zone(uint64_t rdi, uint64_t rsi) {
    uint64_t var_0;
    uint64_t var_1;
    uint64_t var_2;
    uint64_t var_3;
    uint64_t var_4;
    uint64_t var_5;
    bool var_6;
    uint64_t local_sp_3;
    uint64_t local_sp_1;
    uint64_t merge;
    uint64_t var_8;
    uint64_t var_9;
    uint64_t local_sp_0;
    uint64_t local_sp_2;
    uint64_t rbx_1;
    uint64_t var_12;
    uint64_t var_13;
    uint64_t rbx_2;
    uint64_t var_10;
    uint64_t var_11;
    uint64_t rbx_3;
    uint64_t var_7;
    unsigned int loop_state_var;
    var_0 = revng_init_local_sp(0UL);
    var_1 = init_rax();
    var_2 = init_rbx();
    var_3 = init_rbp();
    var_4 = init_r12();
    *(uint64_t *)(var_0 + (-8L)) = var_4;
    *(uint64_t *)(var_0 + (-16L)) = var_3;
    var_5 = var_0 + (-24L);
    *(uint64_t *)var_5 = var_2;
    var_6 = ((uint64_t)(uint32_t)var_1 == 0UL);
    rbx_1 = 4301248UL;
    local_sp_3 = var_5;
    rbx_3 = 4302016UL;
    while (1U)
        {
            var_7 = local_sp_3 + (-8L);
            *(uint64_t *)var_7 = 4215988UL;
            indirect_placeholder();
            merge = rbx_3;
            local_sp_0 = var_7;
            local_sp_2 = var_7;
            local_sp_3 = var_7;
            if (!var_6) {
                loop_state_var = 0U;
                break;
            }
            var_8 = rbx_3 + 16UL;
            merge = 0UL;
            rbx_3 = var_8;
            if (*(uint64_t *)var_8 == 0UL) {
                continue;
            }
            var_9 = rdi + 232UL;
            rbx_2 = var_9;
            if (*(uint64_t *)var_9 != 0UL) {
                loop_state_var = 2U;
                break;
            }
            loop_state_var = 1U;
            break;
        }
    switch (loop_state_var) {
      case 0U:
        {
            return merge;
        }
        break;
      case 2U:
      case 1U:
        {
            switch (loop_state_var) {
              case 2U:
                {
                    var_10 = local_sp_2 + (-8L);
                    *(uint64_t *)var_10 = 4216092UL;
                    indirect_placeholder();
                    var_11 = rbx_2 + 16UL;
                    local_sp_0 = var_10;
                    local_sp_2 = var_10;
                    rbx_2 = var_11;
                    do {
                        var_10 = local_sp_2 + (-8L);
                        *(uint64_t *)var_10 = 4216092UL;
                        indirect_placeholder();
                        var_11 = rbx_2 + 16UL;
                        local_sp_0 = var_10;
                        local_sp_2 = var_10;
                        rbx_2 = var_11;
                    } while (*(uint64_t *)var_11 != 0UL);
                }
                break;
              case 1U:
                {
                    local_sp_1 = local_sp_0;
                    var_12 = local_sp_1 + (-8L);
                    *(uint64_t *)var_12 = 4216060UL;
                    indirect_placeholder();
                    var_13 = rbx_1 + 16UL;
                    local_sp_1 = var_12;
                    rbx_1 = var_13;
                    do {
                        var_12 = local_sp_1 + (-8L);
                        *(uint64_t *)var_12 = 4216060UL;
                        indirect_placeholder();
                        var_13 = rbx_1 + 16UL;
                        local_sp_1 = var_12;
                        rbx_1 = var_13;
                    } while (*(uint64_t *)var_13 != 0UL);
                }
                break;
            }
        }
        break;
    }
}

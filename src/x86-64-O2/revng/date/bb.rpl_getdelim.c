typedef unsigned char __u_char;
typedef unsigned short __u_short;
typedef unsigned int __u_int;
typedef unsigned long __u_long;
typedef signed char __int8_t;
typedef unsigned char __uint8_t;
typedef short __int16_t;
typedef unsigned short __uint16_t;
typedef int __int32_t;
typedef unsigned int __uint32_t;
typedef long __int64_t;
typedef unsigned long __uint64_t;
typedef __int8_t __int_least8_t;
typedef __uint8_t __uint_least8_t;
typedef __int16_t __int_least16_t;
typedef __uint16_t __uint_least16_t;
typedef __int32_t __int_least32_t;
typedef __uint32_t __uint_least32_t;
typedef __int64_t __int_least64_t;
typedef __uint64_t __uint_least64_t;
typedef long __quad_t;
typedef unsigned long __u_quad_t;
typedef long __intmax_t;
typedef unsigned long __uintmax_t;
typedef unsigned long __dev_t;
typedef unsigned int __uid_t;
typedef unsigned int __gid_t;
typedef unsigned long __ino_t;
typedef unsigned long __ino64_t;
typedef unsigned int __mode_t;
typedef unsigned long __nlink_t;
typedef long __off_t;
typedef long __off64_t;
typedef int __pid_t;
typedef struct {
    int __val[2];
} __fsid_t;
typedef long __clock_t;
typedef unsigned long __rlim_t;
typedef unsigned long __rlim64_t;
typedef unsigned int __id_t;
typedef long __time_t;
typedef unsigned int __useconds_t;
typedef long __suseconds_t;
typedef int __daddr_t;
typedef int __key_t;
typedef int __clockid_t;
typedef void *__timer_t;
typedef long __blksize_t;
typedef long __blkcnt_t;
typedef long __blkcnt64_t;
typedef unsigned long __fsblkcnt_t;
typedef unsigned long __fsblkcnt64_t;
typedef unsigned long __fsfilcnt_t;
typedef unsigned long __fsfilcnt64_t;
typedef long __fsword_t;
typedef long __ssize_t;
typedef long __syscall_slong_t;
typedef unsigned long __syscall_ulong_t;
typedef __off64_t __loff_t;
typedef char *__caddr_t;
typedef long __intptr_t;
typedef unsigned int __socklen_t;
typedef int __sig_atomic_t;
typedef __int8_t int8_t;
typedef __int16_t int16_t;
typedef __int32_t int32_t;
typedef __int64_t int64_t;
typedef __uint8_t uint8_t;
typedef __uint16_t uint16_t;
typedef __uint32_t uint32_t;
typedef __uint64_t uint64_t;
typedef __int_least8_t int_least8_t;
typedef __int_least16_t int_least16_t;
typedef __int_least32_t int_least32_t;
typedef __int_least64_t int_least64_t;
typedef __uint_least8_t uint_least8_t;
typedef __uint_least16_t uint_least16_t;
typedef __uint_least32_t uint_least32_t;
typedef __uint_least64_t uint_least64_t;
typedef signed char int_fast8_t;
typedef long int_fast16_t;
typedef long int_fast32_t;
typedef long int_fast64_t;
typedef unsigned char uint_fast8_t;
typedef unsigned long uint_fast16_t;
typedef unsigned long uint_fast32_t;
typedef unsigned long uint_fast64_t;
typedef long intptr_t;
typedef unsigned long uintptr_t;
typedef __intmax_t intmax_t;
typedef __uintmax_t uintmax_t;
typedef _Bool bool;
extern uint64_t revng_init_local_sp(uint64_t param_0);
extern void indirect_placeholder(void);
extern uint64_t init_rbx(void);
extern uint64_t init_rbp(void);
extern uint64_t init_cc_src2(void);
extern uint64_t helper_cc_compute_c_wrapper(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint32_t param_3);
extern uint64_t init_r13(void);
extern uint64_t init_r12(void);
extern uint64_t init_r14(void);
extern uint64_t init_rax(void);
extern uint64_t init_r15(void);
extern void indirect_placeholder_3(uint64_t param_0, uint64_t param_1);
uint64_t bb_rpl_getdelim(uint64_t rcx, uint64_t rdi, uint64_t rdx, uint64_t rsi) {
    uint64_t var_0;
    uint64_t var_1;
    uint64_t var_2;
    uint64_t var_3;
    uint64_t var_4;
    uint64_t var_5;
    uint64_t var_6;
    uint64_t var_7;
    uint64_t var_8;
    uint64_t var_9;
    bool var_10;
    bool var_11;
    uint64_t var_12;
    uint64_t var_27;
    uint64_t var_28;
    uint64_t var_29;
    uint64_t local_sp_0;
    uint64_t local_sp_1;
    uint64_t r15_1;
    uint64_t r15_0;
    uint64_t *var_13;
    uint64_t *_pre_phi49;
    uint64_t rax_0_be;
    uint64_t local_sp_1_be;
    uint64_t rax_0;
    uint64_t var_19;
    uint64_t var_20;
    uint64_t var_21;
    uint64_t var_24;
    uint64_t var_25;
    uint64_t var_26;
    uint64_t var_22;
    uint64_t var_23;
    uint64_t spec_select;
    uint64_t merge;
    uint64_t var_14;
    uint64_t *_pre_phi;
    uint64_t *_pre;
    uint64_t *var_15;
    uint64_t var_17;
    uint32_t var_18;
    uint64_t var_16;
    unsigned int loop_state_var;
    var_0 = revng_init_local_sp(0UL);
    var_1 = init_r15();
    var_2 = init_rax();
    var_3 = init_rbx();
    var_4 = init_rbp();
    var_5 = init_r13();
    var_6 = init_cc_src2();
    var_7 = init_r12();
    var_8 = init_r14();
    *(uint64_t *)(var_0 + (-8L)) = var_1;
    *(uint64_t *)(var_0 + (-16L)) = var_8;
    *(uint64_t *)(var_0 + (-24L)) = var_5;
    *(uint64_t *)(var_0 + (-32L)) = var_7;
    *(uint64_t *)(var_0 + (-40L)) = var_4;
    *(uint64_t *)(var_0 + (-48L)) = var_3;
    var_9 = var_0 + (-72L);
    *(uint32_t *)(var_0 + (-60L)) = (uint32_t)rdx;
    var_10 = (rsi == 0UL);
    var_11 = (rcx == 0UL);
    var_12 = (var_2 & (-256L)) | var_11;
    *(uint64_t *)var_9 = rcx;
    local_sp_0 = var_9;
    r15_1 = 0UL;
    rax_0_be = 9223372036854775808UL;
    rax_0 = var_12;
    merge = 18446744073709551615UL;
    if ((var_10 || var_11) || (rdi == 0UL)) {
        *(uint64_t *)(var_0 + (-80L)) = 4283082UL;
        indirect_placeholder();
        *(uint32_t *)var_12 = 22U;
    } else {
        var_13 = (uint64_t *)rdi;
        var_14 = *var_13;
        if (var_14 == 0UL) {
            _pre = (uint64_t *)rsi;
            _pre_phi49 = _pre;
            *_pre_phi49 = 120UL;
            var_16 = var_0 + (-80L);
            *(uint64_t *)var_16 = 4283018UL;
            indirect_placeholder_3(var_14, 120UL);
            _pre_phi = _pre_phi49;
            local_sp_0 = var_16;
            if (var_12 == 0UL) {
                return merge;
            }
            *var_13 = var_12;
        } else {
            var_15 = (uint64_t *)rsi;
            _pre_phi = var_15;
            _pre_phi49 = var_15;
            if (*var_15 != 0UL) {
                *_pre_phi49 = 120UL;
                var_16 = var_0 + (-80L);
                *(uint64_t *)var_16 = 4283018UL;
                indirect_placeholder_3(var_14, 120UL);
                _pre_phi = _pre_phi49;
                local_sp_0 = var_16;
                if (var_12 != 0UL) {
                    return merge;
                }
                *var_13 = var_12;
            }
        }
        local_sp_1 = local_sp_0;
        while (1U)
            {
                var_17 = local_sp_1 + (-8L);
                *(uint64_t *)var_17 = 4282866UL;
                indirect_placeholder();
                var_18 = (uint32_t)rax_0;
                local_sp_1_be = var_17;
                r15_0 = r15_1;
                if ((uint64_t)(var_18 + 1U) != 0UL) {
                    loop_state_var = 0U;
                    break;
                }
                var_19 = *_pre_phi;
                var_20 = r15_1 + 1UL;
                var_21 = helper_cc_compute_c_wrapper(var_20 - var_19, var_19, var_6, 17U);
                r15_0 = var_20;
                r15_1 = var_20;
                if (var_21 != 0UL) {
                    var_22 = (uint64_t)(var_18 - *(uint32_t *)(local_sp_1 + 4UL));
                    var_23 = *var_13;
                    *(unsigned char *)(r15_1 + var_23) = (unsigned char)rax_0;
                    rax_0_be = var_23;
                    if (var_22 != 0UL) {
                        loop_state_var = 0U;
                        break;
                    }
                    rax_0 = rax_0_be;
                    local_sp_1 = local_sp_1_be;
                    continue;
                }
                var_24 = (var_19 << 1UL) | 1UL;
                var_25 = (var_24 < 9223372036854775808UL) ? var_24 : 9223372036854775808UL;
                var_26 = helper_cc_compute_c_wrapper(var_20 - var_25, var_25, var_6, 17U);
                if (var_26 == 0UL) {
                    var_27 = *var_13;
                    var_28 = local_sp_1 + (-16L);
                    *(uint64_t *)var_28 = 4282932UL;
                    indirect_placeholder_3(var_27, var_25);
                    var_29 = (uint64_t)(var_18 - *(uint32_t *)(local_sp_1 + (-4L)));
                    *var_13 = 9223372036854775808UL;
                    *_pre_phi = var_25;
                    *(unsigned char *)(r15_1 ^ (-9223372036854775808L)) = (unsigned char)rax_0;
                    local_sp_1_be = var_28;
                    if (var_29 != 0UL) {
                        loop_state_var = 0U;
                        break;
                    }
                }
                *(uint64_t *)(local_sp_1 + (-16L)) = 4283069UL;
                indirect_placeholder();
                *(uint32_t *)9223372036854775808UL = 75U;
                loop_state_var = 1U;
                break;
            }
        *(unsigned char *)(r15_0 + *var_13) = (unsigned char)'\x00';
        spec_select = (r15_0 == 0UL) ? 18446744073709551615UL : r15_0;
        merge = spec_select;
    }
}

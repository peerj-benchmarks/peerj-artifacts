typedef unsigned char __u_char;
typedef unsigned short __u_short;
typedef unsigned int __u_int;
typedef unsigned long __u_long;
typedef signed char __int8_t;
typedef unsigned char __uint8_t;
typedef short __int16_t;
typedef unsigned short __uint16_t;
typedef int __int32_t;
typedef unsigned int __uint32_t;
typedef long __int64_t;
typedef unsigned long __uint64_t;
typedef __int8_t __int_least8_t;
typedef __uint8_t __uint_least8_t;
typedef __int16_t __int_least16_t;
typedef __uint16_t __uint_least16_t;
typedef __int32_t __int_least32_t;
typedef __uint32_t __uint_least32_t;
typedef __int64_t __int_least64_t;
typedef __uint64_t __uint_least64_t;
typedef long __quad_t;
typedef unsigned long __u_quad_t;
typedef long __intmax_t;
typedef unsigned long __uintmax_t;
typedef unsigned long __dev_t;
typedef unsigned int __uid_t;
typedef unsigned int __gid_t;
typedef unsigned long __ino_t;
typedef unsigned long __ino64_t;
typedef unsigned int __mode_t;
typedef unsigned long __nlink_t;
typedef long __off_t;
typedef long __off64_t;
typedef int __pid_t;
typedef struct {
    int __val[2];
} __fsid_t;
typedef long __clock_t;
typedef unsigned long __rlim_t;
typedef unsigned long __rlim64_t;
typedef unsigned int __id_t;
typedef long __time_t;
typedef unsigned int __useconds_t;
typedef long __suseconds_t;
typedef int __daddr_t;
typedef int __key_t;
typedef int __clockid_t;
typedef void *__timer_t;
typedef long __blksize_t;
typedef long __blkcnt_t;
typedef long __blkcnt64_t;
typedef unsigned long __fsblkcnt_t;
typedef unsigned long __fsblkcnt64_t;
typedef unsigned long __fsfilcnt_t;
typedef unsigned long __fsfilcnt64_t;
typedef long __fsword_t;
typedef long __ssize_t;
typedef long __syscall_slong_t;
typedef unsigned long __syscall_ulong_t;
typedef __off64_t __loff_t;
typedef char *__caddr_t;
typedef long __intptr_t;
typedef unsigned int __socklen_t;
typedef int __sig_atomic_t;
typedef __int8_t int8_t;
typedef __int16_t int16_t;
typedef __int32_t int32_t;
typedef __int64_t int64_t;
typedef __uint8_t uint8_t;
typedef __uint16_t uint16_t;
typedef __uint32_t uint32_t;
typedef __uint64_t uint64_t;
typedef __int_least8_t int_least8_t;
typedef __int_least16_t int_least16_t;
typedef __int_least32_t int_least32_t;
typedef __int_least64_t int_least64_t;
typedef __uint_least8_t uint_least8_t;
typedef __uint_least16_t uint_least16_t;
typedef __uint_least32_t uint_least32_t;
typedef __uint_least64_t uint_least64_t;
typedef signed char int_fast8_t;
typedef long int_fast16_t;
typedef long int_fast32_t;
typedef long int_fast64_t;
typedef unsigned char uint_fast8_t;
typedef unsigned long uint_fast16_t;
typedef unsigned long uint_fast32_t;
typedef unsigned long uint_fast64_t;
typedef long intptr_t;
typedef unsigned long uintptr_t;
typedef __intmax_t intmax_t;
typedef __uintmax_t uintmax_t;
typedef _Bool bool;
extern uint64_t revng_init_local_sp(uint64_t param_0);
void bb_time_zone_hhmm_isra_3(uint64_t rcx, uint64_t rdi, uint64_t r8, uint64_t rdx, uint64_t rsi) {
    uint64_t r9_3;
    uint64_t var_0;
    uint64_t r9_0;
    uint64_t var_1;
    uint64_t var_2;
    uint64_t storemerge;
    bool var_3;
    uint64_t var_4;
    uint64_t storemerge3;
    revng_init_local_sp(0UL);
    r9_0 = rdx;
    if ((long)rcx > (long)2UL) {
        if ((long)r8 <= (long)18446744073709551615UL) {
            var_1 = (uint64_t)((long)(r9_0 + (uint64_t)(((unsigned __int128)r9_0 * 18446744073709551615ULL) >> 64ULL)) >> (long)6UL) - (uint64_t)((long)r9_0 >> (long)63UL);
            var_2 = (r9_0 + (var_1 * 18446744073709551516UL)) + (var_1 * 60UL);
            r9_3 = var_2;
            if ((r9_3 + 1440UL) > 2880UL) {
                return;
            }
            *(uint32_t *)rdi = ((uint32_t)r9_3 * 60U);
            return;
        }
    }
    if ((long)r8 >= (long)0UL) {
        var_0 = rdx * 100UL;
        r9_0 = var_0;
        var_1 = (uint64_t)((long)(r9_0 + (uint64_t)(((unsigned __int128)r9_0 * 18446744073709551615ULL) >> 64ULL)) >> (long)6UL) - (uint64_t)((long)r9_0 >> (long)63UL);
        var_2 = (r9_0 + (var_1 * 18446744073709551516UL)) + (var_1 * 60UL);
        r9_3 = var_2;
        if ((r9_3 + 1440UL) > 2880UL) {
            return;
        }
        *(uint32_t *)rdi = ((uint32_t)r9_3 * 60U);
        return;
    }
    if ((long)rdx < (long)0UL) {
        storemerge = ((long)rdx < (long)18293021206428638686UL) | (-153722867280913152L);
    } else {
        storemerge = ((long)rdx > (long)153722867280912930UL) | 153722867280912896UL;
    }
    var_3 = ((uint64_t)(unsigned char)storemerge == 0UL);
    var_4 = rdx * 60UL;
    if ((uint64_t)(unsigned char)rsi == 0UL) {
        if ((long)(9223372036854775807UL - r8) >= (long)var_4) {
            return;
        }
        storemerge3 = var_4 + r8;
    } else {
        if ((long)(r8 ^ (-9223372036854775808L)) <= (long)var_4) {
            return;
        }
        storemerge3 = var_4 - r8;
    }
    r9_3 = storemerge3;
    if ((uint64_t)(uint32_t)(var_3 ? 0UL : 1UL) == 0UL) {
        return;
    }
    if ((r9_3 + 1440UL) > 2880UL) {
        return;
    }
    *(uint32_t *)rdi = ((uint32_t)r9_3 * 60U);
    return;
    *(uint32_t *)rdi = ((uint32_t)r9_3 * 60U);
    return;
}

typedef unsigned char __u_char;
typedef unsigned short __u_short;
typedef unsigned int __u_int;
typedef unsigned long __u_long;
typedef signed char __int8_t;
typedef unsigned char __uint8_t;
typedef short __int16_t;
typedef unsigned short __uint16_t;
typedef int __int32_t;
typedef unsigned int __uint32_t;
typedef long __int64_t;
typedef unsigned long __uint64_t;
typedef __int8_t __int_least8_t;
typedef __uint8_t __uint_least8_t;
typedef __int16_t __int_least16_t;
typedef __uint16_t __uint_least16_t;
typedef __int32_t __int_least32_t;
typedef __uint32_t __uint_least32_t;
typedef __int64_t __int_least64_t;
typedef __uint64_t __uint_least64_t;
typedef long __quad_t;
typedef unsigned long __u_quad_t;
typedef long __intmax_t;
typedef unsigned long __uintmax_t;
typedef unsigned long __dev_t;
typedef unsigned int __uid_t;
typedef unsigned int __gid_t;
typedef unsigned long __ino_t;
typedef unsigned long __ino64_t;
typedef unsigned int __mode_t;
typedef unsigned long __nlink_t;
typedef long __off_t;
typedef long __off64_t;
typedef int __pid_t;
typedef struct {
    int __val[2];
} __fsid_t;
typedef long __clock_t;
typedef unsigned long __rlim_t;
typedef unsigned long __rlim64_t;
typedef unsigned int __id_t;
typedef long __time_t;
typedef unsigned int __useconds_t;
typedef long __suseconds_t;
typedef int __daddr_t;
typedef int __key_t;
typedef int __clockid_t;
typedef void *__timer_t;
typedef long __blksize_t;
typedef long __blkcnt_t;
typedef long __blkcnt64_t;
typedef unsigned long __fsblkcnt_t;
typedef unsigned long __fsblkcnt64_t;
typedef unsigned long __fsfilcnt_t;
typedef unsigned long __fsfilcnt64_t;
typedef long __fsword_t;
typedef long __ssize_t;
typedef long __syscall_slong_t;
typedef unsigned long __syscall_ulong_t;
typedef __off64_t __loff_t;
typedef char *__caddr_t;
typedef long __intptr_t;
typedef unsigned int __socklen_t;
typedef int __sig_atomic_t;
typedef __int8_t int8_t;
typedef __int16_t int16_t;
typedef __int32_t int32_t;
typedef __int64_t int64_t;
typedef __uint8_t uint8_t;
typedef __uint16_t uint16_t;
typedef __uint32_t uint32_t;
typedef __uint64_t uint64_t;
typedef __int_least8_t int_least8_t;
typedef __int_least16_t int_least16_t;
typedef __int_least32_t int_least32_t;
typedef __int_least64_t int_least64_t;
typedef __uint_least8_t uint_least8_t;
typedef __uint_least16_t uint_least16_t;
typedef __uint_least32_t uint_least32_t;
typedef __uint_least64_t uint_least64_t;
typedef signed char int_fast8_t;
typedef long int_fast16_t;
typedef long int_fast32_t;
typedef long int_fast64_t;
typedef unsigned char uint_fast8_t;
typedef unsigned long uint_fast16_t;
typedef unsigned long uint_fast32_t;
typedef unsigned long uint_fast64_t;
typedef long intptr_t;
typedef unsigned long uintptr_t;
typedef __intmax_t intmax_t;
typedef __uintmax_t uintmax_t;
typedef _Bool bool;
struct indirect_placeholder_24_ret_type;
struct indirect_placeholder_25_ret_type;
struct indirect_placeholder_26_ret_type;
struct indirect_placeholder_24_ret_type {
    uint64_t field_0;
    uint64_t field_1;
    uint64_t field_2;
};
struct indirect_placeholder_25_ret_type {
    uint64_t field_0;
    uint64_t field_1;
    uint64_t field_2;
};
struct indirect_placeholder_26_ret_type {
    uint64_t field_0;
    uint64_t field_1;
    uint64_t field_2;
};
extern uint64_t revng_init_local_sp(uint64_t param_0);
extern void indirect_placeholder(void);
extern uint64_t init_rbx(void);
extern uint64_t init_rbp(void);
extern uint64_t init_r13(void);
extern uint64_t init_r12(void);
extern uint64_t init_r14(void);
extern uint64_t init_cc_src2(void);
extern uint64_t helper_cc_compute_c_wrapper(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint32_t param_3);
extern uint64_t init_r15(void);
extern struct indirect_placeholder_24_ret_type indirect_placeholder_24(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint64_t param_3, uint64_t param_4, uint64_t param_5, uint64_t param_6);
extern struct indirect_placeholder_25_ret_type indirect_placeholder_25(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint64_t param_3, uint64_t param_4, uint64_t param_5, uint64_t param_6);
extern struct indirect_placeholder_26_ret_type indirect_placeholder_26(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint64_t param_3, uint64_t param_4, uint64_t param_5);
void bb_abformat_init(void) {
    uint64_t *var_18;
    struct indirect_placeholder_24_ret_type var_35;
    struct indirect_placeholder_25_ret_type var_37;
    uint64_t var_0;
    uint64_t var_1;
    uint64_t var_2;
    uint64_t var_3;
    uint64_t var_4;
    uint64_t var_5;
    uint64_t var_6;
    uint64_t var_7;
    uint64_t var_8;
    uint64_t rax_0;
    uint64_t r9_0;
    uint64_t r8_0;
    uint64_t local_sp_0;
    uint64_t rax_2;
    uint64_t var_38;
    uint64_t local_sp_1;
    uint64_t r15_0;
    uint64_t local_sp_2;
    uint64_t *var_39;
    uint64_t var_40;
    uint64_t var_27;
    uint64_t r9_2;
    uint64_t r12_0;
    uint64_t var_22;
    uint64_t local_sp_4;
    uint64_t var_23;
    uint64_t var_24;
    uint64_t rbp_0;
    uint64_t local_sp_3;
    uint64_t var_17;
    uint64_t r9_1;
    uint64_t var_25;
    uint64_t var_26;
    uint64_t r8_1;
    uint64_t var_28;
    uint64_t var_29;
    uint64_t var_30;
    uint64_t var_31;
    uint64_t var_32;
    bool var_33;
    uint64_t var_34;
    uint64_t r8_2;
    uint64_t r13_0;
    uint64_t r15_1;
    uint64_t var_36;
    uint64_t rax_3;
    uint64_t var_19;
    uint64_t var_20;
    struct indirect_placeholder_26_ret_type var_21;
    uint64_t rcx_0;
    uint64_t var_9;
    unsigned char var_10;
    uint64_t rdx_1;
    uint64_t rdx_0_be;
    uint64_t rax_1_be;
    uint64_t rax_1;
    uint64_t rdx_0;
    uint64_t var_11;
    uint64_t var_12;
    unsigned char var_13;
    uint64_t var_14;
    unsigned char var_15;
    uint64_t var_16;
    uint64_t r13_1;
    uint64_t rbx_0;
    unsigned int loop_state_var;
    bool switch_state_var;
    var_0 = revng_init_local_sp(0UL);
    var_1 = init_rbx();
    var_2 = init_rbp();
    var_3 = init_r13();
    var_4 = init_cc_src2();
    var_5 = init_r12();
    var_6 = init_r14();
    var_7 = init_r15();
    *(uint64_t *)(var_0 + (-8L)) = var_7;
    *(uint64_t *)(var_0 + (-16L)) = var_6;
    *(uint64_t *)(var_0 + (-24L)) = var_3;
    *(uint64_t *)(var_0 + (-32L)) = var_5;
    *(uint64_t *)(var_0 + (-40L)) = var_2;
    *(uint64_t *)(var_0 + (-48L)) = var_1;
    var_8 = var_0 + (-1656L);
    r15_0 = 6470528UL;
    var_27 = 0UL;
    r12_0 = 131086UL;
    rbp_0 = 0UL;
    local_sp_3 = var_8;
    rcx_0 = 0UL;
    rdx_1 = 0UL;
    r13_1 = 5UL;
    while (1U)
        {
            var_9 = *(uint64_t *)(rcx_0 + 6469392UL);
            var_10 = *(unsigned char *)var_9;
            rdx_0 = var_9;
            if (var_10 != '\x00') {
                rax_1 = (uint64_t)var_10;
                while (1U)
                    {
                        var_11 = (uint64_t)((unsigned char)rax_1 + '\xdb');
                        var_12 = rdx_0 + 1UL;
                        var_13 = *(unsigned char *)var_12;
                        rax_1_be = (uint64_t)var_13;
                        rdx_0_be = var_12;
                        rdx_1 = rdx_0;
                        if (var_11 != 0UL) {
                            if (var_13 == '\x00') {
                                break;
                            }
                            rax_1 = rax_1_be;
                            rdx_0 = rdx_0_be;
                            continue;
                        }
                        if ((uint64_t)(var_13 + '\xdb') != 0UL) {
                            if ((uint64_t)(var_13 + '\x9e') == 0UL) {
                                break;
                            }
                            if (var_13 == '\x00') {
                                break;
                            }
                        }
                        var_14 = rdx_0 + 2UL;
                        var_15 = *(unsigned char *)var_14;
                        rdx_0_be = var_14;
                        if (var_15 != '\x00') {
                            break;
                        }
                        rax_1_be = (uint64_t)var_15;
                    }
            }
            *(uint64_t *)((rcx_0 + var_8) + 48UL) = rdx_1;
            if (rcx_0 == 8UL) {
                break;
            }
            rcx_0 = rcx_0 + 8UL;
            continue;
        }
    if (*(uint64_t *)(var_0 + (-1608L)) != 0UL) {
        if (*(uint64_t *)(var_0 + (-1600L)) == 0UL) {
            return;
        }
    }
    var_16 = var_0 + (-1592L);
    *(uint64_t *)(var_0 + (-1640L)) = var_16;
    var_17 = var_16;
    rax_2 = var_16;
    while (1U)
        {
            rax_3 = rax_2;
            rbx_0 = var_17;
            local_sp_4 = local_sp_3;
            while (1U)
                {
                    *(uint64_t *)(local_sp_4 + 40UL) = r13_1;
                    var_18 = (uint64_t *)(local_sp_4 + (-8L));
                    *var_18 = 4213709UL;
                    indirect_placeholder();
                    *(uint64_t *)(local_sp_4 + (-16L)) = 4213725UL;
                    indirect_placeholder();
                    if (rax_3 != 0UL) {
                        loop_state_var = 1U;
                        break;
                    }
                    var_19 = local_sp_4 + 24UL;
                    var_20 = local_sp_4 + (-24L);
                    *(uint64_t *)var_20 = 4213761UL;
                    var_21 = indirect_placeholder_26(var_19, 0UL, 0UL, 0UL, 128UL, rbx_0);
                    local_sp_1 = var_20;
                    local_sp_3 = var_20;
                    local_sp_4 = var_20;
                    if (var_21.field_0 <= 127UL) {
                        loop_state_var = 1U;
                        break;
                    }
                    var_22 = *(uint64_t *)(local_sp_4 + 16UL);
                    var_23 = helper_cc_compute_c_wrapper(rbp_0 - var_22, var_22, var_4, 17U);
                    var_24 = (var_23 == 0UL) ? rbp_0 : var_22;
                    rax_2 = var_22;
                    r13_1 = var_24;
                    rax_3 = var_22;
                    rbp_0 = var_24;
                    if (rbx_0 != (local_sp_3 + 1472UL)) {
                        loop_state_var = 0U;
                        break;
                    }
                    rbx_0 = rbx_0 + 128UL;
                    r12_0 = (uint64_t)((uint32_t)r12_0 + 1U);
                    continue;
                }
            switch_state_var = 0;
            switch (loop_state_var) {
              case 1U:
                {
                    switch_state_var = 1;
                    break;
                }
                break;
              case 0U:
                {
                    if (r13_1 > var_24) {
                        var_17 = *var_18;
                        continue;
                    }
                    var_25 = var_21.field_1;
                    var_26 = var_21.field_2;
                    *(uint64_t *)local_sp_4 = 0UL;
                    r9_1 = var_25;
                    r8_1 = var_26;
                    while (1U)
                        {
                            var_28 = *(uint64_t *)(local_sp_1 + 16UL);
                            var_29 = *(uint64_t *)((var_27 + local_sp_1) + 48UL);
                            var_30 = *(uint64_t *)(var_27 + 6469392UL);
                            *(uint64_t *)(local_sp_1 + 8UL) = (r15_0 + 1536UL);
                            var_31 = var_29 + 2UL;
                            var_32 = var_29 - var_30;
                            var_33 = ((long)var_32 > (long)128UL);
                            var_34 = (uint64_t)(uint32_t)var_32;
                            local_sp_2 = local_sp_1;
                            r9_2 = r9_1;
                            r8_2 = r8_1;
                            r13_0 = var_28;
                            r15_1 = r15_0;
                            while (1U)
                                {
                                    if (var_29 == 0UL) {
                                        var_36 = local_sp_2 + (-8L);
                                        *(uint64_t *)var_36 = 4214071UL;
                                        var_37 = indirect_placeholder_25(0UL, var_30, r9_2, r15_1, r8_2, 4342748UL, 128UL);
                                        rax_0 = var_37.field_0;
                                        r9_0 = var_37.field_1;
                                        r8_0 = var_37.field_2;
                                        local_sp_0 = var_36;
                                    } else {
                                        if (!var_33) {
                                            loop_state_var = 1U;
                                            break;
                                        }
                                        *(uint64_t *)(local_sp_2 + (-16L)) = var_31;
                                        *(uint64_t *)(local_sp_2 + (-24L)) = 4213964UL;
                                        var_35 = indirect_placeholder_24(0UL, var_34, r13_0, r15_1, var_30, 4332757UL, 128UL);
                                        rax_0 = var_35.field_0;
                                        r9_0 = var_35.field_1;
                                        r8_0 = var_35.field_2;
                                        local_sp_0 = local_sp_2 + (-8L);
                                    }
                                    local_sp_1 = local_sp_0;
                                    local_sp_2 = local_sp_0;
                                    r9_2 = r9_0;
                                    r9_1 = r9_0;
                                    r8_1 = r8_0;
                                    r8_2 = r8_0;
                                    if ((uint64_t)((uint32_t)rax_0 & (-128)) != 0UL) {
                                        loop_state_var = 1U;
                                        break;
                                    }
                                    var_38 = r15_1 + 128UL;
                                    r15_0 = var_38;
                                    r15_1 = var_38;
                                    if (var_38 != *(uint64_t *)(local_sp_0 + 8UL)) {
                                        loop_state_var = 0U;
                                        break;
                                    }
                                    r13_0 = r13_0 + 128UL;
                                    continue;
                                }
                            switch_state_var = 0;
                            switch (loop_state_var) {
                              case 1U:
                                {
                                    switch_state_var = 1;
                                    break;
                                }
                                break;
                              case 0U:
                                {
                                    var_39 = (uint64_t *)(local_sp_0 + 24UL);
                                    var_40 = *var_39 + 8UL;
                                    *var_39 = var_40;
                                    var_27 = var_40;
                                    if (var_40 == 16UL) {
                                        continue;
                                    }
                                    *(unsigned char *)6470472UL = (unsigned char)'\x01';
                                    switch_state_var = 1;
                                    break;
                                }
                                break;
                            }
                            if (switch_state_var)
                                break;
                        }
                    switch_state_var = 1;
                    break;
                }
                break;
            }
            if (switch_state_var)
                break;
        }
    return;
}

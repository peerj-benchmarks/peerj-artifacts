typedef unsigned char __u_char;
typedef unsigned short __u_short;
typedef unsigned int __u_int;
typedef unsigned long __u_long;
typedef signed char __int8_t;
typedef unsigned char __uint8_t;
typedef short __int16_t;
typedef unsigned short __uint16_t;
typedef int __int32_t;
typedef unsigned int __uint32_t;
typedef long __int64_t;
typedef unsigned long __uint64_t;
typedef __int8_t __int_least8_t;
typedef __uint8_t __uint_least8_t;
typedef __int16_t __int_least16_t;
typedef __uint16_t __uint_least16_t;
typedef __int32_t __int_least32_t;
typedef __uint32_t __uint_least32_t;
typedef __int64_t __int_least64_t;
typedef __uint64_t __uint_least64_t;
typedef long __quad_t;
typedef unsigned long __u_quad_t;
typedef long __intmax_t;
typedef unsigned long __uintmax_t;
typedef unsigned long __dev_t;
typedef unsigned int __uid_t;
typedef unsigned int __gid_t;
typedef unsigned long __ino_t;
typedef unsigned long __ino64_t;
typedef unsigned int __mode_t;
typedef unsigned long __nlink_t;
typedef long __off_t;
typedef long __off64_t;
typedef int __pid_t;
typedef struct {
    int __val[2];
} __fsid_t;
typedef long __clock_t;
typedef unsigned long __rlim_t;
typedef unsigned long __rlim64_t;
typedef unsigned int __id_t;
typedef long __time_t;
typedef unsigned int __useconds_t;
typedef long __suseconds_t;
typedef int __daddr_t;
typedef int __key_t;
typedef int __clockid_t;
typedef void *__timer_t;
typedef long __blksize_t;
typedef long __blkcnt_t;
typedef long __blkcnt64_t;
typedef unsigned long __fsblkcnt_t;
typedef unsigned long __fsblkcnt64_t;
typedef unsigned long __fsfilcnt_t;
typedef unsigned long __fsfilcnt64_t;
typedef long __fsword_t;
typedef long __ssize_t;
typedef long __syscall_slong_t;
typedef unsigned long __syscall_ulong_t;
typedef __off64_t __loff_t;
typedef char *__caddr_t;
typedef long __intptr_t;
typedef unsigned int __socklen_t;
typedef int __sig_atomic_t;
typedef __int8_t int8_t;
typedef __int16_t int16_t;
typedef __int32_t int32_t;
typedef __int64_t int64_t;
typedef __uint8_t uint8_t;
typedef __uint16_t uint16_t;
typedef __uint32_t uint32_t;
typedef __uint64_t uint64_t;
typedef __int_least8_t int_least8_t;
typedef __int_least16_t int_least16_t;
typedef __int_least32_t int_least32_t;
typedef __int_least64_t int_least64_t;
typedef __uint_least8_t uint_least8_t;
typedef __uint_least16_t uint_least16_t;
typedef __uint_least32_t uint_least32_t;
typedef __uint_least64_t uint_least64_t;
typedef signed char int_fast8_t;
typedef long int_fast16_t;
typedef long int_fast32_t;
typedef long int_fast64_t;
typedef unsigned char uint_fast8_t;
typedef unsigned long uint_fast16_t;
typedef unsigned long uint_fast32_t;
typedef unsigned long uint_fast64_t;
typedef long intptr_t;
typedef unsigned long uintptr_t;
typedef __intmax_t intmax_t;
typedef __uintmax_t uintmax_t;
typedef _Bool bool;
struct helper_divq_EAX_wrapper_ret_type;
struct type_5;
struct indirect_placeholder_55_ret_type;
struct indirect_placeholder_56_ret_type;
struct indirect_placeholder_57_ret_type;
struct helper_divq_EAX_wrapper_ret_type {
    uint64_t field_0;
    uint64_t field_1;
    uint32_t field_2;
    uint64_t field_3;
    uint64_t field_4;
    uint32_t field_5;
    uint64_t field_6;
    uint32_t field_7;
    uint64_t field_8;
    uint32_t field_9;
    uint32_t field_10;
    unsigned char field_11;
    uint32_t field_12;
};
struct type_5 {
};
struct indirect_placeholder_55_ret_type {
    uint64_t field_0;
    uint64_t field_1;
    uint64_t field_2;
    uint64_t field_3;
};
struct indirect_placeholder_56_ret_type {
    uint64_t field_0;
    uint64_t field_1;
    uint64_t field_2;
    uint64_t field_3;
};
struct indirect_placeholder_57_ret_type {
    uint64_t field_0;
    uint64_t field_1;
};
extern void abort(void);
extern uint64_t revng_init_local_sp(uint64_t param_0);
extern uint64_t init_rbx(void);
extern uint64_t init_rbp(void);
extern uint64_t init_r13(void);
extern uint64_t init_r12(void);
extern uint64_t init_r14(void);
extern uint64_t init_cc_src2(void);
extern uint64_t helper_cc_compute_c_wrapper(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint32_t param_3);
extern uint64_t init_state_0x9018(void);
extern uint32_t init_state_0x9010(void);
extern uint64_t init_state_0x8408(void);
extern uint64_t init_state_0x8328(void);
extern uint64_t init_state_0x82d8(void);
extern uint32_t init_state_0x9080(void);
extern uint32_t init_state_0x8248(void);
extern struct helper_divq_EAX_wrapper_ret_type helper_divq_EAX_wrapper(struct type_5 *param_0, uint64_t param_1, uint64_t param_2, uint64_t param_3, uint64_t param_4, uint64_t param_5, uint64_t param_6, uint64_t param_7, uint64_t param_8, uint64_t param_9, uint64_t param_10, uint64_t param_11, uint64_t param_12, uint64_t param_13, uint32_t param_14, uint64_t param_15, uint64_t param_16, uint64_t param_17, uint32_t param_18, uint32_t param_19);
extern uint64_t indirect_placeholder_6(uint64_t param_0, uint64_t param_1);
extern void indirect_placeholder_7(uint64_t param_0, uint64_t param_1);
extern struct indirect_placeholder_55_ret_type indirect_placeholder_55(uint64_t param_0, uint64_t param_1);
extern struct indirect_placeholder_56_ret_type indirect_placeholder_56(uint64_t param_0, uint64_t param_1);
extern struct indirect_placeholder_57_ret_type indirect_placeholder_57(uint64_t param_0);
void bb_calculate_columns(uint64_t r9, uint64_t rdi) {
    uint64_t var_0;
    uint64_t var_1;
    uint64_t var_2;
    uint64_t var_3;
    uint64_t var_4;
    uint64_t var_5;
    uint64_t var_6;
    uint64_t var_7;
    uint32_t var_8;
    uint64_t var_9;
    uint64_t var_10;
    uint64_t var_11;
    uint32_t var_12;
    uint32_t var_13;
    uint64_t var_14;
    uint64_t var_15;
    uint64_t var_16;
    uint64_t var_17;
    uint64_t r8_4;
    uint64_t var_55;
    uint64_t var_56;
    uint64_t var_57;
    uint64_t var_58;
    uint64_t state_0x9018_2;
    uint64_t rbp_1;
    uint64_t r10_0;
    uint64_t rcx_0;
    uint64_t rsi_0;
    uint32_t state_0x9010_5;
    uint32_t state_0x9010_2;
    uint64_t state_0x9018_0;
    uint64_t local_sp_3;
    uint64_t state_0x82d8_2;
    uint32_t state_0x9010_0;
    uint32_t state_0x8248_1;
    uint64_t storemerge2;
    uint32_t state_0x9080_5;
    uint32_t state_0x9080_2;
    uint64_t state_0x82d8_0;
    uint32_t state_0x8248_5;
    uint32_t state_0x8248_2;
    uint32_t state_0x9080_0;
    uint32_t state_0x8248_0;
    unsigned char *var_59;
    unsigned char var_60;
    uint64_t var_61;
    uint64_t r10_1;
    uint64_t state_0x9018_5;
    struct helper_divq_EAX_wrapper_ret_type var_65;
    uint64_t state_0x9018_1;
    uint64_t var_63;
    struct helper_divq_EAX_wrapper_ret_type var_62;
    struct helper_divq_EAX_wrapper_ret_type var_64;
    uint32_t state_0x9010_1;
    uint64_t state_0x82d8_1;
    uint32_t state_0x9080_1;
    uint64_t rdx_0;
    uint64_t var_66;
    uint64_t *_cast1;
    uint64_t var_67;
    uint64_t var_68;
    uint64_t *var_69;
    uint64_t var_70;
    uint64_t state_0x9018_3;
    uint32_t state_0x9010_3;
    uint64_t state_0x82d8_3;
    uint32_t state_0x9080_3;
    uint32_t state_0x8248_3;
    uint64_t var_71;
    uint64_t var_72;
    uint64_t r8_2;
    uint64_t var_43;
    uint64_t rcx_1;
    uint64_t rdx_0_in;
    uint64_t rax_0;
    uint64_t var_44;
    uint64_t r10_2;
    uint64_t r91_0;
    uint64_t rbp_0;
    uint64_t rax_1;
    uint64_t local_sp_0;
    uint64_t var_28;
    uint64_t var_29;
    uint64_t var_30;
    uint64_t var_31;
    uint64_t r91_1;
    uint64_t var_33;
    uint64_t var_34;
    uint32_t var_35;
    struct helper_divq_EAX_wrapper_ret_type var_32;
    uint64_t var_36;
    uint32_t var_37;
    uint32_t var_38;
    uint64_t var_39;
    uint64_t var_40;
    uint64_t var_41;
    uint64_t var_42;
    uint64_t var_18;
    uint64_t var_19;
    uint64_t var_20;
    uint64_t var_25;
    uint64_t var_26;
    struct indirect_placeholder_55_ret_type var_27;
    uint64_t var_21;
    uint64_t var_22;
    uint64_t var_23;
    struct indirect_placeholder_56_ret_type var_24;
    uint64_t r8_3;
    uint64_t local_sp_1;
    uint64_t state_0x9018_4;
    uint32_t state_0x9010_4;
    uint64_t local_sp_2;
    uint64_t state_0x82d8_4;
    uint32_t state_0x9080_4;
    uint32_t state_0x8248_4;
    bool var_45;
    uint64_t rsi_1;
    uint64_t rax_2;
    uint64_t var_46;
    uint64_t var_47;
    uint64_t rdx_1;
    uint64_t var_48;
    uint64_t var_73;
    uint64_t state_0x82d8_5;
    uint64_t rbx_0;
    uint64_t rax_3;
    bool var_49;
    uint64_t var_50;
    uint64_t var_51;
    struct indirect_placeholder_57_ret_type var_52;
    uint64_t var_53;
    uint64_t var_54;
    var_0 = revng_init_local_sp(0UL);
    var_1 = init_rbx();
    var_2 = init_rbp();
    var_3 = init_r13();
    var_4 = init_cc_src2();
    var_5 = init_r12();
    var_6 = init_r14();
    var_7 = init_state_0x9018();
    var_8 = init_state_0x9010();
    var_9 = init_state_0x8408();
    var_10 = init_state_0x8328();
    var_11 = init_state_0x82d8();
    var_12 = init_state_0x9080();
    var_13 = init_state_0x8248();
    var_14 = *(uint64_t *)6474640UL;
    var_15 = *(uint64_t *)6474208UL;
    *(uint64_t *)(var_0 + (-8L)) = var_6;
    *(uint64_t *)(var_0 + (-16L)) = var_3;
    *(uint64_t *)(var_0 + (-24L)) = var_5;
    *(uint64_t *)(var_0 + (-32L)) = var_2;
    var_16 = var_0 + (-40L);
    *(uint64_t *)var_16 = var_1;
    var_17 = (var_15 > var_14) ? var_14 : var_15;
    r8_4 = var_14;
    rbp_1 = 0UL;
    rcx_0 = 1UL;
    r91_1 = r9;
    r8_3 = var_14;
    local_sp_1 = var_16;
    state_0x9018_4 = var_7;
    state_0x9010_4 = var_8;
    local_sp_2 = var_16;
    state_0x82d8_4 = var_11;
    state_0x9080_4 = var_12;
    state_0x8248_4 = var_13;
    rax_2 = 0UL;
    rdx_1 = 0UL;
    rbx_0 = var_17;
    if (var_17 <= *(uint64_t *)6470208UL) {
        var_18 = *(uint64_t *)6474216UL;
        var_19 = var_15 >> 1UL;
        var_20 = helper_cc_compute_c_wrapper(var_17 - var_19, var_19, var_4, 17U);
        if (var_20 == 0UL) {
            if (var_15 <= 384307168202282325UL) {
                *(uint64_t *)(local_sp_1 + (-8L)) = 4223949UL;
                indirect_placeholder_7(r91_1, r8_3);
                abort();
            }
            var_25 = var_15 * 24UL;
            var_26 = var_0 + (-48L);
            *(uint64_t *)var_26 = 4223350UL;
            var_27 = indirect_placeholder_55(var_18, var_25);
            r8_2 = var_27.field_3;
            r10_2 = var_27.field_1;
            r91_0 = var_27.field_2;
            rbp_0 = *(uint64_t *)6474208UL;
            rax_1 = var_27.field_0;
            local_sp_0 = var_26;
        } else {
            if (var_17 <= 192153584101141162UL) {
                *(uint64_t *)(local_sp_1 + (-8L)) = 4223949UL;
                indirect_placeholder_7(r91_1, r8_3);
                abort();
            }
            var_21 = var_17 << 1UL;
            var_22 = var_17 * 48UL;
            var_23 = var_0 + (-48L);
            *(uint64_t *)var_23 = 4223921UL;
            var_24 = indirect_placeholder_56(var_18, var_22);
            r8_2 = var_24.field_3;
            r10_2 = var_24.field_1;
            r91_0 = var_24.field_2;
            rbp_0 = var_21;
            rax_1 = var_24.field_0;
            local_sp_0 = var_23;
        }
        *(uint64_t *)6474216UL = rax_1;
        var_28 = *(uint64_t *)6470208UL;
        var_29 = (var_28 + rbp_0) + 1UL;
        var_30 = rbp_0 - var_28;
        var_31 = var_30 * var_29;
        r91_1 = r91_0;
        r8_3 = r8_2;
        local_sp_1 = local_sp_0;
        if (rbp_0 > var_29) {
            *(uint64_t *)(local_sp_1 + (-8L)) = 4223949UL;
            indirect_placeholder_7(r91_1, r8_3);
            abort();
        }
        var_32 = helper_divq_EAX_wrapper((struct type_5 *)(0UL), var_30, 4223403UL, var_31, var_31, r10_2, var_17, r91_0, var_30, rbp_0, r8_2, 0UL, var_29, var_7, var_8, var_9, var_10, var_11, var_12, var_13);
        var_33 = var_32.field_4;
        var_34 = var_32.field_6;
        var_35 = var_32.field_7;
        var_36 = var_32.field_8;
        var_37 = var_32.field_9;
        var_38 = var_32.field_10;
        state_0x9018_4 = var_34;
        state_0x9010_4 = var_35;
        state_0x82d8_4 = var_36;
        state_0x9080_4 = var_37;
        state_0x8248_4 = var_38;
        if ((var_29 != var_32.field_1) || (var_31 > 2305843009213693951UL)) {
            *(uint64_t *)(local_sp_1 + (-8L)) = 4223949UL;
            indirect_placeholder_7(r91_1, r8_3);
            abort();
        }
        var_39 = (var_31 >> 1UL) << 3UL;
        var_40 = local_sp_0 + (-8L);
        *(uint64_t *)var_40 = 4223450UL;
        var_41 = indirect_placeholder_6(var_39, var_33);
        var_42 = *(uint64_t *)6470208UL;
        rax_0 = var_41;
        local_sp_2 = var_40;
        if (rbp_0 <= var_42) {
            var_43 = rbp_0 << 3UL;
            rcx_1 = (var_42 * 24UL) + *(uint64_t *)6474216UL;
            rdx_0_in = var_42 << 3UL;
            rdx_0 = rdx_0_in + 8UL;
            *(uint64_t *)(rcx_1 + 16UL) = rax_0;
            rdx_0_in = rdx_0;
            while (rdx_0 != var_43)
                {
                    rcx_1 = rcx_1 + 24UL;
                    rax_0 = rax_0 + rdx_0;
                    rdx_0 = rdx_0_in + 8UL;
                    *(uint64_t *)(rcx_1 + 16UL) = rax_0;
                    rdx_0_in = rdx_0;
                }
        }
        var_44 = *(uint64_t *)6474640UL;
        *(uint64_t *)6470208UL = rbp_0;
        r8_4 = var_44;
    }
    var_45 = (var_17 == 0UL);
    state_0x9010_5 = state_0x9010_4;
    local_sp_3 = local_sp_2;
    state_0x9080_5 = state_0x9080_4;
    state_0x8248_5 = state_0x8248_4;
    state_0x9018_5 = state_0x9018_4;
    state_0x82d8_5 = state_0x82d8_4;
    if (var_45) {
        if (r8_4 == 0UL) {
            return;
        }
    }
    rsi_1 = *(uint64_t *)6474216UL;
    while (1U)
        {
            var_46 = rax_2 + 1UL;
            var_47 = *(uint64_t *)(rsi_1 + 16UL);
            *(unsigned char *)rsi_1 = (unsigned char)'\x01';
            *(uint64_t *)(rsi_1 + 8UL) = (var_46 * 3UL);
            rax_2 = var_46;
            *(uint64_t *)((rdx_1 << 3UL) + var_47) = 3UL;
            var_48 = rdx_1 + 1UL;
            rdx_1 = var_48;
            do {
                *(uint64_t *)((rdx_1 << 3UL) + var_47) = 3UL;
                var_48 = rdx_1 + 1UL;
                rdx_1 = var_48;
            } while (var_48 <= rax_2);
            if (var_46 == var_17) {
                break;
            }
            rsi_1 = rsi_1 + 24UL;
            continue;
        }
    if (r8_4 != 0UL) {
        var_73 = (var_17 * 24UL) + *(uint64_t *)6474216UL;
        rax_3 = var_73;
        if (var_17 <= 1UL & *(unsigned char *)(var_73 + (-24L)) == '\x00') {
            rbx_0 = rbx_0 + (-1L);
            while (rbx_0 != 2UL)
                {
                    if (*(unsigned char *)(rax_3 + (-48L)) == '\x00') {
                        break;
                    }
                    rax_3 = rax_3 + (-24L);
                    rbx_0 = rbx_0 + (-1L);
                }
        }
        return;
    }
    var_49 = ((uint64_t)(unsigned char)rdi == 0UL);
    var_71 = rbp_1 + 1UL;
    var_72 = helper_cc_compute_c_wrapper(var_71 - var_54, var_54, var_4, 17U);
    rbp_1 = var_71;
    state_0x9010_5 = state_0x9010_3;
    state_0x9080_5 = state_0x9080_3;
    state_0x8248_5 = state_0x8248_3;
    state_0x9018_5 = state_0x9018_3;
    state_0x82d8_5 = state_0x82d8_3;
    do {
        var_50 = *(uint64_t *)((rbp_1 << 3UL) + *(uint64_t *)6474624UL);
        var_51 = local_sp_3 + (-8L);
        *(uint64_t *)var_51 = 4223632UL;
        var_52 = indirect_placeholder_57(var_50);
        var_53 = var_52.field_0;
        var_54 = *(uint64_t *)6474640UL;
        state_0x9018_0 = state_0x9018_5;
        local_sp_3 = var_51;
        state_0x9010_0 = state_0x9010_5;
        state_0x82d8_0 = state_0x82d8_5;
        state_0x9080_0 = state_0x9080_5;
        state_0x8248_0 = state_0x8248_5;
        state_0x9018_3 = state_0x9018_5;
        state_0x9010_3 = state_0x9010_5;
        state_0x82d8_3 = state_0x82d8_5;
        state_0x9080_3 = state_0x9080_5;
        state_0x8248_3 = state_0x8248_5;
        if (!var_45) {
            var_55 = var_52.field_1;
            var_56 = *(uint64_t *)6474216UL;
            var_57 = *(uint64_t *)6474384UL;
            var_58 = var_54 + (-1L);
            r10_0 = var_55;
            rsi_0 = var_56 + 8UL;
            while (1U)
                {
                    var_59 = (unsigned char *)(rsi_0 + (-8L));
                    var_60 = *var_59;
                    var_61 = rcx_0 + (-1L);
                    state_0x9018_2 = state_0x9018_0;
                    state_0x9010_2 = state_0x9010_0;
                    state_0x82d8_2 = state_0x82d8_0;
                    state_0x9080_2 = state_0x9080_0;
                    state_0x8248_2 = state_0x8248_0;
                    r10_1 = r10_0;
                    if (var_60 != '\x00') {
                        if (var_49) {
                            var_65 = helper_divq_EAX_wrapper((struct type_5 *)(0UL), rcx_0, 4223792UL, rbp_1, rcx_0, r10_0, var_17, var_61, var_58, rbp_1, rcx_0, 0UL, rsi_0, state_0x9018_0, state_0x9010_0, var_9, var_10, state_0x82d8_0, state_0x9080_0, state_0x8248_0);
                            state_0x8248_1 = var_65.field_10;
                            storemerge2 = var_65.field_4;
                            state_0x9018_1 = var_65.field_6;
                            state_0x9010_1 = var_65.field_7;
                            state_0x82d8_1 = var_65.field_8;
                            state_0x9080_1 = var_65.field_9;
                        } else {
                            var_62 = helper_divq_EAX_wrapper((struct type_5 *)(0UL), rcx_0, 4223686UL, rcx_0 + var_58, rcx_0, r10_0, var_17, var_61, var_58, rbp_1, rcx_0, 0UL, rsi_0, state_0x9018_0, state_0x9010_0, var_9, var_10, state_0x82d8_0, state_0x9080_0, state_0x8248_0);
                            var_63 = var_62.field_1;
                            var_64 = helper_divq_EAX_wrapper((struct type_5 *)(0UL), var_63, 4223697UL, rbp_1, rcx_0, r10_0, var_17, var_61, var_58, rbp_1, var_63, 0UL, rsi_0, var_62.field_6, var_62.field_7, var_9, var_10, var_62.field_8, var_62.field_9, var_62.field_10);
                            state_0x8248_1 = var_64.field_10;
                            storemerge2 = var_64.field_1;
                            state_0x9018_1 = var_64.field_6;
                            state_0x9010_1 = var_64.field_7;
                            state_0x82d8_1 = var_64.field_8;
                            state_0x9080_1 = var_64.field_9;
                        }
                        var_66 = ((var_61 == storemerge2) ? 0UL : 2UL) + var_53;
                        _cast1 = (uint64_t *)((storemerge2 << 3UL) + *(uint64_t *)(rsi_0 + 8UL));
                        var_67 = *_cast1;
                        state_0x9018_2 = state_0x9018_1;
                        state_0x9010_2 = state_0x9010_1;
                        state_0x82d8_2 = state_0x82d8_1;
                        state_0x9080_2 = state_0x9080_1;
                        state_0x8248_2 = state_0x8248_1;
                        r10_1 = storemerge2;
                        if (var_66 > var_67) {
                            var_68 = var_66 - var_67;
                            var_69 = (uint64_t *)rsi_0;
                            *var_69 = (*var_69 + var_68);
                            *_cast1 = var_66;
                            var_70 = helper_cc_compute_c_wrapper(*var_69 - var_57, var_57, var_4, 17U);
                            *var_59 = (unsigned char)var_70;
                            r10_1 = var_68;
                        }
                    }
                    r10_0 = r10_1;
                    state_0x9018_0 = state_0x9018_2;
                    state_0x9010_0 = state_0x9010_2;
                    state_0x82d8_0 = state_0x82d8_2;
                    state_0x9080_0 = state_0x9080_2;
                    state_0x8248_0 = state_0x8248_2;
                    state_0x9018_3 = state_0x9018_2;
                    state_0x9010_3 = state_0x9010_2;
                    state_0x82d8_3 = state_0x82d8_2;
                    state_0x9080_3 = state_0x9080_2;
                    state_0x8248_3 = state_0x8248_2;
                    if (var_17 > rcx_0) {
                        break;
                    }
                    rcx_0 = rcx_0 + 1UL;
                    rsi_0 = rsi_0 + 24UL;
                    continue;
                }
        }
        var_71 = rbp_1 + 1UL;
        var_72 = helper_cc_compute_c_wrapper(var_71 - var_54, var_54, var_4, 17U);
        rbp_1 = var_71;
        state_0x9010_5 = state_0x9010_3;
        state_0x9080_5 = state_0x9080_3;
        state_0x8248_5 = state_0x8248_3;
        state_0x9018_5 = state_0x9018_3;
        state_0x82d8_5 = state_0x82d8_3;
    } while (var_72 != 0UL);
    var_73 = (var_17 * 24UL) + *(uint64_t *)6474216UL;
    rax_3 = var_73;
    if (~(var_17 <= 1UL & *(unsigned char *)(var_73 + (-24L)) == '\x00')) {
        return;
    }
    rbx_0 = rbx_0 + (-1L);
    while (rbx_0 != 2UL)
        {
            if (*(unsigned char *)(rax_3 + (-48L)) == '\x00') {
                break;
            }
            rax_3 = rax_3 + (-24L);
            rbx_0 = rbx_0 + (-1L);
        }
    return;
}

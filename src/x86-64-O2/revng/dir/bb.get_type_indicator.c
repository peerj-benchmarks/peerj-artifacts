typedef unsigned char __u_char;
typedef unsigned short __u_short;
typedef unsigned int __u_int;
typedef unsigned long __u_long;
typedef signed char __int8_t;
typedef unsigned char __uint8_t;
typedef short __int16_t;
typedef unsigned short __uint16_t;
typedef int __int32_t;
typedef unsigned int __uint32_t;
typedef long __int64_t;
typedef unsigned long __uint64_t;
typedef __int8_t __int_least8_t;
typedef __uint8_t __uint_least8_t;
typedef __int16_t __int_least16_t;
typedef __uint16_t __uint_least16_t;
typedef __int32_t __int_least32_t;
typedef __uint32_t __uint_least32_t;
typedef __int64_t __int_least64_t;
typedef __uint64_t __uint_least64_t;
typedef long __quad_t;
typedef unsigned long __u_quad_t;
typedef long __intmax_t;
typedef unsigned long __uintmax_t;
typedef unsigned long __dev_t;
typedef unsigned int __uid_t;
typedef unsigned int __gid_t;
typedef unsigned long __ino_t;
typedef unsigned long __ino64_t;
typedef unsigned int __mode_t;
typedef unsigned long __nlink_t;
typedef long __off_t;
typedef long __off64_t;
typedef int __pid_t;
typedef struct {
    int __val[2];
} __fsid_t;
typedef long __clock_t;
typedef unsigned long __rlim_t;
typedef unsigned long __rlim64_t;
typedef unsigned int __id_t;
typedef long __time_t;
typedef unsigned int __useconds_t;
typedef long __suseconds_t;
typedef int __daddr_t;
typedef int __key_t;
typedef int __clockid_t;
typedef void *__timer_t;
typedef long __blksize_t;
typedef long __blkcnt_t;
typedef long __blkcnt64_t;
typedef unsigned long __fsblkcnt_t;
typedef unsigned long __fsblkcnt64_t;
typedef unsigned long __fsfilcnt_t;
typedef unsigned long __fsfilcnt64_t;
typedef long __fsword_t;
typedef long __ssize_t;
typedef long __syscall_slong_t;
typedef unsigned long __syscall_ulong_t;
typedef __off64_t __loff_t;
typedef char *__caddr_t;
typedef long __intptr_t;
typedef unsigned int __socklen_t;
typedef int __sig_atomic_t;
typedef __int8_t int8_t;
typedef __int16_t int16_t;
typedef __int32_t int32_t;
typedef __int64_t int64_t;
typedef __uint8_t uint8_t;
typedef __uint16_t uint16_t;
typedef __uint32_t uint32_t;
typedef __uint64_t uint64_t;
typedef __int_least8_t int_least8_t;
typedef __int_least16_t int_least16_t;
typedef __int_least32_t int_least32_t;
typedef __int_least64_t int_least64_t;
typedef __uint_least8_t uint_least8_t;
typedef __uint_least16_t uint_least16_t;
typedef __uint_least32_t uint_least32_t;
typedef __uint_least64_t uint_least64_t;
typedef signed char int_fast8_t;
typedef long int_fast16_t;
typedef long int_fast32_t;
typedef long int_fast64_t;
typedef unsigned char uint_fast8_t;
typedef unsigned long uint_fast16_t;
typedef unsigned long uint_fast32_t;
typedef unsigned long uint_fast64_t;
typedef long intptr_t;
typedef unsigned long uintptr_t;
typedef __intmax_t intmax_t;
typedef __uintmax_t uintmax_t;
typedef _Bool bool;
extern uint64_t revng_init_local_sp(uint64_t param_0);
extern uint64_t init_cc_src2(void);
extern uint64_t helper_cc_compute_c_wrapper(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint32_t param_3);
extern uint64_t init_rcx(void);
uint64_t bb_get_type_indicator(uint64_t rdi, uint64_t rdx, uint64_t rsi) {
    uint64_t var_0;
    uint64_t var_1;
    bool var_2;
    uint32_t var_3;
    uint64_t merge;
    uint64_t var_4;
    uint64_t storemerge1;
    uint32_t var_5;
    uint32_t var_6;
    uint64_t storemerge_in_in;
    uint32_t var_7;
    revng_init_local_sp(0UL);
    var_0 = init_rcx();
    var_1 = init_cc_src2();
    var_2 = ((uint64_t)(unsigned char)rdi == 0UL);
    merge = 0UL;
    if (var_2) {
        var_5 = (uint32_t)rdx;
        if ((uint64_t)(var_5 + (-5)) != 0UL) {
            return merge;
        }
        storemerge1 = ((uint64_t)((uint32_t)var_0 & (-256)) | ((uint64_t)(var_5 + (-9)) == 0UL)) | ((uint64_t)(var_5 + (-3)) == 0UL);
    } else {
        var_3 = (uint32_t)(uint64_t)((uint16_t)rsi & (unsigned short)61440U);
        if ((uint64_t)((var_3 + (-32768)) & (-4096)) != 0UL) {
            if (*(uint32_t *)6474484UL == 3U) {
                return merge;
            }
            var_4 = helper_cc_compute_c_wrapper((rsi & 73UL) + (-1L), 1UL, var_1, 16U);
            return ((0UL - var_4) & 42UL) ^ 42UL;
        }
        storemerge1 = (var_0 & (-256L)) | ((uint64_t)((var_3 + (-16384)) & (-4096)) == 0UL);
    }
    merge = 47UL;
    if ((uint64_t)(unsigned char)storemerge1 == 0UL) {
        return merge;
    }
    merge = 0UL;
    if (*(uint32_t *)6474484UL == 1U) {
        return;
    }
    merge = 64UL;
    if (var_2) {
        var_7 = (uint32_t)rdx;
        if ((uint64_t)(var_7 + (-6)) == 0UL) {
            return merge;
        }
        merge = 124UL;
        if ((uint64_t)(var_7 + (-1)) != 0UL) {
            return merge;
        }
        storemerge_in_in = (uint64_t)(var_7 + (-7));
    } else {
        var_6 = (uint32_t)(uint64_t)((uint16_t)rsi & (unsigned short)61440U);
        if ((uint64_t)((var_6 + (-40960)) & (-4096)) == 0UL) {
            return merge;
        }
        merge = 124UL;
        if ((uint64_t)((var_6 + (-4096)) & (-4096)) != 0UL) {
            return merge;
        }
        storemerge_in_in = (uint64_t)((var_6 + (-49152)) & (-4096));
    }
    return (storemerge_in_in == 0UL) ? 61UL : 0UL;
}

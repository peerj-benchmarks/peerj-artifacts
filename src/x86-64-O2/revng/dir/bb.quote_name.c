typedef unsigned char __u_char;
typedef unsigned short __u_short;
typedef unsigned int __u_int;
typedef unsigned long __u_long;
typedef signed char __int8_t;
typedef unsigned char __uint8_t;
typedef short __int16_t;
typedef unsigned short __uint16_t;
typedef int __int32_t;
typedef unsigned int __uint32_t;
typedef long __int64_t;
typedef unsigned long __uint64_t;
typedef __int8_t __int_least8_t;
typedef __uint8_t __uint_least8_t;
typedef __int16_t __int_least16_t;
typedef __uint16_t __uint_least16_t;
typedef __int32_t __int_least32_t;
typedef __uint32_t __uint_least32_t;
typedef __int64_t __int_least64_t;
typedef __uint64_t __uint_least64_t;
typedef long __quad_t;
typedef unsigned long __u_quad_t;
typedef long __intmax_t;
typedef unsigned long __uintmax_t;
typedef unsigned long __dev_t;
typedef unsigned int __uid_t;
typedef unsigned int __gid_t;
typedef unsigned long __ino_t;
typedef unsigned long __ino64_t;
typedef unsigned int __mode_t;
typedef unsigned long __nlink_t;
typedef long __off_t;
typedef long __off64_t;
typedef int __pid_t;
typedef struct {
    int __val[2];
} __fsid_t;
typedef long __clock_t;
typedef unsigned long __rlim_t;
typedef unsigned long __rlim64_t;
typedef unsigned int __id_t;
typedef long __time_t;
typedef unsigned int __useconds_t;
typedef long __suseconds_t;
typedef int __daddr_t;
typedef int __key_t;
typedef int __clockid_t;
typedef void *__timer_t;
typedef long __blksize_t;
typedef long __blkcnt_t;
typedef long __blkcnt64_t;
typedef unsigned long __fsblkcnt_t;
typedef unsigned long __fsblkcnt64_t;
typedef unsigned long __fsfilcnt_t;
typedef unsigned long __fsfilcnt64_t;
typedef long __fsword_t;
typedef long __ssize_t;
typedef long __syscall_slong_t;
typedef unsigned long __syscall_ulong_t;
typedef __off64_t __loff_t;
typedef char *__caddr_t;
typedef long __intptr_t;
typedef unsigned int __socklen_t;
typedef int __sig_atomic_t;
typedef __int8_t int8_t;
typedef __int16_t int16_t;
typedef __int32_t int32_t;
typedef __int64_t int64_t;
typedef __uint8_t uint8_t;
typedef __uint16_t uint16_t;
typedef __uint32_t uint32_t;
typedef __uint64_t uint64_t;
typedef __int_least8_t int_least8_t;
typedef __int_least16_t int_least16_t;
typedef __int_least32_t int_least32_t;
typedef __int_least64_t int_least64_t;
typedef __uint_least8_t uint_least8_t;
typedef __uint_least16_t uint_least16_t;
typedef __uint_least32_t uint_least32_t;
typedef __uint_least64_t uint_least64_t;
typedef signed char int_fast8_t;
typedef long int_fast16_t;
typedef long int_fast32_t;
typedef long int_fast64_t;
typedef unsigned char uint_fast8_t;
typedef unsigned long uint_fast16_t;
typedef unsigned long uint_fast32_t;
typedef unsigned long uint_fast64_t;
typedef long intptr_t;
typedef unsigned long uintptr_t;
typedef __intmax_t intmax_t;
typedef __uintmax_t uintmax_t;
extern uint64_t revng_init_local_sp(uint64_t param_0);
extern void indirect_placeholder(void);
extern uint64_t init_rbx(void);
extern uint64_t indirect_placeholder_1(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint64_t param_3, uint64_t param_4, uint64_t param_5);
extern uint64_t init_rbp(void);
extern uint64_t init_r13(void);
extern uint64_t init_r12(void);
extern uint64_t init_r14(void);
extern uint64_t indirect_placeholder_5(uint64_t param_0);
extern uint64_t init_r15(void);
extern void indirect_placeholder_9(uint64_t param_0);
extern uint64_t indirect_placeholder_6(uint64_t param_0, uint64_t param_1);
extern void indirect_placeholder_7(uint64_t param_0, uint64_t param_1);
uint64_t bb_quote_name(uint64_t rcx, uint64_t r9, uint64_t rdi, uint64_t r8, uint64_t rdx, uint64_t rsi) {
    uint64_t var_0;
    uint64_t var_1;
    uint64_t var_2;
    uint64_t var_3;
    uint64_t var_4;
    uint64_t var_5;
    uint64_t var_6;
    uint64_t var_7;
    uint64_t var_8;
    uint64_t var_9;
    uint64_t var_10;
    uint64_t var_11;
    uint64_t var_12;
    uint64_t local_sp_5;
    uint64_t var_33;
    uint64_t local_sp_2;
    uint64_t *var_28;
    uint64_t local_sp_0;
    uint64_t var_30;
    uint64_t var_29;
    uint64_t local_sp_10;
    uint64_t local_sp_1;
    uint64_t var_32;
    uint64_t var_34;
    uint64_t local_sp_3;
    uint64_t var_35;
    uint64_t local_sp_4;
    uint64_t var_17;
    uint64_t local_sp_6;
    uint64_t var_16;
    uint64_t var_13;
    uint64_t var_14;
    uint64_t var_15;
    uint64_t var_18;
    uint64_t *var_19;
    uint64_t *_pre_phi;
    uint64_t local_sp_7;
    uint64_t var_20;
    uint64_t var_21;
    uint64_t var_22;
    uint64_t var_23;
    uint64_t local_sp_8;
    uint64_t *var_24;
    uint64_t local_sp_9;
    uint64_t var_26;
    uint64_t var_25;
    uint64_t var_27;
    uint64_t var_31;
    var_0 = revng_init_local_sp(0UL);
    var_1 = init_rbx();
    var_2 = init_rbp();
    var_3 = init_r13();
    var_4 = init_r12();
    var_5 = init_r14();
    var_6 = init_r15();
    *(uint64_t *)(var_0 + (-8L)) = var_6;
    *(uint64_t *)(var_0 + (-16L)) = var_5;
    *(uint64_t *)(var_0 + (-24L)) = var_3;
    *(uint64_t *)(var_0 + (-32L)) = var_4;
    *(uint64_t *)(var_0 + (-40L)) = var_2;
    *(uint64_t *)(var_0 + (-48L)) = var_1;
    var_7 = (uint64_t)(uint32_t)rdx;
    var_8 = var_0 + (-1080L);
    var_9 = var_0 + (-1089L);
    var_10 = var_0 + (-1088L);
    *(uint64_t *)var_10 = var_8;
    var_11 = var_0 + (-1136L);
    *(uint64_t *)var_11 = 4218593UL;
    var_12 = indirect_placeholder_1(var_7, var_9, var_10, 0UL, rsi, rdi);
    local_sp_5 = var_11;
    if ((uint64_t)(unsigned char)r8 != 0UL & *(unsigned char *)(var_0 + (-1097L)) == '\x00') {
        var_13 = var_0 + (-1144L);
        *(uint64_t *)var_13 = 4219138UL;
        indirect_placeholder();
        *(uint64_t *)6474200UL = (*(uint64_t *)6474200UL + 1UL);
        local_sp_5 = var_13;
    }
    local_sp_6 = local_sp_5;
    if (rcx != 0UL) {
        var_14 = local_sp_5 + (-8L);
        *(uint64_t *)var_14 = 4218627UL;
        var_15 = indirect_placeholder_5(4UL);
        local_sp_4 = var_14;
        if ((uint64_t)(unsigned char)var_15 != 0UL) {
            *(uint64_t *)(local_sp_5 + (-16L)) = 4219162UL;
            indirect_placeholder_9(6469440UL);
            var_16 = local_sp_5 + (-24L);
            *(uint64_t *)var_16 = 4219172UL;
            indirect_placeholder_9(6469456UL);
            local_sp_4 = var_16;
        }
        *(uint64_t *)(local_sp_4 + (-8L)) = 4218645UL;
        indirect_placeholder_9(6469440UL);
        *(uint64_t *)(local_sp_4 + (-16L)) = 4218653UL;
        indirect_placeholder_9(rcx);
        var_17 = local_sp_4 + (-24L);
        *(uint64_t *)var_17 = 4218663UL;
        indirect_placeholder_9(6469456UL);
        local_sp_6 = var_17;
    }
    local_sp_7 = local_sp_6;
    local_sp_8 = local_sp_6;
    if (*(uint64_t *)(local_sp_6 + 1136UL) == 0UL) {
        *(uint64_t *)(local_sp_6 + 8UL) = 0UL;
        *(unsigned char *)(local_sp_6 + 31UL) = (unsigned char)'\x00';
    } else {
        if (*(unsigned char *)6474632UL == '\x00') {
            *(uint64_t *)(local_sp_6 + 8UL) = 0UL;
            *(unsigned char *)(local_sp_6 + 31UL) = (unsigned char)'\x00';
            _pre_phi = (uint64_t *)local_sp_6;
        } else {
            if (*(unsigned char *)6474633UL == '\x00') {
                *(uint64_t *)(local_sp_6 + 8UL) = 0UL;
                *(unsigned char *)(local_sp_6 + 31UL) = (unsigned char)'\x00';
                _pre_phi = (uint64_t *)local_sp_6;
            } else {
                if (*(unsigned char *)(local_sp_6 + 39UL) == '\x00') {
                    var_18 = local_sp_6 + (-8L);
                    var_19 = (uint64_t *)var_18;
                    *var_19 = 4219083UL;
                    indirect_placeholder();
                    *(uint64_t *)local_sp_6 = 2UL;
                    *(unsigned char *)(local_sp_6 + 23UL) = (unsigned char)'\x01';
                    _pre_phi = var_19;
                    local_sp_7 = var_18;
                } else {
                    *(uint64_t *)(local_sp_6 + 8UL) = 0UL;
                    *(unsigned char *)(local_sp_6 + 31UL) = (unsigned char)'\x00';
                    _pre_phi = (uint64_t *)local_sp_6;
                }
            }
        }
        var_20 = *(uint64_t *)6474600UL;
        *(uint64_t *)(local_sp_7 + (-8L)) = 4218722UL;
        indirect_placeholder_6(var_20, 0UL);
        var_21 = *(uint64_t *)(local_sp_7 + 1128UL);
        *(uint64_t *)(local_sp_7 + (-16L)) = 4218743UL;
        var_22 = indirect_placeholder_6(var_21, 1UL);
        *_pre_phi = var_22;
        *(uint64_t *)(local_sp_7 + (-24L)) = 4218786UL;
        indirect_placeholder();
        *(uint64_t *)(local_sp_7 + (-32L)) = 4218794UL;
        indirect_placeholder();
        var_23 = local_sp_7 + (-40L);
        *(uint64_t *)var_23 = 4218807UL;
        indirect_placeholder();
        local_sp_8 = var_23;
    }
    local_sp_9 = local_sp_8;
    local_sp_10 = local_sp_8;
    if (r9 == 0UL) {
        var_31 = local_sp_8 + (-8L);
        *(uint64_t *)var_31 = 4219220UL;
        indirect_placeholder();
        *(uint64_t *)6474200UL = (*(uint64_t *)6474200UL + var_12);
        local_sp_1 = var_31;
    } else {
        if (*(unsigned char *)6474488UL != '\x00') {
            var_24 = (uint64_t *)(r9 + 24UL);
            if ((*(uint64_t *)(r9 + 32UL) - *var_24) <= 7UL) {
                var_25 = local_sp_8 + (-8L);
                *(uint64_t *)var_25 = 4219277UL;
                indirect_placeholder_7(r9, 8UL);
                local_sp_9 = var_25;
            }
            var_26 = local_sp_9 + (-8L);
            *(uint64_t *)var_26 = 4218861UL;
            indirect_placeholder();
            *var_24 = (*var_24 + 8UL);
            local_sp_10 = var_26;
        }
        var_27 = local_sp_10 + (-8L);
        *(uint64_t *)var_27 = 4218902UL;
        indirect_placeholder();
        *(uint64_t *)6474200UL = (*(uint64_t *)6474200UL + var_12);
        local_sp_0 = var_27;
        local_sp_1 = var_27;
        if (*(unsigned char *)6474488UL != '\x00') {
            var_28 = (uint64_t *)(r9 + 24UL);
            if ((*(uint64_t *)(r9 + 32UL) - *var_28) <= 7UL) {
                var_29 = local_sp_10 + (-16L);
                *(uint64_t *)var_29 = 4219309UL;
                indirect_placeholder_7(r9, 8UL);
                local_sp_0 = var_29;
            }
            var_30 = local_sp_0 + (-8L);
            *(uint64_t *)var_30 = 4218954UL;
            indirect_placeholder();
            *var_28 = (*var_28 + 8UL);
            local_sp_1 = var_30;
        }
    }
    local_sp_2 = local_sp_1;
    var_32 = local_sp_1 + (-8L);
    *(uint64_t *)var_32 = 4218983UL;
    indirect_placeholder();
    local_sp_2 = var_32;
    if (*(uint64_t *)(local_sp_1 + 1136UL) != 0UL & *(unsigned char *)(local_sp_1 + 23UL) == '\x00') {
        var_33 = local_sp_1 + (-16L);
        *(uint64_t *)var_33 = 4219119UL;
        indirect_placeholder();
        local_sp_2 = var_33;
    }
    var_34 = *(uint64_t *)(local_sp_2 + 40UL);
    local_sp_3 = local_sp_2;
    if (!!((var_34 == rdi) || (var_34 == var_8))) {
        return var_12 + (uint64_t)*(unsigned char *)(local_sp_3 + 39UL);
    }
    var_35 = local_sp_2 + (-8L);
    *(uint64_t *)var_35 = 4219010UL;
    indirect_placeholder();
    local_sp_3 = var_35;
    return var_12 + (uint64_t)*(unsigned char *)(local_sp_3 + 39UL);
}

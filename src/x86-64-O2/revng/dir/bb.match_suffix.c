typedef unsigned char __u_char;
typedef unsigned short __u_short;
typedef unsigned int __u_int;
typedef unsigned long __u_long;
typedef signed char __int8_t;
typedef unsigned char __uint8_t;
typedef short __int16_t;
typedef unsigned short __uint16_t;
typedef int __int32_t;
typedef unsigned int __uint32_t;
typedef long __int64_t;
typedef unsigned long __uint64_t;
typedef __int8_t __int_least8_t;
typedef __uint8_t __uint_least8_t;
typedef __int16_t __int_least16_t;
typedef __uint16_t __uint_least16_t;
typedef __int32_t __int_least32_t;
typedef __uint32_t __uint_least32_t;
typedef __int64_t __int_least64_t;
typedef __uint64_t __uint_least64_t;
typedef long __quad_t;
typedef unsigned long __u_quad_t;
typedef long __intmax_t;
typedef unsigned long __uintmax_t;
typedef unsigned long __dev_t;
typedef unsigned int __uid_t;
typedef unsigned int __gid_t;
typedef unsigned long __ino_t;
typedef unsigned long __ino64_t;
typedef unsigned int __mode_t;
typedef unsigned long __nlink_t;
typedef long __off_t;
typedef long __off64_t;
typedef int __pid_t;
typedef struct {
    int __val[2];
} __fsid_t;
typedef long __clock_t;
typedef unsigned long __rlim_t;
typedef unsigned long __rlim64_t;
typedef unsigned int __id_t;
typedef long __time_t;
typedef unsigned int __useconds_t;
typedef long __suseconds_t;
typedef int __daddr_t;
typedef int __key_t;
typedef int __clockid_t;
typedef void *__timer_t;
typedef long __blksize_t;
typedef long __blkcnt_t;
typedef long __blkcnt64_t;
typedef unsigned long __fsblkcnt_t;
typedef unsigned long __fsblkcnt64_t;
typedef unsigned long __fsfilcnt_t;
typedef unsigned long __fsfilcnt64_t;
typedef long __fsword_t;
typedef long __ssize_t;
typedef long __syscall_slong_t;
typedef unsigned long __syscall_ulong_t;
typedef __off64_t __loff_t;
typedef char *__caddr_t;
typedef long __intptr_t;
typedef unsigned int __socklen_t;
typedef int __sig_atomic_t;
typedef __int8_t int8_t;
typedef __int16_t int16_t;
typedef __int32_t int32_t;
typedef __int64_t int64_t;
typedef __uint8_t uint8_t;
typedef __uint16_t uint16_t;
typedef __uint32_t uint32_t;
typedef __uint64_t uint64_t;
typedef __int_least8_t int_least8_t;
typedef __int_least16_t int_least16_t;
typedef __int_least32_t int_least32_t;
typedef __int_least64_t int_least64_t;
typedef __uint_least8_t uint_least8_t;
typedef __uint_least16_t uint_least16_t;
typedef __uint_least32_t uint_least32_t;
typedef __uint_least64_t uint_least64_t;
typedef signed char int_fast8_t;
typedef long int_fast16_t;
typedef long int_fast32_t;
typedef long int_fast64_t;
typedef unsigned char uint_fast8_t;
typedef unsigned long uint_fast16_t;
typedef unsigned long uint_fast32_t;
typedef unsigned long uint_fast64_t;
typedef long intptr_t;
typedef unsigned long uintptr_t;
typedef __intmax_t intmax_t;
typedef __uintmax_t uintmax_t;
extern uint64_t revng_init_local_sp(uint64_t param_0);
typedef _Bool bool;
uint64_t bb_match_suffix(uint64_t rdi) {
    uint64_t rax_0;
    uint64_t *_cast;
    uint64_t var_0;
    unsigned char var_1;
    uint64_t rax_2;
    uint64_t rax_1;
    uint64_t rcx_1;
    uint64_t rcx_0_ph;
    uint64_t rax_0_ph;
    unsigned char rdx_0_ph_in;
    uint64_t rsi_0_ph;
    unsigned char rdx_0_in;
    uint64_t rsi_0;
    uint64_t var_3;
    uint64_t var_4;
    unsigned char var_5;
    uint32_t var_2;
    uint64_t var_6;
    unsigned char var_7;
    unsigned int loop_state_var;
    bool switch_state_var;
    revng_init_local_sp(0UL);
    _cast = (uint64_t *)rdi;
    var_0 = *_cast;
    var_1 = *(unsigned char *)var_0;
    rax_2 = 0UL;
    rcx_0_ph = 0UL;
    rax_0_ph = 0UL;
    rdx_0_ph_in = var_1;
    rsi_0_ph = var_0;
    if (var_1 == '\x00') {
        return rax_2;
    }
    while (1U)
        {
            rax_0 = rax_0_ph;
            rdx_0_in = rdx_0_ph_in;
            rsi_0 = rsi_0_ph;
            rcx_1 = rcx_0_ph;
            while (1U)
                {
                    rax_1 = rax_0;
                    if ((uint64_t)(unsigned char)rcx_0_ph != 0UL) {
                        loop_state_var = 0U;
                        break;
                    }
                    if ((uint64_t)(rdx_0_in + '\xd2') != 0UL) {
                        loop_state_var = 1U;
                        break;
                    }
                    if ((signed char)rdx_0_in > 'Z') {
                        if ((uint64_t)((rdx_0_in + '\x9f') & '\xfe') <= 25UL) {
                            loop_state_var = 3U;
                            break;
                        }
                    }
                    if ((signed char)rdx_0_in >= 'A') {
                        loop_state_var = 3U;
                        break;
                    }
                    if ((uint64_t)((rdx_0_in + '\xd0') & '\xfe') <= 9UL) {
                        loop_state_var = 3U;
                        break;
                    }
                    var_3 = ((uint64_t)(rdx_0_in + '\x82') == 0UL) ? rax_0 : 0UL;
                    var_4 = rsi_0 + 1UL;
                    *_cast = var_4;
                    var_5 = *(unsigned char *)var_4;
                    rax_0 = var_3;
                    rdx_0_in = var_5;
                    rsi_0 = var_4;
                    rax_2 = var_3;
                    if (var_5 == '\x00') {
                        continue;
                    }
                    loop_state_var = 2U;
                    break;
                }
            switch_state_var = 0;
            switch (loop_state_var) {
              case 2U:
                {
                    switch_state_var = 1;
                    break;
                }
                break;
              case 3U:
              case 1U:
              case 0U:
                {
                    switch_state_var = 0;
                    switch (loop_state_var) {
                      case 1U:
                        {
                            rcx_1 = 1UL;
                            rax_1 = (rax_0 == 0UL) ? rsi_0 : rax_0;
                        }
                        break;
                      case 3U:
                        {
                            var_6 = rsi_0 + 1UL;
                            *_cast = var_6;
                            var_7 = *(unsigned char *)var_6;
                            rax_2 = rax_1;
                            rcx_0_ph = rcx_1;
                            rax_0_ph = rax_1;
                            rdx_0_ph_in = var_7;
                            rsi_0_ph = var_6;
                            if (var_7 != '\x00') {
                                continue;
                            }
                            switch_state_var = 1;
                            break;
                        }
                        break;
                      case 0U:
                        {
                            var_2 = (uint32_t)rdx_0_in + (-65);
                            rcx_1 = 0UL;
                            if (var_2 > 57U) {
                                rax_1 = ((uint64_t)(rdx_0_in + '\x82') == 0UL) ? rax_0 : 0UL;
                            } else {
                                if (((1UL << (uint64_t)(var_2 & 63U)) & 288230371923853311UL) == 0UL) {
                                    rax_1 = ((uint64_t)(rdx_0_in + '\x82') == 0UL) ? rax_0 : 0UL;
                                }
                            }
                        }
                        break;
                    }
                    if (switch_state_var)
                        break;
                }
                break;
            }
            if (switch_state_var)
                break;
        }
    return rax_2;
}

typedef unsigned char __u_char;
typedef unsigned short __u_short;
typedef unsigned int __u_int;
typedef unsigned long __u_long;
typedef signed char __int8_t;
typedef unsigned char __uint8_t;
typedef short __int16_t;
typedef unsigned short __uint16_t;
typedef int __int32_t;
typedef unsigned int __uint32_t;
typedef long __int64_t;
typedef unsigned long __uint64_t;
typedef __int8_t __int_least8_t;
typedef __uint8_t __uint_least8_t;
typedef __int16_t __int_least16_t;
typedef __uint16_t __uint_least16_t;
typedef __int32_t __int_least32_t;
typedef __uint32_t __uint_least32_t;
typedef __int64_t __int_least64_t;
typedef __uint64_t __uint_least64_t;
typedef long __quad_t;
typedef unsigned long __u_quad_t;
typedef long __intmax_t;
typedef unsigned long __uintmax_t;
typedef unsigned long __dev_t;
typedef unsigned int __uid_t;
typedef unsigned int __gid_t;
typedef unsigned long __ino_t;
typedef unsigned long __ino64_t;
typedef unsigned int __mode_t;
typedef unsigned long __nlink_t;
typedef long __off_t;
typedef long __off64_t;
typedef int __pid_t;
typedef struct {
    int __val[2];
} __fsid_t;
typedef long __clock_t;
typedef unsigned long __rlim_t;
typedef unsigned long __rlim64_t;
typedef unsigned int __id_t;
typedef long __time_t;
typedef unsigned int __useconds_t;
typedef long __suseconds_t;
typedef int __daddr_t;
typedef int __key_t;
typedef int __clockid_t;
typedef void *__timer_t;
typedef long __blksize_t;
typedef long __blkcnt_t;
typedef long __blkcnt64_t;
typedef unsigned long __fsblkcnt_t;
typedef unsigned long __fsblkcnt64_t;
typedef unsigned long __fsfilcnt_t;
typedef unsigned long __fsfilcnt64_t;
typedef long __fsword_t;
typedef long __ssize_t;
typedef long __syscall_slong_t;
typedef unsigned long __syscall_ulong_t;
typedef __off64_t __loff_t;
typedef char *__caddr_t;
typedef long __intptr_t;
typedef unsigned int __socklen_t;
typedef int __sig_atomic_t;
typedef __int8_t int8_t;
typedef __int16_t int16_t;
typedef __int32_t int32_t;
typedef __int64_t int64_t;
typedef __uint8_t uint8_t;
typedef __uint16_t uint16_t;
typedef __uint32_t uint32_t;
typedef __uint64_t uint64_t;
typedef __int_least8_t int_least8_t;
typedef __int_least16_t int_least16_t;
typedef __int_least32_t int_least32_t;
typedef __int_least64_t int_least64_t;
typedef __uint_least8_t uint_least8_t;
typedef __uint_least16_t uint_least16_t;
typedef __uint_least32_t uint_least32_t;
typedef __uint_least64_t uint_least64_t;
typedef signed char int_fast8_t;
typedef long int_fast16_t;
typedef long int_fast32_t;
typedef long int_fast64_t;
typedef unsigned char uint_fast8_t;
typedef unsigned long uint_fast16_t;
typedef unsigned long uint_fast32_t;
typedef unsigned long uint_fast64_t;
typedef long intptr_t;
typedef unsigned long uintptr_t;
typedef __intmax_t intmax_t;
typedef __uintmax_t uintmax_t;
struct indirect_placeholder_39_ret_type;
struct indirect_placeholder_40_ret_type;
struct indirect_placeholder_41_ret_type;
struct indirect_placeholder_42_ret_type;
struct indirect_placeholder_39_ret_type {
    uint64_t field_0;
    uint64_t field_1;
    uint64_t field_2;
};
struct indirect_placeholder_40_ret_type {
    uint64_t field_0;
    uint64_t field_1;
    uint64_t field_2;
};
struct indirect_placeholder_41_ret_type {
    uint64_t field_0;
    uint64_t field_1;
    uint64_t field_2;
};
struct indirect_placeholder_42_ret_type {
    uint64_t field_0;
    uint64_t field_1;
};
extern void abort(void);
extern uint64_t revng_init_local_sp(uint64_t param_0);
extern uint64_t init_rbx(void);
extern uint64_t init_rbp(void);
extern void indirect_placeholder_2(void);
extern uint64_t init_cc_src2(void);
extern uint64_t init_r13(void);
extern uint64_t init_r12(void);
extern uint64_t init_r14(void);
extern uint64_t helper_cc_compute_all_wrapper(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint32_t param_3);
extern uint64_t init_r15(void);
extern void indirect_placeholder_12(uint64_t param_0, uint64_t param_1);
extern uint64_t indirect_placeholder(uint64_t param_0);
extern uint64_t indirect_placeholder_4(uint64_t param_0, uint64_t param_1);
extern uint64_t init_r9(void);
extern uint64_t init_r8(void);
extern void indirect_placeholder_1(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint64_t param_3, uint64_t param_4, uint64_t param_5, uint64_t param_6);
extern void indirect_placeholder_38(uint64_t param_0, uint64_t param_1, uint64_t param_2);
extern struct indirect_placeholder_39_ret_type indirect_placeholder_39(uint64_t param_0, uint64_t param_1, uint64_t param_2);
extern struct indirect_placeholder_40_ret_type indirect_placeholder_40(uint64_t param_0, uint64_t param_1, uint64_t param_2);
extern struct indirect_placeholder_41_ret_type indirect_placeholder_41(uint64_t param_0, uint64_t param_1, uint64_t param_2);
extern struct indirect_placeholder_42_ret_type indirect_placeholder_42(uint64_t param_0);
typedef _Bool bool;
uint64_t bb_tee_files(uint64_t rdi, uint64_t rsi) {
    uint64_t var_0;
    uint64_t var_1;
    uint64_t var_2;
    uint64_t var_3;
    uint64_t var_4;
    uint64_t var_5;
    uint64_t var_6;
    uint64_t var_7;
    uint64_t var_8;
    uint64_t var_9;
    unsigned char var_10;
    uint64_t var_11;
    uint64_t *var_12;
    uint64_t var_13;
    uint64_t *var_14;
    uint64_t var_15;
    uint64_t local_sp_10;
    uint64_t local_sp_9_be;
    uint64_t local_sp_9;
    uint64_t rbx_1;
    uint64_t var_81;
    struct indirect_placeholder_39_ret_type var_82;
    uint64_t var_83;
    uint64_t var_84;
    uint64_t var_85;
    uint64_t var_86;
    uint64_t var_87;
    uint64_t local_sp_8;
    uint64_t local_sp_11;
    uint64_t r15_0;
    uint32_t var_58;
    uint64_t var_59;
    uint64_t var_60;
    uint64_t rbx_0;
    uint32_t *var_55;
    uint64_t var_56;
    uint64_t var_57;
    uint64_t local_sp_1;
    uint64_t rax_0;
    uint64_t rcx_0;
    uint64_t local_sp_14;
    uint64_t r9_0;
    uint64_t r12_1;
    uint64_t r8_0;
    uint64_t local_sp_0;
    uint64_t rax_2;
    uint64_t r8_8;
    uint64_t var_62;
    struct indirect_placeholder_40_ret_type var_63;
    uint64_t var_64;
    uint64_t var_65;
    uint64_t var_66;
    uint64_t var_67;
    uint64_t var_68;
    uint64_t var_61;
    uint64_t rcx_6;
    uint64_t r9_8;
    uint64_t var_49;
    uint64_t r12_6;
    uint64_t local_sp_6;
    uint32_t var_50;
    uint64_t var_51;
    uint64_t r12_2168;
    uint64_t rcx_1;
    uint64_t rax_1;
    uint64_t r8_2;
    uint64_t r9_1;
    uint64_t local_sp_3;
    uint64_t r8_1;
    uint64_t local_sp_2;
    uint64_t r12_0;
    uint64_t *var_52;
    uint64_t var_53;
    uint64_t var_54;
    uint64_t r9_2;
    uint64_t rax_4;
    uint64_t cc_src_3;
    uint64_t rcx_3;
    uint64_t r9_4;
    uint64_t r8_4;
    uint64_t local_sp_5;
    uint64_t var_70;
    uint64_t var_71;
    uint64_t local_sp_4167;
    uint64_t r8_3166;
    uint64_t r9_3165;
    uint64_t cc_src_0164;
    uint64_t rcx_2163;
    uint64_t rax_3162;
    uint64_t rax_5;
    uint32_t var_72;
    uint64_t var_73;
    uint64_t var_74;
    uint64_t r13_0;
    uint64_t var_69;
    uint64_t r9_6_ph;
    uint64_t r12_4;
    uint64_t var_36;
    struct indirect_placeholder_41_ret_type var_37;
    uint64_t var_38;
    uint64_t var_39;
    uint64_t var_40;
    uint64_t var_41;
    uint64_t var_42;
    uint64_t var_43;
    uint64_t rcx_4;
    uint64_t r14_0;
    uint64_t var_34;
    uint64_t var_35;
    uint64_t local_sp_12;
    uint64_t r8_6_ph;
    uint64_t r9_5;
    uint64_t local_sp_12_ph;
    uint64_t r8_5;
    uint64_t local_sp_7;
    uint64_t r12_3;
    uint64_t *_pre_phi;
    uint64_t *var_75;
    uint64_t var_76;
    uint64_t var_77;
    uint64_t var_78;
    uint64_t var_79;
    uint64_t var_80;
    uint64_t var_27;
    uint64_t var_28;
    uint64_t r12_4_ph;
    uint64_t r14_0_ph;
    uint64_t var_29;
    uint64_t var_30;
    uint64_t var_31;
    uint64_t *var_32;
    uint64_t var_33;
    uint64_t rcx_5;
    uint64_t cc_src_2;
    uint64_t r9_7;
    uint64_t r8_7;
    uint64_t local_sp_13;
    uint64_t r12_5;
    uint64_t var_44;
    uint64_t var_45;
    uint64_t var_46;
    uint64_t var_47;
    uint64_t *var_48;
    uint64_t var_16;
    uint64_t *var_17;
    struct indirect_placeholder_42_ret_type var_18;
    uint64_t var_19;
    uint64_t var_20;
    uint64_t var_21;
    uint64_t var_22;
    uint64_t var_23;
    uint64_t *var_24;
    uint32_t var_25;
    uint64_t var_26;
    unsigned int loop_state_var;
    bool switch_state_var;
    var_0 = revng_init_local_sp(0UL);
    var_1 = init_rbx();
    var_2 = init_r9();
    var_3 = init_rbp();
    var_4 = init_r8();
    var_5 = init_r13();
    var_6 = init_cc_src2();
    var_7 = init_r12();
    var_8 = init_r15();
    var_9 = init_r14();
    *(uint64_t *)(var_0 + (-8L)) = var_8;
    *(uint64_t *)(var_0 + (-16L)) = var_9;
    *(uint64_t *)(var_0 + (-24L)) = var_5;
    *(uint64_t *)(var_0 + (-32L)) = var_7;
    *(uint64_t *)(var_0 + (-40L)) = var_3;
    *(uint64_t *)(var_0 + (-48L)) = var_1;
    var_10 = *(unsigned char *)6358293UL;
    *(uint32_t *)(var_0 + (-1092L)) = (uint32_t)rdi;
    var_11 = *(uint64_t *)6358152UL;
    var_12 = (uint64_t *)(var_0 + (-1104L));
    *var_12 = rsi;
    var_13 = (var_10 == '\x00') ? 4247682UL : 4247680UL;
    var_14 = (uint64_t *)(var_0 + (-1120L));
    *var_14 = 4202003UL;
    indirect_placeholder_38(var_13, var_11, 2UL);
    var_15 = (rdi << 32UL) + 4294967296UL;
    rbx_1 = 0UL;
    rax_0 = 0UL;
    rcx_1 = 0UL;
    cc_src_0164 = 4UL;
    r9_6_ph = var_2;
    rcx_4 = 0UL;
    r8_6_ph = var_4;
    r12_4_ph = 1UL;
    r14_0_ph = 0UL;
    rcx_5 = 0UL;
    r9_7 = var_2;
    r8_7 = var_4;
    r12_5 = 1UL;
    if ((uint64_t)((long)var_15 >> (long)32UL) > 1152921504606846975UL) {
        *(uint64_t *)(var_0 + (-1128L)) = 4202847UL;
        indirect_placeholder_12(var_2, var_4);
        abort();
    }
    var_16 = (uint64_t)((long)var_15 >> (long)29UL);
    var_17 = (uint64_t *)(var_0 + (-1128L));
    *var_17 = 4202043UL;
    var_18 = indirect_placeholder_42(var_16);
    var_19 = var_18.field_0;
    var_20 = *var_14;
    var_21 = *(uint64_t *)6358144UL;
    *var_17 = var_19;
    var_22 = var_20 + (-8L);
    *(uint64_t *)var_22 = 4247684UL;
    *(uint64_t *)var_19 = var_21;
    *var_12 = var_22;
    var_23 = var_0 + (-1136L);
    var_24 = (uint64_t *)var_23;
    *var_24 = 4202093UL;
    indirect_placeholder_2();
    var_25 = *(uint32_t *)(var_0 + (-1116L));
    var_26 = helper_cc_compute_all_wrapper((uint64_t)var_25, 0UL, 0UL, 24U);
    local_sp_12_ph = var_23;
    _pre_phi = var_24;
    cc_src_2 = var_26;
    local_sp_13 = var_23;
    if ((uint64_t)(((unsigned char)(var_26 >> 4UL) ^ (unsigned char)var_26) & '\xc0') == 0UL) {
        *(unsigned char *)(var_0 + (-1117L)) = (unsigned char)'\x01';
    } else {
        var_27 = (uint64_t)(var_25 + (-1));
        *(unsigned char *)(var_0 + (-1117L)) = (unsigned char)'\x01';
        var_28 = var_27 + 1UL;
        r13_0 = var_27;
        cc_src_2 = var_28;
        while (1U)
            {
                r12_4 = r12_4_ph;
                r14_0 = r14_0_ph;
                local_sp_12 = local_sp_12_ph;
                r9_5 = r9_6_ph;
                r8_5 = r8_6_ph;
                while (1U)
                    {
                        var_29 = *(uint64_t *)(local_sp_12 + 8UL);
                        var_30 = r14_0 << 3UL;
                        var_31 = *(uint64_t *)(var_30 + var_29);
                        var_32 = (uint64_t *)(local_sp_12 + (-8L));
                        *var_32 = 4202183UL;
                        var_33 = indirect_placeholder_4(var_31, var_13);
                        *(uint64_t *)((var_30 + *var_32) + 8UL) = var_33;
                        r12_3 = r12_4;
                        r12_4_ph = r12_4;
                        if (var_33 != 0UL) {
                            loop_state_var = 0U;
                            break;
                        }
                        var_34 = r12_4 + 1UL;
                        var_35 = local_sp_12 + (-16L);
                        *(uint64_t *)var_35 = 4202161UL;
                        indirect_placeholder_2();
                        local_sp_7 = var_35;
                        r12_3 = var_34;
                        local_sp_12 = var_35;
                        r12_4 = var_34;
                        if (r14_0 != var_27) {
                            loop_state_var = 1U;
                            break;
                        }
                        r14_0 = r14_0 + 1UL;
                        continue;
                    }
                switch_state_var = 0;
                switch (loop_state_var) {
                  case 1U:
                    {
                        switch_state_var = 1;
                        break;
                    }
                    break;
                  case 0U:
                    {
                        var_36 = *(uint64_t *)(var_30 + *(uint64_t *)local_sp_12);
                        *(uint64_t *)(local_sp_12 + (-16L)) = 4202218UL;
                        var_37 = indirect_placeholder_41(0UL, 3UL, var_36);
                        var_38 = var_37.field_0;
                        var_39 = var_37.field_1;
                        var_40 = var_37.field_2;
                        *(uint64_t *)(local_sp_12 + (-24L)) = 4202226UL;
                        indirect_placeholder_2();
                        var_41 = (uint64_t)*(uint32_t *)var_38;
                        var_42 = ((*(uint32_t *)6358288UL + (-3)) < 2U);
                        var_43 = local_sp_12 + (-32L);
                        *(uint64_t *)var_43 = 4202265UL;
                        indirect_placeholder_1(0UL, var_38, var_39, var_42, var_40, var_41, 4249692UL);
                        *(unsigned char *)(local_sp_12 + (-13L)) = (unsigned char)'\x00';
                        r9_6_ph = var_39;
                        rcx_4 = var_38;
                        r8_6_ph = var_40;
                        r9_5 = var_39;
                        local_sp_12_ph = var_43;
                        r8_5 = var_40;
                        local_sp_7 = var_43;
                        if (r14_0 == var_27) {
                            switch_state_var = 1;
                            break;
                        }
                        r14_0_ph = r14_0 + 1UL;
                        continue;
                    }
                    break;
                }
                if (switch_state_var)
                    break;
            }
        local_sp_8 = local_sp_7;
        rcx_5 = rcx_4;
        r9_7 = r9_5;
        r8_7 = r8_5;
        local_sp_13 = local_sp_7;
        r12_5 = r12_3;
        if (r12_3 != 0UL) {
            local_sp_9 = local_sp_8;
            while (1U)
                {
                    var_75 = (uint64_t *)local_sp_9;
                    var_76 = *var_75;
                    var_77 = rbx_1 << 3UL;
                    var_78 = *(uint64_t *)((var_77 + var_76) + 8UL);
                    local_sp_10 = local_sp_9;
                    if (var_78 != 0UL) {
                        local_sp_9_be = local_sp_10;
                        local_sp_11 = local_sp_10;
                        if (rbx_1 == r13_0) {
                            break;
                        }
                        rbx_1 = rbx_1 + 1UL;
                        local_sp_9 = local_sp_9_be;
                        continue;
                    }
                    var_79 = local_sp_9 + (-8L);
                    *(uint64_t *)var_79 = 4202660UL;
                    var_80 = indirect_placeholder(var_78);
                    local_sp_10 = var_79;
                    if ((uint64_t)(uint32_t)var_80 != 0UL) {
                        var_81 = *(uint64_t *)(var_77 + *var_75);
                        *(uint64_t *)(local_sp_9 + (-16L)) = 4202689UL;
                        var_82 = indirect_placeholder_39(0UL, 3UL, var_81);
                        var_83 = var_82.field_0;
                        var_84 = var_82.field_1;
                        var_85 = var_82.field_2;
                        *(uint64_t *)(local_sp_9 + (-24L)) = 4202697UL;
                        indirect_placeholder_2();
                        var_86 = (uint64_t)*(uint32_t *)var_83;
                        var_87 = local_sp_9 + (-32L);
                        *(uint64_t *)var_87 = 4202716UL;
                        indirect_placeholder_1(0UL, var_83, var_84, 0UL, var_85, var_86, 4249692UL);
                        *(unsigned char *)(local_sp_9 + (-13L)) = (unsigned char)'\x00';
                        local_sp_9_be = var_87;
                        local_sp_11 = var_87;
                        if (rbx_1 == r13_0) {
                            break;
                        }
                    }
                    local_sp_9_be = local_sp_10;
                    local_sp_11 = local_sp_10;
                    if (rbx_1 == r13_0) {
                        break;
                    }
                }
            *(uint64_t *)(local_sp_11 + (-8L)) = 4202735UL;
            indirect_placeholder_2();
            return (uint64_t)*(unsigned char *)(local_sp_11 + 11UL);
        }
        _pre_phi = (uint64_t *)local_sp_7;
    }
    var_44 = (uint64_t)*(uint32_t *)(local_sp_13 + 20UL);
    var_45 = (var_44 << 3UL) + *_pre_phi;
    var_46 = var_45 + 8UL;
    local_sp_14 = local_sp_13;
    r8_8 = r8_7;
    rcx_6 = rcx_5;
    r9_8 = r9_7;
    r12_6 = r12_5;
    cc_src_3 = cc_src_2;
    rax_5 = var_44;
    while (1U)
        {
            var_47 = local_sp_14 + (-8L);
            var_48 = (uint64_t *)var_47;
            *var_48 = 4202321UL;
            indirect_placeholder_2();
            local_sp_6 = var_47;
            r12_2168 = r12_6;
            r9_1 = r9_8;
            r8_1 = r8_8;
            local_sp_2 = var_47;
            r12_0 = r12_6;
            rax_4 = rax_5;
            rcx_3 = rcx_6;
            r9_4 = r9_8;
            r8_4 = r8_8;
            local_sp_4167 = var_47;
            r8_3166 = r8_8;
            r9_3165 = r9_8;
            rcx_2163 = rcx_6;
            rax_3162 = rax_5;
            if ((long)rax_5 < (long)0UL) {
                var_69 = local_sp_14 + (-16L);
                *(uint64_t *)var_69 = 4202573UL;
                indirect_placeholder_2();
                local_sp_5 = var_69;
                local_sp_4167 = var_69;
                if (*(uint32_t *)rax_5 != 4U) {
                    loop_state_var = 1U;
                    break;
                }
            }
            var_49 = helper_cc_compute_all_wrapper(rax_5, cc_src_3, var_6, 25U);
            cc_src_0164 = var_49;
            if ((var_49 & 64UL) != 0UL) {
                loop_state_var = 0U;
                break;
            }
            var_50 = *(uint32_t *)(local_sp_14 + 12UL);
            var_51 = (uint64_t)var_50;
            rax_1 = var_51;
            rax_3162 = var_51;
            if ((int)var_50 < (int)0U) {
                local_sp_14 = local_sp_4167;
                r8_8 = r8_3166;
                rcx_6 = rcx_2163;
                r9_8 = r9_3165;
                r12_6 = r12_2168;
                cc_src_3 = cc_src_0164;
                rax_5 = rax_3162;
                continue;
            }
            rbx_0 = *(uint64_t *)(local_sp_14 + 16UL);
            r15_0 = *var_48;
            cc_src_0164 = var_46;
            while (1U)
                {
                    var_52 = (uint64_t *)r15_0;
                    var_53 = *var_52;
                    rcx_0 = var_53;
                    r9_0 = r9_1;
                    r12_1 = r12_0;
                    r8_0 = r8_1;
                    rax_2 = rax_1;
                    r8_2 = r8_1;
                    local_sp_3 = local_sp_2;
                    r9_2 = r9_1;
                    if (var_53 == 0UL) {
                        r12_2168 = r12_1;
                        rax_1 = rax_2;
                        r9_1 = r9_2;
                        r8_1 = r8_2;
                        local_sp_2 = local_sp_3;
                        r12_0 = r12_1;
                        rax_4 = rax_2;
                        rcx_3 = rcx_1;
                        r9_4 = r9_2;
                        r8_4 = r8_2;
                        local_sp_5 = local_sp_3;
                        local_sp_4167 = local_sp_3;
                        r8_3166 = r8_2;
                        r9_3165 = r9_2;
                        rcx_2163 = rcx_1;
                        rax_3162 = rax_2;
                        if (r15_0 == var_45) {
                            break;
                        }
                        rbx_0 = rbx_0 + 8UL;
                        r15_0 = r15_0 + 8UL;
                        continue;
                    }
                    var_54 = local_sp_2 + (-8L);
                    *(uint64_t *)var_54 = 4202490UL;
                    indirect_placeholder_2();
                    rax_2 = 1UL;
                    rcx_1 = var_53;
                    local_sp_3 = var_54;
                    if (rax_1 != 1UL) {
                        *(uint64_t *)(local_sp_2 + (-16L)) = 4202501UL;
                        indirect_placeholder_2();
                        var_55 = (uint32_t *)rax_1;
                        var_56 = (uint64_t)*var_55;
                        var_57 = local_sp_2 + (-24L);
                        *(uint64_t *)var_57 = 4202509UL;
                        indirect_placeholder_2();
                        local_sp_0 = var_57;
                        local_sp_1 = var_57;
                        if (*var_55 != 32U) {
                            var_58 = *(uint32_t *)6358288UL & (-3);
                            var_59 = (uint64_t)var_58;
                            rax_0 = var_59;
                            if ((uint64_t)(var_58 + (-1)) != 0UL) {
                                if (*var_52 == *(uint64_t *)6358144UL) {
                                    var_60 = local_sp_2 + (-32L);
                                    *(uint64_t *)var_60 = 4202783UL;
                                    indirect_placeholder_2();
                                    *var_52 = 0UL;
                                    local_sp_0 = var_60;
                                } else {
                                    *var_52 = 0UL;
                                }
                                r12_1 = r12_0 + (-1L);
                                rax_2 = rax_0;
                                rcx_1 = rcx_0;
                                r8_2 = r8_0;
                                local_sp_3 = local_sp_0;
                                r9_2 = r9_0;
                                r12_2168 = r12_1;
                                rax_1 = rax_2;
                                r9_1 = r9_2;
                                r8_1 = r8_2;
                                local_sp_2 = local_sp_3;
                                r12_0 = r12_1;
                                rax_4 = rax_2;
                                rcx_3 = rcx_1;
                                r9_4 = r9_2;
                                r8_4 = r8_2;
                                local_sp_5 = local_sp_3;
                                local_sp_4167 = local_sp_3;
                                r8_3166 = r8_2;
                                r9_3165 = r9_2;
                                rcx_2163 = rcx_1;
                                rax_3162 = rax_2;
                                if (r15_0 == var_45) {
                                    break;
                                }
                                rbx_0 = rbx_0 + 8UL;
                                r15_0 = r15_0 + 8UL;
                                continue;
                            }
                        }
                        if (*var_52 == *(uint64_t *)6358144UL) {
                            var_61 = local_sp_2 + (-32L);
                            *(uint64_t *)var_61 = 4202773UL;
                            indirect_placeholder_2();
                            local_sp_1 = var_61;
                        }
                        var_62 = *(uint64_t *)rbx_0;
                        *(uint64_t *)(local_sp_1 + (-8L)) = 4202399UL;
                        var_63 = indirect_placeholder_40(0UL, 3UL, var_62);
                        var_64 = var_63.field_0;
                        var_65 = var_63.field_1;
                        var_66 = var_63.field_2;
                        var_67 = ((*(uint32_t *)6358288UL + (-3)) < 2U);
                        var_68 = local_sp_1 + (-16L);
                        *(uint64_t *)var_68 = 4202435UL;
                        indirect_placeholder_1(0UL, var_64, var_65, var_67, var_66, var_56, 4249692UL);
                        *var_52 = 0UL;
                        *(unsigned char *)(local_sp_1 + 3UL) = (unsigned char)'\x00';
                        rcx_0 = var_64;
                        r9_0 = var_65;
                        r8_0 = var_66;
                        local_sp_0 = var_68;
                        r12_1 = r12_0 + (-1L);
                        rax_2 = rax_0;
                        rcx_1 = rcx_0;
                        r8_2 = r8_0;
                        local_sp_3 = local_sp_0;
                        r9_2 = r9_0;
                    }
                }
            if (r12_1 == 0UL) {
                loop_state_var = 1U;
                break;
            }
        }
    switch (loop_state_var) {
      case 0U:
        {
            var_72 = *(uint32_t *)(local_sp_6 + 20UL);
            var_73 = helper_cc_compute_all_wrapper((uint64_t)var_72, 0UL, 0UL, 24U);
            local_sp_8 = local_sp_6;
            local_sp_11 = local_sp_6;
            if ((uint64_t)(((unsigned char)(var_73 >> 4UL) ^ (unsigned char)var_73) & '\xc0') == 0UL) {
                *(uint64_t *)(local_sp_11 + (-8L)) = 4202735UL;
                indirect_placeholder_2();
                return (uint64_t)*(unsigned char *)(local_sp_11 + 11UL);
            }
            var_74 = (uint64_t)(var_72 + (-1));
            r13_0 = var_74;
            local_sp_9 = local_sp_8;
            while (1U)
                {
                    var_75 = (uint64_t *)local_sp_9;
                    var_76 = *var_75;
                    var_77 = rbx_1 << 3UL;
                    var_78 = *(uint64_t *)((var_77 + var_76) + 8UL);
                    local_sp_10 = local_sp_9;
                    if (var_78 != 0UL) {
                        local_sp_9_be = local_sp_10;
                        local_sp_11 = local_sp_10;
                        if (rbx_1 == r13_0) {
                            break;
                        }
                        rbx_1 = rbx_1 + 1UL;
                        local_sp_9 = local_sp_9_be;
                        continue;
                    }
                    var_79 = local_sp_9 + (-8L);
                    *(uint64_t *)var_79 = 4202660UL;
                    var_80 = indirect_placeholder(var_78);
                    local_sp_10 = var_79;
                    if ((uint64_t)(uint32_t)var_80 != 0UL) {
                        var_81 = *(uint64_t *)(var_77 + *var_75);
                        *(uint64_t *)(local_sp_9 + (-16L)) = 4202689UL;
                        var_82 = indirect_placeholder_39(0UL, 3UL, var_81);
                        var_83 = var_82.field_0;
                        var_84 = var_82.field_1;
                        var_85 = var_82.field_2;
                        *(uint64_t *)(local_sp_9 + (-24L)) = 4202697UL;
                        indirect_placeholder_2();
                        var_86 = (uint64_t)*(uint32_t *)var_83;
                        var_87 = local_sp_9 + (-32L);
                        *(uint64_t *)var_87 = 4202716UL;
                        indirect_placeholder_1(0UL, var_83, var_84, 0UL, var_85, var_86, 4249692UL);
                        *(unsigned char *)(local_sp_9 + (-13L)) = (unsigned char)'\x00';
                        local_sp_9_be = var_87;
                        local_sp_11 = var_87;
                        if (rbx_1 == r13_0) {
                            break;
                        }
                    }
                    local_sp_9_be = local_sp_10;
                    local_sp_11 = local_sp_10;
                    if (rbx_1 == r13_0) {
                        break;
                    }
                }
            *(uint64_t *)(local_sp_11 + (-8L)) = 4202735UL;
            indirect_placeholder_2();
            return (uint64_t)*(unsigned char *)(local_sp_11 + 11UL);
        }
        break;
      case 1U:
        {
            local_sp_6 = local_sp_5;
            if (rax_5 == 18446744073709551615UL) {
                *(uint64_t *)(local_sp_5 + (-8L)) = 4202800UL;
                indirect_placeholder_2();
                var_70 = (uint64_t)*(uint32_t *)rax_4;
                var_71 = local_sp_5 + (-16L);
                *(uint64_t *)var_71 = 4202816UL;
                indirect_placeholder_1(0UL, rcx_3, r9_4, 0UL, r8_4, var_70, 4247700UL);
                *(unsigned char *)(local_sp_5 + 3UL) = (unsigned char)'\x00';
                local_sp_6 = var_71;
            }
        }
        break;
    }
}

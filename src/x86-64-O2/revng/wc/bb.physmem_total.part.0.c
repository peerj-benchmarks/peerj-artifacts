typedef unsigned char __u_char;
typedef unsigned short __u_short;
typedef unsigned int __u_int;
typedef unsigned long __u_long;
typedef signed char __int8_t;
typedef unsigned char __uint8_t;
typedef short __int16_t;
typedef unsigned short __uint16_t;
typedef int __int32_t;
typedef unsigned int __uint32_t;
typedef long __int64_t;
typedef unsigned long __uint64_t;
typedef __int8_t __int_least8_t;
typedef __uint8_t __uint_least8_t;
typedef __int16_t __int_least16_t;
typedef __uint16_t __uint_least16_t;
typedef __int32_t __int_least32_t;
typedef __uint32_t __uint_least32_t;
typedef __int64_t __int_least64_t;
typedef __uint64_t __uint_least64_t;
typedef long __quad_t;
typedef unsigned long __u_quad_t;
typedef long __intmax_t;
typedef unsigned long __uintmax_t;
typedef unsigned long __dev_t;
typedef unsigned int __uid_t;
typedef unsigned int __gid_t;
typedef unsigned long __ino_t;
typedef unsigned long __ino64_t;
typedef unsigned int __mode_t;
typedef unsigned long __nlink_t;
typedef long __off_t;
typedef long __off64_t;
typedef int __pid_t;
typedef struct {
    int __val[2];
} __fsid_t;
typedef long __clock_t;
typedef unsigned long __rlim_t;
typedef unsigned long __rlim64_t;
typedef unsigned int __id_t;
typedef long __time_t;
typedef unsigned int __useconds_t;
typedef long __suseconds_t;
typedef int __daddr_t;
typedef int __key_t;
typedef int __clockid_t;
typedef void *__timer_t;
typedef long __blksize_t;
typedef long __blkcnt_t;
typedef long __blkcnt64_t;
typedef unsigned long __fsblkcnt_t;
typedef unsigned long __fsblkcnt64_t;
typedef unsigned long __fsfilcnt_t;
typedef unsigned long __fsfilcnt64_t;
typedef long __fsword_t;
typedef long __ssize_t;
typedef long __syscall_slong_t;
typedef unsigned long __syscall_ulong_t;
typedef __off64_t __loff_t;
typedef char *__caddr_t;
typedef long __intptr_t;
typedef unsigned int __socklen_t;
typedef int __sig_atomic_t;
typedef __int8_t int8_t;
typedef __int16_t int16_t;
typedef __int32_t int32_t;
typedef __int64_t int64_t;
typedef __uint8_t uint8_t;
typedef __uint16_t uint16_t;
typedef __uint32_t uint32_t;
typedef __uint64_t uint64_t;
typedef __int_least8_t int_least8_t;
typedef __int_least16_t int_least16_t;
typedef __int_least32_t int_least32_t;
typedef __int_least64_t int_least64_t;
typedef __uint_least8_t uint_least8_t;
typedef __uint_least16_t uint_least16_t;
typedef __uint_least32_t uint_least32_t;
typedef __uint_least64_t uint_least64_t;
typedef signed char int_fast8_t;
typedef long int_fast16_t;
typedef long int_fast32_t;
typedef long int_fast64_t;
typedef unsigned char uint_fast8_t;
typedef unsigned long uint_fast16_t;
typedef unsigned long uint_fast32_t;
typedef unsigned long uint_fast64_t;
typedef long intptr_t;
typedef unsigned long uintptr_t;
typedef __intmax_t intmax_t;
typedef __uintmax_t uintmax_t;
typedef _Bool bool;
struct helper_addsd_wrapper_ret_type;
struct type_4;
struct type_6;
struct helper_pxor_xmm_wrapper_ret_type;
struct helper_cvtsq2sd_wrapper_86_ret_type;
struct helper_mulsd_wrapper_91_ret_type;
struct helper_pxor_xmm_wrapper_83_ret_type;
struct helper_cvtsq2sd_wrapper_84_ret_type;
struct helper_addsd_wrapper_ret_type {
    uint64_t field_0;
    unsigned char field_1;
};
struct type_4 {
};
struct type_6 {
};
struct helper_pxor_xmm_wrapper_ret_type {
    uint64_t field_0;
    uint64_t field_1;
};
struct helper_cvtsq2sd_wrapper_86_ret_type {
    uint64_t field_0;
    unsigned char field_1;
};
struct helper_mulsd_wrapper_91_ret_type {
    uint64_t field_0;
    unsigned char field_1;
};
struct helper_pxor_xmm_wrapper_83_ret_type {
    uint64_t field_0;
    uint64_t field_1;
};
struct helper_cvtsq2sd_wrapper_84_ret_type {
    uint64_t field_0;
    unsigned char field_1;
};
extern uint64_t revng_init_local_sp(uint64_t param_0);
extern void indirect_placeholder(void);
extern unsigned char init_state_0x8549(void);
extern unsigned char init_state_0x8548(void);
extern unsigned char init_state_0x854c(void);
extern unsigned char init_state_0x854b(void);
extern unsigned char init_state_0x8547(void);
extern unsigned char init_state_0x854d(void);
extern struct helper_addsd_wrapper_ret_type helper_addsd_wrapper(struct type_4 *param_0, struct type_6 *param_1, struct type_6 *param_2, uint64_t param_3, unsigned char param_4, unsigned char param_5, unsigned char param_6, unsigned char param_7, unsigned char param_8, unsigned char param_9);
extern struct helper_pxor_xmm_wrapper_ret_type helper_pxor_xmm_wrapper(struct type_4 *param_0, struct type_6 *param_1, struct type_6 *param_2, uint64_t param_3, uint64_t param_4);
extern uint64_t init_state_0x8598(void);
extern uint64_t init_state_0x85a0(void);
extern struct helper_cvtsq2sd_wrapper_86_ret_type helper_cvtsq2sd_wrapper_86(struct type_4 *param_0, struct type_6 *param_1, uint64_t param_2, unsigned char param_3, unsigned char param_4, unsigned char param_5, unsigned char param_6);
extern struct helper_mulsd_wrapper_91_ret_type helper_mulsd_wrapper_91(struct type_4 *param_0, struct type_6 *param_1, struct type_6 *param_2, uint64_t param_3, uint64_t param_4, unsigned char param_5, unsigned char param_6, unsigned char param_7, unsigned char param_8, unsigned char param_9, unsigned char param_10);
extern uint64_t init_rax(void);
extern struct helper_pxor_xmm_wrapper_83_ret_type helper_pxor_xmm_wrapper_83(struct type_4 *param_0, struct type_6 *param_1, struct type_6 *param_2, uint64_t param_3, uint64_t param_4);
extern struct helper_cvtsq2sd_wrapper_84_ret_type helper_cvtsq2sd_wrapper_84(struct type_4 *param_0, struct type_6 *param_1, uint64_t param_2, unsigned char param_3, unsigned char param_4, unsigned char param_5, unsigned char param_6);
void bb_physmem_total_part_0(void) {
    uint64_t var_0;
    uint64_t var_1;
    uint64_t var_2;
    uint64_t var_3;
    unsigned char var_4;
    unsigned char var_5;
    unsigned char var_6;
    unsigned char var_7;
    unsigned char var_8;
    unsigned char var_9;
    uint64_t var_10;
    struct helper_cvtsq2sd_wrapper_86_ret_type var_17;
    uint64_t var_11;
    bool var_12;
    struct helper_cvtsq2sd_wrapper_84_ret_type var_13;
    uint64_t state_0x8558_0;
    struct helper_cvtsq2sd_wrapper_84_ret_type var_14;
    struct helper_addsd_wrapper_ret_type var_15;
    unsigned char storemerge;
    uint64_t var_16;
    var_0 = revng_init_local_sp(0UL);
    var_1 = init_rax();
    var_2 = init_state_0x8598();
    var_3 = init_state_0x85a0();
    var_4 = init_state_0x8549();
    var_5 = init_state_0x8548();
    var_6 = init_state_0x854c();
    var_7 = init_state_0x854b();
    var_8 = init_state_0x8547();
    var_9 = init_state_0x854d();
    *(uint64_t *)(var_0 + (-384L)) = 4208207UL;
    indirect_placeholder();
    var_10 = *(uint64_t *)4257952UL;
    if ((uint64_t)(uint32_t)var_1 == 0UL) {
        return;
    }
    var_11 = *(uint64_t *)(var_0 + (-352L));
    var_12 = ((long)var_11 < (long)0UL);
    helper_pxor_xmm_wrapper_83((struct type_4 *)(0UL), (struct type_6 *)(776UL), (struct type_6 *)(776UL), var_10, 0UL);
    if (var_12) {
        var_14 = helper_cvtsq2sd_wrapper_84((struct type_4 *)(0UL), (struct type_6 *)(776UL), (var_11 >> 1UL) | (var_11 & 1UL), var_4, var_5, var_7, var_8);
        var_15 = helper_addsd_wrapper((struct type_4 *)(0UL), (struct type_6 *)(776UL), (struct type_6 *)(776UL), var_14.field_0, var_14.field_1, var_5, var_6, var_7, var_8, var_9);
        state_0x8558_0 = var_15.field_0;
        storemerge = var_15.field_1;
    } else {
        var_13 = helper_cvtsq2sd_wrapper_84((struct type_4 *)(0UL), (struct type_6 *)(776UL), var_11, var_4, var_5, var_7, var_8);
        state_0x8558_0 = var_13.field_0;
        storemerge = var_13.field_1;
    }
    var_16 = (uint64_t)*(uint32_t *)(var_0 + (-280L));
    helper_pxor_xmm_wrapper((struct type_4 *)(0UL), (struct type_6 *)(840UL), (struct type_6 *)(840UL), var_2, var_3);
    var_17 = helper_cvtsq2sd_wrapper_86((struct type_4 *)(0UL), (struct type_6 *)(840UL), var_16, storemerge, var_5, var_7, var_8);
    helper_mulsd_wrapper_91((struct type_4 *)(0UL), (struct type_6 *)(776UL), (struct type_6 *)(840UL), state_0x8558_0, var_17.field_0, var_17.field_1, var_5, var_6, var_7, var_8, var_9);
    return;
}

typedef unsigned char __u_char;
typedef unsigned short __u_short;
typedef unsigned int __u_int;
typedef unsigned long __u_long;
typedef signed char __int8_t;
typedef unsigned char __uint8_t;
typedef short __int16_t;
typedef unsigned short __uint16_t;
typedef int __int32_t;
typedef unsigned int __uint32_t;
typedef long __int64_t;
typedef unsigned long __uint64_t;
typedef __int8_t __int_least8_t;
typedef __uint8_t __uint_least8_t;
typedef __int16_t __int_least16_t;
typedef __uint16_t __uint_least16_t;
typedef __int32_t __int_least32_t;
typedef __uint32_t __uint_least32_t;
typedef __int64_t __int_least64_t;
typedef __uint64_t __uint_least64_t;
typedef long __quad_t;
typedef unsigned long __u_quad_t;
typedef long __intmax_t;
typedef unsigned long __uintmax_t;
typedef unsigned long __dev_t;
typedef unsigned int __uid_t;
typedef unsigned int __gid_t;
typedef unsigned long __ino_t;
typedef unsigned long __ino64_t;
typedef unsigned int __mode_t;
typedef unsigned long __nlink_t;
typedef long __off_t;
typedef long __off64_t;
typedef int __pid_t;
typedef struct {
    int __val[2];
} __fsid_t;
typedef long __clock_t;
typedef unsigned long __rlim_t;
typedef unsigned long __rlim64_t;
typedef unsigned int __id_t;
typedef long __time_t;
typedef unsigned int __useconds_t;
typedef long __suseconds_t;
typedef int __daddr_t;
typedef int __key_t;
typedef int __clockid_t;
typedef void *__timer_t;
typedef long __blksize_t;
typedef long __blkcnt_t;
typedef long __blkcnt64_t;
typedef unsigned long __fsblkcnt_t;
typedef unsigned long __fsblkcnt64_t;
typedef unsigned long __fsfilcnt_t;
typedef unsigned long __fsfilcnt64_t;
typedef long __fsword_t;
typedef long __ssize_t;
typedef long __syscall_slong_t;
typedef unsigned long __syscall_ulong_t;
typedef __off64_t __loff_t;
typedef char *__caddr_t;
typedef long __intptr_t;
typedef unsigned int __socklen_t;
typedef int __sig_atomic_t;
typedef __int8_t int8_t;
typedef __int16_t int16_t;
typedef __int32_t int32_t;
typedef __int64_t int64_t;
typedef __uint8_t uint8_t;
typedef __uint16_t uint16_t;
typedef __uint32_t uint32_t;
typedef __uint64_t uint64_t;
typedef __int_least8_t int_least8_t;
typedef __int_least16_t int_least16_t;
typedef __int_least32_t int_least32_t;
typedef __int_least64_t int_least64_t;
typedef __uint_least8_t uint_least8_t;
typedef __uint_least16_t uint_least16_t;
typedef __uint_least32_t uint_least32_t;
typedef __uint_least64_t uint_least64_t;
typedef signed char int_fast8_t;
typedef long int_fast16_t;
typedef long int_fast32_t;
typedef long int_fast64_t;
typedef unsigned char uint_fast8_t;
typedef unsigned long uint_fast16_t;
typedef unsigned long uint_fast32_t;
typedef unsigned long uint_fast64_t;
typedef long intptr_t;
typedef unsigned long uintptr_t;
typedef __intmax_t intmax_t;
typedef __uintmax_t uintmax_t;
typedef _Bool bool;
struct indirect_placeholder_125_ret_type;
struct indirect_placeholder_127_ret_type;
struct indirect_placeholder_126_ret_type;
struct indirect_placeholder_128_ret_type;
struct indirect_placeholder_130_ret_type;
struct indirect_placeholder_131_ret_type;
struct indirect_placeholder_129_ret_type;
struct indirect_placeholder_132_ret_type;
struct indirect_placeholder_125_ret_type {
    uint64_t field_0;
    uint64_t field_1;
};
struct indirect_placeholder_127_ret_type {
    uint64_t field_0;
    uint64_t field_1;
    uint64_t field_2;
};
struct indirect_placeholder_126_ret_type {
    uint64_t field_0;
    uint64_t field_1;
};
struct indirect_placeholder_128_ret_type {
    uint64_t field_0;
    uint64_t field_1;
};
struct indirect_placeholder_130_ret_type {
    uint64_t field_0;
    uint64_t field_1;
};
struct indirect_placeholder_131_ret_type {
    uint64_t field_0;
    uint64_t field_1;
};
struct indirect_placeholder_129_ret_type {
    uint64_t field_0;
    uint64_t field_1;
};
struct indirect_placeholder_132_ret_type {
    uint64_t field_0;
    uint64_t field_1;
};
extern uint64_t revng_init_local_sp(uint64_t param_0);
extern uint64_t init_rbx(void);
extern uint64_t init_rbp(void);
extern uint64_t init_r12(void);
extern void indirect_placeholder_29(uint64_t param_0);
extern struct indirect_placeholder_125_ret_type indirect_placeholder_125(uint64_t param_0, uint64_t param_1);
extern struct indirect_placeholder_127_ret_type indirect_placeholder_127(uint64_t param_0);
extern struct indirect_placeholder_126_ret_type indirect_placeholder_126(uint64_t param_0);
extern struct indirect_placeholder_128_ret_type indirect_placeholder_128(uint64_t param_0);
extern struct indirect_placeholder_130_ret_type indirect_placeholder_130(uint64_t param_0);
extern struct indirect_placeholder_131_ret_type indirect_placeholder_131(uint64_t param_0, uint64_t param_1);
extern struct indirect_placeholder_129_ret_type indirect_placeholder_129(uint64_t param_0);
extern struct indirect_placeholder_132_ret_type indirect_placeholder_132(uint64_t param_0);
uint64_t bb_eval(uint64_t rdi, uint64_t rdx) {
    uint64_t var_0;
    uint64_t var_1;
    uint64_t var_2;
    uint64_t var_3;
    uint64_t var_4;
    uint64_t var_5;
    struct indirect_placeholder_125_ret_type var_6;
    bool var_7;
    uint64_t local_sp_2;
    uint64_t local_sp_0;
    uint64_t rdi1_0;
    struct indirect_placeholder_127_ret_type var_20;
    uint64_t var_21;
    uint64_t var_22;
    struct indirect_placeholder_126_ret_type var_23;
    uint64_t rbx_1;
    uint64_t rdx2_0;
    uint64_t var_18;
    struct indirect_placeholder_128_ret_type var_19;
    uint64_t local_sp_2_be;
    uint64_t rbx_2_be;
    uint64_t var_8;
    uint64_t rbx_0;
    uint64_t local_sp_1;
    uint64_t var_10;
    struct indirect_placeholder_130_ret_type var_11;
    uint64_t rdx2_1;
    struct indirect_placeholder_131_ret_type var_12;
    uint64_t var_13;
    struct indirect_placeholder_129_ret_type var_14;
    bool var_15;
    uint64_t var_16;
    uint64_t *var_17;
    uint64_t rbx_2;
    struct indirect_placeholder_132_ret_type var_9;
    unsigned int loop_state_var;
    bool switch_state_var;
    var_0 = revng_init_local_sp(0UL);
    var_1 = init_rbx();
    var_2 = init_rbp();
    var_3 = init_r12();
    *(uint64_t *)(var_0 + (-8L)) = var_3;
    *(uint64_t *)(var_0 + (-16L)) = var_2;
    *(uint64_t *)(var_0 + (-24L)) = var_1;
    var_4 = (uint64_t)(unsigned char)rdi;
    var_5 = var_0 + (-32L);
    *(uint64_t *)var_5 = 4209296UL;
    var_6 = indirect_placeholder_125(var_4, rdx);
    var_7 = (var_4 == 0UL);
    rdi1_0 = 0UL;
    local_sp_2 = var_5;
    rbx_2 = var_6.field_0;
    var_8 = local_sp_2 + (-8L);
    *(uint64_t *)var_8 = 4209314UL;
    var_9 = indirect_placeholder_132(4312739UL);
    local_sp_0 = var_8;
    rbx_0 = rbx_2;
    rbx_1 = rbx_2;
    while ((uint64_t)(unsigned char)var_9.field_0 != 0UL)
        {
            rdx2_0 = var_9.field_1;
            while (1U)
                {
                    rbx_2_be = rbx_0;
                    local_sp_1 = local_sp_0;
                    rdx2_1 = rdx2_0;
                    if (var_7) {
                        var_10 = local_sp_0 + (-8L);
                        *(uint64_t *)var_10 = 4209333UL;
                        var_11 = indirect_placeholder_130(rbx_0);
                        local_sp_1 = var_10;
                        rdi1_0 = (uint64_t)(unsigned char)var_11.field_0;
                        rdx2_1 = var_11.field_1;
                    }
                    *(uint64_t *)(local_sp_1 + (-8L)) = 4209341UL;
                    var_12 = indirect_placeholder_131(rdi1_0, rdx2_1);
                    var_13 = var_12.field_0;
                    *(uint64_t *)(local_sp_1 + (-16L)) = 4209352UL;
                    var_14 = indirect_placeholder_129(rbx_0);
                    var_15 = ((uint64_t)(unsigned char)var_14.field_0 == 0UL);
                    var_16 = local_sp_1 + (-24L);
                    var_17 = (uint64_t *)var_16;
                    local_sp_2_be = var_16;
                    if (!var_15) {
                        loop_state_var = 0U;
                        break;
                    }
                    *var_17 = 4209367UL;
                    indirect_placeholder_29(rbx_0);
                    var_18 = local_sp_1 + (-32L);
                    *(uint64_t *)var_18 = 4209375UL;
                    var_19 = indirect_placeholder_128(var_13);
                    local_sp_2_be = var_18;
                    rbx_2_be = var_13;
                    if ((uint64_t)(unsigned char)var_19.field_0 != 0UL) {
                        loop_state_var = 1U;
                        break;
                    }
                    *(uint64_t *)(local_sp_1 + (-40L)) = 4209387UL;
                    indirect_placeholder_29(var_13);
                    *(uint64_t *)(local_sp_1 + (-48L)) = 4209397UL;
                    var_20 = indirect_placeholder_127(16UL);
                    var_21 = var_20.field_0;
                    *(uint32_t *)var_21 = 0U;
                    *(uint64_t *)(var_21 + 8UL) = 0UL;
                    var_22 = local_sp_1 + (-56L);
                    *(uint64_t *)var_22 = 4209424UL;
                    var_23 = indirect_placeholder_126(4312739UL);
                    local_sp_0 = var_22;
                    rbx_0 = var_21;
                    rbx_1 = var_21;
                    if ((uint64_t)(unsigned char)var_23.field_0 != 0UL) {
                        loop_state_var = 2U;
                        break;
                    }
                    rdx2_0 = var_23.field_1;
                    continue;
                }
            switch_state_var = 0;
            switch (loop_state_var) {
              case 2U:
                {
                    switch_state_var = 1;
                    break;
                }
                break;
              case 1U:
              case 0U:
                {
                    switch (loop_state_var) {
                      case 0U:
                        {
                            *var_17 = 4209448UL;
                            indirect_placeholder_29(var_13);
                        }
                        break;
                      case 1U:
                        {
                            local_sp_2 = local_sp_2_be;
                            rbx_2 = rbx_2_be;
                            var_8 = local_sp_2 + (-8L);
                            *(uint64_t *)var_8 = 4209314UL;
                            var_9 = indirect_placeholder_132(4312739UL);
                            local_sp_0 = var_8;
                            rbx_0 = rbx_2;
                            rbx_1 = rbx_2;
                            continue;
                        }
                        break;
                    }
                }
                break;
            }
            if (switch_state_var)
                break;
        }
    return rbx_1;
}

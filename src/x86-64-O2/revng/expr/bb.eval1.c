typedef unsigned char __u_char;
typedef unsigned short __u_short;
typedef unsigned int __u_int;
typedef unsigned long __u_long;
typedef signed char __int8_t;
typedef unsigned char __uint8_t;
typedef short __int16_t;
typedef unsigned short __uint16_t;
typedef int __int32_t;
typedef unsigned int __uint32_t;
typedef long __int64_t;
typedef unsigned long __uint64_t;
typedef __int8_t __int_least8_t;
typedef __uint8_t __uint_least8_t;
typedef __int16_t __int_least16_t;
typedef __uint16_t __uint_least16_t;
typedef __int32_t __int_least32_t;
typedef __uint32_t __uint_least32_t;
typedef __int64_t __int_least64_t;
typedef __uint64_t __uint_least64_t;
typedef long __quad_t;
typedef unsigned long __u_quad_t;
typedef long __intmax_t;
typedef unsigned long __uintmax_t;
typedef unsigned long __dev_t;
typedef unsigned int __uid_t;
typedef unsigned int __gid_t;
typedef unsigned long __ino_t;
typedef unsigned long __ino64_t;
typedef unsigned int __mode_t;
typedef unsigned long __nlink_t;
typedef long __off_t;
typedef long __off64_t;
typedef int __pid_t;
typedef struct {
    int __val[2];
} __fsid_t;
typedef long __clock_t;
typedef unsigned long __rlim_t;
typedef unsigned long __rlim64_t;
typedef unsigned int __id_t;
typedef long __time_t;
typedef unsigned int __useconds_t;
typedef long __suseconds_t;
typedef int __daddr_t;
typedef int __key_t;
typedef int __clockid_t;
typedef void *__timer_t;
typedef long __blksize_t;
typedef long __blkcnt_t;
typedef long __blkcnt64_t;
typedef unsigned long __fsblkcnt_t;
typedef unsigned long __fsblkcnt64_t;
typedef unsigned long __fsfilcnt_t;
typedef unsigned long __fsfilcnt64_t;
typedef long __fsword_t;
typedef long __ssize_t;
typedef long __syscall_slong_t;
typedef unsigned long __syscall_ulong_t;
typedef __off64_t __loff_t;
typedef char *__caddr_t;
typedef long __intptr_t;
typedef unsigned int __socklen_t;
typedef int __sig_atomic_t;
typedef __int8_t int8_t;
typedef __int16_t int16_t;
typedef __int32_t int32_t;
typedef __int64_t int64_t;
typedef __uint8_t uint8_t;
typedef __uint16_t uint16_t;
typedef __uint32_t uint32_t;
typedef __uint64_t uint64_t;
typedef __int_least8_t int_least8_t;
typedef __int_least16_t int_least16_t;
typedef __int_least32_t int_least32_t;
typedef __int_least64_t int_least64_t;
typedef __uint_least8_t uint_least8_t;
typedef __uint_least16_t uint_least16_t;
typedef __uint_least32_t uint_least32_t;
typedef __uint_least64_t uint_least64_t;
typedef signed char int_fast8_t;
typedef long int_fast16_t;
typedef long int_fast32_t;
typedef long int_fast64_t;
typedef unsigned char uint_fast8_t;
typedef unsigned long uint_fast16_t;
typedef unsigned long uint_fast32_t;
typedef unsigned long uint_fast64_t;
typedef long intptr_t;
typedef unsigned long uintptr_t;
typedef __intmax_t intmax_t;
typedef __uintmax_t uintmax_t;
typedef _Bool bool;
struct bb_eval1_ret_type;
struct indirect_placeholder_122_ret_type;
struct indirect_placeholder_123_ret_type;
struct indirect_placeholder_121_ret_type;
struct indirect_placeholder_124_ret_type;
struct indirect_placeholder_117_ret_type;
struct indirect_placeholder_120_ret_type;
struct indirect_placeholder_119_ret_type;
struct indirect_placeholder_118_ret_type;
struct bb_eval1_ret_type {
    uint64_t field_0;
    uint64_t field_1;
};
struct indirect_placeholder_122_ret_type {
    uint64_t field_0;
    uint64_t field_1;
};
struct indirect_placeholder_123_ret_type {
    uint64_t field_0;
    uint64_t field_1;
};
struct indirect_placeholder_121_ret_type {
    uint64_t field_0;
    uint64_t field_1;
};
struct indirect_placeholder_124_ret_type {
    uint64_t field_0;
    uint64_t field_1;
};
struct indirect_placeholder_117_ret_type {
    uint64_t field_0;
    uint64_t field_1;
};
struct indirect_placeholder_120_ret_type {
    uint64_t field_0;
    uint64_t field_1;
};
struct indirect_placeholder_119_ret_type {
    uint64_t field_0;
    uint64_t field_1;
    uint64_t field_2;
};
struct indirect_placeholder_118_ret_type {
    uint64_t field_0;
    uint64_t field_1;
};
extern uint64_t revng_init_local_sp(uint64_t param_0);
extern uint64_t init_rbx(void);
extern uint64_t init_rbp(void);
extern uint64_t init_r12(void);
extern void indirect_placeholder_29(uint64_t param_0);
extern struct indirect_placeholder_122_ret_type indirect_placeholder_122(uint64_t param_0);
extern struct indirect_placeholder_123_ret_type indirect_placeholder_123(uint64_t param_0, uint64_t param_1);
extern struct indirect_placeholder_121_ret_type indirect_placeholder_121(uint64_t param_0);
extern struct indirect_placeholder_124_ret_type indirect_placeholder_124(uint64_t param_0);
extern struct indirect_placeholder_117_ret_type indirect_placeholder_117(uint64_t param_0, uint64_t param_1);
extern struct indirect_placeholder_120_ret_type indirect_placeholder_120(uint64_t param_0);
extern struct indirect_placeholder_119_ret_type indirect_placeholder_119(uint64_t param_0);
extern struct indirect_placeholder_118_ret_type indirect_placeholder_118(uint64_t param_0);
struct bb_eval1_ret_type bb_eval1(uint64_t rdi, uint64_t rdx) {
    uint64_t rbx_0;
    uint64_t var_12;
    struct indirect_placeholder_122_ret_type var_13;
    uint64_t rdx3_1;
    struct indirect_placeholder_123_ret_type var_14;
    uint64_t var_15;
    uint64_t var_16;
    struct indirect_placeholder_121_ret_type var_17;
    uint64_t rdx3_2;
    struct bb_eval1_ret_type mrv;
    struct bb_eval1_ret_type mrv1;
    uint64_t rbx_2;
    uint64_t var_8;
    struct indirect_placeholder_124_ret_type var_9;
    bool var_10;
    uint64_t var_11;
    uint64_t var_0;
    uint64_t var_1;
    uint64_t var_2;
    uint64_t var_3;
    uint64_t var_4;
    uint64_t var_5;
    struct indirect_placeholder_117_ret_type var_6;
    bool var_7;
    uint64_t local_sp_3;
    uint64_t rdi2_0;
    uint64_t var_26;
    uint64_t var_18;
    struct indirect_placeholder_120_ret_type var_19;
    uint64_t local_sp_2;
    uint64_t local_sp_0;
    uint64_t rdx3_0;
    struct indirect_placeholder_119_ret_type var_20;
    uint64_t var_21;
    uint64_t var_22;
    struct indirect_placeholder_118_ret_type var_23;
    bool var_24;
    uint64_t var_25;
    uint64_t rbx_1;
    uint64_t local_sp_1;
    unsigned int loop_state_var;
    bool switch_state_var;
    var_0 = revng_init_local_sp(0UL);
    var_1 = init_rbx();
    var_2 = init_rbp();
    var_3 = init_r12();
    *(uint64_t *)(var_0 + (-8L)) = var_3;
    *(uint64_t *)(var_0 + (-16L)) = var_2;
    *(uint64_t *)(var_0 + (-24L)) = var_1;
    var_4 = (uint64_t)(unsigned char)rdi;
    var_5 = var_0 + (-32L);
    *(uint64_t *)var_5 = 4209120UL;
    var_6 = indirect_placeholder_117(var_4, rdx);
    var_7 = (var_4 == 0UL);
    rdi2_0 = 0UL;
    local_sp_3 = var_5;
    rbx_2 = var_6.field_0;
    var_8 = local_sp_3 + (-8L);
    *(uint64_t *)var_8 = 4209133UL;
    var_9 = indirect_placeholder_124(4312737UL);
    var_10 = ((uint64_t)(unsigned char)var_9.field_0 == 0UL);
    var_11 = var_9.field_1;
    rbx_0 = rbx_2;
    rdx3_2 = var_11;
    rdx3_0 = var_11;
    rbx_1 = rbx_2;
    local_sp_1 = var_8;
    while (!var_10)
        {
            while (1U)
                {
                    local_sp_2 = local_sp_1;
                    rdx3_1 = rdx3_0;
                    rbx_2 = rbx_0;
                    if (var_7) {
                        var_12 = local_sp_1 + (-8L);
                        *(uint64_t *)var_12 = 4209152UL;
                        var_13 = indirect_placeholder_122(rbx_0);
                        local_sp_2 = var_12;
                        rdi2_0 = (uint64_t)(unsigned char)var_13.field_0 ^ 1UL;
                        rdx3_1 = var_13.field_1;
                    }
                    *(uint64_t *)(local_sp_2 + (-8L)) = 4209163UL;
                    var_14 = indirect_placeholder_123(rdi2_0, rdx3_1);
                    var_15 = var_14.field_0;
                    var_16 = local_sp_2 + (-16L);
                    *(uint64_t *)var_16 = 4209174UL;
                    var_17 = indirect_placeholder_121(rbx_0);
                    local_sp_0 = var_16;
                    if ((uint64_t)(unsigned char)var_17.field_0 != 0UL) {
                        var_18 = local_sp_2 + (-24L);
                        *(uint64_t *)var_18 = 4209186UL;
                        var_19 = indirect_placeholder_120(var_15);
                        local_sp_0 = var_18;
                        if ((uint64_t)(unsigned char)var_19.field_0 != 0UL) {
                            loop_state_var = 1U;
                            break;
                        }
                    }
                    *(uint64_t *)(local_sp_0 + (-8L)) = 4209198UL;
                    indirect_placeholder_29(rbx_0);
                    *(uint64_t *)(local_sp_0 + (-16L)) = 4209206UL;
                    indirect_placeholder_29(var_15);
                    *(uint64_t *)(local_sp_0 + (-24L)) = 4209216UL;
                    var_20 = indirect_placeholder_119(16UL);
                    var_21 = var_20.field_0;
                    *(uint32_t *)var_21 = 0U;
                    *(uint64_t *)(var_21 + 8UL) = 0UL;
                    var_22 = local_sp_0 + (-32L);
                    *(uint64_t *)var_22 = 4209243UL;
                    var_23 = indirect_placeholder_118(4312737UL);
                    var_24 = ((uint64_t)(unsigned char)var_23.field_0 == 0UL);
                    var_25 = var_23.field_1;
                    rbx_0 = var_21;
                    rdx3_2 = var_25;
                    rdx3_0 = var_25;
                    rbx_1 = var_21;
                    local_sp_1 = var_22;
                    if (var_24) {
                        continue;
                    }
                    loop_state_var = 0U;
                    break;
                }
            switch_state_var = 0;
            switch (loop_state_var) {
              case 0U:
                {
                    switch_state_var = 1;
                    break;
                }
                break;
              case 1U:
                {
                    var_26 = local_sp_2 + (-32L);
                    *(uint64_t *)var_26 = 4209272UL;
                    indirect_placeholder_29(var_15);
                    local_sp_3 = var_26;
                    var_8 = local_sp_3 + (-8L);
                    *(uint64_t *)var_8 = 4209133UL;
                    var_9 = indirect_placeholder_124(4312737UL);
                    var_10 = ((uint64_t)(unsigned char)var_9.field_0 == 0UL);
                    var_11 = var_9.field_1;
                    rbx_0 = rbx_2;
                    rdx3_2 = var_11;
                    rdx3_0 = var_11;
                    rbx_1 = rbx_2;
                    local_sp_1 = var_8;
                    continue;
                }
                break;
            }
            if (switch_state_var)
                break;
        }
    mrv.field_0 = rbx_1;
    mrv1 = mrv;
    mrv1.field_1 = rdx3_2;
    return mrv1;
}

typedef unsigned char __u_char;
typedef unsigned short __u_short;
typedef unsigned int __u_int;
typedef unsigned long __u_long;
typedef signed char __int8_t;
typedef unsigned char __uint8_t;
typedef short __int16_t;
typedef unsigned short __uint16_t;
typedef int __int32_t;
typedef unsigned int __uint32_t;
typedef long __int64_t;
typedef unsigned long __uint64_t;
typedef __int8_t __int_least8_t;
typedef __uint8_t __uint_least8_t;
typedef __int16_t __int_least16_t;
typedef __uint16_t __uint_least16_t;
typedef __int32_t __int_least32_t;
typedef __uint32_t __uint_least32_t;
typedef __int64_t __int_least64_t;
typedef __uint64_t __uint_least64_t;
typedef long __quad_t;
typedef unsigned long __u_quad_t;
typedef long __intmax_t;
typedef unsigned long __uintmax_t;
typedef unsigned long __dev_t;
typedef unsigned int __uid_t;
typedef unsigned int __gid_t;
typedef unsigned long __ino_t;
typedef unsigned long __ino64_t;
typedef unsigned int __mode_t;
typedef unsigned long __nlink_t;
typedef long __off_t;
typedef long __off64_t;
typedef int __pid_t;
typedef struct {
    int __val[2];
} __fsid_t;
typedef long __clock_t;
typedef unsigned long __rlim_t;
typedef unsigned long __rlim64_t;
typedef unsigned int __id_t;
typedef long __time_t;
typedef unsigned int __useconds_t;
typedef long __suseconds_t;
typedef int __daddr_t;
typedef int __key_t;
typedef int __clockid_t;
typedef void *__timer_t;
typedef long __blksize_t;
typedef long __blkcnt_t;
typedef long __blkcnt64_t;
typedef unsigned long __fsblkcnt_t;
typedef unsigned long __fsblkcnt64_t;
typedef unsigned long __fsfilcnt_t;
typedef unsigned long __fsfilcnt64_t;
typedef long __fsword_t;
typedef long __ssize_t;
typedef long __syscall_slong_t;
typedef unsigned long __syscall_ulong_t;
typedef __off64_t __loff_t;
typedef char *__caddr_t;
typedef long __intptr_t;
typedef unsigned int __socklen_t;
typedef int __sig_atomic_t;
typedef __int8_t int8_t;
typedef __int16_t int16_t;
typedef __int32_t int32_t;
typedef __int64_t int64_t;
typedef __uint8_t uint8_t;
typedef __uint16_t uint16_t;
typedef __uint32_t uint32_t;
typedef __uint64_t uint64_t;
typedef __int_least8_t int_least8_t;
typedef __int_least16_t int_least16_t;
typedef __int_least32_t int_least32_t;
typedef __int_least64_t int_least64_t;
typedef __uint_least8_t uint_least8_t;
typedef __uint_least16_t uint_least16_t;
typedef __uint_least32_t uint_least32_t;
typedef __uint_least64_t uint_least64_t;
typedef signed char int_fast8_t;
typedef long int_fast16_t;
typedef long int_fast32_t;
typedef long int_fast64_t;
typedef unsigned char uint_fast8_t;
typedef unsigned long uint_fast16_t;
typedef unsigned long uint_fast32_t;
typedef unsigned long uint_fast64_t;
typedef long intptr_t;
typedef unsigned long uintptr_t;
typedef __intmax_t intmax_t;
typedef __uintmax_t uintmax_t;
typedef _Bool bool;
extern uint64_t revng_init_local_sp(uint64_t param_0);
extern uint64_t init_rbx(void);
extern uint64_t init_rbp(void);
extern void indirect_placeholder_3(void);
extern uint64_t init_r12(void);
extern void indirect_placeholder_12(uint64_t param_0);
uint64_t bb_flush_paragraph(void) {
    uint64_t var_0;
    uint64_t var_1;
    uint64_t var_2;
    uint64_t var_3;
    uint64_t var_4;
    bool var_5;
    uint64_t *var_6;
    uint64_t rbx_2;
    uint64_t rax_0;
    uint64_t rdx_0;
    uint64_t var_15;
    uint64_t var_16;
    uint64_t var_17;
    uint64_t rax_1;
    uint64_t rcx_0;
    uint64_t rbx_0;
    uint64_t rdx_1;
    uint64_t var_8;
    uint64_t var_9;
    uint64_t var_10;
    bool var_11;
    uint64_t spec_select;
    uint64_t spec_select28;
    uint64_t var_12;
    uint64_t var_13;
    uint64_t var_14;
    uint64_t var_7;
    var_0 = revng_init_local_sp(0UL);
    var_1 = init_rbx();
    var_2 = init_rbp();
    var_3 = init_r12();
    *(uint64_t *)(var_0 + (-8L)) = var_2;
    *(uint64_t *)(var_0 + (-16L)) = var_1;
    var_4 = *(uint64_t *)6362464UL;
    var_5 = (var_4 == 6362496UL);
    var_6 = (uint64_t *)(var_0 + (-32L));
    rax_1 = 9223372036854775807UL;
    rbx_0 = var_4;
    rbx_2 = var_4;
    if (var_5) {
        *var_6 = 4204300UL;
        indirect_placeholder_3();
        *(uint64_t *)6402496UL = 6402560UL;
        return var_3;
    }
    *var_6 = 4204015UL;
    indirect_placeholder_3();
    var_7 = *(uint64_t *)6362528UL;
    rcx_0 = var_7;
    if (var_4 != var_7) {
        rdx_1 = *(uint64_t *)(var_7 + 24UL);
        var_8 = *(uint64_t *)(rcx_0 + 32UL);
        var_9 = *(uint64_t *)(var_8 + 24UL);
        var_10 = rdx_1 - var_9;
        var_11 = ((long)var_10 < (long)rax_1);
        spec_select = var_11 ? var_10 : rax_1;
        spec_select28 = var_11 ? rcx_0 : rbx_0;
        rcx_0 = var_8;
        rbx_0 = spec_select28;
        rdx_1 = var_9;
        rbx_2 = spec_select28;
        while (var_4 != var_8)
            {
                rax_1 = ((long)spec_select > (long)9223372036854775798UL) ? spec_select : (spec_select + 9UL);
                var_8 = *(uint64_t *)(rcx_0 + 32UL);
                var_9 = *(uint64_t *)(var_8 + 24UL);
                var_10 = rdx_1 - var_9;
                var_11 = ((long)var_10 < (long)rax_1);
                spec_select = var_11 ? var_10 : rax_1;
                spec_select28 = var_11 ? rcx_0 : rbx_0;
                rcx_0 = var_8;
                rbx_0 = spec_select28;
                rdx_1 = var_9;
                rbx_2 = spec_select28;
            }
    }
    *(uint64_t *)(var_0 + (-40L)) = 4204116UL;
    indirect_placeholder_12(rbx_2);
    *(uint64_t *)(var_0 + (-48L)) = 4204139UL;
    indirect_placeholder_3();
    var_12 = *(uint64_t *)rbx_2;
    var_13 = *(uint64_t *)6362464UL;
    var_14 = (uint64_t)((long)((var_12 << 32UL) + (-27498785810677760L)) >> (long)32UL);
    *(uint64_t *)6402496UL = (*(uint64_t *)6402496UL - var_14);
    rax_0 = rbx_2;
    rdx_0 = var_12;
    if (var_13 < rbx_2) {
        var_17 = rbx_2 + (-6362496L);
        *(uint64_t *)(var_0 + (-56L)) = 4204246UL;
        indirect_placeholder_3();
        *(uint64_t *)6362464UL = (*(uint64_t *)6362464UL - var_17);
        return var_3;
    }
    var_15 = rdx_0 - var_14;
    var_16 = rax_0 + 40UL;
    *(uint64_t *)rax_0 = var_15;
    rax_0 = var_16;
    while (var_16 <= var_13)
        {
            rdx_0 = *(uint64_t *)var_16;
            var_15 = rdx_0 - var_14;
            var_16 = rax_0 + 40UL;
            *(uint64_t *)rax_0 = var_15;
            rax_0 = var_16;
        }
    var_17 = rbx_2 + (-6362496L);
    *(uint64_t *)(var_0 + (-56L)) = 4204246UL;
    indirect_placeholder_3();
    *(uint64_t *)6362464UL = (*(uint64_t *)6362464UL - var_17);
    return var_3;
}

typedef unsigned char __u_char;
typedef unsigned short __u_short;
typedef unsigned int __u_int;
typedef unsigned long __u_long;
typedef signed char __int8_t;
typedef unsigned char __uint8_t;
typedef short __int16_t;
typedef unsigned short __uint16_t;
typedef int __int32_t;
typedef unsigned int __uint32_t;
typedef long __int64_t;
typedef unsigned long __uint64_t;
typedef __int8_t __int_least8_t;
typedef __uint8_t __uint_least8_t;
typedef __int16_t __int_least16_t;
typedef __uint16_t __uint_least16_t;
typedef __int32_t __int_least32_t;
typedef __uint32_t __uint_least32_t;
typedef __int64_t __int_least64_t;
typedef __uint64_t __uint_least64_t;
typedef long __quad_t;
typedef unsigned long __u_quad_t;
typedef long __intmax_t;
typedef unsigned long __uintmax_t;
typedef unsigned long __dev_t;
typedef unsigned int __uid_t;
typedef unsigned int __gid_t;
typedef unsigned long __ino_t;
typedef unsigned long __ino64_t;
typedef unsigned int __mode_t;
typedef unsigned long __nlink_t;
typedef long __off_t;
typedef long __off64_t;
typedef int __pid_t;
typedef struct {
    int __val[2];
} __fsid_t;
typedef long __clock_t;
typedef unsigned long __rlim_t;
typedef unsigned long __rlim64_t;
typedef unsigned int __id_t;
typedef long __time_t;
typedef unsigned int __useconds_t;
typedef long __suseconds_t;
typedef int __daddr_t;
typedef int __key_t;
typedef int __clockid_t;
typedef void *__timer_t;
typedef long __blksize_t;
typedef long __blkcnt_t;
typedef long __blkcnt64_t;
typedef unsigned long __fsblkcnt_t;
typedef unsigned long __fsblkcnt64_t;
typedef unsigned long __fsfilcnt_t;
typedef unsigned long __fsfilcnt64_t;
typedef long __fsword_t;
typedef long __ssize_t;
typedef long __syscall_slong_t;
typedef unsigned long __syscall_ulong_t;
typedef __off64_t __loff_t;
typedef char *__caddr_t;
typedef long __intptr_t;
typedef unsigned int __socklen_t;
typedef int __sig_atomic_t;
typedef __int8_t int8_t;
typedef __int16_t int16_t;
typedef __int32_t int32_t;
typedef __int64_t int64_t;
typedef __uint8_t uint8_t;
typedef __uint16_t uint16_t;
typedef __uint32_t uint32_t;
typedef __uint64_t uint64_t;
typedef __int_least8_t int_least8_t;
typedef __int_least16_t int_least16_t;
typedef __int_least32_t int_least32_t;
typedef __int_least64_t int_least64_t;
typedef __uint_least8_t uint_least8_t;
typedef __uint_least16_t uint_least16_t;
typedef __uint_least32_t uint_least32_t;
typedef __uint_least64_t uint_least64_t;
typedef signed char int_fast8_t;
typedef long int_fast16_t;
typedef long int_fast32_t;
typedef long int_fast64_t;
typedef unsigned char uint_fast8_t;
typedef unsigned long uint_fast16_t;
typedef unsigned long uint_fast32_t;
typedef unsigned long uint_fast64_t;
typedef long intptr_t;
typedef unsigned long uintptr_t;
typedef __intmax_t intmax_t;
typedef __uintmax_t uintmax_t;
typedef _Bool bool;
extern uint64_t revng_init_local_sp(uint64_t param_0);
uint64_t bb_path_prefix(uint64_t rdi, uint64_t rsi) {
    uint64_t var_0;
    unsigned char var_1;
    uint64_t merge;
    uint64_t var_2;
    unsigned char var_3;
    bool var_4;
    uint64_t var_5;
    uint64_t rdi1_0;
    uint64_t rdx_0;
    uint64_t var_6;
    unsigned char var_7;
    uint64_t var_8;
    bool var_9;
    unsigned char var_10;
    bool var_11;
    revng_init_local_sp(0UL);
    var_0 = rdi + 1UL;
    var_1 = *(unsigned char *)var_0;
    rdi1_0 = var_0;
    merge = 0UL;
    if (var_1 == '\x00') {
        return (*(unsigned char *)(rsi + 1UL) != '/');
    }
    if ((uint64_t)(var_1 + '\xd1') != 0UL) {
        if (*(unsigned char *)(rdi + 2UL) == '\x00') {
            return (*(unsigned char *)(rsi + 1UL) == '/');
        }
    }
    var_2 = rsi + 1UL;
    var_3 = *(unsigned char *)var_2;
    var_4 = (((uint64_t)(var_1 - var_3) != 0UL) || (var_3 == '\x00'));
    var_5 = var_4;
    rdx_0 = var_2;
    if (!!var_4) {
        return merge;
    }
    while (1U)
        {
            var_6 = rdi1_0 + 1UL;
            var_7 = *(unsigned char *)var_6;
            var_8 = rdx_0 + 1UL;
            var_9 = (var_7 == '\x00');
            var_10 = *(unsigned char *)var_8;
            var_11 = (var_10 == '\x00');
            rdi1_0 = var_6;
            rdx_0 = var_8;
            merge = var_5;
            if (!var_9) {
                if (var_11) {
                    break;
                }
                if ((uint64_t)(var_7 - var_10) != 0UL) {
                    continue;
                }
                break;
            }
            return ((var_10 == '/') || var_11);
        }
    return merge;
}

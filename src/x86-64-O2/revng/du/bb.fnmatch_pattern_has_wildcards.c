typedef unsigned char __u_char;
typedef unsigned short __u_short;
typedef unsigned int __u_int;
typedef unsigned long __u_long;
typedef signed char __int8_t;
typedef unsigned char __uint8_t;
typedef short __int16_t;
typedef unsigned short __uint16_t;
typedef int __int32_t;
typedef unsigned int __uint32_t;
typedef long __int64_t;
typedef unsigned long __uint64_t;
typedef __int8_t __int_least8_t;
typedef __uint8_t __uint_least8_t;
typedef __int16_t __int_least16_t;
typedef __uint16_t __uint_least16_t;
typedef __int32_t __int_least32_t;
typedef __uint32_t __uint_least32_t;
typedef __int64_t __int_least64_t;
typedef __uint64_t __uint_least64_t;
typedef long __quad_t;
typedef unsigned long __u_quad_t;
typedef long __intmax_t;
typedef unsigned long __uintmax_t;
typedef unsigned long __dev_t;
typedef unsigned int __uid_t;
typedef unsigned int __gid_t;
typedef unsigned long __ino_t;
typedef unsigned long __ino64_t;
typedef unsigned int __mode_t;
typedef unsigned long __nlink_t;
typedef long __off_t;
typedef long __off64_t;
typedef int __pid_t;
typedef struct {
    int __val[2];
} __fsid_t;
typedef long __clock_t;
typedef unsigned long __rlim_t;
typedef unsigned long __rlim64_t;
typedef unsigned int __id_t;
typedef long __time_t;
typedef unsigned int __useconds_t;
typedef long __suseconds_t;
typedef int __daddr_t;
typedef int __key_t;
typedef int __clockid_t;
typedef void *__timer_t;
typedef long __blksize_t;
typedef long __blkcnt_t;
typedef long __blkcnt64_t;
typedef unsigned long __fsblkcnt_t;
typedef unsigned long __fsblkcnt64_t;
typedef unsigned long __fsfilcnt_t;
typedef unsigned long __fsfilcnt64_t;
typedef long __fsword_t;
typedef long __ssize_t;
typedef long __syscall_slong_t;
typedef unsigned long __syscall_ulong_t;
typedef __off64_t __loff_t;
typedef char *__caddr_t;
typedef long __intptr_t;
typedef unsigned int __socklen_t;
typedef int __sig_atomic_t;
typedef __int8_t int8_t;
typedef __int16_t int16_t;
typedef __int32_t int32_t;
typedef __int64_t int64_t;
typedef __uint8_t uint8_t;
typedef __uint16_t uint16_t;
typedef __uint32_t uint32_t;
typedef __uint64_t uint64_t;
typedef __int_least8_t int_least8_t;
typedef __int_least16_t int_least16_t;
typedef __int_least32_t int_least32_t;
typedef __int_least64_t int_least64_t;
typedef __uint_least8_t uint_least8_t;
typedef __uint_least16_t uint_least16_t;
typedef __uint_least32_t uint_least32_t;
typedef __uint_least64_t uint_least64_t;
typedef signed char int_fast8_t;
typedef long int_fast16_t;
typedef long int_fast32_t;
typedef long int_fast64_t;
typedef unsigned char uint_fast8_t;
typedef unsigned long uint_fast16_t;
typedef unsigned long uint_fast32_t;
typedef unsigned long uint_fast64_t;
typedef long intptr_t;
typedef unsigned long uintptr_t;
typedef __intmax_t intmax_t;
typedef __uintmax_t uintmax_t;
typedef _Bool bool;
struct bb_fnmatch_pattern_has_wildcards_ret_type;
struct bb_fnmatch_pattern_has_wildcards_ret_type {
    uint64_t field_0;
    uint64_t field_1;
};
extern void abort(void);
extern uint64_t revng_init_local_sp(uint64_t param_0);
struct bb_fnmatch_pattern_has_wildcards_ret_type bb_fnmatch_pattern_has_wildcards(uint64_t rdi, uint64_t rsi) {
    uint64_t var_0;
    uint64_t var_1;
    uint64_t var_2;
    bool var_3;
    bool var_4;
    bool var_5;
    uint64_t rdi4_0_be;
    uint64_t rax_0;
    uint64_t rdi4_0;
    uint64_t var_6;
    uint64_t var_7;
    struct bb_fnmatch_pattern_has_wildcards_ret_type mrv1 = {0UL, /*implicit*/(int)0};
    uint64_t rax_1;
    unsigned char var_8;
    uint64_t var_9;
    uint64_t rax_0_be;
    uint64_t var_10;
    uint64_t var_11;
    struct bb_fnmatch_pattern_has_wildcards_ret_type mrv3 = {1UL, /*implicit*/(int)0};
    uint64_t var_12;
    bool switch_state_var;
    revng_init_local_sp(0UL);
    var_0 = (uint64_t)*(unsigned char *)rdi;
    var_1 = rsi & 32UL;
    var_2 = rsi & 2UL;
    var_3 = (var_1 == 0UL);
    var_4 = ((uint64_t)((uint32_t)rsi & 134217728U) == 0UL);
    var_5 = (var_2 == 0UL);
    rax_0 = var_0;
    rdi4_0 = rdi;
    rax_1 = 0UL;
    while (1U)
        {
            var_6 = rdi4_0 + 1UL;
            rdi4_0_be = var_6;
            if ((uint64_t)((unsigned char)rax_0 & '\xfe') <= 125UL) {
                var_12 = (uint64_t)*(unsigned char *)var_6;
                rax_0_be = var_12;
                rax_0 = rax_0_be;
                rdi4_0 = rdi4_0_be;
                continue;
            }
            var_7 = *(uint64_t *)((rax_0 << 3UL) + 4372432UL) + (-4213800L);
            switch_state_var = 0;
            switch ((var_7 >> 3UL) | (var_7 << 61UL)) {
              case 12UL:
                {
                    mrv1.field_1 = var_1;
                    return mrv1;
                }
                break;
              case 1UL:
                {
                    switch_state_var = 1;
                    break;
                }
                break;
              case 0UL:
                {
                    if (var_4) {
                        switch_state_var = 1;
                        break;
                    }
                    var_12 = (uint64_t)*(unsigned char *)var_6;
                    rax_0_be = var_12;
                    rax_0 = rax_0_be;
                    rdi4_0 = rdi4_0_be;
                    continue;
                }
                break;
              case 9UL:
                {
                    if (var_3) {
                        var_8 = *(unsigned char *)var_6;
                        var_9 = (uint64_t)var_8;
                        rax_0_be = var_9;
                        if ((uint64_t)(var_8 + '\xd8') == 0UL) {
                            switch_state_var = 1;
                            break;
                        }
                        rax_0 = rax_0_be;
                        rdi4_0 = rdi4_0_be;
                        continue;
                    }
                    var_12 = (uint64_t)*(unsigned char *)var_6;
                    rax_0_be = var_12;
                    rax_0 = rax_0_be;
                    rdi4_0 = rdi4_0_be;
                    continue;
                }
                break;
              case 5UL:
              case 3UL:
                {
                    switch ((var_7 >> 3UL) | (var_7 << 61UL)) {
                      case 3UL:
                        {
                            var_12 = (uint64_t)*(unsigned char *)var_6;
                            rax_0_be = var_12;
                            rax_0 = rax_0_be;
                            rdi4_0 = rdi4_0_be;
                            continue;
                        }
                        break;
                      case 5UL:
                        {
                            if (var_4) {
                                if (var_5) {
                                    rax_1 = (*(unsigned char *)var_6 != '\x00');
                                }
                                var_10 = rax_1 + var_6;
                                var_11 = (uint64_t)*(unsigned char *)var_10;
                                rax_0_be = var_11;
                                rdi4_0_be = var_10;
                                rax_0 = rax_0_be;
                                rdi4_0 = rdi4_0_be;
                                continue;
                            }
                        }
                        break;
                    }
                }
                break;
              default:
                {
                    abort();
                }
                break;
            }
            if (switch_state_var)
                break;
        }
    mrv3.field_1 = var_1;
    return mrv3;
}

typedef unsigned char __u_char;
typedef unsigned short __u_short;
typedef unsigned int __u_int;
typedef unsigned long __u_long;
typedef signed char __int8_t;
typedef unsigned char __uint8_t;
typedef short __int16_t;
typedef unsigned short __uint16_t;
typedef int __int32_t;
typedef unsigned int __uint32_t;
typedef long __int64_t;
typedef unsigned long __uint64_t;
typedef __int8_t __int_least8_t;
typedef __uint8_t __uint_least8_t;
typedef __int16_t __int_least16_t;
typedef __uint16_t __uint_least16_t;
typedef __int32_t __int_least32_t;
typedef __uint32_t __uint_least32_t;
typedef __int64_t __int_least64_t;
typedef __uint64_t __uint_least64_t;
typedef long __quad_t;
typedef unsigned long __u_quad_t;
typedef long __intmax_t;
typedef unsigned long __uintmax_t;
typedef unsigned long __dev_t;
typedef unsigned int __uid_t;
typedef unsigned int __gid_t;
typedef unsigned long __ino_t;
typedef unsigned long __ino64_t;
typedef unsigned int __mode_t;
typedef unsigned long __nlink_t;
typedef long __off_t;
typedef long __off64_t;
typedef int __pid_t;
typedef struct {
    int __val[2];
} __fsid_t;
typedef long __clock_t;
typedef unsigned long __rlim_t;
typedef unsigned long __rlim64_t;
typedef unsigned int __id_t;
typedef long __time_t;
typedef unsigned int __useconds_t;
typedef long __suseconds_t;
typedef int __daddr_t;
typedef int __key_t;
typedef int __clockid_t;
typedef void *__timer_t;
typedef long __blksize_t;
typedef long __blkcnt_t;
typedef long __blkcnt64_t;
typedef unsigned long __fsblkcnt_t;
typedef unsigned long __fsblkcnt64_t;
typedef unsigned long __fsfilcnt_t;
typedef unsigned long __fsfilcnt64_t;
typedef long __fsword_t;
typedef long __ssize_t;
typedef long __syscall_slong_t;
typedef unsigned long __syscall_ulong_t;
typedef __off64_t __loff_t;
typedef char *__caddr_t;
typedef long __intptr_t;
typedef unsigned int __socklen_t;
typedef int __sig_atomic_t;
typedef __int8_t int8_t;
typedef __int16_t int16_t;
typedef __int32_t int32_t;
typedef __int64_t int64_t;
typedef __uint8_t uint8_t;
typedef __uint16_t uint16_t;
typedef __uint32_t uint32_t;
typedef __uint64_t uint64_t;
typedef __int_least8_t int_least8_t;
typedef __int_least16_t int_least16_t;
typedef __int_least32_t int_least32_t;
typedef __int_least64_t int_least64_t;
typedef __uint_least8_t uint_least8_t;
typedef __uint_least16_t uint_least16_t;
typedef __uint_least32_t uint_least32_t;
typedef __uint_least64_t uint_least64_t;
typedef signed char int_fast8_t;
typedef long int_fast16_t;
typedef long int_fast32_t;
typedef long int_fast64_t;
typedef unsigned char uint_fast8_t;
typedef unsigned long uint_fast16_t;
typedef unsigned long uint_fast32_t;
typedef unsigned long uint_fast64_t;
typedef long intptr_t;
typedef unsigned long uintptr_t;
typedef __intmax_t intmax_t;
typedef __uintmax_t uintmax_t;
typedef _Bool bool;
extern uint64_t revng_init_local_sp(uint64_t param_0);
extern uint64_t init_rbx(void);
extern uint64_t init_rbp(void);
extern void indirect_placeholder_1(void);
extern uint64_t init_r13(void);
extern uint64_t init_r12(void);
extern uint64_t init_r14(void);
extern uint64_t indirect_placeholder_8(uint64_t param_0);
extern uint64_t init_rax(void);
extern uint64_t init_r15(void);
uint64_t bb_locale_charset(void) {
    uint64_t var_0;
    uint64_t var_1;
    uint64_t var_2;
    uint64_t var_3;
    uint64_t var_4;
    uint64_t var_5;
    uint64_t var_6;
    uint64_t var_7;
    uint64_t var_8;
    uint64_t var_9;
    uint64_t var_10;
    uint64_t local_sp_10;
    uint64_t local_sp_0;
    uint64_t local_sp_8;
    uint64_t var_19;
    uint64_t var_18;
    uint64_t var_15;
    uint64_t local_sp_9;
    uint64_t var_20;
    bool var_11;
    uint64_t *var_12;
    uint64_t var_13;
    uint64_t var_14;
    uint64_t var_16;
    uint64_t var_17;
    uint64_t r15_6;
    uint64_t r15_5;
    uint64_t rbx_0;
    uint64_t var_22;
    uint64_t var_23;
    uint64_t var_24;
    uint64_t local_sp_11;
    uint64_t rax_8;
    bool var_21;
    unsigned int loop_state_var;
    var_0 = revng_init_local_sp(0UL);
    var_1 = init_rax();
    var_2 = init_rbx();
    var_3 = init_rbp();
    var_4 = init_r13();
    var_5 = init_r12();
    var_6 = init_r15();
    var_7 = init_r14();
    *(uint64_t *)(var_0 + (-8L)) = var_6;
    *(uint64_t *)(var_0 + (-16L)) = var_7;
    *(uint64_t *)(var_0 + (-24L)) = var_4;
    *(uint64_t *)(var_0 + (-32L)) = var_5;
    *(uint64_t *)(var_0 + (-40L)) = var_3;
    *(uint64_t *)(var_0 + (-48L)) = var_2;
    var_8 = var_0 + (-208L);
    *(uint64_t *)var_8 = 4327867UL;
    indirect_placeholder_1();
    var_9 = *(uint64_t *)6506752UL;
    var_10 = (var_1 == 0UL) ? 4381700UL : var_1;
    local_sp_10 = var_8;
    rax_8 = 4381700UL;
    r15_5 = var_9;
    rbx_0 = var_10;
    if (var_9 != 0UL) {
        *(uint64_t *)(var_0 + (-216L)) = 4328020UL;
        indirect_placeholder_1();
        var_11 = (*(unsigned char *)4381700UL == '\x00');
        *(uint64_t *)(var_0 + (-224L)) = 4328052UL;
        indirect_placeholder_1();
        var_12 = (uint64_t *)(var_0 + (-232L));
        *var_12 = 4328065UL;
        indirect_placeholder_1();
        rax_8 = 0UL;
        r15_5 = 4381700UL;
        if (*(unsigned char *)var_11 ? 8770655UL : 8767027UL == '/') {
            var_16 = var_0 + (-240L);
            *(uint64_t *)var_16 = 4328162UL;
            var_17 = indirect_placeholder_8(8770657UL);
            local_sp_9 = var_16;
            if (var_17 == 0UL) {
                var_20 = local_sp_9 + (-8L);
                *(uint64_t *)var_20 = 4328590UL;
                indirect_placeholder_1();
                local_sp_0 = var_20;
            } else {
                var_18 = var_0 + (-248L);
                *(uint64_t *)var_18 = 4328188UL;
                indirect_placeholder_1();
                local_sp_8 = var_18;
                *(uint64_t *)(local_sp_8 + (-8L)) = 4328212UL;
                indirect_placeholder_1();
                *(uint64_t *)(local_sp_8 + (-16L)) = 4328219UL;
                indirect_placeholder_1();
                *(uint64_t *)(local_sp_8 + (-24L)) = 4328231UL;
                indirect_placeholder_1();
                *(uint64_t *)(local_sp_8 + (-32L)) = 4328257UL;
                indirect_placeholder_1();
                *(uint64_t *)(local_sp_8 + (-40L)) = 4328600UL;
                indirect_placeholder_1();
                var_19 = local_sp_8 + (-48L);
                *(uint64_t *)var_19 = 4328565UL;
                indirect_placeholder_1();
                local_sp_0 = var_19;
            }
        } else {
            *var_12 = 4385329UL;
            var_13 = var_0 + (-240L);
            *(uint64_t *)var_13 = 4328102UL;
            var_14 = indirect_placeholder_8(8770658UL);
            local_sp_9 = var_13;
            if (var_14 == 0UL) {
                var_20 = local_sp_9 + (-8L);
                *(uint64_t *)var_20 = 4328590UL;
                indirect_placeholder_1();
                local_sp_0 = var_20;
            } else {
                var_15 = var_0 + (-248L);
                *(uint64_t *)var_15 = 4328136UL;
                indirect_placeholder_1();
                *(unsigned char *)(var_14 + 4385328UL) = (unsigned char)'/';
                local_sp_8 = var_15;
                *(uint64_t *)(local_sp_8 + (-8L)) = 4328212UL;
                indirect_placeholder_1();
                *(uint64_t *)(local_sp_8 + (-16L)) = 4328219UL;
                indirect_placeholder_1();
                *(uint64_t *)(local_sp_8 + (-24L)) = 4328231UL;
                indirect_placeholder_1();
                *(uint64_t *)(local_sp_8 + (-32L)) = 4328257UL;
                indirect_placeholder_1();
                *(uint64_t *)(local_sp_8 + (-40L)) = 4328600UL;
                indirect_placeholder_1();
                var_19 = local_sp_8 + (-48L);
                *(uint64_t *)var_19 = 4328565UL;
                indirect_placeholder_1();
                local_sp_0 = var_19;
            }
        }
        *(uint64_t *)6506752UL = 4381700UL;
        local_sp_10 = local_sp_0;
    }
    local_sp_11 = local_sp_10;
    r15_6 = r15_5;
    if (*(unsigned char *)r15_5 == '\x00') {
        return (*(unsigned char *)rbx_0 == '\x00') ? 4385276UL : rbx_0;
    }
    var_21 = ((uint64_t)(uint32_t)rax_8 == 0UL);
    while (1U)
        {
            *(uint64_t *)(local_sp_11 + (-8L)) = 4327960UL;
            indirect_placeholder_1();
            if (!var_21) {
                loop_state_var = 0U;
                break;
            }
            if (*(unsigned char *)r15_6 != '*') {
                if (*(unsigned char *)(r15_6 + 1UL) != '\x00') {
                    loop_state_var = 0U;
                    break;
                }
            }
            *(uint64_t *)(local_sp_11 + (-16L)) = 4327925UL;
            indirect_placeholder_1();
            var_22 = (rax_8 + r15_6) + 1UL;
            var_23 = local_sp_11 + (-24L);
            *(uint64_t *)var_23 = 4327938UL;
            indirect_placeholder_1();
            var_24 = (rax_8 + var_22) + 1UL;
            local_sp_11 = var_23;
            r15_6 = var_24;
            if (*(unsigned char *)var_24 == '\x00') {
                continue;
            }
            loop_state_var = 1U;
            break;
        }
    *(uint64_t *)(local_sp_11 + (-16L)) = 4327972UL;
    indirect_placeholder_1();
    rbx_0 = (rax_8 + r15_6) + 1UL;
}

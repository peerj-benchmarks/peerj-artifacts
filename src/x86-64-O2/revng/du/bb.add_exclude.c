typedef unsigned char __u_char;
typedef unsigned short __u_short;
typedef unsigned int __u_int;
typedef unsigned long __u_long;
typedef signed char __int8_t;
typedef unsigned char __uint8_t;
typedef short __int16_t;
typedef unsigned short __uint16_t;
typedef int __int32_t;
typedef unsigned int __uint32_t;
typedef long __int64_t;
typedef unsigned long __uint64_t;
typedef __int8_t __int_least8_t;
typedef __uint8_t __uint_least8_t;
typedef __int16_t __int_least16_t;
typedef __uint16_t __uint_least16_t;
typedef __int32_t __int_least32_t;
typedef __uint32_t __uint_least32_t;
typedef __int64_t __int_least64_t;
typedef __uint64_t __uint_least64_t;
typedef long __quad_t;
typedef unsigned long __u_quad_t;
typedef long __intmax_t;
typedef unsigned long __uintmax_t;
typedef unsigned long __dev_t;
typedef unsigned int __uid_t;
typedef unsigned int __gid_t;
typedef unsigned long __ino_t;
typedef unsigned long __ino64_t;
typedef unsigned int __mode_t;
typedef unsigned long __nlink_t;
typedef long __off_t;
typedef long __off64_t;
typedef int __pid_t;
typedef struct {
    int __val[2];
} __fsid_t;
typedef long __clock_t;
typedef unsigned long __rlim_t;
typedef unsigned long __rlim64_t;
typedef unsigned int __id_t;
typedef long __time_t;
typedef unsigned int __useconds_t;
typedef long __suseconds_t;
typedef int __daddr_t;
typedef int __key_t;
typedef int __clockid_t;
typedef void *__timer_t;
typedef long __blksize_t;
typedef long __blkcnt_t;
typedef long __blkcnt64_t;
typedef unsigned long __fsblkcnt_t;
typedef unsigned long __fsblkcnt64_t;
typedef unsigned long __fsfilcnt_t;
typedef unsigned long __fsfilcnt64_t;
typedef long __fsword_t;
typedef long __ssize_t;
typedef long __syscall_slong_t;
typedef unsigned long __syscall_ulong_t;
typedef __off64_t __loff_t;
typedef char *__caddr_t;
typedef long __intptr_t;
typedef unsigned int __socklen_t;
typedef int __sig_atomic_t;
typedef __int8_t int8_t;
typedef __int16_t int16_t;
typedef __int32_t int32_t;
typedef __int64_t int64_t;
typedef __uint8_t uint8_t;
typedef __uint16_t uint16_t;
typedef __uint32_t uint32_t;
typedef __uint64_t uint64_t;
typedef __int_least8_t int_least8_t;
typedef __int_least16_t int_least16_t;
typedef __int_least32_t int_least32_t;
typedef __int_least64_t int_least64_t;
typedef __uint_least8_t uint_least8_t;
typedef __uint_least16_t uint_least16_t;
typedef __uint_least32_t uint_least32_t;
typedef __uint_least64_t uint_least64_t;
typedef signed char int_fast8_t;
typedef long int_fast16_t;
typedef long int_fast32_t;
typedef long int_fast64_t;
typedef unsigned char uint_fast8_t;
typedef unsigned long uint_fast16_t;
typedef unsigned long uint_fast32_t;
typedef unsigned long uint_fast64_t;
typedef long intptr_t;
typedef unsigned long uintptr_t;
typedef __intmax_t intmax_t;
typedef __uintmax_t uintmax_t;
typedef _Bool bool;
struct indirect_placeholder_66_ret_type;
struct indirect_placeholder_67_ret_type;
struct indirect_placeholder_68_ret_type;
struct indirect_placeholder_69_ret_type;
struct indirect_placeholder_66_ret_type {
    uint64_t field_0;
    uint64_t field_1;
    uint64_t field_2;
};
struct indirect_placeholder_67_ret_type {
    uint64_t field_0;
    uint64_t field_1;
    uint64_t field_2;
};
struct indirect_placeholder_68_ret_type {
    uint64_t field_0;
    uint64_t field_1;
};
struct indirect_placeholder_69_ret_type {
    uint64_t field_0;
    uint64_t field_1;
    uint64_t field_2;
};
extern void abort(void);
extern uint64_t revng_init_local_sp(uint64_t param_0);
extern uint64_t init_rbx(void);
extern uint64_t init_rbp(void);
extern void indirect_placeholder_1(void);
extern uint64_t init_cc_src2(void);
extern uint64_t helper_cc_compute_c_wrapper(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint32_t param_3);
extern uint64_t init_r13(void);
extern uint64_t init_r12(void);
extern uint64_t init_r14(void);
extern uint64_t indirect_placeholder_8(uint64_t param_0);
extern uint64_t indirect_placeholder_17(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint64_t param_3, uint64_t param_4);
extern uint64_t init_r15(void);
extern uint64_t indirect_placeholder(uint64_t param_0, uint64_t param_1);
extern uint64_t indirect_placeholder_4(uint64_t param_0, uint64_t param_1, uint64_t param_2);
extern uint64_t init_r9(void);
extern void indirect_placeholder_2(uint64_t param_0, uint64_t param_1);
extern struct indirect_placeholder_66_ret_type indirect_placeholder_66(uint64_t param_0, uint64_t param_1);
extern struct indirect_placeholder_67_ret_type indirect_placeholder_67(uint64_t param_0);
extern struct indirect_placeholder_68_ret_type indirect_placeholder_68(uint64_t param_0, uint64_t param_1);
extern struct indirect_placeholder_69_ret_type indirect_placeholder_69(uint64_t param_0);
void bb_add_exclude(uint64_t rdi, uint64_t rsi, uint64_t rdx) {
    uint64_t var_57;
    uint64_t var_0;
    uint64_t var_1;
    uint64_t var_2;
    uint64_t var_3;
    uint64_t var_4;
    uint64_t var_5;
    uint64_t var_6;
    uint64_t var_7;
    uint64_t var_8;
    uint64_t var_9;
    uint32_t var_10;
    uint64_t var_11;
    uint64_t *var_12;
    uint64_t var_13;
    uint64_t local_sp_2;
    uint64_t rbx_1;
    uint64_t rax_0;
    uint64_t rbx_0_in;
    uint64_t rbx_0;
    uint64_t rbp_1;
    uint64_t local_sp_3;
    uint64_t rcx_0;
    uint64_t r8_0;
    uint64_t rdx3_0;
    uint64_t var_61;
    unsigned char var_62;
    uint64_t var_63;
    uint64_t var_64;
    uint64_t rax_1;
    uint64_t var_40;
    uint64_t var_41;
    uint64_t var_42;
    uint64_t var_43;
    uint64_t local_sp_0;
    uint64_t r9_0;
    uint64_t rbp_0;
    uint64_t r8_1;
    uint64_t *var_23;
    uint64_t var_24;
    uint64_t *var_25;
    bool var_26;
    uint64_t *var_27;
    uint64_t var_28;
    uint64_t rsi2_0;
    uint64_t local_sp_1;
    uint64_t rdx3_1;
    uint64_t var_34;
    uint64_t var_35;
    uint64_t var_36;
    uint64_t var_37;
    uint64_t var_38;
    uint64_t var_39;
    uint64_t var_44;
    uint64_t var_45;
    uint64_t r13_0;
    uint64_t var_46;
    uint64_t var_47;
    uint64_t *var_48;
    uint64_t var_49;
    uint64_t var_29;
    uint64_t rdx3_2;
    uint64_t rdx3_3;
    uint64_t var_30;
    struct indirect_placeholder_66_ret_type var_31;
    uint64_t var_32;
    uint64_t var_33;
    uint64_t var_17;
    struct indirect_placeholder_67_ret_type var_18;
    uint64_t var_19;
    uint64_t var_20;
    uint64_t var_21;
    uint64_t var_22;
    uint64_t var_14;
    uint64_t var_15;
    struct indirect_placeholder_68_ret_type var_16;
    uint32_t var_59;
    uint64_t var_60;
    struct indirect_placeholder_69_ret_type var_50;
    uint64_t var_51;
    uint64_t var_52;
    bool var_53;
    uint64_t var_54;
    uint64_t var_55;
    uint64_t var_56;
    uint64_t var_58;
    unsigned int loop_state_var;
    var_0 = revng_init_local_sp(0UL);
    var_1 = init_rbx();
    var_2 = init_r9();
    var_3 = init_rbp();
    var_4 = init_r13();
    var_5 = init_cc_src2();
    var_6 = init_r12();
    var_7 = init_r15();
    var_8 = init_r14();
    *(uint64_t *)(var_0 + (-8L)) = var_7;
    *(uint64_t *)(var_0 + (-16L)) = var_8;
    *(uint64_t *)(var_0 + (-24L)) = var_4;
    *(uint64_t *)(var_0 + (-32L)) = var_6;
    *(uint64_t *)(var_0 + (-40L)) = var_3;
    *(uint64_t *)(var_0 + (-48L)) = var_1;
    var_9 = var_0 + (-72L);
    var_10 = (uint32_t)rdx;
    var_11 = (uint64_t)(var_10 & 402653184U);
    var_12 = (uint64_t *)rdi;
    var_13 = *var_12;
    local_sp_2 = var_9;
    rbp_1 = var_13;
    rdx3_0 = 0UL;
    r9_0 = var_2;
    rbp_0 = var_13;
    rsi2_0 = 72UL;
    r13_0 = rsi;
    rdx3_3 = 1UL;
    if (var_11 == 0UL) {
        local_sp_3 = local_sp_2;
        if (var_13 == 0UL) {
            *(uint64_t *)(local_sp_2 + (-8L)) = 4214802UL;
            var_50 = indirect_placeholder_69(40UL);
            var_51 = var_50.field_0;
            *(uint32_t *)(var_51 + 8UL) = 0U;
            var_52 = rdx & 16UL;
            *(uint32_t *)(var_51 + 12UL) = var_10;
            var_53 = (var_52 == 0UL);
            var_54 = var_53 ? 4212912UL : 4212880UL;
            var_55 = var_53 ? 4212944UL : 4212960UL;
            var_56 = local_sp_2 + (-16L);
            *(uint64_t *)var_56 = 4214863UL;
            var_57 = indirect_placeholder_17(var_54, 0UL, 4212864UL, 0UL, var_55);
            *(uint64_t *)(var_51 + 16UL) = var_57;
            var_58 = *var_12;
            *var_12 = var_51;
            *(uint64_t *)var_51 = var_58;
            local_sp_3 = var_56;
            rbp_1 = var_51;
        } else {
            if (*(uint32_t *)(var_13 + 8UL) == 0U) {
                *(uint64_t *)(local_sp_2 + (-8L)) = 4214802UL;
                var_50 = indirect_placeholder_69(40UL);
                var_51 = var_50.field_0;
                *(uint32_t *)(var_51 + 8UL) = 0U;
                var_52 = rdx & 16UL;
                *(uint32_t *)(var_51 + 12UL) = var_10;
                var_53 = (var_52 == 0UL);
                var_54 = var_53 ? 4212912UL : 4212880UL;
                var_55 = var_53 ? 4212944UL : 4212960UL;
                var_56 = local_sp_2 + (-16L);
                *(uint64_t *)var_56 = 4214863UL;
                var_57 = indirect_placeholder_17(var_54, 0UL, 4212864UL, 0UL, var_55);
                *(uint64_t *)(var_51 + 16UL) = var_57;
                var_58 = *var_12;
                *var_12 = var_51;
                *(uint64_t *)var_51 = var_58;
                local_sp_3 = var_56;
                rbp_1 = var_51;
            } else {
                if ((uint64_t)((*(uint32_t *)(var_13 + 12UL) ^ var_10) & 1610612760U) == 0UL) {
                    *(uint64_t *)(local_sp_2 + (-8L)) = 4214802UL;
                    var_50 = indirect_placeholder_69(40UL);
                    var_51 = var_50.field_0;
                    *(uint32_t *)(var_51 + 8UL) = 0U;
                    var_52 = rdx & 16UL;
                    *(uint32_t *)(var_51 + 12UL) = var_10;
                    var_53 = (var_52 == 0UL);
                    var_54 = var_53 ? 4212912UL : 4212880UL;
                    var_55 = var_53 ? 4212944UL : 4212960UL;
                    var_56 = local_sp_2 + (-16L);
                    *(uint64_t *)var_56 = 4214863UL;
                    var_57 = indirect_placeholder_17(var_54, 0UL, 4212864UL, 0UL, var_55);
                    *(uint64_t *)(var_51 + 16UL) = var_57;
                    var_58 = *var_12;
                    *var_12 = var_51;
                    *(uint64_t *)var_51 = var_58;
                    local_sp_3 = var_56;
                    rbp_1 = var_51;
                }
            }
        }
        var_59 = var_10 & 268435458U;
        *(uint64_t *)(local_sp_3 + (-8L)) = 4214893UL;
        var_60 = indirect_placeholder_8(rsi);
        rcx_0 = var_60;
        r8_0 = var_60;
        if ((uint64_t)((var_59 + (-268435456)) & (-268435454)) == 0UL) {
            while (1U)
                {
                    if (*(unsigned char *)rcx_0 == '\\') {
                        rdx3_0 = (*(unsigned char *)(rcx_0 + 1UL) != '\x00');
                    }
                    var_61 = rdx3_0 + rcx_0;
                    var_62 = *(unsigned char *)var_61;
                    *(unsigned char *)r8_0 = var_62;
                    if (var_62 == '\x00') {
                        break;
                    }
                    rcx_0 = var_61 + 1UL;
                    r8_0 = r8_0 + 1UL;
                    continue;
                }
        }
        var_63 = *(uint64_t *)(rbp_1 + 16UL);
        *(uint64_t *)(local_sp_3 + (-16L)) = 4214916UL;
        var_64 = indirect_placeholder(var_63, var_60);
        if (var_64 == var_60) {
            return;
        }
        indirect_placeholder_1();
        return;
    }
    var_14 = (uint64_t)var_10;
    var_15 = var_0 + (-80L);
    *(uint64_t *)var_15 = 4214559UL;
    var_16 = indirect_placeholder_68(rsi, var_14);
    local_sp_0 = var_15;
    r8_1 = var_16.field_1;
    local_sp_2 = var_15;
    if ((uint64_t)(unsigned char)var_16.field_0 == 0UL) {
        return;
    }
    if (var_13 == 0UL) {
        var_17 = var_0 + (-88L);
        *(uint64_t *)var_17 = 4214592UL;
        var_18 = indirect_placeholder_67(40UL);
        var_19 = var_18.field_0;
        var_20 = var_18.field_1;
        var_21 = var_18.field_2;
        var_22 = *var_12;
        *(uint32_t *)(var_19 + 8UL) = 1U;
        *(uint32_t *)(var_19 + 12UL) = var_10;
        *var_12 = var_19;
        *(uint64_t *)var_19 = var_22;
        local_sp_0 = var_17;
        r9_0 = var_20;
        rbp_0 = var_19;
        r8_1 = var_21;
    } else {
        if (*(uint32_t *)(var_13 + 8UL) == 1U) {
            var_17 = var_0 + (-88L);
            *(uint64_t *)var_17 = 4214592UL;
            var_18 = indirect_placeholder_67(40UL);
            var_19 = var_18.field_0;
            var_20 = var_18.field_1;
            var_21 = var_18.field_2;
            var_22 = *var_12;
            *(uint32_t *)(var_19 + 8UL) = 1U;
            *(uint32_t *)(var_19 + 12UL) = var_10;
            *var_12 = var_19;
            *(uint64_t *)var_19 = var_22;
            local_sp_0 = var_17;
            r9_0 = var_20;
            rbp_0 = var_19;
            r8_1 = var_21;
        } else {
            if ((uint64_t)((*(uint32_t *)(var_13 + 12UL) ^ var_10) & 536870912U) == 0UL) {
                var_17 = var_0 + (-88L);
                *(uint64_t *)var_17 = 4214592UL;
                var_18 = indirect_placeholder_67(40UL);
                var_19 = var_18.field_0;
                var_20 = var_18.field_1;
                var_21 = var_18.field_2;
                var_22 = *var_12;
                *(uint32_t *)(var_19 + 8UL) = 1U;
                *(uint32_t *)(var_19 + 12UL) = var_10;
                *var_12 = var_19;
                *(uint64_t *)var_19 = var_22;
                local_sp_0 = var_17;
                r9_0 = var_20;
                rbp_0 = var_19;
                r8_1 = var_21;
            }
        }
    }
    var_23 = (uint64_t *)(rbp_0 + 32UL);
    var_24 = *var_23;
    var_25 = (uint64_t *)(rbp_0 + 24UL);
    var_26 = (var_24 == *var_25);
    var_27 = (uint64_t *)(rbp_0 + 16UL);
    var_28 = *var_27;
    local_sp_1 = local_sp_0;
    rax_1 = var_28;
    rdx3_1 = var_24;
    rdx3_2 = var_24;
    if (!var_26) {
        if (var_28 == 0UL) {
            if (var_24 != 0UL) {
                if (var_24 <= 128102389400760775UL) {
                    *(uint64_t *)(local_sp_0 + (-8L)) = 4215350UL;
                    indirect_placeholder_2(r9_0, r8_1);
                    abort();
                }
                rsi2_0 = rdx3_2 * 72UL;
                rdx3_3 = rdx3_2;
            }
        } else {
            if (var_24 > 85401592933840515UL) {
                *(uint64_t *)(local_sp_0 + (-8L)) = 4215350UL;
                indirect_placeholder_2(r9_0, r8_1);
                abort();
            }
            var_29 = (var_24 + (var_24 >> 1UL)) + 1UL;
            rdx3_2 = var_29;
            rsi2_0 = rdx3_2 * 72UL;
            rdx3_3 = rdx3_2;
        }
        *var_25 = rdx3_3;
        var_30 = local_sp_0 + (-8L);
        *(uint64_t *)var_30 = 4215100UL;
        var_31 = indirect_placeholder_66(var_28, rsi2_0);
        var_32 = var_31.field_0;
        var_33 = *var_23;
        *var_27 = var_32;
        local_sp_1 = var_30;
        rax_1 = var_32;
        rdx3_1 = var_33;
    }
    var_34 = rdx3_1 + 1UL;
    var_35 = (uint64_t)(var_10 & 134217728U);
    var_36 = (rdx3_1 * 72UL) + rax_1;
    *var_23 = var_34;
    *(uint32_t *)var_36 = var_10;
    if (var_35 == 0UL) {
        if ((uint64_t)(var_10 & 67108864U) != 0UL) {
            *(uint64_t *)(local_sp_1 + (-8L)) = 4215160UL;
            var_46 = indirect_placeholder_8(rsi);
            *(uint64_t *)(local_sp_1 + (-16L)) = 4215173UL;
            var_47 = indirect_placeholder_8(16UL);
            var_48 = (uint64_t *)(rdi + 8UL);
            var_49 = *var_48;
            *(uint64_t *)(var_47 + 8UL) = var_46;
            *var_48 = var_47;
            *(uint64_t *)var_47 = var_49;
            r13_0 = var_46;
        }
        *(uint64_t *)(var_36 + 8UL) = r13_0;
        return;
    }
    var_37 = rdx & 16UL;
    var_38 = helper_cc_compute_c_wrapper(var_37 + (-1L), 1UL, var_5, 16U);
    var_39 = (uint64_t)(((0U - (uint32_t)var_38) & (-2)) + 11U);
    rbx_0_in = var_37;
    rbx_1 = var_37;
    if ((rdx & 8UL) == 0UL) {
        var_44 = var_36 + 8UL;
        *(uint64_t *)(local_sp_1 + (-8L)) = 4215231UL;
        var_45 = indirect_placeholder_4(var_44, rsi, var_39);
        rax_0 = var_45;
    } else {
        *(uint64_t *)(local_sp_1 + (-8L)) = 4214701UL;
        indirect_placeholder_1();
        if (var_37 != 0UL) {
            *var_23 = (*var_23 + (-1L));
            return;
        }
        if (*(unsigned char *)((var_37 + rsi) + (-1L)) != '/') {
            while (1U)
                {
                    rbx_0 = rbx_0_in + (-1L);
                    rbx_0_in = rbx_0;
                    rbx_1 = rbx_0;
                    if (rbx_0 != 0UL) {
                        loop_state_var = 1U;
                        break;
                    }
                    if (*(unsigned char *)((rbx_0 + rsi) + (-1L)) == '/') {
                        continue;
                    }
                    loop_state_var = 0U;
                    break;
                }
            switch (loop_state_var) {
              case 0U:
                {
                    break;
                }
                break;
              case 1U:
                {
                    *var_23 = (*var_23 + (-1L));
                    return;
                }
                break;
            }
        }
        var_40 = rbx_1 + 7UL;
        *(uint64_t *)(local_sp_1 + (-16L)) = 4215257UL;
        var_41 = indirect_placeholder_8(var_40);
        *(uint64_t *)(local_sp_1 + (-24L)) = 4215274UL;
        indirect_placeholder_1();
        *(uint64_t *)(local_sp_1 + (-32L)) = 4215288UL;
        indirect_placeholder_1();
        var_42 = var_36 + 8UL;
        *(uint64_t *)(local_sp_1 + (-40L)) = 4215303UL;
        var_43 = indirect_placeholder_4(var_42, var_41, var_39);
        *(uint32_t *)(local_sp_1 + (-28L)) = (uint32_t)var_43;
        *(uint64_t *)(local_sp_1 + (-48L)) = 4215315UL;
        indirect_placeholder_1();
        rax_0 = (uint64_t)*(uint32_t *)(local_sp_1 + (-36L));
    }
}

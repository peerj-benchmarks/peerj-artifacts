typedef unsigned char __u_char;
typedef unsigned short __u_short;
typedef unsigned int __u_int;
typedef unsigned long __u_long;
typedef signed char __int8_t;
typedef unsigned char __uint8_t;
typedef short __int16_t;
typedef unsigned short __uint16_t;
typedef int __int32_t;
typedef unsigned int __uint32_t;
typedef long __int64_t;
typedef unsigned long __uint64_t;
typedef __int8_t __int_least8_t;
typedef __uint8_t __uint_least8_t;
typedef __int16_t __int_least16_t;
typedef __uint16_t __uint_least16_t;
typedef __int32_t __int_least32_t;
typedef __uint32_t __uint_least32_t;
typedef __int64_t __int_least64_t;
typedef __uint64_t __uint_least64_t;
typedef long __quad_t;
typedef unsigned long __u_quad_t;
typedef long __intmax_t;
typedef unsigned long __uintmax_t;
typedef unsigned long __dev_t;
typedef unsigned int __uid_t;
typedef unsigned int __gid_t;
typedef unsigned long __ino_t;
typedef unsigned long __ino64_t;
typedef unsigned int __mode_t;
typedef unsigned long __nlink_t;
typedef long __off_t;
typedef long __off64_t;
typedef int __pid_t;
typedef struct {
    int __val[2];
} __fsid_t;
typedef long __clock_t;
typedef unsigned long __rlim_t;
typedef unsigned long __rlim64_t;
typedef unsigned int __id_t;
typedef long __time_t;
typedef unsigned int __useconds_t;
typedef long __suseconds_t;
typedef int __daddr_t;
typedef int __key_t;
typedef int __clockid_t;
typedef void *__timer_t;
typedef long __blksize_t;
typedef long __blkcnt_t;
typedef long __blkcnt64_t;
typedef unsigned long __fsblkcnt_t;
typedef unsigned long __fsblkcnt64_t;
typedef unsigned long __fsfilcnt_t;
typedef unsigned long __fsfilcnt64_t;
typedef long __fsword_t;
typedef long __ssize_t;
typedef long __syscall_slong_t;
typedef unsigned long __syscall_ulong_t;
typedef __off64_t __loff_t;
typedef char *__caddr_t;
typedef long __intptr_t;
typedef unsigned int __socklen_t;
typedef int __sig_atomic_t;
typedef __int8_t int8_t;
typedef __int16_t int16_t;
typedef __int32_t int32_t;
typedef __int64_t int64_t;
typedef __uint8_t uint8_t;
typedef __uint16_t uint16_t;
typedef __uint32_t uint32_t;
typedef __uint64_t uint64_t;
typedef __int_least8_t int_least8_t;
typedef __int_least16_t int_least16_t;
typedef __int_least32_t int_least32_t;
typedef __int_least64_t int_least64_t;
typedef __uint_least8_t uint_least8_t;
typedef __uint_least16_t uint_least16_t;
typedef __uint_least32_t uint_least32_t;
typedef __uint_least64_t uint_least64_t;
typedef signed char int_fast8_t;
typedef long int_fast16_t;
typedef long int_fast32_t;
typedef long int_fast64_t;
typedef unsigned char uint_fast8_t;
typedef unsigned long uint_fast16_t;
typedef unsigned long uint_fast32_t;
typedef unsigned long uint_fast64_t;
typedef long intptr_t;
typedef unsigned long uintptr_t;
typedef __intmax_t intmax_t;
typedef __uintmax_t uintmax_t;
typedef _Bool bool;
extern uint64_t revng_init_local_sp(uint64_t param_0);
extern uint64_t init_rbx(void);
extern uint64_t init_rbp(void);
extern void indirect_placeholder(void);
extern uint64_t init_cc_src2(void);
extern uint64_t helper_cc_compute_c_wrapper(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint32_t param_3);
extern uint64_t init_r13(void);
extern uint64_t init_r12(void);
extern uint64_t helper_cc_compute_all_wrapper(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint32_t param_3);
uint64_t bb_gcd2_odd(uint64_t rcx, uint64_t rdi, uint64_t r8, uint64_t rsi, uint64_t rdx) {
    uint64_t var_20;
    uint64_t r12_1;
    uint64_t var_21;
    uint64_t var_22;
    uint64_t var_23;
    uint64_t var_15;
    uint64_t var_16;
    uint64_t rbx_3;
    uint64_t rbp_3;
    uint64_t var_17;
    uint64_t var_18;
    uint64_t var_19;
    uint64_t var_0;
    uint64_t var_1;
    uint64_t var_2;
    uint64_t var_3;
    uint64_t var_4;
    uint64_t var_5;
    uint64_t r83_0;
    uint64_t storemerge;
    uint64_t rbx_2;
    uint64_t rbx_0;
    uint64_t rbp_0;
    uint64_t rbp_2;
    uint64_t r83_2;
    uint64_t rbx_1;
    uint64_t cc_src2_1;
    uint64_t rbp_1;
    uint64_t r83_1;
    uint64_t cc_src2_0;
    uint64_t r12_0;
    uint64_t var_7;
    uint64_t var_8;
    uint64_t var_9;
    uint64_t var_10;
    uint64_t var_11;
    uint64_t var_12;
    bool var_13;
    bool var_14;
    uint64_t var_6;
    unsigned int loop_state_var;
    bool switch_state_var;
    var_0 = revng_init_local_sp(0UL);
    var_1 = init_rbx();
    var_2 = init_rbp();
    var_3 = init_r13();
    var_4 = init_cc_src2();
    var_5 = init_r12();
    *(uint64_t *)(var_0 + (-8L)) = var_3;
    *(uint64_t *)(var_0 + (-16L)) = var_5;
    *(uint64_t *)(var_0 + (-24L)) = var_2;
    *(uint64_t *)(var_0 + (-32L)) = var_1;
    r83_0 = r8;
    rbx_0 = rdx;
    rbp_0 = rsi;
    cc_src2_0 = var_4;
    r12_0 = rcx;
    if ((r8 & 1UL) == 0UL) {
        var_6 = var_0 + (-56L);
        *(uint64_t *)(var_0 + (-48L)) = r8;
        *(uint64_t *)(var_0 + (-64L)) = 4204611UL;
        indirect_placeholder();
        r83_0 = *(uint64_t *)var_6;
    }
    r83_1 = r83_0;
    storemerge = r83_0;
    if ((rdx | rsi) != 0UL) {
        rbx_1 = rbx_0;
        rbp_1 = rbp_0;
        while ((rbx_0 & 1UL) != 0UL)
            {
                rbx_0 = (rbx_0 >> 1UL) | (rbp_0 << 63UL);
                rbp_0 = rbp_0 >> 1UL;
                rbx_1 = rbx_0;
                rbp_1 = rbp_0;
            }
        while (1U)
            {
                rbx_2 = rbx_1;
                rbp_2 = rbp_1;
                cc_src2_1 = cc_src2_0;
                if ((rbp_1 | r12_0) != 0UL) {
                    loop_state_var = 1U;
                    break;
                }
                while (1U)
                    {
                        var_7 = r12_0 - rbp_2;
                        var_8 = helper_cc_compute_c_wrapper(var_7, rbp_2, cc_src2_1, 17U);
                        rbx_1 = rbx_2;
                        rbp_1 = rbp_2;
                        storemerge = rbx_2;
                        if (var_8 != 0UL) {
                            var_9 = helper_cc_compute_all_wrapper(var_7, rbp_2, cc_src2_1, 17U);
                            var_10 = (var_9 >> 6UL) & 1UL;
                            var_11 = r83_1 - rbx_2;
                            var_12 = helper_cc_compute_c_wrapper(var_11, rbx_2, cc_src2_1, 17U);
                            var_13 = (var_12 == 0UL);
                            var_14 = (var_10 == 0UL);
                            r83_2 = var_11;
                            if (!(var_13 || var_14)) {
                                loop_state_var = 1U;
                                break;
                            }
                        }
                        var_15 = rbx_2 - r83_1;
                        var_16 = helper_cc_compute_c_wrapper(var_15, r83_1, cc_src2_1, 17U);
                        cc_src2_1 = var_16;
                        rbx_3 = var_15;
                        rbp_3 = (rbp_2 - r12_0) - var_16;
                        var_17 = rbx_3 >> 1UL;
                        var_18 = rbp_3 >> 1UL;
                        var_19 = var_17 | (rbp_3 << 63UL);
                        rbx_2 = var_19;
                        rbp_2 = var_18;
                        rbx_3 = var_19;
                        rbp_3 = var_18;
                        do {
                            var_17 = rbx_3 >> 1UL;
                            var_18 = rbp_3 >> 1UL;
                            var_19 = var_17 | (rbp_3 << 63UL);
                            rbx_2 = var_19;
                            rbp_2 = var_18;
                            rbx_3 = var_19;
                            rbp_3 = var_18;
                        } while ((rbx_3 & 2UL) != 0UL);
                        if ((var_18 | r12_0) == 0UL) {
                            continue;
                        }
                        loop_state_var = 0U;
                        break;
                    }
                switch_state_var = 0;
                switch (loop_state_var) {
                  case 1U:
                    {
                        if (r12_0 > rbp_2) {
                            var_20 = helper_cc_compute_c_wrapper(var_11, rbx_2, cc_src2_1, 17U);
                            cc_src2_0 = var_20;
                            r12_1 = var_7 - var_20;
                            var_21 = r83_2 >> 1UL;
                            var_22 = r12_1 >> 1UL;
                            var_23 = var_21 | (r12_1 << 63UL);
                            r83_1 = var_23;
                            r12_0 = var_22;
                            r83_2 = var_23;
                            r12_1 = var_22;
                            do {
                                var_21 = r83_2 >> 1UL;
                                var_22 = r12_1 >> 1UL;
                                var_23 = var_21 | (r12_1 << 63UL);
                                r83_1 = var_23;
                                r12_0 = var_22;
                                r83_2 = var_23;
                                r12_1 = var_22;
                            } while ((r83_2 & 2UL) != 0UL);
                            continue;
                        }
                        if (!((r83_1 <= rbx_2) || var_14)) {
                            *(uint64_t *)rdi = rbp_2;
                            loop_state_var = 0U;
                            switch_state_var = 1;
                            break;
                        }
                    }
                    break;
                  case 0U:
                    {
                        loop_state_var = 1U;
                        switch_state_var = 1;
                        break;
                    }
                    break;
                }
                if (switch_state_var)
                    break;
            }
        switch (loop_state_var) {
          case 0U:
            {
                break;
            }
            break;
          case 1U:
            {
                *(uint64_t *)rdi = 0UL;
                indirect_placeholder();
                return 0UL;
            }
            break;
        }
    }
    *(uint64_t *)rdi = rcx;
}

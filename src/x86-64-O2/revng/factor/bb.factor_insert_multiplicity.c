typedef unsigned char __u_char;
typedef unsigned short __u_short;
typedef unsigned int __u_int;
typedef unsigned long __u_long;
typedef signed char __int8_t;
typedef unsigned char __uint8_t;
typedef short __int16_t;
typedef unsigned short __uint16_t;
typedef int __int32_t;
typedef unsigned int __uint32_t;
typedef long __int64_t;
typedef unsigned long __uint64_t;
typedef __int8_t __int_least8_t;
typedef __uint8_t __uint_least8_t;
typedef __int16_t __int_least16_t;
typedef __uint16_t __uint_least16_t;
typedef __int32_t __int_least32_t;
typedef __uint32_t __uint_least32_t;
typedef __int64_t __int_least64_t;
typedef __uint64_t __uint_least64_t;
typedef long __quad_t;
typedef unsigned long __u_quad_t;
typedef long __intmax_t;
typedef unsigned long __uintmax_t;
typedef unsigned long __dev_t;
typedef unsigned int __uid_t;
typedef unsigned int __gid_t;
typedef unsigned long __ino_t;
typedef unsigned long __ino64_t;
typedef unsigned int __mode_t;
typedef unsigned long __nlink_t;
typedef long __off_t;
typedef long __off64_t;
typedef int __pid_t;
typedef struct {
    int __val[2];
} __fsid_t;
typedef long __clock_t;
typedef unsigned long __rlim_t;
typedef unsigned long __rlim64_t;
typedef unsigned int __id_t;
typedef long __time_t;
typedef unsigned int __useconds_t;
typedef long __suseconds_t;
typedef int __daddr_t;
typedef int __key_t;
typedef int __clockid_t;
typedef void *__timer_t;
typedef long __blksize_t;
typedef long __blkcnt_t;
typedef long __blkcnt64_t;
typedef unsigned long __fsblkcnt_t;
typedef unsigned long __fsblkcnt64_t;
typedef unsigned long __fsfilcnt_t;
typedef unsigned long __fsfilcnt64_t;
typedef long __fsword_t;
typedef long __ssize_t;
typedef long __syscall_slong_t;
typedef unsigned long __syscall_ulong_t;
typedef __off64_t __loff_t;
typedef char *__caddr_t;
typedef long __intptr_t;
typedef unsigned int __socklen_t;
typedef int __sig_atomic_t;
typedef __int8_t int8_t;
typedef __int16_t int16_t;
typedef __int32_t int32_t;
typedef __int64_t int64_t;
typedef __uint8_t uint8_t;
typedef __uint16_t uint16_t;
typedef __uint32_t uint32_t;
typedef __uint64_t uint64_t;
typedef __int_least8_t int_least8_t;
typedef __int_least16_t int_least16_t;
typedef __int_least32_t int_least32_t;
typedef __int_least64_t int_least64_t;
typedef __uint_least8_t uint_least8_t;
typedef __uint_least16_t uint_least16_t;
typedef __uint_least32_t uint_least32_t;
typedef __uint_least64_t uint_least64_t;
typedef signed char int_fast8_t;
typedef long int_fast16_t;
typedef long int_fast32_t;
typedef long int_fast64_t;
typedef unsigned char uint_fast8_t;
typedef unsigned long uint_fast16_t;
typedef unsigned long uint_fast32_t;
typedef unsigned long uint_fast64_t;
typedef long intptr_t;
typedef unsigned long uintptr_t;
typedef __intmax_t intmax_t;
typedef __uintmax_t uintmax_t;
struct bb_factor_insert_multiplicity_ret_type;
struct bb_factor_insert_multiplicity_ret_type {
    uint64_t field_0;
    uint64_t field_1;
    uint64_t field_2;
    uint64_t field_3;
};
extern uint64_t revng_init_local_sp(uint64_t param_0);
extern uint64_t init_rbx(void);
extern uint64_t init_rbp(void);
extern uint64_t init_cc_src2(void);
extern uint64_t helper_cc_compute_c_wrapper(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint32_t param_3);
extern uint64_t init_r13(void);
extern uint64_t init_r14(void);
extern uint64_t init_r12(void);
extern uint64_t helper_cc_compute_all_wrapper(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint32_t param_3);
extern uint64_t init_r8(void);
extern uint64_t init_rcx(void);
struct bb_factor_insert_multiplicity_ret_type bb_factor_insert_multiplicity(uint64_t rdi, uint64_t rsi, uint64_t rdx) {
    uint64_t r9_1;
    uint64_t var_0;
    uint64_t var_1;
    uint64_t var_2;
    uint64_t var_3;
    uint64_t var_4;
    uint64_t var_5;
    uint64_t var_6;
    uint64_t var_7;
    uint64_t var_8;
    unsigned char *var_9;
    unsigned char var_10;
    uint64_t var_11;
    uint64_t var_12;
    uint64_t var_13;
    uint64_t var_14;
    uint64_t var_15;
    uint64_t r8_2;
    uint64_t var_16;
    uint64_t var_17;
    uint64_t var_18;
    uint64_t var_19;
    uint64_t var_20;
    uint64_t var_21;
    uint64_t var_22;
    uint64_t r9_0;
    uint64_t rcx_0;
    uint64_t var_23;
    uint64_t var_24;
    uint64_t var_25;
    uint64_t var_26;
    uint64_t r8_1;
    uint64_t var_27;
    uint64_t var_28;
    uint64_t r8_0;
    uint64_t r13_0;
    uint64_t var_29;
    uint64_t r10_0;
    uint64_t rcx_1;
    uint64_t rax_0;
    uint64_t var_30;
    uint64_t var_31;
    uint64_t var_32;
    uint64_t var_33;
    uint64_t rcx_2;
    unsigned char *var_34;
    struct bb_factor_insert_multiplicity_ret_type mrv;
    struct bb_factor_insert_multiplicity_ret_type mrv1;
    struct bb_factor_insert_multiplicity_ret_type mrv2;
    struct bb_factor_insert_multiplicity_ret_type mrv3;
    uint64_t r13_1;
    uint64_t rcx_3;
    unsigned char var_35;
    struct bb_factor_insert_multiplicity_ret_type mrv4;
    struct bb_factor_insert_multiplicity_ret_type mrv5;
    struct bb_factor_insert_multiplicity_ret_type mrv6;
    struct bb_factor_insert_multiplicity_ret_type mrv7;
    unsigned int loop_state_var;
    var_0 = revng_init_local_sp(0UL);
    var_1 = init_rcx();
    var_2 = init_rbx();
    var_3 = init_rbp();
    var_4 = init_r8();
    var_5 = init_r13();
    var_6 = init_cc_src2();
    var_7 = init_r14();
    var_8 = init_r12();
    *(uint64_t *)(var_0 + (-8L)) = var_7;
    var_9 = (unsigned char *)(rdi + 250UL);
    var_10 = *var_9;
    var_11 = (uint64_t)var_10;
    var_12 = rdi + 224UL;
    *(uint64_t *)(var_0 + (-16L)) = var_5;
    *(uint64_t *)(var_0 + (-24L)) = var_8;
    *(uint64_t *)(var_0 + (-32L)) = var_3;
    *(uint64_t *)(var_0 + (-40L)) = var_2;
    var_13 = rdi + 16UL;
    var_14 = var_11 + 4294967295UL;
    var_15 = (uint64_t)(uint32_t)var_14;
    r9_1 = 4294967295UL;
    r8_2 = 0UL;
    r9_0 = var_15;
    r8_1 = var_4;
    r8_0 = 0UL;
    r13_0 = 0UL;
    r10_0 = var_15;
    r13_1 = 0UL;
    rcx_3 = var_1;
    if (var_10 == '\x00') {
        var_35 = var_10 + '\x01';
        *(uint64_t *)((r13_1 + rdi) + 16UL) = rsi;
        *(unsigned char *)((r8_2 + rdi) + 224UL) = (unsigned char)rdx;
        *var_9 = var_35;
        mrv4.field_0 = rcx_3;
        mrv5 = mrv4;
        mrv5.field_1 = rdi;
        mrv6 = mrv5;
        mrv6.field_2 = r8_2;
        mrv7 = mrv6;
        mrv7.field_3 = rsi;
        return mrv7;
    }
    var_16 = var_14 << 32UL;
    var_17 = (uint64_t)((long)var_16 >> (long)32UL);
    var_18 = (uint64_t)((long)var_16 >> (long)29UL);
    var_19 = *(uint64_t *)((var_18 + rdi) + 16UL);
    var_20 = rsi - var_19;
    var_21 = helper_cc_compute_c_wrapper(var_20, var_19, var_6, 17U);
    rcx_2 = var_17;
    if (var_21 != 0UL) {
        var_22 = var_17 + (var_15 ^ (-1L));
        rcx_0 = var_17 + (-1L);
        while (1U)
            {
                var_23 = r9_0 + 4294967295UL;
                var_24 = (uint64_t)(uint32_t)var_23;
                r9_0 = var_24;
                rcx_2 = rcx_0;
                rcx_3 = rcx_0;
                if (rcx_0 != var_22) {
                    loop_state_var = 0U;
                    break;
                }
                var_25 = rcx_0 + (-1L);
                var_26 = *(uint64_t *)(((var_25 << 3UL) + var_13) + 8UL);
                rcx_0 = var_25;
                r9_1 = var_24;
                r8_1 = var_25;
                if (var_26 > rsi) {
                    continue;
                }
                if (var_26 != rsi) {
                    loop_state_var = 1U;
                    break;
                }
                var_27 = rcx_0 + 1UL;
                var_28 = var_27 << 3UL;
                r8_0 = var_27;
                r13_0 = var_28;
                r8_2 = var_27;
                r13_1 = var_28;
                if ((long)var_16 <= (long)(var_23 << 32UL)) {
                    loop_state_var = 2U;
                    break;
                }
                loop_state_var = 0U;
                break;
            }
        switch (loop_state_var) {
          case 1U:
            {
                var_34 = (unsigned char *)(rcx_2 + var_12);
                *var_34 = (*var_34 + (unsigned char)rdx);
                mrv.field_0 = rcx_2;
                mrv1 = mrv;
                mrv1.field_1 = rdi;
                mrv2 = mrv1;
                mrv2.field_2 = r8_1;
                mrv3 = mrv2;
                mrv3.field_3 = rsi;
                return mrv3;
            }
            break;
          case 2U:
          case 0U:
            {
                var_29 = r9_1 << 32UL;
                rcx_1 = var_18 + var_13;
                rax_0 = var_12 + var_17;
                r8_2 = r8_0;
                r13_1 = r13_0;
                var_30 = *(uint64_t *)rcx_1;
                var_31 = r10_0 + 4294967295UL;
                var_32 = rcx_1 + (-8L);
                *(uint64_t *)(rcx_1 + 8UL) = var_30;
                *(unsigned char *)(rax_0 + 1UL) = *(unsigned char *)rax_0;
                rcx_1 = var_32;
                rcx_3 = var_32;
                while ((long)(var_31 << 32UL) <= (long)var_29)
                    {
                        r10_0 = (uint64_t)(uint32_t)var_31;
                        rax_0 = rax_0 + (-1L);
                        var_30 = *(uint64_t *)rcx_1;
                        var_31 = r10_0 + 4294967295UL;
                        var_32 = rcx_1 + (-8L);
                        *(uint64_t *)(rcx_1 + 8UL) = var_30;
                        *(unsigned char *)(rax_0 + 1UL) = *(unsigned char *)rax_0;
                        rcx_1 = var_32;
                        rcx_3 = var_32;
                    }
            }
            break;
        }
    }
    var_33 = helper_cc_compute_all_wrapper(var_20, var_19, var_6, 17U);
    if ((var_33 & 64UL) != 0UL) {
        var_34 = (unsigned char *)(rcx_2 + var_12);
        *var_34 = (*var_34 + (unsigned char)rdx);
        mrv.field_0 = rcx_2;
        mrv1 = mrv;
        mrv1.field_1 = rdi;
        mrv2 = mrv1;
        mrv2.field_2 = r8_1;
        mrv3 = mrv2;
        mrv3.field_3 = rsi;
        return mrv3;
    }
    r8_2 = var_17 + 1UL;
    r13_1 = var_18 + 8UL;
}

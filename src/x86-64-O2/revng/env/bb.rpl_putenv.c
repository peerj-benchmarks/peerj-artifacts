typedef unsigned char __u_char;
typedef unsigned short __u_short;
typedef unsigned int __u_int;
typedef unsigned long __u_long;
typedef signed char __int8_t;
typedef unsigned char __uint8_t;
typedef short __int16_t;
typedef unsigned short __uint16_t;
typedef int __int32_t;
typedef unsigned int __uint32_t;
typedef long __int64_t;
typedef unsigned long __uint64_t;
typedef __int8_t __int_least8_t;
typedef __uint8_t __uint_least8_t;
typedef __int16_t __int_least16_t;
typedef __uint16_t __uint_least16_t;
typedef __int32_t __int_least32_t;
typedef __uint32_t __uint_least32_t;
typedef __int64_t __int_least64_t;
typedef __uint64_t __uint_least64_t;
typedef long __quad_t;
typedef unsigned long __u_quad_t;
typedef long __intmax_t;
typedef unsigned long __uintmax_t;
typedef unsigned long __dev_t;
typedef unsigned int __uid_t;
typedef unsigned int __gid_t;
typedef unsigned long __ino_t;
typedef unsigned long __ino64_t;
typedef unsigned int __mode_t;
typedef unsigned long __nlink_t;
typedef long __off_t;
typedef long __off64_t;
typedef int __pid_t;
typedef struct {
    int __val[2];
} __fsid_t;
typedef long __clock_t;
typedef unsigned long __rlim_t;
typedef unsigned long __rlim64_t;
typedef unsigned int __id_t;
typedef long __time_t;
typedef unsigned int __useconds_t;
typedef long __suseconds_t;
typedef int __daddr_t;
typedef int __key_t;
typedef int __clockid_t;
typedef void *__timer_t;
typedef long __blksize_t;
typedef long __blkcnt_t;
typedef long __blkcnt64_t;
typedef unsigned long __fsblkcnt_t;
typedef unsigned long __fsblkcnt64_t;
typedef unsigned long __fsfilcnt_t;
typedef unsigned long __fsfilcnt64_t;
typedef long __fsword_t;
typedef long __ssize_t;
typedef long __syscall_slong_t;
typedef unsigned long __syscall_ulong_t;
typedef __off64_t __loff_t;
typedef char *__caddr_t;
typedef long __intptr_t;
typedef unsigned int __socklen_t;
typedef int __sig_atomic_t;
typedef __int8_t int8_t;
typedef __int16_t int16_t;
typedef __int32_t int32_t;
typedef __int64_t int64_t;
typedef __uint8_t uint8_t;
typedef __uint16_t uint16_t;
typedef __uint32_t uint32_t;
typedef __uint64_t uint64_t;
typedef __int_least8_t int_least8_t;
typedef __int_least16_t int_least16_t;
typedef __int_least32_t int_least32_t;
typedef __int_least64_t int_least64_t;
typedef __uint_least8_t uint_least8_t;
typedef __uint_least16_t uint_least16_t;
typedef __uint_least32_t uint_least32_t;
typedef __uint_least64_t uint_least64_t;
typedef signed char int_fast8_t;
typedef long int_fast16_t;
typedef long int_fast32_t;
typedef long int_fast64_t;
typedef unsigned char uint_fast8_t;
typedef unsigned long uint_fast16_t;
typedef unsigned long uint_fast32_t;
typedef unsigned long uint_fast64_t;
typedef long intptr_t;
typedef unsigned long uintptr_t;
typedef __intmax_t intmax_t;
typedef __uintmax_t uintmax_t;
typedef _Bool bool;
struct bb_rpl_putenv_ret_type;
struct indirect_placeholder_29_ret_type;
struct bb_rpl_putenv_ret_type {
    uint64_t field_0;
    uint64_t field_1;
};
struct indirect_placeholder_29_ret_type {
    uint64_t field_0;
    uint64_t field_1;
};
extern uint64_t revng_init_local_sp(uint64_t param_0);
extern uint64_t init_rbx(void);
extern uint64_t init_rbp(void);
extern void indirect_placeholder(void);
extern uint64_t init_r12(void);
extern uint64_t init_rax(void);
extern uint64_t init_rcx(void);
extern struct indirect_placeholder_29_ret_type indirect_placeholder_29(uint64_t param_0);
struct bb_rpl_putenv_ret_type bb_rpl_putenv(uint64_t rdi) {
    uint64_t var_0;
    uint64_t var_1;
    uint64_t var_2;
    uint64_t var_3;
    uint64_t var_4;
    uint64_t var_5;
    uint64_t var_6;
    uint64_t var_20;
    uint64_t rcx_0;
    uint64_t rbx_1;
    uint64_t var_22;
    uint64_t var_23;
    uint64_t local_sp_1;
    uint64_t rax_0;
    uint64_t var_21;
    uint64_t *var_11;
    uint64_t local_sp_0;
    uint64_t rax_1;
    uint64_t rcx_1;
    uint64_t rbx_0;
    uint64_t rax_2;
    uint64_t var_19;
    uint64_t var_18;
    uint64_t rdi4_0;
    uint64_t rcx_3;
    struct bb_rpl_putenv_ret_type mrv1 = {0UL, /*implicit*/(int)0};
    uint64_t var_12;
    uint64_t rbx_2;
    uint64_t local_sp_3;
    uint64_t rax_3;
    struct bb_rpl_putenv_ret_type mrv2;
    struct bb_rpl_putenv_ret_type mrv3;
    uint64_t var_13;
    uint64_t local_sp_2;
    uint64_t var_7;
    uint64_t var_8;
    bool var_9;
    uint64_t rbx_3;
    uint64_t var_10;
    struct indirect_placeholder_29_ret_type var_14;
    uint64_t var_15;
    bool var_16;
    uint64_t *var_17;
    unsigned int loop_state_var;
    bool switch_state_var;
    var_0 = revng_init_local_sp(0UL);
    var_1 = init_rax();
    var_2 = init_rcx();
    var_3 = init_rbx();
    var_4 = init_rbp();
    var_5 = init_r12();
    *(uint64_t *)(var_0 + (-8L)) = var_5;
    *(uint64_t *)(var_0 + (-16L)) = var_4;
    *(uint64_t *)(var_0 + (-24L)) = var_3;
    var_6 = var_0 + (-32L);
    *(uint64_t *)var_6 = 4212561UL;
    indirect_placeholder();
    rax_1 = 0UL;
    rcx_1 = var_2;
    rdi4_0 = 16UL;
    rcx_3 = var_2;
    local_sp_3 = var_6;
    rax_3 = 4294967295UL;
    local_sp_2 = var_6;
    if (var_1 == 0UL) {
        var_16 = (*(unsigned char *)rdi == '\x00');
        var_17 = (uint64_t *)(var_0 + (-40L));
        if (!var_16) {
            *var_17 = 4212779UL;
            indirect_placeholder();
            var_18 = var_0 + (-48L);
            *(uint64_t *)var_18 = 4212792UL;
            indirect_placeholder();
            local_sp_0 = var_18;
            rbx_0 = *(uint64_t *)6354304UL;
            local_sp_1 = local_sp_0;
            rax_2 = rax_1;
            rbx_1 = rbx_0;
            rcx_3 = rcx_1;
            while (*(uint64_t *)rbx_0 != 0UL)
                {
                    while (1U)
                        {
                            var_19 = local_sp_1 + (-8L);
                            *(uint64_t *)var_19 = 4212821UL;
                            indirect_placeholder();
                            rcx_0 = rbx_1;
                            local_sp_1 = var_19;
                            rax_0 = rax_2;
                            local_sp_0 = var_19;
                            rbx_0 = rbx_1;
                            if ((uint64_t)(uint32_t)rax_2 != 0UL) {
                                var_20 = *(uint64_t *)rbx_1;
                                rax_0 = var_20;
                                rax_1 = var_20;
                                if (*(unsigned char *)var_20 != '=') {
                                    loop_state_var = 1U;
                                    break;
                                }
                            }
                            var_21 = rbx_1 + 8UL;
                            rax_2 = rax_0;
                            rbx_1 = var_21;
                            if (*(uint64_t *)var_21 == 0UL) {
                                continue;
                            }
                            loop_state_var = 0U;
                            break;
                        }
                    switch_state_var = 0;
                    switch (loop_state_var) {
                      case 0U:
                        {
                            switch_state_var = 1;
                            break;
                        }
                        break;
                      case 1U:
                        {
                            var_22 = rcx_0 + 8UL;
                            var_23 = *(uint64_t *)var_22;
                            *(uint64_t *)rcx_0 = var_23;
                            rcx_0 = var_22;
                            rcx_1 = var_22;
                            do {
                                var_22 = rcx_0 + 8UL;
                                var_23 = *(uint64_t *)var_22;
                                *(uint64_t *)rcx_0 = var_23;
                                rcx_0 = var_22;
                                rcx_1 = var_22;
                            } while (var_23 != 0UL);
                            local_sp_1 = local_sp_0;
                            rax_2 = rax_1;
                            rbx_1 = rbx_0;
                            rcx_3 = rcx_1;
                            continue;
                        }
                        break;
                    }
                    if (switch_state_var)
                        break;
                }
            mrv1.field_1 = rcx_3;
            return mrv1;
        }
        *var_17 = 4212896UL;
        indirect_placeholder();
        *(volatile uint32_t *)(uint32_t *)0UL = 22U;
    } else {
        var_7 = *(uint64_t *)6354304UL;
        rbx_3 = var_7;
        if (*(uint64_t *)var_7 == 0UL) {
            *(uint64_t *)(local_sp_3 + (-8L)) = 4212661UL;
            var_14 = indirect_placeholder_29(rdi4_0);
            var_15 = var_14.field_0;
            if (var_15 != 0UL) {
                *(uint64_t *)var_15 = rdi;
                *(uint64_t *)(local_sp_3 + (-16L)) = 4212696UL;
                indirect_placeholder();
                *(uint64_t *)(local_sp_3 + (-24L)) = 4212708UL;
                indirect_placeholder();
                *(uint64_t *)6354872UL = var_15;
                *(uint64_t *)6354304UL = var_15;
                mrv1.field_1 = rcx_3;
                return mrv1;
            }
        }
        var_8 = var_1 - rdi;
        var_9 = ((uint64_t)(uint32_t)var_8 == 0UL);
        rax_3 = var_8;
        while (1U)
            {
                var_10 = local_sp_2 + (-8L);
                *(uint64_t *)var_10 = 4212611UL;
                indirect_placeholder();
                rbx_2 = rbx_3;
                local_sp_2 = var_10;
                local_sp_3 = var_10;
                if (var_9) {
                    var_13 = rbx_3 + 8UL;
                    rbx_2 = var_13;
                    rbx_3 = var_13;
                    if (*(uint64_t *)var_13 == 0UL) {
                        continue;
                    }
                    loop_state_var = 0U;
                    break;
                }
                var_11 = (uint64_t *)rbx_3;
                var_12 = *var_11;
                if (*(unsigned char *)(var_8 + var_12) != '=') {
                    if (var_12 != 0UL) {
                        loop_state_var = 0U;
                        break;
                    }
                    *var_11 = rdi;
                    loop_state_var = 1U;
                    break;
                }
            }
        switch (loop_state_var) {
          case 0U:
            {
                rdi4_0 = ((rbx_2 - *(uint64_t *)6354304UL) + 16UL) & (-8L);
            }
            break;
          case 1U:
            {
                mrv2.field_0 = rax_3;
                mrv3 = mrv2;
                mrv3.field_1 = var_2;
                return mrv3;
            }
            break;
        }
    }
}

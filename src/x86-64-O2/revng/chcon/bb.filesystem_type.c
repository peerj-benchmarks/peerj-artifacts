typedef unsigned char __u_char;
typedef unsigned short __u_short;
typedef unsigned int __u_int;
typedef unsigned long __u_long;
typedef signed char __int8_t;
typedef unsigned char __uint8_t;
typedef short __int16_t;
typedef unsigned short __uint16_t;
typedef int __int32_t;
typedef unsigned int __uint32_t;
typedef long __int64_t;
typedef unsigned long __uint64_t;
typedef __int8_t __int_least8_t;
typedef __uint8_t __uint_least8_t;
typedef __int16_t __int_least16_t;
typedef __uint16_t __uint_least16_t;
typedef __int32_t __int_least32_t;
typedef __uint32_t __uint_least32_t;
typedef __int64_t __int_least64_t;
typedef __uint64_t __uint_least64_t;
typedef long __quad_t;
typedef unsigned long __u_quad_t;
typedef long __intmax_t;
typedef unsigned long __uintmax_t;
typedef unsigned long __dev_t;
typedef unsigned int __uid_t;
typedef unsigned int __gid_t;
typedef unsigned long __ino_t;
typedef unsigned long __ino64_t;
typedef unsigned int __mode_t;
typedef unsigned long __nlink_t;
typedef long __off_t;
typedef long __off64_t;
typedef int __pid_t;
typedef struct {
    int __val[2];
} __fsid_t;
typedef long __clock_t;
typedef unsigned long __rlim_t;
typedef unsigned long __rlim64_t;
typedef unsigned int __id_t;
typedef long __time_t;
typedef unsigned int __useconds_t;
typedef long __suseconds_t;
typedef int __daddr_t;
typedef int __key_t;
typedef int __clockid_t;
typedef void *__timer_t;
typedef long __blksize_t;
typedef long __blkcnt_t;
typedef long __blkcnt64_t;
typedef unsigned long __fsblkcnt_t;
typedef unsigned long __fsblkcnt64_t;
typedef unsigned long __fsfilcnt_t;
typedef unsigned long __fsfilcnt64_t;
typedef long __fsword_t;
typedef long __ssize_t;
typedef long __syscall_slong_t;
typedef unsigned long __syscall_ulong_t;
typedef __off64_t __loff_t;
typedef char *__caddr_t;
typedef long __intptr_t;
typedef unsigned int __socklen_t;
typedef int __sig_atomic_t;
typedef __int8_t int8_t;
typedef __int16_t int16_t;
typedef __int32_t int32_t;
typedef __int64_t int64_t;
typedef __uint8_t uint8_t;
typedef __uint16_t uint16_t;
typedef __uint32_t uint32_t;
typedef __uint64_t uint64_t;
typedef __int_least8_t int_least8_t;
typedef __int_least16_t int_least16_t;
typedef __int_least32_t int_least32_t;
typedef __int_least64_t int_least64_t;
typedef __uint_least8_t uint_least8_t;
typedef __uint_least16_t uint_least16_t;
typedef __uint_least32_t uint_least32_t;
typedef __uint_least64_t uint_least64_t;
typedef signed char int_fast8_t;
typedef long int_fast16_t;
typedef long int_fast32_t;
typedef long int_fast64_t;
typedef unsigned char uint_fast8_t;
typedef unsigned long uint_fast16_t;
typedef unsigned long uint_fast32_t;
typedef unsigned long uint_fast64_t;
typedef long intptr_t;
typedef unsigned long uintptr_t;
typedef __intmax_t intmax_t;
typedef __uintmax_t uintmax_t;
struct indirect_placeholder_105_ret_type;
struct indirect_placeholder_105_ret_type {
    uint64_t field_0;
    uint64_t field_1;
};
extern void abort(void);
extern uint64_t revng_init_local_sp(uint64_t param_0);
extern uint64_t init_rbx(void);
extern uint64_t init_rbp(void);
extern void indirect_placeholder(void);
extern uint64_t init_r12(void);
extern uint64_t indirect_placeholder_11(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint64_t param_3, uint64_t param_4);
extern uint64_t indirect_placeholder_1(uint64_t param_0, uint64_t param_1);
extern struct indirect_placeholder_105_ret_type indirect_placeholder_105(uint64_t param_0);
uint64_t bb_filesystem_type(uint64_t rdi) {
    uint64_t var_12;
    uint64_t var_0;
    uint64_t var_1;
    uint64_t var_2;
    uint64_t var_3;
    uint64_t *var_4;
    uint64_t var_5;
    unsigned char var_6;
    uint64_t *var_7;
    uint64_t var_8;
    uint64_t r12_0;
    uint64_t *var_21;
    uint64_t *var_19;
    uint64_t var_20;
    uint64_t *var_16;
    struct indirect_placeholder_105_ret_type var_17;
    uint64_t var_18;
    uint64_t *_pre_phi;
    uint64_t var_22;
    uint64_t *var_23;
    uint64_t var_15;
    uint64_t var_9;
    uint64_t *_cast_pre_phi;
    uint64_t *var_13;
    uint64_t local_sp_1;
    uint64_t var_14;
    uint64_t var_10;
    uint64_t *var_11;
    var_0 = revng_init_local_sp(0UL);
    var_1 = init_rbx();
    var_2 = init_rbp();
    var_3 = init_r12();
    *(uint64_t *)(var_0 + (-8L)) = var_3;
    *(uint64_t *)(var_0 + (-16L)) = var_2;
    *(uint64_t *)(var_0 + (-24L)) = var_1;
    var_4 = (uint64_t *)(rdi + 80UL);
    var_5 = *var_4;
    var_6 = *(unsigned char *)(var_5 + 73UL) & '\x02';
    var_7 = (uint64_t *)(var_5 + 80UL);
    var_8 = *var_7;
    r12_0 = var_8;
    if (var_6 == '\x00') {
        return 0UL;
    }
    var_9 = var_0 + (-152L);
    local_sp_1 = var_9;
    if (var_8 == 0UL) {
        var_10 = var_0 + (-160L);
        var_11 = (uint64_t *)var_10;
        *var_11 = 4213684UL;
        var_12 = indirect_placeholder_11(4212528UL, 13UL, 4201936UL, 4212512UL, 0UL);
        *var_7 = var_12;
        _cast_pre_phi = var_11;
        local_sp_1 = var_10;
        r12_0 = var_12;
        if (var_12 != 0UL) {
            var_22 = *var_4;
            var_23 = (uint64_t *)(var_0 + (-168L));
            *var_23 = 4213711UL;
            indirect_placeholder();
            _pre_phi = var_23;
            if ((uint64_t)(uint32_t)var_22 != 0UL) {
                return 0UL;
            }
            return *_pre_phi;
        }
    }
    _cast_pre_phi = (uint64_t *)var_9;
    var_13 = (uint64_t *)(rdi + 120UL);
    *_cast_pre_phi = *var_13;
    *(uint64_t *)(local_sp_1 + (-8L)) = 4213633UL;
    var_14 = indirect_placeholder_1(r12_0, local_sp_1);
    if (var_14 == 0UL) {
        return *(uint64_t *)(var_14 + 8UL);
    }
    var_15 = *var_4;
    *(uint64_t *)(local_sp_1 + (-16L)) = 4213751UL;
    indirect_placeholder();
    if ((uint64_t)(uint32_t)var_15 == 0UL) {
        return 0UL;
    }
    var_16 = (uint64_t *)(local_sp_1 + (-24L));
    *var_16 = 4213765UL;
    var_17 = indirect_placeholder_105(16UL);
    var_18 = var_17.field_0;
    _pre_phi = var_16;
    if (var_18 == 0UL) {
        return *_pre_phi;
    }
    *(uint64_t *)var_18 = *var_13;
    *(uint64_t *)(var_18 + 8UL) = *var_16;
    var_19 = (uint64_t *)(local_sp_1 + (-32L));
    *var_19 = 4213800UL;
    var_20 = indirect_placeholder_1(r12_0, var_18);
    _pre_phi = var_19;
    if (var_20 != 0UL) {
        if (var_20 == var_18) {
            *(uint64_t *)(local_sp_1 + (-40L)) = 4213838UL;
            indirect_placeholder();
            abort();
        }
    }
    var_21 = (uint64_t *)(local_sp_1 + (-40L));
    *var_21 = 4213831UL;
    indirect_placeholder();
    _pre_phi = var_21;
}

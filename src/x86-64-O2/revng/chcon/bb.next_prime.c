typedef unsigned char __u_char;
typedef unsigned short __u_short;
typedef unsigned int __u_int;
typedef unsigned long __u_long;
typedef signed char __int8_t;
typedef unsigned char __uint8_t;
typedef short __int16_t;
typedef unsigned short __uint16_t;
typedef int __int32_t;
typedef unsigned int __uint32_t;
typedef long __int64_t;
typedef unsigned long __uint64_t;
typedef __int8_t __int_least8_t;
typedef __uint8_t __uint_least8_t;
typedef __int16_t __int_least16_t;
typedef __uint16_t __uint_least16_t;
typedef __int32_t __int_least32_t;
typedef __uint32_t __uint_least32_t;
typedef __int64_t __int_least64_t;
typedef __uint64_t __uint_least64_t;
typedef long __quad_t;
typedef unsigned long __u_quad_t;
typedef long __intmax_t;
typedef unsigned long __uintmax_t;
typedef unsigned long __dev_t;
typedef unsigned int __uid_t;
typedef unsigned int __gid_t;
typedef unsigned long __ino_t;
typedef unsigned long __ino64_t;
typedef unsigned int __mode_t;
typedef unsigned long __nlink_t;
typedef long __off_t;
typedef long __off64_t;
typedef int __pid_t;
typedef struct {
    int __val[2];
} __fsid_t;
typedef long __clock_t;
typedef unsigned long __rlim_t;
typedef unsigned long __rlim64_t;
typedef unsigned int __id_t;
typedef long __time_t;
typedef unsigned int __useconds_t;
typedef long __suseconds_t;
typedef int __daddr_t;
typedef int __key_t;
typedef int __clockid_t;
typedef void *__timer_t;
typedef long __blksize_t;
typedef long __blkcnt_t;
typedef long __blkcnt64_t;
typedef unsigned long __fsblkcnt_t;
typedef unsigned long __fsblkcnt64_t;
typedef unsigned long __fsfilcnt_t;
typedef unsigned long __fsfilcnt64_t;
typedef long __fsword_t;
typedef long __ssize_t;
typedef long __syscall_slong_t;
typedef unsigned long __syscall_ulong_t;
typedef __off64_t __loff_t;
typedef char *__caddr_t;
typedef long __intptr_t;
typedef unsigned int __socklen_t;
typedef int __sig_atomic_t;
typedef __int8_t int8_t;
typedef __int16_t int16_t;
typedef __int32_t int32_t;
typedef __int64_t int64_t;
typedef __uint8_t uint8_t;
typedef __uint16_t uint16_t;
typedef __uint32_t uint32_t;
typedef __uint64_t uint64_t;
typedef __int_least8_t int_least8_t;
typedef __int_least16_t int_least16_t;
typedef __int_least32_t int_least32_t;
typedef __int_least64_t int_least64_t;
typedef __uint_least8_t uint_least8_t;
typedef __uint_least16_t uint_least16_t;
typedef __uint_least32_t uint_least32_t;
typedef __uint_least64_t uint_least64_t;
typedef signed char int_fast8_t;
typedef long int_fast16_t;
typedef long int_fast32_t;
typedef long int_fast64_t;
typedef unsigned char uint_fast8_t;
typedef unsigned long uint_fast16_t;
typedef unsigned long uint_fast32_t;
typedef unsigned long uint_fast64_t;
typedef long intptr_t;
typedef unsigned long uintptr_t;
typedef __intmax_t intmax_t;
typedef __uintmax_t uintmax_t;
struct bb_next_prime_ret_type;
struct helper_divq_EAX_wrapper_ret_type;
struct type_5;
struct bb_next_prime_ret_type {
    uint64_t field_0;
    uint64_t field_1;
    uint64_t field_2;
    uint64_t field_3;
    uint64_t field_4;
    uint64_t field_5;
    uint64_t field_6;
};
struct helper_divq_EAX_wrapper_ret_type {
    uint64_t field_0;
    uint64_t field_1;
    uint32_t field_2;
    uint64_t field_3;
    uint64_t field_4;
    uint32_t field_5;
    uint64_t field_6;
    uint32_t field_7;
    uint64_t field_8;
    uint32_t field_9;
    uint32_t field_10;
    unsigned char field_11;
    uint32_t field_12;
};
struct type_5 {
};
extern uint64_t revng_init_local_sp(uint64_t param_0);
extern uint64_t init_cc_src2(void);
extern uint64_t helper_cc_compute_c_wrapper(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint32_t param_3);
extern uint64_t init_state_0x9018(void);
extern uint32_t init_state_0x9010(void);
extern uint64_t init_state_0x8408(void);
extern uint64_t init_state_0x8328(void);
extern uint64_t init_state_0x82d8(void);
extern uint32_t init_state_0x9080(void);
extern uint32_t init_state_0x8248(void);
extern struct helper_divq_EAX_wrapper_ret_type helper_divq_EAX_wrapper(struct type_5 *param_0, uint64_t param_1, uint64_t param_2, uint64_t param_3, uint64_t param_4, uint64_t param_5, uint64_t param_6, uint64_t param_7, uint64_t param_8, uint64_t param_9, uint64_t param_10, uint64_t param_11, uint64_t param_12, uint64_t param_13, uint32_t param_14, uint64_t param_15, uint64_t param_16, uint64_t param_17, uint32_t param_18, uint32_t param_19);
extern uint64_t init_rcx(void);
extern uint64_t init_r9(void);
typedef _Bool bool;
struct bb_next_prime_ret_type bb_next_prime(uint64_t r10, uint64_t rbx, uint64_t rdi, uint64_t rbp, uint64_t r8, uint64_t rsi) {
    uint64_t var_0;
    uint64_t var_1;
    uint64_t var_2;
    uint32_t var_3;
    uint64_t var_4;
    uint64_t var_5;
    uint64_t var_6;
    uint32_t var_7;
    uint32_t var_8;
    uint64_t rdi9_0;
    uint64_t var_9;
    uint64_t var_10;
    uint64_t r811_4;
    uint64_t rsi12_3;
    uint64_t r811_3;
    uint64_t r811_0;
    uint64_t rdi9_1;
    uint64_t state_0x9018_3;
    uint64_t rsi12_0;
    uint32_t state_0x9010_3;
    uint64_t state_0x9018_0;
    uint64_t rcx_3;
    uint32_t state_0x9010_0;
    uint64_t state_0x82d8_3;
    uint64_t rcx_0;
    uint32_t state_0x9080_3;
    uint64_t state_0x82d8_0;
    uint32_t state_0x8248_3;
    uint32_t state_0x9080_0;
    uint64_t rdi9_2;
    uint32_t state_0x8248_0;
    uint64_t r811_2;
    uint64_t var_28;
    uint64_t r811_1;
    uint64_t rsi12_1;
    uint64_t state_0x9018_1;
    uint32_t state_0x9010_1;
    uint64_t rcx_1;
    uint64_t state_0x82d8_1;
    uint32_t state_0x9080_1;
    uint32_t state_0x8248_1;
    uint64_t var_11;
    uint64_t var_12;
    uint64_t var_13;
    uint64_t var_14;
    struct helper_divq_EAX_wrapper_ret_type var_15;
    uint64_t r9_0;
    uint64_t rsi12_2;
    uint64_t state_0x9018_2;
    uint32_t state_0x9010_2;
    uint64_t rcx_2;
    uint64_t state_0x82d8_2;
    uint32_t state_0x9080_2;
    uint32_t state_0x8248_2;
    uint64_t var_22;
    uint64_t var_23;
    uint32_t var_24;
    uint64_t var_25;
    struct helper_divq_EAX_wrapper_ret_type var_21;
    uint32_t var_26;
    uint32_t var_27;
    uint64_t var_16;
    uint32_t var_17;
    uint64_t var_18;
    uint32_t var_19;
    uint32_t var_20;
    uint64_t rcx_4;
    struct bb_next_prime_ret_type mrv;
    struct bb_next_prime_ret_type mrv1;
    struct bb_next_prime_ret_type mrv2;
    struct bb_next_prime_ret_type mrv3;
    struct bb_next_prime_ret_type mrv4;
    struct bb_next_prime_ret_type mrv5;
    struct bb_next_prime_ret_type mrv6;
    unsigned int loop_state_var;
    bool switch_state_var;
    revng_init_local_sp(0UL);
    var_0 = init_rcx();
    var_1 = init_cc_src2();
    var_2 = init_state_0x9018();
    var_3 = init_state_0x9010();
    var_4 = init_state_0x8408();
    var_5 = init_state_0x8328();
    var_6 = init_state_0x82d8();
    var_7 = init_state_0x9080();
    var_8 = init_state_0x8248();
    rdi9_0 = 11UL;
    r811_4 = r8;
    r811_0 = r8;
    rsi12_0 = rsi;
    state_0x9018_0 = var_2;
    state_0x9010_0 = var_3;
    rcx_0 = var_0;
    state_0x82d8_0 = var_6;
    state_0x9080_0 = var_7;
    rdi9_2 = 18446744073709551615UL;
    state_0x8248_0 = var_8;
    r811_1 = 16UL;
    rsi12_1 = 9UL;
    rcx_1 = 3UL;
    r9_0 = 12297829382473034411UL;
    rcx_2 = 3UL;
    rcx_4 = var_0;
    if (rdi <= 9UL) {
        var_9 = init_r9();
        var_10 = rdi | 1UL;
        rdi9_0 = var_10;
        r9_0 = var_9;
        if (var_10 == 18446744073709551615UL) {
            mrv.field_0 = rdi9_2;
            mrv1 = mrv;
            mrv1.field_1 = rcx_4;
            mrv2 = mrv1;
            mrv2.field_2 = r10;
            mrv3 = mrv2;
            mrv3.field_3 = rbx;
            mrv4 = mrv3;
            mrv4.field_4 = r9_0;
            mrv5 = mrv4;
            mrv5.field_5 = rbp;
            mrv6 = mrv5;
            mrv6.field_6 = r811_4;
            return mrv6;
        }
    }
    rdi9_1 = rdi9_0;
    while (1U)
        {
            rsi12_3 = rsi12_0;
            r811_3 = r811_0;
            state_0x9018_3 = state_0x9018_0;
            state_0x9010_3 = state_0x9010_0;
            rcx_3 = rcx_0;
            state_0x82d8_3 = state_0x82d8_0;
            state_0x9080_3 = state_0x9080_0;
            state_0x8248_3 = state_0x8248_0;
            rdi9_2 = rdi9_1;
            r811_2 = r811_0;
            state_0x9018_1 = state_0x9018_0;
            state_0x9010_1 = state_0x9010_0;
            state_0x82d8_1 = state_0x82d8_0;
            state_0x9080_1 = state_0x9080_0;
            state_0x8248_1 = state_0x8248_0;
            rsi12_2 = rsi12_0;
            state_0x9018_2 = state_0x9018_0;
            state_0x9010_2 = state_0x9010_0;
            state_0x82d8_2 = state_0x82d8_0;
            state_0x9080_2 = state_0x9080_0;
            state_0x8248_2 = state_0x8248_0;
            if (rdi9_1 > 9UL) {
                var_21 = helper_divq_EAX_wrapper((struct type_5 *)(0UL), rcx_2, 4226802UL, rdi9_1, rcx_2, r10, rbx, 12297829382473034411UL, rdi9_1, rbp, r811_2, 0UL, rsi12_2, state_0x9018_2, state_0x9010_2, var_4, var_5, state_0x82d8_2, state_0x9080_2, state_0x8248_2);
                var_22 = var_21.field_4;
                var_23 = var_21.field_6;
                var_24 = var_21.field_7;
                var_25 = var_21.field_8;
                var_26 = var_21.field_9;
                var_27 = var_21.field_10;
                r811_4 = r811_2;
                rsi12_3 = rsi12_2;
                r811_3 = r811_2;
                state_0x9018_3 = var_23;
                state_0x9010_3 = var_24;
                rcx_3 = rcx_2;
                state_0x82d8_3 = var_25;
                state_0x9080_3 = var_26;
                state_0x8248_3 = var_27;
                rcx_4 = rcx_2;
                if (var_22 == 0UL) {
                    break;
                }
                var_28 = rdi9_1 + 2UL;
                r811_4 = r811_3;
                r811_0 = r811_3;
                rdi9_1 = var_28;
                rsi12_0 = rsi12_3;
                state_0x9018_0 = state_0x9018_3;
                state_0x9010_0 = state_0x9010_3;
                rcx_0 = rcx_3;
                state_0x82d8_0 = state_0x82d8_3;
                state_0x9080_0 = state_0x9080_3;
                rdi9_2 = var_28;
                state_0x8248_0 = state_0x8248_3;
                rcx_4 = rcx_3;
                if (rdi9_1 != 18446744073709551613UL) {
                    continue;
                }
                break;
            }
            if (rdi9_1 != ((uint64_t)(((unsigned __int128)rdi9_1 * 12297829382473034411ULL) >> 65ULL) * 3UL)) {
                var_28 = rdi9_1 + 2UL;
                r811_4 = r811_3;
                r811_0 = r811_3;
                rdi9_1 = var_28;
                rsi12_0 = rsi12_3;
                state_0x9018_0 = state_0x9018_3;
                state_0x9010_0 = state_0x9010_3;
                rcx_0 = rcx_3;
                state_0x82d8_0 = state_0x82d8_3;
                state_0x9080_0 = state_0x9080_3;
                rdi9_2 = var_28;
                state_0x8248_0 = state_0x8248_3;
                rcx_4 = rcx_3;
                if (rdi9_1 != 18446744073709551613UL) {
                    continue;
                }
                break;
            }
            while (1U)
                {
                    var_11 = rsi12_1 + r811_1;
                    var_12 = rcx_1 + 2UL;
                    var_13 = helper_cc_compute_c_wrapper(var_11 - rdi9_1, rdi9_1, var_1, 17U);
                    rsi12_3 = var_11;
                    rcx_3 = var_12;
                    r811_2 = r811_1;
                    rsi12_1 = var_11;
                    rcx_1 = var_12;
                    rsi12_2 = var_11;
                    state_0x9018_2 = state_0x9018_1;
                    state_0x9010_2 = state_0x9010_1;
                    rcx_2 = var_12;
                    state_0x82d8_2 = state_0x82d8_1;
                    state_0x9080_2 = state_0x9080_1;
                    state_0x8248_2 = state_0x8248_1;
                    if (var_13 != 0UL) {
                        loop_state_var = 0U;
                        break;
                    }
                    var_14 = r811_1 + 8UL;
                    var_15 = helper_divq_EAX_wrapper((struct type_5 *)(0UL), var_12, 4226777UL, rdi9_1, var_12, r10, rbx, 12297829382473034411UL, rdi9_1, rbp, var_14, 0UL, var_11, state_0x9018_1, state_0x9010_1, var_4, var_5, state_0x82d8_1, state_0x9080_1, state_0x8248_1);
                    r811_1 = var_14;
                    r811_3 = var_14;
                    if (var_15.field_4 != 0UL) {
                        loop_state_var = 1U;
                        break;
                    }
                    state_0x9018_1 = var_15.field_6;
                    state_0x9010_1 = var_15.field_7;
                    state_0x82d8_1 = var_15.field_8;
                    state_0x9080_1 = var_15.field_9;
                    state_0x8248_1 = var_15.field_10;
                    continue;
                }
            switch_state_var = 0;
            switch (loop_state_var) {
              case 0U:
                {
                    break;
                }
                break;
              case 1U:
                {
                    var_16 = var_15.field_6;
                    var_17 = var_15.field_7;
                    var_18 = var_15.field_8;
                    var_19 = var_15.field_9;
                    var_20 = var_15.field_10;
                    state_0x9018_3 = var_16;
                    state_0x9010_3 = var_17;
                    state_0x82d8_3 = var_18;
                    state_0x9080_3 = var_19;
                    state_0x8248_3 = var_20;
                    var_28 = rdi9_1 + 2UL;
                    r811_4 = r811_3;
                    r811_0 = r811_3;
                    rdi9_1 = var_28;
                    rsi12_0 = rsi12_3;
                    state_0x9018_0 = state_0x9018_3;
                    state_0x9010_0 = state_0x9010_3;
                    rcx_0 = rcx_3;
                    state_0x82d8_0 = state_0x82d8_3;
                    state_0x9080_0 = state_0x9080_3;
                    rdi9_2 = var_28;
                    state_0x8248_0 = state_0x8248_3;
                    rcx_4 = rcx_3;
                    if (rdi9_1 != 18446744073709551613UL) {
                        continue;
                    }
                    switch_state_var = 1;
                    break;
                }
                break;
            }
            if (switch_state_var)
                break;
        }
    mrv.field_0 = rdi9_2;
    mrv1 = mrv;
    mrv1.field_1 = rcx_4;
    mrv2 = mrv1;
    mrv2.field_2 = r10;
    mrv3 = mrv2;
    mrv3.field_3 = rbx;
    mrv4 = mrv3;
    mrv4.field_4 = r9_0;
    mrv5 = mrv4;
    mrv5.field_5 = rbp;
    mrv6 = mrv5;
    mrv6.field_6 = r811_4;
    return mrv6;
}

typedef unsigned char __u_char;
typedef unsigned short __u_short;
typedef unsigned int __u_int;
typedef unsigned long __u_long;
typedef signed char __int8_t;
typedef unsigned char __uint8_t;
typedef short __int16_t;
typedef unsigned short __uint16_t;
typedef int __int32_t;
typedef unsigned int __uint32_t;
typedef long __int64_t;
typedef unsigned long __uint64_t;
typedef __int8_t __int_least8_t;
typedef __uint8_t __uint_least8_t;
typedef __int16_t __int_least16_t;
typedef __uint16_t __uint_least16_t;
typedef __int32_t __int_least32_t;
typedef __uint32_t __uint_least32_t;
typedef __int64_t __int_least64_t;
typedef __uint64_t __uint_least64_t;
typedef long __quad_t;
typedef unsigned long __u_quad_t;
typedef long __intmax_t;
typedef unsigned long __uintmax_t;
typedef unsigned long __dev_t;
typedef unsigned int __uid_t;
typedef unsigned int __gid_t;
typedef unsigned long __ino_t;
typedef unsigned long __ino64_t;
typedef unsigned int __mode_t;
typedef unsigned long __nlink_t;
typedef long __off_t;
typedef long __off64_t;
typedef int __pid_t;
typedef struct {
    int __val[2];
} __fsid_t;
typedef long __clock_t;
typedef unsigned long __rlim_t;
typedef unsigned long __rlim64_t;
typedef unsigned int __id_t;
typedef long __time_t;
typedef unsigned int __useconds_t;
typedef long __suseconds_t;
typedef int __daddr_t;
typedef int __key_t;
typedef int __clockid_t;
typedef void *__timer_t;
typedef long __blksize_t;
typedef long __blkcnt_t;
typedef long __blkcnt64_t;
typedef unsigned long __fsblkcnt_t;
typedef unsigned long __fsblkcnt64_t;
typedef unsigned long __fsfilcnt_t;
typedef unsigned long __fsfilcnt64_t;
typedef long __fsword_t;
typedef long __ssize_t;
typedef long __syscall_slong_t;
typedef unsigned long __syscall_ulong_t;
typedef __off64_t __loff_t;
typedef char *__caddr_t;
typedef long __intptr_t;
typedef unsigned int __socklen_t;
typedef int __sig_atomic_t;
typedef __int8_t int8_t;
typedef __int16_t int16_t;
typedef __int32_t int32_t;
typedef __int64_t int64_t;
typedef __uint8_t uint8_t;
typedef __uint16_t uint16_t;
typedef __uint32_t uint32_t;
typedef __uint64_t uint64_t;
typedef __int_least8_t int_least8_t;
typedef __int_least16_t int_least16_t;
typedef __int_least32_t int_least32_t;
typedef __int_least64_t int_least64_t;
typedef __uint_least8_t uint_least8_t;
typedef __uint_least16_t uint_least16_t;
typedef __uint_least32_t uint_least32_t;
typedef __uint_least64_t uint_least64_t;
typedef signed char int_fast8_t;
typedef long int_fast16_t;
typedef long int_fast32_t;
typedef long int_fast64_t;
typedef unsigned char uint_fast8_t;
typedef unsigned long uint_fast16_t;
typedef unsigned long uint_fast32_t;
typedef unsigned long uint_fast64_t;
typedef long intptr_t;
typedef unsigned long uintptr_t;
typedef __intmax_t intmax_t;
typedef __uintmax_t uintmax_t;
typedef _Bool bool;
extern uint64_t revng_init_local_sp(uint64_t param_0);
extern uint64_t init_rbx(void);
extern uint64_t init_rbp(void);
extern void indirect_placeholder(void);
extern void indirect_placeholder_5(uint64_t param_0);
uint64_t bb_fts_close(uint64_t rdi) {
    uint64_t var_0;
    uint64_t var_1;
    uint64_t var_2;
    uint64_t var_3;
    uint64_t var_4;
    uint64_t local_sp_9;
    uint64_t var_26;
    uint32_t _pre;
    uint64_t rbx_3;
    uint64_t var_23;
    uint64_t var_24;
    uint64_t var_25;
    uint64_t local_sp_3;
    uint64_t local_sp_5;
    uint32_t *var_19;
    uint32_t var_20;
    uint64_t var_21;
    uint64_t var_22;
    uint64_t local_sp_2;
    uint64_t storemerge;
    uint32_t *_pre_phi69;
    uint32_t _pre_phi;
    uint64_t local_sp_1;
    uint64_t rdi1_0;
    uint64_t rbx_2;
    uint64_t var_36;
    uint32_t var_37;
    uint32_t *_pre68;
    uint64_t local_sp_10;
    uint64_t var_35;
    uint32_t *_pre_phi71;
    uint64_t local_sp_4;
    uint64_t var_27;
    uint64_t var_28;
    uint64_t var_29;
    uint64_t var_30;
    uint64_t var_31;
    uint64_t var_15;
    bool var_16;
    uint64_t var_17;
    uint64_t *var_18;
    uint64_t var_32;
    uint64_t var_33;
    uint64_t var_34;
    uint64_t local_sp_6;
    uint64_t rbx_4;
    uint64_t local_sp_8;
    uint64_t local_sp_7;
    uint64_t var_9;
    uint64_t rdi1_1;
    uint64_t var_5;
    uint64_t var_7;
    uint64_t var_8;
    uint64_t var_6;
    uint64_t var_10;
    uint64_t var_11;
    uint64_t var_12;
    uint32_t var_13;
    uint64_t var_14;
    var_0 = revng_init_local_sp(0UL);
    var_1 = init_rbx();
    var_2 = init_rbp();
    *(uint64_t *)(var_0 + (-8L)) = var_2;
    *(uint64_t *)(var_0 + (-16L)) = var_1;
    var_3 = var_0 + (-24L);
    var_4 = *(uint64_t *)rdi;
    local_sp_9 = var_3;
    rbx_3 = 0UL;
    storemerge = 0UL;
    local_sp_8 = var_3;
    local_sp_7 = var_3;
    rdi1_1 = var_4;
    if (var_4 != 0UL) {
        if ((long)*(uint64_t *)(var_4 + 88UL) <= (long)18446744073709551615UL) {
            while (1U)
                {
                    var_5 = *(uint64_t *)(rdi1_1 + 16UL);
                    rbx_4 = var_5;
                    if (var_5 == 0UL) {
                        var_7 = *(uint64_t *)(rdi1_1 + 8UL);
                        var_8 = local_sp_8 + (-8L);
                        *(uint64_t *)var_8 = 4218785UL;
                        indirect_placeholder();
                        local_sp_6 = var_8;
                        rbx_4 = var_7;
                        local_sp_7 = var_8;
                        if ((long)*(uint64_t *)(var_7 + 88UL) > (long)18446744073709551615UL) {
                            break;
                        }
                    }
                    var_6 = local_sp_8 + (-8L);
                    *(uint64_t *)var_6 = 4218757UL;
                    indirect_placeholder();
                    local_sp_6 = var_6;
                    local_sp_7 = var_6;
                    if ((long)*(uint64_t *)(var_5 + 88UL) < (long)0UL) {
                        break;
                    }
                    local_sp_8 = local_sp_6;
                    rdi1_1 = rbx_4;
                    continue;
                }
        }
        var_9 = local_sp_7 + (-8L);
        *(uint64_t *)var_9 = 4218808UL;
        indirect_placeholder();
        local_sp_9 = var_9;
    }
    var_10 = *(uint64_t *)(rdi + 8UL);
    local_sp_10 = local_sp_9;
    if (var_10 == 0UL) {
        var_11 = local_sp_9 + (-8L);
        *(uint64_t *)var_11 = 4218822UL;
        indirect_placeholder_5(var_10);
        local_sp_10 = var_11;
    }
    *(uint64_t *)(local_sp_10 + (-8L)) = 4218831UL;
    indirect_placeholder();
    var_12 = local_sp_10 + (-16L);
    *(uint64_t *)var_12 = 4218840UL;
    indirect_placeholder();
    var_13 = *(uint32_t *)(rdi + 72UL);
    var_14 = (uint64_t)var_13;
    local_sp_5 = var_12;
    if ((uint64_t)((uint16_t)var_14 & (unsigned short)512U) != 0UL) {
        if ((int)*(uint32_t *)(rdi + 44UL) >= (int)0U) {
            var_32 = rdi + 96UL;
            var_33 = local_sp_5 + (-8L);
            *(uint64_t *)var_33 = 4218875UL;
            indirect_placeholder_5(var_32);
            var_34 = *(uint64_t *)(rdi + 80UL);
            local_sp_3 = var_33;
            rdi1_0 = var_34;
            if (var_34 != 0UL) {
                *(uint64_t *)(local_sp_5 + (-16L)) = 4219077UL;
                indirect_placeholder_5(rdi);
                *(uint64_t *)(local_sp_5 + (-24L)) = 4219085UL;
                indirect_placeholder();
                return storemerge;
            }
            var_35 = local_sp_3 + (-8L);
            *(uint64_t *)var_35 = 4218893UL;
            indirect_placeholder_5(rdi1_0);
            local_sp_2 = var_35;
            rbx_2 = rbx_3;
            *(uint64_t *)(local_sp_2 + (-8L)) = 4218901UL;
            indirect_placeholder_5(rdi);
            var_36 = local_sp_2 + (-16L);
            *(uint64_t *)var_36 = 4218909UL;
            indirect_placeholder();
            var_37 = (uint32_t)rbx_2;
            _pre_phi = var_37;
            local_sp_1 = var_36;
            if ((uint64_t)var_37 == 0UL) {
                return storemerge;
            }
            _pre68 = (uint32_t *)var_14;
            _pre_phi69 = _pre68;
            *(uint64_t *)(local_sp_1 + (-8L)) = 4219057UL;
            indirect_placeholder();
            *_pre_phi69 = _pre_phi;
            storemerge = 4294967295UL;
            return storemerge;
        }
        var_15 = local_sp_10 + (-24L);
        *(uint64_t *)var_15 = 4218860UL;
        indirect_placeholder();
        local_sp_4 = var_15;
        local_sp_5 = var_15;
        if (var_13 == 0U) {
            _pre_phi71 = (uint32_t *)var_14;
            var_27 = local_sp_4 + (-8L);
            *(uint64_t *)var_27 = 4218961UL;
            indirect_placeholder();
            var_28 = (uint64_t)*_pre_phi71;
            var_29 = rdi + 96UL;
            var_30 = var_27 + (-8L);
            *(uint64_t *)var_30 = 4218972UL;
            indirect_placeholder_5(var_29);
            var_31 = *(uint64_t *)(rdi + 80UL);
            rbx_3 = var_28;
            local_sp_3 = var_30;
            local_sp_2 = var_30;
            rdi1_0 = var_31;
            rbx_2 = var_28;
            if (var_31 == 0UL) {
                var_35 = local_sp_3 + (-8L);
                *(uint64_t *)var_35 = 4218893UL;
                indirect_placeholder_5(rdi1_0);
                local_sp_2 = var_35;
                rbx_2 = rbx_3;
            }
            *(uint64_t *)(local_sp_2 + (-8L)) = 4218901UL;
            indirect_placeholder_5(rdi);
            var_36 = local_sp_2 + (-16L);
            *(uint64_t *)var_36 = 4218909UL;
            indirect_placeholder();
            var_37 = (uint32_t)rbx_2;
            _pre_phi = var_37;
            local_sp_1 = var_36;
            if ((uint64_t)var_37 == 0UL) {
                return storemerge;
            }
            _pre68 = (uint32_t *)var_14;
            _pre_phi69 = _pre68;
            *(uint64_t *)(local_sp_1 + (-8L)) = 4219057UL;
            indirect_placeholder();
            *_pre_phi69 = _pre_phi;
            storemerge = 4294967295UL;
            return storemerge;
        }
        var_32 = rdi + 96UL;
        var_33 = local_sp_5 + (-8L);
        *(uint64_t *)var_33 = 4218875UL;
        indirect_placeholder_5(var_32);
        var_34 = *(uint64_t *)(rdi + 80UL);
        local_sp_3 = var_33;
        rdi1_0 = var_34;
        if (var_34 != 0UL) {
            *(uint64_t *)(local_sp_5 + (-16L)) = 4219077UL;
            indirect_placeholder_5(rdi);
            *(uint64_t *)(local_sp_5 + (-24L)) = 4219085UL;
            indirect_placeholder();
            return storemerge;
        }
        var_35 = local_sp_3 + (-8L);
        *(uint64_t *)var_35 = 4218893UL;
        indirect_placeholder_5(rdi1_0);
        local_sp_2 = var_35;
        rbx_2 = rbx_3;
        *(uint64_t *)(local_sp_2 + (-8L)) = 4218901UL;
        indirect_placeholder_5(rdi);
        var_36 = local_sp_2 + (-16L);
        *(uint64_t *)var_36 = 4218909UL;
        indirect_placeholder();
        var_37 = (uint32_t)rbx_2;
        _pre_phi = var_37;
        local_sp_1 = var_36;
        if ((uint64_t)var_37 == 0UL) {
            return storemerge;
        }
        _pre68 = (uint32_t *)var_14;
        _pre_phi69 = _pre68;
        *(uint64_t *)(local_sp_1 + (-8L)) = 4219057UL;
        indirect_placeholder();
        *_pre_phi69 = _pre_phi;
        storemerge = 4294967295UL;
        return storemerge;
    }
    if ((var_14 & 4UL) != 0UL) {
        var_32 = rdi + 96UL;
        var_33 = local_sp_5 + (-8L);
        *(uint64_t *)var_33 = 4218875UL;
        indirect_placeholder_5(var_32);
        var_34 = *(uint64_t *)(rdi + 80UL);
        local_sp_3 = var_33;
        rdi1_0 = var_34;
        if (var_34 != 0UL) {
            *(uint64_t *)(local_sp_5 + (-16L)) = 4219077UL;
            indirect_placeholder_5(rdi);
            *(uint64_t *)(local_sp_5 + (-24L)) = 4219085UL;
            indirect_placeholder();
            return storemerge;
        }
        var_35 = local_sp_3 + (-8L);
        *(uint64_t *)var_35 = 4218893UL;
        indirect_placeholder_5(rdi1_0);
        local_sp_2 = var_35;
        rbx_2 = rbx_3;
        *(uint64_t *)(local_sp_2 + (-8L)) = 4218901UL;
        indirect_placeholder_5(rdi);
        var_36 = local_sp_2 + (-16L);
        *(uint64_t *)var_36 = 4218909UL;
        indirect_placeholder();
        var_37 = (uint32_t)rbx_2;
        _pre_phi = var_37;
        local_sp_1 = var_36;
        if ((uint64_t)var_37 == 0UL) {
            return storemerge;
        }
        _pre68 = (uint32_t *)var_14;
        _pre_phi69 = _pre68;
        *(uint64_t *)(local_sp_1 + (-8L)) = 4219057UL;
        indirect_placeholder();
        *_pre_phi69 = _pre_phi;
        storemerge = 4294967295UL;
        return storemerge;
    }
    *(uint64_t *)(local_sp_10 + (-24L)) = 4218940UL;
    indirect_placeholder();
    var_16 = (var_13 == 0U);
    var_17 = local_sp_10 + (-32L);
    var_18 = (uint64_t *)var_17;
    local_sp_5 = var_17;
    if (!var_16) {
        *var_18 = 4218952UL;
        indirect_placeholder();
        var_32 = rdi + 96UL;
        var_33 = local_sp_5 + (-8L);
        *(uint64_t *)var_33 = 4218875UL;
        indirect_placeholder_5(var_32);
        var_34 = *(uint64_t *)(rdi + 80UL);
        local_sp_3 = var_33;
        rdi1_0 = var_34;
        if (var_34 != 0UL) {
            *(uint64_t *)(local_sp_5 + (-16L)) = 4219077UL;
            indirect_placeholder_5(rdi);
            *(uint64_t *)(local_sp_5 + (-24L)) = 4219085UL;
            indirect_placeholder();
            return storemerge;
        }
        var_35 = local_sp_3 + (-8L);
        *(uint64_t *)var_35 = 4218893UL;
        indirect_placeholder_5(rdi1_0);
        local_sp_2 = var_35;
        rbx_2 = rbx_3;
        *(uint64_t *)(local_sp_2 + (-8L)) = 4218901UL;
        indirect_placeholder_5(rdi);
        var_36 = local_sp_2 + (-16L);
        *(uint64_t *)var_36 = 4218909UL;
        indirect_placeholder();
        var_37 = (uint32_t)rbx_2;
        _pre_phi = var_37;
        local_sp_1 = var_36;
        if ((uint64_t)var_37 == 0UL) {
            return storemerge;
        }
        _pre68 = (uint32_t *)var_14;
        _pre_phi69 = _pre68;
        *(uint64_t *)(local_sp_1 + (-8L)) = 4219057UL;
        indirect_placeholder();
        *_pre_phi69 = _pre_phi;
        storemerge = 4294967295UL;
        return storemerge;
    }
    *var_18 = 4218996UL;
    indirect_placeholder();
    var_19 = (uint32_t *)var_14;
    var_20 = *var_19;
    var_21 = (uint64_t)var_20;
    var_22 = local_sp_10 + (-40L);
    *(uint64_t *)var_22 = 4219006UL;
    indirect_placeholder();
    _pre_phi69 = var_19;
    rbx_3 = var_21;
    _pre_phi71 = var_19;
    local_sp_4 = var_22;
    if (var_20 == 0U) {
        return;
    }
    var_23 = rdi + 96UL;
    var_24 = local_sp_10 + (-48L);
    *(uint64_t *)var_24 = 4219023UL;
    indirect_placeholder_5(var_23);
    var_25 = *(uint64_t *)(rdi + 80UL);
    local_sp_3 = var_24;
    rdi1_0 = var_25;
    if (var_25 != 0UL) {
        *(uint64_t *)(local_sp_10 + (-56L)) = 4219044UL;
        indirect_placeholder_5(rdi);
        var_26 = local_sp_10 + (-64L);
        *(uint64_t *)var_26 = 4219052UL;
        indirect_placeholder();
        _pre = (uint32_t)var_21;
        _pre_phi = _pre;
        local_sp_1 = var_26;
        *(uint64_t *)(local_sp_1 + (-8L)) = 4219057UL;
        indirect_placeholder();
        *_pre_phi69 = _pre_phi;
        storemerge = 4294967295UL;
        return storemerge;
    }
    var_35 = local_sp_3 + (-8L);
    *(uint64_t *)var_35 = 4218893UL;
    indirect_placeholder_5(rdi1_0);
    local_sp_2 = var_35;
    rbx_2 = rbx_3;
    *(uint64_t *)(local_sp_2 + (-8L)) = 4218901UL;
    indirect_placeholder_5(rdi);
    var_36 = local_sp_2 + (-16L);
    *(uint64_t *)var_36 = 4218909UL;
    indirect_placeholder();
    var_37 = (uint32_t)rbx_2;
    _pre_phi = var_37;
    local_sp_1 = var_36;
    if ((uint64_t)var_37 == 0UL) {
        return storemerge;
    }
    _pre68 = (uint32_t *)var_14;
    _pre_phi69 = _pre68;
    *(uint64_t *)(local_sp_1 + (-8L)) = 4219057UL;
    indirect_placeholder();
    *_pre_phi69 = _pre_phi;
    storemerge = 4294967295UL;
    return storemerge;
}

typedef unsigned char __u_char;
typedef unsigned short __u_short;
typedef unsigned int __u_int;
typedef unsigned long __u_long;
typedef signed char __int8_t;
typedef unsigned char __uint8_t;
typedef short __int16_t;
typedef unsigned short __uint16_t;
typedef int __int32_t;
typedef unsigned int __uint32_t;
typedef long __int64_t;
typedef unsigned long __uint64_t;
typedef __int8_t __int_least8_t;
typedef __uint8_t __uint_least8_t;
typedef __int16_t __int_least16_t;
typedef __uint16_t __uint_least16_t;
typedef __int32_t __int_least32_t;
typedef __uint32_t __uint_least32_t;
typedef __int64_t __int_least64_t;
typedef __uint64_t __uint_least64_t;
typedef long __quad_t;
typedef unsigned long __u_quad_t;
typedef long __intmax_t;
typedef unsigned long __uintmax_t;
typedef unsigned long __dev_t;
typedef unsigned int __uid_t;
typedef unsigned int __gid_t;
typedef unsigned long __ino_t;
typedef unsigned long __ino64_t;
typedef unsigned int __mode_t;
typedef unsigned long __nlink_t;
typedef long __off_t;
typedef long __off64_t;
typedef int __pid_t;
typedef struct {
    int __val[2];
} __fsid_t;
typedef long __clock_t;
typedef unsigned long __rlim_t;
typedef unsigned long __rlim64_t;
typedef unsigned int __id_t;
typedef long __time_t;
typedef unsigned int __useconds_t;
typedef long __suseconds_t;
typedef int __daddr_t;
typedef int __key_t;
typedef int __clockid_t;
typedef void *__timer_t;
typedef long __blksize_t;
typedef long __blkcnt_t;
typedef long __blkcnt64_t;
typedef unsigned long __fsblkcnt_t;
typedef unsigned long __fsblkcnt64_t;
typedef unsigned long __fsfilcnt_t;
typedef unsigned long __fsfilcnt64_t;
typedef long __fsword_t;
typedef long __ssize_t;
typedef long __syscall_slong_t;
typedef unsigned long __syscall_ulong_t;
typedef __off64_t __loff_t;
typedef char *__caddr_t;
typedef long __intptr_t;
typedef unsigned int __socklen_t;
typedef int __sig_atomic_t;
typedef __int8_t int8_t;
typedef __int16_t int16_t;
typedef __int32_t int32_t;
typedef __int64_t int64_t;
typedef __uint8_t uint8_t;
typedef __uint16_t uint16_t;
typedef __uint32_t uint32_t;
typedef __uint64_t uint64_t;
typedef __int_least8_t int_least8_t;
typedef __int_least16_t int_least16_t;
typedef __int_least32_t int_least32_t;
typedef __int_least64_t int_least64_t;
typedef __uint_least8_t uint_least8_t;
typedef __uint_least16_t uint_least16_t;
typedef __uint_least32_t uint_least32_t;
typedef __uint_least64_t uint_least64_t;
typedef signed char int_fast8_t;
typedef long int_fast16_t;
typedef long int_fast32_t;
typedef long int_fast64_t;
typedef unsigned char uint_fast8_t;
typedef unsigned long uint_fast16_t;
typedef unsigned long uint_fast32_t;
typedef unsigned long uint_fast64_t;
typedef long intptr_t;
typedef unsigned long uintptr_t;
typedef __intmax_t intmax_t;
typedef __uintmax_t uintmax_t;
typedef _Bool bool;
struct helper_pxor_xmm_wrapper_134_ret_type;
struct type_4;
struct type_6;
struct helper_pxor_xmm_wrapper_139_ret_type;
struct helper_cvtsq2ss_wrapper_ret_type;
struct helper_addss_wrapper_ret_type;
struct helper_mulss_wrapper_142_ret_type;
struct helper_ucomiss_wrapper_143_ret_type;
struct helper_cvttss2sq_wrapper_ret_type;
struct helper_subss_wrapper_ret_type;
struct helper_ucomiss_wrapper_145_ret_type;
struct helper_cvtsq2ss_wrapper_140_ret_type;
struct helper_addss_wrapper_170_ret_type;
struct helper_addss_wrapper_141_ret_type;
struct helper_mulss_wrapper_171_ret_type;
struct helper_ucomiss_wrapper_172_ret_type;
struct helper_mulss_wrapper_173_ret_type;
struct indirect_placeholder_103_ret_type;
struct helper_pxor_xmm_wrapper_134_ret_type {
    uint64_t field_0;
    uint64_t field_1;
};
struct type_4 {
};
struct type_6 {
};
struct helper_pxor_xmm_wrapper_139_ret_type {
    uint64_t field_0;
    uint64_t field_1;
};
struct helper_cvtsq2ss_wrapper_ret_type {
    uint64_t field_0;
    unsigned char field_1;
};
struct helper_addss_wrapper_ret_type {
    uint64_t field_0;
    unsigned char field_1;
};
struct helper_mulss_wrapper_142_ret_type {
    uint64_t field_0;
    unsigned char field_1;
};
struct helper_ucomiss_wrapper_143_ret_type {
    uint64_t field_0;
    unsigned char field_1;
};
struct helper_cvttss2sq_wrapper_ret_type {
    uint64_t field_0;
    unsigned char field_1;
};
struct helper_subss_wrapper_ret_type {
    uint64_t field_0;
    unsigned char field_1;
};
struct helper_ucomiss_wrapper_145_ret_type {
    uint64_t field_0;
    unsigned char field_1;
};
struct helper_cvtsq2ss_wrapper_140_ret_type {
    uint64_t field_0;
    unsigned char field_1;
};
struct helper_addss_wrapper_170_ret_type {
    uint64_t field_0;
    unsigned char field_1;
};
struct helper_addss_wrapper_141_ret_type {
    uint64_t field_0;
    unsigned char field_1;
};
struct helper_mulss_wrapper_171_ret_type {
    uint64_t field_0;
    unsigned char field_1;
};
struct helper_ucomiss_wrapper_172_ret_type {
    uint64_t field_0;
    unsigned char field_1;
};
struct helper_mulss_wrapper_173_ret_type {
    uint64_t field_0;
    unsigned char field_1;
};
struct indirect_placeholder_103_ret_type {
    uint64_t field_0;
    uint64_t field_1;
};
extern uint64_t revng_init_local_sp(uint64_t param_0);
extern uint64_t init_rbx(void);
extern uint64_t init_rbp(void);
extern void indirect_placeholder(void);
extern uint64_t init_state_0x8558(void);
extern unsigned char init_state_0x8549(void);
extern unsigned char init_state_0x854c(void);
extern unsigned char init_state_0x8548(void);
extern unsigned char init_state_0x854b(void);
extern unsigned char init_state_0x8547(void);
extern unsigned char init_state_0x854d(void);
extern uint64_t init_state_0x8560(void);
extern struct helper_pxor_xmm_wrapper_134_ret_type helper_pxor_xmm_wrapper_134(struct type_4 *param_0, struct type_6 *param_1, struct type_6 *param_2, uint64_t param_3, uint64_t param_4);
extern uint64_t init_cc_src2(void);
extern uint64_t helper_cc_compute_c_wrapper(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint32_t param_3);
extern uint64_t init_r12(void);
extern uint64_t init_state_0x8598(void);
extern uint64_t init_state_0x85a0(void);
extern uint64_t init_rax(void);
extern uint64_t init_state_0x8d58(void);
extern struct helper_pxor_xmm_wrapper_139_ret_type helper_pxor_xmm_wrapper_139(struct type_4 *param_0, struct type_6 *param_1, struct type_6 *param_2, uint64_t param_3, uint64_t param_4);
extern struct helper_cvtsq2ss_wrapper_ret_type helper_cvtsq2ss_wrapper(struct type_4 *param_0, struct type_6 *param_1, uint64_t param_2, unsigned char param_3, unsigned char param_4, unsigned char param_5, unsigned char param_6);
extern struct helper_addss_wrapper_ret_type helper_addss_wrapper(struct type_4 *param_0, struct type_6 *param_1, struct type_6 *param_2, uint64_t param_3, unsigned char param_4, unsigned char param_5, unsigned char param_6, unsigned char param_7, unsigned char param_8, unsigned char param_9);
extern struct helper_mulss_wrapper_142_ret_type helper_mulss_wrapper_142(struct type_4 *param_0, struct type_6 *param_1, struct type_6 *param_2, uint64_t param_3, uint64_t param_4, unsigned char param_5, unsigned char param_6, unsigned char param_7, unsigned char param_8, unsigned char param_9, unsigned char param_10);
extern struct helper_ucomiss_wrapper_143_ret_type helper_ucomiss_wrapper_143(struct type_4 *param_0, struct type_6 *param_1, struct type_6 *param_2, uint64_t param_3, uint64_t param_4, unsigned char param_5, unsigned char param_6);
extern struct helper_cvttss2sq_wrapper_ret_type helper_cvttss2sq_wrapper(struct type_4 *param_0, struct type_6 *param_1, uint64_t param_2, unsigned char param_3, unsigned char param_4);
extern struct helper_subss_wrapper_ret_type helper_subss_wrapper(struct type_4 *param_0, struct type_6 *param_1, struct type_6 *param_2, uint64_t param_3, uint64_t param_4, unsigned char param_5, unsigned char param_6, unsigned char param_7, unsigned char param_8, unsigned char param_9, unsigned char param_10);
extern uint64_t indirect_placeholder_10(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint64_t param_3);
extern uint64_t indirect_placeholder_12(uint64_t param_0);
extern struct helper_ucomiss_wrapper_145_ret_type helper_ucomiss_wrapper_145(struct type_4 *param_0, struct type_6 *param_1, struct type_6 *param_2, uint64_t param_3, uint64_t param_4, unsigned char param_5, unsigned char param_6);
extern uint64_t init_rcx(void);
extern uint64_t init_r9(void);
extern uint64_t init_r10(void);
extern uint64_t init_r8(void);
extern struct helper_cvtsq2ss_wrapper_140_ret_type helper_cvtsq2ss_wrapper_140(struct type_4 *param_0, struct type_6 *param_1, uint64_t param_2, unsigned char param_3, unsigned char param_4, unsigned char param_5, unsigned char param_6);
extern struct helper_addss_wrapper_170_ret_type helper_addss_wrapper_170(struct type_4 *param_0, struct type_6 *param_1, struct type_6 *param_2, uint64_t param_3, uint64_t param_4, unsigned char param_5, unsigned char param_6, unsigned char param_7, unsigned char param_8, unsigned char param_9, unsigned char param_10);
extern struct helper_addss_wrapper_141_ret_type helper_addss_wrapper_141(struct type_4 *param_0, struct type_6 *param_1, struct type_6 *param_2, uint64_t param_3, unsigned char param_4, unsigned char param_5, unsigned char param_6, unsigned char param_7, unsigned char param_8, unsigned char param_9);
extern struct helper_mulss_wrapper_171_ret_type helper_mulss_wrapper_171(struct type_4 *param_0, struct type_6 *param_1, struct type_6 *param_2, uint64_t param_3, uint64_t param_4, unsigned char param_5, unsigned char param_6, unsigned char param_7, unsigned char param_8, unsigned char param_9, unsigned char param_10);
extern struct helper_ucomiss_wrapper_172_ret_type helper_ucomiss_wrapper_172(struct type_4 *param_0, struct type_6 *param_1, struct type_6 *param_2, uint64_t param_3, uint64_t param_4, unsigned char param_5, unsigned char param_6);
extern struct helper_mulss_wrapper_173_ret_type helper_mulss_wrapper_173(struct type_4 *param_0, struct type_6 *param_1, struct type_6 *param_2, uint64_t param_3, uint64_t param_4, unsigned char param_5, unsigned char param_6, unsigned char param_7, unsigned char param_8, unsigned char param_9, unsigned char param_10);
extern uint64_t indirect_placeholder_31(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint64_t param_3, uint64_t param_4, uint64_t param_5);
extern struct indirect_placeholder_103_ret_type indirect_placeholder_103(uint64_t param_0);
uint64_t bb_hash_insert_if_absent(uint64_t rdi, uint64_t rdx, uint64_t rsi) {
    struct helper_pxor_xmm_wrapper_134_ret_type var_69;
    struct helper_pxor_xmm_wrapper_139_ret_type var_58;
    struct helper_cvtsq2ss_wrapper_ret_type var_43;
    uint64_t state_0x8558_5;
    uint64_t var_44;
    struct helper_cvtsq2ss_wrapper_ret_type var_45;
    struct helper_addss_wrapper_ret_type var_46;
    uint64_t rcx_4;
    struct helper_pxor_xmm_wrapper_134_ret_type var_31;
    struct helper_pxor_xmm_wrapper_134_ret_type var_25;
    struct helper_cvttss2sq_wrapper_ret_type var_96;
    struct helper_cvttss2sq_wrapper_ret_type var_92;
    struct helper_cvtsq2ss_wrapper_ret_type var_60;
    struct helper_pxor_xmm_wrapper_134_ret_type var_61;
    uint64_t state_0x8558_6;
    uint64_t var_0;
    uint64_t var_1;
    uint64_t var_2;
    uint64_t var_3;
    uint64_t var_4;
    uint64_t var_5;
    uint64_t var_6;
    uint64_t var_7;
    uint64_t var_8;
    uint64_t var_9;
    uint64_t var_10;
    uint64_t var_11;
    uint64_t var_12;
    uint64_t var_13;
    uint64_t var_14;
    unsigned char var_15;
    unsigned char var_16;
    unsigned char var_17;
    unsigned char var_18;
    unsigned char var_19;
    unsigned char var_20;
    uint64_t *_pre_phi185;
    unsigned char storemerge;
    uint64_t var_98;
    uint64_t rcx_3;
    uint64_t var_99;
    uint64_t local_sp_2;
    uint64_t state_0x8598_1;
    uint64_t var_59;
    uint64_t state_0x8558_0;
    uint64_t state_0x85a0_3;
    uint64_t state_0x8d58_2;
    uint64_t var_62;
    uint64_t var_63;
    struct helper_cvtsq2ss_wrapper_140_ret_type var_64;
    uint64_t var_65;
    struct helper_addss_wrapper_170_ret_type var_66;
    uint64_t state_0x8560_0;
    uint64_t state_0x8598_0;
    uint64_t state_0x85a0_0;
    unsigned char storemerge3;
    uint64_t rcx_0;
    unsigned char storemerge2;
    uint64_t var_67;
    bool var_68;
    uint64_t var_70;
    struct helper_cvtsq2ss_wrapper_140_ret_type var_71;
    uint64_t state_0x8560_4;
    uint64_t var_72;
    struct helper_cvtsq2ss_wrapper_140_ret_type var_73;
    struct helper_addss_wrapper_141_ret_type var_74;
    uint64_t rcx_1;
    unsigned char storemerge1;
    struct helper_mulss_wrapper_171_ret_type var_75;
    struct helper_ucomiss_wrapper_172_ret_type var_76;
    unsigned char var_77;
    unsigned char var_78;
    uint64_t var_79;
    struct helper_mulss_wrapper_142_ret_type var_80;
    uint64_t var_81;
    unsigned char var_82;
    uint64_t state_0x8558_1;
    struct helper_mulss_wrapper_173_ret_type var_83;
    unsigned char state_0x8549_0;
    struct helper_ucomiss_wrapper_143_ret_type var_84;
    uint64_t var_85;
    unsigned char var_86;
    uint64_t var_87;
    struct helper_ucomiss_wrapper_143_ret_type var_88;
    uint64_t var_89;
    unsigned char var_90;
    uint64_t var_91;
    uint64_t rsi3_0;
    uint64_t var_93;
    struct helper_subss_wrapper_ret_type var_94;
    uint64_t var_95;
    uint64_t state_0x8558_2;
    uint64_t state_0x8d58_0;
    unsigned char state_0x8549_1;
    uint64_t var_97;
    uint64_t merge;
    uint64_t var_24;
    uint64_t var_26;
    struct helper_cvtsq2ss_wrapper_140_ret_type var_27;
    uint64_t var_28;
    unsigned char var_29;
    uint64_t state_0x8558_4;
    uint64_t state_0x8558_3;
    uint64_t state_0x8598_4;
    uint64_t state_0x8560_1;
    uint64_t state_0x85a0_4;
    uint64_t state_0x8598_2;
    uint64_t state_0x8d58_3;
    uint64_t state_0x85a0_2;
    unsigned char state_0x8549_3;
    uint64_t state_0x8d58_1;
    uint64_t rbx_2;
    unsigned char state_0x8549_2;
    uint64_t rcx_5;
    uint64_t rbx_0;
    uint64_t rax_3;
    uint64_t rcx_2;
    uint64_t local_sp_3;
    uint64_t rax_0;
    uint64_t local_sp_0;
    uint64_t var_32;
    struct helper_cvtsq2ss_wrapper_140_ret_type var_33;
    struct helper_addss_wrapper_141_ret_type var_34;
    uint64_t state_0x8560_2;
    uint64_t state_0x8598_3;
    uint64_t rbx_1;
    uint64_t local_sp_1;
    uint64_t *var_35;
    uint64_t var_36;
    uint64_t var_37;
    uint64_t *var_38;
    uint64_t var_39;
    bool var_40;
    struct helper_pxor_xmm_wrapper_139_ret_type var_41;
    uint64_t var_42;
    uint64_t var_47;
    uint64_t var_48;
    struct helper_mulss_wrapper_142_ret_type var_49;
    uint64_t var_50;
    struct helper_ucomiss_wrapper_145_ret_type var_51;
    uint64_t var_52;
    unsigned char var_53;
    uint64_t var_100;
    uint64_t *_cast;
    uint64_t *var_101;
    uint64_t var_102;
    uint64_t *var_103;
    uint64_t rdx2_0;
    uint64_t rax_1;
    uint64_t *var_106;
    uint64_t var_107;
    uint64_t *var_108;
    struct indirect_placeholder_103_ret_type var_104;
    uint64_t var_105;
    uint64_t *var_109;
    uint64_t *var_110;
    uint64_t var_54;
    uint64_t var_55;
    uint64_t var_56;
    uint64_t var_57;
    uint64_t var_21;
    uint64_t var_22;
    uint64_t var_23;
    uint64_t var_30;
    var_0 = revng_init_local_sp(0UL);
    var_1 = init_rax();
    var_2 = init_rcx();
    var_3 = init_r10();
    var_4 = init_rbx();
    var_5 = init_r9();
    var_6 = init_rbp();
    var_7 = init_r8();
    var_8 = init_cc_src2();
    var_9 = init_state_0x8558();
    var_10 = init_state_0x8560();
    var_11 = init_r12();
    var_12 = init_state_0x8598();
    var_13 = init_state_0x85a0();
    var_14 = init_state_0x8d58();
    var_15 = init_state_0x8549();
    var_16 = init_state_0x854c();
    var_17 = init_state_0x8548();
    var_18 = init_state_0x854b();
    var_19 = init_state_0x8547();
    var_20 = init_state_0x854d();
    *(uint64_t *)(var_0 + (-8L)) = var_11;
    *(uint64_t *)(var_0 + (-16L)) = var_6;
    *(uint64_t *)(var_0 + (-24L)) = var_4;
    state_0x8558_6 = var_9;
    rcx_3 = 0UL;
    state_0x8d58_2 = var_14;
    state_0x8560_4 = var_10;
    merge = 0UL;
    state_0x8558_4 = var_9;
    state_0x8558_3 = var_9;
    state_0x8598_4 = var_12;
    state_0x8560_1 = var_10;
    state_0x85a0_4 = var_13;
    state_0x8598_2 = var_12;
    state_0x8d58_3 = var_14;
    state_0x85a0_2 = var_13;
    state_0x8549_3 = var_15;
    state_0x8d58_1 = var_14;
    rbx_2 = var_4;
    state_0x8549_2 = var_15;
    rcx_5 = var_2;
    rbx_0 = rdi;
    rax_3 = var_1;
    rcx_2 = 0UL;
    state_0x8560_2 = var_10;
    rbx_1 = rdi;
    if (rsi != 0UL) {
        var_21 = var_0 + (-32L);
        var_22 = var_0 + (-48L);
        *(uint64_t *)var_22 = 4228934UL;
        var_23 = indirect_placeholder_10(0UL, rdi, var_21, rsi);
        local_sp_0 = var_22;
        local_sp_1 = var_22;
        if (var_23 != 0UL) {
            if (rdx != 0UL) {
                *(uint64_t *)rdx = var_23;
            }
            return merge;
        }
        var_24 = *(uint64_t *)(rdi + 24UL);
        rax_0 = var_24;
        if ((long)var_24 < (long)0UL) {
            var_25 = helper_pxor_xmm_wrapper_134((struct type_4 *)(0UL), (struct type_6 *)(840UL), (struct type_6 *)(840UL), var_12, var_13);
            var_26 = var_25.field_1;
            var_27 = helper_cvtsq2ss_wrapper_140((struct type_4 *)(0UL), (struct type_6 *)(840UL), var_24, var_15, var_17, var_18, var_19);
            var_28 = var_27.field_0;
            var_29 = var_27.field_1;
            state_0x8598_3 = var_28;
            state_0x85a0_3 = var_26;
            storemerge = var_29;
        } else {
            var_31 = helper_pxor_xmm_wrapper_134((struct type_4 *)(0UL), (struct type_6 *)(840UL), (struct type_6 *)(840UL), state_0x8598_2, state_0x85a0_2);
            var_32 = var_31.field_1;
            var_33 = helper_cvtsq2ss_wrapper_140((struct type_4 *)(0UL), (struct type_6 *)(840UL), (rax_0 >> 1UL) | (rax_0 & 1UL), state_0x8549_2, var_17, var_18, var_19);
            var_34 = helper_addss_wrapper_141((struct type_4 *)(0UL), (struct type_6 *)(840UL), (struct type_6 *)(840UL), var_33.field_0, var_33.field_1, var_16, var_17, var_18, var_19, var_20);
            storemerge = var_34.field_1;
            rcx_3 = rcx_2;
            state_0x85a0_3 = var_32;
            state_0x8d58_2 = state_0x8d58_1;
            state_0x8558_4 = state_0x8558_3;
            state_0x8560_2 = state_0x8560_1;
            state_0x8598_3 = var_34.field_0;
            rbx_1 = rbx_0;
            local_sp_1 = local_sp_0;
        }
        var_35 = (uint64_t *)(rbx_1 + 16UL);
        var_36 = *var_35;
        var_37 = rbx_1 + 40UL;
        var_38 = (uint64_t *)var_37;
        var_39 = *var_38;
        var_40 = ((long)var_36 < (long)0UL);
        var_41 = helper_pxor_xmm_wrapper_139((struct type_4 *)(0UL), (struct type_6 *)(776UL), (struct type_6 *)(776UL), state_0x8558_4, state_0x8560_2);
        var_42 = var_41.field_1;
        rcx_4 = rcx_3;
        local_sp_2 = local_sp_1;
        state_0x8598_0 = state_0x8598_3;
        state_0x85a0_0 = state_0x85a0_3;
        merge = 4294967295UL;
        rbx_2 = rbx_1;
        rcx_5 = 0UL;
        if (var_40) {
            var_44 = (var_36 >> 1UL) | (var_36 & 1UL);
            var_45 = helper_cvtsq2ss_wrapper((struct type_4 *)(0UL), (struct type_6 *)(776UL), var_44, storemerge, var_17, var_18, var_19);
            var_46 = helper_addss_wrapper((struct type_4 *)(0UL), (struct type_6 *)(776UL), (struct type_6 *)(776UL), var_45.field_0, var_45.field_1, var_16, var_17, var_18, var_19, var_20);
            state_0x8558_5 = var_46.field_0;
            rcx_4 = var_44;
            storemerge3 = var_46.field_1;
        } else {
            var_43 = helper_cvtsq2ss_wrapper((struct type_4 *)(0UL), (struct type_6 *)(776UL), var_36, storemerge, var_17, var_18, var_19);
            state_0x8558_5 = var_43.field_0;
            storemerge3 = var_43.field_1;
        }
        var_47 = (uint64_t)*(uint32_t *)(var_39 + 8UL);
        var_48 = state_0x8d58_2 & (-4294967296L);
        var_49 = helper_mulss_wrapper_142((struct type_4 *)(0UL), (struct type_6 *)(776UL), (struct type_6 *)(2824UL), state_0x8558_5, var_48 | var_47, storemerge3, var_16, var_17, var_18, var_19, var_20);
        var_50 = var_49.field_0;
        var_51 = helper_ucomiss_wrapper_145((struct type_4 *)(0UL), (struct type_6 *)(840UL), (struct type_6 *)(776UL), var_50, state_0x8598_3, var_49.field_1, var_16);
        var_52 = var_51.field_0;
        var_53 = var_51.field_1;
        rcx_0 = rcx_4;
        if ((var_52 & 65UL) != 0UL) {
            var_100 = *(uint64_t *)(local_sp_2 + 8UL);
            _cast = (uint64_t *)var_100;
            merge = 1UL;
            rdx2_0 = var_100;
            if (*_cast == 0UL) {
                *_cast = rsi;
                var_109 = (uint64_t *)(rbx_1 + 32UL);
                *var_109 = (*var_109 + 1UL);
                var_110 = (uint64_t *)(rbx_1 + 24UL);
                *var_110 = (*var_110 + 1UL);
                return merge;
            }
            var_101 = (uint64_t *)(rbx_1 + 72UL);
            var_102 = *var_101;
            rax_1 = var_102;
            if (var_102 == 0UL) {
                var_103 = (uint64_t *)(var_102 + 8UL);
                *var_101 = *var_103;
                _pre_phi185 = var_103;
            } else {
                *(uint64_t *)(local_sp_2 + (-8L)) = 4229487UL;
                var_104 = indirect_placeholder_103(16UL);
                var_105 = var_104.field_0;
                rax_1 = var_105;
                if (var_105 != 0UL) {
                    return merge;
                }
                _pre_phi185 = (uint64_t *)(var_105 + 8UL);
                rdx2_0 = *(uint64_t *)local_sp_2;
            }
            var_106 = (uint64_t *)(rdx2_0 + 8UL);
            var_107 = *var_106;
            *(uint64_t *)rax_1 = rsi;
            *_pre_phi185 = var_107;
            *var_106 = rax_1;
            var_108 = (uint64_t *)(rbx_1 + 32UL);
            *var_108 = (*var_108 + 1UL);
            return 1UL;
        }
        var_54 = local_sp_1 + (-8L);
        *(uint64_t *)var_54 = 4229129UL;
        indirect_placeholder_12(var_37);
        var_55 = *var_35;
        var_56 = *var_38;
        var_57 = (uint64_t)*(uint32_t *)(var_56 + 8UL);
        local_sp_2 = var_54;
        if ((long)var_55 < (long)0UL) {
            var_61 = helper_pxor_xmm_wrapper_134((struct type_4 *)(0UL), (struct type_6 *)(840UL), (struct type_6 *)(840UL), state_0x8598_3, state_0x85a0_3);
            var_62 = var_61.field_1;
            var_63 = (var_55 >> 1UL) | (var_55 & 1UL);
            var_64 = helper_cvtsq2ss_wrapper_140((struct type_4 *)(0UL), (struct type_6 *)(840UL), var_63, var_53, var_17, var_18, var_19);
            var_65 = var_64.field_0;
            var_66 = helper_addss_wrapper_170((struct type_4 *)(0UL), (struct type_6 *)(776UL), (struct type_6 *)(840UL), var_65, var_65, var_64.field_1, var_16, var_17, var_18, var_19, var_20);
            state_0x8558_0 = var_66.field_0;
            state_0x8560_0 = var_62;
            state_0x8598_0 = var_65;
            state_0x85a0_0 = var_62;
            rcx_0 = var_63;
            storemerge2 = var_66.field_1;
        } else {
            var_58 = helper_pxor_xmm_wrapper_139((struct type_4 *)(0UL), (struct type_6 *)(776UL), (struct type_6 *)(776UL), var_50, var_42);
            var_59 = var_58.field_1;
            var_60 = helper_cvtsq2ss_wrapper((struct type_4 *)(0UL), (struct type_6 *)(776UL), var_55, var_53, var_17, var_18, var_19);
            state_0x8558_0 = var_60.field_0;
            state_0x8560_0 = var_59;
            storemerge2 = var_60.field_1;
        }
        var_67 = *(uint64_t *)(rbx_1 + 24UL);
        var_68 = ((long)var_67 < (long)0UL);
        var_69 = helper_pxor_xmm_wrapper_134((struct type_4 *)(0UL), (struct type_6 *)(840UL), (struct type_6 *)(840UL), state_0x8598_0, state_0x85a0_0);
        var_70 = var_69.field_1;
        rcx_1 = rcx_0;
        state_0x8560_4 = state_0x8560_0;
        state_0x85a0_4 = var_70;
        if (var_68) {
            var_72 = (var_67 >> 1UL) | (var_67 & 1UL);
            var_73 = helper_cvtsq2ss_wrapper_140((struct type_4 *)(0UL), (struct type_6 *)(840UL), var_72, storemerge2, var_17, var_18, var_19);
            var_74 = helper_addss_wrapper_141((struct type_4 *)(0UL), (struct type_6 *)(840UL), (struct type_6 *)(840UL), var_73.field_0, var_73.field_1, var_16, var_17, var_18, var_19, var_20);
            state_0x8598_1 = var_74.field_0;
            rcx_1 = var_72;
            storemerge1 = var_74.field_1;
        } else {
            var_71 = helper_cvtsq2ss_wrapper_140((struct type_4 *)(0UL), (struct type_6 *)(840UL), var_67, storemerge2, var_17, var_18, var_19);
            state_0x8598_1 = var_71.field_0;
            storemerge1 = var_71.field_1;
        }
        var_75 = helper_mulss_wrapper_171((struct type_4 *)(0UL), (struct type_6 *)(968UL), (struct type_6 *)(776UL), state_0x8558_0, var_57, storemerge1, var_16, var_17, var_18, var_19, var_20);
        var_76 = helper_ucomiss_wrapper_172((struct type_4 *)(0UL), (struct type_6 *)(840UL), (struct type_6 *)(968UL), state_0x8598_1, var_75.field_0, var_75.field_1, var_16);
        state_0x8598_4 = state_0x8598_1;
        if ((var_76.field_0 & 65UL) != 0UL) {
            var_100 = *(uint64_t *)(local_sp_2 + 8UL);
            _cast = (uint64_t *)var_100;
            merge = 1UL;
            rdx2_0 = var_100;
            if (*_cast == 0UL) {
                *_cast = rsi;
                var_109 = (uint64_t *)(rbx_1 + 32UL);
                *var_109 = (*var_109 + 1UL);
                var_110 = (uint64_t *)(rbx_1 + 24UL);
                *var_110 = (*var_110 + 1UL);
                return merge;
            }
            var_101 = (uint64_t *)(rbx_1 + 72UL);
            var_102 = *var_101;
            rax_1 = var_102;
            if (var_102 == 0UL) {
                var_103 = (uint64_t *)(var_102 + 8UL);
                *var_101 = *var_103;
                _pre_phi185 = var_103;
            } else {
                *(uint64_t *)(local_sp_2 + (-8L)) = 4229487UL;
                var_104 = indirect_placeholder_103(16UL);
                var_105 = var_104.field_0;
                rax_1 = var_105;
                if (var_105 != 0UL) {
                    return merge;
                }
                _pre_phi185 = (uint64_t *)(var_105 + 8UL);
                rdx2_0 = *(uint64_t *)local_sp_2;
            }
            var_106 = (uint64_t *)(rdx2_0 + 8UL);
            var_107 = *var_106;
            *(uint64_t *)rax_1 = rsi;
            *_pre_phi185 = var_107;
            *var_106 = rax_1;
            var_108 = (uint64_t *)(rbx_1 + 32UL);
            *var_108 = (*var_108 + 1UL);
            return 1UL;
        }
        var_77 = var_76.field_1;
        var_78 = *(unsigned char *)(var_56 + 16UL);
        var_79 = (uint64_t)var_78;
        var_80 = helper_mulss_wrapper_142((struct type_4 *)(0UL), (struct type_6 *)(776UL), (struct type_6 *)(2824UL), state_0x8558_0, var_48 | (uint64_t)*(uint32_t *)(var_56 + 12UL), var_77, var_16, var_17, var_18, var_19, var_20);
        var_81 = var_80.field_0;
        var_82 = var_80.field_1;
        state_0x8558_1 = var_81;
        state_0x8549_0 = var_82;
        if (var_78 == '\x00') {
            var_83 = helper_mulss_wrapper_173((struct type_4 *)(0UL), (struct type_6 *)(776UL), (struct type_6 *)(904UL), var_81, var_57, var_82, var_16, var_17, var_18, var_19, var_20);
            state_0x8558_1 = var_83.field_0;
            state_0x8549_0 = var_83.field_1;
        }
        var_84 = helper_ucomiss_wrapper_143((struct type_4 *)(0UL), (struct type_6 *)(776UL), (struct type_6 *)(2824UL), state_0x8558_1, var_48 | (uint64_t)*(uint32_t *)4273320UL, state_0x8549_0, var_16);
        var_85 = helper_cc_compute_c_wrapper(var_79, var_84.field_0, var_8, 1U);
        state_0x8558_2 = state_0x8558_1;
        if (var_85 == 0UL) {
            return merge;
        }
        var_86 = var_84.field_1;
        var_87 = var_48 | (uint64_t)*(uint32_t *)4273324UL;
        var_88 = helper_ucomiss_wrapper_143((struct type_4 *)(0UL), (struct type_6 *)(776UL), (struct type_6 *)(2824UL), state_0x8558_1, var_87, var_86, var_16);
        var_89 = var_88.field_0;
        var_90 = var_88.field_1;
        var_91 = helper_cc_compute_c_wrapper(var_79, var_89, var_8, 1U);
        state_0x8d58_0 = var_87;
        if (var_91 == 0UL) {
            var_93 = var_48 | (uint64_t)*(uint32_t *)4273324UL;
            var_94 = helper_subss_wrapper((struct type_4 *)(0UL), (struct type_6 *)(776UL), (struct type_6 *)(2824UL), state_0x8558_1, var_93, var_90, var_16, var_17, var_18, var_19, var_20);
            var_95 = var_94.field_0;
            var_96 = helper_cvttss2sq_wrapper((struct type_4 *)(0UL), (struct type_6 *)(776UL), var_95, var_94.field_1, var_16);
            rsi3_0 = var_96.field_0 ^ (-9223372036854775808L);
            state_0x8558_2 = var_95;
            state_0x8d58_0 = var_93;
            state_0x8549_1 = var_96.field_1;
        } else {
            var_92 = helper_cvttss2sq_wrapper((struct type_4 *)(0UL), (struct type_6 *)(776UL), state_0x8558_1, var_90, var_16);
            rsi3_0 = var_92.field_0;
            state_0x8549_1 = var_92.field_1;
        }
        *(uint64_t *)(local_sp_1 + (-16L)) = 4229252UL;
        var_97 = indirect_placeholder_31(rcx_1, var_3, var_5, rbx_1, var_7, rsi3_0);
        state_0x8558_6 = state_0x8558_2;
        state_0x8d58_3 = state_0x8d58_0;
        state_0x8549_3 = state_0x8549_1;
        if ((uint64_t)(unsigned char)var_97 == 0UL) {
            return merge;
        }
        var_98 = local_sp_1 + (-24L);
        *(uint64_t *)var_98 = 4229278UL;
        var_99 = indirect_placeholder_10(0UL, rbx_1, var_54, rsi);
        local_sp_2 = var_98;
        rax_3 = var_99;
        local_sp_3 = var_98;
        if (var_99 != 0UL) {
            var_100 = *(uint64_t *)(local_sp_2 + 8UL);
            _cast = (uint64_t *)var_100;
            merge = 1UL;
            rdx2_0 = var_100;
            if (*_cast == 0UL) {
                *_cast = rsi;
                var_109 = (uint64_t *)(rbx_1 + 32UL);
                *var_109 = (*var_109 + 1UL);
                var_110 = (uint64_t *)(rbx_1 + 24UL);
                *var_110 = (*var_110 + 1UL);
                return merge;
            }
            var_101 = (uint64_t *)(rbx_1 + 72UL);
            var_102 = *var_101;
            rax_1 = var_102;
            if (var_102 == 0UL) {
                var_103 = (uint64_t *)(var_102 + 8UL);
                *var_101 = *var_103;
                _pre_phi185 = var_103;
            } else {
                *(uint64_t *)(local_sp_2 + (-8L)) = 4229487UL;
                var_104 = indirect_placeholder_103(16UL);
                var_105 = var_104.field_0;
                rax_1 = var_105;
                if (var_105 != 0UL) {
                    return merge;
                }
                _pre_phi185 = (uint64_t *)(var_105 + 8UL);
                rdx2_0 = *(uint64_t *)local_sp_2;
            }
            var_106 = (uint64_t *)(rdx2_0 + 8UL);
            var_107 = *var_106;
            *(uint64_t *)rax_1 = rsi;
            *_pre_phi185 = var_107;
            *var_106 = rax_1;
            var_108 = (uint64_t *)(rbx_1 + 32UL);
            *var_108 = (*var_108 + 1UL);
            return 1UL;
        }
    }
    local_sp_3 = var_0 + (-40L);
}

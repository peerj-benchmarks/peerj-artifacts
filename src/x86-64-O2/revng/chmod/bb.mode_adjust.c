typedef unsigned char __u_char;
typedef unsigned short __u_short;
typedef unsigned int __u_int;
typedef unsigned long __u_long;
typedef signed char __int8_t;
typedef unsigned char __uint8_t;
typedef short __int16_t;
typedef unsigned short __uint16_t;
typedef int __int32_t;
typedef unsigned int __uint32_t;
typedef long __int64_t;
typedef unsigned long __uint64_t;
typedef __int8_t __int_least8_t;
typedef __uint8_t __uint_least8_t;
typedef __int16_t __int_least16_t;
typedef __uint16_t __uint_least16_t;
typedef __int32_t __int_least32_t;
typedef __uint32_t __uint_least32_t;
typedef __int64_t __int_least64_t;
typedef __uint64_t __uint_least64_t;
typedef long __quad_t;
typedef unsigned long __u_quad_t;
typedef long __intmax_t;
typedef unsigned long __uintmax_t;
typedef unsigned long __dev_t;
typedef unsigned int __uid_t;
typedef unsigned int __gid_t;
typedef unsigned long __ino_t;
typedef unsigned long __ino64_t;
typedef unsigned int __mode_t;
typedef unsigned long __nlink_t;
typedef long __off_t;
typedef long __off64_t;
typedef int __pid_t;
typedef struct {
    int __val[2];
} __fsid_t;
typedef long __clock_t;
typedef unsigned long __rlim_t;
typedef unsigned long __rlim64_t;
typedef unsigned int __id_t;
typedef long __time_t;
typedef unsigned int __useconds_t;
typedef long __suseconds_t;
typedef int __daddr_t;
typedef int __key_t;
typedef int __clockid_t;
typedef void *__timer_t;
typedef long __blksize_t;
typedef long __blkcnt_t;
typedef long __blkcnt64_t;
typedef unsigned long __fsblkcnt_t;
typedef unsigned long __fsblkcnt64_t;
typedef unsigned long __fsfilcnt_t;
typedef unsigned long __fsfilcnt64_t;
typedef long __fsword_t;
typedef long __ssize_t;
typedef long __syscall_slong_t;
typedef unsigned long __syscall_ulong_t;
typedef __off64_t __loff_t;
typedef char *__caddr_t;
typedef long __intptr_t;
typedef unsigned int __socklen_t;
typedef int __sig_atomic_t;
typedef __int8_t int8_t;
typedef __int16_t int16_t;
typedef __int32_t int32_t;
typedef __int64_t int64_t;
typedef __uint8_t uint8_t;
typedef __uint16_t uint16_t;
typedef __uint32_t uint32_t;
typedef __uint64_t uint64_t;
typedef __int_least8_t int_least8_t;
typedef __int_least16_t int_least16_t;
typedef __int_least32_t int_least32_t;
typedef __int_least64_t int_least64_t;
typedef __uint_least8_t uint_least8_t;
typedef __uint_least16_t uint_least16_t;
typedef __uint_least32_t uint_least32_t;
typedef __uint_least64_t uint_least64_t;
typedef signed char int_fast8_t;
typedef long int_fast16_t;
typedef long int_fast32_t;
typedef long int_fast64_t;
typedef unsigned char uint_fast8_t;
typedef unsigned long uint_fast16_t;
typedef unsigned long uint_fast32_t;
typedef unsigned long uint_fast64_t;
typedef long intptr_t;
typedef unsigned long uintptr_t;
typedef __intmax_t intmax_t;
typedef __uintmax_t uintmax_t;
typedef _Bool bool;
extern uint64_t revng_init_local_sp(uint64_t param_0);
extern uint64_t init_rbx(void);
extern uint64_t init_rbp(void);
extern uint64_t init_cc_src2(void);
extern uint64_t helper_cc_compute_c_wrapper(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint32_t param_3);
extern uint64_t init_r13(void);
extern uint64_t init_r12(void);
uint64_t bb_mode_adjust(uint64_t rcx, uint64_t r10, uint64_t rdi, uint64_t r8, uint64_t rdx, uint64_t rsi) {
    uint64_t rax_2;
    uint64_t var_0;
    uint64_t var_1;
    uint64_t var_2;
    uint64_t var_3;
    uint64_t var_4;
    uint64_t var_5;
    unsigned char var_6;
    uint64_t var_7;
    uint64_t rbp_2;
    uint64_t var_8;
    uint64_t var_9;
    uint64_t var_10;
    unsigned char rdi3_0_be_in;
    uint64_t rcx1_0;
    uint64_t rax_1;
    unsigned char rdi3_0_in;
    uint64_t rbp_0;
    uint64_t cc_src2_0;
    uint64_t rax_0;
    uint32_t var_11;
    uint32_t var_12;
    uint64_t var_13;
    uint64_t var_14;
    uint64_t var_15;
    uint64_t var_16;
    uint64_t var_17;
    uint64_t r102_0;
    uint64_t var_18;
    uint64_t var_19;
    uint64_t var_20;
    uint64_t var_21;
    uint64_t var_22;
    uint64_t var_23;
    uint64_t cc_src2_1;
    uint64_t var_24;
    bool var_25;
    uint64_t var_26;
    unsigned char var_27;
    uint64_t rbp_0_be;
    uint64_t rbp_1;
    uint64_t var_28;
    uint64_t var_29;
    uint64_t var_30;
    unsigned char var_31;
    uint64_t rax_0_be;
    unsigned char var_32;
    var_0 = revng_init_local_sp(0UL);
    var_1 = init_rbx();
    var_2 = init_rbp();
    var_3 = init_r13();
    var_4 = init_cc_src2();
    var_5 = init_r12();
    var_6 = *(unsigned char *)(rcx + 1UL);
    *(uint64_t *)(var_0 + (-8L)) = var_3;
    var_7 = (uint64_t)((uint16_t)rdi & (unsigned short)4095U);
    *(uint64_t *)(var_0 + (-16L)) = var_5;
    *(uint64_t *)(var_0 + (-24L)) = var_2;
    *(uint64_t *)(var_0 + (-32L)) = var_1;
    rax_2 = var_7;
    rbp_2 = 0UL;
    rcx1_0 = rcx;
    rdi3_0_in = var_6;
    rbp_0 = 0UL;
    cc_src2_0 = var_4;
    rax_0 = var_7;
    if (var_6 != '\x00') {
        var_8 = (uint64_t)(unsigned char)rsi;
        var_9 = rdx ^ 4294967295UL;
        var_10 = rsi + (-1L);
        while (1U)
            {
                var_11 = *(uint32_t *)(rcx1_0 + 12UL);
                var_12 = *(uint32_t *)(rcx1_0 + 4UL);
                var_13 = (uint64_t)var_12;
                var_14 = helper_cc_compute_c_wrapper(var_10, 1UL, cc_src2_0, 14U);
                var_15 = ((uint64_t)(((unsigned short)0U - (uint16_t)var_14) & (unsigned short)3072U) ^ 3072UL) & (uint64_t)(var_11 ^ 3072U);
                var_16 = (uint64_t)(rdi3_0_in + '\xfe');
                var_17 = (uint64_t)*(uint32_t *)(rcx1_0 + 8UL);
                r102_0 = var_17;
                cc_src2_1 = var_14;
                rbp_1 = rbp_0;
                rax_1 = rax_0;
                if (var_16 == 0UL) {
                    r102_0 = (((rax_0 & 73UL) | var_8) == 0UL) ? var_17 : (var_17 | 73UL);
                } else {
                    if ((uint64_t)(rdi3_0_in + '\xfd') == 0UL) {
                        var_18 = rax_0 & var_17;
                        var_19 = helper_cc_compute_c_wrapper((uint64_t)((uint16_t)var_18 & (unsigned short)292U) + (-1L), 1UL, var_14, 16U);
                        var_20 = (uint64_t)(((unsigned short)0U - (uint16_t)var_19) & (unsigned short)292U);
                        var_21 = helper_cc_compute_c_wrapper((uint64_t)((unsigned char)var_18 & '\x92') + (-1L), 1UL, var_19, 16U);
                        var_22 = var_20 | (uint64_t)(('\x00' - (unsigned char)var_21) & '\x92');
                        var_23 = helper_cc_compute_c_wrapper((var_18 & 73UL) + (-1L), 1UL, var_21, 16U);
                        r102_0 = var_18 | ((var_22 | ((0UL - var_23) & 73UL)) ^ 511UL);
                        cc_src2_1 = var_23;
                    }
                }
                var_24 = var_15 ^ 4294967295UL;
                var_25 = (var_12 == 0U);
                var_26 = (r102_0 & var_24) & (uint64_t)(uint32_t)(var_25 ? var_9 : var_13);
                var_27 = *(unsigned char *)rcx1_0;
                cc_src2_0 = cc_src2_1;
                if ((uint64_t)(var_27 + '\xd3') == 0UL) {
                    rbp_1 = (uint64_t)(uint32_t)rbp_0 | var_26;
                    rax_1 = rax_0 & (var_26 ^ 4294967295UL);
                } else {
                    if ((uint64_t)(var_27 + '\xc3') != 0UL) {
                        var_28 = var_15 | (var_25 ? 0UL : (var_13 ^ 4294967295UL));
                        var_29 = (rax_0 & var_28) | var_26;
                        var_30 = (uint64_t)(uint32_t)rbp_0 | ((uint64_t)((uint16_t)var_28 & (unsigned short)4095U) ^ 4095UL);
                        var_31 = *(unsigned char *)(rcx1_0 + 17UL);
                        rax_2 = var_29;
                        rbp_2 = var_30;
                        rdi3_0_be_in = var_31;
                        rbp_0_be = var_30;
                        rax_0_be = var_29;
                        if (var_31 == '\x00') {
                            break;
                        }
                        rcx1_0 = rcx1_0 + 16UL;
                        rdi3_0_in = rdi3_0_be_in;
                        rbp_0 = rbp_0_be;
                        rax_0 = rax_0_be;
                        continue;
                    }
                    if ((uint64_t)(var_27 + '\xd5') == 0UL) {
                        rbp_1 = (uint64_t)(uint32_t)rbp_0 | var_26;
                        rax_1 = (uint64_t)(uint32_t)rax_0 | var_26;
                    }
                }
                var_32 = *(unsigned char *)(rcx1_0 + 17UL);
                rax_2 = rax_1;
                rbp_2 = rbp_1;
                rdi3_0_be_in = var_32;
                rbp_0_be = rbp_1;
                rax_0_be = rax_1;
                if (var_32 == '\x00') {
                    break;
                }
                rcx1_0 = rcx1_0 + 16UL;
                rdi3_0_in = rdi3_0_be_in;
                rbp_0 = rbp_0_be;
                rax_0 = rax_0_be;
                continue;
            }
    }
    if (r8 == 0UL) {
        return rax_2;
    }
    *(uint32_t *)r8 = (uint32_t)rbp_2;
    return rax_2;
}

typedef unsigned char __u_char;
typedef unsigned short __u_short;
typedef unsigned int __u_int;
typedef unsigned long __u_long;
typedef signed char __int8_t;
typedef unsigned char __uint8_t;
typedef short __int16_t;
typedef unsigned short __uint16_t;
typedef int __int32_t;
typedef unsigned int __uint32_t;
typedef long __int64_t;
typedef unsigned long __uint64_t;
typedef __int8_t __int_least8_t;
typedef __uint8_t __uint_least8_t;
typedef __int16_t __int_least16_t;
typedef __uint16_t __uint_least16_t;
typedef __int32_t __int_least32_t;
typedef __uint32_t __uint_least32_t;
typedef __int64_t __int_least64_t;
typedef __uint64_t __uint_least64_t;
typedef long __quad_t;
typedef unsigned long __u_quad_t;
typedef long __intmax_t;
typedef unsigned long __uintmax_t;
typedef unsigned long __dev_t;
typedef unsigned int __uid_t;
typedef unsigned int __gid_t;
typedef unsigned long __ino_t;
typedef unsigned long __ino64_t;
typedef unsigned int __mode_t;
typedef unsigned long __nlink_t;
typedef long __off_t;
typedef long __off64_t;
typedef int __pid_t;
typedef struct {
    int __val[2];
} __fsid_t;
typedef long __clock_t;
typedef unsigned long __rlim_t;
typedef unsigned long __rlim64_t;
typedef unsigned int __id_t;
typedef long __time_t;
typedef unsigned int __useconds_t;
typedef long __suseconds_t;
typedef int __daddr_t;
typedef int __key_t;
typedef int __clockid_t;
typedef void *__timer_t;
typedef long __blksize_t;
typedef long __blkcnt_t;
typedef long __blkcnt64_t;
typedef unsigned long __fsblkcnt_t;
typedef unsigned long __fsblkcnt64_t;
typedef unsigned long __fsfilcnt_t;
typedef unsigned long __fsfilcnt64_t;
typedef long __fsword_t;
typedef long __ssize_t;
typedef long __syscall_slong_t;
typedef unsigned long __syscall_ulong_t;
typedef __off64_t __loff_t;
typedef char *__caddr_t;
typedef long __intptr_t;
typedef unsigned int __socklen_t;
typedef int __sig_atomic_t;
typedef __int8_t int8_t;
typedef __int16_t int16_t;
typedef __int32_t int32_t;
typedef __int64_t int64_t;
typedef __uint8_t uint8_t;
typedef __uint16_t uint16_t;
typedef __uint32_t uint32_t;
typedef __uint64_t uint64_t;
typedef __int_least8_t int_least8_t;
typedef __int_least16_t int_least16_t;
typedef __int_least32_t int_least32_t;
typedef __int_least64_t int_least64_t;
typedef __uint_least8_t uint_least8_t;
typedef __uint_least16_t uint_least16_t;
typedef __uint_least32_t uint_least32_t;
typedef __uint_least64_t uint_least64_t;
typedef signed char int_fast8_t;
typedef long int_fast16_t;
typedef long int_fast32_t;
typedef long int_fast64_t;
typedef unsigned char uint_fast8_t;
typedef unsigned long uint_fast16_t;
typedef unsigned long uint_fast32_t;
typedef unsigned long uint_fast64_t;
typedef long intptr_t;
typedef unsigned long uintptr_t;
typedef __intmax_t intmax_t;
typedef __uintmax_t uintmax_t;
extern void abort(void);
extern uint64_t revng_init_local_sp(uint64_t param_0);
extern uint64_t init_rbx(void);
extern uint64_t init_rbp(void);
extern void indirect_placeholder(void);
extern uint64_t init_r13(void);
extern uint64_t init_r12(void);
extern uint64_t init_r14(void);
extern uint64_t init_rax(void);
extern uint64_t helper_cc_compute_all_wrapper(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint32_t param_3);
extern uint64_t init_r15(void);
extern void indirect_placeholder_1(uint64_t param_0, uint64_t param_1);
extern uint64_t indirect_placeholder_5(uint64_t param_0);
extern uint64_t indirect_placeholder_15(uint64_t param_0, uint64_t param_1);
extern uint64_t init_r9(void);
extern uint64_t init_r8(void);
uint64_t bb_read_utmp(uint64_t rcx, uint64_t rdi, uint64_t rsi, uint64_t rdx) {
    uint64_t var_24;
    uint64_t var_0;
    uint64_t var_1;
    uint64_t var_2;
    uint64_t var_3;
    uint64_t var_4;
    uint64_t var_5;
    uint64_t var_6;
    uint64_t var_7;
    uint64_t var_8;
    uint64_t var_9;
    uint64_t var_10;
    uint64_t rax_4;
    uint64_t rbp_3;
    uint64_t var_27;
    uint32_t *var_28;
    uint32_t var_29;
    uint64_t local_sp_1;
    uint64_t local_sp_6;
    uint64_t var_25;
    uint64_t var_26;
    uint64_t r14_1;
    uint64_t r8_0;
    uint64_t rbx_1;
    uint64_t rax_2;
    uint64_t var_22;
    uint64_t var_23;
    uint64_t local_sp_4;
    uint64_t rbx_0;
    uint64_t var_11;
    uint64_t local_sp_5;
    uint64_t r8_2;
    uint64_t rbp_1;
    uint64_t r15_0;
    uint64_t var_14;
    uint64_t var_15;
    uint64_t var_16;
    uint64_t rbp_2;
    uint64_t r8_1;
    uint64_t var_17;
    uint64_t rsi3_0;
    uint64_t r14_0;
    uint64_t var_18;
    uint64_t var_19;
    uint64_t rax_3;
    uint64_t var_12;
    uint64_t var_13;
    uint64_t var_20;
    uint64_t var_21;
    unsigned int loop_state_var;
    var_0 = revng_init_local_sp(0UL);
    var_1 = init_rax();
    var_2 = init_rbx();
    var_3 = init_r9();
    var_4 = init_rbp();
    var_5 = init_r8();
    var_6 = init_r13();
    var_7 = init_r15();
    var_8 = init_r12();
    var_9 = init_r14();
    *(uint64_t *)(var_0 + (-8L)) = var_7;
    *(uint64_t *)(var_0 + (-16L)) = var_9;
    *(uint64_t *)(var_0 + (-24L)) = var_6;
    *(uint64_t *)(var_0 + (-32L)) = var_8;
    *(uint64_t *)(var_0 + (-40L)) = var_4;
    *(uint64_t *)(var_0 + (-48L)) = var_2;
    *(uint64_t *)(var_0 + (-72L)) = rsi;
    *(uint64_t *)(var_0 + (-64L)) = rdx;
    *(uint32_t *)(var_0 + (-76L)) = (uint32_t)rcx;
    var_10 = var_0 + (-96L);
    *(uint64_t *)var_10 = 4216265UL;
    indirect_placeholder();
    rax_4 = 4294967295UL;
    r8_0 = var_5;
    rbx_0 = 0UL;
    local_sp_5 = var_10;
    rbp_1 = 0UL;
    r15_0 = 0UL;
    rbp_2 = 1UL;
    rsi3_0 = 400UL;
    r14_0 = 0UL;
    if (var_1 == 0UL) {
        return rax_4;
    }
    var_11 = ((rcx >> 1UL) & 1UL) ^ 1UL;
    rax_2 = var_11;
    rax_4 = 0UL;
    while (1U)
        {
            r8_1 = r8_0;
            rbx_1 = rbx_0;
            local_sp_6 = local_sp_5;
            rbp_3 = rbp_1;
            if (r15_0 == rbp_1) {
                var_12 = r15_0 * 5UL;
                var_13 = r15_0 * 400UL;
                rax_3 = var_12;
                r8_2 = var_13;
                r14_1 = var_13;
            } else {
                if (rbx_0 == 0UL) {
                    if (rbp_1 != 0UL) {
                        rbp_2 = rbp_1;
                        if (rbp_1 <= 23058430092136939UL) {
                            loop_state_var = 2U;
                            break;
                        }
                        var_17 = rbp_1 * 400UL;
                        rsi3_0 = var_17;
                        r14_0 = var_17;
                    }
                } else {
                    if (rbp_1 <= 15372286728091292UL) {
                        loop_state_var = 2U;
                        break;
                    }
                    var_14 = ((rbp_1 >> 1UL) + rbp_1) + 1UL;
                    var_15 = var_14 * 400UL;
                    var_16 = rbp_1 * 400UL;
                    rbp_2 = var_14;
                    r8_1 = var_16;
                    rsi3_0 = var_15;
                    r14_0 = var_16;
                }
                var_18 = local_sp_5 + (-8L);
                *(uint64_t *)var_18 = 4216452UL;
                var_19 = indirect_placeholder_15(rbx_0, rsi3_0);
                rbp_3 = rbp_2;
                local_sp_6 = var_18;
                r14_1 = r14_0;
                rbx_1 = var_19;
                r8_2 = r8_1;
                rax_3 = var_19;
            }
            var_20 = r14_1 + rbx_1;
            var_21 = local_sp_6 + (-8L);
            *(uint64_t *)var_21 = 4216343UL;
            indirect_placeholder();
            local_sp_4 = var_21;
            rbx_0 = rbx_1;
            rbp_1 = rbp_3;
            r8_0 = r8_2;
            if (rax_3 == 0UL) {
                var_22 = helper_cc_compute_all_wrapper((uint64_t)*(uint32_t *)(var_20 + 4UL), 0UL, 0UL, 24U);
                rax_2 = 1UL;
                var_23 = local_sp_6 + (-16L);
                *(uint64_t *)var_23 = 4216482UL;
                indirect_placeholder();
                local_sp_4 = var_23;
                if (*(unsigned char *)(var_20 + 44UL) != '\x00' & *(uint16_t *)var_20 != (unsigned short)7U & (uint64_t)(((unsigned char)(var_22 >> 4UL) ^ (unsigned char)var_22) & '\xc0') != 0UL & (*(unsigned char *)(local_sp_6 + 4UL) & '\x01') != '\x00' & (int)(uint32_t)rax_3 < (int)0U) {
                    var_24 = local_sp_6 + (-24L);
                    *(uint64_t *)var_24 = 4216565UL;
                    indirect_placeholder();
                    rax_2 = (*(uint32_t *)rax_3 != 3U);
                    local_sp_4 = var_24;
                }
                local_sp_5 = local_sp_4;
                r15_0 = r15_0 + rax_2;
                continue;
            }
            *(uint64_t *)(local_sp_6 + (-16L)) = 4216592UL;
            indirect_placeholder();
            var_25 = local_sp_6 + (-24L);
            *(uint64_t *)var_25 = 4216604UL;
            var_26 = indirect_placeholder_5(var_1);
            local_sp_1 = var_25;
            if ((uint64_t)(uint32_t)var_26 != 0UL) {
                loop_state_var = 0U;
                break;
            }
            var_27 = local_sp_6 + (-32L);
            *(uint64_t *)var_27 = 4216665UL;
            indirect_placeholder();
            var_28 = (uint32_t *)var_26;
            var_29 = *var_28;
            local_sp_1 = var_27;
            rax_4 = 4294967295UL;
            if (var_29 != 0U) {
                loop_state_var = 0U;
                break;
            }
            *(uint64_t *)(local_sp_6 + (-40L)) = 4216679UL;
            indirect_placeholder();
            *(uint64_t *)(local_sp_6 + (-48L)) = 4216684UL;
            indirect_placeholder();
            *var_28 = var_29;
            loop_state_var = 1U;
            break;
        }
    switch (loop_state_var) {
      case 2U:
        {
            *(uint64_t *)(local_sp_5 + (-8L)) = 4216698UL;
            indirect_placeholder_1(var_3, r8_0);
            abort();
        }
        break;
      case 1U:
      case 0U:
        {
            **(uint64_t **)(local_sp_1 + 16UL) = r15_0;
            **(uint64_t **)(local_sp_1 + 24UL) = rbx_1;
        }
        break;
    }
}

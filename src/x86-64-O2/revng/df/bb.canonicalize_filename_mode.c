typedef unsigned char __u_char;
typedef unsigned short __u_short;
typedef unsigned int __u_int;
typedef unsigned long __u_long;
typedef signed char __int8_t;
typedef unsigned char __uint8_t;
typedef short __int16_t;
typedef unsigned short __uint16_t;
typedef int __int32_t;
typedef unsigned int __uint32_t;
typedef long __int64_t;
typedef unsigned long __uint64_t;
typedef __int8_t __int_least8_t;
typedef __uint8_t __uint_least8_t;
typedef __int16_t __int_least16_t;
typedef __uint16_t __uint_least16_t;
typedef __int32_t __int_least32_t;
typedef __uint32_t __uint_least32_t;
typedef __int64_t __int_least64_t;
typedef __uint64_t __uint_least64_t;
typedef long __quad_t;
typedef unsigned long __u_quad_t;
typedef long __intmax_t;
typedef unsigned long __uintmax_t;
typedef unsigned long __dev_t;
typedef unsigned int __uid_t;
typedef unsigned int __gid_t;
typedef unsigned long __ino_t;
typedef unsigned long __ino64_t;
typedef unsigned int __mode_t;
typedef unsigned long __nlink_t;
typedef long __off_t;
typedef long __off64_t;
typedef int __pid_t;
typedef struct {
    int __val[2];
} __fsid_t;
typedef long __clock_t;
typedef unsigned long __rlim_t;
typedef unsigned long __rlim64_t;
typedef unsigned int __id_t;
typedef long __time_t;
typedef unsigned int __useconds_t;
typedef long __suseconds_t;
typedef int __daddr_t;
typedef int __key_t;
typedef int __clockid_t;
typedef void *__timer_t;
typedef long __blksize_t;
typedef long __blkcnt_t;
typedef long __blkcnt64_t;
typedef unsigned long __fsblkcnt_t;
typedef unsigned long __fsblkcnt64_t;
typedef unsigned long __fsfilcnt_t;
typedef unsigned long __fsfilcnt64_t;
typedef long __fsword_t;
typedef long __ssize_t;
typedef long __syscall_slong_t;
typedef unsigned long __syscall_ulong_t;
typedef __off64_t __loff_t;
typedef char *__caddr_t;
typedef long __intptr_t;
typedef unsigned int __socklen_t;
typedef int __sig_atomic_t;
typedef __int8_t int8_t;
typedef __int16_t int16_t;
typedef __int32_t int32_t;
typedef __int64_t int64_t;
typedef __uint8_t uint8_t;
typedef __uint16_t uint16_t;
typedef __uint32_t uint32_t;
typedef __uint64_t uint64_t;
typedef __int_least8_t int_least8_t;
typedef __int_least16_t int_least16_t;
typedef __int_least32_t int_least32_t;
typedef __int_least64_t int_least64_t;
typedef __uint_least8_t uint_least8_t;
typedef __uint_least16_t uint_least16_t;
typedef __uint_least32_t uint_least32_t;
typedef __uint_least64_t uint_least64_t;
typedef signed char int_fast8_t;
typedef long int_fast16_t;
typedef long int_fast32_t;
typedef long int_fast64_t;
typedef unsigned char uint_fast8_t;
typedef unsigned long uint_fast16_t;
typedef unsigned long uint_fast32_t;
typedef unsigned long uint_fast64_t;
typedef long intptr_t;
typedef unsigned long uintptr_t;
typedef __intmax_t intmax_t;
typedef __uintmax_t uintmax_t;
typedef _Bool bool;
struct indirect_placeholder_64_ret_type;
struct indirect_placeholder_65_ret_type;
struct indirect_placeholder_67_ret_type;
struct indirect_placeholder_68_ret_type;
struct indirect_placeholder_69_ret_type;
struct indirect_placeholder_70_ret_type;
struct indirect_placeholder_71_ret_type;
struct indirect_placeholder_72_ret_type;
struct indirect_placeholder_64_ret_type {
    uint64_t field_0;
    uint64_t field_1;
    uint64_t field_2;
};
struct indirect_placeholder_65_ret_type {
    uint64_t field_0;
    uint64_t field_1;
};
struct indirect_placeholder_67_ret_type {
    uint64_t field_0;
    uint64_t field_1;
    uint64_t field_2;
    uint64_t field_3;
    uint64_t field_4;
};
struct indirect_placeholder_68_ret_type {
    uint64_t field_0;
    uint64_t field_1;
};
struct indirect_placeholder_69_ret_type {
    uint64_t field_0;
    uint64_t field_1;
    uint64_t field_2;
};
struct indirect_placeholder_70_ret_type {
    uint64_t field_0;
    uint64_t field_1;
    uint64_t field_2;
};
struct indirect_placeholder_71_ret_type {
    uint64_t field_0;
    uint64_t field_1;
    uint64_t field_2;
};
struct indirect_placeholder_72_ret_type {
    uint64_t field_0;
    uint64_t field_1;
};
extern void abort(void);
extern uint64_t revng_init_local_sp(uint64_t param_0);
extern uint64_t init_rbx(void);
extern uint64_t init_rbp(void);
extern void indirect_placeholder(void);
extern uint64_t init_cc_src2(void);
extern uint64_t helper_cc_compute_c_wrapper(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint32_t param_3);
extern uint64_t init_r13(void);
extern uint64_t init_r12(void);
extern uint64_t init_r14(void);
extern uint64_t init_r15(void);
extern uint64_t indirect_placeholder_3(void);
extern uint64_t indirect_placeholder_25(uint64_t param_0, uint64_t param_1);
extern uint64_t indirect_placeholder_24(uint64_t param_0, uint64_t param_1, uint64_t param_2);
extern void indirect_placeholder_39(uint64_t param_0, uint64_t param_1);
extern void indirect_placeholder_31(uint64_t param_0);
extern struct indirect_placeholder_64_ret_type indirect_placeholder_64(uint64_t param_0, uint64_t param_1);
extern struct indirect_placeholder_65_ret_type indirect_placeholder_65(uint64_t param_0);
extern uint64_t indirect_placeholder_66(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint64_t param_3, uint64_t param_4, uint64_t param_5, uint64_t param_6);
extern struct indirect_placeholder_67_ret_type indirect_placeholder_67(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint64_t param_3, uint64_t param_4);
extern struct indirect_placeholder_68_ret_type indirect_placeholder_68(uint64_t param_0, uint64_t param_1);
extern struct indirect_placeholder_69_ret_type indirect_placeholder_69(uint64_t param_0, uint64_t param_1);
extern struct indirect_placeholder_70_ret_type indirect_placeholder_70(uint64_t param_0, uint64_t param_1);
extern struct indirect_placeholder_71_ret_type indirect_placeholder_71(uint64_t param_0, uint64_t param_1);
extern struct indirect_placeholder_72_ret_type indirect_placeholder_72(uint64_t param_0);
uint64_t bb_canonicalize_filename_mode(uint64_t rdi, uint64_t rsi) {
    uint64_t var_44;
    uint32_t var_45;
    uint64_t var_46;
    uint64_t var_32;
    uint64_t r15_4;
    uint64_t r14_1;
    uint64_t var_33;
    uint64_t var_77;
    struct indirect_placeholder_67_ret_type var_63;
    uint64_t var_0;
    uint64_t var_1;
    uint64_t var_2;
    uint64_t var_3;
    uint64_t var_4;
    uint64_t var_5;
    uint64_t var_6;
    uint64_t var_7;
    uint64_t var_8;
    uint64_t var_9;
    uint64_t var_10;
    uint64_t var_11;
    uint64_t var_12;
    uint64_t *var_13;
    uint64_t var_109;
    uint64_t var_110;
    uint64_t rbx_1;
    uint64_t rbx_5;
    uint64_t var_36;
    uint64_t var_101;
    uint64_t var_102;
    uint64_t rbx_0;
    uint64_t local_sp_1;
    uint64_t var_103;
    uint64_t var_104;
    uint64_t var_105;
    uint64_t rbx_6;
    uint64_t var_87;
    uint64_t local_sp_0;
    uint64_t var_97;
    unsigned char var_98;
    bool var_99;
    uint64_t var_100;
    uint64_t *var_88;
    uint64_t var_89;
    uint64_t var_90;
    struct indirect_placeholder_64_ret_type var_91;
    uint64_t var_92;
    uint64_t var_93;
    uint64_t var_94;
    uint64_t var_95;
    struct indirect_placeholder_65_ret_type var_96;
    uint64_t var_82;
    uint64_t *var_83;
    uint64_t var_84;
    bool var_85;
    uint64_t var_86;
    uint64_t r12_5;
    uint64_t local_sp_5;
    uint64_t var_106;
    uint64_t var_107;
    uint64_t rax_0;
    uint64_t local_sp_2;
    uint32_t storemerge;
    uint64_t var_108;
    uint32_t r13_2_shrunk;
    uint64_t local_sp_15;
    uint64_t *var_72;
    uint64_t var_73;
    uint64_t var_74;
    uint64_t var_75;
    uint64_t *var_76;
    uint64_t var_78;
    uint64_t var_79;
    uint64_t *var_80;
    uint64_t var_81;
    uint64_t rax_6;
    uint64_t rax_1;
    uint64_t local_sp_10;
    uint32_t r13_0;
    uint64_t local_sp_3;
    uint64_t var_118;
    uint64_t var_119;
    uint64_t rax_2;
    uint32_t r13_1;
    uint64_t local_sp_4;
    uint64_t rbp_4;
    uint64_t var_115;
    uint64_t var_29;
    unsigned char var_30;
    uint32_t var_59;
    uint64_t var_60;
    uint64_t rax_5;
    uint64_t var_61;
    uint64_t var_65;
    uint64_t var_68;
    uint64_t local_sp_6;
    uint64_t *var_66;
    uint64_t var_67;
    uint64_t var_69;
    uint64_t *var_70;
    uint64_t var_71;
    uint64_t var_62;
    uint64_t var_64;
    uint64_t var_111;
    uint64_t rax_3;
    uint64_t local_sp_7;
    uint32_t var_112;
    uint32_t var_113;
    uint64_t var_114;
    uint64_t r12_7;
    uint64_t var_125;
    uint64_t rax_4;
    uint64_t local_sp_8;
    uint64_t local_sp_9;
    uint64_t var_117;
    uint64_t var_48;
    uint64_t var_49;
    struct indirect_placeholder_68_ret_type var_50;
    uint64_t var_51;
    bool var_52;
    uint64_t var_53;
    uint64_t var_54;
    uint64_t var_55;
    uint64_t var_56;
    bool var_57;
    uint64_t var_58;
    uint64_t rbx_2;
    uint64_t rbp_0;
    uint64_t local_sp_11;
    uint64_t r12_1;
    uint64_t var_27;
    uint64_t rbx_3_be;
    uint64_t rbx_3;
    uint64_t r12_4;
    uint64_t rbp_1;
    unsigned char rsi2_0_in;
    uint64_t local_sp_12;
    uint64_t rdi1_0;
    uint64_t r12_2;
    uint64_t r15_0;
    unsigned char var_28;
    uint64_t rbx_7;
    uint64_t rbp_3;
    uint64_t rbx_4;
    uint64_t local_sp_14;
    uint64_t rbp_2;
    uint64_t local_sp_13;
    uint64_t r14_2;
    uint64_t r12_3;
    uint64_t r15_1;
    uint64_t rsi2_0;
    uint64_t r15_2;
    uint64_t rsi2_1;
    uint64_t var_31;
    uint64_t rsi2_2;
    uint64_t r15_3;
    uint64_t r14_0;
    uint64_t var_34;
    uint64_t var_35;
    unsigned char var_116;
    uint64_t var_37;
    uint64_t var_38;
    uint64_t rax_7;
    uint64_t var_39;
    uint64_t var_40;
    uint64_t var_41;
    struct indirect_placeholder_69_ret_type var_42;
    uint64_t var_43;
    bool var_47;
    uint64_t rbp_5;
    uint64_t local_sp_16;
    uint64_t r12_6;
    uint64_t rbx_8;
    uint64_t local_sp_17;
    uint64_t var_120;
    uint64_t var_121;
    struct indirect_placeholder_70_ret_type var_122;
    uint64_t *var_123;
    uint64_t var_124;
    uint64_t var_20;
    struct indirect_placeholder_71_ret_type var_21;
    uint64_t var_22;
    uint64_t rax_8;
    unsigned char var_14;
    uint64_t var_15;
    bool var_16;
    uint64_t var_17;
    uint64_t *var_18;
    uint64_t var_19;
    struct indirect_placeholder_72_ret_type var_23;
    uint64_t var_24;
    uint64_t var_25;
    uint64_t var_26;
    unsigned int loop_state_var;
    bool switch_state_var;
    var_0 = revng_init_local_sp(0UL);
    var_1 = init_rbx();
    var_2 = init_rbp();
    var_3 = init_r13();
    var_4 = init_cc_src2();
    var_5 = init_r12();
    var_6 = init_r15();
    var_7 = init_r14();
    *(uint64_t *)(var_0 + (-8L)) = var_6;
    *(uint64_t *)(var_0 + (-16L)) = var_7;
    *(uint64_t *)(var_0 + (-24L)) = var_3;
    var_8 = rsi >> 2UL;
    *(uint64_t *)(var_0 + (-32L)) = var_5;
    *(uint64_t *)(var_0 + (-40L)) = var_2;
    *(uint64_t *)(var_0 + (-48L)) = var_1;
    var_9 = rsi & 3UL;
    *(unsigned char *)(var_0 + (-241L)) = ((unsigned char)var_8 & '\x01');
    var_10 = var_9 + 4294967295UL;
    var_11 = (uint64_t)(uint32_t)var_10;
    var_12 = var_0 + (-240L);
    var_13 = (uint64_t *)var_12;
    *var_13 = rdi;
    *(uint32_t *)(var_0 + (-256L)) = (uint32_t)var_9;
    rax_0 = 0UL;
    storemerge = 40U;
    r13_2_shrunk = 20U;
    rax_8 = 0UL;
    if ((var_10 & var_9) == 0UL) {
        *(uint64_t *)(var_0 + (-272L)) = 4215431UL;
        indirect_placeholder();
        *(uint32_t *)var_11 = 22U;
        return rax_8;
    }
    if (*var_13 == 0UL) {
        return;
    }
    var_14 = **(unsigned char **)var_12;
    var_15 = (uint64_t)var_14;
    if (var_14 == '\x00') {
        *(uint64_t *)(var_0 + (-272L)) = 4215449UL;
        indirect_placeholder();
        *(uint32_t *)var_15 = 2U;
    } else {
        var_16 = ((uint64_t)(var_14 + '\xd1') == 0UL);
        var_17 = var_0 + (-272L);
        var_18 = (uint64_t *)var_17;
        local_sp_11 = var_17;
        if (var_16) {
            *var_18 = 4215154UL;
            var_23 = indirect_placeholder_72(4096UL);
            var_24 = var_23.field_0;
            var_25 = var_24 + 4096UL;
            var_26 = var_24 + 1UL;
            *(unsigned char *)var_24 = (unsigned char)'/';
            rbx_2 = var_26;
            rbp_0 = var_25;
            r12_1 = var_24;
        } else {
            *var_18 = 4214050UL;
            var_19 = indirect_placeholder_3();
            if (var_19 != 0UL) {
                return rax_8;
            }
            *(uint64_t *)(var_0 + (-280L)) = 4214072UL;
            indirect_placeholder();
            var_20 = var_0 + (-288L);
            *(uint64_t *)var_20 = 4215245UL;
            var_21 = indirect_placeholder_71(var_19, 4096UL);
            var_22 = var_21.field_0;
            rbx_2 = var_22;
            rbp_0 = var_22 + 4096UL;
            local_sp_11 = var_20;
            r12_1 = var_22;
        }
        var_27 = *(uint64_t *)(local_sp_11 + 24UL);
        *(uint64_t *)(local_sp_11 + 32UL) = 0UL;
        *(uint64_t *)(local_sp_11 + 48UL) = 0UL;
        *(uint64_t *)(local_sp_11 + 40UL) = 0UL;
        rbx_3 = rbx_2;
        rbp_1 = rbp_0;
        local_sp_12 = local_sp_11;
        r12_2 = r12_1;
        r15_0 = var_27;
        while (1U)
            {
                var_28 = *(unsigned char *)r15_0;
                rsi2_0_in = var_28;
                rbx_7 = rbx_3;
                rbx_4 = rbx_3;
                rbp_2 = rbp_1;
                local_sp_13 = local_sp_12;
                r12_3 = r12_2;
                r15_1 = r15_0;
                rbp_5 = rbp_1;
                local_sp_16 = local_sp_12;
                r12_6 = r12_2;
                if (var_28 != '\x00') {
                    loop_state_var = 3U;
                    break;
                }
                while (1U)
                    {
                        rsi2_0 = (uint64_t)rsi2_0_in;
                        rbx_6 = r12_3;
                        r12_5 = r12_3;
                        local_sp_15 = local_sp_13;
                        rbp_4 = rbp_2;
                        rbx_3_be = rbx_4;
                        r12_4 = r12_3;
                        rbp_1 = rbp_2;
                        local_sp_12 = local_sp_13;
                        rdi1_0 = rbx_4;
                        r12_2 = r12_3;
                        rbx_7 = rbx_4;
                        rbp_3 = rbp_2;
                        local_sp_14 = local_sp_13;
                        r15_2 = r15_1;
                        rsi2_1 = rsi2_0;
                        r15_3 = r15_1;
                        rbp_5 = rbp_2;
                        local_sp_16 = local_sp_13;
                        r12_6 = r12_3;
                        if ((uint64_t)(rsi2_0_in + '\xd1') != 0UL) {
                            r14_0 = r15_3;
                            rsi2_2 = rsi2_1;
                            r15_4 = r15_3;
                            while (1U)
                                {
                                    switch_state_var = 1;
                                    break;
                                }
                        }
                        var_29 = r15_2 + 1UL;
                        var_30 = *(unsigned char *)var_29;
                        r15_2 = var_29;
                        r15_3 = var_29;
                        r15_4 = var_29;
                        r14_1 = var_29;
                        do {
                            var_29 = r15_2 + 1UL;
                            var_30 = *(unsigned char *)var_29;
                            r15_2 = var_29;
                            r15_3 = var_29;
                            r15_4 = var_29;
                            r14_1 = var_29;
                        } while (var_30 != '/');
                        var_31 = (uint64_t)var_30;
                        rsi2_1 = var_31;
                        rsi2_2 = var_31;
                        if (var_30 != '\x00') {
                            r14_0 = r15_3;
                            rsi2_2 = rsi2_1;
                            r15_4 = r15_3;
                            while (1U)
                                {
                                    switch_state_var = 1;
                                    break;
                                }
                        }
                        r15_0 = r14_1;
                        r14_2 = r14_1;
                        if (r14_1 != r15_4) {
                            loop_state_var = 3U;
                            break;
                        }
                        var_33 = r14_1 - r15_4;
                        switch_state_var = 0;
                        switch (var_33) {
                          case 1UL:
                            {
                                rbx_6 = rbx_4;
                                if ((uint64_t)((unsigned char)rsi2_2 + '\xd2') != 0UL) {
                                    var_116 = *(unsigned char *)r14_2;
                                    rsi2_0_in = var_116;
                                    rbx_7 = rbx_6;
                                    rbx_4 = rbx_6;
                                    rbp_2 = rbp_3;
                                    local_sp_13 = local_sp_14;
                                    r12_3 = r12_4;
                                    r15_1 = r14_2;
                                    rbp_5 = rbp_3;
                                    local_sp_16 = local_sp_14;
                                    r12_6 = r12_4;
                                    if (var_116 == '\x00') {
                                        continue;
                                    }
                                    loop_state_var = 3U;
                                    switch_state_var = 1;
                                    break;
                                }
                            }
                            break;
                          case 2UL:
                            {
                                if ((uint64_t)((unsigned char)rsi2_2 + '\xd2') != 0UL & *(unsigned char *)(r15_4 + 1UL) != '.') {
                                    if (rbx_4 <= (r12_3 + 1UL)) {
                                        loop_state_var = 4U;
                                        switch_state_var = 1;
                                        break;
                                    }
                                    var_34 = rbx_4 + (-1L);
                                    var_35 = helper_cc_compute_c_wrapper(r12_3 - var_34, var_34, var_4, 17U);
                                    rbx_5 = var_34;
                                    rbx_3_be = var_34;
                                    if (var_35 != 0UL) {
                                        loop_state_var = 4U;
                                        switch_state_var = 1;
                                        break;
                                    }
                                    if (*(unsigned char *)(rbx_4 + (-2L)) != '/') {
                                        loop_state_var = 4U;
                                        switch_state_var = 1;
                                        break;
                                    }
                                    var_36 = rbx_5 + (-1L);
                                    rbx_5 = var_36;
                                    while (var_36 != r12_3)
                                        {
                                            rbx_6 = var_36;
                                            if (*(unsigned char *)(rbx_5 + (-2L)) == '/') {
                                                break;
                                            }
                                            var_36 = rbx_5 + (-1L);
                                            rbx_5 = var_36;
                                        }
                                    var_116 = *(unsigned char *)r14_2;
                                    rsi2_0_in = var_116;
                                    rbx_7 = rbx_6;
                                    rbx_4 = rbx_6;
                                    rbp_2 = rbp_3;
                                    local_sp_13 = local_sp_14;
                                    r12_3 = r12_4;
                                    r15_1 = r14_2;
                                    rbp_5 = rbp_3;
                                    local_sp_16 = local_sp_14;
                                    r12_6 = r12_4;
                                    if (var_116 == '\x00') {
                                        continue;
                                    }
                                    loop_state_var = 3U;
                                    switch_state_var = 1;
                                    break;
                                }
                            }
                            break;
                          default:
                            {
                                if (*(unsigned char *)(rbx_4 + (-1L)) == '/') {
                                    var_37 = rbx_4 + 1UL;
                                    *(unsigned char *)rbx_4 = (unsigned char)'/';
                                    rdi1_0 = var_37;
                                }
                                var_38 = var_33 + rdi1_0;
                                rax_7 = var_38;
                                if (rbp_2 > var_38) {
                                    var_39 = rbp_2 - r12_3;
                                    var_40 = ((long)var_33 < (long)4096UL) ? (var_39 + 4096UL) : ((var_39 + var_33) + 1UL);
                                    var_41 = local_sp_13 + (-8L);
                                    *(uint64_t *)var_41 = 4214314UL;
                                    var_42 = indirect_placeholder_69(r12_3, var_40);
                                    var_43 = var_42.field_0;
                                    rax_7 = var_43;
                                    rbp_4 = var_40 + var_43;
                                    local_sp_15 = var_41;
                                    r12_5 = var_43;
                                }
                                var_44 = local_sp_15 + (-8L);
                                *(uint64_t *)var_44 = 4214335UL;
                                indirect_placeholder();
                                var_45 = *(uint32_t *)local_sp_15;
                                var_46 = var_33 + rax_7;
                                *(unsigned char *)var_46 = (unsigned char)'\x00';
                                var_47 = (var_45 == 2U);
                                rbx_6 = var_46;
                                rax_4 = rax_7;
                                local_sp_8 = var_44;
                                r12_4 = r12_5;
                                rbp_3 = rbp_4;
                                if (*(unsigned char *)(local_sp_15 + 15UL) == '\x00') {
                                    var_54 = local_sp_15 + 56UL;
                                    var_55 = local_sp_15 + (-16L);
                                    *(uint64_t *)var_55 = 4214653UL;
                                    var_56 = indirect_placeholder_25(r12_5, var_54);
                                    var_57 = ((uint64_t)(uint32_t)var_56 != 0UL);
                                    var_58 = (var_56 & (-256L)) | var_57;
                                    local_sp_5 = var_55;
                                    rax_3 = var_58;
                                    local_sp_7 = var_55;
                                    if (!var_57) {
                                        var_111 = local_sp_7 + (-8L);
                                        *(uint64_t *)var_111 = 4214731UL;
                                        indirect_placeholder();
                                        var_112 = *(uint32_t *)rax_3;
                                        var_113 = *(uint32_t *)local_sp_7;
                                        var_114 = (uint64_t)var_113;
                                        r13_2_shrunk = var_112;
                                        rax_6 = var_114;
                                        local_sp_10 = var_111;
                                        rax_4 = var_114;
                                        local_sp_8 = var_111;
                                        if (var_113 == 0U) {
                                            loop_state_var = 2U;
                                            switch_state_var = 1;
                                            break;
                                        }
                                        if ((uint64_t)(var_113 + (-1)) != 0UL) {
                                            var_115 = local_sp_7 + (-16L);
                                            *(uint64_t *)var_115 = 4214764UL;
                                            indirect_placeholder();
                                            local_sp_10 = var_115;
                                            local_sp_14 = var_115;
                                            if ((*(unsigned char *)(r14_1 + var_114) == '\x00') && (var_112 == 2U)) {
                                                loop_state_var = 2U;
                                                switch_state_var = 1;
                                                break;
                                            }
                                            var_116 = *(unsigned char *)r14_2;
                                            rsi2_0_in = var_116;
                                            rbx_7 = rbx_6;
                                            rbx_4 = rbx_6;
                                            rbp_2 = rbp_3;
                                            local_sp_13 = local_sp_14;
                                            r12_3 = r12_4;
                                            r15_1 = r14_2;
                                            rbp_5 = rbp_3;
                                            local_sp_16 = local_sp_14;
                                            r12_6 = r12_4;
                                            if (var_116 == '\x00') {
                                                continue;
                                            }
                                            loop_state_var = 3U;
                                            switch_state_var = 1;
                                            break;
                                        }
                                    }
                                    var_59 = (uint32_t)((uint16_t)*(uint32_t *)(local_sp_5 + 88UL) & (unsigned short)61440U);
                                    var_60 = (uint64_t)var_59;
                                    local_sp_6 = local_sp_5;
                                    rax_5 = var_60;
                                    local_sp_9 = local_sp_5;
                                    local_sp_14 = local_sp_5;
                                    if ((uint64_t)((var_59 + (-40960)) & (-4096)) == 0UL) {
                                        if ((uint64_t)((var_59 + (-16384)) & (-4096)) != 0UL) {
                                            var_116 = *(unsigned char *)r14_2;
                                            rsi2_0_in = var_116;
                                            rbx_7 = rbx_6;
                                            rbx_4 = rbx_6;
                                            rbp_2 = rbp_3;
                                            local_sp_13 = local_sp_14;
                                            r12_3 = r12_4;
                                            r15_1 = r14_2;
                                            rbp_5 = rbp_3;
                                            local_sp_16 = local_sp_14;
                                            r12_6 = r12_4;
                                            if (var_116 == '\x00') {
                                                continue;
                                            }
                                            loop_state_var = 3U;
                                            switch_state_var = 1;
                                            break;
                                        }
                                        rax_6 = rax_5;
                                        local_sp_10 = local_sp_9;
                                        local_sp_14 = local_sp_9;
                                        if (*(unsigned char *)r14_1 != '\x00') {
                                            if (*(uint32_t *)(local_sp_9 + 8UL) != 2U) {
                                                loop_state_var = 2U;
                                                switch_state_var = 1;
                                                break;
                                            }
                                        }
                                        var_116 = *(unsigned char *)r14_2;
                                        rsi2_0_in = var_116;
                                        rbx_7 = rbx_6;
                                        rbx_4 = rbx_6;
                                        rbp_2 = rbp_3;
                                        local_sp_13 = local_sp_14;
                                        r12_3 = r12_4;
                                        r15_1 = r14_2;
                                        rbp_5 = rbp_3;
                                        local_sp_16 = local_sp_14;
                                        r12_6 = r12_4;
                                        if (var_116 == '\x00') {
                                            continue;
                                        }
                                        loop_state_var = 3U;
                                        switch_state_var = 1;
                                        break;
                                    }
                                    var_61 = *(uint64_t *)(local_sp_5 + 32UL);
                                    var_65 = var_61;
                                    if (var_61 != 0UL) {
                                        var_62 = local_sp_5 + (-8L);
                                        *(uint64_t *)var_62 = 4215204UL;
                                        var_63 = indirect_placeholder_67(4219136UL, 7UL, 4219200UL, 4219088UL, 0UL);
                                        var_64 = var_63.field_0;
                                        *(uint64_t *)(local_sp_5 + 24UL) = var_64;
                                        var_65 = var_64;
                                        local_sp_6 = var_62;
                                        if (var_64 != 0UL) {
                                            var_109 = var_63.field_2;
                                            var_110 = var_63.field_3;
                                            *(uint64_t *)(local_sp_5 + (-16L)) = 4215223UL;
                                            indirect_placeholder_39(var_109, var_110);
                                            abort();
                                        }
                                    }
                                    var_66 = (uint64_t *)(local_sp_6 + 24UL);
                                    var_67 = *var_66;
                                    var_68 = local_sp_6 + 64UL;
                                    var_69 = local_sp_6 + (-8L);
                                    var_70 = (uint64_t *)var_69;
                                    *var_70 = 4214848UL;
                                    var_71 = indirect_placeholder_24(var_65, var_68, var_67);
                                    local_sp_2 = var_69;
                                    local_sp_14 = var_69;
                                    if ((uint64_t)(unsigned char)var_71 != 0UL) {
                                        rax_0 = var_71;
                                        if (!var_47) {
                                            loop_state_var = 0U;
                                            switch_state_var = 1;
                                            break;
                                        }
                                        var_116 = *(unsigned char *)r14_2;
                                        rsi2_0_in = var_116;
                                        rbx_7 = rbx_6;
                                        rbx_4 = rbx_6;
                                        rbp_2 = rbp_3;
                                        local_sp_13 = local_sp_14;
                                        r12_3 = r12_4;
                                        r15_1 = r14_2;
                                        rbp_5 = rbp_3;
                                        local_sp_16 = local_sp_14;
                                        r12_6 = r12_4;
                                        if (var_116 == '\x00') {
                                            continue;
                                        }
                                        loop_state_var = 3U;
                                        switch_state_var = 1;
                                        break;
                                    }
                                    var_72 = (uint64_t *)(local_sp_6 + 16UL);
                                    var_73 = *var_72;
                                    var_74 = *var_66;
                                    var_75 = local_sp_6 + 56UL;
                                    var_76 = (uint64_t *)(local_sp_6 + (-16L));
                                    *var_76 = 4214951UL;
                                    var_77 = indirect_placeholder_66(var_46, var_74, rbp_4, var_33, var_75, var_73, r12_5);
                                    var_78 = *(uint64_t *)(local_sp_6 + 96UL);
                                    var_79 = local_sp_6 + (-24L);
                                    var_80 = (uint64_t *)var_79;
                                    *var_80 = 4214964UL;
                                    var_81 = indirect_placeholder_25(var_77, var_78);
                                    local_sp_1 = var_79;
                                    r12_4 = var_77;
                                    if (var_81 == 0UL) {
                                        if (!var_47) {
                                            loop_state_var = 1U;
                                            switch_state_var = 1;
                                            break;
                                        }
                                        var_106 = local_sp_6 + (-32L);
                                        *(uint64_t *)var_106 = 4215274UL;
                                        indirect_placeholder();
                                        local_sp_1 = var_106;
                                        local_sp_14 = var_106;
                                        if (*(volatile uint32_t *)(uint32_t *)0UL != 12U) {
                                            loop_state_var = 1U;
                                            switch_state_var = 1;
                                            break;
                                        }
                                    }
                                    *(uint64_t *)(local_sp_6 + (-32L)) = 4214984UL;
                                    indirect_placeholder();
                                    var_82 = local_sp_6 + (-40L);
                                    *(uint64_t *)var_82 = 4214995UL;
                                    indirect_placeholder();
                                    var_83 = (uint64_t *)(local_sp_6 + 8UL);
                                    var_84 = *var_83;
                                    var_85 = (var_84 == 0UL);
                                    var_86 = (var_81 << 1UL) | 1UL;
                                    local_sp_0 = var_82;
                                    if (var_85) {
                                        *var_76 = var_81;
                                        var_93 = helper_cc_compute_c_wrapper(var_86 + (-4096L), 4096UL, var_4, 17U);
                                        var_94 = (var_93 == 0UL) ? var_86 : 4096UL;
                                        *var_83 = var_94;
                                        var_95 = local_sp_6 + (-48L);
                                        *(uint64_t *)var_95 = 4215411UL;
                                        var_96 = indirect_placeholder_65(var_94);
                                        *var_70 = var_96.field_0;
                                        local_sp_0 = var_95;
                                    } else {
                                        var_87 = helper_cc_compute_c_wrapper(var_84 - var_86, var_86, var_4, 17U);
                                        if (var_87 == 0UL) {
                                            var_88 = (uint64_t *)local_sp_6;
                                            var_89 = *var_88;
                                            *var_76 = var_86;
                                            *var_72 = var_81;
                                            var_90 = local_sp_6 + (-48L);
                                            *(uint64_t *)var_90 = 4215319UL;
                                            var_91 = indirect_placeholder_64(var_89, var_86);
                                            var_92 = *var_80;
                                            *var_70 = var_91.field_0;
                                            *var_88 = var_92;
                                            local_sp_0 = var_90;
                                        }
                                    }
                                    var_97 = *(uint64_t *)(local_sp_0 + 40UL);
                                    *(uint64_t *)(local_sp_0 + (-8L)) = 4215047UL;
                                    indirect_placeholder();
                                    *(uint64_t *)(local_sp_0 + (-16L)) = 4215063UL;
                                    indirect_placeholder();
                                    var_98 = *(unsigned char *)var_81;
                                    *(uint64_t *)(local_sp_0 + 8UL) = var_97;
                                    var_99 = (var_98 == '/');
                                    var_100 = var_77 + 1UL;
                                    rbx_1 = var_100;
                                    if (!var_99) {
                                        rbx_1 = var_46;
                                        var_101 = var_46 + (-1L);
                                        var_102 = helper_cc_compute_c_wrapper(var_77 - var_101, var_101, var_4, 17U);
                                        rbx_0 = var_101;
                                        rbx_1 = var_101;
                                        if (var_46 <= var_100 & var_102 != 0UL & *(unsigned char *)(var_46 + (-2L)) == '/') {
                                            var_103 = rbx_0 + (-1L);
                                            rbx_0 = var_103;
                                            rbx_1 = var_77;
                                            while (var_103 != var_77)
                                                {
                                                    rbx_1 = var_103;
                                                    if (*(unsigned char *)(rbx_0 + (-2L)) == '/') {
                                                        break;
                                                    }
                                                    var_103 = rbx_0 + (-1L);
                                                    rbx_0 = var_103;
                                                    rbx_1 = var_77;
                                                }
                                        }
                                    }
                                    *(unsigned char *)var_77 = (unsigned char)'/';
                                    var_104 = local_sp_0 + (-24L);
                                    *(uint64_t *)var_104 = 4215134UL;
                                    indirect_placeholder();
                                    var_105 = *(uint64_t *)local_sp_0;
                                    rbx_6 = rbx_1;
                                    local_sp_14 = var_104;
                                    r14_2 = var_105;
                                } else {
                                    if (var_47) {
                                        *(uint32_t *)(local_sp_8 + 88UL) = 0U;
                                        rax_5 = rax_4;
                                        local_sp_9 = local_sp_8;
                                        rax_6 = rax_5;
                                        local_sp_10 = local_sp_9;
                                        local_sp_14 = local_sp_9;
                                        if (*(unsigned char *)r14_1 != '\x00') {
                                            if (*(uint32_t *)(local_sp_9 + 8UL) != 2U) {
                                                loop_state_var = 2U;
                                                switch_state_var = 1;
                                                break;
                                            }
                                        }
                                        var_116 = *(unsigned char *)r14_2;
                                        rsi2_0_in = var_116;
                                        rbx_7 = rbx_6;
                                        rbx_4 = rbx_6;
                                        rbp_2 = rbp_3;
                                        local_sp_13 = local_sp_14;
                                        r12_3 = r12_4;
                                        r15_1 = r14_2;
                                        rbp_5 = rbp_3;
                                        local_sp_16 = local_sp_14;
                                        r12_6 = r12_4;
                                        if (var_116 == '\x00') {
                                            continue;
                                        }
                                        loop_state_var = 3U;
                                        switch_state_var = 1;
                                        break;
                                    }
                                    var_48 = local_sp_15 + 56UL;
                                    var_49 = local_sp_15 + (-16L);
                                    *(uint64_t *)var_49 = 4214717UL;
                                    var_50 = indirect_placeholder_68(r12_5, var_48);
                                    var_51 = var_50.field_0;
                                    var_52 = ((uint64_t)(uint32_t)var_51 != 0UL);
                                    var_53 = (var_51 & (-256L)) | var_52;
                                    local_sp_5 = var_49;
                                    rax_3 = var_53;
                                    local_sp_7 = var_49;
                                    if (var_52) {
                                        var_111 = local_sp_7 + (-8L);
                                        *(uint64_t *)var_111 = 4214731UL;
                                        indirect_placeholder();
                                        var_112 = *(uint32_t *)rax_3;
                                        var_113 = *(uint32_t *)local_sp_7;
                                        var_114 = (uint64_t)var_113;
                                        r13_2_shrunk = var_112;
                                        rax_6 = var_114;
                                        local_sp_10 = var_111;
                                        rax_4 = var_114;
                                        local_sp_8 = var_111;
                                        if (var_113 != 0U) {
                                            loop_state_var = 2U;
                                            switch_state_var = 1;
                                            break;
                                        }
                                        if ((uint64_t)(var_113 + (-1)) == 0UL) {
                                            var_115 = local_sp_7 + (-16L);
                                            *(uint64_t *)var_115 = 4214764UL;
                                            indirect_placeholder();
                                            local_sp_10 = var_115;
                                            local_sp_14 = var_115;
                                            if (!((*(unsigned char *)(r14_1 + var_114) == '\x00') && (var_112 == 2U))) {
                                                loop_state_var = 2U;
                                                switch_state_var = 1;
                                                break;
                                            }
                                            var_116 = *(unsigned char *)r14_2;
                                            rsi2_0_in = var_116;
                                            rbx_7 = rbx_6;
                                            rbx_4 = rbx_6;
                                            rbp_2 = rbp_3;
                                            local_sp_13 = local_sp_14;
                                            r12_3 = r12_4;
                                            r15_1 = r14_2;
                                            rbp_5 = rbp_3;
                                            local_sp_16 = local_sp_14;
                                            r12_6 = r12_4;
                                            if (var_116 == '\x00') {
                                                continue;
                                            }
                                            loop_state_var = 3U;
                                            switch_state_var = 1;
                                            break;
                                        }
                                    }
                                    var_59 = (uint32_t)((uint16_t)*(uint32_t *)(local_sp_5 + 88UL) & (unsigned short)61440U);
                                    var_60 = (uint64_t)var_59;
                                    local_sp_6 = local_sp_5;
                                    rax_5 = var_60;
                                    local_sp_9 = local_sp_5;
                                    local_sp_14 = local_sp_5;
                                    if ((uint64_t)((var_59 + (-40960)) & (-4096)) == 0UL) {
                                        if ((uint64_t)((var_59 + (-16384)) & (-4096)) != 0UL) {
                                            var_116 = *(unsigned char *)r14_2;
                                            rsi2_0_in = var_116;
                                            rbx_7 = rbx_6;
                                            rbx_4 = rbx_6;
                                            rbp_2 = rbp_3;
                                            local_sp_13 = local_sp_14;
                                            r12_3 = r12_4;
                                            r15_1 = r14_2;
                                            rbp_5 = rbp_3;
                                            local_sp_16 = local_sp_14;
                                            r12_6 = r12_4;
                                            if (var_116 == '\x00') {
                                                continue;
                                            }
                                            loop_state_var = 3U;
                                            switch_state_var = 1;
                                            break;
                                        }
                                        rax_6 = rax_5;
                                        local_sp_10 = local_sp_9;
                                        local_sp_14 = local_sp_9;
                                        if (*(unsigned char *)r14_1 != '\x00') {
                                            if (*(uint32_t *)(local_sp_9 + 8UL) != 2U) {
                                                loop_state_var = 2U;
                                                switch_state_var = 1;
                                                break;
                                            }
                                        }
                                        var_116 = *(unsigned char *)r14_2;
                                        rsi2_0_in = var_116;
                                        rbx_7 = rbx_6;
                                        rbx_4 = rbx_6;
                                        rbp_2 = rbp_3;
                                        local_sp_13 = local_sp_14;
                                        r12_3 = r12_4;
                                        r15_1 = r14_2;
                                        rbp_5 = rbp_3;
                                        local_sp_16 = local_sp_14;
                                        r12_6 = r12_4;
                                        if (var_116 == '\x00') {
                                            continue;
                                        }
                                        loop_state_var = 3U;
                                        switch_state_var = 1;
                                        break;
                                    }
                                    var_61 = *(uint64_t *)(local_sp_5 + 32UL);
                                    var_65 = var_61;
                                    if (var_61 != 0UL) {
                                        var_62 = local_sp_5 + (-8L);
                                        *(uint64_t *)var_62 = 4215204UL;
                                        var_63 = indirect_placeholder_67(4219136UL, 7UL, 4219200UL, 4219088UL, 0UL);
                                        var_64 = var_63.field_0;
                                        *(uint64_t *)(local_sp_5 + 24UL) = var_64;
                                        var_65 = var_64;
                                        local_sp_6 = var_62;
                                        if (var_64 != 0UL) {
                                            var_109 = var_63.field_2;
                                            var_110 = var_63.field_3;
                                            *(uint64_t *)(local_sp_5 + (-16L)) = 4215223UL;
                                            indirect_placeholder_39(var_109, var_110);
                                            abort();
                                        }
                                    }
                                    var_66 = (uint64_t *)(local_sp_6 + 24UL);
                                    var_67 = *var_66;
                                    var_68 = local_sp_6 + 64UL;
                                    var_69 = local_sp_6 + (-8L);
                                    var_70 = (uint64_t *)var_69;
                                    *var_70 = 4214848UL;
                                    var_71 = indirect_placeholder_24(var_65, var_68, var_67);
                                    local_sp_2 = var_69;
                                    local_sp_14 = var_69;
                                    if ((uint64_t)(unsigned char)var_71 != 0UL) {
                                        rax_0 = var_71;
                                        if (!var_47) {
                                            loop_state_var = 0U;
                                            switch_state_var = 1;
                                            break;
                                        }
                                        var_116 = *(unsigned char *)r14_2;
                                        rsi2_0_in = var_116;
                                        rbx_7 = rbx_6;
                                        rbx_4 = rbx_6;
                                        rbp_2 = rbp_3;
                                        local_sp_13 = local_sp_14;
                                        r12_3 = r12_4;
                                        r15_1 = r14_2;
                                        rbp_5 = rbp_3;
                                        local_sp_16 = local_sp_14;
                                        r12_6 = r12_4;
                                        if (var_116 == '\x00') {
                                            continue;
                                        }
                                        loop_state_var = 3U;
                                        switch_state_var = 1;
                                        break;
                                    }
                                    var_72 = (uint64_t *)(local_sp_6 + 16UL);
                                    var_73 = *var_72;
                                    var_74 = *var_66;
                                    var_75 = local_sp_6 + 56UL;
                                    var_76 = (uint64_t *)(local_sp_6 + (-16L));
                                    *var_76 = 4214951UL;
                                    var_77 = indirect_placeholder_66(var_46, var_74, rbp_4, var_33, var_75, var_73, r12_5);
                                    var_78 = *(uint64_t *)(local_sp_6 + 96UL);
                                    var_79 = local_sp_6 + (-24L);
                                    var_80 = (uint64_t *)var_79;
                                    *var_80 = 4214964UL;
                                    var_81 = indirect_placeholder_25(var_77, var_78);
                                    local_sp_1 = var_79;
                                    r12_4 = var_77;
                                    if (var_81 == 0UL) {
                                        if (!var_47) {
                                            loop_state_var = 1U;
                                            switch_state_var = 1;
                                            break;
                                        }
                                        var_106 = local_sp_6 + (-32L);
                                        *(uint64_t *)var_106 = 4215274UL;
                                        indirect_placeholder();
                                        local_sp_1 = var_106;
                                        local_sp_14 = var_106;
                                        if (*(volatile uint32_t *)(uint32_t *)0UL != 12U) {
                                            loop_state_var = 1U;
                                            switch_state_var = 1;
                                            break;
                                        }
                                    }
                                    *(uint64_t *)(local_sp_6 + (-32L)) = 4214984UL;
                                    indirect_placeholder();
                                    var_82 = local_sp_6 + (-40L);
                                    *(uint64_t *)var_82 = 4214995UL;
                                    indirect_placeholder();
                                    var_83 = (uint64_t *)(local_sp_6 + 8UL);
                                    var_84 = *var_83;
                                    var_85 = (var_84 == 0UL);
                                    var_86 = (var_81 << 1UL) | 1UL;
                                    local_sp_0 = var_82;
                                    if (var_85) {
                                        *var_76 = var_81;
                                        var_93 = helper_cc_compute_c_wrapper(var_86 + (-4096L), 4096UL, var_4, 17U);
                                        var_94 = (var_93 == 0UL) ? var_86 : 4096UL;
                                        *var_83 = var_94;
                                        var_95 = local_sp_6 + (-48L);
                                        *(uint64_t *)var_95 = 4215411UL;
                                        var_96 = indirect_placeholder_65(var_94);
                                        *var_70 = var_96.field_0;
                                        local_sp_0 = var_95;
                                    } else {
                                        var_87 = helper_cc_compute_c_wrapper(var_84 - var_86, var_86, var_4, 17U);
                                        if (var_87 == 0UL) {
                                            var_88 = (uint64_t *)local_sp_6;
                                            var_89 = *var_88;
                                            *var_76 = var_86;
                                            *var_72 = var_81;
                                            var_90 = local_sp_6 + (-48L);
                                            *(uint64_t *)var_90 = 4215319UL;
                                            var_91 = indirect_placeholder_64(var_89, var_86);
                                            var_92 = *var_80;
                                            *var_70 = var_91.field_0;
                                            *var_88 = var_92;
                                            local_sp_0 = var_90;
                                        }
                                    }
                                    var_97 = *(uint64_t *)(local_sp_0 + 40UL);
                                    *(uint64_t *)(local_sp_0 + (-8L)) = 4215047UL;
                                    indirect_placeholder();
                                    *(uint64_t *)(local_sp_0 + (-16L)) = 4215063UL;
                                    indirect_placeholder();
                                    var_98 = *(unsigned char *)var_81;
                                    *(uint64_t *)(local_sp_0 + 8UL) = var_97;
                                    var_99 = (var_98 == '/');
                                    var_100 = var_77 + 1UL;
                                    rbx_1 = var_100;
                                    if (!var_99) {
                                        rbx_1 = var_46;
                                        var_101 = var_46 + (-1L);
                                        var_102 = helper_cc_compute_c_wrapper(var_77 - var_101, var_101, var_4, 17U);
                                        rbx_0 = var_101;
                                        rbx_1 = var_101;
                                        if (var_46 <= var_100 & var_102 != 0UL & *(unsigned char *)(var_46 + (-2L)) == '/') {
                                            var_103 = rbx_0 + (-1L);
                                            rbx_0 = var_103;
                                            rbx_1 = var_77;
                                            while (var_103 != var_77)
                                                {
                                                    rbx_1 = var_103;
                                                    if (*(unsigned char *)(rbx_0 + (-2L)) == '/') {
                                                        break;
                                                    }
                                                    var_103 = rbx_0 + (-1L);
                                                    rbx_0 = var_103;
                                                    rbx_1 = var_77;
                                                }
                                        }
                                    }
                                    *(unsigned char *)var_77 = (unsigned char)'/';
                                    var_104 = local_sp_0 + (-24L);
                                    *(uint64_t *)var_104 = 4215134UL;
                                    indirect_placeholder();
                                    var_105 = *(uint64_t *)local_sp_0;
                                    rbx_6 = rbx_1;
                                    local_sp_14 = var_104;
                                    r14_2 = var_105;
                                }
                            }
                            break;
                        }
                        if (switch_state_var)
                            break;
                    }
                switch_state_var = 0;
                switch (loop_state_var) {
                  case 4U:
                    {
                        rbx_3 = rbx_3_be;
                        continue;
                    }
                    break;
                  case 1U:
                    {
                        var_107 = local_sp_1 + (-8L);
                        *(uint64_t *)var_107 = 4215288UL;
                        indirect_placeholder();
                        local_sp_2 = var_107;
                        storemerge = *(volatile uint32_t *)(uint32_t *)0UL;
                        loop_state_var = 2U;
                        switch_state_var = 1;
                        break;
                    }
                    break;
                  case 2U:
                    {
                        *(uint64_t *)(local_sp_10 + (-8L)) = 4214791UL;
                        indirect_placeholder();
                        var_117 = local_sp_10 + (-16L);
                        *(uint64_t *)var_117 = 4214799UL;
                        indirect_placeholder();
                        rax_1 = rax_6;
                        r13_0 = r13_2_shrunk;
                        local_sp_3 = var_117;
                        rax_2 = rax_6;
                        r13_1 = r13_2_shrunk;
                        local_sp_4 = var_117;
                        if (*(uint64_t *)(local_sp_10 + 16UL) != 0UL) {
                            loop_state_var = 1U;
                            switch_state_var = 1;
                            break;
                        }
                        loop_state_var = 0U;
                        switch_state_var = 1;
                        break;
                    }
                    break;
                  case 0U:
                    {
                        loop_state_var = 2U;
                        switch_state_var = 1;
                        break;
                    }
                    break;
                  case 3U:
                    {
                        loop_state_var = 3U;
                        switch_state_var = 1;
                        break;
                    }
                    break;
                }
                if (switch_state_var)
                    break;
            }
        switch (loop_state_var) {
          case 3U:
            {
                rbx_8 = rbx_7;
                local_sp_17 = local_sp_16;
                r12_7 = r12_6;
                if (rbx_7 > (r12_6 + 1UL)) {
                    rbx_8 = rbx_7 + (*(unsigned char *)(rbx_7 + (-1L)) == '/');
                }
                *(unsigned char *)rbx_8 = (unsigned char)'\x00';
                if (rbp_5 == (rbx_8 + 1UL)) {
                    var_120 = (rbx_8 - r12_6) + 1UL;
                    var_121 = local_sp_16 + (-8L);
                    *(uint64_t *)var_121 = 4214461UL;
                    var_122 = indirect_placeholder_70(r12_6, var_120);
                    local_sp_17 = var_121;
                    r12_7 = var_122.field_0;
                }
                var_123 = (uint64_t *)(local_sp_17 + (-8L));
                *var_123 = 4214474UL;
                indirect_placeholder();
                var_124 = *(uint64_t *)(local_sp_17 + 24UL);
                rax_8 = r12_7;
                if (var_124 == 0UL) {
                    *(uint64_t *)local_sp_17 = r12_7;
                    *(uint64_t *)(local_sp_17 + (-16L)) = 4214504UL;
                    indirect_placeholder_39(r12_7, var_124);
                    var_125 = *var_123;
                    rax_8 = var_125;
                }
            }
            break;
          case 2U:
          case 1U:
          case 0U:
            {
                switch (loop_state_var) {
                  case 0U:
                    {
                        *(uint64_t *)(local_sp_4 + (-8L)) = 4214900UL;
                        indirect_placeholder();
                        *(uint32_t *)rax_2 = r13_1;
                    }
                    break;
                  case 2U:
                  case 1U:
                    {
                        switch (loop_state_var) {
                          case 1U:
                            {
                                var_118 = *(uint64_t *)(local_sp_3 + 32UL);
                                var_119 = local_sp_3 + (-8L);
                                *(uint64_t *)var_119 = 4214895UL;
                                indirect_placeholder_31(var_118);
                                rax_2 = rax_1;
                                r13_1 = r13_0;
                                local_sp_4 = var_119;
                            }
                            break;
                          case 2U:
                            {
                                *(uint64_t *)(local_sp_2 + (-8L)) = 4214877UL;
                                indirect_placeholder();
                                var_108 = local_sp_2 + (-16L);
                                *(uint64_t *)var_108 = 4214885UL;
                                indirect_placeholder();
                                rax_1 = rax_0;
                                r13_0 = storemerge;
                                local_sp_3 = var_108;
                            }
                            break;
                        }
                    }
                    break;
                }
            }
            break;
        }
    }
}

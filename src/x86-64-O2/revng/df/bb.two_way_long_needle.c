typedef unsigned char __u_char;
typedef unsigned short __u_short;
typedef unsigned int __u_int;
typedef unsigned long __u_long;
typedef signed char __int8_t;
typedef unsigned char __uint8_t;
typedef short __int16_t;
typedef unsigned short __uint16_t;
typedef int __int32_t;
typedef unsigned int __uint32_t;
typedef long __int64_t;
typedef unsigned long __uint64_t;
typedef __int8_t __int_least8_t;
typedef __uint8_t __uint_least8_t;
typedef __int16_t __int_least16_t;
typedef __uint16_t __uint_least16_t;
typedef __int32_t __int_least32_t;
typedef __uint32_t __uint_least32_t;
typedef __int64_t __int_least64_t;
typedef __uint64_t __uint_least64_t;
typedef long __quad_t;
typedef unsigned long __u_quad_t;
typedef long __intmax_t;
typedef unsigned long __uintmax_t;
typedef unsigned long __dev_t;
typedef unsigned int __uid_t;
typedef unsigned int __gid_t;
typedef unsigned long __ino_t;
typedef unsigned long __ino64_t;
typedef unsigned int __mode_t;
typedef unsigned long __nlink_t;
typedef long __off_t;
typedef long __off64_t;
typedef int __pid_t;
typedef struct {
    int __val[2];
} __fsid_t;
typedef long __clock_t;
typedef unsigned long __rlim_t;
typedef unsigned long __rlim64_t;
typedef unsigned int __id_t;
typedef long __time_t;
typedef unsigned int __useconds_t;
typedef long __suseconds_t;
typedef int __daddr_t;
typedef int __key_t;
typedef int __clockid_t;
typedef void *__timer_t;
typedef long __blksize_t;
typedef long __blkcnt_t;
typedef long __blkcnt64_t;
typedef unsigned long __fsblkcnt_t;
typedef unsigned long __fsblkcnt64_t;
typedef unsigned long __fsfilcnt_t;
typedef unsigned long __fsfilcnt64_t;
typedef long __fsword_t;
typedef long __ssize_t;
typedef long __syscall_slong_t;
typedef unsigned long __syscall_ulong_t;
typedef __off64_t __loff_t;
typedef char *__caddr_t;
typedef long __intptr_t;
typedef unsigned int __socklen_t;
typedef int __sig_atomic_t;
typedef __int8_t int8_t;
typedef __int16_t int16_t;
typedef __int32_t int32_t;
typedef __int64_t int64_t;
typedef __uint8_t uint8_t;
typedef __uint16_t uint16_t;
typedef __uint32_t uint32_t;
typedef __uint64_t uint64_t;
typedef __int_least8_t int_least8_t;
typedef __int_least16_t int_least16_t;
typedef __int_least32_t int_least32_t;
typedef __int_least64_t int_least64_t;
typedef __uint_least8_t uint_least8_t;
typedef __uint_least16_t uint_least16_t;
typedef __uint_least32_t uint_least32_t;
typedef __uint_least64_t uint_least64_t;
typedef signed char int_fast8_t;
typedef long int_fast16_t;
typedef long int_fast32_t;
typedef long int_fast64_t;
typedef unsigned char uint_fast8_t;
typedef unsigned long uint_fast16_t;
typedef unsigned long uint_fast32_t;
typedef unsigned long uint_fast64_t;
typedef long intptr_t;
typedef unsigned long uintptr_t;
typedef __intmax_t intmax_t;
typedef __uintmax_t uintmax_t;
typedef _Bool bool;
struct indirect_placeholder_156_ret_type;
struct indirect_placeholder_157_ret_type;
struct indirect_placeholder_156_ret_type {
    uint64_t field_0;
    uint64_t field_1;
};
struct indirect_placeholder_157_ret_type {
    uint64_t field_0;
    uint64_t field_1;
};
extern uint64_t revng_init_local_sp(uint64_t param_0);
extern uint64_t init_rbx(void);
extern uint64_t init_rbp(void);
extern void indirect_placeholder(void);
extern uint64_t init_cc_src2(void);
extern uint64_t helper_cc_compute_c_wrapper(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint32_t param_3);
extern uint64_t init_r13(void);
extern uint64_t init_r12(void);
extern uint64_t init_r14(void);
extern uint64_t init_r15(void);
extern struct indirect_placeholder_156_ret_type indirect_placeholder_156(uint64_t param_0, uint64_t param_1, uint64_t param_2);
extern struct indirect_placeholder_157_ret_type indirect_placeholder_157(uint64_t param_0, uint64_t param_1, uint64_t param_2);
extern uint64_t indirect_placeholder_18(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint64_t param_3);
void bb_two_way_long_needle(uint64_t rcx, uint64_t rdi, uint64_t rdx, uint64_t rsi) {
    uint64_t r12_2;
    uint64_t var_0;
    uint64_t var_1;
    uint64_t var_2;
    uint64_t var_3;
    uint64_t var_4;
    uint64_t var_5;
    uint64_t var_6;
    uint64_t var_7;
    uint64_t var_8;
    uint64_t var_35;
    uint64_t var_37;
    uint64_t var_38;
    uint64_t local_sp_2;
    uint64_t rdx3_1;
    uint64_t rdx3_0;
    uint64_t var_39;
    uint64_t var_40;
    uint64_t r15_1;
    uint64_t var_41;
    uint64_t r15_0;
    uint64_t var_42;
    uint64_t r12_0;
    uint64_t r8_1;
    uint64_t rdx3_2;
    uint64_t var_43;
    uint64_t var_44;
    uint64_t var_36;
    uint64_t var_58;
    uint64_t var_59;
    uint64_t var_60;
    uint64_t var_65;
    uint64_t var_66;
    uint64_t var_67;
    uint64_t var_68;
    uint64_t var_69;
    uint64_t rax_1;
    uint64_t rax_0;
    uint64_t var_70;
    uint64_t var_71;
    uint64_t var_72;
    uint64_t rax_4;
    uint64_t var_73;
    uint64_t rax_3;
    uint64_t var_74;
    uint64_t var_75;
    uint64_t rax_2;
    uint64_t var_76;
    uint64_t var_77;
    uint64_t r12_1;
    uint64_t var_61;
    uint64_t var_62;
    uint64_t var_63;
    uint64_t var_64;
    uint64_t storemerge;
    uint64_t rcx1_0;
    uint64_t var_45;
    uint64_t var_46;
    uint64_t var_47;
    uint64_t var_48;
    uint64_t r8_0;
    uint64_t local_sp_0;
    uint64_t var_49;
    uint64_t var_50;
    uint64_t *var_51;
    uint64_t *var_52;
    uint64_t var_53;
    uint64_t var_54;
    uint64_t *var_55;
    struct indirect_placeholder_156_ret_type var_56;
    uint64_t var_57;
    uint64_t var_21;
    uint64_t var_22;
    uint64_t var_23;
    uint64_t var_24;
    uint64_t var_25;
    unsigned char *var_26;
    uint64_t local_sp_1;
    uint64_t var_27;
    uint64_t var_28;
    uint64_t *var_29;
    uint64_t var_30;
    uint64_t var_31;
    uint64_t *var_32;
    struct indirect_placeholder_157_ret_type var_33;
    uint64_t var_34;
    uint64_t var_9;
    uint64_t r8_2;
    uint64_t r13_0;
    uint64_t rax_5;
    bool rax_7;
    uint64_t rax_6;
    uint64_t var_14;
    uint64_t var_15;
    uint64_t var_16;
    uint64_t *var_17;
    uint64_t var_18;
    uint64_t *var_19;
    uint64_t var_20;
    uint64_t var_10;
    uint64_t var_11;
    uint64_t *var_12;
    uint64_t var_13;
    unsigned int loop_state_var;
    bool switch_state_var;
    var_0 = revng_init_local_sp(0UL);
    var_1 = init_rbx();
    var_2 = init_rbp();
    var_3 = init_r13();
    var_4 = init_cc_src2();
    var_5 = init_r12();
    var_6 = init_r15();
    var_7 = init_r14();
    *(uint64_t *)(var_0 + (-8L)) = var_6;
    *(uint64_t *)(var_0 + (-16L)) = var_7;
    *(uint64_t *)(var_0 + (-24L)) = var_3;
    *(uint64_t *)(var_0 + (-32L)) = var_5;
    *(uint64_t *)(var_0 + (-40L)) = var_2;
    *(uint64_t *)(var_0 + (-48L)) = var_1;
    var_8 = var_0 + (-2168L);
    r12_2 = 0UL;
    local_sp_2 = var_8;
    r15_1 = 0UL;
    storemerge = 0UL;
    rcx1_0 = 0UL;
    r8_2 = rsi;
    rax_7 = 1;
    rax_6 = 0UL;
    if (rcx > 2UL) {
        var_10 = var_0 + (-2112L);
        *(uint64_t *)var_8 = rsi;
        var_11 = var_0 + (-2176L);
        var_12 = (uint64_t *)var_11;
        *var_12 = 4241630UL;
        var_13 = indirect_placeholder_18(rdx, rsi, var_10, rcx);
        r8_2 = *var_12;
        r13_0 = var_13;
        local_sp_2 = var_11;
    } else {
        var_9 = rcx + (-1L);
        *(uint64_t *)(var_0 + (-2112L)) = 1UL;
        r13_0 = var_9;
    }
    rdx3_0 = r13_0;
    rdx3_1 = r13_0;
    rax_3 = r13_0;
    rax_5 = local_sp_2 + 64UL;
    *(uint64_t *)rax_5 = rcx;
    while (rax_5 != (local_sp_2 + 2104UL))
        {
            rax_5 = rax_5 + 8UL;
            *(uint64_t *)rax_5 = rcx;
        }
    if (rcx != 0UL) {
        var_14 = (uint64_t)*(unsigned char *)(rax_6 + rdx);
        var_15 = (rax_6 ^ (-1L)) + rcx;
        var_16 = rax_6 + 1UL;
        *(uint64_t *)(((var_14 << 3UL) + local_sp_2) + 64UL) = var_15;
        rax_6 = var_16;
        do {
            var_14 = (uint64_t)*(unsigned char *)(rax_6 + rdx);
            var_15 = (rax_6 ^ (-1L)) + rcx;
            var_16 = rax_6 + 1UL;
            *(uint64_t *)(((var_14 << 3UL) + local_sp_2) + 64UL) = var_15;
            rax_6 = var_16;
        } while (var_16 != rcx);
        rax_7 = ((uint64_t)(uint32_t)rcx == 0UL);
    }
    var_17 = (uint64_t *)local_sp_2;
    *var_17 = r8_2;
    var_18 = local_sp_2 + (-8L);
    var_19 = (uint64_t *)var_18;
    *var_19 = 4240989UL;
    indirect_placeholder();
    var_20 = *var_19;
    r8_0 = var_20;
    local_sp_0 = var_18;
    r8_1 = var_20;
    local_sp_1 = var_18;
    if (rax_7) {
        var_45 = *(uint64_t *)(local_sp_2 + 48UL);
        var_46 = rcx + (-1L);
        var_47 = rcx - var_45;
        *(uint64_t *)(local_sp_2 + 8UL) = var_45;
        var_48 = r13_0 + (-1L);
        *(uint64_t *)(local_sp_2 + 16UL) = var_47;
        *(uint64_t *)(local_sp_2 + 24UL) = var_48;
        *(uint64_t *)(local_sp_2 + 32UL) = (1UL - r13_0);
        var_49 = r12_2 + rcx;
        var_50 = r8_0 + rdi;
        var_51 = (uint64_t *)(local_sp_0 + 8UL);
        *var_51 = rcx1_0;
        var_52 = (uint64_t *)local_sp_0;
        *var_52 = var_49;
        var_53 = var_49 - r8_0;
        var_54 = local_sp_0 + (-8L);
        var_55 = (uint64_t *)var_54;
        *var_55 = 4241110UL;
        var_56 = indirect_placeholder_156(var_50, var_53, 0UL);
        var_57 = *var_55;
        r12_0 = r12_2;
        r8_0 = var_57;
        local_sp_0 = var_54;
        while (var_57 != 0UL)
            {
                var_58 = var_56.field_0;
                var_59 = *var_52;
                if (var_58 == 0UL) {
                    break;
                }
                var_60 = *(uint64_t *)((((uint64_t)*(unsigned char *)((var_57 + rdi) + (-1L)) << 3UL) + var_54) + 64UL);
                rax_4 = var_60;
                if (var_60 != 0UL) {
                    if (var_59 == 0UL) {
                        var_61 = *var_51;
                        var_62 = *(uint64_t *)(local_sp_0 + 16UL);
                        var_63 = helper_cc_compute_c_wrapper(var_60 - var_61, var_61, var_4, 17U);
                        var_64 = (var_63 == 0UL) ? var_60 : var_62;
                        rax_4 = var_64;
                    }
                    r12_1 = r12_0 + rax_4;
                    rcx1_0 = storemerge;
                    r12_2 = r12_1;
                    var_49 = r12_2 + rcx;
                    var_50 = r8_0 + rdi;
                    var_51 = (uint64_t *)(local_sp_0 + 8UL);
                    *var_51 = rcx1_0;
                    var_52 = (uint64_t *)local_sp_0;
                    *var_52 = var_49;
                    var_53 = var_49 - r8_0;
                    var_54 = local_sp_0 + (-8L);
                    var_55 = (uint64_t *)var_54;
                    *var_55 = 4241110UL;
                    var_56 = indirect_placeholder_156(var_50, var_53, 0UL);
                    var_57 = *var_55;
                    r12_0 = r12_2;
                    r8_0 = var_57;
                    local_sp_0 = var_54;
                    continue;
                }
                var_65 = var_59 - r13_0;
                var_66 = helper_cc_compute_c_wrapper(var_65, r13_0, var_4, 17U);
                var_67 = (var_66 == 0UL) ? var_59 : r13_0;
                var_68 = helper_cc_compute_c_wrapper(var_67 - var_46, var_46, var_4, 17U);
                rax_0 = var_67;
                rax_1 = var_67;
                if (var_68 != 0UL) {
                    var_69 = r12_2 + rdi;
                    if ((uint64_t)(*(unsigned char *)(var_67 + rdx) - *(unsigned char *)(var_67 + var_69)) != 0UL) {
                        var_72 = r12_2 + *(uint64_t *)(local_sp_0 + 32UL);
                        rax_4 = rax_1;
                        r12_0 = var_72;
                        r12_1 = r12_0 + rax_4;
                        rcx1_0 = storemerge;
                        r12_2 = r12_1;
                        var_49 = r12_2 + rcx;
                        var_50 = r8_0 + rdi;
                        var_51 = (uint64_t *)(local_sp_0 + 8UL);
                        *var_51 = rcx1_0;
                        var_52 = (uint64_t *)local_sp_0;
                        *var_52 = var_49;
                        var_53 = var_49 - r8_0;
                        var_54 = local_sp_0 + (-8L);
                        var_55 = (uint64_t *)var_54;
                        *var_55 = 4241110UL;
                        var_56 = indirect_placeholder_156(var_50, var_53, 0UL);
                        var_57 = *var_55;
                        r12_0 = r12_2;
                        r8_0 = var_57;
                        local_sp_0 = var_54;
                        continue;
                    }
                    while (1U)
                        {
                            var_70 = rax_0 + 1UL;
                            var_71 = helper_cc_compute_c_wrapper(var_70 - var_46, var_46, var_4, 17U);
                            rax_0 = var_70;
                            rax_1 = var_70;
                            if (var_71 != 0UL) {
                                loop_state_var = 0U;
                                break;
                            }
                            if ((uint64_t)(*(unsigned char *)(var_70 + rdx) - *(unsigned char *)(var_70 + var_69)) == 0UL) {
                                continue;
                            }
                            loop_state_var = 1U;
                            break;
                        }
                    switch (loop_state_var) {
                      case 0U:
                        {
                            break;
                        }
                        break;
                      case 1U:
                        {
                            var_72 = r12_2 + *(uint64_t *)(local_sp_0 + 32UL);
                            rax_4 = rax_1;
                            r12_0 = var_72;
                            r12_1 = r12_0 + rax_4;
                            rcx1_0 = storemerge;
                            r12_2 = r12_1;
                            var_49 = r12_2 + rcx;
                            var_50 = r8_0 + rdi;
                            var_51 = (uint64_t *)(local_sp_0 + 8UL);
                            *var_51 = rcx1_0;
                            var_52 = (uint64_t *)local_sp_0;
                            *var_52 = var_49;
                            var_53 = var_49 - r8_0;
                            var_54 = local_sp_0 + (-8L);
                            var_55 = (uint64_t *)var_54;
                            *var_55 = 4241110UL;
                            var_56 = indirect_placeholder_156(var_50, var_53, 0UL);
                            var_57 = *var_55;
                            r12_0 = r12_2;
                            r8_0 = var_57;
                            local_sp_0 = var_54;
                            continue;
                        }
                        break;
                    }
                }
                var_73 = helper_cc_compute_c_wrapper(var_65, r13_0, var_4, 17U);
                var_74 = *(uint64_t *)(local_sp_0 + 24UL);
                var_75 = r12_2 + rdi;
                rax_2 = var_74;
                if (var_73 != 0UL & (uint64_t)(*(unsigned char *)(var_74 + rdx) - *(unsigned char *)(var_74 + var_75)) == 0UL) {
                    rax_3 = var_59;
                    while (rax_2 != var_59)
                        {
                            rax_3 = rax_2;
                            if ((uint64_t)(*(unsigned char *)((rax_2 + rdx) + (-1L)) - *(unsigned char *)((var_75 + rax_2) + (-1L))) == 0UL) {
                                break;
                            }
                            rax_2 = rax_2 + (-1L);
                            rax_3 = var_59;
                        }
                }
                if ((var_59 + 1UL) <= rax_3) {
                    break;
                }
                var_76 = r12_2 + *var_51;
                var_77 = *(uint64_t *)(local_sp_0 + 16UL);
                r12_1 = var_76;
                storemerge = var_77;
            }
    } else {
        var_21 = rcx + (-1L);
        var_22 = rcx - r13_0;
        var_23 = helper_cc_compute_c_wrapper(var_22 - r13_0, r13_0, var_4, 17U);
        var_24 = ((var_23 == 0UL) ? var_22 : r13_0) + 1UL;
        *(uint64_t *)(local_sp_2 + 16UL) = var_24;
        *(uint64_t *)(local_sp_2 + 48UL) = var_24;
        *var_17 = (r13_0 + (-1L));
        *(uint64_t *)(local_sp_2 + 8UL) = (1UL - r13_0);
        var_25 = r13_0 - var_21;
        var_26 = (unsigned char *)(r13_0 + rdx);
        var_27 = r15_1 + rcx;
        var_28 = r8_1 + rdi;
        var_29 = (uint64_t *)local_sp_1;
        *var_29 = var_27;
        var_30 = var_27 - r8_1;
        var_31 = local_sp_1 + (-8L);
        var_32 = (uint64_t *)var_31;
        *var_32 = 4241401UL;
        var_33 = indirect_placeholder_157(var_28, var_30, 0UL);
        var_34 = *var_32;
        r8_1 = var_34;
        local_sp_1 = var_31;
        while (!((var_34 != 0UL) && (var_33.field_0 == 0UL)))
            {
                var_35 = *(uint64_t *)((((uint64_t)*(unsigned char *)((var_34 + rdi) + (-1L)) << 3UL) + var_31) + 64UL);
                if (var_35 != 0UL) {
                    var_36 = r15_1 + var_35;
                    r15_0 = var_36;
                    r15_1 = r15_0;
                    var_27 = r15_1 + rcx;
                    var_28 = r8_1 + rdi;
                    var_29 = (uint64_t *)local_sp_1;
                    *var_29 = var_27;
                    var_30 = var_27 - r8_1;
                    var_31 = local_sp_1 + (-8L);
                    var_32 = (uint64_t *)var_31;
                    *var_32 = 4241401UL;
                    var_33 = indirect_placeholder_157(var_28, var_30, 0UL);
                    var_34 = *var_32;
                    r8_1 = var_34;
                    local_sp_1 = var_31;
                    continue;
                }
                var_37 = r15_1 + rdi;
                var_38 = helper_cc_compute_c_wrapper(var_25, var_21, var_4, 17U);
                if (var_38 != 0UL) {
                    if ((uint64_t)(*var_26 - *(unsigned char *)(r13_0 + var_37)) != 0UL) {
                        var_41 = rdx3_1 + (r15_1 + *(uint64_t *)(local_sp_1 + 8UL));
                        r15_0 = var_41;
                        r15_1 = r15_0;
                        var_27 = r15_1 + rcx;
                        var_28 = r8_1 + rdi;
                        var_29 = (uint64_t *)local_sp_1;
                        *var_29 = var_27;
                        var_30 = var_27 - r8_1;
                        var_31 = local_sp_1 + (-8L);
                        var_32 = (uint64_t *)var_31;
                        *var_32 = 4241401UL;
                        var_33 = indirect_placeholder_157(var_28, var_30, 0UL);
                        var_34 = *var_32;
                        r8_1 = var_34;
                        local_sp_1 = var_31;
                        continue;
                    }
                    while (1U)
                        {
                            var_39 = rdx3_0 + 1UL;
                            var_40 = helper_cc_compute_c_wrapper(var_39 - var_21, var_21, var_4, 17U);
                            rdx3_0 = var_39;
                            rdx3_1 = var_39;
                            if (var_40 != 0UL) {
                                loop_state_var = 0U;
                                break;
                            }
                            if ((uint64_t)(*(unsigned char *)(var_39 + rdx) - *(unsigned char *)(var_39 + var_37)) == 0UL) {
                                continue;
                            }
                            loop_state_var = 1U;
                            break;
                        }
                    switch (loop_state_var) {
                      case 0U:
                        {
                            break;
                        }
                        break;
                      case 1U:
                        {
                            var_41 = rdx3_1 + (r15_1 + *(uint64_t *)(local_sp_1 + 8UL));
                            r15_0 = var_41;
                            r15_1 = r15_0;
                            var_27 = r15_1 + rcx;
                            var_28 = r8_1 + rdi;
                            var_29 = (uint64_t *)local_sp_1;
                            *var_29 = var_27;
                            var_30 = var_27 - r8_1;
                            var_31 = local_sp_1 + (-8L);
                            var_32 = (uint64_t *)var_31;
                            *var_32 = 4241401UL;
                            var_33 = indirect_placeholder_157(var_28, var_30, 0UL);
                            var_34 = *var_32;
                            r8_1 = var_34;
                            local_sp_1 = var_31;
                            continue;
                        }
                        break;
                    }
                }
                var_42 = *var_29;
                rdx3_2 = var_42;
                if (var_42 == 18446744073709551615UL) {
                    break;
                }
                if ((uint64_t)(*(unsigned char *)(var_42 + rdx) - *(unsigned char *)(var_42 + var_37)) != 0UL) {
                    while (1U)
                        {
                            var_43 = rdx3_2 + (-1L);
                            rdx3_2 = var_43;
                            if (rdx3_2 != 0UL) {
                                loop_state_var = 0U;
                                break;
                            }
                            if ((uint64_t)(*(unsigned char *)(var_43 + rdx) - *(unsigned char *)(var_43 + var_37)) == 0UL) {
                                continue;
                            }
                            loop_state_var = 1U;
                            break;
                        }
                    switch_state_var = 0;
                    switch (loop_state_var) {
                      case 1U:
                        {
                            break;
                        }
                        break;
                      case 0U:
                        {
                            switch_state_var = 1;
                            break;
                        }
                        break;
                    }
                    if (switch_state_var)
                        break;
                }
                var_44 = r15_1 + *(uint64_t *)(local_sp_1 + 16UL);
                r15_0 = var_44;
            }
    }
    return;
}

typedef unsigned char __u_char;
typedef unsigned short __u_short;
typedef unsigned int __u_int;
typedef unsigned long __u_long;
typedef signed char __int8_t;
typedef unsigned char __uint8_t;
typedef short __int16_t;
typedef unsigned short __uint16_t;
typedef int __int32_t;
typedef unsigned int __uint32_t;
typedef long __int64_t;
typedef unsigned long __uint64_t;
typedef __int8_t __int_least8_t;
typedef __uint8_t __uint_least8_t;
typedef __int16_t __int_least16_t;
typedef __uint16_t __uint_least16_t;
typedef __int32_t __int_least32_t;
typedef __uint32_t __uint_least32_t;
typedef __int64_t __int_least64_t;
typedef __uint64_t __uint_least64_t;
typedef long __quad_t;
typedef unsigned long __u_quad_t;
typedef long __intmax_t;
typedef unsigned long __uintmax_t;
typedef unsigned long __dev_t;
typedef unsigned int __uid_t;
typedef unsigned int __gid_t;
typedef unsigned long __ino_t;
typedef unsigned long __ino64_t;
typedef unsigned int __mode_t;
typedef unsigned long __nlink_t;
typedef long __off_t;
typedef long __off64_t;
typedef int __pid_t;
typedef struct {
    int __val[2];
} __fsid_t;
typedef long __clock_t;
typedef unsigned long __rlim_t;
typedef unsigned long __rlim64_t;
typedef unsigned int __id_t;
typedef long __time_t;
typedef unsigned int __useconds_t;
typedef long __suseconds_t;
typedef int __daddr_t;
typedef int __key_t;
typedef int __clockid_t;
typedef void *__timer_t;
typedef long __blksize_t;
typedef long __blkcnt_t;
typedef long __blkcnt64_t;
typedef unsigned long __fsblkcnt_t;
typedef unsigned long __fsblkcnt64_t;
typedef unsigned long __fsfilcnt_t;
typedef unsigned long __fsfilcnt64_t;
typedef long __fsword_t;
typedef long __ssize_t;
typedef long __syscall_slong_t;
typedef unsigned long __syscall_ulong_t;
typedef __off64_t __loff_t;
typedef char *__caddr_t;
typedef long __intptr_t;
typedef unsigned int __socklen_t;
typedef int __sig_atomic_t;
typedef __int8_t int8_t;
typedef __int16_t int16_t;
typedef __int32_t int32_t;
typedef __int64_t int64_t;
typedef __uint8_t uint8_t;
typedef __uint16_t uint16_t;
typedef __uint32_t uint32_t;
typedef __uint64_t uint64_t;
typedef __int_least8_t int_least8_t;
typedef __int_least16_t int_least16_t;
typedef __int_least32_t int_least32_t;
typedef __int_least64_t int_least64_t;
typedef __uint_least8_t uint_least8_t;
typedef __uint_least16_t uint_least16_t;
typedef __uint_least32_t uint_least32_t;
typedef __uint_least64_t uint_least64_t;
typedef signed char int_fast8_t;
typedef long int_fast16_t;
typedef long int_fast32_t;
typedef long int_fast64_t;
typedef unsigned char uint_fast8_t;
typedef unsigned long uint_fast16_t;
typedef unsigned long uint_fast32_t;
typedef unsigned long uint_fast64_t;
typedef long intptr_t;
typedef unsigned long uintptr_t;
typedef __intmax_t intmax_t;
typedef __uintmax_t uintmax_t;
typedef _Bool bool;
struct bb_hash_lookup_ret_type;
struct bb_hash_lookup_ret_type {
    uint64_t field_0;
    uint64_t field_1;
    uint64_t field_2;
};
extern uint64_t revng_init_local_sp(uint64_t param_0);
extern uint64_t init_rbx(void);
extern uint64_t init_rbp(void);
extern void indirect_placeholder(void);
extern uint64_t init_r12(void);
extern uint64_t indirect_placeholder_4(uint64_t param_0, uint64_t param_1);
struct bb_hash_lookup_ret_type bb_hash_lookup(uint64_t rdi, uint64_t rsi) {
    uint64_t var_0;
    uint64_t var_1;
    uint64_t var_2;
    uint64_t var_3;
    uint64_t var_4;
    uint64_t var_5;
    uint64_t var_6;
    uint64_t rdi6_2;
    uint64_t rdi6_0;
    uint64_t var_9;
    uint64_t local_sp_0;
    uint64_t rdi6_1;
    bool var_7;
    struct bb_hash_lookup_ret_type mrv5;
    uint64_t rbx_0;
    uint64_t storemerge_in_sroa_speculated;
    uint64_t var_8;
    uint64_t rsi7_0;
    struct bb_hash_lookup_ret_type mrv;
    struct bb_hash_lookup_ret_type mrv1;
    struct bb_hash_lookup_ret_type mrv2;
    uint64_t rsi7_1;
    struct bb_hash_lookup_ret_type mrv4 = {0UL, /*implicit*/(int)0, /*implicit*/(int)0};
    unsigned int loop_state_var;
    var_0 = revng_init_local_sp(0UL);
    var_1 = init_rbx();
    var_2 = init_rbp();
    var_3 = init_r12();
    *(uint64_t *)(var_0 + (-8L)) = var_3;
    *(uint64_t *)(var_0 + (-16L)) = var_2;
    *(uint64_t *)(var_0 + (-24L)) = var_1;
    var_4 = var_0 + (-32L);
    *(uint64_t *)var_4 = 4241855UL;
    var_5 = indirect_placeholder_4(rdi, rsi);
    var_6 = *(uint64_t *)var_5;
    rdi6_2 = rdi;
    rdi6_0 = rdi;
    local_sp_0 = var_4;
    rbx_0 = var_5;
    storemerge_in_sroa_speculated = var_6;
    rsi7_0 = rsi;
    rsi7_1 = rsi;
    if (var_6 == 0UL) {
        mrv4.field_1 = rdi6_2;
        mrv5 = mrv4;
        mrv5.field_2 = rsi7_1;
        return mrv5;
    }
    var_7 = ((uint64_t)(unsigned char)var_6 == 0UL);
    rdi6_2 = rsi;
    while (1U)
        {
            rdi6_0 = rsi;
            rdi6_1 = rdi6_0;
            rsi7_1 = storemerge_in_sroa_speculated;
            if (storemerge_in_sroa_speculated != rsi) {
                loop_state_var = 0U;
                break;
            }
            var_8 = local_sp_0 + (-8L);
            *(uint64_t *)var_8 = 4241888UL;
            indirect_placeholder();
            local_sp_0 = var_8;
            rdi6_1 = rsi;
            if (!var_7) {
                rsi7_0 = *(uint64_t *)rbx_0;
                loop_state_var = 0U;
                break;
            }
            var_9 = *(uint64_t *)(rbx_0 + 8UL);
            rbx_0 = var_9;
            if (var_9 != 0UL) {
                loop_state_var = 1U;
                break;
            }
            storemerge_in_sroa_speculated = *(uint64_t *)var_9;
            continue;
        }
    switch (loop_state_var) {
      case 1U:
        {
            break;
        }
        break;
      case 0U:
        {
            mrv.field_0 = rsi7_0;
            mrv1 = mrv;
            mrv1.field_1 = rdi6_1;
            mrv2 = mrv1;
            mrv2.field_2 = rsi7_0;
            return mrv2;
        }
        break;
    }
}

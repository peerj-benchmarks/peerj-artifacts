typedef unsigned char __u_char;
typedef unsigned short __u_short;
typedef unsigned int __u_int;
typedef unsigned long __u_long;
typedef signed char __int8_t;
typedef unsigned char __uint8_t;
typedef short __int16_t;
typedef unsigned short __uint16_t;
typedef int __int32_t;
typedef unsigned int __uint32_t;
typedef long __int64_t;
typedef unsigned long __uint64_t;
typedef __int8_t __int_least8_t;
typedef __uint8_t __uint_least8_t;
typedef __int16_t __int_least16_t;
typedef __uint16_t __uint_least16_t;
typedef __int32_t __int_least32_t;
typedef __uint32_t __uint_least32_t;
typedef __int64_t __int_least64_t;
typedef __uint64_t __uint_least64_t;
typedef long __quad_t;
typedef unsigned long __u_quad_t;
typedef long __intmax_t;
typedef unsigned long __uintmax_t;
typedef unsigned long __dev_t;
typedef unsigned int __uid_t;
typedef unsigned int __gid_t;
typedef unsigned long __ino_t;
typedef unsigned long __ino64_t;
typedef unsigned int __mode_t;
typedef unsigned long __nlink_t;
typedef long __off_t;
typedef long __off64_t;
typedef int __pid_t;
typedef struct {
    int __val[2];
} __fsid_t;
typedef long __clock_t;
typedef unsigned long __rlim_t;
typedef unsigned long __rlim64_t;
typedef unsigned int __id_t;
typedef long __time_t;
typedef unsigned int __useconds_t;
typedef long __suseconds_t;
typedef int __daddr_t;
typedef int __key_t;
typedef int __clockid_t;
typedef void *__timer_t;
typedef long __blksize_t;
typedef long __blkcnt_t;
typedef long __blkcnt64_t;
typedef unsigned long __fsblkcnt_t;
typedef unsigned long __fsblkcnt64_t;
typedef unsigned long __fsfilcnt_t;
typedef unsigned long __fsfilcnt64_t;
typedef long __fsword_t;
typedef long __ssize_t;
typedef long __syscall_slong_t;
typedef unsigned long __syscall_ulong_t;
typedef __off64_t __loff_t;
typedef char *__caddr_t;
typedef long __intptr_t;
typedef unsigned int __socklen_t;
typedef int __sig_atomic_t;
typedef __int8_t int8_t;
typedef __int16_t int16_t;
typedef __int32_t int32_t;
typedef __int64_t int64_t;
typedef __uint8_t uint8_t;
typedef __uint16_t uint16_t;
typedef __uint32_t uint32_t;
typedef __uint64_t uint64_t;
typedef __int_least8_t int_least8_t;
typedef __int_least16_t int_least16_t;
typedef __int_least32_t int_least32_t;
typedef __int_least64_t int_least64_t;
typedef __uint_least8_t uint_least8_t;
typedef __uint_least16_t uint_least16_t;
typedef __uint_least32_t uint_least32_t;
typedef __uint_least64_t uint_least64_t;
typedef signed char int_fast8_t;
typedef long int_fast16_t;
typedef long int_fast32_t;
typedef long int_fast64_t;
typedef unsigned char uint_fast8_t;
typedef unsigned long uint_fast16_t;
typedef unsigned long uint_fast32_t;
typedef unsigned long uint_fast64_t;
typedef long intptr_t;
typedef unsigned long uintptr_t;
typedef __intmax_t intmax_t;
typedef __uintmax_t uintmax_t;
typedef _Bool bool;
extern uint64_t revng_init_local_sp(uint64_t param_0);
extern uint64_t init_rbx(void);
extern uint64_t init_rbp(void);
extern uint64_t init_r13(void);
extern uint64_t init_r14(void);
extern uint64_t init_r12(void);
extern uint64_t helper_cc_compute_all_wrapper(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint32_t param_3);
void bb_exchange(uint64_t rdi, uint64_t rsi) {
    uint64_t var_0;
    uint64_t var_1;
    uint64_t var_2;
    uint64_t var_3;
    uint64_t var_4;
    uint64_t var_5;
    uint32_t *var_6;
    uint32_t var_7;
    uint64_t var_8;
    uint32_t *var_9;
    uint32_t var_10;
    uint64_t var_11;
    uint32_t var_12;
    uint64_t var_13;
    uint64_t var_14;
    uint64_t var_15;
    uint64_t r11_0;
    uint64_t storemerge_in;
    uint32_t var_16;
    uint64_t var_17;
    bool var_18;
    uint64_t var_19;
    uint64_t var_20;
    uint64_t var_21;
    uint64_t r11_1;
    uint64_t var_22;
    uint64_t var_23;
    uint64_t var_24;
    uint64_t var_25;
    uint64_t rax_0;
    uint64_t var_26;
    uint64_t *var_27;
    uint64_t var_28;
    uint64_t *var_29;
    uint64_t var_30;
    uint64_t var_31;
    uint64_t var_32;
    uint64_t var_33;
    uint64_t var_34;
    uint64_t var_35;
    uint64_t rax_1;
    uint64_t var_36;
    uint64_t *var_37;
    uint64_t var_38;
    uint64_t *var_39;
    uint64_t var_40;
    unsigned int loop_state_var;
    bool switch_state_var;
    var_0 = revng_init_local_sp(0UL);
    var_1 = init_rbx();
    var_2 = init_rbp();
    var_3 = init_r13();
    var_4 = init_r14();
    var_5 = init_r12();
    var_6 = (uint32_t *)(rsi + 48UL);
    var_7 = *var_6;
    var_8 = (uint64_t)var_7;
    *(uint64_t *)(var_0 + (-8L)) = var_4;
    *(uint64_t *)(var_0 + (-16L)) = var_3;
    *(uint64_t *)(var_0 + (-24L)) = var_5;
    var_9 = (uint32_t *)(rsi + 44UL);
    var_10 = *var_9;
    var_11 = (uint64_t)var_10;
    *(uint64_t *)(var_0 + (-32L)) = var_2;
    var_12 = *(uint32_t *)rsi;
    var_13 = (uint64_t)var_12;
    *(uint64_t *)(var_0 + (-40L)) = var_1;
    var_14 = (var_8 << 3UL) + rdi;
    var_15 = var_8 << 32UL;
    r11_0 = var_11;
    storemerge_in = var_13;
    rax_0 = 0UL;
    rax_1 = 0UL;
    var_16 = (uint32_t)storemerge_in;
    var_17 = (uint64_t)var_16;
    r11_1 = r11_0;
    while ((long)var_15 <= (long)(r11_0 << 32UL))
        {
            var_18 = ((long)(storemerge_in << 32UL) > (long)var_15);
            var_19 = var_17 - var_8;
            var_20 = (uint64_t)(uint32_t)var_19;
            var_21 = var_19 << 32UL;
            while (1U)
                {
                    r11_0 = r11_1;
                    if (!var_18) {
                        loop_state_var = 1U;
                        break;
                    }
                    var_22 = var_8 - r11_1;
                    var_23 = var_22 << 32UL;
                    if ((long)var_21 <= (long)var_23) {
                        loop_state_var = 0U;
                        break;
                    }
                    var_24 = helper_cc_compute_all_wrapper(var_20, 0UL, 0UL, 24U);
                    if ((uint64_t)(((unsigned char)(var_24 >> 4UL) ^ (unsigned char)var_24) & '\xc0') != 0UL) {
                        var_25 = (uint64_t)((long)(r11_1 << 32UL) >> (long)29UL) + rdi;
                        var_26 = rax_0 << 3UL;
                        var_27 = (uint64_t *)(var_26 + var_25);
                        var_28 = *var_27;
                        var_29 = (uint64_t *)(var_26 + var_14);
                        *var_27 = *var_29;
                        *var_29 = var_28;
                        var_30 = rax_0 + 1UL;
                        rax_0 = var_30;
                        do {
                            var_26 = rax_0 << 3UL;
                            var_27 = (uint64_t *)(var_26 + var_25);
                            var_28 = *var_27;
                            var_29 = (uint64_t *)(var_26 + var_14);
                            *var_27 = *var_29;
                            *var_29 = var_28;
                            var_30 = rax_0 + 1UL;
                            rax_0 = var_30;
                        } while ((long)var_21 <= (long)(var_30 << 32UL));
                    }
                    var_31 = r11_1 + var_20;
                    if ((long)var_15 <= (long)(var_31 << 32UL)) {
                        loop_state_var = 1U;
                        break;
                    }
                    r11_1 = (uint64_t)(uint32_t)var_31;
                    continue;
                }
            switch_state_var = 0;
            switch (loop_state_var) {
              case 1U:
                {
                    switch_state_var = 1;
                    break;
                }
                break;
              case 0U:
                {
                    var_32 = (uint64_t)(uint32_t)var_22;
                    var_33 = helper_cc_compute_all_wrapper(var_32, 0UL, 0UL, 24U);
                    if ((uint64_t)(((unsigned char)(var_33 >> 4UL) ^ (unsigned char)var_33) & '\xc0') != 0UL) {
                        var_34 = (uint64_t)((long)(r11_1 << 32UL) >> (long)29UL) + rdi;
                        var_35 = (uint64_t)((long)((uint64_t)(((uint32_t)r11_1 - var_7) + var_16) << 32UL) >> (long)29UL) + rdi;
                        var_36 = rax_1 << 3UL;
                        var_37 = (uint64_t *)(var_36 + var_34);
                        var_38 = *var_37;
                        var_39 = (uint64_t *)(var_36 + var_35);
                        *var_37 = *var_39;
                        *var_39 = var_38;
                        var_40 = rax_1 + 1UL;
                        rax_1 = var_40;
                        do {
                            var_36 = rax_1 << 3UL;
                            var_37 = (uint64_t *)(var_36 + var_34);
                            var_38 = *var_37;
                            var_39 = (uint64_t *)(var_36 + var_35);
                            *var_37 = *var_39;
                            *var_39 = var_38;
                            var_40 = rax_1 + 1UL;
                            rax_1 = var_40;
                        } while ((long)var_23 <= (long)(var_40 << 32UL));
                    }
                    storemerge_in = var_17 - var_32;
                    var_16 = (uint32_t)storemerge_in;
                    var_17 = (uint64_t)var_16;
                    r11_1 = r11_0;
                    continue;
                }
                break;
            }
            if (switch_state_var)
                break;
        }
    *var_6 = var_12;
    *var_9 = ((var_12 - var_7) + var_10);
    return;
}

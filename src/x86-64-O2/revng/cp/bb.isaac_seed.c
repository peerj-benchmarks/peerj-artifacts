typedef unsigned char __u_char;
typedef unsigned short __u_short;
typedef unsigned int __u_int;
typedef unsigned long __u_long;
typedef signed char __int8_t;
typedef unsigned char __uint8_t;
typedef short __int16_t;
typedef unsigned short __uint16_t;
typedef int __int32_t;
typedef unsigned int __uint32_t;
typedef long __int64_t;
typedef unsigned long __uint64_t;
typedef __int8_t __int_least8_t;
typedef __uint8_t __uint_least8_t;
typedef __int16_t __int_least16_t;
typedef __uint16_t __uint_least16_t;
typedef __int32_t __int_least32_t;
typedef __uint32_t __uint_least32_t;
typedef __int64_t __int_least64_t;
typedef __uint64_t __uint_least64_t;
typedef long __quad_t;
typedef unsigned long __u_quad_t;
typedef long __intmax_t;
typedef unsigned long __uintmax_t;
typedef unsigned long __dev_t;
typedef unsigned int __uid_t;
typedef unsigned int __gid_t;
typedef unsigned long __ino_t;
typedef unsigned long __ino64_t;
typedef unsigned int __mode_t;
typedef unsigned long __nlink_t;
typedef long __off_t;
typedef long __off64_t;
typedef int __pid_t;
typedef struct {
    int __val[2];
} __fsid_t;
typedef long __clock_t;
typedef unsigned long __rlim_t;
typedef unsigned long __rlim64_t;
typedef unsigned int __id_t;
typedef long __time_t;
typedef unsigned int __useconds_t;
typedef long __suseconds_t;
typedef int __daddr_t;
typedef int __key_t;
typedef int __clockid_t;
typedef void *__timer_t;
typedef long __blksize_t;
typedef long __blkcnt_t;
typedef long __blkcnt64_t;
typedef unsigned long __fsblkcnt_t;
typedef unsigned long __fsblkcnt64_t;
typedef unsigned long __fsfilcnt_t;
typedef unsigned long __fsfilcnt64_t;
typedef long __fsword_t;
typedef long __ssize_t;
typedef long __syscall_slong_t;
typedef unsigned long __syscall_ulong_t;
typedef __off64_t __loff_t;
typedef char *__caddr_t;
typedef long __intptr_t;
typedef unsigned int __socklen_t;
typedef int __sig_atomic_t;
typedef __int8_t int8_t;
typedef __int16_t int16_t;
typedef __int32_t int32_t;
typedef __int64_t int64_t;
typedef __uint8_t uint8_t;
typedef __uint16_t uint16_t;
typedef __uint32_t uint32_t;
typedef __uint64_t uint64_t;
typedef __int_least8_t int_least8_t;
typedef __int_least16_t int_least16_t;
typedef __int_least32_t int_least32_t;
typedef __int_least64_t int_least64_t;
typedef __uint_least8_t uint_least8_t;
typedef __uint_least16_t uint_least16_t;
typedef __uint_least32_t uint_least32_t;
typedef __uint_least64_t uint_least64_t;
typedef signed char int_fast8_t;
typedef long int_fast16_t;
typedef long int_fast32_t;
typedef long int_fast64_t;
typedef unsigned char uint_fast8_t;
typedef unsigned long uint_fast16_t;
typedef unsigned long uint_fast32_t;
typedef unsigned long uint_fast64_t;
typedef long intptr_t;
typedef unsigned long uintptr_t;
typedef __intmax_t intmax_t;
typedef __uintmax_t uintmax_t;
extern uint64_t revng_init_local_sp(uint64_t param_0);
extern uint64_t init_rbx(void);
extern uint64_t init_rbp(void);
extern uint64_t init_r13(void);
extern uint64_t init_r12(void);
void bb_isaac_seed(uint64_t rdi) {
    uint64_t var_0;
    uint64_t var_1;
    uint64_t var_2;
    uint64_t var_3;
    uint64_t var_4;
    uint64_t var_5;
    uint64_t rcx_0;
    uint64_t r10_0;
    uint64_t r9_0;
    uint64_t rax_0;
    uint64_t rbp_0;
    uint64_t r8_0;
    uint64_t rsi_0;
    uint64_t r12_0;
    uint64_t rdx_0;
    uint64_t *var_6;
    uint64_t var_7;
    uint64_t *var_8;
    uint64_t var_9;
    uint64_t *var_10;
    uint64_t var_11;
    uint64_t *var_12;
    uint64_t var_13;
    uint64_t var_14;
    uint64_t *var_15;
    uint64_t var_16;
    uint64_t var_17;
    uint64_t var_18;
    uint64_t *var_19;
    uint64_t var_20;
    uint64_t var_21;
    uint64_t var_22;
    uint64_t var_23;
    uint64_t *var_24;
    uint64_t var_25;
    uint64_t var_26;
    uint64_t var_27;
    uint64_t var_28;
    uint64_t *var_29;
    uint64_t var_30;
    uint64_t var_31;
    uint64_t var_32;
    uint64_t var_33;
    uint64_t var_34;
    uint64_t var_35;
    uint64_t var_36;
    uint64_t var_37;
    uint64_t var_38;
    uint64_t var_39;
    uint64_t var_40;
    uint64_t var_41;
    uint64_t var_42;
    uint64_t var_43;
    uint64_t var_44;
    uint64_t var_45;
    uint64_t var_46;
    uint64_t var_47;
    uint64_t var_48;
    uint64_t var_49;
    uint64_t var_50;
    uint64_t rcx_1;
    uint64_t r10_1;
    uint64_t r9_1;
    uint64_t rax_1;
    uint64_t rbp_1;
    uint64_t r8_1;
    uint64_t rsi_1;
    uint64_t rdx_1;
    uint64_t r11_0;
    uint64_t *var_51;
    uint64_t var_52;
    uint64_t *var_53;
    uint64_t var_54;
    uint64_t *var_55;
    uint64_t var_56;
    uint64_t *var_57;
    uint64_t var_58;
    uint64_t var_59;
    uint64_t *var_60;
    uint64_t var_61;
    uint64_t var_62;
    uint64_t var_63;
    uint64_t *var_64;
    uint64_t var_65;
    uint64_t var_66;
    uint64_t var_67;
    uint64_t var_68;
    uint64_t *var_69;
    uint64_t var_70;
    uint64_t var_71;
    uint64_t var_72;
    uint64_t var_73;
    uint64_t *var_74;
    uint64_t var_75;
    uint64_t var_76;
    uint64_t var_77;
    uint64_t var_78;
    uint64_t var_79;
    uint64_t var_80;
    uint64_t var_81;
    uint64_t var_82;
    uint64_t var_83;
    uint64_t var_84;
    uint64_t var_85;
    uint64_t var_86;
    uint64_t var_87;
    uint64_t var_88;
    uint64_t var_89;
    uint64_t var_90;
    uint64_t var_91;
    uint64_t var_92;
    uint64_t var_93;
    uint64_t var_94;
    uint64_t var_95;
    var_0 = revng_init_local_sp(0UL);
    var_1 = init_rbx();
    var_2 = init_rbp();
    var_3 = init_r13();
    var_4 = init_r12();
    *(uint64_t *)(var_0 + (-8L)) = var_3;
    *(uint64_t *)(var_0 + (-16L)) = var_4;
    *(uint64_t *)(var_0 + (-24L)) = var_2;
    *(uint64_t *)(var_0 + (-32L)) = var_1;
    var_5 = rdi + 2048UL;
    rcx_0 = 10092185256905347744UL;
    r10_0 = 5259722845879046933UL;
    r9_0 = 12580906657422019053UL;
    rax_0 = 7240739780546808700UL;
    rbp_0 = 12869931497269318948UL;
    r8_0 = 11021839149480329387UL;
    rsi_0 = 9435133421607575758UL;
    r12_0 = rdi;
    rdx_0 = 13400657653193689186UL;
    r11_0 = rdi;
    var_6 = (uint64_t *)(r12_0 + 56UL);
    var_7 = r8_0 + *var_6;
    var_8 = (uint64_t *)(r12_0 + 32UL);
    var_9 = rsi_0 + *var_8;
    var_10 = (uint64_t *)(r12_0 + 40UL);
    var_11 = r10_0 + *var_10;
    var_12 = (uint64_t *)(r12_0 + 48UL);
    var_13 = r9_0 + *var_12;
    var_14 = rax_0 - var_9;
    var_15 = (uint64_t *)r12_0;
    var_16 = var_14 + *var_15;
    var_17 = var_11 ^ (var_7 >> 9UL);
    var_18 = rdx_0 - var_17;
    var_19 = (uint64_t *)(r12_0 + 8UL);
    var_20 = var_18 + *var_19;
    var_21 = var_7 + var_16;
    var_22 = var_13 ^ (var_16 << 9UL);
    var_23 = rcx_0 - var_22;
    var_24 = (uint64_t *)(r12_0 + 16UL);
    var_25 = var_23 + *var_24;
    var_26 = var_16 + var_20;
    var_27 = var_21 ^ (var_20 >> 23UL);
    var_28 = rbp_0 - var_27;
    var_29 = (uint64_t *)(r12_0 + 24UL);
    var_30 = var_28 + *var_29;
    var_31 = var_20 + var_25;
    var_32 = var_26 ^ (var_25 << 15UL);
    var_33 = var_9 - var_32;
    var_34 = var_25 + var_30;
    var_35 = var_30 >> 14UL;
    var_36 = var_30 + var_33;
    *var_15 = var_32;
    var_37 = var_31 ^ var_35;
    var_38 = var_17 - var_37;
    var_39 = var_33 << 20UL;
    *var_19 = var_37;
    var_40 = var_34 ^ var_39;
    var_41 = var_33 + var_38;
    var_42 = var_22 - var_40;
    var_43 = var_38 >> 17UL;
    *var_24 = var_40;
    var_44 = var_36 ^ var_43;
    var_45 = var_38 + var_42;
    var_46 = var_27 - var_44;
    var_47 = var_42 << 14UL;
    *var_29 = var_44;
    var_48 = var_41 ^ var_47;
    var_49 = var_42 + var_46;
    *var_10 = var_45;
    *var_8 = var_48;
    *var_12 = var_49;
    var_50 = r12_0 + 64UL;
    *var_6 = var_46;
    rcx_0 = var_40;
    r10_0 = var_45;
    r9_0 = var_49;
    rax_0 = var_32;
    rbp_0 = var_44;
    r8_0 = var_46;
    rsi_0 = var_48;
    r12_0 = var_50;
    rdx_0 = var_37;
    rcx_1 = var_40;
    r10_1 = var_45;
    r9_1 = var_49;
    rax_1 = var_32;
    rbp_1 = var_44;
    r8_1 = var_46;
    rsi_1 = var_48;
    rdx_1 = var_37;
    do {
        var_6 = (uint64_t *)(r12_0 + 56UL);
        var_7 = r8_0 + *var_6;
        var_8 = (uint64_t *)(r12_0 + 32UL);
        var_9 = rsi_0 + *var_8;
        var_10 = (uint64_t *)(r12_0 + 40UL);
        var_11 = r10_0 + *var_10;
        var_12 = (uint64_t *)(r12_0 + 48UL);
        var_13 = r9_0 + *var_12;
        var_14 = rax_0 - var_9;
        var_15 = (uint64_t *)r12_0;
        var_16 = var_14 + *var_15;
        var_17 = var_11 ^ (var_7 >> 9UL);
        var_18 = rdx_0 - var_17;
        var_19 = (uint64_t *)(r12_0 + 8UL);
        var_20 = var_18 + *var_19;
        var_21 = var_7 + var_16;
        var_22 = var_13 ^ (var_16 << 9UL);
        var_23 = rcx_0 - var_22;
        var_24 = (uint64_t *)(r12_0 + 16UL);
        var_25 = var_23 + *var_24;
        var_26 = var_16 + var_20;
        var_27 = var_21 ^ (var_20 >> 23UL);
        var_28 = rbp_0 - var_27;
        var_29 = (uint64_t *)(r12_0 + 24UL);
        var_30 = var_28 + *var_29;
        var_31 = var_20 + var_25;
        var_32 = var_26 ^ (var_25 << 15UL);
        var_33 = var_9 - var_32;
        var_34 = var_25 + var_30;
        var_35 = var_30 >> 14UL;
        var_36 = var_30 + var_33;
        *var_15 = var_32;
        var_37 = var_31 ^ var_35;
        var_38 = var_17 - var_37;
        var_39 = var_33 << 20UL;
        *var_19 = var_37;
        var_40 = var_34 ^ var_39;
        var_41 = var_33 + var_38;
        var_42 = var_22 - var_40;
        var_43 = var_38 >> 17UL;
        *var_24 = var_40;
        var_44 = var_36 ^ var_43;
        var_45 = var_38 + var_42;
        var_46 = var_27 - var_44;
        var_47 = var_42 << 14UL;
        *var_29 = var_44;
        var_48 = var_41 ^ var_47;
        var_49 = var_42 + var_46;
        *var_10 = var_45;
        *var_8 = var_48;
        *var_12 = var_49;
        var_50 = r12_0 + 64UL;
        *var_6 = var_46;
        rcx_0 = var_40;
        r10_0 = var_45;
        r9_0 = var_49;
        rax_0 = var_32;
        rbp_0 = var_44;
        r8_0 = var_46;
        rsi_0 = var_48;
        r12_0 = var_50;
        rdx_0 = var_37;
        rcx_1 = var_40;
        r10_1 = var_45;
        r9_1 = var_49;
        rax_1 = var_32;
        rbp_1 = var_44;
        r8_1 = var_46;
        rsi_1 = var_48;
        rdx_1 = var_37;
    } while (var_50 != var_5);
    var_51 = (uint64_t *)(r11_0 + 56UL);
    var_52 = r8_1 + *var_51;
    var_53 = (uint64_t *)(r11_0 + 32UL);
    var_54 = rsi_1 + *var_53;
    var_55 = (uint64_t *)(r11_0 + 40UL);
    var_56 = r10_1 + *var_55;
    var_57 = (uint64_t *)(r11_0 + 48UL);
    var_58 = r9_1 + *var_57;
    var_59 = rax_1 - var_54;
    var_60 = (uint64_t *)r11_0;
    var_61 = var_59 + *var_60;
    var_62 = var_56 ^ (var_52 >> 9UL);
    var_63 = rdx_1 - var_62;
    var_64 = (uint64_t *)(r11_0 + 8UL);
    var_65 = var_63 + *var_64;
    var_66 = var_52 + var_61;
    var_67 = var_58 ^ (var_61 << 9UL);
    var_68 = rcx_1 - var_67;
    var_69 = (uint64_t *)(r11_0 + 16UL);
    var_70 = var_68 + *var_69;
    var_71 = var_61 + var_65;
    var_72 = var_66 ^ (var_65 >> 23UL);
    var_73 = rbp_1 - var_72;
    var_74 = (uint64_t *)(r11_0 + 24UL);
    var_75 = var_73 + *var_74;
    var_76 = var_65 + var_70;
    var_77 = var_71 ^ (var_70 << 15UL);
    var_78 = var_54 - var_77;
    var_79 = var_70 + var_75;
    var_80 = var_75 >> 14UL;
    var_81 = var_75 + var_78;
    *var_60 = var_77;
    var_82 = var_76 ^ var_80;
    var_83 = var_62 - var_82;
    var_84 = var_78 << 20UL;
    *var_64 = var_82;
    var_85 = var_79 ^ var_84;
    var_86 = var_78 + var_83;
    var_87 = var_67 - var_85;
    var_88 = var_83 >> 17UL;
    *var_69 = var_85;
    var_89 = var_81 ^ var_88;
    var_90 = var_83 + var_87;
    var_91 = var_72 - var_89;
    var_92 = var_87 << 14UL;
    *var_74 = var_89;
    var_93 = var_86 ^ var_92;
    var_94 = var_87 + var_91;
    *var_55 = var_90;
    *var_53 = var_93;
    *var_57 = var_94;
    var_95 = r11_0 + 64UL;
    *var_51 = var_91;
    rcx_1 = var_85;
    r10_1 = var_90;
    r9_1 = var_94;
    rax_1 = var_77;
    rbp_1 = var_89;
    r8_1 = var_91;
    rsi_1 = var_93;
    rdx_1 = var_82;
    r11_0 = var_95;
    do {
        var_51 = (uint64_t *)(r11_0 + 56UL);
        var_52 = r8_1 + *var_51;
        var_53 = (uint64_t *)(r11_0 + 32UL);
        var_54 = rsi_1 + *var_53;
        var_55 = (uint64_t *)(r11_0 + 40UL);
        var_56 = r10_1 + *var_55;
        var_57 = (uint64_t *)(r11_0 + 48UL);
        var_58 = r9_1 + *var_57;
        var_59 = rax_1 - var_54;
        var_60 = (uint64_t *)r11_0;
        var_61 = var_59 + *var_60;
        var_62 = var_56 ^ (var_52 >> 9UL);
        var_63 = rdx_1 - var_62;
        var_64 = (uint64_t *)(r11_0 + 8UL);
        var_65 = var_63 + *var_64;
        var_66 = var_52 + var_61;
        var_67 = var_58 ^ (var_61 << 9UL);
        var_68 = rcx_1 - var_67;
        var_69 = (uint64_t *)(r11_0 + 16UL);
        var_70 = var_68 + *var_69;
        var_71 = var_61 + var_65;
        var_72 = var_66 ^ (var_65 >> 23UL);
        var_73 = rbp_1 - var_72;
        var_74 = (uint64_t *)(r11_0 + 24UL);
        var_75 = var_73 + *var_74;
        var_76 = var_65 + var_70;
        var_77 = var_71 ^ (var_70 << 15UL);
        var_78 = var_54 - var_77;
        var_79 = var_70 + var_75;
        var_80 = var_75 >> 14UL;
        var_81 = var_75 + var_78;
        *var_60 = var_77;
        var_82 = var_76 ^ var_80;
        var_83 = var_62 - var_82;
        var_84 = var_78 << 20UL;
        *var_64 = var_82;
        var_85 = var_79 ^ var_84;
        var_86 = var_78 + var_83;
        var_87 = var_67 - var_85;
        var_88 = var_83 >> 17UL;
        *var_69 = var_85;
        var_89 = var_81 ^ var_88;
        var_90 = var_83 + var_87;
        var_91 = var_72 - var_89;
        var_92 = var_87 << 14UL;
        *var_74 = var_89;
        var_93 = var_86 ^ var_92;
        var_94 = var_87 + var_91;
        *var_55 = var_90;
        *var_53 = var_93;
        *var_57 = var_94;
        var_95 = r11_0 + 64UL;
        *var_51 = var_91;
        rcx_1 = var_85;
        r10_1 = var_90;
        r9_1 = var_94;
        rax_1 = var_77;
        rbp_1 = var_89;
        r8_1 = var_91;
        rsi_1 = var_93;
        rdx_1 = var_82;
        r11_0 = var_95;
    } while (var_5 != var_95);
    *(uint64_t *)(rdi + 2064UL) = 0UL;
    *(uint64_t *)(rdi + 2056UL) = 0UL;
    *(uint64_t *)var_5 = 0UL;
    return;
}

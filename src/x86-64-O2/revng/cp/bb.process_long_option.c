typedef unsigned char __u_char;
typedef unsigned short __u_short;
typedef unsigned int __u_int;
typedef unsigned long __u_long;
typedef signed char __int8_t;
typedef unsigned char __uint8_t;
typedef short __int16_t;
typedef unsigned short __uint16_t;
typedef int __int32_t;
typedef unsigned int __uint32_t;
typedef long __int64_t;
typedef unsigned long __uint64_t;
typedef __int8_t __int_least8_t;
typedef __uint8_t __uint_least8_t;
typedef __int16_t __int_least16_t;
typedef __uint16_t __uint_least16_t;
typedef __int32_t __int_least32_t;
typedef __uint32_t __uint_least32_t;
typedef __int64_t __int_least64_t;
typedef __uint64_t __uint_least64_t;
typedef long __quad_t;
typedef unsigned long __u_quad_t;
typedef long __intmax_t;
typedef unsigned long __uintmax_t;
typedef unsigned long __dev_t;
typedef unsigned int __uid_t;
typedef unsigned int __gid_t;
typedef unsigned long __ino_t;
typedef unsigned long __ino64_t;
typedef unsigned int __mode_t;
typedef unsigned long __nlink_t;
typedef long __off_t;
typedef long __off64_t;
typedef int __pid_t;
typedef struct {
    int __val[2];
} __fsid_t;
typedef long __clock_t;
typedef unsigned long __rlim_t;
typedef unsigned long __rlim64_t;
typedef unsigned int __id_t;
typedef long __time_t;
typedef unsigned int __useconds_t;
typedef long __suseconds_t;
typedef int __daddr_t;
typedef int __key_t;
typedef int __clockid_t;
typedef void *__timer_t;
typedef long __blksize_t;
typedef long __blkcnt_t;
typedef long __blkcnt64_t;
typedef unsigned long __fsblkcnt_t;
typedef unsigned long __fsblkcnt64_t;
typedef unsigned long __fsfilcnt_t;
typedef unsigned long __fsfilcnt64_t;
typedef long __fsword_t;
typedef long __ssize_t;
typedef long __syscall_slong_t;
typedef unsigned long __syscall_ulong_t;
typedef __off64_t __loff_t;
typedef char *__caddr_t;
typedef long __intptr_t;
typedef unsigned int __socklen_t;
typedef int __sig_atomic_t;
typedef __int8_t int8_t;
typedef __int16_t int16_t;
typedef __int32_t int32_t;
typedef __int64_t int64_t;
typedef __uint8_t uint8_t;
typedef __uint16_t uint16_t;
typedef __uint32_t uint32_t;
typedef __uint64_t uint64_t;
typedef __int_least8_t int_least8_t;
typedef __int_least16_t int_least16_t;
typedef __int_least32_t int_least32_t;
typedef __int_least64_t int_least64_t;
typedef __uint_least8_t uint_least8_t;
typedef __uint_least16_t uint_least16_t;
typedef __uint_least32_t uint_least32_t;
typedef __uint_least64_t uint_least64_t;
typedef signed char int_fast8_t;
typedef long int_fast16_t;
typedef long int_fast32_t;
typedef long int_fast64_t;
typedef unsigned char uint_fast8_t;
typedef unsigned long uint_fast16_t;
typedef unsigned long uint_fast32_t;
typedef unsigned long uint_fast64_t;
typedef long intptr_t;
typedef unsigned long uintptr_t;
typedef __intmax_t intmax_t;
typedef __uintmax_t uintmax_t;
typedef _Bool bool;
struct indirect_placeholder_290_ret_type;
struct indirect_placeholder_290_ret_type {
    uint64_t field_0;
    uint64_t field_1;
};
extern uint64_t revng_init_local_sp(uint64_t param_0);
extern uint64_t init_rbx(void);
extern uint64_t init_rbp(void);
extern void indirect_placeholder(void);
extern uint64_t init_r13(void);
extern uint64_t init_r14(void);
extern uint64_t init_r12(void);
extern uint64_t init_r15(void);
extern struct indirect_placeholder_290_ret_type indirect_placeholder_290(uint64_t param_0);
uint64_t bb_process_long_option(uint64_t rcx, uint64_t r9, uint64_t rdi, uint64_t r8, uint64_t rsi, uint64_t rdx) {
    uint64_t var_72;
    uint64_t var_73;
    uint64_t var_0;
    uint64_t var_1;
    uint64_t var_2;
    uint64_t var_3;
    uint64_t var_4;
    uint64_t var_5;
    uint64_t var_6;
    uint64_t var_7;
    uint64_t var_8;
    uint64_t *var_9;
    uint64_t var_10;
    unsigned char var_11;
    uint64_t rax_5;
    uint64_t rbp_5;
    uint64_t r14_1;
    uint64_t var_36;
    uint64_t var_37;
    uint64_t *var_38;
    uint64_t var_39;
    uint64_t _pre;
    uint64_t var_40;
    uint64_t local_sp_1;
    uint64_t local_sp_0;
    uint64_t r12_0;
    uint64_t var_52;
    uint64_t var_53;
    uint64_t var_54;
    uint64_t local_sp_5;
    uint64_t r15_0;
    uint64_t var_44;
    uint64_t r12_1;
    uint64_t rbp_4_ph;
    uint32_t var_25;
    uint64_t var_26;
    uint32_t var_27;
    uint64_t var_28;
    uint64_t local_sp_3;
    uint64_t rax_0;
    uint64_t r14_1_ph;
    bool var_29;
    uint64_t var_30;
    bool var_31;
    uint64_t local_sp_2;
    uint64_t var_32;
    uint64_t var_33;
    struct indirect_placeholder_290_ret_type var_34;
    uint64_t var_35;
    uint64_t rbp_0;
    uint64_t r15_2;
    uint64_t var_42;
    uint64_t var_43;
    uint64_t local_sp_4;
    uint64_t local_sp_10;
    uint64_t rax_1;
    uint64_t rbp_1;
    uint64_t var_41;
    uint64_t rbp_2;
    uint64_t r12_1_ph;
    uint64_t r15_2_ph;
    bool var_23;
    uint64_t var_45;
    uint64_t var_46;
    bool var_47;
    uint64_t local_sp_12;
    uint64_t var_48;
    uint64_t *var_49;
    uint64_t var_50;
    uint64_t var_51;
    uint64_t rax_2;
    uint64_t local_sp_6;
    uint64_t var_55;
    uint32_t *var_56;
    uint64_t local_sp_10_ph;
    uint64_t rbx_1;
    uint64_t local_sp_7;
    uint64_t r15_3;
    uint64_t rbx_0;
    uint64_t r12_2;
    uint64_t rbp_3;
    uint64_t r15_1;
    uint32_t *var_57;
    uint32_t var_58;
    uint64_t var_59;
    uint64_t var_60;
    bool var_61;
    uint32_t var_62;
    uint64_t var_63;
    uint64_t var_64;
    uint64_t local_sp_8;
    uint64_t var_67;
    uint64_t var_66;
    uint64_t var_65;
    uint64_t merge;
    uint64_t var_68;
    uint64_t var_69;
    bool var_70;
    uint32_t var_71;
    uint64_t var_18;
    uint64_t local_sp_11;
    uint64_t local_sp_9;
    uint64_t var_19;
    uint64_t var_20;
    uint64_t var_21;
    uint64_t var_22;
    uint64_t rax_4_ph;
    uint64_t var_24;
    uint64_t var_12;
    unsigned char var_13;
    uint64_t rbp_6;
    uint64_t *var_14;
    bool var_15;
    bool var_16;
    uint64_t var_17;
    uint64_t local_sp_13;
    uint32_t *var_74;
    unsigned int loop_state_var;
    bool switch_state_var;
    var_0 = revng_init_local_sp(0UL);
    var_1 = init_rbx();
    var_2 = init_rbp();
    var_3 = init_r13();
    var_4 = init_r14();
    var_5 = init_r12();
    var_6 = init_r15();
    *(uint64_t *)(var_0 + (-8L)) = var_6;
    *(uint64_t *)(var_0 + (-16L)) = var_4;
    *(uint64_t *)(var_0 + (-24L)) = var_3;
    *(uint64_t *)(var_0 + (-32L)) = var_5;
    *(uint64_t *)(var_0 + (-40L)) = var_2;
    *(uint64_t *)(var_0 + (-48L)) = var_1;
    var_7 = var_0 + (-136L);
    var_8 = *(uint64_t *)(var_0 | 8UL);
    *(uint64_t *)(var_0 + (-120L)) = rsi;
    *(uint32_t *)(var_0 + (-96L)) = (uint32_t)rdi;
    *(uint64_t *)(var_0 + (-112L)) = rdx;
    *(uint64_t *)(var_0 + (-128L)) = r8;
    *(uint32_t *)(var_0 + (-132L)) = (uint32_t)r9;
    var_9 = (uint64_t *)(var_8 + 32UL);
    var_10 = *var_9;
    var_11 = *(unsigned char *)var_10;
    rax_5 = (uint64_t)var_11;
    rbp_5 = var_10;
    r12_0 = 0UL;
    rbp_4_ph = 0UL;
    r14_1_ph = rcx;
    r12_1_ph = 0UL;
    r15_2_ph = 0UL;
    local_sp_12 = var_7;
    rbx_1 = 0UL;
    r15_3 = rcx;
    r12_2 = 0UL;
    merge = 4294967295UL;
    local_sp_11 = var_7;
    rbp_6 = var_10;
    if (!(((uint64_t)(var_11 + '\xc3') == 0UL) || (var_11 == '\x00'))) {
        while (1U)
            {
                switch_state_var = 1;
                break;
            }
        rax_5 = (uint64_t)var_13;
        r12_2 = var_12 - var_10;
    }
    var_14 = (uint64_t *)rcx;
    rbp_3 = rbp_6;
    if (*var_14 != 0UL) {
        local_sp_13 = local_sp_12;
        if (*(uint32_t *)(local_sp_12 + 4UL) != 0U) {
            var_72 = *var_9;
            var_73 = local_sp_12 + (-8L);
            *(uint64_t *)var_73 = 4261747UL;
            indirect_placeholder();
            local_sp_13 = var_73;
            if (*(unsigned char *)(*(uint64_t *)(((uint64_t)*(uint32_t *)var_8 << 3UL) + *(uint64_t *)(local_sp_12 + 16UL)) + 1UL) != '-' & var_72 == 0UL) {
                return merge;
            }
        }
        if (*(uint32_t *)(local_sp_13 + 152UL) != 0U) {
            *(uint64_t *)(local_sp_13 + (-8L)) = 4261369UL;
            indirect_placeholder();
        }
        *var_9 = 0UL;
        var_74 = (uint32_t *)var_8;
        *var_74 = (*var_74 + 1U);
        *(uint32_t *)(var_8 + 8UL) = 0U;
        return 63UL;
    }
    var_15 = (rax_5 == 0UL);
    var_16 = (r12_2 == rax_5);
    merge = 63UL;
    while (1U)
        {
            var_17 = local_sp_11 + (-8L);
            *(uint64_t *)var_17 = 4260635UL;
            indirect_placeholder();
            rbx_0 = rbx_1;
            r15_1 = r15_3;
            local_sp_9 = var_17;
            if (!var_15) {
                var_18 = local_sp_11 + (-16L);
                *(uint64_t *)var_18 = 4260647UL;
                indirect_placeholder();
                local_sp_7 = var_18;
                local_sp_9 = var_18;
                if (!var_16) {
                    loop_state_var = 2U;
                    break;
                }
            }
            var_19 = r15_3 + 32UL;
            var_20 = *(uint64_t *)var_19;
            var_21 = rbx_1 + 1UL;
            local_sp_10_ph = local_sp_9;
            local_sp_11 = local_sp_9;
            r15_3 = var_19;
            local_sp_12 = local_sp_9;
            if (var_20 == 0UL) {
                rbx_1 = (uint64_t)(uint32_t)var_21;
                continue;
            }
            if (*var_14 != 0UL) {
                loop_state_var = 1U;
                break;
            }
            var_22 = (uint64_t)((long)(var_21 << 32UL) >> (long)32UL);
            *(uint64_t *)(local_sp_9 + 48UL) = rbp_6;
            *(uint32_t *)(local_sp_9 + 64UL) = (uint32_t)var_21;
            *(uint32_t *)(local_sp_9 + 44UL) = 4294967295U;
            *(uint32_t *)(local_sp_9 + 68UL) = 0U;
            *(uint64_t *)(local_sp_9 + 32UL) = 0UL;
            *(uint64_t *)(local_sp_9 + 72UL) = var_22;
            *(uint64_t *)(local_sp_9 + 56UL) = rcx;
            rax_4_ph = var_22;
            while (1U)
                {
                    var_23 = ((uint64_t)(uint32_t)rax_4_ph == 0UL);
                    r14_1 = r14_1_ph;
                    r12_1 = r12_1_ph;
                    rax_0 = rax_4_ph;
                    rbp_0 = rbp_4_ph;
                    r15_2 = r15_2_ph;
                    local_sp_10 = local_sp_10_ph;
                    rax_1 = rax_4_ph;
                    rbp_1 = rbp_4_ph;
                    rbp_2 = rbp_4_ph;
                    while (1U)
                        {
                            var_24 = local_sp_10 + (-8L);
                            *(uint64_t *)var_24 = 4260836UL;
                            indirect_placeholder();
                            r15_0 = r14_1;
                            local_sp_3 = var_24;
                            local_sp_2 = var_24;
                            r15_2 = r14_1;
                            local_sp_4 = var_24;
                            local_sp_10 = var_24;
                            r15_2_ph = r15_2;
                            if (!var_23) {
                                loop_state_var = 2U;
                                break;
                            }
                            if (r15_2 != 0UL) {
                                loop_state_var = 0U;
                                break;
                            }
                            var_42 = r14_1 + 32UL;
                            var_43 = *(uint64_t *)var_42;
                            *(uint32_t *)(local_sp_10 + 36UL) = (uint32_t)r12_1;
                            r14_1 = var_42;
                            if (var_43 != 0UL) {
                                loop_state_var = 1U;
                                break;
                            }
                            r12_1 = r12_1 + 1UL;
                            continue;
                        }
                    switch_state_var = 0;
                    switch (loop_state_var) {
                      case 1U:
                        {
                            switch_state_var = 1;
                            break;
                        }
                        break;
                      case 2U:
                      case 0U:
                        {
                            switch_state_var = 0;
                            switch (loop_state_var) {
                              case 2U:
                                {
                                    var_41 = r14_1 + 32UL;
                                    r15_0 = r15_2;
                                    rbp_4_ph = rbp_1;
                                    r14_1_ph = var_41;
                                    local_sp_4 = local_sp_3;
                                    rbp_2 = rbp_1;
                                    local_sp_10_ph = local_sp_3;
                                    rax_4_ph = rax_1;
                                    if (*(uint64_t *)var_41 == 0UL) {
                                        switch_state_var = 1;
                                        break;
                                    }
                                    r12_1_ph = r12_1 + 1UL;
                                    continue;
                                }
                                break;
                              case 0U:
                                {
                                    if (*(uint32_t *)(local_sp_10 + (-4L)) != 0U) {
                                        var_25 = *(uint32_t *)(r14_1 + 8UL);
                                        rax_0 = (uint64_t)var_25;
                                        var_26 = *(uint64_t *)(r14_1 + 16UL);
                                        rax_0 = var_26;
                                        var_27 = *(uint32_t *)(r14_1 + 24UL);
                                        var_28 = (uint64_t)var_27;
                                        rax_0 = var_28;
                                        rax_1 = var_28;
                                        if ((uint64_t)(*(uint32_t *)(r15_2 + 8UL) - var_25) != 0UL & *(uint64_t *)(r15_2 + 16UL) != var_26 & (uint64_t)(*(uint32_t *)(r15_2 + 24UL) - var_27) != 0UL) {
                                            var_41 = r14_1 + 32UL;
                                            r15_0 = r15_2;
                                            rbp_4_ph = rbp_1;
                                            r14_1_ph = var_41;
                                            local_sp_4 = local_sp_3;
                                            rbp_2 = rbp_1;
                                            local_sp_10_ph = local_sp_3;
                                            rax_4_ph = rax_1;
                                            if (*(uint64_t *)var_41 == 0UL) {
                                                switch_state_var = 1;
                                                break;
                                            }
                                            r12_1_ph = r12_1 + 1UL;
                                            continue;
                                        }
                                    }
                                    rax_1 = rax_0;
                                    if ((uint64_t)(uint32_t)rbp_4_ph != 0UL) {
                                        var_29 = (*(uint32_t *)(local_sp_10 + 144UL) == 0U);
                                        var_30 = *(uint64_t *)(local_sp_10 + 24UL);
                                        var_31 = (var_30 == 0UL);
                                        var_40 = var_30;
                                        rax_1 = 0UL;
                                        rbp_1 = 1UL;
                                        if (var_29) {
                                            rbp_0 = 1UL;
                                            rax_1 = rax_0;
                                            if (var_31) {
                                                *(unsigned char *)(r12_1 + var_40) = (unsigned char)'\x01';
                                                local_sp_3 = local_sp_2;
                                                rax_1 = var_40;
                                                rbp_1 = rbp_0;
                                            }
                                        } else {
                                            if (var_31) {
                                                *(unsigned char *)(r12_1 + var_40) = (unsigned char)'\x01';
                                                local_sp_3 = local_sp_2;
                                                rax_1 = var_40;
                                                rbp_1 = rbp_0;
                                            } else {
                                                var_32 = *(uint64_t *)(local_sp_10 + 64UL);
                                                var_33 = local_sp_10 + (-16L);
                                                *(uint64_t *)var_33 = 4261814UL;
                                                var_34 = indirect_placeholder_290(var_32);
                                                var_35 = var_34.field_0;
                                                *(uint64_t *)(local_sp_10 + 16UL) = var_35;
                                                local_sp_3 = var_33;
                                                if (var_35 != 0UL) {
                                                    var_36 = local_sp_10 + (-24L);
                                                    *(uint64_t *)var_36 = 4261841UL;
                                                    indirect_placeholder();
                                                    var_37 = (uint64_t)*(uint32_t *)(local_sp_10 + 20UL);
                                                    var_38 = (uint64_t *)(local_sp_10 + 8UL);
                                                    var_39 = *var_38;
                                                    *(uint32_t *)(local_sp_10 + 44UL) = 1U;
                                                    *(unsigned char *)(var_39 + var_37) = (unsigned char)'\x01';
                                                    _pre = *var_38;
                                                    var_40 = _pre;
                                                    local_sp_2 = var_36;
                                                    *(unsigned char *)(r12_1 + var_40) = (unsigned char)'\x01';
                                                    local_sp_3 = local_sp_2;
                                                    rax_1 = var_40;
                                                    rbp_1 = rbp_0;
                                                }
                                            }
                                        }
                                    }
                                }
                                break;
                            }
                            if (switch_state_var)
                                break;
                        }
                        break;
                    }
                    if (switch_state_var)
                        break;
                }
            var_44 = (uint64_t)(uint32_t)rbp_2;
            var_45 = (uint64_t)*(uint32_t *)(local_sp_4 + 64UL);
            var_46 = *(uint64_t *)(local_sp_4 + 48UL);
            var_47 = ((uint64_t)(unsigned char)rbp_2 == 0UL);
            local_sp_5 = local_sp_4;
            local_sp_12 = local_sp_4;
            rax_2 = var_44;
            local_sp_7 = local_sp_4;
            rbp_3 = var_46;
            r15_1 = r15_0;
            if (!var_47) {
                loop_state_var = 0U;
                break;
            }
            if (*(uint64_t *)(local_sp_4 + 32UL) != 0UL) {
                loop_state_var = 0U;
                break;
            }
            rbx_0 = (uint64_t)*(uint32_t *)(local_sp_4 + 44UL);
            if (r15_0 != 0UL) {
                loop_state_var = 2U;
                break;
            }
            loop_state_var = 1U;
            break;
        }
    switch (loop_state_var) {
      case 0U:
        {
            if (*(uint32_t *)(local_sp_4 + 152UL) != 0U) {
                var_48 = local_sp_4 + (-8L);
                var_49 = (uint64_t *)var_48;
                local_sp_5 = var_48;
                rax_2 = 0UL;
                if (var_47) {
                    *var_49 = 4261532UL;
                    indirect_placeholder();
                } else {
                    *var_49 = 4261003UL;
                    indirect_placeholder();
                    var_50 = local_sp_4 + (-16L);
                    *(uint64_t *)var_50 = 4261038UL;
                    indirect_placeholder();
                    var_51 = var_45 << 32UL;
                    local_sp_0 = var_50;
                    var_53 = r12_0 + 1UL;
                    local_sp_0 = local_sp_1;
                    r12_0 = var_53;
                    do {
                        local_sp_1 = local_sp_0;
                        if (*(unsigned char *)(r12_0 + *(uint64_t *)(local_sp_4 + 16UL)) == '\x00') {
                            var_52 = local_sp_0 + (-8L);
                            *(uint64_t *)var_52 = 4261098UL;
                            indirect_placeholder();
                            local_sp_1 = var_52;
                        }
                        var_53 = r12_0 + 1UL;
                        local_sp_0 = local_sp_1;
                        r12_0 = var_53;
                    } while ((long)var_51 <= (long)(var_53 << 32UL));
                    *(uint64_t *)(local_sp_1 + (-8L)) = 4261791UL;
                    indirect_placeholder();
                    var_54 = local_sp_1 + (-16L);
                    *(uint64_t *)var_54 = 4261799UL;
                    indirect_placeholder();
                    local_sp_5 = var_54;
                }
            }
            local_sp_6 = local_sp_5;
            if (*(uint32_t *)(local_sp_5 + 68UL) != 0U) {
                var_55 = local_sp_5 + (-8L);
                *(uint64_t *)var_55 = 4261552UL;
                indirect_placeholder();
                local_sp_6 = var_55;
            }
            *(uint64_t *)(local_sp_6 + (-8L)) = 4261561UL;
            indirect_placeholder();
            var_56 = (uint32_t *)var_8;
            *var_56 = (*var_56 + 1U);
            *var_9 = (*var_9 + rax_2);
            *(uint32_t *)(var_8 + 8UL) = 0U;
            return 63UL;
        }
        break;
      case 1U:
        {
            local_sp_13 = local_sp_12;
            if (*(uint32_t *)(local_sp_12 + 4UL) != 0U) {
                var_72 = *var_9;
                var_73 = local_sp_12 + (-8L);
                *(uint64_t *)var_73 = 4261747UL;
                indirect_placeholder();
                local_sp_13 = var_73;
                if (*(unsigned char *)(*(uint64_t *)(((uint64_t)*(uint32_t *)var_8 << 3UL) + *(uint64_t *)(local_sp_12 + 16UL)) + 1UL) != '-' & var_72 == 0UL) {
                    return merge;
                }
            }
            if (*(uint32_t *)(local_sp_13 + 152UL) != 0U) {
                *(uint64_t *)(local_sp_13 + (-8L)) = 4261369UL;
                indirect_placeholder();
            }
            *var_9 = 0UL;
            var_74 = (uint32_t *)var_8;
            *var_74 = (*var_74 + 1U);
            *(uint32_t *)(var_8 + 8UL) = 0U;
            return 63UL;
        }
        break;
      case 2U:
        {
            var_57 = (uint32_t *)var_8;
            var_58 = *var_57;
            var_59 = (uint64_t)var_58;
            *var_9 = 0UL;
            var_60 = var_59 + 1UL;
            *var_57 = (uint32_t)var_60;
            var_61 = (*(unsigned char *)rbp_3 == '\x00');
            var_62 = *(uint32_t *)(r15_1 + 8UL);
            local_sp_8 = local_sp_7;
            if (var_61) {
                if (var_62 != 1U) {
                    var_63 = (uint64_t)*(uint32_t *)(local_sp_7 + 40UL);
                    var_64 = var_60 << 32UL;
                    if ((long)var_64 >= (long)(var_63 << 32UL)) {
                        if (*(uint32_t *)(local_sp_7 + 152UL) == 0U) {
                            var_65 = local_sp_7 + (-8L);
                            *(uint64_t *)var_65 = 4261705UL;
                            indirect_placeholder();
                            local_sp_8 = var_65;
                        }
                        *(uint32_t *)(var_8 + 8UL) = *(uint32_t *)(r15_1 + 24UL);
                        var_66 = (**(unsigned char **)(local_sp_8 + 24UL) == ':') ? 58UL : 63UL;
                        merge = var_66;
                        return merge;
                    }
                    var_67 = *(uint64_t *)(local_sp_7 + 16UL);
                    *var_57 = (var_58 + 2U);
                    *(uint64_t *)(var_8 + 16UL) = *(uint64_t *)((uint64_t)((long)var_64 >> (long)29UL) + var_67);
                }
            } else {
                if (var_62 != 0U) {
                    if (*(uint32_t *)(local_sp_7 + 152UL) == 0U) {
                        *(uint32_t *)(var_8 + 8UL) = *(uint32_t *)(r15_1 + 24UL);
                    } else {
                        *(uint64_t *)(local_sp_7 + (-8L)) = 4261276UL;
                        indirect_placeholder();
                        *(uint32_t *)(var_8 + 8UL) = *(uint32_t *)(r15_1 + 24UL);
                    }
                    return merge;
                }
                *(uint64_t *)(var_8 + 16UL) = (rbp_3 + 1UL);
            }
            var_68 = *(uint64_t *)(local_sp_7 + 8UL);
            merge = 0UL;
            if (var_68 == 0UL) {
                *(uint32_t *)var_68 = (uint32_t)rbx_0;
            }
            var_69 = *(uint64_t *)(r15_1 + 16UL);
            var_70 = (var_69 == 0UL);
            var_71 = *(uint32_t *)(r15_1 + 24UL);
            if (var_70) {
                return (uint64_t)var_71;
            }
            *(uint32_t *)var_69 = var_71;
            return merge;
        }
        break;
    }
}

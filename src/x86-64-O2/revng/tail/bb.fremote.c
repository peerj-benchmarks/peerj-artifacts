typedef unsigned char __u_char;
typedef unsigned short __u_short;
typedef unsigned int __u_int;
typedef unsigned long __u_long;
typedef signed char __int8_t;
typedef unsigned char __uint8_t;
typedef short __int16_t;
typedef unsigned short __uint16_t;
typedef int __int32_t;
typedef unsigned int __uint32_t;
typedef long __int64_t;
typedef unsigned long __uint64_t;
typedef __int8_t __int_least8_t;
typedef __uint8_t __uint_least8_t;
typedef __int16_t __int_least16_t;
typedef __uint16_t __uint_least16_t;
typedef __int32_t __int_least32_t;
typedef __uint32_t __uint_least32_t;
typedef __int64_t __int_least64_t;
typedef __uint64_t __uint_least64_t;
typedef long __quad_t;
typedef unsigned long __u_quad_t;
typedef long __intmax_t;
typedef unsigned long __uintmax_t;
typedef unsigned long __dev_t;
typedef unsigned int __uid_t;
typedef unsigned int __gid_t;
typedef unsigned long __ino_t;
typedef unsigned long __ino64_t;
typedef unsigned int __mode_t;
typedef unsigned long __nlink_t;
typedef long __off_t;
typedef long __off64_t;
typedef int __pid_t;
typedef struct {
    int __val[2];
} __fsid_t;
typedef long __clock_t;
typedef unsigned long __rlim_t;
typedef unsigned long __rlim64_t;
typedef unsigned int __id_t;
typedef long __time_t;
typedef unsigned int __useconds_t;
typedef long __suseconds_t;
typedef int __daddr_t;
typedef int __key_t;
typedef int __clockid_t;
typedef void *__timer_t;
typedef long __blksize_t;
typedef long __blkcnt_t;
typedef long __blkcnt64_t;
typedef unsigned long __fsblkcnt_t;
typedef unsigned long __fsblkcnt64_t;
typedef unsigned long __fsfilcnt_t;
typedef unsigned long __fsfilcnt64_t;
typedef long __fsword_t;
typedef long __ssize_t;
typedef long __syscall_slong_t;
typedef unsigned long __syscall_ulong_t;
typedef __off64_t __loff_t;
typedef char *__caddr_t;
typedef long __intptr_t;
typedef unsigned int __socklen_t;
typedef int __sig_atomic_t;
typedef __int8_t int8_t;
typedef __int16_t int16_t;
typedef __int32_t int32_t;
typedef __int64_t int64_t;
typedef __uint8_t uint8_t;
typedef __uint16_t uint16_t;
typedef __uint32_t uint32_t;
typedef __uint64_t uint64_t;
typedef __int_least8_t int_least8_t;
typedef __int_least16_t int_least16_t;
typedef __int_least32_t int_least32_t;
typedef __int_least64_t int_least64_t;
typedef __uint_least8_t uint_least8_t;
typedef __uint_least16_t uint_least16_t;
typedef __uint_least32_t uint_least32_t;
typedef __uint_least64_t uint_least64_t;
typedef signed char int_fast8_t;
typedef long int_fast16_t;
typedef long int_fast32_t;
typedef long int_fast64_t;
typedef unsigned char uint_fast8_t;
typedef unsigned long uint_fast16_t;
typedef unsigned long uint_fast32_t;
typedef unsigned long uint_fast64_t;
typedef long intptr_t;
typedef unsigned long uintptr_t;
typedef __intmax_t intmax_t;
typedef __uintmax_t uintmax_t;
struct indirect_placeholder_51_ret_type;
struct indirect_placeholder_50_ret_type;
struct indirect_placeholder_51_ret_type {
    uint64_t field_0;
    uint64_t field_1;
    uint64_t field_2;
};
struct indirect_placeholder_50_ret_type {
    uint64_t field_0;
    uint64_t field_1;
    uint64_t field_2;
    uint64_t field_3;
    uint64_t field_4;
};
extern uint64_t revng_init_local_sp(uint64_t param_0);
extern uint64_t init_rbx(void);
extern uint64_t init_rbp(void);
extern void indirect_placeholder(void);
extern uint64_t init_cc_src2(void);
extern uint64_t helper_cc_compute_c_wrapper(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint32_t param_3);
extern uint64_t init_rax(void);
extern uint64_t helper_cc_compute_all_wrapper(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint32_t param_3);
extern struct indirect_placeholder_51_ret_type indirect_placeholder_51(uint64_t param_0, uint64_t param_1);
extern struct indirect_placeholder_50_ret_type indirect_placeholder_50(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint64_t param_3, uint64_t param_4, uint64_t param_5, uint64_t param_6);
uint64_t bb_fremote(uint64_t rdi, uint64_t rsi) {
    uint64_t var_17;
    uint64_t var_18;
    uint64_t var_0;
    uint64_t var_1;
    uint64_t var_2;
    uint64_t var_3;
    uint64_t var_4;
    uint64_t *var_5;
    struct indirect_placeholder_51_ret_type var_6;
    uint64_t var_7;
    uint64_t var_8;
    uint64_t var_9;
    uint64_t var_10;
    uint64_t var_11;
    uint64_t var_12;
    uint64_t rbx_0;
    uint64_t var_25;
    uint64_t var_13;
    uint64_t var_42;
    uint64_t var_43;
    uint64_t var_58;
    uint64_t var_59;
    uint64_t var_60;
    uint64_t var_61;
    uint64_t var_64;
    uint64_t var_65;
    uint64_t var_62;
    uint64_t var_63;
    uint64_t var_66;
    uint64_t var_67;
    uint64_t var_44;
    uint64_t var_45;
    uint64_t var_46;
    uint64_t var_47;
    uint64_t var_50;
    uint64_t var_51;
    uint64_t var_48;
    uint64_t var_49;
    uint64_t var_52;
    uint64_t var_53;
    uint64_t var_54;
    uint64_t var_55;
    uint64_t var_56;
    uint64_t var_57;
    uint64_t var_14;
    uint64_t var_15;
    uint64_t var_16;
    uint64_t var_19;
    uint64_t var_22;
    uint64_t var_23;
    uint64_t var_20;
    uint64_t var_21;
    uint64_t var_24;
    uint64_t var_28;
    uint64_t var_29;
    uint64_t var_26;
    uint64_t var_27;
    uint64_t var_30;
    uint64_t var_31;
    uint64_t var_32;
    uint64_t var_33;
    uint64_t var_35;
    uint64_t var_36;
    uint64_t var_34;
    uint64_t var_37;
    uint64_t var_38;
    uint64_t var_39;
    uint64_t var_40;
    uint64_t var_41;
    uint64_t merge;
    var_0 = revng_init_local_sp(0UL);
    var_1 = init_rax();
    var_2 = init_rbx();
    var_3 = init_rbp();
    var_4 = init_cc_src2();
    *(uint64_t *)(var_0 + (-8L)) = var_3;
    *(uint64_t *)(var_0 + (-16L)) = var_2;
    var_5 = (uint64_t *)(var_0 + (-160L));
    *var_5 = 4214564UL;
    indirect_placeholder();
    merge = 1UL;
    rbx_0 = 0UL;
    if ((uint64_t)(uint32_t)var_1 != 0UL) {
        *(uint64_t *)(var_0 + (-168L)) = 4214773UL;
        indirect_placeholder();
        rbx_0 = 1UL;
        if (*(uint32_t *)var_1 != 38U) {
            merge = rbx_0;
            return merge;
        }
        *(uint64_t *)(var_0 + (-176L)) = 4214796UL;
        var_6 = indirect_placeholder_51(4UL, rsi);
        var_7 = var_6.field_0;
        var_8 = var_6.field_1;
        var_9 = var_6.field_2;
        *(uint64_t *)(var_0 + (-184L)) = 4214804UL;
        indirect_placeholder();
        var_10 = (uint64_t)*(uint32_t *)var_7;
        *(uint64_t *)(var_0 + (-192L)) = 4214823UL;
        indirect_placeholder_50(0UL, var_7, var_8, 0UL, var_9, var_10, 4270840UL);
        return 1UL;
    }
    var_11 = *var_5;
    var_12 = var_11 + (-352400198L);
    if (var_12 == 0UL) {
        var_13 = helper_cc_compute_all_wrapper(var_12, 352400198UL, var_4, 17U);
        if ((var_13 & 65UL) == 0UL) {
            var_42 = var_11 + (-1650746742L);
            if (var_42 == 0UL) {
                var_43 = helper_cc_compute_all_wrapper(var_42, 1650746742UL, var_4, 17U);
                if ((var_43 & 65UL) == 0UL) {
                    var_58 = var_11 + (-1936880249L);
                    if (var_58 == 0UL) {
                        var_59 = helper_cc_compute_all_wrapper(var_58, 1936880249UL, var_4, 17U);
                        if ((var_59 & 65UL) == 0UL) {
                            var_66 = var_11 + (-3380511080L);
                            if (var_66 == 0UL) {
                                var_67 = helper_cc_compute_all_wrapper(var_66, 3380511080UL, var_4, 17U);
                                if ((var_67 & 65UL) == 0UL) {
                                    if (var_11 == 4076150800UL) {
                                        if (var_11 > 4076150800UL) {
                                            merge = rbx_0;
                                        } else {
                                            merge = rbx_0;
                                        }
                                    } else {
                                        merge = rbx_0;
                                    }
                                } else {
                                    if (var_11 == 2435016766UL) {
                                        if (var_11 > 2435016766UL) {
                                            merge = rbx_0;
                                        } else {
                                            merge = rbx_0;
                                        }
                                    } else {
                                        merge = rbx_0;
                                    }
                                }
                            } else {
                                merge = rbx_0;
                            }
                        } else {
                            var_60 = var_11 + (-1684300152L);
                            if (var_60 == 0UL) {
                                var_61 = helper_cc_compute_all_wrapper(var_60, 1684300152UL, var_4, 17U);
                                if ((var_61 & 65UL) == 0UL) {
                                    var_64 = var_11 + (-1853056627L);
                                    if (var_64 == 0UL) {
                                        var_65 = helper_cc_compute_all_wrapper(var_64, 1853056627UL, var_4, 17U);
                                        if ((var_65 & 65UL) == 0UL) {
                                            merge = rbx_0;
                                        } else {
                                            merge = rbx_0;
                                        }
                                    } else {
                                        merge = rbx_0;
                                    }
                                } else {
                                    var_62 = var_11 + (-1650812274L);
                                    if (var_62 == 0UL) {
                                        var_63 = helper_cc_compute_all_wrapper(var_62, 1650812274UL, var_4, 17U);
                                        if ((var_63 & 65UL) == 0UL) {
                                            merge = rbx_0;
                                        } else {
                                            if (var_11 == 1650812272UL) {
                                                merge = rbx_0;
                                            }
                                        }
                                    } else {
                                        merge = rbx_0;
                                    }
                                }
                            } else {
                                merge = rbx_0;
                            }
                        }
                    } else {
                        merge = rbx_0;
                    }
                } else {
                    var_44 = var_11 + (-1161678120L);
                    if (var_44 == 0UL) {
                        var_45 = helper_cc_compute_all_wrapper(var_44, 1161678120UL, var_4, 17U);
                        if ((var_45 & 65UL) == 0UL) {
                            var_52 = var_11 + (-1410924800L);
                            if (var_52 == 0UL) {
                                var_53 = helper_cc_compute_all_wrapper(var_52, 1410924800UL, var_4, 17U);
                                if ((var_53 & 65UL) == 0UL) {
                                    var_56 = var_11 + (-1481003842L);
                                    if (var_56 == 0UL) {
                                        var_57 = helper_cc_compute_all_wrapper(var_56, 1481003842UL, var_4, 17U);
                                        if ((var_57 & 65UL) == 0UL) {
                                            merge = rbx_0;
                                        } else {
                                            merge = rbx_0;
                                        }
                                    } else {
                                        merge = rbx_0;
                                    }
                                } else {
                                    var_54 = var_11 + (-1397114950L);
                                    if (var_54 == 0UL) {
                                        var_55 = helper_cc_compute_all_wrapper(var_54, 1397114950UL, var_4, 17U);
                                        if ((var_55 & 65UL) == 0UL) {
                                            merge = rbx_0;
                                        } else {
                                            merge = rbx_0;
                                        }
                                    } else {
                                        merge = rbx_0;
                                    }
                                }
                            } else {
                                merge = rbx_0;
                            }
                        } else {
                            var_46 = var_11 + (-732765674L);
                            if (var_46 == 0UL) {
                                var_47 = helper_cc_compute_all_wrapper(var_46, 732765674UL, var_4, 17U);
                                if ((var_47 & 65UL) == 0UL) {
                                    var_50 = var_11 + (-1111905073L);
                                    if (var_50 == 0UL) {
                                        var_51 = helper_cc_compute_all_wrapper(var_50, 1111905073UL, var_4, 17U);
                                        if ((var_51 & 65UL) == 0UL) {
                                            merge = rbx_0;
                                        } else {
                                            merge = rbx_0;
                                        }
                                    } else {
                                        merge = rbx_0;
                                    }
                                } else {
                                    var_48 = var_11 + (-464386766L);
                                    if (var_48 == 0UL) {
                                        var_49 = helper_cc_compute_all_wrapper(var_48, 464386766UL, var_4, 17U);
                                        if ((var_49 & 65UL) == 0UL) {
                                            merge = rbx_0;
                                        } else {
                                            if (var_11 == 427819522UL) {
                                                merge = rbx_0;
                                            }
                                        }
                                    } else {
                                        merge = rbx_0;
                                    }
                                }
                            } else {
                                merge = rbx_0;
                            }
                        }
                    } else {
                        merge = rbx_0;
                    }
                }
            } else {
                merge = rbx_0;
            }
        } else {
            var_14 = var_11 + (-29366L);
            if (var_14 == 0UL) {
                var_15 = helper_cc_compute_all_wrapper(var_14, 29366UL, var_4, 17U);
                if ((var_15 & 65UL) == 0UL) {
                    var_30 = var_11 + (-4278867L);
                    if (var_30 == 0UL) {
                        var_31 = helper_cc_compute_all_wrapper(var_30, 4278867UL, var_4, 17U);
                        if ((var_31 & 65UL) == 0UL) {
                            if (var_11 > 19920823UL) {
                                var_40 = var_11 + (-195894762L);
                                if (var_40 == 0UL) {
                                    var_41 = helper_cc_compute_all_wrapper(var_40, 195894762UL, var_4, 17U);
                                    if ((var_41 & 65UL) == 0UL) {
                                        merge = rbx_0;
                                    } else {
                                        merge = rbx_0;
                                    }
                                } else {
                                    merge = rbx_0;
                                }
                            } else {
                                var_37 = helper_cc_compute_c_wrapper(var_11 + (-19920820L), 19920820UL, var_4, 17U);
                                if (var_37 == 0UL) {
                                    var_38 = var_11 + (-16914836L);
                                    if (var_38 == 0UL) {
                                        var_39 = helper_cc_compute_all_wrapper(var_38, 16914836UL, var_4, 17U);
                                        if ((var_39 & 65UL) == 0UL) {
                                            merge = rbx_0;
                                        } else {
                                            if (var_11 == 12648430UL) {
                                                merge = rbx_0;
                                            }
                                        }
                                    } else {
                                        merge = rbx_0;
                                    }
                                } else {
                                    merge = rbx_0;
                                }
                            }
                        } else {
                            var_32 = var_11 + (-44543L);
                            if (var_32 == 0UL) {
                                var_33 = helper_cc_compute_all_wrapper(var_32, 44543UL, var_4, 17U);
                                if ((var_33 & 65UL) == 0UL) {
                                    var_35 = var_11 + (-61791L);
                                    if (var_35 == 0UL) {
                                        var_36 = helper_cc_compute_all_wrapper(var_35, 61791UL, var_4, 17U);
                                        if ((var_36 & 65UL) == 0UL) {
                                            merge = rbx_0;
                                        } else {
                                            merge = rbx_0;
                                        }
                                    } else {
                                        merge = rbx_0;
                                    }
                                } else {
                                    if (var_11 > 40866UL) {
                                        if (var_11 == 44533UL) {
                                            merge = rbx_0;
                                        }
                                    } else {
                                        var_34 = helper_cc_compute_c_wrapper(var_11 + (-40864L), 40864UL, var_4, 17U);
                                        if ((var_34 == 0UL) || (var_11 == 38496UL)) {
                                            merge = rbx_0;
                                        }
                                    }
                                }
                            } else {
                                merge = rbx_0;
                            }
                        }
                    } else {
                        merge = rbx_0;
                    }
                } else {
                    var_16 = var_11 + (-13364L);
                    if (var_16 == 0UL) {
                        var_17 = helper_cc_compute_all_wrapper(var_16, 13364UL, var_4, 17U);
                        if ((var_17 & 65UL) == 0UL) {
                            var_24 = var_11 + (-18475L);
                            if (var_24 == 0UL) {
                                var_25 = helper_cc_compute_all_wrapper(var_24, 18475UL, var_4, 17U);
                                if ((var_25 & 65UL) == 0UL) {
                                    var_28 = var_11 + (-19802L);
                                    if (var_28 == 0UL) {
                                        var_29 = helper_cc_compute_all_wrapper(var_28, 19802UL, var_4, 17U);
                                        if ((var_29 & 65UL) == 0UL) {
                                            merge = rbx_0;
                                        } else {
                                            merge = rbx_0;
                                        }
                                    } else {
                                        merge = rbx_0;
                                    }
                                } else {
                                    var_26 = var_11 + (-16388L);
                                    if (var_26 == 0UL) {
                                        var_27 = helper_cc_compute_all_wrapper(var_26, 16388UL, var_4, 17U);
                                        if ((var_27 & 65UL) == 0UL) {
                                            merge = rbx_0;
                                        } else {
                                            if (var_11 == 16384UL) {
                                                merge = rbx_0;
                                            }
                                        }
                                    } else {
                                        merge = rbx_0;
                                    }
                                }
                            } else {
                                merge = rbx_0;
                            }
                        } else {
                            var_18 = var_11 + (-4989L);
                            if (var_18 == 0UL) {
                                var_19 = helper_cc_compute_all_wrapper(var_18, 4989UL, var_4, 17U);
                                if ((var_19 & 65UL) == 0UL) {
                                    var_22 = var_11 + (-7377L);
                                    if (var_22 == 0UL) {
                                        var_23 = helper_cc_compute_all_wrapper(var_22, 7377UL, var_4, 17U);
                                        if ((var_23 & 65UL) == 0UL) {
                                            merge = rbx_0;
                                        } else {
                                            merge = rbx_0;
                                        }
                                    } else {
                                        merge = rbx_0;
                                    }
                                } else {
                                    var_20 = var_11 + (-391L);
                                    if (var_20 == 0UL) {
                                        var_21 = helper_cc_compute_all_wrapper(var_20, 391UL, var_4, 17U);
                                        if ((var_21 & 65UL) == 0UL) {
                                            merge = rbx_0;
                                        } else {
                                            if (var_11 == 47UL) {
                                                merge = rbx_0;
                                            }
                                        }
                                    } else {
                                        merge = rbx_0;
                                    }
                                }
                            } else {
                                merge = rbx_0;
                            }
                        }
                    } else {
                        merge = rbx_0;
                    }
                }
            } else {
                merge = rbx_0;
            }
        }
    } else {
        merge = rbx_0;
    }
}

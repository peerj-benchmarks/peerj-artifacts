typedef unsigned char __u_char;
typedef unsigned short __u_short;
typedef unsigned int __u_int;
typedef unsigned long __u_long;
typedef signed char __int8_t;
typedef unsigned char __uint8_t;
typedef short __int16_t;
typedef unsigned short __uint16_t;
typedef int __int32_t;
typedef unsigned int __uint32_t;
typedef long __int64_t;
typedef unsigned long __uint64_t;
typedef __int8_t __int_least8_t;
typedef __uint8_t __uint_least8_t;
typedef __int16_t __int_least16_t;
typedef __uint16_t __uint_least16_t;
typedef __int32_t __int_least32_t;
typedef __uint32_t __uint_least32_t;
typedef __int64_t __int_least64_t;
typedef __uint64_t __uint_least64_t;
typedef long __quad_t;
typedef unsigned long __u_quad_t;
typedef long __intmax_t;
typedef unsigned long __uintmax_t;
typedef unsigned long __dev_t;
typedef unsigned int __uid_t;
typedef unsigned int __gid_t;
typedef unsigned long __ino_t;
typedef unsigned long __ino64_t;
typedef unsigned int __mode_t;
typedef unsigned long __nlink_t;
typedef long __off_t;
typedef long __off64_t;
typedef int __pid_t;
typedef struct {
    int __val[2];
} __fsid_t;
typedef long __clock_t;
typedef unsigned long __rlim_t;
typedef unsigned long __rlim64_t;
typedef unsigned int __id_t;
typedef long __time_t;
typedef unsigned int __useconds_t;
typedef long __suseconds_t;
typedef int __daddr_t;
typedef int __key_t;
typedef int __clockid_t;
typedef void *__timer_t;
typedef long __blksize_t;
typedef long __blkcnt_t;
typedef long __blkcnt64_t;
typedef unsigned long __fsblkcnt_t;
typedef unsigned long __fsblkcnt64_t;
typedef unsigned long __fsfilcnt_t;
typedef unsigned long __fsfilcnt64_t;
typedef long __fsword_t;
typedef long __ssize_t;
typedef long __syscall_slong_t;
typedef unsigned long __syscall_ulong_t;
typedef __off64_t __loff_t;
typedef char *__caddr_t;
typedef long __intptr_t;
typedef unsigned int __socklen_t;
typedef int __sig_atomic_t;
typedef __int8_t int8_t;
typedef __int16_t int16_t;
typedef __int32_t int32_t;
typedef __int64_t int64_t;
typedef __uint8_t uint8_t;
typedef __uint16_t uint16_t;
typedef __uint32_t uint32_t;
typedef __uint64_t uint64_t;
typedef __int_least8_t int_least8_t;
typedef __int_least16_t int_least16_t;
typedef __int_least32_t int_least32_t;
typedef __int_least64_t int_least64_t;
typedef __uint_least8_t uint_least8_t;
typedef __uint_least16_t uint_least16_t;
typedef __uint_least32_t uint_least32_t;
typedef __uint_least64_t uint_least64_t;
typedef signed char int_fast8_t;
typedef long int_fast16_t;
typedef long int_fast32_t;
typedef long int_fast64_t;
typedef unsigned char uint_fast8_t;
typedef unsigned long uint_fast16_t;
typedef unsigned long uint_fast32_t;
typedef unsigned long uint_fast64_t;
typedef long intptr_t;
typedef unsigned long uintptr_t;
typedef __intmax_t intmax_t;
typedef __uintmax_t uintmax_t;
extern uint64_t revng_init_local_sp(uint64_t param_0);
extern uint64_t init_cc_src2(void);
extern uint64_t helper_cc_compute_c_wrapper(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint32_t param_3);
uint64_t bb_strintcmp(uint64_t rdi, uint64_t rsi) {
    uint64_t rdi1_3;
    uint64_t rdx_4;
    uint64_t var_26;
    uint64_t var_0;
    unsigned char var_1;
    uint64_t var_2;
    unsigned char var_3;
    uint64_t var_4;
    uint64_t rdi1_4;
    uint64_t rsi2_5;
    uint64_t rdi1_0;
    uint64_t rdx_0;
    unsigned char var_5;
    uint64_t rax_0;
    uint64_t rsi2_0;
    unsigned char var_6;
    uint64_t rdi1_2;
    uint64_t var_7;
    uint64_t _pre_phi104;
    uint64_t rdi1_1;
    uint64_t rsi2_1;
    uint64_t var_8;
    uint64_t var_9;
    unsigned char var_10;
    uint64_t var_11;
    unsigned char var_12;
    uint64_t rax_1;
    uint64_t rdx_1;
    uint64_t rsi2_2;
    uint32_t var_13;
    uint64_t var_14;
    uint32_t var_15;
    uint64_t var_16;
    uint64_t r8_0;
    uint64_t _pre103;
    uint64_t var_17;
    uint64_t var_18;
    uint64_t r8_1;
    uint64_t rdx_2;
    uint64_t rsi2_3;
    uint64_t rdx_3;
    uint64_t rsi2_4;
    uint64_t rcx_0;
    uint64_t var_19;
    uint64_t var_20;
    uint64_t var_48;
    uint64_t var_21;
    uint64_t var_22;
    uint64_t var_23;
    uint64_t var_24;
    unsigned char var_25;
    uint64_t var_27;
    unsigned char var_28;
    uint64_t var_29;
    uint64_t rsi2_7;
    uint64_t rax_3;
    uint64_t rsi2_6;
    uint64_t var_30;
    uint64_t merge;
    uint64_t var_31;
    unsigned char var_32;
    uint64_t rdi1_6;
    uint64_t rdi1_5;
    uint64_t rdx_5;
    uint64_t rsi2_8;
    uint64_t var_33;
    uint64_t var_34;
    unsigned char var_35;
    uint64_t var_36;
    unsigned char var_37;
    uint64_t rax_4;
    uint64_t rdx_6;
    uint64_t rsi2_9;
    uint32_t var_38;
    uint32_t var_39;
    uint32_t var_40;
    uint32_t var_41;
    uint64_t var_42;
    uint64_t rcx_1;
    uint64_t var_43;
    uint64_t var_44;
    uint64_t rcx_2;
    uint64_t rdi1_7;
    uint64_t var_45;
    uint64_t var_46;
    uint64_t var_47;
    unsigned int loop_state_var;
    revng_init_local_sp(0UL);
    var_0 = init_cc_src2();
    var_1 = *(unsigned char *)rdi;
    var_2 = (uint64_t)var_1;
    var_3 = *(unsigned char *)rsi;
    var_4 = (uint64_t)var_3;
    rdi1_3 = rdi;
    rdx_4 = var_2;
    rdi1_4 = rdi;
    rsi2_5 = rsi;
    rdi1_0 = rdi;
    rdx_0 = var_2;
    rax_0 = var_4;
    rsi2_0 = rsi;
    r8_0 = 0UL;
    r8_1 = 0UL;
    rdx_2 = 0UL;
    rcx_0 = 0UL;
    rsi2_7 = rsi;
    rax_3 = var_4;
    rsi2_6 = rsi;
    merge = 0UL;
    rcx_1 = 0UL;
    rcx_2 = 0UL;
    rdi1_7 = 0UL;
    if ((uint64_t)(var_1 + '\xd3') != 0UL) {
        var_27 = rdi1_4 + 1UL;
        var_28 = *(unsigned char *)var_27;
        rdi1_4 = var_27;
        merge = 4294967295UL;
        rdi1_5 = var_27;
        rdi1_6 = var_27;
        do {
            var_27 = rdi1_4 + 1UL;
            var_28 = *(unsigned char *)var_27;
            rdi1_4 = var_27;
            merge = 4294967295UL;
            rdi1_5 = var_27;
            rdi1_6 = var_27;
        } while (var_28 != '0');
        var_29 = (uint64_t)var_28;
        rdx_5 = var_29;
        rdx_6 = var_29;
        if ((uint64_t)(var_3 + '\xd3') == 0UL) {
            if ((uint64_t)(((uint32_t)var_29 + (-48)) & (-2)) > 9UL) {
                return merge;
            }
            while ((uint64_t)((unsigned char)rax_3 + '\xd0') != 0UL)
                {
                    var_30 = rsi2_6 + 1UL;
                    rax_3 = (uint64_t)*(unsigned char *)var_30;
                    rsi2_6 = var_30;
                }
            return ((uint64_t)(((uint32_t)rax_3 + (-48)) & (-2)) < 10UL) ? 4294967295UL : 0UL;
        }
        var_31 = rsi2_7 + 1UL;
        var_32 = *(unsigned char *)var_31;
        rsi2_7 = var_31;
        rsi2_8 = var_31;
        rsi2_9 = var_31;
        do {
            var_31 = rsi2_7 + 1UL;
            var_32 = *(unsigned char *)var_31;
            rsi2_7 = var_31;
            rsi2_8 = var_31;
            rsi2_9 = var_31;
        } while (var_32 != '0');
        rax_4 = (uint64_t)var_32;
        if ((uint64_t)(var_32 - var_28) != 0UL) {
            while (1U)
                {
                    if ((uint64_t)(((uint32_t)rdx_5 + (-48)) & (-2)) <= 9UL) {
                        loop_state_var = 1U;
                        break;
                    }
                    var_33 = rdi1_5 + 1UL;
                    var_34 = rsi2_8 + 1UL;
                    var_35 = *(unsigned char *)var_33;
                    var_36 = (uint64_t)var_35;
                    var_37 = *(unsigned char *)var_34;
                    rdi1_6 = var_33;
                    rdi1_5 = var_33;
                    rdx_5 = var_36;
                    rsi2_8 = var_34;
                    rax_4 = (uint64_t)var_37;
                    rdx_6 = var_36;
                    rsi2_9 = var_34;
                    if ((uint64_t)(var_35 - var_37) == 0UL) {
                        continue;
                    }
                    loop_state_var = 0U;
                    break;
                }
            switch (loop_state_var) {
              case 1U:
                {
                    return merge;
                }
                break;
              case 0U:
                {
                    break;
                }
                break;
            }
        }
        var_38 = (uint32_t)rdx_6;
        var_39 = var_38 + (-48);
        var_40 = (uint32_t)rax_4;
        var_41 = var_40 + (-48);
        var_42 = (uint64_t)(var_40 - var_38);
        if ((uint64_t)(var_39 & (-2)) > 9UL) {
            if ((uint64_t)(var_41 & (-2)) > 9UL) {
                return merge;
            }
        }
        var_43 = (uint64_t)*(unsigned char *)((rcx_1 + rdi1_6) + 1UL);
        var_44 = rcx_1 + 1UL;
        rcx_1 = var_44;
        rcx_2 = var_44;
        do {
            var_43 = (uint64_t)*(unsigned char *)((rcx_1 + rdi1_6) + 1UL);
            var_44 = rcx_1 + 1UL;
            rcx_1 = var_44;
            rcx_2 = var_44;
        } while ((uint64_t)(((uint32_t)var_43 + (-48)) & (-2)) <= 9UL);
        if ((uint64_t)(var_41 & (-2)) > 9UL) {
            return (var_44 == 0UL) ? 0UL : 4294967295UL;
        }
        var_45 = (uint64_t)*(unsigned char *)((rdi1_7 + rsi2_9) + 1UL);
        var_46 = rdi1_7 + 1UL;
        rdi1_7 = var_46;
        do {
            var_45 = (uint64_t)*(unsigned char *)((rdi1_7 + rsi2_9) + 1UL);
            var_46 = rdi1_7 + 1UL;
            rdi1_7 = var_46;
        } while ((uint64_t)(((uint32_t)var_45 + (-48)) & (-2)) <= 9UL);
        var_47 = rcx_2 - var_46;
        if (var_47 == 0UL) {
            return (rcx_2 == 0UL) ? 0UL : var_42;
        }
        var_48 = helper_cc_compute_c_wrapper(var_47, var_46, var_0, 17U);
        return (uint64_t)((uint32_t)((0UL - var_48) & 2UL) + (-1));
    }
    if ((uint64_t)(var_3 + '\xd3') == 0UL) {
        var_24 = rsi2_5 + 1UL;
        var_25 = *(unsigned char *)var_24;
        rsi2_5 = var_24;
        merge = 1UL;
        do {
            var_24 = rsi2_5 + 1UL;
            var_25 = *(unsigned char *)var_24;
            rsi2_5 = var_24;
            merge = 1UL;
        } while (var_25 != '0');
        if ((uint64_t)(((uint32_t)(uint64_t)var_25 + (-48)) & (-2)) <= 9UL) {
            while ((uint64_t)((unsigned char)rdx_4 + '\xd0') != 0UL)
                {
                    var_26 = rdi1_3 + 1UL;
                    rdi1_3 = var_26;
                    rdx_4 = (uint64_t)*(unsigned char *)var_26;
                }
            return ((uint64_t)(((uint32_t)rdx_4 + (-48)) & (-2)) < 10UL);
        }
    }
    var_5 = (unsigned char)rdx_0;
    rdi1_1 = rdi1_0;
    rdi1_2 = rdi1_0;
    rdx_1 = rdx_0;
    while ((uint64_t)(var_5 + '\xd0') != 0UL)
        {
            var_23 = rdi1_0 + 1UL;
            rdi1_0 = var_23;
            rdx_0 = (uint64_t)*(unsigned char *)var_23;
            var_5 = (unsigned char)rdx_0;
            rdi1_1 = rdi1_0;
            rdi1_2 = rdi1_0;
            rdx_1 = rdx_0;
        }
    var_6 = (unsigned char)rax_0;
    rsi2_1 = rsi2_0;
    rax_1 = rax_0;
    rsi2_2 = rsi2_0;
    rsi2_3 = rsi2_0;
    while ((uint64_t)(var_6 + '\xd0') != 0UL)
        {
            var_22 = rsi2_0 + 1UL;
            rax_0 = (uint64_t)*(unsigned char *)var_22;
            rsi2_0 = var_22;
            var_6 = (unsigned char)rax_0;
            rsi2_1 = rsi2_0;
            rax_1 = rax_0;
            rsi2_2 = rsi2_0;
            rsi2_3 = rsi2_0;
        }
    if ((uint64_t)(var_6 - var_5) == 0UL) {
        var_13 = (uint32_t)rdx_1;
        var_14 = (uint64_t)((var_13 + (-48)) & (-2));
        var_15 = (uint32_t)rax_1;
        var_16 = (uint64_t)(var_13 - var_15);
        rdx_2 = var_16;
        rsi2_3 = rsi2_2;
        rdx_3 = var_16;
        rsi2_4 = rsi2_2;
        if (var_14 > 9UL) {
            _pre103 = (uint64_t)((var_15 + (-48)) & (-2));
            _pre_phi104 = _pre103;
            rdx_3 = rdx_2;
            rsi2_4 = rsi2_3;
            if (_pre_phi104 > 9UL) {
                return merge;
            }
        }
        var_17 = (uint64_t)*(unsigned char *)((r8_0 + rdi1_2) + 1UL);
        var_18 = r8_0 + 1UL;
        r8_0 = var_18;
        r8_1 = var_18;
        do {
            var_17 = (uint64_t)*(unsigned char *)((r8_0 + rdi1_2) + 1UL);
            var_18 = r8_0 + 1UL;
            r8_0 = var_18;
            r8_1 = var_18;
        } while ((uint64_t)(((uint32_t)var_17 + (-48)) & (-2)) <= 9UL);
        if ((uint64_t)((var_15 + (-48)) & (-2)) > 9UL) {
            return (var_18 != 0UL);
        }
        var_19 = (uint64_t)*(unsigned char *)((rcx_0 + rsi2_4) + 1UL);
        var_20 = rcx_0 + 1UL;
        rcx_0 = var_20;
        do {
            var_19 = (uint64_t)*(unsigned char *)((rcx_0 + rsi2_4) + 1UL);
            var_20 = rcx_0 + 1UL;
            rcx_0 = var_20;
        } while ((uint64_t)(((uint32_t)var_19 + (-48)) & (-2)) <= 9UL);
        if (r8_1 == var_20) {
            return (r8_1 == 0UL) ? 0UL : (uint64_t)(uint32_t)rdx_3;
        }
        var_21 = helper_cc_compute_c_wrapper(r8_1 - var_20, var_20, var_0, 17U);
        return (uint64_t)((0U - (uint32_t)var_21) & (-2)) | 1UL;
    }
    var_7 = (uint64_t)(((uint32_t)rax_0 + (-48)) & (-2));
    _pre_phi104 = var_7;
    if (var_7 > 9UL) {
        while (1U)
            {
                var_8 = rdi1_1 + 1UL;
                var_9 = rsi2_1 + 1UL;
                var_10 = *(unsigned char *)var_8;
                var_11 = (uint64_t)var_10;
                var_12 = *(unsigned char *)var_9;
                rdi1_2 = var_8;
                rdi1_1 = var_8;
                rsi2_1 = var_9;
                rax_1 = (uint64_t)var_12;
                rdx_1 = var_11;
                rsi2_2 = var_9;
                if ((uint64_t)(var_10 - var_12) != 0UL) {
                    loop_state_var = 0U;
                    break;
                }
                if ((uint64_t)(((uint32_t)var_11 + (-48)) & (-2)) > 9UL) {
                    continue;
                }
                loop_state_var = 1U;
                break;
            }
        switch (loop_state_var) {
          case 1U:
            {
                return merge;
            }
            break;
          case 0U:
            {
                break;
            }
            break;
        }
    }
    rdx_3 = rdx_2;
    rsi2_4 = rsi2_3;
    if (_pre_phi104 > 9UL) {
        return merge;
    }
    var_19 = (uint64_t)*(unsigned char *)((rcx_0 + rsi2_4) + 1UL);
    var_20 = rcx_0 + 1UL;
    rcx_0 = var_20;
    do {
        var_19 = (uint64_t)*(unsigned char *)((rcx_0 + rsi2_4) + 1UL);
        var_20 = rcx_0 + 1UL;
        rcx_0 = var_20;
    } while ((uint64_t)(((uint32_t)var_19 + (-48)) & (-2)) <= 9UL);
    if (r8_1 == var_20) {
        return (r8_1 == 0UL) ? 0UL : (uint64_t)(uint32_t)rdx_3;
    }
    var_21 = helper_cc_compute_c_wrapper(r8_1 - var_20, var_20, var_0, 17U);
    return (uint64_t)((0U - (uint32_t)var_21) & (-2)) | 1UL;
    var_21 = helper_cc_compute_c_wrapper(r8_1 - var_20, var_20, var_0, 17U);
    return (uint64_t)((0U - (uint32_t)var_21) & (-2)) | 1UL;
}

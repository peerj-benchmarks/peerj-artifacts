typedef unsigned char __u_char;
typedef unsigned short __u_short;
typedef unsigned int __u_int;
typedef unsigned long __u_long;
typedef signed char __int8_t;
typedef unsigned char __uint8_t;
typedef short __int16_t;
typedef unsigned short __uint16_t;
typedef int __int32_t;
typedef unsigned int __uint32_t;
typedef long __int64_t;
typedef unsigned long __uint64_t;
typedef __int8_t __int_least8_t;
typedef __uint8_t __uint_least8_t;
typedef __int16_t __int_least16_t;
typedef __uint16_t __uint_least16_t;
typedef __int32_t __int_least32_t;
typedef __uint32_t __uint_least32_t;
typedef __int64_t __int_least64_t;
typedef __uint64_t __uint_least64_t;
typedef long __quad_t;
typedef unsigned long __u_quad_t;
typedef long __intmax_t;
typedef unsigned long __uintmax_t;
typedef unsigned long __dev_t;
typedef unsigned int __uid_t;
typedef unsigned int __gid_t;
typedef unsigned long __ino_t;
typedef unsigned long __ino64_t;
typedef unsigned int __mode_t;
typedef unsigned long __nlink_t;
typedef long __off_t;
typedef long __off64_t;
typedef int __pid_t;
typedef struct {
    int __val[2];
} __fsid_t;
typedef long __clock_t;
typedef unsigned long __rlim_t;
typedef unsigned long __rlim64_t;
typedef unsigned int __id_t;
typedef long __time_t;
typedef unsigned int __useconds_t;
typedef long __suseconds_t;
typedef int __daddr_t;
typedef int __key_t;
typedef int __clockid_t;
typedef void *__timer_t;
typedef long __blksize_t;
typedef long __blkcnt_t;
typedef long __blkcnt64_t;
typedef unsigned long __fsblkcnt_t;
typedef unsigned long __fsblkcnt64_t;
typedef unsigned long __fsfilcnt_t;
typedef unsigned long __fsfilcnt64_t;
typedef long __fsword_t;
typedef long __ssize_t;
typedef long __syscall_slong_t;
typedef unsigned long __syscall_ulong_t;
typedef __off64_t __loff_t;
typedef char *__caddr_t;
typedef long __intptr_t;
typedef unsigned int __socklen_t;
typedef int __sig_atomic_t;
typedef __int8_t int8_t;
typedef __int16_t int16_t;
typedef __int32_t int32_t;
typedef __int64_t int64_t;
typedef __uint8_t uint8_t;
typedef __uint16_t uint16_t;
typedef __uint32_t uint32_t;
typedef __uint64_t uint64_t;
typedef __int_least8_t int_least8_t;
typedef __int_least16_t int_least16_t;
typedef __int_least32_t int_least32_t;
typedef __int_least64_t int_least64_t;
typedef __uint_least8_t uint_least8_t;
typedef __uint_least16_t uint_least16_t;
typedef __uint_least32_t uint_least32_t;
typedef __uint_least64_t uint_least64_t;
typedef signed char int_fast8_t;
typedef long int_fast16_t;
typedef long int_fast32_t;
typedef long int_fast64_t;
typedef unsigned char uint_fast8_t;
typedef unsigned long uint_fast16_t;
typedef unsigned long uint_fast32_t;
typedef unsigned long uint_fast64_t;
typedef long intptr_t;
typedef unsigned long uintptr_t;
typedef __intmax_t intmax_t;
typedef __uintmax_t uintmax_t;
struct indirect_placeholder_47_ret_type;
struct indirect_placeholder_46_ret_type;
struct indirect_placeholder_48_ret_type;
struct indirect_placeholder_47_ret_type {
    uint64_t field_0;
    uint64_t field_1;
    uint64_t field_2;
};
struct indirect_placeholder_46_ret_type {
    uint64_t field_0;
    uint64_t field_1;
    uint64_t field_2;
};
struct indirect_placeholder_48_ret_type {
    uint64_t field_0;
    uint64_t field_1;
    uint64_t field_2;
};
extern void abort(void);
extern uint64_t revng_init_local_sp(uint64_t param_0);
extern void indirect_placeholder(void);
extern uint64_t init_cc_src2(void);
extern uint64_t helper_cc_compute_all_wrapper(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint32_t param_3);
extern uint64_t init_rax(void);
extern uint64_t indirect_placeholder_16(void);
extern struct indirect_placeholder_47_ret_type indirect_placeholder_47(uint64_t param_0);
extern struct indirect_placeholder_46_ret_type indirect_placeholder_46(uint64_t param_0);
extern struct indirect_placeholder_48_ret_type indirect_placeholder_48(uint64_t param_0);
uint64_t bb_posixtest(uint64_t rdi) {
    uint64_t var_0;
    uint64_t var_1;
    uint64_t var_2;
    uint64_t var_3;
    uint64_t var_19;
    uint64_t var_20;
    uint64_t rax_0;
    uint64_t var_15;
    uint64_t var_16;
    uint64_t local_sp_0;
    uint64_t var_13;
    uint64_t var_14;
    uint64_t var_21;
    uint64_t var_22;
    uint64_t var_4;
    uint64_t var_5;
    uint32_t var_10;
    uint64_t var_12;
    uint32_t var_6;
    uint64_t var_7;
    uint64_t var_8;
    uint64_t var_9;
    uint64_t var_11;
    uint32_t var_17;
    uint64_t var_18;
    var_0 = revng_init_local_sp(0UL);
    var_1 = init_rax();
    var_2 = var_0 + (-24L);
    var_3 = rdi + (-2L);
    local_sp_0 = var_2;
    if ((uint64_t)(uint32_t)var_3 == 0UL) {
        indirect_placeholder();
        return var_1;
    }
    var_4 = init_cc_src2();
    var_5 = helper_cc_compute_all_wrapper(var_3, 2UL, var_4, 16U);
    if ((uint64_t)(((unsigned char)(var_5 >> 4UL) ^ (unsigned char)var_5) & '\xc0') != 0UL) {
        if ((uint64_t)((uint32_t)rdi + (-1)) != 0UL) {
            var_11 = helper_cc_compute_all_wrapper(rdi, 0UL, 0UL, 24U);
            if ((uint64_t)(((unsigned char)(var_11 >> 4UL) ^ (unsigned char)var_11) & '\xc0') != 0UL) {
                *(uint64_t *)(var_0 + (-32L)) = 4205969UL;
                indirect_placeholder();
                indirect_placeholder();
                return var_1;
            }
            var_17 = *(uint32_t *)6360600UL;
            if ((int)var_17 > (int)*(uint32_t *)6360604UL) {
                var_18 = (uint64_t)var_17;
                indirect_placeholder();
                return var_18;
            }
            *(uint64_t *)(local_sp_0 + (-8L)) = 4206091UL;
            indirect_placeholder();
            abort();
        }
        var_6 = *(uint32_t *)6360604UL;
        var_7 = (uint64_t)var_6;
        *(uint32_t *)6360604UL = (var_6 + 1U);
        var_8 = *(uint64_t *)((var_7 << 3UL) + *(uint64_t *)6360592UL);
        var_9 = (var_8 & (-256L)) | (*(unsigned char *)var_8 != '\x00');
        rax_0 = var_9;
        return rax_0;
    }
    var_10 = (uint32_t)rdi;
    if ((uint64_t)(var_10 + (-3)) == 0UL) {
        indirect_placeholder();
        return var_1;
    }
    if ((uint64_t)(var_10 + (-4)) != 0UL) {
        var_11 = helper_cc_compute_all_wrapper(rdi, 0UL, 0UL, 24U);
        if ((uint64_t)(((unsigned char)(var_11 >> 4UL) ^ (unsigned char)var_11) & '\xc0') != 0UL) {
            *(uint64_t *)(var_0 + (-32L)) = 4205969UL;
            indirect_placeholder();
            indirect_placeholder();
            return var_1;
        }
    }
    var_12 = *(uint64_t *)6360592UL;
    *(uint64_t *)(var_0 + (-32L)) = 4205821UL;
    indirect_placeholder();
    if ((uint64_t)(uint32_t)var_12 != 0UL) {
        *(uint64_t *)(var_0 + (-40L)) = 4206002UL;
        indirect_placeholder_48(1UL);
        *(uint64_t *)(var_0 + (-48L)) = 4206007UL;
        var_21 = indirect_placeholder_16();
        var_22 = (uint64_t)(uint32_t)var_21 ^ 1UL;
        rax_0 = var_22;
        return rax_0;
    }
    var_13 = *(uint64_t *)6360592UL;
    var_14 = var_0 + (-40L);
    *(uint64_t *)var_14 = 4205857UL;
    indirect_placeholder();
    local_sp_0 = var_14;
    var_15 = *(uint64_t *)6360592UL;
    var_16 = var_0 + (-48L);
    *(uint64_t *)var_16 = 4206045UL;
    indirect_placeholder();
    local_sp_0 = var_16;
    if (~((uint64_t)(uint32_t)var_13 != 0UL & (uint64_t)(uint32_t)var_15 != 0UL)) {
        return;
    }
    *(uint64_t *)(var_0 + (-56L)) = 4206060UL;
    indirect_placeholder_47(0UL);
    *(uint64_t *)(var_0 + (-64L)) = 4206065UL;
    var_19 = indirect_placeholder_16();
    *(unsigned char *)(var_0 + (-49L)) = (unsigned char)var_19;
    *(uint64_t *)(var_0 + (-72L)) = 4206076UL;
    indirect_placeholder_46(0UL);
    var_20 = (uint64_t)*(unsigned char *)(var_0 + (-57L));
    rax_0 = var_20;
    return rax_0;
}

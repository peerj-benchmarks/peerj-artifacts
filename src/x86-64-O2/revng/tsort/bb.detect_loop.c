typedef unsigned char __u_char;
typedef unsigned short __u_short;
typedef unsigned int __u_int;
typedef unsigned long __u_long;
typedef signed char __int8_t;
typedef unsigned char __uint8_t;
typedef short __int16_t;
typedef unsigned short __uint16_t;
typedef int __int32_t;
typedef unsigned int __uint32_t;
typedef long __int64_t;
typedef unsigned long __uint64_t;
typedef __int8_t __int_least8_t;
typedef __uint8_t __uint_least8_t;
typedef __int16_t __int_least16_t;
typedef __uint16_t __uint_least16_t;
typedef __int32_t __int_least32_t;
typedef __uint32_t __uint_least32_t;
typedef __int64_t __int_least64_t;
typedef __uint64_t __uint_least64_t;
typedef long __quad_t;
typedef unsigned long __u_quad_t;
typedef long __intmax_t;
typedef unsigned long __uintmax_t;
typedef unsigned long __dev_t;
typedef unsigned int __uid_t;
typedef unsigned int __gid_t;
typedef unsigned long __ino_t;
typedef unsigned long __ino64_t;
typedef unsigned int __mode_t;
typedef unsigned long __nlink_t;
typedef long __off_t;
typedef long __off64_t;
typedef int __pid_t;
typedef struct {
    int __val[2];
} __fsid_t;
typedef long __clock_t;
typedef unsigned long __rlim_t;
typedef unsigned long __rlim64_t;
typedef unsigned int __id_t;
typedef long __time_t;
typedef unsigned int __useconds_t;
typedef long __suseconds_t;
typedef int __daddr_t;
typedef int __key_t;
typedef int __clockid_t;
typedef void *__timer_t;
typedef long __blksize_t;
typedef long __blkcnt_t;
typedef long __blkcnt64_t;
typedef unsigned long __fsblkcnt_t;
typedef unsigned long __fsblkcnt64_t;
typedef unsigned long __fsfilcnt_t;
typedef unsigned long __fsfilcnt64_t;
typedef long __fsword_t;
typedef long __ssize_t;
typedef long __syscall_slong_t;
typedef unsigned long __syscall_ulong_t;
typedef __off64_t __loff_t;
typedef char *__caddr_t;
typedef long __intptr_t;
typedef unsigned int __socklen_t;
typedef int __sig_atomic_t;
typedef __int8_t int8_t;
typedef __int16_t int16_t;
typedef __int32_t int32_t;
typedef __int64_t int64_t;
typedef __uint8_t uint8_t;
typedef __uint16_t uint16_t;
typedef __uint32_t uint32_t;
typedef __uint64_t uint64_t;
typedef __int_least8_t int_least8_t;
typedef __int_least16_t int_least16_t;
typedef __int_least32_t int_least32_t;
typedef __int_least64_t int_least64_t;
typedef __uint_least8_t uint_least8_t;
typedef __uint_least16_t uint_least16_t;
typedef __uint_least32_t uint_least32_t;
typedef __uint_least64_t uint_least64_t;
typedef signed char int_fast8_t;
typedef long int_fast16_t;
typedef long int_fast32_t;
typedef long int_fast64_t;
typedef unsigned char uint_fast8_t;
typedef unsigned long uint_fast16_t;
typedef unsigned long uint_fast32_t;
typedef unsigned long uint_fast64_t;
typedef long intptr_t;
typedef unsigned long uintptr_t;
typedef __intmax_t intmax_t;
typedef __uintmax_t uintmax_t;
extern uint64_t revng_init_local_sp(uint64_t param_0);
extern uint64_t init_rbx(void);
extern uint64_t init_rbp(void);
extern uint64_t init_r12(void);
extern void indirect_placeholder_13(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint64_t param_3, uint64_t param_4, uint64_t param_5, uint64_t param_6);
void bb_detect_loop(uint64_t r9, uint64_t rdi, uint64_t r8) {
    uint64_t local_sp_0;
    uint64_t var_0;
    uint64_t var_1;
    uint64_t var_2;
    uint64_t var_3;
    uint64_t var_4;
    uint64_t *var_15;
    uint64_t var_16;
    uint64_t var_17;
    uint64_t *var_18;
    uint64_t rax_0;
    uint64_t *var_19;
    uint64_t var_20;
    uint64_t var_5;
    uint64_t var_6;
    uint64_t var_7;
    uint64_t rbx_0;
    uint64_t rcx_0;
    uint64_t var_8;
    uint64_t var_9;
    uint64_t *var_10;
    uint64_t rdx_0;
    uint64_t var_11;
    uint64_t var_12;
    uint64_t var_13;
    uint64_t var_14;
    unsigned int loop_state_var;
    var_0 = revng_init_local_sp(0UL);
    var_1 = init_rbx();
    var_2 = init_rbp();
    var_3 = init_r12();
    *(uint64_t *)(var_0 + (-8L)) = var_3;
    *(uint64_t *)(var_0 + (-16L)) = var_2;
    var_4 = var_0 + (-24L);
    *(uint64_t *)var_4 = var_1;
    rax_0 = rdi;
    local_sp_0 = var_4;
    if (*(uint64_t *)(rdi + 32UL) == 0UL) {
        return;
    }
    var_5 = *(uint64_t *)6358296UL;
    rdx_0 = var_5;
    if (var_5 == 0UL) {
        *(uint64_t *)6358296UL = rdi;
    } else {
        var_6 = rdi + 48UL;
        var_7 = *(uint64_t *)var_6;
        rcx_0 = var_7;
        rbx_0 = var_6;
        if (var_7 != 0UL) {
            if (var_5 != *(uint64_t *)var_7) {
                while (1U)
                    {
                        var_8 = rcx_0 + 8UL;
                        var_9 = *(uint64_t *)var_8;
                        rcx_0 = var_9;
                        rbx_0 = var_8;
                        if (var_9 != 0UL) {
                            loop_state_var = 1U;
                            break;
                        }
                        if (var_5 == *(uint64_t *)var_9) {
                            continue;
                        }
                        loop_state_var = 0U;
                        break;
                    }
                switch (loop_state_var) {
                  case 1U:
                    {
                        return;
                    }
                    break;
                  case 0U:
                    {
                        break;
                    }
                    break;
                }
            }
            var_10 = (uint64_t *)(rdi + 40UL);
            if (*var_10 == 0UL) {
                *var_10 = var_5;
                *(uint64_t *)6358296UL = rdi;
            } else {
                while (1U)
                    {
                        var_11 = *(uint64_t *)rdx_0;
                        var_12 = *(uint64_t *)(rdx_0 + 40UL);
                        var_13 = local_sp_0 + (-8L);
                        *(uint64_t *)var_13 = 4203494UL;
                        indirect_placeholder_13(0UL, var_11, r9, 0UL, r8, 0UL, 4249205UL);
                        var_14 = *(uint64_t *)6358296UL;
                        local_sp_0 = var_13;
                        rdx_0 = var_12;
                        if (var_14 == rdi) {
                            *(uint64_t *)(var_14 + 40UL) = 0UL;
                            *(uint64_t *)6358296UL = var_12;
                            if (var_12 == 0UL) {
                                continue;
                            }
                            loop_state_var = 1U;
                            break;
                        }
                        var_15 = (uint64_t *)rbx_0;
                        var_16 = *var_15;
                        var_17 = *(uint64_t *)(var_16 + 8UL);
                        var_18 = (uint64_t *)(*(uint64_t *)var_16 + 32UL);
                        *var_18 = (*var_18 + (-1L));
                        *var_15 = var_17;
                        loop_state_var = 0U;
                        break;
                    }
                var_19 = (uint64_t *)(rax_0 + 40UL);
                var_20 = *var_19;
                *var_19 = 0UL;
                rax_0 = var_20;
                do {
                    var_19 = (uint64_t *)(rax_0 + 40UL);
                    var_20 = *var_19;
                    *var_19 = 0UL;
                    rax_0 = var_20;
                } while (var_20 != 0UL);
                *(uint64_t *)6358296UL = 0UL;
            }
        }
    }
}

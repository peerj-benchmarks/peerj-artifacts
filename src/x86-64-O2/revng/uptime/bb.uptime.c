typedef unsigned char __u_char;
typedef unsigned short __u_short;
typedef unsigned int __u_int;
typedef unsigned long __u_long;
typedef signed char __int8_t;
typedef unsigned char __uint8_t;
typedef short __int16_t;
typedef unsigned short __uint16_t;
typedef int __int32_t;
typedef unsigned int __uint32_t;
typedef long __int64_t;
typedef unsigned long __uint64_t;
typedef __int8_t __int_least8_t;
typedef __uint8_t __uint_least8_t;
typedef __int16_t __int_least16_t;
typedef __uint16_t __uint_least16_t;
typedef __int32_t __int_least32_t;
typedef __uint32_t __uint_least32_t;
typedef __int64_t __int_least64_t;
typedef __uint64_t __uint_least64_t;
typedef long __quad_t;
typedef unsigned long __u_quad_t;
typedef long __intmax_t;
typedef unsigned long __uintmax_t;
typedef unsigned long __dev_t;
typedef unsigned int __uid_t;
typedef unsigned int __gid_t;
typedef unsigned long __ino_t;
typedef unsigned long __ino64_t;
typedef unsigned int __mode_t;
typedef unsigned long __nlink_t;
typedef long __off_t;
typedef long __off64_t;
typedef int __pid_t;
typedef struct {
    int __val[2];
} __fsid_t;
typedef long __clock_t;
typedef unsigned long __rlim_t;
typedef unsigned long __rlim64_t;
typedef unsigned int __id_t;
typedef long __time_t;
typedef unsigned int __useconds_t;
typedef long __suseconds_t;
typedef int __daddr_t;
typedef int __key_t;
typedef int __clockid_t;
typedef void *__timer_t;
typedef long __blksize_t;
typedef long __blkcnt_t;
typedef long __blkcnt64_t;
typedef unsigned long __fsblkcnt_t;
typedef unsigned long __fsblkcnt64_t;
typedef unsigned long __fsfilcnt_t;
typedef unsigned long __fsfilcnt64_t;
typedef long __fsword_t;
typedef long __ssize_t;
typedef long __syscall_slong_t;
typedef unsigned long __syscall_ulong_t;
typedef __off64_t __loff_t;
typedef char *__caddr_t;
typedef long __intptr_t;
typedef unsigned int __socklen_t;
typedef int __sig_atomic_t;
typedef __int8_t int8_t;
typedef __int16_t int16_t;
typedef __int32_t int32_t;
typedef __int64_t int64_t;
typedef __uint8_t uint8_t;
typedef __uint16_t uint16_t;
typedef __uint32_t uint32_t;
typedef __uint64_t uint64_t;
typedef __int_least8_t int_least8_t;
typedef __int_least16_t int_least16_t;
typedef __int_least32_t int_least32_t;
typedef __int_least64_t int_least64_t;
typedef __uint_least8_t uint_least8_t;
typedef __uint_least16_t uint_least16_t;
typedef __uint_least32_t uint_least32_t;
typedef __uint_least64_t uint_least64_t;
typedef signed char int_fast8_t;
typedef long int_fast16_t;
typedef long int_fast32_t;
typedef long int_fast64_t;
typedef unsigned char uint_fast8_t;
typedef unsigned long uint_fast16_t;
typedef unsigned long uint_fast32_t;
typedef unsigned long uint_fast64_t;
typedef long intptr_t;
typedef unsigned long uintptr_t;
typedef __intmax_t intmax_t;
typedef __uintmax_t uintmax_t;
typedef _Bool bool;
extern void abort(void);
extern uint64_t revng_init_local_sp(uint64_t param_0);
extern uint64_t init_rbx(void);
extern uint64_t init_rbp(void);
extern void indirect_placeholder_6(void);
extern uint64_t init_r13(void);
extern uint64_t init_r12(void);
extern uint64_t init_r14(void);
extern uint64_t indirect_placeholder_7(uint64_t param_0);
extern uint64_t helper_cc_compute_all_wrapper(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint32_t param_3);
extern uint64_t indirect_placeholder_8(uint64_t param_0, uint64_t param_1, uint64_t param_2);
extern uint64_t init_r9(void);
extern uint64_t init_r8(void);
extern void indirect_placeholder_1(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint64_t param_3, uint64_t param_4, uint64_t param_5, uint64_t param_6);
extern void indirect_placeholder_9(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint64_t param_3, uint64_t param_4);
void bb_uptime(uint64_t rdi, uint64_t rsi) {
    uint64_t var_0;
    uint64_t var_1;
    uint64_t var_2;
    uint64_t var_3;
    uint64_t var_4;
    uint64_t var_5;
    uint64_t var_6;
    uint64_t var_7;
    uint64_t var_8;
    uint64_t var_9;
    uint64_t var_10;
    uint64_t var_11;
    uint64_t var_12;
    uint64_t rbx_4;
    uint64_t local_sp_1;
    uint64_t var_36;
    uint64_t local_sp_2;
    uint64_t var_31;
    bool var_32;
    uint64_t var_33;
    uint64_t *var_34;
    uint64_t var_35;
    uint64_t var_28;
    uint64_t var_29;
    uint64_t var_30;
    uint64_t rbx_3;
    uint64_t var_23;
    uint64_t var_24;
    uint64_t var_25;
    uint64_t var_26;
    uint64_t var_27;
    uint64_t rax_2;
    uint64_t rax_0;
    uint64_t rcx_0;
    uint64_t rbp_1;
    uint64_t rbx_0;
    uint64_t local_sp_3;
    uint64_t var_38;
    uint64_t var_39;
    uint64_t var_13;
    uint64_t *var_14;
    uint64_t var_15;
    uint64_t rbx_2;
    uint64_t rax_1_ph;
    uint64_t rbx_1_ph;
    uint64_t rdx_0_in;
    uint64_t rbp_0_ph;
    uint64_t rdx_0_ph_in;
    uint64_t rax_1;
    uint64_t rbp_0;
    uint64_t rdx_0;
    unsigned char var_16;
    uint16_t var_17;
    uint64_t var_18;
    uint64_t var_20;
    uint64_t var_21;
    uint64_t var_19;
    uint64_t var_22;
    uint64_t var_37;
    uint64_t local_sp_4;
    uint64_t var_40;
    uint64_t var_41;
    unsigned int loop_state_var;
    bool switch_state_var;
    var_0 = revng_init_local_sp(0UL);
    var_1 = init_rbx();
    var_2 = init_r9();
    var_3 = init_rbp();
    var_4 = init_r8();
    var_5 = init_r13();
    var_6 = init_r12();
    var_7 = init_r14();
    *(uint64_t *)(var_0 + (-8L)) = var_7;
    *(uint64_t *)(var_0 + (-16L)) = var_5;
    var_8 = (uint64_t)(uint32_t)rsi;
    *(uint64_t *)(var_0 + (-24L)) = var_6;
    *(uint64_t *)(var_0 + (-32L)) = var_3;
    *(uint64_t *)(var_0 + (-40L)) = var_1;
    var_9 = var_0 + (-88L);
    var_10 = var_0 + (-96L);
    *(uint64_t *)var_9 = 0UL;
    var_11 = var_0 + (-112L);
    *(uint64_t *)var_11 = 4202265UL;
    var_12 = indirect_placeholder_8(var_8, var_9, var_10);
    rbx_4 = rdi;
    rcx_0 = 0UL;
    rbx_0 = rdi;
    rbx_1_ph = 0UL;
    rbp_0_ph = 0UL;
    local_sp_4 = var_11;
    if ((uint64_t)(uint32_t)var_12 == 0UL) {
        *(uint64_t *)(local_sp_4 + (-8L)) = 4202885UL;
        var_40 = indirect_placeholder_8(0UL, rbx_4, 3UL);
        *(uint64_t *)(local_sp_4 + (-16L)) = 4202893UL;
        indirect_placeholder_6();
        var_41 = (uint64_t)*(uint32_t *)var_40;
        *(uint64_t *)(local_sp_4 + (-24L)) = 4202915UL;
        indirect_placeholder_1(0UL, var_40, var_2, 1UL, var_4, 4259381UL, var_41);
        abort();
    }
    var_13 = *(uint64_t *)(var_0 + (-104L));
    var_14 = (uint64_t *)var_10;
    var_15 = *var_14;
    rax_0 = var_15;
    rax_1_ph = var_15;
    rdx_0_ph_in = var_13;
    if (var_13 != 0UL) {
        while (1U)
            {
                rbx_3 = rbx_1_ph;
                rbx_2 = rbx_1_ph;
                rdx_0_in = rdx_0_ph_in;
                rax_1 = rax_1_ph;
                rbp_0 = rbp_0_ph;
                while (1U)
                    {
                        rdx_0 = rdx_0_in + (-1L);
                        var_16 = *(unsigned char *)(rax_1 + 44UL);
                        var_17 = *(uint16_t *)rax_1;
                        var_18 = (uint64_t)var_17;
                        rcx_0 = var_18;
                        rbp_1 = rbp_0;
                        rdx_0_in = rdx_0;
                        rbp_0_ph = rbp_0;
                        rdx_0_ph_in = rdx_0;
                        if (var_16 != '\x00') {
                            if ((uint64_t)(var_17 + (unsigned short)65529U) != 0UL) {
                                loop_state_var = 0U;
                                break;
                            }
                        }
                        if ((uint64_t)(var_17 + (unsigned short)65534U) != 0UL) {
                            loop_state_var = 2U;
                            break;
                        }
                        var_20 = *(uint64_t *)(rax_1 + 344UL);
                        var_21 = rax_1 + 400UL;
                        rax_1 = var_21;
                        rbp_0 = var_20;
                        rax_2 = var_21;
                        rbp_1 = var_20;
                        if (rdx_0 == 0UL) {
                            continue;
                        }
                        loop_state_var = 1U;
                        break;
                    }
                switch_state_var = 0;
                switch (loop_state_var) {
                  case 1U:
                    {
                        switch_state_var = 1;
                        break;
                    }
                    break;
                  case 2U:
                  case 0U:
                    {
                        switch_state_var = 0;
                        switch (loop_state_var) {
                          case 0U:
                            {
                                rbx_2 = rbx_1_ph + 1UL;
                            }
                            break;
                          case 2U:
                            {
                                var_19 = rax_1 + 400UL;
                                rax_1_ph = var_19;
                                rbx_1_ph = rbx_2;
                                rax_2 = var_19;
                                rbx_3 = rbx_2;
                                if (rdx_0 != 0UL) {
                                    continue;
                                }
                                switch_state_var = 1;
                                break;
                            }
                            break;
                        }
                        if (switch_state_var)
                            break;
                    }
                    break;
                }
                if (switch_state_var)
                    break;
            }
        var_22 = var_0 + (-120L);
        *(uint64_t *)var_22 = 4202375UL;
        indirect_placeholder_6();
        *var_14 = rax_2;
        rax_0 = rax_2;
        rbx_0 = rbx_3;
        local_sp_3 = var_22;
        if (rbp_1 != 0UL) {
            var_23 = rax_2 - rbp_1;
            var_24 = (uint64_t)((long)(uint64_t)(((unsigned __int128)var_23 * 1749024623285053783ULL) >> 64ULL) >> (long)13UL) - (uint64_t)((long)var_23 >> (long)63UL);
            var_25 = (var_24 * 18446744073709465216UL) + var_23;
            var_26 = (var_25 - (uint64_t)((long)((uint64_t)((long)((uint64_t)((uint32_t)(uint64_t)(((unsigned __int128)var_25 * 5247073869855161349ULL) >> 74ULL) - (uint32_t)(uint64_t)((long)var_25 >> (long)63UL)) << 32UL) >> (long)32UL) * 15461882265600UL) >> (long)32UL)) * 9838263505978427529UL;
            *(uint64_t *)(var_0 + (-128L)) = 4202522UL;
            var_27 = indirect_placeholder_7(var_26);
            if (var_27 == 0UL) {
                var_30 = var_0 + (-136L);
                *(uint64_t *)var_30 = 4202789UL;
                indirect_placeholder_6();
                local_sp_2 = var_30;
            } else {
                var_28 = *(uint64_t *)6370496UL;
                var_29 = var_0 + (-136L);
                *(uint64_t *)var_29 = 4202556UL;
                indirect_placeholder_9(0UL, var_28, 0UL, var_27, 4258265UL);
                local_sp_2 = var_29;
            }
            if (var_23 == 18446744073709551615UL) {
                var_35 = local_sp_2 + (-8L);
                *(uint64_t *)var_35 = 4202806UL;
                indirect_placeholder_6();
                local_sp_1 = var_35;
            } else {
                var_31 = helper_cc_compute_all_wrapper(var_24, 0UL, 0UL, 25U);
                var_32 = ((uint64_t)(((unsigned char)(var_31 >> 4UL) ^ (unsigned char)var_31) & '\xc0') == 0UL);
                var_33 = local_sp_2 + (-8L);
                var_34 = (uint64_t *)var_33;
                local_sp_1 = var_33;
                if (var_32) {
                    *var_34 = 4202609UL;
                    indirect_placeholder_6();
                } else {
                    *var_34 = 4202829UL;
                    indirect_placeholder_6();
                }
            }
            *(uint64_t *)(local_sp_1 + (-8L)) = 4202637UL;
            indirect_placeholder_6();
            *(uint64_t *)(local_sp_1 + (-16L)) = 4202652UL;
            indirect_placeholder_6();
            var_36 = helper_cc_compute_all_wrapper(0UL, 0UL, 0UL, 24U);
            if ((uint64_t)(((unsigned char)(var_36 >> 4UL) ^ (unsigned char)var_36) & '\xc0') == 0UL) {
                *(uint64_t *)(local_sp_1 + (-24L)) = 4202684UL;
                indirect_placeholder_6();
                *(uint64_t *)(local_sp_1 + (-32L)) = 4202710UL;
                indirect_placeholder_6();
                *(uint64_t *)(local_sp_1 + (-40L)) = 4202736UL;
                indirect_placeholder_6();
                *(uint64_t *)(local_sp_1 + (-48L)) = 4202746UL;
                indirect_placeholder_6();
            }
            return;
        }
    }
    var_37 = var_0 + (-120L);
    *(uint64_t *)var_37 = 4202841UL;
    indirect_placeholder_6();
    *var_14 = var_15;
    local_sp_3 = var_37;
}

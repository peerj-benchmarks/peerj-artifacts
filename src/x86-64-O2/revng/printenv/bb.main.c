typedef unsigned char __u_char;
typedef unsigned short __u_short;
typedef unsigned int __u_int;
typedef unsigned long __u_long;
typedef signed char __int8_t;
typedef unsigned char __uint8_t;
typedef short __int16_t;
typedef unsigned short __uint16_t;
typedef int __int32_t;
typedef unsigned int __uint32_t;
typedef long __int64_t;
typedef unsigned long __uint64_t;
typedef __int8_t __int_least8_t;
typedef __uint8_t __uint_least8_t;
typedef __int16_t __int_least16_t;
typedef __uint16_t __uint_least16_t;
typedef __int32_t __int_least32_t;
typedef __uint32_t __uint_least32_t;
typedef __int64_t __int_least64_t;
typedef __uint64_t __uint_least64_t;
typedef long __quad_t;
typedef unsigned long __u_quad_t;
typedef long __intmax_t;
typedef unsigned long __uintmax_t;
typedef unsigned long __dev_t;
typedef unsigned int __uid_t;
typedef unsigned int __gid_t;
typedef unsigned long __ino_t;
typedef unsigned long __ino64_t;
typedef unsigned int __mode_t;
typedef unsigned long __nlink_t;
typedef long __off_t;
typedef long __off64_t;
typedef int __pid_t;
typedef struct {
    int __val[2];
} __fsid_t;
typedef long __clock_t;
typedef unsigned long __rlim_t;
typedef unsigned long __rlim64_t;
typedef unsigned int __id_t;
typedef long __time_t;
typedef unsigned int __useconds_t;
typedef long __suseconds_t;
typedef int __daddr_t;
typedef int __key_t;
typedef int __clockid_t;
typedef void *__timer_t;
typedef long __blksize_t;
typedef long __blkcnt_t;
typedef long __blkcnt64_t;
typedef unsigned long __fsblkcnt_t;
typedef unsigned long __fsblkcnt64_t;
typedef unsigned long __fsfilcnt_t;
typedef unsigned long __fsfilcnt64_t;
typedef long __fsword_t;
typedef long __ssize_t;
typedef long __syscall_slong_t;
typedef unsigned long __syscall_ulong_t;
typedef __off64_t __loff_t;
typedef char *__caddr_t;
typedef long __intptr_t;
typedef unsigned int __socklen_t;
typedef int __sig_atomic_t;
typedef __int8_t int8_t;
typedef __int16_t int16_t;
typedef __int32_t int32_t;
typedef __int64_t int64_t;
typedef __uint8_t uint8_t;
typedef __uint16_t uint16_t;
typedef __uint32_t uint32_t;
typedef __uint64_t uint64_t;
typedef __int_least8_t int_least8_t;
typedef __int_least16_t int_least16_t;
typedef __int_least32_t int_least32_t;
typedef __int_least64_t int_least64_t;
typedef __uint_least8_t uint_least8_t;
typedef __uint_least16_t uint_least16_t;
typedef __uint_least32_t uint_least32_t;
typedef __uint_least64_t uint_least64_t;
typedef signed char int_fast8_t;
typedef long int_fast16_t;
typedef long int_fast32_t;
typedef long int_fast64_t;
typedef unsigned char uint_fast8_t;
typedef unsigned long uint_fast16_t;
typedef unsigned long uint_fast32_t;
typedef unsigned long uint_fast64_t;
typedef long intptr_t;
typedef unsigned long uintptr_t;
typedef __intmax_t intmax_t;
typedef __uintmax_t uintmax_t;
struct indirect_placeholder_39_ret_type;
struct indirect_placeholder_39_ret_type {
    uint64_t field_0;
    uint64_t field_1;
};
extern void abort(void);
extern uint64_t revng_init_local_sp(uint64_t param_0);
extern uint64_t init_rbx(void);
extern uint64_t init_rbp(void);
extern void indirect_placeholder_3(void);
extern uint64_t init_cc_src2(void);
extern uint64_t helper_cc_compute_c_wrapper(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint32_t param_3);
extern uint64_t init_r13(void);
extern uint64_t init_r14(void);
extern uint64_t init_r12(void);
extern uint64_t init_r15(void);
extern void indirect_placeholder_8(uint64_t param_0);
extern struct indirect_placeholder_39_ret_type indirect_placeholder_39(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint64_t param_3, uint64_t param_4);
extern void indirect_placeholder_31(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint64_t param_3, uint64_t param_4, uint64_t param_5, uint64_t param_6);
extern void indirect_placeholder_38(uint64_t param_0, uint64_t param_1, uint64_t param_2);
void bb_main(uint64_t rdi, uint64_t rsi) {
    uint64_t var_17;
    uint32_t var_18;
    uint64_t var_19;
    uint64_t var_20;
    uint64_t local_sp_4;
    uint64_t rbx_0;
    uint64_t cc_src2_3;
    uint64_t var_21;
    uint64_t var_22;
    uint64_t var_23;
    uint64_t var_24;
    uint64_t var_25;
    uint64_t rax_1;
    uint64_t var_11;
    uint64_t var_13;
    uint32_t var_14;
    struct indirect_placeholder_39_ret_type var_12;
    uint64_t var_0;
    uint64_t var_1;
    uint64_t var_2;
    uint64_t var_3;
    uint64_t var_4;
    uint64_t var_5;
    uint64_t var_6;
    uint64_t var_7;
    uint64_t var_8;
    uint64_t var_9;
    uint64_t var_10;
    uint64_t local_sp_6;
    uint64_t *var_27;
    uint64_t r9_1;
    uint64_t local_sp_1;
    uint64_t local_sp_0;
    uint64_t cc_src2_1;
    uint64_t r9_0;
    uint64_t r14_0;
    uint64_t cc_src2_0;
    uint64_t r15_0;
    uint64_t var_28;
    uint64_t local_sp_5;
    uint64_t rax_0;
    uint64_t rcx_0;
    uint64_t rdx_0;
    unsigned char var_29;
    uint64_t var_30;
    uint64_t var_31;
    uint64_t var_32;
    unsigned char var_33;
    uint64_t var_34;
    uint64_t var_35;
    uint64_t cc_src2_4;
    uint64_t var_26;
    uint32_t *var_36;
    uint64_t local_sp_2;
    uint64_t cc_src2_2;
    uint64_t var_37;
    uint64_t var_38;
    uint64_t local_sp_3;
    uint64_t rbp_0;
    uint64_t var_15;
    uint64_t var_16;
    unsigned int loop_state_var;
    var_0 = revng_init_local_sp(0UL);
    var_1 = init_rbx();
    var_2 = init_rbp();
    var_3 = init_r13();
    var_4 = init_cc_src2();
    var_5 = init_r14();
    var_6 = init_r15();
    var_7 = init_r12();
    *(uint64_t *)(var_0 + (-8L)) = var_6;
    *(uint64_t *)(var_0 + (-16L)) = var_5;
    *(uint64_t *)(var_0 + (-24L)) = var_3;
    *(uint64_t *)(var_0 + (-32L)) = var_7;
    *(uint64_t *)(var_0 + (-40L)) = var_2;
    *(uint64_t *)(var_0 + (-48L)) = var_1;
    var_8 = (uint64_t)(uint32_t)rdi;
    var_9 = *(uint64_t *)rsi;
    *(uint64_t *)(var_0 + (-80L)) = 4200589UL;
    indirect_placeholder_8(var_9);
    *(uint64_t *)(var_0 + (-88L)) = 4200604UL;
    indirect_placeholder_3();
    *(uint32_t *)6352040UL = 2U;
    var_10 = var_0 + (-96L);
    *(uint64_t *)var_10 = 4200624UL;
    indirect_placeholder_3();
    cc_src2_3 = var_4;
    local_sp_6 = var_10;
    r9_0 = 0UL;
    r14_0 = 0UL;
    cc_src2_4 = var_4;
    rbp_0 = 18446744073709551615UL;
    while (1U)
        {
            var_11 = local_sp_6 + (-8L);
            *(uint64_t *)var_11 = 4200647UL;
            var_12 = indirect_placeholder_39(4245632UL, var_8, 0UL, 4244786UL, rsi);
            var_13 = var_12.field_0;
            var_14 = (uint32_t)var_13;
            local_sp_4 = var_11;
            local_sp_6 = var_11;
            local_sp_5 = var_11;
            local_sp_3 = var_11;
            rbp_0 = 0UL;
            if ((uint64_t)(var_14 + 1U) == 0UL) {
                if ((uint64_t)(var_14 + 130U) != 0UL) {
                    loop_state_var = 0U;
                    break;
                }
                if ((uint64_t)(var_14 + (-48)) == 0UL) {
                    continue;
                }
                if ((uint64_t)(var_14 + 131U) == 0UL) {
                    *(uint64_t *)(local_sp_6 + (-16L)) = 4200686UL;
                    indirect_placeholder_38(var_8, 2UL, 1UL);
                    abort();
                }
                var_15 = *(uint64_t *)6352192UL;
                var_16 = *(uint64_t *)6352032UL;
                *(uint64_t *)(local_sp_6 + (-16L)) = var_13;
                *(uint64_t *)(local_sp_6 + (-24L)) = 0UL;
                *(uint64_t *)(local_sp_6 + (-32L)) = 4200732UL;
                indirect_placeholder_31(0UL, var_16, 4244753UL, var_15, 4244770UL, 4244712UL, 4244610UL);
                var_17 = local_sp_6 + (-40L);
                *(uint64_t *)var_17 = 4200739UL;
                indirect_placeholder_3();
                local_sp_3 = var_17;
                loop_state_var = 0U;
                break;
            }
            var_18 = *(uint32_t *)6352156UL;
            var_19 = (uint64_t)var_18;
            var_20 = rdi << 32UL;
            if ((long)var_20 <= (long)(var_19 << 32UL)) {
                rbx_0 = *(uint64_t *)6352256UL;
                loop_state_var = 2U;
                break;
            }
            var_24 = (uint64_t)var_18;
            *(uint32_t *)(local_sp_6 | 4UL) = 0U;
            var_25 = (var_24 << 3UL) + rsi;
            rax_1 = var_24;
            while (1U)
                {
                    var_26 = local_sp_5 + (-8L);
                    *(uint64_t *)var_26 = 4200869UL;
                    indirect_placeholder_3();
                    local_sp_0 = var_26;
                    cc_src2_0 = cc_src2_4;
                    local_sp_2 = var_26;
                    cc_src2_2 = cc_src2_4;
                    if (rax_1 != 0UL) {
                        var_27 = (uint64_t *)((r14_0 << 3UL) + var_25);
                        r15_0 = *(uint64_t *)6352256UL;
                        var_28 = *(uint64_t *)r15_0;
                        r9_1 = r9_0;
                        local_sp_1 = local_sp_0;
                        cc_src2_1 = cc_src2_0;
                        rcx_0 = var_28;
                        local_sp_2 = local_sp_0;
                        cc_src2_2 = cc_src2_0;
                        while (var_28 != 0UL)
                            {
                                rax_0 = (uint64_t)*(unsigned char *)var_28;
                                rdx_0 = *var_27;
                                while (1U)
                                    {
                                        if (rax_0 != 0UL) {
                                            loop_state_var = 0U;
                                            break;
                                        }
                                        var_29 = *(unsigned char *)rdx_0;
                                        if (var_29 != '\x00') {
                                            loop_state_var = 0U;
                                            break;
                                        }
                                        var_30 = rdx_0 + 1UL;
                                        var_31 = (uint64_t)(var_29 - (unsigned char)rax_0);
                                        var_32 = rcx_0 + 1UL;
                                        rcx_0 = var_32;
                                        rdx_0 = var_30;
                                        if (var_31 != 0UL) {
                                            loop_state_var = 0U;
                                            break;
                                        }
                                        var_33 = *(unsigned char *)var_32;
                                        rax_0 = (uint64_t)var_33;
                                        r9_1 = 1UL;
                                        if ((uint64_t)(var_33 + '\xc3') != 0UL) {
                                            continue;
                                        }
                                        if (*(unsigned char *)var_30 == '\x00') {
                                            continue;
                                        }
                                        loop_state_var = 1U;
                                        break;
                                    }
                                switch (loop_state_var) {
                                  case 1U:
                                    {
                                        var_34 = helper_cc_compute_c_wrapper(rbp_0, 1UL, cc_src2_0, 14U);
                                        var_35 = local_sp_0 + (-8L);
                                        *(uint64_t *)var_35 = 4200978UL;
                                        indirect_placeholder_3();
                                        local_sp_1 = var_35;
                                        cc_src2_1 = var_34;
                                    }
                                    break;
                                  case 0U:
                                    {
                                        local_sp_0 = local_sp_1;
                                        r9_0 = r9_1;
                                        cc_src2_0 = cc_src2_1;
                                        r15_0 = r15_0 + 8UL;
                                        var_28 = *(uint64_t *)r15_0;
                                        r9_1 = r9_0;
                                        local_sp_1 = local_sp_0;
                                        cc_src2_1 = cc_src2_0;
                                        rcx_0 = var_28;
                                        local_sp_2 = local_sp_0;
                                        cc_src2_2 = cc_src2_0;
                                        continue;
                                    }
                                    break;
                                }
                            }
                        var_36 = (uint32_t *)(local_sp_0 + 12UL);
                        *var_36 = (*var_36 + (uint32_t)r9_0);
                    }
                    var_37 = r14_0 + 1UL;
                    var_38 = var_37 + var_19;
                    local_sp_5 = local_sp_2;
                    cc_src2_4 = cc_src2_2;
                    r14_0 = var_37;
                    if ((long)var_20 > (long)(var_38 << 32UL)) {
                        break;
                    }
                    rax_1 = (uint64_t)(uint32_t)var_38;
                    continue;
                }
            loop_state_var = 1U;
            break;
        }
    switch (loop_state_var) {
      case 0U:
        {
            *(uint64_t *)(local_sp_3 + (-8L)) = 4200746UL;
            indirect_placeholder_38(var_8, 0UL, 1UL);
            abort();
        }
        break;
      case 2U:
      case 1U:
        {
            switch (loop_state_var) {
              case 2U:
                {
                    while (*(uint64_t *)rbx_0 != 0UL)
                        {
                            var_21 = helper_cc_compute_c_wrapper(rbp_0, 1UL, cc_src2_3, 14U);
                            var_22 = rbx_0 + 8UL;
                            var_23 = local_sp_4 + (-8L);
                            *(uint64_t *)var_23 = 4200792UL;
                            indirect_placeholder_3();
                            local_sp_4 = var_23;
                            rbx_0 = var_22;
                            cc_src2_3 = var_21;
                        }
                }
                break;
              case 1U:
                {
                    return;
                }
                break;
            }
        }
        break;
    }
}

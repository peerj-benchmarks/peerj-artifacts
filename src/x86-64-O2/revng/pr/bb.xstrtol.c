typedef unsigned char __u_char;
typedef unsigned short __u_short;
typedef unsigned int __u_int;
typedef unsigned long __u_long;
typedef signed char __int8_t;
typedef unsigned char __uint8_t;
typedef short __int16_t;
typedef unsigned short __uint16_t;
typedef int __int32_t;
typedef unsigned int __uint32_t;
typedef long __int64_t;
typedef unsigned long __uint64_t;
typedef __int8_t __int_least8_t;
typedef __uint8_t __uint_least8_t;
typedef __int16_t __int_least16_t;
typedef __uint16_t __uint_least16_t;
typedef __int32_t __int_least32_t;
typedef __uint32_t __uint_least32_t;
typedef __int64_t __int_least64_t;
typedef __uint64_t __uint_least64_t;
typedef long __quad_t;
typedef unsigned long __u_quad_t;
typedef long __intmax_t;
typedef unsigned long __uintmax_t;
typedef unsigned long __dev_t;
typedef unsigned int __uid_t;
typedef unsigned int __gid_t;
typedef unsigned long __ino_t;
typedef unsigned long __ino64_t;
typedef unsigned int __mode_t;
typedef unsigned long __nlink_t;
typedef long __off_t;
typedef long __off64_t;
typedef int __pid_t;
typedef struct {
    int __val[2];
} __fsid_t;
typedef long __clock_t;
typedef unsigned long __rlim_t;
typedef unsigned long __rlim64_t;
typedef unsigned int __id_t;
typedef long __time_t;
typedef unsigned int __useconds_t;
typedef long __suseconds_t;
typedef int __daddr_t;
typedef int __key_t;
typedef int __clockid_t;
typedef void *__timer_t;
typedef long __blksize_t;
typedef long __blkcnt_t;
typedef long __blkcnt64_t;
typedef unsigned long __fsblkcnt_t;
typedef unsigned long __fsblkcnt64_t;
typedef unsigned long __fsfilcnt_t;
typedef unsigned long __fsfilcnt64_t;
typedef long __fsword_t;
typedef long __ssize_t;
typedef long __syscall_slong_t;
typedef unsigned long __syscall_ulong_t;
typedef __off64_t __loff_t;
typedef char *__caddr_t;
typedef long __intptr_t;
typedef unsigned int __socklen_t;
typedef int __sig_atomic_t;
typedef __int8_t int8_t;
typedef __int16_t int16_t;
typedef __int32_t int32_t;
typedef __int64_t int64_t;
typedef __uint8_t uint8_t;
typedef __uint16_t uint16_t;
typedef __uint32_t uint32_t;
typedef __uint64_t uint64_t;
typedef __int_least8_t int_least8_t;
typedef __int_least16_t int_least16_t;
typedef __int_least32_t int_least32_t;
typedef __int_least64_t int_least64_t;
typedef __uint_least8_t uint_least8_t;
typedef __uint_least16_t uint_least16_t;
typedef __uint_least32_t uint_least32_t;
typedef __uint_least64_t uint_least64_t;
typedef signed char int_fast8_t;
typedef long int_fast16_t;
typedef long int_fast32_t;
typedef long int_fast64_t;
typedef unsigned char uint_fast8_t;
typedef unsigned long uint_fast16_t;
typedef unsigned long uint_fast32_t;
typedef unsigned long uint_fast64_t;
typedef long intptr_t;
typedef unsigned long uintptr_t;
typedef __intmax_t intmax_t;
typedef __uintmax_t uintmax_t;
extern void function_dispatcher(unsigned char *param_0);
extern uint64_t revng_init_local_sp(uint64_t param_0);
extern uint64_t init_rbx(void);
extern uint64_t init_rbp(void);
extern void indirect_placeholder(void);
extern uint64_t init_cc_src2(void);
extern uint64_t helper_cc_compute_c_wrapper(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint32_t param_3);
extern uint64_t init_r13(void);
extern uint64_t init_r12(void);
extern uint64_t init_r14(void);
extern uint64_t helper_cc_compute_all_wrapper(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint32_t param_3);
extern uint64_t init_r15(void);
uint64_t bb_xstrtol(uint64_t rcx, uint64_t rdi, uint64_t r8, uint64_t rdx, uint64_t rsi) {
    uint64_t var_0;
    uint64_t var_1;
    uint64_t var_2;
    uint64_t var_3;
    uint64_t var_4;
    uint64_t var_5;
    uint64_t var_6;
    uint64_t var_7;
    uint64_t local_sp_2;
    unsigned char var_27;
    uint64_t rbp_0;
    uint64_t rax_0;
    unsigned char var_19;
    uint64_t var_20;
    uint64_t var_21;
    uint64_t var_22;
    uint64_t var_23;
    uint64_t var_24;
    uint64_t var_25;
    uint64_t r12_0;
    uint64_t var_26;
    unsigned char var_28;
    uint64_t var_29;
    uint64_t local_sp_1;
    uint64_t var_30;
    uint64_t var_31;
    uint64_t local_sp_0;
    uint64_t rbp_2;
    uint64_t rbp_1;
    uint64_t var_18;
    uint64_t r12_1;
    uint64_t var_32;
    uint64_t rax_1;
    uint64_t var_15;
    uint64_t var_16;
    uint64_t var_13;
    uint32_t var_14;
    uint64_t var_17;
    uint64_t var_9;
    uint64_t var_10;
    uint32_t *var_11;
    uint64_t *var_12;
    uint64_t var_8;
    var_0 = revng_init_local_sp(0UL);
    var_1 = init_rbx();
    var_2 = init_rbp();
    var_3 = init_r13();
    var_4 = init_cc_src2();
    var_5 = init_r12();
    var_6 = init_r15();
    var_7 = init_r14();
    *(uint64_t *)(var_0 + (-8L)) = var_6;
    *(uint64_t *)(var_0 + (-16L)) = var_7;
    *(uint64_t *)(var_0 + (-24L)) = var_3;
    *(uint64_t *)(var_0 + (-32L)) = var_5;
    *(uint64_t *)(var_0 + (-40L)) = var_2;
    *(uint64_t *)(var_0 + (-48L)) = var_1;
    local_sp_2 = var_0 + (-88L);
    rbp_0 = 0UL;
    r12_0 = 1UL;
    rbp_1 = 0UL;
    rax_1 = 4UL;
    if ((uint64_t)(uint32_t)rdx > 36UL) {
        var_8 = var_0 + (-96L);
        *(uint64_t *)var_8 = 4231081UL;
        indirect_placeholder();
        local_sp_2 = var_8;
    }
    var_9 = local_sp_2 + 24UL;
    var_10 = (rsi == 0UL) ? var_9 : rsi;
    *(uint64_t *)(local_sp_2 + (-8L)) = 4230887UL;
    indirect_placeholder();
    var_11 = (uint32_t *)var_9;
    *var_11 = 0U;
    *(uint64_t *)(local_sp_2 + (-16L)) = 4230907UL;
    indirect_placeholder();
    var_12 = (uint64_t *)var_10;
    r12_1 = var_9;
    if (*var_12 == rdi) {
        if (r8 == 0UL) {
            return rax_1;
        }
        if (*(unsigned char *)rdi == '\x00') {
            return rax_1;
        }
        var_17 = local_sp_2 + (-24L);
        *(uint64_t *)var_17 = 4231046UL;
        indirect_placeholder();
        local_sp_1 = var_17;
        if (var_9 == 0UL) {
            return rax_1;
        }
        var_18 = *var_12;
        rbp_2 = rbp_1;
        r12_1 = r12_0;
        if (*(unsigned char *)var_18 != '\x00') {
            *(uint64_t *)(local_sp_1 + (-8L)) = 4231096UL;
            indirect_placeholder();
            if (var_18 == 0UL) {
                var_30 = (uint64_t)((uint32_t)rbp_1 & (-3));
                *(uint64_t *)rcx = r12_0;
                var_31 = var_30 | 2UL;
                rax_1 = var_31;
                return rax_1;
            }
            var_19 = **(unsigned char **)var_10;
            var_20 = (uint64_t)var_19;
            var_21 = (uint64_t)((uint32_t)var_20 + (-69));
            var_22 = var_21 + (-47L);
            rax_0 = var_20;
            var_23 = 142129060940101UL >> (var_21 & 63UL);
            var_24 = helper_cc_compute_all_wrapper(var_22, 47UL, var_4, 14U);
            var_25 = helper_cc_compute_c_wrapper(var_22, (var_24 & (-2L)) | (var_23 & 1UL), var_4, 1U);
            if ((uint64_t)((unsigned char)var_21 & '\xf0') <= 47UL & var_25 != 0UL) {
                *(uint32_t *)(local_sp_1 + 4UL) = 1024U;
                *(uint64_t *)(local_sp_1 + (-16L)) = 4231201UL;
                indirect_placeholder();
                var_26 = *var_12;
                if (var_19 == '\x00') {
                    rax_0 = (uint64_t)*(unsigned char *)var_26;
                } else {
                    var_27 = *(unsigned char *)(var_26 + 1UL);
                    if ((uint64_t)(var_27 + '\xbc') == 0UL) {
                        rax_0 = (uint64_t)*(unsigned char *)var_26;
                    } else {
                        if ((uint64_t)(var_27 + '\x97') == 0UL) {
                            rax_0 = (uint64_t)*(unsigned char *)var_26;
                        } else {
                            if ((uint64_t)(var_27 + '\xbe') == 0UL) {
                                rax_0 = (uint64_t)*(unsigned char *)var_26;
                            } else {
                                rax_0 = (uint64_t)*(unsigned char *)var_26;
                            }
                        }
                    }
                }
            }
            var_28 = (unsigned char)rax_0 + '\xbe';
            var_29 = (uint64_t)var_28;
            if ((uint64_t)(var_28 & '\xfe') > 53UL) {
                function_dispatcher((unsigned char *)(0UL));
                return var_29;
            }
        }
    }
    var_13 = local_sp_2 + (-24L);
    *(uint64_t *)var_13 = 4230920UL;
    indirect_placeholder();
    var_14 = *var_11;
    local_sp_0 = var_13;
    r12_0 = var_9;
    if (var_14 != 0U) {
        var_15 = (uint64_t)var_14;
        var_16 = local_sp_2 + (-32L);
        *(uint64_t *)var_16 = 4230981UL;
        indirect_placeholder();
        rbp_0 = 1UL;
        local_sp_0 = var_16;
        if (*(uint32_t *)var_15 == 34U) {
            return rax_1;
        }
    }
    rbp_1 = rbp_0;
    local_sp_1 = local_sp_0;
    rbp_2 = rbp_0;
    if (r8 == 0UL) {
        *(uint64_t *)rcx = r12_1;
        var_32 = (uint64_t)(uint32_t)rbp_2;
        rax_1 = var_32;
        return rax_1;
    }
    var_18 = *var_12;
    rbp_2 = rbp_1;
    r12_1 = r12_0;
    if (*(unsigned char *)var_18 != '\x00') {
        return;
    }
    *(uint64_t *)(local_sp_1 + (-8L)) = 4231096UL;
    indirect_placeholder();
    if (var_18 == 0UL) {
        var_30 = (uint64_t)((uint32_t)rbp_1 & (-3));
        *(uint64_t *)rcx = r12_0;
        var_31 = var_30 | 2UL;
        rax_1 = var_31;
        return rax_1;
    }
    var_19 = **(unsigned char **)var_10;
    var_20 = (uint64_t)var_19;
    var_21 = (uint64_t)((uint32_t)var_20 + (-69));
    var_22 = var_21 + (-47L);
    rax_0 = var_20;
    var_23 = 142129060940101UL >> (var_21 & 63UL);
    var_24 = helper_cc_compute_all_wrapper(var_22, 47UL, var_4, 14U);
    var_25 = helper_cc_compute_c_wrapper(var_22, (var_24 & (-2L)) | (var_23 & 1UL), var_4, 1U);
    if ((uint64_t)((unsigned char)var_21 & '\xf0') <= 47UL & var_25 != 0UL) {
        *(uint32_t *)(local_sp_1 + 4UL) = 1024U;
        *(uint64_t *)(local_sp_1 + (-16L)) = 4231201UL;
        indirect_placeholder();
        var_26 = *var_12;
        if (var_19 == '\x00') {
            rax_0 = (uint64_t)*(unsigned char *)var_26;
        } else {
            var_27 = *(unsigned char *)(var_26 + 1UL);
            if ((uint64_t)(var_27 + '\xbc') == 0UL) {
                rax_0 = (uint64_t)*(unsigned char *)var_26;
            } else {
                if ((uint64_t)(var_27 + '\x97') == 0UL) {
                    rax_0 = (uint64_t)*(unsigned char *)var_26;
                } else {
                    if ((uint64_t)(var_27 + '\xbe') == 0UL) {
                        rax_0 = (uint64_t)*(unsigned char *)var_26;
                    } else {
                        rax_0 = (uint64_t)*(unsigned char *)var_26;
                    }
                }
            }
        }
    }
    var_28 = (unsigned char)rax_0 + '\xbe';
    var_29 = (uint64_t)var_28;
    if ((uint64_t)(var_28 & '\xfe') > 53UL) {
        function_dispatcher((unsigned char *)(0UL));
        return var_29;
    }
}

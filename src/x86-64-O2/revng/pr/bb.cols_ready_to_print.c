typedef unsigned char __u_char;
typedef unsigned short __u_short;
typedef unsigned int __u_int;
typedef unsigned long __u_long;
typedef signed char __int8_t;
typedef unsigned char __uint8_t;
typedef short __int16_t;
typedef unsigned short __uint16_t;
typedef int __int32_t;
typedef unsigned int __uint32_t;
typedef long __int64_t;
typedef unsigned long __uint64_t;
typedef __int8_t __int_least8_t;
typedef __uint8_t __uint_least8_t;
typedef __int16_t __int_least16_t;
typedef __uint16_t __uint_least16_t;
typedef __int32_t __int_least32_t;
typedef __uint32_t __uint_least32_t;
typedef __int64_t __int_least64_t;
typedef __uint64_t __uint_least64_t;
typedef long __quad_t;
typedef unsigned long __u_quad_t;
typedef long __intmax_t;
typedef unsigned long __uintmax_t;
typedef unsigned long __dev_t;
typedef unsigned int __uid_t;
typedef unsigned int __gid_t;
typedef unsigned long __ino_t;
typedef unsigned long __ino64_t;
typedef unsigned int __mode_t;
typedef unsigned long __nlink_t;
typedef long __off_t;
typedef long __off64_t;
typedef int __pid_t;
typedef struct {
    int __val[2];
} __fsid_t;
typedef long __clock_t;
typedef unsigned long __rlim_t;
typedef unsigned long __rlim64_t;
typedef unsigned int __id_t;
typedef long __time_t;
typedef unsigned int __useconds_t;
typedef long __suseconds_t;
typedef int __daddr_t;
typedef int __key_t;
typedef int __clockid_t;
typedef void *__timer_t;
typedef long __blksize_t;
typedef long __blkcnt_t;
typedef long __blkcnt64_t;
typedef unsigned long __fsblkcnt_t;
typedef unsigned long __fsblkcnt64_t;
typedef unsigned long __fsfilcnt_t;
typedef unsigned long __fsfilcnt64_t;
typedef long __fsword_t;
typedef long __ssize_t;
typedef long __syscall_slong_t;
typedef unsigned long __syscall_ulong_t;
typedef __off64_t __loff_t;
typedef char *__caddr_t;
typedef long __intptr_t;
typedef unsigned int __socklen_t;
typedef int __sig_atomic_t;
typedef __int8_t int8_t;
typedef __int16_t int16_t;
typedef __int32_t int32_t;
typedef __int64_t int64_t;
typedef __uint8_t uint8_t;
typedef __uint16_t uint16_t;
typedef __uint32_t uint32_t;
typedef __uint64_t uint64_t;
typedef __int_least8_t int_least8_t;
typedef __int_least16_t int_least16_t;
typedef __int_least32_t int_least32_t;
typedef __int_least64_t int_least64_t;
typedef __uint_least8_t uint_least8_t;
typedef __uint_least16_t uint_least16_t;
typedef __uint_least32_t uint_least32_t;
typedef __uint_least64_t uint_least64_t;
typedef signed char int_fast8_t;
typedef long int_fast16_t;
typedef long int_fast32_t;
typedef long int_fast64_t;
typedef unsigned char uint_fast8_t;
typedef unsigned long uint_fast16_t;
typedef unsigned long uint_fast32_t;
typedef unsigned long uint_fast64_t;
typedef long intptr_t;
typedef unsigned long uintptr_t;
typedef __intmax_t intmax_t;
typedef __uintmax_t uintmax_t;
struct bb_cols_ready_to_print_ret_type;
struct bb_cols_ready_to_print_ret_type {
    uint64_t field_0;
    uint64_t field_1;
    uint64_t field_2;
};
extern uint64_t revng_init_local_sp(uint64_t param_0);
extern uint64_t helper_cc_compute_all_wrapper(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint32_t param_3);
extern uint64_t init_r8(void);
extern uint64_t init_rsi(void);
struct bb_cols_ready_to_print_ret_type bb_cols_ready_to_print(void) {
    struct bb_cols_ready_to_print_ret_type mrv;
    struct bb_cols_ready_to_print_ret_type mrv1;
    struct bb_cols_ready_to_print_ret_type mrv2;
    uint64_t var_0;
    uint32_t var_1;
    uint64_t var_2;
    uint64_t r8_2;
    unsigned char var_3;
    uint64_t var_4;
    uint64_t var_5;
    uint64_t rax_0;
    uint64_t r8_0;
    uint64_t rdx_0;
    uint64_t r8_1;
    uint64_t rax_1;
    uint64_t var_6;
    uint64_t var_7;
    uint64_t var_8;
    uint64_t var_9;
    struct bb_cols_ready_to_print_ret_type mrv4 = {0UL, /*implicit*/(int)0, /*implicit*/(int)0};
    struct bb_cols_ready_to_print_ret_type mrv5;
    revng_init_local_sp(0UL);
    var_0 = init_r8();
    var_1 = *(uint32_t *)6402216UL;
    var_2 = *(uint64_t *)6402824UL;
    rax_0 = 0UL;
    r8_0 = var_0;
    if (var_1 == 0U) {
        var_9 = init_rsi();
        mrv4.field_1 = var_0;
        mrv5 = mrv4;
        mrv5.field_2 = var_9;
        return mrv5;
    }
    var_3 = *(unsigned char *)6402244UL;
    var_4 = (uint64_t)var_3;
    var_5 = ((uint64_t)(var_1 + (-1)) << 6UL) + var_2;
    rdx_0 = var_2 + 16UL;
    while (1U)
        {
            r8_1 = r8_0;
            rax_1 = rax_0;
            r8_2 = r8_0;
            if (*(uint32_t *)rdx_0 > 1U) {
                rax_1 = (uint64_t)((uint32_t)rax_0 + 1U);
                r8_2 = r8_1;
            } else {
                var_6 = (uint64_t)*(uint32_t *)(rdx_0 + 28UL);
                var_7 = helper_cc_compute_all_wrapper(var_6, 0UL, 0UL, 24U);
                r8_1 = var_6;
                r8_2 = var_6;
                var_8 = helper_cc_compute_all_wrapper((uint64_t)*(uint32_t *)(rdx_0 + 32UL), 0UL, 0UL, 24U);
                if (var_3 != '\x00' & (uint64_t)(((unsigned char)(var_7 >> 4UL) ^ (unsigned char)var_7) & '\xc0') != 0UL & (uint64_t)(((unsigned char)(var_8 >> 4UL) ^ (unsigned char)var_8) & '\xc0') == 0UL) {
                    rax_1 = (uint64_t)((uint32_t)rax_0 + 1U);
                    r8_2 = r8_1;
                }
            }
            rax_0 = rax_1;
            r8_0 = r8_2;
            if (rdx_0 == (var_5 + 16UL)) {
                break;
            }
            rdx_0 = rdx_0 + 64UL;
            continue;
        }
    mrv.field_0 = rax_1;
    mrv1 = mrv;
    mrv1.field_1 = r8_2;
    mrv2 = mrv1;
    mrv2.field_2 = var_4;
    return mrv2;
}

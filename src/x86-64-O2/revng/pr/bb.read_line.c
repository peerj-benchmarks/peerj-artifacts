typedef unsigned char __u_char;
typedef unsigned short __u_short;
typedef unsigned int __u_int;
typedef unsigned long __u_long;
typedef signed char __int8_t;
typedef unsigned char __uint8_t;
typedef short __int16_t;
typedef unsigned short __uint16_t;
typedef int __int32_t;
typedef unsigned int __uint32_t;
typedef long __int64_t;
typedef unsigned long __uint64_t;
typedef __int8_t __int_least8_t;
typedef __uint8_t __uint_least8_t;
typedef __int16_t __int_least16_t;
typedef __uint16_t __uint_least16_t;
typedef __int32_t __int_least32_t;
typedef __uint32_t __uint_least32_t;
typedef __int64_t __int_least64_t;
typedef __uint64_t __uint_least64_t;
typedef long __quad_t;
typedef unsigned long __u_quad_t;
typedef long __intmax_t;
typedef unsigned long __uintmax_t;
typedef unsigned long __dev_t;
typedef unsigned int __uid_t;
typedef unsigned int __gid_t;
typedef unsigned long __ino_t;
typedef unsigned long __ino64_t;
typedef unsigned int __mode_t;
typedef unsigned long __nlink_t;
typedef long __off_t;
typedef long __off64_t;
typedef int __pid_t;
typedef struct {
    int __val[2];
} __fsid_t;
typedef long __clock_t;
typedef unsigned long __rlim_t;
typedef unsigned long __rlim64_t;
typedef unsigned int __id_t;
typedef long __time_t;
typedef unsigned int __useconds_t;
typedef long __suseconds_t;
typedef int __daddr_t;
typedef int __key_t;
typedef int __clockid_t;
typedef void *__timer_t;
typedef long __blksize_t;
typedef long __blkcnt_t;
typedef long __blkcnt64_t;
typedef unsigned long __fsblkcnt_t;
typedef unsigned long __fsblkcnt64_t;
typedef unsigned long __fsfilcnt_t;
typedef unsigned long __fsfilcnt64_t;
typedef long __fsword_t;
typedef long __ssize_t;
typedef long __syscall_slong_t;
typedef unsigned long __syscall_ulong_t;
typedef __off64_t __loff_t;
typedef char *__caddr_t;
typedef long __intptr_t;
typedef unsigned int __socklen_t;
typedef int __sig_atomic_t;
typedef __int8_t int8_t;
typedef __int16_t int16_t;
typedef __int32_t int32_t;
typedef __int64_t int64_t;
typedef __uint8_t uint8_t;
typedef __uint16_t uint16_t;
typedef __uint32_t uint32_t;
typedef __uint64_t uint64_t;
typedef __int_least8_t int_least8_t;
typedef __int_least16_t int_least16_t;
typedef __int_least32_t int_least32_t;
typedef __int_least64_t int_least64_t;
typedef __uint_least8_t uint_least8_t;
typedef __uint_least16_t uint_least16_t;
typedef __uint_least32_t uint_least32_t;
typedef __uint_least64_t uint_least64_t;
typedef signed char int_fast8_t;
typedef long int_fast16_t;
typedef long int_fast32_t;
typedef long int_fast64_t;
typedef unsigned char uint_fast8_t;
typedef unsigned long uint_fast16_t;
typedef unsigned long uint_fast32_t;
typedef unsigned long uint_fast64_t;
typedef long intptr_t;
typedef unsigned long uintptr_t;
typedef __intmax_t intmax_t;
typedef __uintmax_t uintmax_t;
typedef _Bool bool;
struct indirect_placeholder_54_ret_type;
struct indirect_placeholder_55_ret_type;
struct indirect_placeholder_56_ret_type;
struct indirect_placeholder_57_ret_type;
struct indirect_placeholder_58_ret_type;
struct indirect_placeholder_59_ret_type;
struct indirect_placeholder_54_ret_type {
    uint64_t field_0;
    uint64_t field_1;
    uint64_t field_2;
    uint64_t field_3;
    uint64_t field_4;
};
struct indirect_placeholder_55_ret_type {
    uint64_t field_0;
    uint64_t field_1;
    uint64_t field_2;
    uint64_t field_3;
    uint64_t field_4;
    uint64_t field_5;
};
struct indirect_placeholder_56_ret_type {
    uint64_t field_0;
    uint64_t field_1;
    uint64_t field_2;
    uint64_t field_3;
    uint64_t field_4;
    uint64_t field_5;
};
struct indirect_placeholder_57_ret_type {
    uint64_t field_0;
    uint64_t field_1;
    uint64_t field_2;
    uint64_t field_3;
    uint64_t field_4;
};
struct indirect_placeholder_58_ret_type {
    uint64_t field_0;
    uint64_t field_1;
    uint64_t field_2;
    uint64_t field_3;
};
struct indirect_placeholder_59_ret_type {
    uint64_t field_0;
    uint64_t field_1;
    uint64_t field_2;
    uint64_t field_3;
};
extern uint64_t revng_init_local_sp(uint64_t param_0);
extern uint64_t init_rbx(void);
extern uint64_t init_rbp(void);
extern void indirect_placeholder(void);
extern uint64_t init_r13(void);
extern uint64_t init_r12(void);
extern uint64_t init_r14(void);
extern uint64_t indirect_placeholder_3(uint64_t param_0);
extern uint64_t init_rax(void);
extern uint64_t helper_cc_compute_all_wrapper(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint32_t param_3);
extern uint64_t init_r15(void);
extern uint64_t init_r8(void);
extern void indirect_placeholder_4(uint64_t param_0);
extern void indirect_placeholder_23(uint64_t param_0, uint64_t param_1);
extern uint64_t init_r9(void);
extern uint64_t init_r10(void);
extern uint64_t init_rsi(void);
extern struct indirect_placeholder_54_ret_type indirect_placeholder_54(void);
extern void indirect_placeholder_30(uint64_t param_0, uint64_t param_1, uint64_t param_2);
extern struct indirect_placeholder_55_ret_type indirect_placeholder_55(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint64_t param_3, uint64_t param_4);
extern struct indirect_placeholder_56_ret_type indirect_placeholder_56(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint64_t param_3, uint64_t param_4);
extern struct indirect_placeholder_57_ret_type indirect_placeholder_57(void);
extern struct indirect_placeholder_58_ret_type indirect_placeholder_58(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint64_t param_3, uint64_t param_4);
extern struct indirect_placeholder_59_ret_type indirect_placeholder_59(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint64_t param_3, uint64_t param_4);
uint64_t bb_read_line(uint64_t rdi) {
    struct indirect_placeholder_59_ret_type var_43;
    struct indirect_placeholder_58_ret_type var_35;
    struct indirect_placeholder_56_ret_type var_24;
    struct indirect_placeholder_55_ret_type var_56;
    uint64_t var_0;
    uint64_t var_1;
    uint64_t var_2;
    uint64_t var_3;
    uint64_t var_4;
    uint64_t var_5;
    uint64_t var_6;
    uint64_t var_7;
    uint64_t var_8;
    uint64_t var_9;
    uint64_t var_10;
    uint64_t var_11;
    uint64_t var_12;
    uint32_t var_13;
    uint32_t var_14;
    bool var_15;
    uint64_t var_19;
    uint64_t _pre119;
    uint64_t var_68;
    uint64_t local_sp_8;
    uint64_t rax_1;
    uint64_t var_69;
    uint64_t _pre_phi120;
    uint64_t local_sp_0;
    unsigned char var_70;
    uint64_t var_71;
    struct indirect_placeholder_54_ret_type var_72;
    uint64_t var_73;
    uint64_t local_sp_2;
    uint64_t local_sp_1;
    uint64_t r10_6;
    uint64_t rax_0;
    uint64_t r8_6;
    uint64_t rbp_0;
    uint64_t rbp_1;
    uint64_t var_74;
    uint64_t var_75;
    uint64_t local_sp_3;
    uint64_t local_sp_15_be;
    uint64_t rbx_4_be;
    uint64_t local_sp_15;
    uint64_t var_62;
    uint32_t var_63;
    uint64_t var_64;
    uint64_t rbx_0;
    uint64_t var_65;
    uint64_t var_66;
    uint64_t local_sp_5;
    uint64_t rax_3;
    uint64_t var_53;
    uint32_t var_54;
    uint64_t var_55;
    uint64_t rbx_4;
    uint64_t rbp_4;
    uint64_t var_57;
    uint64_t var_58;
    uint64_t var_59;
    uint64_t var_60;
    uint64_t var_61;
    uint64_t var_67;
    unsigned char *_pre_phi128;
    uint64_t _pre_phi124;
    uint64_t local_sp_4;
    uint64_t var_20;
    bool var_21;
    uint64_t local_sp_6;
    uint64_t local_sp_10;
    uint64_t var_22;
    uint64_t var_23;
    uint64_t r9_6;
    uint64_t rbx_2;
    uint64_t rbp_2;
    uint64_t r10_0;
    uint64_t r9_0;
    uint64_t rbp_3;
    uint64_t r8_0;
    uint64_t r13_0;
    uint64_t rsi_0;
    uint64_t local_sp_13;
    uint64_t rax_2;
    uint64_t var_25;
    uint64_t local_sp_12;
    unsigned char var_26;
    uint64_t local_sp_7;
    uint64_t var_27;
    struct indirect_placeholder_57_ret_type var_28;
    uint64_t r10_1;
    uint64_t r9_1;
    uint64_t r8_1;
    uint64_t rsi_1;
    uint64_t var_29;
    uint64_t var_30;
    uint64_t var_31;
    uint64_t local_sp_9;
    uint64_t var_32;
    uint64_t r10_2;
    uint64_t r9_2;
    uint64_t r8_2;
    uint64_t rsi_2;
    uint64_t r12_0;
    uint64_t r14_0;
    uint64_t var_33;
    uint64_t var_34;
    uint64_t r10_3;
    uint64_t r9_3;
    uint64_t r8_3;
    uint64_t rsi_3;
    unsigned char var_36;
    uint32_t var_37;
    uint32_t storemerge;
    uint64_t r10_4;
    uint64_t r9_4;
    uint32_t rdi1_0_in;
    uint64_t r8_4;
    uint64_t rsi_4;
    uint64_t rdi1_0;
    uint32_t var_38;
    uint64_t local_sp_11;
    uint64_t rdi1_1;
    uint64_t var_42;
    uint64_t var_39;
    uint64_t var_40;
    uint64_t var_41;
    uint64_t r10_5;
    uint64_t r9_5;
    uint64_t r8_5;
    uint64_t var_45;
    uint32_t var_46;
    uint64_t var_47;
    uint64_t var_48;
    uint64_t local_sp_15_ph;
    uint64_t local_sp_14;
    uint64_t rbx_3;
    uint64_t var_49;
    uint64_t var_50;
    uint64_t rbx_4_ph;
    uint64_t var_51;
    uint32_t var_52;
    uint64_t var_44;
    unsigned char *var_16;
    uint64_t local_sp_16;
    uint64_t var_17;
    uint64_t var_18;
    unsigned int loop_state_var;
    var_0 = revng_init_local_sp(0UL);
    var_1 = init_rax();
    var_2 = init_r10();
    var_3 = init_rbx();
    var_4 = init_r9();
    var_5 = init_rbp();
    var_6 = init_r8();
    var_7 = init_r13();
    var_8 = init_rsi();
    var_9 = init_r12();
    var_10 = init_r15();
    var_11 = init_r14();
    *(uint64_t *)(var_0 + (-8L)) = var_10;
    *(uint64_t *)(var_0 + (-16L)) = var_11;
    *(uint64_t *)(var_0 + (-24L)) = var_7;
    *(uint64_t *)(var_0 + (-32L)) = var_9;
    *(uint64_t *)(var_0 + (-40L)) = var_5;
    *(uint64_t *)(var_0 + (-48L)) = var_3;
    var_12 = var_0 + (-64L);
    *(uint64_t *)var_12 = 4208313UL;
    indirect_placeholder();
    var_13 = (uint32_t)var_1;
    var_14 = *(uint32_t *)6402740UL;
    var_15 = ((uint64_t)(var_13 + (-12)) == 0UL);
    rax_0 = var_1;
    rbp_0 = rdi;
    rbp_1 = rdi;
    local_sp_4 = var_12;
    rbp_2 = rdi;
    r10_0 = var_2;
    r9_0 = var_4;
    rbp_3 = rdi;
    r8_0 = var_6;
    r13_0 = var_7;
    rsi_0 = var_8;
    rax_2 = 1UL;
    r12_0 = 1UL;
    storemerge = 0U;
    local_sp_16 = var_12;
    if (var_15) {
        _pre_phi128 = (unsigned char *)(rdi + 57UL);
        _pre_phi124 = (uint64_t)(var_13 + (-10));
    } else {
        var_16 = (unsigned char *)(rdi + 57UL);
        _pre_phi128 = var_16;
        if (*var_16 != '\x00') {
            _pre119 = (uint64_t)(var_13 + (-10));
            _pre_phi120 = _pre119;
            var_68 = local_sp_16 + (-8L);
            *(uint64_t *)var_68 = 4208747UL;
            indirect_placeholder();
            local_sp_0 = var_68;
            if (_pre_phi120 == 0UL) {
                var_69 = local_sp_16 + (-16L);
                *(uint64_t *)var_69 = 4208763UL;
                indirect_placeholder();
                local_sp_0 = var_69;
            }
            var_70 = *(unsigned char *)6402776UL;
            *(unsigned char *)6402780UL = (unsigned char)'\x01';
            local_sp_1 = local_sp_0;
            if (var_70 == '\x00') {
                if (*(unsigned char *)6402244UL == '\x00') {
                    *(unsigned char *)6402656UL = (unsigned char)'\x01';
                    var_71 = local_sp_0 + (-8L);
                    *(uint64_t *)var_71 = 4209172UL;
                    var_72 = indirect_placeholder_54();
                    var_73 = var_72.field_0;
                    local_sp_2 = var_71;
                    rax_1 = var_73;
                } else {
                    local_sp_2 = local_sp_1;
                    rax_1 = rax_0;
                    rbp_1 = rbp_0;
                    if (*(unsigned char *)6402778UL == '\x00') {
                        *(unsigned char *)6402777UL = (unsigned char)'\x01';
                    }
                }
            } else {
                local_sp_2 = local_sp_1;
                rax_1 = rax_0;
                rbp_1 = rbp_0;
                if (*(unsigned char *)6402778UL == '\x00') {
                    *(unsigned char *)6402777UL = (unsigned char)'\x01';
                }
            }
            var_74 = rbp_1 + 48UL;
            var_75 = rbp_1 + 16UL;
            *(uint64_t *)(local_sp_2 + (-8L)) = 4208821UL;
            indirect_placeholder_30(rax_1, var_75, var_74);
            return 1UL;
        }
        var_17 = var_0 + (-72L);
        *(uint64_t *)var_17 = 4208985UL;
        indirect_placeholder();
        var_18 = (uint64_t)(var_13 + (-10));
        _pre_phi124 = var_18;
        local_sp_4 = var_17;
        if (var_18 == 0UL) {
            var_19 = var_0 + (-80L);
            *(uint64_t *)var_19 = 4209005UL;
            indirect_placeholder();
            _pre_phi124 = 0UL;
            local_sp_4 = var_19;
        }
    }
    var_20 = (uint64_t)var_13;
    *_pre_phi128 = (unsigned char)'\x00';
    var_21 = (_pre_phi124 == 0UL);
    _pre_phi120 = _pre_phi124;
    local_sp_5 = local_sp_4;
    local_sp_6 = local_sp_4;
    rbx_2 = var_20;
    local_sp_16 = local_sp_4;
    if (!var_21) {
        if (!var_15) {
            var_68 = local_sp_16 + (-8L);
            *(uint64_t *)var_68 = 4208747UL;
            indirect_placeholder();
            local_sp_0 = var_68;
            if (_pre_phi120 == 0UL) {
                var_69 = local_sp_16 + (-16L);
                *(uint64_t *)var_69 = 4208763UL;
                indirect_placeholder();
                local_sp_0 = var_69;
            }
            var_70 = *(unsigned char *)6402776UL;
            *(unsigned char *)6402780UL = (unsigned char)'\x01';
            local_sp_1 = local_sp_0;
            if (var_70 == '\x00') {
                if (*(unsigned char *)6402244UL == '\x00') {
                    *(unsigned char *)6402656UL = (unsigned char)'\x01';
                    var_71 = local_sp_0 + (-8L);
                    *(uint64_t *)var_71 = 4209172UL;
                    var_72 = indirect_placeholder_54();
                    var_73 = var_72.field_0;
                    local_sp_2 = var_71;
                    rax_1 = var_73;
                } else {
                    local_sp_2 = local_sp_1;
                    rax_1 = rax_0;
                    rbp_1 = rbp_0;
                    if (*(unsigned char *)6402778UL == '\x00') {
                        *(unsigned char *)6402777UL = (unsigned char)'\x01';
                    }
                }
            } else {
                local_sp_2 = local_sp_1;
                rax_1 = rax_0;
                rbp_1 = rbp_0;
                if (*(unsigned char *)6402778UL == '\x00') {
                    *(unsigned char *)6402777UL = (unsigned char)'\x01';
                }
            }
            var_74 = rbp_1 + 48UL;
            var_75 = rbp_1 + 16UL;
            *(uint64_t *)(local_sp_2 + (-8L)) = 4208821UL;
            indirect_placeholder_30(rax_1, var_75, var_74);
            return 1UL;
        }
        if ((uint64_t)(var_13 + 1U) != 0UL) {
            *(uint64_t *)(local_sp_5 + (-8L)) = 4208704UL;
            indirect_placeholder_23(rbx_2, rbp_2);
            return rax_2;
        }
        var_22 = (uint64_t)(uint32_t)((int)(var_13 << 24U) >> (int)24U);
        var_23 = local_sp_4 + (-8L);
        *(uint64_t *)var_23 = 4208366UL;
        var_24 = indirect_placeholder_56(var_2, var_4, var_22, rdi, var_6);
        local_sp_6 = var_23;
        r10_0 = var_24.field_1;
        r9_0 = var_24.field_2;
        rbp_3 = var_24.field_3;
        r8_0 = var_24.field_4;
        r13_0 = (uint64_t)(uint32_t)var_24.field_0;
        rsi_0 = var_24.field_5;
    }
    rbp_4 = rbp_3;
    rax_2 = 0UL;
    local_sp_12 = local_sp_6;
    local_sp_7 = local_sp_6;
    r10_1 = r10_0;
    r9_1 = r9_0;
    r8_1 = r8_0;
    rsi_1 = rsi_0;
    r10_5 = r10_0;
    r9_5 = r9_0;
    r8_5 = r8_0;
    if (*(unsigned char *)6402765UL != '\x00') {
        if ((int)*(uint32_t *)6402760UL >= (int)*(uint32_t *)6402740UL) {
            *(uint32_t *)6402740UL = var_14;
            return rax_2;
        }
    }
    var_25 = rbp_3 + 32UL;
    if (*(uint64_t *)var_25 != 4204800UL) {
        var_26 = *(unsigned char *)6402776UL;
        *(unsigned char *)6402656UL = (unsigned char)'\x01';
        if (var_26 != '\x00' & *(unsigned char *)6402244UL == '\x00') {
            var_27 = local_sp_6 + (-8L);
            *(uint64_t *)var_27 = 4208422UL;
            var_28 = indirect_placeholder_57();
            local_sp_7 = var_27;
            r10_1 = var_28.field_1;
            r9_1 = var_28.field_2;
            r8_1 = var_28.field_3;
            rsi_1 = var_28.field_4;
        }
        local_sp_8 = local_sp_7;
        local_sp_10 = local_sp_7;
        local_sp_9 = local_sp_7;
        r10_2 = r10_1;
        r9_2 = r9_1;
        r8_2 = r8_1;
        rsi_2 = rsi_1;
        r10_3 = r10_1;
        r9_3 = r9_1;
        r8_3 = r8_1;
        rsi_3 = rsi_1;
        r10_4 = r10_1;
        r9_4 = r9_1;
        r8_4 = r8_1;
        rsi_4 = rsi_1;
        if (*(unsigned char *)6402783UL == '\x00') {
            rdi1_0_in = *(uint32_t *)6402660UL;
        } else {
            if (*(unsigned char *)6402782UL == '\x00') {
                rdi1_0_in = *(uint32_t *)6402660UL;
            } else {
                var_29 = (uint64_t)*(uint32_t *)6402664UL;
                var_30 = *(uint64_t *)6402824UL;
                *(uint32_t *)6402664UL = 0U;
                var_31 = helper_cc_compute_all_wrapper(var_29, 0UL, 0UL, 24U);
                r14_0 = var_30;
                if ((uint64_t)(((unsigned char)(var_31 >> 4UL) ^ (unsigned char)var_31) & '\xc0') != 0UL) {
                    var_32 = var_29 << 32UL;
                    var_33 = r12_0 + 1UL;
                    var_34 = local_sp_8 + (-8L);
                    *(uint64_t *)var_34 = 4208912UL;
                    var_35 = indirect_placeholder_58(r10_2, r9_2, r14_0, r8_2, rsi_2);
                    *(uint32_t *)6402664UL = (*(uint32_t *)6402664UL + 1U);
                    local_sp_8 = var_34;
                    local_sp_9 = var_34;
                    while ((long)var_32 >= (long)(var_33 << 32UL))
                        {
                            r10_2 = var_35.field_0;
                            r9_2 = var_35.field_1;
                            r8_2 = var_35.field_2;
                            rsi_2 = var_35.field_3;
                            r12_0 = (uint64_t)(uint32_t)var_33;
                            r14_0 = r14_0 + 64UL;
                            var_33 = r12_0 + 1UL;
                            var_34 = local_sp_8 + (-8L);
                            *(uint64_t *)var_34 = 4208912UL;
                            var_35 = indirect_placeholder_58(r10_2, r9_2, r14_0, r8_2, rsi_2);
                            *(uint32_t *)6402664UL = (*(uint32_t *)6402664UL + 1U);
                            local_sp_8 = var_34;
                            local_sp_9 = var_34;
                        }
                    r10_3 = var_35.field_0;
                    r9_3 = var_35.field_1;
                    r8_3 = var_35.field_2;
                    rsi_3 = var_35.field_3;
                }
                var_36 = *(unsigned char *)6402765UL;
                var_37 = *(uint32_t *)(rbp_3 + 52UL);
                *(uint32_t *)6402660UL = var_37;
                local_sp_10 = local_sp_9;
                r10_4 = r10_3;
                r9_4 = r9_3;
                rdi1_0_in = var_37;
                r8_4 = r8_3;
                rsi_4 = rsi_3;
                if (var_36 != '\x00') {
                    storemerge = *(uint32_t *)6402760UL;
                }
                *(uint32_t *)6402752UL = storemerge;
                *(unsigned char *)6402782UL = (unsigned char)'\x00';
            }
        }
        rdi1_0 = (uint64_t)rdi1_0_in;
        var_38 = *(uint32_t *)6402668UL;
        local_sp_11 = local_sp_10;
        rdi1_1 = rdi1_0;
        r10_5 = r10_4;
        r9_5 = r9_4;
        r8_5 = r8_4;
        if ((long)((uint64_t)var_38 << 32UL) < (long)(rdi1_0 << 32UL)) {
            var_39 = (uint64_t)(rdi1_0_in - var_38);
            var_40 = local_sp_10 + (-8L);
            *(uint64_t *)var_40 = 4209119UL;
            var_41 = indirect_placeholder_3(var_39);
            *(uint32_t *)6402660UL = 0U;
            local_sp_11 = var_40;
            rdi1_1 = var_41;
        }
        local_sp_12 = local_sp_11;
        if (*(unsigned char *)6402672UL == '\x00') {
            var_42 = local_sp_11 + (-8L);
            *(uint64_t *)var_42 = 4209101UL;
            var_43 = indirect_placeholder_59(r10_4, r9_4, rdi1_1, r8_4, rsi_4);
            local_sp_12 = var_42;
            r10_5 = var_43.field_0;
            r9_5 = var_43.field_1;
            r8_5 = var_43.field_2;
        }
    }
    local_sp_13 = local_sp_12;
    r10_6 = r10_5;
    r9_6 = r9_5;
    r8_6 = r8_5;
    if (*(unsigned char *)(rbp_3 + 56UL) == '\x00') {
        var_44 = local_sp_12 + (-8L);
        *(uint64_t *)var_44 = 4209049UL;
        indirect_placeholder_4(var_25);
        local_sp_13 = var_44;
    }
    *(unsigned char *)6402781UL = (unsigned char)'\x00';
    local_sp_14 = local_sp_13;
    local_sp_15_ph = local_sp_13;
    if (var_21) {
        return rax_2;
    }
    var_45 = *(uint64_t *)6402600UL;
    var_46 = (uint32_t)r13_0;
    var_47 = (uint64_t)(var_46 + (-1));
    var_48 = var_47 + var_45;
    rbx_3 = var_45;
    rbx_4_ph = var_45;
    rax_3 = var_47;
    if ((uint64_t)var_46 == 0UL) {
        var_49 = rbx_3 + 1UL;
        var_50 = local_sp_14 + (-8L);
        *(uint64_t *)var_50 = 4208539UL;
        indirect_placeholder();
        local_sp_14 = var_50;
        rbx_3 = var_49;
        local_sp_15_ph = var_50;
        rbx_4_ph = var_49;
        do {
            var_49 = rbx_3 + 1UL;
            var_50 = local_sp_14 + (-8L);
            *(uint64_t *)var_50 = 4208539UL;
            indirect_placeholder();
            local_sp_14 = var_50;
            rbx_3 = var_49;
            local_sp_15_ph = var_50;
            rbx_4_ph = var_49;
        } while (rbx_3 != var_48);
    }
    local_sp_15 = local_sp_15_ph;
    rbx_4 = rbx_4_ph;
    while (1U)
        {
            var_51 = local_sp_15 + (-8L);
            *(uint64_t *)var_51 = 4208553UL;
            indirect_placeholder();
            var_52 = (uint32_t)rax_3;
            rax_0 = rax_3;
            rbp_0 = rbp_4;
            local_sp_5 = var_51;
            rbx_2 = rbx_4;
            rbp_2 = rbp_4;
            if ((uint64_t)(var_52 + (-10)) != 0UL) {
                loop_state_var = 2U;
                break;
            }
            rax_2 = 0UL;
            if ((uint64_t)(var_52 + (-12)) != 0UL) {
                *(uint64_t *)(local_sp_15 + (-16L)) = 4209065UL;
                indirect_placeholder();
                var_67 = local_sp_15 + (-24L);
                *(uint64_t *)var_67 = 4209085UL;
                indirect_placeholder();
                local_sp_1 = var_67;
                loop_state_var = 0U;
                break;
            }
            if ((uint64_t)(var_52 + 1U) != 0UL) {
                loop_state_var = 1U;
                break;
            }
            var_53 = (uint64_t)(uint32_t)((int)(var_52 << 24U) >> (int)24U);
            var_54 = *(uint32_t *)6402740UL;
            var_55 = local_sp_15 + (-16L);
            *(uint64_t *)var_55 = 4208590UL;
            var_56 = indirect_placeholder_55(r10_6, r9_6, var_53, rbp_4, r8_6);
            var_57 = var_56.field_0;
            var_58 = var_56.field_1;
            var_59 = var_56.field_2;
            var_60 = var_56.field_3;
            var_61 = var_56.field_4;
            r10_6 = var_58;
            r8_6 = var_61;
            local_sp_3 = var_55;
            local_sp_15_be = var_55;
            rax_3 = var_57;
            rbp_4 = var_60;
            r9_6 = var_59;
            if (*(unsigned char *)6402765UL != '\x00') {
                if ((int)*(uint32_t *)6402760UL >= (int)*(uint32_t *)6402740UL) {
                    *(uint32_t *)6402740UL = var_54;
                    loop_state_var = 2U;
                    break;
                }
            }
            var_62 = *(uint64_t *)6402600UL;
            var_63 = (uint32_t)var_57;
            var_64 = (uint64_t)(var_63 + (-1)) + var_62;
            rbx_4_be = var_62;
            rbx_0 = var_62;
            if ((uint64_t)var_63 == 0UL) {
                var_65 = rbx_0 + 1UL;
                var_66 = local_sp_3 + (-8L);
                *(uint64_t *)var_66 = 4208651UL;
                indirect_placeholder();
                local_sp_15_be = var_66;
                rbx_4_be = var_65;
                local_sp_3 = var_66;
                rbx_0 = var_65;
                do {
                    var_65 = rbx_0 + 1UL;
                    var_66 = local_sp_3 + (-8L);
                    *(uint64_t *)var_66 = 4208651UL;
                    indirect_placeholder();
                    local_sp_15_be = var_66;
                    rbx_4_be = var_65;
                    local_sp_3 = var_66;
                    rbx_0 = var_65;
                } while (rbx_0 != var_64);
            }
            local_sp_15 = local_sp_15_be;
            rbx_4 = rbx_4_be;
            continue;
        }
    switch (loop_state_var) {
      case 0U:
        {
            local_sp_2 = local_sp_1;
            rax_1 = rax_0;
            rbp_1 = rbp_0;
            if (*(unsigned char *)6402778UL != '\x00') {
                *(unsigned char *)6402777UL = (unsigned char)'\x01';
            }
            var_74 = rbp_1 + 48UL;
            var_75 = rbp_1 + 16UL;
            *(uint64_t *)(local_sp_2 + (-8L)) = 4208821UL;
            indirect_placeholder_30(rax_1, var_75, var_74);
            return 1UL;
        }
        break;
      case 2U:
      case 1U:
        {
            switch (loop_state_var) {
              case 2U:
                {
                    return rax_2;
                }
                break;
              case 1U:
                {
                    *(uint64_t *)(local_sp_5 + (-8L)) = 4208704UL;
                    indirect_placeholder_23(rbx_2, rbp_2);
                    return rax_2;
                }
                break;
            }
        }
        break;
    }
}

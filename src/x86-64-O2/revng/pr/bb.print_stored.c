typedef unsigned char __u_char;
typedef unsigned short __u_short;
typedef unsigned int __u_int;
typedef unsigned long __u_long;
typedef signed char __int8_t;
typedef unsigned char __uint8_t;
typedef short __int16_t;
typedef unsigned short __uint16_t;
typedef int __int32_t;
typedef unsigned int __uint32_t;
typedef long __int64_t;
typedef unsigned long __uint64_t;
typedef __int8_t __int_least8_t;
typedef __uint8_t __uint_least8_t;
typedef __int16_t __int_least16_t;
typedef __uint16_t __uint_least16_t;
typedef __int32_t __int_least32_t;
typedef __uint32_t __uint_least32_t;
typedef __int64_t __int_least64_t;
typedef __uint64_t __uint_least64_t;
typedef long __quad_t;
typedef unsigned long __u_quad_t;
typedef long __intmax_t;
typedef unsigned long __uintmax_t;
typedef unsigned long __dev_t;
typedef unsigned int __uid_t;
typedef unsigned int __gid_t;
typedef unsigned long __ino_t;
typedef unsigned long __ino64_t;
typedef unsigned int __mode_t;
typedef unsigned long __nlink_t;
typedef long __off_t;
typedef long __off64_t;
typedef int __pid_t;
typedef struct {
    int __val[2];
} __fsid_t;
typedef long __clock_t;
typedef unsigned long __rlim_t;
typedef unsigned long __rlim64_t;
typedef unsigned int __id_t;
typedef long __time_t;
typedef unsigned int __useconds_t;
typedef long __suseconds_t;
typedef int __daddr_t;
typedef int __key_t;
typedef int __clockid_t;
typedef void *__timer_t;
typedef long __blksize_t;
typedef long __blkcnt_t;
typedef long __blkcnt64_t;
typedef unsigned long __fsblkcnt_t;
typedef unsigned long __fsblkcnt64_t;
typedef unsigned long __fsfilcnt_t;
typedef unsigned long __fsfilcnt64_t;
typedef long __fsword_t;
typedef long __ssize_t;
typedef long __syscall_slong_t;
typedef unsigned long __syscall_ulong_t;
typedef __off64_t __loff_t;
typedef char *__caddr_t;
typedef long __intptr_t;
typedef unsigned int __socklen_t;
typedef int __sig_atomic_t;
typedef __int8_t int8_t;
typedef __int16_t int16_t;
typedef __int32_t int32_t;
typedef __int64_t int64_t;
typedef __uint8_t uint8_t;
typedef __uint16_t uint16_t;
typedef __uint32_t uint32_t;
typedef __uint64_t uint64_t;
typedef __int_least8_t int_least8_t;
typedef __int_least16_t int_least16_t;
typedef __int_least32_t int_least32_t;
typedef __int_least64_t int_least64_t;
typedef __uint_least8_t uint_least8_t;
typedef __uint_least16_t uint_least16_t;
typedef __uint_least32_t uint_least32_t;
typedef __uint_least64_t uint_least64_t;
typedef signed char int_fast8_t;
typedef long int_fast16_t;
typedef long int_fast32_t;
typedef long int_fast64_t;
typedef unsigned char uint_fast8_t;
typedef unsigned long uint_fast16_t;
typedef unsigned long uint_fast32_t;
typedef unsigned long uint_fast64_t;
typedef long intptr_t;
typedef unsigned long uintptr_t;
typedef __intmax_t intmax_t;
typedef __uintmax_t uintmax_t;
struct indirect_placeholder_52_ret_type;
struct indirect_placeholder_53_ret_type;
struct indirect_placeholder_52_ret_type {
    uint64_t field_0;
    uint64_t field_1;
    uint64_t field_2;
    uint64_t field_3;
};
struct indirect_placeholder_53_ret_type {
    uint64_t field_0;
    uint64_t field_1;
    uint64_t field_2;
    uint64_t field_3;
    uint64_t field_4;
};
extern uint64_t revng_init_local_sp(uint64_t param_0);
extern uint64_t init_rbx(void);
extern uint64_t init_rbp(void);
extern uint64_t init_r13(void);
extern uint64_t init_r12(void);
extern uint64_t indirect_placeholder_3(uint64_t param_0);
extern uint64_t helper_cc_compute_all_wrapper(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint32_t param_3);
extern void indirect_placeholder_13(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint64_t param_3, uint64_t param_4);
extern struct indirect_placeholder_52_ret_type indirect_placeholder_52(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint64_t param_3, uint64_t param_4);
extern struct indirect_placeholder_53_ret_type indirect_placeholder_53(void);
uint64_t bb_print_stored(uint64_t r10, uint64_t r9, uint64_t rdi, uint64_t r8, uint64_t rsi) {
    struct indirect_placeholder_52_ret_type var_38;
    uint64_t r84_0;
    uint64_t var_0;
    uint64_t var_1;
    uint64_t var_2;
    uint64_t var_3;
    uint64_t var_4;
    uint64_t var_5;
    uint32_t *var_6;
    uint32_t var_7;
    uint64_t var_8;
    uint64_t var_9;
    uint64_t var_10;
    uint64_t var_11;
    uint32_t var_12;
    uint64_t var_13;
    uint32_t var_14;
    uint64_t var_15;
    uint64_t var_16;
    uint64_t var_17;
    uint64_t var_18;
    uint64_t local_sp_0;
    uint64_t local_sp_1;
    uint64_t r101_0;
    uint64_t r92_0;
    uint64_t r92_1;
    uint64_t r84_1;
    uint64_t rsi5_0;
    uint64_t var_25;
    uint64_t var_26;
    uint64_t var_27;
    uint64_t var_28;
    uint64_t rax_0;
    uint64_t var_29;
    uint64_t r101_1;
    uint64_t rsi5_1;
    uint32_t var_30;
    uint64_t var_31;
    uint32_t var_32;
    uint64_t var_33;
    uint64_t local_sp_2;
    uint64_t rdi3_0;
    uint64_t local_sp_3;
    uint64_t r101_2;
    uint64_t r92_2;
    uint64_t r84_2;
    uint64_t rsi5_2;
    uint64_t local_sp_4;
    uint64_t rbx_0;
    uint64_t var_39;
    uint64_t var_40;
    uint64_t var_41;
    uint64_t var_42;
    uint32_t var_43;
    uint32_t var_44;
    uint32_t var_45;
    uint64_t var_46;
    uint64_t var_37;
    uint64_t var_34;
    uint64_t var_35;
    uint64_t var_36;
    uint64_t var_19;
    struct indirect_placeholder_53_ret_type var_20;
    uint64_t var_21;
    uint64_t var_22;
    uint64_t var_23;
    uint64_t var_24;
    var_0 = revng_init_local_sp(0UL);
    var_1 = init_rbx();
    var_2 = init_rbp();
    var_3 = init_r13();
    var_4 = init_r12();
    *(uint64_t *)(var_0 + (-8L)) = var_3;
    *(uint64_t *)(var_0 + (-16L)) = var_4;
    *(uint64_t *)(var_0 + (-24L)) = var_2;
    *(uint64_t *)(var_0 + (-32L)) = var_1;
    var_5 = var_0 + (-40L);
    var_6 = (uint32_t *)(rdi + 40UL);
    var_7 = *var_6;
    var_8 = (uint64_t)var_7;
    var_9 = *(uint64_t *)6402792UL;
    *(unsigned char *)6402656UL = (unsigned char)'\x01';
    *var_6 = (var_7 + 1U);
    var_10 = var_8 << 2UL;
    var_11 = var_10 + var_9;
    var_12 = *(uint32_t *)var_11;
    var_13 = (uint64_t)var_12;
    var_14 = *(uint32_t *)(var_11 + 4UL);
    var_15 = (uint64_t)var_14;
    var_16 = *(uint64_t *)6402816UL;
    var_17 = var_16 + var_13;
    var_18 = var_16 + var_15;
    r84_0 = r8;
    local_sp_0 = var_5;
    local_sp_1 = var_5;
    r101_0 = r10;
    r92_0 = r9;
    r92_1 = r9;
    r84_1 = r8;
    rsi5_0 = rsi;
    r101_1 = r10;
    rsi5_1 = rsi;
    rbx_0 = var_17;
    if (*(unsigned char *)6402776UL == '\x00') {
        if (*(uint32_t *)(rdi + 16UL) != 1U) {
            var_25 = (uint64_t)*(uint32_t *)6402216UL;
            var_26 = *(uint64_t *)6402824UL;
            var_27 = helper_cc_compute_all_wrapper(var_25, 0UL, 0UL, 24U);
            local_sp_1 = local_sp_0;
            r92_1 = r92_0;
            r84_1 = r84_0;
            r101_1 = r101_0;
            rsi5_1 = rsi5_0;
            if ((uint64_t)(((unsigned char)(var_27 >> 4UL) ^ (unsigned char)var_27) & '\xc0') != 0UL) {
                var_28 = (((var_25 << 6UL) + 274877906880UL) & 274877906880UL) + var_26;
                rax_0 = var_26 + 16UL;
                *(uint32_t *)rax_0 = 2U;
                while (rax_0 != (var_28 + 16UL))
                    {
                        rax_0 = rax_0 + 64UL;
                        *(uint32_t *)rax_0 = 2U;
                    }
            }
            var_29 = helper_cc_compute_all_wrapper((uint64_t)*(uint32_t *)(var_26 + 48UL), 0UL, 0UL, 24U);
            if ((uint64_t)(((unsigned char)(var_29 >> 4UL) ^ (unsigned char)var_29) & '\xc0') != 0UL) {
                if (*(unsigned char *)6402245UL == '\x00') {
                    *(unsigned char *)6402656UL = (unsigned char)'\x00';
                }
                return 1UL;
            }
        }
    }
    var_19 = var_0 + (-48L);
    *(uint64_t *)var_19 = 4209573UL;
    var_20 = indirect_placeholder_53();
    var_21 = var_20.field_1;
    var_22 = var_20.field_2;
    var_23 = var_20.field_3;
    var_24 = var_20.field_4;
    r84_0 = var_23;
    local_sp_0 = var_19;
    local_sp_1 = var_19;
    r101_0 = var_21;
    r92_0 = var_22;
    r92_1 = var_22;
    r84_1 = var_23;
    rsi5_0 = var_24;
    r101_1 = var_21;
    rsi5_1 = var_24;
    if (*(uint32_t *)(rdi + 16UL) != 1U) {
        var_25 = (uint64_t)*(uint32_t *)6402216UL;
        var_26 = *(uint64_t *)6402824UL;
        var_27 = helper_cc_compute_all_wrapper(var_25, 0UL, 0UL, 24U);
        local_sp_1 = local_sp_0;
        r92_1 = r92_0;
        r84_1 = r84_0;
        r101_1 = r101_0;
        rsi5_1 = rsi5_0;
        if ((uint64_t)(((unsigned char)(var_27 >> 4UL) ^ (unsigned char)var_27) & '\xc0') != 0UL) {
            var_28 = (((var_25 << 6UL) + 274877906880UL) & 274877906880UL) + var_26;
            rax_0 = var_26 + 16UL;
            *(uint32_t *)rax_0 = 2U;
            while (rax_0 != (var_28 + 16UL))
                {
                    rax_0 = rax_0 + 64UL;
                    *(uint32_t *)rax_0 = 2U;
                }
        }
        var_29 = helper_cc_compute_all_wrapper((uint64_t)*(uint32_t *)(var_26 + 48UL), 0UL, 0UL, 24U);
        if ((uint64_t)(((unsigned char)(var_29 >> 4UL) ^ (unsigned char)var_29) & '\xc0') == 0UL) {
            if (*(unsigned char *)6402245UL == '\x00') {
                *(unsigned char *)6402656UL = (unsigned char)'\x00';
            }
            return 1UL;
        }
    }
    var_30 = *(uint32_t *)6402668UL;
    var_31 = (uint64_t)var_30;
    var_32 = *(uint32_t *)6402660UL;
    var_33 = (uint64_t)var_32;
    local_sp_2 = local_sp_1;
    rdi3_0 = var_33;
    r101_2 = r101_1;
    r92_2 = r92_1;
    r84_2 = r84_1;
    rsi5_2 = rsi5_1;
    if ((long)(var_31 << 32UL) < (long)(var_33 << 32UL)) {
        var_34 = (uint64_t)(var_32 - var_30);
        var_35 = local_sp_1 + (-8L);
        *(uint64_t *)var_35 = 4209703UL;
        var_36 = indirect_placeholder_3(var_34);
        *(uint32_t *)6402660UL = 0U;
        local_sp_2 = var_35;
        rdi3_0 = var_36;
    }
    local_sp_3 = local_sp_2;
    if (*(unsigned char *)6402672UL == '\x00') {
        var_37 = local_sp_2 + (-8L);
        *(uint64_t *)var_37 = 4209685UL;
        var_38 = indirect_placeholder_52(r101_1, r92_1, rdi3_0, r84_1, rsi5_1);
        local_sp_3 = var_37;
        r101_2 = var_38.field_0;
        r92_2 = var_38.field_1;
        r84_2 = var_38.field_2;
        rsi5_2 = var_38.field_3;
    }
    local_sp_4 = local_sp_3;
    if (var_12 == var_14) {
        var_39 = rbx_0 + 1UL;
        var_40 = (uint64_t)(uint32_t)(uint64_t)*(unsigned char *)rbx_0;
        var_41 = local_sp_4 + (-8L);
        *(uint64_t *)var_41 = 4209469UL;
        indirect_placeholder_13(r101_2, r92_2, var_40, r84_2, rsi5_2);
        local_sp_4 = var_41;
        rbx_0 = var_39;
        do {
            var_39 = rbx_0 + 1UL;
            var_40 = (uint64_t)(uint32_t)(uint64_t)*(unsigned char *)rbx_0;
            var_41 = local_sp_4 + (-8L);
            *(uint64_t *)var_41 = 4209469UL;
            indirect_placeholder_13(r101_2, r92_2, var_40, r84_2, rsi5_2);
            local_sp_4 = var_41;
            rbx_0 = var_39;
        } while (var_18 != var_39);
    }
    if (*(uint32_t *)6402752UL == 0U) {
        return 1UL;
    }
    var_42 = *(uint64_t *)6402784UL;
    var_43 = *(uint32_t *)(rdi + 52UL);
    var_44 = *(uint32_t *)6402668UL;
    var_45 = var_43 + *(uint32_t *)(var_10 + var_42);
    var_46 = (uint64_t)((var_43 - var_44) - *(uint32_t *)6402748UL);
    *(uint32_t *)6402744UL = var_45;
    if (var_46 != 0UL) {
        return 1UL;
    }
    *(uint32_t *)6402744UL = (var_45 - var_44);
    return 1UL;
    *(uint32_t *)6402744UL = (var_45 - var_44);
    return 1UL;
}

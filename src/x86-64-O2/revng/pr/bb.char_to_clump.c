typedef unsigned char __u_char;
typedef unsigned short __u_short;
typedef unsigned int __u_int;
typedef unsigned long __u_long;
typedef signed char __int8_t;
typedef unsigned char __uint8_t;
typedef short __int16_t;
typedef unsigned short __uint16_t;
typedef int __int32_t;
typedef unsigned int __uint32_t;
typedef long __int64_t;
typedef unsigned long __uint64_t;
typedef __int8_t __int_least8_t;
typedef __uint8_t __uint_least8_t;
typedef __int16_t __int_least16_t;
typedef __uint16_t __uint_least16_t;
typedef __int32_t __int_least32_t;
typedef __uint32_t __uint_least32_t;
typedef __int64_t __int_least64_t;
typedef __uint64_t __uint_least64_t;
typedef long __quad_t;
typedef unsigned long __u_quad_t;
typedef long __intmax_t;
typedef unsigned long __uintmax_t;
typedef unsigned long __dev_t;
typedef unsigned int __uid_t;
typedef unsigned int __gid_t;
typedef unsigned long __ino_t;
typedef unsigned long __ino64_t;
typedef unsigned int __mode_t;
typedef unsigned long __nlink_t;
typedef long __off_t;
typedef long __off64_t;
typedef int __pid_t;
typedef struct {
    int __val[2];
} __fsid_t;
typedef long __clock_t;
typedef unsigned long __rlim_t;
typedef unsigned long __rlim64_t;
typedef unsigned int __id_t;
typedef long __time_t;
typedef unsigned int __useconds_t;
typedef long __suseconds_t;
typedef int __daddr_t;
typedef int __key_t;
typedef int __clockid_t;
typedef void *__timer_t;
typedef long __blksize_t;
typedef long __blkcnt_t;
typedef long __blkcnt64_t;
typedef unsigned long __fsblkcnt_t;
typedef unsigned long __fsblkcnt64_t;
typedef unsigned long __fsfilcnt_t;
typedef unsigned long __fsfilcnt64_t;
typedef long __fsword_t;
typedef long __ssize_t;
typedef long __syscall_slong_t;
typedef unsigned long __syscall_ulong_t;
typedef __off64_t __loff_t;
typedef char *__caddr_t;
typedef long __intptr_t;
typedef unsigned int __socklen_t;
typedef int __sig_atomic_t;
typedef __int8_t int8_t;
typedef __int16_t int16_t;
typedef __int32_t int32_t;
typedef __int64_t int64_t;
typedef __uint8_t uint8_t;
typedef __uint16_t uint16_t;
typedef __uint32_t uint32_t;
typedef __uint64_t uint64_t;
typedef __int_least8_t int_least8_t;
typedef __int_least16_t int_least16_t;
typedef __int_least32_t int_least32_t;
typedef __int_least64_t int_least64_t;
typedef __uint_least8_t uint_least8_t;
typedef __uint_least16_t uint_least16_t;
typedef __uint_least32_t uint_least32_t;
typedef __uint_least64_t uint_least64_t;
typedef signed char int_fast8_t;
typedef long int_fast16_t;
typedef long int_fast32_t;
typedef long int_fast64_t;
typedef unsigned char uint_fast8_t;
typedef unsigned long uint_fast16_t;
typedef unsigned long uint_fast32_t;
typedef unsigned long uint_fast64_t;
typedef long intptr_t;
typedef unsigned long uintptr_t;
typedef __intmax_t intmax_t;
typedef __uintmax_t uintmax_t;
typedef _Bool bool;
struct bb_char_to_clump_ret_type;
struct helper_idivl_EAX_wrapper_ret_type;
struct type_6;
struct bb_char_to_clump_ret_type {
    uint64_t field_0;
    uint64_t field_1;
    uint64_t field_2;
    uint64_t field_3;
    uint64_t field_4;
    uint64_t field_5;
};
struct helper_idivl_EAX_wrapper_ret_type {
    uint64_t field_0;
    uint64_t field_1;
    uint32_t field_2;
    uint64_t field_3;
    uint64_t field_4;
    uint32_t field_5;
    uint32_t field_6;
    uint64_t field_7;
    uint32_t field_8;
    uint64_t field_9;
    uint32_t field_10;
    unsigned char field_11;
    uint32_t field_12;
};
struct type_6 {
};
extern uint64_t revng_init_local_sp(uint64_t param_0);
extern uint64_t init_rbx(void);
extern void indirect_placeholder(void);
extern uint32_t init_state_0x8248(void);
extern uint64_t init_state_0x9018(void);
extern uint32_t init_state_0x9010(void);
extern uint64_t init_state_0x8408(void);
extern uint64_t init_state_0x8328(void);
extern uint64_t init_state_0x82d8(void);
extern uint32_t init_state_0x9080(void);
extern struct helper_idivl_EAX_wrapper_ret_type helper_idivl_EAX_wrapper(struct type_6 *param_0, uint64_t param_1, uint64_t param_2, uint64_t param_3, uint64_t param_4, uint64_t param_5, uint64_t param_6, uint64_t param_7, uint64_t param_8, uint64_t param_9, uint64_t param_10, uint64_t param_11, uint64_t param_12, uint32_t param_13, uint64_t param_14, uint32_t param_15, uint64_t param_16, uint64_t param_17, uint64_t param_18, uint32_t param_19);
struct bb_char_to_clump_ret_type bb_char_to_clump(uint64_t r10, uint64_t r9, uint64_t rdi, uint64_t rbp, uint64_t r8) {
    uint64_t var_0;
    uint64_t var_1;
    uint32_t var_2;
    uint64_t var_3;
    uint32_t var_4;
    uint64_t var_5;
    uint64_t var_6;
    uint64_t var_7;
    uint32_t var_8;
    unsigned char var_9;
    unsigned char var_10;
    uint64_t var_11;
    uint64_t var_12;
    uint64_t rcx_0;
    uint64_t rdx_0;
    bool var_14;
    unsigned char *var_15;
    uint64_t rax_0;
    uint64_t var_16;
    uint64_t rdx_2;
    uint64_t var_17;
    uint64_t rdx_1;
    uint64_t var_13;
    uint32_t storemerge1;
    struct bb_char_to_clump_ret_type mrv6;
    struct bb_char_to_clump_ret_type mrv7;
    uint64_t var_18;
    struct helper_idivl_EAX_wrapper_ret_type var_19;
    uint32_t var_20;
    uint64_t var_21;
    uint64_t var_22;
    uint64_t rbx_0;
    uint64_t storemerge;
    uint64_t rsi_0;
    uint64_t rax_1;
    uint64_t rsi_1;
    uint64_t rsi_2;
    struct bb_char_to_clump_ret_type mrv1 = {0UL, /*implicit*/(int)0, /*implicit*/(int)0, /*implicit*/(int)0, /*implicit*/(int)0, /*implicit*/(int)0};
    struct bb_char_to_clump_ret_type mrv2;
    struct bb_char_to_clump_ret_type mrv3;
    struct bb_char_to_clump_ret_type mrv4;
    struct bb_char_to_clump_ret_type mrv5;
    uint64_t rax_2;
    uint64_t rax_3;
    struct bb_char_to_clump_ret_type mrv8;
    struct bb_char_to_clump_ret_type mrv9;
    struct bb_char_to_clump_ret_type mrv10;
    struct bb_char_to_clump_ret_type mrv11;
    var_0 = revng_init_local_sp(0UL);
    var_1 = init_rbx();
    var_2 = init_state_0x8248();
    var_3 = init_state_0x9018();
    var_4 = init_state_0x9010();
    var_5 = init_state_0x8408();
    var_6 = init_state_0x8328();
    var_7 = init_state_0x82d8();
    var_8 = init_state_0x9080();
    *(uint64_t *)(var_0 + (-8L)) = var_1;
    var_9 = *(unsigned char *)6402232UL;
    var_10 = (unsigned char)rdi;
    var_11 = (uint64_t)(var_10 - var_9);
    var_12 = *(uint64_t *)6402600UL;
    rcx_0 = (uint64_t)*(uint32_t *)6402228UL;
    rdx_0 = 1UL;
    rax_0 = 1UL;
    rdx_2 = 0UL;
    rdx_1 = 4294967295UL;
    storemerge1 = 0U;
    rbx_0 = var_12;
    storemerge = 1UL;
    rax_1 = 1UL;
    rax_2 = 0UL;
    if (var_11 == 0UL) {
        var_18 = (uint64_t)*(uint32_t *)6402740UL;
        var_19 = helper_idivl_EAX_wrapper((struct type_6 *)(0UL), rcx_0, 4205374UL, var_18, rcx_0, r10, var_12, r9, rdi, rbp, r8, (uint64_t)(uint32_t)(uint64_t)((long)(var_18 << 32UL) >> (long)63UL), var_18, var_2, var_3, var_4, var_5, var_6, var_7, var_8);
        var_20 = (uint32_t)rcx_0 - (uint32_t)var_19.field_4;
        var_21 = (uint64_t)var_20;
        rdx_1 = var_21;
        rsi_0 = var_18;
        rsi_1 = var_18;
        if (*(unsigned char *)6402757UL != '\x00') {
            storemerge = var_21;
            if (var_21 != 0UL) {
                rsi_2 = rsi_1;
                rax_3 = rax_2;
                storemerge1 = (uint32_t)rdx_2 + (uint32_t)rsi_1;
                *(uint32_t *)6402740UL = storemerge1;
                mrv6.field_0 = rax_3;
                mrv7 = mrv6;
                mrv7.field_1 = r10;
                mrv8 = mrv7;
                mrv8.field_2 = r9;
                mrv9 = mrv8;
                mrv9.field_3 = rbp;
                mrv10 = mrv9;
                mrv10.field_4 = r8;
                mrv11 = mrv10;
                mrv11.field_5 = rsi_2;
                return mrv11;
            }
            var_22 = (uint64_t)(var_20 + (-1)) + var_12;
            *(unsigned char *)rbx_0 = (unsigned char)' ';
            while (rbx_0 != var_22)
                {
                    rbx_0 = rbx_0 + 1UL;
                    *(unsigned char *)rbx_0 = (unsigned char)' ';
                }
        }
        *(unsigned char *)var_12 = var_10;
        rax_1 = storemerge;
        rdx_2 = var_21;
        rax_2 = storemerge;
        if ((int)var_20 <= (int)4294967295U) {
            rsi_2 = rsi_1;
            rax_3 = rax_2;
            storemerge1 = (uint32_t)rdx_2 + (uint32_t)rsi_1;
            *(uint32_t *)6402740UL = storemerge1;
            mrv6.field_0 = rax_3;
            mrv7 = mrv6;
            mrv7.field_1 = r10;
            mrv8 = mrv7;
            mrv8.field_2 = r9;
            mrv9 = mrv8;
            mrv9.field_3 = rbp;
            mrv10 = mrv9;
            mrv10.field_4 = r8;
            mrv11 = mrv10;
            mrv11.field_5 = rsi_2;
            return mrv11;
        }
        rdx_2 = rdx_1;
        rsi_1 = rsi_0;
        rsi_2 = rsi_0;
        rax_2 = rax_1;
        rax_3 = rax_1;
        if (rsi_0 == 0UL) {
            mrv1.field_1 = r10;
            mrv2 = mrv1;
            mrv2.field_2 = r9;
            mrv3 = mrv2;
            mrv3.field_3 = rbp;
            mrv4 = mrv3;
            mrv4.field_4 = r8;
            mrv5 = mrv4;
            mrv5.field_5 = 0UL;
            return mrv5;
        }
        if ((long)(rsi_0 << 32UL) > (long)(0UL - (rdx_1 << 32UL))) {
            rsi_2 = rsi_1;
            rax_3 = rax_2;
            storemerge1 = (uint32_t)rdx_2 + (uint32_t)rsi_1;
        }
        *(uint32_t *)6402740UL = storemerge1;
        mrv6.field_0 = rax_3;
        mrv7 = mrv6;
        mrv7.field_1 = r10;
        mrv8 = mrv7;
        mrv8.field_2 = r9;
        mrv9 = mrv8;
        mrv9.field_3 = rbp;
        mrv10 = mrv9;
        mrv10.field_4 = r8;
        mrv11 = mrv10;
        mrv11.field_5 = rsi_2;
        return mrv11;
    }
    rcx_0 = 8UL;
    if ((uint64_t)(var_10 + '\xf7') == 0UL) {
        return;
    }
    if ((uint64_t)((uint32_t)(uint64_t)var_10 + (-32)) <= 94UL) {
        *(unsigned char *)var_12 = var_10;
        var_16 = (uint64_t)*(uint32_t *)6402740UL;
        rdx_2 = rdx_0;
        rsi_1 = var_16;
        rax_2 = rax_0;
        rsi_2 = rsi_1;
        rax_3 = rax_2;
        storemerge1 = (uint32_t)rdx_2 + (uint32_t)rsi_1;
        *(uint32_t *)6402740UL = storemerge1;
        mrv6.field_0 = rax_3;
        mrv7 = mrv6;
        mrv7.field_1 = r10;
        mrv8 = mrv7;
        mrv8.field_2 = r9;
        mrv9 = mrv8;
        mrv9.field_3 = rbp;
        mrv10 = mrv9;
        mrv10.field_4 = r8;
        mrv11 = mrv10;
        mrv11.field_5 = rsi_2;
        return mrv11;
    }
    rdx_0 = 4UL;
    rax_0 = 4UL;
    if (*(unsigned char *)6402682UL != '\x00') {
        rdx_0 = 0UL;
        rax_0 = 2UL;
        if (*(unsigned char *)6402681UL != '\x00') {
            var_14 = ((uint64_t)(var_10 + '\xf8') == 0UL);
            var_15 = (unsigned char *)var_12;
            if (!var_14) {
                *var_15 = var_10;
                var_16 = (uint64_t)*(uint32_t *)6402740UL;
                rdx_2 = rdx_0;
                rsi_1 = var_16;
                rax_2 = rax_0;
                rsi_2 = rsi_1;
                rax_3 = rax_2;
                storemerge1 = (uint32_t)rdx_2 + (uint32_t)rsi_1;
                *(uint32_t *)6402740UL = storemerge1;
                mrv6.field_0 = rax_3;
                mrv7 = mrv6;
                mrv7.field_1 = r10;
                mrv8 = mrv7;
                mrv8.field_2 = r9;
                mrv9 = mrv8;
                mrv9.field_3 = rbp;
                mrv10 = mrv9;
                mrv10.field_4 = r8;
                mrv11 = mrv10;
                mrv11.field_5 = rsi_2;
                return mrv11;
            }
            *var_15 = (unsigned char)'\b';
            var_17 = (uint64_t)*(uint32_t *)6402740UL;
            rsi_0 = var_17;
            rdx_2 = rdx_1;
            rsi_1 = rsi_0;
            rsi_2 = rsi_0;
            rax_2 = rax_1;
            rax_3 = rax_1;
            if (rsi_0 == 0UL) {
                mrv1.field_1 = r10;
                mrv2 = mrv1;
                mrv2.field_2 = r9;
                mrv3 = mrv2;
                mrv3.field_3 = rbp;
                mrv4 = mrv3;
                mrv4.field_4 = r8;
                mrv5 = mrv4;
                mrv5.field_5 = 0UL;
                return mrv5;
            }
            if ((long)(rsi_0 << 32UL) > (long)(0UL - (rdx_1 << 32UL))) {
                rsi_2 = rsi_1;
                rax_3 = rax_2;
                storemerge1 = (uint32_t)rdx_2 + (uint32_t)rsi_1;
            }
            *(uint32_t *)6402740UL = storemerge1;
            mrv6.field_0 = rax_3;
            mrv7 = mrv6;
            mrv7.field_1 = r10;
            mrv8 = mrv7;
            mrv8.field_2 = r9;
            mrv9 = mrv8;
            mrv9.field_3 = rbp;
            mrv10 = mrv9;
            mrv10.field_4 = r8;
            mrv11 = mrv10;
            mrv11.field_5 = rsi_2;
            return mrv11;
        }
        rdx_0 = 2UL;
        if ((signed char)var_10 >= '\x00') {
            *(unsigned char *)var_12 = (unsigned char)'^';
            *(unsigned char *)(var_12 + 1UL) = (var_10 ^ '@');
            var_16 = (uint64_t)*(uint32_t *)6402740UL;
            rdx_2 = rdx_0;
            rsi_1 = var_16;
            rax_2 = rax_0;
            rsi_2 = rsi_1;
            rax_3 = rax_2;
            storemerge1 = (uint32_t)rdx_2 + (uint32_t)rsi_1;
            *(uint32_t *)6402740UL = storemerge1;
            mrv6.field_0 = rax_3;
            mrv7 = mrv6;
            mrv7.field_1 = r10;
            mrv8 = mrv7;
            mrv8.field_2 = r9;
            mrv9 = mrv8;
            mrv9.field_3 = rbp;
            mrv10 = mrv9;
            mrv10.field_4 = r8;
            mrv11 = mrv10;
            mrv11.field_5 = rsi_2;
            return mrv11;
        }
    }
    *(unsigned char *)var_12 = (unsigned char)'\\';
    var_13 = var_0 + (-32L);
    *(uint64_t *)var_13 = 4205522UL;
    indirect_placeholder();
    *(unsigned char *)(var_12 + 1UL) = *(unsigned char *)var_13;
    *(unsigned char *)(var_12 + 2UL) = *(unsigned char *)(var_0 + (-31L));
    *(unsigned char *)(var_12 + 3UL) = *(unsigned char *)(var_0 + (-30L));
}

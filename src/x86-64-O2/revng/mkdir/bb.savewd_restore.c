typedef unsigned char __u_char;
typedef unsigned short __u_short;
typedef unsigned int __u_int;
typedef unsigned long __u_long;
typedef signed char __int8_t;
typedef unsigned char __uint8_t;
typedef short __int16_t;
typedef unsigned short __uint16_t;
typedef int __int32_t;
typedef unsigned int __uint32_t;
typedef long __int64_t;
typedef unsigned long __uint64_t;
typedef __int8_t __int_least8_t;
typedef __uint8_t __uint_least8_t;
typedef __int16_t __int_least16_t;
typedef __uint16_t __uint_least16_t;
typedef __int32_t __int_least32_t;
typedef __uint32_t __uint_least32_t;
typedef __int64_t __int_least64_t;
typedef __uint64_t __uint_least64_t;
typedef long __quad_t;
typedef unsigned long __u_quad_t;
typedef long __intmax_t;
typedef unsigned long __uintmax_t;
typedef unsigned long __dev_t;
typedef unsigned int __uid_t;
typedef unsigned int __gid_t;
typedef unsigned long __ino_t;
typedef unsigned long __ino64_t;
typedef unsigned int __mode_t;
typedef unsigned long __nlink_t;
typedef long __off_t;
typedef long __off64_t;
typedef int __pid_t;
typedef struct {
    int __val[2];
} __fsid_t;
typedef long __clock_t;
typedef unsigned long __rlim_t;
typedef unsigned long __rlim64_t;
typedef unsigned int __id_t;
typedef long __time_t;
typedef unsigned int __useconds_t;
typedef long __suseconds_t;
typedef int __daddr_t;
typedef int __key_t;
typedef int __clockid_t;
typedef void *__timer_t;
typedef long __blksize_t;
typedef long __blkcnt_t;
typedef long __blkcnt64_t;
typedef unsigned long __fsblkcnt_t;
typedef unsigned long __fsblkcnt64_t;
typedef unsigned long __fsfilcnt_t;
typedef unsigned long __fsfilcnt64_t;
typedef long __fsword_t;
typedef long __ssize_t;
typedef long __syscall_slong_t;
typedef unsigned long __syscall_ulong_t;
typedef __off64_t __loff_t;
typedef char *__caddr_t;
typedef long __intptr_t;
typedef unsigned int __socklen_t;
typedef int __sig_atomic_t;
typedef __int8_t int8_t;
typedef __int16_t int16_t;
typedef __int32_t int32_t;
typedef __int64_t int64_t;
typedef __uint8_t uint8_t;
typedef __uint16_t uint16_t;
typedef __uint32_t uint32_t;
typedef __uint64_t uint64_t;
typedef __int_least8_t int_least8_t;
typedef __int_least16_t int_least16_t;
typedef __int_least32_t int_least32_t;
typedef __int_least64_t int_least64_t;
typedef __uint_least8_t uint_least8_t;
typedef __uint_least16_t uint_least16_t;
typedef __uint_least32_t uint_least32_t;
typedef __uint_least64_t uint_least64_t;
typedef signed char int_fast8_t;
typedef long int_fast16_t;
typedef long int_fast32_t;
typedef long int_fast64_t;
typedef unsigned char uint_fast8_t;
typedef unsigned long uint_fast16_t;
typedef unsigned long uint_fast32_t;
typedef unsigned long uint_fast64_t;
typedef long intptr_t;
typedef unsigned long uintptr_t;
typedef __intmax_t intmax_t;
typedef __uintmax_t uintmax_t;
typedef _Bool bool;
extern void abort(void);
extern uint64_t revng_init_local_sp(uint64_t param_0);
extern uint64_t init_rbx(void);
extern uint64_t init_rbp(void);
extern void indirect_placeholder(void);
extern uint64_t init_cc_src2(void);
extern uint64_t helper_cc_compute_all_wrapper(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint32_t param_3);
uint64_t bb_savewd_restore(uint64_t rdi, uint64_t rsi) {
    uint64_t var_0;
    uint64_t var_1;
    uint64_t var_2;
    uint64_t var_3;
    uint64_t var_4;
    uint32_t *_cast;
    uint32_t var_5;
    uint64_t var_17;
    uint64_t rax_0;
    uint64_t var_18;
    uint64_t local_sp_1_be;
    uint32_t *_pre29;
    uint64_t var_19;
    uint64_t local_sp_1;
    uint32_t *var_8;
    uint32_t var_9;
    uint64_t var_10;
    uint32_t *var_11;
    uint32_t *_pre_phi30;
    uint64_t var_6;
    uint64_t var_7;
    uint32_t *_pre_phi28;
    uint64_t local_sp_0;
    uint32_t *_pre27;
    uint32_t *var_12;
    uint32_t var_13;
    uint64_t var_14;
    bool var_15;
    uint32_t *var_16;
    var_0 = revng_init_local_sp(0UL);
    var_1 = init_rbx();
    var_2 = init_rbp();
    var_3 = init_cc_src2();
    *(uint64_t *)(var_0 + (-8L)) = var_2;
    *(uint64_t *)(var_0 + (-16L)) = var_1;
    var_4 = var_0 + (-40L);
    _cast = (uint32_t *)rdi;
    var_5 = *_cast;
    local_sp_0 = var_4;
    local_sp_1 = var_4;
    if (var_5 > 4U) {
        *(uint64_t *)(var_0 + (-48L)) = 4212113UL;
        indirect_placeholder();
        return 0UL;
    }
    var_6 = (uint64_t)var_5;
    var_7 = *(uint64_t *)((var_6 << 3UL) + 4256600UL) + (-4211904L);
    switch ((var_7 >> 4UL) | (var_7 << 60UL)) {
      case 1UL:
        {
            return 0UL;
        }
        break;
      case 0UL:
        {
            var_12 = (uint32_t *)(rdi + 4UL);
            var_13 = *var_12;
            if (var_13 == 0U) {
                *(uint64_t *)(var_0 + (-48L)) = 4212181UL;
                indirect_placeholder();
                abort();
            }
            var_14 = helper_cc_compute_all_wrapper((uint64_t)var_13, 0UL, var_3, 16U);
            if ((uint64_t)(((unsigned char)(var_14 >> 4UL) ^ (unsigned char)var_14) & '\xc0') == 0UL) {
                return 0UL;
            }
            var_15 = ((int)var_5 < (int)0U);
            var_16 = (uint32_t *)var_6;
            *(uint64_t *)(local_sp_1 + (-8L)) = 4212040UL;
            indirect_placeholder();
            while (!var_15)
                {
                    var_18 = local_sp_1 + (-16L);
                    *(uint64_t *)var_18 = 4212021UL;
                    indirect_placeholder();
                    local_sp_1_be = var_18;
                    if (*var_16 == 4U) {
                        var_19 = local_sp_1 + (-24L);
                        *(uint64_t *)var_19 = 4212169UL;
                        indirect_placeholder();
                        local_sp_1_be = var_19;
                    }
                    local_sp_1 = local_sp_1_be;
                    *(uint64_t *)(local_sp_1 + (-8L)) = 4212040UL;
                    indirect_placeholder();
                }
            var_17 = (uint64_t)*(uint32_t *)(local_sp_1 + 4UL);
            *var_12 = 4294967295U;
            rax_0 = var_17;
            if ((var_17 & 127UL) != 0UL) {
                *(uint64_t *)(local_sp_1 + (-16L)) = 4212067UL;
                indirect_placeholder();
                rax_0 = (uint64_t)*(uint32_t *)(local_sp_1 + (-4L));
            }
            return (uint64_t)(unsigned char)(rax_0 >> 8UL);
        }
        break;
      case 2UL:
        {
            *(uint64_t *)(var_0 + (-48L)) = 4211944UL;
            indirect_placeholder();
            if (var_5 == 0U) {
                *_cast = 1U;
                return var_6;
            }
            *(uint64_t *)(var_0 + (-56L)) = 4211957UL;
            indirect_placeholder();
            var_8 = (uint32_t *)var_6;
            var_9 = *var_8;
            var_10 = var_0 + (-64L);
            *(uint64_t *)var_10 = 4211967UL;
            indirect_placeholder();
            *_cast = 4U;
            var_11 = (uint32_t *)(rdi + 4UL);
            *var_11 = var_9;
            _pre_phi30 = var_8;
            _pre_phi28 = var_11;
            local_sp_0 = var_10;
            *(uint64_t *)(local_sp_0 + (-8L)) = 4211989UL;
            indirect_placeholder();
            *_pre_phi30 = *_pre_phi28;
            return 4294967295UL;
        }
        break;
      case 5UL:
        {
            _pre27 = (uint32_t *)(rdi + 4UL);
            _pre29 = (uint32_t *)var_6;
            _pre_phi30 = _pre29;
            _pre_phi28 = _pre27;
            *(uint64_t *)(local_sp_0 + (-8L)) = 4211989UL;
            indirect_placeholder();
            *_pre_phi30 = *_pre_phi28;
            return 4294967295UL;
        }
        break;
      default:
        {
            abort();
        }
        break;
    }
}

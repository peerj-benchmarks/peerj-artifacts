typedef unsigned char __u_char;
typedef unsigned short __u_short;
typedef unsigned int __u_int;
typedef unsigned long __u_long;
typedef signed char __int8_t;
typedef unsigned char __uint8_t;
typedef short __int16_t;
typedef unsigned short __uint16_t;
typedef int __int32_t;
typedef unsigned int __uint32_t;
typedef long __int64_t;
typedef unsigned long __uint64_t;
typedef __int8_t __int_least8_t;
typedef __uint8_t __uint_least8_t;
typedef __int16_t __int_least16_t;
typedef __uint16_t __uint_least16_t;
typedef __int32_t __int_least32_t;
typedef __uint32_t __uint_least32_t;
typedef __int64_t __int_least64_t;
typedef __uint64_t __uint_least64_t;
typedef long __quad_t;
typedef unsigned long __u_quad_t;
typedef long __intmax_t;
typedef unsigned long __uintmax_t;
typedef unsigned long __dev_t;
typedef unsigned int __uid_t;
typedef unsigned int __gid_t;
typedef unsigned long __ino_t;
typedef unsigned long __ino64_t;
typedef unsigned int __mode_t;
typedef unsigned long __nlink_t;
typedef long __off_t;
typedef long __off64_t;
typedef int __pid_t;
typedef struct {
    int __val[2];
} __fsid_t;
typedef long __clock_t;
typedef unsigned long __rlim_t;
typedef unsigned long __rlim64_t;
typedef unsigned int __id_t;
typedef long __time_t;
typedef unsigned int __useconds_t;
typedef long __suseconds_t;
typedef int __daddr_t;
typedef int __key_t;
typedef int __clockid_t;
typedef void *__timer_t;
typedef long __blksize_t;
typedef long __blkcnt_t;
typedef long __blkcnt64_t;
typedef unsigned long __fsblkcnt_t;
typedef unsigned long __fsblkcnt64_t;
typedef unsigned long __fsfilcnt_t;
typedef unsigned long __fsfilcnt64_t;
typedef long __fsword_t;
typedef long __ssize_t;
typedef long __syscall_slong_t;
typedef unsigned long __syscall_ulong_t;
typedef __off64_t __loff_t;
typedef char *__caddr_t;
typedef long __intptr_t;
typedef unsigned int __socklen_t;
typedef int __sig_atomic_t;
typedef __int8_t int8_t;
typedef __int16_t int16_t;
typedef __int32_t int32_t;
typedef __int64_t int64_t;
typedef __uint8_t uint8_t;
typedef __uint16_t uint16_t;
typedef __uint32_t uint32_t;
typedef __uint64_t uint64_t;
typedef __int_least8_t int_least8_t;
typedef __int_least16_t int_least16_t;
typedef __int_least32_t int_least32_t;
typedef __int_least64_t int_least64_t;
typedef __uint_least8_t uint_least8_t;
typedef __uint_least16_t uint_least16_t;
typedef __uint_least32_t uint_least32_t;
typedef __uint_least64_t uint_least64_t;
typedef signed char int_fast8_t;
typedef long int_fast16_t;
typedef long int_fast32_t;
typedef long int_fast64_t;
typedef unsigned char uint_fast8_t;
typedef unsigned long uint_fast16_t;
typedef unsigned long uint_fast32_t;
typedef unsigned long uint_fast64_t;
typedef long intptr_t;
typedef unsigned long uintptr_t;
typedef __intmax_t intmax_t;
typedef __uintmax_t uintmax_t;
extern void abort(void);
extern uint64_t revng_init_local_sp(uint64_t param_0);
extern uint64_t init_rbx(void);
extern uint64_t init_rbp(void);
extern void indirect_placeholder(void);
extern uint64_t init_cc_src2(void);
extern uint64_t helper_cc_compute_c_wrapper(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint32_t param_3);
extern uint64_t init_r13(void);
extern uint64_t init_r12(void);
extern uint64_t init_r14(void);
extern uint64_t init_rax(void);
extern uint64_t helper_cc_compute_all_wrapper(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint32_t param_3);
extern uint64_t indirect_placeholder_12(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint64_t param_3);
extern void llvm_trap(void);
typedef _Bool bool;
uint64_t bb_savewd_chdir(uint64_t rcx, uint64_t rdi, uint64_t rdx, uint64_t rsi) {
    uint64_t var_0;
    uint64_t var_1;
    uint64_t var_2;
    uint64_t var_3;
    uint64_t var_4;
    uint64_t var_5;
    uint64_t var_6;
    uint64_t var_7;
    uint64_t var_8;
    uint64_t var_18;
    uint64_t local_sp_6;
    uint64_t local_sp_0;
    uint32_t *_pre78;
    uint32_t *var_19;
    uint32_t _pre_phi75;
    uint64_t local_sp_7;
    uint64_t var_16;
    uint32_t *var_17;
    uint64_t var_22;
    uint64_t local_sp_4;
    uint32_t *_pre_phi79;
    uint64_t local_sp_1;
    uint64_t rax_0;
    uint64_t var_23;
    uint32_t var_24;
    uint64_t var_25;
    uint32_t var_30;
    uint64_t var_31;
    uint64_t local_sp_3;
    uint64_t var_32;
    uint32_t var_33;
    uint64_t var_34;
    uint64_t var_35;
    uint64_t var_36;
    uint64_t rax_1;
    uint64_t var_38;
    uint64_t rax_2;
    uint64_t r12_0;
    uint32_t *var_39;
    uint32_t var_40;
    uint64_t rax_3;
    uint64_t var_37;
    uint64_t r12_1;
    uint32_t *var_9;
    uint32_t var_10;
    uint64_t rax_6;
    uint32_t var_11;
    uint64_t var_12;
    uint32_t *var_20;
    uint32_t *_pre_phi73;
    uint32_t _pre74;
    uint64_t rax_5;
    uint64_t var_21;
    uint64_t var_13;
    uint64_t var_14;
    uint32_t var_15;
    uint64_t var_26;
    uint64_t rax_7;
    uint64_t var_27;
    uint64_t *var_28;
    uint64_t var_29;
    var_0 = revng_init_local_sp(0UL);
    var_1 = init_rax();
    var_2 = init_rbx();
    var_3 = init_rbp();
    var_4 = init_r13();
    var_5 = init_cc_src2();
    var_6 = init_r12();
    var_7 = init_r14();
    *(uint64_t *)(var_0 + (-8L)) = var_7;
    *(uint64_t *)(var_0 + (-16L)) = var_4;
    *(uint64_t *)(var_0 + (-24L)) = var_6;
    *(uint64_t *)(var_0 + (-32L)) = var_3;
    var_8 = var_0 + (-40L);
    *(uint64_t *)var_8 = var_2;
    local_sp_6 = var_8;
    local_sp_7 = var_8;
    rax_0 = 1UL;
    var_30 = (uint32_t)0L;
    rax_2 = (uint64_t)0L;
    r12_0 = 0UL;
    rax_6 = var_1;
    if (rcx == 0UL) {
        helper_cc_compute_c_wrapper((rdx & 1UL) + (-1L), 1UL, var_5, 16U);
        *(uint64_t *)(var_0 + (-48L)) = 4211337UL;
        indirect_placeholder();
        *(uint32_t *)rcx = 0U;
        *(uint64_t *)(var_0 + (-56L)) = 4211347UL;
        indirect_placeholder();
        llvm_trap();
        abort();
    }
    var_9 = (uint32_t *)rdi;
    var_10 = *var_9;
    if (var_10 <= 5U) {
        var_26 = var_8 + (-8L);
        *(uint64_t *)var_26 = 4211422UL;
        indirect_placeholder();
        local_sp_7 = var_26;
        rax_7 = rax_6;
        var_27 = local_sp_7 + (-8L);
        var_28 = (uint64_t *)var_27;
        local_sp_3 = var_27;
        local_sp_4 = var_27;
        rax_3 = rax_7;
        if (1) {
            *var_28 = 4211592UL;
            indirect_placeholder();
            var_29 = (uint64_t)(uint32_t)rax_7;
            r12_1 = var_29;
            if (var_29 != 0UL) {
                return (uint64_t)(uint32_t)r12_1;
            }
            var_30 = *var_9;
        } else {
            *var_28 = 4211437UL;
            indirect_placeholder();
            local_sp_4 = (uint64_t)0L;
            r12_0 = (uint64_t)0L;
            if ((uint64_t)(uint32_t)rax_7 != 0UL) {
                r12_1 = r12_0;
                if (1) {
                    return (uint64_t)(uint32_t)r12_1;
                }
                *(uint64_t *)(local_sp_4 + (-8L)) = 4211467UL;
                indirect_placeholder();
                var_39 = (uint32_t *)rax_2;
                var_40 = *var_39;
                *(uint64_t *)(local_sp_4 + (-16L)) = 4211476UL;
                indirect_placeholder();
                *(uint64_t *)(local_sp_4 + (-24L)) = 4211481UL;
                indirect_placeholder();
                *var_39 = var_40;
                return r12_0;
            }
        }
        if (var_30 <= 5U) {
            var_37 = var_27 + (-8L);
            *(uint64_t *)var_37 = 4211649UL;
            indirect_placeholder();
            local_sp_3 = var_37;
            rax_1 = rax_3;
            var_38 = (rax_1 & (-256L)) | 1UL;
            local_sp_4 = local_sp_3;
            rax_2 = var_38;
            r12_1 = r12_0;
            if (1) {
                return (uint64_t)(uint32_t)r12_1;
            }
            *(uint64_t *)(local_sp_4 + (-8L)) = 4211467UL;
            indirect_placeholder();
            var_39 = (uint32_t *)rax_2;
            var_40 = *var_39;
            *(uint64_t *)(local_sp_4 + (-16L)) = 4211476UL;
            indirect_placeholder();
            *(uint64_t *)(local_sp_4 + (-24L)) = 4211481UL;
            indirect_placeholder();
            *var_39 = var_40;
            return r12_0;
        }
        var_31 = (uint64_t)var_30;
        rax_1 = var_31;
        rax_3 = var_31;
        switch (*(uint64_t *)((var_31 << 3UL) + 4256552UL)) {
          case 4211792UL:
            {
                *var_9 = 2U;
                var_32 = (uint64_t)(var_30 & (-256)) | 1UL;
                rax_2 = var_32;
            }
            break;
          case 4211744UL:
            {
                var_33 = *(uint32_t *)(rdi + 4UL);
                var_34 = (uint64_t)var_33;
                rax_1 = var_34;
                if (var_33 == 0U) {
                    var_38 = (rax_1 & (-256L)) | 1UL;
                    local_sp_4 = local_sp_3;
                    rax_2 = var_38;
                } else {
                    var_35 = var_27 + (-8L);
                    *(uint64_t *)var_35 = 4211777UL;
                    indirect_placeholder();
                    var_36 = (uint64_t)(var_33 & (-256)) | 1UL;
                    local_sp_4 = var_35;
                    rax_2 = var_36;
                }
            }
            break;
          case 4211649UL:
          case 4211624UL:
            {
                switch (*(uint64_t *)((var_31 << 3UL) + 4256552UL)) {
                  case 4211624UL:
                    {
                        var_37 = var_27 + (-8L);
                        *(uint64_t *)var_37 = 4211649UL;
                        indirect_placeholder();
                        local_sp_3 = var_37;
                        rax_1 = rax_3;
                    }
                    break;
                  case 4211649UL:
                    {
                        var_38 = (rax_1 & (-256L)) | 1UL;
                        local_sp_4 = local_sp_3;
                        rax_2 = var_38;
                    }
                    break;
                }
            }
            break;
          default:
            {
                abort();
            }
            break;
        }
        r12_1 = r12_0;
        if (1) {
            return (uint64_t)(uint32_t)r12_1;
        }
        *(uint64_t *)(local_sp_4 + (-8L)) = 4211467UL;
        indirect_placeholder();
        var_39 = (uint32_t *)rax_2;
        var_40 = *var_39;
        *(uint64_t *)(local_sp_4 + (-16L)) = 4211476UL;
        indirect_placeholder();
        *(uint64_t *)(local_sp_4 + (-24L)) = 4211481UL;
        indirect_placeholder();
        *var_39 = var_40;
        return r12_0;
    }
    var_11 = var_10 & 63U;
    var_12 = 1UL << (uint64_t)var_11;
    rax_2 = 1UL;
    r12_0 = 4294967294UL;
    rax_6 = var_12;
    rax_5 = var_12;
    rax_7 = var_12;
    if ((var_12 & 54UL) == 0UL) {
        return;
    }
    switch (var_11) {
      case 3U:
        {
            var_20 = (uint32_t *)(rdi + 4UL);
            _pre_phi73 = var_20;
            if ((int)*var_20 <= (int)4294967295U) {
                _pre74 = (uint32_t)var_12;
                _pre_phi75 = _pre74;
                var_21 = local_sp_6 + (-8L);
                *(uint64_t *)var_21 = 4211537UL;
                indirect_placeholder();
                *_pre_phi73 = _pre_phi75;
                local_sp_7 = var_21;
                local_sp_4 = var_21;
                _pre_phi79 = _pre_phi73;
                local_sp_1 = var_21;
                rax_7 = rax_5;
                if ((uint64_t)_pre_phi75 != 0UL) {
                    var_22 = helper_cc_compute_all_wrapper(rax_5, 0UL, var_5, 16U);
                    if ((uint64_t)(((unsigned char)(var_22 >> 4UL) ^ (unsigned char)var_22) & '\xc0') != 0UL) {
                        r12_1 = r12_0;
                        if (1) {
                            return (uint64_t)(uint32_t)r12_1;
                        }
                        *(uint64_t *)(local_sp_4 + (-8L)) = 4211467UL;
                        indirect_placeholder();
                        var_39 = (uint32_t *)rax_2;
                        var_40 = *var_39;
                        *(uint64_t *)(local_sp_4 + (-16L)) = 4211476UL;
                        indirect_placeholder();
                        *(uint64_t *)(local_sp_4 + (-24L)) = 4211481UL;
                        indirect_placeholder();
                        *var_39 = var_40;
                        return r12_0;
                    }
                    *var_9 = 4U;
                    var_23 = local_sp_1 + (-8L);
                    *(uint64_t *)var_23 = 4211571UL;
                    indirect_placeholder();
                    var_24 = *(uint32_t *)rax_0;
                    var_25 = (uint64_t)var_24;
                    *_pre_phi79 = var_24;
                    local_sp_7 = var_23;
                    rax_7 = var_25;
                }
            }
        }
        break;
      case 0U:
        {
            var_13 = var_8 + (-8L);
            *(uint64_t *)var_13 = 4211721UL;
            var_14 = indirect_placeholder_12(0UL, 4254584UL, rdx, 2097152UL);
            var_15 = (uint32_t)var_14;
            _pre_phi75 = var_15;
            local_sp_7 = var_13;
            rax_0 = var_14;
            rax_5 = var_14;
            rax_7 = var_14;
            if ((int)var_15 < (int)0U) {
                *var_9 = 1U;
                *(uint32_t *)(rdi + 4UL) = var_15;
            } else {
                var_16 = var_8 + (-16L);
                *(uint64_t *)var_16 = 4211818UL;
                indirect_placeholder();
                var_17 = (uint32_t *)var_14;
                local_sp_0 = var_16;
                if (*var_17 == 13U) {
                    var_18 = var_8 + (-24L);
                    *(uint64_t *)var_18 = 4211828UL;
                    indirect_placeholder();
                    local_sp_0 = var_18;
                    local_sp_1 = var_18;
                    if (*var_17 == 116U) {
                        *var_9 = 3U;
                        var_19 = (uint32_t *)(rdi + 4UL);
                        *var_19 = 4294967295U;
                        _pre_phi73 = var_19;
                        local_sp_6 = local_sp_0;
                        var_21 = local_sp_6 + (-8L);
                        *(uint64_t *)var_21 = 4211537UL;
                        indirect_placeholder();
                        *_pre_phi73 = _pre_phi75;
                        local_sp_7 = var_21;
                        local_sp_4 = var_21;
                        _pre_phi79 = _pre_phi73;
                        local_sp_1 = var_21;
                        rax_7 = rax_5;
                        if ((uint64_t)_pre_phi75 != 0UL) {
                            var_22 = helper_cc_compute_all_wrapper(rax_5, 0UL, var_5, 16U);
                            if ((uint64_t)(((unsigned char)(var_22 >> 4UL) ^ (unsigned char)var_22) & '\xc0') == 0UL) {
                                r12_1 = r12_0;
                                if (!1) {
                                    return (uint64_t)(uint32_t)r12_1;
                                }
                                *(uint64_t *)(local_sp_4 + (-8L)) = 4211467UL;
                                indirect_placeholder();
                                var_39 = (uint32_t *)rax_2;
                                var_40 = *var_39;
                                *(uint64_t *)(local_sp_4 + (-16L)) = 4211476UL;
                                indirect_placeholder();
                                *(uint64_t *)(local_sp_4 + (-24L)) = 4211481UL;
                                indirect_placeholder();
                                *var_39 = var_40;
                                return r12_0;
                            }
                            *var_9 = 4U;
                            var_23 = local_sp_1 + (-8L);
                            *(uint64_t *)var_23 = 4211571UL;
                            indirect_placeholder();
                            var_24 = *(uint32_t *)rax_0;
                            var_25 = (uint64_t)var_24;
                            *_pre_phi79 = var_24;
                            local_sp_7 = var_23;
                            rax_7 = var_25;
                        }
                    } else {
                        _pre78 = (uint32_t *)(rdi + 4UL);
                        _pre_phi79 = _pre78;
                        *var_9 = 4U;
                        var_23 = local_sp_1 + (-8L);
                        *(uint64_t *)var_23 = 4211571UL;
                        indirect_placeholder();
                        var_24 = *(uint32_t *)rax_0;
                        var_25 = (uint64_t)var_24;
                        *_pre_phi79 = var_24;
                        local_sp_7 = var_23;
                        rax_7 = var_25;
                    }
                } else {
                    *var_9 = 3U;
                    var_19 = (uint32_t *)(rdi + 4UL);
                    *var_19 = 4294967295U;
                    _pre_phi73 = var_19;
                    local_sp_6 = local_sp_0;
                    var_21 = local_sp_6 + (-8L);
                    *(uint64_t *)var_21 = 4211537UL;
                    indirect_placeholder();
                    *_pre_phi73 = _pre_phi75;
                    local_sp_7 = var_21;
                    local_sp_4 = var_21;
                    _pre_phi79 = _pre_phi73;
                    local_sp_1 = var_21;
                    rax_7 = rax_5;
                    if ((uint64_t)_pre_phi75 != 0UL) {
                        var_22 = helper_cc_compute_all_wrapper(rax_5, 0UL, var_5, 16U);
                        if ((uint64_t)(((unsigned char)(var_22 >> 4UL) ^ (unsigned char)var_22) & '\xc0') == 0UL) {
                            r12_1 = r12_0;
                            if (!1) {
                                return (uint64_t)(uint32_t)r12_1;
                            }
                            *(uint64_t *)(local_sp_4 + (-8L)) = 4211467UL;
                            indirect_placeholder();
                            var_39 = (uint32_t *)rax_2;
                            var_40 = *var_39;
                            *(uint64_t *)(local_sp_4 + (-16L)) = 4211476UL;
                            indirect_placeholder();
                            *(uint64_t *)(local_sp_4 + (-24L)) = 4211481UL;
                            indirect_placeholder();
                            *var_39 = var_40;
                            return r12_0;
                        }
                        *var_9 = 4U;
                        var_23 = local_sp_1 + (-8L);
                        *(uint64_t *)var_23 = 4211571UL;
                        indirect_placeholder();
                        var_24 = *(uint32_t *)rax_0;
                        var_25 = (uint64_t)var_24;
                        *_pre_phi79 = var_24;
                        local_sp_7 = var_23;
                        rax_7 = var_25;
                    }
                }
            }
        }
        break;
      default:
        {
            var_26 = var_8 + (-8L);
            *(uint64_t *)var_26 = 4211422UL;
            indirect_placeholder();
            local_sp_7 = var_26;
            rax_7 = rax_6;
        }
        break;
    }
}

typedef unsigned char __u_char;
typedef unsigned short __u_short;
typedef unsigned int __u_int;
typedef unsigned long __u_long;
typedef signed char __int8_t;
typedef unsigned char __uint8_t;
typedef short __int16_t;
typedef unsigned short __uint16_t;
typedef int __int32_t;
typedef unsigned int __uint32_t;
typedef long __int64_t;
typedef unsigned long __uint64_t;
typedef __int8_t __int_least8_t;
typedef __uint8_t __uint_least8_t;
typedef __int16_t __int_least16_t;
typedef __uint16_t __uint_least16_t;
typedef __int32_t __int_least32_t;
typedef __uint32_t __uint_least32_t;
typedef __int64_t __int_least64_t;
typedef __uint64_t __uint_least64_t;
typedef long __quad_t;
typedef unsigned long __u_quad_t;
typedef long __intmax_t;
typedef unsigned long __uintmax_t;
typedef unsigned long __dev_t;
typedef unsigned int __uid_t;
typedef unsigned int __gid_t;
typedef unsigned long __ino_t;
typedef unsigned long __ino64_t;
typedef unsigned int __mode_t;
typedef unsigned long __nlink_t;
typedef long __off_t;
typedef long __off64_t;
typedef int __pid_t;
typedef struct {
    int __val[2];
} __fsid_t;
typedef long __clock_t;
typedef unsigned long __rlim_t;
typedef unsigned long __rlim64_t;
typedef unsigned int __id_t;
typedef long __time_t;
typedef unsigned int __useconds_t;
typedef long __suseconds_t;
typedef int __daddr_t;
typedef int __key_t;
typedef int __clockid_t;
typedef void *__timer_t;
typedef long __blksize_t;
typedef long __blkcnt_t;
typedef long __blkcnt64_t;
typedef unsigned long __fsblkcnt_t;
typedef unsigned long __fsblkcnt64_t;
typedef unsigned long __fsfilcnt_t;
typedef unsigned long __fsfilcnt64_t;
typedef long __fsword_t;
typedef long __ssize_t;
typedef long __syscall_slong_t;
typedef unsigned long __syscall_ulong_t;
typedef __off64_t __loff_t;
typedef char *__caddr_t;
typedef long __intptr_t;
typedef unsigned int __socklen_t;
typedef int __sig_atomic_t;
typedef __int8_t int8_t;
typedef __int16_t int16_t;
typedef __int32_t int32_t;
typedef __int64_t int64_t;
typedef __uint8_t uint8_t;
typedef __uint16_t uint16_t;
typedef __uint32_t uint32_t;
typedef __uint64_t uint64_t;
typedef __int_least8_t int_least8_t;
typedef __int_least16_t int_least16_t;
typedef __int_least32_t int_least32_t;
typedef __int_least64_t int_least64_t;
typedef __uint_least8_t uint_least8_t;
typedef __uint_least16_t uint_least16_t;
typedef __uint_least32_t uint_least32_t;
typedef __uint_least64_t uint_least64_t;
typedef signed char int_fast8_t;
typedef long int_fast16_t;
typedef long int_fast32_t;
typedef long int_fast64_t;
typedef unsigned char uint_fast8_t;
typedef unsigned long uint_fast16_t;
typedef unsigned long uint_fast32_t;
typedef unsigned long uint_fast64_t;
typedef long intptr_t;
typedef unsigned long uintptr_t;
typedef __intmax_t intmax_t;
typedef __uintmax_t uintmax_t;
typedef _Bool bool;
extern uint64_t revng_init_local_sp(uint64_t param_0);
extern uint64_t init_rbx(void);
extern uint64_t init_rbp(void);
extern uint64_t init_cc_src2(void);
extern uint64_t helper_cc_compute_c_wrapper(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint32_t param_3);
extern uint64_t init_r13(void);
extern uint64_t init_r12(void);
extern uint64_t init_r14(void);
extern uint64_t init_rax(void);
extern uint64_t init_r15(void);
extern uint64_t indirect_placeholder_18(uint64_t param_0, uint64_t param_1, uint64_t param_2);
extern uint64_t indirect_placeholder_21(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint64_t param_3);
uint64_t bb_base64_decode_ctx(uint64_t rcx, uint64_t rdi, uint64_t r8, uint64_t rsi, uint64_t rdx) {
    uint64_t var_0;
    uint64_t var_1;
    uint64_t var_2;
    uint64_t var_3;
    uint64_t var_4;
    uint64_t var_5;
    uint64_t var_6;
    uint64_t var_7;
    uint64_t var_8;
    uint64_t var_9;
    uint64_t var_10;
    bool var_11;
    uint64_t var_12;
    uint64_t rdx5_0;
    uint64_t var_53;
    uint64_t rbx_5;
    uint64_t r14_1;
    uint64_t rbx_1;
    uint64_t rdi2_1;
    uint64_t r14_5;
    uint64_t var_60;
    uint64_t rax_2;
    uint32_t *var_13;
    uint32_t var_14;
    uint64_t var_15;
    uint64_t var_16;
    uint32_t *_pre_phi;
    uint64_t rax_0_ph;
    uint64_t r12_0_ph;
    uint64_t var_17;
    uint64_t var_18;
    bool var_19;
    uint64_t r14_3;
    uint64_t rbx_0;
    uint64_t local_sp_3;
    uint64_t rax_0;
    uint64_t r15_3;
    uint64_t local_sp_0;
    uint64_t rdx5_1;
    uint64_t r15_0;
    uint64_t r14_0;
    uint64_t var_20;
    uint64_t var_21;
    uint64_t rbx_2;
    uint64_t local_sp_1;
    uint64_t r15_1;
    uint64_t var_22;
    uint64_t var_23;
    uint64_t var_24;
    uint64_t var_25;
    bool var_26;
    uint64_t var_27;
    uint64_t rax_1;
    uint64_t local_sp_2;
    uint64_t r15_2;
    uint64_t r14_2;
    bool var_28;
    uint64_t var_29;
    uint64_t var_30;
    uint64_t rax_7;
    uint64_t var_31;
    uint64_t *var_32;
    uint64_t var_33;
    uint64_t rbx_4;
    uint64_t var_37;
    uint64_t var_38;
    uint64_t var_34;
    uint64_t *var_35;
    uint64_t var_36;
    uint64_t var_39;
    uint64_t *var_40;
    uint32_t var_41;
    uint64_t rax_3;
    uint32_t var_44;
    uint64_t var_42;
    uint64_t var_43;
    uint64_t local_sp_4;
    uint64_t local_sp_5;
    uint64_t var_45;
    uint64_t rax_6;
    uint64_t rax_5;
    uint64_t rax_4;
    uint64_t r15_4;
    uint64_t var_46;
    unsigned char var_47;
    uint32_t var_48;
    uint32_t var_49;
    uint64_t var_50;
    uint64_t var_51;
    uint64_t r15_5;
    uint64_t var_52;
    uint64_t local_sp_7;
    uint64_t local_sp_6;
    uint64_t r15_7;
    uint64_t var_54;
    uint64_t var_55;
    uint64_t var_56;
    uint64_t var_57;
    bool var_58;
    uint64_t var_59;
    uint64_t rdx5_3;
    uint64_t *var_61;
    unsigned int loop_state_var;
    bool switch_state_var;
    var_0 = revng_init_local_sp(0UL);
    var_1 = init_rax();
    var_2 = init_rbx();
    var_3 = init_rbp();
    var_4 = init_r13();
    var_5 = init_cc_src2();
    var_6 = init_r12();
    var_7 = init_r15();
    var_8 = init_r14();
    *(uint64_t *)(var_0 + (-8L)) = var_7;
    *(uint64_t *)(var_0 + (-16L)) = var_8;
    *(uint64_t *)(var_0 + (-24L)) = var_4;
    *(uint64_t *)(var_0 + (-32L)) = var_6;
    *(uint64_t *)(var_0 + (-40L)) = var_3;
    *(uint64_t *)(var_0 + (-48L)) = var_2;
    var_9 = var_0 + (-104L);
    var_10 = *(uint64_t *)r8;
    *(uint64_t *)(var_0 + (-80L)) = rcx;
    *(uint64_t *)(var_0 + (-88L)) = r8;
    var_11 = (rdi != 0UL);
    var_12 = var_11;
    *(uint64_t *)(var_0 + (-64L)) = var_10;
    r14_5 = 4UL;
    rax_0_ph = var_1;
    r12_0_ph = 0UL;
    rbx_0 = var_10;
    local_sp_0 = var_9;
    r15_0 = rsi;
    r14_0 = rdx;
    rax_7 = 1UL;
    var_44 = 0U;
    if (rdi == 0UL) {
        *(unsigned char *)(var_0 + (-89L)) = (unsigned char)'\x01';
        _pre_phi = (uint32_t *)rdi;
    } else {
        var_13 = (uint32_t *)rdi;
        var_14 = *var_13;
        var_15 = (uint64_t)var_14;
        var_16 = (var_6 & (-256L)) | (rdx == 0UL);
        *(unsigned char *)(var_0 + (-89L)) = (var_14 == 0U);
        _pre_phi = var_13;
        rax_0_ph = var_15;
        r12_0_ph = var_16;
    }
    var_17 = (uint64_t)(unsigned char)r12_0_ph;
    var_18 = rdi + 4UL;
    var_19 = (var_17 < var_12);
    rax_0 = rax_0_ph;
    while (1U)
        {
            var_20 = (uint64_t)*(unsigned char *)(local_sp_0 + 15UL);
            var_21 = helper_cc_compute_c_wrapper(r12_0_ph - var_20, var_20, var_5, 14U);
            rdx5_0 = rbx_0;
            r14_1 = r14_0;
            rbx_1 = rbx_0;
            rbx_2 = rbx_0;
            local_sp_1 = local_sp_0;
            r15_1 = r15_0;
            rax_1 = rax_0;
            local_sp_2 = local_sp_0;
            r15_2 = r15_0;
            r14_2 = r14_0;
            if (var_21 == 0UL) {
                var_22 = local_sp_1 + 40UL;
                var_23 = local_sp_1 + 24UL;
                var_24 = local_sp_1 + (-8L);
                *(uint64_t *)var_24 = 4204986UL;
                var_25 = indirect_placeholder_21(var_22, r15_1, r14_1, var_23);
                var_26 = ((uint64_t)(unsigned char)var_25 == 0UL);
                var_27 = *(uint64_t *)(local_sp_1 + 32UL);
                rdx5_0 = var_27;
                rbx_1 = var_27;
                rbx_2 = rbx_1;
                local_sp_1 = var_24;
                rax_1 = var_25;
                local_sp_2 = var_24;
                r15_2 = r15_1;
                r14_2 = r14_1;
                while (!var_26)
                    {
                        r15_1 = r15_1 + 4UL;
                        r14_1 = r14_1 + (-4L);
                        var_22 = local_sp_1 + 40UL;
                        var_23 = local_sp_1 + 24UL;
                        var_24 = local_sp_1 + (-8L);
                        *(uint64_t *)var_24 = 4204986UL;
                        var_25 = indirect_placeholder_21(var_22, r15_1, r14_1, var_23);
                        var_26 = ((uint64_t)(unsigned char)var_25 == 0UL);
                        var_27 = *(uint64_t *)(local_sp_1 + 32UL);
                        rdx5_0 = var_27;
                        rbx_1 = var_27;
                        rbx_2 = rbx_1;
                        local_sp_1 = var_24;
                        rax_1 = var_25;
                        local_sp_2 = var_24;
                        r15_2 = r15_1;
                        r14_2 = r14_1;
                    }
            }
            var_28 = (r14_2 == 0UL);
            var_29 = var_28;
            var_30 = (rax_1 & (-256L)) | var_29;
            rdi2_1 = r15_2;
            rax_2 = var_30;
            local_sp_3 = local_sp_2;
            rdx5_1 = rdx5_0;
            rbx_4 = r15_2;
            local_sp_4 = local_sp_2;
            local_sp_5 = local_sp_2;
            r15_4 = r15_2;
            r15_5 = r15_2;
            local_sp_7 = local_sp_2;
            local_sp_6 = local_sp_2;
            r15_7 = r15_2;
            rdx5_3 = rdx5_0;
            if (var_17 >= var_29) {
                loop_state_var = 0U;
                break;
            }
            rdx5_3 = rbx_2;
            if (var_28) {
                var_39 = rdx5_0 - rbx_2;
                var_40 = (uint64_t *)(local_sp_2 + 24UL);
                *var_40 = (*var_40 + var_39);
                *(uint64_t *)(local_sp_2 + 40UL) = rbx_2;
                if (!var_11) {
                    loop_state_var = 0U;
                    break;
                }
            }
            r14_5 = r14_2;
            if (*(unsigned char *)r15_2 != '\n') {
                if (!var_11) {
                    var_37 = r15_2 + 1UL;
                    var_38 = r14_2 + (-1L);
                    r15_3 = var_37;
                    r14_3 = var_38;
                    rbx_0 = rdx5_1;
                    rax_0 = rax_2;
                    local_sp_0 = local_sp_3;
                    r15_0 = r15_3;
                    r14_0 = r14_3;
                    continue;
                }
                var_34 = rdx5_0 - rbx_2;
                *(uint64_t *)(local_sp_2 + 40UL) = rbx_2;
                var_35 = (uint64_t *)(local_sp_2 + 24UL);
                *var_35 = (*var_35 + var_34);
                var_36 = r14_2 + r15_2;
                rbx_5 = var_36;
                var_54 = local_sp_6 + 40UL;
                var_55 = local_sp_6 + 24UL;
                var_56 = local_sp_6 + (-8L);
                *(uint64_t *)var_56 = 4204913UL;
                var_57 = indirect_placeholder_21(var_54, rdi2_1, r14_5, var_55);
                var_58 = ((uint64_t)(unsigned char)var_57 == 0UL);
                var_59 = *(uint64_t *)(local_sp_6 + 32UL);
                rax_2 = var_57;
                local_sp_3 = var_56;
                r15_3 = r15_7;
                rdx5_1 = var_59;
                rax_7 = var_57;
                local_sp_7 = var_56;
                rdx5_3 = var_59;
                if (!var_58) {
                    loop_state_var = 0U;
                    break;
                }
                var_60 = rbx_5 - r15_7;
                r14_3 = var_60;
                rbx_0 = rdx5_1;
                rax_0 = rax_2;
                local_sp_0 = local_sp_3;
                r15_0 = r15_3;
                r14_0 = r14_3;
                continue;
            }
            var_31 = rdx5_0 - rbx_2;
            var_32 = (uint64_t *)(local_sp_2 + 24UL);
            *var_32 = (*var_32 + var_31);
            *(uint64_t *)(local_sp_2 + 40UL) = rbx_2;
            var_33 = r14_2 + r15_2;
            rbx_4 = var_33;
            rbx_5 = var_33;
            if (!var_11) {
                var_54 = local_sp_6 + 40UL;
                var_55 = local_sp_6 + 24UL;
                var_56 = local_sp_6 + (-8L);
                *(uint64_t *)var_56 = 4204913UL;
                var_57 = indirect_placeholder_21(var_54, rdi2_1, r14_5, var_55);
                var_58 = ((uint64_t)(unsigned char)var_57 == 0UL);
                var_59 = *(uint64_t *)(local_sp_6 + 32UL);
                rax_2 = var_57;
                local_sp_3 = var_56;
                r15_3 = r15_7;
                rdx5_1 = var_59;
                rax_7 = var_57;
                local_sp_7 = var_56;
                rdx5_3 = var_59;
                if (!var_58) {
                    loop_state_var = 0U;
                    break;
                }
                var_60 = rbx_5 - r15_7;
                r14_3 = var_60;
                rbx_0 = rdx5_1;
                rax_0 = rax_2;
                local_sp_0 = local_sp_3;
                r15_0 = r15_3;
                r14_0 = r14_3;
                continue;
            }
            var_41 = *_pre_phi;
            rax_3 = (uint64_t)var_41;
            rbx_5 = rbx_4;
            rdi2_1 = var_18;
            if ((uint64_t)(var_41 + (-4)) == 0UL) {
                *_pre_phi = 0U;
                rdi2_1 = r15_2;
                if ((long)r14_2 <= (long)3UL) {
                    var_42 = local_sp_2 + (-8L);
                    *(uint64_t *)var_42 = 4205239UL;
                    var_43 = indirect_placeholder_18(r15_2, 10UL, 4UL);
                    local_sp_4 = var_42;
                    local_sp_6 = var_42;
                    if (var_43 != 0UL) {
                        var_53 = r15_2 + 4UL;
                        r15_7 = var_53;
                        var_54 = local_sp_6 + 40UL;
                        var_55 = local_sp_6 + 24UL;
                        var_56 = local_sp_6 + (-8L);
                        *(uint64_t *)var_56 = 4204913UL;
                        var_57 = indirect_placeholder_21(var_54, rdi2_1, r14_5, var_55);
                        var_58 = ((uint64_t)(unsigned char)var_57 == 0UL);
                        var_59 = *(uint64_t *)(local_sp_6 + 32UL);
                        rax_2 = var_57;
                        local_sp_3 = var_56;
                        r15_3 = r15_7;
                        rdx5_1 = var_59;
                        rax_7 = var_57;
                        local_sp_7 = var_56;
                        rdx5_3 = var_59;
                        if (var_58) {
                            loop_state_var = 0U;
                            break;
                        }
                        var_60 = rbx_5 - r15_7;
                        r14_3 = var_60;
                        rbx_0 = rdx5_1;
                        rax_0 = rax_2;
                        local_sp_0 = local_sp_3;
                        r15_0 = r15_3;
                        r14_0 = r14_3;
                        continue;
                    }
                    var_44 = *_pre_phi;
                }
                rax_3 = (uint64_t)var_44;
                local_sp_5 = local_sp_4;
            } else {
                if (var_41 != 0U) {
                    rdi2_1 = r15_2;
                    if ((long)r14_2 <= (long)3UL) {
                        var_42 = local_sp_2 + (-8L);
                        *(uint64_t *)var_42 = 4205239UL;
                        var_43 = indirect_placeholder_18(r15_2, 10UL, 4UL);
                        local_sp_4 = var_42;
                        local_sp_6 = var_42;
                        if (var_43 != 0UL) {
                            var_53 = r15_2 + 4UL;
                            r15_7 = var_53;
                            var_54 = local_sp_6 + 40UL;
                            var_55 = local_sp_6 + 24UL;
                            var_56 = local_sp_6 + (-8L);
                            *(uint64_t *)var_56 = 4204913UL;
                            var_57 = indirect_placeholder_21(var_54, rdi2_1, r14_5, var_55);
                            var_58 = ((uint64_t)(unsigned char)var_57 == 0UL);
                            var_59 = *(uint64_t *)(local_sp_6 + 32UL);
                            rax_2 = var_57;
                            local_sp_3 = var_56;
                            r15_3 = r15_7;
                            rdx5_1 = var_59;
                            rax_7 = var_57;
                            local_sp_7 = var_56;
                            rdx5_3 = var_59;
                            if (!var_58) {
                                loop_state_var = 0U;
                                break;
                            }
                            var_60 = rbx_5 - r15_7;
                            r14_3 = var_60;
                            rbx_0 = rdx5_1;
                            rax_0 = rax_2;
                            local_sp_0 = local_sp_3;
                            r15_0 = r15_3;
                            r14_0 = r14_3;
                            continue;
                        }
                        var_44 = *_pre_phi;
                    }
                    rax_3 = (uint64_t)var_44;
                    local_sp_5 = local_sp_4;
                }
            }
            var_45 = helper_cc_compute_c_wrapper(r15_2 - rbx_4, rbx_4, var_5, 17U);
            rax_4 = rax_3;
            rax_6 = rax_3;
            local_sp_6 = local_sp_5;
            local_sp_7 = local_sp_5;
            if (var_45 != 0UL) {
                var_52 = (uint64_t)(uint32_t)rax_6;
                r15_7 = r15_5;
                r14_5 = var_52;
                if (var_52 == 0UL) {
                    loop_state_var = 1U;
                    break;
                }
                if (var_52 <= 3UL) {
                    if (var_19) {
                        loop_state_var = 1U;
                        break;
                    }
                }
                var_54 = local_sp_6 + 40UL;
                var_55 = local_sp_6 + 24UL;
                var_56 = local_sp_6 + (-8L);
                *(uint64_t *)var_56 = 4204913UL;
                var_57 = indirect_placeholder_21(var_54, rdi2_1, r14_5, var_55);
                var_58 = ((uint64_t)(unsigned char)var_57 == 0UL);
                var_59 = *(uint64_t *)(local_sp_6 + 32UL);
                rax_2 = var_57;
                local_sp_3 = var_56;
                r15_3 = r15_7;
                rdx5_1 = var_59;
                rax_7 = var_57;
                local_sp_7 = var_56;
                rdx5_3 = var_59;
                if (!var_58) {
                    loop_state_var = 0U;
                    break;
                }
                var_60 = rbx_5 - r15_7;
                r14_3 = var_60;
                rbx_0 = rdx5_1;
                rax_0 = rax_2;
                local_sp_0 = local_sp_3;
                r15_0 = r15_3;
                r14_0 = r14_3;
                continue;
            }
            while (1U)
                {
                    var_46 = r15_4 + 1UL;
                    var_47 = *(unsigned char *)r15_4;
                    r15_4 = var_46;
                    rax_5 = rax_4;
                    r15_5 = rbx_4;
                    r15_7 = var_46;
                    if (var_47 != '\n') {
                        var_48 = (uint32_t)rax_4;
                        var_49 = var_48 + 1U;
                        var_50 = (uint64_t)var_49;
                        var_51 = (uint64_t)(var_48 + (-3));
                        *_pre_phi = var_49;
                        *(unsigned char *)((rax_4 + rdi) + 4UL) = var_47;
                        rax_5 = var_50;
                        if (var_51 != 0UL) {
                            loop_state_var = 0U;
                            break;
                        }
                    }
                    rax_4 = rax_5;
                    rax_6 = rax_5;
                    if (var_46 == rbx_4) {
                        continue;
                    }
                    loop_state_var = 1U;
                    break;
                }
            switch_state_var = 0;
            switch (loop_state_var) {
              case 0U:
                {
                    var_54 = local_sp_6 + 40UL;
                    var_55 = local_sp_6 + 24UL;
                    var_56 = local_sp_6 + (-8L);
                    *(uint64_t *)var_56 = 4204913UL;
                    var_57 = indirect_placeholder_21(var_54, rdi2_1, r14_5, var_55);
                    var_58 = ((uint64_t)(unsigned char)var_57 == 0UL);
                    var_59 = *(uint64_t *)(local_sp_6 + 32UL);
                    rax_2 = var_57;
                    local_sp_3 = var_56;
                    r15_3 = r15_7;
                    rdx5_1 = var_59;
                    rax_7 = var_57;
                    local_sp_7 = var_56;
                    rdx5_3 = var_59;
                    if (var_58) {
                        loop_state_var = 0U;
                        switch_state_var = 1;
                        break;
                    }
                    var_60 = rbx_5 - r15_7;
                    r14_3 = var_60;
                    rbx_0 = rdx5_1;
                    rax_0 = rax_2;
                    local_sp_0 = local_sp_3;
                    r15_0 = r15_3;
                    r14_0 = r14_3;
                    continue;
                }
                break;
              case 1U:
                {
                    var_52 = (uint64_t)(uint32_t)rax_6;
                    r15_7 = r15_5;
                    r14_5 = var_52;
                    if (var_52 != 0UL) {
                        loop_state_var = 1U;
                        switch_state_var = 1;
                        break;
                    }
                    if (var_52 <= 3UL & !var_19) {
                        loop_state_var = 1U;
                        switch_state_var = 1;
                        break;
                    }
                }
                break;
            }
            if (switch_state_var)
                break;
        }
    switch (loop_state_var) {
      case 0U:
        {
            var_61 = (uint64_t *)*(uint64_t *)(local_sp_7 + 16UL);
            *var_61 = (*var_61 - rdx5_3);
            return rax_7;
        }
        break;
      case 1U:
        {
            rdx5_3 = *(uint64_t *)(local_sp_5 + 40UL);
        }
        break;
    }
}

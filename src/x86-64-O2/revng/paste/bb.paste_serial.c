typedef unsigned char __u_char;
typedef unsigned short __u_short;
typedef unsigned int __u_int;
typedef unsigned long __u_long;
typedef signed char __int8_t;
typedef unsigned char __uint8_t;
typedef short __int16_t;
typedef unsigned short __uint16_t;
typedef int __int32_t;
typedef unsigned int __uint32_t;
typedef long __int64_t;
typedef unsigned long __uint64_t;
typedef __int8_t __int_least8_t;
typedef __uint8_t __uint_least8_t;
typedef __int16_t __int_least16_t;
typedef __uint16_t __uint_least16_t;
typedef __int32_t __int_least32_t;
typedef __uint32_t __uint_least32_t;
typedef __int64_t __int_least64_t;
typedef __uint64_t __uint_least64_t;
typedef long __quad_t;
typedef unsigned long __u_quad_t;
typedef long __intmax_t;
typedef unsigned long __uintmax_t;
typedef unsigned long __dev_t;
typedef unsigned int __uid_t;
typedef unsigned int __gid_t;
typedef unsigned long __ino_t;
typedef unsigned long __ino64_t;
typedef unsigned int __mode_t;
typedef unsigned long __nlink_t;
typedef long __off_t;
typedef long __off64_t;
typedef int __pid_t;
typedef struct {
    int __val[2];
} __fsid_t;
typedef long __clock_t;
typedef unsigned long __rlim_t;
typedef unsigned long __rlim64_t;
typedef unsigned int __id_t;
typedef long __time_t;
typedef unsigned int __useconds_t;
typedef long __suseconds_t;
typedef int __daddr_t;
typedef int __key_t;
typedef int __clockid_t;
typedef void *__timer_t;
typedef long __blksize_t;
typedef long __blkcnt_t;
typedef long __blkcnt64_t;
typedef unsigned long __fsblkcnt_t;
typedef unsigned long __fsblkcnt64_t;
typedef unsigned long __fsfilcnt_t;
typedef unsigned long __fsfilcnt64_t;
typedef long __fsword_t;
typedef long __ssize_t;
typedef long __syscall_slong_t;
typedef unsigned long __syscall_ulong_t;
typedef __off64_t __loff_t;
typedef char *__caddr_t;
typedef long __intptr_t;
typedef unsigned int __socklen_t;
typedef int __sig_atomic_t;
typedef __int8_t int8_t;
typedef __int16_t int16_t;
typedef __int32_t int32_t;
typedef __int64_t int64_t;
typedef __uint8_t uint8_t;
typedef __uint16_t uint16_t;
typedef __uint32_t uint32_t;
typedef __uint64_t uint64_t;
typedef __int_least8_t int_least8_t;
typedef __int_least16_t int_least16_t;
typedef __int_least32_t int_least32_t;
typedef __int_least64_t int_least64_t;
typedef __uint_least8_t uint_least8_t;
typedef __uint_least16_t uint_least16_t;
typedef __uint_least32_t uint_least32_t;
typedef __uint_least64_t uint_least64_t;
typedef signed char int_fast8_t;
typedef long int_fast16_t;
typedef long int_fast32_t;
typedef long int_fast64_t;
typedef unsigned char uint_fast8_t;
typedef unsigned long uint_fast16_t;
typedef unsigned long uint_fast32_t;
typedef unsigned long uint_fast64_t;
typedef long intptr_t;
typedef unsigned long uintptr_t;
typedef __intmax_t intmax_t;
typedef __uintmax_t uintmax_t;
typedef _Bool bool;
struct indirect_placeholder_1_ret_type;
struct indirect_placeholder_3_ret_type;
struct indirect_placeholder_2_ret_type;
struct indirect_placeholder_6_ret_type;
struct indirect_placeholder_4_ret_type;
struct indirect_placeholder_7_ret_type;
struct indirect_placeholder_1_ret_type {
    uint64_t field_0;
    uint64_t field_1;
};
struct indirect_placeholder_3_ret_type {
    uint64_t field_0;
    uint64_t field_1;
    uint64_t field_2;
};
struct indirect_placeholder_2_ret_type {
    uint64_t field_0;
    uint64_t field_1;
    uint64_t field_2;
    uint64_t field_3;
    uint64_t field_4;
};
struct indirect_placeholder_6_ret_type {
    uint64_t field_0;
    uint64_t field_1;
    uint64_t field_2;
};
struct indirect_placeholder_4_ret_type {
    uint64_t field_0;
    uint64_t field_1;
    uint64_t field_2;
    uint64_t field_3;
    uint64_t field_4;
};
struct indirect_placeholder_7_ret_type {
    uint64_t field_0;
    uint64_t field_1;
    uint64_t field_2;
};
extern void abort(void);
extern uint64_t revng_init_local_sp(uint64_t param_0);
extern uint64_t init_rbx(void);
extern uint64_t init_rbp(void);
extern void indirect_placeholder(void);
extern uint64_t init_r13(void);
extern uint64_t init_r12(void);
extern uint64_t init_r14(void);
extern uint64_t init_rax(void);
extern uint64_t init_r15(void);
extern uint64_t indirect_placeholder_5(uint64_t param_0);
extern struct indirect_placeholder_1_ret_type indirect_placeholder_1(uint64_t param_0, uint64_t param_1, uint64_t param_2);
extern struct indirect_placeholder_3_ret_type indirect_placeholder_3(uint64_t param_0, uint64_t param_1, uint64_t param_2);
extern struct indirect_placeholder_2_ret_type indirect_placeholder_2(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint64_t param_3, uint64_t param_4, uint64_t param_5, uint64_t param_6);
extern struct indirect_placeholder_6_ret_type indirect_placeholder_6(uint64_t param_0, uint64_t param_1, uint64_t param_2);
extern struct indirect_placeholder_4_ret_type indirect_placeholder_4(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint64_t param_3, uint64_t param_4, uint64_t param_5, uint64_t param_6);
extern struct indirect_placeholder_7_ret_type indirect_placeholder_7(uint64_t param_0);
void bb_paste_serial(uint64_t rdi, uint64_t rsi) {
    struct indirect_placeholder_2_ret_type var_57;
    uint64_t var_0;
    uint64_t var_1;
    uint64_t var_2;
    uint64_t var_3;
    uint64_t var_4;
    uint64_t var_5;
    uint64_t var_6;
    uint64_t var_7;
    uint64_t var_8;
    bool var_9;
    uint64_t local_sp_12;
    uint64_t rax_6;
    uint64_t var_16;
    uint64_t r13_0;
    uint64_t rbx_7;
    uint64_t local_sp_11;
    uint64_t local_sp_4;
    uint64_t local_sp_0;
    uint64_t r14_0;
    uint64_t var_49;
    struct indirect_placeholder_3_ret_type var_50;
    uint64_t var_51;
    uint64_t var_52;
    uint64_t var_53;
    uint64_t var_54;
    uint64_t var_55;
    uint64_t *var_56;
    uint64_t var_58;
    uint64_t r15_1;
    uint64_t rbp_1;
    uint64_t rbx_1;
    uint64_t rax_4;
    uint64_t var_36;
    uint64_t local_sp_9;
    uint64_t local_sp_5;
    uint64_t local_sp_3;
    uint64_t *_pre_phi94;
    uint64_t local_sp_1;
    uint64_t rax_0;
    uint64_t rbx_0;
    uint64_t var_61;
    uint64_t rax_5;
    uint64_t local_sp_2;
    uint64_t rbx_5;
    uint64_t rax_1;
    uint64_t local_sp_10;
    uint64_t rax_2;
    uint64_t rbx_2;
    uint64_t var_59;
    uint64_t *var_60;
    uint64_t rbx_3;
    uint64_t var_46;
    uint64_t *var_47;
    uint64_t var_48;
    uint64_t rbx_4;
    uint64_t r15_0;
    uint64_t var_37;
    struct indirect_placeholder_6_ret_type var_38;
    uint64_t var_39;
    uint64_t var_40;
    uint64_t var_41;
    uint64_t var_42;
    uint64_t var_43;
    struct indirect_placeholder_4_ret_type var_44;
    uint64_t var_45;
    unsigned char var_29;
    uint64_t var_30;
    uint32_t var_31;
    uint32_t var_32;
    uint32_t _pre_phi;
    uint64_t var_33;
    uint64_t local_sp_6_ph88;
    uint64_t rbx_6;
    uint64_t rax_3_ph;
    uint64_t var_23;
    uint64_t var_24;
    uint64_t local_sp_8;
    uint64_t local_sp_7;
    uint64_t var_27;
    uint64_t var_28;
    uint64_t var_26;
    uint64_t rbp_0;
    uint64_t var_25;
    uint64_t var_34;
    uint64_t var_35;
    uint64_t var_20;
    uint32_t var_21;
    uint64_t var_22;
    uint64_t var_17;
    uint64_t r12_0;
    uint64_t var_18;
    uint64_t var_19;
    uint64_t var_15;
    uint64_t var_10;
    uint32_t var_11;
    uint64_t var_12;
    bool var_13;
    uint64_t var_14;
    unsigned int loop_state_var;
    var_0 = revng_init_local_sp(0UL);
    var_1 = init_rax();
    var_2 = init_rbx();
    var_3 = init_rbp();
    var_4 = init_r13();
    var_5 = init_r12();
    var_6 = init_r15();
    var_7 = init_r14();
    *(uint64_t *)(var_0 + (-8L)) = var_6;
    *(uint64_t *)(var_0 + (-16L)) = var_7;
    *(uint64_t *)(var_0 + (-24L)) = var_4;
    *(uint64_t *)(var_0 + (-32L)) = var_5;
    *(uint64_t *)(var_0 + (-40L)) = var_3;
    *(uint64_t *)(var_0 + (-48L)) = var_2;
    var_8 = var_0 + (-72L);
    *(uint64_t *)var_8 = rdi;
    var_9 = (rdi == 0UL);
    *(unsigned char *)(var_0 + (-57L)) = (unsigned char)'\x01';
    local_sp_12 = var_8;
    rax_6 = var_1;
    r13_0 = rsi;
    rbx_7 = var_2;
    r14_0 = var_7;
    if (var_9) {
        return;
    }
    while (1U)
        {
            var_10 = local_sp_12 + (-8L);
            *(uint64_t *)var_10 = 4203270UL;
            indirect_placeholder();
            var_11 = (uint32_t)rax_6;
            var_12 = (uint64_t)var_11;
            var_13 = (var_12 == 0UL);
            var_14 = (r14_0 & (-256L)) | var_13;
            local_sp_11 = var_10;
            r14_0 = var_14;
            rax_4 = rax_6;
            rax_5 = rax_6;
            rbx_5 = var_12;
            _pre_phi = var_11;
            rbx_6 = var_12;
            r12_0 = rax_6;
            if (var_13) {
                var_17 = *(uint64_t *)6355656UL;
                *(unsigned char *)6355809UL = (unsigned char)'\x01';
                r12_0 = var_17;
            } else {
                var_15 = local_sp_12 + (-16L);
                *(uint64_t *)var_15 = 4203710UL;
                indirect_placeholder();
                local_sp_0 = var_15;
                if (rax_6 != 0UL) {
                    var_49 = *(uint64_t *)r13_0;
                    *(uint64_t *)(local_sp_0 + (-8L)) = 4203655UL;
                    var_50 = indirect_placeholder_3(0UL, 3UL, var_49);
                    var_51 = var_50.field_0;
                    var_52 = var_50.field_1;
                    var_53 = var_50.field_2;
                    *(uint64_t *)(local_sp_0 + (-16L)) = 4203663UL;
                    indirect_placeholder();
                    var_54 = (uint64_t)*(uint32_t *)var_51;
                    var_55 = local_sp_0 + (-24L);
                    var_56 = (uint64_t *)var_55;
                    *var_56 = 4203682UL;
                    var_57 = indirect_placeholder_2(0UL, var_51, var_52, 0UL, var_53, var_54, 4248777UL);
                    var_58 = var_57.field_0;
                    *(unsigned char *)(local_sp_0 + (-9L)) = (unsigned char)'\x00';
                    _pre_phi94 = var_56;
                    local_sp_1 = var_55;
                    rax_0 = var_58;
                    rbx_0 = var_51;
                    var_61 = *_pre_phi94 + (-1L);
                    *_pre_phi94 = var_61;
                    local_sp_12 = local_sp_1;
                    rax_6 = rax_0;
                    rbx_7 = rbx_0;
                    if (var_61 != 0UL) {
                        loop_state_var = 1U;
                        break;
                    }
                    r13_0 = r13_0 + 8UL;
                    continue;
                }
                var_16 = local_sp_12 + (-24L);
                *(uint64_t *)var_16 = 4203731UL;
                indirect_placeholder_1(rbx_7, rax_6, 2UL);
                local_sp_11 = var_16;
            }
            var_18 = *(uint64_t *)6355800UL;
            *(uint64_t *)(local_sp_11 + (-8L)) = 4203311UL;
            indirect_placeholder();
            var_19 = local_sp_11 + (-16L);
            *(uint64_t *)var_19 = 4203318UL;
            indirect_placeholder();
            local_sp_9 = var_19;
            local_sp_10 = var_19;
            rbp_1 = var_18;
            if ((uint64_t)(var_11 + 1U) == 0UL) {
                var_34 = (uint64_t)*(uint32_t *)rax_6;
                r15_1 = var_34;
                var_35 = local_sp_9 + (-8L);
                *(uint64_t *)var_35 = 4203552UL;
                indirect_placeholder();
                rbx_1 = rbx_5;
                rax_1 = rax_4;
                rbx_4 = rbx_5;
                r15_0 = r15_1;
                local_sp_6_ph88 = var_35;
                rax_3_ph = rax_4;
                if ((int)_pre_phi < (int)0U) {
                    loop_state_var = 0U;
                    break;
                }
                var_36 = local_sp_9 + (-16L);
                *(uint64_t *)var_36 = 4203568UL;
                indirect_placeholder();
                local_sp_2 = var_36;
                local_sp_5 = var_36;
                if ((uint64_t)_pre_phi == 0UL) {
                    var_37 = *(uint64_t *)r13_0;
                    *(uint64_t *)(local_sp_5 + (-8L)) = 4203588UL;
                    var_38 = indirect_placeholder_6(0UL, 3UL, var_37);
                    var_39 = var_38.field_0;
                    var_40 = var_38.field_1;
                    var_41 = var_38.field_2;
                    var_42 = (uint64_t)(uint32_t)r15_0;
                    var_43 = local_sp_5 + (-16L);
                    *(uint64_t *)var_43 = 4203608UL;
                    var_44 = indirect_placeholder_4(0UL, var_39, var_40, 0UL, var_41, var_42, 4248777UL);
                    var_45 = var_44.field_0;
                    *(unsigned char *)(local_sp_5 + (-1L)) = (unsigned char)'\x00';
                    local_sp_4 = var_43;
                    local_sp_3 = var_43;
                    rax_2 = var_45;
                    rbx_2 = rbx_4;
                    rbx_3 = rbx_4;
                    if (var_13) {
                        var_59 = local_sp_3 + (-8L);
                        var_60 = (uint64_t *)var_59;
                        *var_60 = 4203494UL;
                        indirect_placeholder();
                        _pre_phi94 = var_60;
                        local_sp_1 = var_59;
                        rax_0 = rax_2;
                        rbx_0 = rbx_2;
                        var_61 = *_pre_phi94 + (-1L);
                        *_pre_phi94 = var_61;
                        local_sp_12 = local_sp_1;
                        rax_6 = rax_0;
                        rbx_7 = rbx_0;
                        if (var_61 != 0UL) {
                            r13_0 = r13_0 + 8UL;
                            continue;
                        }
                        loop_state_var = 1U;
                        break;
                    }
                }
                local_sp_4 = local_sp_2;
                local_sp_3 = local_sp_2;
                rax_2 = rax_1;
                rbx_2 = rbx_1;
                rbx_3 = rbx_1;
                if (!var_13) {
                    var_59 = local_sp_3 + (-8L);
                    var_60 = (uint64_t *)var_59;
                    *var_60 = 4203494UL;
                    indirect_placeholder();
                    _pre_phi94 = var_60;
                    local_sp_1 = var_59;
                    rax_0 = rax_2;
                    rbx_0 = rbx_2;
                    var_61 = *_pre_phi94 + (-1L);
                    *_pre_phi94 = var_61;
                    local_sp_12 = local_sp_1;
                    rax_6 = rax_0;
                    rbx_7 = rbx_0;
                    if (var_61 != 0UL) {
                        r13_0 = r13_0 + 8UL;
                        continue;
                    }
                    loop_state_var = 1U;
                    break;
                }
                var_46 = local_sp_4 + (-8L);
                var_47 = (uint64_t *)var_46;
                *var_47 = 4203630UL;
                var_48 = indirect_placeholder_5(r12_0);
                local_sp_0 = var_46;
                _pre_phi94 = var_47;
                local_sp_1 = var_46;
                rax_0 = var_48;
                rbx_0 = rbx_3;
                if ((uint64_t)((uint32_t)var_48 + 1U) != 0UL) {
                    var_61 = *_pre_phi94 + (-1L);
                    *_pre_phi94 = var_61;
                    local_sp_12 = local_sp_1;
                    rax_6 = rax_0;
                    rbx_7 = rbx_0;
                    if (var_61 != 0UL) {
                        loop_state_var = 1U;
                        break;
                    }
                    r13_0 = r13_0 + 8UL;
                    continue;
                }
                var_49 = *(uint64_t *)r13_0;
                *(uint64_t *)(local_sp_0 + (-8L)) = 4203655UL;
                var_50 = indirect_placeholder_3(0UL, 3UL, var_49);
                var_51 = var_50.field_0;
                var_52 = var_50.field_1;
                var_53 = var_50.field_2;
                *(uint64_t *)(local_sp_0 + (-16L)) = 4203663UL;
                indirect_placeholder();
                var_54 = (uint64_t)*(uint32_t *)var_51;
                var_55 = local_sp_0 + (-24L);
                var_56 = (uint64_t *)var_55;
                *var_56 = 4203682UL;
                var_57 = indirect_placeholder_2(0UL, var_51, var_52, 0UL, var_53, var_54, 4248777UL);
                var_58 = var_57.field_0;
                *(unsigned char *)(local_sp_0 + (-9L)) = (unsigned char)'\x00';
                _pre_phi94 = var_56;
                local_sp_1 = var_55;
                rax_0 = var_58;
                rbx_0 = var_51;
                var_61 = *_pre_phi94 + (-1L);
                *_pre_phi94 = var_61;
                local_sp_12 = local_sp_1;
                rax_6 = rax_0;
                rbx_7 = rbx_0;
                if (var_61 != 0UL) {
                    loop_state_var = 1U;
                    break;
                }
                r13_0 = r13_0 + 8UL;
                continue;
            }
            var_20 = local_sp_10 + (-8L);
            *(uint64_t *)var_20 = 4203375UL;
            indirect_placeholder();
            var_21 = (uint32_t)rax_5;
            var_22 = (uint64_t)var_21;
            rbx_1 = rbx_6;
            rbx_5 = rbx_6;
            rbx_4 = rbx_6;
            rbx_6 = var_22;
            rax_3_ph = rax_5;
            local_sp_7 = var_20;
            rbp_0 = rbp_1;
            while ((uint64_t)(var_21 + 1U) != 0UL)
                {
                    var_23 = (uint64_t)*(unsigned char *)6355488UL;
                    rax_5 = var_23;
                    if ((uint64_t)((uint32_t)var_23 - (uint32_t)rbx_6) == 0UL) {
                        var_24 = local_sp_10 + (-16L);
                        *(uint64_t *)var_24 = 4203402UL;
                        indirect_placeholder();
                        local_sp_8 = var_24;
                    } else {
                        if (*(unsigned char *)rbp_1 != '\x00') {
                            var_25 = local_sp_10 + (-16L);
                            *(uint64_t *)var_25 = 4203421UL;
                            indirect_placeholder();
                            local_sp_7 = var_25;
                        }
                        var_26 = rbp_1 + 1UL;
                        local_sp_8 = local_sp_7;
                        rbp_0 = (var_26 == *(uint64_t *)6355792UL) ? *(uint64_t *)6355800UL : var_26;
                    }
                    local_sp_10 = local_sp_8;
                    rbp_1 = rbp_0;
                    var_20 = local_sp_10 + (-8L);
                    *(uint64_t *)var_20 = 4203375UL;
                    indirect_placeholder();
                    var_21 = (uint32_t)rax_5;
                    var_22 = (uint64_t)var_21;
                    rbx_1 = rbx_6;
                    rbx_5 = rbx_6;
                    rbx_4 = rbx_6;
                    rbx_6 = var_22;
                    rax_3_ph = rax_5;
                    local_sp_7 = var_20;
                    rbp_0 = rbp_1;
                }
            *(uint64_t *)(local_sp_10 + (-16L)) = 4203437UL;
            indirect_placeholder();
            var_27 = (uint64_t)*(uint32_t *)rax_5;
            var_28 = local_sp_10 + (-24L);
            *(uint64_t *)var_28 = 4203448UL;
            indirect_placeholder();
            r15_0 = var_27;
            local_sp_6_ph88 = var_28;
            local_sp_9 = var_28;
            r15_1 = var_27;
            if ((int)var_21 >= (int)0U) {
                loop_state_var = 0U;
                break;
            }
            var_29 = *(unsigned char *)6355488UL;
            var_30 = (uint64_t)var_29;
            var_31 = (uint32_t)rbx_6;
            var_32 = (uint32_t)var_30;
            rax_1 = var_30;
            _pre_phi = var_32;
            rax_4 = var_30;
            if ((uint64_t)(var_31 - var_32) == 0UL) {
                var_33 = local_sp_10 + (-32L);
                *(uint64_t *)var_33 = 4203473UL;
                indirect_placeholder();
                local_sp_2 = var_33;
                local_sp_5 = var_33;
                if (var_29 != '\x00') {
                    local_sp_4 = local_sp_2;
                    local_sp_3 = local_sp_2;
                    rax_2 = rax_1;
                    rbx_2 = rbx_1;
                    rbx_3 = rbx_1;
                    if (!var_13) {
                        var_59 = local_sp_3 + (-8L);
                        var_60 = (uint64_t *)var_59;
                        *var_60 = 4203494UL;
                        indirect_placeholder();
                        _pre_phi94 = var_60;
                        local_sp_1 = var_59;
                        rax_0 = rax_2;
                        rbx_0 = rbx_2;
                        var_61 = *_pre_phi94 + (-1L);
                        *_pre_phi94 = var_61;
                        local_sp_12 = local_sp_1;
                        rax_6 = rax_0;
                        rbx_7 = rbx_0;
                        if (var_61 != 0UL) {
                            r13_0 = r13_0 + 8UL;
                            continue;
                        }
                        loop_state_var = 1U;
                        break;
                    }
                }
                var_37 = *(uint64_t *)r13_0;
                *(uint64_t *)(local_sp_5 + (-8L)) = 4203588UL;
                var_38 = indirect_placeholder_6(0UL, 3UL, var_37);
                var_39 = var_38.field_0;
                var_40 = var_38.field_1;
                var_41 = var_38.field_2;
                var_42 = (uint64_t)(uint32_t)r15_0;
                var_43 = local_sp_5 + (-16L);
                *(uint64_t *)var_43 = 4203608UL;
                var_44 = indirect_placeholder_4(0UL, var_39, var_40, 0UL, var_41, var_42, 4248777UL);
                var_45 = var_44.field_0;
                *(unsigned char *)(local_sp_5 + (-1L)) = (unsigned char)'\x00';
                local_sp_4 = var_43;
                local_sp_3 = var_43;
                rax_2 = var_45;
                rbx_2 = rbx_4;
                rbx_3 = rbx_4;
                if (!var_13) {
                    var_59 = local_sp_3 + (-8L);
                    var_60 = (uint64_t *)var_59;
                    *var_60 = 4203494UL;
                    indirect_placeholder();
                    _pre_phi94 = var_60;
                    local_sp_1 = var_59;
                    rax_0 = rax_2;
                    rbx_0 = rbx_2;
                    var_61 = *_pre_phi94 + (-1L);
                    *_pre_phi94 = var_61;
                    local_sp_12 = local_sp_1;
                    rax_6 = rax_0;
                    rbx_7 = rbx_0;
                    if (var_61 != 0UL) {
                        r13_0 = r13_0 + 8UL;
                        continue;
                    }
                    loop_state_var = 1U;
                    break;
                }
            }
            var_35 = local_sp_9 + (-8L);
            *(uint64_t *)var_35 = 4203552UL;
            indirect_placeholder();
            rbx_1 = rbx_5;
            rax_1 = rax_4;
            rbx_4 = rbx_5;
            r15_0 = r15_1;
            local_sp_6_ph88 = var_35;
            rax_3_ph = rax_4;
            if ((int)_pre_phi >= (int)0U) {
                loop_state_var = 0U;
                break;
            }
            var_36 = local_sp_9 + (-16L);
            *(uint64_t *)var_36 = 4203568UL;
            indirect_placeholder();
            local_sp_2 = var_36;
            local_sp_5 = var_36;
            if ((uint64_t)_pre_phi == 0UL) {
                local_sp_4 = local_sp_2;
                local_sp_3 = local_sp_2;
                rax_2 = rax_1;
                rbx_2 = rbx_1;
                rbx_3 = rbx_1;
                if (var_13) {
                    var_59 = local_sp_3 + (-8L);
                    var_60 = (uint64_t *)var_59;
                    *var_60 = 4203494UL;
                    indirect_placeholder();
                    _pre_phi94 = var_60;
                    local_sp_1 = var_59;
                    rax_0 = rax_2;
                    rbx_0 = rbx_2;
                    var_61 = *_pre_phi94 + (-1L);
                    *_pre_phi94 = var_61;
                    local_sp_12 = local_sp_1;
                    rax_6 = rax_0;
                    rbx_7 = rbx_0;
                    if (var_61 != 0UL) {
                        loop_state_var = 1U;
                        break;
                    }
                    r13_0 = r13_0 + 8UL;
                    continue;
                }
            }
            var_37 = *(uint64_t *)r13_0;
            *(uint64_t *)(local_sp_5 + (-8L)) = 4203588UL;
            var_38 = indirect_placeholder_6(0UL, 3UL, var_37);
            var_39 = var_38.field_0;
            var_40 = var_38.field_1;
            var_41 = var_38.field_2;
            var_42 = (uint64_t)(uint32_t)r15_0;
            var_43 = local_sp_5 + (-16L);
            *(uint64_t *)var_43 = 4203608UL;
            var_44 = indirect_placeholder_4(0UL, var_39, var_40, 0UL, var_41, var_42, 4248777UL);
            var_45 = var_44.field_0;
            *(unsigned char *)(local_sp_5 + (-1L)) = (unsigned char)'\x00';
            local_sp_4 = var_43;
            local_sp_3 = var_43;
            rax_2 = var_45;
            rbx_2 = rbx_4;
            rbx_3 = rbx_4;
            if (!var_13) {
                var_59 = local_sp_3 + (-8L);
                var_60 = (uint64_t *)var_59;
                *var_60 = 4203494UL;
                indirect_placeholder();
                _pre_phi94 = var_60;
                local_sp_1 = var_59;
                rax_0 = rax_2;
                rbx_0 = rbx_2;
                var_61 = *_pre_phi94 + (-1L);
                *_pre_phi94 = var_61;
                local_sp_12 = local_sp_1;
                rax_6 = rax_0;
                rbx_7 = rbx_0;
                if (var_61 != 0UL) {
                    loop_state_var = 1U;
                    break;
                }
                r13_0 = r13_0 + 8UL;
                continue;
            }
            var_46 = local_sp_4 + (-8L);
            var_47 = (uint64_t *)var_46;
            *var_47 = 4203630UL;
            var_48 = indirect_placeholder_5(r12_0);
            local_sp_0 = var_46;
            _pre_phi94 = var_47;
            local_sp_1 = var_46;
            rax_0 = var_48;
            rbx_0 = rbx_3;
            if ((uint64_t)((uint32_t)var_48 + 1U) == 0UL) {
                var_61 = *_pre_phi94 + (-1L);
                *_pre_phi94 = var_61;
                local_sp_12 = local_sp_1;
                rax_6 = rax_0;
                rbx_7 = rbx_0;
                if (var_61 != 0UL) {
                    r13_0 = r13_0 + 8UL;
                    continue;
                }
                loop_state_var = 1U;
                break;
            }
            var_49 = *(uint64_t *)r13_0;
            *(uint64_t *)(local_sp_0 + (-8L)) = 4203655UL;
            var_50 = indirect_placeholder_3(0UL, 3UL, var_49);
            var_51 = var_50.field_0;
            var_52 = var_50.field_1;
            var_53 = var_50.field_2;
            *(uint64_t *)(local_sp_0 + (-16L)) = 4203663UL;
            indirect_placeholder();
            var_54 = (uint64_t)*(uint32_t *)var_51;
            var_55 = local_sp_0 + (-24L);
            var_56 = (uint64_t *)var_55;
            *var_56 = 4203682UL;
            var_57 = indirect_placeholder_2(0UL, var_51, var_52, 0UL, var_53, var_54, 4248777UL);
            var_58 = var_57.field_0;
            *(unsigned char *)(local_sp_0 + (-9L)) = (unsigned char)'\x00';
            _pre_phi94 = var_56;
            local_sp_1 = var_55;
            rax_0 = var_58;
            rbx_0 = var_51;
            var_61 = *_pre_phi94 + (-1L);
            *_pre_phi94 = var_61;
            local_sp_12 = local_sp_1;
            rax_6 = rax_0;
            rbx_7 = rbx_0;
            if (var_61 != 0UL) {
                r13_0 = r13_0 + 8UL;
                continue;
            }
            loop_state_var = 1U;
            break;
        }
    switch (loop_state_var) {
      case 0U:
        {
            *(uint64_t *)(local_sp_6_ph88 + (-8L)) = 4203411UL;
            indirect_placeholder_7(rax_3_ph);
            abort();
        }
        break;
      case 1U:
        {
            return;
        }
        break;
    }
}

typedef unsigned char __u_char;
typedef unsigned short __u_short;
typedef unsigned int __u_int;
typedef unsigned long __u_long;
typedef signed char __int8_t;
typedef unsigned char __uint8_t;
typedef short __int16_t;
typedef unsigned short __uint16_t;
typedef int __int32_t;
typedef unsigned int __uint32_t;
typedef long __int64_t;
typedef unsigned long __uint64_t;
typedef __int8_t __int_least8_t;
typedef __uint8_t __uint_least8_t;
typedef __int16_t __int_least16_t;
typedef __uint16_t __uint_least16_t;
typedef __int32_t __int_least32_t;
typedef __uint32_t __uint_least32_t;
typedef __int64_t __int_least64_t;
typedef __uint64_t __uint_least64_t;
typedef long __quad_t;
typedef unsigned long __u_quad_t;
typedef long __intmax_t;
typedef unsigned long __uintmax_t;
typedef unsigned long __dev_t;
typedef unsigned int __uid_t;
typedef unsigned int __gid_t;
typedef unsigned long __ino_t;
typedef unsigned long __ino64_t;
typedef unsigned int __mode_t;
typedef unsigned long __nlink_t;
typedef long __off_t;
typedef long __off64_t;
typedef int __pid_t;
typedef struct {
    int __val[2];
} __fsid_t;
typedef long __clock_t;
typedef unsigned long __rlim_t;
typedef unsigned long __rlim64_t;
typedef unsigned int __id_t;
typedef long __time_t;
typedef unsigned int __useconds_t;
typedef long __suseconds_t;
typedef int __daddr_t;
typedef int __key_t;
typedef int __clockid_t;
typedef void *__timer_t;
typedef long __blksize_t;
typedef long __blkcnt_t;
typedef long __blkcnt64_t;
typedef unsigned long __fsblkcnt_t;
typedef unsigned long __fsblkcnt64_t;
typedef unsigned long __fsfilcnt_t;
typedef unsigned long __fsfilcnt64_t;
typedef long __fsword_t;
typedef long __ssize_t;
typedef long __syscall_slong_t;
typedef unsigned long __syscall_ulong_t;
typedef __off64_t __loff_t;
typedef char *__caddr_t;
typedef long __intptr_t;
typedef unsigned int __socklen_t;
typedef int __sig_atomic_t;
typedef __int8_t int8_t;
typedef __int16_t int16_t;
typedef __int32_t int32_t;
typedef __int64_t int64_t;
typedef __uint8_t uint8_t;
typedef __uint16_t uint16_t;
typedef __uint32_t uint32_t;
typedef __uint64_t uint64_t;
typedef __int_least8_t int_least8_t;
typedef __int_least16_t int_least16_t;
typedef __int_least32_t int_least32_t;
typedef __int_least64_t int_least64_t;
typedef __uint_least8_t uint_least8_t;
typedef __uint_least16_t uint_least16_t;
typedef __uint_least32_t uint_least32_t;
typedef __uint_least64_t uint_least64_t;
typedef signed char int_fast8_t;
typedef long int_fast16_t;
typedef long int_fast32_t;
typedef long int_fast64_t;
typedef unsigned char uint_fast8_t;
typedef unsigned long uint_fast16_t;
typedef unsigned long uint_fast32_t;
typedef unsigned long uint_fast64_t;
typedef long intptr_t;
typedef unsigned long uintptr_t;
typedef __intmax_t intmax_t;
typedef __uintmax_t uintmax_t;
extern uint64_t revng_init_local_sp(uint64_t param_0);
extern void indirect_placeholder(void);
extern void indirect_placeholder_27(uint64_t param_0, uint64_t param_1);
extern uint64_t indirect_placeholder_210(void);
uint64_t bb_remove_line(void) {
    uint64_t var_0;
    uint64_t var_1;
    uint64_t var_2;
    uint64_t local_sp_0;
    uint64_t rcx_0;
    uint64_t var_3;
    uint64_t var_4;
    uint64_t var_5;
    uint64_t var_6;
    uint64_t *var_8;
    uint64_t var_9;
    uint64_t *var_10;
    uint64_t var_11;
    uint64_t *var_12;
    uint64_t var_13;
    uint64_t var_14;
    uint64_t var_15;
    uint64_t var_16;
    uint64_t merge;
    uint64_t var_17;
    uint64_t var_7;
    var_0 = revng_init_local_sp(0UL);
    var_1 = var_0 + (-8L);
    var_2 = *(uint64_t *)6437248UL;
    local_sp_0 = var_1;
    merge = 0UL;
    if (var_2 == 0UL) {
        var_3 = var_2 + 48UL;
        var_4 = var_2 + 40UL;
        *(uint64_t *)(var_0 + (-16L)) = 4209277UL;
        indirect_placeholder_27(var_4, var_3);
        var_5 = var_0 + (-24L);
        *(uint64_t *)var_5 = 4209289UL;
        indirect_placeholder();
        *(uint64_t *)6437248UL = 0UL;
        local_sp_0 = var_5;
    }
    var_6 = *(uint64_t *)6437568UL;
    rcx_0 = var_6;
    if (var_6 != 0UL) {
        if (*(unsigned char *)6437528UL == '\x00') {
            return merge;
        }
        *(uint64_t *)(local_sp_0 + (-8L)) = 4209477UL;
        var_7 = indirect_placeholder_210();
        if ((uint64_t)(unsigned char)var_7 != 0UL) {
            return merge;
        }
        rcx_0 = *(uint64_t *)6437568UL;
    }
    var_8 = (uint64_t *)(rcx_0 + 24UL);
    var_9 = *var_8;
    if (var_9 > *(uint64_t *)6437536UL) {
        *(uint64_t *)6437536UL = var_9;
    }
    var_10 = (uint64_t *)(rcx_0 + 56UL);
    var_11 = *var_10;
    *var_8 = (var_9 + 1UL);
    var_12 = (uint64_t *)(var_11 + 16UL);
    var_13 = *var_12 + 1UL;
    *var_12 = var_13;
    var_14 = *(uint64_t *)var_11;
    var_15 = ((var_13 << 4UL) + var_11) + 8UL;
    merge = var_15;
    if (var_13 == var_14) {
        return merge;
    }
    var_16 = *(uint64_t *)(var_11 + 1304UL);
    *var_10 = var_16;
    if (var_16 != 0UL) {
        if (*(uint64_t *)var_16 != 0UL) {
            return merge;
        }
    }
    var_17 = *(uint64_t *)(rcx_0 + 64UL);
    *(uint64_t *)6437248UL = rcx_0;
    *(uint64_t *)6437568UL = var_17;
    return var_15;
    var_17 = *(uint64_t *)(rcx_0 + 64UL);
    *(uint64_t *)6437248UL = rcx_0;
    *(uint64_t *)6437568UL = var_17;
    return var_15;
}

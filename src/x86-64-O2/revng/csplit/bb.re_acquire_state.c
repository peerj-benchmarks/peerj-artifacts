typedef unsigned char __u_char;
typedef unsigned short __u_short;
typedef unsigned int __u_int;
typedef unsigned long __u_long;
typedef signed char __int8_t;
typedef unsigned char __uint8_t;
typedef short __int16_t;
typedef unsigned short __uint16_t;
typedef int __int32_t;
typedef unsigned int __uint32_t;
typedef long __int64_t;
typedef unsigned long __uint64_t;
typedef __int8_t __int_least8_t;
typedef __uint8_t __uint_least8_t;
typedef __int16_t __int_least16_t;
typedef __uint16_t __uint_least16_t;
typedef __int32_t __int_least32_t;
typedef __uint32_t __uint_least32_t;
typedef __int64_t __int_least64_t;
typedef __uint64_t __uint_least64_t;
typedef long __quad_t;
typedef unsigned long __u_quad_t;
typedef long __intmax_t;
typedef unsigned long __uintmax_t;
typedef unsigned long __dev_t;
typedef unsigned int __uid_t;
typedef unsigned int __gid_t;
typedef unsigned long __ino_t;
typedef unsigned long __ino64_t;
typedef unsigned int __mode_t;
typedef unsigned long __nlink_t;
typedef long __off_t;
typedef long __off64_t;
typedef int __pid_t;
typedef struct {
    int __val[2];
} __fsid_t;
typedef long __clock_t;
typedef unsigned long __rlim_t;
typedef unsigned long __rlim64_t;
typedef unsigned int __id_t;
typedef long __time_t;
typedef unsigned int __useconds_t;
typedef long __suseconds_t;
typedef int __daddr_t;
typedef int __key_t;
typedef int __clockid_t;
typedef void *__timer_t;
typedef long __blksize_t;
typedef long __blkcnt_t;
typedef long __blkcnt64_t;
typedef unsigned long __fsblkcnt_t;
typedef unsigned long __fsblkcnt64_t;
typedef unsigned long __fsfilcnt_t;
typedef unsigned long __fsfilcnt64_t;
typedef long __fsword_t;
typedef long __ssize_t;
typedef long __syscall_slong_t;
typedef unsigned long __syscall_ulong_t;
typedef __off64_t __loff_t;
typedef char *__caddr_t;
typedef long __intptr_t;
typedef unsigned int __socklen_t;
typedef int __sig_atomic_t;
typedef __int8_t int8_t;
typedef __int16_t int16_t;
typedef __int32_t int32_t;
typedef __int64_t int64_t;
typedef __uint8_t uint8_t;
typedef __uint16_t uint16_t;
typedef __uint32_t uint32_t;
typedef __uint64_t uint64_t;
typedef __int_least8_t int_least8_t;
typedef __int_least16_t int_least16_t;
typedef __int_least32_t int_least32_t;
typedef __int_least64_t int_least64_t;
typedef __uint_least8_t uint_least8_t;
typedef __uint_least16_t uint_least16_t;
typedef __uint_least32_t uint_least32_t;
typedef __uint_least64_t uint_least64_t;
typedef signed char int_fast8_t;
typedef long int_fast16_t;
typedef long int_fast32_t;
typedef long int_fast64_t;
typedef unsigned char uint_fast8_t;
typedef unsigned long uint_fast16_t;
typedef unsigned long uint_fast32_t;
typedef unsigned long uint_fast64_t;
typedef long intptr_t;
typedef unsigned long uintptr_t;
typedef __intmax_t intmax_t;
typedef __uintmax_t uintmax_t;
struct bb_re_acquire_state_ret_type;
struct indirect_placeholder_12_ret_type;
struct indirect_placeholder_11_ret_type;
struct indirect_placeholder_13_ret_type;
struct bb_re_acquire_state_ret_type {
    uint64_t field_0;
    uint64_t field_1;
};
struct indirect_placeholder_12_ret_type {
    uint64_t field_0;
    uint64_t field_1;
    uint64_t field_2;
};
struct indirect_placeholder_11_ret_type {
    uint64_t field_0;
    uint64_t field_1;
};
struct indirect_placeholder_13_ret_type {
    uint64_t field_0;
    uint64_t field_1;
    uint64_t field_2;
    uint64_t field_3;
    uint64_t field_4;
    uint64_t field_5;
    uint64_t field_6;
};
extern uint64_t revng_init_local_sp(uint64_t param_0);
extern uint64_t init_rbx(void);
extern uint64_t init_rbp(void);
extern uint64_t init_r12(void);
extern void indirect_placeholder(void);
extern uint64_t init_r13(void);
extern uint64_t init_r14(void);
extern uint64_t helper_cc_compute_all_wrapper(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint32_t param_3);
extern uint64_t init_r15(void);
extern uint64_t indirect_placeholder_7(uint64_t param_0, uint64_t param_1, uint64_t param_2);
extern void indirect_placeholder_4(uint64_t param_0);
extern struct indirect_placeholder_12_ret_type indirect_placeholder_12(uint64_t param_0, uint64_t param_1);
extern struct indirect_placeholder_11_ret_type indirect_placeholder_11(uint64_t param_0, uint64_t param_1, uint64_t param_2);
extern struct indirect_placeholder_13_ret_type indirect_placeholder_13(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint64_t param_3, uint64_t param_4, uint64_t param_5, uint64_t param_6, uint64_t param_7);
struct bb_re_acquire_state_ret_type bb_re_acquire_state(uint64_t rcx, uint64_t r10, uint64_t r9, uint64_t rdi, uint64_t r8, uint64_t rdx, uint64_t rsi) {
    uint64_t local_sp_2;
    uint64_t local_sp_1;
    uint64_t r812_1;
    uint64_t rcx8_1;
    uint64_t rbx_0;
    uint64_t r812_0;
    struct bb_re_acquire_state_ret_type mrv3 = {0UL, /*implicit*/(int)0};
    struct bb_re_acquire_state_ret_type mrv4;
    uint64_t rcx8_0;
    uint64_t var_12;
    uint64_t var_13;
    uint64_t rax_0;
    uint64_t r13_0;
    uint64_t var_14;
    uint64_t var_15;
    uint64_t r13_1;
    uint64_t var_16;
    uint64_t var_17;
    uint64_t var_18;
    uint64_t var_19;
    uint64_t rcx8_2;
    uint64_t var_20;
    uint64_t var_21;
    uint64_t var_22;
    struct indirect_placeholder_12_ret_type var_23;
    uint64_t var_24;
    uint64_t var_0;
    uint64_t var_1;
    uint64_t var_2;
    uint64_t var_3;
    uint64_t var_4;
    uint64_t var_5;
    uint64_t var_6;
    uint64_t var_7;
    uint64_t var_8;
    uint64_t *var_9;
    uint64_t var_10;
    struct bb_re_acquire_state_ret_type mrv;
    struct bb_re_acquire_state_ret_type mrv1;
    uint64_t r910_3;
    uint64_t rcx8_3;
    uint64_t var_56;
    uint64_t var_35;
    uint64_t r910_4;
    uint64_t var_36;
    uint64_t *var_37;
    unsigned char *var_38;
    uint64_t rdx13_0_be;
    uint64_t r910_0_be;
    uint64_t r910_0;
    uint64_t rdx13_0;
    uint64_t var_39;
    uint64_t var_40;
    uint32_t var_41;
    uint32_t *var_49;
    struct bb_re_acquire_state_ret_type mrv5;
    uint64_t var_11;
    unsigned char var_50;
    uint32_t *_pre_phi;
    unsigned char var_42;
    uint64_t var_43;
    unsigned char var_44;
    uint64_t var_45;
    unsigned char var_46;
    uint64_t var_47;
    unsigned char var_52;
    unsigned char var_51;
    uint64_t r910_1;
    uint64_t r910_2;
    uint64_t var_53;
    uint64_t var_48;
    uint64_t var_54;
    uint64_t var_55;
    uint64_t var_34;
    uint64_t var_29;
    uint64_t var_30;
    uint64_t var_31;
    uint64_t *var_32;
    struct indirect_placeholder_11_ret_type var_33;
    uint64_t local_sp_3;
    uint64_t local_sp_0;
    uint64_t r910_5;
    uint64_t rbx_1;
    uint64_t r812_2;
    uint64_t var_25;
    struct indirect_placeholder_13_ret_type var_26;
    uint64_t var_27;
    uint64_t var_28;
    struct bb_re_acquire_state_ret_type mrv7 = {0UL, /*implicit*/(int)0};
    var_0 = revng_init_local_sp(0UL);
    var_1 = init_rbx();
    var_2 = init_rbp();
    var_3 = init_r13();
    var_4 = init_r12();
    var_5 = init_r15();
    var_6 = init_r14();
    *(uint64_t *)(var_0 + (-8L)) = var_5;
    *(uint64_t *)(var_0 + (-16L)) = var_6;
    *(uint64_t *)(var_0 + (-24L)) = var_3;
    *(uint64_t *)(var_0 + (-32L)) = var_4;
    *(uint64_t *)(var_0 + (-40L)) = var_2;
    *(uint64_t *)(var_0 + (-48L)) = var_1;
    var_7 = var_0 + (-72L);
    var_8 = rdx + 8UL;
    var_9 = (uint64_t *)var_8;
    var_10 = *var_9;
    local_sp_1 = var_7;
    rbx_0 = 0UL;
    r812_0 = r8;
    rcx8_0 = rcx;
    r13_0 = var_10;
    r13_1 = var_10;
    rdx13_0 = 0UL;
    local_sp_3 = var_7;
    rbx_1 = var_1;
    r812_2 = r8;
    if (var_10 == 0UL) {
        *(uint32_t *)rdi = 0U;
        mrv7.field_1 = r9;
        return mrv7;
    }
    var_11 = helper_cc_compute_all_wrapper(var_10, 0UL, 0UL, 25U);
    if ((uint64_t)(((unsigned char)(var_11 >> 4UL) ^ (unsigned char)var_11) & '\xc0') != 0UL) {
        var_12 = *(uint64_t *)(rdx + 16UL);
        var_13 = (var_10 << 3UL) + var_12;
        rax_0 = var_12;
        rcx8_0 = var_13;
        var_14 = r13_0 + *(uint64_t *)rax_0;
        var_15 = rax_0 + 8UL;
        rax_0 = var_15;
        r13_0 = var_14;
        r13_1 = var_14;
        do {
            var_14 = r13_0 + *(uint64_t *)rax_0;
            var_15 = rax_0 + 8UL;
            rax_0 = var_15;
            r13_0 = var_14;
            r13_1 = var_14;
        } while (var_15 != var_13);
    }
    var_16 = r13_1 & *(uint64_t *)(rsi + 136UL);
    *(uint64_t *)var_7 = rsi;
    *(uint64_t *)(var_0 + (-64L)) = rdi;
    var_17 = (var_16 * 24UL) + *(uint64_t *)(rsi + 64UL);
    var_18 = *(uint64_t *)var_17;
    var_19 = helper_cc_compute_all_wrapper(var_18, 0UL, 0UL, 25U);
    rcx8_1 = rcx8_0;
    rcx8_3 = rcx8_0;
    if ((uint64_t)(((unsigned char)(var_19 >> 4UL) ^ (unsigned char)var_19) & '\xc0') != 0UL) {
        rbx_1 = var_18;
        while (1U)
            {
                var_20 = *(uint64_t *)((rbx_0 << 3UL) + *(uint64_t *)(var_17 + 16UL));
                local_sp_2 = local_sp_1;
                rcx8_2 = rcx8_1;
                r812_1 = r812_0;
                if (*(uint64_t *)var_20 == r13_1) {
                    var_24 = rbx_0 + 1UL;
                    local_sp_1 = local_sp_2;
                    rcx8_1 = rcx8_2;
                    rbx_0 = var_24;
                    r812_0 = r812_1;
                    rcx8_3 = rcx8_2;
                    local_sp_3 = local_sp_2;
                    r812_2 = r812_1;
                    if (var_24 != var_18) {
                        continue;
                    }
                    break;
                }
                var_21 = var_20 + 8UL;
                var_22 = local_sp_1 + (-8L);
                *(uint64_t *)var_22 = 4238542UL;
                var_23 = indirect_placeholder_12(var_21, rdx);
                local_sp_2 = var_22;
                rcx8_2 = var_23.field_1;
                r812_1 = var_23.field_2;
                if ((uint64_t)(unsigned char)var_23.field_0 == 0UL) {
                    mrv4.field_0 = var_20;
                    mrv5 = mrv4;
                    mrv5.field_1 = r9;
                    return mrv5;
                }
            }
    }
    var_25 = local_sp_3 + (-8L);
    *(uint64_t *)var_25 = 4238583UL;
    var_26 = indirect_placeholder_13(rcx8_3, r10, rbx_1, r9, 112UL, var_18, r812_2, 1UL);
    var_27 = var_26.field_0;
    var_28 = var_26.field_4;
    r910_0 = var_28;
    r910_4 = var_28;
    local_sp_0 = var_25;
    r910_5 = var_28;
    if (var_27 == 0UL) {
        **(uint32_t **)(local_sp_0 + 8UL) = 12U;
        mrv3.field_1 = r910_5;
        return mrv3;
    }
    var_29 = var_27 + 8UL;
    var_30 = rdx + 16UL;
    var_31 = local_sp_3 + (-16L);
    var_32 = (uint64_t *)var_31;
    *var_32 = 4238615UL;
    var_33 = indirect_placeholder_11(var_29, var_30, var_8);
    if ((uint64_t)(uint32_t)var_33.field_0 == 0UL) {
        var_34 = local_sp_3 + (-24L);
        *(uint64_t *)var_34 = 4238891UL;
        indirect_placeholder();
        local_sp_0 = var_34;
    } else {
        var_35 = *var_9;
        *(uint64_t *)(var_27 + 80UL) = var_29;
        if ((long)var_35 <= (long)0UL) {
            var_36 = **(uint64_t **)var_31;
            var_37 = (uint64_t *)var_30;
            var_38 = (unsigned char *)(var_27 + 104UL);
            while (1U)
                {
                    var_39 = (*(uint64_t *)((rdx13_0 << 3UL) + *var_37) << 4UL) + var_36;
                    var_40 = var_39 + 8UL;
                    var_41 = (uint32_t)(uint64_t)*(unsigned char *)var_40;
                    r910_1 = r910_0;
                    r910_3 = r910_0;
                    if ((uint64_t)(var_41 + (-1)) == 0UL) {
                        var_49 = (uint32_t *)var_40;
                        _pre_phi = var_49;
                        if ((*var_49 & 261888U) != 0U) {
                            var_53 = rdx13_0 + 1UL;
                            r910_0_be = r910_3;
                            rdx13_0_be = var_53;
                            r910_4 = r910_3;
                            if ((long)var_53 < (long)*var_9) {
                                break;
                            }
                            r910_0 = r910_0_be;
                            rdx13_0 = rdx13_0_be;
                            continue;
                        }
                        var_50 = ((*(unsigned char *)(var_39 + 10UL) << '\x01') & ' ') | *var_38;
                        *var_38 = var_50;
                        var_51 = var_50;
                    } else {
                        var_42 = *var_38;
                        var_43 = (uint64_t)(*(unsigned char *)(var_39 + 10UL) >> '\x04');
                        var_44 = var_42 & '\xdf';
                        var_45 = (uint64_t)var_42 >> 5UL;
                        var_46 = var_44 | (unsigned char)(((var_45 | var_43) << 5UL) & 32UL);
                        var_47 = (uint64_t)(var_41 + (-2));
                        *var_38 = var_46;
                        r910_3 = var_45;
                        r910_4 = var_45;
                        r910_0_be = var_45;
                        var_52 = var_46;
                        var_51 = var_46;
                        r910_1 = var_45;
                        r910_2 = var_45;
                        if (var_47 != 0UL) {
                            var_48 = rdx13_0 + 1UL;
                            *var_38 = (var_46 | '\x10');
                            rdx13_0_be = var_48;
                            if ((long)var_48 < (long)*var_9) {
                                break;
                            }
                            r910_0 = r910_0_be;
                            rdx13_0 = rdx13_0_be;
                            continue;
                        }
                        if ((uint64_t)(var_41 + (-4)) != 0UL) {
                            *var_38 = (var_46 | '@');
                            var_53 = rdx13_0 + 1UL;
                            r910_0_be = r910_3;
                            rdx13_0_be = var_53;
                            r910_4 = r910_3;
                            if ((long)var_53 < (long)*var_9) {
                                break;
                            }
                            r910_0 = r910_0_be;
                            rdx13_0 = rdx13_0_be;
                            continue;
                        }
                        if ((uint64_t)(var_41 + (-12)) != 0UL) {
                            *var_38 = (var_52 | '\x80');
                            r910_3 = r910_2;
                            var_53 = rdx13_0 + 1UL;
                            r910_0_be = r910_3;
                            rdx13_0_be = var_53;
                            r910_4 = r910_3;
                            if ((long)var_53 < (long)*var_9) {
                                break;
                            }
                            r910_0 = r910_0_be;
                            rdx13_0 = rdx13_0_be;
                            continue;
                        }
                        _pre_phi = (uint32_t *)var_40;
                    }
                    var_52 = var_51;
                    r910_2 = r910_1;
                    r910_3 = r910_1;
                    if ((*_pre_phi & 261888U) == 0U) {
                        *var_38 = (var_52 | '\x80');
                        r910_3 = r910_2;
                    }
                    var_53 = rdx13_0 + 1UL;
                    r910_0_be = r910_3;
                    rdx13_0_be = var_53;
                    r910_4 = r910_3;
                    if ((long)var_53 < (long)*var_9) {
                        break;
                    }
                    r910_0 = r910_0_be;
                    rdx13_0 = rdx13_0_be;
                    continue;
                }
        }
        var_54 = *var_32;
        *(uint64_t *)(local_sp_3 + (-24L)) = 4238847UL;
        var_55 = indirect_placeholder_7(var_54, r13_1, var_27);
        r910_5 = r910_4;
        if ((uint64_t)(uint32_t)var_55 != 0UL) {
            mrv.field_0 = var_27;
            mrv1 = mrv;
            mrv1.field_1 = r910_4;
            return mrv1;
        }
        var_56 = local_sp_3 + (-32L);
        *(uint64_t *)var_56 = 4238950UL;
        indirect_placeholder_4(var_27);
        local_sp_0 = var_56;
    }
    **(uint32_t **)(local_sp_0 + 8UL) = 12U;
    mrv3.field_1 = r910_5;
    return mrv3;
}

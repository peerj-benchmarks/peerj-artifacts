typedef unsigned char __u_char;
typedef unsigned short __u_short;
typedef unsigned int __u_int;
typedef unsigned long __u_long;
typedef signed char __int8_t;
typedef unsigned char __uint8_t;
typedef short __int16_t;
typedef unsigned short __uint16_t;
typedef int __int32_t;
typedef unsigned int __uint32_t;
typedef long __int64_t;
typedef unsigned long __uint64_t;
typedef __int8_t __int_least8_t;
typedef __uint8_t __uint_least8_t;
typedef __int16_t __int_least16_t;
typedef __uint16_t __uint_least16_t;
typedef __int32_t __int_least32_t;
typedef __uint32_t __uint_least32_t;
typedef __int64_t __int_least64_t;
typedef __uint64_t __uint_least64_t;
typedef long __quad_t;
typedef unsigned long __u_quad_t;
typedef long __intmax_t;
typedef unsigned long __uintmax_t;
typedef unsigned long __dev_t;
typedef unsigned int __uid_t;
typedef unsigned int __gid_t;
typedef unsigned long __ino_t;
typedef unsigned long __ino64_t;
typedef unsigned int __mode_t;
typedef unsigned long __nlink_t;
typedef long __off_t;
typedef long __off64_t;
typedef int __pid_t;
typedef struct {
    int __val[2];
} __fsid_t;
typedef long __clock_t;
typedef unsigned long __rlim_t;
typedef unsigned long __rlim64_t;
typedef unsigned int __id_t;
typedef long __time_t;
typedef unsigned int __useconds_t;
typedef long __suseconds_t;
typedef int __daddr_t;
typedef int __key_t;
typedef int __clockid_t;
typedef void *__timer_t;
typedef long __blksize_t;
typedef long __blkcnt_t;
typedef long __blkcnt64_t;
typedef unsigned long __fsblkcnt_t;
typedef unsigned long __fsblkcnt64_t;
typedef unsigned long __fsfilcnt_t;
typedef unsigned long __fsfilcnt64_t;
typedef long __fsword_t;
typedef long __ssize_t;
typedef long __syscall_slong_t;
typedef unsigned long __syscall_ulong_t;
typedef __off64_t __loff_t;
typedef char *__caddr_t;
typedef long __intptr_t;
typedef unsigned int __socklen_t;
typedef int __sig_atomic_t;
typedef __int8_t int8_t;
typedef __int16_t int16_t;
typedef __int32_t int32_t;
typedef __int64_t int64_t;
typedef __uint8_t uint8_t;
typedef __uint16_t uint16_t;
typedef __uint32_t uint32_t;
typedef __uint64_t uint64_t;
typedef __int_least8_t int_least8_t;
typedef __int_least16_t int_least16_t;
typedef __int_least32_t int_least32_t;
typedef __int_least64_t int_least64_t;
typedef __uint_least8_t uint_least8_t;
typedef __uint_least16_t uint_least16_t;
typedef __uint_least32_t uint_least32_t;
typedef __uint_least64_t uint_least64_t;
typedef signed char int_fast8_t;
typedef long int_fast16_t;
typedef long int_fast32_t;
typedef long int_fast64_t;
typedef unsigned char uint_fast8_t;
typedef unsigned long uint_fast16_t;
typedef unsigned long uint_fast32_t;
typedef unsigned long uint_fast64_t;
typedef long intptr_t;
typedef unsigned long uintptr_t;
typedef __intmax_t intmax_t;
typedef __uintmax_t uintmax_t;
struct indirect_placeholder_207_ret_type;
struct indirect_placeholder_207_ret_type {
    uint64_t field_0;
    uint64_t field_1;
};
extern uint64_t revng_init_local_sp(uint64_t param_0);
extern uint64_t init_rbx(void);
extern uint64_t init_rbp(void);
extern uint64_t init_r12(void);
extern uint64_t init_r13(void);
extern uint64_t init_r14(void);
extern uint64_t indirect_placeholder_107(uint64_t param_0, uint64_t param_1);
extern void indirect_placeholder_27(uint64_t param_0, uint64_t param_1);
extern struct indirect_placeholder_207_ret_type indirect_placeholder_207(uint64_t param_0, uint64_t param_1);
uint64_t bb_register_state(uint64_t rdi, uint64_t rdx, uint64_t rsi) {
    uint64_t rbx_0;
    uint64_t var_0;
    uint64_t var_1;
    uint64_t var_2;
    uint64_t var_3;
    uint64_t var_4;
    uint64_t var_5;
    uint64_t var_6;
    uint64_t *var_7;
    uint64_t var_8;
    uint64_t var_9;
    struct indirect_placeholder_207_ret_type var_10;
    uint64_t var_28;
    uint64_t rax_0;
    uint64_t var_11;
    uint64_t local_sp_2;
    uint64_t *var_12;
    uint64_t var_17;
    uint64_t var_13;
    uint64_t local_sp_0;
    uint64_t var_14;
    uint64_t var_15;
    uint64_t var_16;
    uint64_t local_sp_1;
    uint64_t var_18;
    uint64_t var_19;
    uint64_t var_20;
    uint64_t var_21;
    uint64_t *_cast;
    uint64_t var_22;
    uint64_t *var_23;
    uint64_t rdx2_0;
    uint64_t var_24;
    uint64_t *var_25;
    uint64_t var_26;
    uint64_t var_27;
    unsigned int loop_state_var;
    var_0 = revng_init_local_sp(0UL);
    var_1 = init_rbx();
    var_2 = init_rbp();
    var_3 = init_r13();
    var_4 = init_r12();
    var_5 = init_r14();
    *(uint64_t *)(var_0 + (-8L)) = var_5;
    *(uint64_t *)(var_0 + (-16L)) = var_3;
    *(uint64_t *)(var_0 + (-24L)) = var_4;
    var_6 = rsi + 32UL;
    *(uint64_t *)(var_0 + (-32L)) = var_2;
    *(uint64_t *)(var_0 + (-40L)) = var_1;
    var_7 = (uint64_t *)(rsi + 16UL);
    var_8 = *var_7;
    *(uint64_t *)rsi = rdx;
    var_9 = var_0 + (-48L);
    *(uint64_t *)var_9 = 4227301UL;
    var_10 = indirect_placeholder_207(var_6, var_8);
    local_sp_0 = var_9;
    rbx_0 = 0UL;
    local_sp_2 = var_9;
    if ((uint64_t)(uint32_t)var_10.field_0 == 0UL) {
        return 12UL;
    }
    var_11 = *var_7;
    var_13 = var_11;
    if ((long)var_11 <= (long)0UL) {
        var_12 = (uint64_t *)rdi;
        while (1U)
            {
                var_14 = *(uint64_t *)((rbx_0 << 3UL) + *(uint64_t *)(rsi + 24UL));
                var_17 = var_13;
                local_sp_1 = local_sp_0;
                if ((*(unsigned char *)(((var_14 << 4UL) + *var_12) + 8UL) & '\b') != '\x00') {
                    var_15 = local_sp_0 + (-8L);
                    *(uint64_t *)var_15 = 4227362UL;
                    var_16 = indirect_placeholder_107(var_6, var_14);
                    local_sp_1 = var_15;
                    if ((uint64_t)(unsigned char)var_16 != 0UL) {
                        loop_state_var = 0U;
                        break;
                    }
                    var_17 = *var_7;
                }
                var_18 = rbx_0 + 1UL;
                var_13 = var_17;
                local_sp_0 = local_sp_1;
                rbx_0 = var_18;
                local_sp_2 = local_sp_1;
                if ((long)var_17 > (long)var_18) {
                    continue;
                }
                loop_state_var = 1U;
                break;
            }
        switch (loop_state_var) {
          case 0U:
            {
                return 12UL;
            }
            break;
          case 1U:
            {
                break;
            }
            break;
        }
    }
    var_19 = *(uint64_t *)(rdi + 136UL) & rdx;
    var_20 = *(uint64_t *)(rdi + 64UL);
    var_21 = (var_19 * 24UL) + var_20;
    _cast = (uint64_t *)var_21;
    var_22 = *_cast;
    var_23 = (uint64_t *)(var_21 + 8UL);
    rax_0 = var_20;
    rdx2_0 = var_22;
    if ((long)*var_23 > (long)var_22) {
        rax_0 = *(uint64_t *)(var_21 + 16UL);
    } else {
        var_24 = var_22 + 1UL;
        var_25 = (uint64_t *)(var_21 + 16UL);
        var_26 = *var_25;
        var_27 = var_24 << 4UL;
        *(uint64_t *)(local_sp_2 + (-8L)) = 4227460UL;
        indirect_placeholder_27(var_26, var_27);
        if (var_20 != 0UL) {
            return 12UL;
        }
        var_28 = var_24 << 1UL;
        *var_25 = var_20;
        *var_23 = var_28;
        rdx2_0 = *_cast;
    }
    *_cast = (rdx2_0 + 1UL);
    *(uint64_t *)((rdx2_0 << 3UL) + rax_0) = rsi;
    return 0UL;
}

typedef unsigned char __u_char;
typedef unsigned short __u_short;
typedef unsigned int __u_int;
typedef unsigned long __u_long;
typedef signed char __int8_t;
typedef unsigned char __uint8_t;
typedef short __int16_t;
typedef unsigned short __uint16_t;
typedef int __int32_t;
typedef unsigned int __uint32_t;
typedef long __int64_t;
typedef unsigned long __uint64_t;
typedef __int8_t __int_least8_t;
typedef __uint8_t __uint_least8_t;
typedef __int16_t __int_least16_t;
typedef __uint16_t __uint_least16_t;
typedef __int32_t __int_least32_t;
typedef __uint32_t __uint_least32_t;
typedef __int64_t __int_least64_t;
typedef __uint64_t __uint_least64_t;
typedef long __quad_t;
typedef unsigned long __u_quad_t;
typedef long __intmax_t;
typedef unsigned long __uintmax_t;
typedef unsigned long __dev_t;
typedef unsigned int __uid_t;
typedef unsigned int __gid_t;
typedef unsigned long __ino_t;
typedef unsigned long __ino64_t;
typedef unsigned int __mode_t;
typedef unsigned long __nlink_t;
typedef long __off_t;
typedef long __off64_t;
typedef int __pid_t;
typedef struct {
    int __val[2];
} __fsid_t;
typedef long __clock_t;
typedef unsigned long __rlim_t;
typedef unsigned long __rlim64_t;
typedef unsigned int __id_t;
typedef long __time_t;
typedef unsigned int __useconds_t;
typedef long __suseconds_t;
typedef int __daddr_t;
typedef int __key_t;
typedef int __clockid_t;
typedef void *__timer_t;
typedef long __blksize_t;
typedef long __blkcnt_t;
typedef long __blkcnt64_t;
typedef unsigned long __fsblkcnt_t;
typedef unsigned long __fsblkcnt64_t;
typedef unsigned long __fsfilcnt_t;
typedef unsigned long __fsfilcnt64_t;
typedef long __fsword_t;
typedef long __ssize_t;
typedef long __syscall_slong_t;
typedef unsigned long __syscall_ulong_t;
typedef __off64_t __loff_t;
typedef char *__caddr_t;
typedef long __intptr_t;
typedef unsigned int __socklen_t;
typedef int __sig_atomic_t;
typedef __int8_t int8_t;
typedef __int16_t int16_t;
typedef __int32_t int32_t;
typedef __int64_t int64_t;
typedef __uint8_t uint8_t;
typedef __uint16_t uint16_t;
typedef __uint32_t uint32_t;
typedef __uint64_t uint64_t;
typedef __int_least8_t int_least8_t;
typedef __int_least16_t int_least16_t;
typedef __int_least32_t int_least32_t;
typedef __int_least64_t int_least64_t;
typedef __uint_least8_t uint_least8_t;
typedef __uint_least16_t uint_least16_t;
typedef __uint_least32_t uint_least32_t;
typedef __uint_least64_t uint_least64_t;
typedef signed char int_fast8_t;
typedef long int_fast16_t;
typedef long int_fast32_t;
typedef long int_fast64_t;
typedef unsigned char uint_fast8_t;
typedef unsigned long uint_fast16_t;
typedef unsigned long uint_fast32_t;
typedef unsigned long uint_fast64_t;
typedef long intptr_t;
typedef unsigned long uintptr_t;
typedef __intmax_t intmax_t;
typedef __uintmax_t uintmax_t;
extern uint64_t revng_init_local_sp(uint64_t param_0);
extern uint64_t init_rbx(void);
extern uint64_t init_rbp(void);
extern void indirect_placeholder(void);
extern uint64_t init_cc_src2(void);
extern uint64_t helper_cc_compute_c_wrapper(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint32_t param_3);
extern uint64_t indirect_placeholder_210(void);
typedef _Bool bool;
uint64_t bb_find_line(uint64_t rdi) {
    uint64_t var_0;
    uint64_t var_1;
    uint64_t var_2;
    uint64_t var_3;
    uint64_t var_4;
    uint64_t var_5;
    uint64_t rbx_0;
    uint64_t var_16;
    uint64_t local_sp_4;
    uint64_t rbx_2;
    uint64_t local_sp_0;
    uint64_t rdi1_0;
    uint64_t rdi1_1;
    uint64_t rbx_3;
    uint64_t rbx_1;
    uint64_t local_sp_1;
    uint64_t local_sp_2;
    uint64_t var_17;
    uint64_t var_18;
    uint64_t var_19;
    uint64_t rax_0;
    uint64_t local_sp_3;
    uint64_t *var_12;
    uint64_t var_13;
    uint64_t var_14;
    uint64_t var_15;
    uint64_t rbx_4;
    uint64_t var_20;
    uint64_t var_21;
    uint64_t rdx_0;
    uint64_t var_22;
    uint64_t var_23;
    uint64_t rdx_1;
    uint64_t var_8;
    uint64_t var_9;
    uint64_t var_10;
    uint64_t var_11;
    uint64_t var_6;
    uint64_t var_7;
    unsigned int loop_state_var;
    bool switch_state_var;
    var_0 = revng_init_local_sp(0UL);
    var_1 = init_rbx();
    var_2 = init_rbp();
    var_3 = init_cc_src2();
    *(uint64_t *)(var_0 + (-8L)) = var_2;
    *(uint64_t *)(var_0 + (-16L)) = var_1;
    var_4 = var_0 + (-24L);
    var_5 = *(uint64_t *)6437568UL;
    rbx_0 = var_5;
    local_sp_0 = var_4;
    rbx_3 = 0UL;
    rbx_4 = 0UL;
    if (var_5 != 0UL) {
        if (*(unsigned char *)6437528UL == '\x00') {
            return 0UL;
        }
        var_6 = var_0 + (-32L);
        *(uint64_t *)var_6 = 4209932UL;
        var_7 = indirect_placeholder_210();
        local_sp_0 = var_6;
        if ((uint64_t)(unsigned char)var_7 != 0UL) {
            return 0UL;
        }
        rbx_0 = *(uint64_t *)6437568UL;
    }
    rbx_1 = rbx_0;
    local_sp_1 = local_sp_0;
    if (*(uint64_t *)(rbx_0 + 16UL) > rdi) {
        return 0UL;
    }
    loop_state_var = 2U;
    while (1U)
        {
            switch_state_var = 0;
            switch (loop_state_var) {
              case 1U:
                {
                    var_12 = (uint64_t *)(rbx_3 + 64UL);
                    var_13 = *var_12;
                    rbx_1 = var_13;
                    local_sp_1 = local_sp_3;
                    if (var_13 != 0UL) {
                        loop_state_var = 2U;
                        continue;
                    }
                    if (*(unsigned char *)6437528UL != '\x00') {
                        loop_state_var = 1U;
                        switch_state_var = 1;
                        break;
                    }
                    var_14 = local_sp_3 + (-8L);
                    *(uint64_t *)var_14 = 4209787UL;
                    var_15 = indirect_placeholder_210();
                    local_sp_2 = var_14;
                    local_sp_4 = var_14;
                    if ((uint64_t)(unsigned char)var_15 != 0UL) {
                        loop_state_var = 1U;
                        switch_state_var = 1;
                        break;
                    }
                    var_16 = *var_12;
                    rbx_2 = var_16;
                    if (var_16 != 0UL) {
                        loop_state_var = 0U;
                        continue;
                    }
                    var_17 = *(uint64_t *)(rbx_2 + 16UL);
                    var_18 = var_17 + *(uint64_t *)(rbx_2 + 32UL);
                    var_19 = helper_cc_compute_c_wrapper(rdi - var_18, var_18, var_3, 17U);
                    rbx_3 = rbx_2;
                    local_sp_3 = local_sp_2;
                    rax_0 = var_17;
                    rbx_4 = rbx_2;
                    if (var_19 != 0UL) {
                        loop_state_var = 1U;
                        continue;
                    }
                    loop_state_var = 0U;
                    switch_state_var = 1;
                    break;
                }
                break;
              case 0U:
                {
                    var_8 = local_sp_4 + (-8L);
                    *(uint64_t *)var_8 = 4209833UL;
                    indirect_placeholder();
                    var_9 = *(uint64_t *)16UL;
                    var_10 = var_9 + *(uint64_t *)32UL;
                    var_11 = helper_cc_compute_c_wrapper(rdi - var_10, var_10, var_3, 17U);
                    local_sp_3 = var_8;
                    rax_0 = var_9;
                    if (var_11 != 0UL) {
                        loop_state_var = 0U;
                        switch_state_var = 1;
                        break;
                    }
                }
                break;
              case 2U:
                {
                    rbx_2 = rbx_1;
                    local_sp_2 = local_sp_1;
                    local_sp_4 = local_sp_1;
                    if (rbx_1 != 0UL) {
                        var_8 = local_sp_4 + (-8L);
                        *(uint64_t *)var_8 = 4209833UL;
                        indirect_placeholder();
                        var_9 = *(uint64_t *)16UL;
                        var_10 = var_9 + *(uint64_t *)32UL;
                        var_11 = helper_cc_compute_c_wrapper(rdi - var_10, var_10, var_3, 17U);
                        local_sp_3 = var_8;
                        rax_0 = var_9;
                        if (var_11 != 0UL) {
                            loop_state_var = 0U;
                            switch_state_var = 1;
                            break;
                        }
                    }
                    var_17 = *(uint64_t *)(rbx_2 + 16UL);
                    var_18 = var_17 + *(uint64_t *)(rbx_2 + 32UL);
                    var_19 = helper_cc_compute_c_wrapper(rdi - var_18, var_18, var_3, 17U);
                    rbx_3 = rbx_2;
                    local_sp_3 = local_sp_2;
                    rax_0 = var_17;
                    rbx_4 = rbx_2;
                    if (var_19 != 0UL) {
                        loop_state_var = 1U;
                        continue;
                    }
                    loop_state_var = 0U;
                    switch_state_var = 1;
                    break;
                }
                break;
            }
            if (switch_state_var)
                break;
        }
    switch (loop_state_var) {
      case 1U:
        {
            return 0UL;
        }
        break;
      case 0U:
        {
            var_20 = *(uint64_t *)(rbx_4 + 48UL);
            var_21 = rdi - rax_0;
            rdi1_0 = var_21;
            rdx_0 = var_20;
            rdi1_1 = var_21;
            rdx_1 = var_20;
            if (var_21 <= 79UL) {
                var_22 = rdi1_0 + (-80L);
                var_23 = *(uint64_t *)(rdx_0 + 1304UL);
                rdi1_0 = var_22;
                rdx_0 = var_23;
                rdi1_1 = var_22;
                rdx_1 = var_23;
                do {
                    var_22 = rdi1_0 + (-80L);
                    var_23 = *(uint64_t *)(rdx_0 + 1304UL);
                    rdi1_0 = var_22;
                    rdx_0 = var_23;
                    rdi1_1 = var_22;
                    rdx_1 = var_23;
                } while (var_22 <= 79UL);
            }
            return (((rdi1_1 << 4UL) + 16UL) + rdx_1) + 8UL;
        }
        break;
    }
}

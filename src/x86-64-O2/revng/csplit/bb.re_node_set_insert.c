typedef unsigned char __u_char;
typedef unsigned short __u_short;
typedef unsigned int __u_int;
typedef unsigned long __u_long;
typedef signed char __int8_t;
typedef unsigned char __uint8_t;
typedef short __int16_t;
typedef unsigned short __uint16_t;
typedef int __int32_t;
typedef unsigned int __uint32_t;
typedef long __int64_t;
typedef unsigned long __uint64_t;
typedef __int8_t __int_least8_t;
typedef __uint8_t __uint_least8_t;
typedef __int16_t __int_least16_t;
typedef __uint16_t __uint_least16_t;
typedef __int32_t __int_least32_t;
typedef __uint32_t __uint_least32_t;
typedef __int64_t __int_least64_t;
typedef __uint64_t __uint_least64_t;
typedef long __quad_t;
typedef unsigned long __u_quad_t;
typedef long __intmax_t;
typedef unsigned long __uintmax_t;
typedef unsigned long __dev_t;
typedef unsigned int __uid_t;
typedef unsigned int __gid_t;
typedef unsigned long __ino_t;
typedef unsigned long __ino64_t;
typedef unsigned int __mode_t;
typedef unsigned long __nlink_t;
typedef long __off_t;
typedef long __off64_t;
typedef int __pid_t;
typedef struct {
    int __val[2];
} __fsid_t;
typedef long __clock_t;
typedef unsigned long __rlim_t;
typedef unsigned long __rlim64_t;
typedef unsigned int __id_t;
typedef long __time_t;
typedef unsigned int __useconds_t;
typedef long __suseconds_t;
typedef int __daddr_t;
typedef int __key_t;
typedef int __clockid_t;
typedef void *__timer_t;
typedef long __blksize_t;
typedef long __blkcnt_t;
typedef long __blkcnt64_t;
typedef unsigned long __fsblkcnt_t;
typedef unsigned long __fsblkcnt64_t;
typedef unsigned long __fsfilcnt_t;
typedef unsigned long __fsfilcnt64_t;
typedef long __fsword_t;
typedef long __ssize_t;
typedef long __syscall_slong_t;
typedef unsigned long __syscall_ulong_t;
typedef __off64_t __loff_t;
typedef char *__caddr_t;
typedef long __intptr_t;
typedef unsigned int __socklen_t;
typedef int __sig_atomic_t;
typedef __int8_t int8_t;
typedef __int16_t int16_t;
typedef __int32_t int32_t;
typedef __int64_t int64_t;
typedef __uint8_t uint8_t;
typedef __uint16_t uint16_t;
typedef __uint32_t uint32_t;
typedef __uint64_t uint64_t;
typedef __int_least8_t int_least8_t;
typedef __int_least16_t int_least16_t;
typedef __int_least32_t int_least32_t;
typedef __int_least64_t int_least64_t;
typedef __uint_least8_t uint_least8_t;
typedef __uint_least16_t uint_least16_t;
typedef __uint_least32_t uint_least32_t;
typedef __uint_least64_t uint_least64_t;
typedef signed char int_fast8_t;
typedef long int_fast16_t;
typedef long int_fast32_t;
typedef long int_fast64_t;
typedef unsigned char uint_fast8_t;
typedef unsigned long uint_fast16_t;
typedef unsigned long uint_fast32_t;
typedef unsigned long uint_fast64_t;
typedef long intptr_t;
typedef unsigned long uintptr_t;
typedef __intmax_t intmax_t;
typedef __uintmax_t uintmax_t;
typedef _Bool bool;
struct bb_re_node_set_insert_ret_type;
struct indirect_placeholder_209_ret_type;
struct bb_re_node_set_insert_ret_type {
    uint64_t field_0;
    uint64_t field_1;
};
struct indirect_placeholder_209_ret_type {
    uint64_t field_0;
    uint64_t field_1;
};
extern uint64_t revng_init_local_sp(uint64_t param_0);
extern uint64_t init_rbx(void);
extern uint64_t init_rbp(void);
extern uint64_t helper_cc_compute_all_wrapper(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint32_t param_3);
extern void indirect_placeholder_27(uint64_t param_0, uint64_t param_1);
extern uint64_t init_rcx(void);
extern struct indirect_placeholder_209_ret_type indirect_placeholder_209(uint64_t param_0);
struct bb_re_node_set_insert_ret_type bb_re_node_set_insert(uint64_t rdi, uint64_t rsi) {
    uint64_t var_0;
    uint64_t var_1;
    uint64_t var_2;
    uint64_t var_3;
    uint64_t *_cast4;
    uint64_t var_4;
    uint64_t rax_0;
    struct bb_re_node_set_insert_ret_type mrv1 = {1UL, /*implicit*/(int)0};
    uint64_t rax_1;
    uint64_t *var_5;
    uint64_t var_6;
    uint64_t var_18;
    uint64_t rdx_0;
    uint64_t var_15;
    bool var_16;
    uint64_t var_17;
    uint64_t rcx_1;
    uint64_t rcx_0;
    uint64_t rdx_1_in;
    uint64_t rdx_1;
    uint64_t var_11;
    uint64_t var_12;
    uint64_t rdx_2;
    uint64_t var_13;
    uint64_t var_14;
    uint64_t rdx_3;
    uint64_t rcx_2;
    struct bb_re_node_set_insert_ret_type mrv2;
    struct bb_re_node_set_insert_ret_type mrv3;
    uint64_t var_7;
    uint64_t var_8;
    uint64_t *var_9;
    uint64_t var_10;
    uint64_t *var_19;
    struct indirect_placeholder_209_ret_type var_20;
    uint64_t var_21;
    var_0 = revng_init_local_sp(0UL);
    var_1 = init_rcx();
    var_2 = init_rbx();
    var_3 = init_rbp();
    *(uint64_t *)(var_0 + (-8L)) = var_3;
    *(uint64_t *)(var_0 + (-16L)) = var_2;
    _cast4 = (uint64_t *)rdi;
    var_4 = *_cast4;
    rcx_1 = var_1;
    rax_1 = 0UL;
    rcx_2 = var_1;
    if (var_4 == 0UL) {
        *_cast4 = 1UL;
        var_19 = (uint64_t *)(rdi + 8UL);
        *var_19 = 1UL;
        *(uint64_t *)(var_0 + (-32L)) = 4226945UL;
        var_20 = indirect_placeholder_209(8UL);
        var_21 = var_20.field_0;
        *(uint64_t *)(rdi + 16UL) = var_21;
        if (var_21 != 0UL) {
            *(uint64_t *)var_21 = rsi;
            mrv1.field_1 = var_1;
            return mrv1;
        }
        *var_19 = 0UL;
        *_cast4 = 0UL;
    } else {
        var_5 = (uint64_t *)(rdi + 8UL);
        var_6 = *var_5;
        rdx_0 = var_6;
        rax_1 = 1UL;
        if (var_6 == 0UL) {
            **(uint64_t **)(rdi + 16UL) = rsi;
            *var_5 = (*var_5 + 1UL);
        } else {
            rax_1 = 0UL;
            if (var_4 == var_6) {
                rax_0 = *(uint64_t *)(rdi + 16UL);
            } else {
                var_7 = var_4 << 1UL;
                var_8 = var_4 << 4UL;
                *_cast4 = var_7;
                var_9 = (uint64_t *)(rdi + 16UL);
                var_10 = *var_9;
                *(uint64_t *)(var_0 + (-32L)) = 4227139UL;
                indirect_placeholder_27(var_10, var_8);
                rax_0 = var_7;
                if (var_7 != 0UL) {
                    mrv2.field_0 = rax_1;
                    mrv3 = mrv2;
                    mrv3.field_1 = rcx_2;
                    return mrv3;
                }
                *var_9 = var_7;
                rdx_0 = *var_5;
            }
            if ((long)*(uint64_t *)rax_0 > (long)rsi) {
                var_15 = helper_cc_compute_all_wrapper(rdx_0, 0UL, 0UL, 25U);
                var_16 = ((uint64_t)(((unsigned char)(var_15 >> 4UL) ^ (unsigned char)var_15) & '\xc0') == 0UL);
                var_17 = rdx_0 << 3UL;
                rdx_3 = var_17;
                if (!var_16) {
                    rdx_1_in = var_17 + rax_0;
                    rdx_3 = 0UL;
                    rdx_1 = rdx_1_in + (-8L);
                    var_18 = *(uint64_t *)rdx_1;
                    *(uint64_t *)rdx_1_in = var_18;
                    rdx_1_in = rdx_1;
                    rcx_1 = var_18;
                    do {
                        rdx_1 = rdx_1_in + (-8L);
                        var_18 = *(uint64_t *)rdx_1;
                        *(uint64_t *)rdx_1_in = var_18;
                        rdx_1_in = rdx_1;
                        rcx_1 = var_18;
                    } while (rdx_1 != rax_0);
                }
            } else {
                var_11 = rdx_0 << 3UL;
                var_12 = *(uint64_t *)((var_11 + rax_0) + (-8L));
                rcx_0 = var_12;
                rdx_2 = var_11;
                rcx_1 = var_12;
                rdx_3 = var_11;
                if ((long)var_12 > (long)rsi) {
                    *(uint64_t *)(rdx_2 + rax_0) = rcx_0;
                    var_13 = rdx_2 + (-8L);
                    var_14 = *(uint64_t *)((var_13 + rax_0) + (-8L));
                    rcx_0 = var_14;
                    rdx_2 = var_13;
                    rcx_1 = var_14;
                    rdx_3 = var_13;
                    do {
                        *(uint64_t *)(rdx_2 + rax_0) = rcx_0;
                        var_13 = rdx_2 + (-8L);
                        var_14 = *(uint64_t *)((var_13 + rax_0) + (-8L));
                        rcx_0 = var_14;
                        rdx_2 = var_13;
                        rcx_1 = var_14;
                        rdx_3 = var_13;
                    } while ((long)var_14 <= (long)rsi);
                }
            }
            *(uint64_t *)(rdx_3 + rax_0) = rsi;
            *var_5 = (*var_5 + 1UL);
            rcx_2 = rcx_1;
        }
    }
    mrv2.field_0 = rax_1;
    mrv3 = mrv2;
    mrv3.field_1 = rcx_2;
    return mrv3;
}

typedef unsigned char __u_char;
typedef unsigned short __u_short;
typedef unsigned int __u_int;
typedef unsigned long __u_long;
typedef signed char __int8_t;
typedef unsigned char __uint8_t;
typedef short __int16_t;
typedef unsigned short __uint16_t;
typedef int __int32_t;
typedef unsigned int __uint32_t;
typedef long __int64_t;
typedef unsigned long __uint64_t;
typedef __int8_t __int_least8_t;
typedef __uint8_t __uint_least8_t;
typedef __int16_t __int_least16_t;
typedef __uint16_t __uint_least16_t;
typedef __int32_t __int_least32_t;
typedef __uint32_t __uint_least32_t;
typedef __int64_t __int_least64_t;
typedef __uint64_t __uint_least64_t;
typedef long __quad_t;
typedef unsigned long __u_quad_t;
typedef long __intmax_t;
typedef unsigned long __uintmax_t;
typedef unsigned long __dev_t;
typedef unsigned int __uid_t;
typedef unsigned int __gid_t;
typedef unsigned long __ino_t;
typedef unsigned long __ino64_t;
typedef unsigned int __mode_t;
typedef unsigned long __nlink_t;
typedef long __off_t;
typedef long __off64_t;
typedef int __pid_t;
typedef struct {
    int __val[2];
} __fsid_t;
typedef long __clock_t;
typedef unsigned long __rlim_t;
typedef unsigned long __rlim64_t;
typedef unsigned int __id_t;
typedef long __time_t;
typedef unsigned int __useconds_t;
typedef long __suseconds_t;
typedef int __daddr_t;
typedef int __key_t;
typedef int __clockid_t;
typedef void *__timer_t;
typedef long __blksize_t;
typedef long __blkcnt_t;
typedef long __blkcnt64_t;
typedef unsigned long __fsblkcnt_t;
typedef unsigned long __fsblkcnt64_t;
typedef unsigned long __fsfilcnt_t;
typedef unsigned long __fsfilcnt64_t;
typedef long __fsword_t;
typedef long __ssize_t;
typedef long __syscall_slong_t;
typedef unsigned long __syscall_ulong_t;
typedef __off64_t __loff_t;
typedef char *__caddr_t;
typedef long __intptr_t;
typedef unsigned int __socklen_t;
typedef int __sig_atomic_t;
typedef __int8_t int8_t;
typedef __int16_t int16_t;
typedef __int32_t int32_t;
typedef __int64_t int64_t;
typedef __uint8_t uint8_t;
typedef __uint16_t uint16_t;
typedef __uint32_t uint32_t;
typedef __uint64_t uint64_t;
typedef __int_least8_t int_least8_t;
typedef __int_least16_t int_least16_t;
typedef __int_least32_t int_least32_t;
typedef __int_least64_t int_least64_t;
typedef __uint_least8_t uint_least8_t;
typedef __uint_least16_t uint_least16_t;
typedef __uint_least32_t uint_least32_t;
typedef __uint_least64_t uint_least64_t;
typedef signed char int_fast8_t;
typedef long int_fast16_t;
typedef long int_fast32_t;
typedef long int_fast64_t;
typedef unsigned char uint_fast8_t;
typedef unsigned long uint_fast16_t;
typedef unsigned long uint_fast32_t;
typedef unsigned long uint_fast64_t;
typedef long intptr_t;
typedef unsigned long uintptr_t;
typedef __intmax_t intmax_t;
typedef __uintmax_t uintmax_t;
struct indirect_placeholder_186_ret_type;
struct indirect_placeholder_187_ret_type;
struct indirect_placeholder_188_ret_type;
struct indirect_placeholder_189_ret_type;
struct indirect_placeholder_186_ret_type {
    uint64_t field_0;
    uint64_t field_1;
};
struct indirect_placeholder_187_ret_type {
    uint64_t field_0;
    uint64_t field_1;
};
struct indirect_placeholder_188_ret_type {
    uint64_t field_0;
    uint64_t field_1;
    uint64_t field_2;
    uint64_t field_3;
    uint64_t field_4;
};
struct indirect_placeholder_189_ret_type {
    uint64_t field_0;
    uint64_t field_1;
};
extern uint64_t revng_init_local_sp(uint64_t param_0);
extern uint64_t init_rbx(void);
extern uint64_t init_rbp(void);
extern uint64_t init_r12(void);
extern void indirect_placeholder(void);
extern uint64_t init_r13(void);
extern uint64_t init_r14(void);
extern uint64_t indirect_placeholder_52(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint64_t param_3, uint64_t param_4);
extern uint64_t init_r15(void);
extern uint64_t indirect_placeholder_49(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint64_t param_3);
extern struct indirect_placeholder_186_ret_type indirect_placeholder_186(uint64_t param_0, uint64_t param_1);
extern struct indirect_placeholder_187_ret_type indirect_placeholder_187(uint64_t param_0, uint64_t param_1);
extern struct indirect_placeholder_188_ret_type indirect_placeholder_188(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint64_t param_3, uint64_t param_4);
extern struct indirect_placeholder_189_ret_type indirect_placeholder_189(uint64_t param_0, uint64_t param_1);
typedef _Bool bool;
uint64_t bb_calc_eclosure_iter(uint64_t rcx, uint64_t rdi, uint64_t rdx, uint64_t rsi) {
    struct indirect_placeholder_188_ret_type var_44;
    uint64_t var_25;
    uint64_t var_0;
    uint64_t var_1;
    uint64_t var_2;
    uint64_t var_3;
    uint64_t var_4;
    uint64_t var_5;
    uint64_t var_6;
    uint64_t *var_7;
    uint64_t var_8;
    uint64_t var_9;
    uint64_t var_10;
    uint64_t var_11;
    uint64_t var_12;
    struct indirect_placeholder_186_ret_type var_13;
    uint64_t var_14;
    uint64_t _pre_phi;
    uint64_t *_pre_phi136;
    uint64_t *_pre131;
    uint64_t local_sp_5;
    uint64_t local_sp_7;
    uint64_t local_sp_6;
    uint64_t merge;
    uint64_t rbp_2;
    uint64_t r12_1;
    uint64_t local_sp_0;
    uint64_t r13_2;
    uint64_t rbp_0;
    uint64_t *var_58;
    uint64_t var_59;
    uint64_t var_60;
    uint64_t *var_61;
    uint64_t *var_62;
    uint64_t *_pre127;
    uint64_t rsi4_0_ph;
    uint64_t r12_0_ph;
    uint64_t r14_0_ph;
    uint64_t *_pre135;
    uint64_t *_pre_phi132;
    uint64_t *_pre_phi128;
    uint64_t local_sp_1;
    uint64_t var_63;
    uint64_t r10_0;
    uint64_t local_sp_2;
    uint64_t r14_0;
    uint64_t r13_0;
    uint64_t var_49;
    uint64_t var_50;
    uint64_t var_51;
    uint64_t local_sp_4_ph;
    uint64_t var_48;
    uint64_t *var_15;
    uint64_t var_16;
    uint64_t *_cast;
    uint64_t var_17;
    uint64_t var_18;
    uint64_t var_19;
    uint32_t var_20;
    uint64_t var_21;
    uint64_t var_22;
    uint64_t local_sp_3;
    uint64_t var_23;
    uint64_t var_24;
    uint64_t var_26;
    uint64_t var_27;
    uint64_t rax_1_ph;
    uint64_t rbp_1_ph;
    uint64_t r13_1_ph;
    uint64_t var_28;
    uint64_t var_29;
    uint64_t *var_30;
    uint64_t rax_1;
    uint64_t r13_1;
    uint64_t var_31;
    uint64_t var_32;
    uint64_t var_33;
    uint64_t *var_34;
    uint64_t var_39;
    uint64_t var_52;
    uint64_t var_53;
    struct indirect_placeholder_187_ret_type var_54;
    uint64_t var_40;
    uint64_t var_41;
    uint64_t var_42;
    uint64_t var_43;
    uint64_t var_45;
    uint64_t var_46;
    uint64_t var_47;
    uint64_t var_35;
    uint64_t var_36;
    uint64_t var_37;
    uint64_t var_38;
    uint64_t var_55;
    uint64_t var_56;
    struct indirect_placeholder_189_ret_type var_57;
    unsigned int loop_state_var;
    bool switch_state_var;
    var_0 = revng_init_local_sp(0UL);
    var_1 = init_rbx();
    var_2 = init_rbp();
    var_3 = init_r13();
    var_4 = init_r12();
    var_5 = init_r15();
    var_6 = init_r14();
    *(uint64_t *)(var_0 + (-8L)) = var_5;
    *(uint64_t *)(var_0 + (-16L)) = var_6;
    *(uint64_t *)(var_0 + (-24L)) = var_3;
    *(uint64_t *)(var_0 + (-32L)) = var_4;
    *(uint64_t *)(var_0 + (-40L)) = var_2;
    *(uint64_t *)(var_0 + (-48L)) = var_1;
    var_7 = (uint64_t *)(rsi + 40UL);
    var_8 = *var_7;
    var_9 = rdx * 24UL;
    *(uint64_t *)(var_0 + (-136L)) = rdi;
    var_10 = var_0 + (-120L);
    *(uint32_t *)(var_0 + (-124L)) = (uint32_t)rcx;
    var_11 = *(uint64_t *)((var_9 + var_8) + 8UL) + 1UL;
    var_12 = var_0 + (-160L);
    *(uint64_t *)var_12 = 4235996UL;
    var_13 = indirect_placeholder_186(var_10, var_11);
    var_14 = var_13.field_0;
    local_sp_7 = var_12;
    merge = var_14;
    r13_2 = 1UL;
    rbp_0 = var_9;
    r12_0_ph = rdx;
    r14_0_ph = 0UL;
    local_sp_3 = var_12;
    rbp_1_ph = var_9;
    r13_1_ph = 0UL;
    if ((uint64_t)(uint32_t)var_14 == 0UL) {
        return merge;
    }
    var_15 = (uint64_t *)(rsi + 48UL);
    var_16 = *var_15;
    _cast = (uint64_t *)rsi;
    var_17 = *_cast;
    var_18 = rdx << 4UL;
    *(uint64_t *)((var_9 + var_16) + 8UL) = 18446744073709551615UL;
    var_19 = (var_18 + var_17) + 8UL;
    var_20 = *(uint32_t *)var_19;
    var_21 = (uint64_t)var_20;
    merge = 12UL;
    _pre_phi = var_19;
    if ((uint64_t)(var_20 & 261888U) != 0UL) {
        var_22 = var_9 + *var_7;
        if (*(uint64_t *)(var_22 + 8UL) != 0UL) {
            var_55 = local_sp_7 + 32UL;
            var_56 = local_sp_7 + (-8L);
            *(uint64_t *)var_56 = 4236445UL;
            var_57 = indirect_placeholder_189(var_55, rdx);
            local_sp_0 = var_56;
            if ((uint64_t)(unsigned char)var_57.field_0 == 0UL) {
                return merge;
            }
            var_58 = (uint64_t *)(local_sp_0 + 32UL);
            var_59 = *var_58;
            var_60 = rbp_0 + *var_15;
            *(uint64_t *)var_60 = var_59;
            var_61 = (uint64_t *)(local_sp_0 + 40UL);
            *(uint64_t *)(var_60 + 8UL) = *var_61;
            var_62 = (uint64_t *)(local_sp_0 + 48UL);
            *(uint64_t *)(var_60 + 16UL) = *var_62;
            _pre_phi136 = var_62;
            _pre_phi132 = var_61;
            _pre_phi128 = var_58;
            local_sp_1 = local_sp_0;
            var_63 = *(uint64_t *)(local_sp_1 + 16UL);
            *(uint64_t *)var_63 = *_pre_phi128;
            *(uint64_t *)(var_63 + 8UL) = *_pre_phi132;
            *(uint64_t *)(var_63 + 16UL) = *_pre_phi136;
            merge = 0UL;
            return merge;
        }
        if ((*(unsigned char *)(((**(uint64_t **)(var_22 + 16UL) << 4UL) + var_17) + 10UL) & '\x04') != '\x00') {
            var_23 = (uint64_t)((uint16_t)(var_21 >> 8UL) & (unsigned short)1023U);
            var_24 = var_0 + (-168L);
            *(uint64_t *)var_24 = 4236528UL;
            var_25 = indirect_placeholder_52(rdx, rsi, var_23, rdx, rdx);
            merge = var_25;
            local_sp_3 = var_24;
            if ((uint64_t)(uint32_t)var_25 != 0UL) {
                return merge;
            }
            _pre_phi = (var_18 + *_cast) + 8UL;
        }
    }
    local_sp_4_ph = local_sp_3;
    local_sp_7 = local_sp_3;
    if ((*(unsigned char *)_pre_phi & '\b') == '\x00') {
        var_26 = *var_7;
        var_27 = var_9 + var_26;
        rax_1_ph = var_27;
        rsi4_0_ph = var_26;
        if ((long)*(uint64_t *)(var_27 + 8UL) > (long)0UL) {
            var_55 = local_sp_7 + 32UL;
            var_56 = local_sp_7 + (-8L);
            *(uint64_t *)var_56 = 4236445UL;
            var_57 = indirect_placeholder_189(var_55, rdx);
            local_sp_0 = var_56;
            if ((uint64_t)(unsigned char)var_57.field_0 == 0UL) {
                return merge;
            }
        }
        while (1U)
            {
                var_28 = *var_15;
                var_29 = rbp_1_ph + rsi4_0_ph;
                var_30 = (uint64_t *)(var_29 + 8UL);
                local_sp_5 = local_sp_4_ph;
                local_sp_6 = local_sp_4_ph;
                rbp_2 = rbp_1_ph;
                r12_1 = r12_0_ph;
                r14_0 = r14_0_ph;
                rax_1 = rax_1_ph;
                r13_1 = r13_1_ph;
                while (1U)
                    {
                        var_31 = *(uint64_t *)((r14_0 << 3UL) + *(uint64_t *)(rax_1 + 16UL));
                        var_32 = var_31 * 24UL;
                        var_33 = var_32 + var_28;
                        var_34 = (uint64_t *)(var_33 + 8UL);
                        r13_0 = r13_1;
                        rax_1 = var_29;
                        r13_1 = 1UL;
                        switch_state_var = 0;
                        switch (*var_34) {
                          case 18446744073709551615UL:
                            {
                                var_39 = r14_0 + 1UL;
                                r14_0 = var_39;
                                if ((long)*var_30 > (long)var_39) {
                                    continue;
                                }
                                loop_state_var = 2U;
                                switch_state_var = 1;
                                break;
                            }
                            break;
                          case 0UL:
                            {
                                loop_state_var = 0U;
                                switch_state_var = 1;
                                break;
                            }
                            break;
                          default:
                            {
                                loop_state_var = 1U;
                                switch_state_var = 1;
                                break;
                            }
                            break;
                        }
                        if (switch_state_var)
                            break;
                    }
                switch_state_var = 0;
                switch (loop_state_var) {
                  case 0U:
                    {
                        var_35 = local_sp_4_ph + 64UL;
                        *(uint64_t *)(local_sp_4_ph + 8UL) = var_35;
                        var_36 = local_sp_4_ph + (-8L);
                        *(uint64_t *)var_36 = 4236373UL;
                        var_37 = indirect_placeholder_49(0UL, var_35, var_31, rsi);
                        var_38 = *(uint64_t *)local_sp_4_ph;
                        merge = var_37;
                        local_sp_6 = var_36;
                        r10_0 = var_38;
                        if ((uint64_t)(uint32_t)var_37 != 0UL) {
                            loop_state_var = 1U;
                            switch_state_var = 1;
                            break;
                        }
                        var_42 = local_sp_6 + 32UL;
                        var_43 = local_sp_6 + (-8L);
                        *(uint64_t *)var_43 = 4236141UL;
                        var_44 = indirect_placeholder_188(var_32, var_42, rbp_1_ph, r10_0, r12_0_ph);
                        var_45 = var_44.field_0;
                        var_46 = var_44.field_2;
                        var_47 = var_44.field_4;
                        merge = var_45;
                        rbp_2 = var_46;
                        r12_1 = var_47;
                        r12_0_ph = var_47;
                        local_sp_2 = var_43;
                        rbp_1_ph = var_46;
                        if ((uint64_t)(uint32_t)var_45 != 0UL) {
                            loop_state_var = 1U;
                            switch_state_var = 1;
                            break;
                        }
                        if (*(uint64_t *)((var_44.field_1 + *var_15) + 8UL) == 0UL) {
                            var_48 = local_sp_6 + (-16L);
                            *(uint64_t *)var_48 = 4236424UL;
                            indirect_placeholder();
                            local_sp_2 = var_48;
                            r13_0 = 1UL;
                        }
                        var_49 = *var_7;
                        var_50 = r14_0 + 1UL;
                        var_51 = var_46 + var_49;
                        local_sp_5 = local_sp_2;
                        r13_2 = r13_0;
                        rsi4_0_ph = var_49;
                        r14_0_ph = var_50;
                        local_sp_4_ph = local_sp_2;
                        rax_1_ph = var_51;
                        r13_1_ph = r13_0;
                        if ((long)*(uint64_t *)(var_51 + 8UL) > (long)var_50) {
                            continue;
                        }
                        loop_state_var = 0U;
                        switch_state_var = 1;
                        break;
                    }
                    break;
                  case 1U:
                    {
                        var_40 = *(uint64_t *)var_33;
                        var_41 = local_sp_4_ph + 64UL;
                        *(uint64_t *)var_41 = var_40;
                        *(uint64_t *)(local_sp_4_ph + 72UL) = *var_34;
                        *(uint64_t *)(local_sp_4_ph + 80UL) = *(uint64_t *)(var_33 + 16UL);
                        r10_0 = var_41;
                        var_42 = local_sp_6 + 32UL;
                        var_43 = local_sp_6 + (-8L);
                        *(uint64_t *)var_43 = 4236141UL;
                        var_44 = indirect_placeholder_188(var_32, var_42, rbp_1_ph, r10_0, r12_0_ph);
                        var_45 = var_44.field_0;
                        var_46 = var_44.field_2;
                        var_47 = var_44.field_4;
                        merge = var_45;
                        rbp_2 = var_46;
                        r12_1 = var_47;
                        r12_0_ph = var_47;
                        local_sp_2 = var_43;
                        rbp_1_ph = var_46;
                        if ((uint64_t)(uint32_t)var_45 == 0UL) {
                            loop_state_var = 1U;
                            switch_state_var = 1;
                            break;
                        }
                        if (*(uint64_t *)((var_44.field_1 + *var_15) + 8UL) == 0UL) {
                            var_48 = local_sp_6 + (-16L);
                            *(uint64_t *)var_48 = 4236424UL;
                            indirect_placeholder();
                            local_sp_2 = var_48;
                            r13_0 = 1UL;
                        }
                        var_49 = *var_7;
                        var_50 = r14_0 + 1UL;
                        var_51 = var_46 + var_49;
                        local_sp_5 = local_sp_2;
                        r13_2 = r13_0;
                        rsi4_0_ph = var_49;
                        r14_0_ph = var_50;
                        local_sp_4_ph = local_sp_2;
                        rax_1_ph = var_51;
                        r13_1_ph = r13_0;
                        if ((long)*(uint64_t *)(var_51 + 8UL) > (long)var_50) {
                            continue;
                        }
                        loop_state_var = 0U;
                        switch_state_var = 1;
                        break;
                    }
                    break;
                  case 2U:
                    {
                        loop_state_var = 0U;
                        switch_state_var = 1;
                        break;
                    }
                    break;
                }
                if (switch_state_var)
                    break;
            }
        switch (loop_state_var) {
          case 1U:
            {
                return merge;
            }
            break;
          case 0U:
            {
                var_52 = local_sp_5 + 32UL;
                var_53 = local_sp_5 + (-8L);
                *(uint64_t *)var_53 = 4236249UL;
                var_54 = indirect_placeholder_187(var_52, r12_1);
                local_sp_0 = var_53;
                rbp_0 = rbp_2;
                local_sp_1 = var_53;
                if ((uint64_t)(unsigned char)var_54.field_0 == 0UL) {
                    return merge;
                }
                if ((uint64_t)(unsigned char)r13_2 <= (uint64_t)*(unsigned char *)(local_sp_5 + 20UL)) {
                    *(uint64_t *)((rbp_2 + *var_15) + 8UL) = 0UL;
                    _pre127 = (uint64_t *)(var_53 + 32UL);
                    _pre131 = (uint64_t *)(var_53 + 40UL);
                    _pre135 = (uint64_t *)(var_53 + 48UL);
                    _pre_phi136 = _pre135;
                    _pre_phi132 = _pre131;
                    _pre_phi128 = _pre127;
                    var_63 = *(uint64_t *)(local_sp_1 + 16UL);
                    *(uint64_t *)var_63 = *_pre_phi128;
                    *(uint64_t *)(var_63 + 8UL) = *_pre_phi132;
                    *(uint64_t *)(var_63 + 16UL) = *_pre_phi136;
                    merge = 0UL;
                    return merge;
                }
            }
            break;
        }
    }
    var_55 = local_sp_7 + 32UL;
    var_56 = local_sp_7 + (-8L);
    *(uint64_t *)var_56 = 4236445UL;
    var_57 = indirect_placeholder_189(var_55, rdx);
    local_sp_0 = var_56;
    if ((uint64_t)(unsigned char)var_57.field_0 == 0UL) {
        return merge;
    }
    var_58 = (uint64_t *)(local_sp_0 + 32UL);
    var_59 = *var_58;
    var_60 = rbp_0 + *var_15;
    *(uint64_t *)var_60 = var_59;
    var_61 = (uint64_t *)(local_sp_0 + 40UL);
    *(uint64_t *)(var_60 + 8UL) = *var_61;
    var_62 = (uint64_t *)(local_sp_0 + 48UL);
    *(uint64_t *)(var_60 + 16UL) = *var_62;
    _pre_phi136 = var_62;
    _pre_phi132 = var_61;
    _pre_phi128 = var_58;
    local_sp_1 = local_sp_0;
    var_63 = *(uint64_t *)(local_sp_1 + 16UL);
    *(uint64_t *)var_63 = *_pre_phi128;
    *(uint64_t *)(var_63 + 8UL) = *_pre_phi132;
    *(uint64_t *)(var_63 + 16UL) = *_pre_phi136;
    merge = 0UL;
}

typedef unsigned char __u_char;
typedef unsigned short __u_short;
typedef unsigned int __u_int;
typedef unsigned long __u_long;
typedef signed char __int8_t;
typedef unsigned char __uint8_t;
typedef short __int16_t;
typedef unsigned short __uint16_t;
typedef int __int32_t;
typedef unsigned int __uint32_t;
typedef long __int64_t;
typedef unsigned long __uint64_t;
typedef __int8_t __int_least8_t;
typedef __uint8_t __uint_least8_t;
typedef __int16_t __int_least16_t;
typedef __uint16_t __uint_least16_t;
typedef __int32_t __int_least32_t;
typedef __uint32_t __uint_least32_t;
typedef __int64_t __int_least64_t;
typedef __uint64_t __uint_least64_t;
typedef long __quad_t;
typedef unsigned long __u_quad_t;
typedef long __intmax_t;
typedef unsigned long __uintmax_t;
typedef unsigned long __dev_t;
typedef unsigned int __uid_t;
typedef unsigned int __gid_t;
typedef unsigned long __ino_t;
typedef unsigned long __ino64_t;
typedef unsigned int __mode_t;
typedef unsigned long __nlink_t;
typedef long __off_t;
typedef long __off64_t;
typedef int __pid_t;
typedef struct {
    int __val[2];
} __fsid_t;
typedef long __clock_t;
typedef unsigned long __rlim_t;
typedef unsigned long __rlim64_t;
typedef unsigned int __id_t;
typedef long __time_t;
typedef unsigned int __useconds_t;
typedef long __suseconds_t;
typedef int __daddr_t;
typedef int __key_t;
typedef int __clockid_t;
typedef void *__timer_t;
typedef long __blksize_t;
typedef long __blkcnt_t;
typedef long __blkcnt64_t;
typedef unsigned long __fsblkcnt_t;
typedef unsigned long __fsblkcnt64_t;
typedef unsigned long __fsfilcnt_t;
typedef unsigned long __fsfilcnt64_t;
typedef long __fsword_t;
typedef long __ssize_t;
typedef long __syscall_slong_t;
typedef unsigned long __syscall_ulong_t;
typedef __off64_t __loff_t;
typedef char *__caddr_t;
typedef long __intptr_t;
typedef unsigned int __socklen_t;
typedef int __sig_atomic_t;
typedef __int8_t int8_t;
typedef __int16_t int16_t;
typedef __int32_t int32_t;
typedef __int64_t int64_t;
typedef __uint8_t uint8_t;
typedef __uint16_t uint16_t;
typedef __uint32_t uint32_t;
typedef __uint64_t uint64_t;
typedef __int_least8_t int_least8_t;
typedef __int_least16_t int_least16_t;
typedef __int_least32_t int_least32_t;
typedef __int_least64_t int_least64_t;
typedef __uint_least8_t uint_least8_t;
typedef __uint_least16_t uint_least16_t;
typedef __uint_least32_t uint_least32_t;
typedef __uint_least64_t uint_least64_t;
typedef signed char int_fast8_t;
typedef long int_fast16_t;
typedef long int_fast32_t;
typedef long int_fast64_t;
typedef unsigned char uint_fast8_t;
typedef unsigned long uint_fast16_t;
typedef unsigned long uint_fast32_t;
typedef unsigned long uint_fast64_t;
typedef long intptr_t;
typedef unsigned long uintptr_t;
typedef __intmax_t intmax_t;
typedef __uintmax_t uintmax_t;
typedef _Bool bool;
struct indirect_placeholder_197_ret_type;
struct indirect_placeholder_198_ret_type;
struct indirect_placeholder_197_ret_type {
    uint64_t field_0;
    uint64_t field_1;
};
struct indirect_placeholder_198_ret_type {
    uint64_t field_0;
    uint64_t field_1;
};
extern void abort(void);
extern uint64_t revng_init_local_sp(uint64_t param_0);
extern uint64_t init_rbx(void);
extern uint64_t init_rbp(void);
extern uint64_t init_r12(void);
extern void indirect_placeholder(void);
extern uint64_t init_cc_src2(void);
extern uint64_t init_r13(void);
extern uint64_t init_r14(void);
extern uint64_t helper_cc_compute_all_wrapper(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint32_t param_3);
extern struct indirect_placeholder_197_ret_type indirect_placeholder_197(uint64_t param_0);
extern struct indirect_placeholder_198_ret_type indirect_placeholder_198(uint64_t param_0);
uint64_t bb_link_nfa_nodes(uint64_t rdi, uint64_t rsi) {
    uint64_t var_0;
    uint64_t var_1;
    uint64_t var_2;
    uint64_t var_3;
    uint64_t var_4;
    uint64_t var_5;
    uint64_t var_6;
    uint64_t var_7;
    unsigned char *var_8;
    unsigned char var_9;
    uint64_t var_10;
    uint64_t var_11;
    uint64_t var_24;
    bool var_25;
    uint64_t *var_26;
    uint64_t var_27;
    bool var_28;
    uint64_t *var_29;
    uint64_t rax_0;
    uint64_t storemerge2;
    uint64_t var_12;
    uint64_t r13_0;
    uint64_t var_13;
    unsigned char *var_14;
    bool var_15;
    uint64_t var_16;
    uint64_t var_17;
    uint64_t var_18;
    uint64_t storemerge;
    uint64_t local_sp_0;
    uint64_t local_sp_1;
    uint64_t var_21;
    struct indirect_placeholder_197_ret_type var_22;
    uint64_t var_23;
    uint64_t var_20;
    uint64_t var_19;
    uint64_t var_30;
    uint64_t var_31;
    uint64_t *_cast;
    uint64_t *var_32;
    struct indirect_placeholder_198_ret_type var_33;
    uint64_t var_34;
    var_0 = revng_init_local_sp(0UL);
    var_1 = init_rbx();
    var_2 = init_rbp();
    var_3 = init_r13();
    var_4 = init_cc_src2();
    var_5 = init_r12();
    var_6 = init_r14();
    *(uint64_t *)(var_0 + (-8L)) = var_6;
    *(uint64_t *)(var_0 + (-16L)) = var_3;
    *(uint64_t *)(var_0 + (-24L)) = var_5;
    *(uint64_t *)(var_0 + (-32L)) = var_2;
    var_7 = var_0 + (-40L);
    *(uint64_t *)var_7 = var_1;
    var_8 = (unsigned char *)(rsi + 48UL);
    var_9 = *var_8;
    var_10 = (uint64_t)var_9;
    var_11 = *(uint64_t *)(rsi + 56UL);
    local_sp_0 = var_7;
    rax_0 = 0UL;
    if (var_9 <= '\x10') {
        if ((var_10 & 8UL) != 0UL) {
            *(uint64_t *)(var_0 + (-48L)) = 4232665UL;
            indirect_placeholder();
        }
        *(uint64_t *)((var_11 << 3UL) + *(uint64_t *)(rdi + 24UL)) = *(uint64_t *)(*(uint64_t *)(rsi + 32UL) + 56UL);
        return 0UL;
    }
    switch (*(uint64_t *)((var_10 << 3UL) + 4321408UL)) {
      case 4232520UL:
        {
            if (*(uint64_t *)(rsi + 32UL) == 0UL) {
                return rax_0;
            }
            *(uint64_t *)(var_0 + (-48L)) = 4232556UL;
            indirect_placeholder();
            return 0UL;
        }
        break;
      case 4232608UL:
        {
            if ((var_10 & 8UL) != 0UL) {
                *(uint64_t *)(var_0 + (-48L)) = 4232665UL;
                indirect_placeholder();
            }
            *(uint64_t *)((var_11 << 3UL) + *(uint64_t *)(rdi + 24UL)) = *(uint64_t *)(*(uint64_t *)(rsi + 32UL) + 56UL);
            return 0UL;
        }
        break;
      case 4232576UL:
      case 4232368UL:
      case 4232351UL:
      case 4232288UL:
        {
            switch (*(uint64_t *)((var_10 << 3UL) + 4321408UL)) {
              case 4232351UL:
                {
                    return rax_0;
                }
                break;
              case 4232368UL:
                {
                    var_13 = *(uint64_t *)(rsi + 8UL);
                    var_14 = (unsigned char *)(rdi + 176UL);
                    *var_14 = (*var_14 | '\x01');
                    var_15 = (var_13 == 0UL);
                    var_16 = var_13 + 24UL;
                    var_17 = rsi + 32UL;
                    storemerge2 = *(uint64_t *)(*(uint64_t *)(var_15 ? var_17 : var_16) + 56UL);
                    var_18 = *(uint64_t *)(rsi + 16UL);
                    storemerge = *(uint64_t *)(*(uint64_t *)((var_18 == 0UL) ? var_17 : (var_18 + 24UL)) + 56UL);
                    rax_0 = 12UL;
                    if ((long)storemerge2 < (long)0UL) {
                        var_19 = var_0 + (-48L);
                        *(uint64_t *)var_19 = 4232801UL;
                        indirect_placeholder();
                        local_sp_0 = var_19;
                    }
                    local_sp_1 = local_sp_0;
                    if ((long)storemerge < (long)0UL) {
                        var_20 = local_sp_0 + (-8L);
                        *(uint64_t *)var_20 = 4232713UL;
                        indirect_placeholder();
                        local_sp_1 = var_20;
                    }
                    var_21 = (var_11 * 24UL) + *(uint64_t *)(rdi + 40UL);
                    *(uint64_t *)var_21 = 2UL;
                    *(uint64_t *)(local_sp_1 + (-8L)) = 4232466UL;
                    var_22 = indirect_placeholder_197(16UL);
                    var_23 = var_22.field_0;
                    *(uint64_t *)(var_21 + 16UL) = var_23;
                    if (var_23 != 0UL) {
                        var_24 = storemerge2 - storemerge;
                        var_25 = (var_24 == 0UL);
                        var_26 = (uint64_t *)(var_21 + 8UL);
                        rax_0 = 0UL;
                        if (var_25) {
                            *var_26 = 1UL;
                            *(uint64_t *)var_23 = storemerge2;
                        } else {
                            *var_26 = 2UL;
                            var_27 = helper_cc_compute_all_wrapper(var_24, storemerge, var_4, 17U);
                            var_28 = ((signed char)((unsigned char)(var_27 >> 4UL) ^ (unsigned char)var_27) < '\x00');
                            var_29 = (uint64_t *)var_23;
                            if (var_28) {
                                *var_29 = storemerge2;
                                *(uint64_t *)(var_23 + 8UL) = storemerge;
                            } else {
                                *var_29 = storemerge;
                                *(uint64_t *)(var_23 + 8UL) = storemerge2;
                            }
                        }
                    }
                }
                break;
              case 4232288UL:
                {
                    var_30 = *(uint64_t *)(*(uint64_t *)(rsi + 32UL) + 56UL);
                    r13_0 = var_30;
                    var_31 = (var_11 * 24UL) + *(uint64_t *)(rdi + 40UL);
                    _cast = (uint64_t *)var_31;
                    *_cast = 1UL;
                    var_32 = (uint64_t *)(var_31 + 8UL);
                    *var_32 = 1UL;
                    *(uint64_t *)(var_0 + (-48L)) = 4232335UL;
                    var_33 = indirect_placeholder_198(8UL);
                    var_34 = var_33.field_0;
                    *(uint64_t *)(var_31 + 16UL) = var_34;
                    rax_0 = 12UL;
                    if (var_34 == 0UL) {
                        *var_32 = 0UL;
                        *_cast = 0UL;
                    } else {
                        *(uint64_t *)var_34 = r13_0;
                        rax_0 = 0UL;
                    }
                }
                break;
              case 4232576UL:
                {
                    var_12 = *(uint64_t *)(*(uint64_t *)(rsi + 32UL) + 56UL);
                    *(uint64_t *)((var_11 << 3UL) + *(uint64_t *)(rdi + 24UL)) = var_12;
                    r13_0 = var_12;
                    if (*var_8 == '\x04') {
                        return rax_0;
                    }
                    var_31 = (var_11 * 24UL) + *(uint64_t *)(rdi + 40UL);
                    _cast = (uint64_t *)var_31;
                    *_cast = 1UL;
                    var_32 = (uint64_t *)(var_31 + 8UL);
                    *var_32 = 1UL;
                    *(uint64_t *)(var_0 + (-48L)) = 4232335UL;
                    var_33 = indirect_placeholder_198(8UL);
                    var_34 = var_33.field_0;
                    *(uint64_t *)(var_31 + 16UL) = var_34;
                    rax_0 = 12UL;
                    if (var_34 == 0UL) {
                        *(uint64_t *)var_34 = r13_0;
                        rax_0 = 0UL;
                    } else {
                        *var_32 = 0UL;
                        *_cast = 0UL;
                    }
                }
                break;
            }
        }
        break;
      default:
        {
            abort();
        }
        break;
    }
}

typedef unsigned char __u_char;
typedef unsigned short __u_short;
typedef unsigned int __u_int;
typedef unsigned long __u_long;
typedef signed char __int8_t;
typedef unsigned char __uint8_t;
typedef short __int16_t;
typedef unsigned short __uint16_t;
typedef int __int32_t;
typedef unsigned int __uint32_t;
typedef long __int64_t;
typedef unsigned long __uint64_t;
typedef __int8_t __int_least8_t;
typedef __uint8_t __uint_least8_t;
typedef __int16_t __int_least16_t;
typedef __uint16_t __uint_least16_t;
typedef __int32_t __int_least32_t;
typedef __uint32_t __uint_least32_t;
typedef __int64_t __int_least64_t;
typedef __uint64_t __uint_least64_t;
typedef long __quad_t;
typedef unsigned long __u_quad_t;
typedef long __intmax_t;
typedef unsigned long __uintmax_t;
typedef unsigned long __dev_t;
typedef unsigned int __uid_t;
typedef unsigned int __gid_t;
typedef unsigned long __ino_t;
typedef unsigned long __ino64_t;
typedef unsigned int __mode_t;
typedef unsigned long __nlink_t;
typedef long __off_t;
typedef long __off64_t;
typedef int __pid_t;
typedef struct {
    int __val[2];
} __fsid_t;
typedef long __clock_t;
typedef unsigned long __rlim_t;
typedef unsigned long __rlim64_t;
typedef unsigned int __id_t;
typedef long __time_t;
typedef unsigned int __useconds_t;
typedef long __suseconds_t;
typedef int __daddr_t;
typedef int __key_t;
typedef int __clockid_t;
typedef void *__timer_t;
typedef long __blksize_t;
typedef long __blkcnt_t;
typedef long __blkcnt64_t;
typedef unsigned long __fsblkcnt_t;
typedef unsigned long __fsblkcnt64_t;
typedef unsigned long __fsfilcnt_t;
typedef unsigned long __fsfilcnt64_t;
typedef long __fsword_t;
typedef long __ssize_t;
typedef long __syscall_slong_t;
typedef unsigned long __syscall_ulong_t;
typedef __off64_t __loff_t;
typedef char *__caddr_t;
typedef long __intptr_t;
typedef unsigned int __socklen_t;
typedef int __sig_atomic_t;
typedef __int8_t int8_t;
typedef __int16_t int16_t;
typedef __int32_t int32_t;
typedef __int64_t int64_t;
typedef __uint8_t uint8_t;
typedef __uint16_t uint16_t;
typedef __uint32_t uint32_t;
typedef __uint64_t uint64_t;
typedef __int_least8_t int_least8_t;
typedef __int_least16_t int_least16_t;
typedef __int_least32_t int_least32_t;
typedef __int_least64_t int_least64_t;
typedef __uint_least8_t uint_least8_t;
typedef __uint_least16_t uint_least16_t;
typedef __uint_least32_t uint_least32_t;
typedef __uint_least64_t uint_least64_t;
typedef signed char int_fast8_t;
typedef long int_fast16_t;
typedef long int_fast32_t;
typedef long int_fast64_t;
typedef unsigned char uint_fast8_t;
typedef unsigned long uint_fast16_t;
typedef unsigned long uint_fast32_t;
typedef unsigned long uint_fast64_t;
typedef long intptr_t;
typedef unsigned long uintptr_t;
typedef __intmax_t intmax_t;
typedef __uintmax_t uintmax_t;
extern uint64_t revng_init_local_sp(uint64_t param_0);
extern uint64_t init_rbx(void);
extern uint64_t init_rbp(void);
extern uint64_t init_r12(void);
extern uint64_t init_r13(void);
extern uint64_t indirect_placeholder_7(uint64_t param_0, uint64_t param_1, uint64_t param_2);
uint64_t bb_parse_bracket_element_constprop_38(uint64_t rcx, uint64_t r9, uint64_t rdi, uint64_t r8, uint64_t rdx, uint64_t rsi) {
    uint64_t var_0;
    uint64_t var_1;
    uint64_t var_2;
    uint64_t var_3;
    uint64_t var_4;
    uint64_t var_5;
    uint32_t *var_6;
    uint32_t var_7;
    uint64_t *var_8;
    uint64_t var_9;
    uint64_t var_10;
    uint64_t var_11;
    uint64_t var_12;
    uint64_t var_13;
    uint64_t var_14;
    uint64_t rax_0;
    uint64_t var_15;
    uint32_t var_16;
    uint64_t var_17;
    unsigned char *var_18;
    unsigned char var_19;
    uint64_t var_20;
    uint64_t rax_1;
    uint64_t *var_21;
    uint64_t var_22;
    unsigned char var_23;
    unsigned char *var_24;
    unsigned char *var_25;
    uint64_t *var_26;
    uint64_t *var_27;
    uint64_t *var_28;
    uint64_t *var_29;
    uint64_t *var_30;
    uint64_t *var_31;
    uint64_t *var_32;
    uint64_t rax_3;
    uint64_t rcx1_0;
    uint64_t r84_0;
    uint64_t rdx5_0;
    unsigned char rdi3_0_in;
    uint64_t var_33;
    unsigned char var_34;
    uint32_t var_35;
    uint64_t var_36;
    uint64_t rcx1_1;
    uint64_t var_37;
    uint64_t rax_2;
    uint64_t var_38;
    uint64_t rcx1_2;
    uint64_t var_39;
    uint64_t var_40;
    uint64_t var_41;
    unsigned char var_42;
    uint64_t var_43;
    uint64_t var_44;
    uint64_t var_45;
    unsigned char var_46;
    var_0 = revng_init_local_sp(0UL);
    var_1 = init_rbx();
    var_2 = init_rbp();
    var_3 = init_r13();
    var_4 = init_r12();
    *(uint64_t *)(var_0 + (-8L)) = var_3;
    *(uint64_t *)(var_0 + (-16L)) = var_4;
    *(uint64_t *)(var_0 + (-24L)) = var_2;
    *(uint64_t *)(var_0 + (-32L)) = var_1;
    var_5 = var_0 + (-56L);
    var_6 = (uint32_t *)(rsi + 144UL);
    var_7 = *var_6;
    var_8 = (uint64_t *)(rsi + 72UL);
    var_9 = *var_8;
    rax_0 = 2UL;
    rdx5_0 = 0UL;
    rax_3 = 0UL;
    if (var_7 != 1U) {
        var_10 = *(uint64_t *)(rsi + 48UL);
        var_11 = var_9 + 1UL;
        var_12 = (var_11 << 2UL) + *(uint64_t *)(rsi + 16UL);
        if ((long)var_10 <= (long)var_11 & *(uint32_t *)var_12 != 4294967295U) {
            var_13 = var_10 - var_9;
            var_14 = var_12 + (-4L);
            while (rax_0 != var_13)
                {
                    var_15 = rax_0 + 1UL;
                    rax_0 = var_15;
                    if (*(uint32_t *)(((var_15 << 2UL) + var_14) + (-4L)) == 4294967295U) {
                        break;
                    }
                }
            if ((uint64_t)((uint32_t)rax_0 + (-1)) != 0UL) {
                var_16 = *(uint32_t *)var_14;
                *(uint32_t *)rdi = 1U;
                *(uint32_t *)(rdi + 8UL) = var_16;
                *var_8 = (*var_8 + rax_0);
                return rax_3;
            }
        }
    }
    var_17 = (uint64_t)((long)(rcx << 32UL) >> (long)32UL) + var_9;
    *var_8 = var_17;
    var_18 = (unsigned char *)(rdx + 8UL);
    var_19 = *var_18;
    var_20 = (uint64_t)var_19;
    rax_1 = var_17;
    rcx1_0 = var_20;
    rax_3 = 7UL;
    if ((uint64_t)((var_19 & '\xfb') + '\xe6') != 0UL) {
        rax_3 = 0UL;
        if ((uint64_t)(var_19 + '\xe4') != 0UL) {
            if ((uint64_t)(unsigned char)r9 != 0UL) {
                rax_3 = 11UL;
                *(uint64_t *)(var_0 + (-64L)) = 4267596UL;
                indirect_placeholder_7(var_5, r8, rsi);
                if ((uint64_t)(var_19 + '\xea') != 0UL & *(unsigned char *)var_5 == '\x15') {
                    return rax_3;
                }
            }
            *(uint32_t *)rdi = 0U;
            *(unsigned char *)(rdi + 8UL) = *(unsigned char *)rdx;
            return rax_3;
        }
    }
    var_21 = (uint64_t *)(rsi + 104UL);
    var_22 = *var_21;
    var_23 = *(unsigned char *)rdx;
    r84_0 = var_22;
    if ((long)var_17 < (long)var_22) {
        return rax_3;
    }
    var_24 = (unsigned char *)(rsi + 139UL);
    var_25 = (unsigned char *)(rsi + 140UL);
    var_26 = (uint64_t *)(rsi + 48UL);
    var_27 = (uint64_t *)(rsi + 16UL);
    var_28 = (uint64_t *)(rsi + 24UL);
    var_29 = (uint64_t *)rsi;
    var_30 = (uint64_t *)(rsi + 40UL);
    var_31 = (uint64_t *)(rsi + 8UL);
    var_32 = (uint64_t *)(rdi + 8UL);
    while (1U)
        {
            if ((uint64_t)((unsigned char)rcx1_0 + '\xe2') != 0UL) {
                var_43 = *var_31;
                var_44 = rax_1 + 1UL;
                *var_8 = var_44;
                rcx1_2 = var_44;
                rdi3_0_in = *(unsigned char *)(rax_1 + var_43);
                if ((long)r84_0 > (long)rcx1_2) {
                    break;
                }
                rax_3 = 0UL;
                if ((uint64_t)(var_23 - rdi3_0_in) == 0UL) {
                    *(unsigned char *)(rdx5_0 + *var_32) = rdi3_0_in;
                    rax_3 = 7UL;
                    if (rdx5_0 == 31UL) {
                        break;
                    }
                    rax_1 = *var_8;
                    rcx1_0 = (uint64_t)*var_18;
                    r84_0 = *var_21;
                    rdx5_0 = rdx5_0 + 1UL;
                    continue;
                }
                if (*(unsigned char *)(rcx1_2 + *var_31) != ']') {
                    var_45 = (uint64_t)((long)(rdx5_0 << 32UL) >> (long)32UL);
                    *var_8 = (rcx1_2 + 1UL);
                    *(unsigned char *)(var_45 + *var_32) = (unsigned char)'\x00';
                    var_46 = *var_18;
                    if ((uint64_t)(var_46 + '\xe4') == 0UL) {
                        *(uint32_t *)rdi = 2U;
                        break;
                    }
                    if ((uint64_t)(var_46 + '\xe2') == 0UL) {
                        *(uint32_t *)rdi = 4U;
                        break;
                    }
                    if ((uint64_t)(var_46 + '\xe6') == 0UL) {
                        break;
                    }
                    *(uint32_t *)rdi = 3U;
                    break;
                }
            }
            if (*var_24 == '\x00') {
                if (*var_25 == '\x00') {
                    var_39 = rax_1 + 1UL;
                    var_40 = rax_1 + *var_29;
                    var_41 = *var_30;
                    *var_8 = var_39;
                    var_42 = *(unsigned char *)(var_41 + var_40);
                    rcx1_2 = var_39;
                    rdi3_0_in = var_42;
                } else {
                    var_33 = *var_26;
                    if (var_33 != rax_1) {
                        if (*(uint32_t *)((rax_1 << 2UL) + *var_27) != 4294967295U) {
                            var_43 = *var_31;
                            var_44 = rax_1 + 1UL;
                            *var_8 = var_44;
                            rcx1_2 = var_44;
                            rdi3_0_in = *(unsigned char *)(rax_1 + var_43);
                            if ((long)r84_0 > (long)rcx1_2) {
                                break;
                            }
                            rax_3 = 0UL;
                            if ((uint64_t)(var_23 - rdi3_0_in) == 0UL) {
                                *(unsigned char *)(rdx5_0 + *var_32) = rdi3_0_in;
                                rax_3 = 7UL;
                                if (rdx5_0 == 31UL) {
                                    break;
                                }
                                rax_1 = *var_8;
                                rcx1_0 = (uint64_t)*var_18;
                                r84_0 = *var_21;
                                rdx5_0 = rdx5_0 + 1UL;
                                continue;
                            }
                            if (*(unsigned char *)(rcx1_2 + *var_31) != ']') {
                                var_45 = (uint64_t)((long)(rdx5_0 << 32UL) >> (long)32UL);
                                *var_8 = (rcx1_2 + 1UL);
                                *(unsigned char *)(var_45 + *var_32) = (unsigned char)'\x00';
                                var_46 = *var_18;
                                if ((uint64_t)(var_46 + '\xe4') == 0UL) {
                                    *(uint32_t *)rdi = 2U;
                                    break;
                                }
                                if ((uint64_t)(var_46 + '\xe2') == 0UL) {
                                    *(uint32_t *)rdi = 4U;
                                    break;
                                }
                                if ((uint64_t)(var_46 + '\xe6') == 0UL) {
                                    break;
                                }
                                *(uint32_t *)rdi = 3U;
                                break;
                            }
                        }
                    }
                    var_34 = *(unsigned char *)((*(uint64_t *)((rax_1 << 3UL) + *var_28) + *var_29) + *var_30);
                    rdi3_0_in = var_34;
                    if ((signed char)var_34 < '\x00') {
                        var_43 = *var_31;
                        var_44 = rax_1 + 1UL;
                        *var_8 = var_44;
                        rcx1_2 = var_44;
                        rdi3_0_in = *(unsigned char *)(rax_1 + var_43);
                    } else {
                        var_35 = *var_6;
                        var_36 = rax_1 + 1UL;
                        rcx1_1 = var_36;
                        var_37 = *var_27;
                        if (!((var_35 != 1U) && ((long)var_33 > (long)var_36)) & *(uint32_t *)((var_36 << 2UL) + var_37) != 4294967295U) {
                            rax_2 = rax_1 + 2UL;
                            rcx1_1 = var_33;
                            while (rax_2 != var_33)
                                {
                                    var_38 = rax_2 + 1UL;
                                    rax_2 = var_38;
                                    rcx1_1 = rax_2;
                                    if (*(uint32_t *)(((var_38 << 2UL) + var_37) + (-4L)) == 4294967295U) {
                                        break;
                                    }
                                }
                        }
                        *var_8 = rcx1_1;
                        rcx1_2 = rcx1_1;
                    }
                }
            } else {
                var_43 = *var_31;
                var_44 = rax_1 + 1UL;
                *var_8 = var_44;
                rcx1_2 = var_44;
                rdi3_0_in = *(unsigned char *)(rax_1 + var_43);
            }
        }
    return rax_3;
}

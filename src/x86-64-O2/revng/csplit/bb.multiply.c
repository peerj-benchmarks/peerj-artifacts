typedef unsigned char __u_char;
typedef unsigned short __u_short;
typedef unsigned int __u_int;
typedef unsigned long __u_long;
typedef signed char __int8_t;
typedef unsigned char __uint8_t;
typedef short __int16_t;
typedef unsigned short __uint16_t;
typedef int __int32_t;
typedef unsigned int __uint32_t;
typedef long __int64_t;
typedef unsigned long __uint64_t;
typedef __int8_t __int_least8_t;
typedef __uint8_t __uint_least8_t;
typedef __int16_t __int_least16_t;
typedef __uint16_t __uint_least16_t;
typedef __int32_t __int_least32_t;
typedef __uint32_t __uint_least32_t;
typedef __int64_t __int_least64_t;
typedef __uint64_t __uint_least64_t;
typedef long __quad_t;
typedef unsigned long __u_quad_t;
typedef long __intmax_t;
typedef unsigned long __uintmax_t;
typedef unsigned long __dev_t;
typedef unsigned int __uid_t;
typedef unsigned int __gid_t;
typedef unsigned long __ino_t;
typedef unsigned long __ino64_t;
typedef unsigned int __mode_t;
typedef unsigned long __nlink_t;
typedef long __off_t;
typedef long __off64_t;
typedef int __pid_t;
typedef struct {
    int __val[2];
} __fsid_t;
typedef long __clock_t;
typedef unsigned long __rlim_t;
typedef unsigned long __rlim64_t;
typedef unsigned int __id_t;
typedef long __time_t;
typedef unsigned int __useconds_t;
typedef long __suseconds_t;
typedef int __daddr_t;
typedef int __key_t;
typedef int __clockid_t;
typedef void *__timer_t;
typedef long __blksize_t;
typedef long __blkcnt_t;
typedef long __blkcnt64_t;
typedef unsigned long __fsblkcnt_t;
typedef unsigned long __fsblkcnt64_t;
typedef unsigned long __fsfilcnt_t;
typedef unsigned long __fsfilcnt64_t;
typedef long __fsword_t;
typedef long __ssize_t;
typedef long __syscall_slong_t;
typedef unsigned long __syscall_ulong_t;
typedef __off64_t __loff_t;
typedef char *__caddr_t;
typedef long __intptr_t;
typedef unsigned int __socklen_t;
typedef int __sig_atomic_t;
typedef __int8_t int8_t;
typedef __int16_t int16_t;
typedef __int32_t int32_t;
typedef __int64_t int64_t;
typedef __uint8_t uint8_t;
typedef __uint16_t uint16_t;
typedef __uint32_t uint32_t;
typedef __uint64_t uint64_t;
typedef __int_least8_t int_least8_t;
typedef __int_least16_t int_least16_t;
typedef __int_least32_t int_least32_t;
typedef __int_least64_t int_least64_t;
typedef __uint_least8_t uint_least8_t;
typedef __uint_least16_t uint_least16_t;
typedef __uint_least32_t uint_least32_t;
typedef __uint_least64_t uint_least64_t;
typedef signed char int_fast8_t;
typedef long int_fast16_t;
typedef long int_fast32_t;
typedef long int_fast64_t;
typedef unsigned char uint_fast8_t;
typedef unsigned long uint_fast16_t;
typedef unsigned long uint_fast32_t;
typedef unsigned long uint_fast64_t;
typedef long intptr_t;
typedef unsigned long uintptr_t;
typedef __intmax_t intmax_t;
typedef __uintmax_t uintmax_t;
typedef _Bool bool;
struct indirect_placeholder_163_ret_type;
struct indirect_placeholder_164_ret_type;
struct indirect_placeholder_163_ret_type {
    uint64_t field_0;
    uint64_t field_1;
};
struct indirect_placeholder_164_ret_type {
    uint64_t field_0;
    uint64_t field_1;
};
extern uint64_t revng_init_local_sp(uint64_t param_0);
extern uint64_t init_rbx(void);
extern uint64_t init_rbp(void);
extern uint64_t init_r12(void);
extern uint64_t init_r13(void);
extern uint64_t init_r14(void);
extern uint64_t init_r15(void);
extern struct indirect_placeholder_163_ret_type indirect_placeholder_163(uint64_t param_0);
extern struct indirect_placeholder_164_ret_type indirect_placeholder_164(uint64_t param_0);
uint64_t bb_multiply(uint64_t rcx, uint64_t rdi, uint64_t r8, uint64_t rdx, uint64_t rsi) {
    uint64_t var_8;
    uint64_t var_9;
    uint64_t var_10;
    uint64_t var_11;
    uint64_t var_16;
    bool var_17;
    uint64_t rdx4_0;
    uint64_t var_18;
    uint64_t var_19;
    uint64_t var_20;
    uint64_t rcx1_0_be;
    uint64_t rcx1_0;
    uint64_t rdi2_0;
    uint64_t var_21;
    uint64_t r12_1;
    uint64_t r10_0;
    uint64_t rdx4_1;
    uint64_t var_22;
    uint64_t var_23;
    uint32_t *var_24;
    uint64_t var_25;
    uint64_t var_26;
    uint64_t var_27;
    uint64_t var_28;
    uint64_t var_29;
    uint64_t r12_0;
    uint64_t var_30;
    uint64_t var_0;
    uint64_t var_1;
    uint64_t var_2;
    uint64_t var_3;
    uint64_t var_4;
    uint64_t var_5;
    uint64_t var_6;
    bool var_7;
    uint64_t rax_0;
    struct indirect_placeholder_163_ret_type var_31;
    uint64_t var_32;
    uint64_t var_12;
    uint64_t var_13;
    struct indirect_placeholder_164_ret_type var_14;
    uint64_t var_15;
    var_0 = revng_init_local_sp(0UL);
    var_1 = init_rbx();
    var_2 = init_rbp();
    var_3 = init_r13();
    var_4 = init_r12();
    var_5 = init_r15();
    var_6 = init_r14();
    *(uint64_t *)(var_0 + (-8L)) = var_5;
    *(uint64_t *)(var_0 + (-16L)) = var_6;
    *(uint64_t *)(var_0 + (-24L)) = var_3;
    *(uint64_t *)(var_0 + (-32L)) = var_4;
    *(uint64_t *)(var_0 + (-40L)) = var_2;
    *(uint64_t *)(var_0 + (-48L)) = var_1;
    var_7 = (rdi > rdx);
    var_8 = var_7 ? rdx : rdi;
    var_9 = var_7 ? rcx : rsi;
    var_10 = var_7 ? rdi : rdx;
    var_11 = var_7 ? rsi : rcx;
    rdx4_0 = var_10;
    rdi2_0 = var_9;
    r12_1 = 0UL;
    r10_0 = 0UL;
    rdx4_1 = 0UL;
    rax_0 = 0UL;
    if (var_8 != 0UL) {
        var_12 = var_8 + var_10;
        *(uint64_t *)(var_0 + (-64L)) = var_8;
        var_13 = var_12 << 2UL;
        *(uint64_t *)(var_0 + (-80L)) = 4281283UL;
        var_14 = indirect_placeholder_164(var_13);
        var_15 = var_14.field_0;
        rcx1_0 = var_15;
        r12_0 = var_12;
        if (var_15 != 0UL) {
            var_16 = *(uint64_t *)(var_0 + (-72L));
            var_17 = (var_10 == 0UL);
            if (var_17) {
                var_18 = rdx4_0 + (-1L);
                *(uint32_t *)((var_18 << 2UL) + var_15) = 0U;
                rdx4_0 = var_18;
                do {
                    var_18 = rdx4_0 + (-1L);
                    *(uint32_t *)((var_18 << 2UL) + var_15) = 0U;
                    rdx4_0 = var_18;
                } while (var_18 != 0UL);
            }
            var_19 = (var_16 << 2UL) + var_15;
            var_20 = var_10 << 2UL;
            while (1U)
                {
                    var_21 = (uint64_t)*(uint32_t *)rdi2_0;
                    if (var_17) {
                        *(uint32_t *)(var_20 + rcx1_0) = 0U;
                        var_29 = rcx1_0 + 4UL;
                        rcx1_0_be = var_29;
                        if (var_29 == var_19) {
                            break;
                        }
                    }
                    var_22 = r10_0 << 2UL;
                    var_23 = (uint64_t)*(uint32_t *)(var_22 + var_11);
                    var_24 = (uint32_t *)(var_22 + rcx1_0);
                    var_25 = rdx4_1 + ((var_21 * var_23) + (uint64_t)*var_24);
                    *var_24 = (uint32_t)var_25;
                    var_26 = r10_0 + 1UL;
                    var_27 = var_25 >> 32UL;
                    r10_0 = var_26;
                    rdx4_1 = var_27;
                    do {
                        var_22 = r10_0 << 2UL;
                        var_23 = (uint64_t)*(uint32_t *)(var_22 + var_11);
                        var_24 = (uint32_t *)(var_22 + rcx1_0);
                        var_25 = rdx4_1 + ((var_21 * var_23) + (uint64_t)*var_24);
                        *var_24 = (uint32_t)var_25;
                        var_26 = r10_0 + 1UL;
                        var_27 = var_25 >> 32UL;
                        r10_0 = var_26;
                        rdx4_1 = var_27;
                    } while (var_26 != var_10);
                    *(uint32_t *)(var_20 + rcx1_0) = (uint32_t)var_27;
                    var_28 = rcx1_0 + 4UL;
                    rcx1_0_be = var_28;
                    if (var_28 == var_19) {
                        break;
                    }
                    rcx1_0 = rcx1_0_be;
                    rdi2_0 = rdi2_0 + 4UL;
                    continue;
                }
            r12_1 = var_12;
            if (var_12 != 0UL & *(uint32_t *)((var_13 + var_15) + (-4L)) == 0U) {
                var_30 = r12_0 + (-1L);
                r12_0 = var_30;
                r12_1 = 0UL;
                while (var_30 != 0UL)
                    {
                        r12_1 = var_30;
                        if (*(uint32_t *)(((var_30 << 2UL) + var_15) + (-4L)) == 0U) {
                            break;
                        }
                        var_30 = r12_0 + (-1L);
                        r12_0 = var_30;
                        r12_1 = 0UL;
                    }
            }
            *(uint64_t *)r8 = r12_1;
            *(uint64_t *)(r8 + 8UL) = var_15;
            return var_15;
        }
    }
    *(uint64_t *)r8 = 0UL;
    *(uint64_t *)(var_0 + (-80L)) = 4281236UL;
    var_31 = indirect_placeholder_163(1UL);
    var_32 = var_31.field_0;
    *(uint64_t *)(r8 + 8UL) = var_32;
    rax_0 = var_32;
}

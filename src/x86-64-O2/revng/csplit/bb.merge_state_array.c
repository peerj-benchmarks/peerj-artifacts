typedef unsigned char __u_char;
typedef unsigned short __u_short;
typedef unsigned int __u_int;
typedef unsigned long __u_long;
typedef signed char __int8_t;
typedef unsigned char __uint8_t;
typedef short __int16_t;
typedef unsigned short __uint16_t;
typedef int __int32_t;
typedef unsigned int __uint32_t;
typedef long __int64_t;
typedef unsigned long __uint64_t;
typedef __int8_t __int_least8_t;
typedef __uint8_t __uint_least8_t;
typedef __int16_t __int_least16_t;
typedef __uint16_t __uint_least16_t;
typedef __int32_t __int_least32_t;
typedef __uint32_t __uint_least32_t;
typedef __int64_t __int_least64_t;
typedef __uint64_t __uint_least64_t;
typedef long __quad_t;
typedef unsigned long __u_quad_t;
typedef long __intmax_t;
typedef unsigned long __uintmax_t;
typedef unsigned long __dev_t;
typedef unsigned int __uid_t;
typedef unsigned int __gid_t;
typedef unsigned long __ino_t;
typedef unsigned long __ino64_t;
typedef unsigned int __mode_t;
typedef unsigned long __nlink_t;
typedef long __off_t;
typedef long __off64_t;
typedef int __pid_t;
typedef struct {
    int __val[2];
} __fsid_t;
typedef long __clock_t;
typedef unsigned long __rlim_t;
typedef unsigned long __rlim64_t;
typedef unsigned int __id_t;
typedef long __time_t;
typedef unsigned int __useconds_t;
typedef long __suseconds_t;
typedef int __daddr_t;
typedef int __key_t;
typedef int __clockid_t;
typedef void *__timer_t;
typedef long __blksize_t;
typedef long __blkcnt_t;
typedef long __blkcnt64_t;
typedef unsigned long __fsblkcnt_t;
typedef unsigned long __fsblkcnt64_t;
typedef unsigned long __fsfilcnt_t;
typedef unsigned long __fsfilcnt64_t;
typedef long __fsword_t;
typedef long __ssize_t;
typedef long __syscall_slong_t;
typedef unsigned long __syscall_ulong_t;
typedef __off64_t __loff_t;
typedef char *__caddr_t;
typedef long __intptr_t;
typedef unsigned int __socklen_t;
typedef int __sig_atomic_t;
typedef __int8_t int8_t;
typedef __int16_t int16_t;
typedef __int32_t int32_t;
typedef __int64_t int64_t;
typedef __uint8_t uint8_t;
typedef __uint16_t uint16_t;
typedef __uint32_t uint32_t;
typedef __uint64_t uint64_t;
typedef __int_least8_t int_least8_t;
typedef __int_least16_t int_least16_t;
typedef __int_least32_t int_least32_t;
typedef __int_least64_t int_least64_t;
typedef __uint_least8_t uint_least8_t;
typedef __uint_least16_t uint_least16_t;
typedef __uint_least32_t uint_least32_t;
typedef __uint_least64_t uint_least64_t;
typedef signed char int_fast8_t;
typedef long int_fast16_t;
typedef long int_fast32_t;
typedef long int_fast64_t;
typedef unsigned char uint_fast8_t;
typedef unsigned long uint_fast16_t;
typedef unsigned long uint_fast32_t;
typedef unsigned long uint_fast64_t;
typedef long intptr_t;
typedef unsigned long uintptr_t;
typedef __intmax_t intmax_t;
typedef __uintmax_t uintmax_t;
typedef _Bool bool;
struct indirect_placeholder_14_ret_type;
struct indirect_placeholder_15_ret_type;
struct indirect_placeholder_14_ret_type {
    uint64_t field_0;
    uint64_t field_1;
};
struct indirect_placeholder_15_ret_type {
    uint64_t field_0;
    uint64_t field_1;
};
extern uint64_t revng_init_local_sp(uint64_t param_0);
extern uint64_t init_rbx(void);
extern uint64_t init_rbp(void);
extern uint64_t init_r12(void);
extern void indirect_placeholder(void);
extern uint64_t init_r13(void);
extern uint64_t init_r14(void);
extern uint64_t helper_cc_compute_all_wrapper(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint32_t param_3);
extern uint64_t init_r9(void);
extern uint64_t init_r8(void);
extern uint64_t init_r10(void);
extern struct indirect_placeholder_14_ret_type indirect_placeholder_14(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint64_t param_3, uint64_t param_4, uint64_t param_5, uint64_t param_6);
extern struct indirect_placeholder_15_ret_type indirect_placeholder_15(uint64_t param_0, uint64_t param_1, uint64_t param_2);
uint64_t bb_merge_state_array(uint64_t rcx, uint64_t rdi, uint64_t rdx, uint64_t rsi) {
    uint64_t var_0;
    uint64_t var_1;
    uint64_t var_2;
    uint64_t var_3;
    uint64_t var_4;
    uint64_t var_5;
    uint64_t var_6;
    uint64_t var_7;
    uint64_t var_8;
    uint64_t var_9;
    uint64_t rax_0;
    uint64_t var_23;
    uint64_t var_24;
    struct indirect_placeholder_14_ret_type var_25;
    uint64_t var_26;
    uint64_t var_27;
    uint32_t var_28;
    uint64_t r9_1;
    uint64_t rbx_0_ph;
    uint64_t local_sp_1;
    uint64_t r9_0_ph;
    uint64_t local_sp_0_ph;
    uint64_t rbx_0;
    uint64_t var_10;
    uint64_t *var_11;
    uint64_t var_12;
    bool var_13;
    uint64_t var_14;
    uint64_t var_30;
    uint64_t var_15;
    uint64_t var_16;
    uint64_t var_17;
    struct indirect_placeholder_15_ret_type var_18;
    uint64_t var_19;
    uint64_t var_20;
    uint32_t *var_21;
    uint32_t var_22;
    uint64_t var_29;
    unsigned int loop_state_var;
    bool switch_state_var;
    var_0 = revng_init_local_sp(0UL);
    var_1 = init_r10();
    var_2 = init_rbx();
    var_3 = init_r9();
    var_4 = init_rbp();
    var_5 = init_r8();
    var_6 = init_r13();
    var_7 = init_r12();
    var_8 = init_r14();
    *(uint64_t *)(var_0 + (-8L)) = var_8;
    *(uint64_t *)(var_0 + (-16L)) = var_6;
    *(uint64_t *)(var_0 + (-24L)) = var_7;
    *(uint64_t *)(var_0 + (-32L)) = var_4;
    *(uint64_t *)(var_0 + (-40L)) = var_2;
    var_9 = helper_cc_compute_all_wrapper(rcx, 0UL, 0UL, 25U);
    rbx_0_ph = 0UL;
    r9_0_ph = var_3;
    rax_0 = 0UL;
    if ((uint64_t)(((unsigned char)(var_9 >> 4UL) ^ (unsigned char)var_9) & '\xc0') == 0UL) {
        return rax_0;
    }
    local_sp_0_ph = var_0 + (-88L);
    while (1U)
        {
            rbx_0 = rbx_0_ph;
            r9_1 = r9_0_ph;
            local_sp_1 = local_sp_0_ph;
            while (1U)
                {
                    var_10 = rbx_0 << 3UL;
                    var_11 = (uint64_t *)(var_10 + rsi);
                    var_12 = *var_11;
                    var_13 = (var_12 == 0UL);
                    var_14 = *(uint64_t *)(var_10 + rdx);
                    if (!var_13) {
                        loop_state_var = 0U;
                        break;
                    }
                    *var_11 = var_14;
                    var_30 = rbx_0 + 1UL;
                    rbx_0 = var_30;
                    if (var_30 == rcx) {
                        continue;
                    }
                    loop_state_var = 1U;
                    break;
                }
            switch_state_var = 0;
            switch (loop_state_var) {
              case 1U:
                {
                    switch_state_var = 1;
                    break;
                }
                break;
              case 0U:
                {
                    if (var_14 == 0UL) {
                        var_29 = rbx_0 + 1UL;
                        rbx_0_ph = var_29;
                        r9_0_ph = r9_1;
                        local_sp_0_ph = local_sp_1;
                        if (var_29 != rcx) {
                            continue;
                        }
                        switch_state_var = 1;
                        break;
                    }
                    var_15 = var_12 + 8UL;
                    var_16 = local_sp_0_ph + 16UL;
                    var_17 = var_14 + 8UL;
                    *(uint64_t *)(local_sp_0_ph + (-8L)) = 4239028UL;
                    var_18 = indirect_placeholder_15(var_16, var_17, var_15);
                    var_19 = var_18.field_0;
                    var_20 = local_sp_0_ph + 4UL;
                    var_21 = (uint32_t *)var_20;
                    var_22 = (uint32_t)var_19;
                    *var_21 = var_22;
                    rax_0 = var_19;
                    if ((uint64_t)var_22 == 0UL) {
                        switch_state_var = 1;
                        break;
                    }
                    var_23 = var_18.field_1;
                    var_24 = local_sp_0_ph + 8UL;
                    *(uint64_t *)(local_sp_0_ph + (-16L)) = 4239054UL;
                    var_25 = indirect_placeholder_14(var_23, var_1, r9_0_ph, var_20, var_5, var_24, rdi);
                    var_26 = var_25.field_1;
                    *var_11 = var_25.field_0;
                    var_27 = local_sp_0_ph + (-24L);
                    *(uint64_t *)var_27 = 4239069UL;
                    indirect_placeholder();
                    var_28 = *(uint32_t *)(local_sp_0_ph + (-12L));
                    r9_1 = var_26;
                    local_sp_1 = var_27;
                    if (var_28 != 0U) {
                        rax_0 = (uint64_t)var_28;
                        switch_state_var = 1;
                        break;
                    }
                }
                break;
            }
            if (switch_state_var)
                break;
        }
    return rax_0;
}

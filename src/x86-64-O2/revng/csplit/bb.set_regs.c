typedef unsigned char __u_char;
typedef unsigned short __u_short;
typedef unsigned int __u_int;
typedef unsigned long __u_long;
typedef signed char __int8_t;
typedef unsigned char __uint8_t;
typedef short __int16_t;
typedef unsigned short __uint16_t;
typedef int __int32_t;
typedef unsigned int __uint32_t;
typedef long __int64_t;
typedef unsigned long __uint64_t;
typedef __int8_t __int_least8_t;
typedef __uint8_t __uint_least8_t;
typedef __int16_t __int_least16_t;
typedef __uint16_t __uint_least16_t;
typedef __int32_t __int_least32_t;
typedef __uint32_t __uint_least32_t;
typedef __int64_t __int_least64_t;
typedef __uint64_t __uint_least64_t;
typedef long __quad_t;
typedef unsigned long __u_quad_t;
typedef long __intmax_t;
typedef unsigned long __uintmax_t;
typedef unsigned long __dev_t;
typedef unsigned int __uid_t;
typedef unsigned int __gid_t;
typedef unsigned long __ino_t;
typedef unsigned long __ino64_t;
typedef unsigned int __mode_t;
typedef unsigned long __nlink_t;
typedef long __off_t;
typedef long __off64_t;
typedef int __pid_t;
typedef struct {
    int __val[2];
} __fsid_t;
typedef long __clock_t;
typedef unsigned long __rlim_t;
typedef unsigned long __rlim64_t;
typedef unsigned int __id_t;
typedef long __time_t;
typedef unsigned int __useconds_t;
typedef long __suseconds_t;
typedef int __daddr_t;
typedef int __key_t;
typedef int __clockid_t;
typedef void *__timer_t;
typedef long __blksize_t;
typedef long __blkcnt_t;
typedef long __blkcnt64_t;
typedef unsigned long __fsblkcnt_t;
typedef unsigned long __fsblkcnt64_t;
typedef unsigned long __fsfilcnt_t;
typedef unsigned long __fsfilcnt64_t;
typedef long __fsword_t;
typedef long __ssize_t;
typedef long __syscall_slong_t;
typedef unsigned long __syscall_ulong_t;
typedef __off64_t __loff_t;
typedef char *__caddr_t;
typedef long __intptr_t;
typedef unsigned int __socklen_t;
typedef int __sig_atomic_t;
typedef __int8_t int8_t;
typedef __int16_t int16_t;
typedef __int32_t int32_t;
typedef __int64_t int64_t;
typedef __uint8_t uint8_t;
typedef __uint16_t uint16_t;
typedef __uint32_t uint32_t;
typedef __uint64_t uint64_t;
typedef __int_least8_t int_least8_t;
typedef __int_least16_t int_least16_t;
typedef __int_least32_t int_least32_t;
typedef __int_least64_t int_least64_t;
typedef __uint_least8_t uint_least8_t;
typedef __uint_least16_t uint_least16_t;
typedef __uint_least32_t uint_least32_t;
typedef __uint_least64_t uint_least64_t;
typedef signed char int_fast8_t;
typedef long int_fast16_t;
typedef long int_fast32_t;
typedef long int_fast64_t;
typedef unsigned char uint_fast8_t;
typedef unsigned long uint_fast16_t;
typedef unsigned long uint_fast32_t;
typedef unsigned long uint_fast64_t;
typedef long intptr_t;
typedef unsigned long uintptr_t;
typedef __intmax_t intmax_t;
typedef __uintmax_t uintmax_t;
struct indirect_placeholder_82_ret_type;
struct indirect_placeholder_71_ret_type;
struct indirect_placeholder_72_ret_type;
struct indirect_placeholder_73_ret_type;
struct indirect_placeholder_74_ret_type;
struct indirect_placeholder_75_ret_type;
struct indirect_placeholder_76_ret_type;
struct indirect_placeholder_77_ret_type;
struct indirect_placeholder_78_ret_type;
struct indirect_placeholder_79_ret_type;
struct indirect_placeholder_80_ret_type;
struct indirect_placeholder_81_ret_type;
struct indirect_placeholder_83_ret_type;
struct indirect_placeholder_84_ret_type;
struct indirect_placeholder_82_ret_type {
    uint64_t field_0;
    uint64_t field_1;
};
struct indirect_placeholder_71_ret_type {
    uint64_t field_0;
    uint64_t field_1;
};
struct indirect_placeholder_72_ret_type {
    uint64_t field_0;
    uint64_t field_1;
};
struct indirect_placeholder_73_ret_type {
    uint64_t field_0;
    uint64_t field_1;
};
struct indirect_placeholder_74_ret_type {
    uint64_t field_0;
    uint64_t field_1;
};
struct indirect_placeholder_75_ret_type {
    uint64_t field_0;
    uint64_t field_1;
};
struct indirect_placeholder_76_ret_type {
    uint64_t field_0;
    uint64_t field_1;
};
struct indirect_placeholder_77_ret_type {
    uint64_t field_0;
    uint64_t field_1;
};
struct indirect_placeholder_78_ret_type {
    uint64_t field_0;
    uint64_t field_1;
};
struct indirect_placeholder_79_ret_type {
    uint64_t field_0;
    uint64_t field_1;
};
struct indirect_placeholder_80_ret_type {
    uint64_t field_0;
    uint64_t field_1;
};
struct indirect_placeholder_81_ret_type {
    uint64_t field_0;
    uint64_t field_1;
    uint64_t field_2;
    uint64_t field_3;
};
struct indirect_placeholder_83_ret_type {
    uint64_t field_0;
    uint64_t field_1;
};
struct indirect_placeholder_84_ret_type {
    uint64_t field_0;
    uint64_t field_1;
};
extern uint64_t revng_init_local_sp(uint64_t param_0);
extern uint64_t init_rbx(void);
extern uint64_t init_rbp(void);
extern uint64_t init_r12(void);
extern void indirect_placeholder(void);
extern uint64_t init_r13(void);
extern uint64_t init_r14(void);
extern uint64_t helper_cc_compute_all_wrapper(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint32_t param_3);
extern uint64_t init_r15(void);
extern uint64_t indirect_placeholder_53(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint64_t param_3, uint64_t param_4, uint64_t param_5);
extern struct indirect_placeholder_82_ret_type indirect_placeholder_82(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint64_t param_3);
extern void indirect_placeholder_27(uint64_t param_0, uint64_t param_1);
extern struct indirect_placeholder_71_ret_type indirect_placeholder_71(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint64_t param_3);
extern struct indirect_placeholder_72_ret_type indirect_placeholder_72(uint64_t param_0, uint64_t param_1, uint64_t param_2);
extern struct indirect_placeholder_73_ret_type indirect_placeholder_73(uint64_t param_0, uint64_t param_1, uint64_t param_2);
extern struct indirect_placeholder_74_ret_type indirect_placeholder_74(uint64_t param_0);
extern struct indirect_placeholder_75_ret_type indirect_placeholder_75(uint64_t param_0, uint64_t param_1, uint64_t param_2);
extern struct indirect_placeholder_76_ret_type indirect_placeholder_76(uint64_t param_0, uint64_t param_1, uint64_t param_2);
extern struct indirect_placeholder_77_ret_type indirect_placeholder_77(uint64_t param_0, uint64_t param_1);
extern struct indirect_placeholder_78_ret_type indirect_placeholder_78(uint64_t param_0, uint64_t param_1, uint64_t param_2);
extern struct indirect_placeholder_79_ret_type indirect_placeholder_79(uint64_t param_0, uint64_t param_1);
extern struct indirect_placeholder_80_ret_type indirect_placeholder_80(uint64_t param_0, uint64_t param_1, uint64_t param_2);
extern struct indirect_placeholder_81_ret_type indirect_placeholder_81(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint64_t param_3, uint64_t param_4);
extern struct indirect_placeholder_83_ret_type indirect_placeholder_83(uint64_t param_0);
extern struct indirect_placeholder_84_ret_type indirect_placeholder_84(uint64_t param_0);
typedef _Bool bool;
uint64_t bb_set_regs(uint64_t rcx, uint64_t rdi, uint64_t r8, uint64_t rdx, uint64_t rsi) {
    uint64_t *_pre_phi357;
    uint64_t local_sp_16;
    uint64_t rbx_8;
    uint64_t r12_8;
    uint64_t var_172;
    struct indirect_placeholder_81_ret_type var_122;
    uint64_t var_0;
    uint64_t var_1;
    uint64_t var_2;
    uint64_t var_3;
    uint64_t var_4;
    uint64_t var_5;
    uint64_t var_6;
    uint64_t var_7;
    uint64_t var_8;
    uint64_t *var_9;
    uint64_t *var_10;
    uint64_t var_11;
    uint64_t *var_12;
    uint64_t var_13;
    uint64_t var_14;
    uint64_t *var_15;
    uint64_t *var_16;
    uint64_t local_sp_0;
    uint32_t rax_0;
    uint32_t *var_166;
    uint64_t var_167;
    uint64_t rax_5;
    uint64_t *var_105;
    uint64_t local_sp_2;
    uint64_t r12_7;
    uint64_t local_sp_15;
    uint64_t local_sp_1;
    uint64_t var_170;
    struct indirect_placeholder_71_ret_type var_171;
    uint64_t var_169;
    uint64_t var_142;
    uint64_t *rcx1_0_in_pre_phi;
    uint64_t r12_2;
    uint64_t var_133;
    uint64_t var_134;
    uint64_t var_135;
    uint64_t var_136;
    uint64_t var_137;
    uint64_t var_138;
    uint64_t var_139;
    uint64_t rbx_3;
    uint64_t var_140;
    struct indirect_placeholder_72_ret_type var_141;
    uint64_t local_sp_11;
    uint64_t var_157;
    uint64_t var_158;
    uint64_t var_125;
    uint64_t rbx_7;
    uint64_t var_164;
    uint64_t storemerge_in_sroa_speculated;
    uint64_t var_113;
    uint64_t var_114;
    uint64_t var_115;
    uint64_t var_116;
    struct indirect_placeholder_73_ret_type var_117;
    uint64_t local_sp_14;
    uint64_t var_94;
    uint64_t *var_95;
    uint64_t var_96;
    uint64_t var_97;
    uint64_t var_98;
    uint64_t var_99;
    uint64_t rbx_1;
    uint64_t var_106;
    uint64_t var_107;
    uint64_t var_108;
    uint64_t var_109;
    struct indirect_placeholder_74_ret_type var_110;
    uint64_t var_111;
    uint64_t *var_112;
    uint64_t var_65;
    uint64_t var_100;
    uint64_t var_101;
    uint64_t *var_102;
    uint64_t var_103;
    uint64_t var_104;
    uint64_t r12_0;
    uint64_t var_90;
    uint64_t var_91;
    uint64_t var_92;
    struct indirect_placeholder_75_ret_type var_93;
    uint64_t var_85;
    uint64_t rbx_0;
    uint64_t var_118;
    uint64_t var_81;
    uint64_t var_82;
    uint64_t local_sp_13;
    uint64_t var_83;
    uint64_t var_84;
    uint64_t local_sp_3;
    uint64_t local_sp_7;
    uint64_t rsi5_0;
    uint64_t rcx1_0;
    uint64_t var_143;
    uint64_t var_86;
    uint64_t var_87;
    uint64_t var_88;
    struct indirect_placeholder_76_ret_type var_89;
    uint64_t var_146;
    uint64_t var_147;
    uint64_t var_148;
    uint64_t local_sp_9;
    uint64_t *_pre_phi353;
    uint64_t rbx_6;
    uint64_t local_sp_4;
    uint64_t rax_1;
    uint64_t r12_6;
    uint64_t rbx_2;
    uint64_t r12_1;
    uint64_t var_51;
    uint64_t *var_52;
    uint64_t var_53;
    uint64_t *var_54;
    uint64_t var_55;
    uint64_t var_56;
    uint32_t var_57;
    uint64_t local_sp_5;
    uint64_t var_58;
    uint64_t var_59;
    uint64_t var_60;
    uint64_t rax_2;
    uint64_t local_sp_17;
    uint64_t var_61;
    uint64_t var_62;
    uint64_t var_63;
    uint64_t var_64;
    uint64_t rdx4_0;
    uint64_t local_sp_6;
    uint64_t var_66;
    uint64_t var_67;
    uint64_t rax_3;
    uint64_t rdx4_1;
    uint64_t rdx4_2;
    uint64_t var_68;
    uint64_t var_69;
    uint64_t var_70;
    uint64_t var_71;
    uint64_t var_165;
    uint64_t var_72;
    uint64_t var_73;
    uint64_t *_cast;
    uint64_t var_74;
    uint64_t var_75;
    unsigned char var_76;
    uint64_t var_77;
    uint64_t var_78;
    uint64_t var_79;
    struct indirect_placeholder_77_ret_type var_80;
    uint64_t var_144;
    struct indirect_placeholder_78_ret_type var_145;
    uint64_t var_126;
    uint64_t var_127;
    uint64_t var_128;
    uint64_t var_129;
    uint64_t var_130;
    uint64_t local_sp_8;
    uint64_t var_131;
    struct indirect_placeholder_79_ret_type var_132;
    uint64_t r15_0;
    uint64_t rax_4;
    uint64_t rbx_4_in_in;
    uint64_t rbx_4;
    uint64_t var_149;
    uint64_t local_sp_10;
    uint64_t var_150;
    uint64_t var_151;
    uint64_t var_152;
    uint64_t var_153;
    uint64_t var_154;
    struct indirect_placeholder_80_ret_type var_155;
    uint64_t rbx_5;
    uint64_t r12_3;
    uint64_t r12_5;
    uint64_t local_sp_12;
    uint64_t r12_4;
    uint64_t var_156;
    uint64_t var_159;
    uint64_t var_160;
    uint64_t var_161;
    uint64_t var_162;
    uint64_t var_163;
    uint64_t var_119;
    uint64_t var_120;
    uint64_t var_121;
    uint64_t var_123;
    uint64_t var_124;
    uint64_t var_168;
    uint64_t var_29;
    struct indirect_placeholder_83_ret_type var_30;
    uint64_t var_31;
    uint64_t *var_32;
    uint64_t *_pre356;
    uint64_t var_25;
    uint64_t var_26;
    uint64_t var_27;
    uint64_t *var_28;
    uint64_t *var_33;
    uint64_t var_34;
    uint64_t *var_35;
    uint64_t var_36;
    uint64_t var_37;
    uint64_t var_38;
    uint64_t var_39;
    uint64_t *var_40;
    uint64_t *var_41;
    uint64_t var_42;
    uint64_t *var_43;
    uint64_t var_44;
    uint64_t *var_45;
    uint64_t var_46;
    uint64_t var_47;
    uint64_t *var_48;
    uint64_t *var_49;
    uint64_t *var_50;
    uint64_t local_sp_18;
    uint64_t var_17;
    struct indirect_placeholder_84_ret_type var_18;
    uint64_t var_19;
    uint64_t var_20;
    uint64_t var_21;
    uint64_t var_22;
    uint64_t var_23;
    uint64_t *var_24;
    unsigned int loop_state_var;
    bool switch_state_var;
    var_0 = revng_init_local_sp(0UL);
    var_1 = init_rbx();
    var_2 = init_rbp();
    var_3 = init_r13();
    var_4 = init_r12();
    var_5 = init_r15();
    var_6 = init_r14();
    var_7 = var_0 + (-8L);
    *(uint64_t *)var_7 = var_2;
    *(uint64_t *)(var_0 + (-16L)) = var_5;
    *(uint64_t *)(var_0 + (-24L)) = var_6;
    *(uint64_t *)(var_0 + (-32L)) = var_3;
    *(uint64_t *)(var_0 + (-40L)) = var_4;
    *(uint64_t *)(var_0 + (-48L)) = var_1;
    var_8 = *(uint64_t *)rdi;
    var_9 = (uint64_t *)(var_0 + (-160L));
    *var_9 = rsi;
    var_10 = (uint64_t *)(var_0 + (-184L));
    *var_10 = rdx;
    var_11 = var_0 + (-168L);
    var_12 = (uint64_t *)var_11;
    *var_12 = rcx;
    var_13 = var_0 + (-88L);
    *(uint64_t *)var_13 = 0UL;
    var_14 = var_0 + (-192L);
    var_15 = (uint64_t *)var_14;
    *var_15 = var_8;
    *(uint64_t *)(var_0 + (-80L)) = 2UL;
    var_16 = (uint64_t *)(var_0 + (-72L));
    *var_16 = 0UL;
    r12_8 = var_4;
    rax_0 = 1U;
    rax_5 = 12UL;
    rbx_1 = 18446744073709551615UL;
    r12_0 = 0UL;
    r12_1 = var_4;
    rdx4_1 = 0UL;
    if ((uint64_t)(unsigned char)r8 == 0UL) {
        var_20 = var_0 + (-248L);
        *(uint64_t *)(var_0 + (-176L)) = 0UL;
        local_sp_18 = var_20;
    } else {
        var_17 = var_0 + (-256L);
        *(uint64_t *)var_17 = 4251422UL;
        var_18 = indirect_placeholder_84(96UL);
        var_19 = var_18.field_0;
        *var_16 = var_19;
        local_sp_18 = var_17;
        if (var_19 != 0UL) {
            return rax_5;
        }
        *(uint64_t *)(var_0 + (-176L)) = var_13;
    }
    var_21 = *(uint64_t *)(*var_15 + 144UL);
    var_22 = local_sp_18 + (-8L);
    *(uint64_t *)var_22 = 4251484UL;
    indirect_placeholder();
    var_23 = *var_10 << 4UL;
    var_24 = (uint64_t *)(var_0 + (-216L));
    *var_24 = var_23;
    rbx_2 = var_21;
    rbx_8 = var_21;
    rax_5 = 1UL;
    if (var_23 > 4031UL) {
        var_29 = local_sp_18 + (-16L);
        *(uint64_t *)var_29 = 4251526UL;
        var_30 = indirect_placeholder_83(var_23);
        var_31 = var_30.field_0;
        var_32 = (uint64_t *)(var_0 + (-208L));
        *var_32 = var_31;
        *(unsigned char *)(var_0 + (-225L)) = (unsigned char)'\x01';
        local_sp_16 = var_29;
        _pre_phi353 = var_32;
        local_sp_17 = var_29;
        if (var_31 != 0UL) {
            _pre356 = (uint64_t *)(var_0 + (-176L));
            _pre_phi357 = _pre356;
            var_172 = *_pre_phi357;
            *(uint64_t *)(local_sp_16 + (-8L)) = 4252346UL;
            indirect_placeholder_82(rbx_8, var_172, var_7, r12_8);
            return rax_5;
        }
    }
    var_25 = var_23 + 16UL;
    *(unsigned char *)(var_0 + (-225L)) = (unsigned char)'\x00';
    var_26 = var_22 - var_25;
    var_27 = (var_26 + 15UL) & (-16L);
    var_28 = (uint64_t *)(var_0 + (-208L));
    *var_28 = var_27;
    _pre_phi353 = var_28;
    local_sp_17 = var_26;
    var_33 = *(uint64_t **)var_11;
    var_34 = local_sp_17 + (-8L);
    *(uint64_t *)var_34 = 4251578UL;
    indirect_placeholder();
    var_35 = (uint64_t *)(var_0 + (-176L));
    var_36 = *var_35;
    var_37 = *var_33;
    var_38 = var_36 + 16UL;
    var_39 = var_0 + (-128L);
    var_40 = (uint64_t *)var_39;
    *var_40 = var_37;
    var_41 = (uint64_t *)(var_0 + (-200L));
    *var_41 = var_38;
    var_42 = var_0 + (-104L);
    var_43 = (uint64_t *)(var_0 + (-224L));
    *var_43 = var_42;
    var_44 = var_0 + (-112L);
    var_45 = (uint64_t *)(var_0 + (-240L));
    *var_45 = var_44;
    var_46 = var_0 + (-120L);
    var_47 = var_0 + (-144L);
    var_48 = (uint64_t *)var_47;
    var_49 = (uint64_t *)(var_0 + (-152L));
    var_50 = (uint64_t *)var_44;
    local_sp_4 = var_34;
    rax_1 = var_37;
    rcx1_0_in_pre_phi = var_40;
    _pre_phi357 = var_35;
    while (1U)
        {
            var_51 = *var_12;
            var_52 = (uint64_t *)(var_51 + 8UL);
            var_53 = *var_52;
            r12_7 = r12_1;
            local_sp_15 = local_sp_4;
            rbx_3 = rbx_2;
            local_sp_5 = local_sp_4;
            rax_2 = rax_1;
            rdx4_0 = var_53;
            if ((long)var_53 >= (long)rax_1) {
                loop_state_var = 2U;
                break;
            }
            var_54 = *(uint64_t **)var_14;
            var_55 = rbx_2 << 4UL;
            var_56 = var_55 + *var_54;
            var_57 = (uint32_t)(uint64_t)*(unsigned char *)(var_56 + 8UL);
            r12_2 = var_55;
            r12_7 = var_55;
            if ((uint64_t)(var_57 + (-8)) == 0UL) {
                var_63 = *(uint64_t *)var_56 + 1UL;
                if ((long)*var_10 > (long)var_63) {
                    var_64 = (var_63 << 4UL) + var_51;
                    *(uint64_t *)(var_64 + 8UL) = 18446744073709551615UL;
                    *(uint64_t *)var_64 = rax_1;
                    rdx4_0 = *var_52;
                }
            } else {
                var_58 = *(uint64_t *)var_56 + 1UL;
                if ((uint64_t)(var_57 + (-9)) != 0UL & (long)*var_10 <= (long)var_58) {
                    var_59 = var_58 << 4UL;
                    var_60 = var_51 + var_59;
                    if ((long)*(uint64_t *)var_60 < (long)rax_1) {
                        *(uint64_t *)(var_60 + 8UL) = rax_1;
                        var_62 = local_sp_4 + (-8L);
                        *(uint64_t *)var_62 = 4253070UL;
                        indirect_placeholder();
                        local_sp_5 = var_62;
                        rax_2 = *var_40;
                        rdx4_0 = *var_52;
                    } else {
                        if ((*(unsigned char *)(var_56 + 10UL) & '\b') == '\x00') {
                            *(uint64_t *)(var_60 + 8UL) = rax_1;
                            rdx4_0 = *var_52;
                        } else {
                            if (*(uint64_t *)(var_59 + *_pre_phi353) == 18446744073709551615UL) {
                                *(uint64_t *)(var_60 + 8UL) = rax_1;
                                rdx4_0 = *var_52;
                            } else {
                                var_61 = local_sp_4 + (-8L);
                                *(uint64_t *)var_61 = 4253158UL;
                                indirect_placeholder();
                                local_sp_5 = var_61;
                                rax_2 = *var_40;
                                rdx4_0 = *var_52;
                            }
                        }
                    }
                }
            }
            local_sp_6 = local_sp_5;
            local_sp_15 = local_sp_5;
            rax_0 = 0U;
            rax_5 = 0UL;
            if (rdx4_0 != rax_2 & *(uint64_t *)(*var_9 + 176UL) != rbx_2) {
                var_65 = *var_35;
                if (var_65 != 0UL) {
                    var_165 = local_sp_5 + (-8L);
                    *(uint64_t *)var_165 = 4253320UL;
                    indirect_placeholder();
                    local_sp_0 = var_165;
                    if (*(unsigned char *)(var_0 + (-225L)) != '\x00') {
                        loop_state_var = 1U;
                        break;
                    }
                    loop_state_var = 0U;
                    break;
                }
                var_66 = *var_10;
                if (var_66 != 0UL) {
                    loop_state_var = 2U;
                    break;
                }
                var_67 = *var_12;
                rax_3 = var_67;
                while (1U)
                    {
                        rdx4_2 = rdx4_1;
                        if ((long)*(uint64_t *)rax_3 >= (long)0UL) {
                            if (*(uint64_t *)(rax_3 + 8UL) == 18446744073709551615UL) {
                                break;
                            }
                        }
                        var_68 = rdx4_1 + 1UL;
                        rdx4_1 = var_68;
                        rdx4_2 = var_66;
                        if (var_66 == var_68) {
                            break;
                        }
                        rax_3 = rax_3 + 16UL;
                        continue;
                    }
                if (var_66 != rdx4_2) {
                    loop_state_var = 2U;
                    break;
                }
                var_69 = *var_41;
                var_70 = local_sp_5 + (-8L);
                *(uint64_t *)var_70 = 4252944UL;
                var_71 = indirect_placeholder_53(var_66, var_46, var_65, var_67, var_39, var_69);
                local_sp_6 = var_70;
                rbx_3 = var_71;
                r12_2 = var_71 << 4UL;
            }
            var_72 = *var_9;
            var_73 = *(uint64_t *)(var_72 + 152UL);
            _cast = (uint64_t *)var_73;
            var_74 = *_cast;
            var_75 = r12_2 + var_74;
            var_76 = *(unsigned char *)(var_75 + 8UL);
            rbx_7 = rbx_3;
            local_sp_13 = local_sp_6;
            local_sp_7 = local_sp_6;
            rsi5_0 = var_75;
            r12_6 = r12_2;
            local_sp_8 = local_sp_6;
            r12_3 = r12_2;
            r12_5 = r12_2;
            if (((uint64_t)var_76 & 8UL) != 0UL) {
                var_77 = *(uint64_t *)((*var_40 << 3UL) + *(uint64_t *)(var_72 + 184UL));
                var_78 = (rbx_3 * 24UL) + *(uint64_t *)(var_73 + 40UL);
                var_79 = local_sp_6 + (-8L);
                *(uint64_t *)var_79 = 4251989UL;
                var_80 = indirect_placeholder_77(var_46, rbx_3);
                local_sp_14 = var_79;
                local_sp_13 = var_79;
                local_sp_3 = var_79;
                r12_6 = var_77;
                r12_5 = var_77;
                if ((uint64_t)(unsigned char)var_80.field_0 != 0UL) {
                    loop_state_var = 3U;
                    break;
                }
                var_81 = *(uint64_t *)(var_78 + 8UL);
                var_82 = helper_cc_compute_all_wrapper(var_81, 0UL, 0UL, 25U);
                if ((uint64_t)(((unsigned char)(var_82 >> 4UL) ^ (unsigned char)var_82) & '\xc0') == 0UL) {
                    var_156 = *var_35;
                    r12_4 = r12_5;
                    if (var_156 != 0UL) {
                        var_162 = local_sp_13 + (-8L);
                        *(uint64_t *)var_162 = 4253373UL;
                        indirect_placeholder();
                        local_sp_0 = var_162;
                        if (*(unsigned char *)(var_0 + (-225L)) != '\x00') {
                            loop_state_var = 1U;
                            break;
                        }
                        loop_state_var = 0U;
                        break;
                    }
                    var_157 = *var_12;
                    var_158 = *var_10;
                    var_159 = *var_41;
                    var_160 = local_sp_13 + (-8L);
                    *(uint64_t *)var_160 = 4253029UL;
                    var_161 = indirect_placeholder_53(var_158, var_46, var_156, var_157, var_39, var_159);
                    local_sp_12 = var_160;
                    rbx_6 = var_161;
                    local_sp_4 = local_sp_12;
                    rax_1 = *var_40;
                    rbx_2 = rbx_6;
                    r12_1 = r12_4;
                    continue;
                }
                var_83 = *(uint64_t *)(var_78 + 16UL);
                *var_48 = *(uint64_t *)(var_77 + 16UL);
                var_84 = var_77 + 24UL;
                *var_49 = var_84;
                var_85 = var_84;
                while (1U)
                    {
                        var_86 = *(uint64_t *)((r12_0 << 3UL) + var_83);
                        var_87 = *var_48;
                        var_88 = local_sp_3 + (-8L);
                        *(uint64_t *)var_88 = 4252098UL;
                        var_89 = indirect_placeholder_76(var_87, var_86, var_85);
                        local_sp_11 = var_88;
                        rbx_7 = rbx_1;
                        rbx_0 = rbx_1;
                        local_sp_3 = var_88;
                        rbx_5 = var_86;
                        r12_3 = r12_0;
                        if (var_89.field_0 != 0UL) {
                            rbx_0 = var_86;
                            if (rbx_1 != 18446744073709551615UL) {
                                loop_state_var = 0U;
                                break;
                            }
                        }
                        var_118 = r12_0 + 1UL;
                        rbx_1 = rbx_0;
                        r12_0 = var_118;
                        rbx_5 = rbx_0;
                        r12_3 = var_81;
                        if (var_118 != var_81) {
                            loop_state_var = 1U;
                            break;
                        }
                        var_85 = *var_49;
                        continue;
                    }
                switch_state_var = 0;
                switch (loop_state_var) {
                  case 1U:
                    {
                        rbx_7 = 18446744073709551614UL;
                        local_sp_14 = local_sp_11;
                        local_sp_13 = local_sp_11;
                        rbx_6 = rbx_5;
                        r12_6 = r12_3;
                        r12_5 = r12_3;
                        local_sp_12 = local_sp_11;
                        r12_4 = r12_3;
                        if ((long)rbx_5 >= (long)0UL) {
                            if (rbx_5 == 18446744073709551614UL) {
                                loop_state_var = 3U;
                                switch_state_var = 1;
                                break;
                            }
                        }
                        local_sp_4 = local_sp_12;
                        rax_1 = *var_40;
                        rbx_2 = rbx_6;
                        r12_1 = r12_4;
                        continue;
                    }
                    break;
                  case 0U:
                    {
                        var_90 = *var_43;
                        var_91 = *var_50;
                        var_92 = local_sp_3 + (-16L);
                        *(uint64_t *)var_92 = 4252128UL;
                        var_93 = indirect_placeholder_75(var_91, rbx_1, var_90);
                        local_sp_2 = var_92;
                        local_sp_11 = var_92;
                        var_94 = *var_35;
                        rbx_5 = rbx_1;
                        if (var_93.field_0 != 0UL & var_94 != 0UL) {
                            var_95 = (uint64_t *)var_94;
                            var_96 = *var_95;
                            var_97 = *var_40;
                            var_98 = var_96 + 1UL;
                            var_99 = *(uint64_t *)(var_94 + 8UL);
                            *var_95 = var_98;
                            storemerge_in_sroa_speculated = var_98;
                            r12_6 = var_96;
                            if (var_98 == var_99) {
                                storemerge_in_sroa_speculated = *(uint64_t *)(var_94 + 16UL);
                            } else {
                                var_100 = *var_35;
                                var_101 = var_98 * 96UL;
                                var_102 = (uint64_t *)(var_100 + 16UL);
                                var_103 = *var_102;
                                var_104 = local_sp_3 + (-24L);
                                *(uint64_t *)var_104 = 4253256UL;
                                indirect_placeholder_27(var_103, var_101);
                                local_sp_2 = var_104;
                                local_sp_14 = var_104;
                                if (var_98 != 0UL) {
                                    loop_state_var = 3U;
                                    switch_state_var = 1;
                                    break;
                                }
                                var_105 = (uint64_t *)(var_100 + 8UL);
                                *var_105 = (*var_105 << 1UL);
                                *var_102 = var_98;
                            }
                            var_106 = var_96 * 48UL;
                            var_107 = storemerge_in_sroa_speculated + var_106;
                            *(uint64_t *)(var_107 + 8UL) = var_86;
                            var_108 = *var_24;
                            *(uint64_t *)var_107 = var_97;
                            var_109 = local_sp_2 + (-8L);
                            *(uint64_t *)var_109 = 4252216UL;
                            var_110 = indirect_placeholder_74(var_108);
                            var_111 = *var_35;
                            *(uint64_t *)(var_107 + 16UL) = var_110.field_0;
                            var_112 = (uint64_t *)(var_111 + 16UL);
                            r12_3 = var_106;
                            local_sp_14 = var_109;
                            r12_6 = var_106;
                            if (*(uint64_t *)((var_106 + *var_112) + 16UL) != 0UL) {
                                loop_state_var = 3U;
                                switch_state_var = 1;
                                break;
                            }
                            *(uint64_t *)(local_sp_2 + (-16L)) = 4252256UL;
                            indirect_placeholder();
                            var_113 = *var_43;
                            var_114 = *var_45;
                            var_115 = (var_106 + *var_112) + 24UL;
                            var_116 = local_sp_2 + (-24L);
                            *(uint64_t *)var_116 = 4252286UL;
                            var_117 = indirect_placeholder_73(var_115, var_113, var_114);
                            local_sp_11 = var_116;
                            local_sp_14 = var_116;
                            if ((uint64_t)(uint32_t)var_117.field_0 != 0UL) {
                                loop_state_var = 3U;
                                switch_state_var = 1;
                                break;
                            }
                        }
                    }
                    break;
                }
                if (switch_state_var)
                    break;
            }
            if ((*(unsigned char *)(var_75 + 10UL) & '\x10') != '\x00') {
                var_119 = *var_40;
                var_120 = var_73 + 216UL;
                var_121 = local_sp_6 + (-8L);
                *(uint64_t *)var_121 = 4252586UL;
                var_122 = indirect_placeholder_81(var_72, var_74, var_119, rbx_3, var_120);
                var_123 = var_122.field_0 << 32UL;
                var_124 = (uint64_t)((long)var_123 >> (long)32UL);
                local_sp_7 = var_121;
                local_sp_8 = var_121;
                r15_0 = var_124;
                if (var_123 == 0UL) {
                    local_sp_9 = local_sp_8;
                    rax_4 = r15_0 + *var_40;
                    rbx_4_in_in = (rbx_3 << 3UL) + *(uint64_t *)(var_73 + 24UL);
                } else {
                    var_125 = r12_2 + *_cast;
                    rsi5_0 = var_125;
                    rcx1_0 = *rcx1_0_in_pre_phi;
                    var_143 = *var_9;
                    *var_48 = rcx1_0;
                    var_144 = local_sp_7 + (-8L);
                    *(uint64_t *)var_144 = 4251692UL;
                    var_145 = indirect_placeholder_78(var_143, rcx1_0, rsi5_0);
                    local_sp_9 = var_144;
                    local_sp_13 = var_144;
                    if ((uint64_t)(unsigned char)var_145.field_0 != 0UL) {
                        var_156 = *var_35;
                        r12_4 = r12_5;
                        if (var_156 != 0UL) {
                            var_162 = local_sp_13 + (-8L);
                            *(uint64_t *)var_162 = 4253373UL;
                            indirect_placeholder();
                            local_sp_0 = var_162;
                            if (*(unsigned char *)(var_0 + (-225L)) != '\x00') {
                                loop_state_var = 1U;
                                break;
                            }
                            loop_state_var = 0U;
                            break;
                        }
                        var_157 = *var_12;
                        var_158 = *var_10;
                        var_159 = *var_41;
                        var_160 = local_sp_13 + (-8L);
                        *(uint64_t *)var_160 = 4253029UL;
                        var_161 = indirect_placeholder_53(var_158, var_46, var_156, var_157, var_39, var_159);
                        local_sp_12 = var_160;
                        rbx_6 = var_161;
                        local_sp_4 = local_sp_12;
                        rax_1 = *var_40;
                        rbx_2 = rbx_6;
                        r12_1 = r12_4;
                        continue;
                    }
                    var_146 = *var_48;
                    var_147 = (rbx_3 << 3UL) + *(uint64_t *)(var_73 + 24UL);
                    var_148 = var_146 + 1UL;
                    rax_4 = var_148;
                    rbx_4_in_in = var_147;
                }
                rbx_4 = *(uint64_t *)rbx_4_in_in;
                var_149 = *var_35;
                *var_40 = rax_4;
                local_sp_10 = local_sp_9;
                rbx_5 = rbx_4;
                local_sp_13 = local_sp_9;
                if (var_149 != 0UL) {
                    *var_50 = 0UL;
                    local_sp_11 = local_sp_10;
                    rbx_7 = 18446744073709551614UL;
                    local_sp_14 = local_sp_11;
                    local_sp_13 = local_sp_11;
                    rbx_6 = rbx_5;
                    r12_6 = r12_3;
                    r12_5 = r12_3;
                    local_sp_12 = local_sp_11;
                    r12_4 = r12_3;
                    if ((long)rbx_5 < (long)0UL) {
                        local_sp_4 = local_sp_12;
                        rax_1 = *var_40;
                        rbx_2 = rbx_6;
                        r12_1 = r12_4;
                        continue;
                    }
                    if (rbx_5 != 18446744073709551614UL) {
                        loop_state_var = 3U;
                        break;
                    }
                }
                var_150 = *var_9;
                var_151 = *(uint64_t *)((rax_4 << 3UL) + *(uint64_t *)(var_150 + 184UL));
                var_152 = *(uint64_t *)(var_151 + 16UL);
                var_153 = var_151 + 24UL;
                var_154 = local_sp_9 + (-8L);
                *(uint64_t *)var_154 = 4251789UL;
                var_155 = indirect_placeholder_80(var_152, rbx_4, var_153);
                local_sp_10 = var_154;
                local_sp_13 = var_154;
                if ((long)rax_4 <= (long)*(uint64_t *)(var_150 + 168UL) & var_151 != 0UL & var_155.field_0 != 0UL) {
                    *var_50 = 0UL;
                    local_sp_11 = local_sp_10;
                    rbx_7 = 18446744073709551614UL;
                    local_sp_14 = local_sp_11;
                    local_sp_13 = local_sp_11;
                    rbx_6 = rbx_5;
                    r12_6 = r12_3;
                    r12_5 = r12_3;
                    local_sp_12 = local_sp_11;
                    r12_4 = r12_3;
                    if ((long)rbx_5 >= (long)0UL) {
                        local_sp_4 = local_sp_12;
                        rax_1 = *var_40;
                        rbx_2 = rbx_6;
                        r12_1 = r12_4;
                        continue;
                    }
                    if (rbx_5 != 18446744073709551614UL) {
                        loop_state_var = 3U;
                        break;
                    }
                }
            }
            if ((uint64_t)(var_76 + '\xfc') == 0UL) {
                rcx1_0 = *rcx1_0_in_pre_phi;
                var_143 = *var_9;
                *var_48 = rcx1_0;
                var_144 = local_sp_7 + (-8L);
                *(uint64_t *)var_144 = 4251692UL;
                var_145 = indirect_placeholder_78(var_143, rcx1_0, rsi5_0);
                local_sp_9 = var_144;
                local_sp_13 = var_144;
                if ((uint64_t)(unsigned char)var_145.field_0 != 0UL) {
                    var_156 = *var_35;
                    r12_4 = r12_5;
                    if (var_156 == 0UL) {
                        var_162 = local_sp_13 + (-8L);
                        *(uint64_t *)var_162 = 4253373UL;
                        indirect_placeholder();
                        local_sp_0 = var_162;
                        if (*(unsigned char *)(var_0 + (-225L)) != '\x00') {
                            loop_state_var = 0U;
                            break;
                        }
                        loop_state_var = 1U;
                        break;
                    }
                    var_157 = *var_12;
                    var_158 = *var_10;
                    var_159 = *var_41;
                    var_160 = local_sp_13 + (-8L);
                    *(uint64_t *)var_160 = 4253029UL;
                    var_161 = indirect_placeholder_53(var_158, var_46, var_156, var_157, var_39, var_159);
                    local_sp_12 = var_160;
                    rbx_6 = var_161;
                    local_sp_4 = local_sp_12;
                    rax_1 = *var_40;
                    rbx_2 = rbx_6;
                    r12_1 = r12_4;
                    continue;
                }
                var_146 = *var_48;
                var_147 = (rbx_3 << 3UL) + *(uint64_t *)(var_73 + 24UL);
                var_148 = var_146 + 1UL;
                rax_4 = var_148;
                rbx_4_in_in = var_147;
            } else {
                var_126 = ((*(uint64_t *)var_75 << 4UL) + 16UL) + *var_12;
                var_127 = *(uint64_t *)(var_126 + 8UL);
                var_128 = *(uint64_t *)var_126;
                var_129 = var_127 - var_128;
                rcx1_0_in_pre_phi = var_48;
                r15_0 = var_129;
                if (*var_35 == 0UL) {
                    if (var_129 != 0UL) {
                        local_sp_9 = local_sp_8;
                        rax_4 = r15_0 + *var_40;
                        rbx_4_in_in = (rbx_3 << 3UL) + *(uint64_t *)(var_73 + 24UL);
                        rbx_4 = *(uint64_t *)rbx_4_in_in;
                        var_149 = *var_35;
                        *var_40 = rax_4;
                        local_sp_10 = local_sp_9;
                        rbx_5 = rbx_4;
                        local_sp_13 = local_sp_9;
                        if (var_149 == 0UL) {
                            var_150 = *var_9;
                            var_151 = *(uint64_t *)((rax_4 << 3UL) + *(uint64_t *)(var_150 + 184UL));
                            var_152 = *(uint64_t *)(var_151 + 16UL);
                            var_153 = var_151 + 24UL;
                            var_154 = local_sp_9 + (-8L);
                            *(uint64_t *)var_154 = 4251789UL;
                            var_155 = indirect_placeholder_80(var_152, rbx_4, var_153);
                            local_sp_10 = var_154;
                            local_sp_13 = var_154;
                            if ((long)rax_4 <= (long)*(uint64_t *)(var_150 + 168UL) & var_151 != 0UL & var_155.field_0 != 0UL) {
                                *var_50 = 0UL;
                                local_sp_11 = local_sp_10;
                                rbx_7 = 18446744073709551614UL;
                                local_sp_14 = local_sp_11;
                                local_sp_13 = local_sp_11;
                                rbx_6 = rbx_5;
                                r12_6 = r12_3;
                                r12_5 = r12_3;
                                local_sp_12 = local_sp_11;
                                r12_4 = r12_3;
                                if ((long)rbx_5 >= (long)0UL) {
                                    local_sp_4 = local_sp_12;
                                    rax_1 = *var_40;
                                    rbx_2 = rbx_6;
                                    r12_1 = r12_4;
                                    continue;
                                }
                                if (rbx_5 != 18446744073709551614UL) {
                                    loop_state_var = 3U;
                                    break;
                                }
                            }
                        }
                        *var_50 = 0UL;
                        local_sp_11 = local_sp_10;
                        rbx_7 = 18446744073709551614UL;
                        local_sp_14 = local_sp_11;
                        local_sp_13 = local_sp_11;
                        rbx_6 = rbx_5;
                        r12_6 = r12_3;
                        r12_5 = r12_3;
                        local_sp_12 = local_sp_11;
                        r12_4 = r12_3;
                        if ((long)rbx_5 < (long)0UL) {
                            local_sp_4 = local_sp_12;
                            rax_1 = *var_40;
                            rbx_2 = rbx_6;
                            r12_1 = r12_4;
                            continue;
                        }
                        if (rbx_5 != 18446744073709551614UL) {
                            loop_state_var = 3U;
                            break;
                        }
                        var_156 = *var_35;
                        r12_4 = r12_5;
                        if (var_156 != 0UL) {
                            var_162 = local_sp_13 + (-8L);
                            *(uint64_t *)var_162 = 4253373UL;
                            indirect_placeholder();
                            local_sp_0 = var_162;
                            if (*(unsigned char *)(var_0 + (-225L)) != '\x00') {
                                loop_state_var = 1U;
                                break;
                            }
                            loop_state_var = 0U;
                            break;
                        }
                        var_157 = *var_12;
                        var_158 = *var_10;
                        var_159 = *var_41;
                        var_160 = local_sp_13 + (-8L);
                        *(uint64_t *)var_160 = 4253029UL;
                        var_161 = indirect_placeholder_53(var_158, var_46, var_156, var_157, var_39, var_159);
                        local_sp_12 = var_160;
                        rbx_6 = var_161;
                        local_sp_4 = local_sp_12;
                        rax_1 = *var_40;
                        rbx_2 = rbx_6;
                        r12_1 = r12_4;
                        continue;
                    }
                }
                if (!((var_128 == 18446744073709551615UL) || (var_127 == 18446744073709551615UL))) {
                    var_156 = *var_35;
                    r12_4 = r12_5;
                    if (var_156 != 0UL) {
                        var_162 = local_sp_13 + (-8L);
                        *(uint64_t *)var_162 = 4253373UL;
                        indirect_placeholder();
                        local_sp_0 = var_162;
                        if (*(unsigned char *)(var_0 + (-225L)) != '\x00') {
                            loop_state_var = 1U;
                            break;
                        }
                        loop_state_var = 0U;
                        break;
                    }
                    var_157 = *var_12;
                    var_158 = *var_10;
                    var_159 = *var_41;
                    var_160 = local_sp_13 + (-8L);
                    *(uint64_t *)var_160 = 4253029UL;
                    var_161 = indirect_placeholder_53(var_158, var_46, var_156, var_157, var_39, var_159);
                    local_sp_12 = var_160;
                    rbx_6 = var_161;
                    local_sp_4 = local_sp_12;
                    rax_1 = *var_40;
                    rbx_2 = rbx_6;
                    r12_1 = r12_4;
                    continue;
                }
                if (var_129 != 0UL) {
                    var_130 = local_sp_6 + (-8L);
                    *(uint64_t *)var_130 = 4253109UL;
                    indirect_placeholder();
                    local_sp_8 = var_130;
                    local_sp_13 = var_130;
                    if ((uint64_t)(uint32_t)var_72 != 0UL) {
                        var_156 = *var_35;
                        r12_4 = r12_5;
                        if (var_156 != 0UL) {
                            var_162 = local_sp_13 + (-8L);
                            *(uint64_t *)var_162 = 4253373UL;
                            indirect_placeholder();
                            local_sp_0 = var_162;
                            if (*(unsigned char *)(var_0 + (-225L)) != '\x00') {
                                loop_state_var = 1U;
                                break;
                            }
                            loop_state_var = 0U;
                            break;
                        }
                        var_157 = *var_12;
                        var_158 = *var_10;
                        var_159 = *var_41;
                        var_160 = local_sp_13 + (-8L);
                        *(uint64_t *)var_160 = 4253029UL;
                        var_161 = indirect_placeholder_53(var_158, var_46, var_156, var_157, var_39, var_159);
                        local_sp_12 = var_160;
                        rbx_6 = var_161;
                        local_sp_4 = local_sp_12;
                        rax_1 = *var_40;
                        rbx_2 = rbx_6;
                        r12_1 = r12_4;
                        continue;
                    }
                    local_sp_9 = local_sp_8;
                    rax_4 = r15_0 + *var_40;
                    rbx_4_in_in = (rbx_3 << 3UL) + *(uint64_t *)(var_73 + 24UL);
                    rbx_4 = *(uint64_t *)rbx_4_in_in;
                    var_149 = *var_35;
                    *var_40 = rax_4;
                    local_sp_10 = local_sp_9;
                    rbx_5 = rbx_4;
                    local_sp_13 = local_sp_9;
                    if (var_149 == 0UL) {
                        var_150 = *var_9;
                        var_151 = *(uint64_t *)((rax_4 << 3UL) + *(uint64_t *)(var_150 + 184UL));
                        var_152 = *(uint64_t *)(var_151 + 16UL);
                        var_153 = var_151 + 24UL;
                        var_154 = local_sp_9 + (-8L);
                        *(uint64_t *)var_154 = 4251789UL;
                        var_155 = indirect_placeholder_80(var_152, rbx_4, var_153);
                        local_sp_10 = var_154;
                        local_sp_13 = var_154;
                        if ((long)rax_4 <= (long)*(uint64_t *)(var_150 + 168UL) & var_151 != 0UL & var_155.field_0 != 0UL) {
                            *var_50 = 0UL;
                            local_sp_11 = local_sp_10;
                            rbx_7 = 18446744073709551614UL;
                            local_sp_14 = local_sp_11;
                            local_sp_13 = local_sp_11;
                            rbx_6 = rbx_5;
                            r12_6 = r12_3;
                            r12_5 = r12_3;
                            local_sp_12 = local_sp_11;
                            r12_4 = r12_3;
                            if ((long)rbx_5 >= (long)0UL) {
                                local_sp_4 = local_sp_12;
                                rax_1 = *var_40;
                                rbx_2 = rbx_6;
                                r12_1 = r12_4;
                                continue;
                            }
                            if (rbx_5 != 18446744073709551614UL) {
                                loop_state_var = 3U;
                                break;
                            }
                        }
                    }
                    *var_50 = 0UL;
                    local_sp_11 = local_sp_10;
                    rbx_7 = 18446744073709551614UL;
                    local_sp_14 = local_sp_11;
                    local_sp_13 = local_sp_11;
                    rbx_6 = rbx_5;
                    r12_6 = r12_3;
                    r12_5 = r12_3;
                    local_sp_12 = local_sp_11;
                    r12_4 = r12_3;
                    if ((long)rbx_5 < (long)0UL) {
                        local_sp_4 = local_sp_12;
                        rax_1 = *var_40;
                        rbx_2 = rbx_6;
                        r12_1 = r12_4;
                        continue;
                    }
                    if (rbx_5 != 18446744073709551614UL) {
                        loop_state_var = 3U;
                        break;
                    }
                    var_156 = *var_35;
                    r12_4 = r12_5;
                    if (var_156 != 0UL) {
                        var_162 = local_sp_13 + (-8L);
                        *(uint64_t *)var_162 = 4253373UL;
                        indirect_placeholder();
                        local_sp_0 = var_162;
                        if (*(unsigned char *)(var_0 + (-225L)) != '\x00') {
                            loop_state_var = 1U;
                            break;
                        }
                        loop_state_var = 0U;
                        break;
                    }
                    var_157 = *var_12;
                    var_158 = *var_10;
                    var_159 = *var_41;
                    var_160 = local_sp_13 + (-8L);
                    *(uint64_t *)var_160 = 4253029UL;
                    var_161 = indirect_placeholder_53(var_158, var_46, var_156, var_157, var_39, var_159);
                    local_sp_12 = var_160;
                    rbx_6 = var_161;
                    local_sp_4 = local_sp_12;
                    rax_1 = *var_40;
                    rbx_2 = rbx_6;
                    r12_1 = r12_4;
                    continue;
                }
                var_131 = local_sp_6 + (-8L);
                *(uint64_t *)var_131 = 4252710UL;
                var_132 = indirect_placeholder_79(var_46, rbx_3);
                local_sp_14 = var_131;
                if ((uint64_t)(unsigned char)var_132.field_0 != 0UL) {
                    loop_state_var = 3U;
                    break;
                }
                var_133 = *(uint64_t *)(var_73 + 40UL);
                var_134 = *var_40;
                var_135 = (rbx_3 * 24UL) + var_133;
                *var_48 = var_134;
                var_136 = **(uint64_t **)(var_135 + 16UL);
                var_137 = *(uint64_t *)((var_134 << 3UL) + *(uint64_t *)(*var_9 + 184UL));
                var_138 = *(uint64_t *)(var_137 + 16UL);
                var_139 = var_137 + 24UL;
                var_140 = local_sp_6 + (-16L);
                *(uint64_t *)var_140 = 4252782UL;
                var_141 = indirect_placeholder_72(var_138, var_136, var_139);
                local_sp_7 = var_140;
                local_sp_11 = var_140;
                rbx_5 = var_136;
                if (var_141.field_0 != 0UL) {
                    rbx_7 = 18446744073709551614UL;
                    local_sp_14 = local_sp_11;
                    local_sp_13 = local_sp_11;
                    rbx_6 = rbx_5;
                    r12_6 = r12_3;
                    r12_5 = r12_3;
                    local_sp_12 = local_sp_11;
                    r12_4 = r12_3;
                    if ((long)rbx_5 >= (long)0UL) {
                        local_sp_4 = local_sp_12;
                        rax_1 = *var_40;
                        rbx_2 = rbx_6;
                        r12_1 = r12_4;
                        continue;
                    }
                    if (rbx_5 != 18446744073709551614UL) {
                        loop_state_var = 3U;
                        break;
                    }
                    var_156 = *var_35;
                    r12_4 = r12_5;
                    if (var_156 != 0UL) {
                        var_162 = local_sp_13 + (-8L);
                        *(uint64_t *)var_162 = 4253373UL;
                        indirect_placeholder();
                        local_sp_0 = var_162;
                        if (*(unsigned char *)(var_0 + (-225L)) != '\x00') {
                            loop_state_var = 1U;
                            break;
                        }
                        loop_state_var = 0U;
                        break;
                    }
                    var_157 = *var_12;
                    var_158 = *var_10;
                    var_159 = *var_41;
                    var_160 = local_sp_13 + (-8L);
                    *(uint64_t *)var_160 = 4253029UL;
                    var_161 = indirect_placeholder_53(var_158, var_46, var_156, var_157, var_39, var_159);
                    local_sp_12 = var_160;
                    rbx_6 = var_161;
                    local_sp_4 = local_sp_12;
                    rax_1 = *var_40;
                    rbx_2 = rbx_6;
                    r12_1 = r12_4;
                    continue;
                }
                var_142 = r12_2 + *_cast;
                rsi5_0 = var_142;
            }
        }
    switch (loop_state_var) {
      case 3U:
      case 1U:
      case 0U:
        {
            switch (loop_state_var) {
              case 0U:
                {
                    var_166 = (uint32_t *)var_47;
                    *var_166 = rax_0;
                    *(uint64_t *)(local_sp_0 + (-8L)) = 4253353UL;
                    indirect_placeholder();
                    var_167 = (uint64_t)*var_166;
                    rax_5 = var_167;
                }
                break;
              case 1U:
                {
                    return rax_5;
                }
                break;
              case 3U:
                {
                    var_163 = local_sp_14 + (-8L);
                    *(uint64_t *)var_163 = 4252313UL;
                    indirect_placeholder();
                    local_sp_16 = var_163;
                    rbx_8 = rbx_7;
                    r12_8 = r12_6;
                    if (*(unsigned char *)(var_0 + (-225L)) != '\x00') {
                        var_164 = local_sp_14 + (-16L);
                        *(uint64_t *)var_164 = 4252334UL;
                        indirect_placeholder();
                        local_sp_16 = var_164;
                    }
                    var_172 = *_pre_phi357;
                    *(uint64_t *)(local_sp_16 + (-8L)) = 4252346UL;
                    indirect_placeholder_82(rbx_8, var_172, var_7, r12_8);
                }
                break;
            }
        }
        break;
      case 2U:
        {
            var_168 = local_sp_15 + (-8L);
            *(uint64_t *)var_168 = 4253180UL;
            indirect_placeholder();
            local_sp_1 = var_168;
            if (*(unsigned char *)(var_0 + (-225L)) != '\x00') {
                var_169 = local_sp_15 + (-16L);
                *(uint64_t *)var_169 = 4253293UL;
                indirect_placeholder();
                local_sp_1 = var_169;
            }
            var_170 = *var_35;
            *(uint64_t *)(local_sp_1 + (-8L)) = 4253201UL;
            var_171 = indirect_placeholder_71(rbx_2, var_170, var_7, r12_7);
            return var_171.field_0;
        }
        break;
    }
}

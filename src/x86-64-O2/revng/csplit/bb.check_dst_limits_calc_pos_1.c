typedef unsigned char __u_char;
typedef unsigned short __u_short;
typedef unsigned int __u_int;
typedef unsigned long __u_long;
typedef signed char __int8_t;
typedef unsigned char __uint8_t;
typedef short __int16_t;
typedef unsigned short __uint16_t;
typedef int __int32_t;
typedef unsigned int __uint32_t;
typedef long __int64_t;
typedef unsigned long __uint64_t;
typedef __int8_t __int_least8_t;
typedef __uint8_t __uint_least8_t;
typedef __int16_t __int_least16_t;
typedef __uint16_t __uint_least16_t;
typedef __int32_t __int_least32_t;
typedef __uint32_t __uint_least32_t;
typedef __int64_t __int_least64_t;
typedef __uint64_t __uint_least64_t;
typedef long __quad_t;
typedef unsigned long __u_quad_t;
typedef long __intmax_t;
typedef unsigned long __uintmax_t;
typedef unsigned long __dev_t;
typedef unsigned int __uid_t;
typedef unsigned int __gid_t;
typedef unsigned long __ino_t;
typedef unsigned long __ino64_t;
typedef unsigned int __mode_t;
typedef unsigned long __nlink_t;
typedef long __off_t;
typedef long __off64_t;
typedef int __pid_t;
typedef struct {
    int __val[2];
} __fsid_t;
typedef long __clock_t;
typedef unsigned long __rlim_t;
typedef unsigned long __rlim64_t;
typedef unsigned int __id_t;
typedef long __time_t;
typedef unsigned int __useconds_t;
typedef long __suseconds_t;
typedef int __daddr_t;
typedef int __key_t;
typedef int __clockid_t;
typedef void *__timer_t;
typedef long __blksize_t;
typedef long __blkcnt_t;
typedef long __blkcnt64_t;
typedef unsigned long __fsblkcnt_t;
typedef unsigned long __fsblkcnt64_t;
typedef unsigned long __fsfilcnt_t;
typedef unsigned long __fsfilcnt64_t;
typedef long __fsword_t;
typedef long __ssize_t;
typedef long __syscall_slong_t;
typedef unsigned long __syscall_ulong_t;
typedef __off64_t __loff_t;
typedef char *__caddr_t;
typedef long __intptr_t;
typedef unsigned int __socklen_t;
typedef int __sig_atomic_t;
typedef __int8_t int8_t;
typedef __int16_t int16_t;
typedef __int32_t int32_t;
typedef __int64_t int64_t;
typedef __uint8_t uint8_t;
typedef __uint16_t uint16_t;
typedef __uint32_t uint32_t;
typedef __uint64_t uint64_t;
typedef __int_least8_t int_least8_t;
typedef __int_least16_t int_least16_t;
typedef __int_least32_t int_least32_t;
typedef __int_least64_t int_least64_t;
typedef __uint_least8_t uint_least8_t;
typedef __uint_least16_t uint_least16_t;
typedef __uint_least32_t uint_least32_t;
typedef __uint_least64_t uint_least64_t;
typedef signed char int_fast8_t;
typedef long int_fast16_t;
typedef long int_fast32_t;
typedef long int_fast64_t;
typedef unsigned char uint_fast8_t;
typedef unsigned long uint_fast16_t;
typedef unsigned long uint_fast32_t;
typedef unsigned long uint_fast64_t;
typedef long intptr_t;
typedef unsigned long uintptr_t;
typedef __intmax_t intmax_t;
typedef __uintmax_t uintmax_t;
typedef _Bool bool;
extern uint64_t revng_init_local_sp(uint64_t param_0);
extern uint64_t init_rbx(void);
extern uint64_t init_rbp(void);
extern uint64_t init_r12(void);
extern uint64_t init_r13(void);
extern uint64_t init_r14(void);
extern uint64_t init_r15(void);
extern uint64_t indirect_placeholder_49(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint64_t param_3);
extern uint64_t init_r8(void);
uint64_t bb_check_dst_limits_calc_pos_1(uint64_t rcx, uint64_t rdi, uint64_t rdx, uint64_t rsi) {
    uint64_t var_0;
    uint64_t var_1;
    uint64_t var_2;
    uint64_t var_3;
    uint64_t var_4;
    uint64_t var_5;
    uint64_t var_6;
    uint64_t var_7;
    uint64_t var_8;
    uint64_t var_9;
    uint32_t *var_10;
    uint64_t var_11;
    uint64_t local_sp_4;
    uint64_t rbx_0;
    uint64_t local_sp_2;
    uint16_t var_37;
    uint16_t *var_38;
    uint64_t local_sp_3;
    uint64_t local_sp_1_be;
    uint64_t local_sp_1;
    uint32_t var_12;
    uint64_t var_13;
    uint64_t *var_14;
    bool var_15;
    uint64_t *var_16;
    uint64_t local_sp_0_ph;
    uint64_t r13_0_ph;
    uint64_t r14_0_ph;
    uint64_t var_17;
    uint64_t var_18;
    uint64_t *var_19;
    uint32_t *var_20;
    uint32_t *var_21;
    uint64_t *var_22;
    uint64_t r13_0;
    uint64_t var_23;
    uint64_t var_24;
    unsigned char var_25;
    uint64_t merge;
    uint64_t var_26;
    uint64_t var_27;
    uint64_t var_28;
    uint64_t var_29;
    bool var_30;
    uint32_t var_31;
    uint64_t var_32;
    uint64_t var_33;
    uint64_t var_34;
    uint64_t var_35;
    uint32_t var_36;
    uint64_t var_39;
    uint64_t var_40;
    uint64_t var_41;
    unsigned int loop_state_var;
    bool switch_state_var;
    var_0 = revng_init_local_sp(0UL);
    var_1 = init_rbx();
    var_2 = init_rbp();
    var_3 = init_r8();
    var_4 = init_r13();
    var_5 = init_r12();
    var_6 = init_r15();
    var_7 = init_r14();
    *(uint64_t *)(var_0 + (-8L)) = var_6;
    *(uint64_t *)(var_0 + (-16L)) = var_7;
    *(uint64_t *)(var_0 + (-24L)) = var_4;
    *(uint64_t *)(var_0 + (-32L)) = var_5;
    *(uint64_t *)(var_0 + (-40L)) = var_2;
    *(uint64_t *)(var_0 + (-48L)) = var_1;
    var_8 = var_0 + (-136L);
    var_9 = *(uint64_t *)(rdi + 152UL);
    *(uint64_t *)(var_0 + (-120L)) = rdi;
    var_10 = (uint32_t *)(var_0 + (-112L));
    *var_10 = (uint32_t)rsi;
    *(uint64_t *)(var_0 + (-72L)) = rcx;
    *(uint64_t *)(var_0 + (-128L)) = var_3;
    var_11 = (rcx * 24UL) + *(uint64_t *)(var_9 + 48UL);
    local_sp_4 = var_8;
    local_sp_0_ph = var_8;
    r13_0_ph = 0UL;
    r14_0_ph = var_11;
    merge = 4294967295UL;
    if ((long)*(uint64_t *)(var_11 + 8UL) > (long)0UL) {
        return (uint64_t)((*(uint32_t *)(local_sp_4 + 24UL) >> 1U) & 1U);
    }
    var_12 = *var_10;
    *(uint64_t *)(var_0 + (-64L)) = (var_3 * 40UL);
    var_13 = 1UL << (rdx & 63UL);
    *(uint64_t *)(var_0 + (-104L)) = var_13;
    *(uint16_t *)(var_0 + (-94L)) = ((uint16_t)var_13 ^ (unsigned short)65535U);
    *(uint32_t *)(var_0 + (-92L)) = (var_12 & 1U);
    *(uint32_t *)(var_0 + (-108L)) = (var_12 & 2U);
    var_14 = (uint64_t *)var_9;
    var_15 = ((long)rdx > (long)63UL);
    var_16 = (uint64_t *)(var_9 + 40UL);
    while (1U)
        {
            var_17 = *(uint64_t *)(r14_0_ph + 16UL);
            var_18 = *var_14;
            var_19 = (uint64_t *)(local_sp_0_ph + 8UL);
            var_20 = (uint32_t *)(local_sp_0_ph + 28UL);
            var_21 = (uint32_t *)(local_sp_0_ph + 44UL);
            var_22 = (uint64_t *)(r14_0_ph + 8UL);
            r13_0 = r13_0_ph;
            local_sp_1 = local_sp_0_ph;
            local_sp_4 = local_sp_0_ph;
            while (1U)
                {
                    var_23 = *(uint64_t *)((r13_0 << 3UL) + var_17);
                    var_24 = (var_23 << 4UL) + var_18;
                    var_25 = *(unsigned char *)(var_24 + 8UL);
                    if ((uint64_t)(var_25 + '\xf8') == 0UL) {
                        if (*var_21 != 0U & *(uint64_t *)var_24 != rdx) {
                            loop_state_var = 0U;
                            break;
                        }
                    }
                    merge = 0UL;
                    if ((uint64_t)(var_25 + '\xf7') == 0UL) {
                        if (*var_20 != 0U & *(uint64_t *)var_24 != rdx) {
                            loop_state_var = 0U;
                            break;
                        }
                    }
                    if ((uint64_t)(var_25 + '\xfc') != 0UL) {
                        if (*var_19 == 18446744073709551615UL) {
                            loop_state_var = 1U;
                            break;
                        }
                    }
                    var_41 = r13_0 + 1UL;
                    r13_0 = var_41;
                    if ((long)*var_22 > (long)var_41) {
                        continue;
                    }
                    loop_state_var = 2U;
                    break;
                }
            switch_state_var = 0;
            switch (loop_state_var) {
              case 1U:
                {
                    var_26 = *(uint64_t *)(local_sp_0_ph + 16UL);
                    var_27 = *(uint64_t *)(local_sp_0_ph + 72UL);
                    *(uint64_t *)(local_sp_0_ph + 48UL) = r14_0_ph;
                    *(uint64_t *)(local_sp_0_ph + 56UL) = r13_0;
                    var_28 = *(uint64_t *)(local_sp_0_ph + 64UL);
                    rbx_0 = var_27 + *(uint64_t *)(var_26 + 216UL);
                    while (1U)
                        {
                            local_sp_2 = local_sp_1;
                            if (*(uint64_t *)rbx_0 == var_23) {
                                local_sp_1_be = local_sp_2;
                                local_sp_3 = local_sp_2;
                                if (*(unsigned char *)(rbx_0 + 32UL) != '\x00') {
                                    loop_state_var = 1U;
                                    break;
                                }
                                local_sp_1 = local_sp_1_be;
                                rbx_0 = rbx_0 + 40UL;
                                continue;
                            }
                            if (!var_15) {
                                if ((*(uint64_t *)(local_sp_1 + 32UL) & (uint64_t)*(uint16_t *)(rbx_0 + 34UL)) != 0UL) {
                                    local_sp_1_be = local_sp_2;
                                    local_sp_3 = local_sp_2;
                                    if (*(unsigned char *)(rbx_0 + 32UL) != '\x00') {
                                        loop_state_var = 1U;
                                        break;
                                    }
                                    local_sp_1 = local_sp_1_be;
                                    rbx_0 = rbx_0 + 40UL;
                                    continue;
                                }
                            }
                            var_29 = **(uint64_t **)(((var_23 * 24UL) + *var_16) + 16UL);
                            var_30 = (var_29 == var_28);
                            var_31 = *(uint32_t *)(local_sp_1 + 24UL);
                            if (var_30) {
                                return (uint64_t)(0U - (var_31 & 1U));
                            }
                            var_32 = (uint64_t)var_31;
                            var_33 = *(uint64_t *)(local_sp_1 + 16UL);
                            var_34 = local_sp_1 + (-8L);
                            *(uint64_t *)var_34 = 4226091UL;
                            var_35 = indirect_placeholder_49(var_29, var_33, rdx, var_32);
                            var_36 = (uint32_t)var_35;
                            local_sp_1_be = var_34;
                            local_sp_2 = var_34;
                            local_sp_3 = var_34;
                            if ((uint64_t)(var_36 + 1U) != 0UL) {
                                loop_state_var = 0U;
                                break;
                            }
                            merge = 0UL;
                            if ((uint64_t)var_36 != 0UL) {
                                if (*(uint32_t *)(local_sp_1 + 20UL) != 0U) {
                                    loop_state_var = 0U;
                                    break;
                                }
                            }
                            if (!var_15) {
                                var_37 = *(uint16_t *)(local_sp_1 + 34UL);
                                var_38 = (uint16_t *)(rbx_0 + 34UL);
                                *var_38 = (var_37 & *var_38);
                                if (*(unsigned char *)(rbx_0 + 32UL) != '\x00') {
                                    loop_state_var = 1U;
                                    break;
                                }
                                local_sp_1 = local_sp_1_be;
                                rbx_0 = rbx_0 + 40UL;
                                continue;
                            }
                        }
                    switch_state_var = 0;
                    switch (loop_state_var) {
                      case 1U:
                        {
                            var_39 = *(uint64_t *)(local_sp_3 + 48UL);
                            var_40 = *(uint64_t *)(local_sp_3 + 56UL) + 1UL;
                            local_sp_0_ph = local_sp_3;
                            r13_0_ph = var_40;
                            r14_0_ph = var_39;
                            local_sp_4 = local_sp_3;
                            if ((long)*(uint64_t *)(var_39 + 8UL) > (long)var_40) {
                                continue;
                            }
                            loop_state_var = 1U;
                            switch_state_var = 1;
                            break;
                        }
                        break;
                      case 0U:
                        {
                            loop_state_var = 0U;
                            switch_state_var = 1;
                            break;
                        }
                        break;
                    }
                    if (switch_state_var)
                        break;
                }
                break;
              case 0U:
                {
                    loop_state_var = 0U;
                    switch_state_var = 1;
                    break;
                }
                break;
              case 2U:
                {
                    loop_state_var = 1U;
                    switch_state_var = 1;
                    break;
                }
                break;
            }
            if (switch_state_var)
                break;
        }
    switch (loop_state_var) {
      case 1U:
        {
            break;
        }
        break;
      case 0U:
        {
            return merge;
        }
        break;
    }
}

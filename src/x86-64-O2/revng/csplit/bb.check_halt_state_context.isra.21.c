typedef unsigned char __u_char;
typedef unsigned short __u_short;
typedef unsigned int __u_int;
typedef unsigned long __u_long;
typedef signed char __int8_t;
typedef unsigned char __uint8_t;
typedef short __int16_t;
typedef unsigned short __uint16_t;
typedef int __int32_t;
typedef unsigned int __uint32_t;
typedef long __int64_t;
typedef unsigned long __uint64_t;
typedef __int8_t __int_least8_t;
typedef __uint8_t __uint_least8_t;
typedef __int16_t __int_least16_t;
typedef __uint16_t __uint_least16_t;
typedef __int32_t __int_least32_t;
typedef __uint32_t __uint_least32_t;
typedef __int64_t __int_least64_t;
typedef __uint64_t __uint_least64_t;
typedef long __quad_t;
typedef unsigned long __u_quad_t;
typedef long __intmax_t;
typedef unsigned long __uintmax_t;
typedef unsigned long __dev_t;
typedef unsigned int __uid_t;
typedef unsigned int __gid_t;
typedef unsigned long __ino_t;
typedef unsigned long __ino64_t;
typedef unsigned int __mode_t;
typedef unsigned long __nlink_t;
typedef long __off_t;
typedef long __off64_t;
typedef int __pid_t;
typedef struct {
    int __val[2];
} __fsid_t;
typedef long __clock_t;
typedef unsigned long __rlim_t;
typedef unsigned long __rlim64_t;
typedef unsigned int __id_t;
typedef long __time_t;
typedef unsigned int __useconds_t;
typedef long __suseconds_t;
typedef int __daddr_t;
typedef int __key_t;
typedef int __clockid_t;
typedef void *__timer_t;
typedef long __blksize_t;
typedef long __blkcnt_t;
typedef long __blkcnt64_t;
typedef unsigned long __fsblkcnt_t;
typedef unsigned long __fsblkcnt64_t;
typedef unsigned long __fsfilcnt_t;
typedef unsigned long __fsfilcnt64_t;
typedef long __fsword_t;
typedef long __ssize_t;
typedef long __syscall_slong_t;
typedef unsigned long __syscall_ulong_t;
typedef __off64_t __loff_t;
typedef char *__caddr_t;
typedef long __intptr_t;
typedef unsigned int __socklen_t;
typedef int __sig_atomic_t;
typedef __int8_t int8_t;
typedef __int16_t int16_t;
typedef __int32_t int32_t;
typedef __int64_t int64_t;
typedef __uint8_t uint8_t;
typedef __uint16_t uint16_t;
typedef __uint32_t uint32_t;
typedef __uint64_t uint64_t;
typedef __int_least8_t int_least8_t;
typedef __int_least16_t int_least16_t;
typedef __int_least32_t int_least32_t;
typedef __int_least64_t int_least64_t;
typedef __uint_least8_t uint_least8_t;
typedef __uint_least16_t uint_least16_t;
typedef __uint_least32_t uint_least32_t;
typedef __uint_least64_t uint_least64_t;
typedef signed char int_fast8_t;
typedef long int_fast16_t;
typedef long int_fast32_t;
typedef long int_fast64_t;
typedef unsigned char uint_fast8_t;
typedef unsigned long uint_fast16_t;
typedef unsigned long uint_fast32_t;
typedef unsigned long uint_fast64_t;
typedef long intptr_t;
typedef unsigned long uintptr_t;
typedef __intmax_t intmax_t;
typedef __uintmax_t uintmax_t;
typedef _Bool bool;
struct bb_check_halt_state_context_isra_21_ret_type;
struct bb_check_halt_state_context_isra_21_ret_type {
    uint64_t field_0;
    uint64_t field_1;
    uint64_t field_2;
    uint64_t field_3;
};
extern uint64_t revng_init_local_sp(uint64_t param_0);
extern uint64_t init_rbx(void);
extern uint64_t init_rbp(void);
extern uint64_t init_r12(void);
extern uint64_t helper_cc_compute_all_wrapper(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint32_t param_3);
extern uint64_t indirect_placeholder_49(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint64_t param_3);
extern uint64_t init_r9(void);
extern uint64_t init_r8(void);
extern uint64_t init_r10(void);
struct bb_check_halt_state_context_isra_21_ret_type bb_check_halt_state_context_isra_21(uint64_t rax, uint64_t rcx, uint64_t rdi, uint64_t rdx, uint64_t rsi) {
    uint64_t var_0;
    uint64_t var_1;
    uint64_t var_2;
    uint64_t var_3;
    uint64_t var_4;
    uint64_t var_5;
    uint64_t var_6;
    uint64_t var_7;
    uint64_t var_8;
    uint64_t var_9;
    uint64_t r10_0;
    uint64_t *var_10;
    uint64_t var_11;
    uint64_t var_12;
    uint64_t var_13;
    uint64_t var_14;
    uint64_t var_15;
    bool var_16;
    bool var_17;
    bool var_18;
    uint64_t rdi10_0;
    uint64_t rdi10_0_be;
    uint64_t var_19;
    uint64_t var_20;
    uint32_t var_21;
    uint64_t var_22;
    uint64_t var_23;
    uint64_t r8_1;
    uint64_t r8_0;
    uint64_t var_24;
    uint64_t r9_0;
    uint64_t var_25;
    struct bb_check_halt_state_context_isra_21_ret_type mrv;
    struct bb_check_halt_state_context_isra_21_ret_type mrv1;
    struct bb_check_halt_state_context_isra_21_ret_type mrv2;
    struct bb_check_halt_state_context_isra_21_ret_type mrv3;
    uint64_t var_26;
    uint64_t r8_2;
    struct bb_check_halt_state_context_isra_21_ret_type mrv5 = {0UL, /*implicit*/(int)0, /*implicit*/(int)0, /*implicit*/(int)0};
    struct bb_check_halt_state_context_isra_21_ret_type mrv6;
    struct bb_check_halt_state_context_isra_21_ret_type mrv7;
    unsigned int loop_state_var;
    var_0 = revng_init_local_sp(0UL);
    var_1 = init_r10();
    var_2 = init_rbx();
    var_3 = init_r9();
    var_4 = init_rbp();
    var_5 = init_r8();
    var_6 = init_r12();
    *(uint64_t *)(var_0 + (-8L)) = var_6;
    *(uint64_t *)(var_0 + (-16L)) = var_4;
    *(uint64_t *)(var_0 + (-24L)) = var_2;
    var_7 = (uint64_t)*(uint32_t *)(rdi + 160UL);
    *(uint64_t *)(var_0 + (-32L)) = 4246267UL;
    var_8 = indirect_placeholder_49(rax, rdi, var_7, rcx);
    var_9 = helper_cc_compute_all_wrapper(rsi, 0UL, 0UL, 25U);
    rdi10_0 = 0UL;
    r10_0 = var_1;
    r9_0 = var_3;
    r8_2 = var_5;
    if ((uint64_t)(((unsigned char)(var_9 >> 4UL) ^ (unsigned char)var_9) & '\xc0') == 0UL) {
        mrv5.field_1 = r10_0;
        mrv6 = mrv5;
        mrv6.field_2 = r9_0;
        mrv7 = mrv6;
        mrv7.field_3 = r8_2;
        return mrv7;
    }
    var_10 = *(uint64_t **)(rdi + 152UL);
    var_11 = var_8 & 1UL;
    var_12 = *(uint64_t *)rdx;
    var_13 = var_8 & 8UL;
    var_14 = var_8 & 2UL;
    var_15 = *var_10;
    var_16 = (var_11 == 0UL);
    var_17 = (var_14 == 0UL);
    var_18 = (var_13 == 0UL);
    r10_0 = var_11;
    r9_0 = var_12;
    while (1U)
        {
            var_19 = *(uint64_t *)((rdi10_0 << 3UL) + var_12);
            var_20 = ((var_19 << 4UL) + var_15) + 8UL;
            var_21 = *(uint32_t *)var_20 >> 8U;
            var_22 = (uint64_t)var_21;
            var_23 = (uint64_t)(var_21 & 16712703U);
            r8_0 = var_23;
            r8_1 = var_23;
            r8_2 = var_23;
            if (*(unsigned char *)var_20 == '\x02') {
                var_26 = rdi10_0 + 1UL;
                rdi10_0_be = var_26;
                r8_2 = r8_1;
                if (var_26 != rsi) {
                    loop_state_var = 0U;
                    break;
                }
                rdi10_0 = rdi10_0_be;
                continue;
            }
            if ((uint64_t)((uint16_t)var_22 & (unsigned short)1023U) != 0UL) {
                loop_state_var = 1U;
                break;
            }
            if ((var_22 & 4UL) == 0UL) {
                if (!(((var_22 & 8UL) == 0UL) || var_16)) {
                    var_24 = rdi10_0 + 1UL;
                    rdi10_0_be = var_24;
                    if (var_24 != rsi) {
                        loop_state_var = 0U;
                        break;
                    }
                    rdi10_0 = rdi10_0_be;
                    continue;
                }
            }
            if ((var_16 ^ 1) && ((var_22 & 8UL) == 0UL)) {
                var_25 = (uint64_t)((unsigned char)var_22 & '\x80');
                r8_0 = var_25;
                r8_1 = var_25;
                if (!(((var_22 & 32UL) == 0UL) || (var_17 ^ 1)) && !((var_25 == 0UL) || (var_18 ^ 1))) {
                    loop_state_var = 1U;
                    break;
                }
            }
            var_26 = rdi10_0 + 1UL;
            rdi10_0_be = var_26;
            r8_2 = r8_1;
            if (var_26 != rsi) {
                loop_state_var = 0U;
                break;
            }
            rdi10_0 = rdi10_0_be;
            continue;
        }
    switch (loop_state_var) {
      case 0U:
        {
            break;
        }
        break;
      case 1U:
        {
            mrv.field_0 = var_19;
            mrv1 = mrv;
            mrv1.field_1 = var_11;
            mrv2 = mrv1;
            mrv2.field_2 = var_12;
            mrv3 = mrv2;
            mrv3.field_3 = r8_0;
            return mrv3;
        }
        break;
    }
}

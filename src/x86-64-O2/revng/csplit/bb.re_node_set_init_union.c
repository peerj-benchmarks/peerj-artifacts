typedef unsigned char __u_char;
typedef unsigned short __u_short;
typedef unsigned int __u_int;
typedef unsigned long __u_long;
typedef signed char __int8_t;
typedef unsigned char __uint8_t;
typedef short __int16_t;
typedef unsigned short __uint16_t;
typedef int __int32_t;
typedef unsigned int __uint32_t;
typedef long __int64_t;
typedef unsigned long __uint64_t;
typedef __int8_t __int_least8_t;
typedef __uint8_t __uint_least8_t;
typedef __int16_t __int_least16_t;
typedef __uint16_t __uint_least16_t;
typedef __int32_t __int_least32_t;
typedef __uint32_t __uint_least32_t;
typedef __int64_t __int_least64_t;
typedef __uint64_t __uint_least64_t;
typedef long __quad_t;
typedef unsigned long __u_quad_t;
typedef long __intmax_t;
typedef unsigned long __uintmax_t;
typedef unsigned long __dev_t;
typedef unsigned int __uid_t;
typedef unsigned int __gid_t;
typedef unsigned long __ino_t;
typedef unsigned long __ino64_t;
typedef unsigned int __mode_t;
typedef unsigned long __nlink_t;
typedef long __off_t;
typedef long __off64_t;
typedef int __pid_t;
typedef struct {
    int __val[2];
} __fsid_t;
typedef long __clock_t;
typedef unsigned long __rlim_t;
typedef unsigned long __rlim64_t;
typedef unsigned int __id_t;
typedef long __time_t;
typedef unsigned int __useconds_t;
typedef long __suseconds_t;
typedef int __daddr_t;
typedef int __key_t;
typedef int __clockid_t;
typedef void *__timer_t;
typedef long __blksize_t;
typedef long __blkcnt_t;
typedef long __blkcnt64_t;
typedef unsigned long __fsblkcnt_t;
typedef unsigned long __fsblkcnt64_t;
typedef unsigned long __fsfilcnt_t;
typedef unsigned long __fsfilcnt64_t;
typedef long __fsword_t;
typedef long __ssize_t;
typedef long __syscall_slong_t;
typedef unsigned long __syscall_ulong_t;
typedef __off64_t __loff_t;
typedef char *__caddr_t;
typedef long __intptr_t;
typedef unsigned int __socklen_t;
typedef int __sig_atomic_t;
typedef __int8_t int8_t;
typedef __int16_t int16_t;
typedef __int32_t int32_t;
typedef __int64_t int64_t;
typedef __uint8_t uint8_t;
typedef __uint16_t uint16_t;
typedef __uint32_t uint32_t;
typedef __uint64_t uint64_t;
typedef __int_least8_t int_least8_t;
typedef __int_least16_t int_least16_t;
typedef __int_least32_t int_least32_t;
typedef __int_least64_t int_least64_t;
typedef __uint_least8_t uint_least8_t;
typedef __uint_least16_t uint_least16_t;
typedef __uint_least32_t uint_least32_t;
typedef __uint_least64_t uint_least64_t;
typedef signed char int_fast8_t;
typedef long int_fast16_t;
typedef long int_fast32_t;
typedef long int_fast64_t;
typedef unsigned char uint_fast8_t;
typedef unsigned long uint_fast16_t;
typedef unsigned long uint_fast32_t;
typedef unsigned long uint_fast64_t;
typedef long intptr_t;
typedef unsigned long uintptr_t;
typedef __intmax_t intmax_t;
typedef __uintmax_t uintmax_t;
typedef _Bool bool;
struct bb_re_node_set_init_union_ret_type;
struct indirect_placeholder_1_ret_type;
struct bb_re_node_set_init_union_ret_type {
    uint64_t field_0;
    uint64_t field_1;
};
struct indirect_placeholder_1_ret_type {
    uint64_t field_0;
    uint64_t field_1;
};
extern uint64_t revng_init_local_sp(uint64_t param_0);
extern uint64_t init_rax(void);
extern uint64_t init_rbx(void);
extern uint64_t init_rbp(void);
extern uint64_t init_r12(void);
extern void indirect_placeholder(void);
extern uint64_t init_cc_src2(void);
extern uint64_t init_r13(void);
extern uint64_t init_r14(void);
extern uint64_t helper_cc_compute_all_wrapper(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint32_t param_3);
extern uint64_t init_r15(void);
extern uint64_t init_rcx(void);
extern struct indirect_placeholder_1_ret_type indirect_placeholder_1(uint64_t param_0);
struct bb_re_node_set_init_union_ret_type bb_re_node_set_init_union(uint64_t rdi, uint64_t rdx, uint64_t rsi) {
    uint64_t var_0;
    uint64_t var_1;
    uint64_t var_2;
    uint64_t var_3;
    uint64_t var_4;
    uint64_t var_5;
    uint64_t var_6;
    uint64_t var_7;
    uint64_t var_8;
    uint64_t var_9;
    uint64_t *var_22;
    uint64_t *var_23;
    uint64_t rbx_1;
    uint64_t rcx_0;
    uint64_t r13_1;
    uint64_t rbx_0;
    uint64_t r13_0;
    bool var_27;
    uint64_t var_28;
    uint64_t var_30;
    uint64_t var_31;
    uint64_t var_32;
    uint64_t var_33;
    uint64_t var_34;
    uint64_t var_35;
    uint64_t var_29;
    uint64_t rcx_1;
    uint64_t var_24;
    uint64_t var_25;
    uint64_t var_26;
    uint64_t r15_0;
    struct bb_re_node_set_init_union_ret_type mrv1 = {0UL, /*implicit*/(int)0};
    uint64_t storemerge;
    struct bb_re_node_set_init_union_ret_type mrv2;
    struct bb_re_node_set_init_union_ret_type mrv3;
    uint64_t *var_10;
    uint64_t var_11;
    uint64_t var_12;
    bool var_13;
    uint64_t var_14;
    uint64_t rax_178;
    uint64_t *var_15;
    uint64_t var_16;
    uint64_t var_17;
    uint64_t rax_2;
    uint64_t var_18;
    uint64_t var_19;
    struct indirect_placeholder_1_ret_type var_20;
    uint64_t var_21;
    struct bb_re_node_set_init_union_ret_type mrv4;
    struct bb_re_node_set_init_union_ret_type mrv5;
    var_0 = revng_init_local_sp(0UL);
    var_1 = init_rax();
    var_2 = init_rcx();
    var_3 = init_rbx();
    var_4 = init_rbp();
    var_5 = init_r13();
    var_6 = init_cc_src2();
    var_7 = init_r12();
    var_8 = init_r15();
    var_9 = init_r14();
    *(uint64_t *)(var_0 + (-8L)) = var_8;
    *(uint64_t *)(var_0 + (-16L)) = var_9;
    *(uint64_t *)(var_0 + (-24L)) = var_5;
    *(uint64_t *)(var_0 + (-32L)) = var_7;
    *(uint64_t *)(var_0 + (-40L)) = var_4;
    *(uint64_t *)(var_0 + (-48L)) = var_3;
    rcx_0 = 1UL;
    rbx_0 = 0UL;
    r13_0 = 0UL;
    storemerge = 0UL;
    rax_178 = var_1;
    if (rsi == 0UL) {
        rax_2 = rax_178;
        if (rdx != 0UL & (long)*(uint64_t *)(rdx + 8UL) > (long)0UL) {
            indirect_placeholder();
            mrv4.field_0 = rax_2;
            mrv5 = mrv4;
            mrv5.field_1 = var_2;
            return mrv5;
        }
    }
    var_10 = (uint64_t *)(rsi + 8UL);
    var_11 = *var_10;
    var_12 = helper_cc_compute_all_wrapper(var_11, 0UL, 0UL, 25U);
    var_13 = ((uint64_t)(((unsigned char)(var_12 >> 4UL) ^ (unsigned char)var_12) & '\xc0') == 0UL);
    var_14 = (var_1 & (-256L)) | var_13;
    storemerge = 12UL;
    rax_178 = var_14;
    rax_2 = var_14;
    if (rdx == 0UL) {
        if (var_13) {
            indirect_placeholder();
            mrv4.field_0 = rax_2;
            mrv5 = mrv4;
            mrv5.field_1 = var_2;
            return mrv5;
        }
    }
    if (!var_13) {
        rax_2 = rax_178;
        if ((long)*(uint64_t *)(rdx + 8UL) > (long)0UL) {
            indirect_placeholder();
            mrv4.field_0 = rax_2;
            mrv5 = mrv4;
            mrv5.field_1 = var_2;
            return mrv5;
        }
        *(uint64_t *)(var_0 + (-64L)) = 4237311UL;
        indirect_placeholder();
        mrv2.field_0 = storemerge;
        mrv3 = mrv2;
        mrv3.field_1 = var_2;
        return mrv3;
    }
    var_15 = (uint64_t *)(rdx + 8UL);
    var_16 = *var_15;
    var_17 = helper_cc_compute_all_wrapper(var_16, 0UL, 0UL, 25U);
    rax_2 = var_16;
    if ((uint64_t)(((unsigned char)(var_17 >> 4UL) ^ (unsigned char)var_17) & '\xc0') == 0UL) {
        indirect_placeholder();
        mrv4.field_0 = rax_2;
        mrv5 = mrv4;
        mrv5.field_1 = var_2;
        return mrv5;
    }
    var_18 = var_16 + var_11;
    var_19 = var_18 << 3UL;
    *(uint64_t *)rdi = var_18;
    *(uint64_t *)(var_0 + (-64L)) = 4237099UL;
    var_20 = indirect_placeholder_1(var_19);
    var_21 = var_20.field_0;
    *(uint64_t *)(rdi + 16UL) = var_21;
    if (var_21 == 0UL) {
        mrv2.field_0 = storemerge;
        mrv3 = mrv2;
        mrv3.field_1 = var_2;
        return mrv3;
    }
    var_22 = (uint64_t *)(rsi + 16UL);
    var_23 = (uint64_t *)(rdx + 16UL);
    while (1U)
        {
            r13_1 = r13_0;
            rcx_1 = rcx_0;
            if ((long)r13_0 >= (long)*var_10) {
                var_24 = rcx_0 + (-1L);
                r15_0 = var_24;
                if ((long)*var_15 > (long)rbx_0) {
                    break;
                }
                var_25 = *var_23;
                var_26 = var_24 - rbx_0;
                *(uint64_t *)(var_0 + (-72L)) = 4237364UL;
                indirect_placeholder();
                rcx_1 = var_25;
                r15_0 = var_26 + *var_15;
                break;
            }
            var_27 = ((long)rbx_0 < (long)*var_15);
            var_28 = *var_22;
            rcx_1 = var_28;
            if (!var_27) {
                var_29 = rcx_0 + (r13_0 ^ (-1L));
                *(uint64_t *)(var_0 + (-72L)) = 4237428UL;
                indirect_placeholder();
                r15_0 = var_29 + *var_10;
                break;
            }
            var_30 = *var_23;
            var_31 = *(uint64_t *)((r13_0 << 3UL) + var_28);
            var_32 = *(uint64_t *)((rbx_0 << 3UL) + var_30);
            if ((long)var_31 > (long)var_32) {
                var_35 = rbx_0 + 1UL;
                *(uint64_t *)(((rcx_0 << 3UL) + var_21) + (-8L)) = var_32;
                rbx_1 = var_35;
            } else {
                var_33 = helper_cc_compute_all_wrapper(var_31 - var_32, var_32, var_6, 17U);
                var_34 = (var_33 >> 6UL) & 1UL;
                *(uint64_t *)(((rcx_0 << 3UL) + var_21) + (-8L)) = var_31;
                rbx_1 = rbx_0 + var_34;
                r13_1 = r13_0 + 1UL;
            }
            rcx_0 = rcx_0 + 1UL;
            rbx_0 = rbx_1;
            r13_0 = r13_1;
            continue;
        }
    *(uint64_t *)(rdi + 8UL) = r15_0;
    mrv1.field_1 = rcx_1;
    return mrv1;
}

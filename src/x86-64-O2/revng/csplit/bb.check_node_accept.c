typedef unsigned char __u_char;
typedef unsigned short __u_short;
typedef unsigned int __u_int;
typedef unsigned long __u_long;
typedef signed char __int8_t;
typedef unsigned char __uint8_t;
typedef short __int16_t;
typedef unsigned short __uint16_t;
typedef int __int32_t;
typedef unsigned int __uint32_t;
typedef long __int64_t;
typedef unsigned long __uint64_t;
typedef __int8_t __int_least8_t;
typedef __uint8_t __uint_least8_t;
typedef __int16_t __int_least16_t;
typedef __uint16_t __uint_least16_t;
typedef __int32_t __int_least32_t;
typedef __uint32_t __uint_least32_t;
typedef __int64_t __int_least64_t;
typedef __uint64_t __uint_least64_t;
typedef long __quad_t;
typedef unsigned long __u_quad_t;
typedef long __intmax_t;
typedef unsigned long __uintmax_t;
typedef unsigned long __dev_t;
typedef unsigned int __uid_t;
typedef unsigned int __gid_t;
typedef unsigned long __ino_t;
typedef unsigned long __ino64_t;
typedef unsigned int __mode_t;
typedef unsigned long __nlink_t;
typedef long __off_t;
typedef long __off64_t;
typedef int __pid_t;
typedef struct {
    int __val[2];
} __fsid_t;
typedef long __clock_t;
typedef unsigned long __rlim_t;
typedef unsigned long __rlim64_t;
typedef unsigned int __id_t;
typedef long __time_t;
typedef unsigned int __useconds_t;
typedef long __suseconds_t;
typedef int __daddr_t;
typedef int __key_t;
typedef int __clockid_t;
typedef void *__timer_t;
typedef long __blksize_t;
typedef long __blkcnt_t;
typedef long __blkcnt64_t;
typedef unsigned long __fsblkcnt_t;
typedef unsigned long __fsblkcnt64_t;
typedef unsigned long __fsfilcnt_t;
typedef unsigned long __fsfilcnt64_t;
typedef long __fsword_t;
typedef long __ssize_t;
typedef long __syscall_slong_t;
typedef unsigned long __syscall_ulong_t;
typedef __off64_t __loff_t;
typedef char *__caddr_t;
typedef long __intptr_t;
typedef unsigned int __socklen_t;
typedef int __sig_atomic_t;
typedef __int8_t int8_t;
typedef __int16_t int16_t;
typedef __int32_t int32_t;
typedef __int64_t int64_t;
typedef __uint8_t uint8_t;
typedef __uint16_t uint16_t;
typedef __uint32_t uint32_t;
typedef __uint64_t uint64_t;
typedef __int_least8_t int_least8_t;
typedef __int_least16_t int_least16_t;
typedef __int_least32_t int_least32_t;
typedef __int_least64_t int_least64_t;
typedef __uint_least8_t uint_least8_t;
typedef __uint_least16_t uint_least16_t;
typedef __uint_least32_t uint_least32_t;
typedef __uint_least64_t uint_least64_t;
typedef signed char int_fast8_t;
typedef long int_fast16_t;
typedef long int_fast32_t;
typedef long int_fast64_t;
typedef unsigned char uint_fast8_t;
typedef unsigned long uint_fast16_t;
typedef unsigned long uint_fast32_t;
typedef unsigned long uint_fast64_t;
typedef long intptr_t;
typedef unsigned long uintptr_t;
typedef __intmax_t intmax_t;
typedef __uintmax_t uintmax_t;
struct bb_check_node_accept_ret_type;
struct bb_check_node_accept_ret_type {
    uint64_t field_0;
    uint64_t field_1;
};
extern uint64_t revng_init_local_sp(uint64_t param_0);
extern uint64_t init_rbx(void);
extern uint64_t init_cc_src2(void);
extern uint64_t helper_cc_compute_c_wrapper(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint32_t param_3);
extern uint64_t helper_cc_compute_all_wrapper(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint32_t param_3);
extern uint64_t indirect_placeholder_49(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint64_t param_3);
extern uint64_t init_r8(void);
struct bb_check_node_accept_ret_type bb_check_node_accept(uint64_t rdi, uint64_t rdx, uint64_t rsi) {
    uint64_t var_0;
    uint64_t var_1;
    uint64_t var_2;
    uint64_t var_3;
    uint64_t var_4;
    unsigned char var_5;
    uint64_t var_6;
    uint64_t var_7;
    unsigned char var_8;
    uint64_t var_9;
    uint64_t rax_0;
    struct bb_check_node_accept_ret_type mrv1 = {0UL, /*implicit*/(int)0};
    uint64_t var_20;
    uint64_t var_10;
    uint64_t r8_0;
    struct bb_check_node_accept_ret_type mrv3 = {0UL, /*implicit*/(int)0};
    struct bb_check_node_accept_ret_type mrv5 = {0UL, /*implicit*/(int)0};
    struct bb_check_node_accept_ret_type mrv7 = {0UL, /*implicit*/(int)0};
    uint64_t var_11;
    uint64_t var_12;
    uint64_t var_13;
    uint64_t var_14;
    struct bb_check_node_accept_ret_type mrv9 = {0UL, /*implicit*/(int)0};
    uint32_t var_15;
    uint64_t var_16;
    uint64_t var_17;
    uint64_t var_18;
    uint16_t var_19;
    uint64_t r8_1;
    struct bb_check_node_accept_ret_type mrv10;
    struct bb_check_node_accept_ret_type mrv11;
    var_0 = revng_init_local_sp(0UL);
    var_1 = init_rbx();
    var_2 = init_r8();
    var_3 = init_cc_src2();
    var_4 = *(uint64_t *)(rdi + 8UL);
    *(uint64_t *)(var_0 + (-8L)) = var_1;
    var_5 = *(unsigned char *)(var_4 + rdx);
    var_6 = (uint64_t)var_5;
    var_7 = rsi + 8UL;
    var_8 = *(unsigned char *)var_7;
    var_9 = (uint64_t)var_8 + (-3L);
    r8_0 = var_2;
    rax_0 = 0UL;
    r8_1 = var_2;
    if ((uint64_t)(unsigned char)var_9 == 0UL) {
        var_11 = *(uint64_t *)rsi;
        var_12 = var_6 >> 6UL;
        var_13 = *(uint64_t *)((var_12 << 3UL) + var_11);
        var_14 = helper_cc_compute_c_wrapper(var_12, ((var_13 >> (var_6 & 63UL)) & 1UL) | 68UL, var_3, 1U);
        r8_0 = var_13;
        if (var_14 == 0UL) {
            mrv9.field_1 = var_13;
            return mrv9;
        }
    }
    var_10 = helper_cc_compute_all_wrapper(var_9, 3UL, var_3, 14U);
    if ((var_10 & 65UL) == 0UL) {
        if ((uint64_t)(var_8 + '\xfb') != 0UL) {
            if (((uint64_t)(var_8 + '\xf9') != 0UL) || ((signed char)var_5 < '\x00')) {
                mrv7.field_1 = var_2;
                return mrv7;
            }
        }
        if ((uint64_t)(var_5 + '\xf6') == 0UL) {
            if ((*(unsigned char *)(*(uint64_t *)(rdi + 152UL) + 216UL) & '@') == '\x00') {
                mrv3.field_1 = var_2;
                return mrv3;
            }
        }
        if (var_5 != '\x00' & (signed char)*(unsigned char *)(*(uint64_t *)(rdi + 152UL) + 216UL) > '\xff') {
            mrv10.field_0 = rax_0;
            mrv11 = mrv10;
            mrv11.field_1 = r8_1;
            return mrv11;
        }
    }
    if ((uint64_t)(var_8 + '\xff') == 0UL) {
        mrv7.field_1 = var_2;
        return mrv7;
    }
    if ((uint64_t)(*(unsigned char *)rsi - var_5) == 0UL) {
        mrv5.field_1 = var_2;
        return mrv5;
    }
    var_15 = *(uint32_t *)var_7;
    var_16 = (uint64_t)var_15;
    rax_0 = 1UL;
    r8_1 = r8_0;
    if ((uint64_t)(var_15 & 261888U) == 0UL) {
        mrv10.field_0 = rax_0;
        mrv11 = mrv10;
        mrv11.field_1 = r8_1;
        return mrv11;
    }
    var_17 = (uint64_t)*(uint32_t *)(rdi + 160UL);
    *(uint64_t *)(var_0 + (-16L)) = 4244111UL;
    var_18 = indirect_placeholder_49(1UL, rdi, var_17, rdx);
    var_19 = (uint16_t)var_16;
    rax_0 = 0UL;
    if ((uint64_t)(var_19 & (unsigned short)1024U) == 0UL) {
        if (((uint64_t)(var_19 & (unsigned short)2048U) == 0UL) || ((var_18 & 1UL) == 0UL)) {
            mrv10.field_0 = rax_0;
            mrv11 = mrv10;
            mrv11.field_1 = r8_1;
            return mrv11;
        }
    }
    if ((var_18 & 1UL) == 0UL) {
        mrv10.field_0 = rax_0;
        mrv11 = mrv10;
        mrv11.field_1 = r8_1;
        return mrv11;
    }
    if ((uint64_t)(var_19 & (unsigned short)2048U) == 0UL) {
        mrv1.field_1 = r8_0;
        return mrv1;
    }
    rax_0 = 1UL;
    if (!(!(((uint64_t)(var_19 & (unsigned short)8192U) != 0UL) && ((var_18 & 2UL) == 0UL)) && (short)(uint16_t)var_15 > (short)(unsigned short)65535U)) {
        return;
    }
    var_20 = (var_18 >> 3UL) & 1UL;
    rax_0 = var_20;
}

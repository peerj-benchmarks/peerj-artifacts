typedef unsigned char __u_char;
typedef unsigned short __u_short;
typedef unsigned int __u_int;
typedef unsigned long __u_long;
typedef signed char __int8_t;
typedef unsigned char __uint8_t;
typedef short __int16_t;
typedef unsigned short __uint16_t;
typedef int __int32_t;
typedef unsigned int __uint32_t;
typedef long __int64_t;
typedef unsigned long __uint64_t;
typedef __int8_t __int_least8_t;
typedef __uint8_t __uint_least8_t;
typedef __int16_t __int_least16_t;
typedef __uint16_t __uint_least16_t;
typedef __int32_t __int_least32_t;
typedef __uint32_t __uint_least32_t;
typedef __int64_t __int_least64_t;
typedef __uint64_t __uint_least64_t;
typedef long __quad_t;
typedef unsigned long __u_quad_t;
typedef long __intmax_t;
typedef unsigned long __uintmax_t;
typedef unsigned long __dev_t;
typedef unsigned int __uid_t;
typedef unsigned int __gid_t;
typedef unsigned long __ino_t;
typedef unsigned long __ino64_t;
typedef unsigned int __mode_t;
typedef unsigned long __nlink_t;
typedef long __off_t;
typedef long __off64_t;
typedef int __pid_t;
typedef struct {
    int __val[2];
} __fsid_t;
typedef long __clock_t;
typedef unsigned long __rlim_t;
typedef unsigned long __rlim64_t;
typedef unsigned int __id_t;
typedef long __time_t;
typedef unsigned int __useconds_t;
typedef long __suseconds_t;
typedef int __daddr_t;
typedef int __key_t;
typedef int __clockid_t;
typedef void *__timer_t;
typedef long __blksize_t;
typedef long __blkcnt_t;
typedef long __blkcnt64_t;
typedef unsigned long __fsblkcnt_t;
typedef unsigned long __fsblkcnt64_t;
typedef unsigned long __fsfilcnt_t;
typedef unsigned long __fsfilcnt64_t;
typedef long __fsword_t;
typedef long __ssize_t;
typedef long __syscall_slong_t;
typedef unsigned long __syscall_ulong_t;
typedef __off64_t __loff_t;
typedef char *__caddr_t;
typedef long __intptr_t;
typedef unsigned int __socklen_t;
typedef int __sig_atomic_t;
typedef __int8_t int8_t;
typedef __int16_t int16_t;
typedef __int32_t int32_t;
typedef __int64_t int64_t;
typedef __uint8_t uint8_t;
typedef __uint16_t uint16_t;
typedef __uint32_t uint32_t;
typedef __uint64_t uint64_t;
typedef __int_least8_t int_least8_t;
typedef __int_least16_t int_least16_t;
typedef __int_least32_t int_least32_t;
typedef __int_least64_t int_least64_t;
typedef __uint_least8_t uint_least8_t;
typedef __uint_least16_t uint_least16_t;
typedef __uint_least32_t uint_least32_t;
typedef __uint_least64_t uint_least64_t;
typedef signed char int_fast8_t;
typedef long int_fast16_t;
typedef long int_fast32_t;
typedef long int_fast64_t;
typedef unsigned char uint_fast8_t;
typedef unsigned long uint_fast16_t;
typedef unsigned long uint_fast32_t;
typedef unsigned long uint_fast64_t;
typedef long intptr_t;
typedef unsigned long uintptr_t;
typedef __intmax_t intmax_t;
typedef __uintmax_t uintmax_t;
struct indirect_placeholder_194_ret_type;
struct indirect_placeholder_195_ret_type;
struct indirect_placeholder_196_ret_type;
struct indirect_placeholder_194_ret_type {
    uint64_t field_0;
    uint64_t field_1;
};
struct indirect_placeholder_195_ret_type {
    uint64_t field_0;
    uint64_t field_1;
};
struct indirect_placeholder_196_ret_type {
    uint64_t field_0;
    uint64_t field_1;
};
extern uint64_t revng_init_local_sp(uint64_t param_0);
extern uint64_t init_rbx(void);
extern uint64_t init_rbp(void);
extern uint64_t init_r12(void);
extern uint64_t init_r13(void);
extern uint64_t init_r14(void);
extern uint64_t indirect_placeholder_52(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint64_t param_3, uint64_t param_4);
extern uint64_t init_r15(void);
extern struct indirect_placeholder_194_ret_type indirect_placeholder_194(uint64_t param_0, uint64_t param_1);
extern struct indirect_placeholder_195_ret_type indirect_placeholder_195(uint64_t param_0, uint64_t param_1);
extern struct indirect_placeholder_196_ret_type indirect_placeholder_196(uint64_t param_0, uint64_t param_1, uint64_t param_2);
typedef _Bool bool;
uint64_t bb_check_arrival_expand_ecl_sub(uint64_t rcx, uint64_t rdi, uint64_t r8, uint64_t rdx, uint64_t rsi) {
    uint64_t var_23;
    uint64_t var_0;
    uint64_t var_1;
    uint64_t var_2;
    uint64_t var_3;
    uint64_t var_4;
    uint64_t var_5;
    uint64_t var_6;
    uint32_t var_7;
    uint64_t var_8;
    uint64_t var_9;
    uint64_t var_10;
    uint64_t *var_11;
    uint64_t *var_12;
    uint64_t *var_13;
    uint64_t local_sp_1;
    uint64_t var_24;
    uint64_t local_sp_0;
    uint64_t var_19;
    uint64_t var_20;
    uint64_t rax_0;
    uint64_t rbx_0;
    uint64_t var_21;
    uint64_t var_22;
    uint64_t merge;
    uint64_t var_16;
    struct indirect_placeholder_194_ret_type var_25;
    uint64_t var_17;
    struct indirect_placeholder_195_ret_type var_18;
    uint64_t rax_1;
    uint64_t var_14;
    struct indirect_placeholder_196_ret_type var_15;
    unsigned int loop_state_var;
    bool switch_state_var;
    var_0 = revng_init_local_sp(0UL);
    var_1 = init_rbx();
    var_2 = init_rbp();
    var_3 = init_r13();
    var_4 = init_r12();
    var_5 = init_r15();
    var_6 = init_r14();
    *(uint64_t *)(var_0 + (-8L)) = var_5;
    *(uint64_t *)(var_0 + (-16L)) = var_6;
    *(uint64_t *)(var_0 + (-24L)) = var_3;
    *(uint64_t *)(var_0 + (-32L)) = var_4;
    var_7 = (uint32_t)r8;
    var_8 = (uint64_t)var_7;
    *(uint64_t *)(var_0 + (-40L)) = var_2;
    *(uint64_t *)(var_0 + (-48L)) = var_1;
    var_9 = rsi + 16UL;
    var_10 = var_0 + (-56L);
    var_11 = (uint64_t *)(rsi + 8UL);
    var_12 = (uint64_t *)rdi;
    var_13 = (uint64_t *)(rdi + 40UL);
    merge = 12UL;
    rax_0 = 0UL;
    local_sp_1 = var_10;
    rbx_0 = rdx;
    while (1U)
        {
            var_14 = *var_11;
            *(uint64_t *)(local_sp_1 + (-8L)) = 4233118UL;
            var_15 = indirect_placeholder_196(var_14, rbx_0, var_9);
            if (var_15.field_0 != 0UL) {
                loop_state_var = 0U;
                break;
            }
            var_16 = (rbx_0 << 4UL) + *var_12;
            if ((uint64_t)((uint32_t)(uint64_t)*(unsigned char *)(var_16 + 8UL) - var_7) != 0UL) {
                if (*(uint64_t *)var_16 != rcx) {
                    if ((uint64_t)(var_7 + (-9)) != 0UL) {
                        loop_state_var = 0U;
                        break;
                    }
                    *(uint64_t *)(local_sp_1 + (-16L)) = 4233276UL;
                    var_25 = indirect_placeholder_194(rsi, rbx_0);
                    if ((uint64_t)(unsigned char)var_25.field_0 != 0UL) {
                        loop_state_var = 1U;
                        break;
                    }
                    loop_state_var = 0U;
                    break;
                }
            }
            var_17 = local_sp_1 + (-16L);
            *(uint64_t *)var_17 = 4233159UL;
            var_18 = indirect_placeholder_195(rsi, rbx_0);
            local_sp_0 = var_17;
            if ((uint64_t)(unsigned char)var_18.field_0 != 0UL) {
                loop_state_var = 1U;
                break;
            }
            var_19 = rbx_0 * 24UL;
            var_20 = var_19 + *var_13;
            rax_1 = var_20;
            switch_state_var = 0;
            switch (*(uint64_t *)(var_20 + 8UL)) {
              case 2UL:
                {
                    var_21 = *(uint64_t *)(*(uint64_t *)(var_20 + 16UL) + 8UL);
                    var_22 = local_sp_1 + (-24L);
                    *(uint64_t *)var_22 = 4233219UL;
                    var_23 = indirect_placeholder_52(rcx, rdi, var_8, var_21, rsi);
                    rax_0 = var_23;
                    local_sp_0 = var_22;
                    if ((uint64_t)(uint32_t)var_23 != 0UL) {
                        loop_state_var = 0U;
                        switch_state_var = 1;
                        break;
                    }
                    var_24 = var_19 + *var_13;
                    rax_1 = var_24;
                    local_sp_1 = local_sp_0;
                    rbx_0 = **(uint64_t **)(rax_1 + 16UL);
                    continue;
                }
                break;
              case 0UL:
                {
                    loop_state_var = 0U;
                    switch_state_var = 1;
                    break;
                }
                break;
              default:
                {
                    local_sp_1 = local_sp_0;
                    rbx_0 = **(uint64_t **)(rax_1 + 16UL);
                    continue;
                }
                break;
            }
            if (switch_state_var)
                break;
        }
    switch (loop_state_var) {
      case 0U:
        {
            merge = rax_0;
        }
        break;
      case 1U:
        {
            return merge;
        }
        break;
    }
}

typedef unsigned char __u_char;
typedef unsigned short __u_short;
typedef unsigned int __u_int;
typedef unsigned long __u_long;
typedef signed char __int8_t;
typedef unsigned char __uint8_t;
typedef short __int16_t;
typedef unsigned short __uint16_t;
typedef int __int32_t;
typedef unsigned int __uint32_t;
typedef long __int64_t;
typedef unsigned long __uint64_t;
typedef __int8_t __int_least8_t;
typedef __uint8_t __uint_least8_t;
typedef __int16_t __int_least16_t;
typedef __uint16_t __uint_least16_t;
typedef __int32_t __int_least32_t;
typedef __uint32_t __uint_least32_t;
typedef __int64_t __int_least64_t;
typedef __uint64_t __uint_least64_t;
typedef long __quad_t;
typedef unsigned long __u_quad_t;
typedef long __intmax_t;
typedef unsigned long __uintmax_t;
typedef unsigned long __dev_t;
typedef unsigned int __uid_t;
typedef unsigned int __gid_t;
typedef unsigned long __ino_t;
typedef unsigned long __ino64_t;
typedef unsigned int __mode_t;
typedef unsigned long __nlink_t;
typedef long __off_t;
typedef long __off64_t;
typedef int __pid_t;
typedef struct {
    int __val[2];
} __fsid_t;
typedef long __clock_t;
typedef unsigned long __rlim_t;
typedef unsigned long __rlim64_t;
typedef unsigned int __id_t;
typedef long __time_t;
typedef unsigned int __useconds_t;
typedef long __suseconds_t;
typedef int __daddr_t;
typedef int __key_t;
typedef int __clockid_t;
typedef void *__timer_t;
typedef long __blksize_t;
typedef long __blkcnt_t;
typedef long __blkcnt64_t;
typedef unsigned long __fsblkcnt_t;
typedef unsigned long __fsblkcnt64_t;
typedef unsigned long __fsfilcnt_t;
typedef unsigned long __fsfilcnt64_t;
typedef long __fsword_t;
typedef long __ssize_t;
typedef long __syscall_slong_t;
typedef unsigned long __syscall_ulong_t;
typedef __off64_t __loff_t;
typedef char *__caddr_t;
typedef long __intptr_t;
typedef unsigned int __socklen_t;
typedef int __sig_atomic_t;
typedef __int8_t int8_t;
typedef __int16_t int16_t;
typedef __int32_t int32_t;
typedef __int64_t int64_t;
typedef __uint8_t uint8_t;
typedef __uint16_t uint16_t;
typedef __uint32_t uint32_t;
typedef __uint64_t uint64_t;
typedef __int_least8_t int_least8_t;
typedef __int_least16_t int_least16_t;
typedef __int_least32_t int_least32_t;
typedef __int_least64_t int_least64_t;
typedef __uint_least8_t uint_least8_t;
typedef __uint_least16_t uint_least16_t;
typedef __uint_least32_t uint_least32_t;
typedef __uint_least64_t uint_least64_t;
typedef signed char int_fast8_t;
typedef long int_fast16_t;
typedef long int_fast32_t;
typedef long int_fast64_t;
typedef unsigned char uint_fast8_t;
typedef unsigned long uint_fast16_t;
typedef unsigned long uint_fast32_t;
typedef unsigned long uint_fast64_t;
typedef long intptr_t;
typedef unsigned long uintptr_t;
typedef __intmax_t intmax_t;
typedef __uintmax_t uintmax_t;
extern uint64_t revng_init_local_sp(uint64_t param_0);
uint64_t bb_decode_8(uint64_t rcx, uint64_t rdi, uint64_t rsi, uint64_t rdx) {
    uint64_t *_cast;
    uint64_t var_0;
    uint64_t rax_0;
    unsigned char var_1;
    uint64_t var_2;
    unsigned char *var_3;
    unsigned char var_4;
    uint64_t var_5;
    uint64_t *var_6;
    uint64_t var_9;
    uint64_t var_7;
    uint64_t var_8;
    uint64_t r8_0;
    uint64_t var_23;
    unsigned char var_10;
    unsigned char var_11;
    uint64_t var_12;
    uint64_t r8_7;
    unsigned char *var_13;
    unsigned char var_14;
    uint64_t var_17;
    uint64_t var_15;
    uint64_t var_16;
    uint64_t r8_1;
    unsigned char *var_18;
    unsigned char var_19;
    uint64_t r8_4;
    unsigned char var_20;
    uint64_t var_21;
    uint64_t var_22;
    uint64_t r8_2;
    unsigned char var_24;
    uint64_t r8_5;
    unsigned char var_25;
    uint64_t var_26;
    unsigned char *var_27;
    unsigned char var_28;
    uint64_t var_31;
    uint64_t var_29;
    uint64_t var_30;
    uint64_t r8_3;
    unsigned char var_32;
    uint64_t r8_6;
    unsigned char var_33;
    uint64_t var_34;
    uint64_t var_35;
    revng_init_local_sp(0UL);
    _cast = (uint64_t *)rdx;
    var_0 = *_cast;
    rax_0 = 0UL;
    var_9 = 0UL;
    r8_0 = var_0;
    var_23 = 0UL;
    var_17 = 0UL;
    var_31 = 0UL;
    if (rsi > 7UL) {
        return rax_0;
    }
    var_1 = *(unsigned char *)((uint64_t)*(unsigned char *)rdi + 4252096UL);
    var_2 = (uint64_t)var_1;
    var_3 = (unsigned char *)(rdi + 1UL);
    var_4 = *(unsigned char *)((uint64_t)*var_3 + 4252096UL);
    var_5 = (uint64_t)var_4;
    if (~((signed char)var_1 >= '\x00' & (signed char)var_4 >= '\x00')) {
        return;
    }
    var_6 = (uint64_t *)rcx;
    rax_0 = 1UL;
    if (*var_6 == 0UL) {
        var_7 = var_0 + 1UL;
        *(unsigned char *)var_0 = ((unsigned char)(uint64_t)((long)(var_5 << 56UL) >> (long)58UL) | (unsigned char)(var_2 << 3UL));
        var_8 = *var_6 + (-1L);
        *var_6 = var_8;
        var_9 = var_8;
        r8_0 = var_7;
    }
    var_10 = *(unsigned char *)(rdi + 2UL);
    r8_1 = r8_0;
    r8_4 = r8_0;
    r8_7 = r8_0;
    if ((uint64_t)(var_10 + '\xc3') == 0UL) {
        if (*(unsigned char *)(rdi + 3UL) == '=') {
            *_cast = r8_7;
            return 0UL;
        }
        if (*(unsigned char *)(rdi + 4UL) == '=') {
            *_cast = r8_7;
            return 0UL;
        }
        r8_5 = r8_4;
        r8_7 = r8_4;
        if (*(unsigned char *)(rdi + 5UL) == '=') {
            *_cast = r8_7;
            return 0UL;
        }
    }
    var_11 = *(unsigned char *)((uint64_t)var_10 + 4252096UL);
    var_12 = (uint64_t)var_11;
    if ((signed char)var_11 < '\x00') {
        *_cast = r8_7;
        return 0UL;
    }
    var_13 = (unsigned char *)(rdi + 3UL);
    var_14 = *(unsigned char *)((uint64_t)*var_13 + 4252096UL);
    if ((signed char)var_14 < '\x00') {
        *_cast = r8_7;
        return 0UL;
    }
    if (var_9 == 0UL) {
        var_15 = r8_0 + 1UL;
        *(unsigned char *)r8_0 = ((unsigned char)(uint64_t)((long)((uint64_t)var_14 << 56UL) >> (long)60UL) | ((unsigned char)(var_12 << 1UL) | (unsigned char)((uint64_t)*(unsigned char *)((uint64_t)*var_3 + 4252096UL) << 6UL)));
        var_16 = *var_6 + (-1L);
        *var_6 = var_16;
        var_17 = var_16;
        r8_1 = var_15;
    }
    var_18 = (unsigned char *)(rdi + 4UL);
    var_19 = *var_18;
    r8_2 = r8_1;
    r8_4 = r8_1;
    r8_7 = r8_1;
    if ((uint64_t)(var_19 + '\xc3') == 0UL) {
        r8_5 = r8_4;
        r8_7 = r8_4;
        if (*(unsigned char *)(rdi + 5UL) == '=') {
            *_cast = r8_7;
            return 0UL;
        }
    }
    var_20 = *(unsigned char *)((uint64_t)var_19 + 4252096UL);
    if ((signed char)var_20 < '\x00') {
        *_cast = r8_7;
        return 0UL;
    }
    if (var_17 == 0UL) {
        var_21 = r8_1 + 1UL;
        *(unsigned char *)r8_1 = ((unsigned char)(uint64_t)((long)((uint64_t)var_20 << 56UL) >> (long)57UL) | (unsigned char)((uint64_t)*(unsigned char *)((uint64_t)*var_13 + 4252096UL) << 4UL));
        var_22 = *var_6 + (-1L);
        *var_6 = var_22;
        var_23 = var_22;
        r8_2 = var_21;
    }
    var_24 = *(unsigned char *)(rdi + 5UL);
    r8_3 = r8_2;
    r8_5 = r8_2;
    r8_7 = r8_2;
    if ((uint64_t)(var_24 + '\xc3') != 0UL) {
        var_25 = *(unsigned char *)((uint64_t)var_24 + 4252096UL);
        var_26 = (uint64_t)var_25;
        if ((signed char)var_25 < '\x00') {
            *_cast = r8_7;
            return 0UL;
        }
        var_27 = (unsigned char *)(rdi + 6UL);
        var_28 = *(unsigned char *)((uint64_t)*var_27 + 4252096UL);
        if ((signed char)var_28 < '\x00') {
            *_cast = r8_7;
            return 0UL;
        }
        if (var_23 == 0UL) {
            var_29 = r8_2 + 1UL;
            *(unsigned char *)r8_2 = ((unsigned char)(uint64_t)((long)((uint64_t)var_28 << 56UL) >> (long)59UL) | ((unsigned char)(var_26 << 2UL) | (unsigned char)((uint64_t)*(unsigned char *)((uint64_t)*var_18 + 4252096UL) << 7UL)));
            var_30 = *var_6 + (-1L);
            *var_6 = var_30;
            var_31 = var_30;
            r8_3 = var_29;
        }
        var_32 = *(unsigned char *)(rdi + 7UL);
        r8_6 = r8_3;
        r8_7 = r8_3;
        if ((uint64_t)(var_32 + '\xc3') != 0UL) {
            var_33 = *(unsigned char *)((uint64_t)var_32 + 4252096UL);
            if ((signed char)var_33 < '\x00') {
                *_cast = r8_7;
                return 0UL;
            }
            if (var_31 == 0UL) {
                var_34 = (uint64_t)*var_27;
                var_35 = r8_3 + 1UL;
                *(unsigned char *)r8_3 = ((*(unsigned char *)(var_34 + 4252096UL) << '\x05') | var_33);
                *var_6 = (*var_6 + (-1L));
                r8_6 = var_35;
            }
        }
        *_cast = r8_6;
        return rax_0;
    }
    r8_6 = r8_5;
    r8_7 = r8_5;
    if (*(unsigned char *)(rdi + 6UL) == '=') {
        *_cast = r8_7;
        return 0UL;
    }
    if (*(unsigned char *)(rdi + 7UL) != '=') {
        *_cast = r8_7;
        return 0UL;
    }
    *_cast = r8_6;
}

typedef unsigned char __u_char;
typedef unsigned short __u_short;
typedef unsigned int __u_int;
typedef unsigned long __u_long;
typedef signed char __int8_t;
typedef unsigned char __uint8_t;
typedef short __int16_t;
typedef unsigned short __uint16_t;
typedef int __int32_t;
typedef unsigned int __uint32_t;
typedef long __int64_t;
typedef unsigned long __uint64_t;
typedef __int8_t __int_least8_t;
typedef __uint8_t __uint_least8_t;
typedef __int16_t __int_least16_t;
typedef __uint16_t __uint_least16_t;
typedef __int32_t __int_least32_t;
typedef __uint32_t __uint_least32_t;
typedef __int64_t __int_least64_t;
typedef __uint64_t __uint_least64_t;
typedef long __quad_t;
typedef unsigned long __u_quad_t;
typedef long __intmax_t;
typedef unsigned long __uintmax_t;
typedef unsigned long __dev_t;
typedef unsigned int __uid_t;
typedef unsigned int __gid_t;
typedef unsigned long __ino_t;
typedef unsigned long __ino64_t;
typedef unsigned int __mode_t;
typedef unsigned long __nlink_t;
typedef long __off_t;
typedef long __off64_t;
typedef int __pid_t;
typedef struct {
    int __val[2];
} __fsid_t;
typedef long __clock_t;
typedef unsigned long __rlim_t;
typedef unsigned long __rlim64_t;
typedef unsigned int __id_t;
typedef long __time_t;
typedef unsigned int __useconds_t;
typedef long __suseconds_t;
typedef int __daddr_t;
typedef int __key_t;
typedef int __clockid_t;
typedef void *__timer_t;
typedef long __blksize_t;
typedef long __blkcnt_t;
typedef long __blkcnt64_t;
typedef unsigned long __fsblkcnt_t;
typedef unsigned long __fsblkcnt64_t;
typedef unsigned long __fsfilcnt_t;
typedef unsigned long __fsfilcnt64_t;
typedef long __fsword_t;
typedef long __ssize_t;
typedef long __syscall_slong_t;
typedef unsigned long __syscall_ulong_t;
typedef __off64_t __loff_t;
typedef char *__caddr_t;
typedef long __intptr_t;
typedef unsigned int __socklen_t;
typedef int __sig_atomic_t;
typedef __int8_t int8_t;
typedef __int16_t int16_t;
typedef __int32_t int32_t;
typedef __int64_t int64_t;
typedef __uint8_t uint8_t;
typedef __uint16_t uint16_t;
typedef __uint32_t uint32_t;
typedef __uint64_t uint64_t;
typedef __int_least8_t int_least8_t;
typedef __int_least16_t int_least16_t;
typedef __int_least32_t int_least32_t;
typedef __int_least64_t int_least64_t;
typedef __uint_least8_t uint_least8_t;
typedef __uint_least16_t uint_least16_t;
typedef __uint_least32_t uint_least32_t;
typedef __uint_least64_t uint_least64_t;
typedef signed char int_fast8_t;
typedef long int_fast16_t;
typedef long int_fast32_t;
typedef long int_fast64_t;
typedef unsigned char uint_fast8_t;
typedef unsigned long uint_fast16_t;
typedef unsigned long uint_fast32_t;
typedef unsigned long uint_fast64_t;
typedef long intptr_t;
typedef unsigned long uintptr_t;
typedef __intmax_t intmax_t;
typedef __uintmax_t uintmax_t;
extern uint64_t revng_init_local_sp(uint64_t param_0);
void bb_base32_encode(uint64_t rcx, uint64_t rdi, uint64_t rsi, uint64_t rdx) {
    uint64_t rdi2_1;
    uint64_t rcx1_0;
    uint64_t r9_0_in;
    uint64_t rsi3_1;
    uint64_t rdi2_0;
    uint64_t rsi3_0;
    uint64_t rdx4_0_in;
    uint64_t rdx4_0;
    uint64_t r9_0;
    uint64_t var_0;
    uint64_t var_1;
    uint64_t var_2;
    uint64_t var_3;
    uint64_t var_4;
    uint64_t var_5;
    uint64_t var_6;
    uint64_t var_7;
    uint64_t var_8;
    uint64_t var_9;
    uint64_t spec_select;
    revng_init_local_sp(0UL);
    rcx1_0 = rcx;
    r9_0_in = rcx;
    rsi3_1 = 0UL;
    rdi2_0 = rdi;
    rsi3_0 = rsi;
    rdx4_0_in = rdx;
    rdx4_0 = rdx4_0_in + 8UL;
    r9_0 = r9_0_in + (-8L);
    r9_0_in = r9_0;
    rdx4_0_in = rdx4_0;
    rdi2_1 = rdi2_0;
    while (rcx1_0 != 0UL)
        {
            if (rsi3_0 == 0UL) {
                *(unsigned char *)rdx4_0_in = (unsigned char)'\x00';
                return;
            }
            var_0 = (uint64_t)*(unsigned char *)rdi2_0;
            *(unsigned char *)rdx4_0_in = *(unsigned char *)((var_0 >> 3UL) | 4252032UL);
            if (rcx1_0 == 1UL) {
                break;
            }
            var_1 = var_0 << 2UL;
            if (rsi3_0 == 1UL) {
                *(unsigned char *)(rdx4_0_in + 1UL) = *(unsigned char *)((var_1 & 28UL) | 4252032UL);
                if (rcx1_0 == 2UL) {
                    break;
                }
                *(unsigned char *)(rdx4_0_in + 2UL) = (unsigned char)'=';
                if (rcx1_0 == 3UL) {
                    break;
                }
                *(unsigned char *)(rdx4_0_in + 3UL) = (unsigned char)'=';
                if (rcx1_0 == 4UL) {
                    break;
                }
                *(unsigned char *)(rdx4_0_in + 4UL) = (unsigned char)'=';
                if (rcx1_0 == 5UL) {
                    break;
                }
            }
            var_2 = (uint64_t)*(unsigned char *)(rdi2_0 + 1UL);
            *(unsigned char *)(rdx4_0_in + 1UL) = *(unsigned char *)(((var_1 & 28UL) | (var_2 >> 6UL)) | 4252032UL);
            if (rcx1_0 == 2UL) {
                break;
            }
            *(unsigned char *)(rdx4_0_in + 2UL) = *(unsigned char *)(((var_2 >> 1UL) & 31UL) | 4252032UL);
            if (rcx1_0 == 3UL) {
                break;
            }
            var_3 = var_2 << 4UL;
            if (rsi3_0 == 2UL) {
                *(unsigned char *)(rdx4_0_in + 3UL) = *(unsigned char *)((var_3 & 16UL) | 4252032UL);
                if (rcx1_0 == 4UL) {
                    break;
                }
                *(unsigned char *)(rdx4_0_in + 4UL) = (unsigned char)'=';
                if (rcx1_0 == 5UL) {
                    break;
                }
            }
            var_4 = (uint64_t)*(unsigned char *)(rdi2_0 + 2UL);
            *(unsigned char *)(rdx4_0_in + 3UL) = *(unsigned char *)(((var_3 & 16UL) | (var_4 >> 4UL)) | 4252032UL);
            if (rcx1_0 == 4UL) {
                break;
            }
            var_5 = var_4 << 1UL;
            if (rsi3_0 == 3UL) {
                *(unsigned char *)(rdx4_0_in + 4UL) = *(unsigned char *)((var_5 & 30UL) | 4252032UL);
                if (rcx1_0 == 5UL) {
                    break;
                }
                *(unsigned char *)(rdx4_0_in + 5UL) = (unsigned char)'=';
                if (rcx1_0 == 6UL) {
                    break;
                }
                *(unsigned char *)(rdx4_0_in + 6UL) = (unsigned char)'=';
                if (rcx1_0 == 7UL) {
                    break;
                }
                *(unsigned char *)(rdx4_0_in + 7UL) = (unsigned char)'=';
                if (r9_0 == 0UL) {
                    break;
                }
                rcx1_0 = rcx1_0 + (-8L);
                rdi2_0 = rdi2_1;
                rsi3_0 = rsi3_1;
                rdx4_0 = rdx4_0_in + 8UL;
                r9_0 = r9_0_in + (-8L);
                r9_0_in = r9_0;
                rdx4_0_in = rdx4_0;
                rdi2_1 = rdi2_0;
                continue;
            }
            var_6 = (uint64_t)*(unsigned char *)(rdi2_0 + 3UL);
            *(unsigned char *)(rdx4_0_in + 4UL) = *(unsigned char *)(((var_5 & 30UL) | (var_6 >> 7UL)) | 4252032UL);
            if (rcx1_0 == 5UL) {
                break;
            }
            *(unsigned char *)(rdx4_0_in + 5UL) = *(unsigned char *)(((var_6 >> 2UL) & 31UL) | 4252032UL);
            if (rcx1_0 == 6UL) {
                break;
            }
            var_7 = var_6 << 3UL;
            if (rsi3_0 == 4UL) {
                *(unsigned char *)(rdx4_0_in + 6UL) = *(unsigned char *)((var_7 & 24UL) | 4252032UL);
                if (rcx1_0 == 7UL) {
                    break;
                }
                *(unsigned char *)(rdx4_0_in + 7UL) = (unsigned char)'=';
                if (r9_0 == 0UL) {
                    break;
                }
                rcx1_0 = rcx1_0 + (-8L);
                rdi2_0 = rdi2_1;
                rsi3_0 = rsi3_1;
                rdx4_0 = rdx4_0_in + 8UL;
                r9_0 = r9_0_in + (-8L);
                r9_0_in = r9_0;
                rdx4_0_in = rdx4_0;
                rdi2_1 = rdi2_0;
                continue;
            }
            var_8 = (uint64_t)*(unsigned char *)(rdi2_0 + 4UL);
            *(unsigned char *)(rdx4_0_in + 6UL) = *(unsigned char *)(((var_7 & 24UL) | (var_8 >> 5UL)) | 4252032UL);
            if (rcx1_0 == 7UL) {
                break;
            }
            *(unsigned char *)(rdx4_0_in + 7UL) = *(unsigned char *)((var_8 & 31UL) | 4252032UL);
            if (r9_0 == 0UL) {
                break;
            }
            var_9 = rsi3_0 + (-5L);
            spec_select = (var_9 == 0UL) ? rdi2_0 : (rdi2_0 + 5UL);
            rdi2_1 = spec_select;
            rsi3_1 = var_9;
            rcx1_0 = rcx1_0 + (-8L);
            rdi2_0 = rdi2_1;
            rsi3_0 = rsi3_1;
            rdx4_0 = rdx4_0_in + 8UL;
            r9_0 = r9_0_in + (-8L);
            r9_0_in = r9_0;
            rdx4_0_in = rdx4_0;
            rdi2_1 = rdi2_0;
        }
    return;
}

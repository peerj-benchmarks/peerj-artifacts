typedef unsigned char __u_char;
typedef unsigned short __u_short;
typedef unsigned int __u_int;
typedef unsigned long __u_long;
typedef signed char __int8_t;
typedef unsigned char __uint8_t;
typedef short __int16_t;
typedef unsigned short __uint16_t;
typedef int __int32_t;
typedef unsigned int __uint32_t;
typedef long __int64_t;
typedef unsigned long __uint64_t;
typedef __int8_t __int_least8_t;
typedef __uint8_t __uint_least8_t;
typedef __int16_t __int_least16_t;
typedef __uint16_t __uint_least16_t;
typedef __int32_t __int_least32_t;
typedef __uint32_t __uint_least32_t;
typedef __int64_t __int_least64_t;
typedef __uint64_t __uint_least64_t;
typedef long __quad_t;
typedef unsigned long __u_quad_t;
typedef long __intmax_t;
typedef unsigned long __uintmax_t;
typedef unsigned long __dev_t;
typedef unsigned int __uid_t;
typedef unsigned int __gid_t;
typedef unsigned long __ino_t;
typedef unsigned long __ino64_t;
typedef unsigned int __mode_t;
typedef unsigned long __nlink_t;
typedef long __off_t;
typedef long __off64_t;
typedef int __pid_t;
typedef struct {
    int __val[2];
} __fsid_t;
typedef long __clock_t;
typedef unsigned long __rlim_t;
typedef unsigned long __rlim64_t;
typedef unsigned int __id_t;
typedef long __time_t;
typedef unsigned int __useconds_t;
typedef long __suseconds_t;
typedef int __daddr_t;
typedef int __key_t;
typedef int __clockid_t;
typedef void *__timer_t;
typedef long __blksize_t;
typedef long __blkcnt_t;
typedef long __blkcnt64_t;
typedef unsigned long __fsblkcnt_t;
typedef unsigned long __fsblkcnt64_t;
typedef unsigned long __fsfilcnt_t;
typedef unsigned long __fsfilcnt64_t;
typedef long __fsword_t;
typedef long __ssize_t;
typedef long __syscall_slong_t;
typedef unsigned long __syscall_ulong_t;
typedef __off64_t __loff_t;
typedef char *__caddr_t;
typedef long __intptr_t;
typedef unsigned int __socklen_t;
typedef int __sig_atomic_t;
typedef __int8_t int8_t;
typedef __int16_t int16_t;
typedef __int32_t int32_t;
typedef __int64_t int64_t;
typedef __uint8_t uint8_t;
typedef __uint16_t uint16_t;
typedef __uint32_t uint32_t;
typedef __uint64_t uint64_t;
typedef __int_least8_t int_least8_t;
typedef __int_least16_t int_least16_t;
typedef __int_least32_t int_least32_t;
typedef __int_least64_t int_least64_t;
typedef __uint_least8_t uint_least8_t;
typedef __uint_least16_t uint_least16_t;
typedef __uint_least32_t uint_least32_t;
typedef __uint_least64_t uint_least64_t;
typedef signed char int_fast8_t;
typedef long int_fast16_t;
typedef long int_fast32_t;
typedef long int_fast64_t;
typedef unsigned char uint_fast8_t;
typedef unsigned long uint_fast16_t;
typedef unsigned long uint_fast32_t;
typedef unsigned long uint_fast64_t;
typedef long intptr_t;
typedef unsigned long uintptr_t;
typedef __intmax_t intmax_t;
typedef __uintmax_t uintmax_t;
typedef _Bool bool;
struct indirect_placeholder_309_ret_type;
struct indirect_placeholder_310_ret_type;
struct indirect_placeholder_309_ret_type {
    uint64_t field_0;
    uint64_t field_1;
};
struct indirect_placeholder_310_ret_type {
    uint64_t field_0;
    uint64_t field_1;
};
extern uint64_t revng_init_local_sp(uint64_t param_0);
extern uint64_t init_rbx(void);
extern uint64_t init_rbp(void);
extern void indirect_placeholder_1(void);
extern uint64_t init_r13(void);
extern uint64_t init_r12(void);
extern uint64_t init_r14(void);
extern uint64_t init_rax(void);
extern uint64_t init_r15(void);
extern struct indirect_placeholder_309_ret_type indirect_placeholder_309(uint64_t param_0);
extern struct indirect_placeholder_310_ret_type indirect_placeholder_310(uint64_t param_0);
uint64_t bb_locale_charset(void) {
    bool var_11;
    uint64_t *var_12;
    uint64_t var_13;
    struct indirect_placeholder_309_ret_type var_14;
    uint64_t var_15;
    uint64_t var_17;
    struct indirect_placeholder_310_ret_type var_18;
    uint64_t r15_6;
    uint64_t r15_5;
    uint64_t rbx_0;
    uint64_t var_23;
    uint64_t var_24;
    uint64_t var_25;
    uint64_t local_sp_11;
    uint64_t rax_8;
    bool var_22;
    uint64_t var_21;
    uint64_t var_0;
    uint64_t var_1;
    uint64_t var_2;
    uint64_t var_3;
    uint64_t var_4;
    uint64_t var_5;
    uint64_t var_6;
    uint64_t var_7;
    uint64_t var_8;
    uint64_t var_9;
    uint64_t var_10;
    uint64_t local_sp_10;
    uint64_t local_sp_0;
    uint64_t local_sp_8;
    uint64_t var_20;
    uint64_t var_19;
    uint64_t var_16;
    uint64_t local_sp_9;
    unsigned int loop_state_var;
    var_0 = revng_init_local_sp(0UL);
    var_1 = init_rax();
    var_2 = init_rbx();
    var_3 = init_rbp();
    var_4 = init_r13();
    var_5 = init_r12();
    var_6 = init_r15();
    var_7 = init_r14();
    *(uint64_t *)(var_0 + (-8L)) = var_6;
    *(uint64_t *)(var_0 + (-16L)) = var_7;
    *(uint64_t *)(var_0 + (-24L)) = var_4;
    *(uint64_t *)(var_0 + (-32L)) = var_5;
    *(uint64_t *)(var_0 + (-40L)) = var_3;
    *(uint64_t *)(var_0 + (-48L)) = var_2;
    var_8 = var_0 + (-208L);
    *(uint64_t *)var_8 = 4278171UL;
    indirect_placeholder_1();
    var_9 = *(uint64_t *)6442336UL;
    var_10 = (var_1 == 0UL) ? 4324375UL : var_1;
    local_sp_10 = var_8;
    rax_8 = 4324375UL;
    r15_5 = var_9;
    rbx_0 = var_10;
    if (var_9 != 0UL) {
        *(uint64_t *)(var_0 + (-216L)) = 4278324UL;
        indirect_placeholder_1();
        var_11 = (*(unsigned char *)4324375UL == '\x00');
        *(uint64_t *)(var_0 + (-224L)) = 4278356UL;
        indirect_placeholder_1();
        var_12 = (uint64_t *)(var_0 + (-232L));
        *var_12 = 4278369UL;
        indirect_placeholder_1();
        rax_8 = 0UL;
        r15_5 = 4324375UL;
        if (*(unsigned char *)var_11 ? 8649167UL : 8648958UL == '/') {
            var_17 = var_0 + (-240L);
            *(uint64_t *)var_17 = 4278466UL;
            var_18 = indirect_placeholder_310(8649169UL);
            local_sp_9 = var_17;
            if (var_18.field_0 == 0UL) {
                var_21 = local_sp_9 + (-8L);
                *(uint64_t *)var_21 = 4278894UL;
                indirect_placeholder_1();
                local_sp_0 = var_21;
            } else {
                var_19 = var_0 + (-248L);
                *(uint64_t *)var_19 = 4278492UL;
                indirect_placeholder_1();
                local_sp_8 = var_19;
                *(uint64_t *)(local_sp_8 + (-8L)) = 4278516UL;
                indirect_placeholder_1();
                *(uint64_t *)(local_sp_8 + (-16L)) = 4278523UL;
                indirect_placeholder_1();
                *(uint64_t *)(local_sp_8 + (-24L)) = 4278535UL;
                indirect_placeholder_1();
                *(uint64_t *)(local_sp_8 + (-32L)) = 4278561UL;
                indirect_placeholder_1();
                *(uint64_t *)(local_sp_8 + (-40L)) = 4278904UL;
                indirect_placeholder_1();
                var_20 = local_sp_8 + (-48L);
                *(uint64_t *)var_20 = 4278869UL;
                indirect_placeholder_1();
                local_sp_0 = var_20;
            }
        } else {
            *var_12 = 4324585UL;
            var_13 = var_0 + (-240L);
            *(uint64_t *)var_13 = 4278406UL;
            var_14 = indirect_placeholder_309(8649170UL);
            var_15 = var_14.field_0;
            local_sp_9 = var_13;
            if (var_15 == 0UL) {
                var_21 = local_sp_9 + (-8L);
                *(uint64_t *)var_21 = 4278894UL;
                indirect_placeholder_1();
                local_sp_0 = var_21;
            } else {
                var_16 = var_0 + (-248L);
                *(uint64_t *)var_16 = 4278440UL;
                indirect_placeholder_1();
                *(unsigned char *)(var_15 + 4324584UL) = (unsigned char)'/';
                local_sp_8 = var_16;
                *(uint64_t *)(local_sp_8 + (-8L)) = 4278516UL;
                indirect_placeholder_1();
                *(uint64_t *)(local_sp_8 + (-16L)) = 4278523UL;
                indirect_placeholder_1();
                *(uint64_t *)(local_sp_8 + (-24L)) = 4278535UL;
                indirect_placeholder_1();
                *(uint64_t *)(local_sp_8 + (-32L)) = 4278561UL;
                indirect_placeholder_1();
                *(uint64_t *)(local_sp_8 + (-40L)) = 4278904UL;
                indirect_placeholder_1();
                var_20 = local_sp_8 + (-48L);
                *(uint64_t *)var_20 = 4278869UL;
                indirect_placeholder_1();
                local_sp_0 = var_20;
            }
        }
        *(uint64_t *)6442336UL = 4324375UL;
        local_sp_10 = local_sp_0;
    }
    local_sp_11 = local_sp_10;
    r15_6 = r15_5;
    if (*(unsigned char *)r15_5 == '\x00') {
        return (*(unsigned char *)rbx_0 == '\x00') ? 4324532UL : rbx_0;
    }
    var_22 = ((uint64_t)(uint32_t)rax_8 == 0UL);
    while (1U)
        {
            *(uint64_t *)(local_sp_11 + (-8L)) = 4278264UL;
            indirect_placeholder_1();
            if (!var_22) {
                loop_state_var = 0U;
                break;
            }
            if (*(unsigned char *)r15_6 != '*') {
                if (*(unsigned char *)(r15_6 + 1UL) != '\x00') {
                    loop_state_var = 0U;
                    break;
                }
            }
            *(uint64_t *)(local_sp_11 + (-16L)) = 4278229UL;
            indirect_placeholder_1();
            var_23 = (rax_8 + r15_6) + 1UL;
            var_24 = local_sp_11 + (-24L);
            *(uint64_t *)var_24 = 4278242UL;
            indirect_placeholder_1();
            var_25 = (rax_8 + var_23) + 1UL;
            local_sp_11 = var_24;
            r15_6 = var_25;
            if (*(unsigned char *)var_25 == '\x00') {
                continue;
            }
            loop_state_var = 1U;
            break;
        }
    *(uint64_t *)(local_sp_11 + (-16L)) = 4278276UL;
    indirect_placeholder_1();
    rbx_0 = (rax_8 + r15_6) + 1UL;
}

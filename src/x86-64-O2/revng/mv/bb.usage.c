typedef unsigned char __u_char;
typedef unsigned short __u_short;
typedef unsigned int __u_int;
typedef unsigned long __u_long;
typedef signed char __int8_t;
typedef unsigned char __uint8_t;
typedef short __int16_t;
typedef unsigned short __uint16_t;
typedef int __int32_t;
typedef unsigned int __uint32_t;
typedef long __int64_t;
typedef unsigned long __uint64_t;
typedef __int8_t __int_least8_t;
typedef __uint8_t __uint_least8_t;
typedef __int16_t __int_least16_t;
typedef __uint16_t __uint_least16_t;
typedef __int32_t __int_least32_t;
typedef __uint32_t __uint_least32_t;
typedef __int64_t __int_least64_t;
typedef __uint64_t __uint_least64_t;
typedef long __quad_t;
typedef unsigned long __u_quad_t;
typedef long __intmax_t;
typedef unsigned long __uintmax_t;
typedef unsigned long __dev_t;
typedef unsigned int __uid_t;
typedef unsigned int __gid_t;
typedef unsigned long __ino_t;
typedef unsigned long __ino64_t;
typedef unsigned int __mode_t;
typedef unsigned long __nlink_t;
typedef long __off_t;
typedef long __off64_t;
typedef int __pid_t;
typedef struct {
    int __val[2];
} __fsid_t;
typedef long __clock_t;
typedef unsigned long __rlim_t;
typedef unsigned long __rlim64_t;
typedef unsigned int __id_t;
typedef long __time_t;
typedef unsigned int __useconds_t;
typedef long __suseconds_t;
typedef int __daddr_t;
typedef int __key_t;
typedef int __clockid_t;
typedef void *__timer_t;
typedef long __blksize_t;
typedef long __blkcnt_t;
typedef long __blkcnt64_t;
typedef unsigned long __fsblkcnt_t;
typedef unsigned long __fsblkcnt64_t;
typedef unsigned long __fsfilcnt_t;
typedef unsigned long __fsfilcnt64_t;
typedef long __fsword_t;
typedef long __ssize_t;
typedef long __syscall_slong_t;
typedef unsigned long __syscall_ulong_t;
typedef __off64_t __loff_t;
typedef char *__caddr_t;
typedef long __intptr_t;
typedef unsigned int __socklen_t;
typedef int __sig_atomic_t;
typedef __int8_t int8_t;
typedef __int16_t int16_t;
typedef __int32_t int32_t;
typedef __int64_t int64_t;
typedef __uint8_t uint8_t;
typedef __uint16_t uint16_t;
typedef __uint32_t uint32_t;
typedef __uint64_t uint64_t;
typedef __int_least8_t int_least8_t;
typedef __int_least16_t int_least16_t;
typedef __int_least32_t int_least32_t;
typedef __int_least64_t int_least64_t;
typedef __uint_least8_t uint_least8_t;
typedef __uint_least16_t uint_least16_t;
typedef __uint_least32_t uint_least32_t;
typedef __uint_least64_t uint_least64_t;
typedef signed char int_fast8_t;
typedef long int_fast16_t;
typedef long int_fast32_t;
typedef long int_fast64_t;
typedef unsigned char uint_fast8_t;
typedef unsigned long uint_fast16_t;
typedef unsigned long uint_fast32_t;
typedef unsigned long uint_fast64_t;
typedef long intptr_t;
typedef unsigned long uintptr_t;
typedef __intmax_t intmax_t;
typedef __uintmax_t uintmax_t;
typedef _Bool bool;
extern uint64_t revng_init_local_sp(uint64_t param_0);
extern void indirect_placeholder_1(void);
void bb_usage(uint64_t rbx, uint64_t rdi, uint64_t rbp) {
    uint64_t var_0;
    uint64_t var_15;
    uint64_t local_sp_2;
    uint64_t var_16;
    uint64_t local_sp_6;
    uint64_t var_14;
    uint64_t local_sp_7;
    uint64_t *var_2;
    uint64_t *var_3;
    uint64_t *var_4;
    uint64_t *var_5;
    uint64_t *var_6;
    uint64_t *var_7;
    uint64_t *var_8;
    uint64_t *var_9;
    uint64_t *var_10;
    uint64_t *var_11;
    bool var_12;
    uint64_t *var_13;
    uint64_t var_17;
    uint64_t var_1;
    var_0 = revng_init_local_sp(0UL);
    *(uint64_t *)(var_0 + (-8L)) = rbp;
    *(uint64_t *)(var_0 + (-16L)) = rbx;
    if ((uint64_t)(uint32_t)rdi == 0UL) {
        local_sp_7 = var_0 + (-136L);
    } else {
        var_1 = var_0 + (-144L);
        *(uint64_t *)var_1 = 4208342UL;
        indirect_placeholder_1();
        local_sp_6 = var_1;
        var_17 = local_sp_6 + (-8L);
        *(uint64_t *)var_17 = 4208349UL;
        indirect_placeholder_1();
        local_sp_7 = var_17;
    }
    while (1U)
        {
            var_2 = (uint64_t *)(local_sp_7 + (-8L));
            *var_2 = 4208374UL;
            indirect_placeholder_1();
            var_3 = (uint64_t *)(local_sp_7 + (-16L));
            *var_3 = 4208394UL;
            indirect_placeholder_1();
            var_4 = (uint64_t *)(local_sp_7 + (-24L));
            *var_4 = 4208407UL;
            indirect_placeholder_1();
            var_5 = (uint64_t *)(local_sp_7 + (-32L));
            *var_5 = 4208420UL;
            indirect_placeholder_1();
            var_6 = (uint64_t *)(local_sp_7 + (-40L));
            *var_6 = 4208433UL;
            indirect_placeholder_1();
            var_7 = (uint64_t *)(local_sp_7 + (-48L));
            *var_7 = 4208446UL;
            indirect_placeholder_1();
            var_8 = (uint64_t *)(local_sp_7 + (-56L));
            *var_8 = 4208459UL;
            indirect_placeholder_1();
            var_9 = (uint64_t *)(local_sp_7 + (-64L));
            *var_9 = 4208472UL;
            indirect_placeholder_1();
            var_10 = (uint64_t *)(local_sp_7 + (-72L));
            *var_10 = 4208485UL;
            indirect_placeholder_1();
            var_11 = (uint64_t *)(local_sp_7 + (-80L));
            *var_11 = 4208501UL;
            indirect_placeholder_1();
            *var_11 = 4314015UL;
            *var_10 = 4314020UL;
            *var_9 = 4314117UL;
            *var_8 = 4314036UL;
            *var_7 = 4314058UL;
            *var_6 = 4314068UL;
            *var_5 = 4314083UL;
            *var_4 = 4314068UL;
            *var_3 = 4314093UL;
            *var_2 = 4314068UL;
            *(uint64_t *)local_sp_7 = 4314103UL;
            *(uint64_t *)(local_sp_7 + 8UL) = 4314068UL;
            *(uint64_t *)(local_sp_7 + 16UL) = 0UL;
            *(uint64_t *)(local_sp_7 + 24UL) = 0UL;
            *(uint64_t *)(local_sp_7 + (-88L)) = 4208662UL;
            indirect_placeholder_1();
            var_12 = (*var_9 == 0UL);
            var_13 = (uint64_t *)(local_sp_7 + (-96L));
            if (var_12) {
                *var_13 = 4208797UL;
                indirect_placeholder_1();
                *(uint64_t *)(local_sp_7 + (-104L)) = 4208809UL;
                indirect_placeholder_1();
                var_15 = local_sp_7 + (-112L);
                *(uint64_t *)var_15 = 4208882UL;
                indirect_placeholder_1();
                local_sp_2 = var_15;
            } else {
                *var_13 = 4208697UL;
                indirect_placeholder_1();
                *(uint64_t *)(local_sp_7 + (-104L)) = 4208709UL;
                indirect_placeholder_1();
                var_14 = local_sp_7 + (-112L);
                *(uint64_t *)var_14 = 4208758UL;
                indirect_placeholder_1();
                local_sp_2 = var_14;
            }
            var_16 = local_sp_2 + (-8L);
            *(uint64_t *)var_16 = 4208787UL;
            indirect_placeholder_1();
            local_sp_6 = var_16;
            var_17 = local_sp_6 + (-8L);
            *(uint64_t *)var_17 = 4208349UL;
            indirect_placeholder_1();
            local_sp_7 = var_17;
            continue;
        }
}

typedef unsigned char __u_char;
typedef unsigned short __u_short;
typedef unsigned int __u_int;
typedef unsigned long __u_long;
typedef signed char __int8_t;
typedef unsigned char __uint8_t;
typedef short __int16_t;
typedef unsigned short __uint16_t;
typedef int __int32_t;
typedef unsigned int __uint32_t;
typedef long __int64_t;
typedef unsigned long __uint64_t;
typedef __int8_t __int_least8_t;
typedef __uint8_t __uint_least8_t;
typedef __int16_t __int_least16_t;
typedef __uint16_t __uint_least16_t;
typedef __int32_t __int_least32_t;
typedef __uint32_t __uint_least32_t;
typedef __int64_t __int_least64_t;
typedef __uint64_t __uint_least64_t;
typedef long __quad_t;
typedef unsigned long __u_quad_t;
typedef long __intmax_t;
typedef unsigned long __uintmax_t;
typedef unsigned long __dev_t;
typedef unsigned int __uid_t;
typedef unsigned int __gid_t;
typedef unsigned long __ino_t;
typedef unsigned long __ino64_t;
typedef unsigned int __mode_t;
typedef unsigned long __nlink_t;
typedef long __off_t;
typedef long __off64_t;
typedef int __pid_t;
typedef struct {
    int __val[2];
} __fsid_t;
typedef long __clock_t;
typedef unsigned long __rlim_t;
typedef unsigned long __rlim64_t;
typedef unsigned int __id_t;
typedef long __time_t;
typedef unsigned int __useconds_t;
typedef long __suseconds_t;
typedef int __daddr_t;
typedef int __key_t;
typedef int __clockid_t;
typedef void *__timer_t;
typedef long __blksize_t;
typedef long __blkcnt_t;
typedef long __blkcnt64_t;
typedef unsigned long __fsblkcnt_t;
typedef unsigned long __fsblkcnt64_t;
typedef unsigned long __fsfilcnt_t;
typedef unsigned long __fsfilcnt64_t;
typedef long __fsword_t;
typedef long __ssize_t;
typedef long __syscall_slong_t;
typedef unsigned long __syscall_ulong_t;
typedef __off64_t __loff_t;
typedef char *__caddr_t;
typedef long __intptr_t;
typedef unsigned int __socklen_t;
typedef int __sig_atomic_t;
typedef __int8_t int8_t;
typedef __int16_t int16_t;
typedef __int32_t int32_t;
typedef __int64_t int64_t;
typedef __uint8_t uint8_t;
typedef __uint16_t uint16_t;
typedef __uint32_t uint32_t;
typedef __uint64_t uint64_t;
typedef __int_least8_t int_least8_t;
typedef __int_least16_t int_least16_t;
typedef __int_least32_t int_least32_t;
typedef __int_least64_t int_least64_t;
typedef __uint_least8_t uint_least8_t;
typedef __uint_least16_t uint_least16_t;
typedef __uint_least32_t uint_least32_t;
typedef __uint_least64_t uint_least64_t;
typedef signed char int_fast8_t;
typedef long int_fast16_t;
typedef long int_fast32_t;
typedef long int_fast64_t;
typedef unsigned char uint_fast8_t;
typedef unsigned long uint_fast16_t;
typedef unsigned long uint_fast32_t;
typedef unsigned long uint_fast64_t;
typedef long intptr_t;
typedef unsigned long uintptr_t;
typedef __intmax_t intmax_t;
typedef __uintmax_t uintmax_t;
typedef _Bool bool;
struct indirect_placeholder_33_ret_type;
struct indirect_placeholder_34_ret_type;
struct indirect_placeholder_35_ret_type;
struct indirect_placeholder_33_ret_type {
    uint64_t field_0;
    uint64_t field_1;
};
struct indirect_placeholder_34_ret_type {
    uint64_t field_0;
    uint64_t field_1;
};
struct indirect_placeholder_35_ret_type {
    uint64_t field_0;
    uint64_t field_1;
};
extern void abort(void);
extern uint64_t revng_init_local_sp(uint64_t param_0);
extern uint64_t init_rbx(void);
extern uint64_t init_rbp(void);
extern void indirect_placeholder(void);
extern uint64_t init_cc_src2(void);
extern uint64_t helper_cc_compute_c_wrapper(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint32_t param_3);
extern uint64_t init_r13(void);
extern uint64_t init_r12(void);
extern uint64_t init_r14(void);
extern uint64_t init_rax(void);
extern uint64_t helper_cc_compute_all_wrapper(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint32_t param_3);
extern uint64_t init_rdx(void);
extern uint64_t init_r15(void);
extern uint64_t indirect_placeholder_3(uint64_t param_0, uint64_t param_1);
extern uint64_t init_r8(void);
extern struct indirect_placeholder_33_ret_type indirect_placeholder_33(uint64_t param_0);
extern struct indirect_placeholder_34_ret_type indirect_placeholder_34(uint64_t param_0);
extern struct indirect_placeholder_35_ret_type indirect_placeholder_35(uint64_t param_0);
uint64_t bb_seq_fast(uint64_t rdi, uint64_t rsi) {
    uint64_t var_0;
    uint64_t var_1;
    uint64_t var_2;
    uint64_t var_3;
    uint64_t var_4;
    uint64_t var_5;
    uint64_t var_6;
    uint64_t var_7;
    uint64_t var_8;
    uint64_t var_9;
    uint64_t var_10;
    uint64_t local_sp_2;
    uint64_t r14_1;
    uint64_t local_sp_0;
    uint64_t rax_1;
    uint64_t rax_0_be;
    uint64_t local_sp_1_be;
    uint64_t rbp_1;
    uint64_t rbx_2;
    uint64_t rdx_0_be;
    uint64_t local_sp_7;
    uint64_t var_84;
    uint64_t var_85;
    uint64_t local_sp_4;
    uint64_t rdx_2;
    uint64_t var_77;
    uint64_t var_78;
    uint64_t var_79;
    uint64_t var_80;
    uint64_t var_81;
    uint64_t rax_0;
    uint64_t rbx_1;
    uint64_t r15_0;
    uint64_t r12_1;
    uint64_t rbx_0;
    uint64_t r12_0;
    uint64_t rbp_0;
    uint64_t local_sp_1;
    uint64_t r14_0;
    uint64_t rdx_0;
    uint64_t r8_0;
    uint64_t var_82;
    uint64_t local_sp_3;
    uint64_t var_45;
    uint64_t rdx_1;
    uint64_t var_48;
    uint64_t var_49;
    uint64_t var_50;
    uint64_t local_sp_6;
    uint64_t var_46;
    uint64_t var_47;
    unsigned char *var_51;
    uint64_t var_52;
    uint64_t var_53;
    unsigned char var_54;
    uint64_t var_55;
    uint64_t rcx_0;
    uint64_t r8_1;
    unsigned char *var_56;
    unsigned char var_57;
    uint64_t var_58;
    bool var_59;
    uint64_t var_60;
    uint64_t r15_1;
    uint64_t var_61;
    uint64_t var_62;
    uint64_t var_63;
    uint64_t var_64;
    uint64_t var_65;
    uint64_t var_66;
    uint64_t r8_2;
    uint64_t var_67;
    uint64_t *var_68;
    uint64_t var_69;
    uint64_t var_70;
    uint64_t *var_71;
    uint64_t *var_72;
    uint64_t var_73;
    uint64_t *var_74;
    uint64_t var_75;
    uint64_t var_76;
    uint64_t var_83;
    uint64_t var_35;
    uint64_t var_36;
    uint64_t rax_2;
    uint64_t r13_0;
    uint64_t var_30;
    struct indirect_placeholder_33_ret_type var_31;
    uint64_t var_32;
    uint64_t var_33;
    uint64_t var_34;
    uint64_t local_sp_5;
    uint64_t var_37;
    uint64_t var_38;
    uint64_t var_39;
    struct indirect_placeholder_34_ret_type var_40;
    uint64_t var_41;
    uint64_t var_42;
    uint64_t var_43;
    uint64_t var_44;
    uint64_t var_19;
    uint64_t *var_20;
    uint64_t *_pre_phi;
    uint64_t var_21;
    uint64_t var_22;
    uint64_t var_23;
    uint64_t var_24;
    uint64_t var_25;
    uint64_t var_26;
    struct indirect_placeholder_35_ret_type var_27;
    uint64_t var_28;
    uint64_t var_29;
    uint64_t rax_4;
    uint64_t rdx_3;
    unsigned char var_11;
    uint64_t var_12;
    uint64_t var_13;
    uint64_t rax_6;
    uint64_t r12_2;
    unsigned char var_14;
    uint64_t var_15;
    uint64_t var_16;
    uint64_t spec_select210;
    uint64_t var_17;
    uint64_t *var_18;
    unsigned int loop_state_var;
    var_0 = revng_init_local_sp(0UL);
    var_1 = init_rax();
    var_2 = init_r15();
    var_3 = init_r13();
    var_4 = init_rbx();
    var_5 = init_r12();
    var_6 = init_rbp();
    var_7 = init_cc_src2();
    var_8 = init_r14();
    var_9 = init_rdx();
    var_10 = init_r8();
    *(uint64_t *)(var_0 + (-8L)) = var_2;
    *(uint64_t *)(var_0 + (-16L)) = var_8;
    *(uint64_t *)(var_0 + (-24L)) = var_3;
    *(uint64_t *)(var_0 + (-32L)) = var_5;
    *(uint64_t *)(var_0 + (-40L)) = var_6;
    *(uint64_t *)(var_0 + (-48L)) = var_4;
    *(uint64_t *)(var_0 + (-128L)) = 4201201UL;
    indirect_placeholder();
    *(unsigned char *)(var_0 + (-66L)) = ((uint64_t)(uint32_t)var_1 == 0UL);
    r8_0 = var_10;
    rax_2 = 1UL;
    r13_0 = 0UL;
    rax_4 = rdi;
    rdx_3 = var_9;
    r12_2 = rsi;
    var_11 = *(unsigned char *)rax_4;
    var_12 = (rdx_3 & (-256L)) | (uint64_t)var_11;
    var_13 = var_12 + (-48L);
    rdx_3 = var_12;
    while ((uint64_t)(unsigned char)var_13 != 0UL)
        {
            helper_cc_compute_c_wrapper(var_13, 48UL, var_7, 14U);
            rax_4 = rax_4 + 1UL;
            var_11 = *(unsigned char *)rax_4;
            var_12 = (rdx_3 & (-256L)) | (uint64_t)var_11;
            var_13 = var_12 + (-48L);
            rdx_3 = var_12;
        }
    rax_6 = (var_11 == '\x00') ? (rax_4 + (-1L)) : rax_4;
    var_14 = *(unsigned char *)r12_2;
    var_15 = (rax_6 & (-256L)) | (uint64_t)var_14;
    var_16 = var_15 + (-48L);
    rax_6 = var_15;
    while ((uint64_t)(unsigned char)var_16 != 0UL)
        {
            helper_cc_compute_c_wrapper(var_16, 48UL, var_7, 14U);
            r12_2 = r12_2 + 1UL;
            var_14 = *(unsigned char *)r12_2;
            var_15 = (rax_6 & (-256L)) | (uint64_t)var_14;
            var_16 = var_15 + (-48L);
            rax_6 = var_15;
        }
    spec_select210 = (var_14 == '\x00') ? (r12_2 + (-1L)) : var_15;
    var_17 = var_0 + (-136L);
    var_18 = (uint64_t *)var_17;
    *var_18 = 4201294UL;
    indirect_placeholder();
    r15_0 = spec_select210;
    _pre_phi = var_18;
    local_sp_7 = var_17;
    if (*(unsigned char *)(var_0 + (-74L)) == '\x00') {
        var_19 = var_0 + (-144L);
        var_20 = (uint64_t *)var_19;
        *var_20 = 4201312UL;
        indirect_placeholder();
        _pre_phi = var_20;
        r13_0 = spec_select210;
        local_sp_7 = var_19;
    }
    var_21 = spec_select210 + 1UL;
    var_22 = helper_cc_compute_c_wrapper(spec_select210 + (-30L), 31UL, var_7, 17U);
    var_23 = (var_22 == 0UL) ? var_21 : 31UL;
    var_24 = helper_cc_compute_c_wrapper(var_23 - r13_0, r13_0, var_7, 17U);
    var_25 = (var_24 == 0UL) ? var_23 : r13_0;
    var_26 = var_25 + 1UL;
    *(uint64_t *)(local_sp_7 + 16UL) = var_26;
    *(uint64_t *)(local_sp_7 + (-8L)) = 4201356UL;
    var_27 = indirect_placeholder_35(var_26);
    var_28 = var_27.field_0;
    *_pre_phi = var_28;
    var_29 = local_sp_7 + (-16L);
    *(uint64_t *)var_29 = 4201381UL;
    indirect_placeholder();
    rbx_0 = var_28;
    rbp_0 = var_25;
    local_sp_5 = var_29;
    if (*(unsigned char *)(local_sp_7 + 46UL) == '\x00') {
        *(uint64_t *)(local_sp_7 + 32UL) = 0UL;
        *(uint64_t *)(local_sp_7 + 24UL) = 0UL;
    } else {
        var_30 = *_pre_phi;
        *(uint64_t *)(local_sp_7 + (-24L)) = 4201401UL;
        var_31 = indirect_placeholder_33(var_30);
        var_32 = var_31.field_0;
        *(uint64_t *)(local_sp_7 + 24UL) = var_32;
        var_33 = local_sp_7 + (-32L);
        *(uint64_t *)var_33 = 4201427UL;
        indirect_placeholder();
        *(uint64_t *)(local_sp_7 + 8UL) = var_32;
        var_34 = helper_cc_compute_c_wrapper(r13_0 - spec_select210, spec_select210, var_7, 17U);
        rax_2 = 0UL;
        local_sp_5 = var_33;
        if (var_34 == 0UL) {
            var_35 = local_sp_7 + (-40L);
            *(uint64_t *)var_35 = 4201459UL;
            indirect_placeholder();
            var_36 = helper_cc_compute_all_wrapper(0UL, 0UL, 0UL, 24U);
            rax_2 = ((uint64_t)(((unsigned char)(var_36 >> 4UL) ^ (unsigned char)var_36) & '\xc0') != 0UL);
            local_sp_5 = var_35;
        }
    }
    *(unsigned char *)(local_sp_5 + 63UL) = (unsigned char)rax_2;
    local_sp_6 = local_sp_5;
    if (rax_2 == 0UL) {
        *(uint64_t *)(local_sp_6 + (-8L)) = 4201950UL;
        indirect_placeholder();
        *(uint64_t *)(local_sp_6 + (-16L)) = 4201960UL;
        indirect_placeholder();
        return (uint64_t)*(unsigned char *)(local_sp_6 + 47UL);
    }
    var_37 = *(uint64_t *)(local_sp_5 + 16UL) << 1UL;
    var_38 = helper_cc_compute_c_wrapper(var_37 + (-1024L), 1024UL, var_7, 17U);
    var_39 = (var_38 == 0UL) ? var_37 : 1024UL;
    *(uint64_t *)(local_sp_5 + 32UL) = var_39;
    *(uint64_t *)(local_sp_5 + (-8L)) = 4201548UL;
    var_40 = indirect_placeholder_34(var_39);
    var_41 = var_40.field_0;
    var_42 = *(uint64_t *)(local_sp_5 + 24UL);
    var_43 = var_42 + var_41;
    var_44 = local_sp_5 + (-16L);
    *(uint64_t *)var_44 = 4201574UL;
    indirect_placeholder();
    rax_0 = var_42;
    r12_0 = var_41;
    local_sp_1 = var_44;
    r14_0 = var_43;
    rdx_0 = var_42;
    while (1U)
        {
            local_sp_2 = local_sp_1;
            r14_1 = r14_0;
            rbp_1 = rbp_0;
            rdx_2 = rdx_0;
            rbx_1 = rbx_0;
            r12_1 = r12_0;
            local_sp_3 = local_sp_1;
            rdx_1 = rdx_0;
            r8_1 = r8_0;
            r15_1 = r15_0;
            if (r13_0 <= r15_0) {
                if (*(unsigned char *)(local_sp_1 + 62UL) != '\x00') {
                    var_45 = helper_cc_compute_c_wrapper(r13_0 - r15_0, r15_0, var_7, 17U);
                    if (var_45 != 0UL) {
                        loop_state_var = 0U;
                        break;
                    }
                    *(uint64_t *)(local_sp_1 + 16UL) = rdx_0;
                    var_46 = local_sp_1 + (-8L);
                    *(uint64_t *)var_46 = 4201925UL;
                    indirect_placeholder();
                    var_47 = *(uint64_t *)(local_sp_1 + 8UL);
                    local_sp_2 = var_46;
                    rdx_1 = var_47;
                    local_sp_3 = var_46;
                    rdx_2 = var_47;
                    if ((int)(uint32_t)rax_0 >= (int)0U) {
                        loop_state_var = 0U;
                        break;
                    }
                }
            }
            var_51 = *(unsigned char **)6372656UL;
            var_52 = rdx_2 + 1UL;
            var_53 = (r15_0 + rbx_0) + (-1L);
            var_54 = *var_51;
            var_55 = ((uint64_t)var_51 & (-256L)) | (uint64_t)var_54;
            *(unsigned char *)rdx_2 = var_54;
            rcx_0 = var_53;
            rax_1 = var_55;
            local_sp_4 = local_sp_3;
            while (1U)
                {
                    var_56 = (unsigned char *)rcx_0;
                    var_57 = *var_56;
                    var_58 = (r8_1 & (-256L)) | (uint64_t)var_57;
                    var_59 = ((signed char)var_57 > '8');
                    helper_cc_compute_c_wrapper(var_58 + (-56L), 56UL, var_7, 14U);
                    r8_1 = var_58;
                    r8_2 = var_58;
                    if (!var_59) {
                        loop_state_var = 0U;
                        break;
                    }
                    var_61 = rcx_0 + (-1L);
                    *var_56 = (unsigned char)'0';
                    var_62 = var_61 - rbx_0;
                    var_63 = helper_cc_compute_c_wrapper(var_62, rbx_0, var_7, 17U);
                    rcx_0 = var_61;
                    if (var_63 == 0UL) {
                        continue;
                    }
                    loop_state_var = 1U;
                    break;
                }
            switch (loop_state_var) {
              case 0U:
                {
                    var_60 = (uint64_t)((uint32_t)var_58 + 1U);
                    *var_56 = (unsigned char)var_60;
                    r8_2 = var_60;
                }
                break;
              case 1U:
                {
                    var_64 = rbx_0 + (-1L);
                    *(unsigned char *)var_64 = (unsigned char)'1';
                    var_65 = helper_cc_compute_c_wrapper(var_62, rbx_0, var_7, 17U);
                    var_66 = r15_0 + 1UL;
                    helper_cc_compute_c_wrapper(var_66, var_65, var_7, 29U);
                    r15_1 = var_66;
                    rbx_1 = var_64;
                }
                break;
            }
            r15_0 = r15_1;
            r8_0 = r8_2;
            rbx_2 = rbx_1;
            var_67 = rbp_0 << 1UL;
            var_68 = (uint64_t *)(local_sp_3 + 8UL);
            var_69 = *var_68;
            *(uint64_t *)(local_sp_3 + 24UL) = var_52;
            var_70 = var_67 | 1UL;
            var_71 = (uint64_t *)(local_sp_3 + 16UL);
            *var_71 = var_70;
            var_72 = (uint64_t *)(local_sp_3 + (-8L));
            *var_72 = 4201681UL;
            var_73 = indirect_placeholder_3(var_69, var_70);
            var_74 = (uint64_t *)local_sp_3;
            *var_74 = var_73;
            var_75 = local_sp_3 + (-16L);
            *(uint64_t *)var_75 = 4201702UL;
            indirect_placeholder();
            var_76 = *var_74 << 1UL;
            rax_1 = var_73;
            rbx_2 = var_73;
            rbp_1 = var_67;
            local_sp_4 = var_75;
            if (r15_1 != rbp_0 & var_76 > *var_71) {
                *var_74 = (*var_68 - r12_0);
                *var_68 = var_76;
                var_77 = local_sp_3 + (-24L);
                *(uint64_t *)var_77 = 4201749UL;
                var_78 = indirect_placeholder_3(r12_0, var_76);
                var_79 = *var_74;
                var_80 = var_79 + var_78;
                var_81 = *var_72;
                *var_68 = var_79;
                rax_1 = var_81;
                r12_1 = var_78;
                local_sp_4 = var_77;
                r14_1 = var_80;
            }
            var_82 = local_sp_4 + (-8L);
            *(uint64_t *)var_82 = 4201789UL;
            indirect_placeholder();
            var_83 = r14_1 + (r15_1 ^ (-1L));
            rax_0_be = var_83;
            local_sp_1_be = var_82;
            rdx_0_be = rax_1;
            rbx_0 = rbx_2;
            r12_0 = r12_1;
            rbp_0 = rbp_1;
            r14_0 = r14_1;
            if (rax_1 <= var_83) {
                var_84 = rax_1 - r12_1;
                var_85 = local_sp_4 + (-16L);
                *(uint64_t *)var_85 = 4201836UL;
                indirect_placeholder();
                helper_cc_compute_c_wrapper(var_84, r12_1, var_7, 17U);
                local_sp_0 = var_85;
                rax_0_be = 0UL;
                local_sp_1_be = var_85;
                rdx_0_be = r12_1;
                if ((var_83 + (-1L)) != 0UL) {
                    loop_state_var = 1U;
                    break;
                }
            }
            rax_0 = rax_0_be;
            local_sp_1 = local_sp_1_be;
            rdx_0 = rdx_0_be;
            continue;
        }
    switch (loop_state_var) {
      case 1U:
        {
            *(uint64_t *)(local_sp_0 + (-8L)) = 4201846UL;
            indirect_placeholder();
            abort();
        }
        break;
      case 0U:
        {
            var_48 = rdx_1 + 1UL;
            *(unsigned char *)rdx_1 = (unsigned char)'\n';
            var_49 = var_48 - r12_0;
            var_50 = local_sp_2 + (-8L);
            *(uint64_t *)var_50 = 4201900UL;
            indirect_placeholder();
            helper_cc_compute_c_wrapper(var_49, r12_0, var_7, 17U);
            local_sp_0 = var_50;
            local_sp_6 = var_50;
            if ((rax_0 + (-1L)) != 0UL) {
                *(uint64_t *)(local_sp_0 + (-8L)) = 4201846UL;
                indirect_placeholder();
                abort();
            }
        }
        break;
    }
}

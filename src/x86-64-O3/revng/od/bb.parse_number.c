typedef unsigned char __u_char;
typedef unsigned short __u_short;
typedef unsigned int __u_int;
typedef unsigned long __u_long;
typedef signed char __int8_t;
typedef unsigned char __uint8_t;
typedef short __int16_t;
typedef unsigned short __uint16_t;
typedef int __int32_t;
typedef unsigned int __uint32_t;
typedef long __int64_t;
typedef unsigned long __uint64_t;
typedef __int8_t __int_least8_t;
typedef __uint8_t __uint_least8_t;
typedef __int16_t __int_least16_t;
typedef __uint16_t __uint_least16_t;
typedef __int32_t __int_least32_t;
typedef __uint32_t __uint_least32_t;
typedef __int64_t __int_least64_t;
typedef __uint64_t __uint_least64_t;
typedef long __quad_t;
typedef unsigned long __u_quad_t;
typedef long __intmax_t;
typedef unsigned long __uintmax_t;
typedef unsigned long __dev_t;
typedef unsigned int __uid_t;
typedef unsigned int __gid_t;
typedef unsigned long __ino_t;
typedef unsigned long __ino64_t;
typedef unsigned int __mode_t;
typedef unsigned long __nlink_t;
typedef long __off_t;
typedef long __off64_t;
typedef int __pid_t;
typedef struct {
    int __val[2];
} __fsid_t;
typedef long __clock_t;
typedef unsigned long __rlim_t;
typedef unsigned long __rlim64_t;
typedef unsigned int __id_t;
typedef long __time_t;
typedef unsigned int __useconds_t;
typedef long __suseconds_t;
typedef int __daddr_t;
typedef int __key_t;
typedef int __clockid_t;
typedef void *__timer_t;
typedef long __blksize_t;
typedef long __blkcnt_t;
typedef long __blkcnt64_t;
typedef unsigned long __fsblkcnt_t;
typedef unsigned long __fsblkcnt64_t;
typedef unsigned long __fsfilcnt_t;
typedef unsigned long __fsfilcnt64_t;
typedef long __fsword_t;
typedef long __ssize_t;
typedef long __syscall_slong_t;
typedef unsigned long __syscall_ulong_t;
typedef __off64_t __loff_t;
typedef char *__caddr_t;
typedef long __intptr_t;
typedef unsigned int __socklen_t;
typedef int __sig_atomic_t;
typedef __int8_t int8_t;
typedef __int16_t int16_t;
typedef __int32_t int32_t;
typedef __int64_t int64_t;
typedef __uint8_t uint8_t;
typedef __uint16_t uint16_t;
typedef __uint32_t uint32_t;
typedef __uint64_t uint64_t;
typedef __int_least8_t int_least8_t;
typedef __int_least16_t int_least16_t;
typedef __int_least32_t int_least32_t;
typedef __int_least64_t int_least64_t;
typedef __uint_least8_t uint_least8_t;
typedef __uint_least16_t uint_least16_t;
typedef __uint_least32_t uint_least32_t;
typedef __uint_least64_t uint_least64_t;
typedef signed char int_fast8_t;
typedef long int_fast16_t;
typedef long int_fast32_t;
typedef long int_fast64_t;
typedef unsigned char uint_fast8_t;
typedef unsigned long uint_fast16_t;
typedef unsigned long uint_fast32_t;
typedef unsigned long uint_fast64_t;
typedef long intptr_t;
typedef unsigned long uintptr_t;
typedef __intmax_t intmax_t;
typedef __uintmax_t uintmax_t;
typedef _Bool bool;
struct helper_pxor_xmm_wrapper_114_ret_type;
struct type_4;
struct type_6;
struct helper_ucomisd_wrapper_119_ret_type;
struct helper_ucomisd_wrapper_113_ret_type;
struct helper_ucomisd_wrapper_ret_type;
struct helper_mulsd_wrapper_116_ret_type;
struct helper_pxor_xmm_wrapper_ret_type;
struct helper_cvtsi2sd_wrapper_125_ret_type;
struct helper_divsd_wrapper_ret_type;
struct helper_addsd_wrapper_134_ret_type;
struct helper_mulsd_wrapper_173_ret_type;
struct helper_pxor_xmm_wrapper_128_ret_type;
struct helper_cvtsi2sd_wrapper_170_ret_type;
struct helper_divsd_wrapper_171_ret_type;
struct helper_divsd_wrapper_172_ret_type;
struct helper_divsd_wrapper_174_ret_type;
struct helper_mulsd_wrapper_175_ret_type;
struct helper_pxor_xmm_wrapper_114_ret_type {
    uint64_t field_0;
    uint64_t field_1;
};
struct type_4 {
};
struct type_6 {
};
struct helper_ucomisd_wrapper_119_ret_type {
    uint64_t field_0;
    unsigned char field_1;
};
struct helper_ucomisd_wrapper_113_ret_type {
    uint64_t field_0;
    unsigned char field_1;
};
struct helper_ucomisd_wrapper_ret_type {
    uint64_t field_0;
    unsigned char field_1;
};
struct helper_mulsd_wrapper_116_ret_type {
    uint64_t field_0;
    unsigned char field_1;
};
struct helper_pxor_xmm_wrapper_ret_type {
    uint64_t field_0;
    uint64_t field_1;
};
struct helper_cvtsi2sd_wrapper_125_ret_type {
    uint64_t field_0;
};
struct helper_divsd_wrapper_ret_type {
    uint64_t field_0;
    unsigned char field_1;
};
struct helper_addsd_wrapper_134_ret_type {
    uint64_t field_0;
    unsigned char field_1;
};
struct helper_mulsd_wrapper_173_ret_type {
    uint64_t field_0;
    unsigned char field_1;
};
struct helper_pxor_xmm_wrapper_128_ret_type {
    uint64_t field_0;
    uint64_t field_1;
};
struct helper_cvtsi2sd_wrapper_170_ret_type {
    uint64_t field_0;
};
struct helper_divsd_wrapper_171_ret_type {
    uint64_t field_0;
    unsigned char field_1;
};
struct helper_divsd_wrapper_172_ret_type {
    uint64_t field_0;
    unsigned char field_1;
};
struct helper_divsd_wrapper_174_ret_type {
    uint64_t field_0;
    unsigned char field_1;
};
struct helper_mulsd_wrapper_175_ret_type {
    uint64_t field_0;
    unsigned char field_1;
};
extern uint64_t revng_init_local_sp(uint64_t param_0);
extern uint64_t init_rbx(void);
extern uint64_t init_rbp(void);
extern void indirect_placeholder_1(void);
extern unsigned char init_state_0x8549(void);
extern unsigned char init_state_0x854c(void);
extern unsigned char init_state_0x8548(void);
extern unsigned char init_state_0x854b(void);
extern unsigned char init_state_0x8547(void);
extern unsigned char init_state_0x854d(void);
extern struct helper_pxor_xmm_wrapper_114_ret_type helper_pxor_xmm_wrapper_114(struct type_4 *param_0, struct type_6 *param_1, struct type_6 *param_2, uint64_t param_3, uint64_t param_4);
extern struct helper_ucomisd_wrapper_119_ret_type helper_ucomisd_wrapper_119(struct type_4 *param_0, struct type_6 *param_1, struct type_6 *param_2, uint64_t param_3, uint64_t param_4, unsigned char param_5, unsigned char param_6);
extern uint64_t init_cc_src2(void);
extern uint64_t helper_cc_compute_c_wrapper(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint32_t param_3);
extern uint64_t init_r13(void);
extern uint64_t init_r12(void);
extern uint64_t init_r14(void);
extern uint64_t init_state_0x8598(void);
extern uint64_t init_state_0x85a0(void);
extern uint64_t init_state_0x85d8(void);
extern uint64_t init_state_0x85e0(void);
extern struct helper_ucomisd_wrapper_113_ret_type helper_ucomisd_wrapper_113(struct type_4 *param_0, struct type_6 *param_1, struct type_6 *param_2, uint64_t param_3, uint64_t param_4, unsigned char param_5, unsigned char param_6);
extern struct helper_ucomisd_wrapper_ret_type helper_ucomisd_wrapper(struct type_4 *param_0, struct type_6 *param_1, struct type_6 *param_2, uint64_t param_3, uint64_t param_4, unsigned char param_5, unsigned char param_6);
extern struct helper_mulsd_wrapper_116_ret_type helper_mulsd_wrapper_116(struct type_4 *param_0, struct type_6 *param_1, struct type_6 *param_2, uint64_t param_3, uint64_t param_4, unsigned char param_5, unsigned char param_6, unsigned char param_7, unsigned char param_8, unsigned char param_9, unsigned char param_10);
extern uint64_t helper_cc_compute_all_wrapper(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint32_t param_3);
extern struct helper_pxor_xmm_wrapper_ret_type helper_pxor_xmm_wrapper(struct type_4 *param_0, struct type_6 *param_1, struct type_6 *param_2, uint64_t param_3, uint64_t param_4);
extern struct helper_cvtsi2sd_wrapper_125_ret_type helper_cvtsi2sd_wrapper_125(struct type_4 *param_0, struct type_6 *param_1, uint32_t param_2);
extern uint64_t init_r15(void);
extern uint64_t init_state_0x8618(void);
extern uint64_t init_state_0x8620(void);
extern struct helper_divsd_wrapper_ret_type helper_divsd_wrapper(struct type_4 *param_0, struct type_6 *param_1, struct type_6 *param_2, uint64_t param_3, uint64_t param_4, unsigned char param_5, unsigned char param_6, unsigned char param_7, unsigned char param_8, unsigned char param_9, unsigned char param_10);
extern struct helper_addsd_wrapper_134_ret_type helper_addsd_wrapper_134(struct type_4 *param_0, struct type_6 *param_1, struct type_6 *param_2, uint64_t param_3, uint64_t param_4, unsigned char param_5, unsigned char param_6, unsigned char param_7, unsigned char param_8, unsigned char param_9, unsigned char param_10);
extern struct helper_mulsd_wrapper_173_ret_type helper_mulsd_wrapper_173(struct type_4 *param_0, struct type_6 *param_1, struct type_6 *param_2, uint64_t param_3, uint64_t param_4, unsigned char param_5, unsigned char param_6, unsigned char param_7, unsigned char param_8, unsigned char param_9, unsigned char param_10);
extern struct helper_pxor_xmm_wrapper_128_ret_type helper_pxor_xmm_wrapper_128(struct type_4 *param_0, struct type_6 *param_1, struct type_6 *param_2, uint64_t param_3, uint64_t param_4);
extern struct helper_cvtsi2sd_wrapper_170_ret_type helper_cvtsi2sd_wrapper_170(struct type_4 *param_0, struct type_6 *param_1, uint32_t param_2);
extern struct helper_divsd_wrapper_171_ret_type helper_divsd_wrapper_171(struct type_4 *param_0, struct type_6 *param_1, struct type_6 *param_2, uint64_t param_3, uint64_t param_4, unsigned char param_5, unsigned char param_6, unsigned char param_7, unsigned char param_8, unsigned char param_9, unsigned char param_10);
extern struct helper_divsd_wrapper_172_ret_type helper_divsd_wrapper_172(struct type_4 *param_0, struct type_6 *param_1, struct type_6 *param_2, uint64_t param_3, uint64_t param_4, unsigned char param_5, unsigned char param_6, unsigned char param_7, unsigned char param_8, unsigned char param_9, unsigned char param_10);
extern struct helper_divsd_wrapper_174_ret_type helper_divsd_wrapper_174(struct type_4 *param_0, struct type_6 *param_1, struct type_6 *param_2, uint64_t param_3, uint64_t param_4, unsigned char param_5, unsigned char param_6, unsigned char param_7, unsigned char param_8, unsigned char param_9, unsigned char param_10);
extern struct helper_mulsd_wrapper_175_ret_type helper_mulsd_wrapper_175(struct type_4 *param_0, struct type_6 *param_1, struct type_6 *param_2, uint64_t param_3, uint64_t param_4, unsigned char param_5, unsigned char param_6, unsigned char param_7, unsigned char param_8, unsigned char param_9, unsigned char param_10);
uint64_t bb_parse_number(uint64_t rdi, uint64_t r9, uint64_t rcx, uint64_t rdx, uint64_t r8, uint64_t rsi) {
    struct helper_ucomisd_wrapper_113_ret_type var_37;
    struct helper_pxor_xmm_wrapper_114_ret_type var_30;
    uint64_t var_0;
    uint64_t var_1;
    uint64_t var_2;
    uint64_t var_3;
    uint64_t var_4;
    uint64_t var_5;
    uint64_t var_6;
    uint64_t var_7;
    uint64_t var_8;
    uint64_t var_9;
    uint64_t var_10;
    uint64_t var_11;
    uint64_t var_12;
    uint64_t var_13;
    unsigned char var_14;
    unsigned char var_15;
    unsigned char var_16;
    unsigned char var_17;
    unsigned char var_18;
    unsigned char var_19;
    struct helper_pxor_xmm_wrapper_ret_type var_20;
    uint64_t var_21;
    uint32_t var_22;
    uint64_t var_23;
    uint64_t var_24;
    uint32_t var_25;
    uint64_t rbp_2;
    bool var_89;
    uint64_t var_90;
    uint64_t state_0x85a0_2;
    uint64_t state_0x8598_0;
    uint64_t r12_0;
    uint64_t state_0x8558_4;
    uint64_t rdx4_0;
    uint64_t rax_0;
    uint64_t var_91;
    uint64_t rax_7;
    uint64_t r12_6;
    uint64_t var_26;
    uint64_t var_27;
    uint64_t r12_3;
    uint64_t rdi1_1;
    uint64_t rdi1_0;
    uint64_t state_0x8558_2;
    uint64_t r12_1;
    uint64_t rbp_0;
    uint64_t state_0x8598_2;
    uint64_t state_0x8558_0;
    uint64_t state_0x8618_1;
    uint64_t state_0x85a0_0;
    uint64_t state_0x8620_1;
    uint64_t state_0x8618_0;
    unsigned char state_0x8549_2;
    uint64_t state_0x8620_0;
    uint64_t rdx4_3;
    unsigned char state_0x8549_0;
    unsigned char var_28;
    uint64_t var_29;
    uint64_t rdi1_2;
    uint64_t var_31;
    uint64_t var_32;
    struct helper_cvtsi2sd_wrapper_125_ret_type var_33;
    uint64_t var_34;
    struct helper_divsd_wrapper_ret_type var_35;
    uint64_t var_36;
    uint64_t var_38;
    unsigned char var_39;
    uint64_t var_40;
    struct helper_mulsd_wrapper_116_ret_type var_41;
    uint64_t var_42;
    unsigned char var_43;
    struct helper_pxor_xmm_wrapper_114_ret_type var_44;
    uint64_t var_45;
    struct helper_cvtsi2sd_wrapper_125_ret_type var_46;
    uint64_t var_47;
    struct helper_addsd_wrapper_134_ret_type var_48;
    uint64_t r12_2;
    uint64_t state_0x8558_1;
    uint64_t state_0x8598_1;
    uint64_t state_0x85a0_1;
    unsigned char state_0x8549_1;
    uint64_t rdx4_2;
    uint64_t state_0x8558_6;
    unsigned char state_0x8549_5;
    uint64_t var_49;
    uint64_t var_50;
    uint64_t var_51;
    uint64_t r12_9;
    uint64_t r12_4;
    uint64_t rbp_1;
    uint64_t state_0x8558_9;
    uint64_t rdx4_1;
    uint64_t state_0x8598_6;
    uint64_t state_0x8558_3;
    uint64_t state_0x85a0_6;
    uint64_t state_0x8598_3;
    unsigned char state_0x8549_9;
    uint64_t state_0x85a0_3;
    unsigned char state_0x8549_3;
    unsigned char var_52;
    uint64_t var_53;
    uint32_t var_54;
    uint64_t var_55;
    uint32_t var_56;
    uint64_t var_57;
    struct helper_ucomisd_wrapper_ret_type var_118;
    uint64_t var_119;
    unsigned char var_120;
    uint64_t storemerge2_in;
    uint64_t var_71;
    uint64_t var_72;
    uint64_t r12_5;
    uint64_t state_0x8598_4;
    uint64_t state_0x85a0_4;
    uint64_t state_0x8618_2;
    uint64_t state_0x8620_2;
    unsigned char state_0x8549_4;
    uint64_t rax_1;
    uint32_t var_73;
    uint32_t var_74;
    uint64_t var_75;
    uint64_t rax_2;
    uint64_t var_76;
    unsigned char var_77;
    uint64_t var_78;
    uint64_t *var_79;
    uint64_t var_80;
    uint32_t *var_81;
    uint32_t var_82;
    uint64_t *var_83;
    uint64_t var_84;
    uint64_t var_85;
    uint64_t var_86;
    uint64_t *var_87;
    uint64_t var_88;
    uint64_t var_92;
    uint64_t cc_dst_0;
    uint64_t state_0x8558_5;
    uint64_t state_0x85d8_0;
    uint64_t rax_3;
    uint64_t local_sp_0;
    struct helper_ucomisd_wrapper_ret_type var_93;
    uint64_t var_94;
    unsigned char var_95;
    uint64_t cc_src_0;
    uint64_t var_96;
    uint64_t rax_6;
    uint64_t var_97;
    uint64_t var_98;
    struct helper_cvtsi2sd_wrapper_170_ret_type var_99;
    uint64_t var_100;
    struct helper_divsd_wrapper_171_ret_type var_101;
    uint64_t var_102;
    struct helper_ucomisd_wrapper_119_ret_type var_103;
    uint64_t rax_4;
    unsigned char var_104;
    uint64_t var_105;
    uint64_t var_106;
    struct helper_divsd_wrapper_172_ret_type var_107;
    uint64_t var_108;
    struct helper_ucomisd_wrapper_ret_type var_109;
    struct helper_mulsd_wrapper_173_ret_type var_110;
    uint64_t var_111;
    struct helper_ucomisd_wrapper_119_ret_type var_112;
    struct helper_ucomisd_wrapper_ret_type var_113;
    uint64_t var_114;
    struct helper_cvtsi2sd_wrapper_125_ret_type var_115;
    unsigned char state_0x8549_7;
    uint64_t state_0x8558_7;
    unsigned char state_0x8549_6;
    uint64_t rax_5;
    struct helper_divsd_wrapper_174_ret_type var_116;
    uint64_t var_117;
    struct helper_ucomisd_wrapper_113_ret_type var_121;
    uint64_t var_122;
    uint64_t local_sp_1;
    struct helper_ucomisd_wrapper_113_ret_type var_58;
    uint64_t var_59;
    unsigned char var_60;
    uint64_t var_61;
    struct helper_pxor_xmm_wrapper_114_ret_type var_62;
    uint64_t var_63;
    struct helper_mulsd_wrapper_175_ret_type var_64;
    uint64_t var_65;
    unsigned char var_66;
    struct helper_cvtsi2sd_wrapper_125_ret_type var_67;
    uint64_t var_68;
    struct helper_addsd_wrapper_134_ret_type var_69;
    uint64_t r12_8;
    uint64_t state_0x8558_8;
    uint64_t state_0x8598_5;
    uint64_t state_0x85a0_5;
    unsigned char state_0x8549_8;
    uint64_t var_70;
    unsigned int loop_state_var;
    var_0 = revng_init_local_sp(0UL);
    var_1 = init_r15();
    var_2 = init_r13();
    var_3 = init_rbx();
    var_4 = init_r12();
    var_5 = init_rbp();
    var_6 = init_cc_src2();
    var_7 = init_r14();
    var_8 = init_state_0x8598();
    var_9 = init_state_0x85a0();
    var_10 = init_state_0x85d8();
    var_11 = init_state_0x85e0();
    var_12 = init_state_0x8618();
    var_13 = init_state_0x8620();
    var_14 = init_state_0x8549();
    var_15 = init_state_0x854c();
    var_16 = init_state_0x8548();
    var_17 = init_state_0x854b();
    var_18 = init_state_0x8547();
    var_19 = init_state_0x854d();
    *(uint64_t *)(var_0 + (-8L)) = var_1;
    var_20 = helper_pxor_xmm_wrapper((struct type_4 *)(0UL), (struct type_6 *)(904UL), (struct type_6 *)(904UL), var_10, var_11);
    var_21 = var_20.field_0;
    *(uint64_t *)(var_0 + (-16L)) = var_7;
    *(uint64_t *)(var_0 + (-24L)) = var_2;
    *(uint64_t *)(var_0 + (-32L)) = var_4;
    *(uint64_t *)(var_0 + (-40L)) = var_5;
    *(uint64_t *)(var_0 + (-48L)) = var_3;
    var_22 = (uint32_t)rdx;
    var_23 = (uint64_t)var_22;
    var_24 = var_0 + (-104L);
    var_25 = (uint32_t)rsi;
    state_0x8598_0 = var_8;
    rdi1_0 = 0UL;
    r12_1 = 0UL;
    rbp_0 = rdi;
    state_0x8558_0 = var_21;
    state_0x85a0_0 = var_9;
    state_0x8620_1 = 0UL;
    state_0x8618_0 = var_12;
    state_0x8620_0 = var_13;
    state_0x8549_0 = var_14;
    rdx4_2 = 0UL;
    r12_4 = 0UL;
    rbp_1 = rdi;
    rdx4_1 = 0UL;
    state_0x8558_3 = var_21;
    state_0x8598_3 = var_8;
    state_0x85a0_3 = var_9;
    state_0x8549_3 = var_14;
    state_0x8620_2 = 0UL;
    state_0x85d8_0 = var_21;
    local_sp_0 = var_24;
    local_sp_1 = var_24;
    if ((uint64_t)(var_25 + (-16)) != 0UL) {
        var_26 = *(uint64_t *)4291016UL;
        var_27 = (uint64_t)((long)(rcx << 32UL) >> (long)32UL);
        while (1U)
            {
                var_28 = *(unsigned char *)rbp_0;
                var_29 = (uint64_t)(uint32_t)(uint64_t)var_28;
                rbp_2 = rbp_0;
                state_0x85a0_2 = state_0x85a0_0;
                state_0x8558_4 = state_0x8558_0;
                r12_3 = r12_1;
                rdi1_1 = rdi1_0;
                state_0x8558_2 = state_0x8558_0;
                state_0x8598_2 = state_0x8598_0;
                state_0x8618_1 = state_0x8618_0;
                state_0x8549_2 = state_0x8549_0;
                rdi1_2 = var_29;
                r12_2 = r12_1;
                state_0x8558_1 = state_0x8558_0;
                r12_5 = r12_1;
                state_0x8598_4 = state_0x8598_0;
                state_0x85a0_4 = state_0x85a0_0;
                state_0x8618_2 = state_0x8618_0;
                state_0x8620_2 = state_0x8620_0;
                state_0x8549_4 = state_0x8549_0;
                rax_1 = var_29;
                if ((uint64_t)((var_28 + '\xd0') & '\xfe') > 9UL) {
                    rdi1_1 = 1UL;
                    state_0x8620_1 = state_0x8620_0;
                    if ((uint64_t)(unsigned char)rdi1_0 < ((uint64_t)(var_28 + '\xd2') == 0UL)) {
                        break;
                    }
                }
                var_30 = helper_pxor_xmm_wrapper_114((struct type_4 *)(0UL), (struct type_6 *)(840UL), (struct type_6 *)(840UL), state_0x8598_0, state_0x85a0_0);
                var_31 = var_30.field_1;
                var_32 = var_29 + (-48L);
                var_33 = helper_cvtsi2sd_wrapper_125((struct type_4 *)(0UL), (struct type_6 *)(840UL), var_25);
                var_34 = var_33.field_0;
                var_35 = helper_divsd_wrapper((struct type_4 *)(0UL), (struct type_6 *)(968UL), (struct type_6 *)(840UL), var_34, var_26, state_0x8549_0, var_15, var_16, var_17, var_18, var_19);
                var_36 = var_35.field_0;
                var_37 = helper_ucomisd_wrapper_113((struct type_4 *)(0UL), (struct type_6 *)(968UL), (struct type_6 *)(776UL), state_0x8558_0, var_36, var_35.field_1, var_15);
                var_38 = var_37.field_0;
                var_39 = var_37.field_1;
                var_40 = helper_cc_compute_c_wrapper(var_32, var_38, var_6, 1U);
                state_0x8598_1 = var_34;
                state_0x85a0_1 = var_31;
                state_0x8549_1 = var_39;
                state_0x8618_1 = var_36;
                if (var_40 == 0UL) {
                    var_41 = helper_mulsd_wrapper_116((struct type_4 *)(0UL), (struct type_6 *)(776UL), (struct type_6 *)(840UL), state_0x8558_0, var_34, var_39, var_15, var_16, var_17, var_18, var_19);
                    var_42 = var_41.field_0;
                    var_43 = var_41.field_1;
                    var_44 = helper_pxor_xmm_wrapper_114((struct type_4 *)(0UL), (struct type_6 *)(840UL), (struct type_6 *)(840UL), var_34, var_31);
                    var_45 = var_44.field_1;
                    var_46 = helper_cvtsi2sd_wrapper_125((struct type_4 *)(0UL), (struct type_6 *)(840UL), (uint32_t)var_32);
                    var_47 = var_46.field_0;
                    var_48 = helper_addsd_wrapper_134((struct type_4 *)(0UL), (struct type_6 *)(776UL), (struct type_6 *)(840UL), var_42, var_47, var_43, var_15, var_16, var_17, var_18, var_19);
                    state_0x8558_1 = var_48.field_0;
                    state_0x8598_1 = var_47;
                    state_0x85a0_1 = var_45;
                    state_0x8549_1 = var_48.field_1;
                } else {
                    r12_2 = r12_1 + var_27;
                }
                state_0x85a0_2 = state_0x85a0_1;
                r12_3 = r12_2 - (((uint64_t)(unsigned char)rdi1_0 == 0UL) ? 0UL : var_27);
                state_0x8558_2 = state_0x8558_1;
                state_0x8598_2 = state_0x8598_1;
                state_0x8549_2 = state_0x8549_1;
                state_0x8598_0 = state_0x8598_2;
                rdi1_0 = rdi1_1;
                r12_1 = r12_3;
                rbp_0 = rbp_0 + 1UL;
                state_0x8558_0 = state_0x8558_2;
                state_0x85a0_0 = state_0x85a0_2;
                state_0x8618_0 = state_0x8618_1;
                state_0x8620_0 = state_0x8620_1;
                state_0x8549_0 = state_0x8549_2;
                continue;
            }
    }
    var_49 = *(uint64_t *)4291024UL;
    var_50 = *(uint64_t *)4291032UL;
    var_51 = (uint64_t)((long)(rcx << 32UL) >> (long)32UL);
    state_0x8618_2 = var_49;
    while (1U)
        {
            var_52 = *(unsigned char *)rbp_1;
            var_53 = (uint64_t)(uint32_t)(uint64_t)var_52;
            rbp_2 = rbp_1;
            state_0x8558_4 = state_0x8558_3;
            rdx4_3 = rdx4_1;
            r12_9 = r12_4;
            state_0x8558_9 = state_0x8558_3;
            state_0x8598_6 = state_0x8598_3;
            state_0x85a0_6 = state_0x85a0_3;
            state_0x8549_9 = state_0x8549_3;
            r12_5 = r12_4;
            state_0x8598_4 = state_0x8598_3;
            state_0x85a0_4 = state_0x85a0_3;
            state_0x8549_4 = state_0x8549_3;
            rax_1 = var_53;
            r12_8 = r12_4;
            state_0x8558_8 = state_0x8558_3;
            state_0x8598_5 = state_0x8598_3;
            state_0x85a0_5 = state_0x85a0_3;
            if ((uint64_t)((var_52 + '\xd0') & '\xfe') > 9UL) {
                storemerge2_in = var_53 + (-48L);
            } else {
                var_54 = (uint32_t)var_52;
                var_55 = (uint64_t)var_54;
                var_56 = var_54 + (-48);
                var_57 = (uint64_t)var_56;
                rdi1_2 = var_55;
                rdx4_3 = 1UL;
                if (var_57 <= 54UL) {
                    var_71 = (uint64_t)(var_56 & (-256)) | ((uint64_t)(var_52 + '\xd2') == 0UL);
                    var_72 = helper_cc_compute_c_wrapper(rdx4_1 - var_71, var_71, var_6, 14U);
                    if (var_72 == 0UL) {
                        break;
                    }
                    r12_4 = r12_9;
                    rbp_1 = rbp_1 + 1UL;
                    rdx4_1 = rdx4_3;
                    state_0x8558_3 = state_0x8558_9;
                    state_0x8598_3 = state_0x8598_6;
                    state_0x85a0_3 = state_0x85a0_6;
                    state_0x8549_3 = state_0x8549_9;
                    continue;
                }
                if (((1UL << (var_57 & 63UL)) & 35465847073801215UL) != 0UL) {
                    var_71 = (uint64_t)(var_56 & (-256)) | ((uint64_t)(var_52 + '\xd2') == 0UL);
                    var_72 = helper_cc_compute_c_wrapper(rdx4_1 - var_71, var_71, var_6, 14U);
                    if (var_72 == 0UL) {
                        break;
                    }
                    r12_4 = r12_9;
                    rbp_1 = rbp_1 + 1UL;
                    rdx4_1 = rdx4_3;
                    state_0x8558_3 = state_0x8558_9;
                    state_0x8598_3 = state_0x8598_6;
                    state_0x85a0_3 = state_0x85a0_6;
                    state_0x8549_3 = state_0x8549_9;
                    continue;
                }
                storemerge2_in = (uint64_t)(uint32_t)(((uint64_t)((var_52 + '\xbf') & '\xfe') > 25UL) ? var_55 : (var_55 + 32UL)) + (-87L);
            }
            var_58 = helper_ucomisd_wrapper_113((struct type_4 *)(0UL), (struct type_6 *)(968UL), (struct type_6 *)(776UL), state_0x8558_3, var_49, state_0x8549_3, var_15);
            var_59 = var_58.field_0;
            var_60 = var_58.field_1;
            var_61 = helper_cc_compute_c_wrapper(storemerge2_in, var_59, var_6, 1U);
            state_0x8549_8 = var_60;
            if (var_61 == 0UL) {
                var_62 = helper_pxor_xmm_wrapper_114((struct type_4 *)(0UL), (struct type_6 *)(840UL), (struct type_6 *)(840UL), state_0x8598_3, state_0x85a0_3);
                var_63 = var_62.field_1;
                var_64 = helper_mulsd_wrapper_175((struct type_4 *)(0UL), (struct type_6 *)(776UL), (struct type_6 *)(1032UL), state_0x8558_3, var_50, var_60, var_15, var_16, var_17, var_18, var_19);
                var_65 = var_64.field_0;
                var_66 = var_64.field_1;
                var_67 = helper_cvtsi2sd_wrapper_125((struct type_4 *)(0UL), (struct type_6 *)(840UL), (uint32_t)storemerge2_in);
                var_68 = var_67.field_0;
                var_69 = helper_addsd_wrapper_134((struct type_4 *)(0UL), (struct type_6 *)(776UL), (struct type_6 *)(840UL), var_65, var_68, var_66, var_15, var_16, var_17, var_18, var_19);
                state_0x8558_8 = var_69.field_0;
                state_0x8598_5 = var_68;
                state_0x85a0_5 = var_63;
                state_0x8549_8 = var_69.field_1;
            } else {
                r12_8 = r12_4 + var_51;
            }
            var_70 = r12_8 - (((uint64_t)(unsigned char)rdx4_1 == 0UL) ? 0UL : var_51);
            r12_9 = var_70;
            state_0x8558_9 = state_0x8558_8;
            state_0x8598_6 = state_0x8598_5;
            state_0x85a0_6 = state_0x85a0_5;
            state_0x8549_9 = state_0x8549_8;
            r12_4 = r12_9;
            rbp_1 = rbp_1 + 1UL;
            rdx4_1 = rdx4_3;
            state_0x8558_3 = state_0x8558_9;
            state_0x8598_3 = state_0x8598_6;
            state_0x85a0_3 = state_0x85a0_6;
            state_0x8549_3 = state_0x8549_9;
            continue;
        }
    var_73 = (uint32_t)((int)((uint32_t)r8 << 24U) >> (int)24U);
    var_74 = (uint32_t)(((uint64_t)(((unsigned char)rax_1 + '\xbf') & '\xfe') > 25UL) ? rdi1_2 : (rdi1_2 + 32UL));
    var_75 = (uint64_t)var_74;
    r12_0 = r12_5;
    rdx4_0 = rbp_2;
    r12_6 = r12_5;
    rax_2 = var_75;
    state_0x8558_5 = state_0x8558_4;
    if ((uint64_t)(var_73 - var_74) == 0UL) {
        var_76 = rbp_2 + 1UL;
        var_77 = *(unsigned char *)var_76;
        var_78 = (uint64_t)((uint32_t)(uint64_t)var_77 + (-9));
        rax_0 = var_78;
        rax_2 = var_78;
        if (var_78 <= 4UL) {
            var_92 = var_23 + (-2L);
            *(uint64_t *)r9 = rbp_2;
            cc_dst_0 = var_92;
            rax_3 = rax_2;
            rax_7 = rax_2;
            if ((uint64_t)(uint32_t)var_92 == 0UL) {
                *(uint64_t *)(local_sp_1 + (-8L)) = 4236587UL;
                indirect_placeholder_1();
                return rax_7;
            }
        }
        if ((uint64_t)(var_77 + '\xe0') == 0UL) {
            var_92 = var_23 + (-2L);
            *(uint64_t *)r9 = rbp_2;
            cc_dst_0 = var_92;
            rax_3 = rax_2;
            rax_7 = rax_2;
            if ((uint64_t)(uint32_t)var_92 == 0UL) {
                *(uint64_t *)(local_sp_1 + (-8L)) = 4236587UL;
                indirect_placeholder_1();
                return rax_7;
            }
        }
        *(uint64_t *)(var_0 + (-80L)) = var_21;
        var_79 = (uint64_t *)(var_0 + (-88L));
        *var_79 = r9;
        *(uint64_t *)(var_0 + (-96L)) = state_0x8558_4;
        var_80 = var_0 + (-112L);
        *(uint64_t *)var_80 = 4236429UL;
        indirect_placeholder_1();
        var_81 = (uint32_t *)var_78;
        var_82 = *var_81;
        var_83 = (uint64_t *)(var_0 + (-120L));
        *var_83 = 4236450UL;
        indirect_placeholder_1();
        var_84 = var_0 + (-128L);
        *(uint64_t *)var_84 = 4236458UL;
        indirect_placeholder_1();
        var_85 = *var_79;
        *var_81 = var_82;
        var_86 = *var_83;
        var_87 = *(uint64_t **)var_80;
        var_88 = *(uint64_t *)var_24;
        state_0x8558_5 = var_86;
        state_0x85d8_0 = var_88;
        local_sp_0 = var_84;
        local_sp_1 = var_84;
        if (var_76 != var_85) {
            var_89 = ((long)r12_5 < (long)0UL);
            var_90 = var_78 + r12_5;
            rdx4_0 = var_85;
            rax_0 = var_90;
            if (var_89) {
                r12_0 = ((long)var_78 < (long)(9223372036854775808UL - (-9223372036854775808L))) ? 9223372036854775808UL : var_90;
            } else {
                r12_0 = ((long)var_78 > (long)(9223372036854775807UL - r12_5)) ? 9223372036854775807UL : var_90;
            }
        }
        var_91 = var_23 + (-2L);
        *var_87 = rdx4_0;
        r12_6 = r12_0;
        cc_dst_0 = var_91;
        rax_3 = rax_0;
        rax_7 = rax_0;
        if ((uint64_t)(uint32_t)var_91 == 0UL) {
            *(uint64_t *)(local_sp_1 + (-8L)) = 4236587UL;
            indirect_placeholder_1();
            return rax_7;
        }
    }
    var_92 = var_23 + (-2L);
    *(uint64_t *)r9 = rbp_2;
    cc_dst_0 = var_92;
    rax_3 = rax_2;
    rax_7 = rax_2;
    if ((uint64_t)(uint32_t)var_92 == 0UL) {
        *(uint64_t *)(local_sp_1 + (-8L)) = 4236587UL;
        indirect_placeholder_1();
        return rax_7;
    }
    var_93 = helper_ucomisd_wrapper((struct type_4 *)(0UL), (struct type_6 *)(776UL), (struct type_6 *)(904UL), state_0x8558_5, state_0x85d8_0, state_0x8549_4, var_15);
    var_94 = var_93.field_0;
    var_95 = var_93.field_1;
    state_0x8558_6 = state_0x8558_5;
    cc_src_0 = var_94;
    rax_6 = rax_3;
    rax_4 = rax_3;
    state_0x8558_7 = state_0x8558_5;
    state_0x8549_6 = var_95;
    if ((var_94 & 4UL) != 0UL) {
        var_96 = helper_cc_compute_all_wrapper(cc_dst_0, var_94, var_6, 1U);
        cc_src_0 = var_96;
        if ((var_96 & 64UL) == 0UL) {
            return rax_6;
        }
    }
    if ((long)r12_6 < (long)0UL) {
        helper_pxor_xmm_wrapper_114((struct type_4 *)(0UL), (struct type_6 *)(840UL), (struct type_6 *)(840UL), state_0x8598_4, state_0x85a0_4);
        var_114 = r12_6 + 1UL;
        var_115 = helper_cvtsi2sd_wrapper_125((struct type_4 *)(0UL), (struct type_6 *)(840UL), var_22);
        rax_5 = var_114;
        while (1U)
            {
                var_116 = helper_divsd_wrapper_174((struct type_4 *)(0UL), (struct type_6 *)(776UL), (struct type_6 *)(840UL), state_0x8558_7, var_115.field_0, state_0x8549_6, var_15, var_16, var_17, var_18, var_19);
                var_117 = var_116.field_0;
                var_118 = helper_ucomisd_wrapper((struct type_4 *)(0UL), (struct type_6 *)(776UL), (struct type_6 *)(904UL), var_117, state_0x85d8_0, var_116.field_1, var_15);
                var_119 = var_118.field_0;
                var_120 = var_118.field_1;
                state_0x8558_7 = var_117;
                state_0x8549_7 = var_120;
                rax_6 = rax_5;
                if ((var_119 & 4UL) == 0UL) {
                    var_122 = rax_5 + 1UL;
                    state_0x8549_6 = state_0x8549_7;
                    rax_5 = var_122;
                    rax_6 = var_122;
                    if (rax_5 != 0UL) {
                        continue;
                    }
                    break;
                }
                var_121 = helper_ucomisd_wrapper_113((struct type_4 *)(0UL), (struct type_6 *)(776UL), (struct type_6 *)(968UL), var_117, state_0x85d8_0, var_120, var_15);
                state_0x8549_7 = var_121.field_1;
                if ((var_121.field_0 & 64UL) != 0UL) {
                    *(uint64_t *)(local_sp_0 + 8UL) = var_117;
                    *(uint64_t *)(local_sp_0 + (-8L)) = 4236931UL;
                    indirect_placeholder_1();
                    *(uint32_t *)rax_5 = 34U;
                    break;
                }
            }
    }
    var_97 = helper_cc_compute_all_wrapper(r12_6, cc_src_0, var_6, 25U);
    if ((var_97 & 64UL) == 0UL) {
        return;
    }
    helper_pxor_xmm_wrapper_128((struct type_4 *)(0UL), (struct type_6 *)(968UL), (struct type_6 *)(968UL), state_0x8618_2, state_0x8620_2);
    var_98 = *(uint64_t *)4291040UL;
    var_99 = helper_cvtsi2sd_wrapper_170((struct type_4 *)(0UL), (struct type_6 *)(968UL), var_22);
    var_100 = var_99.field_0;
    var_101 = helper_divsd_wrapper_171((struct type_4 *)(0UL), (struct type_6 *)(840UL), (struct type_6 *)(968UL), var_98, var_100, var_95, var_15, var_16, var_17, var_18, var_19);
    var_102 = var_101.field_0;
    var_103 = helper_ucomisd_wrapper_119((struct type_4 *)(0UL), (struct type_6 *)(840UL), (struct type_6 *)(776UL), state_0x8558_5, var_102, var_101.field_1, var_15);
    if ((var_103.field_0 & 65UL) != 0UL) {
        *(uint64_t *)(local_sp_0 + (-8L)) = 4236837UL;
        indirect_placeholder_1();
        *(uint32_t *)rax_4 = 34U;
        rax_6 = rax_4;
        return rax_6;
    }
    var_104 = var_103.field_1;
    var_105 = *(uint64_t *)4291016UL;
    var_106 = r12_6 + (-1L);
    var_107 = helper_divsd_wrapper_172((struct type_4 *)(0UL), (struct type_6 *)(904UL), (struct type_6 *)(968UL), var_105, var_100, var_104, var_15, var_16, var_17, var_18, var_19);
    var_108 = var_107.field_0;
    var_109 = helper_ucomisd_wrapper((struct type_4 *)(0UL), (struct type_6 *)(776UL), (struct type_6 *)(904UL), state_0x8558_5, var_108, var_107.field_1, var_15);
    rax_4 = var_106;
    rax_6 = var_106;
    if ((var_109.field_0 & 65UL) != 0UL) {
        *(uint64_t *)(local_sp_0 + (-8L)) = 4236869UL;
        indirect_placeholder_1();
        *(uint32_t *)var_106 = 34U;
        return rax_6;
    }
    state_0x8549_5 = var_109.field_1;
    while (1U)
        {
            var_110 = helper_mulsd_wrapper_173((struct type_4 *)(0UL), (struct type_6 *)(776UL), (struct type_6 *)(968UL), state_0x8558_6, var_100, state_0x8549_5, var_15, var_16, var_17, var_18, var_19);
            var_111 = var_110.field_0;
            state_0x8558_6 = var_111;
            if (rdx4_2 != var_106) {
                loop_state_var = 0U;
                break;
            }
            var_112 = helper_ucomisd_wrapper_119((struct type_4 *)(0UL), (struct type_6 *)(840UL), (struct type_6 *)(776UL), var_111, var_102, var_110.field_1, var_15);
            if ((var_112.field_0 & 65UL) != 0UL) {
                loop_state_var = 2U;
                break;
            }
            var_113 = helper_ucomisd_wrapper((struct type_4 *)(0UL), (struct type_6 *)(776UL), (struct type_6 *)(904UL), var_111, var_108, var_112.field_1, var_15);
            if ((var_113.field_0 & 65UL) != 0UL) {
                loop_state_var = 1U;
                break;
            }
            rdx4_2 = rdx4_2 + 1UL;
            state_0x8549_5 = var_113.field_1;
            continue;
        }
    switch (loop_state_var) {
      case 0U:
        {
            break;
        }
        break;
      case 1U:
        {
            *(uint64_t *)(local_sp_0 + (-8L)) = 4236869UL;
            indirect_placeholder_1();
            *(uint32_t *)var_106 = 34U;
        }
        break;
      case 2U:
        {
            *(uint64_t *)(local_sp_0 + (-8L)) = 4236837UL;
            indirect_placeholder_1();
            *(uint32_t *)rax_4 = 34U;
            rax_6 = rax_4;
        }
        break;
    }
}

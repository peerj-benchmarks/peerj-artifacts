typedef unsigned char __u_char;
typedef unsigned short __u_short;
typedef unsigned int __u_int;
typedef unsigned long __u_long;
typedef signed char __int8_t;
typedef unsigned char __uint8_t;
typedef short __int16_t;
typedef unsigned short __uint16_t;
typedef int __int32_t;
typedef unsigned int __uint32_t;
typedef long __int64_t;
typedef unsigned long __uint64_t;
typedef __int8_t __int_least8_t;
typedef __uint8_t __uint_least8_t;
typedef __int16_t __int_least16_t;
typedef __uint16_t __uint_least16_t;
typedef __int32_t __int_least32_t;
typedef __uint32_t __uint_least32_t;
typedef __int64_t __int_least64_t;
typedef __uint64_t __uint_least64_t;
typedef long __quad_t;
typedef unsigned long __u_quad_t;
typedef long __intmax_t;
typedef unsigned long __uintmax_t;
typedef unsigned long __dev_t;
typedef unsigned int __uid_t;
typedef unsigned int __gid_t;
typedef unsigned long __ino_t;
typedef unsigned long __ino64_t;
typedef unsigned int __mode_t;
typedef unsigned long __nlink_t;
typedef long __off_t;
typedef long __off64_t;
typedef int __pid_t;
typedef struct {
    int __val[2];
} __fsid_t;
typedef long __clock_t;
typedef unsigned long __rlim_t;
typedef unsigned long __rlim64_t;
typedef unsigned int __id_t;
typedef long __time_t;
typedef unsigned int __useconds_t;
typedef long __suseconds_t;
typedef int __daddr_t;
typedef int __key_t;
typedef int __clockid_t;
typedef void *__timer_t;
typedef long __blksize_t;
typedef long __blkcnt_t;
typedef long __blkcnt64_t;
typedef unsigned long __fsblkcnt_t;
typedef unsigned long __fsblkcnt64_t;
typedef unsigned long __fsfilcnt_t;
typedef unsigned long __fsfilcnt64_t;
typedef long __fsword_t;
typedef long __ssize_t;
typedef long __syscall_slong_t;
typedef unsigned long __syscall_ulong_t;
typedef __off64_t __loff_t;
typedef char *__caddr_t;
typedef long __intptr_t;
typedef unsigned int __socklen_t;
typedef int __sig_atomic_t;
typedef __int8_t int8_t;
typedef __int16_t int16_t;
typedef __int32_t int32_t;
typedef __int64_t int64_t;
typedef __uint8_t uint8_t;
typedef __uint16_t uint16_t;
typedef __uint32_t uint32_t;
typedef __uint64_t uint64_t;
typedef __int_least8_t int_least8_t;
typedef __int_least16_t int_least16_t;
typedef __int_least32_t int_least32_t;
typedef __int_least64_t int_least64_t;
typedef __uint_least8_t uint_least8_t;
typedef __uint_least16_t uint_least16_t;
typedef __uint_least32_t uint_least32_t;
typedef __uint_least64_t uint_least64_t;
typedef signed char int_fast8_t;
typedef long int_fast16_t;
typedef long int_fast32_t;
typedef long int_fast64_t;
typedef unsigned char uint_fast8_t;
typedef unsigned long uint_fast16_t;
typedef unsigned long uint_fast32_t;
typedef unsigned long uint_fast64_t;
typedef long intptr_t;
typedef unsigned long uintptr_t;
typedef __intmax_t intmax_t;
typedef __uintmax_t uintmax_t;
struct helper_divq_EAX_wrapper_ret_type;
struct type_5;
struct helper_divq_EAX_wrapper_ret_type {
    uint64_t field_0;
    uint64_t field_1;
    uint32_t field_2;
    uint64_t field_3;
    uint64_t field_4;
    uint32_t field_5;
    uint64_t field_6;
    uint32_t field_7;
    uint64_t field_8;
    uint32_t field_9;
    unsigned char field_10;
    uint32_t field_11;
    uint32_t field_12;
};
struct type_5 {
};
extern void abort(void);
extern uint64_t revng_init_local_sp(uint64_t param_0);
extern uint64_t init_rbx(void);
extern uint64_t init_rbp(void);
extern void indirect_placeholder_1(void);
extern uint64_t init_cc_src2(void);
extern uint64_t helper_cc_compute_c_wrapper(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint32_t param_3);
extern uint64_t init_r13(void);
extern uint64_t init_r12(void);
extern uint64_t init_r14(void);
extern uint64_t init_r15(void);
extern struct helper_divq_EAX_wrapper_ret_type helper_divq_EAX_wrapper(struct type_5 *param_0, uint64_t param_1, uint64_t param_2, uint64_t param_3, uint64_t param_4, uint64_t param_5, uint64_t param_6, uint64_t param_7, uint64_t param_8, uint64_t param_9, uint64_t param_10, uint64_t param_11, uint64_t param_12, uint32_t param_13, uint64_t param_14, uint32_t param_15, uint64_t param_16, uint64_t param_17, uint64_t param_18, uint32_t param_19);
extern uint32_t init_state_0x8248(void);
extern uint64_t init_state_0x9018(void);
extern uint32_t init_state_0x9010(void);
extern uint64_t init_state_0x8408(void);
extern uint64_t init_state_0x8328(void);
extern uint64_t init_state_0x82d8(void);
extern uint32_t init_state_0x9080(void);
extern uint64_t indirect_placeholder_52(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint64_t param_3, uint64_t param_4, uint64_t param_5, uint64_t param_6);
void bb_print_ascii(uint64_t r10, uint64_t rdi, uint64_t r9, uint64_t rcx, uint64_t rdx, uint64_t r8, uint64_t rsi) {
    uint32_t var_32;
    uint32_t var_33;
    uint32_t var_34;
    uint32_t var_35;
    uint64_t var_36;
    uint64_t var_37;
    uint64_t var_38;
    uint64_t *var_39;
    uint64_t var_40;
    uint64_t var_41;
    uint64_t var_30;
    uint64_t var_31;
    uint64_t r93_0;
    uint64_t var_0;
    uint64_t var_1;
    uint64_t var_2;
    uint64_t var_3;
    uint64_t var_4;
    uint64_t var_5;
    uint64_t var_6;
    uint64_t var_7;
    uint32_t var_8;
    uint64_t var_9;
    uint32_t var_10;
    uint64_t var_11;
    uint64_t var_12;
    uint64_t var_13;
    uint32_t var_14;
    uint64_t var_15;
    uint64_t rdi2_0;
    uint64_t rbx_0;
    uint64_t var_16;
    uint64_t var_17;
    uint64_t var_18;
    uint64_t rbp_0;
    uint64_t r15_0;
    uint64_t r14_0;
    uint64_t rsi7_0;
    uint32_t state_0x8248_0;
    uint64_t state_0x9018_0;
    uint32_t state_0x9010_0;
    uint64_t local_sp_0;
    uint64_t state_0x82d8_0;
    uint32_t state_0x9080_0;
    uint64_t var_19;
    uint64_t var_21;
    struct helper_divq_EAX_wrapper_ret_type var_20;
    uint32_t var_22;
    uint64_t var_23;
    uint32_t var_24;
    uint64_t var_25;
    uint32_t var_26;
    unsigned char var_27;
    uint64_t var_28;
    uint64_t var_29;
    uint64_t r93_1;
    uint64_t rdx5_0;
    uint64_t local_sp_1;
    var_0 = revng_init_local_sp(0UL);
    var_1 = init_r15();
    var_2 = init_r13();
    var_3 = init_rbx();
    var_4 = init_r12();
    var_5 = init_rbp();
    var_6 = init_cc_src2();
    var_7 = init_r14();
    var_8 = init_state_0x8248();
    var_9 = init_state_0x9018();
    var_10 = init_state_0x9010();
    var_11 = init_state_0x8408();
    var_12 = init_state_0x8328();
    var_13 = init_state_0x82d8();
    var_14 = init_state_0x9080();
    *(uint64_t *)(var_0 + (-8L)) = var_1;
    *(uint64_t *)(var_0 + (-16L)) = var_7;
    *(uint64_t *)(var_0 + (-24L)) = var_2;
    *(uint64_t *)(var_0 + (-32L)) = var_4;
    *(uint64_t *)(var_0 + (-40L)) = var_5;
    *(uint64_t *)(var_0 + (-48L)) = var_3;
    var_15 = var_0 + (-88L);
    *(uint32_t *)(var_0 + (-80L)) = (uint32_t)r8;
    r93_0 = r9;
    rdi2_0 = rdi;
    rbp_0 = var_5;
    r15_0 = rdi;
    r14_0 = rdx;
    rsi7_0 = rsi;
    state_0x8248_0 = var_8;
    state_0x9018_0 = var_9;
    state_0x9010_0 = var_10;
    local_sp_0 = var_15;
    state_0x82d8_0 = var_13;
    state_0x9080_0 = var_14;
    rdx5_0 = 4278588UL;
    if (rdi > rsi) {
        return;
    }
    var_16 = rdi + (-1L);
    var_17 = (uint64_t)((long)(r9 << 32UL) >> (long)32UL);
    *(uint64_t *)var_15 = var_17;
    rbx_0 = var_17 * var_16;
    while (1U)
        {
            var_18 = r14_0 + 1UL;
            var_19 = r15_0 + (-1L);
            var_20 = helper_divq_EAX_wrapper((struct type_5 *)(0UL), rdi, 4209141UL, rbx_0, rbx_0, r10, rdi2_0, r93_0, rbp_0, rcx, 0UL, r8, rsi7_0, state_0x8248_0, state_0x9018_0, state_0x9010_0, var_11, var_12, state_0x82d8_0, state_0x9080_0);
            var_21 = var_20.field_1;
            var_22 = var_20.field_5;
            var_23 = var_20.field_6;
            var_24 = var_20.field_7;
            var_25 = var_20.field_8;
            var_26 = var_20.field_9;
            var_27 = *(unsigned char *)r14_0;
            var_28 = (uint64_t)var_27;
            rdi2_0 = 4278609UL;
            rbp_0 = var_21;
            r15_0 = var_19;
            r14_0 = var_18;
            state_0x8248_0 = var_22;
            state_0x9018_0 = var_23;
            state_0x9010_0 = var_24;
            state_0x82d8_0 = var_25;
            state_0x9080_0 = var_26;
            r93_1 = r93_0;
            local_sp_1 = local_sp_0;
            if (var_27 <= '\r') {
                var_30 = (uint64_t)((uint32_t)var_28 + (-32));
                *(uint32_t *)(local_sp_0 + 12UL) = (uint32_t)r93_0;
                helper_cc_compute_c_wrapper(var_30 + (-95L), 95UL, var_6, 16U);
                var_31 = local_sp_0 + (-8L);
                *(uint64_t *)var_31 = 4209381UL;
                indirect_placeholder_1();
                r93_1 = (uint64_t)*(uint32_t *)(local_sp_0 + 4UL);
                rdx5_0 = local_sp_0 + 8UL;
                local_sp_1 = var_31;
                var_32 = *(uint32_t *)(local_sp_1 + 8UL);
                var_33 = (uint32_t)r93_1;
                var_34 = (uint32_t)var_21;
                var_35 = var_33 - var_34;
                var_36 = (uint64_t)var_35;
                var_37 = (uint64_t)(var_35 + var_32);
                var_38 = local_sp_1 + (-8L);
                var_39 = (uint64_t *)var_38;
                *var_39 = 4209207UL;
                indirect_placeholder_52(0UL, 4278609UL, var_36, rcx, rdx5_0, r8, var_37);
                var_40 = *var_39;
                var_41 = helper_cc_compute_c_wrapper(rsi - var_19, var_19, var_6, 17U);
                rsi7_0 = var_37;
                local_sp_0 = var_38;
                if (var_41 == 0UL) {
                    break;
                }
                rbx_0 = rbx_0 - var_40;
                r93_0 = (uint64_t)var_34;
                continue;
            }
            var_29 = *(uint64_t *)((var_28 << 3UL) + 4283504UL) + (-4209176L);
            switch ((var_29 >> 3UL) | (var_29 << 61UL)) {
              case 17UL:
                {
                    rdx5_0 = 4278591UL;
                }
                break;
              case 9UL:
                {
                    rdx5_0 = 4278594UL;
                }
                break;
              case 0UL:
                {
                    rdx5_0 = 4278585UL;
                }
                break;
              case 15UL:
                {
                    rdx5_0 = 4278603UL;
                }
                break;
              case 11UL:
                {
                    rdx5_0 = 4278606UL;
                }
                break;
              case 13UL:
                {
                    rdx5_0 = 4278597UL;
                }
                break;
              case 21UL:
                {
                    var_30 = (uint64_t)((uint32_t)var_28 + (-32));
                    *(uint32_t *)(local_sp_0 + 12UL) = (uint32_t)r93_0;
                    helper_cc_compute_c_wrapper(var_30 + (-95L), 95UL, var_6, 16U);
                    var_31 = local_sp_0 + (-8L);
                    *(uint64_t *)var_31 = 4209381UL;
                    indirect_placeholder_1();
                    r93_1 = (uint64_t)*(uint32_t *)(local_sp_0 + 4UL);
                    rdx5_0 = local_sp_0 + 8UL;
                    local_sp_1 = var_31;
                }
                break;
              case 8UL:
                {
                    rdx5_0 = 4278600UL;
                }
                break;
              case 19UL:
                {
                    break;
                }
                break;
              default:
                {
                    abort();
                }
                break;
            }
        }
    return;
}

typedef unsigned char __u_char;
typedef unsigned short __u_short;
typedef unsigned int __u_int;
typedef unsigned long __u_long;
typedef signed char __int8_t;
typedef unsigned char __uint8_t;
typedef short __int16_t;
typedef unsigned short __uint16_t;
typedef int __int32_t;
typedef unsigned int __uint32_t;
typedef long __int64_t;
typedef unsigned long __uint64_t;
typedef __int8_t __int_least8_t;
typedef __uint8_t __uint_least8_t;
typedef __int16_t __int_least16_t;
typedef __uint16_t __uint_least16_t;
typedef __int32_t __int_least32_t;
typedef __uint32_t __uint_least32_t;
typedef __int64_t __int_least64_t;
typedef __uint64_t __uint_least64_t;
typedef long __quad_t;
typedef unsigned long __u_quad_t;
typedef long __intmax_t;
typedef unsigned long __uintmax_t;
typedef unsigned long __dev_t;
typedef unsigned int __uid_t;
typedef unsigned int __gid_t;
typedef unsigned long __ino_t;
typedef unsigned long __ino64_t;
typedef unsigned int __mode_t;
typedef unsigned long __nlink_t;
typedef long __off_t;
typedef long __off64_t;
typedef int __pid_t;
typedef struct {
    int __val[2];
} __fsid_t;
typedef long __clock_t;
typedef unsigned long __rlim_t;
typedef unsigned long __rlim64_t;
typedef unsigned int __id_t;
typedef long __time_t;
typedef unsigned int __useconds_t;
typedef long __suseconds_t;
typedef int __daddr_t;
typedef int __key_t;
typedef int __clockid_t;
typedef void *__timer_t;
typedef long __blksize_t;
typedef long __blkcnt_t;
typedef long __blkcnt64_t;
typedef unsigned long __fsblkcnt_t;
typedef unsigned long __fsblkcnt64_t;
typedef unsigned long __fsfilcnt_t;
typedef unsigned long __fsfilcnt64_t;
typedef long __fsword_t;
typedef long __ssize_t;
typedef long __syscall_slong_t;
typedef unsigned long __syscall_ulong_t;
typedef __off64_t __loff_t;
typedef char *__caddr_t;
typedef long __intptr_t;
typedef unsigned int __socklen_t;
typedef int __sig_atomic_t;
typedef __int8_t int8_t;
typedef __int16_t int16_t;
typedef __int32_t int32_t;
typedef __int64_t int64_t;
typedef __uint8_t uint8_t;
typedef __uint16_t uint16_t;
typedef __uint32_t uint32_t;
typedef __uint64_t uint64_t;
typedef __int_least8_t int_least8_t;
typedef __int_least16_t int_least16_t;
typedef __int_least32_t int_least32_t;
typedef __int_least64_t int_least64_t;
typedef __uint_least8_t uint_least8_t;
typedef __uint_least16_t uint_least16_t;
typedef __uint_least32_t uint_least32_t;
typedef __uint_least64_t uint_least64_t;
typedef signed char int_fast8_t;
typedef long int_fast16_t;
typedef long int_fast32_t;
typedef long int_fast64_t;
typedef unsigned char uint_fast8_t;
typedef unsigned long uint_fast16_t;
typedef unsigned long uint_fast32_t;
typedef unsigned long uint_fast64_t;
typedef long intptr_t;
typedef unsigned long uintptr_t;
typedef __intmax_t intmax_t;
typedef __uintmax_t uintmax_t;
extern uint64_t revng_init_local_sp(uint64_t param_0);
extern uint64_t init_rbx(void);
extern uint64_t init_rbp(void);
extern uint64_t init_cc_src2(void);
extern uint64_t helper_cc_compute_all_wrapper(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint32_t param_3);
extern void indirect_placeholder_12(uint64_t param_0, uint64_t param_1);
void bb_append_quoted(uint64_t rdi) {
    uint64_t var_0;
    uint64_t var_1;
    uint64_t var_2;
    uint64_t var_3;
    unsigned char var_4;
    uint64_t rax_5;
    unsigned char rax_0_in;
    uint64_t rbx_0;
    uint64_t local_sp_5;
    uint64_t rbp_0;
    uint64_t local_sp_0;
    uint64_t var_5;
    uint64_t var_6;
    uint64_t rbp_1;
    uint64_t var_7;
    uint64_t rax_1;
    uint64_t local_sp_1;
    uint64_t var_9;
    uint64_t rax_2;
    uint64_t local_sp_2;
    uint64_t var_11;
    uint64_t rax_3;
    uint64_t local_sp_3;
    uint64_t var_12;
    uint64_t var_10;
    uint64_t var_8;
    uint64_t var_13;
    uint64_t rax_4;
    uint64_t local_sp_4;
    uint64_t var_14;
    uint64_t var_15;
    uint64_t local_sp_6;
    uint64_t var_17;
    uint64_t var_18;
    unsigned char var_19;
    uint64_t var_16;
    var_0 = revng_init_local_sp(0UL);
    var_1 = init_rbx();
    var_2 = init_rbp();
    var_3 = init_cc_src2();
    *(uint64_t *)(var_0 + (-8L)) = var_2;
    *(uint64_t *)(var_0 + (-16L)) = var_1;
    var_4 = *(unsigned char *)rdi;
    rax_0_in = var_4;
    rbx_0 = rdi;
    rbp_0 = 1UL;
    rbp_1 = 1UL;
    if (var_4 == '\x00') {
        return;
    }
    local_sp_0 = var_0 + (-24L);
    var_17 = rax_5 + 1UL;
    var_18 = rbx_0 + 1UL;
    *(uint64_t *)6392088UL = var_17;
    *(unsigned char *)rax_5 = *(unsigned char *)rbx_0;
    var_19 = *(unsigned char *)var_18;
    rax_0_in = var_19;
    rbx_0 = var_18;
    local_sp_0 = local_sp_6;
    do {
        var_5 = (uint64_t)rax_0_in + (-61L);
        local_sp_1 = local_sp_0;
        local_sp_4 = local_sp_0;
        local_sp_5 = local_sp_0;
        if ((uint64_t)(unsigned char)var_5 == 0UL) {
            var_6 = helper_cc_compute_all_wrapper(var_5, 61UL, var_3, 14U);
            if ((uint64_t)(((unsigned char)(var_6 >> 4UL) ^ (unsigned char)var_6) & '\xc0') == 0UL) {
                if ((uint64_t)(rax_0_in + '\xa4') == 0UL) {
                    rbp_1 = (uint64_t)(uint32_t)rbp_0 ^ 1UL;
                } else {
                    if ((uint64_t)(rax_0_in + '\xa2') == 0UL) {
                        rbp_1 = (uint64_t)(uint32_t)rbp_0 ^ 1UL;
                    }
                }
            } else {
                if ((uint64_t)(rax_0_in + '\xd9') == 0UL) {
                    var_7 = *(uint64_t *)6392088UL;
                    rax_1 = var_7;
                    if (*(uint64_t *)6392096UL == var_7) {
                        var_8 = local_sp_0 + (-8L);
                        *(uint64_t *)var_8 = 4203455UL;
                        indirect_placeholder_12(6392064UL, 1UL);
                        rax_1 = *(uint64_t *)6392088UL;
                        local_sp_1 = var_8;
                    }
                    *(uint64_t *)6392088UL = (rax_1 + 1UL);
                    *(unsigned char *)rax_1 = (unsigned char)'\'';
                    var_9 = *(uint64_t *)6392088UL;
                    rax_2 = var_9;
                    local_sp_2 = local_sp_1;
                    if (*(uint64_t *)6392096UL == var_9) {
                        var_10 = local_sp_1 + (-8L);
                        *(uint64_t *)var_10 = 4203519UL;
                        indirect_placeholder_12(6392064UL, 1UL);
                        rax_2 = *(uint64_t *)6392088UL;
                        local_sp_2 = var_10;
                    }
                    *(uint64_t *)6392088UL = (rax_2 + 1UL);
                    *(unsigned char *)rax_2 = (unsigned char)'\\';
                    var_11 = *(uint64_t *)6392088UL;
                    rax_3 = var_11;
                    local_sp_3 = local_sp_2;
                    if (*(uint64_t *)6392096UL == var_11) {
                        var_12 = local_sp_2 + (-8L);
                        *(uint64_t *)var_12 = 4203487UL;
                        indirect_placeholder_12(6392064UL, 1UL);
                        rax_3 = *(uint64_t *)6392088UL;
                        local_sp_3 = var_12;
                    }
                    *(uint64_t *)6392088UL = (rax_3 + 1UL);
                    *(unsigned char *)rax_3 = (unsigned char)'\'';
                    local_sp_5 = local_sp_3;
                } else {
                    if ((uint64_t)(rax_0_in + '\xc6') != 0UL & (uint64_t)(unsigned char)rbp_0 != 0UL) {
                        var_13 = *(uint64_t *)6392088UL;
                        rax_4 = var_13;
                        rbp_1 = rbp_0;
                        if (*(uint64_t *)6392096UL == var_13) {
                            var_14 = local_sp_0 + (-8L);
                            *(uint64_t *)var_14 = 4203551UL;
                            indirect_placeholder_12(6392064UL, 1UL);
                            rax_4 = *(uint64_t *)6392088UL;
                            local_sp_4 = var_14;
                        }
                        *(uint64_t *)6392088UL = (rax_4 + 1UL);
                        *(unsigned char *)rax_4 = (unsigned char)'\\';
                        local_sp_5 = local_sp_4;
                    }
                }
            }
        } else {
            if ((uint64_t)(unsigned char)rbp_0 != 0UL) {
                var_13 = *(uint64_t *)6392088UL;
                rax_4 = var_13;
                rbp_1 = rbp_0;
                if (*(uint64_t *)6392096UL == var_13) {
                    var_14 = local_sp_0 + (-8L);
                    *(uint64_t *)var_14 = 4203551UL;
                    indirect_placeholder_12(6392064UL, 1UL);
                    rax_4 = *(uint64_t *)6392088UL;
                    local_sp_4 = var_14;
                }
                *(uint64_t *)6392088UL = (rax_4 + 1UL);
                *(unsigned char *)rax_4 = (unsigned char)'\\';
                local_sp_5 = local_sp_4;
            }
        }
        var_15 = *(uint64_t *)6392088UL;
        rbp_0 = rbp_1;
        rax_5 = var_15;
        local_sp_6 = local_sp_5;
        if (*(uint64_t *)6392096UL == var_15) {
            var_16 = local_sp_5 + (-8L);
            *(uint64_t *)var_16 = 4203407UL;
            indirect_placeholder_12(6392064UL, 1UL);
            rax_5 = *(uint64_t *)6392088UL;
            local_sp_6 = var_16;
        }
        var_17 = rax_5 + 1UL;
        var_18 = rbx_0 + 1UL;
        *(uint64_t *)6392088UL = var_17;
        *(unsigned char *)rax_5 = *(unsigned char *)rbx_0;
        var_19 = *(unsigned char *)var_18;
        rax_0_in = var_19;
        rbx_0 = var_18;
        local_sp_0 = local_sp_6;
    } while (var_19 != '\x00');
    return;
}

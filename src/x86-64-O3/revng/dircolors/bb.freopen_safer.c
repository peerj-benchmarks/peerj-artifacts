typedef unsigned char __u_char;
typedef unsigned short __u_short;
typedef unsigned int __u_int;
typedef unsigned long __u_long;
typedef signed char __int8_t;
typedef unsigned char __uint8_t;
typedef short __int16_t;
typedef unsigned short __uint16_t;
typedef int __int32_t;
typedef unsigned int __uint32_t;
typedef long __int64_t;
typedef unsigned long __uint64_t;
typedef __int8_t __int_least8_t;
typedef __uint8_t __uint_least8_t;
typedef __int16_t __int_least16_t;
typedef __uint16_t __uint_least16_t;
typedef __int32_t __int_least32_t;
typedef __uint32_t __uint_least32_t;
typedef __int64_t __int_least64_t;
typedef __uint64_t __uint_least64_t;
typedef long __quad_t;
typedef unsigned long __u_quad_t;
typedef long __intmax_t;
typedef unsigned long __uintmax_t;
typedef unsigned long __dev_t;
typedef unsigned int __uid_t;
typedef unsigned int __gid_t;
typedef unsigned long __ino_t;
typedef unsigned long __ino64_t;
typedef unsigned int __mode_t;
typedef unsigned long __nlink_t;
typedef long __off_t;
typedef long __off64_t;
typedef int __pid_t;
typedef struct {
    int __val[2];
} __fsid_t;
typedef long __clock_t;
typedef unsigned long __rlim_t;
typedef unsigned long __rlim64_t;
typedef unsigned int __id_t;
typedef long __time_t;
typedef unsigned int __useconds_t;
typedef long __suseconds_t;
typedef int __daddr_t;
typedef int __key_t;
typedef int __clockid_t;
typedef void *__timer_t;
typedef long __blksize_t;
typedef long __blkcnt_t;
typedef long __blkcnt64_t;
typedef unsigned long __fsblkcnt_t;
typedef unsigned long __fsblkcnt64_t;
typedef unsigned long __fsfilcnt_t;
typedef unsigned long __fsfilcnt64_t;
typedef long __fsword_t;
typedef long __ssize_t;
typedef long __syscall_slong_t;
typedef unsigned long __syscall_ulong_t;
typedef __off64_t __loff_t;
typedef char *__caddr_t;
typedef long __intptr_t;
typedef unsigned int __socklen_t;
typedef int __sig_atomic_t;
typedef __int8_t int8_t;
typedef __int16_t int16_t;
typedef __int32_t int32_t;
typedef __int64_t int64_t;
typedef __uint8_t uint8_t;
typedef __uint16_t uint16_t;
typedef __uint32_t uint32_t;
typedef __uint64_t uint64_t;
typedef __int_least8_t int_least8_t;
typedef __int_least16_t int_least16_t;
typedef __int_least32_t int_least32_t;
typedef __int_least64_t int_least64_t;
typedef __uint_least8_t uint_least8_t;
typedef __uint_least16_t uint_least16_t;
typedef __uint_least32_t uint_least32_t;
typedef __uint_least64_t uint_least64_t;
typedef signed char int_fast8_t;
typedef long int_fast16_t;
typedef long int_fast32_t;
typedef long int_fast64_t;
typedef unsigned char uint_fast8_t;
typedef unsigned long uint_fast16_t;
typedef unsigned long uint_fast32_t;
typedef unsigned long uint_fast64_t;
typedef long intptr_t;
typedef unsigned long uintptr_t;
typedef __intmax_t intmax_t;
typedef __uintmax_t uintmax_t;
extern void abort(void);
extern uint64_t revng_init_local_sp(uint64_t param_0);
extern uint64_t init_rbx(void);
extern uint64_t init_rbp(void);
extern void indirect_placeholder(void);
extern uint64_t init_r13(void);
extern uint64_t init_r12(void);
extern uint64_t init_r14(void);
extern uint64_t init_r15(void);
extern uint64_t init_rax(void);
extern void llvm_trap(void);
uint64_t bb_freopen_safer(uint64_t rdi, uint64_t rdx, uint64_t rsi) {
    uint64_t var_19;
    uint64_t var_0;
    uint64_t var_1;
    uint64_t var_2;
    uint64_t var_3;
    uint64_t var_4;
    uint64_t var_5;
    uint64_t var_6;
    uint64_t var_7;
    uint64_t var_8;
    uint32_t var_9;
    uint64_t r12_3;
    uint64_t storemerge1;
    uint64_t local_sp_4;
    uint64_t local_sp_3;
    uint64_t local_sp_16;
    uint64_t local_sp_5;
    uint64_t local_sp_7;
    uint64_t local_sp_12;
    uint64_t local_sp_6;
    uint64_t rbx_4;
    uint64_t var_20;
    uint64_t var_18;
    uint64_t local_sp_1389;
    uint64_t r13_5;
    uint64_t rax_988;
    uint64_t r13_57087;
    uint64_t r12_27386;
    uint64_t rbp_37584;
    uint64_t var_16;
    uint32_t var_17;
    uint64_t storemerge2;
    uint64_t var_15;
    uint64_t rax_8;
    uint64_t local_sp_15;
    uint64_t var_10;
    uint64_t var_11;
    uint64_t var_12;
    uint64_t var_13;
    uint64_t var_14;
    var_0 = revng_init_local_sp(0UL);
    var_1 = init_rax();
    var_2 = init_r15();
    var_3 = init_r13();
    var_4 = init_rbx();
    var_5 = init_r12();
    var_6 = init_rbp();
    var_7 = init_r14();
    *(uint64_t *)(var_0 + (-8L)) = var_2;
    *(uint64_t *)(var_0 + (-16L)) = var_7;
    *(uint64_t *)(var_0 + (-24L)) = var_3;
    *(uint64_t *)(var_0 + (-32L)) = var_5;
    *(uint64_t *)(var_0 + (-40L)) = var_6;
    *(uint64_t *)(var_0 + (-48L)) = var_4;
    var_8 = var_0 + (-64L);
    *(uint64_t *)var_8 = 4205855UL;
    indirect_placeholder();
    var_9 = (uint32_t)var_1;
    r12_3 = 0UL;
    storemerge1 = 0UL;
    local_sp_16 = var_8;
    local_sp_1389 = var_8;
    r13_5 = 0UL;
    rax_988 = var_1;
    r13_57087 = 0UL;
    r12_27386 = 0UL;
    rbp_37584 = 0UL;
    storemerge2 = 0UL;
    rax_8 = var_1;
    local_sp_15 = var_8;
    if ((uint64_t)(var_9 + (-1)) != 0UL) {
        var_14 = local_sp_16 + (-8L);
        *(uint64_t *)var_14 = 4205934UL;
        indirect_placeholder();
        r12_27386 = r12_3;
        rbp_37584 = storemerge2;
        local_sp_12 = var_14;
        if ((uint64_t)var_9 == 0UL) {
            var_15 = local_sp_16 + (-16L);
            *(uint64_t *)var_15 = 4205952UL;
            indirect_placeholder();
            rax_8 = 0UL;
            r13_5 = 1UL;
            local_sp_12 = var_15;
        }
        local_sp_1389 = local_sp_12;
        rax_988 = rax_8;
        r13_57087 = r13_5;
        if ((uint64_t)(unsigned char)storemerge2 == 0UL) {
            *(uint64_t *)(local_sp_12 + (-8L)) = 4206222UL;
            indirect_placeholder();
            *(uint64_t *)(local_sp_12 + (-16L)) = 4206242UL;
            indirect_placeholder();
            *(uint64_t *)(local_sp_12 + (-24L)) = 4206247UL;
            indirect_placeholder();
            llvm_trap();
            abort();
        }
        if ((uint64_t)(unsigned char)r12_3 == 0UL) {
            *(uint64_t *)(local_sp_12 + (-8L)) = 4206092UL;
            indirect_placeholder();
            *(uint64_t *)(local_sp_12 + (-16L)) = 4206108UL;
            indirect_placeholder();
            *(uint64_t *)(local_sp_12 + (-24L)) = 4206113UL;
            indirect_placeholder();
            llvm_trap();
            abort();
        }
        *(uint64_t *)(local_sp_1389 + (-8L)) = 4206190UL;
        indirect_placeholder();
        var_16 = local_sp_1389 + (-16L);
        *(uint64_t *)var_16 = 4205995UL;
        indirect_placeholder();
        var_17 = *(uint32_t *)rax_988;
        local_sp_3 = var_16;
        local_sp_4 = var_16;
        rbx_4 = rax_988;
        if ((uint64_t)(unsigned char)r12_27386 == 0UL) {
            if ((uint64_t)(unsigned char)rbp_37584 != 0UL) {
                local_sp_5 = local_sp_4;
                local_sp_7 = local_sp_4;
                if ((uint64_t)(unsigned char)r13_57087 == 0UL) {
                    local_sp_6 = local_sp_5;
                    if (rax_988 == 0UL) {
                        return rbx_4;
                    }
                }
                var_20 = local_sp_7 + (-8L);
                *(uint64_t *)var_20 = 4206034UL;
                indirect_placeholder();
                local_sp_6 = var_20;
                if (rax_988 == 0UL) {
                    return rbx_4;
                }
                *(uint64_t *)(local_sp_6 + (-8L)) = 4206044UL;
                indirect_placeholder();
                *(volatile uint32_t *)(uint32_t *)0UL = var_17;
                rbx_4 = 0UL;
                return rbx_4;
            }
        }
        var_18 = var_16 + (-8L);
        *(uint64_t *)var_18 = 4206139UL;
        indirect_placeholder();
        local_sp_3 = var_18;
        local_sp_4 = var_18;
        if ((uint64_t)(unsigned char)rbp_37584 != 0UL) {
            local_sp_5 = local_sp_4;
            local_sp_7 = local_sp_4;
            if ((uint64_t)(unsigned char)r13_57087 == 0UL) {
                local_sp_6 = local_sp_5;
                if (rax_988 == 0UL) {
                    return rbx_4;
                }
            }
            var_20 = local_sp_7 + (-8L);
            *(uint64_t *)var_20 = 4206034UL;
            indirect_placeholder();
            local_sp_6 = var_20;
            if (rax_988 == 0UL) {
                return rbx_4;
            }
            *(uint64_t *)(local_sp_6 + (-8L)) = 4206044UL;
            indirect_placeholder();
            *(volatile uint32_t *)(uint32_t *)0UL = var_17;
            rbx_4 = 0UL;
            return rbx_4;
        }
        var_19 = local_sp_3 + (-8L);
        *(uint64_t *)var_19 = 4206022UL;
        indirect_placeholder();
        local_sp_5 = var_19;
        local_sp_7 = var_19;
        if ((uint64_t)(unsigned char)r13_57087 == 0UL) {
            local_sp_6 = local_sp_5;
            if (rax_988 == 0UL) {
                return rbx_4;
            }
        }
        var_20 = local_sp_7 + (-8L);
        *(uint64_t *)var_20 = 4206034UL;
        indirect_placeholder();
        local_sp_6 = var_20;
        if (rax_988 == 0UL) {
            return rbx_4;
        }
        *(uint64_t *)(local_sp_6 + (-8L)) = 4206044UL;
        indirect_placeholder();
        *(volatile uint32_t *)(uint32_t *)0UL = var_17;
        rbx_4 = 0UL;
        return rbx_4;
    }
    if ((uint64_t)(var_9 + (-2)) != 0UL) {
        var_12 = local_sp_15 + (-8L);
        *(uint64_t *)var_12 = 4205918UL;
        indirect_placeholder();
        var_13 = (var_6 & (-256L)) | 1UL;
        r12_3 = storemerge1;
        local_sp_16 = var_12;
        storemerge2 = var_13;
        var_14 = local_sp_16 + (-8L);
        *(uint64_t *)var_14 = 4205934UL;
        indirect_placeholder();
        r12_27386 = r12_3;
        rbp_37584 = storemerge2;
        local_sp_12 = var_14;
        if ((uint64_t)var_9 == 0UL) {
            var_15 = local_sp_16 + (-16L);
            *(uint64_t *)var_15 = 4205952UL;
            indirect_placeholder();
            rax_8 = 0UL;
            r13_5 = 1UL;
            local_sp_12 = var_15;
        }
        local_sp_1389 = local_sp_12;
        rax_988 = rax_8;
        r13_57087 = r13_5;
        if ((uint64_t)(unsigned char)storemerge2 == 0UL) {
            *(uint64_t *)(local_sp_12 + (-8L)) = 4206222UL;
            indirect_placeholder();
            *(uint64_t *)(local_sp_12 + (-16L)) = 4206242UL;
            indirect_placeholder();
            *(uint64_t *)(local_sp_12 + (-24L)) = 4206247UL;
            indirect_placeholder();
            llvm_trap();
            abort();
        }
        if ((uint64_t)(unsigned char)r12_3 == 0UL) {
            *(uint64_t *)(local_sp_12 + (-8L)) = 4206092UL;
            indirect_placeholder();
            *(uint64_t *)(local_sp_12 + (-16L)) = 4206108UL;
            indirect_placeholder();
            *(uint64_t *)(local_sp_12 + (-24L)) = 4206113UL;
            indirect_placeholder();
            llvm_trap();
            abort();
        }
    }
    if ((uint64_t)var_9 != 0UL) {
        var_10 = var_0 + (-72L);
        *(uint64_t *)var_10 = 4205896UL;
        indirect_placeholder();
        var_11 = (var_5 & (-256L)) | 1UL;
        local_sp_15 = var_10;
        storemerge1 = var_11;
        var_12 = local_sp_15 + (-8L);
        *(uint64_t *)var_12 = 4205918UL;
        indirect_placeholder();
        var_13 = (var_6 & (-256L)) | 1UL;
        r12_3 = storemerge1;
        local_sp_16 = var_12;
        storemerge2 = var_13;
        var_14 = local_sp_16 + (-8L);
        *(uint64_t *)var_14 = 4205934UL;
        indirect_placeholder();
        r12_27386 = r12_3;
        rbp_37584 = storemerge2;
        local_sp_12 = var_14;
        if ((uint64_t)var_9 == 0UL) {
            var_15 = local_sp_16 + (-16L);
            *(uint64_t *)var_15 = 4205952UL;
            indirect_placeholder();
            rax_8 = 0UL;
            r13_5 = 1UL;
            local_sp_12 = var_15;
        }
        local_sp_1389 = local_sp_12;
        rax_988 = rax_8;
        r13_57087 = r13_5;
        if ((uint64_t)(unsigned char)storemerge2 == 0UL) {
            *(uint64_t *)(local_sp_12 + (-8L)) = 4206222UL;
            indirect_placeholder();
            *(uint64_t *)(local_sp_12 + (-16L)) = 4206242UL;
            indirect_placeholder();
            *(uint64_t *)(local_sp_12 + (-24L)) = 4206247UL;
            indirect_placeholder();
            llvm_trap();
            abort();
        }
        if ((uint64_t)(unsigned char)r12_3 == 0UL) {
            *(uint64_t *)(local_sp_12 + (-8L)) = 4206092UL;
            indirect_placeholder();
            *(uint64_t *)(local_sp_12 + (-16L)) = 4206108UL;
            indirect_placeholder();
            *(uint64_t *)(local_sp_12 + (-24L)) = 4206113UL;
            indirect_placeholder();
            llvm_trap();
            abort();
        }
    }
}

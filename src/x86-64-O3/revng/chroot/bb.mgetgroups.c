typedef unsigned char __u_char;
typedef unsigned short __u_short;
typedef unsigned int __u_int;
typedef unsigned long __u_long;
typedef signed char __int8_t;
typedef unsigned char __uint8_t;
typedef short __int16_t;
typedef unsigned short __uint16_t;
typedef int __int32_t;
typedef unsigned int __uint32_t;
typedef long __int64_t;
typedef unsigned long __uint64_t;
typedef __int8_t __int_least8_t;
typedef __uint8_t __uint_least8_t;
typedef __int16_t __int_least16_t;
typedef __uint16_t __uint_least16_t;
typedef __int32_t __int_least32_t;
typedef __uint32_t __uint_least32_t;
typedef __int64_t __int_least64_t;
typedef __uint64_t __uint_least64_t;
typedef long __quad_t;
typedef unsigned long __u_quad_t;
typedef long __intmax_t;
typedef unsigned long __uintmax_t;
typedef unsigned long __dev_t;
typedef unsigned int __uid_t;
typedef unsigned int __gid_t;
typedef unsigned long __ino_t;
typedef unsigned long __ino64_t;
typedef unsigned int __mode_t;
typedef unsigned long __nlink_t;
typedef long __off_t;
typedef long __off64_t;
typedef int __pid_t;
typedef struct {
    int __val[2];
} __fsid_t;
typedef long __clock_t;
typedef unsigned long __rlim_t;
typedef unsigned long __rlim64_t;
typedef unsigned int __id_t;
typedef long __time_t;
typedef unsigned int __useconds_t;
typedef long __suseconds_t;
typedef int __daddr_t;
typedef int __key_t;
typedef int __clockid_t;
typedef void *__timer_t;
typedef long __blksize_t;
typedef long __blkcnt_t;
typedef long __blkcnt64_t;
typedef unsigned long __fsblkcnt_t;
typedef unsigned long __fsblkcnt64_t;
typedef unsigned long __fsfilcnt_t;
typedef unsigned long __fsfilcnt64_t;
typedef long __fsword_t;
typedef long __ssize_t;
typedef long __syscall_slong_t;
typedef unsigned long __syscall_ulong_t;
typedef __off64_t __loff_t;
typedef char *__caddr_t;
typedef long __intptr_t;
typedef unsigned int __socklen_t;
typedef int __sig_atomic_t;
typedef __int8_t int8_t;
typedef __int16_t int16_t;
typedef __int32_t int32_t;
typedef __int64_t int64_t;
typedef __uint8_t uint8_t;
typedef __uint16_t uint16_t;
typedef __uint32_t uint32_t;
typedef __uint64_t uint64_t;
typedef __int_least8_t int_least8_t;
typedef __int_least16_t int_least16_t;
typedef __int_least32_t int_least32_t;
typedef __int_least64_t int_least64_t;
typedef __uint_least8_t uint_least8_t;
typedef __uint_least16_t uint_least16_t;
typedef __uint_least32_t uint_least32_t;
typedef __uint_least64_t uint_least64_t;
typedef signed char int_fast8_t;
typedef long int_fast16_t;
typedef long int_fast32_t;
typedef long int_fast64_t;
typedef unsigned char uint_fast8_t;
typedef unsigned long uint_fast16_t;
typedef unsigned long uint_fast32_t;
typedef unsigned long uint_fast64_t;
typedef long intptr_t;
typedef unsigned long uintptr_t;
typedef __intmax_t intmax_t;
typedef __uintmax_t uintmax_t;
typedef _Bool bool;
extern uint64_t revng_init_local_sp(uint64_t param_0);
extern uint64_t init_rbx(void);
extern uint64_t init_rbp(void);
extern void indirect_placeholder(void);
extern uint64_t init_cc_src2(void);
extern uint64_t init_r13(void);
extern uint64_t init_r12(void);
extern uint64_t init_r14(void);
extern uint64_t init_rax(void);
extern uint64_t helper_cc_compute_all_wrapper(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint32_t param_3);
extern uint64_t init_r15(void);
extern uint64_t indirect_placeholder_27(void);
extern void indirect_placeholder_11(uint64_t param_0, uint64_t param_1);
uint64_t bb_mgetgroups(uint64_t rdi, uint64_t rdx, uint64_t rsi) {
    uint64_t var_0;
    uint64_t var_1;
    uint64_t var_2;
    uint64_t var_3;
    uint64_t var_4;
    uint64_t var_5;
    uint64_t var_6;
    uint64_t var_7;
    uint64_t var_8;
    uint32_t var_51;
    uint64_t var_52;
    uint64_t rax_2;
    uint64_t var_34;
    uint32_t _pre;
    uint32_t _pre_phi;
    uint64_t rsi3_0_in;
    uint64_t var_39;
    uint32_t var_40;
    uint64_t var_41;
    uint64_t var_42;
    uint64_t rbx_0_ph;
    uint64_t rdx2_0_ph;
    uint64_t rsi3_1_ph;
    uint32_t *var_43;
    uint64_t rdx2_0;
    uint64_t rsi3_1;
    uint32_t var_44;
    uint64_t rsi3_2;
    uint64_t var_45;
    uint64_t var_46;
    uint64_t var_47;
    uint64_t var_48;
    uint32_t var_29;
    bool var_30;
    uint64_t var_31;
    uint64_t *var_32;
    uint64_t var_33;
    uint64_t rax_1;
    uint64_t var_35;
    uint32_t var_36;
    uint64_t var_23;
    uint64_t rax_0;
    uint32_t var_24;
    uint64_t var_25;
    uint64_t var_26;
    uint64_t var_27;
    uint64_t var_28;
    uint64_t var_49;
    uint32_t *var_50;
    uint64_t var_16;
    uint64_t rcx_0;
    uint64_t var_17;
    uint64_t var_18;
    uint64_t local_sp_1;
    uint64_t local_sp_2;
    uint32_t *var_37;
    uint32_t var_38;
    uint64_t var_19;
    bool var_10;
    uint32_t var_11;
    uint64_t var_12;
    uint32_t *var_13;
    uint32_t var_14;
    uint64_t var_15;
    uint64_t var_9;
    uint64_t var_20;
    uint32_t *var_21;
    uint32_t var_22;
    unsigned int loop_state_var;
    bool switch_state_var;
    var_0 = revng_init_local_sp(0UL);
    var_1 = init_rax();
    var_2 = init_r15();
    var_3 = init_r13();
    var_4 = init_rbx();
    var_5 = init_r12();
    var_6 = init_rbp();
    var_7 = init_cc_src2();
    var_8 = init_r14();
    *(uint64_t *)(var_0 + (-8L)) = var_2;
    *(uint64_t *)(var_0 + (-16L)) = var_8;
    *(uint64_t *)(var_0 + (-24L)) = var_3;
    *(uint64_t *)(var_0 + (-32L)) = var_5;
    *(uint64_t *)(var_0 + (-40L)) = var_6;
    *(uint64_t *)(var_0 + (-48L)) = var_4;
    *(uint64_t *)(var_0 + (-80L)) = rdx;
    rax_1 = var_1;
    rax_2 = 4294967295UL;
    if (rdi != 0UL) {
        *(uint32_t *)(var_0 + (-60L)) = 10U;
        var_9 = var_0 + (-96L);
        *(uint64_t *)var_9 = 4232662UL;
        indirect_placeholder_11(0UL, 40UL);
        local_sp_2 = var_9;
        if (var_1 == 0UL) {
            return rax_2;
        }
        var_10 = ((int)(uint32_t)var_1 > (int)4294967295U);
        while (1U)
            {
                var_11 = *(uint32_t *)(local_sp_2 + 28UL);
                var_12 = local_sp_2 + (-8L);
                *(uint64_t *)var_12 = 4232749UL;
                indirect_placeholder();
                var_13 = (uint32_t *)(local_sp_2 + 20UL);
                var_14 = *var_13;
                var_15 = (uint64_t)var_14;
                if (var_10) {
                    if ((uint64_t)(var_11 - var_14) != 0UL) {
                        rcx_0 = (uint64_t)var_14;
                        if ((int)var_14 < (int)0U) {
                            break;
                        }
                    }
                    *var_13 = (var_14 << 1U);
                    var_16 = (uint64_t)((long)(var_15 << 33UL) >> (long)32UL);
                    rcx_0 = var_16;
                    if (var_16 > 2305843009213693951UL) {
                        break;
                    }
                }
                rcx_0 = (uint64_t)var_14;
                if ((int)var_14 < (int)0U) {
                    break;
                }
                var_17 = rcx_0 << 2UL;
                var_18 = local_sp_2 + (-16L);
                *(uint64_t *)var_18 = 4232712UL;
                indirect_placeholder_11(var_1, var_17);
                local_sp_2 = var_18;
                if (!var_10) {
                    continue;
                }
                **(uint64_t **)var_12 = var_1;
                return (uint64_t)*(uint32_t *)(local_sp_2 | 12UL);
            }
        var_19 = local_sp_2 + (-16L);
        *(uint64_t *)var_19 = 4232789UL;
        indirect_placeholder();
        *(uint32_t *)var_1 = 12U;
        local_sp_1 = var_19;
        *(uint64_t *)(local_sp_1 + (-8L)) = 4232800UL;
        indirect_placeholder();
        var_37 = (uint32_t *)rax_1;
        var_38 = *var_37;
        *(uint64_t *)(local_sp_1 + (-16L)) = 4232810UL;
        indirect_placeholder();
        *(uint64_t *)(local_sp_1 + (-24L)) = 4232815UL;
        indirect_placeholder();
        *var_37 = var_38;
        return rax_2;
    }
    *(uint64_t *)(var_0 + (-96L)) = 4232881UL;
    var_20 = indirect_placeholder_27();
    var_21 = (uint32_t *)(var_0 + (-68L));
    var_22 = (uint32_t)var_20;
    *var_21 = var_22;
    rax_0 = var_20;
    if ((int)var_22 >= (int)0U) {
        var_49 = var_0 + (-104L);
        *(uint64_t *)var_49 = 4233085UL;
        indirect_placeholder();
        var_50 = (uint32_t *)var_20;
        *(uint64_t *)(var_0 + (-112L)) = 4233106UL;
        indirect_placeholder_11(0UL, 4UL);
        if (*var_50 != 38U & var_20 == 0UL) {
            **(uint64_t **)var_49 = var_20;
            var_51 = (uint32_t)rsi;
            *var_50 = var_51;
            var_52 = ((uint64_t)(var_51 + 1U) != 0UL);
            rax_2 = var_52;
        }
        return rax_2;
    }
    var_23 = helper_cc_compute_all_wrapper(var_20, 40UL, var_7, 24U);
    if ((var_23 & 64UL) == 0UL) {
        var_24 = var_22 + 1U;
        var_25 = (uint64_t)var_24;
        *var_21 = var_24;
        rax_0 = var_25;
    } else {
        if ((uint64_t)((uint32_t)rsi + 1U) == 0UL) {
            var_24 = var_22 + 1U;
            var_25 = (uint64_t)var_24;
            *var_21 = var_24;
            rax_0 = var_25;
        }
    }
    var_26 = rax_0 << 32UL;
    var_27 = (uint64_t)((long)var_26 >> (long)32UL);
    var_28 = (uint64_t)((long)var_26 >> (long)30UL);
    *(uint64_t *)(var_0 + (-104L)) = 4232925UL;
    indirect_placeholder_11(0UL, var_28);
    rbx_0_ph = var_27;
    if (var_26 == 0UL) {
        return rax_2;
    }
    var_29 = (uint32_t)rsi;
    var_30 = ((uint64_t)(var_29 + 1U) == 0UL);
    var_31 = var_0 + (-112L);
    var_32 = (uint64_t *)var_31;
    local_sp_1 = var_31;
    if (var_30) {
        *var_32 = 4233152UL;
        var_35 = indirect_placeholder_27();
        var_36 = (uint32_t)var_35;
        _pre_phi = var_36;
        rsi3_0_in = var_35;
        rax_1 = var_35;
        if ((int)var_36 <= (int)4294967295U) {
            var_39 = (uint64_t)_pre_phi;
            **(uint64_t **)(var_31 + 8UL) = var_27;
            rsi3_1_ph = var_39;
            rax_2 = var_39;
            if ((int)_pre_phi > (int)1U) {
                return rax_2;
            }
            var_40 = *(uint32_t *)var_27;
            var_41 = (uint64_t)((long)(rsi3_0_in << 32UL) >> (long)30UL) + var_27;
            var_42 = var_27 + 4UL;
            rdx2_0_ph = var_42;
            if (var_41 <= var_42) {
                while (1U)
                    {
                        var_43 = (uint32_t *)rbx_0_ph;
                        rdx2_0 = rdx2_0_ph;
                        rsi3_1 = rsi3_1_ph;
                        while (1U)
                            {
                                var_44 = *(uint32_t *)rdx2_0;
                                rsi3_1_ph = rsi3_1;
                                rsi3_2 = rsi3_1;
                                if ((uint64_t)(var_44 - var_40) != 0UL) {
                                    if ((uint64_t)(var_44 - *var_43) != 0UL) {
                                        loop_state_var = 1U;
                                        break;
                                    }
                                }
                                var_47 = rdx2_0 + 4UL;
                                var_48 = (uint64_t)((uint32_t)rsi3_1 + (-1));
                                rdx2_0 = var_47;
                                rsi3_1 = var_48;
                                rsi3_2 = var_48;
                                if (var_41 > var_47) {
                                    continue;
                                }
                                loop_state_var = 0U;
                                break;
                            }
                        switch_state_var = 0;
                        switch (loop_state_var) {
                          case 0U:
                            {
                                switch_state_var = 1;
                                break;
                            }
                            break;
                          case 1U:
                            {
                                var_45 = rdx2_0 + 4UL;
                                var_46 = rbx_0_ph + 4UL;
                                *(uint32_t *)var_46 = var_44;
                                rbx_0_ph = var_46;
                                rdx2_0_ph = var_45;
                                if (var_41 <= var_45) {
                                    continue;
                                }
                                switch_state_var = 1;
                                break;
                            }
                            break;
                        }
                        if (switch_state_var)
                            break;
                    }
                return (uint64_t)(uint32_t)rsi3_2;
            }
        }
    }
    *var_32 = 4232959UL;
    var_33 = indirect_placeholder_27();
    rax_1 = var_33;
    if ((int)(uint32_t)var_33 < (int)0U) {
        return;
    }
    var_34 = var_33 + 1UL;
    *(uint32_t *)var_27 = var_29;
    _pre = (uint32_t)var_34;
    _pre_phi = _pre;
    rsi3_0_in = var_34;
    var_39 = (uint64_t)_pre_phi;
    **(uint64_t **)(var_31 + 8UL) = var_27;
    rsi3_1_ph = var_39;
    rax_2 = var_39;
    if ((int)_pre_phi > (int)1U) {
        return rax_2;
    }
    var_40 = *(uint32_t *)var_27;
    var_41 = (uint64_t)((long)(rsi3_0_in << 32UL) >> (long)30UL) + var_27;
    var_42 = var_27 + 4UL;
    rdx2_0_ph = var_42;
    if (var_41 <= var_42) {
        while (1U)
            {
                var_43 = (uint32_t *)rbx_0_ph;
                rdx2_0 = rdx2_0_ph;
                rsi3_1 = rsi3_1_ph;
                while (1U)
                    {
                        var_44 = *(uint32_t *)rdx2_0;
                        rsi3_1_ph = rsi3_1;
                        rsi3_2 = rsi3_1;
                        if ((uint64_t)(var_44 - var_40) != 0UL) {
                            if ((uint64_t)(var_44 - *var_43) != 0UL) {
                                loop_state_var = 1U;
                                break;
                            }
                        }
                        var_47 = rdx2_0 + 4UL;
                        var_48 = (uint64_t)((uint32_t)rsi3_1 + (-1));
                        rdx2_0 = var_47;
                        rsi3_1 = var_48;
                        rsi3_2 = var_48;
                        if (var_41 > var_47) {
                            continue;
                        }
                        loop_state_var = 0U;
                        break;
                    }
                switch_state_var = 0;
                switch (loop_state_var) {
                  case 0U:
                    {
                        switch_state_var = 1;
                        break;
                    }
                    break;
                  case 1U:
                    {
                        var_45 = rdx2_0 + 4UL;
                        var_46 = rbx_0_ph + 4UL;
                        *(uint32_t *)var_46 = var_44;
                        rbx_0_ph = var_46;
                        rdx2_0_ph = var_45;
                        if (var_41 <= var_45) {
                            continue;
                        }
                        switch_state_var = 1;
                        break;
                    }
                    break;
                }
                if (switch_state_var)
                    break;
            }
        return (uint64_t)(uint32_t)rsi3_2;
    }
}

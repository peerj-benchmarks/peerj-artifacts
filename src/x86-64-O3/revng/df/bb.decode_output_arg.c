typedef unsigned char __u_char;
typedef unsigned short __u_short;
typedef unsigned int __u_int;
typedef unsigned long __u_long;
typedef signed char __int8_t;
typedef unsigned char __uint8_t;
typedef short __int16_t;
typedef unsigned short __uint16_t;
typedef int __int32_t;
typedef unsigned int __uint32_t;
typedef long __int64_t;
typedef unsigned long __uint64_t;
typedef __int8_t __int_least8_t;
typedef __uint8_t __uint_least8_t;
typedef __int16_t __int_least16_t;
typedef __uint16_t __uint_least16_t;
typedef __int32_t __int_least32_t;
typedef __uint32_t __uint_least32_t;
typedef __int64_t __int_least64_t;
typedef __uint64_t __uint_least64_t;
typedef long __quad_t;
typedef unsigned long __u_quad_t;
typedef long __intmax_t;
typedef unsigned long __uintmax_t;
typedef unsigned long __dev_t;
typedef unsigned int __uid_t;
typedef unsigned int __gid_t;
typedef unsigned long __ino_t;
typedef unsigned long __ino64_t;
typedef unsigned int __mode_t;
typedef unsigned long __nlink_t;
typedef long __off_t;
typedef long __off64_t;
typedef int __pid_t;
typedef struct {
    int __val[2];
} __fsid_t;
typedef long __clock_t;
typedef unsigned long __rlim_t;
typedef unsigned long __rlim64_t;
typedef unsigned int __id_t;
typedef long __time_t;
typedef unsigned int __useconds_t;
typedef long __suseconds_t;
typedef int __daddr_t;
typedef int __key_t;
typedef int __clockid_t;
typedef void *__timer_t;
typedef long __blksize_t;
typedef long __blkcnt_t;
typedef long __blkcnt64_t;
typedef unsigned long __fsblkcnt_t;
typedef unsigned long __fsblkcnt64_t;
typedef unsigned long __fsfilcnt_t;
typedef unsigned long __fsfilcnt64_t;
typedef long __fsword_t;
typedef long __ssize_t;
typedef long __syscall_slong_t;
typedef unsigned long __syscall_ulong_t;
typedef __off64_t __loff_t;
typedef char *__caddr_t;
typedef long __intptr_t;
typedef unsigned int __socklen_t;
typedef int __sig_atomic_t;
typedef __int8_t int8_t;
typedef __int16_t int16_t;
typedef __int32_t int32_t;
typedef __int64_t int64_t;
typedef __uint8_t uint8_t;
typedef __uint16_t uint16_t;
typedef __uint32_t uint32_t;
typedef __uint64_t uint64_t;
typedef __int_least8_t int_least8_t;
typedef __int_least16_t int_least16_t;
typedef __int_least32_t int_least32_t;
typedef __int_least64_t int_least64_t;
typedef __uint_least8_t uint_least8_t;
typedef __uint_least16_t uint_least16_t;
typedef __uint_least32_t uint_least32_t;
typedef __uint_least64_t uint_least64_t;
typedef signed char int_fast8_t;
typedef long int_fast16_t;
typedef long int_fast32_t;
typedef long int_fast64_t;
typedef unsigned char uint_fast8_t;
typedef unsigned long uint_fast16_t;
typedef unsigned long uint_fast32_t;
typedef unsigned long uint_fast64_t;
typedef long intptr_t;
typedef unsigned long uintptr_t;
typedef __intmax_t intmax_t;
typedef __uintmax_t uintmax_t;
typedef _Bool bool;
struct indirect_placeholder_86_ret_type;
struct indirect_placeholder_87_ret_type;
struct indirect_placeholder_88_ret_type;
struct indirect_placeholder_89_ret_type;
struct indirect_placeholder_90_ret_type;
struct indirect_placeholder_91_ret_type;
struct indirect_placeholder_86_ret_type {
    uint64_t field_0;
    uint64_t field_1;
    uint64_t field_2;
    uint64_t field_3;
};
struct indirect_placeholder_87_ret_type {
    uint64_t field_0;
    uint64_t field_1;
    uint64_t field_2;
};
struct indirect_placeholder_88_ret_type {
    uint64_t field_0;
    uint64_t field_1;
    uint64_t field_2;
};
struct indirect_placeholder_89_ret_type {
    uint64_t field_0;
    uint64_t field_1;
    uint64_t field_2;
};
struct indirect_placeholder_90_ret_type {
    uint64_t field_0;
    uint64_t field_1;
    uint64_t field_2;
};
struct indirect_placeholder_91_ret_type {
    uint64_t field_0;
    uint64_t field_1;
    uint64_t field_2;
};
extern void abort(void);
extern uint64_t revng_init_local_sp(uint64_t param_0);
extern uint64_t init_rbx(void);
extern uint64_t init_rbp(void);
extern void indirect_placeholder(void);
extern uint64_t init_r13(void);
extern uint64_t init_r12(void);
extern uint64_t init_r14(void);
extern uint64_t init_r15(void);
extern uint64_t init_r8(void);
extern uint64_t indirect_placeholder_1(uint64_t param_0);
extern void indirect_placeholder_10(uint64_t param_0, uint64_t param_1);
extern void indirect_placeholder_78(uint64_t param_0, uint64_t param_1, uint64_t param_2);
extern uint64_t init_r9(void);
extern struct indirect_placeholder_86_ret_type indirect_placeholder_86(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint64_t param_3, uint64_t param_4, uint64_t param_5, uint64_t param_6);
extern struct indirect_placeholder_87_ret_type indirect_placeholder_87(uint64_t param_0);
extern struct indirect_placeholder_88_ret_type indirect_placeholder_88(uint64_t param_0, uint64_t param_1);
extern struct indirect_placeholder_89_ret_type indirect_placeholder_89(uint64_t param_0, uint64_t param_1);
extern struct indirect_placeholder_90_ret_type indirect_placeholder_90(uint64_t param_0, uint64_t param_1);
extern struct indirect_placeholder_91_ret_type indirect_placeholder_91(uint64_t param_0);
void bb_decode_output_arg(uint64_t rdi) {
    uint64_t var_0;
    uint64_t var_1;
    uint64_t var_2;
    uint64_t var_3;
    uint64_t var_4;
    uint64_t var_5;
    uint64_t var_6;
    uint64_t var_7;
    uint64_t var_8;
    uint64_t var_9;
    uint64_t var_10;
    uint64_t local_sp_6;
    uint64_t _pre_pre_phi;
    unsigned char *_pre103_pre_phi;
    unsigned char *_pre_phi104;
    uint64_t local_sp_1;
    uint64_t _pre_phi;
    uint64_t local_sp_0;
    uint64_t r8_1;
    uint64_t r9_0;
    uint64_t r8_0;
    uint64_t r9_1;
    uint64_t var_68;
    uint64_t local_sp_2;
    uint64_t rax_0;
    uint64_t r8_4;
    uint64_t r9_2;
    uint64_t storemerge;
    uint64_t r8_2;
    uint64_t var_14;
    uint64_t rbx_3;
    uint64_t local_sp_4;
    uint64_t rbp_1;
    uint64_t rbx_2;
    uint64_t r9_3;
    uint64_t rbp_0;
    uint64_t rdx_0;
    uint64_t r8_3;
    uint64_t storemerge1;
    uint32_t var_15;
    uint64_t var_16;
    uint64_t var_17;
    uint64_t local_sp_5;
    uint64_t var_18;
    struct indirect_placeholder_87_ret_type var_19;
    uint64_t var_20;
    uint64_t var_24;
    uint64_t var_25;
    uint64_t var_69;
    uint64_t var_40;
    uint64_t var_41;
    uint64_t var_42;
    uint64_t var_43;
    uint64_t var_44;
    struct indirect_placeholder_88_ret_type var_45;
    uint64_t var_46;
    uint64_t var_47;
    uint64_t var_48;
    uint64_t var_49;
    uint64_t var_50;
    uint64_t var_51;
    unsigned char *var_52;
    unsigned char var_53;
    uint64_t var_54;
    uint64_t var_55;
    uint64_t var_56;
    uint64_t var_57;
    uint64_t var_58;
    struct indirect_placeholder_89_ret_type var_59;
    uint64_t var_60;
    uint64_t var_61;
    uint64_t var_62;
    uint64_t var_63;
    uint64_t var_64;
    uint64_t var_65;
    unsigned char *var_66;
    unsigned char var_67;
    uint64_t var_26;
    uint64_t var_27;
    uint64_t var_28;
    uint64_t var_29;
    uint64_t var_30;
    struct indirect_placeholder_90_ret_type var_31;
    uint64_t var_32;
    uint64_t var_33;
    uint64_t var_34;
    uint64_t var_35;
    uint64_t var_36;
    uint64_t var_37;
    unsigned char *var_38;
    unsigned char var_39;
    uint64_t var_21;
    uint64_t var_22;
    struct indirect_placeholder_91_ret_type var_23;
    uint64_t r9_4;
    uint64_t var_12;
    uint64_t r13_0;
    bool var_13;
    uint64_t rax_1;
    uint64_t var_11;
    unsigned int loop_state_var;
    bool switch_state_var;
    var_0 = revng_init_local_sp(0UL);
    var_1 = init_r15();
    var_2 = init_r13();
    var_3 = init_rbx();
    var_4 = init_r12();
    var_5 = init_r9();
    var_6 = init_rbp();
    var_7 = init_r14();
    var_8 = init_r8();
    *(uint64_t *)(var_0 + (-8L)) = var_1;
    *(uint64_t *)(var_0 + (-16L)) = var_7;
    *(uint64_t *)(var_0 + (-24L)) = var_2;
    *(uint64_t *)(var_0 + (-32L)) = var_4;
    *(uint64_t *)(var_0 + (-40L)) = var_6;
    *(uint64_t *)(var_0 + (-48L)) = var_3;
    var_9 = var_0 + (-64L);
    *(uint64_t *)var_9 = 4213261UL;
    var_10 = indirect_placeholder_1(rdi);
    local_sp_6 = var_9;
    r8_4 = var_8;
    storemerge = var_10;
    rbx_3 = 0UL;
    rbp_1 = 6415752UL;
    rdx_0 = 4298832UL;
    r9_4 = var_5;
    r13_0 = 0UL;
    rax_1 = var_10;
    while (1U)
        {
            var_11 = local_sp_6 + (-8L);
            *(uint64_t *)var_11 = 4213285UL;
            indirect_placeholder();
            r9_2 = r9_4;
            r8_2 = r8_4;
            local_sp_5 = var_11;
            if (rax_1 == 0UL) {
                var_12 = rax_1 + 1UL;
                *(unsigned char *)rax_1 = (unsigned char)'\x00';
                r13_0 = var_12;
            }
            var_13 = ((uint64_t)(uint32_t)rax_1 == 0UL);
            storemerge = r13_0;
            while (1U)
                {
                    var_14 = local_sp_5 + (-8L);
                    *(uint64_t *)var_14 = 4213324UL;
                    indirect_placeholder();
                    rbx_2 = rbx_3;
                    rbp_0 = rbp_1;
                    local_sp_5 = var_14;
                    if (!var_13) {
                        loop_state_var = 0U;
                        break;
                    }
                    var_15 = (uint32_t)rbx_3;
                    var_16 = (uint64_t)(var_15 + 1U);
                    var_17 = rbp_1 + 48UL;
                    rbx_3 = var_16;
                    rbp_1 = var_17;
                    rbx_2 = var_16;
                    rbp_0 = var_17;
                    rdx_0 = 4298880UL;
                    if ((uint64_t)(var_15 + (-11)) == 0UL) {
                        continue;
                    }
                    var_18 = local_sp_5 + (-16L);
                    *(uint64_t *)var_18 = 4213348UL;
                    var_19 = indirect_placeholder_87(storemerge);
                    local_sp_4 = var_18;
                    r9_3 = var_19.field_1;
                    r8_3 = var_19.field_2;
                    storemerge1 = var_19.field_0;
                    loop_state_var = 1U;
                    break;
                }
            switch_state_var = 0;
            switch (loop_state_var) {
              case 0U:
                {
                    var_20 = rbx_3 * 48UL;
                    if (*(unsigned char *)(var_20 + 6415788UL) != '\x00') {
                        var_21 = *(uint64_t *)(var_20 + 6415752UL);
                        var_22 = local_sp_5 + (-16L);
                        *(uint64_t *)var_22 = 4213892UL;
                        var_23 = indirect_placeholder_91(var_21);
                        local_sp_4 = var_22;
                        r9_3 = var_23.field_1;
                        r8_3 = var_23.field_2;
                        storemerge1 = var_23.field_0;
                        loop_state_var = 2U;
                        switch_state_var = 1;
                        break;
                    }
                    var_24 = rbx_3 & 63UL;
                    var_25 = 1UL << var_24;
                    rax_0 = var_25;
                    if ((uint64_t)((uint16_t)var_25 & (unsigned short)4075U) == 0UL) {
                        var_26 = *(uint64_t *)6416632UL;
                        var_27 = *(uint64_t *)6416624UL + 1UL;
                        *(uint64_t *)6416624UL = var_27;
                        if (var_27 <= 1152921504606846975UL) {
                            loop_state_var = 0U;
                            switch_state_var = 1;
                            break;
                        }
                        var_28 = var_27 << 3UL;
                        var_29 = (uint64_t)((long)(rbx_3 << 32UL) >> (long)32UL);
                        var_30 = local_sp_5 + (-16L);
                        *(uint64_t *)var_30 = 4213550UL;
                        var_31 = indirect_placeholder_90(var_26, var_28);
                        var_32 = var_31.field_0;
                        var_33 = var_31.field_1;
                        var_34 = var_31.field_2;
                        var_35 = *(uint64_t *)6416624UL;
                        *(uint64_t *)6416632UL = var_32;
                        var_36 = var_29 * 48UL;
                        var_37 = var_36 + 6415744UL;
                        var_38 = (unsigned char *)(var_36 + 6415788UL);
                        var_39 = *var_38;
                        *(uint64_t *)(((var_35 << 3UL) + var_32) + (-8L)) = var_37;
                        _pre_pre_phi = var_36;
                        _pre103_pre_phi = var_38;
                        _pre_phi104 = var_38;
                        local_sp_1 = var_30;
                        _pre_phi = var_36;
                        local_sp_0 = var_30;
                        r8_1 = var_34;
                        r9_0 = var_33;
                        r8_0 = var_34;
                        r9_1 = var_33;
                        if (var_39 == '\x00') {
                            var_68 = local_sp_1 + (-8L);
                            *(uint64_t *)var_68 = 4213775UL;
                            indirect_placeholder();
                            _pre_phi104 = _pre103_pre_phi;
                            _pre_phi = _pre_pre_phi;
                            local_sp_0 = var_68;
                            r9_0 = r9_1;
                            r8_0 = r8_1;
                        }
                        *_pre_phi104 = (unsigned char)'\x01';
                        local_sp_2 = local_sp_0;
                        rax_0 = _pre_phi;
                        r9_2 = r9_0;
                        r8_2 = r8_0;
                        if (r13_0 != 0UL) {
                            loop_state_var = 1U;
                            switch_state_var = 1;
                            break;
                        }
                        local_sp_6 = local_sp_2;
                        rax_1 = rax_0;
                        r9_4 = r9_2;
                        r8_4 = r8_2;
                        continue;
                    }
                    switch_state_var = 0;
                    switch (var_24) {
                      case 4UL:
                        {
                            var_54 = *(uint64_t *)6416632UL;
                            var_55 = *(uint64_t *)6416624UL + 1UL;
                            *(uint64_t *)6416624UL = var_55;
                            if (var_55 <= 1152921504606846975UL) {
                                loop_state_var = 0U;
                                switch_state_var = 1;
                                break;
                            }
                            var_56 = var_55 << 3UL;
                            var_57 = (uint64_t)((long)(rbx_3 << 32UL) >> (long)32UL);
                            var_58 = local_sp_5 + (-16L);
                            *(uint64_t *)var_58 = 4213698UL;
                            var_59 = indirect_placeholder_89(var_54, var_56);
                            var_60 = var_59.field_0;
                            var_61 = var_59.field_1;
                            var_62 = var_59.field_2;
                            var_63 = *(uint64_t *)6416624UL;
                            *(uint64_t *)6416632UL = var_60;
                            var_64 = var_57 * 48UL;
                            var_65 = var_64 + 6415744UL;
                            var_66 = (unsigned char *)(var_64 + 6415788UL);
                            var_67 = *var_66;
                            *(uint64_t *)(((var_63 << 3UL) + var_60) + (-8L)) = var_65;
                            *(uint64_t *)(var_64 + 6415768UL) = 4295100UL;
                            _pre_pre_phi = var_64;
                            _pre103_pre_phi = var_66;
                            _pre_phi104 = var_66;
                            local_sp_1 = var_58;
                            _pre_phi = var_64;
                            local_sp_0 = var_58;
                            r8_1 = var_62;
                            r9_0 = var_61;
                            r8_0 = var_62;
                            r9_1 = var_61;
                            if (var_67 == '\x00') {
                                var_68 = local_sp_1 + (-8L);
                                *(uint64_t *)var_68 = 4213775UL;
                                indirect_placeholder();
                                _pre_phi104 = _pre103_pre_phi;
                                _pre_phi = _pre_pre_phi;
                                local_sp_0 = var_68;
                                r9_0 = r9_1;
                                r8_0 = r8_1;
                            }
                            *_pre_phi104 = (unsigned char)'\x01';
                            local_sp_2 = local_sp_0;
                            rax_0 = _pre_phi;
                            r9_2 = r9_0;
                            r8_2 = r8_0;
                            if (r13_0 != 0UL) {
                                loop_state_var = 1U;
                                switch_state_var = 1;
                                break;
                            }
                        }
                        break;
                      case 2UL:
                        {
                            var_40 = *(uint64_t *)6416632UL;
                            var_41 = *(uint64_t *)6416624UL + 1UL;
                            *(uint64_t *)6416624UL = var_41;
                            if (var_41 <= 1152921504606846975UL) {
                                loop_state_var = 0U;
                                switch_state_var = 1;
                                break;
                            }
                            var_42 = var_41 << 3UL;
                            var_43 = (uint64_t)((long)(rbx_3 << 32UL) >> (long)32UL);
                            var_44 = local_sp_5 + (-16L);
                            *(uint64_t *)var_44 = 4213826UL;
                            var_45 = indirect_placeholder_88(var_40, var_42);
                            var_46 = var_45.field_0;
                            var_47 = var_45.field_1;
                            var_48 = var_45.field_2;
                            var_49 = *(uint64_t *)6416624UL;
                            *(uint64_t *)6416632UL = var_46;
                            var_50 = var_43 * 48UL;
                            var_51 = var_50 + 6415744UL;
                            var_52 = (unsigned char *)(var_50 + 6415788UL);
                            var_53 = *var_52;
                            *(uint64_t *)(((var_49 << 3UL) + var_46) + (-8L)) = var_51;
                            *(uint64_t *)(var_50 + 6415768UL) = 4295095UL;
                            _pre_pre_phi = var_50;
                            _pre103_pre_phi = var_52;
                            _pre_phi104 = var_52;
                            local_sp_1 = var_44;
                            _pre_phi = var_50;
                            local_sp_0 = var_44;
                            r8_1 = var_48;
                            r9_0 = var_47;
                            r8_0 = var_48;
                            r9_1 = var_47;
                            if (var_53 == '\x00') {
                                var_68 = local_sp_1 + (-8L);
                                *(uint64_t *)var_68 = 4213775UL;
                                indirect_placeholder();
                                _pre_phi104 = _pre103_pre_phi;
                                _pre_phi = _pre_pre_phi;
                                local_sp_0 = var_68;
                                r9_0 = r9_1;
                                r8_0 = r8_1;
                            }
                            *_pre_phi104 = (unsigned char)'\x01';
                            local_sp_2 = local_sp_0;
                            rax_0 = _pre_phi;
                            r9_2 = r9_0;
                            r8_2 = r8_0;
                            if (r13_0 != 0UL) {
                                loop_state_var = 1U;
                                switch_state_var = 1;
                                break;
                            }
                        }
                        break;
                      default:
                        {
                            var_69 = local_sp_5 + (-16L);
                            *(uint64_t *)var_69 = 4213466UL;
                            indirect_placeholder();
                            local_sp_2 = var_69;
                            if (r13_0 != 0UL) {
                                loop_state_var = 1U;
                                switch_state_var = 1;
                                break;
                            }
                            local_sp_6 = local_sp_2;
                            rax_1 = rax_0;
                            r9_4 = r9_2;
                            r8_4 = r8_2;
                            continue;
                        }
                        break;
                    }
                    if (switch_state_var)
                        break;
                }
                break;
              case 1U:
                {
                    loop_state_var = 2U;
                    switch_state_var = 1;
                    break;
                }
                break;
            }
            if (switch_state_var)
                break;
        }
    switch (loop_state_var) {
      case 0U:
        {
            *(uint64_t *)(local_sp_5 + (-16L)) = 4213910UL;
            indirect_placeholder_10(r9_4, r8_4);
            abort();
        }
        break;
      case 1U:
        {
            indirect_placeholder();
            return;
        }
        break;
      case 2U:
        {
            *(uint64_t *)(local_sp_4 + (-8L)) = 4213367UL;
            indirect_placeholder_86(0UL, 0UL, r9_3, storemerge1, rdx_0, r8_3, 0UL);
            *(uint64_t *)(local_sp_4 + (-16L)) = 4213377UL;
            indirect_placeholder_78(rbx_2, 1UL, rbp_0);
            abort();
        }
        break;
    }
}

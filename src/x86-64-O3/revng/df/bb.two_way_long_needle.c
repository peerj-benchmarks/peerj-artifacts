typedef unsigned char __u_char;
typedef unsigned short __u_short;
typedef unsigned int __u_int;
typedef unsigned long __u_long;
typedef signed char __int8_t;
typedef unsigned char __uint8_t;
typedef short __int16_t;
typedef unsigned short __uint16_t;
typedef int __int32_t;
typedef unsigned int __uint32_t;
typedef long __int64_t;
typedef unsigned long __uint64_t;
typedef __int8_t __int_least8_t;
typedef __uint8_t __uint_least8_t;
typedef __int16_t __int_least16_t;
typedef __uint16_t __uint_least16_t;
typedef __int32_t __int_least32_t;
typedef __uint32_t __uint_least32_t;
typedef __int64_t __int_least64_t;
typedef __uint64_t __uint_least64_t;
typedef long __quad_t;
typedef unsigned long __u_quad_t;
typedef long __intmax_t;
typedef unsigned long __uintmax_t;
typedef unsigned long __dev_t;
typedef unsigned int __uid_t;
typedef unsigned int __gid_t;
typedef unsigned long __ino_t;
typedef unsigned long __ino64_t;
typedef unsigned int __mode_t;
typedef unsigned long __nlink_t;
typedef long __off_t;
typedef long __off64_t;
typedef int __pid_t;
typedef struct {
    int __val[2];
} __fsid_t;
typedef long __clock_t;
typedef unsigned long __rlim_t;
typedef unsigned long __rlim64_t;
typedef unsigned int __id_t;
typedef long __time_t;
typedef unsigned int __useconds_t;
typedef long __suseconds_t;
typedef int __daddr_t;
typedef int __key_t;
typedef int __clockid_t;
typedef void *__timer_t;
typedef long __blksize_t;
typedef long __blkcnt_t;
typedef long __blkcnt64_t;
typedef unsigned long __fsblkcnt_t;
typedef unsigned long __fsblkcnt64_t;
typedef unsigned long __fsfilcnt_t;
typedef unsigned long __fsfilcnt64_t;
typedef long __fsword_t;
typedef long __ssize_t;
typedef long __syscall_slong_t;
typedef unsigned long __syscall_ulong_t;
typedef __off64_t __loff_t;
typedef char *__caddr_t;
typedef long __intptr_t;
typedef unsigned int __socklen_t;
typedef int __sig_atomic_t;
typedef __int8_t int8_t;
typedef __int16_t int16_t;
typedef __int32_t int32_t;
typedef __int64_t int64_t;
typedef __uint8_t uint8_t;
typedef __uint16_t uint16_t;
typedef __uint32_t uint32_t;
typedef __uint64_t uint64_t;
typedef __int_least8_t int_least8_t;
typedef __int_least16_t int_least16_t;
typedef __int_least32_t int_least32_t;
typedef __int_least64_t int_least64_t;
typedef __uint_least8_t uint_least8_t;
typedef __uint_least16_t uint_least16_t;
typedef __uint_least32_t uint_least32_t;
typedef __uint_least64_t uint_least64_t;
typedef signed char int_fast8_t;
typedef long int_fast16_t;
typedef long int_fast32_t;
typedef long int_fast64_t;
typedef unsigned char uint_fast8_t;
typedef unsigned long uint_fast16_t;
typedef unsigned long uint_fast32_t;
typedef unsigned long uint_fast64_t;
typedef long intptr_t;
typedef unsigned long uintptr_t;
typedef __intmax_t intmax_t;
typedef __uintmax_t uintmax_t;
typedef _Bool bool;
struct indirect_placeholder_166_ret_type;
struct indirect_placeholder_167_ret_type;
struct helper_punpcklqdq_xmm_wrapper_ret_type;
struct type_6;
struct type_8;
struct indirect_placeholder_166_ret_type {
    uint64_t field_0;
    uint64_t field_1;
};
struct indirect_placeholder_167_ret_type {
    uint64_t field_0;
    uint64_t field_1;
};
struct helper_punpcklqdq_xmm_wrapper_ret_type {
    uint64_t field_0;
    uint64_t field_1;
    uint64_t field_2;
    uint64_t field_3;
    uint64_t field_4;
    uint64_t field_5;
    uint64_t field_6;
    uint64_t field_7;
};
struct type_6 {
};
struct type_8 {
};
extern uint64_t revng_init_local_sp(uint64_t param_0);
extern uint64_t init_rbx(void);
extern uint64_t init_rbp(void);
extern void indirect_placeholder(void);
extern uint64_t init_cc_src2(void);
extern uint64_t helper_cc_compute_c_wrapper(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint32_t param_3);
extern uint64_t init_r13(void);
extern uint64_t init_r12(void);
extern uint64_t init_r14(void);
extern uint64_t helper_cc_compute_all_wrapper(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint32_t param_3);
extern uint64_t init_r15(void);
extern struct indirect_placeholder_166_ret_type indirect_placeholder_166(uint64_t param_0, uint64_t param_1, uint64_t param_2);
extern struct indirect_placeholder_167_ret_type indirect_placeholder_167(uint64_t param_0, uint64_t param_1, uint64_t param_2);
extern struct helper_punpcklqdq_xmm_wrapper_ret_type helper_punpcklqdq_xmm_wrapper(struct type_6 *param_0, struct type_8 *param_1, struct type_8 *param_2, uint64_t param_3);
void bb_two_way_long_needle(uint64_t rdi, uint64_t rcx, uint64_t rdx, uint64_t rsi) {
    uint64_t rdx3_0;
    uint64_t var_61;
    uint64_t var_0;
    uint64_t var_1;
    uint64_t var_2;
    uint64_t var_3;
    uint64_t var_4;
    uint64_t var_5;
    uint64_t var_6;
    uint64_t var_7;
    uint64_t var_8;
    uint64_t var_9;
    uint64_t var_10;
    uint64_t *var_11;
    uint64_t *var_12;
    uint64_t var_13;
    uint64_t var_54;
    uint64_t *var_56;
    uint64_t var_57;
    uint64_t local_sp_1;
    uint64_t var_58;
    uint64_t var_59;
    uint64_t var_60;
    uint64_t rdx3_1;
    uint64_t var_62;
    uint64_t var_63;
    uint64_t rbp_0;
    uint64_t var_64;
    uint64_t r12_1;
    uint64_t rdx3_2;
    uint64_t var_65;
    uint64_t var_66;
    uint64_t var_55;
    uint64_t var_75;
    uint64_t var_76;
    uint64_t var_77;
    uint64_t r8_0;
    uint64_t *var_81;
    uint64_t var_82;
    uint64_t var_83;
    uint64_t var_84;
    uint64_t var_85;
    uint64_t local_sp_0;
    uint64_t rax_0;
    uint64_t var_86;
    uint64_t rax_3;
    uint64_t var_87;
    uint64_t var_88;
    uint64_t var_89;
    uint64_t var_90;
    uint64_t var_91;
    uint64_t var_92;
    uint64_t rax_1;
    uint64_t rax_2;
    uint64_t rbp_2;
    uint64_t var_93;
    uint64_t var_94;
    uint64_t var_78;
    uint64_t var_79;
    uint64_t rbp_1;
    uint64_t var_80;
    uint64_t storemerge;
    uint64_t r12_0;
    uint64_t rbp_3;
    uint64_t var_67;
    uint64_t var_68;
    uint64_t *var_69;
    uint64_t *var_70;
    uint64_t var_71;
    uint64_t var_72;
    struct indirect_placeholder_166_ret_type var_73;
    uint64_t var_74;
    uint64_t var_45;
    uint64_t var_46;
    uint64_t rbp_4;
    uint64_t var_47;
    uint64_t var_48;
    uint64_t *var_49;
    uint64_t var_50;
    uint64_t var_51;
    struct indirect_placeholder_167_ret_type var_52;
    uint64_t var_53;
    uint64_t rdi1_0_ph;
    uint64_t rsi4_0_ph;
    uint64_t rdx3_3;
    uint64_t rsi4_1;
    uint64_t r8_1;
    uint64_t rsi4_0;
    uint64_t var_14;
    uint64_t r8_3;
    uint64_t rax_6;
    uint64_t rax_4;
    uint64_t rdx3_4;
    uint64_t r8_2;
    uint64_t var_15;
    uint64_t var_16;
    uint64_t var_17;
    uint64_t var_18;
    uint64_t var_19;
    uint64_t var_95;
    bool var_96;
    uint64_t rcx2_0_ph;
    uint64_t rsi4_2_ph;
    uint64_t var_20;
    uint64_t rdx3_5;
    uint64_t rsi4_2;
    uint64_t var_21;
    uint64_t rax_5;
    uint64_t rdx3_6;
    uint64_t rsi4_3;
    unsigned char var_22;
    unsigned char var_23;
    uint64_t var_27;
    uint64_t var_24;
    uint64_t var_25;
    bool var_26;
    uint64_t var_28;
    uint64_t var_29;
    uint64_t var_30;
    uint64_t var_31;
    uint64_t *var_32;
    uint64_t var_33;
    uint64_t var_34;
    struct helper_punpcklqdq_xmm_wrapper_ret_type var_35;
    uint64_t var_36;
    uint64_t var_37;
    uint64_t rax_7;
    uint64_t var_38;
    uint64_t var_39;
    uint64_t var_40;
    uint64_t var_41;
    uint64_t var_42;
    bool var_43;
    uint64_t var_44;
    unsigned int loop_state_var;
    bool switch_state_var;
    var_0 = revng_init_local_sp(0UL);
    var_1 = init_r15();
    var_2 = init_r13();
    var_3 = init_rbx();
    var_4 = init_r12();
    var_5 = init_rbp();
    var_6 = init_cc_src2();
    var_7 = init_r14();
    *(uint64_t *)(var_0 + (-8L)) = var_1;
    *(uint64_t *)(var_0 + (-16L)) = var_7;
    var_8 = rcx + (-1L);
    *(uint64_t *)(var_0 + (-24L)) = var_2;
    *(uint64_t *)(var_0 + (-32L)) = var_4;
    *(uint64_t *)(var_0 + (-40L)) = var_5;
    *(uint64_t *)(var_0 + (-48L)) = var_3;
    var_9 = var_0 + (-2168L);
    var_10 = rcx + (-2L);
    var_11 = (uint64_t *)(var_0 + (-2160L));
    *var_11 = var_8;
    var_12 = (uint64_t *)(var_0 + (-2136L));
    *var_12 = 1UL;
    var_13 = helper_cc_compute_all_wrapper(var_10, 2UL, var_6, 17U);
    r12_1 = rsi;
    r8_0 = 0UL;
    storemerge = 0UL;
    r12_0 = rsi;
    rbp_3 = 0UL;
    rbp_4 = 0UL;
    rdi1_0_ph = 18446744073709551615UL;
    rsi4_0_ph = 0UL;
    rdx3_3 = 1UL;
    r8_1 = 1UL;
    rcx2_0_ph = 18446744073709551615UL;
    rsi4_2_ph = 0UL;
    rdx3_5 = 1UL;
    rax_7 = 0UL;
    if ((var_13 & 65UL) != 0UL) {
        while (1U)
            {
                rsi4_0 = rsi4_0_ph;
                while (1U)
                    {
                        var_14 = rdx3_3 + rsi4_0;
                        rsi4_1 = rsi4_0;
                        r8_3 = r8_1;
                        rax_4 = var_14;
                        rdx3_4 = rdx3_3;
                        r8_2 = r8_1;
                        if (var_14 >= rcx) {
                            loop_state_var = 1U;
                            break;
                        }
                        while (1U)
                            {
                                var_15 = (uint64_t)*(unsigned char *)(rdi1_0_ph + (rdx3_4 + rdx));
                                var_16 = (uint64_t)*(unsigned char *)(rax_4 + rdx) - var_15;
                                var_17 = helper_cc_compute_c_wrapper(var_16, var_15, var_6, 14U);
                                rdi1_0_ph = rsi4_1;
                                r8_1 = r8_2;
                                rdx3_4 = 1UL;
                                rsi4_1 = rax_4;
                                if (var_17 != 0UL) {
                                    loop_state_var = 0U;
                                    break;
                                }
                                var_18 = rax_4 + 1UL;
                                var_19 = rax_4 - rdi1_0_ph;
                                rax_4 = var_18;
                                r8_2 = var_19;
                                r8_3 = var_19;
                                if (var_18 < rcx) {
                                    continue;
                                }
                                loop_state_var = 1U;
                                break;
                            }
                        switch_state_var = 0;
                        switch (loop_state_var) {
                          case 0U:
                            {
                                var_95 = helper_cc_compute_all_wrapper(var_16, var_15, var_6, 14U);
                                if ((var_95 & 64UL) != 0UL) {
                                    loop_state_var = 0U;
                                    switch_state_var = 1;
                                    break;
                                }
                                var_96 = (rdx3_4 == r8_2);
                                rdx3_3 = var_96 ? 1UL : (rdx3_4 + 1UL);
                                rsi4_0 = var_96 ? rax_4 : rsi4_1;
                                continue;
                            }
                            break;
                          case 1U:
                            {
                                loop_state_var = 1U;
                                switch_state_var = 1;
                                break;
                            }
                            break;
                        }
                        if (switch_state_var)
                            break;
                    }
                switch_state_var = 0;
                switch (loop_state_var) {
                  case 1U:
                    {
                        switch_state_var = 1;
                        break;
                    }
                    break;
                  case 0U:
                    {
                        rsi4_0_ph = rsi4_1 + 1UL;
                        continue;
                    }
                    break;
                }
                if (switch_state_var)
                    break;
            }
        *var_12 = 1UL;
        while (1U)
            {
                var_20 = rcx2_0_ph + rdx;
                rsi4_2 = rsi4_2_ph;
                while (1U)
                    {
                        var_21 = rdx3_5 + rsi4_2;
                        rax_5 = var_21;
                        rdx3_6 = rdx3_5;
                        rsi4_3 = rsi4_2;
                        if (var_21 >= rcx) {
                            loop_state_var = 0U;
                            break;
                        }
                        while (1U)
                            {
                                var_22 = *(unsigned char *)(rdx3_6 + var_20);
                                var_23 = *(unsigned char *)(rax_5 + rdx);
                                rcx2_0_ph = rsi4_3;
                                rdx3_6 = 1UL;
                                rsi4_3 = rax_5;
                                if (var_22 >= var_23) {
                                    loop_state_var = 1U;
                                    break;
                                }
                                *var_12 = (rax_5 - rcx2_0_ph);
                                var_27 = rax_5 + 1UL;
                                rax_5 = var_27;
                                if (var_27 < rcx) {
                                    continue;
                                }
                                loop_state_var = 0U;
                                break;
                            }
                        switch_state_var = 0;
                        switch (loop_state_var) {
                          case 1U:
                            {
                                var_24 = (uint64_t)var_22;
                                var_25 = helper_cc_compute_all_wrapper((uint64_t)var_23 - var_24, var_24, var_6, 14U);
                                if ((var_25 & 64UL) != 0UL) {
                                    loop_state_var = 1U;
                                    switch_state_var = 1;
                                    break;
                                }
                                var_26 = (rdx3_6 == *var_12);
                                rdx3_5 = var_26 ? 1UL : (rdx3_6 + 1UL);
                                rsi4_2 = var_26 ? rax_5 : rsi4_3;
                                continue;
                            }
                            break;
                          case 0U:
                            {
                                loop_state_var = 0U;
                                switch_state_var = 1;
                                break;
                            }
                            break;
                        }
                        if (switch_state_var)
                            break;
                    }
                switch_state_var = 0;
                switch (loop_state_var) {
                  case 0U:
                    {
                        switch_state_var = 1;
                        break;
                    }
                    break;
                  case 1U:
                    {
                        *var_12 = 1UL;
                        rsi4_2_ph = rsi4_3 + 1UL;
                        continue;
                    }
                    break;
                }
                if (switch_state_var)
                    break;
            }
        var_28 = rcx2_0_ph + 1UL;
        var_29 = rdi1_0_ph + 1UL;
        var_30 = var_28 - var_29;
        *var_11 = var_28;
        var_31 = helper_cc_compute_c_wrapper(var_30, var_29, var_6, 17U);
        if (var_31 == 0UL) {
            *var_12 = r8_3;
            *var_11 = var_29;
        }
    }
    var_32 = (uint64_t *)(var_0 + (-2152L));
    *var_32 = rcx;
    var_33 = var_0 + (-2104L);
    var_34 = var_0 + (-56L);
    var_35 = helper_punpcklqdq_xmm_wrapper((struct type_6 *)(0UL), (struct type_8 *)(776UL), (struct type_8 *)(776UL), rcx);
    var_36 = var_35.field_1;
    rax_6 = var_33;
    *(uint64_t *)rax_6 = var_35.field_0;
    *(uint64_t *)(rax_6 + 8UL) = var_36;
    var_37 = rax_6 + 16UL;
    rax_6 = var_37;
    do {
        *(uint64_t *)rax_6 = var_35.field_0;
        *(uint64_t *)(rax_6 + 8UL) = var_36;
        var_37 = rax_6 + 16UL;
        rax_6 = var_37;
    } while (var_37 != var_34);
    if (rcx == 0UL) {
        var_38 = (uint64_t)*(unsigned char *)(rax_7 + rdx);
        var_39 = (rax_7 ^ (-1L)) + rcx;
        var_40 = rax_7 + 1UL;
        *(uint64_t *)(((var_38 << 3UL) + var_9) + 64UL) = var_39;
        rax_7 = var_40;
        do {
            var_38 = (uint64_t)*(unsigned char *)(rax_7 + rdx);
            var_39 = (rax_7 ^ (-1L)) + rcx;
            var_40 = rax_7 + 1UL;
            *(uint64_t *)(((var_38 << 3UL) + var_9) + 64UL) = var_39;
            rax_7 = var_40;
        } while (var_40 != rcx);
    }
    var_41 = *var_12;
    var_42 = var_0 + (-2176L);
    *(uint64_t *)var_42 = 4248574UL;
    indirect_placeholder();
    var_43 = ((uint64_t)(uint32_t)var_41 == 0UL);
    var_44 = *(uint64_t *)var_9;
    local_sp_0 = var_42;
    local_sp_1 = var_42;
    if (var_43) {
        *(uint64_t *)(var_0 + (-2128L)) = (var_44 + (-1L));
        *var_12 = (rcx - *(uint64_t *)(var_0 + (-2144L)));
        *(uint64_t *)(var_0 + (-2120L)) = (1UL - var_44);
        var_67 = rbp_3 + rcx;
        var_68 = r12_0 + rdi;
        var_69 = (uint64_t *)(local_sp_0 + 24UL);
        *var_69 = r8_0;
        var_70 = (uint64_t *)(local_sp_0 + 16UL);
        *var_70 = var_67;
        var_71 = var_67 - r12_0;
        var_72 = local_sp_0 + (-8L);
        *(uint64_t *)var_72 = 4248698UL;
        var_73 = indirect_placeholder_166(var_68, var_71, 0UL);
        var_74 = *(uint64_t *)(local_sp_0 + 8UL);
        rbp_1 = rbp_3;
        r12_0 = var_74;
        local_sp_0 = var_72;
        while (var_74 != 0UL)
            {
                var_75 = var_73.field_0;
                var_76 = *var_70;
                rax_2 = var_76;
                if (var_75 == 0UL) {
                    break;
                }
                var_77 = *(uint64_t *)((((uint64_t)*(unsigned char *)((var_74 + rdi) + (-1L)) << 3UL) + var_72) + 64UL);
                rax_3 = var_77;
                if (var_77 != 0UL) {
                    var_78 = *var_69;
                    var_79 = helper_cc_compute_c_wrapper(var_77 - var_78, var_78, var_6, 17U);
                    if (var_79 == 0UL) {
                        var_80 = (var_76 == 0UL) ? var_77 : *(uint64_t *)(local_sp_0 + 32UL);
                        rax_3 = var_80;
                    }
                    rbp_2 = rbp_1 + rax_3;
                    rbp_3 = rbp_2;
                    r8_0 = storemerge;
                    var_67 = rbp_3 + rcx;
                    var_68 = r12_0 + rdi;
                    var_69 = (uint64_t *)(local_sp_0 + 24UL);
                    *var_69 = r8_0;
                    var_70 = (uint64_t *)(local_sp_0 + 16UL);
                    *var_70 = var_67;
                    var_71 = var_67 - r12_0;
                    var_72 = local_sp_0 + (-8L);
                    *(uint64_t *)var_72 = 4248698UL;
                    var_73 = indirect_placeholder_166(var_68, var_71, 0UL);
                    var_74 = *(uint64_t *)(local_sp_0 + 8UL);
                    rbp_1 = rbp_3;
                    r12_0 = var_74;
                    local_sp_0 = var_72;
                    continue;
                }
                var_81 = (uint64_t *)local_sp_0;
                var_82 = *var_81;
                var_83 = helper_cc_compute_c_wrapper(var_76 - var_82, var_82, var_6, 17U);
                var_84 = (var_83 == 0UL) ? var_76 : var_82;
                var_85 = helper_cc_compute_c_wrapper(var_84 - var_8, var_8, var_6, 17U);
                rax_0 = var_84;
                if (var_85 != 0UL) {
                    while (1U)
                        {
                            rax_3 = rax_0;
                            if ((uint64_t)(*(unsigned char *)(rax_0 + rdx) - *(unsigned char *)(rax_0 + (rbp_3 + rdi))) != 0UL) {
                                loop_state_var = 1U;
                                break;
                            }
                            var_87 = rax_0 + 1UL;
                            var_88 = helper_cc_compute_c_wrapper(var_87 - var_8, var_8, var_6, 17U);
                            rax_0 = var_87;
                            if (var_88 == 0UL) {
                                continue;
                            }
                            loop_state_var = 0U;
                            break;
                        }
                    switch (loop_state_var) {
                      case 0U:
                        {
                            break;
                        }
                        break;
                      case 1U:
                        {
                            var_86 = rbp_3 + *(uint64_t *)(local_sp_0 + 48UL);
                            rbp_1 = var_86;
                            rbp_2 = rbp_1 + rax_3;
                            rbp_3 = rbp_2;
                            r8_0 = storemerge;
                            var_67 = rbp_3 + rcx;
                            var_68 = r12_0 + rdi;
                            var_69 = (uint64_t *)(local_sp_0 + 24UL);
                            *var_69 = r8_0;
                            var_70 = (uint64_t *)(local_sp_0 + 16UL);
                            *var_70 = var_67;
                            var_71 = var_67 - r12_0;
                            var_72 = local_sp_0 + (-8L);
                            *(uint64_t *)var_72 = 4248698UL;
                            var_73 = indirect_placeholder_166(var_68, var_71, 0UL);
                            var_74 = *(uint64_t *)(local_sp_0 + 8UL);
                            rbp_1 = rbp_3;
                            r12_0 = var_74;
                            local_sp_0 = var_72;
                            continue;
                        }
                        break;
                    }
                }
                var_89 = *var_81;
                var_90 = helper_cc_compute_c_wrapper(var_76 - var_89, var_89, var_6, 17U);
                if (var_90 == 0UL) {
                    rax_2 = *var_81;
                } else {
                    var_91 = *(uint64_t *)(local_sp_0 + 40UL);
                    var_92 = rbp_3 + rdi;
                    rax_1 = var_91;
                    if ((uint64_t)(*(unsigned char *)(var_91 + rdx) - *(unsigned char *)(var_91 + var_92)) != 0UL) {
                        while (rax_1 != var_76)
                            {
                                rax_2 = rax_1;
                                if ((uint64_t)(*(unsigned char *)((rax_1 + rdx) + (-1L)) - *(unsigned char *)((var_92 + rax_1) + (-1L))) == 0UL) {
                                    break;
                                }
                                rax_1 = rax_1 + (-1L);
                            }
                    }
                    rax_2 = *var_81;
                }
                if ((var_76 + 1UL) <= rax_2) {
                    break;
                }
                var_93 = rbp_3 + *var_69;
                var_94 = *(uint64_t *)(local_sp_0 + 32UL);
                rbp_2 = var_93;
                storemerge = var_94;
            }
    } else {
        var_45 = rcx - var_44;
        var_46 = helper_cc_compute_c_wrapper(var_45 - var_44, var_44, var_6, 17U);
        *var_12 = (((var_46 == 0UL) ? var_45 : var_44) + 1UL);
        *var_32 = (var_44 + (-1L));
        *(uint64_t *)(var_0 + (-2144L)) = (1UL - var_44);
        var_47 = rbp_4 + rcx;
        var_48 = r12_1 + rdi;
        var_49 = (uint64_t *)(local_sp_1 + 16UL);
        *var_49 = var_47;
        var_50 = var_47 - r12_1;
        var_51 = local_sp_1 + (-8L);
        *(uint64_t *)var_51 = 4248988UL;
        var_52 = indirect_placeholder_167(var_48, var_50, 0UL);
        var_53 = *(uint64_t *)(local_sp_1 + 8UL);
        r12_1 = var_53;
        local_sp_1 = var_51;
        while (!((var_53 != 0UL) && (var_52.field_0 == 0UL)))
            {
                var_54 = *(uint64_t *)((((uint64_t)*(unsigned char *)((var_53 + rdi) + (-1L)) << 3UL) + var_51) + 64UL);
                if (var_54 != 0UL) {
                    var_55 = rbp_4 + var_54;
                    rbp_0 = var_55;
                    rbp_4 = rbp_0;
                    var_47 = rbp_4 + rcx;
                    var_48 = r12_1 + rdi;
                    var_49 = (uint64_t *)(local_sp_1 + 16UL);
                    *var_49 = var_47;
                    var_50 = var_47 - r12_1;
                    var_51 = local_sp_1 + (-8L);
                    *(uint64_t *)var_51 = 4248988UL;
                    var_52 = indirect_placeholder_167(var_48, var_50, 0UL);
                    var_53 = *(uint64_t *)(local_sp_1 + 8UL);
                    r12_1 = var_53;
                    local_sp_1 = var_51;
                    continue;
                }
                var_56 = (uint64_t *)local_sp_1;
                var_57 = *var_56 - var_8;
                var_58 = rbp_4 + rdi;
                var_59 = helper_cc_compute_c_wrapper(var_57, var_8, var_6, 17U);
                if (var_59 != 0UL) {
                    var_60 = *var_56;
                    rdx3_0 = var_60;
                    rdx3_1 = var_60;
                    if ((uint64_t)(*(unsigned char *)(var_60 + rdx) - *(unsigned char *)(var_60 + var_58)) != 0UL) {
                        var_63 = rdx3_1 + (rbp_4 + *(uint64_t *)(local_sp_1 + 24UL));
                        rbp_0 = var_63;
                        rbp_4 = rbp_0;
                        var_47 = rbp_4 + rcx;
                        var_48 = r12_1 + rdi;
                        var_49 = (uint64_t *)(local_sp_1 + 16UL);
                        *var_49 = var_47;
                        var_50 = var_47 - r12_1;
                        var_51 = local_sp_1 + (-8L);
                        *(uint64_t *)var_51 = 4248988UL;
                        var_52 = indirect_placeholder_167(var_48, var_50, 0UL);
                        var_53 = *(uint64_t *)(local_sp_1 + 8UL);
                        r12_1 = var_53;
                        local_sp_1 = var_51;
                        continue;
                    }
                    while (1U)
                        {
                            var_61 = rdx3_0 + 1UL;
                            var_62 = helper_cc_compute_c_wrapper(var_61 - var_8, var_8, var_6, 17U);
                            rdx3_0 = var_61;
                            rdx3_1 = var_61;
                            if (var_62 != 0UL) {
                                loop_state_var = 1U;
                                break;
                            }
                            if ((uint64_t)(*(unsigned char *)(var_61 + rdx) - *(unsigned char *)(var_61 + var_58)) == 0UL) {
                                continue;
                            }
                            loop_state_var = 0U;
                            break;
                        }
                    switch (loop_state_var) {
                      case 1U:
                        {
                            break;
                        }
                        break;
                      case 0U:
                        {
                            var_63 = rdx3_1 + (rbp_4 + *(uint64_t *)(local_sp_1 + 24UL));
                            rbp_0 = var_63;
                            rbp_4 = rbp_0;
                            var_47 = rbp_4 + rcx;
                            var_48 = r12_1 + rdi;
                            var_49 = (uint64_t *)(local_sp_1 + 16UL);
                            *var_49 = var_47;
                            var_50 = var_47 - r12_1;
                            var_51 = local_sp_1 + (-8L);
                            *(uint64_t *)var_51 = 4248988UL;
                            var_52 = indirect_placeholder_167(var_48, var_50, 0UL);
                            var_53 = *(uint64_t *)(local_sp_1 + 8UL);
                            r12_1 = var_53;
                            local_sp_1 = var_51;
                            continue;
                        }
                        break;
                    }
                }
                var_64 = *var_49;
                rdx3_2 = var_64;
                if (var_64 == 18446744073709551615UL) {
                    break;
                }
                if ((uint64_t)(*(unsigned char *)(var_64 + rdx) - *(unsigned char *)(var_64 + var_58)) != 0UL) {
                    while (1U)
                        {
                            var_65 = rdx3_2 + (-1L);
                            rdx3_2 = var_65;
                            if (rdx3_2 != 0UL) {
                                loop_state_var = 0U;
                                break;
                            }
                            if ((uint64_t)(*(unsigned char *)(var_65 + rdx) - *(unsigned char *)(var_65 + var_58)) == 0UL) {
                                continue;
                            }
                            loop_state_var = 1U;
                            break;
                        }
                    switch_state_var = 0;
                    switch (loop_state_var) {
                      case 1U:
                        {
                            break;
                        }
                        break;
                      case 0U:
                        {
                            switch_state_var = 1;
                            break;
                        }
                        break;
                    }
                    if (switch_state_var)
                        break;
                }
                var_66 = rbp_4 + *(uint64_t *)(local_sp_1 + 32UL);
                rbp_0 = var_66;
            }
    }
    return;
}

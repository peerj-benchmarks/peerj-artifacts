typedef unsigned char __u_char;
typedef unsigned short __u_short;
typedef unsigned int __u_int;
typedef unsigned long __u_long;
typedef signed char __int8_t;
typedef unsigned char __uint8_t;
typedef short __int16_t;
typedef unsigned short __uint16_t;
typedef int __int32_t;
typedef unsigned int __uint32_t;
typedef long __int64_t;
typedef unsigned long __uint64_t;
typedef __int8_t __int_least8_t;
typedef __uint8_t __uint_least8_t;
typedef __int16_t __int_least16_t;
typedef __uint16_t __uint_least16_t;
typedef __int32_t __int_least32_t;
typedef __uint32_t __uint_least32_t;
typedef __int64_t __int_least64_t;
typedef __uint64_t __uint_least64_t;
typedef long __quad_t;
typedef unsigned long __u_quad_t;
typedef long __intmax_t;
typedef unsigned long __uintmax_t;
typedef unsigned long __dev_t;
typedef unsigned int __uid_t;
typedef unsigned int __gid_t;
typedef unsigned long __ino_t;
typedef unsigned long __ino64_t;
typedef unsigned int __mode_t;
typedef unsigned long __nlink_t;
typedef long __off_t;
typedef long __off64_t;
typedef int __pid_t;
typedef struct {
    int __val[2];
} __fsid_t;
typedef long __clock_t;
typedef unsigned long __rlim_t;
typedef unsigned long __rlim64_t;
typedef unsigned int __id_t;
typedef long __time_t;
typedef unsigned int __useconds_t;
typedef long __suseconds_t;
typedef int __daddr_t;
typedef int __key_t;
typedef int __clockid_t;
typedef void *__timer_t;
typedef long __blksize_t;
typedef long __blkcnt_t;
typedef long __blkcnt64_t;
typedef unsigned long __fsblkcnt_t;
typedef unsigned long __fsblkcnt64_t;
typedef unsigned long __fsfilcnt_t;
typedef unsigned long __fsfilcnt64_t;
typedef long __fsword_t;
typedef long __ssize_t;
typedef long __syscall_slong_t;
typedef unsigned long __syscall_ulong_t;
typedef __off64_t __loff_t;
typedef char *__caddr_t;
typedef long __intptr_t;
typedef unsigned int __socklen_t;
typedef int __sig_atomic_t;
typedef __int8_t int8_t;
typedef __int16_t int16_t;
typedef __int32_t int32_t;
typedef __int64_t int64_t;
typedef __uint8_t uint8_t;
typedef __uint16_t uint16_t;
typedef __uint32_t uint32_t;
typedef __uint64_t uint64_t;
typedef __int_least8_t int_least8_t;
typedef __int_least16_t int_least16_t;
typedef __int_least32_t int_least32_t;
typedef __int_least64_t int_least64_t;
typedef __uint_least8_t uint_least8_t;
typedef __uint_least16_t uint_least16_t;
typedef __uint_least32_t uint_least32_t;
typedef __uint_least64_t uint_least64_t;
typedef signed char int_fast8_t;
typedef long int_fast16_t;
typedef long int_fast32_t;
typedef long int_fast64_t;
typedef unsigned char uint_fast8_t;
typedef unsigned long uint_fast16_t;
typedef unsigned long uint_fast32_t;
typedef unsigned long uint_fast64_t;
typedef long intptr_t;
typedef unsigned long uintptr_t;
typedef __intmax_t intmax_t;
typedef __uintmax_t uintmax_t;
typedef _Bool bool;
extern uint64_t revng_init_local_sp(uint64_t param_0);
extern uint64_t init_rbx(void);
extern uint64_t init_rbp(void);
extern void indirect_placeholder(void);
extern uint64_t init_cc_src2(void);
extern uint64_t helper_cc_compute_c_wrapper(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint32_t param_3);
extern uint64_t init_r13(void);
extern uint64_t init_r12(void);
extern uint64_t init_r14(void);
extern uint64_t init_r15(void);
extern uint64_t helper_cc_compute_all_wrapper(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint32_t param_3);
extern uint64_t indirect_placeholder_6(uint64_t param_0);
extern uint64_t indirect_placeholder_5(uint64_t param_0, uint64_t param_1);
extern uint64_t indirect_placeholder_9(uint64_t param_0, uint64_t param_1, uint64_t param_2);
extern void indirect_placeholder_8(uint64_t param_0, uint64_t param_1);
void bb_fmt(uint64_t rdi) {
    uint64_t var_0;
    uint64_t var_1;
    uint64_t var_2;
    uint64_t var_3;
    uint64_t var_4;
    uint64_t var_5;
    uint64_t var_6;
    uint64_t var_7;
    uint64_t var_8;
    uint64_t var_9;
    uint64_t var_10;
    uint32_t var_11;
    uint64_t local_sp_7;
    uint64_t var_38;
    uint32_t var_39;
    uint64_t local_sp_5;
    uint64_t local_sp_19_ph;
    uint32_t var_40;
    uint64_t var_81;
    uint64_t local_sp_2;
    uint64_t var_68;
    uint32_t var_69;
    uint32_t var_70;
    uint64_t local_sp_3;
    uint64_t var_56;
    uint32_t var_57;
    uint32_t var_58;
    uint64_t local_sp_4;
    uint64_t local_sp_6;
    uint64_t local_sp_0;
    uint64_t rbp_0;
    uint64_t var_77;
    uint64_t var_78;
    uint64_t var_79;
    uint64_t var_80;
    uint64_t local_sp_1;
    uint64_t cc_src2_2;
    uint32_t var_32;
    uint64_t rcx_0;
    uint64_t rax_3;
    uint32_t var_33;
    uint64_t rax_0;
    uint64_t var_34;
    uint64_t var_35;
    uint64_t var_36;
    uint32_t var_37;
    unsigned char var_41;
    uint64_t var_46;
    uint64_t rdi1_0;
    uint32_t var_42;
    uint64_t var_43;
    uint32_t var_44;
    uint64_t var_45;
    unsigned char var_48;
    uint64_t r14_0;
    uint64_t var_47;
    uint64_t rcx_1;
    uint64_t cc_src2_0;
    uint64_t rcx_2;
    uint32_t var_59;
    uint64_t var_60;
    uint64_t cc_src2_1;
    uint64_t var_61;
    uint32_t var_62;
    uint32_t var_63;
    uint64_t rax_1;
    uint64_t var_64;
    uint64_t var_65;
    uint64_t var_66;
    uint32_t var_67;
    uint64_t var_49;
    uint32_t var_50;
    uint32_t var_51;
    uint64_t rax_2;
    uint64_t var_52;
    uint64_t var_53;
    uint64_t var_54;
    uint32_t var_55;
    uint64_t cc_src2_3;
    uint64_t var_71;
    uint64_t rax_4;
    uint64_t r12_0;
    unsigned char *var_73;
    uint64_t var_74;
    uint64_t var_75;
    uint64_t var_76;
    uint64_t var_72;
    uint64_t local_sp_11;
    uint64_t local_sp_13;
    uint64_t local_sp_15;
    uint64_t local_sp_10_ph;
    uint64_t r12_1;
    uint64_t cc_src2_4;
    uint64_t storemerge;
    uint64_t local_sp_8;
    uint64_t r15_0;
    uint64_t r12_2;
    uint64_t r14_1;
    uint32_t var_12;
    uint64_t var_13;
    uint32_t var_14;
    uint64_t var_15;
    bool var_16;
    uint64_t var_17;
    uint64_t _pre203;
    uint64_t _pre194_pre_phi;
    uint64_t var_18;
    uint64_t var_19;
    uint64_t var_20;
    uint64_t var_21;
    uint32_t var_22;
    uint64_t var_23;
    uint64_t var_24;
    uint64_t var_25;
    uint64_t _pre_phi195;
    uint64_t var_26;
    uint64_t var_27;
    uint64_t var_28;
    uint32_t var_29;
    uint64_t var_30;
    bool var_31;
    uint64_t _pre_phi191;
    uint64_t _pre_phi;
    uint64_t rax_5;
    uint64_t var_82;
    uint64_t var_83;
    uint64_t var_84;
    bool var_85;
    uint64_t var_86;
    uint64_t var_87;
    uint64_t r12_3;
    uint64_t var_88;
    uint64_t var_89;
    uint32_t var_90;
    uint64_t var_91;
    uint64_t local_sp_9;
    uint64_t var_92;
    uint32_t var_93;
    uint64_t var_94;
    bool var_95;
    uint64_t var_96;
    uint64_t var_97;
    uint64_t var_98;
    uint32_t var_99;
    uint64_t rax_7_ph;
    uint64_t rdx_0_ph;
    uint64_t local_sp_10;
    uint64_t rax_7;
    uint64_t rdx_0;
    uint32_t var_100;
    uint64_t var_101;
    uint64_t var_102;
    uint64_t local_sp_12;
    uint64_t r13_0;
    uint64_t var_103;
    uint32_t var_104;
    uint64_t var_105;
    uint32_t var_106;
    uint64_t var_107;
    uint64_t var_108;
    uint32_t var_109;
    uint64_t var_110;
    uint64_t var_111;
    uint64_t rax_8;
    uint64_t rbp_1;
    uint64_t rdx_1;
    uint64_t rbp_2;
    uint64_t _pre_phi201;
    uint64_t local_sp_14;
    uint64_t rdx_3;
    uint64_t r12_4;
    uint64_t var_131;
    uint64_t var_132;
    uint64_t rax_9;
    uint64_t rbp_3;
    uint64_t rdx_2;
    uint64_t local_sp_17;
    uint64_t var_112;
    uint64_t var_113;
    uint64_t var_114;
    uint64_t var_115;
    uint64_t var_116;
    uint64_t local_sp_16;
    uint64_t var_117;
    uint32_t var_118;
    bool var_119;
    uint64_t var_120;
    uint64_t var_121;
    uint64_t var_122;
    uint32_t var_123;
    uint64_t rax_10;
    uint64_t r12_5;
    uint64_t var_124;
    uint64_t local_sp_18;
    uint64_t var_125;
    uint64_t var_126;
    uint32_t var_127;
    uint64_t rax_11_ph;
    uint32_t var_128;
    uint64_t var_129;
    bool or_cond;
    uint64_t local_sp_19;
    uint64_t var_130;
    uint64_t _pre200;
    uint64_t var_133;
    uint32_t var_134;
    uint64_t var_135;
    uint32_t var_136;
    uint64_t var_137;
    unsigned int loop_state_var;
    bool switch_state_var;
    var_0 = revng_init_local_sp(0UL);
    var_1 = init_r15();
    var_2 = init_r13();
    var_3 = init_rbx();
    var_4 = init_r12();
    var_5 = init_rbp();
    var_6 = init_cc_src2();
    var_7 = init_r14();
    *(uint64_t *)(var_0 + (-8L)) = var_1;
    *(uint64_t *)(var_0 + (-16L)) = var_7;
    *(uint64_t *)(var_0 + (-24L)) = var_2;
    *(uint64_t *)(var_0 + (-32L)) = var_4;
    *(uint64_t *)(var_0 + (-40L)) = var_5;
    *(uint64_t *)(var_0 + (-48L)) = var_3;
    *(uint64_t *)(var_0 + (-80L)) = 4205691UL;
    var_8 = indirect_placeholder_9(rdi, rdi, 2UL);
    *(unsigned char *)6376216UL = (unsigned char)'\x00';
    *(uint32_t *)6376204UL = 0U;
    var_9 = var_0 + (-88L);
    *(uint64_t *)var_9 = 4205716UL;
    var_10 = indirect_placeholder_6(var_8);
    var_11 = (uint32_t)var_10;
    *(uint32_t *)6376200UL = var_11;
    local_sp_7 = var_9;
    rcx_0 = 0UL;
    r14_0 = var_7;
    r12_1 = var_4;
    cc_src2_4 = var_6;
    storemerge = (uint64_t)var_11;
    rdx_0_ph = 0UL;
    while (1U)
        {
            *(uint32_t *)6376192UL = 0U;
            cc_src2_2 = cc_src2_4;
            cc_src2_0 = cc_src2_4;
            cc_src2_1 = cc_src2_4;
            cc_src2_3 = cc_src2_4;
            local_sp_8 = local_sp_7;
            r15_0 = storemerge;
            r12_2 = r12_1;
            r14_1 = r14_0;
            while (1U)
                {
                    var_12 = (uint32_t)r15_0;
                    var_13 = (uint64_t)(var_12 + 1U);
                    var_14 = *(uint32_t *)6376196UL;
                    var_15 = (uint64_t)var_14;
                    var_16 = (var_13 == 0UL);
                    var_17 = (r14_1 & (-256L)) | var_16;
                    r14_0 = var_17;
                    local_sp_10_ph = local_sp_8;
                    r14_1 = var_17;
                    local_sp_9 = local_sp_8;
                    _pre_phi201 = var_13;
                    local_sp_14 = local_sp_8;
                    if (var_16) {
                        _pre203 = (uint64_t)(var_12 + (-10));
                        _pre194_pre_phi = _pre203;
                        var_81 = (uint64_t)*(uint32_t *)6421324UL;
                        _pre_phi195 = _pre194_pre_phi;
                        _pre_phi191 = var_81 << 32UL;
                        _pre_phi = var_15 << 32UL;
                        rax_5 = var_81;
                    } else {
                        var_18 = (uint64_t)(var_12 + (-10));
                        _pre194_pre_phi = var_18;
                        _pre_phi195 = var_18;
                        if (var_18 == 0UL) {
                            var_19 = (uint64_t)*(uint32_t *)6421336UL;
                            var_20 = var_15 << 32UL;
                            _pre_phi = var_20;
                            if ((long)var_20 >= (long)(var_19 << 32UL)) {
                                var_21 = (uint64_t)*(uint32_t *)6421340UL;
                                var_22 = *(uint32_t *)6421324UL;
                                var_23 = (uint64_t)var_22;
                                var_24 = (var_15 + var_21) << 32UL;
                                var_25 = var_23 << 32UL;
                                _pre_phi191 = var_25;
                                rax_5 = var_23;
                                if ((long)var_24 <= (long)var_25) {
                                    loop_state_var = 1U;
                                    break;
                                }
                            }
                            var_81 = (uint64_t)*(uint32_t *)6421324UL;
                            _pre_phi195 = _pre194_pre_phi;
                            _pre_phi191 = var_81 << 32UL;
                            _pre_phi = var_15 << 32UL;
                            rax_5 = var_81;
                        } else {
                            var_81 = (uint64_t)*(uint32_t *)6421324UL;
                            _pre_phi195 = _pre194_pre_phi;
                            _pre_phi191 = var_81 << 32UL;
                            _pre_phi = var_15 << 32UL;
                            rax_5 = var_81;
                        }
                    }
                }
            switch_state_var = 0;
            switch (loop_state_var) {
              case 0U:
                {
                    switch_state_var = 1;
                    break;
                }
                break;
              case 1U:
                {
                    var_26 = (uint64_t)var_12;
                    *(uint32_t *)6376212UL = var_14;
                    *(uint32_t *)6376208UL = var_22;
                    *(uint64_t *)6416256UL = 6416320UL;
                    *(uint64_t *)6376224UL = 6376256UL;
                    var_27 = local_sp_8 + (-8L);
                    *(uint64_t *)var_27 = 4206434UL;
                    var_28 = indirect_placeholder_5(var_8, var_26);
                    var_29 = *(uint32_t *)6376196UL;
                    var_30 = (uint64_t)var_29;
                    var_31 = ((uint64_t)(var_29 - *(uint32_t *)6376212UL) == 0UL);
                    local_sp_5 = var_27;
                    local_sp_2 = var_27;
                    local_sp_3 = var_27;
                    local_sp_4 = var_27;
                    rax_3 = var_28;
                    rax_0 = var_28;
                    rax_1 = var_28;
                    rax_2 = var_28;
                    if (!var_31 && (long)((var_30 + (uint64_t)*(uint32_t *)6421340UL) << 32UL) > (long)((uint64_t)*(uint32_t *)6421324UL << 32UL)) {
                        var_32 = (uint32_t)var_28;
                        rcx_0 = (((uint64_t)(var_32 + (-10)) != 0UL) && ((uint64_t)(var_32 + 1U) != 0UL));
                    }
                    if (*(unsigned char *)6421361UL != '\x00') {
                        if (*(unsigned char *)6421363UL != '\x00') {
                            *(uint32_t *)6376204UL = ((rcx_0 == 0UL) ? *(uint32_t *)6376208UL : *(uint32_t *)6421324UL);
                            var_33 = (uint32_t)var_28;
                            if (!var_31 & (long)((var_30 + (uint64_t)*(uint32_t *)6421340UL) << 32UL) <= (long)((uint64_t)*(uint32_t *)6421324UL << 32UL) & (uint64_t)(var_33 + (-10)) != 0UL && (uint64_t)(var_33 + 1U) == 0UL) {
                                var_34 = (uint64_t)(uint32_t)rax_0;
                                var_35 = local_sp_2 + (-8L);
                                *(uint64_t *)var_35 = 4207220UL;
                                var_36 = indirect_placeholder_5(var_8, var_34);
                                var_37 = *(uint32_t *)6376196UL;
                                local_sp_2 = var_35;
                                rax_0 = var_36;
                                local_sp_5 = var_35;
                                rax_3 = var_36;
                                while ((uint64_t)(var_37 - *(uint32_t *)6376212UL) != 0UL)
                                    {
                                        var_38 = (uint64_t)var_37 + (uint64_t)*(uint32_t *)6421340UL;
                                        var_39 = *(uint32_t *)6421324UL;
                                        if ((long)(var_38 << 32UL) > (long)((uint64_t)var_39 << 32UL)) {
                                            break;
                                        }
                                        var_40 = (uint32_t)var_36;
                                        if ((uint64_t)(var_40 + 1U) == 0UL) {
                                            break;
                                        }
                                        if ((uint64_t)(var_40 + (-10)) == 0UL) {
                                            break;
                                        }
                                        if ((uint64_t)(var_39 - *(uint32_t *)6376204UL) == 0UL) {
                                            break;
                                        }
                                        var_34 = (uint64_t)(uint32_t)rax_0;
                                        var_35 = local_sp_2 + (-8L);
                                        *(uint64_t *)var_35 = 4207220UL;
                                        var_36 = indirect_placeholder_5(var_8, var_34);
                                        var_37 = *(uint32_t *)6376196UL;
                                        local_sp_2 = var_35;
                                        rax_0 = var_36;
                                        local_sp_5 = var_35;
                                        rax_3 = var_36;
                                    }
                            }
                        }
                        var_41 = *(unsigned char *)6421362UL;
                        var_48 = var_41;
                        if (var_41 == '\x00') {
                            var_59 = *(uint32_t *)6376208UL;
                            var_60 = (uint64_t)var_59;
                            *(uint32_t *)6376204UL = var_59;
                            rcx_2 = var_60;
                            cc_src2_3 = cc_src2_1;
                            var_61 = var_30 + (uint64_t)*(uint32_t *)6421340UL;
                            var_62 = *(uint32_t *)6421324UL;
                            var_63 = (uint32_t)var_28;
                            if (!var_31 & (long)(var_61 << 32UL) <= (long)((uint64_t)var_62 << 32UL) & (uint64_t)(var_63 + (-10)) != 0UL & (uint64_t)(var_63 + 1U) != 0UL && (uint64_t)((uint32_t)rcx_2 - var_62) == 0UL) {
                                var_64 = (uint64_t)(uint32_t)rax_1;
                                var_65 = local_sp_3 + (-8L);
                                *(uint64_t *)var_65 = 4207009UL;
                                var_66 = indirect_placeholder_5(var_8, var_64);
                                var_67 = *(uint32_t *)6376196UL;
                                local_sp_3 = var_65;
                                rax_1 = var_66;
                                local_sp_5 = var_65;
                                rax_3 = var_66;
                                while ((uint64_t)(var_67 - *(uint32_t *)6376212UL) != 0UL)
                                    {
                                        var_68 = (uint64_t)var_67 + (uint64_t)*(uint32_t *)6421340UL;
                                        var_69 = *(uint32_t *)6421324UL;
                                        if ((long)(var_68 << 32UL) > (long)((uint64_t)var_69 << 32UL)) {
                                            break;
                                        }
                                        var_70 = (uint32_t)var_66;
                                        if ((uint64_t)(var_70 + 1U) == 0UL) {
                                            break;
                                        }
                                        if ((uint64_t)(var_70 + (-10)) == 0UL) {
                                            break;
                                        }
                                        if ((uint64_t)(var_69 - *(uint32_t *)6376204UL) == 0UL) {
                                            break;
                                        }
                                        var_64 = (uint64_t)(uint32_t)rax_1;
                                        var_65 = local_sp_3 + (-8L);
                                        *(uint64_t *)var_65 = 4207009UL;
                                        var_66 = indirect_placeholder_5(var_8, var_64);
                                        var_67 = *(uint32_t *)6376196UL;
                                        local_sp_3 = var_65;
                                        rax_1 = var_66;
                                        local_sp_5 = var_65;
                                        rax_3 = var_66;
                                    }
                            }
                        }
                        if (rcx_0 == 0UL) {
                            var_46 = (uint64_t)*(uint32_t *)6376208UL;
                            rdi1_0 = var_46;
                            if ((uint64_t)(*(uint32_t *)6376204UL - (uint32_t)rdi1_0) != 0UL) {
                                cc_src2_3 = cc_src2_2;
                                var_49 = var_30 + (uint64_t)*(uint32_t *)6421340UL;
                                var_50 = *(uint32_t *)6421324UL;
                                var_51 = (uint32_t)var_28;
                                if (!var_31 & (long)(var_49 << 32UL) <= (long)((uint64_t)var_50 << 32UL) & (uint64_t)(var_51 + (-10)) != 0UL & (uint64_t)(var_51 + 1U) != 0UL && (uint64_t)(var_50 - *(uint32_t *)6376208UL) == 0UL) {
                                    var_52 = (uint64_t)(uint32_t)rax_2;
                                    var_53 = local_sp_4 + (-8L);
                                    *(uint64_t *)var_53 = 4206900UL;
                                    var_54 = indirect_placeholder_5(var_8, var_52);
                                    var_55 = *(uint32_t *)6376196UL;
                                    local_sp_4 = var_53;
                                    rax_2 = var_54;
                                    local_sp_5 = var_53;
                                    rax_3 = var_54;
                                    while ((uint64_t)(var_55 - *(uint32_t *)6376212UL) != 0UL)
                                        {
                                            var_56 = (uint64_t)var_55 + (uint64_t)*(uint32_t *)6421340UL;
                                            var_57 = *(uint32_t *)6421324UL;
                                            if ((long)(var_56 << 32UL) > (long)((uint64_t)var_57 << 32UL)) {
                                                break;
                                            }
                                            var_58 = (uint32_t)var_54;
                                            if ((uint64_t)(var_58 + 1U) == 0UL) {
                                                break;
                                            }
                                            if ((uint64_t)(var_58 + (-10)) == 0UL) {
                                                break;
                                            }
                                            if ((uint64_t)(var_57 - *(uint32_t *)6376204UL) == 0UL) {
                                                break;
                                            }
                                            var_52 = (uint64_t)(uint32_t)rax_2;
                                            var_53 = local_sp_4 + (-8L);
                                            *(uint64_t *)var_53 = 4206900UL;
                                            var_54 = indirect_placeholder_5(var_8, var_52);
                                            var_55 = *(uint32_t *)6376196UL;
                                            local_sp_4 = var_53;
                                            rax_2 = var_54;
                                            local_sp_5 = var_53;
                                            rax_3 = var_54;
                                        }
                                }
                                var_71 = *(uint64_t *)6376224UL;
                                local_sp_6 = local_sp_5;
                                rax_4 = rax_3;
                                r12_0 = var_71;
                                cc_src2_4 = cc_src2_3;
                                if (var_71 > 6376256UL) {
                                    *(uint32_t *)(local_sp_5 + 12UL) = (uint32_t)rax_3;
                                    var_72 = local_sp_5 + (-8L);
                                    *(uint64_t *)var_72 = 4206696UL;
                                    indirect_placeholder();
                                    local_sp_6 = var_72;
                                    rax_4 = (uint64_t)*(uint32_t *)(local_sp_5 + 4UL);
                                    r12_0 = *(uint64_t *)6376224UL;
                                }
                                var_73 = (unsigned char *)(r12_0 + (-24L));
                                *var_73 = (*var_73 | '\n');
                                *(uint32_t *)6376200UL = (uint32_t)rax_4;
                                *(uint64_t *)(local_sp_6 + (-8L)) = 4206548UL;
                                indirect_placeholder();
                                var_74 = (uint64_t)*(uint32_t *)6376208UL;
                                var_75 = local_sp_6 + (-16L);
                                *(uint64_t *)var_75 = 4206564UL;
                                indirect_placeholder_8(6376256UL, var_74);
                                var_76 = *(uint64_t *)6376288UL;
                                local_sp_0 = var_75;
                                rbp_0 = var_76;
                                local_sp_1 = var_75;
                                r12_1 = r12_0;
                                if (var_76 == r12_0) {
                                    var_77 = (uint64_t)*(uint32_t *)6376204UL;
                                    var_78 = local_sp_0 + (-8L);
                                    *(uint64_t *)var_78 = 4206590UL;
                                    indirect_placeholder_8(rbp_0, var_77);
                                    var_79 = *(uint64_t *)(rbp_0 + 32UL);
                                    var_80 = helper_cc_compute_all_wrapper(var_79 - r12_0, r12_0, cc_src2_3, 17U);
                                    local_sp_0 = var_78;
                                    rbp_0 = var_79;
                                    local_sp_1 = var_78;
                                    do {
                                        var_77 = (uint64_t)*(uint32_t *)6376204UL;
                                        var_78 = local_sp_0 + (-8L);
                                        *(uint64_t *)var_78 = 4206590UL;
                                        indirect_placeholder_8(rbp_0, var_77);
                                        var_79 = *(uint64_t *)(rbp_0 + 32UL);
                                        var_80 = helper_cc_compute_all_wrapper(var_79 - r12_0, r12_0, cc_src2_3, 17U);
                                        local_sp_0 = var_78;
                                        rbp_0 = var_79;
                                        local_sp_1 = var_78;
                                    } while ((var_80 & 64UL) != 0UL);
                                }
                                local_sp_7 = local_sp_1;
                                storemerge = (uint64_t)*(uint32_t *)6376200UL;
                                continue;
                            }
                            var_47 = helper_cc_compute_c_wrapper(rdi1_0 + (-1L), 1UL, cc_src2_4, 16U);
                            var_48 = *(unsigned char *)6421362UL;
                            rcx_1 = (0UL - var_47) & 3UL;
                            cc_src2_0 = var_47;
                        } else {
                            var_42 = *(uint32_t *)6421324UL;
                            var_43 = (uint64_t)var_42;
                            var_44 = *(uint32_t *)6376208UL;
                            var_45 = (uint64_t)var_44;
                            rdi1_0 = var_45;
                            rcx_1 = var_43;
                            if ((uint64_t)(var_42 - var_44) != 0UL) {
                                if ((uint64_t)(*(uint32_t *)6376204UL - (uint32_t)rdi1_0) != 0UL) {
                                    cc_src2_3 = cc_src2_2;
                                    var_49 = var_30 + (uint64_t)*(uint32_t *)6421340UL;
                                    var_50 = *(uint32_t *)6421324UL;
                                    var_51 = (uint32_t)var_28;
                                    if (!var_31 & (long)(var_49 << 32UL) <= (long)((uint64_t)var_50 << 32UL) & (uint64_t)(var_51 + (-10)) != 0UL & (uint64_t)(var_51 + 1U) != 0UL && (uint64_t)(var_50 - *(uint32_t *)6376208UL) == 0UL) {
                                        var_52 = (uint64_t)(uint32_t)rax_2;
                                        var_53 = local_sp_4 + (-8L);
                                        *(uint64_t *)var_53 = 4206900UL;
                                        var_54 = indirect_placeholder_5(var_8, var_52);
                                        var_55 = *(uint32_t *)6376196UL;
                                        local_sp_4 = var_53;
                                        rax_2 = var_54;
                                        local_sp_5 = var_53;
                                        rax_3 = var_54;
                                        while ((uint64_t)(var_55 - *(uint32_t *)6376212UL) != 0UL)
                                            {
                                                var_56 = (uint64_t)var_55 + (uint64_t)*(uint32_t *)6421340UL;
                                                var_57 = *(uint32_t *)6421324UL;
                                                if ((long)(var_56 << 32UL) > (long)((uint64_t)var_57 << 32UL)) {
                                                    break;
                                                }
                                                var_58 = (uint32_t)var_54;
                                                if ((uint64_t)(var_58 + 1U) == 0UL) {
                                                    break;
                                                }
                                                if ((uint64_t)(var_58 + (-10)) == 0UL) {
                                                    break;
                                                }
                                                if ((uint64_t)(var_57 - *(uint32_t *)6376204UL) == 0UL) {
                                                    break;
                                                }
                                                var_52 = (uint64_t)(uint32_t)rax_2;
                                                var_53 = local_sp_4 + (-8L);
                                                *(uint64_t *)var_53 = 4206900UL;
                                                var_54 = indirect_placeholder_5(var_8, var_52);
                                                var_55 = *(uint32_t *)6376196UL;
                                                local_sp_4 = var_53;
                                                rax_2 = var_54;
                                                local_sp_5 = var_53;
                                                rax_3 = var_54;
                                            }
                                    }
                                    var_71 = *(uint64_t *)6376224UL;
                                    local_sp_6 = local_sp_5;
                                    rax_4 = rax_3;
                                    r12_0 = var_71;
                                    cc_src2_4 = cc_src2_3;
                                    if (var_71 > 6376256UL) {
                                        *(uint32_t *)(local_sp_5 + 12UL) = (uint32_t)rax_3;
                                        var_72 = local_sp_5 + (-8L);
                                        *(uint64_t *)var_72 = 4206696UL;
                                        indirect_placeholder();
                                        local_sp_6 = var_72;
                                        rax_4 = (uint64_t)*(uint32_t *)(local_sp_5 + 4UL);
                                        r12_0 = *(uint64_t *)6376224UL;
                                    }
                                    var_73 = (unsigned char *)(r12_0 + (-24L));
                                    *var_73 = (*var_73 | '\n');
                                    *(uint32_t *)6376200UL = (uint32_t)rax_4;
                                    *(uint64_t *)(local_sp_6 + (-8L)) = 4206548UL;
                                    indirect_placeholder();
                                    var_74 = (uint64_t)*(uint32_t *)6376208UL;
                                    var_75 = local_sp_6 + (-16L);
                                    *(uint64_t *)var_75 = 4206564UL;
                                    indirect_placeholder_8(6376256UL, var_74);
                                    var_76 = *(uint64_t *)6376288UL;
                                    local_sp_0 = var_75;
                                    rbp_0 = var_76;
                                    local_sp_1 = var_75;
                                    r12_1 = r12_0;
                                    if (var_76 == r12_0) {
                                        var_77 = (uint64_t)*(uint32_t *)6376204UL;
                                        var_78 = local_sp_0 + (-8L);
                                        *(uint64_t *)var_78 = 4206590UL;
                                        indirect_placeholder_8(rbp_0, var_77);
                                        var_79 = *(uint64_t *)(rbp_0 + 32UL);
                                        var_80 = helper_cc_compute_all_wrapper(var_79 - r12_0, r12_0, cc_src2_3, 17U);
                                        local_sp_0 = var_78;
                                        rbp_0 = var_79;
                                        local_sp_1 = var_78;
                                        do {
                                            var_77 = (uint64_t)*(uint32_t *)6376204UL;
                                            var_78 = local_sp_0 + (-8L);
                                            *(uint64_t *)var_78 = 4206590UL;
                                            indirect_placeholder_8(rbp_0, var_77);
                                            var_79 = *(uint64_t *)(rbp_0 + 32UL);
                                            var_80 = helper_cc_compute_all_wrapper(var_79 - r12_0, r12_0, cc_src2_3, 17U);
                                            local_sp_0 = var_78;
                                            rbp_0 = var_79;
                                            local_sp_1 = var_78;
                                        } while ((var_80 & 64UL) != 0UL);
                                    }
                                    local_sp_7 = local_sp_1;
                                    storemerge = (uint64_t)*(uint32_t *)6376200UL;
                                    continue;
                                }
                                var_47 = helper_cc_compute_c_wrapper(rdi1_0 + (-1L), 1UL, cc_src2_4, 16U);
                                var_48 = *(unsigned char *)6421362UL;
                                rcx_1 = (0UL - var_47) & 3UL;
                                cc_src2_0 = var_47;
                            }
                        }
                        *(uint32_t *)6376204UL = (uint32_t)rcx_1;
                        rcx_2 = rcx_1;
                        cc_src2_1 = cc_src2_0;
                        cc_src2_2 = cc_src2_0;
                        if (var_48 != '\x00') {
                            cc_src2_3 = cc_src2_2;
                            var_49 = var_30 + (uint64_t)*(uint32_t *)6421340UL;
                            var_50 = *(uint32_t *)6421324UL;
                            var_51 = (uint32_t)var_28;
                            if (!var_31 & (long)(var_49 << 32UL) <= (long)((uint64_t)var_50 << 32UL) & (uint64_t)(var_51 + (-10)) != 0UL & (uint64_t)(var_51 + 1U) != 0UL && (uint64_t)(var_50 - *(uint32_t *)6376208UL) == 0UL) {
                                var_52 = (uint64_t)(uint32_t)rax_2;
                                var_53 = local_sp_4 + (-8L);
                                *(uint64_t *)var_53 = 4206900UL;
                                var_54 = indirect_placeholder_5(var_8, var_52);
                                var_55 = *(uint32_t *)6376196UL;
                                local_sp_4 = var_53;
                                rax_2 = var_54;
                                local_sp_5 = var_53;
                                rax_3 = var_54;
                                while ((uint64_t)(var_55 - *(uint32_t *)6376212UL) != 0UL)
                                    {
                                        var_56 = (uint64_t)var_55 + (uint64_t)*(uint32_t *)6421340UL;
                                        var_57 = *(uint32_t *)6421324UL;
                                        if ((long)(var_56 << 32UL) > (long)((uint64_t)var_57 << 32UL)) {
                                            break;
                                        }
                                        var_58 = (uint32_t)var_54;
                                        if ((uint64_t)(var_58 + 1U) == 0UL) {
                                            break;
                                        }
                                        if ((uint64_t)(var_58 + (-10)) == 0UL) {
                                            break;
                                        }
                                        if ((uint64_t)(var_57 - *(uint32_t *)6376204UL) == 0UL) {
                                            break;
                                        }
                                        var_52 = (uint64_t)(uint32_t)rax_2;
                                        var_53 = local_sp_4 + (-8L);
                                        *(uint64_t *)var_53 = 4206900UL;
                                        var_54 = indirect_placeholder_5(var_8, var_52);
                                        var_55 = *(uint32_t *)6376196UL;
                                        local_sp_4 = var_53;
                                        rax_2 = var_54;
                                        local_sp_5 = var_53;
                                        rax_3 = var_54;
                                    }
                            }
                        }
                        cc_src2_3 = cc_src2_1;
                        var_61 = var_30 + (uint64_t)*(uint32_t *)6421340UL;
                        var_62 = *(uint32_t *)6421324UL;
                        var_63 = (uint32_t)var_28;
                        if (!var_31 & (long)(var_61 << 32UL) <= (long)((uint64_t)var_62 << 32UL) & (uint64_t)(var_63 + (-10)) != 0UL & (uint64_t)(var_63 + 1U) != 0UL && (uint64_t)((uint32_t)rcx_2 - var_62) == 0UL) {
                            var_64 = (uint64_t)(uint32_t)rax_1;
                            var_65 = local_sp_3 + (-8L);
                            *(uint64_t *)var_65 = 4207009UL;
                            var_66 = indirect_placeholder_5(var_8, var_64);
                            var_67 = *(uint32_t *)6376196UL;
                            local_sp_3 = var_65;
                            rax_1 = var_66;
                            local_sp_5 = var_65;
                            rax_3 = var_66;
                            while ((uint64_t)(var_67 - *(uint32_t *)6376212UL) != 0UL)
                                {
                                    var_68 = (uint64_t)var_67 + (uint64_t)*(uint32_t *)6421340UL;
                                    var_69 = *(uint32_t *)6421324UL;
                                    if ((long)(var_68 << 32UL) > (long)((uint64_t)var_69 << 32UL)) {
                                        break;
                                    }
                                    var_70 = (uint32_t)var_66;
                                    if ((uint64_t)(var_70 + 1U) == 0UL) {
                                        break;
                                    }
                                    if ((uint64_t)(var_70 + (-10)) == 0UL) {
                                        break;
                                    }
                                    if ((uint64_t)(var_69 - *(uint32_t *)6376204UL) == 0UL) {
                                        break;
                                    }
                                    var_64 = (uint64_t)(uint32_t)rax_1;
                                    var_65 = local_sp_3 + (-8L);
                                    *(uint64_t *)var_65 = 4207009UL;
                                    var_66 = indirect_placeholder_5(var_8, var_64);
                                    var_67 = *(uint32_t *)6376196UL;
                                    local_sp_3 = var_65;
                                    rax_1 = var_66;
                                    local_sp_5 = var_65;
                                    rax_3 = var_66;
                                }
                        }
                    }
                    *(uint32_t *)6376204UL = *(uint32_t *)6376208UL;
                    var_71 = *(uint64_t *)6376224UL;
                    local_sp_6 = local_sp_5;
                    rax_4 = rax_3;
                    r12_0 = var_71;
                    cc_src2_4 = cc_src2_3;
                    if (var_71 > 6376256UL) {
                        *(uint32_t *)(local_sp_5 + 12UL) = (uint32_t)rax_3;
                        var_72 = local_sp_5 + (-8L);
                        *(uint64_t *)var_72 = 4206696UL;
                        indirect_placeholder();
                        local_sp_6 = var_72;
                        rax_4 = (uint64_t)*(uint32_t *)(local_sp_5 + 4UL);
                        r12_0 = *(uint64_t *)6376224UL;
                    }
                    var_73 = (unsigned char *)(r12_0 + (-24L));
                    *var_73 = (*var_73 | '\n');
                    *(uint32_t *)6376200UL = (uint32_t)rax_4;
                    *(uint64_t *)(local_sp_6 + (-8L)) = 4206548UL;
                    indirect_placeholder();
                    var_74 = (uint64_t)*(uint32_t *)6376208UL;
                    var_75 = local_sp_6 + (-16L);
                    *(uint64_t *)var_75 = 4206564UL;
                    indirect_placeholder_8(6376256UL, var_74);
                    var_76 = *(uint64_t *)6376288UL;
                    local_sp_0 = var_75;
                    rbp_0 = var_76;
                    local_sp_1 = var_75;
                    r12_1 = r12_0;
                    if (var_76 == r12_0) {
                        var_77 = (uint64_t)*(uint32_t *)6376204UL;
                        var_78 = local_sp_0 + (-8L);
                        *(uint64_t *)var_78 = 4206590UL;
                        indirect_placeholder_8(rbp_0, var_77);
                        var_79 = *(uint64_t *)(rbp_0 + 32UL);
                        var_80 = helper_cc_compute_all_wrapper(var_79 - r12_0, r12_0, cc_src2_3, 17U);
                        local_sp_0 = var_78;
                        rbp_0 = var_79;
                        local_sp_1 = var_78;
                        do {
                            var_77 = (uint64_t)*(uint32_t *)6376204UL;
                            var_78 = local_sp_0 + (-8L);
                            *(uint64_t *)var_78 = 4206590UL;
                            indirect_placeholder_8(rbp_0, var_77);
                            var_79 = *(uint64_t *)(rbp_0 + 32UL);
                            var_80 = helper_cc_compute_all_wrapper(var_79 - r12_0, r12_0, cc_src2_3, 17U);
                            local_sp_0 = var_78;
                            rbp_0 = var_79;
                            local_sp_1 = var_78;
                        } while ((var_80 & 64UL) != 0UL);
                    }
                    local_sp_7 = local_sp_1;
                    storemerge = (uint64_t)*(uint32_t *)6376200UL;
                    continue;
                }
                break;
            }
            if (switch_state_var)
                break;
        }
    *(uint32_t *)6376200UL = 4294967295U;
    return;
}

typedef unsigned char __u_char;
typedef unsigned short __u_short;
typedef unsigned int __u_int;
typedef unsigned long __u_long;
typedef signed char __int8_t;
typedef unsigned char __uint8_t;
typedef short __int16_t;
typedef unsigned short __uint16_t;
typedef int __int32_t;
typedef unsigned int __uint32_t;
typedef long __int64_t;
typedef unsigned long __uint64_t;
typedef __int8_t __int_least8_t;
typedef __uint8_t __uint_least8_t;
typedef __int16_t __int_least16_t;
typedef __uint16_t __uint_least16_t;
typedef __int32_t __int_least32_t;
typedef __uint32_t __uint_least32_t;
typedef __int64_t __int_least64_t;
typedef __uint64_t __uint_least64_t;
typedef long __quad_t;
typedef unsigned long __u_quad_t;
typedef long __intmax_t;
typedef unsigned long __uintmax_t;
typedef unsigned long __dev_t;
typedef unsigned int __uid_t;
typedef unsigned int __gid_t;
typedef unsigned long __ino_t;
typedef unsigned long __ino64_t;
typedef unsigned int __mode_t;
typedef unsigned long __nlink_t;
typedef long __off_t;
typedef long __off64_t;
typedef int __pid_t;
typedef struct {
    int __val[2];
} __fsid_t;
typedef long __clock_t;
typedef unsigned long __rlim_t;
typedef unsigned long __rlim64_t;
typedef unsigned int __id_t;
typedef long __time_t;
typedef unsigned int __useconds_t;
typedef long __suseconds_t;
typedef int __daddr_t;
typedef int __key_t;
typedef int __clockid_t;
typedef void *__timer_t;
typedef long __blksize_t;
typedef long __blkcnt_t;
typedef long __blkcnt64_t;
typedef unsigned long __fsblkcnt_t;
typedef unsigned long __fsblkcnt64_t;
typedef unsigned long __fsfilcnt_t;
typedef unsigned long __fsfilcnt64_t;
typedef long __fsword_t;
typedef long __ssize_t;
typedef long __syscall_slong_t;
typedef unsigned long __syscall_ulong_t;
typedef __off64_t __loff_t;
typedef char *__caddr_t;
typedef long __intptr_t;
typedef unsigned int __socklen_t;
typedef int __sig_atomic_t;
typedef __int8_t int8_t;
typedef __int16_t int16_t;
typedef __int32_t int32_t;
typedef __int64_t int64_t;
typedef __uint8_t uint8_t;
typedef __uint16_t uint16_t;
typedef __uint32_t uint32_t;
typedef __uint64_t uint64_t;
typedef __int_least8_t int_least8_t;
typedef __int_least16_t int_least16_t;
typedef __int_least32_t int_least32_t;
typedef __int_least64_t int_least64_t;
typedef __uint_least8_t uint_least8_t;
typedef __uint_least16_t uint_least16_t;
typedef __uint_least32_t uint_least32_t;
typedef __uint_least64_t uint_least64_t;
typedef signed char int_fast8_t;
typedef long int_fast16_t;
typedef long int_fast32_t;
typedef long int_fast64_t;
typedef unsigned char uint_fast8_t;
typedef unsigned long uint_fast16_t;
typedef unsigned long uint_fast32_t;
typedef unsigned long uint_fast64_t;
typedef long intptr_t;
typedef unsigned long uintptr_t;
typedef __intmax_t intmax_t;
typedef __uintmax_t uintmax_t;
typedef _Bool bool;
struct helper_idivq_EAX_wrapper_ret_type;
struct type_5;
struct helper_idivq_EAX_wrapper_ret_type {
    uint64_t field_0;
    uint64_t field_1;
    uint32_t field_2;
    uint64_t field_3;
    uint64_t field_4;
    uint32_t field_5;
    uint64_t field_6;
    uint32_t field_7;
    uint64_t field_8;
    uint32_t field_9;
    uint32_t field_10;
    unsigned char field_11;
    uint32_t field_12;
};
struct type_5 {
};
extern uint64_t revng_init_local_sp(uint64_t param_0);
extern uint64_t init_rbx(void);
extern uint64_t init_rbp(void);
extern uint64_t init_cc_src2(void);
extern uint64_t helper_cc_compute_c_wrapper(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint32_t param_3);
extern uint64_t init_r13(void);
extern uint64_t init_r12(void);
extern uint64_t init_r14(void);
extern uint64_t helper_cc_compute_all_wrapper(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint32_t param_3);
extern uint64_t init_r15(void);
extern uint64_t init_state_0x9018(void);
extern uint32_t init_state_0x9010(void);
extern uint64_t init_state_0x8408(void);
extern uint64_t init_state_0x8328(void);
extern uint64_t init_state_0x82d8(void);
extern uint32_t init_state_0x9080(void);
extern uint32_t init_state_0x8248(void);
extern struct helper_idivq_EAX_wrapper_ret_type helper_idivq_EAX_wrapper(struct type_5 *param_0, uint64_t param_1, uint64_t param_2, uint64_t param_3, uint64_t param_4, uint64_t param_5, uint64_t param_6, uint64_t param_7, uint64_t param_8, uint64_t param_9, uint64_t param_10, uint64_t param_11, uint64_t param_12, uint64_t param_13, uint32_t param_14, uint64_t param_15, uint64_t param_16, uint64_t param_17, uint32_t param_18, uint32_t param_19);
void bb_fmt_paragraph(void) {
    uint64_t var_72;
    uint64_t var_73;
    uint64_t var_74;
    uint64_t var_0;
    uint64_t var_1;
    uint64_t var_2;
    uint64_t var_3;
    uint64_t var_4;
    uint64_t var_15;
    uint64_t var_5;
    uint64_t var_6;
    uint64_t var_7;
    uint64_t var_8;
    uint32_t var_9;
    uint64_t var_10;
    uint64_t var_11;
    uint64_t var_12;
    uint32_t var_13;
    uint32_t var_14;
    uint32_t var_16;
    uint64_t var_17;
    uint64_t *var_18;
    uint32_t *var_19;
    uint32_t var_20;
    uint64_t var_21;
    uint64_t var_22;
    uint32_t var_23;
    uint64_t var_24;
    uint32_t var_25;
    uint64_t var_26;
    uint32_t var_27;
    uint64_t var_28;
    uint64_t var_29;
    uint64_t rcx_8;
    uint64_t rdi_0_in;
    uint64_t r14_11;
    uint64_t cc_src2_0;
    uint32_t state_0x9010_4;
    uint64_t state_0x9018_0;
    uint64_t state_0x82d8_4;
    uint32_t state_0x9010_0;
    uint32_t state_0x9080_4;
    uint64_t state_0x82d8_0;
    uint32_t state_0x8248_4;
    uint32_t state_0x9080_0;
    uint32_t state_0x8248_0;
    uint64_t rdi_0;
    uint64_t var_30;
    uint64_t var_31;
    uint64_t *var_32;
    uint32_t *var_33;
    uint64_t r14_1;
    uint64_t rdx_0_in;
    uint64_t r14_0;
    uint64_t rax_0;
    uint32_t var_34;
    uint64_t var_35;
    uint64_t var_36;
    bool var_37;
    uint64_t var_42;
    uint64_t rcx_0;
    uint64_t r10_1;
    uint64_t var_38;
    uint64_t narrow;
    uint64_t var_39;
    uint64_t var_40;
    uint64_t var_41;
    uint64_t var_43;
    uint32_t var_44;
    uint64_t var_45;
    uint64_t var_46;
    uint64_t var_47;
    uint64_t rcx_3;
    uint64_t rdx_1;
    uint64_t rdx_2;
    uint64_t r14_2;
    uint64_t rax_1;
    uint64_t var_48;
    bool var_49;
    uint64_t var_55;
    uint32_t _pre_phi161;
    uint64_t r10_3;
    uint64_t rcx_1;
    uint32_t _pre;
    uint32_t var_50;
    uint64_t var_51;
    uint64_t narrow2;
    uint64_t var_52;
    uint64_t var_53;
    uint64_t r14_3;
    uint64_t var_54;
    uint64_t var_87;
    uint64_t var_56;
    uint64_t r14_4;
    uint64_t rax_2;
    uint64_t var_57;
    bool var_58;
    uint32_t var_68;
    uint64_t var_69;
    uint64_t var_70;
    uint32_t _pre_phi;
    uint32_t var_59;
    uint64_t var_60;
    uint64_t narrow4;
    uint64_t rcx_2;
    uint64_t var_61;
    uint64_t var_62;
    uint64_t var_63;
    uint64_t var_64;
    uint64_t r14_6;
    uint64_t var_65;
    uint64_t var_66;
    uint64_t var_67;
    uint64_t var_71;
    uint64_t var_75;
    uint64_t var_76;
    uint64_t r10_4;
    uint64_t rcx_7;
    uint64_t var_78;
    struct helper_idivq_EAX_wrapper_ret_type var_77;
    uint64_t var_79;
    uint32_t var_80;
    uint64_t var_81;
    uint32_t var_82;
    uint32_t var_83;
    uint64_t var_84;
    uint64_t rcx_5;
    uint64_t r14_8;
    uint64_t state_0x9018_1;
    uint32_t state_0x9010_1;
    uint64_t state_0x82d8_1;
    uint32_t state_0x9080_1;
    uint32_t state_0x8248_1;
    uint64_t var_85;
    uint64_t rcx_6;
    uint64_t cc_src2_1;
    uint64_t r14_9;
    uint64_t state_0x9018_2;
    uint32_t state_0x9010_2;
    uint64_t rax_3;
    uint64_t state_0x82d8_2;
    uint32_t state_0x9080_2;
    uint32_t state_0x8248_2;
    uint64_t var_89;
    uint64_t var_90;
    uint32_t var_91;
    uint64_t var_92;
    struct helper_idivq_EAX_wrapper_ret_type var_88;
    uint32_t var_93;
    uint32_t var_94;
    uint64_t var_95;
    uint64_t cc_src2_2;
    uint64_t r14_10;
    uint64_t state_0x9018_3;
    uint32_t state_0x9010_3;
    uint64_t state_0x82d8_3;
    uint32_t state_0x9080_3;
    uint32_t state_0x8248_3;
    uint64_t var_86;
    uint64_t cc_src2_3;
    uint64_t state_0x9018_4;
    uint64_t var_96;
    unsigned int loop_state_var;
    bool switch_state_var;
    var_0 = revng_init_local_sp(0UL);
    var_1 = init_r15();
    var_2 = init_r13();
    var_3 = init_rbx();
    var_4 = init_r12();
    var_5 = init_rbp();
    var_6 = init_cc_src2();
    var_7 = init_r14();
    var_8 = init_state_0x9018();
    var_9 = init_state_0x9010();
    var_10 = init_state_0x8408();
    var_11 = init_state_0x8328();
    var_12 = init_state_0x82d8();
    var_13 = init_state_0x9080();
    var_14 = init_state_0x8248();
    var_15 = *(uint64_t *)6376224UL;
    var_16 = *(uint32_t *)6421344UL;
    var_17 = (uint64_t)var_16;
    *(uint64_t *)(var_0 + (-8L)) = var_1;
    *(uint64_t *)(var_0 + (-16L)) = var_7;
    *(uint64_t *)(var_0 + (-24L)) = var_2;
    *(uint64_t *)(var_0 + (-32L)) = var_4;
    *(uint64_t *)(var_0 + (-40L)) = var_5;
    *(uint64_t *)(var_0 + (-48L)) = var_3;
    var_18 = (uint64_t *)(var_15 + 24UL);
    *var_18 = 0UL;
    var_19 = (uint32_t *)(var_15 + 8UL);
    var_20 = *var_19;
    var_21 = var_15 + (-6376296L);
    *var_19 = var_16;
    var_22 = helper_cc_compute_c_wrapper(var_21, 6376256UL, var_6, 17U);
    rdi_0_in = var_15;
    cc_src2_0 = var_6;
    state_0x9018_0 = var_8;
    state_0x9010_0 = var_9;
    state_0x82d8_0 = var_12;
    state_0x9080_0 = var_13;
    state_0x8248_0 = var_14;
    r14_0 = 9223372036854775807UL;
    r14_2 = 9223372036854775807UL;
    rax_1 = 6376256UL;
    r14_4 = 9223372036854775807UL;
    rax_2 = 6376256UL;
    r14_8 = 4900UL;
    if (var_22 == 0UL) {
        *var_19 = var_20;
        return;
    }
    var_23 = *(uint32_t *)6376208UL;
    var_24 = (uint64_t)*(uint32_t *)6376204UL;
    var_25 = *(uint32_t *)6421328UL;
    var_26 = (uint64_t)var_25;
    var_27 = *(uint32_t *)6376192UL;
    var_28 = (uint64_t)var_27;
    var_29 = var_17 << 32UL;
    while (1U)
        {
            rdi_0 = rdi_0_in + (-40L);
            rdi_0_in = rdi_0;
            rax_0 = rdi_0;
            state_0x9018_1 = state_0x9018_0;
            state_0x9010_1 = state_0x9010_0;
            state_0x82d8_1 = state_0x82d8_0;
            state_0x9080_1 = state_0x9080_0;
            state_0x8248_1 = state_0x8248_0;
            cc_src2_1 = cc_src2_0;
            state_0x9018_2 = state_0x9018_0;
            state_0x9010_2 = state_0x9010_0;
            state_0x82d8_2 = state_0x82d8_0;
            state_0x9080_2 = state_0x9080_0;
            state_0x8248_2 = state_0x8248_0;
            cc_src2_2 = cc_src2_0;
            state_0x9018_3 = state_0x9018_0;
            state_0x9010_3 = state_0x9010_0;
            state_0x82d8_3 = state_0x82d8_0;
            state_0x9080_3 = state_0x9080_0;
            state_0x8248_3 = state_0x8248_0;
            if (rdi_0 != 6376256UL) {
                var_30 = (uint64_t)*(uint32_t *)(rdi_0_in + (-32L));
                var_31 = var_24 + var_30;
                var_32 = (uint64_t *)(rdi_0_in + (-8L));
                var_33 = (uint32_t *)(rdi_0_in + (-20L));
                rdx_0_in = var_31;
                r10_1 = var_30;
                while (1U)
                    {
                        var_34 = (uint32_t)rdx_0_in;
                        var_35 = (uint64_t)var_34;
                        var_36 = rax_0 + 40UL;
                        var_37 = (var_15 == var_36);
                        rax_0 = var_36;
                        r14_1 = r14_0;
                        rcx_3 = r14_0;
                        if (var_37) {
                            var_42 = *var_18;
                            rcx_0 = var_42;
                            if ((long)r14_0 > (long)var_42) {
                                break;
                            }
                            *var_32 = var_36;
                            *var_33 = var_34;
                            r14_1 = rcx_0;
                            rcx_3 = rcx_0;
                            if (var_37) {
                                break;
                            }
                        }
                        var_38 = (uint64_t)((long)((uint64_t)(var_25 - var_34) * 42949672960UL) >> (long)32UL);
                        narrow = var_38 * var_38;
                        if (var_15 == *(uint64_t *)(rax_0 + 72UL)) {
                            var_41 = narrow + *(uint64_t *)(rax_0 + 64UL);
                            rcx_0 = var_41;
                            *var_32 = var_36;
                            *var_33 = var_34;
                            r14_1 = rcx_0;
                            rcx_3 = rcx_0;
                            if ((long)r14_0 <= (long)var_41 & var_37) {
                                break;
                            }
                        }
                        var_39 = (uint64_t)((long)((uint64_t)(var_34 - *(uint32_t *)(rax_0 + 60UL)) * 42949672960UL) >> (long)32UL);
                        var_40 = (narrow + ((var_39 * var_39) >> 1UL)) + *(uint64_t *)(rax_0 + 64UL);
                        rcx_0 = var_40;
                        if ((long)r14_0 <= (long)var_40) {
                            *var_32 = var_36;
                            *var_33 = var_34;
                            r14_1 = rcx_0;
                            rcx_3 = rcx_0;
                            if (var_37) {
                                break;
                            }
                        }
                        var_43 = var_35 + ((uint64_t)*(uint32_t *)(rax_0 + 48UL) + (uint64_t)*(uint32_t *)(rax_0 + 12UL));
                        rdx_0_in = var_43;
                        r14_0 = r14_1;
                        rcx_3 = r14_1;
                        if ((long)var_29 <= (long)(var_43 << 32UL)) {
                            continue;
                        }
                        break;
                    }
                r10_3 = r10_1;
                r10_4 = r10_1;
                rcx_7 = rcx_3;
                rcx_5 = rcx_3;
                rcx_6 = rcx_3;
                if (rdi_0 <= 6376256UL) {
                    var_85 = (uint64_t)*(unsigned char *)(rdi_0_in + (-24L));
                    r10_4 = r10_3;
                    rcx_7 = rcx_5;
                    rcx_6 = rcx_5;
                    r14_9 = r14_8;
                    state_0x9018_2 = state_0x9018_1;
                    state_0x9010_2 = state_0x9010_1;
                    rax_3 = var_85;
                    state_0x82d8_2 = state_0x82d8_1;
                    state_0x9080_2 = state_0x9080_1;
                    state_0x8248_2 = state_0x8248_1;
                    r14_10 = r14_8;
                    state_0x9018_3 = state_0x9018_1;
                    state_0x9010_3 = state_0x9010_1;
                    state_0x82d8_3 = state_0x82d8_1;
                    state_0x9080_3 = state_0x9080_1;
                    state_0x8248_3 = state_0x8248_1;
                    if ((var_85 & 1UL) != 0UL) {
                        var_86 = r14_10 + (-1600L);
                        rcx_8 = rcx_7;
                        r14_11 = var_86;
                        state_0x9010_4 = state_0x9010_3;
                        state_0x82d8_4 = state_0x82d8_3;
                        state_0x9080_4 = state_0x9080_3;
                        state_0x8248_4 = state_0x8248_3;
                        cc_src2_3 = cc_src2_2;
                        state_0x9018_4 = state_0x9018_3;
                        *(uint64_t *)(rdi_0_in + (-16L)) = (rcx_8 + r14_11);
                        var_96 = helper_cc_compute_c_wrapper(rdi_0_in + (-6376336L), 6376256UL, cc_src2_3, 17U);
                        cc_src2_0 = cc_src2_3;
                        state_0x9018_0 = state_0x9018_4;
                        state_0x9010_0 = state_0x9010_4;
                        state_0x82d8_0 = state_0x82d8_4;
                        state_0x9080_0 = state_0x9080_4;
                        state_0x8248_0 = state_0x8248_4;
                        if (var_96 != 0UL) {
                            continue;
                        }
                        break;
                    }
                    rcx_8 = rcx_6;
                    r14_11 = r14_9;
                    state_0x9010_4 = state_0x9010_2;
                    state_0x82d8_4 = state_0x82d8_2;
                    state_0x9080_4 = state_0x9080_2;
                    state_0x8248_4 = state_0x8248_2;
                    cc_src2_3 = cc_src2_1;
                    state_0x9018_4 = state_0x9018_2;
                    if ((rax_3 & 8UL) == 0UL) {
                        var_87 = (uint64_t)((long)((r10_4 << 32UL) + 8589934592UL) >> (long)32UL);
                        var_88 = helper_idivq_EAX_wrapper((struct type_5 *)(0UL), var_87, 22500UL, 4202863UL, var_24, var_87, rdi_0, var_26, 22500UL, rcx_6, var_15, 0UL, var_17, state_0x9018_2, state_0x9010_2, var_10, var_11, state_0x82d8_2, state_0x9080_2, state_0x8248_2);
                        var_89 = var_88.field_0;
                        var_90 = var_88.field_6;
                        var_91 = var_88.field_7;
                        var_92 = var_88.field_8;
                        var_93 = var_88.field_9;
                        var_94 = var_88.field_10;
                        var_95 = r14_9 + var_89;
                        r14_11 = var_95;
                        state_0x9010_4 = var_91;
                        state_0x82d8_4 = var_92;
                        state_0x9080_4 = var_93;
                        state_0x8248_4 = var_94;
                        state_0x9018_4 = var_90;
                    }
                    *(uint64_t *)(rdi_0_in + (-16L)) = (rcx_8 + r14_11);
                    var_96 = helper_cc_compute_c_wrapper(rdi_0_in + (-6376336L), 6376256UL, cc_src2_3, 17U);
                    cc_src2_0 = cc_src2_3;
                    state_0x9018_0 = state_0x9018_4;
                    state_0x9010_0 = state_0x9010_4;
                    state_0x82d8_0 = state_0x82d8_4;
                    state_0x9080_0 = state_0x9080_4;
                    state_0x8248_0 = state_0x8248_4;
                    if (var_96 != 0UL) {
                        continue;
                    }
                    break;
                }
                var_72 = (uint64_t)*(unsigned char *)(rdi_0_in + (-64L));
                r14_8 = 3300UL;
                if ((var_72 & 2UL) != 0UL) {
                    var_73 = (var_72 & 8UL) + (-1L);
                    var_74 = (uint64_t)*(unsigned char *)(rdi_0_in + (-24L));
                    var_75 = helper_cc_compute_c_wrapper(var_73, 1UL, cc_src2_0, 14U);
                    var_76 = (uint64_t)((0U - (uint32_t)var_75) & 362500U) + 2400UL;
                    cc_src2_1 = var_75;
                    r14_9 = var_76;
                    rax_3 = var_74;
                    cc_src2_2 = var_75;
                    r14_10 = var_76;
                    if ((var_74 & 1UL) != 0UL) {
                        var_86 = r14_10 + (-1600L);
                        rcx_8 = rcx_7;
                        r14_11 = var_86;
                        state_0x9010_4 = state_0x9010_3;
                        state_0x82d8_4 = state_0x82d8_3;
                        state_0x9080_4 = state_0x9080_3;
                        state_0x8248_4 = state_0x8248_3;
                        cc_src2_3 = cc_src2_2;
                        state_0x9018_4 = state_0x9018_3;
                        *(uint64_t *)(rdi_0_in + (-16L)) = (rcx_8 + r14_11);
                        var_96 = helper_cc_compute_c_wrapper(rdi_0_in + (-6376336L), 6376256UL, cc_src2_3, 17U);
                        cc_src2_0 = cc_src2_3;
                        state_0x9018_0 = state_0x9018_4;
                        state_0x9010_0 = state_0x9010_4;
                        state_0x82d8_0 = state_0x82d8_4;
                        state_0x9080_0 = state_0x9080_4;
                        state_0x8248_0 = state_0x8248_4;
                        if (var_96 != 0UL) {
                            continue;
                        }
                        break;
                    }
                }
                r14_8 = 4900UL;
                if ((var_72 & 4UL) != 0UL & rdi_0 <= 6376296UL & (*(unsigned char *)(rdi_0_in + (-104L)) & '\b') == '\x00') {
                    var_77 = helper_idivq_EAX_wrapper((struct type_5 *)(0UL), (uint64_t)((long)(((uint64_t)*(uint32_t *)(rdi_0_in + (-72L)) << 32UL) + 8589934592UL) >> (long)32UL), 40000UL, 4203051UL, var_24, r10_1, rdi_0, var_26, 22500UL, rcx_3, var_15, 0UL, var_17, state_0x9018_0, state_0x9010_0, var_10, var_11, state_0x82d8_0, state_0x9080_0, state_0x8248_0);
                    var_78 = var_77.field_0;
                    var_79 = var_77.field_6;
                    var_80 = var_77.field_7;
                    var_81 = var_77.field_8;
                    var_82 = var_77.field_9;
                    var_83 = var_77.field_10;
                    var_84 = var_78 + 4900UL;
                    r14_8 = var_84;
                    state_0x9018_1 = var_79;
                    state_0x9010_1 = var_80;
                    state_0x82d8_1 = var_81;
                    state_0x9080_1 = var_82;
                    state_0x8248_1 = var_83;
                }
                var_85 = (uint64_t)*(unsigned char *)(rdi_0_in + (-24L));
                r10_4 = r10_3;
                rcx_7 = rcx_5;
                rcx_6 = rcx_5;
                r14_9 = r14_8;
                state_0x9018_2 = state_0x9018_1;
                state_0x9010_2 = state_0x9010_1;
                rax_3 = var_85;
                state_0x82d8_2 = state_0x82d8_1;
                state_0x9080_2 = state_0x9080_1;
                state_0x8248_2 = state_0x8248_1;
                r14_10 = r14_8;
                state_0x9018_3 = state_0x9018_1;
                state_0x9010_3 = state_0x9010_1;
                state_0x82d8_3 = state_0x82d8_1;
                state_0x9080_3 = state_0x9080_1;
                state_0x8248_3 = state_0x8248_1;
                if ((var_85 & 1UL) != 0UL) {
                    var_86 = r14_10 + (-1600L);
                    rcx_8 = rcx_7;
                    r14_11 = var_86;
                    state_0x9010_4 = state_0x9010_3;
                    state_0x82d8_4 = state_0x82d8_3;
                    state_0x9080_4 = state_0x9080_3;
                    state_0x8248_4 = state_0x8248_3;
                    cc_src2_3 = cc_src2_2;
                    state_0x9018_4 = state_0x9018_3;
                    *(uint64_t *)(rdi_0_in + (-16L)) = (rcx_8 + r14_11);
                    var_96 = helper_cc_compute_c_wrapper(rdi_0_in + (-6376336L), 6376256UL, cc_src2_3, 17U);
                    cc_src2_0 = cc_src2_3;
                    state_0x9018_0 = state_0x9018_4;
                    state_0x9010_0 = state_0x9010_4;
                    state_0x82d8_0 = state_0x82d8_4;
                    state_0x9080_0 = state_0x9080_4;
                    state_0x8248_0 = state_0x8248_4;
                    if (var_96 != 0UL) {
                        continue;
                    }
                    break;
                }
            }
            var_44 = *(uint32_t *)6376264UL;
            var_45 = (uint64_t)var_44;
            var_46 = (uint64_t)(var_23 + var_44);
            var_47 = helper_cc_compute_all_wrapper(var_28, 0UL, 0UL, 24U);
            rdx_1 = var_46;
            rdx_2 = var_46;
            r10_1 = var_45;
            r10_3 = var_45;
            if ((uint64_t)(((unsigned char)(var_47 >> 4UL) ^ (unsigned char)var_47) & '\xc0') != 0UL) {
                while (1U)
                    {
                        var_57 = rax_2 + 40UL;
                        var_58 = (var_15 == var_57);
                        rax_2 = var_57;
                        r14_6 = r14_4;
                        rcx_5 = r14_4;
                        if (var_58) {
                            var_68 = (uint32_t)rdx_2;
                            var_69 = (uint64_t)((long)((uint64_t)(var_68 - var_27) * 42949672960UL) >> (long)32UL);
                            var_70 = ((var_69 * var_69) >> 1UL) + *var_18;
                            _pre_phi = var_68;
                            rcx_2 = var_70;
                            if ((long)r14_4 > (long)var_70) {
                                loop_state_var = 0U;
                                break;
                            }
                            *(uint64_t *)6376288UL = var_57;
                            *(uint32_t *)6376276UL = _pre_phi;
                            r14_6 = rcx_2;
                            rcx_5 = rcx_2;
                            if (!var_58) {
                                loop_state_var = 0U;
                                break;
                            }
                        }
                        var_59 = (uint32_t)rdx_2;
                        var_60 = (uint64_t)((long)((uint64_t)(var_25 - var_59) * 42949672960UL) >> (long)32UL);
                        narrow4 = var_60 * var_60;
                        _pre_phi = var_59;
                        if (var_15 == *(uint64_t *)(rax_2 + 72UL)) {
                            var_65 = narrow4 + *(uint64_t *)(rax_2 + 64UL);
                            var_66 = (uint64_t)((long)((uint64_t)(var_59 - var_27) * 42949672960UL) >> (long)32UL);
                            var_67 = var_65 + ((var_66 * var_66) >> 1UL);
                            rcx_2 = var_67;
                            *(uint64_t *)6376288UL = var_57;
                            *(uint32_t *)6376276UL = _pre_phi;
                            r14_6 = rcx_2;
                            rcx_5 = rcx_2;
                            if ((long)r14_4 <= (long)var_67 & !var_58) {
                                loop_state_var = 0U;
                                break;
                            }
                        }
                        var_61 = (uint64_t)((long)((uint64_t)(var_59 - *(uint32_t *)(rax_2 + 60UL)) * 42949672960UL) >> (long)32UL);
                        var_62 = (narrow4 + ((var_61 * var_61) >> 1UL)) + *(uint64_t *)(rax_2 + 64UL);
                        var_63 = (uint64_t)((long)((uint64_t)(var_59 - var_27) * 42949672960UL) >> (long)32UL);
                        var_64 = ((var_63 * var_63) >> 1UL) + var_62;
                        rcx_2 = var_64;
                        if ((long)r14_4 <= (long)var_64) {
                            *(uint64_t *)6376288UL = var_57;
                            *(uint32_t *)6376276UL = _pre_phi;
                            r14_6 = rcx_2;
                            rcx_5 = rcx_2;
                            if (var_58) {
                                loop_state_var = 0U;
                                break;
                            }
                        }
                        var_71 = rdx_2 + ((uint64_t)*(uint32_t *)(rax_2 + 48UL) + (uint64_t)*(uint32_t *)(rax_2 + 12UL));
                        r14_4 = r14_6;
                        rcx_3 = r14_6;
                        if ((long)var_29 <= (long)(var_71 << 32UL)) {
                            loop_state_var = 1U;
                            break;
                        }
                        rdx_2 = (uint64_t)(uint32_t)var_71;
                        continue;
                    }
                var_85 = (uint64_t)*(unsigned char *)(rdi_0_in + (-24L));
                r10_4 = r10_3;
                rcx_7 = rcx_5;
                rcx_6 = rcx_5;
                r14_9 = r14_8;
                state_0x9018_2 = state_0x9018_1;
                state_0x9010_2 = state_0x9010_1;
                rax_3 = var_85;
                state_0x82d8_2 = state_0x82d8_1;
                state_0x9080_2 = state_0x9080_1;
                state_0x8248_2 = state_0x8248_1;
                r14_10 = r14_8;
                state_0x9018_3 = state_0x9018_1;
                state_0x9010_3 = state_0x9010_1;
                state_0x82d8_3 = state_0x82d8_1;
                state_0x9080_3 = state_0x9080_1;
                state_0x8248_3 = state_0x8248_1;
                if ((var_85 & 1UL) != 0UL) {
                    var_86 = r14_10 + (-1600L);
                    rcx_8 = rcx_7;
                    r14_11 = var_86;
                    state_0x9010_4 = state_0x9010_3;
                    state_0x82d8_4 = state_0x82d8_3;
                    state_0x9080_4 = state_0x9080_3;
                    state_0x8248_4 = state_0x8248_3;
                    cc_src2_3 = cc_src2_2;
                    state_0x9018_4 = state_0x9018_3;
                    *(uint64_t *)(rdi_0_in + (-16L)) = (rcx_8 + r14_11);
                    var_96 = helper_cc_compute_c_wrapper(rdi_0_in + (-6376336L), 6376256UL, cc_src2_3, 17U);
                    cc_src2_0 = cc_src2_3;
                    state_0x9018_0 = state_0x9018_4;
                    state_0x9010_0 = state_0x9010_4;
                    state_0x82d8_0 = state_0x82d8_4;
                    state_0x9080_0 = state_0x9080_4;
                    state_0x8248_0 = state_0x8248_4;
                    if (var_96 != 0UL) {
                        continue;
                    }
                    switch_state_var = 1;
                    break;
                }
                rcx_8 = rcx_6;
                r14_11 = r14_9;
                state_0x9010_4 = state_0x9010_2;
                state_0x82d8_4 = state_0x82d8_2;
                state_0x9080_4 = state_0x9080_2;
                state_0x8248_4 = state_0x8248_2;
                cc_src2_3 = cc_src2_1;
                state_0x9018_4 = state_0x9018_2;
                if ((rax_3 & 8UL) == 0UL) {
                    var_87 = (uint64_t)((long)((r10_4 << 32UL) + 8589934592UL) >> (long)32UL);
                    var_88 = helper_idivq_EAX_wrapper((struct type_5 *)(0UL), var_87, 22500UL, 4202863UL, var_24, var_87, rdi_0, var_26, 22500UL, rcx_6, var_15, 0UL, var_17, state_0x9018_2, state_0x9010_2, var_10, var_11, state_0x82d8_2, state_0x9080_2, state_0x8248_2);
                    var_89 = var_88.field_0;
                    var_90 = var_88.field_6;
                    var_91 = var_88.field_7;
                    var_92 = var_88.field_8;
                    var_93 = var_88.field_9;
                    var_94 = var_88.field_10;
                    var_95 = r14_9 + var_89;
                    r14_11 = var_95;
                    state_0x9010_4 = var_91;
                    state_0x82d8_4 = var_92;
                    state_0x9080_4 = var_93;
                    state_0x8248_4 = var_94;
                    state_0x9018_4 = var_90;
                }
                *(uint64_t *)(rdi_0_in + (-16L)) = (rcx_8 + r14_11);
                var_96 = helper_cc_compute_c_wrapper(rdi_0_in + (-6376336L), 6376256UL, cc_src2_3, 17U);
                cc_src2_0 = cc_src2_3;
                state_0x9018_0 = state_0x9018_4;
                state_0x9010_0 = state_0x9010_4;
                state_0x82d8_0 = state_0x82d8_4;
                state_0x9080_0 = state_0x9080_4;
                state_0x8248_0 = state_0x8248_4;
                if (var_96 != 0UL) {
                    continue;
                }
                switch_state_var = 1;
                break;
            }
            while (1U)
                {
                    var_48 = rax_1 + 40UL;
                    var_49 = (var_15 == var_48);
                    rax_1 = var_48;
                    r14_3 = r14_2;
                    rcx_5 = r14_2;
                    if (var_49) {
                        var_55 = *var_18;
                        rcx_1 = var_55;
                        if ((long)r14_2 <= (long)var_55) {
                            loop_state_var = 0U;
                            break;
                        }
                        _pre = (uint32_t)rdx_1;
                        _pre_phi161 = _pre;
                        *(uint64_t *)6376288UL = var_48;
                        *(uint32_t *)6376276UL = _pre_phi161;
                        r14_3 = rcx_1;
                        rcx_5 = rcx_1;
                        if (!var_49) {
                            loop_state_var = 0U;
                            break;
                        }
                    }
                    var_50 = (uint32_t)rdx_1;
                    var_51 = (uint64_t)((long)((uint64_t)(var_25 - var_50) * 42949672960UL) >> (long)32UL);
                    narrow2 = var_51 * var_51;
                    _pre_phi161 = var_50;
                    if (var_15 == *(uint64_t *)(rax_1 + 72UL)) {
                        var_54 = narrow2 + *(uint64_t *)(rax_1 + 64UL);
                        rcx_1 = var_54;
                        *(uint64_t *)6376288UL = var_48;
                        *(uint32_t *)6376276UL = _pre_phi161;
                        r14_3 = rcx_1;
                        rcx_5 = rcx_1;
                        if ((long)r14_2 <= (long)var_54 & !var_49) {
                            loop_state_var = 0U;
                            break;
                        }
                    }
                    var_52 = (uint64_t)((long)((uint64_t)(var_50 - *(uint32_t *)(rax_1 + 60UL)) * 42949672960UL) >> (long)32UL);
                    var_53 = (narrow2 + ((var_52 * var_52) >> 1UL)) + *(uint64_t *)(rax_1 + 64UL);
                    rcx_1 = var_53;
                    if ((long)r14_2 <= (long)var_53) {
                        *(uint64_t *)6376288UL = var_48;
                        *(uint32_t *)6376276UL = _pre_phi161;
                        r14_3 = rcx_1;
                        rcx_5 = rcx_1;
                        if (var_49) {
                            loop_state_var = 0U;
                            break;
                        }
                    }
                    var_56 = rdx_1 + ((uint64_t)*(uint32_t *)(rax_1 + 48UL) + (uint64_t)*(uint32_t *)(rax_1 + 12UL));
                    r14_2 = r14_3;
                    rcx_3 = r14_3;
                    if ((long)var_29 <= (long)(var_56 << 32UL)) {
                        loop_state_var = 1U;
                        break;
                    }
                    rdx_1 = (uint64_t)(uint32_t)var_56;
                    continue;
                }
            switch_state_var = 0;
            switch (loop_state_var) {
              case 1U:
                {
                    break;
                }
                break;
              case 0U:
                {
                    var_85 = (uint64_t)*(unsigned char *)(rdi_0_in + (-24L));
                    r10_4 = r10_3;
                    rcx_7 = rcx_5;
                    rcx_6 = rcx_5;
                    r14_9 = r14_8;
                    state_0x9018_2 = state_0x9018_1;
                    state_0x9010_2 = state_0x9010_1;
                    rax_3 = var_85;
                    state_0x82d8_2 = state_0x82d8_1;
                    state_0x9080_2 = state_0x9080_1;
                    state_0x8248_2 = state_0x8248_1;
                    r14_10 = r14_8;
                    state_0x9018_3 = state_0x9018_1;
                    state_0x9010_3 = state_0x9010_1;
                    state_0x82d8_3 = state_0x82d8_1;
                    state_0x9080_3 = state_0x9080_1;
                    state_0x8248_3 = state_0x8248_1;
                    if ((var_85 & 1UL) != 0UL) {
                        var_86 = r14_10 + (-1600L);
                        rcx_8 = rcx_7;
                        r14_11 = var_86;
                        state_0x9010_4 = state_0x9010_3;
                        state_0x82d8_4 = state_0x82d8_3;
                        state_0x9080_4 = state_0x9080_3;
                        state_0x8248_4 = state_0x8248_3;
                        cc_src2_3 = cc_src2_2;
                        state_0x9018_4 = state_0x9018_3;
                        *(uint64_t *)(rdi_0_in + (-16L)) = (rcx_8 + r14_11);
                        var_96 = helper_cc_compute_c_wrapper(rdi_0_in + (-6376336L), 6376256UL, cc_src2_3, 17U);
                        cc_src2_0 = cc_src2_3;
                        state_0x9018_0 = state_0x9018_4;
                        state_0x9010_0 = state_0x9010_4;
                        state_0x82d8_0 = state_0x82d8_4;
                        state_0x9080_0 = state_0x9080_4;
                        state_0x8248_0 = state_0x8248_4;
                        if (var_96 != 0UL) {
                            continue;
                        }
                        switch_state_var = 1;
                        break;
                    }
                    rcx_8 = rcx_6;
                    r14_11 = r14_9;
                    state_0x9010_4 = state_0x9010_2;
                    state_0x82d8_4 = state_0x82d8_2;
                    state_0x9080_4 = state_0x9080_2;
                    state_0x8248_4 = state_0x8248_2;
                    cc_src2_3 = cc_src2_1;
                    state_0x9018_4 = state_0x9018_2;
                    if ((rax_3 & 8UL) == 0UL) {
                        var_87 = (uint64_t)((long)((r10_4 << 32UL) + 8589934592UL) >> (long)32UL);
                        var_88 = helper_idivq_EAX_wrapper((struct type_5 *)(0UL), var_87, 22500UL, 4202863UL, var_24, var_87, rdi_0, var_26, 22500UL, rcx_6, var_15, 0UL, var_17, state_0x9018_2, state_0x9010_2, var_10, var_11, state_0x82d8_2, state_0x9080_2, state_0x8248_2);
                        var_89 = var_88.field_0;
                        var_90 = var_88.field_6;
                        var_91 = var_88.field_7;
                        var_92 = var_88.field_8;
                        var_93 = var_88.field_9;
                        var_94 = var_88.field_10;
                        var_95 = r14_9 + var_89;
                        r14_11 = var_95;
                        state_0x9010_4 = var_91;
                        state_0x82d8_4 = var_92;
                        state_0x9080_4 = var_93;
                        state_0x8248_4 = var_94;
                        state_0x9018_4 = var_90;
                    }
                    *(uint64_t *)(rdi_0_in + (-16L)) = (rcx_8 + r14_11);
                    var_96 = helper_cc_compute_c_wrapper(rdi_0_in + (-6376336L), 6376256UL, cc_src2_3, 17U);
                    cc_src2_0 = cc_src2_3;
                    state_0x9018_0 = state_0x9018_4;
                    state_0x9010_0 = state_0x9010_4;
                    state_0x82d8_0 = state_0x82d8_4;
                    state_0x9080_0 = state_0x9080_4;
                    state_0x8248_0 = state_0x8248_4;
                    if (var_96 != 0UL) {
                        continue;
                    }
                    switch_state_var = 1;
                    break;
                }
                break;
            }
            if (switch_state_var)
                break;
        }
    *var_19 = var_20;
    return;
}

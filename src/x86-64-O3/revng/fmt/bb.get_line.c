typedef unsigned char __u_char;
typedef unsigned short __u_short;
typedef unsigned int __u_int;
typedef unsigned long __u_long;
typedef signed char __int8_t;
typedef unsigned char __uint8_t;
typedef short __int16_t;
typedef unsigned short __uint16_t;
typedef int __int32_t;
typedef unsigned int __uint32_t;
typedef long __int64_t;
typedef unsigned long __uint64_t;
typedef __int8_t __int_least8_t;
typedef __uint8_t __uint_least8_t;
typedef __int16_t __int_least16_t;
typedef __uint16_t __uint_least16_t;
typedef __int32_t __int_least32_t;
typedef __uint32_t __uint_least32_t;
typedef __int64_t __int_least64_t;
typedef __uint64_t __uint_least64_t;
typedef long __quad_t;
typedef unsigned long __u_quad_t;
typedef long __intmax_t;
typedef unsigned long __uintmax_t;
typedef unsigned long __dev_t;
typedef unsigned int __uid_t;
typedef unsigned int __gid_t;
typedef unsigned long __ino_t;
typedef unsigned long __ino64_t;
typedef unsigned int __mode_t;
typedef unsigned long __nlink_t;
typedef long __off_t;
typedef long __off64_t;
typedef int __pid_t;
typedef struct {
    int __val[2];
} __fsid_t;
typedef long __clock_t;
typedef unsigned long __rlim_t;
typedef unsigned long __rlim64_t;
typedef unsigned int __id_t;
typedef long __time_t;
typedef unsigned int __useconds_t;
typedef long __suseconds_t;
typedef int __daddr_t;
typedef int __key_t;
typedef int __clockid_t;
typedef void *__timer_t;
typedef long __blksize_t;
typedef long __blkcnt_t;
typedef long __blkcnt64_t;
typedef unsigned long __fsblkcnt_t;
typedef unsigned long __fsblkcnt64_t;
typedef unsigned long __fsfilcnt_t;
typedef unsigned long __fsfilcnt64_t;
typedef long __fsword_t;
typedef long __ssize_t;
typedef long __syscall_slong_t;
typedef unsigned long __syscall_ulong_t;
typedef __off64_t __loff_t;
typedef char *__caddr_t;
typedef long __intptr_t;
typedef unsigned int __socklen_t;
typedef int __sig_atomic_t;
typedef __int8_t int8_t;
typedef __int16_t int16_t;
typedef __int32_t int32_t;
typedef __int64_t int64_t;
typedef __uint8_t uint8_t;
typedef __uint16_t uint16_t;
typedef __uint32_t uint32_t;
typedef __uint64_t uint64_t;
typedef __int_least8_t int_least8_t;
typedef __int_least16_t int_least16_t;
typedef __int_least32_t int_least32_t;
typedef __int_least64_t int_least64_t;
typedef __uint_least8_t uint_least8_t;
typedef __uint_least16_t uint_least16_t;
typedef __uint_least32_t uint_least32_t;
typedef __uint_least64_t uint_least64_t;
typedef signed char int_fast8_t;
typedef long int_fast16_t;
typedef long int_fast32_t;
typedef long int_fast64_t;
typedef unsigned char uint_fast8_t;
typedef unsigned long uint_fast16_t;
typedef unsigned long uint_fast32_t;
typedef unsigned long uint_fast64_t;
typedef long intptr_t;
typedef unsigned long uintptr_t;
typedef __intmax_t intmax_t;
typedef __uintmax_t uintmax_t;
typedef _Bool bool;
extern uint64_t revng_init_local_sp(uint64_t param_0);
extern uint64_t init_rbx(void);
extern uint64_t init_rbp(void);
extern void indirect_placeholder(void);
extern uint64_t init_cc_src2(void);
extern uint64_t helper_cc_compute_c_wrapper(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint32_t param_3);
extern uint64_t init_r13(void);
extern uint64_t init_r12(void);
extern uint64_t init_r14(void);
extern uint64_t init_r15(void);
uint64_t bb_get_line(uint64_t rdi, uint64_t rsi) {
    uint64_t var_0;
    uint64_t var_1;
    uint64_t var_2;
    uint64_t var_3;
    uint64_t var_4;
    uint64_t var_5;
    uint64_t var_6;
    uint64_t var_7;
    uint64_t local_sp_4;
    uint32_t storemerge_pre_phi;
    uint64_t local_sp_0;
    uint64_t rax_0;
    uint64_t rbx_0;
    uint64_t rdi1_0;
    uint32_t var_42;
    uint32_t var_64;
    bool var_61;
    uint64_t var_62;
    uint32_t var_63;
    uint64_t rax_1;
    uint64_t var_65;
    uint64_t var_43;
    uint32_t var_44;
    uint32_t var_45;
    uint64_t var_46;
    uint32_t *var_47;
    bool var_48;
    bool var_49;
    uint64_t var_50;
    unsigned char *var_51;
    unsigned char var_52;
    uint64_t rdi1_1;
    uint64_t rdi1_2;
    unsigned char rcx_0;
    uint64_t cc_src2_2;
    unsigned char *_pre_phi87;
    uint64_t cc_src2_0;
    unsigned char *var_53;
    uint64_t var_54;
    uint64_t rax_4;
    uint64_t local_sp_1;
    uint64_t cc_src2_3;
    uint64_t rax_2;
    uint64_t cc_src2_1;
    uint64_t rdx_0;
    uint64_t var_60;
    uint32_t _pre_phi88;
    uint64_t var_55;
    uint64_t var_56;
    uint32_t _pre;
    uint64_t rax_3;
    uint32_t var_57;
    uint64_t var_58;
    uint64_t var_59;
    uint64_t var_39;
    uint64_t local_sp_3;
    uint64_t rax_7;
    uint64_t local_sp_2;
    uint64_t var_40;
    uint32_t var_41;
    bool var_37;
    uint64_t r12_1;
    uint64_t var_38;
    uint64_t local_sp_5;
    uint64_t var_19;
    uint64_t var_20;
    uint64_t var_21;
    uint64_t var_22;
    uint32_t var_23;
    uint32_t var_24;
    uint64_t var_25;
    uint64_t var_26;
    uint64_t var_27;
    unsigned char *var_28;
    unsigned char var_29;
    uint64_t var_30;
    uint32_t var_31;
    uint64_t var_32;
    uint64_t var_33;
    uint64_t var_34;
    uint64_t var_35;
    uint64_t var_36;
    uint64_t rbx_1;
    uint64_t cc_src2_4;
    uint64_t rdx_1;
    uint64_t var_8;
    uint64_t rax_5;
    uint64_t rbx_2;
    uint64_t cc_src2_5;
    uint64_t local_sp_6;
    uint64_t cc_src2_7;
    uint32_t var_9;
    uint64_t var_10;
    uint64_t rax_6;
    uint32_t var_11;
    uint64_t var_12;
    uint64_t var_13;
    uint64_t cc_src2_6;
    uint64_t var_14;
    uint64_t cc_src2_8;
    uint64_t var_15;
    uint64_t var_16;
    uint32_t var_17;
    uint64_t var_18;
    var_0 = revng_init_local_sp(0UL);
    var_1 = init_r15();
    var_2 = init_r13();
    var_3 = init_rbx();
    var_4 = init_r12();
    var_5 = init_rbp();
    var_6 = init_cc_src2();
    var_7 = init_r14();
    *(uint64_t *)(var_0 + (-8L)) = var_1;
    *(uint64_t *)(var_0 + (-16L)) = var_7;
    *(uint64_t *)(var_0 + (-24L)) = var_2;
    *(uint64_t *)(var_0 + (-32L)) = var_4;
    *(uint64_t *)(var_0 + (-40L)) = var_5;
    *(uint64_t *)(var_0 + (-48L)) = var_3;
    local_sp_4 = var_0 + (-56L);
    rdi1_0 = 4265227UL;
    rcx_0 = (unsigned char)'\x00';
    rbx_1 = (uint64_t)(uint32_t)rsi;
    cc_src2_4 = var_6;
    rdx_1 = *(uint64_t *)6376224UL;
    var_60 = rdx_0 + 40UL;
    *(uint64_t *)6376224UL = var_60;
    local_sp_4 = local_sp_1;
    cc_src2_4 = cc_src2_1;
    rdx_1 = var_60;
    do {
        var_8 = *(uint64_t *)6416256UL;
        *(uint64_t *)rdx_1 = var_8;
        local_sp_5 = local_sp_4;
        rax_5 = var_8;
        rbx_2 = rbx_1;
        cc_src2_5 = cc_src2_4;
        while (1U)
            {
                rax_7 = rax_5;
                local_sp_6 = local_sp_5;
                cc_src2_7 = cc_src2_5;
                cc_src2_6 = cc_src2_5;
                cc_src2_8 = cc_src2_5;
                if (rax_5 != 6421320UL) {
                    if (*(unsigned char *)6421361UL == '\x00') {
                        *(uint32_t *)6376204UL = *(uint32_t *)6376208UL;
                    } else {
                        if (*(unsigned char *)6421363UL == '\x00') {
                            *(uint32_t *)6376204UL = *(uint32_t *)6421324UL;
                        } else {
                            if (*(unsigned char *)6421362UL == '\x00') {
                                *(uint32_t *)6376204UL = *(uint32_t *)6376208UL;
                            } else {
                                var_9 = *(uint32_t *)6421324UL;
                                var_10 = (uint64_t)var_9;
                                rax_6 = var_10;
                                if ((uint64_t)(var_9 - *(uint32_t *)6376208UL) == 0UL) {
                                    *(uint32_t *)6376204UL = (uint32_t)rax_6;
                                    cc_src2_7 = cc_src2_6;
                                } else {
                                    var_11 = *(uint32_t *)6376204UL;
                                    if ((uint64_t)(var_11 - var_9) != 0UL) {
                                        var_12 = helper_cc_compute_c_wrapper((uint64_t)var_11 + (-1L), 1UL, cc_src2_5, 16U);
                                        var_13 = (0UL - var_12) & 3UL;
                                        rax_6 = var_13;
                                        cc_src2_6 = var_12;
                                        *(uint32_t *)6376204UL = (uint32_t)rax_6;
                                        cc_src2_7 = cc_src2_6;
                                    }
                                }
                            }
                        }
                    }
                    var_14 = local_sp_5 + (-8L);
                    *(uint64_t *)var_14 = 4204995UL;
                    indirect_placeholder();
                    local_sp_6 = var_14;
                    rax_7 = *(uint64_t *)6416256UL;
                    cc_src2_8 = cc_src2_7;
                }
                var_15 = rax_7 + 1UL;
                *(unsigned char *)rax_7 = (unsigned char)rbx_2;
                *(uint64_t *)6416256UL = var_15;
                var_16 = local_sp_6 + (-8L);
                *(uint64_t *)var_16 = 4204917UL;
                indirect_placeholder();
                var_17 = (uint32_t)rax_7;
                var_18 = (uint64_t)var_17;
                rbx_0 = var_18;
                cc_src2_0 = cc_src2_8;
                local_sp_5 = var_16;
                rbx_2 = var_18;
                cc_src2_5 = cc_src2_8;
                if ((uint64_t)(uint32_t)var_15 == 0UL) {
                    break;
                }
                rax_5 = *(uint64_t *)6416256UL;
                continue;
            }
        var_19 = *(uint64_t *)6376224UL;
        var_20 = *(uint64_t *)6416256UL;
        var_21 = *(uint64_t *)var_19;
        var_22 = var_20 - var_21;
        var_23 = *(uint32_t *)6421324UL;
        var_24 = (uint32_t)var_22;
        *(uint32_t *)6421324UL = (var_23 + var_24);
        *(uint32_t *)(var_19 + 8UL) = var_24;
        var_25 = var_22 << 32UL;
        var_26 = (uint64_t)((long)var_25 >> (long)32UL);
        var_27 = (var_26 + var_21) + (-1L);
        *(uint64_t *)(local_sp_6 + (-16L)) = 4205065UL;
        indirect_placeholder();
        var_28 = (unsigned char *)(var_19 + 16UL);
        var_29 = *var_28;
        var_30 = (var_25 != 0UL);
        var_31 = (uint32_t)var_26;
        var_32 = (uint64_t)(((var_31 & (-256)) | (uint32_t)var_30) | (uint32_t)(uint64_t)(var_29 & '\xfe'));
        *var_28 = (unsigned char)var_32;
        var_33 = local_sp_6 + (-24L);
        *(uint64_t *)var_33 = 4205094UL;
        indirect_placeholder();
        var_34 = (((uint64_t)(var_31 & 1073741568U) | (var_32 != 0UL)) << 2UL) | (uint64_t)(*var_28 & '\xfb');
        var_35 = var_21 - var_27;
        *var_28 = (unsigned char)var_34;
        var_36 = helper_cc_compute_c_wrapper(var_35, var_27, cc_src2_8, 17U);
        local_sp_2 = var_33;
        local_sp_3 = var_33;
        r12_1 = var_27;
        if (var_36 != 0UL) {
            var_37 = (var_34 == 0UL);
            var_38 = local_sp_3 + (-8L);
            *(uint64_t *)var_38 = 4205152UL;
            indirect_placeholder();
            local_sp_2 = var_38;
            local_sp_3 = var_38;
            while (!var_37)
                {
                    var_39 = r12_1 + (-1L);
                    r12_1 = var_39;
                    if (var_39 == var_21) {
                        break;
                    }
                    var_38 = local_sp_3 + (-8L);
                    *(uint64_t *)var_38 = 4205152UL;
                    indirect_placeholder();
                    local_sp_2 = var_38;
                    local_sp_3 = var_38;
                }
        }
        var_40 = local_sp_2 + (-8L);
        *(uint64_t *)var_40 = 4205172UL;
        indirect_placeholder();
        *var_28 = (((var_34 == 0UL) ? '\x00' : '\x02') | (*var_28 & '\xfd'));
        var_41 = *(uint32_t *)6421324UL;
        local_sp_0 = var_40;
        rax_0 = (uint64_t)var_41;
        while (1U)
            {
                var_42 = (uint32_t)rbx_0;
                rdi1_0 = rdi;
                rax_4 = rax_0;
                local_sp_1 = local_sp_0;
                rax_2 = rax_0;
                rax_3 = rax_0;
                rbx_1 = rbx_0;
                if ((uint64_t)(var_42 + (-32)) == 0UL) {
                    var_64 = (uint32_t)(rax_0 + 1UL);
                    storemerge_pre_phi = var_64;
                    rax_1 = (uint64_t)var_64;
                } else {
                    if ((uint64_t)(var_42 + (-9)) != 0UL) {
                        break;
                    }
                    *(unsigned char *)6376216UL = (unsigned char)'\x01';
                    var_61 = ((int)(uint32_t)rax_0 < (int)0U);
                    var_62 = rax_0 << 32UL;
                    var_63 = (uint32_t)(((uint64_t)((long)(var_61 ? (var_62 + 30064771072UL) : var_62) >> (long)32UL) & 34359738360UL) + 8UL);
                    storemerge_pre_phi = var_63;
                    rax_1 = (uint64_t)(var_63 & (-8));
                }
                *(uint32_t *)6421324UL = storemerge_pre_phi;
                var_65 = local_sp_0 + (-8L);
                *(uint64_t *)var_65 = 4205252UL;
                indirect_placeholder();
                local_sp_0 = var_65;
                rax_0 = (uint64_t)*(uint32_t *)6421324UL;
                rbx_0 = rax_1;
                continue;
            }
        var_43 = *(uint64_t *)6376224UL;
        var_44 = (uint32_t)rax_0;
        var_45 = var_44 - var_41;
        var_46 = (uint64_t)(var_42 + 1U);
        var_47 = (uint32_t *)(var_43 + 12UL);
        *var_47 = var_45;
        var_48 = (var_46 == 0UL);
        rdx_0 = var_43;
        _pre_phi88 = var_44;
        if (var_48) {
            var_53 = (unsigned char *)(var_43 + 16UL);
            *var_53 = (*var_53 | '\b');
            _pre_phi87 = var_53;
            var_54 = helper_cc_compute_c_wrapper((uint64_t)(*_pre_phi87 & '\b') + (-1L), 1UL, cc_src2_8, 14U);
            *var_47 = (2U - (uint32_t)var_54);
            cc_src2_0 = var_54;
        } else {
            var_49 = ((uint64_t)(var_42 + (-10)) == 0UL);
            var_50 = (rdi1_0 & (-256L)) | var_49;
            var_51 = (unsigned char *)(var_43 + 16UL);
            var_52 = *var_51;
            rdi1_1 = var_50;
            rdi1_2 = var_50;
            _pre_phi87 = var_51;
            if ((var_52 & '\x02') == '\x00') {
                rdi1_2 = rdi1_1;
            } else {
                rdi1_1 = 0UL;
                rcx_0 = (unsigned char)'\b';
                if (((int)var_45 > (int)1U) || var_49) {
                    rdi1_2 = rdi1_1;
                }
            }
            *var_51 = (rcx_0 | (var_52 & '\xf7'));
            if ((uint64_t)(unsigned char)rdi1_2 == 0UL) {
                var_54 = helper_cc_compute_c_wrapper((uint64_t)(*_pre_phi87 & '\b') + (-1L), 1UL, cc_src2_8, 14U);
                *var_47 = (2U - (uint32_t)var_54);
                cc_src2_0 = var_54;
            } else {
                if (*(unsigned char *)6421360UL == '\x00') {
                    var_54 = helper_cc_compute_c_wrapper((uint64_t)(*_pre_phi87 & '\b') + (-1L), 1UL, cc_src2_8, 14U);
                    *var_47 = (2U - (uint32_t)var_54);
                    cc_src2_0 = var_54;
                }
            }
        }
        cc_src2_1 = cc_src2_0;
        cc_src2_2 = cc_src2_0;
        cc_src2_3 = cc_src2_0;
        if (var_43 != 6416176UL) {
            if (*(unsigned char *)6421361UL == '\x00') {
                var_57 = *(uint32_t *)6376208UL;
                var_58 = (uint64_t)var_57;
                *(uint32_t *)6376204UL = var_57;
                rax_4 = var_58;
            } else {
                if (*(unsigned char *)6421363UL == '\x00') {
                    *(uint32_t *)6376204UL = _pre_phi88;
                    rax_4 = rax_3;
                    cc_src2_3 = cc_src2_2;
                } else {
                    if (*(unsigned char *)6421362UL == '\x00') {
                        var_57 = *(uint32_t *)6376208UL;
                        var_58 = (uint64_t)var_57;
                        *(uint32_t *)6376204UL = var_57;
                        rax_4 = var_58;
                    } else {
                        if ((uint64_t)(*(uint32_t *)6376208UL - var_44) == 0UL) {
                            *(uint32_t *)6376204UL = _pre_phi88;
                            rax_4 = rax_3;
                            cc_src2_3 = cc_src2_2;
                        } else {
                            if ((uint64_t)(*(uint32_t *)6376204UL - var_44) != 0UL) {
                                var_55 = helper_cc_compute_c_wrapper(rax_0 + (-1L), 1UL, cc_src2_0, 16U);
                                var_56 = (0UL - var_55) & 3UL;
                                _pre = (uint32_t)var_56;
                                _pre_phi88 = _pre;
                                rax_3 = var_56;
                                cc_src2_2 = var_55;
                                *(uint32_t *)6376204UL = _pre_phi88;
                                rax_4 = rax_3;
                                cc_src2_3 = cc_src2_2;
                            }
                        }
                    }
                }
            }
            var_59 = local_sp_0 + (-8L);
            *(uint64_t *)var_59 = 4205638UL;
            indirect_placeholder();
            local_sp_1 = var_59;
            rax_2 = rax_4;
            cc_src2_1 = cc_src2_3;
            rdx_0 = *(uint64_t *)6376224UL;
        }
        var_60 = rdx_0 + 40UL;
        *(uint64_t *)6376224UL = var_60;
        local_sp_4 = local_sp_1;
        cc_src2_4 = cc_src2_1;
        rdx_1 = var_60;
    } while (!var_48);
    indirect_placeholder();
    return rax_2;
}

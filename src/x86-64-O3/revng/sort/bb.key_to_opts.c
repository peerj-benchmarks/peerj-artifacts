typedef unsigned char __u_char;
typedef unsigned short __u_short;
typedef unsigned int __u_int;
typedef unsigned long __u_long;
typedef signed char __int8_t;
typedef unsigned char __uint8_t;
typedef short __int16_t;
typedef unsigned short __uint16_t;
typedef int __int32_t;
typedef unsigned int __uint32_t;
typedef long __int64_t;
typedef unsigned long __uint64_t;
typedef __int8_t __int_least8_t;
typedef __uint8_t __uint_least8_t;
typedef __int16_t __int_least16_t;
typedef __uint16_t __uint_least16_t;
typedef __int32_t __int_least32_t;
typedef __uint32_t __uint_least32_t;
typedef __int64_t __int_least64_t;
typedef __uint64_t __uint_least64_t;
typedef long __quad_t;
typedef unsigned long __u_quad_t;
typedef long __intmax_t;
typedef unsigned long __uintmax_t;
typedef unsigned long __dev_t;
typedef unsigned int __uid_t;
typedef unsigned int __gid_t;
typedef unsigned long __ino_t;
typedef unsigned long __ino64_t;
typedef unsigned int __mode_t;
typedef unsigned long __nlink_t;
typedef long __off_t;
typedef long __off64_t;
typedef int __pid_t;
typedef struct {
    int __val[2];
} __fsid_t;
typedef long __clock_t;
typedef unsigned long __rlim_t;
typedef unsigned long __rlim64_t;
typedef unsigned int __id_t;
typedef long __time_t;
typedef unsigned int __useconds_t;
typedef long __suseconds_t;
typedef int __daddr_t;
typedef int __key_t;
typedef int __clockid_t;
typedef void *__timer_t;
typedef long __blksize_t;
typedef long __blkcnt_t;
typedef long __blkcnt64_t;
typedef unsigned long __fsblkcnt_t;
typedef unsigned long __fsblkcnt64_t;
typedef unsigned long __fsfilcnt_t;
typedef unsigned long __fsfilcnt64_t;
typedef long __fsword_t;
typedef long __ssize_t;
typedef long __syscall_slong_t;
typedef unsigned long __syscall_ulong_t;
typedef __off64_t __loff_t;
typedef char *__caddr_t;
typedef long __intptr_t;
typedef unsigned int __socklen_t;
typedef int __sig_atomic_t;
typedef __int8_t int8_t;
typedef __int16_t int16_t;
typedef __int32_t int32_t;
typedef __int64_t int64_t;
typedef __uint8_t uint8_t;
typedef __uint16_t uint16_t;
typedef __uint32_t uint32_t;
typedef __uint64_t uint64_t;
typedef __int_least8_t int_least8_t;
typedef __int_least16_t int_least16_t;
typedef __int_least32_t int_least32_t;
typedef __int_least64_t int_least64_t;
typedef __uint_least8_t uint_least8_t;
typedef __uint_least16_t uint_least16_t;
typedef __uint_least32_t uint_least32_t;
typedef __uint_least64_t uint_least64_t;
typedef signed char int_fast8_t;
typedef long int_fast16_t;
typedef long int_fast32_t;
typedef long int_fast64_t;
typedef unsigned char uint_fast8_t;
typedef unsigned long uint_fast16_t;
typedef unsigned long uint_fast32_t;
typedef unsigned long uint_fast64_t;
typedef long intptr_t;
typedef unsigned long uintptr_t;
typedef __intmax_t intmax_t;
typedef __uintmax_t uintmax_t;
extern uint64_t revng_init_local_sp(uint64_t param_0);
void bb_key_to_opts(uint64_t rdi, uint64_t rsi) {
    uint64_t rax_7;
    uint64_t rax_8;
    uint64_t rdx_0;
    uint64_t rdx_1;
    uint64_t var_0;
    uint64_t var_1;
    uint64_t rax_0;
    uint64_t var_2;
    uint64_t rax_1;
    uint64_t rax_2;
    uint64_t rax_3;
    uint64_t rax_4;
    uint64_t rax_5;
    uint64_t rax_6;
    revng_init_local_sp(0UL);
    rdx_0 = rsi;
    rdx_1 = rsi;
    if (*(unsigned char *)(rdi + 48UL) == '\x00' && *(unsigned char *)(rdi + 49UL) == '\x00') {
        if (*(uint64_t *)(rdi + 32UL) == 6460736UL) {
            *(unsigned char *)rdx_0 = (unsigned char)'d';
            rdx_1 = rdx_0 + 1UL;
        }
    } else {
        *(unsigned char *)rsi = (unsigned char)'b';
        var_0 = *(uint64_t *)(rdi + 32UL);
        var_1 = rsi + 1UL;
        rdx_0 = var_1;
        rdx_1 = var_1;
        if (var_0 == 6460736UL) {
            *(unsigned char *)rdx_0 = (unsigned char)'d';
            rdx_1 = rdx_0 + 1UL;
        }
    }
    rax_0 = rdx_1;
    if (*(uint64_t *)(rdi + 40UL) == 0UL) {
        var_2 = rdx_1 + 1UL;
        *(unsigned char *)rdx_1 = (unsigned char)'f';
        rax_0 = var_2;
    }
    rax_1 = rax_0;
    if (*(unsigned char *)(rdi + 52UL) == '\x00') {
        *(unsigned char *)rax_0 = (unsigned char)'g';
        rax_1 = rax_0 + 1UL;
    }
    rax_2 = rax_1;
    if (*(unsigned char *)(rdi + 53UL) == '\x00') {
        *(unsigned char *)rax_1 = (unsigned char)'h';
        rax_2 = rax_1 + 1UL;
    }
    rax_3 = rax_2;
    if (*(uint64_t *)(rdi + 32UL) == 6460992UL) {
        *(unsigned char *)rax_2 = (unsigned char)'i';
        rax_3 = rax_2 + 1UL;
    }
    rax_4 = rax_3;
    if (*(unsigned char *)(rdi + 54UL) == '\x00') {
        *(unsigned char *)rax_3 = (unsigned char)'M';
        rax_4 = rax_3 + 1UL;
    }
    rax_5 = rax_4;
    if (*(unsigned char *)(rdi + 50UL) == '\x00') {
        *(unsigned char *)rax_4 = (unsigned char)'n';
        rax_5 = rax_4 + 1UL;
    }
    rax_6 = rax_5;
    if (*(unsigned char *)(rdi + 51UL) == '\x00') {
        *(unsigned char *)rax_5 = (unsigned char)'R';
        rax_6 = rax_5 + 1UL;
    }
    rax_7 = rax_6;
    if (*(unsigned char *)(rdi + 55UL) == '\x00') {
        *(unsigned char *)rax_6 = (unsigned char)'r';
        rax_7 = rax_6 + 1UL;
    }
    rax_8 = rax_7;
    if (*(unsigned char *)(rdi + 56UL) == '\x00') {
        *(unsigned char *)rax_8 = (unsigned char)'\x00';
        return;
    }
    *(unsigned char *)rax_7 = (unsigned char)'V';
    rax_8 = rax_7 + 1UL;
    *(unsigned char *)rax_8 = (unsigned char)'\x00';
    return;
}

typedef unsigned char __u_char;
typedef unsigned short __u_short;
typedef unsigned int __u_int;
typedef unsigned long __u_long;
typedef signed char __int8_t;
typedef unsigned char __uint8_t;
typedef short __int16_t;
typedef unsigned short __uint16_t;
typedef int __int32_t;
typedef unsigned int __uint32_t;
typedef long __int64_t;
typedef unsigned long __uint64_t;
typedef __int8_t __int_least8_t;
typedef __uint8_t __uint_least8_t;
typedef __int16_t __int_least16_t;
typedef __uint16_t __uint_least16_t;
typedef __int32_t __int_least32_t;
typedef __uint32_t __uint_least32_t;
typedef __int64_t __int_least64_t;
typedef __uint64_t __uint_least64_t;
typedef long __quad_t;
typedef unsigned long __u_quad_t;
typedef long __intmax_t;
typedef unsigned long __uintmax_t;
typedef unsigned long __dev_t;
typedef unsigned int __uid_t;
typedef unsigned int __gid_t;
typedef unsigned long __ino_t;
typedef unsigned long __ino64_t;
typedef unsigned int __mode_t;
typedef unsigned long __nlink_t;
typedef long __off_t;
typedef long __off64_t;
typedef int __pid_t;
typedef struct {
    int __val[2];
} __fsid_t;
typedef long __clock_t;
typedef unsigned long __rlim_t;
typedef unsigned long __rlim64_t;
typedef unsigned int __id_t;
typedef long __time_t;
typedef unsigned int __useconds_t;
typedef long __suseconds_t;
typedef int __daddr_t;
typedef int __key_t;
typedef int __clockid_t;
typedef void *__timer_t;
typedef long __blksize_t;
typedef long __blkcnt_t;
typedef long __blkcnt64_t;
typedef unsigned long __fsblkcnt_t;
typedef unsigned long __fsblkcnt64_t;
typedef unsigned long __fsfilcnt_t;
typedef unsigned long __fsfilcnt64_t;
typedef long __fsword_t;
typedef long __ssize_t;
typedef long __syscall_slong_t;
typedef unsigned long __syscall_ulong_t;
typedef __off64_t __loff_t;
typedef char *__caddr_t;
typedef long __intptr_t;
typedef unsigned int __socklen_t;
typedef int __sig_atomic_t;
typedef __int8_t int8_t;
typedef __int16_t int16_t;
typedef __int32_t int32_t;
typedef __int64_t int64_t;
typedef __uint8_t uint8_t;
typedef __uint16_t uint16_t;
typedef __uint32_t uint32_t;
typedef __uint64_t uint64_t;
typedef __int_least8_t int_least8_t;
typedef __int_least16_t int_least16_t;
typedef __int_least32_t int_least32_t;
typedef __int_least64_t int_least64_t;
typedef __uint_least8_t uint_least8_t;
typedef __uint_least16_t uint_least16_t;
typedef __uint_least32_t uint_least32_t;
typedef __uint_least64_t uint_least64_t;
typedef signed char int_fast8_t;
typedef long int_fast16_t;
typedef long int_fast32_t;
typedef long int_fast64_t;
typedef unsigned char uint_fast8_t;
typedef unsigned long uint_fast16_t;
typedef unsigned long uint_fast32_t;
typedef unsigned long uint_fast64_t;
typedef long intptr_t;
typedef unsigned long uintptr_t;
typedef __intmax_t intmax_t;
typedef __uintmax_t uintmax_t;
typedef _Bool bool;
struct indirect_placeholder_197_ret_type;
struct indirect_placeholder_198_ret_type;
struct indirect_placeholder_197_ret_type {
    uint64_t field_0;
    uint64_t field_1;
};
struct indirect_placeholder_198_ret_type {
    uint64_t field_0;
    uint64_t field_1;
    uint64_t field_2;
};
extern uint64_t revng_init_local_sp(uint64_t param_0);
extern uint64_t init_rbx(void);
extern uint64_t init_rbp(void);
extern void indirect_placeholder_2(void);
extern uint64_t init_cc_src2(void);
extern uint64_t helper_cc_compute_c_wrapper(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint32_t param_3);
extern uint64_t init_r13(void);
extern uint64_t init_r12(void);
extern uint64_t init_r14(void);
extern uint64_t init_rax(void);
extern uint64_t helper_cc_compute_all_wrapper(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint32_t param_3);
extern struct indirect_placeholder_197_ret_type indirect_placeholder_197(uint64_t param_0, uint64_t param_1, uint64_t param_2);
extern struct indirect_placeholder_198_ret_type indirect_placeholder_198(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint64_t param_3);
uint64_t bb_write_unique(uint64_t rdi, uint64_t rdx, uint64_t rsi, uint64_t r8) {
    uint64_t var_0;
    uint64_t var_1;
    uint64_t var_2;
    uint64_t var_3;
    uint64_t var_4;
    uint64_t var_5;
    uint64_t var_6;
    uint64_t var_7;
    unsigned char var_8;
    uint64_t var_9;
    uint64_t r84_6;
    uint64_t var_25;
    uint64_t var_26;
    uint64_t rax_2;
    uint64_t var_27;
    uint64_t var_28;
    uint64_t r84_1;
    uint64_t r84_4;
    uint64_t rax_0;
    uint64_t r84_5;
    uint64_t var_10;
    struct indirect_placeholder_197_ret_type var_11;
    uint64_t var_12;
    uint64_t var_13;
    uint64_t local_sp_0;
    uint64_t r84_0;
    uint64_t var_14;
    uint64_t var_15;
    uint64_t var_16;
    uint64_t var_17;
    bool var_18;
    bool var_19;
    uint64_t var_29;
    uint64_t rax_1;
    uint64_t rdx2_0_in;
    uint64_t rax_3;
    uint64_t var_20;
    uint64_t var_21;
    struct indirect_placeholder_198_ret_type var_22;
    uint64_t var_23;
    uint64_t var_24;
    uint64_t r84_2;
    uint64_t r84_3;
    var_0 = revng_init_local_sp(0UL);
    var_1 = init_rax();
    var_2 = init_r13();
    var_3 = init_rbx();
    var_4 = init_r12();
    var_5 = init_rbp();
    var_6 = init_cc_src2();
    var_7 = init_r14();
    var_8 = *(unsigned char *)6460441UL;
    *(uint64_t *)(var_0 + (-8L)) = var_7;
    *(uint64_t *)(var_0 + (-16L)) = var_2;
    *(uint64_t *)(var_0 + (-24L)) = var_4;
    *(uint64_t *)(var_0 + (-32L)) = var_5;
    var_9 = var_0 + (-40L);
    *(uint64_t *)var_9 = var_3;
    r84_6 = r8;
    rax_2 = 1UL;
    rax_0 = var_1;
    r84_5 = r8;
    local_sp_0 = var_9;
    r84_0 = r8;
    if (var_8 == '\x00') {
        indirect_placeholder_2();
        return r84_6;
    }
    if (*(uint64_t *)6461504UL != 0UL) {
        if (*(uint64_t *)6460432UL != 0UL) {
            var_10 = var_0 + (-48L);
            *(uint64_t *)var_10 = 4236828UL;
            var_11 = indirect_placeholder_197(rdi, 6461504UL, r8);
            var_12 = var_11.field_0;
            var_13 = var_11.field_1;
            r84_4 = var_13;
            rax_0 = var_12;
            r84_5 = var_13;
            local_sp_0 = var_10;
            r84_0 = var_13;
            if ((uint64_t)(uint32_t)var_12 == 0UL) {
                if (*(unsigned char *)6460441UL == '\x00') {
                    return r84_4;
                }
                if (*(unsigned char *)6460442UL == '\x00') {
                    return r84_4;
                }
            }
            *(uint64_t *)6461504UL = *(uint64_t *)rdi;
            *(uint64_t *)6461512UL = *(uint64_t *)(rdi + 8UL);
            *(uint64_t *)6461520UL = *(uint64_t *)(rdi + 16UL);
            *(uint64_t *)6461528UL = *(uint64_t *)(rdi + 24UL);
            r84_6 = r84_5;
            indirect_placeholder_2();
            return r84_6;
        }
        var_14 = *(uint64_t *)(rdi + 8UL);
        var_15 = *(uint64_t *)6461512UL;
        var_16 = var_15 + (-1L);
        var_17 = var_14 + (-1L);
        var_18 = (var_17 == 0UL);
        var_19 = (var_16 == 0UL);
        r84_1 = r84_0;
        r84_2 = r84_0;
        r84_5 = r84_0;
        if (var_18) {
            var_29 = var_19 ? 0UL : 4294967295UL;
            rax_1 = var_29;
            rax_3 = rax_1;
            r84_2 = r84_1;
            rdx2_0_in = rax_1;
            r84_3 = r84_1;
            if (*(unsigned char *)6460443UL == '\x00') {
                rdx2_0_in = 0UL - rax_3;
                r84_3 = r84_2;
            }
            r84_4 = r84_3;
            r84_5 = r84_3;
            if ((uint64_t)(uint32_t)rdx2_0_in == 0UL) {
                return r84_4;
            }
        }
        if (!var_19) {
            rax_3 = rax_2;
            if (*(unsigned char *)6460443UL != '\x00') {
                rdx2_0_in = 0UL - rax_3;
                r84_3 = r84_2;
                r84_4 = r84_3;
                r84_5 = r84_3;
                if ((uint64_t)(uint32_t)rdx2_0_in == 0UL) {
                    return r84_4;
                }
            }
            *(uint64_t *)6461504UL = *(uint64_t *)rdi;
            *(uint64_t *)6461512UL = *(uint64_t *)(rdi + 8UL);
            *(uint64_t *)6461520UL = *(uint64_t *)(rdi + 16UL);
            *(uint64_t *)6461528UL = *(uint64_t *)(rdi + 24UL);
            r84_6 = r84_5;
            indirect_placeholder_2();
            return r84_6;
        }
        rax_2 = rax_0;
        if (*(unsigned char *)6461537UL != '\x00') {
            var_20 = *(uint64_t *)6461504UL;
            var_21 = *(uint64_t *)rdi;
            *(uint64_t *)(local_sp_0 + (-8L)) = 4237103UL;
            var_22 = indirect_placeholder_198(var_21, var_15, var_20, var_14);
            var_23 = var_22.field_0;
            var_24 = var_22.field_2;
            rax_1 = var_23;
            r84_1 = var_24;
            rax_3 = rax_1;
            r84_2 = r84_1;
            rdx2_0_in = rax_1;
            r84_3 = r84_1;
            if (*(unsigned char *)6460443UL == '\x00') {
                rdx2_0_in = 0UL - rax_3;
                r84_3 = r84_2;
            }
            r84_4 = r84_3;
            r84_5 = r84_3;
            if ((uint64_t)(uint32_t)rdx2_0_in == 0UL) {
                return r84_4;
            }
        }
        *(uint64_t *)(local_sp_0 + (-8L)) = 4237031UL;
        indirect_placeholder_2();
        if ((uint64_t)(uint32_t)rax_0 == 0UL) {
            var_25 = var_17 - var_16;
            var_26 = helper_cc_compute_c_wrapper(var_25, var_16, var_6, 17U);
            rax_2 = 4294967295UL;
            if (var_26 != 0UL) {
                rax_3 = rax_2;
                if (*(unsigned char *)6460443UL != '\x00') {
                    rdx2_0_in = 0UL - rax_3;
                    r84_3 = r84_2;
                    r84_4 = r84_3;
                    r84_5 = r84_3;
                    if ((uint64_t)(uint32_t)rdx2_0_in == 0UL) {
                        return r84_4;
                    }
                }
            }
            var_27 = helper_cc_compute_all_wrapper(var_25, var_16, var_6, 17U);
            var_28 = ((var_27 >> 6UL) & 1UL) ^ 1UL;
            rax_1 = var_28;
            rax_3 = rax_1;
            r84_2 = r84_1;
            rdx2_0_in = rax_1;
            r84_3 = r84_1;
            if (*(unsigned char *)6460443UL == '\x00') {
                rdx2_0_in = 0UL - rax_3;
                r84_3 = r84_2;
            }
            r84_4 = r84_3;
            r84_5 = r84_3;
            if ((uint64_t)(uint32_t)rdx2_0_in == 0UL) {
                return r84_4;
            }
        }
        rax_3 = rax_2;
        if (*(unsigned char *)6460443UL != '\x00') {
            rdx2_0_in = 0UL - rax_3;
            r84_3 = r84_2;
            r84_4 = r84_3;
            r84_5 = r84_3;
            if ((uint64_t)(uint32_t)rdx2_0_in == 0UL) {
                return r84_4;
            }
        }
        *(uint64_t *)6461504UL = *(uint64_t *)rdi;
        *(uint64_t *)6461512UL = *(uint64_t *)(rdi + 8UL);
        *(uint64_t *)6461520UL = *(uint64_t *)(rdi + 16UL);
        *(uint64_t *)6461528UL = *(uint64_t *)(rdi + 24UL);
        r84_6 = r84_5;
        indirect_placeholder_2();
        return r84_6;
    }
    *(uint64_t *)6461504UL = *(uint64_t *)rdi;
    *(uint64_t *)6461512UL = *(uint64_t *)(rdi + 8UL);
    *(uint64_t *)6461520UL = *(uint64_t *)(rdi + 16UL);
    *(uint64_t *)6461528UL = *(uint64_t *)(rdi + 24UL);
    r84_6 = r84_5;
}

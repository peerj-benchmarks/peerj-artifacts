typedef unsigned char __u_char;
typedef unsigned short __u_short;
typedef unsigned int __u_int;
typedef unsigned long __u_long;
typedef signed char __int8_t;
typedef unsigned char __uint8_t;
typedef short __int16_t;
typedef unsigned short __uint16_t;
typedef int __int32_t;
typedef unsigned int __uint32_t;
typedef long __int64_t;
typedef unsigned long __uint64_t;
typedef __int8_t __int_least8_t;
typedef __uint8_t __uint_least8_t;
typedef __int16_t __int_least16_t;
typedef __uint16_t __uint_least16_t;
typedef __int32_t __int_least32_t;
typedef __uint32_t __uint_least32_t;
typedef __int64_t __int_least64_t;
typedef __uint64_t __uint_least64_t;
typedef long __quad_t;
typedef unsigned long __u_quad_t;
typedef long __intmax_t;
typedef unsigned long __uintmax_t;
typedef unsigned long __dev_t;
typedef unsigned int __uid_t;
typedef unsigned int __gid_t;
typedef unsigned long __ino_t;
typedef unsigned long __ino64_t;
typedef unsigned int __mode_t;
typedef unsigned long __nlink_t;
typedef long __off_t;
typedef long __off64_t;
typedef int __pid_t;
typedef struct {
    int __val[2];
} __fsid_t;
typedef long __clock_t;
typedef unsigned long __rlim_t;
typedef unsigned long __rlim64_t;
typedef unsigned int __id_t;
typedef long __time_t;
typedef unsigned int __useconds_t;
typedef long __suseconds_t;
typedef int __daddr_t;
typedef int __key_t;
typedef int __clockid_t;
typedef void *__timer_t;
typedef long __blksize_t;
typedef long __blkcnt_t;
typedef long __blkcnt64_t;
typedef unsigned long __fsblkcnt_t;
typedef unsigned long __fsblkcnt64_t;
typedef unsigned long __fsfilcnt_t;
typedef unsigned long __fsfilcnt64_t;
typedef long __fsword_t;
typedef long __ssize_t;
typedef long __syscall_slong_t;
typedef unsigned long __syscall_ulong_t;
typedef __off64_t __loff_t;
typedef char *__caddr_t;
typedef long __intptr_t;
typedef unsigned int __socklen_t;
typedef int __sig_atomic_t;
typedef __int8_t int8_t;
typedef __int16_t int16_t;
typedef __int32_t int32_t;
typedef __int64_t int64_t;
typedef __uint8_t uint8_t;
typedef __uint16_t uint16_t;
typedef __uint32_t uint32_t;
typedef __uint64_t uint64_t;
typedef __int_least8_t int_least8_t;
typedef __int_least16_t int_least16_t;
typedef __int_least32_t int_least32_t;
typedef __int_least64_t int_least64_t;
typedef __uint_least8_t uint_least8_t;
typedef __uint_least16_t uint_least16_t;
typedef __uint_least32_t uint_least32_t;
typedef __uint_least64_t uint_least64_t;
typedef signed char int_fast8_t;
typedef long int_fast16_t;
typedef long int_fast32_t;
typedef long int_fast64_t;
typedef unsigned char uint_fast8_t;
typedef unsigned long uint_fast16_t;
typedef unsigned long uint_fast32_t;
typedef unsigned long uint_fast64_t;
typedef long intptr_t;
typedef unsigned long uintptr_t;
typedef __intmax_t intmax_t;
typedef __uintmax_t uintmax_t;
struct indirect_placeholder_258_ret_type;
struct indirect_placeholder_258_ret_type {
    uint64_t field_0;
    uint64_t field_1;
    uint64_t field_2;
};
extern void abort(void);
extern uint64_t revng_init_local_sp(uint64_t param_0);
extern uint64_t init_rbx(void);
extern uint64_t init_rbp(void);
extern void indirect_placeholder_2(void);
extern uint64_t init_r13(void);
extern uint64_t init_r12(void);
extern uint64_t init_r14(void);
extern uint64_t helper_cc_compute_all_wrapper(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint32_t param_3);
extern uint64_t init_r15(void);
extern void indirect_placeholder_4(uint64_t param_0, uint64_t param_1);
extern struct indirect_placeholder_258_ret_type indirect_placeholder_258(uint64_t param_0, uint64_t param_1);
void bb_heap_insert(uint64_t rdi, uint64_t r9, uint64_t rsi, uint64_t r8) {
    uint64_t var_0;
    uint64_t var_1;
    uint64_t var_2;
    uint64_t var_3;
    uint64_t var_4;
    uint64_t var_5;
    uint64_t var_6;
    uint64_t *var_7;
    uint64_t var_8;
    uint64_t *var_9;
    uint64_t var_10;
    uint64_t rbx_1;
    uint64_t var_27;
    uint64_t local_sp_1;
    uint64_t *var_11;
    uint64_t var_12;
    uint64_t rbx_0;
    uint64_t var_20;
    uint64_t var_13;
    uint64_t rax_0;
    uint64_t rax_1;
    uint64_t rsi3_0;
    uint64_t var_14;
    struct indirect_placeholder_258_ret_type var_15;
    uint64_t var_16;
    uint64_t var_17;
    uint64_t *_pre_phi;
    uint64_t var_18;
    uint64_t *var_19;
    uint64_t local_sp_0;
    uint64_t rax_2;
    uint64_t var_21;
    uint64_t var_22;
    uint64_t var_23;
    uint64_t rax_3;
    uint64_t rbx_2;
    uint64_t var_24;
    uint64_t var_25;
    uint64_t var_26;
    var_0 = revng_init_local_sp(0UL);
    var_1 = init_r15();
    var_2 = init_r13();
    var_3 = init_rbx();
    var_4 = init_r12();
    var_5 = init_rbp();
    var_6 = init_r14();
    *(uint64_t *)(var_0 + (-8L)) = var_1;
    *(uint64_t *)(var_0 + (-16L)) = var_6;
    *(uint64_t *)(var_0 + (-24L)) = var_2;
    *(uint64_t *)(var_0 + (-32L)) = var_4;
    *(uint64_t *)(var_0 + (-40L)) = var_5;
    *(uint64_t *)(var_0 + (-48L)) = var_3;
    var_7 = (uint64_t *)(rdi + 8UL);
    var_8 = *var_7;
    var_9 = (uint64_t *)(rdi + 16UL);
    var_10 = *var_9;
    rbx_1 = 8UL;
    rbx_0 = var_10;
    rax_0 = var_8;
    rax_1 = 16UL;
    rsi3_0 = 128UL;
    if ((var_8 + (-1L)) > var_10) {
        var_18 = var_0 + (-56L);
        var_19 = (uint64_t *)rdi;
        _pre_phi = var_19;
        local_sp_0 = var_18;
        rax_2 = *var_19;
    } else {
        var_11 = (uint64_t *)rdi;
        var_12 = *var_11;
        _pre_phi = var_11;
        if (var_12 == 0UL) {
            if (var_8 != 0UL) {
                if (var_8 <= 1152921504606846975UL) {
                    *(uint64_t *)(var_0 + (-64L)) = 4255677UL;
                    indirect_placeholder_4(r9, r8);
                    abort();
                }
                rax_1 = rax_0;
                rsi3_0 = rax_0 << 3UL;
            }
        } else {
            if (var_8 > 768614336404564649UL) {
                *(uint64_t *)(var_0 + (-64L)) = 4255677UL;
                indirect_placeholder_4(r9, r8);
                abort();
            }
            var_13 = (var_8 + (var_8 >> 1UL)) + 1UL;
            rax_0 = var_13;
            rax_1 = rax_0;
            rsi3_0 = rax_0 << 3UL;
        }
        *var_7 = rax_1;
        var_14 = var_0 + (-64L);
        *(uint64_t *)var_14 = 4255508UL;
        var_15 = indirect_placeholder_258(var_12, rsi3_0);
        var_16 = var_15.field_0;
        var_17 = *var_9;
        *var_11 = var_16;
        local_sp_0 = var_14;
        rax_2 = var_16;
        rbx_0 = var_17;
    }
    var_20 = rbx_0 + 1UL;
    *var_9 = var_20;
    var_21 = var_20 << 3UL;
    *(uint64_t *)(var_21 + rax_2) = rsi;
    var_22 = *_pre_phi;
    var_23 = *(uint64_t *)(var_21 + var_22);
    local_sp_1 = local_sp_0;
    rax_3 = rax_2;
    rbx_2 = var_20;
    if (rbx_0 == 0UL) {
        *(uint64_t *)(rbx_1 + var_22) = var_23;
        return;
    }
    while (1U)
        {
            var_24 = rbx_2 >> 1UL;
            var_25 = local_sp_1 + (-8L);
            *(uint64_t *)var_25 = 4255591UL;
            indirect_placeholder_2();
            var_26 = helper_cc_compute_all_wrapper(rax_3, 0UL, 0UL, 24U);
            local_sp_1 = var_25;
            rbx_2 = var_24;
            if ((uint64_t)(((unsigned char)(var_26 >> 4UL) ^ (unsigned char)var_26) & '\xc0') == 0UL) {
                var_27 = *(uint64_t *)((var_24 << 3UL) + var_22);
                *(uint64_t *)((rbx_2 << 3UL) + var_22) = var_27;
                rax_3 = var_27;
                if (var_24 != 1UL) {
                    continue;
                }
                break;
            }
            rbx_1 = rbx_2 << 3UL;
            break;
        }
    *(uint64_t *)(rbx_1 + var_22) = var_23;
    return;
}

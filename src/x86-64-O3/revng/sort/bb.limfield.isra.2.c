typedef unsigned char __u_char;
typedef unsigned short __u_short;
typedef unsigned int __u_int;
typedef unsigned long __u_long;
typedef signed char __int8_t;
typedef unsigned char __uint8_t;
typedef short __int16_t;
typedef unsigned short __uint16_t;
typedef int __int32_t;
typedef unsigned int __uint32_t;
typedef long __int64_t;
typedef unsigned long __uint64_t;
typedef __int8_t __int_least8_t;
typedef __uint8_t __uint_least8_t;
typedef __int16_t __int_least16_t;
typedef __uint16_t __uint_least16_t;
typedef __int32_t __int_least32_t;
typedef __uint32_t __uint_least32_t;
typedef __int64_t __int_least64_t;
typedef __uint64_t __uint_least64_t;
typedef long __quad_t;
typedef unsigned long __u_quad_t;
typedef long __intmax_t;
typedef unsigned long __uintmax_t;
typedef unsigned long __dev_t;
typedef unsigned int __uid_t;
typedef unsigned int __gid_t;
typedef unsigned long __ino_t;
typedef unsigned long __ino64_t;
typedef unsigned int __mode_t;
typedef unsigned long __nlink_t;
typedef long __off_t;
typedef long __off64_t;
typedef int __pid_t;
typedef struct {
    int __val[2];
} __fsid_t;
typedef long __clock_t;
typedef unsigned long __rlim_t;
typedef unsigned long __rlim64_t;
typedef unsigned int __id_t;
typedef long __time_t;
typedef unsigned int __useconds_t;
typedef long __suseconds_t;
typedef int __daddr_t;
typedef int __key_t;
typedef int __clockid_t;
typedef void *__timer_t;
typedef long __blksize_t;
typedef long __blkcnt_t;
typedef long __blkcnt64_t;
typedef unsigned long __fsblkcnt_t;
typedef unsigned long __fsblkcnt64_t;
typedef unsigned long __fsfilcnt_t;
typedef unsigned long __fsfilcnt64_t;
typedef long __fsword_t;
typedef long __ssize_t;
typedef long __syscall_slong_t;
typedef unsigned long __syscall_ulong_t;
typedef __off64_t __loff_t;
typedef char *__caddr_t;
typedef long __intptr_t;
typedef unsigned int __socklen_t;
typedef int __sig_atomic_t;
typedef __int8_t int8_t;
typedef __int16_t int16_t;
typedef __int32_t int32_t;
typedef __int64_t int64_t;
typedef __uint8_t uint8_t;
typedef __uint16_t uint16_t;
typedef __uint32_t uint32_t;
typedef __uint64_t uint64_t;
typedef __int_least8_t int_least8_t;
typedef __int_least16_t int_least16_t;
typedef __int_least32_t int_least32_t;
typedef __int_least64_t int_least64_t;
typedef __uint_least8_t uint_least8_t;
typedef __uint_least16_t uint_least16_t;
typedef __uint_least32_t uint_least32_t;
typedef __uint_least64_t uint_least64_t;
typedef signed char int_fast8_t;
typedef long int_fast16_t;
typedef long int_fast32_t;
typedef long int_fast64_t;
typedef unsigned char uint_fast8_t;
typedef unsigned long uint_fast16_t;
typedef unsigned long uint_fast32_t;
typedef unsigned long uint_fast64_t;
typedef long intptr_t;
typedef unsigned long uintptr_t;
typedef __intmax_t intmax_t;
typedef __uintmax_t uintmax_t;
typedef _Bool bool;
struct bb_limfield_isra_2_ret_type;
struct bb_limfield_isra_2_ret_type {
    uint64_t field_0;
    uint64_t field_1;
    uint64_t field_2;
};
extern uint64_t revng_init_local_sp(uint64_t param_0);
extern uint64_t init_cc_src2(void);
extern uint64_t helper_cc_compute_c_wrapper(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint32_t param_3);
extern uint64_t init_r9(void);
struct bb_limfield_isra_2_ret_type bb_limfield_isra_2(uint64_t rdi, uint64_t rdx, uint64_t rsi) {
    uint64_t var_0;
    uint64_t var_1;
    uint64_t var_2;
    uint64_t var_3;
    uint32_t var_4;
    uint64_t var_5;
    uint64_t var_6;
    uint64_t var_7;
    uint64_t var_8;
    bool var_9;
    bool var_10;
    uint64_t r9_1;
    uint64_t var_11;
    uint64_t rax_5;
    uint64_t r9_0;
    uint64_t rax_0;
    uint64_t rax_1;
    uint64_t rcx_0_in;
    uint64_t rcx_0;
    uint64_t rax_2;
    uint64_t rax_6;
    unsigned char var_12;
    uint64_t rax_3;
    uint64_t r8_0;
    uint64_t rax_4;
    uint64_t rcx_1_in;
    uint64_t rcx_1;
    uint64_t r8_1;
    uint64_t rcx_2_in;
    uint64_t rcx_2;
    unsigned char var_13;
    uint64_t var_14;
    uint64_t rdx7_0_in;
    uint64_t rdx7_0;
    uint64_t var_15;
    struct bb_limfield_isra_2_ret_type mrv;
    struct bb_limfield_isra_2_ret_type mrv1;
    struct bb_limfield_isra_2_ret_type mrv2;
    struct bb_limfield_isra_2_ret_type mrv3;
    struct bb_limfield_isra_2_ret_type mrv4;
    struct bb_limfield_isra_2_ret_type mrv5;
    unsigned int loop_state_var;
    bool switch_state_var;
    revng_init_local_sp(0UL);
    var_0 = init_r9();
    var_1 = init_cc_src2();
    var_2 = *(uint64_t *)(rdx + 24UL);
    var_3 = *(uint64_t *)(rdx + 16UL);
    var_4 = *(uint32_t *)6459420UL;
    var_5 = (uint64_t)var_4;
    var_6 = (rsi + rdi) + (-1L);
    var_7 = helper_cc_compute_c_wrapper(var_2 + (-1L), 1UL, var_1, 17U);
    var_8 = var_3 + var_7;
    var_9 = ((uint64_t)(var_4 + (-128)) == 0UL);
    var_10 = (var_6 > rdi);
    r9_1 = var_0;
    rax_5 = rdi;
    rax_0 = rdi;
    rax_2 = var_6;
    rax_3 = rdi;
    r8_0 = var_8;
    r8_1 = var_5;
    if (var_9) {
        r8_1 = 0UL;
        if (!var_10 & var_8 != 0UL) {
            var_12 = *(unsigned char *)rdi;
            rax_5 = var_6;
            while (1U)
                {
                    rcx_1_in = rax_3;
                    rax_4 = rax_3;
                    r8_1 = r8_0;
                    if (*(unsigned char *)((uint64_t)var_12 + 6461248UL) != '\x00') {
                        while (1U)
                            {
                                rcx_1 = rcx_1_in + 1UL;
                                rcx_1_in = rcx_1;
                                rax_4 = rcx_1;
                                if (rcx_1 != var_6) {
                                    loop_state_var = 1U;
                                    break;
                                }
                                if (*(unsigned char *)((uint64_t)*(unsigned char *)rcx_1 + 6461248UL) == '\x00') {
                                    continue;
                                }
                                loop_state_var = 0U;
                                break;
                            }
                        switch_state_var = 0;
                        switch (loop_state_var) {
                          case 1U:
                            {
                                switch_state_var = 1;
                                break;
                            }
                            break;
                          case 0U:
                            {
                                break;
                            }
                            break;
                        }
                        if (switch_state_var)
                            break;
                    }
                    rcx_2_in = rax_4;
                    while (1U)
                        {
                            rcx_2 = rcx_2_in + 1UL;
                            rax_3 = rcx_2;
                            rcx_2_in = rcx_2;
                            rax_5 = rcx_2;
                            if (var_6 <= rcx_2) {
                                loop_state_var = 1U;
                                break;
                            }
                            var_13 = *(unsigned char *)rcx_2;
                            var_12 = var_13;
                            r8_1 = 0UL;
                            if (*(unsigned char *)((uint64_t)var_13 + 6461248UL) == '\x00') {
                                continue;
                            }
                            loop_state_var = 0U;
                            break;
                        }
                    switch_state_var = 0;
                    switch (loop_state_var) {
                      case 1U:
                        {
                            switch_state_var = 1;
                            break;
                        }
                        break;
                      case 0U:
                        {
                            var_14 = r8_0 + (-1L);
                            r8_0 = var_14;
                            if (var_14 != 0UL) {
                                continue;
                            }
                            switch_state_var = 1;
                            break;
                        }
                        break;
                    }
                    if (switch_state_var)
                        break;
                }
        }
    }
    var_11 = var_8 + (-1L);
    r9_0 = var_11;
    r9_1 = var_11;
    if (!var_10 && var_8 == 0UL) {
        rcx_0_in = rax_0;
        rax_1 = rax_0;
        r9_1 = r9_0;
        rax_5 = rax_0;
        while (var_6 <= rax_0)
            {
                if ((uint64_t)(var_4 - (uint32_t)(uint64_t)*(unsigned char *)rax_0) != 0UL) {
                    rax_5 = rax_1;
                    if ((r9_0 | var_2) == 0UL) {
                        break;
                    }
                    rax_2 = rax_1 + 1UL;
                    rax_0 = rax_2;
                    rax_5 = rax_2;
                    if ((var_6 <= rax_2) || (r9_0 == 0UL)) {
                        break;
                    }
                    r9_0 = r9_0 + (-1L);
                    rcx_0_in = rax_0;
                    rax_1 = rax_0;
                    r9_1 = r9_0;
                    rax_5 = rax_0;
                    continue;
                }
                while (1U)
                    {
                        rcx_0 = rcx_0_in + 1UL;
                        rcx_0_in = rcx_0;
                        rax_1 = rcx_0;
                        if (rcx_0 != var_6) {
                            loop_state_var = 0U;
                            break;
                        }
                        if ((uint64_t)(var_4 - (uint32_t)(uint64_t)*(unsigned char *)rcx_0) == 0UL) {
                            continue;
                        }
                        loop_state_var = 1U;
                        break;
                    }
                rax_5 = rax_1;
                if ((r9_0 | var_2) != 0UL) {
                    switch_state_var = 1;
                    break;
                }
                rax_2 = rax_1 + 1UL;
            }
    }
    rdx7_0_in = rax_5;
    rax_6 = rax_5;
    if (var_2 == 0UL) {
        mrv3.field_0 = rax_5;
        mrv4 = mrv3;
        mrv4.field_1 = r9_1;
        mrv5 = mrv4;
        mrv5.field_2 = r8_1;
        return mrv5;
    }
    if (!(!((*(unsigned char *)(rdx + 49UL) != '\x00') && (var_6 > rax_5)) && *(unsigned char *)((uint64_t)*(unsigned char *)rax_5 + 6461248UL) == '\x00')) {
        var_15 = rax_6 + var_2;
        mrv.field_0 = (var_15 > var_6) ? var_6 : var_15;
        mrv1 = mrv;
        mrv1.field_1 = r9_1;
        mrv2 = mrv1;
        mrv2.field_2 = r8_1;
        return mrv2;
    }
    rdx7_0 = rdx7_0_in + 1UL;
    rdx7_0_in = rdx7_0;
    rax_6 = var_6;
    while (rdx7_0 != var_6)
        {
            rax_6 = rdx7_0;
            if (*(unsigned char *)((uint64_t)*(unsigned char *)rdx7_0 + 6461248UL) == '\x00') {
                break;
            }
            rdx7_0 = rdx7_0_in + 1UL;
            rdx7_0_in = rdx7_0;
            rax_6 = var_6;
        }
    var_15 = rax_6 + var_2;
    mrv.field_0 = (var_15 > var_6) ? var_6 : var_15;
    mrv1 = mrv;
    mrv1.field_1 = r9_1;
    mrv2 = mrv1;
    mrv2.field_2 = r8_1;
    return mrv2;
}

typedef unsigned char __u_char;
typedef unsigned short __u_short;
typedef unsigned int __u_int;
typedef unsigned long __u_long;
typedef signed char __int8_t;
typedef unsigned char __uint8_t;
typedef short __int16_t;
typedef unsigned short __uint16_t;
typedef int __int32_t;
typedef unsigned int __uint32_t;
typedef long __int64_t;
typedef unsigned long __uint64_t;
typedef __int8_t __int_least8_t;
typedef __uint8_t __uint_least8_t;
typedef __int16_t __int_least16_t;
typedef __uint16_t __uint_least16_t;
typedef __int32_t __int_least32_t;
typedef __uint32_t __uint_least32_t;
typedef __int64_t __int_least64_t;
typedef __uint64_t __uint_least64_t;
typedef long __quad_t;
typedef unsigned long __u_quad_t;
typedef long __intmax_t;
typedef unsigned long __uintmax_t;
typedef unsigned long __dev_t;
typedef unsigned int __uid_t;
typedef unsigned int __gid_t;
typedef unsigned long __ino_t;
typedef unsigned long __ino64_t;
typedef unsigned int __mode_t;
typedef unsigned long __nlink_t;
typedef long __off_t;
typedef long __off64_t;
typedef int __pid_t;
typedef struct {
    int __val[2];
} __fsid_t;
typedef long __clock_t;
typedef unsigned long __rlim_t;
typedef unsigned long __rlim64_t;
typedef unsigned int __id_t;
typedef long __time_t;
typedef unsigned int __useconds_t;
typedef long __suseconds_t;
typedef int __daddr_t;
typedef int __key_t;
typedef int __clockid_t;
typedef void *__timer_t;
typedef long __blksize_t;
typedef long __blkcnt_t;
typedef long __blkcnt64_t;
typedef unsigned long __fsblkcnt_t;
typedef unsigned long __fsblkcnt64_t;
typedef unsigned long __fsfilcnt_t;
typedef unsigned long __fsfilcnt64_t;
typedef long __fsword_t;
typedef long __ssize_t;
typedef long __syscall_slong_t;
typedef unsigned long __syscall_ulong_t;
typedef __off64_t __loff_t;
typedef char *__caddr_t;
typedef long __intptr_t;
typedef unsigned int __socklen_t;
typedef int __sig_atomic_t;
typedef __int8_t int8_t;
typedef __int16_t int16_t;
typedef __int32_t int32_t;
typedef __int64_t int64_t;
typedef __uint8_t uint8_t;
typedef __uint16_t uint16_t;
typedef __uint32_t uint32_t;
typedef __uint64_t uint64_t;
typedef __int_least8_t int_least8_t;
typedef __int_least16_t int_least16_t;
typedef __int_least32_t int_least32_t;
typedef __int_least64_t int_least64_t;
typedef __uint_least8_t uint_least8_t;
typedef __uint_least16_t uint_least16_t;
typedef __uint_least32_t uint_least32_t;
typedef __uint_least64_t uint_least64_t;
typedef signed char int_fast8_t;
typedef long int_fast16_t;
typedef long int_fast32_t;
typedef long int_fast64_t;
typedef unsigned char uint_fast8_t;
typedef unsigned long uint_fast16_t;
typedef unsigned long uint_fast32_t;
typedef unsigned long uint_fast64_t;
typedef long intptr_t;
typedef unsigned long uintptr_t;
typedef __intmax_t intmax_t;
typedef __uintmax_t uintmax_t;
struct indirect_placeholder_15_ret_type;
struct indirect_placeholder_16_ret_type;
struct indirect_placeholder_17_ret_type;
struct indirect_placeholder_15_ret_type {
    uint64_t field_0;
    uint64_t field_1;
};
struct indirect_placeholder_16_ret_type {
    uint64_t field_0;
    uint64_t field_1;
    uint64_t field_2;
    uint64_t field_3;
    uint64_t field_4;
    uint64_t field_5;
};
struct indirect_placeholder_17_ret_type {
    uint64_t field_0;
    uint64_t field_1;
};
extern uint64_t revng_init_local_sp(uint64_t param_0);
extern uint64_t init_rbx(void);
extern uint64_t init_rbp(void);
extern void indirect_placeholder(void);
extern uint64_t init_cc_src2(void);
extern uint64_t helper_cc_compute_c_wrapper(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint32_t param_3);
extern uint64_t init_r13(void);
extern uint64_t init_r12(void);
extern uint64_t init_rax(void);
extern uint64_t helper_cc_compute_all_wrapper(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint32_t param_3);
extern struct indirect_placeholder_15_ret_type indirect_placeholder_15(void);
extern struct indirect_placeholder_16_ret_type indirect_placeholder_16(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint64_t param_3, uint64_t param_4, uint64_t param_5, uint64_t param_6);
extern struct indirect_placeholder_17_ret_type indirect_placeholder_17(void);
uint64_t bb_iread(uint64_t rdi, uint64_t rsi, uint64_t rdx) {
    uint64_t var_0;
    uint64_t var_1;
    uint64_t var_2;
    uint64_t var_3;
    uint64_t var_4;
    uint64_t var_5;
    uint64_t var_6;
    uint64_t rax_1;
    uint64_t var_12;
    uint64_t var_13;
    uint64_t rax_0;
    uint64_t var_14;
    uint64_t var_15;
    uint64_t rbx_0;
    uint64_t var_10;
    uint64_t local_sp_2;
    uint64_t var_11;
    uint64_t var_16;
    uint64_t local_sp_0;
    uint64_t var_17;
    uint64_t var_18;
    uint64_t var_19;
    uint64_t var_20;
    uint64_t var_21;
    uint64_t cc_src_0;
    uint64_t var_22;
    uint64_t var_23;
    struct indirect_placeholder_15_ret_type var_24;
    uint64_t var_25;
    uint64_t var_26;
    uint64_t r9_0;
    uint64_t var_27;
    uint64_t local_sp_1;
    uint64_t r8_0;
    uint64_t var_28;
    uint64_t var_29;
    uint64_t var_30;
    uint64_t var_31;
    uint64_t var_32;
    uint64_t var_33;
    uint64_t var_34;
    struct indirect_placeholder_17_ret_type var_7;
    uint64_t var_8;
    uint64_t var_9;
    unsigned int loop_state_var;
    var_0 = revng_init_local_sp(0UL);
    var_1 = init_rax();
    var_2 = init_r13();
    var_3 = init_rbx();
    var_4 = init_r12();
    var_5 = init_rbp();
    var_6 = init_cc_src2();
    *(uint64_t *)(var_0 + (-8L)) = var_2;
    *(uint64_t *)(var_0 + (-16L)) = var_4;
    *(uint64_t *)(var_0 + (-24L)) = var_5;
    *(uint64_t *)(var_0 + (-32L)) = var_3;
    cc_src_0 = rdx;
    rax_1 = var_1;
    local_sp_2 = var_0 + (-40L);
    while (1U)
        {
            *(uint64_t *)(local_sp_2 + (-8L)) = 4215116UL;
            var_7 = indirect_placeholder_17();
            var_8 = local_sp_2 + (-16L);
            *(uint64_t *)var_8 = 4215130UL;
            indirect_placeholder();
            var_9 = helper_cc_compute_all_wrapper(rax_1 + 1UL, 18446744073709551615UL, var_6, 17U);
            rax_0 = rax_1;
            local_sp_0 = var_8;
            local_sp_1 = var_8;
            rbx_0 = rax_1;
            if ((var_9 & 64UL) != 0UL) {
                var_10 = local_sp_2 + (-24L);
                *(uint64_t *)var_10 = 4215144UL;
                indirect_placeholder();
                var_11 = helper_cc_compute_all_wrapper((uint64_t)*(uint32_t *)rax_1 + (-22L), 22UL, var_6, 16U);
                local_sp_0 = var_10;
                rbx_0 = 0UL;
                if ((var_11 & 64UL) == 0UL) {
                    var_17 = local_sp_0 + (-8L);
                    *(uint64_t *)var_17 = 4215106UL;
                    indirect_placeholder();
                    var_18 = helper_cc_compute_all_wrapper((uint64_t)*(uint32_t *)rax_0 + (-4L), 4UL, var_6, 16U);
                    rax_1 = rax_0;
                    local_sp_2 = var_17;
                    if ((var_18 & 64UL) == 0UL) {
                        continue;
                    }
                    loop_state_var = 1U;
                    break;
                }
                var_12 = *(uint64_t *)6400664UL;
                var_13 = helper_cc_compute_all_wrapper(var_12, var_11, var_6, 25U);
                rax_0 = var_12;
                var_14 = helper_cc_compute_all_wrapper(rdx - var_12, var_12, var_6, 17U);
                var_15 = helper_cc_compute_all_wrapper((uint64_t)(*(unsigned char *)6401289UL & '@'), var_14, var_6, 22U);
                if ((uint64_t)(((unsigned char)(var_13 >> 4UL) ^ (unsigned char)var_13) & '\xc0') != 0UL & (var_14 & 65UL) != 0UL & (var_15 & 64UL) != 0UL) {
                    *(uint64_t *)(local_sp_2 + (-32L)) = 4215180UL;
                    indirect_placeholder();
                    *(uint32_t *)var_12 = 0U;
                    loop_state_var = 1U;
                    break;
                }
            }
            var_16 = helper_cc_compute_all_wrapper(rax_1, var_9, var_6, 25U);
            if ((signed char)(unsigned char)var_16 <= '\xff') {
                var_19 = var_7.field_0;
                var_20 = var_7.field_1;
                var_21 = helper_cc_compute_c_wrapper(rax_1 - rdx, rdx, var_6, 17U);
                r9_0 = var_19;
                r8_0 = var_20;
                if (var_21 != 0UL) {
                    loop_state_var = 0U;
                    break;
                }
                var_22 = helper_cc_compute_all_wrapper(rax_1, rdx, var_6, 25U);
                cc_src_0 = var_22;
                if ((uint64_t)(((unsigned char)(var_22 >> 4UL) ^ (unsigned char)var_22) & '\xc0') != 0UL) {
                    loop_state_var = 0U;
                    break;
                }
                var_23 = local_sp_2 + (-24L);
                *(uint64_t *)var_23 = 4215231UL;
                var_24 = indirect_placeholder_15();
                var_25 = var_24.field_0;
                var_26 = var_24.field_1;
                r9_0 = var_25;
                local_sp_1 = var_23;
                r8_0 = var_26;
                loop_state_var = 2U;
                break;
            }
        }
    switch (loop_state_var) {
      case 1U:
        {
            *(uint64_t *)6400664UL = rbx_0;
            return rbx_0;
        }
        break;
      case 0U:
        {
            var_27 = helper_cc_compute_all_wrapper(rax_1, cc_src_0, var_6, 25U);
            if ((uint64_t)(((unsigned char)(var_27 >> 4UL) ^ (unsigned char)var_27) & '\xc0') == 0UL) {
                *(uint64_t *)6400664UL = rbx_0;
                return rbx_0;
            }
            var_28 = helper_cc_compute_all_wrapper((uint64_t)*(unsigned char *)6401192UL, 0UL, var_6, 14U);
            var_29 = *(uint64_t *)6400664UL;
            var_30 = helper_cc_compute_all_wrapper(var_29, var_28, var_6, 25U);
            var_31 = helper_cc_compute_all_wrapper(rdx - var_29, var_29, var_6, 17U);
            if ((var_28 & 64UL) != 0UL & (uint64_t)(((unsigned char)(var_30 >> 4UL) ^ (unsigned char)var_30) & '\xc0') != 0UL & (var_31 & 65UL) != 0UL) {
                var_32 = helper_cc_compute_all_wrapper((uint64_t)*(uint32_t *)6400268UL + (-1L), 1UL, var_6, 16U);
                if ((var_32 & 64UL) == 0UL) {
                    var_33 = helper_cc_compute_all_wrapper(var_29 + (-1L), 1UL, var_6, 17U);
                    var_34 = (var_33 & 64UL) ^ 4286456UL;
                    *(uint64_t *)(local_sp_1 + (-8L)) = 4215295UL;
                    indirect_placeholder_16(0UL, 0UL, r9_0, var_29, 0UL, r8_0, var_34);
                }
                *(unsigned char *)6401192UL = (unsigned char)'\x00';
            }
        }
        break;
      case 2U:
        {
            var_28 = helper_cc_compute_all_wrapper((uint64_t)*(unsigned char *)6401192UL, 0UL, var_6, 14U);
            var_29 = *(uint64_t *)6400664UL;
            var_30 = helper_cc_compute_all_wrapper(var_29, var_28, var_6, 25U);
            var_31 = helper_cc_compute_all_wrapper(rdx - var_29, var_29, var_6, 17U);
            if ((var_28 & 64UL) != 0UL & (uint64_t)(((unsigned char)(var_30 >> 4UL) ^ (unsigned char)var_30) & '\xc0') != 0UL & (var_31 & 65UL) != 0UL) {
                var_32 = helper_cc_compute_all_wrapper((uint64_t)*(uint32_t *)6400268UL + (-1L), 1UL, var_6, 16U);
                if ((var_32 & 64UL) == 0UL) {
                    var_33 = helper_cc_compute_all_wrapper(var_29 + (-1L), 1UL, var_6, 17U);
                    var_34 = (var_33 & 64UL) ^ 4286456UL;
                    *(uint64_t *)(local_sp_1 + (-8L)) = 4215295UL;
                    indirect_placeholder_16(0UL, 0UL, r9_0, var_29, 0UL, r8_0, var_34);
                }
                *(unsigned char *)6401192UL = (unsigned char)'\x00';
            }
        }
        break;
    }
}

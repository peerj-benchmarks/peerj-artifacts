typedef unsigned char __u_char;
typedef unsigned short __u_short;
typedef unsigned int __u_int;
typedef unsigned long __u_long;
typedef signed char __int8_t;
typedef unsigned char __uint8_t;
typedef short __int16_t;
typedef unsigned short __uint16_t;
typedef int __int32_t;
typedef unsigned int __uint32_t;
typedef long __int64_t;
typedef unsigned long __uint64_t;
typedef __int8_t __int_least8_t;
typedef __uint8_t __uint_least8_t;
typedef __int16_t __int_least16_t;
typedef __uint16_t __uint_least16_t;
typedef __int32_t __int_least32_t;
typedef __uint32_t __uint_least32_t;
typedef __int64_t __int_least64_t;
typedef __uint64_t __uint_least64_t;
typedef long __quad_t;
typedef unsigned long __u_quad_t;
typedef long __intmax_t;
typedef unsigned long __uintmax_t;
typedef unsigned long __dev_t;
typedef unsigned int __uid_t;
typedef unsigned int __gid_t;
typedef unsigned long __ino_t;
typedef unsigned long __ino64_t;
typedef unsigned int __mode_t;
typedef unsigned long __nlink_t;
typedef long __off_t;
typedef long __off64_t;
typedef int __pid_t;
typedef struct {
    int __val[2];
} __fsid_t;
typedef long __clock_t;
typedef unsigned long __rlim_t;
typedef unsigned long __rlim64_t;
typedef unsigned int __id_t;
typedef long __time_t;
typedef unsigned int __useconds_t;
typedef long __suseconds_t;
typedef int __daddr_t;
typedef int __key_t;
typedef int __clockid_t;
typedef void *__timer_t;
typedef long __blksize_t;
typedef long __blkcnt_t;
typedef long __blkcnt64_t;
typedef unsigned long __fsblkcnt_t;
typedef unsigned long __fsblkcnt64_t;
typedef unsigned long __fsfilcnt_t;
typedef unsigned long __fsfilcnt64_t;
typedef long __fsword_t;
typedef long __ssize_t;
typedef long __syscall_slong_t;
typedef unsigned long __syscall_ulong_t;
typedef __off64_t __loff_t;
typedef char *__caddr_t;
typedef long __intptr_t;
typedef unsigned int __socklen_t;
typedef int __sig_atomic_t;
typedef __int8_t int8_t;
typedef __int16_t int16_t;
typedef __int32_t int32_t;
typedef __int64_t int64_t;
typedef __uint8_t uint8_t;
typedef __uint16_t uint16_t;
typedef __uint32_t uint32_t;
typedef __uint64_t uint64_t;
typedef __int_least8_t int_least8_t;
typedef __int_least16_t int_least16_t;
typedef __int_least32_t int_least32_t;
typedef __int_least64_t int_least64_t;
typedef __uint_least8_t uint_least8_t;
typedef __uint_least16_t uint_least16_t;
typedef __uint_least32_t uint_least32_t;
typedef __uint_least64_t uint_least64_t;
typedef signed char int_fast8_t;
typedef long int_fast16_t;
typedef long int_fast32_t;
typedef long int_fast64_t;
typedef unsigned char uint_fast8_t;
typedef unsigned long uint_fast16_t;
typedef unsigned long uint_fast32_t;
typedef unsigned long uint_fast64_t;
typedef long intptr_t;
typedef unsigned long uintptr_t;
typedef __intmax_t intmax_t;
typedef __uintmax_t uintmax_t;
typedef _Bool bool;
struct helper_divq_EAX_wrapper_ret_type;
struct type_5;
struct helper_divq_EAX_wrapper_ret_type {
    uint64_t field_0;
    uint64_t field_1;
    uint32_t field_2;
    uint64_t field_3;
    uint64_t field_4;
    uint32_t field_5;
    uint64_t field_6;
    uint32_t field_7;
    uint64_t field_8;
    uint32_t field_9;
    unsigned char field_10;
    uint32_t field_11;
    uint32_t field_12;
};
struct type_5 {
};
extern uint64_t revng_init_local_sp(uint64_t param_0);
extern uint64_t init_rbx(void);
extern uint64_t init_rbp(void);
extern void indirect_placeholder(void);
extern uint64_t init_r13(void);
extern uint64_t init_r12(void);
extern uint64_t init_r14(void);
extern uint64_t init_r15(void);
extern struct helper_divq_EAX_wrapper_ret_type helper_divq_EAX_wrapper(struct type_5 *param_0, uint64_t param_1, uint64_t param_2, uint64_t param_3, uint64_t param_4, uint64_t param_5, uint64_t param_6, uint64_t param_7, uint64_t param_8, uint64_t param_9, uint64_t param_10, uint64_t param_11, uint64_t param_12, uint32_t param_13, uint64_t param_14, uint32_t param_15, uint64_t param_16, uint64_t param_17, uint64_t param_18, uint32_t param_19);
extern uint32_t init_state_0x8248(void);
extern uint64_t init_state_0x9018(void);
extern uint32_t init_state_0x9010(void);
extern uint64_t init_state_0x8408(void);
extern uint64_t init_state_0x8328(void);
extern uint64_t init_state_0x82d8(void);
extern uint32_t init_state_0x9080(void);
uint64_t bb_invalidate_cache(uint64_t r10, uint64_t rdi, uint64_t r9, uint64_t rcx, uint64_t rsi, uint64_t r8) {
    uint64_t var_0;
    uint64_t var_1;
    uint64_t var_2;
    uint64_t var_3;
    uint64_t var_4;
    uint64_t var_5;
    uint64_t var_6;
    uint32_t var_7;
    uint64_t var_8;
    uint32_t var_9;
    uint64_t var_10;
    uint64_t var_11;
    uint64_t var_12;
    uint32_t var_13;
    uint64_t var_14;
    bool var_15;
    unsigned char *storemerge1_in_in;
    uint64_t *storemerge;
    unsigned char storemerge1_in;
    bool var_16;
    uint64_t var_17;
    uint64_t var_18;
    uint64_t var_19;
    unsigned char var_27;
    uint64_t rdx_3;
    uint64_t var_20;
    uint64_t var_22;
    uint64_t rbx_1;
    uint64_t var_21;
    uint64_t rbx_2;
    bool var_23;
    uint64_t var_24;
    uint64_t rbx_0;
    uint64_t r13_2;
    uint64_t r14_0;
    uint64_t rbx_5;
    uint64_t rbx_4;
    uint64_t var_28;
    uint64_t rbx_3;
    uint64_t var_35;
    uint64_t r13_3;
    uint64_t rdx_2;
    uint64_t r14_1;
    uint64_t var_25;
    uint64_t rax_069;
    uint64_t var_26;
    uint64_t r13_4;
    uint64_t r14_2;
    uint64_t rax_0;
    uint64_t r14_268;
    uint64_t r13_466;
    uint64_t rbx_365;
    bool var_29;
    uint64_t var_30;
    uint64_t var_31;
    uint64_t var_32;
    uint64_t rsi5_0;
    struct helper_divq_EAX_wrapper_ret_type var_33;
    uint64_t rax_1;
    uint64_t var_34;
    var_0 = revng_init_local_sp(0UL);
    var_1 = init_r15();
    var_2 = init_r13();
    var_3 = init_rbx();
    var_4 = init_r12();
    var_5 = init_rbp();
    var_6 = init_r14();
    var_7 = init_state_0x8248();
    var_8 = init_state_0x9018();
    var_9 = init_state_0x9010();
    var_10 = init_state_0x8408();
    var_11 = init_state_0x8328();
    var_12 = init_state_0x82d8();
    var_13 = init_state_0x9080();
    *(uint64_t *)(var_0 + (-8L)) = var_1;
    *(uint64_t *)(var_0 + (-16L)) = var_6;
    *(uint64_t *)(var_0 + (-24L)) = var_2;
    *(uint64_t *)(var_0 + (-32L)) = var_4;
    var_14 = (uint64_t)(uint32_t)rdi;
    *(uint64_t *)(var_0 + (-40L)) = var_5;
    *(uint64_t *)(var_0 + (-48L)) = var_3;
    var_15 = (var_14 == 0UL);
    storemerge1_in_in = (unsigned char *)var_15 ? 6400969UL : 6400968UL;
    storemerge = (uint64_t *)var_15 ? 6400648UL : 6400640UL;
    storemerge1_in = *storemerge1_in_in;
    var_16 = (rsi == 0UL);
    var_17 = *storemerge;
    rdx_3 = 1UL;
    rbx_0 = var_17;
    r13_2 = 0UL;
    r14_0 = 0UL;
    r13_3 = 0UL;
    rdx_2 = 1UL;
    r14_1 = 0UL;
    if (var_16) {
        var_23 = (var_17 == 0UL);
        var_24 = (var_2 & (-256L)) | var_23;
        r13_2 = var_24;
        r13_3 = var_24;
        rdx_2 = 0UL;
        if (!var_23) {
            rbx_0 = 0UL;
            if (storemerge1_in == '\x00') {
                return (uint64_t)(uint32_t)rdx_3;
            }
        }
        rbx_1 = rbx_0;
        rbx_2 = rbx_0;
        if (var_15) {
            var_27 = *(unsigned char *)6401212UL;
            var_28 = *(uint64_t *)6401200UL;
            rbx_3 = rbx_1;
            r13_4 = r13_2;
            r14_2 = r14_0;
            rax_0 = var_28;
            if (var_27 == '\x00') {
                *(unsigned char *)(var_0 + (-57L)) = (unsigned char)'\x00';
                *(uint64_t *)(var_0 + (-80L)) = 4212777UL;
                indirect_placeholder();
                var_35 = (uint64_t)*(unsigned char *)(var_0 + (-65L));
                *(uint32_t *)var_28 = 29U;
                rdx_3 = var_35;
                return (uint64_t)(uint32_t)rdx_3;
            }
            rdx_3 = 0UL;
            rax_069 = rax_0;
            r14_268 = r14_2;
            r13_466 = r13_4;
            rbx_365 = rbx_3;
            if ((long)rax_0 < (long)0UL) {
                return (uint64_t)(uint32_t)rdx_3;
            }
        }
        var_25 = *(uint64_t *)6400256UL;
        rdx_3 = 0UL;
        rbx_3 = rbx_2;
        rax_069 = var_25;
        r13_4 = r13_3;
        r14_2 = r14_1;
        r14_268 = r14_1;
        r13_466 = r13_3;
        rbx_365 = rbx_2;
        if (var_25 == 18446744073709551615UL) {
            return (uint64_t)(uint32_t)rdx_3;
        }
        if ((long)var_25 >= (long)0UL) {
            *(uint64_t *)(var_0 + (-80L)) = 4212879UL;
            indirect_placeholder();
            *(uint64_t *)6400256UL = var_25;
            return (uint64_t)(uint32_t)rdx_3;
        }
        if ((uint64_t)(unsigned char)rdx_2 != 0UL) {
            var_26 = var_25 + (rbx_2 + r14_1);
            *(uint64_t *)6400256UL = var_26;
            rax_0 = var_26;
            rdx_3 = 0UL;
            rax_069 = rax_0;
            r14_268 = r14_2;
            r13_466 = r13_4;
            rbx_365 = rbx_3;
            if ((long)rax_0 < (long)0UL) {
                return (uint64_t)(uint32_t)rdx_3;
            }
        }
    }
    var_18 = var_17 + rsi;
    var_19 = (uint64_t)((uint32_t)var_18 & 131071U);
    *storemerge = var_19;
    if (var_18 > 131071UL) {
        return (uint64_t)(uint32_t)rdx_3;
    }
    var_20 = var_18 - var_19;
    rbx_1 = var_20;
    rbx_2 = var_20;
    if (var_20 == 0UL) {
        return (uint64_t)(uint32_t)rdx_3;
    }
    if (var_15) {
        var_22 = *(uint64_t *)6400648UL;
        r14_0 = var_22;
        var_27 = *(unsigned char *)6401212UL;
        var_28 = *(uint64_t *)6401200UL;
        rbx_3 = rbx_1;
        r13_4 = r13_2;
        r14_2 = r14_0;
        rax_0 = var_28;
        if (var_27 == '\x00') {
            *(unsigned char *)(var_0 + (-57L)) = (unsigned char)'\x00';
            *(uint64_t *)(var_0 + (-80L)) = 4212777UL;
            indirect_placeholder();
            var_35 = (uint64_t)*(unsigned char *)(var_0 + (-65L));
            *(uint32_t *)var_28 = 29U;
            rdx_3 = var_35;
            return (uint64_t)(uint32_t)rdx_3;
        }
        rdx_3 = 0UL;
        rax_069 = rax_0;
        r14_268 = r14_2;
        r13_466 = r13_4;
        rbx_365 = rbx_3;
        if ((long)rax_0 < (long)0UL) {
            return (uint64_t)(uint32_t)rdx_3;
        }
        rbx_4 = rbx_365;
        rbx_5 = rbx_365;
        if ((rbx_365 == 0UL) || (var_16 ^ 1)) {
            var_29 = (storemerge1_in == '\x00');
            var_30 = rax_069 - rbx_365;
            rbx_4 = 0UL;
            rsi5_0 = var_30;
            rax_1 = var_30;
            if (var_29) {
                var_33 = helper_divq_EAX_wrapper((struct type_5 *)(0UL), *(uint64_t *)6401368UL, 4212568UL, rsi5_0, rbx_4, r10, rdi, r9, rsi, rcx, rsi5_0, r8, 0UL, var_7, var_8, var_9, var_10, var_11, var_12, var_13);
                rbx_5 = rbx_4;
                rax_1 = var_33.field_1;
            }
        } else {
            var_31 = rax_069 - rbx_365;
            var_32 = var_31 - r14_268;
            rsi5_0 = var_32;
            rax_1 = var_31;
            if ((uint64_t)(unsigned char)r13_466 == 0UL) {
                var_33 = helper_divq_EAX_wrapper((struct type_5 *)(0UL), *(uint64_t *)6401368UL, 4212568UL, rsi5_0, rbx_4, r10, rdi, r9, rsi, rcx, rsi5_0, r8, 0UL, var_7, var_8, var_9, var_10, var_11, var_12, var_13);
                rbx_5 = rbx_4;
                rax_1 = var_33.field_1;
            }
        }
        *(uint64_t *)(var_0 + (-80L)) = 4212594UL;
        indirect_placeholder();
        var_34 = (rbx_5 & (-256L)) | ((uint64_t)((uint32_t)rax_1 + 1U) != 0UL);
        rdx_3 = var_34;
        return (uint64_t)(uint32_t)rdx_3;
    }
    var_21 = *(uint64_t *)6400640UL;
    r14_1 = var_21;
    var_25 = *(uint64_t *)6400256UL;
    rdx_3 = 0UL;
    rbx_3 = rbx_2;
    rax_069 = var_25;
    r13_4 = r13_3;
    r14_2 = r14_1;
    r14_268 = r14_1;
    r13_466 = r13_3;
    rbx_365 = rbx_2;
    if (var_25 == 18446744073709551615UL) {
        return (uint64_t)(uint32_t)rdx_3;
    }
    if ((long)var_25 < (long)0UL) {
        *(uint64_t *)(var_0 + (-80L)) = 4212879UL;
        indirect_placeholder();
        *(uint64_t *)6400256UL = var_25;
        return (uint64_t)(uint32_t)rdx_3;
    }
    if ((uint64_t)(unsigned char)rdx_2 != 0UL) {
        var_26 = var_25 + (rbx_2 + r14_1);
        *(uint64_t *)6400256UL = var_26;
        rax_0 = var_26;
        rdx_3 = 0UL;
        rax_069 = rax_0;
        r14_268 = r14_2;
        r13_466 = r13_4;
        rbx_365 = rbx_3;
        if ((long)rax_0 < (long)0UL) {
            return (uint64_t)(uint32_t)rdx_3;
        }
    }
    rbx_4 = rbx_365;
    rbx_5 = rbx_365;
    if ((rbx_365 == 0UL) || (var_16 ^ 1)) {
        var_31 = rax_069 - rbx_365;
        var_32 = var_31 - r14_268;
        rsi5_0 = var_32;
        rax_1 = var_31;
        if ((uint64_t)(unsigned char)r13_466 == 0UL) {
            var_33 = helper_divq_EAX_wrapper((struct type_5 *)(0UL), *(uint64_t *)6401368UL, 4212568UL, rsi5_0, rbx_4, r10, rdi, r9, rsi, rcx, rsi5_0, r8, 0UL, var_7, var_8, var_9, var_10, var_11, var_12, var_13);
            rbx_5 = rbx_4;
            rax_1 = var_33.field_1;
        }
    } else {
        var_29 = (storemerge1_in == '\x00');
        var_30 = rax_069 - rbx_365;
        rbx_4 = 0UL;
        rsi5_0 = var_30;
        rax_1 = var_30;
        if (var_29) {
            var_33 = helper_divq_EAX_wrapper((struct type_5 *)(0UL), *(uint64_t *)6401368UL, 4212568UL, rsi5_0, rbx_4, r10, rdi, r9, rsi, rcx, rsi5_0, r8, 0UL, var_7, var_8, var_9, var_10, var_11, var_12, var_13);
            rbx_5 = rbx_4;
            rax_1 = var_33.field_1;
        }
    }
    *(uint64_t *)(var_0 + (-80L)) = 4212594UL;
    indirect_placeholder();
    var_34 = (rbx_5 & (-256L)) | ((uint64_t)((uint32_t)rax_1 + 1U) != 0UL);
    rdx_3 = var_34;
    return (uint64_t)(uint32_t)rdx_3;
}

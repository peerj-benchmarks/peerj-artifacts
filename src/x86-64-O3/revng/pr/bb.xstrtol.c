typedef unsigned char __u_char;
typedef unsigned short __u_short;
typedef unsigned int __u_int;
typedef unsigned long __u_long;
typedef signed char __int8_t;
typedef unsigned char __uint8_t;
typedef short __int16_t;
typedef unsigned short __uint16_t;
typedef int __int32_t;
typedef unsigned int __uint32_t;
typedef long __int64_t;
typedef unsigned long __uint64_t;
typedef __int8_t __int_least8_t;
typedef __uint8_t __uint_least8_t;
typedef __int16_t __int_least16_t;
typedef __uint16_t __uint_least16_t;
typedef __int32_t __int_least32_t;
typedef __uint32_t __uint_least32_t;
typedef __int64_t __int_least64_t;
typedef __uint64_t __uint_least64_t;
typedef long __quad_t;
typedef unsigned long __u_quad_t;
typedef long __intmax_t;
typedef unsigned long __uintmax_t;
typedef unsigned long __dev_t;
typedef unsigned int __uid_t;
typedef unsigned int __gid_t;
typedef unsigned long __ino_t;
typedef unsigned long __ino64_t;
typedef unsigned int __mode_t;
typedef unsigned long __nlink_t;
typedef long __off_t;
typedef long __off64_t;
typedef int __pid_t;
typedef struct {
    int __val[2];
} __fsid_t;
typedef long __clock_t;
typedef unsigned long __rlim_t;
typedef unsigned long __rlim64_t;
typedef unsigned int __id_t;
typedef long __time_t;
typedef unsigned int __useconds_t;
typedef long __suseconds_t;
typedef int __daddr_t;
typedef int __key_t;
typedef int __clockid_t;
typedef void *__timer_t;
typedef long __blksize_t;
typedef long __blkcnt_t;
typedef long __blkcnt64_t;
typedef unsigned long __fsblkcnt_t;
typedef unsigned long __fsblkcnt64_t;
typedef unsigned long __fsfilcnt_t;
typedef unsigned long __fsfilcnt64_t;
typedef long __fsword_t;
typedef long __ssize_t;
typedef long __syscall_slong_t;
typedef unsigned long __syscall_ulong_t;
typedef __off64_t __loff_t;
typedef char *__caddr_t;
typedef long __intptr_t;
typedef unsigned int __socklen_t;
typedef int __sig_atomic_t;
typedef __int8_t int8_t;
typedef __int16_t int16_t;
typedef __int32_t int32_t;
typedef __int64_t int64_t;
typedef __uint8_t uint8_t;
typedef __uint16_t uint16_t;
typedef __uint32_t uint32_t;
typedef __uint64_t uint64_t;
typedef __int_least8_t int_least8_t;
typedef __int_least16_t int_least16_t;
typedef __int_least32_t int_least32_t;
typedef __int_least64_t int_least64_t;
typedef __uint_least8_t uint_least8_t;
typedef __uint_least16_t uint_least16_t;
typedef __uint_least32_t uint_least32_t;
typedef __uint_least64_t uint_least64_t;
typedef signed char int_fast8_t;
typedef long int_fast16_t;
typedef long int_fast32_t;
typedef long int_fast64_t;
typedef unsigned char uint_fast8_t;
typedef unsigned long uint_fast16_t;
typedef unsigned long uint_fast32_t;
typedef unsigned long uint_fast64_t;
typedef long intptr_t;
typedef unsigned long uintptr_t;
typedef __intmax_t intmax_t;
typedef __uintmax_t uintmax_t;
extern void function_dispatcher(unsigned char *param_0);
extern uint64_t revng_init_local_sp(uint64_t param_0);
extern uint64_t init_rbx(void);
extern uint64_t init_rbp(void);
extern void indirect_placeholder(void);
extern uint64_t init_cc_src2(void);
extern uint64_t helper_cc_compute_c_wrapper(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint32_t param_3);
extern uint64_t init_r13(void);
extern uint64_t init_r12(void);
extern uint64_t init_r14(void);
extern uint64_t helper_cc_compute_all_wrapper(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint32_t param_3);
uint64_t bb_xstrtol(uint64_t rdi, uint64_t rcx, uint64_t rdx, uint64_t r8, uint64_t rsi) {
    uint64_t var_0;
    uint64_t var_1;
    uint64_t var_2;
    uint64_t var_3;
    uint64_t var_4;
    uint64_t var_5;
    uint64_t var_6;
    uint64_t local_sp_2;
    unsigned char var_26;
    uint64_t rbp_0;
    uint64_t rax_1;
    unsigned char var_18;
    uint64_t var_19;
    uint64_t var_20;
    uint64_t var_21;
    uint64_t var_22;
    uint64_t var_23;
    uint64_t var_24;
    uint64_t var_25;
    unsigned char var_27;
    uint64_t var_28;
    uint64_t local_sp_1;
    uint64_t var_29;
    uint64_t rbp_1;
    uint64_t var_30;
    uint64_t local_sp_0;
    uint64_t r12_1;
    uint64_t r12_0;
    uint64_t var_17;
    uint64_t rbp_2;
    uint64_t var_31;
    uint64_t rax_2;
    uint64_t var_14;
    uint64_t var_15;
    uint64_t var_12;
    uint32_t var_13;
    uint64_t var_16;
    uint64_t var_8;
    uint64_t var_9;
    uint32_t *var_10;
    uint64_t *var_11;
    uint64_t var_7;
    var_0 = revng_init_local_sp(0UL);
    var_1 = init_r13();
    var_2 = init_rbx();
    var_3 = init_r12();
    var_4 = init_rbp();
    var_5 = init_cc_src2();
    var_6 = init_r14();
    *(uint64_t *)(var_0 + (-8L)) = var_6;
    *(uint64_t *)(var_0 + (-16L)) = var_1;
    *(uint64_t *)(var_0 + (-24L)) = var_3;
    *(uint64_t *)(var_0 + (-32L)) = var_4;
    *(uint64_t *)(var_0 + (-40L)) = var_2;
    local_sp_2 = var_0 + (-56L);
    rbp_0 = 0UL;
    rbp_1 = 0UL;
    r12_0 = 1UL;
    rax_2 = 4UL;
    if ((uint64_t)(uint32_t)rdx > 36UL) {
        var_7 = var_0 + (-64L);
        *(uint64_t *)var_7 = 4238033UL;
        indirect_placeholder();
        local_sp_2 = var_7;
    }
    var_8 = local_sp_2 + 8UL;
    var_9 = (rsi == 0UL) ? var_8 : rsi;
    *(uint64_t *)(local_sp_2 + (-8L)) = 4237845UL;
    indirect_placeholder();
    var_10 = (uint32_t *)var_8;
    *var_10 = 0U;
    *(uint64_t *)(local_sp_2 + (-16L)) = 4237865UL;
    indirect_placeholder();
    var_11 = (uint64_t *)var_9;
    r12_1 = var_8;
    if (*var_11 == rdi) {
        if (r8 == 0UL) {
            return rax_2;
        }
        if (*(unsigned char *)rdi == '\x00') {
            return rax_2;
        }
        var_16 = local_sp_2 + (-24L);
        *(uint64_t *)var_16 = 4237998UL;
        indirect_placeholder();
        local_sp_1 = var_16;
        if (var_8 == 0UL) {
            return rax_2;
        }
        var_17 = *var_11;
        r12_1 = r12_0;
        rbp_2 = rbp_1;
        if (*(unsigned char *)var_17 != '\x00') {
            *(uint64_t *)(local_sp_1 + (-8L)) = 4238056UL;
            indirect_placeholder();
            if (var_17 == 0UL) {
                var_29 = (uint64_t)((uint32_t)rbp_1 & (-3));
                *(uint64_t *)rcx = r12_0;
                var_30 = var_29 | 2UL;
                rax_2 = var_30;
                return rax_2;
            }
            var_18 = **(unsigned char **)var_9;
            var_19 = (uint64_t)var_18;
            var_20 = (uint64_t)((uint32_t)var_19 + (-69));
            var_21 = var_20 + (-47L);
            rax_1 = var_19;
            var_22 = 142129060940101UL >> (var_20 & 63UL);
            var_23 = helper_cc_compute_all_wrapper(var_21, 47UL, var_5, 14U);
            var_24 = helper_cc_compute_c_wrapper(var_21, (var_23 & (-2L)) | (var_22 & 1UL), var_5, 1U);
            if ((uint64_t)((unsigned char)var_20 & '\xf0') <= 47UL & var_24 != 0UL) {
                *(uint64_t *)(local_sp_1 + (-16L)) = 4238173UL;
                indirect_placeholder();
                var_25 = *var_11;
                if (var_18 == '\x00') {
                    rax_1 = (uint64_t)*(unsigned char *)var_25;
                } else {
                    var_26 = *(unsigned char *)(var_25 + 1UL);
                    if ((uint64_t)(var_26 + '\xbc') == 0UL) {
                        rax_1 = (uint64_t)*(unsigned char *)var_25;
                    } else {
                        if ((uint64_t)(var_26 + '\x97') == 0UL) {
                            rax_1 = (uint64_t)*(unsigned char *)var_25;
                        } else {
                            if ((uint64_t)(var_26 + '\xbe') == 0UL) {
                                rax_1 = (uint64_t)*(unsigned char *)var_25;
                            } else {
                                rax_1 = (uint64_t)*(unsigned char *)var_25;
                            }
                        }
                    }
                }
            }
            var_27 = (unsigned char)rax_1 + '\xbe';
            var_28 = (uint64_t)var_27;
            if ((uint64_t)(var_27 & '\xfe') > 53UL) {
                function_dispatcher((unsigned char *)(0UL));
                return var_28;
            }
        }
    }
    var_12 = local_sp_2 + (-24L);
    *(uint64_t *)var_12 = 4237878UL;
    indirect_placeholder();
    var_13 = *var_10;
    local_sp_0 = var_12;
    r12_0 = var_8;
    if (var_13 != 0U) {
        var_14 = (uint64_t)var_13;
        var_15 = local_sp_2 + (-32L);
        *(uint64_t *)var_15 = 4237933UL;
        indirect_placeholder();
        rbp_0 = 1UL;
        local_sp_0 = var_15;
        if (*(uint32_t *)var_14 == 34U) {
            return rax_2;
        }
    }
    rbp_1 = rbp_0;
    local_sp_1 = local_sp_0;
    rbp_2 = rbp_0;
    if (r8 == 0UL) {
        *(uint64_t *)rcx = r12_1;
        var_31 = (uint64_t)(uint32_t)rbp_2;
        rax_2 = var_31;
        return rax_2;
    }
    var_17 = *var_11;
    r12_1 = r12_0;
    rbp_2 = rbp_1;
    if (*(unsigned char *)var_17 != '\x00') {
        return;
    }
    *(uint64_t *)(local_sp_1 + (-8L)) = 4238056UL;
    indirect_placeholder();
    if (var_17 == 0UL) {
        var_29 = (uint64_t)((uint32_t)rbp_1 & (-3));
        *(uint64_t *)rcx = r12_0;
        var_30 = var_29 | 2UL;
        rax_2 = var_30;
        return rax_2;
    }
    var_18 = **(unsigned char **)var_9;
    var_19 = (uint64_t)var_18;
    var_20 = (uint64_t)((uint32_t)var_19 + (-69));
    var_21 = var_20 + (-47L);
    rax_1 = var_19;
    var_22 = 142129060940101UL >> (var_20 & 63UL);
    var_23 = helper_cc_compute_all_wrapper(var_21, 47UL, var_5, 14U);
    var_24 = helper_cc_compute_c_wrapper(var_21, (var_23 & (-2L)) | (var_22 & 1UL), var_5, 1U);
    if ((uint64_t)((unsigned char)var_20 & '\xf0') <= 47UL & var_24 != 0UL) {
        *(uint64_t *)(local_sp_1 + (-16L)) = 4238173UL;
        indirect_placeholder();
        var_25 = *var_11;
        if (var_18 == '\x00') {
            rax_1 = (uint64_t)*(unsigned char *)var_25;
        } else {
            var_26 = *(unsigned char *)(var_25 + 1UL);
            if ((uint64_t)(var_26 + '\xbc') == 0UL) {
                rax_1 = (uint64_t)*(unsigned char *)var_25;
            } else {
                if ((uint64_t)(var_26 + '\x97') == 0UL) {
                    rax_1 = (uint64_t)*(unsigned char *)var_25;
                } else {
                    if ((uint64_t)(var_26 + '\xbe') == 0UL) {
                        rax_1 = (uint64_t)*(unsigned char *)var_25;
                    } else {
                        rax_1 = (uint64_t)*(unsigned char *)var_25;
                    }
                }
            }
        }
    }
    var_27 = (unsigned char)rax_1 + '\xbe';
    var_28 = (uint64_t)var_27;
    if ((uint64_t)(var_27 & '\xfe') > 53UL) {
        function_dispatcher((unsigned char *)(0UL));
        return var_28;
    }
}

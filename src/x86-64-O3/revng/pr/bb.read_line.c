typedef unsigned char __u_char;
typedef unsigned short __u_short;
typedef unsigned int __u_int;
typedef unsigned long __u_long;
typedef signed char __int8_t;
typedef unsigned char __uint8_t;
typedef short __int16_t;
typedef unsigned short __uint16_t;
typedef int __int32_t;
typedef unsigned int __uint32_t;
typedef long __int64_t;
typedef unsigned long __uint64_t;
typedef __int8_t __int_least8_t;
typedef __uint8_t __uint_least8_t;
typedef __int16_t __int_least16_t;
typedef __uint16_t __uint_least16_t;
typedef __int32_t __int_least32_t;
typedef __uint32_t __uint_least32_t;
typedef __int64_t __int_least64_t;
typedef __uint64_t __uint_least64_t;
typedef long __quad_t;
typedef unsigned long __u_quad_t;
typedef long __intmax_t;
typedef unsigned long __uintmax_t;
typedef unsigned long __dev_t;
typedef unsigned int __uid_t;
typedef unsigned int __gid_t;
typedef unsigned long __ino_t;
typedef unsigned long __ino64_t;
typedef unsigned int __mode_t;
typedef unsigned long __nlink_t;
typedef long __off_t;
typedef long __off64_t;
typedef int __pid_t;
typedef struct {
    int __val[2];
} __fsid_t;
typedef long __clock_t;
typedef unsigned long __rlim_t;
typedef unsigned long __rlim64_t;
typedef unsigned int __id_t;
typedef long __time_t;
typedef unsigned int __useconds_t;
typedef long __suseconds_t;
typedef int __daddr_t;
typedef int __key_t;
typedef int __clockid_t;
typedef void *__timer_t;
typedef long __blksize_t;
typedef long __blkcnt_t;
typedef long __blkcnt64_t;
typedef unsigned long __fsblkcnt_t;
typedef unsigned long __fsblkcnt64_t;
typedef unsigned long __fsfilcnt_t;
typedef unsigned long __fsfilcnt64_t;
typedef long __fsword_t;
typedef long __ssize_t;
typedef long __syscall_slong_t;
typedef unsigned long __syscall_ulong_t;
typedef __off64_t __loff_t;
typedef char *__caddr_t;
typedef long __intptr_t;
typedef unsigned int __socklen_t;
typedef int __sig_atomic_t;
typedef __int8_t int8_t;
typedef __int16_t int16_t;
typedef __int32_t int32_t;
typedef __int64_t int64_t;
typedef __uint8_t uint8_t;
typedef __uint16_t uint16_t;
typedef __uint32_t uint32_t;
typedef __uint64_t uint64_t;
typedef __int_least8_t int_least8_t;
typedef __int_least16_t int_least16_t;
typedef __int_least32_t int_least32_t;
typedef __int_least64_t int_least64_t;
typedef __uint_least8_t uint_least8_t;
typedef __uint_least16_t uint_least16_t;
typedef __uint_least32_t uint_least32_t;
typedef __uint_least64_t uint_least64_t;
typedef signed char int_fast8_t;
typedef long int_fast16_t;
typedef long int_fast32_t;
typedef long int_fast64_t;
typedef unsigned char uint_fast8_t;
typedef unsigned long uint_fast16_t;
typedef unsigned long uint_fast32_t;
typedef unsigned long uint_fast64_t;
typedef long intptr_t;
typedef unsigned long uintptr_t;
typedef __intmax_t intmax_t;
typedef __uintmax_t uintmax_t;
typedef _Bool bool;
struct bb_read_line_ret_type;
struct indirect_placeholder_46_ret_type;
struct indirect_placeholder_47_ret_type;
struct indirect_placeholder_48_ret_type;
struct indirect_placeholder_49_ret_type;
struct indirect_placeholder_50_ret_type;
struct indirect_placeholder_51_ret_type;
struct indirect_placeholder_52_ret_type;
struct indirect_placeholder_53_ret_type;
struct indirect_placeholder_54_ret_type;
struct bb_read_line_ret_type {
    uint64_t field_0;
    uint64_t field_1;
};
struct indirect_placeholder_46_ret_type {
    uint64_t field_0;
    uint64_t field_1;
    uint64_t field_2;
    uint64_t field_3;
    uint64_t field_4;
};
struct indirect_placeholder_47_ret_type {
    uint64_t field_0;
    uint64_t field_1;
    uint64_t field_2;
    uint64_t field_3;
    uint64_t field_4;
    uint64_t field_5;
};
struct indirect_placeholder_48_ret_type {
    uint64_t field_0;
    uint64_t field_1;
    uint64_t field_2;
    uint64_t field_3;
    uint64_t field_4;
    uint64_t field_5;
};
struct indirect_placeholder_49_ret_type {
    uint64_t field_0;
    uint64_t field_1;
    uint64_t field_2;
    uint64_t field_3;
    uint64_t field_4;
    uint64_t field_5;
};
struct indirect_placeholder_50_ret_type {
    uint64_t field_0;
    uint64_t field_1;
    uint64_t field_2;
    uint64_t field_3;
    uint64_t field_4;
};
struct indirect_placeholder_51_ret_type {
    uint64_t field_0;
    uint64_t field_1;
    uint64_t field_2;
    uint64_t field_3;
    uint64_t field_4;
};
struct indirect_placeholder_52_ret_type {
    uint64_t field_0;
    uint64_t field_1;
    uint64_t field_2;
    uint64_t field_3;
    uint64_t field_4;
};
struct indirect_placeholder_53_ret_type {
    uint64_t field_0;
    uint64_t field_1;
    uint64_t field_2;
    uint64_t field_3;
    uint64_t field_4;
};
struct indirect_placeholder_54_ret_type {
    uint64_t field_0;
    uint64_t field_1;
    uint64_t field_2;
    uint64_t field_3;
    uint64_t field_4;
};
extern uint64_t revng_init_local_sp(uint64_t param_0);
extern uint64_t init_rbx(void);
extern uint64_t init_rbp(void);
extern void indirect_placeholder(void);
extern uint64_t init_r13(void);
extern uint64_t init_r12(void);
extern uint64_t init_r14(void);
extern uint64_t init_rax(void);
extern uint64_t helper_cc_compute_all_wrapper(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint32_t param_3);
extern uint64_t init_r15(void);
extern uint64_t init_r8(void);
extern uint64_t init_r9(void);
extern uint64_t init_r10(void);
extern uint64_t init_rsi(void);
extern struct indirect_placeholder_46_ret_type indirect_placeholder_46(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint64_t param_3, uint64_t param_4);
extern struct indirect_placeholder_47_ret_type indirect_placeholder_47(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint64_t param_3);
extern struct indirect_placeholder_48_ret_type indirect_placeholder_48(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint64_t param_3);
extern struct indirect_placeholder_49_ret_type indirect_placeholder_49(uint64_t param_0, uint64_t param_1);
extern struct indirect_placeholder_50_ret_type indirect_placeholder_50(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint64_t param_3, uint64_t param_4);
extern struct indirect_placeholder_51_ret_type indirect_placeholder_51(uint64_t param_0);
extern struct indirect_placeholder_52_ret_type indirect_placeholder_52(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint64_t param_3, uint64_t param_4);
extern struct indirect_placeholder_53_ret_type indirect_placeholder_53(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint64_t param_3, uint64_t param_4);
extern struct indirect_placeholder_54_ret_type indirect_placeholder_54(uint64_t param_0);
struct bb_read_line_ret_type bb_read_line(uint64_t rdi) {
    struct indirect_placeholder_53_ret_type var_71;
    struct indirect_placeholder_52_ret_type var_54;
    struct indirect_placeholder_50_ret_type var_37;
    struct indirect_placeholder_46_ret_type var_110;
    uint64_t rdi6_3;
    uint64_t var_0;
    uint64_t var_1;
    uint64_t var_2;
    uint64_t var_3;
    uint64_t var_4;
    uint64_t var_5;
    uint64_t var_6;
    uint64_t var_7;
    uint64_t var_8;
    uint64_t var_9;
    uint64_t var_10;
    uint64_t var_11;
    uint64_t *var_12;
    uint64_t var_13;
    uint64_t var_14;
    uint32_t var_15;
    uint64_t var_16;
    uint32_t var_17;
    bool var_18;
    uint32_t var_38;
    uint64_t var_39;
    uint64_t var_23;
    uint64_t r10_8;
    uint64_t rdi6_22;
    uint64_t r8_8;
    uint64_t rsi_10;
    uint32_t var_98;
    uint64_t var_99;
    uint64_t var_100;
    uint64_t rdi6_4;
    uint64_t rax_0;
    uint64_t rdx_0;
    uint64_t var_103;
    uint64_t rax_1;
    uint64_t rdx_1;
    uint32_t var_101;
    uint64_t var_102;
    uint64_t var_24;
    uint64_t local_sp_0;
    uint64_t local_sp_21_be;
    uint64_t local_sp_21;
    uint32_t var_89;
    uint64_t rax_4;
    uint64_t rsi_0;
    uint64_t var_91;
    uint32_t var_92;
    uint64_t var_93;
    uint64_t rbp_0;
    uint64_t var_94;
    uint64_t var_106;
    uint64_t var_107;
    uint64_t var_108;
    uint64_t _pre_phi172;
    uint64_t local_sp_1;
    uint64_t rdi6_1;
    uint64_t rsi_1;
    unsigned char var_109;
    uint64_t rdi6_2;
    unsigned char var_35;
    uint32_t var_111;
    uint64_t var_112;
    uint64_t var_113;
    uint64_t rax_2;
    uint64_t rdx_2;
    uint64_t var_116;
    uint64_t rax_3;
    uint64_t rdx_3;
    uint32_t var_114;
    uint64_t var_115;
    struct bb_read_line_ret_type mrv1 = {1UL, /*implicit*/(int)0};
    uint64_t local_sp_3;
    uint64_t var_82;
    uint32_t var_83;
    uint64_t var_84;
    uint64_t r8_9;
    uint64_t rsi_11;
    struct indirect_placeholder_47_ret_type var_85;
    uint64_t var_86;
    uint64_t var_87;
    uint64_t var_88;
    uint64_t var_97;
    uint64_t local_sp_20;
    unsigned char *_pre_phi180;
    uint64_t _pre_phi176;
    uint64_t local_sp_2;
    uint64_t rdi6_5;
    bool var_25;
    uint64_t local_sp_4;
    uint64_t var_26;
    uint64_t var_27;
    struct indirect_placeholder_48_ret_type var_28;
    uint64_t var_29;
    uint64_t var_30;
    uint64_t var_31;
    uint64_t var_32;
    uint64_t var_33;
    struct indirect_placeholder_49_ret_type var_95;
    uint64_t var_96;
    uint64_t rdi6_8;
    uint64_t r10_0;
    uint64_t rdi6_6;
    uint64_t r9_0;
    uint64_t r8_0;
    uint64_t rsi_2;
    uint64_t rdi6_7;
    uint64_t var_90;
    uint32_t rbp_1_in;
    uint64_t storemerge;
    struct bb_read_line_ret_type mrv2;
    struct bb_read_line_ret_type mrv3;
    uint64_t var_34;
    uint64_t local_sp_18;
    uint64_t rdi6_23;
    uint64_t local_sp_5;
    uint64_t var_36;
    uint64_t r10_1;
    uint64_t rdi6_9;
    uint64_t r9_1;
    uint64_t r8_1;
    uint64_t rsi_3;
    uint64_t var_40;
    uint64_t var_41;
    uint64_t local_sp_12;
    uint64_t r10_4;
    uint64_t local_sp_6;
    uint64_t r13_0;
    uint64_t rdi6_14;
    uint64_t r10_2;
    uint64_t r9_4;
    uint64_t rdi6_10;
    uint64_t r8_4;
    uint64_t r9_2;
    uint64_t r14_0;
    uint64_t rsi_6;
    uint64_t r8_2;
    uint64_t r10_5;
    uint64_t rsi_4;
    uint32_t var_42;
    uint64_t var_43;
    uint64_t var_44;
    uint64_t r10_3;
    uint64_t var_45;
    unsigned char var_46;
    uint32_t var_47;
    uint64_t var_48;
    uint64_t var_49;
    uint64_t local_sp_8;
    uint64_t local_sp_7;
    uint64_t r15_0_in;
    uint64_t var_50;
    uint64_t var_51;
    uint64_t rdi6_11;
    uint64_t local_sp_9;
    uint64_t rdi6_12;
    unsigned char var_52;
    uint64_t local_sp_10;
    uint64_t local_sp_13;
    uint64_t rdi6_13;
    uint64_t r9_3;
    uint64_t r8_3;
    uint64_t rsi_5;
    uint64_t local_sp_11;
    uint64_t var_55;
    struct indirect_placeholder_51_ret_type var_56;
    uint64_t var_57;
    uint64_t rdi6_15;
    uint64_t r9_5;
    uint64_t r8_5;
    uint64_t rsi_7;
    unsigned char var_58;
    uint32_t var_59;
    uint32_t var_60;
    bool var_61;
    uint64_t local_sp_14;
    uint64_t rdi6_16;
    uint64_t var_53;
    uint32_t r13_1_in;
    uint64_t r10_6;
    uint64_t rdi6_17;
    uint64_t r9_6;
    uint64_t r8_6;
    uint64_t rsi_8;
    uint64_t r13_1;
    uint64_t var_62;
    uint64_t local_sp_17;
    uint64_t var_63;
    unsigned char var_64;
    uint32_t var_65;
    uint64_t local_sp_16;
    uint64_t var_66;
    uint64_t local_sp_15;
    uint64_t rdi6_18;
    uint64_t rbp_2;
    uint64_t var_67;
    uint64_t rdi6_19;
    uint64_t rdi6_20;
    uint64_t var_70;
    uint64_t var_68;
    uint64_t var_69;
    uint64_t r10_7;
    uint64_t rdi6_21;
    uint64_t r8_7;
    uint64_t rsi_9;
    uint64_t local_sp_19;
    uint32_t var_74;
    uint64_t var_75;
    uint64_t var_76;
    uint64_t var_77;
    uint64_t local_sp_21_ph;
    uint64_t rbp_3;
    uint64_t var_78;
    uint64_t r10_9;
    uint64_t var_79;
    uint64_t var_80;
    uint32_t var_81;
    struct bb_read_line_ret_type mrv5 = {1UL, /*implicit*/(int)0};
    uint64_t var_72;
    struct indirect_placeholder_54_ret_type var_73;
    unsigned char *var_19;
    uint64_t local_sp_22;
    uint64_t _pre171;
    uint64_t var_104;
    uint64_t var_105;
    uint64_t var_20;
    uint64_t var_21;
    uint64_t var_22;
    unsigned int loop_state_var;
    var_0 = revng_init_local_sp(0UL);
    var_1 = init_rax();
    var_2 = init_r15();
    var_3 = init_r13();
    var_4 = init_rbx();
    var_5 = init_r10();
    var_6 = init_r12();
    var_7 = init_r9();
    var_8 = init_rbp();
    var_9 = init_r14();
    var_10 = init_r8();
    var_11 = init_rsi();
    *(uint64_t *)(var_0 + (-8L)) = var_2;
    *(uint64_t *)(var_0 + (-16L)) = var_9;
    *(uint64_t *)(var_0 + (-24L)) = var_3;
    *(uint64_t *)(var_0 + (-32L)) = var_6;
    *(uint64_t *)(var_0 + (-40L)) = var_8;
    *(uint64_t *)(var_0 + (-48L)) = var_4;
    var_12 = (uint64_t *)rdi;
    var_13 = *var_12;
    var_14 = var_0 + (-80L);
    *(uint64_t *)var_14 = 4209337UL;
    indirect_placeholder();
    var_15 = (uint32_t)var_1;
    var_16 = (uint64_t)(var_15 + (-12));
    var_17 = *(uint32_t *)6422708UL;
    var_18 = (var_16 == 0UL);
    rsi_1 = var_11;
    local_sp_2 = var_14;
    rdi6_5 = var_13;
    r10_0 = var_5;
    r9_0 = var_7;
    r8_0 = var_10;
    rsi_2 = var_11;
    rbp_1_in = var_17;
    storemerge = 1UL;
    r14_0 = 1UL;
    local_sp_22 = var_14;
    if (var_18) {
        _pre_phi180 = (unsigned char *)(rdi + 57UL);
        _pre_phi176 = (uint64_t)(var_15 + (-10));
    } else {
        var_19 = (unsigned char *)(rdi + 57UL);
        _pre_phi180 = var_19;
        if (*var_19 != '\x00') {
            _pre171 = (uint64_t)(var_15 + (-10));
            _pre_phi172 = _pre171;
            var_104 = *var_12;
            var_105 = local_sp_22 + (-8L);
            *(uint64_t *)var_105 = 4209938UL;
            indirect_placeholder();
            local_sp_1 = var_105;
            rdi6_1 = var_104;
            if (_pre_phi172 == 0UL) {
                var_106 = *var_12;
                var_107 = (uint64_t)var_15;
                var_108 = local_sp_22 + (-16L);
                *(uint64_t *)var_108 = 4209953UL;
                indirect_placeholder();
                local_sp_1 = var_108;
                rdi6_1 = var_107;
                rsi_1 = var_106;
            }
            var_109 = *(unsigned char *)6422744UL;
            *(unsigned char *)6422748UL = (unsigned char)'\x01';
            rdi6_2 = rdi6_1;
            if (var_109 == '\x00') {
                if (*(unsigned char *)6422244UL == '\x00') {
                    *(unsigned char *)6422624UL = (unsigned char)'\x01';
                    *(uint64_t *)(local_sp_1 + (-8L)) = 4210668UL;
                    var_110 = indirect_placeholder_46(var_5, rdi6_1, var_7, var_10, rsi_1);
                    rdi6_2 = var_110.field_1;
                } else {
                    if (*(unsigned char *)6422746UL == '\x00') {
                        *(unsigned char *)6422745UL = (unsigned char)'\x01';
                    }
                }
            } else {
                if (*(unsigned char *)6422746UL == '\x00') {
                    *(unsigned char *)6422745UL = (unsigned char)'\x01';
                }
            }
            rdi6_3 = rdi6_2;
            rdi6_4 = rdi6_2;
            if (*(unsigned char *)6422751UL != '\x00') {
                var_111 = *(uint32_t *)6422216UL;
                var_112 = (uint64_t)var_111;
                var_113 = *(uint64_t *)6422792UL;
                rax_2 = var_112;
                rdx_2 = var_113;
                rax_3 = var_112;
                rdx_3 = var_113;
                if (var_111 != 0U) {
                    if (*(unsigned char *)6422244UL != '\x00') {
                        *(uint32_t *)(rdx_2 + 16UL) = 2U;
                        var_116 = (uint64_t)((uint32_t)rax_2 + (-1));
                        rax_2 = var_116;
                        while (var_116 != 0UL)
                            {
                                rdx_2 = rdx_2 + 64UL;
                                *(uint32_t *)(rdx_2 + 16UL) = 2U;
                                var_116 = (uint64_t)((uint32_t)rax_2 + (-1));
                                rax_2 = var_116;
                            }
                    }
                    *(uint32_t *)(rdx_3 + 16UL) = 1U;
                    var_114 = (uint32_t)rax_3 + (-1);
                    while ((uint64_t)var_114 != 0UL)
                        {
                            *(uint32_t *)(rdx_3 + 80UL) = 1U;
                            var_115 = (uint64_t)(var_114 + (-1));
                            rax_3 = var_115;
                            if (var_115 == 0UL) {
                                break;
                            }
                            rdx_3 = rdx_3 + 128UL;
                            *(uint32_t *)(rdx_3 + 16UL) = 1U;
                            var_114 = (uint32_t)rax_3 + (-1);
                        }
                }
            }
            *(uint32_t *)(rdi + 16UL) = 2U;
            rdi6_4 = rdi6_3;
            *(uint32_t *)(rdi + 48UL) = 0U;
            *(uint32_t *)6422688UL = (*(uint32_t *)6422688UL + (-1));
            mrv1.field_1 = rdi6_4;
            return mrv1;
        }
        var_20 = *var_12;
        var_21 = var_0 + (-88L);
        *(uint64_t *)var_21 = 4210264UL;
        indirect_placeholder();
        var_22 = (uint64_t)(var_15 + (-10));
        _pre_phi176 = var_22;
        local_sp_2 = var_21;
        rdi6_5 = var_20;
        if (var_22 == 0UL) {
            var_23 = *var_12;
            var_24 = var_0 + (-96L);
            *(uint64_t *)var_24 = 4210284UL;
            indirect_placeholder();
            _pre_phi176 = 0UL;
            local_sp_2 = var_24;
            rdi6_5 = var_23;
        }
    }
    *_pre_phi180 = (unsigned char)'\x00';
    var_25 = (_pre_phi176 == 0UL);
    _pre_phi172 = _pre_phi176;
    local_sp_3 = local_sp_2;
    local_sp_4 = local_sp_2;
    rdi6_6 = rdi6_5;
    local_sp_22 = local_sp_2;
    if (!var_25) {
        if (!var_18) {
            var_104 = *var_12;
            var_105 = local_sp_22 + (-8L);
            *(uint64_t *)var_105 = 4209938UL;
            indirect_placeholder();
            local_sp_1 = var_105;
            rdi6_1 = var_104;
            if (_pre_phi172 == 0UL) {
                var_106 = *var_12;
                var_107 = (uint64_t)var_15;
                var_108 = local_sp_22 + (-16L);
                *(uint64_t *)var_108 = 4209953UL;
                indirect_placeholder();
                local_sp_1 = var_108;
                rdi6_1 = var_107;
                rsi_1 = var_106;
            }
            var_109 = *(unsigned char *)6422744UL;
            *(unsigned char *)6422748UL = (unsigned char)'\x01';
            rdi6_2 = rdi6_1;
            if (var_109 == '\x00') {
                if (*(unsigned char *)6422244UL == '\x00') {
                    *(unsigned char *)6422624UL = (unsigned char)'\x01';
                    *(uint64_t *)(local_sp_1 + (-8L)) = 4210668UL;
                    var_110 = indirect_placeholder_46(var_5, rdi6_1, var_7, var_10, rsi_1);
                    rdi6_2 = var_110.field_1;
                } else {
                    if (*(unsigned char *)6422746UL == '\x00') {
                        *(unsigned char *)6422745UL = (unsigned char)'\x01';
                    }
                }
            } else {
                if (*(unsigned char *)6422746UL == '\x00') {
                    *(unsigned char *)6422745UL = (unsigned char)'\x01';
                }
            }
            rdi6_3 = rdi6_2;
            rdi6_4 = rdi6_2;
            if (*(unsigned char *)6422751UL != '\x00') {
                var_111 = *(uint32_t *)6422216UL;
                var_112 = (uint64_t)var_111;
                var_113 = *(uint64_t *)6422792UL;
                rax_2 = var_112;
                rdx_2 = var_113;
                rax_3 = var_112;
                rdx_3 = var_113;
                if (var_111 != 0U) {
                    if (*(unsigned char *)6422244UL != '\x00') {
                        *(uint32_t *)(rdx_2 + 16UL) = 2U;
                        var_116 = (uint64_t)((uint32_t)rax_2 + (-1));
                        rax_2 = var_116;
                        while (var_116 != 0UL)
                            {
                                rdx_2 = rdx_2 + 64UL;
                                *(uint32_t *)(rdx_2 + 16UL) = 2U;
                                var_116 = (uint64_t)((uint32_t)rax_2 + (-1));
                                rax_2 = var_116;
                            }
                    }
                    *(uint32_t *)(rdx_3 + 16UL) = 1U;
                    var_114 = (uint32_t)rax_3 + (-1);
                    while ((uint64_t)var_114 != 0UL)
                        {
                            *(uint32_t *)(rdx_3 + 80UL) = 1U;
                            var_115 = (uint64_t)(var_114 + (-1));
                            rax_3 = var_115;
                            if (var_115 == 0UL) {
                                break;
                            }
                            rdx_3 = rdx_3 + 128UL;
                            *(uint32_t *)(rdx_3 + 16UL) = 1U;
                            var_114 = (uint32_t)rax_3 + (-1);
                        }
                }
            }
            *(uint32_t *)(rdi + 16UL) = 2U;
            rdi6_4 = rdi6_3;
            *(uint32_t *)(rdi + 48UL) = 0U;
            *(uint32_t *)6422688UL = (*(uint32_t *)6422688UL + (-1));
            mrv1.field_1 = rdi6_4;
            return mrv1;
        }
        if ((uint64_t)(var_15 + 1U) != 0UL) {
            *(uint64_t *)(local_sp_3 + (-8L)) = 4209899UL;
            var_95 = indirect_placeholder_49(rdi, rdi);
            var_96 = var_95.field_2;
            rdi6_8 = var_96;
            mrv2.field_0 = storemerge;
            mrv3 = mrv2;
            mrv3.field_1 = rdi6_8;
            return mrv3;
        }
        var_26 = (uint64_t)(uint32_t)((int)(var_15 << 24U) >> (int)24U);
        var_27 = local_sp_2 + (-8L);
        *(uint64_t *)var_27 = 4209394UL;
        var_28 = indirect_placeholder_48(var_5, var_26, var_10, var_11);
        var_29 = var_28.field_1;
        var_30 = var_28.field_2;
        var_31 = var_28.field_3;
        var_32 = var_28.field_4;
        var_33 = var_28.field_5;
        *(uint32_t *)(local_sp_2 + 4UL) = (uint32_t)var_28.field_0;
        local_sp_4 = var_27;
        r10_0 = var_29;
        rdi6_6 = var_30;
        r9_0 = var_31;
        r8_0 = var_32;
        rsi_2 = var_33;
    }
    rdi6_7 = rdi6_6;
    storemerge = 0UL;
    local_sp_18 = local_sp_4;
    local_sp_5 = local_sp_4;
    r10_1 = r10_0;
    rdi6_9 = rdi6_6;
    r9_1 = r9_0;
    r8_1 = r8_0;
    rsi_3 = rsi_2;
    r10_7 = r10_0;
    rdi6_21 = rdi6_6;
    r8_7 = r8_0;
    rsi_9 = rsi_2;
    if (*(unsigned char *)6422733UL != '\x00') {
        if ((int)*(uint32_t *)6422728UL >= (int)*(uint32_t *)6422708UL) {
            *(uint32_t *)6422708UL = rbp_1_in;
            rdi6_8 = rdi6_7;
            mrv2.field_0 = storemerge;
            mrv3 = mrv2;
            mrv3.field_1 = rdi6_8;
            return mrv3;
        }
    }
    var_34 = rdi + 32UL;
    if (*(uint64_t *)var_34 != 4204592UL) {
        var_35 = *(unsigned char *)6422744UL;
        *(unsigned char *)6422624UL = (unsigned char)'\x01';
        if (var_35 != '\x00' & *(unsigned char *)6422244UL == '\x00') {
            var_36 = local_sp_4 + (-8L);
            *(uint64_t *)var_36 = 4209455UL;
            var_37 = indirect_placeholder_50(r10_0, rdi6_6, r9_0, r8_0, rsi_2);
            local_sp_5 = var_36;
            r10_1 = var_37.field_0;
            rdi6_9 = var_37.field_1;
            r9_1 = var_37.field_2;
            r8_1 = var_37.field_3;
            rsi_3 = var_37.field_4;
        }
        local_sp_12 = local_sp_5;
        local_sp_6 = local_sp_5;
        r10_2 = r10_1;
        rdi6_10 = rdi6_9;
        r9_2 = r9_1;
        r8_2 = r8_1;
        r10_5 = r10_1;
        rdi6_15 = rdi6_9;
        r9_5 = r9_1;
        r8_5 = r8_1;
        local_sp_14 = local_sp_5;
        r10_6 = r10_1;
        rdi6_17 = rdi6_9;
        r9_6 = r9_1;
        r8_6 = r8_1;
        rsi_8 = rsi_3;
        if (*(unsigned char *)6422751UL == '\x00') {
            r13_1_in = *(uint32_t *)6422628UL;
        } else {
            if (*(unsigned char *)6422750UL == '\x00') {
                r13_1_in = *(uint32_t *)6422628UL;
            } else {
                var_38 = *(uint32_t *)6422632UL;
                var_39 = (uint64_t)var_38;
                var_40 = *(uint64_t *)6422792UL;
                *(uint32_t *)6422632UL = 0U;
                *(uint32_t *)(local_sp_5 + 8UL) = var_38;
                var_41 = helper_cc_compute_all_wrapper(var_39, 0UL, 0UL, 24U);
                rsi_4 = var_40;
                rsi_7 = var_40;
                if ((uint64_t)(((unsigned char)(var_41 >> 4UL) ^ (unsigned char)var_41) & '\xc0') != 0UL) {
                    r13_0 = var_40 + 32UL;
                    while (1U)
                        {
                            var_42 = *(uint32_t *)(r13_0 + 20UL);
                            var_43 = (uint64_t)var_42;
                            var_44 = (uint64_t)*(uint32_t *)6422636UL;
                            *(uint32_t *)6422628UL = var_42;
                            r10_3 = r10_2;
                            local_sp_8 = local_sp_6;
                            local_sp_7 = local_sp_6;
                            rdi6_11 = rdi6_10;
                            local_sp_9 = local_sp_6;
                            rdi6_12 = rdi6_10;
                            local_sp_10 = local_sp_6;
                            local_sp_13 = local_sp_6;
                            rdi6_13 = rdi6_10;
                            r9_3 = r9_2;
                            r8_3 = r8_2;
                            rsi_5 = rsi_4;
                            rdi6_16 = rdi6_10;
                            if ((long)(var_43 << 32UL) > (long)(var_44 << 32UL)) {
                                var_45 = var_43 - var_44;
                                var_46 = *(unsigned char *)6422724UL;
                                var_47 = *(uint32_t *)6422712UL;
                                if (var_46 == '\x00') {
                                    *(uint32_t *)6422720UL = ((uint32_t)var_45 - var_47);
                                } else {
                                    var_48 = (uint64_t)var_47 + 1UL;
                                    var_49 = var_45 << 32UL;
                                    r15_0_in = var_48;
                                    if ((long)var_49 >= (long)(var_48 << 32UL)) {
                                        var_50 = (uint64_t)(uint32_t)r15_0_in + 1UL;
                                        var_51 = local_sp_7 + (-8L);
                                        *(uint64_t *)var_51 = 4209662UL;
                                        indirect_placeholder();
                                        local_sp_7 = var_51;
                                        r15_0_in = var_50;
                                        local_sp_8 = var_51;
                                        rdi6_11 = 32UL;
                                        do {
                                            var_50 = (uint64_t)(uint32_t)r15_0_in + 1UL;
                                            var_51 = local_sp_7 + (-8L);
                                            *(uint64_t *)var_51 = 4209662UL;
                                            indirect_placeholder();
                                            local_sp_7 = var_51;
                                            r15_0_in = var_50;
                                            local_sp_8 = var_51;
                                            rdi6_11 = 32UL;
                                        } while ((long)var_49 >= (long)(var_50 << 32UL));
                                    }
                                    *(uint32_t *)6422712UL = (uint32_t)var_45;
                                    local_sp_9 = local_sp_8;
                                    rdi6_12 = rdi6_11;
                                }
                                var_52 = *(unsigned char *)6422640UL;
                                *(uint32_t *)6422628UL = 0U;
                                local_sp_10 = local_sp_9;
                                rdi6_13 = rdi6_12;
                                local_sp_13 = local_sp_9;
                                rdi6_16 = rdi6_12;
                                if (var_52 == '\x00') {
                                    var_53 = local_sp_13 + (-8L);
                                    *(uint64_t *)var_53 = 4209701UL;
                                    var_54 = indirect_placeholder_52(r10_2, rdi6_16, r9_2, r8_2, rsi_4);
                                    r10_3 = var_54.field_0;
                                    local_sp_10 = var_53;
                                    rdi6_13 = var_54.field_1;
                                    r9_3 = var_54.field_2;
                                    r8_3 = var_54.field_3;
                                    rsi_5 = var_54.field_4;
                                }
                            } else {
                                if (*(unsigned char *)6422640UL == '\x00') {
                                    var_53 = local_sp_13 + (-8L);
                                    *(uint64_t *)var_53 = 4209701UL;
                                    var_54 = indirect_placeholder_52(r10_2, rdi6_16, r9_2, r8_2, rsi_4);
                                    r10_3 = var_54.field_0;
                                    local_sp_10 = var_53;
                                    rdi6_13 = var_54.field_1;
                                    r9_3 = var_54.field_2;
                                    r8_3 = var_54.field_3;
                                    rsi_5 = var_54.field_4;
                                }
                            }
                            r10_4 = r10_3;
                            rdi6_14 = rdi6_13;
                            r9_4 = r9_3;
                            r8_4 = r8_3;
                            rsi_6 = rsi_5;
                            local_sp_11 = local_sp_10;
                            if (*(unsigned char *)(r13_0 + 24UL) == '\x00') {
                                var_55 = local_sp_10 + (-8L);
                                *(uint64_t *)var_55 = 4209564UL;
                                var_56 = indirect_placeholder_51(r13_0);
                                r10_4 = var_56.field_0;
                                rdi6_14 = var_56.field_1;
                                r9_4 = var_56.field_2;
                                r8_4 = var_56.field_3;
                                rsi_6 = var_56.field_4;
                                local_sp_11 = var_55;
                            }
                            *(uint32_t *)6422632UL = (*(uint32_t *)6422632UL + 1U);
                            var_57 = r14_0 + 1UL;
                            local_sp_12 = local_sp_11;
                            local_sp_6 = local_sp_11;
                            r10_2 = r10_4;
                            rdi6_10 = rdi6_14;
                            r9_2 = r9_4;
                            r8_2 = r8_4;
                            r10_5 = r10_4;
                            rsi_4 = rsi_6;
                            rdi6_15 = rdi6_14;
                            r9_5 = r9_4;
                            r8_5 = r8_4;
                            rsi_7 = rsi_6;
                            if ((long)(uint64_t)((long)(var_57 << 32UL) >> (long)32UL) > (long)(uint64_t)*(uint32_t *)(local_sp_11 + 8UL)) {
                                break;
                            }
                            r13_0 = r13_0 + 64UL;
                            r14_0 = (uint64_t)(uint32_t)var_57;
                            continue;
                        }
                }
                var_58 = *(unsigned char *)6422733UL;
                var_59 = *(uint32_t *)(rdi + 52UL);
                var_60 = *(uint32_t *)6422728UL;
                var_61 = (var_58 == '\x00');
                *(unsigned char *)6422750UL = (unsigned char)'\x00';
                *(uint32_t *)6422628UL = var_59;
                *(uint32_t *)6422720UL = (var_61 ? 0U : var_60);
                local_sp_14 = local_sp_12;
                r13_1_in = var_59;
                r10_6 = r10_5;
                rdi6_17 = rdi6_15;
                r9_6 = r9_5;
                r8_6 = r8_5;
                rsi_8 = rsi_7;
            }
        }
        r13_1 = (uint64_t)r13_1_in;
        var_62 = (uint64_t)*(uint32_t *)6422636UL;
        local_sp_17 = local_sp_14;
        local_sp_16 = local_sp_14;
        local_sp_15 = local_sp_14;
        rdi6_18 = rdi6_17;
        rdi6_19 = rdi6_17;
        rdi6_20 = rdi6_17;
        r10_7 = r10_6;
        r8_7 = r8_6;
        rsi_9 = rsi_8;
        if ((long)(var_62 << 32UL) >= (long)(r13_1 << 32UL)) {
            var_63 = r13_1 - var_62;
            var_64 = *(unsigned char *)6422724UL;
            var_65 = *(uint32_t *)6422712UL;
            if (var_64 == '\x00') {
                *(uint32_t *)6422720UL = ((uint32_t)var_63 - var_65);
            } else {
                var_66 = var_63 << 32UL;
                rbp_2 = (uint64_t)var_65;
                var_67 = rbp_2 + 1UL;
                rdi6_18 = 32UL;
                local_sp_16 = local_sp_15;
                rdi6_19 = rdi6_18;
                while ((long)var_66 >= (long)(var_67 << 32UL))
                    {
                        var_68 = (uint64_t)(uint32_t)var_67;
                        var_69 = local_sp_15 + (-8L);
                        *(uint64_t *)var_69 = 4210506UL;
                        indirect_placeholder();
                        local_sp_15 = var_69;
                        rbp_2 = var_68;
                        var_67 = rbp_2 + 1UL;
                        rdi6_18 = 32UL;
                        local_sp_16 = local_sp_15;
                        rdi6_19 = rdi6_18;
                    }
                *(uint32_t *)6422712UL = (uint32_t)var_63;
            }
            *(uint32_t *)6422628UL = 0U;
            local_sp_17 = local_sp_16;
            rdi6_20 = rdi6_19;
        }
        local_sp_18 = local_sp_17;
        rdi6_21 = rdi6_20;
        if (*(unsigned char *)6422640UL == '\x00') {
            var_70 = local_sp_17 + (-8L);
            *(uint64_t *)var_70 = 4210461UL;
            var_71 = indirect_placeholder_53(r10_6, rdi6_20, r9_6, r8_6, rsi_8);
            local_sp_18 = var_70;
            r10_7 = var_71.field_0;
            rdi6_21 = var_71.field_1;
            r8_7 = var_71.field_3;
            rsi_9 = var_71.field_4;
        }
    }
    r10_8 = r10_7;
    rdi6_22 = rdi6_21;
    r8_8 = r8_7;
    rsi_10 = rsi_9;
    local_sp_19 = local_sp_18;
    if (*(unsigned char *)(rdi + 56UL) == '\x00') {
        var_72 = local_sp_18 + (-8L);
        *(uint64_t *)var_72 = 4210329UL;
        var_73 = indirect_placeholder_54(var_34);
        r10_8 = var_73.field_0;
        rdi6_22 = var_73.field_1;
        r8_8 = var_73.field_3;
        rsi_10 = var_73.field_4;
        local_sp_19 = var_72;
    }
    *(unsigned char *)6422749UL = (unsigned char)'\x00';
    r8_9 = r8_8;
    rsi_11 = rsi_10;
    local_sp_20 = local_sp_19;
    rdi6_23 = rdi6_22;
    local_sp_21_ph = local_sp_19;
    r10_9 = r10_8;
    if (var_25) {
        mrv5.field_1 = rdi6_23;
        return mrv5;
    }
    var_74 = *(uint32_t *)(local_sp_19 + 12UL);
    var_75 = *(uint64_t *)6422568UL;
    var_76 = (uint64_t)(var_74 + (-1));
    var_77 = var_75 + var_76;
    rbp_3 = var_75;
    rax_4 = var_76;
    if (var_74 == 0U) {
        var_78 = local_sp_20 + (-8L);
        *(uint64_t *)var_78 = 4209851UL;
        indirect_placeholder();
        local_sp_20 = var_78;
        local_sp_21_ph = var_78;
        while (rbp_3 != var_77)
            {
                rbp_3 = rbp_3 + 1UL;
                var_78 = local_sp_20 + (-8L);
                *(uint64_t *)var_78 = 4209851UL;
                indirect_placeholder();
                local_sp_20 = var_78;
                local_sp_21_ph = var_78;
            }
    }
    local_sp_21 = local_sp_21_ph;
    while (1U)
        {
            var_79 = *var_12;
            var_80 = local_sp_21 + (-8L);
            *(uint64_t *)var_80 = 4209864UL;
            indirect_placeholder();
            var_81 = (uint32_t)rax_4;
            local_sp_3 = var_80;
            rdi6_23 = var_79;
            if ((uint64_t)(var_81 + (-10)) != 0UL) {
                loop_state_var = 0U;
                break;
            }
            if ((uint64_t)(var_81 + (-12)) != 0UL) {
                *(uint64_t *)(local_sp_21 + (-16L)) = 4210344UL;
                indirect_placeholder();
                var_97 = (uint64_t)var_81;
                *(uint64_t *)(local_sp_21 + (-24L)) = 4210359UL;
                indirect_placeholder();
                rdi6_3 = var_97;
                rdi6_4 = var_97;
                if (*(unsigned char *)6422746UL != '\x00') {
                    loop_state_var = 2U;
                    break;
                }
                *(unsigned char *)6422745UL = (unsigned char)'\x01';
                loop_state_var = 2U;
                break;
            }
            if ((uint64_t)(var_81 + 1U) != 0UL) {
                loop_state_var = 3U;
                break;
            }
            var_82 = (uint64_t)(uint32_t)((int)(var_81 << 24U) >> (int)24U);
            var_83 = *(uint32_t *)6422708UL;
            var_84 = local_sp_21 + (-16L);
            *(uint64_t *)var_84 = 4210086UL;
            var_85 = indirect_placeholder_47(r10_9, var_82, r8_9, rsi_11);
            var_86 = var_85.field_0;
            var_87 = var_85.field_1;
            var_88 = var_85.field_4;
            local_sp_0 = var_84;
            local_sp_21_be = var_84;
            rax_4 = var_86;
            rsi_0 = var_85.field_5;
            r8_9 = var_88;
            rbp_1_in = var_83;
            r10_9 = var_87;
            if (*(unsigned char *)6422733UL != '\x00') {
                var_89 = *(uint32_t *)6422728UL;
                rsi_0 = (uint64_t)var_89;
                if ((int)var_89 >= (int)*(uint32_t *)6422708UL) {
                    var_90 = var_85.field_2;
                    rdi6_7 = var_90;
                    loop_state_var = 1U;
                    break;
                }
            }
            var_91 = *(uint64_t *)6422568UL;
            var_92 = (uint32_t)var_86;
            var_93 = (uint64_t)(var_92 + (-1)) + var_91;
            rbp_0 = var_91;
            rsi_11 = rsi_0;
            if ((uint64_t)var_92 == 0UL) {
                var_94 = local_sp_0 + (-8L);
                *(uint64_t *)var_94 = 4210155UL;
                indirect_placeholder();
                local_sp_21_be = var_94;
                local_sp_0 = var_94;
                while (rbp_0 != var_93)
                    {
                        rbp_0 = rbp_0 + 1UL;
                        var_94 = local_sp_0 + (-8L);
                        *(uint64_t *)var_94 = 4210155UL;
                        indirect_placeholder();
                        local_sp_21_be = var_94;
                        local_sp_0 = var_94;
                    }
            }
            local_sp_21 = local_sp_21_be;
            continue;
        }
    switch (loop_state_var) {
      case 0U:
        {
            mrv5.field_1 = rdi6_23;
            return mrv5;
        }
        break;
      case 2U:
        {
            if (*(unsigned char *)6422751UL != '\x00') {
                var_98 = *(uint32_t *)6422216UL;
                var_99 = (uint64_t)var_98;
                var_100 = *(uint64_t *)6422792UL;
                rax_0 = var_99;
                rdx_0 = var_100;
                rax_1 = var_99;
                rdx_1 = var_100;
                if (var_98 != 0U) {
                    if (*(unsigned char *)6422244UL != '\x00') {
                        *(uint32_t *)(rdx_0 + 16UL) = 2U;
                        var_103 = (uint64_t)((uint32_t)rax_0 + (-1));
                        rax_0 = var_103;
                        while (var_103 != 0UL)
                            {
                                rdx_0 = rdx_0 + 64UL;
                                *(uint32_t *)(rdx_0 + 16UL) = 2U;
                                var_103 = (uint64_t)((uint32_t)rax_0 + (-1));
                                rax_0 = var_103;
                            }
                    }
                    *(uint32_t *)(rdx_1 + 16UL) = 1U;
                    var_101 = (uint32_t)rax_1 + (-1);
                    while ((uint64_t)var_101 != 0UL)
                        {
                            *(uint32_t *)(rdx_1 + 80UL) = 1U;
                            var_102 = (uint64_t)(var_101 + (-1));
                            rax_1 = var_102;
                            if (var_102 == 0UL) {
                                break;
                            }
                            rdx_1 = rdx_1 + 128UL;
                            *(uint32_t *)(rdx_1 + 16UL) = 1U;
                            var_101 = (uint32_t)rax_1 + (-1);
                        }
                }
            }
            *(uint32_t *)(rdi + 16UL) = 2U;
            rdi6_4 = rdi6_3;
            *(uint32_t *)(rdi + 48UL) = 0U;
            *(uint32_t *)6422688UL = (*(uint32_t *)6422688UL + (-1));
            mrv1.field_1 = rdi6_4;
            return mrv1;
        }
        break;
      case 3U:
      case 1U:
        {
            switch (loop_state_var) {
              case 3U:
                {
                    *(uint64_t *)(local_sp_3 + (-8L)) = 4209899UL;
                    var_95 = indirect_placeholder_49(rdi, rdi);
                    var_96 = var_95.field_2;
                    rdi6_8 = var_96;
                    mrv2.field_0 = storemerge;
                    mrv3 = mrv2;
                    mrv3.field_1 = rdi6_8;
                    return mrv3;
                }
                break;
              case 1U:
                {
                    *(uint32_t *)6422708UL = rbp_1_in;
                    rdi6_8 = rdi6_7;
                    mrv2.field_0 = storemerge;
                    mrv3 = mrv2;
                    mrv3.field_1 = rdi6_8;
                    return mrv3;
                }
                break;
            }
        }
        break;
    }
}

typedef unsigned char __u_char;
typedef unsigned short __u_short;
typedef unsigned int __u_int;
typedef unsigned long __u_long;
typedef signed char __int8_t;
typedef unsigned char __uint8_t;
typedef short __int16_t;
typedef unsigned short __uint16_t;
typedef int __int32_t;
typedef unsigned int __uint32_t;
typedef long __int64_t;
typedef unsigned long __uint64_t;
typedef __int8_t __int_least8_t;
typedef __uint8_t __uint_least8_t;
typedef __int16_t __int_least16_t;
typedef __uint16_t __uint_least16_t;
typedef __int32_t __int_least32_t;
typedef __uint32_t __uint_least32_t;
typedef __int64_t __int_least64_t;
typedef __uint64_t __uint_least64_t;
typedef long __quad_t;
typedef unsigned long __u_quad_t;
typedef long __intmax_t;
typedef unsigned long __uintmax_t;
typedef unsigned long __dev_t;
typedef unsigned int __uid_t;
typedef unsigned int __gid_t;
typedef unsigned long __ino_t;
typedef unsigned long __ino64_t;
typedef unsigned int __mode_t;
typedef unsigned long __nlink_t;
typedef long __off_t;
typedef long __off64_t;
typedef int __pid_t;
typedef struct {
    int __val[2];
} __fsid_t;
typedef long __clock_t;
typedef unsigned long __rlim_t;
typedef unsigned long __rlim64_t;
typedef unsigned int __id_t;
typedef long __time_t;
typedef unsigned int __useconds_t;
typedef long __suseconds_t;
typedef int __daddr_t;
typedef int __key_t;
typedef int __clockid_t;
typedef void *__timer_t;
typedef long __blksize_t;
typedef long __blkcnt_t;
typedef long __blkcnt64_t;
typedef unsigned long __fsblkcnt_t;
typedef unsigned long __fsblkcnt64_t;
typedef unsigned long __fsfilcnt_t;
typedef unsigned long __fsfilcnt64_t;
typedef long __fsword_t;
typedef long __ssize_t;
typedef long __syscall_slong_t;
typedef unsigned long __syscall_ulong_t;
typedef __off64_t __loff_t;
typedef char *__caddr_t;
typedef long __intptr_t;
typedef unsigned int __socklen_t;
typedef int __sig_atomic_t;
typedef __int8_t int8_t;
typedef __int16_t int16_t;
typedef __int32_t int32_t;
typedef __int64_t int64_t;
typedef __uint8_t uint8_t;
typedef __uint16_t uint16_t;
typedef __uint32_t uint32_t;
typedef __uint64_t uint64_t;
typedef __int_least8_t int_least8_t;
typedef __int_least16_t int_least16_t;
typedef __int_least32_t int_least32_t;
typedef __int_least64_t int_least64_t;
typedef __uint_least8_t uint_least8_t;
typedef __uint_least16_t uint_least16_t;
typedef __uint_least32_t uint_least32_t;
typedef __uint_least64_t uint_least64_t;
typedef signed char int_fast8_t;
typedef long int_fast16_t;
typedef long int_fast32_t;
typedef long int_fast64_t;
typedef unsigned char uint_fast8_t;
typedef unsigned long uint_fast16_t;
typedef unsigned long uint_fast32_t;
typedef unsigned long uint_fast64_t;
typedef long intptr_t;
typedef unsigned long uintptr_t;
typedef __intmax_t intmax_t;
typedef __uintmax_t uintmax_t;
typedef _Bool bool;
struct indirect_placeholder_60_ret_type;
struct indirect_placeholder_65_ret_type;
struct indirect_placeholder_66_ret_type;
struct indirect_placeholder_67_ret_type;
struct indirect_placeholder_60_ret_type {
    uint64_t field_0;
    uint64_t field_1;
    uint64_t field_2;
};
struct indirect_placeholder_65_ret_type {
    uint64_t field_0;
    uint64_t field_1;
    uint64_t field_2;
    uint64_t field_3;
    uint64_t field_4;
};
struct indirect_placeholder_66_ret_type {
    uint64_t field_0;
    uint64_t field_1;
    uint64_t field_2;
};
struct indirect_placeholder_67_ret_type {
    uint64_t field_0;
    uint64_t field_1;
    uint64_t field_2;
};
extern void abort(void);
extern uint64_t revng_init_local_sp(uint64_t param_0);
extern uint64_t init_rbx(void);
extern uint64_t init_rbp(void);
extern void indirect_placeholder(void);
extern uint64_t init_r13(void);
extern uint64_t init_r12(void);
extern uint64_t init_r14(void);
extern uint64_t init_r15(void);
extern struct indirect_placeholder_60_ret_type indirect_placeholder_60(uint64_t param_0);
extern struct indirect_placeholder_65_ret_type indirect_placeholder_65(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint64_t param_3, uint64_t param_4, uint64_t param_5, uint64_t param_6);
extern void indirect_placeholder_61(uint64_t param_0, uint64_t param_1, uint64_t param_2);
extern struct indirect_placeholder_66_ret_type indirect_placeholder_66(uint64_t param_0);
extern struct indirect_placeholder_67_ret_type indirect_placeholder_67(uint64_t param_0);
void bb_apply_settings(uint64_t rdi, uint64_t r9, uint64_t rcx, uint64_t rdx, uint64_t r8, uint64_t rsi) {
    uint64_t var_0;
    uint64_t var_1;
    uint64_t var_2;
    uint64_t var_3;
    uint64_t var_4;
    uint64_t var_5;
    uint64_t var_6;
    uint32_t *var_7;
    uint32_t var_8;
    uint64_t var_49;
    uint64_t r13_0;
    uint64_t local_sp_4;
    uint64_t rbp_6;
    uint64_t r15_0;
    uint64_t rbp_0;
    uint64_t rbp_1;
    uint64_t var_50;
    uint64_t rax_1;
    uint64_t var_51;
    uint64_t r12_1;
    uint64_t var_38;
    uint64_t local_sp_12;
    uint64_t r13_3;
    uint32_t var_39;
    uint64_t var_40;
    uint64_t rbp_3;
    uint64_t var_41;
    uint64_t var_42;
    uint64_t rbp_8;
    uint64_t local_sp_24;
    uint64_t r92_0;
    uint64_t var_43;
    uint64_t var_44;
    uint64_t var_45;
    uint64_t var_46;
    uint32_t *var_47;
    uint64_t var_48;
    uint64_t local_sp_5;
    uint64_t var_52;
    struct indirect_placeholder_60_ret_type var_53;
    uint64_t r13_2;
    uint64_t local_sp_10;
    uint64_t r14_0;
    uint64_t var_34;
    uint64_t rax_4;
    uint64_t var_35;
    uint32_t *var_36;
    uint64_t var_37;
    bool var_77;
    uint32_t *var_78;
    uint32_t var_79;
    uint64_t var_76;
    uint64_t local_sp_26;
    uint64_t local_sp_25;
    uint64_t rbp_4;
    uint64_t rcx3_0;
    uint64_t rdx4_0;
    uint64_t local_sp_16;
    uint64_t r85_0;
    uint64_t rbx_5;
    uint64_t local_sp_22;
    uint64_t r13_5;
    uint64_t var_60;
    uint64_t r12_3;
    uint64_t rbp_7;
    uint64_t r15_1;
    uint64_t var_20;
    uint64_t _pre_phi533;
    uint32_t var_54;
    uint32_t var_55;
    uint64_t var_56;
    uint64_t var_57;
    bool var_58;
    uint64_t var_59;
    uint64_t rbp_5;
    uint64_t var_61;
    struct indirect_placeholder_66_ret_type var_62;
    uint64_t var_33;
    uint64_t local_sp_29;
    uint64_t var_28;
    uint64_t var_29;
    uint64_t var_30;
    uint64_t var_31;
    uint64_t r14_1;
    uint32_t _pre_phi534;
    uint64_t rbx_6;
    uint64_t rax_6;
    uint64_t var_32;
    unsigned char var_63;
    uint32_t var_64;
    uint64_t var_65;
    uint64_t rcx3_1;
    uint64_t rax_8;
    uint64_t var_66;
    uint64_t rax_7;
    uint64_t var_67;
    uint64_t var_68;
    uint32_t var_71;
    uint32_t *var_72;
    uint32_t var_69;
    uint32_t *var_70;
    uint64_t var_75;
    uint64_t var_73;
    uint64_t var_74;
    uint64_t var_23;
    uint64_t var_24;
    uint64_t rbp_9;
    uint64_t local_sp_27;
    uint64_t var_25;
    uint64_t var_26;
    uint64_t var_27;
    uint64_t var_80;
    uint64_t var_81;
    struct indirect_placeholder_67_ret_type var_82;
    uint32_t _pre_phi;
    bool var_21;
    uint64_t var_22;
    uint64_t rax_9;
    uint64_t var_9;
    uint64_t r12_2;
    uint64_t local_sp_31;
    uint64_t var_10;
    uint64_t var_11;
    uint64_t var_12;
    bool var_13;
    uint64_t var_14;
    uint64_t *var_15;
    uint32_t var_16;
    uint64_t var_17;
    uint64_t local_sp_32;
    uint32_t var_18;
    uint64_t var_19;
    uint64_t var_83;
    unsigned int loop_state_var;
    bool switch_state_var;
    var_0 = revng_init_local_sp(0UL);
    var_1 = init_r15();
    var_2 = init_r13();
    var_3 = init_rbx();
    var_4 = init_r12();
    var_5 = init_rbp();
    var_6 = init_r14();
    *(uint64_t *)(var_0 + (-8L)) = var_1;
    *(uint64_t *)(var_0 + (-16L)) = var_6;
    *(uint64_t *)(var_0 + (-24L)) = var_2;
    *(uint64_t *)(var_0 + (-32L)) = var_4;
    *(uint64_t *)(var_0 + (-40L)) = var_5;
    *(uint64_t *)(var_0 + (-48L)) = var_3;
    *(uint64_t *)(var_0 + (-96L)) = rsi;
    *(uint64_t *)(var_0 + (-144L)) = rdx;
    var_7 = (uint32_t *)(var_0 + (-124L));
    var_8 = (uint32_t)rcx;
    *var_7 = var_8;
    *(uint64_t *)(var_0 + (-120L)) = r8;
    *(uint64_t *)(var_0 + (-104L)) = r9;
    *(unsigned char *)(var_0 + (-105L)) = (unsigned char)rdi;
    r15_0 = 58UL;
    r13_3 = 0UL;
    r13_2 = 0UL;
    r14_0 = 4271360UL;
    rdx4_0 = 4270149UL;
    rbx_5 = 0UL;
    r13_5 = 4271384UL;
    rbp_7 = 0UL;
    r15_1 = 4272472UL;
    r14_1 = 4272992UL;
    rbp_9 = 0UL;
    r12_2 = 1UL;
    if ((int)var_8 > (int)1U) {
        return;
    }
    var_9 = var_0 + (-152L);
    *(uint32_t *)(var_0 + (-112L)) = (var_8 + (-1));
    local_sp_31 = var_9;
    while (1U)
        {
            var_10 = (uint64_t)((long)(r12_2 << 32UL) >> (long)29UL);
            *(uint64_t *)(local_sp_31 + 16UL) = var_10;
            var_11 = *(uint64_t *)(local_sp_31 + 8UL);
            var_12 = *(uint64_t *)(var_10 + var_11);
            r12_1 = r12_2;
            r12_3 = r12_2;
            rbx_6 = var_12;
            rax_6 = var_11;
            rax_9 = var_11;
            local_sp_32 = local_sp_31;
            if (var_12 == 0UL) {
                var_83 = r12_3 + 1UL;
                local_sp_31 = local_sp_32;
                if ((long)(uint64_t)((long)(var_83 << 32UL) >> (long)32UL) >= (long)(uint64_t)*(uint32_t *)(local_sp_32 + 28UL)) {
                    loop_state_var = 0U;
                    break;
                }
                r12_2 = (uint64_t)(uint32_t)var_83;
                continue;
            }
            var_13 = (*(unsigned char *)var_12 == '-');
            var_14 = local_sp_31 + (-8L);
            var_15 = (uint64_t *)var_14;
            local_sp_29 = var_14;
            local_sp_32 = var_14;
            if (var_13) {
                *var_15 = 4207809UL;
                indirect_placeholder();
                var_18 = (uint32_t)var_11;
                var_19 = (uint64_t)var_18;
                _pre_phi533 = var_19;
                _pre_phi = var_18;
                if (var_19 != 0UL) {
                    *(uint32_t *)6394912UL = (uint32_t)rax_9;
                    var_83 = r12_3 + 1UL;
                    local_sp_31 = local_sp_32;
                    if ((long)(uint64_t)((long)(var_83 << 32UL) >> (long)32UL) >= (long)(uint64_t)*(uint32_t *)(local_sp_32 + 28UL)) {
                        loop_state_var = 0U;
                        break;
                    }
                    r12_2 = (uint64_t)(uint32_t)var_83;
                    continue;
                }
                var_20 = var_12 + 1UL;
                *(unsigned char *)(local_sp_31 + 38UL) = (unsigned char)'\x01';
                rbx_6 = var_20;
            } else {
                *var_15 = 4207584UL;
                indirect_placeholder();
                var_16 = (uint32_t)var_11;
                var_17 = (uint64_t)var_16;
                _pre_phi533 = var_17;
                _pre_phi = var_16;
                rax_9 = 1UL;
                if (var_17 != 0UL) {
                    *(uint32_t *)6394912UL = (uint32_t)rax_9;
                    var_83 = r12_3 + 1UL;
                    local_sp_31 = local_sp_32;
                    if ((long)(uint64_t)((long)(var_83 << 32UL) >> (long)32UL) >= (long)(uint64_t)*(uint32_t *)(local_sp_32 + 28UL)) {
                        loop_state_var = 0U;
                        break;
                    }
                    r12_2 = (uint64_t)(uint32_t)var_83;
                    continue;
                }
                *(unsigned char *)(local_sp_31 + 38UL) = (unsigned char)'\x00';
            }
            var_21 = (_pre_phi533 == 0UL);
            rbp_3 = rbx_6;
            _pre_phi534 = _pre_phi;
            while (1U)
                {
                    var_22 = local_sp_29 + (-8L);
                    *(uint64_t *)var_22 = 4207647UL;
                    indirect_placeholder();
                    local_sp_24 = var_22;
                    local_sp_26 = var_22;
                    local_sp_25 = var_22;
                    local_sp_29 = var_22;
                    local_sp_27 = var_22;
                    if (!var_21) {
                        loop_state_var = 1U;
                        break;
                    }
                    var_23 = *(uint64_t *)r14_1;
                    var_24 = (uint64_t)((uint32_t)rbp_9 + 1U);
                    rbp_8 = var_24;
                    rbp_9 = var_24;
                    if (var_23 != 0UL) {
                        loop_state_var = 0U;
                        break;
                    }
                    r14_1 = r14_1 + 32UL;
                    continue;
                }
            switch_state_var = 0;
            switch (loop_state_var) {
              case 0U:
                {
                    if (*(unsigned char *)(local_sp_29 + 38UL) != '\x00') {
                        loop_state_var = 2U;
                        switch_state_var = 1;
                        break;
                    }
                    var_25 = local_sp_27 + (-8L);
                    *(uint64_t *)var_25 = 4207884UL;
                    indirect_placeholder();
                    var_26 = *(uint64_t *)r15_1;
                    var_27 = (uint64_t)((uint32_t)rbp_7 + 1U);
                    rbp_6 = var_27;
                    local_sp_24 = var_25;
                    rbp_7 = var_27;
                    local_sp_27 = var_25;
                    while (var_26 != 0UL)
                        {
                            r15_1 = r15_1 + 24UL;
                            var_25 = local_sp_27 + (-8L);
                            *(uint64_t *)var_25 = 4207884UL;
                            indirect_placeholder();
                            var_26 = *(uint64_t *)r15_1;
                            var_27 = (uint64_t)((uint32_t)rbp_7 + 1U);
                            rbp_6 = var_27;
                            local_sp_24 = var_25;
                            rbp_7 = var_27;
                            local_sp_27 = var_25;
                        }
                }
                break;
              case 1U:
                {
                    var_28 = rbp_9 << 32UL;
                    var_29 = (uint64_t)((long)var_28 >> (long)32UL);
                    var_30 = (uint64_t)((long)var_28 >> (long)27UL);
                    var_31 = (uint64_t)*(unsigned char *)(var_30 + 4272972UL);
                    rax_6 = var_31;
                    rbp_6 = var_29;
                    rbp_8 = var_29;
                    if ((var_31 & 16UL) != 0UL) {
                        var_63 = *(unsigned char *)(local_sp_29 + 38UL);
                        if (!((var_63 != '\x00') && ((var_31 & 4UL) == 0UL))) {
                            **(unsigned char **)(local_sp_29 + 152UL) = (unsigned char)'\x01';
                            loop_state_var = 2U;
                            switch_state_var = 1;
                            break;
                        }
                        var_64 = *(uint32_t *)(var_30 + 4272968UL);
                        if (var_64 <= 4U) {
                            var_65 = (uint64_t)var_64;
                            rax_7 = var_65;
                            rax_8 = var_65;
                            switch_state_var = 0;
                            switch (*(uint64_t *)((var_65 << 3UL) + 4271144UL)) {
                              case 4208744UL:
                                {
                                    break;
                                }
                                break;
                              case 4208544UL:
                              case 4208537UL:
                              case 4208526UL:
                              case 4208463UL:
                                {
                                    switch (*(uint64_t *)((var_65 << 3UL) + 4271144UL)) {
                                      case 4208544UL:
                                        {
                                            var_66 = *(uint64_t *)(local_sp_29 + 24UL);
                                            rax_7 = var_66;
                                            rcx3_1 = var_66 + 8UL;
                                        }
                                        break;
                                      case 4208537UL:
                                        {
                                            rcx3_1 = *(uint64_t *)(local_sp_29 + 24UL);
                                        }
                                        break;
                                      case 4208463UL:
                                        {
                                            var_68 = *(uint64_t *)(local_sp_29 + 24UL);
                                            rax_7 = var_68;
                                            rcx3_1 = var_68 + 12UL;
                                        }
                                        break;
                                      case 4208526UL:
                                        {
                                            var_67 = *(uint64_t *)(local_sp_29 + 24UL);
                                            rax_7 = var_67;
                                            rcx3_1 = var_67 + 4UL;
                                        }
                                        break;
                                    }
                                    rax_8 = rax_7;
                                    if (rcx3_1 != 0UL) {
                                        if (var_63 == '\x00') {
                                            var_71 = *(uint32_t *)(var_30 + 4272984UL) ^ (-1);
                                            var_72 = (uint32_t *)rcx3_1;
                                            *var_72 = ((*var_72 & var_71) | *(uint32_t *)(var_30 + 4272976UL));
                                        } else {
                                            var_69 = (*(uint32_t *)(var_30 + 4272976UL) | *(uint32_t *)(var_30 + 4272984UL)) ^ (-1);
                                            var_70 = (uint32_t *)rcx3_1;
                                            *var_70 = (*var_70 & var_69);
                                        }
                                        **(unsigned char **)(local_sp_25 + 160UL) = (unsigned char)'\x01';
                                        r12_3 = r12_1;
                                        local_sp_32 = local_sp_25;
                                        var_83 = r12_3 + 1UL;
                                        local_sp_31 = local_sp_32;
                                        if ((long)(uint64_t)((long)(var_83 << 32UL) >> (long)32UL) >= (long)(uint64_t)*(uint32_t *)(local_sp_32 + 28UL)) {
                                            loop_state_var = 0U;
                                            switch_state_var = 1;
                                            break;
                                        }
                                        r12_2 = (uint64_t)(uint32_t)var_83;
                                        continue;
                                    }
                                }
                                break;
                              default:
                                {
                                    abort();
                                }
                                break;
                            }
                            if (switch_state_var)
                                break;
                        }
                        var_73 = var_30 + 4272960UL;
                        var_74 = local_sp_29 + (-16L);
                        *(uint64_t *)var_74 = 4208744UL;
                        indirect_placeholder();
                        rax_8 = var_73;
                        local_sp_26 = var_74;
                    }
                    _pre_phi534 = (uint32_t)var_31;
                }
                break;
            }
            if (switch_state_var)
                break;
            var_32 = local_sp_24 + (-8L);
            *(uint64_t *)var_32 = 4207689UL;
            indirect_placeholder();
            rax_4 = rax_6;
            rdx4_0 = 4270169UL;
            local_sp_22 = var_32;
            rbp_5 = rbp_6;
            local_sp_32 = var_32;
            if ((uint64_t)_pre_phi534 == 0UL) {
                var_54 = *(uint32_t *)(local_sp_24 + 32UL);
                var_55 = (uint32_t)r12_2;
                if ((uint64_t)(var_54 - var_55) != 0UL) {
                    loop_state_var = 3U;
                    break;
                }
                var_56 = *(uint64_t *)local_sp_24;
                rbp_5 = 0UL;
                if (*(uint64_t *)((var_10 + var_56) + 8UL) != 0UL) {
                    loop_state_var = 3U;
                    break;
                }
                var_57 = (uint64_t)(var_55 + 1U);
                r12_3 = var_57;
                if (*(unsigned char *)(local_sp_24 + 39UL) != '\x00') {
                    var_58 = ((uint64_t)(uint32_t)var_56 == 0UL);
                    var_59 = local_sp_22 + (-8L);
                    *(uint64_t *)var_59 = 4208412UL;
                    indirect_placeholder();
                    local_sp_22 = var_59;
                    while (!var_58)
                        {
                            if (*(uint64_t *)r13_5 == 0UL) {
                                break;
                            }
                            r13_5 = r13_5 + 24UL;
                            rbx_5 = (uint64_t)((uint32_t)rbx_5 + 1U);
                            var_59 = local_sp_22 + (-8L);
                            *(uint64_t *)var_59 = 4208412UL;
                            indirect_placeholder();
                            local_sp_22 = var_59;
                        }
                    var_60 = local_sp_22 + (-16L);
                    *(uint64_t *)var_60 = 4208439UL;
                    indirect_placeholder();
                    **(unsigned char **)(local_sp_22 + 32UL) = (unsigned char)'\x01';
                    **(unsigned char **)(local_sp_22 + 144UL) = (unsigned char)'\x01';
                    local_sp_32 = var_60;
                }
            } else {
                *(uint64_t *)(local_sp_24 + (-16L)) = 4208213UL;
                indirect_placeholder();
                *(uint64_t *)(local_sp_24 + (-24L)) = 4208634UL;
                indirect_placeholder();
                *(uint64_t *)(local_sp_24 + (-32L)) = 4208825UL;
                indirect_placeholder();
                *(uint64_t *)(local_sp_24 + (-40L)) = 4209013UL;
                indirect_placeholder();
                *(uint64_t *)(local_sp_24 + (-48L)) = 4209034UL;
                indirect_placeholder();
                *(uint64_t *)(local_sp_24 + (-56L)) = 4209187UL;
                indirect_placeholder();
                var_33 = local_sp_24 + (-64L);
                *(uint64_t *)var_33 = 4209395UL;
                indirect_placeholder();
                local_sp_10 = var_33;
                local_sp_12 = local_sp_10;
                while (*(uint64_t *)r14_0 != 0UL)
                    {
                        var_34 = local_sp_10 + (-8L);
                        *(uint64_t *)var_34 = 4209438UL;
                        indirect_placeholder();
                        r13_2 = (uint64_t)((uint32_t)r13_2 + 1U);
                        local_sp_10 = var_34;
                        r14_0 = r14_0 + 24UL;
                        local_sp_12 = local_sp_10;
                    }
                while (1U)
                    {
                        var_35 = local_sp_12 + (-8L);
                        *(uint64_t *)var_35 = 4209725UL;
                        indirect_placeholder();
                        var_36 = (uint32_t *)rax_4;
                        *var_36 = 0U;
                        *(uint64_t *)(local_sp_12 + (-16L)) = 4209749UL;
                        indirect_placeholder();
                        var_37 = local_sp_12 + (-24L);
                        *(uint64_t *)var_37 = 4209757UL;
                        indirect_placeholder();
                        local_sp_4 = var_37;
                        rbp_1 = rbp_3;
                        local_sp_5 = var_37;
                        local_sp_12 = var_37;
                        if (*var_36 != 0U) {
                            loop_state_var = 1U;
                            break;
                        }
                        var_38 = *(uint64_t *)(local_sp_12 + 48UL);
                        if (var_38 != rbp_3) {
                            loop_state_var = 1U;
                            break;
                        }
                        if (*(unsigned char *)var_38 != ':') {
                            loop_state_var = 1U;
                            break;
                        }
                        var_39 = (uint32_t)rax_4;
                        var_40 = (uint64_t)var_39;
                        rax_4 = var_40;
                        if (rax_4 >= 4294967296UL) {
                            loop_state_var = 1U;
                            break;
                        }
                        *(uint32_t *)(((r13_3 << 2UL) + var_37) + 80UL) = var_39;
                        var_41 = r13_3 + 1UL;
                        var_42 = var_38 + 1UL;
                        rbp_0 = var_42;
                        r13_3 = var_41;
                        rbp_3 = var_42;
                        if (r13_3 == 3UL) {
                            continue;
                        }
                        loop_state_var = 0U;
                        break;
                    }
                switch_state_var = 0;
                switch (loop_state_var) {
                  case 0U:
                    {
                        var_43 = *(uint64_t *)(local_sp_12 + 8UL);
                        *(uint32_t *)var_35 = (uint32_t)r12_2;
                        var_44 = var_41 & (-256L);
                        var_45 = *(uint64_t *)(local_sp_12 + 64UL);
                        *(uint64_t *)var_43 = *(uint64_t *)(local_sp_12 + 56UL);
                        *(uint64_t *)(var_43 + 8UL) = var_45;
                        rax_1 = var_43;
                        r13_0 = var_44;
                        while (1U)
                            {
                                var_46 = local_sp_4 + (-8L);
                                *(uint64_t *)var_46 = 4209862UL;
                                indirect_placeholder();
                                var_47 = (uint32_t *)rax_1;
                                *var_47 = 0U;
                                *(uint64_t *)(local_sp_4 + (-16L)) = 4209886UL;
                                indirect_placeholder();
                                var_48 = local_sp_4 + (-24L);
                                *(uint64_t *)var_48 = 4209894UL;
                                indirect_placeholder();
                                local_sp_4 = var_48;
                                rbp_1 = rbp_0;
                                local_sp_5 = var_48;
                                local_sp_25 = var_48;
                                if (*var_47 != 0U) {
                                    loop_state_var = 0U;
                                    break;
                                }
                                var_49 = *(uint64_t *)(local_sp_4 + 48UL);
                                if (!(((((uint64_t)((unsigned char)r15_0 - *(unsigned char *)var_49) != 0UL) || (var_49 == rbp_0)) ^ 1) && (rax_1 < 256UL))) {
                                    loop_state_var = 0U;
                                    break;
                                }
                                var_50 = *(uint64_t *)(local_sp_4 + 8UL);
                                *(unsigned char *)((r13_0 + var_50) + 17UL) = (unsigned char)rax_1;
                                rax_1 = var_50;
                                if (r13_0 != 31UL) {
                                    loop_state_var = 1U;
                                    break;
                                }
                                r15_0 = (r13_0 == 30UL) ? 0UL : 58UL;
                                r13_0 = r13_0 + 1UL;
                                rbp_0 = var_49 + 1UL;
                                continue;
                            }
                        switch_state_var = 0;
                        switch (loop_state_var) {
                          case 1U:
                            {
                                var_51 = (uint64_t)*(uint32_t *)var_46;
                                r12_1 = var_51;
                                **(unsigned char **)(local_sp_25 + 160UL) = (unsigned char)'\x01';
                                r12_3 = r12_1;
                                local_sp_32 = local_sp_25;
                            }
                            break;
                          case 0U:
                            {
                                loop_state_var = 1U;
                                switch_state_var = 1;
                                break;
                            }
                            break;
                        }
                        if (switch_state_var)
                            break;
                    }
                    break;
                  case 1U:
                    {
                        loop_state_var = 1U;
                        switch_state_var = 1;
                        break;
                    }
                    break;
                }
                if (switch_state_var)
                    break;
            }
        }
    switch (loop_state_var) {
      case 0U:
        {
            return;
        }
        break;
      case 3U:
      case 2U:
      case 1U:
        {
            switch (loop_state_var) {
              case 1U:
                {
                    var_52 = local_sp_5 + (-8L);
                    *(uint64_t *)var_52 = 4210012UL;
                    var_53 = indirect_placeholder_60(rbx_6);
                    r92_0 = var_53.field_1;
                    rbp_4 = rbp_1;
                    rcx3_0 = var_53.field_0;
                    local_sp_16 = var_52;
                    r85_0 = var_53.field_2;
                }
                break;
              case 2U:
                {
                    var_80 = rbx_6 + (-1L);
                    var_81 = local_sp_29 + (-16L);
                    *(uint64_t *)var_81 = 4208600UL;
                    var_82 = indirect_placeholder_67(var_80);
                    r92_0 = var_82.field_1;
                    rbp_4 = rbp_8;
                    rcx3_0 = var_82.field_0;
                    local_sp_16 = var_81;
                    r85_0 = var_82.field_2;
                }
                break;
              case 3U:
                {
                    var_61 = var_32 + (-8L);
                    *(uint64_t *)var_61 = 4209476UL;
                    var_62 = indirect_placeholder_66(rbx_6);
                    r92_0 = var_62.field_1;
                    rbp_4 = rbp_5;
                    rcx3_0 = var_62.field_0;
                    local_sp_16 = var_61;
                    r85_0 = var_62.field_2;
                }
                break;
            }
            *(uint64_t *)(local_sp_16 + (-8L)) = 4209495UL;
            indirect_placeholder_65(0UL, 0UL, r92_0, rcx3_0, rdx4_0, r85_0, 0UL);
            *(uint64_t *)(local_sp_16 + (-16L)) = 4209505UL;
            indirect_placeholder_61(rbx_6, 1UL, rbp_4);
            abort();
        }
        break;
    }
}

typedef unsigned char __u_char;
typedef unsigned short __u_short;
typedef unsigned int __u_int;
typedef unsigned long __u_long;
typedef signed char __int8_t;
typedef unsigned char __uint8_t;
typedef short __int16_t;
typedef unsigned short __uint16_t;
typedef int __int32_t;
typedef unsigned int __uint32_t;
typedef long __int64_t;
typedef unsigned long __uint64_t;
typedef __int8_t __int_least8_t;
typedef __uint8_t __uint_least8_t;
typedef __int16_t __int_least16_t;
typedef __uint16_t __uint_least16_t;
typedef __int32_t __int_least32_t;
typedef __uint32_t __uint_least32_t;
typedef __int64_t __int_least64_t;
typedef __uint64_t __uint_least64_t;
typedef long __quad_t;
typedef unsigned long __u_quad_t;
typedef long __intmax_t;
typedef unsigned long __uintmax_t;
typedef unsigned long __dev_t;
typedef unsigned int __uid_t;
typedef unsigned int __gid_t;
typedef unsigned long __ino_t;
typedef unsigned long __ino64_t;
typedef unsigned int __mode_t;
typedef unsigned long __nlink_t;
typedef long __off_t;
typedef long __off64_t;
typedef int __pid_t;
typedef struct {
    int __val[2];
} __fsid_t;
typedef long __clock_t;
typedef unsigned long __rlim_t;
typedef unsigned long __rlim64_t;
typedef unsigned int __id_t;
typedef long __time_t;
typedef unsigned int __useconds_t;
typedef long __suseconds_t;
typedef int __daddr_t;
typedef int __key_t;
typedef int __clockid_t;
typedef void *__timer_t;
typedef long __blksize_t;
typedef long __blkcnt_t;
typedef long __blkcnt64_t;
typedef unsigned long __fsblkcnt_t;
typedef unsigned long __fsblkcnt64_t;
typedef unsigned long __fsfilcnt_t;
typedef unsigned long __fsfilcnt64_t;
typedef long __fsword_t;
typedef long __ssize_t;
typedef long __syscall_slong_t;
typedef unsigned long __syscall_ulong_t;
typedef __off64_t __loff_t;
typedef char *__caddr_t;
typedef long __intptr_t;
typedef unsigned int __socklen_t;
typedef int __sig_atomic_t;
typedef __int8_t int8_t;
typedef __int16_t int16_t;
typedef __int32_t int32_t;
typedef __int64_t int64_t;
typedef __uint8_t uint8_t;
typedef __uint16_t uint16_t;
typedef __uint32_t uint32_t;
typedef __uint64_t uint64_t;
typedef __int_least8_t int_least8_t;
typedef __int_least16_t int_least16_t;
typedef __int_least32_t int_least32_t;
typedef __int_least64_t int_least64_t;
typedef __uint_least8_t uint_least8_t;
typedef __uint_least16_t uint_least16_t;
typedef __uint_least32_t uint_least32_t;
typedef __uint_least64_t uint_least64_t;
typedef signed char int_fast8_t;
typedef long int_fast16_t;
typedef long int_fast32_t;
typedef long int_fast64_t;
typedef unsigned char uint_fast8_t;
typedef unsigned long uint_fast16_t;
typedef unsigned long uint_fast32_t;
typedef unsigned long uint_fast64_t;
typedef long intptr_t;
typedef unsigned long uintptr_t;
typedef __intmax_t intmax_t;
typedef __uintmax_t uintmax_t;
typedef _Bool bool;
extern uint64_t revng_init_local_sp(uint64_t param_0);
extern void indirect_placeholder(void);
void bb_usage(uint64_t rbx, uint64_t rdi, uint64_t rbp) {
    uint64_t var_0;
    uint64_t var_19;
    uint64_t local_sp_2;
    uint64_t var_20;
    uint64_t local_sp_6;
    uint64_t var_18;
    uint64_t local_sp_7;
    uint64_t *var_2;
    uint64_t *var_3;
    uint64_t *var_4;
    uint64_t *var_5;
    uint64_t *var_6;
    uint64_t *var_7;
    uint64_t *var_8;
    uint64_t *var_9;
    uint64_t *var_10;
    uint64_t *var_11;
    uint64_t *var_12;
    uint64_t *var_13;
    uint64_t *var_14;
    uint64_t *var_15;
    bool var_16;
    uint64_t *var_17;
    uint64_t var_21;
    uint64_t var_1;
    var_0 = revng_init_local_sp(0UL);
    *(uint64_t *)(var_0 + (-8L)) = rbp;
    *(uint64_t *)(var_0 + (-16L)) = rbx;
    if ((uint64_t)(uint32_t)rdi == 0UL) {
        local_sp_7 = var_0 + (-136L);
    } else {
        var_1 = var_0 + (-144L);
        *(uint64_t *)var_1 = 4205926UL;
        indirect_placeholder();
        local_sp_6 = var_1;
        var_21 = local_sp_6 + (-8L);
        *(uint64_t *)var_21 = 4205933UL;
        indirect_placeholder();
        local_sp_7 = var_21;
    }
    while (1U)
        {
            *(uint64_t *)(local_sp_7 + (-8L)) = 4205958UL;
            indirect_placeholder();
            *(uint64_t *)(local_sp_7 + (-16L)) = 4205978UL;
            indirect_placeholder();
            *(uint64_t *)(local_sp_7 + (-24L)) = 4205991UL;
            indirect_placeholder();
            *(uint64_t *)(local_sp_7 + (-32L)) = 4206004UL;
            indirect_placeholder();
            *(uint64_t *)(local_sp_7 + (-40L)) = 4206017UL;
            indirect_placeholder();
            *(uint64_t *)(local_sp_7 + (-48L)) = 4206030UL;
            indirect_placeholder();
            *(uint64_t *)(local_sp_7 + (-56L)) = 4206043UL;
            indirect_placeholder();
            *(uint64_t *)(local_sp_7 + (-64L)) = 4206056UL;
            indirect_placeholder();
            *(uint64_t *)(local_sp_7 + (-72L)) = 4206069UL;
            indirect_placeholder();
            *(uint64_t *)(local_sp_7 + (-80L)) = 4206082UL;
            indirect_placeholder();
            *(uint64_t *)(local_sp_7 + (-88L)) = 4206095UL;
            indirect_placeholder();
            *(uint64_t *)(local_sp_7 + (-96L)) = 4206108UL;
            indirect_placeholder();
            *(uint64_t *)(local_sp_7 + (-104L)) = 4206121UL;
            indirect_placeholder();
            *(uint64_t *)(local_sp_7 + (-112L)) = 4206134UL;
            indirect_placeholder();
            *(uint64_t *)(local_sp_7 + (-120L)) = 4206147UL;
            indirect_placeholder();
            *(uint64_t *)(local_sp_7 + (-128L)) = 4206160UL;
            indirect_placeholder();
            *(uint64_t *)(local_sp_7 + (-136L)) = 4206173UL;
            indirect_placeholder();
            *(uint64_t *)(local_sp_7 + (-144L)) = 4206186UL;
            indirect_placeholder();
            *(uint64_t *)(local_sp_7 + (-152L)) = 4206199UL;
            indirect_placeholder();
            *(uint64_t *)(local_sp_7 + (-160L)) = 4206212UL;
            indirect_placeholder();
            *(uint64_t *)(local_sp_7 + (-168L)) = 4206245UL;
            indirect_placeholder();
            *(uint64_t *)(local_sp_7 + (-176L)) = 4206258UL;
            indirect_placeholder();
            *(uint64_t *)(local_sp_7 + (-184L)) = 4206271UL;
            indirect_placeholder();
            *(uint64_t *)(local_sp_7 + (-192L)) = 4206284UL;
            indirect_placeholder();
            *(uint64_t *)(local_sp_7 + (-200L)) = 4206297UL;
            indirect_placeholder();
            *(uint64_t *)(local_sp_7 + (-208L)) = 4206310UL;
            indirect_placeholder();
            *(uint64_t *)(local_sp_7 + (-216L)) = 4206323UL;
            indirect_placeholder();
            *(uint64_t *)(local_sp_7 + (-224L)) = 4206336UL;
            indirect_placeholder();
            *(uint64_t *)(local_sp_7 + (-232L)) = 4206349UL;
            indirect_placeholder();
            *(uint64_t *)(local_sp_7 + (-240L)) = 4206362UL;
            indirect_placeholder();
            *(uint64_t *)(local_sp_7 + (-248L)) = 4206375UL;
            indirect_placeholder();
            *(uint64_t *)(local_sp_7 + (-256L)) = 4206388UL;
            indirect_placeholder();
            *(uint64_t *)(local_sp_7 + (-264L)) = 4206401UL;
            indirect_placeholder();
            *(uint64_t *)(local_sp_7 + (-272L)) = 4206414UL;
            indirect_placeholder();
            *(uint64_t *)(local_sp_7 + (-280L)) = 4206427UL;
            indirect_placeholder();
            *(uint64_t *)(local_sp_7 + (-288L)) = 4206440UL;
            indirect_placeholder();
            *(uint64_t *)(local_sp_7 + (-296L)) = 4206453UL;
            indirect_placeholder();
            *(uint64_t *)(local_sp_7 + (-304L)) = 4206466UL;
            indirect_placeholder();
            *(uint64_t *)(local_sp_7 + (-312L)) = 4206479UL;
            indirect_placeholder();
            *(uint64_t *)(local_sp_7 + (-320L)) = 4206492UL;
            indirect_placeholder();
            *(uint64_t *)(local_sp_7 + (-328L)) = 4206505UL;
            indirect_placeholder();
            *(uint64_t *)(local_sp_7 + (-336L)) = 4206518UL;
            indirect_placeholder();
            *(uint64_t *)(local_sp_7 + (-344L)) = 4206531UL;
            indirect_placeholder();
            *(uint64_t *)(local_sp_7 + (-352L)) = 4206544UL;
            indirect_placeholder();
            *(uint64_t *)(local_sp_7 + (-360L)) = 4206557UL;
            indirect_placeholder();
            *(uint64_t *)(local_sp_7 + (-368L)) = 4206570UL;
            indirect_placeholder();
            *(uint64_t *)(local_sp_7 + (-376L)) = 4206583UL;
            indirect_placeholder();
            *(uint64_t *)(local_sp_7 + (-384L)) = 4206596UL;
            indirect_placeholder();
            *(uint64_t *)(local_sp_7 + (-392L)) = 4206609UL;
            indirect_placeholder();
            *(uint64_t *)(local_sp_7 + (-400L)) = 4206622UL;
            indirect_placeholder();
            *(uint64_t *)(local_sp_7 + (-408L)) = 4206635UL;
            indirect_placeholder();
            *(uint64_t *)(local_sp_7 + (-416L)) = 4206648UL;
            indirect_placeholder();
            *(uint64_t *)(local_sp_7 + (-424L)) = 4206661UL;
            indirect_placeholder();
            *(uint64_t *)(local_sp_7 + (-432L)) = 4206674UL;
            indirect_placeholder();
            *(uint64_t *)(local_sp_7 + (-440L)) = 4206687UL;
            indirect_placeholder();
            *(uint64_t *)(local_sp_7 + (-448L)) = 4206700UL;
            indirect_placeholder();
            *(uint64_t *)(local_sp_7 + (-456L)) = 4206713UL;
            indirect_placeholder();
            *(uint64_t *)(local_sp_7 + (-464L)) = 4206726UL;
            indirect_placeholder();
            *(uint64_t *)(local_sp_7 + (-472L)) = 4206739UL;
            indirect_placeholder();
            *(uint64_t *)(local_sp_7 + (-480L)) = 4206752UL;
            indirect_placeholder();
            *(uint64_t *)(local_sp_7 + (-488L)) = 4206765UL;
            indirect_placeholder();
            *(uint64_t *)(local_sp_7 + (-496L)) = 4206778UL;
            indirect_placeholder();
            *(uint64_t *)(local_sp_7 + (-504L)) = 4206791UL;
            indirect_placeholder();
            *(uint64_t *)(local_sp_7 + (-512L)) = 4206808UL;
            indirect_placeholder();
            *(uint64_t *)(local_sp_7 + (-520L)) = 4206821UL;
            indirect_placeholder();
            *(uint64_t *)(local_sp_7 + (-528L)) = 4206834UL;
            indirect_placeholder();
            var_2 = (uint64_t *)(local_sp_7 + (-536L));
            *var_2 = 4206847UL;
            indirect_placeholder();
            var_3 = (uint64_t *)(local_sp_7 + (-544L));
            *var_3 = 4206860UL;
            indirect_placeholder();
            var_4 = (uint64_t *)(local_sp_7 + (-552L));
            *var_4 = 4206873UL;
            indirect_placeholder();
            var_5 = (uint64_t *)(local_sp_7 + (-560L));
            *var_5 = 4206886UL;
            indirect_placeholder();
            var_6 = (uint64_t *)(local_sp_7 + (-568L));
            *var_6 = 4206903UL;
            indirect_placeholder();
            var_7 = (uint64_t *)(local_sp_7 + (-576L));
            *var_7 = 4206920UL;
            indirect_placeholder();
            var_8 = (uint64_t *)(local_sp_7 + (-584L));
            *var_8 = 4206933UL;
            indirect_placeholder();
            var_9 = (uint64_t *)(local_sp_7 + (-592L));
            *var_9 = 4206946UL;
            indirect_placeholder();
            var_10 = (uint64_t *)(local_sp_7 + (-600L));
            *var_10 = 4206959UL;
            indirect_placeholder();
            var_11 = (uint64_t *)(local_sp_7 + (-608L));
            *var_11 = 4206981UL;
            indirect_placeholder();
            var_12 = (uint64_t *)(local_sp_7 + (-616L));
            *var_12 = 4206994UL;
            indirect_placeholder();
            var_13 = (uint64_t *)(local_sp_7 + (-624L));
            *var_13 = 4207011UL;
            indirect_placeholder();
            var_14 = (uint64_t *)(local_sp_7 + (-632L));
            *var_14 = 4207038UL;
            indirect_placeholder();
            var_15 = (uint64_t *)(local_sp_7 + (-640L));
            *var_15 = 4207054UL;
            indirect_placeholder();
            *var_15 = 4269693UL;
            *var_14 = 4269908UL;
            *var_13 = 4270005UL;
            *var_12 = 4269924UL;
            *var_11 = 4269946UL;
            *var_10 = 4269956UL;
            *var_9 = 4269971UL;
            *var_8 = 4269956UL;
            *var_7 = 4269981UL;
            *var_6 = 4269956UL;
            *var_5 = 4269991UL;
            *var_4 = 4269956UL;
            *var_3 = 0UL;
            *var_2 = 0UL;
            *(uint64_t *)(local_sp_7 + (-648L)) = 4207214UL;
            indirect_placeholder();
            var_16 = (*var_13 == 0UL);
            var_17 = (uint64_t *)(local_sp_7 + (-656L));
            if (var_16) {
                *var_17 = 4207349UL;
                indirect_placeholder();
                *(uint64_t *)(local_sp_7 + (-664L)) = 4207361UL;
                indirect_placeholder();
                var_19 = local_sp_7 + (-672L);
                *(uint64_t *)var_19 = 4207434UL;
                indirect_placeholder();
                local_sp_2 = var_19;
            } else {
                *var_17 = 4207249UL;
                indirect_placeholder();
                *(uint64_t *)(local_sp_7 + (-664L)) = 4207261UL;
                indirect_placeholder();
                var_18 = local_sp_7 + (-672L);
                *(uint64_t *)var_18 = 4207310UL;
                indirect_placeholder();
                local_sp_2 = var_18;
            }
            var_20 = local_sp_2 + (-8L);
            *(uint64_t *)var_20 = 4207339UL;
            indirect_placeholder();
            local_sp_6 = var_20;
            var_21 = local_sp_6 + (-8L);
            *(uint64_t *)var_21 = 4205933UL;
            indirect_placeholder();
            local_sp_7 = var_21;
            continue;
        }
}

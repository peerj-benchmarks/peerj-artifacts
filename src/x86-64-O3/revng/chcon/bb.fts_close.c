typedef unsigned char __u_char;
typedef unsigned short __u_short;
typedef unsigned int __u_int;
typedef unsigned long __u_long;
typedef signed char __int8_t;
typedef unsigned char __uint8_t;
typedef short __int16_t;
typedef unsigned short __uint16_t;
typedef int __int32_t;
typedef unsigned int __uint32_t;
typedef long __int64_t;
typedef unsigned long __uint64_t;
typedef __int8_t __int_least8_t;
typedef __uint8_t __uint_least8_t;
typedef __int16_t __int_least16_t;
typedef __uint16_t __uint_least16_t;
typedef __int32_t __int_least32_t;
typedef __uint32_t __uint_least32_t;
typedef __int64_t __int_least64_t;
typedef __uint64_t __uint_least64_t;
typedef long __quad_t;
typedef unsigned long __u_quad_t;
typedef long __intmax_t;
typedef unsigned long __uintmax_t;
typedef unsigned long __dev_t;
typedef unsigned int __uid_t;
typedef unsigned int __gid_t;
typedef unsigned long __ino_t;
typedef unsigned long __ino64_t;
typedef unsigned int __mode_t;
typedef unsigned long __nlink_t;
typedef long __off_t;
typedef long __off64_t;
typedef int __pid_t;
typedef struct {
    int __val[2];
} __fsid_t;
typedef long __clock_t;
typedef unsigned long __rlim_t;
typedef unsigned long __rlim64_t;
typedef unsigned int __id_t;
typedef long __time_t;
typedef unsigned int __useconds_t;
typedef long __suseconds_t;
typedef int __daddr_t;
typedef int __key_t;
typedef int __clockid_t;
typedef void *__timer_t;
typedef long __blksize_t;
typedef long __blkcnt_t;
typedef long __blkcnt64_t;
typedef unsigned long __fsblkcnt_t;
typedef unsigned long __fsblkcnt64_t;
typedef unsigned long __fsfilcnt_t;
typedef unsigned long __fsfilcnt64_t;
typedef long __fsword_t;
typedef long __ssize_t;
typedef long __syscall_slong_t;
typedef unsigned long __syscall_ulong_t;
typedef __off64_t __loff_t;
typedef char *__caddr_t;
typedef long __intptr_t;
typedef unsigned int __socklen_t;
typedef int __sig_atomic_t;
typedef __int8_t int8_t;
typedef __int16_t int16_t;
typedef __int32_t int32_t;
typedef __int64_t int64_t;
typedef __uint8_t uint8_t;
typedef __uint16_t uint16_t;
typedef __uint32_t uint32_t;
typedef __uint64_t uint64_t;
typedef __int_least8_t int_least8_t;
typedef __int_least16_t int_least16_t;
typedef __int_least32_t int_least32_t;
typedef __int_least64_t int_least64_t;
typedef __uint_least8_t uint_least8_t;
typedef __uint_least16_t uint_least16_t;
typedef __uint_least32_t uint_least32_t;
typedef __uint_least64_t uint_least64_t;
typedef signed char int_fast8_t;
typedef long int_fast16_t;
typedef long int_fast32_t;
typedef long int_fast64_t;
typedef unsigned char uint_fast8_t;
typedef unsigned long uint_fast16_t;
typedef unsigned long uint_fast32_t;
typedef unsigned long uint_fast64_t;
typedef long intptr_t;
typedef unsigned long uintptr_t;
typedef __intmax_t intmax_t;
typedef __uintmax_t uintmax_t;
typedef _Bool bool;
struct indirect_placeholder_20_ret_type;
struct indirect_placeholder_20_ret_type {
    uint64_t field_0;
    uint64_t field_1;
    uint64_t field_2;
};
extern uint64_t revng_init_local_sp(uint64_t param_0);
extern uint64_t init_rbx(void);
extern uint64_t init_rbp(void);
extern void indirect_placeholder(void);
extern uint64_t init_r12(void);
extern uint64_t indirect_placeholder_5(uint64_t param_0);
extern void indirect_placeholder_19(uint64_t param_0);
extern struct indirect_placeholder_20_ret_type indirect_placeholder_20(uint64_t param_0, uint64_t param_1);
uint64_t bb_fts_close(uint64_t rdi) {
    uint64_t var_40;
    uint64_t r12_0;
    uint64_t local_sp_2;
    uint64_t var_28;
    uint64_t var_0;
    uint64_t var_1;
    uint64_t var_2;
    uint64_t var_3;
    uint64_t var_4;
    uint64_t var_5;
    uint64_t local_sp_9;
    uint64_t local_sp_4;
    uint32_t *var_24;
    uint32_t var_25;
    uint64_t var_26;
    uint64_t var_27;
    uint64_t local_sp_1;
    uint64_t storemerge;
    uint64_t local_sp_5;
    uint32_t *_pre_phi;
    uint64_t var_32;
    uint64_t local_sp_0;
    uint64_t var_33;
    uint32_t var_34;
    uint64_t var_35;
    uint64_t var_36;
    uint64_t var_37;
    uint64_t var_38;
    struct indirect_placeholder_20_ret_type var_39;
    uint64_t local_sp_5_be;
    uint64_t var_20;
    uint32_t *_pre;
    bool var_21;
    uint64_t var_22;
    uint64_t *var_23;
    uint64_t var_29;
    uint64_t var_30;
    uint64_t var_31;
    uint64_t local_sp_6;
    uint64_t rbx_0;
    uint64_t local_sp_8;
    uint64_t local_sp_7;
    uint64_t var_10;
    uint64_t rdi1_0;
    uint64_t var_6;
    uint64_t var_8;
    uint64_t var_9;
    uint64_t var_7;
    uint64_t var_11;
    uint64_t local_sp_12;
    uint64_t local_sp_11;
    uint64_t local_sp_10;
    uint64_t rbx_2;
    uint64_t var_12;
    uint64_t var_13;
    uint64_t var_14;
    uint64_t var_15;
    uint64_t var_16;
    uint32_t *var_17;
    uint32_t var_18;
    uint64_t var_19;
    var_0 = revng_init_local_sp(0UL);
    var_1 = init_rbx();
    var_2 = init_r12();
    var_3 = init_rbp();
    *(uint64_t *)(var_0 + (-8L)) = var_2;
    *(uint64_t *)(var_0 + (-16L)) = var_3;
    var_4 = var_0 + (-24L);
    *(uint64_t *)var_4 = var_1;
    var_5 = *(uint64_t *)rdi;
    r12_0 = 0UL;
    local_sp_9 = var_4;
    storemerge = 0UL;
    local_sp_8 = var_4;
    local_sp_7 = var_4;
    rdi1_0 = var_5;
    if (var_5 != 0UL) {
        if ((long)*(uint64_t *)(var_5 + 88UL) <= (long)18446744073709551615UL) {
            while (1U)
                {
                    var_6 = *(uint64_t *)(rdi1_0 + 16UL);
                    rbx_0 = var_6;
                    if (var_6 == 0UL) {
                        var_8 = *(uint64_t *)(rdi1_0 + 8UL);
                        var_9 = local_sp_8 + (-8L);
                        *(uint64_t *)var_9 = 4224241UL;
                        indirect_placeholder();
                        local_sp_6 = var_9;
                        rbx_0 = var_8;
                        local_sp_7 = var_9;
                        if ((long)*(uint64_t *)(var_8 + 88UL) > (long)18446744073709551615UL) {
                            break;
                        }
                    }
                    var_7 = local_sp_8 + (-8L);
                    *(uint64_t *)var_7 = 4224213UL;
                    indirect_placeholder();
                    local_sp_6 = var_7;
                    local_sp_7 = var_7;
                    if ((long)*(uint64_t *)(var_6 + 88UL) < (long)0UL) {
                        break;
                    }
                    local_sp_8 = local_sp_6;
                    rdi1_0 = rbx_0;
                    continue;
                }
        }
        var_10 = local_sp_7 + (-8L);
        *(uint64_t *)var_10 = 4224264UL;
        indirect_placeholder();
        local_sp_9 = var_10;
    }
    var_11 = *(uint64_t *)(rdi + 8UL);
    local_sp_10 = local_sp_9;
    rbx_2 = var_11;
    local_sp_12 = local_sp_9;
    if (var_11 == 0UL) {
        var_15 = local_sp_11 + (-8L);
        *(uint64_t *)var_15 = 4224309UL;
        indirect_placeholder();
        local_sp_10 = var_15;
        local_sp_12 = var_15;
        do {
            var_12 = *(uint64_t *)(rbx_2 + 24UL);
            var_13 = *(uint64_t *)(rbx_2 + 16UL);
            rbx_2 = var_13;
            local_sp_11 = local_sp_10;
            if (var_12 == 0UL) {
                var_14 = local_sp_10 + (-8L);
                *(uint64_t *)var_14 = 4224298UL;
                indirect_placeholder();
                local_sp_11 = var_14;
            }
            var_15 = local_sp_11 + (-8L);
            *(uint64_t *)var_15 = 4224309UL;
            indirect_placeholder();
            local_sp_10 = var_15;
            local_sp_12 = var_15;
        } while (var_13 != 0UL);
    }
    *(uint64_t *)(local_sp_12 + (-8L)) = 4224323UL;
    indirect_placeholder();
    var_16 = local_sp_12 + (-16L);
    *(uint64_t *)var_16 = 4224332UL;
    indirect_placeholder();
    var_17 = (uint32_t *)(rdi + 72UL);
    var_18 = *var_17;
    var_19 = (uint64_t)var_18;
    local_sp_4 = var_16;
    if ((uint64_t)((uint16_t)var_19 & (unsigned short)512U) == 0UL) {
        if ((var_19 & 4UL) != 0UL) {
            *(uint64_t *)(local_sp_12 + (-24L)) = 4224492UL;
            indirect_placeholder();
            var_21 = (var_18 == 0U);
            var_22 = local_sp_12 + (-32L);
            var_23 = (uint64_t *)var_22;
            local_sp_4 = var_22;
            if (var_21) {
                *var_23 = 4224504UL;
                indirect_placeholder();
            } else {
                *var_23 = 4224532UL;
                indirect_placeholder();
                var_24 = (uint32_t *)var_19;
                var_25 = *var_24;
                var_26 = (uint64_t)var_25;
                var_27 = local_sp_12 + (-40L);
                *(uint64_t *)var_27 = 4224543UL;
                indirect_placeholder();
                _pre_phi = var_24;
                local_sp_2 = var_27;
                local_sp_4 = var_27;
                r12_0 = var_26;
                if (var_25 == 0U) {
                    var_28 = local_sp_2 + (-8L);
                    *(uint64_t *)var_28 = 4224565UL;
                    indirect_placeholder();
                    local_sp_4 = var_28;
                    r12_0 = (uint64_t)*_pre_phi;
                }
            }
        }
    } else {
        var_20 = local_sp_12 + (-24L);
        *(uint64_t *)var_20 = 4224356UL;
        indirect_placeholder();
        local_sp_2 = var_20;
        local_sp_4 = var_20;
        if ((int)*(uint32_t *)(rdi + 44UL) >= (int)0U & var_18 != 0U) {
            _pre = (uint32_t *)var_19;
            _pre_phi = _pre;
            var_28 = local_sp_2 + (-8L);
            *(uint64_t *)var_28 = 4224565UL;
            indirect_placeholder();
            local_sp_4 = var_28;
            r12_0 = (uint64_t)*_pre_phi;
        }
    }
    var_29 = rdi + 96UL;
    local_sp_5 = local_sp_4;
    var_30 = local_sp_5 + (-8L);
    *(uint64_t *)var_30 = 4224396UL;
    var_31 = indirect_placeholder_5(var_29);
    local_sp_0 = var_30;
    while ((uint64_t)(unsigned char)var_31 != 0UL)
        {
            var_38 = local_sp_5 + (-16L);
            *(uint64_t *)var_38 = 4224384UL;
            var_39 = indirect_placeholder_20(var_31, var_29);
            local_sp_5_be = var_38;
            if ((int)(uint32_t)var_39.field_0 > (int)4294967295U) {
                var_40 = local_sp_5 + (-24L);
                *(uint64_t *)var_40 = 4224471UL;
                indirect_placeholder();
                local_sp_5_be = var_40;
            }
            local_sp_5 = local_sp_5_be;
            var_30 = local_sp_5 + (-8L);
            *(uint64_t *)var_30 = 4224396UL;
            var_31 = indirect_placeholder_5(var_29);
            local_sp_0 = var_30;
        }
    var_32 = *(uint64_t *)(rdi + 80UL);
    if (var_32 == 0UL) {
        var_33 = local_sp_5 + (-16L);
        *(uint64_t *)var_33 = 4224414UL;
        indirect_placeholder_19(var_32);
        local_sp_0 = var_33;
    }
    var_34 = (uint32_t)((uint16_t)*var_17 & (unsigned short)258U);
    var_35 = *(uint64_t *)(rdi + 88UL);
    local_sp_1 = local_sp_0;
    if (var_34 == 0U) {
        var_37 = local_sp_0 + (-8L);
        *(uint64_t *)var_37 = 4224525UL;
        indirect_placeholder();
        local_sp_1 = var_37;
    } else {
        if (var_35 == 0UL) {
            var_36 = local_sp_0 + (-8L);
            *(uint64_t *)var_36 = 4224437UL;
            indirect_placeholder_19(var_35);
            local_sp_1 = var_36;
        }
    }
    *(uint64_t *)(local_sp_1 + (-8L)) = 4224445UL;
    indirect_placeholder();
    if (r12_0 == 0UL) {
        return storemerge;
    }
    *(uint64_t *)(local_sp_1 + (-16L)) = 4224586UL;
    indirect_placeholder();
    *(uint32_t *)var_31 = (uint32_t)r12_0;
    storemerge = 4294967295UL;
    return storemerge;
}

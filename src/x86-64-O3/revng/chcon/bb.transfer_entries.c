typedef unsigned char __u_char;
typedef unsigned short __u_short;
typedef unsigned int __u_int;
typedef unsigned long __u_long;
typedef signed char __int8_t;
typedef unsigned char __uint8_t;
typedef short __int16_t;
typedef unsigned short __uint16_t;
typedef int __int32_t;
typedef unsigned int __uint32_t;
typedef long __int64_t;
typedef unsigned long __uint64_t;
typedef __int8_t __int_least8_t;
typedef __uint8_t __uint_least8_t;
typedef __int16_t __int_least16_t;
typedef __uint16_t __uint_least16_t;
typedef __int32_t __int_least32_t;
typedef __uint32_t __uint_least32_t;
typedef __int64_t __int_least64_t;
typedef __uint64_t __uint_least64_t;
typedef long __quad_t;
typedef unsigned long __u_quad_t;
typedef long __intmax_t;
typedef unsigned long __uintmax_t;
typedef unsigned long __dev_t;
typedef unsigned int __uid_t;
typedef unsigned int __gid_t;
typedef unsigned long __ino_t;
typedef unsigned long __ino64_t;
typedef unsigned int __mode_t;
typedef unsigned long __nlink_t;
typedef long __off_t;
typedef long __off64_t;
typedef int __pid_t;
typedef struct {
    int __val[2];
} __fsid_t;
typedef long __clock_t;
typedef unsigned long __rlim_t;
typedef unsigned long __rlim64_t;
typedef unsigned int __id_t;
typedef long __time_t;
typedef unsigned int __useconds_t;
typedef long __suseconds_t;
typedef int __daddr_t;
typedef int __key_t;
typedef int __clockid_t;
typedef void *__timer_t;
typedef long __blksize_t;
typedef long __blkcnt_t;
typedef long __blkcnt64_t;
typedef unsigned long __fsblkcnt_t;
typedef unsigned long __fsblkcnt64_t;
typedef unsigned long __fsfilcnt_t;
typedef unsigned long __fsfilcnt64_t;
typedef long __fsword_t;
typedef long __ssize_t;
typedef long __syscall_slong_t;
typedef unsigned long __syscall_ulong_t;
typedef __off64_t __loff_t;
typedef char *__caddr_t;
typedef long __intptr_t;
typedef unsigned int __socklen_t;
typedef int __sig_atomic_t;
typedef __int8_t int8_t;
typedef __int16_t int16_t;
typedef __int32_t int32_t;
typedef __int64_t int64_t;
typedef __uint8_t uint8_t;
typedef __uint16_t uint16_t;
typedef __uint32_t uint32_t;
typedef __uint64_t uint64_t;
typedef __int_least8_t int_least8_t;
typedef __int_least16_t int_least16_t;
typedef __int_least32_t int_least32_t;
typedef __int_least64_t int_least64_t;
typedef __uint_least8_t uint_least8_t;
typedef __uint_least16_t uint_least16_t;
typedef __uint_least32_t uint_least32_t;
typedef __uint_least64_t uint_least64_t;
typedef signed char int_fast8_t;
typedef long int_fast16_t;
typedef long int_fast32_t;
typedef long int_fast64_t;
typedef unsigned char uint_fast8_t;
typedef unsigned long uint_fast16_t;
typedef unsigned long uint_fast32_t;
typedef unsigned long uint_fast64_t;
typedef long intptr_t;
typedef unsigned long uintptr_t;
typedef __intmax_t intmax_t;
typedef __uintmax_t uintmax_t;
typedef _Bool bool;
struct indirect_placeholder_149_ret_type;
struct indirect_placeholder_149_ret_type {
    uint64_t field_0;
    uint64_t field_1;
};
extern void abort(void);
extern uint64_t revng_init_local_sp(uint64_t param_0);
extern uint64_t init_rbx(void);
extern uint64_t init_rbp(void);
extern void indirect_placeholder(void);
extern uint64_t init_cc_src2(void);
extern uint64_t helper_cc_compute_c_wrapper(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint32_t param_3);
extern uint64_t init_r13(void);
extern uint64_t init_r12(void);
extern uint64_t init_r14(void);
extern uint64_t init_r15(void);
extern struct indirect_placeholder_149_ret_type indirect_placeholder_149(uint64_t param_0);
uint64_t bb_transfer_entries(uint64_t rdi, uint64_t rdx, uint64_t rsi) {
    uint64_t local_sp_1;
    uint64_t var_40;
    struct indirect_placeholder_149_ret_type var_41;
    uint64_t var_42;
    uint64_t var_45;
    uint64_t var_28;
    uint64_t rax_5;
    uint64_t var_12;
    uint64_t *var_13;
    uint64_t *var_14;
    uint64_t *var_15;
    uint64_t *var_16;
    bool var_17;
    uint64_t *var_18;
    uint64_t local_sp_7;
    uint64_t rax_3;
    uint64_t rbp_0;
    uint64_t local_sp_3;
    uint64_t *var_19;
    uint64_t var_20;
    uint64_t *var_21;
    uint64_t var_22;
    uint64_t var_0;
    uint64_t var_1;
    uint64_t var_2;
    uint64_t var_3;
    uint64_t var_4;
    uint64_t var_5;
    uint64_t var_6;
    uint64_t var_7;
    uint64_t var_8;
    uint64_t *var_9;
    uint64_t var_10;
    uint64_t var_11;
    uint64_t rax_6;
    uint64_t merge;
    uint64_t rbx_0;
    uint64_t var_37;
    uint64_t *_cast2;
    uint64_t local_sp_4;
    uint64_t var_38;
    uint64_t *var_39;
    uint64_t *var_43;
    uint64_t local_sp_6;
    uint64_t *_pre_phi65;
    uint64_t var_44;
    uint64_t rax_0;
    uint64_t local_sp_0;
    uint64_t local_sp_5;
    uint64_t var_33;
    uint64_t rax_2;
    uint64_t rax_1;
    uint64_t *var_32;
    uint64_t *var_29;
    uint64_t var_30;
    uint64_t *var_31;
    uint64_t rax_4;
    uint64_t local_sp_2;
    uint64_t r14_0;
    uint64_t *var_23;
    uint64_t var_24;
    uint64_t var_25;
    uint64_t var_26;
    uint64_t var_27;
    uint64_t var_34;
    uint64_t var_35;
    uint64_t var_36;
    uint64_t var_46;
    unsigned int loop_state_var;
    bool switch_state_var;
    var_0 = revng_init_local_sp(0UL);
    var_1 = init_r15();
    var_2 = init_r13();
    var_3 = init_rbx();
    var_4 = init_r12();
    var_5 = init_rbp();
    var_6 = init_cc_src2();
    var_7 = init_r14();
    *(uint64_t *)(var_0 + (-8L)) = var_1;
    *(uint64_t *)(var_0 + (-16L)) = var_7;
    *(uint64_t *)(var_0 + (-24L)) = var_2;
    *(uint64_t *)(var_0 + (-32L)) = var_4;
    *(uint64_t *)(var_0 + (-40L)) = var_5;
    *(uint64_t *)(var_0 + (-48L)) = var_3;
    var_8 = *(uint64_t *)rsi;
    var_9 = (uint64_t *)(rsi + 8UL);
    var_10 = *var_9;
    var_11 = helper_cc_compute_c_wrapper(var_8 - var_10, var_10, var_6, 17U);
    merge = 1UL;
    rax_3 = var_10;
    rbp_0 = var_8;
    if (var_11 == 0UL) {
        return merge;
    }
    var_12 = var_0 + (-56L);
    var_13 = (uint64_t *)(rdi + 16UL);
    var_14 = (uint64_t *)rdi;
    var_15 = (uint64_t *)(rdi + 24UL);
    var_16 = (uint64_t *)(rdi + 72UL);
    var_17 = ((uint64_t)(unsigned char)rdx == 0UL);
    var_18 = (uint64_t *)(rsi + 24UL);
    merge = 0UL;
    local_sp_3 = var_12;
    while (1U)
        {
            var_19 = (uint64_t *)rbp_0;
            var_20 = *var_19;
            rax_5 = rax_3;
            local_sp_7 = local_sp_3;
            rax_6 = rax_3;
            rbx_0 = var_20;
            local_sp_4 = local_sp_3;
            local_sp_5 = local_sp_3;
            rax_4 = rax_3;
            if (var_20 == 0UL) {
                var_45 = rbp_0 + 16UL;
                var_46 = helper_cc_compute_c_wrapper(var_45 - rax_6, rax_6, var_6, 17U);
                merge = 1UL;
                rax_3 = rax_6;
                rbp_0 = var_45;
                local_sp_3 = local_sp_7;
                if (var_46 == 0UL) {
                    continue;
                }
                loop_state_var = 1U;
                break;
            }
            var_21 = (uint64_t *)(rbp_0 + 8UL);
            var_22 = *var_21;
            r14_0 = var_22;
            if (var_22 == 0UL) {
                *var_21 = 0UL;
                local_sp_6 = local_sp_5;
                if (!var_17) {
                    var_34 = local_sp_5 + (-8L);
                    *(uint64_t *)var_34 = 4234763UL;
                    indirect_placeholder();
                    var_35 = *var_13;
                    var_36 = helper_cc_compute_c_wrapper(rax_5 - var_35, var_35, var_6, 17U);
                    local_sp_0 = var_34;
                    local_sp_1 = var_34;
                    local_sp_2 = var_34;
                    if (var_36 != 0UL) {
                        loop_state_var = 0U;
                        break;
                    }
                    var_37 = (rax_5 << 4UL) + *var_14;
                    _cast2 = (uint64_t *)var_37;
                    if (*_cast2 == 0UL) {
                        *_cast2 = rbx_0;
                        *var_15 = (*var_15 + 1UL);
                    } else {
                        var_38 = *var_16;
                        rax_0 = var_38;
                        if (var_38 == 0UL) {
                            var_39 = (uint64_t *)(var_38 + 8UL);
                            *var_16 = *var_39;
                            _pre_phi65 = var_39;
                        } else {
                            var_40 = local_sp_5 + (-16L);
                            *(uint64_t *)var_40 = 4234853UL;
                            var_41 = indirect_placeholder_149(16UL);
                            var_42 = var_41.field_0;
                            rax_0 = var_42;
                            local_sp_0 = var_40;
                            if (var_42 != 0UL) {
                                loop_state_var = 1U;
                                break;
                            }
                            _pre_phi65 = (uint64_t *)(var_42 + 8UL);
                        }
                        var_43 = (uint64_t *)(var_37 + 8UL);
                        var_44 = *var_43;
                        *(uint64_t *)rax_0 = rbx_0;
                        *_pre_phi65 = var_44;
                        *var_43 = rax_0;
                        local_sp_1 = local_sp_0;
                    }
                    *var_19 = 0UL;
                    *var_18 = (*var_18 + (-1L));
                    local_sp_6 = local_sp_1;
                }
                rax_6 = *var_9;
                local_sp_7 = local_sp_6;
            } else {
                while (1U)
                    {
                        var_23 = (uint64_t *)r14_0;
                        var_24 = *var_23;
                        var_25 = local_sp_4 + (-8L);
                        *(uint64_t *)var_25 = 4234622UL;
                        indirect_placeholder();
                        var_26 = *var_13;
                        var_27 = helper_cc_compute_c_wrapper(rax_4 - var_26, var_26, var_6, 17U);
                        local_sp_2 = var_25;
                        local_sp_4 = var_25;
                        local_sp_5 = var_25;
                        if (var_27 != 0UL) {
                            loop_state_var = 0U;
                            break;
                        }
                        var_28 = (rax_4 << 4UL) + *var_14;
                        var_29 = (uint64_t *)(r14_0 + 8UL);
                        var_30 = *var_29;
                        var_31 = (uint64_t *)var_28;
                        rax_1 = var_28;
                        rax_2 = var_28;
                        r14_0 = var_30;
                        if (*var_31 == 0UL) {
                            *var_31 = var_24;
                            *var_15 = (*var_15 + 1UL);
                            *var_23 = 0UL;
                            var_33 = *var_16;
                            *var_29 = var_33;
                            *var_16 = r14_0;
                            rax_1 = var_33;
                            rax_2 = var_33;
                            if (var_30 != 0UL) {
                                loop_state_var = 1U;
                                break;
                            }
                        }
                        var_32 = (uint64_t *)(var_28 + 8UL);
                        *var_29 = *var_32;
                        *var_32 = r14_0;
                        if (var_30 != 0UL) {
                            loop_state_var = 1U;
                            break;
                        }
                        rax_4 = rax_1;
                        continue;
                    }
                switch_state_var = 0;
                switch (loop_state_var) {
                  case 1U:
                    {
                        rax_5 = rax_2;
                        rbx_0 = *var_19;
                    }
                    break;
                  case 0U:
                    {
                        loop_state_var = 0U;
                        switch_state_var = 1;
                        break;
                    }
                    break;
                }
                if (switch_state_var)
                    break;
            }
        }
    switch (loop_state_var) {
      case 1U:
        {
            break;
        }
        break;
      case 0U:
        {
            *(uint64_t *)(local_sp_2 + (-8L)) = 4234880UL;
            indirect_placeholder();
            abort();
        }
        break;
    }
}

typedef unsigned char __u_char;
typedef unsigned short __u_short;
typedef unsigned int __u_int;
typedef unsigned long __u_long;
typedef signed char __int8_t;
typedef unsigned char __uint8_t;
typedef short __int16_t;
typedef unsigned short __uint16_t;
typedef int __int32_t;
typedef unsigned int __uint32_t;
typedef long __int64_t;
typedef unsigned long __uint64_t;
typedef __int8_t __int_least8_t;
typedef __uint8_t __uint_least8_t;
typedef __int16_t __int_least16_t;
typedef __uint16_t __uint_least16_t;
typedef __int32_t __int_least32_t;
typedef __uint32_t __uint_least32_t;
typedef __int64_t __int_least64_t;
typedef __uint64_t __uint_least64_t;
typedef long __quad_t;
typedef unsigned long __u_quad_t;
typedef long __intmax_t;
typedef unsigned long __uintmax_t;
typedef unsigned long __dev_t;
typedef unsigned int __uid_t;
typedef unsigned int __gid_t;
typedef unsigned long __ino_t;
typedef unsigned long __ino64_t;
typedef unsigned int __mode_t;
typedef unsigned long __nlink_t;
typedef long __off_t;
typedef long __off64_t;
typedef int __pid_t;
typedef struct {
    int __val[2];
} __fsid_t;
typedef long __clock_t;
typedef unsigned long __rlim_t;
typedef unsigned long __rlim64_t;
typedef unsigned int __id_t;
typedef long __time_t;
typedef unsigned int __useconds_t;
typedef long __suseconds_t;
typedef int __daddr_t;
typedef int __key_t;
typedef int __clockid_t;
typedef void *__timer_t;
typedef long __blksize_t;
typedef long __blkcnt_t;
typedef long __blkcnt64_t;
typedef unsigned long __fsblkcnt_t;
typedef unsigned long __fsblkcnt64_t;
typedef unsigned long __fsfilcnt_t;
typedef unsigned long __fsfilcnt64_t;
typedef long __fsword_t;
typedef long __ssize_t;
typedef long __syscall_slong_t;
typedef unsigned long __syscall_ulong_t;
typedef __off64_t __loff_t;
typedef char *__caddr_t;
typedef long __intptr_t;
typedef unsigned int __socklen_t;
typedef int __sig_atomic_t;
typedef __int8_t int8_t;
typedef __int16_t int16_t;
typedef __int32_t int32_t;
typedef __int64_t int64_t;
typedef __uint8_t uint8_t;
typedef __uint16_t uint16_t;
typedef __uint32_t uint32_t;
typedef __uint64_t uint64_t;
typedef __int_least8_t int_least8_t;
typedef __int_least16_t int_least16_t;
typedef __int_least32_t int_least32_t;
typedef __int_least64_t int_least64_t;
typedef __uint_least8_t uint_least8_t;
typedef __uint_least16_t uint_least16_t;
typedef __uint_least32_t uint_least32_t;
typedef __uint_least64_t uint_least64_t;
typedef signed char int_fast8_t;
typedef long int_fast16_t;
typedef long int_fast32_t;
typedef long int_fast64_t;
typedef unsigned char uint_fast8_t;
typedef unsigned long uint_fast16_t;
typedef unsigned long uint_fast32_t;
typedef unsigned long uint_fast64_t;
typedef long intptr_t;
typedef unsigned long uintptr_t;
typedef __intmax_t intmax_t;
typedef __uintmax_t uintmax_t;
typedef _Bool bool;
extern void abort(void);
extern uint64_t revng_init_local_sp(uint64_t param_0);
extern uint64_t init_rbx(void);
extern uint64_t init_rbp(void);
extern uint64_t init_cc_src2(void);
extern uint64_t helper_cc_compute_c_wrapper(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint32_t param_3);
extern uint64_t indirect_placeholder_3(uint64_t param_0, uint64_t param_1, uint64_t param_2);
extern void indirect_placeholder_2(uint64_t param_0);
uint64_t bb_read_and_delete(uint64_t rdi, uint64_t rsi) {
    uint64_t rcx_0;
    uint64_t var_0;
    uint64_t var_1;
    uint64_t var_2;
    uint64_t var_3;
    uint64_t local_sp_0;
    uint64_t r9_0;
    bool var_6;
    uint64_t var_7;
    uint64_t var_8;
    uint64_t r9_4;
    uint64_t r9_1;
    uint64_t var_9;
    uint64_t var_10;
    uint64_t r9_5;
    uint64_t var_11;
    uint64_t r9_3;
    uint64_t r9_2;
    uint64_t rcx_1;
    unsigned char var_12;
    uint64_t var_13;
    uint64_t var_4;
    uint64_t var_5;
    unsigned int loop_state_var;
    bool switch_state_var;
    var_0 = revng_init_local_sp(0UL);
    var_1 = init_rbx();
    var_2 = init_rbp();
    var_3 = init_cc_src2();
    *(uint64_t *)(var_0 + (-8L)) = var_2;
    *(uint64_t *)(var_0 + (-16L)) = var_1;
    r9_0 = 0UL;
    r9_5 = 0UL;
    local_sp_0 = var_0 + (-24L);
    while (1U)
        {
            var_4 = local_sp_0 + (-8L);
            *(uint64_t *)var_4 = 4206237UL;
            var_5 = indirect_placeholder_3(0UL, rdi, rsi);
            local_sp_0 = var_4;
            switch_state_var = 0;
            switch (var_5) {
              case 18446744073709551615UL:
                {
                    *(uint64_t *)(local_sp_0 + (-16L)) = 4206370UL;
                    indirect_placeholder_2(18446744073709551615UL);
                    abort();
                }
                break;
              case 0UL:
                {
                    switch_state_var = 1;
                    break;
                }
                break;
              default:
                {
                    while (1U)
                        {
                            var_6 = (*(unsigned char *)((uint64_t)*(unsigned char *)(r9_0 + rdi) + 6383232UL) == '\x00');
                            var_7 = r9_0 + 1UL;
                            rcx_0 = var_7;
                            r9_0 = var_7;
                            r9_4 = r9_0;
                            r9_1 = r9_0;
                            r9_5 = var_5;
                            if (!var_6) {
                                loop_state_var = 0U;
                                break;
                            }
                            r9_1 = var_5;
                            if (var_7 == var_5) {
                                continue;
                            }
                            loop_state_var = 1U;
                            break;
                        }
                    switch_state_var = 0;
                    switch (loop_state_var) {
                      case 0U:
                        {
                            var_8 = helper_cc_compute_c_wrapper(var_7 - var_5, var_5, var_3, 17U);
                            if (var_8 != 0UL) {
                                r9_5 = r9_4;
                                if (r9_4 != 0UL) {
                                    continue;
                                }
                                switch_state_var = 1;
                                break;
                            }
                        }
                        break;
                      case 1U:
                        {
                            var_9 = r9_0 + 2UL;
                            var_10 = helper_cc_compute_c_wrapper(1UL, var_5, var_3, 17U);
                            rcx_0 = var_9;
                            if (var_10 == 0UL) {
                                switch_state_var = 1;
                                break;
                            }
                            var_11 = var_5 + rdi;
                            r9_2 = r9_1;
                            rcx_1 = rcx_0 + rdi;
                            var_13 = rcx_1 + 1UL;
                            r9_2 = r9_3;
                            rcx_1 = var_13;
                            r9_4 = r9_3;
                            do {
                                var_12 = *(unsigned char *)rcx_1;
                                r9_3 = r9_2;
                                if (*(unsigned char *)((uint64_t)var_12 + 6383232UL) == '\x00') {
                                    *(unsigned char *)(r9_2 + rdi) = var_12;
                                    r9_3 = r9_2 + 1UL;
                                }
                                var_13 = rcx_1 + 1UL;
                                r9_2 = r9_3;
                                rcx_1 = var_13;
                                r9_4 = r9_3;
                            } while (var_13 != var_11);
                            r9_5 = r9_4;
                            if (r9_4 != 0UL) {
                                continue;
                            }
                            switch_state_var = 1;
                            break;
                        }
                        break;
                    }
                    if (switch_state_var)
                        break;
                }
                break;
            }
            if (switch_state_var)
                break;
        }
    return r9_5;
}

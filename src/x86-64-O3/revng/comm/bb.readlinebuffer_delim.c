typedef unsigned char __u_char;
typedef unsigned short __u_short;
typedef unsigned int __u_int;
typedef unsigned long __u_long;
typedef signed char __int8_t;
typedef unsigned char __uint8_t;
typedef short __int16_t;
typedef unsigned short __uint16_t;
typedef int __int32_t;
typedef unsigned int __uint32_t;
typedef long __int64_t;
typedef unsigned long __uint64_t;
typedef __int8_t __int_least8_t;
typedef __uint8_t __uint_least8_t;
typedef __int16_t __int_least16_t;
typedef __uint16_t __uint_least16_t;
typedef __int32_t __int_least32_t;
typedef __uint32_t __uint_least32_t;
typedef __int64_t __int_least64_t;
typedef __uint64_t __uint_least64_t;
typedef long __quad_t;
typedef unsigned long __u_quad_t;
typedef long __intmax_t;
typedef unsigned long __uintmax_t;
typedef unsigned long __dev_t;
typedef unsigned int __uid_t;
typedef unsigned int __gid_t;
typedef unsigned long __ino_t;
typedef unsigned long __ino64_t;
typedef unsigned int __mode_t;
typedef unsigned long __nlink_t;
typedef long __off_t;
typedef long __off64_t;
typedef int __pid_t;
typedef struct {
    int __val[2];
} __fsid_t;
typedef long __clock_t;
typedef unsigned long __rlim_t;
typedef unsigned long __rlim64_t;
typedef unsigned int __id_t;
typedef long __time_t;
typedef unsigned int __useconds_t;
typedef long __suseconds_t;
typedef int __daddr_t;
typedef int __key_t;
typedef int __clockid_t;
typedef void *__timer_t;
typedef long __blksize_t;
typedef long __blkcnt_t;
typedef long __blkcnt64_t;
typedef unsigned long __fsblkcnt_t;
typedef unsigned long __fsblkcnt64_t;
typedef unsigned long __fsfilcnt_t;
typedef unsigned long __fsfilcnt64_t;
typedef long __fsword_t;
typedef long __ssize_t;
typedef long __syscall_slong_t;
typedef unsigned long __syscall_ulong_t;
typedef __off64_t __loff_t;
typedef char *__caddr_t;
typedef long __intptr_t;
typedef unsigned int __socklen_t;
typedef int __sig_atomic_t;
typedef __int8_t int8_t;
typedef __int16_t int16_t;
typedef __int32_t int32_t;
typedef __int64_t int64_t;
typedef __uint8_t uint8_t;
typedef __uint16_t uint16_t;
typedef __uint32_t uint32_t;
typedef __uint64_t uint64_t;
typedef __int_least8_t int_least8_t;
typedef __int_least16_t int_least16_t;
typedef __int_least32_t int_least32_t;
typedef __int_least64_t int_least64_t;
typedef __uint_least8_t uint_least8_t;
typedef __uint_least16_t uint_least16_t;
typedef __uint_least32_t uint_least32_t;
typedef __uint_least64_t uint_least64_t;
typedef signed char int_fast8_t;
typedef long int_fast16_t;
typedef long int_fast32_t;
typedef long int_fast64_t;
typedef unsigned char uint_fast8_t;
typedef unsigned long uint_fast16_t;
typedef unsigned long uint_fast32_t;
typedef unsigned long uint_fast64_t;
typedef long intptr_t;
typedef unsigned long uintptr_t;
typedef __intmax_t intmax_t;
typedef __uintmax_t uintmax_t;
extern uint64_t revng_init_local_sp(uint64_t param_0);
extern uint64_t init_rbx(void);
extern uint64_t init_rbp(void);
extern void indirect_placeholder(void);
extern uint64_t init_r13(void);
extern uint64_t init_r12(void);
extern uint64_t init_r14(void);
extern uint64_t init_rax(void);
extern uint64_t init_r15(void);
extern uint64_t indirect_placeholder_25(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint64_t param_3);
extern uint64_t init_r9(void);
extern uint64_t init_r8(void);
typedef _Bool bool;
uint64_t bb_readlinebuffer_delim(uint64_t rdi, uint64_t rdx, uint64_t rsi) {
    uint64_t rdx2_0;
    uint64_t var_0;
    uint64_t var_1;
    uint64_t var_2;
    uint64_t var_3;
    uint64_t var_4;
    uint64_t var_5;
    uint64_t var_6;
    uint64_t var_7;
    uint64_t var_8;
    uint64_t var_9;
    uint32_t var_10;
    uint64_t var_11;
    uint64_t *var_12;
    uint64_t var_13;
    uint64_t *var_14;
    uint64_t var_15;
    uint64_t var_16;
    uint64_t r13_0_ph;
    uint64_t rax_1;
    uint64_t rbx_0;
    uint64_t rbx_1_ph;
    uint64_t rdx2_1;
    uint64_t rax_1_ph;
    uint64_t r14_1_ph;
    uint64_t local_sp_2_ph;
    uint64_t var_21;
    uint64_t local_sp_0;
    uint64_t var_22;
    uint64_t var_23;
    uint64_t rbx_1;
    uint64_t r14_0;
    uint64_t local_sp_1;
    uint64_t var_24;
    uint64_t var_25;
    uint64_t var_26;
    uint32_t var_27;
    uint64_t var_28;
    uint64_t var_29;
    uint64_t var_30;
    uint64_t var_31;
    uint64_t local_sp_2;
    uint64_t var_17;
    uint32_t var_18;
    uint64_t var_19;
    uint64_t var_20;
    unsigned int loop_state_var;
    bool switch_state_var;
    var_0 = revng_init_local_sp(0UL);
    var_1 = init_rax();
    var_2 = init_r15();
    var_3 = init_r13();
    var_4 = init_rbx();
    var_5 = init_r12();
    var_6 = init_r9();
    var_7 = init_rbp();
    var_8 = init_r14();
    var_9 = init_r8();
    *(uint64_t *)(var_0 + (-8L)) = var_2;
    *(uint64_t *)(var_0 + (-16L)) = var_8;
    *(uint64_t *)(var_0 + (-24L)) = var_3;
    *(uint64_t *)(var_0 + (-32L)) = var_5;
    *(uint64_t *)(var_0 + (-40L)) = var_7;
    *(uint64_t *)(var_0 + (-48L)) = var_4;
    var_10 = (uint32_t)((int)((uint32_t)rdx << 24U) >> (int)24U);
    var_11 = (uint64_t)var_10;
    var_12 = (uint64_t *)(rdi + 16UL);
    var_13 = *var_12;
    *(unsigned char *)(var_0 + (-61L)) = (unsigned char)rdx;
    var_14 = (uint64_t *)rdi;
    var_15 = *var_14;
    var_16 = var_0 + (-80L);
    *(uint64_t *)var_16 = 4205009UL;
    indirect_placeholder();
    rdx2_0 = var_11;
    rbx_1_ph = var_13;
    rdx2_1 = var_11;
    rax_1_ph = var_1;
    r14_1_ph = var_13;
    local_sp_2_ph = var_16;
    if ((uint64_t)(uint32_t)var_1 == 0UL) {
        return 0UL;
    }
    r13_0_ph = var_13 + var_15;
    while (1U)
        {
            r14_0 = r14_1_ph;
            rbx_1 = rbx_1_ph;
            rax_1 = rax_1_ph;
            local_sp_2 = local_sp_2_ph;
            while (1U)
                {
                    var_17 = local_sp_2 + (-8L);
                    *(uint64_t *)var_17 = 4205051UL;
                    indirect_placeholder();
                    var_18 = (uint32_t)rax_1;
                    var_19 = (uint64_t)(var_18 + 1U);
                    var_20 = (uint64_t)var_18;
                    local_sp_0 = var_17;
                    rbx_0 = rbx_1;
                    local_sp_1 = var_17;
                    rax_1 = rbx_1;
                    if (var_19 != 0UL) {
                        rdx2_0 = var_20;
                        rdx2_1 = var_20;
                        if (rbx_1 != r13_0_ph) {
                            loop_state_var = 2U;
                            break;
                        }
                        var_22 = (uint64_t)((uint32_t)rdx2_0 - var_10);
                        var_23 = rbx_1 + 1UL;
                        *(unsigned char *)rbx_1 = (unsigned char)rdx2_0;
                        rbx_0 = var_23;
                        rbx_1 = var_23;
                        local_sp_2 = local_sp_0;
                        if (var_22 == 0UL) {
                            continue;
                        }
                        loop_state_var = 0U;
                        break;
                    }
                    if (rbx_1 != r14_1_ph) {
                        loop_state_var = 1U;
                        break;
                    }
                    var_21 = local_sp_2 + (-16L);
                    *(uint64_t *)var_21 = 4205071UL;
                    indirect_placeholder();
                    local_sp_0 = var_21;
                    local_sp_1 = var_21;
                    if (var_20 != 0UL) {
                        loop_state_var = 1U;
                        break;
                    }
                    if ((uint64_t)(*(unsigned char *)(rbx_1 + (-1L)) - *(unsigned char *)(local_sp_2 + (-5L))) != 0UL) {
                        loop_state_var = 0U;
                        break;
                    }
                    if (rbx_1 != r13_0_ph) {
                        loop_state_var = 2U;
                        break;
                    }
                }
            switch_state_var = 0;
            switch (loop_state_var) {
              case 2U:
                {
                    var_24 = *var_14;
                    *(uint32_t *)(local_sp_1 + 12UL) = (uint32_t)rdx2_1;
                    var_25 = local_sp_1 + (-8L);
                    *(uint64_t *)var_25 = 4205114UL;
                    var_26 = indirect_placeholder_25(r14_1_ph, var_6, rdi, var_9);
                    var_27 = *(uint32_t *)(local_sp_1 + 4UL);
                    var_28 = var_24 + var_26;
                    var_29 = *var_14;
                    *var_12 = var_26;
                    var_30 = var_28 + 1UL;
                    var_31 = (uint64_t)(var_27 - var_10);
                    *(unsigned char *)var_28 = (unsigned char)var_27;
                    rbx_0 = var_30;
                    rbx_1_ph = var_30;
                    rax_1_ph = var_28;
                    r14_1_ph = var_26;
                    local_sp_2_ph = var_25;
                    r14_0 = var_26;
                    if (var_31 != 0UL) {
                        loop_state_var = 0U;
                        switch_state_var = 1;
                        break;
                    }
                    r13_0_ph = var_26 + var_29;
                    continue;
                }
                break;
              case 0U:
                {
                    loop_state_var = 0U;
                    switch_state_var = 1;
                    break;
                }
                break;
              case 1U:
                {
                    loop_state_var = 1U;
                    switch_state_var = 1;
                    break;
                }
                break;
            }
            if (switch_state_var)
                break;
        }
    switch (loop_state_var) {
      case 0U:
        {
            *(uint64_t *)(rdi + 8UL) = (rbx_0 - r14_0);
            return rdi;
        }
        break;
      case 1U:
        {
            return 0UL;
        }
        break;
    }
}

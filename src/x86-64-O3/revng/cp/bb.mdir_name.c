typedef unsigned char __u_char;
typedef unsigned short __u_short;
typedef unsigned int __u_int;
typedef unsigned long __u_long;
typedef signed char __int8_t;
typedef unsigned char __uint8_t;
typedef short __int16_t;
typedef unsigned short __uint16_t;
typedef int __int32_t;
typedef unsigned int __uint32_t;
typedef long __int64_t;
typedef unsigned long __uint64_t;
typedef __int8_t __int_least8_t;
typedef __uint8_t __uint_least8_t;
typedef __int16_t __int_least16_t;
typedef __uint16_t __uint_least16_t;
typedef __int32_t __int_least32_t;
typedef __uint32_t __uint_least32_t;
typedef __int64_t __int_least64_t;
typedef __uint64_t __uint_least64_t;
typedef long __quad_t;
typedef unsigned long __u_quad_t;
typedef long __intmax_t;
typedef unsigned long __uintmax_t;
typedef unsigned long __dev_t;
typedef unsigned int __uid_t;
typedef unsigned int __gid_t;
typedef unsigned long __ino_t;
typedef unsigned long __ino64_t;
typedef unsigned int __mode_t;
typedef unsigned long __nlink_t;
typedef long __off_t;
typedef long __off64_t;
typedef int __pid_t;
typedef struct {
    int __val[2];
} __fsid_t;
typedef long __clock_t;
typedef unsigned long __rlim_t;
typedef unsigned long __rlim64_t;
typedef unsigned int __id_t;
typedef long __time_t;
typedef unsigned int __useconds_t;
typedef long __suseconds_t;
typedef int __daddr_t;
typedef int __key_t;
typedef int __clockid_t;
typedef void *__timer_t;
typedef long __blksize_t;
typedef long __blkcnt_t;
typedef long __blkcnt64_t;
typedef unsigned long __fsblkcnt_t;
typedef unsigned long __fsblkcnt64_t;
typedef unsigned long __fsfilcnt_t;
typedef unsigned long __fsfilcnt64_t;
typedef long __fsword_t;
typedef long __ssize_t;
typedef long __syscall_slong_t;
typedef unsigned long __syscall_ulong_t;
typedef __off64_t __loff_t;
typedef char *__caddr_t;
typedef long __intptr_t;
typedef unsigned int __socklen_t;
typedef int __sig_atomic_t;
typedef __int8_t int8_t;
typedef __int16_t int16_t;
typedef __int32_t int32_t;
typedef __int64_t int64_t;
typedef __uint8_t uint8_t;
typedef __uint16_t uint16_t;
typedef __uint32_t uint32_t;
typedef __uint64_t uint64_t;
typedef __int_least8_t int_least8_t;
typedef __int_least16_t int_least16_t;
typedef __int_least32_t int_least32_t;
typedef __int_least64_t int_least64_t;
typedef __uint_least8_t uint_least8_t;
typedef __uint_least16_t uint_least16_t;
typedef __uint_least32_t uint_least32_t;
typedef __uint_least64_t uint_least64_t;
typedef signed char int_fast8_t;
typedef long int_fast16_t;
typedef long int_fast32_t;
typedef long int_fast64_t;
typedef unsigned char uint_fast8_t;
typedef unsigned long uint_fast16_t;
typedef unsigned long uint_fast32_t;
typedef unsigned long uint_fast64_t;
typedef long intptr_t;
typedef unsigned long uintptr_t;
typedef __intmax_t intmax_t;
typedef __uintmax_t uintmax_t;
struct indirect_placeholder_171_ret_type;
struct indirect_placeholder_172_ret_type;
struct indirect_placeholder_171_ret_type {
    uint64_t field_0;
    uint64_t field_1;
};
struct indirect_placeholder_172_ret_type {
    uint64_t field_0;
    uint64_t field_1;
};
extern uint64_t revng_init_local_sp(uint64_t param_0);
extern uint64_t init_rbx(void);
extern uint64_t init_rbp(void);
extern void indirect_placeholder(void);
extern uint64_t init_r13(void);
extern uint64_t init_r12(void);
extern uint64_t init_r14(void);
extern uint64_t indirect_placeholder_55(uint64_t param_0);
extern struct indirect_placeholder_171_ret_type indirect_placeholder_171(uint64_t param_0);
extern struct indirect_placeholder_172_ret_type indirect_placeholder_172(uint64_t param_0);
uint64_t bb_mdir_name(uint64_t rdi) {
    uint64_t var_0;
    uint64_t var_1;
    uint64_t var_2;
    uint64_t var_3;
    uint64_t var_4;
    uint64_t var_5;
    uint64_t var_6;
    uint64_t var_7;
    uint64_t var_8;
    uint64_t rbx_3;
    uint64_t rbx_0;
    uint64_t r12_0;
    uint64_t rbx_2;
    uint64_t storemerge;
    uint64_t rbx_1_in;
    uint64_t rbx_1;
    uint64_t var_9;
    struct indirect_placeholder_171_ret_type var_10;
    uint64_t var_11;
    uint64_t var_12;
    uint64_t var_13;
    uint64_t var_14;
    struct indirect_placeholder_172_ret_type var_15;
    uint64_t var_16;
    unsigned int loop_state_var;
    var_0 = revng_init_local_sp(0UL);
    var_1 = init_r13();
    var_2 = init_rbx();
    var_3 = init_r12();
    var_4 = init_rbp();
    var_5 = init_r14();
    *(uint64_t *)(var_0 + (-8L)) = var_5;
    *(uint64_t *)(var_0 + (-16L)) = var_1;
    *(uint64_t *)(var_0 + (-24L)) = var_3;
    *(uint64_t *)(var_0 + (-32L)) = var_4;
    *(uint64_t *)(var_0 + (-40L)) = var_2;
    var_6 = (*(unsigned char *)rdi == '/');
    *(uint64_t *)(var_0 + (-48L)) = 4239226UL;
    var_7 = indirect_placeholder_55(rdi);
    var_8 = var_7 - rdi;
    storemerge = 0UL;
    rbx_1_in = var_8;
    rbx_2 = var_8;
    rbx_3 = var_8;
    if (var_8 > var_6) {
        var_12 = rbx_3 + 1UL;
        var_13 = (rbx_3 ^ 1UL) & 1UL;
        var_14 = var_13 + var_12;
        *(uint64_t *)(var_0 + (-56L)) = 4239304UL;
        var_15 = indirect_placeholder_172(var_14);
        var_16 = var_15.field_0;
        rbx_0 = rbx_3;
        r12_0 = var_16;
        if (var_16 == 0UL) {
            return storemerge;
        }
        *(uint64_t *)(var_0 + (-64L)) = 4239326UL;
        indirect_placeholder();
        if (var_13 == 0UL) {
            *(unsigned char *)(rbx_3 + var_16) = (unsigned char)'.';
            rbx_0 = var_12;
        }
        *(unsigned char *)(rbx_0 + r12_0) = (unsigned char)'\x00';
        storemerge = r12_0;
        return storemerge;
    }
    rbx_3 = var_6;
    if (*(unsigned char *)(var_7 + (-1L)) != '/') {
        var_9 = (rbx_2 + (rbx_2 == 0UL)) + 1UL;
        *(uint64_t *)(var_0 + (-56L)) = 4239386UL;
        var_10 = indirect_placeholder_171(var_9);
        var_11 = var_10.field_0;
        rbx_0 = rbx_2;
        r12_0 = var_11;
        if (var_11 == 0UL) {
            return storemerge;
        }
        *(uint64_t *)(var_0 + (-64L)) = 4239408UL;
        indirect_placeholder();
        *(unsigned char *)(rbx_0 + r12_0) = (unsigned char)'\x00';
        storemerge = r12_0;
        return storemerge;
    }
    while (1U)
        {
            rbx_1 = rbx_1_in + (-1L);
            rbx_1_in = rbx_1;
            rbx_2 = rbx_1;
            if (rbx_1 != var_6) {
                loop_state_var = 0U;
                break;
            }
            if (*(unsigned char *)((rbx_1 + rdi) + (-1L)) == '/') {
                continue;
            }
            loop_state_var = 1U;
            break;
        }
    switch (loop_state_var) {
      case 0U:
        {
            break;
        }
        break;
      case 1U:
        {
            var_9 = (rbx_2 + (rbx_2 == 0UL)) + 1UL;
            *(uint64_t *)(var_0 + (-56L)) = 4239386UL;
            var_10 = indirect_placeholder_171(var_9);
            var_11 = var_10.field_0;
            rbx_0 = rbx_2;
            r12_0 = var_11;
            if (var_11 == 0UL) {
                return storemerge;
            }
            *(uint64_t *)(var_0 + (-64L)) = 4239408UL;
            indirect_placeholder();
            *(unsigned char *)(rbx_0 + r12_0) = (unsigned char)'\x00';
            storemerge = r12_0;
            return storemerge;
        }
        break;
    }
}

typedef unsigned char __u_char;
typedef unsigned short __u_short;
typedef unsigned int __u_int;
typedef unsigned long __u_long;
typedef signed char __int8_t;
typedef unsigned char __uint8_t;
typedef short __int16_t;
typedef unsigned short __uint16_t;
typedef int __int32_t;
typedef unsigned int __uint32_t;
typedef long __int64_t;
typedef unsigned long __uint64_t;
typedef __int8_t __int_least8_t;
typedef __uint8_t __uint_least8_t;
typedef __int16_t __int_least16_t;
typedef __uint16_t __uint_least16_t;
typedef __int32_t __int_least32_t;
typedef __uint32_t __uint_least32_t;
typedef __int64_t __int_least64_t;
typedef __uint64_t __uint_least64_t;
typedef long __quad_t;
typedef unsigned long __u_quad_t;
typedef long __intmax_t;
typedef unsigned long __uintmax_t;
typedef unsigned long __dev_t;
typedef unsigned int __uid_t;
typedef unsigned int __gid_t;
typedef unsigned long __ino_t;
typedef unsigned long __ino64_t;
typedef unsigned int __mode_t;
typedef unsigned long __nlink_t;
typedef long __off_t;
typedef long __off64_t;
typedef int __pid_t;
typedef struct {
    int __val[2];
} __fsid_t;
typedef long __clock_t;
typedef unsigned long __rlim_t;
typedef unsigned long __rlim64_t;
typedef unsigned int __id_t;
typedef long __time_t;
typedef unsigned int __useconds_t;
typedef long __suseconds_t;
typedef int __daddr_t;
typedef int __key_t;
typedef int __clockid_t;
typedef void *__timer_t;
typedef long __blksize_t;
typedef long __blkcnt_t;
typedef long __blkcnt64_t;
typedef unsigned long __fsblkcnt_t;
typedef unsigned long __fsblkcnt64_t;
typedef unsigned long __fsfilcnt_t;
typedef unsigned long __fsfilcnt64_t;
typedef long __fsword_t;
typedef long __ssize_t;
typedef long __syscall_slong_t;
typedef unsigned long __syscall_ulong_t;
typedef __off64_t __loff_t;
typedef char *__caddr_t;
typedef long __intptr_t;
typedef unsigned int __socklen_t;
typedef int __sig_atomic_t;
typedef __int8_t int8_t;
typedef __int16_t int16_t;
typedef __int32_t int32_t;
typedef __int64_t int64_t;
typedef __uint8_t uint8_t;
typedef __uint16_t uint16_t;
typedef __uint32_t uint32_t;
typedef __uint64_t uint64_t;
typedef __int_least8_t int_least8_t;
typedef __int_least16_t int_least16_t;
typedef __int_least32_t int_least32_t;
typedef __int_least64_t int_least64_t;
typedef __uint_least8_t uint_least8_t;
typedef __uint_least16_t uint_least16_t;
typedef __uint_least32_t uint_least32_t;
typedef __uint_least64_t uint_least64_t;
typedef signed char int_fast8_t;
typedef long int_fast16_t;
typedef long int_fast32_t;
typedef long int_fast64_t;
typedef unsigned char uint_fast8_t;
typedef unsigned long uint_fast16_t;
typedef unsigned long uint_fast32_t;
typedef unsigned long uint_fast64_t;
typedef long intptr_t;
typedef unsigned long uintptr_t;
typedef __intmax_t intmax_t;
typedef __uintmax_t uintmax_t;
extern uint64_t revng_init_local_sp(uint64_t param_0);
extern uint64_t init_rbx(void);
extern void indirect_placeholder(void);
extern uint64_t init_rax(void);
uint64_t bb_rpl_rmdir(uint64_t rdi) {
    uint64_t var_0;
    uint64_t var_1;
    uint64_t var_2;
    uint64_t rax_2;
    uint64_t rbx_0;
    uint32_t *var_10;
    unsigned char var_3;
    uint64_t var_4;
    uint64_t rax_1;
    uint64_t rax_0;
    uint64_t rcx_0;
    uint64_t rdx_0_in;
    uint64_t rdx_0;
    unsigned char var_5;
    uint64_t var_6;
    uint64_t rdx_1;
    uint32_t var_7;
    uint64_t var_8;
    uint64_t var_9;
    unsigned int loop_state_var;
    var_0 = revng_init_local_sp(0UL);
    var_1 = init_rax();
    var_2 = init_rbx();
    *(uint64_t *)(var_0 + (-8L)) = var_2;
    *(uint64_t *)(var_0 + (-16L)) = 4275801UL;
    indirect_placeholder();
    rax_2 = 0UL;
    rbx_0 = 4294967295UL;
    rax_1 = var_1;
    rax_0 = var_1;
    rdx_0_in = var_1;
    rdx_1 = var_1;
    if (var_1 == 0UL) {
        *(uint64_t *)(var_0 + (-24L)) = 4275853UL;
        indirect_placeholder();
        var_7 = (uint32_t)rax_2;
        var_8 = (uint64_t)(var_7 + 1U);
        var_9 = (uint64_t)var_7;
        rbx_0 = var_9;
        *(uint64_t *)(var_0 + (-32L)) = 4275925UL;
        indirect_placeholder();
        var_10 = (uint32_t *)rax_2;
        if (var_8 != 0UL & *var_10 == 22U) {
            *(uint64_t *)(var_0 + (-40L)) = 4275935UL;
            indirect_placeholder();
            *var_10 = 20U;
        }
        return rbx_0;
    }
    var_3 = *(unsigned char *)((var_1 + rdi) + (-1L));
    var_4 = (uint64_t)var_3;
    rcx_0 = var_4;
    if ((uint64_t)(var_3 + '\xd1') == 0UL) {
        while (1U)
            {
                rdx_0 = rdx_0_in + (-1L);
                rdx_0_in = rdx_0;
                rdx_1 = rdx_0;
                rax_2 = rax_0;
                if (rdx_0 != 0UL) {
                    loop_state_var = 0U;
                    break;
                }
                var_5 = *(unsigned char *)((rdx_0 + rdi) + (-1L));
                var_6 = (uint64_t)var_5;
                rax_0 = var_6;
                rax_1 = var_6;
                rcx_0 = var_6;
                if ((uint64_t)(var_5 + '\xd1') == 0UL) {
                    continue;
                }
                loop_state_var = 1U;
                break;
            }
        rax_2 = rax_1;
        if ((uint64_t)((unsigned char)rcx_0 + '\xd2') != 0UL) {
            if (rdx_1 != 1UL) {
                *(uint64_t *)(var_0 + (-24L)) = 4275897UL;
                indirect_placeholder();
                *(uint32_t *)rax_1 = 22U;
                return rbx_0;
            }
            if (*(unsigned char *)((rdx_1 + rdi) + (-2L)) != '/') {
                *(uint64_t *)(var_0 + (-24L)) = 4275897UL;
                indirect_placeholder();
                *(uint32_t *)rax_1 = 22U;
                return rbx_0;
            }
        }
    }
    rax_2 = rax_1;
    if ((uint64_t)((unsigned char)rcx_0 + '\xd2') == 0UL) {
        *(uint64_t *)(var_0 + (-24L)) = 4275853UL;
        indirect_placeholder();
        var_7 = (uint32_t)rax_2;
        var_8 = (uint64_t)(var_7 + 1U);
        var_9 = (uint64_t)var_7;
        rbx_0 = var_9;
        *(uint64_t *)(var_0 + (-32L)) = 4275925UL;
        indirect_placeholder();
        var_10 = (uint32_t *)rax_2;
        if (var_8 != 0UL & *var_10 == 22U) {
            *(uint64_t *)(var_0 + (-40L)) = 4275935UL;
            indirect_placeholder();
            *var_10 = 20U;
        }
        return rbx_0;
    }
    if (rdx_1 == 1UL) {
        *(uint64_t *)(var_0 + (-24L)) = 4275897UL;
        indirect_placeholder();
        *(uint32_t *)rax_1 = 22U;
        return rbx_0;
    }
    if (*(unsigned char *)((rdx_1 + rdi) + (-2L)) != '/') {
        return;
    }
    *(uint64_t *)(var_0 + (-24L)) = 4275897UL;
    indirect_placeholder();
    *(uint32_t *)rax_1 = 22U;
    return rbx_0;
}

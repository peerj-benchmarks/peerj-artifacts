typedef unsigned char __u_char;
typedef unsigned short __u_short;
typedef unsigned int __u_int;
typedef unsigned long __u_long;
typedef signed char __int8_t;
typedef unsigned char __uint8_t;
typedef short __int16_t;
typedef unsigned short __uint16_t;
typedef int __int32_t;
typedef unsigned int __uint32_t;
typedef long __int64_t;
typedef unsigned long __uint64_t;
typedef __int8_t __int_least8_t;
typedef __uint8_t __uint_least8_t;
typedef __int16_t __int_least16_t;
typedef __uint16_t __uint_least16_t;
typedef __int32_t __int_least32_t;
typedef __uint32_t __uint_least32_t;
typedef __int64_t __int_least64_t;
typedef __uint64_t __uint_least64_t;
typedef long __quad_t;
typedef unsigned long __u_quad_t;
typedef long __intmax_t;
typedef unsigned long __uintmax_t;
typedef unsigned long __dev_t;
typedef unsigned int __uid_t;
typedef unsigned int __gid_t;
typedef unsigned long __ino_t;
typedef unsigned long __ino64_t;
typedef unsigned int __mode_t;
typedef unsigned long __nlink_t;
typedef long __off_t;
typedef long __off64_t;
typedef int __pid_t;
typedef struct {
    int __val[2];
} __fsid_t;
typedef long __clock_t;
typedef unsigned long __rlim_t;
typedef unsigned long __rlim64_t;
typedef unsigned int __id_t;
typedef long __time_t;
typedef unsigned int __useconds_t;
typedef long __suseconds_t;
typedef int __daddr_t;
typedef int __key_t;
typedef int __clockid_t;
typedef void *__timer_t;
typedef long __blksize_t;
typedef long __blkcnt_t;
typedef long __blkcnt64_t;
typedef unsigned long __fsblkcnt_t;
typedef unsigned long __fsblkcnt64_t;
typedef unsigned long __fsfilcnt_t;
typedef unsigned long __fsfilcnt64_t;
typedef long __fsword_t;
typedef long __ssize_t;
typedef long __syscall_slong_t;
typedef unsigned long __syscall_ulong_t;
typedef __off64_t __loff_t;
typedef char *__caddr_t;
typedef long __intptr_t;
typedef unsigned int __socklen_t;
typedef int __sig_atomic_t;
typedef __int8_t int8_t;
typedef __int16_t int16_t;
typedef __int32_t int32_t;
typedef __int64_t int64_t;
typedef __uint8_t uint8_t;
typedef __uint16_t uint16_t;
typedef __uint32_t uint32_t;
typedef __uint64_t uint64_t;
typedef __int_least8_t int_least8_t;
typedef __int_least16_t int_least16_t;
typedef __int_least32_t int_least32_t;
typedef __int_least64_t int_least64_t;
typedef __uint_least8_t uint_least8_t;
typedef __uint_least16_t uint_least16_t;
typedef __uint_least32_t uint_least32_t;
typedef __uint_least64_t uint_least64_t;
typedef signed char int_fast8_t;
typedef long int_fast16_t;
typedef long int_fast32_t;
typedef long int_fast64_t;
typedef unsigned char uint_fast8_t;
typedef unsigned long uint_fast16_t;
typedef unsigned long uint_fast32_t;
typedef unsigned long uint_fast64_t;
typedef long intptr_t;
typedef unsigned long uintptr_t;
typedef __intmax_t intmax_t;
typedef __uintmax_t uintmax_t;
typedef _Bool bool;
extern uint64_t revng_init_local_sp(uint64_t param_0);
extern uint64_t init_rbx(void);
extern uint64_t init_rbp(void);
extern void indirect_placeholder(void);
extern uint64_t init_cc_src2(void);
extern uint64_t helper_cc_compute_c_wrapper(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint32_t param_3);
extern uint64_t init_r13(void);
extern uint64_t init_r12(void);
extern uint64_t init_r14(void);
extern uint64_t init_r15(void);
uint64_t bb_careadlinkat(uint64_t rdi, uint64_t r9, uint64_t rcx, uint64_t rdx, uint64_t rsi, uint64_t r8) {
    uint64_t var_13;
    uint64_t var_0;
    uint64_t var_1;
    uint64_t var_2;
    uint64_t var_3;
    uint64_t var_4;
    uint64_t var_5;
    uint64_t var_6;
    uint64_t var_7;
    uint64_t var_8;
    uint64_t var_9;
    bool var_10;
    uint64_t rbx_2;
    uint64_t r12_0;
    uint64_t r13_0;
    uint64_t rax_0;
    uint64_t rbx_3;
    uint64_t var_16;
    uint64_t r13_0_be;
    uint64_t rbx_3_be;
    uint64_t local_sp_4_be;
    uint64_t local_sp_4;
    uint64_t rcx3_0;
    uint64_t local_sp_0;
    uint64_t local_sp_1;
    uint64_t var_17;
    uint64_t var_20;
    bool var_21;
    uint64_t var_22;
    uint64_t *var_23;
    uint64_t var_24;
    uint64_t var_25;
    uint64_t var_26;
    uint64_t var_27;
    uint64_t local_sp_3;
    uint64_t rbx_0;
    uint64_t var_28;
    uint32_t *var_14;
    uint32_t var_15;
    uint64_t var_18;
    uint64_t var_19;
    uint64_t var_29;
    uint64_t var_30;
    uint64_t var_11;
    uint64_t *var_12;
    unsigned int loop_state_var;
    var_0 = revng_init_local_sp(0UL);
    var_1 = init_r15();
    var_2 = init_r13();
    var_3 = init_rbx();
    var_4 = init_r12();
    var_5 = init_rbp();
    var_6 = init_cc_src2();
    var_7 = init_r14();
    *(uint64_t *)(var_0 + (-8L)) = var_1;
    *(uint64_t *)(var_0 + (-16L)) = var_7;
    *(uint64_t *)(var_0 + (-24L)) = var_2;
    *(uint64_t *)(var_0 + (-32L)) = var_4;
    *(uint64_t *)(var_0 + (-40L)) = var_5;
    *(uint64_t *)(var_0 + (-48L)) = var_3;
    var_8 = var_0 + (-1096L);
    var_9 = (r8 == 0UL) ? 4338688UL : r8;
    *(uint32_t *)var_8 = (uint32_t)rdi;
    var_10 = (rcx == 0UL);
    rbx_2 = var_10 ? ((rcx & (-65536L)) | 1024UL) : rcx;
    r12_0 = var_10 ? (var_0 + (-1080L)) : rdx;
    r13_0 = r12_0;
    rax_0 = 4338688UL;
    rbx_3 = rbx_2;
    r13_0_be = 9223372036854775808UL;
    rbx_3_be = 9223372036854775808UL;
    local_sp_4 = var_8;
    rbx_0 = 0UL;
    while (1U)
        {
            var_11 = local_sp_4 + (-8L);
            var_12 = (uint64_t *)var_11;
            *var_12 = 4277064UL;
            indirect_placeholder();
            rcx3_0 = rax_0;
            local_sp_0 = var_11;
            if ((long)rax_0 >= (long)0UL) {
                *(uint64_t *)local_sp_4 = rax_0;
                var_13 = local_sp_4 + (-16L);
                *(uint64_t *)var_13 = 4277218UL;
                indirect_placeholder();
                var_14 = (uint32_t *)rax_0;
                var_15 = *var_14;
                rcx3_0 = *var_12;
                local_sp_0 = var_13;
                if (var_15 != 34U) {
                    *(uint32_t *)var_13 = var_15;
                    if (r12_0 != r13_0) {
                        loop_state_var = 0U;
                        break;
                    }
                    *(uint64_t *)(local_sp_4 + (-24L)) = 4277250UL;
                    indirect_placeholder();
                    var_16 = local_sp_4 + (-32L);
                    *(uint64_t *)var_16 = 4277255UL;
                    indirect_placeholder();
                    *var_14 = *(uint32_t *)var_16;
                    loop_state_var = 0U;
                    break;
                }
            }
            local_sp_1 = local_sp_0;
            rbx_0 = r13_0;
            if (rbx_3 > rcx3_0) {
                if (r13_0 == r12_0) {
                    var_17 = local_sp_0 + (-8L);
                    *(uint64_t *)var_17 = 4277096UL;
                    indirect_placeholder();
                    local_sp_1 = var_17;
                }
                if (rbx_3 > 4611686018427387904UL) {
                    var_18 = rbx_3 << 1UL;
                    var_19 = local_sp_1 + (-8L);
                    *(uint64_t *)var_19 = 4277041UL;
                    indirect_placeholder();
                    r13_0_be = 4611686018427387904UL;
                    rbx_3_be = var_18;
                    local_sp_4_be = var_19;
                } else {
                    var_20 = helper_cc_compute_c_wrapper(rbx_3 ^ (-9223372036854775808L), 9223372036854775808UL, var_6, 17U);
                    var_21 = (var_20 == 0UL);
                    var_22 = local_sp_1 + (-8L);
                    var_23 = (uint64_t *)var_22;
                    local_sp_4_be = var_22;
                    if (!var_21) {
                        *var_23 = 4277333UL;
                        indirect_placeholder();
                        *(uint32_t *)9223372036854775808UL = 36U;
                        loop_state_var = 0U;
                        break;
                    }
                    *var_23 = 4277146UL;
                    indirect_placeholder();
                }
                r13_0 = r13_0_be;
                rbx_3 = rbx_3_be;
                rax_0 = r13_0_be;
                local_sp_4 = local_sp_4_be;
                continue;
            }
            var_24 = local_sp_0 + 16UL;
            var_25 = rcx3_0 + 1UL;
            *(unsigned char *)(rcx3_0 + r13_0) = (unsigned char)'\x00';
            if (r13_0 == var_24) {
                var_26 = helper_cc_compute_c_wrapper(var_25 - rbx_3, rbx_3, var_6, 17U);
                if (!((var_26 == 0UL) || (r12_0 == r13_0))) {
                    loop_state_var = 0U;
                    break;
                }
                var_27 = *(uint64_t *)(var_9 + 8UL);
                if (var_27 != 0UL) {
                    loop_state_var = 0U;
                    break;
                }
                *(uint64_t *)(local_sp_0 + (-8L)) = 4277311UL;
                indirect_placeholder();
                rbx_0 = var_27;
                loop_state_var = 0U;
                break;
            }
            *(uint64_t *)local_sp_0 = var_25;
            var_28 = local_sp_0 + (-8L);
            *(uint64_t *)var_28 = 4277364UL;
            indirect_placeholder();
            rbx_0 = 0UL;
            local_sp_3 = var_28;
            if (r13_0 != 0UL) {
                *(uint64_t *)(local_sp_0 + (-16L)) = 4277390UL;
                indirect_placeholder();
                loop_state_var = 0U;
                break;
            }
            var_29 = *(uint64_t *)(var_9 + 24UL);
            if (var_29 != 0UL) {
                loop_state_var = 1U;
                break;
            }
            var_30 = local_sp_0 + (-16L);
            *(uint64_t *)var_30 = 4277168UL;
            indirect_placeholder();
            local_sp_3 = var_30;
            loop_state_var = 1U;
            break;
        }
    switch (loop_state_var) {
      case 0U:
        {
            return rbx_0;
        }
        break;
      case 1U:
        {
            *(uint64_t *)(local_sp_3 + (-8L)) = 4277173UL;
            indirect_placeholder();
            *(uint32_t *)var_29 = 12U;
        }
        break;
    }
}

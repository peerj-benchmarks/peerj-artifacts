typedef unsigned char __u_char;
typedef unsigned short __u_short;
typedef unsigned int __u_int;
typedef unsigned long __u_long;
typedef signed char __int8_t;
typedef unsigned char __uint8_t;
typedef short __int16_t;
typedef unsigned short __uint16_t;
typedef int __int32_t;
typedef unsigned int __uint32_t;
typedef long __int64_t;
typedef unsigned long __uint64_t;
typedef __int8_t __int_least8_t;
typedef __uint8_t __uint_least8_t;
typedef __int16_t __int_least16_t;
typedef __uint16_t __uint_least16_t;
typedef __int32_t __int_least32_t;
typedef __uint32_t __uint_least32_t;
typedef __int64_t __int_least64_t;
typedef __uint64_t __uint_least64_t;
typedef long __quad_t;
typedef unsigned long __u_quad_t;
typedef long __intmax_t;
typedef unsigned long __uintmax_t;
typedef unsigned long __dev_t;
typedef unsigned int __uid_t;
typedef unsigned int __gid_t;
typedef unsigned long __ino_t;
typedef unsigned long __ino64_t;
typedef unsigned int __mode_t;
typedef unsigned long __nlink_t;
typedef long __off_t;
typedef long __off64_t;
typedef int __pid_t;
typedef struct {
    int __val[2];
} __fsid_t;
typedef long __clock_t;
typedef unsigned long __rlim_t;
typedef unsigned long __rlim64_t;
typedef unsigned int __id_t;
typedef long __time_t;
typedef unsigned int __useconds_t;
typedef long __suseconds_t;
typedef int __daddr_t;
typedef int __key_t;
typedef int __clockid_t;
typedef void *__timer_t;
typedef long __blksize_t;
typedef long __blkcnt_t;
typedef long __blkcnt64_t;
typedef unsigned long __fsblkcnt_t;
typedef unsigned long __fsblkcnt64_t;
typedef unsigned long __fsfilcnt_t;
typedef unsigned long __fsfilcnt64_t;
typedef long __fsword_t;
typedef long __ssize_t;
typedef long __syscall_slong_t;
typedef unsigned long __syscall_ulong_t;
typedef __off64_t __loff_t;
typedef char *__caddr_t;
typedef long __intptr_t;
typedef unsigned int __socklen_t;
typedef int __sig_atomic_t;
typedef __int8_t int8_t;
typedef __int16_t int16_t;
typedef __int32_t int32_t;
typedef __int64_t int64_t;
typedef __uint8_t uint8_t;
typedef __uint16_t uint16_t;
typedef __uint32_t uint32_t;
typedef __uint64_t uint64_t;
typedef __int_least8_t int_least8_t;
typedef __int_least16_t int_least16_t;
typedef __int_least32_t int_least32_t;
typedef __int_least64_t int_least64_t;
typedef __uint_least8_t uint_least8_t;
typedef __uint_least16_t uint_least16_t;
typedef __uint_least32_t uint_least32_t;
typedef __uint_least64_t uint_least64_t;
typedef signed char int_fast8_t;
typedef long int_fast16_t;
typedef long int_fast32_t;
typedef long int_fast64_t;
typedef unsigned char uint_fast8_t;
typedef unsigned long uint_fast16_t;
typedef unsigned long uint_fast32_t;
typedef unsigned long uint_fast64_t;
typedef long intptr_t;
typedef unsigned long uintptr_t;
typedef __intmax_t intmax_t;
typedef __uintmax_t uintmax_t;
typedef _Bool bool;
struct bb_extent_scan_read_ret_type;
struct bb_extent_scan_read_ret_type {
    uint64_t field_0;
    uint64_t field_1;
    uint64_t field_2;
    uint64_t field_3;
};
extern void abort(void);
extern uint64_t revng_init_local_sp(uint64_t param_0);
extern uint64_t init_rbx(void);
extern uint64_t init_rbp(void);
extern void indirect_placeholder(void);
extern uint64_t init_cc_src2(void);
extern uint64_t helper_cc_compute_c_wrapper(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint32_t param_3);
extern uint64_t init_r13(void);
extern uint64_t init_r12(void);
extern uint64_t init_r14(void);
extern uint64_t init_r15(void);
extern uint64_t indirect_placeholder_2(uint64_t param_0, uint64_t param_1);
extern uint64_t init_r9(void);
extern uint64_t init_r8(void);
extern void indirect_placeholder_7(uint64_t param_0, uint64_t param_1);
extern uint64_t init_r10(void);
struct bb_extent_scan_read_ret_type bb_extent_scan_read(uint64_t rdi) {
    uint64_t rax_1;
    uint64_t var_0;
    uint64_t var_1;
    uint64_t var_2;
    uint64_t var_3;
    uint64_t var_4;
    uint64_t var_5;
    uint64_t var_6;
    uint64_t var_7;
    uint64_t var_8;
    uint64_t var_9;
    uint64_t var_10;
    uint64_t var_11;
    uint64_t *var_12;
    uint64_t var_13;
    uint64_t var_14;
    uint64_t *var_15;
    uint32_t *var_16;
    uint64_t *var_17;
    unsigned char *var_18;
    uint64_t r13_3;
    uint64_t r9_7;
    uint64_t rax_2;
    uint64_t r13_1;
    uint64_t r13_0;
    uint64_t rax_0;
    uint64_t local_sp_4;
    uint64_t r14_1;
    uint64_t r9_0;
    uint64_t rdx_0;
    uint64_t local_sp_0;
    uint64_t r14_0;
    uint64_t var_33;
    uint64_t *var_34;
    uint64_t var_35;
    uint64_t *var_36;
    uint64_t var_37;
    uint64_t rcx_0;
    uint64_t local_sp_1;
    uint32_t var_39;
    uint64_t var_40;
    uint32_t var_41;
    uint32_t var_42;
    uint32_t *var_43;
    uint64_t r9_1;
    uint64_t *var_44;
    uint64_t var_45;
    uint64_t var_46;
    uint64_t var_48;
    uint64_t rsi_0;
    uint64_t var_47;
    uint32_t rdi4_0;
    uint64_t r9_2;
    uint64_t var_49;
    uint64_t var_50;
    uint64_t var_51;
    uint64_t var_52;
    uint64_t rsi_1;
    uint64_t var_53;
    uint64_t var_54;
    uint64_t r9_3;
    uint64_t var_55;
    uint64_t var_56;
    uint64_t var_57;
    uint64_t r13_2;
    uint64_t var_38;
    uint64_t r9_4;
    uint64_t local_sp_2;
    uint64_t r14_2;
    uint64_t r8_0;
    uint32_t var_58;
    uint64_t var_59;
    bool var_60;
    unsigned char var_61;
    uint64_t var_62;
    uint64_t var_63;
    uint64_t var_64;
    uint64_t rax_4;
    uint64_t r14_3;
    uint64_t var_19;
    uint32_t var_20;
    uint64_t var_21;
    uint32_t var_22;
    uint64_t var_23;
    uint64_t r9_6;
    uint64_t var_24;
    uint64_t rax_3;
    uint64_t local_sp_3;
    uint64_t rdx_1;
    uint64_t var_26;
    uint64_t var_27;
    uint64_t var_28;
    uint64_t var_29;
    uint64_t var_30;
    uint64_t var_31;
    uint64_t var_32;
    uint64_t var_25;
    uint64_t var_65;
    uint64_t r8_2;
    struct bb_extent_scan_read_ret_type mrv;
    struct bb_extent_scan_read_ret_type mrv1;
    struct bb_extent_scan_read_ret_type mrv2;
    struct bb_extent_scan_read_ret_type mrv3;
    unsigned int loop_state_var;
    bool switch_state_var;
    var_0 = revng_init_local_sp(0UL);
    var_1 = init_r15();
    var_2 = init_r13();
    var_3 = init_rbx();
    var_4 = init_r10();
    var_5 = init_r12();
    var_6 = init_r9();
    var_7 = init_rbp();
    var_8 = init_cc_src2();
    var_9 = init_r14();
    var_10 = init_r8();
    *(uint64_t *)(var_0 + (-8L)) = var_1;
    *(uint64_t *)(var_0 + (-16L)) = var_9;
    *(uint64_t *)(var_0 + (-24L)) = var_2;
    *(uint64_t *)(var_0 + (-32L)) = var_5;
    *(uint64_t *)(var_0 + (-40L)) = var_7;
    *(uint64_t *)(var_0 + (-48L)) = var_3;
    var_11 = var_0 + (-4168L);
    var_12 = (uint64_t *)(rdi + 40UL);
    var_13 = *var_12;
    var_14 = var_0 + (-4120L);
    var_15 = (uint64_t *)(rdi + 8UL);
    var_16 = (uint32_t *)(rdi + 16UL);
    var_17 = (uint64_t *)(rdi + 24UL);
    var_18 = (unsigned char *)(rdi + 33UL);
    r13_3 = 0UL;
    r9_7 = var_6;
    rax_0 = 0UL;
    local_sp_4 = var_11;
    rax_4 = 1UL;
    r14_3 = var_13;
    r8_2 = var_10;
    while (1U)
        {
            *(uint64_t *)(local_sp_4 + (-8L)) = 4232363UL;
            indirect_placeholder();
            var_19 = *var_15;
            var_20 = *var_16;
            *(uint32_t *)(local_sp_4 + 32UL) = 72U;
            *(uint64_t *)(local_sp_4 + 8UL) = var_19;
            *(uint32_t *)(local_sp_4 + 24UL) = var_20;
            *(uint64_t *)(local_sp_4 + 16UL) = (var_19 ^ (-1L));
            var_21 = local_sp_4 + (-16L);
            *(uint64_t *)var_21 = 4232416UL;
            indirect_placeholder();
            var_22 = *(uint32_t *)(local_sp_4 + 20UL);
            var_23 = (uint64_t)var_22;
            r13_0 = r13_3;
            r9_0 = r9_7;
            r13_2 = r13_3;
            r9_4 = r9_7;
            r9_6 = r9_7;
            rax_3 = var_23;
            local_sp_3 = var_21;
            if (var_22 != 0U) {
                var_65 = *var_15;
                *var_18 = (unsigned char)'\x01';
                rax_4 = (var_65 != 0UL);
                loop_state_var = 1U;
                break;
            }
            var_24 = *var_17;
            rdx_1 = var_24;
            if (var_24 > (var_23 ^ (-1L))) {
                var_25 = local_sp_4 + (-24L);
                *(uint64_t *)var_25 = 4232929UL;
                indirect_placeholder();
                rax_3 = (uint64_t)*(uint32_t *)(local_sp_4 + 12UL);
                local_sp_3 = var_25;
                rdx_1 = *var_17;
            }
            var_26 = *var_12;
            var_27 = rax_3 + rdx_1;
            *var_17 = var_27;
            if (var_27 > 384307168202282325UL) {
                *(uint64_t *)(local_sp_3 + (-8L)) = 4233015UL;
                indirect_placeholder_7(r9_7, var_10);
                abort();
            }
            var_28 = r14_3 - var_26;
            var_29 = var_27 * 24UL;
            var_30 = local_sp_3 + (-8L);
            *(uint64_t *)var_30 = 4232501UL;
            var_31 = indirect_placeholder_2(var_26, var_29);
            var_32 = var_28 + var_31;
            *var_12 = var_31;
            local_sp_0 = var_30;
            r14_0 = var_32;
            local_sp_2 = var_30;
            r14_2 = var_32;
            if (*(uint32_t *)(local_sp_3 + 28UL) == 0U) {
                r13_3 = r13_2;
                r9_7 = r9_4;
                local_sp_4 = local_sp_2;
                r14_3 = r14_2;
                r9_6 = r9_4;
                if ((*(unsigned char *)(r14_2 + 16UL) & '\x01') != '\x00') {
                    *var_18 = (unsigned char)'\x01';
                    r8_0 = (uint64_t)(uint32_t)r13_2;
                    loop_state_var = 0U;
                    break;
                }
                var_58 = (uint32_t)r13_2;
                var_59 = (uint64_t)var_58;
                var_60 = (var_59 > 72UL);
                var_61 = *var_18;
                r8_0 = var_59;
                r8_2 = var_59;
                if (var_60) {
                    *var_17 = var_59;
                    if (var_61 != '\x00') {
                        loop_state_var = 1U;
                        break;
                    }
                    *var_15 = (*(uint64_t *)(r14_2 + 8UL) + *(uint64_t *)r14_2);
                    if ((uint64_t)(var_58 & (-8)) > 71UL) {
                        continue;
                    }
                    loop_state_var = 1U;
                    break;
                }
                if (var_61 != '\x00') {
                    loop_state_var = 0U;
                    break;
                }
                var_62 = (uint64_t)(var_58 + (-1));
                var_63 = *var_12;
                *var_17 = var_62;
                var_64 = (var_62 * 24UL) + var_63;
                *var_15 = (*(uint64_t *)(var_64 + (-16L)) + *(uint64_t *)(var_64 + (-24L)));
                loop_state_var = 1U;
                break;
            }
            while (1U)
                {
                    var_33 = (rax_0 * 56UL) + var_14;
                    var_34 = (uint64_t *)(var_33 + 16UL);
                    var_35 = *var_34;
                    var_36 = (uint64_t *)var_33;
                    var_37 = *var_36;
                    rax_1 = rax_0;
                    r13_1 = r13_0;
                    r14_1 = r14_0;
                    rdx_0 = var_37;
                    rcx_0 = var_35;
                    local_sp_1 = local_sp_0;
                    r9_1 = r9_0;
                    r9_2 = r9_0;
                    rax_4 = 0UL;
                    if (var_37 > (9223372036854775807UL - var_35)) {
                        *(uint32_t *)(local_sp_0 + 12UL) = (uint32_t)rax_0;
                        var_38 = local_sp_0 + (-8L);
                        *(uint64_t *)var_38 = 4232741UL;
                        indirect_placeholder();
                        rax_1 = (uint64_t)*(uint32_t *)(local_sp_0 + 4UL);
                        rcx_0 = *var_34;
                        local_sp_1 = var_38;
                        rdx_0 = *var_36;
                    }
                    var_39 = (uint32_t)r13_0;
                    var_40 = (uint64_t)var_39;
                    local_sp_0 = local_sp_1;
                    rax_2 = rax_1;
                    local_sp_2 = local_sp_1;
                    if (var_40 != 0UL) {
                        var_41 = *(uint32_t *)(var_33 + 40UL);
                        var_42 = var_41 & (-2);
                        var_43 = (uint32_t *)(r14_0 + 16UL);
                        rdi4_0 = var_41;
                        if ((uint64_t)(*var_43 - var_42) == 0UL) {
                            var_44 = (uint64_t *)(r14_0 + 8UL);
                            var_45 = *var_44;
                            var_46 = var_45 + *(uint64_t *)r14_0;
                            r9_1 = var_45;
                            rsi_0 = var_46;
                            r9_3 = var_45;
                            if (var_46 != rdx_0) {
                                var_48 = rcx_0 + var_45;
                                *var_43 = var_41;
                                *var_44 = var_48;
                                var_57 = (uint64_t)((uint32_t)rax_2 + 1U);
                                r13_0 = r13_1;
                                rax_0 = var_57;
                                r9_0 = r9_3;
                                r14_0 = r14_1;
                                r13_2 = r13_1;
                                r9_4 = r9_3;
                                r14_2 = r14_1;
                                if (var_57 < (uint64_t)*(uint32_t *)(local_sp_1 + 36UL)) {
                                    continue;
                                }
                                loop_state_var = 0U;
                                break;
                            }
                        }
                        rsi_0 = *(uint64_t *)r14_0 + *(uint64_t *)(r14_0 + 8UL);
                        var_47 = helper_cc_compute_c_wrapper(rdx_0 - rsi_0, rsi_0, var_8, 17U);
                        r9_2 = r9_1;
                        rsi_1 = rsi_0;
                        if (var_47 != 0UL) {
                            var_53 = rsi_1 - rdx_0;
                            var_54 = helper_cc_compute_c_wrapper(var_53 - rcx_0, rcx_0, var_8, 17U);
                            r9_3 = r9_2;
                            r9_6 = r9_2;
                            if (var_54 != 0UL) {
                                if (*var_15 != 0UL) {
                                    loop_state_var = 1U;
                                    break;
                                }
                                *(unsigned char *)(rdi + 32UL) = (unsigned char)'\x01';
                                loop_state_var = 1U;
                                break;
                            }
                            var_55 = rcx_0 - var_53;
                            *var_36 = rsi_1;
                            var_56 = (uint64_t)((uint32_t)rax_1 + (-1));
                            *var_34 = var_55;
                            rax_2 = var_56;
                            var_57 = (uint64_t)((uint32_t)rax_2 + 1U);
                            r13_0 = r13_1;
                            rax_0 = var_57;
                            r9_0 = r9_3;
                            r14_0 = r14_1;
                            r13_2 = r13_1;
                            r9_4 = r9_3;
                            r14_2 = r14_1;
                            if (var_57 < (uint64_t)*(uint32_t *)(local_sp_1 + 36UL)) {
                                continue;
                            }
                            loop_state_var = 0U;
                            break;
                        }
                    }
                    var_49 = *var_15;
                    rsi_1 = var_49;
                    if (var_49 <= rdx_0) {
                        var_53 = rsi_1 - rdx_0;
                        var_54 = helper_cc_compute_c_wrapper(var_53 - rcx_0, rcx_0, var_8, 17U);
                        r9_3 = r9_2;
                        r9_6 = r9_2;
                        if (var_54 != 0UL) {
                            if (*var_15 != 0UL) {
                                loop_state_var = 1U;
                                break;
                            }
                            *(unsigned char *)(rdi + 32UL) = (unsigned char)'\x01';
                            loop_state_var = 1U;
                            break;
                        }
                        var_55 = rcx_0 - var_53;
                        *var_36 = rsi_1;
                        var_56 = (uint64_t)((uint32_t)rax_1 + (-1));
                        *var_34 = var_55;
                        rax_2 = var_56;
                        var_57 = (uint64_t)((uint32_t)rax_2 + 1U);
                        r13_0 = r13_1;
                        rax_0 = var_57;
                        r9_0 = r9_3;
                        r14_0 = r14_1;
                        r13_2 = r13_1;
                        r9_4 = r9_3;
                        r14_2 = r14_1;
                        if (var_57 < (uint64_t)*(uint32_t *)(local_sp_1 + 36UL)) {
                            continue;
                        }
                        loop_state_var = 0U;
                        break;
                    }
                    rdi4_0 = *(uint32_t *)(var_33 + 40UL);
                }
            switch_state_var = 0;
            switch (loop_state_var) {
              case 0U:
                {
                    break;
                }
                break;
              case 1U:
                {
                    loop_state_var = 1U;
                    switch_state_var = 1;
                    break;
                }
                break;
            }
            if (switch_state_var)
                break;
        }
    switch (loop_state_var) {
      case 0U:
        {
            *var_17 = r8_0;
            r8_2 = r8_0;
        }
        break;
      case 1U:
        {
            mrv.field_0 = rax_4;
            mrv1 = mrv;
            mrv1.field_1 = var_4;
            mrv2 = mrv1;
            mrv2.field_2 = r9_6;
            mrv3 = mrv2;
            mrv3.field_3 = r8_2;
            return mrv3;
        }
        break;
    }
}

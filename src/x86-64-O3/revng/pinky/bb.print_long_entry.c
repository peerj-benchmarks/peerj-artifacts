typedef unsigned char __u_char;
typedef unsigned short __u_short;
typedef unsigned int __u_int;
typedef unsigned long __u_long;
typedef signed char __int8_t;
typedef unsigned char __uint8_t;
typedef short __int16_t;
typedef unsigned short __uint16_t;
typedef int __int32_t;
typedef unsigned int __uint32_t;
typedef long __int64_t;
typedef unsigned long __uint64_t;
typedef __int8_t __int_least8_t;
typedef __uint8_t __uint_least8_t;
typedef __int16_t __int_least16_t;
typedef __uint16_t __uint_least16_t;
typedef __int32_t __int_least32_t;
typedef __uint32_t __uint_least32_t;
typedef __int64_t __int_least64_t;
typedef __uint64_t __uint_least64_t;
typedef long __quad_t;
typedef unsigned long __u_quad_t;
typedef long __intmax_t;
typedef unsigned long __uintmax_t;
typedef unsigned long __dev_t;
typedef unsigned int __uid_t;
typedef unsigned int __gid_t;
typedef unsigned long __ino_t;
typedef unsigned long __ino64_t;
typedef unsigned int __mode_t;
typedef unsigned long __nlink_t;
typedef long __off_t;
typedef long __off64_t;
typedef int __pid_t;
typedef struct {
    int __val[2];
} __fsid_t;
typedef long __clock_t;
typedef unsigned long __rlim_t;
typedef unsigned long __rlim64_t;
typedef unsigned int __id_t;
typedef long __time_t;
typedef unsigned int __useconds_t;
typedef long __suseconds_t;
typedef int __daddr_t;
typedef int __key_t;
typedef int __clockid_t;
typedef void *__timer_t;
typedef long __blksize_t;
typedef long __blkcnt_t;
typedef long __blkcnt64_t;
typedef unsigned long __fsblkcnt_t;
typedef unsigned long __fsblkcnt64_t;
typedef unsigned long __fsfilcnt_t;
typedef unsigned long __fsfilcnt64_t;
typedef long __fsword_t;
typedef long __ssize_t;
typedef long __syscall_slong_t;
typedef unsigned long __syscall_ulong_t;
typedef __off64_t __loff_t;
typedef char *__caddr_t;
typedef long __intptr_t;
typedef unsigned int __socklen_t;
typedef int __sig_atomic_t;
typedef __int8_t int8_t;
typedef __int16_t int16_t;
typedef __int32_t int32_t;
typedef __int64_t int64_t;
typedef __uint8_t uint8_t;
typedef __uint16_t uint16_t;
typedef __uint32_t uint32_t;
typedef __uint64_t uint64_t;
typedef __int_least8_t int_least8_t;
typedef __int_least16_t int_least16_t;
typedef __int_least32_t int_least32_t;
typedef __int_least64_t int_least64_t;
typedef __uint_least8_t uint_least8_t;
typedef __uint_least16_t uint_least16_t;
typedef __uint_least32_t uint_least32_t;
typedef __uint_least64_t uint_least64_t;
typedef signed char int_fast8_t;
typedef long int_fast16_t;
typedef long int_fast32_t;
typedef long int_fast64_t;
typedef unsigned char uint_fast8_t;
typedef unsigned long uint_fast16_t;
typedef unsigned long uint_fast32_t;
typedef unsigned long uint_fast64_t;
typedef long intptr_t;
typedef unsigned long uintptr_t;
typedef __intmax_t intmax_t;
typedef __uintmax_t uintmax_t;
typedef _Bool bool;
struct indirect_placeholder_15_ret_type;
struct indirect_placeholder_16_ret_type;
struct indirect_placeholder_15_ret_type {
    uint64_t field_0;
    uint64_t field_1;
};
struct indirect_placeholder_16_ret_type {
    uint64_t field_0;
    uint64_t field_1;
};
extern uint64_t revng_init_local_sp(uint64_t param_0);
extern uint64_t init_rbx(void);
extern uint64_t init_rbp(void);
extern void indirect_placeholder(void);
extern uint64_t init_r13(void);
extern uint64_t init_r12(void);
extern uint64_t init_r14(void);
extern uint64_t init_rax(void);
extern uint64_t indirect_placeholder_1(uint64_t param_0);
extern uint64_t indirect_placeholder_3(uint64_t param_0, uint64_t param_1);
extern struct indirect_placeholder_15_ret_type indirect_placeholder_15(uint64_t param_0);
extern struct indirect_placeholder_16_ret_type indirect_placeholder_16(uint64_t param_0);
void bb_print_long_entry(uint64_t rdi) {
    uint64_t rax_0;
    uint64_t var_0;
    uint64_t var_1;
    uint64_t var_2;
    uint64_t var_3;
    uint64_t var_4;
    uint64_t var_5;
    uint64_t var_6;
    bool var_7;
    uint64_t *var_8;
    uint64_t rax_2;
    uint64_t var_23;
    uint64_t local_sp_6;
    uint64_t local_sp_1;
    uint64_t var_24;
    uint64_t local_sp_5;
    uint64_t var_16;
    uint64_t var_17;
    uint64_t local_sp_7;
    uint64_t local_sp_3;
    uint64_t var_18;
    uint64_t local_sp_4;
    uint64_t var_19;
    struct indirect_placeholder_15_ret_type var_20;
    uint64_t var_21;
    uint64_t var_22;
    struct indirect_placeholder_16_ret_type var_13;
    uint64_t var_14;
    uint64_t var_15;
    uint64_t var_12;
    uint64_t var_9;
    uint64_t var_10;
    uint64_t var_11;
    var_0 = revng_init_local_sp(0UL);
    var_1 = init_rax();
    var_2 = init_r13();
    var_3 = init_rbx();
    var_4 = init_r12();
    var_5 = init_rbp();
    var_6 = init_r14();
    *(uint64_t *)(var_0 + (-8L)) = var_6;
    *(uint64_t *)(var_0 + (-16L)) = var_2;
    *(uint64_t *)(var_0 + (-24L)) = var_4;
    *(uint64_t *)(var_0 + (-32L)) = var_5;
    *(uint64_t *)(var_0 + (-40L)) = var_3;
    *(uint64_t *)(var_0 + (-1072L)) = 4203111UL;
    indirect_placeholder();
    *(uint64_t *)(var_0 + (-1080L)) = 4203126UL;
    indirect_placeholder();
    *(uint64_t *)(var_0 + (-1088L)) = 4203141UL;
    indirect_placeholder();
    *(uint64_t *)(var_0 + (-1096L)) = 4203153UL;
    indirect_placeholder();
    var_7 = (var_1 == 0UL);
    var_8 = (uint64_t *)(var_0 + (-1104L));
    rax_0 = 0UL;
    rax_2 = 0UL;
    if (var_7) {
        *var_8 = 4203753UL;
        indirect_placeholder();
        return;
    }
    *var_8 = 4203176UL;
    indirect_placeholder();
    var_9 = *(uint64_t *)(var_1 + 24UL);
    var_10 = *(uint64_t *)var_1;
    *(uint64_t *)(var_0 + (-1112L)) = 4203197UL;
    indirect_placeholder_3(var_9, var_10);
    *(uint64_t *)(var_0 + (-1120L)) = 4203215UL;
    indirect_placeholder();
    *(uint64_t *)(var_0 + (-1128L)) = 4203223UL;
    indirect_placeholder();
    var_11 = var_0 + (-1136L);
    *(uint64_t *)var_11 = 4203233UL;
    indirect_placeholder();
    local_sp_4 = var_11;
    local_sp_7 = var_11;
    if (*(unsigned char *)6370498UL == '\x00') {
        if (*(unsigned char *)6370500UL == '\x00') {
            local_sp_5 = local_sp_4;
            local_sp_6 = local_sp_4;
            if (*(unsigned char *)6370499UL == '\x00') {
                *(uint64_t *)(local_sp_5 + (-8L)) = 4203274UL;
                indirect_placeholder();
                return;
            }
        }
        *(uint64_t *)(local_sp_7 + (-8L)) = 4203380UL;
        indirect_placeholder();
        *(uint64_t *)(local_sp_7 + (-16L)) = 4203393UL;
        indirect_placeholder();
        *(uint64_t *)(local_sp_7 + (-24L)) = 4203403UL;
        var_13 = indirect_placeholder_16(1UL);
        var_14 = var_13.field_0;
        *(uint64_t *)(local_sp_7 + (-32L)) = 4203418UL;
        indirect_placeholder();
        *(uint64_t *)(local_sp_7 + (-40L)) = 4203431UL;
        indirect_placeholder();
        var_15 = local_sp_7 + (-48L);
        *(uint64_t *)var_15 = 4203444UL;
        indirect_placeholder();
        local_sp_3 = var_15;
        if (var_14 == 0UL) {
            *(uint64_t *)(local_sp_7 + (-56L)) = 4203467UL;
            indirect_placeholder();
            *(uint64_t *)(local_sp_7 + (-64L)) = 4203520UL;
            indirect_placeholder();
            var_16 = local_sp_7 + (-72L);
            *(uint64_t *)var_16 = 4203533UL;
            var_17 = indirect_placeholder_1(var_14);
            local_sp_3 = var_16;
            rax_0 = var_17;
        }
        var_18 = local_sp_3 + (-8L);
        *(uint64_t *)var_18 = 4203541UL;
        indirect_placeholder();
        local_sp_5 = var_18;
        local_sp_6 = var_18;
        rax_2 = rax_0;
        if (*(unsigned char *)6370499UL == '\x00') {
            *(uint64_t *)(local_sp_5 + (-8L)) = 4203274UL;
            indirect_placeholder();
            return;
        }
    }
    *(uint64_t *)(var_0 + (-1144L)) = 4203308UL;
    indirect_placeholder();
    *(uint64_t *)(var_0 + (-1152L)) = 4203324UL;
    indirect_placeholder();
    *(uint64_t *)(var_0 + (-1160L)) = 4203336UL;
    indirect_placeholder();
    *(uint64_t *)(var_0 + (-1168L)) = 4203352UL;
    indirect_placeholder();
    var_12 = var_0 + (-1176L);
    *(uint64_t *)var_12 = 4203362UL;
    indirect_placeholder();
    local_sp_4 = var_12;
    local_sp_7 = var_12;
    if (*(unsigned char *)6370500UL == '\x00') {
        *(uint64_t *)(local_sp_7 + (-8L)) = 4203380UL;
        indirect_placeholder();
        *(uint64_t *)(local_sp_7 + (-16L)) = 4203393UL;
        indirect_placeholder();
        *(uint64_t *)(local_sp_7 + (-24L)) = 4203403UL;
        var_13 = indirect_placeholder_16(1UL);
        var_14 = var_13.field_0;
        *(uint64_t *)(local_sp_7 + (-32L)) = 4203418UL;
        indirect_placeholder();
        *(uint64_t *)(local_sp_7 + (-40L)) = 4203431UL;
        indirect_placeholder();
        var_15 = local_sp_7 + (-48L);
        *(uint64_t *)var_15 = 4203444UL;
        indirect_placeholder();
        local_sp_3 = var_15;
        if (var_14 == 0UL) {
            *(uint64_t *)(local_sp_7 + (-56L)) = 4203467UL;
            indirect_placeholder();
            *(uint64_t *)(local_sp_7 + (-64L)) = 4203520UL;
            indirect_placeholder();
            var_16 = local_sp_7 + (-72L);
            *(uint64_t *)var_16 = 4203533UL;
            var_17 = indirect_placeholder_1(var_14);
            local_sp_3 = var_16;
            rax_0 = var_17;
        }
        var_18 = local_sp_3 + (-8L);
        *(uint64_t *)var_18 = 4203541UL;
        indirect_placeholder();
        local_sp_5 = var_18;
        local_sp_6 = var_18;
        rax_2 = rax_0;
        if (*(unsigned char *)6370499UL == '\x00') {
            *(uint64_t *)(local_sp_5 + (-8L)) = 4203274UL;
            indirect_placeholder();
            return;
        }
    }
    local_sp_5 = local_sp_4;
    local_sp_6 = local_sp_4;
    if (*(unsigned char *)6370499UL == '\x00') {
        *(uint64_t *)(local_sp_5 + (-8L)) = 4203274UL;
        indirect_placeholder();
        return;
    }
    *(uint64_t *)(local_sp_6 + (-8L)) = 4203563UL;
    indirect_placeholder();
    *(uint64_t *)(local_sp_6 + (-16L)) = 4203576UL;
    indirect_placeholder();
    var_19 = (rax_2 << 1UL) | 1UL;
    *(uint64_t *)(local_sp_6 + (-24L)) = 4203586UL;
    var_20 = indirect_placeholder_15(var_19);
    var_21 = var_20.field_0;
    *(uint64_t *)(local_sp_6 + (-32L)) = 4203601UL;
    indirect_placeholder();
    *(uint64_t *)(local_sp_6 + (-40L)) = 4203614UL;
    indirect_placeholder();
    var_22 = local_sp_6 + (-48L);
    *(uint64_t *)var_22 = 4203627UL;
    indirect_placeholder();
    local_sp_1 = var_22;
    if (var_21 == 0UL) {
        *(uint64_t *)(local_sp_6 + (-56L)) = 4203650UL;
        indirect_placeholder();
        *(uint64_t *)(local_sp_6 + (-64L)) = 4203704UL;
        indirect_placeholder();
        var_23 = local_sp_6 + (-72L);
        *(uint64_t *)var_23 = 4203717UL;
        indirect_placeholder_1(var_21);
        local_sp_1 = var_23;
    }
    var_24 = local_sp_1 + (-8L);
    *(uint64_t *)var_24 = 4203725UL;
    indirect_placeholder();
    local_sp_5 = var_24;
    *(uint64_t *)(local_sp_5 + (-8L)) = 4203274UL;
    indirect_placeholder();
    return;
}

typedef unsigned char __u_char;
typedef unsigned short __u_short;
typedef unsigned int __u_int;
typedef unsigned long __u_long;
typedef signed char __int8_t;
typedef unsigned char __uint8_t;
typedef short __int16_t;
typedef unsigned short __uint16_t;
typedef int __int32_t;
typedef unsigned int __uint32_t;
typedef long __int64_t;
typedef unsigned long __uint64_t;
typedef __int8_t __int_least8_t;
typedef __uint8_t __uint_least8_t;
typedef __int16_t __int_least16_t;
typedef __uint16_t __uint_least16_t;
typedef __int32_t __int_least32_t;
typedef __uint32_t __uint_least32_t;
typedef __int64_t __int_least64_t;
typedef __uint64_t __uint_least64_t;
typedef long __quad_t;
typedef unsigned long __u_quad_t;
typedef long __intmax_t;
typedef unsigned long __uintmax_t;
typedef unsigned long __dev_t;
typedef unsigned int __uid_t;
typedef unsigned int __gid_t;
typedef unsigned long __ino_t;
typedef unsigned long __ino64_t;
typedef unsigned int __mode_t;
typedef unsigned long __nlink_t;
typedef long __off_t;
typedef long __off64_t;
typedef int __pid_t;
typedef struct {
    int __val[2];
} __fsid_t;
typedef long __clock_t;
typedef unsigned long __rlim_t;
typedef unsigned long __rlim64_t;
typedef unsigned int __id_t;
typedef long __time_t;
typedef unsigned int __useconds_t;
typedef long __suseconds_t;
typedef int __daddr_t;
typedef int __key_t;
typedef int __clockid_t;
typedef void *__timer_t;
typedef long __blksize_t;
typedef long __blkcnt_t;
typedef long __blkcnt64_t;
typedef unsigned long __fsblkcnt_t;
typedef unsigned long __fsblkcnt64_t;
typedef unsigned long __fsfilcnt_t;
typedef unsigned long __fsfilcnt64_t;
typedef long __fsword_t;
typedef long __ssize_t;
typedef long __syscall_slong_t;
typedef unsigned long __syscall_ulong_t;
typedef __off64_t __loff_t;
typedef char *__caddr_t;
typedef long __intptr_t;
typedef unsigned int __socklen_t;
typedef int __sig_atomic_t;
typedef __int8_t int8_t;
typedef __int16_t int16_t;
typedef __int32_t int32_t;
typedef __int64_t int64_t;
typedef __uint8_t uint8_t;
typedef __uint16_t uint16_t;
typedef __uint32_t uint32_t;
typedef __uint64_t uint64_t;
typedef __int_least8_t int_least8_t;
typedef __int_least16_t int_least16_t;
typedef __int_least32_t int_least32_t;
typedef __int_least64_t int_least64_t;
typedef __uint_least8_t uint_least8_t;
typedef __uint_least16_t uint_least16_t;
typedef __uint_least32_t uint_least32_t;
typedef __uint_least64_t uint_least64_t;
typedef signed char int_fast8_t;
typedef long int_fast16_t;
typedef long int_fast32_t;
typedef long int_fast64_t;
typedef unsigned char uint_fast8_t;
typedef unsigned long uint_fast16_t;
typedef unsigned long uint_fast32_t;
typedef unsigned long uint_fast64_t;
typedef long intptr_t;
typedef unsigned long uintptr_t;
typedef __intmax_t intmax_t;
typedef __uintmax_t uintmax_t;
typedef _Bool bool;
extern uint64_t revng_init_local_sp(uint64_t param_0);
extern void indirect_placeholder(void);
extern uint64_t init_rbx(void);
extern uint64_t init_rbp(void);
extern uint64_t init_cc_src2(void);
extern uint64_t init_r14(void);
extern uint64_t init_r13(void);
extern uint64_t init_r12(void);
extern uint64_t helper_cc_compute_all_wrapper(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint32_t param_3);
extern uint64_t init_r15(void);
uint64_t bb_rpl_mktime(uint64_t rdi) {
    uint64_t rbp_1;
    uint64_t var_0;
    uint64_t var_1;
    uint64_t var_2;
    uint64_t var_3;
    uint64_t var_4;
    uint64_t var_5;
    uint64_t var_6;
    uint64_t var_7;
    uint64_t var_8;
    uint32_t var_9;
    uint32_t var_10;
    uint64_t var_11;
    uint64_t var_12;
    uint32_t var_13;
    uint32_t var_14;
    uint64_t var_15;
    uint32_t *var_16;
    uint64_t var_17;
    uint32_t var_18;
    uint64_t var_19;
    uint64_t var_20;
    uint64_t *var_21;
    bool var_22;
    uint64_t rax_3;
    uint64_t r13_3;
    uint64_t r13_4;
    uint64_t local_sp_19;
    uint64_t r15_1;
    uint64_t local_sp_2;
    uint64_t var_91;
    uint64_t var_92;
    uint64_t var_93;
    uint64_t local_sp_3;
    uint64_t storemerge4;
    uint64_t rdx_0;
    uint32_t var_94;
    uint64_t var_95;
    uint64_t local_sp_16;
    uint64_t var_102;
    uint64_t var_103;
    uint64_t var_104;
    uint32_t var_105;
    uint32_t var_106;
    uint32_t var_107;
    uint64_t var_108;
    uint32_t var_109;
    uint64_t var_110;
    uint64_t var_111;
    uint64_t storemerge5;
    uint64_t var_112;
    uint64_t rdx_5;
    uint64_t var_118;
    uint64_t var_119;
    uint64_t r15_4;
    bool var_113;
    uint64_t *var_114;
    uint64_t r15_2;
    uint64_t var_115;
    uint64_t var_116;
    uint64_t var_117;
    uint64_t rbx_0;
    uint64_t var_52;
    uint64_t var_53;
    uint32_t var_54;
    uint32_t var_55;
    uint32_t var_56;
    uint64_t var_57;
    uint32_t var_58;
    uint64_t rdx_13;
    uint64_t var_59;
    uint64_t var_60;
    uint64_t storemerge1;
    uint64_t var_61;
    uint64_t rax_4;
    uint64_t rax_2;
    uint64_t var_62;
    uint64_t var_63;
    uint64_t var_64;
    uint64_t var_65;
    uint64_t rax_1;
    uint64_t rdx_4;
    uint64_t _pre_phi396;
    uint64_t *var_66;
    uint64_t local_sp_21;
    uint64_t rdx_6;
    uint32_t var_67;
    uint32_t var_68;
    uint64_t *_pre_phi395;
    uint64_t var_69;
    unsigned char var_70;
    uint64_t var_71;
    bool var_72;
    uint64_t *var_73;
    uint64_t r12_1;
    uint64_t var_74;
    uint64_t storemerge2;
    uint32_t var_75;
    uint32_t *var_76;
    uint32_t var_77;
    uint32_t var_78;
    uint64_t *var_79;
    uint64_t var_80;
    unsigned char *var_81;
    unsigned char var_82;
    uint64_t var_83;
    uint64_t var_84;
    uint32_t var_85;
    uint64_t r14_1;
    uint64_t rdx_11;
    uint64_t local_sp_13;
    uint64_t rdx_8;
    uint64_t var_86;
    uint64_t var_87;
    uint64_t r13_2;
    uint64_t r13_0;
    uint64_t rbp_2;
    uint64_t local_sp_14;
    uint64_t rdx_9;
    uint64_t var_88;
    uint64_t rbp_4;
    uint64_t r13_1;
    uint64_t local_sp_17;
    uint64_t rbp_3;
    uint64_t rdx_12;
    uint64_t local_sp_15;
    uint64_t rdx_10;
    uint64_t var_89;
    uint64_t var_90;
    uint64_t var_96;
    uint32_t *var_97;
    uint32_t var_101;
    uint64_t var_98;
    uint64_t var_99;
    uint64_t var_100;
    uint64_t *var_120;
    uint64_t var_121;
    uint32_t var_122;
    uint64_t var_123;
    uint32_t var_124;
    uint64_t var_125;
    uint64_t var_126;
    uint64_t storemerge3;
    uint64_t var_127;
    uint64_t var_128;
    uint64_t var_129;
    uint64_t r15_5;
    uint64_t local_sp_20;
    uint64_t var_23;
    uint64_t var_24;
    uint64_t *var_25;
    uint32_t var_26;
    bool var_27;
    uint64_t *var_28;
    uint32_t *var_29;
    uint64_t var_30;
    uint64_t var_31;
    uint32_t var_32;
    uint32_t var_33;
    uint32_t var_34;
    uint64_t var_35;
    uint64_t var_36;
    uint32_t var_37;
    uint64_t var_38;
    uint64_t var_39;
    uint32_t *var_40;
    uint32_t var_41;
    uint64_t var_42;
    uint32_t *var_43;
    uint32_t var_44;
    uint64_t var_45;
    uint64_t var_46;
    uint64_t var_47;
    uint64_t var_48;
    uint64_t var_49;
    uint64_t var_50;
    uint64_t var_51;
    unsigned int loop_state_var;
    bool switch_state_var;
    var_0 = revng_init_local_sp(0UL);
    var_1 = init_r14();
    var_2 = init_r15();
    var_3 = init_r13();
    var_4 = init_rbx();
    var_5 = init_r12();
    var_6 = init_rbp();
    var_7 = init_cc_src2();
    *(uint64_t *)(var_0 + (-8L)) = var_2;
    *(uint64_t *)(var_0 + (-16L)) = var_1;
    *(uint64_t *)(var_0 + (-24L)) = var_3;
    *(uint64_t *)(var_0 + (-32L)) = var_5;
    *(uint64_t *)(var_0 + (-40L)) = var_6;
    *(uint64_t *)(var_0 + (-48L)) = var_4;
    var_8 = var_0 + (-336L);
    *(uint64_t *)var_8 = 4253705UL;
    indirect_placeholder();
    var_9 = *(uint32_t *)rdi;
    var_10 = *(uint32_t *)(rdi + 16UL);
    var_11 = (uint64_t)var_10;
    var_12 = (uint64_t)*(uint32_t *)(rdi + 20UL);
    var_13 = *(uint32_t *)(rdi + 4UL);
    var_14 = *(uint32_t *)(rdi + 8UL);
    var_15 = (uint64_t)*(uint32_t *)(rdi + 12UL);
    var_16 = (uint32_t *)(var_0 + (-240L));
    *var_16 = var_9;
    *(uint32_t *)(var_0 + (-256L)) = *(uint32_t *)(rdi + 32UL);
    var_17 = (uint64_t)((long)((uint64_t)var_10 * 715827883UL) >> (long)33UL) - (uint64_t)((long)(var_11 << 32UL) >> (long)63UL);
    var_18 = ((uint32_t)var_17 * 4294967284U) + var_10;
    var_19 = ((uint64_t)var_18 >> 31UL) & 1UL;
    var_20 = (var_17 - var_19) + var_12;
    var_21 = (uint64_t *)(var_0 + (-328L));
    *var_21 = var_20;
    var_22 = ((var_20 & 3UL) == 0UL);
    rax_3 = 0UL;
    r15_2 = 9223372036854775807UL;
    rbx_0 = 6UL;
    rax_1 = 9223372036854775807UL;
    local_sp_21 = var_8;
    storemerge2 = 18446744073709551615UL;
    r14_1 = 601200UL;
    r13_2 = 4294967295UL;
    r13_0 = 4294967295UL;
    var_23 = (uint64_t)((long)(var_20 + (uint64_t)(((unsigned __int128)var_20 * 18446744073709551615ULL) >> 64ULL)) >> (long)6UL) - (uint64_t)((long)var_20 >> (long)63UL);
    rax_3 = 1UL;
    if (!var_22 && var_20 == (var_23 * 100UL)) {
        rax_3 = ((var_23 & 3UL) == 1UL);
    }
    var_24 = (uint64_t)((long)(((uint64_t)*(uint16_t *)((((rax_3 * 13UL) + (uint64_t)((long)((uint64_t)(((uint32_t)var_19 * 12U) + var_18) << 32UL) >> (long)32UL)) << 1UL) + 4327104UL) << 32UL) + (-4294967296L)) >> (long)32UL) + var_15;
    var_25 = (uint64_t *)(var_0 + (-312L));
    *var_25 = var_24;
    var_26 = *var_16;
    var_27 = ((int)var_26 < (int)0U);
    var_28 = (uint64_t *)(var_0 + (-320L));
    if (var_27) {
        *var_28 = 0UL;
        *(uint32_t *)(var_0 + (-236L)) = 0U;
    } else {
        *var_28 = 59UL;
        var_29 = (uint32_t *)(var_0 + (-236L));
        *var_29 = 59U;
        if ((int)var_26 > (int)59U) {
            var_30 = (uint64_t)var_26;
            *var_29 = var_26;
            *var_28 = var_30;
        }
    }
    var_31 = *var_21;
    var_32 = *(uint32_t *)6437816UL;
    *(uint32_t *)(var_0 + (-268L)) = 0U;
    *(uint64_t *)(var_0 + (-216L)) = rdi;
    var_33 = 0U - var_32;
    var_34 = ((uint32_t)(var_31 >> 2UL) + 475U) + (uint32_t)var_22;
    var_35 = (uint64_t)var_34;
    *(uint32_t *)(var_0 + (-292L)) = var_34;
    var_36 = var_35 << 32UL;
    var_37 = (uint32_t)(uint64_t)((long)((uint64_t)((long)var_36 >> (long)32UL) * 1374389535UL) >> (long)35UL) - (uint32_t)(uint64_t)((long)var_36 >> (long)63UL);
    var_38 = (uint64_t)var_37 - (((uint64_t)(var_34 + ((var_37 * 5U) * 4294967291U)) >> 31UL) & 1UL);
    var_39 = var_38 << 32UL;
    var_40 = (uint32_t *)(var_0 + (-272L));
    var_41 = (uint32_t)var_38;
    *var_40 = var_41;
    var_42 = (uint64_t)((long)var_39 >> (long)34UL);
    *(uint64_t *)(var_0 + (-248L)) = var_42;
    var_43 = (uint32_t *)(var_0 + (-264L));
    var_44 = (uint32_t)var_42;
    *var_43 = var_44;
    var_45 = (uint64_t)var_14;
    *(uint64_t *)(var_0 + (-288L)) = var_45;
    var_46 = (uint64_t)var_13;
    *(uint64_t *)(var_0 + (-280L)) = var_46;
    var_47 = (uint64_t)var_33;
    *(uint64_t *)(var_0 + (-224L)) = var_47;
    var_48 = (((((uint64_t)((long)(((uint64_t)(var_44 + (var_34 - var_41)) << 32UL) + (-2048699400192L)) >> (long)32UL) + (*var_25 + ((var_31 * 365UL) + (-25550L)))) * 24UL) + var_45) * 60UL) + var_46;
    var_49 = var_48 << 2UL;
    var_50 = ((var_48 * 60UL) + *var_28) - var_47;
    *(uint64_t *)(var_0 + (-232L)) = var_50;
    *(uint64_t *)(var_0 + (-304L)) = var_50;
    rax_4 = var_50;
    r12_1 = var_50;
    rdx_13 = var_49;
    while (1U)
        {
            *(uint64_t *)(local_sp_21 + 208UL) = rax_4;
            var_51 = local_sp_21 + (-8L);
            *(uint64_t *)var_51 = 4254371UL;
            indirect_placeholder();
            local_sp_19 = var_51;
            r15_4 = rax_4;
            rdx_4 = rdx_13;
            local_sp_21 = var_51;
            r12_1 = rax_4;
            local_sp_13 = var_51;
            if (rax_4 != 0UL) {
                rax_2 = rax_1;
                rdx_5 = rdx_4;
                if (rax_1 == rax_4) {
                    var_66 = (uint64_t *)(var_51 + 32UL);
                    rdx_6 = rdx_5;
                    rax_4 = rax_2;
                    if (!((*var_66 != rax_4) || (r12_1 == rax_4))) {
                        var_67 = *(uint32_t *)(var_51 + 176UL);
                        if ((int)var_67 >= (int)0U) {
                            loop_state_var = 4U;
                            break;
                        }
                        var_68 = *(uint32_t *)(var_51 + 80UL);
                        if ((int)var_68 >= (int)0U) {
                            var_69 = helper_cc_compute_all_wrapper((uint64_t)var_68, rax_4, var_7, 24U);
                            var_70 = (unsigned char)((var_69 >> 6UL) & 1UL) ^ '\x01';
                            var_71 = (var_67 != 0U);
                            rdx_6 = (uint64_t)(var_67 & (-256)) | var_71;
                            if ((uint64_t)(var_70 - (unsigned char)var_71) != 0UL) {
                                loop_state_var = 4U;
                                break;
                            }
                        }
                        var_72 = (var_67 != 0U);
                        rdx_6 = var_72;
                        if ((long)(var_72 ? 4294967296UL : 0UL) >= (long)((uint64_t)*(uint32_t *)(var_51 + 68UL) << 32UL)) {
                            loop_state_var = 4U;
                            break;
                        }
                    }
                    var_74 = (uint64_t)((uint32_t)rbx_0 + (-1));
                    rbx_0 = var_74;
                    rdx_13 = rdx_6;
                    if (var_74 != 0UL) {
                        loop_state_var = 0U;
                        break;
                    }
                    var_75 = *(uint32_t *)(var_51 + 176UL);
                    *var_66 = r12_1;
                    *(uint32_t *)(var_51 + 68UL) = (var_75 != 0U);
                    continue;
                }
                var_76 = (uint32_t *)(var_51 + 80UL);
                var_77 = *var_76;
                var_78 = *(uint32_t *)(var_51 + 176UL);
                var_79 = (uint64_t *)(var_51 + 120UL);
                var_80 = *var_79;
                var_81 = (unsigned char *)(var_51 + 68UL);
                var_82 = (var_77 == 0U);
                *var_81 = var_82;
                var_83 = (var_78 == 0U);
                var_84 = (rdx_4 & (-256L)) | var_83;
                rdx_8 = var_84;
                _pre_phi395 = var_79;
                r13_3 = var_80;
                if (((uint64_t)(var_82 - (unsigned char)var_83) == 0UL) || ((int)var_78 < (int)0U)) {
                    loop_state_var = 3U;
                    break;
                }
                var_85 = *var_76;
                *var_79 = rax_4;
                if ((int)var_85 < (int)0U) {
                    loop_state_var = 3U;
                    break;
                }
                *(uint64_t *)(var_51 + 72UL) = var_80;
                loop_state_var = 2U;
                while (1U)
                    {
                        switch (loop_state_var) {
                          case 2U:
                            {
                                var_86 = 0UL - r14_1;
                                *(uint32_t *)(local_sp_13 + 32UL) = 2U;
                                var_87 = (uint64_t)((long)(var_86 << 32UL) >> (long)32UL);
                                rbp_2 = var_87;
                                local_sp_14 = local_sp_13;
                                rdx_9 = rdx_8;
                                rbp_4 = var_87;
                                local_sp_17 = local_sp_13;
                                rdx_12 = rdx_8;
                                if ((int)(uint32_t)var_86 >= (int)0U) {
                                    var_100 = 9223372036854775807UL - rbp_4;
                                    storemerge4 = (var_100 & (-256L)) | ((long)var_100 < (long)rax_4);
                                    r13_1 = r13_2;
                                    rbp_3 = rbp_4;
                                    local_sp_15 = local_sp_17;
                                    rdx_10 = rdx_12;
                                    loop_state_var = 1U;
                                    continue;
                                }
                                var_88 = 9223372036854775808UL - (-9223372036854775808L);
                                storemerge4 = (var_88 & (-256L)) | ((long)var_88 > (long)rax_4);
                                r13_1 = r13_0;
                                rbp_3 = rbp_2;
                                local_sp_15 = local_sp_14;
                                rdx_10 = rdx_9;
                            }
                            break;
                          case 1U:
                          case 0U:
                            {
                                var_88 = 9223372036854775808UL - (-9223372036854775808L);
                                storemerge4 = (var_88 & (-256L)) | ((long)var_88 > (long)rax_4);
                                r13_1 = r13_0;
                                rbp_3 = rbp_2;
                                local_sp_15 = local_sp_14;
                                rdx_10 = rdx_9;
                            }
                            break;
                        }
                    }
                switch_state_var = 0;
                switch (loop_state_var) {
                  case 0U:
                    {
                        loop_state_var = 2U;
                        switch_state_var = 1;
                        break;
                    }
                    break;
                  case 1U:
                    {
                        var_102 = (uint64_t)*(uint32_t *)(local_sp_3 + 228UL);
                        var_103 = *(uint64_t *)(local_sp_3 + 72UL);
                        var_104 = ((uint64_t)((long)var_102 >> (long)2UL) + 475UL) + ((var_102 & 3UL) == 0UL);
                        var_105 = (uint32_t)(uint64_t)((long)(var_104 * 1374389535UL) >> (long)35UL) - (uint32_t)(var_104 >> 31UL);
                        var_106 = (var_105 * 5U) * 4294967291U;
                        var_107 = (uint32_t)var_104;
                        var_108 = *(uint64_t *)(local_sp_3 + 8UL);
                        var_109 = var_105 - (uint32_t)(((uint64_t)(var_107 + var_106) >> 31UL) & 1UL);
                        var_110 = ((((((((((uint64_t)((long)((uint64_t)((*(uint32_t *)(local_sp_3 + 88UL) - (uint32_t)(uint64_t)((long)((uint64_t)var_109 << 32UL) >> (long)34UL)) + ((*(uint32_t *)(local_sp_3 + 44UL) - var_107) + (var_109 - *(uint32_t *)(local_sp_3 + 64UL)))) << 32UL) >> (long)32UL) + ((((var_108 - var_102) * 365UL) + *(uint64_t *)(local_sp_3 + 24UL)) - (uint64_t)*(uint32_t *)(local_sp_3 + 236UL))) * 24UL) + *(uint64_t *)(local_sp_3 + 48UL)) - (uint64_t)*(uint32_t *)(local_sp_3 + 216UL)) * 60UL) + *(uint64_t *)(local_sp_3 + 56UL)) - (uint64_t)*(uint32_t *)(local_sp_3 + 212UL)) * 60UL) + *(uint64_t *)(local_sp_3 + 16UL)) - (uint64_t)*(uint32_t *)(local_sp_3 + 208UL);
                        r13_4 = var_103;
                        if ((long)var_110 >= (long)0UL) {
                            var_112 = 9223372036854775808UL - (-9223372036854775808L);
                            storemerge5 = (var_112 & (-256L)) | ((long)var_112 > (long)rbp_1);
                            loop_state_var = 1U;
                            switch_state_var = 1;
                            break;
                        }
                        var_111 = 9223372036854775807UL - var_110;
                        storemerge5 = (var_111 & (-256L)) | ((long)var_111 < (long)rbp_1);
                        loop_state_var = 1U;
                        switch_state_var = 1;
                        break;
                    }
                    break;
                }
                if (switch_state_var)
                    break;
            }
            var_52 = (uint64_t)*(uint32_t *)(rax_4 + 20UL);
            var_53 = ((uint64_t)((long)var_52 >> (long)2UL) + 475UL) + ((var_52 & 3UL) == 0UL);
            var_54 = (uint32_t)(uint64_t)((long)(var_53 * 1374389535UL) >> (long)35UL) - (uint32_t)(var_53 >> 31UL);
            var_55 = (var_54 * 5U) * 4294967291U;
            var_56 = (uint32_t)var_53;
            var_57 = *(uint64_t *)(var_51 + 48UL);
            var_58 = var_54 - (uint32_t)(((uint64_t)(var_56 + var_55) >> 31UL) & 1UL);
            var_59 = (((((((((((((*(uint64_t *)local_sp_21 - var_52) * 365UL) + *(uint64_t *)(var_51 + 24UL)) - (uint64_t)*(uint32_t *)(rax_4 + 28UL)) + (uint64_t)((long)((uint64_t)((*(uint32_t *)(var_51 + 72UL) - (uint32_t)(uint64_t)((long)((uint64_t)var_58 << 32UL) >> (long)34UL)) + ((*(uint32_t *)(var_51 + 44UL) - var_56) + (var_58 - *(uint32_t *)(var_51 + 64UL)))) << 32UL) >> (long)32UL)) * 24UL) + var_57) - (uint64_t)*(uint32_t *)(rax_4 + 8UL)) * 60UL) + *(uint64_t *)(var_51 + 56UL)) - (uint64_t)*(uint32_t *)(rax_4 + 4UL)) * 60UL) + *(uint64_t *)(var_51 + 16UL)) - (uint64_t)*(uint32_t *)rax_4;
            rdx_4 = var_59;
            rdx_5 = var_59;
            if ((long)var_59 < (long)0UL) {
                var_61 = 9223372036854775808UL - (-9223372036854775808L);
                storemerge1 = (var_61 & (-256L)) | ((long)var_61 > (long)rax_4);
            } else {
                var_60 = 9223372036854775807UL - var_59;
                storemerge1 = (var_60 & (-256L)) | ((long)var_60 < (long)rax_4);
            }
            if ((uint64_t)(unsigned char)storemerge1 == 0UL) {
                var_65 = var_59 + rax_4;
                rax_1 = var_65;
                rax_2 = rax_1;
                rdx_5 = rdx_4;
                if (rax_1 != rax_4) {
                    var_76 = (uint32_t *)(var_51 + 80UL);
                    var_77 = *var_76;
                    var_78 = *(uint32_t *)(var_51 + 176UL);
                    var_79 = (uint64_t *)(var_51 + 120UL);
                    var_80 = *var_79;
                    var_81 = (unsigned char *)(var_51 + 68UL);
                    var_82 = (var_77 == 0U);
                    *var_81 = var_82;
                    var_83 = (var_78 == 0U);
                    var_84 = (rdx_4 & (-256L)) | var_83;
                    rdx_8 = var_84;
                    _pre_phi395 = var_79;
                    r13_3 = var_80;
                    if (((uint64_t)(var_82 - (unsigned char)var_83) == 0UL) || ((int)var_78 < (int)0U)) {
                        loop_state_var = 3U;
                        break;
                    }
                    var_85 = *var_76;
                    *var_79 = rax_4;
                    if ((int)var_85 < (int)0U) {
                        loop_state_var = 3U;
                        break;
                    }
                    *(uint64_t *)(var_51 + 72UL) = var_80;
                    loop_state_var = 2U;
                    while (1U)
                        {
                            switch (loop_state_var) {
                              case 2U:
                                {
                                    var_86 = 0UL - r14_1;
                                    *(uint32_t *)(local_sp_13 + 32UL) = 2U;
                                    var_87 = (uint64_t)((long)(var_86 << 32UL) >> (long)32UL);
                                    rbp_2 = var_87;
                                    local_sp_14 = local_sp_13;
                                    rdx_9 = rdx_8;
                                    rbp_4 = var_87;
                                    local_sp_17 = local_sp_13;
                                    rdx_12 = rdx_8;
                                    if ((int)(uint32_t)var_86 >= (int)0U) {
                                        var_100 = 9223372036854775807UL - rbp_4;
                                        storemerge4 = (var_100 & (-256L)) | ((long)var_100 < (long)rax_4);
                                        r13_1 = r13_2;
                                        rbp_3 = rbp_4;
                                        local_sp_15 = local_sp_17;
                                        rdx_10 = rdx_12;
                                        loop_state_var = 1U;
                                        continue;
                                    }
                                    var_88 = 9223372036854775808UL - (-9223372036854775808L);
                                    storemerge4 = (var_88 & (-256L)) | ((long)var_88 > (long)rax_4);
                                    r13_1 = r13_0;
                                    rbp_3 = rbp_2;
                                    local_sp_15 = local_sp_14;
                                    rdx_10 = rdx_9;
                                }
                                break;
                              case 1U:
                              case 0U:
                                {
                                    var_88 = 9223372036854775808UL - (-9223372036854775808L);
                                    storemerge4 = (var_88 & (-256L)) | ((long)var_88 > (long)rax_4);
                                    r13_1 = r13_0;
                                    rbp_3 = rbp_2;
                                    local_sp_15 = local_sp_14;
                                    rdx_10 = rdx_9;
                                }
                                break;
                            }
                        }
                    switch_state_var = 0;
                    switch (loop_state_var) {
                      case 0U:
                        {
                            loop_state_var = 2U;
                            switch_state_var = 1;
                            break;
                        }
                        break;
                      case 1U:
                        {
                            var_102 = (uint64_t)*(uint32_t *)(local_sp_3 + 228UL);
                            var_103 = *(uint64_t *)(local_sp_3 + 72UL);
                            var_104 = ((uint64_t)((long)var_102 >> (long)2UL) + 475UL) + ((var_102 & 3UL) == 0UL);
                            var_105 = (uint32_t)(uint64_t)((long)(var_104 * 1374389535UL) >> (long)35UL) - (uint32_t)(var_104 >> 31UL);
                            var_106 = (var_105 * 5U) * 4294967291U;
                            var_107 = (uint32_t)var_104;
                            var_108 = *(uint64_t *)(local_sp_3 + 8UL);
                            var_109 = var_105 - (uint32_t)(((uint64_t)(var_107 + var_106) >> 31UL) & 1UL);
                            var_110 = ((((((((((uint64_t)((long)((uint64_t)((*(uint32_t *)(local_sp_3 + 88UL) - (uint32_t)(uint64_t)((long)((uint64_t)var_109 << 32UL) >> (long)34UL)) + ((*(uint32_t *)(local_sp_3 + 44UL) - var_107) + (var_109 - *(uint32_t *)(local_sp_3 + 64UL)))) << 32UL) >> (long)32UL) + ((((var_108 - var_102) * 365UL) + *(uint64_t *)(local_sp_3 + 24UL)) - (uint64_t)*(uint32_t *)(local_sp_3 + 236UL))) * 24UL) + *(uint64_t *)(local_sp_3 + 48UL)) - (uint64_t)*(uint32_t *)(local_sp_3 + 216UL)) * 60UL) + *(uint64_t *)(local_sp_3 + 56UL)) - (uint64_t)*(uint32_t *)(local_sp_3 + 212UL)) * 60UL) + *(uint64_t *)(local_sp_3 + 16UL)) - (uint64_t)*(uint32_t *)(local_sp_3 + 208UL);
                            r13_4 = var_103;
                            if ((long)var_110 >= (long)0UL) {
                                var_112 = 9223372036854775808UL - (-9223372036854775808L);
                                storemerge5 = (var_112 & (-256L)) | ((long)var_112 > (long)rbp_1);
                                loop_state_var = 1U;
                                switch_state_var = 1;
                                break;
                            }
                            var_111 = 9223372036854775807UL - var_110;
                            storemerge5 = (var_111 & (-256L)) | ((long)var_111 < (long)rbp_1);
                            loop_state_var = 1U;
                            switch_state_var = 1;
                            break;
                        }
                        break;
                    }
                    if (switch_state_var)
                        break;
                }
            }
            if ((long)rax_4 < (long)0UL) {
                var_62 = rax_4 + 1UL;
                var_63 = helper_cc_compute_all_wrapper(rax_4 + 9223372036854775806UL, 9223372036854775810UL, var_7, 17U);
                var_64 = ((signed char)((unsigned char)(var_63 >> 4UL) ^ (unsigned char)var_63) > '\xff') ? 9223372036854775808UL : var_62;
                rax_1 = var_64;
                rax_2 = rax_1;
                rdx_5 = rdx_4;
                if (rax_1 == rax_4) {
                    var_76 = (uint32_t *)(var_51 + 80UL);
                    var_77 = *var_76;
                    var_78 = *(uint32_t *)(var_51 + 176UL);
                    var_79 = (uint64_t *)(var_51 + 120UL);
                    var_80 = *var_79;
                    var_81 = (unsigned char *)(var_51 + 68UL);
                    var_82 = (var_77 == 0U);
                    *var_81 = var_82;
                    var_83 = (var_78 == 0U);
                    var_84 = (rdx_4 & (-256L)) | var_83;
                    rdx_8 = var_84;
                    _pre_phi395 = var_79;
                    r13_3 = var_80;
                    if (((uint64_t)(var_82 - (unsigned char)var_83) == 0UL) || ((int)var_78 < (int)0U)) {
                        loop_state_var = 3U;
                        break;
                    }
                    var_85 = *var_76;
                    *var_79 = rax_4;
                    if ((int)var_85 < (int)0U) {
                        loop_state_var = 3U;
                        break;
                    }
                    *(uint64_t *)(var_51 + 72UL) = var_80;
                    loop_state_var = 2U;
                    while (1U)
                        {
                            switch (loop_state_var) {
                              case 2U:
                                {
                                    var_86 = 0UL - r14_1;
                                    *(uint32_t *)(local_sp_13 + 32UL) = 2U;
                                    var_87 = (uint64_t)((long)(var_86 << 32UL) >> (long)32UL);
                                    rbp_2 = var_87;
                                    local_sp_14 = local_sp_13;
                                    rdx_9 = rdx_8;
                                    rbp_4 = var_87;
                                    local_sp_17 = local_sp_13;
                                    rdx_12 = rdx_8;
                                    if ((int)(uint32_t)var_86 >= (int)0U) {
                                        var_100 = 9223372036854775807UL - rbp_4;
                                        storemerge4 = (var_100 & (-256L)) | ((long)var_100 < (long)rax_4);
                                        r13_1 = r13_2;
                                        rbp_3 = rbp_4;
                                        local_sp_15 = local_sp_17;
                                        rdx_10 = rdx_12;
                                        loop_state_var = 1U;
                                        continue;
                                    }
                                    var_88 = 9223372036854775808UL - (-9223372036854775808L);
                                    storemerge4 = (var_88 & (-256L)) | ((long)var_88 > (long)rax_4);
                                    r13_1 = r13_0;
                                    rbp_3 = rbp_2;
                                    local_sp_15 = local_sp_14;
                                    rdx_10 = rdx_9;
                                }
                                break;
                              case 1U:
                              case 0U:
                                {
                                    var_88 = 9223372036854775808UL - (-9223372036854775808L);
                                    storemerge4 = (var_88 & (-256L)) | ((long)var_88 > (long)rax_4);
                                    r13_1 = r13_0;
                                    rbp_3 = rbp_2;
                                    local_sp_15 = local_sp_14;
                                    rdx_10 = rdx_9;
                                }
                                break;
                            }
                        }
                    switch_state_var = 0;
                    switch (loop_state_var) {
                      case 0U:
                        {
                            loop_state_var = 2U;
                            switch_state_var = 1;
                            break;
                        }
                        break;
                      case 1U:
                        {
                            var_102 = (uint64_t)*(uint32_t *)(local_sp_3 + 228UL);
                            var_103 = *(uint64_t *)(local_sp_3 + 72UL);
                            var_104 = ((uint64_t)((long)var_102 >> (long)2UL) + 475UL) + ((var_102 & 3UL) == 0UL);
                            var_105 = (uint32_t)(uint64_t)((long)(var_104 * 1374389535UL) >> (long)35UL) - (uint32_t)(var_104 >> 31UL);
                            var_106 = (var_105 * 5U) * 4294967291U;
                            var_107 = (uint32_t)var_104;
                            var_108 = *(uint64_t *)(local_sp_3 + 8UL);
                            var_109 = var_105 - (uint32_t)(((uint64_t)(var_107 + var_106) >> 31UL) & 1UL);
                            var_110 = ((((((((((uint64_t)((long)((uint64_t)((*(uint32_t *)(local_sp_3 + 88UL) - (uint32_t)(uint64_t)((long)((uint64_t)var_109 << 32UL) >> (long)34UL)) + ((*(uint32_t *)(local_sp_3 + 44UL) - var_107) + (var_109 - *(uint32_t *)(local_sp_3 + 64UL)))) << 32UL) >> (long)32UL) + ((((var_108 - var_102) * 365UL) + *(uint64_t *)(local_sp_3 + 24UL)) - (uint64_t)*(uint32_t *)(local_sp_3 + 236UL))) * 24UL) + *(uint64_t *)(local_sp_3 + 48UL)) - (uint64_t)*(uint32_t *)(local_sp_3 + 216UL)) * 60UL) + *(uint64_t *)(local_sp_3 + 56UL)) - (uint64_t)*(uint32_t *)(local_sp_3 + 212UL)) * 60UL) + *(uint64_t *)(local_sp_3 + 16UL)) - (uint64_t)*(uint32_t *)(local_sp_3 + 208UL);
                            r13_4 = var_103;
                            if ((long)var_110 >= (long)0UL) {
                                var_112 = 9223372036854775808UL - (-9223372036854775808L);
                                storemerge5 = (var_112 & (-256L)) | ((long)var_112 > (long)rbp_1);
                                loop_state_var = 1U;
                                switch_state_var = 1;
                                break;
                            }
                            var_111 = 9223372036854775807UL - var_110;
                            storemerge5 = (var_111 & (-256L)) | ((long)var_111 < (long)rbp_1);
                            loop_state_var = 1U;
                            switch_state_var = 1;
                            break;
                        }
                        break;
                    }
                    if (switch_state_var)
                        break;
                }
            }
            if ((long)rax_4 <= (long)9223372036854775805UL) {
                rax_2 = rax_1;
                rdx_5 = rdx_4;
                if (rax_1 != rax_4) {
                    var_76 = (uint32_t *)(var_51 + 80UL);
                    var_77 = *var_76;
                    var_78 = *(uint32_t *)(var_51 + 176UL);
                    var_79 = (uint64_t *)(var_51 + 120UL);
                    var_80 = *var_79;
                    var_81 = (unsigned char *)(var_51 + 68UL);
                    var_82 = (var_77 == 0U);
                    *var_81 = var_82;
                    var_83 = (var_78 == 0U);
                    var_84 = (rdx_4 & (-256L)) | var_83;
                    rdx_8 = var_84;
                    _pre_phi395 = var_79;
                    r13_3 = var_80;
                    if (!(((uint64_t)(var_82 - (unsigned char)var_83) == 0UL) || ((int)var_78 < (int)0U))) {
                        loop_state_var = 3U;
                        break;
                    }
                    var_85 = *var_76;
                    *var_79 = rax_4;
                    if ((int)var_85 >= (int)0U) {
                        loop_state_var = 3U;
                        break;
                    }
                    *(uint64_t *)(var_51 + 72UL) = var_80;
                    loop_state_var = 2U;
                    while (1U)
                        {
                            switch (loop_state_var) {
                              case 2U:
                                {
                                    var_86 = 0UL - r14_1;
                                    *(uint32_t *)(local_sp_13 + 32UL) = 2U;
                                    var_87 = (uint64_t)((long)(var_86 << 32UL) >> (long)32UL);
                                    rbp_2 = var_87;
                                    local_sp_14 = local_sp_13;
                                    rdx_9 = rdx_8;
                                    rbp_4 = var_87;
                                    local_sp_17 = local_sp_13;
                                    rdx_12 = rdx_8;
                                    if ((int)(uint32_t)var_86 >= (int)0U) {
                                        var_100 = 9223372036854775807UL - rbp_4;
                                        storemerge4 = (var_100 & (-256L)) | ((long)var_100 < (long)rax_4);
                                        r13_1 = r13_2;
                                        rbp_3 = rbp_4;
                                        local_sp_15 = local_sp_17;
                                        rdx_10 = rdx_12;
                                        loop_state_var = 1U;
                                        continue;
                                    }
                                    var_88 = 9223372036854775808UL - (-9223372036854775808L);
                                    storemerge4 = (var_88 & (-256L)) | ((long)var_88 > (long)rax_4);
                                    r13_1 = r13_0;
                                    rbp_3 = rbp_2;
                                    local_sp_15 = local_sp_14;
                                    rdx_10 = rdx_9;
                                }
                                break;
                              case 1U:
                              case 0U:
                                {
                                    var_88 = 9223372036854775808UL - (-9223372036854775808L);
                                    storemerge4 = (var_88 & (-256L)) | ((long)var_88 > (long)rax_4);
                                    r13_1 = r13_0;
                                    rbp_3 = rbp_2;
                                    local_sp_15 = local_sp_14;
                                    rdx_10 = rdx_9;
                                }
                                break;
                            }
                        }
                    switch_state_var = 0;
                    switch (loop_state_var) {
                      case 1U:
                        {
                            var_102 = (uint64_t)*(uint32_t *)(local_sp_3 + 228UL);
                            var_103 = *(uint64_t *)(local_sp_3 + 72UL);
                            var_104 = ((uint64_t)((long)var_102 >> (long)2UL) + 475UL) + ((var_102 & 3UL) == 0UL);
                            var_105 = (uint32_t)(uint64_t)((long)(var_104 * 1374389535UL) >> (long)35UL) - (uint32_t)(var_104 >> 31UL);
                            var_106 = (var_105 * 5U) * 4294967291U;
                            var_107 = (uint32_t)var_104;
                            var_108 = *(uint64_t *)(local_sp_3 + 8UL);
                            var_109 = var_105 - (uint32_t)(((uint64_t)(var_107 + var_106) >> 31UL) & 1UL);
                            var_110 = ((((((((((uint64_t)((long)((uint64_t)((*(uint32_t *)(local_sp_3 + 88UL) - (uint32_t)(uint64_t)((long)((uint64_t)var_109 << 32UL) >> (long)34UL)) + ((*(uint32_t *)(local_sp_3 + 44UL) - var_107) + (var_109 - *(uint32_t *)(local_sp_3 + 64UL)))) << 32UL) >> (long)32UL) + ((((var_108 - var_102) * 365UL) + *(uint64_t *)(local_sp_3 + 24UL)) - (uint64_t)*(uint32_t *)(local_sp_3 + 236UL))) * 24UL) + *(uint64_t *)(local_sp_3 + 48UL)) - (uint64_t)*(uint32_t *)(local_sp_3 + 216UL)) * 60UL) + *(uint64_t *)(local_sp_3 + 56UL)) - (uint64_t)*(uint32_t *)(local_sp_3 + 212UL)) * 60UL) + *(uint64_t *)(local_sp_3 + 16UL)) - (uint64_t)*(uint32_t *)(local_sp_3 + 208UL);
                            r13_4 = var_103;
                            if ((long)var_110 >= (long)0UL) {
                                var_111 = 9223372036854775807UL - var_110;
                                storemerge5 = (var_111 & (-256L)) | ((long)var_111 < (long)rbp_1);
                                loop_state_var = 1U;
                                switch_state_var = 1;
                                break;
                            }
                            var_112 = 9223372036854775808UL - (-9223372036854775808L);
                            storemerge5 = (var_112 & (-256L)) | ((long)var_112 > (long)rbp_1);
                            loop_state_var = 1U;
                            switch_state_var = 1;
                            break;
                        }
                        break;
                      case 0U:
                        {
                            loop_state_var = 2U;
                            switch_state_var = 1;
                            break;
                        }
                        break;
                    }
                    if (switch_state_var)
                        break;
                }
            }
            rax_2 = rax_4 + (-1L);
        }
    switch (loop_state_var) {
      case 0U:
        {
            return storemerge2;
        }
        break;
      case 4U:
      case 3U:
      case 2U:
      case 1U:
        {
            switch (loop_state_var) {
              case 2U:
                {
                    var_120 = (uint64_t *)(local_sp_19 + 120UL);
                    var_121 = (*var_120 - *(uint64_t *)(local_sp_19 + 104UL)) - *(uint64_t *)(local_sp_19 + 112UL);
                    var_122 = *(uint32_t *)(local_sp_19 + 96UL);
                    *(uint64_t *)6437816UL = var_121;
                    var_123 = local_sp_19 + 144UL;
                    var_124 = *(uint32_t *)var_123;
                    _pre_phi396 = var_123;
                    r15_5 = r15_4;
                    local_sp_20 = local_sp_19;
                    if ((uint64_t)(var_122 - var_124) != 0UL) {
                        var_125 = (((*(uint32_t *)(local_sp_19 + 100UL) == 0U) && ((uint64_t)(var_124 + (-60)) == 0UL)) - *(uint64_t *)(local_sp_19 + 16UL)) + (uint64_t)var_122;
                        if ((long)var_125 < (long)0UL) {
                            var_127 = 9223372036854775808UL - (-9223372036854775808L);
                            storemerge3 = (var_127 & (-256L)) | ((long)var_127 > (long)r15_4);
                        } else {
                            var_126 = 9223372036854775807UL - var_125;
                            storemerge3 = (var_126 & (-256L)) | ((long)var_126 < (long)r15_4);
                        }
                        if ((uint64_t)(unsigned char)storemerge3 == 0UL) {
                            return storemerge2;
                        }
                        var_128 = *var_120 + var_125;
                        *(uint64_t *)(local_sp_19 + 208UL) = var_128;
                        var_129 = local_sp_19 + (-8L);
                        *(uint64_t *)var_129 = 4255986UL;
                        indirect_placeholder();
                        r15_5 = var_128;
                        local_sp_20 = var_129;
                        if (var_125 != 0UL) {
                            return storemerge2;
                        }
                        _pre_phi396 = var_129 + 144UL;
                    }
                    *(uint64_t *)r13_4 = *(uint64_t *)_pre_phi396;
                    *(uint64_t *)(r13_4 + 8UL) = *(uint64_t *)(local_sp_20 + 152UL);
                    *(uint64_t *)(r13_4 + 16UL) = *(uint64_t *)(local_sp_20 + 160UL);
                    *(uint64_t *)(r13_4 + 24UL) = *(uint64_t *)(local_sp_20 + 168UL);
                    *(uint64_t *)(r13_4 + 32UL) = *(uint64_t *)(local_sp_20 + 176UL);
                    *(uint64_t *)(r13_4 + 40UL) = *(uint64_t *)(local_sp_20 + 184UL);
                    *(uint64_t *)(r13_4 + 48UL) = *(uint64_t *)(local_sp_20 + 192UL);
                    storemerge2 = r15_5;
                }
                break;
              case 4U:
              case 3U:
                {
                    switch (loop_state_var) {
                      case 3U:
                        {
                            *_pre_phi395 = rax_4;
                            r13_4 = r13_3;
                        }
                        break;
                      case 4U:
                        {
                            var_73 = (uint64_t *)(var_51 + 120UL);
                            _pre_phi395 = var_73;
                            r13_3 = *var_73;
                        }
                        break;
                    }
                }
                break;
              case 1U:
                {
                    if ((uint64_t)(unsigned char)storemerge5 == 0UL) {
                        var_118 = var_110 + rbp_1;
                        *(uint64_t *)(local_sp_3 + 120UL) = var_118;
                        *(uint64_t *)(local_sp_3 + 136UL) = var_118;
                        var_119 = local_sp_3 + (-8L);
                        *(uint64_t *)var_119 = 4255356UL;
                        indirect_placeholder();
                        r15_4 = var_118;
                        local_sp_19 = var_119;
                    } else {
                        var_113 = ((long)rbp_1 < (long)0UL);
                        var_114 = (uint64_t *)(local_sp_3 + 120UL);
                        if (var_113) {
                            *var_114 = 9223372036854775808UL;
                            r15_2 = 9223372036854775808UL;
                            if ((long)rbp_1 < (long)9223372036854775810UL) {
                                var_116 = rbp_1 + 1UL;
                                *var_114 = var_116;
                                r15_2 = var_116;
                            }
                        } else {
                            *var_114 = 9223372036854775807UL;
                            if ((long)rbp_1 > (long)9223372036854775805UL) {
                                var_115 = rbp_1 + (-1L);
                                *var_114 = var_115;
                                r15_2 = var_115;
                            }
                        }
                        *(uint64_t *)(local_sp_3 + 136UL) = r15_2;
                        var_117 = local_sp_3 + (-8L);
                        *(uint64_t *)var_117 = 4256342UL;
                        indirect_placeholder();
                        r15_4 = r15_2;
                        local_sp_19 = var_117;
                    }
                }
                break;
            }
        }
        break;
    }
}

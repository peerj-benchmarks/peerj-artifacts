typedef unsigned char __u_char;
typedef unsigned short __u_short;
typedef unsigned int __u_int;
typedef unsigned long __u_long;
typedef signed char __int8_t;
typedef unsigned char __uint8_t;
typedef short __int16_t;
typedef unsigned short __uint16_t;
typedef int __int32_t;
typedef unsigned int __uint32_t;
typedef long __int64_t;
typedef unsigned long __uint64_t;
typedef __int8_t __int_least8_t;
typedef __uint8_t __uint_least8_t;
typedef __int16_t __int_least16_t;
typedef __uint16_t __uint_least16_t;
typedef __int32_t __int_least32_t;
typedef __uint32_t __uint_least32_t;
typedef __int64_t __int_least64_t;
typedef __uint64_t __uint_least64_t;
typedef long __quad_t;
typedef unsigned long __u_quad_t;
typedef long __intmax_t;
typedef unsigned long __uintmax_t;
typedef unsigned long __dev_t;
typedef unsigned int __uid_t;
typedef unsigned int __gid_t;
typedef unsigned long __ino_t;
typedef unsigned long __ino64_t;
typedef unsigned int __mode_t;
typedef unsigned long __nlink_t;
typedef long __off_t;
typedef long __off64_t;
typedef int __pid_t;
typedef struct {
    int __val[2];
} __fsid_t;
typedef long __clock_t;
typedef unsigned long __rlim_t;
typedef unsigned long __rlim64_t;
typedef unsigned int __id_t;
typedef long __time_t;
typedef unsigned int __useconds_t;
typedef long __suseconds_t;
typedef int __daddr_t;
typedef int __key_t;
typedef int __clockid_t;
typedef void *__timer_t;
typedef long __blksize_t;
typedef long __blkcnt_t;
typedef long __blkcnt64_t;
typedef unsigned long __fsblkcnt_t;
typedef unsigned long __fsblkcnt64_t;
typedef unsigned long __fsfilcnt_t;
typedef unsigned long __fsfilcnt64_t;
typedef long __fsword_t;
typedef long __ssize_t;
typedef long __syscall_slong_t;
typedef unsigned long __syscall_ulong_t;
typedef __off64_t __loff_t;
typedef char *__caddr_t;
typedef long __intptr_t;
typedef unsigned int __socklen_t;
typedef int __sig_atomic_t;
typedef __int8_t int8_t;
typedef __int16_t int16_t;
typedef __int32_t int32_t;
typedef __int64_t int64_t;
typedef __uint8_t uint8_t;
typedef __uint16_t uint16_t;
typedef __uint32_t uint32_t;
typedef __uint64_t uint64_t;
typedef __int_least8_t int_least8_t;
typedef __int_least16_t int_least16_t;
typedef __int_least32_t int_least32_t;
typedef __int_least64_t int_least64_t;
typedef __uint_least8_t uint_least8_t;
typedef __uint_least16_t uint_least16_t;
typedef __uint_least32_t uint_least32_t;
typedef __uint_least64_t uint_least64_t;
typedef signed char int_fast8_t;
typedef long int_fast16_t;
typedef long int_fast32_t;
typedef long int_fast64_t;
typedef unsigned char uint_fast8_t;
typedef unsigned long uint_fast16_t;
typedef unsigned long uint_fast32_t;
typedef unsigned long uint_fast64_t;
typedef long intptr_t;
typedef unsigned long uintptr_t;
typedef __intmax_t intmax_t;
typedef __uintmax_t uintmax_t;
typedef _Bool bool;
extern uint64_t revng_init_local_sp(uint64_t param_0);
extern void indirect_placeholder(void);
extern uint64_t init_rbx(void);
extern uint64_t init_rbp(void);
extern uint64_t init_cc_src2(void);
extern uint64_t helper_cc_compute_c_wrapper(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint32_t param_3);
extern uint64_t init_r14(void);
extern uint64_t init_r13(void);
extern uint64_t init_r12(void);
extern uint64_t indirect_placeholder_1(uint64_t param_0);
extern uint64_t init_rax(void);
extern uint64_t init_r15(void);
uint64_t bb_save_abbr(uint64_t rdi, uint64_t rsi) {
    uint64_t var_0;
    uint64_t var_1;
    uint64_t var_2;
    uint64_t var_3;
    uint64_t var_4;
    uint64_t var_5;
    uint64_t var_6;
    uint64_t var_7;
    uint64_t var_8;
    uint64_t var_9;
    uint64_t *var_10;
    uint64_t var_11;
    uint64_t rax_5;
    uint64_t var_30;
    unsigned char *var_31;
    uint64_t local_sp_1;
    uint64_t rbx_2;
    uint64_t r14_0;
    uint64_t rax_2;
    uint64_t rax_4_be;
    uint64_t local_sp_2;
    uint64_t rax_0;
    uint64_t r15_0;
    uint64_t local_sp_0;
    bool var_26;
    uint64_t *var_27;
    uint64_t var_28;
    uint64_t rdi1_0;
    uint64_t var_29;
    uint64_t var_22;
    uint64_t var_23;
    uint64_t var_24;
    uint64_t rbx_1;
    uint64_t rax_4;
    uint64_t rax_1;
    uint64_t rbp_1;
    uint64_t rbx_0;
    uint64_t rbp_0;
    uint64_t var_15;
    uint64_t var_25;
    uint64_t var_16;
    uint64_t var_17;
    uint64_t var_18;
    uint64_t var_19;
    uint64_t var_20;
    uint64_t var_21;
    uint64_t rax_3;
    uint64_t var_12;
    uint64_t var_13;
    uint64_t var_14;
    unsigned int loop_state_var;
    bool switch_state_var;
    var_0 = revng_init_local_sp(0UL);
    var_1 = init_r14();
    var_2 = init_rax();
    var_3 = init_r15();
    var_4 = init_r13();
    var_5 = init_rbx();
    var_6 = init_r12();
    var_7 = init_rbp();
    var_8 = init_cc_src2();
    *(uint64_t *)(var_0 + (-8L)) = var_3;
    *(uint64_t *)(var_0 + (-16L)) = var_1;
    *(uint64_t *)(var_0 + (-24L)) = var_4;
    *(uint64_t *)(var_0 + (-32L)) = var_6;
    *(uint64_t *)(var_0 + (-40L)) = var_7;
    *(uint64_t *)(var_0 + (-48L)) = var_5;
    var_9 = var_0 + (-56L);
    var_10 = (uint64_t *)(rsi + 48UL);
    var_11 = *var_10;
    rax_5 = 1UL;
    rbx_2 = 4327084UL;
    local_sp_2 = var_9;
    r15_0 = 0UL;
    rbp_1 = rdi;
    rax_3 = var_2;
    if (var_11 == 0UL) {
        return rax_5;
    }
    rax_5 = 0UL;
    if (var_11 >= rsi) {
        var_12 = rsi + 56UL;
        var_13 = helper_cc_compute_c_wrapper(var_11 - var_12, var_12, var_8, 17U);
        rax_3 = 1UL;
        if (var_13 == 0UL) {
            return rax_5;
        }
    }
    rax_4 = rax_3;
    if (*(unsigned char *)var_11 != '\x00') {
        rbx_1 = rdi + 9UL;
        while (1U)
            {
                var_14 = local_sp_2 + (-8L);
                *(uint64_t *)var_14 = 4257683UL;
                indirect_placeholder();
                local_sp_1 = var_14;
                rbx_2 = rbx_1;
                rax_1 = rax_4;
                rbx_0 = rbx_1;
                rbp_0 = rbp_1;
                if ((uint64_t)(uint32_t)rax_4 != 0UL) {
                    loop_state_var = 0U;
                    break;
                }
                while (1U)
                    {
                        rax_0 = rbx_0;
                        rax_2 = rax_1;
                        rbp_1 = rbp_0;
                        rbx_2 = rbx_0;
                        if (*(unsigned char *)rbx_0 == '\x00') {
                            var_20 = local_sp_1 + (-8L);
                            *(uint64_t *)var_20 = 4257719UL;
                            indirect_placeholder();
                            var_21 = (rax_2 + rbx_0) + 1UL;
                            rax_4_be = rax_2;
                            rbx_1 = var_21;
                            local_sp_2 = var_20;
                            if (*(unsigned char *)var_21 != '\x00') {
                                loop_state_var = 0U;
                                break;
                            }
                            var_22 = *(uint64_t *)rbp_0;
                            rax_4_be = 0UL;
                            rax_1 = var_22;
                            rbp_0 = var_22;
                            if (var_22 != 0UL) {
                                loop_state_var = 0U;
                                break;
                            }
                            var_23 = var_22 + 9UL;
                            var_24 = local_sp_1 + (-16L);
                            *(uint64_t *)var_24 = 4257756UL;
                            indirect_placeholder();
                            rbx_0 = var_23;
                            local_sp_1 = var_24;
                            rbx_2 = var_23;
                            if ((uint64_t)(uint32_t)var_22 == 0UL) {
                                continue;
                            }
                            loop_state_var = 2U;
                            break;
                        }
                        var_15 = rbp_0 + 9UL;
                        rax_2 = rbx_0;
                        if (rbx_0 == var_15) {
                            if (*(unsigned char *)(rbp_0 + 8UL) != '\x00') {
                                var_25 = local_sp_1 + (-8L);
                                *(uint64_t *)var_25 = 4257979UL;
                                indirect_placeholder();
                                r14_0 = rbp_0 + 10UL;
                                local_sp_0 = var_25;
                                loop_state_var = 1U;
                                break;
                            }
                        }
                        var_16 = rbx_0 - var_15;
                        var_17 = local_sp_1 + (-8L);
                        *(uint64_t *)var_17 = 4257822UL;
                        indirect_placeholder();
                        var_18 = rbp_0 + 10UL;
                        var_19 = var_16 ^ (-1L);
                        r14_0 = var_18;
                        rax_0 = var_19;
                        r15_0 = var_16;
                        local_sp_0 = var_17;
                        if (var_18 <= var_19) {
                            loop_state_var = 1U;
                            break;
                        }
                        *(uint64_t *)(local_sp_1 + (-16L)) = 4258045UL;
                        indirect_placeholder();
                        *(uint32_t *)var_19 = 12U;
                        loop_state_var = 3U;
                        break;
                    }
                switch_state_var = 0;
                switch (loop_state_var) {
                  case 0U:
                    {
                        rax_4 = rax_4_be;
                        continue;
                    }
                    break;
                  case 1U:
                    {
                        var_26 = ((r15_0 + r14_0) > 118UL);
                        var_27 = (uint64_t *)(local_sp_0 + (-8L));
                        if (var_26) {
                            *var_27 = 4258030UL;
                            indirect_placeholder();
                            *(unsigned char *)(r14_0 + rbx_0) = (unsigned char)'\x00';
                            loop_state_var = 0U;
                            switch_state_var = 1;
                            break;
                        }
                        *var_27 = 4257862UL;
                        indirect_placeholder();
                        var_28 = rax_0 + 1UL;
                        rdi1_0 = (var_28 > 118UL) ? ((rax_0 + 26UL) & (-16L)) : 128UL;
                        *(uint64_t *)(local_sp_0 + (-16L)) = 4257893UL;
                        var_29 = indirect_placeholder_1(rdi1_0);
                        if (var_29 == 0UL) {
                            *(uint64_t *)rbp_0 = 0UL;
                            loop_state_var = 1U;
                            switch_state_var = 1;
                            break;
                        }
                        var_30 = var_29 + 9UL;
                        *(uint64_t *)var_29 = 0UL;
                        var_31 = (unsigned char *)(var_29 + 8UL);
                        *var_31 = (unsigned char)'\x01';
                        *(unsigned char *)var_30 = (unsigned char)'\x00';
                        *(uint64_t *)(local_sp_0 + (-24L)) = 4257940UL;
                        indirect_placeholder();
                        *(unsigned char *)((var_28 + var_29) + 9UL) = (unsigned char)'\x00';
                        *(uint64_t *)rbp_0 = var_29;
                        *var_31 = (unsigned char)'\x00';
                        rbx_2 = var_30;
                        loop_state_var = 0U;
                        switch_state_var = 1;
                        break;
                    }
                    break;
                  case 2U:
                    {
                        loop_state_var = 0U;
                        switch_state_var = 1;
                        break;
                    }
                    break;
                  case 3U:
                    {
                        loop_state_var = 1U;
                        switch_state_var = 1;
                        break;
                    }
                    break;
                }
                if (switch_state_var)
                    break;
            }
        switch (loop_state_var) {
          case 0U:
            {
                break;
            }
            break;
          case 1U:
            {
                return rax_5;
            }
            break;
        }
    }
    *var_10 = rbx_2;
    rax_5 = 1UL;
}

typedef unsigned char __u_char;
typedef unsigned short __u_short;
typedef unsigned int __u_int;
typedef unsigned long __u_long;
typedef signed char __int8_t;
typedef unsigned char __uint8_t;
typedef short __int16_t;
typedef unsigned short __uint16_t;
typedef int __int32_t;
typedef unsigned int __uint32_t;
typedef long __int64_t;
typedef unsigned long __uint64_t;
typedef __int8_t __int_least8_t;
typedef __uint8_t __uint_least8_t;
typedef __int16_t __int_least16_t;
typedef __uint16_t __uint_least16_t;
typedef __int32_t __int_least32_t;
typedef __uint32_t __uint_least32_t;
typedef __int64_t __int_least64_t;
typedef __uint64_t __uint_least64_t;
typedef long __quad_t;
typedef unsigned long __u_quad_t;
typedef long __intmax_t;
typedef unsigned long __uintmax_t;
typedef unsigned long __dev_t;
typedef unsigned int __uid_t;
typedef unsigned int __gid_t;
typedef unsigned long __ino_t;
typedef unsigned long __ino64_t;
typedef unsigned int __mode_t;
typedef unsigned long __nlink_t;
typedef long __off_t;
typedef long __off64_t;
typedef int __pid_t;
typedef struct {
    int __val[2];
} __fsid_t;
typedef long __clock_t;
typedef unsigned long __rlim_t;
typedef unsigned long __rlim64_t;
typedef unsigned int __id_t;
typedef long __time_t;
typedef unsigned int __useconds_t;
typedef long __suseconds_t;
typedef int __daddr_t;
typedef int __key_t;
typedef int __clockid_t;
typedef void *__timer_t;
typedef long __blksize_t;
typedef long __blkcnt_t;
typedef long __blkcnt64_t;
typedef unsigned long __fsblkcnt_t;
typedef unsigned long __fsblkcnt64_t;
typedef unsigned long __fsfilcnt_t;
typedef unsigned long __fsfilcnt64_t;
typedef long __fsword_t;
typedef long __ssize_t;
typedef long __syscall_slong_t;
typedef unsigned long __syscall_ulong_t;
typedef __off64_t __loff_t;
typedef char *__caddr_t;
typedef long __intptr_t;
typedef unsigned int __socklen_t;
typedef int __sig_atomic_t;
typedef __int8_t int8_t;
typedef __int16_t int16_t;
typedef __int32_t int32_t;
typedef __int64_t int64_t;
typedef __uint8_t uint8_t;
typedef __uint16_t uint16_t;
typedef __uint32_t uint32_t;
typedef __uint64_t uint64_t;
typedef __int_least8_t int_least8_t;
typedef __int_least16_t int_least16_t;
typedef __int_least32_t int_least32_t;
typedef __int_least64_t int_least64_t;
typedef __uint_least8_t uint_least8_t;
typedef __uint_least16_t uint_least16_t;
typedef __uint_least32_t uint_least32_t;
typedef __uint_least64_t uint_least64_t;
typedef signed char int_fast8_t;
typedef long int_fast16_t;
typedef long int_fast32_t;
typedef long int_fast64_t;
typedef unsigned char uint_fast8_t;
typedef unsigned long uint_fast16_t;
typedef unsigned long uint_fast32_t;
typedef unsigned long uint_fast64_t;
typedef long intptr_t;
typedef unsigned long uintptr_t;
typedef __intmax_t intmax_t;
typedef __uintmax_t uintmax_t;
typedef _Bool bool;
extern uint64_t revng_init_local_sp(uint64_t param_0);
extern void indirect_placeholder(void);
void bb_usage(uint64_t rbx, uint64_t rdi, uint64_t rbp) {
    uint64_t var_0;
    uint64_t var_19;
    uint64_t local_sp_2;
    uint64_t var_20;
    uint64_t local_sp_6;
    uint64_t var_18;
    uint64_t local_sp_7;
    uint64_t *var_2;
    uint64_t *var_3;
    uint64_t *var_4;
    uint64_t *var_5;
    uint64_t *var_6;
    uint64_t *var_7;
    uint64_t *var_8;
    uint64_t *var_9;
    uint64_t *var_10;
    uint64_t *var_11;
    uint64_t *var_12;
    uint64_t *var_13;
    uint64_t *var_14;
    uint64_t *var_15;
    bool var_16;
    uint64_t *var_17;
    uint64_t var_21;
    uint64_t var_1;
    var_0 = revng_init_local_sp(0UL);
    *(uint64_t *)(var_0 + (-8L)) = rbp;
    *(uint64_t *)(var_0 + (-16L)) = rbx;
    if ((uint64_t)(uint32_t)rdi == 0UL) {
        local_sp_7 = var_0 + (-136L);
    } else {
        var_1 = var_0 + (-144L);
        *(uint64_t *)var_1 = 4204454UL;
        indirect_placeholder();
        local_sp_6 = var_1;
        var_21 = local_sp_6 + (-8L);
        *(uint64_t *)var_21 = 4204461UL;
        indirect_placeholder();
        local_sp_7 = var_21;
    }
    while (1U)
        {
            *(uint64_t *)(local_sp_7 + (-8L)) = 4204483UL;
            indirect_placeholder();
            *(uint64_t *)(local_sp_7 + (-16L)) = 4204503UL;
            indirect_placeholder();
            *(uint64_t *)(local_sp_7 + (-24L)) = 4204516UL;
            indirect_placeholder();
            *(uint64_t *)(local_sp_7 + (-32L)) = 4204529UL;
            indirect_placeholder();
            *(uint64_t *)(local_sp_7 + (-40L)) = 4204542UL;
            indirect_placeholder();
            *(uint64_t *)(local_sp_7 + (-48L)) = 4204555UL;
            indirect_placeholder();
            *(uint64_t *)(local_sp_7 + (-56L)) = 4204568UL;
            indirect_placeholder();
            *(uint64_t *)(local_sp_7 + (-64L)) = 4204581UL;
            indirect_placeholder();
            *(uint64_t *)(local_sp_7 + (-72L)) = 4204594UL;
            indirect_placeholder();
            *(uint64_t *)(local_sp_7 + (-80L)) = 4204607UL;
            indirect_placeholder();
            *(uint64_t *)(local_sp_7 + (-88L)) = 4204620UL;
            indirect_placeholder();
            *(uint64_t *)(local_sp_7 + (-96L)) = 4204633UL;
            indirect_placeholder();
            *(uint64_t *)(local_sp_7 + (-104L)) = 4204646UL;
            indirect_placeholder();
            var_2 = (uint64_t *)(local_sp_7 + (-112L));
            *var_2 = 4204659UL;
            indirect_placeholder();
            var_3 = (uint64_t *)(local_sp_7 + (-120L));
            *var_3 = 4204672UL;
            indirect_placeholder();
            var_4 = (uint64_t *)(local_sp_7 + (-128L));
            *var_4 = 4204685UL;
            indirect_placeholder();
            var_5 = (uint64_t *)(local_sp_7 + (-136L));
            *var_5 = 4204698UL;
            indirect_placeholder();
            var_6 = (uint64_t *)(local_sp_7 + (-144L));
            *var_6 = 4204711UL;
            indirect_placeholder();
            var_7 = (uint64_t *)(local_sp_7 + (-152L));
            *var_7 = 4204724UL;
            indirect_placeholder();
            var_8 = (uint64_t *)(local_sp_7 + (-160L));
            *var_8 = 4204737UL;
            indirect_placeholder();
            var_9 = (uint64_t *)(local_sp_7 + (-168L));
            *var_9 = 4204750UL;
            indirect_placeholder();
            var_10 = (uint64_t *)(local_sp_7 + (-176L));
            *var_10 = 4204763UL;
            indirect_placeholder();
            var_11 = (uint64_t *)(local_sp_7 + (-184L));
            *var_11 = 4204776UL;
            indirect_placeholder();
            var_12 = (uint64_t *)(local_sp_7 + (-192L));
            *var_12 = 4204789UL;
            indirect_placeholder();
            var_13 = (uint64_t *)(local_sp_7 + (-200L));
            *var_13 = 4204802UL;
            indirect_placeholder();
            var_14 = (uint64_t *)(local_sp_7 + (-208L));
            *var_14 = 4204815UL;
            indirect_placeholder();
            var_15 = (uint64_t *)(local_sp_7 + (-216L));
            *var_15 = 4204831UL;
            indirect_placeholder();
            *var_15 = 4307288UL;
            *var_14 = 4307290UL;
            *var_13 = 4307387UL;
            *var_12 = 4307306UL;
            *var_11 = 4307328UL;
            *var_10 = 4307338UL;
            *var_9 = 4307353UL;
            *var_8 = 4307338UL;
            *var_7 = 4307363UL;
            *var_6 = 4307338UL;
            *var_5 = 4307373UL;
            *var_4 = 4307338UL;
            *var_3 = 0UL;
            *var_2 = 0UL;
            *(uint64_t *)(local_sp_7 + (-224L)) = 4204990UL;
            indirect_placeholder();
            var_16 = (*var_13 == 0UL);
            var_17 = (uint64_t *)(local_sp_7 + (-232L));
            if (var_16) {
                *var_17 = 4205125UL;
                indirect_placeholder();
                *(uint64_t *)(local_sp_7 + (-240L)) = 4205137UL;
                indirect_placeholder();
                var_19 = local_sp_7 + (-248L);
                *(uint64_t *)var_19 = 4205210UL;
                indirect_placeholder();
                local_sp_2 = var_19;
            } else {
                *var_17 = 4205025UL;
                indirect_placeholder();
                *(uint64_t *)(local_sp_7 + (-240L)) = 4205037UL;
                indirect_placeholder();
                var_18 = local_sp_7 + (-248L);
                *(uint64_t *)var_18 = 4205086UL;
                indirect_placeholder();
                local_sp_2 = var_18;
            }
            var_20 = local_sp_2 + (-8L);
            *(uint64_t *)var_20 = 4205115UL;
            indirect_placeholder();
            local_sp_6 = var_20;
            var_21 = local_sp_6 + (-8L);
            *(uint64_t *)var_21 = 4204461UL;
            indirect_placeholder();
            local_sp_7 = var_21;
            continue;
        }
}

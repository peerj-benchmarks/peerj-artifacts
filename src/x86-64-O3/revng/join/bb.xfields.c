typedef unsigned char __u_char;
typedef unsigned short __u_short;
typedef unsigned int __u_int;
typedef unsigned long __u_long;
typedef signed char __int8_t;
typedef unsigned char __uint8_t;
typedef short __int16_t;
typedef unsigned short __uint16_t;
typedef int __int32_t;
typedef unsigned int __uint32_t;
typedef long __int64_t;
typedef unsigned long __uint64_t;
typedef __int8_t __int_least8_t;
typedef __uint8_t __uint_least8_t;
typedef __int16_t __int_least16_t;
typedef __uint16_t __uint_least16_t;
typedef __int32_t __int_least32_t;
typedef __uint32_t __uint_least32_t;
typedef __int64_t __int_least64_t;
typedef __uint64_t __uint_least64_t;
typedef long __quad_t;
typedef unsigned long __u_quad_t;
typedef long __intmax_t;
typedef unsigned long __uintmax_t;
typedef unsigned long __dev_t;
typedef unsigned int __uid_t;
typedef unsigned int __gid_t;
typedef unsigned long __ino_t;
typedef unsigned long __ino64_t;
typedef unsigned int __mode_t;
typedef unsigned long __nlink_t;
typedef long __off_t;
typedef long __off64_t;
typedef int __pid_t;
typedef struct {
    int __val[2];
} __fsid_t;
typedef long __clock_t;
typedef unsigned long __rlim_t;
typedef unsigned long __rlim64_t;
typedef unsigned int __id_t;
typedef long __time_t;
typedef unsigned int __useconds_t;
typedef long __suseconds_t;
typedef int __daddr_t;
typedef int __key_t;
typedef int __clockid_t;
typedef void *__timer_t;
typedef long __blksize_t;
typedef long __blkcnt_t;
typedef long __blkcnt64_t;
typedef unsigned long __fsblkcnt_t;
typedef unsigned long __fsblkcnt64_t;
typedef unsigned long __fsfilcnt_t;
typedef unsigned long __fsfilcnt64_t;
typedef long __fsword_t;
typedef long __ssize_t;
typedef long __syscall_slong_t;
typedef unsigned long __syscall_ulong_t;
typedef __off64_t __loff_t;
typedef char *__caddr_t;
typedef long __intptr_t;
typedef unsigned int __socklen_t;
typedef int __sig_atomic_t;
typedef __int8_t int8_t;
typedef __int16_t int16_t;
typedef __int32_t int32_t;
typedef __int64_t int64_t;
typedef __uint8_t uint8_t;
typedef __uint16_t uint16_t;
typedef __uint32_t uint32_t;
typedef __uint64_t uint64_t;
typedef __int_least8_t int_least8_t;
typedef __int_least16_t int_least16_t;
typedef __int_least32_t int_least32_t;
typedef __int_least64_t int_least64_t;
typedef __uint_least8_t uint_least8_t;
typedef __uint_least16_t uint_least16_t;
typedef __uint_least32_t uint_least32_t;
typedef __uint_least64_t uint_least64_t;
typedef signed char int_fast8_t;
typedef long int_fast16_t;
typedef long int_fast32_t;
typedef long int_fast64_t;
typedef unsigned char uint_fast8_t;
typedef unsigned long uint_fast16_t;
typedef unsigned long uint_fast32_t;
typedef unsigned long uint_fast64_t;
typedef long intptr_t;
typedef unsigned long uintptr_t;
typedef __intmax_t intmax_t;
typedef __uintmax_t uintmax_t;
typedef _Bool bool;
struct bb_xfields_ret_type;
struct indirect_placeholder_108_ret_type;
struct indirect_placeholder_107_ret_type;
struct indirect_placeholder_109_ret_type;
struct indirect_placeholder_110_ret_type;
struct bb_xfields_ret_type {
    uint64_t field_0;
    uint64_t field_1;
    uint64_t field_2;
    uint64_t field_3;
};
struct indirect_placeholder_108_ret_type {
    uint64_t field_0;
    uint64_t field_1;
    uint64_t field_2;
    uint64_t field_3;
    uint64_t field_4;
};
struct indirect_placeholder_107_ret_type {
    uint64_t field_0;
    uint64_t field_1;
    uint64_t field_2;
    uint64_t field_3;
    uint64_t field_4;
};
struct indirect_placeholder_109_ret_type {
    uint64_t field_0;
    uint64_t field_1;
    uint64_t field_2;
};
struct indirect_placeholder_110_ret_type {
    uint64_t field_0;
    uint64_t field_1;
    uint64_t field_2;
    uint64_t field_3;
    uint64_t field_4;
};
extern void abort(void);
extern uint64_t revng_init_local_sp(uint64_t param_0);
extern uint64_t init_rbx(void);
extern uint64_t init_rbp(void);
extern void indirect_placeholder(void);
extern uint64_t init_cc_src2(void);
extern uint64_t helper_cc_compute_c_wrapper(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint32_t param_3);
extern uint64_t init_r13(void);
extern uint64_t init_r12(void);
extern uint64_t init_r14(void);
extern uint64_t init_r15(void);
extern void indirect_placeholder_15(uint64_t param_0, uint64_t param_1);
extern struct indirect_placeholder_108_ret_type indirect_placeholder_108(uint64_t param_0, uint64_t param_1);
extern uint64_t init_r10(void);
extern uint64_t init_rcx(void);
extern struct indirect_placeholder_107_ret_type indirect_placeholder_107(uint64_t param_0, uint64_t param_1);
extern struct indirect_placeholder_109_ret_type indirect_placeholder_109(uint64_t param_0, uint64_t param_1, uint64_t param_2);
extern struct indirect_placeholder_110_ret_type indirect_placeholder_110(uint64_t param_0, uint64_t param_1);
struct bb_xfields_ret_type bb_xfields(uint64_t rdi, uint64_t r9, uint64_t r8) {
    uint64_t rsi_1;
    uint64_t r14_3;
    uint64_t var_38;
    uint64_t *var_39;
    struct indirect_placeholder_108_ret_type var_40;
    uint64_t var_41;
    uint64_t var_42;
    uint64_t var_43;
    uint64_t var_44;
    uint64_t var_45;
    uint64_t var_46;
    uint64_t rbx_3;
    uint64_t r13_2;
    uint64_t rbx_2;
    uint64_t r10_1;
    uint64_t r12_1;
    uint64_t local_sp_7;
    uint64_t rcx_2;
    uint64_t local_sp_6;
    uint64_t r11_0;
    uint64_t var_47;
    uint64_t var_48;
    uint64_t var_49;
    uint64_t var_24;
    uint32_t var_15;
    uint64_t var_16;
    bool var_17;
    bool var_18;
    uint64_t local_sp_4;
    uint64_t var_0;
    uint64_t var_1;
    uint64_t var_2;
    uint64_t var_3;
    uint64_t var_4;
    uint64_t var_5;
    uint64_t var_6;
    uint64_t var_7;
    uint64_t var_8;
    uint64_t var_9;
    uint64_t var_10;
    uint64_t var_11;
    uint64_t var_12;
    uint64_t var_13;
    uint64_t var_14;
    uint64_t r10_5;
    uint64_t var_91;
    uint64_t var_62;
    bool var_55;
    uint64_t r14_5;
    unsigned char var_56;
    uint64_t var_92;
    uint64_t var_93;
    uint64_t var_94;
    uint64_t rbx_4;
    uint64_t var_95;
    uint64_t var_96;
    uint64_t local_sp_1;
    uint64_t var_68;
    uint64_t r13_1;
    uint64_t r13_0;
    uint64_t *var_59;
    uint64_t *var_60;
    uint64_t *var_61;
    uint64_t rax_0;
    uint64_t rcx_1;
    uint64_t r95_0;
    uint64_t local_sp_3;
    uint64_t rcx_0;
    uint64_t local_sp_0;
    uint64_t r14_0;
    uint64_t var_63;
    uint64_t storemerge_in_sroa_speculated;
    bool var_64;
    unsigned char var_65;
    uint64_t var_66;
    uint64_t *var_67;
    uint64_t local_sp_2;
    uint64_t var_69;
    uint64_t var_70;
    uint64_t var_71;
    uint64_t var_72;
    bool var_73;
    uint64_t var_74;
    uint64_t *_pre_phi228;
    uint64_t r95_3;
    uint64_t var_75;
    uint64_t rax_1;
    uint64_t rax_2;
    uint64_t rsi_0;
    uint64_t var_76;
    uint64_t *var_77;
    struct indirect_placeholder_107_ret_type var_78;
    uint64_t var_79;
    uint64_t var_80;
    uint64_t var_81;
    uint64_t var_82;
    uint64_t r95_1;
    uint64_t r86_0;
    uint64_t *var_25;
    uint64_t var_26;
    uint64_t var_83;
    uint64_t var_84;
    uint64_t var_85;
    uint64_t var_86;
    uint64_t r14_1;
    uint64_t r10_0;
    bool var_87;
    unsigned char var_88;
    uint64_t var_89;
    uint64_t *var_90;
    uint64_t r12_0;
    uint64_t local_sp_5;
    uint64_t r14_2;
    uint64_t var_97;
    uint64_t var_98;
    uint64_t var_109;
    uint64_t local_sp_10;
    uint64_t var_33;
    uint64_t rcx_3;
    uint64_t r10_2;
    uint64_t var_34;
    uint64_t *_phi_trans_insert_pre_phi;
    uint64_t local_sp_9;
    uint64_t var_35;
    uint64_t rbx_0;
    uint64_t rbx_1;
    uint64_t *var_19;
    uint64_t var_20;
    uint64_t *var_21;
    uint64_t var_22;
    uint64_t *var_23;
    uint64_t r13_3;
    uint64_t r12_2;
    uint64_t var_27;
    uint64_t var_28;
    uint64_t *var_29;
    struct indirect_placeholder_109_ret_type var_30;
    uint64_t var_31;
    uint64_t var_32;
    uint64_t r10_3;
    uint64_t r12_3;
    uint64_t var_52;
    uint64_t var_53;
    uint64_t var_54;
    uint64_t var_50;
    uint64_t var_51;
    uint64_t r95_2;
    uint64_t rbp_0;
    uint64_t rcx_4;
    uint64_t local_sp_8;
    uint64_t r14_4;
    uint64_t r86_1;
    uint64_t var_99;
    uint64_t *_pre_phi232;
    uint64_t var_100;
    uint64_t rbx_5;
    uint64_t rbx_6;
    uint64_t r86_2;
    uint64_t var_36;
    uint64_t var_37;
    uint64_t rsi_2;
    struct indirect_placeholder_110_ret_type var_101;
    uint64_t var_102;
    uint64_t var_103;
    uint64_t var_104;
    uint64_t *var_105;
    uint64_t var_106;
    uint64_t r10_4;
    uint64_t r12_4;
    uint64_t r95_4;
    uint64_t rcx_5;
    uint64_t var_107;
    uint64_t var_108;
    uint64_t var_57;
    uint64_t *var_58;
    uint64_t r95_5;
    uint64_t rcx_6;
    uint64_t r86_3;
    struct bb_xfields_ret_type mrv;
    struct bb_xfields_ret_type mrv1;
    struct bb_xfields_ret_type mrv2;
    struct bb_xfields_ret_type mrv3;
    unsigned int loop_state_var;
    bool switch_state_var;
    var_0 = revng_init_local_sp(0UL);
    var_1 = init_r15();
    var_2 = init_r13();
    var_3 = init_rbx();
    var_4 = init_r10();
    var_5 = init_r12();
    var_6 = init_rbp();
    var_7 = init_rcx();
    var_8 = init_cc_src2();
    var_9 = init_r14();
    *(uint64_t *)(var_0 + (-8L)) = var_1;
    *(uint64_t *)(var_0 + (-16L)) = var_9;
    *(uint64_t *)(var_0 + (-24L)) = var_2;
    *(uint64_t *)(var_0 + (-32L)) = var_5;
    *(uint64_t *)(var_0 + (-40L)) = var_6;
    *(uint64_t *)(var_0 + (-48L)) = var_3;
    var_10 = var_0 + (-72L);
    var_11 = *(uint64_t *)(rdi + 8UL);
    var_12 = *(uint64_t *)(rdi + 16UL);
    var_13 = var_11 + (-1L);
    var_14 = var_13 + var_12;
    *(uint64_t *)var_10 = var_14;
    rsi_1 = 128UL;
    r14_3 = var_12;
    local_sp_7 = var_10;
    var_24 = var_14;
    r10_5 = var_4;
    r14_5 = var_12;
    rax_0 = var_14;
    r95_0 = r9;
    rcx_0 = var_7;
    rax_2 = 8UL;
    rsi_0 = 128UL;
    local_sp_10 = var_10;
    rcx_3 = var_7;
    rbx_1 = 8UL;
    r95_2 = r9;
    rbp_0 = 0UL;
    rcx_4 = var_7;
    local_sp_8 = var_10;
    r14_4 = var_12;
    r86_1 = r8;
    rbx_6 = 8UL;
    rsi_2 = 128UL;
    r95_5 = r9;
    rcx_6 = var_7;
    r86_3 = r8;
    if (var_11 == 1UL) {
        mrv.field_0 = r10_5;
        mrv1 = mrv;
        mrv1.field_1 = r95_5;
        mrv2 = mrv1;
        mrv2.field_2 = rcx_6;
        mrv3 = mrv2;
        mrv3.field_3 = r86_3;
        return mrv3;
    }
    var_15 = *(uint32_t *)6386724UL;
    var_16 = (uint64_t)var_15;
    var_17 = ((uint64_t)(var_15 + (-10)) == 0UL);
    var_18 = ((int)var_15 < (int)0U);
    r13_3 = var_16;
    if (!(var_17 || var_18)) {
        if (!var_18) {
            var_52 = *(uint64_t *)(rdi + 24UL);
            var_53 = *(uint64_t *)(rdi + 40UL);
            var_54 = *(uint64_t *)(rdi + 32UL);
            rbx_4 = var_54;
            r10_3 = var_53;
            r12_3 = var_52;
            rbp_0 = var_13;
            var_99 = helper_cc_compute_c_wrapper(r12_3 - rbx_4, rbx_4, var_8, 17U);
            r95_3 = r95_2;
            local_sp_9 = local_sp_8;
            rbx_5 = rbx_4;
            r86_2 = r86_1;
            r10_4 = r10_3;
            r12_4 = r12_3;
            r95_4 = r95_2;
            rcx_5 = rcx_4;
            if (var_99 == 0UL) {
                _pre_phi232 = (uint64_t *)(rdi + 24UL);
            } else {
                if (r10_3 == 0UL) {
                    if (rbx_4 != 0UL) {
                        if (rbx_4 <= 576460752303423487UL) {
                            *(uint64_t *)(local_sp_9 + (-8L)) = 4209229UL;
                            indirect_placeholder_15(r95_3, r86_2);
                            abort();
                        }
                        rbx_6 = rbx_5;
                        rsi_2 = rbx_5 << 4UL;
                    }
                } else {
                    if (rbx_4 > 384307168202282324UL) {
                        *(uint64_t *)(local_sp_9 + (-8L)) = 4209229UL;
                        indirect_placeholder_15(r95_3, r86_2);
                        abort();
                    }
                    var_100 = (rbx_4 + (rbx_4 >> 1UL)) + 1UL;
                    rbx_5 = var_100;
                    rbx_6 = rbx_5;
                    rsi_2 = rbx_5 << 4UL;
                }
                *(uint64_t *)(rdi + 32UL) = rbx_6;
                *(uint64_t *)(local_sp_8 + (-8L)) = 4208716UL;
                var_101 = indirect_placeholder_110(r10_3, rsi_2);
                var_102 = var_101.field_0;
                var_103 = var_101.field_2;
                var_104 = var_101.field_3;
                var_105 = (uint64_t *)(rdi + 24UL);
                var_106 = *var_105;
                *(uint64_t *)(rdi + 40UL) = var_102;
                _pre_phi232 = var_105;
                r10_4 = var_102;
                r12_4 = var_106;
                r95_4 = var_103;
                rcx_5 = var_104;
            }
            var_107 = r12_4 + 1UL;
            var_108 = r10_4 + (r12_4 << 4UL);
            *(uint64_t *)var_108 = r14_4;
            *(uint64_t *)(var_108 + 8UL) = rbp_0;
            *_pre_phi232 = var_107;
            r10_5 = var_108;
            r95_5 = r95_4;
            rcx_6 = rcx_5;
            r86_3 = var_107;
            mrv.field_0 = r10_5;
            mrv1 = mrv;
            mrv1.field_1 = r95_5;
            mrv2 = mrv1;
            mrv2.field_2 = rcx_6;
            mrv3 = mrv2;
            mrv3.field_3 = r86_3;
            return mrv3;
        }
        var_55 = ((uint64_t)(uint32_t)var_14 == 0UL);
        while (1U)
            {
                var_56 = *(unsigned char *)r14_5;
                var_57 = local_sp_10 + (-8L);
                var_58 = (uint64_t *)var_57;
                *var_58 = 4208821UL;
                indirect_placeholder();
                local_sp_0 = var_57;
                r14_0 = r14_5;
                local_sp_10 = var_57;
                if ((var_56 == '\n') || (var_55 ^ 1)) {
                    var_109 = r14_5 + 1UL;
                    r14_5 = var_109;
                    if (var_109 == *var_58) {
                        continue;
                    }
                    loop_state_var = 0U;
                    break;
                }
                var_59 = (uint64_t *)(rdi + 24UL);
                var_60 = (uint64_t *)(rdi + 32UL);
                var_61 = (uint64_t *)(rdi + 40UL);
                var_62 = *var_58;
                while (1U)
                    {
                        var_63 = r14_0 + 1UL;
                        local_sp_1 = local_sp_0;
                        r13_1 = var_62;
                        r13_0 = var_63;
                        rcx_1 = rcx_0;
                        local_sp_2 = local_sp_0;
                        r95_3 = r95_0;
                        r95_1 = r95_0;
                        if (var_62 != var_63) {
                            var_64 = ((uint64_t)(uint32_t)rax_0 == 0UL);
                            var_65 = *(unsigned char *)r13_0;
                            var_66 = local_sp_1 + (-8L);
                            var_67 = (uint64_t *)var_66;
                            *var_67 = 4208879UL;
                            indirect_placeholder();
                            local_sp_1 = var_66;
                            r13_1 = r13_0;
                            local_sp_2 = var_66;
                            while (!((var_65 != '\n') && var_64))
                                {
                                    var_68 = r13_0 + 1UL;
                                    r13_0 = var_68;
                                    r13_1 = var_68;
                                    if (*var_67 == var_68) {
                                        break;
                                    }
                                    var_65 = *(unsigned char *)r13_0;
                                    var_66 = local_sp_1 + (-8L);
                                    var_67 = (uint64_t *)var_66;
                                    *var_67 = 4208879UL;
                                    indirect_placeholder();
                                    local_sp_1 = var_66;
                                    r13_1 = r13_0;
                                    local_sp_2 = var_66;
                                }
                        }
                        var_69 = *var_59;
                        var_70 = *var_60;
                        var_71 = r13_1 - r14_0;
                        var_72 = helper_cc_compute_c_wrapper(var_69 - var_70, var_70, var_8, 17U);
                        var_73 = (var_72 == 0UL);
                        var_74 = *var_61;
                        local_sp_3 = local_sp_2;
                        storemerge_in_sroa_speculated = var_74;
                        rax_1 = var_70;
                        r86_0 = var_69;
                        local_sp_9 = local_sp_2;
                        r86_2 = var_69;
                        if (var_73) {
                            _pre_phi228 = (uint64_t *)local_sp_2;
                        } else {
                            if (var_74 == 0UL) {
                                if (var_70 != 0UL) {
                                    if (var_70 <= 576460752303423487UL) {
                                        loop_state_var = 1U;
                                        break;
                                    }
                                    rax_2 = rax_1;
                                    rsi_0 = rax_1 << 4UL;
                                }
                            } else {
                                if (var_70 <= 384307168202282324UL) {
                                    loop_state_var = 1U;
                                    break;
                                }
                                var_75 = (var_70 + (var_70 >> 1UL)) + 1UL;
                                rax_1 = var_75;
                                rax_2 = rax_1;
                                rsi_0 = rax_1 << 4UL;
                            }
                            *var_60 = rax_2;
                            var_76 = local_sp_2 + (-8L);
                            var_77 = (uint64_t *)var_76;
                            *var_77 = 4208976UL;
                            var_78 = indirect_placeholder_107(var_74, rsi_0);
                            var_79 = var_78.field_0;
                            var_80 = var_78.field_2;
                            var_81 = var_78.field_3;
                            var_82 = *var_59;
                            *var_61 = var_79;
                            rcx_1 = var_81;
                            local_sp_3 = var_76;
                            storemerge_in_sroa_speculated = var_79;
                            _pre_phi228 = var_77;
                            r95_1 = var_80;
                            r86_0 = var_82;
                        }
                        var_83 = (r86_0 << 4UL) + storemerge_in_sroa_speculated;
                        *(uint64_t *)(var_83 + 8UL) = var_71;
                        *(uint64_t *)var_83 = r14_0;
                        var_84 = r86_0 + 1UL;
                        var_85 = *_pre_phi228;
                        *var_59 = var_84;
                        local_sp_4 = local_sp_3;
                        r10_5 = storemerge_in_sroa_speculated;
                        rax_0 = var_85;
                        r95_0 = r95_1;
                        rcx_0 = rcx_1;
                        r10_0 = storemerge_in_sroa_speculated;
                        r12_0 = var_84;
                        local_sp_5 = local_sp_3;
                        r14_2 = var_85;
                        r95_2 = r95_1;
                        rcx_4 = rcx_1;
                        r86_1 = r86_0;
                        r95_5 = r95_1;
                        rcx_6 = rcx_1;
                        r86_3 = r86_0;
                        if (r13_1 != var_85) {
                            loop_state_var = 0U;
                            break;
                        }
                        var_86 = r13_1 + 1UL;
                        r14_1 = var_86;
                        if (var_85 != var_86) {
                            var_96 = *_pre_phi228;
                            loop_state_var = 3U;
                            break;
                        }
                        var_87 = ((uint64_t)(uint32_t)var_85 == 0UL);
                        while (1U)
                            {
                                var_88 = *(unsigned char *)r14_1;
                                var_89 = local_sp_4 + (-8L);
                                var_90 = (uint64_t *)var_89;
                                *var_90 = 4209046UL;
                                indirect_placeholder();
                                local_sp_4 = var_89;
                                local_sp_0 = var_89;
                                r14_0 = r14_1;
                                local_sp_5 = var_89;
                                local_sp_8 = var_89;
                                r14_4 = r14_1;
                                if (!((var_88 == '\n') || (var_87 ^ 1))) {
                                    loop_state_var = 0U;
                                    break;
                                }
                                var_95 = r14_1 + 1UL;
                                r14_1 = var_95;
                                var_96 = var_95;
                                r14_2 = var_95;
                                if (*var_90 == var_95) {
                                    continue;
                                }
                                r10_0 = *var_61;
                                r12_0 = *var_59;
                                loop_state_var = 1U;
                                break;
                            }
                        switch_state_var = 0;
                        switch (loop_state_var) {
                          case 0U:
                            {
                                var_91 = *var_90;
                                var_62 = var_91;
                                if (var_91 == r14_1) {
                                    continue;
                                }
                                var_92 = *var_59;
                                var_93 = *var_61;
                                var_94 = *var_60;
                                rbx_4 = var_94;
                                r10_3 = var_93;
                                r12_3 = var_92;
                                loop_state_var = 2U;
                                switch_state_var = 1;
                                break;
                            }
                            break;
                          case 1U:
                            {
                                loop_state_var = 3U;
                                switch_state_var = 1;
                                break;
                            }
                            break;
                        }
                        if (switch_state_var)
                            break;
                    }
                switch_state_var = 0;
                switch (loop_state_var) {
                  case 3U:
                    {
                        var_97 = *var_60;
                        var_98 = var_96 - r14_2;
                        rbx_4 = var_97;
                        r10_3 = r10_0;
                        r12_3 = r12_0;
                        rbp_0 = var_98;
                        local_sp_8 = local_sp_5;
                        r14_4 = r14_2;
                        loop_state_var = 2U;
                        switch_state_var = 1;
                        break;
                    }
                    break;
                  case 0U:
                    {
                        loop_state_var = 0U;
                        switch_state_var = 1;
                        break;
                    }
                    break;
                  case 1U:
                    {
                        loop_state_var = 1U;
                        switch_state_var = 1;
                        break;
                    }
                    break;
                  case 2U:
                    {
                        loop_state_var = 2U;
                        switch_state_var = 1;
                        break;
                    }
                    break;
                }
                if (switch_state_var)
                    break;
            }
        switch (loop_state_var) {
          case 0U:
            {
                mrv.field_0 = r10_5;
                mrv1 = mrv;
                mrv1.field_1 = r95_5;
                mrv2 = mrv1;
                mrv2.field_2 = rcx_6;
                mrv3 = mrv2;
                mrv3.field_3 = r86_3;
                return mrv3;
            }
            break;
          case 1U:
            {
                *(uint64_t *)(local_sp_9 + (-8L)) = 4209229UL;
                indirect_placeholder_15(r95_3, r86_2);
                abort();
            }
            break;
          case 2U:
            {
                var_99 = helper_cc_compute_c_wrapper(r12_3 - rbx_4, rbx_4, var_8, 17U);
                r95_3 = r95_2;
                local_sp_9 = local_sp_8;
                rbx_5 = rbx_4;
                r86_2 = r86_1;
                r10_4 = r10_3;
                r12_4 = r12_3;
                r95_4 = r95_2;
                rcx_5 = rcx_4;
                if (var_99 == 0UL) {
                    _pre_phi232 = (uint64_t *)(rdi + 24UL);
                } else {
                    if (r10_3 == 0UL) {
                        if (rbx_4 != 0UL) {
                            if (rbx_4 <= 576460752303423487UL) {
                                *(uint64_t *)(local_sp_9 + (-8L)) = 4209229UL;
                                indirect_placeholder_15(r95_3, r86_2);
                                abort();
                            }
                            rbx_6 = rbx_5;
                            rsi_2 = rbx_5 << 4UL;
                        }
                    } else {
                        if (rbx_4 > 384307168202282324UL) {
                            *(uint64_t *)(local_sp_9 + (-8L)) = 4209229UL;
                            indirect_placeholder_15(r95_3, r86_2);
                            abort();
                        }
                        var_100 = (rbx_4 + (rbx_4 >> 1UL)) + 1UL;
                        rbx_5 = var_100;
                        rbx_6 = rbx_5;
                        rsi_2 = rbx_5 << 4UL;
                    }
                    *(uint64_t *)(rdi + 32UL) = rbx_6;
                    *(uint64_t *)(local_sp_8 + (-8L)) = 4208716UL;
                    var_101 = indirect_placeholder_110(r10_3, rsi_2);
                    var_102 = var_101.field_0;
                    var_103 = var_101.field_2;
                    var_104 = var_101.field_3;
                    var_105 = (uint64_t *)(rdi + 24UL);
                    var_106 = *var_105;
                    *(uint64_t *)(rdi + 40UL) = var_102;
                    _pre_phi232 = var_105;
                    r10_4 = var_102;
                    r12_4 = var_106;
                    r95_4 = var_103;
                    rcx_5 = var_104;
                }
                var_107 = r12_4 + 1UL;
                var_108 = r10_4 + (r12_4 << 4UL);
                *(uint64_t *)var_108 = r14_4;
                *(uint64_t *)(var_108 + 8UL) = rbp_0;
                *_pre_phi232 = var_107;
                r10_5 = var_108;
                r95_5 = r95_4;
                rcx_6 = rcx_5;
                r86_3 = var_107;
                mrv.field_0 = r10_5;
                mrv1 = mrv;
                mrv1.field_1 = r95_5;
                mrv2 = mrv1;
                mrv2.field_2 = rcx_6;
                mrv3 = mrv2;
                mrv3.field_3 = r86_3;
                return mrv3;
            }
            break;
        }
    }
    var_19 = (uint64_t *)(rdi + 24UL);
    var_20 = *var_19;
    var_21 = (uint64_t *)(rdi + 40UL);
    var_22 = *var_21;
    var_23 = (uint64_t *)(rdi + 32UL);
    rbx_3 = *var_23;
    r10_2 = var_22;
    r12_2 = var_20;
    while (1U)
        {
            var_25 = (uint64_t *)local_sp_7;
            var_26 = (uint64_t)(uint32_t)r13_3;
            *(uint64_t *)(local_sp_7 + 8UL) = r10_2;
            var_27 = var_24 - r14_3;
            var_28 = local_sp_7 + (-8L);
            var_29 = (uint64_t *)var_28;
            *var_29 = 4208636UL;
            var_30 = indirect_placeholder_109(r14_3, var_27, var_26);
            var_31 = var_30.field_0;
            var_32 = *var_25;
            r13_2 = r13_3;
            rbx_2 = rbx_3;
            r10_1 = var_32;
            r12_1 = r12_2;
            rcx_2 = rcx_3;
            local_sp_6 = var_28;
            r11_0 = var_31;
            rbx_4 = rbx_3;
            _phi_trans_insert_pre_phi = var_29;
            local_sp_9 = var_28;
            rbx_0 = rbx_3;
            r10_3 = var_32;
            r12_3 = r12_2;
            rbp_0 = var_27;
            rcx_4 = rcx_3;
            local_sp_8 = var_28;
            r14_4 = r14_3;
            if (var_31 != 0UL) {
                var_50 = var_30.field_1;
                var_51 = var_30.field_2;
                r95_2 = var_50;
                r86_1 = var_51;
                loop_state_var = 1U;
                break;
            }
            var_33 = var_31 - r14_3;
            var_34 = helper_cc_compute_c_wrapper(r12_2 - rbx_3, rbx_3, var_8, 17U);
            if (var_34 != 0UL) {
                if (var_32 == 0UL) {
                    if (rbx_3 != 0UL) {
                        if (rbx_3 <= 576460752303423487UL) {
                            loop_state_var = 0U;
                            break;
                        }
                        rbx_1 = rbx_0;
                        rsi_1 = rbx_0 << 4UL;
                    }
                } else {
                    if (rbx_3 <= 384307168202282324UL) {
                        loop_state_var = 0U;
                        break;
                    }
                    var_35 = (rbx_3 + (rbx_3 >> 1UL)) + 1UL;
                    rbx_0 = var_35;
                    rbx_1 = rbx_0;
                    rsi_1 = rbx_0 << 4UL;
                }
                *var_23 = rbx_1;
                *var_25 = var_31;
                var_38 = local_sp_7 + (-16L);
                var_39 = (uint64_t *)var_38;
                *var_39 = 4208554UL;
                var_40 = indirect_placeholder_108(var_32, rsi_1);
                var_41 = var_40.field_0;
                var_42 = var_40.field_3;
                var_43 = *var_19;
                var_44 = (uint64_t)*(uint32_t *)6386724UL;
                var_45 = *var_23;
                var_46 = *var_29;
                *var_21 = var_41;
                r13_2 = var_44;
                rbx_2 = var_45;
                r10_1 = var_41;
                r12_1 = var_43;
                rcx_2 = var_42;
                local_sp_6 = var_38;
                r11_0 = var_46;
                _phi_trans_insert_pre_phi = var_39;
            }
            var_47 = r12_1 + 1UL;
            var_48 = (r12_1 << 4UL) + r10_1;
            *(uint64_t *)var_48 = r14_3;
            var_49 = r11_0 + 1UL;
            *(uint64_t *)(var_48 + 8UL) = var_33;
            *var_19 = var_47;
            r14_3 = var_49;
            rbx_3 = rbx_2;
            local_sp_7 = local_sp_6;
            var_24 = *_phi_trans_insert_pre_phi;
            rcx_3 = rcx_2;
            r10_2 = r10_1;
            r13_3 = r13_2;
            r12_2 = var_47;
            continue;
        }
    switch (loop_state_var) {
      case 1U:
        {
            var_99 = helper_cc_compute_c_wrapper(r12_3 - rbx_4, rbx_4, var_8, 17U);
            r95_3 = r95_2;
            local_sp_9 = local_sp_8;
            rbx_5 = rbx_4;
            r86_2 = r86_1;
            r10_4 = r10_3;
            r12_4 = r12_3;
            r95_4 = r95_2;
            rcx_5 = rcx_4;
            if (var_99 == 0UL) {
                _pre_phi232 = (uint64_t *)(rdi + 24UL);
            } else {
                if (r10_3 == 0UL) {
                    if (rbx_4 != 0UL) {
                        if (rbx_4 <= 576460752303423487UL) {
                            *(uint64_t *)(local_sp_9 + (-8L)) = 4209229UL;
                            indirect_placeholder_15(r95_3, r86_2);
                            abort();
                        }
                        rbx_6 = rbx_5;
                        rsi_2 = rbx_5 << 4UL;
                    }
                } else {
                    if (rbx_4 > 384307168202282324UL) {
                        *(uint64_t *)(local_sp_9 + (-8L)) = 4209229UL;
                        indirect_placeholder_15(r95_3, r86_2);
                        abort();
                    }
                    var_100 = (rbx_4 + (rbx_4 >> 1UL)) + 1UL;
                    rbx_5 = var_100;
                    rbx_6 = rbx_5;
                    rsi_2 = rbx_5 << 4UL;
                }
                *(uint64_t *)(rdi + 32UL) = rbx_6;
                *(uint64_t *)(local_sp_8 + (-8L)) = 4208716UL;
                var_101 = indirect_placeholder_110(r10_3, rsi_2);
                var_102 = var_101.field_0;
                var_103 = var_101.field_2;
                var_104 = var_101.field_3;
                var_105 = (uint64_t *)(rdi + 24UL);
                var_106 = *var_105;
                *(uint64_t *)(rdi + 40UL) = var_102;
                _pre_phi232 = var_105;
                r10_4 = var_102;
                r12_4 = var_106;
                r95_4 = var_103;
                rcx_5 = var_104;
            }
            var_107 = r12_4 + 1UL;
            var_108 = r10_4 + (r12_4 << 4UL);
            *(uint64_t *)var_108 = r14_4;
            *(uint64_t *)(var_108 + 8UL) = rbp_0;
            *_pre_phi232 = var_107;
            r10_5 = var_108;
            r95_5 = r95_4;
            rcx_6 = rcx_5;
            r86_3 = var_107;
        }
        break;
      case 0U:
        {
            var_36 = var_30.field_1;
            var_37 = var_30.field_2;
            r95_3 = var_36;
            r86_2 = var_37;
            *(uint64_t *)(local_sp_9 + (-8L)) = 4209229UL;
            indirect_placeholder_15(r95_3, r86_2);
            abort();
        }
        break;
    }
}

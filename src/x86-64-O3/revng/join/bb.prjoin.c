typedef unsigned char __u_char;
typedef unsigned short __u_short;
typedef unsigned int __u_int;
typedef unsigned long __u_long;
typedef signed char __int8_t;
typedef unsigned char __uint8_t;
typedef short __int16_t;
typedef unsigned short __uint16_t;
typedef int __int32_t;
typedef unsigned int __uint32_t;
typedef long __int64_t;
typedef unsigned long __uint64_t;
typedef __int8_t __int_least8_t;
typedef __uint8_t __uint_least8_t;
typedef __int16_t __int_least16_t;
typedef __uint16_t __uint_least16_t;
typedef __int32_t __int_least32_t;
typedef __uint32_t __uint_least32_t;
typedef __int64_t __int_least64_t;
typedef __uint64_t __uint_least64_t;
typedef long __quad_t;
typedef unsigned long __u_quad_t;
typedef long __intmax_t;
typedef unsigned long __uintmax_t;
typedef unsigned long __dev_t;
typedef unsigned int __uid_t;
typedef unsigned int __gid_t;
typedef unsigned long __ino_t;
typedef unsigned long __ino64_t;
typedef unsigned int __mode_t;
typedef unsigned long __nlink_t;
typedef long __off_t;
typedef long __off64_t;
typedef int __pid_t;
typedef struct {
    int __val[2];
} __fsid_t;
typedef long __clock_t;
typedef unsigned long __rlim_t;
typedef unsigned long __rlim64_t;
typedef unsigned int __id_t;
typedef long __time_t;
typedef unsigned int __useconds_t;
typedef long __suseconds_t;
typedef int __daddr_t;
typedef int __key_t;
typedef int __clockid_t;
typedef void *__timer_t;
typedef long __blksize_t;
typedef long __blkcnt_t;
typedef long __blkcnt64_t;
typedef unsigned long __fsblkcnt_t;
typedef unsigned long __fsblkcnt64_t;
typedef unsigned long __fsfilcnt_t;
typedef unsigned long __fsfilcnt64_t;
typedef long __fsword_t;
typedef long __ssize_t;
typedef long __syscall_slong_t;
typedef unsigned long __syscall_ulong_t;
typedef __off64_t __loff_t;
typedef char *__caddr_t;
typedef long __intptr_t;
typedef unsigned int __socklen_t;
typedef int __sig_atomic_t;
typedef __int8_t int8_t;
typedef __int16_t int16_t;
typedef __int32_t int32_t;
typedef __int64_t int64_t;
typedef __uint8_t uint8_t;
typedef __uint16_t uint16_t;
typedef __uint32_t uint32_t;
typedef __uint64_t uint64_t;
typedef __int_least8_t int_least8_t;
typedef __int_least16_t int_least16_t;
typedef __int_least32_t int_least32_t;
typedef __int_least64_t int_least64_t;
typedef __uint_least8_t uint_least8_t;
typedef __uint_least16_t uint_least16_t;
typedef __uint_least32_t uint_least32_t;
typedef __uint_least64_t uint_least64_t;
typedef signed char int_fast8_t;
typedef long int_fast16_t;
typedef long int_fast32_t;
typedef long int_fast64_t;
typedef unsigned char uint_fast8_t;
typedef unsigned long uint_fast16_t;
typedef unsigned long uint_fast32_t;
typedef unsigned long uint_fast64_t;
typedef long intptr_t;
typedef unsigned long uintptr_t;
typedef __intmax_t intmax_t;
typedef __uintmax_t uintmax_t;
typedef _Bool bool;
extern uint64_t revng_init_local_sp(uint64_t param_0);
extern uint64_t init_rbx(void);
extern uint64_t init_rbp(void);
extern void indirect_placeholder(void);
extern uint64_t init_cc_src2(void);
extern uint64_t helper_cc_compute_c_wrapper(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint32_t param_3);
extern uint64_t init_r13(void);
extern uint64_t init_r12(void);
extern uint64_t init_rcx(void);
uint64_t bb_prjoin(uint64_t rdi, uint64_t rsi) {
    uint64_t var_24;
    uint64_t var_25;
    uint64_t rcx_4;
    uint64_t rax_0;
    uint64_t var_0;
    uint64_t var_1;
    uint64_t var_2;
    uint64_t var_3;
    uint64_t var_4;
    uint64_t var_5;
    uint64_t var_6;
    uint64_t var_7;
    uint64_t var_8;
    uint64_t rbx_0;
    uint64_t local_sp_1;
    uint64_t rcx_0;
    uint64_t local_sp_0;
    uint32_t var_18;
    bool var_19;
    uint64_t *rax_0_in;
    uint64_t storemerge;
    uint64_t local_sp_2;
    uint64_t var_20;
    uint64_t var_21;
    uint64_t var_22;
    uint64_t var_23;
    uint64_t rcx_1;
    uint64_t var_26;
    uint64_t rbx_1;
    uint64_t local_sp_3;
    uint64_t rcx_2;
    uint32_t var_9;
    bool var_10;
    uint64_t *rax_1_in;
    uint64_t storemerge1;
    uint64_t rax_1;
    uint64_t var_11;
    uint64_t var_12;
    uint64_t var_13;
    uint64_t var_14;
    uint64_t rcx_3;
    uint64_t var_15;
    uint64_t var_16;
    uint64_t var_17;
    var_0 = revng_init_local_sp(0UL);
    var_1 = init_r13();
    var_2 = init_rbx();
    var_3 = init_r12();
    var_4 = init_rbp();
    var_5 = init_rcx();
    var_6 = init_cc_src2();
    *(uint64_t *)(var_0 + (-8L)) = var_1;
    *(uint64_t *)(var_0 + (-16L)) = var_3;
    *(uint64_t *)(var_0 + (-24L)) = var_4;
    *(uint64_t *)(var_0 + (-32L)) = var_2;
    var_7 = var_0 + (-40L);
    var_8 = *(uint64_t *)6387152UL;
    rbx_0 = var_8;
    rcx_0 = var_5;
    local_sp_0 = var_7;
    local_sp_2 = var_7;
    rbx_1 = var_8;
    rcx_2 = var_5;
    if (var_8 == 0UL) {
        indirect_placeholder();
        return var_5;
    }
    if (rdi != 6387072UL) {
        while (1U)
            {
                var_18 = *(uint32_t *)rbx_0;
                var_19 = (var_18 == 0U);
                rax_0_in = var_19 ? 6386736UL : (uint64_t *)(rbx_0 + 8UL);
                storemerge = var_19 ? rsi : ((var_18 == 1U) ? 6387072UL : rsi);
                rax_0 = *rax_0_in;
                var_20 = *(uint64_t *)(storemerge + 24UL);
                var_21 = helper_cc_compute_c_wrapper(rax_0 - var_20, var_20, var_6, 17U);
                rcx_1 = rcx_0;
                local_sp_1 = local_sp_0;
                if (var_21 == 0UL) {
                    if (*(uint64_t *)(((rax_0 << 4UL) + *(uint64_t *)(storemerge + 40UL)) + 8UL) == 0UL) {
                        var_22 = *(uint64_t *)6386880UL;
                        var_23 = local_sp_0 + (-8L);
                        *(uint64_t *)var_23 = 4207772UL;
                        indirect_placeholder();
                        rcx_1 = var_22;
                        local_sp_1 = var_23;
                    } else {
                        if (*(uint64_t *)6387184UL == 0UL) {
                            var_24 = local_sp_0 + (-8L);
                            *(uint64_t *)var_24 = 4207703UL;
                            indirect_placeholder();
                            local_sp_1 = var_24;
                        }
                    }
                } else {
                    if (*(uint64_t *)6387184UL == 0UL) {
                        var_24 = local_sp_0 + (-8L);
                        *(uint64_t *)var_24 = 4207703UL;
                        indirect_placeholder();
                        local_sp_1 = var_24;
                    }
                }
                var_25 = *(uint64_t *)(rbx_0 + 16UL);
                rbx_0 = var_25;
                rcx_0 = rcx_1;
                rcx_4 = rcx_1;
                if (var_25 == 0UL) {
                    break;
                }
                var_26 = local_sp_1 + (-8L);
                *(uint64_t *)var_26 = 4207723UL;
                indirect_placeholder();
                local_sp_0 = var_26;
                continue;
            }
    }
    while (1U)
        {
            var_9 = *(uint32_t *)rbx_1;
            var_10 = (var_9 == 0U);
            rax_1_in = var_10 ? 6386744UL : (uint64_t *)(rbx_1 + 8UL);
            storemerge1 = var_10 ? rdi : ((var_9 == 1U) ? rdi : rsi);
            rax_1 = *rax_1_in;
            var_11 = *(uint64_t *)(storemerge1 + 24UL);
            var_12 = helper_cc_compute_c_wrapper(rax_1 - var_11, var_11, var_6, 17U);
            rcx_3 = rcx_2;
            local_sp_3 = local_sp_2;
            if (var_12 == 0UL) {
                if (*(uint64_t *)(((rax_1 << 4UL) + *(uint64_t *)(storemerge1 + 40UL)) + 8UL) == 0UL) {
                    var_13 = *(uint64_t *)6386880UL;
                    var_14 = local_sp_2 + (-8L);
                    *(uint64_t *)var_14 = 4207604UL;
                    indirect_placeholder();
                    rcx_3 = var_13;
                    local_sp_3 = var_14;
                } else {
                    if (*(uint64_t *)6387184UL == 0UL) {
                        var_15 = local_sp_2 + (-8L);
                        *(uint64_t *)var_15 = 4207505UL;
                        indirect_placeholder();
                        local_sp_3 = var_15;
                    }
                }
            } else {
                if (*(uint64_t *)6387184UL == 0UL) {
                    var_15 = local_sp_2 + (-8L);
                    *(uint64_t *)var_15 = 4207505UL;
                    indirect_placeholder();
                    local_sp_3 = var_15;
                }
            }
            var_16 = *(uint64_t *)(rbx_1 + 16UL);
            rbx_1 = var_16;
            rcx_2 = rcx_3;
            rcx_4 = rcx_3;
            if (var_16 == 0UL) {
                break;
            }
            var_17 = local_sp_3 + (-8L);
            *(uint64_t *)var_17 = 4207522UL;
            indirect_placeholder();
            local_sp_2 = var_17;
            continue;
        }
    indirect_placeholder();
    return rcx_4;
}

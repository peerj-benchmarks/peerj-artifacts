typedef unsigned char __u_char;
typedef unsigned short __u_short;
typedef unsigned int __u_int;
typedef unsigned long __u_long;
typedef signed char __int8_t;
typedef unsigned char __uint8_t;
typedef short __int16_t;
typedef unsigned short __uint16_t;
typedef int __int32_t;
typedef unsigned int __uint32_t;
typedef long __int64_t;
typedef unsigned long __uint64_t;
typedef __int8_t __int_least8_t;
typedef __uint8_t __uint_least8_t;
typedef __int16_t __int_least16_t;
typedef __uint16_t __uint_least16_t;
typedef __int32_t __int_least32_t;
typedef __uint32_t __uint_least32_t;
typedef __int64_t __int_least64_t;
typedef __uint64_t __uint_least64_t;
typedef long __quad_t;
typedef unsigned long __u_quad_t;
typedef long __intmax_t;
typedef unsigned long __uintmax_t;
typedef unsigned long __dev_t;
typedef unsigned int __uid_t;
typedef unsigned int __gid_t;
typedef unsigned long __ino_t;
typedef unsigned long __ino64_t;
typedef unsigned int __mode_t;
typedef unsigned long __nlink_t;
typedef long __off_t;
typedef long __off64_t;
typedef int __pid_t;
typedef struct {
    int __val[2];
} __fsid_t;
typedef long __clock_t;
typedef unsigned long __rlim_t;
typedef unsigned long __rlim64_t;
typedef unsigned int __id_t;
typedef long __time_t;
typedef unsigned int __useconds_t;
typedef long __suseconds_t;
typedef int __daddr_t;
typedef int __key_t;
typedef int __clockid_t;
typedef void *__timer_t;
typedef long __blksize_t;
typedef long __blkcnt_t;
typedef long __blkcnt64_t;
typedef unsigned long __fsblkcnt_t;
typedef unsigned long __fsblkcnt64_t;
typedef unsigned long __fsfilcnt_t;
typedef unsigned long __fsfilcnt64_t;
typedef long __fsword_t;
typedef long __ssize_t;
typedef long __syscall_slong_t;
typedef unsigned long __syscall_ulong_t;
typedef __off64_t __loff_t;
typedef char *__caddr_t;
typedef long __intptr_t;
typedef unsigned int __socklen_t;
typedef int __sig_atomic_t;
typedef __int8_t int8_t;
typedef __int16_t int16_t;
typedef __int32_t int32_t;
typedef __int64_t int64_t;
typedef __uint8_t uint8_t;
typedef __uint16_t uint16_t;
typedef __uint32_t uint32_t;
typedef __uint64_t uint64_t;
typedef __int_least8_t int_least8_t;
typedef __int_least16_t int_least16_t;
typedef __int_least32_t int_least32_t;
typedef __int_least64_t int_least64_t;
typedef __uint_least8_t uint_least8_t;
typedef __uint_least16_t uint_least16_t;
typedef __uint_least32_t uint_least32_t;
typedef __uint_least64_t uint_least64_t;
typedef signed char int_fast8_t;
typedef long int_fast16_t;
typedef long int_fast32_t;
typedef long int_fast64_t;
typedef unsigned char uint_fast8_t;
typedef unsigned long uint_fast16_t;
typedef unsigned long uint_fast32_t;
typedef unsigned long uint_fast64_t;
typedef long intptr_t;
typedef unsigned long uintptr_t;
typedef __intmax_t intmax_t;
typedef __uintmax_t uintmax_t;
extern void abort(void);
extern uint64_t revng_init_local_sp(uint64_t param_0);
extern uint64_t init_rbx(void);
extern uint64_t init_rbp(void);
extern void indirect_placeholder(void);
extern uint64_t init_cc_src2(void);
extern uint64_t helper_cc_compute_c_wrapper(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint32_t param_3);
extern uint64_t init_r13(void);
extern uint64_t init_r12(void);
extern uint64_t init_r14(void);
extern uint64_t init_rax(void);
extern uint64_t init_r15(void);
extern uint64_t indirect_placeholder_16(uint64_t param_0, uint64_t param_1);
extern void indirect_placeholder_14(uint64_t param_0, uint64_t param_1);
extern uint64_t init_r9(void);
uint64_t bb_readtoken(uint64_t rdi, uint64_t rcx, uint64_t rdx, uint64_t rsi) {
    uint64_t var_0;
    uint64_t var_1;
    uint64_t var_2;
    uint64_t var_3;
    uint64_t var_4;
    uint64_t var_5;
    uint64_t var_6;
    uint64_t var_7;
    uint64_t var_8;
    uint64_t var_9;
    uint64_t var_10;
    uint64_t var_11;
    uint64_t rax_0;
    uint64_t rsi4_0;
    uint64_t local_sp_0;
    uint64_t r14_0;
    uint64_t var_21;
    uint64_t var_22;
    uint64_t var_23;
    uint64_t var_24;
    uint64_t r15_0;
    uint64_t r12_2;
    uint64_t rbx_0;
    uint64_t local_sp_2;
    uint64_t r12_0;
    uint64_t local_sp_1;
    uint64_t r14_1;
    uint64_t r12_1;
    uint64_t var_25;
    uint64_t var_26;
    uint64_t rbx_1;
    uint64_t rbx_2;
    uint64_t var_27;
    uint64_t var_28;
    uint64_t var_29;
    uint64_t var_31;
    uint64_t var_32;
    uint32_t var_33;
    uint64_t r12_3;
    uint64_t local_sp_3;
    uint64_t var_30;
    uint64_t var_12;
    uint64_t var_13;
    uint64_t var_14;
    uint64_t var_15;
    uint64_t *var_16;
    uint64_t var_17;
    uint64_t local_sp_4;
    uint64_t var_18;
    uint32_t var_19;
    uint64_t var_20;
    unsigned int loop_state_var;
    var_0 = revng_init_local_sp(0UL);
    var_1 = init_rax();
    var_2 = init_r15();
    var_3 = init_r13();
    var_4 = init_rbx();
    var_5 = init_r12();
    var_6 = init_r9();
    var_7 = init_rbp();
    var_8 = init_cc_src2();
    var_9 = init_r14();
    *(uint64_t *)(var_0 + (-8L)) = var_2;
    *(uint64_t *)(var_0 + (-16L)) = var_9;
    *(uint64_t *)(var_0 + (-24L)) = var_3;
    *(uint64_t *)(var_0 + (-32L)) = var_5;
    *(uint64_t *)(var_0 + (-40L)) = var_7;
    *(uint64_t *)(var_0 + (-48L)) = var_4;
    *(uint64_t *)(var_0 + (-96L)) = rcx;
    var_10 = var_0 + (-112L);
    *(uint64_t *)var_10 = 4226333UL;
    indirect_placeholder();
    var_11 = rdx + rsi;
    rax_0 = var_1;
    rsi4_0 = rsi;
    r15_0 = 0UL;
    r12_1 = 128UL;
    local_sp_4 = var_10;
    if (rdx == 0UL) {
        var_12 = (uint64_t)*(unsigned char *)rsi4_0;
        var_13 = rsi4_0 + 1UL;
        var_14 = 1UL << (var_12 & 63UL);
        var_15 = (var_12 >> 3UL) & 24UL;
        var_16 = (uint64_t *)((var_15 + var_10) + 16UL);
        *var_16 = (*var_16 | var_14);
        r14_0 = var_15;
        rsi4_0 = var_13;
        do {
            var_12 = (uint64_t)*(unsigned char *)rsi4_0;
            var_13 = rsi4_0 + 1UL;
            var_14 = 1UL << (var_12 & 63UL);
            var_15 = (var_12 >> 3UL) & 24UL;
            var_16 = (uint64_t *)((var_15 + var_10) + 16UL);
            *var_16 = (*var_16 | var_14);
            r14_0 = var_15;
            rsi4_0 = var_13;
        } while (var_13 != var_11);
        var_17 = var_0 + (-120L);
        *(uint64_t *)var_17 = 4226396UL;
        indirect_placeholder();
        local_sp_0 = var_17;
        var_21 = (uint64_t)((long)(r14_0 << 32UL) >> (long)32UL) >> 6UL;
        var_22 = *(uint64_t *)(((var_21 << 3UL) + local_sp_0) + 16UL);
        var_23 = helper_cc_compute_c_wrapper(var_21, var_22 >> (r14_0 & 63UL), var_8, 41U);
        local_sp_1 = local_sp_0;
        r14_1 = r14_0;
        rax_0 = var_22;
        local_sp_4 = local_sp_0;
        if (var_23 != 0UL) {
            while (1U)
                {
                    var_18 = local_sp_4 + (-8L);
                    *(uint64_t *)var_18 = 4226434UL;
                    indirect_placeholder();
                    var_19 = (uint32_t)rax_0;
                    var_20 = (uint64_t)var_19;
                    local_sp_0 = var_18;
                    r14_0 = var_20;
                    if ((int)var_19 <= (int)4294967295U) {
                        loop_state_var = 0U;
                        break;
                    }
                    var_21 = (uint64_t)((long)(r14_0 << 32UL) >> (long)32UL) >> 6UL;
                    var_22 = *(uint64_t *)(((var_21 << 3UL) + local_sp_0) + 16UL);
                    var_23 = helper_cc_compute_c_wrapper(var_21, var_22 >> (r14_0 & 63UL), var_8, 41U);
                    local_sp_1 = local_sp_0;
                    r14_1 = r14_0;
                    rax_0 = var_22;
                    local_sp_4 = local_sp_0;
                    if (var_23 == 0UL) {
                        continue;
                    }
                    loop_state_var = 1U;
                    break;
                }
            switch (loop_state_var) {
              case 0U:
                {
                    return 18446744073709551615UL;
                }
                break;
              case 1U:
                {
                    break;
                }
                break;
            }
        }
    }
    while (1U)
        {
            var_18 = local_sp_4 + (-8L);
            *(uint64_t *)var_18 = 4226434UL;
            indirect_placeholder();
            var_19 = (uint32_t)rax_0;
            var_20 = (uint64_t)var_19;
            local_sp_0 = var_18;
            r14_0 = var_20;
            if ((int)var_19 <= (int)4294967295U) {
                loop_state_var = 0U;
                break;
            }
            var_21 = (uint64_t)((long)(r14_0 << 32UL) >> (long)32UL) >> 6UL;
            var_22 = *(uint64_t *)(((var_21 << 3UL) + local_sp_0) + 16UL);
            var_23 = helper_cc_compute_c_wrapper(var_21, var_22 >> (r14_0 & 63UL), var_8, 41U);
            local_sp_1 = local_sp_0;
            r14_1 = r14_0;
            rax_0 = var_22;
            local_sp_4 = local_sp_0;
            if (var_23 == 0UL) {
                continue;
            }
            loop_state_var = 1U;
            break;
        }
    switch (loop_state_var) {
      case 0U:
        {
            return 18446744073709551615UL;
        }
        break;
      case 1U:
        {
            var_24 = *(uint64_t *)(local_sp_0 + 8UL);
            rbx_0 = *(uint64_t *)(var_24 + 8UL);
            r12_0 = *(uint64_t *)var_24;
            while (1U)
                {
                    r12_2 = r12_0;
                    local_sp_2 = local_sp_1;
                    rbx_1 = rbx_0;
                    rbx_2 = rbx_0;
                    r12_3 = r12_0;
                    local_sp_3 = local_sp_1;
                    if (r15_0 != r12_0) {
                        if ((int)(uint32_t)r14_1 >= (int)0U) {
                            loop_state_var = 1U;
                            break;
                        }
                        var_27 = (uint64_t)((long)(r14_1 << 32UL) >> (long)32UL) >> 6UL;
                        var_28 = *(uint64_t *)(((var_27 << 3UL) + local_sp_2) + 16UL);
                        var_29 = helper_cc_compute_c_wrapper(var_27, var_28 >> (r14_1 & 63UL), var_8, 41U);
                        rbx_0 = rbx_1;
                        r12_0 = r12_2;
                        rbx_2 = rbx_1;
                        r12_3 = r12_2;
                        local_sp_3 = local_sp_2;
                        if (var_29 != 0UL) {
                            loop_state_var = 1U;
                            break;
                        }
                        *(unsigned char *)(r15_0 + rbx_1) = (unsigned char)r14_1;
                        var_31 = r15_0 + 1UL;
                        var_32 = local_sp_2 + (-8L);
                        *(uint64_t *)var_32 = 4226527UL;
                        indirect_placeholder();
                        var_33 = (uint32_t)var_28;
                        r15_0 = var_31;
                        local_sp_1 = var_32;
                        r14_1 = (uint64_t)var_33;
                        if (((int)var_33 <= (int)4294967295U) && (var_31 == 0UL)) {
                            continue;
                        }
                        loop_state_var = 0U;
                        break;
                    }
                    if (rbx_0 == 0UL) {
                        r12_1 = r15_0;
                        if (r15_0 != 0UL & (long)r15_0 >= (long)0UL) {
                            loop_state_var = 2U;
                            break;
                        }
                    }
                    if (r15_0 <= 6148914691236517203UL) {
                        loop_state_var = 2U;
                        break;
                    }
                    r12_1 = ((r15_0 >> 1UL) + r15_0) + 1UL;
                    var_25 = local_sp_1 + (-8L);
                    *(uint64_t *)var_25 = 4226592UL;
                    var_26 = indirect_placeholder_16(rbx_0, r12_1);
                    r12_2 = r12_1;
                    local_sp_2 = var_25;
                    rbx_1 = var_26;
                    rbx_2 = var_26;
                    r12_3 = r12_1;
                    local_sp_3 = var_25;
                    if ((int)(uint32_t)r14_1 <= (int)4294967295U) {
                        loop_state_var = 1U;
                        break;
                    }
                }
            switch (loop_state_var) {
              case 0U:
                {
                    return 18446744073709551615UL;
                }
                break;
              case 1U:
                {
                    *(unsigned char *)(r15_0 + rbx_2) = (unsigned char)'\x00';
                    var_30 = *(uint64_t *)(local_sp_3 + 8UL);
                    *(uint64_t *)(var_30 + 8UL) = rbx_2;
                    *(uint64_t *)var_30 = r12_3;
                    return r15_0;
                }
                break;
              case 2U:
                {
                    *(uint64_t *)(local_sp_1 + (-8L)) = 4226693UL;
                    indirect_placeholder_14(var_6, var_11);
                    abort();
                }
                break;
            }
        }
        break;
    }
}

typedef unsigned char __u_char;
typedef unsigned short __u_short;
typedef unsigned int __u_int;
typedef unsigned long __u_long;
typedef signed char __int8_t;
typedef unsigned char __uint8_t;
typedef short __int16_t;
typedef unsigned short __uint16_t;
typedef int __int32_t;
typedef unsigned int __uint32_t;
typedef long __int64_t;
typedef unsigned long __uint64_t;
typedef __int8_t __int_least8_t;
typedef __uint8_t __uint_least8_t;
typedef __int16_t __int_least16_t;
typedef __uint16_t __uint_least16_t;
typedef __int32_t __int_least32_t;
typedef __uint32_t __uint_least32_t;
typedef __int64_t __int_least64_t;
typedef __uint64_t __uint_least64_t;
typedef long __quad_t;
typedef unsigned long __u_quad_t;
typedef long __intmax_t;
typedef unsigned long __uintmax_t;
typedef unsigned long __dev_t;
typedef unsigned int __uid_t;
typedef unsigned int __gid_t;
typedef unsigned long __ino_t;
typedef unsigned long __ino64_t;
typedef unsigned int __mode_t;
typedef unsigned long __nlink_t;
typedef long __off_t;
typedef long __off64_t;
typedef int __pid_t;
typedef struct {
    int __val[2];
} __fsid_t;
typedef long __clock_t;
typedef unsigned long __rlim_t;
typedef unsigned long __rlim64_t;
typedef unsigned int __id_t;
typedef long __time_t;
typedef unsigned int __useconds_t;
typedef long __suseconds_t;
typedef int __daddr_t;
typedef int __key_t;
typedef int __clockid_t;
typedef void *__timer_t;
typedef long __blksize_t;
typedef long __blkcnt_t;
typedef long __blkcnt64_t;
typedef unsigned long __fsblkcnt_t;
typedef unsigned long __fsblkcnt64_t;
typedef unsigned long __fsfilcnt_t;
typedef unsigned long __fsfilcnt64_t;
typedef long __fsword_t;
typedef long __ssize_t;
typedef long __syscall_slong_t;
typedef unsigned long __syscall_ulong_t;
typedef __off64_t __loff_t;
typedef char *__caddr_t;
typedef long __intptr_t;
typedef unsigned int __socklen_t;
typedef int __sig_atomic_t;
typedef __int8_t int8_t;
typedef __int16_t int16_t;
typedef __int32_t int32_t;
typedef __int64_t int64_t;
typedef __uint8_t uint8_t;
typedef __uint16_t uint16_t;
typedef __uint32_t uint32_t;
typedef __uint64_t uint64_t;
typedef __int_least8_t int_least8_t;
typedef __int_least16_t int_least16_t;
typedef __int_least32_t int_least32_t;
typedef __int_least64_t int_least64_t;
typedef __uint_least8_t uint_least8_t;
typedef __uint_least16_t uint_least16_t;
typedef __uint_least32_t uint_least32_t;
typedef __uint_least64_t uint_least64_t;
typedef signed char int_fast8_t;
typedef long int_fast16_t;
typedef long int_fast32_t;
typedef long int_fast64_t;
typedef unsigned char uint_fast8_t;
typedef unsigned long uint_fast16_t;
typedef unsigned long uint_fast32_t;
typedef unsigned long uint_fast64_t;
typedef long intptr_t;
typedef unsigned long uintptr_t;
typedef __intmax_t intmax_t;
typedef __uintmax_t uintmax_t;
typedef _Bool bool;
struct bb_check_subexp_matching_top_isra_13_ret_type;
struct indirect_placeholder_13_ret_type;
struct bb_check_subexp_matching_top_isra_13_ret_type {
    uint64_t field_0;
    uint64_t field_1;
    uint64_t field_2;
};
struct indirect_placeholder_13_ret_type {
    uint64_t field_0;
    uint64_t field_1;
    uint64_t field_2;
    uint64_t field_3;
    uint64_t field_4;
    uint64_t field_5;
    uint64_t field_6;
};
extern uint64_t revng_init_local_sp(uint64_t param_0);
extern uint64_t init_rbx(void);
extern uint64_t init_r12(void);
extern uint64_t init_rbp(void);
extern uint64_t init_cc_src2(void);
extern uint64_t helper_cc_compute_c_wrapper(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint32_t param_3);
extern uint64_t init_r13(void);
extern uint64_t init_r14(void);
extern uint64_t helper_cc_compute_all_wrapper(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint32_t param_3);
extern uint64_t init_r15(void);
extern void indirect_placeholder_4(uint64_t param_0, uint64_t param_1);
extern struct indirect_placeholder_13_ret_type indirect_placeholder_13(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint64_t param_3, uint64_t param_4, uint64_t param_5, uint64_t param_6, uint64_t param_7);
struct bb_check_subexp_matching_top_isra_13_ret_type bb_check_subexp_matching_top_isra_13(uint64_t r10, uint64_t rdi, uint64_t r9, uint64_t rcx, uint64_t rdx, uint64_t rsi) {
    uint64_t rbx_0_ph;
    uint64_t r103_1_ph;
    uint64_t var_0;
    uint64_t var_1;
    uint64_t var_2;
    uint64_t var_3;
    uint64_t var_4;
    uint64_t var_5;
    uint64_t var_6;
    uint64_t var_7;
    uint64_t *var_8;
    uint64_t var_9;
    uint64_t var_10;
    uint64_t var_11;
    uint64_t r103_3;
    uint64_t var_34;
    uint64_t var_35;
    uint64_t var_36;
    uint64_t *_pre_phi;
    uint64_t rax_0;
    uint64_t var_45;
    uint64_t var_46;
    uint64_t var_47;
    uint64_t var_48;
    uint64_t var_49;
    uint64_t var_50;
    uint64_t var_26;
    uint64_t var_12;
    uint64_t *var_13;
    uint64_t *var_14;
    uint64_t *var_15;
    uint64_t *var_16;
    uint64_t *var_17;
    uint64_t *var_18;
    uint64_t r95_1_ph;
    uint64_t rcx6_0_ph;
    uint64_t local_sp_0_ph;
    uint64_t r8_0_ph;
    uint64_t r95_3;
    uint64_t rbx_0;
    uint64_t var_19;
    uint64_t var_20;
    uint64_t var_21;
    uint64_t var_22;
    uint64_t var_23;
    uint64_t var_24;
    uint64_t var_25;
    bool var_27;
    uint64_t var_28;
    uint64_t rcx6_1;
    uint64_t local_sp_1;
    uint64_t rsi8_0;
    uint64_t var_37;
    uint64_t var_38;
    struct indirect_placeholder_13_ret_type var_39;
    uint64_t var_40;
    uint64_t var_41;
    uint64_t var_42;
    uint64_t var_43;
    uint64_t var_44;
    uint64_t storemerge;
    uint64_t var_29;
    uint64_t var_30;
    uint64_t *var_31;
    uint64_t var_32;
    uint64_t *var_33;
    uint64_t var_51;
    struct bb_check_subexp_matching_top_isra_13_ret_type mrv;
    struct bb_check_subexp_matching_top_isra_13_ret_type mrv1;
    struct bb_check_subexp_matching_top_isra_13_ret_type mrv2;
    unsigned int loop_state_var;
    bool switch_state_var;
    var_0 = revng_init_local_sp(0UL);
    var_1 = init_r15();
    var_2 = init_r13();
    var_3 = init_rbx();
    var_4 = init_r12();
    var_5 = init_rbp();
    var_6 = init_cc_src2();
    var_7 = init_r14();
    *(uint64_t *)(var_0 + (-8L)) = var_1;
    *(uint64_t *)(var_0 + (-16L)) = var_7;
    *(uint64_t *)(var_0 + (-24L)) = var_2;
    *(uint64_t *)(var_0 + (-32L)) = var_4;
    *(uint64_t *)(var_0 + (-40L)) = var_5;
    *(uint64_t *)(var_0 + (-48L)) = var_3;
    var_8 = (uint64_t *)rsi;
    var_9 = *var_8;
    var_10 = *(uint64_t *)(rdi + 152UL);
    var_11 = helper_cc_compute_all_wrapper(var_9, 0UL, 0UL, 25U);
    rbx_0_ph = 0UL;
    r103_1_ph = r10;
    r103_3 = r10;
    r95_1_ph = r9;
    rcx6_0_ph = rcx;
    r8_0_ph = var_9;
    r95_3 = r9;
    storemerge = 0UL;
    if ((uint64_t)(((unsigned char)(var_11 >> 4UL) ^ (unsigned char)var_11) & '\xc0') == 0UL) {
        mrv.field_0 = storemerge;
        mrv1 = mrv;
        mrv1.field_1 = r103_3;
        mrv2 = mrv1;
        mrv2.field_2 = r95_3;
        return mrv2;
    }
    var_12 = var_0 + (-72L);
    var_13 = (uint64_t *)rdx;
    var_14 = (uint64_t *)var_10;
    var_15 = (uint64_t *)(var_10 + 160UL);
    var_16 = (uint64_t *)(rdi + 232UL);
    var_17 = (uint64_t *)(rdi + 240UL);
    var_18 = (uint64_t *)(rdi + 248UL);
    local_sp_0_ph = var_12;
    storemerge = 12UL;
    while (1U)
        {
            r103_3 = r103_1_ph;
            r95_3 = r95_1_ph;
            rbx_0 = rbx_0_ph;
            rcx6_1 = rcx6_0_ph;
            local_sp_1 = local_sp_0_ph;
            while (1U)
                {
                    var_19 = *(uint64_t *)((rbx_0 << 3UL) + *var_13);
                    var_20 = (var_19 << 4UL) + *var_14;
                    if (*(unsigned char *)(var_20 + 8UL) != '\b') {
                        var_21 = *(uint64_t *)var_20;
                        var_22 = var_21 + (-63L);
                        rax_0 = var_21;
                        var_23 = *var_15 >> (var_21 & 63UL);
                        var_24 = helper_cc_compute_all_wrapper(var_22, 63UL, var_6, 17U);
                        var_25 = helper_cc_compute_c_wrapper(var_22, (var_24 & (-2L)) | (var_23 & 1UL), var_6, 1U);
                        if ((long)var_21 <= (long)63UL & var_25 != 0UL) {
                            loop_state_var = 1U;
                            break;
                        }
                    }
                    var_51 = rbx_0 + 1UL;
                    rbx_0 = var_51;
                    storemerge = 0UL;
                    if ((long)var_51 < (long)r8_0_ph) {
                        continue;
                    }
                    loop_state_var = 0U;
                    break;
                }
            switch_state_var = 0;
            switch (loop_state_var) {
              case 0U:
                {
                    switch_state_var = 1;
                    break;
                }
                break;
              case 1U:
                {
                    var_26 = *var_16;
                    var_27 = (var_26 == *var_17);
                    var_28 = *var_18;
                    rsi8_0 = var_26;
                    if (var_27) {
                        _pre_phi = (uint64_t *)local_sp_0_ph;
                        rax_0 = var_28;
                    } else {
                        var_29 = var_26 << 1UL;
                        var_30 = var_26 << 4UL;
                        *(uint64_t *)(local_sp_0_ph + 8UL) = rcx6_0_ph;
                        var_31 = (uint64_t *)local_sp_0_ph;
                        *var_31 = var_29;
                        var_32 = local_sp_0_ph + (-8L);
                        var_33 = (uint64_t *)var_32;
                        *var_33 = 4236741UL;
                        indirect_placeholder_4(var_28, var_30);
                        _pre_phi = var_33;
                        local_sp_1 = var_32;
                        if (var_21 != 0UL) {
                            switch_state_var = 1;
                            break;
                        }
                        var_34 = *var_33;
                        *var_18 = var_21;
                        var_35 = *var_16;
                        var_36 = *var_31;
                        *var_17 = var_34;
                        rcx6_1 = var_36;
                        rsi8_0 = var_35;
                    }
                    var_37 = (rsi8_0 << 3UL) + rax_0;
                    *(uint64_t *)(local_sp_1 + 8UL) = rcx6_1;
                    *_pre_phi = var_37;
                    var_38 = local_sp_1 + (-8L);
                    *(uint64_t *)var_38 = 4236612UL;
                    var_39 = indirect_placeholder_13(rbx_0, r103_1_ph, 1UL, r95_1_ph, var_19, rcx6_1, 48UL, r8_0_ph);
                    var_40 = var_39.field_0;
                    var_41 = var_39.field_2;
                    var_42 = var_39.field_3;
                    **(uint64_t **)var_38 = var_40;
                    var_43 = *var_16;
                    var_44 = *(uint64_t *)((var_43 << 3UL) + *var_18);
                    r103_1_ph = var_41;
                    r103_3 = var_41;
                    r95_1_ph = var_42;
                    local_sp_0_ph = var_38;
                    r95_3 = var_42;
                    if (var_44 == 0UL) {
                        switch_state_var = 1;
                        break;
                    }
                    var_45 = var_39.field_4;
                    var_46 = var_39.field_1;
                    var_47 = *_pre_phi;
                    var_48 = var_43 + 1UL;
                    *(uint64_t *)(var_44 + 8UL) = var_45;
                    *var_16 = var_48;
                    var_49 = var_46 + 1UL;
                    *(uint64_t *)var_44 = var_47;
                    var_50 = *var_8;
                    rbx_0_ph = var_49;
                    rcx6_0_ph = var_47;
                    r8_0_ph = var_50;
                    storemerge = 0UL;
                    if ((long)var_49 >= (long)var_50) {
                        continue;
                    }
                    switch_state_var = 1;
                    break;
                }
                break;
            }
            if (switch_state_var)
                break;
        }
    mrv.field_0 = storemerge;
    mrv1 = mrv;
    mrv1.field_1 = r103_3;
    mrv2 = mrv1;
    mrv2.field_2 = r95_3;
    return mrv2;
}

typedef unsigned char __u_char;
typedef unsigned short __u_short;
typedef unsigned int __u_int;
typedef unsigned long __u_long;
typedef signed char __int8_t;
typedef unsigned char __uint8_t;
typedef short __int16_t;
typedef unsigned short __uint16_t;
typedef int __int32_t;
typedef unsigned int __uint32_t;
typedef long __int64_t;
typedef unsigned long __uint64_t;
typedef __int8_t __int_least8_t;
typedef __uint8_t __uint_least8_t;
typedef __int16_t __int_least16_t;
typedef __uint16_t __uint_least16_t;
typedef __int32_t __int_least32_t;
typedef __uint32_t __uint_least32_t;
typedef __int64_t __int_least64_t;
typedef __uint64_t __uint_least64_t;
typedef long __quad_t;
typedef unsigned long __u_quad_t;
typedef long __intmax_t;
typedef unsigned long __uintmax_t;
typedef unsigned long __dev_t;
typedef unsigned int __uid_t;
typedef unsigned int __gid_t;
typedef unsigned long __ino_t;
typedef unsigned long __ino64_t;
typedef unsigned int __mode_t;
typedef unsigned long __nlink_t;
typedef long __off_t;
typedef long __off64_t;
typedef int __pid_t;
typedef struct {
    int __val[2];
} __fsid_t;
typedef long __clock_t;
typedef unsigned long __rlim_t;
typedef unsigned long __rlim64_t;
typedef unsigned int __id_t;
typedef long __time_t;
typedef unsigned int __useconds_t;
typedef long __suseconds_t;
typedef int __daddr_t;
typedef int __key_t;
typedef int __clockid_t;
typedef void *__timer_t;
typedef long __blksize_t;
typedef long __blkcnt_t;
typedef long __blkcnt64_t;
typedef unsigned long __fsblkcnt_t;
typedef unsigned long __fsblkcnt64_t;
typedef unsigned long __fsfilcnt_t;
typedef unsigned long __fsfilcnt64_t;
typedef long __fsword_t;
typedef long __ssize_t;
typedef long __syscall_slong_t;
typedef unsigned long __syscall_ulong_t;
typedef __off64_t __loff_t;
typedef char *__caddr_t;
typedef long __intptr_t;
typedef unsigned int __socklen_t;
typedef int __sig_atomic_t;
typedef __int8_t int8_t;
typedef __int16_t int16_t;
typedef __int32_t int32_t;
typedef __int64_t int64_t;
typedef __uint8_t uint8_t;
typedef __uint16_t uint16_t;
typedef __uint32_t uint32_t;
typedef __uint64_t uint64_t;
typedef __int_least8_t int_least8_t;
typedef __int_least16_t int_least16_t;
typedef __int_least32_t int_least32_t;
typedef __int_least64_t int_least64_t;
typedef __uint_least8_t uint_least8_t;
typedef __uint_least16_t uint_least16_t;
typedef __uint_least32_t uint_least32_t;
typedef __uint_least64_t uint_least64_t;
typedef signed char int_fast8_t;
typedef long int_fast16_t;
typedef long int_fast32_t;
typedef long int_fast64_t;
typedef unsigned char uint_fast8_t;
typedef unsigned long uint_fast16_t;
typedef unsigned long uint_fast32_t;
typedef unsigned long uint_fast64_t;
typedef long intptr_t;
typedef unsigned long uintptr_t;
typedef __intmax_t intmax_t;
typedef __uintmax_t uintmax_t;
typedef _Bool bool;
struct indirect_placeholder_297_ret_type;
struct indirect_placeholder_298_ret_type;
struct indirect_placeholder_297_ret_type {
    uint64_t field_0;
    uint64_t field_1;
    uint64_t field_2;
    uint64_t field_3;
};
struct indirect_placeholder_298_ret_type {
    uint64_t field_0;
    uint64_t field_1;
};
extern uint64_t revng_init_local_sp(uint64_t param_0);
extern uint64_t init_rbx(void);
extern uint64_t init_r12(void);
extern uint64_t init_rbp(void);
extern uint64_t init_cc_src2(void);
extern uint64_t helper_cc_compute_c_wrapper(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint32_t param_3);
extern uint64_t init_r13(void);
extern uint64_t init_r14(void);
extern uint64_t helper_cc_compute_all_wrapper(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint32_t param_3);
extern uint64_t indirect_placeholder_8(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint64_t param_3, uint64_t param_4);
extern void indirect_placeholder_4(uint64_t param_0, uint64_t param_1);
extern struct indirect_placeholder_297_ret_type indirect_placeholder_297(uint64_t param_0, uint64_t param_1);
extern struct indirect_placeholder_298_ret_type indirect_placeholder_298(uint64_t param_0);
uint64_t bb_check_arrival_expand_ecl_sub(uint64_t rdi, uint64_t rcx, uint64_t rdx, uint64_t rsi, uint64_t r8) {
    uint64_t var_64;
    uint64_t var_0;
    uint64_t var_1;
    uint64_t var_2;
    uint64_t var_3;
    uint64_t var_4;
    uint64_t var_5;
    uint64_t var_6;
    uint32_t var_7;
    uint64_t var_8;
    uint64_t var_9;
    uint64_t *var_10;
    uint64_t var_11;
    uint64_t *var_12;
    uint64_t *var_13;
    uint64_t *var_14;
    uint64_t *var_15;
    uint64_t rax_2;
    uint64_t rax_4;
    uint64_t var_65;
    uint64_t rax_3;
    uint64_t local_sp_2;
    uint64_t local_sp_0;
    uint64_t r14_0;
    uint64_t var_16;
    uint64_t var_17;
    uint64_t var_18;
    uint64_t var_19;
    uint64_t rax_1;
    uint64_t r10_0;
    uint64_t storemerge;
    uint64_t rcx2_5;
    uint64_t var_20;
    uint64_t rax_0;
    uint64_t var_21;
    uint64_t r10_2;
    uint64_t var_22;
    struct indirect_placeholder_297_ret_type var_66;
    uint64_t var_23;
    uint64_t rcx2_0;
    uint64_t local_sp_1;
    uint64_t var_33;
    bool var_34;
    uint64_t var_35;
    uint64_t var_36;
    uint64_t var_37;
    uint64_t var_38;
    uint64_t rdx3_0;
    uint64_t rcx2_2;
    uint64_t var_39;
    uint64_t *var_40;
    uint64_t rcx2_1;
    uint64_t *var_41;
    uint64_t *var_42;
    uint64_t var_43;
    uint64_t rdx3_1;
    uint64_t var_44;
    uint64_t var_45;
    uint64_t var_46;
    uint64_t rcx2_3;
    uint64_t var_47;
    uint64_t var_48;
    uint64_t rdx3_2;
    uint64_t rsi4_0;
    uint64_t var_49;
    uint64_t var_50;
    uint64_t var_51;
    uint64_t var_52;
    uint64_t var_53;
    uint64_t var_54;
    uint64_t rcx2_4;
    uint64_t var_55;
    uint64_t var_56;
    uint64_t var_28;
    uint64_t var_29;
    uint64_t rdx3_3;
    uint64_t var_30;
    uint64_t var_31;
    uint64_t var_32;
    uint64_t var_60;
    uint64_t var_61;
    uint64_t local_sp_3;
    uint64_t var_62;
    uint64_t var_63;
    uint64_t var_24;
    uint64_t var_25;
    uint64_t var_26;
    uint64_t var_27;
    uint64_t var_57;
    struct indirect_placeholder_298_ret_type var_58;
    uint64_t var_59;
    unsigned int loop_state_var;
    bool switch_state_var;
    var_0 = revng_init_local_sp(0UL);
    var_1 = init_r13();
    var_2 = init_rbx();
    var_3 = init_r12();
    var_4 = init_rbp();
    var_5 = init_cc_src2();
    var_6 = init_r14();
    *(uint64_t *)(var_0 + (-8L)) = var_6;
    *(uint64_t *)(var_0 + (-16L)) = var_1;
    var_7 = (uint32_t)r8;
    var_8 = (uint64_t)var_7;
    *(uint64_t *)(var_0 + (-24L)) = var_3;
    *(uint64_t *)(var_0 + (-32L)) = var_4;
    var_9 = var_0 + (-40L);
    *(uint64_t *)var_9 = var_2;
    var_10 = (uint64_t *)(rsi + 8UL);
    var_11 = rsi + 16UL;
    var_12 = (uint64_t *)var_11;
    var_13 = (uint64_t *)rdi;
    var_14 = (uint64_t *)rsi;
    var_15 = (uint64_t *)(rdi + 40UL);
    rax_4 = 0UL;
    local_sp_0 = var_9;
    r14_0 = rdx;
    storemerge = 0UL;
    rdx3_1 = 0UL;
    rdx3_2 = 0UL;
    rsi4_0 = 0UL;
    while (1U)
        {
            var_16 = *var_10;
            var_17 = helper_cc_compute_all_wrapper(var_16, 0UL, 0UL, 25U);
            rcx2_0 = var_16;
            local_sp_1 = local_sp_0;
            local_sp_2 = local_sp_0;
            if ((uint64_t)(((unsigned char)(var_17 >> 4UL) ^ (unsigned char)var_17) & '\xc0') != 0UL) {
                var_18 = var_16 + (-1L);
                var_19 = *var_12;
                r10_0 = var_18;
                r10_2 = r10_0;
                while (r10_0 <= storemerge)
                    {
                        var_20 = (r10_0 + storemerge) >> 1UL;
                        rax_0 = var_20;
                        rax_1 = var_20;
                        if ((long)r14_0 > (long)*(uint64_t *)((var_20 << 3UL) + var_19)) {
                            r10_0 = r10_2;
                            storemerge = rax_1 + 1UL;
                            r10_2 = r10_0;
                            continue;
                        }
                        if (var_20 > storemerge) {
                            break;
                        }
                        while (1U)
                            {
                                var_21 = (rax_0 + storemerge) >> 1UL;
                                rax_0 = var_21;
                                rax_1 = var_21;
                                r10_2 = rax_0;
                                if ((long)r14_0 <= (long)*(uint64_t *)((var_21 << 3UL) + var_19)) {
                                    loop_state_var = 1U;
                                    break;
                                }
                                if (var_21 > storemerge) {
                                    continue;
                                }
                                loop_state_var = 0U;
                                break;
                            }
                        switch_state_var = 0;
                        switch (loop_state_var) {
                          case 0U:
                            {
                                switch_state_var = 1;
                                break;
                            }
                            break;
                          case 1U:
                            {
                                break;
                            }
                            break;
                        }
                        if (switch_state_var)
                            break;
                    }
                if (r14_0 != *(uint64_t *)((storemerge << 3UL) + var_19)) {
                    loop_state_var = 1U;
                    break;
                }
            }
            var_22 = (r14_0 << 4UL) + *var_13;
            rax_4 = 12UL;
            if ((uint64_t)((uint32_t)(uint64_t)*(unsigned char *)(var_22 + 8UL) - var_7) != 0UL) {
                if (*(uint64_t *)var_22 != rcx) {
                    if ((uint64_t)(var_7 + (-9)) != 0UL) {
                        loop_state_var = 1U;
                        break;
                    }
                    *(uint64_t *)(local_sp_0 + (-8L)) = 4257320UL;
                    var_66 = indirect_placeholder_297(rsi, r14_0);
                    if ((uint64_t)(unsigned char)var_66.field_0 != 0UL) {
                        loop_state_var = 1U;
                        break;
                    }
                    loop_state_var = 0U;
                    break;
                }
            }
            var_23 = *var_14;
            if (var_23 == 0UL) {
                *var_14 = 1UL;
                *var_10 = 1UL;
                var_57 = local_sp_0 + (-8L);
                *(uint64_t *)var_57 = 4256818UL;
                var_58 = indirect_placeholder_298(8UL);
                var_59 = var_58.field_0;
                *var_12 = var_59;
                local_sp_2 = var_57;
                if (var_59 != 0UL) {
                    *var_10 = 0UL;
                    *var_14 = 0UL;
                    return 12UL;
                }
                *(uint64_t *)var_59 = r14_0;
            } else {
                if (var_16 == 0UL) {
                    **(uint64_t **)var_11 = r14_0;
                    *var_10 = (*var_10 + 1UL);
                } else {
                    if (var_16 == var_23) {
                        rax_2 = *var_12;
                    } else {
                        var_24 = var_16 << 1UL;
                        var_25 = *var_12;
                        var_26 = var_16 << 4UL;
                        *var_14 = var_24;
                        var_27 = local_sp_0 + (-8L);
                        *(uint64_t *)var_27 = 4257291UL;
                        indirect_placeholder_4(var_25, var_26);
                        rax_2 = var_24;
                        local_sp_1 = var_27;
                        if (var_24 != 0UL) {
                            loop_state_var = 0U;
                            break;
                        }
                        *var_12 = var_24;
                        rcx2_0 = *var_10;
                    }
                    rdx3_0 = rcx2_0;
                    rcx2_2 = rcx2_0;
                    local_sp_2 = local_sp_1;
                    if ((long)r14_0 < (long)*(uint64_t *)rax_2) {
                        var_33 = helper_cc_compute_all_wrapper(rcx2_0, 0UL, 0UL, 25U);
                        var_34 = ((uint64_t)(((unsigned char)(var_33 >> 4UL) ^ (unsigned char)var_33) & '\xc0') == 0UL);
                        var_35 = rcx2_0 << 3UL;
                        rcx2_5 = var_35;
                        if (var_34) {
                            *(uint64_t *)(rcx2_5 + rax_2) = r14_0;
                            *var_10 = (*var_10 + 1UL);
                        } else {
                            var_36 = var_35 + (-16L);
                            var_37 = ((var_36 + rax_2) >> 3UL) & 1UL;
                            var_38 = (var_37 > rcx2_0) ? rcx2_0 : var_37;
                            rcx2_5 = 0UL;
                            if ((long)rcx2_0 > (long)4UL) {
                                rdx3_0 = var_38;
                                if (var_38 != 0UL) {
                                    var_39 = var_35 + rax_2;
                                    var_40 = (uint64_t *)(var_39 + (-8L));
                                    *(uint64_t *)var_39 = *var_40;
                                    rcx2_1 = rcx2_0 + (-1L);
                                    rdx3_1 = rdx3_0;
                                    var_41 = (uint64_t *)(var_39 + (-16L));
                                    *var_40 = *var_41;
                                    rcx2_1 = rcx2_0 + (-2L);
                                    var_42 = (uint64_t *)(var_39 + (-24L));
                                    *var_41 = *var_42;
                                    rcx2_1 = rcx2_0 + (-3L);
                                    if (rdx3_0 != 1UL & rdx3_0 != 2UL & rdx3_0 == 4UL) {
                                        var_43 = rcx2_0 + (-4L);
                                        *var_42 = *(uint64_t *)(var_39 + (-32L));
                                        rcx2_1 = var_43;
                                    }
                                    rcx2_2 = rcx2_1;
                                    if (rdx3_0 != rcx2_0) {
                                        *(uint64_t *)(rcx2_5 + rax_2) = r14_0;
                                        *var_10 = (*var_10 + 1UL);
                                        var_60 = r14_0 * 24UL;
                                        var_61 = var_60 + *var_15;
                                        rax_3 = var_61;
                                        local_sp_3 = local_sp_2;
                                        switch_state_var = 0;
                                        switch (*(uint64_t *)(var_61 + 8UL)) {
                                          case 0UL:
                                            {
                                                loop_state_var = 1U;
                                                switch_state_var = 1;
                                                break;
                                            }
                                            break;
                                          case 2UL:
                                            {
                                                var_62 = *(uint64_t *)(*(uint64_t *)(var_61 + 16UL) + 8UL);
                                                var_63 = local_sp_2 + (-8L);
                                                *(uint64_t *)var_63 = 4256865UL;
                                                var_64 = indirect_placeholder_8(rdi, rcx, var_62, rsi, var_8);
                                                local_sp_3 = var_63;
                                                rax_4 = var_64;
                                                if ((uint64_t)(uint32_t)var_64 != 0UL) {
                                                    loop_state_var = 1U;
                                                    switch_state_var = 1;
                                                    break;
                                                }
                                                var_65 = var_60 + *var_15;
                                                rax_3 = var_65;
                                                local_sp_0 = local_sp_3;
                                                r14_0 = **(uint64_t **)(rax_3 + 16UL);
                                                continue;
                                            }
                                            break;
                                          default:
                                            {
                                                local_sp_0 = local_sp_3;
                                                r14_0 = **(uint64_t **)(rax_3 + 16UL);
                                                continue;
                                            }
                                            break;
                                        }
                                        if (switch_state_var)
                                            break;
                                    }
                                }
                            }
                            var_39 = var_35 + rax_2;
                            var_40 = (uint64_t *)(var_39 + (-8L));
                            *(uint64_t *)var_39 = *var_40;
                            rcx2_1 = rcx2_0 + (-1L);
                            rdx3_1 = rdx3_0;
                            var_41 = (uint64_t *)(var_39 + (-16L));
                            *var_40 = *var_41;
                            rcx2_1 = rcx2_0 + (-2L);
                            var_42 = (uint64_t *)(var_39 + (-24L));
                            *var_41 = *var_42;
                            rcx2_1 = rcx2_0 + (-3L);
                            if (rdx3_0 != 1UL & rdx3_0 != 2UL & rdx3_0 == 4UL) {
                                var_43 = rcx2_0 + (-4L);
                                *var_42 = *(uint64_t *)(var_39 + (-32L));
                                rcx2_1 = var_43;
                            }
                            rcx2_2 = rcx2_1;
                            if (rdx3_0 != rcx2_0) {
                                *(uint64_t *)(rcx2_5 + rax_2) = r14_0;
                                *var_10 = (*var_10 + 1UL);
                                var_60 = r14_0 * 24UL;
                                var_61 = var_60 + *var_15;
                                rax_3 = var_61;
                                local_sp_3 = local_sp_2;
                                switch_state_var = 0;
                                switch (*(uint64_t *)(var_61 + 8UL)) {
                                  case 0UL:
                                    {
                                        loop_state_var = 1U;
                                        switch_state_var = 1;
                                        break;
                                    }
                                    break;
                                  case 2UL:
                                    {
                                        var_62 = *(uint64_t *)(*(uint64_t *)(var_61 + 16UL) + 8UL);
                                        var_63 = local_sp_2 + (-8L);
                                        *(uint64_t *)var_63 = 4256865UL;
                                        var_64 = indirect_placeholder_8(rdi, rcx, var_62, rsi, var_8);
                                        local_sp_3 = var_63;
                                        rax_4 = var_64;
                                        if ((uint64_t)(uint32_t)var_64 == 0UL) {
                                            loop_state_var = 1U;
                                            switch_state_var = 1;
                                            break;
                                        }
                                        var_65 = var_60 + *var_15;
                                        rax_3 = var_65;
                                        local_sp_0 = local_sp_3;
                                        r14_0 = **(uint64_t **)(rax_3 + 16UL);
                                        continue;
                                    }
                                    break;
                                  default:
                                    {
                                        local_sp_0 = local_sp_3;
                                        r14_0 = **(uint64_t **)(rax_3 + 16UL);
                                        continue;
                                    }
                                    break;
                                }
                                if (switch_state_var)
                                    break;
                            }
                            var_44 = rcx2_0 - rdx3_1;
                            var_45 = ((var_44 + (-2L)) >> 1UL) + 1UL;
                            var_46 = var_45 << 1UL;
                            rcx2_3 = rcx2_2;
                            if (var_44 == 1UL) {
                                var_56 = (rcx2_3 << 3UL) + rax_2;
                                *(uint64_t *)var_56 = *(uint64_t *)(var_56 + (-8L));
                            } else {
                                var_47 = rdx3_1 << 3UL;
                                var_48 = ((var_47 ^ (-8L)) + var_35) + rax_2;
                                var_49 = rdx3_2 + ((var_36 - var_47) + rax_2);
                                var_50 = *(uint64_t *)var_49;
                                var_51 = *(uint64_t *)(var_49 + 8UL);
                                var_52 = rsi4_0 + 1UL;
                                var_53 = rdx3_2 + var_48;
                                *(uint64_t *)var_53 = var_50;
                                *(uint64_t *)(var_53 + 8UL) = var_51;
                                var_54 = helper_cc_compute_c_wrapper(var_52 - var_45, var_45, var_5, 17U);
                                rsi4_0 = var_52;
                                while (var_54 != 0UL)
                                    {
                                        rdx3_2 = rdx3_2 + (-16L);
                                        var_49 = rdx3_2 + ((var_36 - var_47) + rax_2);
                                        var_50 = *(uint64_t *)var_49;
                                        var_51 = *(uint64_t *)(var_49 + 8UL);
                                        var_52 = rsi4_0 + 1UL;
                                        var_53 = rdx3_2 + var_48;
                                        *(uint64_t *)var_53 = var_50;
                                        *(uint64_t *)(var_53 + 8UL) = var_51;
                                        var_54 = helper_cc_compute_c_wrapper(var_52 - var_45, var_45, var_5, 17U);
                                        rsi4_0 = var_52;
                                    }
                                var_55 = rcx2_2 - var_46;
                                rcx2_3 = var_55;
                                if (var_46 == var_44) {
                                    var_56 = (rcx2_3 << 3UL) + rax_2;
                                    *(uint64_t *)var_56 = *(uint64_t *)(var_56 + (-8L));
                                }
                            }
                        }
                    } else {
                        var_28 = rcx2_0 << 3UL;
                        var_29 = *(uint64_t *)((var_28 + rax_2) + (-8L));
                        rcx2_4 = var_28;
                        rdx3_3 = var_29;
                        rcx2_5 = var_28;
                        if ((long)r14_0 < (long)var_29) {
                            *(uint64_t *)(rcx2_5 + rax_2) = r14_0;
                            *var_10 = (*var_10 + 1UL);
                        } else {
                            *(uint64_t *)(rcx2_4 + rax_2) = rdx3_3;
                            var_30 = rcx2_4 + (-8L);
                            var_31 = var_30 + rax_2;
                            var_32 = *(uint64_t *)(var_31 + (-8L));
                            rcx2_4 = var_30;
                            rdx3_3 = var_32;
                            do {
                                *(uint64_t *)(rcx2_4 + rax_2) = rdx3_3;
                                var_30 = rcx2_4 + (-8L);
                                var_31 = var_30 + rax_2;
                                var_32 = *(uint64_t *)(var_31 + (-8L));
                                rcx2_4 = var_30;
                                rdx3_3 = var_32;
                            } while ((long)r14_0 >= (long)var_32);
                            *(uint64_t *)var_31 = r14_0;
                            *var_10 = (*var_10 + 1UL);
                        }
                    }
                }
            }
        }
    switch (loop_state_var) {
      case 0U:
        {
        }
        break;
      case 1U:
        {
            return rax_4;
        }
        break;
    }
}

typedef unsigned char __u_char;
typedef unsigned short __u_short;
typedef unsigned int __u_int;
typedef unsigned long __u_long;
typedef signed char __int8_t;
typedef unsigned char __uint8_t;
typedef short __int16_t;
typedef unsigned short __uint16_t;
typedef int __int32_t;
typedef unsigned int __uint32_t;
typedef long __int64_t;
typedef unsigned long __uint64_t;
typedef __int8_t __int_least8_t;
typedef __uint8_t __uint_least8_t;
typedef __int16_t __int_least16_t;
typedef __uint16_t __uint_least16_t;
typedef __int32_t __int_least32_t;
typedef __uint32_t __uint_least32_t;
typedef __int64_t __int_least64_t;
typedef __uint64_t __uint_least64_t;
typedef long __quad_t;
typedef unsigned long __u_quad_t;
typedef long __intmax_t;
typedef unsigned long __uintmax_t;
typedef unsigned long __dev_t;
typedef unsigned int __uid_t;
typedef unsigned int __gid_t;
typedef unsigned long __ino_t;
typedef unsigned long __ino64_t;
typedef unsigned int __mode_t;
typedef unsigned long __nlink_t;
typedef long __off_t;
typedef long __off64_t;
typedef int __pid_t;
typedef struct {
    int __val[2];
} __fsid_t;
typedef long __clock_t;
typedef unsigned long __rlim_t;
typedef unsigned long __rlim64_t;
typedef unsigned int __id_t;
typedef long __time_t;
typedef unsigned int __useconds_t;
typedef long __suseconds_t;
typedef int __daddr_t;
typedef int __key_t;
typedef int __clockid_t;
typedef void *__timer_t;
typedef long __blksize_t;
typedef long __blkcnt_t;
typedef long __blkcnt64_t;
typedef unsigned long __fsblkcnt_t;
typedef unsigned long __fsblkcnt64_t;
typedef unsigned long __fsfilcnt_t;
typedef unsigned long __fsfilcnt64_t;
typedef long __fsword_t;
typedef long __ssize_t;
typedef long __syscall_slong_t;
typedef unsigned long __syscall_ulong_t;
typedef __off64_t __loff_t;
typedef char *__caddr_t;
typedef long __intptr_t;
typedef unsigned int __socklen_t;
typedef int __sig_atomic_t;
typedef __int8_t int8_t;
typedef __int16_t int16_t;
typedef __int32_t int32_t;
typedef __int64_t int64_t;
typedef __uint8_t uint8_t;
typedef __uint16_t uint16_t;
typedef __uint32_t uint32_t;
typedef __uint64_t uint64_t;
typedef __int_least8_t int_least8_t;
typedef __int_least16_t int_least16_t;
typedef __int_least32_t int_least32_t;
typedef __int_least64_t int_least64_t;
typedef __uint_least8_t uint_least8_t;
typedef __uint_least16_t uint_least16_t;
typedef __uint_least32_t uint_least32_t;
typedef __uint_least64_t uint_least64_t;
typedef signed char int_fast8_t;
typedef long int_fast16_t;
typedef long int_fast32_t;
typedef long int_fast64_t;
typedef unsigned char uint_fast8_t;
typedef unsigned long uint_fast16_t;
typedef unsigned long uint_fast32_t;
typedef unsigned long uint_fast64_t;
typedef long intptr_t;
typedef unsigned long uintptr_t;
typedef __intmax_t intmax_t;
typedef __uintmax_t uintmax_t;
struct bb_check_arrival_expand_ecl_ret_type;
struct indirect_placeholder_299_ret_type;
struct indirect_placeholder_300_ret_type;
struct bb_check_arrival_expand_ecl_ret_type {
    uint64_t field_0;
    uint64_t field_1;
    uint64_t field_2;
};
struct indirect_placeholder_299_ret_type {
    uint64_t field_0;
    uint64_t field_1;
};
struct indirect_placeholder_300_ret_type {
    uint64_t field_0;
    uint64_t field_1;
    uint64_t field_2;
    uint64_t field_3;
};
extern struct indirect_placeholder_299_ret_type indirect_placeholder_299(uint64_t param_0);
extern uint64_t revng_init_local_sp(uint64_t param_0);
extern uint64_t init_rbx(void);
extern uint64_t init_r12(void);
extern uint64_t init_rbp(void);
extern void indirect_placeholder(void);
extern uint64_t init_r13(void);
extern uint64_t init_r14(void);
extern uint64_t helper_cc_compute_all_wrapper(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint32_t param_3);
extern uint64_t indirect_placeholder_8(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint64_t param_3, uint64_t param_4);
extern uint64_t init_r10(void);
extern uint64_t init_r9(void);
extern struct indirect_placeholder_300_ret_type indirect_placeholder_300(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint64_t param_3, uint64_t param_4);
typedef _Bool bool;
struct bb_check_arrival_expand_ecl_ret_type bb_check_arrival_expand_ecl(uint64_t rdi, uint64_t rcx, uint64_t rdx, uint64_t rsi) {
    struct indirect_placeholder_299_ret_type var_14;
    uint64_t var_15;
    uint64_t r10_3;
    uint64_t local_sp_0;
    uint64_t r10_2;
    uint64_t rbx_0;
    struct bb_check_arrival_expand_ecl_ret_type mrv1;
    struct bb_check_arrival_expand_ecl_ret_type mrv2;
    uint64_t r9_4;
    uint64_t r10_0;
    uint64_t r9_2;
    uint64_t r12_0;
    uint64_t r9_0;
    uint64_t rbp_0;
    uint64_t var_42;
    uint64_t local_sp_2;
    uint64_t *_pre_phi66;
    uint32_t _pre_phi;
    uint64_t local_sp_1;
    uint64_t r10_1;
    uint64_t r9_1;
    struct bb_check_arrival_expand_ecl_ret_type mrv;
    struct indirect_placeholder_300_ret_type var_36;
    uint64_t var_32;
    uint64_t var_0;
    uint64_t var_1;
    uint64_t var_2;
    uint64_t var_3;
    uint64_t var_4;
    uint64_t var_5;
    uint64_t var_6;
    uint64_t var_7;
    uint64_t var_8;
    uint64_t *var_9;
    uint64_t var_10;
    uint64_t *var_11;
    uint64_t var_12;
    uint64_t var_13;
    uint64_t r10_4;
    uint64_t r9_5;
    struct bb_check_arrival_expand_ecl_ret_type mrv7 = {12UL, /*implicit*/(int)0, /*implicit*/(int)0};
    struct bb_check_arrival_expand_ecl_ret_type mrv8;
    uint64_t *var_16;
    uint64_t rbx_1;
    uint64_t *var_17;
    uint64_t *var_18;
    uint64_t r12_1;
    uint64_t rbp_1;
    uint64_t var_19;
    uint64_t var_20;
    uint64_t var_21;
    uint64_t var_22;
    uint64_t var_23;
    uint64_t r9_3;
    uint64_t var_24;
    uint64_t var_25;
    uint64_t var_26;
    uint32_t var_27;
    uint64_t var_29;
    uint64_t var_30;
    uint64_t var_31;
    uint32_t var_33;
    uint64_t var_28;
    uint64_t var_34;
    uint64_t var_35;
    uint64_t var_37;
    uint64_t var_38;
    uint64_t var_39;
    uint64_t var_40;
    uint32_t var_41;
    uint64_t local_sp_3;
    struct bb_check_arrival_expand_ecl_ret_type mrv4 = {0UL, /*implicit*/(int)0, /*implicit*/(int)0};
    struct bb_check_arrival_expand_ecl_ret_type mrv5;
    unsigned int loop_state_var;
    bool switch_state_var;
    var_0 = revng_init_local_sp(0UL);
    var_1 = init_r13();
    var_2 = init_rbx();
    var_3 = init_r10();
    var_4 = init_r12();
    var_5 = init_r9();
    var_6 = init_rbp();
    var_7 = init_r14();
    *(uint64_t *)(var_0 + (-8L)) = var_7;
    *(uint64_t *)(var_0 + (-16L)) = var_1;
    *(uint64_t *)(var_0 + (-24L)) = var_4;
    *(uint64_t *)(var_0 + (-32L)) = var_6;
    *(uint64_t *)(var_0 + (-40L)) = var_2;
    var_8 = (uint64_t)(uint32_t)rcx;
    var_9 = (uint64_t *)(rsi + 8UL);
    var_10 = *var_9;
    var_11 = (uint64_t *)(var_0 + (-64L));
    *var_11 = 0UL;
    var_12 = var_10 << 3UL;
    *(uint64_t *)(var_0 + (-72L)) = var_10;
    var_13 = var_0 + (-96L);
    *(uint64_t *)var_13 = 4257414UL;
    var_14 = indirect_placeholder_299(var_12);
    var_15 = var_14.field_0;
    *var_11 = var_15;
    r10_2 = var_3;
    r9_2 = var_5;
    local_sp_2 = var_13;
    r10_4 = var_3;
    r9_5 = var_5;
    rbx_1 = var_8;
    r12_1 = 0UL;
    rbp_1 = rdx;
    r9_3 = 0UL;
    local_sp_3 = var_13;
    if ((var_15 != 0UL) || (var_10 == 0UL)) {
        mrv7.field_1 = var_3;
        mrv8 = mrv7;
        mrv8.field_2 = var_5;
        return mrv8;
    }
    if ((long)*var_9 <= (long)0UL) {
        _pre_phi66 = (uint64_t *)(rsi + 16UL);
        *(uint64_t *)(local_sp_3 + (-8L)) = 4257588UL;
        indirect_placeholder();
        *(uint64_t *)rsi = *(uint64_t *)(local_sp_3 + 8UL);
        *var_9 = *(uint64_t *)(local_sp_3 + 16UL);
        *_pre_phi66 = *(uint64_t *)(local_sp_3 + 24UL);
        mrv4.field_1 = r10_4;
        mrv5 = mrv4;
        mrv5.field_2 = r9_5;
        return mrv5;
    }
    var_16 = (uint64_t *)(rsi + 16UL);
    var_17 = (uint64_t *)(rdi + 48UL);
    var_18 = (uint64_t *)rdi;
    _pre_phi66 = var_16;
    while (1U)
        {
            var_19 = *(uint64_t *)((r12_1 << 3UL) + *var_16);
            var_20 = (var_19 * 24UL) + *var_17;
            var_21 = *(uint64_t *)(var_20 + 8UL);
            var_22 = helper_cc_compute_all_wrapper(var_21, 0UL, 0UL, 25U);
            r10_3 = r10_2;
            rbx_0 = rbx_1;
            r9_4 = r9_2;
            r12_0 = r12_1;
            rbp_0 = rbp_1;
            if ((uint64_t)(((unsigned char)(var_22 >> 4UL) ^ (unsigned char)var_22) & '\xc0') == 0UL) {
                var_34 = local_sp_2 + 16UL;
                var_35 = local_sp_2 + (-8L);
                *(uint64_t *)var_35 = 4257642UL;
                var_36 = indirect_placeholder_300(rbx_1, var_34, r12_1, rbp_1, var_20);
                var_37 = var_36.field_0;
                var_38 = var_36.field_1;
                var_39 = var_36.field_2;
                var_40 = var_36.field_3;
                var_41 = (uint32_t)var_37;
                local_sp_0 = var_35;
                rbx_0 = var_38;
                r10_0 = r10_3;
                r12_0 = var_39;
                r9_0 = r9_4;
                rbp_0 = var_40;
                _pre_phi = var_41;
                local_sp_1 = var_35;
                r10_1 = r10_3;
                r9_1 = r9_4;
                if ((uint64_t)var_41 != 0UL) {
                    loop_state_var = 1U;
                    break;
                }
                var_42 = r12_0 + 1UL;
                r10_2 = r10_0;
                r9_2 = r9_0;
                local_sp_2 = local_sp_0;
                r10_4 = r10_0;
                r9_5 = r9_0;
                rbx_1 = rbx_0;
                r12_1 = var_42;
                rbp_1 = rbp_0;
                local_sp_3 = local_sp_0;
                if ((long)*var_9 > (long)var_42) {
                    continue;
                }
                loop_state_var = 0U;
                break;
            }
            var_23 = *var_18;
            r10_3 = 18446744073709551615UL;
            while (1U)
                {
                    var_24 = *(uint64_t *)((r9_3 << 3UL) + *(uint64_t *)(var_20 + 16UL));
                    var_25 = (var_24 << 4UL) + var_23;
                    var_26 = (uint64_t)*(unsigned char *)(var_25 + 8UL);
                    var_27 = (uint32_t)rbx_1;
                    r9_4 = r9_3;
                    r10_0 = var_24;
                    r9_0 = r9_3;
                    r10_1 = var_24;
                    r9_1 = r9_3;
                    if ((uint64_t)(var_27 - (uint32_t)var_26) != 0UL) {
                        if (rbp_1 != *(uint64_t *)var_25) {
                            loop_state_var = 0U;
                            break;
                        }
                    }
                    var_28 = r9_3 + 1UL;
                    r9_3 = var_28;
                    r10_3 = var_24;
                    r9_4 = var_21;
                    if (var_28 == var_21) {
                        continue;
                    }
                    loop_state_var = 1U;
                    break;
                }
            if (var_24 != 18446744073709551615UL) {
                var_29 = local_sp_2 + 16UL;
                var_30 = (uint64_t)var_27;
                var_31 = local_sp_2 + (-8L);
                *(uint64_t *)var_31 = 4257565UL;
                var_32 = indirect_placeholder_8(rdi, rbp_1, var_19, var_29, var_30);
                var_33 = (uint32_t)var_32;
                local_sp_0 = var_31;
                _pre_phi = var_33;
                local_sp_1 = var_31;
                if ((uint64_t)var_33 != 0UL) {
                    loop_state_var = 1U;
                    switch_state_var = 1;
                    break;
                }
                var_42 = r12_0 + 1UL;
                r10_2 = r10_0;
                r9_2 = r9_0;
                local_sp_2 = local_sp_0;
                r10_4 = r10_0;
                r9_5 = r9_0;
                rbx_1 = rbx_0;
                r12_1 = var_42;
                rbp_1 = rbp_0;
                local_sp_3 = local_sp_0;
                if ((long)*var_9 > (long)var_42) {
                    continue;
                }
                loop_state_var = 0U;
                switch_state_var = 1;
                break;
            }
        }
    switch (loop_state_var) {
      case 0U:
        {
            break;
        }
        break;
      case 1U:
        {
            *(uint32_t *)(local_sp_1 + 12UL) = _pre_phi;
            *(uint64_t *)(local_sp_1 + (-8L)) = 4257660UL;
            indirect_placeholder();
            mrv.field_0 = (uint64_t)*(uint32_t *)(local_sp_1 + 4UL);
            mrv1 = mrv;
            mrv1.field_1 = r10_1;
            mrv2 = mrv1;
            mrv2.field_2 = r9_1;
            return mrv2;
        }
        break;
    }
}

typedef unsigned char __u_char;
typedef unsigned short __u_short;
typedef unsigned int __u_int;
typedef unsigned long __u_long;
typedef signed char __int8_t;
typedef unsigned char __uint8_t;
typedef short __int16_t;
typedef unsigned short __uint16_t;
typedef int __int32_t;
typedef unsigned int __uint32_t;
typedef long __int64_t;
typedef unsigned long __uint64_t;
typedef __int8_t __int_least8_t;
typedef __uint8_t __uint_least8_t;
typedef __int16_t __int_least16_t;
typedef __uint16_t __uint_least16_t;
typedef __int32_t __int_least32_t;
typedef __uint32_t __uint_least32_t;
typedef __int64_t __int_least64_t;
typedef __uint64_t __uint_least64_t;
typedef long __quad_t;
typedef unsigned long __u_quad_t;
typedef long __intmax_t;
typedef unsigned long __uintmax_t;
typedef unsigned long __dev_t;
typedef unsigned int __uid_t;
typedef unsigned int __gid_t;
typedef unsigned long __ino_t;
typedef unsigned long __ino64_t;
typedef unsigned int __mode_t;
typedef unsigned long __nlink_t;
typedef long __off_t;
typedef long __off64_t;
typedef int __pid_t;
typedef struct {
    int __val[2];
} __fsid_t;
typedef long __clock_t;
typedef unsigned long __rlim_t;
typedef unsigned long __rlim64_t;
typedef unsigned int __id_t;
typedef long __time_t;
typedef unsigned int __useconds_t;
typedef long __suseconds_t;
typedef int __daddr_t;
typedef int __key_t;
typedef int __clockid_t;
typedef void *__timer_t;
typedef long __blksize_t;
typedef long __blkcnt_t;
typedef long __blkcnt64_t;
typedef unsigned long __fsblkcnt_t;
typedef unsigned long __fsblkcnt64_t;
typedef unsigned long __fsfilcnt_t;
typedef unsigned long __fsfilcnt64_t;
typedef long __fsword_t;
typedef long __ssize_t;
typedef long __syscall_slong_t;
typedef unsigned long __syscall_ulong_t;
typedef __off64_t __loff_t;
typedef char *__caddr_t;
typedef long __intptr_t;
typedef unsigned int __socklen_t;
typedef int __sig_atomic_t;
typedef __int8_t int8_t;
typedef __int16_t int16_t;
typedef __int32_t int32_t;
typedef __int64_t int64_t;
typedef __uint8_t uint8_t;
typedef __uint16_t uint16_t;
typedef __uint32_t uint32_t;
typedef __uint64_t uint64_t;
typedef __int_least8_t int_least8_t;
typedef __int_least16_t int_least16_t;
typedef __int_least32_t int_least32_t;
typedef __int_least64_t int_least64_t;
typedef __uint_least8_t uint_least8_t;
typedef __uint_least16_t uint_least16_t;
typedef __uint_least32_t uint_least32_t;
typedef __uint_least64_t uint_least64_t;
typedef signed char int_fast8_t;
typedef long int_fast16_t;
typedef long int_fast32_t;
typedef long int_fast64_t;
typedef unsigned char uint_fast8_t;
typedef unsigned long uint_fast16_t;
typedef unsigned long uint_fast32_t;
typedef unsigned long uint_fast64_t;
typedef long intptr_t;
typedef unsigned long uintptr_t;
typedef __intmax_t intmax_t;
typedef __uintmax_t uintmax_t;
typedef _Bool bool;
struct bb_check_halt_state_context_isra_21_ret_type;
struct bb_check_halt_state_context_isra_21_ret_type {
    uint64_t field_0;
    uint64_t field_1;
};
extern uint64_t revng_init_local_sp(uint64_t param_0);
extern uint64_t init_rbx(void);
extern uint64_t init_r12(void);
extern uint64_t init_rbp(void);
extern uint64_t helper_cc_compute_all_wrapper(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint32_t param_3);
extern uint64_t indirect_placeholder_3(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint64_t param_3);
extern uint64_t init_r9(void);
struct bb_check_halt_state_context_isra_21_ret_type bb_check_halt_state_context_isra_21(uint64_t rax, uint64_t rdi, uint64_t rcx, uint64_t rdx, uint64_t rsi) {
    uint64_t var_4;
    uint64_t var_0;
    uint64_t var_1;
    uint64_t var_2;
    uint64_t var_3;
    uint64_t var_5;
    uint64_t var_6;
    uint64_t var_7;
    uint64_t r9_4;
    uint64_t *var_8;
    uint64_t var_9;
    uint64_t var_10;
    uint64_t var_11;
    uint64_t var_12;
    uint64_t var_13;
    bool var_14;
    bool var_15;
    bool var_16;
    uint64_t r9_0;
    uint64_t r8_0;
    uint64_t var_23;
    uint64_t var_24;
    uint32_t var_25;
    uint64_t var_26;
    uint64_t var_27;
    uint64_t rax4_0;
    uint64_t var_28;
    uint64_t var_29;
    uint64_t r8_1;
    uint64_t var_17;
    uint64_t var_18;
    uint32_t var_19;
    uint64_t var_20;
    uint64_t var_21;
    uint64_t var_55;
    uint64_t var_22;
    uint64_t r9_1;
    uint64_t r8_2;
    uint64_t var_37;
    uint64_t var_38;
    uint32_t var_39;
    uint64_t var_40;
    uint64_t var_41;
    uint64_t var_42;
    uint64_t var_43;
    uint64_t r9_2;
    uint64_t r8_3;
    uint64_t var_30;
    uint64_t var_31;
    uint32_t var_32;
    uint64_t var_33;
    uint64_t var_34;
    uint64_t var_35;
    uint64_t var_36;
    uint64_t r8_4;
    uint64_t var_50;
    uint64_t var_51;
    uint32_t var_52;
    uint64_t var_53;
    uint64_t var_54;
    uint64_t r8_5;
    uint64_t var_44;
    uint64_t var_45;
    uint32_t var_46;
    uint64_t var_47;
    uint64_t var_48;
    uint64_t var_49;
    uint64_t r8_6;
    uint64_t var_62;
    uint64_t var_63;
    uint32_t var_64;
    uint64_t var_65;
    uint64_t var_66;
    uint64_t r8_7;
    uint64_t var_67;
    uint64_t var_56;
    uint64_t var_57;
    uint32_t var_58;
    uint64_t var_59;
    uint64_t var_60;
    uint64_t r9_3;
    struct bb_check_halt_state_context_isra_21_ret_type mrv;
    struct bb_check_halt_state_context_isra_21_ret_type mrv1;
    uint64_t var_61;
    struct bb_check_halt_state_context_isra_21_ret_type mrv3 = {0UL, /*implicit*/(int)0};
    unsigned int loop_state_var;
    var_0 = revng_init_local_sp(0UL);
    var_1 = init_rbx();
    var_2 = init_r12();
    var_3 = init_r9();
    var_4 = init_rbp();
    *(uint64_t *)(var_0 + (-8L)) = var_2;
    *(uint64_t *)(var_0 + (-16L)) = var_4;
    *(uint64_t *)(var_0 + (-24L)) = var_1;
    var_5 = (uint64_t)*(uint32_t *)(rdi + 160UL);
    *(uint64_t *)(var_0 + (-32L)) = 4240827UL;
    var_6 = indirect_placeholder_3(rax, rdi, var_5, rcx);
    var_7 = helper_cc_compute_all_wrapper(rsi, 0UL, 0UL, 25U);
    r9_4 = var_3;
    r8_0 = 0UL;
    r8_1 = 0UL;
    r8_2 = 0UL;
    r8_3 = 0UL;
    r8_4 = 0UL;
    r8_5 = 0UL;
    r8_6 = 0UL;
    r8_7 = 0UL;
    if ((uint64_t)(((unsigned char)(var_7 >> 4UL) ^ (unsigned char)var_7) & '\xc0') == 0UL) {
        mrv3.field_1 = r9_4;
        return mrv3;
    }
    var_8 = *(uint64_t **)(rdi + 152UL);
    var_9 = var_6 & 2UL;
    var_10 = var_6 & 8UL;
    var_11 = var_6 & 1UL;
    var_12 = *(uint64_t *)rdx;
    var_13 = *var_8;
    var_14 = (var_11 == 0UL);
    var_15 = (var_10 == 0UL);
    var_16 = (var_9 == 0UL);
    if (var_14) {
        if (!var_15) {
            if (!var_16) {
                while (1U)
                    {
                        var_44 = *(uint64_t *)((r8_5 << 3UL) + var_12);
                        var_45 = ((var_44 << 4UL) + var_13) + 8UL;
                        var_46 = *(uint32_t *)var_45 >> 8U;
                        var_47 = (uint64_t)var_46;
                        var_48 = (uint64_t)(var_46 & 16712703U);
                        rax4_0 = var_44;
                        r9_3 = var_48;
                        r9_4 = var_48;
                        if (*(unsigned char *)var_45 != '\x02') {
                            if (!(((uint64_t)((uint16_t)var_47 & (unsigned short)1023U) == 0UL) || ((var_47 & 4UL) == 0UL))) {
                                loop_state_var = 0U;
                                break;
                            }
                        }
                        var_49 = r8_5 + 1UL;
                        r8_5 = var_49;
                        if (var_49 == rsi) {
                            continue;
                        }
                        loop_state_var = 1U;
                        break;
                    }
                mrv3.field_1 = r9_4;
                return mrv3;
            }
            while (1U)
                {
                    var_50 = *(uint64_t *)((r8_4 << 3UL) + var_12);
                    var_51 = ((var_50 << 4UL) + var_13) + 8UL;
                    var_52 = *(uint32_t *)var_51 >> 8U;
                    var_53 = (uint64_t)var_52;
                    var_54 = (uint64_t)(var_52 & 16712703U);
                    rax4_0 = var_50;
                    r9_3 = var_54;
                    r9_4 = var_54;
                    if (*(unsigned char *)var_51 == '\x02') {
                        var_55 = r8_4 + 1UL;
                        r8_4 = var_55;
                        if (var_55 == rsi) {
                            continue;
                        }
                        loop_state_var = 1U;
                        break;
                    }
                    if ((uint64_t)((uint16_t)var_53 & (unsigned short)1023U) != 0UL) {
                        loop_state_var = 0U;
                        break;
                    }
                    if (!(((var_53 & 4UL) == 0UL) && ((var_53 & 32UL) == 0UL))) {
                        loop_state_var = 0U;
                        break;
                    }
                }
            switch (loop_state_var) {
              case 1U:
                {
                    mrv3.field_1 = r9_4;
                    return mrv3;
                }
                break;
              case 0U:
                {
                    break;
                }
                break;
            }
        }
        if (!var_16) {
            while (1U)
                {
                    var_62 = *(uint64_t *)((r8_6 << 3UL) + var_12);
                    var_63 = ((var_62 << 4UL) + var_13) + 8UL;
                    var_64 = *(uint32_t *)var_63 >> 8U;
                    var_65 = (uint64_t)var_64;
                    var_66 = (uint64_t)(var_64 & 16712703U);
                    rax4_0 = var_62;
                    r9_3 = var_66;
                    r9_4 = var_66;
                    if (*(unsigned char *)var_63 == '\x02') {
                        var_67 = r8_6 + 1UL;
                        r8_6 = var_67;
                        if (var_67 == rsi) {
                            continue;
                        }
                        loop_state_var = 1U;
                        break;
                    }
                    if ((uint64_t)((uint16_t)var_65 & (unsigned short)1023U) != 0UL) {
                        loop_state_var = 0U;
                        break;
                    }
                    if (!((((var_65 & 4UL) == 0UL) && ((var_65 & 32UL) == 0UL)) && ((signed char)(unsigned char)var_64 > '\xff'))) {
                        loop_state_var = 0U;
                        break;
                    }
                }
            mrv3.field_1 = r9_4;
            return mrv3;
        }
        while (1U)
            {
                var_56 = *(uint64_t *)((r8_7 << 3UL) + var_12);
                var_57 = ((var_56 << 4UL) + var_13) + 8UL;
                var_58 = *(uint32_t *)var_57 >> 8U;
                var_59 = (uint64_t)var_58;
                var_60 = (uint64_t)(var_58 & 16712703U);
                rax4_0 = var_56;
                r9_3 = var_60;
                r9_4 = var_60;
                if (*(unsigned char *)var_57 == '\x02') {
                    var_61 = r8_7 + 1UL;
                    r8_7 = var_61;
                    if (var_61 == rsi) {
                        continue;
                    }
                    loop_state_var = 1U;
                    break;
                }
                if ((uint64_t)((uint16_t)var_59 & (unsigned short)1023U) != 0UL) {
                    loop_state_var = 0U;
                    break;
                }
                if (!(((var_59 & 4UL) == 0UL) && ((signed char)(unsigned char)var_58 > '\xff'))) {
                    loop_state_var = 0U;
                    break;
                }
            }
        switch (loop_state_var) {
          case 1U:
            {
                mrv3.field_1 = r9_4;
                return mrv3;
            }
            break;
          case 0U:
            {
                break;
            }
            break;
        }
    }
    if (var_15) {
        if (!var_16) {
            while (1U)
                {
                    var_37 = *(uint64_t *)((r8_2 << 3UL) + var_12);
                    var_38 = ((var_37 << 4UL) + var_13) + 8UL;
                    var_39 = *(uint32_t *)var_38 >> 8U;
                    var_40 = (uint64_t)var_39;
                    var_41 = (uint64_t)(var_39 & 16712703U);
                    r9_1 = var_41;
                    rax4_0 = var_37;
                    r9_3 = var_41;
                    if (*(unsigned char *)var_38 == '\x02') {
                        var_43 = r8_2 + 1UL;
                        r8_2 = var_43;
                        r9_4 = r9_1;
                        if (var_43 == rsi) {
                            continue;
                        }
                        loop_state_var = 1U;
                        break;
                    }
                    if ((uint64_t)((uint16_t)var_40 & (unsigned short)1023U) != 0UL) {
                        loop_state_var = 0U;
                        break;
                    }
                    r9_3 = 0UL;
                    var_42 = (uint64_t)((unsigned char)var_40 & '\x80');
                    r9_1 = var_42;
                    if (!(((var_40 & 8UL) == 0UL) && ((var_40 & 32UL) == 0UL)) & var_42 != 0UL) {
                        loop_state_var = 0U;
                        break;
                    }
                }
            mrv3.field_1 = r9_4;
            return mrv3;
        }
        while (1U)
            {
                var_30 = *(uint64_t *)((r8_3 << 3UL) + var_12);
                var_31 = ((var_30 << 4UL) + var_13) + 8UL;
                var_32 = *(uint32_t *)var_31 >> 8U;
                var_33 = (uint64_t)var_32;
                var_34 = (uint64_t)(var_32 & 16712703U);
                r9_2 = var_34;
                rax4_0 = var_30;
                r9_3 = var_34;
                if (*(unsigned char *)var_31 == '\x02') {
                    var_36 = r8_3 + 1UL;
                    r8_3 = var_36;
                    r9_4 = r9_2;
                    if (var_36 == rsi) {
                        continue;
                    }
                    loop_state_var = 1U;
                    break;
                }
                if ((uint64_t)((uint16_t)var_33 & (unsigned short)1023U) != 0UL) {
                    loop_state_var = 0U;
                    break;
                }
                r9_3 = 0UL;
                var_35 = (uint64_t)((unsigned char)var_33 & '\x80');
                r9_2 = var_35;
                if ((var_33 & 8UL) != 0UL & var_35 != 0UL) {
                    loop_state_var = 0U;
                    break;
                }
            }
        switch (loop_state_var) {
          case 1U:
            {
                mrv3.field_1 = r9_4;
                return mrv3;
            }
            break;
          case 0U:
            {
                break;
            }
            break;
        }
    }
    if (var_16) {
        while (1U)
            {
                var_23 = *(uint64_t *)((r8_0 << 3UL) + var_12);
                var_24 = ((var_23 << 4UL) + var_13) + 8UL;
                var_25 = *(uint32_t *)var_24 >> 8U;
                var_26 = (uint64_t)var_25;
                var_27 = (uint64_t)(var_25 & 16712703U);
                r9_0 = var_27;
                rax4_0 = var_23;
                r9_3 = var_27;
                if (*(unsigned char *)var_24 == '\x02') {
                    var_29 = r8_0 + 1UL;
                    r8_0 = var_29;
                    r9_4 = r9_0;
                    if (var_29 == rsi) {
                        continue;
                    }
                    loop_state_var = 1U;
                    break;
                }
                if ((uint64_t)((uint16_t)var_26 & (unsigned short)1023U) != 0UL) {
                    loop_state_var = 0U;
                    break;
                }
                r9_3 = 0UL;
                var_28 = var_26 & 32UL;
                r9_0 = var_28;
                if ((var_26 & 8UL) != 0UL & var_28 != 0UL) {
                    loop_state_var = 0U;
                    break;
                }
            }
        switch (loop_state_var) {
          case 1U:
            {
                mrv3.field_1 = r9_4;
                return mrv3;
            }
            break;
          case 0U:
            {
                break;
            }
            break;
        }
    }
    while (1U)
        {
            var_17 = *(uint64_t *)((r8_1 << 3UL) + var_12);
            var_18 = ((var_17 << 4UL) + var_13) + 8UL;
            var_19 = *(uint32_t *)var_18 >> 8U;
            var_20 = (uint64_t)var_19;
            var_21 = (uint64_t)(var_19 & 16712703U);
            rax4_0 = var_17;
            r9_3 = var_21;
            r9_4 = var_21;
            if (*(unsigned char *)var_18 != '\x02') {
                if (!(((uint64_t)((uint16_t)var_20 & (unsigned short)1023U) == 0UL) || ((var_20 & 8UL) == 0UL))) {
                    loop_state_var = 0U;
                    break;
                }
            }
            var_22 = r8_1 + 1UL;
            r8_1 = var_22;
            if (var_22 == rsi) {
                continue;
            }
            loop_state_var = 1U;
            break;
        }
    switch (loop_state_var) {
      case 1U:
        {
            mrv3.field_1 = r9_4;
            return mrv3;
        }
        break;
      case 0U:
        {
            mrv.field_0 = rax4_0;
            mrv1 = mrv;
            mrv1.field_1 = r9_3;
            return mrv1;
        }
        break;
    }
}

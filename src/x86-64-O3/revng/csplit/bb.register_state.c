typedef unsigned char __u_char;
typedef unsigned short __u_short;
typedef unsigned int __u_int;
typedef unsigned long __u_long;
typedef signed char __int8_t;
typedef unsigned char __uint8_t;
typedef short __int16_t;
typedef unsigned short __uint16_t;
typedef int __int32_t;
typedef unsigned int __uint32_t;
typedef long __int64_t;
typedef unsigned long __uint64_t;
typedef __int8_t __int_least8_t;
typedef __uint8_t __uint_least8_t;
typedef __int16_t __int_least16_t;
typedef __uint16_t __uint_least16_t;
typedef __int32_t __int_least32_t;
typedef __uint32_t __uint_least32_t;
typedef __int64_t __int_least64_t;
typedef __uint64_t __uint_least64_t;
typedef long __quad_t;
typedef unsigned long __u_quad_t;
typedef long __intmax_t;
typedef unsigned long __uintmax_t;
typedef unsigned long __dev_t;
typedef unsigned int __uid_t;
typedef unsigned int __gid_t;
typedef unsigned long __ino_t;
typedef unsigned long __ino64_t;
typedef unsigned int __mode_t;
typedef unsigned long __nlink_t;
typedef long __off_t;
typedef long __off64_t;
typedef int __pid_t;
typedef struct {
    int __val[2];
} __fsid_t;
typedef long __clock_t;
typedef unsigned long __rlim_t;
typedef unsigned long __rlim64_t;
typedef unsigned int __id_t;
typedef long __time_t;
typedef unsigned int __useconds_t;
typedef long __suseconds_t;
typedef int __daddr_t;
typedef int __key_t;
typedef int __clockid_t;
typedef void *__timer_t;
typedef long __blksize_t;
typedef long __blkcnt_t;
typedef long __blkcnt64_t;
typedef unsigned long __fsblkcnt_t;
typedef unsigned long __fsblkcnt64_t;
typedef unsigned long __fsfilcnt_t;
typedef unsigned long __fsfilcnt64_t;
typedef long __fsword_t;
typedef long __ssize_t;
typedef long __syscall_slong_t;
typedef unsigned long __syscall_ulong_t;
typedef __off64_t __loff_t;
typedef char *__caddr_t;
typedef long __intptr_t;
typedef unsigned int __socklen_t;
typedef int __sig_atomic_t;
typedef __int8_t int8_t;
typedef __int16_t int16_t;
typedef __int32_t int32_t;
typedef __int64_t int64_t;
typedef __uint8_t uint8_t;
typedef __uint16_t uint16_t;
typedef __uint32_t uint32_t;
typedef __uint64_t uint64_t;
typedef __int_least8_t int_least8_t;
typedef __int_least16_t int_least16_t;
typedef __int_least32_t int_least32_t;
typedef __int_least64_t int_least64_t;
typedef __uint_least8_t uint_least8_t;
typedef __uint_least16_t uint_least16_t;
typedef __uint_least32_t uint_least32_t;
typedef __uint_least64_t uint_least64_t;
typedef signed char int_fast8_t;
typedef long int_fast16_t;
typedef long int_fast32_t;
typedef long int_fast64_t;
typedef unsigned char uint_fast8_t;
typedef unsigned long uint_fast16_t;
typedef unsigned long uint_fast32_t;
typedef unsigned long uint_fast64_t;
typedef long intptr_t;
typedef unsigned long uintptr_t;
typedef __intmax_t intmax_t;
typedef __uintmax_t uintmax_t;
struct indirect_placeholder_279_ret_type;
struct indirect_placeholder_279_ret_type {
    uint64_t field_0;
    uint64_t field_1;
};
extern uint64_t revng_init_local_sp(uint64_t param_0);
extern uint64_t init_rbx(void);
extern uint64_t init_r12(void);
extern uint64_t init_rbp(void);
extern uint64_t init_r13(void);
extern uint64_t init_r14(void);
extern uint64_t helper_cc_compute_all_wrapper(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint32_t param_3);
extern void indirect_placeholder_4(uint64_t param_0, uint64_t param_1);
extern struct indirect_placeholder_279_ret_type indirect_placeholder_279(uint64_t param_0);
typedef _Bool bool;
uint64_t bb_register_state(uint64_t rdi, uint64_t rdx, uint64_t rsi) {
    uint64_t var_30;
    uint64_t var_31;
    uint64_t var_32;
    uint64_t *_cast;
    uint64_t var_33;
    uint64_t *var_34;
    uint64_t rdx2_2;
    uint64_t var_35;
    uint64_t *var_36;
    uint64_t var_37;
    uint64_t var_38;
    uint64_t var_0;
    uint64_t var_1;
    uint64_t var_2;
    uint64_t var_3;
    uint64_t var_4;
    uint64_t var_5;
    uint64_t *var_6;
    uint64_t var_7;
    uint64_t *var_8;
    uint64_t var_9;
    uint64_t *var_10;
    uint64_t var_11;
    struct indirect_placeholder_279_ret_type var_12;
    uint64_t var_13;
    uint64_t *var_14;
    uint64_t var_39;
    uint64_t rax_1;
    uint64_t local_sp_1;
    uint64_t var_15;
    uint64_t var_16;
    uint64_t rdx2_1;
    uint64_t var_27;
    uint64_t local_sp_2;
    uint64_t rax_0;
    uint64_t local_sp_0_ph;
    uint64_t rbx_0_ph;
    uint64_t rdx2_0_ph;
    uint64_t var_17;
    uint64_t var_18;
    uint64_t rbx_0;
    uint64_t var_19;
    uint64_t var_21;
    uint64_t var_28;
    uint64_t var_29;
    uint64_t var_22;
    uint64_t var_23;
    uint64_t var_24;
    uint64_t var_25;
    uint64_t var_26;
    uint64_t var_20;
    unsigned int loop_state_var;
    bool switch_state_var;
    var_0 = revng_init_local_sp(0UL);
    var_1 = init_r13();
    var_2 = init_rbx();
    var_3 = init_r12();
    var_4 = init_rbp();
    var_5 = init_r14();
    *(uint64_t *)(var_0 + (-8L)) = var_5;
    *(uint64_t *)(var_0 + (-16L)) = var_1;
    *(uint64_t *)(var_0 + (-24L)) = var_3;
    *(uint64_t *)(var_0 + (-32L)) = var_4;
    *(uint64_t *)(var_0 + (-40L)) = var_2;
    var_6 = (uint64_t *)(rsi + 16UL);
    var_7 = *var_6;
    *(uint64_t *)rsi = rdx;
    var_8 = (uint64_t *)(rsi + 40UL);
    *var_8 = 0UL;
    var_9 = var_7 << 3UL;
    var_10 = (uint64_t *)(rsi + 32UL);
    *var_10 = var_7;
    var_11 = var_0 + (-48L);
    *(uint64_t *)var_11 = 4247554UL;
    var_12 = indirect_placeholder_279(var_9);
    var_13 = var_12.field_0;
    var_14 = (uint64_t *)(rsi + 48UL);
    *var_14 = var_13;
    local_sp_0_ph = var_11;
    rbx_0_ph = 0UL;
    local_sp_2 = var_11;
    if ((var_13 != 0UL) || (var_7 == 0UL)) {
        return 12UL;
    }
    var_15 = *var_6;
    var_16 = helper_cc_compute_all_wrapper(var_15, 0UL, 0UL, 25U);
    rdx2_0_ph = var_15;
    if ((uint64_t)(((unsigned char)(var_16 >> 4UL) ^ (unsigned char)var_16) & '\xc0') == 0UL) {
        var_30 = *(uint64_t *)(rdi + 136UL) & rdx;
        var_31 = *(uint64_t *)(rdi + 64UL);
        var_32 = (var_30 * 24UL) + var_31;
        _cast = (uint64_t *)var_32;
        var_33 = *_cast;
        var_34 = (uint64_t *)(var_32 + 8UL);
        rax_1 = var_31;
        rdx2_2 = var_33;
        if ((long)*var_34 > (long)var_33) {
            rax_1 = *(uint64_t *)(var_32 + 16UL);
        } else {
            var_35 = var_33 + 1UL;
            var_36 = (uint64_t *)(var_32 + 16UL);
            var_37 = *var_36;
            var_38 = var_35 << 4UL;
            *(uint64_t *)(local_sp_2 + (-8L)) = 4247793UL;
            indirect_placeholder_4(var_37, var_38);
            if (var_31 != 0UL) {
                return 12UL;
            }
            var_39 = var_35 << 1UL;
            *var_36 = var_31;
            *var_34 = var_39;
            rdx2_2 = *_cast;
        }
        *_cast = (rdx2_2 + 1UL);
        *(uint64_t *)((rdx2_2 << 3UL) + rax_1) = rsi;
        return 0UL;
    }
    while (1U)
        {
            var_17 = *(uint64_t *)(rsi + 24UL);
            var_18 = *(uint64_t *)rdi;
            rbx_0 = rbx_0_ph;
            local_sp_1 = local_sp_0_ph;
            local_sp_2 = local_sp_0_ph;
            while (1U)
                {
                    var_19 = *(uint64_t *)((rbx_0 << 3UL) + var_17);
                    if ((*(unsigned char *)(((var_19 << 4UL) + var_18) + 8UL) & '\b') != '\x00') {
                        loop_state_var = 1U;
                        break;
                    }
                    var_20 = rbx_0 + 1UL;
                    rbx_0 = var_20;
                    if ((long)var_20 < (long)rdx2_0_ph) {
                        continue;
                    }
                    loop_state_var = 0U;
                    break;
                }
            switch_state_var = 0;
            switch (loop_state_var) {
              case 1U:
                {
                    var_21 = *var_8;
                    rdx2_1 = var_21;
                    if (*var_10 == var_21) {
                        rax_0 = *var_14;
                    } else {
                        var_22 = var_21 + 1UL;
                        var_23 = *var_14;
                        var_24 = var_22 << 1UL;
                        var_25 = var_22 << 4UL;
                        *var_10 = var_24;
                        var_26 = local_sp_0_ph + (-8L);
                        *(uint64_t *)var_26 = 4247753UL;
                        indirect_placeholder_4(var_23, var_25);
                        local_sp_1 = var_26;
                        rax_0 = var_24;
                        if (var_24 != 0UL) {
                            loop_state_var = 1U;
                            switch_state_var = 1;
                            break;
                        }
                        *var_14 = var_24;
                        rdx2_1 = *var_8;
                    }
                    var_27 = rdx2_1 + 1UL;
                    var_28 = rbx_0 + 1UL;
                    *var_8 = var_27;
                    *(uint64_t *)((rdx2_1 << 3UL) + rax_0) = var_19;
                    var_29 = *var_6;
                    local_sp_0_ph = local_sp_1;
                    rbx_0_ph = var_28;
                    rdx2_0_ph = var_29;
                    local_sp_2 = local_sp_1;
                    if ((long)var_28 < (long)var_29) {
                        continue;
                    }
                    loop_state_var = 0U;
                    switch_state_var = 1;
                    break;
                }
                break;
              case 0U:
                {
                    loop_state_var = 0U;
                    switch_state_var = 1;
                    break;
                }
                break;
            }
            if (switch_state_var)
                break;
        }
    switch (loop_state_var) {
      case 1U:
        {
            return 12UL;
        }
        break;
      case 0U:
        {
            break;
        }
        break;
    }
}

typedef unsigned char __u_char;
typedef unsigned short __u_short;
typedef unsigned int __u_int;
typedef unsigned long __u_long;
typedef signed char __int8_t;
typedef unsigned char __uint8_t;
typedef short __int16_t;
typedef unsigned short __uint16_t;
typedef int __int32_t;
typedef unsigned int __uint32_t;
typedef long __int64_t;
typedef unsigned long __uint64_t;
typedef __int8_t __int_least8_t;
typedef __uint8_t __uint_least8_t;
typedef __int16_t __int_least16_t;
typedef __uint16_t __uint_least16_t;
typedef __int32_t __int_least32_t;
typedef __uint32_t __uint_least32_t;
typedef __int64_t __int_least64_t;
typedef __uint64_t __uint_least64_t;
typedef long __quad_t;
typedef unsigned long __u_quad_t;
typedef long __intmax_t;
typedef unsigned long __uintmax_t;
typedef unsigned long __dev_t;
typedef unsigned int __uid_t;
typedef unsigned int __gid_t;
typedef unsigned long __ino_t;
typedef unsigned long __ino64_t;
typedef unsigned int __mode_t;
typedef unsigned long __nlink_t;
typedef long __off_t;
typedef long __off64_t;
typedef int __pid_t;
typedef struct {
    int __val[2];
} __fsid_t;
typedef long __clock_t;
typedef unsigned long __rlim_t;
typedef unsigned long __rlim64_t;
typedef unsigned int __id_t;
typedef long __time_t;
typedef unsigned int __useconds_t;
typedef long __suseconds_t;
typedef int __daddr_t;
typedef int __key_t;
typedef int __clockid_t;
typedef void *__timer_t;
typedef long __blksize_t;
typedef long __blkcnt_t;
typedef long __blkcnt64_t;
typedef unsigned long __fsblkcnt_t;
typedef unsigned long __fsblkcnt64_t;
typedef unsigned long __fsfilcnt_t;
typedef unsigned long __fsfilcnt64_t;
typedef long __fsword_t;
typedef long __ssize_t;
typedef long __syscall_slong_t;
typedef unsigned long __syscall_ulong_t;
typedef __off64_t __loff_t;
typedef char *__caddr_t;
typedef long __intptr_t;
typedef unsigned int __socklen_t;
typedef int __sig_atomic_t;
typedef __int8_t int8_t;
typedef __int16_t int16_t;
typedef __int32_t int32_t;
typedef __int64_t int64_t;
typedef __uint8_t uint8_t;
typedef __uint16_t uint16_t;
typedef __uint32_t uint32_t;
typedef __uint64_t uint64_t;
typedef __int_least8_t int_least8_t;
typedef __int_least16_t int_least16_t;
typedef __int_least32_t int_least32_t;
typedef __int_least64_t int_least64_t;
typedef __uint_least8_t uint_least8_t;
typedef __uint_least16_t uint_least16_t;
typedef __uint_least32_t uint_least32_t;
typedef __uint_least64_t uint_least64_t;
typedef signed char int_fast8_t;
typedef long int_fast16_t;
typedef long int_fast32_t;
typedef long int_fast64_t;
typedef unsigned char uint_fast8_t;
typedef unsigned long uint_fast16_t;
typedef unsigned long uint_fast32_t;
typedef unsigned long uint_fast64_t;
typedef long intptr_t;
typedef unsigned long uintptr_t;
typedef __intmax_t intmax_t;
typedef __uintmax_t uintmax_t;
struct indirect_placeholder_1_ret_type;
struct indirect_placeholder_1_ret_type {
    uint64_t field_0;
    uint64_t field_1;
};
extern uint64_t revng_init_local_sp(uint64_t param_0);
extern uint64_t init_rbx(void);
extern uint64_t init_r12(void);
extern uint64_t init_rbp(void);
extern uint64_t init_r13(void);
extern struct indirect_placeholder_1_ret_type indirect_placeholder_1(uint64_t param_0);
typedef _Bool bool;
uint64_t bb_duplicate_tree(uint64_t rdi, uint64_t rsi) {
    uint64_t var_0;
    uint64_t var_1;
    uint64_t var_2;
    uint64_t var_3;
    uint64_t var_4;
    uint64_t var_5;
    uint64_t var_6;
    uint64_t var_7;
    uint32_t *var_8;
    uint64_t *var_9;
    uint64_t rax_0;
    uint64_t rbp_5;
    uint64_t rbx_0;
    uint64_t r12_0;
    uint64_t rbp_0;
    uint64_t local_sp_3;
    uint64_t rbp_2;
    uint64_t local_sp_0;
    uint32_t var_10;
    uint64_t var_11;
    uint64_t rbx_5;
    uint64_t rbp_4_ph;
    uint64_t rbx_1;
    uint64_t r12_1;
    uint64_t rdx_2;
    uint64_t rbp_1;
    uint64_t rdx_0;
    uint64_t local_sp_1;
    uint64_t r12_3;
    uint64_t rbx_2;
    uint64_t r12_2;
    uint64_t rcx_0;
    uint64_t rdx_1;
    uint64_t local_sp_2;
    uint64_t var_15;
    uint64_t var_16;
    uint64_t var_17;
    uint64_t *var_18;
    uint64_t var_19;
    unsigned char *var_20;
    uint64_t *var_21;
    unsigned char *var_22;
    uint64_t var_23;
    uint64_t var_24;
    uint64_t var_25;
    uint64_t rbx_4_ph;
    uint64_t rbx_3;
    uint64_t rbp_3;
    uint32_t var_29;
    uint64_t var_30;
    uint64_t var_31;
    uint64_t rbx_4;
    uint64_t rbp_4;
    uint64_t var_26;
    uint64_t var_27;
    uint64_t var_28;
    uint64_t var_12;
    struct indirect_placeholder_1_ret_type var_13;
    uint64_t var_14;
    unsigned int loop_state_var;
    bool switch_state_var;
    var_0 = revng_init_local_sp(0UL);
    var_1 = init_r13();
    var_2 = init_rbx();
    var_3 = init_r12();
    var_4 = init_rbp();
    *(uint64_t *)(var_0 + (-8L)) = var_1;
    *(uint64_t *)(var_0 + (-16L)) = var_3;
    *(uint64_t *)(var_0 + (-24L)) = var_4;
    *(uint64_t *)(var_0 + (-32L)) = var_2;
    var_5 = var_0 + (-56L);
    var_6 = *(uint64_t *)rdi;
    var_7 = var_0 + (-48L);
    var_8 = (uint32_t *)(rsi + 128UL);
    var_9 = (uint64_t *)(rsi + 112UL);
    rbx_0 = rdi;
    r12_0 = var_7;
    rbp_0 = var_6;
    local_sp_0 = var_5;
    rcx_0 = 1UL;
    rdx_1 = 0UL;
    loop_state_var = 2U;
    while (1U)
        {
            switch_state_var = 0;
            switch (loop_state_var) {
              case 2U:
                {
                    var_15 = rdx_1 << 6UL;
                    *var_8 = (uint32_t)rcx_0;
                    var_16 = rax_0 + var_15;
                    var_17 = var_16 + 8UL;
                    var_18 = (uint64_t *)var_17;
                    *var_18 = 0UL;
                    *(uint64_t *)(var_16 + 16UL) = 0UL;
                    *(uint64_t *)(var_16 + 24UL) = 0UL;
                    var_19 = *(uint64_t *)(rbx_2 + 48UL);
                    *(uint64_t *)(var_16 + 48UL) = *(uint64_t *)(rbx_2 + 40UL);
                    *(uint64_t *)(var_16 + 56UL) = var_19;
                    var_20 = (unsigned char *)(var_16 + 58UL);
                    *var_20 = (*var_20 & '\xf3');
                    *(uint64_t *)(var_16 + 32UL) = 0UL;
                    *(uint64_t *)(var_16 + 40UL) = 0UL;
                    *(uint64_t *)(var_16 + 64UL) = 18446744073709551615UL;
                    var_21 = (uint64_t *)r12_2;
                    *var_21 = var_17;
                    local_sp_3 = local_sp_2;
                    local_sp_0 = local_sp_2;
                    local_sp_1 = local_sp_2;
                    rbx_4_ph = rbx_2;
                    rbx_3 = rbx_2;
                    if (var_17 == 0UL) {
                        switch_state_var = 1;
                        break;
                    }
                    *var_18 = rbp_2;
                    var_22 = (unsigned char *)(*var_21 + 50UL);
                    *var_22 = (*var_22 | '\x04');
                    var_23 = *(uint64_t *)(rbx_2 + 8UL);
                    var_24 = *var_21;
                    rbx_0 = var_23;
                    rbp_0 = var_24;
                    rbp_3 = var_24;
                    rbp_4_ph = var_24;
                    if (var_23 != 0UL) {
                        r12_0 = var_24 + 8UL;
                        loop_state_var = 1U;
                        continue;
                    }
                    var_25 = *(uint64_t *)(rbx_2 + 16UL);
                    rdx_2 = var_25;
                    if (0UL != var_25) {
                        rbp_5 = rbp_3;
                        rbx_5 = rdx_2;
                        rbp_4_ph = rbp_3;
                        rbx_1 = rdx_2;
                        rbp_1 = rbp_3;
                        rbx_4_ph = rbx_3;
                        if (rdx_2 == 0UL) {
                            var_29 = *var_8;
                            var_30 = (uint64_t)var_29;
                            var_31 = rbp_3 + 16UL;
                            r12_1 = var_31;
                            rdx_0 = var_30;
                            r12_3 = var_31;
                            if ((uint64_t)(var_29 + (-15)) != 0UL) {
                                loop_state_var = 0U;
                                continue;
                            }
                            rax_0 = *var_9;
                            rbp_2 = rbp_1;
                            rbx_2 = rbx_1;
                            r12_2 = r12_1;
                            rcx_0 = (uint64_t)((uint32_t)rdx_0 + 1U);
                            rdx_1 = rdx_0;
                            local_sp_2 = local_sp_1;
                            loop_state_var = 2U;
                            continue;
                        }
                    }
                    rbp_5 = rbp_3;
                    rbx_5 = rdx_2;
                    rbp_4_ph = rbp_3;
                    rbx_1 = rdx_2;
                    rbp_1 = rbp_3;
                    rbx_4_ph = rbx_3;
                    do {
                        rbx_4 = rbx_4_ph;
                        rbp_4 = rbp_4_ph;
                        while (1U)
                            {
                                var_26 = *(uint64_t *)rbx_4;
                                var_27 = *(uint64_t *)rbp_4;
                                rbx_3 = var_26;
                                rbp_3 = var_27;
                                rbx_4 = var_26;
                                rbp_4 = var_27;
                                if (var_26 != 0UL) {
                                    var_28 = *(uint64_t *)(var_26 + 16UL);
                                    rdx_2 = var_28;
                                    if (rbx_4 != var_28) {
                                        continue;
                                    }
                                    break;
                                }
                                return *(uint64_t *)(local_sp_2 + 8UL);
                            }
                        rbp_5 = rbp_3;
                        rbx_5 = rdx_2;
                        rbp_4_ph = rbp_3;
                        rbx_1 = rdx_2;
                        rbp_1 = rbp_3;
                        rbx_4_ph = rbx_3;
                    } while (rdx_2 != 0UL);
                    var_29 = *var_8;
                    var_30 = (uint64_t)var_29;
                    var_31 = rbp_3 + 16UL;
                    r12_1 = var_31;
                    rdx_0 = var_30;
                    r12_3 = var_31;
                    if ((uint64_t)(var_29 + (-15)) != 0UL) {
                        loop_state_var = 0U;
                        continue;
                    }
                    rax_0 = *var_9;
                    rbp_2 = rbp_1;
                    rbx_2 = rbx_1;
                    r12_2 = r12_1;
                    rcx_0 = (uint64_t)((uint32_t)rdx_0 + 1U);
                    rdx_1 = rdx_0;
                    local_sp_2 = local_sp_1;
                    loop_state_var = 2U;
                    continue;
                }
                break;
              case 0U:
                {
                    var_12 = local_sp_3 + (-8L);
                    *(uint64_t *)var_12 = 4248106UL;
                    var_13 = indirect_placeholder_1(968UL);
                    var_14 = var_13.field_0;
                    rax_0 = var_14;
                    rbp_2 = rbp_5;
                    rbx_2 = rbx_5;
                    r12_2 = r12_3;
                    local_sp_2 = var_12;
                    if (var_14 != 0UL) {
                        *(uint64_t *)r12_3 = 0UL;
                        switch_state_var = 1;
                        break;
                    }
                    *(uint64_t *)var_14 = *var_9;
                    *var_9 = var_14;
                }
                break;
              case 1U:
                {
                    var_10 = *var_8;
                    var_11 = (uint64_t)var_10;
                    rbp_5 = rbp_0;
                    local_sp_3 = local_sp_0;
                    rbx_5 = rbx_0;
                    rbx_1 = rbx_0;
                    r12_1 = r12_0;
                    rbp_1 = rbp_0;
                    rdx_0 = var_11;
                    local_sp_1 = local_sp_0;
                    r12_3 = r12_0;
                    if ((uint64_t)(var_10 + (-15)) == 0UL) {
                        rax_0 = *var_9;
                        rbp_2 = rbp_1;
                        rbx_2 = rbx_1;
                        r12_2 = r12_1;
                        rcx_0 = (uint64_t)((uint32_t)rdx_0 + 1U);
                        rdx_1 = rdx_0;
                        local_sp_2 = local_sp_1;
                        loop_state_var = 2U;
                        continue;
                    }
                    var_12 = local_sp_3 + (-8L);
                    *(uint64_t *)var_12 = 4248106UL;
                    var_13 = indirect_placeholder_1(968UL);
                    var_14 = var_13.field_0;
                    rax_0 = var_14;
                    rbp_2 = rbp_5;
                    rbx_2 = rbx_5;
                    r12_2 = r12_3;
                    local_sp_2 = var_12;
                    if (var_14 != 0UL) {
                        *(uint64_t *)r12_3 = 0UL;
                        switch_state_var = 1;
                        break;
                    }
                    *(uint64_t *)var_14 = *var_9;
                    *var_9 = var_14;
                }
                break;
            }
            if (switch_state_var)
                break;
        }
    return 0UL;
}

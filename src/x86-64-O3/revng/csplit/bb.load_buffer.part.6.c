typedef unsigned char __u_char;
typedef unsigned short __u_short;
typedef unsigned int __u_int;
typedef unsigned long __u_long;
typedef signed char __int8_t;
typedef unsigned char __uint8_t;
typedef short __int16_t;
typedef unsigned short __uint16_t;
typedef int __int32_t;
typedef unsigned int __uint32_t;
typedef long __int64_t;
typedef unsigned long __uint64_t;
typedef __int8_t __int_least8_t;
typedef __uint8_t __uint_least8_t;
typedef __int16_t __int_least16_t;
typedef __uint16_t __uint_least16_t;
typedef __int32_t __int_least32_t;
typedef __uint32_t __uint_least32_t;
typedef __int64_t __int_least64_t;
typedef __uint64_t __uint_least64_t;
typedef long __quad_t;
typedef unsigned long __u_quad_t;
typedef long __intmax_t;
typedef unsigned long __uintmax_t;
typedef unsigned long __dev_t;
typedef unsigned int __uid_t;
typedef unsigned int __gid_t;
typedef unsigned long __ino_t;
typedef unsigned long __ino64_t;
typedef unsigned int __mode_t;
typedef unsigned long __nlink_t;
typedef long __off_t;
typedef long __off64_t;
typedef int __pid_t;
typedef struct {
    int __val[2];
} __fsid_t;
typedef long __clock_t;
typedef unsigned long __rlim_t;
typedef unsigned long __rlim64_t;
typedef unsigned int __id_t;
typedef long __time_t;
typedef unsigned int __useconds_t;
typedef long __suseconds_t;
typedef int __daddr_t;
typedef int __key_t;
typedef int __clockid_t;
typedef void *__timer_t;
typedef long __blksize_t;
typedef long __blkcnt_t;
typedef long __blkcnt64_t;
typedef unsigned long __fsblkcnt_t;
typedef unsigned long __fsblkcnt64_t;
typedef unsigned long __fsfilcnt_t;
typedef unsigned long __fsfilcnt64_t;
typedef long __fsword_t;
typedef long __ssize_t;
typedef long __syscall_slong_t;
typedef unsigned long __syscall_ulong_t;
typedef __off64_t __loff_t;
typedef char *__caddr_t;
typedef long __intptr_t;
typedef unsigned int __socklen_t;
typedef int __sig_atomic_t;
typedef __int8_t int8_t;
typedef __int16_t int16_t;
typedef __int32_t int32_t;
typedef __int64_t int64_t;
typedef __uint8_t uint8_t;
typedef __uint16_t uint16_t;
typedef __uint32_t uint32_t;
typedef __uint64_t uint64_t;
typedef __int_least8_t int_least8_t;
typedef __int_least16_t int_least16_t;
typedef __int_least32_t int_least32_t;
typedef __int_least64_t int_least64_t;
typedef __uint_least8_t uint_least8_t;
typedef __uint_least16_t uint_least16_t;
typedef __uint_least32_t uint_least32_t;
typedef __uint_least64_t uint_least64_t;
typedef signed char int_fast8_t;
typedef long int_fast16_t;
typedef long int_fast32_t;
typedef long int_fast64_t;
typedef unsigned char uint_fast8_t;
typedef unsigned long uint_fast16_t;
typedef unsigned long uint_fast32_t;
typedef unsigned long uint_fast64_t;
typedef long intptr_t;
typedef unsigned long uintptr_t;
typedef __intmax_t intmax_t;
typedef __uintmax_t uintmax_t;
struct helper_divq_EAX_wrapper_ret_type;
struct type_5;
struct indirect_placeholder_188_ret_type;
struct indirect_placeholder_189_ret_type;
struct indirect_placeholder_190_ret_type;
struct indirect_placeholder_191_ret_type;
struct indirect_placeholder_187_ret_type;
struct indirect_placeholder_192_ret_type;
struct indirect_placeholder_193_ret_type;
struct indirect_placeholder_195_ret_type;
struct indirect_placeholder_194_ret_type;
struct helper_divq_EAX_wrapper_ret_type {
    uint64_t field_0;
    uint64_t field_1;
    uint32_t field_2;
    uint64_t field_3;
    uint64_t field_4;
    uint64_t field_5;
    uint32_t field_6;
    uint64_t field_7;
    uint32_t field_8;
    uint32_t field_9;
    unsigned char field_10;
    uint32_t field_11;
    uint32_t field_12;
};
struct type_5 {
};
struct indirect_placeholder_188_ret_type {
    uint64_t field_0;
    uint64_t field_1;
};
struct indirect_placeholder_189_ret_type {
    uint64_t field_0;
    uint64_t field_1;
};
struct indirect_placeholder_190_ret_type {
    uint64_t field_0;
    uint64_t field_1;
};
struct indirect_placeholder_191_ret_type {
    uint64_t field_0;
    uint64_t field_1;
};
struct indirect_placeholder_187_ret_type {
    uint64_t field_0;
    uint64_t field_1;
    uint64_t field_2;
    uint64_t field_3;
    uint64_t field_4;
    uint64_t field_5;
    uint64_t field_6;
};
struct indirect_placeholder_192_ret_type {
    uint64_t field_0;
    uint64_t field_1;
    uint64_t field_2;
    uint64_t field_3;
    uint64_t field_4;
    uint64_t field_5;
};
struct indirect_placeholder_193_ret_type {
    uint64_t field_0;
    uint64_t field_1;
    uint64_t field_2;
};
struct indirect_placeholder_195_ret_type {
    uint64_t field_0;
    uint64_t field_1;
};
struct indirect_placeholder_194_ret_type {
    uint64_t field_0;
    uint64_t field_1;
};
extern void abort(void);
extern uint64_t revng_init_local_sp(uint64_t param_0);
extern uint64_t init_rbx(void);
extern uint64_t init_r12(void);
extern uint64_t init_rbp(void);
extern void indirect_placeholder(void);
extern uint64_t init_cc_src2(void);
extern uint64_t helper_cc_compute_c_wrapper(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint32_t param_3);
extern uint64_t init_r13(void);
extern uint64_t init_r14(void);
extern uint64_t init_r15(void);
extern struct helper_divq_EAX_wrapper_ret_type helper_divq_EAX_wrapper(struct type_5 *param_0, uint64_t param_1, uint64_t param_2, uint64_t param_3, uint64_t param_4, uint64_t param_5, uint64_t param_6, uint64_t param_7, uint64_t param_8, uint64_t param_9, uint64_t param_10, uint64_t param_11, uint64_t param_12, uint64_t param_13, uint32_t param_14, uint64_t param_15, uint64_t param_16, uint64_t param_17, uint32_t param_18, uint32_t param_19);
extern uint64_t init_state_0x9018(void);
extern uint32_t init_state_0x9010(void);
extern uint64_t init_state_0x8408(void);
extern uint64_t init_state_0x8328(void);
extern uint64_t init_state_0x82d8(void);
extern uint32_t init_state_0x9080(void);
extern uint32_t init_state_0x8248(void);
extern uint64_t indirect_placeholder_48(uint64_t param_0, uint64_t param_1);
extern void indirect_placeholder_4(uint64_t param_0, uint64_t param_1);
extern uint64_t init_r10(void);
extern uint64_t init_r9(void);
extern uint64_t init_r8(void);
extern struct indirect_placeholder_188_ret_type indirect_placeholder_188(uint64_t param_0);
extern struct indirect_placeholder_189_ret_type indirect_placeholder_189(uint64_t param_0);
extern struct indirect_placeholder_190_ret_type indirect_placeholder_190(uint64_t param_0);
extern struct indirect_placeholder_191_ret_type indirect_placeholder_191(uint64_t param_0);
extern struct indirect_placeholder_187_ret_type indirect_placeholder_187(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint64_t param_3, uint64_t param_4, uint64_t param_5, uint64_t param_6);
extern struct indirect_placeholder_192_ret_type indirect_placeholder_192(uint64_t param_0, uint64_t param_1, uint64_t param_2);
extern struct indirect_placeholder_193_ret_type indirect_placeholder_193(uint64_t param_0, uint64_t param_1, uint64_t param_2);
extern struct indirect_placeholder_195_ret_type indirect_placeholder_195(uint64_t param_0);
extern struct indirect_placeholder_194_ret_type indirect_placeholder_194(uint64_t param_0);
typedef _Bool bool;
uint64_t bb_load_buffer_part_6(void) {
    uint64_t var_0;
    uint64_t var_1;
    uint64_t var_2;
    uint64_t var_3;
    uint64_t var_4;
    uint64_t var_5;
    uint64_t var_6;
    uint64_t var_7;
    uint64_t var_8;
    uint64_t var_9;
    uint64_t var_10;
    uint64_t var_11;
    uint32_t var_12;
    uint64_t var_13;
    uint64_t var_14;
    uint64_t var_15;
    uint32_t var_16;
    uint32_t var_17;
    uint64_t var_18;
    uint64_t var_19;
    uint64_t var_20;
    uint64_t var_21;
    uint64_t r13_2;
    uint64_t r10_2;
    uint64_t r15_1;
    uint64_t var_60;
    uint64_t var_61;
    uint64_t *_cast;
    uint64_t var_62;
    uint64_t *_pre_phi213;
    uint64_t var_63;
    struct indirect_placeholder_188_ret_type var_64;
    uint64_t var_65;
    uint64_t var_66;
    uint64_t var_67;
    uint64_t *var_68;
    uint64_t var_69;
    struct indirect_placeholder_189_ret_type var_70;
    uint64_t var_71;
    uint64_t *var_72;
    uint64_t local_sp_4;
    uint64_t var_81;
    uint64_t var_82;
    uint64_t var_83;
    uint64_t var_84;
    uint64_t var_85;
    uint64_t rbx_1;
    uint64_t rdi_1;
    uint64_t rsi_0;
    uint64_t var_86;
    uint64_t *_cast1;
    uint64_t var_87;
    uint64_t *_pre_phi;
    uint64_t rbx_0;
    uint64_t rdi_0;
    uint64_t rax_0;
    uint64_t local_sp_0;
    uint64_t *var_99;
    uint64_t var_100;
    uint64_t var_101;
    uint64_t var_102;
    uint64_t var_103;
    uint64_t r12_0;
    uint64_t var_88;
    struct indirect_placeholder_190_ret_type var_89;
    uint64_t var_90;
    uint64_t var_91;
    uint64_t var_92;
    uint64_t *var_93;
    uint64_t var_94;
    struct indirect_placeholder_191_ret_type var_95;
    uint64_t var_96;
    uint64_t var_97;
    uint64_t *var_34;
    uint64_t *var_98;
    uint64_t var_104;
    uint64_t var_105;
    uint64_t var_106;
    uint64_t rax_1;
    uint64_t local_sp_3;
    uint64_t local_sp_1;
    uint64_t var_107;
    uint64_t var_108;
    uint64_t rbx_4;
    uint64_t var_109;
    uint64_t var_110;
    uint64_t rdx_0;
    uint64_t *var_111;
    uint64_t var_112;
    uint64_t rbx_2;
    uint64_t rcx_0;
    uint64_t rdx_1;
    uint64_t local_sp_2;
    uint64_t *var_73;
    uint64_t var_74;
    uint64_t var_75;
    uint64_t var_76;
    uint64_t var_77;
    uint64_t var_78;
    uint64_t var_79;
    uint64_t var_80;
    uint64_t r13_0;
    uint64_t var_54;
    uint64_t var_53;
    uint64_t r8_3;
    uint64_t state_0x9018_2;
    uint64_t local_sp_10;
    uint64_t state_0x82d8_2;
    uint64_t rbx_3;
    uint64_t rdi_2;
    uint64_t r15_0;
    uint64_t rsi_1;
    uint64_t local_sp_12;
    uint64_t state_0x82d8_1;
    uint32_t state_0x9080_1;
    uint32_t state_0x8248_1;
    uint64_t var_55;
    uint64_t var_56;
    uint64_t *var_57;
    struct indirect_placeholder_192_ret_type var_58;
    uint64_t var_59;
    uint64_t r9_3;
    uint64_t r10_0;
    uint64_t rdi_3;
    uint64_t r9_0;
    uint64_t rsi_2;
    uint64_t r8_0;
    uint64_t local_sp_5;
    uint32_t state_0x9010_2;
    uint64_t var_116;
    uint64_t local_sp_13;
    uint32_t state_0x9080_2;
    uint32_t state_0x8248_2;
    uint64_t var_118;
    uint64_t var_119;
    uint32_t var_120;
    uint64_t var_121;
    struct helper_divq_EAX_wrapper_ret_type var_117;
    uint32_t var_122;
    uint32_t var_123;
    uint64_t var_124;
    uint64_t var_125;
    uint64_t local_sp_7;
    uint64_t rdi_4;
    uint64_t local_sp_6;
    uint64_t var_126;
    uint64_t var_127;
    uint64_t var_128;
    uint64_t var_113;
    uint64_t local_sp_9;
    uint64_t rdi_5;
    uint64_t local_sp_8;
    uint64_t var_114;
    uint64_t var_115;
    uint64_t rbx_6;
    uint64_t r13_3;
    uint64_t r13_1;
    uint64_t rbx_5;
    uint64_t var_48;
    struct indirect_placeholder_193_ret_type var_49;
    uint64_t var_50;
    uint64_t var_51;
    uint64_t var_52;
    uint64_t var_42;
    uint64_t var_43;
    uint64_t var_44;
    uint64_t var_45;
    uint64_t var_46;
    uint64_t var_47;
    uint64_t r10_1;
    uint64_t r9_1;
    uint64_t r8_1;
    uint64_t state_0x9018_0;
    uint32_t state_0x9010_0;
    uint64_t local_sp_11;
    uint64_t state_0x82d8_0;
    uint32_t state_0x9080_0;
    uint32_t state_0x8248_0;
    uint64_t var_22;
    uint64_t var_23;
    uint64_t var_24;
    uint64_t r9_2;
    uint64_t r8_2;
    uint64_t state_0x9018_1;
    uint32_t state_0x9010_1;
    uint64_t r10_3;
    struct indirect_placeholder_195_ret_type var_25;
    uint64_t var_26;
    uint64_t var_27;
    struct indirect_placeholder_194_ret_type var_28;
    uint64_t var_29;
    uint64_t *var_30;
    uint64_t var_31;
    uint64_t var_32;
    uint64_t *var_33;
    uint64_t *var_35;
    uint64_t var_36;
    uint64_t *var_37;
    uint64_t *var_38;
    uint64_t *var_39;
    uint64_t *var_40;
    uint64_t *var_41;
    unsigned int loop_state_var;
    bool switch_state_var;
    var_0 = revng_init_local_sp(0UL);
    var_1 = init_r15();
    var_2 = init_r13();
    var_3 = init_rbx();
    var_4 = init_r10();
    var_5 = init_r12();
    var_6 = init_r9();
    var_7 = init_rbp();
    var_8 = init_cc_src2();
    var_9 = init_r14();
    var_10 = init_r8();
    var_11 = init_state_0x9018();
    var_12 = init_state_0x9010();
    var_13 = init_state_0x8408();
    var_14 = init_state_0x8328();
    var_15 = init_state_0x82d8();
    var_16 = init_state_0x9080();
    var_17 = init_state_0x8248();
    *(uint64_t *)(var_0 + (-8L)) = var_1;
    *(uint64_t *)(var_0 + (-16L)) = var_9;
    *(uint64_t *)(var_0 + (-24L)) = var_2;
    *(uint64_t *)(var_0 + (-32L)) = var_5;
    *(uint64_t *)(var_0 + (-40L)) = var_7;
    *(uint64_t *)(var_0 + (-48L)) = var_3;
    var_18 = var_0 + (-72L);
    var_19 = *(uint64_t *)6470256UL;
    var_20 = helper_cc_compute_c_wrapper(var_19 + (-8191L), 8191UL, var_8, 17U);
    var_21 = (var_20 == 0UL) ? var_19 : 8191UL;
    r13_2 = var_21;
    r10_2 = var_4;
    rax_0 = 0UL;
    rax_1 = 0UL;
    rcx_0 = 0UL;
    r13_0 = 0UL;
    local_sp_12 = var_18;
    state_0x82d8_1 = var_15;
    state_0x9080_1 = var_16;
    state_0x8248_1 = var_17;
    rbx_6 = 8192UL;
    r13_3 = 8191UL;
    r10_1 = var_4;
    r9_1 = var_6;
    r8_1 = var_10;
    state_0x9018_0 = var_11;
    state_0x9010_0 = var_12;
    local_sp_11 = var_18;
    state_0x82d8_0 = var_15;
    state_0x9080_0 = var_16;
    state_0x8248_0 = var_17;
    r9_2 = var_6;
    r8_2 = var_10;
    state_0x9018_1 = var_11;
    state_0x9010_1 = var_12;
    if (var_21 > 8191UL) {
        while (1U)
            {
                var_22 = (r13_2 + (-6144L)) & (-2048L);
                var_23 = var_22 + 8191UL;
                var_24 = var_22 + 8192UL;
                r8_3 = r8_1;
                state_0x9018_2 = state_0x9018_0;
                state_0x82d8_2 = state_0x82d8_0;
                r9_3 = r9_1;
                state_0x9010_2 = state_0x9010_0;
                local_sp_13 = local_sp_11;
                state_0x9080_2 = state_0x9080_0;
                state_0x8248_2 = state_0x8248_0;
                rbx_6 = var_24;
                r13_3 = var_23;
                r10_3 = r10_1;
                while (1U)
                    {
                        *(uint64_t *)(local_sp_13 + (-8L)) = 4208862UL;
                        var_25 = indirect_placeholder_195(72UL);
                        var_26 = var_25.field_0;
                        var_27 = local_sp_13 + (-16L);
                        *(uint64_t *)var_27 = 4208873UL;
                        var_28 = indirect_placeholder_194(rbx_6);
                        var_29 = var_28.field_0;
                        var_30 = (uint64_t *)(var_26 + 40UL);
                        *var_30 = var_29;
                        var_31 = *(uint64_t *)6470248UL;
                        var_32 = *(uint64_t *)6470256UL;
                        var_33 = (uint64_t *)var_26;
                        *var_33 = r13_3;
                        var_34 = (uint64_t *)(var_26 + 56UL);
                        *var_34 = 0UL;
                        var_35 = (uint64_t *)(var_26 + 48UL);
                        *var_35 = 0UL;
                        var_36 = var_31 + 1UL;
                        var_37 = (uint64_t *)(var_26 + 32UL);
                        *var_37 = 0UL;
                        var_38 = (uint64_t *)(var_26 + 8UL);
                        *var_38 = 0UL;
                        var_39 = (uint64_t *)(var_26 + 64UL);
                        *var_39 = 0UL;
                        var_40 = (uint64_t *)(var_26 + 24UL);
                        *var_40 = var_36;
                        var_41 = (uint64_t *)(var_26 + 16UL);
                        *var_41 = var_36;
                        local_sp_10 = var_27;
                        rdi_2 = var_29;
                        r10_0 = r10_3;
                        r9_0 = r9_3;
                        r8_0 = r8_3;
                        r13_1 = r13_3;
                        rbx_5 = var_29;
                        if (var_32 != 0UL) {
                            *(uint64_t *)(local_sp_10 + (-16L)) = 4210046UL;
                            indirect_placeholder();
                            var_53 = (uint64_t)*(uint32_t *)18446744073709551615UL;
                            *(uint64_t *)(local_sp_10 + (-24L)) = 4210062UL;
                            indirect_placeholder_187(0UL, 0UL, r9_3, var_36, 4348112UL, var_53, r8_3);
                            *(uint64_t *)(local_sp_10 + (-32L)) = 4210067UL;
                            indirect_placeholder_4(rbx_5, var_26);
                            abort();
                        }
                        var_42 = *(uint64_t *)6470264UL;
                        var_43 = local_sp_13 + (-24L);
                        *(uint64_t *)var_43 = 4209143UL;
                        indirect_placeholder();
                        var_44 = *(uint64_t *)6470256UL;
                        *(uint64_t *)6470256UL = 0UL;
                        var_45 = var_44 + *var_38;
                        var_46 = var_29 + var_44;
                        var_47 = r13_3 - var_44;
                        *var_38 = var_45;
                        local_sp_3 = var_43;
                        local_sp_10 = var_43;
                        rbx_3 = var_46;
                        r15_0 = var_45;
                        rsi_1 = var_42;
                        r13_1 = var_47;
                        rbx_5 = var_46;
                        if (var_47 != 0UL) {
                            *(uint64_t *)(local_sp_10 + (-16L)) = 4210046UL;
                            indirect_placeholder();
                            var_53 = (uint64_t)*(uint32_t *)18446744073709551615UL;
                            *(uint64_t *)(local_sp_10 + (-24L)) = 4210062UL;
                            indirect_placeholder_187(0UL, 0UL, r9_3, var_36, 4348112UL, var_53, r8_3);
                            *(uint64_t *)(local_sp_10 + (-32L)) = 4210067UL;
                            indirect_placeholder_4(rbx_5, var_26);
                            abort();
                        }
                    }
                switch_state_var = 0;
                switch (loop_state_var) {
                  case 3U:
                    {
                        continue;
                    }
                    break;
                  case 0U:
                    {
                        loop_state_var = 0U;
                        switch_state_var = 1;
                        break;
                    }
                    break;
                  case 1U:
                    {
                        loop_state_var = 1U;
                        switch_state_var = 1;
                        break;
                    }
                    break;
                  case 2U:
                    {
                        loop_state_var = 2U;
                        switch_state_var = 1;
                        break;
                    }
                    break;
                }
                if (switch_state_var)
                    break;
            }
        switch (loop_state_var) {
          case 2U:
          case 0U:
            {
                switch (loop_state_var) {
                  case 0U:
                    {
                        *var_35 = 0UL;
                        *(uint64_t *)(local_sp_9 + (-8L)) = 4209941UL;
                        indirect_placeholder();
                        *var_30 = 0UL;
                        *(uint64_t *)(local_sp_9 + (-16L)) = 4209957UL;
                        indirect_placeholder();
                        return 0UL;
                    }
                    break;
                  case 2U:
                    {
                        var_114 = *(uint64_t *)(rdi_5 + 1304UL);
                        var_115 = local_sp_8 + (-8L);
                        *(uint64_t *)var_115 = 4209916UL;
                        indirect_placeholder();
                        rdi_5 = var_114;
                        local_sp_8 = var_115;
                        local_sp_9 = var_115;
                        do {
                            var_114 = *(uint64_t *)(rdi_5 + 1304UL);
                            var_115 = local_sp_8 + (-8L);
                            *(uint64_t *)var_115 = 4209916UL;
                            indirect_placeholder();
                            rdi_5 = var_114;
                            local_sp_8 = var_115;
                            local_sp_9 = var_115;
                        } while (var_114 != 0UL);
                    }
                    break;
                }
            }
            break;
          case 1U:
            {
                var_111 = (uint64_t *)(rdx_0 + 64UL);
                var_112 = *var_111;
                rdx_0 = var_112;
                do {
                    var_111 = (uint64_t *)(rdx_0 + 64UL);
                    var_112 = *var_111;
                    rdx_0 = var_112;
                } while (var_112 != 0UL);
                *var_111 = var_26;
                return 1UL;
            }
            break;
        }
    } else {
        r8_3 = r8_2;
        state_0x9018_2 = state_0x9018_1;
        state_0x82d8_2 = state_0x82d8_1;
        r9_3 = r9_2;
        state_0x9010_2 = state_0x9010_1;
        local_sp_13 = local_sp_12;
        state_0x9080_2 = state_0x9080_1;
        state_0x8248_2 = state_0x8248_1;
        r10_3 = r10_2;
        while (1U)
            {
                *(uint64_t *)(local_sp_13 + (-8L)) = 4208862UL;
                var_25 = indirect_placeholder_195(72UL);
                var_26 = var_25.field_0;
                var_27 = local_sp_13 + (-16L);
                *(uint64_t *)var_27 = 4208873UL;
                var_28 = indirect_placeholder_194(rbx_6);
                var_29 = var_28.field_0;
                var_30 = (uint64_t *)(var_26 + 40UL);
                *var_30 = var_29;
                var_31 = *(uint64_t *)6470248UL;
                var_32 = *(uint64_t *)6470256UL;
                var_33 = (uint64_t *)var_26;
                *var_33 = r13_3;
                var_34 = (uint64_t *)(var_26 + 56UL);
                *var_34 = 0UL;
                var_35 = (uint64_t *)(var_26 + 48UL);
                *var_35 = 0UL;
                var_36 = var_31 + 1UL;
                var_37 = (uint64_t *)(var_26 + 32UL);
                *var_37 = 0UL;
                var_38 = (uint64_t *)(var_26 + 8UL);
                *var_38 = 0UL;
                var_39 = (uint64_t *)(var_26 + 64UL);
                *var_39 = 0UL;
                var_40 = (uint64_t *)(var_26 + 24UL);
                *var_40 = var_36;
                var_41 = (uint64_t *)(var_26 + 16UL);
                *var_41 = var_36;
                local_sp_10 = var_27;
                rdi_2 = var_29;
                r10_0 = r10_3;
                r9_0 = r9_3;
                r8_0 = r8_3;
                r13_1 = r13_3;
                rbx_5 = var_29;
                if (var_32 != 0UL) {
                    *(uint64_t *)(local_sp_10 + (-16L)) = 4210046UL;
                    indirect_placeholder();
                    var_53 = (uint64_t)*(uint32_t *)18446744073709551615UL;
                    *(uint64_t *)(local_sp_10 + (-24L)) = 4210062UL;
                    indirect_placeholder_187(0UL, 0UL, r9_3, var_36, 4348112UL, var_53, r8_3);
                    *(uint64_t *)(local_sp_10 + (-32L)) = 4210067UL;
                    indirect_placeholder_4(rbx_5, var_26);
                    abort();
                }
                var_42 = *(uint64_t *)6470264UL;
                var_43 = local_sp_13 + (-24L);
                *(uint64_t *)var_43 = 4209143UL;
                indirect_placeholder();
                var_44 = *(uint64_t *)6470256UL;
                *(uint64_t *)6470256UL = 0UL;
                var_45 = var_44 + *var_38;
                var_46 = var_29 + var_44;
                var_47 = r13_3 - var_44;
                *var_38 = var_45;
                local_sp_3 = var_43;
                local_sp_10 = var_43;
                rbx_3 = var_46;
                r15_0 = var_45;
                rsi_1 = var_42;
                r13_1 = var_47;
                rbx_5 = var_46;
                if (var_47 != 0UL) {
                    *(uint64_t *)(local_sp_10 + (-16L)) = 4210046UL;
                    indirect_placeholder();
                    var_53 = (uint64_t)*(uint32_t *)18446744073709551615UL;
                    *(uint64_t *)(local_sp_10 + (-24L)) = 4210062UL;
                    indirect_placeholder_187(0UL, 0UL, r9_3, var_36, 4348112UL, var_53, r8_3);
                    *(uint64_t *)(local_sp_10 + (-32L)) = 4210067UL;
                    indirect_placeholder_4(rbx_5, var_26);
                    abort();
                }
            }
        switch (loop_state_var) {
          case 3U:
            {
                while (1U)
                    {
                        var_22 = (r13_2 + (-6144L)) & (-2048L);
                        var_23 = var_22 + 8191UL;
                        var_24 = var_22 + 8192UL;
                        r8_3 = r8_1;
                        state_0x9018_2 = state_0x9018_0;
                        state_0x82d8_2 = state_0x82d8_0;
                        r9_3 = r9_1;
                        state_0x9010_2 = state_0x9010_0;
                        local_sp_13 = local_sp_11;
                        state_0x9080_2 = state_0x9080_0;
                        state_0x8248_2 = state_0x8248_0;
                        rbx_6 = var_24;
                        r13_3 = var_23;
                        r10_3 = r10_1;
                        while (1U)
                            {
                                *(uint64_t *)(local_sp_13 + (-8L)) = 4208862UL;
                                var_25 = indirect_placeholder_195(72UL);
                                var_26 = var_25.field_0;
                                var_27 = local_sp_13 + (-16L);
                                *(uint64_t *)var_27 = 4208873UL;
                                var_28 = indirect_placeholder_194(rbx_6);
                                var_29 = var_28.field_0;
                                var_30 = (uint64_t *)(var_26 + 40UL);
                                *var_30 = var_29;
                                var_31 = *(uint64_t *)6470248UL;
                                var_32 = *(uint64_t *)6470256UL;
                                var_33 = (uint64_t *)var_26;
                                *var_33 = r13_3;
                                var_34 = (uint64_t *)(var_26 + 56UL);
                                *var_34 = 0UL;
                                var_35 = (uint64_t *)(var_26 + 48UL);
                                *var_35 = 0UL;
                                var_36 = var_31 + 1UL;
                                var_37 = (uint64_t *)(var_26 + 32UL);
                                *var_37 = 0UL;
                                var_38 = (uint64_t *)(var_26 + 8UL);
                                *var_38 = 0UL;
                                var_39 = (uint64_t *)(var_26 + 64UL);
                                *var_39 = 0UL;
                                var_40 = (uint64_t *)(var_26 + 24UL);
                                *var_40 = var_36;
                                var_41 = (uint64_t *)(var_26 + 16UL);
                                *var_41 = var_36;
                                local_sp_10 = var_27;
                                rdi_2 = var_29;
                                r10_0 = r10_3;
                                r9_0 = r9_3;
                                r8_0 = r8_3;
                                r13_1 = r13_3;
                                rbx_5 = var_29;
                                if (var_32 != 0UL) {
                                    *(uint64_t *)(local_sp_10 + (-16L)) = 4210046UL;
                                    indirect_placeholder();
                                    var_53 = (uint64_t)*(uint32_t *)18446744073709551615UL;
                                    *(uint64_t *)(local_sp_10 + (-24L)) = 4210062UL;
                                    indirect_placeholder_187(0UL, 0UL, r9_3, var_36, 4348112UL, var_53, r8_3);
                                    *(uint64_t *)(local_sp_10 + (-32L)) = 4210067UL;
                                    indirect_placeholder_4(rbx_5, var_26);
                                    abort();
                                }
                                var_42 = *(uint64_t *)6470264UL;
                                var_43 = local_sp_13 + (-24L);
                                *(uint64_t *)var_43 = 4209143UL;
                                indirect_placeholder();
                                var_44 = *(uint64_t *)6470256UL;
                                *(uint64_t *)6470256UL = 0UL;
                                var_45 = var_44 + *var_38;
                                var_46 = var_29 + var_44;
                                var_47 = r13_3 - var_44;
                                *var_38 = var_45;
                                local_sp_3 = var_43;
                                local_sp_10 = var_43;
                                rbx_3 = var_46;
                                r15_0 = var_45;
                                rsi_1 = var_42;
                                r13_1 = var_47;
                                rbx_5 = var_46;
                                if (var_47 != 0UL) {
                                    *(uint64_t *)(local_sp_10 + (-16L)) = 4210046UL;
                                    indirect_placeholder();
                                    var_53 = (uint64_t)*(uint32_t *)18446744073709551615UL;
                                    *(uint64_t *)(local_sp_10 + (-24L)) = 4210062UL;
                                    indirect_placeholder_187(0UL, 0UL, r9_3, var_36, 4348112UL, var_53, r8_3);
                                    *(uint64_t *)(local_sp_10 + (-32L)) = 4210067UL;
                                    indirect_placeholder_4(rbx_5, var_26);
                                    abort();
                                }
                            }
                        switch_state_var = 0;
                        switch (loop_state_var) {
                          case 3U:
                            {
                                continue;
                            }
                            break;
                          case 0U:
                            {
                                loop_state_var = 0U;
                                switch_state_var = 1;
                                break;
                            }
                            break;
                          case 1U:
                            {
                                loop_state_var = 1U;
                                switch_state_var = 1;
                                break;
                            }
                            break;
                          case 2U:
                            {
                                loop_state_var = 2U;
                                switch_state_var = 1;
                                break;
                            }
                            break;
                        }
                        if (switch_state_var)
                            break;
                    }
                switch (loop_state_var) {
                  case 2U:
                  case 0U:
                    {
                        switch (loop_state_var) {
                          case 2U:
                            {
                                var_114 = *(uint64_t *)(rdi_5 + 1304UL);
                                var_115 = local_sp_8 + (-8L);
                                *(uint64_t *)var_115 = 4209916UL;
                                indirect_placeholder();
                                rdi_5 = var_114;
                                local_sp_8 = var_115;
                                local_sp_9 = var_115;
                                do {
                                    var_114 = *(uint64_t *)(rdi_5 + 1304UL);
                                    var_115 = local_sp_8 + (-8L);
                                    *(uint64_t *)var_115 = 4209916UL;
                                    indirect_placeholder();
                                    rdi_5 = var_114;
                                    local_sp_8 = var_115;
                                    local_sp_9 = var_115;
                                } while (var_114 != 0UL);
                            }
                            break;
                          case 0U:
                            {
                                *var_35 = 0UL;
                                *(uint64_t *)(local_sp_9 + (-8L)) = 4209941UL;
                                indirect_placeholder();
                                *var_30 = 0UL;
                                *(uint64_t *)(local_sp_9 + (-16L)) = 4209957UL;
                                indirect_placeholder();
                                return 0UL;
                            }
                            break;
                        }
                    }
                    break;
                  case 1U:
                    {
                        var_111 = (uint64_t *)(rdx_0 + 64UL);
                        var_112 = *var_111;
                        rdx_0 = var_112;
                        do {
                            var_111 = (uint64_t *)(rdx_0 + 64UL);
                            var_112 = *var_111;
                            rdx_0 = var_112;
                        } while (var_112 != 0UL);
                        *var_111 = var_26;
                        return 1UL;
                    }
                    break;
                }
            }
            break;
          case 1U:
            {
                var_111 = (uint64_t *)(rdx_0 + 64UL);
                var_112 = *var_111;
                rdx_0 = var_112;
                do {
                    var_111 = (uint64_t *)(rdx_0 + 64UL);
                    var_112 = *var_111;
                    rdx_0 = var_112;
                } while (var_112 != 0UL);
                *var_111 = var_26;
                return 1UL;
            }
            break;
          case 2U:
          case 0U:
            {
                switch (loop_state_var) {
                  case 2U:
                    {
                        var_114 = *(uint64_t *)(rdi_5 + 1304UL);
                        var_115 = local_sp_8 + (-8L);
                        *(uint64_t *)var_115 = 4209916UL;
                        indirect_placeholder();
                        rdi_5 = var_114;
                        local_sp_8 = var_115;
                        local_sp_9 = var_115;
                        do {
                            var_114 = *(uint64_t *)(rdi_5 + 1304UL);
                            var_115 = local_sp_8 + (-8L);
                            *(uint64_t *)var_115 = 4209916UL;
                            indirect_placeholder();
                            rdi_5 = var_114;
                            local_sp_8 = var_115;
                            local_sp_9 = var_115;
                        } while (var_114 != 0UL);
                    }
                    break;
                  case 0U:
                    {
                        *var_35 = 0UL;
                        *(uint64_t *)(local_sp_9 + (-8L)) = 4209941UL;
                        indirect_placeholder();
                        *var_30 = 0UL;
                        *(uint64_t *)(local_sp_9 + (-16L)) = 4209957UL;
                        indirect_placeholder();
                        return 0UL;
                    }
                    break;
                }
            }
            break;
        }
    }
}

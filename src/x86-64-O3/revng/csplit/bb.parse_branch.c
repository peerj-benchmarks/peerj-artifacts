typedef unsigned char __u_char;
typedef unsigned short __u_short;
typedef unsigned int __u_int;
typedef unsigned long __u_long;
typedef signed char __int8_t;
typedef unsigned char __uint8_t;
typedef short __int16_t;
typedef unsigned short __uint16_t;
typedef int __int32_t;
typedef unsigned int __uint32_t;
typedef long __int64_t;
typedef unsigned long __uint64_t;
typedef __int8_t __int_least8_t;
typedef __uint8_t __uint_least8_t;
typedef __int16_t __int_least16_t;
typedef __uint16_t __uint_least16_t;
typedef __int32_t __int_least32_t;
typedef __uint32_t __uint_least32_t;
typedef __int64_t __int_least64_t;
typedef __uint64_t __uint_least64_t;
typedef long __quad_t;
typedef unsigned long __u_quad_t;
typedef long __intmax_t;
typedef unsigned long __uintmax_t;
typedef unsigned long __dev_t;
typedef unsigned int __uid_t;
typedef unsigned int __gid_t;
typedef unsigned long __ino_t;
typedef unsigned long __ino64_t;
typedef unsigned int __mode_t;
typedef unsigned long __nlink_t;
typedef long __off_t;
typedef long __off64_t;
typedef int __pid_t;
typedef struct {
    int __val[2];
} __fsid_t;
typedef long __clock_t;
typedef unsigned long __rlim_t;
typedef unsigned long __rlim64_t;
typedef unsigned int __id_t;
typedef long __time_t;
typedef unsigned int __useconds_t;
typedef long __suseconds_t;
typedef int __daddr_t;
typedef int __key_t;
typedef int __clockid_t;
typedef void *__timer_t;
typedef long __blksize_t;
typedef long __blkcnt_t;
typedef long __blkcnt64_t;
typedef unsigned long __fsblkcnt_t;
typedef unsigned long __fsblkcnt64_t;
typedef unsigned long __fsfilcnt_t;
typedef unsigned long __fsfilcnt64_t;
typedef long __fsword_t;
typedef long __ssize_t;
typedef long __syscall_slong_t;
typedef unsigned long __syscall_ulong_t;
typedef __off64_t __loff_t;
typedef char *__caddr_t;
typedef long __intptr_t;
typedef unsigned int __socklen_t;
typedef int __sig_atomic_t;
typedef __int8_t int8_t;
typedef __int16_t int16_t;
typedef __int32_t int32_t;
typedef __int64_t int64_t;
typedef __uint8_t uint8_t;
typedef __uint16_t uint16_t;
typedef __uint32_t uint32_t;
typedef __uint64_t uint64_t;
typedef __int_least8_t int_least8_t;
typedef __int_least16_t int_least16_t;
typedef __int_least32_t int_least32_t;
typedef __int_least64_t int_least64_t;
typedef __uint_least8_t uint_least8_t;
typedef __uint_least16_t uint_least16_t;
typedef __uint_least32_t uint_least32_t;
typedef __uint_least64_t uint_least64_t;
typedef signed char int_fast8_t;
typedef long int_fast16_t;
typedef long int_fast32_t;
typedef long int_fast64_t;
typedef unsigned char uint_fast8_t;
typedef unsigned long uint_fast16_t;
typedef unsigned long uint_fast32_t;
typedef unsigned long uint_fast64_t;
typedef long intptr_t;
typedef unsigned long uintptr_t;
typedef __intmax_t intmax_t;
typedef __uintmax_t uintmax_t;
typedef _Bool bool;
struct indirect_placeholder_94_ret_type;
struct indirect_placeholder_94_ret_type {
    uint64_t field_0;
    uint64_t field_1;
};
extern uint64_t revng_init_local_sp(uint64_t param_0);
extern uint64_t init_rbx(void);
extern uint64_t init_r12(void);
extern uint64_t init_rbp(void);
extern void indirect_placeholder(void);
extern uint64_t init_r13(void);
extern uint64_t init_r14(void);
extern uint64_t init_r15(void);
extern uint64_t indirect_placeholder_91(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint64_t param_3, uint64_t param_4, uint64_t param_5);
extern struct indirect_placeholder_94_ret_type indirect_placeholder_94(uint64_t param_0);
uint64_t bb_parse_branch(uint64_t rdi, uint64_t r9, uint64_t rcx, uint64_t rdx, uint64_t rsi, uint64_t r8) {
    unsigned char var_53;
    uint64_t var_54;
    uint64_t var_0;
    uint64_t var_1;
    uint64_t var_2;
    uint64_t var_3;
    uint64_t var_4;
    uint64_t var_5;
    uint64_t var_6;
    uint64_t var_7;
    uint64_t var_8;
    uint64_t var_9;
    uint32_t *_pre_phi;
    uint64_t r14_1;
    uint64_t local_sp_4;
    uint64_t rax_0;
    uint64_t local_sp_3;
    uint64_t local_sp_0;
    uint64_t r14_0;
    uint64_t var_46;
    uint64_t var_47;
    uint64_t local_sp_1;
    uint64_t local_sp_12;
    uint64_t local_sp_5;
    uint64_t rdx4_1;
    uint32_t var_48;
    uint64_t local_sp_2;
    uint64_t var_51;
    uint64_t var_52;
    uint64_t var_49;
    uint64_t var_50;
    uint32_t var_22;
    uint64_t rax_4;
    uint64_t rax_1;
    uint64_t rcx3_0;
    uint64_t rdx4_0;
    uint32_t rsi5_0;
    uint64_t var_27;
    uint64_t var_28;
    uint64_t var_29;
    unsigned char *var_30;
    uint64_t local_sp_5_ph;
    uint64_t local_sp_13;
    uint64_t rdx4_1_ph;
    uint64_t rax_3;
    uint64_t var_31;
    uint64_t var_32;
    uint64_t rdx4_3;
    uint64_t local_sp_6;
    uint64_t rdx4_2;
    uint32_t var_33;
    uint64_t local_sp_7;
    uint64_t var_37;
    uint64_t rax_2;
    uint64_t var_38;
    uint64_t local_sp_11;
    uint64_t local_sp_8;
    uint64_t r14_2;
    uint64_t var_39;
    uint64_t var_40;
    uint64_t local_sp_9;
    uint64_t r14_3;
    uint32_t var_41;
    uint64_t local_sp_10;
    uint64_t var_44;
    uint64_t var_45;
    uint64_t var_42;
    uint64_t var_43;
    uint64_t var_34;
    uint64_t *var_35;
    uint64_t var_36;
    uint64_t var_23;
    struct indirect_placeholder_94_ret_type var_24;
    uint64_t var_25;
    uint64_t var_26;
    uint64_t var_55;
    uint64_t local_sp_14;
    uint32_t *var_10;
    uint64_t merge;
    unsigned char *var_11;
    bool var_12;
    uint32_t *var_13;
    uint64_t *var_14;
    uint64_t r14_4;
    unsigned char var_15;
    uint64_t r14_5;
    uint64_t *var_16;
    uint64_t var_17;
    uint64_t *var_18;
    uint64_t var_19;
    uint64_t var_20;
    uint64_t var_21;
    unsigned int loop_state_var;
    bool switch_state_var;
    var_0 = revng_init_local_sp(0UL);
    var_1 = init_r15();
    var_2 = init_r13();
    var_3 = init_rbx();
    var_4 = init_r12();
    var_5 = init_rbp();
    var_6 = init_r14();
    *(uint64_t *)(var_0 + (-8L)) = var_1;
    *(uint64_t *)(var_0 + (-16L)) = var_6;
    *(uint64_t *)(var_0 + (-24L)) = var_2;
    *(uint64_t *)(var_0 + (-32L)) = var_4;
    *(uint64_t *)(var_0 + (-40L)) = var_5;
    *(uint64_t *)(var_0 + (-48L)) = var_3;
    var_7 = *(uint64_t *)rsi;
    *(uint64_t *)(var_0 + (-80L)) = rdi;
    *(uint64_t *)(var_0 + (-72L)) = rcx;
    var_8 = var_0 + (-96L);
    *(uint64_t *)var_8 = 4297068UL;
    var_9 = indirect_placeholder_91(rdi, r9, rcx, rdx, rsi, r8);
    rcx3_0 = 0UL;
    rsi5_0 = 1U;
    local_sp_13 = var_8;
    merge = 0UL;
    r14_4 = var_9;
    if (var_9 == 0UL) {
        var_10 = (uint32_t *)r9;
        _pre_phi = var_10;
        if (*var_10 == 0U) {
            return merge;
        }
    }
    _pre_phi = (uint32_t *)r9;
    var_11 = (unsigned char *)(rdx + 8UL);
    var_12 = (r8 == 0UL);
    var_13 = (uint32_t *)(var_7 + 128UL);
    var_14 = (uint64_t *)(var_7 + 112UL);
    while (1U)
        {
            var_15 = *var_11;
            merge = r14_4;
            local_sp_14 = local_sp_13;
            rax_4 = (uint64_t)var_15;
            r14_5 = r14_4;
            if ((uint64_t)((var_15 & '\xf7') + '\xfe') != 0UL) {
                loop_state_var = 2U;
                break;
            }
            while (1U)
                {
                    r14_0 = r14_5;
                    r14_2 = r14_5;
                    merge = 0UL;
                    if (!var_12) {
                        merge = r14_5;
                        if ((uint64_t)((unsigned char)rax_4 + '\xf7') != 0UL) {
                            loop_state_var = 2U;
                            break;
                        }
                    }
                    var_16 = (uint64_t *)(local_sp_14 + 16UL);
                    var_17 = *var_16;
                    var_18 = (uint64_t *)(local_sp_14 + 8UL);
                    var_19 = *var_18;
                    var_20 = local_sp_14 + (-8L);
                    *(uint64_t *)var_20 = 4297138UL;
                    var_21 = indirect_placeholder_91(var_19, r9, var_17, rdx, rsi, r8);
                    local_sp_0 = var_20;
                    local_sp_4 = var_20;
                    rdx4_0 = var_21;
                    local_sp_14 = var_20;
                    if (var_21 != 0UL) {
                        if (r14_5 != 0UL) {
                            loop_state_var = 0U;
                            break;
                        }
                        var_53 = *var_11;
                        var_54 = (uint64_t)var_53;
                        var_55 = (r14_5 == 0UL) ? var_21 : r14_5;
                        merge = var_55;
                        rax_4 = var_54;
                        r14_5 = var_55;
                        if ((uint64_t)((var_53 & '\xf7') + '\xfe') == 0UL) {
                            continue;
                        }
                        loop_state_var = 2U;
                        break;
                    }
                    if (*_pre_phi != 0U) {
                        if (r14_5 != 0UL) {
                            loop_state_var = 2U;
                            break;
                        }
                        loop_state_var = 1U;
                        break;
                    }
                }
            switch_state_var = 0;
            switch (loop_state_var) {
              case 0U:
                {
                    var_22 = *var_13;
                    if ((uint64_t)(var_22 + (-15)) == 0UL) {
                        rax_1 = *var_14;
                        rcx3_0 = (uint64_t)var_22;
                        rsi5_0 = var_22 + 1U;
                    } else {
                        *var_16 = var_21;
                        var_23 = local_sp_14 + (-16L);
                        *(uint64_t *)var_23 = 4297647UL;
                        var_24 = indirect_placeholder_94(968UL);
                        var_25 = var_24.field_0;
                        var_26 = *var_18;
                        local_sp_4 = var_23;
                        rax_1 = var_25;
                        rdx4_0 = var_26;
                        local_sp_5_ph = var_23;
                        rdx4_1_ph = var_26;
                        if (var_25 != 0UL) {
                            loop_state_var = 0U;
                            switch_state_var = 1;
                            break;
                        }
                        *(uint64_t *)var_25 = *var_14;
                        *var_14 = var_25;
                    }
                    var_27 = rcx3_0 << 6UL;
                    *var_13 = rsi5_0;
                    var_28 = var_27 + rax_1;
                    var_29 = var_28 + 8UL;
                    *(uint64_t *)var_29 = 0UL;
                    *(uint64_t *)(var_28 + 16UL) = r14_5;
                    *(uint64_t *)(var_28 + 24UL) = rdx4_0;
                    *(unsigned char *)(var_28 + 56UL) = (unsigned char)'\x10';
                    var_30 = (unsigned char *)(var_28 + 58UL);
                    *var_30 = (*var_30 & '\xf3');
                    *(uint64_t *)(var_28 + 32UL) = 0UL;
                    *(uint64_t *)(var_28 + 40UL) = 0UL;
                    *(uint64_t *)(var_28 + 64UL) = 18446744073709551615UL;
                    *(uint64_t *)r14_5 = var_29;
                    *(uint64_t *)rdx4_0 = var_29;
                    local_sp_5_ph = local_sp_4;
                    rdx4_1_ph = rdx4_0;
                    local_sp_13 = local_sp_4;
                    r14_4 = var_29;
                    if (var_29 == 0UL) {
                        continue;
                    }
                    loop_state_var = 0U;
                    switch_state_var = 1;
                    break;
                }
                break;
              case 1U:
                {
                    loop_state_var = 1U;
                    switch_state_var = 1;
                    break;
                }
                break;
              case 2U:
                {
                    loop_state_var = 2U;
                    switch_state_var = 1;
                    break;
                }
                break;
            }
            if (switch_state_var)
                break;
        }
    switch (loop_state_var) {
      case 2U:
        {
            return merge;
        }
        break;
      case 1U:
        {
            while (1U)
                {
                    var_46 = *(uint64_t *)(r14_0 + 8UL);
                    local_sp_1 = local_sp_0;
                    r14_1 = r14_0;
                    local_sp_3 = local_sp_0;
                    rax_0 = var_46;
                    if (var_46 == 0UL) {
                        local_sp_0 = local_sp_3;
                        r14_0 = rax_0;
                        continue;
                    }
                    var_47 = *(uint64_t *)(r14_0 + 16UL);
                    rax_0 = var_47;
                    if (var_47 != 0UL) {
                        while (1U)
                            {
                                var_48 = *(uint32_t *)(r14_1 + 48UL) & 262399U;
                                local_sp_2 = local_sp_1;
                                if ((uint64_t)(var_48 + (-6)) == 0UL) {
                                    *(uint64_t *)(local_sp_1 + (-8L)) = 4297777UL;
                                    indirect_placeholder();
                                    *(uint64_t *)(local_sp_1 + (-16L)) = 4297786UL;
                                    indirect_placeholder();
                                    var_50 = local_sp_1 + (-24L);
                                    *(uint64_t *)var_50 = 4297794UL;
                                    indirect_placeholder();
                                    local_sp_2 = var_50;
                                } else {
                                    if ((uint64_t)(var_48 + (-3)) == 0UL) {
                                        var_49 = local_sp_1 + (-8L);
                                        *(uint64_t *)var_49 = 4297722UL;
                                        indirect_placeholder();
                                        local_sp_2 = var_49;
                                    }
                                }
                                var_51 = *(uint64_t *)r14_1;
                                local_sp_1 = local_sp_2;
                                r14_1 = var_51;
                                local_sp_3 = local_sp_2;
                                if (var_51 != 0UL) {
                                    loop_state_var = 1U;
                                    break;
                                }
                                var_52 = *(uint64_t *)(var_51 + 16UL);
                                rax_0 = var_52;
                                if ((r14_1 == var_52) || (var_52 == 0UL)) {
                                    continue;
                                }
                                loop_state_var = 0U;
                                break;
                            }
                        switch_state_var = 0;
                        switch (loop_state_var) {
                          case 1U:
                            {
                                switch_state_var = 1;
                                break;
                            }
                            break;
                          case 0U:
                            {
                                break;
                            }
                            break;
                        }
                        if (switch_state_var)
                            break;
                    }
                }
        }
        break;
      case 0U:
        {
            local_sp_5 = local_sp_5_ph;
            rdx4_1 = rdx4_1_ph;
            while (1U)
                {
                    var_31 = *(uint64_t *)(rdx4_1 + 8UL);
                    local_sp_6 = local_sp_5;
                    rdx4_2 = rdx4_1;
                    local_sp_12 = local_sp_5;
                    rax_3 = var_31;
                    if (var_31 == 0UL) {
                        local_sp_5 = local_sp_12;
                        rdx4_1 = rax_3;
                        continue;
                    }
                    var_32 = *(uint64_t *)(rdx4_1 + 16UL);
                    rax_3 = var_32;
                    if (var_32 != 0UL) {
                        while (1U)
                            {
                                var_33 = *(uint32_t *)(rdx4_2 + 48UL) & 262399U;
                                local_sp_7 = local_sp_6;
                                rdx4_3 = rdx4_2;
                                if ((uint64_t)(var_33 + (-6)) == 0UL) {
                                    *(uint64_t *)(local_sp_6 + 8UL) = rdx4_2;
                                    *(uint64_t *)(local_sp_6 + (-8L)) = 4297816UL;
                                    indirect_placeholder();
                                    var_35 = (uint64_t *)(local_sp_6 + (-16L));
                                    *var_35 = 4297825UL;
                                    indirect_placeholder();
                                    var_36 = local_sp_6 + (-24L);
                                    *(uint64_t *)var_36 = 4297833UL;
                                    indirect_placeholder();
                                    local_sp_7 = var_36;
                                    rdx4_3 = *var_35;
                                } else {
                                    if ((uint64_t)(var_33 + (-3)) == 0UL) {
                                        *(uint64_t *)(local_sp_6 + 8UL) = rdx4_2;
                                        var_34 = local_sp_6 + (-8L);
                                        *(uint64_t *)var_34 = 4297755UL;
                                        indirect_placeholder();
                                        local_sp_7 = var_34;
                                        rdx4_3 = *(uint64_t *)local_sp_6;
                                    }
                                }
                                var_37 = *(uint64_t *)rdx4_3;
                                local_sp_6 = local_sp_7;
                                rdx4_2 = var_37;
                                local_sp_8 = local_sp_7;
                                local_sp_12 = local_sp_7;
                                if (var_37 == 0UL) {
                                    var_38 = *(uint64_t *)(var_37 + 16UL);
                                    rax_3 = var_38;
                                    if ((rdx4_3 == var_38) || (var_38 == 0UL)) {
                                        continue;
                                    }
                                    loop_state_var = 1U;
                                    break;
                                }
                                while (1U)
                                    {
                                        var_39 = *(uint64_t *)(r14_2 + 8UL);
                                        local_sp_9 = local_sp_8;
                                        r14_3 = r14_2;
                                        local_sp_11 = local_sp_8;
                                        rax_2 = var_39;
                                        if (var_39 == 0UL) {
                                            local_sp_8 = local_sp_11;
                                            r14_2 = rax_2;
                                            continue;
                                        }
                                        var_40 = *(uint64_t *)(r14_2 + 16UL);
                                        rax_2 = var_40;
                                        if (var_40 != 0UL) {
                                            while (1U)
                                                {
                                                    var_41 = *(uint32_t *)(r14_3 + 48UL) & 262399U;
                                                    local_sp_10 = local_sp_9;
                                                    if ((uint64_t)(var_41 + (-6)) == 0UL) {
                                                        *(uint64_t *)(local_sp_9 + (-8L)) = 4297855UL;
                                                        indirect_placeholder();
                                                        *(uint64_t *)(local_sp_9 + (-16L)) = 4297864UL;
                                                        indirect_placeholder();
                                                        var_43 = local_sp_9 + (-24L);
                                                        *(uint64_t *)var_43 = 4297872UL;
                                                        indirect_placeholder();
                                                        local_sp_10 = var_43;
                                                    } else {
                                                        if ((uint64_t)(var_41 + (-3)) == 0UL) {
                                                            var_42 = local_sp_9 + (-8L);
                                                            *(uint64_t *)var_42 = 4297736UL;
                                                            indirect_placeholder();
                                                            local_sp_10 = var_42;
                                                        }
                                                    }
                                                    var_44 = *(uint64_t *)r14_3;
                                                    local_sp_9 = local_sp_10;
                                                    r14_3 = var_44;
                                                    local_sp_11 = local_sp_10;
                                                    if (var_44 == 0UL) {
                                                        var_45 = *(uint64_t *)(var_44 + 16UL);
                                                        rax_2 = var_45;
                                                        if ((r14_3 == var_45) || (var_45 == 0UL)) {
                                                            continue;
                                                        }
                                                        loop_state_var = 0U;
                                                        break;
                                                    }
                                                    *_pre_phi = 12U;
                                                    loop_state_var = 1U;
                                                    break;
                                                }
                                            switch_state_var = 0;
                                            switch (loop_state_var) {
                                              case 1U:
                                                {
                                                    switch_state_var = 1;
                                                    break;
                                                }
                                                break;
                                              case 0U:
                                                {
                                                    break;
                                                }
                                                break;
                                            }
                                            if (switch_state_var)
                                                break;
                                        }
                                    }
                                loop_state_var = 0U;
                                break;
                            }
                        switch_state_var = 0;
                        switch (loop_state_var) {
                          case 0U:
                            {
                                switch_state_var = 1;
                                break;
                            }
                            break;
                          case 1U:
                            {
                                break;
                            }
                            break;
                        }
                        if (switch_state_var)
                            break;
                    }
                }
        }
        break;
    }
}

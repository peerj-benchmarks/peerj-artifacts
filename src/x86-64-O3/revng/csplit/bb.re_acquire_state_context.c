typedef unsigned char __u_char;
typedef unsigned short __u_short;
typedef unsigned int __u_int;
typedef unsigned long __u_long;
typedef signed char __int8_t;
typedef unsigned char __uint8_t;
typedef short __int16_t;
typedef unsigned short __uint16_t;
typedef int __int32_t;
typedef unsigned int __uint32_t;
typedef long __int64_t;
typedef unsigned long __uint64_t;
typedef __int8_t __int_least8_t;
typedef __uint8_t __uint_least8_t;
typedef __int16_t __int_least16_t;
typedef __uint16_t __uint_least16_t;
typedef __int32_t __int_least32_t;
typedef __uint32_t __uint_least32_t;
typedef __int64_t __int_least64_t;
typedef __uint64_t __uint_least64_t;
typedef long __quad_t;
typedef unsigned long __u_quad_t;
typedef long __intmax_t;
typedef unsigned long __uintmax_t;
typedef unsigned long __dev_t;
typedef unsigned int __uid_t;
typedef unsigned int __gid_t;
typedef unsigned long __ino_t;
typedef unsigned long __ino64_t;
typedef unsigned int __mode_t;
typedef unsigned long __nlink_t;
typedef long __off_t;
typedef long __off64_t;
typedef int __pid_t;
typedef struct {
    int __val[2];
} __fsid_t;
typedef long __clock_t;
typedef unsigned long __rlim_t;
typedef unsigned long __rlim64_t;
typedef unsigned int __id_t;
typedef long __time_t;
typedef unsigned int __useconds_t;
typedef long __suseconds_t;
typedef int __daddr_t;
typedef int __key_t;
typedef int __clockid_t;
typedef void *__timer_t;
typedef long __blksize_t;
typedef long __blkcnt_t;
typedef long __blkcnt64_t;
typedef unsigned long __fsblkcnt_t;
typedef unsigned long __fsblkcnt64_t;
typedef unsigned long __fsfilcnt_t;
typedef unsigned long __fsfilcnt64_t;
typedef long __fsword_t;
typedef long __ssize_t;
typedef long __syscall_slong_t;
typedef unsigned long __syscall_ulong_t;
typedef __off64_t __loff_t;
typedef char *__caddr_t;
typedef long __intptr_t;
typedef unsigned int __socklen_t;
typedef int __sig_atomic_t;
typedef __int8_t int8_t;
typedef __int16_t int16_t;
typedef __int32_t int32_t;
typedef __int64_t int64_t;
typedef __uint8_t uint8_t;
typedef __uint16_t uint16_t;
typedef __uint32_t uint32_t;
typedef __uint64_t uint64_t;
typedef __int_least8_t int_least8_t;
typedef __int_least16_t int_least16_t;
typedef __int_least32_t int_least32_t;
typedef __int_least64_t int_least64_t;
typedef __uint_least8_t uint_least8_t;
typedef __uint_least16_t uint_least16_t;
typedef __uint_least32_t uint_least32_t;
typedef __uint_least64_t uint_least64_t;
typedef signed char int_fast8_t;
typedef long int_fast16_t;
typedef long int_fast32_t;
typedef long int_fast64_t;
typedef unsigned char uint_fast8_t;
typedef unsigned long uint_fast16_t;
typedef unsigned long uint_fast32_t;
typedef unsigned long uint_fast64_t;
typedef long intptr_t;
typedef unsigned long uintptr_t;
typedef __intmax_t intmax_t;
typedef __uintmax_t uintmax_t;
typedef _Bool bool;
struct bb_re_acquire_state_context_ret_type;
struct helper_pxor_xmm_wrapper_ret_type;
struct type_5;
struct type_7;
struct indirect_placeholder_32_ret_type;
struct indirect_placeholder_29_ret_type;
struct indirect_placeholder_30_ret_type;
struct indirect_placeholder_31_ret_type;
struct helper_paddq_xmm_wrapper_305_ret_type;
struct helper_psrldq_xmm_wrapper_ret_type;
struct helper_paddq_xmm_wrapper_304_ret_type;
struct bb_re_acquire_state_context_ret_type {
    uint64_t field_0;
    uint64_t field_1;
    uint64_t field_2;
    uint64_t field_3;
};
struct helper_pxor_xmm_wrapper_ret_type {
    uint64_t field_0;
    uint64_t field_1;
};
struct type_5 {
};
struct type_7 {
};
struct indirect_placeholder_32_ret_type {
    uint64_t field_0;
    uint64_t field_1;
    uint64_t field_2;
    uint64_t field_3;
    uint64_t field_4;
    uint64_t field_5;
    uint64_t field_6;
};
struct indirect_placeholder_29_ret_type {
    uint64_t field_0;
    uint64_t field_1;
};
struct indirect_placeholder_30_ret_type {
    uint64_t field_0;
    uint64_t field_1;
};
struct indirect_placeholder_31_ret_type {
    uint64_t field_0;
    uint64_t field_1;
};
struct helper_paddq_xmm_wrapper_305_ret_type {
    uint64_t field_0;
    uint64_t field_1;
};
struct helper_psrldq_xmm_wrapper_ret_type {
    uint64_t field_0;
    uint64_t field_1;
    uint64_t field_2;
    uint64_t field_3;
    uint64_t field_4;
    uint64_t field_5;
    uint64_t field_6;
    uint64_t field_7;
};
struct helper_paddq_xmm_wrapper_304_ret_type {
    uint64_t field_0;
    uint64_t field_1;
};
extern uint64_t revng_init_local_sp(uint64_t param_0);
extern uint64_t init_rbx(void);
extern uint64_t init_r12(void);
extern uint64_t init_rbp(void);
extern void indirect_placeholder(void);
extern uint64_t init_state_0x8558(void);
extern uint64_t init_state_0x8560(void);
extern uint64_t init_cc_src2(void);
extern uint64_t helper_cc_compute_c_wrapper(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint32_t param_3);
extern uint64_t init_r13(void);
extern uint64_t init_r14(void);
extern uint64_t helper_cc_compute_all_wrapper(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint32_t param_3);
extern uint64_t init_r15(void);
extern struct helper_pxor_xmm_wrapper_ret_type helper_pxor_xmm_wrapper(struct type_5 *param_0, struct type_7 *param_1, struct type_7 *param_2, uint64_t param_3, uint64_t param_4);
extern uint64_t indirect_placeholder_5(uint64_t param_0, uint64_t param_1, uint64_t param_2);
extern struct indirect_placeholder_32_ret_type indirect_placeholder_32(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint64_t param_3, uint64_t param_4, uint64_t param_5, uint64_t param_6, uint64_t param_7);
extern uint64_t init_state_0x85a8(void);
extern uint64_t init_state_0x85b0(void);
extern uint64_t init_state_0x85b8(void);
extern uint64_t init_state_0x85c0(void);
extern uint64_t init_state_0x85c8(void);
extern uint64_t init_state_0x85d0(void);
extern struct indirect_placeholder_29_ret_type indirect_placeholder_29(uint64_t param_0);
extern struct indirect_placeholder_30_ret_type indirect_placeholder_30(uint64_t param_0);
extern struct indirect_placeholder_31_ret_type indirect_placeholder_31(uint64_t param_0);
extern struct helper_paddq_xmm_wrapper_305_ret_type helper_paddq_xmm_wrapper_305(struct type_5 *param_0, struct type_7 *param_1, struct type_7 *param_2, uint64_t param_3, uint64_t param_4, uint64_t param_5, uint64_t param_6);
extern struct helper_psrldq_xmm_wrapper_ret_type helper_psrldq_xmm_wrapper(struct type_5 *param_0, struct type_7 *param_1, struct type_7 *param_2, uint64_t param_3, uint64_t param_4, uint64_t param_5, uint64_t param_6, uint64_t param_7, uint64_t param_8, uint64_t param_9, uint64_t param_10, uint64_t param_11);
extern struct helper_paddq_xmm_wrapper_304_ret_type helper_paddq_xmm_wrapper_304(struct type_5 *param_0, struct type_7 *param_1, struct type_7 *param_2, uint64_t param_3, uint64_t param_4, uint64_t param_5, uint64_t param_6);
struct bb_re_acquire_state_context_ret_type bb_re_acquire_state_context(uint64_t r10, uint64_t rdi, uint64_t r9, uint64_t rcx, uint64_t rdx, uint64_t rsi) {
    uint64_t var_49;
    uint64_t var_50;
    uint64_t *var_51;
    uint64_t rbp_3;
    uint64_t rdi13_6_in;
    uint64_t r914_5;
    uint64_t rdi13_6;
    uint64_t var_52;
    uint64_t rcx15_2;
    struct bb_re_acquire_state_context_ret_type mrv4;
    struct bb_re_acquire_state_context_ret_type mrv5;
    struct bb_re_acquire_state_context_ret_type mrv6;
    struct bb_re_acquire_state_context_ret_type mrv7;
    uint64_t var_53;
    uint64_t var_54;
    struct indirect_placeholder_32_ret_type var_57;
    uint64_t var_58;
    uint64_t var_59;
    uint64_t var_60;
    uint64_t var_61;
    uint64_t var_62;
    struct bb_re_acquire_state_context_ret_type mrv9 = {0UL, /*implicit*/(int)0, /*implicit*/(int)0, /*implicit*/(int)0};
    struct bb_re_acquire_state_context_ret_type mrv10;
    struct bb_re_acquire_state_context_ret_type mrv11;
    struct helper_paddq_xmm_wrapper_305_ret_type var_34;
    struct helper_pxor_xmm_wrapper_ret_type var_32;
    uint64_t var_0;
    uint64_t var_1;
    uint64_t var_2;
    uint64_t var_3;
    uint64_t var_4;
    uint64_t var_5;
    uint64_t var_6;
    uint64_t var_7;
    uint64_t var_8;
    uint64_t var_9;
    uint64_t var_10;
    uint64_t var_11;
    uint64_t var_12;
    uint64_t var_13;
    uint64_t var_14;
    uint64_t var_15;
    uint64_t *var_16;
    uint64_t var_17;
    uint64_t var_133;
    uint64_t local_sp_8;
    uint64_t local_sp_0;
    uint64_t storemerge4;
    uint64_t var_134;
    uint64_t r8_1_ph;
    uint64_t local_sp_1;
    uint64_t storemerge5;
    uint64_t r11_2;
    uint64_t local_sp_4_ph;
    uint64_t r11_0_ph;
    uint64_t r1012_2;
    uint64_t var_114;
    uint64_t var_121;
    uint64_t var_104;
    uint64_t var_55;
    uint64_t *var_56;
    uint64_t var_106;
    uint64_t var_107;
    struct indirect_placeholder_29_ret_type var_108;
    uint64_t var_109;
    uint64_t var_110;
    uint64_t var_111;
    uint32_t var_112;
    uint32_t var_113;
    uint64_t var_105;
    uint64_t var_122;
    uint64_t *_pre_phi330;
    uint64_t r1012_0;
    uint64_t local_sp_2;
    uint64_t r8_0;
    uint64_t *var_129;
    uint64_t var_130;
    uint64_t var_131;
    uint64_t var_132;
    uint64_t local_sp_3;
    uint64_t storemerge_in_in;
    uint64_t *storemerge_in;
    uint64_t storemerge;
    unsigned char *var_73;
    unsigned char var_74;
    uint64_t *var_75;
    uint64_t var_76;
    uint64_t var_77;
    uint64_t var_78;
    uint64_t var_79;
    uint64_t *var_80;
    uint64_t *var_81;
    uint64_t *var_82;
    uint64_t rdi13_2;
    uint64_t r1012_3;
    uint64_t r1012_1_ph;
    uint64_t local_sp_6;
    uint64_t rdi13_0_ph;
    uint64_t rbp_0_ph;
    uint64_t rcx15_0_ph;
    uint64_t r1012_5;
    uint64_t rbp_0;
    uint64_t var_83;
    uint64_t var_84;
    uint64_t var_71;
    uint32_t var_85;
    unsigned char var_86;
    uint64_t var_87;
    bool var_88;
    uint32_t _pre_phi334;
    uint32_t var_89;
    uint64_t var_125;
    uint64_t r1012_4;
    unsigned char var_90;
    uint64_t var_91;
    unsigned char var_92;
    uint64_t var_93;
    unsigned char var_94;
    uint64_t var_95;
    uint64_t rdi13_1;
    uint64_t local_sp_5;
    uint64_t r11_1;
    uint64_t var_115;
    uint64_t var_116;
    uint64_t var_117;
    uint64_t rax_0;
    uint64_t var_118;
    uint64_t var_119;
    uint64_t var_120;
    uint32_t *var_96;
    uint64_t var_97;
    struct indirect_placeholder_30_ret_type var_98;
    uint64_t var_99;
    uint64_t var_100;
    uint32_t *var_101;
    uint32_t var_102;
    uint32_t var_103;
    uint64_t var_123;
    uint64_t var_124;
    uint64_t local_sp_7;
    uint64_t r8_2;
    uint64_t var_126;
    uint64_t *_pre329;
    uint64_t r14_0;
    uint64_t r8_3;
    uint64_t var_127;
    uint64_t var_128;
    uint64_t rax_1;
    uint64_t var_72;
    uint64_t var_135;
    uint64_t var_63;
    uint64_t var_64;
    uint64_t *var_65;
    uint64_t *var_67;
    uint64_t var_68;
    struct indirect_placeholder_31_ret_type var_69;
    uint64_t var_70;
    uint64_t var_66;
    uint64_t r1012_11;
    uint64_t r1012_6;
    uint64_t r914_1;
    uint64_t local_sp_9;
    uint64_t r8_4;
    uint64_t r1012_7;
    uint64_t r914_2;
    uint64_t r8_5;
    struct bb_re_acquire_state_context_ret_type mrv;
    struct bb_re_acquire_state_context_ret_type mrv1;
    struct bb_re_acquire_state_context_ret_type mrv2;
    struct bb_re_acquire_state_context_ret_type mrv3;
    uint32_t var_18;
    uint64_t var_19;
    uint64_t var_20;
    uint64_t var_21;
    uint64_t r1012_8;
    uint64_t var_22;
    uint64_t var_23;
    uint64_t var_24;
    uint64_t rax_2;
    uint64_t rax_3;
    uint64_t var_25;
    uint64_t rdi13_3;
    uint64_t var_26;
    uint64_t var_27;
    uint64_t var_28;
    uint64_t r14_1;
    uint64_t rdi13_4;
    uint64_t r14_2;
    uint64_t var_29;
    uint64_t var_30;
    uint64_t var_31;
    uint64_t rdi13_5;
    uint64_t rax_4;
    uint64_t state_0x8558_0;
    uint64_t state_0x8560_0;
    uint64_t rdx16_0;
    uint64_t var_33;
    uint64_t var_35;
    uint64_t var_36;
    uint64_t var_37;
    uint64_t var_38;
    struct helper_psrldq_xmm_wrapper_ret_type var_39;
    struct helper_paddq_xmm_wrapper_304_ret_type var_40;
    uint64_t var_41;
    uint64_t r14_3;
    uint64_t r914_3;
    uint64_t rcx15_1;
    uint64_t r14_4;
    uint64_t var_42;
    uint64_t var_43;
    uint64_t var_44;
    uint64_t var_45;
    uint64_t var_46;
    uint64_t *var_47;
    uint64_t r914_6;
    uint64_t r1012_10;
    uint64_t r1012_9_ph;
    uint64_t rbp_2;
    uint64_t r914_4_ph;
    uint64_t r914_7;
    uint64_t rbp_1_ph;
    uint64_t rdx16_1_ph;
    uint64_t r1012_9;
    uint64_t r914_4;
    uint64_t rdx16_1;
    uint64_t var_48;
    unsigned int loop_state_var;
    bool switch_state_var;
    var_0 = revng_init_local_sp(0UL);
    var_1 = init_r15();
    var_2 = init_r13();
    var_3 = init_rbx();
    var_4 = init_r12();
    var_5 = init_rbp();
    var_6 = init_cc_src2();
    var_7 = init_state_0x8558();
    var_8 = init_state_0x8560();
    var_9 = init_r14();
    var_10 = init_state_0x85a8();
    var_11 = init_state_0x85b0();
    var_12 = init_state_0x85b8();
    var_13 = init_state_0x85c0();
    var_14 = init_state_0x85c8();
    var_15 = init_state_0x85d0();
    *(uint64_t *)(var_0 + (-8L)) = var_1;
    *(uint64_t *)(var_0 + (-16L)) = var_9;
    *(uint64_t *)(var_0 + (-24L)) = var_2;
    *(uint64_t *)(var_0 + (-32L)) = var_4;
    *(uint64_t *)(var_0 + (-40L)) = var_5;
    *(uint64_t *)(var_0 + (-48L)) = var_3;
    var_16 = (uint64_t *)(rdx + 8UL);
    var_17 = *var_16;
    *(uint64_t *)(var_0 + (-80L)) = rdi;
    rbp_3 = var_5;
    r914_5 = var_17;
    rdi13_0_ph = 0UL;
    rbp_0_ph = 0UL;
    rax_1 = 0UL;
    r1012_8 = r10;
    rax_2 = var_17;
    rax_3 = 0UL;
    rdi13_3 = 1UL;
    rdi13_4 = 0UL;
    rax_4 = 0UL;
    r914_3 = r9;
    rcx15_1 = rcx;
    rbp_1_ph = var_5;
    rdx16_1_ph = 0UL;
    if (var_17 == 0UL) {
        *(uint32_t *)rdi = 0U;
        mrv9.field_1 = r10;
        mrv10 = mrv9;
        mrv10.field_2 = r9;
        mrv11 = mrv10;
        mrv11.field_3 = 0UL;
        return mrv11;
    }
    var_18 = (uint32_t)rcx;
    var_19 = (uint64_t)var_18;
    var_20 = var_19 + var_17;
    var_21 = helper_cc_compute_all_wrapper(var_17, 0UL, 0UL, 25U);
    r14_2 = var_20;
    r14_4 = var_20;
    if ((uint64_t)(((unsigned char)(var_21 >> 4UL) ^ (unsigned char)var_21) & '\xc0') != 0UL) {
        var_22 = *(uint64_t *)(rdx + 16UL);
        var_23 = (var_22 >> 3UL) & 1UL;
        var_24 = (var_23 > var_17) ? var_17 : var_23;
        if ((long)var_17 > (long)5UL) {
            rax_2 = var_24;
            if (var_24 == 0UL) {
                var_25 = var_20 + *(uint64_t *)var_22;
                r14_1 = var_25;
                rax_3 = rax_2;
                var_26 = var_25 + *(uint64_t *)(var_22 + 8UL);
                rdi13_3 = 2UL;
                r14_1 = var_26;
                var_27 = var_26 + *(uint64_t *)(var_22 + 16UL);
                rdi13_3 = 3UL;
                r14_1 = var_27;
                var_28 = var_27 + *(uint64_t *)(var_22 + 24UL);
                rdi13_3 = 4UL;
                r14_1 = var_28;
                if (rax_2 != 1UL & rax_2 != 2UL & rax_2 != 3UL & rax_2 == 5UL) {
                    rdi13_3 = 5UL;
                    r14_1 = var_28 + *(uint64_t *)(var_22 + 32UL);
                }
                rdi13_4 = rdi13_3;
                r14_2 = r14_1;
                r14_4 = r14_1;
                if (var_17 != rax_2) {
                    var_29 = var_17 - rax_3;
                    var_30 = ((var_29 + (-2L)) >> 1UL) + 1UL;
                    var_31 = var_30 << 1UL;
                    r1012_8 = var_31;
                    rdi13_5 = rdi13_4;
                    r14_3 = r14_2;
                    r914_3 = var_29;
                    rcx15_1 = var_30;
                    if (var_29 == 1UL) {
                        r14_4 = r14_3 + *(uint64_t *)((rdi13_5 << 3UL) + var_22);
                    } else {
                        var_32 = helper_pxor_xmm_wrapper((struct type_5 *)(0UL), (struct type_7 *)(776UL), (struct type_7 *)(776UL), var_7, var_8);
                        state_0x8558_0 = var_32.field_0;
                        state_0x8560_0 = var_32.field_1;
                        rdx16_0 = (rax_3 << 3UL) + var_22;
                        r1012_8 = var_29;
                        var_33 = rax_4 + 1UL;
                        var_34 = helper_paddq_xmm_wrapper_305((struct type_5 *)(0UL), (struct type_7 *)(776UL), (struct type_7 *)(2824UL), state_0x8558_0, state_0x8560_0, *(uint64_t *)rdx16_0, *(uint64_t *)(rdx16_0 + 8UL));
                        var_35 = helper_cc_compute_c_wrapper(var_33 - var_30, var_30, var_6, 17U);
                        rax_4 = var_33;
                        while (var_35 != 0UL)
                            {
                                state_0x8558_0 = var_34.field_0;
                                state_0x8560_0 = var_34.field_1;
                                rdx16_0 = rdx16_0 + 16UL;
                                var_33 = rax_4 + 1UL;
                                var_34 = helper_paddq_xmm_wrapper_305((struct type_5 *)(0UL), (struct type_7 *)(776UL), (struct type_7 *)(2824UL), state_0x8558_0, state_0x8560_0, *(uint64_t *)rdx16_0, *(uint64_t *)(rdx16_0 + 8UL));
                                var_35 = helper_cc_compute_c_wrapper(var_33 - var_30, var_30, var_6, 17U);
                                rax_4 = var_33;
                            }
                        var_36 = var_34.field_0;
                        var_37 = var_34.field_1;
                        var_38 = rdi13_4 + var_31;
                        var_39 = helper_psrldq_xmm_wrapper((struct type_5 *)(0UL), (struct type_7 *)(840UL), (struct type_7 *)(2824UL), var_36, var_37, 8UL, var_10, var_11, var_12, var_13, var_14, var_15);
                        var_40 = helper_paddq_xmm_wrapper_304((struct type_5 *)(0UL), (struct type_7 *)(776UL), (struct type_7 *)(840UL), var_36, var_37, var_39.field_0, var_39.field_1);
                        var_41 = r14_2 + var_40.field_0;
                        rdi13_5 = var_38;
                        r14_3 = var_41;
                        r14_4 = var_41;
                        if (var_31 == var_29) {
                            r14_4 = r14_3 + *(uint64_t *)((rdi13_5 << 3UL) + var_22);
                        }
                    }
                }
            } else {
                var_29 = var_17 - rax_3;
                var_30 = ((var_29 + (-2L)) >> 1UL) + 1UL;
                var_31 = var_30 << 1UL;
                r1012_8 = var_31;
                rdi13_5 = rdi13_4;
                r14_3 = r14_2;
                r914_3 = var_29;
                rcx15_1 = var_30;
                if (var_29 == 1UL) {
                    r14_4 = r14_3 + *(uint64_t *)((rdi13_5 << 3UL) + var_22);
                } else {
                    var_32 = helper_pxor_xmm_wrapper((struct type_5 *)(0UL), (struct type_7 *)(776UL), (struct type_7 *)(776UL), var_7, var_8);
                    state_0x8558_0 = var_32.field_0;
                    state_0x8560_0 = var_32.field_1;
                    rdx16_0 = (rax_3 << 3UL) + var_22;
                    r1012_8 = var_29;
                    var_33 = rax_4 + 1UL;
                    var_34 = helper_paddq_xmm_wrapper_305((struct type_5 *)(0UL), (struct type_7 *)(776UL), (struct type_7 *)(2824UL), state_0x8558_0, state_0x8560_0, *(uint64_t *)rdx16_0, *(uint64_t *)(rdx16_0 + 8UL));
                    var_35 = helper_cc_compute_c_wrapper(var_33 - var_30, var_30, var_6, 17U);
                    rax_4 = var_33;
                    while (var_35 != 0UL)
                        {
                            state_0x8558_0 = var_34.field_0;
                            state_0x8560_0 = var_34.field_1;
                            rdx16_0 = rdx16_0 + 16UL;
                            var_33 = rax_4 + 1UL;
                            var_34 = helper_paddq_xmm_wrapper_305((struct type_5 *)(0UL), (struct type_7 *)(776UL), (struct type_7 *)(2824UL), state_0x8558_0, state_0x8560_0, *(uint64_t *)rdx16_0, *(uint64_t *)(rdx16_0 + 8UL));
                            var_35 = helper_cc_compute_c_wrapper(var_33 - var_30, var_30, var_6, 17U);
                            rax_4 = var_33;
                        }
                    var_36 = var_34.field_0;
                    var_37 = var_34.field_1;
                    var_38 = rdi13_4 + var_31;
                    var_39 = helper_psrldq_xmm_wrapper((struct type_5 *)(0UL), (struct type_7 *)(840UL), (struct type_7 *)(2824UL), var_36, var_37, 8UL, var_10, var_11, var_12, var_13, var_14, var_15);
                    var_40 = helper_paddq_xmm_wrapper_304((struct type_5 *)(0UL), (struct type_7 *)(776UL), (struct type_7 *)(840UL), var_36, var_37, var_39.field_0, var_39.field_1);
                    var_41 = r14_2 + var_40.field_0;
                    rdi13_5 = var_38;
                    r14_3 = var_41;
                    r14_4 = var_41;
                    if (var_31 == var_29) {
                        r14_4 = r14_3 + *(uint64_t *)((rdi13_5 << 3UL) + var_22);
                    }
                }
            }
        } else {
            var_25 = var_20 + *(uint64_t *)var_22;
            r14_1 = var_25;
            rax_3 = rax_2;
            var_26 = var_25 + *(uint64_t *)(var_22 + 8UL);
            rdi13_3 = 2UL;
            r14_1 = var_26;
            var_27 = var_26 + *(uint64_t *)(var_22 + 16UL);
            rdi13_3 = 3UL;
            r14_1 = var_27;
            var_28 = var_27 + *(uint64_t *)(var_22 + 24UL);
            rdi13_3 = 4UL;
            r14_1 = var_28;
            if (rax_2 != 1UL & rax_2 != 2UL & rax_2 != 3UL & rax_2 == 5UL) {
                rdi13_3 = 5UL;
                r14_1 = var_28 + *(uint64_t *)(var_22 + 32UL);
            }
            rdi13_4 = rdi13_3;
            r14_2 = r14_1;
            r14_4 = r14_1;
            if (var_17 != rax_2) {
                var_29 = var_17 - rax_3;
                var_30 = ((var_29 + (-2L)) >> 1UL) + 1UL;
                var_31 = var_30 << 1UL;
                r1012_8 = var_31;
                rdi13_5 = rdi13_4;
                r14_3 = r14_2;
                r914_3 = var_29;
                rcx15_1 = var_30;
                if (var_29 == 1UL) {
                    r14_4 = r14_3 + *(uint64_t *)((rdi13_5 << 3UL) + var_22);
                } else {
                    var_32 = helper_pxor_xmm_wrapper((struct type_5 *)(0UL), (struct type_7 *)(776UL), (struct type_7 *)(776UL), var_7, var_8);
                    state_0x8558_0 = var_32.field_0;
                    state_0x8560_0 = var_32.field_1;
                    rdx16_0 = (rax_3 << 3UL) + var_22;
                    r1012_8 = var_29;
                    var_33 = rax_4 + 1UL;
                    var_34 = helper_paddq_xmm_wrapper_305((struct type_5 *)(0UL), (struct type_7 *)(776UL), (struct type_7 *)(2824UL), state_0x8558_0, state_0x8560_0, *(uint64_t *)rdx16_0, *(uint64_t *)(rdx16_0 + 8UL));
                    var_35 = helper_cc_compute_c_wrapper(var_33 - var_30, var_30, var_6, 17U);
                    rax_4 = var_33;
                    while (var_35 != 0UL)
                        {
                            state_0x8558_0 = var_34.field_0;
                            state_0x8560_0 = var_34.field_1;
                            rdx16_0 = rdx16_0 + 16UL;
                            var_33 = rax_4 + 1UL;
                            var_34 = helper_paddq_xmm_wrapper_305((struct type_5 *)(0UL), (struct type_7 *)(776UL), (struct type_7 *)(2824UL), state_0x8558_0, state_0x8560_0, *(uint64_t *)rdx16_0, *(uint64_t *)(rdx16_0 + 8UL));
                            var_35 = helper_cc_compute_c_wrapper(var_33 - var_30, var_30, var_6, 17U);
                            rax_4 = var_33;
                        }
                    var_36 = var_34.field_0;
                    var_37 = var_34.field_1;
                    var_38 = rdi13_4 + var_31;
                    var_39 = helper_psrldq_xmm_wrapper((struct type_5 *)(0UL), (struct type_7 *)(840UL), (struct type_7 *)(2824UL), var_36, var_37, 8UL, var_10, var_11, var_12, var_13, var_14, var_15);
                    var_40 = helper_paddq_xmm_wrapper_304((struct type_5 *)(0UL), (struct type_7 *)(776UL), (struct type_7 *)(840UL), var_36, var_37, var_39.field_0, var_39.field_1);
                    var_41 = r14_2 + var_40.field_0;
                    rdi13_5 = var_38;
                    r14_3 = var_41;
                    r14_4 = var_41;
                    if (var_31 == var_29) {
                        r14_4 = r14_3 + *(uint64_t *)((rdi13_5 << 3UL) + var_22);
                    }
                }
            }
        }
    }
    var_42 = ((r14_4 & *(uint64_t *)(rsi + 136UL)) * 24UL) + *(uint64_t *)(rsi + 64UL);
    var_43 = *(uint64_t *)var_42;
    var_44 = helper_cc_compute_all_wrapper(var_43, 0UL, 0UL, 25U);
    rcx15_2 = rcx15_1;
    r14_0 = r14_4;
    r1012_11 = r1012_8;
    r1012_9_ph = r1012_8;
    r914_4_ph = r914_3;
    r914_7 = r914_3;
    if ((uint64_t)(((unsigned char)(var_44 >> 4UL) ^ (unsigned char)var_44) & '\xc0') != 0UL) {
        var_45 = *(uint64_t *)(var_42 + 16UL);
        var_46 = var_17 << 3UL;
        var_47 = (uint64_t *)(rdx + 16UL);
        rdi13_6_in = var_46;
        r914_7 = var_17;
        rcx15_2 = var_45;
        while (1U)
            {
                rbp_3 = rbp_1_ph;
                rbp_2 = rbp_1_ph;
                r1012_9 = r1012_9_ph;
                r914_4 = r914_4_ph;
                rdx16_1 = rdx16_1_ph;
                while (1U)
                    {
                        var_48 = *(uint64_t *)((rdx16_1 << 3UL) + var_45);
                        r914_4 = var_17;
                        r1012_10 = r1012_9;
                        r914_6 = r914_4;
                        if (*(uint64_t *)var_48 != r14_4) {
                            loop_state_var = 1U;
                            break;
                        }
                        if ((uint64_t)((uint32_t)(uint64_t)(*(unsigned char *)(var_48 + 104UL) & '\x0f') - var_18) != 0UL) {
                            loop_state_var = 1U;
                            break;
                        }
                        var_49 = *(uint64_t *)(var_48 + 80UL);
                        r1012_9 = var_49;
                        r1012_10 = 0UL;
                        r1012_11 = var_49;
                        if (var_49 != 0UL) {
                            loop_state_var = 1U;
                            break;
                        }
                        r1012_10 = var_49;
                        if (var_17 != *(uint64_t *)(var_49 + 8UL)) {
                            loop_state_var = 2U;
                            break;
                        }
                        var_50 = rdx16_1 + 1UL;
                        rdx16_1 = var_50;
                        if (var_50 == var_43) {
                            continue;
                        }
                        loop_state_var = 0U;
                        break;
                    }
                switch_state_var = 0;
                switch (loop_state_var) {
                  case 0U:
                    {
                        switch_state_var = 1;
                        break;
                    }
                    break;
                  case 2U:
                  case 1U:
                    {
                        switch_state_var = 0;
                        switch (loop_state_var) {
                          case 1U:
                            {
                                var_54 = rdx16_1 + 1UL;
                                rbp_3 = rbp_2;
                                r1012_11 = r1012_10;
                                r1012_9_ph = r1012_10;
                                r914_4_ph = r914_6;
                                r914_7 = r914_6;
                                rbp_1_ph = rbp_2;
                                rdx16_1_ph = var_54;
                                if (var_54 != var_43) {
                                    continue;
                                }
                                switch_state_var = 1;
                                break;
                            }
                            break;
                          case 2U:
                            {
                                var_51 = (uint64_t *)(var_49 + 16UL);
                                while (1U)
                                    {
                                        rdi13_6 = rdi13_6_in + (-8L);
                                        var_52 = r914_5 + (-1L);
                                        rdi13_6_in = rdi13_6;
                                        r914_5 = var_52;
                                        r914_6 = var_52;
                                        if ((long)var_52 <= (long)18446744073709551615UL) {
                                            var_53 = *(uint64_t *)(rdi13_6 + *var_51);
                                            rbp_2 = var_53;
                                            if (var_53 != *(uint64_t *)(rdi13_6 + *var_47)) {
                                                continue;
                                            }
                                            break;
                                        }
                                        mrv4.field_0 = var_48;
                                        mrv5 = mrv4;
                                        mrv5.field_1 = var_49;
                                        mrv6 = mrv5;
                                        mrv6.field_2 = var_52;
                                        mrv7 = mrv6;
                                        mrv7.field_3 = var_17;
                                        return mrv7;
                                    }
                            }
                            break;
                        }
                        if (switch_state_var)
                            break;
                    }
                    break;
                }
                if (switch_state_var)
                    break;
            }
    }
    var_55 = var_0 + (-112L);
    var_56 = (uint64_t *)var_55;
    *var_56 = 4261263UL;
    var_57 = indirect_placeholder_32(var_19, r1012_11, 112UL, r914_7, rbp_3, rcx15_2, 1UL, var_17);
    var_58 = var_57.field_0;
    var_59 = var_57.field_1;
    var_60 = var_57.field_2;
    var_61 = var_57.field_3;
    var_62 = var_57.field_6;
    r8_1_ph = var_62;
    r1012_5 = var_60;
    r8_3 = var_62;
    r1012_6 = var_60;
    r914_1 = var_61;
    local_sp_9 = var_55;
    r8_4 = var_62;
    if (var_58 == 0UL) {
        **(uint32_t **)(local_sp_9 + 24UL) = 12U;
        r1012_7 = r1012_6;
        r914_2 = r914_1;
        r8_5 = r8_4;
        mrv.field_0 = rax_1;
        mrv1 = mrv;
        mrv1.field_1 = r1012_7;
        mrv2 = mrv1;
        mrv2.field_2 = r914_2;
        mrv3 = mrv2;
        mrv3.field_3 = r8_5;
        return mrv3;
    }
    var_63 = *var_16;
    var_64 = var_58 + 8UL;
    var_65 = (uint64_t *)(var_58 + 16UL);
    *var_65 = var_63;
    rax_1 = var_58;
    if ((long)*var_16 > (long)0UL) {
        *(uint64_t *)(var_0 + (-104L)) = var_64;
        var_66 = var_0 + (-120L);
        *(uint64_t *)var_66 = 4262116UL;
        indirect_placeholder();
        local_sp_3 = var_66;
    } else {
        var_67 = (uint64_t *)var_64;
        *var_67 = var_63;
        var_68 = var_63 << 3UL;
        *(uint64_t *)(var_0 + (-104L)) = var_64;
        *(uint64_t *)(var_0 + (-120L)) = 4261316UL;
        var_69 = indirect_placeholder_31(var_68);
        var_70 = var_69.field_0;
        *(uint64_t *)(var_58 + 24UL) = var_70;
        var_71 = *var_56;
        r914_1 = var_71;
        if (var_70 != 0UL) {
            *var_65 = 0UL;
            *var_67 = 0UL;
            var_135 = var_0 + (-128L);
            *(uint64_t *)var_135 = 4262277UL;
            indirect_placeholder();
            local_sp_9 = var_135;
            **(uint32_t **)(local_sp_9 + 24UL) = 12U;
            r1012_7 = r1012_6;
            r914_2 = r914_1;
            r8_5 = r8_4;
            mrv.field_0 = rax_1;
            mrv1 = mrv;
            mrv1.field_1 = r1012_7;
            mrv2 = mrv1;
            mrv2.field_2 = r914_2;
            mrv3 = mrv2;
            mrv3.field_3 = r8_5;
            return mrv3;
        }
        var_72 = var_0 + (-128L);
        *(uint64_t *)var_72 = 4261364UL;
        indirect_placeholder();
        local_sp_3 = var_72;
    }
    storemerge_in_in = local_sp_3 + 8UL;
    storemerge_in = (uint64_t *)storemerge_in_in;
    storemerge = *storemerge_in;
    var_73 = (unsigned char *)(var_58 + 104UL);
    var_74 = *var_73;
    var_75 = (uint64_t *)(var_58 + 80UL);
    *var_75 = storemerge;
    *var_73 = ((var_74 & '\xf0') | ((unsigned char)var_59 & '\x0f'));
    var_76 = *var_16;
    var_77 = helper_cc_compute_all_wrapper(var_76, 0UL, 0UL, 25U);
    local_sp_8 = local_sp_3;
    local_sp_4_ph = local_sp_3;
    _pre_phi330 = storemerge_in;
    rcx15_0_ph = var_76;
    r914_1 = storemerge;
    r914_2 = storemerge;
    if ((uint64_t)(((unsigned char)(var_77 >> 4UL) ^ (unsigned char)var_77) & '\xc0') == 0UL) {
        *(uint64_t *)(local_sp_3 + 16UL) = r14_4;
        var_78 = var_59 & 1UL;
        var_79 = var_59 & 2UL;
        *(uint32_t *)storemerge_in_in = ((uint32_t)var_59 & 4U);
        var_80 = (uint64_t *)(rdx + 16UL);
        var_81 = (uint64_t *)rsi;
        var_82 = (uint64_t *)(var_58 + 24UL);
        r1012_1_ph = var_78;
        r11_0_ph = var_79;
        while (1U)
            {
                r11_2 = r11_0_ph;
                r1012_2 = r1012_1_ph;
                rdi13_2 = rdi13_0_ph;
                r1012_3 = r1012_1_ph;
                local_sp_6 = local_sp_4_ph;
                rbp_0 = rbp_0_ph;
                r1012_4 = r1012_1_ph;
                rdi13_1 = rdi13_0_ph;
                local_sp_5 = local_sp_4_ph;
                r11_1 = r11_0_ph;
                local_sp_7 = local_sp_4_ph;
                r8_2 = r8_1_ph;
                while (1U)
                    {
                        var_83 = (*(uint64_t *)((rbp_0 << 3UL) + *var_80) << 4UL) + *var_81;
                        var_84 = var_83 + 8UL;
                        var_85 = *(uint32_t *)var_84;
                        var_86 = *(unsigned char *)var_84;
                        var_87 = (uint64_t)(var_85 >> 8U);
                        var_88 = ((uint64_t)((uint16_t)var_87 & (unsigned short)1023U) == 0UL);
                        if (!var_88) {
                            loop_state_var = 1U;
                            break;
                        }
                        var_89 = (uint32_t)(uint64_t)var_86;
                        _pre_phi334 = var_89;
                        if ((uint64_t)(var_89 + (-1)) != 0UL) {
                            loop_state_var = 0U;
                            break;
                        }
                        var_125 = rbp_0 + 1UL;
                        rbp_0 = var_125;
                        if ((long)var_125 < (long)rcx15_0_ph) {
                            continue;
                        }
                        loop_state_var = 2U;
                        break;
                    }
                switch_state_var = 0;
                switch (loop_state_var) {
                  case 2U:
                    {
                        loop_state_var = 2U;
                        switch_state_var = 1;
                        break;
                    }
                    break;
                  case 1U:
                  case 0U:
                    {
                        switch (loop_state_var) {
                          case 1U:
                            {
                                _pre_phi334 = (uint32_t)(uint64_t)var_86;
                            }
                            break;
                          case 0U:
                            {
                            }
                            break;
                        }
                        var_90 = *var_73;
                        var_91 = (uint64_t)(*(unsigned char *)(var_83 + 10UL) >> '\x04');
                        var_92 = var_90 & '\xdf';
                        var_93 = (uint64_t)var_90 >> 5UL;
                        var_94 = (unsigned char)(((var_93 | var_91) << 5UL) & 32UL) | var_92;
                        var_95 = (uint64_t)(_pre_phi334 + (-2));
                        *var_73 = var_94;
                        r8_0 = var_93;
                        r8_1_ph = var_93;
                        r8_2 = var_93;
                        r8_4 = var_93;
                        if (var_95 == 0UL) {
                            *var_73 = (var_94 | '\x10');
                        } else {
                            if ((uint64_t)(_pre_phi334 + (-4)) == 0UL) {
                                *var_73 = (var_94 | '@');
                            }
                        }
                        if (var_88) {
                            var_123 = *var_16;
                            var_124 = rbp_0 + 1UL;
                            local_sp_4_ph = local_sp_6;
                            r11_0_ph = r11_2;
                            r1012_1_ph = r1012_3;
                            rdi13_0_ph = rdi13_2;
                            rbp_0_ph = var_124;
                            rcx15_0_ph = var_123;
                            r1012_4 = r1012_3;
                            local_sp_7 = local_sp_6;
                            if ((long)var_124 < (long)var_123) {
                                continue;
                            }
                            loop_state_var = 2U;
                            switch_state_var = 1;
                            break;
                        }
                        if (storemerge != *var_75) {
                            *(uint32_t *)(local_sp_4_ph + 40UL) = (uint32_t)r11_0_ph;
                            var_96 = (uint32_t *)(local_sp_4_ph + 32UL);
                            *var_96 = (uint32_t)r1012_1_ph;
                            var_97 = local_sp_4_ph + (-8L);
                            *(uint64_t *)var_97 = 4261908UL;
                            var_98 = indirect_placeholder_30(24UL);
                            var_99 = var_98.field_0;
                            *var_75 = var_99;
                            var_100 = local_sp_4_ph + 24UL;
                            var_101 = (uint32_t *)var_100;
                            var_102 = *var_101;
                            var_103 = *var_96;
                            local_sp_2 = var_97;
                            rdi13_1 = 0UL;
                            if (var_99 != 0UL) {
                                var_122 = (uint64_t)var_102;
                                *(uint64_t *)local_sp_4_ph = storemerge;
                                r1012_0 = var_122;
                                loop_state_var = 0U;
                                switch_state_var = 1;
                                break;
                            }
                            var_104 = *var_16;
                            *(uint64_t *)(var_99 + 8UL) = var_104;
                            if ((long)*var_16 > (long)0UL) {
                                *var_96 = var_103;
                                *var_101 = var_102;
                                var_105 = local_sp_4_ph + (-16L);
                                *(uint64_t *)var_105 = 4262357UL;
                                indirect_placeholder();
                                local_sp_1 = var_105;
                            } else {
                                var_106 = var_104 << 3UL;
                                *(uint64_t *)var_99 = var_104;
                                *(uint32_t *)(local_sp_4_ph + 36UL) = var_103;
                                *var_96 = var_102;
                                *(uint64_t *)var_100 = var_99;
                                var_107 = local_sp_4_ph + (-16L);
                                *(uint64_t *)var_107 = 4261985UL;
                                var_108 = indirect_placeholder_29(var_106);
                                var_109 = var_108.field_0;
                                var_110 = local_sp_4_ph + 16UL;
                                var_111 = *(uint64_t *)var_110;
                                var_112 = *var_101;
                                var_113 = *(uint32_t *)(local_sp_4_ph + 28UL);
                                *(uint64_t *)(var_111 + 16UL) = var_109;
                                local_sp_9 = var_107;
                                if (var_109 != 0UL) {
                                    var_121 = (uint64_t)var_112;
                                    *(uint64_t *)(var_111 + 8UL) = 0UL;
                                    *(uint64_t *)var_111 = 0UL;
                                    r1012_6 = var_121;
                                    loop_state_var = 1U;
                                    switch_state_var = 1;
                                    break;
                                }
                                *var_101 = var_113;
                                *(uint32_t *)var_110 = var_112;
                                var_114 = local_sp_4_ph + (-24L);
                                *(uint64_t *)var_114 = 4262047UL;
                                indirect_placeholder();
                                local_sp_1 = var_114;
                            }
                            storemerge5 = (uint64_t)*(uint32_t *)(local_sp_1 + 32UL);
                            storemerge4 = (uint64_t)*(uint32_t *)(local_sp_1 + 40UL);
                            *var_73 = (*var_73 | '\x80');
                            r1012_2 = storemerge5;
                            local_sp_5 = local_sp_1;
                            r11_1 = storemerge4;
                        }
                        r1012_3 = r1012_2;
                        rdi13_2 = rdi13_1;
                        local_sp_6 = local_sp_5;
                        r11_2 = r11_1;
                        if ((var_87 & 1UL) == 0UL) {
                            if (!(((var_87 & 2UL) == 0UL) || ((uint64_t)(uint32_t)r1012_2 == 0UL)) && !(((var_87 & 16UL) != 0UL) && ((uint64_t)(uint32_t)r11_1 == 0UL))) {
                                if ((var_87 & 64UL) != 0UL) {
                                    var_123 = *var_16;
                                    var_124 = rbp_0 + 1UL;
                                    local_sp_4_ph = local_sp_6;
                                    r11_0_ph = r11_2;
                                    r1012_1_ph = r1012_3;
                                    rdi13_0_ph = rdi13_2;
                                    rbp_0_ph = var_124;
                                    rcx15_0_ph = var_123;
                                    r1012_4 = r1012_3;
                                    local_sp_7 = local_sp_6;
                                    if ((long)var_124 < (long)var_123) {
                                        continue;
                                    }
                                    loop_state_var = 2U;
                                    switch_state_var = 1;
                                    break;
                                }
                                if (*(uint32_t *)(local_sp_5 + 8UL) != 0U) {
                                    var_123 = *var_16;
                                    var_124 = rbp_0 + 1UL;
                                    local_sp_4_ph = local_sp_6;
                                    r11_0_ph = r11_2;
                                    r1012_1_ph = r1012_3;
                                    rdi13_0_ph = rdi13_2;
                                    rbp_0_ph = var_124;
                                    rcx15_0_ph = var_123;
                                    r1012_4 = r1012_3;
                                    local_sp_7 = local_sp_6;
                                    if ((long)var_124 < (long)var_123) {
                                        continue;
                                    }
                                    loop_state_var = 2U;
                                    switch_state_var = 1;
                                    break;
                                }
                            }
                        }
                        if (!(((uint64_t)(uint32_t)r1012_2 != 0UL) && ((var_87 & 2UL) == 0UL))) {
                            if (!(((var_87 & 16UL) != 0UL) && ((uint64_t)(uint32_t)r11_1 == 0UL))) {
                                if ((var_87 & 64UL) == 0UL) {
                                    var_123 = *var_16;
                                    var_124 = rbp_0 + 1UL;
                                    local_sp_4_ph = local_sp_6;
                                    r11_0_ph = r11_2;
                                    r1012_1_ph = r1012_3;
                                    rdi13_0_ph = rdi13_2;
                                    rbp_0_ph = var_124;
                                    rcx15_0_ph = var_123;
                                    r1012_4 = r1012_3;
                                    local_sp_7 = local_sp_6;
                                    if ((long)var_124 < (long)var_123) {
                                        continue;
                                    }
                                    loop_state_var = 2U;
                                    switch_state_var = 1;
                                    break;
                                }
                                if (*(uint32_t *)(local_sp_5 + 8UL) == 0U) {
                                    var_123 = *var_16;
                                    var_124 = rbp_0 + 1UL;
                                    local_sp_4_ph = local_sp_6;
                                    r11_0_ph = r11_2;
                                    r1012_1_ph = r1012_3;
                                    rdi13_0_ph = rdi13_2;
                                    rbp_0_ph = var_124;
                                    rcx15_0_ph = var_123;
                                    r1012_4 = r1012_3;
                                    local_sp_7 = local_sp_6;
                                    if ((long)var_124 < (long)var_123) {
                                        continue;
                                    }
                                    loop_state_var = 2U;
                                    switch_state_var = 1;
                                    break;
                                }
                            }
                            var_115 = rbp_0 - rdi13_1;
                            rax_0 = var_115;
                            var_116 = *var_65;
                            var_117 = var_116 + (-1L);
                            *var_65 = var_117;
                            if ((long)var_115 >= (long)0UL & (long)var_115 >= (long)var_116 & (long)var_115 >= (long)var_117) {
                                var_118 = (rax_0 << 3UL) + *var_82;
                                *(uint64_t *)var_118 = *(uint64_t *)(var_118 + 8UL);
                                var_119 = rax_0 + 1UL;
                                rax_0 = var_119;
                                do {
                                    var_118 = (rax_0 << 3UL) + *var_82;
                                    *(uint64_t *)var_118 = *(uint64_t *)(var_118 + 8UL);
                                    var_119 = rax_0 + 1UL;
                                    rax_0 = var_119;
                                } while ((long)var_119 >= (long)*var_65);
                            }
                            var_120 = rdi13_1 + 1UL;
                            rdi13_2 = var_120;
                            var_123 = *var_16;
                            var_124 = rbp_0 + 1UL;
                            local_sp_4_ph = local_sp_6;
                            r11_0_ph = r11_2;
                            r1012_1_ph = r1012_3;
                            rdi13_0_ph = rdi13_2;
                            rbp_0_ph = var_124;
                            rcx15_0_ph = var_123;
                            r1012_4 = r1012_3;
                            local_sp_7 = local_sp_6;
                            if ((long)var_124 < (long)var_123) {
                                continue;
                            }
                            loop_state_var = 2U;
                            switch_state_var = 1;
                            break;
                        }
                        var_115 = rbp_0 - rdi13_1;
                        rax_0 = var_115;
                        var_116 = *var_65;
                        var_117 = var_116 + (-1L);
                        *var_65 = var_117;
                        if ((long)var_115 >= (long)0UL & (long)var_115 >= (long)var_116 & (long)var_115 >= (long)var_117) {
                            var_118 = (rax_0 << 3UL) + *var_82;
                            *(uint64_t *)var_118 = *(uint64_t *)(var_118 + 8UL);
                            var_119 = rax_0 + 1UL;
                            rax_0 = var_119;
                            do {
                                var_118 = (rax_0 << 3UL) + *var_82;
                                *(uint64_t *)var_118 = *(uint64_t *)(var_118 + 8UL);
                                var_119 = rax_0 + 1UL;
                                rax_0 = var_119;
                            } while ((long)var_119 >= (long)*var_65);
                        }
                        var_120 = rdi13_1 + 1UL;
                        rdi13_2 = var_120;
                    }
                    break;
                }
                if (switch_state_var)
                    break;
            }
        switch (loop_state_var) {
          case 2U:
            {
                var_126 = *(uint64_t *)(local_sp_7 + 16UL);
                _pre329 = (uint64_t *)(local_sp_7 + 8UL);
                local_sp_8 = local_sp_7;
                _pre_phi330 = _pre329;
                r1012_5 = r1012_4;
                r14_0 = var_126;
                r8_3 = r8_2;
                *_pre_phi330 = storemerge;
                var_127 = local_sp_8 + (-8L);
                *(uint64_t *)var_127 = 4261740UL;
                var_128 = indirect_placeholder_5(rsi, r14_0, var_58);
                r1012_0 = r1012_5;
                local_sp_2 = var_127;
                r8_0 = r8_3;
                r1012_7 = r1012_5;
                r8_5 = r8_3;
                if ((uint64_t)(uint32_t)var_128 == 0UL) {
                    mrv.field_0 = rax_1;
                    mrv1 = mrv;
                    mrv1.field_1 = r1012_7;
                    mrv2 = mrv1;
                    mrv2.field_2 = r914_2;
                    mrv3 = mrv2;
                    mrv3.field_3 = r8_5;
                    return mrv3;
                }
            }
            break;
          case 1U:
          case 0U:
            {
                **(uint32_t **)(local_sp_9 + 24UL) = 12U;
                r1012_7 = r1012_6;
                r914_2 = r914_1;
                r8_5 = r8_4;
                mrv.field_0 = rax_1;
                mrv1 = mrv;
                mrv1.field_1 = r1012_7;
                mrv2 = mrv1;
                mrv2.field_2 = r914_2;
                mrv3 = mrv2;
                mrv3.field_3 = r8_5;
                return mrv3;
            }
            break;
        }
    }
    *_pre_phi330 = storemerge;
    var_127 = local_sp_8 + (-8L);
    *(uint64_t *)var_127 = 4261740UL;
    var_128 = indirect_placeholder_5(rsi, r14_0, var_58);
    r1012_0 = r1012_5;
    local_sp_2 = var_127;
    r8_0 = r8_3;
    r1012_7 = r1012_5;
    r8_5 = r8_3;
    if ((uint64_t)(uint32_t)var_128 == 0UL) {
        mrv.field_0 = rax_1;
        mrv1 = mrv;
        mrv1.field_1 = r1012_7;
        mrv2 = mrv1;
        mrv2.field_2 = r914_2;
        mrv3 = mrv2;
        mrv3.field_3 = r8_5;
        return mrv3;
    }
    var_129 = (uint64_t *)(local_sp_2 + (-8L));
    *var_129 = 4262151UL;
    indirect_placeholder();
    var_130 = local_sp_2 + (-16L);
    *(uint64_t *)var_130 = 4262161UL;
    indirect_placeholder();
    var_131 = *var_75;
    var_132 = *var_129;
    local_sp_0 = var_130;
    r1012_6 = r1012_0;
    r914_1 = var_132;
    r8_4 = r8_0;
    if (var_132 != var_131) {
        *(uint64_t *)(local_sp_2 + (-24L)) = 4262185UL;
        indirect_placeholder();
        var_133 = local_sp_2 + (-32L);
        *(uint64_t *)var_133 = 4262195UL;
        indirect_placeholder();
        local_sp_0 = var_133;
    }
    *(uint64_t *)(local_sp_0 + (-8L)) = 4262205UL;
    indirect_placeholder();
    *(uint64_t *)(local_sp_0 + (-16L)) = 4262215UL;
    indirect_placeholder();
    *(uint64_t *)(local_sp_0 + (-24L)) = 4262225UL;
    indirect_placeholder();
    var_134 = local_sp_0 + (-32L);
    *(uint64_t *)var_134 = 4262233UL;
    indirect_placeholder();
    local_sp_9 = var_134;
}

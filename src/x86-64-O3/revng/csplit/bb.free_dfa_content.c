typedef unsigned char __u_char;
typedef unsigned short __u_short;
typedef unsigned int __u_int;
typedef unsigned long __u_long;
typedef signed char __int8_t;
typedef unsigned char __uint8_t;
typedef short __int16_t;
typedef unsigned short __uint16_t;
typedef int __int32_t;
typedef unsigned int __uint32_t;
typedef long __int64_t;
typedef unsigned long __uint64_t;
typedef __int8_t __int_least8_t;
typedef __uint8_t __uint_least8_t;
typedef __int16_t __int_least16_t;
typedef __uint16_t __uint_least16_t;
typedef __int32_t __int_least32_t;
typedef __uint32_t __uint_least32_t;
typedef __int64_t __int_least64_t;
typedef __uint64_t __uint_least64_t;
typedef long __quad_t;
typedef unsigned long __u_quad_t;
typedef long __intmax_t;
typedef unsigned long __uintmax_t;
typedef unsigned long __dev_t;
typedef unsigned int __uid_t;
typedef unsigned int __gid_t;
typedef unsigned long __ino_t;
typedef unsigned long __ino64_t;
typedef unsigned int __mode_t;
typedef unsigned long __nlink_t;
typedef long __off_t;
typedef long __off64_t;
typedef int __pid_t;
typedef struct {
    int __val[2];
} __fsid_t;
typedef long __clock_t;
typedef unsigned long __rlim_t;
typedef unsigned long __rlim64_t;
typedef unsigned int __id_t;
typedef long __time_t;
typedef unsigned int __useconds_t;
typedef long __suseconds_t;
typedef int __daddr_t;
typedef int __key_t;
typedef int __clockid_t;
typedef void *__timer_t;
typedef long __blksize_t;
typedef long __blkcnt_t;
typedef long __blkcnt64_t;
typedef unsigned long __fsblkcnt_t;
typedef unsigned long __fsblkcnt64_t;
typedef unsigned long __fsfilcnt_t;
typedef unsigned long __fsfilcnt64_t;
typedef long __fsword_t;
typedef long __ssize_t;
typedef long __syscall_slong_t;
typedef unsigned long __syscall_ulong_t;
typedef __off64_t __loff_t;
typedef char *__caddr_t;
typedef long __intptr_t;
typedef unsigned int __socklen_t;
typedef int __sig_atomic_t;
typedef __int8_t int8_t;
typedef __int16_t int16_t;
typedef __int32_t int32_t;
typedef __int64_t int64_t;
typedef __uint8_t uint8_t;
typedef __uint16_t uint16_t;
typedef __uint32_t uint32_t;
typedef __uint64_t uint64_t;
typedef __int_least8_t int_least8_t;
typedef __int_least16_t int_least16_t;
typedef __int_least32_t int_least32_t;
typedef __int_least64_t int_least64_t;
typedef __uint_least8_t uint_least8_t;
typedef __uint_least16_t uint_least16_t;
typedef __uint_least32_t uint_least32_t;
typedef __uint_least64_t uint_least64_t;
typedef signed char int_fast8_t;
typedef long int_fast16_t;
typedef long int_fast32_t;
typedef long int_fast64_t;
typedef unsigned char uint_fast8_t;
typedef unsigned long uint_fast16_t;
typedef unsigned long uint_fast32_t;
typedef unsigned long uint_fast64_t;
typedef long intptr_t;
typedef unsigned long uintptr_t;
typedef __intmax_t intmax_t;
typedef __uintmax_t uintmax_t;
extern uint64_t revng_init_local_sp(uint64_t param_0);
extern uint64_t init_rbx(void);
extern uint64_t init_r12(void);
extern uint64_t init_rbp(void);
extern void indirect_placeholder(void);
extern uint64_t init_cc_src2(void);
extern uint64_t helper_cc_compute_c_wrapper(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint32_t param_3);
extern uint64_t init_r13(void);
extern uint64_t init_r14(void);
extern uint64_t init_r15(void);
void bb_free_dfa_content(uint64_t rdi) {
    uint64_t var_0;
    uint64_t var_1;
    uint64_t var_2;
    uint64_t var_3;
    uint64_t var_4;
    uint64_t var_5;
    uint64_t var_6;
    uint64_t var_7;
    uint64_t var_8;
    uint64_t *var_9;
    uint64_t var_10;
    uint64_t *_pre_phi88;
    uint64_t var_43;
    uint64_t local_sp_0;
    uint64_t rdi1_0;
    uint64_t r15_0;
    uint64_t var_37;
    uint64_t rbp_0;
    uint64_t local_sp_1;
    uint64_t local_sp_3;
    uint64_t var_38;
    uint64_t var_39;
    uint64_t local_sp_4;
    uint64_t *var_33;
    uint64_t local_sp_2;
    uint64_t r14_0;
    uint64_t var_34;
    uint64_t *_cast;
    uint64_t rdx_0;
    uint64_t var_35;
    uint64_t var_36;
    uint64_t var_40;
    uint64_t var_41;
    uint64_t local_sp_10;
    uint64_t local_sp_5;
    uint64_t var_42;
    uint64_t *var_22;
    uint64_t *var_23;
    uint64_t local_sp_9;
    uint64_t rbp_1;
    uint64_t local_sp_14;
    uint64_t local_sp_6;
    uint64_t local_sp_7;
    uint64_t var_24;
    uint64_t local_sp_8;
    uint64_t var_25;
    uint64_t var_26;
    uint64_t var_27;
    uint64_t var_28;
    uint64_t var_29;
    uint64_t var_30;
    uint64_t *var_31;
    uint64_t var_32;
    uint64_t *var_11;
    uint64_t var_12;
    uint64_t rcx_2;
    uint64_t rbx_2;
    uint64_t rbx_1;
    uint64_t local_sp_13;
    uint64_t rcx_0;
    uint64_t local_sp_11;
    uint32_t var_13;
    uint64_t rcx_1;
    uint64_t var_14;
    uint64_t var_15;
    uint64_t var_16;
    uint64_t var_17;
    uint64_t local_sp_12;
    uint64_t var_19;
    uint64_t var_20;
    uint64_t var_18;
    uint64_t var_21;
    var_0 = revng_init_local_sp(0UL);
    var_1 = init_r15();
    var_2 = init_r13();
    var_3 = init_rbx();
    var_4 = init_r12();
    var_5 = init_rbp();
    var_6 = init_cc_src2();
    var_7 = init_r14();
    *(uint64_t *)(var_0 + (-8L)) = var_1;
    *(uint64_t *)(var_0 + (-16L)) = var_7;
    *(uint64_t *)(var_0 + (-24L)) = var_2;
    *(uint64_t *)(var_0 + (-32L)) = var_4;
    *(uint64_t *)(var_0 + (-40L)) = var_5;
    *(uint64_t *)(var_0 + (-48L)) = var_3;
    var_8 = var_0 + (-56L);
    var_9 = (uint64_t *)rdi;
    var_10 = *var_9;
    r15_0 = 0UL;
    rbp_0 = 0UL;
    r14_0 = 0UL;
    rdx_0 = var_10;
    rbp_1 = 0UL;
    local_sp_14 = var_8;
    rbx_1 = 0UL;
    local_sp_11 = var_8;
    if (var_10 == 0UL) {
        _pre_phi88 = (uint64_t *)(rdi + 16UL);
    } else {
        var_11 = (uint64_t *)(rdi + 16UL);
        var_12 = *var_11;
        rcx_0 = var_12;
        _pre_phi88 = var_11;
        if (var_12 == 0UL) {
            while (1U)
                {
                    var_13 = *(uint32_t *)((rdx_0 + (rbx_1 << 4UL)) + 8UL) & 262399U;
                    rcx_1 = rcx_0;
                    local_sp_12 = local_sp_11;
                    if ((uint64_t)(var_13 + (-6)) != 0UL) {
                        if ((uint64_t)(var_13 + (-3)) != 0UL) {
                            var_14 = rbx_1 + 1UL;
                            var_15 = local_sp_11 + (-8L);
                            *(uint64_t *)var_15 = 4251593UL;
                            indirect_placeholder();
                            var_16 = *var_11;
                            var_17 = helper_cc_compute_c_wrapper(var_14 - var_16, var_16, var_6, 17U);
                            rbx_2 = var_14;
                            rcx_2 = var_16;
                            local_sp_13 = var_15;
                            local_sp_14 = var_15;
                            if (var_17 == 0UL) {
                                break;
                            }
                            rbx_1 = rbx_2;
                            rcx_0 = rcx_2;
                            local_sp_11 = local_sp_13;
                            rdx_0 = *var_9;
                            continue;
                        }
                    }
                    *(uint64_t *)(local_sp_11 + (-8L)) = 4251988UL;
                    indirect_placeholder();
                    *(uint64_t *)(local_sp_11 + (-16L)) = 4251997UL;
                    indirect_placeholder();
                    var_18 = local_sp_11 + (-24L);
                    *(uint64_t *)var_18 = 4252005UL;
                    indirect_placeholder();
                    rcx_1 = *var_11;
                    local_sp_12 = var_18;
                }
        }
    }
    var_21 = local_sp_14 + (-8L);
    *(uint64_t *)var_21 = 4251621UL;
    indirect_placeholder();
    local_sp_6 = var_21;
    local_sp_10 = var_21;
    if (*_pre_phi88 != 0UL) {
        var_22 = (uint64_t *)(rdi + 56UL);
        var_23 = (uint64_t *)(rdi + 40UL);
        var_27 = rbp_1 + 1UL;
        var_28 = *_pre_phi88;
        var_29 = helper_cc_compute_c_wrapper(var_27 - var_28, var_28, var_6, 17U);
        rbp_1 = var_27;
        local_sp_6 = local_sp_9;
        local_sp_10 = local_sp_9;
        do {
            local_sp_7 = local_sp_6;
            if (*(uint64_t *)(rdi + 48UL) == 0UL) {
                var_24 = local_sp_6 + (-8L);
                *(uint64_t *)var_24 = 4251651UL;
                indirect_placeholder();
                local_sp_7 = var_24;
            }
            local_sp_8 = local_sp_7;
            if (*var_22 == 0UL) {
                var_25 = local_sp_7 + (-8L);
                *(uint64_t *)var_25 = 4251670UL;
                indirect_placeholder();
                local_sp_8 = var_25;
            }
            local_sp_9 = local_sp_8;
            if (*var_23 == 0UL) {
                var_26 = local_sp_8 + (-8L);
                *(uint64_t *)var_26 = 4251689UL;
                indirect_placeholder();
                local_sp_9 = var_26;
            }
            var_27 = rbp_1 + 1UL;
            var_28 = *_pre_phi88;
            var_29 = helper_cc_compute_c_wrapper(var_27 - var_28, var_28, var_6, 17U);
            rbp_1 = var_27;
            local_sp_6 = local_sp_9;
            local_sp_10 = local_sp_9;
        } while (var_29 != 0UL);
    }
    *(uint64_t *)(local_sp_10 + (-8L)) = 4251712UL;
    indirect_placeholder();
    *(uint64_t *)(local_sp_10 + (-16L)) = 4251721UL;
    indirect_placeholder();
    *(uint64_t *)(local_sp_10 + (-24L)) = 4251730UL;
    indirect_placeholder();
    var_30 = local_sp_10 + (-32L);
    *(uint64_t *)var_30 = 4251739UL;
    indirect_placeholder();
    var_31 = (uint64_t *)(rdi + 64UL);
    var_32 = *var_31;
    rdi1_0 = var_32;
    local_sp_2 = var_30;
    local_sp_5 = var_30;
    if (var_32 != 0UL) {
        var_33 = (uint64_t *)(rdi + 136UL);
        while (1U)
            {
                var_34 = r15_0 + rdi1_0;
                _cast = (uint64_t *)var_34;
                local_sp_3 = local_sp_2;
                local_sp_4 = local_sp_2;
                if ((long)*_cast <= (long)0UL) {
                    var_38 = rbp_0 + 1UL;
                    *(uint64_t *)(local_sp_1 + (-8L)) = 4251847UL;
                    indirect_placeholder();
                    *(uint64_t *)(local_sp_1 + (-16L)) = 4251856UL;
                    indirect_placeholder();
                    *(uint64_t *)(local_sp_1 + (-24L)) = 4251865UL;
                    indirect_placeholder();
                    var_39 = local_sp_1 + (-32L);
                    *(uint64_t *)var_39 = 4251873UL;
                    indirect_placeholder();
                    rbp_0 = var_38;
                    local_sp_3 = var_39;
                    local_sp_4 = var_39;
                    do {
                        var_35 = *(uint64_t *)((rbp_0 << 3UL) + *(uint64_t *)(var_34 + 16UL));
                        *(uint64_t *)(local_sp_3 + (-8L)) = 4251794UL;
                        indirect_placeholder();
                        var_36 = local_sp_3 + (-16L);
                        *(uint64_t *)var_36 = 4251803UL;
                        indirect_placeholder();
                        local_sp_1 = var_36;
                        if (*(uint64_t *)(var_35 + 80UL) == (var_35 + 8UL)) {
                            *(uint64_t *)(local_sp_3 + (-24L)) = 4251825UL;
                            indirect_placeholder();
                            var_37 = local_sp_3 + (-32L);
                            *(uint64_t *)var_37 = 4251834UL;
                            indirect_placeholder();
                            local_sp_1 = var_37;
                        }
                        var_38 = rbp_0 + 1UL;
                        *(uint64_t *)(local_sp_1 + (-8L)) = 4251847UL;
                        indirect_placeholder();
                        *(uint64_t *)(local_sp_1 + (-16L)) = 4251856UL;
                        indirect_placeholder();
                        *(uint64_t *)(local_sp_1 + (-24L)) = 4251865UL;
                        indirect_placeholder();
                        var_39 = local_sp_1 + (-32L);
                        *(uint64_t *)var_39 = 4251873UL;
                        indirect_placeholder();
                        rbp_0 = var_38;
                        local_sp_3 = var_39;
                        local_sp_4 = var_39;
                    } while ((long)*_cast <= (long)var_38);
                }
                var_40 = r14_0 + 1UL;
                var_41 = local_sp_4 + (-8L);
                *(uint64_t *)var_41 = 4251897UL;
                indirect_placeholder();
                local_sp_2 = var_41;
                r14_0 = var_40;
                local_sp_5 = var_41;
                if (var_40 > *var_33) {
                    break;
                }
                r15_0 = r15_0 + 24UL;
                rdi1_0 = *var_31;
                continue;
            }
    }
    var_42 = local_sp_5 + (-8L);
    *(uint64_t *)var_42 = 4251924UL;
    indirect_placeholder();
    local_sp_0 = var_42;
    if (*(uint64_t *)(rdi + 120UL) == 4357952UL) {
        *(uint64_t *)(local_sp_0 + (-8L)) = 4251954UL;
        indirect_placeholder();
        indirect_placeholder();
        return;
    }
    var_43 = local_sp_5 + (-16L);
    *(uint64_t *)var_43 = 4251942UL;
    indirect_placeholder();
    local_sp_0 = var_43;
    *(uint64_t *)(local_sp_0 + (-8L)) = 4251954UL;
    indirect_placeholder();
    indirect_placeholder();
    return;
}

typedef unsigned char __u_char;
typedef unsigned short __u_short;
typedef unsigned int __u_int;
typedef unsigned long __u_long;
typedef signed char __int8_t;
typedef unsigned char __uint8_t;
typedef short __int16_t;
typedef unsigned short __uint16_t;
typedef int __int32_t;
typedef unsigned int __uint32_t;
typedef long __int64_t;
typedef unsigned long __uint64_t;
typedef __int8_t __int_least8_t;
typedef __uint8_t __uint_least8_t;
typedef __int16_t __int_least16_t;
typedef __uint16_t __uint_least16_t;
typedef __int32_t __int_least32_t;
typedef __uint32_t __uint_least32_t;
typedef __int64_t __int_least64_t;
typedef __uint64_t __uint_least64_t;
typedef long __quad_t;
typedef unsigned long __u_quad_t;
typedef long __intmax_t;
typedef unsigned long __uintmax_t;
typedef unsigned long __dev_t;
typedef unsigned int __uid_t;
typedef unsigned int __gid_t;
typedef unsigned long __ino_t;
typedef unsigned long __ino64_t;
typedef unsigned int __mode_t;
typedef unsigned long __nlink_t;
typedef long __off_t;
typedef long __off64_t;
typedef int __pid_t;
typedef struct {
    int __val[2];
} __fsid_t;
typedef long __clock_t;
typedef unsigned long __rlim_t;
typedef unsigned long __rlim64_t;
typedef unsigned int __id_t;
typedef long __time_t;
typedef unsigned int __useconds_t;
typedef long __suseconds_t;
typedef int __daddr_t;
typedef int __key_t;
typedef int __clockid_t;
typedef void *__timer_t;
typedef long __blksize_t;
typedef long __blkcnt_t;
typedef long __blkcnt64_t;
typedef unsigned long __fsblkcnt_t;
typedef unsigned long __fsblkcnt64_t;
typedef unsigned long __fsfilcnt_t;
typedef unsigned long __fsfilcnt64_t;
typedef long __fsword_t;
typedef long __ssize_t;
typedef long __syscall_slong_t;
typedef unsigned long __syscall_ulong_t;
typedef __off64_t __loff_t;
typedef char *__caddr_t;
typedef long __intptr_t;
typedef unsigned int __socklen_t;
typedef int __sig_atomic_t;
typedef __int8_t int8_t;
typedef __int16_t int16_t;
typedef __int32_t int32_t;
typedef __int64_t int64_t;
typedef __uint8_t uint8_t;
typedef __uint16_t uint16_t;
typedef __uint32_t uint32_t;
typedef __uint64_t uint64_t;
typedef __int_least8_t int_least8_t;
typedef __int_least16_t int_least16_t;
typedef __int_least32_t int_least32_t;
typedef __int_least64_t int_least64_t;
typedef __uint_least8_t uint_least8_t;
typedef __uint_least16_t uint_least16_t;
typedef __uint_least32_t uint_least32_t;
typedef __uint_least64_t uint_least64_t;
typedef signed char int_fast8_t;
typedef long int_fast16_t;
typedef long int_fast32_t;
typedef long int_fast64_t;
typedef unsigned char uint_fast8_t;
typedef unsigned long uint_fast16_t;
typedef unsigned long uint_fast32_t;
typedef unsigned long uint_fast64_t;
typedef long intptr_t;
typedef unsigned long uintptr_t;
typedef __intmax_t intmax_t;
typedef __uintmax_t uintmax_t;
struct bb_re_acquire_state_ret_type;
struct helper_pxor_xmm_wrapper_ret_type;
struct type_5;
struct type_7;
struct helper_paddq_xmm_wrapper_305_ret_type;
struct helper_psrldq_xmm_wrapper_ret_type;
struct helper_paddq_xmm_wrapper_304_ret_type;
struct indirect_placeholder_20_ret_type;
struct indirect_placeholder_21_ret_type;
struct bb_re_acquire_state_ret_type {
    uint64_t field_0;
    uint64_t field_1;
    uint64_t field_2;
};
struct helper_pxor_xmm_wrapper_ret_type {
    uint64_t field_0;
    uint64_t field_1;
};
struct type_5 {
};
struct type_7 {
};
struct helper_paddq_xmm_wrapper_305_ret_type {
    uint64_t field_0;
    uint64_t field_1;
};
struct helper_psrldq_xmm_wrapper_ret_type {
    uint64_t field_0;
    uint64_t field_1;
    uint64_t field_2;
    uint64_t field_3;
    uint64_t field_4;
    uint64_t field_5;
    uint64_t field_6;
    uint64_t field_7;
};
struct helper_paddq_xmm_wrapper_304_ret_type {
    uint64_t field_0;
    uint64_t field_1;
};
struct indirect_placeholder_20_ret_type {
    uint64_t field_0;
    uint64_t field_1;
};
struct indirect_placeholder_21_ret_type {
    uint64_t field_0;
    uint64_t field_1;
    uint64_t field_2;
    uint64_t field_3;
    uint64_t field_4;
    uint64_t field_5;
    uint64_t field_6;
};
extern uint64_t revng_init_local_sp(uint64_t param_0);
extern uint64_t init_rbx(void);
extern uint64_t init_r12(void);
extern uint64_t init_rbp(void);
extern void indirect_placeholder(void);
extern uint64_t init_state_0x8558(void);
extern uint64_t init_state_0x8560(void);
extern uint64_t init_cc_src2(void);
extern uint64_t helper_cc_compute_c_wrapper(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint32_t param_3);
extern uint64_t init_r13(void);
extern uint64_t init_r14(void);
extern uint64_t helper_cc_compute_all_wrapper(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint32_t param_3);
extern uint64_t init_r15(void);
extern struct helper_pxor_xmm_wrapper_ret_type helper_pxor_xmm_wrapper(struct type_5 *param_0, struct type_7 *param_1, struct type_7 *param_2, uint64_t param_3, uint64_t param_4);
extern uint64_t indirect_placeholder_5(uint64_t param_0, uint64_t param_1, uint64_t param_2);
extern uint64_t init_state_0x85a8(void);
extern uint64_t init_state_0x85b0(void);
extern uint64_t init_state_0x85b8(void);
extern uint64_t init_state_0x85c0(void);
extern uint64_t init_state_0x85c8(void);
extern uint64_t init_state_0x85d0(void);
extern struct helper_paddq_xmm_wrapper_305_ret_type helper_paddq_xmm_wrapper_305(struct type_5 *param_0, struct type_7 *param_1, struct type_7 *param_2, uint64_t param_3, uint64_t param_4, uint64_t param_5, uint64_t param_6);
extern struct helper_psrldq_xmm_wrapper_ret_type helper_psrldq_xmm_wrapper(struct type_5 *param_0, struct type_7 *param_1, struct type_7 *param_2, uint64_t param_3, uint64_t param_4, uint64_t param_5, uint64_t param_6, uint64_t param_7, uint64_t param_8, uint64_t param_9, uint64_t param_10, uint64_t param_11);
extern struct helper_paddq_xmm_wrapper_304_ret_type helper_paddq_xmm_wrapper_304(struct type_5 *param_0, struct type_7 *param_1, struct type_7 *param_2, uint64_t param_3, uint64_t param_4, uint64_t param_5, uint64_t param_6);
extern struct indirect_placeholder_20_ret_type indirect_placeholder_20(uint64_t param_0);
extern struct indirect_placeholder_21_ret_type indirect_placeholder_21(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint64_t param_3, uint64_t param_4, uint64_t param_5, uint64_t param_6, uint64_t param_7);
typedef _Bool bool;
struct bb_re_acquire_state_ret_type bb_re_acquire_state(uint64_t r10, uint64_t rdi, uint64_t r9, uint64_t rdx, uint64_t rsi, uint64_t r8) {
    struct helper_pxor_xmm_wrapper_ret_type var_29;
    uint64_t var_0;
    uint64_t var_1;
    uint64_t var_2;
    uint64_t var_3;
    uint64_t var_4;
    uint64_t var_5;
    uint64_t var_6;
    uint64_t var_7;
    uint64_t var_8;
    uint64_t var_9;
    uint64_t var_10;
    uint64_t var_11;
    uint64_t var_12;
    uint64_t var_13;
    uint64_t var_14;
    struct helper_paddq_xmm_wrapper_305_ret_type var_31;
    uint64_t var_15;
    uint64_t *var_16;
    uint64_t var_17;
    uint64_t var_95;
    uint64_t local_sp_0;
    uint64_t var_94;
    uint64_t local_sp_1;
    uint64_t var_69;
    uint64_t *var_70;
    uint64_t var_71;
    uint64_t r911_4;
    uint64_t var_72;
    uint64_t *var_73;
    unsigned char *var_74;
    uint64_t rcx_1;
    uint64_t rax_0_be;
    uint64_t rax_0;
    uint64_t r814_0;
    uint64_t r911_0;
    uint64_t rax_4;
    uint64_t rcx_0;
    uint64_t r109_8;
    uint64_t r911_5;
    uint64_t var_75;
    uint64_t var_76;
    uint32_t var_77;
    uint32_t *var_86;
    uint64_t r911_3;
    unsigned char var_87;
    uint64_t r911_1;
    unsigned char var_88;
    unsigned char var_78;
    uint64_t var_79;
    unsigned char var_80;
    uint64_t var_81;
    unsigned char var_82;
    uint64_t var_83;
    uint64_t var_91;
    uint64_t r911_2;
    uint64_t var_89;
    uint64_t var_90;
    uint64_t r911_0_be;
    uint64_t rcx_0_be;
    uint64_t var_84;
    uint64_t var_85;
    uint64_t var_92;
    uint64_t var_93;
    uint64_t var_68;
    uint64_t var_60;
    uint64_t var_61;
    uint64_t *var_62;
    uint64_t *var_64;
    uint64_t var_65;
    struct indirect_placeholder_20_ret_type var_66;
    uint64_t var_67;
    uint64_t var_63;
    uint64_t r911_8;
    uint64_t r814_2;
    uint64_t var_49;
    struct bb_re_acquire_state_ret_type mrv1 = {0UL, /*implicit*/(int)0, /*implicit*/(int)0};
    struct bb_re_acquire_state_ret_type mrv2;
    uint64_t var_18;
    uint64_t r109_3;
    uint64_t var_19;
    uint64_t var_20;
    uint64_t var_21;
    uint64_t rax_1;
    uint64_t rax_2;
    uint64_t var_22;
    uint64_t r109_0;
    uint64_t var_23;
    uint64_t var_24;
    uint64_t var_25;
    uint64_t r14_0;
    uint64_t r109_1;
    uint64_t r14_1;
    uint64_t var_26;
    uint64_t var_27;
    uint64_t var_28;
    uint64_t r109_2;
    uint64_t rax_3;
    uint64_t rcx_2;
    uint64_t state_0x8558_0;
    uint64_t state_0x8560_0;
    uint64_t var_30;
    uint64_t var_32;
    uint64_t var_33;
    uint64_t var_34;
    uint64_t var_35;
    struct helper_psrldq_xmm_wrapper_ret_type var_36;
    struct helper_paddq_xmm_wrapper_304_ret_type var_37;
    uint64_t var_38;
    uint64_t r14_2;
    uint64_t r911_6;
    uint64_t r14_3;
    uint64_t var_39;
    uint64_t var_40;
    uint64_t var_41;
    uint64_t var_42;
    uint64_t var_43;
    uint64_t var_44;
    uint64_t var_45;
    uint64_t *var_46;
    uint64_t r911_10;
    uint64_t r109_7;
    uint64_t r109_4;
    uint64_t r814_3;
    uint64_t r911_7;
    uint64_t rcx_3;
    uint64_t r814_1;
    uint64_t var_47;
    uint64_t *var_48;
    uint64_t r911_11;
    uint64_t r109_5;
    uint64_t r109_6;
    uint64_t r911_9;
    struct bb_re_acquire_state_ret_type mrv3;
    struct bb_re_acquire_state_ret_type mrv4;
    struct bb_re_acquire_state_ret_type mrv5;
    uint64_t var_50;
    uint64_t var_51;
    uint64_t var_52;
    uint64_t var_53;
    uint64_t rcx_4;
    uint64_t r814_4;
    struct indirect_placeholder_21_ret_type var_54;
    uint64_t var_55;
    uint64_t var_56;
    uint64_t var_57;
    uint64_t var_58;
    uint64_t var_59;
    struct bb_re_acquire_state_ret_type mrv7 = {0UL, /*implicit*/(int)0, /*implicit*/(int)0};
    struct bb_re_acquire_state_ret_type mrv8;
    unsigned int loop_state_var;
    bool switch_state_var;
    var_0 = revng_init_local_sp(0UL);
    var_1 = init_r15();
    var_2 = init_r13();
    var_3 = init_rbx();
    var_4 = init_r12();
    var_5 = init_rbp();
    var_6 = init_cc_src2();
    var_7 = init_state_0x8558();
    var_8 = init_state_0x8560();
    var_9 = init_r14();
    var_10 = init_state_0x85a8();
    var_11 = init_state_0x85b0();
    var_12 = init_state_0x85b8();
    var_13 = init_state_0x85c0();
    var_14 = init_state_0x85c8();
    var_15 = init_state_0x85d0();
    *(uint64_t *)(var_0 + (-8L)) = var_1;
    *(uint64_t *)(var_0 + (-16L)) = var_9;
    *(uint64_t *)(var_0 + (-24L)) = var_2;
    *(uint64_t *)(var_0 + (-32L)) = var_4;
    *(uint64_t *)(var_0 + (-40L)) = var_5;
    *(uint64_t *)(var_0 + (-48L)) = var_3;
    var_16 = (uint64_t *)(rdx + 8UL);
    var_17 = *var_16;
    rax_0 = 0UL;
    r814_0 = r8;
    r911_8 = var_17;
    r109_3 = r10;
    rax_1 = var_17;
    rax_2 = 0UL;
    r109_0 = 1UL;
    r109_1 = 0UL;
    r14_1 = var_17;
    rax_3 = 0UL;
    r911_6 = r9;
    r14_3 = var_17;
    rcx_3 = 0UL;
    if (var_17 == 0UL) {
        *(uint32_t *)rdi = 0U;
        mrv7.field_1 = r10;
        mrv8 = mrv7;
        mrv8.field_2 = r9;
        return mrv8;
    }
    var_18 = helper_cc_compute_all_wrapper(var_17, 0UL, var_6, 17U);
    if ((uint64_t)(((unsigned char)(var_18 >> 4UL) ^ (unsigned char)var_18) & '\xc0') != 0UL) {
        var_19 = *(uint64_t *)(rdx + 16UL);
        var_20 = (var_19 >> 3UL) & 1UL;
        var_21 = (var_17 > var_20) ? var_20 : var_17;
        r911_6 = var_19;
        if (var_17 > 5UL) {
            rax_1 = var_21;
            if (var_21 == 0UL) {
                var_22 = var_17 + *(uint64_t *)var_19;
                r14_0 = var_22;
                rax_2 = rax_1;
                var_23 = var_22 + *(uint64_t *)(var_19 + 8UL);
                r109_0 = 2UL;
                r14_0 = var_23;
                var_24 = var_23 + *(uint64_t *)(var_19 + 16UL);
                r109_0 = 3UL;
                r14_0 = var_24;
                var_25 = var_24 + *(uint64_t *)(var_19 + 24UL);
                r109_0 = 4UL;
                r14_0 = var_25;
                if (rax_1 != 1UL & rax_1 != 2UL & rax_1 != 3UL & rax_1 == 5UL) {
                    r109_0 = 5UL;
                    r14_0 = var_25 + *(uint64_t *)(var_19 + 32UL);
                }
                r109_1 = r109_0;
                r14_1 = r14_0;
                r109_3 = r109_0;
                r14_3 = r14_0;
                if (var_17 != rax_1) {
                    var_26 = var_17 - rax_2;
                    var_27 = ((var_26 + (-2L)) >> 1UL) + 1UL;
                    var_28 = var_27 << 1UL;
                    r109_2 = r109_1;
                    r14_2 = r14_1;
                    r814_0 = var_27;
                    if (var_26 == 1UL) {
                        r109_3 = r109_2;
                        r14_3 = r14_2 + *(uint64_t *)((r109_2 << 3UL) + var_19);
                    } else {
                        var_29 = helper_pxor_xmm_wrapper((struct type_5 *)(0UL), (struct type_7 *)(776UL), (struct type_7 *)(776UL), var_7, var_8);
                        rcx_2 = (rax_2 << 3UL) + var_19;
                        state_0x8558_0 = var_29.field_0;
                        state_0x8560_0 = var_29.field_1;
                        var_30 = rax_3 + 1UL;
                        var_31 = helper_paddq_xmm_wrapper_305((struct type_5 *)(0UL), (struct type_7 *)(776UL), (struct type_7 *)(2824UL), state_0x8558_0, state_0x8560_0, *(uint64_t *)rcx_2, *(uint64_t *)(rcx_2 + 8UL));
                        var_32 = helper_cc_compute_c_wrapper(var_30 - var_27, var_27, var_6, 17U);
                        rax_3 = var_30;
                        while (var_32 != 0UL)
                            {
                                rcx_2 = rcx_2 + 16UL;
                                state_0x8558_0 = var_31.field_0;
                                state_0x8560_0 = var_31.field_1;
                                var_30 = rax_3 + 1UL;
                                var_31 = helper_paddq_xmm_wrapper_305((struct type_5 *)(0UL), (struct type_7 *)(776UL), (struct type_7 *)(2824UL), state_0x8558_0, state_0x8560_0, *(uint64_t *)rcx_2, *(uint64_t *)(rcx_2 + 8UL));
                                var_32 = helper_cc_compute_c_wrapper(var_30 - var_27, var_27, var_6, 17U);
                                rax_3 = var_30;
                            }
                        var_33 = var_31.field_0;
                        var_34 = var_31.field_1;
                        var_35 = r109_1 + var_28;
                        var_36 = helper_psrldq_xmm_wrapper((struct type_5 *)(0UL), (struct type_7 *)(840UL), (struct type_7 *)(2824UL), var_33, var_34, 8UL, var_10, var_11, var_12, var_13, var_14, var_15);
                        var_37 = helper_paddq_xmm_wrapper_304((struct type_5 *)(0UL), (struct type_7 *)(776UL), (struct type_7 *)(840UL), var_33, var_34, var_36.field_0, var_36.field_1);
                        var_38 = r14_1 + var_37.field_0;
                        r109_2 = var_35;
                        r14_2 = var_38;
                        r109_3 = var_35;
                        r14_3 = var_38;
                        if (var_28 == var_26) {
                            r109_3 = r109_2;
                            r14_3 = r14_2 + *(uint64_t *)((r109_2 << 3UL) + var_19);
                        }
                    }
                }
            } else {
                var_26 = var_17 - rax_2;
                var_27 = ((var_26 + (-2L)) >> 1UL) + 1UL;
                var_28 = var_27 << 1UL;
                r109_2 = r109_1;
                r14_2 = r14_1;
                r814_0 = var_27;
                if (var_26 == 1UL) {
                    r109_3 = r109_2;
                    r14_3 = r14_2 + *(uint64_t *)((r109_2 << 3UL) + var_19);
                } else {
                    var_29 = helper_pxor_xmm_wrapper((struct type_5 *)(0UL), (struct type_7 *)(776UL), (struct type_7 *)(776UL), var_7, var_8);
                    rcx_2 = (rax_2 << 3UL) + var_19;
                    state_0x8558_0 = var_29.field_0;
                    state_0x8560_0 = var_29.field_1;
                    var_30 = rax_3 + 1UL;
                    var_31 = helper_paddq_xmm_wrapper_305((struct type_5 *)(0UL), (struct type_7 *)(776UL), (struct type_7 *)(2824UL), state_0x8558_0, state_0x8560_0, *(uint64_t *)rcx_2, *(uint64_t *)(rcx_2 + 8UL));
                    var_32 = helper_cc_compute_c_wrapper(var_30 - var_27, var_27, var_6, 17U);
                    rax_3 = var_30;
                    while (var_32 != 0UL)
                        {
                            rcx_2 = rcx_2 + 16UL;
                            state_0x8558_0 = var_31.field_0;
                            state_0x8560_0 = var_31.field_1;
                            var_30 = rax_3 + 1UL;
                            var_31 = helper_paddq_xmm_wrapper_305((struct type_5 *)(0UL), (struct type_7 *)(776UL), (struct type_7 *)(2824UL), state_0x8558_0, state_0x8560_0, *(uint64_t *)rcx_2, *(uint64_t *)(rcx_2 + 8UL));
                            var_32 = helper_cc_compute_c_wrapper(var_30 - var_27, var_27, var_6, 17U);
                            rax_3 = var_30;
                        }
                    var_33 = var_31.field_0;
                    var_34 = var_31.field_1;
                    var_35 = r109_1 + var_28;
                    var_36 = helper_psrldq_xmm_wrapper((struct type_5 *)(0UL), (struct type_7 *)(840UL), (struct type_7 *)(2824UL), var_33, var_34, 8UL, var_10, var_11, var_12, var_13, var_14, var_15);
                    var_37 = helper_paddq_xmm_wrapper_304((struct type_5 *)(0UL), (struct type_7 *)(776UL), (struct type_7 *)(840UL), var_33, var_34, var_36.field_0, var_36.field_1);
                    var_38 = r14_1 + var_37.field_0;
                    r109_2 = var_35;
                    r14_2 = var_38;
                    r109_3 = var_35;
                    r14_3 = var_38;
                    if (var_28 == var_26) {
                        r109_3 = r109_2;
                        r14_3 = r14_2 + *(uint64_t *)((r109_2 << 3UL) + var_19);
                    }
                }
            }
        } else {
            var_22 = var_17 + *(uint64_t *)var_19;
            r14_0 = var_22;
            rax_2 = rax_1;
            var_23 = var_22 + *(uint64_t *)(var_19 + 8UL);
            r109_0 = 2UL;
            r14_0 = var_23;
            var_24 = var_23 + *(uint64_t *)(var_19 + 16UL);
            r109_0 = 3UL;
            r14_0 = var_24;
            var_25 = var_24 + *(uint64_t *)(var_19 + 24UL);
            r109_0 = 4UL;
            r14_0 = var_25;
            if (rax_1 != 1UL & rax_1 != 2UL & rax_1 != 3UL & rax_1 == 5UL) {
                r109_0 = 5UL;
                r14_0 = var_25 + *(uint64_t *)(var_19 + 32UL);
            }
            r109_1 = r109_0;
            r14_1 = r14_0;
            r109_3 = r109_0;
            r14_3 = r14_0;
            if (var_17 != rax_1) {
                var_26 = var_17 - rax_2;
                var_27 = ((var_26 + (-2L)) >> 1UL) + 1UL;
                var_28 = var_27 << 1UL;
                r109_2 = r109_1;
                r14_2 = r14_1;
                r814_0 = var_27;
                if (var_26 == 1UL) {
                    r109_3 = r109_2;
                    r14_3 = r14_2 + *(uint64_t *)((r109_2 << 3UL) + var_19);
                } else {
                    var_29 = helper_pxor_xmm_wrapper((struct type_5 *)(0UL), (struct type_7 *)(776UL), (struct type_7 *)(776UL), var_7, var_8);
                    rcx_2 = (rax_2 << 3UL) + var_19;
                    state_0x8558_0 = var_29.field_0;
                    state_0x8560_0 = var_29.field_1;
                    var_30 = rax_3 + 1UL;
                    var_31 = helper_paddq_xmm_wrapper_305((struct type_5 *)(0UL), (struct type_7 *)(776UL), (struct type_7 *)(2824UL), state_0x8558_0, state_0x8560_0, *(uint64_t *)rcx_2, *(uint64_t *)(rcx_2 + 8UL));
                    var_32 = helper_cc_compute_c_wrapper(var_30 - var_27, var_27, var_6, 17U);
                    rax_3 = var_30;
                    while (var_32 != 0UL)
                        {
                            rcx_2 = rcx_2 + 16UL;
                            state_0x8558_0 = var_31.field_0;
                            state_0x8560_0 = var_31.field_1;
                            var_30 = rax_3 + 1UL;
                            var_31 = helper_paddq_xmm_wrapper_305((struct type_5 *)(0UL), (struct type_7 *)(776UL), (struct type_7 *)(2824UL), state_0x8558_0, state_0x8560_0, *(uint64_t *)rcx_2, *(uint64_t *)(rcx_2 + 8UL));
                            var_32 = helper_cc_compute_c_wrapper(var_30 - var_27, var_27, var_6, 17U);
                            rax_3 = var_30;
                        }
                    var_33 = var_31.field_0;
                    var_34 = var_31.field_1;
                    var_35 = r109_1 + var_28;
                    var_36 = helper_psrldq_xmm_wrapper((struct type_5 *)(0UL), (struct type_7 *)(840UL), (struct type_7 *)(2824UL), var_33, var_34, 8UL, var_10, var_11, var_12, var_13, var_14, var_15);
                    var_37 = helper_paddq_xmm_wrapper_304((struct type_5 *)(0UL), (struct type_7 *)(776UL), (struct type_7 *)(840UL), var_33, var_34, var_36.field_0, var_36.field_1);
                    var_38 = r14_1 + var_37.field_0;
                    r109_2 = var_35;
                    r14_2 = var_38;
                    r109_3 = var_35;
                    r14_3 = var_38;
                    if (var_28 == var_26) {
                        r109_3 = r109_2;
                        r14_3 = r14_2 + *(uint64_t *)((r109_2 << 3UL) + var_19);
                    }
                }
            }
        }
    }
    var_39 = r14_3 & *(uint64_t *)(rsi + 136UL);
    var_40 = var_39 * 3UL;
    var_41 = (var_39 * 24UL) + *(uint64_t *)(rsi + 64UL);
    var_42 = *(uint64_t *)var_41;
    var_43 = helper_cc_compute_all_wrapper(var_42, 0UL, 0UL, 25U);
    r109_8 = r109_3;
    r109_4 = r109_3;
    r911_7 = r911_6;
    r814_1 = r814_0;
    r911_11 = r911_6;
    rcx_4 = var_40;
    r814_4 = r814_0;
    if ((uint64_t)(((unsigned char)(var_43 >> 4UL) ^ (unsigned char)var_43) & '\xc0') != 0UL) {
        var_44 = *(uint64_t *)(var_41 + 16UL);
        var_45 = (var_17 << 3UL) + (-8L);
        var_46 = (uint64_t *)(rdx + 16UL);
        r814_2 = var_45;
        rcx_4 = var_42;
        while (1U)
            {
                var_47 = *(uint64_t *)((rcx_3 << 3UL) + var_44);
                rax_4 = var_47;
                r911_10 = r911_7;
                r109_7 = r109_4;
                r814_3 = r814_1;
                r109_5 = r109_4;
                if (*(uint64_t *)var_47 == r14_3) {
                    var_53 = rcx_3 + 1UL;
                    r109_8 = r109_7;
                    r109_4 = r109_7;
                    r911_7 = r911_10;
                    rcx_3 = var_53;
                    r814_1 = r814_3;
                    r911_11 = r911_10;
                    r814_4 = r814_3;
                    if (var_53 == var_42) {
                        continue;
                    }
                    loop_state_var = 0U;
                    break;
                }
                r911_10 = var_17;
                r814_3 = var_45;
                if (var_17 != *(uint64_t *)(var_47 + 16UL)) {
                    var_48 = (uint64_t *)(var_47 + 24UL);
                    while (1U)
                        {
                            var_49 = r911_8 + (-1L);
                            r911_8 = var_49;
                            r109_6 = r109_5;
                            r911_9 = var_49;
                            r911_10 = var_49;
                            if ((long)var_49 <= (long)18446744073709551615UL) {
                                loop_state_var = 1U;
                                break;
                            }
                            var_50 = *(uint64_t *)(r814_2 + *var_48);
                            var_51 = *(uint64_t *)(r814_2 + *var_46);
                            var_52 = r814_2 + (-8L);
                            r109_5 = var_50;
                            r814_2 = var_52;
                            r109_7 = var_51;
                            r814_3 = var_52;
                            if (var_50 == var_51) {
                                continue;
                            }
                            loop_state_var = 0U;
                            break;
                        }
                    switch_state_var = 0;
                    switch (loop_state_var) {
                      case 0U:
                        {
                            break;
                        }
                        break;
                      case 1U:
                        {
                            loop_state_var = 1U;
                            switch_state_var = 1;
                            break;
                        }
                        break;
                    }
                    if (switch_state_var)
                        break;
                }
            }
        switch (loop_state_var) {
          case 1U:
            {
                mrv3.field_0 = rax_4;
                mrv4 = mrv3;
                mrv4.field_1 = r109_6;
                mrv5 = mrv4;
                mrv5.field_2 = r911_9;
                return mrv5;
            }
            break;
          case 0U:
            {
                break;
            }
            break;
        }
    }
    *(uint64_t *)(var_0 + (-64L)) = 4259048UL;
    var_54 = indirect_placeholder_21(rsi, r109_8, 112UL, r911_11, rdi, rcx_4, 1UL, r814_4);
    var_55 = var_54.field_0;
    var_56 = var_54.field_1;
    var_57 = var_54.field_2;
    var_58 = var_54.field_3;
    var_59 = var_54.field_4;
    r911_4 = var_58;
    r911_0 = var_58;
    rax_4 = var_55;
    r911_5 = var_58;
    r109_6 = var_57;
    if (var_55 == 0UL) {
        *(uint32_t *)var_59 = 12U;
        mrv1.field_1 = var_57;
        mrv2 = mrv1;
        mrv2.field_2 = r911_5;
        return mrv2;
    }
    var_60 = *var_16;
    var_61 = var_55 + 8UL;
    var_62 = (uint64_t *)(var_55 + 16UL);
    *var_62 = var_60;
    if ((long)*var_16 > (long)0UL) {
        var_63 = var_0 + (-72L);
        *(uint64_t *)var_63 = 4259567UL;
        indirect_placeholder();
        local_sp_1 = var_63;
    } else {
        var_64 = (uint64_t *)var_61;
        *var_64 = var_60;
        var_65 = var_60 << 3UL;
        *(uint64_t *)(var_0 + (-72L)) = 4259096UL;
        var_66 = indirect_placeholder_20(var_65);
        var_67 = var_66.field_0;
        *(uint64_t *)(var_55 + 24UL) = var_67;
        if (var_67 != 0UL) {
            *var_62 = 0UL;
            *var_64 = 0UL;
            *(uint64_t *)(var_0 + (-80L)) = 4259660UL;
            indirect_placeholder();
            *(uint32_t *)var_59 = 12U;
            mrv1.field_1 = var_57;
            mrv2 = mrv1;
            mrv2.field_2 = r911_5;
            return mrv2;
        }
        var_68 = var_0 + (-80L);
        *(uint64_t *)var_68 = 4259129UL;
        indirect_placeholder();
        local_sp_1 = var_68;
    }
    var_69 = *var_16;
    var_70 = (uint64_t *)(var_55 + 80UL);
    *var_70 = var_61;
    var_71 = helper_cc_compute_all_wrapper(var_69, 0UL, 0UL, 25U);
    rcx_0 = var_69;
    if ((uint64_t)(((unsigned char)(var_71 >> 4UL) ^ (unsigned char)var_71) & '\xc0') != 0UL) {
        var_72 = *(uint64_t *)var_56;
        var_73 = (uint64_t *)(rdx + 16UL);
        var_74 = (unsigned char *)(var_55 + 104UL);
        while (1U)
            {
                var_75 = (*(uint64_t *)((rax_0 << 3UL) + *var_73) << 4UL) + var_72;
                var_76 = var_75 + 8UL;
                var_77 = (uint32_t)(uint64_t)*(unsigned char *)var_76;
                r911_1 = r911_0;
                r911_2 = r911_0;
                r911_3 = r911_0;
                rcx_1 = rcx_0;
                if ((uint64_t)(var_77 + (-1)) == 0UL) {
                    var_86 = (uint32_t *)var_76;
                    if ((*var_86 & 261888U) != 0U) {
                        var_92 = rax_0 + 1UL;
                        rax_0_be = var_92;
                        r911_0_be = r911_3;
                        rcx_0_be = rcx_1;
                        r911_4 = r911_3;
                        if ((long)var_92 < (long)rcx_1) {
                            break;
                        }
                        rax_0 = rax_0_be;
                        r911_0 = r911_0_be;
                        rcx_0 = rcx_0_be;
                        continue;
                    }
                    var_87 = ((*(unsigned char *)(var_75 + 10UL) << '\x01') & ' ') | *var_74;
                    *var_74 = var_87;
                    var_88 = var_87;
                    if ((*var_86 & 261888U) != 0U) {
                        var_91 = *var_16;
                        r911_3 = r911_1;
                        rcx_1 = var_91;
                        var_92 = rax_0 + 1UL;
                        rax_0_be = var_92;
                        r911_0_be = r911_3;
                        rcx_0_be = rcx_1;
                        r911_4 = r911_3;
                        if ((long)var_92 < (long)rcx_1) {
                            break;
                        }
                        rax_0 = rax_0_be;
                        r911_0 = r911_0_be;
                        rcx_0 = rcx_0_be;
                        continue;
                    }
                }
                var_78 = *var_74;
                var_79 = (uint64_t)(*(unsigned char *)(var_75 + 10UL) >> '\x04');
                var_80 = var_78 & '\xdf';
                var_81 = (uint64_t)var_78 >> 5UL;
                var_82 = var_80 | (unsigned char)(((var_81 | var_79) << 5UL) & 32UL);
                var_83 = (uint64_t)(var_77 + (-2));
                *var_74 = var_82;
                r911_1 = var_81;
                var_88 = var_82;
                r911_2 = var_81;
                r911_3 = var_81;
                if (var_83 != 0UL) {
                    *var_74 = (var_82 | '\x10');
                    var_85 = *var_16;
                    rcx_1 = var_85;
                    var_92 = rax_0 + 1UL;
                    rax_0_be = var_92;
                    r911_0_be = r911_3;
                    rcx_0_be = rcx_1;
                    r911_4 = r911_3;
                    if ((long)var_92 < (long)rcx_1) {
                        break;
                    }
                    rax_0 = rax_0_be;
                    r911_0 = r911_0_be;
                    rcx_0 = rcx_0_be;
                    continue;
                }
                if ((uint64_t)(var_77 + (-4)) != 0UL) {
                    *var_74 = (var_82 | '@');
                    var_84 = *var_16;
                    rcx_1 = var_84;
                    var_92 = rax_0 + 1UL;
                    rax_0_be = var_92;
                    r911_0_be = r911_3;
                    rcx_0_be = rcx_1;
                    r911_4 = r911_3;
                    if ((long)var_92 < (long)rcx_1) {
                        break;
                    }
                    rax_0 = rax_0_be;
                    r911_0 = r911_0_be;
                    rcx_0 = rcx_0_be;
                    continue;
                }
                if ((uint64_t)(var_77 + (-12)) == 0UL) {
                    *var_74 = (var_88 | '\x80');
                    var_89 = rax_0 + 1UL;
                    var_90 = *var_16;
                    rax_0_be = var_89;
                    r911_0_be = r911_2;
                    rcx_0_be = var_90;
                    r911_4 = r911_2;
                    if ((long)var_89 < (long)var_90) {
                        break;
                    }
                    rax_0 = rax_0_be;
                    r911_0 = r911_0_be;
                    rcx_0 = rcx_0_be;
                    continue;
                }
                if ((*(uint32_t *)var_76 & 261888U) == 0U) {
                    var_91 = *var_16;
                    r911_3 = r911_1;
                    rcx_1 = var_91;
                    var_92 = rax_0 + 1UL;
                    rax_0_be = var_92;
                    r911_0_be = r911_3;
                    rcx_0_be = rcx_1;
                    r911_4 = r911_3;
                    if ((long)var_92 < (long)rcx_1) {
                        break;
                    }
                    rax_0 = rax_0_be;
                    r911_0 = r911_0_be;
                    rcx_0 = rcx_0_be;
                    continue;
                }
            }
    }
    *(uint64_t *)(local_sp_1 + (-8L)) = 4259359UL;
    var_93 = indirect_placeholder_5(var_56, r14_3, var_55);
    r911_5 = r911_4;
    r911_9 = r911_4;
    if ((uint64_t)(uint32_t)var_93 == 0UL) {
        mrv3.field_0 = rax_4;
        mrv4 = mrv3;
        mrv4.field_1 = r109_6;
        mrv5 = mrv4;
        mrv5.field_2 = r911_9;
        return mrv5;
    }
    *(uint64_t *)(local_sp_1 + (-16L)) = 4259381UL;
    indirect_placeholder();
    var_94 = local_sp_1 + (-24L);
    *(uint64_t *)var_94 = 4259390UL;
    indirect_placeholder();
    local_sp_0 = var_94;
    if (var_61 == *var_70) {
        *(uint64_t *)(local_sp_1 + (-32L)) = 4259408UL;
        indirect_placeholder();
        var_95 = local_sp_1 + (-40L);
        *(uint64_t *)var_95 = 4259417UL;
        indirect_placeholder();
        local_sp_0 = var_95;
    }
    *(uint64_t *)(local_sp_0 + (-8L)) = 4259426UL;
    indirect_placeholder();
    *(uint64_t *)(local_sp_0 + (-16L)) = 4259435UL;
    indirect_placeholder();
    *(uint64_t *)(local_sp_0 + (-24L)) = 4259444UL;
    indirect_placeholder();
    *(uint64_t *)(local_sp_0 + (-32L)) = 4259452UL;
    indirect_placeholder();
    *(uint32_t *)var_59 = 12U;
    mrv1.field_1 = var_57;
    mrv2 = mrv1;
    mrv2.field_2 = r911_5;
    return mrv2;
}

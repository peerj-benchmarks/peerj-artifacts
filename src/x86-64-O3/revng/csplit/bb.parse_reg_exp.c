typedef unsigned char __u_char;
typedef unsigned short __u_short;
typedef unsigned int __u_int;
typedef unsigned long __u_long;
typedef signed char __int8_t;
typedef unsigned char __uint8_t;
typedef short __int16_t;
typedef unsigned short __uint16_t;
typedef int __int32_t;
typedef unsigned int __uint32_t;
typedef long __int64_t;
typedef unsigned long __uint64_t;
typedef __int8_t __int_least8_t;
typedef __uint8_t __uint_least8_t;
typedef __int16_t __int_least16_t;
typedef __uint16_t __uint_least16_t;
typedef __int32_t __int_least32_t;
typedef __uint32_t __uint_least32_t;
typedef __int64_t __int_least64_t;
typedef __uint64_t __uint_least64_t;
typedef long __quad_t;
typedef unsigned long __u_quad_t;
typedef long __intmax_t;
typedef unsigned long __uintmax_t;
typedef unsigned long __dev_t;
typedef unsigned int __uid_t;
typedef unsigned int __gid_t;
typedef unsigned long __ino_t;
typedef unsigned long __ino64_t;
typedef unsigned int __mode_t;
typedef unsigned long __nlink_t;
typedef long __off_t;
typedef long __off64_t;
typedef int __pid_t;
typedef struct {
    int __val[2];
} __fsid_t;
typedef long __clock_t;
typedef unsigned long __rlim_t;
typedef unsigned long __rlim64_t;
typedef unsigned int __id_t;
typedef long __time_t;
typedef unsigned int __useconds_t;
typedef long __suseconds_t;
typedef int __daddr_t;
typedef int __key_t;
typedef int __clockid_t;
typedef void *__timer_t;
typedef long __blksize_t;
typedef long __blkcnt_t;
typedef long __blkcnt64_t;
typedef unsigned long __fsblkcnt_t;
typedef unsigned long __fsblkcnt64_t;
typedef unsigned long __fsfilcnt_t;
typedef unsigned long __fsfilcnt64_t;
typedef long __fsword_t;
typedef long __ssize_t;
typedef long __syscall_slong_t;
typedef unsigned long __syscall_ulong_t;
typedef __off64_t __loff_t;
typedef char *__caddr_t;
typedef long __intptr_t;
typedef unsigned int __socklen_t;
typedef int __sig_atomic_t;
typedef __int8_t int8_t;
typedef __int16_t int16_t;
typedef __int32_t int32_t;
typedef __int64_t int64_t;
typedef __uint8_t uint8_t;
typedef __uint16_t uint16_t;
typedef __uint32_t uint32_t;
typedef __uint64_t uint64_t;
typedef __int_least8_t int_least8_t;
typedef __int_least16_t int_least16_t;
typedef __int_least32_t int_least32_t;
typedef __int_least64_t int_least64_t;
typedef __uint_least8_t uint_least8_t;
typedef __uint_least16_t uint_least16_t;
typedef __uint_least32_t uint_least32_t;
typedef __uint_least64_t uint_least64_t;
typedef signed char int_fast8_t;
typedef long int_fast16_t;
typedef long int_fast32_t;
typedef long int_fast64_t;
typedef unsigned char uint_fast8_t;
typedef unsigned long uint_fast16_t;
typedef unsigned long uint_fast32_t;
typedef unsigned long uint_fast64_t;
typedef long intptr_t;
typedef unsigned long uintptr_t;
typedef __intmax_t intmax_t;
typedef __uintmax_t uintmax_t;
struct indirect_placeholder_92_ret_type;
struct indirect_placeholder_93_ret_type;
struct indirect_placeholder_92_ret_type {
    uint64_t field_0;
    uint64_t field_1;
};
struct indirect_placeholder_93_ret_type {
    uint64_t field_0;
    uint64_t field_1;
    uint64_t field_2;
    uint64_t field_3;
    uint64_t field_4;
};
extern uint64_t revng_init_local_sp(uint64_t param_0);
extern uint64_t init_rbx(void);
extern uint64_t init_r12(void);
extern uint64_t init_rbp(void);
extern void indirect_placeholder(void);
extern uint64_t init_r13(void);
extern uint64_t init_r14(void);
extern uint64_t init_r15(void);
extern uint64_t indirect_placeholder_91(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint64_t param_3, uint64_t param_4, uint64_t param_5);
extern struct indirect_placeholder_92_ret_type indirect_placeholder_92(uint64_t param_0);
extern struct indirect_placeholder_93_ret_type indirect_placeholder_93(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint64_t param_3, uint64_t param_4, uint64_t param_5, uint64_t param_6, uint64_t param_7);
typedef _Bool bool;
uint64_t bb_parse_reg_exp(uint64_t rdi, uint64_t r9, uint64_t rcx, uint64_t rdx, uint64_t rsi, uint64_t r8) {
    uint64_t var_0;
    uint64_t var_1;
    uint64_t var_2;
    uint64_t var_3;
    uint64_t var_4;
    uint64_t var_5;
    uint64_t var_6;
    uint64_t var_7;
    uint64_t *var_8;
    uint64_t var_9;
    uint64_t *var_10;
    uint64_t var_11;
    uint64_t merge;
    uint64_t r12_0;
    uint64_t var_48;
    uint64_t local_sp_6;
    uint64_t local_sp_1;
    uint64_t r13_0;
    uint64_t rax_0;
    uint64_t local_sp_3;
    uint64_t local_sp_0;
    uint64_t local_sp_4;
    uint64_t r14_0;
    uint64_t var_34;
    uint64_t var_35;
    uint64_t rcx3_0;
    uint64_t rdx4_1;
    uint64_t rsi5_2;
    uint64_t var_49;
    uint64_t r14_1;
    uint32_t var_36;
    uint64_t local_sp_2;
    uint64_t var_39;
    uint64_t var_40;
    uint64_t var_37;
    uint64_t var_38;
    uint32_t var_41;
    uint64_t var_42;
    uint64_t local_sp_5;
    uint32_t var_43;
    uint64_t var_44;
    uint64_t rbx_0;
    uint64_t rsi5_0;
    uint64_t var_45;
    struct indirect_placeholder_92_ret_type var_46;
    uint64_t var_47;
    uint64_t local_sp_7;
    uint64_t rbx_1;
    uint64_t rdx4_0;
    uint64_t rsi5_1;
    uint64_t rax_1;
    uint64_t rbx_2;
    uint64_t var_50;
    uint64_t var_51;
    unsigned char *var_52;
    uint64_t local_sp_8;
    uint64_t var_27;
    uint64_t var_28;
    uint64_t var_29;
    uint64_t var_30;
    uint64_t var_31;
    uint64_t var_32;
    uint64_t var_33;
    uint64_t var_12;
    uint32_t *var_13;
    uint64_t *var_14;
    uint64_t rbx_3;
    uint64_t rbp_0;
    uint64_t r14_2;
    uint64_t *var_15;
    uint64_t var_16;
    uint64_t var_17;
    struct indirect_placeholder_93_ret_type var_18;
    uint64_t var_19;
    uint64_t var_20;
    uint64_t var_21;
    uint64_t var_22;
    uint64_t var_23;
    uint64_t var_24;
    uint64_t *var_25;
    unsigned char var_26;
    unsigned int loop_state_var;
    bool switch_state_var;
    var_0 = revng_init_local_sp(0UL);
    var_1 = init_r15();
    var_2 = init_r13();
    var_3 = init_rbx();
    var_4 = init_r12();
    var_5 = init_rbp();
    var_6 = init_r14();
    *(uint64_t *)(var_0 + (-8L)) = var_1;
    *(uint64_t *)(var_0 + (-16L)) = var_6;
    *(uint64_t *)(var_0 + (-24L)) = var_2;
    *(uint64_t *)(var_0 + (-32L)) = var_4;
    *(uint64_t *)(var_0 + (-40L)) = var_5;
    *(uint64_t *)(var_0 + (-48L)) = var_3;
    var_7 = *(uint64_t *)rsi;
    *(uint64_t *)(var_0 + (-72L)) = rsi;
    *(uint64_t *)(var_0 + (-88L)) = rcx;
    var_8 = (uint64_t *)(var_7 + 168UL);
    *(uint64_t *)(var_0 + (-80L)) = *var_8;
    var_9 = var_0 + (-112L);
    var_10 = (uint64_t *)var_9;
    *var_10 = 4297950UL;
    var_11 = indirect_placeholder_91(rdi, r9, rcx, rdx, rsi, r8);
    merge = 0UL;
    r12_0 = r8;
    r13_0 = rdx;
    rcx3_0 = 1UL;
    rdx4_1 = 0UL;
    rsi5_0 = 0UL;
    rsi5_1 = 0UL;
    local_sp_8 = var_9;
    rbx_3 = rdx;
    rbp_0 = rdi;
    r14_2 = var_11;
    if (var_11 != 0UL) {
        if (*(uint32_t *)r9 == 0U) {
            return merge;
        }
    }
    var_12 = *(uint64_t *)(var_0 + (-96L));
    *(uint64_t *)(var_0 + (-104L)) = r9;
    *var_10 = (var_12 | 8388608UL);
    var_13 = (uint32_t *)(var_7 + 128UL);
    var_14 = (uint64_t *)(var_7 + 112UL);
    while (1U)
        {
            rbx_0 = rbx_3;
            rbx_1 = rbx_3;
            merge = r14_2;
            if (*(unsigned char *)(r13_0 + 8UL) != '\n') {
                loop_state_var = 0U;
                break;
            }
            var_15 = (uint64_t *)local_sp_8;
            var_16 = *var_15;
            var_17 = local_sp_8 + (-8L);
            *(uint64_t *)var_17 = 4298240UL;
            var_18 = indirect_placeholder_93(r13_0, rbx_3, r13_0, r12_0, rbp_0, var_16, r14_2, rbp_0);
            var_19 = var_18.field_0;
            var_20 = var_18.field_1;
            var_21 = var_18.field_2;
            var_22 = var_18.field_3;
            var_23 = var_18.field_4;
            var_24 = (uint64_t)((long)(var_19 << 32UL) >> (long)32UL);
            var_25 = (uint64_t *)(var_22 + 72UL);
            *var_25 = (*var_25 + var_24);
            var_26 = *(unsigned char *)(var_20 + 8UL);
            merge = 0UL;
            r12_0 = var_21;
            r13_0 = var_20;
            local_sp_4 = var_17;
            r14_0 = var_23;
            local_sp_5 = var_17;
            rbp_0 = var_22;
            if ((uint64_t)((var_26 & '\xf7') + '\xfe') == 0UL) {
                var_43 = *var_13;
                var_44 = (uint64_t)var_43;
                rdx4_0 = var_44;
                if ((uint64_t)(var_43 + (-15)) == 0UL) {
                    local_sp_6 = local_sp_5;
                    rcx3_0 = (uint64_t)((uint32_t)rdx4_0 + 1U);
                    rdx4_1 = rdx4_0;
                    rsi5_2 = rsi5_1;
                    rax_1 = *var_14;
                    rbx_2 = rbx_1;
                } else {
                    *(uint64_t *)(local_sp_4 + 40UL) = rsi5_0;
                    var_45 = local_sp_4 + (-8L);
                    *(uint64_t *)var_45 = 4298298UL;
                    var_46 = indirect_placeholder_92(968UL);
                    var_47 = var_46.field_0;
                    local_sp_6 = var_45;
                    rax_1 = var_47;
                    rbx_2 = rbx_0;
                    local_sp_7 = var_45;
                    if (var_47 != 0UL) {
                        loop_state_var = 1U;
                        break;
                    }
                    var_48 = *(uint64_t *)(local_sp_4 + 32UL);
                    *(uint64_t *)var_47 = *var_14;
                    *var_14 = var_47;
                    rsi5_2 = var_48;
                }
            } else {
                if (var_21 != 0UL) {
                    if ((uint64_t)(var_26 + '\xf7') != 0UL) {
                        var_43 = *var_13;
                        var_44 = (uint64_t)var_43;
                        rdx4_0 = var_44;
                        if ((uint64_t)(var_43 + (-15)) == 0UL) {
                            local_sp_6 = local_sp_5;
                            rcx3_0 = (uint64_t)((uint32_t)rdx4_0 + 1U);
                            rdx4_1 = rdx4_0;
                            rsi5_2 = rsi5_1;
                            rax_1 = *var_14;
                            rbx_2 = rbx_1;
                        } else {
                            *(uint64_t *)(local_sp_4 + 40UL) = rsi5_0;
                            var_45 = local_sp_4 + (-8L);
                            *(uint64_t *)var_45 = 4298298UL;
                            var_46 = indirect_placeholder_92(968UL);
                            var_47 = var_46.field_0;
                            local_sp_6 = var_45;
                            rax_1 = var_47;
                            rbx_2 = rbx_0;
                            local_sp_7 = var_45;
                            if (var_47 != 0UL) {
                                loop_state_var = 1U;
                                break;
                            }
                            var_48 = *(uint64_t *)(local_sp_4 + 32UL);
                            *(uint64_t *)var_47 = *var_14;
                            *var_14 = var_47;
                            rsi5_2 = var_48;
                        }
                        var_49 = rdx4_1 << 6UL;
                        *var_13 = (uint32_t)rcx3_0;
                        var_50 = var_49 + rax_1;
                        var_51 = var_50 + 8UL;
                        *(uint64_t *)var_51 = 0UL;
                        *(uint64_t *)(var_50 + 16UL) = var_23;
                        *(uint64_t *)(var_50 + 24UL) = rsi5_2;
                        *(unsigned char *)(var_50 + 56UL) = (unsigned char)'\n';
                        var_52 = (unsigned char *)(var_50 + 58UL);
                        *var_52 = (*var_52 & '\xf3');
                        *(uint64_t *)(var_50 + 32UL) = 0UL;
                        *(uint64_t *)(var_50 + 40UL) = 0UL;
                        *(uint64_t *)(var_50 + 64UL) = 18446744073709551615UL;
                        local_sp_7 = local_sp_6;
                        local_sp_8 = local_sp_6;
                        rbx_3 = rbx_2;
                        r14_2 = var_51;
                        if (var_23 == 0UL) {
                            *(uint64_t *)var_23 = var_51;
                        }
                        if (rsi5_2 == 0UL) {
                            *(uint64_t *)rsi5_2 = var_51;
                        }
                        if (var_51 == 0UL) {
                            continue;
                        }
                        loop_state_var = 1U;
                        break;
                    }
                }
                var_27 = *(uint64_t *)(local_sp_8 + 16UL);
                var_28 = *(uint64_t *)(local_sp_8 + 24UL);
                var_29 = *var_15;
                var_30 = *(uint64_t *)(local_sp_8 + 8UL);
                var_31 = *var_8;
                *var_8 = var_27;
                var_32 = local_sp_8 + (-16L);
                *(uint64_t *)var_32 = 4298061UL;
                var_33 = indirect_placeholder_91(var_22, var_29, var_30, var_20, var_28, var_21);
                local_sp_0 = var_32;
                local_sp_4 = var_32;
                local_sp_5 = var_32;
                rbx_0 = var_31;
                rsi5_0 = var_33;
                rbx_1 = var_31;
                rsi5_1 = var_33;
                if (var_33 != 0UL) {
                    if (**(uint32_t **)var_17 != 0U) {
                        if (var_23 != 0UL) {
                            loop_state_var = 2U;
                            break;
                        }
                        loop_state_var = 0U;
                        break;
                    }
                }
                var_41 = *var_13;
                var_42 = (uint64_t)var_41;
                *var_8 = (*var_8 | var_31);
                rdx4_0 = var_42;
                if ((uint64_t)(var_41 + (-15)) == 0UL) {
                    local_sp_6 = local_sp_5;
                    rcx3_0 = (uint64_t)((uint32_t)rdx4_0 + 1U);
                    rdx4_1 = rdx4_0;
                    rsi5_2 = rsi5_1;
                    rax_1 = *var_14;
                    rbx_2 = rbx_1;
                } else {
                    *(uint64_t *)(local_sp_4 + 40UL) = rsi5_0;
                    var_45 = local_sp_4 + (-8L);
                    *(uint64_t *)var_45 = 4298298UL;
                    var_46 = indirect_placeholder_92(968UL);
                    var_47 = var_46.field_0;
                    local_sp_6 = var_45;
                    rax_1 = var_47;
                    rbx_2 = rbx_0;
                    local_sp_7 = var_45;
                    if (var_47 != 0UL) {
                        loop_state_var = 1U;
                        break;
                    }
                    var_48 = *(uint64_t *)(local_sp_4 + 32UL);
                    *(uint64_t *)var_47 = *var_14;
                    *var_14 = var_47;
                    rsi5_2 = var_48;
                }
            }
        }
    switch (loop_state_var) {
      case 0U:
        {
            return merge;
        }
        break;
      case 1U:
        {
            **(uint32_t **)(local_sp_7 + 8UL) = 12U;
        }
        break;
      case 2U:
        {
            while (1U)
                {
                    var_34 = *(uint64_t *)(r14_0 + 8UL);
                    local_sp_1 = local_sp_0;
                    r14_1 = r14_0;
                    local_sp_3 = local_sp_0;
                    rax_0 = var_34;
                    if (var_34 == 0UL) {
                        local_sp_0 = local_sp_3;
                        r14_0 = rax_0;
                        continue;
                    }
                    var_35 = *(uint64_t *)(r14_0 + 16UL);
                    rax_0 = var_35;
                    if (var_35 != 0UL) {
                        while (1U)
                            {
                                var_36 = *(uint32_t *)(r14_1 + 48UL) & 262399U;
                                local_sp_2 = local_sp_1;
                                if ((uint64_t)(var_36 + (-6)) == 0UL) {
                                    *(uint64_t *)(local_sp_1 + (-8L)) = 4298505UL;
                                    indirect_placeholder();
                                    *(uint64_t *)(local_sp_1 + (-16L)) = 4298514UL;
                                    indirect_placeholder();
                                    var_38 = local_sp_1 + (-24L);
                                    *(uint64_t *)var_38 = 4298522UL;
                                    indirect_placeholder();
                                    local_sp_2 = var_38;
                                } else {
                                    if ((uint64_t)(var_36 + (-3)) == 0UL) {
                                        var_37 = local_sp_1 + (-8L);
                                        *(uint64_t *)var_37 = 4298491UL;
                                        indirect_placeholder();
                                        local_sp_2 = var_37;
                                    }
                                }
                                var_39 = *(uint64_t *)r14_1;
                                local_sp_1 = local_sp_2;
                                r14_1 = var_39;
                                local_sp_3 = local_sp_2;
                                if (var_39 != 0UL) {
                                    loop_state_var = 1U;
                                    break;
                                }
                                var_40 = *(uint64_t *)(var_39 + 16UL);
                                rax_0 = var_40;
                                if ((r14_1 == var_40) || (var_40 == 0UL)) {
                                    continue;
                                }
                                loop_state_var = 0U;
                                break;
                            }
                        switch_state_var = 0;
                        switch (loop_state_var) {
                          case 1U:
                            {
                                switch_state_var = 1;
                                break;
                            }
                            break;
                          case 0U:
                            {
                                break;
                            }
                            break;
                        }
                        if (switch_state_var)
                            break;
                    }
                }
        }
        break;
    }
}

typedef unsigned char __u_char;
typedef unsigned short __u_short;
typedef unsigned int __u_int;
typedef unsigned long __u_long;
typedef signed char __int8_t;
typedef unsigned char __uint8_t;
typedef short __int16_t;
typedef unsigned short __uint16_t;
typedef int __int32_t;
typedef unsigned int __uint32_t;
typedef long __int64_t;
typedef unsigned long __uint64_t;
typedef __int8_t __int_least8_t;
typedef __uint8_t __uint_least8_t;
typedef __int16_t __int_least16_t;
typedef __uint16_t __uint_least16_t;
typedef __int32_t __int_least32_t;
typedef __uint32_t __uint_least32_t;
typedef __int64_t __int_least64_t;
typedef __uint64_t __uint_least64_t;
typedef long __quad_t;
typedef unsigned long __u_quad_t;
typedef long __intmax_t;
typedef unsigned long __uintmax_t;
typedef unsigned long __dev_t;
typedef unsigned int __uid_t;
typedef unsigned int __gid_t;
typedef unsigned long __ino_t;
typedef unsigned long __ino64_t;
typedef unsigned int __mode_t;
typedef unsigned long __nlink_t;
typedef long __off_t;
typedef long __off64_t;
typedef int __pid_t;
typedef struct {
    int __val[2];
} __fsid_t;
typedef long __clock_t;
typedef unsigned long __rlim_t;
typedef unsigned long __rlim64_t;
typedef unsigned int __id_t;
typedef long __time_t;
typedef unsigned int __useconds_t;
typedef long __suseconds_t;
typedef int __daddr_t;
typedef int __key_t;
typedef int __clockid_t;
typedef void *__timer_t;
typedef long __blksize_t;
typedef long __blkcnt_t;
typedef long __blkcnt64_t;
typedef unsigned long __fsblkcnt_t;
typedef unsigned long __fsblkcnt64_t;
typedef unsigned long __fsfilcnt_t;
typedef unsigned long __fsfilcnt64_t;
typedef long __fsword_t;
typedef long __ssize_t;
typedef long __syscall_slong_t;
typedef unsigned long __syscall_ulong_t;
typedef __off64_t __loff_t;
typedef char *__caddr_t;
typedef long __intptr_t;
typedef unsigned int __socklen_t;
typedef int __sig_atomic_t;
typedef __int8_t int8_t;
typedef __int16_t int16_t;
typedef __int32_t int32_t;
typedef __int64_t int64_t;
typedef __uint8_t uint8_t;
typedef __uint16_t uint16_t;
typedef __uint32_t uint32_t;
typedef __uint64_t uint64_t;
typedef __int_least8_t int_least8_t;
typedef __int_least16_t int_least16_t;
typedef __int_least32_t int_least32_t;
typedef __int_least64_t int_least64_t;
typedef __uint_least8_t uint_least8_t;
typedef __uint_least16_t uint_least16_t;
typedef __uint_least32_t uint_least32_t;
typedef __uint_least64_t uint_least64_t;
typedef signed char int_fast8_t;
typedef long int_fast16_t;
typedef long int_fast32_t;
typedef long int_fast64_t;
typedef unsigned char uint_fast8_t;
typedef unsigned long uint_fast16_t;
typedef unsigned long uint_fast32_t;
typedef unsigned long uint_fast64_t;
typedef long intptr_t;
typedef unsigned long uintptr_t;
typedef __intmax_t intmax_t;
typedef __uintmax_t uintmax_t;
struct bb_check_dst_limits_isra_11_ret_type;
struct bb_check_dst_limits_isra_11_ret_type {
    uint64_t field_0;
    uint64_t field_1;
    uint64_t field_2;
    uint64_t field_3;
};
extern uint64_t revng_init_local_sp(uint64_t param_0);
extern uint64_t init_rbx(void);
extern uint64_t init_r12(void);
extern uint64_t init_rbp(void);
extern uint64_t init_r13(void);
extern uint64_t init_r14(void);
extern uint64_t helper_cc_compute_all_wrapper(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint32_t param_3);
extern uint64_t indirect_placeholder_8(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint64_t param_3, uint64_t param_4);
extern uint64_t init_r15(void);
typedef _Bool bool;
struct bb_check_dst_limits_isra_11_ret_type bb_check_dst_limits_isra_11(uint64_t rdi, uint64_t r9, uint64_t rcx, uint64_t rdx, uint64_t rsi, uint64_t r8) {
    uint64_t var_52;
    uint64_t var_38;
    uint64_t var_0;
    uint64_t var_1;
    uint64_t var_2;
    uint64_t var_3;
    uint64_t var_4;
    uint64_t var_5;
    uint64_t var_6;
    uint64_t var_7;
    uint64_t var_8;
    uint64_t var_9;
    uint64_t var_10;
    uint64_t *var_11;
    uint64_t r89_1;
    uint64_t local_sp_1;
    uint64_t rax_1;
    uint64_t rdi4_0;
    uint64_t rsi8_0;
    uint64_t r89_0;
    uint64_t r89_2;
    uint64_t var_12;
    uint64_t var_13;
    uint64_t var_14;
    uint64_t var_15;
    uint64_t rax_0;
    uint64_t var_16;
    uint64_t rdi4_2;
    uint64_t r89_4;
    uint64_t rax_3;
    uint64_t rdi4_3;
    uint64_t r89_3;
    uint64_t storemerge2;
    uint64_t r89_5;
    uint64_t var_17;
    uint64_t var_18;
    uint64_t var_19;
    uint64_t var_20;
    uint64_t rax_2;
    uint64_t var_21;
    uint64_t rdi4_5;
    uint64_t var_56;
    uint64_t var_22;
    uint64_t var_23;
    uint64_t r10_5;
    uint64_t r10_7;
    uint64_t local_sp_4;
    uint64_t local_sp_0;
    uint64_t r13_0;
    uint64_t r95_5;
    uint64_t r10_0;
    uint64_t rsi8_2;
    uint64_t r95_0;
    uint64_t r89_11;
    uint64_t rsi8_1;
    uint64_t r95_7;
    uint64_t r89_6;
    uint64_t var_24;
    uint64_t *var_25;
    uint64_t var_26;
    uint64_t var_27;
    uint64_t var_28;
    uint64_t var_29;
    uint64_t var_30;
    uint64_t var_31;
    uint64_t r14_0;
    uint64_t var_32;
    uint64_t var_33;
    uint64_t var_34;
    uint64_t *var_35;
    uint64_t *var_36;
    uint64_t var_37;
    uint64_t var_39;
    uint64_t var_40;
    uint64_t var_41;
    uint64_t var_42;
    uint64_t var_43;
    uint64_t var_44;
    uint64_t local_sp_3;
    uint64_t r10_1;
    uint64_t r95_1;
    uint64_t rcx6_0;
    uint64_t rdx7_0;
    uint64_t r14_1;
    uint64_t r89_7;
    uint64_t local_sp_2;
    uint64_t var_45;
    uint64_t r10_2;
    uint64_t r95_2;
    uint64_t rcx6_1;
    uint64_t rdx7_1;
    uint64_t r14_2;
    uint64_t r89_8;
    uint64_t var_46;
    uint64_t *var_47;
    uint64_t var_48;
    uint64_t var_49;
    uint64_t *var_50;
    uint64_t var_51;
    uint64_t var_53;
    uint64_t var_54;
    uint64_t rax_4;
    uint64_t r10_3;
    uint64_t r95_3;
    uint64_t r14_3;
    uint64_t r89_9;
    uint64_t var_55;
    uint64_t r89_13;
    uint64_t storemerge;
    struct bb_check_dst_limits_isra_11_ret_type mrv;
    struct bb_check_dst_limits_isra_11_ret_type mrv1;
    struct bb_check_dst_limits_isra_11_ret_type mrv2;
    struct bb_check_dst_limits_isra_11_ret_type mrv3;
    unsigned int loop_state_var;
    bool switch_state_var;
    var_0 = revng_init_local_sp(0UL);
    var_1 = init_r15();
    var_2 = init_r13();
    var_3 = init_rbx();
    var_4 = init_r12();
    var_5 = init_rbp();
    var_6 = init_r14();
    *(uint64_t *)(var_0 + (-8L)) = var_1;
    *(uint64_t *)(var_0 + (-16L)) = var_6;
    *(uint64_t *)(var_0 + (-24L)) = var_2;
    *(uint64_t *)(var_0 + (-32L)) = var_4;
    *(uint64_t *)(var_0 + (-40L)) = var_5;
    *(uint64_t *)(var_0 + (-48L)) = var_3;
    var_7 = var_0 + (-120L);
    var_8 = *(uint64_t *)(rdi + 152UL);
    *(uint64_t *)(var_0 + (-112L)) = rcx;
    var_9 = *(uint64_t *)(rdi + 200UL);
    *(uint64_t *)(var_0 + (-104L)) = r9;
    var_10 = *(uint64_t *)(var_0 | 8UL);
    *(uint64_t *)var_7 = var_8;
    var_11 = (uint64_t *)(rdi + 216UL);
    rdi4_0 = var_9;
    rsi8_0 = 0UL;
    r89_0 = r8;
    rdi4_3 = var_9;
    storemerge2 = 0UL;
    r10_7 = rsi;
    local_sp_0 = var_7;
    r13_0 = 0UL;
    r10_0 = rsi;
    r95_0 = rdx;
    r95_7 = rdx;
    r14_0 = 1UL;
    r14_2 = 4294967295UL;
    rax_4 = 0UL;
    storemerge = 0UL;
    rdi4_2 = rdi4_0;
    r89_2 = r89_0;
    while ((long)rdi4_0 <= (long)rsi8_0)
        {
            var_12 = rsi8_0 + rdi4_0;
            var_13 = *var_11;
            var_14 = (uint64_t)((long)var_12 >> (long)1UL);
            var_15 = var_14 * 5UL;
            rax_0 = var_14;
            rax_1 = var_14;
            r89_1 = var_15;
            r89_2 = var_15;
            if ((long)*(uint64_t *)(((var_14 * 40UL) + var_13) + 8UL) < (long)r8) {
                rdi4_0 = rdi4_2;
                rsi8_0 = rax_1 + 1UL;
                r89_0 = r89_1;
                rdi4_2 = rdi4_0;
                r89_2 = r89_0;
                continue;
            }
            if ((long)rsi8_0 < (long)var_14) {
                break;
            }
            while (1U)
                {
                    var_16 = (uint64_t)((long)(rax_0 + rsi8_0) >> (long)1UL);
                    rax_0 = var_16;
                    rax_1 = var_16;
                    rdi4_2 = rax_0;
                    if ((long)*(uint64_t *)(((var_16 * 40UL) + var_13) + 8UL) >= (long)r8) {
                        loop_state_var = 1U;
                        break;
                    }
                    if ((long)rsi8_0 < (long)var_16) {
                        continue;
                    }
                    r89_2 = var_16 * 5UL;
                    loop_state_var = 0U;
                    break;
                }
            switch_state_var = 0;
            switch (loop_state_var) {
              case 1U:
                {
                    r89_1 = var_16 * 5UL;
                }
                break;
              case 0U:
                {
                    switch_state_var = 1;
                    break;
                }
                break;
            }
            if (switch_state_var)
                break;
        }
    r89_3 = r89_2;
    if ((long)var_9 > (long)rsi8_0) {
        *(uint64_t *)(var_0 + (-96L)) = ((*(uint64_t *)(((rsi8_0 * 40UL) + *var_11) + 8UL) == r8) ? rsi8_0 : 18446744073709551615UL);
    } else {
        *(uint64_t *)(var_0 + (-96L)) = 18446744073709551615UL;
    }
    rdi4_5 = rdi4_3;
    r89_5 = r89_3;
    while ((long)rdi4_3 <= (long)storemerge2)
        {
            var_17 = storemerge2 + rdi4_3;
            var_18 = *var_11;
            var_19 = (uint64_t)((long)var_17 >> (long)1UL);
            var_20 = var_19 * 5UL;
            rax_2 = var_19;
            rax_3 = var_19;
            r89_4 = var_20;
            r89_5 = var_20;
            if ((long)var_10 > (long)*(uint64_t *)(((var_19 * 40UL) + var_18) + 8UL)) {
                rdi4_3 = rdi4_5;
                r89_3 = r89_4;
                storemerge2 = rax_3 + 1UL;
                rdi4_5 = rdi4_3;
                r89_5 = r89_3;
                continue;
            }
            if ((long)storemerge2 < (long)var_19) {
                break;
            }
            while (1U)
                {
                    var_21 = (uint64_t)((long)(rax_2 + storemerge2) >> (long)1UL);
                    rax_2 = var_21;
                    rax_3 = var_21;
                    rdi4_5 = rax_2;
                    if ((long)var_10 <= (long)*(uint64_t *)(((var_21 * 40UL) + var_18) + 8UL)) {
                        loop_state_var = 1U;
                        break;
                    }
                    if ((long)storemerge2 < (long)var_21) {
                        continue;
                    }
                    r89_5 = var_21 * 5UL;
                    loop_state_var = 0U;
                    break;
                }
            switch_state_var = 0;
            switch (loop_state_var) {
              case 1U:
                {
                    r89_4 = var_21 * 5UL;
                }
                break;
              case 0U:
                {
                    switch_state_var = 1;
                    break;
                }
                break;
            }
            if (switch_state_var)
                break;
        }
    r89_6 = r89_5;
    r89_13 = r89_5;
    if ((long)var_9 > (long)storemerge2) {
        *(uint64_t *)(var_0 + (-88L)) = ((var_10 == *(uint64_t *)(((storemerge2 * 40UL) + *var_11) + 8UL)) ? storemerge2 : 18446744073709551615UL);
    } else {
        *(uint64_t *)(var_0 + (-88L)) = 18446744073709551615UL;
    }
    var_22 = *(uint64_t *)rsi;
    var_23 = helper_cc_compute_all_wrapper(var_22, 0UL, 0UL, 25U);
    rsi8_1 = var_22;
    if ((uint64_t)(((unsigned char)(var_23 >> 4UL) ^ (unsigned char)var_23) & '\xc0') == 0UL) {
        mrv.field_0 = storemerge;
        mrv1 = mrv;
        mrv1.field_1 = r10_7;
        mrv2 = mrv1;
        mrv2.field_2 = r95_7;
        mrv3 = mrv2;
        mrv3.field_3 = r89_13;
        return mrv3;
    }
    while (1U)
        {
            var_24 = *(uint64_t *)r95_0;
            var_25 = *(uint64_t **)local_sp_0;
            var_26 = r13_0 << 3UL;
            var_27 = (*(uint64_t *)(var_26 + var_24) * 40UL) + *var_11;
            var_28 = *(uint64_t *)var_27;
            var_29 = *(uint64_t *)(var_27 + 16UL);
            var_30 = *(uint64_t *)((var_28 << 4UL) + *var_25);
            local_sp_1 = local_sp_0;
            r10_5 = r10_0;
            r10_7 = r10_0;
            local_sp_4 = local_sp_0;
            r95_5 = r95_0;
            rsi8_2 = rsi8_1;
            r89_11 = r89_6;
            r95_7 = r95_0;
            r10_1 = r10_0;
            r95_1 = r95_0;
            rdx7_0 = var_29;
            r89_7 = r89_6;
            local_sp_2 = local_sp_0;
            r10_2 = r10_0;
            r95_2 = r95_0;
            rdx7_1 = var_29;
            r89_8 = r89_6;
            r89_13 = r89_6;
            storemerge = 1UL;
            if ((long)var_29 > (long)r8) {
                if ((long)var_10 >= (long)var_29) {
                    var_56 = r13_0 + 1UL;
                    r10_7 = r10_5;
                    local_sp_0 = local_sp_4;
                    r13_0 = var_56;
                    r10_0 = r10_5;
                    r95_0 = r95_5;
                    rsi8_1 = rsi8_2;
                    r95_7 = r95_5;
                    r89_6 = r89_11;
                    r89_13 = r89_11;
                    storemerge = 0UL;
                    if ((long)var_56 >= (long)rsi8_2) {
                        continue;
                    }
                    break;
                }
                var_45 = *(uint64_t *)(var_27 + 24UL);
                rcx6_1 = var_45;
                if ((long)var_10 > (long)var_45) {
                    break;
                }
            }
            var_31 = *(uint64_t *)(var_27 + 24UL);
            rcx6_0 = var_31;
            rax_4 = 4294967295UL;
            if ((long)var_31 >= (long)r8) {
                r14_1 = r14_0;
                if ((long)var_10 < (long)var_29) {
                    break;
                }
                local_sp_3 = local_sp_1;
                local_sp_2 = local_sp_1;
                r10_2 = r10_1;
                r95_2 = r95_1;
                rcx6_1 = rcx6_0;
                rdx7_1 = rdx7_0;
                r14_2 = r14_1;
                r89_8 = r89_7;
                rax_4 = 1UL;
                r10_3 = r10_1;
                r95_3 = r95_1;
                r14_3 = r14_1;
                r89_9 = r89_7;
                if ((long)var_10 <= (long)rcx6_0) {
                    r10_5 = r10_3;
                    r10_7 = r10_3;
                    local_sp_4 = local_sp_3;
                    r95_5 = r95_3;
                    r89_11 = r89_9;
                    r95_7 = r95_3;
                    r89_13 = r89_9;
                    if ((uint64_t)((uint32_t)r14_3 - (uint32_t)rax_4) == 0UL) {
                        break;
                    }
                    var_55 = *(uint64_t *)r10_3;
                    rsi8_2 = var_55;
                    var_56 = r13_0 + 1UL;
                    r10_7 = r10_5;
                    local_sp_0 = local_sp_4;
                    r13_0 = var_56;
                    r10_0 = r10_5;
                    r95_0 = r95_5;
                    rsi8_1 = rsi8_2;
                    r95_7 = r95_5;
                    r89_6 = r89_11;
                    r89_13 = r89_11;
                    storemerge = 0UL;
                    if ((long)var_56 >= (long)rsi8_2) {
                        continue;
                    }
                    break;
                }
                var_46 = ((var_10 == rcx6_1) ? 2UL : 0UL) | (var_10 == rdx7_1);
                local_sp_3 = local_sp_2;
                r10_3 = r10_2;
                r95_3 = r95_2;
                r14_3 = r14_2;
                r89_9 = r89_8;
                if (var_46 == 0UL) {
                    var_47 = (uint64_t *)(local_sp_2 + 32UL);
                    var_48 = *var_47;
                    var_49 = *(uint64_t *)(local_sp_2 + 16UL);
                    *(uint64_t *)(local_sp_2 + 48UL) = r95_2;
                    var_50 = (uint64_t *)(local_sp_2 + 40UL);
                    *var_50 = r10_2;
                    var_51 = local_sp_2 + (-8L);
                    *(uint64_t *)var_51 = 4255247UL;
                    var_52 = indirect_placeholder_8(rdi, var_49, var_30, var_46, var_48);
                    var_53 = *var_47;
                    var_54 = *var_50;
                    local_sp_3 = var_51;
                    rax_4 = var_52;
                    r10_3 = var_53;
                    r95_3 = var_54;
                    r89_9 = var_48;
                }
                r10_5 = r10_3;
                r10_7 = r10_3;
                local_sp_4 = local_sp_3;
                r95_5 = r95_3;
                r89_11 = r89_9;
                r95_7 = r95_3;
                r89_13 = r89_9;
                if ((uint64_t)((uint32_t)r14_3 - (uint32_t)rax_4) == 0UL) {
                    break;
                }
                var_55 = *(uint64_t *)r10_3;
                rsi8_2 = var_55;
                var_56 = r13_0 + 1UL;
                r10_7 = r10_5;
                local_sp_0 = local_sp_4;
                r13_0 = var_56;
                r10_0 = r10_5;
                r95_0 = r95_5;
                rsi8_1 = rsi8_2;
                r95_7 = r95_5;
                r89_6 = r89_11;
                r89_13 = r89_11;
                storemerge = 0UL;
                if ((long)var_56 >= (long)rsi8_2) {
                    continue;
                }
                break;
            }
            var_32 = ((var_31 == r8) ? 2UL : 0UL) | (var_29 == r8);
            r14_0 = 0UL;
            if (var_32 == 0UL) {
                r14_1 = r14_0;
                if ((long)var_10 < (long)var_29) {
                    break;
                }
            }
            var_33 = *(uint64_t *)(local_sp_0 + 24UL);
            var_34 = *(uint64_t *)(local_sp_0 + 8UL);
            *(uint64_t *)(local_sp_0 + 56UL) = r95_0;
            var_35 = (uint64_t *)(local_sp_0 + 48UL);
            *var_35 = r10_0;
            var_36 = (uint64_t *)(local_sp_0 + 40UL);
            *var_36 = var_26;
            var_37 = local_sp_0 + (-8L);
            *(uint64_t *)var_37 = 4255364UL;
            var_38 = indirect_placeholder_8(rdi, var_34, var_30, var_32, var_33);
            var_39 = *var_35;
            var_40 = (uint64_t)(uint32_t)var_38;
            var_41 = *(uint64_t *)(local_sp_0 + 32UL);
            var_42 = *var_36;
            var_43 = (*(uint64_t *)(var_41 + *(uint64_t *)var_39) * 40UL) + *var_11;
            var_44 = *(uint64_t *)(var_43 + 16UL);
            local_sp_1 = var_37;
            local_sp_3 = var_37;
            r10_1 = var_42;
            r95_1 = var_39;
            rdx7_0 = var_44;
            r14_1 = var_40;
            r89_7 = var_33;
            r10_3 = var_42;
            r95_3 = var_39;
            r14_3 = var_40;
            r89_9 = var_33;
            if ((long)var_10 >= (long)var_44) {
                r10_5 = r10_3;
                r10_7 = r10_3;
                local_sp_4 = local_sp_3;
                r95_5 = r95_3;
                r89_11 = r89_9;
                r95_7 = r95_3;
                r89_13 = r89_9;
                if ((uint64_t)((uint32_t)r14_3 - (uint32_t)rax_4) == 0UL) {
                    break;
                }
                var_55 = *(uint64_t *)r10_3;
                rsi8_2 = var_55;
                var_56 = r13_0 + 1UL;
                r10_7 = r10_5;
                local_sp_0 = local_sp_4;
                r13_0 = var_56;
                r10_0 = r10_5;
                r95_0 = r95_5;
                rsi8_1 = rsi8_2;
                r95_7 = r95_5;
                r89_6 = r89_11;
                r89_13 = r89_11;
                storemerge = 0UL;
                if ((long)var_56 >= (long)rsi8_2) {
                    continue;
                }
                break;
            }
            rcx6_0 = *(uint64_t *)(var_43 + 24UL);
        }
    mrv.field_0 = storemerge;
    mrv1 = mrv;
    mrv1.field_1 = r10_7;
    mrv2 = mrv1;
    mrv2.field_2 = r95_7;
    mrv3 = mrv2;
    mrv3.field_3 = r89_13;
    return mrv3;
}

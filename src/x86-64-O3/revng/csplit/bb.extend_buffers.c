typedef unsigned char __u_char;
typedef unsigned short __u_short;
typedef unsigned int __u_int;
typedef unsigned long __u_long;
typedef signed char __int8_t;
typedef unsigned char __uint8_t;
typedef short __int16_t;
typedef unsigned short __uint16_t;
typedef int __int32_t;
typedef unsigned int __uint32_t;
typedef long __int64_t;
typedef unsigned long __uint64_t;
typedef __int8_t __int_least8_t;
typedef __uint8_t __uint_least8_t;
typedef __int16_t __int_least16_t;
typedef __uint16_t __uint_least16_t;
typedef __int32_t __int_least32_t;
typedef __uint32_t __uint_least32_t;
typedef __int64_t __int_least64_t;
typedef __uint64_t __uint_least64_t;
typedef long __quad_t;
typedef unsigned long __u_quad_t;
typedef long __intmax_t;
typedef unsigned long __uintmax_t;
typedef unsigned long __dev_t;
typedef unsigned int __uid_t;
typedef unsigned int __gid_t;
typedef unsigned long __ino_t;
typedef unsigned long __ino64_t;
typedef unsigned int __mode_t;
typedef unsigned long __nlink_t;
typedef long __off_t;
typedef long __off64_t;
typedef int __pid_t;
typedef struct {
    int __val[2];
} __fsid_t;
typedef long __clock_t;
typedef unsigned long __rlim_t;
typedef unsigned long __rlim64_t;
typedef unsigned int __id_t;
typedef long __time_t;
typedef unsigned int __useconds_t;
typedef long __suseconds_t;
typedef int __daddr_t;
typedef int __key_t;
typedef int __clockid_t;
typedef void *__timer_t;
typedef long __blksize_t;
typedef long __blkcnt_t;
typedef long __blkcnt64_t;
typedef unsigned long __fsblkcnt_t;
typedef unsigned long __fsblkcnt64_t;
typedef unsigned long __fsfilcnt_t;
typedef unsigned long __fsfilcnt64_t;
typedef long __fsword_t;
typedef long __ssize_t;
typedef long __syscall_slong_t;
typedef unsigned long __syscall_ulong_t;
typedef __off64_t __loff_t;
typedef char *__caddr_t;
typedef long __intptr_t;
typedef unsigned int __socklen_t;
typedef int __sig_atomic_t;
typedef __int8_t int8_t;
typedef __int16_t int16_t;
typedef __int32_t int32_t;
typedef __int64_t int64_t;
typedef __uint8_t uint8_t;
typedef __uint16_t uint16_t;
typedef __uint32_t uint32_t;
typedef __uint64_t uint64_t;
typedef __int_least8_t int_least8_t;
typedef __int_least16_t int_least16_t;
typedef __int_least32_t int_least32_t;
typedef __int_least64_t int_least64_t;
typedef __uint_least8_t uint_least8_t;
typedef __uint_least16_t uint_least16_t;
typedef __uint_least32_t uint_least32_t;
typedef __uint_least64_t uint_least64_t;
typedef signed char int_fast8_t;
typedef long int_fast16_t;
typedef long int_fast32_t;
typedef long int_fast64_t;
typedef unsigned char uint_fast8_t;
typedef unsigned long uint_fast16_t;
typedef unsigned long uint_fast32_t;
typedef unsigned long uint_fast64_t;
typedef long intptr_t;
typedef unsigned long uintptr_t;
typedef __intmax_t intmax_t;
typedef __uintmax_t uintmax_t;
typedef _Bool bool;
struct bb_extend_buffers_ret_type;
struct bb_extend_buffers_ret_type {
    uint64_t field_0;
    uint64_t field_1;
    uint64_t field_2;
    uint64_t field_3;
};
extern uint64_t revng_init_local_sp(uint64_t param_0);
extern uint64_t init_rbx(void);
extern uint64_t init_r12(void);
extern uint64_t init_rbp(void);
extern void indirect_placeholder(void);
extern uint64_t init_r13(void);
extern void indirect_placeholder_6(uint64_t param_0);
extern void indirect_placeholder_4(uint64_t param_0, uint64_t param_1);
extern uint64_t init_r10(void);
extern uint64_t init_r9(void);
extern uint64_t init_r8(void);
struct bb_extend_buffers_ret_type bb_extend_buffers(uint64_t rdi, uint64_t rsi) {
    uint64_t var_0;
    uint64_t var_1;
    uint64_t var_2;
    uint64_t var_3;
    uint64_t var_4;
    uint64_t var_5;
    uint64_t var_6;
    uint64_t var_7;
    uint64_t *var_8;
    uint64_t var_9;
    uint64_t local_sp_0;
    uint64_t local_sp_1;
    uint64_t local_sp_2;
    uint64_t var_24;
    uint64_t var_25;
    uint64_t rax_0;
    uint64_t *var_18;
    uint64_t var_19;
    uint64_t var_20;
    uint64_t var_21;
    uint64_t *var_22;
    uint64_t var_23;
    uint64_t var_10;
    uint64_t *var_11;
    uint64_t var_12;
    uint64_t var_13;
    uint64_t var_14;
    uint64_t var_15;
    uint64_t var_16;
    uint32_t *var_17;
    struct bb_extend_buffers_ret_type mrv6;
    struct bb_extend_buffers_ret_type mrv7;
    uint64_t *var_29;
    uint64_t var_30;
    uint64_t var_31;
    uint64_t var_32;
    bool var_33;
    bool var_34;
    struct bb_extend_buffers_ret_type mrv4;
    struct bb_extend_buffers_ret_type mrv5;
    struct bb_extend_buffers_ret_type mrv2;
    struct bb_extend_buffers_ret_type mrv3;
    uint64_t var_35;
    uint64_t var_36;
    uint64_t var_37;
    uint64_t *var_38;
    uint64_t var_39;
    uint64_t rbp_1;
    uint64_t rbp_0;
    uint64_t local_sp_3;
    unsigned char var_40;
    uint64_t var_41;
    uint64_t var_42;
    uint64_t var_43;
    uint64_t *var_44;
    uint64_t var_45;
    uint64_t var_46;
    uint64_t var_47;
    uint64_t var_48;
    uint64_t *var_49;
    uint64_t var_50;
    uint64_t rax_2;
    uint64_t rax_1;
    uint64_t rcx_0;
    uint64_t var_51;
    struct bb_extend_buffers_ret_type mrv9 = {0UL, /*implicit*/(int)0, /*implicit*/(int)0, /*implicit*/(int)0};
    struct bb_extend_buffers_ret_type mrv10;
    struct bb_extend_buffers_ret_type mrv11;
    struct bb_extend_buffers_ret_type mrv1 = {0UL, /*implicit*/(int)0, /*implicit*/(int)0, /*implicit*/(int)0};
    uint64_t *var_26;
    uint64_t var_27;
    uint64_t var_28;
    struct bb_extend_buffers_ret_type mrv13 = {12UL, /*implicit*/(int)0, /*implicit*/(int)0, /*implicit*/(int)0};
    struct bb_extend_buffers_ret_type mrv14;
    struct bb_extend_buffers_ret_type mrv15;
    var_0 = revng_init_local_sp(0UL);
    var_1 = init_r13();
    var_2 = init_rbx();
    var_3 = init_r10();
    var_4 = init_r12();
    var_5 = init_r9();
    var_6 = init_rbp();
    var_7 = init_r8();
    *(uint64_t *)(var_0 + (-8L)) = var_1;
    *(uint64_t *)(var_0 + (-16L)) = var_4;
    *(uint64_t *)(var_0 + (-24L)) = var_6;
    *(uint64_t *)(var_0 + (-32L)) = var_2;
    var_8 = (uint64_t *)(rdi + 64UL);
    var_9 = *var_8;
    if (var_9 > 1152921504606846974UL) {
        mrv13.field_1 = var_3;
        mrv14 = mrv13;
        mrv14.field_2 = var_5;
        mrv15 = mrv14;
        mrv15.field_3 = var_7;
        return mrv15;
    }
    var_10 = var_0 + (-40L);
    var_11 = (uint64_t *)(rdi + 88UL);
    var_12 = *var_11;
    var_13 = var_9 << 1UL;
    var_14 = (uint64_t)((long)(rsi << 32UL) >> (long)32UL);
    var_15 = ((long)var_13 > (long)var_12) ? var_12 : var_13;
    var_16 = ((long)var_14 < (long)var_15) ? var_15 : var_14;
    var_17 = (uint32_t *)(rdi + 144UL);
    rax_0 = var_12;
    local_sp_0 = var_10;
    if ((int)*var_17 <= (int)1U) {
        rax_0 = 2305843009213693951UL;
        if (var_16 > 2305843009213693951UL) {
            mrv13.field_1 = var_3;
            mrv14 = mrv13;
            mrv14.field_2 = var_5;
            mrv15 = mrv14;
            mrv15.field_3 = var_7;
            return mrv15;
        }
        var_18 = (uint64_t *)(rdi + 16UL);
        var_19 = *var_18;
        var_20 = var_16 << 2UL;
        var_21 = var_0 + (-48L);
        *(uint64_t *)var_21 = 4248297UL;
        indirect_placeholder_4(var_19, var_20);
        var_22 = (uint64_t *)(rdi + 24UL);
        var_23 = *var_22;
        *var_18 = 2305843009213693951UL;
        local_sp_0 = var_21;
        if (var_23 == 0UL) {
            var_24 = var_16 << 3UL;
            var_25 = var_0 + (-56L);
            *(uint64_t *)var_25 = 4248332UL;
            indirect_placeholder_4(var_23, var_24);
            *var_22 = 2305843009213693951UL;
            local_sp_0 = var_25;
        }
    }
    local_sp_1 = local_sp_0;
    if (*(unsigned char *)(rdi + 139UL) != '\x00') {
        var_26 = (uint64_t *)(rdi + 8UL);
        var_27 = *var_26;
        var_28 = local_sp_0 + (-8L);
        *(uint64_t *)var_28 = 4248572UL;
        indirect_placeholder_4(var_27, var_16);
        local_sp_1 = var_28;
        if (rax_0 != 0UL) {
            mrv13.field_1 = var_3;
            mrv14 = mrv13;
            mrv14.field_2 = var_5;
            mrv15 = mrv14;
            mrv15.field_3 = var_7;
            return mrv15;
        }
        *var_26 = rax_0;
    }
    var_29 = (uint64_t *)(rdi + 184UL);
    var_30 = *var_29;
    *var_8 = var_16;
    local_sp_2 = local_sp_1;
    if (var_30 != 0UL) {
        var_31 = (var_16 << 3UL) + 8UL;
        var_32 = local_sp_1 + (-8L);
        *(uint64_t *)var_32 = 4248387UL;
        indirect_placeholder_4(var_30, var_31);
        local_sp_2 = var_32;
        if (rax_0 != 0UL) {
            mrv13.field_1 = var_3;
            mrv14 = mrv13;
            mrv14.field_2 = var_5;
            mrv15 = mrv14;
            mrv15.field_3 = var_7;
            return mrv15;
        }
        *var_29 = rax_0;
    }
    var_33 = (*(unsigned char *)(rdi + 136UL) == '\x00');
    var_34 = ((int)*var_17 > (int)1U);
    local_sp_3 = local_sp_2;
    if (var_33) {
        if (var_34) {
            *(uint64_t *)(local_sp_2 + (-8L)) = 4248632UL;
            indirect_placeholder_6(rdi);
            mrv1.field_1 = var_3;
            mrv2 = mrv1;
            mrv2.field_2 = var_5;
            mrv3 = mrv2;
            mrv3.field_3 = var_7;
            return mrv3;
        }
        var_44 = (uint64_t *)(rdi + 120UL);
        var_45 = *var_44;
        rcx_0 = var_45;
        if (var_45 != 0UL) {
            var_46 = *var_8;
            var_47 = *var_11;
            var_48 = ((long)var_47 > (long)var_46) ? var_46 : var_47;
            var_49 = (uint64_t *)(rdi + 48UL);
            var_50 = *var_49;
            rax_1 = var_50;
            rax_2 = var_50;
            if ((long)var_48 <= (long)var_50) {
                rax_2 = var_48;
                *(unsigned char *)(rax_1 + *(uint64_t *)(rdi + 8UL)) = *(unsigned char *)(rcx_0 + (uint64_t)*(unsigned char *)(rax_1 + (*(uint64_t *)(rdi + 40UL) + *(uint64_t *)rdi)));
                var_51 = rax_1 + 1UL;
                rax_1 = var_51;
                while (var_51 != var_48)
                    {
                        rcx_0 = *var_44;
                        *(unsigned char *)(rax_1 + *(uint64_t *)(rdi + 8UL)) = *(unsigned char *)(rcx_0 + (uint64_t)*(unsigned char *)(rax_1 + (*(uint64_t *)(rdi + 40UL) + *(uint64_t *)rdi)));
                        var_51 = rax_1 + 1UL;
                        rax_1 = var_51;
                    }
            }
            *var_49 = rax_2;
            *(uint64_t *)(rdi + 56UL) = rax_2;
        }
    } else {
        if (var_34) {
            indirect_placeholder();
            mrv4.field_0 = rax_0;
            mrv5 = mrv4;
            mrv5.field_1 = var_3;
            mrv6 = mrv5;
            mrv6.field_2 = var_5;
            mrv7 = mrv6;
            mrv7.field_3 = var_7;
            return mrv7;
        }
        var_35 = *var_8;
        var_36 = *var_11;
        var_37 = ((long)var_36 > (long)var_35) ? var_35 : var_36;
        var_38 = (uint64_t *)(rdi + 48UL);
        var_39 = *var_38;
        rbp_0 = var_39;
        rbp_1 = var_39;
        if ((long)var_37 <= (long)var_39) {
            rbp_0 = var_37;
            var_40 = (unsigned char)*(uint64_t *)(rdi + 40UL) + (unsigned char)*(uint64_t *)rdi;
            var_41 = rbp_1 + *(uint64_t *)(rdi + 8UL);
            var_42 = rbp_1 + 1UL;
            var_43 = local_sp_3 + (-8L);
            *(uint64_t *)var_43 = 4248688UL;
            indirect_placeholder();
            *(unsigned char *)var_41 = var_40;
            rbp_1 = var_42;
            local_sp_3 = var_43;
            do {
                var_40 = (unsigned char)*(uint64_t *)(rdi + 40UL) + (unsigned char)*(uint64_t *)rdi;
                var_41 = rbp_1 + *(uint64_t *)(rdi + 8UL);
                var_42 = rbp_1 + 1UL;
                var_43 = local_sp_3 + (-8L);
                *(uint64_t *)var_43 = 4248688UL;
                indirect_placeholder();
                *(unsigned char *)var_41 = var_40;
                rbp_1 = var_42;
                local_sp_3 = var_43;
            } while (var_42 != var_37);
        }
        *var_38 = rbp_0;
        *(uint64_t *)(rdi + 56UL) = rbp_0;
    }
    mrv9.field_1 = var_3;
    mrv10 = mrv9;
    mrv10.field_2 = var_5;
    mrv11 = mrv10;
    mrv11.field_3 = var_7;
    return mrv11;
}

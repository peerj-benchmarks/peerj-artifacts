typedef unsigned char __u_char;
typedef unsigned short __u_short;
typedef unsigned int __u_int;
typedef unsigned long __u_long;
typedef signed char __int8_t;
typedef unsigned char __uint8_t;
typedef short __int16_t;
typedef unsigned short __uint16_t;
typedef int __int32_t;
typedef unsigned int __uint32_t;
typedef long __int64_t;
typedef unsigned long __uint64_t;
typedef __int8_t __int_least8_t;
typedef __uint8_t __uint_least8_t;
typedef __int16_t __int_least16_t;
typedef __uint16_t __uint_least16_t;
typedef __int32_t __int_least32_t;
typedef __uint32_t __uint_least32_t;
typedef __int64_t __int_least64_t;
typedef __uint64_t __uint_least64_t;
typedef long __quad_t;
typedef unsigned long __u_quad_t;
typedef long __intmax_t;
typedef unsigned long __uintmax_t;
typedef unsigned long __dev_t;
typedef unsigned int __uid_t;
typedef unsigned int __gid_t;
typedef unsigned long __ino_t;
typedef unsigned long __ino64_t;
typedef unsigned int __mode_t;
typedef unsigned long __nlink_t;
typedef long __off_t;
typedef long __off64_t;
typedef int __pid_t;
typedef struct {
    int __val[2];
} __fsid_t;
typedef long __clock_t;
typedef unsigned long __rlim_t;
typedef unsigned long __rlim64_t;
typedef unsigned int __id_t;
typedef long __time_t;
typedef unsigned int __useconds_t;
typedef long __suseconds_t;
typedef int __daddr_t;
typedef int __key_t;
typedef int __clockid_t;
typedef void *__timer_t;
typedef long __blksize_t;
typedef long __blkcnt_t;
typedef long __blkcnt64_t;
typedef unsigned long __fsblkcnt_t;
typedef unsigned long __fsblkcnt64_t;
typedef unsigned long __fsfilcnt_t;
typedef unsigned long __fsfilcnt64_t;
typedef long __fsword_t;
typedef long __ssize_t;
typedef long __syscall_slong_t;
typedef unsigned long __syscall_ulong_t;
typedef __off64_t __loff_t;
typedef char *__caddr_t;
typedef long __intptr_t;
typedef unsigned int __socklen_t;
typedef int __sig_atomic_t;
typedef __int8_t int8_t;
typedef __int16_t int16_t;
typedef __int32_t int32_t;
typedef __int64_t int64_t;
typedef __uint8_t uint8_t;
typedef __uint16_t uint16_t;
typedef __uint32_t uint32_t;
typedef __uint64_t uint64_t;
typedef __int_least8_t int_least8_t;
typedef __int_least16_t int_least16_t;
typedef __int_least32_t int_least32_t;
typedef __int_least64_t int_least64_t;
typedef __uint_least8_t uint_least8_t;
typedef __uint_least16_t uint_least16_t;
typedef __uint_least32_t uint_least32_t;
typedef __uint_least64_t uint_least64_t;
typedef signed char int_fast8_t;
typedef long int_fast16_t;
typedef long int_fast32_t;
typedef long int_fast64_t;
typedef unsigned char uint_fast8_t;
typedef unsigned long uint_fast16_t;
typedef unsigned long uint_fast32_t;
typedef unsigned long uint_fast64_t;
typedef long intptr_t;
typedef unsigned long uintptr_t;
typedef __intmax_t intmax_t;
typedef __uintmax_t uintmax_t;
struct indirect_placeholder_280_ret_type;
struct indirect_placeholder_281_ret_type;
struct indirect_placeholder_282_ret_type;
struct indirect_placeholder_283_ret_type;
struct indirect_placeholder_280_ret_type {
    uint64_t field_0;
    uint64_t field_1;
};
struct indirect_placeholder_281_ret_type {
    uint64_t field_0;
    uint64_t field_1;
    uint64_t field_2;
    uint64_t field_3;
};
struct indirect_placeholder_282_ret_type {
    uint64_t field_0;
    uint64_t field_1;
    uint64_t field_2;
    uint64_t field_3;
};
struct indirect_placeholder_283_ret_type {
    uint64_t field_0;
    uint64_t field_1;
    uint64_t field_2;
    uint64_t field_3;
};
extern uint64_t revng_init_local_sp(uint64_t param_0);
extern uint64_t init_rbx(void);
extern uint64_t init_r12(void);
extern uint64_t init_rbp(void);
extern void indirect_placeholder(void);
extern uint64_t init_r13(void);
extern uint64_t init_r14(void);
extern uint64_t indirect_placeholder_8(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint64_t param_3, uint64_t param_4);
extern uint64_t init_r15(void);
extern uint64_t indirect_placeholder_3(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint64_t param_3);
extern struct indirect_placeholder_280_ret_type indirect_placeholder_280(uint64_t param_0);
extern struct indirect_placeholder_281_ret_type indirect_placeholder_281(uint64_t param_0, uint64_t param_1);
extern struct indirect_placeholder_282_ret_type indirect_placeholder_282(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint64_t param_3, uint64_t param_4);
extern struct indirect_placeholder_283_ret_type indirect_placeholder_283(uint64_t param_0, uint64_t param_1);
typedef _Bool bool;
uint64_t bb_calc_eclosure_iter(uint64_t rdi, uint64_t rcx, uint64_t rdx, uint64_t rsi) {
    uint64_t var_24;
    uint64_t local_sp_3;
    uint64_t var_25;
    uint64_t var_26;
    struct indirect_placeholder_282_ret_type var_46;
    uint64_t var_27;
    uint64_t var_0;
    uint64_t var_1;
    uint64_t var_2;
    uint64_t var_3;
    uint64_t var_4;
    uint64_t var_5;
    uint64_t var_6;
    uint64_t *var_7;
    uint64_t var_8;
    uint64_t var_9;
    uint64_t var_10;
    uint64_t *var_11;
    uint64_t var_12;
    uint64_t var_13;
    uint64_t var_14;
    struct indirect_placeholder_280_ret_type var_15;
    uint64_t var_16;
    uint64_t _pre_phi;
    uint64_t local_sp_5;
    uint64_t local_sp_7;
    uint64_t local_sp_6;
    uint64_t merge;
    uint64_t r13_2;
    uint64_t r12_1;
    uint64_t local_sp_0;
    uint64_t rbp_0;
    uint64_t *var_60;
    uint64_t var_61;
    uint64_t var_62;
    uint64_t *var_63;
    uint64_t *var_64;
    uint64_t *_pre_phi142;
    uint64_t *_pre133;
    uint64_t r12_0_ph;
    uint64_t rbp_1_ph;
    uint64_t r14_0_ph;
    uint64_t *_pre137;
    uint64_t *_pre141;
    uint64_t *_pre_phi138;
    uint64_t *_pre_phi134;
    uint64_t local_sp_1;
    uint64_t var_65;
    uint64_t r10_0;
    uint64_t local_sp_2;
    uint64_t r14_0;
    uint64_t r13_0;
    uint64_t var_51;
    uint64_t var_52;
    uint64_t var_53;
    uint64_t local_sp_4_ph;
    uint64_t var_50;
    uint64_t *var_17;
    uint64_t var_18;
    uint64_t *_cast;
    uint64_t var_19;
    uint64_t var_20;
    uint64_t var_21;
    uint32_t var_22;
    uint64_t var_23;
    uint64_t var_28;
    uint64_t var_29;
    uint64_t rax_2_ph;
    uint64_t r13_1_ph;
    uint64_t rsi4_0_ph;
    uint64_t var_30;
    uint64_t var_31;
    uint64_t *var_32;
    uint64_t rax_2;
    uint64_t r13_1;
    uint64_t var_33;
    uint64_t var_34;
    uint64_t var_35;
    uint64_t *var_36;
    uint64_t var_41;
    uint64_t rbp_2;
    uint64_t var_54;
    uint64_t var_55;
    struct indirect_placeholder_281_ret_type var_56;
    uint64_t var_42;
    uint64_t var_43;
    uint64_t var_44;
    uint64_t var_45;
    uint64_t var_47;
    uint64_t var_48;
    uint64_t var_49;
    uint64_t var_37;
    uint64_t var_38;
    uint64_t var_39;
    uint64_t var_40;
    uint64_t var_57;
    uint64_t var_58;
    struct indirect_placeholder_283_ret_type var_59;
    unsigned int loop_state_var;
    bool switch_state_var;
    var_0 = revng_init_local_sp(0UL);
    var_1 = init_r15();
    var_2 = init_r13();
    var_3 = init_rbx();
    var_4 = init_r12();
    var_5 = init_rbp();
    var_6 = init_r14();
    *(uint64_t *)(var_0 + (-8L)) = var_1;
    *(uint64_t *)(var_0 + (-16L)) = var_6;
    *(uint64_t *)(var_0 + (-24L)) = var_2;
    *(uint64_t *)(var_0 + (-32L)) = var_4;
    *(uint64_t *)(var_0 + (-40L)) = var_5;
    *(uint64_t *)(var_0 + (-48L)) = var_3;
    var_7 = (uint64_t *)(rsi + 40UL);
    var_8 = *var_7;
    var_9 = rdx * 24UL;
    *(uint64_t *)(var_0 + (-136L)) = rdi;
    *(uint32_t *)(var_0 + (-124L)) = (uint32_t)rcx;
    var_10 = *(uint64_t *)((var_9 + var_8) + 8UL);
    var_11 = (uint64_t *)(var_0 + (-112L));
    *var_11 = 0UL;
    var_12 = var_10 + 1UL;
    var_13 = var_12 << 3UL;
    *(uint64_t *)(var_0 + (-120L)) = var_12;
    var_14 = var_0 + (-160L);
    *(uint64_t *)var_14 = 4250301UL;
    var_15 = indirect_placeholder_280(var_13);
    var_16 = var_15.field_0;
    *var_11 = var_16;
    local_sp_3 = var_14;
    local_sp_7 = var_14;
    merge = 12UL;
    r13_2 = 1UL;
    rbp_0 = var_9;
    r12_0_ph = rdx;
    rbp_1_ph = var_9;
    r14_0_ph = 0UL;
    r13_1_ph = 0UL;
    if ((var_16 != 0UL) || (var_12 == 0UL)) {
        return merge;
    }
    var_17 = (uint64_t *)(rsi + 48UL);
    var_18 = *var_17;
    _cast = (uint64_t *)rsi;
    var_19 = *_cast;
    var_20 = rdx << 4UL;
    *(uint64_t *)((var_9 + var_18) + 8UL) = 18446744073709551615UL;
    var_21 = (var_20 + var_19) + 8UL;
    var_22 = *(uint32_t *)var_21;
    var_23 = (uint64_t)var_22;
    _pre_phi = var_21;
    if ((uint64_t)(var_22 & 261888U) != 0UL) {
        var_24 = var_9 + *var_7;
        if (*(uint64_t *)(var_24 + 8UL) != 0UL) {
            var_57 = local_sp_7 + 32UL;
            var_58 = local_sp_7 + (-8L);
            *(uint64_t *)var_58 = 4250781UL;
            var_59 = indirect_placeholder_283(var_57, rdx);
            local_sp_0 = var_58;
            if ((uint64_t)(unsigned char)var_59.field_0 == 0UL) {
                return merge;
            }
            var_60 = (uint64_t *)(local_sp_0 + 32UL);
            var_61 = *var_60;
            var_62 = rbp_0 + *var_17;
            *(uint64_t *)var_62 = var_61;
            var_63 = (uint64_t *)(local_sp_0 + 40UL);
            *(uint64_t *)(var_62 + 8UL) = *var_63;
            var_64 = (uint64_t *)(local_sp_0 + 48UL);
            *(uint64_t *)(var_62 + 16UL) = *var_64;
            _pre_phi142 = var_64;
            _pre_phi138 = var_63;
            _pre_phi134 = var_60;
            local_sp_1 = local_sp_0;
            var_65 = *(uint64_t *)(local_sp_1 + 16UL);
            *(uint64_t *)var_65 = *_pre_phi134;
            *(uint64_t *)(var_65 + 8UL) = *_pre_phi138;
            *(uint64_t *)(var_65 + 16UL) = *_pre_phi142;
            return 0UL;
        }
        if ((*(unsigned char *)(((**(uint64_t **)(var_24 + 16UL) << 4UL) + var_19) + 10UL) & '\x04') != '\x00') {
            var_25 = (uint64_t)((uint16_t)(var_23 >> 8UL) & (unsigned short)1023U);
            var_26 = var_0 + (-168L);
            *(uint64_t *)var_26 = 4250864UL;
            var_27 = indirect_placeholder_8(rsi, rdx, rdx, rdx, var_25);
            merge = var_27;
            local_sp_3 = var_26;
            if ((uint64_t)(uint32_t)var_27 != 0UL) {
                return merge;
            }
            _pre_phi = (var_20 + *_cast) + 8UL;
        }
    }
    local_sp_4_ph = local_sp_3;
    local_sp_7 = local_sp_3;
    if ((*(unsigned char *)_pre_phi & '\b') != '\x00') {
        var_57 = local_sp_7 + 32UL;
        var_58 = local_sp_7 + (-8L);
        *(uint64_t *)var_58 = 4250781UL;
        var_59 = indirect_placeholder_283(var_57, rdx);
        local_sp_0 = var_58;
        if ((uint64_t)(unsigned char)var_59.field_0 == 0UL) {
            return merge;
        }
        var_60 = (uint64_t *)(local_sp_0 + 32UL);
        var_61 = *var_60;
        var_62 = rbp_0 + *var_17;
        *(uint64_t *)var_62 = var_61;
        var_63 = (uint64_t *)(local_sp_0 + 40UL);
        *(uint64_t *)(var_62 + 8UL) = *var_63;
        var_64 = (uint64_t *)(local_sp_0 + 48UL);
        *(uint64_t *)(var_62 + 16UL) = *var_64;
        _pre_phi142 = var_64;
        _pre_phi138 = var_63;
        _pre_phi134 = var_60;
        local_sp_1 = local_sp_0;
        var_65 = *(uint64_t *)(local_sp_1 + 16UL);
        *(uint64_t *)var_65 = *_pre_phi134;
        *(uint64_t *)(var_65 + 8UL) = *_pre_phi138;
        *(uint64_t *)(var_65 + 16UL) = *_pre_phi142;
        return 0UL;
    }
    var_28 = *var_7;
    var_29 = var_9 + var_28;
    rax_2_ph = var_29;
    rsi4_0_ph = var_28;
    if ((long)*(uint64_t *)(var_29 + 8UL) > (long)0UL) {
        var_57 = local_sp_7 + 32UL;
        var_58 = local_sp_7 + (-8L);
        *(uint64_t *)var_58 = 4250781UL;
        var_59 = indirect_placeholder_283(var_57, rdx);
        local_sp_0 = var_58;
        if ((uint64_t)(unsigned char)var_59.field_0 == 0UL) {
            return merge;
        }
    }
    while (1U)
        {
            var_30 = *var_17;
            var_31 = rbp_1_ph + rsi4_0_ph;
            var_32 = (uint64_t *)(var_31 + 8UL);
            local_sp_5 = local_sp_4_ph;
            local_sp_6 = local_sp_4_ph;
            r12_1 = r12_0_ph;
            r14_0 = r14_0_ph;
            rax_2 = rax_2_ph;
            r13_1 = r13_1_ph;
            rbp_2 = rbp_1_ph;
            while (1U)
                {
                    var_33 = *(uint64_t *)((r14_0 << 3UL) + *(uint64_t *)(rax_2 + 16UL));
                    var_34 = var_33 * 24UL;
                    var_35 = var_34 + var_30;
                    var_36 = (uint64_t *)(var_35 + 8UL);
                    r13_0 = r13_1;
                    rax_2 = var_31;
                    r13_1 = 1UL;
                    switch_state_var = 0;
                    switch (*var_36) {
                      case 18446744073709551615UL:
                        {
                            var_41 = r14_0 + 1UL;
                            r14_0 = var_41;
                            if ((long)*var_32 > (long)var_41) {
                                continue;
                            }
                            loop_state_var = 2U;
                            switch_state_var = 1;
                            break;
                        }
                        break;
                      case 0UL:
                        {
                            loop_state_var = 0U;
                            switch_state_var = 1;
                            break;
                        }
                        break;
                      default:
                        {
                            loop_state_var = 1U;
                            switch_state_var = 1;
                            break;
                        }
                        break;
                    }
                    if (switch_state_var)
                        break;
                }
            switch_state_var = 0;
            switch (loop_state_var) {
              case 0U:
                {
                    var_37 = local_sp_4_ph + 64UL;
                    *(uint64_t *)(local_sp_4_ph + 8UL) = var_37;
                    var_38 = local_sp_4_ph + (-8L);
                    *(uint64_t *)var_38 = 4250709UL;
                    var_39 = indirect_placeholder_3(var_37, 0UL, var_33, rsi);
                    var_40 = *(uint64_t *)local_sp_4_ph;
                    merge = var_39;
                    local_sp_6 = var_38;
                    r10_0 = var_40;
                    if ((uint64_t)(uint32_t)var_39 != 0UL) {
                        loop_state_var = 1U;
                        switch_state_var = 1;
                        break;
                    }
                    var_44 = local_sp_6 + 32UL;
                    var_45 = local_sp_6 + (-8L);
                    *(uint64_t *)var_45 = 4250477UL;
                    var_46 = indirect_placeholder_282(var_34, var_44, r12_0_ph, rbp_1_ph, r10_0);
                    var_47 = var_46.field_0;
                    var_48 = var_46.field_2;
                    var_49 = var_46.field_3;
                    merge = var_47;
                    r12_1 = var_48;
                    r12_0_ph = var_48;
                    rbp_1_ph = var_49;
                    local_sp_2 = var_45;
                    rbp_2 = var_49;
                    if ((uint64_t)(uint32_t)var_47 != 0UL) {
                        loop_state_var = 1U;
                        switch_state_var = 1;
                        break;
                    }
                    if (*(uint64_t *)((var_46.field_1 + *var_17) + 8UL) == 0UL) {
                        var_50 = local_sp_6 + (-16L);
                        *(uint64_t *)var_50 = 4250760UL;
                        indirect_placeholder();
                        local_sp_2 = var_50;
                        r13_0 = 1UL;
                    }
                    var_51 = *var_7;
                    var_52 = r14_0 + 1UL;
                    var_53 = var_49 + var_51;
                    local_sp_5 = local_sp_2;
                    r13_2 = r13_0;
                    r14_0_ph = var_52;
                    local_sp_4_ph = local_sp_2;
                    rax_2_ph = var_53;
                    r13_1_ph = r13_0;
                    rsi4_0_ph = var_51;
                    if ((long)*(uint64_t *)(var_53 + 8UL) > (long)var_52) {
                        continue;
                    }
                    loop_state_var = 0U;
                    switch_state_var = 1;
                    break;
                }
                break;
              case 1U:
                {
                    var_42 = *(uint64_t *)var_35;
                    var_43 = local_sp_4_ph + 64UL;
                    *(uint64_t *)var_43 = var_42;
                    *(uint64_t *)(local_sp_4_ph + 72UL) = *var_36;
                    *(uint64_t *)(local_sp_4_ph + 80UL) = *(uint64_t *)(var_35 + 16UL);
                    r10_0 = var_43;
                    var_44 = local_sp_6 + 32UL;
                    var_45 = local_sp_6 + (-8L);
                    *(uint64_t *)var_45 = 4250477UL;
                    var_46 = indirect_placeholder_282(var_34, var_44, r12_0_ph, rbp_1_ph, r10_0);
                    var_47 = var_46.field_0;
                    var_48 = var_46.field_2;
                    var_49 = var_46.field_3;
                    merge = var_47;
                    r12_1 = var_48;
                    r12_0_ph = var_48;
                    rbp_1_ph = var_49;
                    local_sp_2 = var_45;
                    rbp_2 = var_49;
                    if ((uint64_t)(uint32_t)var_47 == 0UL) {
                        loop_state_var = 1U;
                        switch_state_var = 1;
                        break;
                    }
                    if (*(uint64_t *)((var_46.field_1 + *var_17) + 8UL) == 0UL) {
                        var_50 = local_sp_6 + (-16L);
                        *(uint64_t *)var_50 = 4250760UL;
                        indirect_placeholder();
                        local_sp_2 = var_50;
                        r13_0 = 1UL;
                    }
                    var_51 = *var_7;
                    var_52 = r14_0 + 1UL;
                    var_53 = var_49 + var_51;
                    local_sp_5 = local_sp_2;
                    r13_2 = r13_0;
                    r14_0_ph = var_52;
                    local_sp_4_ph = local_sp_2;
                    rax_2_ph = var_53;
                    r13_1_ph = r13_0;
                    rsi4_0_ph = var_51;
                    if ((long)*(uint64_t *)(var_53 + 8UL) > (long)var_52) {
                        continue;
                    }
                    loop_state_var = 0U;
                    switch_state_var = 1;
                    break;
                }
                break;
              case 2U:
                {
                    loop_state_var = 0U;
                    switch_state_var = 1;
                    break;
                }
                break;
            }
            if (switch_state_var)
                break;
        }
    switch (loop_state_var) {
      case 1U:
        {
            return merge;
        }
        break;
      case 0U:
        {
            var_54 = local_sp_5 + 32UL;
            var_55 = local_sp_5 + (-8L);
            *(uint64_t *)var_55 = 4250585UL;
            var_56 = indirect_placeholder_281(var_54, r12_1);
            local_sp_0 = var_55;
            rbp_0 = rbp_2;
            local_sp_1 = var_55;
            if ((uint64_t)(unsigned char)var_56.field_0 == 0UL) {
                return merge;
            }
            if ((uint64_t)(unsigned char)r13_2 <= (uint64_t)*(unsigned char *)(local_sp_5 + 20UL)) {
                *(uint64_t *)((rbp_2 + *var_17) + 8UL) = 0UL;
                _pre133 = (uint64_t *)(var_55 + 32UL);
                _pre137 = (uint64_t *)(var_55 + 40UL);
                _pre141 = (uint64_t *)(var_55 + 48UL);
                _pre_phi142 = _pre141;
                _pre_phi138 = _pre137;
                _pre_phi134 = _pre133;
                var_65 = *(uint64_t *)(local_sp_1 + 16UL);
                *(uint64_t *)var_65 = *_pre_phi134;
                *(uint64_t *)(var_65 + 8UL) = *_pre_phi138;
                *(uint64_t *)(var_65 + 16UL) = *_pre_phi142;
                return 0UL;
            }
        }
        break;
    }
}

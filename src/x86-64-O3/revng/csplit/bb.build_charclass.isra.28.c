typedef unsigned char __u_char;
typedef unsigned short __u_short;
typedef unsigned int __u_int;
typedef unsigned long __u_long;
typedef signed char __int8_t;
typedef unsigned char __uint8_t;
typedef short __int16_t;
typedef unsigned short __uint16_t;
typedef int __int32_t;
typedef unsigned int __uint32_t;
typedef long __int64_t;
typedef unsigned long __uint64_t;
typedef __int8_t __int_least8_t;
typedef __uint8_t __uint_least8_t;
typedef __int16_t __int_least16_t;
typedef __uint16_t __uint_least16_t;
typedef __int32_t __int_least32_t;
typedef __uint32_t __uint_least32_t;
typedef __int64_t __int_least64_t;
typedef __uint64_t __uint_least64_t;
typedef long __quad_t;
typedef unsigned long __u_quad_t;
typedef long __intmax_t;
typedef unsigned long __uintmax_t;
typedef unsigned long __dev_t;
typedef unsigned int __uid_t;
typedef unsigned int __gid_t;
typedef unsigned long __ino_t;
typedef unsigned long __ino64_t;
typedef unsigned int __mode_t;
typedef unsigned long __nlink_t;
typedef long __off_t;
typedef long __off64_t;
typedef int __pid_t;
typedef struct {
    int __val[2];
} __fsid_t;
typedef long __clock_t;
typedef unsigned long __rlim_t;
typedef unsigned long __rlim64_t;
typedef unsigned int __id_t;
typedef long __time_t;
typedef unsigned int __useconds_t;
typedef long __suseconds_t;
typedef int __daddr_t;
typedef int __key_t;
typedef int __clockid_t;
typedef void *__timer_t;
typedef long __blksize_t;
typedef long __blkcnt_t;
typedef long __blkcnt64_t;
typedef unsigned long __fsblkcnt_t;
typedef unsigned long __fsblkcnt64_t;
typedef unsigned long __fsfilcnt_t;
typedef unsigned long __fsfilcnt64_t;
typedef long __fsword_t;
typedef long __ssize_t;
typedef long __syscall_slong_t;
typedef unsigned long __syscall_ulong_t;
typedef __off64_t __loff_t;
typedef char *__caddr_t;
typedef long __intptr_t;
typedef unsigned int __socklen_t;
typedef int __sig_atomic_t;
typedef __int8_t int8_t;
typedef __int16_t int16_t;
typedef __int32_t int32_t;
typedef __int64_t int64_t;
typedef __uint8_t uint8_t;
typedef __uint16_t uint16_t;
typedef __uint32_t uint32_t;
typedef __uint64_t uint64_t;
typedef __int_least8_t int_least8_t;
typedef __int_least16_t int_least16_t;
typedef __int_least32_t int_least32_t;
typedef __int_least64_t int_least64_t;
typedef __uint_least8_t uint_least8_t;
typedef __uint_least16_t uint_least16_t;
typedef __uint_least32_t uint_least32_t;
typedef __uint_least64_t uint_least64_t;
typedef signed char int_fast8_t;
typedef long int_fast16_t;
typedef long int_fast32_t;
typedef long int_fast64_t;
typedef unsigned char uint_fast8_t;
typedef unsigned long uint_fast16_t;
typedef unsigned long uint_fast32_t;
typedef unsigned long uint_fast64_t;
typedef long intptr_t;
typedef unsigned long uintptr_t;
typedef __intmax_t intmax_t;
typedef __uintmax_t uintmax_t;
extern uint64_t revng_init_local_sp(uint64_t param_0);
extern uint64_t init_rax(void);
extern uint64_t init_rbx(void);
extern uint64_t init_r12(void);
extern uint64_t init_rbp(void);
extern void indirect_placeholder(void);
extern uint64_t init_r13(void);
extern uint64_t init_r14(void);
extern uint64_t init_r15(void);
extern void indirect_placeholder_4(uint64_t param_0, uint64_t param_1);
uint64_t bb_build_charclass_isra_28(uint64_t rdi, uint64_t r9, uint64_t rcx, uint64_t rdx, uint64_t rsi, uint64_t r8) {
    uint64_t rax_34;
    uint64_t var_0;
    uint64_t var_1;
    uint64_t var_2;
    uint64_t var_3;
    uint64_t var_4;
    uint64_t var_5;
    uint64_t var_6;
    uint64_t var_7;
    uint64_t var_8;
    uint64_t rax_32;
    uint64_t rsi5_2;
    uint64_t *var_23;
    uint64_t local_sp_13;
    uint64_t *var_24;
    uint64_t *_pre_phi;
    uint64_t *var_14;
    uint64_t var_15;
    uint64_t *_pre_pre_phi;
    uint64_t var_12;
    uint64_t var_13;
    uint64_t local_sp_9;
    uint64_t var_28;
    uint64_t var_29;
    uint64_t var_30;
    uint64_t *var_31;
    uint64_t rbx_9;
    uint64_t rax_31;
    uint64_t rax_27;
    uint64_t local_sp_8;
    uint64_t var_33;
    uint64_t var_34;
    uint64_t *var_35;
    uint64_t rbx_8;
    uint64_t local_sp_12;
    uint64_t rax_28;
    uint64_t var_18;
    uint64_t *var_19;
    uint64_t rsi5_1;
    uint64_t rax_29;
    uint64_t rax_30;
    uint64_t var_32;
    uint64_t var_27;
    uint64_t local_sp_10;
    uint64_t r86_0;
    uint64_t *var_16;
    uint64_t var_17;
    uint64_t local_sp_11;
    uint64_t rsi5_0;
    uint64_t rax_33;
    uint64_t var_25;
    uint64_t var_26;
    uint64_t r86_1;
    uint64_t var_20;
    uint64_t var_21;
    uint64_t var_22;
    uint64_t var_9;
    uint64_t *var_10;
    uint64_t var_11;
    var_0 = revng_init_local_sp(0UL);
    var_1 = init_rax();
    var_2 = init_r15();
    var_3 = init_r13();
    var_4 = init_rbx();
    var_5 = init_r12();
    var_6 = init_rbp();
    var_7 = init_r14();
    *(uint64_t *)(var_0 + (-8L)) = var_2;
    *(uint64_t *)(var_0 + (-16L)) = var_7;
    *(uint64_t *)(var_0 + (-24L)) = var_3;
    *(uint64_t *)(var_0 + (-32L)) = var_5;
    *(uint64_t *)(var_0 + (-40L)) = var_6;
    *(uint64_t *)(var_0 + (-48L)) = var_4;
    var_8 = var_0 + (-72L);
    rax_34 = var_1;
    rax_32 = var_1;
    rbx_9 = 0UL;
    rbx_8 = 0UL;
    rax_29 = 12UL;
    local_sp_10 = var_8;
    r86_0 = r8;
    if ((uint64_t)((uint32_t)*(uint64_t *)(var_0 | 8UL) & 4194304U) == 0UL) {
        *(uint64_t *)(var_0 + (-64L)) = r8;
        var_9 = var_0 + (-80L);
        var_10 = (uint64_t *)var_9;
        *var_10 = 4241898UL;
        indirect_placeholder();
        var_11 = *(uint64_t *)var_8;
        rax_32 = 4355356UL;
        local_sp_11 = var_9;
        local_sp_13 = var_9;
        r86_1 = var_11;
        if ((uint64_t)(uint32_t)var_1 == 0UL) {
            var_14 = (uint64_t *)rcx;
            var_15 = *var_14;
            _pre_pre_phi = var_14;
            rsi5_0 = var_15;
            rsi5_2 = var_15;
            if (*(uint64_t *)var_11 == var_15) {
                _pre_phi = _pre_pre_phi;
                rax_33 = *(uint64_t *)rdx;
                local_sp_12 = local_sp_11;
                rsi5_1 = rsi5_0;
            } else {
                var_18 = (rsi5_2 << 1UL) | 1UL;
                var_19 = (uint64_t *)rdx;
                var_20 = *var_19;
                *(uint64_t *)(local_sp_13 + 8UL) = r86_1;
                var_21 = var_18 << 3UL;
                var_22 = local_sp_13 + (-8L);
                *(uint64_t *)var_22 = 4241955UL;
                indirect_placeholder_4(var_20, var_21);
                rax_33 = rax_34;
                local_sp_12 = var_22;
                if (rax_34 == 0UL) {
                    return rax_29;
                }
                var_23 = *(uint64_t **)local_sp_13;
                *var_19 = rax_34;
                *var_23 = var_18;
                var_24 = (uint64_t *)rcx;
                _pre_phi = var_24;
                rsi5_1 = *var_24;
            }
        } else {
            var_12 = var_0 + (-88L);
            *(uint64_t *)var_12 = 4242217UL;
            indirect_placeholder();
            var_13 = *var_10;
            local_sp_10 = var_12;
            r86_0 = var_13;
            var_16 = (uint64_t *)rcx;
            var_17 = *var_16;
            rax_34 = rax_32;
            rsi5_2 = var_17;
            local_sp_13 = local_sp_10;
            _pre_pre_phi = var_16;
            local_sp_11 = local_sp_10;
            rsi5_0 = var_17;
            r86_1 = r86_0;
            if (*(uint64_t *)r86_0 == var_17) {
                _pre_phi = _pre_pre_phi;
                rax_33 = *(uint64_t *)rdx;
                local_sp_12 = local_sp_11;
                rsi5_1 = rsi5_0;
            } else {
                var_18 = (rsi5_2 << 1UL) | 1UL;
                var_19 = (uint64_t *)rdx;
                var_20 = *var_19;
                *(uint64_t *)(local_sp_13 + 8UL) = r86_1;
                var_21 = var_18 << 3UL;
                var_22 = local_sp_13 + (-8L);
                *(uint64_t *)var_22 = 4241955UL;
                indirect_placeholder_4(var_20, var_21);
                rax_33 = rax_34;
                local_sp_12 = var_22;
                if (rax_34 != 0UL) {
                    return rax_29;
                }
                var_23 = *(uint64_t **)local_sp_13;
                *var_19 = rax_34;
                *var_23 = var_18;
                var_24 = (uint64_t *)rcx;
                _pre_phi = var_24;
                rsi5_1 = *var_24;
            }
        }
    } else {
        var_16 = (uint64_t *)rcx;
        var_17 = *var_16;
        rax_34 = rax_32;
        rsi5_2 = var_17;
        local_sp_13 = local_sp_10;
        _pre_pre_phi = var_16;
        local_sp_11 = local_sp_10;
        rsi5_0 = var_17;
        r86_1 = r86_0;
        if (*(uint64_t *)r86_0 == var_17) {
            _pre_phi = _pre_pre_phi;
            rax_33 = *(uint64_t *)rdx;
            local_sp_12 = local_sp_11;
            rsi5_1 = rsi5_0;
        } else {
            var_18 = (rsi5_2 << 1UL) | 1UL;
            var_19 = (uint64_t *)rdx;
            var_20 = *var_19;
            *(uint64_t *)(local_sp_13 + 8UL) = r86_1;
            var_21 = var_18 << 3UL;
            var_22 = local_sp_13 + (-8L);
            *(uint64_t *)var_22 = 4241955UL;
            indirect_placeholder_4(var_20, var_21);
            rax_33 = rax_34;
            local_sp_12 = var_22;
            if (rax_34 == 0UL) {
                return rax_29;
            }
            var_23 = *(uint64_t **)local_sp_13;
            *var_19 = rax_34;
            *var_23 = var_18;
            var_24 = (uint64_t *)rcx;
            _pre_phi = var_24;
            rsi5_1 = *var_24;
        }
    }
    *_pre_phi = (rsi5_1 + 1UL);
    var_25 = (rsi5_1 << 3UL) + rax_33;
    *(uint64_t *)(local_sp_12 + (-8L)) = 4241712UL;
    indirect_placeholder();
    *(uint64_t *)var_25 = rax_33;
    var_26 = local_sp_12 + (-16L);
    *(uint64_t *)var_26 = 4241729UL;
    indirect_placeholder();
    local_sp_9 = var_26;
    rax_31 = rax_33;
    local_sp_8 = var_26;
    rax_29 = 0UL;
    rax_30 = rax_33;
    if ((uint64_t)(uint32_t)rax_33 != 0UL) {
        if (rdi == 0UL) {
            while (1U)
                {
                    var_32 = local_sp_8 + (-8L);
                    *(uint64_t *)var_32 = 4241767UL;
                    indirect_placeholder();
                    rax_28 = rax_30;
                    local_sp_8 = var_32;
                    if ((uint64_t)(uint32_t)rax_30 == 0UL) {
                        var_33 = (uint64_t)((long)rbx_8 >> (long)6UL);
                        var_34 = 1UL << (rbx_8 & 63UL);
                        var_35 = (uint64_t *)((var_33 << 3UL) + rsi);
                        *var_35 = (*var_35 | var_34);
                        rax_28 = var_33;
                    }
                    rax_30 = rax_28;
                    if (rbx_8 == 255UL) {
                        break;
                    }
                    rbx_8 = rbx_8 + 1UL;
                    continue;
                }
        }
        while (1U)
            {
                var_27 = local_sp_9 + (-8L);
                *(uint64_t *)var_27 = 4241831UL;
                indirect_placeholder();
                rax_27 = rax_31;
                local_sp_9 = var_27;
                if ((uint64_t)(uint32_t)rax_31 == 0UL) {
                    var_28 = (uint64_t)*(unsigned char *)(rbx_9 + rdi);
                    var_29 = 1UL << (var_28 & 63UL);
                    var_30 = (var_28 >> 3UL) & 24UL;
                    var_31 = (uint64_t *)(var_30 + rsi);
                    *var_31 = (*var_31 | var_29);
                    rax_27 = var_30;
                }
                rax_31 = rax_27;
                if (rbx_9 == 255UL) {
                    break;
                }
                rbx_9 = rbx_9 + 1UL;
                continue;
            }
    }
    *(uint64_t *)(local_sp_12 + (-24L)) = 4241997UL;
    indirect_placeholder();
    *(uint64_t *)(local_sp_12 + (-32L)) = 4242152UL;
    indirect_placeholder();
    *(uint64_t *)(local_sp_12 + (-40L)) = 4242251UL;
    indirect_placeholder();
    *(uint64_t *)(local_sp_12 + (-48L)) = 4242354UL;
    indirect_placeholder();
    *(uint64_t *)(local_sp_12 + (-56L)) = 4242570UL;
    indirect_placeholder();
    *(uint64_t *)(local_sp_12 + (-64L)) = 4242757UL;
    indirect_placeholder();
    *(uint64_t *)(local_sp_12 + (-72L)) = 4242917UL;
    indirect_placeholder();
    *(uint64_t *)(local_sp_12 + (-80L)) = 4243092UL;
    indirect_placeholder();
    *(uint64_t *)(local_sp_12 + (-88L)) = 4243233UL;
    indirect_placeholder();
    *(uint64_t *)(local_sp_12 + (-96L)) = 4243374UL;
    indirect_placeholder();
    *(uint64_t *)(local_sp_12 + (-104L)) = 4243514UL;
    indirect_placeholder();
    rax_29 = 4UL;
    return rax_29;
}

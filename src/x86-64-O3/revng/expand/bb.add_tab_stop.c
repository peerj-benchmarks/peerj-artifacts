typedef unsigned char __u_char;
typedef unsigned short __u_short;
typedef unsigned int __u_int;
typedef unsigned long __u_long;
typedef signed char __int8_t;
typedef unsigned char __uint8_t;
typedef short __int16_t;
typedef unsigned short __uint16_t;
typedef int __int32_t;
typedef unsigned int __uint32_t;
typedef long __int64_t;
typedef unsigned long __uint64_t;
typedef __int8_t __int_least8_t;
typedef __uint8_t __uint_least8_t;
typedef __int16_t __int_least16_t;
typedef __uint16_t __uint_least16_t;
typedef __int32_t __int_least32_t;
typedef __uint32_t __uint_least32_t;
typedef __int64_t __int_least64_t;
typedef __uint64_t __uint_least64_t;
typedef long __quad_t;
typedef unsigned long __u_quad_t;
typedef long __intmax_t;
typedef unsigned long __uintmax_t;
typedef unsigned long __dev_t;
typedef unsigned int __uid_t;
typedef unsigned int __gid_t;
typedef unsigned long __ino_t;
typedef unsigned long __ino64_t;
typedef unsigned int __mode_t;
typedef unsigned long __nlink_t;
typedef long __off_t;
typedef long __off64_t;
typedef int __pid_t;
typedef struct {
    int __val[2];
} __fsid_t;
typedef long __clock_t;
typedef unsigned long __rlim_t;
typedef unsigned long __rlim64_t;
typedef unsigned int __id_t;
typedef long __time_t;
typedef unsigned int __useconds_t;
typedef long __suseconds_t;
typedef int __daddr_t;
typedef int __key_t;
typedef int __clockid_t;
typedef void *__timer_t;
typedef long __blksize_t;
typedef long __blkcnt_t;
typedef long __blkcnt64_t;
typedef unsigned long __fsblkcnt_t;
typedef unsigned long __fsblkcnt64_t;
typedef unsigned long __fsfilcnt_t;
typedef unsigned long __fsfilcnt64_t;
typedef long __fsword_t;
typedef long __ssize_t;
typedef long __syscall_slong_t;
typedef unsigned long __syscall_ulong_t;
typedef __off64_t __loff_t;
typedef char *__caddr_t;
typedef long __intptr_t;
typedef unsigned int __socklen_t;
typedef int __sig_atomic_t;
typedef __int8_t int8_t;
typedef __int16_t int16_t;
typedef __int32_t int32_t;
typedef __int64_t int64_t;
typedef __uint8_t uint8_t;
typedef __uint16_t uint16_t;
typedef __uint32_t uint32_t;
typedef __uint64_t uint64_t;
typedef __int_least8_t int_least8_t;
typedef __int_least16_t int_least16_t;
typedef __int_least32_t int_least32_t;
typedef __int_least64_t int_least64_t;
typedef __uint_least8_t uint_least8_t;
typedef __uint_least16_t uint_least16_t;
typedef __uint_least32_t uint_least32_t;
typedef __uint_least64_t uint_least64_t;
typedef signed char int_fast8_t;
typedef long int_fast16_t;
typedef long int_fast32_t;
typedef long int_fast64_t;
typedef unsigned char uint_fast8_t;
typedef unsigned long uint_fast16_t;
typedef unsigned long uint_fast32_t;
typedef unsigned long uint_fast64_t;
typedef long intptr_t;
typedef unsigned long uintptr_t;
typedef __intmax_t intmax_t;
typedef __uintmax_t uintmax_t;
typedef _Bool bool;
struct indirect_placeholder_12_ret_type;
struct indirect_placeholder_12_ret_type {
    uint64_t field_0;
    uint64_t field_1;
    uint64_t field_2;
    uint64_t field_3;
};
extern void abort(void);
extern uint64_t revng_init_local_sp(uint64_t param_0);
extern uint64_t init_rbx(void);
extern uint64_t init_rbp(void);
extern uint64_t init_cc_src2(void);
extern uint64_t helper_cc_compute_c_wrapper(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint32_t param_3);
extern void indirect_placeholder_10(uint64_t param_0, uint64_t param_1);
extern struct indirect_placeholder_12_ret_type indirect_placeholder_12(uint64_t param_0, uint64_t param_1);
void bb_add_tab_stop(uint64_t rdi, uint64_t r9, uint64_t r8) {
    uint64_t var_10;
    uint64_t var_0;
    uint64_t var_1;
    uint64_t var_2;
    uint64_t var_3;
    uint64_t var_4;
    bool var_5;
    uint64_t var_6;
    uint64_t rcx_0;
    uint64_t var_7;
    uint64_t var_8;
    uint64_t var_9;
    uint64_t rbp_2;
    uint64_t rbp_1;
    uint64_t rbp_3;
    uint64_t rdi1_4;
    uint64_t rax_0;
    uint64_t rdx_2;
    uint64_t rbp_0;
    uint64_t rdx_0;
    uint64_t var_15;
    uint64_t var_16;
    uint64_t var_11;
    uint64_t rdx_1;
    uint64_t rbp_4;
    uint64_t rsi_0;
    struct indirect_placeholder_12_ret_type var_12;
    uint64_t var_13;
    uint64_t var_14;
    var_0 = revng_init_local_sp(0UL);
    var_1 = init_rbx();
    var_2 = init_rbp();
    var_3 = init_cc_src2();
    *(uint64_t *)(var_0 + (-8L)) = var_2;
    *(uint64_t *)(var_0 + (-16L)) = var_1;
    var_4 = *(uint64_t *)6370608UL;
    var_5 = (var_4 == 0UL);
    var_6 = *(uint64_t *)6370624UL;
    rcx_0 = 0UL;
    rbp_2 = 0UL;
    rbp_1 = 0UL;
    rdi1_4 = var_6;
    rax_0 = var_6;
    rdx_2 = 16UL;
    rdx_0 = var_4;
    rdx_1 = var_4;
    rsi_0 = 128UL;
    if (!var_5) {
        var_10 = rdi - rcx_0;
        rbp_2 = var_10;
        rbp_1 = var_10;
        rdi1_4 = 0UL;
        rbp_0 = var_10;
        rbp_4 = var_10;
        if (var_4 == *(uint64_t *)6370616UL) {
            *(uint64_t *)((rdx_0 << 3UL) + rax_0) = rdi;
            var_15 = *(uint64_t *)6371072UL - rbp_0;
            *(uint64_t *)6370608UL = (rdx_0 + 1UL);
            var_16 = helper_cc_compute_c_wrapper(var_15, rbp_0, var_3, 17U);
            if (var_16 == 0UL) {
                *(uint64_t *)6371072UL = rbp_0;
            }
            return;
        }
        if (var_6 == 0UL) {
            rbp_3 = rbp_1;
            if (var_4 > 768614336404564649UL) {
                *(uint64_t *)(var_0 + (-32L)) = 4203051UL;
                indirect_placeholder_10(r9, r8);
                abort();
            }
            var_11 = ((var_4 >> 1UL) + var_4) + 1UL;
            rdx_1 = var_11;
            rbp_4 = rbp_3;
            rdx_2 = rdx_1;
            rsi_0 = rdx_1 << 3UL;
        } else {
            if (!var_5) {
                rbp_3 = rbp_2;
                if (var_4 > 1152921504606846975UL) {
                    *(uint64_t *)(var_0 + (-32L)) = 4203051UL;
                    indirect_placeholder_10(r9, r8);
                    abort();
                }
                rbp_4 = rbp_3;
                rdx_2 = rdx_1;
                rsi_0 = rdx_1 << 3UL;
            }
        }
        *(uint64_t *)6370616UL = rdx_2;
        *(uint64_t *)(var_0 + (-32L)) = 4203003UL;
        var_12 = indirect_placeholder_12(rdi1_4, rsi_0);
        var_13 = var_12.field_0;
        var_14 = *(uint64_t *)6370608UL;
        *(uint64_t *)6370624UL = var_13;
        rax_0 = var_13;
        rbp_0 = rbp_4;
        rdx_0 = var_14;
        *(uint64_t *)((rdx_0 << 3UL) + rax_0) = rdi;
        var_15 = *(uint64_t *)6371072UL - rbp_0;
        *(uint64_t *)6370608UL = (rdx_0 + 1UL);
        var_16 = helper_cc_compute_c_wrapper(var_15, rbp_0, var_3, 17U);
        if (var_16 != 0UL) {
            *(uint64_t *)6371072UL = rbp_0;
        }
        return;
    }
    var_7 = (var_4 << 3UL) + var_6;
    var_8 = *(uint64_t *)(var_7 + (-8L));
    rcx_0 = var_8;
    if (var_8 > rdi) {
        if (var_4 != *(uint64_t *)6370616UL) {
            var_9 = var_4 + 1UL;
            *(uint64_t *)var_7 = rdi;
            *(uint64_t *)6370608UL = var_9;
            return;
        }
        if (var_6 == 0UL) {
            rbp_3 = rbp_2;
            if (var_4 > 1152921504606846975UL) {
                *(uint64_t *)(var_0 + (-32L)) = 4203051UL;
                indirect_placeholder_10(r9, r8);
                abort();
            }
        }
        rbp_3 = rbp_1;
        if (var_4 > 768614336404564649UL) {
            *(uint64_t *)(var_0 + (-32L)) = 4203051UL;
            indirect_placeholder_10(r9, r8);
            abort();
        }
        var_11 = ((var_4 >> 1UL) + var_4) + 1UL;
        rdx_1 = var_11;
        rbp_4 = rbp_3;
        rdx_2 = rdx_1;
        rsi_0 = rdx_1 << 3UL;
    } else {
        var_10 = rdi - rcx_0;
        rbp_2 = var_10;
        rbp_1 = var_10;
        rdi1_4 = 0UL;
        rbp_0 = var_10;
        rbp_4 = var_10;
        if (var_4 != *(uint64_t *)6370616UL) {
            *(uint64_t *)((rdx_0 << 3UL) + rax_0) = rdi;
            var_15 = *(uint64_t *)6371072UL - rbp_0;
            *(uint64_t *)6370608UL = (rdx_0 + 1UL);
            var_16 = helper_cc_compute_c_wrapper(var_15, rbp_0, var_3, 17U);
            if (var_16 != 0UL) {
                *(uint64_t *)6371072UL = rbp_0;
            }
            return;
        }
        if (var_6 == 0UL) {
            if (!var_5) {
                rbp_3 = rbp_2;
                if (var_4 <= 1152921504606846975UL) {
                    *(uint64_t *)(var_0 + (-32L)) = 4203051UL;
                    indirect_placeholder_10(r9, r8);
                    abort();
                }
                rbp_4 = rbp_3;
                rdx_2 = rdx_1;
                rsi_0 = rdx_1 << 3UL;
            }
        } else {
            rbp_3 = rbp_1;
            if (var_4 > 768614336404564649UL) {
                *(uint64_t *)(var_0 + (-32L)) = 4203051UL;
                indirect_placeholder_10(r9, r8);
                abort();
            }
            var_11 = ((var_4 >> 1UL) + var_4) + 1UL;
            rdx_1 = var_11;
            rbp_4 = rbp_3;
            rdx_2 = rdx_1;
            rsi_0 = rdx_1 << 3UL;
        }
    }
}

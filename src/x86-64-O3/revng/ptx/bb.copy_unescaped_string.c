typedef unsigned char __u_char;
typedef unsigned short __u_short;
typedef unsigned int __u_int;
typedef unsigned long __u_long;
typedef signed char __int8_t;
typedef unsigned char __uint8_t;
typedef short __int16_t;
typedef unsigned short __uint16_t;
typedef int __int32_t;
typedef unsigned int __uint32_t;
typedef long __int64_t;
typedef unsigned long __uint64_t;
typedef __int8_t __int_least8_t;
typedef __uint8_t __uint_least8_t;
typedef __int16_t __int_least16_t;
typedef __uint16_t __uint_least16_t;
typedef __int32_t __int_least32_t;
typedef __uint32_t __uint_least32_t;
typedef __int64_t __int_least64_t;
typedef __uint64_t __uint_least64_t;
typedef long __quad_t;
typedef unsigned long __u_quad_t;
typedef long __intmax_t;
typedef unsigned long __uintmax_t;
typedef unsigned long __dev_t;
typedef unsigned int __uid_t;
typedef unsigned int __gid_t;
typedef unsigned long __ino_t;
typedef unsigned long __ino64_t;
typedef unsigned int __mode_t;
typedef unsigned long __nlink_t;
typedef long __off_t;
typedef long __off64_t;
typedef int __pid_t;
typedef struct {
    int __val[2];
} __fsid_t;
typedef long __clock_t;
typedef unsigned long __rlim_t;
typedef unsigned long __rlim64_t;
typedef unsigned int __id_t;
typedef long __time_t;
typedef unsigned int __useconds_t;
typedef long __suseconds_t;
typedef int __daddr_t;
typedef int __key_t;
typedef int __clockid_t;
typedef void *__timer_t;
typedef long __blksize_t;
typedef long __blkcnt_t;
typedef long __blkcnt64_t;
typedef unsigned long __fsblkcnt_t;
typedef unsigned long __fsblkcnt64_t;
typedef unsigned long __fsfilcnt_t;
typedef unsigned long __fsfilcnt64_t;
typedef long __fsword_t;
typedef long __ssize_t;
typedef long __syscall_slong_t;
typedef unsigned long __syscall_ulong_t;
typedef __off64_t __loff_t;
typedef char *__caddr_t;
typedef long __intptr_t;
typedef unsigned int __socklen_t;
typedef int __sig_atomic_t;
typedef __int8_t int8_t;
typedef __int16_t int16_t;
typedef __int32_t int32_t;
typedef __int64_t int64_t;
typedef __uint8_t uint8_t;
typedef __uint16_t uint16_t;
typedef __uint32_t uint32_t;
typedef __uint64_t uint64_t;
typedef __int_least8_t int_least8_t;
typedef __int_least16_t int_least16_t;
typedef __int_least32_t int_least32_t;
typedef __int_least64_t int_least64_t;
typedef __uint_least8_t uint_least8_t;
typedef __uint_least16_t uint_least16_t;
typedef __uint_least32_t uint_least32_t;
typedef __uint_least64_t uint_least64_t;
typedef signed char int_fast8_t;
typedef long int_fast16_t;
typedef long int_fast32_t;
typedef long int_fast64_t;
typedef unsigned char uint_fast8_t;
typedef unsigned long uint_fast16_t;
typedef unsigned long uint_fast32_t;
typedef unsigned long uint_fast64_t;
typedef long intptr_t;
typedef unsigned long uintptr_t;
typedef __intmax_t intmax_t;
typedef __uintmax_t uintmax_t;
typedef _Bool bool;
struct indirect_placeholder_51_ret_type;
struct indirect_placeholder_51_ret_type {
    uint64_t field_0;
    uint64_t field_1;
    uint64_t field_2;
    uint64_t field_3;
    uint64_t field_4;
};
extern uint64_t revng_init_local_sp(uint64_t param_0);
extern uint64_t init_rbx(void);
extern uint64_t init_rbp(void);
extern void indirect_placeholder(void);
extern uint64_t init_cc_src2(void);
extern uint64_t init_r13(void);
extern uint64_t init_r12(void);
extern uint64_t init_r14(void);
extern uint64_t init_rax(void);
extern uint64_t helper_cc_compute_all_wrapper(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint32_t param_3);
extern uint64_t init_r15(void);
extern struct indirect_placeholder_51_ret_type indirect_placeholder_51(uint64_t param_0);
uint64_t bb_copy_unescaped_string(uint64_t rdi) {
    uint64_t r14_1;
    uint64_t var_0;
    uint64_t var_1;
    uint64_t var_2;
    uint64_t var_3;
    uint64_t var_4;
    uint64_t var_5;
    uint64_t var_6;
    uint64_t var_7;
    uint64_t var_8;
    uint64_t var_9;
    uint64_t var_10;
    struct indirect_placeholder_51_ret_type var_11;
    uint64_t var_12;
    uint64_t rbx_1;
    uint64_t local_sp_1;
    unsigned char var_61;
    uint64_t var_62;
    uint64_t rdx_0_in;
    bool var_63;
    uint64_t var_64;
    uint64_t rbx_0;
    uint64_t var_65;
    uint64_t var_66;
    uint64_t r15_0;
    uint64_t rbp_0_be;
    uint64_t rdx_2;
    uint64_t var_69;
    uint64_t var_70;
    uint64_t r14_0;
    uint64_t rdx_3;
    uint64_t rax_0;
    uint64_t rbx_1_be;
    uint64_t rdx_1;
    uint64_t local_sp_0_be;
    uint64_t rdx_1_be;
    uint64_t var_67;
    uint64_t var_68;
    uint64_t rbp_0;
    uint64_t local_sp_0;
    uint64_t rbp_2;
    uint64_t rbx_2;
    uint64_t rbp_1;
    unsigned char var_13;
    uint64_t var_14;
    unsigned char var_15;
    uint64_t var_16;
    uint64_t var_17;
    unsigned char var_18;
    uint64_t var_19;
    uint64_t var_20;
    uint64_t var_21;
    uint64_t var_22;
    uint64_t var_23;
    uint64_t var_24;
    unsigned char var_25;
    uint64_t var_26;
    uint64_t rbx_3;
    uint64_t var_27;
    uint64_t var_28;
    uint64_t var_79;
    unsigned char var_29;
    uint64_t var_30;
    uint64_t rbx_4;
    uint64_t var_31;
    uint64_t var_32;
    unsigned char var_33;
    uint64_t var_34;
    uint64_t var_35;
    uint64_t var_36;
    uint64_t var_37;
    uint64_t var_38;
    uint64_t var_39;
    uint64_t var_40;
    uint64_t var_41;
    uint64_t var_42;
    uint64_t var_43;
    uint64_t var_44;
    uint64_t var_45;
    uint64_t var_46;
    uint64_t var_47;
    uint64_t var_48;
    uint64_t var_49;
    uint64_t var_50;
    uint64_t var_51;
    uint64_t var_52;
    uint64_t var_56;
    uint64_t var_57;
    uint32_t var_58;
    uint64_t var_59;
    uint64_t var_60;
    uint64_t var_53;
    uint64_t var_54;
    uint64_t var_55;
    uint64_t var_71;
    uint64_t var_72;
    uint64_t var_73;
    uint64_t var_74;
    uint64_t var_75;
    uint64_t var_76;
    uint64_t var_77;
    uint64_t var_78;
    unsigned int loop_state_var;
    bool switch_state_var;
    var_0 = revng_init_local_sp(0UL);
    var_1 = init_rax();
    var_2 = init_r15();
    var_3 = init_r13();
    var_4 = init_rbx();
    var_5 = init_r12();
    var_6 = init_rbp();
    var_7 = init_cc_src2();
    var_8 = init_r14();
    *(uint64_t *)(var_0 + (-8L)) = var_2;
    *(uint64_t *)(var_0 + (-16L)) = var_8;
    *(uint64_t *)(var_0 + (-24L)) = var_3;
    *(uint64_t *)(var_0 + (-32L)) = var_5;
    *(uint64_t *)(var_0 + (-40L)) = var_6;
    *(uint64_t *)(var_0 + (-48L)) = var_4;
    *(uint64_t *)(var_0 + (-80L)) = 4213686UL;
    indirect_placeholder();
    var_9 = var_1 + 1UL;
    var_10 = var_0 + (-88L);
    *(uint64_t *)var_10 = 4213695UL;
    var_11 = indirect_placeholder_51(var_9);
    var_12 = var_11.field_0;
    r14_1 = 0UL;
    rbx_1 = rdi;
    rax_0 = 0UL;
    rdx_1 = (uint64_t)*(unsigned char *)rdi;
    rbp_0 = var_12;
    local_sp_0 = var_10;
    while (1U)
        {
            local_sp_1 = local_sp_0;
            rdx_2 = rdx_1;
            local_sp_0_be = local_sp_0;
            rbp_2 = rbp_0;
            rbx_2 = rbx_1;
            rbp_1 = rbp_0;
            if ((uint64_t)(unsigned char)rdx_1 != 0UL) {
                loop_state_var = 0U;
                break;
            }
            while (1U)
                {
                    var_13 = (unsigned char)rdx_2;
                    rbp_2 = rbp_1;
                    if ((uint64_t)(var_13 + '\xa4') != 0UL) {
                        loop_state_var = 1U;
                        break;
                    }
                    *(unsigned char *)rbp_1 = var_13;
                    var_14 = rbx_2 + 1UL;
                    var_15 = *(unsigned char *)var_14;
                    var_16 = rbp_1 + 1UL;
                    rbx_2 = var_14;
                    rbp_1 = var_16;
                    rbp_2 = var_16;
                    if (var_15 != '\x00') {
                        loop_state_var = 0U;
                        break;
                    }
                    rdx_2 = (uint64_t)var_15;
                    continue;
                }
            switch_state_var = 0;
            switch (loop_state_var) {
              case 1U:
                {
                    var_17 = rbx_2 + 1UL;
                    var_18 = *(unsigned char *)var_17;
                    var_19 = (uint64_t)var_18;
                    var_20 = var_19 + (-102L);
                    rbx_4 = var_17;
                    if ((uint64_t)(unsigned char)var_20 == 0UL) {
                        *(unsigned char *)rbp_1 = (unsigned char)'\f';
                        var_77 = rbx_2 + 2UL;
                        var_78 = (uint64_t)*(unsigned char *)var_77;
                        var_79 = rbp_1 + 1UL;
                        rbx_1_be = var_77;
                        rbp_0_be = var_79;
                        rdx_1_be = var_78;
                    } else {
                        var_21 = helper_cc_compute_all_wrapper(var_20, 102UL, var_7, 14U);
                        if ((uint64_t)(((unsigned char)(var_21 >> 4UL) ^ (unsigned char)var_21) & '\xc0') == 0UL) {
                            var_45 = var_19 + (-116L);
                            if ((uint64_t)(unsigned char)var_45 != 0UL) {
                                *(unsigned char *)rbp_1 = (unsigned char)'\t';
                                var_74 = rbx_2 + 2UL;
                                var_75 = (uint64_t)*(unsigned char *)var_74;
                                var_76 = rbp_1 + 1UL;
                                rbx_1_be = var_74;
                                rbp_0_be = var_76;
                                rdx_1_be = var_75;
                                rbx_1 = rbx_1_be;
                                rbp_0 = rbp_0_be;
                                local_sp_0 = local_sp_0_be;
                                rdx_1 = rdx_1_be;
                                continue;
                            }
                            var_46 = helper_cc_compute_all_wrapper(var_45, 116UL, var_7, 14U);
                            if ((uint64_t)(((unsigned char)(var_46 >> 4UL) ^ (unsigned char)var_46) & '\xc0') != 0UL) {
                                if ((uint64_t)(var_18 + '\x92') != 0UL) {
                                    *(unsigned char *)rbp_1 = (unsigned char)'\n';
                                    var_50 = rbx_2 + 2UL;
                                    var_51 = (uint64_t)*(unsigned char *)var_50;
                                    var_52 = rbp_1 + 1UL;
                                    rbx_1_be = var_50;
                                    rbp_0_be = var_52;
                                    rdx_1_be = var_51;
                                    rbx_1 = rbx_1_be;
                                    rbp_0 = rbp_0_be;
                                    local_sp_0 = local_sp_0_be;
                                    rdx_1 = rdx_1_be;
                                    continue;
                                }
                                if ((uint64_t)(var_18 + '\x8e') != 0UL) {
                                    *(unsigned char *)rbp_1 = (unsigned char)'\r';
                                    var_47 = rbx_2 + 2UL;
                                    var_48 = (uint64_t)*(unsigned char *)var_47;
                                    var_49 = rbp_1 + 1UL;
                                    rbx_1_be = var_47;
                                    rbp_0_be = var_49;
                                    rdx_1_be = var_48;
                                    rbx_1 = rbx_1_be;
                                    rbp_0 = rbp_0_be;
                                    local_sp_0 = local_sp_0_be;
                                    rdx_1 = rdx_1_be;
                                    continue;
                                }
                            }
                            if ((uint64_t)(var_18 + '\x8a') != 0UL) {
                                *(unsigned char *)rbp_1 = (unsigned char)'\v';
                                var_71 = rbx_2 + 2UL;
                                var_72 = (uint64_t)*(unsigned char *)var_71;
                                var_73 = rbp_1 + 1UL;
                                rbx_1_be = var_71;
                                rbp_0_be = var_73;
                                rdx_1_be = var_72;
                                rbx_1 = rbx_1_be;
                                rbp_0 = rbp_0_be;
                                local_sp_0 = local_sp_0_be;
                                rdx_1 = rdx_1_be;
                                continue;
                            }
                            if ((uint64_t)(var_18 + '\x88') != 0UL) {
                                var_56 = rbx_2 + 2UL;
                                var_57 = rbx_2 + 5UL;
                                r15_0 = var_56;
                                while (1U)
                                    {
                                        var_58 = (uint32_t)r15_0 - (uint32_t)var_56;
                                        var_59 = (uint64_t)var_58;
                                        *(uint32_t *)(local_sp_1 + 12UL) = var_58;
                                        var_60 = local_sp_1 + (-8L);
                                        *(uint64_t *)var_60 = 4214031UL;
                                        indirect_placeholder();
                                        local_sp_1 = var_60;
                                        rbx_0 = r15_0;
                                        r14_0 = r14_1;
                                        rbx_1_be = r15_0;
                                        local_sp_0_be = var_60;
                                        if (var_59 != 0UL) {
                                            loop_state_var = 1U;
                                            break;
                                        }
                                        var_61 = *(unsigned char *)r15_0;
                                        var_62 = r14_1 << 4UL;
                                        rbx_0 = var_57;
                                        if ((uint64_t)((var_61 + '\x9f') & '\xfe') > 5UL) {
                                            rdx_0_in = (uint64_t)var_61 + 4294967209UL;
                                        } else {
                                            var_63 = ((uint64_t)((var_61 + '\xbf') & '\xfe') > 5UL);
                                            var_64 = (uint64_t)var_61;
                                            if (var_63) {
                                                rdx_0_in = var_64 + 4294967248UL;
                                            } else {
                                                rdx_0_in = var_64 + 4294967241UL;
                                            }
                                        }
                                        var_65 = r15_0 + 1UL;
                                        var_66 = (uint64_t)((uint32_t)var_62 + (uint32_t)rdx_0_in);
                                        r14_0 = var_66;
                                        r15_0 = var_65;
                                        r14_1 = var_66;
                                        if (var_65 == var_57) {
                                            continue;
                                        }
                                        loop_state_var = 0U;
                                        break;
                                    }
                                switch (loop_state_var) {
                                  case 0U:
                                    {
                                        *(unsigned char *)rbp_1 = (unsigned char)r14_0;
                                        var_67 = (uint64_t)*(unsigned char *)rbx_0;
                                        var_68 = rbp_1 + 1UL;
                                        rbx_1_be = rbx_0;
                                        rbp_0_be = var_68;
                                        rdx_1_be = var_67;
                                        rbx_1 = rbx_1_be;
                                        rbp_0 = rbp_0_be;
                                        local_sp_0 = local_sp_0_be;
                                        rdx_1 = rdx_1_be;
                                        continue;
                                    }
                                    break;
                                  case 1U:
                                    {
                                        if (*(uint32_t *)(local_sp_1 + 4UL) != 0U) {
                                            *(unsigned char *)rbp_1 = (unsigned char)'\\';
                                            *(unsigned char *)(rbp_1 + 1UL) = (unsigned char)'x';
                                            var_69 = rbp_1 + 2UL;
                                            var_70 = (uint64_t)*(unsigned char *)r15_0;
                                            rbp_0_be = var_69;
                                            rdx_1_be = var_70;
                                            rbx_1 = rbx_1_be;
                                            rbp_0 = rbp_0_be;
                                            local_sp_0 = local_sp_0_be;
                                            rdx_1 = rdx_1_be;
                                            continue;
                                        }
                                    }
                                    break;
                                }
                            }
                        }
                        var_22 = var_19 + (-97L);
                        if ((uint64_t)(unsigned char)var_22 != 0UL) {
                            *(unsigned char *)rbp_1 = (unsigned char)'\a';
                            var_42 = rbx_2 + 2UL;
                            var_43 = (uint64_t)*(unsigned char *)var_42;
                            var_44 = rbp_1 + 1UL;
                            rbx_1_be = var_42;
                            rbp_0_be = var_44;
                            rdx_1_be = var_43;
                            rbx_1 = rbx_1_be;
                            rbp_0 = rbp_0_be;
                            local_sp_0 = local_sp_0_be;
                            rdx_1 = rdx_1_be;
                            continue;
                        }
                        var_23 = helper_cc_compute_all_wrapper(var_22, 97UL, var_7, 14U);
                        if ((uint64_t)(((unsigned char)(var_23 >> 4UL) ^ (unsigned char)var_23) & '\xc0') == 0UL) {
                            if ((uint64_t)(var_18 + '\x9e') != 0UL) {
                                *(unsigned char *)rbp_1 = (unsigned char)'\b';
                                var_39 = rbx_2 + 2UL;
                                var_40 = (uint64_t)*(unsigned char *)var_39;
                                var_41 = rbp_1 + 1UL;
                                rbx_1_be = var_39;
                                rbp_0_be = var_41;
                                rdx_1_be = var_40;
                                rbx_1 = rbx_1_be;
                                rbp_0 = rbp_0_be;
                                local_sp_0 = local_sp_0_be;
                                rdx_1 = rdx_1_be;
                                continue;
                            }
                            if ((uint64_t)(var_18 + '\x9d') == 0UL) {
                                loop_state_var = 1U;
                                switch_state_var = 1;
                                break;
                            }
                        }
                        if (var_18 != '\x00') {
                            loop_state_var = 0U;
                            switch_state_var = 1;
                            break;
                        }
                        if ((uint64_t)(var_18 + '\xd0') != 0UL) {
                            var_24 = rbx_2 + 2UL;
                            var_25 = *(unsigned char *)var_24;
                            var_26 = (uint64_t)var_25;
                            rbx_3 = var_24;
                            if ((uint64_t)((var_25 + '\xd0') & '\xf8') == 0UL) {
                                rdx_3 = (uint64_t)(uint32_t)var_26;
                            } else {
                                var_27 = (uint64_t)((uint32_t)var_26 + (-48));
                                var_28 = rbx_2 + 3UL;
                                var_29 = *(unsigned char *)var_28;
                                var_30 = (uint64_t)var_29;
                                rax_0 = var_27;
                                rbx_3 = var_28;
                                if ((uint64_t)((var_29 + '\xd0') & '\xf8') == 0UL) {
                                    rdx_3 = (uint64_t)(uint32_t)var_30;
                                } else {
                                    var_31 = (uint64_t)(((uint32_t)(var_27 << 3UL) + (uint32_t)var_30) + (-48));
                                    var_32 = rbx_2 + 4UL;
                                    var_33 = *(unsigned char *)var_32;
                                    var_34 = (uint64_t)var_33;
                                    rax_0 = var_31;
                                    rbx_3 = var_32;
                                    rdx_3 = var_34;
                                    if ((uint64_t)((var_33 + '\xd0') & '\xf8') == 0UL) {
                                        var_35 = (uint64_t)(((uint32_t)(var_31 << 3UL) + (uint32_t)var_34) + (-48));
                                        var_36 = rbx_2 + 5UL;
                                        rax_0 = var_35;
                                        rbx_3 = var_36;
                                        rdx_3 = (uint64_t)*(unsigned char *)var_36;
                                    }
                                }
                            }
                            *(unsigned char *)rbp_1 = (unsigned char)rax_0;
                            var_37 = rbp_1 + 1UL;
                            rbx_1_be = rbx_3;
                            rbp_0_be = var_37;
                            rdx_1_be = rdx_3;
                            rbx_1 = rbx_1_be;
                            rbp_0 = rbp_0_be;
                            local_sp_0 = local_sp_0_be;
                            rdx_1 = rdx_1_be;
                            continue;
                        }
                        *(unsigned char *)(rbp_1 + 1UL) = var_18;
                        *(unsigned char *)rbp_1 = (unsigned char)'\\';
                        var_53 = rbx_2 + 2UL;
                        var_54 = (uint64_t)*(unsigned char *)var_53;
                        var_55 = rbp_1 + 2UL;
                        rbx_1_be = var_53;
                        rbp_0_be = var_55;
                        rdx_1_be = var_54;
                    }
                }
                break;
              case 0U:
                {
                    loop_state_var = 0U;
                    switch_state_var = 1;
                    break;
                }
                break;
            }
            if (switch_state_var)
                break;
        }
    switch (loop_state_var) {
      case 0U:
        {
            *(unsigned char *)rbp_2 = (unsigned char)'\x00';
            return var_12;
        }
        break;
      case 1U:
        {
            var_38 = rbx_4 + 1UL;
            rbx_4 = var_38;
            do {
                var_38 = rbx_4 + 1UL;
                rbx_4 = var_38;
            } while (*(unsigned char *)var_38 != '\x00');
        }
        break;
    }
}

typedef unsigned char __u_char;
typedef unsigned short __u_short;
typedef unsigned int __u_int;
typedef unsigned long __u_long;
typedef signed char __int8_t;
typedef unsigned char __uint8_t;
typedef short __int16_t;
typedef unsigned short __uint16_t;
typedef int __int32_t;
typedef unsigned int __uint32_t;
typedef long __int64_t;
typedef unsigned long __uint64_t;
typedef __int8_t __int_least8_t;
typedef __uint8_t __uint_least8_t;
typedef __int16_t __int_least16_t;
typedef __uint16_t __uint_least16_t;
typedef __int32_t __int_least32_t;
typedef __uint32_t __uint_least32_t;
typedef __int64_t __int_least64_t;
typedef __uint64_t __uint_least64_t;
typedef long __quad_t;
typedef unsigned long __u_quad_t;
typedef long __intmax_t;
typedef unsigned long __uintmax_t;
typedef unsigned long __dev_t;
typedef unsigned int __uid_t;
typedef unsigned int __gid_t;
typedef unsigned long __ino_t;
typedef unsigned long __ino64_t;
typedef unsigned int __mode_t;
typedef unsigned long __nlink_t;
typedef long __off_t;
typedef long __off64_t;
typedef int __pid_t;
typedef struct {
    int __val[2];
} __fsid_t;
typedef long __clock_t;
typedef unsigned long __rlim_t;
typedef unsigned long __rlim64_t;
typedef unsigned int __id_t;
typedef long __time_t;
typedef unsigned int __useconds_t;
typedef long __suseconds_t;
typedef int __daddr_t;
typedef int __key_t;
typedef int __clockid_t;
typedef void *__timer_t;
typedef long __blksize_t;
typedef long __blkcnt_t;
typedef long __blkcnt64_t;
typedef unsigned long __fsblkcnt_t;
typedef unsigned long __fsblkcnt64_t;
typedef unsigned long __fsfilcnt_t;
typedef unsigned long __fsfilcnt64_t;
typedef long __fsword_t;
typedef long __ssize_t;
typedef long __syscall_slong_t;
typedef unsigned long __syscall_ulong_t;
typedef __off64_t __loff_t;
typedef char *__caddr_t;
typedef long __intptr_t;
typedef unsigned int __socklen_t;
typedef int __sig_atomic_t;
typedef __int8_t int8_t;
typedef __int16_t int16_t;
typedef __int32_t int32_t;
typedef __int64_t int64_t;
typedef __uint8_t uint8_t;
typedef __uint16_t uint16_t;
typedef __uint32_t uint32_t;
typedef __uint64_t uint64_t;
typedef __int_least8_t int_least8_t;
typedef __int_least16_t int_least16_t;
typedef __int_least32_t int_least32_t;
typedef __int_least64_t int_least64_t;
typedef __uint_least8_t uint_least8_t;
typedef __uint_least16_t uint_least16_t;
typedef __uint_least32_t uint_least32_t;
typedef __uint_least64_t uint_least64_t;
typedef signed char int_fast8_t;
typedef long int_fast16_t;
typedef long int_fast32_t;
typedef long int_fast64_t;
typedef unsigned char uint_fast8_t;
typedef unsigned long uint_fast16_t;
typedef unsigned long uint_fast32_t;
typedef unsigned long uint_fast64_t;
typedef long intptr_t;
typedef unsigned long uintptr_t;
typedef __intmax_t intmax_t;
typedef __uintmax_t uintmax_t;
extern uint64_t revng_init_local_sp(uint64_t param_0);
extern uint64_t init_rbx(void);
extern uint64_t init_rbp(void);
extern void indirect_placeholder(void);
extern uint64_t init_cc_src2(void);
extern uint64_t helper_cc_compute_c_wrapper(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint32_t param_3);
extern uint64_t init_r13(void);
extern uint64_t init_r12(void);
extern uint64_t init_r14(void);
extern uint64_t indirect_placeholder_1(uint64_t param_0);
extern uint64_t init_rax(void);
extern uint64_t init_r15(void);
extern void indirect_placeholder_3(uint64_t param_0, uint64_t param_1);
uint64_t bb_fread_file(uint64_t rdi, uint64_t rsi) {
    uint64_t local_sp_0;
    uint64_t var_0;
    uint64_t var_1;
    uint64_t var_2;
    uint64_t var_3;
    uint64_t var_4;
    uint64_t var_5;
    uint64_t var_6;
    uint64_t var_7;
    uint64_t var_8;
    uint64_t var_9;
    uint64_t rbx_1;
    uint64_t var_12;
    uint64_t var_13;
    uint64_t var_14;
    uint64_t rax_2;
    uint64_t rbp_0;
    uint64_t var_23;
    uint64_t var_24;
    uint64_t var_25;
    uint64_t local_sp_4;
    uint64_t rdx_0;
    uint64_t *var_28;
    uint64_t var_26;
    uint64_t var_27;
    uint64_t *var_20;
    uint32_t var_21;
    uint64_t var_22;
    uint64_t rax_0;
    uint64_t var_35;
    uint64_t var_29;
    uint64_t var_30;
    uint64_t var_31;
    uint64_t var_32;
    uint64_t var_33;
    uint64_t var_34;
    uint64_t rax_1;
    uint32_t r15_0_shrunk;
    uint64_t local_sp_1;
    uint64_t rbx_0;
    uint64_t local_sp_2;
    uint64_t var_17;
    uint64_t var_18;
    uint64_t var_19;
    uint32_t var_10;
    uint64_t var_11;
    uint64_t var_15;
    uint64_t var_16;
    unsigned int loop_state_var;
    var_0 = revng_init_local_sp(0UL);
    var_1 = init_rax();
    var_2 = init_r15();
    var_3 = init_r13();
    var_4 = init_rbx();
    var_5 = init_r12();
    var_6 = init_rbp();
    var_7 = init_cc_src2();
    var_8 = init_r14();
    *(uint64_t *)(var_0 + (-8L)) = var_2;
    *(uint64_t *)(var_0 + (-16L)) = var_8;
    *(uint64_t *)(var_0 + (-24L)) = var_3;
    *(uint64_t *)(var_0 + (-32L)) = var_5;
    *(uint64_t *)(var_0 + (-40L)) = var_6;
    *(uint64_t *)(var_0 + (-48L)) = var_4;
    *(uint64_t *)(var_0 + (-216L)) = rsi;
    *(uint64_t *)(var_0 + (-224L)) = 4227437UL;
    indirect_placeholder();
    var_9 = var_0 + (-232L);
    *(uint64_t *)var_9 = 4227449UL;
    indirect_placeholder();
    rbx_1 = 1024UL;
    rax_2 = 0UL;
    rbp_0 = 0UL;
    local_sp_4 = var_9;
    r15_0_shrunk = 12U;
    var_10 = (uint32_t)((uint16_t)*(uint32_t *)(var_0 + (-192L)) & (unsigned short)61440U);
    var_11 = (uint64_t)var_10;
    var_12 = var_0 + (-240L);
    *(uint64_t *)var_12 = 4227768UL;
    indirect_placeholder();
    var_13 = *(uint64_t *)(var_0 + (-176L));
    local_sp_4 = var_12;
    if ((int)(uint32_t)var_1 >= (int)0U & (uint64_t)((var_10 + (-32768)) & (-4096)) != 0UL & (long)var_13 <= (long)var_11) {
        var_14 = var_13 - var_11;
        if (var_14 != 18446744073709551615UL) {
            *(uint64_t *)(var_0 + (-248L)) = 4227821UL;
            indirect_placeholder();
            *(uint32_t *)var_11 = 12U;
            return rax_2;
        }
        rbx_1 = var_14 + 1UL;
    }
    var_15 = local_sp_4 + (-8L);
    *(uint64_t *)var_15 = 4227486UL;
    var_16 = indirect_placeholder_1(rbx_1);
    rax_1 = var_16;
    rbx_0 = rbx_1;
    local_sp_2 = var_15;
    if (var_16 == 0UL) {
        return rax_2;
    }
    while (1U)
        {
            var_17 = rbx_0 - rbp_0;
            var_18 = local_sp_2 + (-8L);
            *(uint64_t *)var_18 = 4227596UL;
            indirect_placeholder();
            var_19 = rbp_0 + rax_1;
            rax_0 = rax_1;
            local_sp_1 = var_18;
            rbp_0 = var_19;
            if (rax_1 == var_17) {
                if (rbx_0 != 18446744073709551615UL) {
                    loop_state_var = 1U;
                    break;
                }
                var_29 = rbx_0 >> 1UL;
                var_30 = var_29 + rbx_0;
                var_31 = var_29 ^ (-1L);
                var_32 = helper_cc_compute_c_wrapper(rbx_0 - var_31, var_31, var_7, 17U);
                var_33 = (var_32 == 0UL) ? 18446744073709551615UL : var_30;
                var_34 = local_sp_2 + (-16L);
                *(uint64_t *)var_34 = 4227558UL;
                indirect_placeholder_3(rax_1, var_33);
                rax_0 = 0UL;
                rax_1 = var_33;
                rbx_0 = var_33;
                local_sp_2 = var_34;
                if (var_33 == 0UL) {
                    continue;
                }
                var_35 = local_sp_2 + (-24L);
                *(uint64_t *)var_35 = 4227717UL;
                indirect_placeholder();
                r15_0_shrunk = *(volatile uint32_t *)(uint32_t *)0UL;
                local_sp_1 = var_35;
                loop_state_var = 1U;
                break;
            }
            *(uint64_t *)local_sp_2 = rax_1;
            var_20 = (uint64_t *)(local_sp_2 + (-16L));
            *var_20 = 4227614UL;
            indirect_placeholder();
            var_21 = *(uint32_t *)rax_1;
            var_22 = local_sp_2 + (-24L);
            *(uint64_t *)var_22 = 4227625UL;
            indirect_placeholder();
            local_sp_0 = var_22;
            r15_0_shrunk = var_21;
            local_sp_1 = var_22;
            if ((uint64_t)(uint32_t)rax_1 != 0UL) {
                loop_state_var = 1U;
                break;
            }
            var_23 = rbx_0 + (-1L);
            var_24 = *var_20;
            var_25 = helper_cc_compute_c_wrapper(var_19 - var_23, var_23, var_7, 17U);
            rdx_0 = var_24;
            if (var_25 != 0UL) {
                loop_state_var = 0U;
                break;
            }
            var_26 = var_19 + 1UL;
            var_27 = local_sp_2 + (-32L);
            *(uint64_t *)var_27 = 4227740UL;
            indirect_placeholder_3(var_24, var_26);
            local_sp_0 = var_27;
            rdx_0 = rax_1;
            loop_state_var = 0U;
            break;
        }
    switch (loop_state_var) {
      case 0U:
        {
            var_28 = *(uint64_t **)local_sp_0;
            *(unsigned char *)(var_19 + rdx_0) = (unsigned char)'\x00';
            *var_28 = var_19;
            rax_2 = rdx_0;
        }
        break;
      case 1U:
        {
            *(uint64_t *)(local_sp_1 + (-8L)) = 4227694UL;
            indirect_placeholder();
            *(uint64_t *)(local_sp_1 + (-16L)) = 4227699UL;
            indirect_placeholder();
            *(uint32_t *)rax_0 = r15_0_shrunk;
        }
        break;
    }
    return rax_2;
}

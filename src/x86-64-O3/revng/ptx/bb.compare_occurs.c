typedef unsigned char __u_char;
typedef unsigned short __u_short;
typedef unsigned int __u_int;
typedef unsigned long __u_long;
typedef signed char __int8_t;
typedef unsigned char __uint8_t;
typedef short __int16_t;
typedef unsigned short __uint16_t;
typedef int __int32_t;
typedef unsigned int __uint32_t;
typedef long __int64_t;
typedef unsigned long __uint64_t;
typedef __int8_t __int_least8_t;
typedef __uint8_t __uint_least8_t;
typedef __int16_t __int_least16_t;
typedef __uint16_t __uint_least16_t;
typedef __int32_t __int_least32_t;
typedef __uint32_t __uint_least32_t;
typedef __int64_t __int_least64_t;
typedef __uint64_t __uint_least64_t;
typedef long __quad_t;
typedef unsigned long __u_quad_t;
typedef long __intmax_t;
typedef unsigned long __uintmax_t;
typedef unsigned long __dev_t;
typedef unsigned int __uid_t;
typedef unsigned int __gid_t;
typedef unsigned long __ino_t;
typedef unsigned long __ino64_t;
typedef unsigned int __mode_t;
typedef unsigned long __nlink_t;
typedef long __off_t;
typedef long __off64_t;
typedef int __pid_t;
typedef struct {
    int __val[2];
} __fsid_t;
typedef long __clock_t;
typedef unsigned long __rlim_t;
typedef unsigned long __rlim64_t;
typedef unsigned int __id_t;
typedef long __time_t;
typedef unsigned int __useconds_t;
typedef long __suseconds_t;
typedef int __daddr_t;
typedef int __key_t;
typedef int __clockid_t;
typedef void *__timer_t;
typedef long __blksize_t;
typedef long __blkcnt_t;
typedef long __blkcnt64_t;
typedef unsigned long __fsblkcnt_t;
typedef unsigned long __fsblkcnt64_t;
typedef unsigned long __fsfilcnt_t;
typedef unsigned long __fsfilcnt64_t;
typedef long __fsword_t;
typedef long __ssize_t;
typedef long __syscall_slong_t;
typedef unsigned long __syscall_ulong_t;
typedef __off64_t __loff_t;
typedef char *__caddr_t;
typedef long __intptr_t;
typedef unsigned int __socklen_t;
typedef int __sig_atomic_t;
typedef __int8_t int8_t;
typedef __int16_t int16_t;
typedef __int32_t int32_t;
typedef __int64_t int64_t;
typedef __uint8_t uint8_t;
typedef __uint16_t uint16_t;
typedef __uint32_t uint32_t;
typedef __uint64_t uint64_t;
typedef __int_least8_t int_least8_t;
typedef __int_least16_t int_least16_t;
typedef __int_least32_t int_least32_t;
typedef __int_least64_t int_least64_t;
typedef __uint_least8_t uint_least8_t;
typedef __uint_least16_t uint_least16_t;
typedef __uint_least32_t uint_least32_t;
typedef __uint_least64_t uint_least64_t;
typedef signed char int_fast8_t;
typedef long int_fast16_t;
typedef long int_fast32_t;
typedef long int_fast64_t;
typedef unsigned char uint_fast8_t;
typedef unsigned long uint_fast16_t;
typedef unsigned long uint_fast32_t;
typedef unsigned long uint_fast64_t;
typedef long intptr_t;
typedef unsigned long uintptr_t;
typedef __intmax_t intmax_t;
typedef __uintmax_t uintmax_t;
typedef _Bool bool;
extern uint64_t revng_init_local_sp(uint64_t param_0);
extern uint64_t init_rbx(void);
extern uint64_t init_cc_src2(void);
extern uint64_t helper_cc_compute_c_wrapper(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint32_t param_3);
extern uint64_t helper_cc_compute_all_wrapper(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint32_t param_3);
void bb_compare_occurs(uint64_t rdi, uint64_t rsi) {
    uint64_t var_17;
    uint64_t var_0;
    uint64_t var_1;
    uint64_t var_2;
    uint64_t var_3;
    uint64_t var_4;
    uint64_t var_5;
    bool var_6;
    uint64_t var_7;
    bool var_8;
    uint64_t var_14;
    uint64_t var_15;
    uint64_t var_16;
    uint64_t rax_0;
    uint64_t rdx_0;
    uint64_t var_18;
    uint64_t var_9;
    uint64_t var_10;
    uint64_t var_11;
    uint64_t rdx_1;
    uint64_t var_12;
    uint64_t var_13;
    uint64_t var_19;
    uint64_t var_20;
    unsigned int loop_state_var;
    var_0 = revng_init_local_sp(0UL);
    var_1 = init_rbx();
    var_2 = init_cc_src2();
    var_3 = *(uint64_t *)(rsi + 8UL);
    var_4 = *(uint64_t *)(rdi + 8UL);
    *(uint64_t *)(var_0 + (-8L)) = var_1;
    var_5 = ((long)var_3 < (long)var_4) ? var_3 : var_4;
    var_6 = (*(unsigned char *)6492960UL == '\x00');
    var_7 = helper_cc_compute_all_wrapper(var_5, 0UL, 0UL, 25U);
    var_8 = ((uint64_t)(((unsigned char)(var_7 >> 4UL) ^ (unsigned char)var_7) & '\xc0') == 0UL);
    rdx_0 = 0UL;
    rdx_1 = 0UL;
    if (var_6) {
        if (var_8) {
            var_14 = *(uint64_t *)rdi;
            var_15 = *(uint64_t *)rsi;
            var_16 = (uint64_t)((uint32_t)(uint64_t)*(unsigned char *)var_14 - (uint32_t)(uint64_t)*(unsigned char *)var_15);
            rax_0 = var_16;
            if (var_16 != 0UL) {
                while (1U)
                    {
                        var_17 = rdx_0 + 1UL;
                        rdx_0 = var_17;
                        if (var_17 != var_5) {
                            loop_state_var = 0U;
                            break;
                        }
                        var_18 = (uint64_t)((uint32_t)(uint64_t)*(unsigned char *)(var_17 + var_14) - (uint32_t)(uint64_t)*(unsigned char *)(var_17 + var_15));
                        rax_0 = var_18;
                        if (var_18 == 0UL) {
                            continue;
                        }
                        loop_state_var = 1U;
                        break;
                    }
                if ((long)var_3 <= (long)var_4) {
                    return;
                }
                var_19 = helper_cc_compute_all_wrapper(var_3 - var_4, var_4, var_2, 17U);
                rax_0 = ((var_19 >> 11UL) ^ (var_19 >> 7UL)) & 1UL;
            }
        } else {
            if ((long)var_3 > (long)var_4) {
                return;
            }
            var_19 = helper_cc_compute_all_wrapper(var_3 - var_4, var_4, var_2, 17U);
            rax_0 = ((var_19 >> 11UL) ^ (var_19 >> 7UL)) & 1UL;
        }
    } else {
        if (!var_8) {
            if ((long)var_3 > (long)var_4) {
                return;
            }
            var_19 = helper_cc_compute_all_wrapper(var_3 - var_4, var_4, var_2, 17U);
            rax_0 = ((var_19 >> 11UL) ^ (var_19 >> 7UL)) & 1UL;
            if (rax_0 == 0UL) {
                var_20 = *(uint64_t *)rsi;
                helper_cc_compute_c_wrapper(*(uint64_t *)rdi - var_20, var_20, var_2, 17U);
            }
            return;
        }
        var_9 = *(uint64_t *)rdi;
        var_10 = *(uint64_t *)rsi;
        var_11 = (uint64_t)((uint32_t)(uint64_t)*(unsigned char *)((uint64_t)*(unsigned char *)var_9 + 6491968UL) - (uint32_t)(uint64_t)*(unsigned char *)((uint64_t)*(unsigned char *)var_10 + 6491968UL));
        rax_0 = var_11;
        if (var_11 != 0UL) {
            while (1U)
                {
                    var_12 = rdx_1 + 1UL;
                    rdx_1 = var_12;
                    if (var_12 == var_5) {
                        var_13 = (uint64_t)((uint32_t)(uint64_t)*(unsigned char *)((uint64_t)*(unsigned char *)(var_12 + var_9) + 6491968UL) - (uint32_t)(uint64_t)*(unsigned char *)((uint64_t)*(unsigned char *)(var_12 + var_10) + 6491968UL));
                        rax_0 = var_13;
                        if (var_13 == 0UL) {
                            continue;
                        }
                        loop_state_var = 2U;
                        break;
                    }
                    if ((long)var_3 <= (long)var_4) {
                        loop_state_var = 1U;
                        break;
                    }
                    loop_state_var = 0U;
                    break;
                }
            switch (loop_state_var) {
              case 0U:
                {
                    return;
                }
                break;
              case 2U:
              case 1U:
                {
                    var_19 = helper_cc_compute_all_wrapper(var_3 - var_4, var_4, var_2, 17U);
                    rax_0 = ((var_19 >> 11UL) ^ (var_19 >> 7UL)) & 1UL;
                }
                break;
            }
        }
    }
}

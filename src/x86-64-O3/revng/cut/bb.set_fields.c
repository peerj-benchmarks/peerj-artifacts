typedef unsigned char __u_char;
typedef unsigned short __u_short;
typedef unsigned int __u_int;
typedef unsigned long __u_long;
typedef signed char __int8_t;
typedef unsigned char __uint8_t;
typedef short __int16_t;
typedef unsigned short __uint16_t;
typedef int __int32_t;
typedef unsigned int __uint32_t;
typedef long __int64_t;
typedef unsigned long __uint64_t;
typedef __int8_t __int_least8_t;
typedef __uint8_t __uint_least8_t;
typedef __int16_t __int_least16_t;
typedef __uint16_t __uint_least16_t;
typedef __int32_t __int_least32_t;
typedef __uint32_t __uint_least32_t;
typedef __int64_t __int_least64_t;
typedef __uint64_t __uint_least64_t;
typedef long __quad_t;
typedef unsigned long __u_quad_t;
typedef long __intmax_t;
typedef unsigned long __uintmax_t;
typedef unsigned long __dev_t;
typedef unsigned int __uid_t;
typedef unsigned int __gid_t;
typedef unsigned long __ino_t;
typedef unsigned long __ino64_t;
typedef unsigned int __mode_t;
typedef unsigned long __nlink_t;
typedef long __off_t;
typedef long __off64_t;
typedef int __pid_t;
typedef struct {
    int __val[2];
} __fsid_t;
typedef long __clock_t;
typedef unsigned long __rlim_t;
typedef unsigned long __rlim64_t;
typedef unsigned int __id_t;
typedef long __time_t;
typedef unsigned int __useconds_t;
typedef long __suseconds_t;
typedef int __daddr_t;
typedef int __key_t;
typedef int __clockid_t;
typedef void *__timer_t;
typedef long __blksize_t;
typedef long __blkcnt_t;
typedef long __blkcnt64_t;
typedef unsigned long __fsblkcnt_t;
typedef unsigned long __fsblkcnt64_t;
typedef unsigned long __fsfilcnt_t;
typedef unsigned long __fsfilcnt64_t;
typedef long __fsword_t;
typedef long __ssize_t;
typedef long __syscall_slong_t;
typedef unsigned long __syscall_ulong_t;
typedef __off64_t __loff_t;
typedef char *__caddr_t;
typedef long __intptr_t;
typedef unsigned int __socklen_t;
typedef int __sig_atomic_t;
typedef __int8_t int8_t;
typedef __int16_t int16_t;
typedef __int32_t int32_t;
typedef __int64_t int64_t;
typedef __uint8_t uint8_t;
typedef __uint16_t uint16_t;
typedef __uint32_t uint32_t;
typedef __uint64_t uint64_t;
typedef __int_least8_t int_least8_t;
typedef __int_least16_t int_least16_t;
typedef __int_least32_t int_least32_t;
typedef __int_least64_t int_least64_t;
typedef __uint_least8_t uint_least8_t;
typedef __uint_least16_t uint_least16_t;
typedef __uint_least32_t uint_least32_t;
typedef __uint_least64_t uint_least64_t;
typedef signed char int_fast8_t;
typedef long int_fast16_t;
typedef long int_fast32_t;
typedef long int_fast64_t;
typedef unsigned char uint_fast8_t;
typedef unsigned long uint_fast16_t;
typedef unsigned long uint_fast32_t;
typedef unsigned long uint_fast64_t;
typedef long intptr_t;
typedef unsigned long uintptr_t;
typedef __intmax_t intmax_t;
typedef __uintmax_t uintmax_t;
typedef _Bool bool;
struct indirect_placeholder_6_ret_type;
struct indirect_placeholder_7_ret_type;
struct indirect_placeholder_8_ret_type;
struct indirect_placeholder_9_ret_type;
struct indirect_placeholder_4_ret_type;
struct indirect_placeholder_10_ret_type;
struct indirect_placeholder_11_ret_type;
struct indirect_placeholder_12_ret_type;
struct indirect_placeholder_13_ret_type;
struct indirect_placeholder_6_ret_type {
    uint64_t field_0;
    uint64_t field_1;
    uint64_t field_2;
};
struct indirect_placeholder_7_ret_type {
    uint64_t field_0;
    uint64_t field_1;
    uint64_t field_2;
};
struct indirect_placeholder_8_ret_type {
    uint64_t field_0;
    uint64_t field_1;
    uint64_t field_2;
};
struct indirect_placeholder_9_ret_type {
    uint64_t field_0;
    uint64_t field_1;
    uint64_t field_2;
};
struct indirect_placeholder_4_ret_type {
    uint64_t field_0;
    uint64_t field_1;
    uint64_t field_2;
};
struct indirect_placeholder_10_ret_type {
    uint64_t field_0;
    uint64_t field_1;
    uint64_t field_2;
};
struct indirect_placeholder_11_ret_type {
    uint64_t field_0;
    uint64_t field_1;
    uint64_t field_2;
};
struct indirect_placeholder_12_ret_type {
    uint64_t field_0;
    uint64_t field_1;
    uint64_t field_2;
};
struct indirect_placeholder_13_ret_type {
    uint64_t field_0;
    uint64_t field_1;
    uint64_t field_2;
};
extern void abort(void);
extern uint64_t revng_init_local_sp(uint64_t param_0);
extern uint64_t init_rbx(void);
extern uint64_t init_rbp(void);
extern void indirect_placeholder(void);
extern uint64_t init_cc_src2(void);
extern uint64_t helper_cc_compute_c_wrapper(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint32_t param_3);
extern uint64_t init_r13(void);
extern uint64_t init_r12(void);
extern uint64_t init_r14(void);
extern uint64_t helper_cc_compute_all_wrapper(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint32_t param_3);
extern uint64_t init_r15(void);
extern uint64_t indirect_placeholder_5(uint64_t param_0, uint64_t param_1);
extern void indirect_placeholder_1(uint64_t param_0, uint64_t param_1);
extern uint64_t init_r9(void);
extern uint64_t init_r8(void);
extern void indirect_placeholder_2(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint64_t param_3, uint64_t param_4, uint64_t param_5, uint64_t param_6);
extern void indirect_placeholder_3(uint64_t param_0, uint64_t param_1, uint64_t param_2);
extern uint64_t init_rcx(void);
extern struct indirect_placeholder_6_ret_type indirect_placeholder_6(uint64_t param_0, uint64_t param_1);
extern struct indirect_placeholder_7_ret_type indirect_placeholder_7(uint64_t param_0, uint64_t param_1);
extern struct indirect_placeholder_8_ret_type indirect_placeholder_8(uint64_t param_0, uint64_t param_1);
extern struct indirect_placeholder_9_ret_type indirect_placeholder_9(uint64_t param_0, uint64_t param_1);
extern struct indirect_placeholder_4_ret_type indirect_placeholder_4(uint64_t param_0);
extern struct indirect_placeholder_10_ret_type indirect_placeholder_10(uint64_t param_0);
extern struct indirect_placeholder_11_ret_type indirect_placeholder_11(uint64_t param_0, uint64_t param_1);
extern struct indirect_placeholder_12_ret_type indirect_placeholder_12(uint64_t param_0, uint64_t param_1);
extern struct indirect_placeholder_13_ret_type indirect_placeholder_13(uint64_t param_0, uint64_t param_1);
void bb_set_fields(uint64_t rdi, uint64_t rsi) {
    uint64_t var_0;
    uint64_t var_1;
    uint64_t var_2;
    uint64_t var_3;
    uint64_t var_4;
    uint64_t var_5;
    uint64_t var_6;
    uint64_t var_7;
    uint64_t var_8;
    uint64_t var_9;
    uint64_t var_10;
    uint64_t var_11;
    uint64_t var_12;
    uint64_t local_sp_13;
    uint64_t rdx_2;
    uint64_t var_102;
    uint64_t var_103;
    uint64_t var_104;
    uint64_t _pre_phi;
    uint64_t rax_0;
    uint64_t var_43;
    uint64_t var_69;
    uint64_t var_70;
    uint64_t rbx_0;
    uint64_t rbp_0;
    uint64_t local_sp_0;
    uint64_t rsi2_2;
    uint64_t rdx_0;
    uint64_t r8_1;
    uint64_t rsi2_0;
    uint64_t r8_0;
    uint64_t var_85;
    uint64_t var_86;
    uint64_t var_87;
    uint64_t var_88;
    uint64_t var_89;
    uint64_t var_90;
    uint64_t var_91;
    uint64_t var_92;
    uint64_t var_93;
    uint64_t rdi1_0;
    uint64_t rcx_0;
    uint64_t local_sp_1;
    uint64_t var_94;
    uint64_t var_95;
    uint64_t var_96;
    uint64_t var_97;
    uint64_t var_98;
    uint64_t var_99;
    uint64_t var_100;
    uint64_t var_101;
    uint64_t local_sp_2;
    uint64_t local_sp_9;
    uint64_t r15_0;
    uint64_t r13_0;
    uint64_t var_105;
    uint64_t local_sp_3;
    uint64_t rdx_3;
    uint64_t rsi2_3;
    uint64_t r8_2;
    uint64_t local_sp_11;
    uint64_t rdi1_1;
    uint64_t local_sp_4;
    uint64_t rsi2_4;
    uint64_t var_146;
    uint64_t var_147;
    struct indirect_placeholder_6_ret_type var_148;
    uint64_t var_149;
    uint64_t var_150;
    uint64_t var_151;
    uint64_t var_152;
    uint64_t var_106;
    uint64_t rcx_7;
    uint64_t var_107;
    uint64_t var_108;
    struct indirect_placeholder_7_ret_type var_109;
    uint64_t var_110;
    uint64_t var_111;
    uint64_t var_112;
    uint64_t var_113;
    uint64_t var_114;
    uint64_t var_115;
    uint64_t var_116;
    uint64_t r9_0;
    uint64_t local_sp_5;
    uint64_t rdx_4;
    uint64_t r8_3;
    uint64_t r9_4;
    uint64_t var_117;
    uint64_t rsi2_6;
    uint64_t var_140;
    uint64_t r9_1;
    uint64_t r8_6;
    uint64_t local_sp_6;
    uint64_t r8_4;
    uint64_t var_118;
    uint64_t var_119;
    uint64_t r9_3;
    uint64_t var_120;
    uint64_t var_121;
    uint64_t var_122;
    uint64_t var_123;
    uint64_t rax_1;
    uint64_t r9_12;
    uint64_t var_124;
    uint64_t rcx_1;
    uint64_t rcx_2;
    uint64_t rsi2_5;
    uint64_t var_125;
    struct indirect_placeholder_8_ret_type var_126;
    uint64_t var_127;
    uint64_t var_128;
    uint64_t var_129;
    uint64_t var_130;
    uint64_t r9_2;
    uint64_t rcx_3;
    uint64_t local_sp_7;
    uint64_t r8_5;
    uint64_t var_131;
    uint64_t var_132;
    uint64_t local_sp_8;
    uint64_t var_133;
    uint64_t r14_0;
    uint64_t r8_7;
    uint64_t var_134;
    uint64_t var_135;
    uint64_t var_136;
    bool var_137;
    uint64_t var_138;
    uint64_t rax_3;
    uint64_t rcx_4;
    uint64_t local_sp_10;
    uint64_t rdx_6;
    uint64_t var_144;
    uint64_t var_139;
    uint64_t rax_2;
    uint64_t local_sp_20;
    uint64_t rsi2_10;
    uint64_t r8_13;
    uint64_t var_81;
    struct indirect_placeholder_9_ret_type var_141;
    uint64_t var_142;
    uint64_t var_143;
    uint64_t var_145;
    unsigned char var_22;
    uint64_t var_23;
    uint32_t var_24;
    uint64_t var_25;
    uint64_t local_sp_15;
    uint64_t local_sp_25;
    uint64_t r12_1;
    bool var_26;
    uint64_t var_27;
    uint64_t r9_13;
    uint64_t var_28;
    uint64_t rcx_9;
    uint64_t rax_4;
    uint64_t var_29;
    uint64_t var_30;
    uint64_t rax_12;
    uint64_t r14_2;
    uint64_t var_31;
    uint64_t var_32;
    struct indirect_placeholder_4_ret_type var_33;
    uint64_t var_34;
    uint64_t var_35;
    uint64_t var_36;
    uint64_t var_37;
    uint64_t local_sp_24;
    struct indirect_placeholder_10_ret_type var_38;
    uint64_t var_39;
    uint64_t var_40;
    uint64_t var_41;
    uint64_t var_42;
    uint64_t var_14;
    uint64_t r15_4;
    uint64_t rax_5;
    uint64_t r13_2;
    uint64_t r15_1;
    uint64_t rbx_3;
    uint64_t r13_1;
    uint64_t rbx_2;
    uint64_t r12_0;
    uint64_t r9_5;
    uint64_t rbp_1;
    uint64_t rcx_5;
    unsigned char rdx_17;
    uint64_t local_sp_14;
    unsigned char rdx_7;
    uint64_t r8_16;
    uint64_t r14_1;
    uint64_t r8_8;
    uint64_t var_15;
    unsigned char *var_16;
    unsigned char var_17;
    uint64_t var_44;
    uint64_t r15_2;
    uint64_t r9_14;
    uint64_t var_45;
    uint64_t var_46;
    uint64_t var_47;
    uint64_t rdx_10;
    uint64_t rax_6;
    uint64_t r9_6;
    uint64_t local_sp_16;
    uint64_t rdx_8;
    uint64_t r8_9;
    uint64_t var_55;
    uint64_t var_56;
    uint64_t rax_8;
    uint64_t var_48;
    uint64_t rdx_9;
    uint64_t rsi2_7;
    uint64_t var_49;
    struct indirect_placeholder_11_ret_type var_50;
    uint64_t var_51;
    uint64_t var_52;
    uint64_t var_53;
    uint64_t var_54;
    uint64_t var_57;
    uint64_t var_58;
    uint64_t var_59;
    uint64_t rdx_13;
    uint64_t rax_7;
    uint64_t r9_7;
    uint64_t local_sp_17;
    uint64_t rdx_11;
    uint64_t r8_10;
    uint64_t var_67;
    uint64_t var_68;
    uint64_t r9_8;
    uint64_t rcx_6;
    uint64_t local_sp_18;
    uint64_t rsi2_8;
    uint64_t r8_11;
    uint64_t r9_10;
    uint64_t var_60;
    uint64_t rdx_12;
    uint64_t rsi2_9;
    uint64_t var_61;
    struct indirect_placeholder_12_ret_type var_62;
    uint64_t var_63;
    uint64_t var_64;
    uint64_t var_65;
    uint64_t var_66;
    uint64_t cc_src_1;
    uint64_t var_71;
    uint64_t rdx_16;
    uint64_t rax_9;
    uint64_t r9_9;
    uint64_t local_sp_19;
    uint64_t rdx_14;
    uint64_t r8_12;
    uint64_t var_79;
    uint64_t var_80;
    uint64_t var_82;
    uint64_t var_83;
    uint64_t var_84;
    uint64_t var_153;
    uint64_t var_72;
    uint64_t rdx_15;
    uint64_t local_sp_22;
    uint64_t r8_15;
    uint64_t rsi2_11;
    uint64_t var_73;
    struct indirect_placeholder_13_ret_type var_74;
    uint64_t var_75;
    uint64_t var_76;
    uint64_t var_77;
    uint64_t var_78;
    uint64_t var_18;
    uint64_t var_19;
    unsigned char var_20;
    uint64_t var_21;
    bool var_154;
    bool var_155;
    uint64_t storemerge;
    uint64_t storemerge10;
    uint64_t var_156;
    uint64_t spec_select;
    uint64_t spec_select319;
    uint64_t var_157;
    uint64_t var_158;
    uint64_t rcx_10;
    uint64_t local_sp_27;
    uint64_t rdx_19;
    uint64_t r8_17;
    uint64_t var_13;
    unsigned int loop_state_var;
    var_0 = revng_init_local_sp(0UL);
    var_1 = init_r15();
    var_2 = init_r13();
    var_3 = init_rbx();
    var_4 = init_r12();
    var_5 = init_r9();
    var_6 = init_rbp();
    var_7 = init_rcx();
    var_8 = init_cc_src2();
    var_9 = init_r14();
    var_10 = init_r8();
    *(uint64_t *)(var_0 + (-8L)) = var_1;
    *(uint64_t *)(var_0 + (-16L)) = var_9;
    *(uint64_t *)(var_0 + (-24L)) = var_2;
    *(uint64_t *)(var_0 + (-32L)) = var_4;
    *(uint64_t *)(var_0 + (-40L)) = var_6;
    *(uint64_t *)(var_0 + (-48L)) = var_3;
    var_11 = var_0 + (-88L);
    var_12 = rsi & 1UL;
    *(uint32_t *)(var_0 + (-60L)) = (uint32_t)rsi;
    *(uint32_t *)(var_0 + (-64L)) = (uint32_t)var_12;
    local_sp_13 = var_11;
    rbx_0 = 0UL;
    rbp_0 = 0UL;
    r15_0 = 1UL;
    rsi2_6 = 128UL;
    rcx_2 = 8UL;
    rsi2_5 = 128UL;
    rax_3 = 8UL;
    r12_1 = 0UL;
    r14_2 = 0UL;
    r13_2 = 0UL;
    r15_1 = 1UL;
    rbx_3 = 0UL;
    r13_1 = 0UL;
    rbx_2 = 0UL;
    r12_0 = 0UL;
    r9_5 = var_5;
    rcx_5 = var_7;
    rdx_17 = (unsigned char)'\x00';
    rdx_7 = (unsigned char)'\x00';
    r14_1 = 0UL;
    r8_8 = var_10;
    rdx_10 = 8UL;
    rsi2_7 = 128UL;
    rdx_13 = 8UL;
    rsi2_9 = 128UL;
    cc_src_1 = 45UL;
    rdx_16 = 8UL;
    rsi2_11 = 128UL;
    storemerge = 4267712UL;
    storemerge10 = 4267942UL;
    rdx_19 = 4268044UL;
    if (var_12 == 0UL) {
        var_13 = var_0 + (-96L);
        *(uint64_t *)var_13 = 4207313UL;
        indirect_placeholder();
        local_sp_13 = var_13;
    }
    var_14 = rdi + 1UL;
    rax_5 = var_14;
    rbp_1 = var_14;
    local_sp_14 = local_sp_13;
    while (1U)
        {
            var_15 = rbp_1 + (-1L);
            var_16 = (unsigned char *)var_15;
            var_17 = *var_16;
            r9_12 = r9_5;
            local_sp_15 = local_sp_14;
            local_sp_25 = local_sp_14;
            r9_13 = r9_5;
            rcx_9 = rcx_5;
            rax_12 = rax_5;
            local_sp_24 = local_sp_14;
            r15_4 = r15_1;
            r8_16 = r8_8;
            r15_2 = r15_1;
            r9_14 = r9_5;
            r9_6 = r9_5;
            r8_9 = r8_8;
            r9_7 = r9_5;
            r8_10 = r8_8;
            r9_9 = r9_5;
            r8_12 = r8_8;
            r8_15 = r8_8;
            rcx_10 = rcx_5;
            r8_17 = r8_8;
            if ((uint64_t)(var_17 + '\xd3') != 0UL) {
                rcx_9 = 1844674407370955161UL;
                rdx_17 = (unsigned char)'\x01';
                cc_src_1 = 44UL;
                if ((uint64_t)(var_17 + '\xd4') != 0UL) {
                    *(uint64_t *)(local_sp_14 + 16UL) = var_15;
                    var_18 = local_sp_14 + 8UL;
                    *(unsigned char *)var_18 = rdx_7;
                    var_19 = local_sp_14 + (-8L);
                    *(uint64_t *)var_19 = 4205614UL;
                    indirect_placeholder();
                    var_20 = *(unsigned char *)local_sp_14;
                    var_21 = *(uint64_t *)var_18;
                    local_sp_15 = var_19;
                    r13_2 = r13_1;
                    local_sp_24 = var_19;
                    var_22 = *var_16;
                    var_23 = (uint64_t)var_22;
                    var_24 = (uint32_t)var_23;
                    var_25 = (uint64_t)var_24;
                    rax_4 = var_25;
                    if ((uint64_t)(uint32_t)rax_5 != 0UL & (uint64_t)var_22 != 0UL) {
                        if ((uint64_t)((var_24 + (-48)) & (-2)) > 9UL) {
                            *(uint64_t *)(local_sp_14 + (-16L)) = 4207722UL;
                            var_38 = indirect_placeholder_10(var_21);
                            var_39 = var_38.field_0;
                            var_40 = var_38.field_1;
                            var_41 = var_38.field_2;
                            var_42 = ((*(unsigned char *)(local_sp_14 + 12UL) & '\x04') == '\x00') ? 4267998UL : 4267800UL;
                            *(uint64_t *)(local_sp_14 + (-24L)) = 4207755UL;
                            indirect_placeholder_2(0UL, 0UL, var_40, var_39, var_42, 0UL, var_41);
                            *(uint64_t *)(local_sp_14 + (-32L)) = 4207765UL;
                            indirect_placeholder_3(rbx_2, 1UL, rbp_1);
                            abort();
                        }
                        if (var_20 == '\x00') {
                            *(uint64_t *)6376520UL = var_21;
                        } else {
                            if (*(uint64_t *)6376520UL == 0UL) {
                                *(uint64_t *)6376520UL = var_21;
                            }
                        }
                        var_26 = ((uint64_t)(unsigned char)r13_1 == 0UL);
                        var_27 = var_26 ? 1UL : (uint64_t)(uint32_t)r14_1;
                        var_28 = var_26 ? (uint64_t)(uint32_t)rbx_2 : 1UL;
                        rbx_3 = var_28;
                        r14_2 = var_27;
                        if (r12_0 <= 1844674407370955161UL) {
                            loop_state_var = 4U;
                            break;
                        }
                        var_29 = (r12_0 * 10UL) + (uint64_t)((long)((var_23 << 32UL) + (-206158430208L)) >> (long)32UL);
                        var_30 = helper_cc_compute_c_wrapper(var_29 - r12_0, r12_0, var_8, 17U);
                        rax_4 = var_29;
                        rax_12 = var_29;
                        r12_1 = var_29;
                        if (!((var_30 != 0UL) || (var_29 == 18446744073709551615UL))) {
                            loop_state_var = 4U;
                            break;
                        }
                        rax_5 = rax_12;
                        r15_1 = r15_4;
                        r13_1 = r13_2;
                        rbx_2 = rbx_3;
                        r12_0 = r12_1;
                        r9_5 = r9_13;
                        rbp_1 = rbp_1 + 1UL;
                        rcx_5 = rcx_9;
                        local_sp_14 = local_sp_24;
                        rdx_7 = rdx_17;
                        r14_1 = r14_2;
                        r8_8 = r8_16;
                        continue;
                    }
                }
                local_sp_25 = local_sp_15;
                local_sp_16 = local_sp_15;
                local_sp_17 = local_sp_15;
                local_sp_19 = local_sp_15;
                local_sp_22 = local_sp_15;
                local_sp_27 = local_sp_15;
                if ((uint64_t)(unsigned char)r13_1 == 0UL) {
                    if (r12_0 != 0UL) {
                        loop_state_var = 3U;
                        break;
                    }
                    var_69 = *(uint64_t *)6376968UL;
                    var_70 = *(uint64_t *)6376528UL;
                    var_71 = *(uint64_t *)6376960UL;
                    rax_9 = var_71;
                    rdx_14 = var_69;
                    rdx_15 = var_69;
                    if (var_69 != var_70) {
                        if (var_71 == 0UL) {
                            if (var_69 != 0UL) {
                                if (var_69 <= 576460752303423487UL) {
                                    loop_state_var = 0U;
                                    break;
                                }
                                rdx_16 = rdx_15;
                                rsi2_11 = rdx_15 << 4UL;
                            }
                        } else {
                            if (var_69 <= 384307168202282324UL) {
                                loop_state_var = 0U;
                                break;
                            }
                            var_72 = (var_69 + (var_69 >> 1UL)) + 1UL;
                            rdx_15 = var_72;
                            rdx_16 = rdx_15;
                            rsi2_11 = rdx_15 << 4UL;
                        }
                        *(uint64_t *)6376528UL = rdx_16;
                        var_73 = local_sp_15 + (-8L);
                        *(uint64_t *)var_73 = 4206441UL;
                        var_74 = indirect_placeholder_13(var_71, rsi2_11);
                        var_75 = var_74.field_0;
                        var_76 = var_74.field_1;
                        var_77 = var_74.field_2;
                        var_78 = *(uint64_t *)6376968UL;
                        *(uint64_t *)6376960UL = var_75;
                        rax_9 = var_75;
                        r9_9 = var_76;
                        local_sp_19 = var_73;
                        rdx_14 = var_78;
                        r8_12 = var_77;
                    }
                    var_79 = rdx_14 + 1UL;
                    var_80 = (rdx_14 << 4UL) + rax_9;
                    *(uint64_t *)var_80 = r12_0;
                    *(uint64_t *)(var_80 + 8UL) = r12_0;
                    *(uint64_t *)6376968UL = var_79;
                    rcx_7 = var_80;
                    local_sp_20 = local_sp_19;
                    rsi2_10 = var_79;
                    r8_13 = r8_12;
                    r9_13 = r9_9;
                    rcx_9 = var_80;
                    rax_12 = rax_9;
                    local_sp_24 = local_sp_19;
                    r8_16 = r8_12;
                    r9_10 = r9_9;
                    if (*var_16 == '\x00') {
                        loop_state_var = 1U;
                        break;
                    }
                }
                var_43 = (uint64_t)(uint32_t)rbx_2 ^ 1UL;
                var_44 = helper_cc_compute_c_wrapper(r14_1 - var_43, var_43, var_8, 14U);
                if (var_44 != 0UL) {
                    r15_2 = 1UL;
                    rdx_19 = 4267888UL;
                    if (*(uint32_t *)(local_sp_15 + 24UL) != 0U) {
                        loop_state_var = 2U;
                        break;
                    }
                }
                r15_4 = r15_2;
                if ((uint64_t)(unsigned char)rbx_2 == 0UL) {
                    var_57 = *(uint64_t *)6376968UL;
                    var_58 = *(uint64_t *)6376528UL;
                    var_59 = *(uint64_t *)6376960UL;
                    rax_7 = var_59;
                    rdx_11 = var_57;
                    rdx_12 = var_57;
                    if (var_57 != var_58) {
                        if (var_59 == 0UL) {
                            if (var_57 != 0UL) {
                                if (var_57 <= 576460752303423487UL) {
                                    loop_state_var = 0U;
                                    break;
                                }
                                rdx_13 = rdx_12;
                                rsi2_9 = rdx_12 << 4UL;
                            }
                        } else {
                            if (var_57 <= 384307168202282324UL) {
                                loop_state_var = 0U;
                                break;
                            }
                            var_60 = (var_57 + (var_57 >> 1UL)) + 1UL;
                            rdx_12 = var_60;
                            rdx_13 = rdx_12;
                            rsi2_9 = rdx_12 << 4UL;
                        }
                        *(uint64_t *)6376528UL = rdx_13;
                        var_61 = local_sp_15 + (-8L);
                        *(uint64_t *)var_61 = 4206705UL;
                        var_62 = indirect_placeholder_12(var_59, rsi2_9);
                        var_63 = var_62.field_0;
                        var_64 = var_62.field_1;
                        var_65 = var_62.field_2;
                        var_66 = *(uint64_t *)6376968UL;
                        *(uint64_t *)6376960UL = var_63;
                        rax_7 = var_63;
                        r9_7 = var_64;
                        local_sp_17 = var_61;
                        rdx_11 = var_66;
                        r8_10 = var_65;
                    }
                    var_67 = rdx_11 + 1UL;
                    var_68 = (rdx_11 << 4UL) + rax_7;
                    *(uint64_t *)var_68 = r15_2;
                    *(uint64_t *)(var_68 + 8UL) = 18446744073709551615UL;
                    rax_8 = rax_7;
                    r9_8 = r9_7;
                    rcx_6 = var_68;
                    local_sp_18 = local_sp_17;
                    rsi2_8 = var_67;
                    r8_11 = r8_10;
                } else {
                    if (r15_2 <= r12_0) {
                        loop_state_var = 2U;
                        break;
                    }
                    var_45 = *(uint64_t *)6376968UL;
                    var_46 = *(uint64_t *)6376528UL;
                    var_47 = *(uint64_t *)6376960UL;
                    rax_6 = var_47;
                    rdx_8 = var_45;
                    rdx_9 = var_45;
                    if (var_45 != var_46) {
                        if (var_47 == 0UL) {
                            if (var_45 != 0UL) {
                                if (var_45 <= 576460752303423487UL) {
                                    loop_state_var = 0U;
                                    break;
                                }
                                rdx_10 = rdx_9;
                                rsi2_7 = rdx_9 << 4UL;
                            }
                        } else {
                            if (var_45 <= 384307168202282324UL) {
                                loop_state_var = 0U;
                                break;
                            }
                            var_48 = (var_45 + (var_45 >> 1UL)) + 1UL;
                            rdx_9 = var_48;
                            rdx_10 = rdx_9;
                            rsi2_7 = rdx_9 << 4UL;
                        }
                        *(uint64_t *)6376528UL = rdx_10;
                        var_49 = local_sp_15 + (-8L);
                        *(uint64_t *)var_49 = 4206593UL;
                        var_50 = indirect_placeholder_11(var_47, rsi2_7);
                        var_51 = var_50.field_0;
                        var_52 = var_50.field_1;
                        var_53 = var_50.field_2;
                        var_54 = *(uint64_t *)6376968UL;
                        *(uint64_t *)6376960UL = var_51;
                        rax_6 = var_51;
                        r9_6 = var_52;
                        local_sp_16 = var_49;
                        rdx_8 = var_54;
                        r8_9 = var_53;
                    }
                    var_55 = rdx_8 + 1UL;
                    var_56 = (rdx_8 << 4UL) + rax_6;
                    *(uint64_t *)var_56 = r15_2;
                    *(uint64_t *)(var_56 + 8UL) = r12_0;
                    rax_8 = rax_6;
                    r9_8 = r9_6;
                    rcx_6 = var_56;
                    local_sp_18 = local_sp_16;
                    rsi2_8 = var_55;
                    r8_11 = r8_9;
                }
                *(uint64_t *)6376968UL = rsi2_8;
                rcx_7 = rcx_6;
                local_sp_20 = local_sp_18;
                rsi2_10 = rsi2_8;
                r8_13 = r8_11;
                r9_13 = r9_8;
                rcx_9 = rcx_6;
                rax_12 = rax_8;
                local_sp_24 = local_sp_18;
                r8_16 = r8_11;
                r9_10 = r9_8;
                if (*var_16 != '\x00') {
                    loop_state_var = 1U;
                    break;
                }
                rax_5 = rax_12;
                r15_1 = r15_4;
                r13_1 = r13_2;
                rbx_2 = rbx_3;
                r12_0 = r12_1;
                r9_5 = r9_13;
                rbp_1 = rbp_1 + 1UL;
                rcx_5 = rcx_9;
                local_sp_14 = local_sp_24;
                rdx_7 = rdx_17;
                r14_1 = r14_2;
                r8_8 = r8_16;
                continue;
            }
            r14_2 = r14_1;
            r15_4 = 1UL;
            r13_2 = 1UL;
            rbx_3 = rbx_2;
            storemerge = 4267680UL;
            storemerge10 = 4267922UL;
            if ((uint64_t)(unsigned char)r13_1 != 0UL) {
                loop_state_var = 3U;
                break;
            }
            var_154 = (r12_0 == 0UL);
            var_155 = ((uint64_t)(unsigned char)r14_1 == 0UL);
            storemerge10 = 4267942UL;
            storemerge = 4267712UL;
            if (var_154) {
                if (!var_155) {
                    loop_state_var = 3U;
                    break;
                }
            }
            var_156 = (uint64_t)(uint32_t)r14_1;
            spec_select = var_155 ? 1UL : r12_0;
            spec_select319 = var_155 ? 1UL : var_156;
            r15_4 = spec_select;
            r13_2 = spec_select319;
        }
    switch (loop_state_var) {
      case 0U:
        {
            *(uint64_t *)(local_sp_22 + (-8L)) = 4207556UL;
            indirect_placeholder_1(r9_12, r8_15);
            abort();
        }
        break;
      case 4U:
        {
            *(uint64_t *)(local_sp_14 + (-16L)) = 4207637UL;
            indirect_placeholder();
            var_31 = *(uint64_t *)6376520UL;
            *(uint64_t *)(local_sp_14 + (-24L)) = 4207652UL;
            var_32 = indirect_placeholder_5(var_31, rax_4);
            *(uint64_t *)(local_sp_14 + (-32L)) = 4207663UL;
            var_33 = indirect_placeholder_4(var_32);
            var_34 = var_33.field_0;
            var_35 = var_33.field_1;
            var_36 = var_33.field_2;
            var_37 = ((*(unsigned char *)(local_sp_14 + (-4L)) & '\x04') == '\x00') ? 4267969UL : 4267760UL;
            *(uint64_t *)(local_sp_14 + (-40L)) = 4207696UL;
            indirect_placeholder_2(0UL, 0UL, var_35, var_34, var_37, 0UL, var_36);
            *(uint64_t *)(local_sp_14 + (-48L)) = 4207704UL;
            indirect_placeholder();
            *(uint64_t *)(local_sp_14 + (-56L)) = 4207714UL;
            indirect_placeholder_3(var_32, 1UL, rbp_1);
            abort();
        }
        break;
      case 1U:
        {
            r8_0 = r8_13;
            r8_2 = r8_13;
            r9_0 = r9_10;
            r9_14 = r9_10;
            rcx_10 = rcx_7;
            local_sp_27 = local_sp_20;
            r8_17 = r8_13;
            if (rsi2_10 == 0UL) {
                var_153 = ((*(unsigned char *)(local_sp_20 + 28UL) & '\x04') == '\x00') ? 4268021UL : 4267840UL;
                rdx_19 = var_153;
                *(uint64_t *)(local_sp_27 + (-8L)) = 4207517UL;
                indirect_placeholder_2(0UL, 0UL, r9_14, rcx_10, rdx_19, 0UL, r8_17);
                *(uint64_t *)(local_sp_27 + (-16L)) = 4207527UL;
                indirect_placeholder_3(rbx_2, 1UL, rbp_1);
                abort();
            }
            var_81 = local_sp_20 + (-8L);
            *(uint64_t *)var_81 = 4206027UL;
            indirect_placeholder();
            var_82 = *(uint64_t *)6376968UL;
            var_83 = *(uint64_t *)6376960UL;
            var_84 = helper_cc_compute_c_wrapper(0UL - var_82, var_82, var_8, 17U);
            local_sp_0 = var_81;
            rdx_0 = var_82;
            rsi2_0 = var_83;
            local_sp_3 = var_81;
            rdx_3 = var_82;
            rsi2_3 = var_83;
            if (var_84 == 0UL) {
                while (1U)
                    {
                        var_85 = rbx_0 + 1UL;
                        var_86 = var_85 - rdx_0;
                        var_87 = helper_cc_compute_c_wrapper(var_86, rdx_0, var_8, 17U);
                        rdx_2 = rdx_0;
                        _pre_phi = var_86;
                        rbx_0 = var_85;
                        rsi2_2 = rsi2_0;
                        r8_1 = r8_0;
                        local_sp_1 = local_sp_0;
                        local_sp_2 = local_sp_0;
                        var_88 = rbp_0 + rsi2_0;
                        var_89 = var_85 << 4UL;
                        var_90 = var_89 + rsi2_0;
                        var_91 = *(uint64_t *)(var_88 + 8UL);
                        var_92 = *(uint64_t *)var_90;
                        var_93 = helper_cc_compute_c_wrapper(var_91 - var_92, var_92, var_8, 17U);
                        rax_0 = var_91;
                        rdi1_0 = var_90;
                        rcx_0 = var_88;
                        if (var_87 != 0UL & var_93 == 0UL) {
                            var_94 = *(uint64_t *)(rdi1_0 + 8UL);
                            var_95 = helper_cc_compute_c_wrapper(rax_0 - var_94, var_94, var_8, 17U);
                            *(uint64_t *)(rcx_0 + 8UL) = ((var_95 == 0UL) ? rax_0 : var_94);
                            var_96 = local_sp_1 + (-8L);
                            *(uint64_t *)var_96 = 4206163UL;
                            indirect_placeholder();
                            var_97 = *(uint64_t *)6376968UL;
                            var_98 = *(uint64_t *)6376960UL;
                            var_99 = var_97 + (-1L);
                            var_100 = var_85 - var_99;
                            *(uint64_t *)6376968UL = var_99;
                            var_101 = helper_cc_compute_c_wrapper(var_100, var_99, var_8, 17U);
                            rdx_2 = var_99;
                            _pre_phi = var_100;
                            rsi2_2 = var_98;
                            r8_1 = var_94;
                            local_sp_1 = var_96;
                            local_sp_2 = var_96;
                            while (var_101 != 0UL)
                                {
                                    var_102 = var_89 + var_98;
                                    var_103 = rbp_0 + var_98;
                                    var_104 = *(uint64_t *)(var_103 + 8UL);
                                    rax_0 = var_104;
                                    rdi1_0 = var_102;
                                    rcx_0 = var_103;
                                    if (*(uint64_t *)var_102 > var_104) {
                                        break;
                                    }
                                    var_94 = *(uint64_t *)(rdi1_0 + 8UL);
                                    var_95 = helper_cc_compute_c_wrapper(rax_0 - var_94, var_94, var_8, 17U);
                                    *(uint64_t *)(rcx_0 + 8UL) = ((var_95 == 0UL) ? rax_0 : var_94);
                                    var_96 = local_sp_1 + (-8L);
                                    *(uint64_t *)var_96 = 4206163UL;
                                    indirect_placeholder();
                                    var_97 = *(uint64_t *)6376968UL;
                                    var_98 = *(uint64_t *)6376960UL;
                                    var_99 = var_97 + (-1L);
                                    var_100 = var_85 - var_99;
                                    *(uint64_t *)6376968UL = var_99;
                                    var_101 = helper_cc_compute_c_wrapper(var_100, var_99, var_8, 17U);
                                    rdx_2 = var_99;
                                    _pre_phi = var_100;
                                    rsi2_2 = var_98;
                                    r8_1 = var_94;
                                    local_sp_1 = var_96;
                                    local_sp_2 = var_96;
                                }
                        }
                        var_105 = helper_cc_compute_c_wrapper(_pre_phi, rdx_2, var_8, 17U);
                        local_sp_0 = local_sp_2;
                        rdx_0 = rdx_2;
                        rsi2_0 = rsi2_2;
                        r8_0 = r8_1;
                        local_sp_3 = local_sp_2;
                        rdx_3 = rdx_2;
                        rsi2_3 = rsi2_2;
                        r8_2 = r8_1;
                        if (var_105 == 0UL) {
                            break;
                        }
                        rbp_0 = rbp_0 + 16UL;
                        continue;
                    }
            }
            rdi1_1 = rsi2_3;
            local_sp_4 = local_sp_3;
            rsi2_4 = rdx_3;
            local_sp_5 = local_sp_3;
            rdx_4 = rdx_3;
            r8_3 = r8_2;
            r14_0 = rsi2_3;
            if ((*(unsigned char *)(local_sp_3 + 28UL) & '\x02') == '\x00') {
                var_146 = rsi2_4 + 1UL;
                *(uint64_t *)6376968UL = var_146;
                var_147 = var_146 << 4UL;
                *(uint64_t *)(local_sp_4 + (-8L)) = 4206246UL;
                var_148 = indirect_placeholder_6(rdi1_1, var_147);
                var_149 = var_148.field_0;
                var_150 = *(uint64_t *)6376968UL;
                *(uint64_t *)6376960UL = var_149;
                var_151 = (var_150 << 4UL) + var_149;
                var_152 = var_151 + (-16L);
                *(uint64_t *)(var_151 + (-8L)) = 18446744073709551615UL;
                *(uint64_t *)var_152 = 18446744073709551615UL;
                return;
            }
            var_106 = *(uint64_t *)rsi2_3;
            *(uint64_t *)6376960UL = 0UL;
            *(uint64_t *)6376968UL = 0UL;
            *(uint64_t *)6376528UL = 0UL;
            if (var_106 > 1UL) {
                *(uint64_t *)(local_sp_3 + 8UL) = rdx_3;
                *(uint64_t *)6376528UL = 8UL;
                var_107 = var_106 + (-1L);
                var_108 = local_sp_3 + (-8L);
                *(uint64_t *)var_108 = 4206938UL;
                var_109 = indirect_placeholder_7(0UL, 128UL);
                var_110 = var_109.field_0;
                var_111 = var_109.field_1;
                var_112 = var_109.field_2;
                var_113 = *(uint64_t *)6376968UL;
                var_114 = *(uint64_t *)local_sp_3;
                *(uint64_t *)6376960UL = var_110;
                var_115 = var_113 + 1UL;
                var_116 = var_110 + (var_113 << 4UL);
                *(uint64_t *)var_116 = 1UL;
                *(uint64_t *)(var_116 + 8UL) = var_107;
                *(uint64_t *)6376968UL = var_115;
                r9_0 = var_111;
                local_sp_5 = var_108;
                rdx_4 = var_114;
                r8_3 = var_112;
            }
            local_sp_9 = local_sp_5;
            r9_4 = r9_0;
            r9_1 = r9_0;
            local_sp_6 = local_sp_5;
            r8_4 = r8_3;
            r8_7 = r8_3;
            if (rdx_4 <= 1UL) {
                var_117 = rsi2_3 + 8UL;
                *(uint64_t *)(local_sp_5 + 8UL) = rsi2_3;
                r13_0 = var_117;
                while (1U)
                    {
                        var_118 = *(uint64_t *)r13_0 + 1UL;
                        var_119 = *(uint64_t *)(r13_0 + 8UL);
                        r8_6 = r8_4;
                        r9_3 = r9_1;
                        r9_12 = r9_1;
                        r9_2 = r9_1;
                        local_sp_7 = local_sp_6;
                        r8_5 = r8_4;
                        local_sp_8 = local_sp_6;
                        local_sp_22 = local_sp_6;
                        r8_15 = r8_4;
                        if (var_118 != var_119) {
                            var_120 = *(uint64_t *)6376968UL;
                            var_121 = *(uint64_t *)6376528UL;
                            var_122 = var_119 + (-1L);
                            var_123 = *(uint64_t *)6376960UL;
                            rcx_1 = var_120;
                            rax_1 = var_123;
                            rcx_3 = var_120;
                            if (var_120 != var_121) {
                                if (var_123 == 0UL) {
                                    if (var_120 != 0UL) {
                                        if (var_120 <= 576460752303423487UL) {
                                            loop_state_var = 0U;
                                            break;
                                        }
                                        rcx_2 = rcx_1;
                                        rsi2_5 = rcx_1 << 4UL;
                                    }
                                } else {
                                    if (var_120 <= 384307168202282324UL) {
                                        loop_state_var = 0U;
                                        break;
                                    }
                                    var_124 = (var_120 + (var_120 >> 1UL)) + 1UL;
                                    rcx_1 = var_124;
                                    rcx_2 = rcx_1;
                                    rsi2_5 = rcx_1 << 4UL;
                                }
                                *(uint64_t *)6376528UL = rcx_2;
                                var_125 = local_sp_6 + (-8L);
                                *(uint64_t *)var_125 = 4207172UL;
                                var_126 = indirect_placeholder_8(var_123, rsi2_5);
                                var_127 = var_126.field_0;
                                var_128 = var_126.field_1;
                                var_129 = var_126.field_2;
                                var_130 = *(uint64_t *)6376968UL;
                                *(uint64_t *)6376960UL = var_127;
                                rax_1 = var_127;
                                r9_2 = var_128;
                                rcx_3 = var_130;
                                local_sp_7 = var_125;
                                r8_5 = var_129;
                            }
                            var_131 = rcx_3 + 1UL;
                            var_132 = rax_1 + (rcx_3 << 4UL);
                            *(uint64_t *)var_132 = var_118;
                            *(uint64_t *)(var_132 + 8UL) = var_122;
                            *(uint64_t *)6376968UL = var_131;
                            r9_3 = r9_2;
                            local_sp_8 = local_sp_7;
                            r8_6 = r8_5;
                        }
                        var_133 = r15_0 + 1UL;
                        local_sp_9 = local_sp_8;
                        r15_0 = var_133;
                        r9_4 = r9_3;
                        r9_1 = r9_3;
                        local_sp_6 = local_sp_8;
                        r8_4 = r8_6;
                        r8_7 = r8_6;
                        if (var_133 == rdx_4) {
                            r13_0 = r13_0 + 16UL;
                            continue;
                        }
                        r14_0 = *(uint64_t *)(local_sp_8 + 8UL);
                        loop_state_var = 1U;
                        break;
                    }
                switch (loop_state_var) {
                  case 0U:
                    {
                        *(uint64_t *)(local_sp_22 + (-8L)) = 4207556UL;
                        indirect_placeholder_1(r9_12, r8_15);
                        abort();
                    }
                    break;
                  case 1U:
                    {
                        break;
                    }
                    break;
                }
            }
            var_134 = *(uint64_t *)(((rdx_4 << 4UL) + r14_0) + (-8L));
            local_sp_11 = local_sp_9;
            r9_12 = r9_4;
            local_sp_10 = local_sp_9;
            local_sp_22 = local_sp_9;
            r8_15 = r8_7;
            if (var_134 != 18446744073709551615UL) {
                var_135 = var_134 + 1UL;
                var_136 = *(uint64_t *)6376968UL;
                var_137 = (var_136 == *(uint64_t *)6376528UL);
                var_138 = *(uint64_t *)6376960UL;
                rcx_4 = var_138;
                rdx_6 = var_136;
                rax_2 = var_136;
                if (!var_137) {
                    if (var_138 == 0UL) {
                        if (var_136 != 0UL) {
                            if (var_136 <= 576460752303423487UL) {
                                *(uint64_t *)(local_sp_22 + (-8L)) = 4207556UL;
                                indirect_placeholder_1(r9_12, r8_15);
                                abort();
                            }
                            rax_3 = rax_2;
                            rsi2_6 = rax_2 << 4UL;
                        }
                    } else {
                        if (var_136 > 384307168202282324UL) {
                            *(uint64_t *)(local_sp_22 + (-8L)) = 4207556UL;
                            indirect_placeholder_1(r9_12, r8_15);
                            abort();
                        }
                        var_139 = (var_136 + (var_136 >> 1UL)) + 1UL;
                        rax_2 = var_139;
                        rax_3 = rax_2;
                        rsi2_6 = rax_2 << 4UL;
                    }
                    *(uint64_t *)6376528UL = rax_3;
                    var_140 = local_sp_9 + (-8L);
                    *(uint64_t *)var_140 = 4207421UL;
                    var_141 = indirect_placeholder_9(var_138, rsi2_6);
                    var_142 = var_141.field_0;
                    var_143 = *(uint64_t *)6376968UL;
                    *(uint64_t *)6376960UL = var_142;
                    rcx_4 = var_142;
                    local_sp_10 = var_140;
                    rdx_6 = var_143;
                }
                var_144 = (rdx_6 << 4UL) + rcx_4;
                *(uint64_t *)var_144 = var_135;
                *(uint64_t *)(var_144 + 8UL) = 18446744073709551615UL;
                *(uint64_t *)6376968UL = (rdx_6 + 1UL);
                local_sp_11 = local_sp_10;
            }
            var_145 = local_sp_11 + (-8L);
            *(uint64_t *)var_145 = 4207284UL;
            indirect_placeholder();
            rdi1_1 = *(uint64_t *)6376960UL;
            local_sp_4 = var_145;
            rsi2_4 = *(uint64_t *)6376968UL;
        }
        break;
      case 3U:
      case 2U:
        {
            switch (loop_state_var) {
              case 3U:
                {
                    var_157 = helper_cc_compute_all_wrapper((uint64_t)(*(unsigned char *)(local_sp_25 + 28UL) & '\x04'), cc_src_1, var_8, 22U);
                    var_158 = ((var_157 & 64UL) == 0UL) ? storemerge : storemerge10;
                    local_sp_27 = local_sp_25;
                    rdx_19 = var_158;
                }
                break;
              case 2U:
                {
                    *(uint64_t *)(local_sp_27 + (-8L)) = 4207517UL;
                    indirect_placeholder_2(0UL, 0UL, r9_14, rcx_10, rdx_19, 0UL, r8_17);
                    *(uint64_t *)(local_sp_27 + (-16L)) = 4207527UL;
                    indirect_placeholder_3(rbx_2, 1UL, rbp_1);
                    abort();
                }
                break;
            }
        }
        break;
    }
}

typedef unsigned char __u_char;
typedef unsigned short __u_short;
typedef unsigned int __u_int;
typedef unsigned long __u_long;
typedef signed char __int8_t;
typedef unsigned char __uint8_t;
typedef short __int16_t;
typedef unsigned short __uint16_t;
typedef int __int32_t;
typedef unsigned int __uint32_t;
typedef long __int64_t;
typedef unsigned long __uint64_t;
typedef __int8_t __int_least8_t;
typedef __uint8_t __uint_least8_t;
typedef __int16_t __int_least16_t;
typedef __uint16_t __uint_least16_t;
typedef __int32_t __int_least32_t;
typedef __uint32_t __uint_least32_t;
typedef __int64_t __int_least64_t;
typedef __uint64_t __uint_least64_t;
typedef long __quad_t;
typedef unsigned long __u_quad_t;
typedef long __intmax_t;
typedef unsigned long __uintmax_t;
typedef unsigned long __dev_t;
typedef unsigned int __uid_t;
typedef unsigned int __gid_t;
typedef unsigned long __ino_t;
typedef unsigned long __ino64_t;
typedef unsigned int __mode_t;
typedef unsigned long __nlink_t;
typedef long __off_t;
typedef long __off64_t;
typedef int __pid_t;
typedef struct {
    int __val[2];
} __fsid_t;
typedef long __clock_t;
typedef unsigned long __rlim_t;
typedef unsigned long __rlim64_t;
typedef unsigned int __id_t;
typedef long __time_t;
typedef unsigned int __useconds_t;
typedef long __suseconds_t;
typedef int __daddr_t;
typedef int __key_t;
typedef int __clockid_t;
typedef void *__timer_t;
typedef long __blksize_t;
typedef long __blkcnt_t;
typedef long __blkcnt64_t;
typedef unsigned long __fsblkcnt_t;
typedef unsigned long __fsblkcnt64_t;
typedef unsigned long __fsfilcnt_t;
typedef unsigned long __fsfilcnt64_t;
typedef long __fsword_t;
typedef long __ssize_t;
typedef long __syscall_slong_t;
typedef unsigned long __syscall_ulong_t;
typedef __off64_t __loff_t;
typedef char *__caddr_t;
typedef long __intptr_t;
typedef unsigned int __socklen_t;
typedef int __sig_atomic_t;
typedef __int8_t int8_t;
typedef __int16_t int16_t;
typedef __int32_t int32_t;
typedef __int64_t int64_t;
typedef __uint8_t uint8_t;
typedef __uint16_t uint16_t;
typedef __uint32_t uint32_t;
typedef __uint64_t uint64_t;
typedef __int_least8_t int_least8_t;
typedef __int_least16_t int_least16_t;
typedef __int_least32_t int_least32_t;
typedef __int_least64_t int_least64_t;
typedef __uint_least8_t uint_least8_t;
typedef __uint_least16_t uint_least16_t;
typedef __uint_least32_t uint_least32_t;
typedef __uint_least64_t uint_least64_t;
typedef signed char int_fast8_t;
typedef long int_fast16_t;
typedef long int_fast32_t;
typedef long int_fast64_t;
typedef unsigned char uint_fast8_t;
typedef unsigned long uint_fast16_t;
typedef unsigned long uint_fast32_t;
typedef unsigned long uint_fast64_t;
typedef long intptr_t;
typedef unsigned long uintptr_t;
typedef __intmax_t intmax_t;
typedef __uintmax_t uintmax_t;
extern uint64_t revng_init_local_sp(uint64_t param_0);
extern uint64_t init_rbx(void);
extern uint64_t init_rbp(void);
extern void indirect_placeholder(void);
extern uint64_t init_r13(void);
extern uint64_t init_r12(void);
extern uint64_t init_r14(void);
extern uint64_t init_rax(void);
uint64_t bb_memchr2(uint64_t rdi, uint64_t rcx, uint64_t rdx, uint64_t rsi) {
    uint64_t var_0;
    uint64_t var_1;
    uint64_t var_2;
    uint64_t var_3;
    uint64_t var_4;
    uint64_t var_5;
    uint64_t var_6;
    unsigned char var_7;
    unsigned char var_8;
    uint64_t var_9;
    uint64_t rdi1_1;
    unsigned char var_10;
    uint64_t rdi1_5;
    uint64_t rdi1_2;
    uint64_t rdi1_0;
    uint64_t rcx2_0;
    uint64_t var_11;
    uint64_t var_12;
    uint64_t rcx2_1;
    unsigned char var_13;
    uint64_t var_14;
    uint64_t var_15;
    uint64_t var_16;
    uint64_t var_17;
    uint64_t var_18;
    uint64_t var_19;
    uint64_t var_20;
    uint64_t var_21;
    uint64_t var_22;
    uint64_t var_23;
    uint64_t var_24;
    uint64_t rdi1_3;
    uint64_t var_25;
    uint64_t var_26;
    uint64_t var_27;
    uint64_t rdi1_4;
    uint64_t rcx2_2;
    uint64_t var_28;
    uint64_t var_29;
    uint64_t var_30;
    uint64_t var_31;
    uint64_t var_32;
    uint64_t rcx2_3;
    uint64_t rcx2_4;
    unsigned char var_33;
    uint64_t var_34;
    uint64_t rax_0_in;
    uint64_t rax_0;
    unsigned char var_35;
    unsigned int loop_state_var;
    var_0 = revng_init_local_sp(0UL);
    var_1 = init_rax();
    var_2 = init_r13();
    var_3 = init_rbx();
    var_4 = init_r12();
    var_5 = init_rbp();
    var_6 = init_r14();
    var_7 = (unsigned char)rsi;
    var_8 = (unsigned char)rdx;
    var_9 = (uint64_t)(var_7 - var_8);
    *(uint64_t *)(var_0 + (-8L)) = var_6;
    *(uint64_t *)(var_0 + (-16L)) = var_2;
    *(uint64_t *)(var_0 + (-24L)) = var_4;
    *(uint64_t *)(var_0 + (-32L)) = var_5;
    *(uint64_t *)(var_0 + (-40L)) = var_3;
    rdi1_1 = rdi;
    rdi1_5 = rdi;
    rdi1_0 = rdi;
    rcx2_0 = rcx;
    rcx2_1 = rcx;
    if (var_9 == 0UL) {
        indirect_placeholder();
        return var_1;
    }
    if (!((rcx == 0UL) || ((rdi & 7UL) == 0UL))) {
        var_10 = *(unsigned char *)rdi;
        if ((uint64_t)(var_10 - var_7) == 0UL) {
            return rdi1_5;
        }
        if ((uint64_t)(var_10 - var_8) == 0UL) {
            return rdi1_5;
        }
        while (1U)
            {
                var_11 = rdi1_0 + 1UL;
                var_12 = rcx2_0 + (-1L);
                rdi1_1 = var_11;
                rdi1_5 = var_11;
                rdi1_0 = var_11;
                rcx2_0 = var_12;
                rcx2_1 = var_12;
                if (!((var_12 == 0UL) || ((var_11 & 7UL) == 0UL))) {
                    loop_state_var = 0U;
                    break;
                }
                var_13 = *(unsigned char *)var_11;
                if ((uint64_t)(var_7 - var_13) != 0UL) {
                    loop_state_var = 1U;
                    break;
                }
                if ((uint64_t)(var_8 - var_13) == 0UL) {
                    continue;
                }
                loop_state_var = 1U;
                break;
            }
        switch (loop_state_var) {
          case 1U:
            {
                return rdi1_5;
            }
            break;
          case 0U:
            {
                break;
            }
            break;
        }
    }
    var_14 = (uint64_t)var_7;
    var_15 = (uint64_t)var_8;
    var_16 = (var_14 << 40UL) | (var_14 << 32UL);
    var_17 = var_16 >> 32UL;
    var_18 = (var_15 << 40UL) | (var_15 << 32UL);
    var_19 = var_18 >> 32UL;
    var_20 = var_18 >> 16UL;
    var_21 = (var_16 >> 16UL) | var_17;
    var_22 = var_20 | var_19;
    var_23 = var_21 | (var_21 << 32UL);
    var_24 = var_22 | (var_22 << 32UL);
    rdi1_5 = 0UL;
    rdi1_2 = rdi1_1;
    rdi1_3 = rdi1_1;
    rdi1_4 = rdi1_1;
    rcx2_2 = rcx2_1;
    rcx2_3 = rcx2_1;
    rcx2_4 = rcx2_1;
    if (rcx2_1 > 7UL) {
        var_25 = *(uint64_t *)rdi1_1;
        var_26 = var_25 ^ var_24;
        var_27 = var_25 ^ var_23;
        if (((((var_26 + (-72340172838076673L)) & (var_26 ^ (-9187201950435737472L))) | ((var_27 + (-72340172838076673L)) & (var_27 ^ (-9187201950435737472L)))) & (-9187201950435737472L)) != 0UL) {
            while (1U)
                {
                    var_28 = rcx2_2 + (-8L);
                    var_29 = rdi1_2 + 8UL;
                    rdi1_2 = var_29;
                    rdi1_3 = var_29;
                    rdi1_4 = var_29;
                    rcx2_2 = var_28;
                    rcx2_3 = var_28;
                    rcx2_4 = var_28;
                    if (var_28 <= 7UL) {
                        loop_state_var = 1U;
                        break;
                    }
                    var_30 = *(uint64_t *)var_29;
                    var_31 = var_23 ^ var_30;
                    var_32 = var_30 ^ var_24;
                    if (((((var_32 + (-72340172838076673L)) & (var_32 ^ (-9187201950435737472L))) | ((var_31 + (-72340172838076673L)) & (var_31 ^ (-9187201950435737472L)))) & (-9187201950435737472L)) == 0UL) {
                        continue;
                    }
                    loop_state_var = 0U;
                    break;
                }
            rdi1_4 = rdi1_3;
            rcx2_4 = rcx2_3;
            if (rcx2_3 == 0UL) {
                return rdi1_5;
            }
        }
    }
    rdi1_4 = rdi1_3;
    rcx2_4 = rcx2_3;
    if (rcx2_3 == 0UL) {
        return rdi1_5;
    }
    var_33 = *(unsigned char *)rdi1_4;
    rax_0_in = rdi1_4;
    rdi1_5 = rdi1_4;
    if (~((uint64_t)(var_33 - var_7) != 0UL & (uint64_t)(var_33 - var_8) != 0UL)) {
        return rdi1_5;
    }
    var_34 = rcx2_4 + rdi1_4;
    rdi1_5 = 0UL;
    rax_0 = rax_0_in + 1UL;
    rax_0_in = rax_0;
    while (rax_0 != var_34)
        {
            var_35 = *(unsigned char *)rax_0;
            rdi1_5 = rax_0;
            if ((uint64_t)(var_7 - var_35) == 0UL) {
                break;
            }
            if ((uint64_t)(var_8 - var_35) == 0UL) {
                break;
            }
            rax_0 = rax_0_in + 1UL;
            rax_0_in = rax_0;
        }
    return rdi1_5;
}

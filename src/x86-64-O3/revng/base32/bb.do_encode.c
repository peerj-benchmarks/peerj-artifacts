typedef unsigned char __u_char;
typedef unsigned short __u_short;
typedef unsigned int __u_int;
typedef unsigned long __u_long;
typedef signed char __int8_t;
typedef unsigned char __uint8_t;
typedef short __int16_t;
typedef unsigned short __uint16_t;
typedef int __int32_t;
typedef unsigned int __uint32_t;
typedef long __int64_t;
typedef unsigned long __uint64_t;
typedef __int8_t __int_least8_t;
typedef __uint8_t __uint_least8_t;
typedef __int16_t __int_least16_t;
typedef __uint16_t __uint_least16_t;
typedef __int32_t __int_least32_t;
typedef __uint32_t __uint_least32_t;
typedef __int64_t __int_least64_t;
typedef __uint64_t __uint_least64_t;
typedef long __quad_t;
typedef unsigned long __u_quad_t;
typedef long __intmax_t;
typedef unsigned long __uintmax_t;
typedef unsigned long __dev_t;
typedef unsigned int __uid_t;
typedef unsigned int __gid_t;
typedef unsigned long __ino_t;
typedef unsigned long __ino64_t;
typedef unsigned int __mode_t;
typedef unsigned long __nlink_t;
typedef long __off_t;
typedef long __off64_t;
typedef int __pid_t;
typedef struct {
    int __val[2];
} __fsid_t;
typedef long __clock_t;
typedef unsigned long __rlim_t;
typedef unsigned long __rlim64_t;
typedef unsigned int __id_t;
typedef long __time_t;
typedef unsigned int __useconds_t;
typedef long __suseconds_t;
typedef int __daddr_t;
typedef int __key_t;
typedef int __clockid_t;
typedef void *__timer_t;
typedef long __blksize_t;
typedef long __blkcnt_t;
typedef long __blkcnt64_t;
typedef unsigned long __fsblkcnt_t;
typedef unsigned long __fsblkcnt64_t;
typedef unsigned long __fsfilcnt_t;
typedef unsigned long __fsfilcnt64_t;
typedef long __fsword_t;
typedef long __ssize_t;
typedef long __syscall_slong_t;
typedef unsigned long __syscall_ulong_t;
typedef __off64_t __loff_t;
typedef char *__caddr_t;
typedef long __intptr_t;
typedef unsigned int __socklen_t;
typedef int __sig_atomic_t;
typedef __int8_t int8_t;
typedef __int16_t int16_t;
typedef __int32_t int32_t;
typedef __int64_t int64_t;
typedef __uint8_t uint8_t;
typedef __uint16_t uint16_t;
typedef __uint32_t uint32_t;
typedef __uint64_t uint64_t;
typedef __int_least8_t int_least8_t;
typedef __int_least16_t int_least16_t;
typedef __int_least32_t int_least32_t;
typedef __int_least64_t int_least64_t;
typedef __uint_least8_t uint_least8_t;
typedef __uint_least16_t uint_least16_t;
typedef __uint_least32_t uint_least32_t;
typedef __uint_least64_t uint_least64_t;
typedef signed char int_fast8_t;
typedef long int_fast16_t;
typedef long int_fast32_t;
typedef long int_fast64_t;
typedef unsigned char uint_fast8_t;
typedef unsigned long uint_fast16_t;
typedef unsigned long uint_fast32_t;
typedef unsigned long uint_fast64_t;
typedef long intptr_t;
typedef unsigned long uintptr_t;
typedef __intmax_t intmax_t;
typedef __uintmax_t uintmax_t;
typedef _Bool bool;
extern void abort(void);
extern uint64_t revng_init_local_sp(uint64_t param_0);
extern uint64_t init_rbx(void);
extern uint64_t init_rbp(void);
extern void indirect_placeholder(void);
extern uint64_t init_r13(void);
extern uint64_t init_r12(void);
extern uint64_t init_r14(void);
extern uint64_t init_r15(void);
extern void indirect_placeholder_4(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint64_t param_3, uint64_t param_4, uint64_t param_5, uint64_t param_6);
extern uint64_t init_r9(void);
extern uint64_t init_r8(void);
extern void indirect_placeholder_14(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint64_t param_3, uint64_t param_4);
void bb_do_encode(uint64_t rdi, uint64_t rdx, uint64_t rsi) {
    uint64_t var_0;
    uint64_t var_1;
    uint64_t var_2;
    uint64_t var_3;
    uint64_t var_4;
    uint64_t var_5;
    uint64_t var_6;
    uint64_t var_7;
    uint64_t var_8;
    uint64_t var_9;
    bool var_10;
    uint64_t r12_3;
    uint64_t rcx_6;
    uint64_t var_44;
    uint64_t var_45;
    uint64_t r8_5;
    uint32_t *rax_0;
    uint64_t var_39;
    uint64_t var_40;
    uint64_t rax_5;
    uint64_t rcx_2;
    uint64_t var_38;
    uint64_t var_32;
    uint64_t local_sp_3;
    uint64_t var_33;
    uint64_t local_sp_1;
    uint64_t r12_1_ph;
    uint32_t *phitmp;
    uint64_t local_sp_0;
    uint64_t rcx_0;
    uint64_t r8_0;
    uint64_t var_46;
    uint64_t var_47;
    uint32_t *rax_1;
    uint64_t r12_1;
    uint64_t r8_6;
    uint64_t rcx_4;
    uint64_t r14_0_ph;
    uint64_t rcx_1;
    uint64_t r8_1;
    uint64_t var_48;
    uint64_t var_49;
    uint64_t local_sp_7;
    uint64_t rax_2;
    uint64_t local_sp_10;
    uint64_t r12_0;
    uint64_t local_sp_2;
    uint64_t r8_2;
    uint64_t var_37;
    uint64_t local_sp_3_ph;
    uint64_t rcx_3_ph;
    uint64_t var_22;
    bool var_23;
    bool var_24;
    uint64_t var_25;
    uint64_t var_26;
    uint64_t var_27;
    uint64_t var_28;
    uint64_t var_29;
    uint64_t *var_30;
    uint64_t var_31;
    uint64_t var_34;
    uint64_t var_35;
    uint64_t var_36;
    uint64_t rax_6;
    uint64_t rax_3;
    uint64_t local_sp_4;
    uint64_t r8_3;
    uint64_t var_50;
    uint64_t rax_4;
    uint64_t r12_2;
    uint64_t local_sp_5;
    uint64_t rcx_5;
    uint64_t r8_4;
    uint64_t local_sp_6;
    uint64_t var_41;
    uint64_t var_42;
    uint64_t rcx_7;
    uint64_t var_43;
    uint64_t var_15;
    uint64_t local_sp_9;
    uint64_t rbx_0;
    uint64_t var_14;
    uint64_t var_16;
    unsigned __int128 var_17;
    uint64_t var_18;
    uint64_t var_19;
    uint64_t var_20;
    uint64_t var_21;
    uint64_t r8_7;
    uint64_t local_sp_11;
    uint64_t var_11;
    uint64_t var_12;
    uint64_t var_13;
    unsigned int loop_state_var;
    bool switch_state_var;
    var_0 = revng_init_local_sp(0UL);
    var_1 = init_r15();
    var_2 = init_r13();
    var_3 = init_rbx();
    var_4 = init_r12();
    var_5 = init_r9();
    var_6 = init_rbp();
    var_7 = init_r8();
    var_8 = init_r14();
    *(uint64_t *)(var_0 + (-8L)) = var_1;
    *(uint64_t *)(var_0 + (-16L)) = var_8;
    *(uint64_t *)(var_0 + (-24L)) = var_2;
    *(uint64_t *)(var_0 + (-32L)) = var_4;
    *(uint64_t *)(var_0 + (-40L)) = var_6;
    *(uint64_t *)(var_0 + (-48L)) = var_3;
    var_9 = var_0 + (-79944L);
    *(uint64_t *)(var_0 + (-79936L)) = rsi;
    var_10 = (rdx == 0UL);
    r12_3 = 0UL;
    rax_0 = (uint32_t *)0UL;
    rax_5 = 0UL;
    rcx_2 = 0UL;
    rax_1 = (uint32_t *)0UL;
    r14_0_ph = 0UL;
    r8_1 = 0UL;
    local_sp_10 = var_9;
    rax_3 = 0UL;
    rcx_5 = rdi;
    rbx_0 = 0UL;
    r8_7 = var_7;
    while (1U)
        {
            r8_5 = r8_7;
            r12_1_ph = r12_3;
            r12_0 = r12_3;
            r8_2 = r8_7;
            r12_2 = r12_3;
            r8_4 = r8_7;
            local_sp_11 = local_sp_10;
            while (1U)
                {
                    var_11 = local_sp_11 + 16UL;
                    *(uint64_t *)(local_sp_11 + (-8L)) = 4202843UL;
                    indirect_placeholder();
                    var_12 = rbx_0 + var_11;
                    var_13 = local_sp_11 + (-16L);
                    *(uint64_t *)var_13 = 4202854UL;
                    indirect_placeholder();
                    rax_4 = var_11;
                    local_sp_9 = var_13;
                    rbx_0 = var_12;
                    if ((uint64_t)(uint32_t)var_11 != 0UL) {
                        loop_state_var = 0U;
                        break;
                    }
                    var_15 = local_sp_11 + (-24L);
                    *(uint64_t *)var_15 = 4202800UL;
                    indirect_placeholder();
                    local_sp_9 = var_15;
                    local_sp_11 = var_15;
                    if (var_12 > 30719UL) {
                        continue;
                    }
                    loop_state_var = 1U;
                    break;
                }
            switch_state_var = 0;
            switch (loop_state_var) {
              case 1U:
                {
                    var_16 = local_sp_9 + 16UL;
                    var_17 = (unsigned __int128)(var_12 + 4UL) * 14757395258967641293ULL;
                    var_18 = (uint64_t)var_17;
                    var_19 = (uint64_t)((var_17 >> 66ULL) << 3ULL);
                    var_20 = local_sp_9 + 30736UL;
                    var_21 = local_sp_9 + (-8L);
                    *(uint64_t *)var_21 = 4202989UL;
                    indirect_placeholder_14(var_18, var_16, var_19, var_20, var_12);
                    rax_2 = var_18;
                    local_sp_2 = var_21;
                    local_sp_3_ph = var_21;
                    rcx_3_ph = var_19;
                    if (!var_10) {
                        var_35 = *(uint64_t *)6374528UL;
                        var_36 = local_sp_9 + (-16L);
                        *(uint64_t *)var_36 = 4203202UL;
                        indirect_placeholder();
                        local_sp_2 = var_36;
                        rcx_2 = var_35;
                        rcx_6 = var_35;
                        if (var_19 > var_18) {
                            var_37 = local_sp_2 + (-8L);
                            *(uint64_t *)var_37 = 4203132UL;
                            indirect_placeholder();
                            r12_3 = r12_0;
                            rax_4 = rax_2;
                            r12_2 = r12_0;
                            local_sp_5 = var_37;
                            rcx_5 = rcx_2;
                            r8_4 = r8_2;
                            r8_7 = r8_2;
                            if ((uint64_t)(uint32_t)rax_2 != 0UL) {
                                loop_state_var = 2U;
                                switch_state_var = 1;
                                break;
                            }
                            var_38 = local_sp_2 + (-16L);
                            *(uint64_t *)var_38 = 4203148UL;
                            indirect_placeholder();
                            local_sp_5 = var_38;
                            local_sp_10 = var_38;
                            if (var_12 == 30720UL) {
                                continue;
                            }
                            loop_state_var = 2U;
                            switch_state_var = 1;
                            break;
                        }
                        *(uint64_t *)(local_sp_9 + (-24L)) = 4203212UL;
                        indirect_placeholder();
                        var_39 = (uint64_t)*(uint32_t *)var_18;
                        var_40 = local_sp_9 + (-32L);
                        *(uint64_t *)var_40 = 4203231UL;
                        indirect_placeholder_4(0UL, 1UL, var_5, var_35, 4263243UL, r8_7, var_39);
                        local_sp_6 = var_40;
                        loop_state_var = 1U;
                        switch_state_var = 1;
                        break;
                    }
                    if (var_19 != 0UL) {
                        var_37 = local_sp_2 + (-8L);
                        *(uint64_t *)var_37 = 4203132UL;
                        indirect_placeholder();
                        r12_3 = r12_0;
                        rax_4 = rax_2;
                        r12_2 = r12_0;
                        local_sp_5 = var_37;
                        rcx_5 = rcx_2;
                        r8_4 = r8_2;
                        r8_7 = r8_2;
                        if ((uint64_t)(uint32_t)rax_2 != 0UL) {
                            loop_state_var = 2U;
                            switch_state_var = 1;
                            break;
                        }
                        var_38 = local_sp_2 + (-16L);
                        *(uint64_t *)var_38 = 4203148UL;
                        indirect_placeholder();
                        local_sp_5 = var_38;
                        local_sp_10 = var_38;
                        if (var_12 == 30720UL) {
                            continue;
                        }
                        loop_state_var = 2U;
                        switch_state_var = 1;
                        break;
                    }
                    while (1U)
                        {
                            var_22 = var_19 - r14_0_ph;
                            var_23 = ((uint64_t)((uint32_t)var_22 + 1U) == 0UL);
                            var_24 = (r14_0_ph < var_19);
                            rcx_2 = rcx_3_ph;
                            local_sp_3 = local_sp_3_ph;
                            r12_1 = r12_1_ph;
                            rcx_1 = rcx_3_ph;
                            rax_2 = var_22;
                            r12_0 = 0UL;
                            r8_2 = 0UL;
                            while (1U)
                                {
                                    var_25 = rdx - r12_1;
                                    var_26 = (var_22 > var_25) ? var_25 : var_22;
                                    r12_1 = 0UL;
                                    if (var_26 != 0UL) {
                                        loop_state_var = 0U;
                                        break;
                                    }
                                    var_34 = local_sp_3 + (-8L);
                                    *(uint64_t *)var_34 = 4203023UL;
                                    indirect_placeholder();
                                    local_sp_1 = var_34;
                                    local_sp_2 = var_34;
                                    local_sp_3 = var_34;
                                    if (var_23) {
                                        if (var_24) {
                                            continue;
                                        }
                                        loop_state_var = 1U;
                                        break;
                                    }
                                    rax_1 = (uint32_t *)var_22;
                                    loop_state_var = 2U;
                                    break;
                                }
                            switch_state_var = 0;
                            switch (loop_state_var) {
                              case 0U:
                                {
                                    var_27 = local_sp_3 + 30736UL;
                                    var_28 = *(uint64_t *)6374528UL;
                                    *(uint64_t *)local_sp_3 = var_26;
                                    var_29 = local_sp_3 + (-8L);
                                    var_30 = (uint64_t *)var_29;
                                    *var_30 = 4203100UL;
                                    indirect_placeholder();
                                    var_31 = *var_30;
                                    rcx_2 = var_28;
                                    local_sp_0 = var_29;
                                    rcx_0 = var_28;
                                    r8_0 = var_31;
                                    rax_2 = var_27;
                                    local_sp_2 = var_29;
                                    r8_2 = var_31;
                                    local_sp_3_ph = var_29;
                                    rcx_3_ph = var_28;
                                    if (var_31 > var_27) {
                                        var_32 = r14_0_ph + var_31;
                                        var_33 = r12_1 + var_31;
                                        r12_0 = var_33;
                                        r12_1_ph = var_33;
                                        r14_0_ph = var_32;
                                        if (var_32 < var_19) {
                                            continue;
                                        }
                                        loop_state_var = 1U;
                                        switch_state_var = 1;
                                        break;
                                    }
                                    phitmp = (uint32_t *)var_27;
                                    rax_0 = phitmp;
                                    loop_state_var = 0U;
                                    switch_state_var = 1;
                                    break;
                                }
                                break;
                              case 1U:
                                {
                                    loop_state_var = 1U;
                                    switch_state_var = 1;
                                    break;
                                }
                                break;
                              case 2U:
                                {
                                    loop_state_var = 2U;
                                    switch_state_var = 1;
                                    break;
                                }
                                break;
                            }
                            if (switch_state_var)
                                break;
                        }
                    switch_state_var = 0;
                    switch (loop_state_var) {
                      case 1U:
                        {
                            var_37 = local_sp_2 + (-8L);
                            *(uint64_t *)var_37 = 4203132UL;
                            indirect_placeholder();
                            r12_3 = r12_0;
                            rax_4 = rax_2;
                            r12_2 = r12_0;
                            local_sp_5 = var_37;
                            rcx_5 = rcx_2;
                            r8_4 = r8_2;
                            r8_7 = r8_2;
                            if ((uint64_t)(uint32_t)rax_2 != 0UL) {
                                loop_state_var = 2U;
                                switch_state_var = 1;
                                break;
                            }
                            var_38 = local_sp_2 + (-16L);
                            *(uint64_t *)var_38 = 4203148UL;
                            indirect_placeholder();
                            local_sp_5 = var_38;
                            local_sp_10 = var_38;
                            if (var_12 == 30720UL) {
                                continue;
                            }
                            loop_state_var = 2U;
                            switch_state_var = 1;
                            break;
                        }
                        break;
                      case 0U:
                        {
                            loop_state_var = 0U;
                            switch_state_var = 1;
                            break;
                        }
                        break;
                      case 2U:
                        {
                            loop_state_var = 3U;
                            switch_state_var = 1;
                            break;
                        }
                        break;
                    }
                    if (switch_state_var)
                        break;
                }
                break;
              case 0U:
                {
                    if (var_12 != 0UL) {
                        var_14 = var_13 + (-8L);
                        *(uint64_t *)var_14 = 4202871UL;
                        indirect_placeholder();
                        local_sp_5 = var_14;
                        if (!0) {
                            loop_state_var = 2U;
                            switch_state_var = 1;
                            break;
                        }
                        *(uint64_t *)(var_13 + (-16L)) = 4202883UL;
                        indirect_placeholder();
                        rax_4 = (uint64_t)0L;
                        r12_2 = (uint64_t)0L;
                        local_sp_5 = (uint64_t)0L;
                        rcx_5 = (uint64_t)0L;
                        r8_4 = (uint64_t)0L;
                        loop_state_var = 2U;
                        switch_state_var = 1;
                        break;
                    }
                }
                break;
            }
            if (switch_state_var)
                break;
        }
    switch (loop_state_var) {
      case 1U:
        {
            var_41 = local_sp_6 + (-8L);
            *(uint64_t *)var_41 = 4203246UL;
            indirect_placeholder();
            var_42 = (uint64_t)((uint32_t)rax_5 + 1U);
            rcx_0 = rcx_6;
            r8_0 = r8_5;
            r8_6 = r8_5;
            local_sp_7 = var_41;
            rax_6 = var_42;
            rcx_7 = rcx_6;
            if (var_42 != 0UL) {
                var_43 = local_sp_7 + (-8L);
                *(uint64_t *)var_43 = 4202905UL;
                indirect_placeholder();
                rax_3 = rax_6;
                local_sp_4 = var_43;
                rcx_4 = rcx_7;
                r8_3 = r8_6;
                if ((uint64_t)(uint32_t)rax_6 != 0UL) {
                    return;
                }
                *(uint64_t *)(local_sp_4 + (-8L)) = 4203332UL;
                indirect_placeholder();
                var_50 = (uint64_t)*(uint32_t *)rax_3;
                *(uint64_t *)(local_sp_4 + (-16L)) = 4203351UL;
                indirect_placeholder_4(0UL, 1UL, var_5, rcx_4, 4263232UL, r8_3, var_50);
                abort();
            }
            *(uint64_t *)(local_sp_6 + (-16L)) = 4203260UL;
            indirect_placeholder();
            var_44 = (uint64_t)*(volatile uint32_t *)(uint32_t *)0UL;
            var_45 = local_sp_6 + (-24L);
            *(uint64_t *)var_45 = 4203279UL;
            indirect_placeholder_4(0UL, 1UL, var_5, rcx_6, 4263243UL, r8_5, var_44);
            local_sp_0 = var_45;
            *(uint64_t *)(local_sp_0 + (-8L)) = 4203284UL;
            indirect_placeholder();
            var_46 = (uint64_t)*rax_0;
            var_47 = local_sp_0 + (-16L);
            *(uint64_t *)var_47 = 4203303UL;
            indirect_placeholder_4(0UL, 1UL, var_5, rcx_0, 4263243UL, r8_0, var_46);
            local_sp_1 = var_47;
            rcx_1 = rcx_0;
            r8_1 = r8_0;
        }
        break;
      case 2U:
        {
            rcx_6 = rcx_5;
            r8_5 = r8_4;
            rax_5 = rax_4;
            r8_6 = r8_4;
            local_sp_7 = local_sp_5;
            rax_6 = rax_4;
            local_sp_6 = local_sp_5;
            rcx_7 = rcx_5;
            if (var_10 || (r12_2 == 0UL)) {
                var_43 = local_sp_7 + (-8L);
                *(uint64_t *)var_43 = 4202905UL;
                indirect_placeholder();
                rax_3 = rax_6;
                local_sp_4 = var_43;
                rcx_4 = rcx_7;
                r8_3 = r8_6;
                if ((uint64_t)(uint32_t)rax_6 != 0UL) {
                    return;
                }
                *(uint64_t *)(local_sp_4 + (-8L)) = 4203332UL;
                indirect_placeholder();
                var_50 = (uint64_t)*(uint32_t *)rax_3;
                *(uint64_t *)(local_sp_4 + (-16L)) = 4203351UL;
                indirect_placeholder_4(0UL, 1UL, var_5, rcx_4, 4263232UL, r8_3, var_50);
                abort();
            }
            var_41 = local_sp_6 + (-8L);
            *(uint64_t *)var_41 = 4203246UL;
            indirect_placeholder();
            var_42 = (uint64_t)((uint32_t)rax_5 + 1U);
            rcx_0 = rcx_6;
            r8_0 = r8_5;
            r8_6 = r8_5;
            local_sp_7 = var_41;
            rax_6 = var_42;
            rcx_7 = rcx_6;
            if (var_42 == 0UL) {
                var_43 = local_sp_7 + (-8L);
                *(uint64_t *)var_43 = 4202905UL;
                indirect_placeholder();
                rax_3 = rax_6;
                local_sp_4 = var_43;
                rcx_4 = rcx_7;
                r8_3 = r8_6;
                if ((uint64_t)(uint32_t)rax_6 == 0UL) {
                    return;
                }
                *(uint64_t *)(local_sp_4 + (-8L)) = 4203332UL;
                indirect_placeholder();
                var_50 = (uint64_t)*(uint32_t *)rax_3;
                *(uint64_t *)(local_sp_4 + (-16L)) = 4203351UL;
                indirect_placeholder_4(0UL, 1UL, var_5, rcx_4, 4263232UL, r8_3, var_50);
                abort();
            }
            *(uint64_t *)(local_sp_6 + (-16L)) = 4203260UL;
            indirect_placeholder();
            var_44 = (uint64_t)*(volatile uint32_t *)(uint32_t *)0UL;
            var_45 = local_sp_6 + (-24L);
            *(uint64_t *)var_45 = 4203279UL;
            indirect_placeholder_4(0UL, 1UL, var_5, rcx_6, 4263243UL, r8_5, var_44);
            local_sp_0 = var_45;
            *(uint64_t *)(local_sp_0 + (-8L)) = 4203284UL;
            indirect_placeholder();
            var_46 = (uint64_t)*rax_0;
            var_47 = local_sp_0 + (-16L);
            *(uint64_t *)var_47 = 4203303UL;
            indirect_placeholder_4(0UL, 1UL, var_5, rcx_0, 4263243UL, r8_0, var_46);
            local_sp_1 = var_47;
            rcx_1 = rcx_0;
            r8_1 = r8_0;
        }
        break;
      case 3U:
      case 0U:
        {
            *(uint64_t *)(local_sp_0 + (-8L)) = 4203284UL;
            indirect_placeholder();
            var_46 = (uint64_t)*rax_0;
            var_47 = local_sp_0 + (-16L);
            *(uint64_t *)var_47 = 4203303UL;
            indirect_placeholder_4(0UL, 1UL, var_5, rcx_0, 4263243UL, r8_0, var_46);
            local_sp_1 = var_47;
            rcx_1 = rcx_0;
            r8_1 = r8_0;
        }
        break;
    }
    *(uint64_t *)(local_sp_1 + (-8L)) = 4203308UL;
    indirect_placeholder();
    var_48 = (uint64_t)*rax_1;
    var_49 = local_sp_1 + (-16L);
    *(uint64_t *)var_49 = 4203327UL;
    indirect_placeholder_4(0UL, 1UL, var_5, rcx_1, 4263243UL, r8_1, var_48);
    local_sp_4 = var_49;
    rcx_4 = rcx_1;
    r8_3 = r8_1;
    *(uint64_t *)(local_sp_4 + (-8L)) = 4203332UL;
    indirect_placeholder();
    var_50 = (uint64_t)*(uint32_t *)rax_3;
    *(uint64_t *)(local_sp_4 + (-16L)) = 4203351UL;
    indirect_placeholder_4(0UL, 1UL, var_5, rcx_4, 4263232UL, r8_3, var_50);
    abort();
}

typedef unsigned char __u_char;
typedef unsigned short __u_short;
typedef unsigned int __u_int;
typedef unsigned long __u_long;
typedef signed char __int8_t;
typedef unsigned char __uint8_t;
typedef short __int16_t;
typedef unsigned short __uint16_t;
typedef int __int32_t;
typedef unsigned int __uint32_t;
typedef long __int64_t;
typedef unsigned long __uint64_t;
typedef __int8_t __int_least8_t;
typedef __uint8_t __uint_least8_t;
typedef __int16_t __int_least16_t;
typedef __uint16_t __uint_least16_t;
typedef __int32_t __int_least32_t;
typedef __uint32_t __uint_least32_t;
typedef __int64_t __int_least64_t;
typedef __uint64_t __uint_least64_t;
typedef long __quad_t;
typedef unsigned long __u_quad_t;
typedef long __intmax_t;
typedef unsigned long __uintmax_t;
typedef unsigned long __dev_t;
typedef unsigned int __uid_t;
typedef unsigned int __gid_t;
typedef unsigned long __ino_t;
typedef unsigned long __ino64_t;
typedef unsigned int __mode_t;
typedef unsigned long __nlink_t;
typedef long __off_t;
typedef long __off64_t;
typedef int __pid_t;
typedef struct {
    int __val[2];
} __fsid_t;
typedef long __clock_t;
typedef unsigned long __rlim_t;
typedef unsigned long __rlim64_t;
typedef unsigned int __id_t;
typedef long __time_t;
typedef unsigned int __useconds_t;
typedef long __suseconds_t;
typedef int __daddr_t;
typedef int __key_t;
typedef int __clockid_t;
typedef void *__timer_t;
typedef long __blksize_t;
typedef long __blkcnt_t;
typedef long __blkcnt64_t;
typedef unsigned long __fsblkcnt_t;
typedef unsigned long __fsblkcnt64_t;
typedef unsigned long __fsfilcnt_t;
typedef unsigned long __fsfilcnt64_t;
typedef long __fsword_t;
typedef long __ssize_t;
typedef long __syscall_slong_t;
typedef unsigned long __syscall_ulong_t;
typedef __off64_t __loff_t;
typedef char *__caddr_t;
typedef long __intptr_t;
typedef unsigned int __socklen_t;
typedef int __sig_atomic_t;
typedef __int8_t int8_t;
typedef __int16_t int16_t;
typedef __int32_t int32_t;
typedef __int64_t int64_t;
typedef __uint8_t uint8_t;
typedef __uint16_t uint16_t;
typedef __uint32_t uint32_t;
typedef __uint64_t uint64_t;
typedef __int_least8_t int_least8_t;
typedef __int_least16_t int_least16_t;
typedef __int_least32_t int_least32_t;
typedef __int_least64_t int_least64_t;
typedef __uint_least8_t uint_least8_t;
typedef __uint_least16_t uint_least16_t;
typedef __uint_least32_t uint_least32_t;
typedef __uint_least64_t uint_least64_t;
typedef signed char int_fast8_t;
typedef long int_fast16_t;
typedef long int_fast32_t;
typedef long int_fast64_t;
typedef unsigned char uint_fast8_t;
typedef unsigned long uint_fast16_t;
typedef unsigned long uint_fast32_t;
typedef unsigned long uint_fast64_t;
typedef long intptr_t;
typedef unsigned long uintptr_t;
typedef __intmax_t intmax_t;
typedef __uintmax_t uintmax_t;
typedef _Bool bool;
extern void abort(void);
extern uint64_t revng_init_local_sp(uint64_t param_0);
extern uint64_t init_rbx(void);
extern uint64_t init_rbp(void);
extern void indirect_placeholder(void);
extern uint64_t init_cc_src2(void);
extern uint64_t helper_cc_compute_c_wrapper(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint32_t param_3);
void bb_version_etc_va(uint64_t rdi, uint64_t rcx, uint64_t rdx, uint64_t rsi, uint64_t r8) {
    uint64_t var_0;
    uint64_t var_1;
    uint64_t var_2;
    uint64_t var_3;
    uint64_t var_4;
    uint32_t *var_5;
    uint32_t var_6;
    uint64_t var_7;
    uint64_t rbp_0;
    uint64_t *_pre_phi149;
    uint64_t local_sp_0;
    uint64_t var_62;
    uint64_t var_52;
    uint64_t var_61;
    uint64_t *var_37;
    uint64_t var_38;
    uint64_t var_39;
    uint64_t var_40;
    uint64_t var_41;
    uint64_t var_8;
    uint32_t var_9;
    uint64_t var_10;
    uint64_t var_11;
    uint32_t var_12;
    uint64_t var_13;
    uint64_t var_14;
    uint64_t *var_33;
    uint64_t var_34;
    uint64_t *_pre_phi151;
    uint32_t var_15;
    uint64_t var_16;
    uint64_t var_17;
    uint64_t *var_31;
    uint64_t var_32;
    uint64_t *_pre_phi153;
    uint32_t var_18;
    uint64_t var_19;
    uint64_t var_20;
    uint64_t *var_29;
    uint64_t var_30;
    uint64_t *_pre_phi155;
    uint32_t var_21;
    uint64_t var_22;
    uint64_t var_23;
    uint64_t *var_27;
    uint64_t var_28;
    uint64_t *_pre_phi157;
    uint64_t var_24;
    uint64_t *var_25;
    uint64_t var_26;
    uint64_t *_pre_phi159;
    uint64_t *var_35;
    uint64_t var_36;
    uint64_t rax_0;
    uint64_t var_42;
    uint64_t rax_1;
    uint64_t var_43;
    uint64_t var_44;
    uint64_t rax_2;
    uint64_t var_45;
    uint64_t var_46;
    uint64_t rax_3;
    uint64_t var_47;
    uint64_t var_48;
    uint64_t rax_4;
    uint64_t var_49;
    uint64_t var_50;
    uint64_t rax_5;
    uint64_t var_51;
    uint64_t var_53;
    uint64_t var_54;
    uint64_t var_55;
    uint64_t var_56;
    uint64_t var_57;
    bool var_58;
    uint64_t var_59;
    uint64_t *var_60;
    var_0 = revng_init_local_sp(0UL);
    var_1 = init_rbx();
    var_2 = init_rbp();
    var_3 = init_cc_src2();
    *(uint64_t *)(var_0 + (-8L)) = var_2;
    *(uint64_t *)(var_0 + (-16L)) = var_1;
    var_4 = var_0 + (-104L);
    var_5 = (uint32_t *)r8;
    var_6 = *var_5;
    var_7 = (uint64_t)var_6;
    rbp_0 = 0UL;
    if (var_6 > 47U) {
        var_37 = (uint64_t *)(r8 + 8UL);
        var_38 = *var_37;
        var_39 = var_38 + 8UL;
        *var_37 = var_39;
        var_40 = *(uint64_t *)var_38;
        *(uint64_t *)var_4 = var_40;
        _pre_phi149 = var_37;
        rax_0 = var_39;
        var_41 = rax_0 + 8UL;
        *_pre_phi149 = var_41;
        var_42 = *(uint64_t *)rax_0;
        *(uint64_t *)(var_0 + (-96L)) = var_42;
        _pre_phi151 = _pre_phi149;
        rax_1 = var_41;
        rbp_0 = 1UL;
        var_43 = rax_1 + 8UL;
        *_pre_phi151 = var_43;
        var_44 = *(uint64_t *)rax_1;
        *(uint64_t *)(var_0 + (-88L)) = var_44;
        _pre_phi153 = _pre_phi151;
        rax_2 = var_43;
        rbp_0 = 2UL;
        var_45 = rax_2 + 8UL;
        *_pre_phi153 = var_45;
        var_46 = *(uint64_t *)rax_2;
        *(uint64_t *)(var_0 + (-80L)) = var_46;
        _pre_phi155 = _pre_phi153;
        rax_3 = var_45;
        rbp_0 = 3UL;
        var_47 = rax_3 + 8UL;
        *_pre_phi155 = var_47;
        var_48 = *(uint64_t *)rax_3;
        *(uint64_t *)(var_0 + (-72L)) = var_48;
        _pre_phi157 = _pre_phi155;
        rax_4 = var_47;
        rbp_0 = 4UL;
        var_49 = rax_4 + 8UL;
        *_pre_phi157 = var_49;
        var_50 = *(uint64_t *)rax_4;
        *(uint64_t *)(var_0 + (-64L)) = var_50;
        _pre_phi159 = _pre_phi157;
        rax_5 = var_49;
        rbp_0 = 5UL;
        var_51 = rax_5 + 8UL;
        *_pre_phi159 = var_51;
        var_52 = *(uint64_t *)rax_5;
        *(uint64_t *)(var_0 + (-56L)) = var_52;
        rbp_0 = 6UL;
        var_53 = rax_5 + 16UL;
        *_pre_phi159 = var_53;
        var_54 = *(uint64_t *)var_51;
        *(uint64_t *)(var_0 + (-48L)) = var_54;
        rbp_0 = 7UL;
        var_55 = rax_5 + 24UL;
        *_pre_phi159 = var_55;
        var_56 = *(uint64_t *)var_53;
        *(uint64_t *)(var_0 + (-40L)) = var_56;
        rbp_0 = 8UL;
        if (var_40 != 0UL & var_42 != 0UL & var_44 != 0UL & var_46 != 0UL & var_48 != 0UL & var_50 != 0UL & var_52 != 0UL & var_54 != 0UL & var_56 == 0UL) {
            *_pre_phi159 = (rax_5 + 32UL);
            var_57 = helper_cc_compute_c_wrapper(*(uint64_t *)var_55 + (-1L), 1UL, var_3, 17U);
            rbp_0 = 10UL - var_57;
        }
    } else {
        var_8 = *(uint64_t *)(r8 + 16UL);
        var_9 = var_6 + 8U;
        var_10 = (uint64_t)var_9;
        *var_5 = var_9;
        var_11 = *(uint64_t *)(var_8 + var_7);
        *(uint64_t *)var_4 = var_11;
        if (var_11 != 0UL) {
            rbp_0 = 1UL;
            if ((uint64_t)(var_9 & (-16)) > 47UL) {
                var_35 = (uint64_t *)(r8 + 8UL);
                var_36 = *var_35;
                _pre_phi149 = var_35;
                rax_0 = var_36;
                var_41 = rax_0 + 8UL;
                *_pre_phi149 = var_41;
                var_42 = *(uint64_t *)rax_0;
                *(uint64_t *)(var_0 + (-96L)) = var_42;
                _pre_phi151 = _pre_phi149;
                rax_1 = var_41;
                rbp_0 = 1UL;
                var_43 = rax_1 + 8UL;
                *_pre_phi151 = var_43;
                var_44 = *(uint64_t *)rax_1;
                *(uint64_t *)(var_0 + (-88L)) = var_44;
                _pre_phi153 = _pre_phi151;
                rax_2 = var_43;
                rbp_0 = 2UL;
                var_45 = rax_2 + 8UL;
                *_pre_phi153 = var_45;
                var_46 = *(uint64_t *)rax_2;
                *(uint64_t *)(var_0 + (-80L)) = var_46;
                _pre_phi155 = _pre_phi153;
                rax_3 = var_45;
                rbp_0 = 3UL;
                var_47 = rax_3 + 8UL;
                *_pre_phi155 = var_47;
                var_48 = *(uint64_t *)rax_3;
                *(uint64_t *)(var_0 + (-72L)) = var_48;
                _pre_phi157 = _pre_phi155;
                rax_4 = var_47;
                rbp_0 = 4UL;
                var_49 = rax_4 + 8UL;
                *_pre_phi157 = var_49;
                var_50 = *(uint64_t *)rax_4;
                *(uint64_t *)(var_0 + (-64L)) = var_50;
                _pre_phi159 = _pre_phi157;
                rax_5 = var_49;
                rbp_0 = 5UL;
                var_51 = rax_5 + 8UL;
                *_pre_phi159 = var_51;
                var_52 = *(uint64_t *)rax_5;
                *(uint64_t *)(var_0 + (-56L)) = var_52;
                rbp_0 = 6UL;
                var_53 = rax_5 + 16UL;
                *_pre_phi159 = var_53;
                var_54 = *(uint64_t *)var_51;
                *(uint64_t *)(var_0 + (-48L)) = var_54;
                rbp_0 = 7UL;
                var_55 = rax_5 + 24UL;
                *_pre_phi159 = var_55;
                var_56 = *(uint64_t *)var_53;
                *(uint64_t *)(var_0 + (-40L)) = var_56;
                rbp_0 = 8UL;
                if (var_42 != 0UL & var_44 != 0UL & var_46 != 0UL & var_48 != 0UL & var_50 != 0UL & var_52 != 0UL & var_54 != 0UL & var_56 == 0UL) {
                    *_pre_phi159 = (rax_5 + 32UL);
                    var_57 = helper_cc_compute_c_wrapper(*(uint64_t *)var_55 + (-1L), 1UL, var_3, 17U);
                    rbp_0 = 10UL - var_57;
                }
            } else {
                var_12 = var_6 + 16U;
                var_13 = (uint64_t)var_12;
                *var_5 = var_12;
                var_14 = *(uint64_t *)(var_10 + var_8);
                *(uint64_t *)(var_0 + (-96L)) = var_14;
                if (var_14 != 0UL) {
                    rbp_0 = 2UL;
                    if ((uint64_t)(var_12 & (-16)) > 47UL) {
                        var_33 = (uint64_t *)(r8 + 8UL);
                        var_34 = *var_33;
                        _pre_phi151 = var_33;
                        rax_1 = var_34;
                        var_43 = rax_1 + 8UL;
                        *_pre_phi151 = var_43;
                        var_44 = *(uint64_t *)rax_1;
                        *(uint64_t *)(var_0 + (-88L)) = var_44;
                        _pre_phi153 = _pre_phi151;
                        rax_2 = var_43;
                        rbp_0 = 2UL;
                        var_45 = rax_2 + 8UL;
                        *_pre_phi153 = var_45;
                        var_46 = *(uint64_t *)rax_2;
                        *(uint64_t *)(var_0 + (-80L)) = var_46;
                        _pre_phi155 = _pre_phi153;
                        rax_3 = var_45;
                        rbp_0 = 3UL;
                        var_47 = rax_3 + 8UL;
                        *_pre_phi155 = var_47;
                        var_48 = *(uint64_t *)rax_3;
                        *(uint64_t *)(var_0 + (-72L)) = var_48;
                        _pre_phi157 = _pre_phi155;
                        rax_4 = var_47;
                        rbp_0 = 4UL;
                        var_49 = rax_4 + 8UL;
                        *_pre_phi157 = var_49;
                        var_50 = *(uint64_t *)rax_4;
                        *(uint64_t *)(var_0 + (-64L)) = var_50;
                        _pre_phi159 = _pre_phi157;
                        rax_5 = var_49;
                        rbp_0 = 5UL;
                        var_51 = rax_5 + 8UL;
                        *_pre_phi159 = var_51;
                        var_52 = *(uint64_t *)rax_5;
                        *(uint64_t *)(var_0 + (-56L)) = var_52;
                        rbp_0 = 6UL;
                        var_53 = rax_5 + 16UL;
                        *_pre_phi159 = var_53;
                        var_54 = *(uint64_t *)var_51;
                        *(uint64_t *)(var_0 + (-48L)) = var_54;
                        rbp_0 = 7UL;
                        var_55 = rax_5 + 24UL;
                        *_pre_phi159 = var_55;
                        var_56 = *(uint64_t *)var_53;
                        *(uint64_t *)(var_0 + (-40L)) = var_56;
                        rbp_0 = 8UL;
                        if (var_44 != 0UL & var_46 != 0UL & var_48 != 0UL & var_50 != 0UL & var_52 != 0UL & var_54 != 0UL & var_56 == 0UL) {
                            *_pre_phi159 = (rax_5 + 32UL);
                            var_57 = helper_cc_compute_c_wrapper(*(uint64_t *)var_55 + (-1L), 1UL, var_3, 17U);
                            rbp_0 = 10UL - var_57;
                        }
                    } else {
                        var_15 = var_6 + 24U;
                        var_16 = (uint64_t)var_15;
                        *var_5 = var_15;
                        var_17 = *(uint64_t *)(var_13 + var_8);
                        *(uint64_t *)(var_0 + (-88L)) = var_17;
                        if (var_17 != 0UL) {
                            rbp_0 = 3UL;
                            if ((uint64_t)(var_15 & (-16)) > 47UL) {
                                var_31 = (uint64_t *)(r8 + 8UL);
                                var_32 = *var_31;
                                _pre_phi153 = var_31;
                                rax_2 = var_32;
                                var_45 = rax_2 + 8UL;
                                *_pre_phi153 = var_45;
                                var_46 = *(uint64_t *)rax_2;
                                *(uint64_t *)(var_0 + (-80L)) = var_46;
                                _pre_phi155 = _pre_phi153;
                                rax_3 = var_45;
                                rbp_0 = 3UL;
                                var_47 = rax_3 + 8UL;
                                *_pre_phi155 = var_47;
                                var_48 = *(uint64_t *)rax_3;
                                *(uint64_t *)(var_0 + (-72L)) = var_48;
                                _pre_phi157 = _pre_phi155;
                                rax_4 = var_47;
                                rbp_0 = 4UL;
                                var_49 = rax_4 + 8UL;
                                *_pre_phi157 = var_49;
                                var_50 = *(uint64_t *)rax_4;
                                *(uint64_t *)(var_0 + (-64L)) = var_50;
                                _pre_phi159 = _pre_phi157;
                                rax_5 = var_49;
                                rbp_0 = 5UL;
                                var_51 = rax_5 + 8UL;
                                *_pre_phi159 = var_51;
                                var_52 = *(uint64_t *)rax_5;
                                *(uint64_t *)(var_0 + (-56L)) = var_52;
                                rbp_0 = 6UL;
                                var_53 = rax_5 + 16UL;
                                *_pre_phi159 = var_53;
                                var_54 = *(uint64_t *)var_51;
                                *(uint64_t *)(var_0 + (-48L)) = var_54;
                                rbp_0 = 7UL;
                                var_55 = rax_5 + 24UL;
                                *_pre_phi159 = var_55;
                                var_56 = *(uint64_t *)var_53;
                                *(uint64_t *)(var_0 + (-40L)) = var_56;
                                rbp_0 = 8UL;
                                if (var_46 != 0UL & var_48 != 0UL & var_50 != 0UL & var_52 != 0UL & var_54 != 0UL & var_56 == 0UL) {
                                    *_pre_phi159 = (rax_5 + 32UL);
                                    var_57 = helper_cc_compute_c_wrapper(*(uint64_t *)var_55 + (-1L), 1UL, var_3, 17U);
                                    rbp_0 = 10UL - var_57;
                                }
                            } else {
                                var_18 = var_6 + 32U;
                                var_19 = (uint64_t)var_18;
                                *var_5 = var_18;
                                var_20 = *(uint64_t *)(var_16 + var_8);
                                *(uint64_t *)(var_0 + (-80L)) = var_20;
                                if (var_20 != 0UL) {
                                    rbp_0 = 4UL;
                                    if ((uint64_t)(var_18 & (-16)) > 47UL) {
                                        var_29 = (uint64_t *)(r8 + 8UL);
                                        var_30 = *var_29;
                                        _pre_phi155 = var_29;
                                        rax_3 = var_30;
                                        var_47 = rax_3 + 8UL;
                                        *_pre_phi155 = var_47;
                                        var_48 = *(uint64_t *)rax_3;
                                        *(uint64_t *)(var_0 + (-72L)) = var_48;
                                        _pre_phi157 = _pre_phi155;
                                        rax_4 = var_47;
                                        rbp_0 = 4UL;
                                        var_49 = rax_4 + 8UL;
                                        *_pre_phi157 = var_49;
                                        var_50 = *(uint64_t *)rax_4;
                                        *(uint64_t *)(var_0 + (-64L)) = var_50;
                                        _pre_phi159 = _pre_phi157;
                                        rax_5 = var_49;
                                        rbp_0 = 5UL;
                                        var_51 = rax_5 + 8UL;
                                        *_pre_phi159 = var_51;
                                        var_52 = *(uint64_t *)rax_5;
                                        *(uint64_t *)(var_0 + (-56L)) = var_52;
                                        rbp_0 = 6UL;
                                        var_53 = rax_5 + 16UL;
                                        *_pre_phi159 = var_53;
                                        var_54 = *(uint64_t *)var_51;
                                        *(uint64_t *)(var_0 + (-48L)) = var_54;
                                        rbp_0 = 7UL;
                                        var_55 = rax_5 + 24UL;
                                        *_pre_phi159 = var_55;
                                        var_56 = *(uint64_t *)var_53;
                                        *(uint64_t *)(var_0 + (-40L)) = var_56;
                                        rbp_0 = 8UL;
                                        if (var_48 != 0UL & var_50 != 0UL & var_52 != 0UL & var_54 != 0UL & var_56 == 0UL) {
                                            *_pre_phi159 = (rax_5 + 32UL);
                                            var_57 = helper_cc_compute_c_wrapper(*(uint64_t *)var_55 + (-1L), 1UL, var_3, 17U);
                                            rbp_0 = 10UL - var_57;
                                        }
                                    } else {
                                        var_21 = var_6 + 40U;
                                        var_22 = (uint64_t)var_21;
                                        *var_5 = var_21;
                                        var_23 = *(uint64_t *)(var_19 + var_8);
                                        *(uint64_t *)(var_0 + (-72L)) = var_23;
                                        if (var_23 != 0UL) {
                                            rbp_0 = 5UL;
                                            if ((uint64_t)(var_21 & (-16)) > 47UL) {
                                                var_27 = (uint64_t *)(r8 + 8UL);
                                                var_28 = *var_27;
                                                _pre_phi157 = var_27;
                                                rax_4 = var_28;
                                                var_49 = rax_4 + 8UL;
                                                *_pre_phi157 = var_49;
                                                var_50 = *(uint64_t *)rax_4;
                                                *(uint64_t *)(var_0 + (-64L)) = var_50;
                                                _pre_phi159 = _pre_phi157;
                                                rax_5 = var_49;
                                                rbp_0 = 5UL;
                                                var_51 = rax_5 + 8UL;
                                                *_pre_phi159 = var_51;
                                                var_52 = *(uint64_t *)rax_5;
                                                *(uint64_t *)(var_0 + (-56L)) = var_52;
                                                rbp_0 = 6UL;
                                                var_53 = rax_5 + 16UL;
                                                *_pre_phi159 = var_53;
                                                var_54 = *(uint64_t *)var_51;
                                                *(uint64_t *)(var_0 + (-48L)) = var_54;
                                                rbp_0 = 7UL;
                                                var_55 = rax_5 + 24UL;
                                                *_pre_phi159 = var_55;
                                                var_56 = *(uint64_t *)var_53;
                                                *(uint64_t *)(var_0 + (-40L)) = var_56;
                                                rbp_0 = 8UL;
                                                if (var_50 != 0UL & var_52 != 0UL & var_54 != 0UL & var_56 == 0UL) {
                                                    *_pre_phi159 = (rax_5 + 32UL);
                                                    var_57 = helper_cc_compute_c_wrapper(*(uint64_t *)var_55 + (-1L), 1UL, var_3, 17U);
                                                    rbp_0 = 10UL - var_57;
                                                }
                                            } else {
                                                *var_5 = (var_6 + 48U);
                                                var_24 = *(uint64_t *)(var_22 + var_8);
                                                *(uint64_t *)(var_0 + (-64L)) = var_24;
                                                if (var_24 != 0UL) {
                                                    var_25 = (uint64_t *)(r8 + 8UL);
                                                    var_26 = *var_25;
                                                    _pre_phi159 = var_25;
                                                    rax_5 = var_26;
                                                    var_51 = rax_5 + 8UL;
                                                    *_pre_phi159 = var_51;
                                                    var_52 = *(uint64_t *)rax_5;
                                                    *(uint64_t *)(var_0 + (-56L)) = var_52;
                                                    rbp_0 = 6UL;
                                                    var_53 = rax_5 + 16UL;
                                                    *_pre_phi159 = var_53;
                                                    var_54 = *(uint64_t *)var_51;
                                                    *(uint64_t *)(var_0 + (-48L)) = var_54;
                                                    rbp_0 = 7UL;
                                                    var_55 = rax_5 + 24UL;
                                                    *_pre_phi159 = var_55;
                                                    var_56 = *(uint64_t *)var_53;
                                                    *(uint64_t *)(var_0 + (-40L)) = var_56;
                                                    rbp_0 = 8UL;
                                                    if (var_52 != 0UL & var_54 != 0UL & var_56 == 0UL) {
                                                        *_pre_phi159 = (rax_5 + 32UL);
                                                        var_57 = helper_cc_compute_c_wrapper(*(uint64_t *)var_55 + (-1L), 1UL, var_3, 17U);
                                                        rbp_0 = 10UL - var_57;
                                                    }
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }
    }
    var_58 = (rsi == 0UL);
    var_59 = var_0 + (-112L);
    var_60 = (uint64_t *)var_59;
    if (var_58) {
        *var_60 = 4216815UL;
        indirect_placeholder();
    } else {
        *var_60 = 4215753UL;
        indirect_placeholder();
    }
    *(uint64_t *)(var_59 + (-8L)) = 4215778UL;
    indirect_placeholder();
    var_61 = var_59 + (-16L);
    *(uint64_t *)var_61 = 4215791UL;
    indirect_placeholder();
    local_sp_0 = var_61;
    if (rbp_0 > 9UL) {
        *(uint64_t *)(var_59 + (-32L)) = *(uint64_t *)(var_59 + 48UL);
        *(uint64_t *)(var_59 + (-40L)) = *(uint64_t *)(var_59 + 40UL);
        *(uint64_t *)(var_59 + (-48L)) = *(uint64_t *)(var_59 + 32UL);
        *(uint64_t *)(var_59 + (-56L)) = *(uint64_t *)(var_59 + 24UL);
        *(uint64_t *)(var_59 + (-64L)) = *(uint64_t *)(var_59 + 16UL);
        *(uint64_t *)(var_59 + (-72L)) = 4216883UL;
        indirect_placeholder();
        return;
    }
    switch (*(uint64_t *)((rbp_0 << 3UL) + 4266904UL)) {
      case 4216568UL:
        {
            indirect_placeholder();
            return;
        }
        break;
      case 4216608UL:
        {
            indirect_placeholder();
            return;
        }
        break;
      case 4216648UL:
        {
            indirect_placeholder();
            return;
        }
        break;
      case 4216688UL:
        {
            indirect_placeholder();
            return;
        }
        break;
      case 4216728UL:
      case 4216720UL:
      case 4216512UL:
      case 4216464UL:
      case 4216400UL:
      case 4216328UL:
        {
            switch (*(uint64_t *)((rbp_0 << 3UL) + 4266904UL)) {
              case 4216464UL:
                {
                    *(uint64_t *)(var_59 + (-24L)) = *(uint64_t *)(var_59 + 24UL);
                    *(uint64_t *)(var_59 + (-32L)) = *(uint64_t *)(var_59 + 16UL);
                    *(uint64_t *)(var_59 + (-40L)) = 4216507UL;
                    indirect_placeholder();
                }
                break;
              case 4216400UL:
                {
                    *(uint64_t *)(var_59 + (-32L)) = *(uint64_t *)(var_59 + 32UL);
                    *(uint64_t *)(var_59 + (-40L)) = *(uint64_t *)(var_59 + 24UL);
                    *(uint64_t *)(var_59 + (-48L)) = *(uint64_t *)(var_59 + 16UL);
                    *(uint64_t *)(var_59 + (-56L)) = 4216451UL;
                    indirect_placeholder();
                }
                break;
              case 4216512UL:
                {
                    *(uint64_t *)(var_59 + (-32L)) = *(uint64_t *)(var_59 + 16UL);
                    *(uint64_t *)(var_59 + (-40L)) = 4216555UL;
                    indirect_placeholder();
                }
                break;
              case 4216328UL:
                {
                    *(uint64_t *)(var_59 + (-24L)) = *(uint64_t *)(var_59 + 40UL);
                    *(uint64_t *)(var_59 + (-32L)) = *(uint64_t *)(var_59 + 32UL);
                    *(uint64_t *)(var_59 + (-40L)) = *(uint64_t *)(var_59 + 24UL);
                    *(uint64_t *)(var_59 + (-48L)) = *(uint64_t *)(var_59 + 16UL);
                    *(uint64_t *)(var_59 + (-56L)) = 4216379UL;
                    indirect_placeholder();
                }
                break;
              case 4216728UL:
              case 4216720UL:
                {
                    switch (*(uint64_t *)((rbp_0 << 3UL) + 4266904UL)) {
                      case 4216728UL:
                        {
                            *(uint64_t *)(local_sp_0 + (-16L)) = *(uint64_t *)(local_sp_0 + 64UL);
                            *(uint64_t *)(local_sp_0 + (-24L)) = *(uint64_t *)(local_sp_0 + 56UL);
                            *(uint64_t *)(local_sp_0 + (-32L)) = *(uint64_t *)(local_sp_0 + 48UL);
                            *(uint64_t *)(local_sp_0 + (-40L)) = *(uint64_t *)(local_sp_0 + 40UL);
                            *(uint64_t *)(local_sp_0 + (-48L)) = *(uint64_t *)(local_sp_0 + 32UL);
                            *(uint64_t *)(local_sp_0 + (-56L)) = 4216787UL;
                            indirect_placeholder();
                        }
                        break;
                      case 4216720UL:
                        {
                            var_62 = var_59 + (-24L);
                            *(uint64_t *)var_62 = 4216725UL;
                            indirect_placeholder();
                            local_sp_0 = var_62;
                        }
                        break;
                    }
                }
                break;
            }
            return;
        }
        break;
      default:
        {
            abort();
        }
        break;
    }
}

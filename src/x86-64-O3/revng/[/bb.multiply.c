typedef unsigned char __u_char;
typedef unsigned short __u_short;
typedef unsigned int __u_int;
typedef unsigned long __u_long;
typedef signed char __int8_t;
typedef unsigned char __uint8_t;
typedef short __int16_t;
typedef unsigned short __uint16_t;
typedef int __int32_t;
typedef unsigned int __uint32_t;
typedef long __int64_t;
typedef unsigned long __uint64_t;
typedef __int8_t __int_least8_t;
typedef __uint8_t __uint_least8_t;
typedef __int16_t __int_least16_t;
typedef __uint16_t __uint_least16_t;
typedef __int32_t __int_least32_t;
typedef __uint32_t __uint_least32_t;
typedef __int64_t __int_least64_t;
typedef __uint64_t __uint_least64_t;
typedef long __quad_t;
typedef unsigned long __u_quad_t;
typedef long __intmax_t;
typedef unsigned long __uintmax_t;
typedef unsigned long __dev_t;
typedef unsigned int __uid_t;
typedef unsigned int __gid_t;
typedef unsigned long __ino_t;
typedef unsigned long __ino64_t;
typedef unsigned int __mode_t;
typedef unsigned long __nlink_t;
typedef long __off_t;
typedef long __off64_t;
typedef int __pid_t;
typedef struct {
    int __val[2];
} __fsid_t;
typedef long __clock_t;
typedef unsigned long __rlim_t;
typedef unsigned long __rlim64_t;
typedef unsigned int __id_t;
typedef long __time_t;
typedef unsigned int __useconds_t;
typedef long __suseconds_t;
typedef int __daddr_t;
typedef int __key_t;
typedef int __clockid_t;
typedef void *__timer_t;
typedef long __blksize_t;
typedef long __blkcnt_t;
typedef long __blkcnt64_t;
typedef unsigned long __fsblkcnt_t;
typedef unsigned long __fsblkcnt64_t;
typedef unsigned long __fsfilcnt_t;
typedef unsigned long __fsfilcnt64_t;
typedef long __fsword_t;
typedef long __ssize_t;
typedef long __syscall_slong_t;
typedef unsigned long __syscall_ulong_t;
typedef __off64_t __loff_t;
typedef char *__caddr_t;
typedef long __intptr_t;
typedef unsigned int __socklen_t;
typedef int __sig_atomic_t;
typedef __int8_t int8_t;
typedef __int16_t int16_t;
typedef __int32_t int32_t;
typedef __int64_t int64_t;
typedef __uint8_t uint8_t;
typedef __uint16_t uint16_t;
typedef __uint32_t uint32_t;
typedef __uint64_t uint64_t;
typedef __int_least8_t int_least8_t;
typedef __int_least16_t int_least16_t;
typedef __int_least32_t int_least32_t;
typedef __int_least64_t int_least64_t;
typedef __uint_least8_t uint_least8_t;
typedef __uint_least16_t uint_least16_t;
typedef __uint_least32_t uint_least32_t;
typedef __uint_least64_t uint_least64_t;
typedef signed char int_fast8_t;
typedef long int_fast16_t;
typedef long int_fast32_t;
typedef long int_fast64_t;
typedef unsigned char uint_fast8_t;
typedef unsigned long uint_fast16_t;
typedef unsigned long uint_fast32_t;
typedef unsigned long uint_fast64_t;
typedef long intptr_t;
typedef unsigned long uintptr_t;
typedef __intmax_t intmax_t;
typedef __uintmax_t uintmax_t;
typedef _Bool bool;
struct helper_pxor_xmm_wrapper_62_ret_type;
struct type_3;
struct type_5;
struct indirect_placeholder_58_ret_type;
struct indirect_placeholder_59_ret_type;
struct helper_pxor_xmm_wrapper_62_ret_type {
    uint64_t field_0;
    uint64_t field_1;
};
struct type_3 {
};
struct type_5 {
};
struct indirect_placeholder_58_ret_type {
    uint64_t field_0;
    uint64_t field_1;
};
struct indirect_placeholder_59_ret_type {
    uint64_t field_0;
    uint64_t field_1;
};
extern uint64_t revng_init_local_sp(uint64_t param_0);
extern uint64_t init_rbx(void);
extern uint64_t init_rbp(void);
extern uint64_t init_state_0x8558(void);
extern uint64_t init_state_0x8560(void);
extern uint64_t init_cc_src2(void);
extern uint64_t helper_cc_compute_c_wrapper(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint32_t param_3);
extern uint64_t init_r13(void);
extern uint64_t init_r12(void);
extern uint64_t init_r14(void);
extern uint64_t init_r15(void);
extern struct helper_pxor_xmm_wrapper_62_ret_type helper_pxor_xmm_wrapper_62(struct type_3 *param_0, struct type_5 *param_1, struct type_5 *param_2, uint64_t param_3, uint64_t param_4);
extern struct indirect_placeholder_58_ret_type indirect_placeholder_58(uint64_t param_0);
extern struct indirect_placeholder_59_ret_type indirect_placeholder_59(uint64_t param_0);
uint64_t bb_multiply(uint64_t rdi, uint64_t rcx, uint64_t rdx, uint64_t rsi, uint64_t r8) {
    struct helper_pxor_xmm_wrapper_62_ret_type var_35;
    uint64_t var_0;
    uint64_t var_1;
    uint64_t var_2;
    uint64_t var_3;
    uint64_t var_4;
    uint64_t var_5;
    uint64_t var_6;
    uint64_t var_7;
    uint64_t var_8;
    uint64_t var_9;
    bool var_10;
    uint64_t var_11;
    uint64_t var_12;
    uint64_t var_13;
    uint64_t var_14;
    uint64_t var_19;
    bool var_20;
    uint64_t _pre_phi;
    uint64_t _pre;
    uint64_t var_21;
    uint64_t var_22;
    uint64_t var_23;
    uint64_t rdx3_0;
    uint64_t var_30;
    uint64_t rcx2_1;
    uint64_t var_24;
    uint64_t rcx2_0;
    uint64_t var_25;
    uint64_t var_26;
    uint64_t var_27;
    uint64_t var_28;
    uint64_t var_29;
    uint64_t rdx3_1;
    uint64_t r11_0;
    uint64_t var_31;
    uint64_t var_32;
    uint64_t var_33;
    uint64_t var_34;
    uint64_t rcx2_2;
    uint64_t var_36;
    uint64_t var_37;
    uint64_t rdx3_2;
    uint64_t rsi4_0;
    uint64_t var_38;
    uint64_t var_39;
    uint64_t var_40;
    uint64_t var_41;
    uint64_t var_42;
    uint64_t var_43;
    uint64_t rcx2_3_be;
    uint64_t r13_0;
    uint64_t rcx2_3;
    uint64_t var_44;
    uint64_t r12_1;
    uint64_t r10_0;
    uint64_t rdx3_3;
    uint64_t var_45;
    uint64_t var_46;
    uint32_t *var_47;
    uint64_t var_48;
    uint64_t var_49;
    uint64_t var_50;
    uint64_t var_51;
    uint64_t var_52;
    uint64_t r12_0;
    uint64_t var_53;
    uint64_t rax_0;
    struct indirect_placeholder_58_ret_type var_54;
    uint64_t var_55;
    uint64_t var_15;
    uint64_t var_16;
    struct indirect_placeholder_59_ret_type var_17;
    uint64_t var_18;
    var_0 = revng_init_local_sp(0UL);
    var_1 = init_r15();
    var_2 = init_r13();
    var_3 = init_rbx();
    var_4 = init_r12();
    var_5 = init_rbp();
    var_6 = init_cc_src2();
    var_7 = init_r14();
    var_8 = init_state_0x8558();
    var_9 = init_state_0x8560();
    *(uint64_t *)(var_0 + (-8L)) = var_1;
    *(uint64_t *)(var_0 + (-16L)) = var_7;
    *(uint64_t *)(var_0 + (-24L)) = var_2;
    *(uint64_t *)(var_0 + (-32L)) = var_4;
    *(uint64_t *)(var_0 + (-40L)) = var_5;
    *(uint64_t *)(var_0 + (-48L)) = var_3;
    var_10 = (rdi > rdx);
    var_11 = var_10 ? rdx : rdi;
    var_12 = var_10 ? rcx : rsi;
    var_13 = var_10 ? rdi : rdx;
    var_14 = var_10 ? rsi : rcx;
    rdx3_0 = var_13;
    rcx2_1 = var_13;
    rdx3_1 = 0UL;
    rsi4_0 = 0UL;
    r13_0 = var_12;
    r12_1 = 0UL;
    r10_0 = 0UL;
    rdx3_3 = 0UL;
    rax_0 = 0UL;
    if (var_11 == 0UL) {
        *(uint64_t *)r8 = 0UL;
        *(uint64_t *)(var_0 + (-80L)) = 4221427UL;
        var_54 = indirect_placeholder_58(1UL);
        var_55 = var_54.field_0;
        *(uint64_t *)(r8 + 8UL) = var_55;
        rax_0 = var_55;
    } else {
        var_15 = var_11 + var_13;
        *(uint64_t *)(var_0 + (-64L)) = var_11;
        var_16 = var_15 << 2UL;
        *(uint64_t *)(var_0 + (-80L)) = 4221474UL;
        var_17 = indirect_placeholder_59(var_16);
        var_18 = var_17.field_0;
        rcx2_3 = var_18;
        r12_0 = var_15;
        if (var_18 != 0UL) {
            var_19 = *(uint64_t *)(var_0 + (-72L));
            var_20 = (var_13 == 0UL);
            if (var_20) {
                _pre = var_13 << 2UL;
                _pre_phi = _pre;
            } else {
                var_21 = var_13 << 2UL;
                var_22 = ((var_21 + var_18) >> 2UL) & 3UL;
                var_23 = (var_22 > var_13) ? var_13 : var_22;
                _pre_phi = var_21;
                if (var_13 <= 6UL) {
                    rdx3_0 = var_23;
                    if (var_23 != 0UL) {
                        var_30 = var_13 + (-1L);
                        r11_0 = var_30;
                        var_31 = r11_0 - rdx3_1;
                        var_32 = var_13 - rdx3_1;
                        var_33 = ((var_32 + (-4L)) >> 2UL) + 1UL;
                        var_34 = var_33 << 2UL;
                        rcx2_2 = rcx2_1;
                        if (var_31 > 2UL) {
                            var_35 = helper_pxor_xmm_wrapper_62((struct type_3 *)(0UL), (struct type_5 *)(776UL), (struct type_5 *)(776UL), var_8, var_9);
                            var_36 = var_35.field_0;
                            var_37 = var_35.field_1;
                            rdx3_2 = ((((rdx3_1 << 2UL) ^ (-4L)) + var_21) + (-12L)) + var_18;
                            var_38 = rsi4_0 + 1UL;
                            *(uint64_t *)rdx3_2 = var_36;
                            *(uint64_t *)(rdx3_2 + 8UL) = var_37;
                            var_39 = helper_cc_compute_c_wrapper(var_38 - var_33, var_33, var_6, 17U);
                            rsi4_0 = var_38;
                            while (var_39 != 0UL)
                                {
                                    rdx3_2 = rdx3_2 + (-16L);
                                    var_38 = rsi4_0 + 1UL;
                                    *(uint64_t *)rdx3_2 = var_36;
                                    *(uint64_t *)(rdx3_2 + 8UL) = var_37;
                                    var_39 = helper_cc_compute_c_wrapper(var_38 - var_33, var_33, var_6, 17U);
                                    rsi4_0 = var_38;
                                }
                            var_40 = rcx2_1 - var_34;
                            rcx2_2 = var_40;
                            var_41 = rcx2_2 + (-1L);
                            *(uint32_t *)((var_41 << 2UL) + var_18) = 0U;
                            var_42 = rcx2_2 + (-2L);
                            *(uint32_t *)((var_42 << 2UL) + var_18) = 0U;
                            if (var_34 != var_32 & var_41 != 0UL & var_42 == 0UL) {
                                *(uint32_t *)(((rcx2_2 << 2UL) + var_18) + (-12L)) = 0U;
                            }
                        } else {
                            var_41 = rcx2_2 + (-1L);
                            *(uint32_t *)((var_41 << 2UL) + var_18) = 0U;
                            var_42 = rcx2_2 + (-2L);
                            *(uint32_t *)((var_42 << 2UL) + var_18) = 0U;
                            if (var_41 != 0UL & var_42 == 0UL) {
                                *(uint32_t *)(((rcx2_2 << 2UL) + var_18) + (-12L)) = 0U;
                            }
                        }
                        var_43 = (var_19 << 2UL) + var_18;
                        while (1U)
                            {
                                var_44 = (uint64_t)*(uint32_t *)r13_0;
                                if (var_20) {
                                    *(uint32_t *)(_pre_phi + rcx2_3) = 0U;
                                    var_52 = rcx2_3 + 4UL;
                                    rcx2_3_be = var_52;
                                    if (var_52 == var_43) {
                                        break;
                                    }
                                }
                                var_45 = r10_0 << 2UL;
                                var_46 = (uint64_t)*(uint32_t *)(var_45 + var_14);
                                var_47 = (uint32_t *)(var_45 + rcx2_3);
                                var_48 = rdx3_3 + ((var_44 * var_46) + (uint64_t)*var_47);
                                *var_47 = (uint32_t)var_48;
                                var_49 = r10_0 + 1UL;
                                var_50 = var_48 >> 32UL;
                                r10_0 = var_49;
                                rdx3_3 = var_50;
                                do {
                                    var_45 = r10_0 << 2UL;
                                    var_46 = (uint64_t)*(uint32_t *)(var_45 + var_14);
                                    var_47 = (uint32_t *)(var_45 + rcx2_3);
                                    var_48 = rdx3_3 + ((var_44 * var_46) + (uint64_t)*var_47);
                                    *var_47 = (uint32_t)var_48;
                                    var_49 = r10_0 + 1UL;
                                    var_50 = var_48 >> 32UL;
                                    r10_0 = var_49;
                                    rdx3_3 = var_50;
                                } while (var_49 != var_13);
                                *(uint32_t *)(_pre_phi + rcx2_3) = (uint32_t)var_50;
                                var_51 = rcx2_3 + 4UL;
                                rcx2_3_be = var_51;
                                if (var_51 == var_43) {
                                    break;
                                }
                                r13_0 = r13_0 + 4UL;
                                rcx2_3 = rcx2_3_be;
                                continue;
                            }
                        r12_1 = var_15;
                        if (var_15 != 0UL & *(uint32_t *)((var_16 + var_18) + (-4L)) == 0U) {
                            var_53 = r12_0 + (-1L);
                            r12_0 = var_53;
                            r12_1 = 0UL;
                            while (var_53 != 0UL)
                                {
                                    r12_1 = var_53;
                                    if (*(uint32_t *)(((var_53 << 2UL) + var_18) + (-4L)) == 0U) {
                                        break;
                                    }
                                    var_53 = r12_0 + (-1L);
                                    r12_0 = var_53;
                                    r12_1 = 0UL;
                                }
                        }
                        *(uint64_t *)r8 = r12_1;
                        *(uint64_t *)(r8 + 8UL) = var_18;
                        return var_18;
                    }
                }
                var_24 = var_13 + (-1L);
                *(uint32_t *)((var_24 << 2UL) + var_18) = 0U;
                rcx2_0 = var_24;
                rdx3_1 = rdx3_0;
                r11_0 = var_24;
                var_25 = var_13 + (-2L);
                *(uint32_t *)((var_25 << 2UL) + var_18) = 0U;
                rcx2_0 = var_25;
                var_26 = var_13 + (-3L);
                *(uint32_t *)((var_26 << 2UL) + var_18) = 0U;
                rcx2_0 = var_26;
                var_27 = var_13 + (-4L);
                *(uint32_t *)((var_27 << 2UL) + var_18) = 0U;
                rcx2_0 = var_27;
                var_28 = var_13 + (-5L);
                *(uint32_t *)((var_28 << 2UL) + var_18) = 0U;
                rcx2_0 = var_28;
                if (rdx3_0 != 1UL & rdx3_0 != 2UL & rdx3_0 != 3UL & rdx3_0 != 4UL & rdx3_0 == 6UL) {
                    var_29 = var_13 + (-6L);
                    *(uint32_t *)((var_29 << 2UL) + var_18) = 0U;
                    rcx2_0 = var_29;
                }
                rcx2_1 = rcx2_0;
                if (var_13 != rdx3_0) {
                    var_31 = r11_0 - rdx3_1;
                    var_32 = var_13 - rdx3_1;
                    var_33 = ((var_32 + (-4L)) >> 2UL) + 1UL;
                    var_34 = var_33 << 2UL;
                    rcx2_2 = rcx2_1;
                    if (var_31 > 2UL) {
                        var_35 = helper_pxor_xmm_wrapper_62((struct type_3 *)(0UL), (struct type_5 *)(776UL), (struct type_5 *)(776UL), var_8, var_9);
                        var_36 = var_35.field_0;
                        var_37 = var_35.field_1;
                        rdx3_2 = ((((rdx3_1 << 2UL) ^ (-4L)) + var_21) + (-12L)) + var_18;
                        var_38 = rsi4_0 + 1UL;
                        *(uint64_t *)rdx3_2 = var_36;
                        *(uint64_t *)(rdx3_2 + 8UL) = var_37;
                        var_39 = helper_cc_compute_c_wrapper(var_38 - var_33, var_33, var_6, 17U);
                        rsi4_0 = var_38;
                        while (var_39 != 0UL)
                            {
                                rdx3_2 = rdx3_2 + (-16L);
                                var_38 = rsi4_0 + 1UL;
                                *(uint64_t *)rdx3_2 = var_36;
                                *(uint64_t *)(rdx3_2 + 8UL) = var_37;
                                var_39 = helper_cc_compute_c_wrapper(var_38 - var_33, var_33, var_6, 17U);
                                rsi4_0 = var_38;
                            }
                        var_40 = rcx2_1 - var_34;
                        rcx2_2 = var_40;
                        var_41 = rcx2_2 + (-1L);
                        *(uint32_t *)((var_41 << 2UL) + var_18) = 0U;
                        var_42 = rcx2_2 + (-2L);
                        *(uint32_t *)((var_42 << 2UL) + var_18) = 0U;
                        if (var_34 != var_32 & var_41 != 0UL & var_42 == 0UL) {
                            *(uint32_t *)(((rcx2_2 << 2UL) + var_18) + (-12L)) = 0U;
                        }
                    } else {
                        var_41 = rcx2_2 + (-1L);
                        *(uint32_t *)((var_41 << 2UL) + var_18) = 0U;
                        var_42 = rcx2_2 + (-2L);
                        *(uint32_t *)((var_42 << 2UL) + var_18) = 0U;
                        if (var_41 != 0UL & var_42 == 0UL) {
                            *(uint32_t *)(((rcx2_2 << 2UL) + var_18) + (-12L)) = 0U;
                        }
                    }
                }
            }
        }
    }
}

typedef unsigned char __u_char;
typedef unsigned short __u_short;
typedef unsigned int __u_int;
typedef unsigned long __u_long;
typedef signed char __int8_t;
typedef unsigned char __uint8_t;
typedef short __int16_t;
typedef unsigned short __uint16_t;
typedef int __int32_t;
typedef unsigned int __uint32_t;
typedef long __int64_t;
typedef unsigned long __uint64_t;
typedef __int8_t __int_least8_t;
typedef __uint8_t __uint_least8_t;
typedef __int16_t __int_least16_t;
typedef __uint16_t __uint_least16_t;
typedef __int32_t __int_least32_t;
typedef __uint32_t __uint_least32_t;
typedef __int64_t __int_least64_t;
typedef __uint64_t __uint_least64_t;
typedef long __quad_t;
typedef unsigned long __u_quad_t;
typedef long __intmax_t;
typedef unsigned long __uintmax_t;
typedef unsigned long __dev_t;
typedef unsigned int __uid_t;
typedef unsigned int __gid_t;
typedef unsigned long __ino_t;
typedef unsigned long __ino64_t;
typedef unsigned int __mode_t;
typedef unsigned long __nlink_t;
typedef long __off_t;
typedef long __off64_t;
typedef int __pid_t;
typedef struct {
    int __val[2];
} __fsid_t;
typedef long __clock_t;
typedef unsigned long __rlim_t;
typedef unsigned long __rlim64_t;
typedef unsigned int __id_t;
typedef long __time_t;
typedef unsigned int __useconds_t;
typedef long __suseconds_t;
typedef int __daddr_t;
typedef int __key_t;
typedef int __clockid_t;
typedef void *__timer_t;
typedef long __blksize_t;
typedef long __blkcnt_t;
typedef long __blkcnt64_t;
typedef unsigned long __fsblkcnt_t;
typedef unsigned long __fsblkcnt64_t;
typedef unsigned long __fsfilcnt_t;
typedef unsigned long __fsfilcnt64_t;
typedef long __fsword_t;
typedef long __ssize_t;
typedef long __syscall_slong_t;
typedef unsigned long __syscall_ulong_t;
typedef __off64_t __loff_t;
typedef char *__caddr_t;
typedef long __intptr_t;
typedef unsigned int __socklen_t;
typedef int __sig_atomic_t;
typedef __int8_t int8_t;
typedef __int16_t int16_t;
typedef __int32_t int32_t;
typedef __int64_t int64_t;
typedef __uint8_t uint8_t;
typedef __uint16_t uint16_t;
typedef __uint32_t uint32_t;
typedef __uint64_t uint64_t;
typedef __int_least8_t int_least8_t;
typedef __int_least16_t int_least16_t;
typedef __int_least32_t int_least32_t;
typedef __int_least64_t int_least64_t;
typedef __uint_least8_t uint_least8_t;
typedef __uint_least16_t uint_least16_t;
typedef __uint_least32_t uint_least32_t;
typedef __uint_least64_t uint_least64_t;
typedef signed char int_fast8_t;
typedef long int_fast16_t;
typedef long int_fast32_t;
typedef long int_fast64_t;
typedef unsigned char uint_fast8_t;
typedef unsigned long uint_fast16_t;
typedef unsigned long uint_fast32_t;
typedef unsigned long uint_fast64_t;
typedef long intptr_t;
typedef unsigned long uintptr_t;
typedef __intmax_t intmax_t;
typedef __uintmax_t uintmax_t;
struct helper_paddq_xmm_wrapper_ret_type;
struct type_4;
struct type_6;
struct helper_psllq_xmm_wrapper_ret_type;
struct helper_psllq_xmm_wrapper_181_ret_type;
struct helper_paddq_xmm_wrapper_182_ret_type;
struct helper_paddq_xmm_wrapper_183_ret_type;
struct indirect_placeholder_54_ret_type;
struct helper_punpcklqdq_xmm_wrapper_ret_type;
struct helper_paddq_xmm_wrapper_ret_type {
    uint64_t field_0;
    uint64_t field_1;
};
struct type_4 {
};
struct type_6 {
};
struct helper_psllq_xmm_wrapper_ret_type {
    uint64_t field_0;
    uint64_t field_1;
};
struct helper_psllq_xmm_wrapper_181_ret_type {
    uint64_t field_0;
    uint64_t field_1;
};
struct helper_paddq_xmm_wrapper_182_ret_type {
    uint64_t field_0;
    uint64_t field_1;
};
struct helper_paddq_xmm_wrapper_183_ret_type {
    uint64_t field_0;
    uint64_t field_1;
};
struct indirect_placeholder_54_ret_type {
    uint64_t field_0;
    uint64_t field_1;
    uint64_t field_2;
};
struct helper_punpcklqdq_xmm_wrapper_ret_type {
    uint64_t field_0;
    uint64_t field_1;
    uint64_t field_2;
    uint64_t field_3;
    uint64_t field_4;
    uint64_t field_5;
    uint64_t field_6;
    uint64_t field_7;
};
extern void abort(void);
extern uint64_t revng_init_local_sp(uint64_t param_0);
extern void indirect_placeholder(void);
extern uint64_t init_rbx(void);
extern uint64_t init_cc_src2(void);
extern uint64_t helper_cc_compute_c_wrapper(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint32_t param_3);
extern uint64_t init_r8(void);
extern void indirect_placeholder_20(uint64_t param_0, uint64_t param_1, uint64_t param_2);
extern uint64_t init_r9(void);
extern void indirect_placeholder_2(uint64_t param_0, uint64_t param_1);
extern struct helper_paddq_xmm_wrapper_ret_type helper_paddq_xmm_wrapper(struct type_4 *param_0, struct type_6 *param_1, struct type_6 *param_2, uint64_t param_3, uint64_t param_4, uint64_t param_5, uint64_t param_6);
extern struct helper_psllq_xmm_wrapper_ret_type helper_psllq_xmm_wrapper(struct type_4 *param_0, struct type_6 *param_1, struct type_6 *param_2, uint64_t param_3, uint64_t param_4, uint64_t param_5);
extern struct helper_psllq_xmm_wrapper_181_ret_type helper_psllq_xmm_wrapper_181(struct type_4 *param_0, struct type_6 *param_1, struct type_6 *param_2, uint64_t param_3, uint64_t param_4, uint64_t param_5);
extern struct helper_paddq_xmm_wrapper_182_ret_type helper_paddq_xmm_wrapper_182(struct type_4 *param_0, struct type_6 *param_1, struct type_6 *param_2, uint64_t param_3, uint64_t param_4, uint64_t param_5, uint64_t param_6);
extern struct helper_paddq_xmm_wrapper_183_ret_type helper_paddq_xmm_wrapper_183(struct type_4 *param_0, struct type_6 *param_1, struct type_6 *param_2, uint64_t param_3, uint64_t param_4, uint64_t param_5, uint64_t param_6);
extern struct indirect_placeholder_54_ret_type indirect_placeholder_54(uint64_t param_0);
extern struct helper_punpcklqdq_xmm_wrapper_ret_type helper_punpcklqdq_xmm_wrapper(struct type_4 *param_0, struct type_6 *param_1, struct type_6 *param_2, uint64_t param_3);
void bb_sort_files(void) {
    uint64_t rax_0;
    uint64_t rdx_2;
    uint64_t state_0x85d8_0;
    uint64_t state_0x85e0_0;
    uint64_t var_69;
    struct helper_psllq_xmm_wrapper_ret_type var_71;
    uint64_t var_72;
    uint64_t var_73;
    struct helper_psllq_xmm_wrapper_181_ret_type var_74;
    struct helper_paddq_xmm_wrapper_182_ret_type var_75;
    uint64_t var_76;
    uint64_t var_77;
    struct helper_psllq_xmm_wrapper_ret_type var_78;
    struct helper_paddq_xmm_wrapper_182_ret_type var_79;
    struct helper_paddq_xmm_wrapper_183_ret_type var_80;
    uint64_t var_81;
    uint64_t var_82;
    uint64_t var_83;
    uint64_t var_84;
    uint64_t var_49;
    uint64_t var_0;
    uint64_t var_1;
    uint64_t var_2;
    uint64_t var_3;
    uint64_t var_4;
    uint64_t var_5;
    uint64_t var_6;
    uint64_t rax_2;
    uint64_t var_8;
    uint64_t var_9;
    struct indirect_placeholder_54_ret_type var_10;
    struct helper_paddq_xmm_wrapper_ret_type var_70;
    uint64_t var_11;
    uint32_t var_48;
    uint64_t local_sp_0;
    uint64_t r8_0;
    uint64_t var_50;
    uint64_t var_51;
    uint64_t var_52;
    uint64_t var_53;
    uint64_t rdx_1;
    uint64_t var_54;
    uint64_t r10_0;
    uint64_t r10_1;
    uint64_t rdx_0;
    uint64_t var_55;
    uint64_t var_56;
    uint64_t r10_2;
    uint64_t var_57;
    uint64_t *var_58;
    uint64_t *var_59;
    uint64_t var_60;
    uint64_t var_61;
    uint64_t *var_62;
    uint64_t var_63;
    struct helper_punpcklqdq_xmm_wrapper_ret_type var_64;
    uint64_t var_65;
    uint64_t var_66;
    uint64_t var_67;
    uint64_t var_68;
    uint64_t rax_1;
    uint64_t var_85;
    uint64_t var_86;
    uint64_t var_87;
    uint64_t rdi_0;
    uint64_t local_sp_1;
    uint64_t r8_1;
    uint64_t rsi_0;
    uint32_t var_88;
    uint64_t var_89;
    uint64_t local_sp_2;
    uint64_t rax_4;
    uint64_t var_12;
    uint64_t var_13;
    uint64_t var_14;
    uint64_t r9_0;
    uint64_t rcx_0;
    uint64_t var_17;
    uint64_t rax_3;
    uint64_t var_15;
    uint64_t var_16;
    uint64_t r9_2;
    uint64_t r10_3_in;
    uint64_t r9_1;
    uint64_t rcx_1;
    uint64_t r10_3;
    uint64_t var_18;
    uint64_t *var_19;
    uint64_t *var_20;
    uint64_t var_21;
    uint64_t var_22;
    uint64_t *var_23;
    uint64_t var_24;
    struct helper_punpcklqdq_xmm_wrapper_ret_type var_25;
    uint64_t var_26;
    uint64_t var_27;
    uint64_t var_28;
    uint64_t var_29;
    uint64_t rcx_2;
    uint64_t rdx_3;
    uint64_t state_0x85d8_1;
    uint64_t state_0x85e0_1;
    uint64_t var_30;
    struct helper_paddq_xmm_wrapper_ret_type var_31;
    struct helper_psllq_xmm_wrapper_ret_type var_32;
    uint64_t var_33;
    uint64_t var_34;
    struct helper_psllq_xmm_wrapper_181_ret_type var_35;
    struct helper_paddq_xmm_wrapper_182_ret_type var_36;
    uint64_t var_37;
    uint64_t var_38;
    struct helper_psllq_xmm_wrapper_ret_type var_39;
    struct helper_paddq_xmm_wrapper_182_ret_type var_40;
    struct helper_paddq_xmm_wrapper_183_ret_type var_41;
    uint64_t var_42;
    uint64_t var_43;
    uint64_t var_44;
    uint64_t var_45;
    uint64_t var_46;
    uint64_t var_47;
    uint64_t var_7;
    var_0 = revng_init_local_sp(0UL);
    var_1 = init_rbx();
    var_2 = init_r9();
    var_3 = init_cc_src2();
    var_4 = init_r8();
    *(uint64_t *)(var_0 + (-8L)) = var_1;
    var_5 = var_0 + (-56L);
    var_6 = *(uint64_t *)6509456UL;
    rax_0 = 0UL;
    rax_2 = var_6;
    rdx_1 = 0UL;
    r10_0 = 1UL;
    r10_1 = 0UL;
    rdx_0 = 1UL;
    rax_1 = 1UL;
    rsi_0 = 0UL;
    local_sp_2 = var_5;
    rax_4 = 0UL;
    r9_0 = 1UL;
    rcx_0 = 1UL;
    r9_1 = 0UL;
    rcx_1 = 0UL;
    rdx_3 = 0UL;
    if (((var_6 >> 1UL) + var_6) <= *(uint64_t *)6509432UL) {
        *(uint64_t *)(var_0 + (-64L)) = 4215228UL;
        indirect_placeholder();
        var_7 = *(uint64_t *)6509456UL;
        if (var_7 <= 384307168202282325UL) {
            *(uint64_t *)(var_0 + (-72L)) = 4215528UL;
            indirect_placeholder_2(var_2, var_4);
            abort();
        }
        var_8 = var_7 * 24UL;
        var_9 = var_0 + (-72L);
        *(uint64_t *)var_9 = 4215267UL;
        var_10 = indirect_placeholder_54(var_8);
        *(uint64_t *)6509440UL = var_10.field_0;
        var_11 = *(uint64_t *)6509456UL;
        *(uint64_t *)6509432UL = (var_11 * 3UL);
        rax_2 = var_11;
        local_sp_2 = var_9;
    }
    rax_3 = rax_2;
    if (rax_2 != 0UL) {
        var_12 = *(uint64_t *)6509440UL;
        var_13 = *(uint64_t *)6509472UL;
        var_14 = (var_12 >> 3UL) & 1UL;
        rax_4 = rax_2;
        if (rax_2 > 6UL) {
            if (((rax_2 > var_14) ? var_14 : rax_2) == 0UL) {
                var_17 = ((rax_2 + (-2L)) >> 1UL) + 1UL;
                r10_3_in = var_17;
                r10_3 = r10_3_in << 1UL;
                var_18 = r9_1 + 1UL;
                var_19 = (uint64_t *)(local_sp_2 + 16UL);
                *var_19 = var_13;
                var_20 = (uint64_t *)(local_sp_2 + 8UL);
                *var_20 = r9_1;
                var_21 = *var_19;
                var_22 = (rcx_1 << 3UL) + var_12;
                var_23 = (uint64_t *)local_sp_2;
                *var_23 = var_18;
                var_24 = *var_20;
                var_25 = helper_punpcklqdq_xmm_wrapper((struct type_4 *)(0UL), (struct type_6 *)(968UL), (struct type_6 *)(968UL), var_21);
                var_26 = var_25.field_0;
                var_27 = var_25.field_1;
                var_28 = *(uint64_t *)4376704UL;
                var_29 = *(uint64_t *)4376712UL;
                rcx_2 = var_22;
                state_0x85d8_1 = var_24;
                state_0x85e0_1 = *var_23;
                rax_4 = rax_3;
                var_30 = rdx_3 + 1UL;
                var_31 = helper_paddq_xmm_wrapper((struct type_4 *)(0UL), (struct type_6 *)(904UL), (struct type_6 *)(1032UL), state_0x85d8_1, state_0x85e0_1, var_28, var_29);
                var_32 = helper_psllq_xmm_wrapper((struct type_4 *)(0UL), (struct type_6 *)(840UL), (struct type_6 *)(2824UL), state_0x85d8_1, state_0x85e0_1, 3UL);
                var_33 = var_32.field_0;
                var_34 = var_32.field_1;
                var_35 = helper_psllq_xmm_wrapper_181((struct type_4 *)(0UL), (struct type_6 *)(776UL), (struct type_6 *)(2824UL), var_33, var_34, 2UL);
                var_36 = helper_paddq_xmm_wrapper_182((struct type_4 *)(0UL), (struct type_6 *)(776UL), (struct type_6 *)(840UL), var_35.field_0, var_35.field_1, var_33, var_34);
                var_37 = var_36.field_0;
                var_38 = var_36.field_1;
                var_39 = helper_psllq_xmm_wrapper((struct type_4 *)(0UL), (struct type_6 *)(840UL), (struct type_6 *)(2824UL), var_37, var_38, 2UL);
                var_40 = helper_paddq_xmm_wrapper_182((struct type_4 *)(0UL), (struct type_6 *)(776UL), (struct type_6 *)(840UL), var_37, var_38, var_39.field_0, var_39.field_1);
                var_41 = helper_paddq_xmm_wrapper_183((struct type_4 *)(0UL), (struct type_6 *)(776UL), (struct type_6 *)(968UL), var_40.field_0, var_40.field_1, var_26, var_27);
                var_42 = var_41.field_0;
                var_43 = var_41.field_1;
                *(uint64_t *)rcx_2 = var_42;
                *(uint64_t *)(rcx_2 + 8UL) = var_43;
                var_44 = helper_cc_compute_c_wrapper(var_30 - r10_3_in, r10_3_in, var_3, 17U);
                rdx_3 = var_30;
                while (var_44 != 0UL)
                    {
                        rcx_2 = rcx_2 + 16UL;
                        state_0x85d8_1 = var_31.field_0;
                        state_0x85e0_1 = var_31.field_1;
                        var_30 = rdx_3 + 1UL;
                        var_31 = helper_paddq_xmm_wrapper((struct type_4 *)(0UL), (struct type_6 *)(904UL), (struct type_6 *)(1032UL), state_0x85d8_1, state_0x85e0_1, var_28, var_29);
                        var_32 = helper_psllq_xmm_wrapper((struct type_4 *)(0UL), (struct type_6 *)(840UL), (struct type_6 *)(2824UL), state_0x85d8_1, state_0x85e0_1, 3UL);
                        var_33 = var_32.field_0;
                        var_34 = var_32.field_1;
                        var_35 = helper_psllq_xmm_wrapper_181((struct type_4 *)(0UL), (struct type_6 *)(776UL), (struct type_6 *)(2824UL), var_33, var_34, 2UL);
                        var_36 = helper_paddq_xmm_wrapper_182((struct type_4 *)(0UL), (struct type_6 *)(776UL), (struct type_6 *)(840UL), var_35.field_0, var_35.field_1, var_33, var_34);
                        var_37 = var_36.field_0;
                        var_38 = var_36.field_1;
                        var_39 = helper_psllq_xmm_wrapper((struct type_4 *)(0UL), (struct type_6 *)(840UL), (struct type_6 *)(2824UL), var_37, var_38, 2UL);
                        var_40 = helper_paddq_xmm_wrapper_182((struct type_4 *)(0UL), (struct type_6 *)(776UL), (struct type_6 *)(840UL), var_37, var_38, var_39.field_0, var_39.field_1);
                        var_41 = helper_paddq_xmm_wrapper_183((struct type_4 *)(0UL), (struct type_6 *)(776UL), (struct type_6 *)(968UL), var_40.field_0, var_40.field_1, var_26, var_27);
                        var_42 = var_41.field_0;
                        var_43 = var_41.field_1;
                        *(uint64_t *)rcx_2 = var_42;
                        *(uint64_t *)(rcx_2 + 8UL) = var_43;
                        var_44 = helper_cc_compute_c_wrapper(var_30 - r10_3_in, r10_3_in, var_3, 17U);
                        rdx_3 = var_30;
                    }
                var_45 = r9_1 + r10_3;
                r9_2 = var_45;
                if (rax_3 == r10_3) {
                    var_46 = (r9_2 * 200UL) + var_13;
                    *(uint64_t *)((r9_2 << 3UL) + var_12) = var_46;
                    rax_4 = var_46;
                }
            } else {
                *(uint64_t *)var_12 = var_13;
                r9_1 = r9_0;
                rcx_1 = rcx_0;
                r9_2 = r9_0;
                if (rax_2 != rcx_0) {
                    var_15 = rax_2 - rcx_0;
                    var_16 = ((var_15 + (-2L)) >> 1UL) + 1UL;
                    rax_3 = var_15;
                    r10_3_in = var_16;
                    if (var_15 == 1UL) {
                        var_46 = (r9_2 * 200UL) + var_13;
                        *(uint64_t *)((r9_2 << 3UL) + var_12) = var_46;
                        rax_4 = var_46;
                    } else {
                        r10_3 = r10_3_in << 1UL;
                        var_18 = r9_1 + 1UL;
                        var_19 = (uint64_t *)(local_sp_2 + 16UL);
                        *var_19 = var_13;
                        var_20 = (uint64_t *)(local_sp_2 + 8UL);
                        *var_20 = r9_1;
                        var_21 = *var_19;
                        var_22 = (rcx_1 << 3UL) + var_12;
                        var_23 = (uint64_t *)local_sp_2;
                        *var_23 = var_18;
                        var_24 = *var_20;
                        var_25 = helper_punpcklqdq_xmm_wrapper((struct type_4 *)(0UL), (struct type_6 *)(968UL), (struct type_6 *)(968UL), var_21);
                        var_26 = var_25.field_0;
                        var_27 = var_25.field_1;
                        var_28 = *(uint64_t *)4376704UL;
                        var_29 = *(uint64_t *)4376712UL;
                        rcx_2 = var_22;
                        state_0x85d8_1 = var_24;
                        state_0x85e0_1 = *var_23;
                        rax_4 = rax_3;
                        var_30 = rdx_3 + 1UL;
                        var_31 = helper_paddq_xmm_wrapper((struct type_4 *)(0UL), (struct type_6 *)(904UL), (struct type_6 *)(1032UL), state_0x85d8_1, state_0x85e0_1, var_28, var_29);
                        var_32 = helper_psllq_xmm_wrapper((struct type_4 *)(0UL), (struct type_6 *)(840UL), (struct type_6 *)(2824UL), state_0x85d8_1, state_0x85e0_1, 3UL);
                        var_33 = var_32.field_0;
                        var_34 = var_32.field_1;
                        var_35 = helper_psllq_xmm_wrapper_181((struct type_4 *)(0UL), (struct type_6 *)(776UL), (struct type_6 *)(2824UL), var_33, var_34, 2UL);
                        var_36 = helper_paddq_xmm_wrapper_182((struct type_4 *)(0UL), (struct type_6 *)(776UL), (struct type_6 *)(840UL), var_35.field_0, var_35.field_1, var_33, var_34);
                        var_37 = var_36.field_0;
                        var_38 = var_36.field_1;
                        var_39 = helper_psllq_xmm_wrapper((struct type_4 *)(0UL), (struct type_6 *)(840UL), (struct type_6 *)(2824UL), var_37, var_38, 2UL);
                        var_40 = helper_paddq_xmm_wrapper_182((struct type_4 *)(0UL), (struct type_6 *)(776UL), (struct type_6 *)(840UL), var_37, var_38, var_39.field_0, var_39.field_1);
                        var_41 = helper_paddq_xmm_wrapper_183((struct type_4 *)(0UL), (struct type_6 *)(776UL), (struct type_6 *)(968UL), var_40.field_0, var_40.field_1, var_26, var_27);
                        var_42 = var_41.field_0;
                        var_43 = var_41.field_1;
                        *(uint64_t *)rcx_2 = var_42;
                        *(uint64_t *)(rcx_2 + 8UL) = var_43;
                        var_44 = helper_cc_compute_c_wrapper(var_30 - r10_3_in, r10_3_in, var_3, 17U);
                        rdx_3 = var_30;
                        while (var_44 != 0UL)
                            {
                                rcx_2 = rcx_2 + 16UL;
                                state_0x85d8_1 = var_31.field_0;
                                state_0x85e0_1 = var_31.field_1;
                                var_30 = rdx_3 + 1UL;
                                var_31 = helper_paddq_xmm_wrapper((struct type_4 *)(0UL), (struct type_6 *)(904UL), (struct type_6 *)(1032UL), state_0x85d8_1, state_0x85e0_1, var_28, var_29);
                                var_32 = helper_psllq_xmm_wrapper((struct type_4 *)(0UL), (struct type_6 *)(840UL), (struct type_6 *)(2824UL), state_0x85d8_1, state_0x85e0_1, 3UL);
                                var_33 = var_32.field_0;
                                var_34 = var_32.field_1;
                                var_35 = helper_psllq_xmm_wrapper_181((struct type_4 *)(0UL), (struct type_6 *)(776UL), (struct type_6 *)(2824UL), var_33, var_34, 2UL);
                                var_36 = helper_paddq_xmm_wrapper_182((struct type_4 *)(0UL), (struct type_6 *)(776UL), (struct type_6 *)(840UL), var_35.field_0, var_35.field_1, var_33, var_34);
                                var_37 = var_36.field_0;
                                var_38 = var_36.field_1;
                                var_39 = helper_psllq_xmm_wrapper((struct type_4 *)(0UL), (struct type_6 *)(840UL), (struct type_6 *)(2824UL), var_37, var_38, 2UL);
                                var_40 = helper_paddq_xmm_wrapper_182((struct type_4 *)(0UL), (struct type_6 *)(776UL), (struct type_6 *)(840UL), var_37, var_38, var_39.field_0, var_39.field_1);
                                var_41 = helper_paddq_xmm_wrapper_183((struct type_4 *)(0UL), (struct type_6 *)(776UL), (struct type_6 *)(968UL), var_40.field_0, var_40.field_1, var_26, var_27);
                                var_42 = var_41.field_0;
                                var_43 = var_41.field_1;
                                *(uint64_t *)rcx_2 = var_42;
                                *(uint64_t *)(rcx_2 + 8UL) = var_43;
                                var_44 = helper_cc_compute_c_wrapper(var_30 - r10_3_in, r10_3_in, var_3, 17U);
                                rdx_3 = var_30;
                            }
                        var_45 = r9_1 + r10_3;
                        r9_2 = var_45;
                        if (rax_3 == r10_3) {
                            var_46 = (r9_2 * 200UL) + var_13;
                            *(uint64_t *)((r9_2 << 3UL) + var_12) = var_46;
                            rax_4 = var_46;
                        }
                    }
                }
            }
        } else {
            *(uint64_t *)var_12 = var_13;
            *(uint64_t *)(var_12 + 8UL) = (var_13 + 200UL);
            r9_0 = 2UL;
            rcx_0 = 2UL;
            *(uint64_t *)(var_12 + 16UL) = (var_13 + 400UL);
            r9_0 = 3UL;
            rcx_0 = 3UL;
            *(uint64_t *)(var_12 + 24UL) = (var_13 + 600UL);
            r9_0 = 4UL;
            rcx_0 = 4UL;
            *(uint64_t *)(var_12 + 32UL) = (var_13 + 800UL);
            r9_0 = 5UL;
            rcx_0 = rax_2;
            if (rax_2 != 1UL & rax_2 != 2UL & rax_2 != 3UL & rax_2 != 4UL & rax_2 == 6UL) {
                *(uint64_t *)(var_12 + 40UL) = (var_13 + 1000UL);
                r9_0 = 6UL;
                rcx_0 = 6UL;
            }
            r9_1 = r9_0;
            rcx_1 = rcx_0;
            r9_2 = r9_0;
            if (rax_2 != rcx_0) {
                var_15 = rax_2 - rcx_0;
                var_16 = ((var_15 + (-2L)) >> 1UL) + 1UL;
                rax_3 = var_15;
                r10_3_in = var_16;
                if (var_15 == 1UL) {
                    var_46 = (r9_2 * 200UL) + var_13;
                    *(uint64_t *)((r9_2 << 3UL) + var_12) = var_46;
                    rax_4 = var_46;
                } else {
                    r10_3 = r10_3_in << 1UL;
                    var_18 = r9_1 + 1UL;
                    var_19 = (uint64_t *)(local_sp_2 + 16UL);
                    *var_19 = var_13;
                    var_20 = (uint64_t *)(local_sp_2 + 8UL);
                    *var_20 = r9_1;
                    var_21 = *var_19;
                    var_22 = (rcx_1 << 3UL) + var_12;
                    var_23 = (uint64_t *)local_sp_2;
                    *var_23 = var_18;
                    var_24 = *var_20;
                    var_25 = helper_punpcklqdq_xmm_wrapper((struct type_4 *)(0UL), (struct type_6 *)(968UL), (struct type_6 *)(968UL), var_21);
                    var_26 = var_25.field_0;
                    var_27 = var_25.field_1;
                    var_28 = *(uint64_t *)4376704UL;
                    var_29 = *(uint64_t *)4376712UL;
                    rcx_2 = var_22;
                    state_0x85d8_1 = var_24;
                    state_0x85e0_1 = *var_23;
                    rax_4 = rax_3;
                    var_30 = rdx_3 + 1UL;
                    var_31 = helper_paddq_xmm_wrapper((struct type_4 *)(0UL), (struct type_6 *)(904UL), (struct type_6 *)(1032UL), state_0x85d8_1, state_0x85e0_1, var_28, var_29);
                    var_32 = helper_psllq_xmm_wrapper((struct type_4 *)(0UL), (struct type_6 *)(840UL), (struct type_6 *)(2824UL), state_0x85d8_1, state_0x85e0_1, 3UL);
                    var_33 = var_32.field_0;
                    var_34 = var_32.field_1;
                    var_35 = helper_psllq_xmm_wrapper_181((struct type_4 *)(0UL), (struct type_6 *)(776UL), (struct type_6 *)(2824UL), var_33, var_34, 2UL);
                    var_36 = helper_paddq_xmm_wrapper_182((struct type_4 *)(0UL), (struct type_6 *)(776UL), (struct type_6 *)(840UL), var_35.field_0, var_35.field_1, var_33, var_34);
                    var_37 = var_36.field_0;
                    var_38 = var_36.field_1;
                    var_39 = helper_psllq_xmm_wrapper((struct type_4 *)(0UL), (struct type_6 *)(840UL), (struct type_6 *)(2824UL), var_37, var_38, 2UL);
                    var_40 = helper_paddq_xmm_wrapper_182((struct type_4 *)(0UL), (struct type_6 *)(776UL), (struct type_6 *)(840UL), var_37, var_38, var_39.field_0, var_39.field_1);
                    var_41 = helper_paddq_xmm_wrapper_183((struct type_4 *)(0UL), (struct type_6 *)(776UL), (struct type_6 *)(968UL), var_40.field_0, var_40.field_1, var_26, var_27);
                    var_42 = var_41.field_0;
                    var_43 = var_41.field_1;
                    *(uint64_t *)rcx_2 = var_42;
                    *(uint64_t *)(rcx_2 + 8UL) = var_43;
                    var_44 = helper_cc_compute_c_wrapper(var_30 - r10_3_in, r10_3_in, var_3, 17U);
                    rdx_3 = var_30;
                    while (var_44 != 0UL)
                        {
                            rcx_2 = rcx_2 + 16UL;
                            state_0x85d8_1 = var_31.field_0;
                            state_0x85e0_1 = var_31.field_1;
                            var_30 = rdx_3 + 1UL;
                            var_31 = helper_paddq_xmm_wrapper((struct type_4 *)(0UL), (struct type_6 *)(904UL), (struct type_6 *)(1032UL), state_0x85d8_1, state_0x85e0_1, var_28, var_29);
                            var_32 = helper_psllq_xmm_wrapper((struct type_4 *)(0UL), (struct type_6 *)(840UL), (struct type_6 *)(2824UL), state_0x85d8_1, state_0x85e0_1, 3UL);
                            var_33 = var_32.field_0;
                            var_34 = var_32.field_1;
                            var_35 = helper_psllq_xmm_wrapper_181((struct type_4 *)(0UL), (struct type_6 *)(776UL), (struct type_6 *)(2824UL), var_33, var_34, 2UL);
                            var_36 = helper_paddq_xmm_wrapper_182((struct type_4 *)(0UL), (struct type_6 *)(776UL), (struct type_6 *)(840UL), var_35.field_0, var_35.field_1, var_33, var_34);
                            var_37 = var_36.field_0;
                            var_38 = var_36.field_1;
                            var_39 = helper_psllq_xmm_wrapper((struct type_4 *)(0UL), (struct type_6 *)(840UL), (struct type_6 *)(2824UL), var_37, var_38, 2UL);
                            var_40 = helper_paddq_xmm_wrapper_182((struct type_4 *)(0UL), (struct type_6 *)(776UL), (struct type_6 *)(840UL), var_37, var_38, var_39.field_0, var_39.field_1);
                            var_41 = helper_paddq_xmm_wrapper_183((struct type_4 *)(0UL), (struct type_6 *)(776UL), (struct type_6 *)(968UL), var_40.field_0, var_40.field_1, var_26, var_27);
                            var_42 = var_41.field_0;
                            var_43 = var_41.field_1;
                            *(uint64_t *)rcx_2 = var_42;
                            *(uint64_t *)(rcx_2 + 8UL) = var_43;
                            var_44 = helper_cc_compute_c_wrapper(var_30 - r10_3_in, r10_3_in, var_3, 17U);
                            rdx_3 = var_30;
                        }
                    var_45 = r9_1 + r10_3;
                    r9_2 = var_45;
                    if (rax_3 == r10_3) {
                        var_46 = (r9_2 * 200UL) + var_13;
                        *(uint64_t *)((r9_2 << 3UL) + var_12) = var_46;
                        rax_4 = var_46;
                    }
                }
            }
        }
    }
    if (*(uint32_t *)6509328UL == 4294967295U) {
        return;
    }
    var_47 = local_sp_2 + (-8L);
    *(uint64_t *)var_47 = 4214578UL;
    indirect_placeholder();
    local_sp_0 = var_47;
    local_sp_1 = var_47;
    if ((uint64_t)(uint32_t)rax_4 == 0UL) {
        var_85 = *(uint64_t *)6509456UL;
        var_86 = *(uint64_t *)6509440UL;
        var_87 = (uint64_t)*(uint32_t *)6509328UL;
        rax_1 = rax_4;
        rdi_0 = var_86;
        r8_1 = var_87;
        rsi_0 = var_85;
    } else {
        var_48 = *(uint32_t *)6509328UL;
        r8_0 = (uint64_t)var_48;
        if ((uint64_t)(var_48 + (-3)) == 0UL) {
            var_49 = local_sp_2 + (-16L);
            *(uint64_t *)var_49 = 4215433UL;
            indirect_placeholder();
            local_sp_0 = var_49;
            r8_0 = (uint64_t)*(uint32_t *)6509328UL;
        }
        var_50 = *(uint64_t *)6509456UL;
        var_51 = *(uint64_t *)6509440UL;
        rdi_0 = var_51;
        local_sp_1 = local_sp_0;
        r8_1 = r8_0;
        if (var_50 != 0UL) {
            var_52 = *(uint64_t *)6509472UL;
            var_53 = (var_51 >> 3UL) & 1UL;
            rsi_0 = var_50;
            if (var_50 > 6UL) {
                if (((var_53 > var_50) ? var_50 : var_53) != 0UL) {
                    *(uint64_t *)var_51 = var_52;
                    r10_1 = r10_0;
                    rdx_1 = rdx_0;
                    if (var_50 != rdx_0) {
                        var_88 = (uint32_t)r8_1;
                        var_89 = *(uint64_t *)((((((((((uint64_t)((uint32_t)(((uint64_t)(var_88 + (-4)) == 0UL) ? (uint64_t)*(uint32_t *)6509332UL : 0UL) + var_88) << 1UL) & 8589934590UL) + (uint64_t)((long)(rax_1 << 32UL) >> (long)32UL)) << 1UL) + (uint64_t)*(unsigned char *)6509327UL) << 1UL) + (uint64_t)*(unsigned char *)6509268UL) << 3UL) + 4359936UL);
                        *(uint64_t *)(local_sp_1 + (-8L)) = 4215041UL;
                        indirect_placeholder_20(rdi_0, var_89, rsi_0);
                        return;
                    }
                }
            }
            *(uint64_t *)var_51 = var_52;
            *(uint64_t *)(var_51 + 8UL) = (var_52 + 200UL);
            r10_0 = 2UL;
            rdx_0 = 2UL;
            *(uint64_t *)(var_51 + 16UL) = (var_52 + 400UL);
            r10_0 = 3UL;
            rdx_0 = 3UL;
            *(uint64_t *)(var_51 + 24UL) = (var_52 + 600UL);
            r10_0 = 4UL;
            rdx_0 = 4UL;
            *(uint64_t *)(var_51 + 32UL) = (var_52 + 800UL);
            r10_0 = 5UL;
            rdx_0 = var_50;
            if (var_50 != 1UL & var_50 != 2UL & var_50 != 3UL & var_50 != 4UL & var_50 == 6UL) {
                *(uint64_t *)(var_51 + 40UL) = (var_52 + 1000UL);
                r10_0 = 6UL;
                rdx_0 = 6UL;
            }
            r10_1 = r10_0;
            rdx_1 = rdx_0;
            if (var_50 != rdx_0) {
                var_88 = (uint32_t)r8_1;
                var_89 = *(uint64_t *)((((((((((uint64_t)((uint32_t)(((uint64_t)(var_88 + (-4)) == 0UL) ? (uint64_t)*(uint32_t *)6509332UL : 0UL) + var_88) << 1UL) & 8589934590UL) + (uint64_t)((long)(rax_1 << 32UL) >> (long)32UL)) << 1UL) + (uint64_t)*(unsigned char *)6509327UL) << 1UL) + (uint64_t)*(unsigned char *)6509268UL) << 3UL) + 4359936UL);
                *(uint64_t *)(local_sp_1 + (-8L)) = 4215041UL;
                indirect_placeholder_20(rdi_0, var_89, rsi_0);
                return;
            }
            var_54 = var_50 - rdx_1;
            var_55 = ((var_54 + (-2L)) >> 1UL) + 1UL;
            var_56 = var_55 << 1UL;
            r10_2 = r10_1;
            if (var_54 == 1UL) {
                *(uint64_t *)((r10_2 << 3UL) + var_51) = ((r10_2 * 200UL) + var_52);
            } else {
                var_57 = r10_1 + 1UL;
                var_58 = (uint64_t *)(local_sp_0 + 40UL);
                *var_58 = var_52;
                var_59 = (uint64_t *)(local_sp_0 + 32UL);
                *var_59 = r10_1;
                var_60 = *var_58;
                var_61 = (rdx_1 << 3UL) + var_51;
                var_62 = (uint64_t *)(local_sp_0 + 24UL);
                *var_62 = var_57;
                var_63 = *var_59;
                var_64 = helper_punpcklqdq_xmm_wrapper((struct type_4 *)(0UL), (struct type_6 *)(968UL), (struct type_6 *)(968UL), var_60);
                var_65 = var_64.field_0;
                var_66 = var_64.field_1;
                var_67 = *(uint64_t *)4376704UL;
                var_68 = *(uint64_t *)4376712UL;
                rdx_2 = var_61;
                state_0x85d8_0 = var_63;
                state_0x85e0_0 = *var_62;
                var_69 = rax_0 + 1UL;
                var_70 = helper_paddq_xmm_wrapper((struct type_4 *)(0UL), (struct type_6 *)(904UL), (struct type_6 *)(1032UL), state_0x85d8_0, state_0x85e0_0, var_67, var_68);
                var_71 = helper_psllq_xmm_wrapper((struct type_4 *)(0UL), (struct type_6 *)(840UL), (struct type_6 *)(2824UL), state_0x85d8_0, state_0x85e0_0, 3UL);
                var_72 = var_71.field_0;
                var_73 = var_71.field_1;
                var_74 = helper_psllq_xmm_wrapper_181((struct type_4 *)(0UL), (struct type_6 *)(776UL), (struct type_6 *)(2824UL), var_72, var_73, 2UL);
                var_75 = helper_paddq_xmm_wrapper_182((struct type_4 *)(0UL), (struct type_6 *)(776UL), (struct type_6 *)(840UL), var_74.field_0, var_74.field_1, var_72, var_73);
                var_76 = var_75.field_0;
                var_77 = var_75.field_1;
                var_78 = helper_psllq_xmm_wrapper((struct type_4 *)(0UL), (struct type_6 *)(840UL), (struct type_6 *)(2824UL), var_76, var_77, 2UL);
                var_79 = helper_paddq_xmm_wrapper_182((struct type_4 *)(0UL), (struct type_6 *)(776UL), (struct type_6 *)(840UL), var_76, var_77, var_78.field_0, var_78.field_1);
                var_80 = helper_paddq_xmm_wrapper_183((struct type_4 *)(0UL), (struct type_6 *)(776UL), (struct type_6 *)(968UL), var_79.field_0, var_79.field_1, var_65, var_66);
                var_81 = var_80.field_0;
                var_82 = var_80.field_1;
                *(uint64_t *)rdx_2 = var_81;
                *(uint64_t *)(rdx_2 + 8UL) = var_82;
                var_83 = helper_cc_compute_c_wrapper(var_69 - var_55, var_55, var_3, 17U);
                rax_0 = var_69;
                while (var_83 != 0UL)
                    {
                        rdx_2 = rdx_2 + 16UL;
                        state_0x85d8_0 = var_70.field_0;
                        state_0x85e0_0 = var_70.field_1;
                        var_69 = rax_0 + 1UL;
                        var_70 = helper_paddq_xmm_wrapper((struct type_4 *)(0UL), (struct type_6 *)(904UL), (struct type_6 *)(1032UL), state_0x85d8_0, state_0x85e0_0, var_67, var_68);
                        var_71 = helper_psllq_xmm_wrapper((struct type_4 *)(0UL), (struct type_6 *)(840UL), (struct type_6 *)(2824UL), state_0x85d8_0, state_0x85e0_0, 3UL);
                        var_72 = var_71.field_0;
                        var_73 = var_71.field_1;
                        var_74 = helper_psllq_xmm_wrapper_181((struct type_4 *)(0UL), (struct type_6 *)(776UL), (struct type_6 *)(2824UL), var_72, var_73, 2UL);
                        var_75 = helper_paddq_xmm_wrapper_182((struct type_4 *)(0UL), (struct type_6 *)(776UL), (struct type_6 *)(840UL), var_74.field_0, var_74.field_1, var_72, var_73);
                        var_76 = var_75.field_0;
                        var_77 = var_75.field_1;
                        var_78 = helper_psllq_xmm_wrapper((struct type_4 *)(0UL), (struct type_6 *)(840UL), (struct type_6 *)(2824UL), var_76, var_77, 2UL);
                        var_79 = helper_paddq_xmm_wrapper_182((struct type_4 *)(0UL), (struct type_6 *)(776UL), (struct type_6 *)(840UL), var_76, var_77, var_78.field_0, var_78.field_1);
                        var_80 = helper_paddq_xmm_wrapper_183((struct type_4 *)(0UL), (struct type_6 *)(776UL), (struct type_6 *)(968UL), var_79.field_0, var_79.field_1, var_65, var_66);
                        var_81 = var_80.field_0;
                        var_82 = var_80.field_1;
                        *(uint64_t *)rdx_2 = var_81;
                        *(uint64_t *)(rdx_2 + 8UL) = var_82;
                        var_83 = helper_cc_compute_c_wrapper(var_69 - var_55, var_55, var_3, 17U);
                        rax_0 = var_69;
                    }
                var_84 = r10_1 + var_56;
                r10_2 = var_84;
                if (var_56 == var_54) {
                    *(uint64_t *)((r10_2 << 3UL) + var_51) = ((r10_2 * 200UL) + var_52);
                }
            }
        }
    }
}

typedef unsigned char __u_char;
typedef unsigned short __u_short;
typedef unsigned int __u_int;
typedef unsigned long __u_long;
typedef signed char __int8_t;
typedef unsigned char __uint8_t;
typedef short __int16_t;
typedef unsigned short __uint16_t;
typedef int __int32_t;
typedef unsigned int __uint32_t;
typedef long __int64_t;
typedef unsigned long __uint64_t;
typedef __int8_t __int_least8_t;
typedef __uint8_t __uint_least8_t;
typedef __int16_t __int_least16_t;
typedef __uint16_t __uint_least16_t;
typedef __int32_t __int_least32_t;
typedef __uint32_t __uint_least32_t;
typedef __int64_t __int_least64_t;
typedef __uint64_t __uint_least64_t;
typedef long __quad_t;
typedef unsigned long __u_quad_t;
typedef long __intmax_t;
typedef unsigned long __uintmax_t;
typedef unsigned long __dev_t;
typedef unsigned int __uid_t;
typedef unsigned int __gid_t;
typedef unsigned long __ino_t;
typedef unsigned long __ino64_t;
typedef unsigned int __mode_t;
typedef unsigned long __nlink_t;
typedef long __off_t;
typedef long __off64_t;
typedef int __pid_t;
typedef struct {
    int __val[2];
} __fsid_t;
typedef long __clock_t;
typedef unsigned long __rlim_t;
typedef unsigned long __rlim64_t;
typedef unsigned int __id_t;
typedef long __time_t;
typedef unsigned int __useconds_t;
typedef long __suseconds_t;
typedef int __daddr_t;
typedef int __key_t;
typedef int __clockid_t;
typedef void *__timer_t;
typedef long __blksize_t;
typedef long __blkcnt_t;
typedef long __blkcnt64_t;
typedef unsigned long __fsblkcnt_t;
typedef unsigned long __fsblkcnt64_t;
typedef unsigned long __fsfilcnt_t;
typedef unsigned long __fsfilcnt64_t;
typedef long __fsword_t;
typedef long __ssize_t;
typedef long __syscall_slong_t;
typedef unsigned long __syscall_ulong_t;
typedef __off64_t __loff_t;
typedef char *__caddr_t;
typedef long __intptr_t;
typedef unsigned int __socklen_t;
typedef int __sig_atomic_t;
typedef __int8_t int8_t;
typedef __int16_t int16_t;
typedef __int32_t int32_t;
typedef __int64_t int64_t;
typedef __uint8_t uint8_t;
typedef __uint16_t uint16_t;
typedef __uint32_t uint32_t;
typedef __uint64_t uint64_t;
typedef __int_least8_t int_least8_t;
typedef __int_least16_t int_least16_t;
typedef __int_least32_t int_least32_t;
typedef __int_least64_t int_least64_t;
typedef __uint_least8_t uint_least8_t;
typedef __uint_least16_t uint_least16_t;
typedef __uint_least32_t uint_least32_t;
typedef __uint_least64_t uint_least64_t;
typedef signed char int_fast8_t;
typedef long int_fast16_t;
typedef long int_fast32_t;
typedef long int_fast64_t;
typedef unsigned char uint_fast8_t;
typedef unsigned long uint_fast16_t;
typedef unsigned long uint_fast32_t;
typedef unsigned long uint_fast64_t;
typedef long intptr_t;
typedef unsigned long uintptr_t;
typedef __intmax_t intmax_t;
typedef __uintmax_t uintmax_t;
typedef _Bool bool;
extern uint64_t revng_init_local_sp(uint64_t param_0);
extern void indirect_placeholder(void);
extern uint64_t init_r13(void);
extern uint64_t init_rbx(void);
extern uint64_t init_r12(void);
extern uint64_t init_rbp(void);
extern uint64_t init_rax(void);
void bb_signal_setup(uint64_t rdi) {
    uint64_t local_sp_3;
    uint64_t local_sp_1;
    uint64_t var_0;
    uint64_t var_1;
    uint64_t var_2;
    uint64_t var_3;
    uint64_t var_4;
    uint64_t var_5;
    uint64_t rax_1;
    uint64_t var_15;
    uint64_t var_16;
    uint64_t local_sp_2;
    uint64_t local_sp_0;
    uint64_t rbx_0;
    uint64_t rax_0;
    uint64_t var_17;
    uint64_t rbp_0;
    uint64_t var_9;
    uint64_t rbx_1_be;
    uint64_t var_10;
    uint64_t var_6;
    bool var_18;
    uint64_t var_19;
    uint64_t var_11;
    uint32_t var_12;
    uint64_t var_13;
    uint64_t *var_14;
    uint64_t var_7;
    uint64_t *var_8;
    uint64_t local_sp_4_be;
    uint64_t local_sp_4;
    uint64_t var_20;
    uint64_t rbx_1;
    uint64_t var_21;
    var_0 = revng_init_local_sp(0UL);
    var_1 = init_rax();
    var_2 = init_r13();
    var_3 = init_rbx();
    var_4 = init_r12();
    var_5 = init_rbp();
    *(uint64_t *)(var_0 + (-8L)) = var_2;
    *(uint64_t *)(var_0 + (-16L)) = var_4;
    *(uint64_t *)(var_0 + (-24L)) = var_5;
    *(uint64_t *)(var_0 + (-32L)) = var_3;
    rbx_0 = 4359776UL;
    rbp_0 = 4359776UL;
    rbx_1 = 4359776UL;
    if ((uint64_t)(unsigned char)rdi == 0UL) {
        var_18 = ((uint64_t)(uint32_t)var_1 == 0UL);
        local_sp_4 = var_0 + (-200L);
        while (1U)
            {
                var_19 = local_sp_4 + (-8L);
                *(uint64_t *)var_19 = 4216299UL;
                indirect_placeholder();
                local_sp_4_be = var_19;
                if (!var_18) {
                    var_20 = rbx_1 + 4UL;
                    var_21 = local_sp_4 + (-16L);
                    *(uint64_t *)var_21 = 4216316UL;
                    indirect_placeholder();
                    local_sp_4_be = var_21;
                    rbx_1_be = var_20;
                    if (var_20 == 4359824UL) {
                        break;
                    }
                }
                if (rbx_1 != 4359820UL) {
                    break;
                }
                rbx_1_be = rbx_1 + 4UL;
            }
    } else {
        var_6 = var_0 + (-208L);
        *(uint64_t *)var_6 = 4216363UL;
        indirect_placeholder();
        local_sp_3 = var_6;
        while (1U)
            {
                var_7 = local_sp_3 + (-8L);
                var_8 = (uint64_t *)var_7;
                *var_8 = 4216385UL;
                indirect_placeholder();
                local_sp_1 = var_7;
                if (*var_8 == 1UL) {
                    var_9 = local_sp_3 + (-16L);
                    *(uint64_t *)var_9 = 4216405UL;
                    indirect_placeholder();
                    local_sp_1 = var_9;
                }
                local_sp_2 = local_sp_1;
                local_sp_3 = local_sp_1;
                if (rbp_0 == 4359820UL) {
                    break;
                }
                rbp_0 = rbp_0 + 4UL;
                continue;
            }
        var_10 = *(uint64_t *)6509056UL;
        *(uint32_t *)(local_sp_1 + 136UL) = 268435456U;
        *(uint64_t *)(local_sp_1 + 8UL) = var_10;
        *(uint64_t *)(local_sp_1 + 16UL) = *(uint64_t *)6509064UL;
        *(uint64_t *)(local_sp_1 + 24UL) = *(uint64_t *)6509072UL;
        *(uint64_t *)(local_sp_1 + 32UL) = *(uint64_t *)6509080UL;
        *(uint64_t *)(local_sp_1 + 40UL) = *(uint64_t *)6509088UL;
        *(uint64_t *)(local_sp_1 + 48UL) = *(uint64_t *)6509096UL;
        *(uint64_t *)(local_sp_1 + 56UL) = *(uint64_t *)6509104UL;
        *(uint64_t *)(local_sp_1 + 64UL) = *(uint64_t *)6509112UL;
        *(uint64_t *)(local_sp_1 + 72UL) = *(uint64_t *)6509120UL;
        *(uint64_t *)(local_sp_1 + 80UL) = *(uint64_t *)6509128UL;
        *(uint64_t *)(local_sp_1 + 88UL) = *(uint64_t *)6509136UL;
        *(uint64_t *)(local_sp_1 + 96UL) = *(uint64_t *)6509144UL;
        *(uint64_t *)(local_sp_1 + 104UL) = *(uint64_t *)6509152UL;
        *(uint64_t *)(local_sp_1 + 112UL) = *(uint64_t *)6509160UL;
        *(uint64_t *)(local_sp_1 + 120UL) = *(uint64_t *)6509168UL;
        var_11 = *(uint64_t *)6509176UL;
        *(uint64_t *)(local_sp_1 + 128UL) = var_11;
        rax_1 = var_11;
        var_17 = rbx_0 + 4UL;
        local_sp_2 = local_sp_0;
        rax_1 = rax_0;
        rbx_0 = var_17;
        do {
            var_12 = *(uint32_t *)rbx_0;
            var_13 = local_sp_2 + (-8L);
            var_14 = (uint64_t *)var_13;
            *var_14 = 4216654UL;
            indirect_placeholder();
            local_sp_0 = var_13;
            rax_0 = rax_1;
            if ((uint64_t)(uint32_t)rax_1 == 0UL) {
                var_15 = (var_12 == 20U) ? 4215600UL : 4212144UL;
                *var_14 = var_15;
                var_16 = local_sp_2 + (-16L);
                *(uint64_t *)var_16 = 4216686UL;
                indirect_placeholder();
                local_sp_0 = var_16;
                rax_0 = var_15;
            }
            var_17 = rbx_0 + 4UL;
            local_sp_2 = local_sp_0;
            rax_1 = rax_0;
            rbx_0 = var_17;
        } while (var_17 != 4359824UL);
    }
    return;
}

typedef unsigned char __u_char;
typedef unsigned short __u_short;
typedef unsigned int __u_int;
typedef unsigned long __u_long;
typedef signed char __int8_t;
typedef unsigned char __uint8_t;
typedef short __int16_t;
typedef unsigned short __uint16_t;
typedef int __int32_t;
typedef unsigned int __uint32_t;
typedef long __int64_t;
typedef unsigned long __uint64_t;
typedef __int8_t __int_least8_t;
typedef __uint8_t __uint_least8_t;
typedef __int16_t __int_least16_t;
typedef __uint16_t __uint_least16_t;
typedef __int32_t __int_least32_t;
typedef __uint32_t __uint_least32_t;
typedef __int64_t __int_least64_t;
typedef __uint64_t __uint_least64_t;
typedef long __quad_t;
typedef unsigned long __u_quad_t;
typedef long __intmax_t;
typedef unsigned long __uintmax_t;
typedef unsigned long __dev_t;
typedef unsigned int __uid_t;
typedef unsigned int __gid_t;
typedef unsigned long __ino_t;
typedef unsigned long __ino64_t;
typedef unsigned int __mode_t;
typedef unsigned long __nlink_t;
typedef long __off_t;
typedef long __off64_t;
typedef int __pid_t;
typedef struct {
    int __val[2];
} __fsid_t;
typedef long __clock_t;
typedef unsigned long __rlim_t;
typedef unsigned long __rlim64_t;
typedef unsigned int __id_t;
typedef long __time_t;
typedef unsigned int __useconds_t;
typedef long __suseconds_t;
typedef int __daddr_t;
typedef int __key_t;
typedef int __clockid_t;
typedef void *__timer_t;
typedef long __blksize_t;
typedef long __blkcnt_t;
typedef long __blkcnt64_t;
typedef unsigned long __fsblkcnt_t;
typedef unsigned long __fsblkcnt64_t;
typedef unsigned long __fsfilcnt_t;
typedef unsigned long __fsfilcnt64_t;
typedef long __fsword_t;
typedef long __ssize_t;
typedef long __syscall_slong_t;
typedef unsigned long __syscall_ulong_t;
typedef __off64_t __loff_t;
typedef char *__caddr_t;
typedef long __intptr_t;
typedef unsigned int __socklen_t;
typedef int __sig_atomic_t;
typedef __int8_t int8_t;
typedef __int16_t int16_t;
typedef __int32_t int32_t;
typedef __int64_t int64_t;
typedef __uint8_t uint8_t;
typedef __uint16_t uint16_t;
typedef __uint32_t uint32_t;
typedef __uint64_t uint64_t;
typedef __int_least8_t int_least8_t;
typedef __int_least16_t int_least16_t;
typedef __int_least32_t int_least32_t;
typedef __int_least64_t int_least64_t;
typedef __uint_least8_t uint_least8_t;
typedef __uint_least16_t uint_least16_t;
typedef __uint_least32_t uint_least32_t;
typedef __uint_least64_t uint_least64_t;
typedef signed char int_fast8_t;
typedef long int_fast16_t;
typedef long int_fast32_t;
typedef long int_fast64_t;
typedef unsigned char uint_fast8_t;
typedef unsigned long uint_fast16_t;
typedef unsigned long uint_fast32_t;
typedef unsigned long uint_fast64_t;
typedef long intptr_t;
typedef unsigned long uintptr_t;
typedef __intmax_t intmax_t;
typedef __uintmax_t uintmax_t;
struct helper_divq_EAX_wrapper_ret_type;
struct type_5;
struct indirect_placeholder_4_ret_type;
struct indirect_placeholder_5_ret_type;
struct indirect_placeholder_6_ret_type;
struct helper_divq_EAX_wrapper_ret_type {
    uint64_t field_0;
    uint64_t field_1;
    uint32_t field_2;
    uint64_t field_3;
    uint64_t field_4;
    uint32_t field_5;
    uint64_t field_6;
    uint32_t field_7;
    uint64_t field_8;
    uint32_t field_9;
    unsigned char field_10;
    uint32_t field_11;
    uint32_t field_12;
};
struct type_5 {
};
struct indirect_placeholder_4_ret_type {
    uint64_t field_0;
    uint64_t field_1;
    uint64_t field_2;
};
struct indirect_placeholder_5_ret_type {
    uint64_t field_0;
    uint64_t field_1;
    uint64_t field_2;
    uint64_t field_3;
};
struct indirect_placeholder_6_ret_type {
    uint64_t field_0;
    uint64_t field_1;
    uint64_t field_2;
    uint64_t field_3;
};
extern void abort(void);
extern uint64_t revng_init_local_sp(uint64_t param_0);
extern uint64_t init_rbx(void);
extern uint64_t init_rbp(void);
extern uint64_t init_cc_src2(void);
extern uint64_t helper_cc_compute_c_wrapper(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint32_t param_3);
extern uint32_t init_state_0x8248(void);
extern uint64_t init_state_0x9018(void);
extern uint32_t init_state_0x9010(void);
extern uint64_t init_state_0x8408(void);
extern uint64_t init_state_0x8328(void);
extern uint64_t init_state_0x82d8(void);
extern uint32_t init_state_0x9080(void);
extern struct helper_divq_EAX_wrapper_ret_type helper_divq_EAX_wrapper(struct type_5 *param_0, uint64_t param_1, uint64_t param_2, uint64_t param_3, uint64_t param_4, uint64_t param_5, uint64_t param_6, uint64_t param_7, uint64_t param_8, uint64_t param_9, uint64_t param_10, uint64_t param_11, uint64_t param_12, uint32_t param_13, uint64_t param_14, uint32_t param_15, uint64_t param_16, uint64_t param_17, uint64_t param_18, uint32_t param_19);
extern void indirect_placeholder_2(uint64_t param_0, uint64_t param_1);
extern struct indirect_placeholder_4_ret_type indirect_placeholder_4(uint64_t param_0, uint64_t param_1);
extern struct indirect_placeholder_5_ret_type indirect_placeholder_5(uint64_t param_0, uint64_t param_1);
extern struct indirect_placeholder_6_ret_type indirect_placeholder_6(uint64_t param_0, uint64_t param_1);
void bb_init_column_info(uint64_t r9, uint64_t r8) {
    uint64_t r82_0;
    uint64_t var_0;
    uint64_t var_1;
    uint64_t var_2;
    uint64_t var_3;
    uint32_t var_4;
    uint64_t var_5;
    uint32_t var_6;
    uint64_t var_7;
    uint64_t var_8;
    uint64_t var_9;
    uint32_t var_10;
    uint64_t var_11;
    uint64_t var_12;
    uint64_t var_13;
    uint64_t var_14;
    uint64_t var_34;
    uint64_t rcx_0;
    uint64_t rdx_0_in;
    uint64_t rax_0;
    uint64_t rdx_0;
    uint64_t r10_0;
    uint64_t r91_0;
    uint64_t rbp_0;
    uint64_t local_sp_0;
    uint64_t storemerge;
    uint64_t var_25;
    uint64_t var_26;
    uint64_t var_27;
    uint64_t var_28;
    uint64_t r91_1;
    uint64_t var_30;
    struct helper_divq_EAX_wrapper_ret_type var_29;
    uint64_t var_31;
    struct indirect_placeholder_4_ret_type var_32;
    uint64_t var_33;
    uint64_t var_15;
    uint64_t var_16;
    uint64_t var_17;
    uint64_t var_22;
    uint64_t var_23;
    struct indirect_placeholder_5_ret_type var_24;
    uint64_t var_18;
    uint64_t var_19;
    uint64_t var_20;
    struct indirect_placeholder_6_ret_type var_21;
    uint64_t r82_1;
    uint64_t local_sp_1;
    uint64_t rdx_1;
    uint64_t rsi_0;
    uint64_t var_35;
    uint64_t var_36;
    uint64_t rax_1;
    uint64_t var_37;
    var_0 = revng_init_local_sp(0UL);
    var_1 = init_rbx();
    var_2 = init_rbp();
    var_3 = init_cc_src2();
    var_4 = init_state_0x8248();
    var_5 = init_state_0x9018();
    var_6 = init_state_0x9010();
    var_7 = init_state_0x8408();
    var_8 = init_state_0x8328();
    var_9 = init_state_0x82d8();
    var_10 = init_state_0x9080();
    *(uint64_t *)(var_0 + (-8L)) = var_2;
    *(uint64_t *)(var_0 + (-16L)) = var_1;
    var_11 = var_0 + (-24L);
    var_12 = *(uint64_t *)6509024UL;
    var_13 = *(uint64_t *)6509456UL;
    var_14 = (var_12 > var_13) ? var_13 : var_12;
    r91_1 = r9;
    r82_1 = r8;
    local_sp_1 = var_11;
    rdx_1 = 0UL;
    rax_1 = 0UL;
    if (var_14 <= *(uint64_t *)6505024UL) {
        var_15 = *(uint64_t *)6509032UL;
        var_16 = var_12 >> 1UL;
        var_17 = helper_cc_compute_c_wrapper(var_14 - var_16, var_16, var_3, 17U);
        if (var_17 == 0UL) {
            if (var_12 <= 384307168202282325UL) {
                *(uint64_t *)(local_sp_1 + (-8L)) = 4214302UL;
                indirect_placeholder_2(r91_1, r82_1);
                abort();
            }
            var_22 = var_12 * 24UL;
            var_23 = var_0 + (-32L);
            *(uint64_t *)var_23 = 4213998UL;
            var_24 = indirect_placeholder_5(var_15, var_22);
            r82_0 = var_24.field_3;
            r10_0 = var_24.field_1;
            r91_0 = var_24.field_2;
            rbp_0 = *(uint64_t *)6509024UL;
            local_sp_0 = var_23;
            storemerge = var_24.field_0;
        } else {
            if (var_14 <= 192153584101141162UL) {
                *(uint64_t *)(local_sp_1 + (-8L)) = 4214302UL;
                indirect_placeholder_2(r91_1, r82_1);
                abort();
            }
            var_18 = var_14 << 1UL;
            var_19 = var_14 * 48UL;
            var_20 = var_0 + (-32L);
            *(uint64_t *)var_20 = 4214285UL;
            var_21 = indirect_placeholder_6(var_15, var_19);
            r82_0 = var_21.field_3;
            r10_0 = var_21.field_1;
            r91_0 = var_21.field_2;
            rbp_0 = var_18;
            local_sp_0 = var_20;
            storemerge = var_21.field_0;
        }
        *(uint64_t *)6509032UL = storemerge;
        var_25 = *(uint64_t *)6505024UL;
        var_26 = (var_25 + rbp_0) + 1UL;
        var_27 = rbp_0 - var_25;
        var_28 = var_27 * var_26;
        r91_1 = r91_0;
        r82_1 = r82_0;
        local_sp_1 = local_sp_0;
        if (rbp_0 > var_26) {
            *(uint64_t *)(local_sp_1 + (-8L)) = 4214302UL;
            indirect_placeholder_2(r91_1, r82_1);
            abort();
        }
        var_29 = helper_divq_EAX_wrapper((struct type_5 *)(0UL), var_27, 4214051UL, var_28, var_14, r10_0, var_27, r91_0, rbp_0, var_28, 0UL, r82_0, var_26, var_4, var_5, var_6, var_7, var_8, var_9, var_10);
        var_30 = var_29.field_4;
        if ((var_29.field_1 != var_26) || (var_28 > 2305843009213693951UL)) {
            *(uint64_t *)(local_sp_1 + (-8L)) = 4214302UL;
            indirect_placeholder_2(r91_1, r82_1);
            abort();
        }
        var_31 = (var_28 >> 1UL) << 3UL;
        *(uint64_t *)(local_sp_0 + (-8L)) = 4214098UL;
        var_32 = indirect_placeholder_4(var_31, var_30);
        var_33 = *(uint64_t *)6505024UL;
        if (rbp_0 <= var_33) {
            var_34 = rbp_0 << 3UL;
            rcx_0 = (var_33 * 24UL) + *(uint64_t *)6509032UL;
            rdx_0_in = var_33 << 3UL;
            rax_0 = var_32.field_0;
            rdx_0 = rdx_0_in + 8UL;
            *(uint64_t *)(rcx_0 + 16UL) = rax_0;
            rdx_0_in = rdx_0;
            while (rdx_0 != var_34)
                {
                    rcx_0 = rcx_0 + 24UL;
                    rax_0 = rax_0 + rdx_0;
                    rdx_0 = rdx_0_in + 8UL;
                    *(uint64_t *)(rcx_0 + 16UL) = rax_0;
                    rdx_0_in = rdx_0;
                }
        }
        *(uint64_t *)6505024UL = rbp_0;
    }
    if (var_14 == 0UL) {
        return;
    }
    rsi_0 = *(uint64_t *)6509032UL;
    while (1U)
        {
            var_35 = rdx_1 + 1UL;
            var_36 = *(uint64_t *)(rsi_0 + 16UL);
            *(unsigned char *)rsi_0 = (unsigned char)'\x01';
            *(uint64_t *)(rsi_0 + 8UL) = (var_35 * 3UL);
            rdx_1 = var_35;
            *(uint64_t *)((rax_1 << 3UL) + var_36) = 3UL;
            var_37 = rax_1 + 1UL;
            rax_1 = var_37;
            do {
                *(uint64_t *)((rax_1 << 3UL) + var_36) = 3UL;
                var_37 = rax_1 + 1UL;
                rax_1 = var_37;
            } while (var_37 <= rdx_1);
            if (var_35 == var_14) {
                break;
            }
            rsi_0 = rsi_0 + 24UL;
            continue;
        }
    return;
}

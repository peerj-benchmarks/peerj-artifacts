typedef unsigned char __u_char;
typedef unsigned short __u_short;
typedef unsigned int __u_int;
typedef unsigned long __u_long;
typedef signed char __int8_t;
typedef unsigned char __uint8_t;
typedef short __int16_t;
typedef unsigned short __uint16_t;
typedef int __int32_t;
typedef unsigned int __uint32_t;
typedef long __int64_t;
typedef unsigned long __uint64_t;
typedef __int8_t __int_least8_t;
typedef __uint8_t __uint_least8_t;
typedef __int16_t __int_least16_t;
typedef __uint16_t __uint_least16_t;
typedef __int32_t __int_least32_t;
typedef __uint32_t __uint_least32_t;
typedef __int64_t __int_least64_t;
typedef __uint64_t __uint_least64_t;
typedef long __quad_t;
typedef unsigned long __u_quad_t;
typedef long __intmax_t;
typedef unsigned long __uintmax_t;
typedef unsigned long __dev_t;
typedef unsigned int __uid_t;
typedef unsigned int __gid_t;
typedef unsigned long __ino_t;
typedef unsigned long __ino64_t;
typedef unsigned int __mode_t;
typedef unsigned long __nlink_t;
typedef long __off_t;
typedef long __off64_t;
typedef int __pid_t;
typedef struct {
    int __val[2];
} __fsid_t;
typedef long __clock_t;
typedef unsigned long __rlim_t;
typedef unsigned long __rlim64_t;
typedef unsigned int __id_t;
typedef long __time_t;
typedef unsigned int __useconds_t;
typedef long __suseconds_t;
typedef int __daddr_t;
typedef int __key_t;
typedef int __clockid_t;
typedef void *__timer_t;
typedef long __blksize_t;
typedef long __blkcnt_t;
typedef long __blkcnt64_t;
typedef unsigned long __fsblkcnt_t;
typedef unsigned long __fsblkcnt64_t;
typedef unsigned long __fsfilcnt_t;
typedef unsigned long __fsfilcnt64_t;
typedef long __fsword_t;
typedef long __ssize_t;
typedef long __syscall_slong_t;
typedef unsigned long __syscall_ulong_t;
typedef __off64_t __loff_t;
typedef char *__caddr_t;
typedef long __intptr_t;
typedef unsigned int __socklen_t;
typedef int __sig_atomic_t;
typedef __int8_t int8_t;
typedef __int16_t int16_t;
typedef __int32_t int32_t;
typedef __int64_t int64_t;
typedef __uint8_t uint8_t;
typedef __uint16_t uint16_t;
typedef __uint32_t uint32_t;
typedef __uint64_t uint64_t;
typedef __int_least8_t int_least8_t;
typedef __int_least16_t int_least16_t;
typedef __int_least32_t int_least32_t;
typedef __int_least64_t int_least64_t;
typedef __uint_least8_t uint_least8_t;
typedef __uint_least16_t uint_least16_t;
typedef __uint_least32_t uint_least32_t;
typedef __uint_least64_t uint_least64_t;
typedef signed char int_fast8_t;
typedef long int_fast16_t;
typedef long int_fast32_t;
typedef long int_fast64_t;
typedef unsigned char uint_fast8_t;
typedef unsigned long uint_fast16_t;
typedef unsigned long uint_fast32_t;
typedef unsigned long uint_fast64_t;
typedef long intptr_t;
typedef unsigned long uintptr_t;
typedef __intmax_t intmax_t;
typedef __uintmax_t uintmax_t;
typedef _Bool bool;
struct helper_divq_EAX_wrapper_ret_type;
struct type_5;
struct indirect_placeholder_108_ret_type;
struct indirect_placeholder_109_ret_type;
struct indirect_placeholder_113_ret_type;
struct indirect_placeholder_112_ret_type;
struct indirect_placeholder_114_ret_type;
struct indirect_placeholder_111_ret_type;
struct indirect_placeholder_110_ret_type;
struct helper_divq_EAX_wrapper_ret_type {
    uint64_t field_0;
    uint64_t field_1;
    uint32_t field_2;
    uint64_t field_3;
    uint64_t field_4;
    uint32_t field_5;
    uint64_t field_6;
    uint32_t field_7;
    uint64_t field_8;
    uint32_t field_9;
    unsigned char field_10;
    uint32_t field_11;
    uint32_t field_12;
};
struct type_5 {
};
struct indirect_placeholder_108_ret_type {
    uint64_t field_0;
    uint64_t field_1;
    uint64_t field_2;
};
struct indirect_placeholder_109_ret_type {
    uint64_t field_0;
    uint64_t field_1;
    uint64_t field_2;
};
struct indirect_placeholder_113_ret_type {
    uint64_t field_0;
    uint64_t field_1;
};
struct indirect_placeholder_112_ret_type {
    uint64_t field_0;
    uint64_t field_1;
};
struct indirect_placeholder_114_ret_type {
    uint64_t field_0;
    uint64_t field_1;
};
struct indirect_placeholder_111_ret_type {
    uint64_t field_0;
    uint64_t field_1;
};
struct indirect_placeholder_110_ret_type {
    uint64_t field_0;
    uint64_t field_1;
};
extern void abort(void);
extern uint64_t revng_init_local_sp(uint64_t param_0);
extern void indirect_placeholder(void);
extern uint64_t init_r13(void);
extern uint64_t init_rbx(void);
extern uint64_t init_r12(void);
extern uint64_t init_rbp(void);
extern uint64_t init_r14(void);
extern uint64_t init_r15(void);
extern uint32_t init_state_0x8248(void);
extern uint64_t init_state_0x9018(void);
extern uint32_t init_state_0x9010(void);
extern uint64_t init_state_0x8408(void);
extern uint64_t init_state_0x8328(void);
extern uint64_t init_state_0x82d8(void);
extern uint32_t init_state_0x9080(void);
extern struct helper_divq_EAX_wrapper_ret_type helper_divq_EAX_wrapper(struct type_5 *param_0, uint64_t param_1, uint64_t param_2, uint64_t param_3, uint64_t param_4, uint64_t param_5, uint64_t param_6, uint64_t param_7, uint64_t param_8, uint64_t param_9, uint64_t param_10, uint64_t param_11, uint64_t param_12, uint32_t param_13, uint64_t param_14, uint32_t param_15, uint64_t param_16, uint64_t param_17, uint64_t param_18, uint32_t param_19);
extern uint64_t init_r10(void);
extern void indirect_placeholder_2(uint64_t param_0, uint64_t param_1);
extern uint64_t indirect_placeholder_19(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint64_t param_3, uint64_t param_4, uint64_t param_5);
extern struct indirect_placeholder_108_ret_type indirect_placeholder_108(uint64_t param_0, uint64_t param_1);
extern struct indirect_placeholder_109_ret_type indirect_placeholder_109(uint64_t param_0, uint64_t param_1);
extern struct indirect_placeholder_113_ret_type indirect_placeholder_113(uint64_t param_0);
extern struct indirect_placeholder_112_ret_type indirect_placeholder_112(uint64_t param_0);
extern struct indirect_placeholder_114_ret_type indirect_placeholder_114(uint64_t param_0);
extern struct indirect_placeholder_111_ret_type indirect_placeholder_111(uint64_t param_0);
extern struct indirect_placeholder_110_ret_type indirect_placeholder_110(uint64_t param_0);
uint64_t bb_quote_name(uint64_t rdi, uint64_t r9, uint64_t rcx, uint64_t rdx, uint64_t r8, uint64_t rsi) {
    uint64_t rsi6_1;
    uint64_t rbx_1_in;
    uint64_t rsi6_0;
    uint64_t storemerge3;
    uint64_t local_sp_6;
    uint64_t rbx_1;
    unsigned char var_46;
    bool var_47;
    uint64_t var_48;
    uint64_t var_0;
    uint64_t var_1;
    uint64_t var_2;
    uint64_t var_3;
    uint64_t var_4;
    uint64_t var_5;
    uint64_t var_6;
    uint64_t var_7;
    uint32_t var_8;
    uint64_t var_9;
    uint32_t var_10;
    uint64_t var_11;
    uint64_t var_12;
    uint64_t var_13;
    uint32_t var_14;
    uint64_t var_15;
    uint64_t var_16;
    uint64_t var_17;
    uint64_t var_18;
    uint64_t var_19;
    uint64_t var_20;
    uint64_t local_sp_9;
    uint64_t var_71;
    uint64_t *var_72;
    uint64_t *_cast1_pre_phi;
    uint64_t *var_65;
    uint64_t local_sp_0;
    uint64_t var_67;
    uint64_t var_66;
    uint64_t local_sp_16;
    uint64_t local_sp_1;
    uint64_t local_sp_3;
    uint64_t local_sp_2;
    uint64_t var_69;
    uint64_t *var_70;
    uint64_t r12_2;
    uint64_t rax_0;
    uint64_t local_sp_7;
    uint64_t var_73;
    uint64_t var_74;
    uint64_t r12_1;
    uint64_t r15_0_in;
    uint64_t local_sp_5;
    uint64_t r12_0;
    uint64_t local_sp_4;
    uint64_t r15_0;
    unsigned char var_58;
    uint64_t var_59;
    uint64_t var_60;
    uint64_t local_sp_14;
    uint64_t var_54;
    uint64_t var_55;
    uint64_t var_56;
    struct indirect_placeholder_108_ret_type var_57;
    uint64_t var_49;
    uint64_t var_50;
    uint64_t var_51;
    uint64_t var_52;
    struct helper_divq_EAX_wrapper_ret_type var_53;
    uint64_t var_39;
    uint64_t var_40;
    uint64_t var_41;
    struct indirect_placeholder_109_ret_type var_42;
    uint64_t var_43;
    uint64_t var_44;
    uint64_t var_45;
    uint64_t rax_1;
    uint64_t r92_0;
    uint64_t r85_0;
    uint64_t local_sp_8;
    uint64_t var_21;
    uint64_t rsi6_2;
    uint64_t var_22;
    uint64_t local_sp_10;
    uint64_t local_sp_11;
    uint64_t var_25;
    uint64_t var_23;
    uint64_t var_24;
    uint64_t var_26;
    struct indirect_placeholder_110_ret_type var_27;
    uint64_t local_sp_12;
    uint64_t var_28;
    uint64_t var_29;
    uint64_t local_sp_13;
    uint64_t var_30;
    uint64_t var_31;
    uint64_t var_32;
    uint32_t var_34;
    uint64_t var_35;
    uint32_t var_36;
    struct helper_divq_EAX_wrapper_ret_type var_33;
    uint64_t var_37;
    uint32_t var_38;
    uint64_t *var_61;
    uint64_t local_sp_15;
    uint64_t var_63;
    uint64_t var_62;
    uint64_t var_64;
    uint64_t var_68;
    var_0 = revng_init_local_sp(0UL);
    var_1 = init_r15();
    var_2 = init_r13();
    var_3 = init_rbx();
    var_4 = init_r10();
    var_5 = init_r12();
    var_6 = init_rbp();
    var_7 = init_r14();
    var_8 = init_state_0x8248();
    var_9 = init_state_0x9018();
    var_10 = init_state_0x9010();
    var_11 = init_state_0x8408();
    var_12 = init_state_0x8328();
    var_13 = init_state_0x82d8();
    var_14 = init_state_0x9080();
    *(uint64_t *)(var_0 + (-8L)) = var_1;
    *(uint64_t *)(var_0 + (-16L)) = var_7;
    *(uint64_t *)(var_0 + (-24L)) = var_2;
    *(uint64_t *)(var_0 + (-32L)) = var_5;
    *(uint64_t *)(var_0 + (-40L)) = var_6;
    *(uint64_t *)(var_0 + (-48L)) = var_3;
    var_15 = (uint64_t)(uint32_t)rdx;
    *(uint64_t *)(var_0 + (-1128L)) = rdi;
    var_16 = var_0 + (-1080L);
    var_17 = var_0 + (-1089L);
    var_18 = var_0 + (-1088L);
    *(uint64_t *)var_18 = var_16;
    var_19 = var_0 + (-1136L);
    *(uint64_t *)var_19 = 4226722UL;
    var_20 = indirect_placeholder_19(var_18, var_17, var_15, rsi, 0UL, rdi);
    rsi6_1 = 4366849UL;
    local_sp_9 = var_19;
    rax_0 = var_20;
    r92_0 = var_17;
    r85_0 = 0UL;
    rsi6_2 = rdi;
    if ((uint64_t)(unsigned char)r8 != 0UL & *(unsigned char *)(var_0 + (-1097L)) == '\x00') {
        var_21 = var_0 + (-1144L);
        *(uint64_t *)var_21 = 4227586UL;
        indirect_placeholder();
        *(uint64_t *)6509016UL = (*(uint64_t *)6509016UL + 1UL);
        local_sp_9 = var_21;
    }
    local_sp_10 = local_sp_9;
    local_sp_11 = local_sp_9;
    local_sp_12 = local_sp_9;
    if (rcx == 0UL) {
        rsi6_0 = rsi6_2;
        rax_1 = rax_0;
        local_sp_13 = local_sp_12;
        local_sp_14 = local_sp_12;
        if (*(uint64_t *)(local_sp_12 + 1136UL) == 0UL) {
            *(uint64_t *)(local_sp_12 + 16UL) = 0UL;
            *(uint64_t *)(local_sp_12 + 8UL) = 0UL;
            *(unsigned char *)(local_sp_12 + 31UL) = (unsigned char)'\x00';
        } else {
            if (*(unsigned char *)6509448UL == '\x00') {
                *(uint64_t *)(local_sp_12 + 16UL) = 0UL;
                *(uint64_t *)(local_sp_12 + 8UL) = 0UL;
                *(unsigned char *)(local_sp_12 + 31UL) = (unsigned char)'\x00';
            } else {
                if (*(unsigned char *)6509449UL == '\x00') {
                    *(uint64_t *)(local_sp_12 + 16UL) = 0UL;
                    *(uint64_t *)(local_sp_12 + 8UL) = 0UL;
                    *(unsigned char *)(local_sp_12 + 31UL) = (unsigned char)'\x00';
                } else {
                    if (*(unsigned char *)(local_sp_12 + 39UL) == '\x00') {
                        var_28 = *(uint64_t *)(local_sp_12 + 40UL);
                        var_29 = local_sp_12 + (-8L);
                        *(uint64_t *)var_29 = 4227517UL;
                        indirect_placeholder();
                        *(uint64_t *)(local_sp_12 + 8UL) = 1UL;
                        *(uint64_t *)local_sp_12 = 2UL;
                        *(unsigned char *)(local_sp_12 + 23UL) = (unsigned char)'\x01';
                        rax_1 = var_28;
                        local_sp_13 = var_29;
                    } else {
                        *(uint64_t *)(local_sp_12 + 16UL) = 0UL;
                        *(uint64_t *)(local_sp_12 + 8UL) = 0UL;
                        *(unsigned char *)(local_sp_12 + 31UL) = (unsigned char)'\x00';
                    }
                }
            }
            var_30 = *(uint64_t *)6509416UL;
            var_31 = local_sp_13 + (-8L);
            *(uint64_t *)var_31 = 4226899UL;
            indirect_placeholder();
            var_32 = rax_1 + 1UL;
            var_33 = helper_divq_EAX_wrapper((struct type_5 *)(0UL), var_32, 4226915UL, 9223372036854775807UL, var_30, var_4, var_30, var_17, r9, var_32, 0UL, 0UL, rsi6_2, var_8, var_9, var_10, var_11, var_12, var_13, var_14);
            var_34 = var_33.field_5;
            var_35 = var_33.field_6;
            var_36 = var_33.field_7;
            var_37 = var_33.field_8;
            var_38 = var_33.field_9;
            rbx_1_in = var_30;
            local_sp_8 = var_31;
            if (var_33.field_1 > 2UL) {
                *(uint64_t *)(local_sp_8 + (-8L)) = 4227795UL;
                indirect_placeholder_2(r92_0, r85_0);
                abort();
            }
            var_39 = var_33.field_4;
            var_40 = var_32 * 3UL;
            var_41 = local_sp_13 + (-16L);
            *(uint64_t *)var_41 = 4226941UL;
            var_42 = indirect_placeholder_109(var_40, var_39);
            var_43 = var_42.field_0;
            var_44 = var_42.field_1;
            var_45 = var_42.field_2;
            r12_2 = var_43;
            local_sp_6 = var_41;
            r92_0 = var_44;
            r85_0 = var_45;
            rbx_1 = rbx_1_in + 1UL;
            var_46 = *(unsigned char *)rbx_1_in;
            var_47 = (var_46 == '\x00');
            var_48 = (uint64_t)var_46;
            rbx_1_in = rbx_1;
            local_sp_7 = local_sp_6;
            storemerge3 = r12_2;
            while (!var_47)
                {
                    if (*(unsigned char *)(var_48 + 6508416UL) == '\x00') {
                        var_49 = local_sp_6 + (-8L);
                        *(uint64_t *)var_49 = 4227002UL;
                        indirect_placeholder();
                        local_sp_7 = var_49;
                    } else {
                        *(unsigned char *)r12_2 = var_46;
                        rsi6_1 = rsi6_0;
                        storemerge3 = r12_2 + 1UL;
                    }
                    r12_2 = storemerge3;
                    rsi6_0 = rsi6_1;
                    local_sp_6 = local_sp_7;
                    rbx_1 = rbx_1_in + 1UL;
                    var_46 = *(unsigned char *)rbx_1_in;
                    var_47 = (var_46 == '\x00');
                    var_48 = (uint64_t)var_46;
                    rbx_1_in = rbx_1;
                    local_sp_7 = local_sp_6;
                    storemerge3 = r12_2;
                }
            var_50 = *(uint64_t *)(local_sp_6 + 1136UL);
            *(unsigned char *)r12_2 = (unsigned char)'\x00';
            var_51 = local_sp_6 + (-8L);
            *(uint64_t *)var_51 = 4227034UL;
            indirect_placeholder();
            var_52 = var_48 + 1UL;
            var_53 = helper_divq_EAX_wrapper((struct type_5 *)(0UL), var_52, 4227050UL, 9223372036854775807UL, rbx_1, var_4, var_50, var_44, r9, var_52, 0UL, var_45, rsi6_0, var_34, var_35, var_36, var_11, var_12, var_37, var_38);
            local_sp_8 = var_51;
            if (var_53.field_1 > 2UL) {
                *(uint64_t *)(local_sp_8 + (-8L)) = 4227795UL;
                indirect_placeholder_2(r92_0, r85_0);
                abort();
            }
            var_54 = var_53.field_4;
            var_55 = var_52 * 3UL;
            var_56 = local_sp_6 + (-16L);
            *(uint64_t *)var_56 = 4227072UL;
            var_57 = indirect_placeholder_108(var_55, var_54);
            r15_0_in = *(uint64_t *)(local_sp_6 + 1120UL);
            r12_0 = var_57.field_0;
            local_sp_4 = var_56;
            r15_0 = r15_0_in + 1UL;
            var_58 = *(unsigned char *)r15_0_in;
            r15_0_in = r15_0;
            r12_1 = r12_0;
            local_sp_5 = local_sp_4;
            while (var_58 != '\x00')
                {
                    if ((uint64_t)(var_58 + '\xd1') == 0UL) {
                        *(unsigned char *)r12_0 = (unsigned char)'/';
                        r12_1 = r12_0 + 1UL;
                    } else {
                        if (*(unsigned char *)((uint64_t)var_58 + 6508416UL) == '\x00') {
                            var_59 = local_sp_4 + (-8L);
                            *(uint64_t *)var_59 = 4227167UL;
                            indirect_placeholder();
                            local_sp_5 = var_59;
                        } else {
                            *(unsigned char *)r12_0 = var_58;
                            r12_1 = r12_0 + 1UL;
                        }
                    }
                    r12_0 = r12_1;
                    local_sp_4 = local_sp_5;
                    r15_0 = r15_0_in + 1UL;
                    var_58 = *(unsigned char *)r15_0_in;
                    r15_0_in = r15_0;
                    r12_1 = r12_0;
                    local_sp_5 = local_sp_4;
                }
            *(unsigned char *)r12_0 = (unsigned char)'\x00';
            *(uint64_t *)(local_sp_4 + (-8L)) = 4227224UL;
            indirect_placeholder();
            *(uint64_t *)(local_sp_4 + (-16L)) = 4227232UL;
            indirect_placeholder();
            var_60 = local_sp_4 + (-24L);
            *(uint64_t *)var_60 = 4227240UL;
            indirect_placeholder();
            local_sp_14 = var_60;
        }
        local_sp_15 = local_sp_14;
        local_sp_16 = local_sp_14;
        if (r9 == 0UL) {
            var_68 = local_sp_14 + (-8L);
            *(uint64_t *)var_68 = 4227638UL;
            indirect_placeholder();
            *(uint64_t *)6509016UL = (*(uint64_t *)6509016UL + var_20);
            local_sp_1 = var_68;
        } else {
            if (*(unsigned char *)6509304UL != '\x00') {
                var_61 = (uint64_t *)(r9 + 24UL);
                if ((*(uint64_t *)(r9 + 32UL) - *var_61) <= 7UL) {
                    var_62 = local_sp_14 + (-8L);
                    *(uint64_t *)var_62 = 4227701UL;
                    indirect_placeholder_2(r9, 8UL);
                    local_sp_15 = var_62;
                }
                var_63 = local_sp_15 + (-8L);
                *(uint64_t *)var_63 = 4227294UL;
                indirect_placeholder();
                *var_61 = (*var_61 + 8UL);
                local_sp_16 = var_63;
            }
            var_64 = local_sp_16 + (-8L);
            *(uint64_t *)var_64 = 4227337UL;
            indirect_placeholder();
            *(uint64_t *)6509016UL = (*(uint64_t *)6509016UL + var_20);
            local_sp_0 = var_64;
            local_sp_1 = var_64;
            if (*(unsigned char *)6509304UL != '\x00') {
                var_65 = (uint64_t *)(r9 + 24UL);
                if ((*(uint64_t *)(r9 + 32UL) - *var_65) <= 7UL) {
                    var_66 = local_sp_16 + (-16L);
                    *(uint64_t *)var_66 = 4227725UL;
                    indirect_placeholder_2(r9, 8UL);
                    local_sp_0 = var_66;
                }
                var_67 = local_sp_0 + (-8L);
                *(uint64_t *)var_67 = 4227389UL;
                indirect_placeholder();
                *var_65 = (*var_65 + 8UL);
                local_sp_1 = var_67;
            }
        }
        local_sp_2 = local_sp_1;
        if (*(uint64_t *)(local_sp_1 + 1136UL) == 0UL) {
            _cast1_pre_phi = (uint64_t *)local_sp_1;
        } else {
            var_69 = local_sp_1 + (-8L);
            var_70 = (uint64_t *)var_69;
            *var_70 = 4227418UL;
            indirect_placeholder();
            _cast1_pre_phi = var_70;
            local_sp_2 = var_69;
            if (*(unsigned char *)(local_sp_1 + 23UL) == '\x00') {
                var_71 = local_sp_1 + (-16L);
                var_72 = (uint64_t *)var_71;
                *var_72 = 4227568UL;
                indirect_placeholder();
                _cast1_pre_phi = var_72;
                local_sp_2 = var_71;
            }
        }
        var_73 = *(uint64_t *)(local_sp_2 + 40UL);
        local_sp_3 = local_sp_2;
        if (!((*_cast1_pre_phi == var_73) || (var_73 == (local_sp_2 + 48UL)))) {
            var_74 = local_sp_2 + (-8L);
            *(uint64_t *)var_74 = 4227451UL;
            indirect_placeholder();
            local_sp_3 = var_74;
        }
        return var_20 + (uint64_t)*(unsigned char *)(local_sp_3 + 39UL);
    }
    *(uint64_t *)(local_sp_11 + (-8L)) = 4226815UL;
    indirect_placeholder_114(6504256UL);
    *(uint64_t *)(local_sp_11 + (-16L)) = 4226823UL;
    indirect_placeholder_111(rcx);
    var_26 = local_sp_11 + (-24L);
    *(uint64_t *)var_26 = 4226833UL;
    var_27 = indirect_placeholder_110(6504272UL);
    rsi6_2 = var_27.field_1;
    local_sp_12 = var_26;
}

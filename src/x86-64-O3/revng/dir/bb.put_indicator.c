typedef unsigned char __u_char;
typedef unsigned short __u_short;
typedef unsigned int __u_int;
typedef unsigned long __u_long;
typedef signed char __int8_t;
typedef unsigned char __uint8_t;
typedef short __int16_t;
typedef unsigned short __uint16_t;
typedef int __int32_t;
typedef unsigned int __uint32_t;
typedef long __int64_t;
typedef unsigned long __uint64_t;
typedef __int8_t __int_least8_t;
typedef __uint8_t __uint_least8_t;
typedef __int16_t __int_least16_t;
typedef __uint16_t __uint_least16_t;
typedef __int32_t __int_least32_t;
typedef __uint32_t __uint_least32_t;
typedef __int64_t __int_least64_t;
typedef __uint64_t __uint_least64_t;
typedef long __quad_t;
typedef unsigned long __u_quad_t;
typedef long __intmax_t;
typedef unsigned long __uintmax_t;
typedef unsigned long __dev_t;
typedef unsigned int __uid_t;
typedef unsigned int __gid_t;
typedef unsigned long __ino_t;
typedef unsigned long __ino64_t;
typedef unsigned int __mode_t;
typedef unsigned long __nlink_t;
typedef long __off_t;
typedef long __off64_t;
typedef int __pid_t;
typedef struct {
    int __val[2];
} __fsid_t;
typedef long __clock_t;
typedef unsigned long __rlim_t;
typedef unsigned long __rlim64_t;
typedef unsigned int __id_t;
typedef long __time_t;
typedef unsigned int __useconds_t;
typedef long __suseconds_t;
typedef int __daddr_t;
typedef int __key_t;
typedef int __clockid_t;
typedef void *__timer_t;
typedef long __blksize_t;
typedef long __blkcnt_t;
typedef long __blkcnt64_t;
typedef unsigned long __fsblkcnt_t;
typedef unsigned long __fsblkcnt64_t;
typedef unsigned long __fsfilcnt_t;
typedef unsigned long __fsfilcnt64_t;
typedef long __fsword_t;
typedef long __ssize_t;
typedef long __syscall_slong_t;
typedef unsigned long __syscall_ulong_t;
typedef __off64_t __loff_t;
typedef char *__caddr_t;
typedef long __intptr_t;
typedef unsigned int __socklen_t;
typedef int __sig_atomic_t;
typedef __int8_t int8_t;
typedef __int16_t int16_t;
typedef __int32_t int32_t;
typedef __int64_t int64_t;
typedef __uint8_t uint8_t;
typedef __uint16_t uint16_t;
typedef __uint32_t uint32_t;
typedef __uint64_t uint64_t;
typedef __int_least8_t int_least8_t;
typedef __int_least16_t int_least16_t;
typedef __int_least32_t int_least32_t;
typedef __int_least64_t int_least64_t;
typedef __uint_least8_t uint_least8_t;
typedef __uint_least16_t uint_least16_t;
typedef __uint_least32_t uint_least32_t;
typedef __uint_least64_t uint_least64_t;
typedef signed char int_fast8_t;
typedef long int_fast16_t;
typedef long int_fast32_t;
typedef long int_fast64_t;
typedef unsigned char uint_fast8_t;
typedef unsigned long uint_fast16_t;
typedef unsigned long uint_fast32_t;
typedef unsigned long uint_fast64_t;
typedef long intptr_t;
typedef unsigned long uintptr_t;
typedef __intmax_t intmax_t;
typedef __uintmax_t uintmax_t;
typedef _Bool bool;
struct bb_put_indicator_ret_type;
struct indirect_placeholder_38_ret_type;
struct indirect_placeholder_40_ret_type;
struct indirect_placeholder_41_ret_type;
struct indirect_placeholder_37_ret_type;
struct indirect_placeholder_36_ret_type;
struct indirect_placeholder_42_ret_type;
struct indirect_placeholder_39_ret_type;
struct bb_put_indicator_ret_type {
    uint64_t field_0;
    uint64_t field_1;
};
struct indirect_placeholder_38_ret_type {
    uint64_t field_0;
    uint64_t field_1;
};
struct indirect_placeholder_40_ret_type {
    uint64_t field_0;
    uint64_t field_1;
};
struct indirect_placeholder_41_ret_type {
    uint64_t field_0;
    uint64_t field_1;
};
struct indirect_placeholder_37_ret_type {
    uint64_t field_0;
    uint64_t field_1;
};
struct indirect_placeholder_36_ret_type {
    uint64_t field_0;
    uint64_t field_1;
};
struct indirect_placeholder_42_ret_type {
    uint64_t field_0;
    uint64_t field_1;
};
struct indirect_placeholder_39_ret_type {
    uint64_t field_0;
    uint64_t field_1;
};
extern struct indirect_placeholder_38_ret_type indirect_placeholder_38(uint64_t param_0);
extern uint64_t revng_init_local_sp(uint64_t param_0);
extern void indirect_placeholder(void);
extern uint64_t init_rbx(void);
extern uint64_t init_rbp(void);
extern uint64_t init_rax(void);
extern void indirect_placeholder_12(uint64_t param_0);
extern struct indirect_placeholder_40_ret_type indirect_placeholder_40(uint64_t param_0);
extern struct indirect_placeholder_41_ret_type indirect_placeholder_41(uint64_t param_0);
extern struct indirect_placeholder_37_ret_type indirect_placeholder_37(uint64_t param_0);
extern struct indirect_placeholder_36_ret_type indirect_placeholder_36(uint64_t param_0);
extern struct indirect_placeholder_42_ret_type indirect_placeholder_42(uint64_t param_0);
extern struct indirect_placeholder_39_ret_type indirect_placeholder_39(uint64_t param_0);
struct bb_put_indicator_ret_type bb_put_indicator(uint64_t rdi, uint64_t rdx) {
    uint64_t var_13;
    uint64_t var_14;
    uint64_t var_6;
    uint64_t var_0;
    uint64_t var_1;
    uint64_t var_2;
    uint64_t var_3;
    uint64_t var_8;
    uint64_t local_sp_0;
    bool var_9;
    uint64_t var_10;
    uint64_t *var_11;
    uint64_t local_sp_2;
    uint64_t var_12;
    uint64_t local_sp_1;
    uint64_t var_7;
    struct bb_put_indicator_ret_type mrv;
    struct bb_put_indicator_ret_type mrv1;
    uint64_t var_4;
    bool var_5;
    var_0 = revng_init_local_sp(0UL);
    var_1 = init_rax();
    var_2 = init_rbx();
    var_3 = init_rbp();
    *(uint64_t *)(var_0 + (-8L)) = var_3;
    *(uint64_t *)(var_0 + (-16L)) = var_2;
    if (*(unsigned char *)6509296UL == '\x00') {
        var_13 = *(uint64_t *)(rdi + 8UL);
        var_14 = *(uint64_t *)rdi;
        indirect_placeholder();
        mrv.field_0 = var_13;
        mrv1 = mrv;
        mrv1.field_1 = var_14;
        return mrv1;
    }
    *(unsigned char *)6509296UL = (unsigned char)'\x01';
    var_4 = var_0 + (-32L);
    *(uint64_t *)var_4 = 4226433UL;
    indirect_placeholder();
    var_5 = ((int)(uint32_t)var_1 < (int)0U);
    local_sp_1 = var_4;
    if (var_5) {
        var_6 = var_0 + (-40L);
        *(uint64_t *)var_6 = 4226447UL;
        indirect_placeholder_12(1UL);
        local_sp_1 = var_6;
    }
    local_sp_2 = local_sp_1;
    if (*(uint64_t *)6504296UL == 0UL) {
        *(uint64_t *)(local_sp_1 + (-8L)) = 4226578UL;
        indirect_placeholder_42(6504256UL);
        *(uint64_t *)(local_sp_1 + (-16L)) = 4226588UL;
        indirect_placeholder_39(6504304UL);
        *(uint64_t *)(local_sp_1 + (-24L)) = 4226598UL;
        indirect_placeholder_38(6504272UL);
    } else {
        if (*(unsigned char *)6509296UL != '\x00') {
            *(unsigned char *)6509296UL = (unsigned char)'\x01';
            var_7 = local_sp_1 + (-8L);
            *(uint64_t *)var_7 = 4226521UL;
            indirect_placeholder();
            local_sp_0 = var_7;
            if (var_5) {
                var_8 = local_sp_1 + (-16L);
                *(uint64_t *)var_8 = 4226535UL;
                indirect_placeholder_12(1UL);
                local_sp_0 = var_8;
            }
            var_9 = (*(uint64_t *)6504296UL == 0UL);
            var_10 = local_sp_0 + (-8L);
            var_11 = (uint64_t *)var_10;
            local_sp_2 = var_10;
            if (var_9) {
                *var_11 = 4226618UL;
                indirect_placeholder_41(6504256UL);
                *(uint64_t *)(local_sp_0 + (-16L)) = 4226628UL;
                indirect_placeholder_37(6504304UL);
                var_12 = local_sp_0 + (-24L);
                *(uint64_t *)var_12 = 4226638UL;
                indirect_placeholder_36(6504272UL);
                local_sp_2 = var_12;
            } else {
                *var_11 = 4226555UL;
                indirect_placeholder_40(6504288UL);
            }
        }
        *(uint64_t *)(local_sp_2 + (-8L)) = 4226495UL;
        indirect_placeholder();
    }
    var_13 = *(uint64_t *)(rdi + 8UL);
    var_14 = *(uint64_t *)rdi;
    indirect_placeholder();
    mrv.field_0 = var_13;
    mrv1 = mrv;
    mrv1.field_1 = var_14;
    return mrv1;
}

typedef unsigned char __u_char;
typedef unsigned short __u_short;
typedef unsigned int __u_int;
typedef unsigned long __u_long;
typedef signed char __int8_t;
typedef unsigned char __uint8_t;
typedef short __int16_t;
typedef unsigned short __uint16_t;
typedef int __int32_t;
typedef unsigned int __uint32_t;
typedef long __int64_t;
typedef unsigned long __uint64_t;
typedef __int8_t __int_least8_t;
typedef __uint8_t __uint_least8_t;
typedef __int16_t __int_least16_t;
typedef __uint16_t __uint_least16_t;
typedef __int32_t __int_least32_t;
typedef __uint32_t __uint_least32_t;
typedef __int64_t __int_least64_t;
typedef __uint64_t __uint_least64_t;
typedef long __quad_t;
typedef unsigned long __u_quad_t;
typedef long __intmax_t;
typedef unsigned long __uintmax_t;
typedef unsigned long __dev_t;
typedef unsigned int __uid_t;
typedef unsigned int __gid_t;
typedef unsigned long __ino_t;
typedef unsigned long __ino64_t;
typedef unsigned int __mode_t;
typedef unsigned long __nlink_t;
typedef long __off_t;
typedef long __off64_t;
typedef int __pid_t;
typedef struct {
    int __val[2];
} __fsid_t;
typedef long __clock_t;
typedef unsigned long __rlim_t;
typedef unsigned long __rlim64_t;
typedef unsigned int __id_t;
typedef long __time_t;
typedef unsigned int __useconds_t;
typedef long __suseconds_t;
typedef int __daddr_t;
typedef int __key_t;
typedef int __clockid_t;
typedef void *__timer_t;
typedef long __blksize_t;
typedef long __blkcnt_t;
typedef long __blkcnt64_t;
typedef unsigned long __fsblkcnt_t;
typedef unsigned long __fsblkcnt64_t;
typedef unsigned long __fsfilcnt_t;
typedef unsigned long __fsfilcnt64_t;
typedef long __fsword_t;
typedef long __ssize_t;
typedef long __syscall_slong_t;
typedef unsigned long __syscall_ulong_t;
typedef __off64_t __loff_t;
typedef char *__caddr_t;
typedef long __intptr_t;
typedef unsigned int __socklen_t;
typedef int __sig_atomic_t;
typedef __int8_t int8_t;
typedef __int16_t int16_t;
typedef __int32_t int32_t;
typedef __int64_t int64_t;
typedef __uint8_t uint8_t;
typedef __uint16_t uint16_t;
typedef __uint32_t uint32_t;
typedef __uint64_t uint64_t;
typedef __int_least8_t int_least8_t;
typedef __int_least16_t int_least16_t;
typedef __int_least32_t int_least32_t;
typedef __int_least64_t int_least64_t;
typedef __uint_least8_t uint_least8_t;
typedef __uint_least16_t uint_least16_t;
typedef __uint_least32_t uint_least32_t;
typedef __uint_least64_t uint_least64_t;
typedef signed char int_fast8_t;
typedef long int_fast16_t;
typedef long int_fast32_t;
typedef long int_fast64_t;
typedef unsigned char uint_fast8_t;
typedef unsigned long uint_fast16_t;
typedef unsigned long uint_fast32_t;
typedef unsigned long uint_fast64_t;
typedef long intptr_t;
typedef unsigned long uintptr_t;
typedef __intmax_t intmax_t;
typedef __uintmax_t uintmax_t;
extern uint64_t revng_init_local_sp(uint64_t param_0);
extern void indirect_placeholder(void);
extern uint64_t init_r13(void);
extern uint64_t init_rbx(void);
extern uint64_t init_r12(void);
extern uint64_t init_rbp(void);
extern uint64_t init_r14(void);
extern uint64_t init_rax(void);
extern uint64_t init_cc_src2(void);
extern uint64_t helper_cc_compute_all_wrapper(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint32_t param_3);
extern uint64_t init_r15(void);
extern void indirect_placeholder_7(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint64_t param_3);
void bb_mpsort_with_tmp(uint64_t rdi, uint64_t rcx, uint64_t rdx, uint64_t rsi) {
    uint64_t var_0;
    uint64_t var_1;
    uint64_t var_2;
    uint64_t var_3;
    uint64_t var_4;
    uint64_t var_5;
    uint64_t var_6;
    uint64_t var_7;
    uint64_t var_8;
    uint64_t *var_9;
    uint64_t r14_3;
    uint64_t *var_49;
    uint64_t r13_3;
    uint64_t local_sp_2;
    uint64_t var_50;
    uint64_t r15_1;
    uint64_t _pre_phi;
    uint64_t var_51;
    uint64_t var_52;
    uint64_t r12_4;
    uint64_t rax_0;
    uint64_t r13_0;
    uint64_t r12_0;
    uint64_t r14_0;
    uint64_t rax_3;
    uint64_t r14_2;
    uint64_t *var_34;
    uint64_t r13_2;
    uint64_t local_sp_0;
    uint64_t var_35;
    uint64_t *var_36;
    uint64_t var_37;
    uint64_t var_38;
    uint64_t var_46;
    uint64_t *var_47;
    uint64_t r12_2;
    uint64_t var_39;
    uint64_t var_40;
    uint64_t var_41;
    uint64_t rax_1;
    uint64_t r13_1;
    uint64_t r12_1;
    uint64_t r14_1;
    uint64_t var_42;
    uint64_t r12_3;
    uint64_t rax_2;
    uint64_t r15_0_in;
    uint64_t r15_0;
    uint64_t var_32;
    uint64_t var_33;
    uint64_t var_22;
    uint64_t var_23;
    uint64_t var_24;
    uint64_t var_25;
    uint64_t var_26;
    uint64_t var_27;
    uint64_t *var_28;
    uint64_t var_29;
    uint64_t var_30;
    uint64_t var_31;
    uint64_t var_43;
    uint64_t local_sp_1;
    uint64_t var_44;
    uint64_t var_45;
    uint64_t var_48;
    uint64_t var_10;
    uint64_t *var_11;
    uint64_t var_12;
    uint64_t *_cast;
    uint64_t var_13;
    uint64_t var_14;
    uint64_t var_15;
    uint64_t var_16;
    uint64_t *var_17;
    uint64_t var_18;
    uint64_t var_19;
    uint64_t var_20;
    uint64_t *var_21;
    var_0 = revng_init_local_sp(0UL);
    var_1 = init_rax();
    var_2 = init_r15();
    var_3 = init_r13();
    var_4 = init_rbx();
    var_5 = init_r12();
    var_6 = init_rbp();
    var_7 = init_cc_src2();
    var_8 = init_r14();
    *(uint64_t *)(var_0 + (-8L)) = var_2;
    *(uint64_t *)(var_0 + (-16L)) = var_8;
    *(uint64_t *)(var_0 + (-24L)) = var_3;
    *(uint64_t *)(var_0 + (-32L)) = var_5;
    *(uint64_t *)(var_0 + (-40L)) = var_6;
    *(uint64_t *)(var_0 + (-48L)) = var_4;
    *(uint64_t *)(var_0 + (-104L)) = rsi;
    var_9 = (uint64_t *)(var_0 + (-80L));
    *var_9 = rdx;
    r15_1 = 1UL;
    if (rsi <= 2UL) {
        var_10 = helper_cc_compute_all_wrapper(rsi + (-2L), 2UL, var_7, 17U);
        if ((var_10 & 64UL) == 0UL) {
            return;
        }
        var_11 = (uint64_t *)(rdi + 8UL);
        var_12 = *var_11;
        _cast = (uint64_t *)rdi;
        var_13 = *_cast;
        *(uint64_t *)(var_0 + (-112L)) = 4262735UL;
        indirect_placeholder();
        var_14 = helper_cc_compute_all_wrapper(var_1, 0UL, 0UL, 24U);
        if ((uint64_t)(((unsigned char)(var_14 >> 4UL) ^ (unsigned char)var_14) & '\xc0') != 0UL) {
            return;
        }
        *_cast = var_12;
        *var_11 = var_13;
        return;
    }
    var_15 = rsi >> 1UL;
    var_16 = var_0 + (-88L);
    var_17 = (uint64_t *)var_16;
    *var_17 = var_15;
    var_18 = (var_15 << 3UL) + rdi;
    var_19 = rsi - var_15;
    *(uint64_t *)(var_0 + (-64L)) = var_18;
    var_20 = var_0 + (-112L);
    var_21 = (uint64_t *)var_20;
    *var_21 = 4262309UL;
    indirect_placeholder_7(var_18, rcx, rdx, var_19);
    local_sp_1 = var_20;
    if (var_15 == 1UL) {
        var_43 = *(uint64_t *)rdi;
        **(uint64_t **)var_16 = var_43;
        r12_3 = var_43;
    } else {
        var_22 = *var_21;
        var_23 = *var_17;
        var_24 = *(uint64_t *)(var_0 + (-96L));
        var_25 = var_22 >> 2UL;
        var_26 = (var_25 << 3UL) + rdi;
        var_27 = var_24 - var_25;
        *var_9 = var_25;
        var_28 = (uint64_t *)(var_0 + (-120L));
        *var_28 = 4262366UL;
        indirect_placeholder_7(var_26, rcx, var_23, var_27);
        var_29 = var_0 + (-128L);
        *(uint64_t *)var_29 = 4262387UL;
        indirect_placeholder_7(rdi, rcx, var_23, var_25);
        var_30 = *(uint64_t *)rdi;
        var_31 = *(uint64_t *)var_26;
        *var_28 = 0UL;
        r14_2 = var_25;
        r13_2 = var_31;
        local_sp_0 = var_29;
        r12_2 = var_30;
        rax_2 = var_25;
        r15_0_in = var_23;
        while (1U)
            {
                r15_0 = r15_0_in + 8UL;
                var_32 = local_sp_0 + (-8L);
                *(uint64_t *)var_32 = 4262443UL;
                indirect_placeholder();
                var_33 = helper_cc_compute_all_wrapper(rax_2, 0UL, 0UL, 24U);
                local_sp_0 = var_32;
                rax_1 = rax_2;
                r13_1 = r13_2;
                r12_1 = r12_2;
                r14_1 = r14_2;
                r15_0_in = r15_0;
                if ((uint64_t)(((unsigned char)(var_33 >> 4UL) ^ (unsigned char)var_33) & '\xc0') == 0UL) {
                    var_39 = r14_2 + 1UL;
                    var_40 = local_sp_0 + 8UL;
                    var_41 = *(uint64_t *)var_40;
                    *(uint64_t *)r15_0_in = r13_2;
                    r14_1 = var_39;
                    _pre_phi = var_40;
                    if (var_41 != var_39) {
                        break;
                    }
                    r13_1 = *(uint64_t *)((var_39 << 3UL) + rdi);
                } else {
                    var_34 = (uint64_t *)local_sp_0;
                    *var_34 = (*var_34 + 1UL);
                    *(uint64_t *)r15_0_in = r12_2;
                    var_35 = *var_34;
                    var_36 = (uint64_t *)(local_sp_0 + 24UL);
                    rax_1 = var_35;
                    if (*var_36 != var_35) {
                        var_37 = local_sp_0 + 8UL;
                        var_38 = *(uint64_t *)var_37;
                        *var_34 = r14_2;
                        *var_36 = var_38;
                        _pre_phi = var_37;
                        break;
                    }
                    r12_1 = *(uint64_t *)((var_35 << 3UL) + rdi);
                }
                rax_2 = rax_1;
                r13_2 = r13_1;
                r12_2 = r12_1;
                r14_2 = r14_1;
                continue;
            }
        var_42 = local_sp_0 + (-16L);
        *(uint64_t *)var_42 = 4262705UL;
        indirect_placeholder();
        r12_3 = **(uint64_t **)_pre_phi;
        local_sp_1 = var_42;
    }
    var_44 = *(uint64_t *)(local_sp_1 + 40UL);
    var_45 = *(uint64_t *)(local_sp_1 + 16UL);
    *(uint64_t *)(local_sp_1 + 8UL) = 0UL;
    r14_3 = var_45;
    r13_3 = *(uint64_t *)var_44;
    local_sp_2 = local_sp_1;
    r12_4 = r12_3;
    rax_3 = var_44;
    while (1U)
        {
            var_46 = local_sp_2 + (-8L);
            var_47 = (uint64_t *)var_46;
            *var_47 = 4262559UL;
            indirect_placeholder();
            var_48 = helper_cc_compute_all_wrapper(rax_3, 0UL, 0UL, 24U);
            local_sp_2 = var_46;
            rax_0 = rax_3;
            r13_0 = r13_3;
            r12_0 = r12_4;
            r14_0 = r14_3;
            if ((uint64_t)(((unsigned char)(var_48 >> 4UL) ^ (unsigned char)var_48) & '\xc0') == 0UL) {
                var_51 = r14_3 + 1UL;
                var_52 = *var_47;
                *(uint64_t *)(((r15_1 << 3UL) + rdi) + (-8L)) = r13_3;
                r14_0 = var_51;
                if (var_52 != var_51) {
                    indirect_placeholder();
                    return;
                }
                r13_0 = *(uint64_t *)((var_51 << 3UL) + rdi);
            } else {
                var_49 = (uint64_t *)local_sp_2;
                *var_49 = (*var_49 + 1UL);
                *(uint64_t *)(((r15_1 << 3UL) + rdi) + (-8L)) = r12_4;
                var_50 = *var_49;
                rax_0 = var_50;
                if (*(uint64_t *)(local_sp_2 + 8UL) != var_50) {
                    break;
                }
                r12_0 = *(uint64_t *)((var_50 << 3UL) + *(uint64_t *)(local_sp_2 + 16UL));
            }
            r14_3 = r14_0;
            r13_3 = r13_0;
            r15_1 = r15_1 + 1UL;
            r12_4 = r12_0;
            rax_3 = rax_0;
            continue;
        }
    return;
}

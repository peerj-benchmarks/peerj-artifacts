typedef unsigned char __u_char;
typedef unsigned short __u_short;
typedef unsigned int __u_int;
typedef unsigned long __u_long;
typedef signed char __int8_t;
typedef unsigned char __uint8_t;
typedef short __int16_t;
typedef unsigned short __uint16_t;
typedef int __int32_t;
typedef unsigned int __uint32_t;
typedef long __int64_t;
typedef unsigned long __uint64_t;
typedef __int8_t __int_least8_t;
typedef __uint8_t __uint_least8_t;
typedef __int16_t __int_least16_t;
typedef __uint16_t __uint_least16_t;
typedef __int32_t __int_least32_t;
typedef __uint32_t __uint_least32_t;
typedef __int64_t __int_least64_t;
typedef __uint64_t __uint_least64_t;
typedef long __quad_t;
typedef unsigned long __u_quad_t;
typedef long __intmax_t;
typedef unsigned long __uintmax_t;
typedef unsigned long __dev_t;
typedef unsigned int __uid_t;
typedef unsigned int __gid_t;
typedef unsigned long __ino_t;
typedef unsigned long __ino64_t;
typedef unsigned int __mode_t;
typedef unsigned long __nlink_t;
typedef long __off_t;
typedef long __off64_t;
typedef int __pid_t;
typedef struct {
    int __val[2];
} __fsid_t;
typedef long __clock_t;
typedef unsigned long __rlim_t;
typedef unsigned long __rlim64_t;
typedef unsigned int __id_t;
typedef long __time_t;
typedef unsigned int __useconds_t;
typedef long __suseconds_t;
typedef int __daddr_t;
typedef int __key_t;
typedef int __clockid_t;
typedef void *__timer_t;
typedef long __blksize_t;
typedef long __blkcnt_t;
typedef long __blkcnt64_t;
typedef unsigned long __fsblkcnt_t;
typedef unsigned long __fsblkcnt64_t;
typedef unsigned long __fsfilcnt_t;
typedef unsigned long __fsfilcnt64_t;
typedef long __fsword_t;
typedef long __ssize_t;
typedef long __syscall_slong_t;
typedef unsigned long __syscall_ulong_t;
typedef __off64_t __loff_t;
typedef char *__caddr_t;
typedef long __intptr_t;
typedef unsigned int __socklen_t;
typedef int __sig_atomic_t;
typedef __int8_t int8_t;
typedef __int16_t int16_t;
typedef __int32_t int32_t;
typedef __int64_t int64_t;
typedef __uint8_t uint8_t;
typedef __uint16_t uint16_t;
typedef __uint32_t uint32_t;
typedef __uint64_t uint64_t;
typedef __int_least8_t int_least8_t;
typedef __int_least16_t int_least16_t;
typedef __int_least32_t int_least32_t;
typedef __int_least64_t int_least64_t;
typedef __uint_least8_t uint_least8_t;
typedef __uint_least16_t uint_least16_t;
typedef __uint_least32_t uint_least32_t;
typedef __uint_least64_t uint_least64_t;
typedef signed char int_fast8_t;
typedef long int_fast16_t;
typedef long int_fast32_t;
typedef long int_fast64_t;
typedef unsigned char uint_fast8_t;
typedef unsigned long uint_fast16_t;
typedef unsigned long uint_fast32_t;
typedef unsigned long uint_fast64_t;
typedef long intptr_t;
typedef unsigned long uintptr_t;
typedef __intmax_t intmax_t;
typedef __uintmax_t uintmax_t;
struct indirect_placeholder_30_ret_type;
struct indirect_placeholder_31_ret_type;
struct indirect_placeholder_32_ret_type;
struct indirect_placeholder_30_ret_type {
    uint64_t field_0;
    uint64_t field_1;
    uint64_t field_2;
};
struct indirect_placeholder_31_ret_type {
    uint64_t field_0;
    uint64_t field_1;
    uint64_t field_2;
};
struct indirect_placeholder_32_ret_type {
    uint64_t field_0;
    uint64_t field_1;
    uint64_t field_2;
};
extern uint64_t revng_init_local_sp(uint64_t param_0);
extern void indirect_placeholder(void);
extern uint64_t init_r13(void);
extern uint64_t init_rbx(void);
extern uint64_t init_r12(void);
extern uint64_t init_rbp(void);
extern uint64_t init_r14(void);
extern uint64_t init_cc_src2(void);
extern uint64_t helper_cc_compute_c_wrapper(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint32_t param_3);
extern uint64_t init_r15(void);
extern struct indirect_placeholder_30_ret_type indirect_placeholder_30(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint64_t param_3, uint64_t param_4, uint64_t param_5, uint64_t param_6);
extern struct indirect_placeholder_31_ret_type indirect_placeholder_31(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint64_t param_3, uint64_t param_4, uint64_t param_5, uint64_t param_6);
extern struct indirect_placeholder_32_ret_type indirect_placeholder_32(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint64_t param_3, uint64_t param_4, uint64_t param_5);
typedef _Bool bool;
void bb_abformat_init(void) {
    uint64_t rax_1;
    uint64_t var_24;
    uint64_t var_25;
    uint64_t var_26;
    uint64_t var_27;
    uint64_t r14_0;
    uint64_t var_28;
    uint64_t var_29;
    uint64_t var_31;
    uint64_t var_32;
    uint64_t rbp_0;
    uint64_t r8_2;
    uint64_t var_33;
    uint64_t var_34;
    struct indirect_placeholder_31_ret_type var_35;
    uint64_t var_36;
    uint64_t var_37;
    uint64_t var_38;
    uint64_t rax_2;
    uint64_t var_18;
    uint64_t var_19;
    struct indirect_placeholder_32_ret_type var_20;
    uint64_t rcx_0;
    uint64_t var_9;
    unsigned char var_10;
    uint64_t rdx_1;
    struct indirect_placeholder_30_ret_type var_30;
    uint64_t var_0;
    uint64_t var_1;
    uint64_t var_2;
    uint64_t var_3;
    uint64_t var_4;
    uint64_t var_5;
    uint64_t var_6;
    uint64_t var_7;
    uint64_t var_8;
    uint64_t local_sp_3;
    uint64_t r9_0;
    uint64_t r9_2;
    uint64_t local_sp_0;
    uint64_t r8_0;
    uint64_t r12_0;
    uint64_t local_sp_1;
    uint64_t r8_1;
    uint64_t r9_1;
    uint64_t r13_0;
    uint64_t r15_0;
    uint64_t local_sp_2;
    uint64_t var_21;
    uint64_t local_sp_5;
    uint64_t var_22;
    uint64_t var_23;
    uint64_t storemerge;
    uint64_t r12_1;
    uint64_t local_sp_4;
    uint64_t rbp_1;
    uint64_t rdx_0_be;
    uint64_t rax_0_be;
    uint64_t rax_0;
    uint64_t rdx_0;
    uint64_t var_11;
    uint64_t var_12;
    unsigned char var_13;
    uint64_t var_14;
    unsigned char var_15;
    uint64_t var_16;
    uint64_t rbx_0;
    uint64_t *var_17;
    unsigned int loop_state_var;
    bool switch_state_var;
    var_0 = revng_init_local_sp(0UL);
    var_1 = init_r15();
    var_2 = init_r13();
    var_3 = init_rbx();
    var_4 = init_r12();
    var_5 = init_rbp();
    var_6 = init_cc_src2();
    var_7 = init_r14();
    *(uint64_t *)(var_0 + (-8L)) = var_1;
    *(uint64_t *)(var_0 + (-16L)) = var_7;
    *(uint64_t *)(var_0 + (-24L)) = var_2;
    *(uint64_t *)(var_0 + (-32L)) = var_4;
    *(uint64_t *)(var_0 + (-40L)) = var_5;
    *(uint64_t *)(var_0 + (-48L)) = var_3;
    var_8 = var_0 + (-1640L);
    r14_0 = 0UL;
    rbp_0 = 0UL;
    rcx_0 = 0UL;
    rdx_1 = 0UL;
    r12_0 = 0UL;
    r13_0 = 5UL;
    r15_0 = 0UL;
    r12_1 = 131086UL;
    local_sp_4 = var_8;
    rbp_1 = 0UL;
    while (1U)
        {
            var_9 = *(uint64_t *)(rcx_0 + 6504208UL);
            var_10 = *(unsigned char *)var_9;
            rdx_0 = var_9;
            if (var_10 != '\x00') {
                rax_0 = (uint64_t)var_10;
                while (1U)
                    {
                        var_11 = (uint64_t)((unsigned char)rax_0 + '\xdb');
                        var_12 = rdx_0 + 1UL;
                        var_13 = *(unsigned char *)var_12;
                        rax_0_be = (uint64_t)var_13;
                        rdx_0_be = var_12;
                        rdx_1 = rdx_0;
                        if (var_11 != 0UL) {
                            if (var_13 == '\x00') {
                                break;
                            }
                            rax_0 = rax_0_be;
                            rdx_0 = rdx_0_be;
                            continue;
                        }
                        if ((uint64_t)(var_13 + '\xdb') != 0UL) {
                            if ((uint64_t)(var_13 + '\x9e') == 0UL) {
                                break;
                            }
                            if (var_13 == '\x00') {
                                break;
                            }
                        }
                        var_14 = rdx_0 + 2UL;
                        var_15 = *(unsigned char *)var_14;
                        rdx_0_be = var_14;
                        if (var_15 != '\x00') {
                            break;
                        }
                        rax_0_be = (uint64_t)var_15;
                    }
            }
            *(uint64_t *)((rcx_0 + var_8) + 32UL) = rdx_1;
            if (rcx_0 == 8UL) {
                break;
            }
            rcx_0 = rcx_0 + 8UL;
            continue;
        }
    if (*(uint64_t *)(var_0 + (-1608L)) != 0UL) {
        if (*(uint64_t *)(var_0 + (-1600L)) == 0UL) {
            return;
        }
    }
    var_16 = var_0 + (-1592L);
    *(uint64_t *)(var_0 + (-1632L)) = var_16;
    rax_1 = var_16;
    storemerge = var_16;
    while (1U)
        {
            rax_2 = rax_1;
            rbx_0 = storemerge;
            local_sp_5 = local_sp_4;
            while (1U)
                {
                    *(uint64_t *)(local_sp_5 + 24UL) = r13_0;
                    *(uint64_t *)(local_sp_5 + (-8L)) = 4213485UL;
                    indirect_placeholder();
                    var_17 = (uint64_t *)(local_sp_5 + (-16L));
                    *var_17 = 4213501UL;
                    indirect_placeholder();
                    if (rax_2 != 0UL) {
                        loop_state_var = 1U;
                        break;
                    }
                    var_18 = local_sp_5 + 8UL;
                    var_19 = local_sp_5 + (-24L);
                    *(uint64_t *)var_19 = 4213537UL;
                    var_20 = indirect_placeholder_32(0UL, 0UL, var_18, 128UL, 0UL, rbx_0);
                    local_sp_1 = var_19;
                    local_sp_4 = var_19;
                    local_sp_5 = var_19;
                    if (var_20.field_0 <= 127UL) {
                        loop_state_var = 1U;
                        break;
                    }
                    var_21 = *(uint64_t *)local_sp_5;
                    var_22 = helper_cc_compute_c_wrapper(rbp_1 - var_21, var_21, var_6, 17U);
                    var_23 = (var_22 == 0UL) ? rbp_1 : var_21;
                    rax_1 = var_21;
                    r13_0 = var_23;
                    rax_2 = var_21;
                    rbp_1 = var_23;
                    if (rbx_0 != (local_sp_4 + 1456UL)) {
                        loop_state_var = 0U;
                        break;
                    }
                    rbx_0 = rbx_0 + 128UL;
                    r12_1 = (uint64_t)((uint32_t)r12_1 + 1U);
                    continue;
                }
            switch_state_var = 0;
            switch (loop_state_var) {
              case 1U:
                {
                    switch_state_var = 1;
                    break;
                }
                break;
              case 0U:
                {
                    if (r13_0 > var_23) {
                        storemerge = *var_17;
                        continue;
                    }
                    r9_1 = var_20.field_1;
                    r8_1 = var_20.field_2;
                    while (1U)
                        {
                            var_24 = *(uint64_t *)((r15_0 + local_sp_1) + 32UL);
                            var_25 = *(uint64_t *)(r15_0 + 6504208UL);
                            var_26 = var_24 - var_25;
                            local_sp_2 = local_sp_1;
                            r9_2 = r9_1;
                            local_sp_3 = local_sp_1;
                            r8_2 = r8_1;
                            if (var_24 != 0UL) {
                                while (1U)
                                    {
                                        var_33 = (r12_0 + rbp_0) + 6505344UL;
                                        var_34 = local_sp_3 + (-8L);
                                        *(uint64_t *)var_34 = 4213838UL;
                                        var_35 = indirect_placeholder_31(0UL, var_33, r9_2, var_25, 4376828UL, r8_2, 128UL);
                                        var_36 = var_35.field_0;
                                        var_37 = var_35.field_1;
                                        var_38 = var_35.field_2;
                                        r8_2 = var_38;
                                        local_sp_3 = var_34;
                                        r9_0 = var_37;
                                        r9_2 = var_37;
                                        local_sp_0 = var_34;
                                        r8_0 = var_38;
                                        if ((uint64_t)((uint32_t)var_36 & (-128)) != 0UL) {
                                            loop_state_var = 1U;
                                            break;
                                        }
                                        if (rbp_0 != 1408UL) {
                                            loop_state_var = 0U;
                                            break;
                                        }
                                        rbp_0 = rbp_0 + 128UL;
                                        continue;
                                    }
                                switch_state_var = 0;
                                switch (loop_state_var) {
                                  case 1U:
                                    {
                                        switch_state_var = 1;
                                        break;
                                    }
                                    break;
                                  case 0U:
                                    {
                                        r9_1 = r9_0;
                                        local_sp_1 = local_sp_0;
                                        r8_1 = r8_0;
                                        if (r15_0 != 8UL) {
                                            *(unsigned char *)6505288UL = (unsigned char)'\x01';
                                            switch_state_var = 1;
                                            break;
                                        }
                                        r15_0 = r15_0 + 8UL;
                                        r12_0 = r12_0 + 1536UL;
                                        continue;
                                    }
                                    break;
                                }
                                if (switch_state_var)
                                    break;
                            }
                            if ((long)var_26 > (long)128UL) {
                                break;
                            }
                            var_27 = (uint64_t)(uint32_t)var_26;
                            while (1U)
                                {
                                    var_28 = (r14_0 + r12_0) + 6505344UL;
                                    *(uint64_t *)(local_sp_2 + (-16L)) = (var_24 + 2UL);
                                    var_29 = r14_0 + *(uint64_t *)(local_sp_2 + 8UL);
                                    *(uint64_t *)(local_sp_2 + (-24L)) = 4213725UL;
                                    var_30 = indirect_placeholder_30(0UL, var_28, var_29, var_27, 4366805UL, var_25, 128UL);
                                    var_31 = var_30.field_0;
                                    var_32 = local_sp_2 + (-8L);
                                    local_sp_0 = var_32;
                                    local_sp_2 = var_32;
                                    if ((uint64_t)((uint32_t)var_31 & (-128)) != 0UL) {
                                        loop_state_var = 1U;
                                        break;
                                    }
                                    if (r14_0 != 1408UL) {
                                        loop_state_var = 0U;
                                        break;
                                    }
                                    r14_0 = r14_0 + 128UL;
                                    continue;
                                }
                            switch_state_var = 0;
                            switch (loop_state_var) {
                              case 0U:
                                {
                                    r9_0 = var_30.field_1;
                                    r8_0 = var_30.field_2;
                                }
                                break;
                              case 1U:
                                {
                                    switch_state_var = 1;
                                    break;
                                }
                                break;
                            }
                            if (switch_state_var)
                                break;
                        }
                    switch_state_var = 1;
                    break;
                }
                break;
            }
            if (switch_state_var)
                break;
        }
    return;
}

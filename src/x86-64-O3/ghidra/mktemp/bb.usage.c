
#include "mktemp.h"

long null_ARRAY_00614b4_0_4_;
long null_ARRAY_00614b4_40_8_;
long null_ARRAY_00614b4_4_4_;
long null_ARRAY_00614b4_48_8_;
long null_ARRAY_00614b8_0_8_;
long null_ARRAY_00614b8_8_8_;
long null_ARRAY_00614d8_0_8_;
long null_ARRAY_00614d8_16_8_;
long null_ARRAY_00614d8_24_8_;
long null_ARRAY_00614d8_32_8_;
long null_ARRAY_00614d8_40_8_;
long null_ARRAY_00614d8_48_8_;
long null_ARRAY_00614d8_8_8_;
long null_ARRAY_00614dc_0_4_;
long null_ARRAY_00614dc_16_8_;
long null_ARRAY_00614dc_24_4_;
long null_ARRAY_00614dc_32_8_;
long null_ARRAY_00614dc_40_4_;
long null_ARRAY_00614dc_4_4_;
long null_ARRAY_00614dc_44_4_;
long null_ARRAY_00614dc_48_4_;
long null_ARRAY_00614dc_8_4_;
long local_2f_1_1_;
long local_5b_1_1_;
long local_9c_4_4_;
long local_a1_0_4_;
long local_a1_0_8_;
long local_a1_1_9_;
long local_a1_4_6_;
long local_a1_8_2_;
long local_a3_4_4_;
long local_a4_0_2_;
long local_a4_0_4_;
long local_a4_4_4_;
long local_a5_0_8_;
long local_a5_1_9_;
long local_a5_4_6_;
long local_a5_8_2_;
long local_a6_0_4_;
long DAT_00000008;
long DAT_00000010;
long DAT_004111c0;
long DAT_0041124b;
long DAT_004112e8;
long DAT_00411c10;
long DAT_00411c14;
long DAT_00411c18;
long DAT_00411c1b;
long DAT_00411c1d;
long DAT_00411c21;
long DAT_00411c25;
long DAT_00412252;
long DAT_004126d5;
long DAT_004127d9;
long DAT_004127df;
long DAT_004127f1;
long DAT_004127f2;
long DAT_00412810;
long DAT_00412814;
long DAT_004128b7;
long DAT_004128cd;
long DAT_006146d0;
long DAT_006146e0;
long DAT_006146f0;
long DAT_00614b28;
long DAT_00614b90;
long DAT_00614b94;
long DAT_00614b98;
long DAT_00614b9c;
long DAT_00614bc0;
long DAT_00614bd0;
long DAT_00614be0;
long DAT_00614be8;
long DAT_00614c00;
long DAT_00614c08;
long DAT_00614c50;
long DAT_00614c58;
long DAT_00614c60;
long DAT_00614c68;
long DAT_00614df8;
long DAT_00614e38;
long DAT_00614e40;
long DAT_00614e48;
long DAT_00614e58;
long _DYNAMIC;
long fde_00413340;
long int7;
long null_ARRAY_00411ac0;
long null_ARRAY_00412b60;
long null_ARRAY_00412db0;
long null_ARRAY_00614b40;
long null_ARRAY_00614b80;
long null_ARRAY_00614c20;
long null_ARRAY_00614c80;
long null_ARRAY_00614d80;
long null_ARRAY_00614dc0;
long PTR_DAT_00614b20;
long PTR_null_ARRAY_00614b78;
long PTR_null_ARRAY_00614ba0;
long register0x00000020;
void
FUN_00402480 (uint uParm1)
{
  int iVar1;
  long lVar2;
  undefined8 uVar3;
  undefined **ppuVar4;
  char *pcVar5;
  undefined *puVar6;
  undefined *local_88;
  char *local_80;
  undefined8 local_78;
  char *local_70;
  char *local_68;
  char *local_60;
  char *local_58;
  char *local_50;
  char *local_48;
  char *local_40;
  char *local_38;
  char *local_30;
  undefined8 local_28;
  undefined8 local_20;

  if (uParm1 == 0)
    goto LAB_004024ad;
  func_0x00401a20 (DAT_00614be0, "Try \'%s --help\' for more information.\n",
		   DAT_00614c68);
  do
    {
      func_0x00401c30 ((ulong) uParm1);
    LAB_004024ad:
      ;
      func_0x00401850 ("Usage: %s [OPTION]... [TEMPLATE]\n", DAT_00614c68);
      uVar3 = DAT_00614bc0;
      func_0x00401a30
	("Create a temporary file or directory, safely, and print its name.\nTEMPLATE must contain at least 3 consecutive \'X\'s in last component.\nIf TEMPLATE is not specified, use tmp.XXXXXXXXXX, and --tmpdir is implied.\n",
	 DAT_00614bc0);
      func_0x00401a30
	("Files are created u+rw, and directories u+rwx, minus umask restrictions.\n",
	 uVar3);
      func_0x00401a30 (0x41280e, uVar3);
      func_0x00401a30
	("  -d, --directory     create a directory, not a file\n  -u, --dry-run       do not create anything; merely print a name (unsafe)\n  -q, --quiet         suppress diagnostics about file/dir-creation failure\n",
	 uVar3);
      func_0x00401a30
	("      --suffix=SUFF   append SUFF to TEMPLATE; SUFF must not contain a slash.\n                        This option is implied if TEMPLATE does not end in X\n",
	 uVar3);
      func_0x00401a30
	("  -p DIR, --tmpdir[=DIR]  interpret TEMPLATE relative to DIR; if DIR is not\n                        specified, use $TMPDIR if set, else /tmp.  With\n                        this option, TEMPLATE must not be an absolute name;\n                        unlike with -t, TEMPLATE may contain slashes, but\n                        mktemp creates only the final component\n",
	 uVar3);
      func_0x00401a30
	("  -t                  interpret TEMPLATE as a single file name component,\n                        relative to a directory: $TMPDIR, if set; else the\n                        directory specified via -p; else /tmp [deprecated]\n",
	 uVar3);
      func_0x00401a30 ("      --help     display this help and exit\n",
		       uVar3);
      func_0x00401a30
	("      --version  output version information and exit\n", uVar3);
      local_88 = &DAT_004111c0;
      local_80 = "test invocation";
      puVar6 = &DAT_004111c0;
      local_78 = 0x41122a;
      local_70 = "Multi-call invocation";
      local_68 = "sha224sum";
      local_60 = "sha2 utilities";
      local_58 = "sha256sum";
      local_50 = "sha2 utilities";
      local_48 = "sha384sum";
      local_40 = "sha2 utilities";
      local_38 = "sha512sum";
      local_30 = "sha2 utilities";
      local_28 = 0;
      local_20 = 0;
      ppuVar4 = &local_88;
      do
	{
	  iVar1 = func_0x00401b80 ("mktemp", puVar6);
	  if (iVar1 == 0)
	    break;
	  ppuVar4 = ppuVar4 + 2;
	  puVar6 = *ppuVar4;
	}
      while (puVar6 != (undefined *) 0x0);
      pcVar5 = ppuVar4[1];
      if (pcVar5 == (char *) 0x0)
	{
	  func_0x00401850 ("\n%s online help: <%s>\n", "GNU coreutils",
			   "https://www.gnu.org/software/coreutils/");
	  lVar2 = func_0x00401be0 (5, 0);
	  if (lVar2 != 0)
	    {
	      iVar1 = func_0x00401aa0 (lVar2, &DAT_0041124b, 3);
	      if (iVar1 != 0)
		{
		  pcVar5 = "mktemp";
		  goto LAB_00402691;
		}
	    }
	  func_0x00401850 ("Full documentation at: <%s%s>\n",
			   "https://www.gnu.org/software/coreutils/",
			   "mktemp");
	LAB_004026ba:
	  ;
	  pcVar5 = "mktemp";
	  uVar3 = 0x4111e3;
	}
      else
	{
	  func_0x00401850 ("\n%s online help: <%s>\n", "GNU coreutils",
			   "https://www.gnu.org/software/coreutils/");
	  lVar2 = func_0x00401be0 (5, 0);
	  if (lVar2 != 0)
	    {
	      iVar1 = func_0x00401aa0 (lVar2, &DAT_0041124b, 3);
	      if (iVar1 != 0)
		{
		LAB_00402691:
		  ;
		  func_0x00401850
		    ("Report %s translation bugs to <https://translationproject.org/team/>\n",
		     "mktemp");
		}
	    }
	  func_0x00401850 ("Full documentation at: <%s%s>\n",
			   "https://www.gnu.org/software/coreutils/",
			   "mktemp");
	  uVar3 = 0x41280f;
	  if (pcVar5 == "mktemp")
	    goto LAB_004026ba;
	}
      func_0x00401850
	("or available locally via: info \'(coreutils) %s%s\'\n", pcVar5,
	 uVar3);
    }
  while (true);
}

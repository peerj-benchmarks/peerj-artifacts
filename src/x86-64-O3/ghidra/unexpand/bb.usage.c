
#include "unexpand.h"

long null_ARRAY_006135e_0_4_;
long null_ARRAY_006135e_40_8_;
long null_ARRAY_006135e_4_4_;
long null_ARRAY_006135e_48_8_;
long null_ARRAY_0061362_0_8_;
long null_ARRAY_0061362_8_8_;
long null_ARRAY_0061384_0_8_;
long null_ARRAY_0061384_16_8_;
long null_ARRAY_0061384_24_8_;
long null_ARRAY_0061384_32_8_;
long null_ARRAY_0061384_40_8_;
long null_ARRAY_0061384_48_8_;
long null_ARRAY_0061384_8_8_;
long null_ARRAY_0061388_0_4_;
long null_ARRAY_0061388_16_8_;
long null_ARRAY_0061388_24_4_;
long null_ARRAY_0061388_32_8_;
long null_ARRAY_0061388_40_4_;
long null_ARRAY_0061388_4_4_;
long null_ARRAY_0061388_44_4_;
long null_ARRAY_0061388_48_4_;
long null_ARRAY_0061388_8_4_;
long local_2f_1_1_;
long local_5b_1_1_;
long local_9c_4_4_;
long local_a1_0_4_;
long local_a1_0_8_;
long local_a1_1_9_;
long local_a1_4_6_;
long local_a1_8_2_;
long local_a3_4_4_;
long local_a4_0_2_;
long local_a4_0_4_;
long local_a4_4_4_;
long local_a5_0_8_;
long local_a5_1_9_;
long local_a5_4_6_;
long local_a5_8_2_;
long local_a6_0_4_;
long DAT_00000008;
long DAT_00000010;
long DAT_00410440;
long DAT_004104cd;
long DAT_00410cd8;
long DAT_00410cdc;
long DAT_00410ce0;
long DAT_00410ce3;
long DAT_00410ce5;
long DAT_00410ce9;
long DAT_00410ced;
long DAT_0041126b;
long DAT_004116f5;
long DAT_004117f9;
long DAT_004117ff;
long DAT_00411811;
long DAT_00411812;
long DAT_00411830;
long DAT_00411834;
long DAT_004118be;
long DAT_006131d0;
long DAT_006131e0;
long DAT_006131f0;
long DAT_006135d8;
long DAT_00613630;
long DAT_00613634;
long DAT_00613638;
long DAT_0061363c;
long DAT_00613640;
long DAT_00613648;
long DAT_00613650;
long DAT_00613660;
long DAT_00613668;
long DAT_00613680;
long DAT_00613688;
long DAT_006136d0;
long DAT_006136d4;
long DAT_006136d8;
long DAT_006136e0;
long DAT_006136e8;
long DAT_006136f0;
long DAT_006136f8;
long DAT_00613700;
long DAT_00613708;
long DAT_00613710;
long DAT_00613718;
long DAT_00613720;
long DAT_00613728;
long DAT_00613730;
long DAT_006138b8;
long DAT_006138c0;
long DAT_006138c8;
long DAT_006138d0;
long DAT_006138e0;
long fde_00412298;
long int7;
long null_ARRAY_00410880;
long null_ARRAY_00411b60;
long null_ARRAY_00411db0;
long null_ARRAY_006135c0;
long null_ARRAY_006135e0;
long null_ARRAY_00613620;
long null_ARRAY_006136a0;
long null_ARRAY_00613740;
long null_ARRAY_00613840;
long null_ARRAY_00613880;
long PTR_DAT_006135d0;
long PTR_null_ARRAY_00613618;
void
FUN_004020b0 (uint uParm1)
{
  int iVar1;
  long lVar2;
  undefined8 uVar3;
  undefined **ppuVar4;
  char *pcVar5;
  undefined *puVar6;
  undefined *local_88;
  char *local_80;
  undefined8 local_78;
  char *local_70;
  char *local_68;
  char *local_60;
  char *local_58;
  char *local_50;
  char *local_48;
  char *local_40;
  char *local_38;
  char *local_30;
  undefined8 local_28;
  undefined8 local_20;

  if (uParm1 == 0)
    goto LAB_004020dd;
  func_0x00401830 (DAT_00613660, "Try \'%s --help\' for more information.\n",
		   DAT_00613730);
  do
    {
      func_0x00401a00 ((ulong) uParm1);
    LAB_004020dd:
      ;
      func_0x00401690 ("Usage: %s [OPTION]... [FILE]...\n", DAT_00613730);
      uVar3 = DAT_00613640;
      func_0x00401840
	("Convert blanks in each FILE to tabs, writing to standard output.\n",
	 DAT_00613640);
      func_0x00401840
	("\nWith no FILE, or when FILE is -, read standard input.\n", uVar3);
      func_0x00401840
	("\nMandatory arguments to long options are mandatory for short options too.\n",
	 uVar3);
      func_0x00401840
	("  -a, --all        convert all blanks, instead of just initial blanks\n      --first-only  convert only leading sequences of blanks (overrides -a)\n  -t, --tabs=N     have tabs N characters apart instead of 8 (enables -a)\n",
	 uVar3);
      FUN_00402cb0 ();
      func_0x00401840 ("      --help     display this help and exit\n",
		       uVar3);
      func_0x00401840
	("      --version  output version information and exit\n", uVar3);
      local_88 = &DAT_00410440;
      local_80 = "test invocation";
      puVar6 = &DAT_00410440;
      local_78 = 0x4104ac;
      local_70 = "Multi-call invocation";
      local_68 = "sha224sum";
      local_60 = "sha2 utilities";
      local_58 = "sha256sum";
      local_50 = "sha2 utilities";
      local_48 = "sha384sum";
      local_40 = "sha2 utilities";
      local_38 = "sha512sum";
      local_30 = "sha2 utilities";
      local_28 = 0;
      local_20 = 0;
      ppuVar4 = &local_88;
      do
	{
	  iVar1 = func_0x00401940 ("unexpand", puVar6);
	  if (iVar1 == 0)
	    break;
	  ppuVar4 = ppuVar4 + 2;
	  puVar6 = *ppuVar4;
	}
      while (puVar6 != (undefined *) 0x0);
      pcVar5 = ppuVar4[1];
      if (pcVar5 == (char *) 0x0)
	{
	  func_0x00401690 ("\n%s online help: <%s>\n", "GNU coreutils",
			   "https://www.gnu.org/software/coreutils/");
	  lVar2 = func_0x004019a0 (5, 0);
	  if (lVar2 != 0)
	    {
	      iVar1 = func_0x004018a0 (lVar2, &DAT_004104cd, 3);
	      if (iVar1 != 0)
		{
		  pcVar5 = "unexpand";
		  goto LAB_004022a1;
		}
	    }
	  func_0x00401690 ("Full documentation at: <%s%s>\n",
			   "https://www.gnu.org/software/coreutils/",
			   "unexpand");
	LAB_004022ca:
	  ;
	  pcVar5 = "unexpand";
	  uVar3 = 0x410465;
	}
      else
	{
	  func_0x00401690 ("\n%s online help: <%s>\n", "GNU coreutils",
			   "https://www.gnu.org/software/coreutils/");
	  lVar2 = func_0x004019a0 (5, 0);
	  if (lVar2 != 0)
	    {
	      iVar1 = func_0x004018a0 (lVar2, &DAT_004104cd, 3);
	      if (iVar1 != 0)
		{
		LAB_004022a1:
		  ;
		  func_0x00401690
		    ("Report %s translation bugs to <https://translationproject.org/team/>\n",
		     "unexpand");
		}
	    }
	  func_0x00401690 ("Full documentation at: <%s%s>\n",
			   "https://www.gnu.org/software/coreutils/",
			   "unexpand");
	  uVar3 = 0x41182f;
	  if (pcVar5 == "unexpand")
	    goto LAB_004022ca;
	}
      func_0x00401690
	("or available locally via: info \'(coreutils) %s%s\'\n", pcVar5,
	 uVar3);
    }
  while (true);
}

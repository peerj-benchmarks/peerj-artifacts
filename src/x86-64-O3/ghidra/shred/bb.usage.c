
#include "shred.h"

long null_ARRAY_006194c_0_4_;
long null_ARRAY_006194c_40_8_;
long null_ARRAY_006194c_4_4_;
long null_ARRAY_006194c_48_8_;
long null_ARRAY_0061950_0_8_;
long null_ARRAY_0061950_8_8_;
long null_ARRAY_0061970_0_8_;
long null_ARRAY_0061970_16_8_;
long null_ARRAY_0061970_24_8_;
long null_ARRAY_0061970_32_8_;
long null_ARRAY_0061970_40_8_;
long null_ARRAY_0061970_48_8_;
long null_ARRAY_0061970_8_8_;
long null_ARRAY_0061974_0_4_;
long null_ARRAY_0061974_16_8_;
long null_ARRAY_0061974_24_4_;
long null_ARRAY_0061974_32_8_;
long null_ARRAY_0061974_40_4_;
long null_ARRAY_0061974_4_4_;
long null_ARRAY_0061974_44_4_;
long null_ARRAY_0061974_48_4_;
long null_ARRAY_0061974_8_4_;
long local_2f_1_1_;
long local_4_0_5_;
long local_4_0_6_;
long local_4_0_7_;
long local_4_4_1_;
long local_4_4_4_;
long local_5b_1_1_;
long local_9c_4_4_;
long local_a1_0_4_;
long local_a1_0_8_;
long local_a1_1_9_;
long local_a1_4_6_;
long local_a1_8_2_;
long local_a3_4_4_;
long local_a4_0_2_;
long local_a4_0_4_;
long local_a4_4_4_;
long local_a5_0_8_;
long local_a5_1_9_;
long local_a5_4_6_;
long local_a5_8_2_;
long local_a6_0_4_;
long DAT_00000008;
long DAT_00000010;
long DAT_004147b4;
long DAT_0041483e;
long DAT_00414955;
long DAT_00415a7a;
long DAT_00415a7c;
long DAT_00415b80;
long DAT_00415b84;
long DAT_00415b88;
long DAT_00415b8b;
long DAT_00415b8d;
long DAT_00415b91;
long DAT_00415b95;
long DAT_00416137;
long DAT_0041615a;
long DAT_004167c8;
long DAT_004168d1;
long DAT_004168d7;
long DAT_004168d9;
long DAT_004168da;
long DAT_004168f8;
long DAT_004168fc;
long DAT_00416986;
long DAT_00619000;
long DAT_00619010;
long DAT_00619020;
long DAT_006194b0;
long DAT_00619510;
long DAT_00619514;
long DAT_00619518;
long DAT_0061951c;
long DAT_00619540;
long DAT_00619550;
long DAT_00619560;
long DAT_00619568;
long DAT_00619580;
long DAT_00619588;
long DAT_006195d0;
long DAT_006195d8;
long DAT_006195e0;
long DAT_006195e8;
long DAT_00619738;
long DAT_006197b8;
long DAT_006197c0;
long DAT_006197c8;
long DAT_006197d8;
long _DYNAMIC;
long fde_00417450;
long int7;
long null_ARRAY_00415780;
long null_ARRAY_00415880;
long null_ARRAY_004159e0;
long null_ARRAY_00415a00;
long null_ARRAY_00415b18;
long null_ARRAY_00416c20;
long null_ARRAY_00416e70;
long null_ARRAY_006194c0;
long null_ARRAY_00619500;
long null_ARRAY_006195a0;
long null_ARRAY_00619600;
long null_ARRAY_00619700;
long null_ARRAY_00619740;
long PTR_DAT_006194a0;
long PTR_null_ARRAY_006194f8;
long PTR_null_ARRAY_00619520;
long register0x00000020;
void
FUN_00403930 (uint uParm1)
{
  int iVar1;
  long lVar2;
  undefined8 uVar3;
  undefined **ppuVar4;
  char *pcVar5;
  undefined *puVar6;
  undefined *local_88;
  char *local_80;
  undefined8 local_78;
  char *local_70;
  char *local_68;
  char *local_60;
  char *local_58;
  char *local_50;
  char *local_48;
  char *local_40;
  char *local_38;
  char *local_30;
  undefined8 local_28;
  undefined8 local_20;

  if (uParm1 == 0)
    goto LAB_0040395d;
  func_0x00401e10 (DAT_00619560, "Try \'%s --help\' for more information.\n",
		   DAT_006195e8);
  do
    {
      func_0x00402060 ((ulong) uParm1);
    LAB_0040395d:
      ;
      func_0x00401bc0 ("Usage: %s [OPTION]... FILE...\n", DAT_006195e8);
      uVar3 = DAT_00619540;
      func_0x00401e20
	("Overwrite the specified FILE(s) repeatedly, in order to make it harder\nfor even very expensive hardware probing to recover the data.\n",
	 DAT_00619540);
      func_0x00401e20 ("\nIf FILE is -, shred standard output.\n", uVar3);
      func_0x00401e20
	("\nMandatory arguments to long options are mandatory for short options too.\n",
	 uVar3);
      func_0x00401bc0
	("  -f, --force    change permissions to allow writing if necessary\n  -n, --iterations=N  overwrite N times instead of the default (%d)\n      --random-source=FILE  get random bytes from FILE\n  -s, --size=N   shred this many bytes (suffixes like K, M, G accepted)\n",
	 3);
      func_0x00401e20
	("  -u             deallocate and remove file after overwriting\n      --remove[=HOW]  like -u but give control on HOW to delete;  See below\n  -v, --verbose  show progress\n  -x, --exact    do not round file sizes up to the next full block;\n                   this is the default for non-regular files\n  -z, --zero     add a final overwrite with zeros to hide shredding\n",
	 uVar3);
      func_0x00401e20 ("      --help     display this help and exit\n",
		       uVar3);
      func_0x00401e20
	("      --version  output version information and exit\n", uVar3);
      func_0x00401e20
	("\nDelete FILE(s) if --remove (-u) is specified.  The default is not to remove\nthe files because it is common to operate on device files like /dev/hda,\nand those files usually should not be removed.\nThe optional HOW parameter indicates how to remove a directory entry:\n\'unlink\' => use a standard unlink call.\n\'wipe\' => also first obfuscate bytes in the name.\n\'wipesync\' => also sync each obfuscated byte to disk.\nThe default mode is \'wipesync\', but note it can be expensive.\n\n",
	 uVar3);
      func_0x00401e20
	("CAUTION: Note that shred relies on a very important assumption:\nthat the file system overwrites data in place.  This is the traditional\nway to do things, but many modern file system designs do not satisfy this\nassumption.  The following are examples of file systems on which shred is\nnot effective, or is not guaranteed to be effective in all file system modes:\n\n",
	 uVar3);
      func_0x00401e20
	("* log-structured or journaled file systems, such as those supplied with\nAIX and Solaris (and JFS, ReiserFS, XFS, Ext3, etc.)\n\n* file systems that write redundant data and carry on even if some writes\nfail, such as RAID-based file systems\n\n* file systems that make snapshots, such as Network Appliance\'s NFS server\n\n",
	 uVar3);
      func_0x00401e20
	("* file systems that cache in temporary locations, such as NFS\nversion 3 clients\n\n* compressed file systems\n\n",
	 uVar3);
      func_0x00401e20
	("In the case of ext3 file systems, the above disclaimer applies\n(and shred is thus of limited effectiveness) only in data=journal mode,\nwhich journals file data in addition to just metadata.  In both the\ndata=ordered (default) and data=writeback modes, shred works as usual.\nExt3 journaling modes can be changed by adding the data=something option\nto the mount options for a particular file system in the /etc/fstab file,\nas documented in the mount man page (man mount).\n\n",
	 uVar3);
      func_0x00401e20
	("In addition, file system backups and remote mirrors may contain copies\nof the file that cannot be removed, and that will allow a shredded file\nto be recovered later.\n",
	 uVar3);
      local_88 = &DAT_004147b4;
      local_80 = "test invocation";
      puVar6 = &DAT_004147b4;
      local_78 = 0x41481d;
      local_70 = "Multi-call invocation";
      local_68 = "sha224sum";
      local_60 = "sha2 utilities";
      local_58 = "sha256sum";
      local_50 = "sha2 utilities";
      local_48 = "sha384sum";
      local_40 = "sha2 utilities";
      local_38 = "sha512sum";
      local_30 = "sha2 utilities";
      local_28 = 0;
      local_20 = 0;
      ppuVar4 = &local_88;
      do
	{
	  iVar1 = func_0x00401f90 ("shred", puVar6);
	  if (iVar1 == 0)
	    break;
	  ppuVar4 = ppuVar4 + 2;
	  puVar6 = *ppuVar4;
	}
      while (puVar6 != (undefined *) 0x0);
      pcVar5 = ppuVar4[1];
      if (pcVar5 == (char *) 0x0)
	{
	  func_0x00401bc0 ("\n%s online help: <%s>\n", "GNU coreutils",
			   "https://www.gnu.org/software/coreutils/");
	  lVar2 = func_0x00401ff0 (5, 0);
	  if (lVar2 != 0)
	    {
	      iVar1 = func_0x00401e90 (lVar2, &DAT_0041483e, 3);
	      if (iVar1 != 0)
		{
		  pcVar5 = "shred";
		  goto LAB_00403b79;
		}
	    }
	  func_0x00401bc0 ("Full documentation at: <%s%s>\n",
			   "https://www.gnu.org/software/coreutils/",
			   "shred");
	LAB_00403ba2:
	  ;
	  pcVar5 = "shred";
	  uVar3 = 0x4147d6;
	}
      else
	{
	  func_0x00401bc0 ("\n%s online help: <%s>\n", "GNU coreutils",
			   "https://www.gnu.org/software/coreutils/");
	  lVar2 = func_0x00401ff0 (5, 0);
	  if (lVar2 != 0)
	    {
	      iVar1 = func_0x00401e90 (lVar2, &DAT_0041483e, 3);
	      if (iVar1 != 0)
		{
		LAB_00403b79:
		  ;
		  func_0x00401bc0
		    ("Report %s translation bugs to <https://translationproject.org/team/>\n",
		     "shred");
		}
	    }
	  func_0x00401bc0 ("Full documentation at: <%s%s>\n",
			   "https://www.gnu.org/software/coreutils/",
			   "shred");
	  uVar3 = 0x4168f7;
	  if (pcVar5 == "shred")
	    goto LAB_00403ba2;
	}
      func_0x00401bc0
	("or available locally via: info \'(coreutils) %s%s\'\n", pcVar5,
	 uVar3);
    }
  while (true);
}

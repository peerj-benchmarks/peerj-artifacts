
#include "who.h"

long DAT_0061442_1_1_;
long null_ARRAY_0061444_0_4_;
long null_ARRAY_0061444_40_8_;
long null_ARRAY_0061444_4_4_;
long null_ARRAY_0061444_48_8_;
long null_ARRAY_0061448_0_8_;
long null_ARRAY_0061448_8_8_;
long null_ARRAY_006145e_0_4_;
long null_ARRAY_0061474_0_8_;
long null_ARRAY_0061474_16_8_;
long null_ARRAY_0061474_24_8_;
long null_ARRAY_0061474_32_8_;
long null_ARRAY_0061474_40_8_;
long null_ARRAY_0061474_48_8_;
long null_ARRAY_0061474_8_8_;
long null_ARRAY_0061478_0_4_;
long null_ARRAY_0061478_16_8_;
long null_ARRAY_0061478_24_4_;
long null_ARRAY_0061478_32_8_;
long null_ARRAY_0061478_40_4_;
long null_ARRAY_0061478_4_4_;
long null_ARRAY_0061478_44_4_;
long null_ARRAY_0061478_48_4_;
long null_ARRAY_0061478_8_4_;
long local_2f_1_1_;
long local_5b_1_1_;
long local_9c_4_4_;
long local_a1_0_4_;
long local_a1_0_8_;
long local_a1_1_9_;
long local_a1_4_6_;
long local_a1_8_2_;
long local_a3_4_4_;
long local_a4_0_2_;
long local_a4_0_4_;
long local_a4_4_4_;
long local_a5_0_8_;
long local_a5_1_9_;
long local_a5_4_6_;
long local_a5_8_2_;
long local_a6_0_4_;
long DAT_00000008;
long DAT_00000010;
long DAT_00410e40;
long DAT_00410e5e;
long DAT_00410e62;
long DAT_00410e64;
long DAT_00410ea0;
long DAT_00410ea5;
long DAT_00410eb1;
long DAT_00410eb6;
long DAT_00410ec9;
long DAT_00410ece;
long DAT_00410ed3;
long DAT_00410ee5;
long DAT_00410ee9;
long DAT_00410f04;
long DAT_00410f22;
long DAT_00410f42;
long DAT_00410f44;
long DAT_00410fe8;
long DAT_00411918;
long DAT_00411960;
long DAT_00411964;
long DAT_00411968;
long DAT_0041196b;
long DAT_0041196d;
long DAT_00411971;
long DAT_00411975;
long DAT_00411f2b;
long DAT_004123b5;
long DAT_004124b9;
long DAT_004124bf;
long DAT_004124d1;
long DAT_004124d2;
long DAT_004124f0;
long DAT_00412576;
long DAT_00614000;
long DAT_00614010;
long DAT_00614020;
long DAT_00614420;
long DAT_00614428;
long DAT_00614438;
long DAT_00614490;
long DAT_00614494;
long DAT_00614498;
long DAT_0061449c;
long DAT_006144c0;
long DAT_006144d0;
long DAT_006144e0;
long DAT_006144e8;
long DAT_00614500;
long DAT_00614508;
long DAT_00614560;
long DAT_00614568;
long DAT_00614570;
long DAT_006145a1;
long DAT_006145a8;
long DAT_006145b0;
long DAT_006145b8;
long DAT_006145c0;
long DAT_006145c8;
long DAT_006145c9;
long DAT_006145ca;
long DAT_006145cb;
long DAT_006145cc;
long DAT_006145cd;
long DAT_006145ce;
long DAT_006145cf;
long DAT_006145d0;
long DAT_006145d1;
long DAT_006145d2;
long DAT_006145d3;
long DAT_006145d4;
long DAT_006145d5;
long DAT_006145d6;
long DAT_00614610;
long DAT_00614618;
long DAT_00614620;
long DAT_00614628;
long DAT_006147f8;
long DAT_00614800;
long DAT_00614808;
long DAT_00614818;
long fde_00412f40;
long int7;
long null_ARRAY_004116c0;
long null_ARRAY_00412800;
long null_ARRAY_00412a50;
long null_ARRAY_00614440;
long null_ARRAY_00614480;
long null_ARRAY_00614520;
long null_ARRAY_00614580;
long null_ARRAY_006145e0;
long null_ARRAY_00614640;
long null_ARRAY_00614740;
long null_ARRAY_00614780;
long null_ARRAY_006147c0;
long PTR_DAT_00614430;
long PTR_null_ARRAY_00614478;
long PTR_null_ARRAY_006144a0;
long register0x00000020;
long stack0x00000008;
void
FUN_00402ff0 (uint uParm1)
{
  int iVar1;
  long lVar2;
  undefined8 uVar3;
  undefined **ppuVar4;
  undefined *puVar5;
  undefined *local_88;
  char *local_80;
  undefined8 local_78;
  char *local_70;
  char *local_68;
  char *local_60;
  char *local_58;
  char *local_50;
  char *local_48;
  char *local_40;
  char *local_38;
  char *local_30;
  undefined8 local_28;
  undefined8 local_20;

  if (uParm1 == 0)
    goto LAB_0040301d;
  func_0x00401900 (DAT_006144e0, "Try \'%s --help\' for more information.\n",
		   DAT_00614628);
  do
    {
      func_0x00401b30 ((ulong) uParm1);
    LAB_0040301d:
      ;
      func_0x00401770 ("Usage: %s [OPTION]... [ FILE | ARG1 ARG2 ]\n",
		       DAT_00614628);
      uVar3 = DAT_006144c0;
      func_0x00401920
	("Print information about users who are currently logged in.\n",
	 DAT_006144c0);
      func_0x00401920
	("\n  -a, --all         same as -b -d --login -p -r -t -T -u\n  -b, --boot        time of last system boot\n  -d, --dead        print dead processes\n  -H, --heading     print line of column headings\n",
	 uVar3);
      func_0x00401920 ("  -l, --login       print system login processes\n",
		       uVar3);
      func_0x00401920
	("      --lookup      attempt to canonicalize hostnames via DNS\n  -m                only hostname and user associated with stdin\n  -p, --process     print active processes spawned by init\n",
	 uVar3);
      func_0x00401920
	("  -q, --count       all login names and number of users logged on\n  -r, --runlevel    print current runlevel\n  -s, --short       print only name, line, and time (default)\n  -t, --time        print last system clock change\n",
	 uVar3);
      func_0x00401920
	("  -T, -w, --mesg    add user\'s message status as +, - or ?\n  -u, --users       list users logged in\n      --message     same as -T\n      --writable    same as -T\n",
	 uVar3);
      func_0x00401920 ("      --help     display this help and exit\n",
		       uVar3);
      func_0x00401920
	("      --version  output version information and exit\n", uVar3);
      func_0x00401770
	("\nIf FILE is not specified, use %s.  %s as FILE is common.\nIf ARG1 ARG2 given, -m presumed: \'am i\' or \'mom likes\' are usual.\n",
	 "/dev/null/utmp", "/dev/null/wtmp");
      local_88 = &DAT_00410f42;
      local_80 = "test invocation";
      puVar5 = &DAT_00410f42;
      local_78 = 0x410fc7;
      local_70 = "Multi-call invocation";
      local_68 = "sha224sum";
      local_60 = "sha2 utilities";
      local_58 = "sha256sum";
      local_50 = "sha2 utilities";
      local_48 = "sha384sum";
      local_40 = "sha2 utilities";
      local_38 = "sha512sum";
      local_30 = "sha2 utilities";
      local_28 = 0;
      local_20 = 0;
      ppuVar4 = &local_88;
      do
	{
	  iVar1 = func_0x00401a50 (&DAT_00410f44, puVar5);
	  if (iVar1 == 0)
	    break;
	  ppuVar4 = ppuVar4 + 2;
	  puVar5 = *ppuVar4;
	}
      while (puVar5 != (undefined *) 0x0);
      puVar5 = ppuVar4[1];
      if (puVar5 == (undefined *) 0x0)
	{
	  func_0x00401770 ("\n%s online help: <%s>\n", "GNU coreutils",
			   "https://www.gnu.org/software/coreutils/");
	  lVar2 = func_0x00401ac0 (5, 0);
	  if (lVar2 != 0)
	    {
	      iVar1 = func_0x00401970 (lVar2, &DAT_00410fe8, 3);
	      if (iVar1 != 0)
		{
		  puVar5 = &DAT_00410f44;
		  goto LAB_00403209;
		}
	    }
	  func_0x00401770 ("Full documentation at: <%s%s>\n",
			   "https://www.gnu.org/software/coreutils/",
			   &DAT_00410f44);
	LAB_00403232:
	  ;
	  puVar5 = &DAT_00410f44;
	  uVar3 = 0x410f80;
	}
      else
	{
	  func_0x00401770 ("\n%s online help: <%s>\n", "GNU coreutils",
			   "https://www.gnu.org/software/coreutils/");
	  lVar2 = func_0x00401ac0 (5, 0);
	  if (lVar2 != 0)
	    {
	      iVar1 = func_0x00401970 (lVar2, &DAT_00410fe8, 3);
	      if (iVar1 != 0)
		{
		LAB_00403209:
		  ;
		  func_0x00401770
		    ("Report %s translation bugs to <https://translationproject.org/team/>\n",
		     &DAT_00410f44);
		}
	    }
	  func_0x00401770 ("Full documentation at: <%s%s>\n",
			   "https://www.gnu.org/software/coreutils/",
			   &DAT_00410f44);
	  uVar3 = 0x4124ef;
	  if (puVar5 == &DAT_00410f44)
	    goto LAB_00403232;
	}
      func_0x00401770
	("or available locally via: info \'(coreutils) %s%s\'\n", puVar5,
	 uVar3);
    }
  while (true);
}

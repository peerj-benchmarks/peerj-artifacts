
#include "join.h"

long DAT_006175f_0_1_;
long DAT_006175f_1_1_;
long null_ARRAY_0061746_0_4_;
long null_ARRAY_0061746_40_8_;
long null_ARRAY_0061746_4_4_;
long null_ARRAY_0061746_48_8_;
long null_ARRAY_006174a_0_8_;
long null_ARRAY_006174a_8_8_;
long null_ARRAY_006175c_16_8_;
long null_ARRAY_0061760_0_8_;
long null_ARRAY_0061760_8_8_;
long null_ARRAY_0061761_0_8_;
long null_ARRAY_0061761_8_8_;
long null_ARRAY_0061762_0_8_;
long null_ARRAY_0061762_8_8_;
long null_ARRAY_0061763_0_8_;
long null_ARRAY_0061763_8_8_;
long null_ARRAY_0061778_0_8_;
long null_ARRAY_0061778_16_8_;
long null_ARRAY_0061778_24_8_;
long null_ARRAY_0061778_32_8_;
long null_ARRAY_0061778_40_8_;
long null_ARRAY_0061778_48_8_;
long null_ARRAY_0061778_8_8_;
long null_ARRAY_006177c_0_4_;
long null_ARRAY_006177c_16_8_;
long null_ARRAY_006177c_24_4_;
long null_ARRAY_006177c_32_8_;
long null_ARRAY_006177c_40_4_;
long null_ARRAY_006177c_4_4_;
long null_ARRAY_006177c_44_4_;
long null_ARRAY_006177c_48_4_;
long null_ARRAY_006177c_8_4_;
long local_2f_1_1_;
long local_5b_1_1_;
long local_9c_4_4_;
long local_a1_0_4_;
long local_a1_0_8_;
long local_a1_1_9_;
long local_a1_4_6_;
long local_a1_8_2_;
long local_a3_4_4_;
long local_a4_0_2_;
long local_a4_0_4_;
long local_a4_4_4_;
long local_a5_0_8_;
long local_a5_1_9_;
long local_a5_4_6_;
long local_a5_8_2_;
long local_a6_0_4_;
long DAT_00000008;
long DAT_00000010;
long DAT_004137cd;
long DAT_004137d0;
long DAT_00413814;
long DAT_00413816;
long DAT_0041389d;
long DAT_004138b2;
long DAT_004138b7;
long DAT_00413a98;
long DAT_00413ae0;
long DAT_00413ae4;
long DAT_00413ae8;
long DAT_00413aeb;
long DAT_00413aed;
long DAT_00413af1;
long DAT_00413af5;
long DAT_004140ab;
long DAT_0041479e;
long DAT_004148a1;
long DAT_004148a7;
long DAT_004148b9;
long DAT_004148ba;
long DAT_004148d8;
long DAT_0041495e;
long DAT_00617000;
long DAT_00617010;
long DAT_00617020;
long DAT_00617420;
long DAT_00617424;
long DAT_00617430;
long DAT_00617438;
long DAT_00617448;
long DAT_006174b0;
long DAT_006174b4;
long DAT_006174b8;
long DAT_006174bc;
long DAT_006174c0;
long DAT_006174c8;
long DAT_006174d0;
long DAT_006174e0;
long DAT_006174e8;
long DAT_00617500;
long DAT_00617508;
long DAT_00617560;
long DAT_00617561;
long DAT_006175b0;
long DAT_006175d8;
long DAT_006175e0;
long DAT_006175e8;
long DAT_006175f0;
long DAT_006175f8;
long DAT_006175fa;
long DAT_006175fb;
long DAT_006175fc;
long DAT_006175fd;
long DAT_006175fe;
long DAT_00617640;
long DAT_00617648;
long DAT_00617650;
long DAT_006177b8;
long DAT_006177f8;
long DAT_00617800;
long DAT_00617808;
long DAT_00617818;
long fde_004153d8;
long int7;
long null_ARRAY_00413980;
long null_ARRAY_00414c00;
long null_ARRAY_00414e50;
long null_ARRAY_00617460;
long null_ARRAY_006174a0;
long null_ARRAY_00617520;
long null_ARRAY_00617580;
long null_ARRAY_006175c0;
long null_ARRAY_00617600;
long null_ARRAY_00617610;
long null_ARRAY_00617620;
long null_ARRAY_00617630;
long null_ARRAY_00617680;
long null_ARRAY_00617780;
long null_ARRAY_006177c0;
long PTR_DAT_00617440;
long PTR_null_ARRAY_00617428;
long PTR_null_ARRAY_00617498;
long register0x00000020;
void
FUN_00404040 (uint uParm1)
{
  undefined8 uVar1;
  int iVar2;
  long lVar3;
  undefined *puVar4;
  undefined **ppuVar5;
  undefined *puVar6;
  undefined *local_88;
  char *local_80;
  undefined8 local_78;
  char *local_70;
  char *local_68;
  char *local_60;
  char *local_58;
  char *local_50;
  char *local_48;
  char *local_40;
  char *local_38;
  char *local_30;
  undefined8 local_28;
  undefined8 local_20;

  if (uParm1 == 0)
    goto LAB_0040406d;
  func_0x004018e0 (DAT_006174e0, "Try \'%s --help\' for more information.\n",
		   DAT_00617650);
  do
    {
      func_0x00401ad0 ((ulong) uParm1);
    LAB_0040406d:
      ;
      func_0x00401740 ("Usage: %s [OPTION]... FILE1 FILE2\n", DAT_00617650);
      uVar1 = DAT_006174c0;
      func_0x004018f0
	("For each pair of input lines with identical join fields, write a line to\nstandard output.  The default join field is the first, delimited by blanks.\n",
	 DAT_006174c0);
      func_0x004018f0
	("\nWhen FILE1 or FILE2 (not both) is -, read standard input.\n",
	 uVar1);
      func_0x004018f0
	("\n  -a FILENUM        also print unpairable lines from file FILENUM, where\n                      FILENUM is 1 or 2, corresponding to FILE1 or FILE2\n  -e EMPTY          replace missing input fields with EMPTY\n",
	 uVar1);
      func_0x004018f0
	("  -i, --ignore-case  ignore differences in case when comparing fields\n  -j FIELD          equivalent to \'-1 FIELD -2 FIELD\'\n  -o FORMAT         obey FORMAT while constructing output line\n  -t CHAR           use CHAR as input and output field separator\n",
	 uVar1);
      func_0x004018f0
	("  -v FILENUM        like -a FILENUM, but suppress joined output lines\n  -1 FIELD          join on this FIELD of file 1\n  -2 FIELD          join on this FIELD of file 2\n  --check-order     check that the input is correctly sorted, even\n                      if all input lines are pairable\n  --nocheck-order   do not check that the input is correctly sorted\n  --header          treat the first line in each file as field headers,\n                      print them without trying to pair them\n",
	 uVar1);
      func_0x004018f0
	("  -z, --zero-terminated     line delimiter is NUL, not newline\n",
	 uVar1);
      func_0x004018f0 ("      --help     display this help and exit\n",
		       uVar1);
      func_0x004018f0
	("      --version  output version information and exit\n", uVar1);
      func_0x004018f0
	("\nUnless -t CHAR is given, leading blanks separate fields and are ignored,\nelse fields are separated by CHAR.  Any FIELD is a field number counted\nfrom 1.  FORMAT is one or more comma or blank separated specifications,\neach being \'FILENUM.FIELD\' or \'0\'.  Default FORMAT outputs the join field,\nthe remaining fields from FILE1, the remaining fields from FILE2, all\nseparated by CHAR.  If FORMAT is the keyword \'auto\', then the first\nline of each file determines the number of fields output for each line.\n\nImportant: FILE1 and FILE2 must be sorted on the join fields.\nE.g., use \"sort -k 1b,1\" if \'join\' has no options,\nor use \"join -t \'\'\" if \'sort\' has no options.\nNote, comparisons honor the rules specified by \'LC_COLLATE\'.\nIf the input is not sorted and some lines cannot be joined, a\nwarning message will be given.\n",
	 uVar1);
      local_88 = &DAT_00413814;
      local_80 = "test invocation";
      puVar6 = &DAT_00413814;
      local_78 = 0x41387c;
      local_70 = "Multi-call invocation";
      local_68 = "sha224sum";
      local_60 = "sha2 utilities";
      local_58 = "sha256sum";
      local_50 = "sha2 utilities";
      local_48 = "sha384sum";
      local_40 = "sha2 utilities";
      local_38 = "sha512sum";
      local_30 = "sha2 utilities";
      local_28 = 0;
      local_20 = 0;
      ppuVar5 = &local_88;
      do
	{
	  iVar2 = func_0x00401a00 (&DAT_00413816, puVar6);
	  if (iVar2 == 0)
	    break;
	  ppuVar5 = ppuVar5 + 2;
	  puVar6 = *ppuVar5;
	}
      while (puVar6 != (undefined *) 0x0);
      puVar6 = ppuVar5[1];
      if (puVar6 == (undefined *) 0x0)
	{
	  func_0x00401740 ("\n%s online help: <%s>\n", "GNU coreutils",
			   "https://www.gnu.org/software/coreutils/");
	  lVar3 = func_0x00401a60 (5, 0);
	  if (lVar3 != 0)
	    {
	      iVar2 = func_0x00401960 (lVar3, &DAT_0041389d, 3);
	      if (iVar2 != 0)
		{
		  puVar6 = &DAT_00413816;
		  goto LAB_00404251;
		}
	    }
	  func_0x00401740 ("Full documentation at: <%s%s>\n",
			   "https://www.gnu.org/software/coreutils/",
			   &DAT_00413816);
	LAB_0040427a:
	  ;
	  puVar6 = &DAT_00413816;
	  puVar4 = (undefined *) 0x413835;
	}
      else
	{
	  func_0x00401740 ("\n%s online help: <%s>\n", "GNU coreutils",
			   "https://www.gnu.org/software/coreutils/");
	  lVar3 = func_0x00401a60 (5, 0);
	  if (lVar3 != 0)
	    {
	      iVar2 = func_0x00401960 (lVar3, &DAT_0041389d, 3);
	      if (iVar2 != 0)
		{
		LAB_00404251:
		  ;
		  func_0x00401740
		    ("Report %s translation bugs to <https://translationproject.org/team/>\n",
		     &DAT_00413816);
		}
	    }
	  func_0x00401740 ("Full documentation at: <%s%s>\n",
			   "https://www.gnu.org/software/coreutils/",
			   &DAT_00413816);
	  puVar4 = &DAT_004137d0;
	  if (puVar6 == &DAT_00413816)
	    goto LAB_0040427a;
	}
      func_0x00401740
	("or available locally via: info \'(coreutils) %s%s\'\n", puVar6,
	 puVar4);
    }
  while (true);
}

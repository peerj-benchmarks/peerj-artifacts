
#include "csplit.h"

long null_ARRAY_0062b82_0_4_;
long null_ARRAY_0062b82_40_8_;
long null_ARRAY_0062b82_4_4_;
long null_ARRAY_0062b82_48_8_;
long null_ARRAY_0062b86_0_8_;
long null_ARRAY_0062b86_8_8_;
long null_ARRAY_0062bbc_0_8_;
long null_ARRAY_0062bbc_16_8_;
long null_ARRAY_0062bbc_24_8_;
long null_ARRAY_0062bbc_32_8_;
long null_ARRAY_0062bbc_40_8_;
long null_ARRAY_0062bbc_48_8_;
long null_ARRAY_0062bbc_8_8_;
long null_ARRAY_0062bc0_0_4_;
long null_ARRAY_0062bc0_16_8_;
long null_ARRAY_0062bc0_24_4_;
long null_ARRAY_0062bc0_32_8_;
long null_ARRAY_0062bc0_40_4_;
long null_ARRAY_0062bc0_4_4_;
long null_ARRAY_0062bc0_44_4_;
long null_ARRAY_0062bc0_48_4_;
long null_ARRAY_0062bc0_8_4_;
long local_19_0_4_;
long local_19_4_4_;
long local_2f_1_1_;
long local_5b_1_1_;
long local_9_4_4_;
long local_9c_4_4_;
long local_a_0_4_;
long local_a1_0_4_;
long local_a1_0_8_;
long local_a1_1_9_;
long local_a1_4_6_;
long local_a1_8_2_;
long local_a3_4_4_;
long local_a4_0_2_;
long local_a4_0_4_;
long local_a4_4_4_;
long local_a5_0_8_;
long local_a5_1_9_;
long local_a5_4_6_;
long local_a5_8_2_;
long local_a6_0_4_;
long local_b_0_4_;
long local_d_0_1_;
long local_d_1_7_;
long DAT_00000008;
long DAT_00000010;
long DAT_00425892;
long DAT_004258bd;
long DAT_00425900;
long DAT_00425902;
long DAT_0042598d;
long DAT_0042598f;
long DAT_00425991;
long DAT_004265f8;
long DAT_004265fc;
long DAT_00426600;
long DAT_00426603;
long DAT_00426605;
long DAT_00426609;
long DAT_0042660d;
long DAT_00426bab;
long DAT_004273d8;
long DAT_004274e1;
long DAT_004274e7;
long DAT_004274f9;
long DAT_004274fa;
long DAT_00427518;
long DAT_00427558;
long DAT_00427588;
long DAT_00428206;
long DAT_00428620;
long DAT_0062b350;
long DAT_0062b360;
long DAT_0062b370;
long DAT_0062b800;
long DAT_0062b810;
long DAT_0062b870;
long DAT_0062b874;
long DAT_0062b878;
long DAT_0062b87c;
long DAT_0062b880;
long DAT_0062b890;
long DAT_0062b8a0;
long DAT_0062b8a8;
long DAT_0062b8c0;
long DAT_0062b8c8;
long DAT_0062b940;
long DAT_0062b948;
long DAT_0062b950;
long DAT_0062ba00;
long DAT_0062ba08;
long DAT_0062ba10;
long DAT_0062ba11;
long DAT_0062ba12;
long DAT_0062ba13;
long DAT_0062ba18;
long DAT_0062ba20;
long DAT_0062ba28;
long DAT_0062ba30;
long DAT_0062ba38;
long DAT_0062ba40;
long DAT_0062ba48;
long DAT_0062ba50;
long DAT_0062ba58;
long DAT_0062ba60;
long DAT_0062ba68;
long DAT_0062ba70;
long DAT_0062ba78;
long DAT_0062ba80;
long DAT_0062ba88;
long DAT_0062ba90;
long DAT_0062ba98;
long DAT_0062bbf8;
long DAT_0062bc38;
long DAT_0062bc40;
long DAT_0062bc48;
long DAT_0062bc58;
long DAT_0062bc60;
long _DYNAMIC;
long fde_00428e80;
long int7;
long null_ARRAY_004263e0;
long null_ARRAY_00426440;
long null_ARRAY_00427f40;
long null_ARRAY_00427f80;
long null_ARRAY_004284a0;
long null_ARRAY_00428770;
long null_ARRAY_0062b820;
long null_ARRAY_0062b860;
long null_ARRAY_0062b8e0;
long null_ARRAY_0062b980;
long null_ARRAY_0062bac0;
long null_ARRAY_0062bbc0;
long null_ARRAY_0062bc00;
long PTR_DAT_0062b808;
long PTR_null_ARRAY_0062b858;
long register0x00000020;
void
FUN_004040a0 (uint uParm1)
{
  int iVar1;
  long lVar2;
  undefined8 uVar3;
  undefined **ppuVar4;
  char *pcVar5;
  undefined *puVar6;
  undefined *local_88;
  char *local_80;
  undefined8 local_78;
  char *local_70;
  char *local_68;
  char *local_60;
  char *local_58;
  char *local_50;
  char *local_48;
  char *local_40;
  char *local_38;
  char *local_30;
  undefined8 local_28;
  undefined8 local_20;

  if (uParm1 == 0)
    goto LAB_004040cd;
  func_0x00401e40 (DAT_0062b8a0, "Try \'%s --help\' for more information.\n",
		   DAT_0062ba98);
  do
    {
      func_0x004020a0 ((ulong) uParm1);
    LAB_004040cd:
      ;
      func_0x00401c10 ("Usage: %s [OPTION]... FILE PATTERN...\n",
		       DAT_0062ba98);
      uVar3 = DAT_0062b880;
      func_0x00401e50
	("Output pieces of FILE separated by PATTERN(s) to files \'xx00\', \'xx01\', ...,\nand output byte counts of each piece to standard output.\n",
	 DAT_0062b880);
      func_0x00401e50 ("\nRead standard input if FILE is -\n", uVar3);
      func_0x00401e50
	("\nMandatory arguments to long options are mandatory for short options too.\n",
	 uVar3);
      func_0x00401e50
	("  -b, --suffix-format=FORMAT  use sprintf FORMAT instead of %02d\n  -f, --prefix=PREFIX        use PREFIX instead of \'xx\'\n  -k, --keep-files           do not remove output files on errors\n",
	 uVar3);
      func_0x00401e50
	("      --suppress-matched     suppress the lines matching PATTERN\n",
	 uVar3);
      func_0x00401e50
	("  -n, --digits=DIGITS        use specified number of digits instead of 2\n  -s, --quiet, --silent      do not print counts of output file sizes\n  -z, --elide-empty-files    remove empty output files\n",
	 uVar3);
      func_0x00401e50 ("      --help     display this help and exit\n",
		       uVar3);
      func_0x00401e50
	("      --version  output version information and exit\n", uVar3);
      func_0x00401e50
	("\nEach PATTERN may be:\n  INTEGER            copy up to but not including specified line number\n  /REGEXP/[OFFSET]   copy up to but not including a matching line\n  %REGEXP%[OFFSET]   skip to, but not including a matching line\n  {INTEGER}          repeat the previous pattern specified number of times\n  {*}                repeat the previous pattern as many times as possible\n\nA line OFFSET is a required \'+\' or \'-\' followed by a positive integer.\n",
	 uVar3);
      local_88 = &DAT_00425902;
      local_80 = "test invocation";
      puVar6 = &DAT_00425902;
      local_78 = 0x42596c;
      local_70 = "Multi-call invocation";
      local_68 = "sha224sum";
      local_60 = "sha2 utilities";
      local_58 = "sha256sum";
      local_50 = "sha2 utilities";
      local_48 = "sha384sum";
      local_40 = "sha2 utilities";
      local_38 = "sha512sum";
      local_30 = "sha2 utilities";
      local_28 = 0;
      local_20 = 0;
      ppuVar4 = &local_88;
      do
	{
	  iVar1 = func_0x00401fd0 ("csplit", puVar6);
	  if (iVar1 == 0)
	    break;
	  ppuVar4 = ppuVar4 + 2;
	  puVar6 = *ppuVar4;
	}
      while (puVar6 != (undefined *) 0x0);
      pcVar5 = ppuVar4[1];
      if (pcVar5 == (char *) 0x0)
	{
	  func_0x00401c10 ("\n%s online help: <%s>\n", "GNU coreutils",
			   "https://www.gnu.org/software/coreutils/");
	  lVar2 = func_0x00402030 (5, 0);
	  if (lVar2 != 0)
	    {
	      iVar1 = func_0x00401ee0 (lVar2, &DAT_0042598d, 3);
	      if (iVar1 != 0)
		{
		  pcVar5 = "csplit";
		  goto LAB_004042b1;
		}
	    }
	  func_0x00401c10 ("Full documentation at: <%s%s>\n",
			   "https://www.gnu.org/software/coreutils/",
			   "csplit");
	LAB_004042da:
	  ;
	  pcVar5 = "csplit";
	  uVar3 = 0x425925;
	}
      else
	{
	  func_0x00401c10 ("\n%s online help: <%s>\n", "GNU coreutils",
			   "https://www.gnu.org/software/coreutils/");
	  lVar2 = func_0x00402030 (5, 0);
	  if (lVar2 != 0)
	    {
	      iVar1 = func_0x00401ee0 (lVar2, &DAT_0042598d, 3);
	      if (iVar1 != 0)
		{
		LAB_004042b1:
		  ;
		  func_0x00401c10
		    ("Report %s translation bugs to <https://translationproject.org/team/>\n",
		     "csplit");
		}
	    }
	  func_0x00401c10 ("Full documentation at: <%s%s>\n",
			   "https://www.gnu.org/software/coreutils/",
			   "csplit");
	  uVar3 = 0x427517;
	  if (pcVar5 == "csplit")
	    goto LAB_004042da;
	}
      func_0x00401c10
	("or available locally via: info \'(coreutils) %s%s\'\n", pcVar5,
	 uVar3);
    }
  while (true);
}

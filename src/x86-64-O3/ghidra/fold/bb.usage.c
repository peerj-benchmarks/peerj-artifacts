
#include "fold.h"

long null_ARRAY_0061342_0_4_;
long null_ARRAY_0061342_40_8_;
long null_ARRAY_0061342_4_4_;
long null_ARRAY_0061342_48_8_;
long null_ARRAY_0061346_0_8_;
long null_ARRAY_0061346_8_8_;
long null_ARRAY_0061364_0_8_;
long null_ARRAY_0061364_16_8_;
long null_ARRAY_0061364_24_8_;
long null_ARRAY_0061364_32_8_;
long null_ARRAY_0061364_40_8_;
long null_ARRAY_0061364_48_8_;
long null_ARRAY_0061364_8_8_;
long null_ARRAY_0061368_0_4_;
long null_ARRAY_0061368_16_8_;
long null_ARRAY_0061368_24_4_;
long null_ARRAY_0061368_32_8_;
long null_ARRAY_0061368_40_4_;
long null_ARRAY_0061368_4_4_;
long null_ARRAY_0061368_44_4_;
long null_ARRAY_0061368_48_4_;
long null_ARRAY_0061368_8_4_;
long local_2f_1_1_;
long local_5b_1_1_;
long local_9c_4_4_;
long local_a1_0_4_;
long local_a1_0_8_;
long local_a1_1_9_;
long local_a1_4_6_;
long local_a1_8_2_;
long local_a3_4_4_;
long local_a4_0_2_;
long local_a4_0_4_;
long local_a4_4_4_;
long local_a5_0_8_;
long local_a5_1_9_;
long local_a5_4_6_;
long local_a5_8_2_;
long local_a6_0_4_;
long DAT_00000008;
long DAT_00000010;
long DAT_00410300;
long DAT_00410302;
long DAT_00410389;
long DAT_00410800;
long DAT_00410804;
long DAT_00410808;
long DAT_0041080b;
long DAT_0041080d;
long DAT_00410811;
long DAT_00410815;
long DAT_00410dab;
long DAT_00411428;
long DAT_00411531;
long DAT_00411537;
long DAT_00411549;
long DAT_0041154a;
long DAT_00411568;
long DAT_0041156c;
long DAT_004115f6;
long DAT_00613000;
long DAT_00613010;
long DAT_00613020;
long DAT_00613408;
long DAT_00613470;
long DAT_00613474;
long DAT_00613478;
long DAT_0061347c;
long DAT_00613480;
long DAT_00613488;
long DAT_00613490;
long DAT_006134a0;
long DAT_006134a8;
long DAT_006134c0;
long DAT_006134c8;
long DAT_00613510;
long DAT_00613518;
long DAT_00613520;
long DAT_00613521;
long DAT_00613522;
long DAT_00613528;
long DAT_00613530;
long DAT_00613538;
long DAT_006136b8;
long DAT_006136c0;
long DAT_006136c8;
long DAT_006136d8;
long _DYNAMIC;
long fde_00411f90;
long int7;
long null_ARRAY_004106c0;
long null_ARRAY_00410780;
long null_ARRAY_00411880;
long null_ARRAY_00411ad0;
long null_ARRAY_00613420;
long null_ARRAY_00613460;
long null_ARRAY_006134e0;
long null_ARRAY_00613540;
long null_ARRAY_00613640;
long null_ARRAY_00613680;
long PTR_DAT_00613400;
long PTR_null_ARRAY_00613458;
long register0x00000020;
void
FUN_00402210 (uint uParm1)
{
  int iVar1;
  long lVar2;
  undefined8 uVar3;
  undefined **ppuVar4;
  undefined *puVar5;
  undefined *local_88;
  char *local_80;
  undefined8 local_78;
  char *local_70;
  char *local_68;
  char *local_60;
  char *local_58;
  char *local_50;
  char *local_48;
  char *local_40;
  char *local_38;
  char *local_30;
  undefined8 local_28;
  undefined8 local_20;

  if (uParm1 == 0)
    goto LAB_0040223d;
  func_0x004017f0 (DAT_006134a0, "Try \'%s --help\' for more information.\n",
		   DAT_00613538);
  do
    {
      func_0x004019c0 ((ulong) uParm1);
    LAB_0040223d:
      ;
      func_0x00401650 ("Usage: %s [OPTION]... [FILE]...\n", DAT_00613538);
      uVar3 = DAT_00613480;
      func_0x00401800
	("Wrap input lines in each FILE, writing to standard output.\n",
	 DAT_00613480);
      func_0x00401800
	("\nWith no FILE, or when FILE is -, read standard input.\n", uVar3);
      func_0x00401800
	("\nMandatory arguments to long options are mandatory for short options too.\n",
	 uVar3);
      func_0x00401800
	("  -b, --bytes         count bytes rather than columns\n  -s, --spaces        break at spaces\n  -w, --width=WIDTH   use WIDTH columns instead of 80\n",
	 uVar3);
      func_0x00401800 ("      --help     display this help and exit\n",
		       uVar3);
      func_0x00401800
	("      --version  output version information and exit\n", uVar3);
      local_88 = &DAT_00410300;
      local_80 = "test invocation";
      puVar5 = &DAT_00410300;
      local_78 = 0x410368;
      local_70 = "Multi-call invocation";
      local_68 = "sha224sum";
      local_60 = "sha2 utilities";
      local_58 = "sha256sum";
      local_50 = "sha2 utilities";
      local_48 = "sha384sum";
      local_40 = "sha2 utilities";
      local_38 = "sha512sum";
      local_30 = "sha2 utilities";
      local_28 = 0;
      local_20 = 0;
      ppuVar4 = &local_88;
      do
	{
	  iVar1 = func_0x00401900 (&DAT_00410302, puVar5);
	  if (iVar1 == 0)
	    break;
	  ppuVar4 = ppuVar4 + 2;
	  puVar5 = *ppuVar4;
	}
      while (puVar5 != (undefined *) 0x0);
      puVar5 = ppuVar4[1];
      if (puVar5 == (undefined *) 0x0)
	{
	  func_0x00401650 ("\n%s online help: <%s>\n", "GNU coreutils",
			   "https://www.gnu.org/software/coreutils/");
	  lVar2 = func_0x00401960 (5, 0);
	  if (lVar2 != 0)
	    {
	      iVar1 = func_0x00401860 (lVar2, &DAT_00410389, 3);
	      if (iVar1 != 0)
		{
		  puVar5 = &DAT_00410302;
		  goto LAB_004023f9;
		}
	    }
	  func_0x00401650 ("Full documentation at: <%s%s>\n",
			   "https://www.gnu.org/software/coreutils/",
			   &DAT_00410302);
	LAB_00402422:
	  ;
	  puVar5 = &DAT_00410302;
	  uVar3 = 0x410321;
	}
      else
	{
	  func_0x00401650 ("\n%s online help: <%s>\n", "GNU coreutils",
			   "https://www.gnu.org/software/coreutils/");
	  lVar2 = func_0x00401960 (5, 0);
	  if (lVar2 != 0)
	    {
	      iVar1 = func_0x00401860 (lVar2, &DAT_00410389, 3);
	      if (iVar1 != 0)
		{
		LAB_004023f9:
		  ;
		  func_0x00401650
		    ("Report %s translation bugs to <https://translationproject.org/team/>\n",
		     &DAT_00410302);
		}
	    }
	  func_0x00401650 ("Full documentation at: <%s%s>\n",
			   "https://www.gnu.org/software/coreutils/",
			   &DAT_00410302);
	  uVar3 = 0x411567;
	  if (puVar5 == &DAT_00410302)
	    goto LAB_00402422;
	}
      func_0x00401650
	("or available locally via: info \'(coreutils) %s%s\'\n", puVar5,
	 uVar3);
    }
  while (true);
}


#include "env.h"

long null_ARRAY_006126c_0_4_;
long null_ARRAY_006126c_40_8_;
long null_ARRAY_006126c_4_4_;
long null_ARRAY_006126c_48_8_;
long null_ARRAY_0061270_0_8_;
long null_ARRAY_0061270_8_8_;
long null_ARRAY_0061294_0_8_;
long null_ARRAY_0061294_16_8_;
long null_ARRAY_0061294_24_8_;
long null_ARRAY_0061294_32_8_;
long null_ARRAY_0061294_40_8_;
long null_ARRAY_0061294_48_8_;
long null_ARRAY_0061294_8_8_;
long null_ARRAY_0061298_0_4_;
long null_ARRAY_0061298_16_8_;
long null_ARRAY_0061298_24_4_;
long null_ARRAY_0061298_32_8_;
long null_ARRAY_0061298_40_4_;
long null_ARRAY_0061298_4_4_;
long null_ARRAY_0061298_44_4_;
long null_ARRAY_0061298_48_4_;
long null_ARRAY_0061298_8_4_;
long local_2f_1_1_;
long local_5b_1_1_;
long local_9c_4_4_;
long local_a1_0_4_;
long local_a1_0_8_;
long local_a1_1_9_;
long local_a1_4_6_;
long local_a1_8_2_;
long local_a3_4_4_;
long local_a4_0_2_;
long local_a4_0_4_;
long local_a4_4_4_;
long local_a5_0_8_;
long local_a5_1_9_;
long local_a5_4_6_;
long local_a5_8_2_;
long local_a6_0_4_;
long DAT_00000008;
long DAT_00000010;
long DAT_0040f900;
long DAT_0040f902;
long DAT_0040f988;
long DAT_0040f9d3;
long DAT_0040ff38;
long DAT_0040ff3c;
long DAT_0040ff40;
long DAT_0040ff43;
long DAT_0040ff45;
long DAT_0040ff49;
long DAT_0040ff4d;
long DAT_004104eb;
long DAT_00410975;
long DAT_00410a79;
long DAT_00410a7f;
long DAT_00410a91;
long DAT_00410a92;
long DAT_00410ab0;
long DAT_00410ab4;
long DAT_00410b3e;
long DAT_006122d8;
long DAT_006122e8;
long DAT_006122f8;
long DAT_006126a8;
long DAT_00612710;
long DAT_00612714;
long DAT_00612718;
long DAT_0061271c;
long DAT_00612740;
long DAT_00612780;
long DAT_00612790;
long DAT_006127a0;
long DAT_006127a8;
long DAT_006127c0;
long DAT_006127c8;
long DAT_00612810;
long DAT_00612818;
long DAT_00612820;
long DAT_00612828;
long DAT_006129b8;
long DAT_006129c0;
long DAT_006129c8;
long DAT_006129d0;
long DAT_006129e0;
long _DYNAMIC;
long fde_004114d8;
long int7;
long null_ARRAY_0040fe00;
long null_ARRAY_00410de0;
long null_ARRAY_00411030;
long null_ARRAY_006126c0;
long null_ARRAY_00612700;
long null_ARRAY_006127e0;
long null_ARRAY_00612840;
long null_ARRAY_00612940;
long null_ARRAY_00612980;
long PTR_DAT_006126a0;
long PTR_null_ARRAY_006126f8;
long register0x00000020;
long stack0x00000008;
void
FUN_00401e50 (uint uParm1)
{
  int iVar1;
  long lVar2;
  undefined8 uVar3;
  undefined **ppuVar4;
  undefined *puVar5;
  undefined *local_88;
  char *local_80;
  undefined8 local_78;
  char *local_70;
  char *local_68;
  char *local_60;
  char *local_58;
  char *local_50;
  char *local_48;
  char *local_40;
  char *local_38;
  char *local_30;
  undefined8 local_28;
  undefined8 local_20;

  if (uParm1 == 0)
    goto LAB_00401e7d;
  func_0x004016a0 (DAT_006127a0, "Try \'%s --help\' for more information.\n",
		   DAT_00612828);
  do
    {
      func_0x00401830 ((ulong) uParm1);
    LAB_00401e7d:
      ;
      func_0x00401530
	("Usage: %s [OPTION]... [-] [NAME=VALUE]... [COMMAND [ARG]...]\n",
	 DAT_00612828);
      uVar3 = DAT_00612740;
      func_0x004016b0
	("Set each NAME to VALUE in the environment and run COMMAND.\n",
	 DAT_00612740);
      func_0x004016b0
	("\nMandatory arguments to long options are mandatory for short options too.\n",
	 uVar3);
      func_0x004016b0
	("  -i, --ignore-environment  start with an empty environment\n  -0, --null           end each output line with NUL, not newline\n  -u, --unset=NAME     remove variable from the environment\n",
	 uVar3);
      func_0x004016b0
	("  -C, --chdir=DIR      change working directory to DIR\n", uVar3);
      func_0x004016b0 ("      --help     display this help and exit\n",
		       uVar3);
      func_0x004016b0
	("      --version  output version information and exit\n", uVar3);
      func_0x004016b0
	("\nA mere - implies -i.  If no COMMAND, print the resulting environment.\n",
	 uVar3);
      local_88 = &DAT_0040f900;
      local_80 = "test invocation";
      puVar5 = &DAT_0040f900;
      local_78 = 0x40f967;
      local_70 = "Multi-call invocation";
      local_68 = "sha224sum";
      local_60 = "sha2 utilities";
      local_58 = "sha256sum";
      local_50 = "sha2 utilities";
      local_48 = "sha384sum";
      local_40 = "sha2 utilities";
      local_38 = "sha512sum";
      local_30 = "sha2 utilities";
      local_28 = 0;
      local_20 = 0;
      ppuVar4 = &local_88;
      do
	{
	  iVar1 = func_0x00401790 (&DAT_0040f902, puVar5);
	  if (iVar1 == 0)
	    break;
	  ppuVar4 = ppuVar4 + 2;
	  puVar5 = *ppuVar4;
	}
      while (puVar5 != (undefined *) 0x0);
      puVar5 = ppuVar4[1];
      if (puVar5 == (undefined *) 0x0)
	{
	  func_0x00401530 ("\n%s online help: <%s>\n", "GNU coreutils",
			   "https://www.gnu.org/software/coreutils/");
	  lVar2 = func_0x004017f0 (5, 0);
	  if (lVar2 != 0)
	    {
	      iVar1 = func_0x00401710 (lVar2, &DAT_0040f988, 3);
	      if (iVar1 != 0)
		{
		  puVar5 = &DAT_0040f902;
		  goto LAB_00402049;
		}
	    }
	  func_0x00401530 ("Full documentation at: <%s%s>\n",
			   "https://www.gnu.org/software/coreutils/",
			   &DAT_0040f902);
	LAB_00402072:
	  ;
	  puVar5 = &DAT_0040f902;
	  uVar3 = 0x40f920;
	}
      else
	{
	  func_0x00401530 ("\n%s online help: <%s>\n", "GNU coreutils",
			   "https://www.gnu.org/software/coreutils/");
	  lVar2 = func_0x004017f0 (5, 0);
	  if (lVar2 != 0)
	    {
	      iVar1 = func_0x00401710 (lVar2, &DAT_0040f988, 3);
	      if (iVar1 != 0)
		{
		LAB_00402049:
		  ;
		  func_0x00401530
		    ("Report %s translation bugs to <https://translationproject.org/team/>\n",
		     &DAT_0040f902);
		}
	    }
	  func_0x00401530 ("Full documentation at: <%s%s>\n",
			   "https://www.gnu.org/software/coreutils/",
			   &DAT_0040f902);
	  uVar3 = 0x410aaf;
	  if (puVar5 == &DAT_0040f902)
	    goto LAB_00402072;
	}
      func_0x00401530
	("or available locally via: info \'(coreutils) %s%s\'\n", puVar5,
	 uVar3);
    }
  while (true);
}


#include "users.h"

long null_ARRAY_006126a_0_4_;
long null_ARRAY_006126a_40_8_;
long null_ARRAY_006126a_4_4_;
long null_ARRAY_006126a_48_8_;
long null_ARRAY_006126e_0_8_;
long null_ARRAY_006126e_8_8_;
long null_ARRAY_006128c_0_8_;
long null_ARRAY_006128c_16_8_;
long null_ARRAY_006128c_24_8_;
long null_ARRAY_006128c_32_8_;
long null_ARRAY_006128c_40_8_;
long null_ARRAY_006128c_48_8_;
long null_ARRAY_006128c_8_8_;
long null_ARRAY_0061290_0_4_;
long null_ARRAY_0061290_16_8_;
long null_ARRAY_0061290_24_4_;
long null_ARRAY_0061290_32_8_;
long null_ARRAY_0061290_40_4_;
long null_ARRAY_0061290_4_4_;
long null_ARRAY_0061290_44_4_;
long null_ARRAY_0061290_48_4_;
long null_ARRAY_0061290_8_4_;
long local_2f_1_1_;
long local_5b_1_1_;
long local_9c_4_4_;
long local_a1_0_4_;
long local_a1_0_8_;
long local_a1_1_9_;
long local_a1_4_6_;
long local_a1_8_2_;
long local_a3_4_4_;
long local_a4_0_2_;
long local_a4_0_4_;
long local_a4_4_4_;
long local_a5_0_8_;
long local_a5_1_9_;
long local_a5_4_6_;
long local_a5_8_2_;
long local_a6_0_4_;
long DAT_00000008;
long DAT_00000010;
long DAT_0040fa40;
long DAT_0040fb06;
long DAT_0040fd58;
long DAT_0040fe20;
long DAT_0040fe24;
long DAT_0040fe28;
long DAT_0040fe2b;
long DAT_0040fe2d;
long DAT_0040fe31;
long DAT_0040fe35;
long DAT_004103eb;
long DAT_00410875;
long DAT_00410979;
long DAT_0041097f;
long DAT_00410991;
long DAT_00410992;
long DAT_004109b0;
long DAT_004109b4;
long DAT_00410a3e;
long DAT_00612298;
long DAT_006122a8;
long DAT_006122b8;
long DAT_00612688;
long DAT_006126f0;
long DAT_006126f4;
long DAT_006126f8;
long DAT_006126fc;
long DAT_00612700;
long DAT_00612710;
long DAT_00612720;
long DAT_00612728;
long DAT_00612740;
long DAT_00612748;
long DAT_00612790;
long DAT_00612798;
long DAT_006127a0;
long DAT_00612938;
long DAT_00612940;
long DAT_00612948;
long DAT_00612958;
long fde_004113f0;
long int7;
long null_ARRAY_0040fd20;
long null_ARRAY_0040fd80;
long null_ARRAY_00410ce0;
long null_ARRAY_00410f30;
long null_ARRAY_006126a0;
long null_ARRAY_006126e0;
long null_ARRAY_00612760;
long null_ARRAY_006127c0;
long null_ARRAY_006128c0;
long null_ARRAY_00612900;
long PTR_DAT_00612680;
long PTR_null_ARRAY_006126d8;
long register0x00000020;
void
FUN_00401de0 (uint uParm1)
{
  int iVar1;
  long lVar2;
  undefined8 uVar3;
  undefined **ppuVar4;
  char *pcVar5;
  undefined *puVar6;
  undefined *local_88;
  char *local_80;
  undefined8 local_78;
  char *local_70;
  char *local_68;
  char *local_60;
  char *local_58;
  char *local_50;
  char *local_48;
  char *local_40;
  char *local_38;
  char *local_30;
  undefined8 local_28;
  undefined8 local_20;

  if (uParm1 == 0)
    goto LAB_00401e0d;
  func_0x00401710 (DAT_00612720, "Try \'%s --help\' for more information.\n",
		   DAT_006127a0);
  do
    {
      func_0x004018e0 ((ulong) uParm1);
    LAB_00401e0d:
      ;
      func_0x00401590 ("Usage: %s [OPTION]... [FILE]\n", DAT_006127a0);
      func_0x00401590
	("Output who is currently logged in according to FILE.\nIf FILE is not specified, use %s.  %s as FILE is common.\n\n",
	 "/dev/null/utmp", "/dev/null/wtmp");
      uVar3 = DAT_00612700;
      func_0x00401730 ("      --help     display this help and exit\n",
		       DAT_00612700);
      func_0x00401730
	("      --version  output version information and exit\n", uVar3);
      local_88 = &DAT_0040fa40;
      local_80 = "test invocation";
      puVar6 = &DAT_0040fa40;
      local_78 = 0x40fae5;
      local_70 = "Multi-call invocation";
      local_68 = "sha224sum";
      local_60 = "sha2 utilities";
      local_58 = "sha256sum";
      local_50 = "sha2 utilities";
      local_48 = "sha384sum";
      local_40 = "sha2 utilities";
      local_38 = "sha512sum";
      local_30 = "sha2 utilities";
      local_28 = 0;
      local_20 = 0;
      ppuVar4 = &local_88;
      do
	{
	  iVar1 = func_0x00401830 ("users", puVar6);
	  if (iVar1 == 0)
	    break;
	  ppuVar4 = ppuVar4 + 2;
	  puVar6 = *ppuVar4;
	}
      while (puVar6 != (undefined *) 0x0);
      pcVar5 = ppuVar4[1];
      if (pcVar5 == (char *) 0x0)
	{
	  func_0x00401590 ("\n%s online help: <%s>\n", "GNU coreutils",
			   "https://www.gnu.org/software/coreutils/");
	  lVar2 = func_0x00401890 (5, 0);
	  if (lVar2 != 0)
	    {
	      iVar1 = func_0x00401780 (lVar2, &DAT_0040fb06, 3);
	      if (iVar1 != 0)
		{
		  pcVar5 = "users";
		  goto LAB_00401fa9;
		}
	    }
	  func_0x00401590 ("Full documentation at: <%s%s>\n",
			   "https://www.gnu.org/software/coreutils/",
			   "users");
	LAB_00401fd2:
	  ;
	  pcVar5 = "users";
	  uVar3 = 0x40fa9e;
	}
      else
	{
	  func_0x00401590 ("\n%s online help: <%s>\n", "GNU coreutils",
			   "https://www.gnu.org/software/coreutils/");
	  lVar2 = func_0x00401890 (5, 0);
	  if (lVar2 != 0)
	    {
	      iVar1 = func_0x00401780 (lVar2, &DAT_0040fb06, 3);
	      if (iVar1 != 0)
		{
		LAB_00401fa9:
		  ;
		  func_0x00401590
		    ("Report %s translation bugs to <https://translationproject.org/team/>\n",
		     "users");
		}
	    }
	  func_0x00401590 ("Full documentation at: <%s%s>\n",
			   "https://www.gnu.org/software/coreutils/",
			   "users");
	  uVar3 = 0x4109af;
	  if (pcVar5 == "users")
	    goto LAB_00401fd2;
	}
      func_0x00401590
	("or available locally via: info \'(coreutils) %s%s\'\n", pcVar5,
	 uVar3);
    }
  while (true);
}

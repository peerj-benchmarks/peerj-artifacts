
#include "echo.h"

long null_ARRAY_0061140_0_8_;
long null_ARRAY_0061140_8_8_;
long null_ARRAY_0061160_0_8_;
long null_ARRAY_0061160_16_8_;
long null_ARRAY_0061160_24_8_;
long null_ARRAY_0061160_32_8_;
long null_ARRAY_0061160_40_8_;
long null_ARRAY_0061160_48_8_;
long null_ARRAY_0061160_8_8_;
long local_2f_1_1_;
long local_5b_1_1_;
long local_9c_4_4_;
long local_a1_0_4_;
long local_a1_0_8_;
long local_a1_1_9_;
long local_a1_4_6_;
long local_a1_8_2_;
long local_a3_4_4_;
long local_a4_0_2_;
long local_a4_0_4_;
long local_a4_4_4_;
long local_a5_0_8_;
long local_a5_1_9_;
long local_a5_4_6_;
long local_a5_8_2_;
long local_a6_0_4_;
long uVar8;
long DAT_0040e578;
long DAT_0040e57a;
long DAT_0040e601;
long DAT_0040e615;
long DAT_0040eb70;
long DAT_0040eb74;
long DAT_0040eb76;
long DAT_0040eb7a;
long DAT_0040eb7d;
long DAT_0040eb7f;
long DAT_0040eb83;
long DAT_0040eb87;
long DAT_0040f12b;
long DAT_0040f5b5;
long DAT_0040f5c6;
long DAT_0040f64e;
long DAT_00611000;
long DAT_00611010;
long DAT_00611020;
long DAT_006113a8;
long DAT_00611410;
long DAT_00611440;
long DAT_00611450;
long DAT_00611460;
long DAT_00611468;
long DAT_00611480;
long DAT_00611488;
long DAT_006114d0;
long DAT_006114d8;
long DAT_006114e0;
long DAT_00611638;
long DAT_00611640;
long DAT_00611648;
long _DYNAMIC;
long fde_0040ffa0;
long null_ARRAY_0040f8e0;
long null_ARRAY_0040fb30;
long null_ARRAY_00611400;
long null_ARRAY_006114a0;
long null_ARRAY_00611500;
long null_ARRAY_00611600;
long PTR_DAT_006113a0;
long PTR_null_ARRAY_006113f8;
long register0x00000020;
void
FUN_00401e10 (uint uParm1)
{
  int iVar1;
  long lVar2;
  undefined8 uVar3;
  undefined **ppuVar4;
  undefined *puVar5;
  undefined *local_88;
  char *local_80;
  undefined8 local_78;
  char *local_70;
  char *local_68;
  char *local_60;
  char *local_58;
  char *local_50;
  char *local_48;
  char *local_40;
  char *local_38;
  char *local_30;
  undefined8 local_28;
  undefined8 local_20;

  if (uParm1 == 0)
    goto LAB_00401e3d;
  func_0x00401560 (DAT_00611460, "Try \'%s --help\' for more information.\n",
		   DAT_006114e0);
  do
    {
      func_0x004016d0 ((ulong) uParm1);
    LAB_00401e3d:
      ;
      func_0x004013e0
	("Usage: %s [SHORT-OPTION]... [STRING]...\n  or:  %s LONG-OPTION\n",
	 DAT_006114e0, DAT_006114e0);
      uVar3 = DAT_00611440;
      func_0x00401570
	("Echo the STRING(s) to standard output.\n\n  -n             do not output the trailing newline\n",
	 DAT_00611440);
      func_0x00401570
	("  -e             enable interpretation of backslash escapes\n  -E             disable interpretation of backslash escapes (default)\n",
	 uVar3);
      func_0x00401570 ("      --help     display this help and exit\n",
		       uVar3);
      func_0x00401570
	("      --version  output version information and exit\n", uVar3);
      func_0x00401570
	("\nIf -e is in effect, the following sequences are recognized:\n\n",
	 uVar3);
      func_0x00401570
	("  \\\\      backslash\n  \\a      alert (BEL)\n  \\b      backspace\n  \\c      produce no further output\n  \\e      escape\n  \\f      form feed\n  \\n      new line\n  \\r      carriage return\n  \\t      horizontal tab\n  \\v      vertical tab\n",
	 uVar3);
      func_0x00401570
	("  \\0NNN   byte with octal value NNN (1 to 3 digits)\n  \\xHH    byte with hexadecimal value HH (1 to 2 digits)\n",
	 uVar3);
      func_0x004013e0
	("\nNOTE: your shell may have its own version of %s, which usually supersedes\nthe version described here.  Please refer to your shell\'s documentation\nfor details about the options it supports.\n",
	 &DAT_0040e57a);
      local_88 = &DAT_0040e578;
      local_80 = "test invocation";
      puVar5 = &DAT_0040e578;
      local_78 = 0x40e5e0;
      local_70 = "Multi-call invocation";
      local_68 = "sha224sum";
      local_60 = "sha2 utilities";
      local_58 = "sha256sum";
      local_50 = "sha2 utilities";
      local_48 = "sha384sum";
      local_40 = "sha2 utilities";
      local_38 = "sha512sum";
      local_30 = "sha2 utilities";
      local_28 = 0;
      local_20 = 0;
      ppuVar4 = &local_88;
      do
	{
	  iVar1 = func_0x00401630 (&DAT_0040e57a, puVar5);
	  if (iVar1 == 0)
	    break;
	  ppuVar4 = ppuVar4 + 2;
	  puVar5 = *ppuVar4;
	}
      while (puVar5 != (undefined *) 0x0);
      puVar5 = ppuVar4[1];
      if (puVar5 == (undefined *) 0x0)
	{
	  func_0x004013e0 ("\n%s online help: <%s>\n", "GNU coreutils",
			   "https://www.gnu.org/software/coreutils/");
	  lVar2 = func_0x00401680 (5, 0);
	  if (lVar2 != 0)
	    {
	      iVar1 = func_0x004015c0 (lVar2, &DAT_0040e601, 3);
	      if (iVar1 != 0)
		{
		  puVar5 = &DAT_0040e57a;
		  goto LAB_00402019;
		}
	    }
	  func_0x004013e0 ("Full documentation at: <%s%s>\n",
			   "https://www.gnu.org/software/coreutils/",
			   &DAT_0040e57a);
	LAB_00402042:
	  ;
	  puVar5 = &DAT_0040e57a;
	  uVar3 = 0x40e599;
	}
      else
	{
	  func_0x004013e0 ("\n%s online help: <%s>\n", "GNU coreutils",
			   "https://www.gnu.org/software/coreutils/");
	  lVar2 = func_0x00401680 (5, 0);
	  if (lVar2 != 0)
	    {
	      iVar1 = func_0x004015c0 (lVar2, &DAT_0040e601, 3);
	      if (iVar1 != 0)
		{
		LAB_00402019:
		  ;
		  func_0x004013e0
		    ("Report %s translation bugs to <https://translationproject.org/team/>\n",
		     &DAT_0040e57a);
		}
	    }
	  func_0x004013e0 ("Full documentation at: <%s%s>\n",
			   "https://www.gnu.org/software/coreutils/",
			   &DAT_0040e57a);
	  uVar3 = 0x40f171;
	  if (puVar5 == &DAT_0040e57a)
	    goto LAB_00402042;
	}
      func_0x004013e0
	("or available locally via: info \'(coreutils) %s%s\'\n", puVar5,
	 uVar3);
    }
  while (true);
}

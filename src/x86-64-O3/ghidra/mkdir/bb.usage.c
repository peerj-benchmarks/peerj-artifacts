
#include "mkdir.h"

long null_ARRAY_00614c4_0_4_;
long null_ARRAY_00614c4_40_8_;
long null_ARRAY_00614c4_4_4_;
long null_ARRAY_00614c4_48_8_;
long null_ARRAY_00614c8_0_8_;
long null_ARRAY_00614c8_8_8_;
long null_ARRAY_00614e8_0_8_;
long null_ARRAY_00614e8_16_8_;
long null_ARRAY_00614e8_24_8_;
long null_ARRAY_00614e8_32_8_;
long null_ARRAY_00614e8_40_8_;
long null_ARRAY_00614e8_48_8_;
long null_ARRAY_00614e8_8_8_;
long null_ARRAY_00614ec_0_4_;
long null_ARRAY_00614ec_16_8_;
long null_ARRAY_00614ec_24_4_;
long null_ARRAY_00614ec_32_8_;
long null_ARRAY_00614ec_40_4_;
long null_ARRAY_00614ec_4_4_;
long null_ARRAY_00614ec_44_4_;
long null_ARRAY_00614ec_48_4_;
long null_ARRAY_00614ec_8_4_;
long local_2f_1_1_;
long local_5b_1_1_;
long local_9c_4_4_;
long local_a1_0_4_;
long local_a1_0_8_;
long local_a1_1_9_;
long local_a1_4_6_;
long local_a1_8_2_;
long local_a3_4_4_;
long local_a4_0_2_;
long local_a4_0_4_;
long local_a4_4_4_;
long local_a5_0_8_;
long local_a5_1_9_;
long local_a5_4_6_;
long local_a5_8_2_;
long local_a6_0_4_;
long DAT_00000008;
long DAT_00000010;
long DAT_00411909;
long DAT_0041190b;
long DAT_0041190f;
long DAT_00411993;
long DAT_00411b38;
long DAT_00411cf0;
long DAT_00411cf4;
long DAT_00411cf8;
long DAT_00411cfb;
long DAT_00411cfd;
long DAT_00411d01;
long DAT_00411d05;
long DAT_004123d9;
long DAT_00412855;
long DAT_00412857;
long DAT_00412959;
long DAT_0041295f;
long DAT_00412971;
long DAT_00412972;
long DAT_00412990;
long DAT_00412994;
long DAT_00412a1e;
long DAT_006147d8;
long DAT_006147e8;
long DAT_006147f8;
long DAT_00614c28;
long DAT_00614c90;
long DAT_00614c94;
long DAT_00614c98;
long DAT_00614c9c;
long DAT_00614cc0;
long DAT_00614cd0;
long DAT_00614ce0;
long DAT_00614ce8;
long DAT_00614d00;
long DAT_00614d08;
long DAT_00614d50;
long DAT_00614d58;
long DAT_00614d60;
long DAT_00614ef8;
long DAT_00614f00;
long DAT_00614f08;
long DAT_00614f10;
long DAT_00614f20;
long fde_00413470;
long int7;
long null_ARRAY_00411a40;
long null_ARRAY_00412cc0;
long null_ARRAY_00412f10;
long null_ARRAY_00614c40;
long null_ARRAY_00614c80;
long null_ARRAY_00614d20;
long null_ARRAY_00614d80;
long null_ARRAY_00614e80;
long null_ARRAY_00614ec0;
long PTR_DAT_00614c20;
long PTR_null_ARRAY_00614c78;
void
FUN_00402360 (uint uParm1)
{
  int iVar1;
  long lVar2;
  undefined8 uVar3;
  undefined **ppuVar4;
  undefined *puVar5;
  undefined *local_88;
  char *local_80;
  undefined8 local_78;
  char *local_70;
  char *local_68;
  char *local_60;
  char *local_58;
  char *local_50;
  char *local_48;
  char *local_40;
  char *local_38;
  char *local_30;
  undefined8 local_28;
  undefined8 local_20;

  if (uParm1 == 0)
    goto LAB_0040238d;
  func_0x00401a10 (DAT_00614ce0, "Try \'%s --help\' for more information.\n",
		   DAT_00614d60);
  do
    {
      func_0x00401c00 ((ulong) uParm1);
    LAB_0040238d:
      ;
      func_0x00401830 ("Usage: %s [OPTION]... DIRECTORY...\n", DAT_00614d60);
      uVar3 = DAT_00614cc0;
      func_0x00401a20
	("Create the DIRECTORY(ies), if they do not already exist.\n",
	 DAT_00614cc0);
      func_0x00401a20
	("\nMandatory arguments to long options are mandatory for short options too.\n",
	 uVar3);
      func_0x00401a20
	("  -m, --mode=MODE   set file mode (as in chmod), not a=rwx - umask\n  -p, --parents     no error if existing, make parent directories as needed\n  -v, --verbose     print a message for each created directory\n",
	 uVar3);
      func_0x00401a20
	("  -Z                   set SELinux security context of each created directory\n                         to the default type\n      --context[=CTX]  like -Z, or if CTX is specified then set the SELinux\n                         or SMACK security context to CTX\n",
	 uVar3);
      func_0x00401a20 ("      --help     display this help and exit\n",
		       uVar3);
      func_0x00401a20
	("      --version  output version information and exit\n", uVar3);
      local_88 = &DAT_00411909;
      local_80 = "test invocation";
      puVar5 = &DAT_00411909;
      local_78 = 0x411972;
      local_70 = "Multi-call invocation";
      local_68 = "sha224sum";
      local_60 = "sha2 utilities";
      local_58 = "sha256sum";
      local_50 = "sha2 utilities";
      local_48 = "sha384sum";
      local_40 = "sha2 utilities";
      local_38 = "sha512sum";
      local_30 = "sha2 utilities";
      local_28 = 0;
      local_20 = 0;
      ppuVar4 = &local_88;
      do
	{
	  iVar1 = func_0x00401b50 (&DAT_0041190b, puVar5);
	  if (iVar1 == 0)
	    break;
	  ppuVar4 = ppuVar4 + 2;
	  puVar5 = *ppuVar4;
	}
      while (puVar5 != (undefined *) 0x0);
      puVar5 = ppuVar4[1];
      if (puVar5 == (undefined *) 0x0)
	{
	  func_0x00401830 ("\n%s online help: <%s>\n", "GNU coreutils",
			   "https://www.gnu.org/software/coreutils/");
	  lVar2 = func_0x00401bb0 (5, 0);
	  if (lVar2 != 0)
	    {
	      iVar1 = func_0x00401aa0 (lVar2, &DAT_00411993, 3);
	      if (iVar1 != 0)
		{
		  puVar5 = &DAT_0041190b;
		  goto LAB_00402549;
		}
	    }
	  func_0x00401830 ("Full documentation at: <%s%s>\n",
			   "https://www.gnu.org/software/coreutils/",
			   &DAT_0041190b);
	LAB_00402572:
	  ;
	  puVar5 = &DAT_0041190b;
	  uVar3 = 0x41192b;
	}
      else
	{
	  func_0x00401830 ("\n%s online help: <%s>\n", "GNU coreutils",
			   "https://www.gnu.org/software/coreutils/");
	  lVar2 = func_0x00401bb0 (5, 0);
	  if (lVar2 != 0)
	    {
	      iVar1 = func_0x00401aa0 (lVar2, &DAT_00411993, 3);
	      if (iVar1 != 0)
		{
		LAB_00402549:
		  ;
		  func_0x00401830
		    ("Report %s translation bugs to <https://translationproject.org/team/>\n",
		     &DAT_0041190b);
		}
	    }
	  func_0x00401830 ("Full documentation at: <%s%s>\n",
			   "https://www.gnu.org/software/coreutils/",
			   &DAT_0041190b);
	  uVar3 = 0x41298f;
	  if (puVar5 == &DAT_0041190b)
	    goto LAB_00402572;
	}
      func_0x00401830
	("or available locally via: info \'(coreutils) %s%s\'\n", puVar5,
	 uVar3);
    }
  while (true);
}

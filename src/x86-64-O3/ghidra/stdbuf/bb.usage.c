
#include "stdbuf.h"

long null_ARRAY_0061442_0_4_;
long null_ARRAY_0061442_40_8_;
long null_ARRAY_0061442_4_4_;
long null_ARRAY_0061442_48_8_;
long null_ARRAY_0061446_0_8_;
long null_ARRAY_0061446_8_8_;
long null_ARRAY_0061470_0_8_;
long null_ARRAY_0061470_16_8_;
long null_ARRAY_0061470_24_8_;
long null_ARRAY_0061470_32_8_;
long null_ARRAY_0061470_40_8_;
long null_ARRAY_0061470_48_8_;
long null_ARRAY_0061470_8_8_;
long null_ARRAY_0061474_0_4_;
long null_ARRAY_0061474_16_8_;
long null_ARRAY_0061474_24_4_;
long null_ARRAY_0061474_32_8_;
long null_ARRAY_0061474_40_4_;
long null_ARRAY_0061474_4_4_;
long null_ARRAY_0061474_44_4_;
long null_ARRAY_0061474_48_4_;
long null_ARRAY_0061474_8_4_;
long local_2f_1_1_;
long local_5b_1_1_;
long local_9c_4_4_;
long local_a1_0_4_;
long local_a1_0_8_;
long local_a1_1_9_;
long local_a1_4_6_;
long local_a1_8_2_;
long local_a3_4_4_;
long local_a4_0_2_;
long local_a4_0_4_;
long local_a4_4_4_;
long local_a5_0_8_;
long local_a5_1_9_;
long local_a5_4_6_;
long local_a5_8_2_;
long local_a6_0_4_;
long DAT_00000008;
long DAT_00000010;
long DAT_00410800;
long DAT_004108a8;
long DAT_004108e2;
long DAT_004108e8;
long DAT_00410903;
long DAT_00410908;
long DAT_00410922;
long DAT_00411218;
long DAT_0041121c;
long DAT_00411220;
long DAT_00411223;
long DAT_00411225;
long DAT_00411229;
long DAT_0041122d;
long DAT_004117ab;
long DAT_00411e28;
long DAT_00411f31;
long DAT_00411f37;
long DAT_00411f49;
long DAT_00411f4a;
long DAT_00411f68;
long DAT_00411f6c;
long DAT_00411ff6;
long DAT_00614000;
long DAT_00614010;
long DAT_00614020;
long DAT_00614408;
long DAT_00614470;
long DAT_00614474;
long DAT_00614478;
long DAT_0061447c;
long DAT_00614480;
long DAT_006144c0;
long DAT_006144d0;
long DAT_006144e0;
long DAT_006144e8;
long DAT_00614500;
long DAT_00614508;
long DAT_006145c8;
long DAT_006145d0;
long DAT_006145d8;
long DAT_006145e0;
long DAT_00614778;
long DAT_00614780;
long DAT_00614788;
long DAT_00614790;
long DAT_006147a0;
long _DYNAMIC;
long fde_00412a08;
long int7;
long null_ARRAY_004110c0;
long null_ARRAY_00411100;
long null_ARRAY_00412280;
long null_ARRAY_00412400;
long null_ARRAY_004124f0;
long null_ARRAY_00614420;
long null_ARRAY_00614460;
long null_ARRAY_00614520;
long null_ARRAY_00614580;
long null_ARRAY_00614600;
long null_ARRAY_00614700;
long null_ARRAY_00614740;
long PTR_null_ARRAY_00614458;
long register0x00000020;
void
FUN_00402270 (uint uParm1)
{
  int iVar1;
  long lVar2;
  undefined8 uVar3;
  undefined **ppuVar4;
  char *pcVar5;
  undefined *puVar6;
  undefined *local_88;
  char *local_80;
  undefined8 local_78;
  char *local_70;
  char *local_68;
  char *local_60;
  char *local_58;
  char *local_50;
  char *local_48;
  char *local_40;
  char *local_38;
  char *local_30;
  undefined8 local_28;
  undefined8 local_20;

  if (uParm1 == 0)
    goto LAB_0040229d;
  func_0x00401830 (DAT_006144e0, "Try \'%s --help\' for more information.\n",
		   DAT_006145e0);
  do
    {
      func_0x004019f0 ((ulong) uParm1);
    LAB_0040229d:
      ;
      func_0x00401690 ("Usage: %s OPTION... COMMAND\n", DAT_006145e0);
      uVar3 = DAT_00614480;
      func_0x00401840
	("Run COMMAND, with modified buffering operations for its standard streams.\n",
	 DAT_00614480);
      func_0x00401840
	("\nMandatory arguments to long options are mandatory for short options too.\n",
	 uVar3);
      func_0x00401840
	("  -i, --input=MODE   adjust standard input stream buffering\n  -o, --output=MODE  adjust standard output stream buffering\n  -e, --error=MODE   adjust standard error stream buffering\n",
	 uVar3);
      func_0x00401840 ("      --help     display this help and exit\n",
		       uVar3);
      func_0x00401840
	("      --version  output version information and exit\n", uVar3);
      func_0x00401840
	("\nIf MODE is \'L\' the corresponding stream will be line buffered.\nThis option is invalid with standard input.\n",
	 uVar3);
      func_0x00401840
	("\nIf MODE is \'0\' the corresponding stream will be unbuffered.\n",
	 uVar3);
      func_0x00401840
	("\nOtherwise MODE is a number which may be followed by one of the following:\nKB 1000, K 1024, MB 1000*1000, M 1024*1024, and so on for G, T, P, E, Z, Y.\nIn this case the corresponding stream will be fully buffered with the buffer\nsize set to MODE bytes.\n",
	 uVar3);
      func_0x00401840
	("\nNOTE: If COMMAND adjusts the buffering of its standard streams (\'tee\' does\nfor example) then that will override corresponding changes by \'stdbuf\'.\nAlso some filters (like \'dd\' and \'cat\' etc.) don\'t use streams for I/O,\nand are thus unaffected by \'stdbuf\' settings.\n",
	 uVar3);
      local_88 = &DAT_00410800;
      local_80 = "test invocation";
      puVar6 = &DAT_00410800;
      local_78 = 0x410887;
      local_70 = "Multi-call invocation";
      local_68 = "sha224sum";
      local_60 = "sha2 utilities";
      local_58 = "sha256sum";
      local_50 = "sha2 utilities";
      local_48 = "sha384sum";
      local_40 = "sha2 utilities";
      local_38 = "sha512sum";
      local_30 = "sha2 utilities";
      local_28 = 0;
      local_20 = 0;
      ppuVar4 = &local_88;
      do
	{
	  iVar1 = func_0x00401930 ("stdbuf", puVar6);
	  if (iVar1 == 0)
	    break;
	  ppuVar4 = ppuVar4 + 2;
	  puVar6 = *ppuVar4;
	}
      while (puVar6 != (undefined *) 0x0);
      pcVar5 = ppuVar4[1];
      if (pcVar5 == (char *) 0x0)
	{
	  func_0x00401690 ("\n%s online help: <%s>\n", "GNU coreutils",
			   "https://www.gnu.org/software/coreutils/");
	  lVar2 = func_0x00401990 (5, 0);
	  if (lVar2 != 0)
	    {
	      iVar1 = func_0x00401890 (lVar2, &DAT_004108a8, 3);
	      if (iVar1 != 0)
		{
		  pcVar5 = "stdbuf";
		  goto LAB_00402481;
		}
	    }
	  func_0x00401690 ("Full documentation at: <%s%s>\n",
			   "https://www.gnu.org/software/coreutils/",
			   "stdbuf");
	LAB_004024aa:
	  ;
	  pcVar5 = "stdbuf";
	  uVar3 = 0x410840;
	}
      else
	{
	  func_0x00401690 ("\n%s online help: <%s>\n", "GNU coreutils",
			   "https://www.gnu.org/software/coreutils/");
	  lVar2 = func_0x00401990 (5, 0);
	  if (lVar2 != 0)
	    {
	      iVar1 = func_0x00401890 (lVar2, &DAT_004108a8, 3);
	      if (iVar1 != 0)
		{
		LAB_00402481:
		  ;
		  func_0x00401690
		    ("Report %s translation bugs to <https://translationproject.org/team/>\n",
		     "stdbuf");
		}
	    }
	  func_0x00401690 ("Full documentation at: <%s%s>\n",
			   "https://www.gnu.org/software/coreutils/",
			   "stdbuf");
	  uVar3 = 0x411f67;
	  if (pcVar5 == "stdbuf")
	    goto LAB_004024aa;
	}
      func_0x00401690
	("or available locally via: info \'(coreutils) %s%s\'\n", pcVar5,
	 uVar3);
    }
  while (true);
}

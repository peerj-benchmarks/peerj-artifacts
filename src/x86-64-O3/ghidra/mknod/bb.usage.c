
#include "mknod.h"

long null_ARRAY_0061440_0_4_;
long null_ARRAY_0061440_40_8_;
long null_ARRAY_0061440_4_4_;
long null_ARRAY_0061440_48_8_;
long null_ARRAY_0061444_0_8_;
long null_ARRAY_0061444_8_8_;
long null_ARRAY_0061464_0_8_;
long null_ARRAY_0061464_16_8_;
long null_ARRAY_0061464_24_8_;
long null_ARRAY_0061464_32_8_;
long null_ARRAY_0061464_40_8_;
long null_ARRAY_0061464_48_8_;
long null_ARRAY_0061464_8_8_;
long null_ARRAY_0061468_0_4_;
long null_ARRAY_0061468_16_8_;
long null_ARRAY_0061468_24_4_;
long null_ARRAY_0061468_32_8_;
long null_ARRAY_0061468_40_4_;
long null_ARRAY_0061468_4_4_;
long null_ARRAY_0061468_44_4_;
long null_ARRAY_0061468_48_4_;
long null_ARRAY_0061468_8_4_;
long local_2f_1_1_;
long local_5b_1_1_;
long local_9c_4_4_;
long local_a1_0_4_;
long local_a1_0_8_;
long local_a1_1_9_;
long local_a1_4_6_;
long local_a1_8_2_;
long local_a3_4_4_;
long local_a4_0_2_;
long local_a4_0_4_;
long local_a4_4_4_;
long local_a5_0_8_;
long local_a5_1_9_;
long local_a5_4_6_;
long local_a5_8_2_;
long local_a6_0_4_;
long DAT_00000008;
long DAT_00000010;
long DAT_00410880;
long DAT_0041090a;
long DAT_0041091e;
long DAT_00411280;
long DAT_00411284;
long DAT_00411288;
long DAT_0041128b;
long DAT_0041128d;
long DAT_00411291;
long DAT_00411295;
long DAT_0041182b;
long DAT_00411ea8;
long DAT_00411fb1;
long DAT_00411fb7;
long DAT_00411fc9;
long DAT_00411fca;
long DAT_00411fe8;
long DAT_00411fec;
long DAT_00412076;
long DAT_00614000;
long DAT_00614010;
long DAT_00614020;
long DAT_006143e8;
long DAT_00614450;
long DAT_00614454;
long DAT_00614458;
long DAT_0061445c;
long DAT_00614480;
long DAT_00614490;
long DAT_006144a0;
long DAT_006144a8;
long DAT_006144c0;
long DAT_006144c8;
long DAT_00614510;
long DAT_00614518;
long DAT_00614520;
long DAT_006146b8;
long DAT_006146c0;
long DAT_006146c8;
long DAT_006146d8;
long fde_00412a20;
long int7;
long null_ARRAY_00411080;
long null_ARRAY_00412300;
long null_ARRAY_00412550;
long null_ARRAY_00614400;
long null_ARRAY_00614440;
long null_ARRAY_006144e0;
long null_ARRAY_00614540;
long null_ARRAY_00614640;
long null_ARRAY_00614680;
long PTR_DAT_006143e0;
long PTR_null_ARRAY_00614438;
long register0x00000020;
void
FUN_004020c0 (uint uParm1)
{
  int iVar1;
  long lVar2;
  undefined8 uVar3;
  undefined **ppuVar4;
  char *pcVar5;
  undefined *puVar6;
  undefined *local_88;
  char *local_80;
  undefined8 local_78;
  char *local_70;
  char *local_68;
  char *local_60;
  char *local_58;
  char *local_50;
  char *local_48;
  char *local_40;
  char *local_38;
  char *local_30;
  undefined8 local_28;
  undefined8 local_20;

  if (uParm1 == 0)
    goto LAB_004020ed;
  func_0x00401750 (DAT_006144a0, "Try \'%s --help\' for more information.\n",
		   DAT_00614520);
  do
    {
      func_0x00401920 ((ulong) uParm1);
    LAB_004020ed:
      ;
      func_0x004015c0 ("Usage: %s [OPTION]... NAME TYPE [MAJOR MINOR]\n",
		       DAT_00614520);
      uVar3 = DAT_00614480;
      func_0x00401760 ("Create the special file NAME of the given TYPE.\n",
		       DAT_00614480);
      func_0x00401760
	("\nMandatory arguments to long options are mandatory for short options too.\n",
	 uVar3);
      func_0x00401760
	("  -m, --mode=MODE    set file permission bits to MODE, not a=rw - umask\n",
	 uVar3);
      func_0x00401760
	("  -Z                   set the SELinux security context to default type\n      --context[=CTX]  like -Z, or if CTX is specified then set the SELinux\n                         or SMACK security context to CTX\n",
	 uVar3);
      func_0x00401760 ("      --help     display this help and exit\n",
		       uVar3);
      func_0x00401760
	("      --version  output version information and exit\n", uVar3);
      func_0x00401760
	("\nBoth MAJOR and MINOR must be specified when TYPE is b, c, or u, and they\nmust be omitted when TYPE is p.  If MAJOR or MINOR begins with 0x or 0X,\nit is interpreted as hexadecimal; otherwise, if it begins with 0, as octal;\notherwise, as decimal.  TYPE may be:\n",
	 uVar3);
      func_0x00401760
	("\n  b      create a block (buffered) special file\n  c, u   create a character (unbuffered) special file\n  p      create a FIFO\n",
	 uVar3);
      func_0x004015c0
	("\nNOTE: your shell may have its own version of %s, which usually supersedes\nthe version described here.  Please refer to your shell\'s documentation\nfor details about the options it supports.\n",
	 "mknod");
      local_88 = &DAT_00410880;
      local_80 = "test invocation";
      puVar6 = &DAT_00410880;
      local_78 = 0x4108e9;
      local_70 = "Multi-call invocation";
      local_68 = "sha224sum";
      local_60 = "sha2 utilities";
      local_58 = "sha256sum";
      local_50 = "sha2 utilities";
      local_48 = "sha384sum";
      local_40 = "sha2 utilities";
      local_38 = "sha512sum";
      local_30 = "sha2 utilities";
      local_28 = 0;
      local_20 = 0;
      ppuVar4 = &local_88;
      do
	{
	  iVar1 = func_0x00401870 ("mknod", puVar6);
	  if (iVar1 == 0)
	    break;
	  ppuVar4 = ppuVar4 + 2;
	  puVar6 = *ppuVar4;
	}
      while (puVar6 != (undefined *) 0x0);
      pcVar5 = ppuVar4[1];
      if (pcVar5 == (char *) 0x0)
	{
	  func_0x004015c0 ("\n%s online help: <%s>\n", "GNU coreutils",
			   "https://www.gnu.org/software/coreutils/");
	  lVar2 = func_0x004018d0 (5, 0);
	  if (lVar2 != 0)
	    {
	      iVar1 = func_0x004017d0 (lVar2, &DAT_0041090a, 3);
	      if (iVar1 != 0)
		{
		  pcVar5 = "mknod";
		  goto LAB_004022d9;
		}
	    }
	  func_0x004015c0 ("Full documentation at: <%s%s>\n",
			   "https://www.gnu.org/software/coreutils/",
			   "mknod");
	LAB_00402302:
	  ;
	  pcVar5 = "mknod";
	  uVar3 = 0x4108a2;
	}
      else
	{
	  func_0x004015c0 ("\n%s online help: <%s>\n", "GNU coreutils",
			   "https://www.gnu.org/software/coreutils/");
	  lVar2 = func_0x004018d0 (5, 0);
	  if (lVar2 != 0)
	    {
	      iVar1 = func_0x004017d0 (lVar2, &DAT_0041090a, 3);
	      if (iVar1 != 0)
		{
		LAB_004022d9:
		  ;
		  func_0x004015c0
		    ("Report %s translation bugs to <https://translationproject.org/team/>\n",
		     "mknod");
		}
	    }
	  func_0x004015c0 ("Full documentation at: <%s%s>\n",
			   "https://www.gnu.org/software/coreutils/",
			   "mknod");
	  uVar3 = 0x411fe7;
	  if (pcVar5 == "mknod")
	    goto LAB_00402302;
	}
      func_0x004015c0
	("or available locally via: info \'(coreutils) %s%s\'\n", pcVar5,
	 uVar3);
    }
  while (true);
}

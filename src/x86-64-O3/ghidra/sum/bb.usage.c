
#include "sum.h"

long null_ARRAY_006135e_0_8_;
long null_ARRAY_006135e_8_8_;
long null_ARRAY_006137c_0_8_;
long null_ARRAY_006137c_16_8_;
long null_ARRAY_006137c_24_8_;
long null_ARRAY_006137c_32_8_;
long null_ARRAY_006137c_40_8_;
long null_ARRAY_006137c_48_8_;
long null_ARRAY_006137c_8_8_;
long null_ARRAY_0061380_0_4_;
long null_ARRAY_0061380_16_8_;
long null_ARRAY_0061380_24_4_;
long null_ARRAY_0061380_32_8_;
long null_ARRAY_0061380_40_4_;
long null_ARRAY_0061380_4_4_;
long null_ARRAY_0061380_44_4_;
long null_ARRAY_0061380_48_4_;
long null_ARRAY_0061380_8_4_;
long local_2f_1_1_;
long local_5b_1_1_;
long local_9c_4_4_;
long local_a1_0_4_;
long local_a1_0_8_;
long local_a1_1_9_;
long local_a1_4_6_;
long local_a1_8_2_;
long local_a3_4_4_;
long local_a4_0_2_;
long local_a4_0_4_;
long local_a4_4_4_;
long local_a5_0_8_;
long local_a5_1_9_;
long local_a5_4_6_;
long local_a5_8_2_;
long local_a6_0_4_;
long DAT_00000008;
long DAT_00000010;
long DAT_004108cf;
long DAT_00410924;
long DAT_0041092a;
long DAT_00410953;
long DAT_00410979;
long DAT_00410c98;
long DAT_00410d80;
long DAT_00410d84;
long DAT_00410d88;
long DAT_00410d8b;
long DAT_00410d8d;
long DAT_00410d91;
long DAT_00410d95;
long DAT_0041132b;
long DAT_004117b5;
long DAT_004118b9;
long DAT_004118bf;
long DAT_004118c1;
long DAT_004118c2;
long DAT_004118e0;
long DAT_004118e4;
long DAT_0041196e;
long DAT_00613190;
long DAT_006131a0;
long DAT_006131b0;
long DAT_00613588;
long DAT_006135f0;
long DAT_006135f4;
long DAT_006135f8;
long DAT_006135fc;
long DAT_00613600;
long DAT_00613608;
long DAT_00613610;
long DAT_00613620;
long DAT_00613628;
long DAT_00613640;
long DAT_00613648;
long DAT_00613690;
long DAT_00613698;
long DAT_006136a0;
long DAT_006136a8;
long DAT_00613838;
long DAT_00613840;
long DAT_00613848;
long DAT_00613858;
long fde_00412308;
long int7;
long null_ARRAY_00410c00;
long null_ARRAY_00410d18;
long null_ARRAY_00411c00;
long null_ARRAY_00411e50;
long null_ARRAY_006135e0;
long null_ARRAY_00613660;
long null_ARRAY_006136c0;
long null_ARRAY_006137c0;
long null_ARRAY_00613800;
long PTR_DAT_00613580;
long PTR_null_ARRAY_006135d8;
long register0x00000020;
long stack0x00000008;
long stack0xffffffffffffffd8;
void
FUN_004022b0 (uint uParm1)
{
  int iVar1;
  long lVar2;
  undefined8 uVar3;
  undefined **ppuVar4;
  undefined *puVar5;
  undefined *local_88;
  char *local_80;
  undefined8 local_78;
  char *local_70;
  char *local_68;
  char *local_60;
  char *local_58;
  char *local_50;
  char *local_48;
  char *local_40;
  undefined *local_38;
  char *local_30;
  undefined8 local_28;
  undefined8 local_20;

  if (uParm1 == 0)
    goto LAB_004022dd;
  func_0x00401790 (DAT_00613620, "Try \'%s --help\' for more information.\n",
		   DAT_006136a8);
  do
    {
      func_0x00401960 ((ulong) uParm1);
    LAB_004022dd:
      ;
      func_0x00401600 ("Usage: %s [OPTION]... [FILE]...\n", DAT_006136a8);
      uVar3 = DAT_00613600;
      func_0x004017a0 ("Print checksum and block counts for each FILE.\n",
		       DAT_00613600);
      func_0x004017a0
	("\nWith no FILE, or when FILE is -, read standard input.\n", uVar3);
      func_0x004017a0
	("\n  -r              use BSD sum algorithm, use 1K blocks\n  -s, --sysv      use System V sum algorithm, use 512 bytes blocks\n",
	 uVar3);
      func_0x004017a0 ("      --help     display this help and exit\n",
		       uVar3);
      func_0x004017a0
	("      --version  output version information and exit\n", uVar3);
      local_88 = &DAT_004108cf;
      local_80 = "test invocation";
      puVar5 = &DAT_004108cf;
      local_78 = 0x410932;
      local_70 = "Multi-call invocation";
      local_68 = "sha224sum";
      local_60 = "sha2 utilities";
      local_58 = "sha256sum";
      local_50 = "sha2 utilities";
      local_48 = "sha384sum";
      local_40 = "sha2 utilities";
      local_38 = &DAT_00410924;
      local_30 = "sha2 utilities";
      local_28 = 0;
      local_20 = 0;
      ppuVar4 = &local_88;
      do
	{
	  iVar1 = func_0x004018a0 (&DAT_0041092a, puVar5);
	  if (iVar1 == 0)
	    break;
	  ppuVar4 = ppuVar4 + 2;
	  puVar5 = *ppuVar4;
	}
      while (puVar5 != (undefined *) 0x0);
      puVar5 = ppuVar4[1];
      if (puVar5 == (undefined *) 0x0)
	{
	  func_0x00401600 ("\n%s online help: <%s>\n", "GNU coreutils",
			   "https://www.gnu.org/software/coreutils/");
	  lVar2 = func_0x00401900 (5, 0);
	  if (lVar2 != 0)
	    {
	      iVar1 = func_0x00401810 (lVar2, &DAT_00410953, 3);
	      if (iVar1 != 0)
		{
		  puVar5 = &DAT_0041092a;
		  goto LAB_00402489;
		}
	    }
	  func_0x00401600 ("Full documentation at: <%s%s>\n",
			   "https://www.gnu.org/software/coreutils/",
			   &DAT_0041092a);
	LAB_004024b2:
	  ;
	  puVar5 = &DAT_0041092a;
	  uVar3 = 0x4108eb;
	}
      else
	{
	  func_0x00401600 ("\n%s online help: <%s>\n", "GNU coreutils",
			   "https://www.gnu.org/software/coreutils/");
	  lVar2 = func_0x00401900 (5, 0);
	  if (lVar2 != 0)
	    {
	      iVar1 = func_0x00401810 (lVar2, &DAT_00410953, 3);
	      if (iVar1 != 0)
		{
		LAB_00402489:
		  ;
		  func_0x00401600
		    ("Report %s translation bugs to <https://translationproject.org/team/>\n",
		     &DAT_0041092a);
		}
	    }
	  func_0x00401600 ("Full documentation at: <%s%s>\n",
			   "https://www.gnu.org/software/coreutils/",
			   &DAT_0041092a);
	  uVar3 = 0x4118df;
	  if (puVar5 == &DAT_0041092a)
	    goto LAB_004024b2;
	}
      func_0x00401600
	("or available locally via: info \'(coreutils) %s%s\'\n", puVar5,
	 uVar3);
    }
  while (true);
}

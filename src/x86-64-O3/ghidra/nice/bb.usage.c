
#include "nice.h"

long null_ARRAY_006129c_0_4_;
long null_ARRAY_006129c_40_8_;
long null_ARRAY_006129c_4_4_;
long null_ARRAY_006129c_48_8_;
long null_ARRAY_00612a0_0_8_;
long null_ARRAY_00612a0_8_8_;
long null_ARRAY_00612c0_0_8_;
long null_ARRAY_00612c0_16_8_;
long null_ARRAY_00612c0_24_8_;
long null_ARRAY_00612c0_32_8_;
long null_ARRAY_00612c0_40_8_;
long null_ARRAY_00612c0_48_8_;
long null_ARRAY_00612c0_8_8_;
long null_ARRAY_00612c4_0_4_;
long null_ARRAY_00612c4_16_8_;
long null_ARRAY_00612c4_24_4_;
long null_ARRAY_00612c4_32_8_;
long null_ARRAY_00612c4_40_4_;
long null_ARRAY_00612c4_4_4_;
long null_ARRAY_00612c4_44_4_;
long null_ARRAY_00612c4_48_4_;
long null_ARRAY_00612c4_8_4_;
long local_2f_1_1_;
long local_5b_1_1_;
long local_9c_4_4_;
long local_a1_0_4_;
long local_a1_0_8_;
long local_a1_1_9_;
long local_a1_4_6_;
long local_a1_8_2_;
long local_a3_4_4_;
long local_a4_0_2_;
long local_a4_0_4_;
long local_a4_4_4_;
long local_a5_0_8_;
long local_a5_1_9_;
long local_a5_4_6_;
long local_a5_8_2_;
long local_a6_0_4_;
long DAT_00000008;
long DAT_00000010;
long DAT_0040fb40;
long DAT_0040fb42;
long DAT_0040fbc9;
long DAT_0040fbcd;
long DAT_0040fc0b;
long DAT_00410118;
long DAT_0041011c;
long DAT_00410120;
long DAT_00410123;
long DAT_00410125;
long DAT_00410129;
long DAT_0041012d;
long DAT_004106ab;
long DAT_00410d25;
long DAT_00410e29;
long DAT_00410e2f;
long DAT_00410e41;
long DAT_00410e42;
long DAT_00410e60;
long DAT_00410e64;
long DAT_00410eee;
long DAT_006125d0;
long DAT_006125e0;
long DAT_006125f0;
long DAT_006129a8;
long DAT_00612a10;
long DAT_00612a14;
long DAT_00612a18;
long DAT_00612a1c;
long DAT_00612a40;
long DAT_00612a50;
long DAT_00612a60;
long DAT_00612a68;
long DAT_00612a80;
long DAT_00612a88;
long DAT_00612ad0;
long DAT_00612ad8;
long DAT_00612ae0;
long DAT_00612c78;
long DAT_00612c80;
long DAT_00612c88;
long DAT_00612c98;
long _DYNAMIC;
long fde_00411868;
long int7;
long null_ARRAY_00410040;
long null_ARRAY_00411180;
long null_ARRAY_004113d0;
long null_ARRAY_006129c0;
long null_ARRAY_00612a00;
long null_ARRAY_00612aa0;
long null_ARRAY_00612b00;
long null_ARRAY_00612c00;
long null_ARRAY_00612c40;
long PTR_DAT_006129a0;
long PTR_null_ARRAY_006129f8;
long register0x00000020;
void
FUN_00401e70 (uint uParm1)
{
  int iVar1;
  long lVar2;
  undefined8 uVar3;
  undefined **ppuVar4;
  undefined *puVar5;
  undefined *local_88;
  char *local_80;
  undefined8 local_78;
  char *local_70;
  char *local_68;
  char *local_60;
  char *local_58;
  char *local_50;
  char *local_48;
  char *local_40;
  char *local_38;
  char *local_30;
  undefined8 local_28;
  undefined8 local_20;

  if (uParm1 == 0)
    goto LAB_00401e9d;
  func_0x004016f0 (DAT_00612a60, "Try \'%s --help\' for more information.\n",
		   DAT_00612ae0);
  do
    {
      func_0x00401880 ((ulong) uParm1);
    LAB_00401e9d:
      ;
      func_0x00401550 ("Usage: %s [OPTION] [COMMAND [ARG]...]\n",
		       DAT_00612ae0);
      func_0x00401550
	("Run COMMAND with an adjusted niceness, which affects process scheduling.\nWith no COMMAND, print the current niceness.  Niceness values range from\n%d (most favorable to the process) to %d (least favorable to the process).\n",
	 0xffffffec, 0x13);
      uVar3 = DAT_00612a40;
      func_0x00401700
	("\nMandatory arguments to long options are mandatory for short options too.\n",
	 DAT_00612a40);
      func_0x00401700
	("  -n, --adjustment=N   add integer N to the niceness (default 10)\n",
	 uVar3);
      func_0x00401700 ("      --help     display this help and exit\n",
		       uVar3);
      func_0x00401700
	("      --version  output version information and exit\n", uVar3);
      func_0x00401550
	("\nNOTE: your shell may have its own version of %s, which usually supersedes\nthe version described here.  Please refer to your shell\'s documentation\nfor details about the options it supports.\n",
	 &DAT_0040fb42);
      local_88 = &DAT_0040fb40;
      local_80 = "test invocation";
      puVar5 = &DAT_0040fb40;
      local_78 = 0x40fba8;
      local_70 = "Multi-call invocation";
      local_68 = "sha224sum";
      local_60 = "sha2 utilities";
      local_58 = "sha256sum";
      local_50 = "sha2 utilities";
      local_48 = "sha384sum";
      local_40 = "sha2 utilities";
      local_38 = "sha512sum";
      local_30 = "sha2 utilities";
      local_28 = 0;
      local_20 = 0;
      ppuVar4 = &local_88;
      do
	{
	  iVar1 = func_0x004017e0 (&DAT_0040fb42, puVar5);
	  if (iVar1 == 0)
	    break;
	  ppuVar4 = ppuVar4 + 2;
	  puVar5 = *ppuVar4;
	}
      while (puVar5 != (undefined *) 0x0);
      puVar5 = ppuVar4[1];
      if (puVar5 == (undefined *) 0x0)
	{
	  func_0x00401550 ("\n%s online help: <%s>\n", "GNU coreutils",
			   "https://www.gnu.org/software/coreutils/");
	  lVar2 = func_0x00401840 (5, 0);
	  if (lVar2 != 0)
	    {
	      iVar1 = func_0x00401750 (lVar2, &DAT_0040fbc9, 3);
	      if (iVar1 != 0)
		{
		  puVar5 = &DAT_0040fb42;
		  goto LAB_00402069;
		}
	    }
	  func_0x00401550 ("Full documentation at: <%s%s>\n",
			   "https://www.gnu.org/software/coreutils/",
			   &DAT_0040fb42);
	LAB_00402092:
	  ;
	  puVar5 = &DAT_0040fb42;
	  uVar3 = 0x40fb61;
	}
      else
	{
	  func_0x00401550 ("\n%s online help: <%s>\n", "GNU coreutils",
			   "https://www.gnu.org/software/coreutils/");
	  lVar2 = func_0x00401840 (5, 0);
	  if (lVar2 != 0)
	    {
	      iVar1 = func_0x00401750 (lVar2, &DAT_0040fbc9, 3);
	      if (iVar1 != 0)
		{
		LAB_00402069:
		  ;
		  func_0x00401550
		    ("Report %s translation bugs to <https://translationproject.org/team/>\n",
		     &DAT_0040fb42);
		}
	    }
	  func_0x00401550 ("Full documentation at: <%s%s>\n",
			   "https://www.gnu.org/software/coreutils/",
			   &DAT_0040fb42);
	  uVar3 = 0x410e5f;
	  if (puVar5 == &DAT_0040fb42)
	    goto LAB_00402092;
	}
      func_0x00401550
	("or available locally via: info \'(coreutils) %s%s\'\n", puVar5,
	 uVar3);
    }
  while (true);
}


#include "chown.h"

long null_ARRAY_0061c4e_0_4_;
long null_ARRAY_0061c4e_40_8_;
long null_ARRAY_0061c4e_4_4_;
long null_ARRAY_0061c4e_48_8_;
long null_ARRAY_0061c52_0_8_;
long null_ARRAY_0061c52_8_8_;
long null_ARRAY_0061c70_0_8_;
long null_ARRAY_0061c70_16_8_;
long null_ARRAY_0061c70_24_8_;
long null_ARRAY_0061c70_32_8_;
long null_ARRAY_0061c70_40_8_;
long null_ARRAY_0061c70_48_8_;
long null_ARRAY_0061c70_8_8_;
long null_ARRAY_0061c74_0_4_;
long null_ARRAY_0061c74_16_8_;
long null_ARRAY_0061c74_24_4_;
long null_ARRAY_0061c74_32_8_;
long null_ARRAY_0061c74_40_4_;
long null_ARRAY_0061c74_4_4_;
long null_ARRAY_0061c74_44_4_;
long null_ARRAY_0061c74_48_4_;
long null_ARRAY_0061c74_8_4_;
long local_2f_1_1_;
long local_5b_1_1_;
long local_7_0_4_;
long local_9c_4_4_;
long local_a1_0_4_;
long local_a1_0_8_;
long local_a1_1_9_;
long local_a1_4_6_;
long local_a1_8_2_;
long local_a3_4_4_;
long local_a4_0_2_;
long local_a4_0_4_;
long local_a4_4_4_;
long local_a5_0_8_;
long local_a5_1_9_;
long local_a5_4_6_;
long local_a5_8_2_;
long local_a6_0_4_;
long DAT_00000008;
long DAT_00000010;
long DAT_004170c0;
long DAT_0041714a;
long DAT_00418340;
long DAT_00418344;
long DAT_00418348;
long DAT_0041834b;
long DAT_0041834d;
long DAT_00418351;
long DAT_00418355;
long DAT_00418913;
long DAT_00418fae;
long DAT_00418fbd;
long DAT_00418fbf;
long DAT_00418fc0;
long DAT_00419119;
long DAT_0041911a;
long DAT_00419138;
long DAT_004191a9;
long DAT_004192f5;
long DAT_004192ff;
long DAT_0041934a;
long DAT_0061c000;
long DAT_0061c010;
long DAT_0061c020;
long DAT_0061c4c8;
long DAT_0061c530;
long DAT_0061c534;
long DAT_0061c538;
long DAT_0061c53c;
long DAT_0061c540;
long DAT_0061c550;
long DAT_0061c560;
long DAT_0061c568;
long DAT_0061c580;
long DAT_0061c588;
long DAT_0061c5e0;
long DAT_0061c5e8;
long DAT_0061c5f0;
long DAT_0061c5f8;
long DAT_0061c778;
long DAT_0061c77c;
long DAT_0061c780;
long DAT_0061c788;
long DAT_0061c790;
long DAT_0061c798;
long DAT_0061c7a8;
long fde_00419ed8;
long int7;
long null_ARRAY_00417d00;
long null_ARRAY_00418fe0;
long null_ARRAY_004191c0;
long null_ARRAY_004195e0;
long null_ARRAY_00419820;
long null_ARRAY_0061c4e0;
long null_ARRAY_0061c520;
long null_ARRAY_0061c5a0;
long null_ARRAY_0061c5d0;
long null_ARRAY_0061c600;
long null_ARRAY_0061c700;
long null_ARRAY_0061c740;
long PTR_DAT_0061c4c0;
long PTR_null_ARRAY_0061c518;
long register0x00000020;
void
FUN_00402820 (uint uParm1)
{
  int iVar1;
  long lVar2;
  undefined8 uVar3;
  undefined **ppuVar4;
  char *pcVar5;
  undefined *puVar6;
  undefined *local_88;
  char *local_80;
  undefined8 local_78;
  char *local_70;
  char *local_68;
  char *local_60;
  char *local_58;
  char *local_50;
  char *local_48;
  char *local_40;
  char *local_38;
  char *local_30;
  undefined8 local_28;
  undefined8 local_20;

  if (uParm1 == 0)
    goto LAB_0040284d;
  func_0x00401ed0 (DAT_0061c560, "Try \'%s --help\' for more information.\n",
		   DAT_0061c5f8);
  do
    {
      func_0x004020f0 ((ulong) uParm1);
    LAB_0040284d:
      ;
      func_0x00401c50
	("Usage: %s [OPTION]... [OWNER][:[GROUP]] FILE...\n  or:  %s [OPTION]... --reference=RFILE FILE...\n",
	 DAT_0061c5f8, DAT_0061c5f8);
      uVar3 = DAT_0061c540;
      func_0x00401ee0
	("Change the owner and/or group of each FILE to OWNER and/or GROUP.\nWith --reference, change the owner and group of each FILE to those of RFILE.\n\n",
	 DAT_0061c540);
      func_0x00401ee0
	("  -c, --changes          like verbose but report only when a change is made\n  -f, --silent, --quiet  suppress most error messages\n  -v, --verbose          output a diagnostic for every file processed\n",
	 uVar3);
      func_0x00401ee0
	("      --dereference      affect the referent of each symbolic link (this is\n                         the default), rather than the symbolic link itself\n  -h, --no-dereference   affect symbolic links instead of any referenced file\n",
	 uVar3);
      func_0x00401ee0
	("                         (useful only on systems that can change the\n                         ownership of a symlink)\n",
	 uVar3);
      func_0x00401ee0
	("      --from=CURRENT_OWNER:CURRENT_GROUP\n                         change the owner and/or group of each file only if\n                         its current owner and/or group match those specified\n                         here.  Either may be omitted, in which case a match\n                         is not required for the omitted attribute\n",
	 uVar3);
      func_0x00401ee0
	("      --no-preserve-root  do not treat \'/\' specially (the default)\n      --preserve-root    fail to operate recursively on \'/\'\n",
	 uVar3);
      func_0x00401ee0
	("      --reference=RFILE  use RFILE\'s owner and group rather than\n                         specifying OWNER:GROUP values\n",
	 uVar3);
      func_0x00401ee0
	("  -R, --recursive        operate on files and directories recursively\n",
	 uVar3);
      func_0x00401ee0
	("\nThe following options modify how a hierarchy is traversed when the -R\noption is also specified.  If more than one is specified, only the final\none takes effect.\n\n  -H                     if a command line argument is a symbolic link\n                         to a directory, traverse it\n  -L                     traverse every symbolic link to a directory\n                         encountered\n  -P                     do not traverse any symbolic links (default)\n\n",
	 uVar3);
      func_0x00401ee0 ("      --help     display this help and exit\n",
		       uVar3);
      func_0x00401ee0
	("      --version  output version information and exit\n", uVar3);
      func_0x00401ee0
	("\nOwner is unchanged if missing.  Group is unchanged if missing, but changed\nto login group if implied by a \':\' following a symbolic OWNER.\nOWNER and GROUP may be numeric as well as symbolic.\n",
	 uVar3);
      func_0x00401c50
	("\nExamples:\n  %s root /u        Change the owner of /u to \"root\".\n  %s root:staff /u  Likewise, but also change its group to \"staff\".\n  %s -hR root /u    Change the owner of /u and subfiles to \"root\".\n",
	 DAT_0061c5f8, DAT_0061c5f8, DAT_0061c5f8);
      local_88 = &DAT_004170c0;
      local_80 = "test invocation";
      puVar6 = &DAT_004170c0;
      local_78 = 0x417129;
      local_70 = "Multi-call invocation";
      local_68 = "sha224sum";
      local_60 = "sha2 utilities";
      local_58 = "sha256sum";
      local_50 = "sha2 utilities";
      local_48 = "sha384sum";
      local_40 = "sha2 utilities";
      local_38 = "sha512sum";
      local_30 = "sha2 utilities";
      local_28 = 0;
      local_20 = 0;
      ppuVar4 = &local_88;
      do
	{
	  iVar1 = func_0x00402010 ("chown", puVar6);
	  if (iVar1 == 0)
	    break;
	  ppuVar4 = ppuVar4 + 2;
	  puVar6 = *ppuVar4;
	}
      while (puVar6 != (undefined *) 0x0);
      pcVar5 = ppuVar4[1];
      if (pcVar5 == (char *) 0x0)
	{
	  func_0x00401c50 ("\n%s online help: <%s>\n", "GNU coreutils",
			   "https://www.gnu.org/software/coreutils/");
	  lVar2 = func_0x00402090 (5, 0);
	  if (lVar2 != 0)
	    {
	      iVar1 = func_0x00401f70 (lVar2, &DAT_0041714a, 3);
	      if (iVar1 != 0)
		{
		  pcVar5 = "chown";
		  goto LAB_00402a79;
		}
	    }
	  func_0x00401c50 ("Full documentation at: <%s%s>\n",
			   "https://www.gnu.org/software/coreutils/",
			   "chown");
	LAB_00402aa2:
	  ;
	  pcVar5 = "chown";
	  uVar3 = 0x4170e2;
	}
      else
	{
	  func_0x00401c50 ("\n%s online help: <%s>\n", "GNU coreutils",
			   "https://www.gnu.org/software/coreutils/");
	  lVar2 = func_0x00402090 (5, 0);
	  if (lVar2 != 0)
	    {
	      iVar1 = func_0x00401f70 (lVar2, &DAT_0041714a, 3);
	      if (iVar1 != 0)
		{
		LAB_00402a79:
		  ;
		  func_0x00401c50
		    ("Report %s translation bugs to <https://translationproject.org/team/>\n",
		     "chown");
		}
	    }
	  func_0x00401c50 ("Full documentation at: <%s%s>\n",
			   "https://www.gnu.org/software/coreutils/",
			   "chown");
	  uVar3 = 0x419137;
	  if (pcVar5 == "chown")
	    goto LAB_00402aa2;
	}
      func_0x00401c50
	("or available locally via: info \'(coreutils) %s%s\'\n", pcVar5,
	 uVar3);
    }
  while (true);
}

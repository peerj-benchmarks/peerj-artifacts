
#include "runcon.h"

long null_ARRAY_0061242_0_8_;
long null_ARRAY_0061242_8_8_;
long null_ARRAY_0061260_0_8_;
long null_ARRAY_0061260_16_8_;
long null_ARRAY_0061260_24_8_;
long null_ARRAY_0061260_32_8_;
long null_ARRAY_0061260_40_8_;
long null_ARRAY_0061260_48_8_;
long null_ARRAY_0061260_8_8_;
long null_ARRAY_0061264_0_4_;
long null_ARRAY_0061264_16_8_;
long null_ARRAY_0061264_24_4_;
long null_ARRAY_0061264_32_8_;
long null_ARRAY_0061264_40_4_;
long null_ARRAY_0061264_4_4_;
long null_ARRAY_0061264_44_4_;
long null_ARRAY_0061264_48_4_;
long null_ARRAY_0061264_8_4_;
long local_2f_1_1_;
long local_5b_1_1_;
long local_9c_4_4_;
long local_a1_0_4_;
long local_a1_0_8_;
long local_a1_1_9_;
long local_a1_4_6_;
long local_a1_8_2_;
long local_a3_4_4_;
long local_a4_0_2_;
long local_a4_0_4_;
long local_a4_4_4_;
long local_a5_0_8_;
long local_a5_1_9_;
long local_a5_4_6_;
long local_a5_8_2_;
long local_a6_0_4_;
long DAT_00000008;
long DAT_00000010;
long DAT_0040f100;
long DAT_0040f18b;
long DAT_0040f7d8;
long DAT_0040f7dc;
long DAT_0040f7e0;
long DAT_0040f7e3;
long DAT_0040f7e5;
long DAT_0040f7e9;
long DAT_0040f7ed;
long DAT_0040fd6b;
long DAT_004101f5;
long DAT_004102f9;
long DAT_004102ff;
long DAT_00410311;
long DAT_00410312;
long DAT_00410330;
long DAT_00410334;
long DAT_004103be;
long DAT_00612000;
long DAT_00612010;
long DAT_00612020;
long DAT_006123c8;
long DAT_00612430;
long DAT_00612434;
long DAT_00612438;
long DAT_0061243c;
long DAT_00612440;
long DAT_00612450;
long DAT_00612460;
long DAT_00612468;
long DAT_00612480;
long DAT_00612488;
long DAT_006124d0;
long DAT_006124d8;
long DAT_006124e0;
long DAT_00612678;
long DAT_00612680;
long DAT_00612688;
long DAT_00612698;
long _DYNAMIC;
long fde_00410d38;
long int7;
long null_ARRAY_0040f680;
long null_ARRAY_00410660;
long null_ARRAY_004108b0;
long null_ARRAY_00612420;
long null_ARRAY_006124a0;
long null_ARRAY_00612500;
long null_ARRAY_00612600;
long null_ARRAY_00612640;
long PTR_DAT_006123c0;
long PTR_null_ARRAY_00612418;
long register0x00000020;
void
FUN_00401bd0 (uint uParm1)
{
  int iVar1;
  long lVar2;
  undefined8 uVar3;
  undefined **ppuVar4;
  char *pcVar5;
  undefined *puVar6;
  undefined *local_88;
  char *local_80;
  undefined8 local_78;
  char *local_70;
  char *local_68;
  char *local_60;
  char *local_58;
  char *local_50;
  char *local_48;
  char *local_40;
  char *local_38;
  char *local_30;
  undefined8 local_28;
  undefined8 local_20;

  if (uParm1 == 0)
    goto LAB_00401bfd;
  func_0x00401590 (DAT_00612460, "Try \'%s --help\' for more information.\n",
		   DAT_006124e0);
  do
    {
      func_0x00401710 ((ulong) uParm1);
    LAB_00401bfd:
      ;
      func_0x00401420
	("Usage: %s CONTEXT COMMAND [args]\n  or:  %s [ -c ] [-u USER] [-r ROLE] [-t TYPE] [-l RANGE] COMMAND [args]\n",
	 DAT_006124e0, DAT_006124e0);
      uVar3 = DAT_00612440;
      func_0x004015a0
	("Run a program in a different SELinux security context.\nWith neither CONTEXT nor COMMAND, print the current security context.\n",
	 DAT_00612440);
      func_0x004015a0
	("\nMandatory arguments to long options are mandatory for short options too.\n",
	 uVar3);
      func_0x004015a0
	("  CONTEXT            Complete security context\n  -c, --compute      compute process transition context before modifying\n  -t, --type=TYPE    type (for same role as parent)\n  -u, --user=USER    user identity\n  -r, --role=ROLE    role\n  -l, --range=RANGE  levelrange\n\n",
	 uVar3);
      func_0x004015a0 ("      --help     display this help and exit\n",
		       uVar3);
      func_0x004015a0
	("      --version  output version information and exit\n", uVar3);
      local_88 = &DAT_0040f100;
      local_80 = "test invocation";
      puVar6 = &DAT_0040f100;
      local_78 = 0x40f16a;
      local_70 = "Multi-call invocation";
      local_68 = "sha224sum";
      local_60 = "sha2 utilities";
      local_58 = "sha256sum";
      local_50 = "sha2 utilities";
      local_48 = "sha384sum";
      local_40 = "sha2 utilities";
      local_38 = "sha512sum";
      local_30 = "sha2 utilities";
      local_28 = 0;
      local_20 = 0;
      ppuVar4 = &local_88;
      do
	{
	  iVar1 = func_0x00401670 ("runcon", puVar6);
	  if (iVar1 == 0)
	    break;
	  ppuVar4 = ppuVar4 + 2;
	  puVar6 = *ppuVar4;
	}
      while (puVar6 != (undefined *) 0x0);
      pcVar5 = ppuVar4[1];
      if (pcVar5 == (char *) 0x0)
	{
	  func_0x00401420 ("\n%s online help: <%s>\n", "GNU coreutils",
			   "https://www.gnu.org/software/coreutils/");
	  lVar2 = func_0x004016d0 (5, 0);
	  if (lVar2 != 0)
	    {
	      iVar1 = func_0x004015f0 (lVar2, &DAT_0040f18b, 3);
	      if (iVar1 != 0)
		{
		  pcVar5 = "runcon";
		  goto LAB_00401db1;
		}
	    }
	  func_0x00401420 ("Full documentation at: <%s%s>\n",
			   "https://www.gnu.org/software/coreutils/",
			   "runcon");
	LAB_00401dda:
	  ;
	  pcVar5 = "runcon";
	  uVar3 = 0x40f123;
	}
      else
	{
	  func_0x00401420 ("\n%s online help: <%s>\n", "GNU coreutils",
			   "https://www.gnu.org/software/coreutils/");
	  lVar2 = func_0x004016d0 (5, 0);
	  if (lVar2 != 0)
	    {
	      iVar1 = func_0x004015f0 (lVar2, &DAT_0040f18b, 3);
	      if (iVar1 != 0)
		{
		LAB_00401db1:
		  ;
		  func_0x00401420
		    ("Report %s translation bugs to <https://translationproject.org/team/>\n",
		     "runcon");
		}
	    }
	  func_0x00401420 ("Full documentation at: <%s%s>\n",
			   "https://www.gnu.org/software/coreutils/",
			   "runcon");
	  uVar3 = 0x41032f;
	  if (pcVar5 == "runcon")
	    goto LAB_00401dda;
	}
      func_0x00401420
	("or available locally via: info \'(coreutils) %s%s\'\n", pcVar5,
	 uVar3);
    }
  while (true);
}


#include "wc.h"

long null_ARRAY_006164a_0_8_;
long null_ARRAY_006164a_8_8_;
long null_ARRAY_0061670_0_8_;
long null_ARRAY_0061670_16_8_;
long null_ARRAY_0061670_24_8_;
long null_ARRAY_0061670_32_8_;
long null_ARRAY_0061670_40_8_;
long null_ARRAY_0061670_48_8_;
long null_ARRAY_0061670_8_8_;
long null_ARRAY_0061674_0_4_;
long null_ARRAY_0061674_16_8_;
long null_ARRAY_0061674_24_4_;
long null_ARRAY_0061674_32_8_;
long null_ARRAY_0061674_40_4_;
long null_ARRAY_0061674_4_4_;
long null_ARRAY_0061674_44_4_;
long null_ARRAY_0061674_48_4_;
long null_ARRAY_0061674_8_4_;
long local_2f_1_1_;
long local_5b_1_1_;
long local_9c_4_4_;
long local_a1_0_4_;
long local_a1_0_8_;
long local_a1_1_9_;
long local_a1_4_6_;
long local_a1_8_2_;
long local_a3_4_4_;
long local_a4_0_2_;
long local_a4_0_4_;
long local_a4_4_4_;
long local_a5_0_8_;
long local_a5_1_9_;
long local_a5_4_6_;
long local_a5_8_2_;
long local_a6_0_4_;
long _auVar23;
long DAT_00000008;
long DAT_00000009;
long DAT_00000010;
long DAT_00000fe0;
long DAT_00411f8f;
long DAT_00411f91;
long DAT_00412016;
long DAT_00412280;
long DAT_00412a58;
long DAT_00412a5c;
long DAT_00412a60;
long DAT_00412a63;
long DAT_00412a65;
long DAT_00412a69;
long DAT_00412a6d;
long DAT_00412feb;
long DAT_00413475;
long DAT_00413579;
long DAT_0041357f;
long DAT_00413591;
long DAT_00413592;
long DAT_004135b0;
long DAT_004135b4;
long DAT_0041364d;
long DAT_00414106;
long DAT_00616000;
long DAT_00616010;
long DAT_00616020;
long DAT_00616448;
long DAT_006164b0;
long DAT_006164b4;
long DAT_006164b8;
long DAT_006164bc;
long DAT_00616500;
long DAT_00616508;
long DAT_00616510;
long DAT_00616520;
long DAT_00616528;
long DAT_00616540;
long DAT_00616548;
long DAT_00616590;
long DAT_00616598;
long DAT_006165a0;
long DAT_006165a4;
long DAT_006165a8;
long DAT_006165a9;
long DAT_006165aa;
long DAT_006165ab;
long DAT_006165ac;
long DAT_006165b0;
long DAT_006165b8;
long DAT_006165c0;
long DAT_006165c8;
long DAT_006165d0;
long DAT_006165d8;
long DAT_006165e0;
long DAT_006165e8;
long DAT_00616778;
long DAT_00616780;
long DAT_00616788;
long DAT_00616798;
long _DYNAMIC;
long fde_00414b48;
long int7;
long null_ARRAY_004122c0;
long null_ARRAY_004129e0;
long null_ARRAY_00413680;
long null_ARRAY_00413780;
long null_ARRAY_004143a0;
long null_ARRAY_004145e0;
long null_ARRAY_006164a0;
long null_ARRAY_00616560;
long null_ARRAY_00616600;
long null_ARRAY_00616700;
long null_ARRAY_00616740;
long PTR_DAT_004121c0;
long PTR_DAT_00616440;
long PTR_FUN_006164c0;
long PTR_null_ARRAY_00616498;
long register0x00000020;
void
FUN_00403460 (uint uParm1)
{
  int iVar1;
  long lVar2;
  undefined8 uVar3;
  undefined **ppuVar4;
  undefined *puVar5;
  undefined *local_88;
  char *local_80;
  undefined8 local_78;
  char *local_70;
  char *local_68;
  char *local_60;
  char *local_58;
  char *local_50;
  char *local_48;
  char *local_40;
  char *local_38;
  char *local_30;
  undefined8 local_28;
  undefined8 local_20;

  if (uParm1 == 0)
    goto LAB_0040348d;
  func_0x00401a40 (DAT_00616520, "Try \'%s --help\' for more information.\n",
		   DAT_006165e8);
  do
    {
      func_0x00401c50 ((ulong) uParm1);
    LAB_0040348d:
      ;
      func_0x00401860
	("Usage: %s [OPTION]... [FILE]...\n  or:  %s [OPTION]... --files0-from=F\n",
	 DAT_006165e8, DAT_006165e8);
      uVar3 = DAT_00616500;
      func_0x00401a50
	("Print newline, word, and byte counts for each FILE, and a total line if\nmore than one FILE is specified.  A word is a non-zero-length sequence of\ncharacters delimited by white space.\n",
	 DAT_00616500);
      func_0x00401a50
	("\nWith no FILE, or when FILE is -, read standard input.\n", uVar3);
      func_0x00401a50
	("\nThe options below may be used to select which counts are printed, always in\nthe following order: newline, word, character, byte, maximum line length.\n  -c, --bytes            print the byte counts\n  -m, --chars            print the character counts\n  -l, --lines            print the newline counts\n",
	 uVar3);
      func_0x00401a50
	("      --files0-from=F    read input from the files specified by\n                           NUL-terminated names in file F;\n                           If F is - then read names from standard input\n  -L, --max-line-length  print the maximum display width\n  -w, --words            print the word counts\n",
	 uVar3);
      func_0x00401a50 ("      --help     display this help and exit\n",
		       uVar3);
      func_0x00401a50
	("      --version  output version information and exit\n", uVar3);
      local_88 = &DAT_00411f8f;
      local_80 = "test invocation";
      puVar5 = &DAT_00411f8f;
      local_78 = 0x411ff5;
      local_70 = "Multi-call invocation";
      local_68 = "sha224sum";
      local_60 = "sha2 utilities";
      local_58 = "sha256sum";
      local_50 = "sha2 utilities";
      local_48 = "sha384sum";
      local_40 = "sha2 utilities";
      local_38 = "sha512sum";
      local_30 = "sha2 utilities";
      local_28 = 0;
      local_20 = 0;
      ppuVar4 = &local_88;
      do
	{
	  iVar1 = func_0x00401b80 (&DAT_00411f91, puVar5);
	  if (iVar1 == 0)
	    break;
	  ppuVar4 = ppuVar4 + 2;
	  puVar5 = *ppuVar4;
	}
      while (puVar5 != (undefined *) 0x0);
      puVar5 = ppuVar4[1];
      if (puVar5 == (undefined *) 0x0)
	{
	  func_0x00401860 ("\n%s online help: <%s>\n", "GNU coreutils",
			   "https://www.gnu.org/software/coreutils/");
	  lVar2 = func_0x00401bf0 (5, 0);
	  if (lVar2 != 0)
	    {
	      iVar1 = func_0x00401ad0 (lVar2, &DAT_00412016, 3);
	      if (iVar1 != 0)
		{
		  puVar5 = &DAT_00411f91;
		  goto LAB_00403649;
		}
	    }
	  func_0x00401860 ("Full documentation at: <%s%s>\n",
			   "https://www.gnu.org/software/coreutils/",
			   &DAT_00411f91);
	LAB_00403672:
	  ;
	  puVar5 = &DAT_00411f91;
	  uVar3 = 0x411fae;
	}
      else
	{
	  func_0x00401860 ("\n%s online help: <%s>\n", "GNU coreutils",
			   "https://www.gnu.org/software/coreutils/");
	  lVar2 = func_0x00401bf0 (5, 0);
	  if (lVar2 != 0)
	    {
	      iVar1 = func_0x00401ad0 (lVar2, &DAT_00412016, 3);
	      if (iVar1 != 0)
		{
		LAB_00403649:
		  ;
		  func_0x00401860
		    ("Report %s translation bugs to <https://translationproject.org/team/>\n",
		     &DAT_00411f91);
		}
	    }
	  func_0x00401860 ("Full documentation at: <%s%s>\n",
			   "https://www.gnu.org/software/coreutils/",
			   &DAT_00411f91);
	  uVar3 = 0x4135af;
	  if (puVar5 == &DAT_00411f91)
	    goto LAB_00403672;
	}
      func_0x00401860
	("or available locally via: info \'(coreutils) %s%s\'\n", puVar5,
	 uVar3);
    }
  while (true);
}

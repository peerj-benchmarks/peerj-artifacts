typedef unsigned char   undefined;

typedef unsigned char    byte;
typedef unsigned char    dwfenc;
typedef unsigned int    dword;
typedef unsigned long    qword;
typedef unsigned char    undefined1;
typedef unsigned short    undefined2;
typedef unsigned int    undefined4;
typedef unsigned long    undefined6;
typedef unsigned long    undefined8;
typedef unsigned short    word;
#define unkbyte9   unsigned long long
#define unkbyte10   unsigned long long
#define unkbyte11   unsigned long long
#define unkbyte12   unsigned long long
#define unkbyte13   unsigned long long
#define unkbyte14   unsigned long long
#define unkbyte15   unsigned long long
#define unkbyte16   unsigned long long

#define unkuint9   unsigned long long
#define unkuint10   unsigned long long
#define unkuint11   unsigned long long
#define unkuint12   unsigned long long
#define unkuint13   unsigned long long
#define unkuint14   unsigned long long
#define unkuint15   unsigned long long
#define unkuint16   unsigned long long

#define unkint9   long long
#define unkint10   long long
#define unkint11   long long
#define unkint12   long long
#define unkint13   long long
#define unkint14   long long
#define unkint15   long long
#define unkint16   long long

#define unkfloat1   float
#define unkfloat2   float
#define unkfloat3   float
#define unkfloat5   double
#define unkfloat6   double
#define unkfloat7   double
#define unkfloat9   long double
#define unkfloat11   long double
#define unkfloat12   long double
#define unkfloat13   long double
#define unkfloat14   long double
#define unkfloat15   long double
#define unkfloat16   long double

#define BADSPACEBASE   void
#define code   void

#define ulong long
#define true 1
#define bool int
#define false 0
#define float10 int
#define ushort int
#define undefined7 long
#define undefined2 long
#define uint5 long
#define uint6 long

// Add definition for uint
typedef unsigned int    uint;

#define sbyte char

extern imperfection_wrapper(void);
typedef struct eh_frame_hdr eh_frame_hdr, *Peh_frame_hdr;

struct eh_frame_hdr {
    byte eh_frame_hdr_version; // Exception Handler Frame Header Version
    dwfenc eh_frame_pointer_encoding; // Exception Handler Frame Pointer Encoding
    dwfenc eh_frame_desc_entry_count_encoding; // Encoding of # of Exception Handler FDEs
    dwfenc eh_frame_table_encoding; // Exception Handler Table Encoding
};

typedef struct fde_table_entry fde_table_entry, *Pfde_table_entry;

struct fde_table_entry {
    dword initial_loc; // Initial Location
    dword data_loc; // Data location
};

typedef enum Elf64_DynTag {
    DT_AUDIT=1879047932,
    DT_AUXILIARY=2147483645,
    DT_BIND_NOW=24,
    DT_CHECKSUM=1879047672,
    DT_CONFIG=1879047930,
    DT_DEBUG=21,
    DT_DEPAUDIT=1879047931,
    DT_ENCODING=32,
    DT_FEATURE_1=1879047676,
    DT_FILTER=2147483647,
    DT_FINI=13,
    DT_FINI_ARRAY=26,
    DT_FINI_ARRAYSZ=28,
    DT_FLAGS=30,
    DT_FLAGS_1=1879048187,
    DT_GNU_CONFLICT=1879047928,
    DT_GNU_CONFLICTSZ=1879047670,
    DT_GNU_HASH=1879047925,
    DT_GNU_LIBLIST=1879047929,
    DT_GNU_LIBLISTSZ=1879047671,
    DT_GNU_PRELINKED=1879047669,
    DT_HASH=4,
    DT_INIT=12,
    DT_INIT_ARRAY=25,
    DT_INIT_ARRAYSZ=27,
    DT_JMPREL=23,
    DT_MOVEENT=1879047674,
    DT_MOVESZ=1879047675,
    DT_MOVETAB=1879047934,
    DT_NEEDED=1,
    DT_NULL=0,
    DT_PLTGOT=3,
    DT_PLTPAD=1879047933,
    DT_PLTPADSZ=1879047673,
    DT_PLTREL=20,
    DT_PLTRELSZ=2,
    DT_POSFLAG_1=1879047677,
    DT_PREINIT_ARRAYSZ=33,
    DT_REL=17,
    DT_RELA=7,
    DT_RELACOUNT=1879048185,
    DT_RELAENT=9,
    DT_RELASZ=8,
    DT_RELCOUNT=1879048186,
    DT_RELENT=19,
    DT_RELSZ=18,
    DT_RPATH=15,
    DT_RUNPATH=29,
    DT_SONAME=14,
    DT_STRSZ=10,
    DT_STRTAB=5,
    DT_SYMBOLIC=16,
    DT_SYMENT=11,
    DT_SYMINENT=1879047679,
    DT_SYMINFO=1879047935,
    DT_SYMINSZ=1879047678,
    DT_SYMTAB=6,
    DT_TEXTREL=22,
    DT_TLSDESC_GOT=1879047927,
    DT_TLSDESC_PLT=1879047926,
    DT_VERDEF=1879048188,
    DT_VERDEFNUM=1879048189,
    DT_VERNEED=1879048190,
    DT_VERNEEDNUM=1879048191,
    DT_VERSYM=1879048176
} Elf64_DynTag;

typedef enum Elf_ProgramHeaderType {
    PT_DYNAMIC=2,
    PT_GNU_EH_FRAME=1685382480,
    PT_GNU_RELRO=1685382482,
    PT_GNU_STACK=1685382481,
    PT_INTERP=3,
    PT_LOAD=1,
    PT_NOTE=4,
    PT_NULL=0,
    PT_PHDR=6,
    PT_SHLIB=5,
    PT_TLS=7
} Elf_ProgramHeaderType;

typedef struct Elf64_Dyn Elf64_Dyn, *PElf64_Dyn;

struct Elf64_Dyn {
    enum Elf64_DynTag d_tag;
    qword d_val;
};

typedef struct Elf64_Shdr Elf64_Shdr, *PElf64_Shdr;

typedef enum Elf_SectionHeaderType {
    SHT_CHECKSUM=1879048184,
    SHT_DYNAMIC=6,
    SHT_DYNSYM=11,
    SHT_FINI_ARRAY=15,
    SHT_GNU_ATTRIBUTES=1879048181,
    SHT_GNU_HASH=1879048182,
    SHT_GNU_LIBLIST=1879048183,
    SHT_GNU_verdef=1879048189,
    SHT_GNU_verneed=1879048190,
    SHT_GNU_versym=1879048191,
    SHT_GROUP=17,
    SHT_HASH=5,
    SHT_INIT_ARRAY=14,
    SHT_NOBITS=8,
    SHT_NOTE=7,
    SHT_NULL=0,
    SHT_PREINIT_ARRAY=16,
    SHT_PROGBITS=1,
    SHT_REL=9,
    SHT_RELA=4,
    SHT_SHLIB=10,
    SHT_STRTAB=3,
    SHT_SUNW_COMDAT=1879048187,
    SHT_SUNW_move=1879048186,
    SHT_SUNW_syminfo=1879048188,
    SHT_SYMTAB=2,
    SHT_SYMTAB_SHNDX=18
} Elf_SectionHeaderType;

struct Elf64_Shdr {
    dword sh_name;
    enum Elf_SectionHeaderType sh_type;
    qword sh_flags;
    qword sh_addr;
    qword sh_offset;
    qword sh_size;
    dword sh_link;
    dword sh_info;
    qword sh_addralign;
    qword sh_entsize;
};

typedef struct Elf64_Phdr Elf64_Phdr, *PElf64_Phdr;

struct Elf64_Phdr {
    enum Elf_ProgramHeaderType p_type;
    dword p_flags;
    qword p_offset;
    qword p_vaddr;
    qword p_paddr;
    qword p_filesz;
    qword p_memsz;
    qword p_align;
};

typedef struct Elf64_Rela Elf64_Rela, *PElf64_Rela;

struct Elf64_Rela {
    qword r_offset; // location to apply the relocation action
    qword r_info; // the symbol table index and the type of relocation
    qword r_addend; // a constant addend used to compute the relocatable field value
};

typedef struct Elf64_Sym Elf64_Sym, *PElf64_Sym;

struct Elf64_Sym {
    dword st_name;
    byte st_info;
    byte st_other;
    word st_shndx;
    qword st_value;
    qword st_size;
};

typedef struct Elf64_Ehdr Elf64_Ehdr, *PElf64_Ehdr;

struct Elf64_Ehdr {
    byte e_ident_magic_num;
    char e_ident_magic_str[3];
    byte e_ident_class;
    byte e_ident_data;
    byte e_ident_version;
    byte e_ident_pad[9];
    word e_type;
    word e_machine;
    dword e_version;
    qword e_entry;
    qword e_phoff;
    qword e_shoff;
    dword e_flags;
    word e_ehsize;
    word e_phentsize;
    word e_phnum;
    word e_shentsize;
    word e_shnum;
    word e_shstrndx;
};




undefined8 _DT_INIT(void);
void FUN_004018b0(void);
void thunk_FUN_0062c030(void);
void thunk_FUN_0062c0e8(void);
void FUN_00401e10(char cParm1);
ulong FUN_00401e30(uint uParm1,undefined8 *puParm2);
void entry(void);
void FUN_00402010(undefined8 *puParm1);
void FUN_00402040(void);
void FUN_004020c0(void);
void FUN_00402140(void);
long FUN_00402180(long *plParm1,long *plParm2,long *plParm3);
void FUN_004021c0(long *plParm1,long *plParm2,long *plParm3);
void FUN_004021f0(long *plParm1,long *plParm2,long *plParm3);
void FUN_00402220(long *plParm1,long *plParm2,long *plParm3);
void FUN_00402280(long *plParm1,long *plParm2,long *plParm3);
void FUN_004022b0(uint uParm1);
undefined4 * FUN_00402510(int *piParm1,int *piParm2);
int * FUN_00402a00(byte bParm1);
int * FUN_00402e60(byte bParm1);
int * FUN_00403060(byte bParm1);
int * FUN_00403260(byte bParm1);
undefined4 * FUN_00404110(ulong uParm1);
int * FUN_004042a0(byte bParm1);
int * FUN_004048d0(byte bParm1);
void FUN_00404b60(void);
char * FUN_00404bf0(long lParm1,long lParm2);
void FUN_00404c90(undefined4 uParm1,undefined4 uParm2,undefined4 uParm3,undefined4 uParm4,undefined4 uParm5,undefined4 uParm6,undefined4 uParm7,undefined4 uParm8,undefined8 param_1,undefined8 param_2,undefined8 param_3,undefined8 param_4,undefined8 param_5,code *param_6);
byte * FUN_00404da0(byte *pbParm1,ulong uParm2);
long FUN_00405000(byte *pbParm1);
void FUN_00405200(long lParm1);
undefined * FUN_004052a0(char *pcParm1,int iParm2);
void FUN_00405370(undefined *param_1,ulong param_2,long param_3,ulong param_4,uint param_5,ulong param_6,long param_7,char *param_8,char *param_9);
undefined1 * FUN_00406200(uint uParm1,uint uParm2,undefined8 uParm3);
undefined1 * FUN_00406400(undefined8 uParm1);
long FUN_004065e0(long lParm1,undefined8 uParm2);
ulong FUN_00406620(byte *pbParm1,byte *pbParm2);
long FUN_00406870(undefined8 uParm1,long lParm2,undefined8 uParm3,undefined8 uParm4,long *plParm5,undefined8 uParm6);
long FUN_00406ad0(undefined8 uParm1,long lParm2,undefined8 uParm3,undefined8 uParm4,long *plParm5);
long FUN_00406d20(undefined8 uParm1,long lParm2,undefined8 uParm3,undefined8 uParm4,uint *puParm5);
long FUN_00407240(undefined8 param_1,long param_2,undefined8 param_3,undefined8 param_4,long param_5,long param_6,long param_7,long param_8,long param_9,long param_10,long param_11,long param_12,long param_13,long param_14);
void FUN_004076f0(long lParm1);
long FUN_00407710(long lParm1,long lParm2);
void FUN_00407750(undefined8 uParm1);
void FUN_00407790(void);
ulong FUN_004077c0(byte *pbParm1,byte **ppbParm2,uint uParm3,long *plParm4,long lParm5);
undefined8 FUN_00407da0(ulong uParm1,ulong uParm2);
void FUN_00407df0(undefined8 uParm1);
void FUN_00407e30(undefined auParm1 [16],undefined auParm2 [16],undefined4 uParm3,undefined4 uParm4,undefined4 uParm5,undefined4 uParm6,undefined4 uParm7,undefined4 uParm8,uint param_1,ulong param_2,undefined8 param_3,undefined8 param_4,undefined8 param_5,undefined8 param_6);
void FUN_00407f80(long lParm1,int *piParm2);
ulong FUN_00408260(int param_1,undefined8 *param_2,char *param_3,long *param_4,int *param_5,int param_6,int *param_7,int param_8,undefined8 param_9);
ulong FUN_004089c0(uint param_1,undefined8 *param_2,char *param_3,long param_4,undefined8 param_5,ulong param_6,int param_7);
void FUN_004090c0(void);
void FUN_004090e0(long lParm1);
ulong FUN_00409100(uint *puParm1,byte *pbParm2,long lParm3);
ulong * FUN_00409170(ulong *puParm1,char cParm2,ulong uParm3);
void FUN_00409260(long lParm1,long lParm2);
undefined8 FUN_004092a0(char *pcParm1,long lParm2,ulong uParm3);
undefined8 FUN_004093d0(ulong *puParm1,long lParm2);
void FUN_00409600(long *plParm1);
undefined8 FUN_004098e0(long *plParm1);
long FUN_0040a3d0(long *plParm1,undefined8 uParm2,undefined8 uParm3);
undefined8 *FUN_0040a5c0(undefined8 *puParm1,int *piParm2,long *plParm3,long *plParm4,undefined *puParm5);
undefined8 FUN_0040a690(long lParm1,long *plParm2,long *plParm3,undefined8 uParm4);
undefined8 FUN_0040a7e0(long *plParm1,long lParm2);
undefined8 FUN_0040a9d0(long *plParm1,long *plParm2,long *plParm3,long *plParm4,long *plParm5);
ulong FUN_0040ab90(long lParm1,long lParm2,ulong uParm3);
undefined8 FUN_0040ac60(byte **ppbParm1,byte *pbParm2,uint uParm3);
long FUN_0040b790(long lParm1,long lParm2,long *plParm3,undefined8 uParm4);
undefined8 FUN_0040bad0(long param_1,ulong *param_2,long *param_3,long *param_4,long *param_5,char *param_6,ulong param_7);
ulong FUN_0040c2f0(long lParm1,ulong *puParm2,long lParm3,long lParm4,long lParm5);
undefined8 * FUN_0040c5f0(undefined8 *puParm1,int *piParm2,undefined8 *puParm3);
undefined8 FUN_0040c6a0(long *plParm1,long *plParm2,ulong uParm3);
undefined8 FUN_0040cd40(long *plParm1,ulong *puParm2,ulong uParm3);
long * FUN_0040ce80(long **pplParm1,long lParm2);
undefined8 FUN_0040cff0(long *plParm1,int iParm2);
long FUN_0040d220(long *plParm1,long lParm2,undefined8 uParm3);
undefined8 FUN_0040d4f0(long *plParm1,long lParm2,long lParm3,long lParm4,uint uParm5);
undefined8 FUN_0040d7e0(long *plParm1,long *plParm2,long lParm3,byte bParm4);
undefined8 FUN_0040da90(long *plParm1,long lParm2,long lParm3);
void FUN_0040dcd0(long *plParm1);
ulong FUN_0040dee0(long lParm1,uint uParm2,long lParm3,long lParm4,long lParm5);
undefined8 FUN_0040e990(long param_1,long *param_2,long *param_3,undefined8 param_4,long param_5,undefined8 param_6,long param_7);
undefined8 *FUN_0040ec80(long lParm1,undefined8 uParm2,undefined8 uParm3,char *pcParm4,byte bParm5,int *piParm6);
undefined8 FUN_0040f090(long *plParm1,ulong *puParm2,long lParm3,long lParm4,uint uParm5);
ulong FUN_0040f3c0(long *plParm1,long *plParm2,long lParm3,ulong uParm4);
ulong FUN_0040f500(long *plParm1,long lParm2,long lParm3,long lParm4);
ulong * FUN_0040f8d0(undefined4 *puParm1,long *plParm2,long lParm3);
ulong FUN_0040fcd0(long lParm1,long lParm2,long lParm3,undefined8 uParm4,uint uParm5);
ulong * FUN_00410180(undefined4 *puParm1,long *plParm2,long lParm3,ulong uParm4);
ulong FUN_00410770(long *plParm1,long lParm2);
ulong FUN_00411510(long lParm1,ulong *puParm2,ulong uParm3,long lParm4);
ulong FUN_00412040(long lParm1,long *plParm2);
ulong FUN_004125c0(long param_1,long *param_2,long param_3,long param_4,long param_5,long param_6,uint param_7);
undefined8 FUN_00413140(long lParm1,long *plParm2,undefined8 *puParm3,undefined8 uParm4,long lParm5);
ulong FUN_00413360(long *plParm1,long *plParm2,long *plParm3);
long FUN_00413fa0(int *piParm1,long lParm2,long lParm3);
undefined8 FUN_00414170(long **pplParm1,long lParm2,long lParm3,long *plParm4,char cParm5);
ulong FUN_00414db0(long **param_1,long param_2,ulong param_3,long param_4,long param_5,ulong param_6,long *param_7,undefined8 *param_8,uint param_9);
long ** FUN_00416a90(long *plParm1,long *plParm2,undefined8 *puParm3,ulong uParm4,long lParm5,int *piParm6);
long ** FUN_00418eb0(undefined8 uParm1,long *plParm2,long lParm3,undefined8 uParm4,long lParm5,int *piParm6);
long ** FUN_00419210(long lParm1,long *plParm2,long lParm3,ulong uParm4,long lParm5,int *piParm6);
ulong FUN_00419490(long **pplParm1,long lParm2,long *plParm3,long *plParm4);
char * FUN_0041b140(undefined8 uParm1,undefined8 uParm2,long lParm3);
void FUN_0041b1a0(long *plParm1);
long FUN_0041b1f0(long lParm1,undefined8 uParm2,long lParm3,long lParm4,ulong *puParm5);
ulong FUN_0041b5a0(undefined8 uParm1,undefined8 uParm2,undefined8 uParm3);
ulong FUN_0041b670(undefined8 uParm1);
undefined8 FUN_0041b6e0(void);
ulong FUN_0041b6f0(ulong uParm1);
char * FUN_0041b790(void);
void FUN_0041bae0(undefined8 uParm1);
ulong FUN_0041bb70(long lParm1);
undefined8 FUN_0041bc00(void);
void FUN_0041bc10(void);
undefined4 * FUN_0041bc20(ulong uParm1,uint *puParm2,ulong uParm3,uint *puParm4,long *plParm5);
ulong FUN_0041be80(void);
uint * FUN_0041c0f0(ulong uParm1,long lParm2,ulong uParm3,uint *puParm4,ulong *puParm5);
char * FUN_0041c6a0(int iParm1,ulong uParm2,undefined4 *puParm3,long lParm4,uint uParm5);
void FUN_0041d3f0(uint param_1);
ulong FUN_0041d5d0(void);
void FUN_0041d7f0(undefined8 uParm1,uint uParm2);
undefined8 *FUN_0041d960(undefined8 *puParm1,undefined8 *puParm2,undefined8 *puParm3,undefined8 uParm4);
double FUN_00424330(int *piParm1);
void FUN_00424370(uint *param_1);
undefined8 FUN_004243f0(uint *puParm1,ulong *puParm2);
undefined8 FUN_00424610(byte *pbParm1,ulong *puParm2,byte **ppbParm3);
ulong FUN_004252c0(void);
ulong FUN_004252f0(void);
void FUN_00425330(void);
undefined8 _DT_FINI(void);
undefined FUN_0062c000();
undefined FUN_0062c008();
undefined FUN_0062c010();
undefined FUN_0062c018();
undefined FUN_0062c020();
undefined FUN_0062c028();
undefined FUN_0062c030();
undefined FUN_0062c038();
undefined FUN_0062c040();
undefined FUN_0062c048();
undefined FUN_0062c050();
undefined FUN_0062c058();
undefined FUN_0062c060();
undefined FUN_0062c068();
undefined FUN_0062c070();
undefined FUN_0062c078();
undefined FUN_0062c080();
undefined FUN_0062c088();
undefined FUN_0062c090();
undefined FUN_0062c098();
undefined FUN_0062c0a0();
undefined FUN_0062c0a8();
undefined FUN_0062c0b0();
undefined FUN_0062c0b8();
undefined FUN_0062c0c0();
undefined FUN_0062c0c8();
undefined FUN_0062c0d0();
undefined FUN_0062c0d8();
undefined FUN_0062c0e0();
undefined FUN_0062c0e8();
undefined FUN_0062c0f0();
undefined FUN_0062c0f8();
undefined FUN_0062c100();
undefined FUN_0062c108();
undefined FUN_0062c110();
undefined FUN_0062c118();
undefined FUN_0062c120();
undefined FUN_0062c128();
undefined FUN_0062c130();
undefined FUN_0062c138();
undefined FUN_0062c140();
undefined FUN_0062c148();
undefined FUN_0062c150();
undefined FUN_0062c158();
undefined FUN_0062c160();
undefined FUN_0062c168();
undefined FUN_0062c170();
undefined FUN_0062c178();
undefined FUN_0062c180();
undefined FUN_0062c188();
undefined FUN_0062c190();
undefined FUN_0062c198();
undefined FUN_0062c1a0();
undefined FUN_0062c1a8();
undefined FUN_0062c1b0();
undefined FUN_0062c1b8();
undefined FUN_0062c1c0();
undefined FUN_0062c1c8();
undefined FUN_0062c1d0();
undefined FUN_0062c1d8();
undefined FUN_0062c1e0();
undefined FUN_0062c1e8();
undefined FUN_0062c1f0();
undefined FUN_0062c1f8();
undefined FUN_0062c200();
undefined FUN_0062c208();
undefined FUN_0062c210();
undefined FUN_0062c218();
undefined FUN_0062c220();
undefined FUN_0062c228();
undefined FUN_0062c230();
undefined FUN_0062c238();
undefined FUN_0062c240();
undefined FUN_0062c248();
undefined FUN_0062c250();
undefined FUN_0062c258();
undefined FUN_0062c260();
undefined FUN_0062c268();
undefined FUN_0062c270();
undefined FUN_0062c278();
undefined FUN_0062c280();
undefined FUN_0062c288();
undefined FUN_0062c290();
undefined FUN_0062c298();
undefined FUN_0062c2a0();



#include "expr.h"

long null_ARRAY_0062b4c_0_8_;
long null_ARRAY_0062b4c_8_8_;
long null_ARRAY_0062b6c_0_8_;
long null_ARRAY_0062b6c_16_8_;
long null_ARRAY_0062b6c_24_8_;
long null_ARRAY_0062b6c_32_8_;
long null_ARRAY_0062b6c_40_8_;
long null_ARRAY_0062b6c_48_8_;
long null_ARRAY_0062b6c_8_8_;
long null_ARRAY_0062b70_0_4_;
long null_ARRAY_0062b70_16_8_;
long null_ARRAY_0062b70_24_4_;
long null_ARRAY_0062b70_32_8_;
long null_ARRAY_0062b70_40_4_;
long null_ARRAY_0062b70_4_4_;
long null_ARRAY_0062b70_44_4_;
long null_ARRAY_0062b70_48_4_;
long null_ARRAY_0062b70_8_4_;
long local_19_0_4_;
long local_19_4_4_;
long local_2f_1_1_;
long local_5b_1_1_;
long local_9_4_4_;
long local_9c_4_4_;
long local_a_0_4_;
long local_a1_0_4_;
long local_a1_0_8_;
long local_a1_1_9_;
long local_a1_4_6_;
long local_a1_8_2_;
long local_a3_4_4_;
long local_a4_0_2_;
long local_a4_0_4_;
long local_a4_4_4_;
long local_a5_0_8_;
long local_a5_1_9_;
long local_a5_4_6_;
long local_a5_8_2_;
long local_a6_0_4_;
long local_b_0_4_;
long local_d_0_1_;
long local_d_1_7_;
long DAT_00000008;
long DAT_00000010;
long DAT_00425380;
long DAT_00425383;
long DAT_00425385;
long DAT_0042540c;
long DAT_0042540e;
long DAT_00425460;
long DAT_00425462;
long DAT_00425465;
long DAT_00425468;
long DAT_00425469;
long DAT_0042546b;
long DAT_0042546e;
long DAT_00425489;
long DAT_0042548b;
long DAT_004254a0;
long DAT_004254bf;
long DAT_004254c8;
long DAT_004254ca;
long DAT_004254f2;
long DAT_0042552e;
long DAT_0042552f;
long DAT_004260b0;
long DAT_004260b6;
long DAT_004260b8;
long DAT_004260bc;
long DAT_004260c0;
long DAT_004260c3;
long DAT_004260c5;
long DAT_004260c9;
long DAT_004260cd;
long DAT_0042666b;
long DAT_0042666d;
long DAT_00426ce8;
long DAT_00426cf7;
long DAT_00426e25;
long DAT_00426e65;
long DAT_00426e95;
long DAT_00427b00;
long DAT_00427b86;
long DAT_0062b000;
long DAT_0062b010;
long DAT_0062b020;
long DAT_0062b460;
long DAT_0062b4d0;
long DAT_0062b4d4;
long DAT_0062b4d8;
long DAT_0062b4dc;
long DAT_0062b500;
long DAT_0062b510;
long DAT_0062b520;
long DAT_0062b528;
long DAT_0062b540;
long DAT_0062b548;
long DAT_0062b590;
long DAT_0062b598;
long DAT_0062b5a0;
long DAT_0062b5a8;
long DAT_0062b738;
long DAT_0062b740;
long DAT_0062b748;
long DAT_0062b758;
long DAT_0062b760;
long fde_00428748;
long int7;
long null_ARRAY_00425fc0;
long null_ARRAY_00426020;
long null_ARRAY_00427840;
long null_ARRAY_00427880;
long null_ARRAY_00427e20;
long null_ARRAY_00428070;
long null_ARRAY_0062b4c0;
long null_ARRAY_0062b560;
long null_ARRAY_0062b5c0;
long null_ARRAY_0062b6c0;
long null_ARRAY_0062b700;
long PTR_null_ARRAY_0062b4b8;
long register0x00000020;
void
FUN_004022b0 (uint uParm1)
{
  int iVar1;
  long lVar2;
  undefined8 uVar3;
  undefined **ppuVar4;
  undefined *puVar5;
  undefined *local_88;
  char *local_80;
  undefined8 local_78;
  char *local_70;
  char *local_68;
  char *local_60;
  char *local_58;
  char *local_50;
  char *local_48;
  char *local_40;
  char *local_38;
  char *local_30;
  undefined8 local_28;
  undefined8 local_20;

  if (uParm1 == 0)
    goto LAB_004022dd;
  func_0x00401ad0 (DAT_0062b520, "Try \'%s --help\' for more information.\n",
		   DAT_0062b5a8);
  do
    {
      func_0x00401cd0 ((ulong) uParm1);
    LAB_004022dd:
      ;
      func_0x004018d0 ("Usage: %s EXPRESSION\n  or:  %s OPTION\n",
		       DAT_0062b5a8, DAT_0062b5a8);
      func_0x00401c90 (10);
      uVar3 = DAT_0062b500;
      func_0x00401ae0 ("      --help     display this help and exit\n",
		       DAT_0062b500);
      func_0x00401ae0
	("      --version  output version information and exit\n", uVar3);
      func_0x00401ae0
	("\nPrint the value of EXPRESSION to standard output.  A blank line below\nseparates increasing precedence groups.  EXPRESSION may be:\n\n  ARG1 | ARG2       ARG1 if it is neither null nor 0, otherwise ARG2\n\n  ARG1 & ARG2       ARG1 if neither argument is null or 0, otherwise 0\n",
	 uVar3);
      func_0x00401ae0
	("\n  ARG1 < ARG2       ARG1 is less than ARG2\n  ARG1 <= ARG2      ARG1 is less than or equal to ARG2\n  ARG1 = ARG2       ARG1 is equal to ARG2\n  ARG1 != ARG2      ARG1 is unequal to ARG2\n  ARG1 >= ARG2      ARG1 is greater than or equal to ARG2\n  ARG1 > ARG2       ARG1 is greater than ARG2\n",
	 uVar3);
      func_0x00401ae0
	("\n  ARG1 + ARG2       arithmetic sum of ARG1 and ARG2\n  ARG1 - ARG2       arithmetic difference of ARG1 and ARG2\n",
	 uVar3);
      func_0x00401ae0
	("\n  ARG1 * ARG2       arithmetic product of ARG1 and ARG2\n  ARG1 / ARG2       arithmetic quotient of ARG1 divided by ARG2\n  ARG1 % ARG2       arithmetic remainder of ARG1 divided by ARG2\n",
	 uVar3);
      func_0x00401ae0
	("\n  STRING : REGEXP   anchored pattern match of REGEXP in STRING\n\n  match STRING REGEXP        same as STRING : REGEXP\n  substr STRING POS LENGTH   substring of STRING, POS counted from 1\n  index STRING CHARS         index in STRING where any CHARS is found, or 0\n  length STRING              length of STRING\n",
	 uVar3);
      func_0x00401ae0
	("  + TOKEN                    interpret TOKEN as a string, even if it is a\n                               keyword like \'match\' or an operator like \'/\'\n\n  ( EXPRESSION )             value of EXPRESSION\n",
	 uVar3);
      func_0x00401ae0
	("\nBeware that many operators need to be escaped or quoted for shells.\nComparisons are arithmetic if both ARGs are numbers, else lexicographical.\nPattern matches return the string matched between \\( and \\) or null; if\n\\( and \\) are not used, they return the number of characters matched or 0.\n",
	 uVar3);
      func_0x00401ae0
	("\nExit status is 0 if EXPRESSION is neither null nor 0, 1 if EXPRESSION is null\nor 0, 2 if EXPRESSION is syntactically invalid, and 3 if an error occurred.\n",
	 uVar3);
      local_88 = &DAT_00425383;
      local_80 = "test invocation";
      puVar5 = &DAT_00425383;
      local_78 = 0x4253eb;
      local_70 = "Multi-call invocation";
      local_68 = "sha224sum";
      local_60 = "sha2 utilities";
      local_58 = "sha256sum";
      local_50 = "sha2 utilities";
      local_48 = "sha384sum";
      local_40 = "sha2 utilities";
      local_38 = "sha512sum";
      local_30 = "sha2 utilities";
      local_28 = 0;
      local_20 = 0;
      ppuVar4 = &local_88;
      do
	{
	  iVar1 = func_0x00401c10 (&DAT_00425385, puVar5);
	  if (iVar1 == 0)
	    break;
	  ppuVar4 = ppuVar4 + 2;
	  puVar5 = *ppuVar4;
	}
      while (puVar5 != (undefined *) 0x0);
      puVar5 = ppuVar4[1];
      if (puVar5 == (undefined *) 0x0)
	{
	  func_0x004018d0 ("\n%s online help: <%s>\n", "GNU coreutils",
			   "https://www.gnu.org/software/coreutils/");
	  lVar2 = func_0x00401c80 (5, 0);
	  if (lVar2 != 0)
	    {
	      iVar1 = func_0x00401b50 (lVar2, &DAT_0042540c, 3);
	      if (iVar1 != 0)
		{
		  puVar5 = &DAT_00425385;
		  goto LAB_004024d9;
		}
	    }
	  func_0x004018d0 ("Full documentation at: <%s%s>\n",
			   "https://www.gnu.org/software/coreutils/",
			   &DAT_00425385);
	LAB_00402502:
	  ;
	  puVar5 = &DAT_00425385;
	  uVar3 = 0x4253a4;
	}
      else
	{
	  func_0x004018d0 ("\n%s online help: <%s>\n", "GNU coreutils",
			   "https://www.gnu.org/software/coreutils/");
	  lVar2 = func_0x00401c80 (5, 0);
	  if (lVar2 != 0)
	    {
	      iVar1 = func_0x00401b50 (lVar2, &DAT_0042540c, 3);
	      if (iVar1 != 0)
		{
		LAB_004024d9:
		  ;
		  func_0x004018d0
		    ("Report %s translation bugs to <https://translationproject.org/team/>\n",
		     &DAT_00425385);
		}
	    }
	  func_0x004018d0 ("Full documentation at: <%s%s>\n",
			   "https://www.gnu.org/software/coreutils/",
			   &DAT_00425385);
	  uVar3 = 0x426e24;
	  if (puVar5 == &DAT_00425385)
	    goto LAB_00402502;
	}
      func_0x004018d0
	("or available locally via: info \'(coreutils) %s%s\'\n", puVar5,
	 uVar3);
    }
  while (true);
}

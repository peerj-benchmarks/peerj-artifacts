
#include "pathchk.h"

long null_ARRAY_0061340_0_4_;
long null_ARRAY_0061340_40_8_;
long null_ARRAY_0061340_4_4_;
long null_ARRAY_0061340_48_8_;
long null_ARRAY_0061344_0_8_;
long null_ARRAY_0061344_8_8_;
long null_ARRAY_0061364_0_8_;
long null_ARRAY_0061364_16_8_;
long null_ARRAY_0061364_24_8_;
long null_ARRAY_0061364_32_8_;
long null_ARRAY_0061364_40_8_;
long null_ARRAY_0061364_48_8_;
long null_ARRAY_0061364_8_8_;
long null_ARRAY_0061368_0_4_;
long null_ARRAY_0061368_16_8_;
long null_ARRAY_0061368_24_4_;
long null_ARRAY_0061368_32_8_;
long null_ARRAY_0061368_40_4_;
long null_ARRAY_0061368_4_4_;
long null_ARRAY_0061368_44_4_;
long null_ARRAY_0061368_48_4_;
long null_ARRAY_0061368_8_4_;
long local_2f_1_1_;
long local_5b_1_1_;
long local_9c_4_4_;
long local_a1_0_4_;
long local_a1_0_8_;
long local_a1_1_9_;
long local_a1_4_6_;
long local_a1_8_2_;
long local_a3_4_4_;
long local_a4_0_2_;
long local_a4_0_4_;
long local_a4_4_4_;
long local_a5_0_8_;
long local_a5_1_9_;
long local_a5_4_6_;
long local_a5_8_2_;
long local_a6_0_4_;
long DAT_00000008;
long DAT_00000010;
long DAT_004100c0;
long DAT_0041014c;
long DAT_00410150;
long DAT_0041017b;
long DAT_00410650;
long DAT_00410656;
long DAT_00410658;
long DAT_0041065c;
long DAT_00410660;
long DAT_00410663;
long DAT_00410665;
long DAT_00410669;
long DAT_0041066d;
long DAT_00410beb;
long DAT_00411075;
long DAT_00411179;
long DAT_0041117f;
long DAT_00411191;
long DAT_00411192;
long DAT_004111b0;
long DAT_004111b4;
long DAT_0041123e;
long DAT_00613000;
long DAT_00613010;
long DAT_00613020;
long DAT_006133e8;
long DAT_00613450;
long DAT_00613454;
long DAT_00613458;
long DAT_0061345c;
long DAT_00613480;
long DAT_00613490;
long DAT_006134a0;
long DAT_006134a8;
long DAT_006134c0;
long DAT_006134c8;
long DAT_00613510;
long DAT_00613518;
long DAT_00613520;
long DAT_006136b8;
long DAT_006136c0;
long DAT_006136c8;
long DAT_006136d0;
long DAT_006136e0;
long fde_00411bf8;
long int7;
long null_ARRAY_00410580;
long null_ARRAY_004114e0;
long null_ARRAY_00411730;
long null_ARRAY_00613400;
long null_ARRAY_00613440;
long null_ARRAY_006134e0;
long null_ARRAY_00613540;
long null_ARRAY_00613640;
long null_ARRAY_00613680;
long PTR_DAT_006133e0;
long PTR_null_ARRAY_00613438;
long register0x00000020;
void
FUN_004020e0 (uint uParm1)
{
  int iVar1;
  long lVar2;
  undefined8 uVar3;
  undefined **ppuVar4;
  char *pcVar5;
  undefined *puVar6;
  undefined *local_88;
  char *local_80;
  undefined8 local_78;
  char *local_70;
  char *local_68;
  char *local_60;
  char *local_58;
  char *local_50;
  char *local_48;
  char *local_40;
  char *local_38;
  char *local_30;
  undefined8 local_28;
  undefined8 local_20;

  if (uParm1 == 0)
    goto LAB_0040210d;
  func_0x00401680 (DAT_006134a0, "Try \'%s --help\' for more information.\n",
		   DAT_00613520);
  do
    {
      func_0x00401820 ((ulong) uParm1);
    LAB_0040210d:
      ;
      func_0x00401500 ("Usage: %s [OPTION]... NAME...\n", DAT_00613520);
      uVar3 = DAT_00613480;
      func_0x00401690
	("Diagnose invalid or unportable file names.\n\n  -p                  check for most POSIX systems\n  -P                  check for empty names and leading \"-\"\n      --portability   check for all POSIX systems (equivalent to -p -P)\n",
	 DAT_00613480);
      func_0x00401690 ("      --help     display this help and exit\n",
		       uVar3);
      func_0x00401690
	("      --version  output version information and exit\n", uVar3);
      local_88 = &DAT_004100c0;
      local_80 = "test invocation";
      puVar6 = &DAT_004100c0;
      local_78 = 0x41012b;
      local_70 = "Multi-call invocation";
      local_68 = "sha224sum";
      local_60 = "sha2 utilities";
      local_58 = "sha256sum";
      local_50 = "sha2 utilities";
      local_48 = "sha384sum";
      local_40 = "sha2 utilities";
      local_38 = "sha512sum";
      local_30 = "sha2 utilities";
      local_28 = 0;
      local_20 = 0;
      ppuVar4 = &local_88;
      do
	{
	  iVar1 = func_0x00401770 ("pathchk", puVar6);
	  if (iVar1 == 0)
	    break;
	  ppuVar4 = ppuVar4 + 2;
	  puVar6 = *ppuVar4;
	}
      while (puVar6 != (undefined *) 0x0);
      pcVar5 = ppuVar4[1];
      if (pcVar5 == (char *) 0x0)
	{
	  func_0x00401500 ("\n%s online help: <%s>\n", "GNU coreutils",
			   "https://www.gnu.org/software/coreutils/");
	  lVar2 = func_0x004017d0 (5, 0);
	  if (lVar2 != 0)
	    {
	      iVar1 = func_0x004016f0 (lVar2, &DAT_0041014c, 3);
	      if (iVar1 != 0)
		{
		  pcVar5 = "pathchk";
		  goto LAB_004022a1;
		}
	    }
	  func_0x00401500 ("Full documentation at: <%s%s>\n",
			   "https://www.gnu.org/software/coreutils/",
			   "pathchk");
	LAB_004022ca:
	  ;
	  pcVar5 = "pathchk";
	  uVar3 = 0x4100e4;
	}
      else
	{
	  func_0x00401500 ("\n%s online help: <%s>\n", "GNU coreutils",
			   "https://www.gnu.org/software/coreutils/");
	  lVar2 = func_0x004017d0 (5, 0);
	  if (lVar2 != 0)
	    {
	      iVar1 = func_0x004016f0 (lVar2, &DAT_0041014c, 3);
	      if (iVar1 != 0)
		{
		LAB_004022a1:
		  ;
		  func_0x00401500
		    ("Report %s translation bugs to <https://translationproject.org/team/>\n",
		     "pathchk");
		}
	    }
	  func_0x00401500 ("Full documentation at: <%s%s>\n",
			   "https://www.gnu.org/software/coreutils/",
			   "pathchk");
	  uVar3 = 0x4111af;
	  if (pcVar5 == "pathchk")
	    goto LAB_004022ca;
	}
      func_0x00401500
	("or available locally via: info \'(coreutils) %s%s\'\n", pcVar5,
	 uVar3);
    }
  while (true);
}


#include "tsort.h"

long null_ARRAY_0061462_0_4_;
long null_ARRAY_0061462_40_8_;
long null_ARRAY_0061462_4_4_;
long null_ARRAY_0061462_48_8_;
long null_ARRAY_0061466_0_8_;
long null_ARRAY_0061466_8_8_;
long null_ARRAY_0061488_0_8_;
long null_ARRAY_0061488_16_8_;
long null_ARRAY_0061488_24_8_;
long null_ARRAY_0061488_32_8_;
long null_ARRAY_0061488_40_8_;
long null_ARRAY_0061488_48_8_;
long null_ARRAY_0061488_8_8_;
long null_ARRAY_006148c_0_4_;
long null_ARRAY_006148c_16_8_;
long null_ARRAY_006148c_24_4_;
long null_ARRAY_006148c_32_8_;
long null_ARRAY_006148c_40_4_;
long null_ARRAY_006148c_4_4_;
long null_ARRAY_006148c_44_4_;
long null_ARRAY_006148c_48_4_;
long null_ARRAY_006148c_8_4_;
long local_2f_1_1_;
long local_5b_1_1_;
long local_9c_4_4_;
long local_a1_0_4_;
long local_a1_0_8_;
long local_a1_1_9_;
long local_a1_4_6_;
long local_a1_8_2_;
long local_a3_4_4_;
long local_a4_0_2_;
long local_a4_0_4_;
long local_a4_4_4_;
long local_a5_0_8_;
long local_a5_1_9_;
long local_a5_4_6_;
long local_a5_8_2_;
long local_a6_0_4_;
long DAT_00000008;
long DAT_00000010;
long DAT_004116c0;
long DAT_0041174a;
long DAT_0041177c;
long DAT_0041177e;
long DAT_0041177f;
long DAT_00411a82;
long DAT_00411b60;
long DAT_00411b64;
long DAT_00411b68;
long DAT_00411b6b;
long DAT_00411b6d;
long DAT_00411b71;
long DAT_00411b75;
long DAT_0041212b;
long DAT_004125b5;
long DAT_004126b9;
long DAT_004126bf;
long DAT_004126d1;
long DAT_004126d2;
long DAT_004126f0;
long DAT_004126f4;
long DAT_0041277e;
long DAT_00614228;
long DAT_00614238;
long DAT_00614248;
long DAT_00614608;
long DAT_00614670;
long DAT_00614674;
long DAT_00614678;
long DAT_0061467c;
long DAT_00614680;
long DAT_00614688;
long DAT_00614690;
long DAT_006146a0;
long DAT_006146a8;
long DAT_006146c0;
long DAT_006146c8;
long DAT_00614710;
long DAT_00614718;
long DAT_00614720;
long DAT_00614728;
long DAT_00614730;
long DAT_00614738;
long DAT_00614740;
long DAT_006148f8;
long DAT_00614900;
long DAT_00614908;
long DAT_00614918;
long _DYNAMIC;
long fde_00413168;
long int7;
long null_ARRAY_00411a40;
long null_ARRAY_00411ac0;
long null_ARRAY_00412a20;
long null_ARRAY_00412c70;
long null_ARRAY_00614620;
long null_ARRAY_00614660;
long null_ARRAY_006146e0;
long null_ARRAY_00614780;
long null_ARRAY_00614880;
long null_ARRAY_006148c0;
long PTR_DAT_00614600;
long PTR_null_ARRAY_00614658;
long register0x00000020;
void
FUN_00402440 (uint uParm1)
{
  undefined8 uVar1;
  int iVar2;
  long lVar3;
  undefined *puVar4;
  undefined **ppuVar5;
  char *pcVar6;
  undefined *local_88;
  char *local_80;
  undefined8 local_78;
  char *local_70;
  char *local_68;
  char *local_60;
  char *local_58;
  char *local_50;
  char *local_48;
  char *local_40;
  char *local_38;
  char *local_30;
  undefined8 local_28;
  undefined8 local_20;

  if (uParm1 == 0)
    goto LAB_0040246d;
  func_0x00401710 (DAT_006146a0, "Try \'%s --help\' for more information.\n",
		   DAT_00614740);
  do
    {
      func_0x004018c0 ((ulong) uParm1);
    LAB_0040246d:
      ;
      func_0x00401580
	("Usage: %s [OPTION] [FILE]\nWrite totally ordered list consistent with the partial ordering in FILE.\n",
	 DAT_00614740);
      uVar1 = DAT_00614680;
      func_0x00401720
	("\nWith no FILE, or when FILE is -, read standard input.\n",
	 DAT_00614680);
      func_0x00401720 (&DAT_0041177e, uVar1);
      func_0x00401720 ("      --help     display this help and exit\n",
		       uVar1);
      func_0x00401720
	("      --version  output version information and exit\n", uVar1);
      local_88 = &DAT_004116c0;
      local_80 = "test invocation";
      puVar4 = &DAT_004116c0;
      local_78 = 0x411729;
      local_70 = "Multi-call invocation";
      local_68 = "sha224sum";
      local_60 = "sha2 utilities";
      local_58 = "sha256sum";
      local_50 = "sha2 utilities";
      local_48 = "sha384sum";
      local_40 = "sha2 utilities";
      local_38 = "sha512sum";
      local_30 = "sha2 utilities";
      local_28 = 0;
      local_20 = 0;
      ppuVar5 = &local_88;
      do
	{
	  iVar2 = func_0x00401810 ("tsort", puVar4);
	  if (iVar2 == 0)
	    break;
	  ppuVar5 = ppuVar5 + 2;
	  puVar4 = *ppuVar5;
	}
      while (puVar4 != (undefined *) 0x0);
      pcVar6 = ppuVar5[1];
      if (pcVar6 == (char *) 0x0)
	{
	  func_0x00401580 ("\n%s online help: <%s>\n", "GNU coreutils",
			   "https://www.gnu.org/software/coreutils/");
	  lVar3 = func_0x00401870 (5, 0);
	  if (lVar3 != 0)
	    {
	      iVar2 = func_0x00401780 (lVar3, &DAT_0041174a, 3);
	      if (iVar2 != 0)
		{
		  pcVar6 = "tsort";
		  goto LAB_00402611;
		}
	    }
	  func_0x00401580 ("Full documentation at: <%s%s>\n",
			   "https://www.gnu.org/software/coreutils/",
			   "tsort");
	LAB_0040263a:
	  ;
	  pcVar6 = "tsort";
	  puVar4 = (undefined *) 0x4116e2;
	}
      else
	{
	  func_0x00401580 ("\n%s online help: <%s>\n", "GNU coreutils",
			   "https://www.gnu.org/software/coreutils/");
	  lVar3 = func_0x00401870 (5, 0);
	  if (lVar3 != 0)
	    {
	      iVar2 = func_0x00401780 (lVar3, &DAT_0041174a, 3);
	      if (iVar2 != 0)
		{
		LAB_00402611:
		  ;
		  func_0x00401580
		    ("Report %s translation bugs to <https://translationproject.org/team/>\n",
		     "tsort");
		}
	    }
	  func_0x00401580 ("Full documentation at: <%s%s>\n",
			   "https://www.gnu.org/software/coreutils/",
			   "tsort");
	  puVar4 = &DAT_0041177f;
	  if (pcVar6 == "tsort")
	    goto LAB_0040263a;
	}
      func_0x00401580
	("or available locally via: info \'(coreutils) %s%s\'\n", pcVar6,
	 puVar4);
    }
  while (true);
}

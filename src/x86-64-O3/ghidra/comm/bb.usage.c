
#include "comm.h"

long DAT_00613b1_0_1_;
long DAT_00613b1_1_1_;
long null_ARRAY_00613a0_0_4_;
long null_ARRAY_00613a0_40_8_;
long null_ARRAY_00613a0_4_4_;
long null_ARRAY_00613a0_48_8_;
long null_ARRAY_00613a4_0_8_;
long null_ARRAY_00613a4_8_8_;
long null_ARRAY_00613c4_0_8_;
long null_ARRAY_00613c4_16_8_;
long null_ARRAY_00613c4_24_8_;
long null_ARRAY_00613c4_32_8_;
long null_ARRAY_00613c4_40_8_;
long null_ARRAY_00613c4_48_8_;
long null_ARRAY_00613c4_8_8_;
long null_ARRAY_00613c8_0_4_;
long null_ARRAY_00613c8_16_8_;
long null_ARRAY_00613c8_24_4_;
long null_ARRAY_00613c8_32_8_;
long null_ARRAY_00613c8_40_4_;
long null_ARRAY_00613c8_4_4_;
long null_ARRAY_00613c8_44_4_;
long null_ARRAY_00613c8_48_4_;
long null_ARRAY_00613c8_8_4_;
long local_2f_1_1_;
long local_5b_1_1_;
long local_9c_4_4_;
long local_a1_0_4_;
long local_a1_0_8_;
long local_a1_1_9_;
long local_a1_4_6_;
long local_a1_8_2_;
long local_a3_4_4_;
long local_a4_0_2_;
long local_a4_0_4_;
long local_a4_4_4_;
long local_a5_0_8_;
long local_a5_1_9_;
long local_a5_4_6_;
long local_a5_8_2_;
long local_a6_0_4_;
long DAT_00000008;
long DAT_00000010;
long DAT_00410657;
long DAT_00410659;
long DAT_004106e0;
long DAT_00410708;
long DAT_00410784;
long DAT_00410ed8;
long DAT_00410f20;
long DAT_00410f24;
long DAT_00410f28;
long DAT_00410f2b;
long DAT_00410f2d;
long DAT_00410f31;
long DAT_00410f35;
long DAT_004114eb;
long DAT_004119ea;
long DAT_00411af1;
long DAT_00411af7;
long DAT_00411b09;
long DAT_00411b0a;
long DAT_00411b28;
long DAT_00411bae;
long DAT_00613608;
long DAT_00613618;
long DAT_00613628;
long DAT_006139e8;
long DAT_006139f8;
long DAT_00613a50;
long DAT_00613a54;
long DAT_00613a58;
long DAT_00613a5c;
long DAT_00613a80;
long DAT_00613a88;
long DAT_00613a90;
long DAT_00613aa0;
long DAT_00613aa8;
long DAT_00613ac0;
long DAT_00613ac8;
long DAT_00613b10;
long DAT_00613b18;
long DAT_00613b1c;
long DAT_00613b1d;
long DAT_00613b1f;
long DAT_00613b20;
long DAT_00613b21;
long DAT_00613b22;
long DAT_00613b23;
long DAT_00613b28;
long DAT_00613b30;
long DAT_00613b38;
long DAT_00613c78;
long DAT_00613cb8;
long DAT_00613cc0;
long DAT_00613cc8;
long DAT_00613cd8;
long fde_00412590;
long int7;
long null_ARRAY_00410dc0;
long null_ARRAY_00411e40;
long null_ARRAY_00412090;
long null_ARRAY_00613a00;
long null_ARRAY_00613a40;
long null_ARRAY_00613ae0;
long null_ARRAY_00613b40;
long null_ARRAY_00613c40;
long null_ARRAY_00613c80;
long PTR_DAT_006139e0;
long PTR_DAT_006139f0;
long PTR_null_ARRAY_00613a38;
long register0x00000020;
void
FUN_004024c0 (uint uParm1)
{
  undefined8 uVar1;
  int iVar2;
  long lVar3;
  undefined *puVar4;
  undefined **ppuVar5;
  undefined *puVar6;
  undefined *local_88;
  char *local_80;
  undefined8 local_78;
  char *local_70;
  char *local_68;
  char *local_60;
  char *local_58;
  char *local_50;
  char *local_48;
  char *local_40;
  char *local_38;
  char *local_30;
  undefined8 local_28;
  undefined8 local_20;

  if (uParm1 == 0)
    goto LAB_004024ed;
  func_0x00401700 (DAT_00613aa0, "Try \'%s --help\' for more information.\n",
		   DAT_00613b38);
  do
    {
      func_0x004018d0 ((ulong) uParm1);
    LAB_004024ed:
      ;
      func_0x00401590 ("Usage: %s [OPTION]... FILE1 FILE2\n", DAT_00613b38);
      uVar1 = DAT_00613a80;
      func_0x00401710 ("Compare sorted files FILE1 and FILE2 line by line.\n",
		       DAT_00613a80);
      func_0x00401710
	("\nWhen FILE1 or FILE2 (not both) is -, read standard input.\n",
	 uVar1);
      func_0x00401710
	("\nWith no options, produce three-column output.  Column one contains\nlines unique to FILE1, column two contains lines unique to FILE2,\nand column three contains lines common to both files.\n",
	 uVar1);
      func_0x00401710
	("\n  -1              suppress column 1 (lines unique to FILE1)\n  -2              suppress column 2 (lines unique to FILE2)\n  -3              suppress column 3 (lines that appear in both files)\n",
	 uVar1);
      func_0x00401710
	("\n  --check-order     check that the input is correctly sorted, even\n                      if all input lines are pairable\n  --nocheck-order   do not check that the input is correctly sorted\n",
	 uVar1);
      func_0x00401710
	("  --output-delimiter=STR  separate columns with STR\n", uVar1);
      func_0x00401710 ("  --total           output a summary\n", uVar1);
      func_0x00401710
	("  -z, --zero-terminated    line delimiter is NUL, not newline\n",
	 uVar1);
      func_0x00401710 ("      --help     display this help and exit\n",
		       uVar1);
      func_0x00401710
	("      --version  output version information and exit\n", uVar1);
      func_0x00401710
	("\nNote, comparisons honor the rules specified by \'LC_COLLATE\'.\n",
	 uVar1);
      func_0x00401590
	("\nExamples:\n  %s -12 file1 file2  Print only lines present in both file1 and file2.\n  %s -3 file1 file2  Print lines in file1 not in file2, and vice versa.\n",
	 DAT_00613b38, DAT_00613b38);
      local_88 = &DAT_00410657;
      local_80 = "test invocation";
      puVar6 = &DAT_00410657;
      local_78 = 0x4106bf;
      local_70 = "Multi-call invocation";
      local_68 = "sha224sum";
      local_60 = "sha2 utilities";
      local_58 = "sha256sum";
      local_50 = "sha2 utilities";
      local_48 = "sha384sum";
      local_40 = "sha2 utilities";
      local_38 = "sha512sum";
      local_30 = "sha2 utilities";
      local_28 = 0;
      local_20 = 0;
      ppuVar5 = &local_88;
      do
	{
	  iVar2 = func_0x00401810 (&DAT_00410659, puVar6);
	  if (iVar2 == 0)
	    break;
	  ppuVar5 = ppuVar5 + 2;
	  puVar6 = *ppuVar5;
	}
      while (puVar6 != (undefined *) 0x0);
      puVar6 = ppuVar5[1];
      if (puVar6 == (undefined *) 0x0)
	{
	  func_0x00401590 ("\n%s online help: <%s>\n", "GNU coreutils",
			   "https://www.gnu.org/software/coreutils/");
	  lVar3 = func_0x00401870 (5, 0);
	  if (lVar3 != 0)
	    {
	      iVar2 = func_0x00401780 (lVar3, &DAT_004106e0, 3);
	      if (iVar2 != 0)
		{
		  puVar6 = &DAT_00410659;
		  goto LAB_00402701;
		}
	    }
	  func_0x00401590 ("Full documentation at: <%s%s>\n",
			   "https://www.gnu.org/software/coreutils/",
			   &DAT_00410659);
	LAB_0040272a:
	  ;
	  puVar6 = &DAT_00410659;
	  puVar4 = (undefined *) 0x410678;
	}
      else
	{
	  func_0x00401590 ("\n%s online help: <%s>\n", "GNU coreutils",
			   "https://www.gnu.org/software/coreutils/");
	  lVar3 = func_0x00401870 (5, 0);
	  if (lVar3 != 0)
	    {
	      iVar2 = func_0x00401780 (lVar3, &DAT_004106e0, 3);
	      if (iVar2 != 0)
		{
		LAB_00402701:
		  ;
		  func_0x00401590
		    ("Report %s translation bugs to <https://translationproject.org/team/>\n",
		     &DAT_00410659);
		}
	    }
	  func_0x00401590 ("Full documentation at: <%s%s>\n",
			   "https://www.gnu.org/software/coreutils/",
			   &DAT_00410659);
	  puVar4 = &DAT_00410784;
	  if (puVar6 == &DAT_00410659)
	    goto LAB_0040272a;
	}
      func_0x00401590
	("or available locally via: info \'(coreutils) %s%s\'\n", puVar6,
	 puVar4);
    }
  while (true);
}

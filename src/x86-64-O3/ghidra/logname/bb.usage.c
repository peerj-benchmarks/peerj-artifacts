
#include "logname.h"

long null_ARRAY_00611ce_0_4_;
long null_ARRAY_00611ce_40_8_;
long null_ARRAY_00611ce_4_4_;
long null_ARRAY_00611ce_48_8_;
long null_ARRAY_00611d2_0_8_;
long null_ARRAY_00611d2_8_8_;
long null_ARRAY_00611f0_0_8_;
long null_ARRAY_00611f0_16_8_;
long null_ARRAY_00611f0_24_8_;
long null_ARRAY_00611f0_32_8_;
long null_ARRAY_00611f0_40_8_;
long null_ARRAY_00611f0_48_8_;
long null_ARRAY_00611f0_8_8_;
long null_ARRAY_00611f4_0_4_;
long null_ARRAY_00611f4_16_8_;
long null_ARRAY_00611f4_24_4_;
long null_ARRAY_00611f4_32_8_;
long null_ARRAY_00611f4_40_4_;
long null_ARRAY_00611f4_4_4_;
long null_ARRAY_00611f4_44_4_;
long null_ARRAY_00611f4_48_4_;
long null_ARRAY_00611f4_8_4_;
long local_2f_1_1_;
long local_5b_1_1_;
long local_9c_4_4_;
long local_a1_0_4_;
long local_a1_0_8_;
long local_a1_1_9_;
long local_a1_4_6_;
long local_a1_8_2_;
long local_a3_4_4_;
long local_a4_0_2_;
long local_a4_0_4_;
long local_a4_4_4_;
long local_a5_0_8_;
long local_a5_1_9_;
long local_a5_4_6_;
long local_a5_8_2_;
long local_a6_0_4_;
long DAT_00000008;
long DAT_00000010;
long DAT_0040f2c0;
long DAT_0040f360;
long DAT_0040f558;
long DAT_0040f620;
long DAT_0040f624;
long DAT_0040f628;
long DAT_0040f62b;
long DAT_0040f62d;
long DAT_0040f631;
long DAT_0040f635;
long DAT_0040fbeb;
long DAT_00410075;
long DAT_00410179;
long DAT_0041017f;
long DAT_00410191;
long DAT_00410192;
long DAT_004101b0;
long DAT_004101b4;
long DAT_0041023e;
long DAT_00611908;
long DAT_00611918;
long DAT_00611928;
long DAT_00611cc8;
long DAT_00611d30;
long DAT_00611d34;
long DAT_00611d38;
long DAT_00611d3c;
long DAT_00611d40;
long DAT_00611d50;
long DAT_00611d60;
long DAT_00611d68;
long DAT_00611d80;
long DAT_00611d88;
long DAT_00611dd0;
long DAT_00611dd8;
long DAT_00611de0;
long DAT_00611f78;
long DAT_00611f80;
long DAT_00611f88;
long DAT_00611f98;
long _DYNAMIC;
long fde_00410bc8;
long int7;
long null_ARRAY_0040f520;
long null_ARRAY_0040f580;
long null_ARRAY_004104e0;
long null_ARRAY_00410730;
long null_ARRAY_00611ce0;
long null_ARRAY_00611d20;
long null_ARRAY_00611da0;
long null_ARRAY_00611e00;
long null_ARRAY_00611f00;
long null_ARRAY_00611f40;
long PTR_DAT_00611cc0;
long PTR_null_ARRAY_00611d18;
long register0x00000020;
void
FUN_00401b10 (uint uParm1)
{
  int iVar1;
  long lVar2;
  undefined8 uVar3;
  undefined **ppuVar4;
  char *pcVar5;
  undefined *puVar6;
  undefined *local_88;
  char *local_80;
  undefined8 local_78;
  char *local_70;
  char *local_68;
  char *local_60;
  char *local_58;
  char *local_50;
  char *local_48;
  char *local_40;
  char *local_38;
  char *local_30;
  undefined8 local_28;
  undefined8 local_20;

  if (uParm1 == 0)
    goto LAB_00401b3d;
  func_0x00401620 (DAT_00611d60, "Try \'%s --help\' for more information.\n",
		   DAT_00611de0);
  do
    {
      func_0x004017a0 ((ulong) uParm1);
    LAB_00401b3d:
      ;
      func_0x00401490 ("Usage: %s [OPTION]\n", DAT_00611de0);
      uVar3 = DAT_00611d40;
      func_0x00401630 ("Print the name of the current user.\n\n",
		       DAT_00611d40);
      func_0x00401630 ("      --help     display this help and exit\n",
		       uVar3);
      func_0x00401630
	("      --version  output version information and exit\n", uVar3);
      local_88 = &DAT_0040f2c0;
      local_80 = "test invocation";
      puVar6 = &DAT_0040f2c0;
      local_78 = 0x40f33f;
      local_70 = "Multi-call invocation";
      local_68 = "sha224sum";
      local_60 = "sha2 utilities";
      local_58 = "sha256sum";
      local_50 = "sha2 utilities";
      local_48 = "sha384sum";
      local_40 = "sha2 utilities";
      local_38 = "sha512sum";
      local_30 = "sha2 utilities";
      local_28 = 0;
      local_20 = 0;
      ppuVar4 = &local_88;
      do
	{
	  iVar1 = func_0x00401700 ("logname", puVar6);
	  if (iVar1 == 0)
	    break;
	  ppuVar4 = ppuVar4 + 2;
	  puVar6 = *ppuVar4;
	}
      while (puVar6 != (undefined *) 0x0);
      pcVar5 = ppuVar4[1];
      if (pcVar5 == (char *) 0x0)
	{
	  func_0x00401490 ("\n%s online help: <%s>\n", "GNU coreutils",
			   "https://www.gnu.org/software/coreutils/");
	  lVar2 = func_0x00401760 (5, 0);
	  if (lVar2 != 0)
	    {
	      iVar1 = func_0x00401680 (lVar2, &DAT_0040f360, 3);
	      if (iVar1 != 0)
		{
		  pcVar5 = "logname";
		  goto LAB_00401cd1;
		}
	    }
	  func_0x00401490 ("Full documentation at: <%s%s>\n",
			   "https://www.gnu.org/software/coreutils/",
			   "logname");
	LAB_00401cfa:
	  ;
	  pcVar5 = "logname";
	  uVar3 = 0x40f2f8;
	}
      else
	{
	  func_0x00401490 ("\n%s online help: <%s>\n", "GNU coreutils",
			   "https://www.gnu.org/software/coreutils/");
	  lVar2 = func_0x00401760 (5, 0);
	  if (lVar2 != 0)
	    {
	      iVar1 = func_0x00401680 (lVar2, &DAT_0040f360, 3);
	      if (iVar1 != 0)
		{
		LAB_00401cd1:
		  ;
		  func_0x00401490
		    ("Report %s translation bugs to <https://translationproject.org/team/>\n",
		     "logname");
		}
	    }
	  func_0x00401490 ("Full documentation at: <%s%s>\n",
			   "https://www.gnu.org/software/coreutils/",
			   "logname");
	  uVar3 = 0x4101af;
	  if (pcVar5 == "logname")
	    goto LAB_00401cfa;
	}
      func_0x00401490
	("or available locally via: info \'(coreutils) %s%s\'\n", pcVar5,
	 uVar3);
    }
  while (true);
}

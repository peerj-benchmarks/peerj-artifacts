
#include "groups.h"

long null_ARRAY_006127c_0_4_;
long null_ARRAY_006127c_40_8_;
long null_ARRAY_006127c_4_4_;
long null_ARRAY_006127c_48_8_;
long null_ARRAY_0061280_0_8_;
long null_ARRAY_0061280_8_8_;
long null_ARRAY_00612a0_0_8_;
long null_ARRAY_00612a0_16_8_;
long null_ARRAY_00612a0_24_8_;
long null_ARRAY_00612a0_32_8_;
long null_ARRAY_00612a0_40_8_;
long null_ARRAY_00612a0_48_8_;
long null_ARRAY_00612a0_8_8_;
long null_ARRAY_00612a4_0_4_;
long null_ARRAY_00612a4_16_8_;
long null_ARRAY_00612a4_24_4_;
long null_ARRAY_00612a4_32_8_;
long null_ARRAY_00612a4_40_4_;
long null_ARRAY_00612a4_4_4_;
long null_ARRAY_00612a4_44_4_;
long null_ARRAY_00612a4_48_4_;
long null_ARRAY_00612a4_8_4_;
long local_2f_1_1_;
long local_5b_1_1_;
long local_9c_4_4_;
long local_a1_0_4_;
long local_a1_0_8_;
long local_a1_1_9_;
long local_a1_4_6_;
long local_a1_8_2_;
long local_a3_4_4_;
long local_a4_0_2_;
long local_a4_0_4_;
long local_a4_4_4_;
long local_a5_0_8_;
long local_a5_1_9_;
long local_a5_4_6_;
long local_a5_8_2_;
long local_a6_0_4_;
long DAT_00000008;
long DAT_00000010;
long DAT_0040fb40;
long DAT_0040fbcb;
long DAT_0040ffb8;
long DAT_0040ffbc;
long DAT_0040ffc0;
long DAT_0040ffc3;
long DAT_0040ffc5;
long DAT_0040ffc9;
long DAT_0040ffcd;
long DAT_0041056b;
long DAT_004109f5;
long DAT_00410af9;
long DAT_00410aff;
long DAT_00410b11;
long DAT_00410b12;
long DAT_00410b30;
long DAT_00410b34;
long DAT_00410bbe;
long DAT_006123b0;
long DAT_006123c0;
long DAT_006123d0;
long DAT_006127a8;
long DAT_00612810;
long DAT_00612814;
long DAT_00612818;
long DAT_0061281c;
long DAT_00612840;
long DAT_00612850;
long DAT_00612860;
long DAT_00612868;
long DAT_00612880;
long DAT_00612888;
long DAT_006128e8;
long DAT_006128f0;
long DAT_006128f8;
long DAT_00612a78;
long DAT_00612a80;
long DAT_00612a88;
long DAT_00612a98;
long _DYNAMIC;
long fde_00411568;
long int7;
long null_ARRAY_0040fe80;
long null_ARRAY_00410e60;
long null_ARRAY_004110b0;
long null_ARRAY_006127c0;
long null_ARRAY_00612800;
long null_ARRAY_006128a0;
long null_ARRAY_006128d0;
long null_ARRAY_00612900;
long null_ARRAY_00612a00;
long null_ARRAY_00612a40;
long PTR_DAT_006127a0;
long PTR_null_ARRAY_006127f8;
long register0x00000020;
long stack0x00000008;
void
FUN_00401e80 (uint uParm1)
{
  int iVar1;
  long lVar2;
  undefined8 uVar3;
  undefined **ppuVar4;
  char *pcVar5;
  undefined *puVar6;
  undefined *local_88;
  char *local_80;
  undefined8 local_78;
  char *local_70;
  char *local_68;
  char *local_60;
  char *local_58;
  char *local_50;
  char *local_48;
  char *local_40;
  char *local_38;
  char *local_30;
  undefined8 local_28;
  undefined8 local_20;

  if (uParm1 == 0)
    goto LAB_00401ead;
  func_0x004017c0 (DAT_00612860, "Try \'%s --help\' for more information.\n",
		   DAT_006128f8);
  do
    {
      func_0x00401980 ((ulong) uParm1);
    LAB_00401ead:
      ;
      func_0x00401620 ("Usage: %s [OPTION]... [USERNAME]...\n", DAT_006128f8);
      uVar3 = DAT_00612840;
      func_0x004017d0
	("Print group memberships for each USERNAME or, if no USERNAME is specified, for\nthe current process (which may differ if the groups database has changed).\n",
	 DAT_00612840);
      func_0x004017d0 ("      --help     display this help and exit\n",
		       uVar3);
      func_0x004017d0
	("      --version  output version information and exit\n", uVar3);
      local_88 = &DAT_0040fb40;
      local_80 = "test invocation";
      puVar6 = &DAT_0040fb40;
      local_78 = 0x40fbaa;
      local_70 = "Multi-call invocation";
      local_68 = "sha224sum";
      local_60 = "sha2 utilities";
      local_58 = "sha256sum";
      local_50 = "sha2 utilities";
      local_48 = "sha384sum";
      local_40 = "sha2 utilities";
      local_38 = "sha512sum";
      local_30 = "sha2 utilities";
      local_28 = 0;
      local_20 = 0;
      ppuVar4 = &local_88;
      do
	{
	  iVar1 = func_0x004018b0 ("groups", puVar6);
	  if (iVar1 == 0)
	    break;
	  ppuVar4 = ppuVar4 + 2;
	  puVar6 = *ppuVar4;
	}
      while (puVar6 != (undefined *) 0x0);
      pcVar5 = ppuVar4[1];
      if (pcVar5 == (char *) 0x0)
	{
	  func_0x00401620 ("\n%s online help: <%s>\n", "GNU coreutils",
			   "https://www.gnu.org/software/coreutils/");
	  lVar2 = func_0x00401930 (5, 0);
	  if (lVar2 != 0)
	    {
	      iVar1 = func_0x00401820 (lVar2, &DAT_0040fbcb, 3);
	      if (iVar1 != 0)
		{
		  pcVar5 = "groups";
		  goto LAB_00402041;
		}
	    }
	  func_0x00401620 ("Full documentation at: <%s%s>\n",
			   "https://www.gnu.org/software/coreutils/",
			   "groups");
	LAB_0040206a:
	  ;
	  pcVar5 = "groups";
	  uVar3 = 0x40fb63;
	}
      else
	{
	  func_0x00401620 ("\n%s online help: <%s>\n", "GNU coreutils",
			   "https://www.gnu.org/software/coreutils/");
	  lVar2 = func_0x00401930 (5, 0);
	  if (lVar2 != 0)
	    {
	      iVar1 = func_0x00401820 (lVar2, &DAT_0040fbcb, 3);
	      if (iVar1 != 0)
		{
		LAB_00402041:
		  ;
		  func_0x00401620
		    ("Report %s translation bugs to <https://translationproject.org/team/>\n",
		     "groups");
		}
	    }
	  func_0x00401620 ("Full documentation at: <%s%s>\n",
			   "https://www.gnu.org/software/coreutils/",
			   "groups");
	  uVar3 = 0x410b2f;
	  if (pcVar5 == "groups")
	    goto LAB_0040206a;
	}
      func_0x00401620
	("or available locally via: info \'(coreutils) %s%s\'\n", pcVar5,
	 uVar3);
    }
  while (true);
}

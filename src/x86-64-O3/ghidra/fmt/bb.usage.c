
#include "fmt.h"

long null_ARRAY_006149c_0_4_;
long null_ARRAY_006149c_40_8_;
long null_ARRAY_006149c_4_4_;
long null_ARRAY_006149c_48_8_;
long null_ARRAY_00614a0_0_8_;
long null_ARRAY_00614a0_8_8_;
long null_ARRAY_00614b4_20_4_;
long null_ARRAY_00614b4_32_8_;
long null_ARRAY_00614b4_8_4_;
long null_ARRAY_0061fcc_0_8_;
long null_ARRAY_0061fcc_16_8_;
long null_ARRAY_0061fcc_24_8_;
long null_ARRAY_0061fcc_32_8_;
long null_ARRAY_0061fcc_40_8_;
long null_ARRAY_0061fcc_48_8_;
long null_ARRAY_0061fcc_8_8_;
long null_ARRAY_0061fd0_0_4_;
long null_ARRAY_0061fd0_16_8_;
long null_ARRAY_0061fd0_24_4_;
long null_ARRAY_0061fd0_32_8_;
long null_ARRAY_0061fd0_40_4_;
long null_ARRAY_0061fd0_4_4_;
long null_ARRAY_0061fd0_44_4_;
long null_ARRAY_0061fd0_48_4_;
long null_ARRAY_0061fd0_8_4_;
long local_2f_1_1_;
long local_5b_1_1_;
long local_9c_4_4_;
long local_a1_0_4_;
long local_a1_0_8_;
long local_a1_1_9_;
long local_a1_4_6_;
long local_a1_8_2_;
long local_a3_4_4_;
long local_a4_0_2_;
long local_a4_0_4_;
long local_a4_4_4_;
long local_a5_0_8_;
long local_a5_1_9_;
long local_a5_4_6_;
long local_a5_8_2_;
long local_a6_0_4_;
long DAT_00000008;
long DAT_00000010;
long DAT_00411506;
long DAT_00411509;
long DAT_0041152b;
long DAT_0041152d;
long DAT_004115b3;
long DAT_00411d98;
long DAT_00411d9c;
long DAT_00411da0;
long DAT_00411da3;
long DAT_00411da7;
long DAT_00411dab;
long DAT_0041232b;
long DAT_004129a8;
long DAT_00412ab1;
long DAT_00412ab7;
long DAT_00412ac9;
long DAT_00412aca;
long DAT_00412ae8;
long DAT_00412aec;
long DAT_00412b76;
long DAT_006145b0;
long DAT_006145c0;
long DAT_006145d0;
long DAT_006149a8;
long DAT_00614a10;
long DAT_00614a14;
long DAT_00614a18;
long DAT_00614a1c;
long DAT_00614a40;
long DAT_00614a48;
long DAT_00614a50;
long DAT_00614a60;
long DAT_00614a68;
long DAT_00614a80;
long DAT_00614a88;
long DAT_00614b00;
long DAT_00614b04;
long DAT_00614b08;
long DAT_00614b0c;
long DAT_00614b10;
long DAT_00614b14;
long DAT_00614b18;
long DAT_00614b20;
long DAT_0061e780;
long DAT_0061fb48;
long DAT_0061fb4c;
long DAT_0061fb50;
long DAT_0061fb54;
long DAT_0061fb58;
long DAT_0061fb5c;
long DAT_0061fb60;
long DAT_0061fb68;
long DAT_0061fb70;
long DAT_0061fb71;
long DAT_0061fb72;
long DAT_0061fb73;
long DAT_0061fb78;
long DAT_0061fb80;
long DAT_0061fb88;
long DAT_0061fd38;
long DAT_0061fd40;
long DAT_0061fd48;
long DAT_0061fd58;
long fde_00413538;
long int7;
long null_ARRAY_00411c00;
long null_ARRAY_00412e00;
long null_ARRAY_00413050;
long null_ARRAY_006149c0;
long null_ARRAY_00614a00;
long null_ARRAY_00614aa0;
long null_ARRAY_00614b40;
long null_ARRAY_0061e7c0;
long null_ARRAY_0061fbc0;
long null_ARRAY_0061fcc0;
long null_ARRAY_0061fd00;
long PTR_DAT_006149a0;
long PTR_null_ARRAY_006149f8;
long register0x00000020;
void
FUN_00403290 (uint uParm1)
{
  int iVar1;
  long lVar2;
  undefined8 uVar3;
  undefined **ppuVar4;
  undefined *puVar5;
  undefined *local_88;
  char *local_80;
  undefined8 local_78;
  char *local_70;
  char *local_68;
  char *local_60;
  char *local_58;
  char *local_50;
  char *local_48;
  char *local_40;
  char *local_38;
  char *local_30;
  undefined8 local_28;
  undefined8 local_20;

  if (uParm1 == 0)
    goto LAB_004032bd;
  func_0x004017f0 (DAT_00614a60, "Try \'%s --help\' for more information.\n",
		   DAT_0061fb88);
  do
    {
      func_0x004019c0 ((ulong) uParm1);
    LAB_004032bd:
      ;
      func_0x00401650 ("Usage: %s [-WIDTH] [OPTION]... [FILE]...\n",
		       DAT_0061fb88);
      uVar3 = DAT_00614a40;
      func_0x00401800
	("Reformat each paragraph in the FILE(s), writing to standard output.\nThe option -WIDTH is an abbreviated form of --width=DIGITS.\n",
	 DAT_00614a40);
      func_0x00401800
	("\nWith no FILE, or when FILE is -, read standard input.\n", uVar3);
      func_0x00401800
	("\nMandatory arguments to long options are mandatory for short options too.\n",
	 uVar3);
      func_0x00401800
	("  -c, --crown-margin        preserve indentation of first two lines\n  -p, --prefix=STRING       reformat only lines beginning with STRING,\n                              reattaching the prefix to reformatted lines\n  -s, --split-only          split long lines, but do not refill\n",
	 uVar3);
      func_0x00401800
	("  -t, --tagged-paragraph    indentation of first line different from second\n  -u, --uniform-spacing     one space between words, two after sentences\n  -w, --width=WIDTH         maximum line width (default of 75 columns)\n  -g, --goal=WIDTH          goal width (default of 93% of width)\n",
	 uVar3);
      func_0x00401800 ("      --help     display this help and exit\n",
		       uVar3);
      func_0x00401800
	("      --version  output version information and exit\n", uVar3);
      local_88 = &DAT_0041152b;
      local_80 = "test invocation";
      puVar5 = &DAT_0041152b;
      local_78 = 0x411592;
      local_70 = "Multi-call invocation";
      local_68 = "sha224sum";
      local_60 = "sha2 utilities";
      local_58 = "sha256sum";
      local_50 = "sha2 utilities";
      local_48 = "sha384sum";
      local_40 = "sha2 utilities";
      local_38 = "sha512sum";
      local_30 = "sha2 utilities";
      local_28 = 0;
      local_20 = 0;
      ppuVar4 = &local_88;
      do
	{
	  iVar1 = func_0x00401900 (&DAT_0041152d, puVar5);
	  if (iVar1 == 0)
	    break;
	  ppuVar4 = ppuVar4 + 2;
	  puVar5 = *ppuVar4;
	}
      while (puVar5 != (undefined *) 0x0);
      puVar5 = ppuVar4[1];
      if (puVar5 == (undefined *) 0x0)
	{
	  func_0x00401650 ("\n%s online help: <%s>\n", "GNU coreutils",
			   "https://www.gnu.org/software/coreutils/");
	  lVar2 = func_0x00401960 (5, 0);
	  if (lVar2 != 0)
	    {
	      iVar1 = func_0x00401860 (lVar2, &DAT_004115b3, 3);
	      if (iVar1 != 0)
		{
		  puVar5 = &DAT_0041152d;
		  goto LAB_00403489;
		}
	    }
	  func_0x00401650 ("Full documentation at: <%s%s>\n",
			   "https://www.gnu.org/software/coreutils/",
			   &DAT_0041152d);
	LAB_004034b2:
	  ;
	  puVar5 = &DAT_0041152d;
	  uVar3 = 0x41154b;
	}
      else
	{
	  func_0x00401650 ("\n%s online help: <%s>\n", "GNU coreutils",
			   "https://www.gnu.org/software/coreutils/");
	  lVar2 = func_0x00401960 (5, 0);
	  if (lVar2 != 0)
	    {
	      iVar1 = func_0x00401860 (lVar2, &DAT_004115b3, 3);
	      if (iVar1 != 0)
		{
		LAB_00403489:
		  ;
		  func_0x00401650
		    ("Report %s translation bugs to <https://translationproject.org/team/>\n",
		     &DAT_0041152d);
		}
	    }
	  func_0x00401650 ("Full documentation at: <%s%s>\n",
			   "https://www.gnu.org/software/coreutils/",
			   &DAT_0041152d);
	  uVar3 = 0x412ae7;
	  if (puVar5 == &DAT_0041152d)
	    goto LAB_004034b2;
	}
      func_0x00401650
	("or available locally via: info \'(coreutils) %s%s\'\n", puVar5,
	 uVar3);
    }
  while (true);
}

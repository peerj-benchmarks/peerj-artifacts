
#include "rmdir.h"

long null_ARRAY_00612a4_0_8_;
long null_ARRAY_00612a4_8_8_;
long null_ARRAY_00612c4_0_8_;
long null_ARRAY_00612c4_16_8_;
long null_ARRAY_00612c4_24_8_;
long null_ARRAY_00612c4_32_8_;
long null_ARRAY_00612c4_40_8_;
long null_ARRAY_00612c4_48_8_;
long null_ARRAY_00612c4_8_8_;
long null_ARRAY_00612c8_0_4_;
long null_ARRAY_00612c8_16_8_;
long null_ARRAY_00612c8_24_4_;
long null_ARRAY_00612c8_32_8_;
long null_ARRAY_00612c8_40_4_;
long null_ARRAY_00612c8_4_4_;
long null_ARRAY_00612c8_44_4_;
long null_ARRAY_00612c8_48_4_;
long null_ARRAY_00612c8_8_4_;
long local_2f_1_1_;
long local_5b_1_1_;
long local_9c_4_4_;
long local_a1_0_4_;
long local_a1_0_8_;
long local_a1_1_9_;
long local_a1_4_6_;
long local_a1_8_2_;
long local_a3_4_4_;
long local_a4_0_2_;
long local_a4_0_4_;
long local_a4_4_4_;
long local_a5_0_8_;
long local_a5_1_9_;
long local_a5_4_6_;
long local_a5_8_2_;
long local_a6_0_4_;
long DAT_00000008;
long DAT_00000010;
long DAT_0040fbc0;
long DAT_0040fbc2;
long DAT_0040fbc6;
long DAT_0040fc4a;
long DAT_0040fc5e;
long DAT_00410138;
long DAT_0041013c;
long DAT_00410140;
long DAT_00410143;
long DAT_00410145;
long DAT_00410149;
long DAT_0041014d;
long DAT_004106eb;
long DAT_00410b75;
long DAT_00410b77;
long DAT_00410c79;
long DAT_00410c7f;
long DAT_00410c91;
long DAT_00410c92;
long DAT_00410cb0;
long DAT_00410cb4;
long DAT_00410d3e;
long DAT_00612600;
long DAT_00612610;
long DAT_00612620;
long DAT_006129e8;
long DAT_00612a50;
long DAT_00612a54;
long DAT_00612a58;
long DAT_00612a5c;
long DAT_00612a80;
long DAT_00612a90;
long DAT_00612aa0;
long DAT_00612aa8;
long DAT_00612ac0;
long DAT_00612ac8;
long DAT_00612b10;
long DAT_00612b11;
long DAT_00612b12;
long DAT_00612b18;
long DAT_00612b20;
long DAT_00612b28;
long DAT_00612cb8;
long DAT_00612cc0;
long DAT_00612cc8;
long DAT_00612cd0;
long DAT_00612cd8;
long DAT_00612ce8;
long fde_00411710;
long int7;
long null_ARRAY_00410000;
long null_ARRAY_00410fe0;
long null_ARRAY_00411230;
long null_ARRAY_00612a40;
long null_ARRAY_00612ae0;
long null_ARRAY_00612b40;
long null_ARRAY_00612c40;
long null_ARRAY_00612c80;
long PTR_DAT_006129e0;
long PTR_null_ARRAY_00612a38;
long register0x00000020;
void
FUN_00401f40 (uint uParm1)
{
  int iVar1;
  long lVar2;
  undefined8 uVar3;
  undefined **ppuVar4;
  undefined *puVar5;
  undefined *local_88;
  char *local_80;
  undefined8 local_78;
  char *local_70;
  char *local_68;
  char *local_60;
  char *local_58;
  char *local_50;
  char *local_48;
  char *local_40;
  char *local_38;
  char *local_30;
  undefined8 local_28;
  undefined8 local_20;

  if (uParm1 == 0)
    goto LAB_00401f6d;
  func_0x00401760 (DAT_00612aa0, "Try \'%s --help\' for more information.\n",
		   DAT_00612b28);
  do
    {
      func_0x004018e0 ((ulong) uParm1);
    LAB_00401f6d:
      ;
      func_0x004015b0 ("Usage: %s [OPTION]... DIRECTORY...\n", DAT_00612b28);
      uVar3 = DAT_00612a80;
      func_0x00401770
	("Remove the DIRECTORY(ies), if they are empty.\n\n      --ignore-fail-on-non-empty\n                  ignore each failure that is solely because a directory\n                    is non-empty\n",
	 DAT_00612a80);
      func_0x00401770
	("  -p, --parents   remove DIRECTORY and its ancestors; e.g., \'rmdir -p a/b/c\' is\n                    similar to \'rmdir a/b/c a/b a\'\n  -v, --verbose   output a diagnostic for every directory processed\n",
	 uVar3);
      func_0x00401770 ("      --help     display this help and exit\n",
		       uVar3);
      func_0x00401770
	("      --version  output version information and exit\n", uVar3);
      local_88 = &DAT_0040fbc0;
      local_80 = "test invocation";
      puVar5 = &DAT_0040fbc0;
      local_78 = 0x40fc29;
      local_70 = "Multi-call invocation";
      local_68 = "sha224sum";
      local_60 = "sha2 utilities";
      local_58 = "sha256sum";
      local_50 = "sha2 utilities";
      local_48 = "sha384sum";
      local_40 = "sha2 utilities";
      local_38 = "sha512sum";
      local_30 = "sha2 utilities";
      local_28 = 0;
      local_20 = 0;
      ppuVar4 = &local_88;
      do
	{
	  iVar1 = func_0x00401840 (&DAT_0040fbc2, puVar5);
	  if (iVar1 == 0)
	    break;
	  ppuVar4 = ppuVar4 + 2;
	  puVar5 = *ppuVar4;
	}
      while (puVar5 != (undefined *) 0x0);
      puVar5 = ppuVar4[1];
      if (puVar5 == (undefined *) 0x0)
	{
	  func_0x004015b0 ("\n%s online help: <%s>\n", "GNU coreutils",
			   "https://www.gnu.org/software/coreutils/");
	  lVar2 = func_0x004018a0 (5, 0);
	  if (lVar2 != 0)
	    {
	      iVar1 = func_0x004017c0 (lVar2, &DAT_0040fc4a, 3);
	      if (iVar1 != 0)
		{
		  puVar5 = &DAT_0040fbc2;
		  goto LAB_00402111;
		}
	    }
	  func_0x004015b0 ("Full documentation at: <%s%s>\n",
			   "https://www.gnu.org/software/coreutils/",
			   &DAT_0040fbc2);
	LAB_0040213a:
	  ;
	  puVar5 = &DAT_0040fbc2;
	  uVar3 = 0x40fbe2;
	}
      else
	{
	  func_0x004015b0 ("\n%s online help: <%s>\n", "GNU coreutils",
			   "https://www.gnu.org/software/coreutils/");
	  lVar2 = func_0x004018a0 (5, 0);
	  if (lVar2 != 0)
	    {
	      iVar1 = func_0x004017c0 (lVar2, &DAT_0040fc4a, 3);
	      if (iVar1 != 0)
		{
		LAB_00402111:
		  ;
		  func_0x004015b0
		    ("Report %s translation bugs to <https://translationproject.org/team/>\n",
		     &DAT_0040fbc2);
		}
	    }
	  func_0x004015b0 ("Full documentation at: <%s%s>\n",
			   "https://www.gnu.org/software/coreutils/",
			   &DAT_0040fbc2);
	  uVar3 = 0x410caf;
	  if (puVar5 == &DAT_0040fbc2)
	    goto LAB_0040213a;
	}
      func_0x004015b0
	("or available locally via: info \'(coreutils) %s%s\'\n", puVar5,
	 uVar3);
    }
  while (true);
}


#include "base32.h"

long null_ARRAY_0061442_0_4_;
long null_ARRAY_0061442_40_8_;
long null_ARRAY_0061442_4_4_;
long null_ARRAY_0061442_48_8_;
long null_ARRAY_0061446_0_8_;
long null_ARRAY_0061446_8_8_;
long null_ARRAY_0061464_0_8_;
long null_ARRAY_0061464_16_8_;
long null_ARRAY_0061464_24_8_;
long null_ARRAY_0061464_32_8_;
long null_ARRAY_0061464_40_8_;
long null_ARRAY_0061464_48_8_;
long null_ARRAY_0061464_8_8_;
long null_ARRAY_0061468_0_4_;
long null_ARRAY_0061468_16_8_;
long null_ARRAY_0061468_24_4_;
long null_ARRAY_0061468_32_8_;
long null_ARRAY_0061468_40_4_;
long null_ARRAY_0061468_4_4_;
long null_ARRAY_0061468_44_4_;
long null_ARRAY_0061468_48_4_;
long null_ARRAY_0061468_8_4_;
long local_2f_1_1_;
long local_5b_1_1_;
long local_9c_4_4_;
long local_a1_0_4_;
long local_a1_0_8_;
long local_a1_1_9_;
long local_a1_4_6_;
long local_a1_8_2_;
long local_a3_4_4_;
long local_a4_0_2_;
long local_a4_0_4_;
long local_a4_4_4_;
long local_a5_0_8_;
long local_a5_1_9_;
long local_a5_4_6_;
long local_a5_8_2_;
long local_a6_0_4_;
long DAT_00000008;
long DAT_00000010;
long DAT_00410d65;
long DAT_00410df0;
long DAT_00410e16;
long DAT_00410e2c;
long DAT_00411548;
long DAT_0041154c;
long DAT_00411550;
long DAT_00411553;
long DAT_00411555;
long DAT_00411559;
long DAT_0041155d;
long DAT_00411aeb;
long DAT_00412168;
long DAT_00412271;
long DAT_00412277;
long DAT_00412289;
long DAT_0041228a;
long DAT_004122a8;
long DAT_004122ac;
long DAT_00412336;
long DAT_00614000;
long DAT_00614010;
long DAT_00614020;
long DAT_00614408;
long DAT_00614470;
long DAT_00614474;
long DAT_00614478;
long DAT_0061447c;
long DAT_00614480;
long DAT_00614488;
long DAT_00614490;
long DAT_006144a0;
long DAT_006144a8;
long DAT_006144c0;
long DAT_006144c8;
long DAT_00614510;
long DAT_00614518;
long DAT_00614520;
long DAT_006146b8;
long DAT_006146c0;
long DAT_006146c8;
long DAT_006146d8;
long fde_00412cf8;
long int7;
long null_ARRAY_004112c0;
long null_ARRAY_00411400;
long null_ARRAY_004125c0;
long null_ARRAY_00412810;
long null_ARRAY_00614420;
long null_ARRAY_00614460;
long null_ARRAY_006144e0;
long null_ARRAY_00614540;
long null_ARRAY_00614640;
long null_ARRAY_00614680;
long PTR_DAT_00614400;
long PTR_null_ARRAY_00614458;
long register0x00000020;
void
FUN_00402360 (uint uParm1)
{
  int iVar1;
  long lVar2;
  undefined8 uVar3;
  undefined **ppuVar4;
  char *pcVar5;
  undefined *puVar6;
  undefined *local_88;
  char *local_80;
  undefined8 local_78;
  char *local_70;
  char *local_68;
  char *local_60;
  char *local_58;
  char *local_50;
  char *local_48;
  char *local_40;
  char *local_38;
  char *local_30;
  undefined8 local_28;
  undefined8 local_20;

  if (uParm1 == 0)
    goto LAB_0040238d;
  func_0x00401820 (DAT_006144a0, "Try \'%s --help\' for more information.\n",
		   DAT_00614520);
  do
    {
      func_0x00401a00 ((ulong) uParm1);
    LAB_0040238d:
      ;
      func_0x00401690
	("Usage: %s [OPTION]... [FILE]\nBase%d encode or decode FILE, or standard input, to standard output.\n",
	 DAT_00614520, 0x20);
      uVar3 = DAT_00614480;
      func_0x00401830
	("\nWith no FILE, or when FILE is -, read standard input.\n",
	 DAT_00614480);
      func_0x00401830
	("\nMandatory arguments to long options are mandatory for short options too.\n",
	 uVar3);
      func_0x00401830
	("  -d, --decode          decode data\n  -i, --ignore-garbage  when decoding, ignore non-alphabet characters\n  -w, --wrap=COLS       wrap encoded lines after COLS character (default 76).\n                          Use 0 to disable line wrapping\n\n",
	 uVar3);
      func_0x00401830 ("      --help     display this help and exit\n",
		       uVar3);
      func_0x00401830
	("      --version  output version information and exit\n", uVar3);
      func_0x00401690
	("\nThe data are encoded as described for the %s alphabet in RFC 4648.\nWhen decoding, the input may contain newlines in addition to the bytes of\nthe formal %s alphabet.  Use --ignore-garbage to attempt to recover\nfrom any other non-alphabet bytes in the encoded stream.\n",
	 "base32", "base32");
      local_88 = &DAT_00410d65;
      local_80 = "test invocation";
      puVar6 = &DAT_00410d65;
      local_78 = 0x410dcf;
      local_70 = "Multi-call invocation";
      local_68 = "sha224sum";
      local_60 = "sha2 utilities";
      local_58 = "sha256sum";
      local_50 = "sha2 utilities";
      local_48 = "sha384sum";
      local_40 = "sha2 utilities";
      local_38 = "sha512sum";
      local_30 = "sha2 utilities";
      local_28 = 0;
      local_20 = 0;
      ppuVar4 = &local_88;
      do
	{
	  iVar1 = func_0x00401940 ("base32", puVar6);
	  if (iVar1 == 0)
	    break;
	  ppuVar4 = ppuVar4 + 2;
	  puVar6 = *ppuVar4;
	}
      while (puVar6 != (undefined *) 0x0);
      pcVar5 = ppuVar4[1];
      if (pcVar5 == (char *) 0x0)
	{
	  func_0x00401690 ("\n%s online help: <%s>\n", "GNU coreutils",
			   "https://www.gnu.org/software/coreutils/");
	  lVar2 = func_0x004019a0 (5, 0);
	  if (lVar2 != 0)
	    {
	      iVar1 = func_0x00401890 (lVar2, &DAT_00410df0, 3);
	      if (iVar1 != 0)
		{
		  pcVar5 = "base32";
		  goto LAB_00402559;
		}
	    }
	  func_0x00401690 ("Full documentation at: <%s%s>\n",
			   "https://www.gnu.org/software/coreutils/",
			   "base32");
	LAB_00402582:
	  ;
	  pcVar5 = "base32";
	  uVar3 = 0x410d88;
	}
      else
	{
	  func_0x00401690 ("\n%s online help: <%s>\n", "GNU coreutils",
			   "https://www.gnu.org/software/coreutils/");
	  lVar2 = func_0x004019a0 (5, 0);
	  if (lVar2 != 0)
	    {
	      iVar1 = func_0x00401890 (lVar2, &DAT_00410df0, 3);
	      if (iVar1 != 0)
		{
		LAB_00402559:
		  ;
		  func_0x00401690
		    ("Report %s translation bugs to <https://translationproject.org/team/>\n",
		     "base32");
		}
	    }
	  func_0x00401690 ("Full documentation at: <%s%s>\n",
			   "https://www.gnu.org/software/coreutils/",
			   "base32");
	  uVar3 = 0x4122a7;
	  if (pcVar5 == "base32")
	    goto LAB_00402582;
	}
      func_0x00401690
	("or available locally via: info \'(coreutils) %s%s\'\n", pcVar5,
	 uVar3);
    }
  while (true);
}

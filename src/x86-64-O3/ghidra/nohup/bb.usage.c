
#include "nohup.h"

long null_ARRAY_00612b0_0_8_;
long null_ARRAY_00612b0_8_8_;
long null_ARRAY_00612d0_0_8_;
long null_ARRAY_00612d0_16_8_;
long null_ARRAY_00612d0_24_8_;
long null_ARRAY_00612d0_32_8_;
long null_ARRAY_00612d0_40_8_;
long null_ARRAY_00612d0_48_8_;
long null_ARRAY_00612d0_8_8_;
long null_ARRAY_00612d4_0_4_;
long null_ARRAY_00612d4_16_8_;
long null_ARRAY_00612d4_24_4_;
long null_ARRAY_00612d4_32_8_;
long null_ARRAY_00612d4_40_4_;
long null_ARRAY_00612d4_4_4_;
long null_ARRAY_00612d4_44_4_;
long null_ARRAY_00612d4_48_4_;
long null_ARRAY_00612d4_8_4_;
long local_2f_1_1_;
long local_4_4_4_;
long local_5_4_4_;
long local_5b_1_1_;
long local_9c_4_4_;
long local_a1_0_4_;
long local_a1_0_8_;
long local_a1_1_9_;
long local_a1_4_6_;
long local_a1_8_2_;
long local_a3_4_4_;
long local_a4_0_2_;
long local_a4_0_4_;
long local_a4_4_4_;
long local_a5_0_8_;
long local_a5_1_9_;
long local_a5_4_6_;
long local_a5_8_2_;
long local_a6_0_4_;
long DAT_00000008;
long DAT_00000010;
long DAT_0040fbc0;
long DAT_0040fc4a;
long DAT_0040fca9;
long DAT_0040fcd4;
long DAT_00410260;
long DAT_00410264;
long DAT_00410268;
long DAT_0041026b;
long DAT_0041026d;
long DAT_00410271;
long DAT_00410275;
long DAT_0041082b;
long DAT_00410cb5;
long DAT_00410db9;
long DAT_00410dbf;
long DAT_00410dc1;
long DAT_00410dc2;
long DAT_00410de0;
long DAT_00410de4;
long DAT_00410e6e;
long DAT_006126b0;
long DAT_006126c0;
long DAT_006126d0;
long DAT_00612aa8;
long DAT_00612b10;
long DAT_00612b14;
long DAT_00612b18;
long DAT_00612b1c;
long DAT_00612b40;
long DAT_00612b50;
long DAT_00612b60;
long DAT_00612b68;
long DAT_00612b80;
long DAT_00612b88;
long DAT_00612bd0;
long DAT_00612bd8;
long DAT_00612be0;
long DAT_00612d38;
long DAT_00612d78;
long DAT_00612d80;
long DAT_00612d88;
long DAT_00612d98;
long _DYNAMIC;
long fde_00411818;
long int7;
long null_ARRAY_00410160;
long null_ARRAY_004101c0;
long null_ARRAY_00411100;
long null_ARRAY_00411350;
long null_ARRAY_00612b00;
long null_ARRAY_00612ba0;
long null_ARRAY_00612c00;
long null_ARRAY_00612d00;
long null_ARRAY_00612d40;
long PTR_DAT_00612aa0;
long PTR_null_ARRAY_00612af8;
long register0x00000020;
void
FUN_00402030 (uint uParm1)
{
  int iVar1;
  long lVar2;
  undefined8 uVar3;
  undefined **ppuVar4;
  char *pcVar5;
  undefined *puVar6;
  undefined *local_88;
  char *local_80;
  undefined8 local_78;
  char *local_70;
  char *local_68;
  char *local_60;
  char *local_58;
  char *local_50;
  char *local_48;
  char *local_40;
  char *local_38;
  char *local_30;
  undefined8 local_28;
  undefined8 local_20;

  if (uParm1 == 0)
    goto LAB_0040205d;
  func_0x00401720 (DAT_00612b60, "Try \'%s --help\' for more information.\n",
		   DAT_00612be0);
  do
    {
      func_0x004018c0 ((ulong) uParm1);
    LAB_0040205d:
      ;
      func_0x00401580 ("Usage: %s COMMAND [ARG]...\n  or:  %s OPTION\n",
		       DAT_00612be0, DAT_00612be0);
      uVar3 = DAT_00612b40;
      func_0x00401730 ("Run COMMAND, ignoring hangup signals.\n\n",
		       DAT_00612b40);
      func_0x00401730 ("      --help     display this help and exit\n",
		       uVar3);
      func_0x00401730
	("      --version  output version information and exit\n", uVar3);
      func_0x00401580
	("\nIf standard input is a terminal, redirect it from an unreadable file.\nIf standard output is a terminal, append output to \'nohup.out\' if possible,\n\'$HOME/nohup.out\' otherwise.\nIf standard error is a terminal, redirect it to standard output.\nTo save output to FILE, use \'%s COMMAND > FILE\'.\n",
	 DAT_00612be0);
      func_0x00401580
	("\nNOTE: your shell may have its own version of %s, which usually supersedes\nthe version described here.  Please refer to your shell\'s documentation\nfor details about the options it supports.\n",
	 "nohup");
      local_88 = &DAT_0040fbc0;
      local_80 = "test invocation";
      puVar6 = &DAT_0040fbc0;
      local_78 = 0x40fc29;
      local_70 = "Multi-call invocation";
      local_68 = "sha224sum";
      local_60 = "sha2 utilities";
      local_58 = "sha256sum";
      local_50 = "sha2 utilities";
      local_48 = "sha384sum";
      local_40 = "sha2 utilities";
      local_38 = "sha512sum";
      local_30 = "sha2 utilities";
      local_28 = 0;
      local_20 = 0;
      ppuVar4 = &local_88;
      do
	{
	  iVar1 = func_0x00401820 ("nohup", puVar6);
	  if (iVar1 == 0)
	    break;
	  ppuVar4 = ppuVar4 + 2;
	  puVar6 = *ppuVar4;
	}
      while (puVar6 != (undefined *) 0x0);
      pcVar5 = ppuVar4[1];
      if (pcVar5 == (char *) 0x0)
	{
	  func_0x00401580 ("\n%s online help: <%s>\n", "GNU coreutils",
			   "https://www.gnu.org/software/coreutils/");
	  lVar2 = func_0x00401880 (5, 0);
	  if (lVar2 != 0)
	    {
	      iVar1 = func_0x004017a0 (lVar2, &DAT_0040fc4a, 3);
	      if (iVar1 != 0)
		{
		  pcVar5 = "nohup";
		  goto LAB_00402219;
		}
	    }
	  func_0x00401580 ("Full documentation at: <%s%s>\n",
			   "https://www.gnu.org/software/coreutils/",
			   "nohup");
	LAB_00402242:
	  ;
	  pcVar5 = "nohup";
	  uVar3 = 0x40fbe2;
	}
      else
	{
	  func_0x00401580 ("\n%s online help: <%s>\n", "GNU coreutils",
			   "https://www.gnu.org/software/coreutils/");
	  lVar2 = func_0x00401880 (5, 0);
	  if (lVar2 != 0)
	    {
	      iVar1 = func_0x004017a0 (lVar2, &DAT_0040fc4a, 3);
	      if (iVar1 != 0)
		{
		LAB_00402219:
		  ;
		  func_0x00401580
		    ("Report %s translation bugs to <https://translationproject.org/team/>\n",
		     "nohup");
		}
	    }
	  func_0x00401580 ("Full documentation at: <%s%s>\n",
			   "https://www.gnu.org/software/coreutils/",
			   "nohup");
	  uVar3 = 0x410ddf;
	  if (pcVar5 == "nohup")
	    goto LAB_00402242;
	}
      func_0x00401580
	("or available locally via: info \'(coreutils) %s%s\'\n", pcVar5,
	 uVar3);
    }
  while (true);
}

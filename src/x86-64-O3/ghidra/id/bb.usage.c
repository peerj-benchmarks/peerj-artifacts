
#include "id.h"

long null_ARRAY_0061450_0_4_;
long null_ARRAY_0061450_40_8_;
long null_ARRAY_0061450_4_4_;
long null_ARRAY_0061450_48_8_;
long null_ARRAY_0061454_0_8_;
long null_ARRAY_0061454_8_8_;
long null_ARRAY_006147c_0_8_;
long null_ARRAY_006147c_16_8_;
long null_ARRAY_006147c_24_8_;
long null_ARRAY_006147c_32_8_;
long null_ARRAY_006147c_40_8_;
long null_ARRAY_006147c_48_8_;
long null_ARRAY_006147c_8_8_;
long null_ARRAY_0061480_0_4_;
long null_ARRAY_0061480_16_8_;
long null_ARRAY_0061480_24_4_;
long null_ARRAY_0061480_32_8_;
long null_ARRAY_0061480_40_4_;
long null_ARRAY_0061480_4_4_;
long null_ARRAY_0061480_44_4_;
long null_ARRAY_0061480_48_4_;
long null_ARRAY_0061480_8_4_;
long local_2f_1_1_;
long local_5b_1_1_;
long local_7_0_4_;
long local_9c_4_4_;
long local_a1_0_4_;
long local_a1_0_8_;
long local_a1_1_9_;
long local_a1_4_6_;
long local_a1_8_2_;
long local_a3_4_4_;
long local_a4_0_2_;
long local_a4_0_4_;
long local_a4_4_4_;
long local_a5_0_8_;
long local_a5_1_9_;
long local_a5_4_6_;
long local_a5_8_2_;
long local_a6_0_4_;
long DAT_00000008;
long DAT_00000010;
long DAT_00411040;
long DAT_00411042;
long DAT_004110e5;
long DAT_0041119d;
long DAT_00411940;
long DAT_00411944;
long DAT_00411948;
long DAT_0041194b;
long DAT_0041194d;
long DAT_00411951;
long DAT_00411955;
long DAT_00411f13;
long DAT_00412586;
long DAT_00412689;
long DAT_0041268f;
long DAT_00412691;
long DAT_00412692;
long DAT_004126b0;
long DAT_004126b4;
long DAT_0041273e;
long DAT_006140b0;
long DAT_006140c0;
long DAT_006140d0;
long DAT_006144e0;
long DAT_006144f0;
long DAT_00614550;
long DAT_00614554;
long DAT_00614558;
long DAT_0061455c;
long DAT_00614580;
long DAT_00614590;
long DAT_006145a0;
long DAT_006145a8;
long DAT_006145c0;
long DAT_006145c8;
long DAT_00614648;
long DAT_0061464c;
long DAT_00614650;
long DAT_00614654;
long DAT_00614658;
long DAT_00614678;
long DAT_00614680;
long DAT_00614688;
long DAT_00614838;
long DAT_00614840;
long DAT_00614848;
long DAT_00614858;
long _DYNAMIC;
long fde_00413110;
long int7;
long null_ARRAY_00411780;
long null_ARRAY_004129e0;
long null_ARRAY_00412c30;
long null_ARRAY_00614500;
long null_ARRAY_00614540;
long null_ARRAY_006145e0;
long null_ARRAY_00614610;
long null_ARRAY_00614630;
long null_ARRAY_00614660;
long null_ARRAY_006146c0;
long null_ARRAY_006147c0;
long null_ARRAY_00614800;
long PTR_DAT_006144e8;
long PTR_null_ARRAY_00614538;
long register0x00000020;
void
FUN_004025d0 (uint uParm1)
{
  int iVar1;
  long lVar2;
  undefined8 uVar3;
  undefined **ppuVar4;
  undefined *puVar5;
  undefined *local_88;
  char *local_80;
  undefined8 local_78;
  char *local_70;
  char *local_68;
  char *local_60;
  char *local_58;
  char *local_50;
  char *local_48;
  char *local_40;
  char *local_38;
  char *local_30;
  undefined8 local_28;
  undefined8 local_20;

  if (uParm1 == 0)
    goto LAB_004025fd;
  func_0x00401960 (DAT_006145a0, "Try \'%s --help\' for more information.\n",
		   DAT_00614688);
  do
    {
      func_0x00401b40 ((ulong) uParm1);
    LAB_004025fd:
      ;
      func_0x00401790 ("Usage: %s [OPTION]... [USER]\n", DAT_00614688);
      uVar3 = DAT_00614580;
      func_0x00401970
	("Print user and group information for the specified USER,\nor (when USER omitted) for the current user.\n\n",
	 DAT_00614580);
      func_0x00401970
	("  -a             ignore, for compatibility with other versions\n  -Z, --context  print only the security context of the process\n  -g, --group    print only the effective group ID\n  -G, --groups   print all group IDs\n  -n, --name     print a name instead of a number, for -ugG\n  -r, --real     print the real ID instead of the effective ID, with -ugG\n  -u, --user     print only the effective user ID\n  -z, --zero     delimit entries with NUL characters, not whitespace;\n                   not permitted in default format\n",
	 uVar3);
      func_0x00401970 ("      --help     display this help and exit\n",
		       uVar3);
      func_0x00401970
	("      --version  output version information and exit\n", uVar3);
      func_0x00401970
	("\nWithout any OPTION, print some useful set of identified information.\n",
	 uVar3);
      local_88 = &DAT_00411040;
      local_80 = "test invocation";
      puVar5 = &DAT_00411040;
      local_78 = 0x4110c4;
      local_70 = "Multi-call invocation";
      local_68 = "sha224sum";
      local_60 = "sha2 utilities";
      local_58 = "sha256sum";
      local_50 = "sha2 utilities";
      local_48 = "sha384sum";
      local_40 = "sha2 utilities";
      local_38 = "sha512sum";
      local_30 = "sha2 utilities";
      local_28 = 0;
      local_20 = 0;
      ppuVar4 = &local_88;
      do
	{
	  iVar1 = func_0x00401a70 (&DAT_00411042, puVar5);
	  if (iVar1 == 0)
	    break;
	  ppuVar4 = ppuVar4 + 2;
	  puVar5 = *ppuVar4;
	}
      while (puVar5 != (undefined *) 0x0);
      puVar5 = ppuVar4[1];
      if (puVar5 == (undefined *) 0x0)
	{
	  func_0x00401790 ("\n%s online help: <%s>\n", "GNU coreutils",
			   "https://www.gnu.org/software/coreutils/");
	  lVar2 = func_0x00401af0 (5, 0);
	  if (lVar2 != 0)
	    {
	      iVar1 = func_0x004019d0 (lVar2, &DAT_004110e5, 3);
	      if (iVar1 != 0)
		{
		  puVar5 = &DAT_00411042;
		  goto LAB_004027a9;
		}
	    }
	  func_0x00401790 ("Full documentation at: <%s%s>\n",
			   "https://www.gnu.org/software/coreutils/",
			   &DAT_00411042);
	LAB_004027d2:
	  ;
	  puVar5 = &DAT_00411042;
	  uVar3 = 0x41107d;
	}
      else
	{
	  func_0x00401790 ("\n%s online help: <%s>\n", "GNU coreutils",
			   "https://www.gnu.org/software/coreutils/");
	  lVar2 = func_0x00401af0 (5, 0);
	  if (lVar2 != 0)
	    {
	      iVar1 = func_0x004019d0 (lVar2, &DAT_004110e5, 3);
	      if (iVar1 != 0)
		{
		LAB_004027a9:
		  ;
		  func_0x00401790
		    ("Report %s translation bugs to <https://translationproject.org/team/>\n",
		     &DAT_00411042);
		}
	    }
	  func_0x00401790 ("Full documentation at: <%s%s>\n",
			   "https://www.gnu.org/software/coreutils/",
			   &DAT_00411042);
	  uVar3 = 0x4126af;
	  if (puVar5 == &DAT_00411042)
	    goto LAB_004027d2;
	}
      func_0x00401790
	("or available locally via: info \'(coreutils) %s%s\'\n", puVar5,
	 uVar3);
    }
  while (true);
}

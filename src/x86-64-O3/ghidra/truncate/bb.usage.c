
#include "truncate.h"

long null_ARRAY_0061392_0_4_;
long null_ARRAY_0061392_40_8_;
long null_ARRAY_0061392_4_4_;
long null_ARRAY_0061392_48_8_;
long null_ARRAY_0061396_0_8_;
long null_ARRAY_0061396_8_8_;
long null_ARRAY_00613b4_0_8_;
long null_ARRAY_00613b4_16_8_;
long null_ARRAY_00613b4_24_8_;
long null_ARRAY_00613b4_32_8_;
long null_ARRAY_00613b4_40_8_;
long null_ARRAY_00613b4_48_8_;
long null_ARRAY_00613b4_8_8_;
long null_ARRAY_00613b8_0_4_;
long null_ARRAY_00613b8_16_8_;
long null_ARRAY_00613b8_24_4_;
long null_ARRAY_00613b8_32_8_;
long null_ARRAY_00613b8_40_4_;
long null_ARRAY_00613b8_4_4_;
long null_ARRAY_00613b8_44_4_;
long null_ARRAY_00613b8_48_4_;
long null_ARRAY_00613b8_8_4_;
long bVar1;
long local_2f_1_1_;
long local_5b_1_1_;
long local_9c_4_4_;
long local_a1_0_4_;
long local_a1_0_8_;
long local_a1_1_9_;
long local_a1_4_6_;
long local_a1_8_2_;
long local_a3_4_4_;
long local_a4_0_2_;
long local_a4_0_4_;
long local_a4_4_4_;
long local_a5_0_8_;
long local_a5_1_9_;
long local_a5_4_6_;
long local_a5_8_2_;
long local_a6_0_4_;
long DAT_00000008;
long DAT_00000010;
long DAT_00410540;
long DAT_004105ea;
long DAT_00410eb8;
long DAT_00410ebc;
long DAT_00410ec0;
long DAT_00410ec3;
long DAT_00410ec5;
long DAT_00410ec9;
long DAT_00410ecd;
long DAT_0041146b;
long DAT_00411ae8;
long DAT_00411bf1;
long DAT_00411bf7;
long DAT_00411c09;
long DAT_00411c0a;
long DAT_00411c28;
long DAT_00411c2c;
long DAT_00411cb6;
long DAT_00613528;
long DAT_00613538;
long DAT_00613548;
long DAT_00613908;
long DAT_00613970;
long DAT_00613974;
long DAT_00613978;
long DAT_0061397c;
long DAT_00613980;
long DAT_00613990;
long DAT_006139a0;
long DAT_006139a8;
long DAT_006139c0;
long DAT_006139c8;
long DAT_00613a10;
long DAT_00613a18;
long DAT_00613a19;
long DAT_00613a20;
long DAT_00613a28;
long DAT_00613a30;
long DAT_00613bb8;
long DAT_00613bc0;
long DAT_00613bc8;
long DAT_00613bd8;
long fde_00412648;
long int7;
long null_ARRAY_00411f40;
long null_ARRAY_00412190;
long null_ARRAY_00613920;
long null_ARRAY_00613960;
long null_ARRAY_006139e0;
long null_ARRAY_00613a40;
long null_ARRAY_00613b40;
long null_ARRAY_00613b80;
long PTR_DAT_00613900;
long PTR_null_ARRAY_00613958;
long register0x00000020;
void
FUN_00402390 (uint uParm1)
{
  int iVar1;
  long lVar2;
  undefined8 uVar3;
  undefined **ppuVar4;
  char *pcVar5;
  undefined *puVar6;
  undefined *local_88;
  char *local_80;
  undefined8 local_78;
  char *local_70;
  char *local_68;
  char *local_60;
  char *local_58;
  char *local_50;
  char *local_48;
  char *local_40;
  char *local_38;
  char *local_30;
  undefined8 local_28;
  undefined8 local_20;

  if (uParm1 == 0)
    goto LAB_004023bd;
  func_0x004016e0 (DAT_006139a0, "Try \'%s --help\' for more information.\n",
		   DAT_00613a30);
  do
    {
      func_0x00401880 ((ulong) uParm1);
    LAB_004023bd:
      ;
      func_0x00401540 ("Usage: %s OPTION... FILE...\n", DAT_00613a30);
      uVar3 = DAT_00613980;
      func_0x004016f0
	("Shrink or extend the size of each FILE to the specified size\n\nA FILE argument that does not exist is created.\n\nIf a FILE is larger than the specified size, the extra data is lost.\nIf a FILE is shorter, it is extended and the extended part (hole)\nreads as zero bytes.\n",
	 DAT_00613980);
      func_0x004016f0
	("\nMandatory arguments to long options are mandatory for short options too.\n",
	 uVar3);
      func_0x004016f0 ("  -c, --no-create        do not create any files\n",
		       uVar3);
      func_0x004016f0
	("  -o, --io-blocks        treat SIZE as number of IO blocks instead of bytes\n",
	 uVar3);
      func_0x004016f0
	("  -r, --reference=RFILE  base size on RFILE\n  -s, --size=SIZE        set or adjust the file size by SIZE bytes\n",
	 uVar3);
      func_0x004016f0 ("      --help     display this help and exit\n",
		       uVar3);
      func_0x004016f0
	("      --version  output version information and exit\n", uVar3);
      func_0x004016f0
	("\nThe SIZE argument is an integer and optional unit (example: 10K is 10*1024).\nUnits are K,M,G,T,P,E,Z,Y (powers of 1024) or KB,MB,... (powers of 1000).\n",
	 uVar3);
      func_0x004016f0
	("\nSIZE may also be prefixed by one of the following modifying characters:\n\'+\' extend by, \'-\' reduce by, \'<\' at most, \'>\' at least,\n\'/\' round down to multiple of, \'%\' round up to multiple of.\n",
	 uVar3);
      local_88 = &DAT_00410540;
      local_80 = "test invocation";
      puVar6 = &DAT_00410540;
      local_78 = 0x4105c9;
      local_70 = "Multi-call invocation";
      local_68 = "sha224sum";
      local_60 = "sha2 utilities";
      local_58 = "sha256sum";
      local_50 = "sha2 utilities";
      local_48 = "sha384sum";
      local_40 = "sha2 utilities";
      local_38 = "sha512sum";
      local_30 = "sha2 utilities";
      local_28 = 0;
      local_20 = 0;
      ppuVar4 = &local_88;
      do
	{
	  iVar1 = func_0x004017d0 ("truncate", puVar6);
	  if (iVar1 == 0)
	    break;
	  ppuVar4 = ppuVar4 + 2;
	  puVar6 = *ppuVar4;
	}
      while (puVar6 != (undefined *) 0x0);
      pcVar5 = ppuVar4[1];
      if (pcVar5 == (char *) 0x0)
	{
	  func_0x00401540 ("\n%s online help: <%s>\n", "GNU coreutils",
			   "https://www.gnu.org/software/coreutils/");
	  lVar2 = func_0x00401830 (5, 0);
	  if (lVar2 != 0)
	    {
	      iVar1 = func_0x00401740 (lVar2, &DAT_004105ea, 3);
	      if (iVar1 != 0)
		{
		  pcVar5 = "truncate";
		  goto LAB_004025a1;
		}
	    }
	  func_0x00401540 ("Full documentation at: <%s%s>\n",
			   "https://www.gnu.org/software/coreutils/",
			   "truncate");
	LAB_004025ca:
	  ;
	  pcVar5 = "truncate";
	  uVar3 = 0x410582;
	}
      else
	{
	  func_0x00401540 ("\n%s online help: <%s>\n", "GNU coreutils",
			   "https://www.gnu.org/software/coreutils/");
	  lVar2 = func_0x00401830 (5, 0);
	  if (lVar2 != 0)
	    {
	      iVar1 = func_0x00401740 (lVar2, &DAT_004105ea, 3);
	      if (iVar1 != 0)
		{
		LAB_004025a1:
		  ;
		  func_0x00401540
		    ("Report %s translation bugs to <https://translationproject.org/team/>\n",
		     "truncate");
		}
	    }
	  func_0x00401540 ("Full documentation at: <%s%s>\n",
			   "https://www.gnu.org/software/coreutils/",
			   "truncate");
	  uVar3 = 0x411c27;
	  if (pcVar5 == "truncate")
	    goto LAB_004025ca;
	}
      func_0x00401540
	("or available locally via: info \'(coreutils) %s%s\'\n", pcVar5,
	 uVar3);
    }
  while (true);
}


#include "tee.h"

long null_ARRAY_0061356_0_4_;
long null_ARRAY_0061356_40_8_;
long null_ARRAY_0061356_4_4_;
long null_ARRAY_0061356_48_8_;
long null_ARRAY_006135a_0_8_;
long null_ARRAY_006135a_8_8_;
long null_ARRAY_0061378_0_8_;
long null_ARRAY_0061378_16_8_;
long null_ARRAY_0061378_24_8_;
long null_ARRAY_0061378_32_8_;
long null_ARRAY_0061378_40_8_;
long null_ARRAY_0061378_48_8_;
long null_ARRAY_0061378_8_8_;
long null_ARRAY_006137c_0_4_;
long null_ARRAY_006137c_16_8_;
long null_ARRAY_006137c_24_4_;
long null_ARRAY_006137c_32_8_;
long null_ARRAY_006137c_40_4_;
long null_ARRAY_006137c_4_4_;
long null_ARRAY_006137c_44_4_;
long null_ARRAY_006137c_48_4_;
long null_ARRAY_006137c_8_4_;
long local_2f_1_1_;
long local_5b_1_1_;
long local_9c_4_4_;
long local_a1_0_4_;
long local_a1_0_8_;
long local_a1_1_9_;
long local_a1_4_6_;
long local_a1_8_2_;
long local_a3_4_4_;
long local_a4_0_2_;
long local_a4_0_4_;
long local_a4_4_4_;
long local_a5_0_8_;
long local_a5_1_9_;
long local_a5_4_6_;
long local_a5_8_2_;
long local_a6_0_4_;
long DAT_00000008;
long DAT_00000010;
long DAT_00410300;
long DAT_00410302;
long DAT_0041031f;
long DAT_00410321;
long DAT_004103a7;
long DAT_004103ea;
long DAT_00410ada;
long DAT_00410adc;
long DAT_00410b38;
long DAT_00410b3c;
long DAT_00410b40;
long DAT_00410b43;
long DAT_00410b45;
long DAT_00410b49;
long DAT_00410b4d;
long DAT_004110eb;
long DAT_00411575;
long DAT_00411679;
long DAT_0041167f;
long DAT_00411691;
long DAT_00411692;
long DAT_004116b0;
long DAT_004116b4;
long DAT_0041173e;
long DAT_00613148;
long DAT_00613158;
long DAT_00613168;
long DAT_00613550;
long DAT_006135b0;
long DAT_006135b4;
long DAT_006135b8;
long DAT_006135bc;
long DAT_006135c0;
long DAT_006135c8;
long DAT_006135d0;
long DAT_006135e0;
long DAT_006135e8;
long DAT_00613600;
long DAT_00613608;
long DAT_00613650;
long DAT_00613654;
long DAT_00613655;
long DAT_00613658;
long DAT_00613660;
long DAT_00613668;
long DAT_006137b8;
long DAT_006137f8;
long DAT_00613800;
long DAT_00613808;
long DAT_00613818;
long _DYNAMIC;
long fde_00412118;
long int7;
long null_ARRAY_00410940;
long null_ARRAY_00410960;
long null_ARRAY_004109c0;
long null_ARRAY_004119e0;
long null_ARRAY_00411c30;
long null_ARRAY_00613560;
long null_ARRAY_006135a0;
long null_ARRAY_00613620;
long null_ARRAY_00613680;
long null_ARRAY_00613780;
long null_ARRAY_006137c0;
long PTR_DAT_00613540;
long PTR_null_ARRAY_00613598;
long register0x00000020;
void
FUN_00402160 (uint uParm1)
{
  int iVar1;
  long lVar2;
  undefined8 uVar3;
  undefined **ppuVar4;
  undefined *puVar5;
  undefined *local_88;
  char *local_80;
  undefined8 local_78;
  char *local_70;
  char *local_68;
  char *local_60;
  char *local_58;
  char *local_50;
  char *local_48;
  char *local_40;
  char *local_38;
  char *local_30;
  undefined8 local_28;
  undefined8 local_20;

  if (uParm1 == 0)
    goto LAB_0040218d;
  func_0x004017a0 (DAT_006135e0, "Try \'%s --help\' for more information.\n",
		   DAT_00613668);
  do
    {
      func_0x00401970 ((ulong) uParm1);
    LAB_0040218d:
      ;
      func_0x00401610 ("Usage: %s [OPTION]... [FILE]...\n", DAT_00613668);
      uVar3 = DAT_006135c0;
      func_0x004017b0
	("Copy standard input to each FILE, and also to standard output.\n\n  -a, --append              append to the given FILEs, do not overwrite\n  -i, --ignore-interrupts   ignore interrupt signals\n",
	 DAT_006135c0);
      func_0x004017b0
	("  -p                        diagnose errors writing to non pipes\n      --output-error[=MODE]   set behavior on write error.  See MODE below\n",
	 uVar3);
      func_0x004017b0 ("      --help     display this help and exit\n",
		       uVar3);
      func_0x004017b0
	("      --version  output version information and exit\n", uVar3);
      func_0x004017b0
	("\nMODE determines behavior with write errors on the outputs:\n  \'warn\'         diagnose errors writing to any output\n  \'warn-nopipe\'  diagnose errors writing to any output not a pipe\n  \'exit\'         exit on error writing to any output\n  \'exit-nopipe\'  exit on error writing to any output not a pipe\nThe default MODE for the -p option is \'warn-nopipe\'.\nThe default operation when --output-error is not specified, is to\nexit immediately on error writing to a pipe, and diagnose errors\nwriting to non pipe outputs.\n",
	 uVar3);
      local_88 = &DAT_0041031f;
      local_80 = "test invocation";
      puVar5 = &DAT_0041031f;
      local_78 = 0x410386;
      local_70 = "Multi-call invocation";
      local_68 = "sha224sum";
      local_60 = "sha2 utilities";
      local_58 = "sha256sum";
      local_50 = "sha2 utilities";
      local_48 = "sha384sum";
      local_40 = "sha2 utilities";
      local_38 = "sha512sum";
      local_30 = "sha2 utilities";
      local_28 = 0;
      local_20 = 0;
      ppuVar4 = &local_88;
      do
	{
	  iVar1 = func_0x004018c0 (&DAT_00410321, puVar5);
	  if (iVar1 == 0)
	    break;
	  ppuVar4 = ppuVar4 + 2;
	  puVar5 = *ppuVar4;
	}
      while (puVar5 != (undefined *) 0x0);
      puVar5 = ppuVar4[1];
      if (puVar5 == (undefined *) 0x0)
	{
	  func_0x00401610 ("\n%s online help: <%s>\n", "GNU coreutils",
			   "https://www.gnu.org/software/coreutils/");
	  lVar2 = func_0x00401920 (5, 0);
	  if (lVar2 != 0)
	    {
	      iVar1 = func_0x00401830 (lVar2, &DAT_004103a7, 3);
	      if (iVar1 != 0)
		{
		  puVar5 = &DAT_00410321;
		  goto LAB_00402339;
		}
	    }
	  func_0x00401610 ("Full documentation at: <%s%s>\n",
			   "https://www.gnu.org/software/coreutils/",
			   &DAT_00410321);
	LAB_00402362:
	  ;
	  puVar5 = &DAT_00410321;
	  uVar3 = 0x41033f;
	}
      else
	{
	  func_0x00401610 ("\n%s online help: <%s>\n", "GNU coreutils",
			   "https://www.gnu.org/software/coreutils/");
	  lVar2 = func_0x00401920 (5, 0);
	  if (lVar2 != 0)
	    {
	      iVar1 = func_0x00401830 (lVar2, &DAT_004103a7, 3);
	      if (iVar1 != 0)
		{
		LAB_00402339:
		  ;
		  func_0x00401610
		    ("Report %s translation bugs to <https://translationproject.org/team/>\n",
		     &DAT_00410321);
		}
	    }
	  func_0x00401610 ("Full documentation at: <%s%s>\n",
			   "https://www.gnu.org/software/coreutils/",
			   &DAT_00410321);
	  uVar3 = 0x4116af;
	  if (puVar5 == &DAT_00410321)
	    goto LAB_00402362;
	}
      func_0x00401610
	("or available locally via: info \'(coreutils) %s%s\'\n", puVar5,
	 uVar3);
    }
  while (true);
}

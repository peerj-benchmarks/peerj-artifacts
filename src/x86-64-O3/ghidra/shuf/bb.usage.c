
#include "shuf.h"

long null_ARRAY_006178c_0_4_;
long null_ARRAY_006178c_40_8_;
long null_ARRAY_006178c_4_4_;
long null_ARRAY_006178c_48_8_;
long null_ARRAY_0061790_0_8_;
long null_ARRAY_0061790_8_8_;
long null_ARRAY_00617b0_0_8_;
long null_ARRAY_00617b0_16_8_;
long null_ARRAY_00617b0_24_8_;
long null_ARRAY_00617b0_32_8_;
long null_ARRAY_00617b0_40_8_;
long null_ARRAY_00617b0_48_8_;
long null_ARRAY_00617b0_8_8_;
long null_ARRAY_00617b4_0_4_;
long null_ARRAY_00617b4_16_8_;
long null_ARRAY_00617b4_24_4_;
long null_ARRAY_00617b4_32_8_;
long null_ARRAY_00617b4_40_4_;
long null_ARRAY_00617b4_4_4_;
long null_ARRAY_00617b4_44_4_;
long null_ARRAY_00617b4_48_4_;
long null_ARRAY_00617b4_8_4_;
long local_10_1_7_;
long local_2f_1_1_;
long local_5b_1_1_;
long local_9c_4_4_;
long local_a1_0_4_;
long local_a1_0_8_;
long local_a1_1_9_;
long local_a1_4_6_;
long local_a1_8_2_;
long local_a3_4_4_;
long local_a4_0_2_;
long local_a4_0_4_;
long local_a4_4_4_;
long local_a5_0_8_;
long local_a5_1_9_;
long local_a5_4_6_;
long local_a5_8_2_;
long local_a6_0_4_;
long DAT_00000008;
long DAT_00000010;
long DAT_00413b40;
long DAT_00413b42;
long DAT_00413bc9;
long DAT_00413c7e;
long DAT_00414350;
long DAT_00414354;
long DAT_00414358;
long DAT_0041435b;
long DAT_0041435d;
long DAT_00414361;
long DAT_00414365;
long DAT_0041490f;
long DAT_00414932;
long DAT_00415168;
long DAT_00415271;
long DAT_00415277;
long DAT_00415289;
long DAT_0041528a;
long DAT_004152a8;
long DAT_004152ac;
long DAT_00415376;
long DAT_00617448;
long DAT_00617458;
long DAT_00617468;
long DAT_006178a8;
long DAT_00617910;
long DAT_00617914;
long DAT_00617918;
long DAT_0061791c;
long DAT_00617940;
long DAT_00617948;
long DAT_00617950;
long DAT_00617960;
long DAT_00617968;
long DAT_00617980;
long DAT_00617988;
long DAT_006179d0;
long DAT_006179d8;
long DAT_006179e0;
long DAT_00617bb8;
long DAT_00617bc0;
long DAT_00617bc8;
long DAT_00617bd0;
long DAT_00617be0;
long _DYNAMIC;
long fde_00415e00;
long int7;
long null_ARRAY_004141c0;
long null_ARRAY_004152c0;
long null_ARRAY_00415600;
long null_ARRAY_00415840;
long null_ARRAY_006178c0;
long null_ARRAY_00617900;
long null_ARRAY_006179a0;
long null_ARRAY_00617a00;
long null_ARRAY_00617b00;
long null_ARRAY_00617b40;
long PTR_DAT_006178a0;
long PTR_null_ARRAY_006178f8;
long PTR_null_ARRAY_00617920;
long register0x00000020;
void
FUN_00402c20 (uint uParm1)
{
  int iVar1;
  long lVar2;
  undefined8 uVar3;
  undefined **ppuVar4;
  undefined *puVar5;
  undefined *local_88;
  char *local_80;
  undefined8 local_78;
  char *local_70;
  char *local_68;
  char *local_60;
  char *local_58;
  char *local_50;
  char *local_48;
  char *local_40;
  char *local_38;
  char *local_30;
  undefined8 local_28;
  undefined8 local_20;

  if (uParm1 == 0)
    goto LAB_00402c4d;
  func_0x00401ba0 (DAT_00617960, "Try \'%s --help\' for more information.\n",
		   DAT_006179e0);
  do
    {
      func_0x00401de0 ((ulong) uParm1);
    LAB_00402c4d:
      ;
      func_0x004019b0
	("Usage: %s [OPTION]... [FILE]\n  or:  %s -e [OPTION]... [ARG]...\n  or:  %s -i LO-HI [OPTION]...\n",
	 DAT_006179e0, DAT_006179e0, DAT_006179e0);
      uVar3 = DAT_00617940;
      func_0x00401bb0
	("Write a random permutation of the input lines to standard output.\n",
	 DAT_00617940);
      func_0x00401bb0
	("\nWith no FILE, or when FILE is -, read standard input.\n", uVar3);
      func_0x00401bb0
	("\nMandatory arguments to long options are mandatory for short options too.\n",
	 uVar3);
      func_0x00401bb0
	("  -e, --echo                treat each ARG as an input line\n  -i, --input-range=LO-HI   treat each number LO through HI as an input line\n  -n, --head-count=COUNT    output at most COUNT lines\n  -o, --output=FILE         write result to FILE instead of standard output\n      --random-source=FILE  get random bytes from FILE\n  -r, --repeat              output lines can be repeated\n",
	 uVar3);
      func_0x00401bb0
	("  -z, --zero-terminated     line delimiter is NUL, not newline\n",
	 uVar3);
      func_0x00401bb0 ("      --help     display this help and exit\n",
		       uVar3);
      func_0x00401bb0
	("      --version  output version information and exit\n", uVar3);
      local_88 = &DAT_00413b40;
      local_80 = "test invocation";
      puVar5 = &DAT_00413b40;
      local_78 = 0x413ba8;
      local_70 = "Multi-call invocation";
      local_68 = "sha224sum";
      local_60 = "sha2 utilities";
      local_58 = "sha256sum";
      local_50 = "sha2 utilities";
      local_48 = "sha384sum";
      local_40 = "sha2 utilities";
      local_38 = "sha512sum";
      local_30 = "sha2 utilities";
      local_28 = 0;
      local_20 = 0;
      ppuVar4 = &local_88;
      do
	{
	  iVar1 = func_0x00401d10 (&DAT_00413b42, puVar5);
	  if (iVar1 == 0)
	    break;
	  ppuVar4 = ppuVar4 + 2;
	  puVar5 = *ppuVar4;
	}
      while (puVar5 != (undefined *) 0x0);
      puVar5 = ppuVar4[1];
      if (puVar5 == (undefined *) 0x0)
	{
	  func_0x004019b0 ("\n%s online help: <%s>\n", "GNU coreutils",
			   "https://www.gnu.org/software/coreutils/");
	  lVar2 = func_0x00401d70 (5, 0);
	  if (lVar2 != 0)
	    {
	      iVar1 = func_0x00401c30 (lVar2, &DAT_00413bc9, 3);
	      if (iVar1 != 0)
		{
		  puVar5 = &DAT_00413b42;
		  goto LAB_00402e19;
		}
	    }
	  func_0x004019b0 ("Full documentation at: <%s%s>\n",
			   "https://www.gnu.org/software/coreutils/",
			   &DAT_00413b42);
	LAB_00402e42:
	  ;
	  puVar5 = &DAT_00413b42;
	  uVar3 = 0x413b61;
	}
      else
	{
	  func_0x004019b0 ("\n%s online help: <%s>\n", "GNU coreutils",
			   "https://www.gnu.org/software/coreutils/");
	  lVar2 = func_0x00401d70 (5, 0);
	  if (lVar2 != 0)
	    {
	      iVar1 = func_0x00401c30 (lVar2, &DAT_00413bc9, 3);
	      if (iVar1 != 0)
		{
		LAB_00402e19:
		  ;
		  func_0x004019b0
		    ("Report %s translation bugs to <https://translationproject.org/team/>\n",
		     &DAT_00413b42);
		}
	    }
	  func_0x004019b0 ("Full documentation at: <%s%s>\n",
			   "https://www.gnu.org/software/coreutils/",
			   &DAT_00413b42);
	  uVar3 = 0x4152a7;
	  if (puVar5 == &DAT_00413b42)
	    goto LAB_00402e42;
	}
      func_0x004019b0
	("or available locally via: info \'(coreutils) %s%s\'\n", puVar5,
	 uVar3);
    }
  while (true);
}

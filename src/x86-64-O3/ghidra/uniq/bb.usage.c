
#include "uniq.h"

long null_ARRAY_006158a_0_4_;
long null_ARRAY_006158a_40_8_;
long null_ARRAY_006158a_4_4_;
long null_ARRAY_006158a_48_8_;
long null_ARRAY_006158e_0_8_;
long null_ARRAY_006158e_8_8_;
long null_ARRAY_00615b0_0_8_;
long null_ARRAY_00615b0_16_8_;
long null_ARRAY_00615b0_24_8_;
long null_ARRAY_00615b0_32_8_;
long null_ARRAY_00615b0_40_8_;
long null_ARRAY_00615b0_48_8_;
long null_ARRAY_00615b0_8_8_;
long null_ARRAY_00615b4_0_4_;
long null_ARRAY_00615b4_16_8_;
long null_ARRAY_00615b4_24_4_;
long null_ARRAY_00615b4_32_8_;
long null_ARRAY_00615b4_40_4_;
long null_ARRAY_00615b4_4_4_;
long null_ARRAY_00615b4_44_4_;
long null_ARRAY_00615b4_48_4_;
long null_ARRAY_00615b4_8_4_;
long local_2f_1_1_;
long local_5b_1_1_;
long local_9c_4_4_;
long local_a1_0_4_;
long local_a1_0_8_;
long local_a1_1_9_;
long local_a1_4_6_;
long local_a1_8_2_;
long local_a3_4_4_;
long local_a4_0_2_;
long local_a4_0_4_;
long local_a4_4_4_;
long local_a5_0_8_;
long local_a5_1_9_;
long local_a5_4_6_;
long local_a5_8_2_;
long local_a6_0_4_;
long DAT_00000008;
long DAT_00000010;
long DAT_00411b87;
long DAT_00411b89;
long DAT_00411c10;
long DAT_00411c88;
long DAT_0041283a;
long DAT_0041283c;
long DAT_00412855;
long DAT_004128b0;
long DAT_004128b4;
long DAT_004128b8;
long DAT_004128bb;
long DAT_004128bd;
long DAT_004128c1;
long DAT_004128c5;
long DAT_00412e6b;
long DAT_0041355e;
long DAT_00413661;
long DAT_00413667;
long DAT_00413669;
long DAT_0041366a;
long DAT_00413688;
long DAT_0041370e;
long DAT_00615468;
long DAT_00615478;
long DAT_00615488;
long DAT_00615890;
long DAT_006158f0;
long DAT_006158f4;
long DAT_006158f8;
long DAT_006158fc;
long DAT_00615900;
long DAT_00615908;
long DAT_00615910;
long DAT_00615920;
long DAT_00615928;
long DAT_00615940;
long DAT_00615948;
long DAT_00615990;
long DAT_00615994;
long DAT_00615998;
long DAT_00615999;
long DAT_0061599a;
long DAT_0061599b;
long DAT_0061599c;
long DAT_006159a0;
long DAT_006159a8;
long DAT_006159b0;
long DAT_006159b8;
long DAT_006159c0;
long DAT_006159c8;
long DAT_006159d0;
long DAT_00615b78;
long DAT_00615b80;
long DAT_00615b88;
long DAT_00615b98;
long fde_00414120;
long int7;
long null_ARRAY_004125c0;
long null_ARRAY_00412760;
long null_ARRAY_00412780;
long null_ARRAY_004127a8;
long null_ARRAY_004127c0;
long null_ARRAY_004139a0;
long null_ARRAY_00413bf0;
long null_ARRAY_006158a0;
long null_ARRAY_006158e0;
long null_ARRAY_00615960;
long null_ARRAY_00615a00;
long null_ARRAY_00615b00;
long null_ARRAY_00615b40;
long PTR_DAT_00615880;
long PTR_FUN_00615888;
long PTR_null_ARRAY_006158d8;
long register0x00000020;
void
FUN_00402af0 (uint uParm1)
{
  int iVar1;
  long lVar2;
  undefined8 uVar3;
  undefined **ppuVar4;
  undefined *puVar5;
  undefined *local_88;
  char *local_80;
  undefined8 local_78;
  char *local_70;
  char *local_68;
  char *local_60;
  char *local_58;
  char *local_50;
  char *local_48;
  char *local_40;
  char *local_38;
  char *local_30;
  undefined8 local_28;
  undefined8 local_20;

  if (uParm1 == 0)
    goto LAB_00402b1d;
  func_0x004018f0 (DAT_00615920, "Try \'%s --help\' for more information.\n",
		   DAT_006159d0);
  do
    {
      func_0x00401ae0 ((ulong) uParm1);
    LAB_00402b1d:
      ;
      func_0x00401740 ("Usage: %s [OPTION]... [INPUT [OUTPUT]]\n",
		       DAT_006159d0);
      uVar3 = DAT_00615900;
      func_0x00401900
	("Filter adjacent matching lines from INPUT (or standard input),\nwriting to OUTPUT (or standard output).\n\nWith no options, matching lines are merged to the first occurrence.\n",
	 DAT_00615900);
      func_0x00401900
	("\nMandatory arguments to long options are mandatory for short options too.\n",
	 uVar3);
      func_0x00401900
	("  -c, --count           prefix lines by the number of occurrences\n  -d, --repeated        only print duplicate lines, one for each group\n",
	 uVar3);
      func_0x00401900
	("  -D                    print all duplicate lines\n      --all-repeated[=METHOD]  like -D, but allow separating groups\n                                 with an empty line;\n                                 METHOD={none(default),prepend,separate}\n",
	 uVar3);
      func_0x00401900
	("  -f, --skip-fields=N   avoid comparing the first N fields\n",
	 uVar3);
      func_0x00401900
	("      --group[=METHOD]  show all items, separating groups with an empty line;\n                          METHOD={separate(default),prepend,append,both}\n",
	 uVar3);
      func_0x00401900
	("  -i, --ignore-case     ignore differences in case when comparing\n  -s, --skip-chars=N    avoid comparing the first N characters\n  -u, --unique          only print unique lines\n",
	 uVar3);
      func_0x00401900
	("  -z, --zero-terminated     line delimiter is NUL, not newline\n",
	 uVar3);
      func_0x00401900
	("  -w, --check-chars=N   compare no more than N characters in lines\n",
	 uVar3);
      func_0x00401900 ("      --help     display this help and exit\n",
		       uVar3);
      func_0x00401900
	("      --version  output version information and exit\n", uVar3);
      func_0x00401900
	("\nA field is a run of blanks (usually spaces and/or TABs), then non-blank\ncharacters.  Fields are skipped before chars.\n",
	 uVar3);
      func_0x00401900
	("\nNote: \'uniq\' does not detect repeated lines unless they are adjacent.\nYou may want to sort the input first, or use \'sort -u\' without \'uniq\'.\nAlso, comparisons honor the rules specified by \'LC_COLLATE\'.\n",
	 uVar3);
      local_88 = &DAT_00411b87;
      local_80 = "test invocation";
      puVar5 = &DAT_00411b87;
      local_78 = 0x411bef;
      local_70 = "Multi-call invocation";
      local_68 = "sha224sum";
      local_60 = "sha2 utilities";
      local_58 = "sha256sum";
      local_50 = "sha2 utilities";
      local_48 = "sha384sum";
      local_40 = "sha2 utilities";
      local_38 = "sha512sum";
      local_30 = "sha2 utilities";
      local_28 = 0;
      local_20 = 0;
      ppuVar4 = &local_88;
      do
	{
	  iVar1 = func_0x00401a00 (&DAT_00411b89, puVar5);
	  if (iVar1 == 0)
	    break;
	  ppuVar4 = ppuVar4 + 2;
	  puVar5 = *ppuVar4;
	}
      while (puVar5 != (undefined *) 0x0);
      puVar5 = ppuVar4[1];
      if (puVar5 == (undefined *) 0x0)
	{
	  func_0x00401740 ("\n%s online help: <%s>\n", "GNU coreutils",
			   "https://www.gnu.org/software/coreutils/");
	  lVar2 = func_0x00401a60 (5, 0);
	  if (lVar2 != 0)
	    {
	      iVar1 = func_0x00401970 (lVar2, &DAT_00411c10, 3);
	      if (iVar1 != 0)
		{
		  puVar5 = &DAT_00411b89;
		  goto LAB_00402d39;
		}
	    }
	  func_0x00401740 ("Full documentation at: <%s%s>\n",
			   "https://www.gnu.org/software/coreutils/",
			   &DAT_00411b89);
	LAB_00402d62:
	  ;
	  puVar5 = &DAT_00411b89;
	  uVar3 = 0x411ba8;
	}
      else
	{
	  func_0x00401740 ("\n%s online help: <%s>\n", "GNU coreutils",
			   "https://www.gnu.org/software/coreutils/");
	  lVar2 = func_0x00401a60 (5, 0);
	  if (lVar2 != 0)
	    {
	      iVar1 = func_0x00401970 (lVar2, &DAT_00411c10, 3);
	      if (iVar1 != 0)
		{
		LAB_00402d39:
		  ;
		  func_0x00401740
		    ("Report %s translation bugs to <https://translationproject.org/team/>\n",
		     &DAT_00411b89);
		}
	    }
	  func_0x00401740 ("Full documentation at: <%s%s>\n",
			   "https://www.gnu.org/software/coreutils/",
			   &DAT_00411b89);
	  uVar3 = 0x413687;
	  if (puVar5 == &DAT_00411b89)
	    goto LAB_00402d62;
	}
      func_0x00401740
	("or available locally via: info \'(coreutils) %s%s\'\n", puVar5,
	 uVar3);
    }
  while (true);
}

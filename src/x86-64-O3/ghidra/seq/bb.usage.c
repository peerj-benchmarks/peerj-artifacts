
#include "seq.h"

long null_ARRAY_00613c0_0_4_;
long null_ARRAY_00613c0_40_8_;
long null_ARRAY_00613c0_4_4_;
long null_ARRAY_00613c0_48_8_;
long null_ARRAY_00613c4_0_8_;
long null_ARRAY_00613c4_8_8_;
long null_ARRAY_00613e8_0_8_;
long null_ARRAY_00613e8_16_8_;
long null_ARRAY_00613e8_24_8_;
long null_ARRAY_00613e8_32_8_;
long null_ARRAY_00613e8_40_8_;
long null_ARRAY_00613e8_48_8_;
long null_ARRAY_00613e8_8_8_;
long null_ARRAY_00613ec_0_4_;
long null_ARRAY_00613ec_16_8_;
long null_ARRAY_00613ec_24_4_;
long null_ARRAY_00613ec_32_8_;
long null_ARRAY_00613ec_40_4_;
long null_ARRAY_00613ec_4_4_;
long null_ARRAY_00613ec_44_4_;
long null_ARRAY_00613ec_48_4_;
long null_ARRAY_00613ec_8_4_;
long local_2f_1_1_;
long local_5b_1_1_;
long local_9c_4_4_;
long local_a1_0_4_;
long local_a1_0_8_;
long local_a1_1_9_;
long local_a1_4_6_;
long local_a1_8_2_;
long local_a3_4_4_;
long local_a4_0_2_;
long local_a4_0_4_;
long local_a4_4_4_;
long local_a5_0_8_;
long local_a5_1_9_;
long local_a5_4_6_;
long local_a5_8_2_;
long local_a6_0_4_;
long local_f_0_8_;
long local_f_4_6_;
long local_f_8_2_;
long DAT_00000008;
long DAT_00000010;
long DAT_00410917;
long DAT_0041091b;
long DAT_0041091d;
long DAT_004109a3;
long DAT_004109cc;
long DAT_004109cf;
long DAT_004109d2;
long DAT_004109d4;
long DAT_00410a2d;
long DAT_00410a32;
long DAT_00410a6b;
long DAT_00411240;
long DAT_00411298;
long DAT_0041129c;
long DAT_004112a0;
long DAT_004112a3;
long DAT_004112a5;
long DAT_004112a9;
long DAT_004112ad;
long DAT_0041182b;
long DAT_00411cb5;
long DAT_00411dd1;
long DAT_00411dd2;
long DAT_00411df0;
long DAT_00411e76;
long DAT_006137e0;
long DAT_006137f0;
long DAT_00613800;
long DAT_00613be8;
long DAT_00613c50;
long DAT_00613c54;
long DAT_00613c58;
long DAT_00613c5c;
long DAT_00613c80;
long DAT_00613c90;
long DAT_00613ca0;
long DAT_00613ca8;
long DAT_00613cc0;
long DAT_00613cc8;
long DAT_00613d30;
long DAT_00613d38;
long DAT_00613d40;
long DAT_00613d48;
long DAT_00613d50;
long DAT_00613d58;
long DAT_00613ef8;
long DAT_00613f00;
long DAT_00613f08;
long DAT_00613f18;
long _DYNAMIC;
long fde_00412830;
long int7;
long null_ARRAY_00412100;
long null_ARRAY_00412350;
long null_ARRAY_00613c00;
long null_ARRAY_00613c40;
long null_ARRAY_00613ce0;
long null_ARRAY_00613d10;
long null_ARRAY_00613d80;
long null_ARRAY_00613e80;
long null_ARRAY_00613ec0;
long PTR_DAT_00613be0;
long PTR_null_ARRAY_00613c38;
long register0x00000020;
void
FUN_00402b20 (uint uParm1)
{
  int iVar1;
  long lVar2;
  undefined8 uVar3;
  undefined **ppuVar4;
  undefined *puVar5;
  undefined *local_88;
  char *local_80;
  undefined8 local_78;
  char *local_70;
  char *local_68;
  char *local_60;
  char *local_58;
  char *local_50;
  char *local_48;
  char *local_40;
  char *local_38;
  char *local_30;
  undefined8 local_28;
  undefined8 local_20;

  if (uParm1 == 0)
    goto LAB_00402b4d;
  func_0x00401810 (DAT_00613ca0, "Try \'%s --help\' for more information.\n",
		   DAT_00613d58);
  do
    {
      func_0x004019b0 ((ulong) uParm1);
    LAB_00402b4d:
      ;
      func_0x00401640
	("Usage: %s [OPTION]... LAST\n  or:  %s [OPTION]... FIRST LAST\n  or:  %s [OPTION]... FIRST INCREMENT LAST\n",
	 DAT_00613d58, DAT_00613d58, DAT_00613d58);
      uVar3 = DAT_00613c80;
      func_0x00401820
	("Print numbers from FIRST to LAST, in steps of INCREMENT.\n",
	 DAT_00613c80);
      func_0x00401820
	("\nMandatory arguments to long options are mandatory for short options too.\n",
	 uVar3);
      func_0x00401820
	("  -f, --format=FORMAT      use printf style floating-point FORMAT\n  -s, --separator=STRING   use STRING to separate numbers (default: \\n)\n  -w, --equal-width        equalize width by padding with leading zeroes\n",
	 uVar3);
      func_0x00401820 ("      --help     display this help and exit\n",
		       uVar3);
      func_0x00401820
	("      --version  output version information and exit\n", uVar3);
      func_0x00401820
	("\nIf FIRST or INCREMENT is omitted, it defaults to 1.  That is, an\nomitted INCREMENT defaults to 1 even when LAST is smaller than FIRST.\nThe sequence of numbers ends when the sum of the current number and\nINCREMENT would become greater than LAST.\nFIRST, INCREMENT, and LAST are interpreted as floating point values.\nINCREMENT is usually positive if FIRST is smaller than LAST, and\nINCREMENT is usually negative if FIRST is greater than LAST.\nINCREMENT must not be 0; none of FIRST, INCREMENT and LAST may be NaN.\n",
	 uVar3);
      func_0x00401820
	("FORMAT must be suitable for printing one argument of type \'double\';\nit defaults to %.PRECf if FIRST, INCREMENT, and LAST are all fixed point\ndecimal numbers with maximum precision PREC, and to %g otherwise.\n",
	 uVar3);
      local_88 = &DAT_0041091b;
      local_80 = "test invocation";
      puVar5 = &DAT_0041091b;
      local_78 = 0x410982;
      local_70 = "Multi-call invocation";
      local_68 = "sha224sum";
      local_60 = "sha2 utilities";
      local_58 = "sha256sum";
      local_50 = "sha2 utilities";
      local_48 = "sha384sum";
      local_40 = "sha2 utilities";
      local_38 = "sha512sum";
      local_30 = "sha2 utilities";
      local_28 = 0;
      local_20 = 0;
      ppuVar4 = &local_88;
      do
	{
	  iVar1 = func_0x004018f0 (&DAT_0041091d, puVar5);
	  if (iVar1 == 0)
	    break;
	  ppuVar4 = ppuVar4 + 2;
	  puVar5 = *ppuVar4;
	}
      while (puVar5 != (undefined *) 0x0);
      puVar5 = ppuVar4[1];
      if (puVar5 == (undefined *) 0x0)
	{
	  func_0x00401640 ("\n%s online help: <%s>\n", "GNU coreutils",
			   "https://www.gnu.org/software/coreutils/");
	  lVar2 = func_0x00401960 (5, 0);
	  if (lVar2 != 0)
	    {
	      iVar1 = func_0x00401870 (lVar2, &DAT_004109a3, 3);
	      if (iVar1 != 0)
		{
		  puVar5 = &DAT_0041091d;
		  goto LAB_00402d19;
		}
	    }
	  func_0x00401640 ("Full documentation at: <%s%s>\n",
			   "https://www.gnu.org/software/coreutils/",
			   &DAT_0041091d);
	LAB_00402d42:
	  ;
	  puVar5 = &DAT_0041091d;
	  uVar3 = 0x41093b;
	}
      else
	{
	  func_0x00401640 ("\n%s online help: <%s>\n", "GNU coreutils",
			   "https://www.gnu.org/software/coreutils/");
	  lVar2 = func_0x00401960 (5, 0);
	  if (lVar2 != 0)
	    {
	      iVar1 = func_0x00401870 (lVar2, &DAT_004109a3, 3);
	      if (iVar1 != 0)
		{
		LAB_00402d19:
		  ;
		  func_0x00401640
		    ("Report %s translation bugs to <https://translationproject.org/team/>\n",
		     &DAT_0041091d);
		}
	    }
	  func_0x00401640 ("Full documentation at: <%s%s>\n",
			   "https://www.gnu.org/software/coreutils/",
			   &DAT_0041091d);
	  uVar3 = 0x411def;
	  if (puVar5 == &DAT_0041091d)
	    goto LAB_00402d42;
	}
      func_0x00401640
	("or available locally via: info \'(coreutils) %s%s\'\n", puVar5,
	 uVar3);
    }
  while (true);
}

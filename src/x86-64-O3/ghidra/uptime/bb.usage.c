
#include "uptime.h"

long null_ARRAY_0061744_0_4_;
long null_ARRAY_0061744_40_8_;
long null_ARRAY_0061744_4_4_;
long null_ARRAY_0061744_48_8_;
long null_ARRAY_0061748_0_8_;
long null_ARRAY_0061748_8_8_;
long null_ARRAY_0061768_0_8_;
long null_ARRAY_0061768_16_8_;
long null_ARRAY_0061768_24_8_;
long null_ARRAY_0061768_32_8_;
long null_ARRAY_0061768_40_8_;
long null_ARRAY_0061768_48_8_;
long null_ARRAY_0061768_8_8_;
long null_ARRAY_006176c_0_4_;
long null_ARRAY_006176c_16_8_;
long null_ARRAY_006176c_24_4_;
long null_ARRAY_006176c_32_8_;
long null_ARRAY_006176c_40_4_;
long null_ARRAY_006176c_4_4_;
long null_ARRAY_006176c_44_4_;
long null_ARRAY_006176c_48_4_;
long null_ARRAY_006176c_8_4_;
long local_12_0_4_;
long local_2f_1_1_;
long local_5b_1_1_;
long local_7_4_4_;
long local_9c_4_4_;
long local_a1_0_4_;
long local_a1_0_8_;
long local_a1_1_9_;
long local_a1_4_6_;
long local_a1_8_2_;
long local_a3_4_4_;
long local_a4_0_2_;
long local_a4_0_4_;
long local_a4_4_4_;
long local_a5_0_8_;
long local_a5_1_9_;
long local_a5_4_6_;
long local_a5_8_2_;
long local_a6_0_4_;
long DAT_00000008;
long DAT_00000010;
long DAT_004137f3;
long DAT_004138ba;
long DAT_004143d0;
long DAT_004144a0;
long DAT_004144a4;
long DAT_004144a8;
long DAT_004144ab;
long DAT_004144ad;
long DAT_004144b1;
long DAT_004144b5;
long DAT_00414a6b;
long DAT_00414ef5;
long DAT_00414ff9;
long DAT_00414fff;
long DAT_00415011;
long DAT_00415012;
long DAT_00415030;
long DAT_00415034;
long DAT_00415037;
long DAT_004150fa;
long DAT_00617000;
long DAT_00617010;
long DAT_00617020;
long DAT_00617428;
long DAT_00617490;
long DAT_00617494;
long DAT_00617498;
long DAT_0061749c;
long DAT_006174c0;
long DAT_006174d0;
long DAT_006174e0;
long DAT_006174e8;
long DAT_00617500;
long DAT_00617508;
long DAT_00617550;
long DAT_00617558;
long DAT_00617560;
long DAT_00617738;
long DAT_00617740;
long DAT_00617748;
long DAT_00617f88;
long DAT_00617f90;
long DAT_00617fa0;
long fde_00415b18;
long int7;
long LAB_004018f0;
long null_ARRAY_00413bc0;
long null_ARRAY_00414400;
long null_ARRAY_004150c0;
long null_ARRAY_004153a0;
long null_ARRAY_004155f0;
long null_ARRAY_00617440;
long null_ARRAY_00617480;
long null_ARRAY_00617520;
long null_ARRAY_00617580;
long null_ARRAY_00617680;
long null_ARRAY_006176c0;
long null_ARRAY_00617700;
long null_ARRAY_00617780;
long PTR_DAT_00617420;
long PTR_null_ARRAY_00617478;
long PTR_null_ARRAY_006174a0;
long register0x00000020;
long stack0x00000008;
void
FUN_004021b0 (uint uParm1)
{
  int iVar1;
  long lVar2;
  undefined8 uVar3;
  undefined **ppuVar4;
  char *pcVar5;
  undefined *puVar6;
  undefined *local_88;
  char *local_80;
  undefined8 local_78;
  char *local_70;
  char *local_68;
  char *local_60;
  char *local_58;
  char *local_50;
  char *local_48;
  char *local_40;
  char *local_38;
  char *local_30;
  undefined8 local_28;
  undefined8 local_20;

  if (uParm1 == 0)
    goto LAB_004021dd;
  func_0x00401950 (DAT_006174e0, "Try \'%s --help\' for more information.\n",
		   DAT_00617560);
  do
    {
      func_0x00401b60 ((ulong) uParm1);
    LAB_004021dd:
      ;
      func_0x004017c0 ("Usage: %s [OPTION]... [FILE]\n", DAT_00617560);
      func_0x004017c0
	("Print the current time, the length of time the system has been up,\nthe number of users on the system, and the average number of jobs\nin the run queue over the last 1, 5 and 15 minutes.");
      func_0x004017c0
	("  Processes in\nan uninterruptible sleep state also contribute to the load average.\n");
      func_0x004017c0
	("If FILE is not specified, use %s.  %s as FILE is common.\n\n",
	 "/dev/null/utmp", "/dev/null/wtmp");
      uVar3 = DAT_006174c0;
      func_0x00401970 ("      --help     display this help and exit\n",
		       DAT_006174c0);
      func_0x00401970
	("      --version  output version information and exit\n", uVar3);
      local_88 = &DAT_004137f3;
      local_80 = "test invocation";
      puVar6 = &DAT_004137f3;
      local_78 = 0x413899;
      local_70 = "Multi-call invocation";
      local_68 = "sha224sum";
      local_60 = "sha2 utilities";
      local_58 = "sha256sum";
      local_50 = "sha2 utilities";
      local_48 = "sha384sum";
      local_40 = "sha2 utilities";
      local_38 = "sha512sum";
      local_30 = "sha2 utilities";
      local_28 = 0;
      local_20 = 0;
      ppuVar4 = &local_88;
      do
	{
	  iVar1 = func_0x00401a80 ("uptime", puVar6);
	  if (iVar1 == 0)
	    break;
	  ppuVar4 = ppuVar4 + 2;
	  puVar6 = *ppuVar4;
	}
      while (puVar6 != (undefined *) 0x0);
      pcVar5 = ppuVar4[1];
      if (pcVar5 == (char *) 0x0)
	{
	  func_0x004017c0 ("\n%s online help: <%s>\n", "GNU coreutils",
			   "https://www.gnu.org/software/coreutils/");
	  lVar2 = func_0x00401ae0 (5, 0);
	  if (lVar2 != 0)
	    {
	      iVar1 = func_0x004019c0 (lVar2, &DAT_004138ba, 3);
	      if (iVar1 != 0)
		{
		  pcVar5 = "uptime";
		  goto LAB_00402399;
		}
	    }
	  func_0x004017c0 ("Full documentation at: <%s%s>\n",
			   "https://www.gnu.org/software/coreutils/",
			   "uptime");
	LAB_004023c2:
	  ;
	  pcVar5 = "uptime";
	  uVar3 = 0x413852;
	}
      else
	{
	  func_0x004017c0 ("\n%s online help: <%s>\n", "GNU coreutils",
			   "https://www.gnu.org/software/coreutils/");
	  lVar2 = func_0x00401ae0 (5, 0);
	  if (lVar2 != 0)
	    {
	      iVar1 = func_0x004019c0 (lVar2, &DAT_004138ba, 3);
	      if (iVar1 != 0)
		{
		LAB_00402399:
		  ;
		  func_0x004017c0
		    ("Report %s translation bugs to <https://translationproject.org/team/>\n",
		     "uptime");
		}
	    }
	  func_0x004017c0 ("Full documentation at: <%s%s>\n",
			   "https://www.gnu.org/software/coreutils/",
			   "uptime");
	  uVar3 = 0x41502f;
	  if (pcVar5 == "uptime")
	    goto LAB_004023c2;
	}
      func_0x004017c0
	("or available locally via: info \'(coreutils) %s%s\'\n", pcVar5,
	 uVar3);
    }
  while (true);
}

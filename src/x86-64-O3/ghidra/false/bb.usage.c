
#include "false.h"

long null_ARRAY_0061040_0_8_;
long null_ARRAY_0061040_8_8_;
long null_ARRAY_0061060_0_8_;
long null_ARRAY_0061060_16_8_;
long null_ARRAY_0061060_24_8_;
long null_ARRAY_0061060_32_8_;
long null_ARRAY_0061060_40_8_;
long null_ARRAY_0061060_48_8_;
long null_ARRAY_0061060_8_8_;
long local_2f_1_1_;
long local_5b_1_1_;
long local_9c_4_4_;
long local_a1_0_4_;
long local_a1_0_8_;
long local_a1_1_9_;
long local_a1_4_6_;
long local_a1_8_2_;
long local_a3_4_4_;
long local_a4_0_2_;
long local_a4_0_4_;
long local_a4_4_4_;
long local_a5_0_8_;
long local_a5_1_9_;
long local_a5_4_6_;
long local_a5_8_2_;
long local_a6_0_4_;
long DAT_0040dd00;
long DAT_0040dd08;
long DAT_0040dd0c;
long DAT_0040dd8f;
long DAT_0040e070;
long DAT_0040e074;
long DAT_0040e076;
long DAT_0040e07a;
long DAT_0040e07d;
long DAT_0040e07f;
long DAT_0040e083;
long DAT_0040e087;
long DAT_0040e62b;
long DAT_0040eab5;
long DAT_0040eac6;
long DAT_0040eb4e;
long DAT_00610000;
long DAT_00610010;
long DAT_00610020;
long DAT_006103a8;
long DAT_00610410;
long DAT_00610440;
long DAT_00610450;
long DAT_00610460;
long DAT_00610468;
long DAT_00610480;
long DAT_00610488;
long DAT_006104d0;
long DAT_006104d8;
long DAT_006104e0;
long DAT_00610638;
long DAT_00610640;
long DAT_00610648;
long _DYNAMIC;
long fde_0040f498;
long null_ARRAY_0040ede0;
long null_ARRAY_0040f030;
long null_ARRAY_00610400;
long null_ARRAY_006104a0;
long null_ARRAY_00610500;
long null_ARRAY_00610600;
long PTR_DAT_006103a0;
long PTR_null_ARRAY_006103f8;
long register0x00000020;
long stack0x00000008;
void
FUN_00401950 (uint uParm1)
{
  undefined8 uVar1;
  int iVar2;
  long lVar3;
  undefined *puVar4;
  undefined **ppuVar5;
  char *pcVar6;
  undefined *local_88;
  char *local_80;
  undefined8 local_78;
  char *local_70;
  char *local_68;
  char *local_60;
  char *local_58;
  char *local_50;
  char *local_48;
  char *local_40;
  char *local_38;
  char *local_30;
  undefined8 local_28;
  undefined8 local_20;

  ppuVar5 = &local_88;
  func_0x00401360
    ("Usage: %s [ignored command line arguments]\n  or:  %s OPTION\n",
     DAT_006104e0, DAT_006104e0);
  func_0x00401360 (&DAT_0040dd08,
		   "Exit with a status code indicating failure.");
  uVar1 = DAT_00610440;
  func_0x004014e0 ("      --help     display this help and exit\n",
		   DAT_00610440);
  func_0x004014e0 ("      --version  output version information and exit\n",
		   uVar1);
  func_0x00401360
    ("\nNOTE: your shell may have its own version of %s, which usually supersedes\nthe version described here.  Please refer to your shell\'s documentation\nfor details about the options it supports.\n",
     "false");
  local_88 = &DAT_0040dd00;
  local_80 = "test invocation";
  puVar4 = &DAT_0040dd00;
  local_78 = 0x40dd6e;
  local_70 = "Multi-call invocation";
  local_68 = "sha224sum";
  local_60 = "sha2 utilities";
  local_58 = "sha256sum";
  local_50 = "sha2 utilities";
  local_48 = "sha384sum";
  local_40 = "sha2 utilities";
  local_38 = "sha512sum";
  local_30 = "sha2 utilities";
  local_28 = 0;
  local_20 = 0;
  do
    {
      iVar2 = func_0x004015a0 ("false", puVar4);
      if (iVar2 == 0)
	break;
      ppuVar5 = ppuVar5 + 2;
      puVar4 = *ppuVar5;
    }
  while (puVar4 != (undefined *) 0x0);
  pcVar6 = ppuVar5[1];
  if (pcVar6 != (char *) 0x0)
    {
      func_0x00401360 ("\n%s online help: <%s>\n", "GNU coreutils",
		       "https://www.gnu.org/software/coreutils/");
      lVar3 = func_0x004015f0 (5, 0);
      if (lVar3 != 0)
	{
	  iVar2 = func_0x00401530 (lVar3, &DAT_0040dd8f, 3);
	  if (iVar2 != 0)
	    goto LAB_00401adf;
	}
      do
	{
	  func_0x00401360 ("Full documentation at: <%s%s>\n",
			   "https://www.gnu.org/software/coreutils/",
			   "false");
	  puVar4 = &DAT_0040dd0c;
	  if (pcVar6 == "false")
	    {
	    LAB_00401b34:
	      ;
	      pcVar6 = "false";
	      puVar4 = (undefined *) 0x40dd27;
	    }
	  func_0x00401360
	    ("or available locally via: info \'(coreutils) %s%s\'\n", pcVar6,
	     puVar4);
	  func_0x00401630 ((ulong) uParm1);
	LAB_00401ada:
	  ;
	  pcVar6 = "false";
	LAB_00401adf:
	  ;
	  func_0x00401360
	    ("Report %s translation bugs to <https://translationproject.org/team/>\n",
	     "false");
	}
      while (true);
    }
  func_0x00401360 ("\n%s online help: <%s>\n", "GNU coreutils",
		   "https://www.gnu.org/software/coreutils/");
  lVar3 = func_0x004015f0 (5, 0);
  if (lVar3 != 0)
    {
      iVar2 = func_0x00401530 (lVar3, &DAT_0040dd8f, 3);
      if (iVar2 != 0)
	goto LAB_00401ada;
    }
  func_0x00401360 ("Full documentation at: <%s%s>\n",
		   "https://www.gnu.org/software/coreutils/", "false");
  goto LAB_00401b34;
}

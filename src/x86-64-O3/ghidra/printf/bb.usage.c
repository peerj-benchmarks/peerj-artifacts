
#include "printf.h"

long null_ARRAY_0061342_0_4_;
long null_ARRAY_0061342_40_8_;
long null_ARRAY_0061342_4_4_;
long null_ARRAY_0061342_48_8_;
long null_ARRAY_0061346_0_8_;
long null_ARRAY_0061346_8_8_;
long null_ARRAY_0061364_0_8_;
long null_ARRAY_0061364_16_8_;
long null_ARRAY_0061364_24_8_;
long null_ARRAY_0061364_32_8_;
long null_ARRAY_0061364_40_8_;
long null_ARRAY_0061364_48_8_;
long null_ARRAY_0061364_8_8_;
long local_16_0_1_;
long local_2f_1_1_;
long local_5b_1_1_;
long local_9c_4_4_;
long local_a1_0_4_;
long local_a1_0_8_;
long local_a1_1_9_;
long local_a1_4_6_;
long local_a1_8_2_;
long local_a3_4_4_;
long local_a4_0_2_;
long local_a4_0_4_;
long local_a4_4_4_;
long local_a5_0_8_;
long local_a5_1_9_;
long local_a5_4_6_;
long local_a5_8_2_;
long local_a6_0_4_;
long lVar8;
long var8;
long DAT_00410149;
long DAT_00410179;
long DAT_00410204;
long DAT_00410249;
long DAT_0041054d;
long DAT_004105a8;
long DAT_004105ac;
long DAT_004105ae;
long DAT_004105b2;
long DAT_004105b5;
long DAT_004105b7;
long DAT_004105bb;
long DAT_004105bf;
long DAT_00410c1c;
long DAT_004110d5;
long DAT_00411166;
long DAT_00613000;
long DAT_00613010;
long DAT_00613020;
long DAT_00613408;
long DAT_00613470;
long DAT_00613480;
long DAT_00613490;
long DAT_006134a0;
long DAT_006134a8;
long DAT_006134c0;
long DAT_006134c8;
long DAT_00613510;
long DAT_00613514;
long DAT_00613518;
long DAT_00613520;
long DAT_00613528;
long DAT_00613530;
long DAT_00613678;
long DAT_00613680;
long DAT_00613684;
long DAT_00613688;
long DAT_00613690;
long DAT_00613698;
long fde_00411b18;
long null_ARRAY_00411400;
long null_ARRAY_00411650;
long null_ARRAY_00613420;
long null_ARRAY_00613460;
long null_ARRAY_006134e0;
long null_ARRAY_00613540;
long null_ARRAY_00613640;
long PTR_DAT_00613400;
long PTR_null_ARRAY_00613458;
long stack0x00000008;
void
FUN_00402cf0 (uint uParm1)
{
  int iVar1;
  long lVar2;
  undefined8 uVar3;
  undefined **ppuVar4;
  char *pcVar5;
  undefined *puVar6;
  undefined *local_88;
  char *local_80;
  undefined8 local_78;
  char *local_70;
  char *local_68;
  char *local_60;
  char *local_58;
  char *local_50;
  char *local_48;
  char *local_40;
  char *local_38;
  char *local_30;
  undefined8 local_28;
  undefined8 local_20;

  if (uParm1 == 0)
    goto LAB_00402d1d;
  func_0x004017e0 (DAT_006134a0, "Try \'%s --help\' for more information.\n",
		   DAT_00613530);
  do
    {
      func_0x00401970 ((ulong) uParm1);
    LAB_00402d1d:
      ;
      func_0x00401600 ("Usage: %s FORMAT [ARGUMENT]...\n  or:  %s OPTION\n",
		       DAT_00613530, DAT_00613530);
      uVar3 = DAT_00613480;
      func_0x004017f0
	("Print ARGUMENT(s) according to FORMAT, or execute according to OPTION:\n\n",
	 DAT_00613480);
      func_0x004017f0 ("      --help     display this help and exit\n",
		       uVar3);
      func_0x004017f0
	("      --version  output version information and exit\n", uVar3);
      func_0x004017f0
	("\nFORMAT controls the output as in C printf.  Interpreted sequences are:\n\n  \\\"      double quote\n",
	 uVar3);
      func_0x004017f0
	("  \\\\      backslash\n  \\a      alert (BEL)\n  \\b      backspace\n  \\c      produce no further output\n  \\e      escape\n  \\f      form feed\n  \\n      new line\n  \\r      carriage return\n  \\t      horizontal tab\n  \\v      vertical tab\n",
	 uVar3);
      func_0x004017f0
	("  \\NNN    byte with octal value NNN (1 to 3 digits)\n  \\xHH    byte with hexadecimal value HH (1 to 2 digits)\n  \\uHHHH  Unicode (ISO/IEC 10646) character with hex value HHHH (4 digits)\n  \\UHHHHHHHH  Unicode character with hex value HHHHHHHH (8 digits)\n",
	 uVar3);
      func_0x004017f0
	("  %%      a single %\n  %b      ARGUMENT as a string with \'\\\' escapes interpreted,\n          except that octal escapes are of the form \\0 or \\0NNN\n  %q      ARGUMENT is printed in a format that can be reused as shell input,\n          escaping non-printable characters with the proposed POSIX $\'\' syntax.\n\nand all C format specifications ending with one of diouxXfeEgGcs, with\nARGUMENTs converted to proper type first.  Variable widths are handled.\n",
	 uVar3);
      func_0x00401600
	("\nNOTE: your shell may have its own version of %s, which usually supersedes\nthe version described here.  Please refer to your shell\'s documentation\nfor details about the options it supports.\n",
	 "printf");
      local_88 = &DAT_00410179;
      local_80 = "test invocation";
      puVar6 = &DAT_00410179;
      local_78 = 0x4101e3;
      local_70 = "Multi-call invocation";
      local_68 = "sha224sum";
      local_60 = "sha2 utilities";
      local_58 = "sha256sum";
      local_50 = "sha2 utilities";
      local_48 = "sha384sum";
      local_40 = "sha2 utilities";
      local_38 = "sha512sum";
      local_30 = "sha2 utilities";
      local_28 = 0;
      local_20 = 0;
      ppuVar4 = &local_88;
      do
	{
	  iVar1 = func_0x004018c0 ("printf", puVar6);
	  if (iVar1 == 0)
	    break;
	  ppuVar4 = ppuVar4 + 2;
	  puVar6 = *ppuVar4;
	}
      while (puVar6 != (undefined *) 0x0);
      pcVar5 = ppuVar4[1];
      if (pcVar5 == (char *) 0x0)
	{
	  func_0x00401600 ("\n%s online help: <%s>\n", "GNU coreutils",
			   "https://www.gnu.org/software/coreutils/");
	  lVar2 = func_0x00401910 (5, 0);
	  if (lVar2 != 0)
	    {
	      iVar1 = func_0x00401850 (lVar2, &DAT_00410204, 3);
	      if (iVar1 != 0)
		{
		  pcVar5 = "printf";
		  goto LAB_00402ef9;
		}
	    }
	  func_0x00401600 ("Full documentation at: <%s%s>\n",
			   "https://www.gnu.org/software/coreutils/",
			   "printf");
	LAB_00402f22:
	  ;
	  pcVar5 = "printf";
	  uVar3 = 0x41019c;
	}
      else
	{
	  func_0x00401600 ("\n%s online help: <%s>\n", "GNU coreutils",
			   "https://www.gnu.org/software/coreutils/");
	  lVar2 = func_0x00401910 (5, 0);
	  if (lVar2 != 0)
	    {
	      iVar1 = func_0x00401850 (lVar2, &DAT_00410204, 3);
	      if (iVar1 != 0)
		{
		LAB_00402ef9:
		  ;
		  func_0x00401600
		    ("Report %s translation bugs to <https://translationproject.org/team/>\n",
		     "printf");
		}
	    }
	  func_0x00401600 ("Full documentation at: <%s%s>\n",
			   "https://www.gnu.org/software/coreutils/",
			   "printf");
	  uVar3 = 0x410c62;
	  if (pcVar5 == "printf")
	    goto LAB_00402f22;
	}
      func_0x00401600
	("or available locally via: info \'(coreutils) %s%s\'\n", pcVar5,
	 uVar3);
    }
  while (true);
}

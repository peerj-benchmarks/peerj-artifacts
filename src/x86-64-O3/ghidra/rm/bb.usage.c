
#include "rm.h"

long null_ARRAY_0061a48_0_4_;
long null_ARRAY_0061a48_40_8_;
long null_ARRAY_0061a48_4_4_;
long null_ARRAY_0061a48_48_8_;
long null_ARRAY_0061a4c_0_8_;
long null_ARRAY_0061a4c_8_8_;
long null_ARRAY_0061a6c_0_8_;
long null_ARRAY_0061a6c_16_8_;
long null_ARRAY_0061a6c_24_8_;
long null_ARRAY_0061a6c_32_8_;
long null_ARRAY_0061a6c_40_8_;
long null_ARRAY_0061a6c_48_8_;
long null_ARRAY_0061a6c_8_8_;
long null_ARRAY_0061a70_0_4_;
long null_ARRAY_0061a70_16_8_;
long null_ARRAY_0061a70_24_4_;
long null_ARRAY_0061a70_32_8_;
long null_ARRAY_0061a70_40_4_;
long null_ARRAY_0061a70_4_4_;
long null_ARRAY_0061a70_44_4_;
long null_ARRAY_0061a70_48_4_;
long null_ARRAY_0061a70_8_4_;
long local_2f_1_1_;
long local_5b_1_1_;
long local_9c_4_4_;
long local_a1_0_4_;
long local_a1_0_8_;
long local_a1_1_9_;
long local_a1_4_6_;
long local_a1_8_2_;
long local_a3_4_4_;
long local_a4_0_2_;
long local_a4_0_4_;
long local_a4_4_4_;
long local_a5_0_8_;
long local_a5_1_9_;
long local_a5_4_6_;
long local_a5_8_2_;
long local_a6_0_4_;
long DAT_00000008;
long DAT_00000010;
long DAT_00415a40;
long DAT_00415a42;
long DAT_00415ac7;
long DAT_00416480;
long DAT_004166d6;
long DAT_004166d7;
long DAT_00416a85;
long DAT_00416a87;
long DAT_00416afe;
long DAT_00416b68;
long DAT_00416b6e;
long DAT_00416b70;
long DAT_00416b74;
long DAT_00416b78;
long DAT_00416b7b;
long DAT_00416b7d;
long DAT_00416b81;
long DAT_00416b85;
long DAT_0041712b;
long DAT_004175ba;
long DAT_00417701;
long DAT_00417707;
long DAT_00417719;
long DAT_0041771a;
long DAT_00417738;
long DAT_00417772;
long DAT_00417836;
long DAT_0061a000;
long DAT_0061a010;
long DAT_0061a020;
long DAT_0061a470;
long DAT_0061a4d0;
long DAT_0061a4d4;
long DAT_0061a4d8;
long DAT_0061a4dc;
long DAT_0061a500;
long DAT_0061a508;
long DAT_0061a510;
long DAT_0061a520;
long DAT_0061a528;
long DAT_0061a540;
long DAT_0061a548;
long DAT_0061a5a0;
long DAT_0061a5a8;
long DAT_0061a5b0;
long DAT_0061a5b8;
long DAT_0061a6f8;
long DAT_0061a6f9;
long DAT_0061a738;
long DAT_0061a740;
long DAT_0061a748;
long DAT_0061a750;
long DAT_0061a758;
long DAT_0061a768;
long fde_00418370;
long int7;
long null_ARRAY_00416490;
long null_ARRAY_004164c0;
long null_ARRAY_00416500;
long null_ARRAY_004175e0;
long null_ARRAY_00417780;
long null_ARRAY_00417ac0;
long null_ARRAY_00417d00;
long null_ARRAY_0061a480;
long null_ARRAY_0061a4c0;
long null_ARRAY_0061a560;
long null_ARRAY_0061a590;
long null_ARRAY_0061a5c0;
long null_ARRAY_0061a6c0;
long null_ARRAY_0061a700;
long PTR_DAT_0061a460;
long PTR_FUN_0061a468;
long PTR_null_ARRAY_0061a4b8;
long register0x00000020;
void
FUN_00402460 (uint uParm1)
{
  int iVar1;
  long lVar2;
  undefined8 uVar3;
  undefined **ppuVar4;
  undefined *puVar5;
  undefined *local_88;
  char *local_80;
  undefined8 local_78;
  char *local_70;
  char *local_68;
  char *local_60;
  char *local_58;
  char *local_50;
  char *local_48;
  char *local_40;
  char *local_38;
  char *local_30;
  undefined8 local_28;
  undefined8 local_20;

  if (uParm1 == 0)
    goto LAB_0040248d;
  func_0x00401b70 (DAT_0061a520, "Try \'%s --help\' for more information.\n",
		   DAT_0061a5b8);
  do
    {
      func_0x00401d50 ((ulong) uParm1);
    LAB_0040248d:
      ;
      func_0x00401950 ("Usage: %s [OPTION]... [FILE]...\n", DAT_0061a5b8);
      uVar3 = DAT_0061a500;
      func_0x00401b80
	("Remove (unlink) the FILE(s).\n\n  -f, --force           ignore nonexistent files and arguments, never prompt\n  -i                    prompt before every removal\n",
	 DAT_0061a500);
      func_0x00401b80
	("  -I                    prompt once before removing more than three files, or\n                          when removing recursively; less intrusive than -i,\n                          while still giving protection against most mistakes\n      --interactive[=WHEN]  prompt according to WHEN: never, once (-I), or\n                          always (-i); without WHEN, prompt always\n",
	 uVar3);
      func_0x00401b80
	("      --one-file-system  when removing a hierarchy recursively, skip any\n                          directory that is on a file system different from\n                          that of the corresponding command line argument\n",
	 uVar3);
      func_0x00401b80
	("      --no-preserve-root  do not treat \'/\' specially\n      --preserve-root   do not remove \'/\' (default)\n  -r, -R, --recursive   remove directories and their contents recursively\n  -d, --dir             remove empty directories\n  -v, --verbose         explain what is being done\n",
	 uVar3);
      func_0x00401b80 ("      --help     display this help and exit\n",
		       uVar3);
      func_0x00401b80
	("      --version  output version information and exit\n", uVar3);
      func_0x00401b80
	("\nBy default, rm does not remove directories.  Use the --recursive (-r or -R)\noption to remove each listed directory, too, along with all of its contents.\n",
	 uVar3);
      func_0x00401950
	("\nTo remove a file whose name starts with a \'-\', for example \'-foo\',\nuse one of these commands:\n  %s -- -foo\n\n  %s ./-foo\n",
	 DAT_0061a5b8, DAT_0061a5b8);
      func_0x00401b80
	("\nNote that if you use rm to remove a file, it might be possible to recover\nsome of its contents, given sufficient expertise and/or time.  For greater\nassurance that the contents are truly unrecoverable, consider using shred.\n",
	 uVar3);
      local_88 = &DAT_00415a40;
      local_80 = "test invocation";
      puVar5 = &DAT_00415a40;
      local_78 = 0x415aa6;
      local_70 = "Multi-call invocation";
      local_68 = "sha224sum";
      local_60 = "sha2 utilities";
      local_58 = "sha256sum";
      local_50 = "sha2 utilities";
      local_48 = "sha384sum";
      local_40 = "sha2 utilities";
      local_38 = "sha512sum";
      local_30 = "sha2 utilities";
      local_28 = 0;
      local_20 = 0;
      ppuVar4 = &local_88;
      do
	{
	  iVar1 = func_0x00401ca0 (&DAT_00415a42, puVar5);
	  if (iVar1 == 0)
	    break;
	  ppuVar4 = ppuVar4 + 2;
	  puVar5 = *ppuVar4;
	}
      while (puVar5 != (undefined *) 0x0);
      puVar5 = ppuVar4[1];
      if (puVar5 == (undefined *) 0x0)
	{
	  func_0x00401950 ("\n%s online help: <%s>\n", "GNU coreutils",
			   "https://www.gnu.org/software/coreutils/");
	  lVar2 = func_0x00401d00 (5, 0);
	  if (lVar2 != 0)
	    {
	      iVar1 = func_0x00401c00 (lVar2, &DAT_00415ac7, 3);
	      if (iVar1 != 0)
		{
		  puVar5 = &DAT_00415a42;
		  goto LAB_00402679;
		}
	    }
	  func_0x00401950 ("Full documentation at: <%s%s>\n",
			   "https://www.gnu.org/software/coreutils/",
			   &DAT_00415a42);
	LAB_004026a2:
	  ;
	  puVar5 = &DAT_00415a42;
	  uVar3 = 0x415a5f;
	}
      else
	{
	  func_0x00401950 ("\n%s online help: <%s>\n", "GNU coreutils",
			   "https://www.gnu.org/software/coreutils/");
	  lVar2 = func_0x00401d00 (5, 0);
	  if (lVar2 != 0)
	    {
	      iVar1 = func_0x00401c00 (lVar2, &DAT_00415ac7, 3);
	      if (iVar1 != 0)
		{
		LAB_00402679:
		  ;
		  func_0x00401950
		    ("Report %s translation bugs to <https://translationproject.org/team/>\n",
		     &DAT_00415a42);
		}
	    }
	  func_0x00401950 ("Full documentation at: <%s%s>\n",
			   "https://www.gnu.org/software/coreutils/",
			   &DAT_00415a42);
	  uVar3 = 0x417737;
	  if (puVar5 == &DAT_00415a42)
	    goto LAB_004026a2;
	}
      func_0x00401950
	("or available locally via: info \'(coreutils) %s%s\'\n", puVar5,
	 uVar3);
    }
  while (true);
}


#include "realpath.h"

long null_ARRAY_006158c_0_8_;
long null_ARRAY_006158c_8_8_;
long null_ARRAY_00615ac_0_8_;
long null_ARRAY_00615ac_16_8_;
long null_ARRAY_00615ac_24_8_;
long null_ARRAY_00615ac_32_8_;
long null_ARRAY_00615ac_40_8_;
long null_ARRAY_00615ac_48_8_;
long null_ARRAY_00615ac_8_8_;
long null_ARRAY_00615b0_0_4_;
long null_ARRAY_00615b0_16_8_;
long null_ARRAY_00615b0_24_4_;
long null_ARRAY_00615b0_32_8_;
long null_ARRAY_00615b0_40_4_;
long null_ARRAY_00615b0_4_4_;
long null_ARRAY_00615b0_44_4_;
long null_ARRAY_00615b0_48_4_;
long null_ARRAY_00615b0_8_4_;
long local_2f_1_1_;
long local_4_0_1_;
long local_5b_1_1_;
long local_9c_4_4_;
long local_a1_0_4_;
long local_a1_0_8_;
long local_a1_1_9_;
long local_a1_4_6_;
long local_a1_8_2_;
long local_a3_4_4_;
long local_a4_0_2_;
long local_a4_0_4_;
long local_a4_4_4_;
long local_a5_0_8_;
long local_a5_1_9_;
long local_a5_4_6_;
long local_a5_8_2_;
long local_a6_0_4_;
long DAT_00000008;
long DAT_00000010;
long DAT_0041208f;
long DAT_0041211c;
long DAT_004127e0;
long DAT_004127e1;
long DAT_004127e2;
long DAT_00412890;
long DAT_00412896;
long DAT_00412898;
long DAT_0041289c;
long DAT_004128a0;
long DAT_004128a3;
long DAT_004128a5;
long DAT_004128a9;
long DAT_004128ad;
long DAT_00412e2b;
long DAT_004132b5;
long DAT_004133b9;
long DAT_004133bf;
long DAT_004133d1;
long DAT_004133d2;
long DAT_004133f0;
long DAT_004133f4;
long DAT_0041347e;
long DAT_00615440;
long DAT_00615450;
long DAT_00615460;
long DAT_00615860;
long DAT_00615870;
long DAT_006158d0;
long DAT_006158d4;
long DAT_006158d8;
long DAT_006158dc;
long DAT_00615900;
long DAT_00615910;
long DAT_00615920;
long DAT_00615928;
long DAT_00615940;
long DAT_00615948;
long DAT_00615990;
long DAT_00615998;
long DAT_006159a0;
long DAT_006159a1;
long DAT_006159a8;
long DAT_006159b0;
long DAT_006159b8;
long DAT_00615b38;
long DAT_00615b40;
long DAT_00615b48;
long DAT_00615b50;
long DAT_00615b58;
long DAT_00615b68;
long _DYNAMIC;
long fde_00413ef8;
long int7;
long null_ARRAY_00412820;
long null_ARRAY_00413720;
long null_ARRAY_00413960;
long null_ARRAY_006158c0;
long null_ARRAY_00615960;
long null_ARRAY_006159c0;
long null_ARRAY_00615ac0;
long null_ARRAY_00615b00;
long PTR_DAT_00615868;
long PTR_null_ARRAY_006158b8;
long register0x00000020;
void
FUN_00402420 (uint uParm1)
{
  int iVar1;
  long lVar2;
  undefined8 uVar3;
  undefined **ppuVar4;
  char *pcVar5;
  undefined *puVar6;
  undefined *local_88;
  char *local_80;
  undefined8 local_78;
  char *local_70;
  char *local_68;
  char *local_60;
  char *local_58;
  char *local_50;
  char *local_48;
  char *local_40;
  char *local_38;
  char *local_30;
  undefined8 local_28;
  undefined8 local_20;

  if (uParm1 == 0)
    goto LAB_0040244d;
  func_0x00401950 (DAT_00615920, "Try \'%s --help\' for more information.\n",
		   DAT_006159b8);
  do
    {
      func_0x00401b00 ((ulong) uParm1);
    LAB_0040244d:
      ;
      func_0x00401770 ("Usage: %s [OPTION]... FILE...\n", DAT_006159b8);
      uVar3 = DAT_00615900;
      func_0x00401960
	("Print the resolved absolute file name;\nall but the last component must exist\n\n",
	 DAT_00615900);
      func_0x00401960
	("  -e, --canonicalize-existing  all components of the path must exist\n  -m, --canonicalize-missing   no path components need exist or be a directory\n  -L, --logical                resolve \'..\' components before symlinks\n  -P, --physical               resolve symlinks as encountered (default)\n  -q, --quiet                  suppress most error messages\n      --relative-to=DIR        print the resolved path relative to DIR\n      --relative-base=DIR      print absolute paths unless paths below DIR\n  -s, --strip, --no-symlinks   don\'t expand symlinks\n  -z, --zero                   end each output line with NUL, not newline\n\n",
	 uVar3);
      func_0x00401960 ("      --help     display this help and exit\n",
		       uVar3);
      func_0x00401960
	("      --version  output version information and exit\n", uVar3);
      local_88 = &DAT_0041208f;
      local_80 = "test invocation";
      puVar6 = &DAT_0041208f;
      local_78 = 0x4120fb;
      local_70 = "Multi-call invocation";
      local_68 = "sha224sum";
      local_60 = "sha2 utilities";
      local_58 = "sha256sum";
      local_50 = "sha2 utilities";
      local_48 = "sha384sum";
      local_40 = "sha2 utilities";
      local_38 = "sha512sum";
      local_30 = "sha2 utilities";
      local_28 = 0;
      local_20 = 0;
      ppuVar4 = &local_88;
      do
	{
	  iVar1 = func_0x00401a40 ("realpath", puVar6);
	  if (iVar1 == 0)
	    break;
	  ppuVar4 = ppuVar4 + 2;
	  puVar6 = *ppuVar4;
	}
      while (puVar6 != (undefined *) 0x0);
      pcVar5 = ppuVar4[1];
      if (pcVar5 == (char *) 0x0)
	{
	  func_0x00401770 ("\n%s online help: <%s>\n", "GNU coreutils",
			   "https://www.gnu.org/software/coreutils/");
	  lVar2 = func_0x00401aa0 (5, 0);
	  if (lVar2 != 0)
	    {
	      iVar1 = func_0x004019c0 (lVar2, &DAT_0041211c, 3);
	      if (iVar1 != 0)
		{
		  pcVar5 = "realpath";
		  goto LAB_004025f1;
		}
	    }
	  func_0x00401770 ("Full documentation at: <%s%s>\n",
			   "https://www.gnu.org/software/coreutils/",
			   "realpath");
	LAB_0040261a:
	  ;
	  pcVar5 = "realpath";
	  uVar3 = 0x4120b4;
	}
      else
	{
	  func_0x00401770 ("\n%s online help: <%s>\n", "GNU coreutils",
			   "https://www.gnu.org/software/coreutils/");
	  lVar2 = func_0x00401aa0 (5, 0);
	  if (lVar2 != 0)
	    {
	      iVar1 = func_0x004019c0 (lVar2, &DAT_0041211c, 3);
	      if (iVar1 != 0)
		{
		LAB_004025f1:
		  ;
		  func_0x00401770
		    ("Report %s translation bugs to <https://translationproject.org/team/>\n",
		     "realpath");
		}
	    }
	  func_0x00401770 ("Full documentation at: <%s%s>\n",
			   "https://www.gnu.org/software/coreutils/",
			   "realpath");
	  uVar3 = 0x4133ef;
	  if (pcVar5 == "realpath")
	    goto LAB_0040261a;
	}
      func_0x00401770
	("or available locally via: info \'(coreutils) %s%s\'\n", pcVar5,
	 uVar3);
    }
  while (true);
}
